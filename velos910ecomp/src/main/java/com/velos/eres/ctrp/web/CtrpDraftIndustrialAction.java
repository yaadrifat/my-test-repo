package com.velos.eres.ctrp.web;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.velos.eres.business.common.CodeDao;
import com.velos.eres.service.util.Configuration;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.MC;
import com.velos.eres.service.util.NumberUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.service.util.VelosResourceBundle;

public class CtrpDraftIndustrialAction extends ActionSupport  {

	private static final long serialVersionUID = 858922193329291637L;
	private CtrpDraftJB ctrpDraftJB = null;
	private HttpServletRequest request = null;
	private static final String CTRP_DRAFT_INDUSTRIAL_TILE = "ctrpDraftIndustrialTile";
	private static final String USER_ID = "userId";
	private static final String IP_ADD = "ipAdd";
	private static final String E_SIGN = "eSign";
	private static final String DRAFT_ID = "draftId";
	private static final String STUDY_ID = "studyId";
	private static final String MESSAGE="message";
	private Map errorMap = null;


	
	public CtrpDraftIndustrialAction() {
		request = ServletActionContext.getRequest();
		ctrpDraftJB = new CtrpDraftJB();
	}
	
	public CtrpDraftJB getCtrpDraftJB() { return ctrpDraftJB; }
	
	public void setCtrpDraftJB(CtrpDraftJB ctrpDraftJB) { this.ctrpDraftJB = ctrpDraftJB; }
	
	public String getCtrpDraftIndustrial() {
		try {
			String userId = null;
			try {
				userId = (String)request.getSession(false).getAttribute(USER_ID);
			} finally {
				userId = StringUtil.trueValue(userId);
			}
			ctrpDraftJB.setLoggedUser(Integer.parseInt(userId));
			if (ctrpDraftJB == null) { new CtrpDraftJB(); }
			if (request.getParameter(DRAFT_ID) != null) {
				ctrpDraftJB.retrieveCtrpDraft(StringUtil.stringToNum(request.getParameter(DRAFT_ID)));
				ctrpDraftJB.retreiveCtrpDocumentInfo(StringUtil.stringToNum(request.getParameter(DRAFT_ID)));
				ctrpDraftJB.retrieveStudy(StringUtil.stringToNum(ctrpDraftJB.getFkStudy()));
			} else if (request.getParameter(STUDY_ID) != null) {
				ctrpDraftJB.retrieveStudy(StringUtil.stringToNum(request.getParameter(STUDY_ID)));
			}
		} catch (Exception e) {
			System.out.println("Exception in getCtrpDraftIndustrial in CtrpDraftIndustrialAction ");
			e.printStackTrace();
		}
		return CTRP_DRAFT_INDUSTRIAL_TILE;
	}
	public String updateCtrpDraftIndustrial()
	{
		try {
			if (ctrpDraftJB == null) { new CtrpDraftJB(); }
			ctrpDraftJB.setIndusTrialTypeInt(
					StringUtil.stringToNum(request.getParameter(CtrpDraftJB.FLD_SUBM_TYPE)));
			ctrpDraftJB.setDraftStudyPhase(
					StringUtil.stringToNum(request.getParameter(CtrpDraftJB.FLD_STUDY_PHASE)));
			ctrpDraftJB.setStudyPurpose(
					StringUtil.stringToNum(request.getParameter(CtrpDraftJB.FLD_STUDY_PURPOSE)));
			ctrpDraftJB.setLeadOrgTypeInt(
					StringUtil.stringToNum(request.getParameter(CtrpDraftJB.FLD_LEAD_ORG_TYPE)));
			ctrpDraftJB.setSubOrgTypeInt(
					StringUtil.stringToNum(request.getParameter(CtrpDraftJB.FLD_SUB_ORG_TYPE)));
			ctrpDraftJB.setSumm4Type(request.getParameter(CtrpDraftJB.FLD_SUMM4_TYPE));
			ctrpDraftJB.setSumm4TypeInt(
					StringUtil.stringToNum(request.getParameter(CtrpDraftJB.FLD_SUMM4_TYPE_INT)));
			ctrpDraftJB.setCtrpIntvnTypeInt(
					StringUtil.stringToNum(request.getParameter(CtrpDraftJB.FLD_CTRP_INTERVENTION_TYPE)));
			ctrpDraftJB.setSiteRecruitTypeInt(
					StringUtil.stringToNum(request.getParameter(CtrpDraftJB.FLD_SITE_RECRUIT_STAT)));
		validateCtrpDraftIndustrialBasic();
		if (hasFieldErrors()) {
			Map<String, List<String>> errorMap = getFieldErrors();
			this.errorMap = errorMap;
			return INPUT;
		}
		
		HashMap<String, String> argMap = new HashMap<String, String>();
		String userId = null;
		try {
			userId = (String)request.getSession(false).getAttribute(USER_ID);
		} finally {
			userId = StringUtil.trueValue(userId);
		}
		argMap.put(USER_ID, userId);
		String ipAdd = null;
		try {
			ipAdd = (String)request.getSession(false).getAttribute(IP_ADD);
		} finally {
			ipAdd = StringUtil.trueValue(ipAdd);
		}
		argMap.put(IP_ADD, ipAdd);
		argMap.put(CtrpDraftJB.STATUS_CODE_ID, StringUtil.trueValue(request.getParameter(CtrpDraftJB.FLD_DRAFT_STATUS)));
		Integer output = ctrpDraftJB.updateCtrpDraftIndustrial(argMap);
		if (output < 0) { return ERROR; }
		if (output == 0) { return NONE; }
		if (output > 0){
			  Integer TrialOutput=0;
			  TrialOutput = ctrpDraftJB.setCtrpDraftDocinfo(argMap,output);
				  if (TrialOutput < 0) { return ERROR; }	
		}
		} catch (Exception e) {
			System.out.println("Exception in updateCtrpDraftIndustrial in CtrpDraftIndustrialAction ");
			e.printStackTrace();
		}
		this.errorMap = new HashMap();
		return SUCCESS;
	}
	
	public String validateCtrpDraftIndustrialBasic() {
		String[] dateFormatParams = { DateUtil.getAppDateFormat() };
		
	 	if (!StringUtil.isEmpty((request.getParameter(CtrpDraftJB.FLD_SITE_RECRUIT_DATE)))) {
			if (!DateUtil.isValidDateFormat(request.getParameter(CtrpDraftJB.FLD_SITE_RECRUIT_DATE))) {
				addFieldError(CtrpDraftJB.FLD_SITE_RECRUIT_DATE, 
						VelosResourceBundle.getMessageString("M_IvdDt_PlsVldFmt", (Object[])dateFormatParams));
			}
		}
	 	
	 	if (!StringUtil.isEmpty((request.getParameter(CtrpDraftJB.FLD_OPEN_FOR_ACCRUAL)))) {
			if (!DateUtil.isValidDateFormat(request.getParameter(CtrpDraftJB.FLD_OPEN_FOR_ACCRUAL))) {
				addFieldError(CtrpDraftJB.FLD_OPEN_FOR_ACCRUAL, 
						VelosResourceBundle.getMessageString("M_IvdDt_PlsVldFmt", (Object[])dateFormatParams));
			}
		}
	 	
	 	if (!StringUtil.isEmpty((request.getParameter(CtrpDraftJB.FLD_CLOSED_FOR_ACCRUAL)))) {
			if (!DateUtil.isValidDateFormat(request.getParameter(CtrpDraftJB.FLD_CLOSED_FOR_ACCRUAL))) {
				addFieldError(CtrpDraftJB.FLD_CLOSED_FOR_ACCRUAL, 
						VelosResourceBundle.getMessageString("M_IvdDt_PlsVldFmt", (Object[])dateFormatParams));
			}
		}
	 	
	 	if (!StringUtil.isEmpty((request.getParameter(CtrpDraftJB.FLD_SITE_TRGT_ACRUAL)))) {
	 		try{
	 			if(! NumberUtil.isInteger(request.getParameter(CtrpDraftJB.FLD_SITE_TRGT_ACRUAL))){
	 				addFieldError(CtrpDraftJB.FLD_SITE_TRGT_ACRUAL, MC.M_InvalidNum_EtrAgain);
	 			}else{
			 		if (Integer.parseInt(request.getParameter(CtrpDraftJB.FLD_SITE_TRGT_ACRUAL)) < 0) {
						addFieldError(CtrpDraftJB.FLD_SITE_TRGT_ACRUAL, MC.M_InvalidNum_EtrAgain);
					}
	 			}
	 		} catch (Exception e) {}
		}
		validateESignForUpdateCtrpDraft();
		if (hasFieldErrors()) {
			Map<String, List<String>> errorMap = getFieldErrors();
			this.errorMap = errorMap;
			return INPUT;
		}
		this.errorMap = new HashMap();
		return SUCCESS;
	}
	
	public String validateCtrpDraftIndustrialBusiness() {
		if (ctrpDraftJB == null) { new CtrpDraftJB(); }
		ctrpDraftJB.setTrialSubmTypeInt(
				StringUtil.stringToNum(request.getParameter(CtrpDraftJB.FLD_SUBM_TYPE)));
		ctrpDraftJB.setDraftStudyPhase(
				StringUtil.stringToNum(request.getParameter(CtrpDraftJB.FLD_STUDY_PHASE)));
		ctrpDraftJB.setStudyPurpose(
				StringUtil.stringToNum(request.getParameter(CtrpDraftJB.FLD_STUDY_PURPOSE)));
		ctrpDraftJB.setLeadOrgTypeInt(
				StringUtil.stringToNum(request.getParameter(CtrpDraftJB.FLD_LEAD_ORG_TYPE)));
		ctrpDraftJB.setSumm4Type(request.getParameter(CtrpDraftJB.FLD_SUMM4_TYPE));
		ctrpDraftJB.setSumm4TypeInt(
				StringUtil.stringToNum(request.getParameter(CtrpDraftJB.FLD_SUMM4_TYPE_INT)));
		validateIndusTrialSubmisionCtrpDraft();
		validateIndusTrialIdentifyCtrpDraft();
		validateSectTrialDetailsForCtrpDraft();
		validateIndusTrialLeadSubPiForCtrpDraft();
		validateIndusTrialSumm4ForCtrpDraft();
		validateIndusTrialStatusDatesForCtrpDraft();
		validateIndusTrialDocsForCtrpDraft();
		if (hasFieldErrors()) {
			Map<String, List<String>> errorMap = getFieldErrors();
			this.errorMap = errorMap;
			return INPUT;
		}
		this.errorMap = new HashMap();
		return SUCCESS;
	}
	
	private void validateCtrpDraftIndustrialForUpdate() {
		validateIndusTrialSubmisionCtrpDraft();
		validateIndusTrialIdentifyCtrpDraft();
		validateSectTrialDetailsForCtrpDraft();
		validateIndusTrialLeadSubPiForCtrpDraft();
		validateIndusTrialSumm4ForCtrpDraft();
		validateIndusTrialStatusDatesForCtrpDraft();
		validateIndusTrialDocsForCtrpDraft();
		validateESignForCtrpDraftIndus();
	}

	private void validateESignForUpdateCtrpDraft() {
		if (StringUtil.isEmpty(request.getParameter(E_SIGN))) { 
			addFieldError(CtrpDraftJB.FLD_E_SIGN, MC.M_Etr_Esign_ToProc);
		} else if (request.getSession(false).getAttribute(E_SIGN) != null) {
			if (!((String)request.getSession(false).getAttribute(E_SIGN))
					.equals(request.getParameter(E_SIGN))) {
				addFieldError(CtrpDraftJB.FLD_E_SIGN, MC.M_IncorrEsign_EtrAgain);
			}
		}
	}
	
	private void validateIndusTrialSubmisionCtrpDraft() {
		if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.TRIAL_OWNER_ID))) {
		 	if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.TRIAL_OWNER_FNAME))) {
				addFieldError(CtrpDraftJB.TRIAL_OWNER_FNAME, MC.M_FieldRequired);
			} 
			if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.TRIAL_OWNER_LNAME))) {
				addFieldError(CtrpDraftJB.TRIAL_OWNER_LNAME, MC.M_FieldRequired);
			} 
			if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.TRIAL_OWNER_EMAIL))) {
				addFieldError(CtrpDraftJB.TRIAL_OWNER_EMAIL, MC.M_FieldRequired);
			} 
		}
	}
	
	private void validateIndusTrialIdentifyCtrpDraft() {
		if (!(request.getParameter(CtrpDraftJB.FLD_SUBM_TYPE)).equals(ctrpDraftJB.getTrialSubmTypeO())) {
		 	if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_NCI_TRIAL_ID))) {
				addFieldError(CtrpDraftJB.FLD_NCI_TRIAL_ID, MC.M_FieldRequired);
			}else if(null==StringUtil.stringToInteger(request.getParameter(CtrpDraftJB.FLD_NCI_TRIAL_ID))){
			 		addFieldError(CtrpDraftJB.FLD_NCI_TRIAL_ID, MC.CTRP_DraftFldTakesNum);
		 	}
		}
		if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_ORG_TRIAL_ID))) {
			addFieldError(CtrpDraftJB.FLD_ORG_TRIAL_ID, MC.M_FieldRequired);
		}
		String s1 = StringUtil.trueValue(request.getParameter(CtrpDraftJB.FLD_NCI_TRIAL_ID));
		if (s1.getBytes().length > 100) {
			addFieldError(CtrpDraftJB.FLD_NCI_TRIAL_ID, "Entered value exceeds 100 bytes.");
		}
		s1 = StringUtil.trueValue(request.getParameter(CtrpDraftJB.FLD_ORG_TRIAL_ID));
		if (s1.getBytes().length > 100) {
			addFieldError(CtrpDraftJB.FLD_ORG_TRIAL_ID, "Entered value exceeds 100 bytes.");
		}
		s1 = StringUtil.trueValue(request.getParameter(CtrpDraftJB.FLD_NCT_NUM));
		if (s1.getBytes().length > 20) {
			addFieldError(CtrpDraftJB.FLD_NCT_NUM, "Entered value exceeds 20 bytes.");
		}
		
	}
	private void validateSectTrialDetailsForCtrpDraft() {
	
		if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_STUDY_TYPE))) {
			addFieldError(CtrpDraftJB.FLD_STUDY_TYPE, MC.M_FieldRequired);
		}
		if(StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_NCT_NUM))){
			if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_STUDY_PHASE))) {
				addFieldError(CtrpDraftJB.FLD_STUDY_PHASE, MC.M_FieldRequired);
			}
			
			if(StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_CTRP_INTERVENTION_TYPE)))
			{
				addFieldError(CtrpDraftJB.FLD_CTRP_INTERVENTION_TYPE, MC.M_FieldRequired);
			}
			if(StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_CTRP_INTERVENTION_NAME)))
			{
				addFieldError(CtrpDraftJB.FLD_CTRP_INTERVENTION_NAME, MC.M_FieldRequired);
			}
			if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_STUDY_PURPOSE))) {
				addFieldError(CtrpDraftJB.FLD_STUDY_PURPOSE, MC.M_FieldRequired);
			} else if (ctrpDraftJB.getStudyPurposeOtherPk().equals(
					request.getParameter(CtrpDraftJB.FLD_STUDY_PURPOSE))) {
				if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_STUDY_PURPOSE_OTHER))) {
					addFieldError(CtrpDraftJB.FLD_STUDY_PURPOSE_OTHER, MC.M_FieldRequired);
				}
				String s1 = StringUtil.trueValue(request.getParameter(CtrpDraftJB.FLD_STUDY_PURPOSE_OTHER));
				if (s1.getBytes().length > 1000) {
					addFieldError(CtrpDraftJB.FLD_STUDY_PURPOSE_OTHER, "Entered value exceeds 1000 bytes.");
				}
			}
			if(StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_STUDY_DISEASE_SITE)))
			{
				addFieldError(CtrpDraftJB.FLD_STUDY_DISEASE_SITE, MC.M_FieldRequired+" "+MC.CTRP_DraftPlsUpdtStudy);
			}
		}else{
			
			if (!StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_STUDY_PURPOSE))
					&& ctrpDraftJB.getStudyPurposeOtherPk().equals(request.getParameter(CtrpDraftJB.FLD_STUDY_PURPOSE))) {
				if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_STUDY_PURPOSE_OTHER))) {
					addFieldError(CtrpDraftJB.FLD_STUDY_PURPOSE_OTHER, MC.M_FieldRequired);
				}
				String s1 = StringUtil.trueValue(request.getParameter(CtrpDraftJB.FLD_STUDY_PURPOSE_OTHER));
				if (s1.getBytes().length > 1000) {
					addFieldError(CtrpDraftJB.FLD_STUDY_PURPOSE_OTHER, "Entered value exceeds 1000 bytes.");
				}
			}
		}
	}
	
	private void validateIndusTrialLeadSubPiForCtrpDraft() {
		if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_LEAD_ORG_ID)) &&
				StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_LEAD_ORG_FK_SITE))) {
			addFieldError(CtrpDraftJB.FLD_LEAD_ORG_ID, MC.CTRP_PerformThisOperation);
		}
		if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_LEAD_ORG_ID)) &&
				!StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_LEAD_ORG_FK_SITE))){
			Integer fkAddress = StringUtil.stringToInteger(request.getParameter(CtrpDraftJB.FLD_LEAD_ORG_FK_ADD));
			if (fkAddress == null || fkAddress < 0){
				addFieldError(CtrpDraftJB.FLD_LEAD_ORG_ID, MC.CTRP_CompleteAddressReqd);
			}else{
				validateAddressForCtrpDraftIndustrial(CtrpDraftJB.FLD_LEAD_ORG_ID,"LEAD_ORG");
			}
		}
		
		if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_SUB_ORG_ID)) &&
				StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_SUB_ORG_FK_SITE))) {
			addFieldError(CtrpDraftJB.FLD_SUB_ORG_ID, MC.CTRP_PerformThisOperation);
		}
		if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_SUB_ORG_ID)) &&
				!StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_SUB_ORG_FK_SITE))){
			Integer fkAddress = StringUtil.stringToInteger(request.getParameter(CtrpDraftJB.FLD_SUB_ORG_FK_ADD));
			if (fkAddress == null || fkAddress < 0){
				addFieldError(CtrpDraftJB.FLD_SUB_ORG_ID, MC.CTRP_CompleteAddressReqd);
			}else{
				validateAddressForCtrpDraftIndustrial(CtrpDraftJB.FLD_SUB_ORG_ID, "SUB_ORG");
			}
		}
		
		if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_PI_ID)) &&
				StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_PI_FK_USER))) {
			addFieldError(CtrpDraftJB.FLD_PI_ID, MC.CTRP_PerformThisOperation);
		}
		if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_PI_ID)) &&
				!StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_PI_FK_USER))){
			Integer fkAddress = StringUtil.stringToInteger(request.getParameter(CtrpDraftJB.FLD_PI_FK_ADD));
			if (fkAddress == null || fkAddress < 0){
				addFieldError(CtrpDraftJB.FLD_PI_ID, MC.CTRP_CompleteAddressReqd);
			}else{
				validateAddressForCtrpDraftIndustrial(CtrpDraftJB.FLD_PI_ID,"PI");
			}
		}

		if (ctrpDraftJB.getIsNCIDesignated()!= null && ctrpDraftJB.getIsNCIDesignated()==1) {
			if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_SUB_ORG_PROG_CODE))) {
				addFieldError(CtrpDraftJB.FLD_SUB_ORG_PROG_CODE, MC.M_FieldRequired);
			}
		}
	}
	private void validateIndusTrialSumm4ForCtrpDraft() {
	 	if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_SUMM4_ID)) &&
				StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_SUMM4_FK_SITE))) {
			addFieldError(CtrpDraftJB.FLD_SUMM4_ID, MC.CTRP_PerformThisOperation);
		}
	 	if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_SUMM4_ID)) &&
				!StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_SUMM4_FK_SITE))){
			Integer fkAddress = StringUtil.stringToInteger(request.getParameter(CtrpDraftJB.FLD_SUMM4_FK_ADD));
			if (fkAddress == null || fkAddress < 0){
				addFieldError(CtrpDraftJB.FLD_SUMM4_ID, MC.CTRP_CompleteAddressReqd);
			}else{
				validateAddressForCtrpDraftIndustrial(CtrpDraftJB.FLD_SUMM4_ID, "SUMM4");
			}
		}		
	}
	private void validateIndusTrialStatusDatesForCtrpDraft() {
		String s1 = null;
		String[] dateFormatParams = { DateUtil.getAppDateFormat() };
			if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_SITE_RECRUIT_STAT))) {
				addFieldError(CtrpDraftJB.FLD_SITE_RECRUIT_STAT, MC.M_FieldRequired);
			}
			s1 = StringUtil.trueValue(request.getParameter(CtrpDraftJB.FLD_SITE_RECRUIT_DATE));
		 	if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_SITE_RECRUIT_DATE))) {
				addFieldError(CtrpDraftJB.FLD_SITE_RECRUIT_DATE, MC.M_FieldRequired);
			}else if (!DateUtil.isValidDateFormat(s1)) {
				addFieldError(CtrpDraftJB.FLD_SITE_RECRUIT_DATE, 
						VelosResourceBundle.getMessageString("M_IvdDt_PlsVldFmt", (Object[])dateFormatParams));
			}
		 	
			if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_CLOSED_FOR_ACCRUAL)) && StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_OPEN_FOR_ACCRUAL))) {
				addFieldError(CtrpDraftJB.FLD_OPEN_FOR_ACCRUAL, MC.M_FieldRequired);
			}
			s1 = StringUtil.trueValue(request.getParameter(CtrpDraftJB.FLD_OPEN_FOR_ACCRUAL));
			if (!(StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_OPEN_FOR_ACCRUAL)))) {
				if (!DateUtil.isValidDateFormat(s1)) {
					addFieldError(CtrpDraftJB.FLD_OPEN_FOR_ACCRUAL, 
						VelosResourceBundle.getMessageString("M_IvdDt_PlsVldFmt", (Object[])dateFormatParams));
				}
			}
			s1 = StringUtil.trueValue(request.getParameter(CtrpDraftJB.FLD_CLOSED_FOR_ACCRUAL));
			if (!(StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_CLOSED_FOR_ACCRUAL)))) {
				if (!DateUtil.isValidDateFormat(s1)) {
					addFieldError(CtrpDraftJB.FLD_CLOSED_FOR_ACCRUAL, 
						VelosResourceBundle.getMessageString("M_IvdDt_PlsVldFmt", (Object[])dateFormatParams));
				}
			}
			if (ctrpDraftJB.getIsNCIDesignated()!= null && ctrpDraftJB.getIsNCIDesignated()==1) {
				if (StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_SITE_TRGT_ACRUAL))) {
					addFieldError(CtrpDraftJB.FLD_SITE_TRGT_ACRUAL, MC.M_FieldRequired);
				}else if(null==StringUtil.stringToInteger(request.getParameter(CtrpDraftJB.FLD_SITE_TRGT_ACRUAL))){
			 		addFieldError(CtrpDraftJB.FLD_SITE_TRGT_ACRUAL, MC.CTRP_DraftFldTakesNum);
			 	} 
			}
	}
	
	private void validateIndusTrialDocsForCtrpDraft() {
		if(StringUtil.isEmpty(request.getParameter(CtrpDraftJB.FLD_NCT_NUM)) && 
				(null == StringUtil.stringToInteger(request.getParameter(CtrpDraftJB.FLD_CTRP_ABBTRIAL_DOC_PK))
					||	0 == StringUtil.stringToInteger(request.getParameter(CtrpDraftJB.FLD_CTRP_ABBTRIAL_DOC_PK))
				)){
				addFieldError(CtrpDraftJB.FLD_CTRP_ABBTRIAL_DOC_DESC, MC.M_FieldRequired);
		}
		if (null != StringUtil.stringToInteger(request.getParameter(CtrpDraftJB.FLD_CTRP_ABBTRIAL_DOC_PK))){
			if (Integer.parseInt(request.getParameter(CtrpDraftJB.FLD_CTRP_ABBTRIAL_DOC_PK))>0) {
				if( request.getParameter(CtrpDraftJB.FLD_CTRP_ABBTRIAL_DOC_DESC).indexOf(".doc")==-1 &&
						request.getParameter(CtrpDraftJB.FLD_CTRP_ABBTRIAL_DOC_DESC).indexOf(".xls")==-1 &&
						request.getParameter(CtrpDraftJB.FLD_CTRP_ABBTRIAL_DOC_DESC).indexOf(".pdf")==-1 &&
						request.getParameter(CtrpDraftJB.FLD_CTRP_ABBTRIAL_DOC_DESC).indexOf(".wp")==-1)
				{
				 addFieldError(CtrpDraftJB.FLD_CTRP_ABBTRIAL_DOC_DESC,MC.CTRP_DraftPlsChkDocType);
				}
			}
		}
		if (null != StringUtil.stringToInteger(request.getParameter(CtrpDraftJB.FLD_CTRP_OTHER_DOC_PK))){
			if (Integer.parseInt(request.getParameter(CtrpDraftJB.FLD_CTRP_OTHER_DOC_PK))>0 ) {
				if( request.getParameter(CtrpDraftJB.FLD_CTRP_OTHER_DOC_DESC).indexOf(".doc")==-1 &&
						request.getParameter(CtrpDraftJB.FLD_CTRP_OTHER_DOC_DESC).indexOf(".xls")==-1 &&
						request.getParameter(CtrpDraftJB.FLD_CTRP_OTHER_DOC_DESC).indexOf(".pdf")==-1 &&
						request.getParameter(CtrpDraftJB.FLD_CTRP_OTHER_DOC_DESC).indexOf(".wp")==-1)
				{
				 addFieldError(CtrpDraftJB.FLD_CTRP_OTHER_DOC_DESC,MC.CTRP_DraftPlsChkDocType);
				}
			}
		}
	}
	private void validateESignForCtrpDraftIndus() {
		if (StringUtil.isEmpty(request.getParameter(E_SIGN))) { 
			addFieldError(CtrpDraftJB.FLD_E_SIGN, MC.M_Etr_Esign_ToProc);
		} else if (request.getSession(false).getAttribute(E_SIGN) != null) {
			if (!((String)request.getSession(false).getAttribute(E_SIGN))
					.equals(request.getParameter(E_SIGN))) {
				addFieldError(CtrpDraftJB.FLD_E_SIGN, MC.M_IncorrEsign_EtrAgain);
			}
		}
	}
	
	private void validateAddressForCtrpDraftIndustrial(String idField, String addressIdentifier) {
		int addressComplete = 1;
		
		if (addressIdentifier != null){
			synchronized(CtrpDraftJB.class) {
				Field[] fields = CtrpDraftJB.class.getFields();
				for (Field field : fields) {
					String fieldName = field.getName();
					boolean checkMe = false;
					if (fieldName.equals("FLD_"+addressIdentifier+"_STREET")
						|| fieldName.equals("FLD_"+addressIdentifier+"_CITY")
						|| fieldName.equals("FLD_"+addressIdentifier+"_STATE")
						|| fieldName.equals("FLD_"+addressIdentifier+"_ZIP")
						|| fieldName.equals("FLD_"+addressIdentifier+"_COUNTRY")
						|| fieldName.equals("FLD_"+addressIdentifier+"_EMAIL"))
					{
						checkMe = true;
					}else{
						if (fieldName.equals("FLD_"+addressIdentifier+"_PHONE") && addressIdentifier.equals("PI"))
							checkMe = true;
					}
					
					if (checkMe){
						try {
							String fieldValue = (String) field.get(null);
							
							if (StringUtil.isEmpty(request.getParameter(fieldValue))) {
								if (addressComplete == 1){
									addressComplete = 0;
									addFieldError(idField, MC.CTRP_CompleteAddressReqd);
								}
								addFieldError(fieldValue, MC.M_FieldRequired);							
							}
						} catch(Exception e) {
							System.out.println(e.getMessage());
						}
					}
				}
			}
		}
	}

	public void setErrorMap(Map errorMap) {
		this.errorMap = errorMap;
	}

	public Map getErrorMap() {
		return errorMap;
	}

	public String markCtrpDraftReadyForIndustrial() {
		try {
			//Saving the data to make sure un-saved but validated data is saved 
			if (SUCCESS.equals(this.updateCtrpDraftIndustrial())){
				HashMap<String, String> argMap = new HashMap<String, String>();
				String userId = null;
				try {
					userId = (String)request.getSession(false).getAttribute(USER_ID);
				} finally {
					userId = StringUtil.trueValue(userId);
				}
				argMap.put(USER_ID, userId);
				String ipAdd = null;
				try {
					ipAdd = (String)request.getSession(false).getAttribute(IP_ADD);
				} finally {
					ipAdd = StringUtil.trueValue(ipAdd);
				}
				argMap.put(IP_ADD, ipAdd);
				
				CodeDao cdDraftStatus = new CodeDao();
				int readySubmissionPK =  cdDraftStatus.getCodeId("ctrpDraftStatus", "readySubmission");
				
				argMap.put(CtrpDraftJB.STATUS_CODE_ID, ""+readySubmissionPK);
				int draftId = ctrpDraftJB.getId();
				ctrpDraftJB.retrieveCtrpDraft(draftId);
				Integer output = ctrpDraftJB.markCtrpDraftReadyForSubmission(argMap);
				if (output < 0) {
					request.setAttribute(MESSAGE, MC.M_DataNtSvd_Succ);
					return ERROR;
				}
				
				request.setAttribute(MESSAGE, MC.M_Data_SavedSucc);
				return SUCCESS;
			} else {
				request.setAttribute(MESSAGE, MC.M_DataNtSvd_Succ);
				return ERROR;
			}
		} catch (Exception e){
			request.setAttribute(MESSAGE, MC.M_DataNtSvd_Succ);
			return ERROR;
		}
	}
}
