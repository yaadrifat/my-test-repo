package com.velos.services.studypatient;

import java.util.List;

import javax.ejb.Remote;

import com.velos.services.OperationException;
import com.velos.services.OperationRolledBackException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.Patient;
import com.velos.services.model.PatientEnrollmentDetails;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyPatient;
/**
 * Remote Interface with declaration of methods in Service Implementation.
 * @author Virendra
 *
 */
@Remote
public interface StudyPatientService{
	/**
	 * 
	 * @param studyId
	 * @return
	 * @throws OperationException
	 */
	public List<StudyPatient> getStudyPatients(StudyIdentifier studyId) throws OperationException;

	/**
	 * Enrolls Patient to Study
	 * 
	 * @param patientIdentifier identifies patient to be enrolled
	 * 
	 * @param studyIdentifier identifies study to which patient has to be enrolled
	 * 
	 * @param patientEntrollmentDetails gives detailed information about patient status on study
	 * 
	 * @return A ResponseHolder instance that may hold information about database persisting 
	 * actions that occurred as a result of this operation
	 * 
	 * @throws OperationException
	 */
	public ResponseHolder enrollPatientToStudy(
			PatientIdentifier patientIdentifier, StudyIdentifier studyIdentifier, PatientEnrollmentDetails patientEntrollmentDetails) throws OperationException;
	
	
	/**
	 * Creates Patient and enroll created Patient to Study
	 * 
	 * @param Patient identifies patient to be created and enrolled to study
	 * 
	 * @param studyIdentifier identifies study to which patient has to be enrolled
	 * 
	 * @param patientEntrollmentDetails gives detailed information about patient status on study
	 * 
	 * @return A ResponseHolder instance that may hold information about database persisting 
	 * actions that occurred as a result of this operation
	 * 
	 * @throws OperationException
	 */
	public ResponseHolder createAndEnrollPatient(Patient patient, StudyIdentifier studyIdentifier, PatientEnrollmentDetails patientEnrollmentDetails)
	throws OperationException; 

}