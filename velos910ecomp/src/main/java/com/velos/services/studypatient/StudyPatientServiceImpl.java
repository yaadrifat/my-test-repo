package com.velos.services.studypatient;

import static javax.ejb.TransactionAttributeType.REQUIRED;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.apache.log4j.Logger;


import com.velos.eres.business.common.StudyDao;
import com.velos.eres.business.common.StudySiteDao;
import com.velos.eres.service.grpRightsAgent.GrpRightsAgentRObj;
import com.velos.eres.service.patFacilityAgent.PatFacilityAgentRObj;
import com.velos.eres.service.patProtAgent.PatProtAgentRObj;
import com.velos.eres.service.patStudyStatAgent.PatStudyStatAgentRObj;
import com.velos.eres.service.personAgent.PersonAgentRObj;
import com.velos.eres.service.siteAgent.SiteAgentRObj;
import com.velos.eres.service.studyAgent.StudyAgent;
import com.velos.eres.service.studySiteAgent.StudySiteAgentRObj;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.userSiteAgent.UserSiteAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.services.AbstractService;
import com.velos.services.AuthorizationException;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.authorization.GroupAuthModule;
import com.velos.services.authorization.TeamAuthModule;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.Patient;
import com.velos.services.model.PatientEnrollmentDetails;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyPatient;
import com.velos.services.util.ObjectLocator;
/**
 * Implementation for StuyPatient Service 
 * @author Virendra
 *
 */
@Stateless
@Remote(StudyPatientService.class)
public class StudyPatientServiceImpl 
extends AbstractService 
implements StudyPatientService{

@EJB
private ObjectMapService objectMapService;
@EJB
GrpRightsAgentRObj groupRightsAgent;
@EJB
private UserAgentRObj userAgent;
@Resource 
private SessionContext sessionContext;
@EJB
private StudySiteAgentRObj studySiteAgent;
@EJB
private PatStudyStatAgentRObj patStudyStatAgent; 
@EJB
private PersonAgentRObj personAgent; 
@EJB
private SiteAgentRObj siteAgent;
@EJB
private PatProtAgentRObj patProtAgent;
@EJB
private UserSiteAgentRObj userSiteAgent;
@EJB
private PatFacilityAgentRObj patFacilityAgent;
@EJB
private StudyAgent studyAgent;

private static Logger logger = Logger.getLogger(StudyPatientServiceImpl.class.getName());
	/**
	 * getStudyPatients with param StudyIdentifier
	 */
	public List<StudyPatient> getStudyPatients(StudyIdentifier studyIdentifier) throws OperationException {
		
		try{
			
			//call to ObjectLocator for StudyPk
			Integer studyPK = ObjectLocator.studyPKFromIdentifier(callingUser, studyIdentifier, objectMapService);
			objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY, studyPK);
			
			if(studyPK ==0 || studyPK == null){
				addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found"));
				throw new OperationException("Study not found");
				
			}
			GroupAuthModule groupAuth = 
				new GroupAuthModule(callingUser, groupRightsAgent);
			Integer manageProtocolPriv = 
				groupAuth.getAppManageProtocolsPrivileges();
			
			boolean hasViewManageProt = 
				GroupAuthModule.hasViewPermission(manageProtocolPriv);
			
			if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
			if (!hasViewManageProt){
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to view study data"));
				throw new AuthorizationException("User Not Authorized to view study data");
			}
			
	
			Integer managePatientsPriv = 
				groupAuth.getAppManagePatientsPrivileges();
			
			boolean hasViewManagePat = 
				GroupAuthModule.hasViewPermission(managePatientsPriv);
			
			if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + managePatientsPriv);
			if (!hasViewManagePat){
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to view patient data"));
				throw new AuthorizationException("User Not Authorized to view patient data");
			}
			
			// Handle access control in the same way as the where clause in Patient.getStudyPatientsSQL().
			// Get sitePK and pass it into StudyPatientDAO.
			StudySiteDao studySiteDao = studySiteAgent.getSitesForStatAndEnrolledPat(studyPK, 
			        callingUser.getUserId(), 0); // 0 = accountId, which is not used in the SQL
			ArrayList siteIds = studySiteDao.getSiteIds();
			Integer sitePK = 0;
			ArrayList<StudyPatient> listCompleteStudyPatient = new ArrayList<StudyPatient>();
			//Virendra:#6757
			if (siteIds != null && siteIds.size() > 0) {
				for(int i = 0 ; i< siteIds.size(); i++){
					sitePK = (Integer)siteIds.get(i);
					listCompleteStudyPatient.addAll((ArrayList<StudyPatient>) StudyPatientDAO.
							getStudyPatientByStudyPK(studyPK, sitePK));
				}
			}
			
			if(listCompleteStudyPatient.isEmpty()){
				addIssue(new Issue(IssueTypes.PATIENT_NOT_FOUND, "Patient not found"));
				throw new OperationException("Patient not found");
			}
			//Virendra:#6074
			TeamAuthModule teamAuthModule = new TeamAuthModule(callingUser.getUserId(), studyPK);
			int patientManagePrivileges = teamAuthModule.getPatientManagePrivileges();//StudyTeamPrivileges();
			
			if (!TeamAuthModule.hasViewPermission(patientManagePrivileges)){
				if (logger.isDebugEnabled()) logger.debug("user does not have permission to view study patient");
			
				addIssue(
						new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, 
								"User does not have permission to view study patient"));
				throw new AuthorizationException("User does not have permission to view study patient");
			}
			//Hide PHI information if user not having complete patient data view rights 
			Integer managePatientPrivComplete = 
				groupAuth.getAppViewCompletePatientDataPrivileges();
			//Virendra:#6044
			int viewPatientDetailsPrivileges = teamAuthModule.getPatientViewDataPrivileges();
			
			if(managePatientPrivComplete == 3 || 
					(!TeamAuthModule.hasViewPermission(viewPatientDetailsPrivileges)) ){
				return getLstPhiStudyPatient(listCompleteStudyPatient);
			}
			return listCompleteStudyPatient;
		}
		catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", t);
			throw new OperationException(t);
		}
	}
	
	
	private ArrayList<StudyPatient> getLstPhiStudyPatient(
			ArrayList<StudyPatient> lstStudyPat) throws OperationException{
		
		for(int i = 0; i < lstStudyPat.size(); i++ ){
			StudyPatient studyPat = lstStudyPat.get(i);
			studyPat.setStudyPatFirstName("*");
			studyPat.setStudyPatLastName("*");
		}
		return lstStudyPat;
	}


	@TransactionAttribute(REQUIRED)
	public ResponseHolder enrollPatientToStudy(
			PatientIdentifier patientIdentifier, StudyIdentifier studyIdentifier, PatientEnrollmentDetails patientEnrollmentDetails)
	throws OperationException {
		try{
			
			
			if(patientIdentifier == null || studyIdentifier == null || patientEnrollmentDetails == null)
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "StudyIdentifier, patientDemographics, patientEnrollmentDetails are required to enroll patient to Study"));
				throw new OperationException();
			}
			// check users group right to manage Patients
			GroupAuthModule groupAuth = 
				new GroupAuthModule(callingUser, groupRightsAgent);
			Integer managePatientPriv = 
				groupAuth.getAppManagePatientsPrivileges();
			boolean hasNewManagePat = 
				GroupAuthModule.hasEditPermission(managePatientPriv);


			if(!hasNewManagePat)
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to edit patient  data"));
				throw new AuthorizationException("User Not Authorized to edit patient data");
			}



			// get StudyPK for checking Team rights for Study
			Integer studyPK = ObjectLocator.studyPKFromIdentifier(callingUser, studyIdentifier, objectMapService); 
			if(studyPK == null || studyPK == 0){
				addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found for StudyIdentifier OID:" + studyIdentifier.getOID() + " StudyNumber " + studyIdentifier.getStudyNumber()));
				throw new OperationException();
			}

			TeamAuthModule teamAuth = new TeamAuthModule(callingUser.getUserId(), studyPK); 
			Integer managePatientEnrollPriv = teamAuth.getPatientManagePrivileges(); 
			boolean hasEnrollPatientPriv = TeamAuthModule.hasNewPermission(managePatientEnrollPriv); 

			if(!hasEnrollPatientPriv)
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized enroll patient to this Study"));
				throw new AuthorizationException("User Not Authorized to enroll patient");
			}
			
			canEnrollToThisStudy(studyPK); 

			Map<String, Object> parameters = new HashMap<String, Object>(); 

			parameters.put("personAgent", personAgent);
			parameters.put("sessionContext", sessionContext); 
			parameters.put("userAgent", userAgent);
			parameters.put("siteAgent", siteAgent);
			parameters.put("objectMapService", objectMapService);
			parameters.put("patProtAgent", patProtAgent) ;
			parameters.put("userSiteAgent", userSiteAgent);
			parameters.put("patFacilityAgent", patFacilityAgent);
			parameters.put("callingUser", callingUser);
			parameters.put("ResponseHolder", response); 
			parameters.put("studySiteAgent", studySiteAgent); 
			parameters.put("patStudyStatAgent", patStudyStatAgent); 
			parameters.put("studyAgent", studyAgent); 

			PatientEnrollmentHandler handler = new PatientEnrollmentHandler(); 
			handler.enrollPatientToStudy(patientIdentifier, studyPK, patientEnrollmentDetails, parameters);
		}catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly(); 
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", t);
			throw new OperationException(t);
		}

		return response;
	}
	
	
	@TransactionAttribute(REQUIRED)
	public ResponseHolder createAndEnrollPatient(Patient patient, StudyIdentifier studyIdentifier, PatientEnrollmentDetails patientEnrollmentDetails)
	throws OperationException
	{

		try{
			
			if(patient == null || patient.getPatientDemographics() == null || studyIdentifier == null || patientEnrollmentDetails == null)
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "StudyIdentifier, patientDemographics, patientEnrollmentDetails are required to create and enroll patient to Study"));
				throw new OperationException();
			}
			
			GroupAuthModule authModule = new GroupAuthModule(callingUser, groupRightsAgent);
			int managePatients = authModule.getAppManagePatientsPrivileges();
			boolean hasNewPatientPermissions = GroupAuthModule.hasNewPermission(managePatients); 
			if(!hasNewPatientPermissions)
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User not authorized to add patient data"));
				throw new AuthorizationException("User not authorized to add patient data");
			}
			
			
			
			GroupAuthModule groupAuth = 
				new GroupAuthModule(callingUser, groupRightsAgent);
			Integer managePatientPriv = 
				groupAuth.getAppManagePatientsPrivileges();
			boolean hasNewManagePat = 
				GroupAuthModule.hasEditPermission(managePatientPriv);


			if(!hasNewManagePat)
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to edit patient  data"));
				throw new AuthorizationException("User Not Authorized to edit patient data");
			}



			// get StudyPK for checking Team rights for Study
			Integer studyPK = ObjectLocator.studyPKFromIdentifier(callingUser, studyIdentifier, objectMapService); 
			if(studyPK == null || studyPK == 0){
				addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found for StudyIdentifier OID:" + studyIdentifier.getOID() + " StudyNumber " + studyIdentifier.getStudyNumber()));
				throw new OperationException();
			}

			TeamAuthModule teamAuth = new TeamAuthModule(callingUser.getUserId(), studyPK); 
			Integer managePatientEnrollPriv = teamAuth.getPatientManagePrivileges(); 
			boolean hasEnrollPatientPriv = TeamAuthModule.hasNewPermission(managePatientEnrollPriv); 

			if(!hasEnrollPatientPriv)
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized enroll patient to this Study"));
				throw new AuthorizationException("User Not Authorized to enroll patient");
			}
	
			canEnrollToThisStudy(studyPK);
			canEnrollToThisOrg(studyPK, patient);//BugFix: 10887
			
			Map<String, Object> parameters = new HashMap<String, Object>(); 
			parameters.put("personAgent", personAgent);
			parameters.put("sessionContext", sessionContext); 
			parameters.put("userAgent", userAgent);
			parameters.put("siteAgent", siteAgent);
			parameters.put("objectMapService", objectMapService);
			parameters.put("patProtAgent", patProtAgent) ;
			parameters.put("userSiteAgent", userSiteAgent);
			parameters.put("patFacilityAgent", patFacilityAgent);
			parameters.put("callingUser", callingUser);
			parameters.put("ResponseHolder", response); 
			parameters.put("studySiteAgent", studySiteAgent); 
			parameters.put("patStudyStatAgent", patStudyStatAgent); 
			parameters.put("studyAgent", studyAgent); 

			PatientEnrollmentHandler handler = new PatientEnrollmentHandler(); 
			handler.createAndEnrollPatient(patient, studyPK, patientEnrollmentDetails, parameters); 
		}catch(OperationException e){
			sessionContext.setRollbackOnly();			
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			sessionContext.setRollbackOnly(); 	
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", t);
			throw new OperationException(t);
		}

		return response;		
	}
	
	
	private boolean canEnrollToThisStudy(Integer studyPK) throws OperationException
	{
		StudyDao studydao = studyAgent.getUserStudies(EJBUtil.integerToString(callingUser.getUserId()), "dStudy", 0, "active");
		boolean canEnroll = false; 
		for(int i = 0; i < studydao.getStudyIds().size(); i++)
		{
			int studyID = EJBUtil.stringToInteger(studydao.getStudyIds().get(i).toString());
			if(studyID == studyPK)
			{
			   canEnroll = true; 
			   break; 
			}
		}
		
		if(!canEnroll)
		{
			addIssue(
					new Issue(IssueTypes.DATA_VALIDATION, 
							"Patient cannot be enroll to Study provided"));
			throw new AuthorizationException("Patient cannot be enrolled to Study provided");
		}
		 
		return canEnroll; 

	}
	//Bug Fix:10887
	private boolean canEnrollToThisOrg(Integer studyPK, Patient patient) throws OperationException{
		
		boolean canEnroll = false;
		StudySiteDao studySiteDao = studySiteAgent.getStdSitesLSampleSize(studyPK);
		for(int i =0; i < studySiteDao.getStudySiteIds().size(); i++){
					
			String patientSiteName = patient.getPatientDemographics().getOrganization().getSiteName();
			
			if(studySiteDao.getSiteNames().get(i).toString().compareTo(patientSiteName) == 0){
				
				canEnroll = true;
				break;
			}
		
	}
		if(!canEnroll){
			addIssue(
					new Issue(IssueTypes.DATA_VALIDATION, 
							"Patient Organization is not part of the Study Team Organization"));
			throw new AuthorizationException("Patient cannot be enrolled to Study provided");
		}
		
		return canEnroll;
	}
	



	@AroundInvoke
	public Object myInterceptor(InvocationContext ctx) throws Exception {
		response = new ResponseHolder();
		callingUser = 
			getLoggedInUser(
					sessionContext,
					userAgent);
		return ctx.proceed();

	}
	

}