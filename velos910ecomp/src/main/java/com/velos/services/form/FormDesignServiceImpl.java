/**
 * Created On Sep 9, 2011
 */
package com.velos.services.form;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.LinkedFormsDao;
import com.velos.eres.business.formLib.impl.FormLibBean;
import com.velos.eres.business.linkedForms.impl.LinkedFormsBean;
import com.velos.eres.service.formLibAgent.FormLibAgentRObj;
import com.velos.eres.service.grpRightsAgent.GrpRightsAgentRObj;
import com.velos.eres.service.linkedFormsAgent.LinkedFormsAgentRObj;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.esch.service.util.EJBUtil;
import com.velos.services.AbstractService;
import com.velos.services.AuthorizationException;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.authorization.GroupAuthModule;
import com.velos.services.authorization.TeamAuthModule;
import com.velos.services.map.MultipleObjectsFoundException;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.FormDesign;
import com.velos.services.model.FormIdentifier;
import com.velos.services.model.FormInfo;
import com.velos.services.model.FormList;
import com.velos.services.model.FormType;
import com.velos.services.model.LinkedFormDesign;
import com.velos.services.model.LinkedFormDesign.DisplayType;
import com.velos.services.model.LinkedFormDesign.EntrySettings;
import com.velos.services.model.StudyFormDesign;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyPatientFormDesign;
import com.velos.services.model.StudyPatientScheduleFormDesign;
import com.velos.services.util.EnumUtil;
import com.velos.services.util.ObjectLocator;

/**
 * @author Kanwaldeep
 *
 */
@Stateless
@Remote(FormDesignService.class)
public class FormDesignServiceImpl extends AbstractService implements FormDesignService {
	
	private static Logger logger = Logger.getLogger(FormDesignServiceImpl.class); 
	@EJB
	private ObjectMapService objectMapService;
	
	@EJB
	private UserAgentRObj userAgent;
	
	@EJB
	GrpRightsAgentRObj groupRightsAgent;
	
	@EJB
	private LinkedFormsAgentRObj linkedFormAgent; 
	
	@EJB
	private FormLibAgentRObj formLibAgent;

	
	@Resource 
	private SessionContext sessionContext;	
	
	@AroundInvoke
	public Object myInterceptor(InvocationContext ctx) throws Exception {
		response = new ResponseHolder();
		callingUser = 
			getLoggedInUser(
					sessionContext,
					userAgent);
		return ctx.proceed();

	}

	/* (non-Javadoc)
	 * @see com.velos.services.form.FormDesignService#getLibraryFormDesign(com.velos.services.model.FormIdentifier)
	 */
	public FormDesign getLibraryFormDesign(FormIdentifier formIdentifier, boolean includeFormatting, String formName)
			throws OperationException {
		
		try{
		FormDesignDAO dao = new FormDesignDAO();
		//check permissions for user to view Forms
		checkLibraryFormDesignViewPermissions(); 
		Integer formPK;
		try {
			formPK = ObjectLocator.formPKFromIdentifier(callingUser, formIdentifier, objectMapService,formName, FormType.LIBRARY, null);
			if(formPK == null || formPK ==0)
			{
				addIssue(new Issue(IssueTypes.FORM_NOT_FOUND, "Form not found")); 
				throw new OperationException(); 
			}
			
			if(!dao.getLibraryFormListUserHasAccess(callingUser.getUserId()).contains(formPK))
			{
				addIssue(new Issue(IssueTypes.FORM_DESIGN_AUTHORIZATION, "User is not authorized to view this form")); 
				throw new OperationException(); 
			}
			FormLibBean formlibBean = formLibAgent.getFormLibDetails(formPK); 
			if(!(callingUser.getUserAccountId().equals(formlibBean.getAccountId())))
			{
				addIssue(new Issue(IssueTypes.FORM_NOT_FOUND, "Form not found")); 
				throw new OperationException(); 
			}
			
			//Now check type if type is same as method or not	
			if(!formlibBean.getFormLibLinkTo().equals(FormType.LIBRARY.getValue()))
			{
				addIssue(new Issue(IssueTypes.FORM_TYPE_MISMATCH, "Form not found")); 
				throw new OperationException();
			}
			
					
		} catch (MultipleObjectsFoundException e) {
			addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND, "Mulitple forms found with information provided"));
		  throw new OperationException(); 
		} 
		if(formPK == null || formPK == 0)
		{
			addIssue(new Issue(IssueTypes.FORM_NOT_FOUND, "Form not found")); 
			throw new OperationException(); 
		}
		
	
		FormDesign formDesign = new FormDesign();
		return dao.getLibraryFormDesign(formPK, includeFormatting, callingUser, objectMapService, formDesign); 
		}catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("StudyServiceImpl getStudySummary", e); 
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyServiceImpl getStudySummary", t);
			throw new OperationException(t);
		}
		
	}

	/* (non-Javadoc)
	 * @see com.velos.services.form.FormDesignService#getStudyFormDesign(com.velos.services.model.FormIdentifier, com.velos.services.model.StudyIdentifier)
	 */
	public StudyFormDesign getStudyFormDesign(FormIdentifier formIdentifier,
			StudyIdentifier studyIdentifier, String formName, boolean includeFormatting) throws OperationException {
		StudyFormDesign studyFormDesign = new StudyFormDesign();
		FormDesignDAO formDesignDAO = new FormDesignDAO(); 
		try{
			
			//Dependent on Bug 7377. Bug need to be Fixed. My Service will return first form it will find with this name 
			if((formIdentifier == null || formIdentifier.getOID() == null || formIdentifier.getOID().length() == 0)
					&& (formName == null || formName.length() == 0 || studyIdentifier == null || (studyIdentifier.getOID() == null && studyIdentifier.getStudyNumber() == null)))
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "FormIdentifier or FormName and StudyIdentifier is required to retrieve StudyForm"));
				throw new OperationException();
			}
			Integer formPK; 
			
			try{		
				formPK  = ObjectLocator.formPKFromIdentifier(callingUser, formIdentifier, objectMapService, formName, FormType.STUDY, studyIdentifier) ;	
			}catch(OperationException oe)
			{
				addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found")); 
				throw new OperationException(); 				
			}

			if(formPK == null || formPK ==0)
			{
				addIssue(new Issue(IssueTypes.FORM_NOT_FOUND, "Form not found")); 
				throw new OperationException(); 
			}
			// get StudyID from Linked Form 
			LinkedFormsBean formBean = linkedFormAgent.findByFormId(formPK); 
			Integer studyID = null; 
			if(formBean != null)
			{
				if(!formBean.getAccountId().equals(callingUser.getUserAccountId()))
				{
					addIssue(new Issue(IssueTypes.FORM_NOT_FOUND, "Form not found")); 
					throw new OperationException(); 				
				}
				studyID = EJBUtil.stringToInteger(formBean.getStudyId()); 
			}
			
			if(formBean == null || !(formBean.getLFDisplayType().trim().equals(DisplayType.STUDY.toString())
					|| formBean.getLFDisplayType().equals(DisplayType.ALL_STUDIES.toString())))
			{
				addIssue(new Issue(IssueTypes.FORM_TYPE_MISMATCH, "Requested Form is not StudyForm")); 
				throw new OperationException();
			}
			//Integer studyID = ObjectLocator.studyPKFromIdentifier(callingUser, studyIdentifier, objectMapService); 
			if(studyID != null) checkStudyFormDesignViewPermissions(studyID); 
						
			boolean hasFormAccessFromStudy = false; 
			// This form can be viewed from FormManagement no matter whether user has StudyTeam Rights to view FormDesign. 
			GroupAuthModule authModule = new GroupAuthModule(callingUser, groupRightsAgent);				 		
			Integer manageAccountFormManagement = authModule.getFormsAccountFormManagementPrivileges(); 
				
			boolean hasFormManagementAccess = GroupAuthModule.hasViewPermission(manageAccountFormManagement); 
					
					

			
			if(hasFormManagementAccess == false)
			{
				Integer managePatient = authModule.getAppManagePatientsPrivileges(); 
				if(GroupAuthModule.hasViewPermission(managePatient))
				{
					LinkedFormsDao linkedformsDao = linkedFormAgent.getStudyForms(EJBUtil.stringToInteger(callingUser.getUserAccountId()), studyID, callingUser.getUserId(), EJBUtil.stringToInteger(callingUser.getUserSiteId())); 
					if(linkedformsDao.getFormId().contains(formPK)) hasFormAccessFromStudy = true; 
				}
				
		
			}
			
			if(hasFormManagementAccess || hasFormAccessFromStudy)
			{
				formDesignDAO.getStudyPatientFormDesign(formPK, callingUser, objectMapService, studyFormDesign, studyID, includeFormatting);
					

			}else{
				addIssue(new Issue(IssueTypes.FORM_DESIGN_AUTHORIZATION, "User not authorized to view this form design")); 
				throw new OperationException(); 
			}	
			
	}catch(OperationException e){
		sessionContext.setRollbackOnly();
		if (logger.isDebugEnabled()) logger.debug("FormDesignServiceImpl getStudyFormDesign", e); 
		e.setIssues(response.getIssues());
		throw e;
	}
	catch(Throwable t){
		sessionContext.setRollbackOnly();
		this.addUnknownThrowableIssue(t);
		if (logger.isDebugEnabled()) logger.debug("FormDesignServiceImpl getStudyFormDesign", t);
		throw new OperationException(t);
	}
	
		return studyFormDesign;
	}

	/* (non-Javadoc)
	 * @see com.velos.services.form.FormDesignService#getAccountFormDesign(com.velos.services.model.FormIdentifier)
	 */
	public LinkedFormDesign getAccountFormDesign(FormIdentifier formIdentifier, String formName, boolean includeFormatting)
			throws OperationException {

		LinkedFormDesign linkedFormDesign = new LinkedFormDesign(); 
		
		FormDesignDAO formDesignDAO = new FormDesignDAO(); 
		Integer formPK; 
		
		try{
			formPK = ObjectLocator.formPKFromIdentifier(callingUser, formIdentifier, objectMapService, formName, FormType.ACCOUNT, null); 
			
			
			formDesignDAO.getAccountPatientFormDesign(formPK, FormType.ACCOUNT, callingUser, objectMapService, linkedFormDesign, includeFormatting); 
			
		}catch(MultipleObjectsFoundException e)
		{
			e.printStackTrace(); 
		}
		
		return linkedFormDesign;
	}

	/* (non-Javadoc)
	 * @see com.velos.services.form.FormDesignService#getPatientFormDesign(com.velos.services.model.FormIdentifier)
	 */
	public LinkedFormDesign getPatientFormDesign(FormIdentifier formIdentifier, String formName)
			throws OperationException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.velos.services.form.FormDesignService#getStudyPatientFormDesign(com.velos.services.model.FormIdentifier, com.velos.services.model.StudyIdentifier)
	 */
	public StudyPatientFormDesign getStudyPatientFormDesign(FormIdentifier formIdentifier,
			StudyIdentifier studyIdentifier,String formName, boolean includeFormatting) throws OperationException {

		StudyPatientFormDesign studyPatientFormDesign  = new StudyPatientFormDesign(); 
		FormDesignDAO formDesignDAO = new FormDesignDAO(); 
		try{
			//Dependent on Bug 7377. Bug need to be Fixed. My Service will return first form it will find with this name 
			if((formIdentifier == null || formIdentifier.getOID() == null || formIdentifier.getOID().length() == 0)
					&& (formName == null || formName.length() == 0 || studyIdentifier == null || (studyIdentifier.getOID() == null && studyIdentifier.getStudyNumber() == null)))
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "FormIdentifier or FormName and StudyIdentifier is required to retrieve StudyPatientForm"));
				throw new OperationException();
			}
			Integer formPK; 
			
			try{		
				formPK  = ObjectLocator.formPKFromIdentifier(callingUser, formIdentifier, objectMapService, formName, FormType.STUDYPATIENT, studyIdentifier) ;	
			}catch(OperationException oe)
			{
				addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found")); 
				throw new OperationException(); 				
			}

			if(formPK == null || formPK ==0)
			{
				addIssue(new Issue(IssueTypes.FORM_NOT_FOUND, "Form not found")); 
				throw new OperationException(); 
			}
			// get StudyID from Linked Form 
			LinkedFormsBean formBean = linkedFormAgent.findByFormId(formPK); 
			Integer studyID = null; 
			if(formBean != null)
			{
				if(!formBean.getAccountId().equals(callingUser.getUserAccountId()))
				{
					addIssue(new Issue(IssueTypes.FORM_NOT_FOUND, "Form not found")); 
					throw new OperationException(); 				
				}
				studyID = EJBUtil.stringToInteger(formBean.getStudyId()); 
			}
			
			if(formBean == null || !(formBean.getLFDisplayType().equals(DisplayType.PATIENT_ENROLLED_TO_SPECIFIC_STUDY.toString())
					|| formBean.getLFDisplayType().equals(DisplayType.PATIENT_LEVEL_ALL_STUDIES.toString())
							|| formBean.getLFDisplayType().equals(DisplayType.PATIENT_LEVEL_ALL_STUDIES_RESTRICTED.toString())))
			{
				addIssue(new Issue(IssueTypes.FORM_TYPE_MISMATCH, "Requested Form is not StudyPatientForm")); 
				throw new OperationException();
			}
			//Integer studyID = ObjectLocator.studyPKFromIdentifier(callingUser, studyIdentifier, objectMapService); 
			if(studyID != null) checkStudyFormDesignViewPermissions(studyID); 
						
			boolean hasFormAccessFromStudy = false; 
			// This form can be viewed from FormManagement no matter whether user has StudyTeam Rights to view FormDesign. 
			GroupAuthModule authModule = new GroupAuthModule(callingUser, groupRightsAgent);				 		
			Integer manageAccountFormManagement = authModule.getFormsAccountFormManagementPrivileges(); 
				
			boolean hasFormManagementAccess = GroupAuthModule.hasViewPermission(manageAccountFormManagement); 
					
					
//			if(hasFormManagementAccess == false && studyID != null)
//			{
//			
//					if(checkPatientStudyFormDesignViewPermissions(studyID) == true)
//					{
//						if(formDesignDAO.getStudyPatientFormListUserHasAccess(callingUser, studyID).contains(formPK)) hasFormAccessFromStudy = true; 
//					}
//				
//			}
			
			if(hasFormManagementAccess == false)
			{
				Integer managePatient = authModule.getAppManagePatientsPrivileges(); 
				if(GroupAuthModule.hasViewPermission(managePatient))
				{
					if(formDesignDAO.getStudyPatientFormListUserHasAccess(callingUser, studyID, false).contains(formPK)) hasFormAccessFromStudy = true; 
				}
				
		
			}
			
			if(hasFormManagementAccess || hasFormAccessFromStudy)
			{
				formDesignDAO.getStudyPatientFormDesign(formPK, callingUser, objectMapService, studyPatientFormDesign, studyID, includeFormatting);
					

			}else{
				addIssue(new Issue(IssueTypes.FORM_DESIGN_AUTHORIZATION, "User not authorized to view this form design")); 
				throw new OperationException(); 
			}
			
			
		}catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("FormDesignServiceImpl getStudyPatientFormDesign", e); 
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("FormDesignServiceImpl getStudyPatientFormDesign", t);
			throw new OperationException(t);
		}
		
			return studyPatientFormDesign;
	}

	/* (non-Javadoc)
	 * @see com.velos.services.form.FormDesignService#getAllFormsForStudy(com.velos.services.model.StudyIdentifier)
	 */
	public FormList getAllFormsForStudy(
			StudyIdentifier studyIdentifier) throws OperationException {
		
		Integer studyId = ObjectLocator.studyPKFromIdentifier(callingUser, studyIdentifier, objectMapService); 
		List<FormInfo> formInfoList = new ArrayList<FormInfo>();
	
		
		if(studyId == null || studyId == 0)	
		{
			//TODO throw exception
		}
		
		
		GroupAuthModule groupAuthModule = new GroupAuthModule(callingUser, groupRightsAgent); 
		Integer manageProtocolPriv = groupAuthModule.getAppManageProtocolsPrivileges();
		
		
		Integer manageStudyFormsPriv = groupAuthModule.getFormsAllStudyFormsPrivileges(); 
		
		
		TeamAuthModule teamAuthModule = new TeamAuthModule(callingUser.getUserId(), studyId);

		
		
		//TODO check group rights and study rights
		
		 LinkedFormsDao lnkFrmDao = new LinkedFormsDao();
		 lnkFrmDao = linkedFormAgent.getStudyForms(EJBUtil.stringToInteger(callingUser.getUserAccountId()), studyId, callingUser.getUserId(), EJBUtil.stringToInteger(callingUser.getUserSiteId())); 
		 
		 
		
		 
		 List formIDs  = lnkFrmDao.getFormId();		 
		 List formNames = lnkFrmDao.getFormName();
		 
		 for(int i =0; i < formIDs.size(); i++)
		 {
			 FormInfo formInfo = new FormInfo();  
			 ObjectMap objectMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_FORM_LIBRARY, (Integer) formIDs.get(i)); 
			 FormIdentifier formIdentifier = new FormIdentifier(); 
			 formIdentifier.setOID(objectMap.getOID()); 
			 
			 formInfo.setFormIdentifier(formIdentifier); 
			 formInfo.setFormName(formNames.get(i).toString()); 
			 formInfoList.add(formInfo); 
			 
		 }
		
		 FormList formList = new FormList(); 
		 formList.setFormInfo(formInfoList); 
		 return formList ;
	}
	
	private boolean checkLibraryFormDesignViewPermissions() throws AuthorizationException
	{
		GroupAuthModule authModule = new GroupAuthModule(callingUser, groupRightsAgent); 
		Integer manageFormLibrary = authModule.getFormsFormLibraryPrivileges(); 		
		Integer manageAccountFormManagement = authModule.getFormsAccountFormManagementPrivileges(); 
		
		boolean hasViewPermissions = GroupAuthModule.hasViewPermission(manageFormLibrary) || GroupAuthModule.hasNewPermission(manageAccountFormManagement); 
		
		
		if(!hasViewPermissions)
		{		
			addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to view Form Design"));
				throw new AuthorizationException("User Not Authorized to view Form Design");
		
		}	
		
		return hasViewPermissions; 
	}
	
	private boolean checkStudyFormDesignViewPermissions(Integer studyPK) throws AuthorizationException
	{			
		TeamAuthModule teamModule = new TeamAuthModule(callingUser.getUserId(), studyPK);		
		Integer formsManagement = teamModule.getFormsStudyManagementPrivileges(); 
		Integer formsAccess = teamModule.getFormsStudyAccessPrivileges(); 
		
		boolean hasViewPermissions = TeamAuthModule.hasViewPermission(formsManagement|formsAccess); 
		
		if(!hasViewPermissions)
		{
			addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User Not Authorized for Study"));
			throw new AuthorizationException("User Not Authorized to view Form Design for this Study");
	
		}
		
		return hasViewPermissions; 
		
	}

	public StudyPatientScheduleFormDesign getStudyPatientScheduleFormDesign(
			FormIdentifier formIdentifier, StudyIdentifier studyIdentifier,
			String formName, boolean includeFormatting)
			throws OperationException {
		
		StudyPatientScheduleFormDesign studyPatientScheduleFormDesign  = new StudyPatientScheduleFormDesign(); 
		FormDesignDAO formDesignDAO = new FormDesignDAO(); 
		try{
			//Dependent on Bug 7377. Bug need to be Fixed. My Service will return first form it will find with this name 
			if((formIdentifier == null || formIdentifier.getOID() == null || formIdentifier.getOID().length() == 0)
					&& (formName == null || formName.length() == 0 || studyIdentifier == null || (studyIdentifier.getOID() == null && studyIdentifier.getStudyNumber() == null)))
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "FormIdentifier or FormName and StudyIdentifier is required to retrieve StudyPatientForm"));
				throw new OperationException();
			}
			Integer formPK; 
			
			try{		
				formPK  = ObjectLocator.formPKFromIdentifier(callingUser, formIdentifier, objectMapService, formName, FormType.STUDYPATIENT, studyIdentifier) ;	
			}catch(OperationException oe)
			{
				addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found")); 
				throw new OperationException(); 				
			}

			if(formPK == null || formPK ==0)
			{
				addIssue(new Issue(IssueTypes.FORM_NOT_FOUND, "Form not found")); 
				throw new OperationException(); 
			}
			// get StudyID from Linked Form 
			LinkedFormsBean formBean = linkedFormAgent.findByFormId(formPK); 
			Integer studyID = null; 
			if(formBean != null)
			{
				if(!formBean.getAccountId().equals(callingUser.getUserAccountId()))
				{
					addIssue(new Issue(IssueTypes.FORM_NOT_FOUND, "Form not found")); 
					throw new OperationException(); 				
				}
				studyID = EJBUtil.stringToInteger(formBean.getStudyId()); 
			}
			
			if(!formDesignDAO.isFormLinkedToAnyEvent(formPK) || formBean == null || !(formBean.getLFDisplayType().equals(DisplayType.PATIENT_ENROLLED_TO_SPECIFIC_STUDY.toString())
					|| formBean.getLFDisplayType().equals(DisplayType.PATIENT_LEVEL_ALL_STUDIES.toString())
							|| formBean.getLFDisplayType().equals(DisplayType.PATIENT_LEVEL_ALL_STUDIES_RESTRICTED.toString())))
			{
				addIssue(new Issue(IssueTypes.FORM_TYPE_MISMATCH, "Requested Form is not StudyPatientScheduleForm")); 
				throw new OperationException();
			}
			//Integer studyID = ObjectLocator.studyPKFromIdentifier(callingUser, studyIdentifier, objectMapService); 
			if(studyID != null) checkStudyFormDesignViewPermissions(studyID); 
						
			boolean hasFormAccessFromStudy = false; 
			// This form can be viewed from FormManagement no matter whether user has StudyTeam Rights to view FormDesign. 
			GroupAuthModule authModule = new GroupAuthModule(callingUser, groupRightsAgent);				 		
			Integer manageAccountFormManagement = authModule.getFormsAccountFormManagementPrivileges(); 
				
			boolean hasFormManagementAccess = GroupAuthModule.hasViewPermission(manageAccountFormManagement); 
					

			
			if(hasFormManagementAccess == false)
			{
				Integer managePatient = authModule.getAppManagePatientsPrivileges(); 
				if(GroupAuthModule.hasViewPermission(managePatient))
				{
					if(formDesignDAO.getStudyPatientFormListUserHasAccess(callingUser, studyID, false).contains(formPK)) hasFormAccessFromStudy = true; 
				}
				
		
			}
			
			if(hasFormManagementAccess || hasFormAccessFromStudy)
			{
				formDesignDAO.getStudyPatientFormDesign(formPK, callingUser, objectMapService, studyPatientScheduleFormDesign, studyID, includeFormatting);
					

			}else{
				addIssue(new Issue(IssueTypes.FORM_DESIGN_AUTHORIZATION, "User not authorized to view this form design")); 
				throw new OperationException(); 
			}
			
			
		}catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("FormDesignServiceImpl getStudyPatientFormDesign", e); 
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("FormDesignServiceImpl getStudyPatientFormDesign", t);
			throw new OperationException(t);
		}
		
			return studyPatientScheduleFormDesign;
		
	
	}
	
//	private boolean checkPatientStudyFormDesignViewPermissions(Integer studyPK) throws AuthorizationException
//	{			
//		TeamAuthModule teamModule = new TeamAuthModule(callingUser.getUserId(), studyPK);
//	//	Integer patientManagement = teamModule.getPatientManagePrivileges(); 
//		Integer formsManagement = teamModule.getFormsStudyManagementPrivileges(); 
//		Integer formsAccess = teamModule.getFormsStudyAccessPrivileges(); 
//		
//		boolean hasViewPermissions = TeamAuthModule.hasViewPermission(formsManagement|formsAccess); 
//		
//		if(!hasViewPermissions)
//		{
//			addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User Not Authorized to view Form Design"));
//			throw new AuthorizationException("User Not Authorized to view Form Design");
//	
//		}
//		
//		return hasViewPermissions; 
//		
//	}
//	
	

}
