/**
 * 
 */
package com.velos.services.monitoring;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.services.OperationException;


/**
 * @author dylan
 * Implementation class for Session Service related operations.
 *
 */
  
@Stateless
@Remote(SessionService.class)
public class SessionServiceImpl implements SessionService{

	@PersistenceContext(unitName = "eres")
    protected EntityManager em;
	
	public SessionLog setSessionDetails(SessionLog session) {
		em.persist(session);
		return session;
	}

	
	public SessionLog getSessionDetails(Integer sessionPK) {
  		return (SessionLog)em.find(SessionLog.class, sessionPK);
	}

	public SessionLog updateSessionDetails(SessionLog session) {
		em.merge(session);
		return session;
	}

}
