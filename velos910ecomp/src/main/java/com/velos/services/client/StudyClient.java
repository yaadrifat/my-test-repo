/**
 * 
 */
package com.velos.services.client;

import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.Study;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyOrganization;
import com.velos.services.model.StudyStatus;
import com.velos.services.model.StudyStatuses;
import com.velos.services.model.StudyStatusIdentifier;
import com.velos.services.model.StudySummary;
import com.velos.services.model.StudyTeamMember;
import com.velos.services.model.UserIdentifier;
import com.velos.services.study.StudyService;
import com.velos.services.util.JNDINames;

/**
 * @author dylan
 *
 */
public class StudyClient{
	
	/**
	 * 	
	 * @param studyId
	 * @return
	 * @throws OperationException
	 */
	public static Study getStudy(StudyIdentifier studyId) 
	
	throws 
		OperationException{

		StudyService studyRemote = getStudyRemote();
		Study study =  studyRemote.getStudy(studyId);
	 
		return study;
		
	}
	
	/**
	 * 	
	 * @param studyId
	 * @return
	 * @throws OperationException
	 */
	public static StudySummary getStudySummary(StudyIdentifier studyId) 
	
	throws 
		OperationException{

		StudyService studyRemote = getStudyRemote();
		StudySummary studySummary =  studyRemote.getStudySummary(studyId);
	 
		return studySummary;
		
	}
	/**
	 * 
	 * @param studyId
	 * @param studySummary
	 * @return
	 * @throws OperationException
	 */
	public static ResponseHolder updateStudySummary(
			StudyIdentifier studyId, 
			StudySummary studySummary,
			boolean createNonSystemUsers)
	throws 
		OperationException{
		
		StudyService studyRemote = getStudyRemote();
		
		ResponseHolder response = 
			studyRemote.updateStudySummary(
					studyId, 
					studySummary,
					createNonSystemUsers);
		return response;
	}
	
	
	/**
	 * 
	 * @param study
	 * @param createNonSystemUsers
	 * @return
	 * @throws OperationException
	 */
	public static ResponseHolder create(Study study, boolean createNonSystemUsers)
	throws
		OperationException{
		
		StudyService studyRemote = getStudyRemote();
		
		ResponseHolder response = studyRemote.createStudy(study, createNonSystemUsers);
		return response;
	}

		
	/**
	 * 
	 * @param studyId
	 * @param studyTeamMember
	 * @param createNonSystemUsers
	 * @return
	 * @throws OperationException
	 */
	public static ResponseHolder addStudyTeamMember(
			StudyIdentifier studyId, 
			StudyTeamMember studyTeamMember, 
			boolean createNonSystemUsers) 
	throws OperationException{
		StudyService studyRemote = getStudyRemote();
		return studyRemote.addStudyTeamMember(studyId, studyTeamMember, createNonSystemUsers);
	}
	
	/**
	 * 
	 * @param studyId
	 * @param studyTeamMember
	 * @return
	 * @throws OperationException
	 */
	public static ResponseHolder updateStudyTeamMember(
			StudyIdentifier studyId, 
			StudyTeamMember studyTeamMember) 
	throws OperationException{
		StudyService studyRemote = getStudyRemote();
		return studyRemote.updateStudyTeamMember(studyId, studyTeamMember);
	}
	
	/**
	 * 
	 * @param studyId
	 * @param teamUserId
	 * @return
	 * @throws OperationException
	 */
	public static ResponseHolder removeStudyTeamMember(
			StudyIdentifier studyId, 
			UserIdentifier teamUserId) 
	throws OperationException{
		StudyService studyRemote = getStudyRemote();
		return studyRemote.removeStudyTeamMember(studyId, teamUserId);
	}
	
	/**
	 * 
	 * @param studyId
	 * @param studyOrganization
	 * @return
	 * @throws OperationException
	 */
	public static ResponseHolder addStudyOrganization(
			StudyIdentifier studyId, 
			StudyOrganization studyOrganization) 
	throws OperationException{
		StudyService studyRemote = getStudyRemote();
		return studyRemote.addStudyOrganization(studyId, studyOrganization);
	}
	
	/**
	 * 
	 * @param studyId
	 * @param studyOrganization
	 * @return
	 * @throws OperationException
	 */
	public static ResponseHolder updateStudyOrganization(
			StudyIdentifier studyId, 
			StudyOrganization studyOrganization) 
	throws OperationException{
		StudyService studyRemote = getStudyRemote();
		return studyRemote.updateStudyOrganization(studyId, studyOrganization);
	}
	
	/**
	 * 
	 * @param studyId
	 * @param organizationId
	 * @return
	 * @throws OperationException
	 */
	public static ResponseHolder removeStudyOrganization(
			StudyIdentifier studyId, 
			OrganizationIdentifier organizationId) 
	throws OperationException{
		StudyService studyRemote = getStudyRemote();
		return studyRemote.removeStudyOrganization(studyId, organizationId);
	}
	
	/**
	 * 
	 * @param studyId
	 * @param studyStatus
	 * @return
	 * @throws OperationException
	 */
	public static ResponseHolder addStudyStatus(
			StudyIdentifier studyId, 
			StudyStatus studyStatus) 
	throws OperationException{
		StudyService studyRemote = getStudyRemote();
		return studyRemote.addStudyStatus(studyId, studyStatus);
	}
	
//	/**
//	 * 
//	 * @param studyId
//	 * @param studyStatus
//	 * @return
//	 * @throws OperationException
//	 */
//	public static ResponseHolder updateStudyStatus(
//			StudyIdentifier studyId, 
//			StudyStatus studyStatus) 
//	throws OperationException{
//		StudyService studyRemote = getStudyRemote();
//		return studyRemote.updateStudyStatus(studyId, studyStatus);
//	}
//	
	/**
	 * 
	 * @param study
	 * @param createNonSystemUsers
	 * @return
	 * @throws OperationException
	 */
	public static ResponseHolder createStudy(Study study, boolean createNonSystemUsers)
			throws OperationException {
		StudyService studyRemote = getStudyRemote();
		return studyRemote.createStudy(study, createNonSystemUsers);
	}
	
	/**
	 * 
	 * @return
	 * @throws NamingException
	 */
	private static StudyService getStudyRemote()
		throws OperationException{
		//TODO Should this be a member of this class so that we only get the studyremote once??
		
		StudyService studyRemote = null;
		InitialContext ic;
		try{
			ic = new InitialContext();
			studyRemote =
				(StudyService) ic.lookup(JNDINames.StudyServiceImpl);
		}
		catch(NamingException e){
			throw new OperationException(e);
		}
		return studyRemote;
	}

	public static StudyStatuses getStudyStatuses(
			StudyIdentifier studyIdentifier)
	throws OperationException{
		StudyService studyRemote = getStudyRemote();
		return studyRemote.getStudyStatuses(studyIdentifier);
		
	}
	/**
	 * Calls getStudyStatus on getStudyStatus Remote object
	 * to return StudyStatus
	 * @param StudyStatusIdentifier
	 * @return StudyStatus
	 * @throws OperationException
	 */
	public static StudyStatus getStudyStatus(StudyStatusIdentifier statusIdentifier) 
	throws OperationException{
		StudyService studyService = getStudyRemote();
		return studyService.getStudyStatus(statusIdentifier); 
	}


}
