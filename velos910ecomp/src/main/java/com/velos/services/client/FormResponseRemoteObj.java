/**
 * Created On Dec 19, 2011
 */
package com.velos.services.client;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import com.velos.services.OperationException;
import com.velos.services.OperationRolledBackException;
import com.velos.services.ResponseHolder;

import com.velos.services.formresponse.FormResponseService;
import com.velos.services.model.CalendarIdentifier;
import com.velos.services.model.EventIdentifier;
import com.velos.services.model.FormResponse;
import com.velos.services.model.StudyFormResponse;
import com.velos.services.model.StudyFormResponseIdentifier;
import com.velos.services.model.StudyPatientFormResponse;
import com.velos.services.model.StudyPatientFormResponseIdentifier;
import com.velos.services.model.StudyPatientScheduleFormResponse;
import com.velos.services.model.VisitIdentifier;
import com.velos.services.util.JNDINames;

/**
 * @author Kanwaldeep
 *
 */
public class FormResponseRemoteObj {

	private static FormResponseService getFormResponseRemote() throws OperationException
	{
		FormResponseService formResponseRemote = null; 
		InitialContext ic; 
		
		try
		{
			ic = new InitialContext(); 
			formResponseRemote = (FormResponseService) ic.lookup(JNDINames.FormResponseServiceImpl);
			
			
		}catch(NamingException e){
				throw new OperationException(e);
		}
		
		return formResponseRemote; 
	}
	
	
	public static ResponseHolder createStudyFormResponse(
			StudyFormResponse studyFormResponse)
	throws OperationRolledBackException, OperationException {
		
		FormResponseService formResponseService = getFormResponseRemote(); 
		 return formResponseService.createStudyFormResponse(studyFormResponse); 
		
	}
	
	public static StudyFormResponse getStudyFormResponse(StudyFormResponseIdentifier studyFormResponseIdentifier)
	throws OperationRolledBackException, OperationException
	{
		FormResponseService formResponseService = getFormResponseRemote(); 
		return formResponseService.getStudyFormResponse(studyFormResponseIdentifier); 
	}
	
	public static ResponseHolder createStudyPatientFormResponse(StudyPatientFormResponse studyPatientFormResponse) throws OperationException
	{
		FormResponseService formResponseService = getFormResponseRemote(); 
		return formResponseService.createStudyPatientFormResponse(studyPatientFormResponse); 
	}
	
	
	public static StudyPatientFormResponse getStudyPatientFormResponse(StudyPatientFormResponseIdentifier formResponseIdentifier, boolean includeDesign) throws OperationException
	{
		FormResponseService formResponseService = getFormResponseRemote(); 
		return formResponseService.getStudyPatientFormResponse(formResponseIdentifier); 
	}


	/**
	 * @param formResponseIdentifier
	 * @param studyPatientFormResponse
	 * @return
	 */
	public static ResponseHolder updateStudyPatientFormResponse(
			StudyPatientFormResponseIdentifier formResponseIdentifier,
			FormResponse studyPatientFormResponse) throws OperationException{
		FormResponseService formResponseService = getFormResponseRemote(); 
		return formResponseService.updateStudyPatientFormResponse(formResponseIdentifier, studyPatientFormResponse);
	}
	
	public static ResponseHolder removeStudyPatientFormResponse(
			StudyPatientFormResponseIdentifier formResponseIdentifier) throws OperationException{
		FormResponseService formResponseService = getFormResponseRemote(); 
		return formResponseService.removeStudyPatientFormResponse(formResponseIdentifier);
	}


	/**
	 * @param studyPatientScheduleFormResponse
	 * @param calendarIdentifier
	 * @param calendarName
	 * @param visitIdentifier
	 * @param visitName
	 * @param eventIdentifier
	 * @param eventName
	 * @return
	 * @throws OperationException 
	 */
	public static ResponseHolder createStudyPatientScheduleFormResponse(
			StudyPatientScheduleFormResponse studyPatientScheduleFormResponse,
			CalendarIdentifier calendarIdentifier, String calendarName,
			VisitIdentifier visitIdentifier, String visitName,
			EventIdentifier eventIdentifier, String eventName) throws OperationException {
		FormResponseService formResponseService = getFormResponseRemote(); 
		return formResponseService.createStudyPatientScheduleFormResponse(studyPatientScheduleFormResponse, calendarIdentifier, calendarName, visitIdentifier, visitName, eventIdentifier, eventName);
	}


	/**
	 * @param studyPatientFormResponseIdentifier
	 * @return
	 */
	public static StudyPatientScheduleFormResponse getStudyPatientScheduleFormResponse(
			StudyPatientFormResponseIdentifier studyPatientFormResponseIdentifier) throws OperationException{
		FormResponseService formResponseService = getFormResponseRemote(); 
		return formResponseService.getStudyPatientScheduleFormResponse(studyPatientFormResponseIdentifier); 
		
	}
	
	public static ResponseHolder updateStudyPatientScheduleFormResponse(
			StudyPatientFormResponseIdentifier studyPatientFormResponseIdentifier,
			StudyPatientScheduleFormResponse studyPatientScheduleFormResponse) throws OperationException {
		
		FormResponseService formResponseService = getFormResponseRemote(); 
		return formResponseService.updateStudyPatientScheduleFormResponse(studyPatientFormResponseIdentifier, studyPatientScheduleFormResponse); 
		
	}


	/**
	 * @param studyPatientFormResponseIdentifier
	 * @return
	 */
	public static ResponseHolder removeStudyPatientScheduleFormResponse(
			StudyPatientFormResponseIdentifier studyPatientFormResponseIdentifier) throws OperationException{
		FormResponseService formResponseService = getFormResponseRemote(); 
		return formResponseService.removeStudyPatientScheduleFormResponse(studyPatientFormResponseIdentifier);
	}


	/**
	 * @param formResponseIdentifier
	 * @param studyFormResponse
	 */
	public static ResponseHolder updateStudyFormResponse(
			StudyFormResponseIdentifier formResponseIdentifier,
			StudyFormResponse studyFormResponse) 
	throws OperationException{
		FormResponseService formResponseService = getFormResponseRemote(); 
		return formResponseService.updateStudyFormResponse(formResponseIdentifier, studyFormResponse); 
		
	}
	
	
	public static ResponseHolder removeStudyFormResponse(
			StudyFormResponseIdentifier formResponseIdentifier)
	throws OperationException
	{
		FormResponseService formResponseService = getFormResponseRemote(); 
		return formResponseService.removeStudyFormResponse(formResponseIdentifier); 
	}
		
}
