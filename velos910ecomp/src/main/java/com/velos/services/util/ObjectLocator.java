/**
 * 
 */
package com.velos.services.util;

import java.util.ArrayList;

import javax.ejb.SessionContext;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.SiteDao;
import com.velos.eres.business.common.StudyDao;
import com.velos.eres.business.common.UserDao;
import com.velos.eres.business.patProt.impl.PatProtBean;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.patProtAgent.PatProtAgentRObj;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.util.StringUtil;
import com.velos.esch.service.util.EJBUtil;
import com.velos.services.AuthenticationException;
import com.velos.services.OperationException;
import com.velos.services.budget.BudgetServiceDAO;
import com.velos.services.form.FormDesignDAO;
import com.velos.services.map.MultipleObjectsFoundException;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.BudgetIdentifier;
import com.velos.services.model.FormIdentifier;
import com.velos.services.model.FormType;
import com.velos.services.model.GroupIdentifier;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyStatusIdentifier;
import com.velos.services.model.UserIdentifier;
import com.velos.services.patientdemographics.PatientDemographicsDAO;
import com.velos.services.patientstudy.PatientStudyDAO;
import com.velos.services.study.StudyStatusDAO;

/**
 * @author dylan
 *
 */
public class ObjectLocator {
	private static Logger logger = Logger.getLogger(ObjectLocator.class); 
	public static final String USER_TYPE_NON_SYSTEM = "N";
	public static final String USER_TYPE_SYSTEM = "S";
	
	public static final String USER_STATUS_ACTIVE="A";
	/**
	 * Utility method for obtaining the of an organization based
	 * on the OrganizationIdentifier class. This method can be used to map between
	 * a variety of identification models (implied by the fields in OrganizationIdentifier.)
	 * 
	 * This method will first look at the ExtneralId. If that exists, it will find the organization
	 * primary key based on the implementation of ObjectMapModel.
	 * 
	 * Next, it will look for at the site name form the OrganizationIdentifier.

	 * 
	 * If a user was found through means other than the object map, 
	 * we will create a map entry and return it sounds.
	 * @param orgIdentifier
	 * @param siteAgent
	 * @param sessionContext
	 * @param objectMapService
	 * @return
	 */
	public static Integer sitePKFromIdentifier(
							UserBean callingUser,
							OrganizationIdentifier orgIdentifier,
							SessionContext sessionContext, //TODO unused
							ObjectMapService objectMapService)
	throws
	MultipleObjectsFoundException{
		Integer sitePK = null;
		if (orgIdentifier.getOID() != null){
			//get the organization based on the ObjectMapService
			ObjectMap objectMap = 
				objectMapService.getObjectMapFromId(orgIdentifier.getOID());
			
			if(objectMap != null){
				//return siteAgent.getSiteDetails(objectMap.getTablePK());
				return objectMap.getTablePK();
			}
		}
		//Virendra: Fixed#6230
		String siteAltId = orgIdentifier.getSiteAltId();
		if (siteAltId != null && !siteAltId.equals("?") && !StringUtil.isEmpty(siteAltId)  ){
			//find the site by the site alt id
			sitePK = 
				SiteDao.getSitePKFromAltId(
						orgIdentifier.getSiteAltId(),
						EJBUtil.stringToNum(callingUser.getUserAccountId()));
		
		}
		
		if (sitePK == null && !StringUtil.isEmpty(orgIdentifier.getSiteName())){
			//find the site by the site name
			sitePK = 
				SiteDao.getSitePKBySiteName(
						orgIdentifier.getSiteName(), 
						EJBUtil.stringToNum(callingUser.getUserAccountId()));
		
		}
		
		if (sitePK != null && sitePK > 0){
			//create an external id for the caller to use if desired
			orgIdentifier.
				setOID(
						objectMapService.getOrCreateObjectMapFromPK(
									ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, 
									sitePK).getOID());
//			objectMapService.createObjectMap(
//					ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, 
//					sitePK).getSystemId());
				
		}
		return sitePK;
	}
	
	/**
	 * Utility method for obtaining the of a user based
	 * on the UserIdentifierModelModel class. This method can be used to map between
	 * a variety of identification models (implied by the fields in UserIentifierModel.)
	 * 
	 * This method will first look at the ExtneralId. If that exists, it will find the user's
	 * primary key based on the implementation of ObjectMapModel.
	 * 
	 * Next, it will look for at the user login name form the UserIdentifier.
	 * 
	 * If we still don't have a user, we will attempt to find a user
	 * based on the first name and last name.
	 * 
	 * Finally, if a user has not been found and createNonSystemUser is true, then
	 * we will create a non-system user with the fields in UserIdentifier
	 * 
	 * If a user was found through means other than the object map, 
	 * we will create a map entry and return it sounds.
	 * @param userIdentifier
	 * @param userAgent
	 * @param sessionContext
	 * @param objectMapService
	 * @return
	 * @throws MultipleObjectsFoundException
	 * @throws AuthenticationException
	 */
	public static UserBean userBeanFromIdentifier(
							UserBean callingUser,
							UserIdentifier userIdentifier, 
							UserAgentRObj userAgent,
							SessionContext sessionContext,
							ObjectMapService objectMapService)
	
	throws 
		MultipleObjectsFoundException{
		UserBean userBean = null;
        if (userIdentifier == null) return null;
		if (userIdentifier.getOID() != null){
			//get the user based on the ObjectMapService
			ObjectMap objectMap = 
				objectMapService.getObjectMapFromId(userIdentifier.getOID());
			
			if(objectMap != null){
			    UserBean userBeanFromOID = userAgent.getUserDetails(objectMap.getTablePK());
			    // If user from OID belongs to the right account, return it.
			    // Otherwise, keep going to find user using login or name.
				if (userBeanFromOID != null && 
				        userBeanFromOID.getUserAccountId().equals(callingUser.getUserAccountId())) {
				    return userBeanFromOID;
				}
			}
			
		}
        boolean goToNonSystem = false;
		if (!StringUtil.isEmpty(userIdentifier.getUserLoginName())){
			//find the user by the login name
			userBean = 
				userAgent.findUser(userIdentifier.getUserLoginName());
			
			//5692 DRM Check that the found userBean is in the same 
			//account as the calling user
			if (userBean != null && !userBean.getUserAccountId().equals(callingUser.getUserAccountId())){
				if (StringUtil.isEmpty(userIdentifier.getFirstName())
				        || StringUtil.isEmpty(userIdentifier.getLastName())) {
				    return null;
				}
				goToNonSystem = true; // UserBean found but for the wrong account.
				                      // Skip to the non-system user section.
			}
			
			//create an external id for the caller to use if desired
			//Virendra:#6273
			if(userBean != null && !goToNonSystem){//virendra
				userIdentifier.
					setOID(
							objectMapService.getOrCreateObjectMapFromPK(
									ObjectMapService.PERSISTENCE_UNIT_USER, 
									userBean.getUserId()).getOID());
//							objectMapService.createObjectMap(
//										ObjectMapService.PERSISTENCE_UNIT_STUDY, 
//										userBean.getUserId()).getSystemId());
			}
			
		}
		
		if (userBean != null && !goToNonSystem) return userBean;
		
		
		//Now try and find the user by first/last name (most likely a non-system user)
		if (!StringUtil.isEmpty(userIdentifier.getFirstName()) && 
				!StringUtil.isEmpty(userIdentifier.getLastName())){
			//find a non-system user based on first and last name
			
			//First, get the calling user's account id
			int userAcctId = 
				EJBUtil.stringToNum(callingUser.getUserAccountId());
			UserDao userDAO=new UserDao();
			//DRM
			//Bug 5665 - #SM-F-4- 'addStudyStatus' method throws an error while adding a new status
			Integer userPK  = userDAO.getUserPK(userIdentifier.getFirstName(), userIdentifier.getLastName());
			
			if (userAcctId == 0){
				return null;
			}
			//Raman:Bug#11473
			if (userDAO.getCRows() > 1){
				throw new MultipleObjectsFoundException("Found multiple users " +
						"with firstName " + userIdentifier.getFirstName() + " and " +
						"with lastName " + userIdentifier.getLastName());
			}

			//found one user, return it
			userBean = 
				userAgent.getUserDetails(userPK);
			
			// Once again make sure that the found non-system user belongs to the same account 
			if (userBean == null || !userBean.getUserAccountId().equals(callingUser.getUserAccountId())){
			    return null;
			}
			
			userIdentifier.
			setOID(
//					objectMapService.createObjectMap(
//								ObjectMapService.PERSISTENCE_UNIT_STUDY, 
//								userBean.getUserId()).getSystemId());
					objectMapService.getOrCreateObjectMapFromPK(
							ObjectMapService.PERSISTENCE_UNIT_STUDY, 
							userBean.getUserId()).getOID());
					
		}
		if (userBean == null){
			return null;
		}
		//fix for 5634 - #SM-F-1- 'createStudy' method creates a study, with studyAuthor 
		//NOT belonging to the eRes account, but exists in DB
		if (!callingUser.getUserAccountId().equals(userBean.getUserAccountId())){
			return null;
		}
		
		//5620 DRM 12/16/10 - Treat hidden users as though the don't exist.
		
		if ( 1 == (EJBUtil.stringToInteger(userBean.getUserHidden()).intValue())){
			return null;
		}
		
		return userBean;
		
		
	}
	
	public static Integer studyPKFromIdentifier(
			UserBean callingUser, 
			StudyIdentifier studyIdentifier, 
			ObjectMapService objectMapService){
	
		Integer studyPK = null;
		
		
		//We need to identify this study, using the provided studyIdentifer object.
		//First, we'll try by system Id. If that isn't provided, or it fails, we'll
		//try by study number
		if (studyIdentifier.getOID()!=null){
			//got a system id, try using it
			ObjectMap map = null;
			try {
				map = 
					objectMapService.getObjectMapFromId(studyIdentifier.getOID());
				if (map != null){
					studyPK = map.getTablePK();
				}
			} catch (MultipleObjectsFoundException e) {
				logger.debug("getStudy", e);
			}
		}
		if (studyPK == null && 
				studyIdentifier.getStudyNumber() != null){
			//either no system id, or finding the pk failed. Try by study number.
			studyPK = 
				StudyDao.getStudyPK(
						studyIdentifier.getStudyNumber(), 
						EJBUtil.stringToNum(callingUser.getUserAccountId()));
			if(studyPK != null)
			{
			//	objectMapService.createObjectMap(ObjectMapService.PERSISTENCE_UNIT_STUDY, studyPK);
				objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY, studyPK);
			}
		}
		
		return studyPK;
	}
	/**
	 * 
	 * @param callingUser
	 * @param patientIdentifier
	 * @param objectMapService
	 * @return
	 */
	public static Integer personPKFromPatientIdentifier(
			UserBean callingUser, 
			PatientIdentifier patientIdentifier, 
			ObjectMapService objectMapService) throws MultipleObjectsFoundException{
	
		Integer personPK = null;
		
		
		//We need to identify this patient, using the provided patintIdentifer object.
		
		if (patientIdentifier.getOID()!=null){
			//got a system id, try using it
			ObjectMap map = null;
			try {
				map = 
					objectMapService.getObjectMapFromId(patientIdentifier.getOID());
				if (map != null){
					personPK = map.getTablePK();
				}
			} catch (MultipleObjectsFoundException e) {
				logger.debug("PersonPK from PatientIdentifier", e);
			}
		}
		if (personPK == null && 
				patientIdentifier.getPatientId() != null && patientIdentifier.getOrganizationId() != null){
			//either no system id, or finding the pk failed. Try by patientCode.
			try {
				
				Integer organizationPK = sitePKFromIdentifier(callingUser, patientIdentifier.getOrganizationId(), null, objectMapService);
				
				if(organizationPK != null){
				personPK = 
					PatientDemographicsDAO.getPersonPKFromPersonCode(
							patientIdentifier.getPatientId(), 
							organizationPK);}
			} catch (OperationException e) {
				logger.debug("PersonPK from PatientIdentifier",e);
			} catch (MultipleObjectsFoundException e) {
				logger.debug("PersonPK from PatientIdentifier",e);
				throw e; 
			}
			if(personPK != null)
			{
			//	objectMapService.createObjectMap(ObjectMapService.PERSISTENCE_UNIT_STUDY, studyPK);
				objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PERSON, personPK);
			}
		}
		
		return personPK;
	}
	//virendra
	public static ArrayList<Integer> getPatProtPkFromPersonPk(Integer personPk, ObjectMapService objectMapService) throws OperationException{
		ArrayList<Integer> lstPatProtPk = new ArrayList<Integer>();
		if(personPk != null){
			lstPatProtPk = PatientStudyDAO.getPatProtPkFromPersonPk(personPk);
		}
		for(Integer patProtPk: lstPatProtPk ){
			objectMapService.getOrCreateObjectMapFromPK(objectMapService.PERSISTENCE_UNIT_PATPROT, patProtPk);
		}
		return lstPatProtPk;
		
	}
	//virendra
//	public static Integer studyStatusPKFromIdentifier(
//			UserBean callingUser, 
//			StudyStatusIdentifier studyStatusIdentifier, 
//			ObjectMapService objectMapService){
//	
//		Integer studyStatusPK = null;
//		
//		
//		//We need to identify this study, using the provided studyIdentifer object.
//		//First, we'll try by system Id. If that isn't provided, or it fails, we'll
//		//try by study number
//		if (studyStatusIdentifier.getOID()!=null){
//			//got a system id, try using it
//			ObjectMap map = null;
//			try {
//				map = 
//					objectMapService.getObjectMapFromId(studyStatusIdentifier.getOID());
//				if (map != null){
//					studyStatusPK = map.getTablePK();
//				}
//			} catch (MultipleObjectsFoundException e) {
//				logger.debug("getStudy", e);
//			}
//		}
//		if (studyStatusPK == null && 
//				studyStatusIdentifier.getOID() != null){
//			//either no system id, or finding the pk failed. Try by study number.
//			studyStatusPK = 
//				StudyStatusDao.get(
//						studyIdentifier.getStudyNumber(), 
//						EJBUtil.stringToNum(callingUser.getUserAccountId()));
//			if(studyPK != null)
//			{
//			//	objectMapService.createObjectMap(ObjectMapService.PERSISTENCE_UNIT_STUDY, studyPK);
//				objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY, studyPK);
//			}
//		}
//		
//		return studyPK;
//	}
	
	public static Integer budgetPKFromIdentifier(
			UserBean callingUser, 
			BudgetIdentifier budgetIdentifier, 
			ObjectMapService objectMapService) throws  MultipleObjectsFoundException{
	
		Integer budgetPK = null;
		
		if (budgetIdentifier.getOID()!=null && budgetIdentifier.getOID().length() > 0){
			//got a system id, try using it
			ObjectMap map = null;
		
			map = 
					objectMapService.getObjectMapFromId(budgetIdentifier.getOID());
				if (map != null){
					budgetPK = map.getTablePK();
				}
			
		}else if (budgetIdentifier.getBudgetName() != null && budgetIdentifier.getBudgetName().length() > 0){
			BudgetServiceDAO budgetDAO = new BudgetServiceDAO();
			budgetPK = 
				budgetDAO.locateBudgetPK(budgetIdentifier.getBudgetName(), budgetIdentifier.getBudgetVersion(), callingUser.getUserAccountId());
			if(budgetPK != null)
			{
				objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_CALENDAR_BUDGET, budgetPK);
			}
		}
		
		return budgetPK;
	}
	
	public static Integer formPKFromIdentifier(UserBean callingUser, FormIdentifier formIdentifier, ObjectMapService objectMapService, String formName,FormType  formType, StudyIdentifier studyIdentifier)
	throws MultipleObjectsFoundException, OperationException 
	{
		
		Integer formPK = null; 
		
		
		if(formIdentifier != null && formIdentifier.getOID() != null)
		{
			ObjectMap map = null; 			
			try
			{
				map = objectMapService.getObjectMapFromId(formIdentifier.getOID()); 
				if(map != null)
				{
					formPK = map.getTablePK(); 
				}
			} catch (MultipleObjectsFoundException e) {
				logger.debug("getForm", e);
			}
		}
		
		if(formPK == null 
				&& formName != null
				&& formName.length() > 0 
				&& formType != null )
		{
			
			Integer studyPK = null; 		
			
			if(formType.equals(FormType.STUDY) || formType.equals(FormType.STUDYPATIENT))
			{
				studyPK  = studyPKFromIdentifier(callingUser, studyIdentifier, objectMapService); 
				if(studyPK ==null || studyPK == 0) throw new OperationException("Study not found"); 
			}
			FormDesignDAO designDAO = new FormDesignDAO();
			formPK = designDAO.getFormPK(formName, formType , studyPK, callingUser.getUserAccountId()); 
			
		}		
		return formPK; 
		
	}
	// Navneet
	public static Integer groupPKFromGroupIdentifier(UserBean callingUser,GroupIdentifier groupIdentifier,ObjectMapService objectMapService) throws MultipleObjectsFoundException
	{
		Integer groupPK = null;
		if(groupIdentifier != null && groupIdentifier.getOID() != null)
		{
			ObjectMap map = null; 
			try
			{
				map = objectMapService.getObjectMapFromId(groupIdentifier.getOID());
				if(map != null)
				{
					groupPK = map.getTablePK();
				}
						
			}
			catch (MultipleObjectsFoundException e) {
				logger.debug("getGroup", e);
			}
			
		}	
		else if(groupIdentifier.getGroupName() != null && groupIdentifier.getGroupName().length()>0)
		{
			CodeDao codeDao = new CodeDao();
			codeDao.getAccountGroups((StringUtil.stringToNum(callingUser.getUserAccountId())));
			codeDao.getCDesc();
			for(int counter = 0; counter<codeDao.getCDesc().size();counter++)
			{
				if(codeDao.getCDesc().get(counter).toString().equals(groupIdentifier.getGroupName()))
				{
					groupPK = (Integer) codeDao.getCId().get(counter);
					
				}	
			}	
						
		}	
		return groupPK;
		
	}	
	
	
	
	
	
}
