/**
 * Created On Dec 14, 2011
 */
package com.velos.services.formresponse;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import javax.annotation.Resource;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.FormLibDao;
import com.velos.eres.business.common.LinkedFormsDao;
import com.velos.eres.business.common.SaveFormDao;
import com.velos.eres.business.patProt.impl.PatProtBean;
import com.velos.eres.business.patStudyStat.impl.PatStudyStatBean;
import com.velos.eres.business.saveForm.SaveFormStateKeeper;
import com.velos.eres.service.fieldLibAgent.FieldLibAgentRObj;
import com.velos.eres.service.formFieldAgent.FormFieldAgentRObj;
import com.velos.eres.service.formLibAgent.FormLibAgentRObj;
import com.velos.eres.service.grpRightsAgent.GrpRightsAgentRObj;
import com.velos.eres.service.linkedFormsAgent.LinkedFormsAgentRObj;
import com.velos.eres.service.patProtAgent.PatProtAgentRObj;
import com.velos.eres.service.patStudyStatAgent.PatStudyStatAgentRObj;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.userSiteAgent.UserSiteAgentRObj;
import com.velos.eres.service.util.Configuration;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.EJBUtil;
import com.velos.services.AbstractService;
import com.velos.services.AuthorizationException;
import com.velos.services.CRUDAction;
import com.velos.services.CodeNotFoundException;
import com.velos.services.CompletedAction;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.OperationRolledBackException;
import com.velos.services.ResponseHolder;
import com.velos.services.authorization.GroupAuthModule;
import com.velos.services.authorization.TeamAuthModule;
import com.velos.services.calendar.StudyCalendarDAO;
import com.velos.services.form.FormDesignDAO;
import com.velos.services.map.MultipleObjectsFoundException;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.CalendarIdentifier;
import com.velos.services.model.Code;
import com.velos.services.model.DateFieldValidations;
import com.velos.services.model.EventIdentifier;
import com.velos.services.model.FieldIdentifier;
import com.velos.services.model.FormField;
import com.velos.services.model.FormField.FieldCharacteristics;
import com.velos.services.model.FormField.FieldType;
import com.velos.services.model.FormFieldResponse;
import com.velos.services.model.FormFieldResponses;
import com.velos.services.model.FormResponse;
import com.velos.services.model.FormSection;
import com.velos.services.model.FormSections;
import com.velos.services.model.LinkedFormDesign;
import com.velos.services.model.NumberFieldValidations;
import com.velos.services.model.NumberRange;
import com.velos.services.model.ScheduleEventIdentifier;
import com.velos.services.model.StudyFormDesign;
import com.velos.services.model.StudyPatientFormResponseIdentifier;
import com.velos.services.model.LinkedFormDesign.DisplayType;
import com.velos.services.model.LinkedFormDesign.EntrySettings;
import com.velos.services.model.NumberRange.LogicalOperator;
import com.velos.services.model.NumberRange.Operator;
import com.velos.services.model.MultipleChoice;
import com.velos.services.model.StudyFormResponse;
import com.velos.services.model.StudyFormResponseIdentifier;
import com.velos.services.model.StudyPatientFormDesign;
import com.velos.services.model.StudyPatientFormResponse;
import com.velos.services.model.StudyPatientScheduleFormDesign;
import com.velos.services.model.StudyPatientScheduleFormResponse;
import com.velos.services.model.TextFieldValidations;
import com.velos.services.model.VisitIdentifier;
import com.velos.services.util.CodeCache;
import com.velos.services.util.ObjectLocator;

/**
 * @author Kanwaldeep
 *
 */
@Stateless
@Remote(FormResponseService.class)
public class FormResponseServiceImpl extends AbstractService implements FormResponseService {
	
	@EJB
	FormLibAgentRObj formLibAgent; 
	
	@EJB
	private ObjectMapService objectMapService;
	
	@EJB
	private UserAgentRObj userAgent;
	
	@EJB
	private UserSiteAgentRObj userSiteAgent; 
	
	@EJB
	GrpRightsAgentRObj groupRightsAgent;
	
	@EJB
	FormFieldAgentRObj formFieldAgent; 
	
	@EJB
	FieldLibAgentRObj fieldLibAgent; 
	
	@EJB
	LinkedFormsAgentRObj linkedFormAgent;
	
	@EJB
	private PatStudyStatAgentRObj patStudyStatAgent; 
	
	@Resource 
	private SessionContext sessionContext;	
	@EJB
	private PatProtAgentRObj patProtAgent;
	@AroundInvoke
	public Object myInterceptor(InvocationContext ctx) throws Exception {
		response = new ResponseHolder();
		callingUser = 
			getLoggedInUser(
					sessionContext, 
					userAgent);
		return ctx.proceed();

	}
	
	private static Logger logger = Logger.getLogger(FormResponseServiceImpl.class); 


	/* (non-Javadoc)
	 * @see com.velos.services.formresponse.FormResponseService#createStudyFormResponse(com.velos.services.model.StudyFormResponse)
	 */
	public ResponseHolder createStudyFormResponse(
			StudyFormResponse studyFormResponse)
			throws OperationRolledBackException, OperationException {
		try{
			Integer formPK = null; 
			

			GroupAuthModule authModule = new GroupAuthModule(callingUser, groupRightsAgent); 
			Integer protocolManagementAccess = authModule.getAppManageProtocolsPrivileges(); 
			
			if(!GroupAuthModule.hasViewPermission(protocolManagementAccess))
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User is not authorized to createStudyFormResponse")); 
				throw new OperationException(); 
			}
			
			
			if(studyFormResponse.getFormId() != null && studyFormResponse.getFormId().getOID() != null && studyFormResponse.getFormId().getOID().length() > 0)
			{
				try {
					formPK = ObjectLocator.formPKFromIdentifier(callingUser, studyFormResponse.getFormId(), objectMapService, null, null, studyFormResponse.getStudyIdentifier());
					if(formPK == null)
					{
						addIssue(new Issue(IssueTypes.FORM_NOT_FOUND, "Form not found")); 
						throw new OperationException(); 
					}
				} catch (MultipleObjectsFoundException e) {
				
					e.printStackTrace();
				} 
			}else
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid FormId is required to create StudyFormResponse")); 
				throw new OperationException(); 
			}
			
			Integer studyPK = null; 
			
			if(studyFormResponse.getStudyIdentifier() != null && 
					((studyFormResponse.getStudyIdentifier().getOID() != null && studyFormResponse.getStudyIdentifier().getOID().length() > 0) ||
							(studyFormResponse.getStudyIdentifier().getStudyNumber() != null && studyFormResponse.getStudyIdentifier().getStudyNumber().length() > 0)))
			{
				studyPK = ObjectLocator.studyPKFromIdentifier(callingUser, studyFormResponse.getStudyIdentifier(), objectMapService); 
				if(studyPK == null || studyPK == 0)
				{
					addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found")); 
					throw new OperationException(); 
				}
				
				
			}else
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid StudyIdentifier is required to create Study Form Response")); 
				throw new OperationException(); 
			}
						
			//Study Level access rights
			
			TeamAuthModule teamAuth = new TeamAuthModule(callingUser.getUserId(), studyPK); 
			Integer studyFormAccess = teamAuth.getFormsStudyAccessPrivileges();
			Integer allStudyGroupRights = authModule.getFormsAllStudyFormsPrivileges();
			
			FormDesignDAO formDesignDao = new FormDesignDAO();
			StudyFormDesign studyFormDesign = new StudyFormDesign(); 
			formDesignDao.getStudyPatientFormDesign(formPK, callingUser, objectMapService, studyFormDesign, null, false); 
			
			//return error if form status is anything but 'Active'
			if(!(studyFormDesign.getFormStatus().getCode().equals("A"))){
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Form is not active, response cannot be created")); 
				throw new OperationException(); 
			}
			
			if(!teamAuth.hasAccessToStudy() || (!TeamAuthModule.hasNewPermission(studyFormAccess) && studyFormDesign.getDisplayType().equals(DisplayType.STUDY))
					|| (!GroupAuthModule.hasNewPermission(allStudyGroupRights) && studyFormDesign.getDisplayType().equals(DisplayType.ALL_STUDIES)))
			{
				addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User is not authorized to create Form Response for study")); 
				throw new OperationException(); 
			}
			//check if user has access to Form( This will cover group level All Study Forms access and studyLevel form access
            //Bug Fix : 9989 : Tarandeep Singh Bali
			LinkedFormsDao linkedformdao = linkedFormAgent.getStudyForms(EJBUtil.stringToInteger(callingUser.getUserAccountId()), studyPK, callingUser.getUserId(), EJBUtil.stringToInteger(callingUser.getUserSiteId()),allStudyGroupRights,studyFormAccess,false,"",""); 
			
			if(!linkedformdao.getFormId().contains(formPK))
			{
				addIssue(new Issue(IssueTypes.FORM_ACCESS_AUTHORIZATION, "User is not authorized to create Form Response for this form")); 
				throw new OperationException(); 
			}			
		
			
			FormResponseServiceDAO responseDao = new FormResponseServiceDAO(); 
			//Single/Multiple entry check
			if((!studyFormDesign.getEntrySetting().equals(EntrySettings.MULTIPLE)) && (responseDao.getStudyFormResponseCount(formPK, studyPK) > 0))
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Form is Single Entry form and a response already exists")); 
				throw new OperationException(); 
			}
			
			Map<String, String> formFieldValueMap = new HashMap<String, String>(); 

			for(FormFieldResponse response: studyFormResponse.getFormFieldResponses().getField())
			{
				if(response.getFieldIdentifier() == null || response.getFieldIdentifier().getOID() == null || response.getFieldIdentifier().getOID().length() == 0)
				{
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "FieldIdentifier with OID is required")); 
					throw new OperationException(); 
				}
				formFieldValueMap.put(response.getFieldIdentifier().getOID(), response.getValue()); 			
			}

			
				SaveFormStateKeeper stateKeeper = new SaveFormStateKeeper(); 
				fillFormDetailsWithValidations(studyFormDesign, stateKeeper, formFieldValueMap); 	
				stateKeeper.setAccountId(callingUser.getUserAccountId()); 
				stateKeeper.setCreator(EJBUtil.integerToString(callingUser.getUserId()));
				stateKeeper.setFormId(EJBUtil.integerToString(formPK)); 
				stateKeeper.setIpAdd(IP_ADDRESS_FIELD_VALUE)   ;
				stateKeeper.setFormFillDate(DateUtil.dateToString(studyFormResponse.getFormFillDt())); 
				stateKeeper.setDisplayType(DisplayType.STUDY.toString()); 
				stateKeeper.setStudyId(EJBUtil.integerToString(ObjectLocator.studyPKFromIdentifier(callingUser, studyFormResponse.getStudyIdentifier(), objectMapService))); 
				String formStatus = null; 
				if(studyFormResponse.getFormStatus() != null )
				{
					try
					{
						formStatus = dereferenceCodeStr(studyFormResponse.getFormStatus(), CodeCache.CODE_TYPE_FILLED_FORM_STATUS, callingUser); 
					}catch(CodeNotFoundException ce)
					{
						addIssue(new Issue(IssueTypes.CODE_NOT_FOUND, "Form Status code not found")); 
					}
				}
				
				stateKeeper.setFormCompleted(formStatus);
				
					
				if(response.getIssues().getIssue().size() > 0)
				{
					throw new OperationException(); 
				}
				
				SaveFormDao saveFormDao  =  new SaveFormDao(); 
				int result = saveFormDao.insertFilledFormBySP(stateKeeper); 
				if(result > 0)
				{
					StudyFormResponseIdentifier studyFormIdentifier = new StudyFormResponseIdentifier(); 
					ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY_FORM_RESPONSE, result); 
					studyFormIdentifier.setOID(map.getOID()); 
				
					response.addObjectCreatedAction(studyFormIdentifier);
				}else
				{
					addIssue(new Issue(IssueTypes.ERROR_CREATING_FORM_RESPONSE, "Error occured while creating formResponse"));
					throw new OperationException();
				}
				return response;
			

		}catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("FormResponseServiceImpl create", e);
			throw new OperationRolledBackException(response.getIssues());
	
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("FormResponseServiceImpl create", t);
			throw new OperationRolledBackException(response.getIssues());

		}
	}

	/* (non-Javadoc)
	 * @see com.velos.services.formresponse.FormResponseService#removeStudyFormResponse(com.velos.services.model.AccountFormResponseIdentifier)
	 */
	public ResponseHolder removeStudyFormResponse(
			StudyFormResponseIdentifier formResponseIdentifier)
			throws OperationRolledBackException, OperationException {
		FormResponseServiceDAO formResponseServiceDao = new FormResponseServiceDAO(); 
		Integer filledFormPK= null; 
		try
		{
			GroupAuthModule authModule = new GroupAuthModule(callingUser, groupRightsAgent); 
			Integer manageProtocolAccess = authModule.getAppManageProtocolsPrivileges(); 

			if(!GroupAuthModule.hasViewPermission(manageProtocolAccess))
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User is not authorized to delete Study Patient forms")); 
				throw new AuthorizationException(); 
			}

			if(formResponseIdentifier != null && formResponseIdentifier.getOID() != null && formResponseIdentifier.getOID().length() > 0)
			{
				ObjectMap objectMap = objectMapService.getObjectMapFromId(formResponseIdentifier.getOID()); 
				if(objectMap == null)
				{
					addIssue(new Issue(IssueTypes.FORM_RESPONSE_NOT_FOUND, "Form Response not found for OID " + formResponseIdentifier.getOID())); 
					throw new OperationException(); 
				}
				if(!(objectMap.getTableName().equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_STUDY_FORM_RESPONSE)))
				{
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Please send valid StudyFormResponseIdentifier")); 
					throw new OperationException(); 
				}
				filledFormPK = objectMap.getTablePK(); 
				if(filledFormPK == null || filledFormPK == 0)
				{
					addIssue(new Issue(IssueTypes.FORM_RESPONSE_NOT_FOUND, "Form Response not found for OID " + formResponseIdentifier.getOID())); 
					throw new OperationException(); 
				}

				if(formResponseServiceDao.isStudyFormResponseDeleted(filledFormPK)){
					addIssue(new Issue(IssueTypes.FORM_RESPONSE_DOES_NOT_EXIT, "Form Response doesn't exist")); 
					throw new OperationException(); 
				}
				
				formResponseServiceDao.populateFormIDForStudyFilledFormID(filledFormPK); 
				Integer formLibPK = formResponseServiceDao.getFormLibIDforFilledForm(); 
				Integer studyPK =  formResponseServiceDao.getStudyPKForFilledForm(); 
				
				TeamAuthModule teamAuth = new TeamAuthModule(callingUser.getUserId(), studyPK); 
				Integer studyFormAccess = teamAuth.getFormsStudyAccessPrivileges();
				Integer allStudyGroupRights = authModule.getFormsAllStudyFormsPrivileges(); 
				
				FormDesignDAO formDesignDao = new FormDesignDAO(); 
				StudyFormDesign studyFormDesign = new StudyFormDesign(); 
				studyFormDesign = formDesignDao.getStudyPatientFormDesign(formLibPK, callingUser, objectMapService, studyFormDesign, studyPK, false); 

				if( !teamAuth.hasAccessToStudy() || (!TeamAuthModule.hasEditPermission(studyFormAccess) && studyFormDesign.getDisplayType().equals(DisplayType.STUDY)) ||
						(!GroupAuthModule.hasEditPermission(allStudyGroupRights) && studyFormDesign.getDisplayType().equals(DisplayType.ALL_STUDIES)))
				{
					addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User is not authorized to delete Form Response for study")); 
					throw new OperationException(); 
				}
				//Added the check to validate of the user belongs to the Organization which is mentioned in the filter for the Study Form: Tarandeep Singh Bali
				LinkedFormsDao linkedformdao = linkedFormAgent.getStudyForms(EJBUtil.stringToInteger(callingUser.getUserAccountId()), studyPK, callingUser.getUserId(), EJBUtil.stringToInteger(callingUser.getUserSiteId()),allStudyGroupRights,studyFormAccess,false,"",""); 
				
				if(!linkedformdao.getFormId().contains(formLibPK))
				{
					addIssue(new Issue(IssueTypes.FORM_ACCESS_AUTHORIZATION, "User is not authorized to delete Form Response for this form")); 
					throw new OperationException(); 
				}
				
				if(!(studyFormDesign.getFormStatus().getCode().equals("A"))){
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Form is not active, response cannot be deleted")); 
					throw new OperationException(); 
					
				}
			
				linkedFormAgent.deleteFilledFormResp(EJBUtil.integerToString(filledFormPK),studyFormDesign.getDisplayType().toString() , EJBUtil.integerToString(callingUser.getUserId()), IP_ADDRESS_FIELD_VALUE);
				
				
			}else
			{
				addIssue(new Issue(IssueTypes.FORM_RESPONSE_NOT_FOUND, "Valid StudyPatientFormResponseIdentifier is required to get form response")); 
				throw new OperationException(); 
			}		

			response.addAction(new CompletedAction(formResponseIdentifier, CRUDAction.REMOVE));
		}catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("FormResponseServiceImpl removeStudyFormResponse", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("FormResponseServiceImpl removeStudyFormResponse", t);
			throw new OperationException(t);
		}
		
		return response;
	}

	/* (non-Javadoc)
	 * @see com.velos.services.formresponse.FormResponseService#getStudyFormResponse(com.velos.services.model.AccountFormResponseIdentifier)
	 */
	public StudyFormResponse getStudyFormResponse(
			StudyFormResponseIdentifier formResponseIdentifier)
			throws OperationException {
		StudyFormResponse studyFormResponse = new StudyFormResponse();
		
		try{
			Integer studyFormID = 0; 
			GroupAuthModule authModule = new GroupAuthModule(callingUser, groupRightsAgent); 
			Integer protocolManagementAccess = authModule.getAppManageProtocolsPrivileges(); 
			
			if(!GroupAuthModule.hasViewPermission(protocolManagementAccess))
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User is not authorized to getStudyFormResponse")); 
				throw new OperationException(); 
			}
			
		
			if(formResponseIdentifier != null && formResponseIdentifier.getOID() != null && formResponseIdentifier.getOID().length() > 0)
			{
				ObjectMap objectMap = objectMapService.getObjectMapFromId(formResponseIdentifier.getOID()); 
				
				if(objectMap == null || !(objectMap.getTableName().equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_STUDY_FORM_RESPONSE)))
				{
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Please send valid StudyFormResponseIdentifier"));
					throw new OperationException();
				}
	//			studyPatientFormID = ((ObjectMap) objectMapService.getObjectMapFromId(formResponseIdentifier.getOID())).getTablePK(); 
				studyFormID = objectMap.getTablePK(); 
				
				if(studyFormID == null || studyFormID == 0)
				{
					addIssue(new Issue(IssueTypes.FORM_RESPONSE_NOT_FOUND, "StudyFormResponse not found")); 
					throw new OperationException(); 
				}
				
				
				FormResponseServiceDAO formResponseServiceDao = new FormResponseServiceDAO(); 
				formResponseServiceDao.populateFormIDForStudyFilledFormID(studyFormID); 
				
				Integer formLibPK = formResponseServiceDao.getFormLibIDforFilledForm(); 
				Integer studyID = formResponseServiceDao.getStudyPKForFilledForm(); 
				
				TeamAuthModule teamAuth = new TeamAuthModule(callingUser.getUserId(), studyID); 
				Integer studyFormAccess = teamAuth.getFormsStudyAccessPrivileges();
				Integer allStudyGroupRights = authModule.getFormsAllStudyFormsPrivileges(); 
				
				FormDesignDAO formDesignDao = new FormDesignDAO(); 
				StudyFormDesign studyFormDesign = new StudyFormDesign(); 
				studyFormDesign = formDesignDao.getStudyPatientFormDesign(formLibPK, callingUser, objectMapService, studyFormDesign, studyID, false); 

				if(!teamAuth.hasAccessToStudy() || (!TeamAuthModule.hasViewPermission(studyFormAccess) && studyFormDesign.getDisplayType().equals(DisplayType.STUDY)) ||
						(!GroupAuthModule.hasViewPermission(allStudyGroupRights) && studyFormDesign.getDisplayType().equals(DisplayType.ALL_STUDIES)))
				{
					addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User is not authorized to View Form Response for study")); 
					throw new OperationException(); 
				}
				
				//check if user has access to Form( This will cover group level All Study Forms access and studyLevel form access
				//Bug Fix : 9915 : Tarandeep Singh Bali
				LinkedFormsDao linkedformdao = linkedFormAgent.getStudyForms(EJBUtil.stringToInteger(callingUser.getUserAccountId()), studyID, callingUser.getUserId(), EJBUtil.stringToInteger(callingUser.getUserSiteId()),allStudyGroupRights,studyFormAccess,false,"","");
				
				if(!linkedformdao.getFormId().contains(formLibPK))
				{
					addIssue(new Issue(IssueTypes.FORM_ACCESS_AUTHORIZATION, "User is not authorized to View Form Response for this form")); 
					throw new OperationException(); 
				}		
			
				
				if(!(studyFormDesign.getFormStatus().getCode().equals("A") || studyFormDesign.getFormStatus().getCode().equals("L")))
		        {
		        	addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Form is not active"));
		        	throw new OperationException(); 
		        }
				try{
					studyFormResponse = formResponseServiceDao.getStudyFormResponse(studyFormID, callingUser, objectMapService, studyFormDesign); 
				}catch(OperationException oe)
				{
					addIssue(new Issue(IssueTypes.FORM_RESPONSE_DOES_NOT_EXIT, "Form Response does'nt exit")); 
					throw new OperationException(); 
				}
				
				
			}else
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "StudyFormResponseIdentifier with valid OID is required to retrieve StudyFormResponse")); 
				throw new OperationException(); 
			}
			
			
		}catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("FormResponseServiceImpl create", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("FormResponseServiceImpl create", t);
			throw new OperationException(t);
		}
	
		return studyFormResponse;
	}
	
	
	public StudyPatientFormResponse getStudyPatientFormResponse(
			StudyPatientFormResponseIdentifier formResponseIdentifier)
			throws OperationException{
		
		StudyPatientFormResponse formResponse = new StudyPatientFormResponse();
		FormResponseServiceDAO formResponseServicedao = new FormResponseServiceDAO(); 
		Integer studyPatientFormID; 
		try{
			
			
			
			GroupAuthModule authModule = new GroupAuthModule(callingUser, groupRightsAgent); 
			Integer patientManagmentAccess = authModule.getAppManagePatientsPrivileges(); 

			if(!GroupAuthModule.hasEditPermission(patientManagmentAccess))
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User is not authorized to view Study Patient forms")); 
				throw new AuthorizationException(); 
			}
			
			if(formResponseIdentifier != null && formResponseIdentifier.getOID() != null && formResponseIdentifier.getOID().length() > 0)
			{
				ObjectMap objectMap = objectMapService.getObjectMapFromId(formResponseIdentifier.getOID()); 
				
				if(objectMap == null || !(objectMap.getTableName().equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_PATIENT_FORM_RESPONSE)))
				{
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Please send valid StudyPatientFormResponseIdentifier"));
					throw new OperationException();
				}
	//			studyPatientFormID = ((ObjectMap) objectMapService.getObjectMapFromId(formResponseIdentifier.getOID())).getTablePK(); 
				studyPatientFormID = objectMap.getTablePK(); 
				if(studyPatientFormID == null || studyPatientFormID == 0)
				{
					addIssue(new Issue(IssueTypes.FORM_RESPONSE_NOT_FOUND, "Form Response not found for OID " + formResponseIdentifier.getOID())); 
					throw new OperationException(); 
				}
				
				formResponseServicedao.populateStudyPatFormID(studyPatientFormID); 
				
				
				Integer patProtID = formResponseServicedao.getPatProtIDforFilledForm(); 
				Integer patientID = formResponseServicedao.getPerIDForFilledForm(); 
				Integer studyID = formResponseServicedao.getStudyPKForFilledForm(); 
				Integer formLibPK = formResponseServicedao.getFormLibIDforFilledForm(); 

		

				if(patProtID == null || patProtID == 0)
				{
					addIssue(new Issue(IssueTypes.STUDY_PATIENT_NOT_FOUND, "PatProt ID not found")); 
					throw new OperationException(); 
				}
						
				//check patient level access
				Integer userMaxStudyPatientRight = userSiteAgent.getMaxRightForStudyPatient(callingUser.getUserId(), patientID, studyID); 
				if(userMaxStudyPatientRight == 0)
				{
					addIssue(new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION, "User is not authorized to view Form Response for patient")); 
					throw new OperationException();
				}

				//check Study level access

				TeamAuthModule teamAuth = new TeamAuthModule(callingUser.getUserId(), studyID); 
				Integer studyPatientManagement = teamAuth.getPatientManagePrivileges(); 
				Integer studyFormAccess = teamAuth.getFormsStudyAccessPrivileges(); 

				if(!TeamAuthModule.hasNewPermission(studyPatientManagement))
				{
					addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User is not authorized to view Form Response for study")); 
					throw new OperationException(); 
				}

				if(!TeamAuthModule.hasViewPermission(studyFormAccess))
				{
					addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User is not authorized to view Form Response for study")); 
					throw new OperationException(); 
				}

				//check form level access
			    FormDesignDAO  formDesignDAO = new FormDesignDAO(); 
			    StudyPatientFormDesign studyPatientFormDesign  = new StudyPatientFormDesign(); 
		        try {
					formDesignDAO.getStudyPatientFormDesign(formLibPK, callingUser, objectMapService, studyPatientFormDesign, studyID, false);
				} catch (OperationException e) {
					
					e.printStackTrace();
				} 
		        
		        if(!(studyPatientFormDesign.getFormStatus().getCode().equals("A") || studyPatientFormDesign.getFormStatus().getCode().equals("L")))
		        {
		        	addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Form is not active"));
		        	throw new OperationException(); 
		        }
				try{
				formResponse = formResponseServicedao.getStudyPatientFormResponse(studyPatientFormID, callingUser, objectMapService, studyPatientFormDesign); 
				}catch(OperationException oe)
				{
					addIssue(new Issue(IssueTypes.FORM_RESPONSE_DOES_NOT_EXIT, "Form Response does'nt exit")); 
					throw new OperationException(); 
				}
				
			}else
			{
				addIssue(new Issue(IssueTypes.FORM_RESPONSE_NOT_FOUND, "Valid StudyPatientFormResponseIdentifier is required to get form response")); 
				throw new OperationException(); 
			}
				
			

			

		}catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("FormResponseServiceImpl create", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("FormResponseServiceImpl create", t);
			throw new OperationException(t);
		}
	
		return formResponse; 
	}
	
	
	
	/* (non-Javadoc)
	 * @see com.velos.services.formresponse.FormResponseService#createStudyPatientFormResponse(com.velos.services.model.StudyPatientFormResponse)
	 */
	public ResponseHolder createStudyPatientFormResponse(StudyPatientFormResponse studyPatientFormResponse) throws OperationException {
		
		try{
		// Group rights
		validate(studyPatientFormResponse); 
		GroupAuthModule authModule = new GroupAuthModule(callingUser, groupRightsAgent); 
		Integer patientManagmentAccess = authModule.getAppManagePatientsPrivileges(); 
		
		if(!GroupAuthModule.hasEditPermission(patientManagmentAccess))
		{
			addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User is not authorized to fill Study Patient forms")); 
			throw new AuthorizationException(); 
		}
	
		Integer studyID; 
	    Integer patientID;
	    Integer patProtID;
	    Integer formID = null;
			
		if(studyPatientFormResponse.getFormId() != null && studyPatientFormResponse.getFormId().getOID() != null)
		{
			
			try {
				formID = ObjectLocator.formPKFromIdentifier(callingUser, studyPatientFormResponse.getFormId(), objectMapService, null, null, studyPatientFormResponse.getStudyIdentifier());
				if(formID == null)
				{
					addIssue(new Issue(IssueTypes.FORM_NOT_FOUND, "Form not found")); 
					throw new OperationException(); 
				}
			} catch (MultipleObjectsFoundException e) {
			
				e.printStackTrace();
			} 

		
		}else
		{
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "FormId is required to create Form Response"));
			throw new OperationException(); 
		}
		
		if(studyPatientFormResponse.getPatientProtocolIdentifier() != null && studyPatientFormResponse.getPatientProtocolIdentifier().getOID() != null && studyPatientFormResponse.getPatientProtocolIdentifier().getOID().length() != 0)
		{
			 	patProtID = objectMapService.getObjectPkFromOID(studyPatientFormResponse.getPatientProtocolIdentifier().getOID()); 
				
				if(patProtID == null || patProtID == 0)
				{
					addIssue(new Issue(IssueTypes.STUDY_PATIENT_NOT_FOUND, "PatProt ID not found")); 
					throw new OperationException(); 
				}
				PatProtBean patProtBean = patProtAgent.getPatProtDetails(patProtID); 
				studyID = EJBUtil.stringToInteger(patProtBean.getPatStudyId()); 
				patientID = EJBUtil.stringToInteger(patProtBean.getPatProtPersonId()); 
				
				//check current 
				if(EJBUtil.stringToInteger(patProtBean.getPatProtStat()) == 0)
				{
					addIssue(new Issue(IssueTypes.PATIENT_PROTOCOL_INACTIVE , "Given PatientProtocolIdentifier is inactive")); 
					throw new OperationException(); 
				}
				
				
			
		}else if(studyPatientFormResponse.getPatientIdentifier() != null && (studyPatientFormResponse.getPatientIdentifier().getOID() != null || (studyPatientFormResponse.getPatientIdentifier().getPatientId() != null && studyPatientFormResponse.getPatientIdentifier().getOrganizationId()!= null))
				&& studyPatientFormResponse.getStudyIdentifier() != null && (studyPatientFormResponse.getStudyIdentifier().getOID() != null || studyPatientFormResponse.getStudyIdentifier().getStudyNumber() != null))
		{
			studyID = ObjectLocator.studyPKFromIdentifier(callingUser, studyPatientFormResponse.getStudyIdentifier(), objectMapService); 
			
			if(studyID == null || studyID == 0)
			{
				addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found")); 
				throw new OperationException(); 
			}
			try{
				patientID = ObjectLocator.personPKFromPatientIdentifier(callingUser, studyPatientFormResponse.getPatientIdentifier(), objectMapService); 
			} catch (MultipleObjectsFoundException e) {
				addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND, 
				"Multiple Patients found")); 
				throw new OperationException(); 
			}
			
			if(patientID == null || patientID == 0)
			{
				addIssue(new Issue(IssueTypes.PATIENT_NOT_FOUND, "Patient not found")); 
				throw new OperationException();
			}
			
			//Now check if patient is linked to Study	
			
			PatProtBean  patProtBean = patProtAgent.findCurrentPatProtDetails(studyID, patientID); 
			
			if(patProtBean.getPatProtId() == 0)
			{
				addIssue(new Issue(IssueTypes.PATIENT_NOT_ONSTUDY, "Given patient is not linked to study provided")); 
				throw new OperationException(); 
			}
			patProtID = patProtBean.getPatProtId(); 
			
			
			
			
		}else
		{
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "PatientProtocolIdentifier or PatientIdentifier and StudyIdentifier is required to create StudyPatient FormResponse")); 
			throw new OperationException(); 
		}
		
		
		PatStudyStatBean patStatBean = patStudyStatAgent.getLatestPatStudyStat(studyID, patientID); 
		
		
		CodeCache codeCache = CodeCache.getInstance(); 
		
		Code patStudyStatus = codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_PATPROT_STATUS, patStatBean.getPatStudyStat(), EJBUtil.stringToInteger(callingUser.getUserAccountId()));
		
		if(patStudyStatus.getCode().equals("lockdown"))
		{
			addIssue(new Issue(IssueTypes.PATIENT_STATUS_LOCKDOWN, "Patient Study status is lockdown"));
			throw new OperationException(); 
		}
		
		//check patient level access
//		Integer userPatientFacilityRight = userSiteAgent.getUserPatientFacilityRight(callingUser.getUserId(), patientID); 
//		if(userPatientFacilityRight < 4)
//		{
//			addIssue(new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION, "User is not authorized to create Form Response for patient")); 
//			throw new OperationException(); 
//		}
		
		Integer userMaxStudyPatientRight = userSiteAgent.getMaxRightForStudyPatient(callingUser.getUserId(), patientID, studyID); 
		if(userMaxStudyPatientRight == 0)
		{
			addIssue(new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION, "User is not authorized to create Form Response for patient")); 
			throw new OperationException();
		}
		
		//check Study level access
	
		TeamAuthModule teamAuth = new TeamAuthModule(callingUser.getUserId(), studyID); 
		Integer studyPatientManagement = teamAuth.getPatientManagePrivileges(); 
		Integer studyFormAccess = teamAuth.getFormsStudyAccessPrivileges(); 
		
		if(!TeamAuthModule.hasNewPermission(studyPatientManagement))
		{
			addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User is not authorized to create Form Response for study")); 
			throw new OperationException(); 
		}
		
		if(!TeamAuthModule.hasNewPermission(studyFormAccess))
		{
			addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User is not authorized to create Form Response for study")); 
			throw new OperationException(); 
		}
		
		//check form level access
		FormDesignDAO formDesignDao = new FormDesignDAO();
		
		if(!formDesignDao.getStudyPatientFormListUserHasAccess(callingUser, studyID, true).contains(formID))
		{
			addIssue(new Issue(IssueTypes.FORM_ACCESS_AUTHORIZATION, "User is not authorized to view this Form")); 
			throw new OperationException(); 
		}
		StudyPatientFormDesign studyPatientFormDesign  = new StudyPatientFormDesign(); 
		formDesignDao.getStudyPatientFormDesign(formID, callingUser, objectMapService, studyPatientFormDesign, studyID, false); 
		
		
		//return error if form status is anything but 'Active'
		if(!(studyPatientFormDesign.getFormStatus().getCode().equals("A"))){
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Form is not active, response cannot be created")); 
			throw new OperationException(); 
			
		}
		
		FormResponseServiceDAO responseDao = new FormResponseServiceDAO(); 
		//Single/Multiple entry check
		if((!studyPatientFormDesign.getEntrySetting().equals(EntrySettings.MULTIPLE)) && (responseDao.getFormResponseCount(formID, patientID, null) > 0))
		{
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Form is Single Entry form and a response already exists")); 
		}
			
		//Field level checks (mandatory and dataType validations) 
		Map<String, String> formFieldValueMap = new HashMap<String, String>(); 

		for(FormFieldResponse response: studyPatientFormResponse.getFormFieldResponses().getField())
		{
			if(response.getFieldIdentifier() == null || response.getFieldIdentifier().getOID() == null || response.getFieldIdentifier().getOID().length() == 0)
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "FieldIdentifier with OID is required")); 
				throw new OperationException(); 
			}
			formFieldValueMap.put(response.getFieldIdentifier().getOID(), response.getValue()); 			
		}
		
		SaveFormStateKeeper stateKeeper = new SaveFormStateKeeper(); 
		fillFormDetailsWithValidations(studyPatientFormDesign, stateKeeper, formFieldValueMap); 
		stateKeeper.setAccountId(callingUser.getUserAccountId()); 
		stateKeeper.setCreator(EJBUtil.integerToString(callingUser.getUserId()));
		stateKeeper.setFormId(EJBUtil.integerToString(formID)); 
		stateKeeper.setIpAdd(IP_ADDRESS_FIELD_VALUE)   ;
		stateKeeper.setFormFillDate(DateUtil.dateToString(studyPatientFormResponse.getFormFillDt())); 
		stateKeeper.setDisplayType("SP"); 
		stateKeeper.setStudyId(EJBUtil.integerToString(studyID)); 
		stateKeeper.setPatientId(EJBUtil.integerToString(patientID)); 
		stateKeeper.setPatprotId(EJBUtil.integerToString(patProtID)); 
		String formStatus = null; 
		if(studyPatientFormResponse.getFormStatus() != null )
		{
		try
		{
			formStatus = dereferenceCodeStr(studyPatientFormResponse.getFormStatus(), CodeCache.CODE_TYPE_FILLED_FORM_STATUS, callingUser); 
		}catch(CodeNotFoundException ce)
		{
			addIssue(new Issue(IssueTypes.CODE_NOT_FOUND, "Form Status code not found")); 
		}
		}
		
		stateKeeper.setFormCompleted(formStatus);

		
		if(response.getIssues().getIssue().size() > 0)
		{
			throw new OperationException(); 
		}
	 
		SaveFormDao saveFormDao  =  new SaveFormDao(); 
		int result = saveFormDao.insertFilledFormBySP(stateKeeper); 
		if(result > 0)
		{
			StudyFormResponseIdentifier studyFormIdentifier = new StudyFormResponseIdentifier(); 
			ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PATIENT_FORM_RESPONSE, result); 
			studyFormIdentifier.setOID(map.getOID()); 
		
			response.addObjectCreatedAction(studyFormIdentifier);
		}else
		{
			
		}
		
		}catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("FormDesignServiceImpl getStudyPatientFormDesign", e); 
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("FormDesignServiceImpl getStudyPatientFormDesign", t);
			throw new OperationException(t);
		}
		
		return response;
		
	}
	
	
	private void fillFormDetailsWithValidations(LinkedFormDesign linkedFormDesign, SaveFormStateKeeper stateKeeper, Map<String, String> formFieldValueMap)
	{
		FormSections formSections = linkedFormDesign.getSections(); 

		List<FormSection> formSectionsList = formSections.getSection(); 
		List<FormField> formfieldList = new ArrayList<FormField>(); 
//		Map<String, String> fieldNameOIDMap = new HashMap<String, String>();

		for(FormSection formSection: formSectionsList)
		{
			formfieldList.addAll(formSection.getFields().getField()); 		
		}		

		for(FormField formfield: formfieldList)
		{
			//exclude disabled, hidden fields from any validation checks
			if((formfield.getFieldType().equals(FieldType.EDIT_BOX) || formfield.getFieldType().equals(FieldType.MULTIPLE_CHOICE)) && (formfield.getCharacteristics().equals(FieldCharacteristics.VISIBLE) || formfield.getCharacteristics().equals(FieldCharacteristics.READONLY)))
			{
				FieldIdentifier formFieldIdentifier = formfield.getFieldIdentifier(); 

				String value = formFieldValueMap.get(formFieldIdentifier.getOID()); 
								
				if((value == null || value.length() == 0)  && formfield.getValidations().isMandatory())
				{
					
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Missing value of Field with OID : '" + formFieldIdentifier.getOID() + "' and Field Name : '" + formfield.getName() + "'")); 
				}
				
				
				switch(formfield.getDataType()){
				case EDIT_BOX_DATE:
				{
					if(value != null && value.length() > 0 )
					{
						if(!DateUtil.isValidDateFormat(value))
						{
							addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Date for field with OID : '"+ formFieldIdentifier.getOID()+ "' and Field Name : '" + formfield.getName() + "'  is not valid date"));
						}else{
							Date date = DateUtil.stringToDate(value); 
							if(DateUtil.stringToDate(value)== null)
							{
								addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Date should be in format " + Configuration.getAppDateFormat() + "for Field with OID : '"+ formFieldIdentifier.getOID() +"' and Field Name : '" + formfield.getName()+"'"));
							}else if(date.after(new Date(System.currentTimeMillis())) && !((DateFieldValidations)formfield.getValidations()).isFutureDateAllowed())
							{
								addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Future date is not allowed for Form Field with OID : '"+ formFieldIdentifier.getOID() + "' and Field Name : '" + formfield.getName() +"'") ); 
							}
							value = DateUtil.dateToString(date); 
						}
					}
					break; 
				}
				case EDIT_BOX_NUMBER:
				{
					String format = null;
					NumberRange range = null; 
					if(formfield.getValidations() != null)
					{
						format = ((NumberFieldValidations)formfield.getValidations()).getFormat(); 
						range = ((NumberFieldValidations)formfield.getValidations()).getRange();
					}
					if(value != null && value.length() > 0)
					{
						boolean hasNoCharacters = true; 
						Integer maxlength = ((NumberFieldValidations) formfield.getValidations()).getMaxCharAllowed(); 
						if(maxlength != null && value.length() > maxlength )
						{
							addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Form Field with OID : '"+formfield.getFieldIdentifier().getOID() + "' and Field Name : '" + formfield.getName()+ "' exceeds "+ maxlength + ", field can only have upto "+ maxlength + " characters")); 
						}
						if(!(value.replaceFirst("[.]", "")).matches("\\d*"))
						{
							 addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Form Field with OID : '" + formFieldIdentifier.getOID() + "' and Field Name : '" + formfield.getName()+ "' should be number")); 
							 hasNoCharacters = false; 
						}
						if(hasNoCharacters && format != null && !value.matches(format.replaceAll("#", "\\\\d")))
						{
							addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Form Field with OID : '"+ formFieldIdentifier.getOID() + "' and Field Name : '" + formfield.getName()+ "' should be in format " + ((NumberFieldValidations)formfield.getValidations()).getFormat())); 
						}
						if(hasNoCharacters && range != null && !isNumberInRange(value, range))
						{
							StringBuffer errMsg = new StringBuffer(); 
							errMsg.append("Form Field with OID : '"); 
							errMsg.append(formFieldIdentifier.getOID());
							errMsg.append("' and Field Name : '");
							errMsg.append(formfield.getName());
							errMsg.append("' should be "); 
							if(range.getFirstOperator() != null && range.getFirstValue() != null) errMsg.append(range.getFirstOperator().name() + " " + range.getFirstValue()); 
							if(range.getLogicOperator() != null) errMsg.append(" " + range.getLogicOperator() + " "); 
							if(range.getSecondOperator() != null && range.getSecondValue() != null ) errMsg.append(range.getSecondOperator().name() + " " + range.getSecondValue());
							
							addIssue(new Issue(IssueTypes.DATA_VALIDATION, errMsg.toString())); 
						}
					}
					break; 
				}
				case EDIT_BOX_TEXT:
				{
					Integer maxlength = ((TextFieldValidations) formfield.getValidations()).getMaxCharAllowed(); 
					if(value != null && value.length()> 0  && value.contains("\""))
					{
						addIssue(new Issue(IssueTypes.DATA_VALIDATION, "\" is not allowed")); 
					}
					if(maxlength != null && value != null && value.length() > maxlength )
					{
						addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Form Field with OID : '"+  formfield.getFieldIdentifier().getOID() + "' and Field Name : '" + formfield.getName() + "' exceeds "+ maxlength + ", field can only have upto "+ maxlength + " characters")); 
					}
					break; 
				}
				case EDIT_BOX_TIME:
				{
					// Not sure where it is used
					break; 
				}

				case MULTIPLE_CHOICE_CHECKBOX:
				{
					if(value != null && value.length() > 0)
					{
						Map<String, String> displayValueList = new HashMap<String, String>(); 
						for(MultipleChoice choice:formfield.getChoices().getChoice())
						{
							if(!choice.isHidden())
							{
								displayValueList.put(choice.getDisplayValue().toUpperCase(), choice.getDataValue()); 
							}
						}
//						value = value.replace("[", ""); 
//						value = value.replace("]", ""); 
						String[] valuelist = splitValuesForCheckBox(value);
						value = ""; 
						for(int i = 0; i < valuelist.length; i++)
						{
							if(!displayValueList.containsKey(valuelist[i].toUpperCase()))
							{
								addIssue(new Issue(IssueTypes.DATA_VALIDATION, valuelist[i] + " is not allowed for Form Field with OID : '" + formFieldIdentifier.getOID()+ "' and Field Name : '" + formfield.getName() +"'"));
							}
							if(i > 0) value=value + "[VELCOMMA]"; 
							value = value + displayValueList.get(valuelist[i].toUpperCase());
						}
					}
					break; 

				}

				case MULTIPLE_CHOICE_DROPDOWN:
				{
					if(value != null && value.length() > 0)
					{
						Map<String, String> displayValueList = new HashMap<String, String>(); 
						for(MultipleChoice choice:formfield.getChoices().getChoice())
						{
							if(!choice.isHidden())
							{
								displayValueList.put(choice.getDisplayValue().toUpperCase(), choice.getDataValue()); 
							}
						}

						if(!displayValueList.containsKey(value.toUpperCase()))
						{
							addIssue(new Issue(IssueTypes.DATA_VALIDATION, value + " is not allowed for Form Field with OID : '"+ formFieldIdentifier.getOID()+ "' and  Field Name : '" + formfield.getName() + "'"));
						}

						value = displayValueList.get(value.toUpperCase());
					}
					break; 
				}

				case MULTIPLE_CHOICE_RADIO_BUTTON:
				{
					if(value != null && value.length() > 0)
					{
						Map<String, String> displayValueList = new HashMap<String, String>(); 
						for(MultipleChoice choice:formfield.getChoices().getChoice())
						{
							if(!choice.isHidden())
							{
								displayValueList.put(choice.getDisplayValue().toUpperCase(), choice.getDataValue()); 
							}
						}

						if(!displayValueList.containsKey(value.toUpperCase()))
						{
							addIssue(new Issue(IssueTypes.DATA_VALIDATION, value + " is not allowed for Form Field with OID : '"+ formFieldIdentifier.getOID()+ "' and Field Name : '" + formfield.getName() + "'"));
						}

						value = displayValueList.get(value.toUpperCase()); 
					}
					break; 
				}
				case MULTIPLE_CHOICE_LOOKUP:
				{

				}
				}

				stateKeeper.setParamValues(value==null?"":value ); 
				stateKeeper.setParams(formfield.getSystemID());
			}
			
		}
	}
	
	
	private String[] splitValuesForCheckBox(String s)
	{
		List<String> valueList = new ArrayList<String>(); 
		if(s != null && s.length() > 0)
		{
			if(s.matches("\\[.*\\]"))
			{
				for(int i = 0; i < s.length();)
				{
					valueList.add(s.substring(s.indexOf("[", i) + 1, s.indexOf("]",i)));
					i = s.indexOf("]", i) + 1; 
				}
			}else
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Checkbox Entries should be enclosed in square bracket []")); 
			}
		}
		
		return valueList.toArray(new String[valueList.size()]); 
	}	

	/* (non-Javadoc)
	 * @see com.velos.services.formresponse.FormResponseService#updateStudyPatientFormResponse(com.velos.services.model.StudyPatientFormResponseIdentifier, com.velos.services.model.StudyPatientFormResponse)
	 */
	public ResponseHolder updateStudyPatientFormResponse(
			StudyPatientFormResponseIdentifier formResponseIdentifier,
			FormResponse studyPatientFormResponse)
			throws OperationException {
		return updateStudyPatientFormResponse(formResponseIdentifier, studyPatientFormResponse, false); 
	}


	private ResponseHolder removeStudyPatientFormResponse(StudyPatientFormResponseIdentifier formResponseIdentifier, boolean isScheduleForm) throws OperationException
	{
		FormResponseServiceDAO formResponseServicedao = new FormResponseServiceDAO(); 
		Integer studyPatientFormID; 
		try{
			
			
			
			GroupAuthModule authModule = new GroupAuthModule(callingUser, groupRightsAgent); 
			Integer patientManagmentAccess = authModule.getAppManagePatientsPrivileges(); 

			if(!GroupAuthModule.hasViewPermission(patientManagmentAccess))
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User is not authorized to view Study Patient forms")); 
				throw new AuthorizationException(); 
			}
			
			if(formResponseIdentifier != null && formResponseIdentifier.getOID() != null && formResponseIdentifier.getOID().length() > 0)
			{
				ObjectMap objectMap = objectMapService.getObjectMapFromId(formResponseIdentifier.getOID()); 
				if(objectMap == null)
				{
					addIssue(new Issue(IssueTypes.FORM_RESPONSE_NOT_FOUND, "Form Response not found for OID " + formResponseIdentifier.getOID())); 
					throw new OperationException(); 
				}
				if(!(objectMap.getTableName().equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_PATIENT_FORM_RESPONSE)))
				{
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Please send valid StudyPatientFormResponseIdentifier"));
					throw new OperationException(); 
				}
				studyPatientFormID = objectMap.getTablePK(); 
				if(studyPatientFormID == null || studyPatientFormID == 0)
				{
					addIssue(new Issue(IssueTypes.FORM_RESPONSE_NOT_FOUND, "Form Response not found for OID " + formResponseIdentifier.getOID())); 
					throw new OperationException(); 
				}
				if(formResponseServicedao.isStudyPatientFormResponseDeleted(studyPatientFormID)){
					addIssue(new Issue(IssueTypes.FORM_RESPONSE_DOES_NOT_EXIT, "Form Response doesn't exist")); 
					throw new OperationException(); 
				}
				formResponseServicedao.populateStudyPatFormID(studyPatientFormID); 
				
				
				Integer patProtID = formResponseServicedao.getPatProtIDforFilledForm(); 
				Integer patientID = formResponseServicedao.getPerIDForFilledForm(); 
				Integer studyID = formResponseServicedao.getStudyPKForFilledForm(); 
				Integer formLibPK = formResponseServicedao.getFormLibIDforFilledForm(); 
				Integer schEventPK = null; 
				if(isScheduleForm) 
				{
					schEventPK = formResponseServicedao.getSchEventforFilledForm();
					if(schEventPK == null || schEventPK == 0 ) 
					{
						addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Requested Form response is not StudyPatientScheduleFormResponse, please use appropriate method to delete this form"));
						throw new OperationException(); 
						
					}
				}
				
		

				if(patProtID == null || patProtID == 0)
				{
					addIssue(new Issue(IssueTypes.STUDY_PATIENT_NOT_FOUND, "PatProt ID not found")); 
					throw new OperationException(); 
				}
						
				//check patient level access
			
				Integer userMaxStudyPatientRight = userSiteAgent.getMaxRightForStudyPatient(callingUser.getUserId(), patientID, studyID); 
				if(userMaxStudyPatientRight == 0)
				{
					addIssue(new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION, "User is not authorized to delete Form Response for patient")); 
					throw new OperationException();
				}

				//check Study level access

				TeamAuthModule teamAuth = new TeamAuthModule(callingUser.getUserId(), studyID); 
				Integer studyPatientManagement = teamAuth.getPatientManagePrivileges(); 
				Integer studyFormAccess = teamAuth.getFormsStudyAccessPrivileges(); 

				if(!TeamAuthModule.hasViewPermission(studyPatientManagement))
				{
					addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User is not authorized to delete Form Response for study")); 
					throw new OperationException(); 
				}

				if(!TeamAuthModule.hasEditPermission(studyFormAccess))
				{
					addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User is not authorized to delete Form Response for study")); 
					throw new OperationException(); 
				}
				
				
				PatStudyStatBean patStatBean = patStudyStatAgent.getLatestPatStudyStat(studyID, patientID); 
				
				
				CodeCache codeCache = CodeCache.getInstance(); 
				
				Code patStudyStatus = codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_PATPROT_STATUS, patStatBean.getPatStudyStat(), EJBUtil.stringToInteger(callingUser.getUserAccountId()));
				
				if(patStudyStatus.getCode().equals("lockdown"))
				{
					addIssue(new Issue(IssueTypes.PATIENT_STATUS_LOCKDOWN, "Patient Study status is lockdown"));
					throw new OperationException(); 
				}

				//check form level access
				FormDesignDAO formDesignDao = new FormDesignDAO();
				if(!formDesignDao.getStudyPatientFormListUserHasAccess(callingUser, studyID, true).contains(formLibPK))
				{
					addIssue(new Issue(IssueTypes.FORM_ACCESS_AUTHORIZATION, "User is not authorized to view this Form")); 
					throw new OperationException(); 
				}
				
				//BugFix#8852: Tarandeep Singh Bali
				//Check the form status
				
				StudyPatientFormDesign studyPatientFormDesign = new StudyPatientFormDesign(); 
				studyPatientFormDesign = (StudyPatientFormDesign) formDesignDao.getStudyPatientFormDesign(formLibPK, callingUser, objectMapService, studyPatientFormDesign, studyID, false);
				
				if(!(studyPatientFormDesign.getFormStatus().getCode().equals("A"))){
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Form is not active, response cannot be deleted")); 
					throw new OperationException(); 
					
				}
				//end of check the form status
				
				linkedFormAgent.deleteFilledFormResp(EJBUtil.integerToString(studyPatientFormID), "SP", EJBUtil.integerToString(callingUser.getUserId()), IP_ADDRESS_FIELD_VALUE);
				}else
			{
				addIssue(new Issue(IssueTypes.FORM_RESPONSE_NOT_FOUND, "Valid StudyPatientFormResponseIdentifier is required to get form response")); 
				throw new OperationException(); 
			}
			
			response.addAction(new CompletedAction(formResponseIdentifier, CRUDAction.REMOVE)); 
		}
	catch(OperationException e){
		sessionContext.setRollbackOnly();
		if (logger.isDebugEnabled()) logger.debug("FormResponseServiceImpl removeStudyPatientFormResponse", e);
		throw new OperationRolledBackException(response.getIssues()); 

	}
	catch(Throwable t){
		sessionContext.setRollbackOnly();
		addUnknownThrowableIssue(t);
		if (logger.isDebugEnabled()) logger.debug("FormResponseServiceImpl removeStudyPatientFormResponse", t);
		throw new OperationRolledBackException(response.getIssues());
	}
		return response;
	}
	
	
	public ResponseHolder removeStudyPatientFormResponse(
			StudyPatientFormResponseIdentifier formResponseIdentifier)
			throws OperationException {
		return removeStudyPatientFormResponse(formResponseIdentifier, false); 
	}
	
	public boolean isNumberInRange(String value, NumberRange range)
	{

		if(value != null && value.length() >0 && ((range.getFirstOperator() != null && range.getFirstValue() != null) || (range.getSecondOperator() != null && range.getSecondValue() != null)))
		{
			float num = Float.valueOf(value); 
			Boolean str1 = null; 
			Boolean str2 = null; 
			if(range.getFirstOperator() != null && range.getFirstValue() != null)
			{
				if(range.getFirstOperator().equals(Operator.GREATER_THAN))
				{
					str1 = num > Float.valueOf(range.getFirstValue());
				}else if(range.getFirstOperator().equals(Operator.LESS_THAN))
				{
					str1 = num < Float.valueOf(range.getFirstValue());
				}else if(range.getFirstOperator().equals(Operator.GREATER_THAN_EQUAL_TO))
				{
					str1 = num >= Float.valueOf(range.getFirstValue());
				}else if(range.getFirstOperator().equals(Operator.LESS_THAN_EQUAL_TO))
				{
					str1 = num <= Float.valueOf(range.getFirstValue());
				}
			}

			if(range.getSecondOperator() != null && range.getSecondValue() != null)
			{
				if(range.getSecondOperator().equals(Operator.GREATER_THAN))
				{
					str2 = num > Float.valueOf(range.getSecondValue());
				}else if(range.getSecondOperator().equals(Operator.LESS_THAN))
				{
					str2 = num < Float.valueOf(range.getSecondValue());
				}else if(range.getSecondOperator().equals(Operator.GREATER_THAN_EQUAL_TO))
				{
					str2 = num >= Float.valueOf(range.getSecondValue());
				}else if(range.getSecondOperator().equals(Operator.LESS_THAN_EQUAL_TO))
				{	
					str2 = num <= Float.valueOf(range.getSecondValue());
				}
			}


			if(str1 == null) return str2; 
			if(str2 == null) return str1; 

			if(range.getLogicOperator() != null && range.getLogicOperator().equals( LogicalOperator.AND))
			{
				
				if((str1) && (str2))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else if(range.getLogicOperator() != null && range.getLogicOperator().equals(LogicalOperator.OR))
			{
				if((str1) || (str2))
				{
					return true;
				}
				else
				{
					return false;
				}
			} else
			{
				if((str1) || (str2))
					return true;

				else
					return false;

			}



		}else
		{
			return true; 
		}


	}

	public ResponseHolder createStudyPatientScheduleFormResponse(
			StudyPatientScheduleFormResponse studyPatientScheduleFormResponse,
			CalendarIdentifier calendarIdentifier, String calendarName,
			VisitIdentifier visitIdentifier, String visitName,
			EventIdentifier eventIdentifier, String eventName) throws OperationException 
	{
		
		
		
		try{
			// Group rights
			validate(studyPatientScheduleFormResponse); 
			GroupAuthModule authModule = new GroupAuthModule(callingUser, groupRightsAgent); 
			Integer patientManagmentAccess = authModule.getAppManagePatientsPrivileges(); 
			
			if(!GroupAuthModule.hasEditPermission(patientManagmentAccess))
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User is not authorized to fill Study Patient forms")); 
				throw new AuthorizationException(); 
			}
		
			Integer studyID; 
		    Integer patientID;
		    Integer patProtID;
		    Integer schEventID;
		    Integer formID = null;
				
			if(studyPatientScheduleFormResponse.getFormId() != null && studyPatientScheduleFormResponse.getFormId().getOID() != null)
			{
				
				try {
					formID = ObjectLocator.formPKFromIdentifier(callingUser, studyPatientScheduleFormResponse.getFormId(), objectMapService, null, null, studyPatientScheduleFormResponse.getStudyIdentifier());
					if(formID == null)
					{
						addIssue(new Issue(IssueTypes.FORM_NOT_FOUND, "Form not found")); 
						throw new OperationException(); 
					}
				} catch (MultipleObjectsFoundException e) {
				
					e.printStackTrace();
				} 

			
			}else
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "FormId is required to create Form Response"));
				throw new OperationException(); 
			}
			
			FormResponseServiceDAO responseDao = new FormResponseServiceDAO(); 
			
			if(studyPatientScheduleFormResponse.getPatientProtocolIdentifier() != null && studyPatientScheduleFormResponse.getPatientProtocolIdentifier().getOID() != null && studyPatientScheduleFormResponse.getPatientProtocolIdentifier().getOID().length() != 0)
			{
				 	patProtID = objectMapService.getObjectPkFromOID(studyPatientScheduleFormResponse.getPatientProtocolIdentifier().getOID()); 
					
					if(patProtID == null || patProtID == 0)
					{
						addIssue(new Issue(IssueTypes.STUDY_PATIENT_NOT_FOUND, "PatProt ID not found")); 
						throw new OperationException(); 
					}
					PatProtBean patProtBean = patProtAgent.getPatProtDetails(patProtID); 
					studyID = EJBUtil.stringToInteger(patProtBean.getPatStudyId()); 
					patientID = EJBUtil.stringToInteger(patProtBean.getPatProtPersonId()); 
					
				
			}else if(studyPatientScheduleFormResponse.getPatientIdentifier() != null && (studyPatientScheduleFormResponse.getPatientIdentifier().getOID() != null || (studyPatientScheduleFormResponse.getPatientIdentifier().getPatientId() != null && studyPatientScheduleFormResponse.getPatientIdentifier().getOrganizationId() != null))
					&& studyPatientScheduleFormResponse.getStudyIdentifier() != null && (studyPatientScheduleFormResponse.getStudyIdentifier().getOID() != null || studyPatientScheduleFormResponse.getStudyIdentifier().getStudyNumber() != null))
			{
				studyID = ObjectLocator.studyPKFromIdentifier(callingUser, studyPatientScheduleFormResponse.getStudyIdentifier(), objectMapService); 
				
				if(studyID == null || studyID == 0)
				{
					addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found")); 
					throw new OperationException(); 
				}
				try{
					patientID = ObjectLocator.personPKFromPatientIdentifier(callingUser, studyPatientScheduleFormResponse.getPatientIdentifier(), objectMapService); 
				} catch (MultipleObjectsFoundException e) {
					addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND, 
							"Multiple Patients found")); 
					throw new OperationException(); 
				}
				
				if(patientID == null || patientID == 0)
				{
					addIssue(new Issue(IssueTypes.PATIENT_NOT_FOUND, "Patient not found")); 
					throw new OperationException();
				}
				
				//Now check if patient is linked to Study	
				
				PatProtBean  patProtBean = patProtAgent.findCurrentPatProtDetails(studyID, patientID); 
				
				if(patProtBean.getPatProtId() == 0)
				{
					addIssue(new Issue(IssueTypes.PATIENT_NOT_ONSTUDY, "Given patient is not linked to study provided")); 
					throw new OperationException(); 
				}
				patProtID = patProtBean.getPatProtId();			
				
				
			}else
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "PatientProtocolIdentifier or PatientIdentifier and StudyIdentifier is required to create StudyPatient FormResponse")); 
				throw new OperationException(); 
			}
			
			PatStudyStatBean patStatBean = patStudyStatAgent.getLatestPatStudyStat(studyID, patientID); 
			
			
			CodeCache codeCache = CodeCache.getInstance(); 
			
			Code patStudyStatus = codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_PATPROT_STATUS, patStatBean.getPatStudyStat(), EJBUtil.stringToInteger(callingUser.getUserAccountId()));
			
			if(patStudyStatus.getCode().equals("lockdown"))
			{
				addIssue(new Issue(IssueTypes.PATIENT_STATUS_LOCKDOWN, "Patient Study status is lockdown"));
				throw new OperationException(); 
			}
			
			
			Integer calendarEventID = null;
			
			ScheduleEventIdentifier schEventIdentifier = studyPatientScheduleFormResponse.getScheduleEventIdentifier(); 
			
			if(schEventIdentifier != null && schEventIdentifier.getOID() != null && schEventIdentifier.getOID().length() != 0)
			{
				schEventID = objectMapService.getObjectPkFromOID(schEventIdentifier.getOID());				
				if(schEventID == null || schEventID ==0)
				{
					addIssue(new Issue(IssueTypes.PATIENT_SCHEDULE_EVENT_NOT_FOUND, "Patient schedule not found"));
					throw new OperationException(); 
				}
				
				
				//check if schedule belongs to given patprot 
				if(!responseDao.isStudyPatientOnScheduleEvent(schEventID, studyID, patientID))
				{
					addIssue(new Issue(IssueTypes.STUDY_PATIENT_NOT_ON_CALENDAR, "Patient is not on given schedule event"));
					throw new OperationException();
				}
		
				calendarEventID = responseDao.getCalendarEventID(schEventID); 
				patProtID = responseDao.getPatProtIDforFilledForm();
			
				
			}else{
				
				if(eventIdentifier != null && eventIdentifier.getOID() != null && eventIdentifier.getOID().length() > 0)
				{
					calendarEventID = objectMapService.getObjectPkFromOID(eventIdentifier.getOID()); 
					if(calendarEventID == null || calendarEventID == 0)
					{ 
						addIssue(new Issue(IssueTypes.EVENT_NOT_FOUND,				
							"Event not found for Event Identifier : OID " + eventIdentifier.getOID())); 
						throw new OperationException(); 
					}
				}else if(eventName != null && eventName.length() > 0)
				{
					StudyCalendarDAO studyCalendarDao = new StudyCalendarDAO(); 
					Integer visitPK; 
					if(visitIdentifier != null && visitIdentifier.getOID() != null && visitIdentifier.getOID().length() > 0)
					{
						visitPK = objectMapService.getObjectPkFromOID(visitIdentifier.getOID()); 
						if(visitPK == null || visitPK == 0)
						{
							addIssue(new Issue(IssueTypes.VISIT_NOT_FOUND,
									"Visit not found for VisitIdentifier : OID " + visitIdentifier.getOID())); 
							throw new OperationException();
						}					
						
					}else if(visitName != null && visitName.length() > 0)
					{
						Integer calendarPK; 
						if(calendarIdentifier != null && calendarIdentifier.getOID() != null && calendarIdentifier.getOID().length() > 0)
						{
							calendarPK = objectMapService.getObjectPkFromOID(calendarIdentifier.getOID()); 
							if(calendarPK == null || calendarPK == 0)
							{
								addIssue(new Issue(IssueTypes.CALENDAR_NOT_FOUND,
										"Calendar not found for CalendarIdentifier: OID " + calendarIdentifier.getOID())); 
								throw new OperationException(); 
							}
						}else if(calendarName != null && calendarName.length() > 0)
						{
							
							calendarPK = studyCalendarDao.locateCalendarPK(calendarName, studyID); 
							if(calendarPK == null || calendarPK == 0)
							{
								addIssue(new Issue(IssueTypes.CALENDAR_NOT_FOUND,
										"Calendar not found for Calendar Name " + calendarName)); 
								throw new OperationException(); 
							}
						}else{
							addIssue(new Issue(IssueTypes.DATA_VALIDATION, "EventIdentifier Or VisitIdentifier and EventName OR CalendarIdentifier, VisitName and EventName or CalendarName, VisitName and Event Name is required to locate event"));
							 throw new OperationException(); 
						}
						
						visitPK = studyCalendarDao.getVisitPKByName(visitName, calendarPK); 
						if(visitPK == null || visitPK == 0)
						{
							addIssue(new Issue(IssueTypes.VISIT_NOT_FOUND, "Visit not found for Visit Name "+ visitName)); 
							throw new OperationException(); 
						}
						
						
					}else{
						
						addIssue(new Issue(IssueTypes.DATA_VALIDATION, "EventIdentifier Or VisitIdentifier and EventName OR CalendarIdentifier, VisitName and EventName or CalendarName, VisitName and Event Name is required to locate event")); 
						throw new OperationException();
					}
					

					calendarEventID = studyCalendarDao.getEventPKByEventName(eventName, visitPK); 
					if(calendarEventID == null || calendarEventID == 0)
					{ 
						addIssue(new Issue(IssueTypes.EVENT_NOT_FOUND,				
							"Event not found for Event Identifier : OID " + eventIdentifier.getOID())); 
						throw new OperationException(); 
					}
					
				}else
				{
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "EventIdentifier Or VisitIdentifier and EventName OR CalendarIdentifier, VisitName and EventName or CalendarName, VisitName and Event Name is required to locate event"));
					throw new OperationException(); 
				}
				
				
				schEventID = responseDao.getActivePatientScheduleEventID(calendarEventID,patProtID); 
				if(schEventID == null || schEventID ==0)
				{
					addIssue(new Issue(IssueTypes.PATIENT_SCHEDULE_EVENT_NOT_FOUND, "Patient schedule event not found"));
					throw new OperationException(); 
				}
				
			}
			
			
			
			
			
			
			
			//check if given forms are linked to events
			
			if(!responseDao.getEventLinkedForms(calendarEventID).contains(formID))
			{
				addIssue(new Issue(IssueTypes.CRF_NOT_LINKED_WITH_CALENDAR_EVENT, "Given form is not linked with event provided"));
				throw new OperationException(); 
			}
				
			
			//check patient level access			
			Integer userMaxStudyPatientRight = userSiteAgent.getMaxRightForStudyPatient(callingUser.getUserId(), patientID, studyID); 
			if(userMaxStudyPatientRight == 0)
			{
				addIssue(new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION, "User is not authorized to create Form Response for patient")); 
				throw new OperationException();
			}
			
			//check Study level access
		
			TeamAuthModule teamAuth = new TeamAuthModule(callingUser.getUserId(), studyID); 
			Integer studyPatientManagement = teamAuth.getPatientManagePrivileges(); 
			Integer studyFormAccess = teamAuth.getFormsStudyAccessPrivileges(); 
			
			if(!TeamAuthModule.hasNewPermission(studyPatientManagement))
			{
				addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User is not authorized to create Form Response for study")); 
				throw new OperationException(); 
			}
			
			if(!TeamAuthModule.hasNewPermission(studyFormAccess))
			{
				addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User is not authorized to create Form Response for study")); 
				throw new OperationException(); 
			}
			
			//check form level access
			FormDesignDAO formDesignDao = new FormDesignDAO();
			
			if(!formDesignDao.getStudyPatientFormListUserHasAccess(callingUser, studyID, true).contains(formID))
			{
				addIssue(new Issue(IssueTypes.FORM_ACCESS_AUTHORIZATION, "User is not authorized to view this Form")); 
				throw new OperationException(); 
			}
			StudyPatientScheduleFormDesign studyPatientFormDesign  = new StudyPatientScheduleFormDesign(); 
			formDesignDao.getStudyPatientFormDesign(formID, callingUser, objectMapService, studyPatientFormDesign, studyID, false); 
			
			
			// return error if form status is anything but 'Active'
			if(!(studyPatientFormDesign.getFormStatus().getCode().equals("A"))){
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Form is not active, response cannot be created")); 
				throw new OperationException(); 
				
			}
	
			//Single/Multiple entry check
			if((!studyPatientFormDesign.getEntrySetting().equals(EntrySettings.MULTIPLE)) && (responseDao.getFormResponseCount(formID, patientID, schEventID) > 0))
			{
				addIssue(new Issue(IssueTypes.SINGLE_ENTRY_FORM_RESPONSE_ALREADY_EXISTS, "Form is Single Entry form and a response already exists")); 
				throw new OperationException();
			}
			

				
			//Field level checks (mandatory and dataType validations) 
			Map<String, String> formFieldValueMap = new HashMap<String, String>(); 

			for(FormFieldResponse response: studyPatientScheduleFormResponse.getFormFieldResponses().getField())
			{
				if(response.getFieldIdentifier() == null || response.getFieldIdentifier().getOID() == null || response.getFieldIdentifier().getOID().length() == 0)
				{
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "FieldIdentifier with OID is required")); 
					throw new OperationException(); 
				}
				formFieldValueMap.put(response.getFieldIdentifier().getOID(), response.getValue()); 			
			}
			
			SaveFormStateKeeper stateKeeper = new SaveFormStateKeeper(); 
			fillFormDetailsWithValidations(studyPatientFormDesign, stateKeeper, formFieldValueMap); 
			stateKeeper.setAccountId(callingUser.getUserAccountId()); 
			stateKeeper.setCreator(EJBUtil.integerToString(callingUser.getUserId()));
			stateKeeper.setFormId(EJBUtil.integerToString(formID)); 
			stateKeeper.setIpAdd(IP_ADDRESS_FIELD_VALUE)   ;
			stateKeeper.setFormFillDate(DateUtil.dateToString(studyPatientScheduleFormResponse.getFormFillDt())); 
			stateKeeper.setDisplayType("SP"); 
			stateKeeper.setStudyId(EJBUtil.integerToString(studyID)); 
			stateKeeper.setPatientId(EJBUtil.integerToString(patientID)); 
			stateKeeper.setPatprotId(EJBUtil.integerToString(patProtID)); 
			stateKeeper.setSchEvent(EJBUtil.integerToString(schEventID)); 
			String formStatus = null; 
			if(studyPatientScheduleFormResponse.getFormStatus() != null )
			{
			try
			{
				formStatus = dereferenceCodeStr(studyPatientScheduleFormResponse.getFormStatus(), CodeCache.CODE_TYPE_FILLED_FORM_STATUS, callingUser); 
			}catch(CodeNotFoundException ce)
			{
				addIssue(new Issue(IssueTypes.CODE_NOT_FOUND, "Form Status code not found")); 
			}
			}
			
			stateKeeper.setFormCompleted(formStatus);

			
			if(response.getIssues().getIssue().size() > 0)
			{
				throw new OperationException(); 
			}
		 
			SaveFormDao saveFormDao  =  new SaveFormDao(); 
			int result = saveFormDao.insertFilledFormBySP(stateKeeper); 
			if(result > 0)
			{
				StudyFormResponseIdentifier studyFormIdentifier = new StudyFormResponseIdentifier(); 
				ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PATIENT_FORM_RESPONSE, result); 
				studyFormIdentifier.setOID(map.getOID()); 
			
				response.addObjectCreatedAction(studyFormIdentifier);
			}else
			{
				
			}
			
			}catch(OperationException e){
				sessionContext.setRollbackOnly();
				if (logger.isDebugEnabled()) logger.debug("FormDesignServiceImpl getStudyPatientFormDesign", e); 
				e.setIssues(response.getIssues());
				throw e;
			}
			catch(Throwable t){
				sessionContext.setRollbackOnly();
				this.addUnknownThrowableIssue(t);
				if (logger.isDebugEnabled()) logger.debug("FormDesignServiceImpl getStudyPatientFormDesign", t);
				throw new OperationException(t);
			}
			
			return response;	

	}

	/* (non-Javadoc)
	 * @see com.velos.services.formresponse.FormResponseService#getStudyPatientScheduleFormResponse(com.velos.services.model.StudyPatientFormResponseIdentifier)
	 */
	public StudyPatientScheduleFormResponse getStudyPatientScheduleFormResponse(
			StudyPatientFormResponseIdentifier studyPatientFormResponseIdentifier)
			throws OperationException {
		
		StudyPatientScheduleFormResponse formResponse = new StudyPatientScheduleFormResponse();
		FormResponseServiceDAO formResponseServicedao = new FormResponseServiceDAO(); 
		Integer studyPatientFormID; 
		try{
			
			
			
			GroupAuthModule authModule = new GroupAuthModule(callingUser, groupRightsAgent); 
			Integer patientManagmentAccess = authModule.getAppManagePatientsPrivileges(); 

			if(!GroupAuthModule.hasEditPermission(patientManagmentAccess))
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User is not authorized to view Study Patient forms")); 
				throw new AuthorizationException(); 
			}
			
			if(studyPatientFormResponseIdentifier != null && studyPatientFormResponseIdentifier.getOID() != null && studyPatientFormResponseIdentifier.getOID().length() > 0)
			{
				ObjectMap objectMap = objectMapService.getObjectMapFromId(studyPatientFormResponseIdentifier.getOID()); 
				
				if(objectMap == null || !(objectMap.getTableName().equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_PATIENT_FORM_RESPONSE)))
				{
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Please send valid StudyPatientFormResponseIdentifier"));
					throw new OperationException();
				}
	//			studyPatientFormID = ((ObjectMap) objectMapService.getObjectMapFromId(formResponseIdentifier.getOID())).getTablePK(); 
				studyPatientFormID = objectMap.getTablePK(); 
				if(studyPatientFormID == null || studyPatientFormID == 0)
				{
					addIssue(new Issue(IssueTypes.FORM_RESPONSE_NOT_FOUND, "Form Response not found for OID " + studyPatientFormResponseIdentifier.getOID())); 
					throw new OperationException(); 
				}
				
				formResponseServicedao.populateStudyPatFormID(studyPatientFormID); 
				
				
				Integer patProtID = formResponseServicedao.getPatProtIDforFilledForm(); 
				Integer patientID = formResponseServicedao.getPerIDForFilledForm(); 
				Integer studyID = formResponseServicedao.getStudyPKForFilledForm(); 
				Integer formLibPK = formResponseServicedao.getFormLibIDforFilledForm(); 
				Integer schEventPK = formResponseServicedao.getSchEventforFilledForm(); 
				
				if(schEventPK == null || schEventPK == 0)
				{
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Form response requested is not StudyPatientScheduleFormResponse"));
					throw new OperationException(); 
				}
		

				if(patProtID == null || patProtID == 0)
				{
					addIssue(new Issue(IssueTypes.STUDY_PATIENT_NOT_FOUND, "PatProt ID not found")); 
					throw new OperationException(); 
				}
						
				//check patient level access
				Integer userMaxStudyPatientRight = userSiteAgent.getMaxRightForStudyPatient(callingUser.getUserId(), patientID, studyID); 
				if(userMaxStudyPatientRight == 0)
				{
					addIssue(new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION, "User is not authorized to view Form Response for patient")); 
					throw new OperationException();
				}

				//check Study level access

				TeamAuthModule teamAuth = new TeamAuthModule(callingUser.getUserId(), studyID); 
				Integer studyPatientManagement = teamAuth.getPatientManagePrivileges(); 
				Integer studyFormAccess = teamAuth.getFormsStudyAccessPrivileges(); 

				if(!TeamAuthModule.hasNewPermission(studyPatientManagement))
				{
					addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User is not authorized to view Form Response for study")); 
					throw new OperationException(); 
				}

				if(!TeamAuthModule.hasViewPermission(studyFormAccess))
				{
					addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User is not authorized to view Form Response for study")); 
					throw new OperationException(); 
				}

				//check form level access
			    FormDesignDAO  formDesignDAO = new FormDesignDAO(); 
			    StudyPatientFormDesign studyPatientFormDesign  = new StudyPatientFormDesign(); 
		        try {
					formDesignDAO.getStudyPatientFormDesign(formLibPK, callingUser, objectMapService, studyPatientFormDesign, studyID, false);
				} catch (OperationException e) {				
					e.printStackTrace();
				} 
		        
		        if(!(studyPatientFormDesign.getFormStatus().getCode().equals("A") || studyPatientFormDesign.getFormStatus().getCode().equals("L")))
		        {
		        	addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Form is not active"));
		        	throw new OperationException(); 
		        }
				try{
				formResponse = formResponseServicedao.getStudyPatientScheduleFormResponse(studyPatientFormID, callingUser, objectMapService, studyPatientFormDesign); 
				}catch(OperationException oe)
				{
					addIssue(new Issue(IssueTypes.FORM_RESPONSE_DOES_NOT_EXIT, "Form Response does'nt exit")); 
					throw new OperationException(); 
				}
				
			}else
			{
				addIssue(new Issue(IssueTypes.FORM_RESPONSE_NOT_FOUND, "Valid StudyPatientFormResponseIdentifier is required to get form response")); 
				throw new OperationException(); 
			}
				
		

		}catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("FormResponseServiceImpl create", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("FormResponseServiceImpl create", t);
			throw new OperationException(t);
		}
		
		return formResponse;
	}
	
	
	public ResponseHolder updateStudyPatientScheduleFormResponse(StudyPatientFormResponseIdentifier formResponseIdentifier,
			StudyPatientScheduleFormResponse studyPatientScheduleFormResponse) throws OperationException{
		return updateStudyPatientFormResponse(formResponseIdentifier, studyPatientScheduleFormResponse, true); 
	}
	
	private ResponseHolder updateStudyPatientFormResponse(StudyPatientFormResponseIdentifier formResponseIdentifier,
			FormResponse studyPatientScheduleFormResponse,
			boolean isCRF)
	throws OperationException{

		StudyPatientFormResponse currentDBFormResponse = new StudyPatientFormResponse();
		FormResponseServiceDAO formResponseServicedao = new FormResponseServiceDAO(); 
		Integer studyPatientFormID; 
		try{



			GroupAuthModule authModule = new GroupAuthModule(callingUser, groupRightsAgent); 
			Integer patientManagmentAccess = authModule.getAppManagePatientsPrivileges(); 

			if(!GroupAuthModule.hasViewPermission(patientManagmentAccess))
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User is not authorized to update Study Patient forms")); 
				throw new AuthorizationException(); 
			}

			if(formResponseIdentifier != null && formResponseIdentifier.getOID() != null && formResponseIdentifier.getOID().length() > 0)
			{
				ObjectMap objectMap = objectMapService.getObjectMapFromId(formResponseIdentifier.getOID()); 
				if(objectMap == null)
				{
					addIssue(new Issue(IssueTypes.FORM_RESPONSE_NOT_FOUND, "Form Response not found for OID " + formResponseIdentifier.getOID())); 
					throw new OperationException(); 
				}
				if(!(objectMap.getTableName().equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_PATIENT_FORM_RESPONSE)))
				{
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Please send valid StudyPatientFormResponseIdentifier")); 
					throw new OperationException(); 
				}
				studyPatientFormID = objectMap.getTablePK(); 
				if(studyPatientFormID == null || studyPatientFormID == 0)
				{
					addIssue(new Issue(IssueTypes.FORM_RESPONSE_NOT_FOUND, "Form Response not found for OID " + formResponseIdentifier.getOID())); 
					throw new OperationException(); 
				}

				if(formResponseServicedao.isStudyPatientFormResponseDeleted(studyPatientFormID)){
					addIssue(new Issue(IssueTypes.FORM_RESPONSE_DOES_NOT_EXIT, "Form Response doesn't exist")); 
					throw new OperationException(); 
				}

				formResponseServicedao.populateStudyPatFormID(studyPatientFormID); 


				Integer patProtID = formResponseServicedao.getPatProtIDforFilledForm(); 
				Integer patientID = formResponseServicedao.getPerIDForFilledForm(); 
				Integer studyID = formResponseServicedao.getStudyPKForFilledForm(); 
				Integer formLibPK = formResponseServicedao.getFormLibIDforFilledForm(); 
				Integer schEventID = formResponseServicedao.getSchEventforFilledForm();
				
				if(isCRF && (schEventID == null || schEventID == 0))
				{
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Form Response Identifier is not StudyPatientScheduleFormResponse, please use appropriate method to update this form")); 
					throw new OperationException(); 
				}
				
				PatStudyStatBean patStatBean = patStudyStatAgent.getLatestPatStudyStat(studyID, patientID); 
				
				
				CodeCache codeCache = CodeCache.getInstance(); 
				
				Code patStudyStatus = codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_PATPROT_STATUS, patStatBean.getPatStudyStat(), EJBUtil.stringToInteger(callingUser.getUserAccountId()));
				
				if(patStudyStatus.getCode().equals("lockdown"))
				{
					addIssue(new Issue(IssueTypes.PATIENT_STATUS_LOCKDOWN, "Patient Study status is lockdown"));
					throw new OperationException(); 
				}
				


				if(patProtID == null || patProtID == 0)
				{
					addIssue(new Issue(IssueTypes.STUDY_PATIENT_NOT_FOUND, "PatProt ID not found")); 
					throw new OperationException(); 
				}

				//check patient level access
				Integer userMaxStudyPatientRight = userSiteAgent.getMaxRightForStudyPatient(callingUser.getUserId(), patientID, studyID); 
				if(userMaxStudyPatientRight == 0)
				{
					addIssue(new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION, "User is not authorized to update Form Response for patient")); 
					throw new OperationException();
				}

				//check Study level access

				TeamAuthModule teamAuth = new TeamAuthModule(callingUser.getUserId(), studyID); 
				Integer studyPatientManagement = teamAuth.getPatientManagePrivileges(); 
				Integer studyFormAccess = teamAuth.getFormsStudyAccessPrivileges(); 

				if(!TeamAuthModule.hasViewPermission(studyPatientManagement))
				{
					addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User is not authorized to edit Form Response for study")); 
					throw new OperationException(); 
				}

				if(!TeamAuthModule.hasEditPermission(studyFormAccess))
				{
					addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User is not authorized to edit Form Response for study")); 
					throw new OperationException(); 
				}

 


				//check form level access
				FormDesignDAO formDesignDao = new FormDesignDAO();
				StudyPatientFormDesign studyPatientFormDesign = new StudyPatientFormDesign(); 
				studyPatientFormDesign = (StudyPatientFormDesign) formDesignDao.getStudyPatientFormDesign(formLibPK, callingUser, objectMapService, studyPatientFormDesign, studyID, false);

				if(!(studyPatientFormDesign.getFormStatus().getCode().equals("A"))){
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Form is not active, response cannot be updated")); 
					throw new OperationException(); 

				}
				if(!formDesignDao.getStudyPatientFormListUserHasAccess(callingUser, studyID, true).contains(formLibPK))
				{
					addIssue(new Issue(IssueTypes.FORM_ACCESS_AUTHORIZATION, "User is not authorized to update this Form")); 
					throw new OperationException(); 
				}

				currentDBFormResponse = formResponseServicedao.getStudyPatientFormResponse(studyPatientFormID, callingUser, objectMapService, studyPatientFormDesign); 
				FormFieldResponses dbResponses = currentDBFormResponse.getFormFieldResponses(); 
				FormFieldResponses updatedResponses = studyPatientScheduleFormResponse.getFormFieldResponses(); 

				String formStatus = null; 
				if(studyPatientScheduleFormResponse.getFormStatus() != null )
				{
					try
					{
						formStatus = dereferenceCodeStr(studyPatientScheduleFormResponse.getFormStatus(), CodeCache.CODE_TYPE_FILLED_FORM_STATUS, callingUser); 
					}catch(CodeNotFoundException ce)
					{
						addIssue(new Issue(IssueTypes.CODE_NOT_FOUND, "Form Status code not found")); 
						throw new OperationException(); 
					}
				}else
				{
					formStatus = dereferenceCodeStr(currentDBFormResponse.getFormStatus(), CodeCache.CODE_TYPE_FILLED_FORM_STATUS, callingUser);
				}
				if(currentDBFormResponse.getFormStatus().getCode().equals("lockdown"))
				{
					addIssue( new Issue(IssueTypes.FORM_RESPONSE_LOCKDOWN, "Form Response is in lockdown status and cannot be updated")); 
					throw new OperationException(); 
				}

				Map<String, String> responseMap = new HashMap<String, String>(); 
				for(FormFieldResponse response:dbResponses.getField())
				{
					responseMap.put(response.getFieldIdentifier().getOID(), response.getValue()); 
				}
				if(updatedResponses != null){
					for(FormFieldResponse response: updatedResponses.getField())
					{
						if(response.getFieldIdentifier() == null || response.getFieldIdentifier().getOID() == null || response.getFieldIdentifier().getOID().length() == 0)
						{
							addIssue(new Issue(IssueTypes.DATA_VALIDATION, "FieldIdentifier with OID is required")); 
							throw new OperationException(); 
						}
						responseMap.put(response.getFieldIdentifier().getOID(), response.getValue()); 
					}
				}


				FormLibDao fd = new FormLibDao();
				String formLibVer = fd.getLatestFormLibVersionPK(formLibPK);
				String formLibVerNumber = fd.getFormLibVersionNumber(EJBUtil.stringToNum(formLibVer));
				//Bug Fix # 8875
				if(!currentDBFormResponse.getFormVersion().equals(Integer.parseInt(formLibVerNumber)))
				{
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Form response can only be updated for most recent version of Form")); 
				}
				//End of Bug Fix # 8875
				SaveFormStateKeeper stateKeeper = new SaveFormStateKeeper(); 
				fillFormDetailsWithValidations(studyPatientFormDesign, stateKeeper, responseMap); 
				stateKeeper.setAccountId(callingUser.getUserAccountId()); 
				stateKeeper.setLastModifiedBy(EJBUtil.integerToString(callingUser.getUserId()));
				stateKeeper.setFormId(EJBUtil.integerToString(formLibPK)); 
				stateKeeper.setIpAdd(IP_ADDRESS_FIELD_VALUE)   ;
				stateKeeper.setFormFillDate(DateUtil.dateToString(studyPatientScheduleFormResponse.getFormFillDt())); 
				stateKeeper.setDisplayType("SP"); 
				stateKeeper.setStudyId(EJBUtil.integerToString(studyID)); 
				stateKeeper.setPatientId(EJBUtil.integerToString(patientID)); 
				stateKeeper.setPatprotId(EJBUtil.integerToString(patProtID)); 
				if(isCRF) stateKeeper.setSchEvent(EJBUtil.integerToString(schEventID)); 
				stateKeeper.setFormCompleted(formStatus);

				stateKeeper.setFormLibVer(formLibVer);
				stateKeeper.setId(studyPatientFormID);


				if(response.getIssues().getIssue().size() > 0)
				{
					throw new OperationException(); 
				}

				SaveFormDao saveFormDao  =  new SaveFormDao(); 
				int result = saveFormDao.updateFilledFormBySP(stateKeeper); 
				if(result < 0)
				{

				}
			}else
			{
				addIssue(new Issue(IssueTypes.FORM_RESPONSE_NOT_FOUND, "Valid StudyPatientFormResponseIdentifier is required to get form response")); 
				throw new OperationException(); 
			}		

			response.addAction(new CompletedAction(formResponseIdentifier, CRUDAction.UPDATE));

		}catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("FormResponseServiceImpl update", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("FormResponseServiceImpl update", t);
			throw new OperationException(t);
		}

		return response;

	}

	public ResponseHolder removeStudyPatientScheduleFormResponse(
			StudyPatientFormResponseIdentifier formResponseIdentifier)
			throws OperationException {
		
		return removeStudyPatientFormResponse(formResponseIdentifier, true); 
		
	}

	/* (non-Javadoc)
	 * @see com.velos.services.formresponse.FormResponseService#updateStudyFormResponse(com.velos.services.model.StudyFormResponseIdentifier, com.velos.services.model.StudyFormResponse)
	 */
	public ResponseHolder updateStudyFormResponse(
			StudyFormResponseIdentifier formResponseIdentifier,
			StudyFormResponse studyFormResponse) throws OperationException {
		
		StudyFormResponse currentDBFormResponse = null; 
		FormResponseServiceDAO formResponseServiceDao = new FormResponseServiceDAO(); 
		Integer filledFormPK= null; 
		try
		{
			GroupAuthModule authModule = new GroupAuthModule(callingUser, groupRightsAgent); 
			Integer manageProtocolAccess = authModule.getAppManageProtocolsPrivileges(); 

			if(!GroupAuthModule.hasViewPermission(manageProtocolAccess))
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User is not authorized to update Study Patient forms")); 
				throw new AuthorizationException(); 
			}

			if(formResponseIdentifier != null && formResponseIdentifier.getOID() != null && formResponseIdentifier.getOID().length() > 0)
			{
				ObjectMap objectMap = objectMapService.getObjectMapFromId(formResponseIdentifier.getOID()); 
				if(objectMap == null)
				{
					addIssue(new Issue(IssueTypes.FORM_RESPONSE_NOT_FOUND, "Form Response not found for OID " + formResponseIdentifier.getOID())); 
					throw new OperationException(); 
				}
				if(!(objectMap.getTableName().equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_STUDY_FORM_RESPONSE)))
				{
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Please send valid StudyFormResponseIdentifier")); 
					throw new OperationException(); 
				}
				filledFormPK = objectMap.getTablePK(); 
				if(filledFormPK == null || filledFormPK == 0)
				{
					addIssue(new Issue(IssueTypes.FORM_RESPONSE_NOT_FOUND, "Form Response not found for OID " + formResponseIdentifier.getOID())); 
					throw new OperationException(); 
				}

				if(formResponseServiceDao.isStudyFormResponseDeleted(filledFormPK)){
					addIssue(new Issue(IssueTypes.FORM_RESPONSE_DOES_NOT_EXIT, "Form Response doesn't exist")); 
					throw new OperationException(); 
				}
				
				formResponseServiceDao.populateFormIDForStudyFilledFormID(filledFormPK); 
				Integer formLibPK = formResponseServiceDao.getFormLibIDforFilledForm(); 
				Integer studyPK =  formResponseServiceDao.getStudyPKForFilledForm(); 
				
				TeamAuthModule teamAuth = new TeamAuthModule(callingUser.getUserId(), studyPK); 
				Integer studyFormAccess = teamAuth.getFormsStudyAccessPrivileges();
				Integer allStudyGroupRights = authModule.getFormsAllStudyFormsPrivileges(); 
				
				FormDesignDAO formDesignDao = new FormDesignDAO(); 
				StudyFormDesign studyFormDesign = new StudyFormDesign(); 
				studyFormDesign = formDesignDao.getStudyPatientFormDesign(formLibPK, callingUser, objectMapService, studyFormDesign, studyPK, false); 

				if( !teamAuth.hasAccessToStudy() || (!TeamAuthModule.hasEditPermission(studyFormAccess) && studyFormDesign.getDisplayType().equals(DisplayType.STUDY)) ||
						(!GroupAuthModule.hasEditPermission(allStudyGroupRights) && studyFormDesign.getDisplayType().equals(DisplayType.ALL_STUDIES)))
				{
					addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User is not authorized to View Form Response for study")); 
					throw new OperationException(); 
				}
				//Added the check to validate of the user belongs to the Organization which is mentioned in the filter for the Study Form: Tarandeep Singh Bali
				LinkedFormsDao linkedformdao = linkedFormAgent.getStudyForms(EJBUtil.stringToInteger(callingUser.getUserAccountId()), studyPK, callingUser.getUserId(), EJBUtil.stringToInteger(callingUser.getUserSiteId()),allStudyGroupRights,studyFormAccess,false,"",""); 
				
				if(!linkedformdao.getFormId().contains(formLibPK))
				{
					addIssue(new Issue(IssueTypes.FORM_ACCESS_AUTHORIZATION, "User is not authorized to create Form Response for this form")); 
					throw new OperationException(); 
				}
				
				if(!(studyFormDesign.getFormStatus().getCode().equals("A"))){
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Form is not active, response cannot be updated")); 
					throw new OperationException(); 
					
				}
				
				currentDBFormResponse = formResponseServiceDao.getStudyFormResponse(filledFormPK, callingUser, objectMapService, studyFormDesign); 
				FormFieldResponses dbResponses = currentDBFormResponse.getFormFieldResponses(); 
				FormFieldResponses updatedResponses = studyFormResponse.getFormFieldResponses(); 

				String formStatus = null; 

				if(studyFormResponse.getFormStatus() != null )
				{
					try
					{
						formStatus = dereferenceCodeStr(studyFormResponse.getFormStatus(), CodeCache.CODE_TYPE_FILLED_FORM_STATUS, callingUser); 
					}catch(CodeNotFoundException ce)
					{
						addIssue(new Issue(IssueTypes.CODE_NOT_FOUND, "Form Status code not found")); 
						throw new OperationException(); 
					}
				}else
				{
					formStatus = dereferenceCodeStr(currentDBFormResponse.getFormStatus(), CodeCache.CODE_TYPE_FILLED_FORM_STATUS, callingUser);
				}
				if(currentDBFormResponse.getFormStatus().getCode().equals("lockdown"))
				{
					addIssue( new Issue(IssueTypes.FORM_RESPONSE_LOCKDOWN, "Form Response is in lockdown status and cannot be updated")); 
					throw new OperationException(); 
				}

				Map<String, String> responseMap = new HashMap<String, String>(); 
				for(FormFieldResponse response:dbResponses.getField())
				{
					responseMap.put(response.getFieldIdentifier().getOID(), response.getValue()); 
				}
				if(updatedResponses != null){
					for(FormFieldResponse response: updatedResponses.getField())
					{
						if(response.getFieldIdentifier() == null || response.getFieldIdentifier().getOID() == null || response.getFieldIdentifier().getOID().length() == 0)
						{
							addIssue(new Issue(IssueTypes.DATA_VALIDATION, "FieldIdentifier with OID is required")); 
							throw new OperationException(); 
						}
						responseMap.put(response.getFieldIdentifier().getOID(), response.getValue()); 
					}
				}


				FormLibDao fd = new FormLibDao();
				String formLibVer = fd.getLatestFormLibVersionPK(formLibPK);
				String formLibVerNumber = fd.getFormLibVersionNumber(EJBUtil.stringToNum(formLibVer));
				//Bug Fix # 8875
				if(!currentDBFormResponse.getFormVersion().equals(Integer.parseInt(formLibVerNumber)))
				{
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Form response can only be updated for most recent version of Form")); 
				}
				//End of Bug Fix # 8875
				SaveFormStateKeeper stateKeeper = new SaveFormStateKeeper(); 
				fillFormDetailsWithValidations(studyFormDesign, stateKeeper, responseMap); 
				stateKeeper.setAccountId(callingUser.getUserAccountId()); 
				stateKeeper.setLastModifiedBy(EJBUtil.integerToString(callingUser.getUserId()));
				stateKeeper.setFormId(EJBUtil.integerToString(formLibPK)); 
				stateKeeper.setIpAdd(IP_ADDRESS_FIELD_VALUE)   ;
				stateKeeper.setFormFillDate(DateUtil.dateToString(studyFormResponse.getFormFillDt())); 
				stateKeeper.setDisplayType(DisplayType.STUDY.toString());
				stateKeeper.setStudyId(EJBUtil.integerToString(studyPK)); 
				stateKeeper.setFormCompleted(formStatus);

				stateKeeper.setFormLibVer(formLibVer);
				stateKeeper.setId(filledFormPK);


				if(response.getIssues().getIssue().size() > 0)
				{
					throw new OperationException(); 
				}

				SaveFormDao saveFormDao  =  new SaveFormDao(); 
				int result = saveFormDao.updateFilledFormBySP(stateKeeper); 
				if(result < 0)
				{

				}
				
				
			}else
			{
				addIssue(new Issue(IssueTypes.FORM_RESPONSE_NOT_FOUND, "Valid StudyPatientFormResponseIdentifier is required to get form response")); 
				throw new OperationException(); 
			}		

			response.addAction(new CompletedAction(formResponseIdentifier, CRUDAction.UPDATE));
		}catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("FormResponseServiceImpl update", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("FormResponseServiceImpl update", t);
			throw new OperationException(t);
		}
		
		return response;
	}
	
	

	


}
