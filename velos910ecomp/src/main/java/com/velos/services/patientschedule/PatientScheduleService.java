package com.velos.services.patientschedule;

import java.util.ArrayList;

import javax.ejb.Remote;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.EventIdentifier;
import com.velos.services.model.EventStatus;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.PatientProtocolIdentifier;
import com.velos.services.model.PatientSchedule;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientSchedules;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.VisitIdentifier;
/**
 * Remote Interface declaring methods from service implementation 
 * @author Virendra
 *
 */
@Remote
public interface PatientScheduleService{
	/**
	 * 
	 * @param patientId
	 * @param studyIdentifier
	 * @return PatSchedule
	 * @throws OperationException
	 */
	
    public PatientSchedules getPatientScheduleList(PatientIdentifier patientId, StudyIdentifier studyIdentifier) throws OperationException;
    
    public PatientSchedule getPatientSchedule(PatientProtocolIdentifier scheduleOID) throws OperationException;
    
    public PatientSchedule getCurrentPatientSchedule(PatientIdentifier patientId, StudyIdentifier studyIdentifier) throws OperationException;
    
    public ResponseHolder addScheduleEventStatus(EventIdentifier eventIdentifier, EventStatus eventStatus) throws OperationException;
    }
