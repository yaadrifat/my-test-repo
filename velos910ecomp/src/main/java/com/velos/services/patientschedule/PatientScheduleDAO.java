package com.velos.services.patientschedule;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.business.site.impl.SiteBean;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.EJBUtil;
import com.velos.esch.business.common.SchCodeDao;
import com.velos.esch.business.eventdef.impl.EventdefBean;
import com.velos.services.Issue;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.Code;
import com.velos.services.model.Duration;
import com.velos.services.model.Event;
import com.velos.services.model.EventCRF;
import com.velos.services.model.EventCRFs;
import com.velos.services.model.EventIdentifier;
import com.velos.services.model.EventStatus;
import com.velos.services.model.Events;
import com.velos.services.model.FormIdentifier;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientProtocolIdentifier;
import com.velos.services.model.PatientSchedule;
import com.velos.services.model.PatientScheduleSummary;
import com.velos.services.model.PatientSchedules;
import com.velos.services.model.ScheduleEventIdentifier;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyPatient;
import com.velos.services.model.Visit;
import com.velos.services.model.VisitIdentifier;
import com.velos.services.model.Duration.TimeUnits;
import com.velos.services.model.VisitNameIdentifier;
import com.velos.services.model.Visits;
import com.velos.services.patientdemographics.PatientDemographicsDAO;
import com.velos.services.util.CodeCache;
import com.velos.services.util.ObjectLocator;
import com.velos.services.util.ServicesUtil;

/**
 * Data access object dealing with 
 * @author Virendra
 *
 */
public class PatientScheduleDAO extends CommonDAO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(PatientDemographicsDAO.class);	
	private static String getPatScheduleSql="Select distinct(p.PATPROT_START), p.PK_PATPROT, p.PATPROT_STAT," +
	                                        "e.name as PROTOCOL_NAME, pat.per_code as PERSON_CODE," +
                                            "st.STUDY_NUMBER from ER_PATPROT p, EVENT_ASSOC e, ER_PER pat, ER_STUDY st" +
	                                        " where p.fk_per = ? and p.fk_study = ? and p.fk_protocol=e.event_id" +
                                            " and pat.pk_per = p.fk_per and st.pk_study = p.fk_study";
	private static String getPatientScheduleSql="select  distinct(p.PATPROT_START), p.PATPROT_STAT, pat.per_code, pat.pk_per, site.pk_site, st.pk_study, st.study_number, " +
			                                    "(select e.name from event_assoc e where p.fk_protocol = e.event_id) as PROTOCOL_NAME, " + 
			                                    "v.pk_protocol_visit, v.VISIT_NAME, v.visit_no, v.win_before_number, v.win_before_unit, v.win_after_number, v.win_after_unit, " +
			                                    "s.event_id, e.name as EVENT_NAME, e.description, " +
			                                    "s.start_date_time, s.actual_schdate, " +
			                                    "TO_NUMBER(e.fuzzy_period) as EVENT_WIN_BEFORE, e.event_durationbefore, e.event_fuzzyafter as EVENT_WIN_AFTER, e.event_durationafter, " +
			                                    "code.codelst_desc as Event_Status, code.codelst_subtyp, " +
			                                    "s.service_site_id, (select site.site_name from er_site site where s.service_site_id = site.pk_site) as SITE_OF_SERVICE, " +
                                                "(select code.codelst_desc from sch_codelst code where s.fk_codelst_covertype = code.pk_codelst) as COVERAGE_DESC, " +
                                                "(select code.codelst_subtyp from sch_codelst code where s.fk_codelst_covertype = code.pk_codelst) as COVERAGE_CODE, " +
                                                "(select code.codelst_type from sch_codelst code where s.fk_codelst_covertype = code.pk_codelst) as COVERAGE_TYPE, " +
			                                    "c.fk_form, (Select f.form_name from er_formlib f where c.fk_form = f.pk_formlib) as FORM_NAME " +
			                                    "from er_patprot p, sch_events1 s, sch_protocol_visit v, event_assoc e, ESCH.sch_event_crf c, " + 
			                                    "er_per pat, er_site site, er_study st, sch_eventstat estat, sch_codelst code " +
			                                    "where p.pk_patprot = ? and p.pk_patprot = s.fk_patprot and s.fk_visit = v.pk_protocol_visit " +
			                                    "and s.fk_assoc = e.event_id and s.fk_assoc = c.fk_event(+) and p.fk_per = pat.pk_per " +
			                                    "and pat.fk_site = site.pk_site and p.fk_study = st.pk_study and s.event_id = estat.fk_event " +
			                                    "and estat.eventstat_enddt is null and estat.eventstat = code.pk_codelst order by v.visit_no, s.event_id";
	
	private static String getCurrentPatientScheduleSql="select  distinct(p.PATPROT_START), p.PATPROT_STAT, p.pk_patprot, pat.per_code, pat.pk_per, site.pk_site, st.pk_study, st.study_number, " +
                                                       "(select e.name from event_assoc e where p.fk_protocol = e.event_id) as PROTOCOL_NAME, " + 
                                                       "v.pk_protocol_visit, v.VISIT_NAME, v.visit_no, v.win_before_number, v.win_before_unit, v.win_after_number, v.win_after_unit, " +
                                                       "s.event_id, e.name as EVENT_NAME, e.description, " +
                                                       "s.start_date_time, s.actual_schdate, " +
                                                       "TO_NUMBER(e.fuzzy_period) as EVENT_WIN_BEFORE, e.event_durationbefore, e.event_fuzzyafter as EVENT_WIN_AFTER, e.event_durationafter, " +
                                                       "code.codelst_desc as Event_Status, code.codelst_subtyp, " +
                                                       "s.service_site_id, (select site.site_name from er_site site where s.service_site_id = site.pk_site) as SITE_OF_SERVICE, " +
                                                       "(select code.codelst_desc from sch_codelst code where s.fk_codelst_covertype = code.pk_codelst) as COVERAGE_DESC, " +
                                                       "(select code.codelst_subtyp from sch_codelst code where s.fk_codelst_covertype = code.pk_codelst) as COVERAGE_CODE, " +
                                                       "(select code.codelst_type from sch_codelst code where s.fk_codelst_covertype = code.pk_codelst) as COVERAGE_TYPE, " +
                                                       "c.fk_form, (Select f.form_name from er_formlib f where c.fk_form = f.pk_formlib) as FORM_NAME " +
                                                       "from er_patprot p, sch_events1 s, sch_protocol_visit v, event_assoc e, ESCH.sch_event_crf c, " + 
                                                       "er_per pat, er_site site, er_study st, sch_eventstat estat, sch_codelst code " +
                                                       "where p.fk_per = ? and p.fk_study = ? and p.patprot_stat = 1 and p.pk_patprot = s.fk_patprot and s.fk_visit = v.pk_protocol_visit " +
                                                       "and s.fk_assoc = e.event_id and s.fk_assoc = c.fk_event(+) and p.fk_per = pat.pk_per " +
                                                       "and pat.fk_site = site.pk_site and p.fk_study = st.pk_study and s.event_id = estat.fk_event " +
                                                       "and estat.eventstat_enddt is null and estat.eventstat = code.pk_codelst order by v.visit_no, s.event_id";
	
	protected static ResponseHolder response = new ResponseHolder();
	Issue issue = new Issue();
	SchCodeDao schCodeDao = new SchCodeDao(); 
	//EventStatus evenStatus = new EventStatus();
	static String siteName = "";
	/**
	 * 
	 * @param personPk
	 * @param studyPk
	 * @return
	 * @throws OperationException
	 */
	
	  public enum WindowUnit {

		    H,
		    D,
		    W,
		    M,
		    Y
		  }
	
	public static PatientSchedules getPatientScheduleList(int personPk, int studyPk) throws OperationException {
		
		
		PreparedStatement pstmt = null;
		Connection conn = null;
		PatientSchedules patSchedules = new PatientSchedules();
		
	//	ArrayList<Integer> lstPatProtPK = new ArrayList<Integer>();
	//	ArrayList<PatientSchedule> lstPatientSchedule = new ArrayList<PatientSchedule>();
		
		try{
			
			conn = getConnection();

			if (logger.isDebugEnabled()) logger.debug(" sql:" + getPatScheduleSql);
			
			pstmt = conn.prepareStatement(getPatScheduleSql);
			pstmt.setInt(1, personPk);
			pstmt.setInt(2, studyPk);
			ResultSet rs = pstmt.executeQuery();
			ObjectMapService objectMapService = ServicesUtil.getObjectMapService(); 
			int ctr = 0; 
			
			while (rs.next()) {
				
				if(ctr == 0)
				{
					PatientIdentifier patientIdentifier = new PatientIdentifier();
					//create or get person object map for studyPatient
	
					ObjectMap obj = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PERSON, personPk);
					String strPerOID = obj.getOID();
					
					patientIdentifier.setPatientId(rs.getString("PERSON_CODE"));
					patientIdentifier.setOID(strPerOID);
					patSchedules.setPatientIdentifier(patientIdentifier);
					
					
					StudyIdentifier studyIdentifier = new StudyIdentifier();
					
					ObjectMap objMapStudy = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY, studyPk);
					String strStudyOID = objMapStudy.getOID();
					studyIdentifier.setStudyNumber(rs.getString("STUDY_NUMBER"));
					studyIdentifier.setOID(strStudyOID);
					patSchedules.setStudyIdentifier(studyIdentifier);
				}
				
				ctr++; 
				//Fixed:# 6059, reopened issue. initial assign null
				//and instantiate if schedule exists.
				PatientScheduleSummary patSchedule = new PatientScheduleSummary();
				
				StudyPatient studyPatient = new StudyPatient();
			
				
				//studyPatient.setStudyPatEnrollDate(rs.getDate("PATPROT_START"));
				//Viendra:#6061, for more than one schedule names
				patSchedule.setCalendarName(rs.getString("PROTOCOL_NAME")+","+(rs.getDate("PATPROT_START").toString()));
				patSchedule.setStartDate(rs.getDate("PATPROT_START"));
				patSchedule.setCurrent(rs.getString("PATPROT_STAT").equals("1") ? true : false);
				ObjectMap mapScheduleOID = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PATIENT_SCHEDULE, rs.getInt("PK_PATPROT"));
				PatientProtocolIdentifier patProtID = new PatientProtocolIdentifier();
				patProtID.setOID(mapScheduleOID.getOID());
				patSchedule.setScheduleIdentifier(patProtID);
				patSchedules.addPatientSchedule(patSchedule);
		
			}
			
			return patSchedules;
		}
		catch(Throwable t){
	
			t.printStackTrace();
			
			throw new OperationException();
			
		}
		finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}
		
	}
	
	
	public static PatientSchedule getPatientSchedule(int patProtPK) throws OperationException{
		
		PreparedStatement pstmt = null;
		Connection conn = null;
		PatientSchedule patSchedule = null;
		ObjectMapService objectMapService = ServicesUtil.getObjectMapService();
		
		try{
			
			conn = getConnection();
			if(logger.isDebugEnabled()) logger.debug("sql:" + "getPatientScheduleSql");
			
			pstmt = conn.prepareStatement(getPatientScheduleSql);
			pstmt.setInt(1, patProtPK);
			ResultSet rs = pstmt.executeQuery();
			Visits visits = new Visits();
			Visit visit = new Visit();
			Events events = new Events();
			Event event = new Event(); 
			PatientScheduleSummary patientScheduleSummary = new PatientScheduleSummary();
			patSchedule = new PatientSchedule();
			PatientProtocolIdentifier patientProtocolIdentifier = new PatientProtocolIdentifier();
			PatientIdentifier patientIdentifier = new PatientIdentifier();
			StudyIdentifier studyIdentifier = new StudyIdentifier();
			ObjectMap mapOID = objectMapService.getOrCreateObjectMapFromPK(objectMapService.PERSISTENCE_UNIT_PATIENT_SCHEDULE, patProtPK); 
			patientProtocolIdentifier.setOID(mapOID.getOID());
			int ctr = 0;
			int visitCtr = 0;
			int curEventPK = 0;
			int visitWinBefore = 0;
			int visitWinAfter = 0;
			int eventWinAfter = 0;
			int eventWinBefore = 0;
            String scheduleDate = "";
            String suggestedDate = "";
			int visitFlag = 0;//Bug Fix : 12511 & 12407
			String visitWinBeforeUnit = "";
			String visitWinAfterUnit = "";
			String eventWinBeforeUnit = "";
			String eventWinAfterUnit = "";
            List<String> visitScheduleList = new ArrayList<String>();
            List<String> visitSuggList = new ArrayList<String>();
			while(rs.next()){

				VisitIdentifier visitIdentifier = new VisitIdentifier();
				if ((visitCtr == 0 || rs.getInt("PK_PROTOCOL_VISIT") != visitCtr)){
					visitSuggList = new ArrayList<String>();
					visitScheduleList = new ArrayList<String>();
					if(visitFlag > 0){//Bug Fix : 12511 & 12407
			            visit.setEvents(events);
			            visits.addVisit(visit);
					}
					visitFlag++;
					visit = new Visit();
					events = new Events();
					curEventPK = 0; 
					visitCtr = rs.getInt("PK_PROTOCOL_VISIT");
					visit.setVisitName(rs.getString("VISIT_NAME"));
					ObjectMap mapVisitOID = objectMapService.getOrCreateObjectMapFromPK(objectMapService.PERSISTENCE_UNIT_VISIT, rs.getInt("PK_PROTOCOL_VISIT"));
					visitIdentifier.setOID(mapVisitOID.getOID());
					visit.setVisitIdentifier(visitIdentifier);
					visitWinBefore = rs.getInt("WIN_BEFORE_NUMBER");
					visitWinAfter = rs.getInt("WIN_AFTER_NUMBER");
					if((visitWinBefore != 0) || (visitWinAfter != 0)){
						visitWinBeforeUnit = rs.getString("WIN_BEFORE_UNIT");
						visitWinAfterUnit = rs.getString("WIN_AFTER_UNIT");
						Calendar currBefore = Calendar.getInstance();
						Calendar currAfter = Calendar.getInstance();
						currBefore.setTime(rs.getDate("START_DATE_TIME"));
						currAfter.setTime(rs.getDate("START_DATE_TIME"));
						visit.setVisitWindow(PatientScheduleDAO.dateWindow(visitWinBeforeUnit, currBefore, visitWinBefore, visitWinAfterUnit, currAfter, visitWinAfter));
					}
			        
				}
			
				if(curEventPK == 0 || rs.getInt("EVENT_ID") != curEventPK)
				{
					curEventPK = rs.getInt("EVENT_ID"); 
					event = new Event();
					EventStatus eventStatus = new EventStatus();
					Code eventStatusCode = new Code();
					Code coverageType = new Code();
					ScheduleEventIdentifier scheduleEventIdentifier = new ScheduleEventIdentifier();
					OrganizationIdentifier organizationIdentifier = new OrganizationIdentifier();
				
					EventCRFs eventCRFs = new EventCRFs();
					if(rs.getString("ACTUAL_SCHDATE") != null) visitScheduleList.add(rs.getString("ACTUAL_SCHDATE"));//Bug Fix : 12406 && 12431
					if(rs.getString("ACTUAL_SCHDATE") != null) visitSuggList.add(rs.getString("START_DATE_TIME"));//BUg Fix : 12406 && 12431
					event.setEventName(rs.getString("EVENT_NAME"));
					event.setDescription(rs.getString("DESCRIPTION"));
					if(rs.getString("ACTUAL_SCHDATE") != null){
						event.setEventSuggestedDate(rs.getString("START_DATE_TIME"));
						event.setEventScheduledDate(rs.getString("ACTUAL_SCHDATE"));
					}
					else{
						event.setEventSuggestedDate("Not Defined");
						event.setEventScheduledDate("Not Defined");
					}
					eventWinBefore = rs.getInt("EVENT_WIN_BEFORE");
					eventWinAfter = rs.getInt("EVENT_WIN_AFTER");
					if((eventWinBefore != 0) || (eventWinAfter != 0)){
						eventWinBeforeUnit = rs.getString("EVENT_DURATIONBEFORE");
						eventWinAfterUnit = rs.getString("EVENT_DURATIONAFTER");
						Calendar currBefore = Calendar.getInstance();
						Calendar currAfter = Calendar.getInstance();
						currBefore.setTime(rs.getDate("START_DATE_TIME"));
						currAfter.setTime(rs.getDate("START_DATE_TIME"));
						System.out.println("currBefore value while calling the case :::" + currBefore.getTime());
						event.setEventWindow(PatientScheduleDAO.dateWindow(eventWinBeforeUnit, currBefore, eventWinBefore, eventWinAfterUnit, currAfter, eventWinAfter));
					}
					eventStatusCode.setCode(rs.getString("CODELST_SUBTYP"));
					eventStatusCode.setDescription(rs.getString("EVENT_STATUS"));
					eventStatus.setEventStatusCode(eventStatusCode);
					event.setEventStatus(eventStatus);
					//Bug Fix : 12367
					if(rs.getInt("SERVICE_SITE_ID") > 0){
						ObjectMap mapSiteOID = objectMapService.getOrCreateObjectMapFromPK(objectMapService.PERSISTENCE_UNIT_ORGANIZATION, rs.getInt("SERVICE_SITE_ID"));
						organizationIdentifier.setOID(mapSiteOID.getOID());
						organizationIdentifier.setSiteName(rs.getString("SITE_OF_SERVICE"));
						event.setSiteOfService(organizationIdentifier);
					}
					coverageType.setCode(rs.getString("COVERAGE_CODE"));
					coverageType.setDescription(rs.getString("COVERAGE_DESC"));
					coverageType.setType(rs.getString("COVERAGE_TYPE"));
					event.setCoverageType(coverageType);
					ObjectMap mapEventOID = objectMapService.getOrCreateObjectMapFromPK(objectMapService.PERSISTENCE_UNIT_EVENT, rs.getInt("EVENT_ID"));
					scheduleEventIdentifier.setOID(mapEventOID.getOID());
					event.setScheduleEventIdentifier(scheduleEventIdentifier);
					event.setEventCRFs(eventCRFs);
					events.addEvent(event);

				}
				
				
				if(rs.getInt("FK_FORM") != 0){
					FormIdentifier formIdentifier = new FormIdentifier();
					EventCRF eventCRF = new EventCRF();
					ObjectMap mapFormOID = objectMapService.getOrCreateObjectMapFromPK(objectMapService.PERSISTENCE_UNIT_FORM_LIBRARY, rs.getInt("FK_FORM"));
					formIdentifier.setOID(mapFormOID.getOID());
					eventCRF.setFormIdentifier(formIdentifier);
					eventCRF.setFormName(rs.getString("FORM_NAME"));
					event.getEventCRFs().addEventCRF(eventCRF);
				
				}

				if (ctr == 0){
					OrganizationIdentifier organizationIdentifier = new OrganizationIdentifier();
					patientScheduleSummary.setStartDate(rs.getDate("PATPROT_START"));
					patientScheduleSummary.setCalendarName(rs.getString("PROTOCOL_NAME") + "," + rs.getString("PATPROT_START").toString());
					patientScheduleSummary.setCurrent(rs.getString("PATPROT_STAT").equals("1") ? true:false);
					patientScheduleSummary.setScheduleIdentifier(patientProtocolIdentifier);
					patSchedule.setPatientScheduleSummary(patientScheduleSummary);
					ObjectMap mapPatOID = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PERSON, rs.getInt("PK_PER"));
					ObjectMap mapOrgOID = objectMapService.getOrCreateObjectMapFromPK(objectMapService.PERSISTENCE_UNIT_ORGANIZATION, rs.getInt("PK_SITE"));
					patientIdentifier.setPatientId(rs.getString("PER_CODE"));
					patientIdentifier.setOID(mapPatOID.getOID());
					organizationIdentifier.setOID(mapOrgOID.getOID());
					patientIdentifier.setOrganizationId(organizationIdentifier);
					patSchedule.setPatientIdentifier(patientIdentifier);
					
					ObjectMap mapStudyOID = objectMapService.getOrCreateObjectMapFromPK(objectMapService.PERSISTENCE_UNIT_STUDY, rs.getInt("PK_STUDY"));
					studyIdentifier.setStudyNumber(rs.getString("STUDY_NUMBER"));
					studyIdentifier.setOID(mapStudyOID.getOID());
					patSchedule.setStudyIdentifier(studyIdentifier);
					
					
				}
				ctr++;			
				//BUg Fix : 12406 && 12431
				if(visitScheduleList.isEmpty())
				{
					suggestedDate = "NOT DEFINED";
					scheduleDate = "NOT DEFINED";
						
				}else{
					suggestedDate = Collections.min(visitSuggList);
					scheduleDate = Collections.min(visitScheduleList);
				}
				visit.setVisitSuggestedDate(suggestedDate);
				visit.setVisitScheduledDate(scheduleDate);//Bug Fix : 12406
			}
			
			//Bug Fix : 12511 & 12407
			if(visitFlag > 0){

				visit.setEvents(events);
	            visits.addVisit(visit);
				patSchedule.setVisits(visits);
			}
			return patSchedule;
			
		}catch(Throwable t){
			
			t.printStackTrace();
			
			throw new OperationException();
		}
		
		finally{
			try{
				if(pstmt!=null){
					pstmt.close();
				}
			}catch(Exception e){
				
			}
			try{
				if(conn!=null){
					conn.close();
				}
			}catch(Exception e){
				
			}
		}
	
		
	}
	
	
	public static PatientSchedule getCurrentPatientSchedule(int personPK, int studyPK) throws OperationException{
		
		PreparedStatement pstmt = null;
		Connection conn = null;
		PatientSchedule patSchedule = null;
		ObjectMapService objectMapService = ServicesUtil.getObjectMapService();
		
		try{
			
			conn = getConnection();
			if(logger.isDebugEnabled()) logger.debug("sql:" + "getCurrentPatientScheduleSql");
			
			pstmt = conn.prepareStatement(getCurrentPatientScheduleSql);
			pstmt.setInt(1, personPK);
			pstmt.setInt(2, studyPK);
			ResultSet rs = pstmt.executeQuery();
			Visits visits = new Visits();
			Visit visit = new Visit();
			Events events = new Events();
			Event event = new Event(); 
			PatientScheduleSummary patientScheduleSummary = new PatientScheduleSummary();
			patSchedule = new PatientSchedule();
			PatientProtocolIdentifier patientProtocolIdentifier = new PatientProtocolIdentifier();
			PatientIdentifier patientIdentifier = new PatientIdentifier();
			StudyIdentifier studyIdentifier = new StudyIdentifier();
          
			int ctr = 0;
			int visitCtr = 0;
			int curEventPK = 0;
			int visitWinBefore = 0;
			int visitWinAfter = 0;
			int eventWinAfter = 0;
			int eventWinBefore = 0;
            String scheduleDate = "";
            String suggestedDate = "";
            int visitFlag = 0;//Bug Fix : 12511 & 12407
			String visitWinBeforeUnit = "";
			String visitWinAfterUnit = "";
			String eventWinBeforeUnit = "";
			String eventWinAfterUnit = "";
            List<String> visitScheduleList = new ArrayList<String>();
            List<String> visitSuggList = new ArrayList<String>();
			while(rs.next()){
                int rsval = rs.getInt("PK_PROTOCOL_VISIT");
				VisitIdentifier visitIdentifier = new VisitIdentifier();
				if ((visitCtr == 0 || rs.getInt("PK_PROTOCOL_VISIT") != visitCtr)){
					visitSuggList = new ArrayList<String>();
					visitScheduleList = new ArrayList<String>();
					if(visitFlag > 0){//Bug Fix : 12511 & 12407
			            visit.setEvents(events);
			            visits.addVisit(visit);
					}
					visitFlag++;
					visit = new Visit();
					events = new Events();
					curEventPK = 0; 
					visitCtr = rs.getInt("PK_PROTOCOL_VISIT");
					visit.setVisitName(rs.getString("VISIT_NAME"));
					ObjectMap mapVisitOID = objectMapService.getOrCreateObjectMapFromPK(objectMapService.PERSISTENCE_UNIT_VISIT, rs.getInt("PK_PROTOCOL_VISIT"));
					visitIdentifier.setOID(mapVisitOID.getOID());
					visit.setVisitIdentifier(visitIdentifier);
					visitWinBefore = rs.getInt("WIN_BEFORE_NUMBER");
					visitWinAfter = rs.getInt("WIN_AFTER_NUMBER");
					if((visitWinBefore != 0) || (visitWinAfter != 0)){
						visitWinBeforeUnit = rs.getString("WIN_BEFORE_UNIT");
						visitWinAfterUnit = rs.getString("WIN_AFTER_UNIT");
						Calendar currBefore = Calendar.getInstance();
						Calendar currAfter = Calendar.getInstance();
						currBefore.setTime(rs.getDate("START_DATE_TIME"));
						currAfter.setTime(rs.getDate("START_DATE_TIME"));
						visit.setVisitWindow(PatientScheduleDAO.dateWindow(visitWinBeforeUnit, currBefore, visitWinBefore, visitWinAfterUnit, currAfter, visitWinAfter));
					}
	
				}

				if(curEventPK == 0 || rs.getInt("EVENT_ID") != curEventPK)
				{
					curEventPK = rs.getInt("EVENT_ID"); 
					event = new Event();
					EventStatus eventStatus = new EventStatus();
					Code eventStatusCode = new Code();
					Code coverageType = new Code();
					ScheduleEventIdentifier scheduleEventIdentifier = new ScheduleEventIdentifier();
					OrganizationIdentifier organizationIdentifier = new OrganizationIdentifier();//Bug Fix : 12402
				
					EventCRFs eventCRFs = new EventCRFs();
					if(rs.getString("ACTUAL_SCHDATE") != null) visitScheduleList.add(rs.getString("ACTUAL_SCHDATE"));//Bug Fix : 12406 && 12427
					if(rs.getString("ACTUAL_SCHDATE") != null) visitSuggList.add(rs.getString("START_DATE_TIME"));//BUg Fix : 12406 && 12427
					event.setEventName(rs.getString("EVENT_NAME"));
					event.setDescription(rs.getString("DESCRIPTION"));
					if(rs.getString("ACTUAL_SCHDATE") != null){
						event.setEventSuggestedDate(rs.getString("START_DATE_TIME"));
						event.setEventScheduledDate(rs.getString("ACTUAL_SCHDATE"));
					}
					else{
						event.setEventSuggestedDate("Not Defined");
						event.setEventScheduledDate("Not Defined");
					}
					eventWinBefore = rs.getInt("EVENT_WIN_BEFORE");
					eventWinAfter = rs.getInt("EVENT_WIN_AFTER");
					if((eventWinBefore != 0) || (eventWinAfter != 0)){
						eventWinBeforeUnit = rs.getString("EVENT_DURATIONBEFORE");
						eventWinAfterUnit = rs.getString("EVENT_DURATIONAFTER");
						Calendar currBefore = Calendar.getInstance();
						Calendar currAfter = Calendar.getInstance();
						currBefore.setTime(rs.getDate("START_DATE_TIME"));
						currAfter.setTime(rs.getDate("START_DATE_TIME"));
						event.setEventWindow(PatientScheduleDAO.dateWindow(eventWinBeforeUnit, currBefore, eventWinBefore, eventWinAfterUnit, currAfter, eventWinAfter));
					}
					eventStatusCode.setCode(rs.getString("CODELST_SUBTYP"));
					eventStatusCode.setDescription(rs.getString("EVENT_STATUS"));
					eventStatus.setEventStatusCode(eventStatusCode);
					event.setEventStatus(eventStatus);
					if(rs.getInt("SERVICE_SITE_ID") > 0){
						ObjectMap mapSiteOID = objectMapService.getOrCreateObjectMapFromPK(objectMapService.PERSISTENCE_UNIT_ORGANIZATION, rs.getInt("SERVICE_SITE_ID"));
						organizationIdentifier.setOID(mapSiteOID.getOID());
						organizationIdentifier.setSiteName(rs.getString("SITE_OF_SERVICE"));
						event.setSiteOfService(organizationIdentifier);
					}
					coverageType.setCode(rs.getString("COVERAGE_CODE"));
					coverageType.setDescription(rs.getString("COVERAGE_DESC"));
					coverageType.setType(rs.getString("COVERAGE_TYPE"));
					event.setCoverageType(coverageType);
					ObjectMap mapEventOID = objectMapService.getOrCreateObjectMapFromPK(objectMapService.PERSISTENCE_UNIT_EVENT, rs.getInt("EVENT_ID"));
					scheduleEventIdentifier.setOID(mapEventOID.getOID());
					event.setScheduleEventIdentifier(scheduleEventIdentifier);
					event.setEventCRFs(eventCRFs);
					events.addEvent(event);

				}
				
				
				if(rs.getInt("FK_FORM") != 0){
					FormIdentifier formIdentifier = new FormIdentifier();
					EventCRF eventCRF = new EventCRF();
					ObjectMap mapFormOID = objectMapService.getOrCreateObjectMapFromPK(objectMapService.PERSISTENCE_UNIT_FORM_LIBRARY, rs.getInt("FK_FORM"));
					formIdentifier.setOID(mapFormOID.getOID());
					eventCRF.setFormIdentifier(formIdentifier);
					eventCRF.setFormName(rs.getString("FORM_NAME"));
					event.getEventCRFs().addEventCRF(eventCRF);
				
				}
		
				if (ctr == 0){
					OrganizationIdentifier organizationIdentifier = new OrganizationIdentifier();//Bug Fix : 12402
					ObjectMap mapOID = objectMapService.getOrCreateObjectMapFromPK(objectMapService.PERSISTENCE_UNIT_PATIENT_SCHEDULE, rs.getInt("PK_PATPROT"));
					patientProtocolIdentifier.setOID(mapOID.getOID());
					patientScheduleSummary.setStartDate(rs.getDate("PATPROT_START"));
					patientScheduleSummary.setCalendarName(rs.getString("PROTOCOL_NAME") + "," + rs.getString("PATPROT_START").toString());
					patientScheduleSummary.setCurrent(rs.getString("PATPROT_STAT").equals("1") ? true:false);
					patientScheduleSummary.setScheduleIdentifier(patientProtocolIdentifier);
					patSchedule.setPatientScheduleSummary(patientScheduleSummary);
					ObjectMap mapPatOID = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PERSON, rs.getInt("PK_PER"));
					ObjectMap mapOrgOID = objectMapService.getOrCreateObjectMapFromPK(objectMapService.PERSISTENCE_UNIT_ORGANIZATION, rs.getInt("PK_SITE"));
					patientIdentifier.setPatientId(rs.getString("PER_CODE"));
					patientIdentifier.setOID(mapPatOID.getOID());
					organizationIdentifier.setOID(mapOrgOID.getOID());
					patientIdentifier.setOrganizationId(organizationIdentifier);
					patSchedule.setPatientIdentifier(patientIdentifier);
					
					ObjectMap mapStudyOID = objectMapService.getOrCreateObjectMapFromPK(objectMapService.PERSISTENCE_UNIT_STUDY, rs.getInt("PK_STUDY"));
					studyIdentifier.setStudyNumber(rs.getString("STUDY_NUMBER"));
					studyIdentifier.setOID(mapStudyOID.getOID());
					patSchedule.setStudyIdentifier(studyIdentifier);
					
					
				}
				ctr++;
				//BUg Fix : 12406 && 12427
				if(visitScheduleList.isEmpty())
				{
					suggestedDate = "NOT DEFINED";
					scheduleDate = "NOT DEFINED";
						
				}else{
					suggestedDate = Collections.min(visitSuggList);
					scheduleDate = Collections.min(visitScheduleList);
				}
				visit.setVisitSuggestedDate(suggestedDate);
				visit.setVisitScheduledDate(scheduleDate);//Bug Fix : 12406
			}
			//Bug Fix : 12511 & 12407
			if(visitFlag > 0){

				visit.setEvents(events);
	            visits.addVisit(visit);
				patSchedule.setVisits(visits);
			}
			return patSchedule;
			
		}catch(Throwable t){
			
			t.printStackTrace();
			
			throw new OperationException();
		}
		
		finally{
			try{
				if(pstmt!=null){
					pstmt.close();
				}
			}catch(Exception e){
				
			}
			try{
				if(conn!=null){
					conn.close();
				}
			}catch(Exception e){
				
			}
		}
	
		
	}
	
	
	/**
	 * 
	 * @param personPk
	 * @param StudyPk
	 * @return
	 * @throws OperationException
	 */
	public static ArrayList<Integer> getVisitPkPatProtPk(int personPK, int StudyPK, int patProtPK) throws OperationException{
		PreparedStatement pstmt = null;
	    Connection conn = null;
	    String sql = "";
	   
	    ArrayList<Integer> lstVisitPk = new ArrayList<Integer>();
	    try {
	    	conn = getConnection();
	
	        sql= "select distinct(ev1.fk_visit),p.pk_patprot,p.fk_protocol,ev.name as protocol_name, " +
	        		"p.patprot_start patprot_start, p.fk_study " +
	        		"from er_patprot p, event_assoc ev,SCH_EVENTS1 ev1 " +
	        		"where p.fk_study = ?  and  p.fk_per = ? and p.pk_patprot= ? and ev.event_id = p.fk_protocol " +
	        		"and ev1.fk_patprot = p.pk_patprot and ev.event_type = 'P'" +
	        		" order by pk_patprot desc";	
		          pstmt = conn.prepareStatement(sql);
		          pstmt.setInt(1, StudyPK);                      
		          pstmt.setInt(2, personPK);  
		          pstmt.setInt(3, patProtPK); 
	        ResultSet rs = pstmt.executeQuery();
	        
	          while (rs.next()) {
	          	
	          	Integer VisitPk = rs.getInt("fk_visit");
	          	lstVisitPk.add(VisitPk);
	          	
	          }
	          return lstVisitPk;
	          
	    } 
	    catch(Throwable t){
	    	t.printStackTrace();
	    	throw new OperationException();
	}
	finally {
		try {
			if (pstmt != null)
				pstmt.close();
		} catch (Exception e) {
		}
		try {
			if (conn != null)
				conn.close();
			} catch (Exception e) {
		}
	    } 
	
	}
	/**
	 * 
	 * @param VisitPk
	 * @param patProtPk
	 * @param userAccountPk
	 * @return
//	 * @throws OperationException
//	 */
//	public Visit getVisitsEvents(int visitPk, int patProtPk, int userAccountPk) throws OperationException{
//		PreparedStatement pstmt = null;
//	    Connection conn = null;
//	    Visit visit = new Visit();
//	    VisitIdentifier visitIdentifier = new VisitIdentifier();
//	    ArrayList<Event> lstEvent = new ArrayList<Event>();
//	    CodeCache codeCache = CodeCache.getInstance();
//	    
//	    ObjectMapService objectMapService = ServicesUtil.getObjectMapService(); 
//	  	ObjectMap obj2 = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_VISIT, visitPk);
//	  	String OID = obj2.getOID();
//	  	visitIdentifier.setOID(OID);
//	  	visit.setVisitIdentifier(visitIdentifier);
//	    
//	    try {
//	    	conn = getConnection();
//	
//	    	String scheduleSql=   "select ev.DESCRIPTION, to_char(START_DATE_TIME, 'yyyy-mm-dd') START_DATE_TIME, "
//				+ " nvl(evass.FUZZY_PERIOD,0) FUZZY_PERIOD ,"
//				+ " evass.EVENT_DURATIONBEFORE,evass.EVENT_FUZZYAFTER,evass.EVENT_DURATIONAFTER,"
//				+ " ev.EVENT_ID,estat.PK_EVENTSTAT, estat.EVENTSTAT_DT, estat.EVENTSTAT_NOTES,"//EVENTSTAT_DT,ev.SERVICE_SITE_ID,ev.REASON_FOR_COVERAGECHANGE,EVENTSTAT_NOTES,,
//				+ " ev.ISCONFIRMED, "
//				+ " ev.FK_ASSOC, "
//				+ " ev.NOTES, "
//				+ "	 to_char(ev.EVENT_EXEON, PKG_DATEUTIL.F_GET_DATEFORMAT) as EVENT_EXEON, "
//				+ "	to_char(ev.ACTUAL_SCHDATE, 'yyyy-mm-dd')  ACTUAL_SCHDATE, "
//				+ "	ev.EVENT_EXEBY, "
//				+ "	ev.ADVERSE_COUNT, "
//				+ " ev.visit,vis.pk_protocol_visit, "
//				+ " vis.visit_name," 
//				+ " vis.win_after_unit, vis.win_after_number, vis.win_before_unit,vis.win_before_number," 
//				+ "ev.fk_visit, "
//				+ " ev.status,ev.event_notes, (select count(*) from sch_event_kit where fk_event = ev.fk_assoc) kitCount ,pkg_gensch.f_get_event_roles(ev.fk_assoc) event_roles, vis.no_interval_flag interval_flag,  "
//				+ " ev.SERVICE_SITE_ID,ev.FACILITY_ID,ev.FK_CODELST_COVERTYPE, ev.REASON_FOR_COVERAGECHANGE "
//				+ " FROM SCH_EVENTS1 ev"
//				+ " , SCH_PROTOCOL_VISIT vis"
//				+ " , EVENT_ASSOC evass, SCH_EVENTSTAT estat"
//				+ " WHERE  ev.fk_patprot = ?" 
//				+ " and ev.fk_visit = ?" 
//				+ " and ev.fk_visit = vis.pk_protocol_visit "
//				+ " and evass.EVENT_ID = ev.FK_ASSOC and estat.FK_EVENT = ev.EVENT_ID "
//				//Virendra:Fixed #6117, modified query for event with latest event status
//				+ " and estat.pk_eventstat = (select max(estat1.pk_eventstat) "
//				+ "from sch_eventstat estat1 where estat1.fk_event = ev.event_id ) ";
//	    	
//		          pstmt = conn.prepareStatement(scheduleSql);
//		          pstmt.setInt(1, patProtPk);                      
//		          pstmt.setInt(2, visitPk);             
//	        ResultSet rs = pstmt.executeQuery();
//	        
//	          while (rs.next()) {
//	        	  
//	        	 
//	        	  
//	        	  visit.setVisitName(rs.getString("VISIT_NAME"));
//	        	  String suggestedStartDate = rs.getString("START_DATE_TIME");
//	        	  String actualScheduleDate =rs.getString("ACTUAL_SCHDATE");
//	        	  visit.setVisitSuggestedDate(suggestedStartDate);
//	        	  visit.setVisitScheduledDate(actualScheduleDate);
//	        	  //VISIT WINDOW
//	        	  Integer visitWinBeforeNumber = rs.getInt("WIN_BEFORE_NUMBER");
//	        	  String visitWinBeforeUnit = rs.getString("WIN_BEFORE_UNIT");
//	        	  Integer visitWinAfterNumber = rs.getInt("WIN_AFTER_NUMBER");
//	        	  String visitWinAfterUnit = rs.getString("WIN_AFTER_UNIT");
//	        	  
//	        	  StringBuffer visitWindowSB = new StringBuffer();
//	        	  String visitWindow = null;
//	        	 
//	        	  if (visitWinBeforeNumber != null && visitWinBeforeNumber != 0 &&
//	        			  visitWinBeforeUnit != null && visitWinBeforeUnit.length() > 0 && actualScheduleDate != null){
//		        	  Date beginDate = calculateDate(actualScheduleDate, -1*visitWinBeforeNumber,
//		        			  visitWinBeforeUnit);
//		        	  visitWindowSB.append(DateUtil.dateToString(beginDate)).append("~");
//	        	  }
//	        	  else{
//	        		  visitWindowSB = null;
//	        	  }
//	        	  if (visitWinAfterNumber != null && visitWinAfterNumber != 0 &&
//	        			  visitWinAfterUnit != null && visitWinAfterUnit.length() > 0 && actualScheduleDate != null){
//		        	  String endDate = calculateDate(actualScheduleDate, visitWinAfterNumber,
//		        			  visitWinAfterUnit);
//		        	  visitWindowSB.append(DateUtil.dateToString(endDate));
//	        	  }
//	        	  else{
//	        		  visitWindowSB = null;
//	        	  }
//	        	  if(visitWindowSB != null){
//		        	  visitWindow = visitWindowSB.toString();
//		        	  visit.setVisitWindow(visitWindow);
//	        	  }
//	        	  //VISIT WINDOW END
//	        	  ObjectMap obj = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_EVENT, rs.getInt("EVENT_ID"));
////	        	  String strOID = obj.getOID();
//	        	  Event patEvent = new Event();
//	        	  ScheduleEventIdentifier eventId = new ScheduleEventIdentifier();
//	        	  eventId.setOID(obj.getOID());
//	        	  patEvent.setScheduleEventIdentifier(eventId);
//	        	  
//	        	  patEvent.setEventSuggestedDate(suggestedStartDate);
//	        	  patEvent.setEventScheduledDate(actualScheduleDate);
//	        	  
//	        	  //setting eventstatus
//	        	
//	        	  /*
//	        	   * Code studyStatusCodeType = 
//						codeCache.getCodeSubTypeByPK(
//								CodeCache.CODE_TYPE_STATUS_TYPE, 
//								rs.getInt("STATUS_TYPE"),
//								userAccountPK1);
//	        	   * 
//	        	   * 
//	        	   * 
//	        	   * */
//	        	  EventStatus eventStatus = new EventStatus();
//	        	  Integer eventStatusPk = rs.getInt("ISCONFIRMED");
//	        	  Code eventStatusCode = 
//						codeCache.getSchCodeSubTypeByPK(
//								CodeCache.CODE_TYPE_EVENT_STATUS_TYPE, 
//								eventStatusPk,
//								userAccountPk);
//	        	  
//	        	  Code coverageTypeCode = 
//						codeCache.getSchCodeSubTypeByPK(
//								CodeCache.CODE_TYPE_COVERAGE_TYPE, 
//								rs.getInt("FK_CODELST_COVERTYPE"),
//								userAccountPk);
//	        	  
//	        	  //Virendra: Fixed#6118
//	        	  String reasonForChangeCoverType = rs.getString("REASON_FOR_COVERAGECHANGE");
//	        	  
//	        	  Date eventStatusDateValidFrom = rs.getDate("EVENTSTAT_DT");
//	        	  
//	        	  String eventStatusNotes = rs.getString("EVENTSTAT_NOTES");
//	        	  
////	        	  OrganizationIdentifier organizationIdService = new OrganizationIdentifier();
////	        	  Integer serviceSitePK = rs.getInt("SERVICE_SITE_ID");
//	        	  
//	        	  OrganizationIdentifier orgIdentifier = new OrganizationIdentifier();
//	        	  orgIdentifier.setSiteName(siteName);
//	        	  
//	        	  
//	        	  
//	        	  if(eventStatusCode != null){
//	        		  eventStatus.setEventStatusCode(eventStatusCode);
//	        	  }
//	        	  if(coverageTypeCode != null){
//	        		  eventStatus.setCoverageType(coverageTypeCode);
//	        	  }
//	        	 
//	        	  eventStatus.setStatusValidFrom(eventStatusDateValidFrom);
//	        	  eventStatus.setSiteOfService(orgIdentifier);
//	        	  eventStatus.setReasonForChangeCoverType(reasonForChangeCoverType);
//	        	  eventStatus.setNotes(eventStatusNotes);
//	        	  
//	        	  patEvent.setEventStatus(eventStatus);
//	        	
//	        	  
//	        	  patEvent.setDescription(rs.getString("DESCRIPTION"));
//	        	  Integer winBeforeNumber = rs.getInt("FUZZY_PERIOD");
//	        	  String winBeforeUnit = rs.getString("EVENT_DURATIONBEFORE");
//	        	  Integer winAfterNumber = rs.getInt("EVENT_FUZZYAFTER");
//	        	  String winAfterUnit = rs.getString("EVENT_DURATIONAFTER");
//	        	  //Duration dur = new Duration();
//	        	  //dur.setUnit(TimeUnits.);
//	        	  //EVENT WINDOW
//	        	  //String strActualDate = actualDate.toString();
//	        	  StringBuffer windowSB = new StringBuffer();
//	        	  String eventWindow = null;
//	        	 
//	        	  if (winBeforeNumber != null && winBeforeNumber != 0 &&
//	        			  winBeforeUnit != null && winBeforeUnit.length() > 0 && actualScheduleDate != null){
//		        	  Date beginDate = calculateDate(actualScheduleDate, -1*winBeforeNumber,
//		                        winBeforeUnit);
//		        	  windowSB.append(DateUtil.dateToString(beginDate)).append("~");
//	        	  }
//	        	  else{
//	        	  windowSB = null;
//	        	  }
//	        	  if (winAfterNumber != null && winAfterNumber != 0 &&
//	        			  winAfterUnit != null && winAfterUnit.length() > 0 && actualScheduleDate != null){
//		        	  Date endDate = calculateDate(actualScheduleDate, winAfterNumber,
//		                        winAfterUnit);
//		        	  windowSB.append(DateUtil.dateToString(endDate));
//	        	  }
//	        	  else{
//		        	  windowSB = null;
//	        	  }
//	        	  if(windowSB != null){
//		        	  eventWindow = windowSB.toString();
//		        	  patEvent.setEventWindow(eventWindow);
//	        	  }
//	        	  //EVENT WINDOW END
//	        	 
//	        	  //setting status coverage type
//	        	 
//	        	  if(coverageTypeCode != null){
//	        		  patEvent.setCoverageType(coverageTypeCode);
//	        	  }
//	        	  
//	        	  lstEvent.add(patEvent);
//	   //     	  visit.setEvents(lstEvent);
//	          }
//		} 
//		catch(Throwable t){
//			
//			t.printStackTrace();
//			throw new OperationException();
//	}
//	finally {
//		try {
//			if (pstmt != null)
//				pstmt.close();
//		} catch (Exception e) {
//		}
//		try {
//			if (conn != null)
//				conn.close();
//		} catch (Exception e) {
//		}
//		
//    	} 
//		return visit;
//	}
	/**
	 * 
	 * @param startDate
	 * @param offsetNumber
	 * @param offsetUnit
	 * @return
	 */
	 private Date calculateDate(Date startDate, Integer offsetNumber, String offsetUnit) {
		    Calendar cal1 = Calendar.getInstance();
	        cal1.setTime(startDate);
	        cal1.add(convertUnitToField(offsetUnit), offsetNumber);
	        return cal1.getTime();
    }
    /**
     * 
     * @param offsetUnit
     * @return
     */
    private int convertUnitToField(String offsetUnit) {
        if (offsetUnit == null) { return 0; }
        if ("D".equals(offsetUnit.toUpperCase())) { return Calendar.DAY_OF_YEAR; }
        if ("W".equals(offsetUnit.toUpperCase())) { return Calendar.WEEK_OF_YEAR; }
        if ("M".equals(offsetUnit.toUpperCase())) { return Calendar.MONTH; }
        if ("Y".equals(offsetUnit.toUpperCase())) { return Calendar.YEAR; }
        return 0;
    }
    /**
     * get Eventstatus with StatusPK 
     * @param statusPk
     * @return
     */
//	private EventStatus getEventStatus(int statusPk){
//		
//		  EventStatAgentRObj eventStatAgent = EJBUtil.getEventStatAgentHome();
//		  EventStatus eventStatus = new EventStatus();
//		  
//    	  EventStatBean eventStatBean =  eventStatAgent.getEventStatDetails(statusPk);
//    	 
//    	  eventStatus.setNotes(eventStatBean.getEvtStatNotes());
//    	  
//    	  String statusCodeSubType = schCodeDao.getCodeSubtype(statusPk);
//    	  String statusCodeDesc = schCodeDao.getCodeDescription(statusPk);
//    	  Code statusCode = new Code();
//    	  statusCode.setType(statusCodeSubType);
//    	  statusCode.setDescription(statusCodeDesc);
//    	  Date eventStatDate = eventStatBean.getEventStatDt();
//    	  eventStatus.setStatusValidFrom(eventStatDate);
//    	  
//    	  return eventStatus;
//	}
    public ArrayList<String> getScheduleNames(){
    	ArrayList<String> lstScheduleNames = new ArrayList<String>();
    	return lstScheduleNames;
    }
    
    private static  String dateWindow(String unitBefore, Calendar currBefore, int winBeforeNumber, String unitAfter, Calendar currAfter, int winAfterNumber){
    	
    	String window = "";
		WindowUnit windowUnit = WindowUnit.valueOf(unitBefore);
		switch(windowUnit){
		    
		case H : currBefore.add(Calendar.HOUR, -winBeforeNumber);
		           break;
		case D : currBefore.add(Calendar.DATE, -winBeforeNumber);
		           break;
		case W : currBefore.add(Calendar.WEEK_OF_MONTH, -winBeforeNumber);
		           break;
		case M : currBefore.add(Calendar.MONTH, -winBeforeNumber);
		           break;
		case Y : currBefore.add(Calendar.YEAR, -winBeforeNumber);
		           break;
		}
		windowUnit = WindowUnit.valueOf(unitAfter);
		switch(windowUnit){
		    
		case H : currAfter.add(Calendar.HOUR, winAfterNumber);
                   break;
		case D : currAfter.add(Calendar.DATE, winAfterNumber);
		           break;
		case W : currAfter.add(Calendar.WEEK_OF_MONTH, winAfterNumber);
		           break;
		case M : currAfter.add(Calendar.MONTH, winAfterNumber);
		           break;
		case Y : currAfter.add(Calendar.YEAR, winAfterNumber);
		           break;
		}
		return (currBefore.getTime().toString() + "-" + currAfter.getTime().toString());
		
    }
    
    public static int getStudyPK(int patprotPK) throws OperationException{
    	
    	String sql = "select fk_study from er_patprot where pk_patprot = ?";
    	int studyPK = 0;
    	PreparedStatement pstmt  = null;
    	Connection conn =  null;
    	try{
    		conn = getConnection();
    		pstmt = conn.prepareStatement(sql);
        	pstmt.setInt(1, patprotPK);
        	
        	ResultSet rs = pstmt.executeQuery();
        			
        	while(rs.next()){
        		studyPK = rs.getInt("FK_STUDY");
        	}
        	return studyPK;
    	}catch(Throwable t){
    		t.printStackTrace();
    		throw new OperationException();
    	}
    	
    	finally{
    		try{
    			if(pstmt!=null){
    				pstmt.close();
    			}
    		}catch(Exception e){
    			
    		}
    		try{
    			if(conn!=null){
    				conn.close();
    			}
    		}catch(Exception e){
    			
    		}
    	}
    }
    
    public static int getPersonPK(int patprotPK) throws OperationException{
    	
    	String sql = "select fk_per from er_patprot where pk_patprot = ?";
    	int personPK = 0;
    	PreparedStatement pstmt  = null;
    	Connection conn =  null;
    	try{
    		conn = getConnection();
    		pstmt = conn.prepareStatement(sql);
        	pstmt.setInt(1, patprotPK);
        	
        	ResultSet rs = pstmt.executeQuery();
        			
        	while(rs.next()){
        		personPK = rs.getInt("FK_PER");
        	}
        	return personPK;
    	}catch(Throwable t){
    		t.printStackTrace();
    		throw new OperationException();
    	}
    	
    	finally{
    		try{
    			if(pstmt!=null){
    				pstmt.close();
    			}
    		}catch(Exception e){
    			
    		}
    		try{
    			if(conn!=null){
    				conn.close();
    			}
    		}catch(Exception e){
    			
    		}
    	}
    }
   
    public static int getPatProtPK(int eventPK) throws OperationException{
    	
    	String sql = "select fk_patprot from sch_events1 where event_id = ?";
    	int patProtPK = 0;
    	PreparedStatement pstmt  = null;
    	Connection conn =  null;
    	try{
    		conn = getConnection();
    		pstmt = conn.prepareStatement(sql);
        	pstmt.setInt(1, eventPK);
        	
        	ResultSet rs = pstmt.executeQuery();
        			
        	while(rs.next()){
        		patProtPK = rs.getInt("FK_PATPROT");
        	}
        	return patProtPK;
    	}catch(Throwable t){
    		t.printStackTrace();
    		throw new OperationException();
    	}
    	
    	finally{
    		try{
    			if(pstmt!=null){
    				pstmt.close();
    			}
    		}catch(Exception e){
    			
    		}
    		try{
    			if(conn!=null){
    				conn.close();
    			}
    		}catch(Exception e){
    			
    		}
    	}
    }
    
    public static int getOldEventStatus(int eventPK) throws OperationException{
    	
    	String sql = "select isconfirmed from sch_events1 where event_id = ?";
    	int eventStatus = 0;
    	PreparedStatement pstmt  = null;
    	Connection conn =  null;
    	try{
    		conn = getConnection();
    		pstmt = conn.prepareStatement(sql);
        	pstmt.setInt(1, eventPK);
        	
        	ResultSet rs = pstmt.executeQuery();
        			
        	while(rs.next()){
        		eventStatus = rs.getInt("ISCONFIRMED");
        	}
        	return eventStatus;
    	}catch(Throwable t){
    		t.printStackTrace();
    		throw new OperationException();
    	}
    	
    	finally{
    		try{
    			if(pstmt!=null){
    				pstmt.close();
    			}
    		}catch(Exception e){
    			
    		}
    		try{
    			if(conn!=null){
    				conn.close();
    			}
    		}catch(Exception e){
    			
    		}
    	}
    }
	

    public ArrayList<Integer> getDeactivatedEvents(int patientId, int protId, java.sql.Date bookedOn){	
		PreparedStatement pstmt = null;
	    Connection conn = null;
	    String sql = "";
	   
	    ArrayList<Integer> lstEventPk = new ArrayList<Integer>();
	    try {
	    	conn = getConnection();
	
	        sql= "select EVENT_ID FROM SCH_EVENTS1 " 
	        	+ "WHERE PATIENT_ID= LPAD(TO_CHAR(?),10,'0') AND " 
	        	+ "SESSION_ID= LPAD(TO_CHAR(?),10,'0') AND bookedon=?";	
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, patientId);                      
			pstmt.setInt(2, protId);  
			pstmt.setDate(3, bookedOn); 
	        ResultSet rs = pstmt.executeQuery();
	        
			while (rs.next()) {
			  	Integer eventPk = rs.getInt("EVENT_ID");
			  	lstEventPk.add(eventPk);
			}
	    } 
	    catch(Exception e){
	    	e.printStackTrace();
	    	return null;
		}
		finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
				} catch (Exception e) {
			}
		}
		return lstEventPk;
	}
      
    //For Bug Fix : 12377
    public static EventdefBean getOldEventDetails(int eventPK) throws OperationException{
    	
    	String sql = "select fk_codelst_covertype, reason_for_coveragechange, notes, service_site_id from sch_events1 where event_id = ?";
    	EventdefBean eventDefBean = new EventdefBean();
    	PreparedStatement pstmt  = null;
    	Connection conn =  null;
    	try{
    		conn = getConnection();
    		pstmt = conn.prepareStatement(sql);
        	pstmt.setInt(1, eventPK);
        	
        	ResultSet rs = pstmt.executeQuery();
        			
        	while(rs.next()){
        		eventDefBean.setEventCoverageType(EJBUtil.integerToString(rs.getInt("FK_CODELST_COVERTYPE")));
        		eventDefBean.setEventSOSId(EJBUtil.integerToString(rs.getInt("SERVICE_SITE_ID")));
        		eventDefBean.setReasonForCoverageChange(rs.getString("REASON_FOR_COVERAGECHANGE"));
        		System.out.println("the valu for notes ::: " + rs.getString("NOTES"));
        		eventDefBean.setNotes(rs.getString("NOTES"));
        		
        	}
        	return eventDefBean;
    	}catch(Throwable t){
    		t.printStackTrace();
    		throw new OperationException();
    	}
    	
    	finally{
    		try{
    			if(pstmt!=null){
    				pstmt.close();
    			}
    		}catch(Exception e){
    			
    		}
    		try{
    			if(conn!=null){
    				conn.close();
    			}
    		}catch(Exception e){
    			
    		}
    	}
    }
}
