package com.velos.services.patientstudy;

import java.util.List;

import javax.ejb.Remote;

import com.velos.services.OperationException;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientStudy;
/**
 * Remote Interface with declarations of PatientStudies methods 
 * @author Virendra
 *
 */
@Remote
public interface PatientStudyService{
	/**
	 * @param patientId
	 * @return List<PatientStudy>
	 * @throws OperationException
	 */
	public List<PatientStudy> getPatientStudies(PatientIdentifier patientId)
	throws OperationException;
	
}