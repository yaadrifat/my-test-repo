/**
 * Created On Jun 8, 2011
 */
package com.velos.services.calendar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.ejb.SessionContext;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.SiteDao;
import com.velos.eres.business.site.impl.SiteBean;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.siteAgent.SiteAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.esch.business.eventassoc.impl.EventAssocBean;
import com.velos.esch.business.eventcost.impl.EventcostBean;
import com.velos.esch.business.protvisit.impl.ProtVisitBean;
import com.velos.esch.service.eventassocAgent.EventAssocAgentRObj;
import com.velos.esch.service.eventcostAgent.EventcostAgentRObj;
import com.velos.esch.service.eventdefAgent.EventdefAgentRObj;
import com.velos.services.AbstractService;
import com.velos.services.CRUDAction;
import com.velos.services.CodeNotFoundException;
import com.velos.services.CompletedAction;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.OperationRolledBackException;
import com.velos.services.ResponseHolder;
import com.velos.services.map.MultipleObjectsFoundException;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.CalendarEvent;
import com.velos.services.model.CalendarEventSummary;
import com.velos.services.model.CalendarEvents;
import com.velos.services.model.Cost;
import com.velos.services.model.Costs;
import com.velos.services.model.Duration;
import com.velos.services.model.EventIdentifier;
import com.velos.services.model.EventNameIdentifier;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.Duration.TimeUnits;
import com.velos.services.model.SimpleIdentifier;
import com.velos.services.model.VisitIdentifier;
import com.velos.services.util.CodeCache;
import com.velos.services.util.ObjectLocator;

/**Helper class to facilitate operations for Event
 * in a Study Calendar
 * @author Kanwaldeep
 *
 */
public class CalendarEventHelper {
	
	private static Logger logger = Logger.getLogger(CalendarEventHelper.class); 
	/**
	 * 
	 * @param studyCalendarEvents
	 * @param calendarPK
	 * @param visitPK
	 * @param visitDisplacement
	 * @param parameters
	 * @throws OperationException
	 */
	public void addEventsToVisit(CalendarEvents studyCalendarEvents,
			Integer calendarPK, Integer visitPK, String visitDisplacement, Map<String, Object> parameters, Map<String, Integer> eventNames) throws OperationException {
		ResponseHolder response = (ResponseHolder) parameters.get("ResponseHolder"); 
		EventdefAgentRObj  eventDefAgent= (EventdefAgentRObj) parameters.get("EventdefAgentRObj"); 
		EventAssocAgentRObj eventAssocAgent = (EventAssocAgentRObj) parameters.get("EventAssocAgentRObj"); 
		ObjectMapService objectMapService = (ObjectMapService) parameters.get("ObjectMapService"); 
		
		//look for duplicate event names from request
		List<String> eventsToAdd = new ArrayList<String>();
		Integer eventPK = 0;
		fixSequence(studyCalendarEvents, visitPK, parameters); 
		for (CalendarEvent calendarEvent : studyCalendarEvents.getEvent()) {
			Integer maxSequenceEvent = eventDefAgent.getMaxSeqForVisitEvents(
					visitPK, calendarEvent.getCalendarEventSummary().getEventName(), "event_assoc");
			EventNameIdentifier eventIdentifier = new EventNameIdentifier(); //BugFix#9758 & 9778:Tarandeep Singh Bali
			VisitIdentifier visitIdentifier = new VisitIdentifier();
			if (maxSequenceEvent == -3) {
				response.getIssues().add(
						new Issue(IssueTypes.STUDY_CALENDAR_ERROR_CREATE_EVENT,
								"Event already exists in the visit "));
				throw new OperationException();
				
			}
			
			if(calendarEvent.getCalendarEventSummary().getEventName() == null || calendarEvent.getCalendarEventSummary().getEventName().length() == 0)
			{
				Issue issue = new Issue(IssueTypes.DATA_VALIDATION , "eventName is required to create an Event"); 
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(issue);
				throw new OperationException(issue.toString());
			}
			
			if(eventsToAdd.contains(calendarEvent.getCalendarEventSummary().getEventName().toUpperCase()))
			{
				Issue issue = new Issue(IssueTypes.DUPLICATE_EVENT_NAME, "For Event Name : " + calendarEvent.getCalendarEventSummary().getEventName());
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(issue);
				logger.error(issue);
				throw new OperationException(issue.toString());
				
			}

			eventsToAdd.add(calendarEvent.getCalendarEventSummary().getEventName().toUpperCase()); 
			
			
	//		if(maxSequence == 0)maxSequence =  maxSequenceEvent; 
			
			EventAssocBean eventAssocBean = new EventAssocBean();

			calendarEventIntoBean(calendarEvent, eventAssocBean,
					calendarPK, visitPK, true, EJBUtil.integerToString(eventNames.get(calendarEvent.getCalendarEventSummary().getEventName().toLowerCase())),visitDisplacement, parameters);

			//Integer eventPK = 0;
			eventPK = eventAssocAgent.setEventAssocDetails(eventAssocBean);

			if (eventPK == -2) {
				response.getIssues().add(
						new Issue(
								IssueTypes.ERROR_CREATING_STUDY_CALENDAR_EVENT,
								"Error creating event"));
				throw new OperationRolledBackException(response.getIssues());
			}
			if (calendarEvent.getCosts() != null) {
				boolean isCostsAddedToEvent = false;
				Costs eventCosts = calendarEvent.getCosts();
				isCostsAddedToEvent = addEventCostsToEvent(eventCosts, eventPK,
						true, parameters);
				if (!isCostsAddedToEvent) {
					response.getIssues()
							.add(new Issue(
									IssueTypes.ERROR_CREATING_STUDY_CALENDAR_EVENT_COST,
									"Error creating event costs"));
					throw new OperationRolledBackException(response.getIssues());
				}
			}
			//Begin: BugFix# 9758 : Tarandeep Singh Bali
			ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY_CALENDAR, eventPK);
			ObjectMap mapVisit = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_VISIT, visitPK);//Bug Fix# 10079 : Tarandeep Singh Bali
			// Integer eventPK = eventAssocBean.getEvent_id();
			
			visitIdentifier.setOID(mapVisit.getOID());
            eventIdentifier.setParentOID(visitIdentifier);//Bug Fix# 10079 : Tarandeep Singh Bali		   
			eventIdentifier.setOID(map.getOID());
			eventIdentifier.setEventName(calendarEvent.getCalendarEventSummary().getEventName()); // Bug Fix : 9778 : Tarandeep Singh Bali
			response.addObjectCreatedAction(eventIdentifier);
			//End: BugFix# 9758
		}
	}
	/**
	 * 
	 * @param calendarEvent
	 * @param persistEventAssocBean
	 * @param calendarPK
	 * @param visitPK
	 * @param isNew
	 * @param costID
	 * @param visitDisplacement
	 * @param parameters
	 * @param maxSequence
	 * @throws OperationException
	 * @throws MultipleObjectsFoundException
	 */
	private void calendarEventIntoBean(CalendarEvent calendarEvent,
			EventAssocBean persistEventAssocBean,Integer calendarPK ,Integer visitPK, boolean isNew, String costID, String visitDisplacement,Map<String, Object> parameters) throws OperationException {
		ResponseHolder response = (ResponseHolder) parameters.get("ResponseHolder"); 
		persistEventAssocBean.setIpAdd(AbstractService.IP_ADDRESS_FIELD_VALUE);
		UserBean callingUser = (UserBean) parameters.get("callingUser"); 
		ObjectMapService objectMapService = (ObjectMapService) parameters.get("ObjectMapService"); 
		SessionContext sessionContext = (SessionContext) parameters.get("sessionContext"); 
		EventAssocAgentRObj eventAssocAgent = (EventAssocAgentRObj) parameters.get("EventAssocAgentRObj");
		SiteAgentRObj siteAgent = (SiteAgentRObj) parameters.get("SiteAgentRObj"); 
		
		ArrayList siteID = null; 
		
		if (isNew){ 
			persistEventAssocBean.setCreator(EJBUtil.integerToString(callingUser.getUserId()));
		}
		persistEventAssocBean.setModifiedBy(EJBUtil.integerToString(callingUser.getUserId()));
		//kanwal added to fix cost and displacement
		if(isNew || visitDisplacement != null){
			persistEventAssocBean.setDisplacement(visitDisplacement);
		}
		if(costID != null){
			persistEventAssocBean.setCost(costID); 
		}
		//kanwal added eventDuration 
		TimeUnits durationUnit = TimeUnits.DAY; 
		Integer durationValue = 0; 
		
		EventAssocBean calendarBean = eventAssocAgent.getEventAssocDetails(calendarPK);
		int calendarDuration= EJBUtil.stringToInteger(calendarBean.getDuration());
		
		Duration eventDuration = calendarEvent.getCalendarEventSummary().getEventDuration();
		if(eventDuration != null){
			durationUnit = eventDuration.getUnit();
			durationValue = eventDuration.getValue();
			//Fixed Bug #6694
         if (durationValue<0)
         {
        	 response.getIssues().add(
						new Issue(
								IssueTypes.DATA_VALIDATION, 
								"Event duration should be greater than zero"));
				throw new OperationException();
         }
         
         if(durationUnit == null)
         {
        		response.getIssues().add(
						new Issue(
								IssueTypes.DATA_VALIDATION, 
								"Please enter event duration unit"));
				throw new OperationException();
         }
         
         if(durationUnit != null && durationValue != null ){

				if(!durationUnit.equals(TimeUnits.DAY)){

					response.getIssues().add(
							new Issue(
									IssueTypes.DATA_VALIDATION, 
									"Please enter event duration after unit as 'DAY'"));
					throw new OperationException();

				}				
			}
         
         if (durationValue > calendarDuration )
         {
        	 response.getIssues().add(
						new Issue(
								IssueTypes.INVALID_EVENT_DURATION, 
								"Event duration should not be greater than Calendar duration"));
				throw new OperationException();
         }
         
        String encodedUnit = Duration.encodeUnit(durationUnit);
 		persistEventAssocBean.setDuration(EJBUtil.integerToString(durationValue));
 		persistEventAssocBean.setDurationUnit(encodedUnit);
		}
		
		
		
		
		//Hardcoded for event type
		persistEventAssocBean.setEvent_type(CodeCache.CODE_EVENT_TYPE);
		persistEventAssocBean.setEventVisit(EJBUtil.integerToString(visitPK));
		
		if(calendarEvent.getCalendarEventSummary().getCPTCode() != null){
			persistEventAssocBean.setCptCode(calendarEvent.getCalendarEventSummary().getCPTCode());
		}
		
		
		persistEventAssocBean.setChain_id(EJBUtil.integerToString(calendarPK));
		if(calendarEvent.getCalendarEventSummary().getCoverageType() != null)
		{
			Integer coverageTypePk = null;
			//CodeCache codes = CodeCache.getInstance();
			
			if(!isNew && (calendarEvent.getCalendarEventSummary().getCoverageType().getCode() == null || calendarEvent.getCalendarEventSummary().getCoverageType().getCode().length() == 0))
			{
				coverageTypePk = 0; 
			}
			else{
				try{
					coverageTypePk = AbstractService.dereferenceSchCode(calendarEvent.getCalendarEventSummary().getCoverageType(), CodeCache.CODE_TYPE_COVERAGE_TYPE, callingUser);

				}catch(CodeNotFoundException e){
					response.getIssues().add(
							new Issue(
									IssueTypes.CODE_NOT_FOUND, 
							"CoverageType code not found"));
					throw new OperationException();
				}


				if(coverageTypePk == null){
					response.getIssues().add(
							new Issue(
									IssueTypes.CODE_NOT_FOUND, 
							"CoverageType code not found"));
					throw new OperationException();

				}
			}
			persistEventAssocBean.setEventCoverageType(EJBUtil.integerToString(coverageTypePk));
		}
		if(calendarEvent.getCalendarEventSummary().getDescription() != null){
			persistEventAssocBean.setDescription(calendarEvent.getCalendarEventSummary().getDescription());
		}
		
		//EventIdentifier eventIdentifier = calendarEvent.getEventIdentifier();
		//Virendra: Fixed 7233
		if(isNew){
			if(calendarEvent.getCalendarEventSummary().getEventName() == null || 
					calendarEvent.getCalendarEventSummary().getEventName().length() == 0 ){
				response.getIssues().add(
						new Issue(
							IssueTypes.DATA_VALIDATION, 
							"Calendar event name is mandatory"));
				throw new OperationException();
			}
			persistEventAssocBean.setName(calendarEvent.getCalendarEventSummary().getEventName());
		}
		//Virendra:#7403
		else if(calendarEvent.getCalendarEventSummary().getEventName() != null){
			//@Kanwal - Bug Fix 7548 
			if(calendarEvent.getCalendarEventSummary().getEventName().length() == 0)
			{

				response.getIssues().add(
						new Issue(
								IssueTypes.DATA_VALIDATION, 
								"Event Name is required for Event"));
				throw new OperationException();
			}
			persistEventAssocBean.setName(calendarEvent.getCalendarEventSummary().getEventName());
		}
		
		
		
		//Set Defaults 
		TimeUnits durationUnitAfter = TimeUnits.DAY; 
		Integer durationValueAfter = 0; 
		
		Duration eventDurationAfter = calendarEvent.getCalendarEventSummary().getEventWindowAfter();
		if(eventDurationAfter != null){
			durationUnitAfter = eventDurationAfter.getUnit();
			durationValueAfter = eventDurationAfter.getValue();
			
			if(durationUnitAfter == null)
			{
				response.getIssues().add(
						new Issue(
								IssueTypes.DATA_VALIDATION, 
								"Please enter event duration after unit"));
				throw new OperationException();
			}

			if(durationUnitAfter != null && durationValueAfter != null ){

				if(!durationUnitAfter.equals(TimeUnits.DAY)){

					response.getIssues().add(
							new Issue(
									IssueTypes.DATA_VALIDATION, 
									"Please enter event duration after unit as 'DAY'"));
					throw new OperationException();

				}				
			}
		}
		
		String encodedUnitAfter = Duration.encodeUnit(durationUnitAfter);
		persistEventAssocBean.setFuzzyAfter(EJBUtil.integerToString(durationValueAfter));
		persistEventAssocBean.setDurationUnitAfter(encodedUnitAfter);
		
		TimeUnits durationUnitBefore = TimeUnits.DAY; 
		int durationValueBefore = 0; 
		
		Duration eventDurationBefore = calendarEvent.getCalendarEventSummary().getEventWindowBefore();
		if(eventDurationBefore != null){
			durationUnitBefore = eventDurationBefore.getUnit();
			durationValueBefore = eventDurationBefore.getValue(); 
			
			if(durationUnitBefore == null)
			{
				response.getIssues().add(
						new Issue(
								IssueTypes.DATA_VALIDATION, 
								"Please enter event duration Before Unit"));
				throw new OperationException();
			}
			
			if(durationUnitBefore != null){
				//Integer durationValueBefore = eventDurationBefore.getValue();

				if(!durationUnitBefore.equals(TimeUnits.DAY)){

					response.getIssues().add(
							new Issue(
									IssueTypes.DATA_VALIDATION, 
									"Please enter event duration Before unit as 'DAY'"));
					throw new OperationException();

				}
				
			}
		}
		
		String encodedUnitBefore = Duration.encodeUnit(durationUnitBefore);
		persistEventAssocBean.setDurationUnitBefore(encodedUnitBefore);
		persistEventAssocBean.setFuzzy_period(EJBUtil.integerToString(durationValueBefore)); 
		
		OrganizationIdentifier organizationIdentifier = calendarEvent.getCalendarEventSummary().getFacility();
		//Virendra: #7243
		if(organizationIdentifier != null)
		{
			Integer eventSitePK = null;
			try {
				eventSitePK = ObjectLocator.sitePKFromIdentifier(callingUser, organizationIdentifier, sessionContext, objectMapService);
			} catch (MultipleObjectsFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(!isNew && (organizationIdentifier.getOID() == null || organizationIdentifier.getOID().length() ==0) 
					&& (organizationIdentifier.getSiteAltId() == null || organizationIdentifier.getSiteAltId().length() ==0)
					&& (organizationIdentifier.getSiteName() == null || organizationIdentifier.getSiteName().length() == 0))
			{
				eventSitePK = 0; 
			}else if(eventSitePK == null || !isSiteFromSameAccount(eventSitePK, 
					EJBUtil.stringToInteger(callingUser.getUserAccountId()), 
					parameters)){

				response.getIssues().add(
						new Issue(
								IssueTypes.ORGANIZATION_NOT_FOUND, 
						"Facility not found"));
				throw new OperationException();

			}
			
			if(eventSitePK != 0)
			{
				siteID = getSiteIDs(siteAgent, callingUser); 
				if(!siteID.contains(eventSitePK)){ 

					response.getIssues().add(
							new Issue(
									IssueTypes.DATA_VALIDATION, 
									"Event facility not valid"));
					throw new OperationException();

				}
			}
			
			persistEventAssocBean.setEventFacilityId(EJBUtil.integerToString(eventSitePK));
		
		}
	
		if(calendarEvent.getCalendarEventSummary().getNotes()!= null){
			persistEventAssocBean.setNotes(calendarEvent.getCalendarEventSummary().getNotes());
		}
		//Virendra: Fixed #7180, #7235
		if(calendarEvent.getCalendarEventSummary().getSequence() != null && !calendarEvent.getCalendarEventSummary().getSequence().equals("") && calendarEvent.getCalendarEventSummary().getSequence() != 0 ){
			if (!isNew){
				fixEventSequenceOnUpdate(calendarEvent,  visitPK, persistEventAssocBean, parameters);
				}
				persistEventAssocBean.setEventSequence(EJBUtil.integerToString(calendarEvent.getCalendarEventSummary().getSequence()));		
		}
	
		OrganizationIdentifier siteOfService = calendarEvent.getCalendarEventSummary().getSiteOfService();
		//Virendra: #7243
		if(siteOfService != null)
		{
			Integer eventSiteOfServicePK = null;
			try {
				eventSiteOfServicePK = ObjectLocator.sitePKFromIdentifier(callingUser, siteOfService, sessionContext, objectMapService);
			} catch (MultipleObjectsFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(!isNew && (siteOfService.getOID() == null || siteOfService.getOID().length() ==0) 
					&& (siteOfService.getSiteAltId() == null || siteOfService.getSiteAltId().length() ==0)
					&& (siteOfService.getSiteName() == null || siteOfService.getSiteName().length() == 0))
			{
				eventSiteOfServicePK = 0; 
			//Virendra:#7301
			}else if(eventSiteOfServicePK == null || !isSiteFromSameAccount(eventSiteOfServicePK, 
					EJBUtil.stringToInteger(callingUser.getUserAccountId()), parameters)){

				response.getIssues().add(
						new Issue(
								IssueTypes.ORGANIZATION_NOT_FOUND, 
						"Event site of service not found"));
				throw new OperationException();
			}
			
			if(eventSiteOfServicePK != 0)
			{
				if(siteID == null)
				{
					siteID = getSiteIDs(siteAgent, callingUser); 
				}
				if(!siteID.contains(eventSiteOfServicePK)){ 
			
					response.getIssues().add(
							new Issue(
									IssueTypes.DATA_VALIDATION, 
							"Event site of Service not valid"));
					throw new OperationException();
				}
			}
			
			persistEventAssocBean.setEventSOSId((EJBUtil.integerToString(eventSiteOfServicePK)));
		}

		
	}
	/**
	 * 
	 * @param eventCosts
	 * @param eventPK
	 * @param isNew
	 * @param parameters
	 * @return
	 * @throws OperationException
	 */

	private boolean addEventCostsToEvent(Costs eventCosts,
			Integer eventPK, boolean isNew, Map<String, Object> parameters) throws OperationException {
		boolean isEventCostAdded =  false;
		ResponseHolder response = (ResponseHolder) parameters.get("ResponseHolder");
		ObjectMapService objectMapService = (ObjectMapService) parameters.get("ObjectMapService"); 
		SessionContext sessionContext = (SessionContext) parameters.get("sessionContext"); 
		EventcostAgentRObj eventCostAgent =(EventcostAgentRObj) parameters.get("EventcostAgentRObj"); 
		try {
			for (Cost eventCost : eventCosts.getCost()) {
				addEventCost(eventCost, eventPK, parameters);
			}
			isEventCostAdded = true;
		} catch(Throwable t){
			sessionContext.setRollbackOnly();
		//	addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyCalendarServiceImpl create", t);
			throw new OperationRolledBackException(response.getIssues());

		}
		return isEventCostAdded;
			
	}
	/**
	 * 
	 * @param persistEventCostBean
	 * @param eventCost
	 * @param eventPK
	 * @param isNew
	 * @param parameters
	 * @throws OperationException
	 */
	private void calendarEventCostIntoBean(EventcostBean persistEventCostBean, Cost eventCost, Integer eventPK, boolean isNew, Map<String, Object> parameters) throws OperationException{
		ResponseHolder response = (ResponseHolder) parameters.get("ResponseHolder");
		persistEventCostBean.setIpAdd(AbstractService.IP_ADDRESS_FIELD_VALUE);
		UserBean callingUser = (UserBean) parameters.get("callingUser");
		if (isNew){ 
			persistEventCostBean.setCreator(EJBUtil.integerToString(callingUser.getUserId()));
		}
		persistEventCostBean.setModifiedBy(EJBUtil.integerToString(callingUser.getUserId()));
		persistEventCostBean.setEventId(EJBUtil.integerToString(eventPK));
		if(eventCost.getCost() != null){
			if(eventCost.getCost().signum() == -1)
			{
				response.getIssues().add(
						new Issue(
								IssueTypes.DATA_VALIDATION, 
								"Cost cannot be negative"));
				throw new OperationException();
			}
			persistEventCostBean.setEventcostValue((eventCost.getCost()).toString());
		}

		if(isNew  || eventCost.getCostType() != null)
		{
			if(eventCost.getCostType() == null)
			{
				response.getIssues().add(
						new Issue(
								IssueTypes.DATA_VALIDATION, 
						"Cost type code is mandatory"));
				throw new OperationException();
				
			}

			Integer costDescriptionTypePk= 0;
			try{
				
				if(eventCost.getCostType().getCode() != null && eventCost.getCostType().getCode().length() != 0)
				{
					costDescriptionTypePk = AbstractService.dereferenceSchCode(eventCost.getCostType(), CodeCache.CODE_TYPE_COST_DESC, callingUser);
					persistEventCostBean.setEventcostDescId(EJBUtil.integerToString(costDescriptionTypePk));					
				}else
				{
					response.getIssues().add(
							new Issue(
									IssueTypes.DATA_VALIDATION, 
							"Cost type code is mandatory"));
					throw new OperationException();
				}
			}catch(CodeNotFoundException e){
				response.getIssues().add(
						new Issue(
								IssueTypes.CODE_NOT_FOUND, 
						"Cost description type code not found"));
				throw new OperationException();
			}
		}
		
		if(isNew || eventCost.getCurrency() != null)
		{
			Integer costCurrencyPK = 0;
			if(eventCost.getCurrency() == null){
				response.getIssues().add(
						new Issue(
								IssueTypes.DATA_VALIDATION, 
						"Currency code is mandatory"));
				throw new OperationException();

			}
			try{
				
				//BugFix Bug 6624
				
				if(eventCost.getCurrency().getCode() != null && eventCost.getCurrency().getCode().length()!= 0)
				{
					costCurrencyPK = AbstractService.dereferenceSchCode(eventCost.getCurrency(), CodeCache.CODE_TYPE_CURRENCY, callingUser);
					persistEventCostBean.setCurrencyId(EJBUtil.integerToString(costCurrencyPK));
				}else
				{
					response.getIssues().add(
							new Issue(
									IssueTypes.DATA_VALIDATION, 
							"Currency code is mandatory"));
					throw new OperationException();
				}

			}catch(CodeNotFoundException e){
				response.getIssues().add(
						new Issue(
								IssueTypes.CODE_NOT_FOUND, 
						"Currency code not found"));
				throw new OperationException();
			}
		}
	}

	/**
	 * @param visit
	 * @param eventNames
	 */
	public int populateEventNames(CalendarEvents events,
			Map<String, Integer> eventNames, int calendarID, Map<String, Object> parameters, Integer costMax) throws OperationException{
		EventAssocAgentRObj eventAssocAgent = (EventAssocAgentRObj) parameters.get("EventAssocAgentRObj"); 
		
		for(CalendarEvent event: events.getEvent())
		{
			if(event.getCalendarEventSummary().getEventName().toUpperCase() != null && event.getCalendarEventSummary().getEventName().length() > 0 && !eventNames.containsKey(event.getCalendarEventSummary().getEventName().toLowerCase()))
			{
				StudyCalendarDAO calendarDao = new StudyCalendarDAO(); 
				int costID = calendarDao.getCostIDforEventPool(event.getCalendarEventSummary().getEventName(), calendarID); 
				if(costID == 0 )// not in event pool for Calendar add to pool
				{
					EventAssocBean poolEventAssocBean = new EventAssocBean(); 
					if(costMax == 0)// just call once when we are adding events 
					{
						costMax = eventAssocAgent.getMaxCost(EJBUtil.integerToString(calendarID)); 
					}
					//Bug Fix 6603
					calendarEventIntoBean(event, poolEventAssocBean, calendarID, null, true, EJBUtil.integerToString(++costMax), null, parameters); 
					eventAssocAgent.setEventAssocDetails(poolEventAssocBean); 			
					costID = costMax; 
				}
				
				eventNames.put(event.getCalendarEventSummary().getEventName().toLowerCase(), costID); 
			}
		}
		
		return costMax; 
		
	}
	
	private void fixSequence(CalendarEvents studyCalendarEvents, Integer visitPK, Map<String, Object> parameters)
	throws OperationException
	{
		
		ResponseHolder response = (ResponseHolder) parameters.get("ResponseHolder"); 
		EventdefAgentRObj  eventDefAgent= (EventdefAgentRObj) parameters.get("EventdefAgentRObj"); 
		EventAssocAgentRObj eventAssocAgent = (EventAssocAgentRObj) parameters.get("EventAssocAgentRObj"); 
	//	ObjectMapService objectMapService = (ObjectMapService) parameters.get("ObjectMapService");
		UserBean callingUser = (UserBean) parameters.get("callingUser");
		
		StudyCalendarDAO studyCalendarDAO = new StudyCalendarDAO();
		List<CalendarEvent> eventList  = studyCalendarDAO.getEventIDSequenceMapForVisit(visitPK);
			
		eventList.add(0, new CalendarEvent()); // we dont have 0 in event sequence
		
		Collections.sort(studyCalendarEvents.getEvent(), new EventSequenceCompartor());  
				
		for (CalendarEvent calendarEvent : studyCalendarEvents.getEvent()) {
			
			if(calendarEvent.getCalendarEventSummary().getSequence() != null && calendarEvent.getCalendarEventSummary().getSequence() < eventList.size() && calendarEvent.getCalendarEventSummary().getSequence() != 0)
			{
				if( eventList.get(calendarEvent.getCalendarEventSummary().getSequence()).getCalendarEventSummary().getSequence() != null && eventList.get(calendarEvent.getCalendarEventSummary().getSequence()).getCalendarEventSummary().getSequence() == calendarEvent.getCalendarEventSummary().getSequence())
					{
						response.getIssues().add(
								new Issue(IssueTypes.STUDY_CALENDAR_ERROR_CREATE_EVENT,
								"Multiple events with same sequence number is found"));
						throw new OperationException(); 
					}
				eventList.add(calendarEvent.getCalendarEventSummary().getSequence(), calendarEvent); 
			}else
			{
				eventList.add(calendarEvent); 
			}
		}
		
		// Now sequence is calculated set in objects. 		
		for(int i= 1; i < eventList.size(); i++)
		{
			CalendarEvent event = eventList.get(i); 
			if(event.getCalendarEventSummary().getSequence() == null  && event.getCalendarEventSummary().getEventName() == null) // existing event  in DB
			{
				if(event.getCalendarEventSummary().getEventIdentifier() != null && event.getCalendarEventSummary().getEventIdentifier().getOID() != null)
				{
					EventAssocBean bean = eventAssocAgent.getEventAssocDetails(EJBUtil.stringToInteger(event.getCalendarEventSummary().getEventIdentifier().getOID()));
					bean.setEventSequence(EJBUtil.integerToString(i));
					bean.setModifiedBy(EJBUtil.integerToString(callingUser.getUserId()));
					eventAssocAgent.updateEventAssoc(bean);
				}
			}else // new event 
			{
				event.getCalendarEventSummary().setSequence(i); 
			}
	
		}
		
		
	}
	public Integer updateStudyCalendarEvent(CalendarEventSummary calendarEventSummary, Integer eventPK, Integer calendarPK, Integer visitPK, boolean isOffline, Map<String, Object> parameters) throws OperationException{
		
		ResponseHolder response = (ResponseHolder) parameters.get("ResponseHolder"); 
		ObjectMapService objectMapService = (ObjectMapService) parameters.get("ObjectMapService"); 
		EventAssocAgentRObj eventAssocAgent = (EventAssocAgentRObj) parameters.get("EventAssocAgentRObj");
		
		EventAssocBean eventAssocBean = eventAssocAgent.getEventAssocDetails(eventPK);
		
		if(eventAssocBean == null){
			response.addIssue(new Issue(IssueTypes.EVENT_NOT_FOUND, "Event not found ")); 
			throw new OperationException();
			
		}
		//CalendarEvents studyCalendarEvents = new CalendarEvents();
		//studyCalendarEvents.getEvent().add(calendarEvent);
		
		//fixSequence(studyCalendarEvents, visitPK, parameters); 
		CalendarEvent calendarEvent = new CalendarEvent();
		calendarEvent.setCalendarEventSummary(calendarEventSummary); 
		if(calendarEventSummary.getEventName() != null)
		{
			if( isOffline && !calendarEventSummary.getEventName().equals(eventAssocBean.getName()) )
			{
				response.getIssues().add(new Issue(IssueTypes.DATA_VALIDATION, "Event Name cannot be updated after Calendar with this event was activated"));
				throw new OperationException(); 
			}
		}

			
		calendarEventIntoBean(calendarEvent, eventAssocBean,
				calendarPK, visitPK, false, null, null, parameters);
		
		Integer eventPK1 = 0;
		eventPK1 = eventAssocAgent.updateEventAssoc(eventAssocBean);

		if (eventPK1 == -2) {
			response.getIssues().add(
					new Issue(
							IssueTypes.ERROR_UPDATING_STUDY_CALENDAR_EVENT,
							"Error updating event"));
			throw new OperationRolledBackException(response.getIssues());
		}
		objectMapService.getOrCreateObjectMapFromPK(
				ObjectMapService.PERSISTENCE_UNIT_STUDY_CALENDAR, eventPK);
		// Integer eventPK = eventAssocBean.getEvent_id();
//		if (calendarEvent.getCosts() != null) {
//			boolean isCostsAddedToEvent = false;
//			Costs eventCosts = calendarEvent.getCosts();
//			isCostsAddedToEvent = addEventCostsToEvent(eventCosts, eventPK,
//					false, parameters);
//			if (!isCostsAddedToEvent) {
//				response.getIssues()
//						.add(new Issue(
//								IssueTypes.ERROR_CREATING_STUDY_CALENDAR_EVENT_COST,
//								"Error creating event costs"));
//				throw new OperationRolledBackException(response.getIssues());
//			}
//		}
		
		return eventPK1;
		
	}
	/**method to fix eventsequence on event update
	 * 
	 * @param studyCalendarEvents
	 * @param visitPK
	 * @param parameters
	 * @throws OperationException
	 */
	private void fixEventSequenceOnUpdate(CalendarEvent calendarEvent, Integer visitPK, EventAssocBean persistEventAssocBean, Map<String, Object> parameters)
	throws OperationException
	{		
		EventdefAgentRObj  eventDefAgent= (EventdefAgentRObj) parameters.get("EventdefAgentRObj"); 
		EventAssocAgentRObj eventAssocAgent = (EventAssocAgentRObj) parameters.get("EventAssocAgentRObj"); 	
		UserBean callingUser = (UserBean) parameters.get("callingUser");
		
		StudyCalendarDAO studyCalendarDAO = new StudyCalendarDAO();
		List<CalendarEvent> eventList  = studyCalendarDAO.getEventIDSequenceMapForVisit(visitPK);
			
		eventList.add(0, new CalendarEvent()); // we dont have 0 in event sequence
		
		int currentSequenceInDB =  EJBUtil.stringToInteger(persistEventAssocBean.getEventSequence()); 
		int newSequence = calendarEvent.getCalendarEventSummary().getSequence(); 
		if(newSequence <= 0)
		{	
			newSequence = 1; 
			calendarEvent.getCalendarEventSummary().setSequence(newSequence); 			
		}
		
		
		if( newSequence != currentSequenceInDB)
		{	
			for(int i= 1; i < eventList.size(); i++)
			{
				if(eventList.get(i).getCalendarEventSummary().getEventIdentifier().getOID().equals(EJBUtil.integerToString(persistEventAssocBean.getEvent_id())))
				{
					eventList.remove(i); 
					break; 
				}
			}
			if(newSequence <= eventList.size()) 
			{			
					eventList.add(newSequence, calendarEvent);
					
			}else
			{
				eventList.add(calendarEvent); 
				calendarEvent.getCalendarEventSummary().setSequence(eventList.size() - 1); 	// max sequence - putting at end of	list		
			}
			
			//NOTE: we can initialize the loop based on where it is being inserted but 
			//I am updating all the sequence because currently eResearch is not updating sequence as it should. 
			//Once that is fixed we can put in logic to only update the events whose sequence is changing.
			for(int i= 1; i < eventList.size(); i++)
			{
				CalendarEvent event = eventList.get(i); 
				if(event.getCalendarEventSummary().getSequence() == null  && event.getCalendarEventSummary().getEventName() == null) // existing event  in DB
				{
					if(event.getCalendarEventSummary().getEventIdentifier() != null && event.getCalendarEventSummary().getEventIdentifier().getOID() != null)
					{
						EventAssocBean bean = eventAssocAgent.getEventAssocDetails(EJBUtil.stringToInteger(event.getCalendarEventSummary().getEventIdentifier().getOID()));
						bean.setEventSequence(EJBUtil.integerToString(i));
						bean.setModifiedBy(EJBUtil.integerToString(callingUser.getUserId()));
						eventAssocAgent.updateEventAssoc(bean);
					}
				}
			}		
			
		}
	
		
	}
	
	public Integer addEventCost(Cost eventCost, Integer eventPK, Map<String, Object> parameters) throws OperationException{
		
		ResponseHolder response = (ResponseHolder) parameters.get("ResponseHolder");
		SessionContext sessionContext = (SessionContext) parameters.get("sessionContext"); 
		EventcostAgentRObj eventCostAgent =(EventcostAgentRObj) parameters.get("EventcostAgentRObj"); 
		EventcostBean persistEventCostBean = new EventcostBean();
		calendarEventCostIntoBean(persistEventCostBean, eventCost, eventPK,
				true, parameters);
		Integer intCalendarEventCostPK = eventCostAgent.setEventcostDetails(persistEventCostBean);
		
		if(intCalendarEventCostPK == -2){
			sessionContext.setRollbackOnly();
			response.getIssues().add(new Issue(IssueTypes.ERROR_CREATING_STUDY_CALENDAR_EVENT_COST));
			throw new OperationException("Error Creating Study Calendar event cost");
		}
		return intCalendarEventCostPK;
		
	}
	
	public Integer updateEventCost(Cost eventCost, Integer eventPK,Integer eventCostPK, Map<String, Object> parameters) throws OperationException{
		
		ResponseHolder response = (ResponseHolder) parameters.get("ResponseHolder");
		SessionContext sessionContext = (SessionContext) parameters.get("sessionContext"); 
		EventcostAgentRObj eventCostAgent =(EventcostAgentRObj) parameters.get("EventcostAgentRObj"); 
		EventcostBean persistEventCostBean = eventCostAgent.getEventcostDetails(eventCostPK);
		calendarEventCostIntoBean(persistEventCostBean, eventCost, eventPK,
				false, parameters);
		Integer intCalendarEventCostPK = eventCostAgent.updateEventcost(persistEventCostBean);
		
		if(intCalendarEventCostPK == -2){
			sessionContext.setRollbackOnly();
			response.getIssues().add(new Issue(IssueTypes.ERROR_UPDATING_EVENT_COST));
			throw new OperationException("Error updating event cost");
		}
		return intCalendarEventCostPK;
		
	}
	//Virendra#7301,7547
	private static boolean isSiteFromSameAccount(Integer sitePK, Integer accountPK, Map<String, Object> parameters){
		
		SiteAgentRObj siteAgent =(SiteAgentRObj) parameters.get("SiteAgentRObj"); 
		SiteBean siteBean = siteAgent.getSiteDetails(sitePK);
		if(siteBean != null){
			Integer siteAccount = EJBUtil.stringToInteger(siteBean.getSiteAccountId());
			if(siteAccount.equals(accountPK)){
				return true;
			}
		}
		return false;
	}
	
	private ArrayList getSiteIDs(SiteAgentRObj siteAgent, UserBean callingUser)
	{
		
		CodeDao codeDao = new CodeDao();
		codeDao.getCodeValues("site_type","service");
		
		ArrayList siteType = new ArrayList();
		siteType = codeDao.getCId();
		
		SiteDao siteDao = siteAgent.getBySiteTypeCodeId(EJBUtil.stringToInteger(callingUser.getUserAccountId()),EJBUtil.ArrayListToString(codeDao.getCId()));
		
		ArrayList siteID = siteDao.getSiteIds();
		
		return siteID; 
	}

	

}
