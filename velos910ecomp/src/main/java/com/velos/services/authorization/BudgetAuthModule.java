/**
 * Created On Oct 6, 2011
 */
package com.velos.services.authorization;

import java.util.ArrayList;
import java.util.Hashtable;

import com.velos.eres.business.common.CtrlDao;
import com.velos.eres.business.common.TeamDao;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.esch.business.budget.impl.BudgetBean;
import com.velos.services.budget.BudgetServiceDAO;

/**
 * @author Kanwaldeep
 *
 */
public class BudgetAuthModule extends AbstractAuthModule {
	
	public static final String BUDGET = "BGTDET"; 
	public static final String BUDGET_ACCESS = "BGTACCESS"; 
	public static final String BUDGET_APPENDIX  = "BGTAPNDX";
	
	protected Hashtable<String, Integer> rightsTable = new Hashtable<String, Integer>();
	

	
	public BudgetAuthModule(BudgetBean budgetBean, UserBean userbean)
	{
		
		BudgetServiceDAO budgetDao = new BudgetServiceDAO(); 
		String budgetRights = budgetDao.getAccessRightsForBudget(budgetBean, userbean); 
		CtrlDao ctrl = new CtrlDao(); 
		ctrl.getControlValues("bgt_rights");
		ArrayList cVal = ctrl.getCValue();
		
		

	      for (int counter = 0; counter < cVal.size(); counter++) {
	        	String privilegeName = (String)cVal.get(counter);
	        	if (!privilegeName.startsWith(HEADER_PREFIX)){
	        		Integer privilegeInt = 
	        			new Integer(String.valueOf(budgetRights.charAt(counter)));
	
		        	rightsTable.put(
		        			privilegeName,
		        			privilegeInt); 
	        	}
	        }
	      
	  
	}
	
	public Integer getBudgetDetailsPriviledges(){
		return getRights(BUDGET);
	}
	
	public Integer getBudgetAppendixPriviledges()
	{
		return getRights(BUDGET_ACCESS); 
	}
	
	public Integer getBudgetAccessRightsPriviledges()
	{
		return getRights(BUDGET_APPENDIX); 
	}	
	
	private Integer getRights(String key){
		if (rightsTable.containsKey(key)){
			return rightsTable.get(key);
		}
		return 0;
	}

}
