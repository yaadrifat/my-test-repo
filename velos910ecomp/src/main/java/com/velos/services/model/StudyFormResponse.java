/**
 * Created On Sep 6, 2011
 */
package com.velos.services.model;

/**
 * @author Kanwaldeep
 *
 */
public class StudyFormResponse extends FormResponse{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6473030439276958183L;
	
	protected StudyIdentifier studyIdentifier;
	
	public StudyIdentifier getStudyIdentifier() {
		return studyIdentifier;
	}
	public void setStudyIdentifier(StudyIdentifier studyIdentifier) {
		this.studyIdentifier = studyIdentifier;
	}
}
