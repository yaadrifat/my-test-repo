/**
 * Created On Jul 25, 2011
 */
package com.velos.services.model;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Kanwaldeep
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="PatientProtocolIdentifier")	
public class PatientProtocolIdentifier extends SimpleIdentifier{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3785200184251025342L;

	public PatientProtocolIdentifier(){

	}

}
