/**
 * Created On Nov 9, 2011
 */
package com.velos.services.model;

/**
 * @author Kanwaldeep
 *
 */
public class AccountFormResponse extends FormResponse {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8029597666928291080L;
	
	protected Integer accountID;
	
	protected AccountFormResponseIdentifier formFilledFormId;

	public Integer getAccountID() {
		return accountID;
	}

	public void setAccountID(Integer accountID) {
		this.accountID = accountID;
	}

	public AccountFormResponseIdentifier getFormFilledFormId() {
		return formFilledFormId;
	}

	public void setFormFilledFormId(AccountFormResponseIdentifier formFilledFormId) {
		this.formFilledFormId = formFilledFormId;
	} 	
	
	
	

}
