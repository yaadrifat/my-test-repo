/**
 * Created On Sep 30, 2011
 */
package com.velos.services.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Kanwaldeep
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="NumberFieldValidations")
public class NumberFieldValidations extends TextFieldValidations{	

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8180521056925803865L;
	private NumberRange range; 
	private String format;
	private boolean overrideRange;	
	private boolean overrideFormat;
	
	
	public NumberRange getRange() {
		return range;
	}
	public void setRange(NumberRange range) {
		this.range = range;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	public boolean isOverrideRange() {
		return overrideRange;
	}
	public void setOverrideRange(boolean overrideRange) {
		this.overrideRange = overrideRange;
	}
	public boolean isOverrideFormat() {
		return overrideFormat;
	}
	public void setOverrideFormat(boolean overrideFormat) {
		this.overrideFormat = overrideFormat;
	}		

}
