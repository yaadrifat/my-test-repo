package com.velos.services.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="VisitIdentifier")
public class VisitIdentifier extends SimpleIdentifier{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
}