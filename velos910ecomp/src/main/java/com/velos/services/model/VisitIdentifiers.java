/**
 * Created On Jul 5, 2011
 */
package com.velos.services.model;

import java.util.List;

/**
 * @author Kanwaldeep
 *
 */
public class VisitIdentifiers extends ServiceObject {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8828866364960059725L;
	private List<VisitIdentifier> visitIdentifier;

	public List<VisitIdentifier> getVisitIdentifier() {
		return visitIdentifier;
	}

	public void setVisitIdentifier(List<VisitIdentifier> visitIdentifier) {
		this.visitIdentifier = visitIdentifier;
	} 
	
	public void addVisitIdentifier(VisitIdentifier visitIdentifier)
	{
		this.visitIdentifier.add(visitIdentifier); 
	}

}
