/**
 * Created On May 6, 2011
 */
package com.velos.services.model;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;

/**
 * @author Kanwaldeep
 *
 */
@XmlRootElement(name="Visit")
@XmlAccessorType(XmlAccessType.FIELD)
public class CalendarVisit extends ServiceObject{
	
	private static final long serialVersionUID = -2136767714648967858L;
	
	protected CalendarVisitSummary calendarVisitSummary; 
	
	protected CalendarEvents events;

	

	public CalendarVisit(){
		
	}
	

	@Valid
	public CalendarEvents getEvents() {
		return events;
	}

	public void setEvents(CalendarEvents events) {
		this.events = events;
	}

	@Valid
	public CalendarVisitSummary getCalendarVisitSummary() {
		return calendarVisitSummary;
	}


	public void setCalendarVisitSummary(CalendarVisitSummary calendarVisitSummary) {
		this.calendarVisitSummary = calendarVisitSummary;
	}	

}
