/**
 * Created On Jun 20, 2011
 */
package com.velos.services.model;

/**
 * @author Kanwaldeep
 *
 */
public enum AccessRights {
	GRANTED,
	REVOKED,
}
