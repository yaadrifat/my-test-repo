/**
 * Created On Aug 25, 2011
 */
package com.velos.services.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.velos.services.model.Duration.TimeUnits;
import com.velos.services.util.EnumUtil;

/**
 * @author Kanwaldeep
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Field")
public class FormField extends ServiceObject {


	private static final long serialVersionUID = 143987331903517973L;

	private FieldIdentifier fieldIdentifier; 
	
	private String name; 
	private String desc; 
	private Integer sequence; 

	private Boolean browserFld; 
	
	private String uniqueID; 
	private String systemID; 
	
	
	private FieldType fieldType;
	//comment Text
	
	// stores comment and help text
	private String instructions; 
	private Boolean isUnique; 
	
	private Boolean sameline; 
	private Alignment Align; 
	private Boolean bold; 
	private Boolean italics;
	private Boolean hideLabel; 
	private Integer displayWidth; 
	private Boolean expandLabel; 
	
//indicates whether field is result of repeated sections
//	private Boolean isRepeated; 
	private FieldCharacteristics characteristics; 
	private Boolean isUnderlined; 
	private int length;
	private Boolean isReadOnly;
	private String fieldNameFormat;
		
	

	private SortOrder sortOrder;

	private DataType dataType; 

	
// Lookup Fields
	private LookupType lookUpType; 
	private String lookUpDisplayValue; 
	private String lookUpDataValue; 	
//	private LookUpIdentifier lookup;  Need feedack from Sonia to how to support lookup for now
	
    private Integer decimal; 
    
// not needed for Services as it is form related fields only which are being returned
//	private fld_libflag
    
    // Needed for Queries
    private  String keyword; 
    
    private MultipleChoices choices; 

	//Preview fields not needed for services????  
//	private Boolean fld_hideresplabel
//	private Boolean fld_hidelabel

 // May not be needed for services
//	private fld_explabel   
//	private fld_colcount
    
    private FieldValidations validations; 
    
	
		public enum SortOrder {
			DESCENDING("D"),
			ASCENDING("A"); 
			
			private String value;
			private SortOrder(String value)
			{
				this.value = value; 
			}
			public String toString()
			{
				return value; 
			}
		}
		
		
		public enum DataType
		{
			EDIT_BOX_TEXT("ET"),
			EDIT_BOX_NUMBER("EN"),
			EDIT_BOX_DATE("ED"),
			EDIT_BOX_TIME("EI"),
			MULTIPLE_CHOICE_LOOKUP("ML"),
			MULTIPLE_CHOICE_DROPDOWN("MD"),
			MULTIPLE_CHOICE_CHECKBOX("MC"),
			MULTIPLE_CHOICE_RADIO_BUTTON("MR"); 
			private String value; 
			private DataType(String value)
			{
				this.value = value; 
			}
			
			public String toString()
			{
				return value;
			}
		}
		
		public enum FieldCharacteristics
		{
			VISIBLE("0"),
			HIDE("1"),
			DISABLED("2"),
			READONLY("3"); 
			
			private String visible; 
	
			private FieldCharacteristics(String visible)
			{
				this.visible = visible; 
			}
			
			public String toString()
			{
				return visible; 
			}
					
		}
		
		public enum FieldType
		{
			COMMENT("C"),
			EDIT_BOX("E"),
			MULTIPLE_CHOICE("M"),
			HORIZONTAL_LINE("H"),
			SPACER("S"),
			FORM_LINKS("F"); 
			
			private String value; 
			private FieldType(String value)
			{
				this.value = value; 
			}
			
			public String toString()
			{
				return value; 
			}
		}
		
		public enum Alignment
		{
			LEFT("left"),
			RIGHT("right"),
			CENTER("center"),
			TOP("top"); 
			
			private String value; 
			private Alignment(String value)
			{
				this.value = value; 
			}
			
			public String toString()
			{
				return value; 
			}
		}
		
		
		public enum LookupType		
		{
			ADHOC_QUERY_LOOKUP,
			NORMAL_LOOKUP
		}
		
		

		public FieldIdentifier getFieldIdentifier() {
			return fieldIdentifier;
		}

		public void setFieldIdentifier(FieldIdentifier fieldIdentifier) {
			this.fieldIdentifier = fieldIdentifier;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}

		public Boolean isBrowserFld() {
			return browserFld;
		}

		public void setBrowserFld(Boolean browserFld) {
			this.browserFld = browserFld;
		}

		public FieldType getFieldType() {
			return fieldType;
		}

		public void setFieldType(FieldType fieldType) {
			this.fieldType = fieldType;
		}
		
		public void setFieldType(String fieldType)
		{
			this.fieldType = EnumUtil.getEnumType(FieldType.class,fieldType); 
		}

		public String getInstructions() {
			return instructions;
		}

		public void setInstructions(String instructions) {
			this.instructions = instructions;
		}	

		public Boolean isUnique() {
			return isUnique;
		}

		public void setUnique(Boolean isUnique) {
			this.isUnique = isUnique;
		}
		
		public Boolean isUnderlined() {
			return isUnderlined;
		}

		public void setUnderlined(Boolean isUnderlined) {
			this.isUnderlined = isUnderlined;
		}

		public int getLength() {
			return length;
		}

		public void setLength(int length) {
			this.length = length;
		}

		public Boolean isReadOnly() {
			return isReadOnly;
		}

		public void setReadOnly(Boolean isReadOnly) {
			this.isReadOnly = isReadOnly;
		}

		public String getFieldNameFormat() {
			return fieldNameFormat;
		}

		public void setFieldNameFormat(String fieldNameFormat) {
			this.fieldNameFormat = fieldNameFormat;
		}

		public DataType getDataType() {
			return dataType;
		}

		public void setDataType(DataType dataType) {
			this.dataType = dataType;
		}
		
		public void setDataType(String dataType){
			this.dataType = EnumUtil.getEnumType(DataType.class, dataType); 
		}

		public LookupType getLookUpType() {
			return lookUpType;
		}

		public void setLookUpType(LookupType lookUpType) {
			this.lookUpType = lookUpType;
		}

		public String getLookUpDisplayValue() {
			return lookUpDisplayValue;
		}

		public void setLookUpDisplayValue(String lookUpDisplayValue) {
			this.lookUpDisplayValue = lookUpDisplayValue;
		}

		public String getLookUpDataValue() {
			return lookUpDataValue;
		}

		public void setLookUpDataValue(String lookUpDataValue) {
			this.lookUpDataValue = lookUpDataValue;
		}

		public Integer getDecimal() {
			return decimal;
		}

		public void setDecimal(Integer decimal) {
			this.decimal = decimal;
		}

		public String getKeyword() {
			return keyword;
		}

		public void setKeyword(String keyword) {
			this.keyword = keyword;
		}

		
		public String getUniqueID() {
			return uniqueID;
		}

		public void setUniqueID(String uniqueID) {
			this.uniqueID = uniqueID;
		}

		public String getSystemID() {
			return systemID;
		}

		public void setSystemID(String systemID) {
			this.systemID = systemID;
		}

		public Boolean isSameline() {
			return sameline;
		}

		public void setSameline(Boolean sameline) {
			this.sameline = sameline;
		}

		public SortOrder getSortOrder() {
			return sortOrder;
		}

		public void setSortOrder(String sortOrder) {
			this.sortOrder = EnumUtil.getEnumType(SortOrder.class, sortOrder);
		}		

		public MultipleChoices getChoices() {
			return choices;
		}

		public void setChoices(MultipleChoices choices) {
			this.choices = choices;
		}
		/**
		 * @param validations the validations to set
		 */
		public void setValidations(FieldValidations validations) {
			this.validations = validations;
		}

		/**
		 * @return the validations
		 */
		public FieldValidations getValidations() {
			return validations;
		}

		/**
		 * @param characteristics the characteristics to set
		 */
		public void setCharacteristics(String characteristics) {
			this.characteristics = EnumUtil.getEnumType(FieldCharacteristics.class, characteristics);
		}
		/**
		 * @return the characteristics
		 */
		public FieldCharacteristics getCharacteristics() {
			return characteristics;
		}

		public void setCharacteristics(FieldCharacteristics characteristics) {
			this.characteristics = characteristics;
		}

		public Alignment getAlign() {
			return Align;
		}

		public void setAlign(Alignment align) {
			Align = align;
		}

		public void setSortOrder(SortOrder sortOrder) {
			this.sortOrder = sortOrder;
		}

		public Boolean isBold() {
			return bold;
		}

		public void setBold(Boolean bold) {
			this.bold = bold;
		}

		public Boolean isItalics() {
			return italics;
		}

		public void setItalics(Boolean italics) {
			this.italics = italics;
		}

		public Boolean isHideLabel() {
			return hideLabel;
		}

		public void setHideLabel(Boolean hideLabel) {
			this.hideLabel = hideLabel;
		}

		public Integer getDisplayWidth() {
			return displayWidth;
		}

		public void setDisplayWidth(Integer displayWidth) {
			this.displayWidth = displayWidth;
		}

		public Boolean isExpandLabel() {
			return expandLabel;
		}

		public void setExpandLabel(Boolean expandLabel) {
			this.expandLabel = expandLabel;
		}

		/**
		 * @param sequence the sequence to set
		 */
		public void setSequence(Integer sequence) {
			this.sequence = sequence;
		}

		/**
		 * @return the sequence
		 */
		public Integer getSequence() {
			return sequence;
		}	
		
		
		
		
}
