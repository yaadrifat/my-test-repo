package com.velos.services.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Visits")
public class Visits extends ServiceObject {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1411000301993694675L;
	private List<Visit> visit = new ArrayList<Visit>();

	public List<Visit> getVisit() {
		return visit;
	}

	public void setVisit(List<Visit> visit) {
		this.visit = visit;
	} 
	
	public void addVisit(Visit visit)
	{
		this.visit.add(visit); 
	}

}
