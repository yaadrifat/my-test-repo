/**
 * Created On Jul 25, 2011
 */
package com.velos.services.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Kanwaldeep
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="PatientStudyStatusIdentifier")	
public class PatientStudyStatusIdentifier extends SimpleIdentifier{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7148988896885514374L;

	public PatientStudyStatusIdentifier()
	{
		
	}
}
