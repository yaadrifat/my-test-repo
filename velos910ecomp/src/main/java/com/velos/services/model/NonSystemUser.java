/**
 * Created On Aug 8, 2012
 */
package com.velos.services.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.velos.services.model.User.UserStatus;

/**
 * @author Kanwaldeep
 *
 */
@XmlRootElement(name="NonSystemUser")
@XmlAccessorType(XmlAccessType.FIELD)
public class NonSystemUser extends ServiceObject {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1235053588912828078L;
	protected String firstName; 
	protected String lastName;
	protected String address; 
	protected String city; 
	protected String state; 
	protected String zip; 
	protected String country; 
	protected String phone; 
	protected String email;
	protected Code jobType; 
	protected Code primarySpecialty; 
	protected OrganizationIdentifier organization; 
	protected UserStatus userStatus;
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Code getJobType() {
		return jobType;
	}
	public void setJobType(Code jobType) {
		this.jobType = jobType;
	}
	public Code getPrimarySpecialty() {
		return primarySpecialty;
	}
	public void setPrimarySpecialty(Code primarySpecialty) {
		this.primarySpecialty = primarySpecialty;
	}
	public OrganizationIdentifier getOrganization() {
		return organization;
	}
	public void setOrganization(OrganizationIdentifier organization) {
		this.organization = organization;
	}
	public UserStatus getUserStatus() {
		return userStatus;
	}
	public void setUserStatus(UserStatus userStatus) {
		this.userStatus = userStatus;
	} 
}
