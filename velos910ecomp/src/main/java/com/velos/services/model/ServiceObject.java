
package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.velos.services.model.ParentIdentifier;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="ServiceObject")

/**
 * Class that identifies all transfer objects that can be identified using 
 * an {@link SimpleIdentifer} object.
 */
public abstract class ServiceObject implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 8967394295630756550L;

	protected SimpleIdentifier systemId;
	
	//virendra;Fixed#6124, overloading setParentId for studyIdentifier
	protected ParentIdentifier parentIdentifier;
	
//    protected CustomizableItemList extensionFields;
//
////    /**
////     * Gets the value of the externalId property.
////     * 
////     * @return
////     *     possible object is
////     *     {@link String }
////     *     
////     */
////    public SimpleIdentifier getSystemId() {
////        return systemId;
////    }
////
////    /**
////     * Sets the value of the externalId property.
////     * 
////     * @param value
////     *     allowed object is
////     *     {@link String }
////     *     
////     */
////    public void setSystemId(SimpleIdentifier systemId) {
////        this.systemId = systemId;
////    }
//
//    /**
//     * Gets the value of the extensionFields property.
//     * 
//     * @return
//     *     possible object is
//     *     {@link CustomizableItemList }
//     *     
//     */
//    public CustomizableItemList getExtensionFields() {
//        return extensionFields;
//    }
//
//    /**
//     * Sets the value of the extensionFields property.
//     * 
//     * @param value
//     *     allowed object is
//     *     {@link CustomizableItemList }
//     *     
//     */
//    public void setExtensionFields(CustomizableItemList value) {
//        this.extensionFields = value;
//    }
//
//  IH: Commenting out the getter and setter so this field won't show in WSDL.
//  The getting and setter should go in the subclass of ServiceObject.
//	public SimpleIdentifier getParentIdentifier() {
//		return parentIdentifier;
//	}
//
//	public void setParentIdentifier(SimpleIdentifier parentIdentifier) {
//		this.parentIdentifier = parentIdentifier;
//	}
	

 }
