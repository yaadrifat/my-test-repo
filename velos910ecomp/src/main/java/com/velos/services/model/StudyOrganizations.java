/**
 * Created On May 31, 2011
 */
package com.velos.services.model;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Kanwaldeep
 *
 */
public class StudyOrganizations extends ServiceObject {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1876809328393402092L;
	protected List<StudyOrganization> studyOrganizaton;

	@NotNull
	@Size(min=1)
	@Valid
	public List<StudyOrganization> getStudyOrganizaton() {
		return studyOrganizaton;
	}

	public void setStudyOrganizaton(List<StudyOrganization> studyOrganizaton) {
		this.studyOrganizaton = studyOrganizaton;
	} 
	
	 public StudyIdentifier getParentIdentifier() {
	        if (super.parentIdentifier == null || 
	                super.parentIdentifier.getId().size() < 1) { return null; }
	        for (SimpleIdentifier myId : super.parentIdentifier.getId()) {
	            if (myId instanceof StudyIdentifier) {
	                return (StudyIdentifier)myId;
	            }
	        }
	        return null;
	    }
	    
	    public void setParentIdentifier(StudyIdentifier studyIdentifer) {
	        List<SimpleIdentifier> idList = new ArrayList<SimpleIdentifier>();
	        idList.add(studyIdentifer);
	        super.parentIdentifier = new ParentIdentifier();
			super.parentIdentifier.setId(idList);
		}

}
