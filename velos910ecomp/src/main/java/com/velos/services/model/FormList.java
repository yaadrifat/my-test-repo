/**
 * Created On Dec 2, 2011
 */
package com.velos.services.model;

import java.util.List;

/**
 * @author Kanwaldeep
 *
 */
public class FormList extends ServiceObject {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2740539440306021105L;
	protected List<FormInfo> formInfo;

	public List<FormInfo> getFormInfo() {
		return formInfo;
	}

	public void setFormInfo(List<FormInfo> formInfo) {
		this.formInfo = formInfo;
	} 
}
