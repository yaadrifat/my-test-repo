/**
 * Created On Jun 17, 2011
 */
package com.velos.services.model;

import javax.validation.Valid;


/**
 * @author Kanwaldeep
 *
 */
public class Patient extends ServiceObject{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 7805927634808969078L;

	protected PatientDemographics patientDemographics;

	protected PatientOrganizations patientOrganizations;
    
    protected PatientIdentifier patientIdentifier; 

    @Valid
    public PatientDemographics getPatientDemographics() {
		return patientDemographics;
	}

	public void setPatientDemographics(PatientDemographics patientDemographics) {
		this.patientDemographics = patientDemographics;
	}
	@Valid
	public PatientOrganizations getPatientOrganizations() {
		return patientOrganizations;
	}

	public void setPatientOrganizations(PatientOrganizations patientOrganizations) {
		this.patientOrganizations = patientOrganizations;
	}

	public PatientIdentifier getPatientIdentifier() {
		return patientIdentifier;
	}

	public void setPatientIdentifier(PatientIdentifier patientIdentifier) {
		this.patientIdentifier = patientIdentifier;
	}



}
