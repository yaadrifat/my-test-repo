/**
 * Created On Nov 9, 2011
 */
package com.velos.services.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Kanwaldeep
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="FieldResponses")

public class FormFieldResponses extends ServiceObject {


	private List<FormFieldResponse> field = new ArrayList<FormFieldResponse>();

	/**
	 * @param field the field to set
	 */
	public void setField(List<FormFieldResponse> field) {
		this.field = field;
	}

	/**
	 * @return the field
	 */
	public List<FormFieldResponse> getField() {
		return field;
	} 


	public void addField(FormFieldResponse field)
	{
		this.field.add(field); 
	}


}
