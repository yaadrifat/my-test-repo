package com.velos.services.patientdemographics;

import javax.ejb.Remote;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.UpdatePatientDemographics;



@Remote
public interface UpdatePatientDemographicsService
{
	
		public ResponseHolder UpdatePatientDemographics(PatientIdentifier patId, UpdatePatientDemographics updatePatDemo) throws OperationException;
}
