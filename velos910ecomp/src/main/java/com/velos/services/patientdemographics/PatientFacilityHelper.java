package com.velos.services.patientdemographics;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.SessionContext;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.business.common.PatFacilityDao;
import com.velos.eres.business.patFacility.impl.PatFacilityBean;
import com.velos.eres.business.person.impl.PersonBean;
import com.velos.eres.business.pref.impl.PrefBean;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.patFacilityAgent.PatFacilityAgentRObj;
import com.velos.eres.service.personAgent.PersonAgentRObj;
import com.velos.eres.service.prefAgent.PrefAgentRObj;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.userSiteAgent.UserSiteAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.services.AbstractService;
import com.velos.services.AuthorizationException;
import com.velos.services.CodeNotFoundException;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.authorization.AbstractAuthModule;
import com.velos.services.authorization.GroupAuthModule;
import com.velos.services.map.MultipleObjectsFoundException;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.AccessRights;
import com.velos.services.model.Code;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientOrganization;
import com.velos.services.model.PatientOrganizationIdentifier;
import com.velos.services.util.CodeCache;
import com.velos.services.util.ObjectLocator;

public class PatientFacilityHelper extends CommonDAO
{
	private static final long serialVersionUID = 8445240983096035188L;
	private static Logger logger = Logger.getLogger(PatientFacilityHelper.class);
	
	
	public int addPatientFacility(PatientIdentifier patId,PatientOrganization patOrg,Map<String, Object> parameters) throws OperationException
	{
		
		Integer orgPK=null;
		UserBean callingUser=(UserBean) parameters.get("callingUser");
		SessionContext sessionContext = (SessionContext)parameters.get("sessionContext");
		ObjectMapService objectMapService = (ObjectMapService)parameters.get("objectMapService");
		UserSiteAgentRObj userSiteAgent = (UserSiteAgentRObj) parameters.get("userSiteAgent");
		PatFacilityAgentRObj patFacilityAgent=(PatFacilityAgentRObj) parameters.get("patFacilityAgent");
		UserAgentRObj userAgent = (UserAgentRObj)parameters.get("userAgent");
		Integer personPk=(Integer) parameters.get("personPk");
		
		validatePatientOrganization(patOrg,parameters);
		
		//-----------------------------------------------getting sitePk of organization to be added----------------------------------------------------------
		try
		{
			orgPK = ObjectLocator.sitePKFromIdentifier(callingUser, patOrg.getOrganizationID(), sessionContext, objectMapService);
			
		}
	    catch (MultipleObjectsFoundException moe)
	    {
	    	((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
	          new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND, "Found multiple organizations for OID:" + 
	          patOrg.getOrganizationID().getOID() + " siteAltID:" + patOrg.getOrganizationID().getSiteAltId() + " siteName:" + patOrg.getOrganizationID().getSiteName()));

	        throw new OperationException();
	      }

	      if (orgPK==null || orgPK.intValue() == 0)
	      {
	        ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
	          new Issue(IssueTypes.ORGANIZATION_NOT_FOUND, "Organization not found for OID:" + 
	        		  patOrg.getOrganizationID().getOID() + " siteAltID:" + patOrg.getOrganizationID().getSiteAltId() + " siteName:" + patOrg.getOrganizationID().getSiteName()));

	        throw new OperationException();
	      }
	      //---------------------------------checking access rights of calling user to the organization to be added------------------------------------------------
	      Integer accessRights=userSiteAgent.getRightForUserSite(callingUser.getUserId(),orgPK );
	      if(!AbstractAuthModule.hasNewPermission(accessRights))
	      {
		        ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
		  	          new Issue(IssueTypes.ORGANIZATION_AUTHORIZATION, "User not authorized to access the organization to be added with OID : " + 
		  	        		  patOrg.getOrganizationID().getOID() + " siteAltID:" + patOrg.getOrganizationID().getSiteAltId() + " siteName:" + patOrg.getOrganizationID().getSiteName()));

		  	        throw new OperationException();
		  }
	      
	      //---------------------------------checking if patient already registered to the organization or not--------------------------------------
			PatFacilityDao patFacilityDao = patFacilityAgent.getPatientFacilities(personPk);
			int facilityCount = (patFacilityDao.getId()).size();
			boolean isPatientAlreadyRegistered=false;
			for (int patFacilityCount = 0; patFacilityCount < facilityCount ; patFacilityCount++)
			{
				String sitePK = patFacilityDao.getPatientSite().get(patFacilityCount).toString();
				if(sitePK.equals(EJBUtil.integerToString(orgPK)))
				{
					isPatientAlreadyRegistered=true;
					break;
				}
			}
			
		     if(isPatientAlreadyRegistered)
		     {
			        ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
				  	          new Issue(IssueTypes.PATIENT_ALREADY_REGISTERED_TO_THIS_ORGANIZATION, "Patient is already registered to the organization with OID : " + 
				  	        		  patOrg.getOrganizationID().getOID() + " siteAltID:" + patOrg.getOrganizationID().getSiteAltId() + " siteName:" + patOrg.getOrganizationID().getSiteName()));

				  	        throw new OperationException();
			 }
		     
		     PatFacilityBean facilityBean = new PatFacilityBean();
		     facilityBean.setIPAdd(AbstractService.IP_ADDRESS_FIELD_VALUE);
		     facilityBean.setIsDefault(patOrg.isDefault() ? "1" : "0");
		     facilityBean.setCreator(EJBUtil.integerToString(callingUser.getUserId()));
		     facilityBean.setPatAccessFlag(patOrg.getAccess().equals(AccessRights.GRANTED) ? "7" : "0");
		     
		     PatientFacilityDAO addPatientFacilityServiceDao=new PatientFacilityDAO();
		     if(addPatientFacilityServiceDao.isFacilityIdExists(patOrg.getFacilityID(),orgPK.intValue()))
		     {
		    	 ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
			              new Issue(IssueTypes.PATIENT_FACILITY_ID_ALREADY_EXISTS, "Patient Facility Id \""+patOrg.getFacilityID()+"\" already exists for organization with OID : " + 
				  	        		  patOrg.getOrganizationID().getOID() + " siteAltID:" + patOrg.getOrganizationID().getSiteAltId() + " siteName:" + patOrg.getOrganizationID().getSiteName()));
			            throw new OperationException();
		     }
		     facilityBean.setPatientFacilityId(patOrg.getFacilityID());
		     
		     String registeredBy = "";
		     try
		      {
		        if (patOrg.getProvider() != null)
		        {
		          UserBean bean = ObjectLocator.userBeanFromIdentifier(callingUser, patOrg.getProvider(), userAgent, sessionContext, objectMapService);
		          if (bean == null)
		          {
		            ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
		              new Issue(IssueTypes.USER_NOT_FOUND, "Provider not found for OID:" + 
		              patOrg.getProvider().getOID() + " FirstName:" + patOrg.getProvider().getFirstName() + " lastName:" + patOrg.getProvider().getLastName() + " UserLoginName:" + patOrg.getProvider().getUserLoginName()));
		            throw new OperationException();
		          }
		          
		        /*  Integer providerAccessRights=userSiteAgent.getRightForUserSite(bean.getUserId(),orgPK );
		 	      boolean hasViewPatientPermissions = GroupAuthModule.hasViewPermission(providerAccessRights);
		          if (!hasViewPatientPermissions)
					{
		        	  ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.GROUP_AUTHORIZATION,"Provider is not authorized for this organization with OID : " + 
		  	        		  patOrg.getOrganizationID().getOID() + " siteAltID:" + patOrg.getOrganizationID().getSiteAltId() + " siteName:" + patOrg.getOrganizationID().getSiteName()));
						throw new AuthorizationException("Provider is not authorized for this organization with OID : " + 
		  	        		  patOrg.getOrganizationID().getOID() + " siteAltID:" + patOrg.getOrganizationID().getSiteAltId() + " siteName:" + patOrg.getOrganizationID().getSiteName());
					}*/
					
		          registeredBy = EJBUtil.integerToString(bean.getUserId());
		        }
		      }
		      catch (MultipleObjectsFoundException mce) {
		        ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
		          new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND, "Found multiple Users for OID:" + 
		          patOrg.getProvider().getOID() + " FirstName:" + patOrg.getProvider().getFirstName() + " lastName:" + patOrg.getProvider().getLastName() + " UserLoginName:" + patOrg.getProvider().getUserLoginName()));
		        throw new OperationException();
		      }

		      facilityBean.setRegisteredBy(registeredBy);

		      facilityBean.setRegisteredByOther(patOrg.getOtherProvider());
		      facilityBean.setRegDate(patOrg.getRegistrationDate());
		      facilityBean.setPatientPK(EJBUtil.integerToString(personPk));
		      facilityBean.setPatientSite(EJBUtil.integerToString(Integer.valueOf(orgPK)));

		      if ((patOrg.getSpecialtyAccess() != null) && (patOrg.getSpecialtyAccess().getSpeciality() != null))
		      {
		        List specialties = patOrg.getSpecialtyAccess().getSpeciality();
		        StringBuffer specialtyStr = new StringBuffer();

		        for (int x = 0; x < specialties.size(); x++)
		        {
		          if (x != 0) specialtyStr.append(","); try
		          {
		            specialtyStr.append(AbstractService.dereferenceCodeStr((Code)specialties.get(x),CodeCache.CODE_TYPE_PRIMARY_SPECIALTY, callingUser));
		          }
		          catch (CodeNotFoundException cde) {
		            ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
		              new Issue(
		              IssueTypes.DATA_VALIDATION, 
		              "Specialty Code Not Found: " + ((Code)specialties.get(x)).getCode()));
		            throw new OperationException();
		          }
		        }
		        facilityBean.setPatSpecialtylAccess(specialtyStr.toString());
		      }
 		     
		      int result=patFacilityAgent.setPatFacilityDetails(facilityBean);
		      if (patOrg.isDefault())
	            {
	            	PersonAgentRObj personAgent = (PersonAgentRObj)parameters.get("personAgent");
	            	String personPK = parameters.get("personPk").toString(); 
	            	PersonBean perBean = personAgent.getPersonDetails(StringUtil.stringToInteger(personPK));
	            	perBean.setPersonRegDate(DateUtil.dateToString(facilityBean.getRegDate()));
	        		perBean.setPersonRegBy(facilityBean.getRegisteredBy());
	        		perBean.setPhyOther(facilityBean.getRegisteredByOther());
	        		perBean.setPersonSplAccess(facilityBean.getPatSpecialtylAccess());
	        		perBean.setPersonLocation(facilityBean.getPatientSite());
	        		perBean.setPatientFacilityId(facilityBean.getPatientFacilityId());
	        		perBean.setModifiedBy(StringUtil.integerToString(callingUser.getUserId()));
	        		
	        		PrefAgentRObj prefAgent = (PrefAgentRObj)parameters.get("prefAgent"); 
	        		PrefBean pref = prefAgent.getPrefDetails(StringUtil.stringToInteger(personPK));
	        		pref.setPrefPId(perBean.getPersonPId());
	        		pref.setPrefAccount(perBean.getPersonAccount());
	        		pref.setPrefLocation(perBean.getPersonLocation());
	        		pref.setModifiedBy(perBean.getModifiedBy());
	        		pref.setIpAdd(perBean.getIpAdd());
	        		pref.setPatientFacilityId(perBean.getPatientFacilityId());
	        		if((prefAgent.updatePref(pref))<0)
	        		{
	        			((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
						        new Issue(
						        IssueTypes.PATIENT_ERROR_UPDATE_ORGANIZATION));
						      throw new OperationException();
	        		}
	        		if((personAgent.updatePerson(perBean))<0)
	        		{
	        			((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
						        new Issue(
						        IssueTypes.PATIENT_ERROR_UPDATE_ORGANIZATION));
						      throw new OperationException();
	        		}

	             }
		return result; 
		
	}


	private void validatePatientOrganization(PatientOrganization patOrg,Map<String,Object> parameters) throws OperationException
	{
		if(patOrg.getOrganizationID()==null)
		{
			((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(new Issue(
			        IssueTypes.DATA_VALIDATION,"Valid OrganizationIdentifier is required"));
			      throw new OperationException();
		}
		if((patOrg.getOrganizationID().getOID()==null || patOrg.getOrganizationID().getOID().trim().length()==0) 
				&& (patOrg.getOrganizationID().getSiteAltId()==null || patOrg.getOrganizationID().getSiteAltId().trim().length()==0) 
				&& (patOrg.getOrganizationID().getSiteName()==null || patOrg.getOrganizationID().getSiteName().trim().length()==0))
		{
			((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(new Issue(
			        IssueTypes.DATA_VALIDATION,"Valid OrganizationIdentifier is required with OID or siteAltID or siteName"));
			      throw new OperationException();
		}
		if(patOrg.getFacilityID()==null || patOrg.getFacilityID().trim().length()==0)
		{
			((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(new Issue(
			        IssueTypes.DATA_VALIDATION,"FacilityId is required"));
			      throw new OperationException();
		}
		if(patOrg.getAccess()==null || patOrg.getAccess().toString().trim().length()==0)
		{
			((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(new Issue(
			          IssueTypes.DATA_VALIDATION, 
			          "Access can be either GRANTED or REVOKED"));
			      throw new OperationException();
		}
		
	}
	

	
	
	public int updatePatientfacility(PatientOrganization patOrg,Map<String, Object> parameters,PatFacilityBean patBean)
			  throws OperationException
			{
				  UserBean callingUser = (UserBean)parameters.get("callingUser");
			      SessionContext sessionContext = (SessionContext)parameters.get("sessionContext");
			      ObjectMapService objectMapService = (ObjectMapService)parameters.get("objectMapService");
			      UserAgentRObj userAgent = (UserAgentRObj)parameters.get("userAgent");
			      
			      UserSiteAgentRObj userSiteAgent = (UserSiteAgentRObj)parameters.get("userSiteAgent");
			      PatFacilityAgentRObj patFacilityAgent = (PatFacilityAgentRObj)parameters.get("patFacilityAgent");
			      Integer sitePK = 0;
			            
			      PatFacilityBean facilityBean = new PatFacilityBean();
			          
			      if(patOrg != null)
			      {
			    	  if(patOrg.getOrganizationID()!=null)
			    	  {
			    		  try
					      	{
					          	sitePK = ObjectLocator.sitePKFromIdentifier(callingUser, patOrg.getOrganizationID(), sessionContext, objectMapService);
					          			          	
					      	}
					        catch (MultipleObjectsFoundException moe)
					        {
					          ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
					            new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND, "Found multiple organizations for OID:" + 
					            patOrg.getOrganizationID().getOID() + " siteAltID:" + patOrg.getOrganizationID().getSiteAltId() + " siteName:" + patOrg.getOrganizationID().getSiteName()));
				
					          throw new OperationException();
					        }
			    		  if (sitePK == null || sitePK.intValue() == 0) 
							{
					           ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
					             new Issue(IssueTypes.ORGANIZATION_NOT_FOUND, "Organization not found for OID:" + 
					             patOrg.getOrganizationID().getOID() + " siteAltID:" + patOrg.getOrganizationID().getSiteAltId() + " siteName:" + patOrg.getOrganizationID().getSiteName()));
		   			             throw new OperationException();
					         }
					      	 
						      	Integer userSiteRight = Integer.valueOf(userSiteAgent.getRightForUserSite(callingUser.getUserId().intValue(),sitePK.intValue())); 
						      	if (!AbstractAuthModule.hasEditPermission(userSiteRight))
						      	{
						      		((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
				      	 				new Issue(
				      	 						IssueTypes.ORGANIZATION_AUTHORIZATION, 
				      	 						"User not Authorized to update Patient to this Organization " + 
				      	 						patOrg.getOrganizationID().getOID() + " siteAltID:" + patOrg.getOrganizationID().getSiteAltId() + " siteName:" + patOrg.getOrganizationID().getSiteName()));
				      	 						throw new AuthorizationException();
						      	}
				      	 	
						      if(!(sitePK.intValue() == (EJBUtil.stringToInteger(patBean.getPatientSite()))))
						      {
						    	  List<Integer> existingOrganization = new ArrayList<Integer>();
						    	  PatFacilityDao facilityDao = patFacilityAgent.getPatientFacilities(EJBUtil.stringToNum(patBean.getPatientPK()));
						    	  for (int i = 0; i < facilityDao.getPatientSite().size(); i++)
						    	  {
						    		  existingOrganization.add(EJBUtil.stringToInteger(facilityDao.getPatientSite().get(i).toString()));
						    	  }
			    	       
						    	  if(existingOrganization.contains(sitePK))
						    	  {	  
						    		  ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.PATIENT_ALREADY_REGISTERED_TO_THIS_ORGANIZATION, "Patient is already registered to this Organization"));
						    		  throw new AuthorizationException();
				      	 	
						    	  }
						      }
						    	  facilityBean.setPatientSite(EJBUtil.integerToString(sitePK)); 
					     }	  
			    	  else
			    	  {
			    		  sitePK=EJBUtil.stringToInteger(patBean.getPatientSite());
			    		  facilityBean.setPatientSite(patBean.getPatientSite()); 
			    	  }	  
			    	  
			      } 
			      else
		    	  {
		    		  
			    	  ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.DATA_VALIDATION, "Valid PatientOrganization object is required"));
						throw new OperationException();  
			    	  
		    	  }
			     
			      	 facilityBean.setIPAdd(AbstractService.IP_ADDRESS_FIELD_VALUE);
			      
			       if(!patOrg.isDefault())
			       {
			    	   facilityBean.setIsDefault(patBean.isDefault);
			       } 
			       else
			       {   
			      	 if((!patOrg.isDefault())&& (patBean.getIsDefault().equals("1")))
			    	  {
			    		  ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
		    				  new Issue(IssueTypes.DATA_VALIDATION,"Organization already set to default can not be set not default"));
			    		  throw new OperationException();
			    	  }
			    	else
			        {
			    	  facilityBean.setIsDefault(patOrg.isDefault() ? "1" : "0");
				        
			        }
			       }	 
			      facilityBean.setLastModifiedBy(EJBUtil.integerToString(callingUser.getUserId()));
			      if(patOrg.getAccess() == null)
			      {
			    	  facilityBean.setPatAccessFlag(patBean.getPatAccessFlag());
			      }	  
			      else
			      {	  
			    	  if (patOrg.getAccess() != null)
			    	  {
			    		  facilityBean.setPatAccessFlag(patOrg.getAccess().equals(AccessRights.GRANTED) ? "7" : "0");
			    	  }
			    	  else
			    	  {
			    		  ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
			    				  new Issue(IssueTypes.DATA_VALIDATION,"Access can be either GRANTED or REVOKED"));
			    		  throw new OperationException();
			    	  }
			      }
			      
			      String registeredBy = "";
			      UserBean bean = null;
			      try
			      {
			           if (patOrg.getProvider() != null)
			           {
			           if((StringUtil.isEmpty(patOrg.getProvider().getFirstName())) &&
			             (StringUtil.isEmpty(patOrg.getProvider().getLastName())) &&
			             (StringUtil.isEmpty(patOrg.getProvider().getUserLoginName())) &&
			             (StringUtil.isEmpty(patOrg.getProvider().getOID()))
			             )
			           {
			           registeredBy="";
			           }
			           else
			           {
			              bean = ObjectLocator.userBeanFromIdentifier(callingUser, patOrg.getProvider(), userAgent, sessionContext, objectMapService);
			                if (bean == null)
			                {
			                  ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
			                    new Issue(IssueTypes.USER_NOT_FOUND, "Provider not found for OID:" + 
			                    patOrg.getProvider().getOID() + " FirstName:" + patOrg.getProvider().getFirstName() + " lastName:" + patOrg.getProvider().getLastName() + " UserLoginName:" + patOrg.getProvider().getUserLoginName()));
			                  throw new OperationException();
			                }
			                  
			               // checking provider rights for organization ------ 
			                 
			                 Integer providerSiteRight = Integer.valueOf(userSiteAgent.getRightForUserSite(bean.getUserId().intValue(),sitePK.intValue())); 
			                 if (!AbstractAuthModule.hasEditPermission(providerSiteRight))
			                  {
			                  ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
			                   new Issue(IssueTypes.ORGANIZATION_AUTHORIZATION, 
			              "Provider not Authorized to update Patient to this Organization " + 
			               patOrg.getOrganizationID().getOID() + " siteAltID:" + patOrg.getOrganizationID().getSiteAltId() + " siteName:" + patOrg.getOrganizationID().getSiteName()));
			                   throw new AuthorizationException();
			                  }
			                             
			                registeredBy = EJBUtil.integerToString(bean.getUserId());
			              }
			             
			            
			           }
			      }
			      catch (MultipleObjectsFoundException mce)
			      {
			        ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
			          new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND, "Found multiple Users for OID:" + 
			          patOrg.getProvider().getOID() + " FirstName:" + patOrg.getProvider().getFirstName() + " lastName:" + patOrg.getProvider().getLastName() + " UserLoginName:" + patOrg.getProvider().getUserLoginName()));
			        throw new OperationException();
			      }
			    	
			      facilityBean.setId(patBean.getId()); 
			      facilityBean.setRegisteredBy(registeredBy);

			      if(patOrg.getOtherProvider() != null)
			      {	  
			    	  facilityBean.setRegisteredByOther(patOrg.getOtherProvider());
			      }
			      else
			      {
			    	  facilityBean.setRegisteredByOther(patBean.getRegisteredByOther());
			      } 
			      
			      //Raman bug#11974
			      if(patOrg.isEmptyRegistrationDate()&&(patOrg.getRegistrationDate()!= null))
			      {
			    	  ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
					          new Issue(IssueTypes.DATA_VALIDATION, "Registration date field can not be supplied with emptyRegistrationDate field set to true"));
					        throw new OperationException();
			      }
			      else if(!patOrg.isEmptyRegistrationDate()&&(patOrg.getRegistrationDate()== null))
			      {
			    	  facilityBean.setRegDate(patBean.getRegDate());
			      }
			      else if(patOrg.getRegistrationDate()!= null && !patOrg.isEmptyRegistrationDate())
			      {
			    	  facilityBean.setRegDate(patOrg.getRegistrationDate());
			    	  }
			      else if(patOrg.isEmptyRegistrationDate()&& patOrg.getRegistrationDate()== null)
			      {
			    	  facilityBean.setRegDate(null);
			      }
			     
			     facilityBean.setPatientPK(patBean.getPatientPK());
			     	 
			      if ((patOrg.getSpecialtyAccess() != null) && (patOrg.getSpecialtyAccess().getSpeciality() != null))
			      {
			        List<Code> specialties = patOrg.getSpecialtyAccess().getSpeciality();
			        StringBuffer specialtyStr = new StringBuffer();

			        for (int x = 0; x < specialties.size(); x++)
			        {
			          if (x != 0) specialtyStr.append(",");
			          try
			          {
			            specialtyStr.append(AbstractService.dereferenceCodeStr((Code)specialties.get(x), CodeCache.CODE_TYPE_PRIMARY_SPECIALTY, callingUser));
			          }
			          catch (CodeNotFoundException cde)
			          {
			            ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
			              new Issue(
			              IssueTypes.DATA_VALIDATION, 
			              "Specialty Code Not Found: " + ((Code)specialties.get(x)).getCode()));
			            throw new OperationException();
			          }
			        }
			        facilityBean.setPatSpecialtylAccess(specialtyStr.toString());
			      }
			      else
			      {
			    	  facilityBean.setPatSpecialtylAccess(patBean.getPatSpecialtylAccess());
			      }	  
			  // checks for Patient Facility Id  
			      
			      if(patOrg.getFacilityID()!=null )
			      {	
			    	  //Raman Bug#12448      
			    	  if(StringUtil.isEmpty(patOrg.getFacilityID()))
			    	  {
			    		  ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
				                  new Issue(IssueTypes.DATA_VALIDATION, "Patient Facility Id can not be set to blank"));
				    			  throw new OperationException();
			    	  }      
			    	  if(!(patOrg.getFacilityID().equalsIgnoreCase(patBean.getPatientFacilityId())))
			    	  {	 
			    		  PatientFacilityDAO patientFacilityServiceDAO=new PatientFacilityDAO();
			    		  if(patientFacilityServiceDAO.isFacilityIdExists(patOrg.getFacilityID(),EJBUtil.stringToInteger(facilityBean.getPatientSite()).intValue()))
			    	      {
			    			  ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
			                  new Issue(IssueTypes.PATIENT_FACILITY_ID_ALREADY_EXISTS, "Patient Facility Id \""+patOrg.getFacilityID()+"\" already exists "));
			    			  throw new OperationException();
			    	      } 
			    		  facilityBean.setPatientFacilityId(patOrg.getFacilityID());
			    	  }
			    	  else
			    	  {
			    		  facilityBean.setPatientFacilityId(patOrg.getFacilityID());
			    	  }	
			      }
			      else
			      {
			    	  facilityBean.setPatientFacilityId(patBean.getPatientFacilityId());
			      }
			      int saved = patFacilityAgent.updatePatFacility(facilityBean);
			      
			      if (patOrg.isDefault())
		            {
		            	PersonAgentRObj personAgent = (PersonAgentRObj)parameters.get("personAgent");
		            	String personPK = (String) parameters.get("personPK"); 
		            	PersonBean perBean = personAgent.getPersonDetails(StringUtil.stringToInteger(personPK));
		            	perBean.setPersonRegDate(DateUtil.dateToString(facilityBean.getRegDate()));
		        		perBean.setPersonRegBy(facilityBean.getRegisteredBy());
		        		perBean.setPhyOther(facilityBean.getRegisteredByOther());
		        		perBean.setPersonSplAccess(facilityBean.getPatSpecialtylAccess());
		        		perBean.setPersonLocation(facilityBean.getPatientSite());
		        		perBean.setPatientFacilityId(facilityBean.getPatientFacilityId());
		        		perBean.setModifiedBy(StringUtil.integerToString(callingUser.getUserId()));
		        		
		        		PrefAgentRObj prefAgent = (PrefAgentRObj)parameters.get("prefAgent"); 
		        		PrefBean pref = prefAgent.getPrefDetails(StringUtil.stringToInteger(personPK));
		        		pref.setPrefPId(perBean.getPersonPId());
		        		pref.setPrefAccount(perBean.getPersonAccount());
		        		pref.setPrefLocation(perBean.getPersonLocation());
		        		pref.setModifiedBy(perBean.getModifiedBy());
		        		pref.setIpAdd(perBean.getIpAdd());
		        		pref.setPatientFacilityId(perBean.getPatientFacilityId());
		        		if((prefAgent.updatePref(pref))<0)
		        		{
		        			((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
							        new Issue(
							        IssueTypes.PATIENT_ERROR_UPDATE_ORGANIZATION));
							      throw new OperationException();
		        		}
		        		if((personAgent.updatePerson(perBean))<0)
		        		{
		        			((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
							        new Issue(
							        IssueTypes.PATIENT_ERROR_UPDATE_ORGANIZATION));
							      throw new OperationException();
		        		}

		             }
			      		
				return saved;
				
			}
}
