package com.velos.remoteservice.lab;

public interface ILabTestDetail {
	
	public String getAbnormalFlag(); 
	
	public String getCode();
	
	public String getComment();
	
	public int getConceptId();

	public String getConceptName();
		
	public String getMnemonic();
	
	public String getName();
	
	public Float getRangeLower();
	
	public Float getRangeUpper();
	
	public String getReferenceRange();
	
	public String getResultUnits();
	
	public boolean getIsDisplayable();
	
	public boolean getIsTrendable();
	
	public String getResultValue();
	
	public String getStatus();
	
	public String getValueType();
	
	public String getVerifyingPersonnel();
}
