package com.velos.remoteservice;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
//import java.util.ServiceLoader;

import com.velos.eres.service.util.Rlog;

import static com.velos.remoteservice.AbstractService.REMOTE_SERVICE_LOG_CAT;

/**
 * AbstractFactory class for defining remote service factory implementations.
 * @author dylan
 *
 */
public abstract class AbstractServiceFactory {
	
	
	protected static <S> Iterable<S> load(Class<S> ifc) throws ClassNotFoundException{
		try{
		Rlog.debug(
			  REMOTE_SERVICE_LOG_CAT, 
			  "Attempting to find service for class: " + ifc.getName());
		  ClassLoader ldr = Thread.currentThread().getContextClassLoader();
		  Enumeration<URL> e = ldr.getResources("META-INF/services/" + ifc.getName());
		   
		  Rlog.debug(REMOTE_SERVICE_LOG_CAT, "Found services file");
		  
		  Collection<S> services = new ArrayList<S>();
		  while (e.hasMoreElements()) {
		    URL url = e.nextElement();
		    InputStream is = url.openStream();
		    try {
		      BufferedReader r = new BufferedReader(new InputStreamReader(is, "UTF-8"));
		      while (true) {
		        String line = r.readLine();
		        if (line == null)
		          break;
		        int comment = line.indexOf('#');
		        if (comment >= 0)
		          line = line.substring(0, comment);
		        String name = line.trim();
		        if (name.length() == 0)
		          continue;
		        Class<?> clz = Class.forName(name, true, ldr);
		        Class<? extends S> impl = clz.asSubclass(ifc);
		        Constructor<? extends S> ctor = null;
				try {
					ctor = impl.getConstructor();
				} catch (SecurityException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NoSuchMethodException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		        S svc = null;
				try {
					svc = ctor.newInstance();
				} catch (IllegalArgumentException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (InstantiationException e1) {
					Rlog.error(
							  REMOTE_SERVICE_LOG_CAT, 
							  e1.getMessage());
				} catch (IllegalAccessException e1) {
					Rlog.error(
							  REMOTE_SERVICE_LOG_CAT, 
							  e1.getMessage());
				} catch (InvocationTargetException e1) {
					Rlog.error(
							  REMOTE_SERVICE_LOG_CAT, 
							  e1.getMessage());
				}
		        services.add(svc);
		      }
		    }
		    finally {
		      is.close();
		    }
		  }
		  return services;
		}
		catch(IOException e){
			return null;
		}
	}
	
	/**
	 * Loads a class using the thread current context class loader.
	 * The first parameter, ifc, is sent in to perform a check that the 
	 * returned class extends or implements the correct class or interface. The returned
	 * object will be a subclass of the ifc class.
	 * 
	 * An example call looks like:
	 * <code>DemographicsServiceFactory.loadRemoteService(
					IDemographicsService.class, 
					TestDemographicsService.class.getName());
		</code>
	 * Note that this expects the class to be instantiated to have a no-argument
	 * contstructor.
	 * @param <S>
	 * @param ifc identifying class
	 * @param className name of the class to load and return
	 * @return An instantiated object whose full name is className and which is a 
	 * subclass of ifc
	 * @throws VelosServiceException
	 */
	protected static <S> Object loadRemoteService(Class<S> ifc, String className)
	throws VelosServiceException{

		ClassLoader ldr = Thread.currentThread().getContextClassLoader();
        Class<?> clz = null;
		try {
			clz = Class.forName(className, true, ldr);
		} catch (ClassNotFoundException e) {
			throw new VelosServiceException(e);
		}
        Class<? extends S> impl = clz.asSubclass(ifc);
        Constructor<? extends S> ctor = null;
		try {
			ctor = impl.getConstructor();
		} catch (SecurityException e1) {
			throw new VelosServiceException(e1);
		} catch (NoSuchMethodException e1) {
			throw new VelosServiceException(e1);
		}
		Object svc = null;
		try {
			svc = ctor.newInstance();
		} catch (IllegalArgumentException e) {
			throw new VelosServiceException(e);
		} catch (InstantiationException e) {
			throw new VelosServiceException(e);
		} catch (IllegalAccessException e) {
			throw new VelosServiceException(e);
		} catch (InvocationTargetException e) {
			throw new VelosServiceException(e);
		}
		return svc;
		
	}
}
