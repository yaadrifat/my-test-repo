package com.velos.remoteservice.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

public class FormatUtils {

	public static Date dateFromString(String dateStr) throws ParseException{
		DateFormat df = DateFormat.getDateInstance();
		return df.parse(dateStr);
	}
	
}
