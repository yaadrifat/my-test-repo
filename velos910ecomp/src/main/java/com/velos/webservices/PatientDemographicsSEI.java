package com.velos.webservices;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import com.velos.services.OperationException;
import com.velos.services.OperationRolledBackException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.Patient;
import com.velos.services.model.PatientDemographics;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientOrganization;
import com.velos.services.model.PatientOrganizationIdentifier;
import com.velos.services.model.PatientSearch;
import com.velos.services.model.PatientSearchResponse;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudySummary;
import com.velos.services.model.UpdatePatientDemographics;
/**
 * Service Endpoint Interface dealing with generation of Operations
 *  of Patient demographics services
 * @author Virendra
 *
 */
@WebService(
		 targetNamespace="http://velos.com/services/")
	public interface PatientDemographicsSEI {
		/***
		 * public method takes PatientIdentifier as input
		 * and returns PatientDemographics object for the patient 
		 * @param patientId
		 * @return
		 * @throws OperationException
		 */
		@WebResult(name = "PatientDemographics" )
		public abstract PatientDemographics getPatientDemographics(
		@WebParam(name = "PatientIdentifier")		
		PatientIdentifier patientId)
				throws OperationException; 
		
		@WebResult(name = "Response")
		public abstract ResponseHolder createPatient(
				@WebParam(name = "Patient")
				Patient patient)
		throws OperationException, 
		OperationRolledBackException; 
		
		@WebResult(name="ResponseHolder")
		public abstract ResponseHolder updatePatient(
				  @WebParam(name="PatientIdentifier") 
				  PatientIdentifier paramPatientIdentifer, 
				  @WebParam(name="PatientDemographics") 
				  UpdatePatientDemographics paramPatientUpdateDemographics)
		throws OperationException, OperationRolledBackException;
		
		@WebResult(name="addPatientFacility")
		public ResponseHolder addPatientFacility(
				@WebParam(name="PatientIdentifier")
				PatientIdentifier patId,
				@WebParam(name="PatientOrganization")
				PatientOrganization patOrg) 
		throws OperationException, OperationRolledBackException;
		
		@WebResult(name="updatePatientFacility")
		public ResponseHolder updatePatientFacility(
				@WebParam(name="PatientIdentifier")
				PatientIdentifier patId,
				@WebParam(name="OrganizationIdentifier")
				OrganizationIdentifier orgID,
				@WebParam(name="PatientOrganizationIdentifier")
				PatientOrganizationIdentifier pat,
				@WebParam(name="PatientOrganization")
				PatientOrganization patOrgIdentifier)
		throws OperationException,OperationRolledBackException;
		
		@WebResult(name="PatientSearchResponse")
		public PatientSearchResponse searchPatient(
				@WebParam(name="PatientSearch") 
				PatientSearch paramPatientIdentifier)
		throws OperationException;
	
		
}