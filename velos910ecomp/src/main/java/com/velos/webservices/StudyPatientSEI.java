package com.velos.webservices;

import java.util.List;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import com.velos.services.OperationException;
import com.velos.services.OperationRolledBackException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.Patient;
import com.velos.services.model.PatientEnrollmentDetails;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyPatient;
/**
 * Service Endpoint Interface dealing with generation of Operations
 *  of StudyPatient(Patients in a study) services 
 * @author Virendra
 *
 */
@WebService(
		 targetNamespace="http://velos.com/services/")

	public interface StudyPatientSEI {
		/**
		 * public method of Study Patient Service Endpoint Interface
		 * which calls getStudyPatient method of StudyPatient webservice
		 * and returns List of StudyPatient
		 * @param StudyId
		 * @return List<StudyPatient>
		 * @throws OperationException
		 */
		@WebResult(name = "StudyPatient" )
		public List<StudyPatient> getStudyPatients(
		@WebParam(name = "StudyIdentifier")		
		StudyIdentifier StudyId)
				throws OperationException;
		
		
		@WebResult(name = "Response")
		public abstract ResponseHolder enrollPatientToStudy(
				@WebParam(name="PatientIdentifier")
				PatientIdentifier patientIdentifier,
				@WebParam(name = "StudyIdentifier")
				 StudyIdentifier studyIdentifier, 
				@WebParam(name = "PatientEnrollmentDetails") 
				PatientEnrollmentDetails patientEnrollmentDetails)
				throws OperationException, OperationRolledBackException;
		
		@WebResult(name="Response")
		public abstract ResponseHolder createAndEnrollPatient(
				@WebParam(name ="Patient")
				Patient patient,
				@WebParam(name = "StudyIdentifier")
				 StudyIdentifier studyIdentifier, 
				@WebParam(name = "PatientEnrollmentDetails")
				PatientEnrollmentDetails patientEnrollmentDetails)
				throws OperationException, OperationRolledBackException; 
}