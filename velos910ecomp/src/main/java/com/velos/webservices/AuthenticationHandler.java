/**
 * 
 */
package com.velos.webservices;

import javax.ws.rs.core.Response;

import org.apache.cxf.configuration.security.AuthorizationPolicy;
import org.apache.cxf.jaxrs.ext.RequestHandler;
import org.apache.cxf.jaxrs.model.ClassResourceInfo;
import org.apache.cxf.message.Message;

/**
 * @author dylan
 *
 */
public class AuthenticationHandler implements RequestHandler {

    public Response handleRequest(Message m, ClassResourceInfo resourceClass) {
        AuthorizationPolicy policy = (AuthorizationPolicy)m.get(AuthorizationPolicy.class);
        policy.getUserName();
        policy.getPassword(); 
        // alternatively :
        // HttpHeaders headers = new HttpHeadersImpl(m);
        // access the headers as needed  
        return null; 
    }

}

