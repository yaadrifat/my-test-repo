package com.velos.webservices;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import com.velos.services.OperationException;
import com.velos.services.model.BudgetIdentifier;
import com.velos.services.model.BudgetStatus;

@WebService(
		 targetNamespace="http://velos.com/services/")
public interface BudgetSEI {
	
	@WebResult(name = "BudgetStatus")
	public abstract BudgetStatus getBudgetStatus(
			@WebParam(name = "BudgetIdentifier")
			BudgetIdentifier budgetIdentifier)
			
		throws OperationException;

}
