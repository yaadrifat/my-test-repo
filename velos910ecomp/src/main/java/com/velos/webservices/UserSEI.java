/**
 * Created On Aug 9, 2012
 */
package com.velos.webservices;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.Groups;

import com.velos.services.model.NonSystemUser;
import com.velos.services.model.Organizations;
import com.velos.services.model.User;
import com.velos.services.model.User.UserStatus;
import com.velos.services.model.UserIdentifier;

/**
 * @author Kanwaldeep
 *
 */
@WebService(targetNamespace="http://velos.com/services/")
public interface UserSEI {
//
//	@WebResult(name="Response")
//	public ResponseHolder createNonSystemUser(
//			@WebParam(name="NonSystemUser")
//			NonSystemUser nonSystemUser); 

//	@WebResult(name="Response")
//	public ResponseHolder createUser(
//			@WebParam(name="User")
//			User user); 

	@WebMethod(operationName = "changeUserStatus")
	@WebResult(name="Response")
	public ResponseHolder changeUserStatus(
			@WebParam(name = "UserIdentifier")
			UserIdentifier uId,
			@WebParam(name = "UserStatus")
			UserStatus uStat) 
	throws OperationException;


	@WebMethod(operationName = "getAllGroups")
	@WebResult(name="UserGroups")
	public Groups getAllGroups() throws OperationException;

	@WebResult(name = "UserGroups")
	public Groups getUserGroups(
			@WebParam(name="UserIdentifier")UserIdentifier userIdent) 
	throws OperationException;
	
	@WebMethod(operationName = "createNonSystemUser")
	@WebResult(name="Response")
	public ResponseHolder createNonSystemUser(@WebParam(name = "NonSystemUser")NonSystemUser nonsystemuser)
 	throws OperationException;
	
	@WebMethod(operationName = "getAllOrganizations")
	@WebResult(name="UserOrganizations")
	public Organizations getAllOrganizations() throws OperationException;
	
	@WebMethod(operationName = "killUserSession")
	@WebResult(name="Response")
	public ResponseHolder killUserSession(
			@WebParam(name = "UserIdentifier")
			UserIdentifier uId) 
	throws OperationException;
	
	@WebMethod(operationName = "createUser")
	@WebResult(name="Response")
	public ResponseHolder createUser(@WebParam(name = "User")User user)
 	throws OperationException;
	
}
