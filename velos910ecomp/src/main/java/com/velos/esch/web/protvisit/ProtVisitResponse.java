package com.velos.esch.web.protvisit;

import java.util.HashMap; 

public class ProtVisitResponse {
	
	/**
	 * visit id 
	 */
	int visitId;
	
	/**
	 * visitName. 
	 */
	String visitName;
	
	/**
	 * 
	 * @return
	 */
	int rowNumber;
	HashMap<Integer, Integer> copyVisitMap= new HashMap<Integer, Integer>(); //Ak:Added for enhancement PCAL-20801
	
	
	public HashMap<Integer, Integer> getCopyVisitMap() {
		return copyVisitMap;
	}
	public void setCopyVisitMap(HashMap<Integer, Integer> copyVisitMap) {
		this.copyVisitMap = copyVisitMap;
	}
	public int getVisitId() {
		return visitId;
	}
	public void setVisitId(int visitId) {
		this.visitId = visitId;
	}
	public String getVisitName() {
		return visitName;
	}
	public void setVisitName(String visitName) {
		this.visitName = visitName;
	}
	public int getRowNumber() {
		return rowNumber;
	}
	public void setRowNumber(int rowNumber) {
		this.rowNumber = rowNumber;
	}
	
	
	

}
