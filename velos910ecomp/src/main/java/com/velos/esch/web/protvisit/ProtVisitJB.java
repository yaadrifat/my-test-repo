/*
 * Classname : ProtVisitJB
 *
 * Version information: 1.0
 *
 * Date: 09/22/04
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Sam Varadarajan
 */

package com.velos.esch.web.protvisit;

/* IMPORT STATEMENTS */


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;


import com.velos.eres.service.util.DateUtil;
import com.velos.esch.business.common.ProtVisitDao;
import com.velos.esch.business.common.ScheduleDao;
import com.velos.esch.business.protvisit.impl.ProtVisitBean;
import com.velos.esch.service.protvisitAgent.ProtVisitAgentRObj;
import com.velos.esch.service.util.EJBUtil;
import com.velos.esch.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * Client side Java Bean for ProtVisit
 *
 * @author Dinesh
 * @version 1.0 05/14/2001
 *
 */

public class ProtVisitJB {

    /**
     * visit id
     */
    private int visit_id;

    /**
     * protocol id
     */
    private int protocol_id;

    /**
     * visit name
     */
    private String name;

    /**
     * visit description
     */
    private String description;

    /**
     * user id
     */

    private int visit_no;

    /**
     * displacement
     */
    private String displacement;

    /*
     * interval months
     */
    private int months;

    /*
     * interval weeks
     */
    private int weeks;

    /*
     * interval days
     */
     //D-FIN-25-DAY0 BK JAN-23-2011
    private Integer days;

    /*
     * insert after
     */
    private Integer insertAfter;

    /*
     * insert after interval
     */
    private Integer insertAfterInterval;

    /*
     * insert after interval Unit
     */
    private String insertAfterIntervalUnit;

    /*
     * creator
     */
    private String creator;  // Amar

    /*
     * last modified by
     */
    private String modifiedBy;  // Amar

    /*
     * IP Address
     */
    private String ip_add;

    private String visitType;
    
    private String intervalFlag;
    
    private String durationBefore;
    
    private String durationAfter;
    
    private String durationUnitBefore;
    
    private String durationUnitAfter;
    
    //AK:Added for enh PCAL-20353 and PCAL-20354
    private Integer offlineFlag;
	private Integer hideFlag;
    
    
    private ArrayList<ArrayList<String>> listOfVisitWindowsByMonth;
    
    // GETTER SETTER METHODS
    public int getVisit_id() {
        return visit_id;
    }

    public int getProtocol_id() {
        return protocol_id;
    }

    public String getName() {
        return this.name;
    }

    public String getDescription() {
        return this.description;
    }

    public int getVisit_no() {
        return this.visit_no;
    }

    public String getDisplacement() {
        return this.displacement;
    }

    public int getMonths() {
        return this.months;
    }

    public int getWeeks() {
        return this.weeks;
    }

    public Integer getDays() {
        return this.days;
    }

    public Integer getInsertAfter() {
        return this.insertAfter;
    }

    public Integer getInsertAfterInterval() {
        return this.insertAfterInterval;
    }

    public String getInsertAfterIntervalUnit() {
        return this.insertAfterIntervalUnit;
    }

//  Modified By Amarnadh for Bugzilla issue #3131

    public String getCreator() {
        return creator;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public String getip_add() {
        return this.ip_add;
    }

    public String getVisitType() {
        return this.visitType;
    }
    
    public String getIntervalFlag() {
        return this.intervalFlag;
    }

    // SETTER METHODS

    public void setVisit_id(int visit_id) {
        this.visit_id = visit_id;
    }

    public void setProtocol_id(int protocol_id) {
        this.protocol_id = protocol_id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setVisit_no(int visit_no) {
        this.visit_no = visit_no;
    }

    public void setDisplacement(String displacement) {
        this.displacement = displacement;
    }

    public void setMonths(int months) {
        this.months = months;
    }

    public void setWeeks(int weeks) {
        this.weeks = weeks;
    }
//D-FIN-25-DAY0 BK JAN-23-2011
    public void setDays(Integer days) {
        this.days = days;
    }

    public void setInsertAfter(Integer insertAfter) {
        this.insertAfter = insertAfter;
    }

    public void setInsertAfterInterval(Integer insertAfterInterval) {
        this.insertAfterInterval = insertAfterInterval;
    }

    public void setInsertAfterIntervalUnit(String insertAfterIntervalUnit) {
        this.insertAfterIntervalUnit = insertAfterIntervalUnit;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public void setip_add(String ip_add) {
        this.ip_add = ip_add;
    }

    public void setVisitType(String visitType) {
        this.visitType = visitType;
    }
    
    public void setIntervalFlag(String intervalFlag) {
        this.intervalFlag = intervalFlag;
    }
    
    //KM-DFIN4
    public String getDurationBefore() {
        return durationBefore;
    }

    public void setDurationBefore(String durationBefore) {
        this.durationBefore = durationBefore;
    }
    
    public String getDurationUnitBefore() {
        return durationUnitBefore;
    }

    public void setDurationUnitBefore(String durationUnitBefore) {
        this.durationUnitBefore = durationUnitBefore;
    }
    
    public String getDurationAfter() {
        return durationAfter;
    }

    public void setDurationAfter(String durationAfter) {
        this.durationAfter = durationAfter;
    }
    
    
    public String getDurationUnitAfter() {
        return durationUnitAfter;
    }

    public void setDurationUnitAfter(String durationUnitAfter) {
        this.durationUnitAfter = durationUnitAfter;
    }
    
    //Ak:Added for enh PCAL-20353 and PCAL-20354
    
    public Integer getOfflineFlag() {
		return offlineFlag;
	}

	public void setOfflineFlag(Integer offlineFlag) {
		this.offlineFlag = offlineFlag;
	}

	public Integer getHideFlag() {
		return hideFlag;
	}

	public void setHideFlag(Integer hideFlag) {
		this.hideFlag = hideFlag;
	}
    

    // END OF GETTER SETTER METHODS

    /**
     * Constructor; Accepts visit id
     *
     * @param visit_id
     *            Id of the prot visit
     */
    public ProtVisitJB(int visit_id) {
        setVisit_id(visit_id);
    }

    /**
     * Default Constructor
     *
     */
    public ProtVisitJB() {
        Rlog.debug("protvisits", "ProtVisitJB.ProtVisitJB() ");
    }

    public ProtVisitJB(int visit_id, int protocol_id, String name,
            String description, int visit_no, String displacement, int months,
            int weeks, Integer days, int insertAfter, int insertAfterInterval,
            String insertAfterIntervalUnit, String creator, String modifiedBy,
            String ip_add, String visitType, String intervalFlag, String durationBefore, String durationUnitBefore,
            String durationAfter, String durationUnitAfter) {
        Rlog.debug("protvisit", "ProtVisitJB.ProtVisitJB(all parameters)");
        this.setVisit_id(visit_id);
        this.setName(name);
        this.setDescription(description);
        this.setVisit_no(visit_no);
        this.setDisplacement(displacement);
        this.setMonths(months);
        this.setWeeks(weeks);
        this.setDays(days);
        this.setInsertAfter(insertAfter);
        this.setInsertAfterInterval(insertAfterInterval);
        this.setInsertAfterIntervalUnit(insertAfterIntervalUnit);
        this.setModifiedBy(modifiedBy);
        this.setip_add(ip_add);
        this.setVisitType(visitType);
        this.setIntervalFlag(intervalFlag);
        this.setDurationBefore(durationBefore);
        this.setDurationUnitBefore(durationUnitBefore);
        this.setDurationAfter(durationAfter);
        this.setDurationUnitAfter(durationUnitAfter);
    }

    public ProtVisitDao getProtocolVisits(int protocol_id) {
        ProtVisitDao protVisitDao = new ProtVisitDao();
        try {

            ProtVisitAgentRObj ProtVisitAgentRObj = EJBUtil
                    .getProtVisitAgentHome();
            Rlog.debug("protvisit", "ProtVisitJB.getProtVisits after remote");
            protVisitDao = ProtVisitAgentRObj.getProtocolVisits(protocol_id);
            Rlog.debug("protvisit", "ProtVisitJB.getProtVisits after Dao");
            if (protVisitDao == null)
                protVisitDao = new ProtVisitDao();
            return protVisitDao;
        } catch (Exception e) {
            if (protVisitDao == null) {
                protVisitDao = new ProtVisitDao();
                Rlog.fatal("protvisit",
                        "Error in getProtVisits in ProtVisitJB " + e);
            }
        }
        return protVisitDao;
    }


    /**JM: 16Apr2008
    *
    * @param protocol_id, search
    */
    public ProtVisitDao getProtocolVisits(int protocol_id, String search) {
        ProtVisitDao protVisitDao = new ProtVisitDao();
        try {

            ProtVisitAgentRObj ProtVisitAgentRObj = EJBUtil.getProtVisitAgentHome();
            Rlog.debug("protvisit", "ProtVisitJB.getProtVisits after remote");
            protVisitDao = ProtVisitAgentRObj.getProtocolVisits(protocol_id,search);
            Rlog.debug("protvisit", "ProtVisitJB.getProtVisits after Dao");
            if (protVisitDao == null)
                protVisitDao = new ProtVisitDao();
            return protVisitDao;
        } catch (Exception e) {
            if (protVisitDao == null) {
                protVisitDao = new ProtVisitDao();
                Rlog.fatal("protvisit",
                        "Error in getProtVisits in ProtVisitJB " + e);
            }
        }
        return protVisitDao;
    }

    /**
     * Calls getProtVisitDetails() of ProtVisit Session Bean: ProtVisitAgentBean
     *
     *
     * @return The Details of the ProtVisit in the ProtVisit StateKeeper Object
     * @See ProtVisitAgentBean
     */

    public ProtVisitBean getProtVisitDetails() {
        ProtVisitBean vdsk = null;

        try {

            ProtVisitAgentRObj protVisitAgent = EJBUtil.getProtVisitAgentHome();
            vdsk = protVisitAgent.getProtVisitDetails(this.visit_id);
            Rlog.debug("protvisit",
                    "ProtVisitJB.getProtVisitDetails() ProtVisitStateKeeper "
                            + vdsk);
        } catch (Exception e) {
            Rlog.debug("protvisit",
                    "Error in getProtVisitDetails() in ProtVisitJB " + e);
        }
        if (vdsk != null) {
            this.visit_id = vdsk.getVisit_id();
            this.name = vdsk.getName();
            this.description = vdsk.getDescription();
            this.visit_no = vdsk.getVisit_no();
            this.displacement = vdsk.getDisplacement();
            this.months = vdsk.getMonths();
            this.weeks = vdsk.getWeeks();
            this.days = vdsk.getDays();
            this.insertAfter = vdsk.getInsertAfter();
            this.insertAfterInterval = vdsk.getInsertAfterInterval();
            this.insertAfterIntervalUnit = vdsk.getInsertAfterIntervalUnit();
            this.modifiedBy = vdsk.getModifiedBy();
            this.ip_add = vdsk.getip_add();
            this.visitType = vdsk.getVisitType();
            this.intervalFlag = vdsk.getIntervalFlag();
            this.durationBefore = vdsk.getDurationBefore();
            this.durationUnitBefore = vdsk.getDurationUnitBefore();
            this.durationAfter = vdsk.getDurationAfter();
            this.durationUnitAfter = vdsk.getDurationUnitAfter();
            this.offlineFlag=vdsk.getOfflineFlag();
            this.hideFlag=vdsk.getHideFlag();
            
            
        }
        return vdsk;
    }

    /**
     * Calls setProtVisitDetails() of ProtVisit Session Bean: protVisitAgentBean
     *
     */

    public void setProtVisitDetails() {
        try {

            ProtVisitAgentRObj protVisitAgent = EJBUtil.getProtVisitAgentHome();
            Rlog.debug("protvisit", "after calling create");
            this.setVisit_id(protVisitAgent.setProtVisitDetails(this
                    .createProtVisitStateKeeper()));
            Rlog.debug("protvisit", "after calling setvisit_id");
            Rlog.debug("protvisit", "ProtVisitJB.visit_id=" + this.getVisit_id());
            
        } catch (Exception e) {
            Rlog.debug("protvisit",
                    "Error in setProtVisitDetails() in ProtVisitJB " + e);
        }
    }

    /**
     * Calls updateProtVisit() of ProtVisit Session Bean: protVisitAgentBean
     *
     * @return
     */
    public int updateProtVisit(String calType) {
        int output;
        try {
            ProtVisitDao protVisitDao = new ProtVisitDao();

            ProtVisitAgentRObj ProtVisitAgentRObj = EJBUtil
                    .getProtVisitAgentHome();
            ProtVisitBean psk = this.createProtVisitStateKeeper();
            output = ProtVisitAgentRObj.updateProtVisit(psk);
            if (output == 0) {
                protVisitDao.updateVisitEvents(psk.getProtocol_id(), psk
                        .getVisit_id(), calType);
            }

        } catch (Exception e) {
            Rlog.debug("protvisit",
                    "EXCEPTION IN SETTING USER DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    /**
     * Calls Delete ProtVisit() of ProtVisit Session Bean: ProtVisitJB
     *
     * @return
     */

    public int removeProtVisit(int protocol_id, int visit_id, String calType) {
        int output;
        try {
            ProtVisitDao protVisitDao = new ProtVisitDao();

            ProtVisitAgentRObj ProtVisitAgentRObj = EJBUtil
                    .getProtVisitAgentHome();
            output = ProtVisitAgentRObj.removeProtVisit(visit_id);
            if (output == 0) {
                protVisitDao.DeleteVisitEvents(protocol_id, visit_id, calType);
            }
        } catch (Exception e) {
            Rlog.debug("protvisit", "EXCEPTION IN DELETING THE CHILD NODE");
            e.printStackTrace();
            return -2;
        }
        return output;
    }
    
    
    // removeProtVisit() Overloaded for INF-18183 ::: Raviesh
    public int removeProtVisit(int protocol_id, int visit_id, String calType,Hashtable<String, String> args) {
        int output;
        try {
            ProtVisitDao protVisitDao = new ProtVisitDao();

            ProtVisitAgentRObj ProtVisitAgentRObj = EJBUtil
                    .getProtVisitAgentHome();
            output = ProtVisitAgentRObj.removeProtVisit(visit_id,args);
            if (output == 0) {
                protVisitDao.DeleteVisitEvents(protocol_id, visit_id, calType);
            }
        } catch (Exception e) {
            Rlog.debug("protvisit", "EXCEPTION IN DELETING THE CHILD NODE");
            e.printStackTrace();
            return -2;
        }
        return output;
    }
    //BK,May/20/2011,Fixed #6014
    
    /**
     * Calls validateDayZeroVisit of ProtVisit Session Bean: protVisitAgentBean
     *
     * @return
     */
    public int validateDayZeroVisit(int duration,int protocolId,int check,int visitId) {
        int output;
        try {
            ProtVisitDao protVisitDao = new ProtVisitDao();
            ProtVisitAgentRObj ProtVisitAgentRObj = EJBUtil
                    .getProtVisitAgentHome();                        
            output = ProtVisitAgentRObj.validateDayZeroVisit(duration, protocolId,check,visitId);           
        } catch (Exception e) {
            Rlog.debug("protvisit",
                    "EXCEPTION IN SETTING USER DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -6;
        }
        return output;
    }
    /** 
     *
     *
     * @return the ProtVisit StateKeeper Object with the current values of the
     *         Bean
     */

    public ProtVisitBean createProtVisitStateKeeper() {
        // SV, 10/29/04, as far as online is concerned all visits updated here
        // are user visits. This will reset the system generated status, if it's
        // edited on line
        Rlog.debug("protvisit", "ProtVisitJB.createProtVisitStateKeeper ");

        return new ProtVisitBean(visit_id, protocol_id, name, description,
                visit_no, displacement, months, weeks, days, insertAfter,
                insertAfterInterval, insertAfterIntervalUnit, creator,
                modifiedBy, ip_add, visitType, intervalFlag, durationBefore, durationUnitBefore,
                durationAfter, durationUnitAfter);

    }

    /*
     * generates a String of XML for the ProtVisit details entry form.<br><br>
     * NOT IN USE
     */

    public String toXML() {
        Rlog.debug("protvisit", "ProtVisitJB.toXML()");
        return "test";
        // to be implemented later
        /*
         * return new String("<?xml version=\"1.0\"?>" + "<?cocoon-process
         * type=\"xslt\"?>" + "<?xml-stylesheet
         * href=\"/stylesheet/form-html.xsl\" type=\"text/xsl\"?>" + "<?xml-stylesheet
         * href=\"/stylesheet/form-wml.xsl\" type=\"text/xsl\" media=\"wap\"?>" + "<head>" + "</head>" + "</form>" );
         */

    }// end of method

    //This method is not used anymore. Bug #6631
    public int pushNoIntervalVisitsOut(int protocol_visit_id, String ip, int userId) {

        int output = -1;
        try {
            ProtVisitAgentRObj ProtVisitAgentRObj = EJBUtil
                    .getProtVisitAgentHome();
            output = ProtVisitAgentRObj.pushNoIntervalVisitsOut(protocol_visit_id, ip, userId);

        } catch (Exception e) {
            Rlog.debug("protvisit", "EXCEPTION IN DELETING THE CHILD NODE");
            e.printStackTrace();

        }
        return output;

    }
    public int generateRipple(int protocol_visit_id,String calledFrom) {

        int output = -1;
        try {
            ProtVisitAgentRObj ProtVisitAgentRObj = EJBUtil
                    .getProtVisitAgentHome();
            output = ProtVisitAgentRObj.generateRipple(protocol_visit_id,calledFrom);

        } catch (Exception e) {
            Rlog.debug("protvisit", "EXCEPTION IN DELETING THE CHILD NODE");
            e.printStackTrace();

        }
        return output;

    }
    public int getMaxVisitNo(int protocol_id){

        int output = -1;
        try {
            ProtVisitAgentRObj ProtVisitAgentRObj = EJBUtil
                    .getProtVisitAgentHome();
            output = ProtVisitAgentRObj.getMaxVisitNo(protocol_id);

        } catch (Exception e) {
            Rlog.debug("protvisit", "EXCEPTION IN getting max Visit No");
            e.printStackTrace();

        }
        return output;

    }

    // Added by Manimaran for the Enhancement #C8.3
    public int copyVisit(int protocolId,int frmVisitId, int visitId,String calledFrom, String usr, String ipAdd) {
    	int ret = 0;
    	try {
    		Rlog.debug("protvisit", "ProtVisitJB:copyVisit starting");

    		ProtVisitAgentRObj ProtVisitAgentRObj = EJBUtil
            .getProtVisitAgentHome();
    		ret = ProtVisitAgentRObj.copyVisit(protocolId,frmVisitId,visitId,calledFrom,usr,ipAdd);//KM-3447

    	} catch (Exception e) {
    		Rlog.fatal("protvisit", "ProtVisitJB:copyVisit-Error " + e);
    		return -1;
    	}
    	return ret; 

    }
    
    public ScheduleDao getVisitsByMonth(int patProtId, HashMap paramMap) {
        ProtVisitAgentRObj protVisitAgentRObj = EJBUtil.getProtVisitAgentHome();
        if (protVisitAgentRObj == null) {
            return new ScheduleDao();
        }
        ScheduleDao schDao = protVisitAgentRObj.getVisitsByMonth(patProtId, paramMap);
        calculateVisitWindows(schDao);
        return schDao;
    }
    
    public ArrayList getListOfVisitWindowsByMonth() {
        return listOfVisitWindowsByMonth;
    }
    
    private void calculateVisitWindows(ScheduleDao schDao) {
        ArrayList<ArrayList<Integer>> listOfWinBeforeNumbersByMonth = schDao.getListOfWinBeforeNumbersByMonth();
        ArrayList<ArrayList<String>> listOfWinBeforeUnitsByMonth = schDao.getListOfWinBeforeUnitsByMonth();
        ArrayList<ArrayList<Integer>> listOfWinAfterNumbersByMonth = schDao.getListOfWinAfterNumbersByMonth();
        ArrayList<ArrayList<String>> listOfWinAfterUnitsByMonth = schDao.getListOfWinAfterUnitsByMonth();
        listOfVisitWindowsByMonth = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> listOfActualDatesByMonth = schDao.getListOfActualDatesByMonth();
        for (int iMonth=0; iMonth<listOfActualDatesByMonth.size(); iMonth++) {
            ArrayList<String> actualDates = listOfActualDatesByMonth.get(iMonth);
            ArrayList<String> visitWindowOfOneMonth = new ArrayList<String>();
            ArrayList<Integer> winBeforeNumbersOfOneMonth = listOfWinBeforeNumbersByMonth.get(iMonth);
            ArrayList<String> winBeforeUnitsOfOneMonth = listOfWinBeforeUnitsByMonth.get(iMonth);
            ArrayList<Integer> winAfterNumbersOfOneMonth = listOfWinAfterNumbersByMonth.get(iMonth);
            ArrayList<String> winAfterUnitsOfOneMonth = listOfWinAfterUnitsByMonth.get(iMonth);
            for (int iVisit=0; iVisit<actualDates.size(); iVisit++) {
                StringBuffer windowSB = new StringBuffer();
                if ("".equals(actualDates.get(iVisit))) {
                    visitWindowOfOneMonth.add(windowSB.toString());
                    continue;
                }
                if (winBeforeNumbersOfOneMonth.get(iVisit) == 0 
                        && winAfterNumbersOfOneMonth.get(iVisit) == 0) {
                    visitWindowOfOneMonth.add(windowSB.toString());
                    continue;
                }
                Date actualDate = DateUtil.stringToDate(actualDates.get(iVisit));
                Date beginDate = calculateDate(actualDate, -1*winBeforeNumbersOfOneMonth.get(iVisit),
                        winBeforeUnitsOfOneMonth.get(iVisit));
                windowSB.append(DateUtil.dateToString(beginDate)).append("~");
                Date endDate = calculateDate(actualDate, winAfterNumbersOfOneMonth.get(iVisit),
                        winAfterUnitsOfOneMonth.get(iVisit));
                windowSB.append(DateUtil.dateToString(endDate));
                visitWindowOfOneMonth.add(windowSB.toString());
            }
            listOfVisitWindowsByMonth.add(visitWindowOfOneMonth);
        }
    }
    
    private Date calculateDate(Date startDate, Integer offsetNumber, String offsetUnit) {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(startDate);
        cal1.add(convertUnitToField(offsetUnit), offsetNumber);
        return cal1.getTime();
    }
    
    private int convertUnitToField(String offsetUnit) {
        if (offsetUnit == null) { return 0; }
        if ("D".equals(offsetUnit.toUpperCase())) { return Calendar.DAY_OF_YEAR; }
        if ("W".equals(offsetUnit.toUpperCase())) { return Calendar.WEEK_OF_YEAR; }
        if ("M".equals(offsetUnit.toUpperCase())) { return Calendar.MONTH; }
        if ("Y".equals(offsetUnit.toUpperCase())) { return Calendar.YEAR; }
        return 0;
    }
    /**
     * 
     * @param dataGridString
     * @param protocolId
     * @param duration
     * @param calType
     * @return
     */   
    
    //Modified for INF-18183 : Raviesh and BUG#7224
    public ProtVisitResponse manageVisits(String dataGridString,String updateString,int duration,String calType,String calStatus,String deleteString,Hashtable<String, String> args) {
    	int output;
    	ProtVisitResponse visitResponse = null;
        try {
            ProtVisitDao protVisitDao = new ProtVisitDao();

            ProtVisitAgentRObj ProtVisitAgentRObj = EJBUtil
                    .getProtVisitAgentHome();
            ProtVisitBean psk = this.createProtVisitStateKeeper();

            visitResponse = ProtVisitAgentRObj.manageVisit(dataGridString,updateString,psk,duration,calStatus,calType,deleteString,args); //Raviesh:Fixed BUG#7224

            if (visitResponse.getVisitId() >= 0) {
                protVisitDao.updateVisitEvents(psk.getProtocol_id(), psk
                        .getVisit_id(), calType);
            }

        } catch (Exception e) {
            Rlog.debug("protvisit",
                    "EXCEPTION IN SETTING USER DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            visitResponse.setVisitId(-2);						 
            return  visitResponse;
        }
        return visitResponse;
      
    }

}// end of class
