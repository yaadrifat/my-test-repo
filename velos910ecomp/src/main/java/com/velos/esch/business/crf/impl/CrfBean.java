/*
 * Classname : CrfBean
 * 
 * Version information: 1.0
 *
 * Date: 11/23/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Arvind
 */

package com.velos.esch.business.crf.impl;

/* Import Statements */
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.esch.service.util.Rlog;
import com.velos.esch.service.util.StringUtil;

// import java.sql.*;

/* End of Import Statements */
@Entity
@Table(name = "sch_crf")
public class CrfBean implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 3833752070619410995L;

    /**
     * crf id
     */
    public int crfId;

    /**
     * events1 id
     */
    public String events1Id;

    /**
     * crf number
     */
    public String crfNumber;

    /**
     * crf name
     */

    public String crfName;

    /*
     * the record creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * the IP Address
     */
    public String ipAdd;

    /**
     * PatProt id
     */
    public Integer patProtId;

    /**
     * CRF Flag (whether CRF or Other form)
     */
    public String crfFlag;

    /**
     * CRF Delete Flag (Whether the record has been deleted logically or not)
     */
    public String crfDelFlag;

    // GETTER SETTER METHODS
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_SCH_CRF", allocationSize=1)
    @Column(name = "PK_CRF")
    public int getCrfId() {
        return this.crfId;
    }

    public void setCrfId(int crfId) {
        this.crfId = crfId;
    }

    @Column(name = "FK_EVENTS1")
    public String getEvents1Id() {
        return this.events1Id;
    }

    public void setEvents1Id(String events1Id) {
        int len = events1Id.length();
        len = 10 - len;
        String zeroStr = "";
        for (int i = 0; i < len; i++) {
            zeroStr = zeroStr + "0";
        }
        events1Id = zeroStr + events1Id;
        this.events1Id = events1Id;
    }

    @Column(name = "CRF_NUMBER")
    public String getCrfNumber() {
        return this.crfNumber;
    }

    public void setCrfNumber(String crfNumber) {
        this.crfNumber = crfNumber;
    }

    @Column(name = "CRF_NAME")
    public String getCrfName() {
        return this.crfName;
    }

    public void setCrfName(String crfName) {
        this.crfName = crfName;
    }

    /**
     * @return The Id of the creator of the record
     */
    @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    /**
     * @return Modified By
     */
    @Column(name = "LAST_MODIFIED_BY")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modified
     *            by
     */
    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    /**
     * @return IP Address
     */
    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    @Column(name = "FK_PATPROT")
    public String getPatProtId() {
        return StringUtil.integerToString(this.patProtId);
    }

    public void setPatProtId(String patProtId) {
        if (patProtId != null) {
            this.patProtId = Integer.valueOf(patProtId);
        }
    }

    @Column(name = "CRF_FLAG")
    public String getCrfFlag() {
        return this.crfFlag;
    }

    public void setCrfFlag(String crfFlag) {
        this.crfFlag = crfFlag;
    }

    @Column(name = "CRF_DELFLAG")
    public String getCrfDelFlag() {
        return this.crfDelFlag;
    }

    public void setCrfDelFlag(String crfDelFlag) {
        this.crfDelFlag = crfDelFlag;
    }

    // / END OF GETTER SETTER METHODS

    /**
     * returns the state holder object associated with this crf
     */

    /*
     * public CrfStateKeeper getCrfStateKeeper() { Rlog.debug("crf",
     * "CrfBean.getCrfStateKeeper"); return new CrfStateKeeper(getCrfId(),
     * getEvents1Id(), getCrfNumber(), getCrfName(), getCreator(),
     * getModifiedBy(), getIpAdd(), getPatProtId(), getCrfFlag(),
     * getCrfDelFlag()); }
     */

    /**
     * sets up a state keeper containing details of the crf
     * 
     * @param edsk
     */

    /*
     * public void setCrfStateKeeper(CrfStateKeeper usk) { GenerateId cfId =
     * null;
     * 
     * try { Connection conn = null; conn = getConnection(); Rlog .debug("crf",
     * "CrfBean.setCrfStateKeeper() Connection :" + conn); crfId =
     * cfId.getId("SEQ_SCH_CRF", conn); conn.close();
     * setEvents1Id(usk.getEvents1Id()); setCrfNumber(usk.getCrfNumber());
     * setCrfName(usk.getCrfName()); setCreator(usk.getCreator());
     * setModifiedBy(usk.getModifiedBy()); setIpAdd(usk.getIpAdd());
     * setPatProtId(usk.getPatProtId()); setCrfFlag(usk.getCrfFlag());
     * setCrfDelFlag(usk.getCrfDelFlag());
     * 
     * Rlog .debug("user", "UserBean.setUserStateKeeper() userId :" + crfId); }
     * catch (Exception e) { Rlog .fatal("user", "Error in setUserStateKeeper()
     * in UserBean " + e); } }
     */

    /**
     * updates the crf details
     * 
     * @param edsk
     * @return 0 if successful; -2 for exception
     */
    public int updateCrf(CrfBean edsk) {
        try {

            setEvents1Id(edsk.getEvents1Id());
            setCrfNumber(edsk.getCrfNumber());
            setCrfName(edsk.getCrfName());
            setCreator(edsk.getCreator());
            setModifiedBy(edsk.getModifiedBy());
            setIpAdd(edsk.getIpAdd());
            setPatProtId(edsk.getPatProtId());
            setCrfFlag(edsk.getCrfFlag());
            setCrfDelFlag(edsk.getCrfDelFlag());

            return 0;
        } catch (Exception e) {
            Rlog.fatal("crf", " error in CrfBean.updateCrf : " + e);
            return -2;
        }
    }

    public CrfBean(int crfId, String events1Id, String crfNumber,
            String crfName, String creator, String modifiedBy, String ipAdd,
            String patProtId, String crfFlag, String crfDelFlag) {
        super();
        // TODO Auto-generated constructor stub
        setCrfId(crfId);
        setEvents1Id(events1Id);
        setCrfNumber(crfNumber);
        setCrfName(crfName);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setPatProtId(patProtId);
        setCrfFlag(crfFlag);
        setCrfDelFlag(crfDelFlag);
    }

}// end of class
