/*
 * Classname			CrflibDao.class
 * 
 * Version information 	1.0
 *
 * Date					31/10/2003
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.esch.business.common;

import java.util.ArrayList;

import com.velos.eres.service.util.Rlog;

/**
 * CrflibDao for getting crflib records
 * 
 * @author Anu Khanna
 * @version : 1.0 31/10/2003
 */

public class CrflibDao extends CommonDAO implements java.io.Serializable {

    private ArrayList eventsId;

    private ArrayList crflibName;

    private ArrayList crflibNumber;

    private ArrayList crflibFlag;

    private ArrayList creator;

    private ArrayList ipAdd;

    private ArrayList modifiedBy;

    private int rows;

    private ArrayList crflibId; // SV, 10/21/04, added to use in propagation

    // routine

    public CrflibDao() {
        eventsId = new ArrayList();
        crflibName = new ArrayList();
        crflibNumber = new ArrayList();
        crflibFlag = new ArrayList();
        creator = new ArrayList();
        modifiedBy = new ArrayList();
        ipAdd = new ArrayList();
        crflibId = new ArrayList(); // SV, 10/21
    }

    // //////////////////////////////////////////////////////////////////////////////

    public ArrayList getEventsId() {
        return this.eventsId;
    }

    public void setEventsId(ArrayList eventsId) {
        this.eventsId = eventsId;
    }

    public ArrayList getCrflibName() {
        return this.crflibName;
    }

    public void setCrflibName(ArrayList crflibName) {
        this.crflibName = crflibName;
    }

    public ArrayList getCrflibNumber() {
        return this.crflibNumber;
    }

    public void setCrflibNumber(ArrayList crflibNumber) {
        this.crflibNumber = crflibNumber;
    }

    public ArrayList getCrflibFlag() {
        return this.crflibFlag;
    }

    public void setCrflibFlag(ArrayList crflibFlag) {
        this.crflibFlag = crflibFlag;
    }

    public ArrayList getCreator() {
        return this.creator;
    }

    public void setCreator(ArrayList creator) {
        this.creator = creator;
    }

    public ArrayList getModifiedBy() {
        return this.modifiedBy;
    }

    public void setModifiedBy(ArrayList modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public ArrayList getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(ArrayList ipAdd) {
        this.ipAdd = ipAdd;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int i) {
        rows = i;
        Rlog.debug("crflib", "setRows()" + i);

    }

    // /SINGLE GETTERS AND SETTERS
    // /////////////////////////////////////////////////////////////////////////////////

    public Object getEventsId(int i) {

        return this.eventsId.get(i);
    }

    public void setEventsId(Integer eventsId) {
        this.eventsId.add(eventsId);
    }

    public Object getCrflibName(int i) {
        return this.crflibName.get(i);
    }

    public void setCrflibName(String crflibName) {
        this.crflibName.add(crflibName);
    }

    // ///////////////

    public Object getCrflibNumber(int i) {
        return this.crflibNumber.get(i);
    }

    public void setCrflibNumber(String crflibNumber) {
        this.crflibNumber.add(crflibNumber);
    }

    // //////////////

    public Object getCrflibFlag(int i) {
        return this.crflibFlag.get(i);
    }

    public void setCrflibFlag(String crflibFlag) {
        this.crflibFlag.add(crflibFlag);
    }

    public Object getCreator(int i) {

        return this.creator.get(i);

    }

    public void setCreator(String creator) {
        this.creator.add(creator);
    }

    public Object getModifiedBy(int i) {
        return this.modifiedBy.get(i);
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy.add(modifiedBy);
    }

    public Object getIpAdd(int i) {
        return this.ipAdd.get(i);
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd.add(ipAdd);
    }

    // SV, 10/21/04, added to use in propagate routine - cal-enh 04, 05.
    public ArrayList getCrflibId() {
        return this.crflibId;
    }

    public Object getCrflibId(int i) {
        return this.crflibId.get(i);
    }

    public void setCrflibId(ArrayList crflibId) {
        this.crflibId = crflibId;
    }

    public void setCrflibId(String crflibId) {
        this.crflibId.add(crflibId);
    }

    public void setCrflibId(int crflibId) {
        Integer id = new Integer(crflibId);
        this.crflibId.add(id.toString());
    }

    // SV, 10/21/04, end of changes for cal-enh-04

    /**
     * 
     * 
     */

    public void insertCrfNamesNums(ArrayList eventsId, ArrayList crflibName,
            ArrayList crflibNumber, ArrayList crflibFlag, ArrayList creator,
            ArrayList ipAdd)

    {

        setEventsId(eventsId);
        Rlog.debug("crflib", "crfLibDao.eventsid" + eventsId);
        setCrflibName(crflibName);
        Rlog.debug("crflib", "crfLibDao.crflibname" + crflibName);
        setCrflibNumber(crflibNumber);
        Rlog.debug("crflib", "crfLibDao.crflibnumber" + crflibNumber);
        setCrflibFlag(crflibFlag);
        Rlog.debug("crflib", "crfLibDao.crflibflag" + crflibFlag);
        setCreator(creator);
        Rlog.debug("crflib", "crfLibDao.creator" + creator);
        setIpAdd(ipAdd);
        Rlog.debug("crflib", "crfLibDao.ipadd" + ipAdd);
        Rlog.debug("crflib", "crfLibDao.NUMBER OF ROWS+ getRows()"
                + ipAdd.size());
        setRows(ipAdd.size());
        Rlog
                .debug("crflib", "crfLibDao.NUMBER OF ROWS starting###"
                        + getRows());

    }

}