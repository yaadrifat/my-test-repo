/*
 * Classname : AlertNotifyBean
 * 
 * Version information: 1.0
 *
 * Date: 11/27/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Arvind
 */

package com.velos.esch.business.alertNotify.impl;

/* Import Statements */
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.esch.service.util.Rlog;
import com.velos.esch.service.util.StringUtil;

// import java.sql.*;

/* End of Import Statements */
@Entity
@Table(name = "sch_alertnotify")
public class AlertNotifyBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3258698723281221680L;

    public Integer alNotId;

    public Integer alertNotifyPatProtId;

    public Integer alertNotifyCodelstAnId;

    public String alertNotifyAlNotType;

    public String alertNotifyAlNotUsers;

    public String alertNotifyAlNotMobEve;

    public Integer alertNotifyAlNotFlag;

    public Integer creator;

    public Integer modifiedBy;

    public String ipAdd;

    public String alertNotifyGlobalFlag;

    public Integer alertNotifyStudy;

    public Integer alertNotifyProtocol;

    // GETTER SETTER METHODS
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_SCH_ALERTNOTIFY", allocationSize=1)
    @Column(name = "PK_ALNOT")
    public Integer getAlNotId() {
        return this.alNotId;
    }

    public void setAlNotId(Integer alNotId) {
        this.alNotId = alNotId;
    }

    @Column(name = "FK_PATPROT")
    public String getAlertNotifyPatProtId() {
        return StringUtil.integerToString(this.alertNotifyPatProtId);
    }

    public void setAlertNotifyPatProtId(String alertNotifyPatProtId) {
        if (alertNotifyPatProtId != null) {
            this.alertNotifyPatProtId = Integer.valueOf(alertNotifyPatProtId);
        }
    }

    @Column(name = "FK_CODELST_AN")
    public String getAlertNotifyCodelstAnId() {
        return StringUtil.integerToString(this.alertNotifyCodelstAnId);
    }

    public void setAlertNotifyCodelstAnId(String alertNotifyCodelstAnId) {
        if (alertNotifyCodelstAnId != null) {
            this.alertNotifyCodelstAnId = Integer
                    .valueOf(alertNotifyCodelstAnId);
        }
    }

    @Column(name = "ALNOT_TYPE")
    public String getAlertNotifyAlNotType() {
        return this.alertNotifyAlNotType;
    }

    public void setAlertNotifyAlNotType(String alertNotifyAlNotType) {
        this.alertNotifyAlNotType = alertNotifyAlNotType;
    }

    @Column(name = "ALNOT_USERS")
    public String getAlertNotifyAlNotUsers() {
        return this.alertNotifyAlNotUsers;
    }

    public void setAlertNotifyAlNotUsers(String alertNotifyAlNotUsers) {
        this.alertNotifyAlNotUsers = alertNotifyAlNotUsers;
    }

    @Column(name = "ALNOT_MOBEVE")
    public String getAlertNotifyAlNotMobEve() {
        return this.alertNotifyAlNotMobEve;
    }

    public void setAlertNotifyAlNotMobEve(String alertNotifyAlNotMobEve) {
        this.alertNotifyAlNotMobEve = alertNotifyAlNotMobEve;
    }

    @Column(name = "ALNOT_FLAG")
    public String getAlertNotifyAlNotFlag() {
        return StringUtil.integerToString(this.alertNotifyAlNotFlag);
    }

    public void setAlertNotifyAlNotFlag(String alertNotifyAlNotFlag) {
        if (alertNotifyAlNotFlag != null) {
            this.alertNotifyAlNotFlag = Integer.valueOf(alertNotifyAlNotFlag);
        }
    }

    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    @Column(name = "ALNOT_GLOBALFLAG")
    public String getAlertNotifyGlobalFlag() {
        return this.alertNotifyGlobalFlag;
    }

    public void setAlertNotifyGlobalFlag(String alertNotifyGlobalFlag) {
        this.alertNotifyGlobalFlag = alertNotifyGlobalFlag;
    }

    @Column(name = "FK_PROTOCOL")
    public String getAlertNotifyProtocol() {
        return StringUtil.integerToString(this.alertNotifyProtocol);
    }

    public void setAlertNotifyProtocol(String alertNotifyProtocol) {
        if (alertNotifyProtocol != null
                && (alertNotifyProtocol.trim().length() > 0)) {
            this.alertNotifyProtocol = Integer.valueOf(alertNotifyProtocol);
        }

    }

    @Column(name = "FK_STUDY")
    public String getAlertNotifyStudy() {
        return StringUtil.integerToString(this.alertNotifyStudy);
    }

    public void setAlertNotifyStudy(String alertNotifyStudy) {
        if (alertNotifyStudy != null && (alertNotifyStudy.trim().length() > 0)) {
            this.alertNotifyStudy = Integer.valueOf(alertNotifyStudy);
        }

    }

    // END OF GETTER SETTER METHODS

    /**
     * updates the alertNotify details
     * 
     * @param edsk
     * @return 0 if successful; -2 for exception
     */
    public int updateAlertNotify(AlertNotifyBean edsk) {
        try {

            setAlNotId(edsk.getAlNotId());
            Rlog.debug("eventinfo", "In AlertNotifyBean.getAlNotId()()"
                    + edsk.getAlNotId());

            setAlertNotifyPatProtId(edsk.getAlertNotifyPatProtId());
            Rlog.debug("eventinfo",
                    "In AlertNotifyBean.getAlertNotifyPatProtId() 0"
                            + edsk.getAlertNotifyPatProtId());

            setAlertNotifyCodelstAnId(edsk.getAlertNotifyCodelstAnId());
            Rlog.debug("eventinfo",
                    "In AlertNotifyBean.edsk.getAlertNotifyCodelstAnId()()1"
                            + edsk.getAlertNotifyCodelstAnId());

            setAlertNotifyAlNotType(edsk.getAlertNotifyAlNotType());
            Rlog.debug("eventinfo",
                    "In AlertNotifyBean.edsk.getAlertNotifyAlNotType()()2"
                            + edsk.getAlertNotifyAlNotType());

            setAlertNotifyAlNotUsers(edsk.getAlertNotifyAlNotUsers());
            Rlog.debug("eventinfo",
                    "In AlertNotifyBean.edsk.getAlertNotifyAlNotUsers()"
                            + edsk.getAlertNotifyAlNotUsers());

            setAlertNotifyAlNotMobEve(edsk.getAlertNotifyAlNotMobEve());
            Rlog.debug("eventinfo", "In edsk.getAlertNotifyAlNotMobEve()"
                    + edsk.getAlertNotifyAlNotMobEve());

            setAlertNotifyAlNotFlag(edsk.getAlertNotifyAlNotFlag());
            Rlog.debug("eventinfo",
                    "In AlertNotifyBean.edsk.getAlertNotifyAlNotFlag())"
                            + edsk.getAlertNotifyAlNotFlag());

            setCreator(edsk.getCreator());
            Rlog.debug("eventinfo", "edsk.getCreator()" + edsk.getCreator());
            setModifiedBy(edsk.getModifiedBy());
            Rlog.debug("eventinfo", "In AlertNotifyBean.edsk.getModifiedBy()"
                    + edsk.getModifiedBy());
            setIpAdd(edsk.getIpAdd());
            Rlog.debug("eventinfo", "In AlertNotifyBean.edsk.getIpAdd()"
                    + edsk.getIpAdd());

            setAlertNotifyProtocol(edsk.getAlertNotifyProtocol());
            setAlertNotifyStudy(edsk.getAlertNotifyStudy());
            setAlertNotifyGlobalFlag(edsk.getAlertNotifyGlobalFlag());

            return 0;
        } catch (Exception e) {
            Rlog.fatal("alertNotify",
                    " error in AlertNotifyBean.updateAlertNotify : " + e);
            return -2;
        }
    }

    public AlertNotifyBean() {

    }

    public AlertNotifyBean(Integer alNotId, String alertNotifyPatProtId,
            String alertNotifyCodelstAnId, String alertNotifyAlNotType,
            String alertNotifyAlNotUsers, String alertNotifyAlNotMobEve,
            String alertNotifyAlNotFlag, String creator, String modifiedBy,
            String ipAdd, String alertNotifyGlobalFlag,
            String alertNotifyStudy, String alertNotifyProtocol) {
        super();
        // TODO Auto-generated constructor stub
        setAlNotId(alNotId);
        setAlertNotifyPatProtId(alertNotifyPatProtId);
        setAlertNotifyCodelstAnId(alertNotifyCodelstAnId);
        setAlertNotifyAlNotType(alertNotifyAlNotType);
        setAlertNotifyAlNotUsers(alertNotifyAlNotUsers);
        setAlertNotifyAlNotMobEve(alertNotifyAlNotMobEve);
        setAlertNotifyAlNotFlag(alertNotifyAlNotFlag);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setAlertNotifyGlobalFlag(alertNotifyGlobalFlag);
        setAlertNotifyStudy(alertNotifyStudy);
        setAlertNotifyProtocol(alertNotifyProtocol);
    }

}// end of class
