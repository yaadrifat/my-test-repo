/*
 * Classname : EventdefBean
 *
 * Version information: 1.0
 *
 * Date: 05/16/2001
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh Khurana /Vikas Chawla
 */

package com.velos.esch.business.eventdef.impl;

/* Import Statements */
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.esch.service.util.GenerateId;
import com.velos.esch.service.util.Rlog;
import com.velos.esch.service.util.StringUtil;

/* End of Import Statements */
@Entity
@Table(name = "event_def")
@NamedQuery(name = "findEventIdsDef", query = "SELECT OBJECT(event) FROM EventdefBean event where to_number(chain_id)=:chainId")
public class EventdefBean implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 3257568403769603890L;

    /**
     * event id
     */
    public Integer event_id;

    /**
     * chain id
     */
    public Integer chain_id;

    public int org_id;

    /**
     * event type
     */
    public String event_type;

    /**
     * event name
     */

    public String name;

    /**
     * event notes
     */
    public String notes;

    /**
     * cost of the event
     */
    public Integer cost;

    /**
     * cost description
     */
    public String cost_description;

    /**
     * event duration
     */
    public Integer duration;

    /**
     * user id
     */

    public Integer user_id;

    /**
     * linked uri
     */

    public String linked_uri;

    /**
     * fuzzy_period
     */
    public String fuzzy_period;

    /**
     * msg_to
     */
    public String msg_to;

    
    /**
     * description
     */

    public String description;

    /**
     * displacement
     */
    public Integer displacement;

    /**
     * event_msg
     */
    public String event_msg;

    /**
     * patDaysBefore
     */

    public Integer patDaysBefore;

    /**
     * patDaysAfter
     */

    public Integer patDaysAfter;

    /**
     * usrDaysBefore
     */

    public Integer usrDaysBefore;

    /**
     * usrDaysAfter
     */

    public Integer usrDaysAfter;

    /**
     * patMsgBefore
     */

    public String patMsgBefore;

    /**
     * patMsgAfter
     */

    public String patMsgAfter;

    /**
     * usrMsgBefore
     */

    public String usrMsgBefore;

    /**
     * usrMsgAfter
     */

    public String usrMsgAfter;

    /*
     * the record creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * the IP Address
     */
    public String ipAdd;

    /*
     * Calendar Shared With
     */

    public String calSharedWith = ""; // SV, 9/7, cal enh-01

    /*
     * Duration Type
     */

    public String durationUnit = ""; // SV, 9/14, cal enh-02

    /*
     * Event's visit key
     */

    public Integer eventVisit; // SV, 9/29/04, cal-enh-03-b

    public String durationUnitAfter;

    public String cptCode;

    public String fuzzyAfter;

    private String durationUnitBefore;

    private Integer eventCalType;

    private String eventCategory;//KM

    private String eventSequence; //JM:27Mar2008

    private String eventLibType;

    private String eventLineCategory;

    /* Event Site of Service */
    private Integer eventSOSId;

    /* Event Facility */
    private Integer eventFacilityId;

    /* Event Coverage Type */
    private Integer eventCoverageType;

    /* Reason for change in coverage type */

    private String reasonForCoverageChange;
    
    /* Coverage Notes*/
    
    private String coverageNotes; 
    
    
    /* calendar status */
    
    private Integer statCode;
    


    // GETTER SETTER METHODS
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "EVENT_DEFINITION_SEQ", allocationSize=1)
    @Column(name = "event_id")
    public Integer getEvent_id() {
        return this.event_id;
    }

    public void setEvent_id(Integer event_id) {
        this.event_id = event_id;
    }

    @Column(name = "org_id")
    public int getOrg_id() {
        return this.org_id;
    }

    public void setOrg_id(int org_id) {
        this.org_id = org_id;
    }

    @Column(name = "chain_id")
    public String getChain_id() {
        return StringUtil.integerToString(this.chain_id);
    }

    public void setChain_id(String chain_id) {
        if (chain_id != null) {
            this.chain_id = Integer.valueOf(chain_id);
        }
    }

    @Column(name = "event_type")
    public String getEvent_type() {
        return this.event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    @Column(name = "name")
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "notes")
    public String getNotes() {
        return this.notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Column(name = "cost")
    public String getCost() {
        return StringUtil.integerToString(this.cost);
    }

    public void setCost(String cost) {
        if (cost != null) {
            this.cost = Integer.valueOf(cost);
        }

    }

    @Column(name = "cost_description")
    public String getCost_description() {
        return this.cost_description;
    }

    public void setCost_description(String cost_description) {
        this.cost_description = cost_description;
    }

    @Column(name = "duration")
    public String getDuration() {
        return StringUtil.integerToString(this.duration);
    }

    public void setDuration(String duration) {
        if (duration != null) {
            this.duration = Integer.valueOf(duration);
        }
    }

    @Column(name = "user_id")
    public String getUser_id() {
        return StringUtil.integerToString(this.user_id);
    }

    public void setUser_id(String user_id) {
        if (user_id != null) {
            this.user_id = Integer.valueOf(user_id);
        }
    }

    @Column(name = "linked_uri")
    public String getLinked_uri() {
        return this.linked_uri;
    }

    public void setLinked_uri(String linked_uri) {
        this.linked_uri = linked_uri;
    }

    @Column(name = "fuzzy_period")
    public String getFuzzy_period() {
        return this.fuzzy_period;
    }

    public void setFuzzy_period(String fuzzy_period) {
        this.fuzzy_period = fuzzy_period;
    }

    @Transient
    public String getMsg_to() {
        return this.msg_to;
    }

    public void setMsg_to(String msg_to) {
        this.msg_to = msg_to;
    }

        
  //JM: 20JAN2011: enh #D-FIN-9
  	@Column(name = "FK_CODELST_CALSTAT")
    public String getStatCode() {
        return StringUtil.integerToString(this.statCode);
    }

    public void setStatCode(String statCode) {
    	if (statCode != null){
    		this.statCode = Integer.valueOf(statCode);
    	}
    }


    @Column(name = "description")
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "displacement")
    public String getDisplacement() {
        return StringUtil.integerToString(this.displacement);
    }

    public void setDisplacement(String displacement) {
        if (displacement != null) {
            this.displacement = Integer.valueOf(displacement);
        }
    }

    @Transient
    public String getEvent_msg() {
        return this.event_msg;
    }

    public void setEvent_msg(String event_msg) {
        this.event_msg = event_msg;
    }

    // ////////////////
    @Column(name = "PAT_DAYSBEFORE")
    public String getPatDaysBefore() {
        return StringUtil.integerToString(this.patDaysBefore);
    }

    public void setPatDaysBefore(String patDaysBefore) {
        if (patDaysBefore != null) {
            this.patDaysBefore = Integer.valueOf(patDaysBefore);
        }
    }

    @Column(name = "PAT_DAYSAFTER")
    public String getPatDaysAfter() {
        return StringUtil.integerToString(this.patDaysAfter);
    }

    public void setPatDaysAfter(String patDaysAfter) {
        if (patDaysAfter != null) {
            this.patDaysAfter = Integer.valueOf(patDaysAfter);
        }
    }

    @Column(name = "USR_DAYSBEFORE")
    public String getUsrDaysBefore() {
        return StringUtil.integerToString(this.usrDaysBefore);
    }

    public void setUsrDaysBefore(String usrDaysBefore) {
        if (usrDaysBefore != null) {
            this.usrDaysBefore = Integer.valueOf(usrDaysBefore);
        }
    }

    @Column(name = "USR_DAYSAFTER")
    public String getUsrDaysAfter() {
        return StringUtil.integerToString(this.usrDaysAfter);
    }

    public void setUsrDaysAfter(String usrDaysAfter) {
        if (usrDaysAfter != null) {
            this.usrDaysAfter = Integer.valueOf(usrDaysAfter);
        }
    }

    @Column(name = "PAT_MSGBEFORE")
    public String getPatMsgBefore() {
        return this.patMsgBefore;
    }

    public void setPatMsgBefore(String patMsgBefore) {
        this.patMsgBefore = patMsgBefore;
    }

    @Column(name = "PAT_MSGAFTER")
    public String getPatMsgAfter() {
        return this.patMsgAfter;
    }

    public void setPatMsgAfter(String patMsgAfter) {
        this.patMsgAfter = patMsgAfter;
    }

    @Column(name = "USR_MSGBEFORE")
    public String getUsrMsgBefore() {
        return this.usrMsgBefore;
    }

    public void setUsrMsgBefore(String usrMsgBefore) {
        this.usrMsgBefore = usrMsgBefore;
    }

    @Column(name = "USR_MSGAFTER")
    public String getUsrMsgAfter() {
        return this.usrMsgAfter;
    }

    public void setUsrMsgAfter(String usrMsgAfter) {
        this.usrMsgAfter = usrMsgAfter;
    }

    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // SV, 9/7/04, cal enh-01
    @Column(name = "calendar_sharedwith")
    public String getCalSharedWith() {
        return this.calSharedWith;
    }

    public void setCalSharedWith(String sharedWith) {
        if (sharedWith != null) {
            this.calSharedWith = sharedWith;
        }
    }

    // SV, 9/7/04, end cal enh-01

    // SV, 9/14/04, cal enh-02
    @Column(name = "duration_unit")
    public String getDurationUnit() {
        return this.durationUnit;
    }

    public void setDurationUnit(String durationUnit) {
        if (durationUnit != null) {
            this.durationUnit = durationUnit;
        }
    }

    // SV, 9/7/04, end cal enh-01

    // SV, 9/29/04, cal enh-02
    @Column(name = "fk_visit")
    public String getEventVisit() {
        return StringUtil.integerToString(this.eventVisit);

    }

    public void setEventVisit(String eventVisit) {
        if (eventVisit != null) {
            this.eventVisit = Integer.valueOf(eventVisit);

        }
    }

    // SV, 9/29/04, end cal enh-01

    // //////////////////

    // VA 09/21/2005
    @Column(name = "EVENT_CPTCODE")
    public String getCptCode() {
        return cptCode;
    }

    public void setCptCode(String cptCode) {
        this.cptCode = cptCode;
    }

    @Column(name = "event_durationafter")
    public String getDurationUnitAfter() {
        return durationUnitAfter;
    }

    public void setDurationUnitAfter(String durationUnitAfter) {
        this.durationUnitAfter = durationUnitAfter;
    }

    @Column(name = "event_durationbefore")
    public String getDurationUnitBefore() {
        return durationUnitBefore;
    }

    public void setDurationUnitBefore(String durationUnit) {
        this.durationUnitBefore = durationUnit;
    }

    @Column(name = "event_fuzzyafter")
    public String getFuzzyAfter() {
        return fuzzyAfter;
    }

    public void setFuzzyAfter(String fuzzyAfter) {
        this.fuzzyAfter = fuzzyAfter;
    }
    @Column(name = "FK_CATLIB")
    public Integer getEventCalType() {
        return eventCalType;
    }

    public void setEventCalType(Integer eventCalType) {
        this.eventCalType = eventCalType;
    }

    //KM
    @Column(name = "EVENT_CATEGORY")
    public String getEventCategory() {
    	return eventCategory;
    }

    public void setEventCategory(String eventCategory) {
    	this.eventCategory = eventCategory;
    }



    //JM: 27Mar2008

    @Column(name = "EVENT_SEQUENCE")
    public String getEventSequence() {
        return eventSequence;
    }

    public void setEventSequence(String eventSequence) {
        this.eventSequence = eventSequence;
    }

    @Column(name = "EVENT_LIBRARY_TYPE")
    public String getEventLibType() {
        return eventLibType;
    }

    public void setEventLibType(String eventLibType) {
        this.eventLibType = eventLibType;
    }

    @Column(name = "EVENT_LINE_CATEGORY")
    public String getEventLineCategory() {
        return eventLineCategory;
    }

    public void setEventLineCategory(String eventLineCategory) {
        this.eventLineCategory = eventLineCategory;
    }

    @Column(name = "SERVICE_SITE_ID")
    public String getEventSOSId() {
        return StringUtil.integerToString(this.eventSOSId);
    }

    public void setEventSOSId(String eventSOSId) {
        if (eventSOSId != null) {
            this.eventSOSId = Integer.valueOf(eventSOSId);
        }
    }

    @Column(name = "FACILITY_ID")
    public String getEventFacilityId() {
        return StringUtil.integerToString(this.eventFacilityId);
    }

    public void setEventFacilityId(String eventFacilityId) {
    	if (eventFacilityId != null) {
            this.eventFacilityId = Integer.valueOf(eventFacilityId);
        }
    }

    @Column(name = "FK_CODELST_COVERTYPE")
    public String getEventCoverageType() {
        return StringUtil.integerToString(this.eventCoverageType);
    }

    public void setEventCoverageType(String eventCoverageType) {
    	if (eventCoverageType != null) {
            this.eventCoverageType = Integer.valueOf(eventCoverageType);
        }
    }


    @Column(name = "REASON_FOR_COVERAGECHANGE")
    public String getReasonForCoverageChange() {
        return reasonForCoverageChange;
    }

    public void setReasonForCoverageChange(String reasonForCoverageChange) {
        this.reasonForCoverageChange = reasonForCoverageChange;
    }
    
  //BK 23rd july 2010
    @Column(name = "COVERAGE_NOTES")  
      public String getCoverageNotes() {
  		return coverageNotes;
  	}

  	public void setCoverageNotes(String coverageNotes) {
  		this.coverageNotes = coverageNotes;
  	}		

    // END OF GETTER SETTER METHODS

    public void cloneObject(EventdefBean edsk) {
        GenerateId eventId = null;
        int status = 0;

        try {

            Rlog.debug("eventdef", "after setting the event id");
            if (edsk.getEvent_type().equalsIgnoreCase("L")
                    || edsk.getEvent_type().equalsIgnoreCase("P")) {
                setChain_id(Integer.toString(event_id));
            } else {
                setChain_id(edsk.getChain_id());
            }

            setEvent_type(edsk.getEvent_type());
            setName(edsk.getName());
            setNotes(edsk.getNotes());
            setCost(edsk.getCost());
            setCost_description(edsk.getCost_description());
            setDuration(edsk.getDuration());
            setUser_id(edsk.getUser_id());
            setLinked_uri(edsk.getLinked_uri());
            setFuzzy_period(edsk.getFuzzy_period());
            setMsg_to(edsk.getMsg_to());            
            setDescription(edsk.getDescription());
            setDisplacement(edsk.getDisplacement());
            setOrg_id(edsk.getOrg_id());
            setEvent_msg(edsk.getEvent_msg());
            setPatDaysBefore(edsk.getPatDaysBefore());
            setPatDaysAfter(edsk.getPatDaysAfter());
            setUsrDaysBefore(edsk.getUsrDaysBefore());
            setUsrDaysAfter(edsk.getUsrDaysAfter());
            setPatMsgBefore(edsk.getPatMsgBefore());
            setPatMsgAfter(edsk.getPatMsgAfter());
            setUsrMsgBefore(edsk.getUsrMsgBefore());
            setUsrMsgAfter(edsk.getUsrMsgAfter());
            setCalSharedWith(edsk.getCalSharedWith());
            setDurationUnit(edsk.getDurationUnit()); // SV, 9/14
            setEventVisit(edsk.getEventVisit()); // SV, 9/29
            this.setCptCode(edsk.getCptCode());
            this.setDurationUnitAfter(edsk.getDurationUnitAfter());
            this.setDurationUnitBefore(edsk.getDurationUnitBefore());
            this.setFuzzyAfter(edsk.getFuzzyAfter());
            this.setEventCalType(edsk.getEventCalType());

            Rlog.debug("eventdef",
                    "EventdefBean.setEventdefStateKeeper() GET   CREATOR:"
                            + edsk.getCreator());

            setCreator(edsk.getCreator());

            Rlog.debug("eventdef",
                    "EventdefBean.setEventdefStateKeeper() CREATOR SET :"
                            + this.creator);
            /*Commented setModified Method to fix Bug Scenario 3335 by Mukul*/
            //setModifiedBy(edsk.getModifiedBy());
            setIpAdd(edsk.getIpAdd());
            setEventCategory(edsk.getEventCategory()); //KM

            setEventSequence(edsk.getEventSequence());	//JM: 27Mar08
            setEventLibType(edsk.getEventLibType());
            setEventLineCategory(edsk.getEventLineCategory());

            setEventSOSId(edsk.getEventSOSId());
            setEventFacilityId(edsk.getEventFacilityId());
            setEventCoverageType(edsk.getEventCoverageType());
            this.setCoverageNotes(edsk.getCoverageNotes());
            Rlog.debug("eventdef",
                    "EventdefBean.setEventdefStateKeeper() eventdefId :"
                            + event_id);
        }

        catch (Exception e) {
            System.out
                    .println("Error in  setEventdefStateKeeper() in EventdefBean "
                            + e);
        }
    }

    /**
     * updates the eventdef details
     *
     * @param edsk
     * @return 0 if successful; -2 for exception
     */
    public int updateEventdef(EventdefBean edsk) {
        try {

            setChain_id(edsk.getChain_id());
            setEvent_type(edsk.getEvent_type());
            setName(edsk.getName());
            setNotes(edsk.getNotes());
            setCost(edsk.getCost());
            setCost_description(edsk.getCost_description());
            setDuration(edsk.getDuration());
            setUser_id(edsk.getUser_id());
            setLinked_uri(edsk.getLinked_uri());
            setFuzzy_period(edsk.getFuzzy_period());
            setMsg_to(edsk.getMsg_to());            
            setDescription(edsk.getDescription());
            setDisplacement(edsk.getDisplacement());
            setOrg_id(edsk.getOrg_id());
            setEvent_msg(edsk.getEvent_msg());

            Rlog.debug("eventdef", "after setting edsk.getPatDaysBefore()"
                    + edsk.getPatDaysBefore() + "-");
            setPatDaysBefore(edsk.getPatDaysBefore());

            Rlog.debug("eventdef", "after setting edsk.getPatDaysAfter()"
                    + edsk.getPatDaysAfter() + "-");
            setPatDaysAfter(edsk.getPatDaysAfter());

            Rlog.debug("eventdef", "after setting edsk.getUsrDaysBefore()"
                    + edsk.getUsrDaysBefore() + "-");
            setUsrDaysBefore(edsk.getUsrDaysBefore());

            Rlog.debug("eventdef", "after setting edsk.getUsrDaysAfter()"
                    + edsk.getUsrDaysAfter() + "-");
            setUsrDaysAfter(edsk.getUsrDaysAfter());

            Rlog.debug("eventdef", "after setting edsk.getPatMsgBefore()"
                    + edsk.getPatMsgBefore() + "-");
            setPatMsgBefore(edsk.getPatMsgBefore());

            Rlog.debug("eventdef", "after setting edsk.getPatMsgAfter()"
                    + edsk.getPatMsgAfter() + "-");
            setPatMsgAfter(edsk.getPatMsgAfter());

            Rlog.debug("eventdef", "after setting getUsrMsgBefore()"
                    + getUsrMsgBefore() + "-");
            setUsrMsgBefore(edsk.getUsrMsgBefore());

            Rlog.debug("eventdef", "after setting edsk.getUsrMsgAfter()"
                    + edsk.getUsrMsgAfter() + "-");
            setUsrMsgAfter(edsk.getUsrMsgAfter());
            setCreator(edsk.getCreator());
            setModifiedBy(edsk.getModifiedBy());
            setIpAdd(edsk.getIpAdd());

            setCalSharedWith(edsk.getCalSharedWith());

            setDurationUnit(edsk.getDurationUnit()); // SV, 9/14/04
            setEventVisit(edsk.getEventVisit()); // SV, 9/29/04

            this.setCptCode(edsk.getCptCode());
            this.setDurationUnitAfter(edsk.getDurationUnitAfter());
            this.setDurationUnitBefore(edsk.getDurationUnitBefore());
            this.setFuzzyAfter(edsk.getFuzzyAfter());
            this.setEventCalType(edsk.getEventCalType());
            this.setEventCategory(edsk.getEventCategory());//KM
            this.setEventSequence(edsk.getEventSequence());	//JM: 27Mar08
            this.setEventLibType(edsk.getEventLibType());
            this.setEventLineCategory(edsk.getEventLineCategory());

            setEventSOSId(edsk.getEventSOSId());
            setEventFacilityId(edsk.getEventFacilityId());
            setEventCoverageType(edsk.getEventCoverageType());
            setReasonForCoverageChange(edsk.getReasonForCoverageChange());
            this.setCoverageNotes(edsk.getCoverageNotes()); 
            this.setStatCode(edsk.getStatCode());
            return 0;
        } catch (Exception e) {
            Rlog.fatal("eventdef", " error in EventdefBean.updateEventdef : "
                    + e);
            return -2;
        }
    }

    public EventdefBean() {

    }

    public EventdefBean(Integer event_id, String chain_id, int org_id,
            String event_type, String name, String notes, String cost,
            String cost_description, String duration, String user_id,
            String linked_uri, String fuzzy_period, String msg_to,
            String description, String displacement,
            String event_msg, String patDaysBefore, String patDaysAfter,
            String usrDaysBefore, String usrDaysAfter, String patMsgBefore,
            String patMsgAfter, String usrMsgBefore, String usrMsgAfter,
            String creator, String modifiedBy, String ipAdd,
            String calSharedWith, String durationUnit, String eventVisit,
            String cptCode, String durationUnitAfter, String fuzzyAfter,
            String durationUnitBefore,String eventCalType, String eventCategory, String sequence,
            String eventLibType, String eventLineCategory,
            String eventSOSId, String eventFacilityId, String eventCoverageType, String reasonForCoverageChange, 
            String coverageNotes,String statCode) {

        super();
        // TODO Auto-generated constructor stub
        setEvent_id(event_id);
        setChain_id(chain_id);
        setOrg_id(org_id);
        setEvent_type(event_type);
        setName(name);
        setNotes(notes);
        setCost(cost);
        setCost_description(cost_description);
        setDuration(duration);
        setUser_id(user_id);
        setLinked_uri(linked_uri);
        setFuzzy_period(fuzzy_period);
        setMsg_to(msg_to);        
        setDescription(description);
        setDisplacement(displacement);
        setEvent_msg(event_msg);
        setPatDaysBefore(patDaysBefore);
        setPatDaysAfter(patDaysAfter);
        setUsrDaysBefore(usrDaysBefore);
        setUsrDaysAfter(usrDaysAfter);
        setPatMsgBefore(patMsgBefore);
        setPatMsgAfter(patMsgAfter);
        setUsrMsgBefore(usrMsgBefore);
        setUsrMsgAfter(usrMsgAfter);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setCalSharedWith(calSharedWith);
        setDurationUnit(durationUnit);
        setEventVisit(eventVisit);
        this.setCptCode(cptCode);
        this.setDurationUnitAfter(durationUnitAfter);
        this.setDurationUnitBefore(durationUnitBefore);
        this.setFuzzyAfter(fuzzyAfter);
        this.setEventCalType(StringUtil.stringToNum(eventCalType));
        this.setEventCategory(eventCategory);//KM
        this.setEventSequence(sequence);//JM: 27Mar08
        this.setEventLibType(eventLibType);
        this.setEventLineCategory(eventLineCategory);

        setEventSOSId(eventSOSId);
        setEventFacilityId(eventFacilityId);
        setEventCoverageType(eventCoverageType);
        setReasonForCoverageChange(reasonForCoverageChange);
        //23rd july 2010 BK
        this.setCoverageNotes(coverageNotes);
        this.setStatCode(statCode);

    }



}// end of class
