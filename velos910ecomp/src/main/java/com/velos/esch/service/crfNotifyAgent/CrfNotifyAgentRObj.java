/*
 * Classname : CrfNotifyAgentRObj
 * 
 * Version information: 1.0
 *
 * Date: 11/27/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Arvind
 */

package com.velos.esch.service.crfNotifyAgent;

/* Import Statements */
import javax.ejb.Remote;

import com.velos.esch.business.common.CrfNotifyDao;
import com.velos.esch.business.crfNotify.impl.CrfNotifyBean;

@Remote
public interface CrfNotifyAgentRObj {

    CrfNotifyBean getCrfNotifyDetails(int crfNotifyId);

    public int setCrfNotifyDetails(CrfNotifyBean account);

    public int updateCrfNotify(CrfNotifyBean usk);

    public CrfNotifyDao getCrfNotifyValues(int patProtId);

    public CrfNotifyDao getCrfNotifyValues(int FK_STUDY, int FK_PROTOCOL);
}