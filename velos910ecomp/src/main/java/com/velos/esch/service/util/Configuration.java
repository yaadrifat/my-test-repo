/* 
 * $Id: Configuration.java,v 1.4 2011/06/30 23:02:58 ihuang Exp $
 * Copyright 2000 Velos, Inc. All rights reserved.
 */

package com.velos.esch.service.util;

/**
 * This class reads the configuration parameters from xml files and stores in
 * the staic variables.when the starts up the function of this class is called
 * to setup the values.
 */

import java.io.IOException;
import java.util.Hashtable;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Configuration {
    private static Hashtable appServerParam;

    private static Hashtable classURLs;

    private static Hashtable businessLogicClass;

    public static String DBDriverName;

    public static String DBUrl;

    public static String DBUser;

    public static String DBPwd;

    public static String dsJndiName;

    public static String ESCH_HOME;

    /**
     * This method is called when the server starts up, typically from startup
     * servelt.
     */
    public static void readSettings() {
        String fileName = null;
        String eHome = null;

        try {
            eHome = EnvUtil.getEnvVariable("ESCH_HOME");
            eHome = (eHome == null) ? "" : eHome;
            if (eHome.trim().equals("%ESCH_HOME%"))
                eHome = System.getProperty("ESCH_HOME");
            ESCH_HOME = eHome;
            eHome = eHome.trim();
            Rlog.debug("common", "ESCH home is:" + ESCH_HOME);

            fileName = eHome + "escheduling.xml";

            Rlog.debug("common", "File Name is:" + fileName);

            Rlog.debug("common", "Read Settings");
            Document root = DOMUtil.readDocument(fileName);
            /**
             * read the application server parameters
             */
            readAppserverParam(root);
            Rlog.debug("common", "After reading the server parameter");

            /**
             * read the class & url parameters
             */
            // readClassURLs(root);
            readDBParam(root);

            // readAppendixParam(root);

        } catch (IOException ie) {
            Rlog.fatal("common", "io ex" + ie);
        } catch (SAXException se) {
            Rlog.fatal("common", "sax ex" + se);
        } catch (Exception e) {
            Rlog.fatal("common", "e ex" + e);
        }
    }

    /**
     * Reads the application server parameters. first read the vendor name then
     * based on the vendor name, reads the vendor specific server parameters.
     */

    private static void readAppserverParam(Document root) {
        String name, value;
        appServerParam = new Hashtable();
        Rlog.debug("common", "Application Server Param");
        Node appServer = DOMUtil.findNode(root, "appserver");
        Node vendor = DOMUtil.findNode(appServer, "vendor");

        String venName = (vendor.getFirstChild()).getNodeValue();
        Rlog.debug("common", "Vendor Name" + venName);
        Node venNode = DOMUtil.findNode(appServer, venName);

        Node paramNode = null;
        NodeList nl = venNode.getChildNodes();

        int len = nl.getLength();

        for (int i = 0; i < len; i++) {
            paramNode = nl.item(i);
            if (paramNode == null || paramNode.toString().trim().equals(""))
                continue;

            // if (paramNode.toString().trim().equals("dsjndi")){
            // dsJndiName=DOMUtil.findNode(paramNode,"name").getFirstChild().getNodeValue();
            // }else
            name = DOMUtil.findNode(paramNode, "name").getFirstChild()
                    .getNodeValue();

            Rlog.debug("common", "name:" + name);

            if (name.equals("dsjndi")) {
                dsJndiName = DOMUtil.findNode(paramNode, "value")
                        .getFirstChild().getNodeValue();
                Rlog.debug("common", "DAtasource Jndi Name" + dsJndiName);
                Rlog.debug("common", "test message");
            } else {
                value = DOMUtil.findNode(paramNode, "value").getFirstChild()
                        .getNodeValue();
                appServerParam.put(name, value);
            }
            Rlog.debug("common", "return after reading the server parameter");

        }
    }

    /**
     * read the url & class parameters for all modules.
     */

    private static void findURLNode(NodeList nl) {
        String name = null, value = null, logicClass = null;
        Node paramNode = null;
        Node attr = null;

        int len = nl.getLength();

        for (int i = 0; i < len; i++) {
            paramNode = nl.item(i);

            if (paramNode == null || paramNode.toString().trim().equals(""))
                continue;
            // get the url tag and corresponding class and logic class values
            if ((paramNode.getNodeName().equals("url"))) {
                name = paramNode.getFirstChild().getNodeValue();
                try {
                    attr = (paramNode.getAttributes()).getNamedItem("class");
                    if (attr != null)
                        value = attr.getNodeValue();
                } catch (Exception e) {

                }

                try {
                    attr = (paramNode.getAttributes()).getNamedItem("logic");
                    if (attr != null)
                        logicClass = attr.getNodeValue();
                } catch (Exception e) {

                }

                // store all values in hash table
                classURLs.put(name, value);
                businessLogicClass.put(name, logicClass);
            } else {
                findURLNode(paramNode.getChildNodes());
            }

        }
        return;
    }

    /**
     * reads database connection parameters.
     */

    private static void readDBParam(Document root) {
        Rlog.debug("common", "reading the database paramenters");
        // String name,value ;

        // DBParam=new Hashtable();

        Node dbNode = null;
        Node CurrentBDNode = null;

        dbNode = DOMUtil.findNode(root, "DBURL");
        CurrentBDNode = DOMUtil.findNode(dbNode, "DB");
        Rlog.debug("common", "Current DB Node" + CurrentBDNode);

        DBDriverName = DOMUtil.findNode(CurrentBDNode, "DRIVER")
                .getFirstChild().getNodeValue();
        Rlog.debug("common", "DBDriverName" + DBDriverName);
        DBUrl = DOMUtil.findNode(CurrentBDNode, "DB_URL").getFirstChild()
                .getNodeValue();
        Rlog.debug("common", "DBUrl" + DBUrl);
        DBUser = DOMUtil.findNode(CurrentBDNode, "USERID").getFirstChild()
                .getNodeValue();
        Rlog.debug("common", "DBUser" + DBUser);
        DBPwd = DOMUtil.findNode(CurrentBDNode, "PWD").getFirstChild()
                .getNodeValue();
        Rlog.debug("common", "DBPwd" + DBPwd);

    }

    /**
     * reurns the application server parameter hash table
     */

    public static Hashtable getServerParam() {
        return appServerParam;
    }

    /**
     * reurns the url & class parameter hash table
     */

    public static Hashtable getClassURLs() {
        return classURLs;
    }

    public static Hashtable getLogicClass() {
        return businessLogicClass;
    }
}