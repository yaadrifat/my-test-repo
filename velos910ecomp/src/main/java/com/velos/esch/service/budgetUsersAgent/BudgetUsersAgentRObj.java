/* 
 * Classname			BudgetUsersAgentRObj.class
 * 
 * Version information
 *
 * Date					03/21/2002
 *
 * Author 				Sonika Talwar
 * Copyright notice
 */

package com.velos.esch.service.budgetUsersAgent;

/* Import Statements */

import java.util.Hashtable;
import javax.ejb.Remote;

import com.velos.esch.business.budgetUsers.impl.BudgetUsersBean;
import com.velos.esch.business.common.BudgetUsersDao;

@Remote
public interface BudgetUsersAgentRObj {

    BudgetUsersBean getBudgetUsersDetails(int bgtUsersId);

    public int setBudgetUsersDetails(BudgetUsersBean busk);

    public int updateBudgetUsers(BudgetUsersBean busk);

    public BudgetUsersDao getUsersNotInBudget(int accountId, int budgetId,
            String lname, String fname, String role, String organization,
            String userType);

    public BudgetUsersDao getUsersInBudget(int budgetId);

    public int removeBudgetUser(int bgtUserId);
    // Overloaded for INF-18183 ::: AGodara
    public int removeBudgetUser(int bgtUserId,Hashtable<String, String> auditInfo);

    public String getBudgetUserRight(int budgetId, int userId);
}