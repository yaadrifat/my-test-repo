/*
 * Classname : BudgetcalAgentBean
 * 
 * Version information: 1.0
 *
 * Date: 03/19/2002
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh Khurana 
 */

package com.velos.esch.service.budgetcalAgent.impl;

/* IMPORT STATEMENTS */

import java.util.Hashtable;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.service.util.DateUtil;
import com.velos.esch.business.audit.impl.AuditBean;
import com.velos.esch.business.budget.impl.BudgetBean;
import com.velos.esch.business.budgetcal.impl.BudgetcalBean;
import com.velos.esch.business.common.BudgetcalDao;
import com.velos.esch.service.budgetcalAgent.BudgetcalAgentRObj;
import com.velos.esch.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the entity CMP
 * BudgetcalBean.<br>
 * <br>
 * 
 */
@Stateless
public class BudgetcalAgentBean implements BudgetcalAgentRObj {
    @PersistenceContext(unitName = "esch")
    protected EntityManager em;
    @Resource private SessionContext context;

    /**
     * 
     */
    private static final long serialVersionUID = 3257007661496480563L;

    /*
     * * Calls getBudgetcalDetails() on Budgetcal Entity Bean BudgetcalBean.
     * Looks up on the Budgetcal id parameter passed in and constructs and
     * returns a Budgetcal state holder. containing all the summary details of
     * the Budgetcal<br>
     * 
     * @param budgetcalId the Budgetcal id @ee BudgetcalBean
     */
    public BudgetcalBean getBudgetcalDetails(int budgetcalId) {

        try {

            return (BudgetcalBean) em.find(BudgetcalBean.class, new Integer(
                    budgetcalId));

        } catch (Exception e) {
            System.out
                    .print("Error in getBudgetcalDetails() in BudgetcalAgentBean"
                            + e);
            return null;
        }

    }

    /**
     * Calls setBudgetcalDetails() on Budgetcal Entity Bean
     * BudgetcalBean.Creates a new Budgetcal with the values in the StateKeeper
     * object.
     * 
     * @param budgetcal
     *            a budgetcal state keeper containing budgetcal attributes for
     *            the new Budgetcal.
     * @return int - 0 if successful; -2 for Exception;
     */

    public int setBudgetcalDetails(BudgetcalBean bcsk) {
        try {

            BudgetcalBean bdtcal = new BudgetcalBean();

            bdtcal.updateBudgetcal(bcsk);

            em.persist(bdtcal);
            return (bdtcal.getBudgetcalId());

        }

        catch (Exception e) {
            System.out
                    .print("Error in setBudgetcalDetails() in BudgetcalAgentBean "
                            + e);
        }
        return 0;
    }

    public int removeBudgetcal(int budgetcalId) {

        BudgetcalBean retrieved = null; // Eventdef Entity Bean Remote Object
        int output = 0;

        try {

            retrieved = (BudgetcalBean) em.find(BudgetcalBean.class,
                    new Integer(budgetcalId));
        } catch (Exception e) {
            Rlog
                    .debug("eventbudgetcalId",
                            "Error in remove budgetcalChild  in budgetcalAgentBean"
                                    + e);
            return -2;
        }
        return output;
    }

    public int updateBudgetcal(BudgetcalBean bcsk) {

        BudgetcalBean retrieved = null; // Budgetcal Entity Bean Remote Object
        int output;

        try {

            retrieved = (BudgetcalBean) em.find(BudgetcalBean.class,
                    new Integer(bcsk.getBudgetcalId()));

            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updateBudgetcal(bcsk);
            em.merge(retrieved);

        } catch (Exception e) {
            Rlog.debug("budgetcal",
                    "Error in updateBudgetcal in BudgetcalAgentBean" + e);
            return -2;
        }
        return output;
    }
    // Overloaded for INF-18183 ::: Akshi
    public int updateBudgetcal(BudgetcalBean bcsk,Hashtable<String, String> args) {

        BudgetcalBean retrieved = null; // Budgetcal Entity Bean Remote Object
        int output;

        try {
        	
            retrieved = (BudgetcalBean) em.find(BudgetcalBean.class,
                    new Integer(bcsk.getBudgetcalId()));

            if (retrieved == null) {
                return -2;
            }
            
            AuditBean audit=null;
            String currdate =DateUtil.getCurrentDate();
            String userID=(String)args.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
            String ipAdd=(String)args.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
            String reason=(String)args.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
            String appmodule=(String)args.get(AuditUtils.APP_MODULE); /*Fetches the application module name from the Hashtable*/
            String getRidValue= AuditUtils.getRidValue("SCH_BGTCAL","esch","PK_BGTCAL="+bcsk.getBudgetcalId());/*Fetches the RID/PK_VALUE*/ 
            audit = new AuditBean("SCH_BGTCAL",String.valueOf(bcsk.getBudgetcalId()),getRidValue,
        			userID,currdate,appmodule,ipAdd,reason);/*POPULATE THE AUDIT BEAN*/ 
            em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/ 
            
            output = retrieved.updateBudgetcal(bcsk);
            em.merge(retrieved);

        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.debug("budgetcal",
                    "Error in updateBudgetcal in BudgetcalAgentBean" + e);
            return -2;
        }
        return output;
    }

    public BudgetcalDao getAllBgtCals(int budgetId) {
        Rlog.debug("budgetcal",
                "In getAllBgtCals in BudgetcalAgentBean line AAA");
        BudgetcalDao budgetcalDao = new BudgetcalDao();
        try {
            Rlog.debug("budgetcal",
                    "In getAllBgtCals in BudgetcalAgentBean line BBB");

            Rlog
                    .debug("budgetcal",
                            "In getAllBgtCals in BudgetcalAgentBean line number 1 ret ");
            budgetcalDao.getAllBgtCals(budgetId);
            Rlog.debug("budgetcal",
                    "In getAllBgtCals in BudgetcalAgentBean line CCC");
            Rlog
                    .debug("budgetcal",
                            "In getAllBgtCals in BudgetcalAgentBean line number 2 ret ");
            Rlog.debug("budgetcal",
                    "In getAllBgtCals in BudgetcalAgentBean line DDD");
        } catch (Exception e) {
            Rlog.fatal("budgetcal",
                    "Error in getAllBgtCals() in BudgetcalAgentBean " + e);
        }
        return budgetcalDao;
    }

    public BudgetcalDao getBgtcalDetail(int bgtcalId) {
        Rlog.debug("budgetcal", "In getBgtcalDetail in BudgetcalAgentBean");
        BudgetcalDao budgetcalDao = new BudgetcalDao();
        try {
            budgetcalDao.getBgtcalDetail(bgtcalId);
        } catch (Exception e) {
            Rlog.fatal("budgetcal",
                    "Error in getBgtcalDetail() in BudgetcalAgentBean " + e);
        }
        return budgetcalDao;
    }
    
    // ///////////////////////
    /*
     * Update lineitem_othercost when 'exclude SOC' is updated
     */

    public int updateOtherCostToExcludeSOC(String bgtCalId, int excludeFlag,String modifiedBy,String ipAdd) 
    {
        int ret = -1;
        
        BudgetcalDao budgetcalDao = new BudgetcalDao();
        try {
            ret = budgetcalDao.updateOtherCostToExcludeSOC(bgtCalId, excludeFlag,modifiedBy,ipAdd);
            
        } catch (Exception e) {
            Rlog.fatal("budgetcal",
                    "Error in updateOtherCostToExcludeSOC() in BudgetcalAgentBean " + e);
        }
        return ret;
    }
    
}// end of class
