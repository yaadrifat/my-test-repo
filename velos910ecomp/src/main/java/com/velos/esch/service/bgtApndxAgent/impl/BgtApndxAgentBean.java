/* 
 * Classname			BgtApndxAgentBean.class
 * 
 * Version information
 *
 * Date					03/26/2002
 *
 * Author 				Sajal
 * Copyright notice
 */

package com.velos.esch.service.bgtApndxAgent.impl;

/* IMPORT STATEMENTS */

import java.util.Hashtable;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.service.util.DateUtil;
import com.velos.esch.business.audit.impl.AuditBean;
import com.velos.esch.business.bgtApndx.impl.BgtApndxBean;
import com.velos.esch.business.common.BgtApndxDao;
import com.velos.esch.service.bgtApndxAgent.BgtApndxAgentRObj;
import com.velos.esch.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the entity CMP BgtApndxBean.
 * 
 */
@Stateless
public class BgtApndxAgentBean implements BgtApndxAgentRObj {

    @PersistenceContext(unitName = "esch")
    protected EntityManager em;
    
  	 
	@Resource
	private SessionContext context;

    /**
     * Calls getBgtApndxDetails() on Budget Appendix Entity Bean. Looks up on
     * the BgtApndx Id parameter passed in and constructs and returns a Budget
     * state keeper. containing all the summary details of the Budget<br>
     * 
     * @param BgtApndxId
     *            the Budget id
     * @ee BudgetBean
     */
    public BgtApndxBean getBgtApndxDetails(int bgtApndxId) {

        try {
            return (BgtApndxBean) em.find(BgtApndxBean.class, new Integer(
                    bgtApndxId));
        } catch (Exception e) {
            Rlog.debug("BgtApndx",
                    "Error in getBgtApndxDetails() in BgtApndxAgentBean" + e);
            return null;
        }

    }

    /**
     * Calls setBgtApndxDetails() on Budget Appendix Entity Bean
     * BudgetBean.Creates a new Budget Appendix with the values in the
     * StateKeeper object.
     * 
     * @param budget
     *            appendix state keeper containing budget appendix attributes
     *            for the new Budget appendix.
     * @return int - 0 if successful; -2 for Exception;
     */

    public int setBgtApndxDetails(BgtApndxBean bask) {
        try {
            BgtApndxBean bgtABean = new BgtApndxBean();
            bgtABean.updateBgtApndx(bask);
            em.persist(bgtABean);
            return (bgtABean.getBgtApndxId());
        } catch (Exception e) {
            Rlog.debug("BgtApndx",
                    "Error in setBgtApndxDetails() in BgtApndxAgentBean" + e);
        }
        return 0;
    }

    public int updateBgtApndx(BgtApndxBean bask) {

        BgtApndxBean retrieved = null; // Budget Entity Bean Remote Object
        int output;

        try {

            retrieved = (BgtApndxBean) em.find(BgtApndxBean.class, bask
                    .getBgtApndxId());

            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updateBgtApndx(bask);
            em.merge(retrieved);
        } catch (Exception e) {
            Rlog.debug("BgtApndx",
                    "Error in updateBgtApndx in BgtApndxAgentBean " + e);
            return -2;
        }
        return output;
    }

    /**
     * Gets all URLs linked with the budget
     * 
     * @param budgetId
     */
    public BgtApndxDao getBgtApndxUrls(int budgetId) {
        try {
            BgtApndxDao budgetApndxDao = new BgtApndxDao();
            budgetApndxDao.getBgtApndxUrls(budgetId);
            return budgetApndxDao;
        }

        catch (Exception e) {
            Rlog.fatal("BgtApndx",
                    "Error in getBgtApndxUrls in BgtApndxAgentBean" + e);
        }
        return null;
    }

    /**
     * Gets all Files linked with the budget
     * 
     * @param budgetId
     */
    public BgtApndxDao getBgtApndxFiles(int budgetId) {
        try {
            BgtApndxDao budgetApndxDao = new BgtApndxDao();
            budgetApndxDao.getBgtApndxFiles(budgetId);
            return budgetApndxDao;
        }

        catch (Exception e) {
            Rlog.fatal("BgtApndx",
                    "Error in getBgtApndxFiles in BgtApndxAgentBean" + e);
        }
        return null;
    }

    /**
     * Delete appendix file/url from budget
     * 
     * @return 0 for successful delete, -2 in case of exception
     */
    public int bgtApndxDelete(int bgtApndxId) {
        BgtApndxBean bgtApndxBean = null; // Budget Appendix Entity Bean
        // Remote
        // Object
        int ret = 0;
        try {
            Rlog.debug("BudgetUser", "remove Budget User");
            

            bgtApndxBean = (BgtApndxBean) em.find(BgtApndxBean.class,
                    new Integer(bgtApndxId));
            em.remove(bgtApndxBean);
            Rlog.debug("BgtApndx", "remove from BgtApndx");
            return 0;
        }

        catch (Exception e) {
            e.printStackTrace();
            Rlog.fatal("BgtApndx", "EXCEPTION IN REMOVING BUDGET APPENDIX" + e);
            return -2;
        }
    }
    
    // Overloaded for INF-18183 ::: AGodara
    public int bgtApndxDelete(int bgtApndxId,Hashtable<String, String> auditInfo) {
       
    	BgtApndxBean bgtApndxBean = null; // Budget Appendix Entity Bean
    	AuditBean audit=null;    
        
    	try {
    		String currdate =DateUtil.getCurrentDate();
        	String userID=(String)auditInfo.get(AuditUtils.USER_ID_KEY); //Fetches the User ID from the Hashtable
            String ipAdd=(String)auditInfo.get(AuditUtils.IP_ADD_KEY); //Fetches the IP Address from the Hashtable
            String reason=(String)auditInfo.get(AuditUtils.REASON_FOR_DEL); //Fetches the reason for deletion from the Hashtable
            String app_module=(String)auditInfo.get(AuditUtils.APP_MODULE);// Fetches the APP_Module from the Hashtable
            String rid= AuditUtils.getRidValue("SCH_BGTAPNDX","esch","PK_BGTAPNDX="+bgtApndxId); //Fetches the RID
            
            //POPULATE THE AUDIT BEAN 
            audit = new AuditBean("SCH_BGTAPNDX",String.valueOf(bgtApndxId),rid,userID,currdate,app_module,ipAdd,reason);
            bgtApndxBean = (BgtApndxBean) em.find(BgtApndxBean.class,
                    new Integer(bgtApndxId));
            
            em.persist(audit); //PERSIST THE DATA IN AUDIT TABLE
            em.remove(bgtApndxBean);
            return 0;
        }catch (Exception e) {
        	context.setRollbackOnly();
            e.printStackTrace();
            Rlog.fatal("BgtApndx", "EXCEPTION IN REMOVING BUDGET APPENDIX bgtApndxDelete(int bgtApndxId,Hashtable<String, String> auditInfo)" + e);
            return -2;
        }
    }
    

}// end of class
