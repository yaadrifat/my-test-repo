/*
 * Classname			EventKitAgentBean
 * 
 * Version information : 
 *
 * Date					05/09/2008
 * 
 * Copyright notice : Velos Inc
 */

package com.velos.esch.service.eventKitAgent.impl;

/* IMPORT STATEMENTS */

import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import java.util.Hashtable;
import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.service.util.DateUtil;
import com.velos.esch.business.audit.impl.AuditBean;

import com.velos.esch.business.common.EventKitDao;

import com.velos.esch.business.eventKit.impl.EventKitBean;

import com.velos.esch.service.eventKitAgent.EventKitAgentRObj;

import com.velos.esch.service.util.Rlog;


/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the entity CMP.
 * 
 * @author Sonia Abrol
 * @version 1.0, 05/09/2008
 * @ejbRemote EventKitAgentRObj
 */

@Stateless
public class EventKitAgentBean implements EventKitAgentRObj {

    @PersistenceContext(unitName = "esch")
    protected EntityManager em;
    @Resource private SessionContext context;

   
    public EventKitBean getEventKitDetails(int eventKitId) {
        EventKitBean eb = null;
        try {

            eb = (EventKitBean) em.find(EventKitBean.class, new Integer(eventKitId));

        } catch (Exception e) {
            Rlog.fatal("eventkit",
                    "Exception in getEventKitDetails() in EventKitAgentBean" + e);
        }

        return eb;
    }
    
    
    /**
     * Sets an EventKit.
     * 
     * @param eventkit
     *              new event kit record
     * @return int
     */

    public int setEventKitDetails(EventKitBean eventkit) {
        try {
        	EventKitBean cd = new EventKitBean();
            cd.updateEventKit(eventkit);
            em.persist(cd);
            return (cd.getEventKitId());
        } catch (Exception e) {
            System.out
                    .print("Exception in setEventKitDetails() in EventKitAgentBean "
                            + e);
        }
        return 0;
    }

    
    /**
     * Description of the Method
     * 
     * @param eventkit
     *            EventKitBean object for the event kit record for update
     * @return update success flag
     */
    public int updateEventKit(EventKitBean eventkit) {

    	EventKitBean retrieved = null;

        int output;

        try {

            retrieved = (EventKitBean) em.find(EventKitBean.class, new Integer(
            		eventkit.getEventKitId()));

            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updateEventKit(eventkit);
            em.merge(retrieved);
        } catch (Exception e) {
            Rlog.debug("eventkit", "Exception in updateEventKit in EventCRfAgentBean"
                    + e);
            return -2;
        }
        return output;
    }
    

    public EventKitDao getEventStorageKits(int eventId ){
    	try {
            EventKitDao kdao = new EventKitDao();
            kdao.getEventStorageKits(eventId);
            return kdao;
        } catch (Exception e) {
            Rlog.fatal("eventkit", "Exception In getEventStorageKits in EventKitAgentBean " + e);
        }
        return null;
    }
    
    /**
     * Delete Event Storage Kits
     * 
     * @param eventId
     * 
     */
    
    public void deleteEventKit(int eventId){
    	try {
            EventKitDao eventkitDao = new EventKitDao();
            eventkitDao.deleteEventKit(eventId);
        } catch (Exception e) {
            Rlog.fatal("eventkit", "Exception In deleteEventKit in EventKitAgentBean " + e);
        }
    }
    
    /**
     * Sets an EventKit.
     * 
     * @param eventkit
     *              new event kit record
     * @return int
     */

    public int setEventKitDetails(EventKitBean eventkit,String[] storagePks) {
        try {
        	
        	for (int k = 0;k<storagePks.length;k++)
        	{
	        	EventKitBean cd = new EventKitBean();
	        	eventkit.setEventStorageKit(storagePks[k]);
	            cd.updateEventKit(eventkit);
	            //cd.setEventKitId(StringUtil.stringToNum(storagePks[0]));
	            em.merge(cd);
	            
        	}
            
        } catch (Exception e) {
            System.out
                    .print("Exception in setEventKitDetails(EventKitBean eventkit,String[] storagePks) in EventKitAgentBean "
                            + e);
            e.printStackTrace();
            return -2;
        }
        return 0;
    }

    /**
     * deletes an event kit record
     * 
     * @param eventkit PK
     *            
     * @return update success flag
     */
    public int deleteEventKitRecord(int eventKitId) {

    	EventKitBean retrieved = null;

        int output = 0;

        try {

            retrieved = (EventKitBean) em.find(EventKitBean.class, new Integer(
            		eventKitId));

            if (retrieved == null) {
                return -2;
            }
             
            em.remove(retrieved);
        } catch (Exception e) {
            Rlog.debug("eventkit", "Exception in deleteEventKitRecord in EventKitAgentBean"
                    + e);
            output = -2;
        }
       return output;
    }

    //Overloaded for INF-18183 ::: Ankit
    public int deleteEventKitRecord(int eventKitId, Hashtable<String, String> argsvalues) {

    	EventKitBean retrieved = null;

        int output = 0;

        try {

            retrieved = (EventKitBean) em.find(EventKitBean.class, new Integer(
            		eventKitId));

            if (retrieved == null) {
                return -2;
            }
            
            AuditBean audit=null;
            String currdate =DateUtil.getCurrentDate();
            String userID=(String)argsvalues.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
            String ipAdd=(String)argsvalues.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
            String reason=(String)argsvalues.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
            String moduleName = (String)argsvalues.get(AuditUtils.APP_MODULE);
            String getRidValue= AuditUtils.getRidValue("SCH_EVENT_KIT","esch","PK_EVENTKIT="+eventKitId);/*Fetches the RID/PK_VALUE*/ 
            audit = new AuditBean("SCH_EVENT_KIT",String.valueOf(eventKitId),getRidValue,
        			userID,currdate,moduleName,ipAdd,reason);/*POPULATE THE AUDIT BEAN*/ 
        	em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
             
            em.remove(retrieved);
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.debug("eventkit", "Exception in deleteEventKitRecord in EventKitAgentBean"
                    + e);
            output = -2;
        }
       return output;
    }
}// end of class

