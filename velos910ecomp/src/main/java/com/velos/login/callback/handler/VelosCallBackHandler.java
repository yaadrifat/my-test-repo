package com.velos.login.callback.handler;
import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;

import com.velos.login.ConnectObj;
import com.velos.login.callback.VelosCallBack;

public class VelosCallBackHandler implements CallbackHandler {
	private ConnectObj genObj;

	private String name;

	private String password;

	public VelosCallBackHandler(ConnectObj connObj) {
		this.genObj = connObj;

	}

	public VelosCallBackHandler() {
	}

	public void handle(Callback[] callbacks) throws IOException,
			UnsupportedCallbackException {

		for (int i = 0; i < callbacks.length; i++) {
			if (callbacks[i] instanceof NameCallback) {
				((NameCallback) callbacks[i]).setName(name);
			} else if (callbacks[i] instanceof PasswordCallback) {
				((PasswordCallback) callbacks[i]).setPassword(password
						.toCharArray());
			} else if (callbacks[i] instanceof VelosCallBack) {
				((VelosCallBack) callbacks[i]).setConnectObj(genObj);
			}		
			else {
				throw new UnsupportedCallbackException(callbacks[i],
						"Callback class not supported");
			}
		}
	}

	public ConnectObj getConnectObj() {
		return this.genObj;
	}

	public void setConnectObj(ConnectObj connObj) {
		this.genObj = connObj;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}