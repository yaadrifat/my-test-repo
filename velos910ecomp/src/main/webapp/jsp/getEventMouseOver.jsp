<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="eventAssocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="codeLst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="grpRights" scope="request" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<%@page import="com.velos.eres.business.common.*,com.velos.eres.service.util.*"%>
<%@page import="com.velos.esch.business.common.*"%>
<%@page import="com.velos.remoteservice.lab.*,java.net.URLEncoder,com.velos.eres.business.section.*"%>
<%@page import="com.velos.eres.web.user.UserJB,com.velos.eres.web.studyRights.StudyRightsJB" %>
<%@page import="java.util.*,java.io.*"%>
<%@page import="org.json.*,org.w3c.dom.*"%>

  <%
   HttpSession tSession = request.getSession(true);
   if (sessionmaint.isValidSession(tSession))
   {

    int eventId = StringUtil.stringToNum(request.getParameter("eventId"));
    if (eventId == 0) { eventId = -1; }
    
    String source = (String)request.getParameter("source");
    source = (null == source)? "" : source;

    String eventType = (String)request.getParameter("eventType");
    eventType = (null == eventType)? "E" : eventType;
    
    int studyId = 0;
    int finDetRight =0;
    //if ("S".equals(source)){
    	studyId = StringUtil.stringToNum((String)request.getParameter("studyId"));
    	StudyRightsJB stdRights =(StudyRightsJB) tSession.getAttribute("studyRights");
		if (stdRights == null || stdRights.getFtrRights() == null || stdRights.getFtrRights().size() == 0) {
			finDetRight = 0;
		} else {
			finDetRight=Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFIN"));
		}
    //}
	String usr = (String) tSession.getValue("userId");
	int accountId = EJBUtil.stringToNum((String)tSession.getValue("accountId"));

	if ((eventId > 0))
	{
		String mouseover = "";
		if (null != source){
			if ("P".equals(source) || "L".equals(source)){
				if (studyId == 0){
					//Library Event/Calendar
					EventdefDao defDao = new EventdefDao();
					mouseover = defDao.getEventMouseOver(eventId, eventType);
				} else {
					//Library Calendar in a Budget linked to a study
					EventdefDao defDao = new EventdefDao();
					mouseover = defDao.getEventMouseOver(eventId, eventType, finDetRight);
				}
			}else if ("S".equals(source)){
				//Study Setup or Admin Schedule
				EventAssocDao assocDao = new EventAssocDao();
				mouseover = assocDao.getEventMouseOver(eventId, eventType, finDetRight);

			}
		}
		//out.print(""+ mouseover + "");
 		out.print("overlib('"+ mouseover + "',CAPTION,'"+LC.L_Event_Details+"');");
	}
} //end of if session times out
else
{
%>
 <jsp:include page="timeout.html" flush="true"/>
  <%
} //end of else body for page right
%>
