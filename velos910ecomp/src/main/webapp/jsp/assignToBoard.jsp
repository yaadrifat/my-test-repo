<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=MC.M_BoardMember_AssignUsr%><%-- Board >> Members >> Assign Users*****--%></title>

<SCRIPT Language="javascript">
if (document.getElementById('eSign')) { document.getElementById('eSign').disabled=false; } 
if (document.getElementById('submit_btn')) { document.getElementById('submit_btn').disabled=false; }
function  blockSubmit()
{
	 setTimeout('document.getElementById(\'eSign\').disabled=true', 10);
	 setTimeout('document.getElementById(\'submit_btn\').disabled=true', 10);
}
function changeCount(row)
{	  	
	selrow = row ;
	checkedrows = document.users.checkedrows.value;
	totusers = document.users.totalrows.value;
	rows = parseInt(checkedrows);	
	usernum = parseInt(totusers);	
    if (usernum > 1)
	{
       if (document.users.assign[selrow].checked)
       { 
			rows = parseInt(rows) + 1;
       }
	   else
		{
			rows = parseInt(rows) - 1;
		}
	}
	else
	{
		if (document.users.assign.checked)
		{ 
			rows = parseInt(rows) + 1;
		}
	  	else
		{
			rows = parseInt(rows) - 1;
		}
	}
	document.users.checkedrows.value = rows;
}

function validate(formobj)
{
     ls_tot =formobj.lusers.value;
     var chk = false;
     if (ls_tot == 1)	
	 {
    	 if (formobj.assign.checked==false)
         {
       		alert("<%=MC.M_Check_TheUser%> 1");/*alert("Please check the user.");*****/
			return false;
		 }
     } 
     else  
	 {
         for (ll_cnt = 0;ll_cnt < ls_tot;ll_cnt ++)  
		 {
             if (formobj.assign[ll_cnt].checked==true)
			 {	
        		chk = true; 
        		break;			 
			 }
         }
         if(chk == false)
         {
        	alert("<%=MC.M_PlsSel_TheUsr%> 2");/*alert("Please select the user.");*****/
			return false;
         } 
     }
     
     //whether board role has been entered if the checkbox is checked
	 if (ls_tot == 1)
     {
         if (formobj.assign != '' && formobj.assign.value != null)
         {
            if (!(validate_col('board_role',formobj.board_role))) return false;
         } 
     }
	 else
     {
         for (ll_cnt = 0;ll_cnt < ls_tot;ll_cnt ++)
         {
			 if ( formobj.assign[ll_cnt].checked)
             {	
			   if (!(validate_col('board_role',formobj.board_role[ll_cnt]))) return false;
             } 
         }
     }

	 //whether checkbox is checked if board role is selected
     if (ls_tot == 1)
     {
         if (formobj.board_role.value != '' && formobj.board_role.value != null)
         {
            if (formobj.assign.checked==false){
            	alert("<%=MC.M_Check_TheUser%>");/*alert("Please check the user.");*****/
				return false;
			}
         } 
     }
     else
     {
         for (ll_cnt = 0;ll_cnt < ls_tot;ll_cnt ++)
         {
	         if (formobj.board_role[ll_cnt].value != '' && formobj.board_role[ll_cnt].value != null)
             {
	             if (formobj.assign[ll_cnt].checked==false)
		         {
			         alert("<%=MC.M_PlsSel_TheUsr%>");/*alert("Please select the user.");*****/
			         return false;
				 }
             } 
         }
     }

	if (!(validate_col('e-Signature',formobj.eSign))) return false
	if(document.getElementById("eSignMessage").innerHTML != "<%=LC.L_Valid_Esign%>")
	{
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
	}	 
}
</SCRIPT>

<% String src = request.getParameter("srcmenu"); %>

<jsp:include page="panel.jsp" flush="true"> 
	<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
   
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.business.common.*" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<body>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="reviewB" scope="session" class="com.velos.eres.web.reviewBoard.ReviewBoardJB" />
<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.*"%>

<%  
    UserDao userDao=new UserDao(); 
    HttpSession tSession = request.getSession(true);
    if(sessionmaint.isValidSession(tSession))
    {
		String ufname= request.getParameter("fname");
		String ulname=request.getParameter("lname");
		String tab= request.getParameter("selectedTab");
		int rbId;
		Integer codeId;
		String role;
		CodeDao cdRole = new CodeDao();   
		cdRole.getCodeValues("board_role");
		ArrayList cDesc= cdRole.getCDesc();
		ArrayList cId= cdRole.getCId();
		role = cdRole.toPullDown("board_role");        
	
		String uName = (String) tSession.getValue("userName");
		int pageRight = 7; //hardcoded 
		String accId = (String) tSession.getValue("accountId");
		
		if (pageRight > 0 )
		{
			rbId=EJBUtil.stringToNum(request.getParameter("rbId"));				
            userDao.getAvailableTeamBoard(EJBUtil.stringToNum(accId),rbId,ulname.trim(),ufname.trim());
			reviewB.setReviewBoardId(rbId);	
			reviewB.getReviewBoardNames(reviewB.getReviewBoardId());	
			String rbName=reviewB.getReviewBoardName();
			
			ArrayList usrLastNames;
			ArrayList usrFirstNames;
			ArrayList usrMidNames;
			ArrayList usrIds;    
			ArrayList usrStats;	 
 				
			String usrLastName = null;
			String usrFirstName = null;
			String usrMidName = null;
			String usrId=null;		
			String usrStat = null;
  %>
<BR/>

<table>
	<%
	Object[] arguments = {rbName};
	%>
	<td class= "grayComments lhsFont">
		<%=VelosResourceBundle.getMessageString("M_Working_BoardName",arguments)%>
	</td>
</table>
  
<table width="100%" cellspacing="0" cellpadding="0" border="0">
	<Input type="hidden" name="checkedrows" value=0>
	<Input type="hidden" name= "src" value= <%=src%> >					
	<Input type="hidden" name="selectedTab" value="<%=tab%>">
	<Input type="hidden" name="right" value="<%=pageRight%>">
	<Input type="hidden" name="rbId" value="<%=rbId%>">
	
	<tr>
		<td colspan="4">
		<jsp:include page="irbConfigTabs.jsp" flush="true">
			<jsp:param name="selectedTab" value="<%=tab%>" />
		</jsp:include>
		</td>
	</tr>
</table>
<DIV class="BrowserBotN BrowserBotN_S_1" id="div1">

<Form name="users" id="usertoboard" method="post" action="updateboardmember.jsp?srcmenu=tdmenubaritem6&selectedTab=<%=tab%>&rbId=<%=rbId%>" onSubmit = "if (validate(document.users)== false) { setValidateFlag('false'); return false; } else { blockSubmit(); setValidateFlag('true'); return true;}">
<table width="98%" cellspacing="0" cellpadding="0" border=0>
  <tr> 
	<td> 
	  <P class = "sectionHeadings"> <%=MC.M_AssignMember_ToBoard%><%--Assign Members To Boards*****--%></P>
	</td>
  </tr>
</table>
	<BR>
	<table width="98%" border=0>
			<tr> 
				<th width="25%"> <%=LC.L_First_Name%><%--First Name*****--%> </th>
				<th width="25%"> <%=LC.L_Last_Name%><%--Last Name*****--%> </th>
				<th width="15%"> <%=LC.L_User_Type%><%--User Type*****--%> </th>
				<th width="15%"><%=LC.L_Select%><%--Select*****--%> </th>
				<th width="20%"><%=LC.L_Role%><%--Role*****--%></th>
			</tr>
		<%
			usrLastNames = userDao.getUsrLastNames();
			usrFirstNames = userDao.getUsrFirstNames();
			usrMidNames = userDao.getUsrMidNames();
			usrIds = userDao.getUsrIds();   							
			usrStats = userDao.getUsrStats();			
			int i;
			int lenUsers = usrLastNames.size();
		%>
				<tr id ="browserBoldRow"><%Object[] arguments1 = {lenUsers}; %> 
					<td colspan="6"> <%=MC.M_Tot_NumOfUsers%><%--Total Number of Users*****--%> : <%=VelosResourceBundle.getLabelString("L_Usr_S",arguments1)%><%-- <%= lenUsers%> User(s)*****--%> </td>
				</tr><Input type="hidden" name = "lusers" value= <%=lenUsers%> >
		<%
			for(i = 0 ; i < lenUsers ; i++)
			{
				usrLastName=((usrLastNames.get(i)) == null)?"-":(usrLastNames.get(i)).toString();
				usrFirstName=((usrFirstNames.get(i))==null)?"-":(usrFirstNames.get(i)).toString();
				usrMidName=((usrMidNames.get(i))==null)?"-":(usrMidNames.get(i)).toString();
				usrId = ((Integer)usrIds.get(i)).toString();				
				usrStat=((usrStats.get(i)) == null)?"-":(usrStats.get(i)).toString();				
				
				if (usrStat.equals("A"))
				{
					usrStat=LC.L_Active_AccUser/*"Active Account User"*****/;
				}
				else if (usrStat.equals("B")){
					usrStat=LC.L_Blocked_User/*"Blocked User"*****/;
				}
				else if (usrStat.equals("D")){
					usrStat=LC.L_Deactivated_User/*"Deactivated User"*****/;
				}
				
				if ((i%2)==0)
				{ 
					%> <tr class="browserEvenRow"> <%
				}
				else
				{
					%> <tr class="browserOddRow"> <%
				}
				
			%> 	
					<td width =250>  <%=usrFirstName%> <input type="hidden" name="userId" value="<%=usrId%>"> </td>	
						<input type="hidden" name="usrFirstName" value="<%=usrFirstName%>"> 
						<input type="hidden" name="usrLastName" value="<%=usrLastName%>"> 
						<input type="hidden" name="usrStat" value="<%=usrStat%>"> 
					<td width =250><%=usrLastName%></td>												
					<td width =250><%=usrStat%></td>
					<td width =50> 
						<Input type="checkbox" name="assign" value = "<%=usrId%>" onclick="changeCount(<%=i%>);">
					</td>
					<td><%=role%></td>
				</tr>
			<%	}	%> 
			<Input type="hidden" name="totalrows" value=<%=lenUsers%>  >	</table>
			<% 
				if(pageRight > 4)
				{
					String showSubmit = "";
					if(!(lenUsers > 0)) 
					{
						showSubmit = "N";
					}
			%>
				<jsp:include page="submitBar.jsp" flush="true"> 
						<jsp:param name="displayESign" value="Y"/>
						<jsp:param name="formID" value="usertoboard"/>
						<jsp:param name="showDiscard" value="N"/>
						<jsp:param name="showSubmit" value="<%=showSubmit%>"/>
				</jsp:include>
			<% } %>
		 </Form>
		<%
			} //end of if body for page right
			else
			{
		%> <jsp:include page="accessdenied.jsp" flush="true"/> <%
	} //end of else body for page right
}//end of if body for session
else
{
%> <jsp:include page="timeout.html" flush="true"/> <%
}
%>
  <div class = "myHomebottomPanel"> 
  	<jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id = "emenu"> 
  	<jsp:include page="getmenu.jsp" flush="true"/>
</div>
</body></html>
