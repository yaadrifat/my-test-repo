<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<script>
function openwinUser() {
    windowName = window.open("usersearchdetails.jsp?fname=&lname=&from=patient","User","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
	windowName.focus();
}

 function setFilterText(form)
 	{
 	  form.ptxt.value=form.patCode.value;
 	  form.atxt.value=form.patage[form.patage.selectedIndex].text ;
 	  if (form.patgender[form.patgender.selectedIndex].value != "")
          form.gtxt.value=form.patgender[form.patgender.selectedIndex].text ;
 	  if (form.patstatus[form.patstatus.selectedIndex].value != "")
 	   form.rtxt.value=form.patstatus[form.patstatus.selectedIndex].text ;
 	   form.rbytxt.value=form.patregby.value ;
  	   form.pattxt.value=form.patName.value ;
	   form.patregbyname.value = form.patregbyname.value ;


	 }

function setOrder(formObj,orderBy) //orderBy column number
{
	var lorderType;
	var page ;

	if (formObj.orderType.value=="asc") {
		formObj.orderType.value= "desc";
		lorderType="desc";
	} else 	if (formObj.orderType.value=="desc") {
		formObj.orderType.value= "asc";
		lorderType="asc";
	}

	formObj.orderBy.value=orderBy;
	//formObj.orderBy.value="lower("+orderBy+")";
	page=formObj.pageHidden.value
	formObj.action="enrollpatientsearch.jsp?page=" + page;
	formObj.submit();
}


//Added by Gopu for fix the bugzilla Issue #2630 and May-June 2006 Enhancement (#P1)
function openOrgpopupView(patId,viewCompRight) {

	var viewCRight ;
	var rtStr;

	viewCRight = parseInt(viewCompRight);

	if (viewCRight <=0)
	{
		rtStr = "nx";
	}
	else
	{

		rtStr = "dx";
	}

    windowName = window.open("patOrgview.jsp?patId="+patId+"&c="+rtStr,"patient","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=300,height=150,left=400,top=400");
	windowName.focus();
}







</script>
<title><%=LC.L_Search_Pats%><%--Search <%=LC.Pat_Patients%>*****--%></title>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*,java.text.*" %>
<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.person.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.user.*,com.velos.eres.web.studyRights.StudyRightsJB"%>



<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="codelst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="usrSite" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" />
<jsp:useBean id ="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id="commonB" scope="request" class="com.velos.eres.web.common.CommonJB"/>
<jsp:useBean id="studyStatB" scope="page" class="com.velos.eres.web.studyStatus.StudyStatusJB" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<% String src;
src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<body style="overflow:scroll">
<br>
<DIV class="browserDefault" id="div1">
<!-- Modified By Parminder singh Bug #10397 -->
<script language="javascript">
 var ua = navigator.userAgent;
    var MSIEOffset = navigator.userAgent.indexOf("MSIE ");
    if (MSIEOffset == -1) {
document.getElementById("div1").style.height= 790;
}
	else
	{   
		document.getElementById("div1").style.height= 750;
	}
</script>
<%

  HttpSession tSession = request.getSession(true);
  if (sessionmaint.isValidSession(tSession))
 {
	  //String JFlag=request.getParameter("JFlag");//JM
	  	String JFlag = "";

		String studyId = "";
		String from ="" , selectedTab="";


		studyId = request.getParameter("studyId");



		if (EJBUtil.stringToNum(studyId) > 0)
		{
			String studyPermClosureDate="",studyCentricStr="";
			studyPermClosureDate = studyStatB.getMinPermanentClosureDate(EJBUtil.stringToNum(studyId) );

			if (! StringUtil.isEmpty(studyPermClosureDate)) //i.e. a permanent closure status exists
			{
				JFlag="1";
			}

		}

		//keep another string for Jflag to manipulate locally

	  String jFlagCpy=JFlag;
	  jFlagCpy=(jFlagCpy==null)?"":jFlagCpy;


	studyB.setId(EJBUtil.stringToNum(studyId));
	studyB.getStudyDetails();
	String studyNum = studyB.getStudyNumber();

	from = request.getParameter("from");
	if (from==null) from="" ;
	if (from.equals("studypatients")) {
		selectedTab=request.getParameter("selectedTab");
	}
	String userIdFromSession = (String) tSession.getValue("userId");


	TeamDao teamDao = new TeamDao();
	teamDao.getTeamRights(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(userIdFromSession));



	ArrayList tId = teamDao.getTeamIds();
	stdRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));

	 	ArrayList teamRights ;
					teamRights  = new ArrayList();
					teamRights = teamDao.getTeamRights();

					stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
					stdRights.loadStudyRights();


	tSession.putValue("studyRights",stdRights);
	int patdetright = 0;
	int pageRight = 0;

	/////////////////////////
	Calendar cal = new GregorianCalendar();
	String active_status="" ;
	String dGender="" ;
	String genderVal="" ;
	String pstat="" ;
	String age ="" ;
	String patName = "";
 	String patCode = "";
 	String patregby ="", ptxt="All",atxt="All",gtxt="All",rtxt="All",rbytxt="All",pattxt="All";;
	String patregbyname = "";
	String deathdt = "";

	patCode = request.getParameter("patCode");

	if (patCode==null)	 patCode="";

	genderVal = request.getParameter("patgender");
	if (genderVal==null)
	{
	    genderVal = "";
	}

	pstat= request.getParameter("patstatus");
	if (pstat==null) 	pstat="" ;
		patregby =  request.getParameter("patregby");
	if (patregby==null) 	patregby="" ;
	 age=request.getParameter("patage");

	if (EJBUtil.isEmpty(age)) age = "All" ;

	if (( request.getParameter("ptxt"))!= null &&  (request.getParameter("ptxt").length() > 0)  )		    ptxt= request.getParameter("ptxt");
   	if (( request.getParameter("atxt"))!= null &&  (request.getParameter("atxt").length() > 0)  )		    atxt= request.getParameter("atxt");
   	if (( request.getParameter("gtxt"))!= null &&  (request.getParameter("gtxt").length() > 0)  )		    gtxt= request.getParameter("gtxt");
   	if (( request.getParameter("rtxt"))!= null &&  (request.getParameter("rtxt").length() > 0)  )		    rtxt= request.getParameter("rtxt");
   	if (( request.getParameter("rbytxt"))!= null &&  (request.getParameter("rbytxt").length() > 0)  )		rbytxt= request.getParameter("rbytxt");

	patName = request.getParameter("patName");
        if ( patName == null)
	 {
	 	patName = "";
		pattxt ="-" ;
	}
	else
	{
		pattxt =   patName   ;
	}

	 CodeDao cd1 = new CodeDao();
	 CodeDao cd2 = new CodeDao();

	 boolean withSelect = true;

	String dStatus="";
	cd1.getCodeValues("gender");

	if (genderVal.equals(""))
	   dGender=cd1.toPullDown("patgender");
	else
  	  dGender=cd1.toPullDown("patgender",EJBUtil.stringToNum(genderVal),withSelect);

	  cd2.getCodeValues("patient_status");

	if (pstat.equals(""))
	    dStatus = cd2.toPullDown("patstatus");
	else
  	    dStatus = cd2.toPullDown("patstatus",EJBUtil.stringToNum(pstat),withSelect);

//Patient Age
	String dAge="<select size='1' name='patage'>";

	if (age.equals(""))
     dAge = dAge + " <option selected value='All' SELECTED>"+LC.L_All/* All*****/+"</option>" ;
	 else
	 dAge = dAge + " <option selected value='All'>"+LC.L_All/* All*****/+"</option>" ;
	if (age.equals("0,15"))
	   dAge = dAge +  "<option value='0,15' SELECTED>"+LC.L_0_15/* 0-15*****/+"</option>";
	else
   	   dAge = dAge +  "<option value='0,15'>"+LC.L_0_15/* 0-15*****/+"</option>";
	if (age.equals("16,30"))
	   dAge = dAge +  " <option value='16,30' SELECTED>"+LC.L_16_30/* 16-30*****/+"</option>";
	 else
      dAge = dAge +  " <option value='16,30'>"+LC.L_16_30/* 16-30*****/+"</option>";

	if (age.equals("31,45"))
		dAge = dAge +   "<option value='31,45' SELECTED>"+LC.L_31_45/* 31-45*****/+"</option>";
	else
		dAge = dAge +   "<option value='31,45'>"+LC.L_31_45/* 31-45*****/+"</option>";

	if (age.equals("46,60"))
		dAge = dAge + " <option value='46,60' SELECTED>"+LC.L_46_60/* 46-60*****/+"</option>";
		else
		dAge = dAge + " <option value='46,60'>"+LC.L_46_60/* 46-60*****/+"</option>";
	if (age.equals("61,75"))
		dAge = dAge + "<option value='61,75' SELECTED>"+LC.L_61_75/* 61-75*****/+"</option>";
	else
		dAge = dAge + "<option value='61,75'>"+LC.L_61_75/* 61-75*****/+"</option>";

	if (age.equals("76,Over"))
		//Modified by Manimaran to fix the Bug2597
		dAge = dAge +  " <option value='76,Over' SELECTED>"+LC.L_76_Over/* 76 &amp; Over*****/+" </option> </select>" ;
	else
		dAge = dAge +  " <option value='76,Over'>"+LC.L_76_Over/* 76 &amp; Over*****/+" </option> </select>" ;
 //end Patient age
	if ((stdRights.getFtrRights().size()) == 0){
	 	pageRight= 0;
		patdetright = 0;
	}else{
		pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYMPAT"));
		patdetright = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYVPDET"));

		}

	tSession.putValue("studyManagePat",new Integer(pageRight));
	tSession.putValue("studyViewPatDet",new Integer(patdetright));


	UserSiteDao usd = new UserSiteDao ();
	ArrayList arSiteid = new ArrayList();
	ArrayList arSitedesc = new ArrayList();
	String ddPatSite = "";


	if (pageRight == 5  || pageRight == 7)

	   {

	   String uName = (String) tSession.getValue("userName");
	   UserJB user = (UserJB) tSession.getValue("currentUser");
	   String siteId = user.getUserSiteId();

	   String accountId = user.getUserAccountId();
	   String selSite = null;
	   selSite =  request.getParameter("dPatSite");





	   if(selSite == null)
	   	   selSite= "";




	if (!(selSite == null) && (!(selSite.trim()).equals("")))
		{
			 siteId = selSite;
		}


//	Check if the Organization study combination is open for enrolling
	StudyStatusDao statDao=null;
	int iStudyId=EJBUtil.stringToNum(studyId);
	int index=-1,stopEnrollCount=0;
	String studyAccrualFlag="";
	ArrayList statList=null;
	if (studyId.length()>0)
	{
	 SettingsDao settingsDao=commonB.retrieveSettings(iStudyId,3);
	 ArrayList settingsValue=settingsDao.getSettingValue();
	 ArrayList settingsKeyword=settingsDao.getSettingKeyword();
	 index=settingsKeyword.indexOf("STUDY_ACCRUAL_FLAG");
		if (index>=0)
		{
		 studyAccrualFlag=(String)settingsValue.get(index);
		 studyAccrualFlag=(studyAccrualFlag==null)?"":studyAccrualFlag;
		}
		else
		{
			studyAccrualFlag="D";
		}

	  if ( (studyAccrualFlag.equals("D")))  {
//		Disable this for now, with Default settings everything will work as it was in ver 6.2
	  //stopEnrollCount=studyStatB.getCountByOrgStudyStat(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(selSite),"'active_cls'","A");
	  stopEnrollCount=0;
	  }else if (studyAccrualFlag.equals("O"))
	  {

		 statDao=studyStatB.getDAOByOrgStudyStat(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(selSite),"'active_cls','active','prmnt_cls'","A");
		 statList=statDao.getStudyStatList();
		 if (statList.size()==0) stopEnrollCount=0;

		 if (statList.indexOf("active_cls")>=0)
			 stopEnrollCount=1;

		 if (statList.indexOf("active")<0)
			 stopEnrollCount=1;

		 if (statList.indexOf("prmnt_cls")>0)
			 stopEnrollCount=1;

 		// This is done to allow users to come to this page even if selected organizxation has blocked status
 		if  ((jFlagCpy.length()>0) ) stopEnrollCount=0;
	  }
	}

	//End check for Organization study combination is open for enrolling

		 //usd = usrSite.getSitesWithViewRight(EJBUtil.stringToNum(accountId),EJBUtil.stringToNum(userIdFromSession));
		//new req

	     usd = usrSite.getSitesToEnrollPat(EJBUtil.stringToNum(studyId), EJBUtil.stringToNum(accountId),EJBUtil.stringToNum(userIdFromSession));
		 arSiteid = usd.getUserSiteSiteIds();

		 arSitedesc = usd.getUserSiteNames();

		    if(selSite.equals("0") && arSiteid.size() > 0)
			 {
			   selSite = arSiteid.get(0).toString();
			 }


		 int chkCnt = 0;
		 for(int chk =0 ;chk < arSiteid.size();chk++){
		 if(selSite.equals((String)arSiteid.get(chk))){
		    siteId = selSite;
			chkCnt =1;
			}

		 }


		 	if(chkCnt == 0){

			siteId= "0";

			}


		 ddPatSite = EJBUtil.createPullDownNoSelect("dPatSite",EJBUtil.stringToNum(siteId),arSiteid,arSitedesc);


		 String  studyIdStr = "0";
		 int idStudy = 0 ;
		 studyIdStr = request.getParameter("dStudyExists");
		 if ( ! EJBUtil.isEmpty(studyIdStr))
		 {
		   idStudy = EJBUtil.stringToNum(studyIdStr);
	 	 }else
		 {
		 	studyIdStr = "0";
		 }

		 String ddstudyExists ="";
		 StudyDao studyDao = studyB.getUserStudies(userIdFromSession, "dStudyExists  STYLE='WIDTH:177px' ",idStudy,"active");
		 ddstudyExists = studyDao.getStudyDropDown();

		  String searchFrom = request.getParameter("searchFrom");


		 if(searchFrom.equals("search")){
			patCode = request.getParameter("patCode");
		 }

		int patDataDetail = 0;
		int personPK = 0;
		int usrGroup = 0;
		userB.setUserId(EJBUtil.stringToNum(userIdFromSession));
		userB.getUserDetails();
		usrGroup =EJBUtil.stringToNum( userB.getUserGrpDefault());
		personPK = EJBUtil.stringToNum(request.getParameter("pkey"));
		patDataDetail = person.getPatientCompleteDetailsAccessRight( EJBUtil.stringToNum(userIdFromSession),usrGroup,personPK );

		if (! StringUtil.isEmpty(patregby))
		{

			UserJB userReg = new UserJB();
			userReg.setUserId(EJBUtil.stringToNum(patregby));
			userReg.getUserDetails();
			patregbyname = userReg.getUserFirstName() + " " + userReg.getUserLastName();

		}


%>


<% if (EJBUtil.stringToNum(JFlag)==1){%>

  <p class="defComments" align=center>
  <%=MC.M_StdClosed_NoEnrl%><%-- This <%=LC.Std_Study%> is now closed , no further enrollments allowed.*****--%>
  </p>
<p align=center><button onclick="window.history.back();"><%=LC.L_Back%></button></p>

<%}else if (stopEnrollCount>0) {%>
	<br><br><br><br><br>
	  <p class="defComments" align=center>
	  <%=MC.M_StdNotHave_StatEnrlPat%><%-- This <%=LC.Std_Study%> does not have an appropriate status to enroll a <%=LC.Pat_Patient_Lower%>.*****--%>
	  </p>
	<p align=center><button onclick="window.history.back();"><%=LC.L_Back%></button></p>
<%}else{%>

 <P class = "userName"> <%= uName %> </P>
 <P class="sectionHeadings"><%=MC.M_MngPat_EnrlPat%><%-- Manage <%=LC.Pat_Patients%> >> Enroll <%=LC.Pat_Patient%>*****--%></P>
 <!--P class="defComments">Please specify the Search criteria and then click on 'Search' to
  view Patient List</P-->




<Form  name="patient" method="post" action="enrollpatientsearch.jsp" onSubmit="setFilterText(document.patient)">

 <%
 //Pagination
	String pagenum = "";
	int curPage = 0;
	long startPage = 1;
	long cntr = 0;
	long rowsPerPage=0;
	long totalPages=0;
	long rowsReturned = 0;
	long showPages = 0;
	long totalRows = 0;
	long firstRec = 0;
	long lastRec = 0;
	boolean hasMore = false;
	boolean hasPrevious = false;

 	pagenum = request.getParameter("page");
	if (pagenum == null)
	{
		pagenum = "1";
	}
	curPage = EJBUtil.stringToNum(pagenum);
	String orderBy = "";
	orderBy = request.getParameter("orderBy");

	if (EJBUtil.isEmpty(orderBy))
		orderBy = "lower(PERSON_LNAME)"; //patient_code

	String orderType = "";
	orderType = request.getParameter("orderType");
	if (EJBUtil.isEmpty(orderType))
	{
		orderType = "asc";
	}
	rowsPerPage =  Configuration.ROWSPERBROWSERPAGE ;
	totalPages =Configuration.PAGEPERBROWSER ;

	 if (from.equals("studypatients")) {%>
  		<input type="hidden" name="from"  value=<%=from%>>
		<input type="hidden" name="selectedTab"  value=<%=selectedTab%>>
		<jsp:include page="mgpatienttabs.jsp" flush="true"/>
	 <br>

       <%}
  	    %>

  	    <P class="sectionHeadingsFrm"><%=MC.M_EnrlPat_Std%><%-- Enroll <%=LC.Pat_Patients%> to <%=LC.Std_Study%>*****--%>: <%=studyNum%> </P>

<input type="hidden" name="srcmenu" Value="<%=src%>">
<input type="hidden" name="searchFrom" Value="search">
<input type="hidden" name="studyId" Value=<%=studyId%>>
<input type="hidden" name="orderType" value=<%=orderType%>>
<input type="hidden" name="orderBy" value=<%=orderBy%> >
<input type="hidden" name="pageHidden" value=<%=pagenum%> >


  <table width="100%" cellspacing="0" cellpadding="0" border="0" class="basetbl outline">
  <tr><td colspan="6"></td></tr>
    <tr>
	<td > <%=LC.L_Patient_Id%><%-- <%=LC.Pat_Patient%> ID*****--%> </td>
	<td><Input type=text name="patCode"  size="15" value = "<%=patCode%>"></td>
	<td ><%=LC.L_Age %><%-- Age*****--%></td>
	<td ><%=dAge%></td>
	<td ><%=LC.L_Organization%><%-- Organization*****--%></td>
	<td ><%=ddPatSite%></td>
   </tr>

    <tr>
	<td > <%=LC.L_Pat_Name%><%-- <%=LC.Pat_Patient%> Name*****--%> </td>
	<td > <Input type=text name="patName"  size="15" value = "<%=patName%>"> </td>
	<td ><%=LC.L_Gender%><%-- Gender*****--%></td>
	<td ><%=dGender%></td>
	<td ><%=LC.L_Provider%><%-- Provider*****--%></td>
    <td ><Input type=hidden name="patregby" size="15" value = "<%=patregby%>">
	<input type=text name=patregbyname readonly value="<%=patregbyname%>">
          <A HREF=# onClick=openwinUser() ><%=LC.L_Select_User%><%-- Select User*****--%></A>

	</td>
	</tr>

    <tr>
    <!--  commented to fix the bug #2394 -->
	<!-- td class=tdDefault>Registration Status</td-->
	<td ><%=LC.L_Survival_Status%><%-- Survival Status*****--%></td>
	<td ><%=dStatus%></td>
	<td ><%=LC.L_Study%><%--<%=LC.Std_Study%>*****--%></td>
	<td ><%=ddstudyExists%></td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
  	</tr>

  <tr>
  	<td>&nbsp;</td>
	<td>&nbsp;</td>
		<td>&nbsp;</td>
	<td>&nbsp;</td>
  <td >
 	&nbsp;
  <%
   if (from.equals("studypatients")){%>
<button onclick="window.history.back(); return false;"><%=LC.L_Back%></button>
<%}%>
	</td>
	<td >
	 <button type="submit"><%=LC.L_Search%></button>
	</td>
</tr>
</table>

<input type="hidden" name="selectedTab" Value=<%=selectedTab%>>
<input type="hidden" name="ptxt">
<input type="hidden" name="atxt">
<input type="hidden" name="gtxt">
<input type="hidden" name="rtxt">
<input type="hidden" name="rbytxt">
<input type="hidden" name="pattxt">
</Form>

<DIV>
<hr color="gray" id="div3">

  <Form name="results" method="post" onSubmit="setFilterText(document.results)">
  <%
 if(searchFrom.equals("initial"))
 {
%>

 <P class="defComments" align="center"><%=MC.M_PlsSrchCrit_ViewPatList%><%-- Please specify the Search criteria and then click on 'Search' to
  view <%=LC.Pat_Patient%> List*****--%></P>



<%
 }
 else
 {
%>
	 <P class="defComments">
 	    <%=MC.M_SelFilters_PatId%><%-- The Selected Filters are: <%=LC.Pat_Patient%> ID*****--%>:&nbsp;<I><u><%=ptxt%></u></I>
 	    &nbsp;&nbsp;<%=LC.L_Age %><%-- Age*****--%>:&nbsp;<I><u><%=atxt%></u></I>
				&nbsp;&nbsp;<%=LC.L_Organization%><%-- Organization*****--%> :&nbsp;<I><u><script><%if(chkCnt >0 ){%>document.write(document.patient.dPatSite.options[document.patient.dPatSite.selectedIndex].text);<%}%></script></u></I>
	    &nbsp;&nbsp;<%=LC.L_Pat_Name%><%--<%=LC.Pat_Patient%> Name*****--%>:&nbsp;<I><u><%=pattxt%></u></I>
	

	<%
	     if(patregbyname.equals("")){
	       patregbyname = "All";
	     }

	%>
	
  	    <%=LC.L_Gender%><%-- Gender*****--%>:&nbsp;<I><u><%=gtxt%></u></I>&nbsp;&nbsp;
  	    <%=LC.L_Provider%><%-- Provider*****--%>:&nbsp;<I><u><%=patregbyname%></u></I>&nbsp;&nbsp;
  	    <%=LC.L_Survival_Status%><%-- Survival Status*****--%>:&nbsp;<I><u><%=rtxt%></u></I>&nbsp;&nbsp;
  	    <%=LC.L_Study%><%-- <%=LC.Std_Study%>*****--%>:&nbsp;<I><u><script>document.write(document.patient.dStudyExists.options[document.patient.dStudyExists.selectedIndex].text);</script></u></I>
	</P>
<%}%>
<%
	PatientDao pdao = new PatientDao();
	StringBuffer sbSelectSQL = new StringBuffer();
	StringBuffer sbFromSQL = new StringBuffer();
	StringBuffer sbWhereSQL = new StringBuffer();
	String finalSQL = "";
	String countSQL = "";
	int lowLimit=0,highLimit=0 ;
	String ageFilter ="" ;

	Date dt1 = new java.util.Date();
	Date dt2= new java.util.Date();
	Format formatter;
        formatter = new SimpleDateFormat("yyyy/MM/dd");

	String minDate="";
	String maxDate="";

	//Prepare SQL
	//modified by sonia abrol, 07/18/06 - removed function access for pat_facilities
	//removed extra patfacility join and optimized sql

	 sbSelectSQL.append("SELECT DISTINCT  A.PK_PERSON PK_PERSON, lower(PERSON_CODE) person_code_lower , PERSON_CODE, PERSON_DOB, (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = FK_CODELST_GENDER) AS GENDER ,");
	 sbSelectSQL.append(" A.FK_ACCOUNT, (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = FK_CODELST_PSTAT) PSTAT, PERSON_REGBY, PERSON_FNAME ,PERSON_LNAME, PERSON_MNAME, to_char( person_deathdt ,PKG_DATEUTIL.F_GET_DATEFORMAT) person_deathdt ");
	 sbFromSQL.append(" from person A, ( SELECT fk_per from er_patfacility fac WHERE fac.FK_SITE = "+siteId);
	 sbFromSQL.append("   and fac.patfacility_accessright > 0 and not exists (SELECT * FROM ER_PATPROT pp WHERE fk_study = "+ studyId+" and pp.fk_per = fac.fk_per)) B ");
	 sbWhereSQL.append(" where A.PK_PERSON = B.FK_PER ");



	//patient code

	if (!EJBUtil.isEmpty(patCode))
	{
		sbWhereSQL.append(" and (UPPER(PERSON_CODE) LIKE UPPER('%" + patCode.trim() + "%') or exists (select * from er_patfacility f where " +
              " lower(pat_facilityid) like lower('%"+patCode.trim()+"%') and  f.fk_site = " + siteId + " and f.patfacility_accessright > 0 " +
            " AND f.fk_per = A.PK_PERSON ) )");
	}
	//patient status

	if (!EJBUtil.isEmpty(pstat))
	{
		sbWhereSQL.append(" and ( fk_codelst_pstat =  " + EJBUtil.stringToNum(pstat) +" )");
	}
 	//patient gender

	if (!EJBUtil.isEmpty(genderVal))
	{
		sbWhereSQL.append("  and ( fk_codelst_gender  = " + genderVal + " ) ");
	}

	//Query Modified by Manimaran to fix the Bug2597
	//patient age
	// query modified by gopu to fix the bugzilla issues #2518 (Age fillter not working for the Dead patients whose Age is now Calculated differently)
		if (!EJBUtil.isEmpty(age))
		{
			if (age.equals("0,15")) {

			dt1.setYear(dt1.getYear() - 0);
		 	dt2.setYear(dt2.getYear() -16);
		 	maxDate = formatter.format(dt1);
    		 	minDate=formatter.format(dt2);

		 	ageFilter=" and (((to_char(a.person_dob,'YYYY/MM/DD')) > '"+ minDate + "'  and (to_char(a.person_dob,'YYYY/MM/DD')) <='" + maxDate+ "'  AND a.person_deathdt IS NULL) or  trunc(months_between(a.person_deathdt,a.person_dob))/12 between 0 and 15.99)" ;

			  /*lowLimit= cal.get(Calendar.YEAR) - EJBUtil.stringToNum("0");
			  highLimit= cal.get(Calendar.YEAR) - EJBUtil.stringToNum("15");
			  //ageFilter=" and ((SUBSTR(TO_CHAR(person_dob,'YYYYMMDD'),0,4) >= '"+ highLimit + "' ) and (SUBSTR(TO_CHAR(person_dob,'YYYYMMDD'),0,4) <='" + lowLimit+ "'))" ;
			  ageFilter=" and ((((SUBSTR(TO_CHAR(person_dob,'YYYYMMDD'),0,4) >= '"+ highLimit + "' ) and (SUBSTR(TO_CHAR(a.person_dob,'YYYYMMDD'),0,4) <='" + lowLimit+ "')) and a.person_deathdt IS NULL) or ((substr(to_char(a.person_deathdt,'YYYYMMDD'),0,4))- (SUBSTR(TO_CHAR(person_dob,'YYYYMMDD'),0,4)) <=15))" ;
			  */
			 }//end for age.equals("1,15")
			if (age.equals("16,30")) {


			    dt1.setYear(dt1.getYear() - 16);
		 	    dt2.setYear(dt2.getYear() -31);
		 	    maxDate = formatter.format(dt1);
    		 	    minDate=formatter.format(dt2);

		 	    ageFilter=" and (((to_char(a.person_dob,'YYYY/MM/DD')) > '"+ minDate + "'  and (to_char(a.person_dob,'YYYY/MM/DD')) <='" + maxDate+ "'  AND a.person_deathdt IS NULL)  or trunc(months_between(a.person_deathdt,a.person_dob))/12 between 16 and 30.99)" ;
			  /*lowLimit= cal.get(Calendar.YEAR) - EJBUtil.stringToNum("16");
			  highLimit= cal.get(Calendar.YEAR) - EJBUtil.stringToNum("30");
			  //ageFilter=" and ((SUBSTR(TO_CHAR(person_dob,'YYYYMMDD'),0,4) >= '"+ highLimit + "' ) and (SUBSTR(TO_CHAR(person_dob,'YYYYMMDD'),0,4) <='" + lowLimit+ "'))" ;
			  ageFilter=" and ((((SUBSTR(TO_CHAR(person_dob,'YYYYMMDD'),0,4) >= '"+ highLimit + "' ) and (SUBSTR(TO_CHAR(a.person_dob,'YYYYMMDD'),0,4) <='" + lowLimit+ "')) and a.person_deathdt IS NULL) or ((substr(to_char(a.person_deathdt,'YYYYMMDD'),0,4))- (SUBSTR(TO_CHAR(person_dob,'YYYYMMDD'),0,4)) between 16 and 30))" ;
			  */
			 }//end for age.equals("16,30")

			 if (age.equals("31,45")) {

			dt1.setYear(dt1.getYear() -31);
		 	dt2.setYear(dt2.getYear() -46);
		 	maxDate = formatter.format(dt1);
    		 	minDate=formatter.format(dt2);

		 	ageFilter=" and (((to_char(a.person_dob,'YYYY/MM/DD')) > '"+ minDate + "'  and (to_char(a.person_dob,'YYYY/MM/DD')) <='" + maxDate+ "'  AND a.person_deathdt IS NULL) or   trunc(months_between(a.person_deathdt,a.person_dob))/12 between 31 and 45.99)" ;

			  /*lowLimit= cal.get(Calendar.YEAR) - EJBUtil.stringToNum("31");
			  highLimit= cal.get(Calendar.YEAR) - EJBUtil.stringToNum("45");
			  //ageFilter=" and ((SUBSTR(TO_CHAR(person_dob,'YYYYMMDD'),0,4) >= '"+ highLimit + "' ) and (SUBSTR(TO_CHAR(person_dob,'YYYYMMDD'),0,4) <='" + lowLimit+ "'))" ;
			  ageFilter=" and ((((SUBSTR(TO_CHAR(person_dob,'YYYYMMDD'),0,4) >= '"+ highLimit + "' ) and (SUBSTR(TO_CHAR(a.person_dob,'YYYYMMDD'),0,4) <='" + lowLimit+ "')) and a.person_deathdt IS NULL) or ((substr(to_char(a.person_deathdt,'YYYYMMDD'),0,4))- (SUBSTR(TO_CHAR(person_dob,'YYYYMMDD'),0,4)) between 31 and 45))" ;
			  */
			 }//end for age.equals("31,45")
			  if (age.equals("46,60")) {

			dt1.setYear(dt1.getYear() - 46);
		 	dt2.setYear(dt2.getYear() -61);
		 	maxDate = formatter.format(dt1);
    		 	minDate=formatter.format(dt2);

		 	ageFilter=" and (((to_char(a.person_dob,'YYYY/MM/DD')) > '"+ minDate + "'  and (to_char(a.person_dob,'YYYY/MM/DD')) <='" + maxDate+ "'  AND a.person_deathdt IS NULL) or  trunc(months_between(a.person_deathdt,a.person_dob))/12 between 46 and 60.99)" ;
			  /*
			  lowLimit= cal.get(Calendar.YEAR) - EJBUtil.stringToNum("46");
			  highLimit= cal.get(Calendar.YEAR) - EJBUtil.stringToNum("60");
			  //ageFilter=" and ((SUBSTR(TO_CHAR(person_dob,'YYYYMMDD'),0,4) >= '"+ highLimit + "' ) and (SUBSTR(TO_CHAR(person_dob,'YYYYMMDD'),0,4) <='" + lowLimit+ "'))" ;
			  ageFilter=" and ((((SUBSTR(TO_CHAR(person_dob,'YYYYMMDD'),0,4) >= '"+ highLimit + "' ) and (SUBSTR(TO_CHAR(a.person_dob,'YYYYMMDD'),0,4) <='" + lowLimit+ "')) and a.person_deathdt IS NULL) or ((substr(to_char(a.person_deathdt,'YYYYMMDD'),0,4))- (SUBSTR(TO_CHAR(person_dob,'YYYYMMDD'),0,4)) between 46 and 60))" ;*/
			 }//end for age.equals("46,60")
			  if (age.equals("61,75")) {
			      dt1.setYear(dt1.getYear() - 61);
		 	      dt2.setYear(dt2.getYear() -76);
		 	      maxDate = formatter.format(dt1);
    		 	      minDate=formatter.format(dt2);

		 	ageFilter=" and (((to_char(a.person_dob,'YYYY/MM/DD')) > '"+ minDate + "'  and (to_char(a.person_dob,'YYYY/MM/DD')) <='" + maxDate+ "'  AND a.person_deathdt IS NULL) or trunc(months_between(a.person_deathdt,a.person_dob))/12 between 61 and 75.99)" ;
			  /*
			  lowLimit= cal.get(Calendar.YEAR) - EJBUtil.stringToNum("61");
			  highLimit= cal.get(Calendar.YEAR) - EJBUtil.stringToNum("75");
			  //ageFilter=" and ((SUBSTR(TO_CHAR(person_dob,'YYYYMMDD'),0,4) >= '"+ highLimit + "' ) and (SUBSTR(TO_CHAR(person_dob,'YYYYMMDD'),0,4) <='" + lowLimit+ "'))" ;
			  ageFilter=" and ((((SUBSTR(TO_CHAR(person_dob,'YYYYMMDD'),0,4) >= '"+ highLimit + "' ) and (SUBSTR(TO_CHAR(a.person_dob,'YYYYMMDD'),0,4) <='" + lowLimit+ "')) and a.person_deathdt IS NULL) or ((substr(to_char(a.person_deathdt,'YYYYMMDD'),0,4))- (SUBSTR(TO_CHAR(person_dob,'YYYYMMDD'),0,4)) between 61 and 75))" ;*/
			 }//end for age.equals("61,75")
			  if (age.equals("76,Over")) {

			     dt1.setYear(dt1.getYear() - 76);
		 	     maxDate = formatter.format(dt1);

		 	 ageFilter=" and (((to_char(a.person_dob,'YYYY/MM/DD')) <='" + maxDate+ "'  AND a.person_deathdt IS NULL) or   trunc(months_between(a.person_deathdt,a.person_dob))/12 >= 76)" ;
			  /*
			  lowLimit= cal.get(Calendar.YEAR) - EJBUtil.stringToNum("76");
			  //ageFilter=" and (SUBSTR(TO_CHAR(person_dob,'YYYYMMDD'),0,4) <= '"+ lowLimit + "' )" ;
			  ageFilter=" and ((((SUBSTR(TO_CHAR(person_dob,'YYYYMMDD'),0,4) >= '"+ highLimit + "' ) and (SUBSTR(TO_CHAR(a.person_dob,'YYYYMMDD'),0,4) <='" + lowLimit+ "')) and a.person_deathdt IS NULL) or ((substr(to_char(a.person_deathdt,'YYYYMMDD'),0,4))- (SUBSTR(TO_CHAR(person_dob,'YYYYMMDD'),0,4)) >=76))" ;
			  */
			 }//end for age.equals("76,Over")
			sbWhereSQL.append(ageFilter);
		}

		//patient registered by
		if (!EJBUtil.isEmpty(patregby))
		{

			sbWhereSQL.append(" and exists (select * from er_patfacility f where " +
              "  f.fk_site = " + siteId + " and f.patfacility_accessright > 0 " +
            " AND f.fk_per = A.PK_PERSON AND f.patfacility_provider = " + patregby + " )");

		}

		//patient pStudyExists

		if (!EJBUtil.isEmpty(studyIdStr)  && (! studyIdStr.equals("0") ))
		{
			sbWhereSQL.append(" and ( A.PK_PERSON  in (select distinct fk_per from er_patprot where fk_study = " + studyIdStr+ ") )  ");
		}

		if  (!EJBUtil.isEmpty(patName))
		{
		   	String[] names1 = patName.trim().split(" ");
		   	sbWhereSQL.append(" and  (  lower( a.person_fname) like lower('%"+patName.trim()+"%') or ");
		   	if (names1 != null && names1.length > 0) {
		   	    sbWhereSQL.append(" lower(a.person_fname) like lower('%"+names1[0]+"%')  or ");
		   	}
	        if (names1 != null && names1.length > 0) {
	            sbWhereSQL.append(" lower(a.person_lname) like lower('%"+names1[names1.length-1]+"%')  or ");
	        }
	        if (names1 != null && names1.length > 1) {
	            sbWhereSQL.append(" lower(a.person_mname) like lower('%"+names1[1]+"%')  or ");
	        }
	        sbWhereSQL.append(" lower(a.person_lname) like lower('%"+patName.trim()+"%')  or ");
	        sbWhereSQL.append(" lower(a.person_mname) like lower('%"+patName.trim()+"%')     ") ;
	        sbWhereSQL.append(" ) ");
		}

	      //String orderByClause = " Order By PERSON_CODE "	;


	//Got SQL
	finalSQL = sbSelectSQL.toString() + sbFromSQL.toString() + sbWhereSQL.toString(); //+ orderByClause;
	countSQL = "Select count(distinct person_code ) "  + sbFromSQL.toString() + sbWhereSQL.toString() ; //+ orderByClause;

	// Get paginated data

	BrowserRows br = new BrowserRows();

	System.out.println("finalSQL" + finalSQL);
	System.out.println("countSQL" + countSQL);
	//System.out.println("curPage" + curPage);

        br.getPageRows(curPage,rowsPerPage,finalSQL,totalPages,countSQL,orderBy,orderType);


	rowsReturned = br.getRowReturned();
	showPages = br.getShowPages();
        startPage = br.getStartPage();
	hasMore = br.getHasMore();
	hasPrevious = br.getHasPrevious();
	totalRows = br.getTotalRows();
	firstRec = br.getFirstRec();
	lastRec = br.getLastRec();
	//Got paginated data

		//pdao = person.searchPatientsForEnr(siteId, studyId, patCode,age,genderVal,patregby,pstat,patName,studyIdStr);


	if ((totalRows <= 0 || rowsReturned <= 0) && !(searchFrom.equals("initial"))) {

%>

	<P class="defComments"><%=MC.M_NoPatFound_InSrchCrit%><%-- No <%=LC.Pat_Patient_Lower%> found matching your search criteria. Please modify the criteria and click on 'Search'.*****--%></P>

<%

	} else {


	   String patientId = null;
	   String patientCode = null;
	   String patientYob1 = null;
	   String patientGender = null;
	   String patLastName = null;
	   String patFirstName = null;
	   String patientName = null;


	   String patientStatus = null;

	   int patientAge = 0;
   	   String patientAgeStr = "";
		int patientMob=0;
		int patientDob=0;
		int patientYob=0;
		//int patientAge=0;
		int sysYear=0;
		int sysMonth=0;
		int sysDate=0;
		int noOfMonths=0;
		int noOfYears=0;
		int noOfDays=0;




	%>

<%
 if(searchFrom.equals("initial"))
 {

 }
 else
 {
%>

	<P class="defComments"><%=MC.M_ListDispPat_SrchCrit%><%-- The list below displays <%=LC.Pat_Patients_Lower%> that match your Search criteria. Select the <%=LC.Pat_Patient_Lower%> by clicking the corresponding 'Select' link.*****--%></P>

    <table  >

      <tr>
 	  <th width=5% >&nbsp;</th>
      <th width=15% align =center onClick="setOrder(document.patient,2)"><%=LC.L_Patient_Id%><%-- <%=LC.Pat_Patient%> ID*****--%> &loz;</th>
	  <th width=10% align=center onClick="setOrder(document.patient,'lower(PERSON_FNAME)')"><%=LC.L_First_Name%><%-- First Name*****--%> &loz; </td>
	  <th width=10% align=center onClick="setOrder(document.patient,'lower(PERSON_LNAME)')"><%=LC.L_Last_Name%><%-- Last Name*****--%> &loz; </td>
	  <th width=10% align =center onClick="setOrder(document.patient,'PERSON_DOB')"><%=LC.L_Age%><%-- Age*****--%> &loz;</th>
	  <th width=6% align =center onClick="setOrder(document.patient,'GENDER')"><%=LC.L_Gender%><%-- Gender*****--%> &loz;</th>
	  <th width=12% align ="center" onClick="setOrder(document.patient,'PSTAT')"><%=LC.L_Survival_Status%><%-- Survival Status*****--%> &loz;</th>
	  <!--th width=38% align =center >Organization(s)</th-->
	  <th width=10%  align ="center"><%=LC.L_Other_Id_S%><%-- Other ID(s)*****--%> </th>


   </tr>

<%

	for(int i = 1 ; i <= rowsReturned ; i++)
	  {
		patientId = br.getBValues(i,"PK_PERSON") ;
	  	patientCode = br.getBValues(i,"PERSON_CODE");
		patientYob1 = br.getBValues(i,"PERSON_DOB");

		deathdt = br.getBValues(i,"person_deathdt")	 		   ;

		if (!EJBUtil.isEmpty(patientYob1))
		{
			patientMob = EJBUtil.stringToNum(patientYob1.substring(5,7));
			patientDob = EJBUtil.stringToNum(patientYob1.substring(8,10));
			patientYob = EJBUtil.stringToNum(patientYob1.substring(0,4));

			  if (! StringUtil.isEmpty(deathdt)) // if patient is dead, calculate age till his dod
				  {
	  	     	  sysYear = EJBUtil.stringToNum(deathdt.substring(6,10));
				  sysMonth = EJBUtil.stringToNum(deathdt.substring(0,2));
				  sysDate = EJBUtil.stringToNum(deathdt.substring(3,5));

				  }
				  else
				  {
						sysMonth=cal.get(Calendar.MONTH)+1;
						sysDate=cal.get(Calendar.DATE);
						sysYear=cal.get(Calendar.YEAR);

				}


			if (sysYear==patientYob)
	              patientAge=0;
	          if (sysYear > patientYob)
	          {
				  patientAge=sysYear-patientYob;
                  if(patientMob > sysMonth)
		             patientAge--;
                  if(patientMob==sysMonth && patientDob>sysDate)
			         patientAge--;
	          }
    		  if(patientAge!=0)
    			  patientAgeStr = String.valueOf(patientAge)+"&nbsp;"+LC.L_Years_Lower;/*patientAgeStr = String.valueOf(patientAge)+"&nbsp;years";*****/
			  if(patientAge==0)
			  {
				  if(patientMob <= sysMonth && sysDate>=patientDob)
					  noOfMonths=sysMonth-patientMob;
				  else if(patientMob<sysMonth && patientDob>sysDate)
						noOfMonths=(sysMonth-patientMob)-1;
				  else if (patientMob>=sysMonth&&patientDob<=sysDate)
				  {
					  noOfMonths=(patientMob-sysMonth);
					  noOfMonths=12-(noOfMonths);
				  }
				  else if(patientMob>=sysMonth&&patientDob>sysDate)
				  {
					  noOfMonths=patientMob-sysMonth;
					  noOfMonths=11-(noOfMonths);
				  }
				  patientAgeStr=String.valueOf(noOfMonths)+"&nbsp;"+LC.L_Months_Lower;/*patientAgeStr=String.valueOf(noOfMonths)+"&nbsp;months";*****/
			  }
			  if(patientAge==0 && noOfMonths==0)
			  {
				  if(patientDob<=sysDate)
					  noOfDays=(sysDate-patientDob)+1;
				  else
				  {
					  noOfDays=sysDate-patientDob;
					  if (patientMob==1)
						  noOfDays=31-(-noOfDays);
					  if (patientMob==2)
					  {
						  noOfDays=28-(-noOfDays);
						  if(sysYear%4==0 && (sysYear % 100 != 0 || sysYear % 400 == 0))
							  noOfDays=29-(-noOfDays);
					  }
					  if (patientMob==3)
						  noOfDays=31-(-noOfDays);
					  if (patientMob==4)
						  noOfDays=30-(-noOfDays);
					  if (patientMob==5)
						  noOfDays=31-(-noOfDays);
					  if (patientMob==6)
						  noOfDays=30-(-noOfDays);
					  if (patientMob==7)
						  noOfDays=31-(-noOfDays);
					  if (patientMob==8)
						  noOfDays=31-(-noOfDays);
					  if (patientMob==9)
						  noOfDays=30-(-noOfDays);
					  if (patientMob==10)
						  noOfDays=31-(-noOfDays);
					  if (patientMob==11)
						  noOfDays=30-(-noOfDays);
					  if (patientMob==12)
						  noOfDays=31-(-noOfDays);
					   noOfDays=noOfDays+1;
				  }
				  patientAgeStr= String.valueOf(noOfDays)+"&nbsp;"+LC.L_Days_Lower;/*patientAgeStr= String.valueOf(noOfDays)+"&nbsp;days";*****/
			  }
			//patientAge = cal.get(Calendar.YEAR) - EJBUtil.stringToNum(patientYob);
			//patientAgeStr = patientAge + "&nbsp;years" ;
		}
		else
		{
			patientAgeStr = "-";
		}

		patientGender = br.getBValues(i,"GENDER");


 		patientId=(patientId == null)?"-":patientId;
		patientGender = (patientGender == null)?"Not Specified":patientGender;

		patientStatus = br.getBValues(i,"PSTAT");
	        patLastName = br.getBValues(i,"PERSON_LNAME");
		if(patLastName==null)
		{
			patLastName = "";
		}
		patFirstName =  br.getBValues(i,"PERSON_FNAME");
		if(patFirstName ==null)
		{
			patFirstName = "";
		}

		patientName = patLastName + " " + patFirstName;
		if(patientName.equals(" "))
		{
			patientName="-";
		}
	   if ((i%2)==0) {

%>

      <tr class="browserEvenRow">

<%

	 		   }else{

%>

      <tr class="browserOddRow">

<%

	 		   }

%>
<td width =5%> <A href="enrollpatient.jsp?patStudiesEnrollingOrg=<%=siteId%>&mode=N&srcmenu=<%=src%>&patientCode=<%=StringUtil.encodeString(patientCode)%>&patProtId=&pkey=<%=patientId%>&page=patientEnroll&selectedTab=2&studyId=<%=studyId%>"><%=LC.L_Select%><%-- Select*****--%></A> </td>
        <td width =15%><A href = "patientdetails.jsp?page=enrollPatientsearch&from=<%=from%>&srcmenu=<%=src%>&mode=M&selectedTab=1&page=patient&pkey=<%=patientId%>&patCode=<%=patCode%>&studyId=<%=studyId%>"><%=patientCode%></A></td>

		<%if( patDataDetail >= 4 ){%>
		<td width =10%> <%=patFirstName%></td>
		<td width =10%> <%=patLastName%></td>
					<%}else{%>
		<td width =10%>****</td>
		<td width =10%>****</td>
			<%}%>

        <td width =10%> <%=patientAgeStr%></td>


		<td width =10%> <%=patientGender%></td>
		<td width=12%><%=patientStatus%></td>
		<!--Added by Gopu to fix the bugzilla Issues #2603 and May-June 2006 Enhancement (#P1)-->
		<td width="10%" align="center"><A HREF="#" onClick="openOrgpopupView('<%=patientId%>',<%=patDataDetail%>);"><img title="<%=LC.L_View%>" src="./images/View.gif" border ="0"></img></A></td>

<!--        <td width =5%> <A href="enrollpatient.jsp?mode=N&srcmenu=<=src%>&patientCode=<=StringUtil.encodeString(patientCode)%>&patProtId=&pkey=<=patientId%>&page=patientEnroll&selectedTab=2&studyId=<=studyId%>">Select</A> </td> -->

      </tr>

	  <%


	  } // end of for browser rows loop

	  } //end of if for the check of rowsReturned

        	 %>

    </table>
    <!--pagination links -->

    <table>
	<tr>
	<td>

	<%
        if(searchFrom.equals("initial"))
        {

        }
        else
        {
        if (totalRows > 0) { Object[] arguments = {firstRec,lastRec,totalRows}; %>
		<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%-- <font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
	<%} %>
	</td>
	</tr>
	</table>

	<table align=center>
	<tr>
<%

	if (curPage==1) startPage=1;

    for (int count = 1; count <= showPages;count++)
	{
	cntr = (startPage - 1) + count;
	if ((count == 1) && (hasPrevious))
	{

    %>
	<td colspan = 2>
  	<!--Modified by Manimaran to fix the Bug 2645 -->
	<A href="enrollpatientsearch.jsp?selectedTab=<%=selectedTab%>&srcmenu=<%=src%>&studyId=<%=studyId%>&from=<%=from%>&patCode=<%=patCode%>&patgender=<%=genderVal%>&patstatus=<%=pstat%>&patregby=<%=patregby%>&patage=<%=age%>&patName=<%=patName%>&ptxt=<%=ptxt%>&atxt=<%=atxt%>&gtxt=<%=gtxt%>&rtxt=<%=rtxt%>&rbytxt=<%=rbytxt%>&dPatSite=<%=siteId%>&dStudyExists=<%=studyIdStr%>&searchFrom=search&pkey=<%=personPK%>&page=<%=cntr-1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>">< <%=LC.L_Previous %><%-- Previous*****--%> <%=totalPages%> ></A>&nbsp;&nbsp;&nbsp;&nbsp;

	</td>
	<%
  	}
	%>
	<td>
	<%

	 if (curPage  == cntr)
	 {
     %>
		<FONT class = "pageNumber"><%= cntr %></Font>
       <%
       }
      else
        {

       %>
          <!--Modified by Manimaran to fix the Bug 2645 -->
	   <A href="enrollpatientsearch.jsp?selectedTab=<%=selectedTab%>&srcmenu=<%=src%>&studyId=<%=studyId%>&from=<%=from%>&patCode=<%=patCode%>&patgender=<%=genderVal%>&patstatus=<%=pstat%>&patregby=<%=patregby%>&patage=<%=age%>&patName=<%=patName%>&ptxt=<%=ptxt%>&atxt=<%=atxt%>&gtxt=<%=gtxt%>&rtxt=<%=rtxt%>&rbytxt=<%=rbytxt%>&dPatSite=<%=siteId%>&dStudyExists=<%=studyIdStr%>&searchFrom=search&pkey=<%=
personPK%>&page=<%=cntr%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>"><%= cntr%></A>
       <%
    	}
	 %>
	</td>
	<%
	  }

	if (hasMore)
	{
   %>
   <!--Modified by Manimaran to fix the Bug 2645 -->
   <td colspan = 3 align = center>
  	&nbsp;&nbsp;&nbsp;&nbsp;<A href="enrollpatientsearch.jsp?selectedTab=<%=selectedTab%>&srcmenu=<%=src%>&studyId=<%=studyId%>&from=<%=from%>&patCode=<%=patCode%>&patgender=<%=genderVal%>&patstatus=<%=pstat%>&patregby=<%=patregby%>&patage=<%=age%>&patName=<%=patName%>&ptxt=<%=ptxt%>&atxt=<%=atxt%>&gtxt=<%=gtxt%>&rtxt=<%=rtxt%>&rbytxt=<%=rbytxt%>&dPatSite=<%=siteId%>&dStudyExists=<%=studyIdStr%>&searchFrom=search&pkey=<%=
personPK%>&page=<%=cntr+1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>">< <%=LC.L_Next %><%-- Next*****--%> <%=totalPages%>></A>
	</td>
	<%
  	}}}
	%>
   </tr>
  </table>


    <!--pagination links -->

  </Form>
  <%
  } //JM

  %>

  <%

	} //end of if body for page right

	else

	{

%>

	  <jsp:include page="accessdenied.jsp" flush="true"/>

 <%	} //end of else body for page right



}//end of if body for session

else{

%>

  <jsp:include page="timeout.html" flush="true"/>

  <%

}



%>

  <div>

    <jsp:include page="bottompanel.jsp" flush="true"/>

  </div>

</div>

</div>

<div class ="mainMenu" id = "emenu">

 <jsp:include page="getmenu.jsp" flush="true"/>

</div>

</body>

</html>

