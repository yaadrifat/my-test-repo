<%@taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC" %>
<table cellpadding="0" cellspacing="0" width="100%" border="0">
  <tr>
  	<td align="center" valign="middle" width="60%"><div class="defComments"><%=MC.CTRP_DraftSelectValFrmVelosEres %></div></td>
  	<td width="40%">&nbsp;</td>
  </tr>
</table>

<table width="100%" cellpadding="0" cellspacing="0" border="0">
<tr>
<td width="50%" valign="top">
<table cellpadding="0" cellspacing="0" width="50%" border="0">
<tr>
	<td align="right" width="30%" height="22px">
		<%=LC.CTRP_DraftSiteRecruitStatus %>&nbsp;
		<span id="ctrpDraftJB.siteRecruitType_error" class="errorMessage" style="display:none;"></span>
	</td>
	<td width="2%"><FONT style="visibility:visible" class="Mandatory">* </FONT></td>
	<td align="left">
	<s:property value="ctrpDraftJB.siteRecruitTypeMenu" escape="false"></s:property></td>
	

</tr>
<tr>
<td align="right"  height="22px">
	<%=LC.CTRP_DraftSiteRecruitStatusDate %>&nbsp;
	<span id="ctrpDraftJB.recStatusDate_error" class="errorMessage" style="display:none;"></span>
</td>
<td ><FONT style="visibility:visible" class="Mandatory">* </FONT></td>
<td align="left" >
<s:textfield name="ctrpDraftJB.recStatusDate" cssClass="datefield" size="20"></s:textfield> </td>


</tr>
<tr>
<td align="right"  height="22px">
	<%=LC.CTRP_DraftOpenAccrualDate %>&nbsp;
	<span id="ctrpDraftJB.openAccrualDate_error" class="errorMessage" style="display:none;"></span>
</td>
<td ><FONT style="visibility:visible" color="green">* </FONT></td>
<td align="left" >
<s:textfield name="ctrpDraftJB.openAccrualDate" cssClass="datefield" size="20"></s:textfield></td>


</tr>
<tr>
<td align="right"  height="22px">
	<%=LC.CTRP_DraftCloseAccrualDate %>&nbsp;
	<span id="ctrpDraftJB.closedAccrualDate_error" class="errorMessage" style="display:none;"></span>
</td>
<td >&nbsp;</td>
<td align="left" >
<s:textfield name="ctrpDraftJB.closedAccrualDate" cssClass="datefield" size="20"></s:textfield></td>


</tr>
<tr>
<td align="right"  height="22px">
	<span id="siteTrgtAccrual0" ><%=LC.CTRP_DraftSiteTargetAccrual %>&nbsp;</span>
	<span id="ctrpDraftJB.siteTrgtAccrual_error" class="errorMessage" style="display:none;"></span>
</td>
<td ><span id="siteTrgtAccrual1" ><FONT style="visibility:visible" class="Mandatory">* </FONT></span></td>
<td align="left" ><span id="siteTrgtAccrual2" >
<s:textfield name="ctrpDraftJB.siteTrgtAccrual" size="20"></s:textfield></span></td>
</tr>
</table>
</td>
<td width="50%">
	<table>
		<tr>
			<td align="right" width="30%"><%=LC.L_CurStd_Status %>&nbsp;<span id="ctrpDraftJB.currStudyStatus_error" class="errorMessage" style="display:none;"></span></td>
			<td width="2%">&nbsp;</td>
			<td align="left"><s:textfield name="ctrpDraftJB.currStudyStatus" style="background-color:#F7F7F7; border:0px;" size="50" readonly="true" /></td>
		</tr>
		<tr>
			<td align="right" ><%=LC.L_CurStat_Date %>&nbsp;</td>
			<td >&nbsp;</td>
			<td align="left" >	<s:textfield readonly="true" name="ctrpDraftJB.statusValidFrm" style="background-color:#F7F7F7; border:0px;" size="50"></s:textfield></td>
		</tr>
		<tr>
			<td align="right" ><%=LC.CTRP_DraftActStatusValidfrm %>&nbsp;</td>
			<td >&nbsp;</td>
			<td align="left" ><s:textfield readonly="true" name="ctrpDraftJB.activeStatusValidFrom" style="background-color:#F7F7F7; border:0px;" size="50"></s:textfield></td>
		</tr>
		<tr>
			<td align="right" ><%=MC.CTRP_DraftClsdAccStatsVldFrm %>&nbsp;</td>
			<td >&nbsp;</td>
			<td align="left" ><s:textfield readonly="true" name="ctrpDraftJB.studyEnrollClosedDt" style="background-color:#F7F7F7; border:0px;" size="50"></s:textfield></td>
		</tr>
		<tr>
			<td colspan="3">&nbsp;</td>
		</tr>
	</table>
</td>
</tr>
</table>