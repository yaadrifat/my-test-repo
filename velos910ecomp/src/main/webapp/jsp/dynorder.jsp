<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=LC.L_Form_RptPreview%><%--Form Report Preview*****--%></title>
<%@ page language = "java" import = "java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="dynrepB" scope="page" class="com.velos.eres.web.dynrep.DynRepJB" />
<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>

<script>

function setOrder(formobj){
totcount=formobj.totcount.value;
var orderStr="";
var index="";
var dataOrder="";
if (totcount==1){
fldName=formobj.fldName.value;
fldOrder=formobj.fldOrder.value;
if ((fldName.length>0) && (fldOrder.length>0)) orderStr="Order by " + fldName+ " " + fldOrder;

} else if (totcount>0){
for (i=0;i<totcount;i++){
fldName=formobj.fldName[i].value;
index=fldName.indexOf("|");
if (index>=0){
fldName=fldName.substring(0,index);
}

fldOrder=formobj.fldOrder[i].value;
//create a string to store in the datbase and will be pasrsed in modify mode to display on screen
if ((fldName.length>0) && (fldOrder.length>0)){
if (dataOrder.length==0) dataOrder=fldName+"[$]"+fldOrder;
else dataOrder=dataOrder+"[$$]"+fldName+"[$]"+fldOrder;
}
if ((fldName.length>0) && (fldOrder.length>0)){ 
if (orderStr.length==0) orderStr="Order by " + fldName+ " " + fldOrder;
else if (orderStr.length>0) orderStr=orderStr+", " + fldName+ " " + fldOrder;
} // if
}//end for

} //end else
orderStr=replaceSubstring(orderStr,"|",",");
formobj.order.value=orderStr;

formobj.dataOrder.value=dataOrder;
formobj.submit();
}
</script>
</head>
<% String src,sql="",tempStr="",sqlStr="",fltrName="",fltrId="",prevId="";
   int strlen=0,firstpos=-1,secondpos=-1,index=-1;
src= request.getParameter("srcmenu");
System.out.println(src);
	
	
%>
<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>
<jsp:param name="sess" value="keep"/>

</jsp:include>   
<body style="overflow:scroll;">
<br>
<DIV class="formDefault" id="div1">  
<%

	String mode = request.getParameter("mode");
	String dataOrder="",dynType="";
	String selectedTab = request.getParameter("selectedTab");
	ArrayList sessCol=new ArrayList();
	HashMap attributes = new HashMap();
	ArrayList sessColName=new ArrayList();
	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))
	{
	String[] scriteria=null,fldData=null,fldSelect= null;
	ArrayList selectedfld=null;
	ArrayList dataList=new ArrayList() ;
	ArrayList tempList=new ArrayList();
	ArrayList startBracList=new ArrayList();
	ArrayList endBracList=new ArrayList();
	ArrayList scriteriaList=new ArrayList();
	ArrayList fldDataList=new ArrayList();
	HashMap FltrAttr = new HashMap();
	HashMap FldAttr= new HashMap();
	HashMap TypeAttr= new HashMap();
	HashMap SortAttr= new HashMap();
	String[] fldOrder=null,fldOrderBy=null;
	String[] fldName=request.getParameterValues("fldLocal");
	String[] fldCol=request.getParameterValues("fldLocalCol");
	String[] fldNameSelect=null;
	String[] extend=null;
	ArrayList fldNameSelList=new ArrayList();
	ArrayList extendList=new ArrayList();
	ArrayList excludeList=new ArrayList();
	ArrayList fltrDtIdList=new ArrayList();
	String[] startBracket=request.getParameterValues("startBracket");
	String[] endBracket=request.getParameterValues("endBracket");
	String[] exclude = request.getParameterValues("exclude");
	String[] fltrDtId= request.getParameterValues("fltrDtId");
	String filter=request.getParameter("filter");
	String direction=request.getParameter("direction");
	direction=(direction==null)?"":direction;
	fltrName=request.getParameter("fltrName");
	if (fltrName==null) fltrName="";
	if (filter==null) filter="";
	fltrId=request.getParameter("selFltr");
	if (fltrId==null) fltrId="0";
	if (fltrDtId!=null)
	fltrDtIdList=EJBUtil.strArrToArrayList(fltrDtId);
	if (startBracket!=null)
	startBracList=EJBUtil.strArrToArrayList(startBracket);
	if (endBracket!=null)
	endBracList=EJBUtil.strArrToArrayList(endBracket);
	fldNameSelect=request.getParameterValues("fldName");
	if (fldNameSelect!=null)
	fldNameSelList=EJBUtil.strArrToArrayList(fldNameSelect);
	scriteria=request.getParameterValues("scriteria");
	if (scriteria!=null)
	scriteriaList=EJBUtil.strArrToArrayList(scriteria);
	fldData=request.getParameterValues("fltData");
	if (fldData!=null) 
	fldDataList=EJBUtil.strArrToArrayList(fldData);
	extend=request.getParameterValues("extend");
	if (extend!=null)
	extendList=EJBUtil.strArrToArrayList(extend);
	System.out.println("fldNameSelect"+fldNameSelect);
	if (exclude!=null)
	excludeList=EJBUtil.strArrToArrayList(exclude);
	//Preprocess excludeList,startBracList,endBracList to add -1 for chgeckboxes not selected,"" is stored for rows for unchecled chkbox
	if (fldNameSelList!=null){
	for(int z=0;z<fldNameSelList.size();z++){
	if (excludeList.indexOf(EJBUtil.integerToString(new Integer(z)))>=0){}
	else{
	excludeList.add(z,"");
	}
	if (startBracList.indexOf(EJBUtil.integerToString(new Integer(z)))>=0){}
	else{
	startBracList.add(z,"");
	}
	if (endBracList.indexOf(EJBUtil.integerToString(new Integer(z)))>=0){}
	else{
	endBracList.add(z,"");
	}
	
	}//end for 
	}//end if
	//end
	
	attributes=(HashMap)(tSession.getAttribute("attributes"));
		if (attributes==null){%>
	<META HTTP-EQUIV=Refresh CONTENT="0; URL=dynrepbrowse.jsp?srcmenu=<%=src%>">
	
	<%}else {
	if (attributes.containsKey("TypeAttr")) TypeAttr=(HashMap)attributes.get("TypeAttr");
	if (attributes.containsKey("FltrAttr")) FltrAttr=(HashMap)attributes.get("FltrAttr");
	if (fltrDtIdList!=null){
	if (fltrDtIdList.size()>0) FltrAttr.put("fltrDtIdList",fltrDtIdList);
	}
	if (excludeList!=null) {
	if (excludeList.size()>0) FltrAttr.put("excludeList",excludeList);
	}
	if (startBracList!=null){
	if (startBracList.size()>0) FltrAttr.put("startBracList",startBracList);
	}
	if (fltrName.length()>0) FltrAttr.put("fltrName",fltrName);
	if ((fltrId.length()>0) && (direction.equals("forward"))) FltrAttr.put("fltrId",fltrId);
	
	if (endBracList!=null){ 
	if (endBracList.size()>0) FltrAttr.put("endBracList",endBracList);
	}
	if (fldNameSelList!=null){
	if (fldNameSelList.size()>0)	FltrAttr.put("fldNameSelList",fldNameSelList);
	}
	if (scriteriaList!=null) {
	if (scriteriaList.size()>0) FltrAttr.put("scriteriaList",scriteriaList);
	}
	if (fldDataList!=null){
	if (fldDataList.size()>0)  FltrAttr.put("fldDataList",fldDataList);
	  }
	if (extendList!=null) {
	if (extendList.size()>0) FltrAttr.put("extendList",extendList);
	}
	
	if (fldName!=null) FltrAttr.put("fldName",fldName);
	if (fldCol!=null) FltrAttr.put("fldCol",fldCol);
	
	//if (filter.length()>0)	FltrAttr.put("filter",filter);
	 if (filter.length()==0) filter="undefined";
	 System.out.println("mode"+ mode);
	 if (direction.equals("forward")) FltrAttr.put("filter",filter);
	 System.out.println("FltrAttr"+ FltrAttr);
	 if (FltrAttr!=null) attributes.put("FltrAttr",FltrAttr);
	String formId=request.getParameter("formId");
	if (formId==null) formId="";
	if (formId.length()==0){
	if (TypeAttr.containsKey("formId")) formId=(String)TypeAttr.get("formId");
	}
	if (formId==null) formId="";
	
	if (TypeAttr.containsKey("prevId")) prevId=(String)TypeAttr.get("prevId");
	if (prevId==null) prevId="";
	if (prevId.length()>0){
	if (!formId.equals(prevId)){
	 if (attributes.containsKey("SortAttr")) attributes.remove("SortAttr");
	 
	}}
	if (TypeAttr.containsKey("dynType")) dynType=(String) TypeAttr.get("dynType");
	if (dynType==null) dynType="";
	if (attributes.containsKey("FldAttr")) FldAttr=(HashMap)attributes.get("FldAttr");
	if (attributes.containsKey("SortAttr")) SortAttr=(HashMap)attributes.get("SortAttr");
	if (FldAttr.containsKey("sessCol")) sessCol=(ArrayList)FldAttr.get("sessCol");
	if (FldAttr.containsKey("sessColName")) sessColName=(ArrayList)FldAttr.get("sessColName");
	if (SortAttr.containsKey("repOrder")) dataOrder=(String)SortAttr.get("repOrder");
	dataOrder=(dataOrder==null)?"":dataOrder;
	
	if (SortAttr.containsKey("fldName")) fldOrder=(String[])SortAttr.get("fldName");
	if (SortAttr.containsKey("fldOrder")) fldOrderBy=(String[])SortAttr.get("fldOrder");
	System.out.println("fldOrder" + fldOrder + " dataOrder" + dataOrder);
	if ((fldOrder==null) && (dataOrder.length()>0)){
	ArrayList tempName=new ArrayList();
	ArrayList tempOrder=new ArrayList();
	//[$$] is replaced with ~ because of imporoper token results from tokenzer using [$$]
	dataOrder=StringUtil.replaceAll(dataOrder,"[$$]","~");
	StringTokenizer st= new StringTokenizer(dataOrder,"~");
	System.out.println("&&&&&&&&&&&&&&&&&dataOrder&&&&&&&&&&&&&"+dataOrder);
	StringTokenizer stTemp=null;
	while (st.hasMoreTokens()){
	 tempStr=(String)st.nextToken();
	 System.out.println("tempStr"+tempStr);
	System.out.println(tempStr.substring(0,tempStr.indexOf("[$]")));
	System.out.println(tempStr.substring((tempStr.indexOf("[$]"))+3));
	if ((tempStr.trim()).length()>3){
	System.out.println("inhere"+tempStr.length());
	tempName.add(tempStr.substring(0,tempStr.indexOf("[$]")));
	tempOrder.add(tempStr.substring((tempStr.indexOf("[$]"))+3));
	}
	
	}
	fldOrder=(( String [] ) tempName.toArray ( new String [ tempName.size() ] ));
	fldOrderBy=(( String [] ) tempOrder.toArray ( new String [ tempOrder.size() ] ));
	
	}
	tSession.setAttribute("attributes",attributes);
	int counter=0;
	String name="";
	StringBuffer fldDD=new StringBuffer();
	
	System.out.println("*****************************************************************************************");
	if (attributes.containsKey("FldAttr")) System.out.println("I am here-FldAttr"+attributes.get("FldAttr"));
	System.out.println("*****************************************************************************************");
	if (attributes.containsKey("TypeAttr")) System.out.println("I am here-TypeAttr"+attributes.get("TypeAttr"));
	System.out.println("*****************************************************************************************");
	if (attributes.containsKey("FltrAttr")) System.out.println("I am here-FltrAttr"+attributes.get("FltrAttr"));
	System.out.println("*****************************************************************************************");
	if (attributes.containsKey("SortAttr")) System.out.println("I am here-SortAttr"+attributes.get("SortAttr"));
	System.out.println("*****************************************************************************************");
	if (attributes.containsKey("RepAttr")) System.out.println("I am here-RepAttr"+attributes.get("RepAttr"));
	System.out.println("*****************************************************************************************");
	String formName=(String)TypeAttr.get("formName");
	 if( formName==null) formName="";
	%>

<P class="sectionHeadings"> <%=LC.L_FrmRpt_Sort%><%--Form Report >> Sort*****--%> </P>
<form name="dynorder" action="dynrep.jsp" method="post">
<table><tr>
<%if (attributes.get("TypeAttr")!=null){%>
<td><A href="dynreptype.jsp?mode=link&srcmenu=<%=src%>&dynType=<%=dynType%>"><%=LC.L_Select_RptType%><%--Select Report Type*****--%></A></td>
<%}else{%>
<td><p class="sectionHeadings"><%=LC.L_Select_RptType%><%--Select Report Type*****--%></p></td>
<%}%>
<td><p class="sectionHeadings">>></p></td>
<%if (attributes.get("FldAttr")!=null){%>
<td><A href="dynselectflds.jsp?mode=link&srcmenu=<%=src%>"><%=LC.L_Select_Flds%><%--Select Fields*****--%></A></td>

<%}else{%>
<td><p class="sectionHeadings"><%=LC.L_Select_Flds%><%--Select Fields*****--%></p></td>
<%}%>
<td><p class="sectionHeadings">>></p></td>
<%if (attributes.get("FltrAttr")!=null){%>
<td><A href="dynfilter.jsp?srcmenu=<%=src%>"><%=LC.L_Filter_Criteria%><%--Filter Criteria*****--%></A></td>

<%}else{%>
<td><p class="sectionHeadings"><%=LC.L_Filter_Criteria%><%--Filter Criteria*****--%></p></td>
<%}%>
<td><p class="sectionHeadings">>></p></td>
<%if (attributes.get("SortAttr")!=null){%>
<td><A href="dynorder.jsp?mode=link&srcmenu=<%=src%>"><font color="red"><b><%=LC.L_Sort_Criteria%><%--Sort Criteria*****--%></b></font></A></td>

<%}else{%>
<td><p class="sectionHeadings"><font color="red"><b><%=LC.L_Sort_Criteria%><%--Sort Criteria*****--%></b></font></p></td>
<%}%>
<td><p class="sectionHeadings">>></p></td>
<%if (attributes.get("RepAttr")!=null){%>
<td><A href="dynrep.jsp?mode=link&srcmenu=<%=src%>"><%=LC.L_Preview_Save%><%--Preview & Save*****--%></A></td>

<%}else{%>
<td><p class="sectionHeadings"><%=LC.L_Preview_Save%><%--Preview & Save*****--%></p></td>
<%}%>
</tr></table>
<input type="hidden" name="formId" value="<%=formId%>")>
<input type="hidden" name="totcount" value="<%=sessCol.size()%>">
<input type="hidden" name="mode" value="<%=mode%>">
<input type="hidden" name="order" value="">
<input type="hidden" name="dataOrder" value="<%=dataOrder%>">
<input type="hidden" name="filter" value="<%=filter%>">
<input type="hidden" name="srcmenu" value="<%=src%>">
<P class="sectionHeadings"><%=LC.L_Selected_Form%><%--Selected Form*****--%>: <%=formName%> </P>
<P class="sectionHeadings"><%=MC.M_Specify_SortCriteria%><%--Please Specify a sort criteria*****--%>:</P>
<table class="tableDefault" width="100%" border="1">
<tr>
<th width="15%"><%=LC.L_Select_Fld%><%--Select Field*****--%></th>
<th width="15%"><%=LC.L_Sort_Order%><%--Sort Order*****--%></th>

</tr>

<%
for (int i=0;i<(sessCol.size());i++){
	
  if ((i%2)==0)
  {%>
  <tr>
   <%}else{ %>
   <tr>
   <%}%>
   
   <td width="15%"><%
   if (sessCol!=null){
   
   %>
	<SELECT NAME='fldName'> ;
	<OPTION value='' ><%=LC.L_Select_AnOption%><%--Select an Option*****--%></OPTION>
	<%for (counter = 0; counter < (sessCol.size()) ; counter++){
		name =(String)sessCol.get(counter);
		if (name==null) name="";
		/*if (name.length()>=32) name=name.substring(0,30);
		if (name.indexOf("?")>=0) 
		name=name.replace('?','q');
		if (name.indexOf("\\")>=0)
		name=name.replace('\\','S');*/
		if (name.length()>0){
		index=name.indexOf("|");
		if (index>=0){
		name=name.substring(0,index);
}
		if ((fldOrder!=null) && (i<fldOrder.length)){
	       	if (name.equals(fldOrder[i])){		%>
		<OPTION value='<%=name%>' selected><%=sessColName.get(counter)%></OPTION>
		<%}else{%>
		<OPTION value='<%=name%>'><%=sessColName.get(counter)%></OPTION>
		<%}}else{%> 		
		<OPTION value='<%=name%>'><%=sessColName.get(counter)%></OPTION>
		<%}}}%> 
		
		</SELECT>
	<%}%>
	</td>
   <td width="10%"><select size="1" name="fldOrder">
   <option value=""><%=LC.L_Select_AnOption%><%--Select an Option*****--%></option>   
   <option value="Asc" <% if (fldOrderBy!=null){if (i<fldOrderBy.length) {System.out.println(fldOrderBy[i]); if (fldOrderBy[i].equals("Asc")){%>selected<%}}}%>><%=LC.L_Ascending%><%--Ascending*****--%></option>
   <option value="Desc" <%if (fldOrderBy!=null){ if (i<fldOrderBy.length){if (fldOrderBy[i].equals("Desc")){%>selected<%}}}%>><%=LC.L_Descending%><%--Descending*****--%></option>
</select></td><td width="65%" ></td>
 </tr>
<%}%>
</table>
<table width="100%" cellspacing="0" cellpadding="0">
      <td width="20%"> 
	   
		
      </td> 

      <td> 
		<button type="submit" onClick ="return setOrder(document.dynorder);"><%=LC.L_Next%></button>		<!--<A href=""><img src="../images/jpg/Next.gif" align="absmiddle" border="0"></img></A>-->
      </td> 
      </tr>
  </table>
  <input type="hidden" name="sess" value="keep">
</form>
<%
}//end for attributes==null
} else {  //else of if body for session

%>
  <jsp:include page="timeout.html" flush="true"/>

  <%
}


%>
 <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</DIV>
  <div class ="mainMenu" id = "emenu">
  <jsp:include page="getmenu.jsp" flush="true"/>
  </div>


</body>
</html>
