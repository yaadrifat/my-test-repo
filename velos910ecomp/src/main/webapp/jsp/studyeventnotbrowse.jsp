<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head> 
<title><%=LC.L_Alerts_Notifications%><%--Alerts and Notifications*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT language="JavaScript1.1">
function  validate(formobj){    
     if (!(validate_col('e-Signature',formobj.eSign))) return false
	 if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
     }
}
function openwin1(formobj,counter) {
	  var names = formobj.enteredByName[counter].value;
	  var ids = formobj.enteredByIds[counter].value;
	  var windowToOpen = "multipleusersearchdetails.jsp?fname=&lname=&from=alertnotify&mode=initial&ids=" + ids + "&names=" + names + "&rownum=" + counter;
      window.open(windowToOpen,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=400")
;}
</SCRIPT>
</head>

<%
	String src="";
	src= request.getParameter("srcmenu");
%>	

<jsp:include page="panel.jsp" flush="true"> 
	<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<%@ page language = "java" import = "com.velos.eres.business.section.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.DateUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.esch.business.common.*,com.velos.eres.service.util.*"%>
<body>
<br>
<DIV class="browserDefault" id="div1"> 
  <%

   HttpSession tSession = request.getSession(true); 
   if (sessionmaint.isValidSession(tSession))
   {
    String mode="N";//request.getParameter("mode");
	String globalFlag = "G";
	String protocolId="";
	Calendar cal1 = new GregorianCalendar();
   String selectedTab ;
   String from;
   int pageRight = 0;
   	protocolId=request.getParameter("protocolId");
	String studyId = request.getParameter("studyId");
	from = request.getParameter("from");
    selectedTab = request.getParameter("selectedTab");

%>
	<jsp:include page="studytabs.jsp" flush="true"> 
		<jsp:param name="from" value="<%=from%>"/> 
		<jsp:param name="selectedTab" value="<%=selectedTab%>"/>
		<jsp:param name="studyId" value="<%=studyId%>"/>
	</jsp:include>
	
	<% stdRights =(StudyRightsJB) tSession.getValue("studyRights");	 
	if ((stdRights.getFtrRights().size()) == 0){
 		pageRight= 0;
		}else{
		pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));
		}
	if (pageRight >= 4 )
	  {
 	  %>
	
<br>


<table class=tableDefault  width="100%" border=0>
<tr><td width= 40%>
	<p class = "sectionHeadings" ><%=LC.L_Notification%><%--Notification*****--%> </p>
</td>

<td width=60% align=right>
	<a href="studyeventnot.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&mode=N&studyId=<%=studyId%>&globalFlag=<%=globalFlag%>&protocolId=<%=protocolId%>&from=<%=from%>" onClick="return f_check_perm(<%=pageRight%>,'N');"> <%=LC.L_Add_Notification%><%--Add Another Notification*****--%></a>
</td>
</tr>
</table>
<table  class=tableDefault  width="100%" border=0>
<tr>
	<th width=35% ><%=LC.L_For_EvtStatus%><%--For Event Status*****--%> </th>
	<th width=35% ><%=LC.L_Notify_User%><%--Notify User*****--%> </th>
	<th width=30% ><%=LC.L_Event%><%--Event*****--%></th>
</tr>
	<% EventInfoDao alertNotify = new EventInfoDao();	
	
	alertNotify.getAlertNotifyValues(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(protocolId),"N"); 
	ArrayList anCodelstDescs=alertNotify.getAnCodelstDescs();
	ArrayList anIds=alertNotify.getAnIds();	
	ArrayList anCodelstSubTypes=alertNotify.getAnCodelstSubTypes();
	ArrayList anTypes=alertNotify.getAnTypes();
	ArrayList anuserLists=alertNotify.getAnuserLists();
	ArrayList anuserIds=alertNotify.getAnuserIds();
	ArrayList anMobIds=alertNotify.getAnMobIds();
	ArrayList anFlags=alertNotify.getAnFlags();
	String anCodelstDesc="";
	String anId="";
	String anCodelstSubType="";
	String anType="";
	String anuserList="";
	String anuserId="";
	String anMobId="";
	String anFlag="";
	String status="";
	int checkcount=0;
	int length=anIds.size();

	%>
	<% for (int i=0;i<length; i++) {
		anCodelstDesc=((anCodelstDescs.get(i)) == null)?"-":(anCodelstDescs.get(i)).toString();
		anId=((anIds.get(i)) == null)?"-":(anIds.get(i)).toString();
		anCodelstSubType=((anCodelstSubTypes.get(i)) == null)?"-":(anCodelstSubTypes.get(i)).toString();
		anType=((anTypes.get(i)) == null)?"-":(anTypes.get(i)).toString();
		anuserList=((anuserLists.get(i)) == null)?"":(anuserLists.get(i)).toString();
		anMobId=((anMobIds.get(i)) == null)?"":(anMobIds.get(i)).toString();
	%>
	<% if ((checkcount%2)==0) { %>
      <tr class="browserEvenRow"> 
        <%}
	else{
	 %>
      <tr class="browserOddRow"> 
      <%	
	}
	checkcount++;
	%>	
	<td width=20% align="center">
		<a href="studyeventnot.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&mode=M&studyId=<%=studyId%>&alertNotifyId=<%=anId%>&globalFlag=<%=globalFlag%>&protocolId=<%=protocolId%>&from=<%=from%>" onClick="return f_check_perm(<%=pageRight%>,'E');"> <%=anCodelstDesc%></a>
 	</td>
	<td width=25% align="center"> <%=anuserList%> </td>
	<td width=25% align="center"> <%=anMobId%> </td>
	<% } %>	
	</tr>

<tr><td>&nbsp;</td><tr>
</table>
  <table width="100%" cellspacing="0" cellpadding="0">
  	<tr>
      <td align=center> 
		<A href="studynotification.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>" type="submit"><%=LC.L_Back%></A>	
      </td>
	<tr>
  </table>
<%
} //end of else page right
} //end of if session times out
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
 <%
	
} //end of else body for time out
%>
   <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id = "emenu">
	<jsp:include page="getmenu.jsp" flush="true"/>   
</div>
</body>
</html>
