<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Crf_Dets %><%-- CRF Details*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
</head>


<jsp:useBean id="crflibB" scope="request" class="com.velos.esch.web.crflib.CrflibJB"/>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.esch.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.esch.web.crflib.CrflibJB,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*"%>

<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>

<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>


<body>
<DIV> 

<%
	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))
	{
		
%>	
	<jsp:include page="sessionlogging.jsp" flush="true"/>
 <%   
		String eSign= request.getParameter("eSign");
    	String oldESign = (String) tSession.getValue("eSign");
	   if(oldESign.equals(eSign)) 
	   {
//SV, 10/21/04			int errorCode=0;

			String src = request.getParameter("srcmenu");
			String duration = request.getParameter("duration");
			String protocolId = request.getParameter("protocolId");
			String calledFrom = request.getParameter("calledFrom");
			
			//KM-Flag value as mentioned in the column comment.
			if (calledFrom.equals("P"))
				calledFrom ="C";
			String eventId = request.getParameter("eventId");
			String mode = request.getParameter("mode");
			String fromPage = request.getParameter("fromPage");
			String calStatus = request.getParameter("calStatus");
			String eventmode = request.getParameter("eventmode");
			String displayDur=request.getParameter("displayDur");
			String displayType=request.getParameter("displayType");
			String creator=(String) tSession.getAttribute("userId");
			String ipAdd=(String)tSession.getAttribute("ipAdd");
		
			
			String crflibId=request.getParameter("crflibId");

			String propagateInVisitFlag = request.getParameter("propagateInVisitFlag"); //SV, 10/12/04, cal-enh-05
			String propagateInEventFlag = request.getParameter("propagateInEventFlag");
			//String eventName=	request.getParameter("eventName");
			String eventName ="";//KM
			String crfType="";

			String[] saCrfNames = request.getParameterValues("crflibName");            			
            String[] saCrfNumbers=request.getParameterValues("crflibNumber");
			ArrayList arrEventIds = new ArrayList();
			ArrayList arrCrfNames = new ArrayList();
			ArrayList arrCrfNumbers = new ArrayList();
			ArrayList arrCrfFlag = new ArrayList();
			ArrayList creators = new ArrayList();
			ArrayList ipAdds = new  ArrayList(); 
			int strArrLen= saCrfNames.length;

			
			
			if (calledFrom.equals("P")||calledFrom.equals("L"))
			{
				  eventdefB.setEvent_id(EJBUtil.stringToNum(eventId));
				  eventdefB.getEventdefDetails();
				  eventName = eventdefB.getName();
			}else{	
				  eventassocB.setEvent_id(EJBUtil.stringToNum(eventId));
				  eventassocB.getEventAssocDetails();     
				  eventName = eventassocB.getName(); 
			 }			



			for(int i=0;i<strArrLen;i++)
				{
				if(!( EJBUtil.isEmpty (saCrfNames[i])))
					{
						
						arrEventIds.add(eventId);
						arrCrfNames.add(saCrfNames[i]) ;
						arrCrfNumbers.add(saCrfNumbers[i]) ;
						arrCrfFlag.add(calledFrom) ;
						creators.add(creator) ;
						ipAdds.add(ipAdd);
					}
				}

			if (calledFrom.equals("C"))
				calledFrom ="P";
			
			int len = arrCrfNames.size();
			int iret= -1;
				
			
			CrflibDao crflibDao = new CrflibDao();
			CrflibDao crflibIds = null;
			crflibDao.setEventsId(arrEventIds);
			crflibDao.setCrflibNumber(arrCrfNumbers); 
			crflibDao.setCrflibName(arrCrfNames);
			crflibDao.setCrflibFlag(arrCrfFlag);
			crflibDao.setCreator(creators);
			crflibDao.setIpAdd(ipAdds);
			crflibDao.setRows(ipAdds.size()) ;
//SV,10/21/04			errorCode=crflibB.insertCrfNamesNums(crflibDao);
			crflibIds=crflibB.insertCrfNamesNums(crflibDao);
//			iret = crflibB.getCrflibId();

System.out.println("********************************iret" + 		iret);
		EventdefDao eventdefdao = new EventdefDao();

		for (int i=0; i<crflibIds.getCrflibId().size(); i++) {
			int id = 0;
		// Cal-enh - 05, now check if the user wishes to propagate these changes to other events.
			if ((propagateInVisitFlag == null) || (propagateInVisitFlag.equals("")))
				propagateInVisitFlag = "N";

			if ((propagateInEventFlag == null) || (propagateInEventFlag.equals("")))
				propagateInEventFlag = "N";
			
			if ( (propagateInVisitFlag.equals("Y")) || (propagateInEventFlag.equals("Y"))) {
//		   		crflibB.propagateCrflib(EJBUtil.stringToNum(protocolId), propagateInVisitFlag, propagateInEventFlag);  
				id = EJBUtil.stringToNum(crflibIds.getCrflibId(i).toString());
				
				System.out.println("id.........." + id);
				//SV, 10/22/04, add mode for propagation, "P" protocol calendar.
				eventdefdao.propagateEventUpdates(EJBUtil.stringToNum(protocolId), EJBUtil.stringToNum(eventId), "EVENT_CRF", id, propagateInVisitFlag, propagateInEventFlag, "N", calledFrom);
			}
		}
				
		%>
<br><br><br><br><br>

<p class = "sectionHeadings" align = center><%=MC.M_Data_SavedSucc %><%-- Data saved successfully.*****--%></p>
	<% if((request.getParameter("fromPage")).equals("fetchProt"))
		   {%>
		   <META HTTP-EQUIV=Refresh CONTENT="1;
		URL=crflibbrowser.jsp?eventmode=M&mode=M&duration=&srcmenu=<%=src%>&selectedTab=6&eventId=<%=eventId%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>" >
		   <%}
		   else{%>

		<script>
			window.opener.location.reload();
			setTimeout("self.close()",1000);
		</script>
		   
		<%
		   }
}//end of if for eSign check
else{  
%>
		<jsp:include page="incorrectesign.jsp" flush="true"/>
<%	  
			}//end of else of incorrect of esign

}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
</BODY>
</HTML>
