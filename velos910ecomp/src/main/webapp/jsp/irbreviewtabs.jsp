<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language = "java" import = "com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.*,com.velos.eres.business.common.CtrlDao,java.util.*,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration,com.velos.eres.business.common.TeamDao"%>
<%@ page import="com.velos.eres.web.objectSettings.ObjectSettingsCache, com.velos.eres.web.objectSettings.ObjectSettings"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
 
<%!
  private String getHrefBySubtype(String subtype, String mode) {
      if (subtype == null) { return "#"; }
      if ("review_menu".equals(subtype)) {
          return "#";
      }
      if ("irb_revsumm_tab".equals(subtype)) {
          return "irbreviewarea.jsp?selectedTab=irb_revsumm_tab&mode=N";
      }
      if ("irb_revfull_tab".equals(subtype)) {
          return "irbreviewarea.jsp?selectedTab=irb_revfull_tab&mode=N";
      }
      if ("irb_revexp_tab".equals(subtype)) {
          return "irbreviewarea.jsp?selectedTab=irb_revexp_tab&mode=N";
      }
      if ("irb_revexem_tab".equals(subtype)) {
          return "irbreviewarea.jsp?selectedTab=irb_revexem_tab&mode=N";
      }
      if ("irb_revanc_tab".equals(subtype)) {
          return "irbreviewarea.jsp?selectedTab=irb_revanc_tab&mode=N";
      }
      return "#";
  }
%>

<%
String mode="N";
String selclass;
 
 
String userId = "";
 
String userIdFromSession = "";
String accId = "";

String tab= request.getParameter("selectedTab");
String from= request.getParameter("from");
mode= request.getParameter("mode");
HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession))
{
 	userIdFromSession= (String) tSession.getValue("userId");
	accId = (String) tSession.getValue("accountId");
	
 	
 	userId = (String) tSession.getValue("userId");
	
	int pendRevRight= 0;
	int fullRevRight=  0;
	int expRevRight=  0;
	int exeRevRight  = 0;
	int ancRevRight  = 0;
	
	     	   	   	
    
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
	
	pendRevRight = Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_RPR"));

	fullRevRight =  Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_RFR"));
	
	expRevRight =  Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_RXPR"));

    exeRevRight = Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_RXER"));
    
    ancRevRight = Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_RACR"));
    
    
  
String uName = (String) tSession.getValue("userName");
 
ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
ArrayList tabList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "irb_rev_tab");

%>
<DIV>
<!-- <table  class= "selectedTab" cellspacing="0" cellpadding="0" border="0">  --> 
<table  cellspacing="0" cellpadding="0" border="0"> 
	<tr>
    <%
    // check the rights
 	// To check for the account level rights
	String modRight = (String) tSession.getValue("modRight");
	 
	 for (int iX=0; iX<tabList.size(); iX++) {
        ObjectSettings settings = (ObjectSettings)tabList.get(iX);
	    // If this tab is configured as invisible in DB, skip it
	    if ("0".equals(settings.getObjVisible())) {
	        continue;
	    }
	    // Figure out the access rights
        boolean showThisTab = false;
		
	 	  
		if ("irb_revsumm_tab".equals(settings.getObjSubType()) && (pendRevRight >=6 || pendRevRight == 4))
		{
			showThisTab = true;
		}
		else if ("irb_revfull_tab".equals(settings.getObjSubType()) && (fullRevRight >=6 || fullRevRight == 4))
		{
			showThisTab = true;
		}
		else if ("irb_revexp_tab".equals(settings.getObjSubType()) && (expRevRight >=6 || expRevRight == 4))
		{
			showThisTab = true;
		}
				else if ("irb_revexem_tab".equals(settings.getObjSubType()) && (exeRevRight >=6 || exeRevRight == 4))
		{
			showThisTab = true;
		}
		else if ("irb_revanc_tab".equals(settings.getObjSubType()) && (ancRevRight >=6 || ancRevRight == 4))
		{
			showThisTab = true;
		}


	
		    
        //for access right check, showThisTab should be set to false if access right check fails
        
 	    if (!showThisTab) { continue; } // no access right; skip this tab
	    // Figure out the selected tab position
        if (tab == null) { 
            selclass = "unselectedTab";
	    } else if (tab.equals(settings.getObjSubType())) {
            selclass = "selectedTab";
        } else {
            selclass = "unselectedTab";
        }
    %>
		<td  valign="TOP">
			<table class="<%=selclass%>"  cellspacing="0" cellpadding="0" border="0">
				<tr>
				<!-- <td class="<%=selclass%>" rowspan="3" valign="top" >
						<img src="../images/leftgreytab.gif" height=20 border=0 alt="">
					</td>  --> 
					<td class="<%=selclass%>">
						<a href="<%=getHrefBySubtype(settings.getObjSubType(),mode)%>"><%=settings.getObjDispTxt()%></a>
					</td> 
				  <!--   <td class="<%=selclass%>" rowspan="3" valign="top">
						<img src="../images/rightgreytab.gif"  height=20 border=0 alt="">
			        </td>  --> 
			  	</tr> 
		   	</table> 
        </td>
    <%
     } // End of tabList loop
    %>
		
   	</tr>
 <!--   <tr>
     <td colspan=10 height=10></td>
  </tr>  --> 
</table>
<table class="tabBorder" width="99%" height="5"><tr><td></td></tr></table>
<%		
} //session time out.
%>
</DIV> 
