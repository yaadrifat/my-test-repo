<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Form_Sec%><%--Form Section*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %><%@page import="com.velos.eres.service.util.*"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="formSecJB" scope="request"  class="com.velos.eres.web.formSec.FormSecJB"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB"%>

<body>
<DIV class="popDefault" id="div1"> 

<%
	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))
	{
		
%>
	
		<jsp:include page="sessionlogging.jsp" flush="true"/>
 <%   
		String eSign= request.getParameter("eSign");
    	String oldESign = (String) tSession.getValue("eSign");
	   if(oldESign.equals(eSign)) 
	   {
	
		
			int errorCode=0;
			int rows = 0;
			String mode="" ; 
			String creator="";
			String ipAdd="";
			String accountId="";
			String formId="";	
			
		
			creator=(String) tSession.getAttribute("userId");
			ipAdd=(String)tSession.getAttribute("ipAdd");
	     	accountId=(String)tSession.getAttribute("accountId");
			mode=request.getParameter("mode");
			formId=request.getParameter("formId");
			
			String recordType=mode;  
			String sectionSequence="";
			String[] sectionNameStrA= request.getParameterValues("sectionName");
			String[] sectionSequenceStrA= request.getParameterValues("sectionSequence");
			String[] repeatNumStrA= request.getParameterValues("repeatNum");
			String[] sectionFormatStrA= request.getParameterValues("sectionFormat");
			String[] formSecIdsStrA =request.getParameterValues("formSecId");
			
			
			
			
			
			
			ArrayList formSecIds= new  ArrayList(); 
			ArrayList sectionNames = new ArrayList(); 
			ArrayList sectionSequences = new ArrayList(); 
			ArrayList repeatNums = new ArrayList();
			ArrayList sectionFormats = new ArrayList();
			ArrayList creators = new ArrayList();
			ArrayList ipAdds = new  ArrayList(); 
			ArrayList recordTypes = new  ArrayList(); 
			ArrayList formIds= new  ArrayList(); 
								
			
					
			FormSecDao formSecDao= new FormSecDao();
				
						 		
			
			if (mode.equals("N"))
			{
							
				int first= sectionNames.indexOf("");
				int last = sectionNames.lastIndexOf("");
				
				sectionNames = EJBUtil.strArrToArrayList(sectionNameStrA) ;
				
				int len = sectionNames.size();
				int internalLen= 0 ;
				for ( int j=0 ; j< len ; j++)
				{
					if   (     !( EJBUtil.isEmpty (sectionNameStrA[j]  )   )     )
					{	
						sectionNames.add(internalLen,sectionNameStrA[j]) ;
						sectionSequences.add(internalLen,sectionSequenceStrA[j].trim()) ;
						sectionFormats.add(internalLen,sectionFormatStrA[j]) ;
						repeatNums.add(internalLen,repeatNumStrA[j]) ;
						creators.add(internalLen,creator) ;
						ipAdds.add(internalLen,ipAdd);
						recordTypes.add(internalLen,recordType);
						formIds.add(internalLen,formId);
						internalLen = internalLen + 1 ;
						
					}
			
					
				}
					
				formSecDao.setFormLibId(formIds);
				formSecDao.setFormSecName(sectionNames);
				formSecDao.setFormSecSeq(sectionSequences);
				formSecDao.setFormSecFmt(sectionFormats);
				formSecDao.setFormSecRepNo(repeatNums);
				formSecDao.setCreator(creators);
				formSecDao.setIpAdd(ipAdds);
				formSecDao.setRecordType(recordTypes);
				formSecDao.setRows(ipAdds.size()) ;
				errorCode=formSecJB.insertNewSections(formSecDao);

			
			}
			if (mode.equals("M")) 
			{
			
				sectionNames = EJBUtil.strArrToArrayList(sectionNameStrA) ;
				int count = sectionNames.size();
	
						
				String[] tempCreatorStrA =new String[count] ; 
				String[] tempIpAddStrA =new String[count] ; 
				String[] tempRecordTypeStrA = new String[count] ; 
				String[] tempFormIdStrA = new String[count] ;  
				
				formSecIds =	 EJBUtil.strArrToArrayList(formSecIdsStrA) ;	
			    sectionNames = EJBUtil.strArrToArrayList(sectionNameStrA) ;
			    sectionSequences =  EJBUtil.strArrToArrayList(sectionSequenceStrA) ;
			    sectionFormats = EJBUtil.strArrToArrayList(sectionFormatStrA);
			    repeatNums =  EJBUtil.strArrToArrayList(repeatNumStrA) ;				
				
			
				for ( int i = 0 ; i<count ; i++)
				{	
					tempCreatorStrA[i]=creator ;
					tempIpAddStrA[i]= ipAdd ;
					tempRecordTypeStrA[i]=mode;
					tempFormIdStrA[i]=formId ;
					
				}
				
				
				
				creators = EJBUtil.strArrToArrayList(tempCreatorStrA); 
				ipAdds =  EJBUtil.strArrToArrayList(tempIpAddStrA);
				recordTypes =  EJBUtil.strArrToArrayList(tempRecordTypeStrA);
				formIds =  EJBUtil.strArrToArrayList(tempFormIdStrA);
				
			
				
				formSecDao.setFormSecId(formSecIds);
				formSecDao.setFormLibId(formIds);
				formSecDao.setFormSecName(sectionNames);
				formSecDao.setFormSecSeq(sectionSequences);
				formSecDao.setFormSecFmt(sectionFormats);
				formSecDao.setFormSecRepNo(repeatNums);
				formSecDao.setModifiedBy(creators);
				formSecDao.setIpAdd(ipAdds);
				formSecDao.setRecordType(recordTypes);
				formSecDao.setRows(ipAdds.size()) ;
				errorCode=formSecJB.updateNewSections(formSecDao); 
				
				
				
			}
		
		
		if ( errorCode == -2  )
		{
%>
			
				<br><br><br><br><br><p class = "successfulmsg" align = center>		
	 			<p class = "successfulmsg" ><%=MC.M_Data_NotSvdSucc%><%--Data not saved successfully*****--%> </p>

<%
		
		} //end of if for Error Code

		else

		{
%>	
		
<%
		
%>		
		<br><br><br><br><br>
		<p class = "successfulmsg" align = center><%=MC.M_Data_SavedSucc%><%--Data Saved Successfully*****--%> </p>
		<script>
			window.opener.location.reload();
			setTimeout("self.close()",1000);
		</script>		

<%
		}//end of else for Error Code Msg Display
	}//end of if old esign
	else{  
%>
		<jsp:include page="incorrectesign.jsp" flush="true"/>
<%	  
			}//end of else of incorrect of esign
     }//end of if body for session 	
	
	else
	{

%>
		  <jsp:include page="timeout.html" flush="true"/>
<%
	
} //end of else of the Session 
 

%>

</body>

</html>

