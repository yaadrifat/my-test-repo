<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Acc_Grps%><%--Account Groups*****--%></title>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META HTTP-EQUIV="no-cache">

</head>
<Script>
function openwinsnap() {
    windowName=window.open("snapshottext.htm","Information","toolbar=no,scrollbars=yes,resizable=no,menubar=no,status=yes,width=500,height=400")
	windowName.focus();
;}

function confirmBox(pageRight) {

	if(f_check_perm(pageRight,'N'))
	{
	if (confirm("<%=MC.M_SureToCreate_StdPcolVer%>"))/*if (confirm("Are you sure you want to create a version of this study protocol?"))*****/ {
	    return true;
	}else{
		return false;
	}
	}
	else
	{
	return false;
	}
}

function protocolDetails(id,name,grprights)
{
	var src = document.study.src.value;
	var mode =document.study.mode.value;
	study.action= "study.jsp?mode="+mode +"&srcmenu="+src +"&selectedTab=1&studyId="+id;
	study.submit();
}

function createVersion(id,name,grprights)
{
	alert("jkk");
	var src = document.study.src.value;
	study.action= "createversion.jsp?srcmenu="+src +"&calledfrom=S&studyId="+id;
	study.submit();
}

</Script>
<% String src;
src= request.getParameter("src");
%>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<body>
<br>
<jsp:useBean id="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.web.user.UserJB,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.ulink.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.*"%>

<% int accountId=0;
   int pageRight = 0;
   HttpSession tSession = request.getSession(true);
%>
<DIV class="browserDefault" id="div1">
	<%
	if (sessionmaint.isValidSession(tSession))
	{

%>
		<form name="study" method=post action="studyBrowser.jsp">
	<%
	   	String acc = (String) tSession.getValue("accountId");
	   	String uName = (String) tSession.getValue("userName");
	   	accountId = EJBUtil.stringToNum(acc);
		GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");
		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));

	   	int counter = 0;
	   	if (pageRight > 0 )
		{
		   	String userId = (String) tSession.getValue("userId");
			Vector vec = new Vector();

			Column pkstudy = new Column(LC.L_Std_Id/*Study Id*****/);
			pkstudy.hidden = true;
			vec.add(pkstudy);


			Column stdNo = new Column(LC.L_Study_Number/*Study Number*****/);
			stdNo.passAsParameter = true;
			stdNo.width = "25%"	;
			stdNo.align = "center"	;
			vec.add(stdNo);

			Column version = new Column(LC.L_Version/*Version*****/);
			version.width = "25%" ;
			version.align = "center"	;
			vec.add(version);

			Column title = new Column(LC.L_Title/*Title*****/);
			title.width = "25%" ;
			title.align = "center"	;
			vec.add(title);

			Column det  = new Column("");
			det.value = LC.L_Pcol_Dets/*Protocol Details*****/;
			det.funcName = "protocolDetails";
			det.width = "15%";
			det.align = "center";
			vec.add(det);

			Column createVer  = new Column("");
			createVer.value = MC.M_Create_NewVer/*Create a new Version*****/;
			createVer.funcName = "createVersion";
			createVer.width = "15%";
			createVer.align = "center";
			vec.add(createVer);

			String sSQL = " select PK_STUDY,STUDY_NUMBER,STUDY_VER_NO,STUDY_TITLE from er_study where pk_study in (select FK_STUDY from er_studyteam where fk_user ='" +userId +"') order by STUDY_NUMBER,  STUDY_VER_NO ";
			//out.print(sSQL);
			String title1 =LC.L_Study_Results/*Study Results*****/;
			String srchType=request.getParameter("srchType");
			String sortType=request.getParameter("sortType");
			String sortOrder=request.getParameter("sortOrder");

			request.setAttribute("sSQL",sSQL);

			request.setAttribute("srchType",srchType);
			request.setAttribute("vec",vec);
			request.setAttribute("sTitle",title1);

			String s_scroll=request.getParameter("scroll");
			request.setAttribute("scroll",s_scroll);
			request.setAttribute("sortType",sortType);
			request.setAttribute("sortOrder",sortOrder);
		%>
		<input type=hidden name=src value=<%=src%>>
		<input type=hidden name=frompage value="groupbrowser">
		<input type=hidden name=mode value="M">

			<P class = "userName"> <%= uName %> </P>
			<P class="sectionHeadings"> <%=LC.L_MngPcol_Open%><%--Manage Protocols >> Open*****--%></P>

			<br>
          <table width="100%" cellspacing="0" cellpadding="0" border=0 >
           <tr >
             <td width = "100%">
               <P class = "defComments"> <%=MC.M_ShdAccPcol_PatInfoStd%><%--You should have access to a protocol and have been assigned rights to <%=LC.Pat_Patient_Lower%> information to be able to Manage <%=LC.Pat_Patients%>.<Br>You have access to the following Study Protocols*****--%>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<A href = '#' onClick=openwinsnap()><%=MC.M_What_IsSnapshot%><%--What is a Snapshot*****--%> </A>
               </P>
             </td>
           </tr>
           <tr>
             <td height="10"></td>
             <td height="10"></td>
           </tr>
         </table>

			<jsp:include page="result.jsp?jsFile=searchJS" flush="true"></jsp:include>
	</form>
				<%
		} //end of if body for page right
		else
		{
		%>
			<jsp:include page="accessdenied.jsp" flush="true"/>
		<%
		} //end of else body for page right
	}//end of if body for session
	else
	{
	%>
		<jsp:include page="timeout.html" flush="true"/>
	<%
	}
	%>
	<div>
    	<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
	</DIV>
<div class ="mainMenu" id="emenu">
  	<jsp:include page="getmenu.jsp" flush="true"/>
</div>
</body>

</html>
