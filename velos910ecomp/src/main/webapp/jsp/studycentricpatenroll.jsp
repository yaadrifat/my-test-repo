<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=MC.M_Std_CentricPatEnrl%><%--<%=LC.Std_Study%> Centric <%=LC.Pat_Patient%> Enrollment*****--%></title>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,java.text.*" %>
<%@ page import="com.velos.eres.business.common.*,com.velos.esch.business.common.*,com.velos.eres.service.util.*,com.velos.eres.web.user.UserJB,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.business.patProt.impl.*,com.velos.eres.web.user.*"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:include page="include.jsp" flush="true"/>
</head>


<SCRIPT Language="javascript">

var dd1, delay;

function closeme(){
window.close();
}

$j().ready(function(){
	
	var subType ;
	var formobj = document.enroll;
	var num = formobj.patStudyStatus.options.selectedIndex;
	var patStdStat = formobj.patStudyStatus[num].value;


	if(patStdStat==undefined ||patStdStat==null|| patStdStat==''){
		document.enroll.protocolId.disabled = true;
	 	document.enroll.protStDate.disabled = true;
	 	// Ankit: Bug-11751 Date-07-Sep-2012
	 	//document.enroll.protocolId.value = ""; 
	 	document.enroll.protStDate.value = "";
	}

	if (formobj.statusSubType.length == undefined){
	 	subType = formobj.statusSubType.value;
	 }else{
	 	subType = formobj.statusSubType[num].value;
	 }
	 if (subType == 'scrfail'){
	 	document.enroll.protocolId.disabled = true;
	 	document.enroll.protStDate.disabled = true;
	 	// Ankit: Bug-11751 Date-07-Sep-2012
	 	//document.enroll.protocolId.value = "";
	 	document.enroll.protStDate.value = "";
	 }
});

function startDate1(delay1) {
  var adate, date, amonth;
  delay = delay1;
  adate = new Date();
  date = adate.getDate();
  amonth = adate.getMonth()+1;

  if (amonth == 1) date += " January";
  else if (amonth == 2) date += " February";
  else if (amonth == 3) date += " March";
  else if (amonth == 4) date += " April";
  else if (amonth == 5) date += " May";
  else if (amonth == 6) date += " June";
  else if (amonth == 7) date += " July";
  else if (amonth == 8) date += " August";
  else if (amonth == 9) date += " September";
  else if (amonth == 10) date += " October";
  else if (amonth == 11) date += " November";
  else if (amonth == 12) date += " December";
  date += " " + adate.getFullYear();
  document.atime21.date.value = date;
  dd1 = setTimeout("startDate1(delay)",delay1);
}
	function f_setdate()
	{
		if ((!document.enroll.protStDate.disabled) && (document.enroll.protStDate.value == '') )
		{
			document.enroll.protStDate.value = document.enroll.statusDate.value
	    }
		return true;
	}

	function fChangeStatus(formobj)
	{
		var num;
		var subType ;
		var value;

		 num = formobj.patStudyStatus.options.selectedIndex;
		 value = formobj.patStudyStatus.options[num].value;

		 if (formobj.statusSubType.length == undefined)
		 {
		 	subType = formobj.statusSubType.value;
		 }
		 else
		 {
		 	subType = formobj.statusSubType[num].value;
		 }

		 if (subType == 'scrfail')
		 {
		 	document.enroll.protocolId.disabled = true;
		 	document.enroll.protStDate.disabled = true;
		 	document.enroll.protocolId.value = "";
		 	document.enroll.protStDate.value = "";

		 }
		 else
		 {
		 	document.enroll.protocolId.disabled = false;
		 	document.enroll.protStDate.disabled = false;

			if (document.enroll.protStDate.value == '')
		 	{
		 		document.enroll.protStDate.value = document.enroll.statusDate.value;
		 	}

		 }

		document.body.onkeypress = enterkey
	  }


	 function enterkey(evt) {
		  var evt = (evt) ? evt : event
		  var charCode =  evt.keyCode
		  if (charCode == 13) {
		    if (validate(document.enroll) != false) {
				document.enroll.action="updatestudycentricenroll.jsp";
				document.enroll.submit();
			}
		  }
     }

 function  validate(formobj){


	 
	//JM: 18Mar2011: #D-CTMS-1a
		if (VAL_PATSTUDYSTAT_RESTRICT_FUTURE_STATDATE == 1 && isFutureDate(formobj.statusDate.value))
		{
			//JM: 29MAR2011: #5997	
			if (!(validate_date(formobj.statusDate))){ 		
				return false;
			} else{
				alert("<%=MC.M_RestrictFutureDate%>");/*alert("<%=MC.M_RestrictFutureDate%>");*****/
			}
			
			formobj.statusDate.focus();
			return false;
		}
		

	//Virendra: Fixed Bug No. 4729, Added '\' validation for PatientID, Firstname, Lastname
	 patIDval = formobj.patid.value;
	 patfnameval = formobj.patfname.value;
	 patlnameval = formobj.patlname.value;

 	 if(patIDval.indexOf("%")!=-1 || patIDval.indexOf("\\")!=-1  )
	  {
 		  var paramArray = ["#,\\"];
 		  alert(getLocalizedMessageString("M_SplCharNotAllowed", paramArray));/*alert("Special Character(%,\\) not allowed for this Field");*****/
		  formobj.patid.focus();
		  return false;
	  }
 	 if( patfnameval.indexOf("\\")!=-1  )
	  {
 		  var paramArray = ["\\"];
		  alert(getLocalizedMessageString("M_SplCharNotAllowed", paramArray));/*alert("Special Character(\\) not allowed for this Field");*****/
		  formobj.patfname.focus();
		  return false;
	  }
 	if( patlnameval.indexOf("\\")!=-1  )
	  {
 		  var paramArray = ["\\"];
		  alert(getLocalizedMessageString("M_SplCharNotAllowed", paramArray));/*alert("Special Character(\\) not allowed for this Field");*****/
		  formobj.patlname.focus();
		  return false;
	  }


     if (!(validate_col('Organization',formobj.dPatSite))) return false;
     if (!(validate_col('<%=LC.Pat_Patient%> <%=LC.Std_Study%> Id',formobj.patStudyId))) return false;

// Added by gopu to fix the bugzilla Issue #2189 Doesn't prompt user to enter date in the correct format
//JM: 20July2006
     if (formobj.patDataDetail.value > 0)
     if (!(validate_date(formobj.patdob))) return false
     if (!(validate_date(formobj.statusDate))) return false


     if (document.getElementById('pgcustompatdob')) {
      if (!(typeof(formobj.patdob)=="undefined")) {
	     if (!(validate_col('Date of Birth',formobj.patdob))) return false
	     }
	 }
	 //gender mandatory/non-mand implementation
	 if (document.getElementById('pgcustompatgender')) {
	     if (!(validate_col('patgender',formobj.patgender))) return false
	 }

	 if (!(validate_col('<%=LC.Pat_Patient%> <%=LC.Std_Study%> Status',formobj.patStudyStatus))) return false;
	 if (!(validate_col('Status Date',formobj.statusDate))) return false;

	 //if protocol is selected, prompt for date and vice versa
	 var protocol;
	 var protocoldate;

	 protocol = formobj.protocolId.value;
	 protocoldate = formobj.protStDate.value;


	 if(protocol == null || protocol == '' ){
		 formobj.protStDate.value="";
	 }else{
		 if (!(validate_col('Calendar Start Date',formobj.protStDate))) return false;
	 }

// Added by Gopu on 15th April 2005 DOB Validation DOB should not be greater than Today's Date for fixing the Bug No. 2118
	 da= new Date();
	 var str1,str2;
	 str1=parseInt(da.getMonth());
	 str2=str1+1;
	todate = str2+'/'+da.getDate()+'/'+da.getFullYear();
	if (!(typeof(formobj.patdob)=="undefined")) {
	if (formobj.patdob.value != null && formobj.patdob.value !=''){
		if (CompareDates(formobj.patdob.value,todate,'>')){
		  	alert ("<%=MC.M_BirthDtNotGrt_Today%>");/*alert ("Date of Birth should not be greater than Today's Date");*****/
			formobj.patdob.focus();
			return false;
		}
		}
	 }
// Added by Gopu on 15th April 2005 Validation Status Date should not be less than Birth Date for fixing the bug No. 2118
	if (!(typeof(formobj.patdob)=="undefined")) {
		if (formobj.patdob.value != null && formobj.patdob.value !=''){
			if (CompareDates(formobj.patdob.value,formobj.statusDate.value,'>')){
				alert ("<%=MC.M_StatDtNotLess_BirthDt%>");/*alert ("Status Date should not be less than Birth Date");*****/
				formobj.statusDate.focus();
				return false;
			}
		}}



	 /*if(protocoldate != null && protocoldate !='' )
	 {
	 	 //if (!(validate_col('Protocol Calendar',formobj.protocolId))) return false;


	 }*/

	 if (!(validate_col('e-Signature',formobj.eSign))) return false
     if(isNaN(formobj.eSign.value) == true) {
    	 alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/

	formobj.eSign.focus();
	return false;

	  }


   }

function asyncValidate()
{

 valChangeReturn=0;
// In case where view patient right is revoked, we will not see any patfname,patlname,dob fields
//so it's safe to check with just patfname for now.
 if (!(typeof(document.getElementById("patfname"))=="undefined") && (document.getElementById("patfname")!=null)){
 if ((document.getElementById("patfname").value.length>0) && (document.getElementById("patlname").value.length>0) && (document.getElementById("patdob").value.length>0) ) {
 ajaxvalidate('patient~fldob:siteId',0,'fldobMessage','','<%=MC.M_PatWithSameNameDobAldy%><%--A <%=LC.Pat_Patient%> already exists with same First Name,Last Name and Date Of Birth.*****--%>','patfname:patlname:patdob:accountId:patid');
 }
 }
 if (document.getElementById("patid").value.length>0) {
  ajaxvalidate('patient:patid',0,'ajaxMessage','','<%=MC.M_PatId_AldyExst%><%--This <%=LC.Pat_Patient%> ID already exists for the selected organization or it\'s  parent/child organizations.*****--%>','siteId') ;
 }

}

</SCRIPT>



<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="studyStatB" scope="page" class="com.velos.eres.web.studyStatus.StudyStatusJB" />
<jsp:useBean id="usrSite" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" />
<jsp:useBean id="studyProt" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="CommonB" scope="page" class="com.velos.eres.web.common.CommonJB" />
<jsp:include page="include.jsp" flush="true"/>
<body>
<DIV class="popDefault" id="div1">

  <%
	HttpSession tSession = request.getSession(true);

	if (sessionmaint.isValidSession(tSession))
	{
		String statid = "";

		String startDate = "";
		String patStudyStat = "";
		String studyId = "";
		String patId = "";
		String pcode="";
		String studyNum = "";
		String mode = "";
		PatProtBean psk = null;

		String patStudyId = "";
		String oldPatStudyId = ""; //Amar

		String patEnrolledBy = "";
		String patEnrolledByName = "";
		String dGender = "";

		String userIdFromSession = (String) tSession.getValue("userId");
		String accountId = (String) tSession.getValue("accountId");





		int siteId=0;
		int personPK = 0;
		int orgRight=0,index=-1;
		UserSiteDao usd = new UserSiteDao ();
		ArrayList arSiteid = new ArrayList();
		ArrayList arSitedesc = new ArrayList();
		String ddPatSite = "";

		SettingsDao settingsdao = new SettingsDao();
		ArrayList arSettingsValue = new ArrayList();
		String patStudyIdAutoStr = "",forwardPage="";
		int patStudyIdAuto = 0;
		String patStudyIdDefValue = "";
		String readOnlyStr = "";

		studyId = request.getParameter("studyId");
		int iStudyId=EJBUtil.stringToNum(studyId);


			//JM: 16/09/05 get study status related data
			int JFlag=0;


			String studyPermClosureDate="",studyCentricStr="";

			if (iStudyId > 0)
			{
				studyPermClosureDate = studyStatB.getMinPermanentClosureDate(iStudyId);

				if (! StringUtil.isEmpty(studyPermClosureDate)) //i.e. a permanent closure status exists
				{
					JFlag=1;
				}

				//get study centric settings

				settingsdao = CommonB.retrieveSettings(iStudyId,3,"STUDY_CENTRIC_ENROLL")	;

				if (settingsdao != null)
				{
					arSettingsValue = settingsdao.getSettingValue();

					if (arSettingsValue != null && arSettingsValue.size() > 0)
						studyCentricStr = (String) arSettingsValue.get(0);
				} // end of if for settings dao

			}	//iStudyId

		if 	(!studyCentricStr.equals("Y"))
		{ %>

			<br><br><br><br><br>
  <p class="defComments" align=center><b>
  	<%=MC.M_FeatureAval_StdEnrlOpt%><%--This feature is only available if the <%=LC.Std_Study%>-centric enrollment option is enabled*****--%>
  </b></p>

  <p align=center><button onClick ="closeme();"><%=LC.L_Close%></button></p>

		<%}
		else
		{

		com.velos.eres.web.user.UserJB userB=(com.velos.eres.web.user.UserJB)(tSession.getValue("currentUser"));
		int usrGroup =EJBUtil.stringToNum( userB.getUserGrpDefault());

		String userSiteId = userB.getUserSiteId();
		userSiteId=(userSiteId==null)?"":userSiteId;

		int patDataDetail = studyB.getStudyCompleteDetailsAccessRight( EJBUtil.stringToNum(userIdFromSession),usrGroup,EJBUtil.stringToNum(studyId));


		studyB.setId(EJBUtil.stringToNum(studyId));
	    studyB.getStudyDetails();
		studyNum = studyB.getStudyNumber();

		String dStatus = "";
		CodeDao cd1 = new CodeDao();
		CodeDao cdGender = new CodeDao();


		cd1.getCodeValues("patStatus",EJBUtil.stringToNum(accountId));

		cdGender.getCodeValues("gender");
		dGender = cdGender.toPullDown("patgender", 0);


		// for the selected status, get its subtype

		ArrayList arCid = new ArrayList();
		arCid = cd1.getCId();

   		ArrayList arCSubType = new ArrayList();
		arCSubType = cd1.getCSubType();

   		ArrayList cDesc = new ArrayList();
		cDesc = cd1.getCDesc();

		//JM: 09Sep2010, #3356-part(16)
		ArrayList cHide = new ArrayList();
		cHide = cd1.getCodeHide();
		String hideStr = "";


		//create dropdown for patient study status

		StringBuffer mainStr = new StringBuffer();
		StringBuffer codeSubTypeStr = new StringBuffer();
		int pcounter = 0;
		String statSubType  = "";

		ConfigFacade cFacade=ConfigFacade.getConfigFacade();
		HashMap hashPgCustFld = new HashMap();

		ConfigDetailsObject cdoPgField = cFacade.populateObject(Integer.parseInt(accountId), "patient");
		//if customized fields exists then put the field name and its value in hashmap
		//with key as the field name and value is the position at which the values can be referred to

		if (cdoPgField.getPcfField()!= null && cdoPgField.getPcfField().size() > 0) {
			for (int i=0;i<cdoPgField.getPcfField().size();i++){
				hashPgCustFld.put((String)cdoPgField.getPcfField().get(i), String.valueOf(i));
			}
		}



		mainStr.append("<SELECT NAME='patStudyStatus' onChange='fChangeStatus(document.enroll)'>") ;

		for (pcounter = 0; pcounter  <= cDesc.size() -1 ; pcounter++)
		{

			hideStr = (String)cHide.get(pcounter);

			if (hideStr.equals("N")){
				mainStr.append("<OPTION value = "+ arCid.get(pcounter)+">" + cDesc.get(pcounter)+ "</OPTION>");
				statSubType = (String) arCSubType.get(pcounter);
				codeSubTypeStr.append("<input type=hidden name=\"statusSubType\" value=\"" + statSubType + "\">");
			}
		}

		mainStr.append("<OPTION value = '' SELECTED >"+LC.L_Select_AnOption/*Select an Option*****/+"</OPTION>") ;

		mainStr.append("</SELECT>");
		codeSubTypeStr.append("<input type=hidden name=\"statusSubType\" value=\"\">");
		dStatus = mainStr.toString();

		//dStatus = EJBUtil.createPullDown("patStudyStatus",0,arCid ,cDesc );


		int pageRight = 0;



	//******************GET STUDY TEAM RIGHTS //***************************************************************

	TeamDao teamDao = new TeamDao();
	teamDao.getTeamRights(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(userIdFromSession));
	ArrayList tId = teamDao.getTeamIds();
	int patdetright = 0;
	if (tId.size() == 0) {
		pageRight=0 ;
	}else
	{
		stdRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));

			ArrayList teamRights ;
						teamRights  = new ArrayList();
						teamRights = teamDao.getTeamRights();

						stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
						stdRights.loadStudyRights();


		if ((stdRights.getFtrRights().size()) == 0){
		 	pageRight= 0;
			patdetright = 0;
		}else{
			pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYMPAT"));
			patdetright = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYVPDET"));

		}
	}


		 usd = usrSite.getSitesToEnrollPat(EJBUtil.stringToNum(studyId), EJBUtil.stringToNum(accountId),EJBUtil.stringToNum(userIdFromSession));
		 arSiteid = usd.getUserSiteSiteIds();
		 arSitedesc = usd.getUserSiteNames();

		 if (arSiteid.size() > 0)
		 {
		 	ddPatSite = EJBUtil.createPullDownNoSelect("dPatSite id='siteId' onchange='asyncValidate()'",0,arSiteid,arSitedesc);
		 }
		 else
		 {
		 	ddPatSite = EJBUtil.createPullDown("dPatSite id='siteId' onchange='asyncValidate()'",0,arSiteid,arSitedesc);
		 }

		 int ddProtCount = 0;
		 EventAssocDao eventAssoc = studyProt.getStudyProts(EJBUtil.stringToNum(studyId),"A");
		 ArrayList protocolIds = new ArrayList();
		 ArrayList names = new ArrayList();
		 String ddProtList = "";
		 int defProtId = 0;
		 oldPatStudyId = patStudyId; //Amar

		 protocolIds = eventAssoc.getEvent_ids();
		 names = eventAssoc.getNames();

	 	 ddProtCount = protocolIds.size();

	 	 if (ddProtCount == 1)
	 	 {
	 	 	 defProtId = ((Integer) protocolIds.get(0)).intValue();
	 	 }

	 	 ddProtList = EJBUtil.createPullDown("protocolId",defProtId,protocolIds ,names );


		settingsdao = CommonB.retrieveSettings(EJBUtil.stringToNum(studyId),3)	;
		ArrayList settingsKeyword=settingsdao.getSettingKeyword();
		if (settingsdao != null)
		{
			index=settingsKeyword.indexOf("PATSTUDY_ID_GEN");
			arSettingsValue = settingsdao.getSettingValue();
			if (arSettingsValue != null && arSettingsValue.size() > 0)
			{
				if (index>=0) patStudyIdAutoStr = (String) arSettingsValue.get(index);
				if (StringUtil.isEmpty(patStudyIdAutoStr))
				{
					patStudyIdAutoStr = "";
				}
				if (patStudyIdAutoStr.equals("auto"))
				{
					patStudyIdAuto = 1;
					patStudyIdDefValue = "<"+LC.L_SysHypenGen+">"/*<system-generated>*****/;
					readOnlyStr = " READONLY ";
				}
				else
				{
					patStudyIdAuto = 0;
					patStudyIdDefValue = "";
					readOnlyStr = "";
				}

			index=settingsKeyword.indexOf("STUDY_ENROLL_FORWARD");
			if (index>=0)	forwardPage=(String) arSettingsValue.get(index);
			forwardPage=(forwardPage==null)?"":forwardPage;

			}


		}// end of if for settings dao


//**************************************************************************************************************
//String JFlag=request.getParameter("JFlag");//JM

%>
<input type="hidden" name="accountId" id="accountId" value="<%=accountId%>" >

<%
 if(pageRight == 5 ||  pageRight == 7)
	{

 if ((JFlag)==1){%>

  <br><br><br><br><br>
  <p class="defComments" align=center><b>
  <%=MC.M_StdClosed_NoEnrl%><%--This <%=LC.Std_Study%> is now closed, no further enrollments allowed.*****--%>
  </b></p>

  <p align=center><button onClick ="closeme();"><%=LC.L_Close%></button></p>


<%}else{%>


  <P class="sectionHeadings"><%=LC.L_Add_NewPat%><%--Add New <%=LC.Pat_Patient%>*****--%></P>

  <Form name="enroll" id="stdcentricenroll" method="post" action="updatestudycentricenroll.jsp" onsubmit="if (validate(document.enroll)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
<input type="hidden" name="patDataDetail" value="<%=patDataDetail%>" >



    <%=codeSubTypeStr.toString()%>
    <table width="100%" cellspacing="0" cellpadding="0" border="0" class="basetbl">

	<tr>
	  <td width="40%">
         <b><%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%>: </b><br>
     </td>
	<td width="60%">
		<b><%=studyNum%></b>
	</td>
    </tr>

    <input type="hidden" name="studyId" size = 15 MAXLENGTH = 50 value="<%=studyId %>">
      <tr>
		 <td ><%=LC.L_Organization%><%--Organization*****--%><FONT class="Mandatory">* </FONT></td>
	     <td>
	   		<%=ddPatSite%>
	 	 </td>
	</tr>

	<tr>
		 <td > <%=LC.L_Patient_StudyId%><%--<%=LC.Pat_Patient%> <%=LC.Std_Study%> ID*****--%>  <FONT class="Mandatory">* </FONT></td>
	     <td>
	   		<input type=text id="patid" name=patStudyId size=18  maxlength = 20 value='<%=patStudyIdDefValue%>' <%=readOnlyStr%>  onblur="ajaxvalidate('patient:'+this.id,0,'ajaxMessage','','<%=MC.M_PatId_AldyExst%><%--This <%=LC.Pat_Patient%> ID already exists for the selected organization or it\'s  parent/child organizations.*****--%>','siteId')">
	   		<input type=hidden name="patStudyIdAuto" size=10 maxlength = 20 value='<%=patStudyIdAuto%>'>
	 	 </td>
	</tr>
	<tr>
    <td >
	   <%=LC.L_First_Name%><%--First Name*****--%>
    </td>
	<td >
	<%if( patDataDetail >= 4 ){%>
	<input type=text name="patfname" id="patfname" size = 20 MAXLENGTH = 30 onblur="valChangeReturn=0;ajaxvalidate('patient~fldob:patfname',0,'fldobMessage','','<%=MC.M_PatWithSameNameDobAldy%><%--A <%=LC.Pat_Patient%> already exists with same First Name,Last Name and Date Of Birth.*****--%>','patfname:patlname:patdob:accountId:patid')">
	<%}else{ %>
	****

	<%}%>
    </td>
    </tr>
  <tr>
    <td >
       <%=LC.L_Last_Name%><%--Last Name*****--%>
    </td>
    <td >
    <%if( patDataDetail >= 4 ){%>
		<input type=text id="patlname" name="patlname" size = 20 MAXLENGTH = 20 onblur="valChangeReturn=0;ajaxvalidate('patient~fldob:patlname',0,'fldobMessage','','<%=MC.M_PatWithSameNameDobAldy%><%--A <%=LC.Pat_Patient%> already exists with same First Name,Last Name and Date Of Birth.*****--%>','patfname:patlname:patdob:accountId:patid')">
	<%}else{ %>
	****

	<%}%>
    </td>
 </tr>
 <tr>
    <td >
       <%=LC.L_Date_OfBirth%><%--Date of Birth*****--%>
        <% if( patDataDetail >= 4 ){
			if (hashPgCustFld.containsKey("dob")) {
			int fldNum= Integer.parseInt((String)hashPgCustFld.get("dob"));
			String dobMand = ((String)cdoPgField.getPcfMandatory().get(fldNum));

			//Check whether the column is manadtory in page customizeed fields table
			if (dobMand.equals("1")) {
				//id property is added to the font column to validate the same in Javascript
				//with the value being pgcustom+[input column name]  (This is not mandtory)
				%>
			   <FONT class="Mandatory" id="pgcustompatdob">* </FONT>
			<% }
		 }else { %>
				   <FONT class="Mandatory" id="pgcustompatdob">* </FONT>
		<%
		}%>
	</td>
<%-- INF-20084 Datepicker-- AGodara --%>
    <td>
    	<input type=text name="patdob" id="patdob" class="datefield" size = 10 MAXLENGTH = 11 onchange="valChangeReturn=0;ajaxvalidate('patient~fldob:patdob',0,'fldobMessage','','<%=MC.M_PatWithSameNameDobAldy%><%--A <%=LC.Pat_Patient%> already exists with same First Name,Last Name and Date Of Birth.*****--%>','patfname:patlname:patdob:accountId:patid')">
	</td>
	<%}else{%>
	<td>****</td>
	<input type=hidden name="patdob" id="patdob" size = 10 MAXLENGTH = 11 >
	<%}%>
   </tr>
   <tr>
    <td >
       <%=LC.L_Gender%><%--Gender*****--%>
        <% if (hashPgCustFld.containsKey("gender")) {
			int fldNumGender= Integer.parseInt((String)hashPgCustFld.get("gender"));
			String gendMand = ((String)cdoPgField.getPcfMandatory().get(fldNumGender));

			if (gendMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatgender">* </FONT>
			<% }
		 }%>
    </td>
    <td >
		<%=dGender%>
    </td>
   </tr>

	<tr>
        <td > <%=LC.L_Status%><%--Status*****--%> <FONT class="Mandatory">* </FONT></td>
        <td >
			<%=dStatus%>

			 <input type = "hidden" name ="oldPatStudyId" size ="20" value ="<%=oldPatStudyId%>" >
         </td>
     </tr>
     <tr>
         <td > <%=LC.L_Status_Date%><%--Status Date*****--%> <FONT class="Mandatory">* </FONT></td>
   	     <td >
<%-- INF-20084 Datepicker-- AGodara --%>
		 	<input type="text" name="statusDate" class="datefield" onchange="f_setdate()" size = 10 MAXLENGTH = "11"/>
         </td>
	</tr>
	  <tr>
		 <td ><%=LC.L_Protocol_Calendar%><%--Protocol Calendar*****--%></td>
	     <td>
	   		<%=ddProtList%>
	 	 </td>
	</tr>
    <tr>
    <td>
       	<%=LC.L_Cal_StartDate%><%--Calendar Start Date*****--%>
    </td>
    <td><input type=text name="protStDate" class="datefield" size = 10 MAXLENGTH = 10></td>
	</tr>

    </table>

	<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="stdcentricenroll"/>
		<jsp:param name="showDiscard" value="N"/>
   </jsp:include>

   <table width="100%">
   <%--Style added for removing the background green color Bug#:9797 :Date 16 May 2012 --%>
   <tr><td width="100%"><span id="ajaxMessage" ></span></td><td></td></tr>
   <tr><td width="100%"><span id="fldobMessage" ></span></td><td></td></tr>


   </table>

    <br>
    <input type="hidden" name="forwardpage" value="<%=forwardPage%>">

  </Form>

  <%

	}//JM

	} //end of if body for page right

else

{

%>

  <jsp:include page="accessdenied.jsp" flush="true"/>

  <%
} //end of else body for page right
} //End for studyCentricStr
}//end of if body for session

else
{
%>

 <jsp:include page="timeout_childwindow.jsp" flush="true"/>

  <%
}

%>

  <div class = "myHomebottomPanel">
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</div>

<script type="text/javascript">

$j("#ajaxMessage").bind('DOMNodeInserted', function(event) {
	$j("#patid").css("border","1px solid red");

});
$j("#ajaxMessage").bind('DOMNodeRemoved', function(event) {
	$j("#patid").css("border","");
});
</script>

</body>



</html>



