<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>

<head>

<title><%=LC.L_Eres_Logout %><%-- eresearch Logout*****--%></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>

<%@ page language = "java" import="com.velos.eres.service.util.*"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB"/>


<%
		

HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession)){ 
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%

//unbind the session for session tracking
tSession.removeAttribute("eresSessionBinder");

tSession.putValue("userName","");
tSession.putValue("currentUser","");
tSession.putValue("userId", "");
tSession.putValue("accountId", "");
tSession.putValue("userSession", "");
tSession.putValue("studyId","");
tSession.putValue("studyNo","");
tSession.putValue("userAddressId","");
tSession.setAttribute("userId","");
tSession.setAttribute("sessionName","");
tSession.removeAttribute("accMaxStorage");
tSession.invalidate();/* INF-22330 06-Aug-2012 -Sudhir */
}

%>

<body>

<SCRIPT>
window.self.close();
</SCRIPT>

</body>

</html>  


