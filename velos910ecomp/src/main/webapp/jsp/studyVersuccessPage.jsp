<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@page import="com.velos.eres.service.util.MC"%>
<%@page import="com.velos.eres.service.util.LC"%>

<%@page import="com.velos.eres.service.util.VelosResourceBundle"%><HTML>
<HEAD>
<title><%=MC.M_AddNew_VerOrDocu%><%--Add New Version/Document*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</HEAD>
<jsp:include page="sessionlogging.jsp" flush="true"/> 
<jsp:include page="include.jsp" flush="true"/>

<% 
String outOfSpaceStr = request.getParameter("outOfSpace");
outOfSpaceStr=outOfSpaceStr==null?"0":outOfSpaceStr;
String incorrectFileStr = request.getParameter("incorrectFile");
incorrectFileStr=incorrectFileStr==null?"0":incorrectFileStr;
String studyVerErrorStr = request.getParameter("studyVerError");
studyVerErrorStr=studyVerErrorStr==null?"0":studyVerErrorStr;
String fileHashIndexStr = request.getParameter("fileHashIndex");
fileHashIndexStr=fileHashIndexStr==null?"0":fileHashIndexStr;
int outOfSpace = Integer.parseInt(outOfSpaceStr);
int incorrectFile = Integer.parseInt(incorrectFileStr);
int studyVerError = Integer.parseInt(studyVerErrorStr);
int fileHashIndex = Integer.parseInt(fileHashIndexStr);
%>
    <BODY>
<%
int totalError = incorrectFile + outOfSpace + studyVerError;
int totalSuccess = fileHashIndex-1-totalError;
if (totalSuccess < 0) { totalSuccess = 0; }%>
<%if (totalSuccess > 0) {%>
<SCRIPT>
  window.opener.location.reload();
  setTimeout('self.close()',5000);
</SCRIPT>
<%}%>
<P class="successfulmsg" align = center ><%=VelosResourceBundle.getMessageString("M_StdVerFileSucc",totalSuccess)%></p>
<%if (totalError > 0 && outOfSpace == 0) {%>
 
   <P class="successfulmsg" align = center ><%=VelosResourceBundle.getMessageString("M_StdVerFileFail",totalError)%><br>
    
    <%if (studyVerError > 0) {%>
       <%=VelosResourceBundle.getMessageString("M_StdVerFileFailReasonOne",studyVerError)%><br>
    <%}
    if (incorrectFile > 0) {%>
        <%=VelosResourceBundle.getMessageString("M_StdVerFileFailReasonTwo",incorrectFile)%>
    <%}%>
    
<%}%>
</P>
<%if (outOfSpace > 0) {%>
    <p class="sectionHeadings" align="center">
    <%=MC.M_UploadSpace_ContAdmin %><br/><br/>
    <input type="button" value="<%=LC.L_Close%>" onclick="window.close();"/></p>
   
<%}%>
 
</BODY></HTML>