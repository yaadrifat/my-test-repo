<!--

	Project Name:	Velos eResearch

	Author:	Jnanamay Majumdar

	Created on Date:	19October2007

	Purpose:	Update page for the Specimen Status, GUI

	File Name:	updatespcimenstatus.jsp

-->
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>


  <jsp:useBean id="specStatJB" scope="request" class="com.velos.eres.web.specimenStatus.SpecimenStatusJB"/>
  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
   <jsp:useBean id ="specJB" scope="request" class="com.velos.eres.web.specimen.SpecimenJB"/>

  <%@ page language = "java" import = "java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.business.common.*,com.velos.eres.service.util.*"%>

  <%

	String src = null;
	src = request.getParameter("srcmenu");
	String eSign = request.getParameter("eSign");
	String mode = request.getParameter("mode");


	HttpSession tSession = request.getSession(true);

	if (sessionmaint.isValidSession(tSession))

   {
     %>
     <jsp:include page="sessionlogging.jsp" flush="true"/>

     <%

   	String oldESign = (String) tSession.getValue("eSign");

	if(!oldESign.equals(eSign))
	{
%>
  	   <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
	}
	else
  	{
		String ipAdd = (String) tSession.getValue("ipAdd");
		String usr = (String) tSession.getValue("userId");



		int ret = -1;


		String fkSpecimen = request.getParameter("pkspecimen");
		fkSpecimen = (fkSpecimen==null)?"":fkSpecimen;

		String pkSpecStat = request.getParameter("pkSpecimenStat");
		pkSpecStat = (pkSpecStat==null)?"":pkSpecStat;

		//Specimen status..when it is disabled
		String specStatDis = request.getParameter("specStatDisabled");
		specStatDis = (specStatDis==null)?"":specStatDis;

		String specStat = request.getParameter("specimenStatus");
		specStat = (specStat==null)?"":specStat;

		String specQuant = request.getParameter("avlQuantity");
		specQuant = (specQuant==null)?"":specQuant;

		String specQuantUnit = request.getParameter("specQuantityUnit");
		specQuantUnit = (specQuantUnit==null)?"":specQuantUnit;

		String specDate = request.getParameter("statDate");
		specDate = (specDate==null)?"":specDate;
		//KM-#INVP2.5.1
		String statusHH = request.getParameter("statusHH");
		statusHH = (statusHH==null)?"00":statusHH;
		String statusMM = request.getParameter("statusMM");
		statusMM = (statusMM==null)?"00":statusMM;
		String statusSS = request.getParameter("statusSS");
		statusSS = (statusSS==null)?"00":statusSS;
		String statusTime = statusHH + ":" + statusMM + ":" +statusSS;
		specDate = specDate + " " +statusTime;


		String handOffDt = request.getParameter("handOffDate");
		handOffDt = (handOffDt==null)?"":handOffDt;
		String handOffHH = request.getParameter("handOffHH");
		handOffHH = (handOffHH==null)?"00":handOffHH;
		String handOffMM = request.getParameter("handOffMM");
		handOffMM = (handOffMM==null)?"00":handOffMM;
		String handOffSS = request.getParameter("handOffSS");
		handOffSS = (handOffSS==null)?"00":handOffSS;
		String handOffTime = handOffHH + ":" + handOffMM + ":" +handOffSS;
		if (! StringUtil.isEmpty(handOffDt))
	 	{
	 		handOffDt = handOffDt + " " +handOffTime;
	 	}else{
	 		handOffDt = "";
	 	}

		String procDt = request.getParameter("procDate");
		procDt = (procDt==null)?"":procDt;

		String procHH = request.getParameter("procHH");
		procHH = (procHH==null)?"00":procHH;
		String procMM = request.getParameter("procMM");
		procMM = (procMM==null)?"00":procMM;
		String procSS = request.getParameter("procSS");
		procSS = (procSS==null)?"00":procSS;
		String procTime = procHH + ":" + procMM + ":" +procSS;
		if (! StringUtil.isEmpty(procDt))
	 	{
	 		procDt = procDt + " " +procTime;
	 	}else{
	 		procDt = "";
	 	}


		String specStudy = request.getParameter("selStudyIds");
		specStudy = (specStudy==null)?"":specStudy;

		String specStatBy = request.getParameter("specCreatorId");
		specStatBy = (specStatBy==null)?"":specStatBy;


		String specUser = request.getParameter("creatorId");
		specUser = (specUser==null)?"":specUser;



		String storNotes = request.getParameter("statusNotes");
		storNotes = (storNotes==null)?"":storNotes;

		String trackingNum = request.getParameter("trackingNo");
		trackingNum = (trackingNum==null)?"":trackingNum;

		String pProcTypeCode = request.getParameter("pProcType");
		pProcTypeCode = (pProcTypeCode==null)?"":pProcTypeCode;


		if (mode.equals("M")){

 			specStatJB.setPkSpecimenStat(EJBUtil.stringToNum(pkSpecStat));
			specStatJB.getSpecimenStatusDetails();

     	}
			specStatJB.setFkSpecimen(fkSpecimen);
			specStatJB.setSsDate(specDate);

			if (mode.equals("N")){
			specStatJB.setFkCodelstSpecimenStat(specStat);
			}
			specStatJB.setSsQuantityUnits(specQuantUnit);
			specStatJB.setSsQuantity(specQuant);
			specStatJB.setSsTrackingNum(trackingNum);


			specStatJB.setSsNote(storNotes);

			specStatJB.setFkUserRecpt(specUser);
			specStatJB.setFkStudy(specStudy);
			specStatJB.setSsStatusBy(specStatBy);
			specStatJB.setIpAdd(ipAdd);


			specStatJB.setSsProcType(pProcTypeCode);
			specStatJB.setSsHandOffDate(handOffDt);
			specStatJB.setSsProcDate(procDt);



		if (mode.equals("M")){
 			specStatJB.setModifiedBy(usr);
			ret=specStatJB.updateSpecimenStatus();
		}
		else{
			specStatJB.setCreator(usr);
			ret=specStatJB.setSpecimenStatusDetails();
		}


	if ( ret >=0){
	%>
	  <br>
      <br>
      <br>
      <br>
      <br>
     <p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%> </p>
	  <script>
			window.opener.location.reload();
			setTimeout("self.close()",1000);
	  </script>

<% }

}//end of if for eSign check

}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

</HTML>





