<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
</HEAD>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<BODY>


<%@ page language = "java" import = "com.velos.eres.business.common.*,com.velos.eres.service.util.*,java.util.*"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="addressUserB" scope="page" class="com.velos.eres.web.address.AddressJB" />
<%

String cnt,email;


String messageTo = null;

String userId = null;
String userEmail = null;
int rows  = 0;

String mailText = null;
String mailText1,mailText2;
String userMailStatus = "";

String src;
src=request.getParameter("src");


HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession)) {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
	String supportEmail = Configuration.SUPPORTEMAIL;
    String eSign = request.getParameter("eSign");
    String oldESign = (String) tSession.getValue("eSign");

	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
	} else {





 CtrlDao userCtrl = new CtrlDao();
 userCtrl.getControlValues("eresuser");
 rows = userCtrl.getCRows();
 if (rows > 0)
	{
	   userEmail = (String) userCtrl.getCDesc().get(0);
	}

rows = 0;

String msgFrom = request.getParameter("messagefrom");

if (msgFrom.equals("U")){ // in case User Wants to send his own message
	mailText1 = MC.M_DearFrnd_RecvVelosJoin+"\n\n" + request.getParameter("fromName")+ MC.M_InvtSignUp_AccMsg+" (" + request.getParameter("fromName")+ ") :\n" +request.getParameter("message");
	/*mailText1 = "Dear Friend, \n\nYou are receiving this message from Velos eResearch's 'Invite a Friend to Join' service. " +
	"\n\n" + request.getParameter("fromName")+ " has invited you to join the Velos eResearch community. Simply go to" +
	" www.veloseresearch.com and click on the Sign Up link. Complete the registration form and Velos will set up an account for you." +
	"\n\nMessage from (" + request.getParameter("fromName")+ ") :\n" +request.getParameter("message");*****/

	mailText2  = "\n\n"+MC.M_IfHaveQue_CnctUsAt+" " + supportEmail + MC.M_WebsiteInfo_VisionSuiteCa ; 
	/*mailText2  = "\n\nIf you have any questions, please feel free to contact us at " + supportEmail + " or visit our website at www.veloseresearch.com for more information." +
	 "\n\nEasy-to-use..powerful..productive in minutes..and FREE!!!  It doesn't get any better!" +
	 "\n\nThank you for your interest in Velos eResearch." +
	 "\n\nVelos is..Vision and Value with Velocity." +
     "\n\nVelos eResearch Customer Support" +
     "\n\nVelos, Inc." +
     "\n2201 Walnut Avenue, Suite 208" +
     "\nFremont, CA. 94538" ;*****/

}else{ // Standard Message
	mailText1 = MC.M_DearFrnd_RecvVelosJoin+" " +"\n\n" + request.getParameter("fromName")+ MC.M_VelosEresInst_StdInetHosted ;
     /*mailText1 = "Dear Friend, \n\nYou are receiving this message from Velos eResearch's 'Invite a Friend to Join' service. " +
     "\n\n" + request.getParameter("fromName")+ " has invited you to join the Velos eResearch community. Simply go to" +
     " www.veloseresearch.com and click on the Sign Up link. Complete the registration form and Velos will set up an account for you." +
	 "\n\nVelos eResearch is ushering in a powerful new paradigm for managing clinical"+ LC.Std_Studies_Lower+" on the web. Velos eResearch is:"+
	"\n	1.	Productive quickly---no downloads or installation required" +
	"\n 2.	Useful for most any kind of "+LC.Std_Study_Lower+
	"\n 3.	Flexible, intuitive and friendly --- "+LC.Std_Study_Lower+" coordinators can be proficient using Velos eResearch in minutes not hours"+
	"\n 4.	FREE!!! --- exclusively for investigators and your "+LC.Std_Study_Lower+" team"+
	"\n\nMost clinical research software suppliers focus on the needs of "+LC.Std_Study_Lower+" sponsors. Investigators and "+LC.Std_Study_Lower+" administrators are largely unsupported. What sponsors and investigators need is a system that supports the investigator.  Enter Velos eResearch."+
	"\n\nVelos eResearch is a suite of Internet services (hosted software in internet parlance) that gives investigators and "+LC.Std_Study_Lower+" coordinators the ability to:"+
	"\n-	Create and manage "+LC.Std_Study_Lower+" protocols;" +
	"\n-	Enroll "+LC.Std_Study_Lower+" subjects;" +
	"\n-	Automatically generate and update "+LC.Pat_Patient_Lower+"schedules;" +
	"\n-	Track "+LC.Std_Study_Lower+" progress and tasks;" +
	"\n-	Send alerts and reminders to "+LC.Pat_Patients_Lower+" and "+LC.Std_Study_Lower+" team members;" +
	"\n-	Capture billing information; and" +
	"\n-	Generate related reports relatively effortlessly." +
 	"\n\nBeing web-based, Velos eResearch allows the investigators and sponsors to connect with anyone anywhere, and share data for efficient, team-oriented "+LC.Std_Study_Lower+" execution.  Velos eResearch employs state-of-the-art security, encryption, and firewall technology that enables you to keep "+LC.Pat_Patient+"-identifiable information confidential." +
	"\n\nVelos eResearch Console, which includes all the above capabilities, is FREE for you and your "+LC.Std_Study_Lower+" team.  In the future, Velos intends to provide additional services that plug into your Console, and enhanced versions of services included in the Console, for a reasonable subscription fee.  We also reserve the right to charge trial sponsors for "+LC.Std_Studies_Lower+" hosted on Velos eResearch." ;*****/

	mailText2  = "\n\n"+MC.M_IfHaveQue_CnctUsAt+" " + supportEmail + MC.M_WebsiteInfo_VisionSuiteCa ; 
	/*mailText2  = "\n\nIf you have any questions, please feel free to contact us at " + supportEmail + " or visit our website at www.veloseresearch.com for more information." +
	 "\n\nEasy-to-use..powerful..productive in minutes..and FREE!!!  It doesn't get any better!" +
	 "\n\nThank you for your interest in Velos eResearch." +
	 "\n\nVelos is..Vision and Value with Velocity." +
     "\n\nVelos eResearch Customer Support" +
     "\n\nVelos, Inc." +
     "\n2201 Walnut Avenue, Suite 208" +
     "\nFremont, CA. 94538" ;*****/
}


String messageFooter = "\n\n"+MC.M_EmailMsgRead_ErrReadSav ;
/*String messageFooter = "\n\nThis email transmission and any documents, files or previous email messages attached to it may contain information that is confidential or legally privileged. If you are not the intended recipient or a person responsible for delivering this transmission to the intended recipient you are hereby notified that you must not read this transmission and that any disclosure, copying, printing, distribution or use of this transmission is strictly prohibited. If you have received this transmission in error, please immediately notify the sender by telephone or return email and delete the original transmission and its attachments without reading or saving in any manner." ;*****/
mailText = mailText1 + mailText2 +messageFooter;

messageTo = request.getParameter("toEmail");

		//send mail
			try{
   				   	VMailer vm = new VMailer();
			    	VMailMessage msgObj = new VMailMessage();

					msgObj.setMessageFrom(userEmail);
					msgObj.setMessageFromDescription(LC.L_Velos_Eres);/*msgObj.setMessageFromDescription("Velos eResearch");*****/
					//msgObj.setMessageTo(userEmail);
					//msgObj.setMessageBCC(messageTo);
					msgObj.setMessageSubject(request.getParameter("fromName") +" "+MC.M_Invitation_JoinVeRes);/*msgObj.setMessageSubject(request.getParameter("fromName") +" invites you to join Velos eResearch");*****/
					msgObj.setMessageSentDate(new Date());
					msgObj.setMessageText(mailText);

					vm.setVMailMessage(msgObj);
					userMailStatus = vm.sendMail();
					System.out.println("userMailStatus" + userMailStatus);

			  }
			 catch(Exception  m)
			  {
			  	userMailStatus = m.toString();
			  }

//end of sending mail


	%>



<br>
<br>
<br>
<br>
<br>
<br>
<br>

 <%
   if ( ! EJBUtil.isEmpty(userMailStatus) )
	{
  %>
	<p class = "redMessage" align = center> <%=MC.M_InviSent_FlwgReas %><%-- Your Invitation could not be sent
		  because of the following reason(s)*****--%>: </p>
	<br>
	<p class = "redMessage"  align = center>
	<ul>
    <li> <%=MC.M_EtrIncorrect_EmailAddr %><%-- You have entered an incorrect e-mail address*****--%> </li>
    <li> <%=MC.M_YouEtrEmail_NtSeprComma %><%-- If you have entered more than one e-mail address, they are not separated
      by a ',' (comma)*****--%></li>
    <li> <%=MC.M_ProbWith_MailServer %><%-- There is some problem with the mail server*****--%> </li>
    	<li><%=MC.M_CnctSupport_WithMsg %><%-- Please contact Customer Support with the following message*****--%>:<BR><%=userMailStatus%></li>
	</ul></p>

    <table width=100%>
    <tr>
    <td align=center width=50%>

    		<button onclick="window.history.back();"><%=LC.L_Back%></button>
    </td>
    </tr>
    </table>
  <%
  	}
	else
	{
  %>
	<p class = "sectionHeadings" align = center> <%=MC.M_YourInvitation_SentSucc %><%-- Your Invitation was sent successfully*****--%></p>
	<META HTTP-EQUIV=Refresh CONTENT="1; URL=myHome.jsp?srcmenu=<%=src%>">
  <%
  	} //message sent

	} //end of if for esign
	}else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
} //end of if for session
%>

</BODY>
</HTML>


