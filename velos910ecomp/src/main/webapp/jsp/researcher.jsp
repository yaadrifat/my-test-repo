<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>

<HEAD>

<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=windows-1252">

<TITLE><%=MC.M_Are_YouAResearcher%><%--Are You a Researcher?*****--%></TITLE>
<%@ page import="com.velos.eres.service.util.*"%>



</HEAD>



<SCRIPT>

	function openwin() {

      window.open("","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=500,height=400")

;}

</SCRIPT>



<BODY style = "overflow:auto">



<Link Rel=STYLESHEET HREF="common.css" type=text/css>



<DIV class="staticDefault">



<table width="80%" cellspacing="0" cellpadding="0" border=0 align = center>

<tr>

<td height="20">

</td>

</tr>

<tr>

<td width="30%">

</td>

<td width="70%">

<img src="../images/researcher.gif" width="400" height="200" align="center"></img>

</td>

</tr>

</table>

<br><br>

<DIV style="margin-left:50px;margin-top:-10px;position:absolute">

	<table width="80%" cellspacing="0" cellpadding="0" border=0 align = center>

	<tr>

	<td  id="researchHeadings"><%=MC.M_Reser_PartOfRes%><%--So you are a researcher or part of a research team*****--%>...

	</td>

	</tr>

	<tr>

	<td height="5">

	</td>

	</tr>

	<tr>

	<td id="researchHeadings" align="right">.....<%=MC.M_Why_ShouldYouReg%><%--why should you register?*****--%>

	</td>

	</tr>

	</table>

</DIV>

&nbsp;

&nbsp;

<br><br>

<table width="80%" cellspacing="0" cellpadding="0" border=0 align = center>

<tr>

<td height="30">

</td>

</tr>

<tr>

<td colspan="2">

<%=MC.M_MostSolFilings_DrugInfo%><%--Most solutions today focus on activities related to electronic submission of trial data to sponsors through regulatory filings. While such initiatives can accelerate drug approval, much of the ground to be gained from information technology lies in the*****--%>

</tr>



<tr>

<td id="researchHeadings" width="30%" align="center">

<%=MC.M_IsThere_NeedASol%><%--Is there a need for a solution?*****--%>

</td>

<td width="70%">

<%=MC.M_DataElectronic_Sbmt%><%--activities leading up to trial data collection and submission. These activities include <%=LC.Std_Study_Lower%> design, <%=LC.Pat_Patient_Lower%> recruitment, task/time management and facilitating investigator and IRB activities leading up to the posting of <%=LC.Pat_Patient_Lower%> results to a <%=LC.Std_Study_Lower%> database.  By far, this area of clinical trial activity represents the most elapsed time and inefficiency in clinical trials and correspondingly, the largest potential return on investment for researchers.  This is the focus of Velos.*****--%>

</td>

</tr>

</table>



<table width="80%" cellspacing="0" cellpadding="0" border=0 align = center>

<tr>

<td height="20">

</td>

</tr>

<tr>

<td colspan="2">

<%=MC.M_Spon_ExecClinicalTrail%><%--Clinical trials and research are executed for a variety of sponsors, and investigators are expected to use different products and processes to execute trials. This is inefficient and time-intensive, and also limits the value of source information and the*****--%>

</tr>

<tr>

<td width="70%">

<%=MC.M_PatPopSwSol_ConComOrg%><%--<%=LC.Pat_Patient_Lower%> population. An estimated $0.25 to $0.40 of every healthcare dollar is spent on excessive administrative costs, the performance of redundant tests and the delivery of unnecessary care. The increasing use of software products by the healthcare industry has not been able to provide an ideal solution, mainly because a lot of the data and information remains buried in non-integrated systems in different facilities across the country. Web-based connectivity applications provide a solution for this problem by facilitating communication and automating transactions that occur within and between healthcare organizations.*****--%>

</td>



<td id="researchHeadings" width="30%" align="center">

<%=LC.L_Why_Webbased%><%--Why Web-based?*****--%>

</td>

</tr>

</table>



<table width="80%" cellspacing="0" cellpadding="0" border=0 align = center>

<tr>

<td height="20">

</td>

</tr>

<tr>

<td id="researchHeadings" width="40%" align="center">

<%=LC.L_Why_VelosEres%><%--Why Velos eResearch?*****--%>

</td>



<td width="60%">

<%=MC.M_VelosClt_InfoSecurely%><%--Velos eResearch is based on the concept that there is a great need for a connectivity and data aggregating solution in the field of clinical research. The application offers a cost-effective means to bring together all the*****--%>

</td>

</tr>

<tr>

<td colspan="2">

<%=MC.M_ClinicComm_ColShrEnv%><%--participants in the clinical trial process, thus facilitating communication and providing a centralized platform for collecting and sharing data in a completely secure environment.*****--%>

</td>

</tr>

<tr>

</table>



<table width="80%" cellspacing="0" cellpadding="0" border=0 align = center>

<tr>

<td height="20">

</td>

</tr>

<tr>

<td width="45%">

<%=MC.M_VelosEresStd_MgmtStdEasy%><%--Velos eResearch is a complete clinical research management solution designed for research teams that accelerates the clinical <%=LC.Std_Study_Lower%> process, is easy to use*****--%>  </td>

<td id="researchHeadings" width="55%" align="center">

<%=MC.M_WhatVelosEres_Offer%><%--What Does Velos eResearch Offer?*****--%>

</td>

</tr>

<tr>

<td colspan="2">

<%=MC.M_ProdQltyErs_SpclConn%><%--and produces quality <%=LC.Std_Study_Lower%> results. This application provides researchers with centralized content and services designed especially for their needs to enhance productivity, efficiency and connectivity.*****--%>

</td>

</tr>





</table>



<table width="80%" cellspacing="0" cellpadding="0" border=0 align = center>

<tr>

<td height="20">

</td>

</tr>

<tr>

<td width="18%">

</td>

<td width="88%">

<img src="../images/userbenefits.gif" WIDTH=499 HEIGHT=372 align="center"></img>

</td>

</tr>

</table>



<table width="80%" cellspacing="0" cellpadding="0" border=0 align = center>

<tr>

<td height="20">

</td>

</tr>

<tr>

<td width="35%">

</td>

<td>

<%=MC.M_ToCreate_Acc%><%--To create an account*****--%>, <A href="register.jsp"><font size = 2><%=LC.L_Reg_Now%><%--Register Now*****--%></font></A>,

</td>

</tr>

<td colspan="2" align="center">

<%=LC.L_Or%><%--Or*****--%> <A href="contactus.jsp" target="Information" onClick="openwin()"> <font size = 2><%=MC.M_CnctUs_ForMoreInfoOnSrv%><%--Contact Us</font></A> for more information regarding our product and services*****--%>

</td>

</tr>

</table>



<div>

<jsp:include page="bottompanel.jsp" flush="true">

</jsp:include>

</div>





</DIV>



</BODY>



</HTML>

