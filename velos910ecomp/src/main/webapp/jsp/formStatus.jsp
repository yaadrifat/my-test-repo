<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Form_StatusDets%><%--Form Status Details*****--%></title>
<%@ page import="com.velos.eres.service.util.*,java.util.*,java.io.*" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<SCRIPT language="JavaScript1.1">

function openwin1() {
     windowName=window.open("usersearchdetails.jsp?fname=&lname=&from=formStatus","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
	windowName.focus();
;}

function  validate(formobj){

	
	preStatus = formobj.preStatus.value;
	activeId = formobj.activeId.value;
	offlineId = formobj.offlineId.value;
	deactivateId = formobj.deactivateId.value;
	lockdownId = formobj.lockdownId.value;
	wipId = formobj.wipId.value;
	migrationId = formobj.migrationId.value;
	
	var selIndex;
	
	selIndex =  formobj.formStat.selectedIndex;
	var status = formobj.formStat.options[selIndex].value;
	
	if(migrationId == status)
	{
		alert("<%=MC.M_CntSelStat_SetAppResp%>");/*alert("You cannot select this status. This status can be only set by the application for the purpose of form responses' migration.");*****/
		formobj.formStat.value = preStatus;		
		return false;
	}
	
	// When form status change from 'Active' 
	if(preStatus == activeId &&  status == wipId)
	{
		alert("<%=MC.M_CntChg_ActvToWorkInPgress%>");/*alert("You cannot change status from 'Active' to 'Work in progress'");*****/
		formobj.formStat.value = activeId;		
		return false;
	}
 
	
	// when form status change from offline for editing
	if(preStatus == offlineId && status == wipId)
	{
		alert("<%=MC.M_CntChgStat_WorkPgress%>");/*alert("You cannot change status from 'Offline for Editing' to 'Work in Progress'");*****/
		formobj.formStat.value = offlineId;	
		return false;
	}


	if(preStatus == offlineId && status == deactivateId)
	{
		alert("<%=MC.M_CntChg_OfflineToDeac%>");/*alert("You cannot change status from 'Offline for Editing' to 'Deactivated'");*****/
		formobj.formStat.value = offlineId;	
		return false;
	}
	

	if(preStatus == offlineId && status == lockdownId)
	{
		alert("<%=MC.M_CntChg_OfflineToLkdwn%>");/*alert("You cannot change status from 'Offline for Editing' to 'Lockdown'");*****/
		formobj.formStat.value = offlineId;	
		return false;
	}

// when form status change from lockdown
	if(preStatus == lockdownId && status == wipId)
	{
		alert("<%=MC.M_CntChg_LkdwnToWorkInPgress%>");/*alert("You cannot change status from 'Lockdown' to 'Work in Progress'");*****/
		formobj.formStat.value = lockdownId;	
		return false;
	}


	if(preStatus == lockdownId && status == deactivateId)
	{
		alert("<%=MC.M_CntChgStat_FromLkdwn%>");/*alert("You cannot change status from 'Lockdown' to 'Deactivated'");*****/
		formobj.formStat.value = lockdownId;	
		return false;
	}
	

	if(preStatus == lockdownId && status == offlineId)
	{
		alert("<%=MC.M_CntChg_FromLkdwnToOffline%>");/*alert("You cannot change status from 'Lockdown' to 'Offline for Editing'");*****/
		formobj.formStat.value = lockdownId;	
		return false;
	}

  if(preStatus == wipId && status == offlineId)
		 {
		alert("<%=MC.M_CntChg_OfflineEdit%>");/*alert("You cannot change status from 'Work in Progress' to 'Offline for Editing'");*****/
		formobj.formStat.value = wipId;	
		return false;
		 }

	if(preStatus == wipId && status == lockdownId)
		 {
		   alert("<%=MC.M_CntChg_WorkInPgressToLkdwn%>");/*alert("You cannot change status from 'Work in Progress' to 'Lockdown'");*****/
		   formobj.formStat.value = wipId;	
		return false;
		 }
 
	

	if (!(validate_col('Form Status',formobj.formStat))) return false
	if (!(validate_col('Form Start Date',formobj.startDate))) return false
	if (!(validate_col('Changed By',formobj.changedBy))) return false	
	if (!(validate_col('e-Signature',formobj.eSign))) return false	
	
	if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
	}
}
</SCRIPT>
<!-- Virendra: Fixed Bug no. 4737, added import com.velos.eres.service.util.StringUtil  -->
<%@ page language = "java" import = "com.velos.esch.business.common.EventAssocDao,com.velos.eres.business.section.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.StringUtil"%>
<jsp:useBean id="formStatB" scope="request" class="com.velos.eres.web.formStat.FormStatJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="formLibB" scope="request" class="com.velos.eres.web.formLib.FormLibJB"/>
<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB"/>


<%	String src;
	src= request.getParameter("srcmenu");
	String tab = request.getParameter("selectedTab");
	String from = request.getParameter("from");
	//Virendra: Fixed Bug no. 4737, added variable addAnotherWidth  -->
	int addAnotherWidth = 0;
%>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="srcmenu" value="<%=src%>"/> 
</jsp:include>

<body>
<br>

<DIV class="formDefault"  id="div1">

<%
   HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession))
	{   
   	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
	//int study_not_associated_right = Integer.parseInt(grpRights.getFtrRightsByValue("MACCFRMSNOSTUDY"));
	//int study_associated_right =Integer.parseInt(grpRights.getFtrRightsByValue("MACCFRMSWSTUDY"));
	//int account_form_rights = Integer.parseInt(grpRights.getFtrRightsByValue("MACCFRMS"));
	
	///////
		String lnkfrom="";
		lnkfrom = request.getParameter("lnkfrom");

		if(lnkfrom==null)
		{lnkfrom="-";}				
    	
		//int teamRight=0;
		//int account_form_rights =0;
		int pageRight = 0;
		String userIdFromSession = (String) tSession.getValue("userId");
		int studyId=EJBUtil.stringToNum(request.getParameter("studyId"));

	int flag=0;
	if( from.equals("A")){
	 	    
			if (lnkfrom.equals("S")) 
	 	    {  
				
				ArrayList tId ;
				tId = new ArrayList();
				if(studyId >0)
				{
    				TeamDao teamDao = new TeamDao();
    				teamDao.getTeamRights(studyId,EJBUtil.stringToNum(userIdFromSession));
    				tId = teamDao.getTeamIds();
			
					if (tId.size() <=0)
					{
						pageRight = 0;
					} else {
						StudyRightsJB stdRights = new StudyRightsJB();
						stdRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));
						
						ArrayList teamRights ;
						teamRights  = new ArrayList();
						teamRights = teamDao.getTeamRights();
							 
						stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
						stdRights.loadStudyRights();
						 
						 		
						if ((stdRights.getFtrRights().size()) == 0){					
						 pageRight = 0;		
						} else {								
						pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMMANAG"));
					}
				}
			}    
				
		  }
	 		else
	        {	
				pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MACCFRMS"));	  				

			 }
	  }
	
	 if( from.equals("L")) //form library
	 	{
			pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MFRMLIB"));
	 	}
		
		if( from.equals("S")) //study
	 	{
			StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");	
	  		pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMMANAG"));
		}
		
	
	/////////////////

	
  	
   	if (pageRight >= 4 ) {
	   String formId = request.getParameter("formId");
	   formLibB.setFormLibId(EJBUtil.stringToNum(formId));
	   formLibB.getFormLibDetails();
	   String formName = formLibB.getFormLibName();
			   
       String formStatus ="";
	   String codeLst="";
       String stDate="";
	   int formStatId = 0;
       
	   String notes="";
	   String changedBy="";
       String  dFormStatus ="";
       CodeDao cd = new CodeDao();
       cd.getCodeValues("frmstat");
	   String changedById=""; 
	   String codeStatus="";
	     %>
	    <% 			formStatId=EJBUtil.stringToNum(request.getParameter("formStatId"));
					
					formStatB.setFormStatId(formStatId);
					formStatB.getFormStatDetails();
					changedById = formStatB.getChangedBy();
					userB.setUserId(EJBUtil.stringToNum(changedById));
					userB.getUserDetails();
 		   			changedBy = userB.getUserFirstName()+ " " +userB.getUserLastName();
							    
					codeLst=formStatB.getCodeLst();
					 stDate=formStatB.getStDate();
				     notes=formStatB.getNotes();
					 if (notes==null) notes="";		
            	
					int frmstatus = EJBUtil.stringToNum(formStatB.getCodeLst());
					dFormStatus = cd.toPullDown("formStat",frmstatus); 
		
					int activeId = cd.getCodeId("frmstat","A");
					int offlineId = cd.getCodeId("frmstat","O");
					int deactivateId = cd.getCodeId("frmstat","D");
					int lockdownId = cd.getCodeId("frmstat","L");
					int wipId = cd.getCodeId("frmstat","W");
					
					int migrationId = cd.getCodeId("frmstat","M");
					
					if(activeId==frmstatus)
					{
					codeStatus = "Active";
					}
					if(deactivateId==frmstatus)
					{
					codeStatus = "Deactivated";
					}
					
					if(migrationId==frmstatus)
					{
					codeStatus = "migration";
					}
					if(offlineId==frmstatus)
					{
					codeStatus = "Offline";
					}
					
			  %>		
	   
       <Form name="formStatus" id="formStatId" method="post" action="updateFormStatus.jsp" onSubmit = "if (validate(document.formStatus)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	   <input type="hidden" name="formId" value="<%=formId%>"> 
	   <input type="hidden" name="changedById" value="<%=changedById%>"> 
	   <input type="hidden" name="formStatId" value="<%=formStatId%>">
	   <input type="hidden" name="srcmenu" value="<%=src%>">
	   <input type="hidden" name="selectedTab" value="<%=tab%>">
	   <input type="hidden" name="from" value="<%=from%>">
	   <input type="hidden" name="formStatusCdLst" value="<%=codeLst%>">			   	 
		
      <%
       if (from.equals("S")) {
      %>
      <p class="sectionHeadings"> <%=MC.M_MngPcolsAssoc_FrmStat%><%--Manage Protocols >> Associated Forms >> Form Status*****--%> </p>
      <% }else {%>	
      <p class="sectionHeadings"> <%=MC.M_MngAcc_FrmMgmtStat%><%--Manage Account >> Form Management >> Form Status*****--%> </p>
      <%}%>
	
	  <P><%=LC.L_Frm_Name%><%--Form Name*****--%> : <%=formName%></P> 
		   
       <%if(codeStatus.equals("Deactivated")){%> 
	   <P class = "defComments"><FONT class="Mandatory"><%=MC.M_AnyChgNotSvd_DeacFrm%><%--Any changes made will not be saved for a Deactivated form*****--%> </FONT></P>
		   <%}%>
	   <P class = "defComments"><%=MC.M_Etr_StatusDets%><%--Please enter status details*****--%>:</P>
       
       <table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl midAlign" >
       <tr><td height=7 colspan="2"></td></tr>  
         <tr height = 20> 
           <td width="300">
              <%=LC.L_Form_Status%><%--Form Status*****--%><FONT class="Mandatory">* </FONT>
           </td>
        	<td>
               <%=dFormStatus%>
           </td>
          
       	<tr><td height=10 colspan="2"></td></tr>  
         <tr> 
           <td width="300">
              <%=LC.L_Form_StartDate%><%--Form Start Date*****--%> <FONT class="Mandatory">* </FONT>
           </td>
<%-- INF-20084 Datepicker-- AGodara --%>
           <td><input type="text" name="startDate" class="datefield" size = 15 MAXLENGTH = 20 value='<%=stDate%>' READONLY ></td>
         </tr>
        <tr><td height=10  colspan="2"></td></tr>
        <tr> 
           <td width="300">
           <%=LC.L_Changed_By%><%--Changed By*****--%> <FONT class="Mandatory">* </FONT>
           </td>
           <td>
       
       	<input type=text name='changedBy' value="<%=changedBy%>" size='30' READONLY>
       	<A HREF=# onClick=openwin1() ><%=LC.L_Select_User%><%--Select User*****--%></A>
           </td>
         </tr>			
       	<tr><td height=10 colspan="2"></td></tr>  
        <tr> 
           <td width="300">
              <%=LC.L_Notes%><%--Notes*****--%>
           </td>
           <td>
       	<TextArea name="notes" rows=3 cols=40 MAXLENGTH =2000><%=notes%></TextArea>
           </td>
         </tr>
         <tr><td height=7 colspan="2"></td></tr>  
       	   </table>		
       <br>
       
       <table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl midAlign">

       <%
	     
	   if(!codeStatus.equals("Deactivated")){
		   if(pageRight == 4 || pageRight == 5){
		   /*if((from.equals("A")||from.equals("S")) && ( ( lnkfrom.equals("S") && (teamRight==4 || teamRight==5 || pageRight==4 || pageRight==5 )) || (lnkfrom.equals("A") && (account_form_rights==4 || account_form_rights==5)))) {*/%>
			   <tr>
			<td bgcolor="<%=StringUtil.eSignBgcolor%>">
			<!-- Akshi: Fixed Bug no. 7540 -->
			<input class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-button-text" type="button" name="<%=LC.L_Back%>" value="<%=LC.L_Back%>" onclick="window.history.back(); return false;"></input>
				</td>
				</tr><%}
			else{%>	   
		
			<%	if (codeStatus.equals("Offline")) 
		{ %>
		
				<tr><td colspan=3><input type="checkbox" name="migrateForm" value="1"><%=MC.M_MigrateResp_StatActive%><%--Migrate the existing form responses to the latest version (applicable when you change status to 'Active')*****--%>
				<P class="defComments"><FONT class="Mandatory"><%=MC.M_ChangedVal_ChoiceVersion%><%--If you changed 'Data Value(s)' in any Multiple Choice field, please migrate the existing form responses to the latest version*****--%></FONT></P>
				</td></tr>
				
		
		<% } else
		{
			%>
				<input type="hidden" name="migrateForm" value="0">
			
			<%
		}%>
	 
			
		
		<% 
		 String showSubmit = "";
		 if (codeStatus.equals("migration"))		{ 
			showSubmit = "N";
		 }
		%>
		 <tr>
			<table width=100% cellspacing="0" cellpadding="0">
				 <tr>
					<td bgcolor="<%=StringUtil.eSignBgcolor%>">
						<!-- Akshi: Fixed Bug no. 7540 -->
						<input class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-button-text" type="button" name="<%=LC.L_Back%>" value="<%=LC.L_Back%>" onclick="window.history.back(); return false;"></input>
					</td>
					<!-- Virendra: Fixed Bug no. 4737, added td with addAnotherWidth  -->
					<td bgcolor="<%=StringUtil.eSignBgcolor%>" valign="baseline" align="left" width="85%" <%=addAnotherWidth > 0 ? "" : "colspan=5" %>>
						<jsp:include page="submitBar.jsp" flush="true"> 
							<jsp:param name="displayESign" value="Y"/>
							<jsp:param name="formID" value="formStatId"/>
							<jsp:param name="showDiscard" value="N"/>
							<jsp:param name="showSubmit" value="<%=showSubmit%>"/>
							<jsp:param name="noBR" value="Y"/>
						</jsp:include>
					</td>
				</tr>
			</table>
		</tr>
			<%}
	   }else{%>
		 <tr>
			<td bgcolor="<%=StringUtil.eSignBgcolor%>">
				<!-- Akshi: Fixed Bug no. 7540 -->
				<input class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-button-text" type="button" name="<%=LC.L_Back%>" value="<%=LC.L_Back%>" onclick="window.history.back(); return false;"></input>
			</td>
			
		</tr> 
		<%}%>


       </table>
       
       <% if ( codeStatus.equals("migration"))
		{%>
			<p class = "redMessage"><%=MC.M_AppMigrating_FrmResps%><%--The application is migrating the form's responses to the latest Version. The form cannot be edited at this point.*****--%></p>
			  
	<% } %>
       
					<input type="hidden" name="activeId" value=<%=activeId%>>
					<input type="hidden" name="offlineId" value=<%=offlineId%>>
					<input type="hidden" name="deactivateId" value=<%=deactivateId%>>
					<input type="hidden" name="lockdownId" value=<%=lockdownId%>>
					<input type="hidden" name="wipId" value=<%=wipId%>>
					<input type="hidden" name="preStatus" value=<%=frmstatus%>>
					<input type="hidden" name="migrationId" value=<%=migrationId%>>

       </Form>
<%	
	} //end of page right
	else 
	{ %>
		<jsp:include page="accessdenied.jsp" flush="true"/>
	<%
	}
		
}//end of if body for session
   	else
	{
%>

	<jsp:include page="timeout.html" flush="true"/>
	<%
	}
%>
	<div style="position:relative;">
	    <jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</div>

<div class ="mainMenu" id="emenu"> 
  	<jsp:include page="getmenu.jsp" flush="true"/>
</div>

</body>
</html>

