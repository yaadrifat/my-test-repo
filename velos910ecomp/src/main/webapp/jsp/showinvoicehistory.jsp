<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
	<title><%=MC.M_Inv_StatHist%><%--Invoice >> Status History*****--%></title>
	<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*" %>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<%
	String src;
	src= request.getParameter("srcmenu");
%>
<jsp:include page="panel.jsp" flush="true">
	<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<SCRIPT language="javascript"></SCRIPT>
<body>
<!-- Bug NO: 3976 fixed by PK -->
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"> </div> 
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<br>
<%
	HttpSession tSession = request.getSession(true);
	String tab = request.getParameter("selectedTab");
	String studyId=request.getParameter("studyId");
	String from = request.getParameter("from");
	String currentStatId = request.getParameter("currentStatId");
%>

<DIV class="BrowserTopn" id = "div1">
    
	 <jsp:include page="milestonetabs.jsp" flush="true">
		<jsp:param name="studyId" value="<%=studyId%>"/>
		<jsp:param name="from" value="<%=from%>"/>
		<jsp:param name="selectedTab" value="<%=tab%>"/>
	  </jsp:include>
	</div>

<DIV class="BrowserBotN BrowserBotN_SiH_1" id="div1"> 
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<%@ page language = "java" import = "com.velos.eres.business.ulink.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%>
<%
	if (sessionmaint.isValidSession(tSession)){

		String modulePk = request.getParameter("modulePk");
		String fromjsp = request.getParameter("fromjsp");
		String invNumber = request.getParameter("invNumber");
		String pageRight = "";
		pageRight = request.getParameter("pageRight");
                if(pageRight == null)
                   pageRight ="0";
		
		int pgRightInt = EJBUtil.stringToNum(pageRight) ;
		
		String showDel = "";
		
		if (pgRightInt < 6)
		{
			showDel = "false";
		}
		else
		{
			showDel = "true";
		}
	 
		

 	        if (pgRightInt  >= 0 ){


%>
			<jsp:include page="statusHistory.jsp" flush="true">
				<jsp:param name="studyId" value="<%=studyId%>"/>
				<jsp:param name="modulePk" value="<%=modulePk%>"/>
				<jsp:param name="moduleTable" value="er_invoice"/>
				<jsp:param name="srcmenu" value="<%=src%>"/>
				<jsp:param name="selectedTab" value="<%=tab%>"/>
				<jsp:param name="fromjsp" value="<%=fromjsp%>"/>
				<jsp:param name="pageRight" value="<%=pageRight%>"/>
				<jsp:param name="invNumber" value="<%=StringUtil.encodeString(invNumber)%>"/>
				<jsp:param name="currentStatId" value="<%=currentStatId%>"/>
				<jsp:param name="showDeleteLink" value="<%=showDel%>"/>
				<jsp:param name="showEditLink" value="false"/>

			</jsp:include>
<%		} //end of if body for page right
		else {
%>
			<jsp:include page="accessdenied.jsp" flush="true"/>
<%
		}//end of else body for page right
	}//end of if body for session
		else{
%>
			<jsp:include page="timeout.html" flush="true"/>

<%	}
%>
	<div>
    	<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</div>
	<div class ="mainMenu" id = "emenu" >
		<jsp:include page="getmenu.jsp" flush="true"/>
	</div>
</body>

</html>

