<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<%@ page import="java.util.*,com.velos.eres.service.util.StringUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.EJBUtil" %><%@page import="com.velos.eres.service.util.LC"%>
<%@ page import="com.velos.eres.web.objectSettings.ObjectSettingsCache, com.velos.eres.web.objectSettings.ObjectSettings"%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>


<%!
  private String getHrefBySubtype(String subtype) {
      if (subtype == null) { return "#"; }
      if ("1".equals(subtype)) {
          return "specimenbrowser.jsp?srcmenu=tdmenubaritem6&selectedTab=1";
      }
      if ("2".equals(subtype)) {
          return "storageadminbrowser.jsp?srcmenu=tdmenubaritem6&selectedTab=2";
      }
      if ("3".equals(subtype)) {
          return "storagekitbrowser.jsp?srcmenu=tdmenubaritem6&selectedTab=3";
      }
      if ("4".equals(subtype)) {
          return "preparationAreaBrowser.jsp?srcmenu=tdmenubaritem6&selectedTab=4";
      }

      return "#";
  }
%>




<%
String mode="M";
String selclass;
String userId="";
String acc="";
String tab= request.getParameter("selectedTab");
String specimenPk = "";

String perId = "", stdId = "";

HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession))
{

 	 int pageRightStorage = 0;
 	 int pageRightSpecimen = 0;

	 GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");

	 pageRightStorage = Integer.parseInt(grpRights.getFtrRightsByValue("MSTORAGE"));
	 pageRightSpecimen = Integer.parseInt(grpRights.getFtrRightsByValue("MSPEC"));

	acc = (String) tSession.getValue("accountId");
	userId = (String) tSession.getValue("userId");

	specimenPk = request.getParameter("specimenPk");

	//JM: 07Jul2009: #INVP2.11
	perId = request.getParameter("pkey");
 	perId = (perId==null)?"":perId;

	stdId = request.getParameter("studyId");
	stdId = (stdId==null)?"":stdId;



%>
<!-- <table  class= "selectedTab" cellspacing="0" cellpadding="0" border="0">  -->
<table  cellspacing="0" cellpadding="0" border="0">
	<tr>

	<%
			ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
			ArrayList tabList = objCache.getAccountObjects(EJBUtil.stringToNum(acc), "inv_tab");

			for (int iX=0; iX<tabList.size(); iX++) {
            ObjectSettings settings = (ObjectSettings)tabList.get(iX);

    		if ("0".equals(settings.getObjVisible())) {
    		    continue;
    		}

            boolean showThisTab = false;

			if ("1".equals(settings.getObjSubType())) {
    		    if (pageRightSpecimen >= 4 )
    		    {
    		        showThisTab = true;
    		    }

    		}

			else if ("2".equals(settings.getObjSubType())) {
    		    if (pageRightStorage >= 4 )
    		    {
    		        showThisTab = true;
    		    }

    		}else if ("3".equals(settings.getObjSubType())) {
    		    //if (pageRightSpecimen >= 4 )
    		    //{
    		        showThisTab = true;
    		    //}

    		}
    		else if ("4".equals(settings.getObjSubType())) {    		 
				//KM-5788 and 5789
				if (pageRightSpecimen > 4 )
    		    {
    		        showThisTab = true;    	
				}
    		}
			else {
    		    showThisTab = true;
    		}

            if (!showThisTab) { continue; }

			if (tab == null) {
                selclass = "unselectedTab";
    		} else if (tab.equals(settings.getObjSubType())) {
                selclass = "selectedTab";
            } else {
                selclass = "unselectedTab";
            }

		%>
		<td  valign="TOP">
		<table class="<%=selclass%>"  cellspacing="0" cellpadding="0" border="0">
				<tr>
				<!-- <td rowspan="3" valign="top" wclassth="7">
						<img src="../images/leftgreytab.gif" width=8 height=20 border=0 alt="">
					</td> -->
					<td>
						<a href="<%=getHrefBySubtype(settings.getObjSubType())%>"><%=settings.getObjDispTxt()%></a>
					</td>
			     <!--    <td rowspan="3" valign="top" wclassth="7">
						<img src="../images/rightgreytab.gif" wclassth=7 height=20 border=0 alt="">
			        </td> -->
			   	</tr>
		</table>
		 </td>

		<%
        } // End of tabList loop
      %>

	</tr>
 <!--    <tr>
	     <td colspan=4 height=10></td><% // colspan here corresponds to the number of tabs %>
	</tr>  -->
</table>
<table class="tabBorder" width="99%" height="5"><tr><td></td></tr></table>


	<%
	if (tab.equals("1")&& pageRightSpecimen >= 4 && (! StringUtil.isEmpty(specimenPk)) )
	    	{
	    	%>
	<table width="99%" cellspacing="0" cellpadding="0">

	<tr align="center" class="speca">
		<td>

		<A href="specimendetails.jsp?srcmenu=tdmenubaritem6&selectedTab=1&mode=M&pkId=<%=specimenPk%>&pkey=<%=perId%>&studyId=<%=stdId%>" ><b><%=LC.L_Spmen_DetOrStat%><%--Specimen Details/ Status*****--%></b></A>
		&nbsp;&nbsp;&nbsp;&nbsp;

		<A href="specimenapndx.jsp?srcmenu=tdmenubaritem6&selectedTab=1&mode=M&pkId=<%=specimenPk%>&pkey=<%=perId%>&studyId=<%=stdId%>"><b><%=LC.L_Appendix%><%--Appendix*****--%></b></A>

		<!-- &nbsp;&nbsp;&nbsp;&nbsp;
		<A href="#" ><b>Access Rights</b></A> -->


		&nbsp;&nbsp;&nbsp;&nbsp;
		<A href="specimenForms.jsp?srcmenu=tdmenubaritem6&selectedTab=1&mode=M&specimenPk=<%=specimenPk%>&pkey=<%=perId%>&studyId=<%=stdId%>"><b><%=LC.L_Forms%><%--Forms*****--%></b></A>

		&nbsp;&nbsp;&nbsp;&nbsp;
		<A href="specimenlabbrowser.jsp?srcmenu=tdmenubaritem6&selectedTab=1&mode=M&specimenPk=<%=specimenPk%>&calledFromPg=specimen&pkey=<%=perId%>&studyId=<%=stdId%>&page=specimen"><b><%=LC.L_Labs%><%--Labs*****--%></b></A>


		</td>
	</tr>
	</table>



<%
	} //if first tab is selected
 } %>