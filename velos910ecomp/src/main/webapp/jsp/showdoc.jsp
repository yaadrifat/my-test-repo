<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title><%=LC.L_Show_Docu%><%--show document*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<Link Rel=STYLESHEET HREF="common.css" type=text/css>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,javax.servlet.ServletOutputStream"%>
<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>
<jsp:useBean id="eventdocB" scope="request" class="com.velos.esch.web.eventdoc.EventdocJB"/>


<body>
<DIV class="formDefault" id="div1">
<%

	String docId = request.getParameter("docId");

	HttpSession tSession = request.getSession(true);

	if (sessionmaint.isValidSession(tSession))
	{
		eventdocB.setDocId(docId);
		eventdocB.getEventdocDetails();
		byte[] mybyte=eventdocB.getDocValue();

		%>

		if(eventmode.equals("M")) {
		   eventId = request.getParameter("eventId");
		   if (fromPage.equals("eventbrowser") || (fromPage.equals("fetchProt") && 			calledFrom.equals("P"))){
  		  	eventdefB.setEvent_id(EJBUtil.stringToNum(eventId));
			eventdefB.getEventdefDetails();
		      eventName = eventdefB.getName();
   		   	eventDesc = eventdefB.getDescription();
		      eventDuration = eventdefB.getDuration();
	   	      eventFuzzyPerDays = eventdefB.getFuzzy_period();
  	   	      eventNotes = eventdefB.getNotes();
		      eventMsgTo = eventdefB.getMsg_to();
		   }else{
  		  	eventassocB.setEvent_id(EJBUtil.stringToNum(eventId));
			eventassocB.getEventAssocDetails();
		      eventName = eventassocB.getName();
   		   	eventDesc = eventassocB.getDescription();
		      eventDuration = eventassocB.getDuration();
	   	      eventFuzzyPerDays = eventassocB.getFuzzy_period();
  	   	      eventNotes = eventassocB.getNotes();
		      eventMsgTo = eventassocB.getMsg_to();
		   }
		   if(eventFuzzyPerDays.equals("0")){
			eventFuzzyPer = "0";
		   } else {
			eventFuzzyPer = "1";
			if(eventFuzzyPerDays.substring(0,1).equals("+")) {
			   eventFuzzyPerBA = "+";
			} else {
			   eventFuzzyPerBA = "-";
			}
			   eventFuzzyPerDays = eventFuzzyPerDays.substring(1,eventFuzzyPerDays.length());
		   }
		   if(eventMsgTo == null || eventMsgTo.equals("") || eventMsgTo.equals("0")) {
			eventMsgFlag = "0";
		   } else {
			eventMsgFlag = "1";
			if (fromPage.equals("eventbrowser") || (fromPage.equals("fetchProt") && calledFrom.equals("P"))){
			   eventMsg = eventdefB.getEvent_msg();
			} else {
			   eventMsg = eventassocB.getEvent_msg();
			}
		   }

		} else {
		   categoryId = request.getParameter("categoryId");
		}
%>

<%
if(eventmode.equals("N")) {
%>
   <form METHOD=POST action=eventsave.jsp?srcmenu=<%=src%>&eventmode=<%=eventmode%>&categoryId=<%=categoryId%>&eventId=<%=eventId%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>>
<%
} else {
%>
   <form METHOD=POST action=eventsave.jsp?srcmenu=<%=src%>&eventmode=<%=eventmode%>&eventId=<%=eventId%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>>
<%
}
%>

<TABLE width="100%" cellspacing="0" cellpadding="0" >
   <tr>
	<td width="40%"> <%=MC.M_NameOf_TheEvt%><%--Name of the event*****--%> </td>
	<td width="60%">
	   <INPUT NAME="eventName" value="<%=eventName%>" TYPE=TEXT SIZE=30 MAXLENGTH=50 >
	</td>
   </tr>
   <tr>
	<td width="40%"> Description </td>
	<td width="60%"> <INPUT NAME="eventDesc" value="<%=eventDesc%>" TYPE=TEXT SIZE=30 MAXLENGTH=200> </td>
   </tr>
   <tr>
	<td width="40%"> <%=MC.M_Duration_OfEvt%><%--Duration of the event*****--%> </td>
	<td width="60%"> <INPUT NAME="eventDuration" value="<%=eventDuration%>" TYPE=TEXT SIZE=5>
	   <select name="eventDurDays">
		<option value=Days><%=LC.L_Days%><%--Days*****--%></option>
		<option value=Weeks><%=LC.L_Weeks%><%--Weeks*****--%></option>
	   </select>
	</td>
   </tr>
   <tr>
	<td width="40%"> <%=LC.L_Fuzzy_Period%><%--Fuzzy Period*****--%> </td>
	<td width="60%">
<%
	if(eventFuzzyPer.equals("0")) {
%>
	   <INPUT NAME="eventFuzzyPer" TYPE= radio value="0" checked><%=LC.L_No%><%--No*****--%>
	   <INPUT NAME="eventFuzzyPer" TYPE= radio value="1"><%=LC.L_Yes%><%--Yes*****--%>
<%
	} else {

%>
	   <INPUT NAME="eventFuzzyPer" TYPE= radio value="0"><%=LC.L_No%><%--No*****--%>
	   <INPUT NAME="eventFuzzyPer" TYPE= radio value="1" checked><%=LC.L_Yes%><%--Yes*****--%>
<%
	}

%>
	</td>
   </tr>
   <tr>
	<td width="40%"> <P class = "defComments">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <%=MC.M_IfYes_Specify%><%--If yes, please specify*****--%> </P></td>
	<td width="60%"> <INPUT NAME="eventFuzzyPerDays" value="<%=eventFuzzyPerDays%>" TYPE=TEXT SIZE=5> <%=LC.L_Days_Lower%><%--days*****--%>
	   <select name="eventFuzzyPerBA">
<%
	if(eventFuzzyPerBA.equals("-")) {
%>
	   <option value=Before selected><%=LC.L_Before%><%--Before*****--%></option>
	   <option value=After><%=LC.L_After%><%--After*****--%></option>
<%
	} else {

%>
	   <option value=Before><%=LC.L_Before%><%--Before*****--%></option>
	   <option value=After selected><%=LC.L_After%><%--After*****--%></option>
<%
	}

%>

	   </select>
	</td>
   </tr>

   <tr>
	<td width="40%"> <%=LC.L_Notes%><%--Notes*****--%> </td>
	<td width="60%"> <TEXTAREA name="eventNotes"  rows=3 cols=30><%=eventNotes%></TEXTAREA> </td>

   </tr>
   <tr>
	<td width="40%"> <%=MC.M_MsgAssoc_ThisEvt%><%--Message associated with this event*****--%> </td>
	<td width="60%">
<%
	if(eventMsgFlag.equals("0")) {
%>
	   <INPUT NAME="eventMsgFlag" TYPE= radio checked><%=LC.L_No%><%--No*****--%>
	   <INPUT NAME="eventMsgFlag" TYPE= radio ><%=LC.L_Yes%><%--Yes*****--%>
<%
	} else {

%>
	   <INPUT NAME="eventMsgFlag" TYPE= radio><%=LC.L_No%><%--No*****--%>
	   <INPUT NAME="eventMsgFlag" TYPE= radio checked><%=LC.L_Yes%><%--Yes*****--%>
<%
	}

%>


	</td>
   </tr>

   <tr>
	<td width="40%"> <P class = "defComments">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <%=MC.M_IfYes_EtrMsgToSent%><%--If yes, enter the email message to be sent*****--%> </P></td>
	<td width="60%"> <TEXTAREA name="eventMsg" rows=4 cols=40><%=eventMsg%></TEXTAREA> </td>
   </tr>

   <tr>
   	<td width="40%"> <%=MC.M_SelWhoRecv_Msg%><%--Select individuals who are to receive this message*****--%> </td>
	<td width="60%">
<%
	if(!(eventMsgTo == null) && eventMsgTo.equals("P") ) {
%>
	   <INPUT NAME="eventMsgTo" TYPE= radio VALUE=P checked><%=LC.L_Patient%><%--<%=LC.Pat_Patients%>*****--%>
	   <BR>
	   <INPUT NAME="eventMsgTo" TYPE= radio VALUE=U><%=LC.L_Selected_Users%><%--Selected Users*****--%>
	   <BR>
   	   <INPUT NAME="eventMsgTo" TYPE= radio VALUE=B><%=MC.M_PatSel_Usr%><%--<%=LC.Pat_Patients%> and selected users*****--%>
<%
	} else if(!(eventMsgTo == null) && eventMsgTo.equals("U") ) {
%>
	   <INPUT NAME="eventMsgTo" TYPE= radio VALUE=P><%=LC.L_Patient%><%--<%=LC.Pat_Patients%>*****--%>
	   <BR>
	   <INPUT NAME="eventMsgTo" TYPE= radio VALUE=U checked><%=LC.L_Selected_Users%><%--Selected Users*****--%>
	   <BR>
   	   <INPUT NAME="eventMsgTo" TYPE= radio VALUE=B><%=MC.M_PatSel_Usr%><%--<%=LC.Pat_Patients%> and selected users*****--%>
<%
	} else {
%>
	   <INPUT NAME="eventMsgTo" TYPE= radio VALUE=P><%=LC.L_Patient%><%--<%=LC.Pat_Patients%>*****--%>
	   <BR>
	   <INPUT NAME="eventMsgTo" TYPE= radio VALUE=U><%=LC.L_Selected_Users%><%--Selected Users*****--%>
	   <BR>
   	   <INPUT NAME="eventMsgTo" TYPE= radio VALUE=B checked><%=MC.M_PatSel_Usr%><%--<%=LC.Pat_Patients%> and selected users*****--%>
<%
	}
%>
	</td>
<BR>
   </tr>


<tr>
<td colspan=2 align=center>
<button type="submit"><%=LC.L_Next%></button>
</td>
</tr>


</table>

</form>



<%

} else {  //else of if body for session

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>

  <div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</div>
<div class ="mainMenu" id = "emenu">
<jsp:include page="menus.htm" flush="true"/>
</div>
</body>

</html>


