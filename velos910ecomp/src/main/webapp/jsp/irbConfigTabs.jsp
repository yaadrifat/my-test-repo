<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ page import="java.util.*,com.velos.eres.service.util.StringUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.EJBUtil" %><%@page import="com.velos.eres.service.util.LC"%>
<%@ page import="com.velos.eres.web.objectSettings.ObjectSettingsCache, com.velos.eres.web.objectSettings.ObjectSettings"%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%!
	private String getHrefBySubtype(String subtype)
	{
      if (subtype == null) { return "#"; }
	  
      if ("1".equals(subtype)) {
          return "irbBoardSchedule.jsp?srcmenu=tdmenubaritem6&selectedTab=1";
      }
      if ("2".equals(subtype)) {
          return "irbConfigForms.jsp?srcmenu=tdmenubaritem6&selectedTab=2";
      }
      if ("3".equals(subtype)) {
          return "irbGroupAcess.jsp?srcmenu=tdmenubaritem6&selectedTab=3";
      }
      if ("4".equals(subtype)) {
          return "irbReviewBoard.jsp?srcmenu=tdmenubaritem6&selectedTab=4";
      }
      return "#";
  	}
%>

<%
String mode="M";
String selclass;
String userId="";
String acc="";
String tab= request.getParameter("selectedTab");
HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession))
{
	GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");
	acc = (String) tSession.getValue("accountId");
	userId = (String) tSession.getValue("userId");
%>

<table  cellspacing="0" cellpadding="0" border="0">
<tr>
	<%
		ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
		ArrayList tabList = objCache.getAccountObjects(EJBUtil.stringToNum(acc), "irb_config");

		for (int iX=0; iX<tabList.size(); iX++) 
		{
           	ObjectSettings settings = (ObjectSettings)tabList.get(iX);
           	
    		if ("0".equals(settings.getObjVisible())) {
    		   	continue;
    		}

	        boolean showThisTab = false;
			if ("1".equals(tab)) {
    		    showThisTab = true;
    		}
			else if ("2".equals(tab)) {
    		    showThisTab = true;
	    	}else if ("3".equals(tab)) {
    		    showThisTab = true;
    		}
    		else if ("4".equals(tab)) { 
    		   	showThisTab = true; 
	    	}
			else {
    		    showThisTab = true;
    		}
			
	        if (!showThisTab) { continue; }
			if (tab == null) {
				selclass = "unselectedTab";
	    	} else if (String.valueOf(iX+1).equals(tab)) {
    	        selclass = "selectedTab";
        	} else {
                selclass = "unselectedTab";
            }
	%>
		<td  valign="TOP">
		<table class="<%=selclass%>"  cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td>
						<a href="<%=getHrefBySubtype(String.valueOf(iX+1))%>"><%=settings.getObjDispTxt()%></a>
					</td>
			   	</tr>
		</table>
		</td>
		<%
        	} // End of tabList loop
      	%>
	</tr>
</table>
<table class="tabBorder" width="99%" height="5"><tr><td></td></tr></table>


	</td>
	</tr></table>

<% } %> 
 
