<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%><HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>

<BODY>
<jsp:useBean id="memRights" scope="request" class="com.velos.eres.web.memberRights.MemberRightsJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.MC"%>
<%
String memberId,rights[],cnt,rght="0";
int totrows =0,count=0;
String src = request.getParameter("src");
String tab = request.getParameter("selectedTab");
String eSign = request.getParameter("eSign");
HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession))
   {
		%> <jsp:include page="sessionlogging.jsp" flush="true"/> <%   
		String oldESign = (String) tSession.getValue("eSign");		
		if(!oldESign.equals(eSign)) 
		{
			%> <jsp:include page="incorrectesign.jsp" flush="true"/> <%
		}
		else 
		{			
		memberId = request.getParameter("memId"); //Total Rows		
		int memId=EJBUtil.stringToNum(memberId);
		int rbId = EJBUtil.stringToNum(request.getParameter("rbId"));	
		String ipAdd = (String) tSession.getValue("ipAdd");		
		String userId = (String) tSession.getValue("userId");		
		memRights.setId(memId);		
		totrows = Integer.parseInt(request.getParameter("totalrows")); //Total Rows			
		if (totrows > 1)
		{
			rights = request.getParameterValues("rights");			
			for (count=0;count<totrows;count++)
			{	
				rght = rights[count];				
				cnt = String.valueOf(count);				
				memRights.setGrSeq(cnt);	
				memRights.setModifiedBy(userId);
				memRights.setFtrRights(rght);
			}	
		}
		else
		{
			rght = request.getParameter("rights");			
			memRights.setGrSeq("1");			
			//memRights.setIpAdd(ipAdd);
			//memRights.setCreator(userId);
			memRights.setModifiedBy(userId);
			memRights.setFtrRights(rght);
		}		
		memRights.updateMemberRights();			
		%>
		<br><br><br><br><br>
		<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%-- Data was Saved successfully.*****--%> </p>		
		<META HTTP-EQUIV=Refresh CONTENT="1; URL=modifymember.jsp?srcmenu=<%=src %>&selectedTab=<%=tab%>&rbId=<%=rbId%>">	
		<%	
		}//end of if for eSign check
	}//end of if body for session
else
{
	%> <jsp:include page="timeout.html" flush="true"/> <%
}
%>
</BODY></HTML>
