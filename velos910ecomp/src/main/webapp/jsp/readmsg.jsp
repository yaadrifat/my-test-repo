<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<HEAD>
<title><%=LC.L_Read_Messages%><%--Read Messages*****--%></title>
</HEAD>
<BODY>
<jsp:useBean id="msgcntr" scope="request" class="com.velos.eres.web.msgcntr.MsgcntrJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,java.util.*"%>
<%@ page import="com.velos.eres.service.util.LC"%><%@ page import="com.velos.eres.service.util.MC"%>

<jsp:include page="homepanel.jsp" flush="true">

</jsp:include>

<% int usrId=0; %>

<%
HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession))
{

usrId=510;
// usrId =tSession.getValue("usrId");
}
%>


<% com.velos.eres.business.common.MsgCntrDao ctrldao= msgcntr.getMsgCntrVelosUser(usrId); %>
<% java.util.ArrayList texts=ctrldao.getMsgcntrTexts() ; %>
<% java.util.ArrayList username=ctrldao.getUserTos() ; %>
<% java.util.ArrayList studyname=ctrldao.getStudyNames() ; %>
<% java.util.ArrayList reqtype=ctrldao.getMsgcntrReqTypes() ; %>
<% java.util.ArrayList studyid=ctrldao.getMsgcntrStudyIds(); %>
<% java.util.ArrayList userto=ctrldao.getMsgcntrFromUserIds(); %>

<% String userName="";
   String studyName = "";
   String studyId="";
   String userTo="";
   String reqType="";
   String permission="";
%>


<DIV class="browserDefault" id="div1">
<P class="defComments"><i><%=MC.M_AssignRgt_ToUsrReq%><%--Assign rights to the user requests*****--%></i></p>
<table width="100%" >
 <tr>
   <th width="40%">
	<%=LC.L_Name%><%--Name*****--%>
   </th>
   <th width="20%">
	<%=LC.L_Study_No%><%--<%=LC.Std_Study%> No*****--%>
   </th>
   <th width="20%">
	<%=LC.L_Request%><%--Request*****--%>
   </th>

   <th width="40%">
	<%=LC.L_Rights%><%--Rights*****--%>
   </th>
 </tr>


 <%
	for(int i=0; i<texts.size() ; i++)
	{

		reqType=((reqtype.get(i)) == null)?"-":(reqtype.get(i)).toString();
		reqType=reqType.trim();

		if(!reqType.equalsIgnoreCase("a"))
		{

			if(reqType.equalsIgnoreCase("v")){
			permission="view";}

			if(reqType.equalsIgnoreCase("m")){
			permission="modify";}


		userName= ((username.get(i)) == null)?"-":(username.get(i)).toString();
		studyName=((studyname.get(i)) == null)?"-":(studyname.get(i)).toString();
		studyId=((studyid.get(i)) == null)?"-":(studyid.get(i)).toString();
		userTo=((userto.get(i)) == null)?"-":(userto.get(i)).toString();

		if ((i%2)==0) {
  %>
		<tr  id="msgcenterEvenRow">
  <%
		}
		else{
  %>

			<tr id="msgcenterOddRow">
  <%
		}
  %>
		<td >
			<%=userName.substring(0,12)%>
		</td>
		<td >
			<%= studyName.substring(0,10)%>
		</td>
		<td>
			<%= permission%>
		</td>

		<td>
		<%--	<A href="updatemsgcntr.jsp?&srcmenu=tdmenubaritem3&studyId=<%=studyId%>&userTo=<%=userTo %>&mode=V">view</A>
			<A href="updatemsgcntr.jsp?&srcmenu=tdmenubaritem3&studyId=<%=studyId%>&userTo=<%=userTo %>&mode=M">modify</A>
			<A href="updatemsgcntr.jsp?&&srcmenu=tdmenubaritem3&studyId=<%=studyId%>&userTo=<%=userTo %>&mode=D">deny</A>  --%>


		<Input type="radio" id ="view" name="right" >
		<Input type="radio" id ="modify" name="right" >
		<Input type="radio" id="deny" name="right">


		</td>
		</tr>

<%
		}}
%>
</table>
<input type= "button" value="<%=LC.L_Save%><%--save*****--%>" size=20 >
<div>
<jsp:include page="bottompanel.jsp" flush="true">
</jsp:include>
</div>

</div>
</BODY>
</HTML>