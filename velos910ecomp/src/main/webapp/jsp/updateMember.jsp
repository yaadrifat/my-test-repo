<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>

<BODY>
<jsp:useBean id="memberJB" scope="session" class="com.velos.eres.web.memberBoard.MemberJB" />
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.MC"%>
<%
	int totrows =0;
	int count=0; 
	String teamId;

	String src = request.getParameter("src");
	String tab = request.getParameter("selectedTab");
	String eSign = request.getParameter("eSign");
	HttpSession tSession = request.getSession(true); 
	String ipAdd = (String) tSession.getValue("ipAdd");
	String userId = (String) tSession.getValue("userId");
	String usr = (String) tSession.getValue("userId");
	String brdId = request.getParameter("brdId");
 
 if (sessionmaint.isValidSession(tSession))
   {
	%> <jsp:include page="sessionlogging.jsp" flush="true"/> <%   
   	String oldESign = (String) tSession.getValue("eSign");	
	if(!oldESign.equals(eSign)) 
	{ 
		%> <jsp:include page="incorrectesign.jsp" flush="true"/> <%
	}
	else
	{
	totrows = EJBUtil.stringToNum(request.getParameter("totrows"));	
	if (totrows > 0)
	{			
		String[] teamIds = request.getParameterValues("memPK");
		String[] roles = request.getParameterValues("role");		
		for (count = 0;count < totrows;count++)
		{
			int pkMem=EJBUtil.stringToNum(teamIds[count]);
			int roleMem=EJBUtil.stringToNum(roles[count]);
			int brId=EJBUtil.stringToNum(brdId);
			memberJB.setPkRBMember(pkMem);
			memberJB.getMemberDetailsById(pkMem);
			memberJB.setFkMemberRole(roleMem);	
			memberJB.setIpAdd(ipAdd);
			memberJB.setCreator(userId);
			memberJB.setModifiedBy(userId);
			memberJB.updateMember(); 	
		}
}
%>
<br><br><br><br><br>
<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%-- Data was Saved successfully.*****--%> </p>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=modifymember.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&rbId=<%=brdId%>">
<%
	}//end of if for eSign check
}//end of if body for session
else
{
	%> <jsp:include page="timeout.html" flush="true"/> <%
}
%>
</BODY></HTML>
