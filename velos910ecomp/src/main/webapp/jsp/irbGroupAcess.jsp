<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html><head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_Grp_Access%><%--Group Access*****--%></title>

<%@ page import="com.velos.eres.service.util.*"%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.studyRights.StudyRightsJB" %>
<jsp:useBean id="sv" scope="request" class="com.velos.eres.web.studyView.StudyViewJB"/>

</head>

<%
	String src = request.getParameter("srcmenu");
%>
<jsp:include page="panel.jsp" flush="true">
	<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0">
	<jsp:useBean id="userB" scope="session" class="com.velos.eres.web.user.UserJB"/>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<%@ page language = "java" import = "com.velos.eres.business.team.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil, com.velos.eres.service.util.StringUtil"%>

<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
	<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
	<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
	<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<%
	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))
	{
		String ipAdd = (String) tSession.getValue("ipAdd");
		String usr = (String) tSession.getValue("userId");
%>
	<jsp:include page="irbConfigTabs.jsp" flush="true">
		<jsp:param name="selectedTab" value="3"/>
	</jsp:include>
	
	<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="showDiscard" value="N"/>
	</jsp:include>
	
<%
	}//end of if body for session
	else
	{
		%> <jsp:include page="timeout.html" flush="true"/> <%
	}
	%>
	
	<div>
		<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</div>
	<div class ="mainMenu" id="emenu">
		<jsp:include page="getmenu.jsp" flush="true"/>
	</div>
</body></html>
