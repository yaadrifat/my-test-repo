<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<Link Rel=STYLESHEET HREF="common.css" type=text/css>

</HEAD>
<BODY>
  <jsp:useBean id="lineitemB" scope="request" class="com.velos.esch.web.lineitem.LineitemJB"/>
  <jsp:useBean id="budgetcalB" scope="request" class="com.velos.esch.web.budgetcal.BudgetcalJB"/>
  <jsp:useBean id="bgtSectionB" scope="request" class="com.velos.esch.web.bgtSection.BgtSectionJB"/>
  <jsp:useBean id="budgetB" scope="request" class="com.velos.esch.web.budget.BudgetJB"/>
  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
  <%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,java.text.*,com.velos.eres.service.util.*, com.velos.esch.business.budgetcal.impl.*"%>
  <%

	String src = null;
	src = request.getParameter("srcmenu");
	String eSign = request.getParameter("eSign");
	eSign=(eSign==null)?"0":eSign;
	String mode = request.getParameter("mode");
	String selectedTab = request.getParameter("selectedTab");
	String pageMode = "initial";

HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession))

   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
//   	String oldESign = (String) tSession.getValue("eSign");

//if(!oldESign.equals(eSign) && !(eSign.equals("0"))) {
%>


<%
//	} else {


	String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = (String) tSession.getValue("userId");
	String budgetId = request.getParameter("budgetId");
	int bgtcalId = EJBUtil.stringToNum(request.getParameter("bgtcalId"));


	budgetB.setBudgetId(EJBUtil.stringToNum(budgetId));
	budgetB.getBudgetDetails();
	budgetB.setModifiedBy(usr);


	 

	//budgetB.setLastModifiedDate(sdateTime);
	budgetB.setLastModifiedDate(DateUtil.dateToString(new Date()));

    int rete = budgetB.updateBudget();

//out.println(bgtcalId);

	int rows = EJBUtil.stringToNum(request.getParameter("rows"));
	int bgtSectionId = 0;

	String indirectCostPer = request.getParameter("indirectCostPer");
	String grandResTotalCost = request.getParameter("netTotal");
	String grandIndTotalCost = request.getParameter("indirectCost");
	String grandTotalTotalCost = request.getParameter("grandTotal");




	String lineitemId = "";
	String unitCost = "";
	String noUnit = "";
	String category = "";
	String srtLineitemCost = "";
	String srtCostGrandTotal = "";
	String srtCostGrandTotalOld= "";

	ArrayList lineitemIdArrayList = new ArrayList();
	ArrayList unitCostArrayList = new ArrayList();
	ArrayList noUnitArrayList = new ArrayList();
	ArrayList dummyArrayList = new ArrayList();
	ArrayList discountArrayList = new ArrayList();
	ArrayList researchCostArrayList = new ArrayList();
	ArrayList stdCareCostArrayList = new ArrayList();
	ArrayList invCostArrayList = new ArrayList();
	ArrayList otherCostArrayList = new ArrayList();
	ArrayList categoryArrayList = new ArrayList();
	ArrayList  appIndirectsArrayList = new ArrayList();
	ArrayList  srtLineitemCostArrayList = new ArrayList();
	ArrayList  srtCostGrandTotalArrayList = new ArrayList();
	ArrayList strtDummy =  new ArrayList();

	int i = 0;

	if(rows == 1) {
		lineitemId = request.getParameter("lineitemId");
		unitCost = request.getParameter("unitCost");
		noUnit = request.getParameter("noUnits");
		category= request.getParameter("cmbCtgry");

		srtLineitemCost = request.getParameter("totalCost");
		srtCostGrandTotal = request.getParameter("subTotal");

		lineitemIdArrayList.add(lineitemId);
		unitCostArrayList.add(unitCost);
		noUnitArrayList.add(noUnit);
		categoryArrayList.add(category);
		discountArrayList.add("0");
	   	researchCostArrayList.add("0");
	 	stdCareCostArrayList.add("0");
	 	invCostArrayList.add("0");
		otherCostArrayList.add("0");
		appIndirectsArrayList.add("0");
		strtDummy.add("0");

		srtLineitemCostArrayList.add(srtLineitemCost);

		   bgtSectionId = 	EJBUtil.stringToNum(request.getParameter("secId"));
		   bgtSectionB.setBgtSectionId(bgtSectionId);
		   bgtSectionB.getBgtSectionDetails();
		   bgtSectionB.setSrtCostGrandTotal(srtCostGrandTotal);
		   int ret = bgtSectionB.updateBgtSection();



		//dummyArrayList.add("0.0");

	} else {

		String lineitemIds[] = request.getParameterValues("lineitemId");
		String unitCosts[] = request.getParameterValues("unitCost");
		String noUnits[] = request.getParameterValues("noUnits");
		String categories[]= request.getParameterValues("cmbCtgry");

		String srtLineiteCosts[] = request.getParameterValues("totalCost");

		// Added by IA 9/22/2006

		String srtCostGrandTotals[] = request.getParameterValues("subTotal");
	  	String bgtSectionIds[] = request.getParameterValues("bgtSectionId");


		int secid = 0;
		if(bgtSectionIds != null){

			secid = bgtSectionIds.length;
		}


		for (i =0;i<secid;i++) {

		  	 // check if there are any lineitems for the section


			  	  if (!(bgtSectionIds[i] == null)) {

				       		   bgtSectionB.setBgtSectionId(EJBUtil.stringToNum(bgtSectionIds[i]));
							   bgtSectionB.getBgtSectionDetails();
							   bgtSectionB.setSrtCostGrandTotal(srtCostGrandTotals[i]);
							   int ret = bgtSectionB.updateBgtSection();

				    }
			}


		//End Added 9/22/2006

		for(i =0;i<rows;i++) {

			lineitemIdArrayList.add(lineitemIds[i]);
			unitCostArrayList.add(unitCosts[i]);
			noUnitArrayList.add(noUnits[i]);
			categoryArrayList.add(categories[i]);
			//dummyArrayList.add("0.0");
			discountArrayList.add("0");
			researchCostArrayList.add("0");
			stdCareCostArrayList.add("0");
			invCostArrayList.add("0");
			otherCostArrayList.add("0");
			appIndirectsArrayList.add("0");
			strtDummy.add("0");
			srtLineitemCostArrayList.add(srtLineiteCosts[i]);
		}
	}

/*public int updateAllLineitems(ArrayList lineitemIds, ArrayList discounts, ArrayList unitCosts, ArrayList noUnits,
	ArrayList researchCosts, ArrayList stdCareCosts, ArrayList invCosts,
	ArrayList otherCosts, String ipAdd, String usr)*/

	//int output = lineitemB.updateAllLineitems(lineitemIdArrayList, unitCostArrayList, noUnitArrayList, dummyArrayList, ipAdd, usr);
	int output = lineitemB.updateAllLineitems(lineitemIdArrayList,discountArrayList, unitCostArrayList, noUnitArrayList,
	   researchCostArrayList,stdCareCostArrayList,categoryArrayList,appIndirectsArrayList,ipAdd, usr, srtLineitemCostArrayList,  strtDummy, strtDummy);
	if(output == -2) {
%>
<br>
<br>
<br>
<br>
<br>
<p class = "sectionHeadings" align = center> <%=MC.M_DataCnt_Svd%><%--Data could not be saved.*****--%> </p>
<%
	} else {

		budgetcalB.setBudgetcalId(bgtcalId);
		BudgetcalBean bcsk = budgetcalB.getBudgetcalDetails();
		budgetcalB.setIndirectCost(indirectCostPer);
		budgetcalB.setBudgetResearchTotalCost(grandResTotalCost);
		budgetcalB.setBudgetIndirectTotalCost(grandIndTotalCost);
		budgetcalB.setBudgetTotalTotalCost(grandTotalTotalCost);
		output = budgetcalB.updateBudgetcal();
		if(output == -2) {
%>
<br>
<br>
<br>
<br>
<br>
<p class = "sectionHeadings" align = center> <%=MC.M_DataCnt_Svd%><%--Data could not be saved.*****--%> </p>

<%
		} else {
%>
<br>
<br>
<br>
<br>
<br>
<p class = "sectionHeadings" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully.*****--%> </p>
<%
		}
	}
%>
	<META HTTP-EQUIV=Refresh CONTENT="2; URL=studybudget.jsp?srcmenu=<%=src%>&mode=M&selectedTab=<%=selectedTab%>&pageMode=final&bgtcalId=<%=bgtcalId%>&budgetId=<%=budgetId%>&budgetTemplate=S">
<%
//}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

</HTML>





