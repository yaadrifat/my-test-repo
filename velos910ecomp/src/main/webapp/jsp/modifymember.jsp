<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html><head>
<title><%=MC.M_ReviewBoard_Member%><%--Review Board >> Board Member*****--%></title>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.business.common.*,com.velos.eres.service.util.*" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<SCRIPT Language="javascript">
function  validate(formobj)
{
 if (!(validate_col('e-Signature',formobj.eSign))) return false
    if(document.getElementById("eSignMessage").innerHTML != "<%=LC.L_Valid_Esign%>") {
				alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
				formobj.eSign.focus();
				return false;
			}
}

function checkDelete(pright, mode, username, curTeam) {		
		if (f_check_perm(pright,mode) == false){
			return false;
		}
		
		if (confirm("Do you want to delete Member from Board")){/*if (confirm("Delete '" + username + "' from <%=LC.L_Study%> Team?")){*****/
			return true;
		}else{
			return false;
		}

	}
</SCRIPT>

<% 
	String src = request.getParameter("srcmenu");
	String from = "board";
%>
<jsp:include page="panel.jsp" flush="true"> 
	<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   

<body>
<jsp:useBean id="memberB" scope="session" class="com.velos.eres.web.memberBoard.MemberJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="userB" scope="session" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id="reviewB" scope="session" class="com.velos.eres.web.reviewBoard.ReviewBoardJB" />
<jsp:useBean id="commonDao" scope="session" class="com.velos.eres.business.common.ReviewBoardDao" />
<jsp:useBean id="memberJB" scope="session" class="com.velos.eres.web.memberBoard.MemberJB" />
<jsp:useBean id="commonDaoMember" scope="session" class="com.velos.eres.business.common.MemberDao" />

<%@ page language = "java" import = "com.velos.eres.business.team.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.reviewBoard.ReviewBoardJB,com.velos.eres.business.reviewboard.impl.ReviewBoardBean"%>
  <%
   HttpSession tSession = request.getSession(true); 

   if (sessionmaint.isValidSession(tSession))
	{

	String uName = (String) tSession.getValue("userName");
	String acc = (String) tSession.getValue("accountId");
	String rights = request.getParameter("right");
	String tab = request.getParameter("selectedTab");	
	String rbId=request.getParameter("rbId");	

	int pageRight = 7;
	
	if (pageRight > 0 )
	{ 
	 reviewB.setReviewBoardId(EJBUtil.stringToNum(rbId));	
	 reviewB.getReviewBoardNames(reviewB.getReviewBoardId());	
     String rbName=reviewB.getReviewBoardName();
	 Object[] arguments1 = {rbName};
     
%>
<BR/>
<table>
	<td class= "grayComments lhsFont">
		<%=VelosResourceBundle.getMessageString("M_Working_BoardName",arguments1)%>
	</td>
</table>
 <table width="100%" cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td colspan="4">
		<jsp:include page="irbConfigTabs.jsp" flush="true">
		<jsp:param name="selectedTab" value="<%=tab%>" />
		</jsp:include>
		</td>
	</tr>
</table>
<BR/>
<BR/>
<DIV class="BrowserBotN BrowserBotN_S_3" id="div1"> 
	<Form  name="search" method="post" action="assignToBoard.jsp?srcmenu=tdmenubaritem6&selectedTab=<%=tab%>&rbId=<%=rbId%>">


 <table class="tableDefault" width="100%" border=0 cellspacing="0" cellpadding="0">
	<tr height="20">
	<td colspan="6" bgcolor="#dcdcdc"> <b> <%=MC.M_AddNewUsr_ToBoard%><%--To add a new User to the Board, Search By*****--%></b></td>
	</tr>
	<tr>
	<td width="10%" bgcolor="#dcdcdc"> <%=LC.L_First_Name%><%--First Name*****--%>:</td>
	 <td width="20%" bgcolor="#dcdcdc"> <Input type=text name="fname">  </td>
     <td width="10%" bgcolor="#dcdcdc" align="right"> <%=LC.L_Last_Name%><%--Last Name*****--%>:&nbsp; </td>
     <td width="20%" bgcolor="#dcdcdc"> <Input type=text name="lname"> </td>
	  <td colspan="2" align="Right" bgcolor="#dcdcdc"> 
			
			 <button type="submit"><%=LC.L_Search%></button> 
		 
        </td>
     </tr>
	</table>
    <Input type=hidden name="srcmenu" value="<%=src%>">
  </Form>
  
 <Form name="team" id="teamFrmId" method="post" action="updateMember.jsp" onsubmit="if (validate(document.team)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

    <Input type="hidden" name="selectedTab" value="<%=tab%>">
    <table width="100%" cellspacing="0" cellpadding="0" border=1 >
      <tr > 
        <td width = "100%"> 
          <P class = "defcomments">
          <% String Hyper_Link = "<A href=\"userdetails.jsp?mode=N&srcmenu=tdMenuBarItem3&selectedTab=2\">"+LC.L_Add_NewUser+"</a>";
          Object[] arguments = {Hyper_Link}; %><%=VelosResourceBundle.getMessageString("M_UnableToFindUsr_AddUsr",arguments) %>
           <%--If you are unable to find a user in the existing user list, you may <A href="userdetails.jsp?mode=N&srcmenu=tdMenuBarItem3&selectedTab=2">Add New User </a> here*****--%></P> 
           <br><br>
            <p class = "sectionheadings"><%=VelosResourceBundle.getMessageString("M_MemberBoard_DeatailFor",arguments1)%></P>
        </td>
      </tr>
    </table>
    <table width="100%" border="2"  >
      <tr> 
        <th width="40%"> <%=LC.L_User_Name%><%--User Name*****--%> </th>
        <th width="30%"> <%=LC.L_Role%><%--Role*****--%> </th>
        <th width="5%"> <%=LC.L_Access_Rights%><%--Access Rights*****--%> </th>
		<th width="15%"><%=LC.L_Delete%></th>
      </tr>	 
	 <%
	
	
	 int brdId=EJBUtil.stringToNum(rbId);
	 memberJB.setFkBoardId(brdId);
	 MemberDao memDao=new MemberDao();
	 memDao.getMemberValues(brdId);
	 ArrayList memberIds =memDao.getPkRBMember();
     ArrayList brdIds =memDao.getFkBoardId();
     ArrayList memberRoles =memDao.getFkMemberRole();
     ArrayList fkUser = memDao.getFkMemberUser();
     ArrayList memberStatus = memDao.getBrdMemStatus();
     ArrayList memberRgts= memDao.getBrdMemRights();
     int len=brdIds.size();     
     for(int counter = 0;counter <len; counter++)
	 {	
    	 String selName = null;
		int memberId=EJBUtil.stringToNum(((memberIds.get(counter)) == null)?"-":(memberIds.get(counter)).toString());
		int fkbrdId=EJBUtil.stringToNum(((brdIds.get(counter)) == null)?"-":(brdIds.get(counter)).toString());
		int memRole=EJBUtil.stringToNum(((memberRoles.get(counter)) == null)?"-":(memberRoles.get(counter)).toString());
		int userId= EJBUtil.stringToNum(((fkUser.get(counter)) == null)?"-":(fkUser.get(counter)).toString());
		//String memStatus=EJBUtil.ArrayListToString(memberStatus);
		//String memStatus=((memberStatus.get(counter)) == null)?"-":(memberStatus.get(counter)).toString();
		//String memRight=((memberRgts.get(counter)) == null)?"-":(memberRgts.get(counter)).toString();
		//String memRight= EJBUtil.ArrayListToString(memberRgts);		
		
		userB.setUserId(userId);
		userB.getUserDetails();
		String userFName=userB.getUserFirstName();
		String userLName=userB.getUserLastName();
		selName=userFName+" "+userLName;		
		CodeDao cdRole = new CodeDao();
	   	cdRole.getCodeValues("board_role");
		String role = cdRole.toPullDown("role",memRole);		
		%>
		<input type="hidden" name="memPK" Value="<%=memberId%>">
		<input type="hidden" name="roleId" Value="<%=memRole%>">
        <input type="hidden" name="brdId" Value="<%=fkbrdId%>">
		<input type="hidden" name="selName" Value="<%=selName%>">
		<td><%=selName%></td>
		<td  align="center"><%=role%></td>
        <td  align="center">
    <A href="memberRights.jsp?srcmenu=tdmenubaritem6&selectedTab=<%=tab%>&memPK=<%=memberId%>&selName=<%=selName%>&rbId=<%=fkbrdId%>" onClick="">
      <img border="0" src="./images/AccessRights.gif" title="<%=LC.L_Access_Rights%><%--Access Rights*****--%>"></A> </td>
	<td  align="center"> 
		<A href="deletemember.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&rbId=<%=fkbrdId%>&right=<%=pageRight%>&userName=<%=selName%>&userId=<%=userId%>&mempk=<%=memberId%>" onClick="return checkDelete(<%=pageRight%>,'E', '<%=selName%>',<%=userId%>);"><img src="./images/delete.gif" border ="0"  title="<%=LC.L_Rem_FromTeam%>" alt="<%=LC.L_Rem_FromTeam%>"/><%//=LC.L_Rem_FromTeam%><%--Remove from Team*****--%></A>
			</td>
      </tr>
      <%
	 }
%>
      <input type="hidden" name="src" Value="<%=src%>">
      <input type="hidden" name="totrows" Value="<%=len%>">
	  </table>
	  <BR/>
	  <BR/>
	  	<% if (pageRight == 6 || pageRight == 7)
				{
				%>        
	  	<jsp:include page="submitBar.jsp" flush="true"> 
				<jsp:param name="displayESign" value="Y"/>
				<jsp:param name="formID" value="teamFrmId"/>
				<jsp:param name="showDiscard" value="N"/>
		</jsp:include>
  <% }%>
  </Form>   
  <%
	}
}
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
<%
}
%>
  <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<DIV class="mainMenu" id = "emenu"> 
  <jsp:include page="getmenu.jsp" flush="true"/>
</DIV>
</body></html>
