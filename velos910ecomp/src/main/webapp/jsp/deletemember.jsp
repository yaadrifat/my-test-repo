<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@ page language = "java" import="com.velos.eres.business.common.*,com.velos.eres.service.util.*,java.util.*,com.velos.eres.service.util.EJBUtil"%>
<%@ page language = "java" import="com.velos.eres.service.util.StringUtil,com.aithent.audittrail.reports.AuditUtils"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=MC.M_Del_Board_Member %></title>
<jsp:useBean id="memberJB" scope="session" class="com.velos.eres.web.memberBoard.MemberJB"/>
<jsp:useBean id="reviewB" scope="session" class="com.velos.eres.web.reviewBoard.ReviewBoardJB" />
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<SCRIPT>
function  validate(formobj)
{
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true)
	{
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");
		formobj.eSign.focus();
		return false;
	}
}
</SCRIPT>

<%
    String src = request.getParameter("srcmenu");
%>

<jsp:include page="skinChoser.jsp" flush="true"/>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>  

<BODY> 
<BR/>
<% 
HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession))	
{
	String tab = StringUtil.htmlEncode(request.getParameter("selectedTab"));
	String memIdStr =request.getParameter("mempk");
	int memPK =EJBUtil.stringToNum(memIdStr);			
	String rbIdStr =request.getParameter("rbId");
	int rbId =EJBUtil.stringToNum(rbIdStr);
	reviewB.setReviewBoardId(rbId);	
	reviewB.getReviewBoardNames(reviewB.getReviewBoardId());	
    String rbName=reviewB.getReviewBoardName();
	int userId = EJBUtil.stringToNum(request.getParameter("userId"));
	String delMode=request.getParameter("delMode");	
	Object[] arguments = {rbName};
%>

<table>
	<td class= "grayComments lhsFont">
		<%=VelosResourceBundle.getMessageString("M_Working_BoardName",arguments)%>
	</td>
</table>
	<%
	if (delMode==null) 
	{
		delMode="final";
	%>
	<FORM name="deleteTeam" id="delTeamId" method="post" action="deletemember.jsp" onSubmit="if (validate(document.deleteTeam)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<br><br>
	
	<TABLE width="98%" cellspacing="0" cellpadding="0" >
		<tr>
		<tr><td align = "right"><A href="modifymember.jsp?srcmenu=tdmenubaritem6&selectedTab=<%=tab%>&rbId=<%=rbId%>"><%=LC.L_GoBack_ToBoard%></A><%--Go Back to Board*****--%>
	</td></tr>
		<td align="center">
		<b><%=MC.M_PlsEtrEsign_Del%><%--Please enter e-Signature to proceed with deletion*****--%> </b>
		</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
		</table>
		<jsp:include page="submitBar.jsp" flush="true"> 
				<jsp:param name="displayESign" value="Y"/>
				<jsp:param name="formID" value="copyverfrm"/>
				<jsp:param name="showDiscard" value="N"/>
		</jsp:include>

 	 <input type="hidden" name="delMode" value="<%=delMode%>">
  	 <input type="hidden" name="srcmenu" value="<%=src%>">
 	 <input type="hidden" name="rbId" value="<%=rbId%>">
	 <input type="hidden" name="mempk" value="<%=memPK%>">
 	 <input type="hidden" name="userId" value="<%=userId%>">
 	 <input type="hidden" name="selectedTab" value="<%=tab%>">
	 
	</FORM>
<%
	} else {
				String eSign = request.getParameter("eSign");	
				String oldESign = (String) tSession.getValue("eSign");
				if(!oldESign.equals(eSign)) 
				{
					%> <jsp:include page="incorrectesign.jsp" flush="true"/> <%
				} else {
					memberJB.setPkRBMember(memPK);
					memberJB.deleteFromBoard();
			%>
				<table align=center>
				<BR><BR><BR><BR><BR><BR><BR><BR>				
				<p class = "successfulmsg" align = "center" ><%=MC.M_Data_DelSucc%><%--Data Deleted successfully*****--%></p>
				<META HTTP-EQUIV=Refresh CONTENT="1; URL=modifymember.jsp?srcmenu=tdmenubaritem6&selectedTab=<%=tab%>&rbId=<%=rbId%>">		
				</table>
				<%	} //end esign
			} //end of delMode
		}//end of if body for session
		else
		{
			%> <jsp:include page="timeout.html" flush="true"/> <%
		}
%>

<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</DIV>

<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/> 
</div>

</BODY>
</HTML>

