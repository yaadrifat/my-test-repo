<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=MC.M_Selc_ActionCndnVal%><%--Select Action Condition Values*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

</head>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>


<SCRIPT Language="javascript">

function toRefresh(formobj)
{
		if(formobj.toRefresh.value=="yes")
		{
			window.opener.location.reload();
		}
}


 function  populateOpener(formobj)
 {
	
	totRows = formobj.totRows.value;
	fieldtype = formobj.fieldtype.value;
	var valueStr, dispStr, keywordConditionValue;
	var conditionDispFld,conditionIdFld,conditionKeywordFld,sourceFormName;
				
	dispStr = "";
	valueStr = "";
	keywordConditionValue = "";
	
	var value = "";
	if ( fieldtype == 'M')
	{
		if (totRows > 1) {
        	for(cnt = 0; cnt<totRows;cnt++)
        	{
    			  if (formobj.selectCondition[cnt].checked)
    			  {
    			  	  value = formobj.fldCondition[cnt].value;
   			  		  value = "[" + value + "]";  
   			  		  
    			  	  dispStr = dispStr + "," + value;
    			  	  valueStr = valueStr + "[VELSEP]" + formobj.fldRespId[cnt].value;
    			  	  keywordConditionValue = keywordConditionValue  + "[VELSEP]" + formobj.fldCondition[cnt].value;
    			  	  
	    		  }
    		
			}//end for
			valueStr = valueStr + "[VELSEP]";	
			keywordConditionValue = keywordConditionValue  + "[VELSEP]";	
		}	 //endif for totRows
		else if (totRows == 1)
		{
		 	 	  value = formobj.fldCondition.value;
  			  	  value = "[" + value + "]";  
   			  		  
    			  dispStr =  "," + value;
    			  //valueStr = "[VELSEP]" + formobj.fldRespId.value;
   			  	  //keywordConditionValue = "[VELSEP]" + formobj.fldCondition.value;
   			  	  //dispStr =   value;
   			  	  
    			  valueStr = formobj.fldRespId.value;
   			  	  keywordConditionValue = formobj.fldCondition.value;
 				 
 				// valueStr = valueStr + "[VELSEP]";	
  	  			//keywordConditionValue = keywordConditionValue  + "[VELSEP]";	
		}	
	} //end if for fieldtype = M			
	else	
	{
       	for(cnt = 0; cnt<totRows;cnt++)
        	{
        			value = formobj.fldCondition[cnt].value;
        			if (fnTrimSpaces(value)!= '')
        			{
        				value = "[" + value + "]";
    			    	dispStr = dispStr + "," + value;
    			    	keywordConditionValue = keywordConditionValue  + "[VELSEP]" + formobj.fldCondition[cnt].value;
    			    }	
    		
			}//end for
		keywordConditionValue = keywordConditionValue  + "[VELSEP]";	
	}	
			if (dispStr.length > 1)
			{
			
				dispStr = dispStr.substring(1,dispStr.length);
			}
			
			/*if (valueStr.length > 8)
			{	
				valueStr = valueStr.substring(8,valueStr.length);
				keywordConditionValue = keywordConditionValue.substring(8,keywordConditionValue.length);
			}*/
			

	conditionDispFld = formobj.conditionDispFld.value;
	conditionIdFld = formobj.conditionIdFld.value;
	conditionKeywordFld = formobj.conditionKeywordFld.value;
	sourceFormName = formobj.sourceFormName.value;
	
	//set values;

	 window.opener.document.forms[sourceFormName].elements[conditionDispFld].value=dispStr ;	
 	 window.opener.document.forms[sourceFormName].elements[conditionIdFld].value=valueStr ;	
 	 window.opener.document.forms[sourceFormName].elements[conditionKeywordFld].value=keywordConditionValue ;
			
	self.close();		
	return false;

 }

</SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="fieldRespB" scope="request" class="com.velos.eres.web.fldResp.FldRespJB"/>
<jsp:useBean id="fieldLibJB" scope="request"  class="com.velos.eres.web.fieldLib.FieldLibJB"/>


<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB"%>

<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1) 
   	ienet = 0; //IE
else
	ienet = 1;

if (ienet == 0) {
%>
<body style="overflow:scroll">
<%} else {%>
<body>
<%}%>
	
<DIV class="popDefault" id="div1"> 
	
 <%

	HttpSession tSession = request.getSession(true);
	int pageRight = 0;	
	
   if (sessionmaint.isValidSession(tSession))
   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
	<jsp:include page="include.jsp" flush="true"/>

<%  
	
		CtrlDao ctrl = new CtrlDao();
		String use_dispflag = "";
		use_dispflag = ctrl.getControlValue("use_disp");
		
		if (StringUtil.isEmpty(use_dispflag))
		{
			use_dispflag = "1";
		}
	
		//the number of defualt responses
		int fldRespCount = 0 ; 
		String fieldLibId="";
		
		String mode=request.getParameter("mode");
		String fldType="";
		String fldDataType="";
		String dispVal = "";
		String secHeading  = "";
		int fldRespId = 0;
		int fldConditionCount = 10;
		
		String conditionDispFld = "";
		String conditionIdFld  = "";
		String conditionKeywordFld  = "";
		String sourceFormName = "";
		
	
		ArrayList arrDispVal=new ArrayList();
		
		ArrayList arrDispValToDisplay=new ArrayList();
		ArrayList arrRespId = new ArrayList();
		
		fieldLibId=request.getParameter("fieldLibId");
		
		conditionDispFld = request.getParameter("conditionDispFld");
		conditionIdFld  = request.getParameter("conditionIdFld");
		conditionKeywordFld  = request.getParameter("conditionKeywordFld");
		sourceFormName = request.getParameter("sourceFormName");

		fieldLibJB.setFieldLibId(Integer.parseInt(fieldLibId));
		fieldLibJB.getFieldLibDetails();
		fldType=fieldLibJB.getFldType();
		fldDataType=fieldLibJB.getFldDataType();
		
		if (fldType.equals("M"))
		{	
			secHeading = MC.M_FldType_MultiChoice_Upper ;/*secHeading = "FIELD TYPE: MULTIPLE CHOICE " ;*****/
			FldRespDao fldRespDao = fieldRespB.getResponsesForField(EJBUtil.stringToNum(fieldLibId));	
			arrRespId = fldRespDao.getArrFldRespIds();
			
			if (use_dispflag.equals("1"))
			{
				arrDispVal=fldRespDao.getArrDispVals();
			}
			else
			{
				arrDispVal=fldRespDao.getArrDataVals(); //get data values when use display falg = 0
				
			}
			
			arrDispValToDisplay =fldRespDao.getArrDispVals();
			
			fldRespCount = arrRespId.size();
			
		}
		else
		{
			secHeading = MC.M_FldTyp_EdtBox_Upper ;/*secHeading = "FIELD TYPE: EDIT BOX " ;*****/
		}	
		
		
		
	%>

	
	
  	
	<table width="99%" cellspacing="1" cellpadding="0" border="0" class="basetbl midAlign">
  	<tr>
		<P class="sectionHeadings"><%=secHeading %></P>
		
	 </tr>   
	 </table>
	 	 
    <Form name="conditionvalueform" id="condvalfrm" method="post" action="" onsubmit="if (populateOpener(document.conditionvalueform)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	 
	<Input type="hidden" name="mode" value=<%=mode%> >
	<Input type="hidden" name="fieldLibId" value=<%=fieldLibId%> >
	<Input type="hidden" name="fieldtype" value=<%=fldType%> >

	<Input type="hidden" name="conditionDispFld" value="<%=conditionDispFld%>" >
	<Input type="hidden" name="conditionIdFld" value="<%=conditionIdFld%>" >
	<Input type="hidden" name="conditionKeywordFld" value="<%=conditionKeywordFld%>" >
	<Input type="hidden" name="sourceFormName" value=<%=sourceFormName%> >
	
	<script>
		//get the selected condition values values 
		var conditionfld, conditionfldVal, parentForm, conditionKeywordFld;

		//declare an associative array
		var arSelectedConditionVal = new Object();
		var fldType;

		fldType = window.document.conditionvalueform.fieldtype.value;
		
		//get the value of this field from parent popup
		parentForm = window.document.conditionvalueform.sourceFormName.value;
		
		if (fldType == 'M')
		{
			conditionfld = window.document.conditionvalueform.conditionIdFld.value;
			conditionfldVal = window.opener.document.forms[parentForm].elements[conditionfld].value ;
		}
		else
		{
			conditionKeywordFld = window.document.conditionvalueform.conditionKeywordFld.value;
			conditionfldVal = window.opener.document.forms[parentForm].elements[conditionKeywordFld].value ;
		}
				
		//replace the [velcomma keyword with a comma]
		conditionfldVal = replaceSubstring(conditionfldVal,"[VELSEP]",",")
		
		if (conditionfldVal.length > 0)
		{
			conditionfldVal = conditionfldVal.substring(1,conditionfldVal.length);
			conditionfldVal = conditionfldVal.substring(0,conditionfldVal.length - 1);
		}
		// split
				
		var idarray = conditionfldVal.split(",");
		var part_num=0;
		
		while (part_num < idarray.length)
		 {
		   arSelectedConditionVal[idarray[part_num]] = idarray[part_num];
		   part_num+=1;
		 }
		 
					
	</script>
	
<%
if (fldType.equals("M"))
{	
 %>		
  <Input type="hidden" name="totRows" value=<%=fldRespCount%> >			
  <table width="99%" cellspacing="1" cellpadding="0" border="0" class="basetbl midalign">
	<th width="10%"><%=LC.L_Select%><%--Select*****--%></FONT></th>
	<th width="50%"><%=MC.M_DataUsed_InInterFldAct%><%--Data/Display Value to be used in the Inter-Field action*****--%></th>
	<th width="50%"><%=LC.L_Responses%><%--Responses*****--%></th>
  </tr>
  
 <%
  for(int i=0 ; i < fldRespCount; i++) 
  {
    fldRespId = ((Integer)arrRespId.get(i)).intValue();
    dispVal = (String)arrDispVal.get(i);
 %>  	  	  	  
      <tr> 
     	<td>
     		<Input type="checkbox" name = "selectCondition" value = <%=fldRespId%>>
     	</td>
        <td> 
		 <input name="fldRespId" type="hidden" value="<%=fldRespId%>" >
		 <input name="fldCondition" type="hidden" value="<%=dispVal%>" >
       	<%=dispVal%> 
        </td>
        <td><%= (String)arrDispValToDisplay.get(i)%></td>
	  </tr>
		
		<%} //for loop
		%>
</table>
<%
	}
%>		
<%
if (fldType.equals("E"))
{	
		%>		
 <Input type="hidden" name="totRows" value=<%=fldConditionCount%> >						
  <table width="99%" cellspacing="0" cellpadding="0"  class="basetbl midalign">
  <tr>
  	  <td colspan="2"><P class="defComments"><%=LC.L_Enter_ConditionValues%><%--Enter Condition Values*****--%></P></td>
 <%
  for(int i=0 ; i < fldConditionCount; i++) 
  {

 %>  	  	  	  
      <tr> 
        <td> 
		 <input name="fldCondition" type="text"> &nbsp; <%=LC.L_Or_Lower%><%--or*****--%>
        </td>
	  </tr>
		
		<%} //for loop
		%>
</table>
<%
	}
%>		
   <br>

  
 <jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="N"/>
		<jsp:param name="formID" value="condvalfrm"/>
		<jsp:param name="showDiscard" value="N"/>
 </jsp:include>
 
 	<script>
	//  loop through the responses check boxes and select the ones in the associative array
	formobj = window.document.conditionvalueform;
	
	totRows = formobj.totRows.value;
	fieldtype = formobj.fieldtype.value;
	var conditionVal;
			
	if ( fieldtype == 'M')
	{
		if (totRows > 1) 
		{
			for(cnt = 0; cnt<totRows;cnt++)
			{
				 conditionVal = formobj.selectCondition[cnt].value;
				 
				 // check for this conditionVal in the associative array
				 if (typeof arSelectedConditionVal[conditionVal] != "undefined")
				 {
					formobj.selectCondition[cnt].checked = true;
				 }
				 else
				 {
				 }
		 
			}	 //
								
		}	 //endif for totRows
		else if (totRows == 1)
		{
		 	 conditionVal = formobj.selectCondition.value;
			 // check for this conditionVal in the associative array
			 if (typeof arSelectedConditionVal[conditionVal] != "undefined")
			 {
			 	formobj.selectCondition.checked = true;
			 }
	 
			 
  		}	
	} //end if for fieldtype = M			
	else	
	{
		cnt = 0;
	
		for (anItem in arSelectedConditionVal)
		{
		   formobj.fldCondition[cnt].value = arSelectedConditionVal[anItem];
		   cnt++;
		}
 		
		
	}	
		
		
	</script>
	
				 
  </Form>
  

<%				
		
}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
<%

}

%>
 <div class = "myHomebottomPanel"> 
 <jsp:include page="bottompanel.jsp" flush="true"/>
 </div>
</body>
</html>
