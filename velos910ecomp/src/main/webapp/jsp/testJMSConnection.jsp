<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@ page import="javax.security.auth.login.LoginContext, javax.naming.*,java.util.*,java.io.*,javax.jms.*,org.jboss.mq.SpyConnectionFactory,org.jboss.mq.il.http.HTTPServerILFactory,org.jboss.mq.SpyXAConnectionFactory	" %><%@page import="com.velos.eres.service.util.LC"%><%@page import="com.velos.eres.service.util.MC"%>
<SCRIPT LANGUAGE="JavaScript" SRC="whichcss_skin.js"></SCRIPT>
<title><%=LC.L_Test_JmsConnection%><%--Test JMS Connection*****--%></title>

<body>
<BR><B><%=LC.L_Test_JmsConnection%><%--Test JMS Connection*****--%></B></BR>

<%
String modeVal = "";
String ip = "";
String jndiport = "";
String connFactory = "";

modeVal = request.getParameter("mode");

if (modeVal  == null)
{
	modeVal = "";
}

ip  = request.getParameter("ipAdd");

	jndiport = request.getParameter("jndiport");

	connFactory = request.getParameter("conn");
	

	if (modeVal.equals(""))
	{
		ip = "";
		jndiport = "1099";
		connFactory = "ConnectionFactory";

	}

	


%>
<DIV >
<form action = "testJMSConnection.jsp" method="post">
	<table>
	<tr><td width="30%"><%=MC.M_EtrIpOrHost_OfRemEresSrvr%><%--Enter the IP Address/Host Name of a remote eResearch Server*****--%> : </td><td width="30%"><input name = "ipAdd" type="text" value = "<%=ip%>"/></td></tr>
	<tr><td width="30%"><%=LC.L_Jndi_Port%><%--JNDI Port*****--%> : </td><td width="30%">  <input name = "jndiport" type="text"   value = "<%=jndiport%>" /></td></tr>
	<tr><td width="30%"><%=LC.L_JmsCon_Factory%><%--JMS Connection Factory*****--%> </td><td width="30%"><input name = "conn" READONLY type="text" value = <%=connFactory%> /></td></tr>
	<tr><td colspan = 2 align="right"><input name = "submit" type="submit" /></td></tr>

	
	<input name = "mode" type="hidden" value = "M"/>

</form>

      <% 
	 

String fullUri = "";



if (modeVal.equals("M"))
{
  %>
  	<table>
  <%
	
	fullUri = "jnp://"+ ip +":" + jndiport + "/" + connFactory;

	out.println("<tr class=\"browserOddRow\"><td>"+MC.M_StartTo_ConnectTo/*Starting to connect to*****/+" : "+fullUri  +" *******************</td></tr>");

try{
	      InitialContext ctx = new InitialContext();
	      TopicConnectionFactory cf = (TopicConnectionFactory) ctx.lookup(fullUri);


      out.println("<tr class=\"browserEvenRow\"><td>"+LC.L_Creating_Connection/*Creating  connection*****/+"....</td></tr>");
      
  TopicConnection connection = cf.createTopicConnection();
	TopicPublisher topicPublisher = null;
	TopicSession tsession = null;
	Topic topic = null;	
 
	ObjectMessage objMessage = null;
	


      
	 
	out.println("<tr class=\"browserOddRow\"><td>"+LC.L_Got_Connection/*Got connection*****/+"...." + connection + "</td></tr>");

	tsession = connection.createTopicSession(false,     Session.AUTO_ACKNOWLEDGE);

	out.println("<tr class=\"browserEvenRow\"><td>"+LC.L_Got_Sess/*Got session*****/+"...." + tsession + "</td></tr>");

	out.println("<tr class=\"browserOddRow\"><td>"+LC.L_LookupTopic_Jnp/*lookup topic....jnp*****/+"://"+ ip +":" + jndiport + "/" +  "topic/sponsor "+ "</td></tr>");

	topic = (Topic) ctx.lookup("jnp://"+ ip +":" + jndiport + "/" +  "topic/sponsor");

	out.println("<tr class=\"browserEvenRow\"><td>"+LC.L_Got_Topic/*Got topic*****/+"...." + topic + "</td></tr>");
	
	topicPublisher = tsession.createPublisher(topic);

 	objMessage = tsession.createObjectMessage();
        objMessage.setStringProperty("SSSSSvelosExpId","999999");

	
	out.println("<tr class=\"browserOddRow\"><td>"+MC.M_Pubr_Pub_DummyMsg/*[Publisher]: Publish Dummy Message*****/+" "+ "</td></tr>");
        topicPublisher.publish( objMessage , DeliveryMode.NON_PERSISTENT, 1, 0);


	
     out.println("<tr class=\"browserEvenRow\"><td> "+LC.L_Closing_Connection/*Closing connection*****/+"...."+ "</td></tr>");
		if (topicPublisher != null)
		{

			topicPublisher.close();
		}
		if(tsession != null)
		{
			tsession.close(); 
		}

           connection.close();
        }
        catch (Exception ex)
        {
        	out.println("<tr class=\"browserEvenRow\"><td> "+MC.M_CldntCon_Excp/*Could not connect....Exception*****/+":"+ex+ "</td></tr>");
        }   
      
  %>
  </table>    
       
<%  
} 
 
%>     

	 

  
<DIV>
   </body>