<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<Link Rel=STYLESHEET HREF="common.css" type=text/css>

</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
<jsp:useBean id="codelstB" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="ctrltabB" scope="request" class="com.velos.eres.web.ctrltab.CtrltabJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*"%>
<%

String rights[],cnt,rght="0",ctrlval="";

int totrows =0,count=0; 

String role=request.getParameter("role");
String rolesubtype=request.getParameter("rolesubtype");
String mode=request.getParameter("mode");

String cid=request.getParameter("cid");
HttpSession tSession = request.getSession(true); 
String accId = (String) tSession.getValue("accountId");
 

 if (sessionmaint.isValidSession(tSession))
   {
 String ipAdd = (String) tSession.getValue("ipAdd");
 String usr = null;
 usr = (String) tSession.getValue("userId");

 totrows = Integer.parseInt(request.getParameter("totalrows")); //Total Rows


if (totrows > 1){

	rights = request.getParameterValues("rights");

	for (count=0;count<totrows;count++){
	rght = rights[count];
	cnt = String.valueOf(count);
	ctrlval=ctrlval+rght;

	}

}else{
	rght = request.getParameter("rights");
	ctrlval=rght;
}

if(mode.equals("M"))
{
	int icid=Integer.parseInt(cid);
	codelstB.setClstId(icid);
	codelstB.getCodelstDetails();
	String subType=codelstB.getClstSubType();	
	codelstB.setClstSubType(rolesubtype);
	codelstB.setClstDesc(role);
	codelstB.setCreator(usr);
	codelstB.setModifiedBy(usr);
	codelstB.setIpAdd(ipAdd);	
	int iupret=codelstB.updateCodelst();
	if (iupret == -3)
{%>
	<br><br><br><br><br>
	<p class = "successfulmsg" align = center > <%=MC.M_UnqRoleSubTyp_EtrSubType%><%--The unique Role Subtype name already exists. Please enter a different SubType name*****--%></p>
			<center><button tabindex=2 onClick="history.go(-1);"><%=LC.L_Back%></button>
<%
}
else{
	CtrlDao ctrlDao=new CtrlDao();
	int pkctrltab=ctrlDao.getPkCtrlTab(subType);
		
	ctrltabB.setCtabId(pkctrltab);
	ctrltabB.getCtrltabDetails();
	
	ctrltabB.setCtabCtrlValue(ctrlval);
	ctrltabB.setCtabCtrlDesc(role);
	ctrltabB.setCtabCtrlKey(rolesubtype);
	ctrltabB.setCreator(usr);
	ctrltabB.setModifiedBy(usr);
	ctrltabB.setIpAdd(ipAdd);
	int cupret=ctrltabB.updateCtrltab();
		
%>
<br>
<br>
<br>
<br>
<br>
<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%> </p>
<script>
		window.opener.location.reload();
		setTimeout("self.close()",1000);
</script>
<%
}
}
else
{

codelstB.setClstType("role");
codelstB.setClstSubType(rolesubtype);
codelstB.setClstDesc(role);
codelstB.setCreator(usr);
codelstB.setModifiedBy(usr);
codelstB.setIpAdd(ipAdd);
int ret=codelstB.setCodelstDetails();

if (ret == -3)
{
%>
	<br><br><br><br><br>
	<p class = "successfulmsg" align = center > <%=MC.M_UnqRoleSubTyp_EtrSubType%><%--The unique Role Subtype name already exists. Please enter a different SubType name*****--%></p>
			<center><button tabindex=2 onClick="history.go(-1);"><%=LC.L_Back%></button>
<%
}
else{
ctrltabB.setCtabCtrlValue(ctrlval);
ctrltabB.setCtabCtrlDesc(role);
ctrltabB.setCtabCtrlKey(rolesubtype);
ctrltabB.setCreator(usr);
ctrltabB.setModifiedBy(usr);
ctrltabB.setIpAdd(ipAdd);
ctrltabB.setCtrltabDetails();

%>
<br>
<br>
<br>
<br>
<br>
<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%> </p>
<script>
		window.opener.location.reload();
		setTimeout("self.close()",1000);
</script>
<%
}

}
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

</HTML>





 