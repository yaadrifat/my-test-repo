<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML><HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>

<BODY>
  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
  <%@ page language = "java" import = "java.util.*,java.text.*,com.velos.eres.service.util.*,com.velos.eres.web.storage.*,com.velos.eres.web.storageStatus.*,com.velos.eres.business.common.*,com.velos.eres.web.storageAllowedItems.*, com.velos.eres.web.storageKit.StorageKitJB, com.velos.eres.web.storage.StorageJB, com.velos.eres.web.storageStatus.StorageStatusJB,com.velos.eres.web.reviewBoard.ReviewBoardJB,com.velos.eres.business.reviewboard.impl.ReviewBoardBean"%>
  <jsp:useBean id="reviewB" scope="request" class="com.velos.eres.web.reviewBoard.ReviewBoardJB" />
	<%
		String mode= request.getParameter("mode");
		if (EJBUtil.isEmpty(mode))
		{
			mode="N";
		}
		
		String eSign = request.getParameter("eSign");
		HttpSession tSession = request.getSession(true);
		if (sessionmaint.isValidSession(tSession))
		{
			String userId = (String) tSession.getValue("userId");
			String accId = (String) tSession.getValue("accountId");
			String ipAdd = (String) tSession.getValue("ipAdd");			
			String oldESign = (String) tSession.getValue("eSign");
			String tab = request.getParameter("selectedTab");
			String rbNameAdded = StringUtil.stripScript(request.getParameter("boardName"));
			int rbNameCount=reviewB.getRBNameCount(rbNameAdded);
			int rFkAccount=0;
			String rBrdGrpAccess="";
			String rMOMLogic="select PKG_IRBMOM_LOGIC.f_generateMOM(?,?) from dual";
			int rBoardDefault=0;				
			String rbId=request.getParameter("rbId");			
			if(!oldESign.equals(eSign))
			{
				%><jsp:include page="incorrectesign.jsp" flush="true"/> <%
			}
			else if (rbNameAdded==null)
			{
			%>
				<table align=center>
				<BR><BR><BR><BR><BR><BR><BR><BR><BR>						
				<p class = "successfulmsg" align = center><%=MC.M_BoardNameRow_PlzAdd%><%--No Board Name/Row added . Please add row and enter a board name.*****--%></p>
				<META HTTP-EQUIV=Refresh CONTENT="1; URL=irbReviewBoard.jsp?srcmenu=tdmenubaritem6&selectedTab=<%=tab%>">		
				</table>
			<%	
			}
			else 
			{
			if(rbNameAdded.length()>0 && rbNameCount==0)
				{
					String rBoardDesc=rbNameAdded;		
					reviewB.setReviewBoardName(rbNameAdded);	
					reviewB.setReviewBoardDescription(rBoardDesc);
					reviewB.setReviewboardAccId(rFkAccount);
					reviewB.setBoardGroupAccess(rBrdGrpAccess);
					reviewB.setBoardMomLogic(rMOMLogic);			
					reviewB.setReviewBoardDefault(rBoardDefault);
					reviewB.setIpAdd(ipAdd);
					reviewB.setCreator(userId);
					//reviewB.setModifiedBy(userId);
					reviewB.setReviewBoardDetails();					
			%>
			<jsp:include page="sessionlogging.jsp" flush="true"/>
				<table align=center>
				<BR><BR><BR><BR><BR><BR><BR><BR>				
				<p class = "successfulmsg" align = "center" ><%=MC.M_Data_SvdSucc%><%-- Data was Saved successfully.*****--%></p>
				<META HTTP-EQUIV=Refresh CONTENT="1; URL=irbReviewBoard.jsp?srcmenu=tdmenubaritem6&selectedTab=<%=tab%>">		
				</table>
			<%
					}
				else
					{
					%>
						<BR><BR><BR><BR><BR><BR><BR><BR>						
						<p class = "successfulmsg" align = center><%=MC.M_BoardNameExit_EtrNew%><%--The Board Name that you have entered already exists. Please enter a new name.*****--%></p>
						<form method="post">
						  <table align=center>
							<tr width="100%"> 
							  <td width="100%" > 	
							<button class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" onclick="window.history.back(); return false;"><%=LC.L_Back%></button>
							  </td>
							</tr>
						  </table>
						</form>
					<%				
					}
			}
		}
		else
		{
			%><jsp:include page="timeout.html" flush="true"/><%
		}
			%>
		<div class = "myHomebottomPanel">
	    <jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</BODY></HTML>
