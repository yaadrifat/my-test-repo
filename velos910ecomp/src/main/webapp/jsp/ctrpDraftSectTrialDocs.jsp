<%@taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC" %>
<%@page import="com.velos.eres.business.common.CtrlDao"%>

<script>
var studyID = '<s:property escape="false" value="ctrpDraftJB.fkStudy" />';
function openStudyAppdx(docPkval,docDisVal){	    
	windowName=window.open("getlookup.jsp?viewId=&viewNameKeyword=study_appendix&protocolid="+studyID+"&form=ctrpDraftNonIndustrialForm&dfilter=&keyword="+docPkval+"|PK_STUDYAPNDX~"+docDisVal+"|STUDYAPNDX_URI","Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=550 top=100,left=100");
	windowName.focus();
}
</script>
<table>
<tr><td colspan="3"><p class="defComments"><%=MC.M_Trial_Rel_Doc_Info%></p></td></tr>
	<tr>
	<td width="50%">
<table border="0">
     <tr height="10px">
		<td align="right" width="30%">
			<%=LC.L_Protocol_Docu%>
			<span id="ctrpDraftJB.protocolDocDesc_error" class="errorMessage" style="display:none;"></span>
		</td>
		<td width="1%">
			<FONT id="protocol_doc_asterisk" style="visibility:hidden" class="Mandatory">* </FONT>
		</td>
		<td align="left" valign="top">
			<s:textfield name="ctrpDraftJB.protocolDocDesc" id="ctrpDraftJB.protocolDocDesc" style="height:2em; width:15em;valign:top" readonly="true"/>
			<s:hidden name="ctrpDraftJB.protocolDocPK" id="ctrpDraftJB.protocolDocPK"  />
			<s:hidden name="ctrpDraftJB.trialProtocolDocPK" id="ctrpDraftJB.trialProtocolDocPK"  />
			<button type="button" id="select" name="select" onClick="openStudyAppdx('ctrpDraftJB.protocolDocPK','ctrpDraftJB.protocolDocDesc')"> <%=LC.L_Select%></button>
		</td>
	</tr>
	<tr>
		<td align="right">
			<%=LC.L_IRB_Approval%>
			<span id="ctrpDraftJB.IRBApprovalDesc_error" class="errorMessage" style="display:none;"></span>
		</td>
		<td>
			<FONT id="irb_App_Doc_asterisk" style="visibility:hidden" class="Mandatory">* </FONT>
		</td>
		<td align="left" valign="top">
			<s:textfield name="ctrpDraftJB.IRBApprovalDesc" id="ctrpDraftJB.IRBApprovalDesc" style="height:2em; width:15em;" readonly="true"/>
			<s:hidden name="ctrpDraftJB.IRBApprovalPK" id="ctrpDraftJB.IRBApprovalPK"  />
			<s:hidden name="ctrpDraftJB.trialIRBApprovalPK" id="ctrpDraftJB.trialIRBApprovalPK"  />
			<button type="button" id="select" name="select" onClick="openStudyAppdx('ctrpDraftJB.IRBApprovalPK','ctrpDraftJB.IRBApprovalDesc')"> <%=LC.L_Select%></button>
		</td>
	</tr>
	<tr>
		<td align="right">
			<%=LC.L_ListOf_Part_Sites%> 
			<span id="ctrpDraftJB.partSitesListDesc_error" class="errorMessage" style="display:none;"></span>
		</td>
        <td>&nbsp;&nbsp;</td>
		<td align="left" valign="top">
			<s:textfield name="ctrpDraftJB.partSitesListDesc" id="ctrpDraftJB.partSitesListDesc" style="height:2em; width:15em;" readonly="true"/>
			<s:hidden name="ctrpDraftJB.partSitesListPK" id="ctrpDraftJB.partSitesListPK" />
			<s:hidden name="ctrpDraftJB.trialPartSitesListPK" id="ctrpDraftJB.trialPartSitesListPK" />
			<button type="button" id="select" name="select" onClick="openStudyAppdx('ctrpDraftJB.partSitesListPK','ctrpDraftJB.partSitesListDesc')"> <%=LC.L_Select%></button>
		</td>
	</tr>
	<tr>
		<td align="right">
			<%=LC.L_Inform_Cons_Docu%> 
			<span id="ctrpDraftJB.informedConsentDesc_error" class="errorMessage" style="display:none;"></span>
		</td>
        <td>&nbsp;&nbsp;</td>
		<td align="left" valign="top">
			<s:textfield name="ctrpDraftJB.informedConsentDesc" id="ctrpDraftJB.informedConsentDesc" style="height:2em; width:15em;" readonly="true"/>
			<s:hidden name="ctrpDraftJB.informedConsentPK" id="ctrpDraftJB.informedConsentPK" />
			<s:hidden name="ctrpDraftJB.trialInformedConsentPK" id="ctrpDraftJB.trialInformedConsentPK" />
			<button type="button" id="select" name="select" onClick="openStudyAppdx('ctrpDraftJB.informedConsentPK','ctrpDraftJB.informedConsentDesc')"> <%=LC.L_Select%></button>
		</td>
	</tr>
	<tr>
		<td align="right">
			<%=LC.L_Other%>
			<span id="ctrpDraftJB.otherDesc_error" class="errorMessage" style="display:none;"></span>
		</td>
        <td>&nbsp;&nbsp;</td>
		<td align="left" valign="top">
			<s:textfield name="ctrpDraftJB.otherDesc" id="ctrpDraftJB.otherDesc" style="height:2em; width:15em;" readonly="true"/>
			<s:hidden name="ctrpDraftJB.otherPK" id="ctrpDraftJB.otherPK" />
			<s:hidden name="ctrpDraftJB.trialOtherPK" id="ctrpDraftJB.trialOtherPK" />
			<button type="button" id="select" name="select" onClick="openStudyAppdx('ctrpDraftJB.otherPK','ctrpDraftJB.otherDesc')"> <%=LC.L_Select%></button>
		</td>
	</tr>
	<tr>
		<td align="right">
			<%=LC.L_Change_Memo_Docu%>
			<span id="ctrpDraftJB.changeMemoDocDesc_error" class="errorMessage" style="display:none;"></span>
		</td>
		<td>
			<FONT id="change_memo_Doc_asterisk" style="visibility:hidden" class="Mandatory">* </FONT>
		</td>
		<td align="left" valign="top">
			<s:textfield name="ctrpDraftJB.changeMemoDocDesc" id="ctrpDraftJB.changeMemoDocDesc" style="height:2em; width:15em;" readonly="true"/>
			<s:hidden name="ctrpDraftJB.changeMemoDocPK" id="ctrpDraftJB.changeMemoDocPK"  />
			<s:hidden name="ctrpDraftJB.trialChangeMemoDocPK" id="ctrpDraftJB.trialChangeMemoDocPK" />
			<button type="button" id="select" name="select" onClick="openStudyAppdx('ctrpDraftJB.changeMemoDocPK','ctrpDraftJB.changeMemoDocDesc')"> <%=LC.L_Select%></button>&nbsp;<i><%=LC.L_Req_For_Amendment%></i>
		</td>
	</tr>
	
	<tr>
		<td align="right">
			<%=LC.L_Prot_HighLight_Docu%>
			<span id="ctrpDraftJB.protHighlightDesc_error" class="errorMessage" style="display:none;"></span>
		</td>
        <td>&nbsp;&nbsp;</td>
		<td align="left" valign="top">
			<s:textfield name="ctrpDraftJB.protHighlightDesc" id="ctrpDraftJB.protHighlightDesc" style="height:2em; width:15em;" readonly="true"/>
			<s:hidden name="ctrpDraftJB.protHighlightPK" id="ctrpDraftJB.protHighlightPK" />
			<s:hidden name="ctrpDraftJB.trialProtHighlightPK" id="ctrpDraftJB.trialProtHighlightPK" />
			<button type="button" id="select" name="select" onClick="openStudyAppdx('ctrpDraftJB.protHighlightPK','ctrpDraftJB.protHighlightDesc')"> <%=LC.L_Select%></button>&nbsp;<i><%=LC.L_Amendment_Only%></i>
		</td>
	</tr>
	<tr height="10px"><td colspan="3"></td></tr>
</table>
</td>
	<td width="50%">
</td>
</tr>
<tr>
		<td colspan="4"><p class="defComments"><%=MC.M_Trial_Document_Formats %></p></td>
	</tr>
</table>
