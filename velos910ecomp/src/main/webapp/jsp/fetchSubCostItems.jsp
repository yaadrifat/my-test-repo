<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<style type="text/css">
.yui-dt-liner { white-space:normal; } 
.yui-skin-sam .yui-dt td.up { background-color: #efe; } 
.yui-skin-sam .yui-dt td.down { background-color: #fee; }
</style>
<%@page import="com.velos.eres.web.objectSettings.ObjectSettingsCache,com.velos.eres.web.objectSettings.ObjectSettings"%>
<%@page import="com.velos.eres.web.grpRights.GrpRightsJB"%>
<%@page import="com.velos.eres.business.ulink.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.EJBUtil"%>
<%@page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*" %>
<%@page import="com.velos.esch.business.common.SchCodeDao"%>
<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<%
ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
ArrayList tabList = null;
String titleStr = LC.L_Calendar;/*String titleStr = "Calendar";*****/
if (sessionmaint.isValidSession(request.getSession(true)))
{
    String accId = (String) request.getSession(true).getValue("accountId");
    
    ArrayList tempList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "protocol_tab");
    for (int iX=0; iX<tempList.size(); iX++) {
        ObjectSettings settings = (ObjectSettings) tempList.get(iX);
        if ("8".equals(settings.getObjSubType())) {
            if (settings.getObjDispTxt() != null) {
            	Object[] arguments = {titleStr,settings.getObjDispTxt()};
            	titleStr = VelosResourceBundle.getLabelString("L_Gtgt",arguments);
                /*titleStr +=  " >> " + settings.getObjDispTxt();*****/
            }
            break;
        } 
    }
    tempList = null;
}
String src= StringUtil.htmlEncodeXss(request.getParameter("srcmenu"));
%>
<title><%=titleStr %></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<jsp:include page="ui-include.jsp" flush="true"/>
</head>

<% 
HttpSession tSession = request.getSession(true); 
String accountId = "";
String studyId = "";
String grpId = "";
String usrId = "";
if (sessionmaint.isValidSession(tSession)) {
	accountId = (String) tSession.getValue("accountId");
	studyId = (String) tSession.getValue("studyId");
	grpId = (String) tSession.getValue("defUserGroup");
	usrId = (String) tSession.getValue("userId");
	
	if ("N".equals(request.getParameter("mode"))) {
		studyId = "";
		tSession.removeAttribute("studyId");
	}
%>
<body class="yui-skin-sam yui-dt yui-dt-liner">
<div id="overDiv" style="position:absolute;visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
<%
	String from = "version";
	String tab = request.getParameter("selectedTab");
    String calassoc = StringUtil.trueValue(request.getParameter("calassoc"));
	String studyIdForTabs = request.getParameter("studyId");
    String includeTabsJsp = "irbnewtabs.jsp";
    int pageRight = 0;
	String calledFrom = request.getParameter("calledFrom");
	if (calledFrom.equals("S")) {
		StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");
		if ((stdRights.getFtrRights().size()) == 0){
			pageRight= 0;
		} else {
			pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));
		}
	} else if ((calledFrom.equals("P")) || (calledFrom.equals("L"))) {
		GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));
	}
	%>
	<DIV class="BrowserTopn" id = "div1">
	<jsp:include page="protocoltabs.jsp" flush="true">
	<jsp:param name="calassoc" value="<%=StringUtil.htmlEncodeXss(calassoc)%>" />
	<jsp:param name="selectedTab" value="<%=StringUtil.htmlEncodeXss(tab)%>" />
	</jsp:include>
</DIV>
	<%
	if (pageRight > 0) {
%>

<DIV class="BrowserBotN BrowserBotN_CL_1" id = "div2">
<%
	} // end of pageRight > 0
	String protocolId = request.getParameter("protocolId");
	if(protocolId == null || protocolId == "" || protocolId.equals("null") || protocolId.equals("")) {
%>
	<jsp:include page="calDoesNotExist.jsp" flush="true"/>
<%
	} else {
	String calStatus = null;
	String calStatusPk = "";
	String calStatDesc = "";
	SchCodeDao scho = new SchCodeDao();
	if (pageRight > 0) {
	    String tableName = null;
 		if("L".equals(calledFrom) || "P".equals(calledFrom)){
	   		tableName ="event_def";
			eventdefB.setEvent_id(EJBUtil.stringToNum(protocolId));
			eventdefB.getEventdefDetails();
			//KM-#DFin9
			calStatusPk = eventdefB.getStatCode();
			calStatus = scho.getCodeSubtype(EJBUtil.stringToNum(calStatusPk)).trim();
			calStatDesc = scho.getCodeDescription(EJBUtil.stringToNum(calStatusPk));


	   	} else {
	   		tableName ="event_assoc";
			eventassocB.setEvent_id(EJBUtil.stringToNum(protocolId));
			eventassocB.getEventAssocDetails();
			//KM-#DFin9
			calStatusPk = eventassocB.getStatCode();
			calStatus = scho.getCodeSubtype(EJBUtil.stringToNum(calStatusPk)).trim();
			calStatDesc = scho.getCodeDescription(EJBUtil.stringToNum(calStatusPk));

	   	}
		String duration = StringUtil.trueValue(request.getParameter("duration"));
		StringBuffer urlParamSB = new StringBuffer();
		urlParamSB.append("protocolId=").append(StringUtil.htmlEncodeXss(protocolId))
		.append("&calledFrom=").append(StringUtil.htmlEncodeXss(calledFrom))
		.append("&calledfrom=").append(StringUtil.htmlEncodeXss(calledFrom))
		.append("&duration=").append(StringUtil.htmlEncodeXss(duration))
		.append("&calassoc=").append(StringUtil.htmlEncodeXss(calassoc))
		.append("&calstatus=").append(StringUtil.htmlEncodeXss(request.getParameter("calStatus")))
		.append("&mode=").append(StringUtil.htmlEncodeXss(request.getParameter("mode")))
		.append("&calProtocolId=").append(StringUtil.htmlEncodeXss(protocolId))
		.append("&tableName=").append(tableName)
		;
%>
<script>
function reloadSubjectGrid() {
	YAHOO.example.SubjectGrid = function() {
		var args = {
			urlParams: "<%=urlParamSB.toString()%>",
			dataTable: "subjectgrid"
		};
		
		mySubjectGrid = new VELOS.subjectGrid('fetchSCIJSON.jsp', args);
		mySubjectGrid.startRequest();
    }();
}
YAHOO.util.Event.addListener(window, "load", function() {
	reloadSubjectGrid();
});

function validateNumber(evt){
	if(evt.shiftKey) return false;
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	//delete, backspace & arrow keys
	if (charCode == 8 || charCode == 46 || charCode == 37 || charCode == 39)
		return true;

	if (charCode > 31 && (!((47 < charCode && charCode  < 58) || (95 < charCode &&  charCode < 106))))
		return false;
	return true;
}
</script>

<%
%>
  <input type="hidden" id ="pageRight" name="pageRight" value="<%=pageRight%>"/>
  <table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl midAlign" >
    <tr height="7"></tr>
  <tr>
   <!--KM-#DFin9-->
  <td width="18%"><P class="defComments"><%=LC.L_Cal_Status%><%--Calendar Status*****--%>: <%=calStatDesc%>&nbsp;&nbsp;&nbsp;&nbsp;</P></td>
  <td>  
    <%if ((calStatus.equals("A")) || (calStatus.equals("F")) || (calStatus.equals("D")))
		{%>
	       <FONT class="Mandatory"><%=MC.M_Changes_CntSvd%><%--Changes cannot be saved*****--%></Font>&nbsp;&nbsp;&nbsp;&nbsp;
	<%}else{%>
		<%if (isAccessibleFor(pageRight, 'N')||isAccessibleFor(pageRight, 'E')){%> 
  			<FONT class='Mandatory'>(<%=MC.M_ClkRow_ToEdt%><%--Click row to edit*****--%>)</FONT>&nbsp;&nbsp;&nbsp;&nbsp;
  		<%} %>
  	<%} %>
  		<button type="submit" id="save_changes" name="save_changes"
		<%if ((!isAccessibleFor(pageRight, 'N')) && (!isAccessibleFor(pageRight, 'E'))){%>
		style="display:none"
		<%} %>
    	onclick="if (f_check_perm_noAlert(<%=pageRight%>,'N') || f_check_perm_noAlert(<%=pageRight%>,'E')) { VELOS.subjectGrid.saveDialog(event); }"><%=LC.L_Preview_AndSave%><%--Preview and Save*****--%></button>
  </td>
  </tr> 
  <tr></tr>
  <%if (isAccessibleFor(pageRight, 'N')){%>
  <tr>
  <%if (!(calStatus.equals("A") || calStatus.equals("F") || calStatus.equals("D"))){%>
 	<td align="right" colspan="2">
  		<%=LC.L_Add%><%--Add*****--%> &nbsp;<input id="rowCount" type="text" value="0" size="2" maxlength="2" class="index"
  		onkeydown = "return validateNumber(event);"/>  <%=LC.L_AddRow_S%><%--Add Row(s)*****--%>
		<span>
			<img id="addRows" name="addRows" title="<%=LC.L_Add%>" alt="<%=LC.L_Add%>" src="../images/add.png" onclick="if (f_check_perm(<%=pageRight%>,'N')) { if(eval(document.getElementById('rowCount').value) > 20){ alert('<%=MC.M_AddMax_20Rows%><%--Please add maximum 20 rows at a time.*****--%>'); return;} if(eval(document.getElementById('rowCount').value) > 0){ VELOS.subjectGrid.addRows(); }}"/>
		</span> 
		<span width="50%">&nbsp;</span>
  	</td>
  <%} %>
  </tr>
  <%} %>
  <tr height="7"></tr>  
  </table>
  <div id="subjectgrid"></div>
<%
  } // end of pageRight
  else {
%>
<DIV class="BrowserBotN" id="div2">
	<jsp:include page="accessdenied.jsp" flush="true"/>
</DIV>
<%
  } // end of else of pageRight
} // end of else of invalid protocolId
} else { // else of valid session
%>
  <jsp:include page="timeout.html" flush="true"/>
<%
} // end of else of valid session
%>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id = "emenu" > 
  <jsp:include page="getmenu.jsp" flush="true"/> 
</div>
</body>
</html>
<%!
private boolean isAccessibleFor(int pageRight, char type) {
    switch(type) {
    case 'V': return ((pageRight & 0x0004) >> 2) == 1;
    case 'E': return ((pageRight & 0x0002) >> 1) == 1;
    case 'N': return  (pageRight & 0x0001) == 1;
    }
    return false;
}
%>