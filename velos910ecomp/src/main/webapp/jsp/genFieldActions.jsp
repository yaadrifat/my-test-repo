<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>


<meta name="Generator" content="AceHTML 4 Freeware">
</head>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="formLibJB" scope="request"  class="com.velos.eres.web.formLib.FormLibJB"/>
<%@page language="java" import="com.velos.eres.tags.*,java.util.*,com.velos.eres.business.common.*,java.sql.*,com.velos.eres.service.util.*"%>

<body>

<%
	// this code needs to be transferred to FormLibDao
	String formId = "";
    
    formId = request.getParameter("formId");	
    int formIdInt = 0;
    
    formIdInt = EJBUtil.stringToNum(formId);
    
	Hashtable htData = new Hashtable();
	htData = VFieldAction.getFieldActions(formId);

	StringBuffer sbZeroScr = new StringBuffer();
	StringBuffer sbFieldScr = new StringBuffer();
	StringBuffer sbFormActivationScr = new StringBuffer();
	StringBuffer sbFieldTypeCheck = new StringBuffer();
	
	StringBuffer sbFinal = new StringBuffer();
	
	sbFinal.append(" var actualField ; if (currentfld.type == undefined) { actualField = currentfld[0]; } else { actualField = currentfld; } ");
	

	System.out.println("htData.size()" + htData.size());
	
    for (Enumeration e = htData.keys() ; e.hasMoreElements() ;) {
    	String htFieldkey = "";
	   
         htFieldkey = (String) e.nextElement();
	    
	    ArrayList arObjs = new ArrayList();
	    arObjs = (ArrayList) htData.get(htFieldkey );
		
		System.out.println("arObjs .size()" + arObjs.size());

 	    StringBuffer sbscr = new StringBuffer();
		
		for (int i=0 ; i < arObjs.size() ; i++)
		{
		   	
		   VFieldAction vf = new VFieldAction();
		   ArrayList arSource = new ArrayList();
	   	   
	  	   StringBuffer sbBeginBlock = new StringBuffer();
		   String srcField = "";
		   StringBuffer srcCheckbox = new StringBuffer();

		   
		   String scr = "";
		   vf = (VFieldAction )arObjs.get(i);
		   
		   arSource = (ArrayList) vf.getHtKeywordValues("[VELSOURCE]");
		   

		   for (int j = 0; j < arSource.size(); j++)
		   	{
		   	  srcField = (String) arSource.get(j);
		   	  
  	  		   if (j == 0)
			   {
				  sbBeginBlock.append("if (actualField.name == '" + srcField  + "'");
			   }
			   else
			   {
			   	  sbBeginBlock.append(" || actualField.name == '" + srcField  + "' ");

			   }
			 
			
			}   	
			
		   if (i == 0 && ! (vf.getSourceField()).equals("0"))
			   {
				sbscr.append(sbBeginBlock.toString()); //for main if block
				sbscr.append(srcCheckbox.toString()); //for main if block
				
		   		sbscr.append(") { ");		   	
			    sbscr.append("\n");		    	   
			   	} 
		   	//after theloop of source field
	       	
			 sbscr.append(sbBeginBlock.toString());//for individual field action of a field		   	
		 	 sbscr.append(srcCheckbox.toString()); //for individual field action of a field		   	
	 	 	 sbscr.append(") { ");	
			 sbscr.append("\n");		   	
			
			 scr = vf.getActionScript();
			 sbscr.append(scr);
			 sbscr.append("\n } \n");		
			 
			 
			 sbFormActivationScr.append("<script>" + scr + "</script>");
			 
		   
		   //if last field action append a return statement and a closing '}' for 	the field
		   if ( i == arObjs.size() - 1)
		  {
		  	  	  
			  if (! (vf.getSourceField()).equals("0"))
			  { 			
			   sbscr.append("return ; ");		
      		   sbscr.append("\n } \n");		//for main loop for field
   		   	   sbFieldScr.append(sbscr.toString());
			  }
			  else 
			  {
   		   	   sbZeroScr.append(sbscr.toString());	  
			  }
		  }
		  
		}

	
	  }	//for iterating through the hashtable
	  	 
	 
	 	//update the form_custom_js
	 		
	 	sbFinal.append(sbZeroScr.toString());	
	 	sbFinal.append(sbFieldScr.toString());	
	 		
	 	Connection conn = null;
	    PreparedStatement stmt = null;
		String sqlStr = "";
		CommonDAO cdao = new CommonDAO();

    try
	{
	     conn = cdao.getConnection();
		
		 sqlStr = " Update er_formlib set form_custom_js = ?, FORM_ACTIVATION_JS = ? where pk_formlib = ?";	    

	 	 stmt = conn.prepareStatement(sqlStr);
	     stmt.setClob(1,EJBUtil.getCLOB(sbFinal.toString(),conn));
   	     stmt.setClob(2,EJBUtil.getCLOB(sbFormActivationScr.toString(),conn));
	     stmt.setInt(3,formIdInt);	 	  
	   	 stmt.execute();	
	   	 
	   	 if (! conn.getAutoCommit())
	   	 {
	   	 	 conn.commit();
	   	 }

	   	formLibJB.setFormLibId(formIdInt);
		formLibJB.getFormLibDetails();
		formLibJB.setFormLibXslRefresh("1");
		formLibJB.updateFormLib();
	   	 
	   	 
 	}
   	catch (Exception ex) 
  	{
    	System.out.println("Could not update form_custom_js" + ex);		
   	}				
   	finally
   	{
   		try
    		{
    			if (stmt != null) stmt.close();
    	  	}
    		catch (Exception e) 
    		{
    		}

    		try 
    		{
    			if (conn!=null) conn.close();
    		}
    		catch (Exception e) {}

    		
   	}
  
	 	//end of update	
	 	
%>
</body>
</html>


