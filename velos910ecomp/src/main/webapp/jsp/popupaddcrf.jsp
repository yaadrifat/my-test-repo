<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title><%=LC.L_Add_NewCrf%><%-- Add New CRF*****--%></title>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<SCRIPT>

 function  validate(formobj){
	 count=0;
	for(i=0;i<5;i++)
	 {
		 if(formobj.crflibName[i].value!="")
		 {
			count++;
		 }
		 if(formobj.crflibNumber[i].value!="" && formobj.crflibName[i].value=="")
		 {
			 alert("<%=MC.M_Etr_CrfName%>");/*alert("Please enter Crf Name");*****/
			formobj.crflibName[i].focus();
			return false;
		 }
		 if(formobj.crflibNumber[i].value=="" && formobj.crflibName[i].value!="")
		 {
			 alert("<%=MC.M_Etr_CrfName%>");/*alert("Please enter Crf Number");*****/
			formobj.crflibNumber[i].focus();
			return false;
		 }
	 }

	 if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {

		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/

	formobj.eSign.focus();

	return false;

   }

	 if(count==0)
	 {
		 alert("<%=MC.M_Etr_MandantoryFlds%>");/*alert("Please enter data in all mandatory fields");*****/
		 formobj.crflibNumber[0].focus();
		 return false;
	 }




//if (!(validate_col('CRF Name',formobj.crflibName(0)))) return false



   }


</SCRIPT>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.esch.web.crflib.CrflibJB,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*"%>

<jsp:useBean id="crflibB" scope="request" class="com.velos.esch.web.crflib.CrflibJB"/>

<jsp:useBean id="codelstB" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>

<jsp:include page="include.jsp" flush="true"/>

<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>

<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>

<%

String src;

src= request.getParameter("srcmenu");
//SV, commented 10/28/04, added event name handling, cal-enh
//String eventName = request.getParameter("eventName");
String eventName = "";

%>




<body id="forms">

<DIV class="popDefault" id="div1">
<br>


<%

	int pageRight = 0;

	String eventmode = request.getParameter("eventmode");

	String duration = request.getParameter("duration");

	String protocolId = request.getParameter("protocolId");

	String calledFrom = request.getParameter("calledFrom");


	String fromPage = request.getParameter("fromPage");
	String mode = request.getParameter("mode");

	String calStatus= request.getParameter("calStatus");

	String displayDur=request.getParameter("displayDur");

	String displayType=request.getParameter("displayType");


  	String eventId = request.getParameter("eventId");

	String crflibmode=request.getParameter("crflibmode");

	String crflibId=request.getParameter("crflibId");
	String selectedTab=request.getParameter("selectedTab");

	String categoryId = "";


	String eventDesc = "";

	String crflibNumber="";

	String crflibName="";


	ArrayList crfNames = new ArrayList();

	ArrayList crfNumbers = new ArrayList();

//KM

	if (calledFrom.equals("P")||calledFrom.equals("L"))
    {
       	  eventdefB.setEvent_id(EJBUtil.stringToNum(eventId));
       	  eventdefB.getEventdefDetails();
          eventName = eventdefB.getName();
//JM: 12Nov2009: #4399
	  	  eventName = (eventName==null)?"":eventName;
	  	  if (eventName.length()>1000){//for the old existing event names > 1000 or larger
	  		eventName=eventName.substring(0,1000);
	  	  }
    }else{
       	  eventassocB.setEvent_id(EJBUtil.stringToNum(eventId));
          eventassocB.getEventAssocDetails();
       	  eventName = eventassocB.getName();
//JM: 12Nov2009: #4399
	  	  eventName = (eventName==null)?"":eventName;
	  	  if (eventName.length()>1000){//for the old existing event names > 1000 or larger
	  		eventName=eventName.substring(0,1000);
	  	  }

     }



	HttpSession tSession = request.getSession(true);

	String users ="";

	if (sessionmaint.isValidSession(tSession))	{



		if (calledFrom.equals("S")) {

   	   	   StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");

	  	   if ((stdRights.getFtrRights().size()) == 0){

			 	pageRight= 0;

		   }else{

				pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));

    	   }

		} else if (calledFrom.equals("P")) {

				GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");

				pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));
		}

		else if(calledFrom.equals("L")){

				GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");

				pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("EVLIB"));

		}




		String uName = (String) tSession.getValue("userName");

	%>

	  <%

		String calName = "";
		if (fromPage.equals("eventbrowser") || fromPage.equals("eventlibrary") || fromPage.equals("patientschedule") )

		{

	%>


	<%

		} else

		{

		calName = (String) tSession.getValue("protocolname");

	%>

	<P class="sectionHeadings"><%Object[] arguments = {calName}; %>
	    <%=VelosResourceBundle.getMessageString("M_PcolCal_AddCrf",arguments)%><%-- Protocol Calendar [ <%=calName%> ] >> Add Crf*****--%> </P>

	<%

		 }
	%>

<% if (calledFrom.equals("S")) {%>
  <!--<jsp:include page="crftabs.jsp" flush="true">
  <jsp:param name="mode" value="<%=eventmode%>"/>
  <jsp:param name="duration" value="<%=duration%>"/>
  <jsp:param name="protocolId" value="<%=protocolId%>"/>
  <jsp:param name="calledFrom" value="<%=calledFrom%>"/>
  <jsp:param name="fromPage" value="<%=fromPage%>"/>
  <jsp:param name="mode" value="<%=mode%>"/>
  <jsp:param name="calStatus" value="<%=calStatus%>"/>
  <jsp:param name="displayDur" value="<%=displayDur%>"/>
  <jsp:param name="eventId" value="<%=eventId%>"/>
  <jsp:param name="crflibId" value="<%=crflibId%>"/>
  <jsp:param name="src" value="<%=src%>"/>
  <jsp:param name="selectedTab" value="<%=selectedTab%>"/>
  <jsp:param name="eventId" value="<%=eventId%>"/>
  </jsp:include>-->
<%} else if (fromPage.equals("fetchProt")){%>
  <jsp:include page="eventtabs.jsp" flush="true">
  <jsp:param name="duration" value="<%=duration%>"/>
  <jsp:param name="protocolId" value="<%=protocolId%>"/>
  <jsp:param name="calledFrom" value="<%=calledFrom%>"/>
  <jsp:param name="fromPage" value="<%=fromPage%>"/>
  <jsp:param name="mode" value="<%=mode%>"/>
  <jsp:param name="calStatus" value="<%=calStatus%>"/>
  <jsp:param name="displayDur" value="<%=displayDur%>"/>
  <jsp:param name="displayType" value="<%=displayType%>"/>
  <jsp:param name="eventId" value="<%=eventId%>"/>
  <jsp:param name="src" value="<%=src%>"/>
  <jsp:param name="selectedTab" value="6"/>
  <jsp:param name="eventName" value="<%=eventName%>"/>
  </jsp:include>
<%}%>
   <%

	 if(eventId == "" || eventId == null) {

	%>

	  <jsp:include page="eventDoesNotExist.jsp" flush="true"/>

  <%

	}else {



		if(crflibmode.equals("M")){

			  crflibB.setCrflibId(EJBUtil.stringToNum(crflibId));
			  crflibB.getCrflibDetails();
	     	  crflibNumber=crflibB.getCrflibNumber();
			  crflibName=crflibB.getCrflibName();
		}

	%>
<form name="crflib" id="crflibfrm" METHOD=POST action="popupaddcrfsave.jsp?srcmenu=<%=src%>&eventmode=<%=eventmode%>&eventId=<%=eventId%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>" onsubmit="if (validate(document.crflib)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

<%
if (pageRight == 4) {

%>

   <P class = "defComments"><FONT class="Mandatory"><%=MC.M_OnlyViewPerm_ForEvt%><%-- You have only View permission for the Event.*****--%></FONT></P>

<%}
int i=0;
%>
<TABLE width="98%" cellspacing="0" cellpadding="0" border="0" class="basetbl" >
<tr>
<th width="20%" align=center> <%=LC.L_Crf_Number%><%-- CRF Number*****--%> <FONT class="Mandatory">* </FONT></th>
<th width="30%" align=center> <%=LC.L_Crf_Name%><%-- CRF Name*****--%> <FONT class="Mandatory">* </FONT></th>
</tr>
<%for(i=0;i<5;i++){%>
<tr>


	<td width="20%" align=center><br> <INPUT NAME="crflibNumber" maxlength=50> </td>
	<td width="30%" align=center><br><INPUT NAME="crflibName" size=30  type=text maxlength=100> </td>
	<!-- <td > <INPUT NAME="crflibmode" value=<%=crflibmode%> type=hidden > </td>
	<td > <INPUT NAME="crflibId" value=<%=crflibId%> type=hidden > </td> -->
	<td > <INPUT NAME="displayType" value=<%=displayType%> type=hidden > </td>
	<td > <INPUT NAME="displayDur" value=<%=displayDur%> type=hidden > </td>

<td width="30%">&nbsp;
			 </td>
</tr>

<%}%>





<Br>


<jsp:include page="propagateEventUpdate.jsp" flush="true">
<jsp:param name="fromPage" value="<%=fromPage%>"/>
<jsp:param name="formName" value="crflib"/>
<jsp:param name="eventName" value="<%=eventName%>"/>
</jsp:include>

</Table>
<table width="80%">
<tr>
<td style="width: 60%">
<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="crflibfrm"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>
</td>
<%--Modified By Yogendra: Bug# 7986 --%>
<%if(fromPage.equalsIgnoreCase("fetchProt")){%>
	<td style="width: 30%"><input class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-button-text" type="button" onclick="window.history.back();" value="<%=LC.L_Back%>" name="<%=LC.L_Back%>"></input></td>
<%} %>
</tr>

 </Table>
<table width=75% >

  <tr>

  <td>




 </td>



  </tr>

</table>



</form>



<%

	} //event



} else {  //else of if body for session



%>

  <jsp:include page="timeout.html" flush="true"/>

  <%



}



%>



  <%if ((request.getParameter("fromPage")).equals("selectEvent")){}

else {

%>




</div>



	<% }%>
<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</body>


</html>

