<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>

<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


<title><%=MC.M_SrchUsr_ByID%><%--Search User by User ID*****--%></title>

<Link Rel=STYLESHEET HREF="common.css" type=text/css>


<SCRIPT Language="javascript">

	
function back(id,name) {

	//alert("Check");
	window.opener.document.user.userId.value = id;
	window.opener.document.user.userName.value = name;
	self.close();
}

</SCRIPT>

<% response.setHeader("Cache-Control", "max-age=1, must-revalidate");%>

<jsp:useBean id="userB" scope="session" class="com.velos.eres.web.user.UserJB"/><%@page import="com.velos.eres.service.util.MC"%><%@page import="com.velos.eres.service.util.LC"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil"%>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>



<body style="overflow:scroll">
<%HttpSession tSession = request.getSession(true);
  if (sessionmaint.isValidSession(tSession))
{
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
	String from=request.getParameter("from") ;
%>
<BR>
<P class="defComments"><%=LC.L_Search_Criteria%><%--Search Criteria*****--%></P>
<br>
<Form  name="search" method="post" action="userSearchByPin.jsp">
  <input type=hidden name=from value=<%=from%>>
  <Input type="hidden" name="selectedTab" value="1">
  <table class = tableDefault width="100%" cellspacing="0" cellpadding="0" border=0 >
    <tr> 
      <td class=tdDefault width=20%> <%=LC.L_User_Search%><%--User Search*****--%></td>
      <td class=tdDefault width=30%> <%=LC.L_User_Id%><%--User ID*****--%>: 
        <Input type=text name="userPin">
      </td>
      <td class=tdDefault width=10%> 
        <!--<Input type="submit" name="submit" value="Go"> -->
		<button type="submit"><%=LC.L_Search%></button>
      </td>
    </tr>
  </table>
</Form>
<DIV > 
  <%
	String uName = (String) tSession.getValue("userName");
	int pageRight = 0;
	String accId = (String) tSession.getValue("accountId");
	String userPin= request.getParameter("userPin") ;
	
	if (userPin.equals("")){%>
  <P class="defComments"> <%=MC.M_PlsSrchCrit_ClkGo%><%--Please specify the Search criteria and then click on 
    'Search' to view the User List*****--%></P>
  <%}else{
		UserDao userDao = new UserDao();
		userDao.searchAccountUsersByPin(EJBUtil.stringToNum(accId),userPin);
		//changes by salil 
		ArrayList usrIds=null;	
		usrIds = userDao.getUsrIds();
		int size = usrIds.size();
		if(size==0){ %>
		<P class="defComments"><%=MC.M_NoSrchResMatch_Criteria%><%--No search results matching given criteria found.*****--%> </P>

		<%}
		else
		{
		
		
	      ArrayList usrLastNames;
	  	ArrayList usrFirstNames=null;
	      ArrayList usrMidNames=null;
      	      ArrayList siteNames=null;
	      ArrayList jobTypes=null;
      	
	      ArrayList grpNames=null;	
      	      String usrLastName = null;
	      String grpName = null;
	      String usrFirstName = null;
	      String usrMidName = null;
      	      String siteName = null;
	      String jobType = null;
      	      String usrId = null;	
	      int counter = 0;
	      String oldGrp = null;
	      String usrName= "";
%>
  <br>
  <P class = "userNameNew"> <%= uName %> </P>
  <Form name="accountBrowser" method="post">
    <table class=tableDefault width="100%" border=0>
      <tr> 
        <th width=35% align =left> <%=LC.L_User_Name%><%--User Name*****--%> </th>
      </tr>
      <%
		usrLastNames = userDao.getUsrLastNames();
		usrFirstNames = userDao.getUsrFirstNames();
		int i;
		int lenUsers = usrLastNames.size();
	//	out.print(lenUsers);
	%>
      <%

		for(i = 0 ; i < lenUsers ; i++)
	  	{
			usrLastName=((usrLastNames.get(i)) == null)?"-":(usrLastNames.get(i)).toString();
			usrFirstName=((usrFirstNames.get(i))==null)?"-":(usrFirstNames.get(i)).toString();
			usrId = ((Integer)usrIds.get(i)).toString();
			if ((i%2)==0) {
	%>
      <tr class="browserEvenRow"> 
        <%
			}else{
		  %>
      <tr class="browserOddRow"> 
        <% } usrName = usrFirstName+" "+usrLastName; 
		   %>
        <td width =200> <A href = "#" onClick="back('<%=usrId%>','<%=usrName%>');"><%= usrFirstName%>,&nbsp<%= usrLastName%>; 
          </A> </td>
      </tr>
      <%}%>
    </table>
  </Form>
  <% }// end of usrid check
	}//end of Parameter check
}//end of if body for session
else{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}

%>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id = "emenu"> </div>
</body>
</html>
