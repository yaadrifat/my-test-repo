<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Evt_DetsPage%><%--Event Details Page*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB"%>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>

<SCRIPT Language="javascript">

 function  validate(){

     formobj=document.Addevent

     if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }

}


</SCRIPT>






<% String src;

src= request.getParameter("srcmenu");

%>

<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>




<body>
<DIV class="formDefault" id="div1"> 
<%

	String eventmode = request.getParameter("eventmode");   
	String duration = request.getParameter("duration");   
	String protocolId = request.getParameter("protocolId");   
	String calledFrom = request.getParameter("calledFrom");   
	String fromPage = request.getParameter("fromPage");
	String mode = request.getParameter("mode");   
	String calStatus= request.getParameter("calStatus");   

	String categoryId = "";
	String eventId = "";

	String eventName = "";
	String eventDesc = "";
	String eventDuration = "";
	String eventDurDays = "";
	String eventFuzzyPer = "0";
	String eventMsgPer = "0";
	String eventFuzzyPerDays = "";
	String eventMsgPerDays = "";
	String eventFuzzyPerBA = "";
	String eventMsgPerBA = "";
	String eventNotes = "";
	String eventMsgFlag = "0";
	String eventMsg = "";
	String eventMsgTo = "";

	HttpSession tSession = request.getSession(true); 

	if (sessionmaint.isValidSession(tSession))
	{
		String uName = (String) tSession.getValue("userName");
%>

<P class = "userName"><%= uName %></p>

<%
	String calName = "";
	
	if (fromPage.equals("eventbrowser"))
	{
		
			
%>
	<P class="sectionHeadings"> <%=MC.M_PcolCal_EvtDets%><%--Protocol Calendar >> Event Details*****--%> </P>

<%
	}
	else
	{
		calName = (String) tSession.getValue("protocolname");
%>
	<P class="sectionHeadings"> <%Object[] arguments = {calName}; %><%=VelosResourceBundle.getMessageString("M_PcolCal_EvtDetails",arguments)%><%--Protocol Calendar [ <%=calName%> ] >> Event Details*****--%> </P>

<%
	}
	

		if(eventmode.equals("M")) {
		   eventId = request.getParameter("eventId");   
		   if (fromPage.equals("eventbrowser") || (fromPage.equals("fetchProt") && calledFrom.equals("P"))){
  		  	eventdefB.setEvent_id(EJBUtil.stringToNum(eventId));
			eventdefB.getEventdefDetails();
		      eventName = eventdefB.getName(); 
   		   	eventDesc = eventdefB.getDescription();
		      eventDuration = eventdefB.getDuration();  
	   	      eventFuzzyPerDays = eventdefB.getFuzzy_period();
	   	      eventMsgPerDays = eventdefB.getMsg_details();
  	   	      eventNotes = eventdefB.getNotes();
		      eventMsgTo = eventdefB.getMsg_to();
		   }else{	
  		  	eventassocB.setEvent_id(EJBUtil.stringToNum(eventId));
			eventassocB.getEventAssocDetails();
		      eventName = eventassocB.getName(); 
   		   	eventDesc = eventassocB.getDescription();
		      eventDuration = eventassocB.getDuration();  
	   	      eventFuzzyPerDays = eventassocB.getFuzzy_period();
	   	      eventMsgPerDays = eventassocB.getMsg_details();
  	   	      eventNotes = eventassocB.getNotes();
		      eventMsgTo = eventassocB.getMsg_to();
		   }

		   if(eventFuzzyPerDays.equals("0")){
			eventFuzzyPer = "0";
		   } else {
			eventFuzzyPer = "1";
			if(eventFuzzyPerDays.substring(0,1).equals("+")) {
			   eventFuzzyPerBA = "+";
				eventFuzzyPerDays = eventFuzzyPerDays.substring(1,eventFuzzyPerDays.length());
				}else if(eventFuzzyPerDays.substring(0,1).equals("-")) {
			   eventFuzzyPerBA = "-";
			eventFuzzyPerDays = eventFuzzyPerDays.substring(1,eventFuzzyPerDays.length());
			} else  {
			   eventFuzzyPerBA = "";
			   eventFuzzyPerDays = eventFuzzyPerDays.substring(0,eventFuzzyPerDays.length());

			}
		   }
		
		
		  if (!((eventMsgPerDays==null) || (eventMsgPerDays==""))) {
			if(eventMsgPerDays.substring(0,1).equals("+")) {
			   eventMsgPerBA = "+";
			   eventMsgPerDays =eventMsgPerDays.substring(1,eventMsgPerDays.length());
			} else if(eventMsgPerDays.substring(0,1).equals("-")) { 
			   eventMsgPerBA = "-";
			   eventMsgPerDays =eventMsgPerDays.substring(1,eventMsgPerDays.length());
			} else {
			   eventMsgPerBA = "";
			}
  		   }


		   if(eventMsgTo == null || eventMsgTo.equals("") || eventMsgTo.equals("0")) {
			eventMsgFlag = "0";
		   } else {
			eventMsgFlag = "1";
			if (fromPage.equals("eventbrowser") || (fromPage.equals("fetchProt") && calledFrom.equals("P"))){
			   eventMsg = eventdefB.getEvent_msg();
			} else {
			   eventMsg = eventassocB.getEvent_msg();
			}
		   }	

		} else {
		   categoryId = request.getParameter("categoryId");   		   	
		}
%>

<%
if(eventmode.equals("N")) {
%>
   <form name= "Addevent" METHOD=POST action=eventsave.jsp?srcmenu=<%=src%>&eventmode=<%=eventmode%>&categoryId=<%=categoryId%>&eventId=<%=eventId%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>>	
<%
} else {
%>
   <form name="Addevent"  METHOD=POST action=eventsave.jsp?srcmenu=<%=src%>&eventmode=<%=eventmode%>&eventId=<%=eventId%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>>
<%
}
%>

<TABLE width="100%" cellspacing="0" cellpadding="0" >
   <tr>
	<td width="40%"> <%=MC.M_NameOf_TheEvt%><%--Name of the event*****--%> </td>
	<td width="60%"> 
	   <INPUT NAME="eventName" value="<%=eventName%>" TYPE=TEXT SIZE=30 MAXLENGTH=50 > 
	</td>
   </tr>
   <tr>
	<td width="40%"> <%=LC.L_Description%><%--Description*****--%> </td>
	<td width="60%"> <INPUT NAME="eventDesc" value="<%=eventDesc%>" TYPE=TEXT SIZE=30 MAXLENGTH=200> </td>
   </tr>
   <tr>
	<td width="40%"> <%=MC.M_Duration_OfEvt%><%--Duration of the event*****--%> </td>
	<td width="60%"> <INPUT NAME="eventDuration" value="<%=eventDuration%>" TYPE=TEXT SIZE=5> 
	   <select name="eventDurDays">
		<option value=Days><%=LC.L_Days%><%--Days*****--%></option>
		<option value=Weeks><%=LC.L_Weeks%><%--Weeks--%></option>
	   </select>			
	</td>
   </tr>
   <tr>
	<td width="40%"> <%=LC.L_Fuzzy_Period%><%--Fuzzy Period*****--%> </td>
	<td width="60%"> 
<%
	if(eventFuzzyPer.equals("0")) {
%>	
	   <INPUT NAME="eventFuzzyPer" TYPE= radio value="0" checked><%=LC.L_No%><%--No*****--%>
	   <INPUT NAME="eventFuzzyPer" TYPE= radio value="1"><%=LC.L_Yes%><%--Yes*****--%>  
<%
	} else {
		
%>
	   <INPUT NAME="eventFuzzyPer" TYPE= radio value="0"><%=LC.L_No%><%--No*****--%>
	   <INPUT NAME="eventFuzzyPer" TYPE= radio value="1" checked><%=LC.L_Yes%><%--Yes*****--%>  
<%
	} 
		
%>
	</td>
   </tr>
   <tr>
	<td width="40%"> <P class = "defComments">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <%=MC.M_IfYes_Specify%><%--If yes, please specify*****--%> </P></td>
	<td width="60%"> <INPUT NAME="eventFuzzyPerDays" value="<%=eventFuzzyPerDays%>" TYPE=TEXT SIZE=5> <%=LC.L_Days_Lower%><%--days*****--%>  
	   <select name="eventFuzzyPerBA">
<%
	if(eventFuzzyPerBA.equals("-")) {
%>	
	   <option value=Before selected><%=LC.L_Before%><%--Before*****--%></option>
	   <option value=After><%=LC.L_After%><%--After*****--%></option>
	   <option value=BA><%=LC.L_Before_AndAfter%><%--Before and After*****--%></option> 
	<% }else if(eventFuzzyPerBA.equals("+")) { %>
 	   <option value=Before ><%=LC.L_Before%><%--Before*****--%></option>
	   <option value=After selected ><%=LC.L_After%><%--After*****--%></option>
	   <option value=BA><%=LC.L_Before_AndAfter%><%--Before and After*****--%></option> 
	<%
	} else {
%>
	   <option value=Before><%=LC.L_Before%><%--Before*****--%></option>
	   <option value=After ><%=LC.L_After%><%--After*****--%></option>
	   <option value=BA selected ><%=LC.L_Before_AndAfter%><%--Before and After*****--%></option> 
<%
	} 
		
%>
	
	   </select>			
	</td>
   </tr>

   <tr>
	<td width="40%"> <%=LC.L_Notes%><%--Notes*****--%> </td>
	<td width="60%"> <TEXTAREA name="eventNotes"  rows=3 cols=30><%=eventNotes%></TEXTAREA> </td> 

   </tr>
   <tr>
	<td width="40%"> <%=MC.M_MsgAssoc_ThisEvt%><%--Message associated with this event*****--%> </td> 
	<td width="60%"> 
<%
	if(eventMsgFlag.equals("0")) {
%>	
	   <INPUT NAME="eventMsgFlag" TYPE= radio value="0" checked><%=LC.L_No%><%--No*****--%> 
	   <INPUT NAME="eventMsgFlag" TYPE= radio value="1" ><%=LC.L_Yes%><%--Yes*****--%>
<%
	} else {
		
%>
	   <INPUT NAME="eventMsgFlag" TYPE= radio value="0"><%=LC.L_No%><%--No*****--%> 
	   <INPUT NAME="eventMsgFlag" TYPE= radio value="1" checked><%=LC.L_Yes%><%--Yes*****--%>
<%
	} 
		
%>


	</td>
   </tr>

   <tr>
	<td width="40%"> <P class = "defComments">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <%=MC.M_IfYes_EtrMsgToSent%><%--If yes, enter the email message to be sent*****--%> </P></td>
	<td width="60%"> <TEXTAREA name="eventMsg" rows=4 cols=40><%=eventMsg%></TEXTAREA> </td>
   </tr>

   <tr>
	<td width="40%"> <P class = "defComments">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <%=LC.L_Send_Msg%><%--Send Message*****--%> </P></td>
	<td width="60%"> <INPUT NAME="eventMsgPerDays" value="<%=eventMsgPerDays%>" TYPE=TEXT SIZE=5> <%=LC.L_Days%><%--Days*****--%>  
   <select name="eventMsgPerBA">
<%
	if(eventMsgPerBA.equals("-")) {
%>	
	   <option value=Before selected><%=LC.L_Before_Evt%><%--Before the event*****--%></option>
	   <option value=After><%=LC.L_After_Evt%><%--After the event*****--%></option>
	   <option value=BeforeAfter><%=MC.M_BeforeAfter_Evt%><%--Before and After the event*****--%></option>
<%
	} else if(eventMsgPerBA.equals("+")) {
		
%>
	   <option value=Before><%=LC.L_Before_Evt%><%--Before the event*****--%></option>
	   <option value=After ><%=LC.L_After_Evt%><%--After the event*****--%></option>
	   <option value=BeforeAfter selected ><%=MC.M_BeforeAfter_Evt%><%--Before and After the event*****--%></option>
<%
	} else {
%>
	   <option value=Before><%=LC.L_Before_Evt%><%--Before the event*****--%></option>
	   <option value=After><%=LC.L_After_Evt%><%--After the event*****--%></option>
	   <option value=BeforeAfter selected><%=MC.M_BeforeAfter_Evt%><%--Before and After the event*****--%></option>
<%
	}

%>

	   </select>			
	</td>


	 </td>
   </tr>
</table>
<Br>
<TABLE width="100%" cellspacing="0" cellpadding="0" >
   <tr>
   	<td width="40%"> <%=MC.M_SelWhoRecv_Msg%><%--Select individuals who are to receive this message*****--%> 
	<P class = "defComments"><%=MC.M_SpecifyUsr_OnNextPage%><%--You may specify the Users on the next page.*****--%></P></td>
	<td width="60%"> 
<%
	if(!(eventMsgTo == null) && eventMsgTo.equals("P") ) {
%>
	   <INPUT NAME="eventMsgTo" TYPE= radio VALUE=P checked><%=LC.L_Patient%><%--<%=LC.Pat_Patients%>*****--%>

	   <BR>
	   <INPUT NAME="eventMsgTo" TYPE= radio VALUE=U><%=LC.L_Selected_Users%><%--Selected Users*****--%>
	   <BR>
   	   <INPUT NAME="eventMsgTo" TYPE= radio VALUE=B><%=MC.M_PatSel_Usr%><%--<%=LC.Pat_Patients%> and selected users*****--%>
<%
	} else if(!(eventMsgTo == null) && eventMsgTo.equals("U") ) {
%>
	   <INPUT NAME="eventMsgTo" TYPE= radio VALUE=P><%=LC.L_Patient%><%--<%=LC.Pat_Patients%>*****--%>
	   <BR>
	   <INPUT NAME="eventMsgTo" TYPE= radio VALUE=U checked><%=LC.L_Selected_Users%><%--Selected Users*****--%>
	   <BR>
   	   <INPUT NAME="eventMsgTo" TYPE= radio VALUE=B><%=MC.M_PatSel_Usr%><%--<%=LC.Pat_Patients%> and selected users*****--%>
<%
	} else {
%>
	   <INPUT NAME="eventMsgTo" TYPE= radio VALUE=P><%=LC.L_Patient%><%--<%=LC.Pat_Patients%>*****--%>
	   <BR>
	   <INPUT NAME="eventMsgTo" TYPE= radio VALUE=U><%=LC.L_Selected_Users%><%--Selected Users*****--%>
	   <BR>
   	   <INPUT NAME="eventMsgTo" TYPE= radio VALUE=B checked><%=MC.M_PatSel_Usr%><%--<%=LC.Pat_Patients%> and selected users*****--%>
<%
	}
%>
	</td>
<BR>
   </tr>
<table>
<tr>
	   <td>
		<%=LC.L_Esignature%><%--e-Signature*****--%> <FONT class="Mandatory">* </FONT>
	   </td>
	   <td>
		<input type="password" name="eSign" maxlength="8">
	   </td>
</tr>
</table>
</table>

<table width=75%>
  <tr><td align=right>
	<button type="submit"><%=LC.L_Next%></button>
</td></tr>
</table>

</form>



<%

} else {  //else of if body for session

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>

  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</div>
<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/>
</div>
</body>

</html>


