<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></HEAD>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
<!--Added by Amarnadh to check Patient study ID being entered is a duplicate  -->
<script>

	function confirmCheck(formobj) {
			formobj.action="updatestudycentricenroll.jsp";
			formobj.flag.value = "1";
    		formobj.submit();
	}

</script>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="prefB" scope="request" class="com.velos.eres.web.pref.PrefJB"/>
<jsp:useBean id="patStudyStatB" scope="request" class="com.velos.eres.web.patStudyStat.PatStudyStatJB"/>
<jsp:useBean id="patEnrollB" scope="request" class="com.velos.eres.web.patProt.PatProtJB"/>
<jsp:useBean id="CommonB" scope="page" class="com.velos.eres.web.common.CommonJB" />
<jsp:useBean id="StudyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="SiteB" scope="page" class="com.velos.eres.web.site.SiteJB" />
<jsp:useBean id="ssJB" scope="page" class="com.velos.eres.web.studySite.StudySiteJB" />
<jsp:useBean id="studyStatB" scope="page" class="com.velos.eres.web.studyStatus.StudyStatusJB" />
<jsp:useBean id="patB" scope="request" class="com.velos.eres.web.patStudyStat.PatStudyStatJB"/> <!--Amar -->
<jsp:include page="include.jsp" flush="true"/>

<div class = "myHomebottomPanel">
	<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
<%@ page language = "java" import = "com.velos.eres.business.common.*,com.velos.eres.service.util.*,java.util.*,com.velos.eres.business.pref.*,com.velos.eres.business.person.impl.*,com.velos.eres.business.patProt.impl.*"%>

<div class = "myHomebottomPanel"> 
		<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
<Form name ="enroll" method ="post"  action = "updatestudycentricenroll.jsp" onsubmit ="">

<%

String eSign = request.getParameter("eSign");
HttpSession tSession = request.getSession(true);

String oldPatStudyId = request.getParameter("oldPatStudyId"); //Amar

if (sessionmaint.isValidSession(tSession))

{
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
	String oldESign = (String) tSession.getValue("eSign");
	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
	} else {
	String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = null;
	usr = (String) tSession.getValue("userId");
	int personPK  = 0;
    String patientStudyID = "";
	String organization = "";

  	int patstat = 0;
	String studyId = "";
    int saved = 0;
    boolean codeExists = false;
	int count = 0; // amar
    String accountId = "";

    String patientStudyStatus = "";
    String patientStudyStatusDate = "";
    String selStatusSubType = "";
	ArrayList arSubType = null;

    CodeDao cd = new CodeDao();
    boolean proceed = true;
    String patientPkStr = "";
	String protocolId = "";
	String protocolDate = "";

	java.sql.Date protStSQLDate = null;
	Date st_date1 = null;
	int patProtPk = 0;

	String patStudyIdAutoStr = "";

	SettingsDao settingsdao = new SettingsDao();
	ArrayList arSettingsValue = new ArrayList();
	String formatString = "";
	String idNumberFormat = "";
	String idNumberFormat_val = "";
	String firstKeyword = "";
	String secondKeyword = "",forwardPage="";
	String fname = "";
	String lname = "";
	String dob = "";
	String gender = "";

	int curEnrCount = 0;

    PatProtBean psk = new PatProtBean();

    studyId  = request.getParameter("studyId");
    studyId=(studyId==null)?"":studyId;

   organization = request.getParameter("dPatSite");
   organization=(organization==null)?"":organization;


//	Check if the Organization study combination is open for enrolling
	StudyStatusDao statDao=null;
	int iStudyId=EJBUtil.stringToNum(studyId);
	int index=-1,stopEnrollCount=0;
	String studyAccrualFlag="";
	ArrayList statList=null;
	if (studyId.length()>0)
	{
	 SettingsDao settingsDao=CommonB.retrieveSettings(iStudyId,3);
	 ArrayList settingsValue=settingsDao.getSettingValue();
	 ArrayList settingsKeyword=settingsDao.getSettingKeyword();
	 index=settingsKeyword.indexOf("STUDY_ACCRUAL_FLAG");
		if (index>=0)
		{
		 studyAccrualFlag=(String)settingsValue.get(index);
		 studyAccrualFlag=(studyAccrualFlag==null)?"":studyAccrualFlag;
		}
		else
		{
			studyAccrualFlag="D";
		}

	  if ( (studyAccrualFlag.equals("D")))  {
	   //Disable this for now, with Default settings everything will work as it was in ver 6.2
	  //stopEnrollCount=studyStatB.getCountByOrgStudyStat(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(organization),"'active_cls'","A");
	   stopEnrollCount=0;
	  }else if (studyAccrualFlag.equals("O"))
	  {

		 statDao=studyStatB.getDAOByOrgStudyStat(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(organization),"'active_cls','active','prmnt_cls'","A");
		 statList=statDao.getStudyStatList();
		 if (statList.size()==0) stopEnrollCount=0;

		 if (statList.indexOf("active_cls")>=0)
			 stopEnrollCount=1;

		 if (statList.indexOf("active")<0)
			 stopEnrollCount=1;

		 if (statList.indexOf("prmnt_cls")>0)
			 stopEnrollCount=1;

	  }
	}

	//End check for Organization study combination is open for enrolling

	//get values from request object
   patientStudyID = request.getParameter("patStudyId");

   if (StringUtil.isEmpty(patientStudyID))
   {
   		patientStudyID = "";
   }
   else
   {
   		patientStudyID = patientStudyID.trim();
   }

   patstat = cd.getCodeId("patient_status", "A");
   fname = request.getParameter("patfname");
   lname = request.getParameter("patlname");
   dob = request.getParameter("patdob");
   gender = request.getParameter("patgender");
   if (StringUtil.isEmpty(gender)) gender = null;
   //Added by Amarnadh to check Patient study ID being entered is a duplicate
   String flag = request.getParameter("flag");
			flag = (flag == null)?"" :flag;

   patientStudyStatus = request.getParameter("patStudyStatus");
   patientStudyStatusDate = request.getParameter("statusDate");
   protocolId = request.getParameter("protocolId");
   protocolDate = request.getParameter("protStDate");
   patStudyIdAutoStr = request.getParameter("patStudyIdAuto");
   forwardPage=request.getParameter("forwardpage");
   forwardPage=(forwardPage==null)?"":forwardPage;

   if (StringUtil.isEmpty(patStudyIdAutoStr))
   {
   	patStudyIdAutoStr = "";
   }

   accountId = (String) tSession.getValue("accountId");


		/*validate if the patient status is ok*/

		//get the status sub type

		cd.getCodeValuesById(Integer.parseInt(patientStudyStatus));
		arSubType = cd.getCSubType();
		if (arSubType != null && arSubType.size() > 0)
			{
				selStatusSubType = (String) arSubType.get(0);
			}
		if (stopEnrollCount>0)
		{
			proceed = false;
			%>
			<br><br><br><br><br><br><br>
				<TABLE width="500" border = "0">
			 	<tr>
					<td align="center">
			     	<P class = "successfulmsg">
			     		<%=MC.M_StdNotHave_StatEnrlPat %><%-- This <%=LC.Std_Study%> does not have an appropriate status to enroll a <%=LC.Pat_Patient_Lower%>.*****--%>
				 	</P>
				</td>
			   </tr>
			 </table>

			<TABLE width="550" border = "0">
				<tr>
					<td align=center>
					<button type="button" onclick="window.history.back();"><%=LC.L_Back%></button>
					</td>
				</tr>
   				</table>
   				<script>
						if (!(typeof(document.getElementById("notification"))=="undefined") && (document.getElementById("notification")!=null))	
							toggleNotification("off","");
				</script>
			<%
						return;

		}
		if (selStatusSubType.equals("active") || selStatusSubType.equals("followup")|| selStatusSubType.equals("offtreat")
			|| selStatusSubType.equals("offstudy") || selStatusSubType.equals("lockdown")  )
		{
			proceed = false;
			%>
			<br><br><br><br><br><br><br>
				<TABLE width="550" border = "0">
			 	<tr>
					<td align="center">
			     	<P class = "successfulmsg">
			     		<%=MC.M_PatNotEnrl_EtrStat %><%-- The <%=LC.Pat_Patient_Lower%> is not currently enrolled. To enter this status, please enter an 'Enrolled' status first.*****--%>
				 	</P>
				</td>
			   </tr>
			 </table>

			<TABLE width="550" border = "0">
				<tr>
					<td align=center>
					<button type="button" onclick="window.history.back();"><%=LC.L_Back%></button>
					</td>
				</tr>
   				</table>
   				<script>
						if (!(typeof(document.getElementById("notification"))=="undefined") && (document.getElementById("notification")!=null))	
							toggleNotification("off","");
				</script>
			<%
						return;
		}

		patientStudyID = patientStudyID.trim();

		codeExists = prefB.patientCodeExistsOneOrg(patientStudyID, organization);
		if(codeExists){
			proceed = false;
		%>
			<br><br><br><br><br>
			<p class = "successfulmsg" align = center> <%=MC.M_PatIdExst_OrgEtrNew %><%-- The <%=LC.Pat_Patient%> ID you have entered already exists for the selected organization. Please click on back and enter a new ID.*****--%></p>
			<p align = center>
			<button type="button" onclick="window.history.back();"><%=LC.L_Back%></button>
			</p>
			<script>
						if (!(typeof(document.getElementById("notification"))=="undefined") && (document.getElementById("notification")!=null))	
							toggleNotification("off","");
				</script>
		<% saved = -1;
			return;
		}


		if (proceed)
		{
			 	if (patStudyIdAutoStr.equals("1"))
			 	{ //generate patient study id

			 			settingsdao = CommonB.retrieveSettings(EJBUtil.stringToNum(studyId),3,"PATSTUDY_ID_FORMAT")	;
							if (settingsdao != null)
							{
								arSettingsValue = settingsdao.getSettingValue();
								if (arSettingsValue != null && arSettingsValue.size() > 0)
								{
									formatString = (String) arSettingsValue.get(0);
									if (StringUtil.isEmpty(formatString))
									{
										idNumberFormat = "";
										idNumberFormat_val = "";
									}
									else
									{
										String[] st  = StringUtil.chopChop(formatString,'-');

											if (st.length >= 3)
											{
												firstKeyword = st[0];
												secondKeyword = st[1];
												String[] numformat  = StringUtil.chopChop(st[2],'|');
												if (numformat.length == 2){
													idNumberFormat_val = numformat[0];
													idNumberFormat = numformat[1];
												} else {
													idNumberFormat_val = "";
													idNumberFormat = numformat[0];
												}

											}

											if (StringUtil.isEmpty(idNumberFormat))
											{
												idNumberFormat = "";
												idNumberFormat_val = "";
											}

										if (firstKeyword.equals("STUDY") || secondKeyword.equals("STUDY"))
										{
											StudyB.setId(EJBUtil.stringToNum(studyId));
											StudyB.getStudyDetails();
										}
										if (firstKeyword.equals("SITEID") || secondKeyword.equals("SITEID"))
										{
											SiteB.setSiteId(EJBUtil.stringToNum(organization));
											SiteB.getSiteDetails();
										}

										if (firstKeyword.equals("STUDY"))
										{firstKeyword = StudyB.getStudyNumber();}
										 else if (firstKeyword.equals("SITEID"))
										{ firstKeyword = SiteB.getSiteIdentifier();}
										 else {	firstKeyword =""; }

										if (StringUtil.isEmpty(firstKeyword ))
										{ firstKeyword = "";}

										if (secondKeyword.equals("STUDY"))
										{ secondKeyword = StudyB.getStudyNumber(); }
										else if (secondKeyword.equals("SITEID"))
										{ secondKeyword = SiteB.getSiteIdentifier();
										} else
										{
										secondKeyword ="";
										}
										if (StringUtil.isEmpty(secondKeyword ))
										{
											secondKeyword = "";
										}
										//include hyphens
										if (! StringUtil.isEmpty(firstKeyword))
										{
											firstKeyword  = firstKeyword + "-";
										}
										if (! StringUtil.isEmpty(secondKeyword))
										{
											secondKeyword  = secondKeyword + "-";
										}
										if (! StringUtil.isEmpty(idNumberFormat_val))
										{
											idNumberFormat_val  = idNumberFormat_val + "-";
										}


									}

								}
							} // end of if for settings dao
					//get the next enrollment number for the study -site of the patient
					ssJB.setStudyId(studyId);
					ssJB.setSiteId(organization);
					ssJB.findBeanByStudySite();
					curEnrCount = EJBUtil.stringToNum( ssJB.getCurrentSitePatientCount() );
					curEnrCount = curEnrCount + 1;
					//generate patientStudyID
					patientStudyID = StringUtil.getFormattedString((firstKeyword +secondKeyword+idNumberFormat_val),"",idNumberFormat,curEnrCount);
					//cut the string to 20
					if 	(patientStudyID.length() > 20)
					{
						patientStudyID = patientStudyID.substring(( patientStudyID.length() - 20) );
					}

			 	} // end of if (patStudyIdAutoStr.equals("1"))

			    patientStudyID = patientStudyID.trim();

			 //   codeExists = prefB.patientCodeExistsOneOrg(patientStudyID, organization);  --Amar

			/*	Added by Amarnadh to check Patient study ID being entered is a duplicate  */

				if(!(oldPatStudyId.toLowerCase()).equals(patientStudyID.toLowerCase())) {

				count = patB.getCountPatStudyId(EJBUtil.stringToNum(studyId),patientStudyID.trim());

				}

				//if(count > 0){ ---Amar
					if (count > 0 && flag.equals("")) {
				%>

			<br><br><br><br><br><br><br>
			<input type="hidden" name="flag" size =20 value =<%=flag%> >
			<input type="hidden" name="eSign" size =20 value =<%=eSign%> >
			<input type="hidden" name="oldPatStudyId" size =20 value =<%=oldPatStudyId%> >
   			<input type="hidden" name="studyId" size = 20  value = <%=studyId%> >
			<input type="hidden" name="dPatSite" size = 20  value = <%=organization%> >
			<input type="hidden" name="patStudyId" size = 20  value = <%=patientStudyID%> >
			<input type="hidden" name="patfname" size =20 value =<%=fname%> >
			<input type="hidden" name="patlname" size =20 value =<%=lname%> >
			<input type="hidden" name="patdob" size =20 value =<%=dob%> >
			<input type="hidden" name="patgender" size =20 value =<%=gender%> >
			<input type="hidden" name="patStudyStatus" size =20 value =<%=patientStudyStatus%> >
			<input type="hidden" name="statusDate" size =20 value =<%=patientStudyStatusDate%> >
			<input type="hidden" name="protocolId" size =20 value =<%=protocolId%> >
			<input type="hidden" name="protStDate" size =20 value =<%=protocolDate%> >
			<input type="hidden" name="patStudyIdAuto" size =20 value =<%=patStudyIdAutoStr%> >
			<input type="hidden" name="forwardpage" size =20 value =<%=forwardPage%> >

			<TABLE width="500" border = "0">
			 	<tr>
					<td align="center">
			     	<P class = "successfulmsg">
			     		 <%=MC.M_PatStdId_Dupli %><%-- The <%=LC.Pat_Patient%> <%=LC.Std_Study%> Id you have entered is duplicate. Do you want to continue ?*****--%>
					<br><br>
				  <button type="button" name="ok" " border="0" align="left" onclick="return confirmCheck(document.enroll);"><%=LC.L_Yes%><%--Yes--%></button>
		         <button type="button" name="cancel" onClick="window.history.back()"><%=LC.L_No%><%--No--%> </button>
				 	</P>
				</td>
			   </tr>
			</table>
			<!--end added -->
				<% saved = -1;
					return;
				} else {

				/*Create the patient*/
   				//Added by Gopu for July-August'06 Enhancement (U2) - Admin Settings for Patient Time zone
				SettingsDao settingsDao=CommonB.getSettingsInstance();
				int modname=1;
				String keyword="ACC_PAT_TZ";
				settingsDao.retrieveSettings(EJBUtil.stringToNum(accountId),modname,keyword);
				ArrayList setvalues=settingsDao.getSettingValue();
				String setvalue="";
				if(setvalues!=null){
				setvalue=(setvalues.get(0)==null)?"":(setvalues.get(0)).toString();
				}
				person.setPersonPId(patientStudyID);
				person.setPersonAccount(accountId);
				person.setPersonLocation(organization);
		    	person.setPersonStatus(String.valueOf(patstat));
				// commentd By Amarnadh for issue #3013
			//	person.setPersonRegBy(usr);
				person.setPersonDob(dob);
        		person.setPersonFname(fname);
				person.setPersonGender(gender);
        		person.setPersonLname(lname);
				person.setPatientFacilityId(patientStudyID);

				PersonBean pers = null;

				if (!(setvalue.equals("")))
		     			person.setTimeZoneId(setvalue);
				person.setCreator(usr);
				person.setIpAdd(ipAdd);
				//Commented by Manimaran to fix the issue 2956
				//person.setModifiedBy(usr);
				pers = person.setPersonDetails();

				if (pers !=null)
				  {
					saved = 0;
					personPK =  pers.getPersonPKId();
					patientPkStr = String.valueOf(personPK);
				 }
				else
				{
					saved = -1;
				}
			  if (saved == 0) //the patient was saved
			  {
			  	  /* Enroll the patient and add patient status */

			  	  	  	//set standard enrollment details on psk Object

						psk.setPatProtStudyId(studyId);
						psk.setPatProtPersonId(patientPkStr);
						psk.setPatStudyId(patientStudyID);
						psk.setPatProtStat("1");

						psk.setCreator(usr);

						psk.setPatProtStartDt(protocolDate);
						psk.setPatProtProtocolId(protocolId);
						psk.setPatProtStartDay(null);

						//set default enrolling site
						psk.setEnrollingSite(organization);


						//set enrolled status specific enrollment details
						if (selStatusSubType.equals("enrolled"))
						{
							psk.updatePatProtEnrolDt(patientStudyStatusDate);
							psk.setPatProtUserId(usr);
						}

						//patient study status data

						patStudyStatB.setPatientId(patientPkStr);
						patStudyStatB.setStartDate(patientStudyStatusDate);
						patStudyStatB.setPatStudyStat(patientStudyStatus);
						patStudyStatB.setStudyId(studyId);
		   				//JM: 30Nov2006
		   				patStudyStatB.setCurrentStat("1");

		   				patStudyStatB.setIpAdd(ipAdd);
		   				patStudyStatB.setCreator(usr);
						patStudyStatB.setPatProtObj(psk);
						saved = patStudyStatB.setPatStudyStatDetails();


						if ( saved >= 0)
						{
							proceed = true;
						}
						else
						{
							proceed = false;
						}

					if (proceed)
					{
						patEnrollB.findCurrentPatProtDetails(EJBUtil.stringToNum(studyId),personPK);
						patProtPk = patEnrollB.getPatProtId();

						//if patient protocol details are provided, generate schedule
						if ((! StringUtil.isEmpty(protocolId)) && (! StringUtil.isEmpty(protocolDate)) )
						{

								if (patProtPk > 0)
								{
									st_date1 = DateUtil.stringToDate(protocolDate,"");
									protStSQLDate = DateUtil.dateToSqlDate(st_date1);
									eventdefB.GenerateSchedule(Integer.parseInt(protocolId),personPK,protStSQLDate,patProtPk,null,usr,ipAdd);
								}
						}
					}

				}

	 } //for save patient;code does not exist
 %>
<br><br><br><br><br>
<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%-- Data was Saved successfully.*****--%> </p>
</form>
<form name="forward" method="post">

<!--Create the harcoded url to forward the control-->
<%
String url="";

if (forwardPage.length()>0)
{
  if (forwardPage.indexOf("[VELCHAR]")>=0)
  {
   forwardPage=StringUtil.replace(forwardPage,"[VELCHAR]","");
   if (forwardPage.equals("DEMO"))
   {
   url="patientdetails.jsp?mode=M&&srcmenu=tdmenubaritem5&selectedTab=1&pkey="+personPK+
   "&page=patientEnroll&studyId="+studyId+"&calledFrom=S";

   }
   if (forwardPage.equals("ADV"))
   {
   url="adveventbrowser.jsp?srcmenu=tdMenuBarItem5&page=patientEnroll&selectedTab=6&pkey="+personPK+
   "&patProtId="+patProtPk+"&studyId="+studyId;
   }
   if (forwardPage.equals("PATSTAT"))
   {
   url="enrollpatient.jsp?srcmenu=tdMenuBarItem5&selectedTab=2&mode=M&pkey="+personPK+
   "&patProtId="+patProtPk+"&patientCode="+patientStudyID+"&page=patientEnroll&studyId="+studyId;
   }
   if (forwardPage.equals("LAB"))
   {
   url="formfilledstdpatbrowser.jsp?mode=M&srcmenu=tdmenubaritem5&selectedTab=8&pkey="+personPK+
        "&patProtId="+patProtPk+"&studyId="+studyId+"&calledFrom=S&patientCode="+patientStudyID+"&formPullDown=lab";
   }


  }//end for forwardpage.indexOf
  else //if formId is stored
  {
    url="formfilledstdpatbrowser.jsp?mode=M&srcmenu=tdmenubaritem5&selectedTab=8&pkey="+personPK+
        "&patProtId="+patProtPk+"&studyId="+studyId+"&calledFrom=S&patientCode="+patientStudyID+"&formPullDown="+forwardPage+"**";
  }

}//end for forwardpage.length
else
{
url="";
}

%>
<input type="hidden" name="url" value=<%=url%>>
 <script>
    this.document.forward.target="studypatients";

    if (this.document.forward.url.value.length==0)
    {
    window.opener.location.reload();
    }
    else
    {
    this.document.forward.action=this.document.forward.url.value;
    this.document.forward.method="post";
    this.document.forward.submit();
    }

    this.window.close();
    </script>
</form>
<!--End forwarding control -->

<%
} //for proceed = true;

}//end of if for eSign check
}//end of if body for session
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
</BODY>
</HTML>

