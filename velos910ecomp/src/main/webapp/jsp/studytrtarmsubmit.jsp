<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=MC.M_Std_TreatArmSubmit%><%--<%=LC.Std_Study%> Treatment Arm Submit*****--%></title>
<%@ page import="com.velos.eres.service.util.VelosResourceBundle"%>
<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="stdTXArmJB" scope="request"  class="com.velos.eres.web.studyTXArm.StudyTXArmJB"/>

<body><%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB, com.velos.eres.business.fieldLib.impl.*"%>
<DIV class="popDefault" id="div1">

<%
	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))
	{

%>

		<jsp:include page="sessionlogging.jsp" flush="true"/>
 <%
		String eSign= request.getParameter("eSign");
    	String oldESign = (String) tSession.getValue("eSign");
	   if(oldESign.equals(eSign))
	   {


			int errorCode=0;
			String mode=request.getParameter("mode");
			if(mode==null)
			mode="";

			String studyId="";
  		String stdTXArmId="";
  		String name= request.getParameter("trtname");
  		String desc= request.getParameter("trtdesc");
  		String drugInfo= request.getParameter("trtinfo");



  		String accountId=(String)tSession.getAttribute("accountId");
  		int tempAccountId=EJBUtil.stringToNum(accountId);

			String creator="";
			String ipAdd="";

			creator=(String) tSession.getAttribute("userId");
			ipAdd=(String)tSession.getAttribute("ipAdd");
	     	accountId=(String)tSession.getAttribute("accountId");
			studyId=request.getParameter("studyId");





			stdTXArmJB.setStudyId(studyId);
			stdTXArmJB.setTXName(name);
			stdTXArmJB.setTXDesc(desc);
			stdTXArmJB.setTXDrugInfo(drugInfo);
			stdTXArmJB.setIpAdd(ipAdd);

			if (mode.equals("N"))
			{


				stdTXArmJB.setCreator(creator);
				errorCode=stdTXArmJB.setStudyTXArmDetails();

			}

			else if (mode.equals("M"))
			{
			    stdTXArmId = request.getParameter("stdTXArmId");

				stdTXArmJB.setStudyTXArmId(EJBUtil.stringToNum(stdTXArmId));
				stdTXArmJB.getStudyTXArmDetails();
				stdTXArmJB.setTXName(name);
			    stdTXArmJB.setTXDesc(desc);
			    stdTXArmJB.setTXDrugInfo(drugInfo);
				stdTXArmJB.setModifiedBy(creator);
				errorCode = stdTXArmJB.updateStudyTXArm();
		 }


		if ( errorCode == -2 || errorCode == -3 )
		{
%>

<%             if (errorCode == -3 )
				{
%>

				<br><br><br><br><br>
				<p class = "sectionHeadings" > <%=MC.M_FldIdExst_EtrDiff%><%--The Field ID already exists. Please enter a different Field ID*****--%></p>
				<button tabindex=2 onClick="history.go(-1);"><%=LC.L_Back%></button>


<%
				} else{
%>
				<br><br><br><br><br>
	 			<p class = "sectionHeadings" ><%=MC.M_Data_NotSvdSucc%><%--Data not saved successfully*****--%> </p>

<%
				}
		} //end of if for Error Code

		else

		{
%>

<%

%>
		<br><br><br><br><br>
		<p class = "sectionHeadings"><%=MC.M_Data_SavedSucc%><%--Data Saved Successfully*****--%> </p>
		<script>
			window.opener.location.reload();
			setTimeout("self.close()",1000);
		</script>

<%
		}//end of else for Error Code Msg Display
	}//end of if old esign
	else{
%>
		<jsp:include page="incorrectesign.jsp" flush="true"/>
<%
			}//end of else of incorrect of esign
     }//end of if body for session

	else
	{

%>
		  <jsp:include page="timeout.html" flush="true"/>
<%

} //end of else of the Session


%>

</body>

</html>
