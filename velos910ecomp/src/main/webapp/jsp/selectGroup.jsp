<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<title><%=LC.L_Select_Grp%><%--Select Group*****--%></title>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
	function removeAll(formobj)
	{
	openerform = formobj.openerform.value;
	dispData= formobj.dispData.value;
	dispId=formobj.dispId.value;
	
	//Added by Manimaran to fix the Bug 2409.
	counter = formobj.counter.value;
	formRows = formobj.formRows.value;	
	if ((dispData.length>0) || (dispId.length>0))
	 {
		window.opener.document.forms[openerform].elements[dispId].value = "";
		window.opener.document.forms[openerform].elements[dispData].value = "";
			 	 
	 } else {
	   if (openerform=="selectForms"){
	   	   
		 //km
		 if(formRows==1){
		      window.opener.document.selectForms.selGrpIds.value="";	  
		      window.opener.document.selectForms.selGrpNames.value="";
		 }
		 else {
		      window.opener.document.selectForms.selGrpIds[counter].value="";	  
		      window.opener.document.selectForms.selGrpNames[counter].value="";
		 }
	   
	   }else {
	 	window.opener.document.forms[openerform].selGrpIds.value='';
		window.opener.document.forms[openerform].selGrpNames.value='';
		   }
	
		 }
		 self.close();   

	}


	function getGrpNames(formobj)
	{
	
	selIds = new Array(); //array of selected group Ids
	selNames = new Array(); //array of selected group names
	totrows = formobj.totalrows.value;

	checked = false;
	var k=0;
	if (totrows==1) {
		if (formobj.chkGrpNames.checked) {
			checked=true;
		}else {
			checked=false;
		}
	} else {
		for (i=0;i<totrows;i++) {
			if (formobj.chkGrpNames[i].checked) {
				checked=true;
				break;
			} else {
				checked=false;
			}		
		}		
	}
	
	if (!checked) {
		alert("<%=MC.M_Selc_Grp%>");/*alert("Please select a Group.");*****/
		return false;
	}	

	if (totrows==1) {
		if (formobj.chkGrpNames.checked) {
			selValue = formobj.chkGrpNames.value;
			pos = selValue.indexOf("*");
			selIds[0] = selValue.substring(0,pos);
			selNames[0] = selValue.substring(pos+1);
			//selNames[0] = selValue;
		}
	} else {
		j=0;
		for (i=0;i<totrows;i++) {
			if (formobj.chkGrpNames[i].checked) {
				selValue = formobj.chkGrpNames[i].value;
				pos = selValue.indexOf("*");
				selIds[j] = selValue.substring(0,pos);
				selNames[j] = selValue.substring(pos+1);
				j++;
				var len=j;
			} 	
		}		
	}

	openerform = formobj.openerform.value;
	counter = formobj.counter.value;
	formRows = formobj.formRows.value;	
	dispData= formobj.dispData.value;
	dispId=formobj.dispId.value;

	if ((dispData.length>0) || (dispId.length>0))
	 {
	 	window.opener.document.forms[openerform].elements[dispId].value = selIds;
		window.opener.document.forms[openerform].elements[dispData].value = selNames;
			 	 
	 }else {
	
	if (document.layers) {
		if (openerform=="null") { 
		    window.opener.document.div1.document.formlib.selGrpIds.value = selIds;	 	  
			window.opener.document.div1.document.formlib.selGrpNames.value = selNames;
  	    } else if (openerform=="selectForms") {
			 if (formRows==1) {//check if no. of rows is 1 or , used when called from linkformstostudyacc
			 	window.opener.document.div1.document.selectForms.selGrpIds.value = selIds;	 	  
				window.opener.document.div1.document.selectForms.selGrpNames.value = selNames;
			} else {
			 	window.opener.document.div1.document.selectForms.selGrpIds[counter].value = selIds;	 	  
				window.opener.document.div1.document.selectForms.selGrpNames[counter].value = selNames;			
			}
	    } else 
		{
		   //form is passed as a string
		   eval("window.opener.document.div1.document."+openerform+".selGrpIds.value='"+selIds+"'");
		   eval("window.opener.document.div1.document."+openerform+".selGrpNames.value='"+selNames+"'");			   		
		}
	} else {
	   if (openerform=="null") { 
	   	  window.opener.document.formlib.selGrpIds.value=selIds;	  
		  window.opener.document.formlib.selGrpNames.value=selNames;
  	    } else if (openerform=="selectForms") {
			 if (formRows==1) {//check if no. of rows is 1 or , used when called from linkformstostudyacc
			   	  window.opener.document.selectForms.selGrpIds.value=selIds;	  
				  window.opener.document.selectForms.selGrpNames.value=selNames;
			} else {			
			   	  window.opener.document.selectForms.selGrpIds[counter].value=selIds;	  
				  window.opener.document.selectForms.selGrpNames[counter].value=selNames;
			}	
		} else 
		{ //form is passed as a string
	      eval("window.opener.document."+openerform+".selGrpIds.value='"+selIds+"'" ) ;
		  eval("window.opener.document."+openerform+".selGrpNames.value='"+selNames+"'" ) ;		  
		}				  	
	}
}
	self.close();
}
	
	
	
	</SCRIPT>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.business.common.UserSiteDao"%>
<%@ page language="java" import="com.velos.eres.service.util.*,com.velos.eres.service.util.StringUtil"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="groupB" scope="request" class="com.velos.eres.web.group.GroupJB"/>
<jsp:include page="include.jsp" flush="true"/>

<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1) 
   	ienet = 0; //IE
else
	ienet = 1;

if (ienet == 0) {
%>
<body style="overflow:scroll">
<%} else {%>
<body>
<%}%>


<DIV class="popDefault" style="width:310px"> 
<P class="sectionHeadings"> <%=MC.M_FrmLib_SelGrp%><%--Form Library >> Select Group*****--%> </P>
<p class="defcomments"><A href="#" onClick="removeAll(document.grpnames)"><%=MC.M_Rem_SelcGrp%><%--Remove Already Selected Group*****--%></A></p>
<%
	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))	{
 %>
<jsp:include page="sessionlogging.jsp" flush="true"/> 

<%
  		//used when called from linkformstostudy.jsp
  		
  		String selectGroups=request.getParameter("selectGroups");
		selectGroups=StringUtil.decodeString(selectGroups);
		String openerform = request.getParameter("openerform");
 	 	String counter = request.getParameter("counter");
  		String formRows = request.getParameter("formRows");
		String dispData=request.getParameter("dispFld");
		dispData=(dispData==null)?"":dispData;
		String dispId=request.getParameter("idFld");
		dispId=(dispId==null)?"":dispId;
				
		String grpName="";
		String grpId="";
	    int accountId = EJBUtil.stringToNum((String) tSession.getValue("accountId"));
		ArrayList arrGrpNames=null;
		ArrayList arrGrpIds=null;
		GroupDao grpDao=new GroupDao();
		grpDao.getGroupValues(accountId);
		arrGrpNames=grpDao.getGrpNames();
		arrGrpIds=grpDao.getGrpIds();
		int grpLen=arrGrpIds.size();%>
	
<Form  name="grpnames" id="selGroupId" method="post" action="" onsubmit="if (getGrpNames(document.grpnames)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

    <table width="100%" cellspacing="2" cellpadding="0" border="0" class="basetbl" >
      <tr class="browserOddRow"> 
		<th width="85%"><%=LC.L_Group_Names%><%--Group Names*****--%> </th>
        <th width="10%">&nbsp;&nbsp;</th>
      </tr>
		<%
		boolean flag=false;
		
		if(grpLen==0)
		{
		}
		else
		{
		for(int i=0;i<grpLen;i++)
			{
			grpName = arrGrpNames.get(i).toString();
			grpId=arrGrpIds.get(i).toString();
		
			if(i%2==0)
			{%>

          <tr class="browserEvenRow">
			<%}
			   else
			{%>
				<tr class="browserOddRow">
			<%}%>

			<td ><%=grpName%></td>
			<% 
		       	  
			  String selGrpName="";
			  StringTokenizer st = new StringTokenizer(selectGroups,",");
			  while (st.hasMoreTokens()) {
			     selGrpName=st.nextToken();
				 
			     if(grpId.equals(selGrpName)){ //KM
			    
			        flag=true;
			     break;
			     }
			     else
			       flag=false;
			  }
			 
			 if (flag==true){
			 //km
			 %>
			
		    <td><input type="checkbox" checked name="chkGrpNames" value="<%=grpId%>*<%=grpName%>"></td>
		    <%}else{%>
		     <td><input type="checkbox" name="chkGrpNames" value="<%=grpId%>*<%=grpName%>"></td>
		<%
		}}	
		}%>
	<tr>
	<tr height=10>
	  <td></td>
	</tr>
	<tr>
		
	<jsp:include page="submitBar.jsp" flush="true"> 
				<jsp:param name="displayESign" value="N"/>
				<jsp:param name="formID" value="selGroupId"/>
				<jsp:param name="showDiscard" value="N"/>
		</jsp:include>		
		
		
	<!-- td colspan=2 align="center"><button type="submit"><%=LC.L_Submit%></button></td-->
	</tr>




	</table>
    <Input type="hidden" name="checkedrows" value=0>
    <Input type="hidden" name="totalrows" value=<%=grpLen%> >
    <input type="hidden" name="openerform" value="<%=openerform%>">
    <input type="hidden" name="dispData" value="<%=dispData%>">
    <input type="hidden" name="dispId" value="<%=dispId%>">
    <input type="hidden" name="formRows" value="<%=formRows%>">   
    <input type="hidden" name="counter" value="<%=counter%>">   	
</Form>

<%
}//end of if body for session
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>

</div>
</body>

</html>

