<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true" />
<html>
<head>
<title><%=LC.L_Review_Board%><%--*****Review Board--%></title>
</head>

<body>
<SCRIPT language="javascript">
function fnAddMoreComponentRows(formobj, exits)
{
	addBoardEnable();
	var rowId = formobj.rowId.value;
	var newAddCount = formobj.addmore.value
	var numCompRowExists = 0;
	numCompRowExists = formobj.totalExtgRows.value;
	for (var f=0; f<newAddCount; f++)
	{	
	    var cTurn= (eval(formobj.totalExtgRows.value)+1);	
		var myRow = document.getElementById("myMainTable").insertRow((eval(formobj.totalExtgRows.value)+1));
	    var myCell = myRow.insertCell(0);
		myCell.innerHTML = '<td><input type="text" id = "'+cTurn+'" name="boardName" value=""  maxlength="200" size="50"/></td>';		
		myCell = myRow.insertCell(1);
		myCell.innerHTML = '<td width="10%" align="center"><input type=image src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" onClick ="deleteRow(this.parentNode.parentNode.rowIndex)"></td></tr>';
		numCompRowExists++		
		rowId++;
		document.getElementById(cTurn).focus();
	}	
	formobj.rowId.value = rowId;	
}

function deleteRow(i)
	{
		document.getElementById('myMainTable').deleteRow(i);	
		addBoardDisable();
	}
	
function addBoardEnable()
	{
		var rbEnable = document.getElementById('addBoardLink');		
		rbEnable.disabled = true;			
	}
	
function addBoardDisable()
	{
		var rbDisable = document.getElementById('addBoardLink');
		rbDisable.disabled = false;
	}

function  validate(formobj)
{	
	if (!(validate_col('e-Signature',formobj.eSign))) return false
					
		if(document.getElementById("eSignMessage").innerHTML != "<%=LC.L_Valid_Esign%>")
		{
			alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
			formobj.eSign.focus();
			return false;
		} 
		if (!(validate_col('Board Name',formobj.boardName))) return false;
		formobj.boardName.value = formobj.boardName.value.replace(/^\s+|\s+$/g,"");
		if (!(splcharcheck(formobj.boardName.value)==true))return false;		
} 	
</SCRIPT>
<%
	String src = request.getParameter("srcmenu");	
	String mode = "";
%>
<jsp:include page="panel.jsp" flush="true">
	<jsp:param name="src" value="srctdmenubaritem5"/>
</jsp:include>
<%@ page language="java" import="com.velos.eres.business.team.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.StringUtil,com.velos.eres.web.reviewBoard.ReviewBoardJB,com.velos.eres.business.reviewboard.impl.ReviewBoardBean"%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*"%>
<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint" />
<jsp:useBean id="reviewB" scope="session" class="com.velos.eres.web.reviewBoard.ReviewBoardJB" />

<div class="tabDefTopN" id="div1">
<%
	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession)) 
	{
		int pageRight = 0;	
		String tab = request.getParameter("selectedTab");
		String ipAdd = (String) tSession.getValue("ipAdd");
		String usrId = (String) tSession.getValue("userId");
		ReviewBoardDao boardDao = new ReviewBoardDao();
		boardDao.getReviewBoardAllValues();
		ArrayList rbNames = boardDao.getRbNames();
		ArrayList rbIds = boardDao.getRbIds();		
%>
	<table width="100%" cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td colspan="4">
			<jsp:include page="irbConfigTabs.jsp" flush="true">
				<jsp:param name="tab" value="<%=tab%>" />
			</jsp:include>
			</td>
		</tr>
	</table>
</div>
<div class="tabDefBotN" id="div2">

<Form name="reviewBoard" id="reviewBoardId" method="post" action="updatereviewBoard.jsp" onSubmit="if (validate(document.reviewBoard)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<input type="hidden" name="mode" value=<%=mode%>> 
	<input type="hidden" name="selectedTab" value="<%=tab%>"> 
		<%
			int baseOginalRows = rbNames.size();
			String numExi = String.valueOf(baseOginalRows);
		%>
	<input type="hidden" name="totalExtgRows" value=<%=numExi%>>
	<input type="hidden" name="baseOrigRows" value=<%=baseOginalRows%>>	
	<table width="100%" cellspacing="0" cellpadding="0" border="0">	
			<table width="100%" cellspacing="0" cellpadding="0" border="0">			
				<input type="hidden" name="counterBox" value="" size="2">
				<tr><input type="hidden" name="addmore" value="1" size="1">	&nbsp;
				<td></td>
				<td width="80%" align="center">
				<input  class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" type='button' id="addBoardLink" onClick="return fnAddMoreComponentRows(document.reviewBoard, <%=numExi%>);" value="<%=LC.L_Add_NewBoard%>">					
				</td>
				</tr>
			</table>
			<table id="myMainTable" width="65%" border=1>
				<TR id="myiRow0">
					<th width="50%" align=center><%=LC.L_Board%><%--Board*****--%></th>
					<th width="5%" align=center><%=LC.L_Delete%></th>
					<th width="20%" align=center><%=LC.L_Add_NewMember%><%--Add New Member*****--%></th>
					<INPUT type="checkbox" name="hiddenChk" id="hiddenChk_0" style="visibility: hidden" />
					<%
						for (int counter = 0; counter < baseOginalRows; counter++) 
						{
								String rbName = (String) rbNames.get(counter);
								int rbId=(Integer)rbIds.get(counter);							
								%> <tr><td id="rbName" ><%=rbName%></td>															
								<td></td><!--<td><A href="#" onClick = "deleteRow(<%=counter%>)"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" /></A></td>-->
								<td align='center'><A href="modifymember.jsp?srcmenu=tdmenubaritem6&selectedTab=<%=tab%>&rbId=<%=rbId%>"><%=LC.L_Add_Member%><%--Add Member*****--%></A></td>
								</tr> <%
						}
					%>
			  </tr>
			</table>
	</table>
	<br/>
		<jsp:include page="submitBar.jsp" flush="true">
			<jsp:param name="displayESign" value="Y" />
			<jsp:param name="formID" value="reviewBoardId" />
			<jsp:param name="showDiscard" value="N" />
		</jsp:include>
		<Input type="hidden" name="rowId" value="<%=numExi%>">
		<%
		}//end of if body for session		
		else
		{
			%><jsp:include page="timeout.html" flush="true" /><%
		}
		%><div class="myHomebottomPanel"><jsp:include page="bottompanel.jsp" flush="true" /></div>
</div>
</Form>
</body></html>
