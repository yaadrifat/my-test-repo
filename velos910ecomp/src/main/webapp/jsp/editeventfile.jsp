<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=MC.M_EvtLibEvt_FilePage%><%--Event Library >> Event Appendix >> Event File Page*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>

<%	String fromPage = request.getParameter("fromPage"); %>
<% if ((fromPage.equals("selectEvent")) || (fromPage.equals("fetchProt")))
{ %>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>	

<%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>

 <% } //end for if ((request.getParameter("fromPage")).equals("selectEvent")) %>



<SCRIPT Language="javascript">

 function  validate(formobj){
     if (!(validate_col('eSign',formobj.eSign))) return false
     if (!(validate_col('Description',formobj.desc))) return false

	if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
   }

 }
</SCRIPT>


<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.esch.business.common.EventInfoDao"%><%@page import="com.velos.eres.service.util.*"%>
<jsp:useBean id="eventdocB" scope="request" class="com.velos.esch.web.eventdoc.EventdocJB"/>

<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>

<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>



<% String src;

src= request.getParameter("srcmenu");

//String eventName = request.getParameter("eventName");
String eventName = "";
//KM
if ((fromPage.equals("selectEvent")) || (fromPage.equals("fetchProt"))) {
%>
<jsp:include page="include.jsp" flush="true"/>

<%
}
 else{

%>

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   

<%}%>

<body>
<% if ((fromPage.equals("selectEvent")) || (fromPage.equals("fetchProt"))) {%>

		<DIV class="formDefaultpopup" id="div1"> 

	<%	} 
 
else { %>

<DIV class="formDefault" id="div1"> 

<%}%>	
 
<%
	String docId = request.getParameter("docId");
	String tab = request.getParameter("selectedTab");
	String docType = request.getParameter("docType");
	String docmode = request.getParameter("docmode");
	String eventId = request.getParameter("eventId");
	String duration = request.getParameter("duration");
	String protocolId = request.getParameter("protocolId");
	String calledFrom = request.getParameter("calledFrom");
	String mode = request.getParameter("mode");
	String calStatus = request.getParameter("calStatus");
	String eventmode = request.getParameter("eventmode");
	String displayType = request.getParameter("displayType");
	String displayDur = request.getParameter("displayDur");						

	eventdocB.setDocId(EJBUtil.stringToNum(docId));
	eventdocB.getEventdocDetails();
	

	//KM
	if (calledFrom.equals("P")||calledFrom.equals("L"))
	{
		  eventdefB.setEvent_id(EJBUtil.stringToNum(eventId));
		  eventdefB.getEventdefDetails();
		  eventName = eventdefB.getName();
	}else{	
		  eventassocB.setEvent_id(EJBUtil.stringToNum(eventId));
		  eventassocB.getEventAssocDetails();     
		  eventName = eventassocB.getName(); 
	 }			


	
	HttpSession tSession = request.getSession(true); 

	if (sessionmaint.isValidSession(tSession))
	{
	   String uName = (String) tSession.getValue("userName");
	   
	   String calAssoc = request.getParameter("calassoc");
	   calAssoc = (calAssoc == null) ? "" : calAssoc;
%>
<P class = "userName"><%=uName%></p>

<%
	String calName = "";

	if (fromPage.equals("eventbrowser") || fromPage.equals("eventlibrary") || fromPage.equals("patientschedule") )
	{
%>
	<!--P class="sectionHeadings"> Protocol Calendar >> Event Appendix >> Upload Document</P-->
<%
	}
	else
	{
		calName = (String) tSession.getValue("protocolname");
%>
	<P class="sectionHeadings"><%Object[] arguments1 = {calName}; %>
	    <%=VelosResourceBundle.getMessageString("M_PcolCalEvt_UploadDocu",arguments1)%><%--Protocol Calendar [ <%=calName%> ] >> Event Appendix >> Upload Document*****--%> </P>
<%
	}
%>

<form name="addfile" id="addevtfile" METHOD=POST action="updateeventfile.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&docId=<%=docId%>&docType=<%=docType%>&docmode=<%=docmode%>&eventId=<%=eventId%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&eventmode=<%=eventmode%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&calassoc=<%=calAssoc%>" onSubmit="if (validate(document.addfile)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

<TABLE width="90%">
      <tr> 
        <td width="35%"> <%=LC.L_Short_Desc%><%--Short Description*****--%> <FONT class="Mandatory" >* </FONT> 
        </td>
        <td width="65%"> 
          <input type=text name=desc MAXLENGTH=100 size=40 value='<%=eventdocB.getDocDesc()%>'>

        </td>
      </tr>
      <tr> 
        <td width="35%"> </td>
        <td width="65%"> 
          <P class="defComments">  <%=MC.M_ShortDescFile_100CharMax%><%--Give a short description of your file (100 char max.)*****--%> 
 </P>
        </td>
      </tr>

<jsp:include page="propagateEventUpdate.jsp" flush="true"> 
<jsp:param name="fromPage" value="<%=fromPage%>"/>
<jsp:param name="formName" value="addfile"/>
<jsp:param name="eventName" value="<%=eventName%>"/>
</jsp:include>   

 <jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="addevtfile"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>

</form>

<%

} else {  //else of if body for session

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
<% if ((fromPage.equals("selectEvent")) || (fromPage.equals("fetchProt"))) {

%>

<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
<%


}
else {

%>

  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

  </div>

  <div class ="mainMenu" id = "emenu">

  <jsp:include page="getmenu.jsp" flush="true"/>

  </div>

<% }%>
</body>

</html>


