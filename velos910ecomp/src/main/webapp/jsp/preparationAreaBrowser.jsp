<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><%=MC.M_MngInv_PrepAreaSrch%><%--Manage Inventory >> Preparation Area Search*****--%></title>
<jsp:include page="panel.jsp" flush="true"/>
<jsp:include page="ui-include.jsp" flush="true"/>
<script>

function confirmBox(name){
	var paramArray = [name];
    if (confirm(getLocalizedMessageString("M_WantDel_Pat",paramArray)))/*if (confirm("Do you want to delete <%=LC.Pat_Patient%> " + name+"?" ))*****/
	return true;
    else
	return false;
    
}

var currentEnabled1 = null ,currentEnabled2 = null;var rowId = 0;
//function to enable elements based on radio buttons.
function enableElement(elem1,elem2,elem3) {
if (currentEnabled1) {
     	    currentEnabled1.disabled = true;
      		currentEnabled1.value = "";
   	    }
if (currentEnabled2) {
	    currentEnabled2.disabled = true;
		currentEnabled2.value = "";
   }

elem1.disabled = false;
currentEnabled1 = elem1;
currentEnabled2 = elem2; 
 if(elem2){	
	elem2.disabled = false;
   } 
 document.patient.dateChoice.value = elem3;
}

function setCurrentDate(obj1,fdate){
	obj1.value=fdate;
}

function openOrgpopupView(patId,viewCompRight) {
	var viewCRight ;
	var rtStr;
	
	viewCRight = parseInt(viewCompRight);
	
	if (viewCRight <=0)
	{
		rtStr = "nx";
	}
	else
	{
		
		rtStr = "dx";
	}
	
	windowName = window.open("patOrgview.jsp?patId="+patId+"&c="+rtStr,"patient","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=300,height=150,left=400,top=400");
	windowName.focus();
}
otherLink=function(elCell, oRecord, oColumn, oData){
var htmlStr="";
htmlStr="<A HREF=\"javascript:void(0);\" onClick=\"openOrgpopupView('"+oRecord.getData("PK_PERSON")+"',"+oRecord.getData("RIGHT_MASK")+");\"><%=LC.L_View%><%--View*****--%></A>";
elCell.innerHTML=htmlStr;
} 
	//formatter added for adding checkbox in each row.		
checkList=function(elCell, oRecord, oColumn, oData)
{	
		
			var myRnum=0;
			if(oRecord._oData.RNUM){
				myRnum = oRecord._oData.RNUM;
			}		
			else{
				myRnum = oRecord._nCount+1;
			}
		//	alert(myRnum);
		//alert(YAHOO.lang.dump(oRecord));
			//alert(oRecord.getData("RNUM"));
		
var htmlStr="";
	htmlStr = "<input type='checkbox' name = 'checkRow' id='check_"+myRnum+"\' ></input>";
elCell.innerHTML=htmlStr;
}
//Ashu:Added for BUG#5794(10thMar11)
function checkForAll()
{

	var siteId=$('dPatSite'),studyId=$('studyId') ;
	var ip=siteId.selectedIndex,is=studyId.selectedIndex;
	if (is<0) return false;

	if ((siteId.options[ip].text=='All') && (studyId.options[is].text=='All'))
	{
		alert("<%=MC.M_CntSelOrg_WhileStd%>");/*alert("You cannot select 'All' Organizations while selecting 'All' <%=LC.Std_Studies%>");*****/
		return false;
	}

	//YK: 05APR2011 Bug#6012 
	var dateSel = document.patient.dateChoice.value;
	var fromDate = document.patient.fromDate.value;
	var toDate = document.patient.toDate.value;
	if(dateSel=="4")
	{
		//YK: 14APR2011 Bug#6012
		if(fromDate==null || fromDate=="" )
		{
			
			alert("<%=MC.M_PlsEtr_FrmDt%>");/*alert("Please enter From date");*****/
			document.patient.fromDate.focus();
			return false;
		}
		if(!isDate(fromDate,calDateFormat) )
		{
			var paramArray = [calDateFormat];
			alert(getLocalizedMessageString("M_IvdDt_PlsVldFmt",paramArray));/*alert("Invalid Date. Please enter a valid Date in '"+calDateFormat+"' format");*****/
			document.patient.fromDate.focus();
			return false;
		}
		if(toDate==null || toDate=="")
		{
			alert("<%=MC.M_PlsEtr_ToDt%>");alert("Please enter To date");
			document.patient.toDate.focus();
			return false;
		}
		
		if(!isDate(toDate,calDateFormat) )
		{
			var paramArray = [calDateFormat];
			alert(getLocalizedMessageString("M_IvdDt_PlsVldFmt",paramArray));/*alert("Invalid Date. Please enter a valid Date in '"+calDateFormat+"' format");*****/
			document.patient.toDate.focus();
			return false;
		}
		
	}
	
	return true;
}
//Ak:FixedBUG#5694(18Mar2011)
function orgRefresh(filter)
{
  l_fltr=filter||'';
  orgObject=new VELOS.ajaxObject("getStudyData.jsp",{
     urlData:"keyword=patenrorg&studyId="+$('studyId').value+"&userId="+$('userId').value+"&accountId="+$('accountId').value,
     success:function(o){
     $('orgDD').innerHTML=o.responseText;
     paginate_pat.rowsPerPage=0;
      
     }
   });
 orgObject.startRequest();
}
	
//function to select all rows
function checkAll()
{
	
var formobj=document.patient;
var kitLength=document.getElementsByName("checkRow").length;
if(formobj.checkallrow.checked){
	
 if(kitLength>0){ 	//AK: 10Mar2011 BUG#5854
	if(formobj.checkRow.length == undefined)
		{
			formobj.checkRow.checked = true;
		}else{
		for(i=0;i<formobj.checkRow.length;i++){			
			formobj.checkRow[i].checked=true;
			}			
	  	}
  }else
  {
	  alert("<%=MC.M_NoItem_PrstSel%>");/*alert("No Item present to select.");*****/
  }
}
else{
	if(kitLength>0){ //AK: 10Mar2011 BUG#5854
		if(formobj.checkRow.length == undefined)
		{
			formobj.checkRow.checked = false;
		}else{
		for(i=0;i<formobj.checkRow.length;i++){	
			formobj.checkRow[i].checked=false;
			 }			
	  	}
  }
 }
}

   var paginate_pat;     
  $E.addListener(window, "load", function() {  
	  orgRefresh();
	 paginate_pat=new VELOS.Paginator('prepArea',{
 				sortDir:"desc", 
				sortKey:"EVENT_SCHDATE_DATESORT",	
				defaultParam:"userId,accountId,grpId",
				filterParam:"patCode,dPatSite,studyId,patStdCode,dateChoice,inp1,year,year1,month,fromDate,toDate",
				dataTable:'serverpagination',
				searchEnable:false,
				saveEnable:false,
				exportEnable:false,
				viewCartEnable:true,				
				addCartEnable:true,
				manageCartEnable:true,
				prepareEnable:true								
				});
				//paginate_pat.render();
				
				 }  )    
				 
/* YK 26APR2011 Bug#6053*/
document.body.onkeypress = enterkey;

function enterkey(evt) {
  var evt = (evt) ? evt : event
  var charCode = (evt.which) ? evt.which : evt.keyCode
  if (charCode == 13) {
	  <%int ienet = 2;

	  String agent1 = request.getHeader("USER-AGENT");
	     if (agent1 != null && agent1.indexOf("MSIE") != -1)
	       ienet = 0; //IE
	      else
	  	ienet = 1;
	  	if(ienet == 0) {%>
	  if (checkForAll()){ rowId=0;  (paginate_pat.runFilter())};
	  <%}%>
  }
}   
 </script>


<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id ="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>

  
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="js/velos/dateformatSetting.js">/*Date Format*/</script>


<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.DateUtil,com.velos.eres.business.common.*,java.text.*"%>

<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.person.*,com.velos.eres.service.util.*,com.velos.eres.web.user.*,com.velos.eres.web.grpRights.GrpRightsJB"%>

<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="usrSite" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" />
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="groupB" scope="page" class="com.velos.eres.web.group.GroupJB"/>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<body class="yui-skin-sam" style="overflow:auto;">

<% String src;
src= request.getParameter("srcmenu");
BrowserRows br = new BrowserRows();

String module="prepArea" ;


  HttpSession tSession = request.getSession(true);

  if (sessionmaint.isValidSession(tSession))
{
	// give access right for default admin group to the delete link 

	String userId = (String) tSession.getValue("userId");

   	int usrId = EJBUtil.stringToNum(userId);
   	
   	int grpId=0;
   	
   	String protocolManagementRight = (String) tSession.getValue("protocolManagementRight");
		
	int protocolManagementRightInt = 0;
	protocolManagementRightInt = EJBUtil.stringToNum(protocolManagementRight);
	

	userB.setUserId(usrId);

	userB.getUserDetails();

	String defGroup = userB.getUserGrpDefault();
	
	grpId=EJBUtil.stringToNum(defGroup);

	groupB.setGroupId(grpId);

	groupB.getGroupDetails();

	String groupName = groupB.getGroupName();
	
	//Get User's saved searches 
	BrowserDao bDao=new BrowserDao();
	bDao.getBrowserSavedSearch(module,userId);
	ArrayList searchNameList=bDao.getSearchNameList();
	ArrayList searchCriteriaList=bDao.getSearchCriteriaList();
	ArrayList defaultList=bDao.getSearchDefaultList();

		

	
   	int pageRight = 0;
	GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");		
	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MPATIENTS"));

 if (pageRight>0) {

	Calendar cal = new GregorianCalendar();
	String studyId = "";
	String selectedTab = "" ; 
   	selectedTab=request.getParameter("selectedTab");
	String userIdFromSession = (String) tSession.getValue("userId");	
	String uName = (String) tSession.getValue("userName");	
	UserJB user = (UserJB) tSession.getValue("currentUser");

   	String siteId = user.getUserSiteId();
   	String accountId = user.getUserAccountId();
   	String active_status="" ;   
    String group = "";

	
 	String patCode = "";
 	String patStdCode ="";
 	String ptxt="All",atxt="All",gtxt="All",rtxt="",rbytxt="All" , pattxt="All";
	String spltxt = "All" ;
	
	String selSite = "";
	//Written for the filter 
	String orgId = "";  //for passing the org id 
	String orgtxt = ""; //for passing the org name
	String stdyId = "" ; //for passing the study id 
	String stdytxt = "" ; //for passing the study name
	String patstatId = ""  ; //for passing the patstatusid
	String regByname = "";
	String deathdt = "";

	
 	int lowLimit=0,highLimit=0 ;
	patCode = request.getParameter("patCode");
	patStdCode = request.getParameter("patStdCode");
	if (patCode==null)	 patCode="";		
	if (patStdCode==null)	 patStdCode="";
	int patDataDetail = 0;
	int personPK = 0;
	int usrGroup = 0;
	userB.setUserId(EJBUtil.stringToNum(userIdFromSession));
	userB.getUserDetails();
	usrGroup =EJBUtil.stringToNum( userB.getUserGrpDefault());
	personPK = EJBUtil.stringToNum(request.getParameter("pkey"));
	 

	selSite =  request.getParameter("dPatSite");
	
	Date dt1 = new java.util.Date();
	Date dt2= new java.util.Date();
	Format formatter;        
	
	String minDate="";
	String maxDate="";
	int inSite = 0;
	String uOrg = "";
	StringBuffer sbSite = new StringBuffer();

	
	//if( (request.getParameter("spltxt")) != null && (request.getParameter("spltxt").length()> 0))
	//spltxt = request.getParameter("spltxt");
	
	//String spltxtName = request.getParameter("speciality");
	//if (spltxtName ==null || spltxtName.equals("")) spltxt="All";
		 
	
	
	// JM : 04May05
	if (orgId.equals("0")) 
	{
		
		 siteId ="0"; 		 
	}	

	

	if (   (!  ( orgId == null ))  && ( !orgId.equals("null")) && (!(orgId.equals("")     )) )
	{
		
		 siteId = orgId; 		
	}
	
	
	else  if (     !(selSite == null) &&  (  !  (  selSite.trim() ).equals("") )  )
	{
		 siteId = selSite;		
	}
		
		
	
	 //for study and patient name
	 String  studyIdStr = "";
	 int idStudy = 0 ;
	 studyIdStr = request.getParameter("dStudy");
	 if ( studyId != null )
	 {
	 	idStudy = EJBUtil.stringToNum(studyIdStr);		
	}
	 if ( ( stdyId != null )  && ( !stdyId.equals("null"))    && (!(stdyId.equals("")     )) )
	{
		idStudy = EJBUtil.stringToNum(stdyId);
	}
	
	
	 UserSiteDao usd = new UserSiteDao ();
	 ArrayList arSiteid = new ArrayList();
	 ArrayList arSitedesc = new ArrayList();
	 String ddSite = "";
	 
	 usd = usrSite.getSitesWithViewRight(EJBUtil.stringToNum(accountId),EJBUtil.stringToNum(userId));
	 
	 arSiteid = usd.getUserSiteSiteIds();	 
	 arSitedesc = usd.getUserSiteNames();	 
 	 //6 November
		 
	
		if ( ( orgtxt == null )  || ( orgtxt.equals("null"  )   )    || (orgtxt.equals(""))   )
		{
			//take the orgtxt from session else take it from the parameters set on the Go 	
	
			for(int cnt = 0 ; cnt < arSiteid.size() ; cnt++)
			{
				if(       siteId.equals(  arSiteid.get(cnt).toString() )   )  
				{
					orgtxt = arSitedesc.get(cnt).toString() ;				
					break ; 
					
				}
			}
			
			
		}


 //AK:Commented for BUG#5704
 /*	 sbSite.append("<SELECT NAME='siteId' id='siteId'>") ;	
	 sbSite.append("<option selected value='' >All</option>") ;	 		
		if (arSiteid.size() > 0)
		{
			for (int counter = 0; counter < arSiteid.size()  ; counter++)
			{
				//iSite = (Integer)arSiteid.get(counter);
				inSite = Integer.parseInt((String)arSiteid.get(counter));
				if(inSite == EJBUtil.stringToNum(selSite)){			
					sbSite.append("<OPTION value = "+ inSite+" selected>" + arSitedesc.get(counter)+ "</OPTION>");
				}else{						
				sbSite.append("<OPTION value = "+ inSite+">" + arSitedesc.get(counter)+ "</OPTION>");
				}
			
			}
		}

		sbSite.append("</SELECT>");
		 ddSite  = sbSite.toString();*/
	
	///////////////

	 
	 CodeDao cd1 = new CodeDao();
	 CodeDao cd2 = new CodeDao();
	 CodeDao cd3 = new CodeDao();
     
	 boolean withSelect = true;  	     
	 
	
	 String ddstudy ="";
//   	StudyDao studyDao = studyB.getUserStudies(userId, "dStudy",idStudy,"active"); 
    //Ashu:Modified for BUG#5794(10thMar11)
    StringBuffer sbStd = new StringBuffer();
    Integer iStd ;
	//StudyDao studyDao = studyB.getUserStudies(userId, "studyId id='studyId' STYLE='WIDTH:177px' onchange='if (checkForAll()) {orgRefresh();}' ",idStudy,"activeForEnrl");
	//ddstudy = studyDao.getStudyDropDown();
	StudyDao studyDao = studyB.getUserStudies(userId,"studyId",0,"activeForEnrl");
	ArrayList stdIds = studyDao.getStudyIds();
	int selVal=0;
	if( stdIds!=null && stdIds.size()>0){ //AK:Fixed BUG#5988 (30thMar11)
	selVal=((Integer)stdIds.get(0)).intValue(); 
	ArrayList stdNums = studyDao.getStudyNumbers();
	ddstudy = EJBUtil.createPullDownWAll("studyId id='studyId' STYLE='WIDTH:177px' onchange='if (checkForAll()) {orgRefresh();}' ",selVal ,
			stdIds,stdNums);
	}else{
		sbStd.append("<SELECT NAME='studyId' id='studyId' onchange='if (checkForAll()) {orgRefresh();}' STYLE='WIDTH:177px' >") ;
		sbStd.append("<OPTION value = 0 >"+LC.L_All/*All*****/+"</OPTION>") ;
	    sbStd.append("</SELECT>");
		ddstudy  = sbStd.toString();
	}
	int year = 1999;
	
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy"); 
	String s1 =sdf.format(new Date());
	int currentYear=Integer.parseInt(s1);
	
	String dYear="<select size='1' name='year' id='year' disabled>";	
	 dYear = dYear+" <option value='1999' SELECTED>"+LC.L_1999/*1999*****/+"</option>" ;
	for(int i=0;i<=25;i++){				
		 if(year+i == currentYear){
			 dYear = dYear+" <option value='"+ (year+i) +"' SELECTED>"+ (year+i) +"</option>" ;
			}
		else{
				dYear = dYear+" <option value='"+ (year+i) +"'>"+ (year+i) +"</option>" ;
		}
	}
	 dYear = dYear + "</select>";
	 
	 year = 1999;
	 
	 String dYear1="<select size='1' name='year1' id='year1' disabled>";

	for(int i=0;i<=25;i++){		 
		if(year+i == currentYear){
			 dYear1 = dYear1+" <option value='"+ (year+i) +"' SELECTED>"+ (year+i) +"</option>" ;
			}
		else{
				dYear1 = dYear1+" <option value='"+ (year+i) +"'>"+ (year+i) +"</option>" ;
		}
	}
	 dYear1 = dYear1 + "</select>";
	 
    	 
	 String dMonth="<select size='1' name='month' id='month' disabled>";	
	 dMonth = dMonth+" <option value='1' SELECTED>"+LC.L_1/*1*****/+"</option>" ;
	for(int i=2;i<=12;i++){		 
		 dMonth = dMonth+" <option value='"+ i +"'>"+ i +"</option>" ;
	}
	 dMonth = dMonth + "</select>"; 
		  	   
	    
%>
 
 
 

<input type="hidden" id="srcmenu" name="srcmenu" Value="<%=src%>">
<input type="hidden" name="searchFrom" Value="search">
<input type="hidden" name="dstudyId" Value=<%=studyId%>>

<input type="hidden" id="accountId" Value=<%=accountId%>>
<input type="hidden" id="userId"  name="userId"  Value=<%=userId%>>
<input type="hidden" id="grpId" name="grpId" Value=<%=grpId%>>
<input type="hidden" id="pMRI" name="pMRI" Value=<%=protocolManagementRightInt%>>
<input type="hidden" id="groupName" name="groupName" Value=<%=groupName%>>

  <Form name="patient" METHOD="POST" onsubmit="return false;">
 
 <DIV class="tabDefTopN" id="div1">
  <jsp:include page="inventorytabs.jsp" flush="true">
  <jsp:param name="selectedTab" value="4"/> 
  </jsp:include> 
</DIV>
 <DIV class="tabFormTopN tabFormTopN_PAS" id="div2">
  <table class="basetbl outline midAlign lhsFont" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr class="tmpHeight plainBG"></tr>
    <tr height="18">
	<td  colspan="9"><b><%=LC.L_Search_By%><%--Search By*****--%></b></td>
	</tr>
	<tr >
	<td > <%=LC.L_Patient_Id%><%--<%=LC.Pat_Patient%> ID*****--%>: </td>
	<td > <Input type=text name="patCode" id="patCode"  size="15" value = "<%=patCode%>">&nbsp;</td>
	<td  align="right"><%=LC.L_Study%><%--<%=LC.Std_Study%>*****--%>:&nbsp;</td>	
	<td  ><%=ddstudy%></td>	
	<!-- BK:Feb/10/2011 Fixed Bug 5767 --> 	<!-- Ashu:03/10/2011 Browser compatablity alignment for Mozilla --> 
	<td  align="right" ><%=LC.L_Patient_StudyId%><%--<%=LC.Pat_Patient%> <%=LC.Std_Study%> Id*****--%>:&nbsp;</td>	
	<td > <Input type=text name="patStdCode" id="patStdCode"  size="15" value = "<%=patStdCode%>"> </td>
	<td  align="right"><%=LC.L_Organization%><%--Organization*****--%>:&nbsp;</td>
	<td  ><span id="orgDD"> </td>	
	</tr>
	<tr  >
	<td colspan="8">&nbsp;</td>
	</tr>
	<tr  >
	<td colspan="8" >
   <table  width="100%" cellspacing="0" cellpadding="0" border="0">
	<tr >
	<td ><%=LC.L_Sch_On%><%--Scheduled On*****--%>:&nbsp;</td>	
	<td >		
	<input type="radio" name="dateOption" id = "dateOption" value="1" checked="checked"
	onclick="enableElement(document.patient.elements['dateChoice'],null,1);"/>&nbsp;<%=LC.L_Today%><%--Today*****--%>
	</td>
	<td >	
	<input type="hidden" name="dateChoice" id = "dateChoice" value="1" />
	</td>
	<td >	
	<input type="radio" name="dateOption" id ="dateOption" value="2"
	onclick="enableElement(document.patient.elements['year'],null,2);document.patient.elements['year'].value = '<%=currentYear%>';"/>&nbsp;<%=LC.L_Year%><%--Year*****--%>	
	<%=dYear%>
	</td>	
	<td  >	
	<input type="radio" name="dateOption" id ="dateOption" value="3"
	onclick="enableElement(document.patient.elements['year1'],document.patient.elements['month'],3);document.patient.elements['year1'].value = '<%=currentYear%>';document.patient.elements['month'].value = '1';"/>&nbsp;<%=LC.L_Month%><%--Month*****--%>
	    <%=dMonth%>
		<%=dYear1%>		
	</td>	
	<!-- YK: 14APR2011 Bug#6012 -->
	<td  >	
	<input type="radio" name="dateOption" id ="dateOption" value="4"
	onclick="enableElement(document.patient.elements['fromDate'],document.patient.elements['toDate'],4);"/>
<%-- INF-20084 Datepicker-- AGodara --%>
	&nbsp;<%=LC.L_Date_Range%><%--Date Range*****--%>: 
	<%=LC.L_From%><%--From*****--%>&nbsp;<input type="text" size="10" name="fromDate" id = "fromDate" disabled="disabled" class="datefield" />
	<%=LC.L_To%><%--To*****--%>&nbsp;<input type="text" size="10" name="toDate" id = "toDate" disabled="disabled" class="datefield" />
	</td>				
	<!-- <A href="javascript:void(0);"  onClick="paginate_pat.runFilter()"><img type="image" src="../images/jpg/Go.gif" border="0"></A>-->
	<td >
	<button type="submit"
	onclick="if (checkForAll()){ rowId=0;  (paginate_pat.runFilter())};"><%=LC.L_Search%></button>
	</td>
 	</tr>
</table>
</td>

</tr>
  </table>
</div>

<DIV class="tabFormBotN tabFormBotN_MI_3" id="div3" style="width:99%;height:71%;overflow:auto;position:absolute;border-bottom: 20%;">
<div id="serverpagination">	<P class="defComments_txt" align="center">
<%=MC.M_Specify_SrchCriteria%><%--Please specify Search criteria*****--%> 
</P></div>
<input type="hidden" name="selectedTab" Value=<%=selectedTab%>> 	  




</DIV>    	



  </Form>

 		

<% } 
//end of if body for page right
else{
%>

<jsp:include page="accessdenied.jsp" flush="true"/>
<%
} //end of if body for session

}
else{

%>

  <jsp:include page="timeout.html" flush="true"/>

  <%

}



%>

   <div> 

    <jsp:include page="bottompanel.jsp" flush="true"/>

  </div>
</div>
<div class ="mainMenu" id = "emenu">

 <jsp:include page="getmenu.jsp" flush="true"/>

</div> 

</body>

</html>
