<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>


<%@ page language = "java" import="com.velos.eres.business.common.*,com.velos.eres.service.util.*,java.util.*,com.velos.eres.service.util.EJBUtil"%>
<%@page import="com.aithent.audittrail.reports.AuditUtils"%>
<jsp:useBean id="formfieldB" scope="request" class="com.velos.eres.web.formField.FormFieldJB"/>
<jsp:useBean id="fldLibB" scope="request" class="com.velos.eres.web.fieldLib.FieldLibJB"/> <!--KM-->

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/> 

<BODY> 
 
<% 
	String[] formFldId=new String[1000];
	String modBy="";
	
	HttpSession tSession = request.getSession(true); 
	 if (sessionmaint.isValidSession(tSession))	{ 	
%>	
		<jsp:include page="sessionlogging.jsp" flush="true"/>
 <%  
						
		
						String eSign = request.getParameter("eSign");	
						String oldESign = (String) tSession.getValue("eSign");
						if(!oldESign.equals(eSign)) {
%>
	 		  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
			} else {
	
					int ret=0;
					formFldId= request.getParameterValues("chkformFldId");
					modBy=(String) tSession.getAttribute("userId");
					int formId=0;
				    if(request.getParameter("formId")!=null && !request.getParameter("formId").equals("")){ 
					formId=Integer.parseInt(request.getParameter("formId"));
					}
					//Added by Gopu to fix the Bugzilla issue 2593.
					boolean hasInterFieldActions = false;
					int fldLen=formFldId.length;
					String fldId="";
					for(int fId=0;fId<fldLen;fId++) {
					    fldId=formFldId[fId];
					    formfieldB.setFormFieldId(Integer.parseInt(fldId));
					    formfieldB.getFormFieldDetails();
					
					    hasInterFieldActions = fldLibB.hasInterFieldActions(formfieldB.getFieldId());
					
					    if (hasInterFieldActions) {
					    	ret = -4;
					    }	
					}
								
					if(ret != -4)
					{
						//Modified for INF-18183 ::: Ankit
						  String moduleName = ("L".equals(request.getParameter("calledFrom")))? LC.L_Form_Lib : LC.L_Form_Mgmt;
				      	  formfieldB.removeFormField(formId,formFldId,modBy,AuditUtils.createArgs(session,"",moduleName));
					}					
					if (ret==-2) 
					{%>	

						<br><br><br><br><br><br>
	
						<table width="100%"  >
						<tr>
						<td align=center><p class = "sectionHeadings"><%=MC.M_Data_CntDel%><%--Data cannot be deleted.*****--%></p>
						<button onclick="window.history.back();"><%=LC.L_Back%></button>
					<%} else 
					if(ret == -4)
					{
					%>
					<table width="100%"  >
						<tr>
						<td align=center><p class = "sectionHeadings"><b><%=MC.M_Data_CntDel%><%--Data cannot be deleted.*****--%></b></p>
						<p class = "sectionHeadings"><b><%=MC.M_FldAct_RemFldTryAgain%><%--This field is part of one or more Inter Field Actions. Please remove the field from the Inter Field Action(s) and then try again.*****--%></b></p>
						
						<button onclick="window.history.back();"><%=LC.L_Back%></button>
					
					<%
					}
					else
					{ %>
						
						<br><br><br><br><br><br>
						<table width="100%"  >
						<tr>
						<td align=center>
						<P class="sectionHeadings"><%=MC.M_Data_DelSucc%><%--Data deleted successfully.*****--%></P>
						<script>
							window.opener.location.reload();
							setTimeout("self.close()",1000);
						</script>	
	
					<%}
					%>
					</p>
			 			</td>
						</tr>
						<tr height=20></tr>
						<tr>
						<td align=center>
						</td>		
						</tr>		
						</table>				
						<br><br><br><br><br> <p class = "sectionHeadings" align = center>  </p>
				<% 
				} //end of else part
		
		}//end of session
	else
	{ %> 
	 	<jsp:include page="timeout.html" flush="true"/> 
 <% } %>

</body>
</HTML>


