<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
	<SCRIPT>
	function AddInvoice(windowName,pgRight)
	{
	if (f_check_perm(pgRight,'N') == true) {
      windowName= window.open(windowName,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=530,left=125,top=200")
      windowName.focus();
	}else {
		return false;
	}

	}

	function viewInvoice(strCovTypes,inv,mode,pgright)
	{
	 if (mode == 'E')
	 {
       if(!f_check_perm(pgright,'E')) {
	          return false;
	  }
	 } 
      windowName= window.open("viewInvoice.jsp?invPk=" + inv + "&CoverType="+strCovTypes+"&mode=" + mode,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=530,left=125,top=200")
      windowName.focus();

	}
	 function confirmBox(name,pageRight,value){
	 
	  if(!f_check_perm(pageRight,value)) {
	          return false;
	  }

	  	var paramArray = [name];
        if (confirm(getLocalizedMessageString("M_Want_DelInv",paramArray))) {/* if (confirm("Do you want to delete Invoice "+ name+"?" )) {*****/
              return true;
       }
        else
       { 
	  return false;
	  }

}


//KM-031708
function printWindow(invPk){
       windowName= window.open("invoicePrint.jsp?invPk=" + invPk,"Information", "toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=530,left=125,top=200")
       windowName.focus();
    
}



//JM: 13Mar2008: #FIN11
 function openWinStatus(pgRight,mode, studyNumber, invoiceId,invNumber,statusId){
 

	var checkType;

	 
		checkType = 'E';

	var otherParam;

		otherParam = "&moduleTable=er_invoice&statusCodeType=invStatus&sectionHeading=<%=MC.M_MstoneInv_StatDets%>&statusId=" + statusId;/*otherParam = "&moduleTable=er_invoice&statusCodeType=invStatus&sectionHeading=Milestones >> Invoicing >> Status Details&statusId=" + statusId;*****/

		if (f_check_perm(pgRight,checkType) == true) {

			windowName= window.open("editstatus.jsp?mode=" + mode + "&studyNumber="+ studyNumber+"&modulePk=" +  invoiceId + "&invNumber=" + invNumber + otherParam,"statusdetails","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=500,height=300");
			windowName.focus();
		}
}

</SCRIPT>


<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="invB" scope="page" class="com.velos.eres.web.invoice.InvoiceJB" />
<%@ page language = "java" import = "com.velos.eres.business.common.InvoiceDao,com.velos.eres.business.common.TeamDao,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*,com.velos.esch.business.common.*"%>

<%

	String studyId = request.getParameter("studyId");

	studyB.setId(EJBUtil.stringToNum(studyId));
	studyB.getStudyDetails();
	String studyNumber = studyB.getStudyNumber();
	



	String calledFrom = request.getParameter("calledFrom");
	int pageRight = EJBUtil.stringToNum(request.getParameter("pageRight"));
	String paymentPk = request.getParameter("paymentPk");


if (StringUtil.isEmpty(calledFrom))
{

	 calledFrom = "";
}

	String src;
	src= request.getParameter("srcmenu");
	String selectedTab = request.getParameter("selectedTab");

		InvoiceDao invdao = new InvoiceDao();
		int invCount = 0;
		invdao = invB.getSavedInvoices(studyId);

		ArrayList arId = new ArrayList ();
		ArrayList  arInvDate = new ArrayList ();

		ArrayList arInvIntervalFrom = new ArrayList ();
		ArrayList  arInvIntervalTo = new ArrayList ();

		ArrayList arInvNotes = new ArrayList();

		ArrayList arInvNumber = new ArrayList();

		ArrayList  arCreator = new ArrayList();


//JM:
		ArrayList arInvStats = new ArrayList();

	 	ArrayList arHistoryIds = new ArrayList();

	 	ArrayList arSubTypes = new ArrayList();
	 	
	 	ArrayList arCovTypes = new ArrayList();

		String invDate = "";
		String invNumber = "";
		String invCreatedBy = "";
		String invNotes = "";
		String invRange = "";
		String invId = "";
		String dateFrom = "";
		String dateTo = "";

		String invStat = "";
		String historyId = "";
		String subType = "";
		String strCovTypes = "";
		if (invdao != null)
		{
			arId =  invdao.getId();
            arInvNumber = invdao.getInvNumber();
            arInvDate =   invdao.getInvDate();
            arCreator =   invdao.getCreator();
            arInvIntervalFrom =  invdao.getInvIntervalFrom();
            arInvIntervalTo =  invdao.getInvIntervalTo();
            arInvNotes =    invdao.getInvNotes();
            arInvStats =    invdao.getInvStats();
            arHistoryIds =    invdao.getHistoryIds();
			arSubTypes = invdao.getSubTypes();
			arCovTypes = invdao.getCovType();
			

			if (arId == null)
			{
				arId = new ArrayList();
			}
		}


		if (StringUtil.isEmpty(calledFrom))
	 	{
	 	%>
	 	<table width="98%"><tr height="18"><td align="right" width="98%">
	 	<A href="invoice_step1.jsp?studyId=<%=studyId%>&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>"  onClick="return f_check_perm(<%=pageRight%>,'N')"><%=MC.M_CreateNew_Inv_Upper%><%--CREATE A NEW INVOICE*****--%></A>
		</td></tr></table>
		<% } %>

	 <table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl outline midAlign">
      <tr>
        <th width="10%"><%=LC.L_Invoice%><%--Invoice*****--%> #</th>
        <th width="5%"> <%=LC.L_Edit %><%--Edit*****--%></th>
         <th width="5%"><%=LC.L_Print%><%--Print*****--%></th>
        <th width="10%"><%=LC.L_Invoice_Date%><%--Invoice Date*****--%></th>
        <th width="10%"><%=LC.L_Created_By%><%--Created By*****--%> </th>
        <th width="10%" ><%=LC.L_Date_Range%><%--Date Range*****--%></th>
        <th width="20%"><%=LC.L_Status%><%--Status*****--%>        </th>
        <th width="23%"><%=LC.L_Notes%><%--Notes*****--%></th>
        <%if (StringUtil.isEmpty(calledFrom)){  %>
        <th width="5%"><%=LC.L_Delete%><%--Delete*****--%></th>
        <%}else{ %>
        <th width="5%"><%=LC.L_Select%><%--Select*****--%></th>
        <%} %>
      </tr>

<%
	invCount = arId.size();


	 for(int counter = 0;counter<invCount;counter++)
	{
	    dateFrom =  (String) arInvIntervalFrom.get(counter);

        dateTo =    (String) arInvIntervalTo.get(counter);
		invDate = (String) arInvDate.get(counter);
		invNumber = (String) arInvNumber.get(counter);
		invCreatedBy = (String) arCreator.get(counter);
		invNotes = (String) arInvNotes.get(counter) ;
		invId = (String) arId.get(counter);

		//JM:
		invStat = (String) arInvStats.get(counter);
		invStat=(invStat==null)?"":invStat;

		historyId = (String) arHistoryIds.get(counter);
		historyId=(historyId==null)?"":historyId;

		subType = (String) arSubTypes.get(counter);
		subType = (subType==null)?"":subType;
		
		if(arCovTypes!=null  && arCovTypes.size()>0 ){
			strCovTypes = (String) arCovTypes.get(counter);
			strCovTypes = (strCovTypes==null)?"":strCovTypes;			
		}
		if (StringUtil.isEmpty(dateFrom))
		{
			dateFrom = "";
		}

		if (StringUtil.isEmpty(dateTo))
		{
			dateTo = "";
		}

		if (StringUtil.isEmpty(invDate))
		{
			invDate = "";
		}

			if (StringUtil.isEmpty(invNumber))
		{
			invNumber = "";
		}
			if (StringUtil.isEmpty(invCreatedBy))
		{
			invCreatedBy = "";
		}
		if (StringUtil.isEmpty(invNotes))
		{
			invNotes = "";
		}

		if (! StringUtil.isEmpty(dateFrom ))
		{
			invRange = dateFrom + " - " + dateTo;
		}
		else
		{
			invRange = LC.L_All/*"All"*****/;
		}

		String invNumberSubStr = "";
		if(invNumber.length() > 20){
		   invNumberSubStr = invNumber.substring(0,20);
		}
		else {
		  invNumberSubStr = invNumber;

		}


		%>

	<%
		if(counter%2==0){
	%>
		<TR class="browserEvenRow">
	<%
	}else{
	%>
		<TR class="browserOddRow">
	<%
	}
	%>

	<td class="tdDefault" align="center"><A href="#" onClick="return viewInvoice('<%=strCovTypes.replaceAll("'","") %>','<%=invId%>','V',<%=pageRight%>)" onClick="" HREF="#"><%=invNumber%></A>
	</td>
	<td align="center">
	<%if (subType.equals("F")){
	}else {%>
	&nbsp;&nbsp;&nbsp;<A href="#" onClick="return viewInvoice('<%=strCovTypes.replaceAll("'","") %>','<%=invId%>','E',<%=pageRight%>)" onClick="" HREF="#"><img src="./images/edit.gif" title="<%=LC.L_Edit%>" border="0"/></A>   <!--Akshi:Modified for Bug#8607-->
	<%}%>

	</td>
	<td align="center"><A href="#" onClick="return printWindow('<%=invId %>')"> <img src="./images/printer.gif" title="<%=LC.L_Print%>" border="0"/> </A>  </td>
	
	<td class="tdDefault" align="center"><%=invDate%></td>
	<td class="tdDefault" align="center"><%=invCreatedBy %></td>
	<td class="tdDefault" align="center"><%=invRange %></td>

	<!--JM: #FIN11, Feb2008 enhancements-->
	<td class="tdDefault" >
	<!--KM:Modified-->
	<A href="#" onclick="openWinStatus(<%=pageRight%>,'M','<%=StringUtil.encodeString(studyNumber)%>','<%=invId%>','<%=invNumber%>','<%=historyId%>')"><%= invStat%></A>&nbsp;&nbsp;
		
	<%
	if (StringUtil.isEmpty(calledFrom))
 	{  %>
	<A href="#" onclick="openWinStatus(<%=pageRight%>,'N','<%=StringUtil.encodeString(studyNumber)%>','<%=invId%>', '<%=invNumber%>','0')"><img src="./images/edit.gif" title="<%=LC.L_Copy%>" border="0"/></A>&nbsp;&nbsp;
	<A href="showinvoicehistory.jsp?modulePk=<%=invId%>&pageRight=<%=pageRight%>&srcmenu=<%=src%>&selectedTab=7&from=invhistory&fromjsp=showinvoicehistory.jsp&invNumber=<%=invNumber%>&currentStatId=<%=historyId%>&studyId=<%=studyId%>"><img border="0" title="<%=LC.L_History%>" alt="<%=LC.L_History%>" src="./images/History.gif" ><%//=LC.L_H%><%--H*****--%></A></td>
	<% } else { %>
	&nbsp;
	<% }  %>
	</td>

	</td>
	<td class="tdDefault" ><%=invNotes %></td>
	
	<!-- Added by Manimarn for #FIN12 Enhancement -->
	
	<td align="center">
	
	
	<%
	if (StringUtil.isEmpty(calledFrom))
 	{  %>
 		 <A href="deleteInvoice.jsp?studyId=<%=studyId%>&invoiceId=<%=invId%>&selectedTab=<%=selectedTab%>&srcmenu=<%=src%>" 
		onClick="return confirmBox('<%=invNumberSubStr%>',<%=pageRight%>,'E')"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0"/>  
	<% } else { %>
		<a href="linkInvDetails.jsp?invPk=<%=invId %>&studyId=<%=studyId %>&paymentPk=<%=paymentPk%>&pR=<%=pageRight%>" ><%=LC.L_Select%><%--Select*****--%></a>
	<% }  %>
	</td>
	</tr>




	<%


	}

	 %>

