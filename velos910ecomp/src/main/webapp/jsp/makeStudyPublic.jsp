<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<HTML> 
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 <Link Rel=STYLESHEET HREF="common.css" type=text/css> 
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
 <BODY> 
 <jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
 <jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
 <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
 <%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,javax.mail.*, javax.mail.internet.*, javax.activation.*,com.velos.eres.service.util.MC"%> 
 <%@page import="com.velos.eres.service.util.LC"%> <%-- YK: Bug #5415 : Localization Bug--%> 
 <%  HttpSession tSession = request.getSession(true); 
     int ret = 0; 
   String studyId = (String) request.getParameter("studyId");
   String mode = (String) request.getParameter("mode");
   String srcmenu = (String) request.getParameter("srcmenu");
   String selectedTab = (String) request.getParameter("selectedTab");
String eSign = request.getParameter("eSign");
    StudyViewDao svd = new StudyViewDao();

if (sessionmaint.isValidSession(tSession))

 {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<% 
 	String oldESign = (String) tSession.getValue("eSign");


	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {
	
		//check whether any of the public flag is set to yes
	    studyB.setId(EJBUtil.stringToNum(studyId));
		studyB.getStudyDetails();
		String studyPubCol = studyB.getStudyPubCol();

		if (studyPubCol == null)
			studyPubCol = "";
			 		
		if (studyPubCol.indexOf('1')==-1) {
	%>
		<br><br><br><br><br><br><br><br>
		<table width=100%>
		<tr>
			<td align=center>
				<P class="successfulmsg" align="center"><%=MC.M_NoSecAvailPublic_TryAgain%><%-- None of the sections in Study Summary are available for public viewing. Please make any of the section public and try again.*****--%></P>
			</td>
		</tr>
		<tr height=20></tr>
		<tr>
			<td align=center>
				<button onclick="window.history.back();"><%=LC.L_Back%></button>
			</td>		
		</tr>		
	</table>		
	<%
		} else { 

 	
	int pkStudyView = 0;
 	pkStudyView =  svd.getStudyViewIdByStudy(EJBUtil.stringToNum(studyId));
	
	%>
	
	<jsp:include page="getsnapshot.jsp" flush="true"> 
	<jsp:param name="studyId" value="<%=studyId%>"/>
	<jsp:param name="refreshFlag" value="T"/>
	</jsp:include>   
<%
   String ipAdd = (String) tSession.getValue("ipAdd");
   String usr = (String) tSession.getValue("userId");

  
   studyB.getStudyDetails();
   studyB.setStudyPubFlag("Y");
   studyB.setModifiedBy(usr);
   studyB.setIpAdd(ipAdd);
   studyB.updateStudy();
%>
<br> <br> <br> <br> 
<p class = "successfulmsg" align = center> <%=MC.M_PcolReleased_ToPublic%><%-- The protocol has been released to public*****--%> </p>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=notify.jsp?mode=M&srcmenu=<%=srcmenu%>&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>"> 

<%
	} //end of if of public column check
}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

 </BODY>
 </HTML>   
