<%@taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC" %>
<script>
$j(document).ready(function(){
	studyPurposeAction();
	studyPhaseAction();
	alignTrailDetailsField();
});
var studyPhaseNA = <s:property escape="false" value="ctrpDraftJB.draftStudyPhaseNA" />
var studyPurposeOther = <s:property escape="false" value="ctrpDraftJB.studyPurposeOtherPk" />
function studyPhaseAction() {
	if (!$('ctrpDraftJB.ctrpStudyPhase')) { return; }
	if (!$('ctrp_draft_pilot_row')) { return; }
	if ($('ctrpDraftJB.ctrpStudyPhase').options[$('ctrpDraftJB.ctrpStudyPhase').selectedIndex].value == studyPhaseNA) {
		$('ctrp_draft_pilot_row').style.visibility = 'visible';
	} else {
		$('ctrp_draft_pilot_row').style.visibility = 'hidden';
	}
}
function studyPurposeAction() {
	if (!$('ctrpDraftJB.studyPurpose')) { return; }
	if (!$('ctrp_draft_purpose_other_row')) { return; }
	if ($('ctrpDraftJB.studyPurpose').options[$('ctrpDraftJB.studyPurpose').selectedIndex].value == studyPurposeOther) {
		$('ctrp_draft_purpose_other_row').style.visibility = 'visible';
		$('study_purpose_other_asterisk').style.visibility = 'visible';
	} else {
		$('ctrp_draft_purpose_other_row').style.visibility = 'hidden';
		$('study_purpose_other_asterisk').style.visibility = 'hidden';
	}
}
function alignTrailDetailsField(){
	isIE= jQuery.browser.msie;
	if(isIE!=true){
		if(screen.height==768){
			$j("#studyPhase").attr("style", "padding-left:5px");
			$j("#trailType").attr("style", "padding-left:5px");
			$j("#draftPurpose").attr("style", "padding-left:5px");
		}
	}
}
</script>
<table>
	<tr>
		<td align="right" width="15%" valign="top" style="padding-top:4px">
			<%=LC.L_Title%>
		</td>
		<td width="4">&nbsp;</td>
		<td align="left" colspan="3">
			<s:textarea name="ctrpDraftJB.studyTitle" id="ctrpDraftJB.studyTitle" 
					style="width:100%;" cssClass="readonly-input"
					readonly="true" cols="100" rows="2" />
		</td>
	</tr>
</table>
<table>
	<tr>
		<td width="50%">
			<table>
				<tr>
					<td align="right" width="30%">
						<%=LC.L_Phase%>
						<span id="ctrpDraftJB.ctrpStudyPhase_error" class="errorMessage" style="display:none;"></span>
					</td>
					<td width="1%" valign="top">
						<FONT id="draft_study_phase_menu_asterisk" style="visibility:visible" class="Mandatory">* </FONT>
					</td>
					<td align="left" valign="top" id="studyPhase" style="padding-left: 0px;">
						<s:property escape="false" value="ctrpDraftJB.draftStudyPhaseMenu" />
					</td>
				</tr>
				<tr id="ctrp_draft_pilot_row" style="visibility:hidden">
					<td align="right" ><%=LC.CTRP_DraftPilotTrial %></td>
					<td valign="top">
						<FONT id="draft_pilot_asterisk" class="Mandatory">* </FONT>
					</td>
					<td valign="top">
						<s:select name="ctrpDraftJB.draftPilot" id="ctrpDraftJB.draftPilot" list="ctrpDraftJB.draftPilotList" 
								listKey="radioCode" listValue="radioDisplay" value="ctrpDraftJB.draftPilot" />
					</td>
				</tr>
				<tr>
					<td align="right" >
						<%=LC.CTRP_DraftTrialType %>
						<span id="ctrpDraftJB.draftStudyType_error" class="errorMessage" style="display:none;"></span>
					</td>
					<td valign="top">
						<FONT id="draft_study_type_asterisk" style="visibility:visible" class="Mandatory">* </FONT>
					</td>
					<td valign="top" id="trailType">
						<s:radio name="ctrpDraftJB.draftStudyType" id="ctrpDraftJB.draftStudyType" list="ctrpDraftJB.draftStudyTypeList" 
								listKey="radioCode" listValue="radioDisplay" value="ctrpDraftJB.draftStudyType" />
					</td>
				</tr>
				<tr>
					<td align="right" >
						<%=LC.CTRP_DraftPurpose %>
						<span id="ctrpDraftJB.studyPurpose_error" class="errorMessage" style="display:none;"></span>
					</td>
					<td valign="top">
						<FONT id="study_purpose_menu_asterisk" style="visibility:visible" class="Mandatory">* </FONT>
					</td>
					<td align="left" valign="top" id="draftPurpose">
						<s:property escape="false" value="ctrpDraftJB.studyPurposeMenu" />
					</td>
				</tr>
				<tr id="ctrp_draft_purpose_other_row" style="visibility:hidden">
					<td align="right">
						<%=LC.CTRP_DraftPurposeOther %>
						<span id="ctrpDraftJB.studyPurposeOther_error" class="errorMessage" style="display:none;"></span>
					</td>
					<td>
						<FONT id="study_purpose_other_asterisk" style="visibility:hidden" class="Mandatory">* </FONT>
					</td>
					<td align="left">
						<s:textarea name="ctrpDraftJB.studyPurposeOther" id="ctrpDraftJB.studyPurposeOther" cols="50" rows="4" />
					</td>
				</tr>
			</table>
		</td>
		<td>
			<table>
				<tr>
					<td width="30%" align="right">
						<%=LC.CTRP_DraftPhaseColon %>
					</td>
					<td width="1%">&nbsp;</td>
					<td>
						<s:textfield name="ctrpDraftJB.studyPhase" cssClass="readonly-input" readonly="true" />
					</td>
				</tr>
				<tr><td colspan="3">&nbsp;</td></tr>
				<tr>
					<td align="right">
						<%=LC.CTRP_DraftTrialTypeColon %>
					</td>
					<td>&nbsp;</td>
					<td>
						<s:textfield name="ctrpDraftJB.studyType" cssClass="readonly-input" readonly="true" />
					</td>
				</tr>
				<tr><td colspan="3">&nbsp;</td></tr>
				<tr><td colspan="3">&nbsp;</td></tr>
				<tr><td colspan="3" height="50">&nbsp;</td></tr>
			</table>
		</td>
	</tr>
</table>