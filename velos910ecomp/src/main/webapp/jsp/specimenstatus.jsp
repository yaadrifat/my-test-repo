<!--

	Project Name:	Velos eResearch

	Author:	Jnanamay Majumdar

	Created on Date:	19Oct2007

	Purpose:	Gui for Specimen Status

	File Name:	specimenstatus.jsp

-->
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Specimen_StatusDets%><%--Specimen Status Details*****--%></title>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,java.math.BigDecimal" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:include page="skinChoser.jsp" flush="true"/>
</head>


<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="whichcss.js"></SCRIPT>

<SCRIPT language="JavaScript">

 function  validate(formobj) {
	//Yogendra Pratap Singh : Modified for Bug#-6674 : Date 02 April 2012
	 var selectedStatus = formobj.specimenStatus.options[formobj.specimenStatus.selectedIndex].text;
	if(selectedStatus=='Moved' || selectedStatus=='Depleted' || selectedStatus=='Shipped'){
		var avlQnty = parseInt(formobj.avlQnty.value);
		var avlQuantity = parseInt(formobj.avlQuantity.value);
		var finalAmount  =parseInt(avlQnty - avlQuantity);
		if(finalAmount < 0){
			alert("<%=MC.M_Status_Neg_Specimen_Quntity%>");
			/*alert("The Quantity should not exceed the specimens current amount.");****/
			return false;
		}
	}
	 if(formobj.specimenstat) {
  	      if (!(validate_col('Status',formobj.specimenStatus))) return false;
	 }

	 if(formobj.statDate) {
	  	   if (!(validate_col('Status Date',formobj.statDate))) return false;
	  	   if (!(validate_date(formobj.statDate))) return false;
	 }

	//KM-#INVP2.5.1
	statusHHVal = formobj.statusHH.value;
	statusMMVal = formobj.statusMM.value;
	statusSSVal = formobj.statusSS.value;

	if (!validateDataRange(statusHHVal,'>=',0,'and','<',24)  || !isInteger(statusHHVal))
	{

	    alert("<%=MC.M_Vldt_NumAsPositive%>");/*alert("Please enter a positive number below 24");*****/
		formobj.statusHH.focus();
		return false;
	}

	if (!validateDataRange(statusMMVal,'>=',0,'and','<',60) || !isInteger(statusMMVal) )
	{

	    alert("<%=MC.M_PlsEtrPtv_NumBlw60%>");/*alert("Please enter a positive number below 60");*****/
		formobj.statusMM.focus();
		return false;
	}

	if (!validateDataRange(statusSSVal,'>=',0,'and','<',60) || !isInteger(statusSSVal) )
	{

	    alert("<%=MC.M_PlsEtrPtv_NumBlw60%>");/*alert("Please enter a positive number below 60");*****/
		formobj.statusSS.focus();
		return false;
	}


	/** JM: 03Jun2009 ****************************************/



	if (!(validate_date(formobj.handOffDate))) return false	;
	if (!(validate_date(formobj.procDate))) return false	;

	if (!validateDataRange(formobj.handOffHH.value,'>=',0,'and','<',24) || !isInteger(formobj.handOffHH.value))
	{

	    alert("<%=MC.M_Vldt_NumAsPositive%>");/*alert("Please enter a positive number below 24");*****/
		formobj.handOffHH.focus();
		return false;
	}

	if (!validateDataRange(formobj.handOffMM.value,'>=',0,'and','<',60) || !isInteger(formobj.handOffMM.value) )
	{

	    alert("<%=MC.M_PlsEtrPtv_NumBlw60%>");/*alert("Please enter a positive number below 60");*****/
		formobj.handOffMM.focus();
		return false;
	}

  	if (!validateDataRange(formobj.handOffSS.value,'>=',0,'and','<',60) || !isInteger(formobj.handOffSS.value) )
	{
	   alert("<%=MC.M_PlsEtrPtv_NumBlw60%>");/*alert("Please enter a positive number below 60");*****/
		formobj.handOffSS.focus();
		return false;
	}

	if (!validateDataRange(formobj.procHH.value,'>=',0,'and','<',24) || !isInteger(formobj.procHH.value))
	{

	    alert("<%=MC.M_Vldt_NumAsPositive%>");/*alert("Please enter a positive number below 24");*****/
		formobj.procHH.focus();
		return false;
	}

	if (!validateDataRange(formobj.procMM.value,'>=',0,'and','<',60)   || !isInteger(formobj.procMM.value) )
	{
	    alert("<%=MC.M_PlsEtrPtv_NumBlw60%>");/*alert("Please enter a positive number below 60");*****/
		formobj.procMM.focus();
		return false;
	}

  	if (!validateDataRange(formobj.procSS.value,'>=',0,'and','<',60) || !isInteger(formobj.procSS.value))
	{

	    alert("<%=MC.M_PlsEtrPtv_NumBlw60%>");/*alert("Please enter a positive number below 60");*****/
		formobj.procSS.focus();
		return false;
	}


	if ((formobj.procHH.value != '' || formobj.procMM.value != '' || formobj.procSS.value != '') && formobj.procDate.value == ''){
	if ((formobj.procHH.value != '00' || formobj.procMM.value != '00' || formobj.procSS.value != '00') && formobj.procDate.value == ''){
		alert("<%=MC.M_Etr_ProcessingDt%>");/*alert("Please enter the Processing Date");*****/
		formobj.procDate.focus();
		return false;
	}}

	if ((formobj.handOffHH.value != '' || formobj.handOffMM.value != '' || formobj.handOffSS.value != '') && formobj.handOffDate.value == ''){
	if ((formobj.handOffHH.value != '00' || formobj.handOffMM.value != '00' || formobj.handOffSS.value != '00') && formobj.handOffDate.value == ''){
		alert("<%=MC.M_Etr_HandOffDt%>");/*alert("Please enter the Hand-Off Date");*****/
		formobj.handOffDate.focus();
		return false;
	}}


	/******************************************************/

	 if (formobj.avlQuantity.value != '' && formobj.specQtyUnitEdit.value == ''){
    	alert("<%=MC.M_Selc_QtyUnit%>");/*alert("please select the Quantity Unit");*****/
    	return false;
     }

     if (formobj.specQtyUnitEdit.value!= undefined){
	     if (formobj.avlQuantity.value == '' && formobj.specQtyUnitEdit.value !='' && formobj.mode.value=='N'){
	    	alert("<%=MC.M_PlsEnterQty%>");/*alert("please enter Quantity");*****/
	    	formobj.avlQuantity.focus();
	    	return false;
	     }
     }else{
    	 if (formobj.avlQuantity.value != ''){
   	    	alert("<%=MC.M_Selc_QtyUnit%>");/*alert("please select the Quantity Unit");*****/
   	    	return false;
   	     }
     }

//JM: 04Sep2009: #4204
	if (formobj.avlQuantity.value < 0 && formobj.mode.value =='N'){
	    alert("<%=MC.M_PositiveNum_ForQty%>");/*alert("please enter a positive number for the Quantity");*****/
	    return false;
    }

     if(isNaN(formobj.avlQuantity.value) == true){
	 	alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a Valid Number");*****/
		formobj.avlQuantity.focus();
		return false;
	 }

	 if (formobj.avlQuantity.value != '' && formobj.specQtyUnitEdit.value ==''){
		var pattern = /[1|2|3|4|5|6|7|8|9]/g;
		var result = formobj.avlQuantity.value.match(pattern);
		if (result != null && result.length > 0) {
    	    alert("<%=MC.M_Selc_QtyUnit%>");/*alert("please select the Quantity Unit");*****/
        	return false;
		}
	 }


     if (!(validate_col('e-Signature',formobj.eSign))) return false




     if(isNaN(formobj.eSign.value) == true) {

		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/

		formobj.eSign.focus();

		return false;

	   }

	if (formobj.specQtyUnitEdit.value !=''){
		if (!(validate_col('Quantity',formobj.avlQuantity))) return false
	}

//JM: 11Jan2010: #4616
	if (formobj.avlQuantity.value != '' && formobj.avlQuantity.value > 0 && formobj.mode.value=='N'){
		alert("<%=MC.M_StatQtyCurAmt_ReCalcu%>");/*alert("This Status Quantity may affect the Current Amount of specimen. To see the latest Current Amount please click on the 'Re-Calculate' link.");*****/
	}



 }



function openUserWindowSpecBy(frm) {

		windowName = window.open("usersearchdetails.jsp?fname=&lname=&from="+ frm,"TEST","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
		windowName.focus();
}


function openUserWindow(frm) {

		windowName = window.open("usersearchdetails.jsp?fname=&lname=&from="+ frm,"TEST","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
		windowName.focus();
}
function openStudyWindow(formobj) {
	formobj.target="Lookup";
	formobj.method="post";
	formobj.action="multilookup.jsp?viewId=6013&form=specimenstat&seperator=,"+
                  "&keyword=selStudy|STUDY_NUMBER~selStudyIds|LKP_PK|[VELHIDE]&maxselect=1";

	formWin =open('donotdelete.html','Lookup','resizable=1,status=0, width=850,height=550 top=100,left=100,menubar=no,scrollbars=1');

	if (formWin && !formWin.closed) formWin.focus();
	formobj.submit();
	formobj.target="_self";
	formobj.action="updatespcimenstatus.jsp";
	void(0);
}




function setCurrentTime(formobj, link)
{

  var currentTime = new Date();
  var hours = currentTime.getHours();
  var minutes = currentTime.getMinutes();
  var seconds = currentTime.getSeconds();

  if(link==1){

	  formobj.statusHH.value = hours;
	  formobj.statusMM.value = minutes;
	  formobj.statusSS.value = seconds;

  }else if(link==2){

	  formobj.handOffHH.value = hours;
	  formobj.handOffMM.value = minutes;
	  formobj.handOffSS.value = seconds;

  }else if(link==3){

	  formobj.procHH.value = hours;
	  formobj.procMM.value = minutes;
	  formobj.procSS.value = seconds;

  }


}

function resetCurrentTime(formobj, link){
	if (link == 1){

	  formobj.statusHH.value = "00";
	  formobj.statusMM.value = "00";
	  formobj.statusSS.value = "00";


	}else if (link == 2){

 	  formobj.handOffHH.value = "00";
	  formobj.handOffMM.value = "00";
	  formobj.handOffSS.value = "00";


	}else if (link == 3){

  	  formobj.procHH.value = "00";
	  formobj.procMM.value = "00";
	  formobj.procSS.value = "00";
	}
}

</SCRIPT>



<%@ page language = "java" import = "com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.*,com.velos.eres.service.util.StringUtil,com.velos.eres.business.common.*,com.velos.eres.web.user.*,com.velos.esch.business.common.EventAssocDao"%>

<jsp:useBean id="SpecimenStatJB" scope="request" class="com.velos.eres.web.specimenStatus.SpecimenStatusJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="stdJB" scope="request" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id="userJB" scope="request" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id="usrJB" scope="request" class="com.velos.eres.web.user.UserJB"/>
<jsp:include page="include.jsp" flush="true"/>

<% String src;
String from = "status";
src= request.getParameter("srcmenu");

%>


<body>
<br>
<DIV class="tableDefault" id="div1">

<%

   HttpSession tSession = request.getSession(true);
   if (sessionmaint.isValidSession(tSession))
	{

	 String defUserGroup = (String) tSession.getAttribute("defUserGroup");

	String userIdFromSession = (String) tSession.getValue("userId");
	UserJB user = (UserJB) tSession.getValue("currentUser");
	String userId = (String) tSession.getValue("userId");

	String uName = (String) tSession.getValue("userName");
	String acc = (String) tSession.getValue("accountId");


	//pk of the Specimen
	String fkSpecimen = request.getParameter("pkSpecimen");
	fkSpecimen = (fkSpecimen==null)? "" :fkSpecimen;


	//Specimen Status PK
	String pkSpecStat = request.getParameter("pkSpecimenStatus");
	pkSpecStat = (pkSpecStat==null)?"":pkSpecStat;


	String mode =  request.getParameter("mode");

		if (mode==null) mode="N";

	//Yogendra Pratap Singh : Modified for Bug#-6674 : Date 02 April 2012
	String avlQnty = request.getParameter("avlQnty");
	avlQnty = (avlQnty==null)? "" :avlQnty;



String selStat = request.getParameter("specimenStatus");
selStat = (selStat==null)?"":selStat;



	String dSpecimenStatus = "";
 int defaultStat = 0;

 CodeDao cd3 = new CodeDao();

 //JM: 01Jun2009,#INV 2.29(5)
 String openFrom = request.getParameter("openFrom");
 openFrom = (openFrom==null)?"":openFrom;

 if (openFrom.equals("process")) {
 	cd3.getCodeValuesFilterCustom1("specimen_stat","process");
 }else{
	 cd3.getCodeValuesFilterCustom1("specimen_stat","");
 }

 cd3.setCType("specimen_stat");
 cd3.setForGroup(defUserGroup);

 CodeDao cd5 =new CodeDao();


//JM: 03Jun2009, #INVP2.29 find the processing type

	String subtypeVal = "";
	String code_subtype = "", custom_col1_val="";
	String code_subtype_subStr = "";

	//if User selected an item from the drop down...
	subtypeVal  = request.getParameter("subtype");
 	subtypeVal = (subtypeVal==null)?"":subtypeVal;


 	//codelst_type is 'specimen_stat'
 	CodeDao cdStat = new CodeDao();

//JM: 01Jun2009,#INV 2.29(5)
 String processLink = request.getParameter("processLink");
 processLink = (processLink==null)?"":processLink;

 if (mode.equals("N")) {

	 if (processLink.equals("ProcLink")) {
	 	defaultStat = cd5.getCodeId("specimen_stat","Processed");
	 	custom_col1_val ="process";
	 }else{
	 	defaultStat = cd5.getCodeId("specimen_stat","Collected");
	 	custom_col1_val="";
	 }
 }

	if(! StringUtil.isEmpty(subtypeVal)){
		dSpecimenStatus=cd3.toPullDown("specimenStatus",EJBUtil.stringToNum(subtypeVal));
	}else{
		dSpecimenStatus=cd3.toPullDown("specimenStatus",defaultStat);
	}

	String trackNo = "";
	String specStat = "";
 	String  startDt = "";
	String  studyId = "", studyNumber = "";
	String statByUserId = "", statByUsrName ="";
	String usrId = "", usrName ="";
	String statNotes = "";

	String  specQuUnit = "";
	String  avQty = "";

	StringBuffer stStatbuffer = new StringBuffer();

	String statusHH ="00";
	String statusMM = "00";
	String statusSS = "00";
	String stTime ="";

	String handOffHH ="00";
	String handOffMM = "00";
	String handOffSS = "00";


	String procHH ="00";
	String procMM = "00";
	String procSS = "00";


 	 if (mode.equals("M")) {

 		 SpecimenStatJB.setPkSpecimenStat(EJBUtil.stringToNum(pkSpecStat));
		SpecimenStatJB.getSpecimenStatusDetails();

		specStat = SpecimenStatJB.getFkCodelstSpecimenStat();
		dSpecimenStatus=cd3.toPullDown("specimenStatus",EJBUtil.stringToNum(specStat));

		specQuUnit  = SpecimenStatJB.getSsQuantityUnits();
		specQuUnit=(specQuUnit==null)?"":specQuUnit;


		avQty  = SpecimenStatJB.getSsQuantity();
		avQty=(avQty==null)?"":avQty;
		/*try {
		    BigDecimal bdAvQty = new BigDecimal(avQty);
		    if (BigDecimal.ZERO.compareTo(bdAvQty) == 0) avQty = "0";
		} catch (Exception e) {}*/

		startDt  = SpecimenStatJB.getSsDate();
		startDt=(startDt==null)?"":startDt;

		if(! StringUtil.isEmpty(startDt)) {
			stTime = startDt.substring(startDt.indexOf(" ")+1,startDt.length());
			startDt = startDt.substring(0,startDt.indexOf(" "));
		}

		String arrTime[] = null;
		if(! StringUtil.isEmpty(stTime)) {
			arrTime = stTime.split(":");
			statusHH = arrTime[0];
			statusMM = arrTime[1];
			statusSS = arrTime[2];
		}

		trackNo  = SpecimenStatJB.getSsTrackingNum();
		trackNo=(trackNo==null)?"":trackNo;


		studyId =   SpecimenStatJB.getFkStudy();
		studyId=(studyId==null)?"":studyId;


		//get the study number to be displyed
		stdJB.setId(EJBUtil.stringToNum(studyId));
		stdJB.getStudyDetails();
		studyNumber = stdJB.getStudyNumber();
		studyNumber=(studyNumber==null)?"":studyNumber;


		//Status By
		statByUserId = SpecimenStatJB.getSsStatusBy();
		statByUserId=(statByUserId==null)?"":statByUserId;

		//Recipient
		usrId = SpecimenStatJB.getFkUserRecpt();
		usrId=(usrId==null)?"":usrId;



		statNotes = SpecimenStatJB.getSsNote();

	 }


//JM: 13Jul2009: #4098
	if (mode.equals("M")) {


		if(! StringUtil.isEmpty(specStat)){
			custom_col1_val = cdStat.getCodeCustomCol1(EJBUtil.stringToNum(specStat));
		}

	}else{

		if(! StringUtil.isEmpty(subtypeVal)){
			custom_col1_val = cdStat.getCodeCustomCol1(EJBUtil.stringToNum(subtypeVal));
		}

	}

	custom_col1_val = (custom_col1_val==null)?"":custom_col1_val;



	 	int len = dSpecimenStatus.length();
		int strIndex = dSpecimenStatus.indexOf(">");

		String strSel =  dSpecimenStatus.substring(0, strIndex);
		if (mode.equals("N")){
		 strSel = strSel + " "+ ">";
		 //strSel = strSel + " "+ "onchange='OnChange(document.specimenstat.specimenStatus);'";
		}else{
		 strSel = strSel + " "+ "disabled>";
		}
		stStatbuffer.append(strSel);

		String strApp = dSpecimenStatus.substring(strIndex+1, len);

		stStatbuffer.append(strApp);
		dSpecimenStatus = stStatbuffer.toString();



		if (mode.equals("N")){

		statByUserId = userIdFromSession;
		}


		//get the user name to be displayed	under Status By
		userJB.setUserId(EJBUtil.stringToNum(statByUserId));
		userJB.getUserDetails();
		String fNameStatBy=userJB.getUserFirstName();
			fNameStatBy=(fNameStatBy==null)?"":fNameStatBy;
		String lNameStatBy=userJB.getUserLastName();
			lNameStatBy=(lNameStatBy==null)?"":lNameStatBy;
		statByUsrName =  fNameStatBy + " " + lNameStatBy ;


		//get the user name to be displayed	under Recipient
		usrJB.setUserId(EJBUtil.stringToNum(usrId));
		usrJB.getUserDetails();
		String fName=usrJB.getUserFirstName();
			fName=(fName==null)?"":fName;
		String lName=usrJB.getUserLastName();
			lName=(lName==null)?"":lName;
		usrName =  fName + " " + lName ;




	CodeDao cdSpecQuantUnit = new CodeDao();
	cdSpecQuantUnit.getCodeValues("spec_q_unit");
	String dSpecQuantUnit = "";


	//String specQntUnit = request.getParameter("specQuantityUnit");
	String specQntUnit = request.getParameter("specQtyUnit");

	if (specQntUnit ==null) specQntUnit ="";

	if (mode.equals("M")) {
		specQntUnit = specQuUnit;

	}
	//In any mode (N/M) the dropdown should be disabled as the unit will be same as specimendetails page
	String ddProp =" DISABLED ";



	if (specQntUnit.equals("")){
 	   dSpecQuantUnit=cdSpecQuantUnit.toPullDown("specQuantityUnit",-1,ddProp);
	}
	else{
       dSpecQuantUnit=cdSpecQuantUnit.toPullDown("specQuantityUnit",EJBUtil.stringToNum(specQntUnit),ddProp);
	}

	//Processing type
	CodeDao cdProcTyp = new CodeDao();
	cdProcTyp.getCodeValues("spec_proctype");


	//From the existing..
	String procType  = SpecimenStatJB.getSsProcType();
	procType=(procType==null)?"":procType;


	//from the request..
	String pProcTyp =	request.getParameter("pProcType");
	if (pProcTyp ==null) pProcTyp ="";

	if (mode.equals("M")) {
		pProcTyp = procType;
	}

	String dpProcType= "";


	if (pProcTyp.equals("")){
 	   dpProcType=cdProcTyp.toPullDown("pProcType");
	}
	else{
       dpProcType=cdProcTyp.toPullDown("pProcType",EJBUtil.stringToNum(pProcTyp),true);
	}




		String handOffTime = "";
		String procTime = "";

		String handOffDt  = SpecimenStatJB.getSsHandOffDate();
		handOffDt=(handOffDt==null)?"":handOffDt;

		if(! StringUtil.isEmpty(handOffDt)) {
			handOffTime = handOffDt.substring(handOffDt.indexOf(" ")+1,handOffDt.length());
			handOffDt = handOffDt.substring(0,handOffDt.indexOf(" "));
		}

		String arrhOffTime[] = null;
		if(! StringUtil.isEmpty(handOffTime)) {
			arrhOffTime = handOffTime.split(":");
			handOffHH = arrhOffTime[0];
			handOffMM = arrhOffTime[1];
			handOffSS = arrhOffTime[2];
		}




		String ssProcDt  = SpecimenStatJB.getSsProcDate();
		ssProcDt=(ssProcDt==null)?"":ssProcDt;

		if(! StringUtil.isEmpty(ssProcDt)) {
			procTime = ssProcDt.substring(ssProcDt.indexOf(" ")+1,ssProcDt.length());
			ssProcDt = ssProcDt.substring(0,ssProcDt.indexOf(" "));
		}

		String arrProcTime[] = null;
		if(! StringUtil.isEmpty(procTime)) {
			arrProcTime = procTime.split(":");
			procHH = arrProcTime[0];
			procMM = arrProcTime[1];
			procSS = arrProcTime[2];
		}


%>





<Form name="specimenstat" id="specimenstatfrm" method="post" action="updatespcimenstatus.jsp" onSubmit ="if (validate(document.specimenstat)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">


<P class = "sectionheadings">
<%=MC.M_Etr_SpmenStatDet%><%--Please enter specimen status details*****--%>:
</P>


<input type="hidden" name="srcmenu" Value="<%=src%>">
<input type="hidden" name="pkspecimen" Value="<%=fkSpecimen%>">
<input type="hidden" name="pkSpecimenStat" Value="<%=pkSpecStat%>">
<input type="hidden" name="mode" Value="<%=mode%>">
<input type="hidden" name="specStatDisabled" Value="<%=specStat%>">

<input type="hidden" name="specQtyUnitEdit" Value="<%=specQntUnit%>">

<input type="hidden" name = "specQuantityUnit" value="<%=specQntUnit%>"><!--JM: 02DEC2009: #4509-->

<input type="hidden" name="avlQnty" value="<%=avlQnty%>"/>




<table width="100%" cellspacing="0" cellpadding="0" class="basetbl">
<tr>
<td> <%=LC.L_Tracking_Number%><%--Tracking Number*****--%></td>

<td>
<input type ="text" name="trackingNo" value = "<%=trackNo%>"  size= 25 MAXLENGTH = 50>
</td>

</tr>

<tr>
<td><%=LC.L_Status%><%--Status*****--%><FONT class="Mandatory">* </FONT></td>
<td width="30%"><%=dSpecimenStatus%></td>

<input type="hidden" name="subtype" value="<%=subtypeVal%>" maxlength=10 size=10>
<%-- INF-20084 Datepicker-- AGodara --%>
<td><%=MC.M_Status_DateTime%><%--Status Date and Time*****--%> <FONT class="Mandatory">* </FONT></td>
<td class=tdDefault><Input type = "text" name="statDate"  value="<%=startDt%>"  size=10 MAXLENGTH =20 class="datefield" >
<input type="text" name="statusHH" value="<%=statusHH%>" maxlength=2 size=1>:<input type="text" name="statusMM" value="<%=statusMM%>" maxlength=2 size=1>:<input type="text" name="statusSS" value="<%=statusSS%>" maxlength=2 size =1>
<a href=# onclick="setCurrentTime(document.specimenstat, 1)"><%=LC.L_Cur_Time%><%--Current Time*****--%> </a>&nbsp;
<a href="#" onclick="return resetCurrentTime(document.specimenstat, 1)"><%=LC.L_Reset%><%--Reset*****--%></a>
<td>

</tr>
<% if (processLink.equals("ProcLink") ||  custom_col1_val.equals("process")) {%>
<tr><td>&nbsp;</td></tr>
<tr>
<td><%=LC.L_Processing_Type%><%--Processing Type*****--%></td>
<td><%=dpProcType%></td>
</tr>
<div id="processStat" name="processStat"  style="display:none">
<tr>
<td><%=MC.M_Handoff_DateTime%><%--Hand-Off date and time*****--%></td>
<%-- INF-20084 Datepicker-- AGodara --%>
<td class=tdDefault><Input type = "text" name="handOffDate"  value="<%=handOffDt%>" size=10 MAXLENGTH =20 class="datefield" >
<input type="text" name="handOffHH" value="<%=handOffHH%>" maxlength=2 size=1>:
<input type="text" name="handOffMM" value="<%=handOffMM%>" maxlength=2 size=1>:
<input type="text" name="handOffSS" value="<%=handOffSS%>" maxlength=2 size =1>
<a href=# onclick="setCurrentTime(document.specimenstat, 2)"><%=LC.L_Cur_Time%><%--Current Time*****--%> </a>&nbsp;
<a href="#" onclick="return resetCurrentTime(document.specimenstat,2)"><%=LC.L_Reset%><%--Reset*****--%></a>
<td>

</tr>
<tr>

<td><%=MC.M_Processing_DateTime%><%--Processing Date and Time*****--%></td>
<%-- INF-20084 Datepicker-- AGodara --%>
<td class=tdDefault><Input type = "text" name="procDate"  value="<%=ssProcDt%>" size=10 MAXLENGTH =20 class="datefield" >
<input type="text" name="procHH" value="<%=procHH%>" maxlength=2 size=1>:
<input type="text" name="procMM" value="<%=procMM%>" maxlength=2 size=1>:
<input type="text" name="procSS" value="<%=procSS%>" maxlength=2 size =1>
<a href=# onclick="setCurrentTime(document.specimenstat, 3)"><%=LC.L_Cur_Time%><%--Current Time*****--%> </a>&nbsp;
<a href="#" onclick="return resetCurrentTime(document.specimenstat, 3)"><%=LC.L_Reset%><%--Reset*****--%></a>
<td>

</tr>
</div>
<tr><td>&nbsp;</td></tr>
<%}else{%>
<Input type="hidden" name="handOffDate"  value="">
<input type="hidden" name="handOffHH" value="">
<input type="hidden" name="handOffMM" value="">
<input type="hidden" name="handOffSS" value="">

<Input type="hidden" name="procDate"  value="">
<input type="hidden" name="procHH" value="">
<input type="hidden" name="procMM" value="">
<input type="hidden" name="procSS" value="">

<%}%>

<tr>

<td><%=LC.L_Status_By%><%--Status By*****--%></td>
<input type="hidden" name="specCreatorId" value="<%=statByUserId%>">
<td class=tdDefault >
<Input type = "text" name="specCreatedBy"    value="<%=statByUsrName%>"  size=25 align = right readonly>
<A href="#" onClick="return openUserWindowSpecBy('specimenstatBy')"><%=LC.L_Select_User%><%--Select User*****--%></A>
</td>


<td><%=LC.L_Recipient%><%--Recipient*****--%></td>
<input type="hidden" name="creatorId" value="<%=usrId%>">
<td class=tdDefault >
				<Input type = "text" name="createdBy"    value="<%=usrName%>"  size=25 align = right readonly>
				<A href="#" onClick="return openUserWindow('specimenstat')"><%=LC.L_Select_User%><%--Select User*****--%></A></td>




</tr>

<tr>
<td><%=LC.L_For_Study%><%--For <%=LC.Std_Study%>*****--%></td>

<input type="hidden" name="selStudyIds" value="<%=studyId%>">
				<td class=tdDefault width="30%"><Input type = "text" name="selStudy"  value="<%=studyNumber%>" size=25 readonly >

				<A href="#" onClick="openStudyWindow(document.specimenstat)" ><%=LC.L_Select_Study%><%--Select <%=LC.Std_Study%>*****--%></A>
				</td>




				<td class=tdDefault width="25%">
				<%=LC.L_Quantity%><%--Quantity*****--%></td>
			<%
			if (mode.equals("N")) {
			%>
				<td class=tdDefault >
				<Input type = "text" name="avlQuantity"   value="<%=avQty%>" size=15 MAXLENGTH = 20 align = right>
				<%=dSpecQuantUnit%>
				</td>
			<%}else{%>
				<td class=tdDefault >
				<Input type = "text" name="avlQuantity" READONLY  value="<%=avQty%>" size=15 MAXLENGTH = 20 align = right>
				<%=dSpecQuantUnit%>
				</td>
			<%}%>
</tr>


<tr>
<td><%=LC.L_Status_Notes%><%--Status Notes*****--%></td><td><textarea name="statusNotes" rows=4 cols=35 MAXLENGTH = 20000><%=statNotes%></textarea></td>


</tr>

</table>


<br>

<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="specimenstatfrm"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>

</Form>

<%

}//end of if body for session

else

{

%>

<jsp:include page="timeout.html" flush="true"/>

<%

}

%>

<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>

</div>

<DIV class="mainMenu" id = "emenu">

  <jsp:include page="getmenu.jsp" flush="true"/>

</DIV>

</body>

</html>



