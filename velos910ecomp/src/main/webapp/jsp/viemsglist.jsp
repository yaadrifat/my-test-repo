<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>



<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


<script>

function openwindow(msg)
{
	    windowName=window.open("viemsg.jsp?message="+msg,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=400,height=200 top=100,left=100 0,");
}
	function setOrder(formObj,orderBy,pgRight) //orderBy column number
{
	var lorderType;
	if (formObj.orderType.value=="asc") {
		formObj.orderType.value= "desc";
		lorderType="desc";
	} else 	if (formObj.orderType.value=="desc") {
		formObj.orderType.value= "asc";
		lorderType="asc";
	}

	lsrc = formObj.srcmenu.value;
	lcurPage = formObj.page.value;
	lselectedtab= formObj.selectedTab.value;
	formObj.action="allPatient.jsp?srcmenu="+lsrc+"&page="+lcurPage+"&orderBy="+orderBy+"&orderType="+lorderType+"&selectedTab="+lselectedtab;
	formObj.submit();
}
 function setFilterText(form){
 	  form.ptxt.value=form.patCode.value;
 	  form.atxt.value=form.patage[form.patage.selectedIndex].text ;
 	  if (form.patgender[form.patgender.selectedIndex].value != "")
 	       form.gtxt.value=form.patgender[form.patgender.selectedIndex].text ;
 	  if (form.patstatus[form.patstatus.selectedIndex].value != "")
 	   form.rtxt.value=form.patstatus[form.patstatus.selectedIndex].text ;
 	  form.rbytxt.value=form.regBy.value ;

 }
  function openwindow1()
{
	    windowName=window.open("viesettings.jsp?selectedTab=1","Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=400,height=500 top=100,left=100 0,");
}
  function openwindow2(msg)
{
	    windowName=window.open("vieerror.jsp?message="+msg,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=400,height=250 top=100,left=100 0,");
}

	</script>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>

<title><%=LC.L_Search_Pats%><%--Search <%=LC.Pat_Patients%>*****--%></title>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id ="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.DateUtil,com.velos.eres.business.common.*" %>
<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.person.*,com.velos.eres.service.util.*,com.velos.eres.web.user.*,com.velos.eres.web.grpRights.GrpRightsJB"%>

<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="codelst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB"/>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>


<%  HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))
	{

 String src;
src= request.getParameter("srcmenu");
%>


<%
int ienet = 2;

String agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1)
     ienet = 0; //IE
    else
	ienet = 1;
	if(ienet == 0) {
%>
<body style="overflow:scroll;">
<%
	} else {
%>
<body>
<%
	}
%>
<jsp:include page="viepanel.jsp" flush="true"/>
<br><div id="overDiv" style="position:absolute;visibility:hidden; z-index:1000;"></div>
<DIV >
<%

   	     String selectedTab = "" ;
   	     String filter="";
   	     String dateselect="", datespec="", fromdate="",todate="";


String filterdate=request.getParameter("filterdate");
String clientName=request.getParameter("clientname");
String status_msg=request.getParameter("status");
if (status_msg.equals("All")) filter="";
if (status_msg.equals("0")) filter=" and status='0' " ;
if (status_msg.equals("-9")) filter=" and status='-9' " ;
if (status_msg.equals("1")) filter=" and status='1' " ;
if (status_msg.equals("-1")) filter=" and status='-1' " ;
if (status_msg.equals("-2")) filter=" and status='-2' " ;




String mode=request.getParameter("mode");

String sql =" select pk_message,to_char(SUBSTR(message,0,80)) msg,(message)as message,msg_type,event,status,to_char(msg_date,'mm/dd/yyyy hh24:mi') msg_date from vie_message "+
			"where trunc(msg_date) =(to_date('" + filterdate + "','mm/dd/yyyy') ) and sending_app='" + clientName+"'" + filter  ;



String countsql="select count(*) from (select pk_message, SUBSTR(message,0,80) as msg,(message)as message,msg_type,event,status,to_char(msg_date,'mm/dd/yyyy hh24:mi') msg_date from vie_message "+
			"where trunc(msg_date) =(to_date('" + filterdate + "','mm/dd/yyyy') ) and sending_app='" + clientName +"'" + filter +")"  ;
System.out.println(sql);
System.out.println(countsql);

			String pagenum = "";
			int curPage = 0;
			long startPage = 1;
			long cntr = 0;
			String pagenumView = "";
			int curPageView = 0;
			long startPageView = 1;
			long cntrView = 0;

			pagenum = request.getParameter("page");
			if (pagenum == null)
			{
			pagenum = "1";
			}
			curPage = EJBUtil.stringToNum(pagenum);

			String orderBy = "";
			orderBy = request.getParameter("orderBy");

			String orderType = "";
			orderType = request.getParameter("orderType");

			if (orderType == null)
			{
			orderType = "asc";
			}


			pagenumView = request.getParameter("pageView");
			if (pagenumView == null)
			{
			pagenumView = "1";
			}
			curPageView = EJBUtil.stringToNum(pagenumView);

			String orderByView = "";
			orderByView = request.getParameter("orderByView");

			String orderTypeView = "";
			orderTypeView = request.getParameter("orderTypeView");

			if (orderTypeView == null)
			{
			orderTypeView = "asc";
			}

			long rowsPerPage=0;
			long totalPages=0;
			long rowsReturned = 0;
			long showPages = 0;
			long totalRows = 0;
			long firstRec = 0;
			long lastRec = 0;
			boolean hasMore = false;
			boolean hasPrevious = false;

			Configuration conf = new Configuration();
			rowsPerPage = conf.getRowsPerBrowserPage(conf.ERES_HOME +"eresearch.xml");
			totalPages = conf.getTotalPagesPerBrowser(conf.ERES_HOME +"eresearch.xml");

			BrowserRows br = new BrowserRows();


            br.getPageRows(curPage,rowsPerPage,sql,totalPages,countsql,orderBy,orderType);
			rowsReturned = br.getRowReturned();
			showPages = br.getShowPages();

		     startPage = br.getStartPage();

			hasMore = br.getHasMore();

			hasPrevious = br.getHasPrevious();


			totalRows = br.getTotalRows();


			firstRec = br.getFirstRec();

			lastRec = br.getLastRec();


%>

  <Form name="messgaes" method="post" action="viemsglist.jsp?page=1&mode=M">

    <br>


<!--<Form  name="search" method="post" action="allPatient.jsp">-->
<input type="hidden" name="srcmenu" Value="<%=src%>">
<input type="hidden" name="searchFrom" Value="search">
<input type="hidden" name="clientname" Value="<%=clientName%>">
<input type="hidden" name="filterdate" Value="<%=filterdate%>">

<span class="pagebanner">
  <table width="100%" cellspacing="0" cellpadding="0" border=0 class="displaytag">


	<tr>
	<td width="10%" class="tdDefault"><%=LC.L_Status%><%--Status*****--%></td>
<td width="10%"><select size="1" name="status">
<option value="All" selected><%=LC.L_All%><%--All*****--%></option>
<option value="1"><%=LC.L_Success%><%--Success*****--%></option>
<option value="-1"><%=LC.L_Failure%><%--Failure*****--%></option>
<option value="0"><%=LC.L_Unprocessed%><%--Unprocessed*****--%></option>
<option value="-9"><%=LC.L_Unsupported%><%--Unsupported*****--%></option>
<option value="-2"><%=LC.L_Warning%><%--Warning*****--%></option>
</select></td>
 	 	<td class=tdDefault width=10%> <button type="submit"><%=LC.L_Search%><%--<%=LC.L_Search%>*****--%></button> </td>
	    <td><A type="submit" href="viemsgbrowse.jsp?mode=initial&page=1"><%=LC.L_Back%></A></td>
		</tr>
  </table></span>

<!--</Form>-->

<input type="hidden" name="selectedTab" Value=<%=selectedTab%>>
<Input type="hidden" name="orderBy" value="<%=orderBy%>">
<Input type="hidden" name="page" value="<%=curPage%>">
<Input type="hidden" name="orderType" value="<%=orderType%>">

<%
//Retrieve study count for this user
/*	PatStudyStatDao pstudydao = new PatStudyStatDao();
	pstudydao = studyB.getStudyPatientResults(EJBUtil.stringToNum(""), "", EJBUtil.stringToNum("") ,EJBUtil.stringToNum(userId), EJBUtil.stringToNum(siteId) );
     ArrayList patientCodes=   pstudydao.getPatientCodes();
     int patcodes= patientCodes.size() ;



	//end retrieve study count*/


/*	PatientDao pdao = new PatientDao();

	pdao = person.getPatients(EJBUtil.stringToNum(siteId),patCode,418);*/

	ArrayList studyList=new ArrayList();


//	patientResults = pdao.getPatient();

	if (totalRows <= 0 ) {

%>

	<P class="defComments"><%=MC.M_NoMsg_SrchCrit%><%--No messages found for your search criteria. Please modify the criteria and click on ''Go''*****--%></P>

<%

	} else {

	   int i = 0;

	   //int lenPatient = rowsReturned ;

	   //lenPatient = patientResults.size();

	   if(rowsReturned == 0) {

%>

	<P class="defComments"><%=MC.M_NoMsg_SrchCrit%><%--No messages found for your search criteria. Please modify the criteria and click on ''Go''*****--%></P>

<%

	} else {

//	   PersonStateKeeper psk = null;








	%>





    <table width="100%" border=0 class="displaytag">
 <tr>
	<th class=tdDefault width=15%><%=LC.L_Date%><%--Date*****--%></th>

   	<th class=tdDefault width=60%><%=LC.L_Message%><%--Message*****--%></th>

    <th class=tdDefault width=10%><%=LC.L_Msg_Type%><%--Message Type*****--%></th>

	<th class=tdDefault width=5%><%=LC.L_Event%><%--Event*****--%></th>

  	<th class=tdDefault width=10%><%=LC.L_Status%><%--Status*****--%></th>






   </tr>

<%



        String tempPercode="" ;
        String creator="" ;
		String msgId = "" ;
		String msgDate="";
		String message = "" ,msgtype="",event="",status="",sending_app="",fullmsg="";


		for(i = 1 ; i <=rowsReturned ; i++)

	  	{
	  	    msgDate= br.getBValues(i,"msg_date")	 		   ;
	  	    message = br.getBValues(i,"msg")	 		   ;
	  	    msgId=br.getBValues(i,"pk_message")	 		   ;
	  	    event = br.getBValues(i,"event")	 		   ;
	  	    msgtype=br.getBValues(i,"msg_type")	 		   ;
            status=  br.getBValues(i,"status")	 		   ;
            if (status.equals("0"))  	status=LC.L_Unprocessed/*Unprocessed*****/ ;
			if (status.equals("-9"))  	status=LC.L_Unsupported/*Unsupported*****/ ;
			if (status.equals("1"))  	status=LC.L_Success/*Success*****/ ;
			if (status.equals("-1"))  	status=LC.L_Failure/*Failure*****/+"<A href=# onClick=openwindow2("+msgId+")>("+LC.L_View_Lower+")</A>";
			if (status.equals("-2"))  	status=LC.L_Warning/*Warning*****/+"<A href=# onClick=openwindow2("+msgId+")>("+LC.L_View_Lower+")</A>";








//			if (psk!= null)

//			{








	 		   if ((i%2)==0) {

%>

      <tr class="even">

<%

	 		   }else{

%>

      <tr class="odd">

<%

	 		   }

%>
      <td width =15%><%=msgDate%></td>
        <td width =60%><A href="#"   onclick="openwindow(<%=msgId%>)"><%=message%></A></td>
        <td width =10%><%=msgtype%></td>
        <td width =5%><%=event%></td>
        <td width =10%><%=status%></td>




      </tr>

	  <%

//	  		} //end for temppatient

	  }// end for for loop

	  } // end of if for length == 0

  } //end of if for the check of patientResults == null

	  %>

    </table>
    	<table>
	<tr>
	<td>
	<% if (totalRows > 0) { Object[] arguments = {firstRec,lastRec,totalRows}; %>
		<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%--<font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
	<%} else {%>
		<font class="recNumber"><%=MC.M_NoRecordsFound%><%--No Records Found*****--%></font>
	<%}%>
	</td>
	</tr>
	</table>

	<table align=center>
	<tr>
<%

	if (curPage==1) startPage=1;

    for (int count = 1; count <= showPages;count++)
	{
   cntr = (startPage - 1) + count;

	if ((count == 1) && (hasPrevious))
	{
    %>
	<td colspan = 2>
  	<A href="viemsglist.jsp?selectedTab=1&srcmenu=<%=src%>&page=<%=cntr-1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&mode=M&filterdate=<%=filterdate%>&clientname=<%=clientName%>&status=<%=status_msg%>">< <%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%> ></A>&nbsp;&nbsp;&nbsp;&nbsp;
	</td>
	<%
  	}
	%>
	<td>
	<%

	 if (curPage  == cntr)
	 {
     %>
		<FONT class = "pageNumber"><%= cntr %></Font>
       <%
       }
      else
        {
       %>

	   <A href="viemsglist.jsp?selectedTab=2&searchFrom=initial&srcmenu=<%=src%>&page=<%=cntr%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&mode=M&filterdate=<%=filterdate%>&clientname=<%=clientName%>&status=<%=status_msg%>"><%= cntr%></A>
       <%
    	}
	 %>
	</td>
	<%
	  }

	if (hasMore)
	{
   %>
   <td colspan = 3 align = center>
  	&nbsp;&nbsp;&nbsp;&nbsp;<A href="viemsglist.jsp?selectedTab=2&searchFrom=initial&srcmenu=<%=src%>&page=<%=cntr+1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&mode=M&filterdate=<%=filterdate%>&clientname=<%=clientName%>&status=<%=status_msg%>">< <%=LC.L_Next%><%--Next*****--%> <%=totalPages%>></A>
	</td>
	<%
  	}
	%>
   </tr>
  </table>





  </Form>





</div>
 <%}else{%>
<br>
<br>
<br>
<br>
<br>

<p class = "sectionHeadings" align = center> <%=MC.M_YourSessExpired_Relogin%><%--Your session has Expired.Please Relogin.*****--%> </p>

	<META HTTP-EQUIV=Refresh CONTENT="3; URL=viehome.jsp">

<%}%>

</body>

</html>

