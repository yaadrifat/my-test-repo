<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<!-- 
Auther 		: Yogesh Kumar
Purpose		: Display Menu Panel as Horizontal Bar at the Top.

 -->
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<SCRIPT type="text/javascript">

/*Dynamically put CSS for FireFox and IE depending on screen resolution*/
  if ((document.layers) || (navigator.appName == 'Netscape') || (navigator.appName == 'Opera')){
	if (screen.width=='800'){
		document.write('<LINK id=smthemenewprint-css rel=stylesheet type=text/css href="styles/mgPanel/mgPanelstyle_FF_800.css" />');
	}else {
			document.write('<LINK id=smthemenewprint-css rel=stylesheet type=text/css href="styles/mgPanel/mgPanelstyle_FF.css" />');
		}
 	} else {
	 if (screen.width=='800'){
	 document.write('<LINK id=smthemenewprint-css rel=stylesheet type=text/css href="styles/mgPanel/mgPanelstyle_IE_800.css" />');
	 }else {
		 document.write('<LINK id=smthemenewprint-css rel=stylesheet type=text/css href="styles/mgPanel/mgPanelstyle_IE.css" />');
		 }
	}
// Call to Jquery Custom Method to enable smooth opening and closing of Menu Panels
var $j = jQuery;
$j.noConflict();
$j(document).ready(function(){
	$j("#nav-one").dropmenu();
});
</SCRIPT>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="codeLst" scope="session" class="com.velos.eres.web.codelst.CodelstJB"/>
<%@ page language = "java" import = "java.util.*,com.velos.eres.web.user.UserJB, com.velos.eres.service.util.*"%>
<%@ page import="com.velos.eres.web.objectSettings.ObjectSettingsCache, com.velos.eres.web.objectSettings.ObjectSettings"%>

<%!
  private static final int PRIVILEGE_VIEW = 4;
  private static final int PRIVILEGE_EDIT = 2;
  private static final int PRIVILEGE_NEW = 1;
  
  private boolean hasViewPermission(Integer permissionFlags) {
    if (permissionFlags == null || permissionFlags == 0) return false;
    return (permissionFlags & PRIVILEGE_VIEW) == PRIVILEGE_VIEW;
  }
  private boolean hasEditPermission(Integer permissionFlags) {
    if (permissionFlags == null || permissionFlags == 0) return false;
    return (permissionFlags & PRIVILEGE_EDIT) == PRIVILEGE_EDIT;
  }
  public static boolean hasNewPermission(Integer permissionFlags) {
    if (permissionFlags == null || permissionFlags == 0) return false;
    return (permissionFlags & PRIVILEGE_NEW) == PRIVILEGE_NEW;
  }
%>

<%!
  private String getHrefBySubtypeForIrb(String subtype) {
    if (subtype == null) { return "#"; }
    if ("landing_menu".equals(subtype)) {
        return "irblanding.jsp?mode=N";
    }
    if ("new_menu".equals(subtype)) {
        return "irbnewinit.jsp?srcmenu=tdMenuBarItem3&selectedTab=irb_init_tab&mode=N";
    }
    if ("pend_menu".equals(subtype)) {
        return "irbpenditems.jsp?selectedTab=irb_act_tab&mode=N";
    }
    if ("manage_menu".equals(subtype)) {
        return "irbsubmissions.jsp?selectedTab=irb_summary_tab&mode=N";
    }
    if ("ongoing_menu".equals(subtype)) {
        return "irbongoing.jsp?mode=N";
    }
    if ("review_menu".equals(subtype)) {
        return "irbreviewarea.jsp?selectedTab=irb_revsumm_tab&mode=N";
    }
    if ("meeting_menu".equals(subtype)) {
        return "irbmeeting.jsp?mode=N";
    }
    if ("monitor_menu".equals(subtype)) {
        return "advStudysearchpg.jsp";
    }
    
    if ("admin_menu".equals(subtype)) {
        return "formfilledaccountbrowser.jsp?srcmenu=tdMenuBarItem1";
    }
    return "#";
  }
%>

<%!
  private String getHrefBySubtypeForRegReporting(String subtype) {
    if (subtype == null) { return "#"; }
    if ("ctrp_reporting".equals(subtype)) {
        return "ctrpDraftBrowser";
    }
    return "#";
}
%>

<%!
  private String getHrefBySubtypeStd(String subtype) {
      if (subtype == null) { return "#"; }

	  if ("new_menu".equals(subtype)) {
          return "study.jsp?srcmenu=tdMenuBarItem3&selectedTab=1&mode=N";
      }
      if ("open_menu".equals(subtype)) {
          return "studybrowserpg.jsp?srcmenu=tdMenuBarItem3";
      }
	  return "#";
}
%>


<%!
  private String getHrefBySubtypePat(String subtype) {
      if (subtype == null) { return "#"; }

	  if ("new_menu".equals(subtype)) {
          return "patientdetailsquick.jsp?srcmenu=tdmenubaritem5&mode=N&selectedTab=1&pkey=&page=patient";
      }
      if ("open_menu".equals(subtype)) {
          return "allPatient.jsp?srcmenu=tdmenubaritem5&selectedTab=1&searchFrom=initial";
      }
      if ("enrolled_menu".equals(subtype)) {
          return "studypatients.jsp?srcmenu=tdmenubaritem5&selectedTab=2&openMode=F";
      }
      if ("schedule_menu".equals(subtype)) {
          return "allSchedules.jsp?srcmenu=tdmenubaritem5&selectedTab=3&openMode=F";
      }
	  return "#";
}
%>


<%!
  private String getHrefBySubtypeBudget(String subtype) {
      if (subtype == null) { return "#"; }

	  if ("new_menu".equals(subtype)) {
          return "budget.jsp?srcmenu=tdmenubaritem6&mode=N";
      }
      if ("open_menu".equals(subtype)) {
          return "budgetbrowserpg.jsp?srcmenu=tdmenubaritem6";
      }
      
	  return "#";
}
%>

<%!
  private String getHrefBySubtypeInv(String subtype) {
      if (subtype == null) { return "#"; }

	  if ("specimen_menu".equals(subtype)) {
          return "specimenbrowser.jsp?srcmenu=tdmenubaritem6&selectedTab=1";
      }
      if ("storage_menu".equals(subtype)) {
          return "storageadminbrowser.jsp?srcmenu=tdmenubaritem6&selectedTab=2";
      }
      if ("kit_menu".equals(subtype)) {
          return "storagekitbrowser.jsp?srcmenu=tdmenubaritem6&selectedTab=3";
      }
      if ("prep_menu".equals(subtype)) {
          return "preparationAreaBrowser.jsp?srcmenu=tdmenubaritem6&selectedTab=4";
      }

	  return "#";
}
%>
<%!
  private String getHrefBySubtypeForLib(String subtype) {
      if (subtype == null) { return "#"; }
      if ("callib_menu".equals(subtype)) {
          return "protocollist.jsp?mode=N&srcmenu=tdmenubaritem4&calledFrom=P&selectedTab=1";
      }
      if ("evtlib_menu".equals(subtype)) {
          return "eventlibrary.jsp?srcmenu=tdmenubaritem4&calledFrom=L&duration=&protocolId=&mode=M&calStatus=W&selectedTab=2";
      }
      if ("formlib_menu".equals(subtype)) {
          return "formLibraryBrowser.jsp?srcmenu=tdmenubaritem4&selectedTab=3";
      }
      if ("fldlib_menu".equals(subtype)) {
          return "fieldLibraryBrowser.jsp?srcmenu=tdmenubaritem4&mode=initial&selectedTab=4";
      }

      return "#";
  }
%>
<%!
  private String getHrefBySubtypeRep(String subtype) {
      if (subtype == null) { return "#"; }
      if ("rpt_menu".equals(subtype)) {
          return "reportcentral.jsp?selectedTab=1&srcmenu=tdmenubaritem8";
      }
      if ("adhoc_menu".equals(subtype)) {
          return "dynrepbrowse.jsp?srcmenu=tdmenubaritem12&selectedTab=1";
      }
      if ("dash_menu".equals(subtype)) {
    	  return "dashboardcentral.jsp";
      }
      
      if ("publishrep_menu".equals(subtype)) {
		  return "/velos/jsp/publishedReports.action";
      }
     

      return "#";
  }
%>
<%!
  private String getHrefBySubtypeAcct(String subtype) {
      if (subtype == null) { return "#"; }
	  if ("org_menu".equals(subtype)) {
          return "sitebrowser.jsp?srcmenu=tdMenuBarItem2&selectedTab=1";
      }
      if ("group_menu".equals(subtype)) {
          return "groupbrowserpg.jsp?srcmenu=tdMenuBarItem2&selectedTab=2";
      }
      if ("users_menu".equals(subtype)) {
          return "accountbrowser.jsp?srcmenu=tdMenuBarItem2&selectedTab=3";
      }
      if ("acclinks_menu".equals(subtype)) {
          return "accountlinkbrowser.jsp?srcmenu=tdMenuBarItem2&selectedTab=4";
      }

      if ("formmgmt_menu".equals(subtype)) {
          return "accountforms.jsp?srcmenu=tdMenuBarItem2&selectedTab=5";
      }

      if ("portal_menu".equals(subtype)) {
          return "portal.jsp?srcmenu=tdMenuBarItem2&selectedTab=6";
      }	
	  return "#";
}
%>

<!-- Methods for newly intorduced Links in the Menu Panel-->

<!-- To generate links for Personalize account-->
<%!
  private String getHrefBySubtypePersonal(String subtype, String userID) {
	
      if (subtype == null) { return "#"; }
	  if ("my_prof".equals(subtype)) {
          return "userdetails.jsp?mode=M&srcmenu=tdmenubaritem13&selectedTab=1&pname=ulinkBrowser";
      }
      if ("my_links".equals(subtype)) {
          return "ulinkBrowser.jsp?srcmenu=tdMenuBarItem13&selectedTab=2";
      }
      if ("new_trials".equals(subtype)) {
          return "mySubscriptions.jsp?srcmenu=tdMenuBarItem13&selectedTab=3";
      }
      if ("my_setting".equals(subtype)) {
          return "systemsettings.jsp?srcmenu=tdMenuBarItem13&selectedTab=4";
      }

      if ("pass_word".equals(subtype)) {
          return "changepwd.jsp?srcmenu=tdmenubaritem13&selectedTab=5";
      }
  
	  return "#";
}
%>
<!-- To generate links for Calender Library-->
<%!
  private String getHrefBySubtypeCalLib(String subtype) {
	
      if (subtype == null) { return "#"; }
	  if ("srch_cal".equals(subtype)) {
          return "protocollist.jsp?mode=N&srcmenu=tdmenubaritem4&calledFrom=P&selectedTab=1";
      }
      if ("add_newcal".equals(subtype)) {
          return "protocolcalendar.jsp?srcmenu=tdmenubaritem4&mode=N&selectedTab=1&calledFrom=P";
      }
      if ("newcal_type".equals(subtype)) {
          return "fieldCategory.jsp?type=L&mode=N";
      }
      if ("copy_cal".equals(subtype)) {
          return "copyprotocol.jsp?srcmenu=tdmenubaritem4&selectedTab=1&from=initial&calledFrom=P";
      }
	  return "#";
}
%>
<!-- To generate links for Event Library-->
<%!
  private String getHrefBySubtypeEvntLib(String subtype) {
	
      if (subtype == null) { return "#"; }
	  if ("srch_evnt".equals(subtype)) {
          return "eventlibrary.jsp?srcmenu=tdmenubaritem4&calledFrom=L&duration=&protocolId=&mode=M&calStatus=W&selectedTab=2";
      }
      if ("add_evnt".equals(subtype)) {
          return "eventdetails.jsp?srcmenu=tdmenubaritem4&eventmode=N&categoryId=&duration=&protocolId=&calledFrom=L&mode=M&calStatus=W&fromPage=eventlibrary&selectedTab=1";
      }
      if ("evnt_cat".equals(subtype)) {
          return "category.jsp?mode=M&catMode=N&srcmenu=tdmenubaritem4&duration=&protocolId=&calledFrom=L";
      }
	  return "#";
}
%>
<!-- To generate links for Form Library-->
<%!
  private String getHrefBySubtypeFormLib(String subtype) {
	
      if (subtype == null) { return "#"; }
	  if ("srch_form".equals(subtype)) {
          return "formLibraryBrowser.jsp?srcmenu=tdmenubaritem4&selectedTab=3";
      }
      if ("add_newform".equals(subtype)) {
          return "formcreation.jsp?srcmenu=tdmenubaritem4&calledFrom=L&mode=N&selectedTab=1&formLibId=";
      }
      if ("form_type".equals(subtype)) {
          return "fieldCategory.jsp?type=T&mode=N";
      }
      if ("copy_form".equals(subtype)) {
          return "copyFormFromLib.jsp?srcmenu=tdmenubaritem4&selectedTab=3&calledFrom=L";
      }
	  return "#";
}
%>
<!-- To generate links for Field Library-->
<%!
  private String getHrefBySubtypeFldLib(String subtype) {
	
      if (subtype == null) { return "#"; }
	  if ("srch_fld".equals(subtype)) {
          return "fieldLibraryBrowser.jsp?srcmenu=tdmenubaritem4&mode=initial&selectedTab=4";
      }
      if ("add_fld".equals(subtype)) {
          return "editBox.jsp?fieldLibId=0&mode=N";
      }
      if ("add_mulfld".equals(subtype)) {
          return "multipleChoiceBox.jsp?fieldLibId=0&mode=N";
      }
      if ("fld_cat".equals(subtype)) {
          return "fieldCategory.jsp?&mode=N&type=C";
      }
      if ("copy_fld".equals(subtype)) {
          return "copyFromFieldLibrary.jsp?mode=initial";
      }
  	  return "#";
}
%>

<!-- To generate links for CBU Entry Menu-->
<%!
  private String getHrefBySubtypeCordEntry(String subtype) {
	  if (subtype == null) { return "#"; }
	  if ("cbuentry_menu".equals(subtype)) {
		  return "/velos/jsp/cordEntryInProgress.action";
      }
	  return "#";
	}
%>
<!-- To generate links for Upload Document Menu-->
<%!
  private String getHrefBySubtypeUploadDoc(String subtype) {
	  if (subtype == null) { return "#"; }
	  if ("open_order".equals(subtype)) {
          return "/velos/jsp/openorder.action";
      }
	  return "#";
	}
%>
<!-- To generate links for Product Fullfilment Menu-->
<%!
  private String getHrefBySubtypePF(String subtype) {
	  if (subtype == null) { return "#"; }

	  if ("prdt_fullfill".equals(subtype)) {
          return "/velos/jsp/productfullfillmenthome.action";
      }
	  if ("opn_task".equals(subtype)) {
          return "/velos/jsp/opentask.action";
      }
	  return "#";
  }
%>

<!-- To generate links for Center Management Menu-->
<%!
  private String getHrefBySubtypeCentreMange(String subtype) {
      if (subtype == null) { return "#"; }
      if ("cbb_profile".equals(subtype)) {
          return "/velos/jsp/getCbbDetails.action";
      }
	  if ("proc_proced".equals(subtype)) {
          return "/velos/jsp/cbbProcedure.action";
      }
	  return "#";
}
%>
<!-- To generate links for User Profile Menu-->
<%!
  private String getHrefBySubtypeUserPro(String subtype) {
      if (subtype == null) { return "#"; }
      if ("my_pro".equals(subtype)) {
          return "/velos/jsp/userProfile.action";
      }
	  return "#";
}
%>
<!-- To generate links for Reference Menu-->
<%!
  private String getHrefBySubtypeRefrence(String subtype) {
	  if (subtype == null) { return "#"; }
	  if ("lnk_sops".equals(subtype)) {
          return "linkReference?linkRefCode=SOP";
      }
	  if ("lnk_mops".equals(subtype)) {
          return "linkReference?linkRefCode=MOP";
      }
	  if ("usr_guide".equals(subtype)) {
          return "linkReference?linkRefCode=GUIDE";
      }
	  if ("help_lnk".equals(subtype)) {
          return "linkReference?linkRefCode=HELP";
      }
	  if ("faq".equals(subtype)) {
          return "#";
      }
	  return "#";
  }
%>
<!-- To generate links for Contact Menu-->
<%!
  private String getHrefBySubtypeContacUs(String subtype) {
	  if (subtype == null) { return "#"; }
	  if ("staff_dir".equals(subtype)) {
          return "#";
      }
	  if ("chat_help".equals(subtype)) {
          return "#";
      }
	  if ("net_dir".equals(subtype)) {
          return "contactReference?linkRefCode=NETWORK";
      }
	  return "#";
  }
%>
<!-- To generate links for Funding Menu-->
<%!
  private String getHrefBySubtypeFunding(String subtype) {
	  if (subtype == null) { return "#"; }
	  if ("funding_menu".equals(subtype)) {
		  return "/velos/jsp/fundingnmdp.action";
      }
	  return "#";
	}
%>
<!-- To generate links for Pending Request Menu-->
<%!
  private String getHrefBySubtypePendingReq(String subtype) {
	  if (subtype == null) { return "#"; }
	  if ("pending_ordr".equals(subtype)) {
          return "/velos/jsp/pendingOrders.action";
      }
	  return "#";
	}
%>
<!-- To generate links for CBU Tools Menu Menu-->
<%!
  private String getHrefBySubtypeCbuTools(String subtype) {
	  if (subtype == null) { return "#"; }
	  if ("app_msg_menu".equals(subtype)) {
          return "/velos/jsp/getAppMsgs.action";
      }
	  if ("creat_data_req".equals(subtype)) {
          return "/velos/jsp/createDataReq.action";
      }
      if ("view_data_req".equals(subtype)) {
          return "/velos/jsp/viewDataReq.action";
      }
      if ("mng_ling_menu".equals(subtype)) {
          return "/velos/jsp/getnmdpLink.action";
      }
	  return "#";
	}
%>
<!-- To generate links for ecompliance Menu-->

<%!
  private String getHrefBySubtypeirbConfig(String subtype) {
      if (subtype == null) { return "#"; }

	  if ("new_config_menu".equals(subtype)) {
          return "irbBoardSchedule.jsp?srcmenu=tdmenubaritem6&selectedTab=1";
      }
	  if ("new_forms_menu".equals(subtype)) {
          return "irbConfigForms.jsp?srcmenu=tdmenubaritem6&selectedTab=2";
      }
	  if ("new_grp_menu".equals(subtype)) {
          return "irbGroupAcess.jsp?srcmenu=tdmenubaritem6&selectedTab=3";
      }
	  if ("new_board_menu".equals(subtype)) {
          return "irbReviewBoard.jsp?srcmenu=tdmenubaritem6&selectedTab=4";
      }
	  return "#";
}
%>
<%

HttpSession tSession = request.getSession(true); 

	String accName = (String) tSession.getValue("accName");
	accName=(accName==null)?"default":accName;
	String accSkinId = ""; 
	String usersSkinId = "";
	String userSkin = "";
	String accSkin = "";
	String skin = "default";
	accSkinId = 	(String) tSession.getValue("accSkin");
   	UserJB userB1 = (UserJB) tSession.getValue("currentUser");      
	usersSkinId = userB1.getUserSkin();
	userSkin= codeLst.getCodeCustomCol(EJBUtil.stringToNum(usersSkinId) );
	accSkin= codeLst.getCodeCustomCol(EJBUtil.stringToNum(accSkinId) );
	if (accSkin == null || accSkin.equals("") || accSkin.equals("default") ){
		accSkin = "accSkin";
	}
	else{
		skin = accSkin;
	}
	if (userSkin != null && !userSkin.equals("") ){
		skin = userSkin;
	}
%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>

<jsp:useBean id="modCtlDao" scope="request" class="com.velos.eres.business.common.CtrlDao"/>


<%
//	HttpSession tSession = request.getSession(true);

	int siteRight = 0, groupRight = 0 , userRight = 0, userPers = 7, accLinks = 0, milestoneRight = 0;
	int userProtocol = 0, calLib = 0, userPat = 0, userReports = 0, manageAct = 1, modbudget = 0, budgetrights = 0,manageLib = 1;
	int dataSafMonGrpRight = 0, fieldLibGrpRight = 0, formLibGrpRight = 0; int A2ARight = 0;
	int frmManageGrpRight = 0; int storageRight = 0; int specRight = 0;
	int evLib = 0;
	int adHocGrpRight = 0;
	int ctrpGrpRight = 0;
	char eIRBRightMod = '0';
	char eDashboardMod = '0';
	
	//added by Sanjeev 02/06/12
	char cdrpfRight='0';
	int modCDRPFseq = 0;
	int pageRightModCdrPf = 0;
	
	GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getAttribute("GRights");
	String logUsr = (String) tSession.getValue("userId");
	
	String modRight = (String) tSession.getValue("modRight");

	int modlen = modRight.length();

	modCtlDao.getControlValues("module"); //get extra modules
	
	ArrayList modCtlDaofeature =  modCtlDao.getCValue();
	ArrayList modCtlDaoftrSeq = modCtlDao.getCSeq();
	 
	int modseq = 0, budseq = 0, mileSeq = 0, milestoneSeq = 0, dsmSeq = 0;
	int patProfileSeq = 0, formLibSeq = 0 , dsmModSeq = 0;
	
	int irbSeq = 0;
	int dashSeq = 0;
	int irbModSeq = 0;
	int dashModSeq = 0;
	
	int A2ASeq = 0;
	
	int adhocqSeq = 0; 
	int adhocqModSeq = 0;
	int A2AModSeq = 0;
	int inventorySeq = 0;
	int inventoryModSeq = 0;
	int regReportSeq = 0;
	int regReportModSeq = 0;
	
	char budRight = '0';
	char mileRight = '0';
	char dataSafMonAppRight = '0';
	char patProfileAppRight = '0';
	char formLibAppRight = '0';
	char adhocqAppRight = '0';
	char a2aRight = '0';
	char inventoryRight = '0';
	char regReportAppRight = '0';
	
	char portalRight = '0';
	char protocolManagementRight = '0';
	int patientPortalSeq = 0,patientPortalModSeq = 0 ;
	int protocolMgmtSeq = 0,protocolMgmtModSeq = 0;
	int portalGroupRight = 0;
	
 
	int dashGroupRight = 0;
	
	  int irbNewAppGroupRight =  0;
	  int irbActionReqGroupRight =  0; 	  int irbPendingRevGroupRight =  0;
	  int irbOngoingGroupRight =  0;
	  int irbSubActSumGroupRight =  0;
	  int irbSubNewGroupRight =  0;
	  int irbSubAssignedGroupRight =  0;
	  int irbSubComplGroupRight =  0;
	  int irbSubPRGroupRight =  0;
	  int irbSubPendingPIGroupRight =  0;
	  int irbRevPendingGroupRight =  0;
	  int irbRevFullGroupRight =  0;
	  int irbRevExpGroupRight =  0;
	  int irbRevExemGroupRight =  0;
	  int irbRevAncGroupRight =  0;

	int irbManageMeetingsGroupRight =  0;
	int irbMonitorGroupRight =  0;
	int irbAToolsGroupRight =  0;
	
	int cbuCordEntryRight = 0;
	int cbuUploadDocRight=0;
	int cbuFundingMenuRght=0;
	int cbuPendingReqMenuRght=0;
	int cbuMaintMenuRght=0;
	int cbuMyProMenuRght=0;
	int cbuCBBProMenuRght=0;
	int cbuProcProcedureMenuRght=0;
	int cbuReferencesMenuRght=0;
	int cbuLinkToSOPMenuRght=0;
	int cbuLinkToMOPMenuRght=0;
	int cbuGuideMenuRght=0;
	int cbuHelpfulLinkMenuRght=0;
	int cbuFAQMenuRght=0;
	int cbuContactInfoMenuRght=0;
	int cbuNetworkDirMenuRght=0;
	int cbuStaffDirMenuRght=0;
	int cbuToolsMenuRght=0;
	int cbuAppMssgsMenuRght=0;
	
	
	
	 
	modseq = modCtlDaofeature.indexOf("MODBUD");
    mileSeq = modCtlDaofeature.indexOf("MODMIL");
	dsmModSeq = modCtlDaofeature.indexOf("MODDSM");
	patProfileSeq = modCtlDaofeature.indexOf("MODPATPROF");
	formLibSeq = modCtlDaofeature.indexOf("MODFORMS");
	adhocqModSeq =  modCtlDaofeature.indexOf("ADHOCQ");

	//added by Sonia Abrol, 04/11/06, to show A2A link
	A2AModSeq =  modCtlDaofeature.indexOf("A2A");
	inventoryModSeq = modCtlDaofeature.indexOf("MODINVM");
	
	dashModSeq = modCtlDaofeature.indexOf("MODDASH");
 	irbModSeq = modCtlDaofeature.indexOf("MODEIRB");
 	regReportModSeq =  modCtlDaofeature.indexOf("MODREGREPORTING");
 	
 	
 	//added by Sanjeev 02/06/12
	 modCDRPFseq = modCtlDaofeature.indexOf("MODCDRPF");
	 pageRightModCdrPf = ((Integer) modCtlDaoftrSeq.get(modCDRPFseq)).intValue();
	 cdrpfRight = modRight.charAt(pageRightModCdrPf - 1);
	 
	budseq	= ((Integer) modCtlDaoftrSeq.get(modseq)).intValue();   	
	milestoneSeq = ((Integer) modCtlDaoftrSeq.get(mileSeq)).intValue();
	patProfileSeq = ((Integer) modCtlDaoftrSeq.get(patProfileSeq)).intValue();
	formLibSeq = ((Integer) modCtlDaoftrSeq.get(formLibSeq)).intValue();
	dsmSeq  = ((Integer) modCtlDaoftrSeq.get(dsmModSeq)).intValue();
	adhocqSeq  = ((Integer) modCtlDaoftrSeq.get(adhocqModSeq)).intValue();
	
	inventorySeq = ((Integer) modCtlDaoftrSeq.get(inventoryModSeq)).intValue();
	 
	dashSeq = ((Integer) modCtlDaoftrSeq.get(dashModSeq)).intValue();
	irbSeq = ((Integer) modCtlDaoftrSeq.get(irbModSeq)).intValue();
	regReportSeq  = ((Integer) modCtlDaoftrSeq.get(regReportModSeq)).intValue();

	//added by Sonia Abrol, 04/11/06, to show A2A link
	A2ASeq = ((Integer) modCtlDaoftrSeq.get(A2AModSeq)).intValue();
	
	//for portal and protocol management
	
	patientPortalModSeq = modCtlDaofeature.indexOf("MODPPORTAL");
	protocolMgmtModSeq = modCtlDaofeature.indexOf("MODPROTOCOL");
	
	 
	
	patientPortalSeq = ((Integer) modCtlDaoftrSeq.get(patientPortalModSeq)).intValue();
	protocolMgmtSeq = ((Integer) modCtlDaoftrSeq.get(protocolMgmtModSeq)).intValue();
	
	portalRight = modRight.charAt(patientPortalSeq - 1);
	protocolManagementRight = modRight.charAt(protocolMgmtSeq - 1);
	
	portalGroupRight = Integer.parseInt(grpRights.getFtrRightsByValue("PORTALAD"));
	
	
    budRight = 	modRight.charAt(budseq - 1);
    mileRight = modRight.charAt(milestoneSeq - 1);
    dataSafMonAppRight = modRight.charAt(dsmSeq - 1);
	patProfileAppRight = modRight.charAt(patProfileSeq - 1);
	formLibAppRight = modRight.charAt(formLibSeq - 1);
	adhocqAppRight = modRight.charAt(adhocqSeq - 1);	
	inventoryRight= modRight.charAt(inventorySeq - 1);	
	 
	eIRBRightMod = modRight.charAt(irbSeq- 1);
	eDashboardMod = modRight.charAt(dashSeq- 1);
	regReportAppRight = modRight.charAt(regReportSeq - 1);
	
	//set this value to session if not set
	
	String sessionInventoryRight = "";
	
	sessionInventoryRight  = (String) tSession.getAttribute("sessionInventoryRight");
	
	if (StringUtil.isEmpty(sessionInventoryRight))
	{
	  sessionInventoryRight = String.valueOf(inventoryRight);
	  tSession.setAttribute("sessionInventoryRight",sessionInventoryRight);
	  System.out.println("sessionInventoryRight" + sessionInventoryRight);
	}
		
		
	storageRight =  Integer.parseInt(grpRights.getFtrRightsByValue("MSTORAGE"));
	specRight =  Integer.parseInt(grpRights.getFtrRightsByValue("MSPEC"));
	
	//added by Sonia Abrol, 04/11/06, to show A2A link
	a2aRight = modRight.charAt(A2ASeq - 1);	
	A2ARight =  Integer.parseInt(grpRights.getFtrRightsByValue("A2A"));
	
	
	
	//check for organizations
    siteRight = Integer.parseInt(grpRights.getFtrRightsByValue("MSITES"));
	
	//check for Groups
    groupRight = Integer.parseInt(grpRights.getFtrRightsByValue("CGRP"));
	
	//check for Users
   userRight = Integer.parseInt(grpRights.getFtrRightsByValue("MUSERS"));
	
	//check for Personalize Account

    //userPers = Integer.parseInt(grpRights.getFtrRightsByValue("HPERSHOME"));

	//check for Account Links
	accLinks = Integer.parseInt(grpRights.getFtrRightsByValue("MACCLINKS"));
	
	//check for account form management
	frmManageGrpRight = Integer.parseInt(grpRights.getFtrRightsByValue("MACCFRMS"));
		
	//check for Manage Protocols 
    userProtocol = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));
	
	//check for Calendar Library 
    calLib = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));

  	//check for event Library 
    evLib = Integer.parseInt(grpRights.getFtrRightsByValue("EVLIB"));
	
	//check for Manage Patients 
   userPat = Integer.parseInt(grpRights.getFtrRightsByValue("MPATIENTS"));

	//check for Manage Patients 
  userReports = Integer.parseInt(grpRights.getFtrRightsByValue("REPORTS"));
  
  	//check for Milestone 
  milestoneRight = Integer.parseInt(grpRights.getFtrRightsByValue("MILEST"));

  
  // check for data safety montitoring
  dataSafMonGrpRight = Integer.parseInt(grpRights.getFtrRightsByValue("DSM"));
  
  // check for field library group right
  fieldLibGrpRight = Integer.parseInt(grpRights.getFtrRightsByValue("MFLDLIB"));  

 boolean showSubmission = false;
 boolean showCompliance = false;  
 boolean showPending = false;
 boolean showReviewer = false;
 
 
   // check for form library group right
  formLibGrpRight = Integer.parseInt(grpRights.getFtrRightsByValue("MFRMLIB"));  
 
  adHocGrpRight = Integer.parseInt(grpRights.getFtrRightsByValue("ADHOC"));  
  
  ctrpGrpRight = Integer.parseInt(grpRights.getFtrRightsByValue("REGREPORT_CTRP"));
  
  if (eIRBRightMod=='1')    //IRB module available
	{
 
			  irbNewAppGroupRight =  Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_APP"));
			  
			  
			  irbActionReqGroupRight =  Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_AR"));
			  irbPendingRevGroupRight =  Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_PR"));
			  
			  if (irbNewAppGroupRight >=4)
			  {
			  	showPending = true;
			  	showCompliance = true;
			  }
			  
			  irbOngoingGroupRight =  Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_ON"));
			  
			  irbSubActSumGroupRight =  Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_SSUM"));
			  irbSubNewGroupRight =  Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_SNS"));
			  irbSubAssignedGroupRight =  Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_SAS"));
			  irbSubComplGroupRight =  Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_SCS"));
			  irbSubPRGroupRight =  Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_SPRS"));
			  irbSubPendingPIGroupRight =  Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_SPI"));
			  
			  if (irbSubActSumGroupRight >=4 || irbSubNewGroupRight >= 4 || irbSubAssignedGroupRight >= 4 || irbSubComplGroupRight >= 4 
			  	  || irbSubPRGroupRight >= 4 || irbSubPendingPIGroupRight >= 4)
			  {
			  	showSubmission = true;
			  	showCompliance = true;
			  }
			  
			  irbRevPendingGroupRight =  Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_RPR"));
			  irbRevFullGroupRight =  Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_RFR"));
			  irbRevExpGroupRight =  Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_RXPR"));
			  irbRevExemGroupRight =  Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_RXER"));
			  irbRevAncGroupRight =  Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_RACR"));

			  if (irbRevPendingGroupRight >=4 || irbRevFullGroupRight >= 4 || irbRevExpGroupRight >= 4 || irbRevExemGroupRight >= 4 
			  	  || irbRevAncGroupRight >= 4 )
			  {
			  	showReviewer = true;
			  	showCompliance = true;
			  }


				irbManageMeetingsGroupRight =  Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_MM"));
				irbMonitorGroupRight =  Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_MS"));
				irbAToolsGroupRight =  Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_AT"));
			  
			  if (irbActionReqGroupRight >=4  || irbPendingRevGroupRight >= 4 || irbOngoingGroupRight >= 4 || irbManageMeetingsGroupRight>=4
			  	  || irbMonitorGroupRight >= 4 || irbAToolsGroupRight >= 4)
			  {
			  	showCompliance = true;
			  }
	}
	else
	{
			  	showCompliance = false;
		
	}
	
  dashGroupRight = Integer.parseInt(grpRights.getFtrRightsByValue("DASH"));  
	

 //Modified by Manimaran to fix the Bug2655 //SM Bug#4231
 if (siteRight == 0 && groupRight == 0 && userRight == 0 && accLinks == 0 && frmManageGrpRight==0 && portalGroupRight==0) 
 {
	 manageAct = 0;
 }

 if(calLib == 0 && evLib == 0 && fieldLibGrpRight == 0 && formLibGrpRight == 0)
 {
	 manageLib = 0;
 }

  budgetrights = Integer.parseInt(grpRights.getFtrRightsByValue("BUDGT"));
  
  cbuCordEntryRight = Integer.parseInt(grpRights.getFtrRightsByValue("CB_ADD"));
  cbuUploadDocRight = Integer.parseInt(grpRights.getFtrRightsByValue("CB_UPLDOC"));
  cbuFundingMenuRght = Integer.parseInt(grpRights.getFtrRightsByValue("CB_FUNDING"));
  cbuPendingReqMenuRght = Integer.parseInt(grpRights.getFtrRightsByValue("CB_PENDINGREQ"));
  cbuMaintMenuRght = Integer.parseInt(grpRights.getFtrRightsByValue("CB_MAINT"));
  cbuMyProMenuRght = Integer.parseInt(grpRights.getFtrRightsByValue("CB_MYPRO"));
  cbuCBBProMenuRght = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CBBPROFILE"));
  cbuProcProcedureMenuRght = Integer.parseInt(grpRights.getFtrRightsByValue("CB_PROCEDURE"));
  cbuReferencesMenuRght = Integer.parseInt(grpRights.getFtrRightsByValue("CB_REFERENCES"));
  cbuLinkToSOPMenuRght = Integer.parseInt(grpRights.getFtrRightsByValue("CB_LINKSOP"));
  cbuLinkToMOPMenuRght = Integer.parseInt(grpRights.getFtrRightsByValue("CB_LINKMOP"));
  cbuGuideMenuRght = Integer.parseInt(grpRights.getFtrRightsByValue("CB_GUIDE"));
  cbuHelpfulLinkMenuRght = Integer.parseInt(grpRights.getFtrRightsByValue("CB_HELPFULLNK"));
  cbuFAQMenuRght = Integer.parseInt(grpRights.getFtrRightsByValue("CB_FAQ"));
  cbuContactInfoMenuRght = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CONTINFO"));
  cbuNetworkDirMenuRght = Integer.parseInt(grpRights.getFtrRightsByValue("CB_NETWORKDIR"));
  cbuStaffDirMenuRght = Integer.parseInt(grpRights.getFtrRightsByValue("CB_STAFFDIR"));
  cbuToolsMenuRght = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CBUTOOLS"));
  cbuAppMssgsMenuRght = Integer.parseInt(grpRights.getFtrRightsByValue("CB_APPMSGS"));
  
  
  String accId = (String) tSession.getValue("accountId");
  ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
  ArrayList topMenuList = objCache.getAccountObjects(EJBUtil.stringToNum(accId),"top_menu");
  %>
  
<body>
<table width="99%">
<tr>
<td>
<!-- Main UL starts for Mega Menu Panel-->
<ul	id="nav-one" class="dropmenu"> 
	<%	for(int topIndex=0; topIndex<topMenuList.size(); topIndex++) {
  ObjectSettings thisTopObj = (ObjectSettings) topMenuList.get(topIndex);
  if ("0".equals(thisTopObj.getObjVisible())) { continue; }

  if ("homepage_menu".equals(thisTopObj.getObjSubType())) {
  %>
  		<li> 
			<a id="mgPanel" href="myHome.jsp?srcmenu=tdMenuBarItem1"><%=thisTopObj.getObjDispTxt()%></a> 
		</li> 
 <% 
  	}else if ("personalize_menu".equals(thisTopObj.getObjSubType())) {
  		String divClass="personalize";
	   %>
	  	<li> 
			<a id="mgPanel" href="#"><%=thisTopObj.getObjDispTxt()%></a> 
			<div class="<%=divClass %>">
			<%
			 ArrayList acctMenu = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "personalize_menu");
			String colClass="left";
			%>
			<ul class=<%=colClass%>> 
			<%-- li class="subheading"><h2><%=settingAccMenu.getObjDispTxt()%></h2></li--%>
			<%ArrayList personalMenuList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "personalize_menu");
			for (int iZ=0; iZ<personalMenuList.size(); iZ++) {
	        ObjectSettings personsettings = (ObjectSettings)personalMenuList.get(iZ);
			// If this sub menu is configured as invisible in DB, skip it
			if ("0".equals(personsettings.getObjVisible())) { continue; }
	
			 // Figure out access rights of this sub menu
		    boolean showThisSubMenu = false;
	
			if ("my_prof".equals(personsettings.getObjSubType())) {
				showThisSubMenu = true;
	        } else if ("my_links".equals(personsettings.getObjSubType())) {
	        	showThisSubMenu = true;
			} else if ("new_trials".equals(personsettings.getObjSubType())) {
				showThisSubMenu = true;
			} else if ("my_setting".equals(personsettings.getObjSubType())) {
				showThisSubMenu = true;
			} else if ("pass_word".equals(personsettings.getObjSubType())) {
				showThisSubMenu = true;
			} else {
	            showThisSubMenu = true;
	        }
	        if (!showThisSubMenu) { continue; } // No access right; skip this sub menu
		    // Display the visible sub menu
		    %>
		   <li><a id="mgPanel" href="<%=getHrefBySubtypePersonal(personsettings.getObjSubType(), logUsr)%>"><%=personsettings.getObjDispTxt()%></a></li>
		    <%
		  	 } // End of acctMenuList loop*/
		    %>
			</ul>
			
			</div>
		</li> 
	 <%
	}else if ("manage_accnt".equals(thisTopObj.getObjSubType())) { 
	 if(((String.valueOf(protocolManagementRight).compareTo("1") == 0)&& userProtocol > 0) || 
			 (userPat > 0) || (String.valueOf(budRight).compareTo("1") == 0 && budgetrights > 2)){
		%><%--YK 06Sep2011: Fixed Bug #6856  --%>
	  	<li> 
			<a id="mgPanel" href="#"><%=thisTopObj.getObjDispTxt()%></a> 
			<div class="manage">
			<%
			 ArrayList manageAcctMenu = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "manage_accnt");
			String colClass="left";
			int checkClass=0;
			for (int iX=0; iX<manageAcctMenu.size(); iX++) {
				
		        ObjectSettings settingAccMenu = (ObjectSettings)manageAcctMenu.get(iX);
				// If this sub menu is configured as invisible in DB, skip it
				if ("0".equals(settingAccMenu.getObjVisible())) { continue; }
				boolean showThisMenu = false;
				 if ("study_menu".equals(settingAccMenu.getObjSubType())) {
					 showThisMenu = (String.valueOf(protocolManagementRight).compareTo("1") == 0)&& userProtocol > 0 ? true : false; /*YK 05Sep2011 : Fixed for Bug#6840*/
				} else if ("application_menu".equals(settingAccMenu.getObjSubType())) {
					showThisMenu = manageAct > 0 ? true : false;
				} else if ("mgpat_menu".equals(settingAccMenu.getObjSubType())) {
					showThisMenu = userPat > 0 ? true : false;
				} else if ("budget_menu".equals(settingAccMenu.getObjSubType())) {
					showThisMenu = (String.valueOf(budRight).compareTo("1") == 0 && budgetrights > 2) ? true : false;
				} else if ("milestone_menu".equals(settingAccMenu.getObjSubType())) {
					showThisMenu = (String.valueOf(mileRight).compareTo("1") == 0  && milestoneRight >= 4) ? true : false;
				}else {
					showThisMenu = true;
				}
				if (!showThisMenu) { continue; } // No access right; skip this sub menu
				checkClass++;
				if ((checkClass%2)==0) {
						colClass="right";
				}else{
						colClass="left";
				}
				 if ("study_menu".equals(settingAccMenu.getObjSubType())) {
					 	%>
					<ul class=<%=colClass%>> 
					<li class="subheading"><h2><%=settingAccMenu.getObjDispTxt()%></h2></li>
					<%
					
					 ArrayList stdMenuList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "study_menu");
					
					for (int iY=0; iY<stdMenuList.size(); iY++) {
			        ObjectSettings settings = (ObjectSettings)stdMenuList.get(iY);
					if ("0".equals(settings.getObjVisible())) { continue; }
					

				    boolean showThisSubMenu = false;
			        if ("new_menu".equals(settings.getObjSubType())) {
			            showThisSubMenu = (userProtocol == 5 || userProtocol == 7 ) ? true : false;
			        } else if ("open_menu".equals(settings.getObjSubType())) {
			            showThisSubMenu = userProtocol > 0 ? true : false;
					} else {
			            showThisSubMenu = true;
			        }
			        if (!showThisSubMenu) { continue; } // No access right; skip this sub menu
			    // Display the visible sub menu
			    %>
			     <li><a id="mgPanel" href="<%=getHrefBySubtypeStd(settings.getObjSubType())%>"><%=settings.getObjDispTxt()%></a></li>
				<%
					} // End of acctMenuList loop
				    %>
					
					</ul>
				<%}else if ("application_menu".equals(settingAccMenu.getObjSubType())) { %>
					<ul class=<%=colClass%>> 
					<li class="subheading"><h2><%=settingAccMenu.getObjDispTxt()%></h2></li>
					<%ArrayList acctMenuList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "application_menu");
					for (int iY=0; iY<acctMenuList.size(); iY++) {
			        ObjectSettings accsettings = (ObjectSettings)acctMenuList.get(iY);
					// If this sub menu is configured as invisible in DB, skip it
					if ("0".equals(accsettings.getObjVisible())) { continue; }
			
					 // Figure out access rights of this sub menu
				    boolean showThisSubMenu = false;
			
					if ("org_menu".equals(accsettings.getObjSubType())) {
			            showThisSubMenu = siteRight > 0 ? true : false;
			        } else if ("group_menu".equals(accsettings.getObjSubType())) {
			            showThisSubMenu = groupRight > 0 ? true : false;
					} else if ("users_menu".equals(accsettings.getObjSubType())) {
			            showThisSubMenu = userRight > 0 ? true : false;
					} else if ("users_menu".equals(accsettings.getObjSubType())) {
			            showThisSubMenu = userRight > 0 ? true : false;
					} else if ("acclinks_menu".equals(accsettings.getObjSubType())) {
			            showThisSubMenu = accLinks > 0 ? true : false;
					} else if ("formmgmt_menu".equals(accsettings.getObjSubType())) {
						if (((String.valueOf(formLibAppRight).compareTo("1") == 0) || (String.valueOf(patProfileAppRight).compareTo("1") == 0)) && (frmManageGrpRight > 0 )) {
						showThisSubMenu = true;
					   }
					} else if ("portal_menu".equals(accsettings.getObjSubType())) {
						if ( (String.valueOf(portalRight).compareTo("1") == 0)  && (portalGroupRight > 0 )) {
						showThisSubMenu = true;
						}
					} else {
			            showThisSubMenu = true;
			        }
			        if (!showThisSubMenu) { continue; } // No access right; skip this sub menu
				    // Display the visible sub menu
				    %>
				    <li><a id="mgPanel" href="<%=getHrefBySubtypeAcct(accsettings.getObjSubType())%>"><%=accsettings.getObjDispTxt()%></a></li>
				    <%
				   	 } // End of acctMenuList loop
					%>
					</ul>
				<%}else   if ("mgpat_menu".equals(settingAccMenu.getObjSubType())) {
					%>
					<ul class=<%=colClass%>> 
					<li class="subheading"><h2><%=settingAccMenu.getObjDispTxt()%></h2></li>
					<%
					ArrayList patMenuList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "mgpat_menu");
					
					for (int iZ=0; iZ<patMenuList.size(); iZ++) {
			            ObjectSettings settings = (ObjectSettings)patMenuList.get(iZ);
					   // If this sub menu is configured as invisible in DB, skip it
					   if ("0".equals(settings.getObjVisible())) { continue; }
					
				       // Figure out access rights of this sub menu
				       boolean showThisSubMenu = false;
					
					   if ("new_menu".equals(settings.getObjSubType())) {
							showThisSubMenu = (userPat == 5 || userPat == 7) ? true : false;
					   } else if ("open_menu".equals(settings.getObjSubType())) {
							showThisSubMenu = userPat > 0 ? true : false;
					   }  else {
			                showThisSubMenu = true;
					   }
							if (!showThisSubMenu) { continue; } // No access right; skip this sub menu
					 %>
					  <li><a id="mgPanel" href="<%=getHrefBySubtypePat(settings.getObjSubType())%>"><%=settings.getObjDispTxt()%></a></li>
					
					<%
					}
					%>
					</ul>
					<%
				}else   if ("budget_menu".equals(settingAccMenu.getObjSubType())) {
					%>
					<ul class=<%=colClass%>> 
					<li class="subheading"><h2><%=settingAccMenu.getObjDispTxt()%></h2></li>
					<%
					ArrayList bgtMenuList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "budget_menu");
					
					for (int iA=0; iA<bgtMenuList.size(); iA++) {
			            ObjectSettings settings = (ObjectSettings)bgtMenuList.get(iA);
					   // If this sub menu is configured as invisible in DB, skip it
					   if ("0".equals(settings.getObjVisible())) { continue; }
					
				       // Figure out access rights of this sub menu
				       boolean showThisSubMenu = false;
					
				       if ("new_menu".equals(settings.getObjSubType())) {
				    		
							if ( (String.valueOf(budRight).compareTo("1") == 0) && (budgetrights == 5 || budgetrights == 7))
						    {
								 showThisSubMenu = true;
							}
					  }
					  else if ("open_menu".equals(settings.getObjSubType())) {
							if ((String.valueOf(budRight).compareTo("1") == 0) && budgetrights >  2)
							{
								 showThisSubMenu = true;
							}
					  } else {
			                showThisSubMenu = true;
					   }
							if (!showThisSubMenu) { continue; } // No access right; skip this sub menu
					 %>
					  <li><a id="mgPanel" href="<%=getHrefBySubtypeBudget(settings.getObjSubType())%>"><%=settings.getObjDispTxt()%></a></li>
					
					<%
					}
					%>
					</ul>
					<%
				} else if ("milestone_menu".equals(settingAccMenu.getObjSubType())) {
		  		ArrayList financeMenu = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "milestone_menu");
				%>
				<ul class=<%=colClass%>> 
					<li class="subheading"><h2><%=settingAccMenu.getObjDispTxt()%></h2></li>
					<%
				 for (int iZ=0; iZ<financeMenu.size(); iZ++) {
		            ObjectSettings settingFinMenu = (ObjectSettings)financeMenu.get(iZ);
				   // If this sub menu is configured as invisible in DB, skip it
				   if ("0".equals(settingFinMenu.getObjVisible())) { continue; }
			       // Figure out access rights of this sub menu
			       boolean showThisSubMenu = false;
					 if ("open_menu".equals(settingFinMenu.getObjSubType()) && (String.valueOf(mileRight).compareTo("1") == 0 && milestoneRight >= 4)) {
						showThisSubMenu = true;
					%> 
						<li><a id="mgPanel" href="milestonehome.jsp?srcmenu=tdmenubaritem7"><%=settingFinMenu.getObjDispTxt()%></a></li>
						
					<%}
						if (!showThisSubMenu) { continue; } // No access right; skip this sub menu
					%>
					 
			    <%
		      	} // End of financeMenu loop
			   %>
			      </ul>
			   <%
		  	}
			}
			%>
			</div>
		</li> 
	 <% }
	} else if  ("irb_menu".equals(thisTopObj.getObjSubType()) && showCompliance== true) {
	 {
		%>
	  	<li> 
			<a id="irbPanel" href="#"><%=thisTopObj.getObjDispTxt()%></a> 
			<div class="research">
			<%
			 ArrayList irbMenu = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "irb_menu");
			String colClass="left";
			int checkClass=0;
			for (int iX=0; iX<irbMenu.size(); iX++) {
				
		        ObjectSettings settingirbMenu = (ObjectSettings)irbMenu.get(iX);
				
				if ("0".equals(settingirbMenu.getObjVisible())) { continue; }
				boolean showThisMenu = false;
				if ("app_sub".equals(settingirbMenu.getObjSubType())) {
					 showThisMenu = true ; 
				} else if ("irb_config".equals(settingirbMenu.getObjSubType())) {
					showThisMenu =true;
				} else if ("cont_rev_menu".equals(settingirbMenu.getObjSubType())) {
					showThisMenu = true ;
				} else if ("pre_rev_menu".equals(settingirbMenu.getObjSubType())) {
					showThisMenu = true ;
				} else if ("irb_review".equals(settingirbMenu.getObjSubType())) {
					showThisMenu = true ;
				} else if ("irb_report".equals(settingirbMenu.getObjSubType())) {
					showThisMenu = true ;
				}else {
					showThisMenu = true;
				}
				if (!showThisMenu) { continue; } // No access right; skip this sub menu
				
				 if ("app_sub".equals(settingirbMenu.getObjSubType())) {
					 	%>
					<ul class=<%=colClass%>> 
					<li class="research"><a href='#'><%=settingirbMenu.getObjDispTxt()%></a></li>
					</ul>
					<% }
					
				 if ("irb_config".equals(settingirbMenu.getObjSubType())) {
					 	%>
					<ul class=<%=colClass%>> 
					<li class="subheading"><h2><%=settingirbMenu.getObjDispTxt()%></h2></li>
					<%
					
					 ArrayList irbConfigList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "irb_config");
					
					for (int iY=0; iY<irbConfigList.size(); iY++) {
			        ObjectSettings settings = (ObjectSettings)irbConfigList.get(iY);
					if ("0".equals(settings.getObjVisible())) { continue; }
					

				    boolean showThisSubMenu = false;
			        if ("new_config_menu".equals(settings.getObjSubType())) {
			            showThisSubMenu =  true ;
			        } else if ("new_forms_menu".equals(settings.getObjSubType())) {
			            showThisSubMenu = true;
					} else if ("new_grp_menu".equals(settings.getObjSubType())) {
			            showThisSubMenu = true;
					} else if ("new_board_menu".equals(settings.getObjSubType())) {
			            showThisSubMenu = true;
					} else {
			            showThisSubMenu = true;
			        }
			        if (!showThisSubMenu) { continue; } // No access right; skip this sub menu
			    // Display the visible sub menu
			    %>
			     <li><a id="irbPanel" href="<%=getHrefBySubtypeirbConfig(settings.getObjSubType())%>"><%=settings.getObjDispTxt()%></a></li>
				<%
					} // End of acctMenuList loop
				    %>
					
					</ul>
			 
				<% }
	 
	 if ("pre_rev_menu".equals(settingirbMenu.getObjSubType())) {
					 	%>
					<ul class=<%=colClass%>> 
					<li class="research"><h2><%=settingirbMenu.getObjDispTxt()%></h2></li>
					<%
					
					 ArrayList irbConfigList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "pre_rev_menu");
					
					for (int iY=0; iY<irbConfigList.size(); iY++) {
			        ObjectSettings settings = (ObjectSettings)irbConfigList.get(iY);
					if ("0".equals(settings.getObjVisible())) { continue; }
	
				    boolean showThisSubMenu = false;
			        if ("new_submission".equals(settings.getObjSubType())) {
			            showThisSubMenu =  true ;
			        } else if ("pi_area".equals(settings.getObjSubType())) {
			            showThisSubMenu = true;
					} else if ("under_review".equals(settings.getObjSubType())) {
			            showThisSubMenu = true;
					} else {
			            showThisSubMenu = true;
			        }
			        if (!showThisSubMenu) { continue; } // No access right; skip this sub menu
			    // Display the visible sub menu
			    %>
			     <li><a id="irbPanel" href="<%=getHrefBySubtypeStd(settings.getObjSubType())%>"><%=settings.getObjDispTxt()%></a></li>
				<%
					} // End of acctMenuList loop
				    %>
					</ul>
		<% }
	 
	 
	
	  if ("irb_review".equals(settingirbMenu.getObjSubType())) {
					 	%>
					<ul class=<%=colClass%>> 
					<li class="research"><a href='#'><%=settingirbMenu.getObjDispTxt()%></a></li>
					</ul>
		<% }
	 
	 
	 
	 
	  if ("irb_report".equals(settingirbMenu.getObjSubType())) {
					 	%>
					<ul class=<%=colClass%>> 
					<li class="research"><a href='#'><%=settingirbMenu.getObjDispTxt()%></a></li>
					</ul>
		<% }
	} 
	}
	} else if ("lib_menu".equals(thisTopObj.getObjSubType())) {
		
		if ( (calLib > 0 ) 
			 || (evLib > 0) 
			 || (((String.valueOf(formLibAppRight).compareTo("1") == 0) || (String.valueOf(patProfileAppRight).compareTo("1") == 0)) && formLibGrpRight > 0)
             || (((String.valueOf(fieldLibGrpRight).compareTo("1") == 0)|| (String.valueOf(patProfileAppRight).compareTo("1") == 0)) && fieldLibGrpRight > 0)
			)
		{%> <%-- YK 06Sep2011: Fixed for Bug#6867 	 --%>
		<li> 
			<a id="mgPanel" href="#"><%=thisTopObj.getObjDispTxt()%></a> 
			<div class="library">
			<%
			ArrayList libMenuList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "lib_menu");
			String colClass="left";
			int checkClass=0;
			for (int iX=0; iX<libMenuList.size(); iX++) {
				
		        ObjectSettings settingLibMenu = (ObjectSettings)libMenuList.get(iX);
				// If this sub menu is configured as invisible in DB, skip it
				if ("0".equals(settingLibMenu.getObjVisible())) { continue; }
				boolean showThisMenu = false;
				 if ("callib_menu".equals(settingLibMenu.getObjSubType())) {
					 showThisMenu = calLib > 0 ? true : false;
			        } else if ("evtlib_menu".equals(settingLibMenu.getObjSubType())) {
			        	showThisMenu = evLib > 0 ? true : false;
			        } else if ("formlib_menu".equals(settingLibMenu.getObjSubType())) {
			            if (((String.valueOf(formLibAppRight).compareTo("1") == 0) 
			                || (String.valueOf(patProfileAppRight).compareTo("1") == 0)) && formLibGrpRight > 0) {
			            	showThisMenu = true;
			            }
			        } else if ("fldlib_menu".equals(settingLibMenu.getObjSubType())) {
			            if (((String.valueOf(fieldLibGrpRight).compareTo("1") == 0) 
			                    || (String.valueOf(patProfileAppRight).compareTo("1") == 0)) && fieldLibGrpRight > 0) {
			            	showThisMenu = true;
			            }
			        } else {
			        	showThisMenu = true;
			        }
				if (!showThisMenu) { continue; } // No access right; skip this sub menu
				checkClass++;
				if ((checkClass%2)==0) {
						colClass="right";
				}else{
						colClass="left";
				}		
				 if ("callib_menu".equals(settingLibMenu.getObjSubType())) {
					 	%>
					<ul class=<%=colClass%>> 
					<li class="subheading"><h2><%=settingLibMenu.getObjDispTxt()%></h2></li>
					<%
					
					 ArrayList stdMenuList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "callib_menu");
					
					for (int iY=0; iY<stdMenuList.size(); iY++) {
			        ObjectSettings settings = (ObjectSettings)stdMenuList.get(iY);
					if ("0".equals(settings.getObjVisible())) { continue; }
					

				    boolean showThisSubMenu = false;
			        if ("srch_cal".equals(settings.getObjSubType())) {
			            showThisSubMenu = true;
			        } else if ("add_newcal".equals(settings.getObjSubType())) {
			        	 showThisSubMenu = true;
			        } else if ("newcal_type".equals(settings.getObjSubType())) {
			        	 showThisSubMenu = true;
			        } else if ("copy_cal".equals(settings.getObjSubType())) {
			        	 showThisSubMenu = true;
					} else {
			            showThisSubMenu = true;
			        }
			        if (!showThisSubMenu) { continue; } // No access right; skip this sub menu
			    // Display the visible sub menu
			    if ("newcal_type".equals(settings.getObjSubType())){
			    %>
			     <li><a id="mgPanel" href="#" onclick="javascript: if(f_check_perm(<%=calLib%>,'N')==true){window.open('<%=getHrefBySubtypeCalLib(settings.getObjSubType())%>','information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=600,height=300,top=250 ,left=200')}"><%=settings.getObjDispTxt()%></a></li>
				<%}else if ("srch_cal".equals(settings.getObjSubType())){
					%>
					<li><a id="mgPanel" href="<%=getHrefBySubtypeCalLib(settings.getObjSubType())%>" ><%=settings.getObjDispTxt()%></a></li>
					<%
				}else {
					%>
					<li><a id="mgPanel" href="<%=getHrefBySubtypeCalLib(settings.getObjSubType())%>" onclick="return f_check_perm(<%=calLib%>,'N')"><%=settings.getObjDispTxt()%></a></li>
					<%
						}
					} // End of acctMenuList loop
				    %>
					
					</ul>
				<%}else   if ("evtlib_menu".equals(settingLibMenu.getObjSubType())) {
					%>
					<ul class=<%=colClass%>> 
					<li class="subheading"><h2><%=settingLibMenu.getObjDispTxt()%></h2></li>
					<%
					ArrayList patMenuList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "evtlib_menu");
					
					for (int iZ=0; iZ<patMenuList.size(); iZ++) {
			            ObjectSettings settings = (ObjectSettings)patMenuList.get(iZ);
					   // If this sub menu is configured as invisible in DB, skip it
					   if ("0".equals(settings.getObjVisible())) { continue; }
					
				       // Figure out access rights of this sub menu
				       boolean showThisSubMenu = false;
					
					   if ("srch_evnt".equals(settings.getObjSubType())) {
						   showThisSubMenu = true;
					   } else if ("add_evnt".equals(settings.getObjSubType())) {
						   showThisSubMenu = true;
					   } else if ("evnt_cat".equals(settings.getObjSubType())) {
						   showThisSubMenu = true;
					   } else {
			                showThisSubMenu = true;
					   }
							if (!showThisSubMenu) { continue; } // No access right; skip this sub menu
					
				 if ("srch_evnt".equals(settings.getObjSubType())){
			    %>
			   		<li><a id="mgPanel" href="<%=getHrefBySubtypeEvntLib(settings.getObjSubType())%>" ><%=settings.getObjDispTxt()%></a></li>
				<%}else {
					%>
					<li><a id="mgPanel" href="<%=getHrefBySubtypeEvntLib(settings.getObjSubType())%>" onclick="return f_check_perm(<%=evLib%>,'N')"><%=settings.getObjDispTxt()%></a></li>
					<%
			     		}
					}
					%>
					</ul>
					<%
				}else   if ("formlib_menu".equals(settingLibMenu.getObjSubType())) {
					%>
					<ul class=<%=colClass%>> 
					<li class="subheading"><h2><%=settingLibMenu.getObjDispTxt()%></h2></li>
					<%
					ArrayList bgtMenuList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "formlib_menu");
					
					for (int iA=0; iA<bgtMenuList.size(); iA++) {
			            ObjectSettings settings = (ObjectSettings)bgtMenuList.get(iA);
					   // If this sub menu is configured as invisible in DB, skip it
					   if ("0".equals(settings.getObjVisible())) { continue; }
					
				       // Figure out access rights of this sub menu
				       boolean showThisSubMenu = false;
					
				       if ("srch_form".equals(settings.getObjSubType())) {
				    		
				    	   showThisSubMenu = true;
					  } else if ("add_newform".equals(settings.getObjSubType())) {
						  showThisSubMenu = true;
					  } else if ("form_type".equals(settings.getObjSubType())) {
						  showThisSubMenu = true;
					  } else if ("copy_form".equals(settings.getObjSubType())) {
						  showThisSubMenu = true;
					  } else {
			                showThisSubMenu = true;
					   }
							if (!showThisSubMenu) { continue; } // No access right; skip this sub menu
					if ("form_type".equals(settings.getObjSubType())){
					    %>
					     <li><a id="mgPanel" href="#" onclick="javascript:if(f_check_perm(<%=formLibGrpRight%>,'N')==true){window.open('<%=getHrefBySubtypeFormLib(settings.getObjSubType())%>','information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=600,height=300,top=250 ,left=200')}"><%=settings.getObjDispTxt()%></a></li>
					<%}else if ("srch_form".equals(settings.getObjSubType())){
							%>
							<li><a id="mgPanel" href="<%=getHrefBySubtypeFormLib(settings.getObjSubType())%>" ><%=settings.getObjDispTxt()%></a></li>
							<%
					  }else{
							%>
							<li><a id="mgPanel" href="<%=getHrefBySubtypeFormLib(settings.getObjSubType())%>" onclick="return f_check_perm(<%=formLibGrpRight%>,'N')"><%=settings.getObjDispTxt()%></a></li>
							<%
						}
					}
					%>
					</ul>
					<%
				}else   if ("fldlib_menu".equals(settingLibMenu.getObjSubType())) {
					%>
					<ul class=<%=colClass%>> 
					<li class="subheading"><h2><%=settingLibMenu.getObjDispTxt()%></h2></li>
					<%
					ArrayList bgtMenuList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "fldlib_menu");
					
					for (int iA=0; iA<bgtMenuList.size(); iA++) {
			            ObjectSettings settings = (ObjectSettings)bgtMenuList.get(iA);
					   // If this sub menu is configured as invisible in DB, skip it
					   if ("0".equals(settings.getObjVisible())) { continue; }
					
				       // Figure out access rights of this sub menu
				       boolean showThisSubMenu = false;
					
				       if ("srch_fld".equals(settings.getObjSubType())) {
				    	   showThisSubMenu = true;
					  } else if ("add_fld".equals(settings.getObjSubType())) {
						  showThisSubMenu = true;
					  } else if ("add_mulfld".equals(settings.getObjSubType())) {
						  showThisSubMenu = true;
					  } else if ("fld_cat".equals(settings.getObjSubType())) {
						  showThisSubMenu = true;
					  } else if ("copy_fld".equals(settings.getObjSubType())) {
						  showThisSubMenu = true;
					  } else {
			                showThisSubMenu = true;
					   }
							if (!showThisSubMenu) { continue; } // No access right; skip this sub menu
					if ("srch_fld".equals(settings.getObjSubType())){
					    %>
					     <li><a id="mgPanel" href="<%=getHrefBySubtypeFldLib(settings.getObjSubType())%>"><%=settings.getObjDispTxt()%></a></li>
					<%}else if ("copy_fld".equals(settings.getObjSubType())) {
							%>
						<li><a id="mgPanel" href="#" onclick="javascript:if(f_check_perm(<%=fieldLibGrpRight%>,'N')==true){window.open('<%=getHrefBySubtypeFldLib(settings.getObjSubType())%>','information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=870,height=600,top=50 ,left=100')}"><%=settings.getObjDispTxt()%></a></li>
							
							<%
					}else if ("fld_cat".equals(settings.getObjSubType())) {
							%>
						<li><a id="mgPanel" href="#" onclick="javascript:if(f_check_perm(<%=fieldLibGrpRight%>,'N')==true){window.open('<%=getHrefBySubtypeFldLib(settings.getObjSubType())%>','information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=600,height=300 , top=200 ,left=250')}"><%=settings.getObjDispTxt()%></a></li>
							
							<%
					}else if (("add_fld".equals(settings.getObjSubType())) || ("add_mulfld".equals(settings.getObjSubType()))) {
							%>
						<li><a id="mgPanel" href="#" onclick="javascript:if(f_check_perm(<%=fieldLibGrpRight%>,'N')==true){window.open('<%=getHrefBySubtypeFldLib(settings.getObjSubType())%>','information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=700,height=600,top=25 ,left=150')}"><%=settings.getObjDispTxt()%></a></li>
							
							<%
					}
					
					}
					%>
					</ul>
					<%
				}
				
			}%>
			</div>
		</li> 
		 <% }
	} else if ("inv_menu".equals(thisTopObj.getObjSubType())) {
		
		ArrayList invMenuList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "inv_menu");
		%>
		 <% if ( (String.valueOf(inventoryRight).compareTo("1") == 0)  && (specRight >=4 || storageRight >=4))
		{
		%>
		<li> 
			<a id="mgPanel" href="#"><%=thisTopObj.getObjDispTxt()%></a> 
			<div class="inventory"><ul>
		
	   <%
		 for (int iX=0; iX<invMenuList.size(); iX++) {
            ObjectSettings invsettings = (ObjectSettings)invMenuList.get(iX);
		   // If this sub menu is configured as invisible in DB, skip it
		   if ("0".equals(invsettings.getObjVisible())) { continue; }
	       // Figure out access rights of this sub menu
	       boolean showThisSubMenu = false;
			 if ("specimen_menu".equals(invsettings.getObjSubType())) {
				showThisSubMenu = specRight >= 4 ? true : false;
			} else if ("storage_menu".equals(invsettings.getObjSubType())) {
				showThisSubMenu = storageRight >= 4 ? true : false;
			} else if ("kit_menu".equals(invsettings.getObjSubType())) {
				showThisSubMenu = storageRight >= 4 ? true : false;
			} else if ("prep_menu".equals(invsettings.getObjSubType())) {
				//showThisSubMenu = storageRight >= 4 ? true : false;
				showThisSubMenu = specRight > 4 ? true : false;
			}else {
                showThisSubMenu = true;
			}
				if (!showThisSubMenu) { continue; } // No access right; skip this sub menu
			%>
			 <li><a id="mgPanel" href="<%=getHrefBySubtypeInv(invsettings.getObjSubType())%>"><%=invsettings.getObjDispTxt()%></a></li>
    <%
      } // End of invMenuList loop
	   %>
	      </ul></div> 
	  </li> <%
	}	
		
  	} else if ("ref_menu".equals(thisTopObj.getObjSubType())) {
  	  if ((String.valueOf(cdrpfRight).compareTo("1") == 0)){
  		if (!hasViewPermission(cbuReferencesMenuRght)) { continue; }
		ArrayList refMenuList = objCache.getAccountObjects(EJBUtil.stringToNum(accId),"ref_menu");
		%>
		 <% 
		{
		%>
		<li> 
			<a id="mgPanel" href="#"><%=thisTopObj.getObjDispTxt()%></a> 
			<div class="inventory"><ul>
		
	   <%
		 for (int iX=0; iX<refMenuList.size(); iX++) {
            ObjectSettings invsettings = (ObjectSettings)refMenuList.get(iX);
	       // Figure out access rights of this sub menu
	       boolean showThisSubMenu = false;
			 if (("lnk_sops".equals(invsettings.getObjSubType()))&& cbuLinkToSOPMenuRght >=4 ) {
				showThisSubMenu  =true;
			} else if (("lnk_mops".equals(invsettings.getObjSubType()))&& cbuLinkToMOPMenuRght >=4 ) {
				showThisSubMenu  =true;
			} else if (("usr_guide".equals(invsettings.getObjSubType()))&& cbuGuideMenuRght >= 4) {
				showThisSubMenu  =true;
			} else if (("help_lnk".equals(invsettings.getObjSubType()))&& cbuHelpfulLinkMenuRght >= 4) {
				showThisSubMenu  =true;
			}
			else if (("faq".equals(invsettings.getObjSubType()))&& cbuFAQMenuRght >=4) {
				
				showThisSubMenu  =false;
			}else {
                showThisSubMenu = false;
			}
				if (!showThisSubMenu) { continue; } // No access right; skip this sub menu
			%>
			 <li><a id="mgPanel" href="<%=getHrefBySubtypeRefrence(invsettings.getObjSubType())%>"><%=invsettings.getObjDispTxt()%></a></li>
    <%
      } // End of refMenuList loop
	   %>
	      </ul></div> 
	  </li> <%
		}	
  	  }
  	}
	else if ("cinfo_menu".equals(thisTopObj.getObjSubType())) {
	if ((String.valueOf(cdrpfRight).compareTo("1") == 0)){
		if (!hasViewPermission(cbuContactInfoMenuRght)) { continue; }
		ArrayList contactMenuList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "cinfo_menu");
		%>
		 <% 
		{
		%>
		<li> 
			<a id="mgPanel" href="#"><%=thisTopObj.getObjDispTxt()%></a> 
			<div class="inventory"><ul>
		
	   <%
		 for (int iX=0; iX<contactMenuList.size(); iX++) {
            ObjectSettings invsettings = (ObjectSettings)contactMenuList.get(iX);
	       // Figure out access rights of this sub menu
	       boolean showThisSubMenu = false;
			 if (("staff_dir".equals(invsettings.getObjSubType()))&& cbuStaffDirMenuRght >= 4) {
				showThisSubMenu  =true;
			} else if (("chat_help".equals(invsettings.getObjSubType()))) {
				showThisSubMenu  =false;
			} else if (("net_dir".equals(invsettings.getObjSubType()))&& cbuNetworkDirMenuRght>=4) {
				showThisSubMenu  =true;
			} else {
                showThisSubMenu = false;
			}
				if (!showThisSubMenu) { continue; } // No access right; skip this sub menu
			%>
			 <li><a id="mgPanel" href="<%=getHrefBySubtypeContacUs(invsettings.getObjSubType())%>"><%=invsettings.getObjDispTxt()%></a></li>
    <%
      } // End of contact menulist loop
	   %>
	      </ul></div> 
	  </li> <%
		}	
	 }
	}else if ("report_menu".equals(thisTopObj.getObjSubType())) {
		ArrayList reportMenu = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "report_menu");
		%>
		<li> 
			<a id="mgPanel" href="#"><%=thisTopObj.getObjDispTxt()%></a> 
			<div class="reports"><ul>
			<%
		 for (int iX=0; iX<reportMenu.size(); iX++) {
            ObjectSettings reportSettings = (ObjectSettings)reportMenu.get(iX);
		   // If this sub menu is configured as invisible in DB, skip it
		   if ("0".equals(reportSettings.getObjVisible())) { continue; }
	       // Figure out access rights of this sub menu
	       boolean showThisSubMenu = false;
	       if ("regreporting_menu".equals(reportSettings.getObjSubType())) {
			    // Start of Regulatory Reporting menus
			    // Get the regreporting_menu settings for this account from the cache
			    if(String.valueOf(regReportAppRight).compareTo("1") == 0 && ctrpGrpRight >= 4){
			    	ArrayList rrMenuList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "regreporting_menu");
			      %>
				<li class="subheading"><h2><%=reportSettings.getObjDispTxt()%></h2></li>
			      <%
			      for (int iY=0; iY<rrMenuList.size(); iY++) {
			        ObjectSettings settings = (ObjectSettings)rrMenuList.get(iY);
					// If this sub menu is configured as invisible in DB, skip it
					if ("0".equals(settings.getObjVisible())) { continue; }
				    // Figure out access rights of this sub menu
				    showThisSubMenu = false;
				    
				    if ("ctrp_reporting".equals(settings.getObjSubType()) && ctrpGrpRight >= 4)
				    {	
				       showThisSubMenu = true;
			        } 	 
		        	if (!showThisSubMenu) { continue; } // No access right; skip this sub menu
			        // Display the visible sub menu
			        %>
			        <li><a id="mgPanel" href="<%=getHrefBySubtypeForRegReporting(settings.getObjSubType())%>"><%=settings.getObjDispTxt()%></a></li>
			       <%
			      }
			  }
			}else {
				if((userReports > 0) || (String.valueOf(adhocqAppRight).compareTo("1") == 0 && adHocGrpRight >= 4) ||
						(eDashboardMod =='1' && dashGroupRight >= 4)) 
				{
					if ("rpt_menu".equals(reportSettings.getObjSubType())) {
						showThisSubMenu = userReports > 0 ? true : false;
					} else if ("adhoc_menu".equals(reportSettings.getObjSubType())) {
						showThisSubMenu = (String.valueOf(adhocqAppRight).compareTo("1") == 0 && adHocGrpRight >= 4) ? true : false; /*YK 05Sep2011 : Fixed for Bug #6839 & #6868*/
					} else if ("dash_menu".equals(reportSettings.getObjSubType())) {
		                showThisSubMenu = (eDashboardMod =='1' && dashGroupRight >= 4) ? true : false;
					}
					
				    else if("publishrep_menu".equals(reportSettings.getObjSubType())){
				       showThisSubMenu = true;  
				    }
				  
					if (!showThisSubMenu) { continue; } // No access right; skip this sub menu
					%>
					 <li><a id="mgPanel" href="<%=getHrefBySubtypeRep(reportSettings.getObjSubType())%>"><%=reportSettings.getObjDispTxt()%></a></li>
		    	<%}
			}
      } // End of reportMenu loop
	   %>
	      </ul></div> 
		
		</li> 
	<% 
	}else if ("dash_menu".equals(thisTopObj.getObjSubType())) {
		if (eDashboardMod =='1' && dashGroupRight >= 4)
		{
		%>
		<li> 
			<a id="mgPanel" href="dashboardcentral.jsp"><%=thisTopObj.getObjDispTxt()%></a> 
		</li> 
	<%
		}
  	}else if ("cbuentry_menu".equals(thisTopObj.getObjSubType())) {
  	  if ((String.valueOf(cdrpfRight).compareTo("1") == 0)){
  		if (!hasViewPermission(cbuCordEntryRight)) { continue; }
		{
		%>
		<li> 
			<a id="mgPanel" href="<%=getHrefBySubtypeCordEntry(thisTopObj.getObjSubType())%>"><%=thisTopObj.getObjDispTxt()%></a> 
		</li> 
	<%
		}
  	  }
  	}
		else if ("open_order".equals(thisTopObj.getObjSubType())) {
		if ((String.valueOf(cdrpfRight).compareTo("1") == 0)){
			if (!hasViewPermission(cbuUploadDocRight)) { continue; }
		{
		%>
		<li> 
			<a id="mgPanel" href="<%=getHrefBySubtypeUploadDoc(thisTopObj.getObjSubType())%>"><%=thisTopObj.getObjDispTxt()%></a> 
		</li> 
	<%
		}
	 }
  	}
	else if ("grid_menu".equals(thisTopObj.getObjSubType())) {
	    if (String.valueOf(a2aRight).compareTo("1") == 0 && A2ARight >= 4)
		{
		%>
		<li> 
			<a id="mgPanel" href="revSetup.jsp"><%=thisTopObj.getObjDispTxt()%></a> 
		</li> 

		<% }
	  }else if ("help_menu".equals(thisTopObj.getObjSubType())) {
	%>	   
		<li> 
			 
		</li> 
  <%
  }else if ("logout_menu".equals(thisTopObj.getObjSubType())) {
	  %>
<!-- 	 	<li> 
			<a id="mgPanel" href="logout.jsp"><%=thisTopObj.getObjDispTxt()%></a> 
		</li> --> 
	 <%
  }else if ("maint_menu".equals(thisTopObj.getObjSubType())) {
	  if ((String.valueOf(cdrpfRight).compareTo("1") == 0)){
	  if (!hasViewPermission(cbuMaintMenuRght)) { continue; }
		%>
	  	<li> 
			<a id="mgPanel" href="#"><%=thisTopObj.getObjDispTxt()%></a> 
			<div class="manage">
			<%
			 ArrayList maintMenu = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "maint_menu");
			String colClass="left";
			//int checkClass=0;
			for (int iX=0; iX<maintMenu.size(); iX++) {
				
		        ObjectSettings settingMaintMenu = (ObjectSettings)maintMenu.get(iX);
				// If this sub menu is configured as invisible in DB, skip it
				if ("0".equals(settingMaintMenu.getObjVisible())) { continue; }
				boolean showThisMenu = false;
				 if (("centr_manag".equals(settingMaintMenu.getObjSubType())) &&  cbuMaintMenuRght>=4) {
					 showThisMenu = true;
				} else if (("my_pro".equals(settingMaintMenu.getObjSubType())) && cbuMyProMenuRght>=4) {
					showThisMenu = true;
				} else {
					showThisMenu = true;
				}
				if (!showThisMenu) { continue; } // No access right; skip this sub menu
				/*checkClass++;
				if ((checkClass%2)==0) {
						colClass="right";
				}else{
						colClass="left";
				}*/		
				 if ("my_pro".equals(settingMaintMenu.getObjSubType())) {
					 	%>
					<ul class="left"> 
				
					<%
					
					ArrayList myProMenuList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "maint_menu");
					for (int iY=0; iY<myProMenuList.size(); iY++) {
			        ObjectSettings settings = (ObjectSettings)myProMenuList.get(iY);
			        
					if ("0".equals(settings.getObjVisible())) { continue; }
					
				    boolean showThisSubMenu = false;
					 if (("my_pro".equals(settings.getObjSubType())) && cbuMyProMenuRght>=4) {
							showThisSubMenu =true;
					   }  
					 if ("centr_manag".equals(settings.getObjSubType())) {
							showThisSubMenu =false;
					   }
						if (!showThisSubMenu) { continue; } // No access right; skip this sub menu
					 %>
					  <li><a id="mgPanel" href="<%=getHrefBySubtypeUserPro(settings.getObjSubType())%>"><%=settings.getObjDispTxt()%></a></li>
					
					<%
			        
					} // End of acctMenuList loop
				    %>
					
					</ul>
				<%}else   if ("centr_manag".equals(settingMaintMenu.getObjSubType())) {
					%>
					<ul class=<%=colClass%>> 
					<li class="subheading"><h2><%=settingMaintMenu.getObjDispTxt()%></h2></li>
					<%
				
					ArrayList centreManageMenuList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "centr_manag");
					for (int iZ=0; iZ<centreManageMenuList.size(); iZ++) {
			            ObjectSettings settings = (ObjectSettings)centreManageMenuList.get(iZ);
					   // If this sub menu is configured as invisible in DB, skip it
					   if ("0".equals(settings.getObjVisible())) { continue; }
					
				       // Figure out access rights of this sub menu
				       boolean showThisSubMenu = false;
					if (("cbb_profile".equals(settings.getObjSubType()))&& cbuCBBProMenuRght>=4) {
			            showThisSubMenu = true;    
			        } else if (("proc_proced".equals(settings.getObjSubType()))&& cbuProcProcedureMenuRght>=4 ){
			            showThisSubMenu = true;
					} else {
			            showThisSubMenu = false;
			        }
			       if (!showThisSubMenu) { continue; } // No access right; skip this sub menu
			    // Display the visible sub menu
			    %>
			     <li><a id="mgPanel" href="<%=getHrefBySubtypeCentreMange(settings.getObjSubType())%>"><%=settings.getObjDispTxt()%></a></li>
				<%
					}
					%>
					</ul>
					<%
				}
				
			}%>
			</div>
		</li> 
	 <%}
	}else if ("pf_menu".equals(thisTopObj.getObjSubType())) {
	  ArrayList pfMenuList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "pf_menu");
	  if ((String.valueOf(cdrpfRight).compareTo("1") == 0)){
	  %>
	 	<li> 
			<a id="mgPanel" href="#"><%=thisTopObj.getObjDispTxt()%></a>
			<div class="inventory">
			<ul>
			<%
		 	for (int iX=0; iX<pfMenuList.size(); iX++) {
            	ObjectSettings pfSettings = (ObjectSettings)pfMenuList.get(iX);
            	// If this sub menu is configured as invisible in DB, skip it
		        if ("0".equals(pfSettings.getObjVisible())) { continue; }
	            // Figure out access rights of this sub menu
	             boolean showThisSubMenu = false;
			     if ("opn_task".equals(pfSettings.getObjSubType())) {
			    	 showThisSubMenu = true;
			     } 
			if (!showThisSubMenu) { continue; } // No access right; skip this sub menu
			%>
			 <li><a id="mgPanel" href="<%=getHrefBySubtypePF(pfSettings.getObjSubType())%>"><%=pfSettings.getObjDispTxt()%></a></li>
    <%
      } // End of reportMenu loop
	   %>
	      </ul></div>
	    </li>
	<% 
	  }
  	}else if ("funding_menu".equals(thisTopObj.getObjSubType())) {
  		 if ((String.valueOf(cdrpfRight).compareTo("1") == 0)){
  		if (!hasViewPermission(cbuFundingMenuRght)) { continue; }
		%>
		<li> 
			<a id="mgPanel" href="<%=getHrefBySubtypeFunding(thisTopObj.getObjSubType())%>"><%=thisTopObj.getObjDispTxt()%></a> 
		</li> 
	<%
  		 }
 
  	}else if ("pending_ordr".equals(thisTopObj.getObjSubType())) {
  		 if ((String.valueOf(cdrpfRight).compareTo("1") == 0)){
  		if (!hasViewPermission(cbuPendingReqMenuRght)) { continue; }
		%>
		<li> 
			<a id="mgPanel" href="<%=getHrefBySubtypePendingReq(thisTopObj.getObjSubType())%>"><%=thisTopObj.getObjDispTxt()%></a> 
		</li> 
	<%
  		 }
 
  	}else if ("cbu_tools_menu".equals(thisTopObj.getObjSubType())) {
  	  if ((String.valueOf(cdrpfRight).compareTo("1") == 0)){
	  ArrayList cbuTollsMenuList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "cbu_tools_menu");
	
	  if (!hasViewPermission(cbuToolsMenuRght)) { continue; }
	  %>
	 	<li> 
			<a id="mgPanel" href="#"><%=thisTopObj.getObjDispTxt()%></a>
			<div class="inventory">
			<ul>
			<%
		 	for (int iX=0; iX<cbuTollsMenuList.size(); iX++) {
            	ObjectSettings cbuToolSettings = (ObjectSettings)cbuTollsMenuList.get(iX);
            	// If this sub menu is configured as invisible in DB, skip it
		        if ("0".equals(cbuToolSettings.getObjVisible())) { continue; }
	            // Figure out access rights of this sub menu
	             boolean showThisSubMenu = false;
			     if (("app_msg_menu".equals(cbuToolSettings.getObjSubType()))&& cbuAppMssgsMenuRght >= 4 ){
			    	 showThisSubMenu = true;
			     } else if ("creat_data_req".equals(cbuToolSettings.getObjSubType())) {
			            showThisSubMenu = true;
				 }  
			     else if ("view_data_req".equals(cbuToolSettings.getObjSubType())) {
			            showThisSubMenu = true;
				 }   
			     else if ("mng_ling_menu".equals(cbuToolSettings.getObjSubType())) {
			            showThisSubMenu = true;
				 }  
			     else {
			            showThisSubMenu = false;
			        }
			if (!showThisSubMenu) { continue; } // No access right; skip this sub menu
			%>
			 <li><a id="mgPanel" href="<%=getHrefBySubtypeCbuTools(cbuToolSettings.getObjSubType())%>"><%=cbuToolSettings.getObjDispTxt()%></a></li>
    <%
      } // End of reportMenu loop
	   %>
	      </ul></div>
	    </li>
	<% }
  	}
}
%>	
</ul> <!-- Main UL ends for Mega Menu Panel-->
</td>
<%
	String logoName = "../images/acclogo/account_" + accName + ".jpg";
%>
	<td align="right" nowrap>
		<div id = "logo">
	<% if (!accName.equals("default")){%>
			<img src=<%=logoName%> align="absbottom">
	<%}%>
		</div>
	</td>
<%
	String uName = (String) tSession.getValue("userName");
%>
	<td align="right" class= "grayComments" valign="middle" nowrap>
		<%= StringUtil.htmlEncodeXss(uName) %>
		&nbsp;|&nbsp;
		<a id="homePage" href="myHome.jsp?srcmenu=tdMenuBarItem1">
			<img style="padding-top:5px; border:none; valign:bottom" src="../images/home.png" title="<%=LC.L_HomePage%>"/></a>
		&nbsp;|&nbsp;
		<a target="_blank" id="help" href="./help/Velos_eResearch_User_Manual.pdf">
			<img style="padding-top:5px; border:none; valign:bottom;" src="../images/help.png" title="<%=LC.L_UserManual%>" /></a>
		&nbsp;|&nbsp;
		<a id="logOut" href="logout.jsp"><img style="padding-top:5px; border:none; valign:bottom" src="../images/SignOff.png" title="<%=LC.L_Logout%>"/></a>
	</td>
</tr>
</table></body>
