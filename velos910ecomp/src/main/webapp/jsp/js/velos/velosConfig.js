/* 
 * Date: Jan-18-2011
 * 
 * List of all configurable Javascript data and functions going forward.
 * This list contains the configurable switches with the default value for each switch.
 * To override a default value, copy the exact line or block to 'velosConfigCustom.js' file and
 * customize the values there. DO NOT CUSTOMIZE ANYTHING IN THIS FILE (velosConfig.js).
 * 
 * Note that this is a site-wide (not an account-wide) configuration. 
 * All these data will be applied to all accounts in a shared/ASP environment.
 * 
 * Developers: Organize all data by functional categories and make sure to add sufficient documentation.
 */


/** Category: Patient Schedule **/
/** 
 *  PAT_SCHED_EDIT_COVERAGE_MANDATORY: In the Edit Visit dialog of Patient Schedule, make the Coverage type
 *  entry for an event mandatory or optional using this variable.
 *  Possible values:
 *    1: Makes Coverage Type entry mandatory for an event that is being edited.
 *    0: Makes Coverage Type entry optional.
 *  By default, it is optional.
 */
var PAT_SCHED_EDIT_COVERAGE_MANDATORY = 0;


/** Category: Patient Study Status **/
/** 
 *  VAL_PATSTUDYSTAT_RESTRICT_FUTURE_STATDATE: The switch to restrict Patient Study Status Date from
 *  being a future date. 
 *  Possible values:
 *    1: A validation check will be applied
 *    0: A validation check will not be applied
 *  By default, this validation will be turned off
 */
var VAL_PATSTUDYSTAT_RESTRICT_FUTURE_STATDATE = 0;

/** Category: Date Format **/
/** 
 *  YEAR_UPPER_BOUND : upper bound used in date picker.It is added to the current year to set the upper
 *  limit of year range. 
 */
var  YEAR_UPPER_BOUND = 100;

/** Category: Date Format **/
/** 
 *  YEAR_LOWER_BOUND : lower bound used in date picker.It is subtracted from the current year to set the lower
 *  limit of year range. 
 */
var  YEAR_LOWER_BOUND = 100;

// --- End of configurable items -- Do not delete or override below block ---
function includeCustomJS(file) {
  var script  = document.createElement('script');
  script.src  = file;
  script.type = 'text/javascript';
  script.defer = true;
  document.getElementsByTagName('head').item(0).appendChild(script);
}
includeCustomJS('js/velos/velosConfigCustom.js');
