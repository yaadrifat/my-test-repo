/*
* File Name 	: calMilestoneSetup.js 
* Created on	: 2/24/11
* Created By	: YogeshKumar/AshuKhatkar
* Enhancement	: 8_10.0DFIV12 
* Purpose		: Validation and display of Event-Visit Grid to create Milestones.
*
*
*/
function includeJS(file)
{

  var script  = document.createElement('script');
  script.src  = file;
  script.type = 'text/javascript';
  script.defer = true;

  document.getElementsByTagName('head').item(0).appendChild(script);

}

/* include any js files here */

// Introduced for localization
includeJS('js/velos/velosUtil.js');
var calMileStoneGridChangeList = null;
var visitLabelArray = null;
var visitDisplacementArray = null;
var eventLabelArray = null;
var incomingUrlParams = null;
var calStatus = null;
var lastTimeSubmitted = 0;
var horizontalPage = 0;
var pgRight=0 // YK 10Mar2011 Bug#5905
var eventNameArray = null;//AK: Implemented enh 'D-FIN12 calendar status amendment (5th April 11).
var ErrGetResp_CnctVelos=M_ErrGetResp_CnctVelos;
var Create_Milestone=L_Create_Milestone;
var CommErr_CnctVelos=M_CommErr_CnctVelos;
var PleaseWait_Dots=L_PleaseWait_Dots;
var CreateVstsOrEvts_Mstone=M_CreateVstsOrEvts_Mstone;
var ThereNoEvt_SelMstone=M_ThereNoEvt_SelMstone;
var LoadingPlsWait=L_LoadingPlsWait;
var Event_Name=L_Event_Name;
var PlsEnterEsign=M_PlsEnterEsign;
var Total_Chg=L_Total_Chg;
var Esignature=L_Esignature;
var Invalid_Esign=L_Invalid_Esign;
var Valid_Esign=L_Valid_Esign;
var Evt_Selc=L_Evt_Selc;
var Vst_Selc=L_Vst_Selc;
var Visit_Name=L_Visit_Name;
var LEvent=L_Event;

// Define VELOS.dataGrid object. "VELOS" is a namespace so that the object can be called globally
VELOS.calMileStoneGrid = function (url, args) {
	pgRight = parseInt(YAHOO.util.Dom.get("pageRight").value); // YK 10Mar2011 Bug#5905
	this.url = url;  // save it in this.url; later it will be sent in startRequest()
	incomingUrlParams = args.urlParams; // this will be sent to updateEvtVisits.jsp
	if (incomingUrlParams) {
		incomingUrlParams = incomingUrlParams.replace("'", "\"");
	}
	
	showPanel(); // Show the wait panel; this will be hidden later
	
	// Define handleSuccess() of VELOS.calMileStoneGrid object. This will be added to the callback object to be
	// used when the call is processed successfully.
	this.handleSuccess = function(o) {
		/* Response Object Fields:
		 * o.tId, o.status, o.statusText, o.getResponseHeader[],
		 * o.getAllResponseHeaders, o.responseText, o.responseXML, o.argument
		 */
        this.dataTable = args.dataTable ? args.dataTable : 'serverpagination'; // name of the div to hold calMileStoneGrid
		var respJ = null; // response in JSON format
		try {
			respJ = $J.parse(o.responseText);
			} catch(e) {
			alert(ErrGetResp_CnctVelos);/*alert('Error getting response from server. Please contact Velos Support.');*****/
			return;
		}
		if (respJ.error < 0) {
			
			var paramArray = [respJ.errorMsg];
			alert(getLocalizedMessageString("L_Error_Msg", paramArray));/*alert('Error Msg: '+respJ.errorMsg);*****/
			return;
		}
		
		var myFieldArray = [];
		var myColumnDefs = [];
		if (respJ.colArray) {
			myColumnDefs = respJ.colArray;
			for (var iX=0; iX<myColumnDefs.length; iX++) {
					myColumnDefs[iX].width = myColumnDefs[iX].key == 'event' ? 200: 100;
				myColumnDefs[iX].resizeable = true;
				myColumnDefs[iX].sortable = false;
				var fieldElem = [];
				
				if (myColumnDefs[iX].key && myColumnDefs[iX].key.match(/^v[0-9]+$/)) {
					
					myColumnDefs[iX].formatter = function(el, oRecord, oColumn, oData) {
					  if(oData!=null){//YK :: DFIN12 BUG #5954
						if(oData.indexOf('v')>0 ){
						el.innerHTML = '<input class="yui-dt-checkbox"  type="checkbox" onmouseover="return overlib(\' <b>'+Event_Name/*Event Name*****/+': </b>'+eventNameArray[oData.slice(0,oData.indexOf('v'))]+'<br><b> '+Visit_Name/*Visit Name*****/+': </b> '+visitLabelArray[oData.slice(oData.indexOf('v'))] +'\',CAPTION,\''+LEvent/*Event*****/+'&#47'+Visit_Name/*Visit Name*****/+'\');"  onmouseout="nd();" name="'+oData+'"/>';//AK: Implemented enh 'D-FIN12 calendar status amendment (5th April 11).
						oRecord.setData.oColumn= el.innerHTML;
						 }
					  }
						
					};
			   } 
				
				if (myColumnDefs[iX].key == 'eventId') {
						myColumnDefs[iX].hidden = true;
				   }
				fieldElem['key'] = myColumnDefs[iX].key; 
				myFieldArray.push(fieldElem);
				
			}
		}
		visitLabelArray = [];
		for (var iX=0; iX<myColumnDefs.length; iX++) {
			if (!myColumnDefs[iX].label) { continue; }
			if (myColumnDefs[iX].key == 'event' || myColumnDefs[iX].key == 'eventId'|| myColumnDefs[iX].key == 'selectAll') { continue; }
			var inputPos = myColumnDefs[iX].label.indexOf(" <input type");
			visitLabelArray[myColumnDefs[iX].key] = myColumnDefs[iX].label.slice(0, inputPos);
			
			
		}
		
		
		visitDisplacementArray = [];
		
		if (respJ.displacements) {
			var myDisplacements = [];
			myDisplacements = respJ.displacements;
			for (var iX=0; iX<myDisplacements.length; iX++) {
				visitDisplacementArray[myDisplacements[iX].key] = myDisplacements[iX].value;
			
			}
		}
//		AK: Implemented enh 'D-FIN12 calendar status amendment (5th April 11).
		eventNameArray = [];
		if (respJ.eventNames) {
			var myEventNames = [];
			myEventNames = respJ.eventNames;
			for (var iX=0; iX<myEventNames.length; iX++) {
				eventNameArray[myEventNames[iX].key] = myEventNames[iX].value;
			}
		}
	

		var myDataSource = new YAHOO.util.DataSource(respJ.dataArray);
		myDataSource.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
		myDataSource.responseSchema = {
		fields: myFieldArray
		};
			
		var maxWidth = 800;
		var maxHeight = 800;

		if (screen.availWidth >= 1000 && screen.availWidth < 1100) { maxWidth = 800; }
		else if (screen.availWidth >= 1100 && screen.availWidth < 1290) { maxWidth = 1000; }
		
		if (screen.availHeight >= 700 && screen.availHeight < 900) { maxHeight = 320; }
		else if (screen.availHeight >= 900 && screen.availHeight < 1000) { maxHeight = 480; }
		
		var calcHeight = respJ.dataArray.length*40 + 40;
		if (calcHeight > maxHeight) { calcHeight = maxHeight; }
		
		var myDataTable = new YAHOO.widget.DataTable(
			this.dataTable,
			myColumnDefs, myDataSource,
			{
				width:maxWidth+"px",
				height:calcHeight+"px",
				//caption:"DataTable Caption", 
				scrollable:true 
			}
		);
		
		eventIdArray = [];
		eventLabelArray = [];
		eventSelectArray = [];
		var eventIdCells = $D.getElementsByClassName('yui-dt-col-eventId', 'td', myDataTable.getTableEl());
		for (var iX=0; iX<eventIdCells.length; iX++) {
			var el = new YAHOO.util.Element(eventIdCells[iX]);
			var parent = new YAHOO.util.Element($D.getAncestorByTagName(el, 'tr'));
			var eventIdTd = parent.getElementsByClassName('yui-dt-col-eventId', 'td')[0];
			var eventIdEl = new YAHOO.util.Element(eventIdTd);
			var eventId = eventIdEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
			eventIdArray.push(eventId);
			var eventSelectIDTD = parent.getElementsByClassName('yui-dt-col-selectAll', 'td')[0];
			var eventSelectIdEl = new YAHOO.util.Element(eventSelectIDTD);
			var eventSelectId = eventSelectIdEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
			eventSelectArray.push(eventSelectId);
			var eventTd = parent.getElementsByClassName('yui-dt-col-event', 'td')[0];
			var eventEl = new YAHOO.util.Element(eventTd);
			var eventName = eventEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
			eventLabelArray['e'+eventId] = eventName;
			var parentId = parent.get('id');
			eventEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML
			= eventEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
			//AK: Implemented enh 'D-FIN12 calendar status amendment (5th April 11).
			eventTd.innerHTML='<span onmouseover="return overlib(\''+eventName+'\',CAPTION,\''+Event_Name/*Event Name*****/+'\')" >'+eventTd.innerHTML+'</span>'

			
			// Added here to Disable the Check box
		
				var cells = $D.getChildren($(parentId));
				for (var iZ=0; iZ<cells.length; iZ++) {
					if (!cells[iZ].className) { continue; }
					var pattern = /\byui-dt-col-v[0-9]+\b/;
					var cName = cells[iZ].className.match(pattern);
					if (!cName) { 
						eventSelectIdEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML='<input type="checkbox" name="all_e'+eventId+'" id="all_e'+eventId+'"'+
						' onclick="VELOS.calMileStoneGrid.eventAll(\''+parentId+'\','+eventId+');" disabled /> '; 
						continue; } /*YK 04MAY2011 Bug# 6197*/
					var vId = (''+cName).slice(11); // To skip 'yui-dt-col-'
					var el = new YAHOO.util.Element(cells[iZ]);
					var input = el.getElementsByClassName('yui-dt-checkbox', 'input')[0];
					if (input==undefined) 
					{
						eventSelectIdEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML='<input type="checkbox" name="all_e'+eventId+'" id="all_e'+eventId+'"'+
						' onclick="VELOS.calMileStoneGrid.eventAll(\''+parentId+'\','+eventId+');" disabled /> ';
						// YK 10Mar2011 removed for Bug#5915
					}else
					{
						eventSelectIdEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML='<input type="checkbox" name="all_e'+eventId+'" id="all_e'+eventId+'"'+
						' onclick="VELOS.calMileStoneGrid.eventAll(\''+parentId+'\','+eventId+');" /> ';
						break; // YK 10Mar2011 Bug#5915
					}
								
				}
				for (var iS=0; iS<cells.length; iS++) {
					if (!cells[iS].className) { continue; }
					var pattern = /\byui-dt-col-v[0-9]+\b/;
					var cName = cells[iS].className.match(pattern);
					if (!cName) { continue; }
					var vId = (''+cName).slice(11); // To skip 'yui-dt-col-'
					var el = new YAHOO.util.Element(cells[iS]);
					var input = el.getElementsByClassName('yui-dt-checkbox', 'input')[0];
					if (input!=undefined) 
					{
						var nameEvent=input.name;
						var eventIds=input.name.slice(0,input.name.indexOf('v'));
						eventLabelArray[eventIds] = eventName;
					}
					
				}
			
			
			// Added here to Disable the Check box
		
		  
			
		}
		
		calMileStoneGridChangeList = [];
		myDataTable.subscribe("checkboxClickEvent", function(oArgs){
			var elCheckbox = oArgs.target;
			var oRecord = this.getRecord(elCheckbox);
			var column = this.getColumn(elCheckbox);
			var oKey = elCheckbox.name;//YK :: DFIN12 BUG #5954
			oRecord.setData(oKey, elCheckbox.checked);
			if (calMileStoneGridChangeList[oKey] == undefined) {
				calMileStoneGridChangeList[oKey] = elCheckbox.checked;
			} else {
				calMileStoneGridChangeList[oKey] = undefined;
			}
		});
		
      	myDataTable.subscribe("rowMouseoverEvent", function(oArgs) {
			myDataTable.onEventHighlightRow(oArgs);
			});
		myDataTable.subscribe("rowMouseoutEvent", function(oArgs) {
			myDataTable.onEventUnhighlightRow(oArgs);
			return nd();
		});
		myDataTable.subscribe("rowClickEvent", myDataTable.onEventSelectRow);
		myDataTable.focus();   


		if (!$('save_changes')) {
			var dt = $(this.dataTable);
			var saveDiv = document.createElement('div');
			saveDiv.innerHTML = "<br/><table border=0 width='100%'><tbody><tr align='left'>" +
				"<td width='350'></td>"+
				"<td align='left'><input onclick='VELOS.calMileStoneGrid.saveDialog();' type='button' id='save_changes' name='save_changes' value='"+Create_Milestone/*Create Milestone*****/+"'/></td>"+
				"</td></tr></tbody></table>";
			$D.insertAfter(saveDiv, dt);
		}
		
		calStatus = respJ.calStatus;
		var cells = null;
		if (calStatus == 'D' ) {//AK: Implemented enh 'D-FIN12 calendar status amendment (5th April 11).
			var the_inputs=document.getElementsByTagName("input");
			for(var n=0;n<the_inputs.length;n++){
				if(the_inputs[n].type=="checkbox"){
					the_inputs[n].disabled =true;
					}
			}
			
			if ($('save_changes')) { $('save_changes').disabled = true; }
		}
		
		hidePanel();
		
	};
	this.handleFailure = function(o) {
		alert(CommErr_CnctVelos);/*alert('Communication Error. Please contact Velos Support');*****/
		hidePanel();
	};
	this.cArgs = [];
	this.callback = {
		success:this.handleSuccess,
		failure:this.handleFailure,
		argument:this.cArgs
	};
	this.initConfigs(args);
}

VELOS.calMileStoneGrid.fnOnceEnterKeyPress = function(e) {
	try {
        if (e.keyCode == 13 || e.keyCode == 10) {
			if ('' == document.getElementById('eSignMessage').innerHTML) {
        		alert(PlsEnterEsign);/*alert('Please enter e-Signature.');*****/
        		return false;
			}
        	if (Valid_Esign != document.getElementById('eSignMessage').innerHTML) {/*if ('Valid e-Sign' != document.getElementById('eSignMessage').innerHTML) {*****/
        		alert(document.getElementById('eSignMessage').innerHTML);
        		return false;
        	}
            var thisTimeSubmitted = new Date();
            if (!lastTimeSubmitted) { return true; }
            if (!thisTimeSubmitted) { return true; }
            if ( thisTimeSubmitted - lastTimeSubmitted <= 1000 ) {
                return false;
            }
            lastTimeSubmitted = thisTimeSubmitted;
        }
	} catch(e) {}
	return true;
}
VELOS.calMileStoneGrid.checkAll = function(vId) {
//AK: Implemented enh 'D-FIN12 calendar status amendment (5th April 11).
	oKey = vId;
	if(document.getElementById('all_'+vId).checked){
		if (calMileStoneGridChangeList[oKey] == undefined) {
			calMileStoneGridChangeList[oKey] = document.getElementById('all_'+vId).checked;
		} else {
			calMileStoneGridChangeList[oKey] = undefined;
		}	
	}
}
VELOS.calMileStoneGrid.eventAll = function(rId, eId) {
	var cells = $D.getChildren($(rId));
	for (var iX=0; iX<cells.length; iX++) {
		if (!cells[iX].className) { continue; }
		var pattern = /\byui-dt-col-v[0-9]+\b/;
		var cName = cells[iX].className.match(pattern);
		if (!cName) { continue; }
		var vId = (''+cName).slice(11); // To skip 'yui-dt-col-'
		var el = new YAHOO.util.Element(cells[iX]);
		var input = el.getElementsByClassName('yui-dt-checkbox', 'input')[0];
		if (input==undefined) { continue; }
		if (input.checked != $('all_e'+eId).checked) {
			var oKey = input.name;//YK :: DFIN12 BUG #5954
			if (calMileStoneGridChangeList[oKey] == undefined) {
				calMileStoneGridChangeList[oKey] = $('all_e'+eId).checked;
			} else {
				calMileStoneGridChangeList[oKey] = undefined;
			}
			input.checked = $('all_e'+eId).checked;
		}
	}
}
VELOS.calMileStoneGrid.saveDialog = function() {
	if (f_check_perm(pgRight,'N')) {// YK 10Mar2011 Bug#5905
	if (!$('saveDialog')) {
		var saveDialog = document.createElement('div');
		saveDialog.setAttribute('id', 'saveDialog');
		saveDialog.innerHTML  = '<div class="hd" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt; width=\"23em\"">'+CreateVstsOrEvts_Mstone/*Create Visit(s)/Event(s) Milestones*****/+'</div>';
		saveDialog.innerHTML += '<div class="bd" id="insertForm" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt; overflow: visible;"></div>';
		$D.insertAfter(saveDialog, $('save_changes'));
	}
	
	var noChange = true;
	var numChanges = 0;
	var oldVisit="";
	var maxChangesDisplayedBeforeScroll = 25;
	$('insertForm').innerHTML = '<form id="dialogForm" method="POST" action="createCalMileStone.jsp?'+incomingUrlParams+'"><div id="insertFormContent"></div></form>';
	for (oKey in calMileStoneGridChangeList) {
		var visitSel='N'; /*YK 19APR Bug#5967 ReOpen*/
		if(oKey.match(/^v[0-9]+$/)!=null)		// YK 24Mar2011 Bug#5967 -- YK 10Mar2011 Bug#5911 
		{
			if($('all_'+oKey.slice(oKey.indexOf('v'))).checked)
			{
				visitSel=oKey.slice(oKey.indexOf('v')+1);
			}
		}
			if (oKey.match(/^e[0-9]+v[0-9]+$/) && calMileStoneGridChangeList[oKey] != undefined ) {
	    	noChange = false;
	    	var visitName=visitLabelArray[oKey.slice(oKey.indexOf('v'))]; // YK 24Mar2011 Removed Un-necessary Code
			var eventName = eventLabelArray[oKey.slice(0, oKey.indexOf('v'))];
			$('insertFormContent').innerHTML += visitName+' / '+eventName+' => <b>'+Evt_Selc/*Event S</b>elected*****/+'<br/>';
			numChanges++;
			var opVal = (calMileStoneGridChangeList[oKey]?'A':'D')+'|'+oKey+'|'+
				visitDisplacementArray[oKey.slice(oKey.indexOf('v'))]+'|'+eventName;
			$('insertFormContent').innerHTML += 
				'<input type="hidden" name="ops" value="'+opVal+'" /><input type="hidden" name="visitSel" value="'+visitSel+'" />';
			}
	    	if ( visitSel!='N'){
	  		noChange = false;
	  			
				var visitName=visitLabelArray[oKey.slice(oKey.indexOf('v'))];// YK 24Mar2011 Removed Un-necessary Code
				if(visitName!=undefined && visitName!=oldVisit ){
				$('insertFormContent').innerHTML += visitName+'=> <b>'+Vst_Selc/*Visit S</b>elected*****/+'<br/>';
				numChanges++;
				var opVal = 'A'+'|v'+visitSel+'|'+
				visitDisplacementArray[oKey.slice(oKey.indexOf('v'))]+'|'+visitName;
				$('insertFormContent').innerHTML += 
				'<input type="hidden" name="ops" value="'+opVal+'" /><input type="hidden" name="visitSel" value="'+visitSel+'" />';
				}
				oldVisit=visitName;
	    	}
	}

	if (numChanges < maxChangesDisplayedBeforeScroll) {
		$('insertForm').style.width = null;
		$('insertForm').style.height = null;
		$('insertForm').style.overflow = 'visible';
	} else {
		$('insertForm').style.width = '51em';
		$('insertForm').style.height = '35em';
		$('insertForm').style.overflow = 'scroll';
	}
	if(numChanges==0)
	{
		noChange = true;
	}
	if (noChange) {
		$('insertFormContent').innerHTML += '<table width="250px"><tr><td style="font-size:8pt;">&nbsp;&nbsp;'+ThereNoEvt_SelMstone/*There are no Event(s) or Visit(s) selected to Create Milestone.*****/+'&nbsp;&nbsp;</td></tr></table>';
		noChange = true;
	} else {
		$('insertFormContent').innerHTML += Total_Chg/*Total changes*****/+': '+numChanges+'<br/><table><tr><td width="150px"><span id="eSignMessage" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;"></span></td>'+
			'<td style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;">'+Esignature/*e-Signature*****/+' <FONT class="Mandatory">*</FONT>&nbsp</td>'+
			'<td><input type="password" name="eSign" id="eSign" maxlength="8" '+
			' onkeyup="ajaxvalidate(\'misc:\'+this.id,4,\'eSignMessage\',\''+Valid_Esign/*Valid e-Sign*****/+'\',\''+Invalid_Esign/*Invalid e-Sign*****/+'\',\'sessUserId\')" '+
			' onkeypress="return VELOS.calMileStoneGrid.fnOnceEnterKeyPress(event)" '+
			' />&nbsp;</td></tr></table>';
	}
		
	var myDialog = new YAHOO.widget.Dialog('saveDialog', 
			{
				visible:false, fixedcenter:true, modal:true, resizeable:true,
				draggable:"true", autofillheight:"body", constraintoviewport:false
			});
	var handleCancel = function(e) {
		myDialog.cancel();
	};
	var handleSubmit = function(e) {
		try {
			if ('' == document.getElementById('eSignMessage').innerHTML) {
        		alert(PlsEnterEsign);/*alert('Please enter e-Signature.');*****/
        		return false;
			}
			
        	if (Valid_Esign != document.getElementById('eSignMessage').innerHTML) {/*if ('Valid e-Sign' != document.getElementById('eSignMessage').innerHTML) {*****/
        		alert(document.getElementById('eSignMessage').innerHTML);
        		return false;
        	}
        	var thisTimeSubmitted = new Date();
	        if (lastTimeSubmitted && thisTimeSubmitted) {
	        	if ( thisTimeSubmitted - lastTimeSubmitted <= 1000 ) { return false; }
		        lastTimeSubmitted = thisTimeSubmitted;
	        }
		} catch(e) {}
		myDialog.submit();
	};
	var onButtonsReady = function() {
	    if (!$E.getListeners($('dialog_submit'))) { $E.addListener($('dialog_submit'), 'click', handleSubmit, false, true); }
	    if (!$E.getListeners($('dialog_cancel'))) { $E.addListener($('dialog_cancel'), 'click', handleCancel); }
	}
	YAHOO.util.Event.onContentReady("saveDialog", onButtonsReady);
	var myButtons = noChange ? 
		[   { text: getLocalizedMessageString("L_Close")/*Close*****/, handler: handleCancel } ] :
		[
			{ text: getLocalizedMessageString('L_Save')/*Save*****/,   handler: handleSubmit },
			{ text: getLocalizedMessageString('L_Cancel')/*Cancel*****/, handler: handleCancel }
		];
	var onSuccess = function(o) {
		hideTransitPanel();
		var respJ = null; // response in JSON format
		try {
			respJ = $J.parse(o.responseText);
		} catch(e) {
			// Log error here
			return;
		}
		if (respJ.result == 0) {
				showFlashPanel(respJ.resultMsg);
			if (window.parent.reloadDataGrid) { setTimeout('window.parent.reloadDataGrid();', 4000); }   // Akshi 3Nov2011 Bug# 7455
		} else {
			var paramArray = [respJ.resultMsg];
			alert(getLocalizedMessageString("L_Error_Cl", paramArray));/*alert("Error: " + respJ.resultMsg);*****/
		}
	};
	var onFailure = function(o) {
		hideTransitPanel();
		var paramArray = [o.status];
	alert(getLocalizedMessageString("M_CommErr_CnctVelosSupp", paramArray));/*alert("Communication Error. Please contact Velos Support: " + o.status);*****/
	};
	var onStart = function(o) {
		showTransitPanel(PleaseWait_Dots);/*showTransitPanel('Please wait...');*****/
	}
	myDialog.callback.customevents = { onStart:onStart };
	myDialog.callback.success = onSuccess;
	myDialog.callback.failure = onFailure;
	myDialog.cfg.queueProperty("buttons", myButtons);
	myDialog.render(document.body);
	myDialog.show();
	if (document.getElementById('eSign')) { document.getElementById('eSign').focus(); }
	
	var showFlashPanel = function(msg) {
		// Initialize the temporary Panel to display while waiting for external content to load
		if (!(VELOS.flashPanel)) {
			VELOS.flashPanel = 
				new YAHOO.widget.Panel("flashPanel",  
					{ width:"500px",                   //FIN-20635 Enhancement : Akshi
					  fixedcenter:true, 
					  close:false, 
					  draggable:false, 
					  zindex:4,
					  modal:true,
					  visible:false
					} 
				);
		}
		VELOS.flashPanel.setHeader("");
		VELOS.flashPanel.setBody('<table align="center"><tr><td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt;"><b>'+msg+'</b></td></tr><table>');
		VELOS.flashPanel.render(document.body);
		VELOS.flashPanel.show();
		setTimeout('VELOS.flashPanel.hide();', 3500);//FIN-20635 Enhancement : Akshi   // Akshi 3Nov2011 Bug# 7455
	}
	
	var showTransitPanel = function(msg) {
		if (!VELOS.transitPanel) {
			VELOS.transitPanel = 
				new YAHOO.widget.Panel("transitPanel",  
					{ width:"240px", 
					  fixedcenter:true, 
					  close:false, 
					  draggable:false, 
					  zindex:4,
					  modal:true,
					  visible:false
					} 
				);
		}
		VELOS.transitPanel.setHeader("");
		VELOS.transitPanel.setBody('<table align="center"><tr><td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt;"><b>'+msg+'</b></td></tr><table>');
		VELOS.transitPanel.render(document.body);
		VELOS.transitPanel.show();
	}
	
	var hideTransitPanel = function() {
		if (VELOS.transitPanel) { VELOS.transitPanel.hide(); }
	}
 }
	$j(function() {
		if ($j("input:submit")){
			$j("input:submit").button();
		}
		if ($j("a:submit")){
			$j("a:submit").button();
		}
		if ($j("button")){
			$j("button").button();
		}
	});	
}

VELOS.calMileStoneGrid.prototype.initConfigs = function(args) {
	if (!args) { return false; }
	if (args.constructor != Object) { return false; }
	if (args.urlParams) { this.urlParams = args.urlParams; }
	else { this.dataTable = 'serverpagination'; }
	if ((args.success) && (typeof args.success == 'function')) { 
		this.handleSuccess = args.success;
	}
	if ((args.failure) && (typeof args.failure == 'function')) {
		this.handleSuccess=args.success;
	}	
	return true;
}

VELOS.calMileStoneGrid.prototype.startRequest = function() {
	$C.asyncRequest(
		'POST',
		this.url,
		this.callback,
		this.urlParams
	);
}

VELOS.calMileStoneGrid.prototype.render = function() {
	this.startRequest();
}

VELOS.calMileStoneGrid.prototype.loadingPanel = function() {
	// Initialize the temporary Panel to display while waiting for external content to load
	if (!(VELOS.wait)) {
		VELOS.wait = 
			new YAHOO.widget.Panel("wait",  
				{ width:"240px", 
				  fixedcenter:true, 
				  close:false, 
				  draggable:false, 
				  zindex:4,
				  modal:true,
				  visible:false
				} 
			);

		VELOS.wait.setHeader(LoadingPlsWait);/*VELOS.wait.setHeader("Loading, please wait...");*****/
		VELOS.wait.setBody('<img class="asIsImage" src="../images/jpg/loading_pg.gif" />');
		VELOS.wait.render(document.body);
	}
}

VELOS.calMileStoneGrid.prototype.showPanel = function () {
	if (!(VELOS.wait)) { loadingPanel(); }
	VELOS.wait.show();
}

VELOS.calMileStoneGrid.prototype.hidePanel = function() {
	if (VELOS.wait) { VELOS.wait.hide(); }
}

