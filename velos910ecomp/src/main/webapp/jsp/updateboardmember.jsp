<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
<jsp:useBean id="team" scope="request" class="com.velos.eres.web.team.TeamJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="ctrl" scope="session" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="member" scope="request" class="com.velos.eres.web.memberBoard.MemberJB" />
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,java.text.*,com.velos.eres.service.util.*"%>	
<%
	int ret = 0;
	String stdId;	
	int totrows =0,id; 	
	String src = null;
	src=request.getParameter("src");
	String[] usrRole = null;
	String[] usrId = null;
	int cnt;
	int counter = 0;
	String[] rolearr = null;
	String[] userarr = null;
	String[] assignarr = null;
	String role= null;
	String user = null;
	String assign = null;
	String rights="";
	String tab = request.getParameter("selectedTab");	
	HttpSession tSession = request.getSession(true);	
	String accId = (String) tSession.getValue("accountId");	
 	int iaccId = EJBUtil.stringToNum(accId);
	String right = request.getParameter("right");
	String ipAdd = (String) tSession.getValue("ipAdd");	
	String userId = (String) tSession.getValue("userId");	
	String usr = null;	
	usr = (String) tSession.getValue("userId");	
	int iusr = EJBUtil.stringToNum(usr);	
	String eSign = request.getParameter("eSign");	
if (sessionmaint.isValidSession(tSession))
{
%> <jsp:include page="sessionlogging.jsp" flush="true"/> <%
	String oldESign = (String) tSession.getValue("eSign");
	if(!oldESign.equals(eSign)) {
	%> <jsp:include page="incorrectesign.jsp" flush="true"/> <%
	}
	else 
	{	
	String boardId = (String) tSession.getValue("rbId");
	int boardIds = EJBUtil.stringToNum(boardId);
	String userList = "";
	String memStatus="";
	String memRights="";
	CodeDao cdao=new CodeDao();	
	int codeId=cdao.getCodeId("teamstatus","Active");
	String statusCodeId=codeId+"";
	Date dt1 = new java.util.Date();
	String stDate = DateUtil.dateToString(dt1);		
	ctrl.getControlValues("member_rights");
	int rows = ctrl.getCRows();
	int rbId=EJBUtil.stringToNum(request.getParameter("rbId"));	
	for(int count=0;count<rows;count++)
	{ 
		rights = rights +"1";
	}
	totrows = Integer.parseInt(request.getParameter("totalrows")); //Total Rows
	if (totrows == 0 ){
		 usrRole = null;
		 usrId = null;		 
	} 
	if (totrows > 1 ) 
	{
		rolearr = request.getParameterValues("board_role");
		
		userarr = request.getParameterValues("userId");
		assignarr = request.getParameterValues("assign");			
	  	for (cnt=0, counter=0 ; cnt < totrows;cnt ++)
       	{
		if (!rolearr[cnt].equals(""))
	        {		
				int brdUser=EJBUtil.stringToNum(userarr[cnt]);
				int roleId = EJBUtil.stringToNum(rolearr[cnt]);
				member.setFkBoardId(rbId);
				member.setFkMemberUser(brdUser);
				member.setFkMemberRole(roleId);
				member.setBrdMemStatus(memStatus);        	
				member.setBrdMemRights(memRights);
				member.setIpAdd(ipAdd);
				member.setCreator(userId);
				member.setMemberDetails();
			counter ++ ;	
			}
		}
		 for(int i =0 ; i< assignarr.length; i++)
        	{
        		if(i==0)
        			userList = assignarr[i].toString();
        		else
        			userList = userList + "," + assignarr[i].toString();
        	}
		}
		 else if(totrows == 1)
		 {
			role = request.getParameter("board_role");
			assign = request.getParameter("assign");
			user = request.getParameter("userId");
			userList = user;
	
	  	 if (role != "")
		  {		
				
				member.setFkBoardId(rbId);
				member.setFkMemberUser(EJBUtil.stringToNum(user));
				member.setFkMemberRole(EJBUtil.stringToNum(role));
				member.setBrdMemStatus(memStatus);        	
				member.setBrdMemRights(memRights);
				member.setIpAdd(ipAdd);
				member.setCreator(userId);
				member.setMemberDetails();
			counter ++ ;
		 }
	}		 
%>
<br><br><br><br><br>
<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%-- Data was Saved successfully.*****--%> </p>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=modifymember.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&rbId=<%=rbId%>">
<%
	}//end of if for eSign check
}//end of if body for session
else
{
	%>  <jsp:include page="timeout.html" flush="true"/> <%
}
%>
</BODY></HTML>
