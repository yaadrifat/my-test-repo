<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>



<title><%=LC.L_AddOrEdt_Link%><%--Add/Edit Link*****--%>


</title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
 <SCRIPT Language="javascript">

 function  validate(formobj){

     formobj=document.upload

     if (!(validate_col('File',formobj.name))) return false

     if (!(validate_col('Description',formobj.desc))) return false
     if (!(checkquote(formobj.desc.value))) return false
	
     if (!(validate_col('Esign',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }


 }

	function openwin() {

      window.open("","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=600,height=450")

;}
</SCRIPT> 

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studySiteApndxB" scope="request" class="com.velos.eres.web.studySiteApndx.StudySiteApndxJB"/>
<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*"%>
<jsp:include page="include.jsp" flush="true"/>
<% String src;

src= request.getParameter("srcmenu");

%>
<body>
<br>
<DIV class="popDefault" id="div1"> 

  <%

	HttpSession tSession = request.getSession(true); 

	String mode=request.getParameter("mode");
	String studySiteApndxId = "";
	String name = "";
	String desc = "";
	
	
	
		String type=request.getParameter("type");
		
		
			String studySiteId=request.getParameter("studySiteId");
			
			
			String studyId = request.getParameter("studyId");
	
	if (sessionmaint.isValidSession(tSession))
	{
		if(mode.equals("M"))
		{
			studySiteApndxId = request.getParameter("studySiteApndxId");
			studySiteApndxB.setStudySiteApndxId(EJBUtil.stringToNum(studySiteApndxId));
			studySiteApndxB.getStudySiteApndxDetails();
			name = studySiteApndxB.getAppendixName();
			desc = studySiteApndxB.getAppendixDescription();
			
		}
%>

<form name=upload id="siteApndxUrl" method="POST" action="insertStudySiteApndxURL.jsp?" onsubmit = "if (validate(document.upload)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}" >
    
   
    <table width="100%" >
      <tr> 
        <td align="center"> 
          <P class = "defComments"> <%=MC.M_AddLnk_ToOrg%><%--Add Links to your Organization.*****--%> </P>
        </td>
      </tr>
    </table>
    <table width="100%" >
      <tr> 
        <td width="35%"> <%=LC.L_Url_Upper%><%--URL*****--%> <FONT class="Mandatory">* </FONT> </td>
        <td width="65%"> 
          <input type=text name=name MAXLENGTH=255 size=50 value="<%=name%>">
        
        </td>
      </tr>
      <tr> 
        <td width="35%"> </td>
        <td width="65%"> 
          <P class="defComments"> <%=MC.M_EtrCpltUrl_255Max%><%--Enter complete url eg. 'http://www.centerwatch.com' 
            or 'ftp://ftp.centerwatch.com' (255 char max.)*****--%> </P>
        </td>
      </tr>
      <tr> 
        <td width="35%"> <%=LC.L_Short_Desc%><%--Short Description*****--%> <FONT class="Mandatory" >* </FONT> 
        </td>
        <td width="65%"> 
          <input type=text name=desc MAXLENGTH=100 size=40 value="<%=desc%>">
        </td>
      </tr>
      <tr> 
        <td width="35%"> </td>
        <td width="65%"> 
          <P class="defComments"> <%=MC.M_NameToLnk_100CharMax%><%--Give a friendly name to your link. (100 char 
            max.)*****--%> </P>
        </td>
      </tr>
    </table>
    
    
    <BR>

	
	

<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="siteApndxUrl"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>
	

    	  <input type="hidden" name="mode" value="<%=mode%>">
    	  <input type="hidden" name="type" value="<%=type%>">
    	  <input type="hidden" name="studySiteId" value="<%=studySiteId%>">
    	  <input type="hidden" name="studyId" value="<%=studyId%>">
    	  <input type="hidden" name="studySiteApndxId" value="<%=studySiteApndxId%>">
  </form>
   <%

	
}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
 
<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</body>

</html>


