<%@taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC" %>
<table  width="100%" border="0">
  <tr>
    <td width="50%">

			<table cellpadding="0" cellspacing="0" width="50%" border="0">
				<tr>
					<td align="right"  height="22px" width="30%">
						<%=LC.CTRP_DraftNCITrialId%>&nbsp;
						<span id="ctrpDraftJB.nciTrialId_error" class="errorMessage" style="display:none;"></span>
					</td>
					<td width="2%"><FONT id="nci_id_asterisk" style="visibility:visible;" class="Mandatory">*</FONT> <font class="green" style="text-decoration: none;">*</font></td>
					<td  align="left" height="22px"><s:textfield name="ctrpDraftJB.nciTrialId" id="ctrpDraftJB.nciTrialId" size="50" /></td>
				</tr>
				<tr>
					<td align="right" height="22px"><%=LC.CTRP_DraftLeadOrgTrialId%>
					<span id="ctrpDraftJB.leadOrgTrialId_error" class="errorMessage" style="display:none;"></span>
					</td>
					<td ><FONT id="lead_org_id_asterisk" style="visibility:visible;" class="Mandatory">*</FONT></td>
					<td height="22px"><s:textfield name="ctrpDraftJB.leadOrgTrialId" id="ctrpDraftJB.leadOrgTrialId" size="50" /></td>
				</tr>
				<tr>
					<td align="right" height="22px"><%=LC.CTRP_DraftSubOrgLclIdentfy %>&nbsp;</td>
					<td >&nbsp;</td>
					<td height="22px"> 
					<s:textfield name="ctrpDraftJB.subOrgTrialId" id="ctrpDraftJB.subOrgTrialId" size="50" maxsize="20" readonly="true"></s:textfield>  </td>
				</tr>
				<tr>
					<td align="right" height="22px">
						<%=LC.CTRP_DraftNCTNumber%>&nbsp;
						<span id="ctrpDraftJB.nctNumber_error"  class="errorMessage" style="display:none;"></span>
					</td>
					<td >&nbsp;</td>
					<td align="left" height="22px"><s:textfield name="ctrpDraftJB.nctNumber" id="ctrpDraftJB.nctNumber" size="50" maxsize="20" /></td>
					
				</tr>
			</table>
		</td>
		<td width="50%">
			<table border="0">
				<tr>
					<td height="22px" valign="top">&nbsp;<font class="Grey">(<%=MC.CTRP_DraftNotRegForOrgnlSub %>)</font></td>
				</tr>
				<tr>
					<td height="22px">&nbsp;</td>
				</tr>
				<tr>
					<td height="22px">&nbsp;</td>
				</tr>
				<tr>
					<td height="22px"  valign="bottom">&nbsp;<font class="Grey">(<%=LC.CTRP_DraftMandIfExst %>)</font></td>
				</tr>
			</table>
		</td>
		</tr>
		</table>