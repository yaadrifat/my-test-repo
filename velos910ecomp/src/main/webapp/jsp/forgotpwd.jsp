<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=LC.L_Forgot_Pwd%><%-- Forgot Password*****--%></title>

<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />

<%@ page language = "java" import = "java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.study.*,com.velos.eres.business.common.*,com.velos.eres.service.util.EJBUtil, java.lang.Integer,com.velos.eres.service.util.*"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>


<SCRIPT Language="javascript">
 function  validate(){
     formobj=document.forgotpwd
     if (!(validate_col('Login Name',formobj.loginName))) return false
     if ((formobj.oldMode.value) =="A") {
	     if (!(validate_col('User Answer',formobj.userAns))) return false
	}
   }

 function setfocus(){
	if ((formobj.oldMode.value) =="N") {
	     document.forgotpwd.loginName.focus()
	}
	if ((formobj.oldMode.value) =="A") {
	     document.forgotpwd.userAns.focus()
	}

 }

</SCRIPT>

<%--The same page is called again on submit  
mode=N means user is coming for the first time, he should be given the option of entering the 
login name. 
mode=A means we have to ask the answer of the secret question from the user and get its password
mode=P means that we have to show the password to the user
--%>

<%
	
		
	String userSecQ = null;
	String Password = null;
	String headMsg = null;
	String loginName = "";
	String enteredUserAns = "";
	int userId = 0;
	int counter = 0;
	boolean correctAns=false;

	String scHeight = "";
	String scWidth = "";

	scHeight = request.getParameter("scHeight");
	scWidth = request.getParameter("scWidth");
	
	String messageStr = "";

	if (scHeight == null)
	{
		scHeight = "600";
		scWidth = "800";	
	}
		
	
	String mode = request.getParameter("mode");	
	if (mode.equals("N")) {
		headMsg = MC.M_ForgetPwd_EtrLoginQue/*"Did you forget your password? Enter your login name below to access your secret question."*****/;
	}
	else if (mode.equals("P")) {
		enteredUserAns = request.getParameter("userAns");
		loginName = request.getParameter("loginName");
		userId = EJBUtil.stringToNum(request.getParameter("userId"));
		userB.setUserId(userId);
		userB.getUserDetails();
//		String userAnswer = userB.getUserAnswer();
		
		String userAnswer = ((userB.getUserAnswer()) == null)?"-":(userB.getUserAnswer());
		
		if (userAnswer.equals(enteredUserAns)) {
			correctAns=true;
			Password = userB.getUserPwd();
			headMsg = MC.M_Here_IsYourPwd/*"Here is your password"*****/;
		}		
		else {
			headMsg = MC.M_NoMatchForEtrAns_Retry/*"Sorry, no matches found for the entered Answer. Try Again."*****/;
			mode="N";
			loginName="";
		}
	}	
	else if (mode.equals("A")) {
		loginName = request.getParameter("loginName");
		UserDao userDao; 
		userDao = userB.getUserValuesByLoginName(loginName);

		ArrayList userIds;
		ArrayList userSecQues;		
		
		userIds = userDao.getUsrIds();
		
		counter = userIds.size();
	
		if (counter==0) {
			Object[] arguments = {loginName};
			headMsg =VelosResourceBundle.getMessageString("M_NotReg_CheckSignUp",arguments);/*headMsg = "We do not currently have '" + loginName + "' registered in the eResearch system. Please check the Login Name or Sign up with eResearch.";*****/
		
			correctAns = false;
			loginName="";
			mode="N";
		}
		else {
			userId = ((Integer)(userIds.get(0))).intValue();
			userSecQues = userDao.getUsrSecQues();
			userSecQ = (String)userSecQues.get(0);
			if (userSecQ==null || (userSecQ.trim().equals(""))) {
				userSecQ="-";
				String Hyper_Link = "<a href='mailto:technicalsupport@veloseresearch.com'>"+LC.L_Techsupp_VelosEresCom+"</a>";
				Object[] arguments1 = {loginName,Hyper_Link};
				messageStr =VelosResourceBundle.getMessageString("M_SecretQue_ReqCnctUs",arguments1);/*messageStr = "User <b><font color = 'red'>" + loginName + "</font></b> does not have a secret question and answer entered in his/her profile. We are sorry but we will be unable to process this request. Please contact us at <a href='mailto:technicalsupport@veloseresearch.com'>technicalsupport@veloseresearch.com</a>";*****/		
			}
			headMsg = MC.M_ForgotPwd_AnsSecretQue/*"Forgot Password? Don't worry, just answer your Secret Question"*****/;
			correctAns = true;
		}



	}
%>


<!--
<jsp:include page="homepanel.jsp" flush="true"> 
</jsp:include>   
-->



<body>

   <table border="0" cellspacing="0" cellpadding="0" width="770">
	<!-- HEADER OBJECT START-->
	  <tr>
	    <td colspan="14"><img src="./images/toplogo.jpg" width="770" height="60" alt="<%=LC.L_Velos_Eres %><%-- *****--%>Velos eResearch"></td>
	  </tr>
 	  <tr>
	  	  <td align="right"><img src="./images/toplefcurve.gif" width="18" height="16"></td> 
          <td><img src="./images/vspacer.gif" width="201" height="16" border="0"></td> 
          <td><img src="./images/topmidcurve.gif" width="30" height="16" border="0"></td>
          <td width="85" align="center" bgcolor="9999FF"><a href="index.html" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_5,250,77);" ><img name="Home" src="./images/home.gif" height="16" border="0" alt="<%=LC.L_Home %><%-- Home*****--%>"></a></td>
          <td><img src="./images/mspacer.gif" width="2" height="16" border="0"></td>
          <td width="85" align="center" bgcolor="9999FF"><a href="ataglance.html" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_0,335,77);" ><img name="About" src="./images/about.gif" height="16" border="0" alt="<%=LC.L_About %><%-- About*****--%>"></a></td>
          <td><img src="./images/mspacer.gif" width="2" height="16" border="0"></td>
          <td width="85" align="center" bgcolor="9999FF"><a href="products.html" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_1,420,77);" ><img name="vpopmenu_r2_c3" src="./images/solutions.gif" height="16" border="0" alt="<%=LC.L_Solutions %><%-- Solutions*****--%>"></a></td>
          <td><img src="./images/mspacer.gif" width="2" height="16" border="0"></td>
          <td width="85" align="center" bgcolor="9999FF"><a href="demo.html" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_2,515,77);" ><img name="vpopmenu_r2_c4" src="./images/demo.gif" height="16" border="0" alt="<%=LC.L_Demo %><%-- Demo*****--%>"></a></td>
          <td><img src="./images/mspacer.gif" width="2" height="16" border="0"></td>
          <td width="85" align="center" bgcolor="9999FF"><a href="contact.html" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_3,597,77);" ><img name="vpopmenu_r2_c6" src="./images/contact.gif" height="16" border="0" alt="<%=LC.L_Contact %><%-- Contact*****--%>"></a></td>
          <td><img src="./images/mspacer.gif" width="2" height="16" border="0"></td>
          <td width="85" align="center" bgcolor="9999FF"><a href="register.jsp" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_4,683,77);" ><img name="vpopmenu_r2_c7" src="./images/register.gif" height="16" border="0" alt="<%=LC.L_Register %><%-- Register*****--%>"></td>
      </tr>
	 </table>
	 <!-- HEADER OBJECT END -->

<!--
<DIV class = "formDefault" id="div1">
-->

<%
if (messageStr.length() > 1)
{
%>
<P class="logoutComments">
<br><br><br><br><br><br><br><br><br>
<%= messageStr%>
</P>

<%
}
else
{
%>
<Form name="forgotpwd" method="post" action="forgotpwd.jsp?scHeight=<%=scHeight%>&scWidth=<%=scWidth%>"  onsubmit="return validate()">

&nbsp;
<P class = "defComments">
	<%=headMsg%>
</P>

<input type="hidden" name="oldMode" value="<%=mode%>">
<input type="hidden" name="userId" MAXLENGTH = 15 value="<%=userId%>">

<table width="300" cellspacing="0" cellpadding="0">
  <tr> 
    
<%
   if(mode.equals("A") || mode.equals("P")) {

%>
    <td>
  	<%=LC.L_Your_LoginName%><%--Your Login Name*****--%> 
    </td>
    <td>
	<input type="text" name="loginName" size = 15 MAXLENGTH = 50 value="<%=loginName%>" readonly>
<%
   } else {
%>
    <td>
  	<%=MC.M_Etr_YourLoginName%><%--Enter your Login Name*****--%> <FONT class="Mandatory">* </FONT>
    </td>
    <td>
	<input type="text" name="loginName" size = 15 MAXLENGTH = 50 value="<%=loginName%>">
<%
   }
%>
    </td>
  </tr>

<% if ((mode.equals("A")) && (correctAns)) {
%>
  <tr height="20"> 
    <td>
       <%=LC.L_Your_SecretQues%><%--Your secret question*****--%>
    </td>
    <td>
	<%=userSecQ%> 
    </td>
  </tr>
  <tr>
    <td>
       <%=LC.L_Answer%><%--Answer*****--%><FONT class="Mandatory">* </FONT>
    </td>
    <td>
	<input type="text" name="userAns" size = 15 MAXLENGTH = 50 value="<%=enteredUserAns%>">
    </td>
  </tr> 
<%
}
%>

</table>

<br>

<table width="300" >
<%
 if ((mode.equals("P")) && (correctAns)) {
%>
<tr>
	<td>
	<P class="defcomments"><%Object[] arguments2 = {Password};%>
	    <%=VelosResourceBundle.getMessageString("M_Login_PwdIs",arguments2)%><%--Your Login Password is <%=Password%>*****--%> </P>
	</td>
</tr>
<% 
}
%>
<tr>
 	<td height="10">
	</td>
</tr>
<%
 if ((mode.equals("N")) || (mode.equals("A")))  {
%>
  <tr>
	<td align="right">
	<input type="image" src="../images/jpg/Submit.gif" border="0">
	</td>
  </tr>
<%
}
%>
</table>

<%
	if (mode.equals("N")) {
		mode="A";
	}
	else if (mode.equals("A")) {
		mode="P";
	}
%>
<input type="hidden" name="mode" value="<%=mode%>">


</Form>

<%
}
%>

<P class="logoutComments">
<br>
<%=LC.L_Return_ToThe%><%--Return to the*****--%> <a href="index.htm" style="font-size:12"><%=LC.L_HomePage%><%--Homepage*****--%></a>.</P>

<div>
<jsp:include page="bottompanel.jsp" flush="true"> 
</jsp:include>   
</div>

</div>

<div class ="mainMenu" id = "emenu">

</div>

</body>
</html>


