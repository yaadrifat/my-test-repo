<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<link REL="stylesheet" TYPE="text/css" HREF="menusNS.css" />
	<script LANGUAGE="JavaScript" SRC="menusNS.js">
	</script>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<%@ page import="com.velos.eres.service.util.LC"%>


<%
	HttpSession tSession = request.getSession(true);

	int siteRight = 0, groupRight = 0 , userRight = 0, userPers = 0;
	int userProtocol = 0, userLib = 0, userPat = 0, userReports = 0, manageAct = 1;

	GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getAttribute("GRights");

	//check for organizations
    siteRight = Integer.parseInt(grpRights.getFtrRightsByValue("MSITES"));

	//check for Groups
    groupRight = Integer.parseInt(grpRights.getFtrRightsByValue("CGRP"));

	//check for Users
   userRight = Integer.parseInt(grpRights.getFtrRightsByValue("MUSERS"));

	//check for Personalize Account
    userPers = Integer.parseInt(grpRights.getFtrRightsByValue("HPERSHOME"));

	//check for Manage Protocols
    userProtocol = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));

	//check for Manage Library
  userLib = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));

	//check for Manage Patients
   userPat = Integer.parseInt(grpRights.getFtrRightsByValue("MPATIENTS"));

	//check for Manage Patients
  userReports = Integer.parseInt(grpRights.getFtrRightsByValue("REPORTS"));

 if (siteRight == 0 && groupRight == 0 && userRight == 0 && userPers == 0)
 {
	 manageAct = 0;
 }

   %>

<body onLoad="load()" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0">

<div Class="backgrnd" id="bakgrd">
 	<table width="100%">
		<tr>
			<td width="100%" height="400">&nbsp;
			</td>
		</tr>
	</table>
</div>

<div Class="backgrnd1" id="bakgrd1">
 	<table width="100%">
		<tr>
			<td width="100%" height="100">&nbsp;
			</td>
		</tr>
	</table>
</div>

<div Class="clsMenuBarItem" id="tdMenuBarItemPict1">
	<img SRC="../images/jpg/left_menu_pict.jpg" height=25 width=17><sup><a href="myHome.jsp?srcmenu=tdMenuBarItem1"><%=LC.L_My_Homepage%><%--My Homepage*****--%></a></sup>
</div>

<% if (manageAct > 0)
		{
		%>
<div Class="clsMenuBarItem" id="tdMenuBarItemPict2">
	<img SRC="../images/jpg/left_menu_pict.jpg" height=25 width=17> <sup><a href="#" onClick="return click('2')"><%=LC.L_Manage_Acc%><%--Manage Account*****--%></a></sup>
</div>
<%
	}
	%>
<% if (siteRight > 0)
	{
%>
	<div Class="clsMenu" id="submenuItem1">
	<img SRC="../images/jpg/submenu_pict.jpg" ><sup><a href="sitebrowser.jsp?srcmenu=tdMenuBarItem2"><%=LC.L_Organization%><%--Organizations*****--%></a></sup>
	</div>
<%
	}
%>
<% if (groupRight > 0)
		{
	%>
<div Class="clsMenu" id="submenuItem2">
	<img SRC="../images/jpg/submenu_pict.jpg" ><sup><a href="groupbrowser.jsp?srcmenu=tdMenuBarItem2"><%=LC.L_Groups%><%--Groups*****--%></a></sup>
</div>
<%
	}
%>

<% if (userRight > 0)
		{
	%>
<div Class="clsMenu" id="submenuItem3">
	<img SRC="../images/jpg/submenu_pict.jpg" ><sup><a href="accountbrowser.jsp?srcmenu=tdMenuBarItem2"><%=LC.L_Users%><%--Users*****--%></a></sup>
</div>
<%
	}
%>

<% if (userPers > 0)
		{
	%>
<div Class="clsMenu" id="submenuItem4">
	<img SRC="../images/jpg/submenu_pict.jpg" ><sup><a href="ulinkBrowser.jsp?srcmenu=tdMenuBarItem2&selectedTab=2"><%=LC.L_Personalize_Acc%><%--Personalize Account*****--%></a></sup>
</div>
<%
	}
%>

<%
	if (userProtocol > 0)
	 {
	%>
<div Class="clsMenuBarItem" id="tdMenuBarItemPict3">
	<img SRC="../images/jpg/left_menu_pict.jpg" height=25 width=17><sup><a href="#" onClick="return click('3')"><%=LC.L_Manage_Pcols%><%--Manage Protocols*****--%></a></sup>
</div>
<%
	}
%>

<%
	if (userProtocol == 5 || userProtocol == 7 )
	 {
	%>
	<DIV  Class="clsMenu" id="submenuItem5">
	<img src = "../images/jpg/submenu_pict.jpg"><sup><A HREF="studywizard.jsp?srcmenu=tdMenuBarItem3"><%=LC.L_New%><%--New*****--%></A></sup>
	</DIV>
	<%
	}
	%>
 <%
	if (userProtocol == 4 || userProtocol == 6  || userProtocol == 7)
	 {
	%>
	<DIV  Class="clsMenu" id="submenuItem6">
	<img src = "../images/jpg/submenu_pict.jpg"><sup><A HREF="userStudies.jsp?srcmenu=tdMenuBarItem3"><%=LC.L_Open%><%--Open*****--%></A></sup>
	</DIV>
 <%
	}
 %>

	<%
	if (userLib > 0)
	 {
	%>
		<div Class="clsMenuBarItem" id="tdMenuBarItemPict4">
		<img src = "../images/jpg/left_menu_pict.jpg" width="17" height="25"><sup><a href="#" onClick="return click('4')"><%=LC.L_Library%><%--Library*****--%></a></sup>
		</div>
	 <%
		}
	 %>

	<%
	if (userLib > 0)
	 {
	%>

	<DIV  Class="clsMenu" id="submenuItem7">
	<img src = "../images/jpg/submenu_pict.jpg"><sup><A HREF="protocollist.jsp?mode=N&srcmenu=tdmenubaritem4&selectedTab=1&calledFrom=L"><%=LC.L_Protocol_Calendar%><%--Protocol Calendar*****--%></A></sup>
	</DIV>
	<DIV  Class="clsMenu" id="submenuItem8">
	<img src = "../images/jpg/submenu_pict.jpg"><sup><A HREF="eventlibrary.jsp?srcmenu=tdmenubaritem4&selectedTab=2&calledFrom=L&duration=&protocolId=&mode=M&calStatus=W"><%=LC.L_Evt_Lib%><%--Event Library*****--%></A></sup>
	</DIV>
	 <%
		}
	 %>

	<%
	if (userPat > 0)
	 {
	%>
	<div Class="clsMenuBarItem" id="tdMenuBarItemPict5">
	<img SRC="../images/jpg/left_menu_pict.jpg" height=25 width=17><sup><a href="#" onClick="return click('5')"><%=LC.L_Mng_Pats%><%--Manage <%=LC.Pat_Patients%>*****--%></a></sup>
	</div>
	 <%
		}
	 %>

	<%
    if (userPat == 5 || userPat == 7)
	  {
	%>
	<div Class="clsMenu" id="submenuItem9">
	<img SRC="../images/jpg/submenu_pict.jpg" ><sup><A href="patientdetails.jsp?srcmenu=tdmenubaritem5&mode=N&selectedTab=1&pkey=&page=patient"><%=LC.L_New%><%--New*****--%></A></sup>
	</div>
	 <%
		}
	 %>
	 <%
 	 if (userPat == 4 || userPat == 6 || userPat == 7)
	  {
	%>
		<div Class="clsMenu" id="submenuItem10">
		<img SRC="../images/jpg/submenu_pict.jpg" ><sup><A href="studypatients.jsp?srcmenu=tdmenubaritem5&studyId=&patid=&patstatus=&openMode=F"><%=LC.L_Open%><%--Open*****--%></A></sup>
		</div>
	<%
		}
	 %>
 <% if (userReports > 0)
	{
%>
<div Class="clsMenuBarItem" id="tdMenuBarItemPict6">
	<img SRC="../images/jpg/left_menu_pict.jpg" height=25 width=17><sup><a href="accountreports.jsp?selectedTab=1&srcmenu=tdmenubaritem4"><%=LC.L_Reports%><%--Reports*****--%></a></sup>
</div>
<%
	}
 %>

<div Class="clsMenuBarItem" id="tdMenuBarItemPict7">
	<img SRC="../images/jpg/left_menu_pict.jpg" height=25 width=17><sup><a href="ereshelp.jsp" onClick="openwin()"><%=LC.L_Help%><%--Help*****--%></a></sup>
</div>
<!-- Commented by Gopu to fix Bug #2416
<div Class="clsMenu" id="submenuItem11">
	<img SRC="../images/jpg/submenu_pict.jpg" ><sup><A href="ereshelp.jsp" target="Information" onclick="openwin()">Online Help</A></sup>
</div>
<div Class="clsMenu" id="submenuItem12">
	<img SRC="../images/jpg/submenu_pict.jpg" ><sup><A href="contactus.html" target="Information" onclick="openwin()">Contact Us</A></sup>
</div>

<div Class="clsMenu" id="submenuItem13">
	<img SRC="../images/jpg/submenu_pict.jpg" ><sup><A href="termsofservice.html" target="Information" onclick="openwin()">Terms of Service</A></sup>
</div>

<div Class="clsMenu" id="submenuItem14">
	<img SRC="../images/jpg/submenu_pict.jpg" ><sup><A href="privacypolicy.html" target="Information" onclick="openwin()">Privacy Policy</A></sup>
</div>
-->
<div Class="clsMenuBarItem" id="tdMenuBarItemPict8">
	<img SRC="../images/jpg/left_menu_pict.jpg" height=25 width=17><sup><a href="logout.jsp"><%=LC.L_Logout%><%--Logout*****--%></a></sup>
</div>

</html>

