<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html> 
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_Pcol_Dets%><%--Protocol Details*****--%></title>
<%-- Nicholas : Start --%>
<%--<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>--%>
<%-- Nicholas : End --%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB, com.velos.eres.web.user.UserJB " %>
<%@ page import="com.velos.eres.business.user.*,com.velos.eres.service.userAgent.*,com.velos.eres.service.util.*" %>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="appendixB" scope="page" class="com.velos.eres.web.appendix.AppendixJB" />
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="sectionB" scope="page" class="com.velos.eres.web.section.SectionJB" />
<jsp:useBean id="studyVerB" scope="request" class="com.velos.eres.web.studyVer.StudyVerJB"/>

<meta http-equiv="Content-Type" content=text/html; charset=UTF-8>

<style>
body, layer { color: 000000; background-color: FFFFFF; }
body, td, center, p, div { font-family: arial; font-size: 10pt }
b { font-family: arial }
.sp { line-height: 2pt; }
.heading { font-weight: bold; font-family: arial; font-size: 10pt;}
.small { font-size: 8pt; font-family: arial }

.large { font-size: 12pt; }
.margin { margin-top: 2px; margin-bottom: 2px; }
.exc { padding-bottom: 3; padding-left: 3; padding-right: 3; padding-top: 5; }

.evenrow{background-color:"#CCCCCC"}
.oddrow{background-color:"#FFFFFF"}
.labelheading { font-size: 11pt; font-family: arial; color: 000000 ;font-weight:bold;background-color:"#CCCCCC" }
</style>

</head>
<%-- Nicholas : Start --%>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%-- Nicholas : End --%>
<%
int ienet = 2;

String agent1 = request.getHeader("USER-AGENT");

   if (agent1 != null && agent1.indexOf("MSIE") != -1) 

     ienet = 0; //IE

    else

	ienet = 1;

	if(ienet == 0) {	

%>

<body style="overflow:scroll;">

<%

	} else {

%>

<body>

<%

	}

%>



<%

	HttpSession tSession = request.getSession(true);

	boolean noDataFlag = false;

	if (sessionmaint.isValidSession(tSession))

	{
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
		int reportNumber = EJBUtil.stringToNum(request.getParameter("reportNumber"));
		String id = request.getParameter("id");
		StringBuffer output = new StringBuffer();
		String userIdFromSession = (String) tSession.getValue("userId");
		String type= request.getParameter("type");
 	    String mode ="";

  	    String acc = (String) tSession.getValue("accountId");

		 int accId = EJBUtil.stringToNum(acc);	
         int studyId = 0;

		 CodeDao cdTArea = new CodeDao();   

		 CodeDao cdPhase = new CodeDao();

		 CodeDao cdResType = new CodeDao();

         CodeDao cdBlinding = new CodeDao();

   		 CodeDao cdRandom = new CodeDao();



		 CodeDao cdType = new CodeDao(); 

		

		 String studyNumber = "" ;

		 String studyTitle = "" ;

		 String studyObjective = "" ;

		 String studySummary = "" ;

		 String studyProduct = "" ;

		 String studyTArea = "" ;

		 String studySize = "0" ;

		 String studyDuration = "0" ;

		 String studyDurationUnit = "" ;

		 String studyEstBgnDate = "" ;

		 String studyPhase = "" ;

		 String studyResType = "" ;

		 String studyType = "" ;

		 String studyBlinding = "" ;

		 String studyRandom = "" ;

		 String studySponsorId = "" ;

		 String studyContactId = "" ;

		 String studyOtherInfo = "" ;

		 String studyPartCenters = "" ;

		 String studyKeywrds = "" ;

		 String studyAuthor = "" ;

		 String studyPubCol="";

		 char[] stdSecRights =null ;

		 int counter = 0;

		 int userId = 0;

		 int siteId = 0;

		 int secCount = 0;	

		 UserDao userDao = new UserDao();

		 SiteDao siteDao = new SiteDao();

		 String str = "";
		
 		 studyId = EJBUtil.stringToNum(request.getParameter("id"));

		   studyB.setId(studyId);

		   studyB.getStudyDetails();

		   studyNumber = studyB.getStudyNumber();
   studyTitle = studyB.getStudyTitle();

   studyObjective = studyB.getStudyObjective();

   studySummary = studyB.getStudySummary();

   studyProduct = studyB.getStudyProduct();

   studySize = studyB.getStudySize();

   studyDuration = studyB.getStudyDuration();

   studyDurationUnit = studyB.getStudyDurationUnit();

   studyEstBgnDate = studyB.getStudyEstBeginDate();

   studyOtherInfo = studyB.getStudyInfo();

   studyPartCenters = studyB.getStudyPartCenters();

   studyKeywrds = studyB.getStudyKeywords();

   studySponsorId = studyB.getStudySponsor();

   studyContactId = studyB.getStudyContact();

   studyAuthor = studyB.getStudyAuthor();



   String reportName = MC.M_SummPub_Bcast;/*String reportName = "Summary for Public Broadcast";*****/

	   //UserRObj userRObj = null;       //Msgcntr Entity Bean Remote Object

	   //UserPK userPK;

	   //userPK = new UserPK(EJBUtil.stringToNum(studyAuthor));

	   //UserHome userHome = EJBUtil.getUserHome();

	   //userRObj = userHome.findByPrimaryKey(userPK);
	  
	  UserJB uj  = new UserJB();
	  uj.setUserId(EJBUtil.stringToNum(studyAuthor));
	  uj.getUserDetails();
	  	
      String authorName = uj.getUserLastName() + ", " + uj.getUserFirstName();

	   studyPubCol = studyB.getStudyPubCol();


	if (type.equals("P")){ //View Public Studies	"P"

		if (studyPubCol == null ){

			studyPubCol = "11111"; // default rights for all section is Public

			reportName = LC.L_Pcol_Summary;/*reportName = "Protocol Summary";*****/

		}

	}else{ //Complete Protocol "A"

		studyPubCol = "10000";
		Object[] arguments = {studyTitle,studyNumber};
		reportName = VelosResourceBundle.getLabelString("L_Std_Num",arguments) ;/*reportName =  studyTitle +"<BR> Study Number: " +studyNumber ;*****/

	}
		
		System.out.println("sssssssssssssssstudyPubCol*" + studyPubCol + "*" );
		stdSecRights= studyPubCol.toCharArray();

		int totSec = studyPubCol.length();

		cdTArea.getCodeValuesById(EJBUtil.stringToNum(studyB.getStudyTArea()));

		if(cdTArea.getCDesc().size() > 0){

      		studyTArea = (String) cdTArea.getCDesc().get(0);

	}



	 cdPhase.getCodeValuesById(EJBUtil.stringToNum(studyB.getStudyPhase()));

	if(cdPhase.getCDesc().size() > 0){

      		studyPhase = (String) cdPhase.getCDesc().get(0);

	}

	cdResType.getCodeValuesById(EJBUtil.stringToNum(studyB.getStudyResType()));

	if(cdResType.getCDesc().size() > 0){

      		studyResType = (String) cdResType.getCDesc().get(0);

	}





	cdBlinding.getCodeValuesById(EJBUtil.stringToNum(studyB.getStudyBlinding()));

	if(cdBlinding.getCDesc().size() > 0){

      		studyBlinding = (String) cdBlinding.getCDesc().get(0);

	}

	cdRandom.getCodeValuesById(EJBUtil.stringToNum(studyB.getStudyRandom()));

	if(cdRandom.getCDesc().size() > 0){

      		studyRandom = (String) cdRandom.getCDesc().get(0);

	}



	cdType.getCodeValuesById(EJBUtil.stringToNum(studyB.getStudyType()));

	if(cdType.getCDesc().size() > 0){

      		studyType = (String) cdType.getCDesc().get(0);

	}

	output.append(" <P><P><P>"); 

	output.append("  <table width='100%' cellspacing='0' cellpadding='0' border=0 >");

	output.append("    <tr > ");

	output.append("      <td width = '100%'> ");

	output.append("        <P class = 'welcome' align='center'> "+ reportName+" </P>");

	output.append("      </td>");

	output.append("    </tr>");

	output.append("  </table>");

	output.append("<hr size=5 class='bluehr'>");

	output.append(" </P></P></P><P><P><P>");

	

	output.append("<Form name='report'>");

	output.append("<input type='hidden' name='studyId' size = 20  value = " +studyId +">");

	counter = 0;

if (type.equals("P") && (stdSecRights[secCount] == '1')) {

	output.append("<BR>");

	output.append("<table width='100%'>");

	output.append("<tr>");

	output.append("<td class= 'labelheading'>");

	output.append(LC.L_Definition);/*output.append("Definition");*****/

	output.append("</td>");

	output.append("</tr>");

	output.append("</table>");

	output.append("<hr size=2 class='bluehr'>");

	output.append("<BR>");

	output.append("<table width='100%' cellspacing='0' cellpadding='0' >");

	output.append("	  <tr>"); 

      output.append("<td width='30%' class= 'heading'>");

      output.append("   "+LC.L_Number+" ");/*output.append("   Number ");*****/

      output.append("</td>");

      output.append("<td width='70%'>");

	output.append(studyNumber);

      output.append("</td>");

      output.append("</tr>");

      output.append("<tr>"); 

      output.append("  <td width='30%' class= 'heading'><br>");

      output.append("      "+LC.L_Title+" ");/*output.append("      Title ");*****/

      output.append("	 </td>");

	output.append("   <td width='70%'><br>");

	output.append(studyTitle);

      output.append("<br></td>");

      output.append("</tr>");	

	output.append("<tr>"); 

      output.append("<td width='30%' class= 'heading'><br>");

     	output.append(LC.L_Primary_Objective);/*output.append("Primary Objective");*****/

      output.append("</td>");

      output.append("<td width='70%'><br>");

	output.append(studyObjective);

      output.append("<br></td>"); 

     	output.append("</tr>");

	output.append("<tr>"); 

	output.append("<td width='30%' class= 'heading'><br>");

	output.append(LC.L_Summary);/*output.append("Summary");*****/

	output.append("</td>");

      output.append("<td width='70%'><br>");

	output.append(studySummary);

      output.append("<br></td>");

      output.append("</tr>");		

      output.append("</tr>");

	output.append("</table>");

} else if (type.equals("A")){

	output.append("<BR>");

	output.append("<table width='100%'>");

	output.append("<tr>"); 

        output.append("<td width='30%' class= 'heading'><br>");

     	output.append(LC.L_Objective);/*output.append("Objective");*****/

        output.append("</td>");

        output.append("<td width='70%'><br>");

	    output.append(studyObjective);

        output.append("<br></td>"); 

     	output.append("</tr>");

		output.append("</table>");

}

	secCount++; 

	if (stdSecRights[secCount] == '1') {  //Display Section 1 only if it is Public section: 								//1=Public 0=Non-Public

		output.append("<BR>");

		output.append("<table width='100%'>");

		output.append("<tr id='headSection' >");

		output.append("<td class= 'labelheading'>");

		output.append(LC.L_Details);/*output.append("Details");*****/

		output.append("</td>");

		output.append("</tr>");

		output.append("</table>");

		output.append("<hr size=2 class='bluehr'>");

		output.append("<BR>");

		output.append("<table width='100%' cellspacing='0' cellpadding='0'> ");

		output.append("<tr>"); 

	      output.append("<td width='30%' class= 'heading'>");

      	output.append(LC.L_Product_Name);/*output.append("Product Name");*****/

	    	output.append("</td>");

	    	output.append("<td width='70%'>");

		output.append(studyProduct); 

	    	output.append("</td>");

		output.append("</tr>");

		output.append("<tr>"); 





	    output.append("<td width='30%' class= 'heading'>");



     	 output.append(LC.L_Therapeutic_Area);/*output.append("Therapeutic Area");*****/ 

	    output.append("</td>");

	  output.append("<td width='70%'>");

		output.append(studyTArea);

	  output.append("</td>");

	  output.append("</tr>");

	  output.append("<tr>"); 

	    output.append("<td width='30%' class= 'heading'>");

	       output.append(LC.L_Sample_Size);/*output.append("Sample Size");*****/

	    output.append("</td>");

	    output.append("<td width='70%'>");

		output.append(studySize);

	    output.append("</td>");

	  output.append("</tr>");

	  output.append("<tr>"); 

	    output.append("<td width='30%' class= 'heading'>");

	       output.append(LC.L_Study_Duration);/*output.append("Study Duration");*****/

	    output.append("</td>");

	    output.append("<td width='70%'>");

		output.append(studyDuration);	output.append(studyDurationUnit);

    	    output.append("</td>");

  	 output.append("</tr>");

	 output.append("<tr>"); 

    	output.append("<td width='30%' class= 'heading'>");

       output.append(LC.L_Estimated_BeginDate);/*output.append("Estimated Begin Date");*****/

    	output.append("</td width='70%'>");

    	output.append("<td colspan=2>");

		output.append(studyEstBgnDate);

	 output.append("</td>");	

	output.append("</table>");



} else {



}

secCount++; 



if (stdSecRights[secCount] == '1' ) { 
	output.append("<BR>");
	output.append("<table width='100%'>");
	output.append("<tr id='headSection' >");
	output.append("<td class= 'labelheading'>");
	output.append(LC.L_Design);/*output.append("Design");*****/
	output.append("</td>");
	output.append("</tr>");
	output.append("</table>");
	output.append("<hr size=2 class='bluehr'><br>");
	output.append("<table width='100%' cellspacing='0' cellpadding='0'>");
    output.append("<td width='30%' class= 'heading'>");
    output.append(LC.L_Phase+" ");/*output.append("Phase ");*****/
    output.append("</td>");
	    output.append("<td width='70%'>");
		output.append(studyPhase);
	    output.append("</td>");
	  output.append("</tr>");		
	  output.append("<tr>"); 
	    output.append("<td width='30%' class= 'heading'>");
      	 output.append(LC.L_Research_Type);/*output.append("Research Type");*****/
	    output.append("</td>");
	    output.append("<td width='70%'>");
		output.append(studyResType);
	  output.append("</td>");
	  output.append("</tr>");			
	  output.append("<tr>"); 
	    output.append("<td width='30%' class= 'heading'>");
	       output.append(LC.L_Study_Type);/*output.append("Study Type");*****/
	    output.append("</td>");
	   output.append("<td width='70%'>");
		output.append(studyType);
	    output.append("</td>");
	  output.append("</tr>");			
	  output.append("<tr>"); 
	    output.append("<td width='30%' class= 'heading'>");
 output.append(LC.L_Blinding);/*output.append("Blinding");*****/
	    output.append("</td>");

	    output.append("<td width='70%'>");
		output.append(studyBlinding);
	    output.append("</td>");
	  output.append("</tr>");			
	  output.append("<tr>"); 

	    output.append("<td width='30%' class= 'heading'>");





	       output.append(LC.L_Randomization);/*output.append("Randomization");*****/





	    output.append("</td>");





	    output.append("<td width='70%'>");





		output.append(studyRandom);





	    output.append("</td>");





	  output.append("</tr>");	





	output.append("</table>");

} 



secCount++; 

if ((type.equals("P")) && (stdSecRights[secCount] == '1')){

	output.append("<BR>");

    output.append("<table width='100%'>");

     output.append("<tr>");

     output.append("<td class= 'labelheading'>");

     output.append(LC.L_Sponsor_Information);/*output.append("Sponsor Information");*****/

     output.append("</td>");

     output.append("</tr>");

     output.append("</table>");

     output.append("<hr size=2 class='bluehr'>");

     output.append("<br>");

     output.append("<table width='100%' cellspacing='0' cellpadding='0'>");

     output.append("<td width='30%' class= 'heading'>");

     output.append(LC.L_Sponsored_By);/*output.append("Sponsored By");*****/

     output.append("</td>");

     output.append("<td width='70%'>");

     output.append(studySponsorId);

     output.append("</td>");

     output.append("</tr>");		

     output.append("<tr>"); 

     output.append("<td width='30%' class= 'heading'>");

     output.append(LC.L_Contact);/*output.append("Contact");*****/

     output.append("</td>");

     output.append("<td width='70%'>");

     output.append(studyContactId);

     output.append("</td>");

     output.append("</tr>");			

     output.append("<tr>"); 

     output.append("<td width='30%' class= 'heading'>");

     output.append(LC.L_Other_Information);/*output.append("Other Information");*****/

     output.append("</td>");

     output.append("<td width='70%'>");

     output.append(studyOtherInfo);

     output.append("</td>");

     output.append("</tr>");		

     output.append("</table>");

     output.append("</table>");

}

secCount++; 

/*if ((type.equals("P")) && (stdSecRights[secCount] == '1')){

	output.append("<BR>");

    output.append("<table width='100%'>");

     output.append("<tr>");

     output.append("<td class= 'labelheading'>");

     output.append("Participating Centers");

     output.append("</td>");

     output.append("</tr>");

     output.append("</table>");

     output.append("<hr size=2 class='bluehr'>");

     output.append("<br>");

     output.append("<table width='100%' cellspacing='0' cellpadding='0' >");

     output.append("<tr>");

     output.append("<td width='30%' class= 'heading'>");

     output.append("Participating Centers");

     output.append("</td>");

     output.append("<td width='70%'>");

     output.append(studyPartCenters);

     output.append("</td>");

     output.append("</tr>");

     output.append("</table>");

 }*/

  
//study versions
output.append("<BR>");
output.append("<table width='100%' >");
output.append("<tr>");
output.append("<td class= 'labelheading'>");
output.append(LC.L_Versions);/*output.append("Versions");*****/
output.append("</td>");
output.append("</tr>");
output.append("</table>");
output.append("<hr size=2 class='bluehr'>");

StudyVerDao studyVerDao = studyVerB.getAllVers(studyId);
ArrayList studyVerIds = studyVerDao.getStudyVerIds();
ArrayList studyVerNumbers = studyVerDao.getStudyVerNumbers();
int verlen = studyVerIds.size();
SectionDao sectionDao;
ArrayList secNames;	 
ArrayList secContents;
AppendixDao appendixDao ;
ArrayList appendixIds;
ArrayList appendixFileUris;
ArrayList appendixDescs;
ArrayList appendixFiles;
ArrayList appendixTypes;

//get all study versions
for(int i=0;i<verlen;i++) {
	String stdVerId = ((studyVerIds.get(i))==null)?"-":(studyVerIds.get(i)).toString();
	int studyVerId = EJBUtil.stringToNum(stdVerId);
	String studyVerNum =((studyVerNumbers.get(i))==null)?"-":(studyVerNumbers.get(i)).toString();
	output.append("<table width='100%' cellspacing='0' cellpadding='0'>");
	output.append("<tr height='10'>");
	output.append("</tr>");	
	output.append("<tr>");
	output.append("<td class= 'labelheading'>"+LC.L_Version+": ");/*output.append("<td class= 'labelheading'>Version: ");*****/
	output.append(studyVerNum);
	output.append("</td>");
	output.append("</tr>");
   	output.append("</table>");
	output.append("<hr size=2 class='bluehr'>");	

if (type.equals("P")){
	sectionDao= sectionB.getStudy(studyVerId, "Y","");
}else{
	sectionDao= sectionB.getStudy(studyVerId, "A","");
}

secNames = sectionDao.getSecNames();
secContents = sectionDao.getContents();

	String secName = "";

	String secContent = "";

	int len = secNames.size();

	boolean flag = true;

	boolean	uflag = true;

	for(counter=0;counter<len;counter++) {
		secName = (String) secNames.get(counter);
		secContent = (String) secContents.get(counter);
		if((flag == true) && type.equals("P")) {
			flag = false;			
			output.append("<BR>");
			output.append("<table width='100%'>");
			output.append("<tr>");
			output.append("<td class= 'labelheading'>");
			output.append(LC.L_Sections);/*output.append("Sections");*****/
			output.append("</td>");
			output.append("</tr>");
			output.append("</table>");
			output.append("<hr size=2 class='bluehr'>");
			output.append("<BR>");
			output.append("<table width='100%' cellspacing='0' cellpadding='0' >");
		} else if (type.equals("A")){
				flag = false;
				output.append("<BR><table width='100%' cellspacing='0' cellpadding='0' >");
		}

		if(flag == false){
			output.append("<tr>");
			output.append("<td class= 'labelheading'>");
			output.append(secName);
			output.append("</td>");
			output.append("</tr><tr><td><br>");
			output.append("<pre>" + secContent + "<pre>");
			output.append("<br><br></td>");
			output.append("</tr>");
		} //end of if for checking value of flag
	}//end of for loop sections

	output.append("</table>");

	if (type.equals("P")) {

		appendixDao	= appendixB.getAppendixValuesPublic(studyVerId);

	}else{

		appendixDao = appendixB.getByStudyVerId(studyVerId);

	}

	appendixIds = appendixDao.getAppendixIds();

	appendixFileUris = appendixDao.getAppendixFile_Uris();	

	appendixDescs = appendixDao.getAppendixDescriptions();

	appendixFiles = appendixDao.getAppendixFiles();

	appendixTypes = appendixDao.getAppendixTypes();

	String appendixFileUri = "";

	String appendixDesc = "";

	String appendixFile = "";

	String appendixType = "";

	len = appendixIds.size();

	Integer appId;

	flag = true;

	uflag = true;

	for(counter=0;counter<len;counter++) {

		appendixFileUri = (String) appendixFileUris.get(counter); 

		appendixDesc = (String) appendixDescs.get(counter);

		appendixFile = (String) appendixFiles.get(counter);

		appendixType = (String) appendixTypes.get(counter);

		appId = (Integer)appendixIds.get(counter);

		

		appendixType = appendixType.trim();

		if(appendixType.trim().equals("file")){



			if(flag == true){



				flag = false;



				output.append("<BR>");





				output.append("<table width='100%'>");





				output.append("<tr id='headSection' >");





				output.append("<td class= 'labelheading'>");





				output.append(LC.L_Appendix_Files);/*output.append("Appendix Files");*****/





				output.append("</td>");





				output.append("</tr>");





				output.append("</table>");

				output.append("<hr size=2 class='bluehr'>");

				output.append("<BR>");



				output.append("<table width='100%' cellspacing='0' cellpadding='0' >");





			} 



			if ( flag == false) {



				output.append("<tr>");



				output.append("<td width='40%' align='left'>");



				String dnld="";



				Configuration.readAppendixParam(Configuration.ERES_HOME + "eresearch.xml");



				dnld=Configuration.DOWNLOADSERVLET+"/"+ appendixFile ;



				output.append("<A Href='" +dnld +"?appndxId=" + appId + "' target = '_new'>"); 



				output.append(appendixFileUri +"</A>");



				output.append("</td>");



				output.append("<td align='left'>[ ");



				output.append(appendixDesc);



				output.append(" ]</td>");



				output.append("</tr>");

			}

		}



		if(appendixType.equals("url")){ 

			 if (uflag == true) {

				uflag = false;

				output.append("</table>");

				output.append("<BR>");

				output.append("<table width='100%'>");

				output.append("<tr>");

				output.append("<td class= 'labelheading'>");

				output.append(LC.L_Appendix_Urls);/*output.append("Appendix URLs");*****/

				output.append("</td>");

				output.append("</tr>");

				output.append("</table>");

				output.append("<hr size=2 class='bluehr'>");



				output.append("<BR>");





				output.append("<table width='100%' cellspacing='0' cellpadding='0' >");



			} 





			if (uflag == false) {



				output.append("<tr>");





				output.append("<td align='left' width='40%' >");





				output.append("<A Href='" +appendixFileUri +"' target = '_new'>"); 



				output.append(appendixFileUri +"</A>");





				output.append("</td>");





				output.append("<td align='left'>[ ");





				output.append(appendixDesc);





				output.append(" ] </td>");





				output.append("</tr>");
			}	
		}//end of if else if ladder
	}//end of for loop appendix
} //for loop study version




	output.append("</table>");

	output.append("<hr size=5 class='bluehr'>");

	output.append("</Form>");





	String contents1, contents2, contents3, Str, styleTag;

	contents1 = null;

	contents2 = null;

	contents3 = null;

	Str = null;

	styleTag = null;

	Str = output.toString();





	styleTag = "<style> body, layer { color: 000000; background-color: FFFFFF; } body, td, center, p, div { font-family: arial; font-size: 10pt } " +  

	" b { font-family: arial } .sp { line-height: 2pt; } .heading { font-weight: bold; font-family: arial; font-size: 10pt;} " + 

	".small { font-size: 8pt; font-family: arial } .welcome { font-size: 12pt; font-family: arial; color: 000099;font-weight:bold }" + 

	".large { font-size: 12pt; } .margin { margin-top: 2px; margin-bottom: 2px; }" + 

	".exc { padding-bottom: 3; padding-left: 3; padding-right: 3; padding-top: 5; }" + 

	".bluehr {color:#330099} .evenrow{background-color:#CCCCCC} .oddrow{background-color:#FFFFFF}" + 

	" .labelheading { font-size: 11pt; font-family: arial; color: 000000 ;font-weight:bold;background-color:#CCCCCC }" + 

	"</style>";



	ReportIO.STYLE_TAG = styleTag;

	contents1 = ReportIO.saveReportToDoc(Str,"doc","reportword");

	contents2 = ReportIO.saveReportToDoc(Str,"xls","reportexcel");

	contents3 = ReportIO.saveReportToDoc(Str,"htm","reporthtm");

	if(!noDataFlag) {

	%>

	<%} //end of if for noDataFlag

		out.print(Str);



	}else{%>

		<jsp:include page='timeout.html' flush='true'/> 

	<%}%>



</body>

</html>





