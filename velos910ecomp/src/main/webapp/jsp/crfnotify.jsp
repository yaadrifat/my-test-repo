<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title><%=MC.M_PatCrf_Notif%><%--<%=LC.Pat_Patient%> CRF Notification Details*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.esch.business.common.CrfDao,com.velos.eres.service.util.LC,com.velos.eres.service.util.MC" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>

<%@ page language = "java" import = "com.velos.eres.service.util.EJBUtil,com.velos.esch.business.common.SchCodeDao,com.velos.eres.business.common.UserDao"%>


<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />

<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="crfNotifyB" scope="request" class="com.velos.esch.web.crfNotify.CrfNotifyJB"/>
<jsp:useBean id="alertNotifyB" scope="request" class="com.velos.esch.web.alertNotify.AlertNotifyJB"/>
<jsp:useBean id="crfB" scope="request" class="com.velos.esch.web.crf.CrfJB"/>
<jsp:useBean id="codeLst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="patProtB" scope="request" class="com.velos.eres.web.patProt.PatProtJB"/>
<jsp:useBean id="eventAssocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>

<% 
	String src;
	src= request.getParameter("srcmenu");
	String selectedTab = request.getParameter("selectedTab");
%>
<SCRIPT>
	function setVals(str,formobj){
				formobj.action = formobj.action + "?addFlag=" + str; 

		return true;	
	}

function openwin1(formobj) {
	  var names = formobj.crfNotifyToName.value;
	  var ids = formobj.crfNotifyToId.value;
	  var windowToOpen = "multipleusersearchdetails.jsp?fname=&lname=&from=crfnotify&mode=initial&ids=" + ids + "&names=" + names ;
      window.open(windowToOpen,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200");
	  }
	
function  validate(formobj,str){
 	if (!(validate_col('Notify To',formobj.crfNotifyToName))) return false
	if (!(validate_col('CRF Status',formobj.crfStat))) return false
 	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
   }

   if (!(setVals(str,formobj))) return false; 
   formobj.submit();
}


</SCRIPT>
<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>   

<body>
<br>
<DIV class="browserDefault"  id="div1">

<%

   HttpSession tSession = request.getSession(true); 

   if (sessionmaint.isValidSession(tSession))

	{

String patProtId= request.getParameter("patProtId");
String studyId = request.getParameter("studyId");	
String statDesc=request.getParameter("statDesc");
String statid=request.getParameter("statid");		
String studyVer = request.getParameter("studyVer");	
String protocolId="";
String tempCrfId = "";

int i =0;
	SchCodeDao cd1 = new SchCodeDao();
	cd1.getCodeValues("crfstatus");
	String page1 = request.getParameter("page");
	String fromPage = request.getParameter("fromPage");
	
	String dCrfStat = "";	
	Calendar cal1 = new GregorianCalendar();
	String mode = request.getParameter("mode");
	int personPK = 0;	
	personPK = EJBUtil.stringToNum(request.getParameter("pkey"));
  person.setPersonPKId(personPK);
 	person.getPersonDetails();
	String uName = (String) tSession.getValue("userName");
	String patientId = person.getPersonPId();
	String genderId = person.getPersonGender();
	String dob = person.getPersonDob();
	String gender = codeLst.getCodeDescription(EJBUtil.stringToNum(genderId));
	if (gender==null){ gender=""; }
	String yob = dob.substring(6,10);
	int age = cal1.get(Calendar.YEAR) - EJBUtil.stringToNum(yob);
	String visit = request.getParameter("visit");

	String eventId = (String) tSession.getValue("eventId");

	String eventName = request.getParameter("eventName");
	if(eventName == null){
		eventName = "";
	} else {
		eventName = eventName.replace('~',' ');
	}
	String crfId = "";
	String crfName = "";
	String crfNumber = "";

	crfId = request.getParameter("crfId");
	
		
	String crfNotifyId = "";
	String crfNotifyToName = "";
	String crfNotifyToId = "";
	String crfNotifyStatus = "";
	String crfNotifyCrfId = "";
	
	String dCrfNotifyStat = "";
	
	String studyTitle = "";
	String studyNumber = "";
	studyB.setId(EJBUtil.stringToNum(studyId));	
	
	studyB.getStudyDetails();
    studyTitle = studyB.getStudyTitle();
	studyNumber = studyB.getStudyNumber();
	
	patProtB.setPatProtId(EJBUtil.stringToNum(patProtId));
	patProtB.getPatProtDetails();
	protocolId =patProtB.getPatProtProtocolId();
	
	CrfDao crfDao = crfB.getCrfValues(EJBUtil.stringToNum(patProtId));
	ArrayList crfIds = crfDao.getCrfIds();
	ArrayList crfNumbers = crfDao.getCrfNumbers();
	String crfDD = "";
 
	
	String protName = "";
	if(protocolId != null) {
		eventAssocB.setEvent_id(EJBUtil.stringToNum(protocolId));
		eventAssocB.getEventAssocDetails();
		protName = 	eventAssocB.getName();
	} else {
		protocolId = "";
	}
	
	
	//if global setting=G or GR, user cannot enter data
	String globalSetting = alertNotifyB.getANGlobalFlag(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(protocolId));		

%>

<P class = "userName"> <%= uName %> </P>
<P class="sectionHeadings"> <%=MC.M_MngPats_CrfNotify%><%--Manage <%=LC.Pat_Patients%> >> CRF Notifications*****--%> </P>

<jsp:include page="patienttabs.jsp" flush="true"> 
<jsp:param name="studyVer" value="<%=studyVer%>"/>
<jsp:param name="patientCode" value="<%=patientId%>"/>
</jsp:include>


<table width=100%>
<tr>
	<td class=tdDefault width = 20% ><%=LC.L_Study_Number%><%--Study Number*****--%>:</td><td class=tdDefault><%=studyNumber%></td>
</tr>
<tr>
	<td class=tdDefault><%=LC.L_Study_Title%><%--Study Title*****--%>:</td><td class=tdDefault><%=studyTitle%></td>
</tr>
 <tr>
 	<td class=tdDefault ><%=LC.L_Protocol_Calendar%><%--Protocol Calendar*****--%>:</td><td class=tdDefault><%=protName%></td>
 </tr>
</table>

<%
	if(mode.equals("M")){
		crfNotifyId = request.getParameter("crfNotifyId");

		crfNotifyB.setCrfNotifyId(EJBUtil.stringToNum(crfNotifyId));
		crfNotifyB.getCrfNotifyDetails();
		
		crfNotifyToId = crfNotifyB.getCrfNotifyUsersTo();
		crfNotifyStatus = crfNotifyB.getCrfNotifyCodelstNotStatId();
		crfNotifyCrfId = crfNotifyB.getCrfNotifyCrfId();
		
		UserDao userDao = userB.getUsersDetails(crfNotifyToId);
	
		ArrayList fname = userDao.getUsrFirstNames();
		ArrayList lname = userDao.getUsrLastNames();
		for(i=0;i<fname.size();i++){
						System.out.println("length for first name is in crfnotify.jsp " + ((String)fname.get(i)).length() +"and " +  (((String)fname.get(i)).trim()).length());
			crfNotifyToName = crfNotifyToName + ((String)lname.get(i)).trim() + "," + ((String)fname.get(i)).trim() + ";";
		}
		crfNotifyToName = crfNotifyToName.substring(0,crfNotifyToName.length()-1);
		
		dCrfNotifyStat = cd1.toPullDown("crfStat",EJBUtil.stringToNum(crfNotifyStatus));
		
		crfDD = "<select name='crfId'> <option value=0 >"+LC.L_All/*All*****/+"</option>";
		for(i=0;i<crfDao.getCRows();i++) {
			tempCrfId = ((Integer)crfIds.get(i)).toString();
			if( tempCrfId.equals(crfNotifyCrfId)) {
				crfDD = crfDD + "<option value=" + crfIds.get(i) + " selected >" + crfNumbers.get(i) + "</option>";
			} else {
				crfDD = crfDD + "<option value=" + crfIds.get(i) + ">" + crfNumbers.get(i) + "</option>";
			}
		}
		crfDD = crfDD + "</select>"; 
	
	} else {
		dCrfNotifyStat = cd1.toPullDown("crfStat");
		crfDD = "<select name='crfId'> <option value=0 selected>"+LC.L_All/*All*****/+"</option>";
		for(i=0;i<crfDao.getCRows();i++) {
			crfDD = crfDD + "<option value=" + crfIds.get(i) + ">" + crfNumbers.get(i) + "</option>"; 
		}
		crfDD = crfDD + "</select>";
	}
%>
<%//changes by Vishal %>
<!--<Form name="crfNotify" method=post action="updatenewcrfnotify.jsp?">-->
	<Form name="crfNotify" method=post action="updatenewcrfnotify.jsp">
	<%//change end %>
	
	<p class = "sectionHeadings" ><%=LC.L_Crf_Notification%><%--CRF Notification*****--%></p>
	<table width=100%>
		<tr width=100%>
			<td width=35%>
				<%=LC.L_Send_MsgTo%><%--Send Message To *****--%> <FONT class="Mandatory">* </FONT>
			</td>
			<td width=65%>
				<input type=text name=crfNotifyToName maxlength=100 size=20 readonly value = '<%=crfNotifyToName%>' > 
				<A HREF=# onClick='openwin1(document.crfNotify)' ><%=LC.L_SelOrDeSel_Usrs%><%--Select/Deselect User(s)*****--%></A>
			</td> 
		</tr>
		<input type=hidden name=crfNotifyToId	value='<%=crfNotifyToId%>' >
		<tr>
			<td>
				<%=MC.M_WhenCRFStatus_ChgTo%><%--When CRF Status Changed To*****--%><FONT class="Mandatory">* </FONT>
			</td>
			<td>
				<%=dCrfNotifyStat%>
			</td> 
		</tr>
		<tr>
			<td>
				<%=LC.L_For_CrfNumber%><%--For CRF Number*****--%>
			</td>
			<td><%=crfDD%>
			</td> 
		</tr>
</table>		
<%if ((globalSetting.equals("N")) || (globalSetting.equals("NR"))) {%>
		
	<table width=100% >
	<tr>


	<td class=tdDefault>
	<%=LC.L_EHypenSign%><%--E-Signature*****--%> <FONT class="Mandatory">* </FONT>
	</td >
	<td class=tdDefault>
		<input type="password" name="eSign" maxlength="8">
<!--		<input type="image" src="../images/jpg/Submit.gif" align="absmiddle"  border="0"  onClick="return setVals('',document.crfNotify);">	
		<input type="image" src="../images/jpg/Add.gif" align="absmiddle" border="0" onClick="return setVals('Add',document.crfNotify);">-->
   	<A href='#' onClick="return validate(document.crfNotify,'');"><img src="../images/jpg/Submit.gif" border="0" align="absmiddle"></img></A>      
    <button onClick="return validate(document.crfNotify,'Add');"><%=LC.L_Submit_AddAnother%></button>
			
	</td>		
	</tr>
</table>
	
	
	
	
	
<%}%>
<input type=hidden name=crfNotifyId value=<%=crfNotifyId%>>
<input type=hidden name=mode value=<%=mode%>>
<input type=hidden name=srcmenu value=<%=src%>>
<input type=hidden name=page value=<%=page1%>>
<input type=hidden name=pkey value=<%=personPK%>>
<input type=hidden name=visit value=<%=visit%>>
<input type=hidden name=fromPage value=<%=fromPage%>>
<input type=hidden name=selectedTab value=<%=selectedTab%>>


<input type=hidden name=patProtId value='<%=patProtId%>'>
<input type=hidden name=studyId value='<%=studyId%>'>
<input type=hidden name=statDesc value='<%=statDesc%>'>
<input type=hidden name=statid value='<%=statid%>'>
<input type=hidden name=studyVer value='<%=studyVer%>'>
<input type=hidden name=patientCode value='<%=patientId%>'>
<input type=hidden name=protocolId value='<%=protocolId%>'>


<%
	eventName = eventName.replace(' ','~');
%>
<input type=hidden name=eventName value=<%=eventName%>>
			
</Form>	
<%
}//end of if body for session
else
{
%>
<jsp:include page="timeout.html" flush="true"/> 
<%
}
%>
<div>
<jsp:include page="bottompanel.jsp" flush="true"> 
</jsp:include>   
</div>
</div>
<DIV class="mainMenu" id = "emenu">
  <jsp:include page="getmenu.jsp" flush="true"/>   
</DIV>
</body>
</html>

