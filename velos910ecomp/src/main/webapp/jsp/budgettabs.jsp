<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="budgetJ" scope="page" class="com.velos.esch.web.budget.BudgetJB"/>
<%@ page language = "java" import = "com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,java.util.*"%>	
<%@ page import="com.velos.eres.web.objectSettings.ObjectSettingsCache,com.velos.eres.business.common.TeamDao, com.velos.eres.web.objectSettings.ObjectSettings,com.velos.eres.web.studyRights.StudyRightsJB"%>
<jsp:useBean id="stdRightsB" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />


<%
String mode="N";
String budgetId="";	
String selclass="";
String tab= request.getParameter("selectedTab");
mode= request.getParameter("mode");
budgetId=request.getParameter("budgetId");

String budgetTemplate = request.getParameter("budgetTemplate");
budgetTemplate=(budgetTemplate==null)?"":budgetTemplate;


int studyId= EJBUtil.stringToNum(request.getParameter("studyId"));

if ((request.getParameter("studyId")==null || studyId ==0 )&& EJBUtil.stringToNum(budgetId) >0) //study id is not passed, so check budget bean for study
{
	budgetJ.setBudgetId(EJBUtil.stringToNum(budgetId));
	budgetJ.getBudgetDetails();
	studyId = EJBUtil.stringToNum(budgetJ.getBudgetStudyId());
	
}
		
HttpSession tSession = request.getSession(true); 
String acc = (String) tSession.getValue("accountId");
if (sessionmaint.isValidSession(tSession))
{

		int mileGrpRight =0;
	
		GrpRightsJB budgetGrpRights = (GrpRightsJB) tSession.getValue("GRights");	
		mileGrpRight = Integer.parseInt(budgetGrpRights.getFtrRightsByValue("MILEST"));

out.print("<Input type=\"hidden\" name=\"mode\" value=\"" +mode+"\">");

	//KM-#4099	
	ArrayList teamId ;
	teamId = new ArrayList();
	int pageRight = 0;

	String userIdFromSession = (String) tSession.getValue("userId");
	TeamDao teamDao = new TeamDao();
	teamDao.getTeamRights(studyId,EJBUtil.stringToNum(userIdFromSession));
	teamId = teamDao.getTeamIds();

	if (teamId.size() <=0)
	{
		pageRight  = 0;
		StudyRightsJB stdRightstemp = new StudyRightsJB();
	}
	if (teamId.size() == 0) {
		pageRight=0 ;
	}else {
		stdRightsB.setId(EJBUtil.stringToNum(teamId.get(0).toString()));
	 	ArrayList teamRights ;
					teamRights  = new ArrayList();
					teamRights = teamDao.getTeamRights();

					stdRightsB.setSuperRightsStringForStudy((String)teamRights.get(0));
					stdRightsB.loadStudyRights();


		if ((stdRightsB.getFtrRights().size()) == 0){
			pageRight = 0;
		}else{
			pageRight = Integer.parseInt(stdRightsB.getFtrRightsByValue("MILESTONES"));
		}
	}

	
	ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
	ArrayList tabList = objCache.getAccountObjects(EJBUtil.stringToNum(acc), "budget_tab");
%>

<DIV>

<table  cellspacing="0" cellpadding="0" border="0"> 
	<tr>
		
	<%
	
	for (int iX=0; iX<tabList.size(); iX++) {
	ObjectSettings settings = (ObjectSettings)tabList.get(iX);

	if ("0".equals(settings.getObjVisible())) {
		continue;
	}

	boolean showThisTab = false;
	if ("1".equals(settings.getObjSubType())) {
		 showThisTab = true; 
	} 
	else if ("2".equals(settings.getObjSubType())) {
		showThisTab = true; 
		
	}
	else if ("3".equals(settings.getObjSubType())) {
		if ("P".equals(budgetTemplate) || "C".equals(budgetTemplate)) {
		    showThisTab = false;
		} else {
		    showThisTab = true;
		}
	}
	else if ("4".equals(settings.getObjSubType())) {
		if ("P".equals(budgetTemplate) || "C".equals(budgetTemplate)) {
		    showThisTab = false;
		} else {
		    showThisTab = true;
		}
	}
	else if ("5".equals(settings.getObjSubType())) {
		 if( (budgetTemplate.equals("P") ||  budgetTemplate.equals("C") ) && studyId>0 && mileGrpRight>=4 && pageRight > 0 ){
			showThisTab = true; 
	}
   }
	else {
		showThisTab = true;
	}

	if (!showThisTab) { continue; } 


	if (tab == null) { 
		selclass = "unselectedTab";
	} else if (tab.equals(settings.getObjSubType())) {
		selclass = "selectedTab";
	} else {
		selclass = "unselectedTab";
	}
	 %>

		<td  valign="TOP">

		<table class="<%=selclass%>"  cellspacing="0" cellpadding="0" border="0">
				<tr>  
			<!--    	<td class="<%=selclass%>" rowspan="4" valign="top" >
						<img src="../images/leftgreytab.gif" height=20 border=0 alt="">
					</td>  -->	
					<td class="<%=selclass%>">

					<%if ("1".equals(settings.getObjSubType())) {  %>
					<a href="budget.jsp?mode=<%=mode%>&budgetId=<%=budgetId%>&srcmenu=tdmenubaritem6&selectedTab=1"><%=settings.getObjDispTxt()%></a>
					<%} else if ("2".equals(settings.getObjSubType())) {
							if(budgetTemplate.equals("P") ||  budgetTemplate.equals("C")){
					%>
						<a href="patientbudget.jsp?mode=<%=mode%>&srcmenu=tdmenubaritem6&selectedTab=2&budgetId=<%=budgetId%>&budgetTemplate=<%=budgetTemplate%>"><%=settings.getObjDispTxt()%></a>

					<% } else if (budgetTemplate.equals("S")) { %>
						
						<a href="studybudget.jsp?mode=<%=mode%>&srcmenu=tdmenubaritem6&selectedTab=2&budgetId=<%=budgetId%>"><%=settings.getObjDispTxt()%></a>
					<%} else {%>
					<a> <%=settings.getObjDispTxt()%></a>
					<%} } else if ("3".equals(settings.getObjSubType())) {
					%>
					<a href="budgetapndx.jsp?mode=<%=mode%>&srcmenu=tdmenubaritem6&selectedTab=3&budgetId=<%=budgetId%>&budgetTemplate=<%=budgetTemplate%>&studyId=<%=studyId%>"><%=settings.getObjDispTxt()%></a>

					<%} else if ("4".equals(settings.getObjSubType())) {  %>
						<a href="budgetrights.jsp?mode=<%=mode%>&srcmenu=tdmenubaritem6&selectedTab=4&budgetId=<%=budgetId%>&budgetTemplate=<%=budgetTemplate%>&studyId=<%=studyId%>"><%=settings.getObjDispTxt()%></a>
					<%} else if ("5".equals(settings.getObjSubType())) { 
						if( (budgetTemplate.equals("P") ||  budgetTemplate.equals("C") ) && studyId>0 && mileGrpRight>=4 && pageRight > 0 ) { %>
							<a  id="setStudy" href="milestone.jsp?srcmenu=tdmenubaritem7&selectedTab=1&studyId=<%=studyId%>"><%=settings.getObjDispTxt()%></a>
					
					<%}}%>

					</td> 
			     <!--    <td class="<%=selclass%>" rowspan="3" valign="top">
						<img src="../images/rightgreytab.gif"  height=20 border=0 alt="">
			        </td> -->
			   	</tr> 
	   </table> 
	   </td>

	  <%
        } // End of tabList loop
      %>

	</tr>
   <!-- <tr>
     <td colspan=5 height=10></td>
  </tr>  -->
</table>
<table class="tabBorder" width="99%" height="5"><tr><td></td></tr></table>
</DIV>

<%}%>
