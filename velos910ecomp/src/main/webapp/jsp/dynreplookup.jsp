<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head> 

	<title><%=LC.L_Form_RptPreview%><%--Form Report Preview*****--%></title>
	<%@ page language = "java" import = "java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB"%>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<jsp:useBean id="dynrepB" scope="page" class="com.velos.eres.web.dynrep.DynRepJB" />
	<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
	<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
	 
	<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="resolution.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<SCRIPT  charset='utf-8'>

//added by sonia abrol, 12/22/05, so that same method can be used from lookup as well as adhoc export page
 
 function fixText(s){
  if (!s) return '';
  var arr = s.split('&#').join('').split(';');
  var s2='';
  for (var i=0;i<arr.length-1;i++){ //ignore last element, it's empty
    s2+=String.fromCharCode(arr[i]);
  }
  return s2;
}
	
	
	
	function setValue(formobj,outputStr,flag){
	 outputStr=decodeString(outputStr);
	 

    if (flag=="remove")
      outputStr=decodeString(formobj.outputString.value);
    var valueStr="",keyword="";
	
    var lform=fnTrimSpaces(formobj.lookupformName.value);
    
    //alert(outputStr);
    
    arrayStr=outputStr.split("[end]");
    
     //alert(arrayStr.length);
    var data,lfdatacolumn,startIndex,endIndex,index,tempKey,tempvalue,tempdata;
    
    for (var i=0;i<arrayStr.length;i++){
    if (flag=="remove"){
    startIndex=arrayStr[i].indexOf("[field]")+ 7;
   endIndex=arrayStr[i].indexOf("[keyword]");
   lfdatacolumn=arrayStr[i].substring(startIndex,endIndex);
   window.opener.document.forms[lform].elements[lfdatacolumn].value="";
    
    }else{
    
    //alert(arrayStr[i].substring(7,11));
    data="";
	startIndex=arrayStr[i].indexOf("[field]")+ 7;
	endIndex=arrayStr[i].indexOf("[keyword]");
	lfdatacolumn=arrayStr[i].substring(startIndex,endIndex);
	startIndex=endIndex+9;
	endIndex=arrayStr[i].indexOf("[dbcol]");
	keyword=arrayStr[i].substring(startIndex,endIndex);
	
        if (keyword.indexOf("[VELEXPR]")>=0){
	 valuearray=keyword.split("*");
	 for (var j=1;j<valuearray.length;j++){
	  valuearray[j]=fnTrimSpaces(valuearray[j].replace('[',''));
	  valuearray[j]=fnTrimSpaces(valuearray[j].replace(']',''));
	 if ((valuearray[j].indexOf("=")) >=0) 
	 tempKey=valuearray[j].substring(0,valuearray[j].indexOf("="));
	 else tempKey=valuearray[j];
	  tempvalue=valuearray[j].substring(valuearray[j].indexOf("=")+1,valuearray[j].length);
	  //alert(tempKey+"and"+tempvalue);
	 if (tempKey=="VELSTR")     data=data+tempvalue;
	 if (tempKey=="VELSPACE") data=data+' ';
	 if (tempKey=="VELCRET") data=data+"\r";
	 if (tempKey=="VELNLINE") data=data+"\n";
	 if (tempKey=="VELKEYWORD"){
	
	  index=valueStr.indexOf(tempvalue);
	   tempdata=valueStr.substring(index+tempvalue.length+1,valueStr.indexOf("]",index));
	    data=data+tempdata;
	  }	
	
	
	}
	} else {

	startIndex=arrayStr[i].indexOf("[value]")+7 ;
	endIndex=arrayStr[i].length ;
	data=arrayStr[i].substring(startIndex,endIndex);
	
	
	}
	
	if (data=="null") data="";
	//alert(data);
	
	if (data == "&#160;")
	{
		data = "";
	}

	window.opener.document.forms[lform].elements[lfdatacolumn].value = "";
	
	 
	
 	 
	
	window.opener.document.forms[lform].elements[lfdatacolumn].value=data;
	
	if (window.opener.document.forms[lform].elements[lfdatacolumn].value == "")
	{
		window.opener.document.forms[lform].elements[lfdatacolumn].value = "";
	}
	
	   ///////////////
	   	var formelem ;
    	formelem = document.getElementById('dynreptest');
    	
    	formelem.outputData.value = data;
    	formelem.openerForm.value = lform;
    	formelem.openerFld.value = lfdatacolumn;
    	
 		formelem.submit();
		 
    ////////////////
	
	
	
	valueStr=valueStr+"["+keyword+"="+data+"]";
    }
    }
    


    
    //window.opener.document.forms[lform].elements[lfdispcolumn].value=disp;
      
    //this.close();
//   	window.opener.document.testlookup.data.value=data ; 
//	window.opener.document.testlookup.disp.value=disp;
	
}	
	
	

</SCRIPT>

</head>
<% String src,sql="",tempStr="",sqlStr="",name="",selValue="",repFldStr="";
   int strlen=0,firstpos=-1,secondpos=-1;
src= request.getParameter("srcmenu");


%>





<body style="overflow:scroll;">


<%
	
		
	
	int seq=0;
	String eolString="";
	String mode = request.getParameter("mode");
	
	String value="",patientID="",studyNumber="",orderStr="",order="",filter="",dataOrder="",fltrDD="",formType="",pkVal="";
	
	int personPk=0,studyPk=0,repId=0,index=-1,pkId=0;;
	
	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))
	{ 
	
	String repIdStr=request.getParameter("repId");
	String fkeyword_str=request.getParameter("keyword");
	String formName=request.getParameter("form");
	String patcodeFilter = request.getParameter("patcode");
	if (StringUtil.isEmpty(patcodeFilter))
	{
		patcodeFilter = "";
	}
		
	formName=(formName==null)?"":formName;
	if (repIdStr==null) repIdStr="";
	if (fkeyword_str==null) fkeyword_str="";
	
	
 	  	 
%>

<form name="dynreptest" id = "dynreptest"  action="postlookup.jsp" method="post">
 <input type="hidden" name="outputData" value="">
 <input type="hidden" name="openerForm" value="">
 <input type="hidden" name="openerFld" value="">

 </form>
 
 

<!--<form name="dynrep">
<input type="hidden" name="keyword" value="<%=fkeyword_str%>"> -->


 
<P class="sectionHeadings"> <%=LC.L_Adhoc_QueryLookup%><%--Ad-Hoc query Lookup*****--%> </P>
 	
 <jsp:include page="mdynexport.jsp" flush="true">
     	<jsp:param name="from" value="browse"/>
		<jsp:param name="exp" value="H"/>
		<jsp:param name="repId" value="<%=repIdStr%>"/>
		<jsp:param name="lookupMode" value="yes"/>
		<jsp:param name="fkeyword_str" value="<%=fkeyword_str%>"/>
		<jsp:param name="lookupformName" value="<%=formName%>"/>
		<jsp:param name="patcode" value="<%=patcodeFilter%>"/>
		
</jsp:include>




 



<%

} else {  //else of if body for session

%>
  <jsp:include page="timeout.html" flush="true"/>

  <%
}
 
	%>
	
	
  

 


</body>
</html>
