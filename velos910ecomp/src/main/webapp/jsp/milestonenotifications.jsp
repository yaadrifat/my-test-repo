<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Milestone_Notifications%><%--Milestone >> Notifications*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<SCRIPT>

	function AddMilepayment(formObj,windowName,pgRight)
	{
	if (f_check_perm(pgRight,'N') == true) {
      windowName= window.open(windowName,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=250")
      windowName.focus();
	}else {
		return false;
	}

	}


function openwin1(milestoneId, milestoneUserNames, milestoneUserIds, studyId, pgRight) {
		if (f_check_perm(pgRight,'E') == true) {
	  var windowToOpen = "multipleusersearchdetailswithsave.jsp?fname=&lname=&from=milestonenotify&mode=initial&milestoneId=" + milestoneId + "&milestoneUserIds=" + milestoneUserIds + "&milestoneUserNames=" + milestoneUserNames +"&studyId=" +studyId ;
      window.open(windowToOpen,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200");

		}else {
		return false;
	}
	  }
</SCRIPT>



<% String src;
src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0">
<!-- Bug NO: 3976 fixed by PK -->
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"> </div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
<jsp:useBean id="milestoneB" scope="request" class="com.velos.eres.web.milestone.MilestoneJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="stdRightsB" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />

<%@ page language = "java" import = "com.velos.eres.service.util.StringUtil,com.velos.eres.business.common.MilestoneDao,com.velos.eres.business.common.TeamDao,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.DateUtil,com.velos.eres.service.util.*"%>

<%
 String selectedTab = request.getParameter("selectedTab");
 String study = request.getParameter("studyId");
%>

<DIV class="BrowserTopn" id="div1">
  <jsp:include page="milestonetabs.jsp" flush="true">
	<jsp:param name="studyId" value="<%=study%>"/>
  </jsp:include>
</div>
<DIV class="BrowserBotN BrowserBotN_M_2" id="div2">

  <%
   HttpSession tSession = request.getSession(true);
   if (sessionmaint.isValidSession(tSession))
	{
	String userIdFromSession = (String) tSession.getValue("userId");
    int studyId = EJBUtil.stringToNum(study);

    String pageRight= "";
    int pgRight = 0;


	pageRight = (String) tSession.getValue("mileRight");
    pgRight = EJBUtil.stringToNum(pageRight);

    if (pgRight > 0 ){
		MilestoneDao milestoneDao = milestoneB.getMilestoneRows(studyId, "PM", "","");

		ArrayList milestoneIds = milestoneDao.getMilestoneIds();

    	ArrayList milestoneRuleDescs= milestoneDao.getMilestoneRuleDescs();
    	//ArrayList milestoneAmounts=milestoneDao.getMilestoneAmounts();
    	ArrayList milestoneCounts= milestoneDao.getMilestoneCounts();
    	ArrayList milestoneUsers= milestoneDao.getMilestoneUsers();
    	ArrayList milestoneUserIds = milestoneDao.getMilestoneUserIds();
    	ArrayList milestoneVisitNames = milestoneDao.getProtocolVisitNames();

     	int milestoneId = 0;
    	String milestoneTypeOld = "";
    	String milestoneType= "";
    	String milestoneCalId= "";
    	String milestoneCalDesc="" ;
    	String milestoneRuleId= "";
    	String milestoneRuleDesc= "";
    	String milestoneAmount="";
    	String milestoneVisit= "";
    	String milestoneEvent= "";
    	String milestoneEventDesc= "";
    	String milestoneCount= "";
    	String milestoneUser= "";
    	String milestoneUserId= "";

		String milestoneHead="";
		String milestoneDesc=""	;
		String hrefVal="";
		boolean pmExist = false;
		boolean vmExist = false;
		boolean emExist = false;

	    int len = milestoneIds.size();
		String protocolVisitName = ""; //SV, 10/28
		int totalProtocolVisits=0; //SV, 10/11

%>
	<Form name="milestone" method="post" action="" onsubmit="">
    <Input type="hidden" name="selectedTab" value="<%=selectedTab%>">
    <table width="100%" cellspacing="0" cellpadding="0" border=0 >
	  <tr>
        <td width = "70%">
          <P class = "defComments">
		  	<%=MC.M_UsrSentNotfics_MstoneAchv%><%--The following users will be sent notifications when the corresponding milestone is achieved, based on the specified Milestone Rule*****--%>
          </P>
        </td>
      </tr>
      <tr>
        <td height="10"></td>
        <td height="10"></td>
      </tr>
    </table>
        </table>

    <table width="100%" >
      <tr>
        <th width="35%"> <%=LC.L_Milestone_Achieved%><%--Milestone Achieved*****--%> </th>
        <th width="40%"> <%=LC.L_Notify_Users%><%--Notify Users*****--%> </th>
        <th width="25%">&nbsp; </th>
      </tr>

	  <tr>
  	    <td>
		<!-- Modified By parminder singh Bug#10127-->
			<B class="sectionHeadingsFrm" style="padding-bottom: 1px;">
			<%=LC.L_Pat_StatusMstones%><%--<%=LC.Pat_Patient%> Status Milestones*****--%> 
			</B>
		</td>
		<td colspan=2>&nbsp;</td>
	  </tr>
    <%
    for(int counter = 0;counter<len;counter++)
	{
		milestoneId=EJBUtil.stringToNum(((milestoneIds.get(counter)) == null)?"-":(milestoneIds.get(counter)).toString());
		milestoneRuleDesc=(((milestoneRuleDescs.get(counter)) == null)?"-":(milestoneRuleDescs.get(counter)).toString());

		milestoneCount=(((milestoneCounts.get(counter)) == null)?"-":(milestoneCounts.get(counter)).toString());

		milestoneUser=(((milestoneUsers.get(counter)) == null)?"-":(milestoneUsers.get(counter)).toString());
		milestoneUserId = (((milestoneUserIds.get(counter)) == null)?"0":(milestoneUserIds.get(counter)).toString());


			milestoneDesc = "Patient Count "+ milestoneCount;

			if ((counter%2)==0) {
  %>
	      <tr class="browserEvenRow">
        <%
			}
			else{
  %>
	      <tr class="browserOddRow">
    	<%
			}
  %>
			<td><%=milestoneRuleDesc%></td>
    	    <td> <%=milestoneUser%> </td>
<%
			milestoneUser = milestoneUser.equals("-")?"0":milestoneUser;
		  	milestoneUser = milestoneUser.replace(' ','~');

///			String str = "openwin1(" + milestoneId + ",'" + milestoneUser + "','" + milestoneUserId + "'," + studyId + ")";
/// by salil
			String str = "openwin1(" + milestoneId + ",'" +  StringUtil.escapeSpecialCharJS(milestoneUser) + "','" + milestoneUserId + "'," + studyId +"," + pgRight + ")";
%>

        	<td> <A href=# onClick="<%=str%>"><%=LC.L_SelOrDeSel_Usrs%><%--Select/Deselect User(s)*****--%></A></td>
	      </tr>
   <%

	} //for loop
%>

	<tr>
		<td>
		<!-- Modified By parminder singh Bug#10127-->
			<B class="sectionHeadingsFrm" style="padding-bottom: 1px;">
				<%=LC.L_Visit_Mstones%><%--Visit Milestones*****--%>
			</B>
			</td>
			<td colspan=2>&nbsp;</td>
	</tr>
    <%
		    milestoneDao = milestoneB.getMilestoneRows(studyId, "VM", "","");

			milestoneIds = milestoneDao.getMilestoneIds();

		    milestoneRuleDescs= milestoneDao.getMilestoneRuleDescs();
		    milestoneCounts= milestoneDao.getMilestoneCounts();
		    milestoneUsers= milestoneDao.getMilestoneUsers();
			milestoneUserIds = milestoneDao.getMilestoneUserIds();

			len = milestoneIds.size();

    for(int counter = 0;counter<len;counter++)
	{

		milestoneId=EJBUtil.stringToNum(((milestoneIds.get(counter)) == null)?"-":(milestoneIds.get(counter)).toString());
		milestoneRuleDesc=(((milestoneRuleDescs.get(counter)) == null)?"-":(milestoneRuleDescs.get(counter)).toString());
		//milestoneAmount=(((milestoneAmounts.get(counter)).equals("null"))?"No Associated Amount":(milestoneAmounts.get(counter)).toString());
		milestoneCount=(((milestoneCounts.get(counter)) == null)?"-":(milestoneCounts.get(counter)).toString());

		milestoneUser=(((milestoneUsers.get(counter)) == null)?"-":(milestoneUsers.get(counter)).toString());
		milestoneUserId = (((milestoneUserIds.get(counter)) == null)?"0":(milestoneUserIds.get(counter)).toString());


			if ((counter%2)==0) {
  %>
    	  <tr class="browserEvenRow">
        <%
			}
			else{
  %>
    	  <tr class="browserOddRow">
        <%
			}
  %>
			<td><%=milestoneRuleDesc%></td>
    	    <td> <%=milestoneUser%> </td>
<%
			milestoneUser = milestoneUser.equals("-")?"0":milestoneUser;
		  	milestoneUser = milestoneUser.replace(' ','~');

			String str = "openwin1(" + milestoneId + ",'" +  StringUtil.escapeSpecialCharJS(milestoneUser) + "','" + milestoneUserId + "'," + studyId + "," + pgRight +")";
%>
	        <td> <A href=# onClick="<%=str%>"><%=LC.L_SelOrDeSel_Usrs%><%--Select/Deselect User(s)*****--%></A></td>
	      </tr>
   <%

	} //for loop
%>

	<tr>
		<td>
		<!-- Modified By parminder singh Bug#10127-->
			<B class="sectionHeadingsFrm" style="padding-bottom: 1px;">
				<%=LC.L_Evt_Mstones%><%--Event Milestones*****--%>
			</B>
			</td>
			<td colspan=2>&nbsp;</td>
	</tr>
    <%
    milestoneDao = milestoneB.getMilestoneRows(studyId, "EM", "","");

	milestoneIds = milestoneDao.getMilestoneIds();

    milestoneRuleDescs= milestoneDao.getMilestoneRuleDescs();

    milestoneCounts= milestoneDao.getMilestoneCounts();
    milestoneUsers= milestoneDao.getMilestoneUsers();
	milestoneUserIds = milestoneDao.getMilestoneUserIds();

	len = milestoneIds.size();

    for(int counter = 0;counter<len;counter++)
	{
		milestoneId=EJBUtil.stringToNum(((milestoneIds.get(counter)) == null)?"-":(milestoneIds.get(counter)).toString());
		milestoneRuleDesc=(((milestoneRuleDescs.get(counter)) == null)?"-":(milestoneRuleDescs.get(counter)).toString());
		//milestoneAmount=(((milestoneAmounts.get(counter)).equals("null"))?"No Associated Amount":(milestoneAmounts.get(counter)).toString());
		milestoneCount=(((milestoneCounts.get(counter)) == null)?"-":(milestoneCounts.get(counter)).toString());

		milestoneUser=(((milestoneUsers.get(counter)) == null)?"-":(milestoneUsers.get(counter)).toString());
		milestoneUserId = (((milestoneUserIds.get(counter)) == null)?"0":(milestoneUserIds.get(counter)).toString());


			if ((counter%2)==0) {
  %>
	      <tr class="browserEvenRow">
        <%
			}
			else{
  %>
	      <tr class="browserOddRow">
        <%
			}
  %>
			<td><%=milestoneRuleDesc%></td>
    	    <td> <%=milestoneUser%> </td>
<%
			milestoneUser = milestoneUser.equals("-")?"0":milestoneUser;
		  	milestoneUser = milestoneUser.replace(' ','~');

			String str = "openwin1(" + milestoneId + ",'" +  StringUtil.escapeSpecialCharJS(milestoneUser) + "','" + milestoneUserId + "'," + studyId + "," + pgRight +")";
%>
	        <td> <A href=# onClick="<%=str%>"><%=LC.L_SelOrDeSel_Usrs%><%--Select/Deselect User(s)*****--%></A></td>
	      </tr>
   <%

	} //for loop
%>
<tr>
		<td>
		<!-- Modified By parminder singh Bug#10127-->
			<B class="sectionHeadingsFrm" style="padding-bottom: 1px;">
				<%=LC.L_Std_Mstones%><%--<%=LC.Std_Study%> Milestones*****--%>
			</B>
			</td>
			<td colspan=2>&nbsp;</td>
	</tr>
 <%

	 milestoneDao = milestoneB.getMilestoneRows(studyId, "SM", "","");

	milestoneIds = milestoneDao.getMilestoneIds();

    milestoneRuleDescs= milestoneDao.getMilestoneRuleDescs();

    milestoneCounts= milestoneDao.getMilestoneCounts();
    milestoneUsers= milestoneDao.getMilestoneUsers();
	milestoneUserIds = milestoneDao.getMilestoneUserIds();

	len = milestoneIds.size();

    for(int counter = 0;counter<len;counter++)
	{
		milestoneId=EJBUtil.stringToNum(((milestoneIds.get(counter)) == null)?"-":(milestoneIds.get(counter)).toString());
		milestoneRuleDesc=(((milestoneRuleDescs.get(counter)) == null)?"-":(milestoneRuleDescs.get(counter)).toString());
		//milestoneAmount=(((milestoneAmounts.get(counter)).equals("null"))?"No Associated Amount":(milestoneAmounts.get(counter)).toString());
		milestoneCount=(((milestoneCounts.get(counter)) == null)?"-":(milestoneCounts.get(counter)).toString());

		milestoneUser=(((milestoneUsers.get(counter)) == null)?"-":(milestoneUsers.get(counter)).toString());
		milestoneUserId = (((milestoneUserIds.get(counter)) == null)?"0":(milestoneUserIds.get(counter)).toString());


			milestoneDesc = "study Milestone";

			if ((counter%2)==0) {
  %>
	      <tr class="browserEvenRow">
        <%
			}
			else{
  %>
	      <tr class="browserOddRow">
    	<%
			}
  %>
			<td><%=milestoneRuleDesc%></td>
    	    <td> <%=milestoneUser%> </td>
<%
			milestoneUser = milestoneUser.equals("-")?"0":milestoneUser;
		  	milestoneUser = milestoneUser.replace(' ','~');

///			String str = "openwin1(" + milestoneId + ",'" + milestoneUser + "','" + milestoneUserId + "'," + studyId + ")";
/// by salil
			String str = "openwin1(" + milestoneId + ",'" +  StringUtil.escapeSpecialCharJS(milestoneUser) + "','" + milestoneUserId + "'," + studyId +"," + pgRight + ")";
%>

        	<td> <A href=# onClick="<%=str%>"><%=LC.L_SelOrDeSel_Usrs%><%--Select/Deselect User(s)*****--%></A></td>
	      </tr>
   <%

	} //for loop
%>


<tr>
		<td>
		<!-- Modified By parminder singh Bug#10127-->
			<B class="sectionHeadingsFrm" style="padding-bottom: 1px;">
				<%=LC.L_Addl_Mstones%><%--Additional Milestones*****--%>
			</B>
			</td>
			<td colspan=2>&nbsp;</td>
	</tr>
 <%

	milestoneDao = milestoneB.getMilestoneRows(studyId, "AM", "","");

	milestoneIds = milestoneDao.getMilestoneIds();

    milestoneUsers= milestoneDao.getMilestoneUsers();
	milestoneUserIds = milestoneDao.getMilestoneUserIds();
	ArrayList milestoneDescriptions = milestoneDao.getMilestoneDescriptions();//KM
	String milestoneDescription ="";


	len = milestoneIds.size();

    for(int counter = 0;counter<len;counter++)
	{
		milestoneId=EJBUtil.stringToNum(((milestoneIds.get(counter)) == null)?"-":(milestoneIds.get(counter)).toString());
		milestoneUser=(((milestoneUsers.get(counter)) == null)?"-":(milestoneUsers.get(counter)).toString());
		milestoneUserId = (((milestoneUserIds.get(counter)) == null)?"0":(milestoneUserIds.get(counter)).toString());
		milestoneDescription=(((milestoneDescriptions.get(counter)) == null)?"-":(milestoneDescriptions.get(counter)).toString());

		if(milestoneDescription.length() > 500) { //KM
			   milestoneDescription = milestoneDescription.substring(0,500)+"...";
		}


			if ((counter%2)==0) {
  %>
	      <tr class="browserEvenRow">
        <%
			}
			else{
  %>
	      <tr class="browserOddRow">
    	<%
			}
  %>
			<td><%=milestoneDescription%></td>
    	    <td> <%=milestoneUser%> </td>
<%
			milestoneUser = milestoneUser.equals("-")?"0":milestoneUser;
		  	milestoneUser = milestoneUser.replace(' ','~');

			String str = "openwin1(" + milestoneId + ",'" +  StringUtil.escapeSpecialCharJS(milestoneUser) + "','" + milestoneUserId + "'," + studyId +"," + pgRight + ")";
%>

        	<td> <A href=# onClick="<%=str%>"><%=LC.L_SelOrDeSel_Usrs%><%--Select/Deselect User(s)*****--%></A></td>
	      </tr>
   <%

	} //for loop
%>

	</table>
  </Form>


  <%
	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id="emenu">
  	<jsp:include page="getmenu.jsp" flush="true"/>
</div>
</body>

</html>

