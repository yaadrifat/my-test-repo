<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@ page language="java" import="com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.objectSettings.ObjectSettingsCache, com.velos.eres.web.objectSettings.ObjectSettings"%>
<%@ page language="java" import="com.velos.eres.business.ulink.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.EJBUtil"%>
<%@ page language="java" import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*" %>

<html>
<head>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%
ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
ArrayList tabList = null;
String titleStr = "";
if (sessionmaint.isValidSession(request.getSession(true)))
{
    String accId = (String) request.getSession(true).getValue("accountId");
    
    ArrayList tempList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "top_menu");
    for (int iX=0; iX<tempList.size(); iX++) {
        ObjectSettings settings = (ObjectSettings) tempList.get(iX);
        if ("irb_menu".equals(settings.getObjSubType())) {
            if (settings.getObjDispTxt() != null) {
                titleStr += settings.getObjDispTxt();
            }
            break;
        } 
    }
    tempList = null;

    tempList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "irb_menu");
    for (int iX=0; iX<tempList.size(); iX++) {
        ObjectSettings settings = (ObjectSettings) tempList.get(iX);
        if ("pend_menu".equals(settings.getObjSubType())) {
            if (settings.getObjDispTxt() != null) {
                titleStr += " >> " + settings.getObjDispTxt();
            }
            break;
        } 
    }
    tempList = null;
    
    tabList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "irb_pend_tab");
    for (int iX=0; iX<tabList.size(); iX++) {
        ObjectSettings settings = (ObjectSettings) tabList.get(iX);
        if (settings.getObjSubType().equals(request.getParameter("selectedTab"))) {
        	Object[] arguments = {titleStr,settings.getObjDispTxt()};
            titleStr = VelosResourceBundle.getLabelString("L_Gtgt",arguments);/*titleStr += " >> "+ settings.getObjDispTxt();*****/
        }
    }
}
%>
<title><%=titleStr %></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<% 
String src= request.getParameter("srcmenu");
HttpSession tSession = request.getSession(true); 
String accountId = "";
String studyId = "";
if (sessionmaint.isValidSession(tSession))
{
	
	int pendingGrpRight = 0;
	int irbActionReqGroupRight=  0;
	int irbPendingRevGroupRight  = 0;
	
			  
	//copy a new study is controlled by Manage Protocols new group right
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
	pendingGrpRight = Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_APP"));

	irbActionReqGroupRight =  Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_AR"));
	irbPendingRevGroupRight =  Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_PR"));
	
	
	
  accountId = (String) tSession.getValue("accountId");
  studyId = (String) tSession.getValue("studyId");
   
 
%>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<SCRIPT language="javascript">
function submitForm(formobj) {
	formobj.action="updateSubmission.jsp?&srcmenu=<%=request.getParameter("srcmenu")%>&selectedTab=irb_init_tab"; 
	return true;
}
</SCRIPT> 
<body>

<div id="overDiv" style="position:absolute;visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<%
  String from = "version";
  String tab = request.getParameter("selectedTab");
  String studyIdForTabs = request.getParameter("studyId");
  String absolute = request.getParameter("absolute");
  String includeTabsJsp = "irbpendtabs.jsp";
  
  boolean showThisTab = false;
  int submissionBrowserRight = 0;
  
  if ("irb_act_tab".equals(tab) )
		{
			if (irbActionReqGroupRight >= 4 && pendingGrpRight >= 4)
			{
				showThisTab = true;
				submissionBrowserRight = irbActionReqGroupRight < pendingGrpRight ?
				        irbActionReqGroupRight : pendingGrpRight;
			}
			else if (!"Y".equals(absolute))
			{
				tab="irb_items_tab";
			}	
		}
		
	if ("irb_items_tab".equals(tab))
		{
			if (irbPendingRevGroupRight >= 4 && pendingGrpRight >= 4)
			{
				showThisTab = true;
				submissionBrowserRight = irbPendingRevGroupRight < pendingGrpRight ?
				        irbPendingRevGroupRight : pendingGrpRight;
			}
			else if (!"Y".equals(absolute))
			{
				tab="irb_saved_tab";
			}
		}
		
  	if ("irb_saved_tab".equals(tab) )
		{
			if (pendingGrpRight >= 4)
			{
				showThisTab = true;
				submissionBrowserRight = pendingGrpRight;
			}
		 	
		}
		
  	if ("irb_appr_tab".equals(tab) )
	{
		if (pendingGrpRight >= 4)
		{
			showThisTab = true;
			submissionBrowserRight = pendingGrpRight;
		}
	 	
	}
		
		if (!showThisTab )
		{
			tab="";
		}
		
%>
<DIV class="BrowserTopn" id = "divtab">
<jsp:include page="<%=includeTabsJsp%>" flush="true">
  <jsp:param name="from" value="<%=from%>"/> 
  <jsp:param name="selectedTab" value="<%=tab%>"/>
  <jsp:param name="studyId" value="<%=studyIdForTabs%>"/>
</jsp:include>
</DIV>
<DIV class="BrowserBotN  BrowserBotN_RC_2" id = "div1">
<%
     String usrId = (String) tSession.getValue("userId");
    String uName = (String) tSession.getValue("userName");
    String defUserGroup = (String) tSession.getAttribute("defUserGroup");
    
    if (showThisTab )
    {
  %>

    <jsp:include page="submissionBrowser.jsp" flush="true">
	      <jsp:param name="tabsubtype" value="<%=tab%>"/>
	      <jsp:param name="tabtype" value="irbPend"/>
	      <jsp:param name="submissionBrowserRight" value="<%=submissionBrowserRight%>"/> 
   </jsp:include>

  <%
  	}
  	else
  	{
  		%>
			<jsp:include page="accessdenied.jsp" flush="true"/>
			<%
  	
  	}
  }//end of if body for session
  else
  {
  %>
    <jsp:include page="timeout.html" flush="true"/>
  <%
  }
  %>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
</body>

</html>

