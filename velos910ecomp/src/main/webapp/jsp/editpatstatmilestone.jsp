<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>
<HTML>
<HEAD>
<title><%=LC.L_Pat_StatusMstones%><%--Patient Status Milestones*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>

<Link Rel=STYLESHEET HREF="common.css" type=text/css>
</HEAD>

<script>




function test(formobj) {

    mode = formobj.mode.value;	
 	 	if (!(validate_col('e-Signature',formobj.eSign))) return false
	
    if(isNaN(formobj.eSign.value) == true) {
    	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
	   }

return true;
} 
</script>

<BODY>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.esch.business.common.*"%>
<jsp:useBean id="milestoneB" scope="request" class="com.velos.eres.web.milestone.MilestoneJB"/>

<%
	HttpSession tSession = request.getSession(true); 
  String studyId = "";
  String mode = "M";
	 if (sessionmaint.isValidSession(tSession))
	{

		//String mode = request.getParameter("mode");
		//String studyId = request.getParameter("studyId");
		
	}
	%>

<Form class=formDefault name="PatientStatus" action="updatepatstatmilestone.jsp" method="post" onSubmit="return test(document.PatientStatus)" >

<table class=tableDefault  width="95%" border="0">
<tr>
	<th width=25% ><%=MC.M_When_PatCntEquals%><%--When <%=LC.Pat_Patient%> Count Equals*****--%> </th>
	<th width=40% ><%=LC.L_Milestone_Rule%><%--Milestone Rule*****--%> </th>
	<th width=30% ><%=LC.L_Amount%><%--Amount*****--%></th>
</tr>

<%
int totalrows = 0;
String patCount="";
String patAmount="";
String ruleDesc = "";
String mileRule = "";
String addFlag = "";
ArrayList counts = null;
ArrayList ruleDescs = null;
ArrayList amounts = null;



MilestoneDao mileDao = new MilestoneDao();
mileDao.getMilestones(studyId,"PM");
counts = mileDao.getMilestoneCounts();
ruleDescs = mileDao.getMilestoneRuleDescs();
amounts = mileDao.getMilestoneAmounts();
totalrows = ruleDescs.size();

//CodeDao codeLst = new CodeDao();
//codeLst.getCodeValues("PM");
//dCur = codeLst.toPullDown("patRule");




for(int i=0;i<totalrows;i++){
  patCount = (String)counts.get(i);
  patAmount = (String)amounts.get(i);
  ruleDesc = (String)ruleDescs.get(i);

 %>
 
 <tr> 
	<td align=center>
	<input type=text name=count size=10 maxlength=10 class="leftAlign" value=<%=patCount%>>
	</td>
	<td>
  <input type=text name=patRule size=10 maxlength=10 class="leftAlign" value=<%=ruleDesc%>>
	</td>
	<td align=center>
	<input type=text name=amount size=10 maxlength=10 class="leftAlign" value=<%=patAmount%>>
	</td>
</tr> 
 
 
 <%
}
%>

<INPUT NAME="mode" TYPE=hidden  value=<%=mode%>> 
<INPUT NAME="studyId" TYPE=hidden  value=<%=studyId%>> 
<input type=hidden name=addFlag value=<%=addFlag%>>

</TABLE>
<br><br>
<table width=100% >
	<tr>
	<td class=tdDefault>
	<%=LC.L_EHypenSign%><%--E-Signature*****--%> <FONT class="Mandatory">* </FONT>
	</td >
	<td class=tdDefault>
		<input type="password" name="eSign" maxlength="8">
		<input type="image" src="../images/jpg/Submit.gif" align="absmiddle"  border="0" >	
	</td>		

	</tr>
</table>


</Form>



</BODY>


