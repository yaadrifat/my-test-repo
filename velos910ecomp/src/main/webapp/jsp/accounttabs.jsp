<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.common.CtrlDao,java.util.*,java.io.*,org.w3c.dom.*"%>
<%@ page import="com.velos.eres.web.objectSettings.ObjectSettingsCache, com.velos.eres.web.objectSettings.ObjectSettings,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB"%>


<%!
  private String getHrefBySubtype(String subtype) {
      if (subtype == null) { return "#"; }
      if ("1".equals(subtype)) {
          return "sitebrowser.jsp?srcmenu=tdMenuBarItem2&selectedTab=1";
      }
      if ("2".equals(subtype)) {
          return "groupbrowserpg.jsp?srcmenu=tdMenuBarItem2&selectedTab=2";
      }
      if ("3".equals(subtype)) {
          return "accountbrowser.jsp?srcmenu=tdMenuBarItem2&selectedTab=3";
      }
      if ("4".equals(subtype)) {
          return "accountlinkbrowser.jsp?srcmenu=tdMenuBarItem2&selectedTab=4";
      }

      if ("5".equals(subtype)) {
          return "accountforms.jsp?srcmenu=tdMenuBarItem2&selectedTab=5";
      }

      if ("6".equals(subtype)) {
          return "portal.jsp?srcmenu=tdMenuBarItem2&selectedTab=6";
      }

      return "#";
  }
%>


<%
String mode="M";
String selclass;
String userId="";	
String acc="";	
String tab= request.getParameter("selectedTab");
HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession))
{
	acc = (String) tSession.getValue("accountId");
	userId = (String) tSession.getValue("userId");	
}
	//JM : 13Aug2007: issue #3028
	String modRight = (String) tSession.getValue("modRight");
	int patientPortalSeq = 0,patientPortalModSeq = 0 ;
	char portalRight = '0';

	CtrlDao modCtlDao = new CtrlDao();
	modCtlDao.getControlValues("module"); 

	ArrayList modCtlDaofeature =  modCtlDao.getCValue();
	ArrayList modCtlDaoftrSeq = modCtlDao.getCSeq();

	 int patProfileSeq = 0, formLibSeq = 0;		
	 char formLibAppRight = '0';
	 char patProAppRight = '0';
	 formLibSeq = modCtlDaofeature.indexOf("MODFORMS");
	 patProfileSeq = modCtlDaofeature.indexOf("MODPATPROF");	
	 formLibSeq = ((Integer) modCtlDaoftrSeq.get(formLibSeq)).intValue();
	 patProfileSeq = ((Integer) modCtlDaoftrSeq.get(patProfileSeq)).intValue();
	 formLibAppRight = modRight.charAt(formLibSeq - 1);
	 patProAppRight = modRight.charAt(patProfileSeq - 1);

	patientPortalModSeq = modCtlDaofeature.indexOf("MODPPORTAL");
	patientPortalSeq = ((Integer) modCtlDaoftrSeq.get(patientPortalModSeq)).intValue();
	portalRight = modRight.charAt(patientPortalSeq - 1);
	
	ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
	ArrayList tabList = objCache.getAccountObjects(EJBUtil.stringToNum(acc), "acct_tab");
%>

<!-- <table  class= "selectedTab" cellspacing="0" cellpadding="0" border="0"> -->
<table  cellspacing="0" cellpadding="0" border="0">
	<tr>
	<%
			GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");		
			int pageRight = 0;

			for (int iX=0; iX<tabList.size(); iX++) {
            ObjectSettings settings = (ObjectSettings)tabList.get(iX);

    		if ("0".equals(settings.getObjVisible())) {
    		    continue;
    		}

            boolean showThisTab = false;

		 if ("1".equals(settings.getObjSubType())) {
			 pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MSITES"));
			 if (pageRight > 0) {showThisTab = true; }
		 }   
		 else if ("2".equals(settings.getObjSubType())) {
			pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CGRP"));
			if (pageRight > 0 )	{showThisTab = true; }
		 }	
		 else if ("3".equals(settings.getObjSubType())) {
			pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MUSERS"));
			if (pageRight > 0 )	{showThisTab = true; }
		 }	
		 else if ("4".equals(settings.getObjSubType())) {
			pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MACCLINKS"));
			if (pageRight > 0 )	{showThisTab = true; }
		 }
		 else if ("5".equals(settings.getObjSubType())) {
			int account_form_rights = Integer.parseInt(grpRights.getFtrRightsByValue("MACCFRMS"));
			if(	((String.valueOf(formLibAppRight).compareTo("1") == 0)&&(account_form_rights>=4))  ||
				((String.valueOf(patProAppRight).compareTo("1") == 0)&&(account_form_rights>=4)) ) {	
					showThisTab = true; 
			}	
		 }
		else if ("6".equals(settings.getObjSubType())) {
				pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("PORTALAD")); //KM-16jul09
    		    if (String.valueOf(portalRight).compareTo("1") == 0 && (pageRight >=4))
    		    {
    		        showThisTab = true; 
    		    }
    		
    		} else {
    		    showThisTab = true;
    		}

            if (!showThisTab) { continue; } 

		
			if (tab == null) { 
                selclass = "unselectedTab";
    		} else if (tab.equals(settings.getObjSubType())) {
                selclass = "selectedTab";
            } else {
                selclass = "unselectedTab";
            }
	 %>
			<td  valign="TOP">
			<table class="<%=selclass%>"  cellspacing="0" cellpadding="0" border="0">
				<tr>
				  <!-- <td class="<%=selclass%>" rowspan="3" valign="top" wclassth="7">
						<img src="../images/leftgreytab.gif" width=8 height=20 border=0 alt="">
					</td>   --> 
					<td class="<%=selclass%>">
						<a href="<%=getHrefBySubtype(settings.getObjSubType())%>"><%=settings.getObjDispTxt()%></a>
					</td> 
			     <!--     <td rowspan="3" valign="top" wclassth="7">
						<img src="../images/rightgreytab.gif" wclassth=7 height=20 border=0 alt="">
			        </td>   --> 
			   	</tr> 
		   	</table> 
        </td> 
     <%
        } // End of tabList loop
      %>
	</tr>
	 
	<!--   <tr>
	     <td colspan=6 height=10></td>
	</tr> --> 
</table>
<table class="tabBorder" width="99%" height="5"><tr><td></td></tr></table>








	 
	 
	 
	 
	 
