<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="include.jsp" flush="true"/>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
  <jsp:useBean id="personB" scope="request" class="com.velos.eres.web.person.PersonJB"/>
  <jsp:useBean id="patloginJB" scope="request" class="com.velos.eres.web.patLogin.PatLoginJB"/>
  <jsp:useBean id="patloginJBValidate" scope="request" class="com.velos.eres.web.patLogin.PatLoginJB"/>
  <jsp:useBean id="portalB" scope="page" class="com.velos.eres.web.portal.PortalJB"/>
  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
  <%@ page language = "java" import = "java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.Security"%>
  <%@ page import="com.velos.eres.service.util.LC"%> <%@ page import="com.velos.eres.service.util.MC"%>
  <%
    String patPassword = "";
	String src = null;
	src = request.getParameter("srcmenu");
	String eSign = request.getParameter("eSign");
	String mode = request.getParameter("mode");


	HttpSession tSession = request.getSession(true);

	if (sessionmaint.isValidSession(tSession))

   {
     %>
     <jsp:include page="sessionlogging.jsp" flush="true"/>

     <%

   	String oldESign = (String) tSession.getValue("eSign");

	if(!oldESign.equals(eSign))
	{
%>
  	   <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
	}
	else
  	{
		String ipAdd = (String) tSession.getValue("ipAdd");
		String usr = (String) tSession.getValue("userId");

		String email = request.getParameter("email");
		String patUserId = request.getParameter("patLogin");

		String status = request.getParameter("patStat");
		String patlogOutTime = request.getParameter("logOutTime");

		int perId = 0;
		perId = EJBUtil.stringToNum(request.getParameter("perId"));


		String plId = request.getParameter("perId");

		String portalId = request.getParameter("portalId");//er_portal_logins.fk_portal OR er_portal.pk_portal


		int portalLoginId = 0;
		portalLoginId = EJBUtil.stringToNum(request.getParameter("portalLoginId"));	//pk of er_portal_logins



		if (EJBUtil.isEmpty(mode))
			mode = "N";

		String portalObjType = request.getParameter("portalObjTyp");

		String isCheck = request.getParameter("isChecked");




		if (mode.equals("M") && isCheck.equals("0")){

			patPassword = request.getParameter("hidPatPassword");
		}
		else{
			patPassword = request.getParameter("patPassword");
			patPassword=(patPassword==null)?"":patPassword;
			if ((patPassword.length())>0)
				patPassword=Security.encrypt(patPassword);
		}



		int userLoginFound = 0;
		userLoginFound = patloginJBValidate.validateLogin(patUserId);

		 System.out.println("portalLoginId" + portalLoginId);
		 System.out.println("patloginJBValidate.getId()" + patloginJBValidate.getId());

		 if (( mode.equals("N") && userLoginFound !=0 ) || ( (mode.equals("M") && (patloginJBValidate.getId() == portalLoginId || patloginJBValidate.getId() == 0  ) ) )){


%>

<%

		//Persom email will only be updated in the epat.person table
		//and other cols will go to the er_portal_logins table
   		personB.setPersonPKId(perId);
     	personB.getPersonDetails();
     	personB.setPersonEmail(email);
     	personB.updatePerson();

     	if ( mode.equals("N") && StringUtil.isEmpty(patPassword) )
     	{
			portalB.setPortalId(EJBUtil.stringToNum(portalId));
	   		portalB.getPortalDetails();
	   		patPassword = portalB.getPortalDefaultPassword();
	   	}


     	if (mode.equals("M")){

 			patloginJB.setId(portalLoginId);
 			patloginJB.getPatLoginDetails();

     	}
			patloginJB.setPlId(plId);
			patloginJB.setPlIdType("P");//patient login
			patloginJB.setPlLogin(patUserId);
			patloginJB.setPlPassword(patPassword);
			patloginJB.setPlStatus(status);
			patloginJB.setPlLogOutTime(patlogOutTime);
			patloginJB.setFkPortal(portalId);
			patloginJB.setIpAdd(ipAdd);

		if (mode.equals("M")){
 			patloginJB.setModifiedBy(usr);
			patloginJB.updatePatLogin();
		}
		else{
			patloginJB.setCreator(usr);//KM-to fix the Bug	3056
			patloginJB.setPatLoginDetails();
		}


	%>
	  <br>
      <br>
      <br>
      <br>
      <br>
     <p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%> </p>
	  <script>
			window.opener.location.reload();
			setTimeout("self.close()",1000);
	  </script>

<%

}//end duplicate login name does not exist

else{%>
	  <br>
      <br>
      <br>
      <br>
      <br>
	  <!--Modified By Amarnadh to fix the issue #3053 -->
      <p class = "successfulmsg" align = center> <%=MC.M_DupliPatLogin_CntSvd%><%--Duplicate <%=LC.Pat_Patient%> login, data could not be saved.*****--%> </p>
	   <p align = center><button onclick="window.history.back();return false;"><%=LC.L_Back%></button> </p>

<%}//else duplicate check

}//end of if for eSign check

}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

</HTML>





