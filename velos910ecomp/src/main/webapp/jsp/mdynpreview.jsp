<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="include.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title><%=LC.L_Form_RptPreview%><%--Form Report Preview*****--%></title>
	<%@ page language = "java" import = "java.sql.Date,java.text.SimpleDateFormat,java.util.*,java.sql.*,com.velos.eres.web.dynrep.holder.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB"%>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<jsp:useBean id="dynrepB" scope="page" class="com.velos.eres.web.dynrep.DynRepJB" />
	<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
	<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<%-- Nicholas : Start --%>
	<%--<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>--%>
	<%-- Nicholas : End --%>
<script>
var formWin=null;
function openFormWin(formobj,fmt) {


if (fmt=="F"){

   windowName = window.open("mdynfilterview.jsp", "AdHocFilPreview","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=850,height=750 top=100,left=100");
   windowName.focus();
   return true;

}
else if (fmt=="S"){
formobj.target="_new";
formobj.action="mdynsasexport.jsp";
} else {
formobj.target="_new";
formobj.action="mdynexport.jsp?exp=" + fmt;
}
//formobj.exp.value=fmt;
//alert("formobj.exp.value" + formobj.exp.value);


//formWin = open('','formWin','menubar=1,resizable=1,status=0, width=850,height=550 top=50,left=100,scrollbars=1');
//if (formWin && !formWin.closed) formWin.focus();
formobj.submit();
//document.dynreport.submit();
formobj.target="";
formobj.action="";
void(0);
}

function jumpTo(formobj)
{
	//get the value from dropdown
	var i;

	i = formobj.ddIds.selectedIndex;

	formobj.patRecStartCounterStr.value = i;
	formobj.action="mdynpreview.jsp";
	//formobj.submit();
}


function nextRecord(formobj)
{
	formobj.action="mdynpreview.jsp";
	formobj.submit();
}

function prevRecord(formobj)
{
	var patRecStartCounter;
	var patTotalRecToDisplay;
	var origPatRecStartCounter;


	patTotalRecToDisplay = parseFloat(formobj.patTotalRecToDisplayStr.value);
	origPatRecStartCounter = parseFloat(formobj.origPatRecStartCounter.value);

	if ((!isNaN(patTotalRecToDisplay)) && (!isNaN(origPatRecStartCounter)) )
	{
		patRecStartCounter = origPatRecStartCounter - patTotalRecToDisplay;
	}

	if (isNaN(patRecStartCounter))
	{
		patRecStartCounter = "0";
	}
	formobj.patRecStartCounterStr.value = patRecStartCounter;


	formobj.action="mdynpreview.jsp";
	formobj.submit();
}

function changeFilter(formobj)
{
	if (typeof(formobj.patRecStartCounterStr)!="undefined")
 		formobj.patRecStartCounterStr.value = "0";
	formobj.action="mdynpreview.jsp";
	//formobj.submit();
}


</script>
</head>
	<%-- Nicholas : Start --%>
	<jsp:include page="skinChoser.jsp" flush="true"/>
	<%-- Nicholas : End --%>
<% String src,sql="",tempStr="",sqlStr="",name="",selValue="",repFldStr="",fldKey="";

   int strlen=0,firstpos=-1,secondpos=-1,patId=0,studyId=0;
//src= request.getParameter("srcmenu");
%>
<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1)
     ienet = 0; //IE
    else
	ienet = 1;
	if(ienet == 0) {
%>
<body style="overflow:scroll;">
<%
	} else {
%>
<body>
<%
	}
%>
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
<form  name="preview" method="post">
<!-- <input type="text" name="exp"  value=""> -->

<%
	//ReportDataHolder rdHolder = new ReportDataHolder(); // report data holder object

	int seq=0;
	String eolString="";
	String repFooter="",formId="",formName="";
	//String mode = request.getParameter("mode");

	/** changed by Sonia Abrol, 02/21/06, to make patient adHoc patient context sensitive, formPatient parameter
	 will be passed from patient forms */

	String formPatient = request.getParameter("formPatient");
	if (StringUtil.isEmpty(formPatient))
	{
		formPatient = "";
	}


	String value="",patientID="",studyNumber="",orderStr="",order="",filter="",dataOrder="",fltrDD="",prevSecId="",mapSecId="",headAlign="",footAlign="";
 	String allFformIdStr = "";
 	boolean repeatFormHeader = true;


	int personPk=0,studyPk=0,repId=0,index=-1;
	DynRepDao dynDao=new DynRepDao();
	DynRepDao dynDaoFltr=new DynRepDao();
	ArrayList selectedfld=null;

	ArrayList tempVal=new ArrayList();
	ArrayList tempList=new ArrayList();
	ArrayList lfltrIds= new ArrayList();
	ArrayList lfltrNames= new ArrayList();
	ArrayList lfltrStrs= new ArrayList();
	String respAccessRightWhereClause = "";

	String rightsWhereClause = "";
	boolean bIsEmptySQL = false;


	HttpSession tSession = request.getSession(true);

	if (sessionmaint.isValidSession(tSession))
	{
		%>
		<jsp:include page="sessionlogging.jsp" flush="true"/>
		<%
	ReportHolder container=null;
	String from=request.getParameter("from");
	from=(from==null)?"":from;
	String fltrId=request.getParameter("fltrId");
	if (fltrId==null) fltrId="";
	String repIdStr=request.getParameter("repId");
	String containerReportId = "";

	if (repIdStr==null) repIdStr="";

	if (fltrId==null) fltrId="";
	int userId=EJBUtil.stringToNum((String)tSession.getAttribute("userId"));
	 String defUserGroup = (String) tSession.getAttribute("defUserGroup");


	respAccessRightWhereClause = " and ( pkg_filledform.fn_getUserAccess (fk_form," + userId +",creator) > 0 )"	;

	String accId = (String) tSession.getValue("accountId");

	String patRecStartCounterStr = request.getParameter("patRecStartCounterStr");
	String patTotalRecToDisplayStr = request.getParameter("patTotalRecToDisplayStr");

	if (StringUtil.isEmpty(patRecStartCounterStr))
		patRecStartCounterStr = "0";

	if (StringUtil.isEmpty(patTotalRecToDisplayStr))
		patTotalRecToDisplayStr = "1";

	int patRecEndCounter = 0;
	int patRecStartCounter = 0;
	int patTotalRecToDisplay =0;
	int totalRecordsFound = 0;
	int origPatRecStartCounter = 0;
	String fldWidthStr = "";

	ArrayList multiFltrIds=new ArrayList();
	ArrayList multiFltrNames=new ArrayList();

	//for repeat column crash, 04/06/06
	int repColCount  = 0;
	int repColDataUpperLimit = 0;
	int repColCharAvailable = 0;




	patRecStartCounter = EJBUtil.stringToNum(patRecStartCounterStr);
	patTotalRecToDisplay = EJBUtil.stringToNum(patTotalRecToDisplayStr);

	origPatRecStartCounter = patRecStartCounter;

	patRecEndCounter = patRecStartCounter + patTotalRecToDisplay;


	 HashMap FldAttr=new HashMap();
	 HashMap TypeAttr=new HashMap();
	 HashMap FltrAttr=new HashMap();
	 HashMap SortAttr= new HashMap();
	 HashMap RepAttr= new HashMap();
	 HashMap attributes=new HashMap();
	 //This is done to remove the session varibale from previous report.

	 if (from.equals("browse")){
		// container=(ReportHolder)tSession.getAttribute("reportHolder");
		//System.out.println("Browse mode container is null");
		from = "browsePreview";

	 }else{
	 //attributes=(HashMap)tSession.getAttribute("attributes");
	 container=(ReportHolder)tSession.getAttribute("reportHolder");

	 }
	  //System.out.println("container::::::::::::::::::"+container);

	 if (container != null)
	 {
	 	//check if container's reportID is same as the one passed as parameter
	 		containerReportId = container.getReportId();
	 		if (StringUtil.isEmpty(containerReportId))
	 				containerReportId = "";

	 		//System.out.println("^^^^^containerReportId^^^^^" + containerReportId);

	 		if (! StringUtil.isEmpty(containerReportId))
	 		{
	 			if (! containerReportId.equals(repIdStr))
	 			{
	 				//container object is for a different report
	 				System.out.println("Different report get the container again");
	 				container = null;
	 			}
	 		}
	  }


	 if (container==null){
	 DynRepDao mDynDao=new DynRepDao();
	if (repIdStr.length() > 0)
		repId=(new Integer(repIdStr)).intValue();
		//dyndao=dynrepB.fillReportContainer(repIdStr);
		mDynDao.fillReportContainer(repIdStr);
		container=mDynDao.getReportContainer();
		container.DisplayDetail();
		tSession.setAttribute("reportHolder",container);
	 }

	/*To support multiple filters*/


	multiFltrIds=container.getFilterIds();
	multiFltrNames=container.getFilterNames();

	if (multiFltrIds!=null){
		if (multiFltrIds.size()>= 1)
		{
			if (StringUtil.isEmpty(fltrId))
			{
			 	fltrId = (String)multiFltrIds.get(0);
			}
			fltrDD=EJBUtil.createPullDownWithStr("fltrId",fltrId,multiFltrIds,multiFltrNames);
		}
	}


	ArrayList secFldCols=new ArrayList();

	ArrayList formIdList=new ArrayList();
	ArrayList formNameList = new ArrayList();
	FilterContainer fltContainer = null;
	String defDateRangeFilterType = "";
	String defDateRangeFilterStr = "";
	String coreDefDateRangeFilterStr = "";
	String origFormFilterSQL = "";
	String ignoreDRFilter = "";

	ArrayList selectedCoreLookupformIds = new ArrayList();
	String  mapTablePK  = "";
 	String  mapPKTableName = "";
 	String  mapTableFilterCol = "";
 	String  mapStudyColumn = "";
 	boolean checkForStudyRights = false;
 	String fldUseDataValue;
 	int fldDataSeperatorPos = 0;

	checkForStudyRights = container.getCheckForStudyRights();

	String repHeader=request.getParameter("repHeader");
	repFooter=request.getParameter("repFooter");
	if (repHeader==null) repHeader="";
	if (repHeader.length()==0){
	 repHeader=container.getReportHeader();
	if (repHeader==null) repHeader="";
	}
	//Extract alignment String from report Header
	if ((repHeader.indexOf("[VELCENTER]")>=0)||(repHeader.indexOf("[VELLEFT]")>=0)||(repHeader.indexOf("[VELRIGHT]")>=0))
	 {
	   headAlign=repHeader.substring(0,(repHeader.indexOf(":",0)));
	   repHeader=repHeader.substring(headAlign.length()+1);
	   }
	   else
	   {
	   	headAlign=request.getParameter("headerAlign");
		if (headAlign==null) headAlign="center";
	   }

	   if (headAlign.equals("[VELCENTER]")) headAlign="center";
	   if (headAlign.equals("[VELLEFT]")) headAlign="left";
	   if (headAlign.equals("[VELRIGHT]")) headAlign="right";

	//end extracting header

	if (repFooter==null) repFooter="";
	if (repFooter.length()==0)
	{
	repFooter=container.getReportFooter();
	if (repFooter==null) repFooter="";
	}

	//Extract alignment String from report Footer
	if ((repFooter.indexOf("[VELCENTER]")>=0)||(repFooter.indexOf("[VELLEFT]")>=0)||(repFooter.indexOf("[VELRIGHT]")>=0))
	 {
	   footAlign=repFooter.substring(0,(repFooter.indexOf(":",0)));
	   repFooter=repFooter.substring(footAlign.length()+1);

	 }
	   else {
	   footAlign=request.getParameter("footerAlign");
	   if (footAlign==null) footAlign="center";
	   }
	   if (footAlign.equals("[VELCENTER]")) footAlign="center";
	   if (footAlign.equals("[VELLEFT]")) footAlign="left";
	   if (footAlign.equals("[VELRIGHT]")) footAlign="right";

	//end extracting footer

	//System.out.println("^^^^^^^^^^^^^^^^^^^^^^fltrId:::"+fltrId);

	// if fltrId is empty, get the first filter from the container.filterObject

	if (EJBUtil.isEmpty(fltrId))
	{
		Collection fltrSet = (container.getFilterObjects()).values();
		Iterator itrFltr = fltrSet.iterator();

		if (itrFltr.hasNext()) {
			fltContainer = (FilterContainer) itrFltr.next();
			fltContainer.Display();
			fltrId = fltContainer.getFilterId();
		}
	}
	else
	{
		fltContainer=container.getFilterObject(fltrId);
	}

	if (fltContainer!=null)
	{
		filter=fltContainer.getFilterStr();
		defDateRangeFilterType = fltContainer.getFilterDateRangeType();
		if (StringUtil.isEmpty(defDateRangeFilterType))
		{
			defDateRangeFilterType = "";
		}
	}


	if (filter==null) filter="";

	filter=filter.trim();
	filter=StringUtil.decodeString(filter);



	 //end
	 if (dynDaoFltr!=null){
	 lfltrIds=dynDaoFltr.getFltrIds();
	 lfltrNames=dynDaoFltr.getFltrNames();
	 lfltrStrs=dynDaoFltr.getFltrStrs();
	  }

	CommonDAO common=new CommonDAO();
	ArrayList ids=new ArrayList();
	ArrayList recSeqs =new ArrayList();
	ArrayList codes =new ArrayList();
	int idCount = 0;

	Connection conn=null;
	PreparedStatement pstmt=null;
	ResultSet rs=null;
	String sqlId="";
	String sqlIdTemp = "";
	boolean isSingleForm = false;


	if (fltContainer!=null)

	sqlId=fltContainer.getFilterStr();


	sqlId=(sqlId==null)?"":sqlId;
	origFormFilterSQL = sqlId;
	sqlId=StringUtil.decodeString(sqlId);



	formIdList = container.getFormIds();
	formNameList = container.getFormNames();

	selectedCoreLookupformIds = container.getSelectedCoreLookupformIds();

	allFformIdStr = container.getAllFormIdStr();

	//find out if its a singleform report
	if 	(allFformIdStr.indexOf(";") == -1)
	{
		//its a single form, increase the number of records to display, and set the repeat header flag to false
		isSingleForm = true; // include the printer friendly format
		repeatFormHeader = false;
		patTotalRecToDisplay = 20;
		patTotalRecToDisplayStr = "20";
		patRecEndCounter = patRecStartCounter + patTotalRecToDisplay;

	}



	String formType=container.getReportType();

	String recordCountString = "";
	String formIdComma = "";
	String patStudyFilter = "";

	formType=(formType==null)?"":formType;
	formType=(formType=="study")?"S":formType;
	formType=(formType=="pat")?"P":formType;

	studyId = EJBUtil.stringToNum(container.getStudyId());
	patId = EJBUtil.stringToNum(container.getPatientId());

	//changed by Sonia Abrol, 02/21/06, to make patient adHoc patient context sensitive
	if((! StringUtil.isEmpty(formPatient)) && patId == 0)
	{
		patId = EJBUtil.stringToNum(formPatient);
	}

	if (formType.equals("S"))
	{
		recordCountString = LC.L_MatchStd_Fnd/*"Matching "+LC.Std_Studies+" Found*****/+" :" ;
	} else if (formType.equals("P"))
	{
		recordCountString = LC.L_MatchPat_Fnd/*"Matching "+LC.Pat_Patients+" Found*****/+" : ";
	}

	//get a comma separated list of formids (normal forms)

	 for (int k=0;k<formIdList.size();k++)
	 {
	 	 if ( ! ((String)formIdList.get(k)).startsWith("C") )
	 	 {
			formIdComma = formIdComma + "," + formIdList.get(k);
		 }
	 }
	if (!  StringUtil.isEmpty(formIdComma) )
	{
		if (formIdComma.startsWith(","))
		{
			formIdComma = formIdComma.substring(1); //remove first comma
		}

	}


	if (sqlId.length()==0)
	{
	 sqlId="";
	 bIsEmptySQL = true;

	 if (formType.equals("P") && studyId > 0)
	 {
	 	patStudyFilter = " and ( (fk_patprot IS NULL) OR " + studyId + " = (SELECT fk_study FROM ER_PATPROT WHERE pk_patprot=fk_patprot)) " ;
	 }

	  if (! StringUtil.isEmpty(formIdComma))
	 {
		 for (int k=0;k<formIdList.size();k++)
		 {
		 	 if ( ! ((String)formIdList.get(k)).startsWith("C") )
		   if ( sqlId.length()==0)
		   	  sqlId="select distinct ID from er_formslinear where fk_form="+formIdList.get(k) + patStudyFilter + respAccessRightWhereClause;
		   else
		    sqlId=sqlId+" union select ID from er_formslinear where fk_form=" + formIdList.get(k) + patStudyFilter + respAccessRightWhereClause;
		 }
	 }
	}
	else
	{
		//replace loggedinuserid place holder

		sqlId = StringUtil.replace(sqlId,"[:loggedinuser]",String.valueOf(userId));
		sqlId = StringUtil.replace(sqlId,"[:VELACCOUNT]",accId);

	}

	//see if the main id sql is emty and it there are core lookup tables selected, add basic 'select id'
 	//the sqlId is empty
	 if ( bIsEmptySQL )
	  {

	 	 	if (selectedCoreLookupformIds != null && selectedCoreLookupformIds.size() > 0)
	 	 	{
	 	 			if (fltContainer !=null)
	 	 			{
						defDateRangeFilterStr = fltContainer.getDateRangeFilterString()	;
					}

					if (! StringUtil.isEmpty(defDateRangeFilterStr))
						{
							defDateRangeFilterStr = " Where " + defDateRangeFilterStr;
           				    defDateRangeFilterStr = StringUtil.decodeString(defDateRangeFilterStr);
           				}

	 	 		//create the sql Id
	 	 		for (int ctr = 0; ctr < selectedCoreLookupformIds.size(); ctr++)
	 	 			{
	 	 				FieldContainer fldCTemp =container.getFldObject((String)selectedCoreLookupformIds.get(ctr));

	 	 				coreDefDateRangeFilterStr = defDateRangeFilterStr;

							if (fldCTemp !=null)
								{
	 								 mapTablePK = fldCTemp.getPkColName();
	 								 mapPKTableName = fldCTemp.getPkTableName();
	 								 mapStudyColumn = fldCTemp.getMapStudyColumn();

	 								 if (formType.equals("S")) //in case of study lookups, table pk is pk/fk_study
									{
										mapStudyColumn = mapTablePK;
									}


	 								 if ( sqlId.length()==0)
	 								 {
	 								 	sqlId = "Select distinct " + mapTablePK + " as ID from " + mapPKTableName;

	 								 }
	 								 else
	 								 {
	 								 	sqlId = sqlId + " union Select distinct " + mapTablePK + " as ID from " + mapPKTableName;
	 								 }
	 								 if (formType.equals("A")) //for account forms
	 								 {
	 								 	sqlId = sqlId + "  where " +  mapTablePK + " =  " + accId;
	 								 }
	 								
	 								ignoreDRFilter = container.getIgnoreFilters((String)selectedCoreLookupformIds.get(ctr));
									if (!StringUtil.isEmpty(ignoreDRFilter))
									{
										coreDefDateRangeFilterStr = "";
									}

	 								if (formType.equals("P") && (! StringUtil.isEmpty(coreDefDateRangeFilterStr)) )
									{
										if (defDateRangeFilterType.equals("PS"))
										{
											coreDefDateRangeFilterStr = defDateRangeFilterStr;

											if (! StringUtil.isEmpty(mapStudyColumn))
	 								 		{
	 								 			if (! StringUtil.isEmpty(coreDefDateRangeFilterStr))
								            	{
								            		coreDefDateRangeFilterStr = StringUtil.replace(coreDefDateRangeFilterStr,"[VELPAT]",mapTablePK);
								            		coreDefDateRangeFilterStr = StringUtil.replace(coreDefDateRangeFilterStr,"[VELSTUDY]",mapStudyColumn);

								            	}
				   					        }
				   					        else
				   					        {
				   					        	// if study rights are not checked, and study id is provided
				   					        	if (studyId > 0)
				   					        	{
				   					        		coreDefDateRangeFilterStr = StringUtil.replace(coreDefDateRangeFilterStr,"[VELPAT]",mapTablePK);
								            		coreDefDateRangeFilterStr = StringUtil.replace(coreDefDateRangeFilterStr,"[VELSTUDY]",String.valueOf(studyId));

				   					        	}
				   					        	else
				   					        	{
				   					        		coreDefDateRangeFilterStr = "";
				   					        	}


				   					        }

										}
									}


	 								sqlId = sqlId + coreDefDateRangeFilterStr;

	 								if (!StringUtil.isEmpty(mapStudyColumn) &&  studyId > 0)
	 								{
										if (! StringUtil.isEmpty(coreDefDateRangeFilterStr))
		 								{
		 									sqlId = sqlId  + " and " + mapStudyColumn + " = " + studyId;
		 								}
		 								else
		 								{
		 									sqlId = sqlId  + " Where " + mapStudyColumn + " = " + studyId;
		 								}
										
										sqlId = sqlId + " and "+ mapPKTableName +".FK_ACCOUNT =  " + accId;
	 								}else{
	 									if (formType.equals("S")) //for study Ad-hoc queries
		 								 {
		 								 	sqlId = sqlId + " where "+ mapPKTableName +".FK_ACCOUNT =  " + accId;
		 								 }
	 								}

	 								if (patId > 0 && formType.equals("P") )
	 								{
										if (! StringUtil.isEmpty(coreDefDateRangeFilterStr))
		 								{
		 									sqlId = sqlId  + " and " + mapTablePK + " = " + patId;
		 								}
		 								else
		 								{
		 									sqlId = sqlId  + " Where " + mapTablePK + " = " + patId;
		 								}
	 								}

	 						}
	 	 							////////////
	 	 			}


	 	 	}
	 	 	mapTablePK = "";
	 		mapPKTableName = "";
	 		System.out.println("Now main sql" +  sqlId);

	}

	//if preview mode and the report is not saved, recalculate checkForStudyRights  flag


	 	 	if ((! from.equals("browsePreview") ) && selectedCoreLookupformIds != null && selectedCoreLookupformIds.size() > 0)
	 	 	{
	 	 		System.out.println("recalculating checkForStudyRights flag");
	 	 		container.setCheckForStudyRights(false);
	 	 		checkForStudyRights = false;


	 	 		//create the sql Id
	 	 		for (int ctr = 0; ctr < selectedCoreLookupformIds.size(); ctr++)
	 	 			{
	 	 				FieldContainer fldCTemp =container.getFldObject((String)selectedCoreLookupformIds.get(ctr));
							if (fldCTemp !=null)
								{
	 								 mapStudyColumn = fldCTemp.getMapStudyColumn();

	 								 if (! StringUtil.isEmpty(mapStudyColumn))
	 								 {
	 								 	 container.setCheckForStudyRights(true);
	 								 	 checkForStudyRights = true;
	 								 	 break;
	 								 }
	 							}
	 	 				////////////
	 	 			}

	 	 	}
	/////////////////////////////////////////////////////////

	//////////////////////append the access rights filters to the main ID sql//////////////////////////////////

	if (formType.equals("P"))
	{
		// get the patient stataus date range filter string only if the original filter sql was empty
		if (defDateRangeFilterType.equals("PS") && StringUtil.isEmpty(origFormFilterSQL))
           	{
           		if (fltContainer != null)
           		{
           			defDateRangeFilterStr = fltContainer.getDateRangeFilterString()	;
           		}
           	}
           	else
           	{
           		defDateRangeFilterStr = "";
           	}

		 /* Attach Organization check */


		 if (checkForStudyRights || studyId > 0 )
		 {
		 	/* sqlId = sqlId + "  SELECT pk_per FROM ER_PER c ,ER_USERSITE d, ER_PATPROT pp WHERE c.fk_site = d.fk_site AND d.fk_user= "
		 	 + userId+  " AND d.usersite_right > 0 and pp.fk_per = pk_per and patprot_stat = 1 and (0 < pkg_user.f_chk_right_for_patprotsite(pp.pk_patprot,"+userId+") )"; */

		  	rightsWhereClause = "  exists ( SELECT * FROM ER_PER c, ER_PATPROT pp "+
			 	 " WHERE c.fk_account = " + accId + " and  c.pk_per = ID and pp.fk_per = pk_per and patprot_stat = 1 and (0 < pkg_user.f_chk_right_for_patprotsite(pp.pk_patprot,"+userId+") )";
				if (studyId > 0)
				{
					rightsWhereClause = rightsWhereClause + " and pp.fk_study = " + studyId ;
				}
			if (patId>0)
				 {
				 	rightsWhereClause = rightsWhereClause+ " and fk_per = "+ patId ;
				 }
			rightsWhereClause = rightsWhereClause + " )";

		 }
		 else
		 {
		 	rightsWhereClause = " exists ( SELECT * FROM ER_PER c  WHERE c.fk_account = " + accId + " and c.pk_per = ID and  "   +
				 	"pkg_user.f_right_forpatsites(pk_per,"+ userId +") > 0";


		 	if (patId>0)
				 {
				 	rightsWhereClause  = rightsWhereClause  + " and pk_per = "+ patId ;
				 }
			rightsWhereClause = rightsWhereClause + " )";

		 }




	//use the output of above sqlId to filter records available for current user, we have to apply this check to minimize the
	//difference in total record count and records available acc to access rights




	} //end of formType equals to P


	if (formType.equals("S"))
	{
		if (! StringUtil.isEmpty(sqlId))
		 {
			// 	sqlId = sqlId + " INTERSECT ";

			sqlId = " Select ID , study_number from ( " + sqlId + " ) , er_study A Where exists ( select * from er_study where  id = pk_study ";

		 }

		//sqlId = sqlId + " Select pk_study from er_study where ";

		if (studyId > 0)
		{
			sqlId = sqlId + " and pk_study = " + studyId + " and 1 = (Pkg_Util.f_checkStudyTeamRight(pk_study,"+userId+",'STUDYFRMACC')) ";
		}
		else
		{
		 	/* block studies to which user does not have access*/
		 	sqlId=sqlId + "  and 1 = (Pkg_Util.f_checkStudyTeamRight(pk_study,"+userId+",'STUDYFRMACC') ) " ;
		}
		
		sqlId = sqlId + " and FK_ACCOUNT =  " + accId;

		sqlId = sqlId + " ) and ID = PK_STUDY  Order by lower(study_number)"; //for exists

	}

	/////////////////////////////////////////////////////////////////////////////////////////

	if(formType.equals("P"))
	{
		//get patient Code
		sqlId = "select  id , ( select per_code from er_per where pk_per = id) patcode  from ( " + sqlId + " ) out ";
		sqlId = sqlId + " Where " + rightsWhereClause + " Order by lower(patcode)" ;
	}


	System.out.println("SQL MAIN....."+sqlId);
	try{

		conn=common.getConnection();
		pstmt = conn.prepareStatement(sqlId);
		rs = pstmt.executeQuery();

		while (rs.next())
		{
	 		ids.add(rs.getString(1));

	 		if(formType.equals("P") || formType.equals("S") )
			{
	 			codes.add(rs.getString(2));
	 			recSeqs.add(new Integer(idCount));
	 		}

	 		idCount++;
		}

	}
	catch (SQLException ex)
	{
		out.println("<p class=\"redMessage\" align=\"center\">Please check the Ad-Hoc query definition</p>");
		System.out.println("SQL" + sqlId);
		System.out.println("SQL exception" + ex);
	}
	finally
	{
	    try { if (rs != null) { rs.close(); } } catch(Exception e) {}
	    try { if (pstmt != null) { pstmt.close(); } } catch(Exception e) {}
	    try { if (conn != null) { conn.close(); } } catch(Exception e) {}
	}

	 //get patient dropdown

	String ddIds = "";

	if(formType.equals("P") || formType.equals("S") )
	{
	ddIds = EJBUtil.createPullDownNoSelect("ddIds",patRecStartCounter,recSeqs,codes) ;
	}

//	 fltrDD=EJBUtil.createPullDown("selFltr",EJBUtil.stringToNum(fltrId),lfltrIds,lfltrNames);

	//retrieve filterSTring

	//dynDaoFltr=dynrepB.getFilterDetails(fltrId);
	%>
	<table width="100%" cellspacing="0" cellpadding="0" border="0">
         <tr>
            <td class="reportPanel">
<%=MC.M_Download_ReportIn%><%--Download the report in*****--%>:
<A href="#" onClick="openFormWin(document.preview,'W')"><img border="0" title="<%=LC.L_Word_Format%>" alt="<%=LC.L_Word_Format%>" src="./images/word.GIF" /><%//=LC.L_Word_Format%><%--Word Format*****--%></A>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<A href="#" onClick="openFormWin(document.preview,'E')"><img border="0" title="<%=LC.L_Excel_Format%>" alt="<%=LC.L_Excel_Format%>" src="./images/excel.GIF" /><%//=LC.L_Excel_Format%><%--Excel Format*****--%></A>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<A href="#" onClick="openFormWin(document.preview,'H')"><img border="0" title="<%=LC.L_Printer_FriendlyFormat%>" alt="<%=LC.L_Printer_FriendlyFormat%>" src="./images/printer.gif"/></A>&nbsp;&nbsp;&nbsp;&nbsp;

<%
repIdStr=(repIdStr==null)?"0":repIdStr;

%>

<input type="hidden" name="repId"  value=<%=repIdStr%>>


<%

if ((EJBUtil.stringToNum(repIdStr))>0){%>
	<A href="#" onClick="openFormWin(document.preview,'S')"><img src="../images/jpg/sas.gif" title="<%=LC.L_Sas_Export%>" alt="<%=LC.L_Sas_Export%>" border="0"></img><%--SAS Export*****--%></A>&nbsp;&nbsp;&nbsp;&nbsp;
<%}%>

<A href="#" onClick="openFormWin(document.preview,'L')"><img src="../images/jpg/previewlinear.gif" title="<%=LC.L_Linear_Format%><%--Linear Format*****--%>" alt="<%=LC.L_Linear_Format%><%--Linear Format*****--%>" border="0"></img></A>&nbsp;&nbsp;&nbsp;&nbsp;
<A href="#" onClick="openFormWin(document.preview,'X')"><img src="../images/jpg/xml.gif" title="<%=LC.L_Xml_Format%>" alt="<%=LC.L_Xml_Format%>" border="0"></img><%--XML Format******--%></A>&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
         </tr>
      </table>

     <% if ( ! StringUtil.isEmpty(fltrDD) ) { %>
	 <table  width="100%" cellspacing="0" cellpadding="0" border="0"><tr><td class="reportPanel">
     	<%=LC.L_Filter%><%--Filter*****--%> : <%=fltrDD%> &nbsp;&nbsp;
     	<button type="submit" onClick = "return changeFilter(document.preview);"><%=LC.L_Go%></button>
     	&nbsp; <A href="#" onClick="openFormWin(document.preview,'F')"><%=LC.L_View_Filters%><%--View Filters*****--%></A>&nbsp;&nbsp;&nbsp;&nbsp;
     	</td></tr></table>
     <% } %>
<P class="sectionHeadings"> <%=LC.L_FrmRpt_Prev%><%--Form Report >> Preview*****--%> </P>
<%
	totalRecordsFound = ids.size();

	if (( formType.equals("P") || formType.equals("S")) && (!isSingleForm) )
	{ %>
		<table width="800" cellspacing="0" cellpadding="0" border="0">
			<tr>
	  			<td align="left" width="300">&nbsp
	    		<% if (origPatRecStartCounter > 0 && (!isSingleForm) ) {%>
	    			<A href="#" onClick="prevRecord(document.preview)"> <%Object[] arguments2 = {patTotalRecToDisplayStr}; %>
	                <%=VelosResourceBundle.getMessageString("M_Prev_Rec",arguments2)%><%--<< Previous {0} Record(s)*****--%>  </A>
	    		<%	}%>
	  			</td>
	  			<td width="200"><p class="redMessage"> <%=recordCountString%> &nbsp; <%=totalRecordsFound%></p></td>
	  			<td align="right" width="300">&nbsp
				<%if ( totalRecordsFound > patRecEndCounter && (!isSingleForm) ) { %>
					<A href="#" onClick="nextRecord(document.preview)"><%Object[] arguments1 = {patTotalRecToDisplayStr}; %>
	                <%=VelosResourceBundle.getMessageString("M_Nxt_Rec",arguments1)%><%--Next {0} Record(s) >>*****--%>  </A>
				<% } %>
				</td>
			</tr>
		</table>
		<% if( (formType.equals("P") || formType.equals("S") ) && !isSingleForm) { %>
		<p class="sectionHeadings">
		<%=LC.L_Jump_To%><%--Jump to*****--%> : &nbsp;<%=ddIds%> &nbsp; <button type="submit" onClick = "return jumpTo(document.preview);"><%=LC.L_Go%></button>
		</p>

	 <% }
	 } %>

	<%
		if (isSingleForm) {%>
		<jsp:include page="mdynexport.jsp" flush="true">
	     	<jsp:param name="from" value="<%=from%>"/>
			<jsp:param name="exp" value="H"/>
			<jsp:param name="repId" value="<%=repIdStr%>"/>
			<jsp:param name="fltrId" value="<%=fltrId%>"/>
		</jsp:include>
	<% } else {%>
	<hr class="thickLine">
	<table width="100%" cellspacing="0" cellpadding="0" border="0">
	 <%if (repHeader.length()>0){
		%>
		<tr><td class="reportName" align="<%=headAlign%>"><pre><%=repHeader%></pre></td></tr>
		<tr height="20"></tr>
		<%}%>
		<tr><td align=right><font class="comments"><%=MC.M_SummaryInfo_DataDisp%><%--Summary Information is based on the data displayed on this page*****--%></font></td></tr>
	</table>

	<%

	for (int y=0;y<ids.size();y++) //for each patient that meets the filter criteria
	{

	  if ( patRecEndCounter	== patRecStartCounter)
	  {
	  	//out.println("<BR> Reached- "+patRecEndCounter+" exting...<br>");
	  	break;
	  }

	  //reach till record requested
	  if ( patRecStartCounter  > y)
	  {
	  	  	//out.println("<BR> Skipping record - "+y+"<br>");
	  		continue;
	  }

	  patRecStartCounter ++; //increase counter


	 String attachId=(String)ids.get(y);

	//out.println("<BR> Next Patient - "+attachId+"<br>");


	for (int z=0;z<formIdList.size();z++)
	{
		ArrayList dataList=new ArrayList() ;
		SummaryHolder repSummaryHolder = new SummaryHolder ();

		boolean anyMaskedColumn = false;
		ArrayList arMaskedColumnList = new ArrayList();

		ArrayList fldKeywords=new ArrayList();


		ArrayList fldCols=new ArrayList();
		ArrayList fldNames=new ArrayList();
		ArrayList fldDispName=new ArrayList();
		ArrayList tmpFldNames=new ArrayList();
		ArrayList tmpFldDispName=new ArrayList();
		ArrayList fldWidth=new ArrayList();
		ArrayList repFldNames=new ArrayList();
		ArrayList repFldNamesSeq=new ArrayList();
		ArrayList mapSecNames=new ArrayList();
		ArrayList mapSecIds=new ArrayList();

		ArrayList fieldTypes = new ArrayList();
		ArrayList fieldDisplayDataValue = new ArrayList();
		ArrayList repFieldDisplayDataValue = new ArrayList();

		ArrayList fldCalcSum = new ArrayList();
		ArrayList fldCalcPer = new ArrayList();
		ArrayList arCompleteFieldList = new ArrayList();

	formId=(String)formIdList.get(z);
		Hashtable moreParameters  = new Hashtable();;


	if (StringUtil.isEmpty(formId))
	{
		continue;
	}
	if (formId.startsWith("C"))
	{
		//check if it exists in selectedCoreLookupformIds, if no then continue

		if (! selectedCoreLookupformIds.contains(formId))
		{
			continue;
		}

	}
	if (z>0 && ( (repeatFormHeader) || ((!repeatFormHeader) && ( y == origPatRecStartCounter ))) )  {
	%>
	<br><br>
	<%}

	formName = (String)formNameList.get(z);

	if (fltContainer!=null)
	{
	 filter=fltContainer.getChoppedFilter(formId);
	 filter=filter.trim();


	 if (StringUtil.isEmpty(filter) && (!defDateRangeFilterType.equals("PS")) )
	 {
	 	 //get the date range filter (if defined)
	 	  ignoreDRFilter = container.getIgnoreFilters(formId);
	 	  if (StringUtil.isEmpty(ignoreDRFilter))
			{
	 			filter = fltContainer.getDateRangeFilterString();
	 		}
	 }

	 filter=StringUtil.decodeString(filter);
	 filter=(filter==null)?"":filter;


	 }
	 else
	 {
	 	defDateRangeFilterType  = "";
	 }


	SortContainer sortContainer=container.getSortObject(formId);
	if (sortContainer!=null)
	{
		dataOrder=sortContainer.getSortStr();
		if (dataOrder==null) dataOrder="";
		if (dataOrder.length()>0){
		dataOrder=StringUtil.replace(dataOrder,"[$$]",",");
		dataOrder=StringUtil.replace(dataOrder,"[$]"," ");
		orderStr=" order by "+ dataOrder;
		}
	}
	else
	{
		System.out.println("sort container null");
		orderStr = "";
	}


	FieldContainer fldContainer=container.getFldObject(formId);
	if (fldContainer!=null)
	{
		  fldKeywords = fldContainer.getFieldKeyword();

		 fldCols=fldContainer.getFieldColId();
		 fldNames=fldContainer.getFieldName();
		 fldDispName=fldContainer.getFieldDisplayName();
		 fldWidth=fldContainer.getFieldWidth();
		 fieldTypes = fldContainer.getFieldType();
		 fieldDisplayDataValue = fldContainer.getUseDataValues();
		 arCompleteFieldList =  fldContainer.getFieldList();


		 fldCalcSum = fldContainer.getCalcSums();
 		 fldCalcPer = fldContainer.getCalcPers();
		 moreParameters.put("fldCalcSum", fldCalcSum);
 		 moreParameters.put("fldCalcPer", fldCalcPer);

		 mapTablePK = fldContainer.getPkColName();
		 mapPKTableName = fldContainer.getPkTableName();
		 mapTableFilterCol = fldContainer.getMainFilterColName();
		 mapStudyColumn = fldContainer.getMapStudyColumn();

	}
	else
	{

	}

	 if (fldCols.size() <= 0)
	 {

	 	 if ( (repeatFormHeader) || ((!repeatFormHeader) && ( y == origPatRecStartCounter )))
			   {
				%>
				<table border="1" width="100%" cellspacing="0" cellpadding="0">
					<TR class="browserOddRow" > <td><B><%=LC.L_Form%><%--Form*****--%> : <%=formName%> </B></td></TR>
				</table>
				<P class="redMessage"><%=MC.M_NoFldsSelected%><%--No Fields Selected*****--%></P>
			  <%
			  }
		 	continue;
	 }
	tmpFldNames=fldNames;
	tmpFldDispName=fldDispName;
	mapSecNames.clear();
	mapSecIds.clear();
	repFldNames.clear();
	repFieldDisplayDataValue.clear();
	secFldCols.clear();

	sql="";
	sqlStr="";

	int idxInMaster = -1;

	for (int i=0;i<fldCols.size();i++){

	if (formId.startsWith("C"))
		{
			 idxInMaster = arCompleteFieldList.indexOf((String)fldCols.get(i));

			if (idxInMaster >= 0)
			{
				fldKey = (String) fldKeywords.get(idxInMaster);


				if (fldKey.startsWith("MASK_"))
				{

					anyMaskedColumn = true;
					arMaskedColumnList.add((String)fldCols.get(i));

				}
			}

		}

	   tempStr=(String)fldCols.get(i) ;
	  name=(String)fldCols.get(i); //use column name instead of display name in sql
	   if (name==null) name="";
	  if (name.length()>30) name=name.substring(0,30);
	  if (name.indexOf("?")>=0)
	   name=name.replace('?','q');
	   if (name.indexOf("\\")>=0)
	   name=name.replace('\\','S');
	  /*if (order.length()>0)
	    if (orderStr.length()>0){
	    orderStr=orderStr + " , \"" + name+"\" " + order;
	    }
	    else{
	    orderStr="\""+name+ "\" " + order;
	    }*/
	  if (tempStr.indexOf("|")>=0)
	  {
	  //store the repeated fields names in an arraylist and the sequence to remove the repeated fldnames from original fldNames list
	  //populate secfldCols to retirve section for only repeated fields.
	  secFldCols.add(tempStr);
	  repFldNames.add( StringUtil.decodeString((String)fldDispName.get(i)) );
	  repFldNamesSeq.add(new Integer(i));
	  repFieldDisplayDataValue.add((String)fieldDisplayDataValue.get(i));

	//Sonia Abrol, 04/06/06 , for repeat field crash, if concatenated string's data is  is more than 4000 char
	  	String[] arRepColsTemp = StringUtil.chopChop(tempStr,'|');
		repColCount  = 0;

	  	if (arRepColsTemp != null)
	  	{
	  		repColCount = arRepColsTemp.length;

	  		if (repColCount > 1)
	  		{
		  		repColCharAvailable = 4000 - (repColCount * 8); //substract (number of columns * 8) 8 for [VELSEP]

			  	//System.out.println("repColCount " + repColCount + "repColCharAvailable  " + repColCharAvailable  + "char" + repColCharAvailable /repColCount + "*");
	  			repColDataUpperLimit = (int) Math.floor(repColCharAvailable /repColCount) ;
			  	//System.out.println("repColDataUpperLimit " + repColDataUpperLimit  + "*");
	  		}
	  		else
	  		{
	  			repColDataUpperLimit  = 4000;
	  		}
	  	}
	  	else
	  	{
	  		repColDataUpperLimit = 4000;
	  	}
		//////////////

	  tempStr=tempStr.replace('|',',');
	  tempStr=StringUtil.replaceAll(tempStr, ",", ",'-') , 1,"+ repColDataUpperLimit +") ||'[VELSEP]'|| substr(nvl(");
	  tempStr = "substr(nvl( " + tempStr + ",'-') ,1,"+ repColDataUpperLimit +")";


	     if (sqlStr.length()==0)
	  	sqlStr=" ("+tempStr+") as \"" + name+ "\"" ;
		 else
		sqlStr=sqlStr+",  ("+tempStr+") as \"" + name +"\"";
	}
	else{
		if (sqlStr.length()==0)  sqlStr=tempStr +" as \"" +name +"\"";
		else sqlStr=sqlStr + ", " + tempStr + " as \"" + name+"\"";
	}
	}

		//if anyMaskedColumn then append access rights column

	if (anyMaskedColumn)
	{
		sqlStr = sqlStr + ", pkg_studystat.f_get_patientright("+userId+", " + defUserGroup+ " , " + mapTablePK +") as pat_detail_right";
	}

	 /* check if the filter is 'undefined' and set it to blank
	 */
	 filter=(filter.equals("undefined")?"":filter);

	 if ((filter.trim()).length()>0)
	 {
			filter= StringUtil.decodeString(filter);
			filter = " and ( " + filter + " ) ";
	 }

	 if (StringUtil.isEmpty(filter))
	 {
	 	 filter = "";
	 }


	if (formType.equals("A"))
	{
		//System.out.println(" i m here");
		if (! formId.startsWith("C") )
		{

			sql =" select ID ,FK_PATPROT as PATPROT, " + sqlStr + " from er_formslinear where fk_form = "+ formId + filter +
			 " and fk_form in (select PK_FORMLIB from er_formlib a,er_linkedforms b,er_formstat c " +
			" Where b.fk_account = "+ accId+"  and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and  b.record_type<>'D' and "+
			"((c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp in ('A','D','O') and codelst_type='frmstat')) "+
			"   OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')))"+
			" and (c.formstat_enddate is null) and  "+
			"b.fk_formlib not in (select fk_formlib from er_formorgacc where fk_site not in (select fk_site from er_usersite where fk_user=" +userId +" and usersite_right <> 0)) and "+
			"b.fk_formlib not in (select fk_formlib from er_formgrpacc where fk_group not in (select fk_grp from er_usrgrp where fk_user="+userId +" )) "+
			" AND ((LF_DISPLAYTYPE = 'A' )) union " +
			"select PK_FORMLIB from er_formlib a,er_linkedforms b,er_formstat c " +
			" where b.fk_account = "+accId+" and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and b.record_type<>'D'and "+
			" ((c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp in ('A','D','O') and codelst_type='frmstat')) "+
			"   OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')))"+
			" and (c.formstat_enddate is null) and  "+
			" (b.fk_formlib in (select fk_formlib from er_formgrpacc where fk_group in (select fk_grp from er_usrgrp where fk_user="+userId+" )) "+
			" or b.fk_formlib in (select fk_formlib from er_formorgacc where fk_site in (select fk_site from er_usersite where fk_user="+userId+" and usersite_right <> 0))) "+
			" AND ((LF_DISPLAYTYPE = 'A' )) ) " + respAccessRightWhereClause  ;
		}
		else //core table
		{

			if (! StringUtil.isEmpty(sqlStr))
			{
				sqlStr = " , " + sqlStr;
			}

			sql =" select "+ mapTablePK + ",'' as PATPROT " + sqlStr + " from " + mapPKTableName + " Where " +  mapTablePK + "="+accId +  filter  ;
		}

		//System.out.println(" i m here" + sql);
	}

	if (formType.equals("S"))
	{
		if (! StringUtil.isEmpty(sqlStr))
		{
			sqlStr = " , " + sqlStr;
		}

		if (! formId.startsWith("C") )
		{
			sql =" select ID,fk_patprot as PATPROT " + sqlStr + " from er_formslinear where fk_form="+formId + filter +  respAccessRightWhereClause   ;
		}
		else //core table
		{
			sql =" select "+ mapTablePK + ",'' as PATPROT " + sqlStr + " from " + mapPKTableName + " Where " +  mapTablePK + " = " + attachId  +  filter  ;
		}


		if (studyId > 0)
		{
			sql = sql + " and "+ mapTablePK+ " = " + studyId + " and 1 = (Pkg_Util.f_checkStudyTeamRight("+studyId+","+userId+",'STUDYFRMACC')) ";
		}
		else
		{
		 	/* block studies to which user does not have access*/
		 	sql = sql + " and  1 = (Pkg_Util.f_checkStudyTeamRight("+ mapTablePK +","+userId+",'STUDYFRMACC') ) " ;
		}

	}


	if (formType.equals("P"))
	{
		defDateRangeFilterStr = "";

		if (defDateRangeFilterType.equals("PS") && StringUtil.isEmpty(filter))
           	{
           		if (fltContainer != null)
           		{
           		  		defDateRangeFilterStr = fltContainer.getDateRangeFilterString()	;
           		}
           	}
           	else
           	{
           		defDateRangeFilterStr = "";
           	}


	 	ignoreDRFilter = container.getIgnoreFilters(formId);

		if (!StringUtil.isEmpty(ignoreDRFilter))
			{
				defDateRangeFilterStr = "";

			}

		if (! StringUtil.isEmpty(sqlStr))
		{
			sqlStr = " , " + sqlStr;
		}
		if (! formId.startsWith("C") )
		{
			sql =" select ID,fk_patprot as PATPROT  " + sqlStr + " from er_formslinear a where fk_form = "+formId  + filter +  respAccessRightWhereClause   ;
		}
		else
		{
			sql =" select "+ mapTablePK + ",'' as PATPROT " + sqlStr + " from " + mapPKTableName+ " Where " +  mapTablePK + " = " + attachId   ;

            if (! StringUtil.isEmpty(mapStudyColumn) )
            {
            	//check study rights on basis of this column

            	sql = sql + " and (0 < pkg_user.f_chk_studyright_using_pat("+attachId +","+mapStudyColumn+" ,"+userId+") ) " ;

            	//if defDateRangeFilterType = 'PS', and defDateRangeFilterStr is not empty, replace values

            	if (! StringUtil.isEmpty(defDateRangeFilterStr))
            	{
            		defDateRangeFilterStr = StringUtil.replace(defDateRangeFilterStr,"[VELPAT]",mapTablePK);
            		defDateRangeFilterStr = StringUtil.replace(defDateRangeFilterStr,"[VELSTUDY]",mapStudyColumn);
            		sql = sql + " and " + defDateRangeFilterStr ;
            	}



            	if (studyId>0)
		 		{
		 			sql = sql + " and "+mapStudyColumn+ " = " + studyId;
		 		}

            }

            sql = sql + filter ;


		}

		 if (patId>0)
		 {
		 	sql = sql + " and "+ mapTablePK + "="+ patId ;
		 }
		 else if (studyId > 0 && StringUtil.isEmpty(mapStudyColumn))
		 {
		 	 sql = sql  + " and " + mapTablePK + " in (select fk_per from er_patprot where fk_study="+studyId+ " and patprot_stat=1 and (0 < pkg_user.f_chk_right_for_patprotsite(pk_patprot,"+ userId +") ) )";

			if (! StringUtil.isEmpty(defDateRangeFilterStr))
            	{
            		defDateRangeFilterStr = StringUtil.replace(defDateRangeFilterStr,"[VELPAT]",mapTablePK);
            		defDateRangeFilterStr = StringUtil.replace(defDateRangeFilterStr,"[VELSTUDY]",String.valueOf(studyId));
            		sql = sql + " and " + defDateRangeFilterStr ;

            	}


		 }

		if (! formId.startsWith("C") )
		{
			if (studyId>0)
		 	{
		 		sql = sql + " and ( (a.fk_patprot is null) or " + studyId + " = (SELECT fk_study FROM ER_PATPROT WHERE pk_patprot=a.fk_patprot)) ";
		 	}
			 sql = sql + " AND ((a.fk_patprot is null) or (0 < pkg_user.f_chk_right_for_patprotsite(a.fk_patprot,"+ userId +") ) )"  ;

			if (! StringUtil.isEmpty(defDateRangeFilterStr))
            	{
            		defDateRangeFilterStr = StringUtil.replace(defDateRangeFilterStr,"[VELPAT]","ID");

            		if (studyId>0)
            		{
            			defDateRangeFilterStr = StringUtil.replace(defDateRangeFilterStr,"[VELSTUDY]",String.valueOf(studyId));
            			sql = sql + " and " + defDateRangeFilterStr  ;
            		}
            		else
            		{
            			defDateRangeFilterStr = StringUtil.replace(defDateRangeFilterStr,"[VELSTUDY]","(SELECT fk_study FROM ER_PATPROT WHERE pk_patprot=a.fk_patprot)");
            			sql = sql + " and ((a.fk_patprot is null) or " + defDateRangeFilterStr + " )";
            		}



            	}



		}

	}



	//end attach criteria
	sql=sql + " and " + mapTablePK + " =" + attachId;

	if (orderStr.length()>0) sql = sql+ "  " + orderStr ;
	else
	{
		//changed to column number
		if (formType.equals("P")) sql = sql+ "  " + " order by 1" ;
		else if (formType.equals("S")) sql =sql+ "  " + " order by 1" ;
	}

	 if (anyMaskedColumn)
		 {
		 	moreParameters.put("arMaskedColumnList", arMaskedColumnList);
		 	moreParameters.put("maskedReferenceColumn", "pat_detail_right");
		 }

	System.out.println("sonia sql!!" + sql+formType);
	dynDao=dynrepB.getReportData(sql,(( String [] ) fldNames.toArray ( new String [ fldNames.size() ] )),formType ,moreParameters);
	//dynDao.getReportData(sql,fldName);
	dataList=dynDao.getDataCollection();
	repSummaryHolder = dynDao.getRepSummaryHolder();

	System.out.println("size of data" + dataList.size());


	//Retrieve section information
	DynRepDao dyndao=new DynRepDao();
	//System.out.println("secFldCols*********************************"+ secFldCols);
	if (secFldCols!=null){
	if (secFldCols.size()>0){
	dyndao=dynrepB.getSectionDetails(secFldCols,formId);
	dyndao.preProcessMap();
	mapSecNames=dyndao.getMapColSecNames();
	mapSecIds=dyndao.getMapColSecIds();
	}
	}

	//set Report data Holder

	if (dataList == null)
	{
		dataList = new ArrayList();
	}

	/*rdHolder.setArFormIds(formId);
	rdHolder.setArFormNames(formName);
	rdHolder.setHtFormData(formId, dataList);
	rdHolder.setHtFormFieldTypes(formId, fieldTypes);
	rdHolder.setHtFormFldDisplayDataValue(formId,fieldDisplayDataValue );
	rdHolder.setHtFormfldWidth(formId,fldWidth);
	rdHolder.setHtFormRepFldNames(formId,repFldNames);
	rdHolder.setHtFormMapSecNames(formId,mapSecNames);
	rdHolder.setHtFormMapSecIds(formId, mapSecIds);
	rdHolder.setHtFormRepFldDisplayDataValue(formId,repFieldDisplayDataValue );
	rdHolder.setHtActualFormFieldNames(formId, fldNames);*/


	//System.out.println("fldNames" + fldNames);

	//out.println("repeatFormHeader" + repeatFormHeader + " origPatRecStartCounter" + origPatRecStartCounter + " y" + y );
  if ( (repeatFormHeader) || ((!repeatFormHeader) && ( y == origPatRecStartCounter )))
   {
   int countSummaryField = 0;
	%>
	<table border="1" width="100%" cellspacing="0" cellpadding="0">
		<TR class="browserOddRow" > <td><B><%=LC.L_Form%><%--Form*****--%> : <%=formName%> </B></td></TR>
	</table>

	<table border="1" width="100%" cellspacing="0" cellpadding="0">

<TR class="browserEvenRow" > <td ><b><%=LC.L_Fld_Name%><%--Field Name*****--%></b></td><td><b><%=LC.L_Basic_Stats%><%--Basic Stats*****--%></b></td><td ><b><%=LC.L_CntOrPerc%><%--Count/Percentage*****--%></b></td></TR>

				<%
					for (Enumeration e = (repSummaryHolder.getHtHolders()).keys() ; e.hasMoreElements() ;)
				    {
				         SummaryHolder sd = new SummaryHolder();
				         String keyName = (String)e.nextElement();
				         countSummaryField++;

				         sd = (SummaryHolder) repSummaryHolder.getHtHolders().get( keyName);

				         if (sd != null)
				         {
				         	 %>

				         	<TR class="browserOddRow" > <td> <%= fldDispName.get(Integer.parseInt(keyName)) %></td>

				         	<%
				         	if (sd.getInfoType().equals("sum") || sd.getInfoType().equals("both"))
				         	{
				         	 %>
				         		<td><b><%=LC.L_Min%><%--Min*****--%>:</b>  <%=  sd.getMinValue() %> &nbsp;<b><%=LC.L_Max%><%--Max*****--%>:</b>  <%= sd.getMaxValue()%> &nbsp;<b><%=LC.L_Median%><%--Median*****--%>:</b>  <%= sd.getMedianValue()%> &nbsp;<b><%=LC.L_Avg%><%--Avg*****--%>:</b>  <%= sd.getAverageValue() %> </td>

				         		<%
				         	  if(!sd.getInfoType().equals("both"))
				     			{
				     			  %>
				     				<td><%=LC.L_NORA%><%--N/A*****--%></td>
				     			<%

				     			}
				         	}
				         	if (sd.getInfoType().equals("per") || sd.getInfoType().equals("both"))
				         	{
				      	     if(!sd.getInfoType().equals("both"))
				     			{
				     			%>

				     			<td><%=LC.L_NORA%><%--N/A*****--%></td>

				     			<%
				       			}
				         		%>
				         		<td>

				         		<%
				         		for (Enumeration p = (sd.getHtValueCounts()).keys() ; p.hasMoreElements() ;)
				         			{
				         				String dispValResp = "";
				         				dispValResp = (String)p.nextElement();

				         			%>
				         				&nbsp;<b> <%=  dispValResp %> </b>:  <%=  sd.getHtValueCounts(dispValResp) %> (  <%=  String.valueOf(sd.getHtValueCountPercentage(dispValResp))  %> %)

				         			<% } %>
				         		</td>

				         	<% }	%>

				         	</TR>

				         <% }
				     }
				     if (countSummaryField == 0)
						{
				      %>
				      		 <tr><td colspan=3>***<%=MC.M_NoFldSumm_InfoFnd%><%--No Fields selected for Summary Information/No Data Found*****--%>***</td></tr>
				      <% } %>
			</table>
	<BR>
  <%
  }

	if ( (repeatFormHeader) || ((!repeatFormHeader) && (y == origPatRecStartCounter)))
	{ %> 
			<table border="0" width="100%" cellspacing="0" cellpadding="0" class="basetbl">

	<%
	 } // end of if to start a new table
if ( (repeatFormHeader) || ((!repeatFormHeader) && (y == origPatRecStartCounter)))
{

	int secIndex=-1;
 for(int count=0;count<fldNames.size();count++){
   index= (repFldNamesSeq.indexOf(new Integer(count)));
   if (index>=0)
   {
   	continue;
   }
    fldWidthStr = (String)fldWidth.get(count);

	if (EJBUtil.stringToNum(fldWidthStr) == 0)
	{
	 fldWidthStr = "15";
	}


   	  %>

   <TH width="<%=fldWidthStr%>%" nowrap><%=StringUtil.decodeString((String)fldDispName.get(count))%></TH>

<%}

if (mapSecIds!=null){
if (mapSecIds.size()>0){
for (int count=0;count<mapSecIds.size();count++){
	mapSecId=(String)mapSecIds.get(count);
	mapSecId=(mapSecId==null)?"":mapSecId.trim();

	//System.out.println("***mapSecId****" + mapSecId);
	if (mapSecId.length()>0){
    if ((mapSecId).equals(prevSecId)){continue;}else{%>
	<TH ><%=mapSecNames.get(count)%></TH>
	<%}
	prevSecId=(String)mapSecIds.get(count);
} else {%>
	<TH></TH>
<%}
}
%>
<%
}
}

} // end if for displaying header
for (int i=0;i<dataList.size();i++)
{
  tempList = (ArrayList) dataList.get(i);
   tempVal=new ArrayList();
   if ((i%2)==0){
   %>
  <tr class="plainRow">
   <%}else{
   %>
   <tr class="plainRow">
   <%}
   %>
  <%if (tempList.size()==0) continue;
   int maxTokens=0,tempCountToken=0;
   for (int j=2;j<tempList.size();j++){
  	ArrayList valList=new ArrayList();

  	String fieldTypeStr = "";

  	seq=0;
  	value=(String)tempList.get(j);
  	fieldTypeStr = (String) fieldTypes.get(j-2);

  	fldUseDataValue = (String) fieldDisplayDataValue.get(j-2)	;
  	if (StringUtil.isEmpty(fldUseDataValue))
  	{
  		fldUseDataValue = "0";
  	}

  	fldWidthStr = (String)fldWidth.get(j-2);
	if (StringUtil.isEmpty(fldWidthStr))
		{
		 fldWidthStr = "15";
		}


	if (value!=null){
	if (value.indexOf("[VELSEP]")>=0){
	value=StringUtil.replaceAll(value,"[VELSEP]","~");
	StringTokenizer valSt=new StringTokenizer(value,"~");
	tempCountToken=valSt.countTokens();
	//valList.add(valSt);
	tempVal.add(valSt);
	if (tempCountToken>maxTokens)	maxTokens=tempCountToken;
	continue;
	//value=EJBUtil.replaceAll(value,"[VELSEP]",", ");
	}
		 //check if its a date field and core data form, format the date




	}
	//System.out.println("EOL"+System.getProperty("line.separator"));
	//if (value!=null) value=StringUtil.replace(value,"[VELSEP]","<br>");
	else value="&#160;";

	fldDataSeperatorPos = value.indexOf("[VELSEP1]");

	if (fldDataSeperatorPos >=0){
		if (fldUseDataValue.equals("0"))
		{
			value=value.substring(0,fldDataSeperatorPos);
		}
		else
		{
			value=value.substring(fldDataSeperatorPos + 9);
		}
	}
	value=StringUtil.replaceAll(value,"[VELCOMMA]",",");
  %>
   <td style="word-wrap:break-word"

   <% if (!((fldWidthStr.trim()).equals("0")) ) { %>
   		width="<%=fldWidthStr%>%"
   <%  } %>

   > <%=value %>
         </td>
 <%
}%>
<% if (repFldNames.size()>0){%>
<td valign=top>
<table border="1" width='100%'>
<%int colCounter=0,counter=0,toCount=0,currCount=0;
  String currSec="",prevSec="";
  String dispVal="";
StringTokenizer tempSt;

while (colCounter<repFldNames.size())
{
 if (mapSecNames.size()>0)
 currSec=(String)mapSecNames.get(colCounter);
 //do the processing here to display data for the repeated fields
 toCount=(colCounter);


if ((colCounter>0) && !currSec.equals(prevSec))
{
if (maxTokens>0)
{
for (int tCount=1;tCount<=maxTokens;tCount++){

%>
<tr>
<%
counter=0;


while ((counter + currCount )<toCount){
   dispVal="";
   tempSt=((StringTokenizer)tempVal.get(counter+currCount));
      if (tempSt.hasMoreTokens()) dispVal=(String)tempSt.nextToken();
      dispVal=(dispVal==null)?"":dispVal;

      fldUseDataValue = (String) repFieldDisplayDataValue.get(counter+currCount);
      fldDataSeperatorPos = dispVal.indexOf("[VELSEP1]");

		if (fldDataSeperatorPos >=0){
			if (fldUseDataValue.equals("0"))
			{
				dispVal = dispVal.substring(0,fldDataSeperatorPos);
			}
			else
			{
				dispVal = dispVal.substring(fldDataSeperatorPos + 9);
			}
		}
      dispVal = StringUtil.replaceAll(dispVal,"[VELCOMMA]",",");
      //if (dispVal.length()==0) dispVal="-";
      %>
      <td style="word-wrap:break-word"

      <%if (!(fldWidthStr.trim().equals("0")) ) { %>
      	width="<%=fldWidthStr%>%"
      <% } %>

      ><%=dispVal%></td>
<%counter++;
 }%>
 </tr>
 <%

 }
 currCount=toCount;
 //counter=toCount;
 }
//end processing here
%>
</table></td>
<td width="<%=fldWidthStr%>%" valign=top>
<table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl">
<%if (((String)repFldNames.get(colCounter)).length()>=10){%>
<th width="<%=fldWidthStr%>%" ><font size="1"><a href="javascript:void(0);" onmouseover="return overlib('< ="<tr><td><font size=1>"+LC.L_Section/*Section*****/+": "+ StringUtil.escapeSpecialCharJS((String)((mapSecNames.size()>0)?mapSecNames.get(colCounter):"None"))+"</font></td></tr><tr><td><font size=1>"+LC.L_Field/*Field*****/+": "+ StringUtil.escapeSpecialCharJS((String)repFldNames.get(colCounter))+"</font></td></tr>"%>');" onmouseout="return nd();"><%=((String)repFldNames.get(colCounter)).substring(0,10)%></font></th>
<%} else {%>
<th width="<%=fldWidthStr%>%" ><font size="1"><a href="javascript:void(0);" onmouseover="return overlib('<%="<tr><td><font size=1>"+LC.L_Section/*Section*****/+": "+StringUtil.escapeSpecialCharJS((String)((mapSecNames.size()>0)?mapSecNames.get(colCounter):"None"))+"</font></td></tr><tr><td><font size=1>"+LC.L_Field/*Field*****/+": "+ StringUtil.escapeSpecialCharJS((String)repFldNames.get(colCounter))+"</font></td></tr>"%>');" onmouseout="return nd();"><%=((String)repFldNames.get(colCounter))%></font></th>
<%}%>
<%}
  else{%>
 <%if (((String)repFldNames.get(colCounter)).length()>=10){%>
<th width="<%=fldWidthStr%>%"><font size="1"><a href="javascript:void(0);" onmouseover="return overlib('<%="<tr><td><font size=1>"+LC.L_Section/*Section*****/+": "+StringUtil.escapeSpecialCharJS((String)((mapSecNames.size()>0)?mapSecNames.get(colCounter):"None"))+"</font></td></tr><tr><td><font size=1>"+LC.L_Field/*Field*****/+": "+ StringUtil.escapeSpecialCharJS((String)repFldNames.get(colCounter))+"</font></td></tr>"%>');" onmouseout="return nd();"><%=((String)repFldNames.get(colCounter)).substring(0,10)%></font></th>
<%}else{%>
<th width="<%=fldWidthStr%>%"><font size="1"><a href="javascript:void(0);" onmouseover="return overlib('<%="<tr><td><font size=1>"+LC.L_Section/*Section*****/+": "+StringUtil.escapeSpecialCharJS((String)((mapSecNames.size()>0)?mapSecNames.get(colCounter):"None"))+"</font></td></tr><tr><td><font size=1>"+LC.L_Field/*Field*****/+": "+StringUtil.escapeSpecialCharJS((String)repFldNames.get(colCounter))+"</font></td></tr>"%>');" onmouseout="return nd();"><%=((String)repFldNames.get(colCounter))%></font></th>
<%}%>
<%}
 colCounter++;
 prevSec=currSec;
}

 //do this processing to display the elements of last section to the end of list

if ((toCount+1) == repFldNames.size())
	if (maxTokens>0){
	for (int tCount=1;tCount<=maxTokens;tCount++){

	%>
	<tr>
	<%
	counter=0;

	while ((counter + currCount )<toCount+1){
	   dispVal="";
	   tempSt=((StringTokenizer)tempVal.get(counter+currCount));

	   	fldUseDataValue = (String) repFieldDisplayDataValue.get(counter+currCount);


	      if (tempSt.hasMoreTokens()) dispVal=(String)tempSt.nextToken();
	      dispVal=(dispVal==null)?"":dispVal;

	         fldDataSeperatorPos = dispVal.indexOf("[VELSEP1]");

			if (fldDataSeperatorPos >=0){
				if (fldUseDataValue.equals("0"))
				{
					dispVal = dispVal.substring(0,fldDataSeperatorPos);
				}
				else
				{
					dispVal = dispVal.substring(fldDataSeperatorPos + 9);
				}
			}
	      dispVal = StringUtil.replaceAll(dispVal,"[VELCOMMA]",",");
	      %>
	      <td style="word-wrap:break-word"
	      <% if (!(fldWidthStr.trim().equals("0")) ) { %>
	      	width="<%=fldWidthStr%>%"
	      <%}%>
	      ><%=dispVal%></td>
	<%counter++;
	 }%>
	 </tr>
	 <%

	 }
	 currCount=toCount;
	 //counter=toCount;
	 }



//End for do this processing to display the elements of last section to the end of list


 %>
</table></td>
<%}//end for repFldNames.size()>0
%>
</tr>
<%
}

if ( (repeatFormHeader) || ((!repeatFormHeader) && ( ((y + 1) == patRecEndCounter) || (y+1) ==  ids.size() )   ))
{ %>
	</table>
<%
} //to end the table
	} // table for patient and form .........end for formids loops

	if ( (repeatFormHeader))
	{
	%>
		<BR>
	<%
	}
	//exit from patient ids loop if required
 } //end for patient ids for loop

 /*String xmlStr = "";

 xmlStr = rdHolder.getReportXML();
 System.out.println(xmlStr ); */

 } // end of else for if(isSingleForm)
%>
<input type="hidden" name="from"  value="<%=from%>">
<input type="hidden" name="filter"  value='<%=StringUtil.encodeString(filter)%>'>



<input type="hidden" name="patTotalRecToDisplayStr"  value=<%=patTotalRecToDisplayStr%>>
<input type="hidden" name="repHeader"  value="<%=repHeader%>">
<input type="hidden" name="repFooter"  value="<%=repFooter%>">
<input type="hidden" name="headerAlign"  value="<%=headAlign%>">
<input type="hidden" name="footerAlign"  value="<%=footAlign%>">
<input type="hidden" name="patRecStartCounterStr"  value=<%=patRecStartCounter%>>
<input type="hidden" name="origPatRecStartCounter"  value=<%=origPatRecStartCounter%>>
<input type="hidden" name="formPatient"  value=<%=formPatient%>>


<%if (! isSingleForm) { %>
<table width="100%">
<hr class="thickLine">
<tr height="20"></tr>
<%
%>
<%if (repFooter.length()>0){
%>
<tr><td class="reportFooter" align="<%=footAlign%>" ><pre><%=repFooter%></pre></td></tr>
<%}
%>
</table>
<% } // end of if for if(!isSingleForm) %>

<input type="hidden" name="sess" value="keep">
</form>
<%

} else {  //else of if body for session
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
<jsp:include page="bottompanel.jsp" flush="true"/>


</body>
</html>
