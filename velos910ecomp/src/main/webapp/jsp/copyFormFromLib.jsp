<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</HEAD>

<script>

function openFormTypeWin(formobj) 
{
		param1="fieldCategory.jsp?type=T&mode=N";
		windowName= window.open(param1,"information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=600,height=300");
		windowName.focus();
}

function openEditFormTypeWin(formobj,catLibId)
{
		
        param1="fieldCategory.jsp?catLibId="+catLibId+"&mode=M&type=T";
		windowName= window.open(param1,"information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=600,height=300");
		windowName.focus();
}


function  validate(formobj, formobj1, numRows)
 {
 	
 	 formobj.newFormType.value = formobj1.newFormType.value ;
	 formobj.formName.value  = formobj1.txtName1.value ;
	flag = false ;
	
	if(numRows > 1)
	{
		for ( count = 0 ; count < numRows ; count ++ )
		{
			
			if (  formobj.selectedForms[count].checked )
			{
				flag = true ;
				break ;
			}
		} 
	}
	
	else if (numRows==1)
	{
		if (  formobj.selectedForms.checked )
				{
					flag = true ;
					
				}
	}
 
 
 
    if ( flag == false ) 
	{
		alert("<%=MC.M_SelFrm_ToBeCopied%>");/*alert("Please select a form to be copied ");*****/
		return false ;
	}
	
	if (   !(validate_col (' Category', formobj1.newFormType)  )  )  return false;
		
 	if (   !(validate_col (' Form Name', formobj1.txtName1)  )  )  return false;
	
		
     if (  !(validate_col('e-Signature',formobj.eSign)     )   ) return false;
	 
	

  }

function openwin1(fid) {
	windowName=window.open("formpreview.jsp?formId="+fid,"1","toolbar=no,scrollbars=yes,resizable=no,menubar=no,status=yes,width=825,height=600,Left=100,Top,150")
	windowName.focus();
}  

</script>

<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1) 
   	ienet = 0; //IE
else
	ienet = 1;

if (ienet == 0) {
%>
<body style="overflow:scroll">
<%} else {%>
<body>
<%}%>


<title><%=LC.L_Forms_Copy%><%--Forms >> Copy*****--%> </title>


<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<% String src;

src= request.getParameter("srcmenu");

%>

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   

<script>
	checkQuote="N";
	</script>
<br>
<jsp:useBean id="formlibB" scope="request" class="com.velos.eres.web.formLib.FormLibJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="catLibJB" scope="request"  class="com.velos.eres.web.catLib.CatLibJB"/>
<jsp:useBean id ="userB" scope="page" class="com.velos.eres.web.user.UserJB"/> <!--km-->
<jsp:useBean id ="userSiteB" scope="request" class="com.velos.eres.web.userSite.UserSiteJB"/>

<%@ page language = "java" import = "com.velos.eres.business.site.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.*"
%>

<DIV class="tabDefTopN" id="div1">  
<%
 HttpSession tSession = request.getSession(true); 
 
  if (sessionmaint.isValidSession(tSession))
	{
%>	
	
<jsp:include page="librarytabs.jsp" flush="true"></jsp:include>

</DIV>
	<DIV class="tabDefBotN" id="div3">
<%
		 String selectedTab = request.getParameter("selectedTab");
		 String codeStatus = request.getParameter("codeStatus");
		 String calledFrom = request.getParameter("calledFrom");
		
		ArrayList userSiteIds = new ArrayList();
		ArrayList userSiteSiteIds = new ArrayList();	
		ArrayList userSiteRights = new ArrayList();

		 
		 String txtName=request.getParameter("txtName");
		 String txtFormType=request.getParameter("ByFormType");
		 String txtName1 = request.getParameter("txtName1");
		 if (txtName1 == null)
		 txtName1 = "";
		 String formType1=request.getParameter("newFormType");
		 int iFormType= EJBUtil.stringToNum(txtFormType);
		 int iFormType1 = EJBUtil.stringToNum(formType1) ;
		 String accId=(String)tSession.getAttribute("accountId");
		 String usr = (String) tSession.getValue("userId");
		 int iusr= EJBUtil.stringToNum(usr);
		 int iaccId=EJBUtil.stringToNum(accId);
		
		 
		userB.setUserId(EJBUtil.stringToNum(usr));
		userB.getUserDetails();
		int userPrimOrg = EJBUtil.stringToNum(userB.getUserSiteId());
		UserSiteDao userSiteDao = userSiteB.getUserSiteTree(EJBUtil.stringToNum(accId),EJBUtil.stringToNum(usr));
		userSiteIds = userSiteDao.getUserSiteIds();
		userSiteSiteIds = userSiteDao.getUserSiteSiteIds();
		userSiteRights = userSiteDao.getUserSiteRights();
		int len1 = userSiteIds.size();
			
			
			String siteIdAcc="";
			int accrights=0;
			int userSiteSiteId=0;		
			int userSiteRight=0;
			for (int cnt=0;cnt<len1;cnt++) {
	 		userSiteSiteId = EJBUtil.stringToNum(((userSiteSiteIds.get(cnt)) == null)?"-":(userSiteSiteIds.get(cnt)).toString());
     		userSiteRight = EJBUtil.stringToNum(((userSiteRights.get(cnt)) == null)?"-":(userSiteRights.get(cnt)).toString());
			
			if (userPrimOrg!=userSiteSiteId)
			   accrights=userSiteRight;
			if(accrights!=0)
			siteIdAcc=siteIdAcc+(userSiteSiteId+",");
					
			}
		        siteIdAcc=siteIdAcc+userPrimOrg; 
			
		 
		 String catLibType="T" ;
		 ArrayList ids= new ArrayList();
		 ArrayList names= new ArrayList();
		 ArrayList ids1= new ArrayList();
		 ArrayList names1= new ArrayList();
		 CatLibDao catLibDao=new CatLibDao();

		 String pullDown="";
		 String pullDown1= "";
		 String formType="";
		 String name="";
		 String desc="";
		 String status="";
		 String sharedWith="";
		 String formId="";
		 String catLibId="";

		 ArrayList arrFormLibIds = null;
		 ArrayList arrCatLibId=null;
		 ArrayList arrFormType=null;
		 ArrayList arrName=null;
		 ArrayList arrDesc=null;
		 ArrayList arrStatus=null;
		 ArrayList arrSharedWith=null;
		 FormLibDao formLibDao=new FormLibDao();
		 int pageRight = 7;
		String mode=request.getParameter("mode");
		int forLastAll = 0 ;
		if(mode==null)
		{
		
		mode="visited";
		///
		
		
		catLibDao= catLibJB.getCategoriesWithAllOption(iaccId,catLibType, forLastAll);
		ids = catLibDao.getCatLibIds();
		names= catLibDao.getCatLibNames();
		pullDown=EJBUtil.createPullDown("ByFormType", 0, ids, names);
		
		catLibDao= catLibJB.getAllCategories(iaccId,catLibType);
		ids1 = catLibDao.getCatLibIds();
		names1= catLibDao.getCatLibNames();
		pullDown1=EJBUtil.createPullDown("newFormType", 0, ids1, names1);
	
		formLibDao=formlibB.getFormDetails(iaccId,iusr,"",-2, siteIdAcc);
		arrFormLibIds = formLibDao.getArrFormLibIds();
		arrFormType=formLibDao.getFormTypes();
		arrName=formLibDao.getFormNames();
		arrDesc=formLibDao.getDescriptions();
		arrStatus=formLibDao.getCodeLst();
		arrSharedWith=formLibDao.getSharedWith();
		arrCatLibId=formLibDao.getArrCatLibIds();
		
		}

		else
		{
		 
		 catLibDao= catLibJB.getCategoriesWithAllOption(iaccId,catLibType,forLastAll);
		 ids = catLibDao.getCatLibIds();
		 names= catLibDao.getCatLibNames();
		 pullDown=EJBUtil.createPullDown("ByFormType",iFormType, ids, names);
		 pullDown1=EJBUtil.createPullDown("newFormType", iFormType1, ids, names);
		 

		 if ((iFormType==-1) || (iFormType==0)) {
		   iFormType=-2;
		 }
			
		 formLibDao=formlibB.getFormDetails(iaccId,iusr,txtName.trim(),iFormType);
		
		 arrFormLibIds = formLibDao.getArrFormLibIds();
		 arrFormType=formLibDao.getFormTypes();
		 arrName=formLibDao.getFormNames();
		 arrDesc=formLibDao.getDescriptions();
		 arrStatus=formLibDao.getCodeLst();
		 arrSharedWith=formLibDao.getSharedWith();
		 arrCatLibId=formLibDao.getArrCatLibIds();
		
		 }
		 int len=arrName.size();
if (pageRight >=6) 
	{
		
 %>
		
<Form name="formlibSearch" action="copyFormFromLib.jsp"  method="post"> 
	<Input type="hidden" name="mode" value=<%=mode%> >
	<Input type="hidden" name="formId" value=<%=formId%> >
	<input type="hidden" name="selectedTab" value=<%=selectedTab%>>
	<input type="hidden" name="srcmenu" value=<%=src%>>
	
	<table width="99%" border="0" cellspacing="0" cellpadding="0" class="midAlign">
	<tr><td colspan="2"><P class="defComments"><%=MC.M_EtrDet_ForNewFrm%><%--Enter the following details for your new form*****--%></P>
	</td>    
	</tr>
	<tr>
	<td width="10%" align="right"><%=LC.L_Frm_Name%><%--Form Name*****--%> <FONT class="Mandatory">* </FONT>&nbsp;&nbsp;&nbsp;</td>
	 <td width="90%" ><input type="text" name="txtName1" size = 50 MAXLENGTH = 50 value="<%=txtName1%>"> </td>
	 </tr>
	 <tr>
	 <td align="right"><%=LC.L_Form_Type%><%--Form Type*****--%><FONT class="Mandatory">* </FONT>&nbsp;&nbsp;&nbsp;</td>
	<td ><%=pullDown1%> </td>
	</tr>	
	</table>
	<table width="99%" cellspacing="0" cellpadding="0" border="0" class="midAlign">
      <tr>
	  <td colspan="5">
	<P class="defComments"> <%=MC.M_SelFrm_CopyClkSbmt%><%--Select one or more forms from your library that you wish to copy, then click on Submit.*****--%></P>
	</td>
	</tr>
	<tr>	
		<td width="20%"><%=LC.L_Search_By%><%--Search by*****--%> </td>
 	   <td width = "10%"><%=LC.L_Frm_Name%><%--Form Name*****--%>: </td>
	   <td width="20%"><input type="text" name="txtName" size = 20 MAXLENGTH = 50> </td>
	   <td width="10%" align="center"><%=LC.L_By_FormType%><%--By Form Type*****--%></td>
		<td width="20%"><%=pullDown%> </td>
		<td width ="20%" align="center"><button type="submit"><%=LC.L_Search%></button></td>
	</tr>
	<tr height="7"></tr>
</table>
 </form>
<Form name="copyForms" method="POST" id="cpyFormLib" action="copyFormFromLibSubmit.jsp"  onSubmit="if (validate(document.copyForms, document.formlibSearch,'<%=len%>')== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
<Input type="hidden" name="mode" value=<%=mode%> >
<Input type="hidden" name="formId" value=<%=formId%> >
<input type="hidden" name="selectedTab" value=<%=selectedTab%>>
<input type="hidden" name="srcmenu" value=<%=src%>>
<input type="hidden" name="formName" >
<input type="hidden" name="newFormType" >
	<table width="99%" cellspacing="1" cellpadding="0" border="0" class="outline midAlign">
					<tr> 
				        <th width="40%"> <%=LC.L_Name%><%--Name*****--%> </th>
				        <th width="45%"> <%=LC.L_Description%><%--Description*****--%> </th>
				        <th width="15%"> <%=LC.L_Selected%><%--Selected*****--%> </th>
					</tr>
				   <%int count=0;
		for(count=0;count<len;count++)
		{
				
		              
						String tempFormType=(arrFormType.get(count)).toString();
												
						if ( count !=0 )
						{ 
						  formType=tempFormType;
						  
						}
						if (( count == 0 ) || ( ! tempFormType.equals((arrFormType.get(count-1)).toString())) )
						{
						 formType=tempFormType;
						}
						else
						{
							formType=" ";	
						}



			name= ((arrName.get(count)) == null)?"-":(arrName.get(count)).toString();
			desc= ((arrDesc.get(count)) == null)?"-":(arrDesc.get(count)).toString();
			status= ((arrStatus.get(count)) == null)?"-":(arrStatus.get(count)).toString();
			sharedWith= ((arrSharedWith.get(count)) == null)?"-":(arrSharedWith.get(count)).toString();
			formId= ((arrFormLibIds.get(count)) == null)?"-":(arrFormLibIds.get(count)).toString();
			catLibId= ((arrCatLibId.get(count)) == null)?"-":(arrCatLibId.get(count)).toString();
			

			if ((count%2)==0) 
				{%>
			 <tr class="browserEvenRow"> 
				<%}else
				 { %>
				 <tr class="browserOddRow"> 
				<%}%>	
					
				<td width="40%"><A href="#" onClick=openwin1(<%=formId%>)><%=name%></A></td>
				<td width="45%"><%=desc%></td>	
				<td width="15%" align="center"><input type="checkbox" name="selectedForms" value="<%=formId%>" onClick=""></td>	
				  <%if(sharedWith.equals("P"))
					{
					  sharedWith=LC.L_Private;/*sharedWith="Private";*****/
					}
				  else if(sharedWith.equals("A"))
					{
					  sharedWith=LC.L_All_AccUsers;/*sharedWith="All Account Users";*****/
					}
				  else if(sharedWith.equals("G"))
					{
					  sharedWith=MC.M_AllUsr_InGrp;/*sharedWith="All Users in a Group";*****/
					}
				  else if(sharedWith.equals("S"))
					{
					  sharedWith=MC.M_UsrsStd_Team;/*sharedWith="All Users in a "+LC.Std_Study+" Team";*****/
					}
				  else if(sharedWith.equals("O"))
					{
					  sharedWith=MC.M_AllUsr_InOrg;/*sharedWith="All Users in an Organization";*****/
					}%>
											
				</tr>
		<%}%>

					
	</table>
	</table>
		<BR>	
		
				<input type="hidden" name="formId" value=<%=formId%>>
<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="cpyFormLib"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>
	</FORM>
	 <%
		
	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right
	}
else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
 <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div><div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/></div>

</body>

</html>


			
