<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>  
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</HEAD>
<%@ page import ="java.util.*, java.io.*, org.w3c.dom.*,com.velos.eres.business.common.*, com.velos.eres.service.util.EJBUtil"  %>
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="TeamB" scope="request" class="com.velos.eres.web.team.TeamJB"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*, com.velos.eres.service.util.EJBUtil, com.velos.eres.service.util.*"%>
<%
    

	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))
   {	  
   
   	String user = null;
	user = (String) tSession.getValue("userId");
	String accId = (String) tSession.getValue("accountId");	
	String ipAdd = null;
	ipAdd = (String) tSession.getValue("ipAdd");
      
   
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%   
    String eSign = request.getParameter("eSign");
   
    String oldESign = (String) tSession.getValue("eSign");

	if(!oldESign.equals(eSign)) {
	%>
	<jsp:include page="incorrectesign.jsp" flush="true"/>	
	<%
	} else {
	
	String strNumRows = "";
	int numrows = 0;
	
	strNumRows = request.getParameter("numrows");    
    numrows = EJBUtil.stringToNum(strNumRows);
    
    String[] userId = new String[numrows];
    String[] studyId = new String[numrows]; 
    String[] RoleStrings = new String[numrows]; //JM: added

    
    for (int k = 0; k < numrows; k++)
    {
    
     userId[k] = request.getParameter("userId"+k);    
     studyId[k] = request.getParameter("studyId"+k);
     
	 RoleStrings[k] = request.getParameter("role"+k); //JM: added 
    
    }
    
    //String[] RoleStrings = request.getParameterValues("role"); 
        
    
	int ret = TeamB.addUsersToMultipleStudies(userId,studyId,RoleStrings,EJBUtil.stringToNum(user), ipAdd);
	

				if (ret > 0)
					{
				%>		
						<br><br><br><br><br>
						<p class = "successfulmsg" align = center><%=MC.M_Data_SvdSucc %><%-- Data was saved successfully.*****--%></p>
						<script>
						 
						    					    
							setTimeout("self.close()",1000);

						</script>

				<% 
					} else if (ret == 0)
					{%>
					
					    <br><br><br><br><br>
						<p class = "successfulmsg" align = center><%=MC.M_Data_SvdSucc %><%-- Data was saved successfully.*****--%></p>
						<script>
						 
						    					    
							setTimeout("self.close()",1000);

						</script>
					
					<%}
					
					else {
				%>		
						<br><br><br><br><br>
						<p class = "successfulmsg" align = center><%=MC.M_DataCnt_SvdSucc%><%-- Data could not be saved successfully.*****--%></p>
						<script>
						
							setTimeout("window.history.back()",1000);
							
							
						</script>

				<% 	
					} // end of if data saved				
} //end of if for eSign 

} //end of session 
else {   

%>
  <jsp:include page="timeout.html" flush="true"/>
<%
}
%> 
</BODY>
</HTML>
