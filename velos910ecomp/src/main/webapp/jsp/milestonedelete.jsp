<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=LC.L_Mstone_Del%><%--Milestone Delete*****--%></title>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%@page import="com.aithent.audittrail.reports.AuditUtils"%>
<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>
<jsp:useBean id="milestoneB" scope="request" class="com.velos.eres.web.milestone.MilestoneJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/> 
<%@ page language = "java" import="com.velos.eres.web.milestone.MilestoneJB"%>

<jsp:include page="include.jsp" flush="true"/>  
	
<% String src;
	src= request.getParameter("srcmenu");
%>

<BODY> 
<br>

<DIV class="popDefault" id="div1">
<% 
String milestoneId= "";
String selectedTab = request.getParameter("selectedTab");
		
HttpSession tSession = request.getSession(true); 
 if (sessionmaint.isValidSession(tSession))	{

	milestoneId= request.getParameter("milestoneId");
	System.out.println("1"+milestoneId);
	String studyId = request.getParameter("studyId");



	int ret=0;
		
	String delMode=request.getParameter("delMode");
	
	if (delMode == null) {
		delMode=LC.L_Final_Lower/*"final"*****/;
%>
	<FORM name="milestonedelete" id="miledelfrmid" method="post" action="milestonedelete.jsp" onSubmit="if (validate(document.milestonedelete)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<br><br>
	
	<TABLE width="98%" cellspacing="0" cellpadding="0" >
		<tr><td align="center">
		<b><%=MC.M_PlsEtrEsign_Del%><%--Please enter e-Signature to proceed with deletion*****--%> </b>
		</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
	
		</table>
			
<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="miledelfrmid"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>

	<input type="hidden" name="delMode" value="<%=delMode%>">
	<input type="hidden" name="srcmenu" value="<%=src%>">
	<input type="hidden" name="milestoneId" value="<%=milestoneId%>">
	<input type="hidden" name="studyId" value="<%=studyId%>">  
	<input type="hidden" name="selectedTab" value="<%=selectedTab%>">
	

	</FORM>
<%
	} else {%>
   <%

			String eSign = request.getParameter("eSign");	
            milestoneId= request.getParameter("milestoneId");			
            studyId=request.getParameter("studyId");			
            selectedTab=request.getParameter("selectedTab");		
            src=	request.getParameter("srcmenu");		
            delMode=null;
			String oldESign = (String) tSession.getValue("eSign");

			if(!oldESign.equals(eSign)) {
%>        
	<br> <br> 	<br> <br> 	<br> <br> 	<br> <br> 	<br> <br> 

 <table width=100%>

 

<tr>

<td align=center>

<p class = "successfulmsg">

<%=MC.M_EtrWrongEsign_ClkBack%><%--You have entered a wrong e-Signature. Please click on Back button to enter again.*****--%>

</p>

</td>

</tr>

<tr height=20></tr>

<tr>

<td align=center>

<!--		<A href="#" onclick="window.history.back();"><img src="../images/jpg/Back.gif" align="absmiddle"  border=0> </A>-->


</td>		

</tr>		

</table>		
   <table>	<tr><td width="85%"></td><td>&nbsp;&nbsp;
<button tabindex=2 onClick="history.go(-1);"><%=LC.L_Back%></button>	
</td></tr>	</table>


	
          
 		  
         
<%
			} else {		
				
			/*
				
			//	ret = lineitemB.lineitemDelete(EJBUtil.stringToNum(lineitemId));
			milestoneB.setId(EJBUtil.stringToNum(milestoneId));
			System.out.println("2"+milestoneId);
			milestoneB.getMilestoneDetails();
			milestoneB.setMilestoneDelFlag("Y");
			ret = milestoneB.updateMilestone();		
				
			*/
				
			StringTokenizer mileIds=new StringTokenizer(milestoneId,",");
			int numIds=mileIds.countTokens();
			String[] strArrMileIds = new String[numIds] ;
				
			for(int cnt=0;cnt<numIds;cnt++){
				
				strArrMileIds[cnt] = mileIds.nextToken();
				
				
				MilestoneJB mlJB= new MilestoneJB();					
					
				mlJB.setId(EJBUtil.stringToNum(strArrMileIds[cnt]));					
				mlJB.getMilestoneDetails();
				mlJB.setMilestoneDelFlag("Y");				
				// Modified for INF-18183 ::: Raviesh
				ret = mlJB.updateMilestone(AuditUtils.createArgs(session,"",LC.L_Milestones));		
				
			}		
			

  
			if (ret==-2) {%>
				<br><br><br><br><br> <p class = "successfulmsg" align = center>  <%=MC.M_Mstone_RecNotDel%><%--Milestone record not deleted.*****--%> </p>			
			<%}else { %>
				<br><br><br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_Mstone_RecordDel%><%--Milestone record deleted.*****--%> </p>
			 	<script>
			 		//Changed for Bug#7599 : Raviesh
			 	    window.opener.location.reload(true);					
					setTimeout("self.close()",1000);
				</script>
			
		<%

			}
			
	
			} //end esign
			
			

	} //end of delMode
	
  }//end of if body for session 
else { %>
 <jsp:include page="timeout.html" flush="true"/> 
 <% } %> <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</DIV>  


</body>
</HTML>


