<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<%

String userType = request.getParameter("userType");

	

if (userType.equals("B")) {%> 

<title><%=LC.L_Bgt_Users%><%--Budget Users*****--%></title>

<%} else {%>

<title><%=LC.L_Evt_UserPage%><%--Event User Page*****--%></title>

<%}%>



<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>



<Link Rel=STYLESHEET HREF="common.css" type=text/css>





<% if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("eventlibrary"))

{ %>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>	
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

 <% } //end for if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("eventlibrary"))

	%>

</head>



<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import = "com.velos.eres.service.util.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.esch.business.common.EventInfoDao"%>

<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB"/>

<jsp:useBean id ="budgetUsersB" scope="request" class="com.velos.esch.web.budgetUsers.BudgetUsersJB"/>

<jsp:useBean id="budgetUsersDao" scope="request" class="com.velos.esch.business.common.BudgetUsersDao"/>



<% response.setHeader("Cache-Control", "max-age=1, must-revalidate");%>





<% 

	String src = request.getParameter("srcmenu");
	String duration = request.getParameter("duration"); 
	String protocolId = request.getParameter("protocolId"); 
	String calledFrom = request.getParameter("calledFrom"); 
	String eventId = request.getParameter("eventId");
	String mode = request.getParameter("mode");
	String fromPage = request.getParameter("fromPage");
	String budgetId = request.getParameter("budgetId");

%>

	

<body>



<DIV id="div1"> 

<%





	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))

	{
 %>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<% 	
	   String uName = (String) tSession.getValue("userName");
	   ArrayList userIds =null;
	   ArrayList userLnames =null;
	   ArrayList userFnames =null;
	   ArrayList userSiteNames = null;
	   int rows=0;	   
		String accountId = (String) tSession.getValue("accountId");
		String userJobType = "";
		String dJobType = "";
		String dAccSites ="";
   		CodeDao cd = new CodeDao();
		CodeDao cd2 = new CodeDao();

	   if (userType.equals("B")) { //called from Budget Rights 

		 	budgetUsersDao= budgetUsersB.getUsersInBudget(EJBUtil.stringToNum(budgetId));

		 	userIds=budgetUsersDao.getBgtUsers(); 
			
			userSiteNames = budgetUsersDao.getUsrSiteNames();
		 	userLnames=budgetUsersDao.getUsrLastNames();
		 	userFnames=budgetUsersDao.getUsrFirstNames();	
		 	rows = budgetUsersDao.getBRows();   
			
		cd2.getAccountSites(EJBUtil.stringToNum(accountId)); 
    	cd.getCodeValues("job_type");
		userJobType =userB.getUserCodelstJobtype();
		dJobType = cd.toPullDown("jobType", EJBUtil.stringToNum(userJobType));	
			
				   

	   } else {

		   UserDao userDao = userB.getUsersForEvent(EJBUtil.stringToNum(eventId),userType);
		   userIds = userDao.getUsrIds();
		   userSiteNames = userDao.getUsrSiteNames();
		   userLnames = userDao.getUsrLastNames();
	   	   userFnames = userDao.getUsrFirstNames();
		   rows = userDao.getCRows();
		



		cd2.getAccountSites(EJBUtil.stringToNum(accountId)); 
    	cd.getCodeValues("job_type");
		userJobType =userB.getUserCodelstJobtype();
		dJobType = cd.toPullDown("jobType", EJBUtil.stringToNum(userJobType));	
		   

	   }		

	   
	   String userId = "";
	   String userLname = "";	
	   String userFname = "";	
	   String userName = "";	  
		String orgNam = "";
if(userType.equals("U")) {

%>

   <P class="sectionHeadings"> <%=MC.M_PcolCalEvt_EvtUsr%><%--Protocol Calendar >> Event Details >> Event User*****--%> </P>

<%

} else if(userType.equals("S")) {

%>

   <P class="sectionHeadings"> <%=MC.M_PcolCalEvt_UsrMail%><%--Protocol Calendar >> Event Details >> Users for Mail*****--%> </P>

<%

} else if(userType.equals("B")) {

%>

   <P class="sectionHeadings"> <%=MC.M_BgtAccRgt_UsrBgt%><%--Budget Access Rights >> Users for Budget*****--%> </P>

<%

}

%>





  <Form  name="search" method="post" action="addeventuser.jsp?budgetId=<%=budgetId%>&userType=<%=userType%>&eventId=<%=eventId%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>"  >

    <table class = tableDefault width="100%" cellspacing="0" cellpadding="0" border=0 >

      <tr valign = "bottom"> 

        <td class=tdDefault width=10%> <%=LC.L_User_Search%><%--User Search*****--%></td>
        <td class=tdDefault width=20%> <%=LC.L_First_Name%><%--First Name*****--%>: 
          <Input type=text name="fname">
        </td>
        <td class=tdDefault width=20%> <%=LC.L_Last_Name%><%--Last Name*****--%>: 
          <Input type=text name="lname">
        </td>		
		<td class=tdDefault width=20%> <%=LC.L_Job_Type%><%--Job Type*****--%>:
		 <%=dJobType%> 
	  	</td>
		<td> &nbsp;&nbsp</td>
	  	<td class=tdDefault width=20%> <%=LC.L_Organization_Name%><%--Organization Name*****--%>:
         <%dAccSites = cd2.toPullDown("accsites");%>
		 <%=dAccSites%> 
	  	</td>		
		
        <td class=tdDefault width=10%> 
          <button type="submit"><%=LC.L_Search%><%--<%=LC.L_Search%>*****--%></button>
        </td>
      </tr>
    </table>

  </Form>
 <Form name="user" method="post" >
<%

if(rows > 0) {

%>

    <table width="100%" cellspacing="2" cellpadding="0" border=0 >
      <tr> 
        <th width="50%"> <%=LC.L_User_Name%><%--User Name*****--%> </th>
		<th width="50%"> <%=LC.L_Organization%><%--Organization*****--%> </th>
      </tr>
<%
   for(int i = 0;i<rows;i++){	
	userId = userIds.get(i).toString();
	userLname = (String) userLnames.get(i);
	userFname = (String) userFnames.get(i);
	userName = userLname + ", " + userFname;	
	
	orgNam = (String)userSiteNames.get(i);
	
	if (orgNam == null){
	orgNam ="-";
	}

			//UserDao userDao = new UserDao();
			//userDao = userB.getUsersDetails(userId);
			//ArrayList organizationNames = userDao.getUsrSiteNames();
			//if(organizationNames.size() > 0 ){
			// orgNam = ((organizationNames.get(0)) ==null)?"-":(organizationNames.get(0)).toString();
			//}else{
			// orgNam = "-";
			//}

		if ((i%2)==0) {
  %>
      <tr class="browserEvenRow"> 
        <%
		}
		else{
	 %>
      <tr class="browserOddRow">
        <%
		}
  %>
  

        <td> <%=userName%></td>
		<td> <%=orgNam%></td>

      </tr>
<%
   }



%>

    </table>
	
	
	
		
	
	
	
	
	
	
	
	
	
	
	
	
	

  </Form>

<%

} else { // else of if for rows>0

%>



<% if(userType.equals("S")) {%>

<P class = "defComments"><%=MC.M_NoUsrAttached_ToEvt%><%--No users currently attached to this event*****--%></P>

<%} else {%>

<P class = "defComments"><%=MC.M_SpySrchCtr_ClkSrch%><%--Please specify the search criteria and click on "Search".*****--%></P> 

<%}%>



  <%

} // end of if for rows>0





}//end of if body for session



else



{%>

<jsp:include page="timeout.html" flush="true"/>

 <%

}

%>



</div>



</body>


</html>







