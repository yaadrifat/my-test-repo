/**
 * This is a utility class consisting of commonly used methods.
 *
 * @author Sajal
 *
 * @version 1.0
 *
 */

package com.aithent.file.uploadDownload;

/* Import all the exceptions can be thrown */
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

/**
 * This is a utility class consisting of commonly used methods.
 * 
 */
public final class EJBUtil {
    static int cReturn;

    /**
     * Returns the int value of a String. Throws an exception in case the String
     * supplied cannot be converted to a number and returns 0.
     * 
     * @param str
     *            a String value that needs to be converted to a integer
     * 
     * @return the integer value of the String supplied and 0 in case the string
     *         supplied is null, invalid or empty.
     * 
     */
    public static int stringToNum(String str) {
        int iReturn = 0;
        try {
            if (str == null) {
                return cReturn;
            } else if (str.equals("")) {
                return cReturn;
            } else {
                iReturn = Integer.parseInt(str);
                return iReturn;
            }
        } catch (Exception e) {
            System.out.println("{EXCEPTION IN CONVERTING TO NUMBER:}" + e);
            return cReturn;
        }
    }

    /**
     * Returns the int value of a String. Throws an exception in case the String
     * supplied cannot be converted to a number and returns 0.
     * 
     * @param str
     *            a String value that needs to be converted to a integer
     * 
     * @return the integer value of the String supplied and 0 in case the string
     *         supplied is null, invalid or empty.
     * 
     */
    /*
     * public static Hashtable getContextProp(){ try{
     * if(Configuration.getServerParam() == null) Configuration.readSettings();
     * }catch(Exception e){ System.out.println("EJBUtil:getContextProp:general
     * ex"+e); } return Configuration.getServerParam(); }
     */

    /**
     * Returns the absolute path of a file whose directory and file name has
     * been provided.
     * 
     * @param dir
     *            a String value that is the directory where the file is stored
     * 
     * @param fileName
     *            a String value that is the name of the file for which the
     *            absolute file path is required.
     * 
     * @return the String value representing the absolute path of the file
     * 
     */
    public static String getActualPath(String dir, String fileName) {
        String path;
        dir = dir.trim();
        fileName = fileName.trim();
        System.out.println("EJBUtil.getActualPath dir " + dir);
        System.out.println("EJBUtil.getActualPath fileName " + fileName);
        File f = new File(dir, fileName);
        try {
            path = f.getAbsolutePath();
        } catch (Exception e) {
            path = null;
        }
        return path;
    }

    /**
     * Gets the value of an environment variable
     * 
     * @param Name
     *            a String value that is the Name of the environment variable
     *            whose value is required
     * 
     * @return the String value representing the value of the environment
     *         variable 'Name'
     * 
     */
    public static String getEnvVariable(String Name) throws Exception {
        String iline = "";
        String envValue = "";
        int i = 0;
        int sType = 0;
        Process theProcess = null;
        BufferedReader stdInput = null;
        try {
            // Figure out what kind of system we are running on
            // 0 = windows
            // 1 = unix
            sType = getSystemType();
            if (sType == 0) {
                String envPath = "cmd /C echo %" + Name + "%";
                theProcess = Runtime.getRuntime().exec(envPath);
            } else {
                theProcess = Runtime.getRuntime().exec("printenv " + Name);
            }
            stdInput = new BufferedReader(new InputStreamReader(theProcess
                    .getInputStream()));
            envValue = stdInput.readLine();
            stdInput.close();
            theProcess.destroy();
            return envValue;
        } catch (IOException ioe) {
            throw new Exception(ioe.getMessage());
        }
    }

    /**
     * Gets the operating system type
     * 
     * @return <code>1</code> if the system is Windows based <code>0</code>
     *         otherwise.
     * 
     */
    public static int getSystemType() {
        Properties p = System.getProperties();
        String os = p.getProperty("os.name");
        int i = 0;
        int osType = 0;
        if ((i = os.indexOf("Win")) != -1) {
            osType = 0;
        } else {
            osType = 1;
        }
        return osType;
    }
}