/*
 * Classname			Rlog
 * 
 * Version information  1.0
 *
 * Date					03/07/2001
 * 
 * Copyright notice		Velos Inc.
 * 
 * Author 				Dinesh
 */

package com.aithent.audittrail.reports;

import org.apache.log4j.Category;
import org.apache.log4j.Priority;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * This class is just a helper class to make it handy to print,store,log,mail
 * debug statements This class implements log 4j which is an effective way of
 * tracking the debug messages to effectively track the errors.Log4j is a full
 * fleged logging service in itself
 * 
 * @author Dinesh
 * @version 1.0 03/07/2001
 */
public final class Rlog {

    public static Category cat = null;

    public static String logFile = null;

    public static String eHome = null;

    public static void init() { // to be initinalized in the login screen
        System.out.println("inside the configurator");
        DOMConfigurator.configure(getLogFile());
    }

    public static void debug(String module, String msg) {
        if (cat == null) {
            System.out.println("inside the configurator1");
            DOMConfigurator.configure(getLogFile());
        }
        cat = Category.getInstance(module);
        cat.log("com.velos.eres.service.util.Rlog", Priority.DEBUG, msg, null);
    }

    public static void info(String module, String msg) {
        if (cat == null) {
            System.out.println("inside the configurator2");
            DOMConfigurator.configure(getLogFile());
        }
        cat = Category.getInstance(module);
        cat.log("com.velos.eres.service.util.Rlog", Priority.INFO, msg, null);
    }

    public static void warn(String module, String msg) {
        if (cat == null) {
            System.out.println("inside the configurator3");
            DOMConfigurator.configure(getLogFile());
        }
        cat = Category.getInstance(module);
        cat.log("com.velos.eres.service.util.Rlog", Priority.WARN, msg, null);

    }

    public static void error(String module, String msg) {
        if (cat == null) {
            System.out.println("inside the configurator4");
            DOMConfigurator.configure(getLogFile());
        }
        cat = Category.getInstance(module);
        cat.log("com.velos.eres.service.util.Rlog", Priority.ERROR, msg, null);
    }

    public static void fatal(String module, String msg) {
        if (cat == null) {
            System.out.println("inside the configurator5");
            DOMConfigurator.configure(getLogFile());
        }
        cat = Category.getInstance(module);
        cat.log("com.velos.eres.service.util.Rlog", Priority.FATAL, msg, null);
    }

    public static String getLogFile() {
        System.out.println("***************Rlog.getLogFile line 1");
        if (logFile == null) {
            try {
                System.out.println("***************Rlog.getLogFile line 2");
                eHome = EJBUtil.getEnvVariable("AUDITREPORT_HOME");
                logFile = eHome + "Log4j.xml";
                System.out.println("***************Rlog.getLogFile line 3");
                System.out.println("Name of the LogFile XML" + logFile);
            } catch (Exception e) {
                System.out.println("Exception in reading eres home for log4j"
                        + e);
            }
        }
        System.out.println("***************Rlog.getLogFile line 4");
        return logFile;
    }
}
// end of class here
