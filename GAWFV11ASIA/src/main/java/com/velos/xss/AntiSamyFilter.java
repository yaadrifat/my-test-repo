package com.velos.xss;

import org.apache.log4j.Logger;
import org.owasp.validator.html.AntiSamy;
import org.owasp.validator.html.CleanResults;
import org.owasp.validator.html.Policy;
import org.owasp.validator.html.PolicyException;

import com.velos.eres.service.util.Configuration;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;

/**
 * Servlet filter that checks all request parameters for potential XSS attacks.
 *
 * @author barry pitman
 * @since 2011/04/12 5:13 PM
 */
public class AntiSamyFilter implements Filter {

    private static final Logger LOG = Logger.getLogger(AntiSamyFilter.class);

    /**
     * AntiSamy is unfortunately not immutable, but is threadsafe if we only call
     * {@link AntiSamy#scan(String taintedHTML, int scanType)}
     */
    private final AntiSamy antiSamy;

    public AntiSamyFilter() {
        try {
        	File file = new File(Configuration.ERES_HOME+"antisamy-highsecurity.xml");
            Policy policy = Policy.getInstance(file.getAbsolutePath());
            antiSamy = new AntiSamy(policy);
        } catch (PolicyException e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (request instanceof HttpServletRequest) {
            CleanServletRequest cleanRequest = new CleanServletRequest((HttpServletRequest) request, antiSamy);
            HttpServletResponse res = (HttpServletResponse)response;
            res.addHeader( "X-FRAME-OPTIONS", "SAMEORIGIN" ); 
            String sessionid=((HttpServletRequest) request).getSession().getId();
            //res.setHeader("SET-COOKIE", "JSESSIONID=" + sessionid + "; HttpOnly ;secure");
            chain.doFilter(cleanRequest, response);
        } else {
        	HttpServletResponse res = (HttpServletResponse)response;
            res.addHeader( "X-FRAME-OPTIONS", "SAMEORIGIN" );
            String sessionid=((HttpServletRequest) request).getSession().getId();
           // res.setHeader("SET-COOKIE", "JSESSIONID=" + sessionid + "; HttpOnly ;secure");
            chain.doFilter(request, response);
        }
    }

    public void init(FilterConfig filterConfig) throws ServletException {
    }

    public void destroy() {
    }

    /**
     * Wrapper for a {@link HttpServletRequest} that returns 'safe' parameter values by
     * passing the raw request parameters through the anti-samy filter. Should be private
     */
    public static class CleanServletRequest extends HttpServletRequestWrapper {

        private final AntiSamy antiSamy;

        private CleanServletRequest(HttpServletRequest request, AntiSamy antiSamy) {
            super(request);
            this.antiSamy = antiSamy;
        }

        /**
         * overriding getParameter functions in {@link ServletRequestWrapper}
         */
        @Override
        public String[] getParameterValues(String name) {
            String[] originalValues = super.getParameterValues(name);
            if (originalValues == null) {
                return null;
            }
            List<String> newValues = new ArrayList<String>(originalValues.length);
            for (String value : originalValues) {
                newValues.add(filterString(value));
            }
            return newValues.toArray(new String[newValues.size()]);
        }

        @Override
        @SuppressWarnings("unchecked")
        public Map getParameterMap() {
            Map<String, String[]> originalMap = super.getParameterMap();
            Map<String, String[]> filteredMap = new ConcurrentHashMap<String, String[]>(originalMap.size());
            for (String name : originalMap.keySet()) {
                filteredMap.put(name, getParameterValues(name));
            }
            return Collections.unmodifiableMap(filteredMap);
        }

        @Override
        public String getParameter(String name) {
            String potentiallyDirtyParameter = super.getParameter(name);
            return filterString(potentiallyDirtyParameter);
        }

        /**
         * This is only here so we can see what the original parameters were, you should delete this method!
         *
         * @return original unwrapped request
         */
        @Deprecated
        public HttpServletRequest getOriginalRequest() {
            return (HttpServletRequest) super.getRequest();
        }

        /**
         * @param potentiallyDirtyParameter string to be cleaned
         * @return a clean version of the same string
         */
        private String filterString(String potentiallyDirtyParameter) {
            if (potentiallyDirtyParameter == null) {
                return null;
            }

            try {
            	potentiallyDirtyParameter = stripXSS(potentiallyDirtyParameter);
                CleanResults cr = antiSamy.scan(potentiallyDirtyParameter, AntiSamy.DOM);
                if (cr.getNumberOfErrors() > 0) {
                    LOG.warn("antisamy encountered problem with input: " + cr.getErrorMessages());
                }
                System.out.println("innerHtml-"+cr.getCleanHTML());
                String cleanString=cr.getCleanHTML();
                System.out.println("str get before from polocies-"+cleanString +":"+cleanString.length());
             // cleanString=  cleanString.replaceAll("[<&gt;'&quot;]", "").replaceAll("&quot;", "").replaceAll("&gt;", "");
                cleanString=cleanString.replaceAll("/&gt;", "");
                cleanString=  cleanString.replaceAll("[<>]", "").replaceAll("&quot;", "").replaceAll("&gt;", "").replaceAll("&lt;", "");
                System.out.println("str get from polocies-"+cleanString);
                return cleanString;
                //return cr.getCleanHTML();
            } catch (Exception e) {
                throw new IllegalStateException(e.getMessage(), e);
            }
        }
        
        private String stripXSS(String value) {

            if (value != null) {

                // NOTE: It's highly recommended to use the ESAPI library and uncomment the following line to

                // avoid encoded attacks.

                // value = ESAPI.encoder().canonicalize(value);

     

                // Avoid null characters

                value = value.replaceAll("", "");

     

                // Avoid anything between script tags

                Pattern scriptPattern = Pattern.compile("<script>(.*?)</script>", Pattern.CASE_INSENSITIVE);

                value = scriptPattern.matcher(value).replaceAll("");

     

                // Avoid anything in a src='...' type of expression

                scriptPattern = Pattern.compile("src[\r\n]*=[\r\n]*\\\'(.*?)\\\'", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);

                value = scriptPattern.matcher(value).replaceAll("");

     

                scriptPattern = Pattern.compile("src[\r\n]*=[\r\n]*\\\"(.*?)\\\"", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);

                value = scriptPattern.matcher(value).replaceAll("");

     

                // Remove any lonesome </script> tag

                scriptPattern = Pattern.compile("</script>", Pattern.CASE_INSENSITIVE);

                value = scriptPattern.matcher(value).replaceAll("");

     

                // Remove any lonesome <script ...> tag

                scriptPattern = Pattern.compile("<script(.*?)>", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);

                value = scriptPattern.matcher(value).replaceAll("");

     

                // Avoid eval(...) expressions

                scriptPattern = Pattern.compile("eval\\((.*?)\\)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);

                value = scriptPattern.matcher(value).replaceAll("");

     

                // Avoid expression(...) expressions

                scriptPattern = Pattern.compile("expression\\((.*?)\\)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);

                value = scriptPattern.matcher(value).replaceAll("");

     

                // Avoid javascript:... expressions

                scriptPattern = Pattern.compile("javascript:", Pattern.CASE_INSENSITIVE);

                value = scriptPattern.matcher(value).replaceAll("");

     

                // Avoid vbscript:... expressions

                scriptPattern = Pattern.compile("vbscript:", Pattern.CASE_INSENSITIVE);

                value = scriptPattern.matcher(value).replaceAll("");

     

                // Avoid onload= expressions

                scriptPattern = Pattern.compile("onload(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);

                value = scriptPattern.matcher(value).replaceAll("");
                
                scriptPattern= Pattern.compile("onmouseover(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
                value = scriptPattern.matcher(value).replaceAll("");
                
                
                
                //

    	        scriptPattern= Pattern.compile("FSCommand(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			 value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onAbort(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			 value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onActivate(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onAfterPrint(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onAfterUpdate(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onBeforeActivate(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onBeforeCopy(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onBeforeCut(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onBeforeDeactivate(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onBeforeEditFocus(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onBeforePaste(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onBeforePrint(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onBeforeUnload(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onBeforeUpdate(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onBegin(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onBlur(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onBounce(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onCellChange(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onChange(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onClick(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onContextMenu(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onControlSelect(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onCopy(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onCut(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onDataAvailable(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onDataSetChanged(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onDataSetComplete(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onDblClick(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onDeactivate(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onDrag(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onDragEnd(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onDragLeave(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onDragEnter(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onDragOver(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onDragDrop(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onDragStart(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onDrop(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onEnd(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onError(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onErrorUpdate(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onFilterChange(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onFinish(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onFocus(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onFocusIn(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onFocusOut(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onHashChange(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onHelp(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onInput(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onKeyDown(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onKeyPress(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onKeyUp(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onLayoutComplete(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onLoseCapture(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onMediaComplete(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onMediaError(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onMessage(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onMouseDown(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onMouseEnter(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onMouseLeave(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onMouseMove(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onMouseOut(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onMouseUp(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onMouseWheel(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onMove(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onMoveEnd(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onMoveStart(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onOffline(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    	        scriptPattern= Pattern.compile("onOnline(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    			scriptPattern= Pattern.compile("(<img\\b[^>]*\\bsrc\\s*=\\s*)([\"\'])((?:(?!\\2)[^>])*)\\2(\\s*[^>]*>)",Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    			scriptPattern= Pattern.compile("(\"'>)<img(?![^>]*\balt=)[^>]*?>",Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    			scriptPattern= Pattern.compile("(?i)<.*?\\s+on.*?>.*?</.*?>",Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    			scriptPattern= Pattern.compile("(?i)<script.*?>.*?</script.*?>",Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    			scriptPattern= Pattern.compile("(?i)<.*?javascript:.*?>.*?</.*?>",Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    			scriptPattern= Pattern.compile("(?i)<.*?javascript:.*?>.*?</.*?>",Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    			value = scriptPattern.matcher(value).replaceAll("");
    			scriptPattern= Pattern.compile("alert(.*)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
                value = scriptPattern.matcher(value).replaceAll("");

            }

            return value;

        }
    }
}

