package com.velos.api.execption;


import org.apache.log4j.Logger;
/**
 * Represents any problem encountered when evaluating the context requested
 * @author Vishal Abrol
 */
public class InvalidContextException extends Exception {
    private static Logger logger = Logger.getLogger(InvalidContextException.class
            .getName());

    /**
     * Constructs an instance of <code>ApplicationException</code> with the
     * specified detail message.
     */
    public InvalidContextException(String msg) {
        super(msg);
        logger.fatal(msg);
    }
}
