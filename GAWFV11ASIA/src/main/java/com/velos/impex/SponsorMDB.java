package com.velos.impex;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJBException;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import com.velos.eres.service.util.EJBUtil;

/*
 Author : Sonia Sahni
 Date : 	 04/23/04
 */
@MessageDriven(activationConfig = {
       @ActivationConfigProperty(propertyName="destinationType",
         propertyValue="javax.jms.Topic"),
       @ActivationConfigProperty(propertyName="destination",
         propertyValue="topic/sponsor")})

public class SponsorMDB implements  MessageListener {
    private MessageDrivenContext ctx = null;

    public SponsorMDB() {
    }


    // --- MessageListener
    public void onMessage(Message message) {
        try {
            String siteCode = new String();
            String expId = new String();
            Hashtable htData = new Hashtable();
            ObjectMessage objMessage = null;
            String errStr = "";
            int reqId = 0;

            if (message instanceof ObjectMessage) {
                objMessage = (ObjectMessage) message;

                System.out.println("MDB Bean got message. The information is:");

                expId = objMessage.getStringProperty("velosExpId");
                siteCode = objMessage.getStringProperty("velosSourceSiteCode");

                System.out.println("\n velosExpId : " + expId);
                System.out.println("\n velosSourceSiteCode : " + siteCode);

                htData = (Hashtable) objMessage.getObject();

                System.out
                        .println("Read Message Object, Object contains data for "
                                + htData.size() + " modules");

                // get current date time

                Calendar now = Calendar.getInstance();
                String requestDate = "";
                requestDate = "" + now.get(now.DAY_OF_MONTH)
                        + now.get(now.MONTH) + (now.get(now.YEAR) - 1900);
                Calendar calToday = Calendar.getInstance();
                String format = "yyyy-MM-dd HH:mm:ss";
                SimpleDateFormat todayFormat = new SimpleDateFormat(format);
                requestDate = todayFormat.format(calToday.getTime());

                // Prepare Import Request

                ImpexRequest impReq = new ImpexRequest();
                Impex impexObj = new Impex();

                impReq.setReqStatus("0"); // import not started
                impexObj.setRequestType("IR"); // for Reverse Import
                // set datetime
                impReq.setReqDateTime(requestDate);
                impReq.setSiteCode(siteCode);
                impReq.setDataExpId(EJBUtil.stringToNum(expId)); // set the
                // expId of
                // the
                // original
                // export
                // req from
                // SITE

                // set the data received via JMS MSG
                impReq.setHtData(htData);

                // Log this import request

                impexObj.setImpexRequest(impReq);
                reqId = impexObj.recordImportRequest();

                System.out.println("Import Request ID" + reqId);

                // calls a wrapper method, synchronized, the method will execute
                // the request
                // according to the Request Type

                System.out.println("htData!!!!!!" + htData.toString());
                impexObj.start();
                // ***********wait for the thread to finish!!!
                try {
                    impexObj.join();
                } catch (InterruptedException ie) {
                    System.out
                            .println("Received a Message and got InterruptedException"
                                    + ie);
                }
                // Impex.impexWrapper(impexObj);
                //notify Users After Import
                
                System.out.println("Notify users after import of:" + reqId);
                errStr = Impex.notifyUsersAfterImport(reqId);
                System.out.println("After Impex.notifyUsersAfterImport errStr :" + errStr );
                
                // cleanup time!!!!!!******************
                impexObj.resetMe();
                impReq.resetMe();

                if (htData != null) {
                    for (Enumeration x = htData.elements(); x.hasMoreElements();) {
                        htData.remove(x.nextElement());
                    }
                }
                htData = null;
                impReq = null;
                impexObj = null;
                objMessage = null;
                // end of cleanup !!*********************

            } else {
                System.out
                        .println("Received a Message of wrong type: Expected ObjectMessage, Received "
                                + message.getClass().getName());
            }

        } catch (Exception e) { //catch (javax.jms.JMSException e) {
            System.out.println("Exception in Sponsor MDB Bean:  " + e);
            e.printStackTrace();
            
        }
    }

} // SponsorMDB
