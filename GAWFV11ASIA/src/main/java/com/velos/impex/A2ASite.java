/**
 * __astcwz_cmpnt_det: Author: Sonia Sahni Date and Time Created: Tue Feb 10
 * 13:44:16 IST 2004 Note: DO NOT DELETE OR MODIFY KONESA CODEWIZARD COMMENTS
 * STARTING WITH ///__astcwz_ and /**__astcwz_
 */
// /__astcwz_model_idt#(0)
// /__astcwz_maxim_idt#(2102)
// /__astcwz_packg_idt#(1046,1047,1048~,,)
package com.velos.impex;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.velos.eres.business.common.CommonDAO;

// /__astcwz_class_idt#(2074!n)
public class A2ASite {
    // /__astcwz_attrb_idt#(2083)
    private int id;

    // /__astcwz_attrb_idt#(2084)
    private String name;

    // /__astcwz_attrb_idt#(2085)
    private String code;

    // /__astcwz_attrb_idt#(2094)
    private FTPInfo ftpInfo;

    // /__astcwz_attrb_idt#(2099)
    private String siteAtSponsor;

    // /__astcwz_attrb_idt#(2100)
    private String siteAtSite;

    // /__astcwz_attrb_idt#(2101)
    private String sitePI;

    // /__astcwz_attrb_idt#(2102)
    private String siteEmail;

    // /__astcwz_default_constructor
    public A2ASite() {
    }

    // /__astcwz_opern_idt#()
    public int getId() {
        return id;
    }

    // /__astcwz_opern_idt#()
    public String getName() {
        return name;
    }

    // /__astcwz_opern_idt#()
    public String getCode() {
        return code;
    }

    // /__astcwz_opern_idt#()
    public FTPInfo getFtpInfo() {
        return ftpInfo;
    }

    // /__astcwz_opern_idt#()
    public String getSiteAtSponsor() {
        return siteAtSponsor;
    }

    // /__astcwz_opern_idt#()
    public String getSiteAtSite() {
        return siteAtSite;
    }

    // /__astcwz_opern_idt#()
    public String getSitePI() {
        return sitePI;
    }

    // /__astcwz_opern_idt#()
    public String getSiteEmail() {
        return siteEmail;
    }

    // /__astcwz_opern_idt#()
    public void setId(int aid) {
        id = aid;
    }

    // /__astcwz_opern_idt#()
    public void setName(String aname) {
        name = aname;
    }

    // /__astcwz_opern_idt#()
    public void setCode(String acode) {
        code = acode;
    }

    // /__astcwz_opern_idt#()
    public void setFtpInfo(FTPInfo aftpInfo) {
        ftpInfo = aftpInfo;
    }

    // /__astcwz_opern_idt#()
    public void setSiteAtSponsor(String asiteAtSponsor) {
        siteAtSponsor = asiteAtSponsor;
    }

    // /__astcwz_opern_idt#()
    public void setSiteAtSite(String asiteAtSite) {
        siteAtSite = asiteAtSite;
    }

    // /__astcwz_opern_idt#()
    public void setSitePI(String asitePI) {
        sitePI = asitePI;
    }

    // /__astcwz_opern_idt#()
    public void setSiteEmail(String asiteEmail) {
        siteEmail = asiteEmail;
    }

    // get information about sites participating in export

    public static ArrayList getA2ASites() {

        ArrayList arSites = new ArrayList();

        String siteSQL = "Select PK_A2ASITES , A2A_SITENAME, A2A_SITECODE , A2A_FTPIP , A2A_FTPUSER , A2A_FTPPASS , A2A_FTPFOLDER , A2A_FKSITE_SPONSOR , A2A_FKSITE_SITE , A2A_PI_NAME , A2A_PI_EMAIL from  er_a2asites ";

        String siteName;
        String siteCode;
        String siteFTPIP;
        String siteFTPUser;
        String siteFTPPass;
        String siteFTPFolder;
        int siteAtSponsor;
        int siteAtSite;
        String piName;
        String piEmail;
        int id;

        PreparedStatement pstmt = null;
        Connection conn = null;

        try {
            // conn = com.velos.eres.service.util.EJBUtil.getConnection();
            CommonDAO cDao = new CommonDAO();
            conn = CommonDAO.getConnection();

            pstmt = conn.prepareStatement(siteSQL);

            ResultSet rs = pstmt.executeQuery();

            DBLogger.log2DB("debug", "MSG", "IMPEX --> in A2ASite.getA2ASites");

            while (rs.next()) {

                id = rs.getInt("PK_A2ASITES");
                siteName = rs.getString("A2A_SITENAME");
                siteCode = rs.getString("A2A_SITECODE");
                siteFTPIP = rs.getString("A2A_FTPIP");
                siteFTPUser = rs.getString("A2A_FTPUSER");
                siteFTPPass = rs.getString("A2A_FTPPASS");
                siteFTPFolder = rs.getString("A2A_FTPFOLDER");
                siteAtSponsor = rs.getInt("A2A_FKSITE_SPONSOR");
                siteAtSite = rs.getInt("A2A_FKSITE_SITE");
                piName = rs.getString("A2A_PI_NAME");
                piEmail = rs.getString("A2A_PI_EMAIL");

                A2ASite site = new A2ASite();

                FTPInfo ftp = new FTPInfo();

                site.setId(id);
                site.setName(siteName);
                site.setCode(siteCode);
                ftp.setFtpFolder(siteFTPFolder);
                ftp.setFtpIp(siteFTPIP);
                ftp.setFtpPass(siteFTPPass);
                ftp.setFtpUser(siteFTPUser);

                site.setFtpInfo(ftp);
                site.setSiteAtSponsor(String.valueOf(siteAtSponsor));

                site.setSiteAtSite(String.valueOf(siteAtSite));
                site.setSitePI(piName);

                site.setSiteEmail(piEmail);

                arSites.add(site);

                DBLogger.log2DB("debug", "MSG",
                        "IMPEX --> in A2ASite.getA2ASites");

            }

            return arSites;

        } catch (Exception ex) {
            DBLogger.log2DB("fatal", "Exception",
                    "IMPEX --> Exception in A2ASite.getA2ASites "
                            + ex.toString());
            return arSites;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

        // end of method
    }

    public void getA2ASiteDetails() {

        if (id <= 0) {
            return;
        }

        String siteSQL = "Select PK_A2ASITES , A2A_SITENAME, A2A_SITECODE , A2A_FTPIP , A2A_FTPUSER , A2A_FTPPASS , A2A_FTPFOLDER , A2A_FKSITE_SPONSOR , A2A_FKSITE_SITE , A2A_PI_NAME , A2A_PI_EMAIL from  er_a2asites Where PK_A2ASITES = ? ";

        String siteName;
        String siteCode;
        String siteFTPIP;
        String siteFTPUser;
        String siteFTPPass;
        String siteFTPFolder;
        int siteAtSponsor;
        int siteAtSite;
        String piName;
        String piEmail;
        int id;

        PreparedStatement pstmt = null;
        Connection conn = null;

        try {
            // conn = com.velos.eres.service.util.EJBUtil.getConnection();

            CommonDAO cDao = new CommonDAO();
            conn = CommonDAO.getConnection();

            pstmt = conn.prepareStatement(siteSQL);

            pstmt.setInt(1, this.id);

            ResultSet rs = pstmt.executeQuery();

            DBLogger.log2DB("debug", "MSG",
                    "IMPEX --> in A2ASite.getA2ASiteDetails");

            while (rs.next()) {

                id = rs.getInt("PK_A2ASITES");
                siteName = rs.getString("A2A_SITENAME");
                siteCode = rs.getString("A2A_SITECODE");
                siteFTPIP = rs.getString("A2A_FTPIP");
                siteFTPUser = rs.getString("A2A_FTPUSER");
                siteFTPPass = rs.getString("A2A_FTPPASS");
                siteFTPFolder = rs.getString("A2A_FTPFOLDER");
                siteAtSponsor = rs.getInt("A2A_FKSITE_SPONSOR");
                siteAtSite = rs.getInt("A2A_FKSITE_SITE");
                piName = rs.getString("A2A_PI_NAME");
                piEmail = rs.getString("A2A_PI_EMAIL");

                FTPInfo ftp = new FTPInfo();

                setId(id);
                setName(siteName);
                setCode(siteCode);
                ftp.setFtpFolder(siteFTPFolder);
                ftp.setFtpIp(siteFTPIP);
                ftp.setFtpPass(siteFTPPass);
                ftp.setFtpUser(siteFTPUser);

                setFtpInfo(ftp);
                setSiteAtSponsor(String.valueOf(siteAtSponsor));

                setSiteAtSite(String.valueOf(siteAtSite));
                setSitePI(piName);

                setSiteEmail(piEmail);

                DBLogger.log2DB("debug", "MSG",
                        "IMPEX --> in A2ASite.getA2ASiteDetails");

            }

        } catch (Exception ex) {
            DBLogger.log2DB("fatal", "Exception",
                    "IMPEX --> Exception in A2ASite.getA2ASiteDetails "
                            + ex.toString());
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

        // end of method
    }

}
