/**
 * __astcwz_cmpnt_det: Author: Sonia Sahni Date and Time Created: Thu Jan 29
 * 11:43:00 IST 2004 Note: DO NOT DELETE OR MODIFY KONESA CODEWIZARD COMMENTS
 * STARTING WITH ///__astcwz_ and /**__astcwz_
 */
// /__astcwz_model_idt#(0)
// /__astcwz_maxim_idt#(2031)
// /__astcwz_packg_idt#(1046,1047,1048~,,)
package com.velos.impex;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import oracle.jdbc.OracleTypes;

import com.velos.eres.business.common.CommonDAO;

// /__astcwz_class_idt#(2016!n)
public class ModuleProcedure implements java.io.Serializable {
    // /__astcwz_attrb_idt#(2017)
    private String procName;

    // /__astcwz_attrb_idt#(2018)
    private String procDbUser;

    // /__astcwz_attrb_idt#(2019)
    private int procSequence;

    // /__astcwz_attrb_idt#(2020)
    private String procType;

    // /__astcwz_asscn_idt#(2028|2029%:ASSOCIATION^COMPOSITION)
    private ProcedureParam procedureParam;

    // /__astcwz_default_constructor
    public ModuleProcedure() {
    }

    // /__astcwz_opern_idt#()
    public String getProcName() {
        return procName;
    }

    // /__astcwz_opern_idt#()
    public String getProcDbUser() {
        return procDbUser;
    }

    // /__astcwz_opern_idt#()
    public int getProcSequence() {
        return procSequence;
    }

    // /__astcwz_opern_idt#()
    public String getProcType() {
        return procType;
    }

    // /__astcwz_opern_idt#()
    public void setProcName(String aprocName) {
        procName = aprocName;
    }

    // /__astcwz_opern_idt#()
    public void setProcDbUser(String aprocDbUser) {
        procDbUser = aprocDbUser;
    }

    // /__astcwz_opern_idt#()
    public void setProcSequence(int aprocSequence) {
        procSequence = aprocSequence;
    }

    // /__astcwz_opern_idt#()
    public void setProcType(String aprocType) {
        procType = aprocType;
    }

    public ProcedureParam getProcedureParam() {
        return this.procedureParam;
    }

    public void setProcedureParam(ProcedureParam procedureParam) {
        this.procedureParam = procedureParam;
    }

    public String getSQLSyntax() {
        /**
         * Returns the SQL for executing the stored procedure
         */
        StringBuffer sbSQL = new StringBuffer();
        int paramCount = 0;

        sbSQL.append("{call ");
        sbSQL.append(getProcName());

        paramCount = procedureParam.getInParamCount()
                + procedureParam.getOutParamCount();

        if (paramCount > 0) {
            sbSQL.append("( ? ");
            for (int ct = 1; ct < paramCount; ct++) {
                sbSQL.append(", ? ");
            }
            sbSQL.append(")} ");

        }

        return sbSQL.toString();
    }

    public ArrayList executeProcedure(ArrayList inParam) {
        /* returns an ArrayList of ModuleData objects */

        CallableStatement cstmt = null;
        Connection conn = null;
        ArrayList arResults = new ArrayList();
        int paramCount = 0;
        int inParamCount = 0;
        int outParamCount = 0;

        String procSQL;
        int givenInParamCount = 0;

        String paramDataType;
        int intOUT = 0;

        // get connection for the db user type
        try {
            givenInParamCount = inParam.size();

            CommonDAO cDao = new CommonDAO();

            if (procDbUser.equalsIgnoreCase("SCH")) {
                // conn = com.velos.esch.service.util.EJBUtil.getConnection();
                conn = cDao.getSchConnection();
            } else {
                // conn = com.velos.eres.service.util.EJBUtil.getConnection();

                DBLogger
                        .log2DB("debug", "MSG",
                                "IMPEX --> Getting connection from CommonDao... not using pool@@%%^^: ");
                conn = cDao.getConnection();
            }

            // DBLogger.log2DB ("debug", "MSG", "IMPEX --> Got procedure user: "
            // + procDbUser);

            procSQL = getSQLSyntax();

            DBLogger.log2DB("debug", "MSG",
                    "IMPEX --> Got procedure SQL syntax: " + procSQL);

            cstmt = conn.prepareCall(procSQL);

            paramCount = procedureParam.getInParamCount()
                    + procedureParam.getOutParamCount();

            inParamCount = procedureParam.getInParamCount();
            outParamCount = procedureParam.getOutParamCount();

            if (paramCount > 0) {

                // set input parameters

                for (int ct = 0; ct < inParamCount; ct++) {
                    String inParamStr = null;

                    if (ct < givenInParamCount) {
                        inParamStr = (String) inParam.get(ct);
                    }

                    // right now handles String type input parameters
                    cstmt.setString(ct + 1, inParamStr);

                }

                // set Output Parameters

                for (int ct = inParamCount; ct < paramCount; ct++) {
                    paramDataType = procedureParam.getParamDataType(ct);

                    if (paramDataType.equalsIgnoreCase("cursor")) {
                        cstmt.registerOutParameter(ct + 1, OracleTypes.CURSOR);
                    } else if (paramDataType.equalsIgnoreCase("integer")) {
                        cstmt.registerOutParameter(ct + 1, OracleTypes.INTEGER);
                    }

                    else {
                        cstmt.registerOutParameter(ct + 1,
                                java.sql.Types.VARCHAR);

                    }
                }
            }

            // execute the procedure after setting the input and output
            // parameters
            cstmt.execute();

            DBLogger.log2DB("debug", "MSG", "IMPEX --> procedure executed : "
                    + procName);

            // if there are output parameters, read the parameters

            for (int ct = inParamCount; ct < paramCount; ct++) {
                paramDataType = procedureParam.getParamDataType(ct);

                DBLogger.log2DB("debug", "MSG",
                        "IMPEX --> procedure executed : " + procName
                                + " Reading Procedure Output Parameter : " + ct
                                + " :" + paramDataType);

                if (paramDataType.equalsIgnoreCase("cursor")) {
                    ResultSet rsOUT = (ResultSet) cstmt.getObject(ct + 1);
                    // process this resultset and prepare ModuleData

                    ModuleData mData = new ModuleData();
                    mData = ModuleData.getModuleDataObject(rsOUT, procDbUser);

                    DBLogger.log2DB("debug", "MSG",
                            "IMPEX --> procedure executed : " + procName
                                    + " Got Procedure Output Parameter : " + ct
                                    + " :" + paramDataType);

                    arResults.add(mData);

                } else if (paramDataType.equalsIgnoreCase("integer")) {
                    intOUT = cstmt.getInt(ct + 1);
                    Integer integerOUT = new Integer(intOUT);
                    arResults.add(integerOUT);

                }

                else {
                    String strOUT = cstmt.getString(ct + 1);
                    arResults.add(strOUT);

                }

            }

            // 
            DBLogger.log2DB("debug", "MSG", "IMPEX --> procedure executed : "
                    + procName + " Got arResults.size" + arResults.size());
            return arResults;

        }// end of try
        catch (Exception ex) {
            DBLogger.log2DB("fatal", "Exception",
                    "IMPEX --> exception in ModuleProcedure.executeProcedure : "
                            + ex.toString());
            return arResults;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }

    // end of class

    public static ArrayList getModuleProcedures(String module,
            String procedureType) {
        /**
         * The method gets List of ModuleProcedure Objects for the given module.
         * Parameters - module : moduleSubType, a given code to the module
         * procedureType : Type of procedure(s) required. Possible values - I -
         * Import; E - Export Returns - ArrayList - The ModuleProcedure objects
         * are returned in an ArrayList
         */

        DBLogger.log2DB("debug", "MSG",
                "IMPEX --> Getting Procedure Information", module);

        PreparedStatement pstmtModuleProc = null;
        PreparedStatement pstmtProcParam = null;

        Connection conn = null;
        String procName;
        String procDBUser;
        int procSequence = 0;

        String procParamType;
        String procParamDataType;
        Integer procParamSequence;
        int inParamCount = 0;
        int outParamCount = 0;

        int procId = 0;

        ArrayList alModuleProcs = new ArrayList();

        // for list of procedures

        StringBuffer sbModuleProcSQL = new StringBuffer();
        StringBuffer sbProcParamSQL = new StringBuffer();

        sbModuleProcSQL
                .append("Select pk_moduleproc, proc_name,proc_user,proc_sequence from er_moduleproc, er_modules ");
        sbModuleProcSQL
                .append(" where module_subtype = ? and	fk_module = pk_module and");
        sbModuleProcSQL.append(" proc_type = ? order by proc_sequence ");

        // for list of procedure parameters

        sbProcParamSQL
                .append(" Select procparam_datatype, procparam_type, procparam_seq from er_procparam ");
        sbProcParamSQL
                .append(" Where fk_moduleproc = ? order by procparam_seq ");

        try {
            // conn = com.velos.eres.service.util.EJBUtil.getConnection();

            CommonDAO cDao = new CommonDAO();
            conn = cDao.getConnection();

            pstmtModuleProc = conn.prepareStatement(sbModuleProcSQL.toString());

            pstmtModuleProc.setString(1, module);
            pstmtModuleProc.setString(2, procedureType);

            ResultSet rsProc = pstmtModuleProc.executeQuery();

            while (rsProc.next()) {

                ModuleProcedure mProcedure = new ModuleProcedure();

                procId = rsProc.getInt("pk_moduleproc");
                procName = rsProc.getString("proc_name");
                procDBUser = rsProc.getString("proc_user");
                procSequence = rsProc.getInt("proc_sequence");

                DBLogger.log2DB("debug", "MSG",
                        "IMPEX --> Got Procedure Information: " + procName,
                        module);

                mProcedure.setProcName(procName);
                mProcedure.setProcDbUser(procDBUser);
                mProcedure.setProcSequence(procSequence);
                mProcedure.setProcType(procedureType);

                // get procedure's parameters

                pstmtProcParam = conn.prepareStatement(sbProcParamSQL
                        .toString());
                pstmtProcParam.setInt(1, procId);

                ResultSet rsProcParam = pstmtProcParam.executeQuery();
                ProcedureParam pParam = new ProcedureParam();

                while (rsProcParam.next()) {

                    procParamDataType = rsProcParam
                            .getString("procparam_datatype");
                    procParamType = rsProcParam.getString("procparam_type");
                    procParamSequence = new Integer(rsProcParam
                            .getInt("procparam_seq"));

                    pParam.setParamDataType(procParamDataType);
                    pParam.setParamType(procParamType);
                    pParam.setParamSequence(procParamSequence);

                    if (procParamType.equalsIgnoreCase("IN")) {
                        inParamCount++;
                    } else {
                        outParamCount++;
                    }

                }

                pParam.setOutParamCount(outParamCount);
                pParam.setInParamCount(inParamCount);

                DBLogger.log2DB("debug", "MSG",
                        "IMPEX --> Got Procedure Information for: " + procName
                                + "IN Parameters:" + inParamCount
                                + ", OUT Parameters:" + outParamCount, module);

                mProcedure.setProcedureParam(pParam);
                alModuleProcs.add(mProcedure);

            }
            return alModuleProcs;

        } catch (Exception ex) {
            // Debug.println("EXCEPTION IN FETCHING FROM SITE TABLE " + ex);
            DBLogger.log2DB("fatal", "Exception", "Exception in IMPEX --> "
                    + ex.toString(), module);
            return alModuleProcs;
        } finally {
            try {
                if (pstmtModuleProc != null)
                    pstmtModuleProc.close();
                if (pstmtProcParam != null)
                    pstmtProcParam.close();

            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

        // end of Method

    }

}
