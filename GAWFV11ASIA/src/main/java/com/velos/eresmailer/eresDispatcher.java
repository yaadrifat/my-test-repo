package com.velos.eresmailer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;

import javax.swing.Timer;

import oracle.jdbc.OracleResultSet;
import oracle.sql.CLOB;

import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.service.util.VMailMessage;
import com.velos.eres.service.util.VMailer;
import com.velos.impex.Impex;
import com.velos.impex.dispatcher.ReverseA2ADispatcher;

public class eresDispatcher {
    static dispatcherInvoker invoker =null;
    public static void main(String arg[]) { 
        invoker = new dispatcherInvoker();
        /*try {
            while (System.in.read() != '\n')
                ;
        } catch (IOException ioe) { 
            System.out.println(ioe);
            ioe.printStackTrace();

        }*/
    }
    public static void startDispatcher() {
        invoker = new dispatcherInvoker();

    }
    public static void stopDispatcher(){
        invoker.StopTimer();
    }
    
// Added by gopu for fix the Bugzilla Issue #2095
	public boolean sentMail(String mailedTo, String subject,
            String mailText, String userEmail){
		dispatcherInvoker invoker = new dispatcherInvoker(); 
		vcDispatcher vcDis = new vcDispatcher();

		vcDis.readIni();
        vcDis.InitDB(); 
		boolean mailSent = vcDis.SendMail(mailedTo," "," ",subject, mailText,userEmail,"RC");			
		return mailSent;
	}
} 

class dispatcherInvoker {
    int delay = 60000; // 60 seconds
    
    int delayRAL = 600000; // 10 minutes

    int count = 0;

    vcDispatcher dispatcher = new vcDispatcher();
//APR-04-2011,MSG-TEMPLAGE FOR USER INFO,TIMER TASK INTRODUCED V-CTMS-1
    //Timer ti = new Timer(delay, new DAL());
    
    Timer RevTi = new Timer(delayRAL, new RAL());

    dispatcherInvoker() {
       // System.out.println("Invoker Activated");
       // ti.start();
        RevTi.start();    
    }

    public void StopTimer() {
       // ti.stop();
    }

    class DAL implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            System.out.println("occurs every 60 secs....!!");
            dispatcher.InitDB();
            //System.out.println("after init!!");
            /* Place the command here */
            dispatcher.FetchActions();
            //System.out.println("after fetch!!");
            dispatcher.CloseDB();
            //System.out.println("after close!!");
        }
    }
    
    // Class Reverse A2A Action Listner
    
    class RAL implements ActionListener {
        public void actionPerformed(ActionEvent e) {
        	int stat = 0;
        	String statDesc = "";
        	String mailStatus = "";
        	
        	try
        	{
        	
	            System.out.println("Reverse A2A Action Listner ---->activates every 10 Minutes!!");
	            System.out.println("Reverse A2A Action Listner ----> ReverseA2ADispatcher.instanceCount : " + ReverseA2ADispatcher.getInstanceCount() );
	            
	            stat = Impex.f_get_ReverseA2AStatus();
	            
	            if (stat == 1)
	            {
	            	statDesc = "Either Reverse A2A is running or not configured in this system";
	            	
	            }
	            if (stat == 0)
	            {
	            	statDesc = "Reverse A2A is not running. Sending email to Velos Admin";
	            	
	            }
	            if (stat < 0)
	            {
	            	statDesc = "Could not get Reverse A2A Status";
	            	
	            }
	            
	           // System.out.println("Reverse A2A Action Listner ----> :" + statDesc);
	            
	            if (stat == 0)
	            {
	            	System.out.println("Reverse A2A Action Listner ----> : Stopping reverse A2A Service");
	            	try{
	            		
	            		ReverseA2ADispatcher.stopDispatcher();
	            	}
	            	catch (Exception ex)
	            	{
	            		
	            		System.out.println("Reverse A2A Action Listner ----> : Stopping reverse A2A Service : " + ex + ", Possibly Reverse A2A was not running");
	            	}
	            	 System.out.println("Reverse A2A Action Listner ----> : Starting reverse A2A Service");
	            	 ReverseA2ADispatcher.startDispatcher();
	            	 mailStatus = Impex.sendA2AstatusEmail();
	            	 
	            	 if (StringUtil.isEmpty(mailStatus ))
	            	 {
	            		 mailStatus = "";
	            	 }
	            	 System.out.println("Reverse A2A Action Listner ----> : Sent reverse A2A Status email (if configured)" + mailStatus );
	            	 
	            	
	            }
	            
	         System.out.println("Reverse A2A Action Listner ----> ReverseA2ADispatcher.instanceCount : " + ReverseA2ADispatcher.getInstanceCount() );
        } catch (Exception ex)//end of try
        {
        	System.out.println("Reverse A2A Action Listner ----> : Exception " + ex);
        }
            
       }
    }
    
    /////////////Reverse A2A action
}

