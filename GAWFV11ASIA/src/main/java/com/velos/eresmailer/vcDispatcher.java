package com.velos.eresmailer;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;

import oracle.jdbc.OracleResultSet;
import oracle.sql.CLOB;

import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.service.util.VMailMessage;
import com.velos.eres.service.util.VMailer;
//THIS CLASS IS SEPARATED FROM eresDispatcher.java
class vcDispatcher {
    String htmlPath;

    String jdbcDriver;

    String jdbcUrl;

    String emailInTable;

    String dispatchTable;

    String rulesTable;

    String loginID;

    String loginPassword;

    String allowedString;

    String smtpServer;
    
    int maxAttempts;

    String flagMode; // used to override doGet for which screen is displayed

    String supportEmail; // email of cusomer support

    Connection dbCon;// each object should use this connection

    vcDispatcher() {
        readIni();
        // InitDB();
    }

    vcDispatcher(String server) {
        smtpServer = server;
    }

    vcDispatcher(String driver, String url, String id, String password,
            String server, String table, String rules) {
        jdbcDriver = driver;
        jdbcUrl = url;
        loginID = id;
        loginPassword = password;
        smtpServer = server;
        emailInTable = table;
        rulesTable = rules;
        InitDB();
    }

    public void InitDB() {
        try {

            // DriverManager.registerDriver(new
            // oracle.jdbc.driver.OracleDriver());
            //System.out.println("INIT DB ----> trying to get connection");
			
            Class.forName(jdbcDriver);

            dbCon = DriverManager
                    .getConnection(jdbcUrl, loginID, loginPassword);

            // Class.forName(jdbcDriver);
            // dbCon
            // =DriverManager.getConnection(jdbcUrl,loginID,loginPassword);
        } catch (Exception e) {
            System.out.println("InitDB" + e); 
        }
    }

    public void CloseDB() {
        try {
            dbCon.close();
        } catch (SQLException e) {
            System.out.println("CloseDB:" + e);
        }
    }

    public Connection GetDBConnection() {
        return dbCon;
    }

    public void editActionStatus(int mail_id) {
        String query = "update sch_dispatchmsg set MSG_STATUS = 1 where PK_MSG = ";
        query += mail_id;
        //System.out.println(query);
        try {
            Statement s = dbCon.createStatement();
            s.executeUpdate(query);
        } catch (SQLException e1) {
            System.out.println(e1);
        }
    }
    
    public void updateSendAttempt(int mail_id) {
        
    	String selectQuery="select msg_attempts from sch_dispatchmsg where pk_msg=?";
    	String query = "";
    	int localAttempts=0;
    	ResultSet rs=null;
        
        //System.out.println(query);
        try {
            PreparedStatement s = dbCon.prepareStatement(selectQuery);
            s.setInt(1, mail_id);
            rs=s.executeQuery();
            while (rs.next())
            {
            	localAttempts=rs.getInt("msg_attempts");
            	
            }
           if (localAttempts<maxAttempts)
           {
        	   s.clearParameters();
        	   s.close();
        	   query="update sch_dispatchmsg set MSG_ATTEMPTS =(?)  where PK_MSG =?"; 
        	   s = dbCon.prepareStatement(query);
        	   s.setInt(1, localAttempts+1);
               s.setInt(2, mail_id);
               s.executeUpdate();
           }
           else
           {
        	   s.clearParameters();
        	   query="update sch_dispatchmsg set MSG_ATTEMPTS =(?),msg_status='-9'  where PK_MSG =?"; 
        	   s = dbCon.prepareStatement(query);
        	   s.setInt(1, (localAttempts+1));
               s.setInt(2, mail_id);
               
               s.executeUpdate();
        	   
           }
           dbCon.commit();
        } catch (SQLException e1) {
            System.out.println(e1.getMessage());
            e1.printStackTrace();
        }
    }
    
    

    public boolean SendMail(String to_addresses, String cc_addresses,
            String bcc_addresses, String subject, String message,
            String from_address, String msg_type) {
        boolean status = true;
        String mailTitle = "";
        String mailStatusString = "";
        //System.out.println("sendmail:" + smtpServer + " " + to_addresses + " "      + subject);

        try {
            VMailer vm = new VMailer();
            VMailMessage msgObj = new VMailMessage();

            if (msg_type.equals("R") || msg_type.equals("C")) {
                mailTitle = "Velos eResearch Notification Center";
            } else if(msg_type.equals("RC")){
				mailTitle = " Velos eResearch Notification";	// for Bugzilla issue #2095 by gopu
			}else if(msg_type.equals("UN") || msg_type.equals("UR")){
				mailTitle = " Velos eResearch Customer Services";	
			}else {
                mailTitle = "Velos eResearch Event Notification Center";  
            }

            // for Bugzilla issue #2095 by gopu
			
            /*String sql =" select add_email from er_user,er_add where pk_add = fk_peradd and usr_stat in ('D') and instr('" + to_addresses +"',add_email) > 0";  
					
			Statement s=dbCon.createStatement();
			ResultSet rs = s.executeQuery(sql);			
			 if(rs != null){
			 while (rs.next()) {
                if (rs.getString("add_email") != null) {
                   String blockedUser = rs.getString("add_email");
					to_addresses = to_addresses.replace(blockedUser," ");
				}
			 }
			 }   
			 rs.close(); */
			
            msgObj.setMessageFrom(from_address);
            msgObj.setMessageFromDescription(mailTitle);
            msgObj.setMessageTo(to_addresses);
            msgObj.setMessageSubject(subject);
            msgObj.setMessageSentDate(new java.util.Date());
            msgObj.setMessageText(message);

            vm.setVMailMessage(msgObj);
            mailStatusString = vm.sendMail();
            if (!StringUtil.isEmpty(mailStatusString)) {
                return false;
            } else {
                //System.out.println("Mail sent...");
                return true;
            }

            // old code as on 03/24/05
            /*
             * Properties props = new Properties(); Session sendMailSession;
             * Store store; Transport transport;
             * 
             * sendMailSession = Session.getInstance(props, null);
             * props.put("mail.smtp.host", smtpServer); Message newMessage = new
             * MimeMessage(sendMailSession); newMessage.setFrom(new
             * InternetAddress(from_address,mailTitle));
             * newMessage.setRecipients(Message.RecipientType.TO,
             * InternetAddress.parse(to_addresses));
             * newMessage.setSubject(subject); newMessage.setSentDate(new
             * java.util.Date()); newMessage.setText(message); transport =
             * sendMailSession.getTransport("smtp"); transport.send(newMessage);
             */

        } catch (Exception e) {
            status = false;
            System.out.println("SendMail Mailing error:" + e);
        }
        /*
         * catch(MessagingException m) { status=false;
         * System.out.println(m.toString()); }
         */

        /*
         * PrintStream out1; SmtpClient send;
         * System.out.println("sendmail:"+smtpServer+" "+ to_addresses + " " +
         * subject); try{ send = new SmtpClient(smtpServer);
         * send.from(from_address); send.to(to_addresses);
         * //send.cc(cc_addresses); //send.bcc(bcc_addresses); out1 =
         * send.startMessage( ); // out1.println("From : " + from_address);
         * out1.println("To : " + to_addresses); out1.println("Cc : " +
         * cc_addresses); out1.println("Bcc : " + bcc_addresses);
         * out1.println("Subject : " + subject); out1.println();
         * out1.println(message); out1.println(new java.util.Date());
         * out1.flush(); out1.close(); send.closeServer(); } catch(Exception e){
         * status=false; System.out.println("mailing error"); }
         */
        return status;
    }

    public void FetchActions() {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        java.util.Date today = new java.util.Date();
        String temp1, temp2;
        String address = "";

        //System.out.println("Fetch Actions ----> started");


		// Modified by gopu for Bugzilla issue #2095 
        String query = "select nvl(MSG_SUBJECT,' ') MSG_SUBJECT ,PK_MSG, MSG_SENDON, MSG_STATUS, MSG_TYPE, FK_EVENT, MSG_TEXT, a.ADD_EMAIL PERSON_EMAIL, 'person code', fk_pat, FK_SCHEVENT , DESCRIPTION, START_DATE_TIME from sch_dispatchmsg  , sch_events1, er_add  a, er_user b   where  MSG_STATUS = 0  and sch_dispatchmsg.msg_sendon <= pkg_tz.f_db2gmt(sch_dispatchmsg.fk_pat, trim(sch_dispatchmsg.msg_type))  AND msg_type = 'U'  and  FK_SCHEVENT = event_id and a.PK_ADD = b.FK_PERADD  and b.PK_USER =  fk_pat  and sch_dispatchmsg.fk_study is not null and b.usr_stat not in ('D') union all   select  nvl(MSG_SUBJECT,' ') MSG_SUBJECT,PK_MSG, MSG_SENDON, MSG_STATUS, MSG_TYPE, FK_EVENT, MSG_TEXT, PERSON_EMAIL, PERSON_CODE, FK_PAT, FK_SCHEVENT ,DESCRIPTION, START_DATE_TIME  from  sch_dispatchmsg , person  , sch_events1 ,er_user b where b.pk_user = fk_pat and b.usr_stat not in ('D') and MSG_STATUS = 0 and sch_dispatchmsg.msg_sendon <= pkg_tz.f_db2gmt(sch_dispatchmsg.fk_pat, trim(sch_dispatchmsg.msg_type))  AND msg_type = 'P' and  fk_pat = pk_person and  FK_SCHEVENT = event_id and sch_dispatchmsg.fk_study is not null";
	

        String notQuery = " select  DISTINCT(PK_MSG), nvl(MSG_SUBJECT,' ') MSG_SUBJECT, MSG_SENDON, MSG_STATUS, MSG_TYPE, 	MSG_TEXT, "
                + "	ADD_EMAIL as PERSON_EMAIL, 	fk_pat, FK_SCHEVENT "
                + " from sch_dispatchmsg  , 	er_user, er_add "
                + " where  MSG_STATUS = 0  and trunc(sch_dispatchmsg.msg_sendon) <= pkg_tz.f_db2gmt(sch_dispatchmsg.fk_pat, trim(sch_dispatchmsg.msg_type))   AND "
                + "  (msg_type = 'U' or msg_type = 'R' or msg_type = 'UN' or msg_type = 'UR')  and  fk_pat = pk_user and "
                + "pk_add = fk_peradd and	fk_study is null and usr_stat not in ('D')";


        String mobQuery = " select  PK_MSG, MSG_SENDON, MSG_STATUS, MSG_TYPE, MSG_TEXT, "
                + "ALNOT_MOBEVE as PERSON_EMAIL, 	fk_pat, FK_SCHEVENT "
                + " from sch_dispatchmsg  , SCH_ALERTNOTIFY "
                + " where  MSG_STATUS = 0  and trunc(sch_dispatchmsg.msg_sendon) <= trunc(sysdate)   AND "
                + " msg_type = 'M'  and  fk_pat = PK_ALNOT and "
                + "SCH_ALERTNOTIFY.fk_patprot = sch_dispatchmsg.fk_patprot and sch_dispatchmsg.fk_study is null";

        String clubQuery = " select  MSG_CLUBTEXT, PK_MSG, MSG_SENDON, MSG_STATUS, MSG_TYPE,  "
                + "ADD_EMAIL as PERSON_EMAIL, 	fk_pat, FK_SCHEVENT, to_char(MSG_SENDON,PKG_DATEUTIL.F_GET_DATEFORMAT) as dispdate  "
                + " from sch_dispatchmsg  , 	er_user, er_add "
                + " where  MSG_STATUS = 0  and sch_dispatchmsg.msg_sendon <= pkg_tz.f_db2gmt(sch_dispatchmsg.fk_pat, trim(sch_dispatchmsg.msg_type))   AND "
                + "  msg_type = 'UC'  and  fk_pat = pk_user and "
                + " pk_add = fk_peradd and	msg_isclubbed = 1 and usr_stat not in ('D')";

       String patClubQuery =" select  MSG_CLUBTEXT, PK_MSG, MSG_SENDON, MSG_STATUS, MSG_TYPE,  "
                + " PERSON_EMAIL as PERSON_EMAIL,fk_pat, FK_SCHEVENT, to_char(MSG_SENDON,PKG_DATEUTIL.F_GET_DATEFORMAT) as dispdate ,msg_subject,msg_text "
                + " from sch_dispatchmsg  ,person  where  MSG_STATUS = 0  and sch_dispatchmsg.msg_sendon <= pkg_tz.f_db2gmt(sch_dispatchmsg.fk_pat, trim(sch_dispatchmsg.msg_type)) AND  msg_type = 'PC'  and  fk_pat = pk_person   and msg_isclubbed = 1";
       ResultSet rs =null;
		ResultSet rsNot = null;
		ResultSet rsMob = null;
       ResultSet rsClub = null;
		ResultSet rsPatClub = null;
		int msgPk=0;
        try {


            Statement s = dbCon.createStatement();
            Statement notSt = dbCon.createStatement();
            Statement notMob = dbCon.createStatement();

            Statement clubSt = dbCon.createStatement();
            Statement patClubSt = dbCon.createStatement();


            s.executeUpdate("alter session set NLS_DATE_FORMAT = 'YYYY-MM-DD HH24:MI:SS'");
            notSt.executeUpdate("alter session set NLS_DATE_FORMAT = 'YYYY-MM-DD HH24:MI:SS'");
            notMob.executeUpdate("alter session set NLS_DATE_FORMAT = 'YYYY-MM-DD HH24:MI:SS'");
            clubSt.executeUpdate("alter session set NLS_DATE_FORMAT = 'YYYY-MM-DD HH24:MI:SS'");
            patClubSt.executeUpdate("alter session set NLS_DATE_FORMAT = 'YYYY-MM-DD HH24:MI:SS'");


             rs = s.executeQuery(query);
			 rsNot = notSt.executeQuery(notQuery);
			 rsMob = notMob.executeQuery(mobQuery);
             rsClub = clubSt.executeQuery(clubQuery);
			 rsPatClub = patClubSt.executeQuery(patClubQuery);

            String message = "";
            String subject = "";
            while (rs.next()) {
                if (rs.getString("MSG_TEXT") != null) {
                	
                	
                    message = rs.getString("MSG_TEXT");

                    temp1 = rs.getString("MSG_TYPE");

                    temp1 = temp1.trim();

                    subject = rs.getString("MSG_SUBJECT");

                    if (subject.trim().equals("")) {
                        subject = "Your Event Notification";
                    }
                    msgPk=rs.getInt("PK_MSG");
                    address = rs.getString("PERSON_EMAIL");
                    
                    /*
                     * if (temp1.equals("P")) { address =
                     * rs.getString("PERSON_EMAIL"); subject = "Your Event
                     * Notification"; } if (temp1.equals("U")) { address =
                     * rs.getString("PERSON_EMAIL"); subject = "Your Event
                     * Notification"; }
                     */

                    // System.out.println("PK_MSG* "+ rs.getString("PK_MSG") +
                    // "*PERSONEMAIL* " + address);
                    if ((address != null) && (!(address.trim().equals("")))) {

                        if (SendMail(address, "", "", subject, message,
                                supportEmail, temp1)) { 
                            editActionStatus(rs.getInt("PK_MSG"));
                        }else
                        {
                        	updateSendAttempt(msgPk);
                        }
                    }
                }
            }
            rs.close();

            // for notification mails

            while (rsNot.next()) {

                if (rsNot.getString("MSG_TEXT") != null) {
                	msgPk=rsNot.getInt("PK_MSG");
                    message = rsNot.getString("MSG_TEXT");
                    address = rsNot.getString("PERSON_EMAIL");

                    temp1 = rsNot.getString("MSG_TYPE");
                    temp1 = temp1.trim();

                    subject = rsNot.getString("MSG_SUBJECT");

                    if (subject.trim().equals("")) {
                        if (temp1.equals("R")) {
                            subject = "Your Milestone Report Notification";
                        }
                        else if (temp1.equals("U")) {
                            subject = "Your Event Notification";
                        }
                        else if (temp1.equals("UN")) {
                            subject = "Your New Velos eResearch Account";
                        }
                        else if (temp1.equals("UR")) {
                            subject = "Request for change of password/eSign";
                        }
                    }

                    if (SendMail(address, "", "", subject, message,
                            supportEmail, temp1)) {
                        editActionStatus(rsNot.getInt("PK_MSG"));
                }else
                {
                	updateSendAttempt(msgPk);
                }
                }
            }

            rsNot.close();
            // end of change

            while (rsMob.next()) {

            	
                if (rsMob.getString("MSG_TEXT") != null) {
                	msgPk=rsMob.getInt("PK_MSG");
                    message = rsMob.getString("MSG_TEXT");

                    address = rsMob.getString("PERSON_EMAIL");

                    if (SendMail(address, "", "", "", message, supportEmail,
                            "M")){
                        editActionStatus(rsMob.getInt("PK_MSG"));
                }else
                {
                	updateSendAttempt(msgPk);
                }
                }
            }

            rsMob.close();

            // for clubbed mails

            int chunk = 0;

            String reportContent = null;
            String dispDate = "";

            int pk = 0;

            while (rsClub.next()) {
                // System.out.println("got clubbed rows"+ rsClub.getRow());
                //System.out.println("message1 ");
            	
                CLOB rCLOB = ((OracleResultSet) rsClub).getCLOB(1);
                if (!(rCLOB == null)) {
                    reportContent = rCLOB.getSubString(1, (int) rCLOB.length());
                }
                // System.out.println("message2 " +reportContent);

                pk = rsClub.getInt("PK_MSG");
                msgPk=pk;
                address = rsClub.getString("PERSON_EMAIL");
                dispDate = rsClub.getString("dispdate");

                subject = "Your Notifications For Today, As Of " + dispDate;

                if (SendMail(address, "", "", subject, reportContent,
                        supportEmail, "C")){
                    editActionStatus(rsClub.getInt("PK_MSG"));
            }else
            {
            	updateSendAttempt(msgPk);
            }

            }

            rsClub.close();

            // for patient club mails

            chunk = 0;

            reportContent = null;
            dispDate = "";

            pk = 0;

            while (rsPatClub.next()) {

                // System.out.println("got clubbed rows"+ rsClub.getRow());
               // System.out.println("message1 ");

                 CLOB rCLOB = ((OracleResultSet) rsPatClub).getCLOB(1);
                if (!(rCLOB == null)) {
                    reportContent = rCLOB.getSubString(1, (int) rCLOB.length());
                }
                else
                {
                	reportContent = rsPatClub.getString("msg_text");
                	
                }
                // System.out.println("message2 " +reportContent);

                pk = rsPatClub.getInt("PK_MSG");
                msgPk=pk;
                address = rsPatClub.getString("PERSON_EMAIL");
                dispDate = rsPatClub.getString("dispdate");
                
                subject = rsPatClub.getString("msg_subject");
                
                if (StringUtil.isEmpty(subject))
                {
                	subject = "Your Notifications For Today, As Of " + dispDate;
                }	
                	
                System.out.println("address" + address);
                System.out.println("supportEmail" + supportEmail);
                System.out.println("reportContent new:" + reportContent);
                
                if (SendMail(address, "", "", subject, reportContent,
                        supportEmail, "C")){
                    editActionStatus(rsPatClub.getInt("PK_MSG"));
            }else
            {
            	updateSendAttempt(msgPk);
            }

            }

            rsPatClub.close();

            // end of club mails
        } catch (Exception e1) {
            System.out.println("FetchActions()" + e1);
        	updateSendAttempt(msgPk);
        
        }

    } 

    /*
     * The following functions are used to read values from the vcMailer.ini
     * file
     */

    public String getValue(String inString, String beginDelimiter,
            String endDelimiter) {
        int begin = inString.indexOf(beginDelimiter);
        if (begin<0) return "";
        int end = inString.indexOf(endDelimiter, begin);
        String valueString = inString.substring(
                begin + beginDelimiter.length(), end).trim();
        sysout(beginDelimiter + " = " + valueString);
        return valueString;
    }

    public void readIni() {
        try {
            String eHome = EJBUtil.getEnvVariable("ERES_HOME");
            eHome = (eHome == null) ? "" : eHome;
             if (eHome.trim().equals("%ERES_HOME%"))
                eHome = System.getProperty("ERES_HOME");
            eHome = eHome.trim();
            String  fileName = eHome + "eresMailer.ini";
            String iniString = getFileText(fileName);
            htmlPath = getValue(iniString, "<htmlPath>", "</htmlPath>");
            jdbcDriver = getValue(iniString, "<jdbcDriver>", "</jdbcDriver>");
            jdbcUrl = getValue(iniString, "<jdbcUrl>", "</jdbcUrl>");
            loginID = getValue(iniString, "<loginID>", "</loginID>");
            loginPassword = getValue(iniString, "<loginPassword>",
                    "</loginPassword>");
            emailInTable = getValue(iniString, "<emailInTable>",
                    "</emailInTable>");
            dispatchTable = getValue(iniString, "<dispatchTable>",
                    "</dispatchTable>");
            rulesTable = getValue(iniString, "<rulesTable>", "</rulesTable>");
            allowedString = getValue(iniString, "<allowedString>",
                    "</allowedString>");
            smtpServer = getValue(iniString, "<smtpServer>", "</smtpServer>");
            maxAttempts = EJBUtil.stringToNum(getValue(iniString, "<maxAttempts>", "</maxAttempts>"));
            if (maxAttempts==0)
            	maxAttempts=10;
            supportEmail = getValue(iniString, "<supportEmail>",
                    "</supportEmail>");
          //  System.out.println("read  INI ----> started");
        } catch (Exception e) {
            System.out.println("getValue" + e);
        }
    }

    public void sysout(String outString) {
        System.out.println("vcMailer: " + outString);
    }

    public String getFileText(String fileName) throws IOException {
        String htmlBuffer = "";
        try {
            BufferedReader in = new BufferedReader(new FileReader(fileName));
            String inputLine;

            while ((inputLine = in.readLine()) != null)
                htmlBuffer = htmlBuffer + inputLine;
            in.close();
        } catch (Exception e) {
            System.out.println("I/O error\n" + e);
        }
        return htmlBuffer;
    }

    public String getPatientAddress(int patientID) {
        String address = "";
        String query = "select PERSON_EMAIL  from person   where pk_person =  "
                + patientID;
        try {
            Statement s = dbCon.createStatement();
            ResultSet rs = s.executeQuery(query);
            //System.out.println(query);
            while (rs.next()) {
                address = rs.getString("PERSON_EMAIL");
            }

            rs.close();
           // System.out.println("patient address is : " + address);
            return address;
        }

        catch (SQLException e1) {
            System.out.println(e1);
        }
        return address;
    }
    
    

    public String getUserAddress(int eventID) {
        String address = "";

        String query = "select ADD_EMAIL  from er_add  where PK_ADD in (select FK_PERADD from er_user where PK_USER in (select EVENTUSR from sch_eventusr where FK_EVENT = "
                + eventID + "  and EVENTUSR_TYPE='S'))";
        try {
            Statement s = dbCon.createStatement();
            ResultSet rs = s.executeQuery(query);
            //System.out.println(query);

            while (rs.next()) {
                address += rs.getString("ADD_EMAIL");
                address += ",";

            }
            rs.close();

            return address;
        }

        catch (SQLException e1) {
            System.out.println("getUserAddress" + e1);
        }
        return address;

    }

}
