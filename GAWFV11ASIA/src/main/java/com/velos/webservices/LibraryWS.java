package com.velos.webservices;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;

import org.apache.log4j.Logger;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.client.LibraryClient;
import com.velos.services.model.EventSearch;
import com.velos.services.model.Events;
import com.velos.services.model.LibraryEvents;

/**
 * 
 * @author Tarandeep Singh Bali
 *
 */


@WebService(
		serviceName="LibraryService",
		endpointInterface="com.velos.webservices.LibrarySEI",
		targetNamespace="http://velos.com/services/")
public class LibraryWS implements LibrarySEI{
	
	private static Logger logger = Logger.getLogger(LibraryWS.class.getName());
	ResponseHolder response = new ResponseHolder();
	
	@Resource
	private WebServiceContext context;
	
	public LibraryWS(){
		
	}

	@Override
	public LibraryEvents searchLibraryEvents(EventSearch eventSearch, Integer pageNumber, Integer pageSize)
			throws OperationException {

		LibraryEvents libraryEvents = LibraryClient.searchLibraryEvents(eventSearch, pageNumber, pageSize);
		return libraryEvents;
	}
	


}
