package com.velos.webservices;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import org.apache.log4j.Logger;

import com.velos.services.AbstractService;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.client.PatientScheduleClient;
import com.velos.services.model.CalendarIdentifier;
import com.velos.services.model.EventIdentifier;
import com.velos.services.model.EventStatus;
import com.velos.services.model.EventStatusHistory;
import com.velos.services.model.MEventStatuses;
import com.velos.services.model.MPatientScheduleList;
import com.velos.services.model.MPatientSchedules;
import com.velos.services.model.MultipleEvents;
import com.velos.services.model.PatientProtocolIdentifier;
import com.velos.services.model.PatientSchedule;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientSchedules;
import com.velos.services.model.ScheduleEventStatuses;
import com.velos.services.model.SitesOfService;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.VisitIdentifier;
/**
 * WebService class for PatientSchedule
 * @author Virendra
 *
 */
@WebService(
		serviceName="PatientScheduleService",
		endpointInterface="com.velos.webservices.PatientScheduleSEI",
		targetNamespace="http://velos.com/services/")
		
public class PatientScheduleWS implements PatientScheduleSEI{
	
	private static Logger logger = Logger.getLogger(PatientScheduleWS.class.getName());
	ResponseHolder response = new ResponseHolder();
	
	@Resource
	private WebServiceContext context;  
	
	public PatientScheduleWS(){
	}
	/**
	 * Calls getPatientSchedule of Patient Schedule Client with PatientIdentifier
	 * and StudyIdentifier objects and returns List of PatientSchedule for a patient in a study
	 */

	
	public PatientSchedules getPatientScheduleList(PatientIdentifier patientId, StudyIdentifier studyIdentifier)
			throws OperationException {
		PatientSchedules lstPatSchedule = PatientScheduleClient.getPatientScheduleList(patientId, studyIdentifier);
		return lstPatSchedule;
	}
	
	public PatientSchedule getPatientSchedule(PatientProtocolIdentifier scheduleOID,VisitIdentifier visitIdentifier,String visitName)
			throws OperationException {
		PatientSchedule patientSchedule = PatientScheduleClient.getPatientSchedule(scheduleOID,visitIdentifier,visitName);
		return patientSchedule;
	}
	
	public PatientSchedule getPatientScheduleVisits(PatientProtocolIdentifier scheduleOID)
			throws OperationException {
		PatientSchedule patientSchedule = PatientScheduleClient.getPatientScheduleVisits(scheduleOID);
		return patientSchedule;
	}
	
	public PatientSchedule getCurrentPatientSchedule(PatientIdentifier patientId, StudyIdentifier studyIdentifier,Date startDate,Date endDate)
			throws OperationException {
		PatientSchedule currentPatientSchedule = PatientScheduleClient.getCurrentPatientSchedule(patientId, studyIdentifier, startDate, endDate);
		return currentPatientSchedule;
	}
	
	public ResponseHolder addScheduleEventStatus(EventIdentifier eventIdentifier, EventStatus eventStatus)
			throws OperationException {
		ResponseHolder response = PatientScheduleClient.addScheduleEventStatus(eventIdentifier, eventStatus);
		return response;
	}
	
	public SitesOfService getSitesOfService()
			throws OperationException {
		SitesOfService sitesOfService = PatientScheduleClient.getSitesOfService();
		return sitesOfService;
	}
	/* (non-Javadoc)
	 * @see com.velos.webservices.PatientScheduleSEI#createMEventStatus(com.velos.services.model.MEventStatuses)
	 */
//	@Override
//	public ResponseHolder createMEventStatus(MEventStatuses mEventStatuses)
//			throws OperationException {
//		// TODO Auto-generated method stub
//		return null;
//	}
	/* (non-Javadoc)
	 * @see com.velos.webservices.PatientScheduleSEI#addUnscheduledEvent(com.velos.services.model.PatientProtocolIdentifier, com.velos.services.model.EventIdentifiers, com.velos.services.model.EventAttributes, com.velos.services.model.VisitIdentifier)
	 */
	@Override
	public ResponseHolder addUnscheduledEvents(
			PatientProtocolIdentifier scheduleID, StudyIdentifier studyIdentifier, 
			PatientIdentifier patientIdentifier,
			MultipleEvents events,
			VisitIdentifier visitIdentifier) throws OperationException {
		return PatientScheduleClient.addUnscheduledEvent(scheduleID, studyIdentifier, patientIdentifier, events,visitIdentifier);
	}
	@Override
	public ResponseHolder addUnscheduledAdminEvents(
			CalendarIdentifier calendarIdentifier,
			MultipleEvents events,
			VisitIdentifier visitIdentifier) throws OperationException {
		
		return PatientScheduleClient.addAdminUnscheduledEvent(calendarIdentifier, events, visitIdentifier);
	}
	@Override
	public ResponseHolder createMEventStatus(MEventStatuses mEventStatuses)
			throws OperationException {
		ResponseHolder response = PatientScheduleClient.createMEventStatus(mEventStatuses);
		return response;
	}
	
//	public ResponseHolder addUnscheduledEvent(PatientProtocolIdentifier scheduleID, EventIdentifiers eventIdentifiers, EventAttributes eventAttributes, VisitIdentifier visitIdentifier)
//			throws OperationException {
//		ResponseHolder response = PatientScheduleClient.addUnscheduledEvent(scheduleID, eventIdentifiers, eventAttributes, visitIdentifier);
//		return response;
//	}
	
	public ResponseHolder updateMEventStatus(ScheduleEventStatuses scheduleEventStatusIdentifiers)
			throws OperationException {
		ResponseHolder response = PatientScheduleClient.updateMEventStatus(scheduleEventStatusIdentifiers);
		return response;
	}
	
	public MPatientScheduleList addMPatientSchedules(MPatientSchedules mPatientSchedules)
	       throws OperationException {
		Map<String, List<String>> requestHeaders = (Map<String, List<String>>)context.getMessageContext().get(MessageContext.HTTP_REQUEST_HEADERS);
		if(requestHeaders.get("remoteaddr")!=null)
			AbstractService.IP_ADDRESS_FIELD_VALUE=(String)requestHeaders.get("remoteaddr").get(0);
		else{
			HttpServletRequest request = (HttpServletRequest)context.getMessageContext().get(MessageContext.SERVLET_REQUEST);
			AbstractService.IP_ADDRESS_FIELD_VALUE=request.getRemoteAddr();
		}
		MPatientScheduleList mPatSchList = PatientScheduleClient.addMPatientSchedules(mPatientSchedules);
		return mPatSchList;
	}
	
	public EventStatusHistory getEventStatusHistory(EventIdentifier eventIdentifier, String sortBy, String orderBy)
		       throws OperationException {
			EventStatusHistory eventStatusHistory = PatientScheduleClient.getEventStatusHistory(eventIdentifier, sortBy, orderBy);
			return eventStatusHistory;
		}
}