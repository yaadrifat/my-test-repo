package com.velos.webservices;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.client.CheckSubmissionClient;
import com.velos.services.client.ProtocolSubmissionClient;
import com.velos.services.model.ReviewBoards;
import com.velos.services.model.ReviewBoardsIdentifier;
import com.velos.services.model.StudyIdentifier;

@Path("/restServices/")
@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
public class CheckSubmissionWS {
	private static Logger logger = Logger.getLogger(CheckSubmissionWS.class.getName());
	ResponseHolder response = new ResponseHolder();
	ReviewBoards csResponse = new ReviewBoards();
	@POST
	@Path("getStudyCheckAndSubmitStatusResponse")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public ReviewBoards getStudyCheckAndSubmitStatusResponse (StudyIdentifier studyIdentifier){
		try {
			csResponse = new ReviewBoards();
			csResponse = CheckSubmissionClient.getStudyCheckAndSubmitStatusResponse(studyIdentifier);
		}catch (OperationException e) {
			logger.error("getStudyCheckAndSubmitStatusResponse", e);
		} 
		return csResponse;
	}
	
	@POST
	@Path("submitProtocolVersion")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public ResponseHolder submitProtocolVersion(ReviewBoardsIdentifier reviewBoardsIdentifier){
		response = new ResponseHolder();
		try {
			response = ProtocolSubmissionClient.submitProtocolVersion(reviewBoardsIdentifier);
		} catch (OperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	return response;	
	}
}
