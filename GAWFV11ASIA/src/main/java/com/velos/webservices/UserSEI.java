/**
 * Created On Aug 9, 2012
 */
package com.velos.webservices;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.Groups;
import com.velos.services.model.NonSystemUser;
import com.velos.services.model.Organizations;
import com.velos.services.model.User;
import com.velos.services.model.User.UserStatus;
import com.velos.services.model.UserIdentifier;
import com.velos.services.model.UserSearch;
import com.velos.services.model.UserSearchResults;

/**
 * @author Kanwaldeep
 *
 */
@Produces
@Consumes
@WebService(targetNamespace="http://velos.com/services/")
public interface UserSEI {
//
//	@WebResult(name="Response")
//	public ResponseHolder createNonSystemUser(
//			@WebParam(name="NonSystemUser")
//			NonSystemUser nonSystemUser); 

//	@WebResult(name="Response")
//	public ResponseHolder createUser(
//			@WebParam(name="User")
//			User user); 

	@WebMethod(operationName = "changeUserStatus")
	@WebResult(name="Response")
	@POST
	@Path("changeuserstatus")
	public ResponseHolder changeUserStatus(
			@WebParam(name = "UserIdentifier")
			UserIdentifier uId,
			@WebParam(name = "UserStatus")
			UserStatus uStat) 
	throws OperationException;


	@GET
	@Path("/getallgroups")
	@WebMethod(operationName = "getAllGroups")
	@WebResult(name="UserGroups")
	public Groups getAllGroups() throws OperationException;

	@POST
	@Path("/getusergroups")
	@WebResult(name = "UserGroups")
	public Groups getUserGroups(
			@WebParam(name="UserIdentifier")UserIdentifier userIdent) 
	throws OperationException;
	
	@POST
	@Path("/createnonsystemuser")
	@WebMethod(operationName = "createNonSystemUser")
	@WebResult(name="Response")
	public ResponseHolder createNonSystemUser(@WebParam(name = "NonSystemUser")NonSystemUser nonsystemuser)
 	throws OperationException;
	
	@GET
	@Path("/getallorganizations")
	@WebMethod(operationName = "getAllOrganizations")
	@WebResult(name="UserOrganizations")
	public Organizations getAllOrganizations() throws OperationException;
	
	@POST
	@Path("/killusersession")
	@WebMethod(operationName = "killUserSession")
	@WebResult(name="Response")
	public ResponseHolder killUserSession(
			@WebParam(name = "UserIdentifier")
			UserIdentifier uId) 
	throws OperationException;
	
	@POST
	@Path("/createuser")
	@WebMethod(operationName = "createUser")
	@WebResult(name="Response")
	public ResponseHolder createUser(@WebParam(name = "User")User user)
 	throws OperationException;
	
	@POST
	@Path("/checkesignature")
	@WebResult(name="Response")
	public ResponseHolder checkESignature(@WebParam(name="eSignature") String eSignature)
	throws OperationException; 

	@POST
	@Path("/searchUser")
	@WebResult(name="UserSearchResults")
	public UserSearchResults searchUser(@WebParam(name="UserSearch") UserSearch userSearch)
	throws OperationException;
	
	@GET
	@Path("/refreshMenuTabs")
	@WebMethod(operationName = "refreshMenuTabs")
	public void refreshMenuTabs() throws OperationException;
	
}
