package com.velos.webservices;

import javax.jws.WebService;

import org.apache.log4j.Logger;

import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.client.UserClient;
import com.velos.services.model.OrganizationDetail;
import com.velos.services.model.OrganizationSearch;
import com.velos.services.model.OrganizationSearchResults;

@WebService(
		serviceName = "OrganizationService", 
		endpointInterface = "com.velos.webservices.OrganizationSEI", 
		targetNamespace = "http://velos.com/services/")
public class OrganizationRS implements OrganizationSEI{
	
private static Logger logger = Logger.getLogger(OrganizationRS.class.getName());
	
	public ResponseHolder createOrganization(OrganizationDetail organizationDetail)throws OperationException{
		
		ResponseHolder response = new ResponseHolder();
		
		try {
			return UserClient.createOrganization(organizationDetail);

		} catch (OperationException e) {
			logger.error("createOrganization", e);
			throw e;
		} catch (Throwable t) {
			logger.error("createOrganization", t);
			response.addIssue(new Issue(IssueTypes.UNKNOWN_THROWABLE));
		}
		return response;
	}
	
	public ResponseHolder updateOrganisation(OrganizationDetail organizationDetail) throws OperationException {
		ResponseHolder response = new ResponseHolder();
		
		try {
			return UserClient.updateOrganisation(organizationDetail);

		} catch (OperationException e) {
			logger.error("updateOrganisation", e);
			throw e;
		} catch (Throwable t) {
			logger.error("updateOrganisation", t);
			response.addIssue(new Issue(IssueTypes.UNKNOWN_THROWABLE));
		}
		return response;
	}
	
	public OrganizationSearchResults searchOrganisations(OrganizationSearch organizationSearch) throws OperationException {
		OrganizationSearchResults response = new OrganizationSearchResults();
		
		try {
			response= UserClient.searchOrganisations(organizationSearch);

		} catch (OperationException e) {
			logger.error("searchOrganisations", e);
			throw e;
		} catch (Throwable t) {
			logger.error("searchOrganisations", t);
			response.addIssue(new Issue(IssueTypes.UNKNOWN_THROWABLE));
		}
		return response;
	}
	
}
