package com.velos.webservices;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.velos.services.OperationException;
import com.velos.services.model.NetworkLevelSite;
import com.velos.services.model.NetworkSiteDetails;
import com.velos.services.model.NetworkSiteLevelDetails;
//import com.velos.services.model.NetworkSiteLevelSearchResult;
import com.velos.services.model.NetworkSiteSearchResults;
import com.velos.services.model.NetworkSiteUserDetails;
import com.velos.services.model.NetworkSiteUserResult;
import com.velos.services.model.Networks;
import com.velos.services.model.NetworksDetails;
import com.velos.services.model.SiteLevelDetails;
import com.velos.services.model.UserNetworkSiteResults;
import com.velos.services.model.UserNetworkSites;
import com.velos.services.model.UserNetworksDetail;
import com.velos.services.model.NetworkUserDetail;
import com.velos.services.model.NetworkUserDetails;
import com.velos.services.model.NetworkUsersSearchResult;

@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
@WebService(targetNamespace="http://velos.com/services/")

public interface NetworkSEI {
	
	/*@POST
	@Path("/getNetworkSiteDetails")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public NetworkSiteSearchResults getNetworkSiteDetails(NetworkSiteDetails networkSiteDetails) throws OperationException;
*/
	
	@POST
	@Path("/searchNetworks")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Networks searchNetworks(NetworksDetails networksDetails) throws OperationException;

	
/*	@POST
	@Path("/getUserNetwork")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public NetworkUsersSearchResult getUserNetwork(NetworkUserDetail networkuser) throws OperationException;*/
	
	
	/*@POST
	@Path("/getNetworkUserDetails")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public NetworkUsersSearchResult getNetworkUserDetails( NetworkUserDetails networkUserDetails)
	throws OperationException;*/
	
	@POST
	@Path("/getNetworkSiteChildren")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public NetworkSiteSearchResults getNetworkSiteChildren ( SiteLevelDetails siteLevelDetails)
	throws OperationException;
	
	@POST
	@Path("/getNetworkSiteParent")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public NetworkSiteSearchResults getNetworkSiteParent ( SiteLevelDetails siteLevelDetails)
	throws OperationException;
	
	@POST
	@Path("/getNetworkLevelSites")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public NetworkSiteSearchResults getNetworkLevelSites ( NetworkLevelSite networklevelsite)
	throws OperationException;
	
	@POST
	@Path("/getNetworkSiteLevel")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public NetworkSiteSearchResults getNetworkSiteLevel(NetworkSiteLevelDetails networkSiteLevelDetails)
	throws OperationException;

	@POST
	@Path("/getUserNetworkSites")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public UserNetworkSiteResults getUserNetworkSites(UserNetworkSites userNetworkSites) 
	throws OperationException;
	@POST
	@Path("/getUserNetworks")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public NetworkUsersSearchResult getUserNetworks(UserNetworksDetail networkuser) 
	throws OperationException;
	
	@POST
	@Path("/getNetworkUser")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public UserNetworkSiteResults getNetworkUser(NetworkSiteUserDetails networkSiteUserDetails)
	throws OperationException; 
}
