package com.velos.webservices;

import javax.jws.WebParam;
import javax.jws.WebService;

import com.velos.services.OperationException;
import com.velos.services.OperationRolledBackException;
import com.velos.services.ResponseHolder;
import com.velos.services.client.BudgetClient;
import com.velos.services.client.MonitoringClient;
import com.velos.services.model.BudgetDetail;
import com.velos.services.model.BudgetIdentifier;
import com.velos.services.model.BudgetStatus;
import com.velos.services.model.CalendarNameIdentifier;
import com.velos.services.model.StudyIdentifier;

@WebService(
		serviceName = "BudgetService", 
		endpointInterface = "com.velos.webservices.BudgetSEI", 
		targetNamespace = "http://velos.com/services/")	 

public class BudgetWS implements BudgetSEI{
	
	public BudgetStatus getBudgetStatus(			
			BudgetIdentifier budgetIdentifier)
		throws OperationException {
		
			BudgetStatus status = null;
			status = BudgetClient.getBudgetStatus(budgetIdentifier);
			return status;
		}
	@Override
	public BudgetDetail getStudyCalBudget(StudyIdentifier studyIdent,CalendarNameIdentifier CalIdent) throws OperationException {
		
		return BudgetClient.getStudyCalBudget(studyIdent,CalIdent) ;
	}

	@Override
	public ResponseHolder createStudyCalBudget(BudgetDetail budgetDetail)
			throws OperationException {
		
		return BudgetClient.createStudyCalBudget(budgetDetail);
	}
	}


