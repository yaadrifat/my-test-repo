/**
 * Created On Nov 6, 2012
 */
package com.velos.webservices;

import javax.jws.WebService;

import org.apache.log4j.Logger;

import com.velos.services.OperationException;
import com.velos.services.client.SystemAdministrationRObj;
import com.velos.services.model.CodeTypes;
import com.velos.services.model.Codes;
import com.velos.services.model.ObjectInfo;
import com.velos.services.model.ObjectInfos;
import com.velos.services.model.ObjectMaps;
import com.velos.services.model.SimpleIdentifier;

/**
 * @author Kanwaldeep
 *
 */
@WebService(
		serviceName = "SystemAdministrationService", 
		endpointInterface = "com.velos.webservices.SystemAdministrationSEI", 
		targetNamespace = "http://velos.com/services/")	  
public class SystemAdministrationWS implements SystemAdministrationSEI {

	private static Logger logger = Logger.getLogger(SystemAdministrationWS.class); 
	@Override
	public Codes getCodeList(String codeType) throws OperationException {
		Codes codes = null; 
		try
		{
			codes = SystemAdministrationRObj.getCodeList(codeType); 
		}catch(OperationException oe)
		{
			logger.error("getCodeList", oe); 
			throw oe; 
		}catch(Throwable t){
			logger.error("getCodeList", t); 
			throw new OperationException(t);
		}
		return codes; 
	}

	public CodeTypes getCodeTypes() throws OperationException {
		CodeTypes types = null; 
		try
		{
			types = SystemAdministrationRObj.getCodeTypes(); 
		}catch(OperationException oe)
		{
			logger.error("getCodeList", oe); 
			throw oe; 
		}catch(Throwable t){
			logger.error("getCodeList", t); 
			throw new OperationException(t);
		}
		return types; 
	}

	/* (non-Javadoc)
	 * @see com.velos.webservices.SystemAdministrationSEI#getorCreateMObjectMap(com.velos.services.model.TableInfos)
	 */
	@Override
	public ObjectMaps getorCreateMObjectMap(ObjectInfos objectInfos) throws OperationException {
		ObjectMaps objectMaps = new ObjectMaps();
		try{
			objectMaps = SystemAdministrationRObj.getorCreateObjectMap(objectInfos)	;	
		}catch(OperationException oe)
		{
			logger.error("getOrCreateMObjectMap", oe);
			throw oe;
			
		}catch(Throwable t)
		{
			logger.error("getOrCreateMObjectMap", t);
		}
		return objectMaps;
	}
	
	public ObjectInfo getObjectInfoFromOID(SimpleIdentifier simpleIdentifier) throws OperationException {
		ObjectInfo objectInfo = new ObjectInfo();
		try{
			objectInfo = SystemAdministrationRObj.getObjectInfoFromOID(simpleIdentifier)	;	
		}catch(OperationException oe)
		{
			logger.error("getObjectInforFromOID ", oe);
			throw oe;
			
		}catch(Throwable t)
		{
			logger.error("getObjectInforFromOID ", t);
		}
		return objectInfo;
	}

}
