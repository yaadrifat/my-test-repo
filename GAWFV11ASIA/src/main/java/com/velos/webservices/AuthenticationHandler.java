/**
 * 
 */
package com.velos.webservices;

import java.io.StringReader;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.UnmarshalException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;


import org.w3c.dom.Document;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;

import java.sql.Connection;
import java.util.Iterator;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import org.apache.cxf.common.i18n.BundleUtils;
import org.apache.cxf.common.logging.LogUtils;
import org.apache.cxf.configuration.security.AuthorizationPolicy;
import org.apache.cxf.interceptor.security.AccessDeniedException;
import org.apache.cxf.jaxrs.ext.RequestHandler;
import org.apache.cxf.jaxrs.impl.WebApplicationExceptionMapper;
import org.apache.cxf.jaxrs.model.ClassResourceInfo;
import org.apache.cxf.jaxrs.provider.JSONProvider;
import org.apache.cxf.message.Message;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.web.user.UserJB;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.ResponseHolder;
import com.velos.services.model.User.UserStatus;


/**
 * @author dylan
 *
 */
public class AuthenticationHandler implements RequestHandler,ExceptionMapper<WebApplicationException> {

	@EJB
	private UserAgentRObj userAgent;
	
	private static final Logger LOG = LogUtils.getL7dLogger(WebApplicationExceptionMapper.class);
    private static final ResourceBundle BUNDLE = BundleUtils.getBundle(WebApplicationExceptionMapper.class);

	public Response handleRequest(Message m, ClassResourceInfo resourceClass) {
		AuthorizationPolicy policy = (AuthorizationPolicy)m.get(AuthorizationPolicy.class);
		if(policy==null){
			ResponseHolder	response=new ResponseHolder();
			response.addIssue(new Issue(IssueTypes.USER_AUTHENTICATION,"Authentication Required"));
			return Response.status(Response.Status.FORBIDDEN).entity(response).build();
		}
		
		
		InputStream inputStream = m.getContent(InputStream.class);
		String RequestFormate= inputStream.toString();
		String Cont_Type = (String)m.get(Message.CONTENT_TYPE);
	
		if(Cont_Type.equals("application/json")  ){
 		boolean isJson=isJSONValid(RequestFormate);
		if(!isJson){
			ResponseHolder	responseJson=new ResponseHolder();
			responseJson.addIssue(new Issue(IssueTypes.INTERNAL_SERVER_ERROR,"NOT A VALID JSON"));
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(responseJson).build();
			}
		}
		
		if(Cont_Type.equals("application/xml")  ){
	 	boolean isXML=isXMLValid(RequestFormate);
		if(!isXML){
				ResponseHolder	responseXML=new ResponseHolder();
				responseXML.addIssue(new Issue(IssueTypes.INTERNAL_SERVER_ERROR,"NOT A VALID XML"));
				return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(responseXML).build();
				}
		}
		
		policy.getUserName();
		policy.getPassword(); 
		// policy.setAuthorization(value)
		UserJB userJB = new UserJB();
		UserBean user = null;
		Connection conn = null;
		try{
			user = userJB.validateUser(policy.getUserName(), policy.getPassword(), true);
			//	 resourceClass.
		}
		catch (AccessDeniedException ex) {
			return Response.status(Response.Status.FORBIDDEN).build();
		}
		finally {
			try { if (conn != null) { conn.close(); } } catch (Exception e){}
		}

		if (user == null){
			ResponseHolder	response=new ResponseHolder();
			response.addIssue(new Issue(IssueTypes.INVALID_USER_PASSWORD,"Invalid User Credential"));
			return Response.status(Response.Status.FORBIDDEN).entity(response).build();
		}

		//DRM - fix for 5448 and 5453 - block authentication if the user
		//is not Active
		if (!user.userStatus.equalsIgnoreCase(UserStatus.ACTIVE.getLegacyString())){
			return Response.status(Response.Status.FORBIDDEN).build();
		}
		// alternatively :
		// HttpHeaders headers = new HttpHeadersImpl(m);
		// access the headers as needed  


		LoginContext lctx;

		try {

			// invokes the standard jboss client login module...required
			// for all EJB client communication in JBOss
			lctx = new LoginContext("client-login",
					new SimpleAuthCallbackHandler(
							policy.getUserName()));
			lctx.login();

		} catch (LoginException e) {
			return Response.status(Response.Status.FORBIDDEN).build();
		}


		return null; 

	}
	
	public static boolean isJSONValid(String JsonRequest) {
		boolean isValid=true;
		
		try{
			JsonParser parser = new JsonParser();
			parser.parse(JsonRequest);
			
	} catch(JsonSyntaxException jse){
		System.out.println("Not a valid Json :"+jse.getMessage());
		return isValid =false;
	}
		try {
		    JSONObject jObj = new JSONObject(JsonRequest);
		  
		    
		} catch (JSONException e) {
			System.out.println("Not a valid Json :"+e.getMessage());
			return isValid =false;
		    // Do something to recover ... or kill the app.
		}
		
		return isValid =true;
	}
	
	public static boolean isXMLValid(String XMLRequest) {
		boolean isValid =false;

	    DocumentBuilder db = null;
		try {
			db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			InputSource isXML = new InputSource();
			isXML.setCharacterStream(new StringReader(XMLRequest));
		    Document doc = db.parse(isXML);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			System.out.println("Not a valid XML :"+e1.getMessage());
			e1.printStackTrace();
			return isValid = false;
		}
	    
		return isValid = true;
		
	}

	

	@Override
	public Response toResponse(WebApplicationException ex) {
		// TODO Auto-generated method stub
		String entityMessage= ex.getResponse().getEntity().toString();
		if(entityMessage.contains("JAXBException")){
			ResponseHolder	responseJson=new ResponseHolder();
			responseJson.addIssue(new Issue(IssueTypes.INTERNAL_SERVER_ERROR,"Incorrect Input. Kindly check the request"));
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(responseJson).build();
		}
        if (LOG.isLoggable(Level.WARNING)) {
            String message = ex.getCause() == null ? ex.getMessage() : ex.getCause().getMessage();
            if (message == null) {
                if (ex.getCause() != null) {
                    message = "cause is " + ex.getCause().getClass().getName();
                } else {
                    message = "no cause is available";
                }
            }
            org.apache.cxf.common.i18n.Message errorMsg = 
                new org.apache.cxf.common.i18n.Message("WEB_APP_EXCEPTION", BUNDLE, message);
            LOG.warning(errorMsg.toString());
        }
        Response r = ex.getResponse(); 
        if (r == null) {
            String message = null;
            if (ex.getCause() == null) {
                message = new org.apache.cxf.common.i18n.Message("DEFAULT_EXCEPTION_MESSAGE", 
                                                                 BUNDLE).toString();
            } else {
                message = ex.getCause().getMessage();
                if (message == null) {
                    message = ex.getCause().getClass().getName();
                }
            }
            r = Response.status(500).type(MediaType.TEXT_PLAIN).entity(message).build();
        }
        return r;
		
	}
	
}

