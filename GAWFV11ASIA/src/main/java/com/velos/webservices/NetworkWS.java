package com.velos.webservices;

import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;

import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.client.UserClient;
import com.velos.services.model.NetworkLevelSite;
import com.velos.services.model.NetworkSiteDetails;
import com.velos.services.model.NetworkSiteLevelDetails;
//import com.velos.services.model.NetworkSiteLevelSearchResult;
import com.velos.services.model.NetworkSiteSearchResults;
import com.velos.services.model.NetworkSiteUserDetails;
import com.velos.services.model.NetworkSiteUserResult;
import com.velos.services.model.Networks;
import com.velos.services.model.NetworksDetails;
import com.velos.services.model.SiteLevelDetails;
import com.velos.services.model.UserNetworkSiteResults;
import com.velos.services.model.UserNetworkSites;
import com.velos.services.model.UserNetworksDetail;
import com.velos.services.model.NetworkUserDetail;
import com.velos.services.model.NetworkUserDetails;
import com.velos.services.model.NetworkUsersSearchResult;

@WebService(
		serviceName = "NetworkService", 
		endpointInterface = "com.velos.webservices.NetworkSEI", 
		targetNamespace = "http://velos.com/services/")

public class NetworkWS implements NetworkSEI{
	
	private static Logger logger = Logger.getLogger(NetworkWS.class.getName());

		/*public NetworkSiteSearchResults getNetworkSiteDetails(NetworkSiteDetails networkSiteDetails)throws OperationException {
			
			NetworkSiteSearchResults networkSiteSearchResults=null;	
			try{
			 networkSiteSearchResults=UserClient.getNetworkSiteDetails(networkSiteDetails);
			}
			catch (OperationException e) {
				logger.error("createOrganization", e);
				throw e;
			} catch (Throwable t) {
				logger.error("createOrganization", t);
			}
			if(networkSiteSearchResults.getNetwork().size()<=0){
				throw new WebApplicationException(Response.status(Status.NOT_FOUND).entity(networkSiteSearchResults).build());
			}
			return networkSiteSearchResults;
		}*/
		
		public Networks searchNetworks(NetworksDetails networkD)throws OperationException {
			
			Networks networks=null;	
			try{
				networks=UserClient.searchNetworks(networkD);
			}
			catch (OperationException e) {
				logger.error("createOrganization", e);
				throw e;
			} catch (Throwable t) {
				logger.error("createOrganization", t);
			}
			if(networks.getNetwork().size()<=0){
				throw new WebApplicationException(Response.status(Status.NOT_FOUND).entity(networks).build());
			}
			return networks;
		}
		
		/*public NetworkUsersSearchResult getUserNetwork(NetworkUserDetail networkuser)throws OperationException {
			
	NetworkUsersSearchResult NetworkUsersSearchResult=null;	
			try{
				NetworkUsersSearchResult=UserClient.getUserNetwork(networkuser);
			}
			catch (OperationException e) {
				logger.error("createOrganization", e);
				throw e;
			} catch (Throwable t) {
				logger.error("createOrganization", t);
			}
			System.out.println("Size--->"+NetworkUsersSearchResult.getNetwork().size());
			if(NetworkUsersSearchResult.getNetwork().size()<=0){
				throw new WebApplicationException(Response.status(Status.NOT_FOUND).entity(NetworkUsersSearchResult).build());
			}
			return NetworkUsersSearchResult;
		}*/
		
/*		public NetworkUsersSearchResult getNetworkUserDetails(NetworkUserDetails networkuser)throws OperationException {
			
			NetworkUsersSearchResult NetworkUsersSearchResult=null;	
					try{
						NetworkUsersSearchResult=UserClient.getNetworkUserDetails(networkuser);
					}
					catch (OperationException e) {
						logger.error("createOrganization", e);
						throw e;
					} catch (Throwable t) {
						logger.error("createOrganization", t);
					}
					if(NetworkUsersSearchResult.getNetwork().size()<=0){
						throw new WebApplicationException(Response.status(Status.NOT_FOUND).entity(NetworkUsersSearchResult).build());
					}
					return NetworkUsersSearchResult;
				}
*/
		@Override
		public NetworkSiteSearchResults getNetworkSiteChildren(SiteLevelDetails siteLevelDetails)
				throws OperationException {
			NetworkSiteSearchResults networkSiteSearchResults=null;	
			try{
			 networkSiteSearchResults=UserClient.getNetworkSiteChildren(siteLevelDetails);
			}
			catch (OperationException e) {
				logger.error("createOrganization", e);
				throw e;
			} catch (Throwable t) {
				logger.error("createOrganization", t);
			}
			if(networkSiteSearchResults.getNetwork().size()<=0){
				throw new WebApplicationException(Response.status(Status.NOT_FOUND).entity(networkSiteSearchResults).build());
			}
			return networkSiteSearchResults;
		}
		
		@Override
		public NetworkSiteSearchResults getNetworkSiteParent(SiteLevelDetails siteLevelDetails)
				throws OperationException {
			NetworkSiteSearchResults networkSiteSearchResults=null;	
			try{
			 networkSiteSearchResults=UserClient.getNetworkSiteParent(siteLevelDetails);
			}
			catch (OperationException e) {
				logger.error("createOrganization", e);
				throw e;
			} catch (Throwable t) {
				logger.error("createOrganization", t);
			}
			if(networkSiteSearchResults.getNetwork().size()<=0){
				throw new WebApplicationException(Response.status(Status.NOT_FOUND).entity(networkSiteSearchResults).build());
			}
			return networkSiteSearchResults;
		}
		
		
		@Override
		public NetworkSiteSearchResults getNetworkLevelSites(NetworkLevelSite networklevelsite)
				throws OperationException {
			NetworkSiteSearchResults networkSiteSearchResults=null;	
			try{
			 networkSiteSearchResults=UserClient.getNetworkLevelSites(networklevelsite);
			}
			catch (OperationException e) {
				logger.error("createOrganization", e);
				throw e;
			} catch (Throwable t) {
				logger.error("createOrganization", t);
			}
			if(networkSiteSearchResults.getNetwork().size()<=0){
				throw new WebApplicationException(Response.status(Status.NOT_FOUND).entity(networkSiteSearchResults).build());
			}
			return networkSiteSearchResults;
		}
				
		public NetworkSiteSearchResults getNetworkSiteLevel(NetworkSiteLevelDetails networkSiteLevel)throws OperationException {
			
			NetworkSiteSearchResults networkSiteSearchResults=null;	
			try{
				networkSiteSearchResults=UserClient.getNetworkSiteLevel(networkSiteLevel);
			}
			catch (OperationException e) {
				logger.error("createOrganization", e);
				throw e;
			} catch (Throwable t) {
				logger.error("createOrganization", t);
			}
			if(networkSiteSearchResults.getNetwork().size()<=0){
				throw new WebApplicationException(Response.status(Status.NOT_FOUND).entity(networkSiteSearchResults).build());
			}
			return networkSiteSearchResults;
		}

		public UserNetworkSiteResults getUserNetworkSites(UserNetworkSites userNetworkSites)throws OperationException {
			
			UserNetworkSiteResults networkSiteSearchResults=null;	
			try{
				networkSiteSearchResults=UserClient.getUserNetworkSites(userNetworkSites);
			}
			catch (OperationException e) {
				logger.error("getUserNetworkSites", e);
				throw e;
			} catch (Throwable t) {
				logger.error("getUserNetworkSites", t);
			}
			if(networkSiteSearchResults.getNetwork().size()<=0){
				throw new WebApplicationException(Response.status(Status.NOT_FOUND).entity(networkSiteSearchResults).build());
			}
			return networkSiteSearchResults;
		}
		public NetworkUsersSearchResult getUserNetworks(UserNetworksDetail networkuser)throws OperationException {
			
			NetworkUsersSearchResult networkUsersSearchResult=null;	
					try{
						networkUsersSearchResult=UserClient.getUserNetworks(networkuser);
					}
					catch (OperationException e) {
						logger.error("createOrganization", e);
						throw e;
					} catch (Throwable t) {
						logger.error("createOrganization", t);
					}
					if(networkUsersSearchResult.getNetwork().size()<=0){
						throw new WebApplicationException(Response.status(Status.NOT_FOUND).entity(networkUsersSearchResult).build());
					}
					return networkUsersSearchResult;
				}
		
		public UserNetworkSiteResults getNetworkUser(NetworkSiteUserDetails networkSiteUserDetails)throws OperationException {
			
			UserNetworkSiteResults networkSiteUserResult=null;	
			try{
				networkSiteUserResult=UserClient.getNetworkUser(networkSiteUserDetails);
			}
			catch (OperationException e) {
				logger.error("createOrganization", e);
				throw e;
			} catch (Throwable t) {
				logger.error("createOrganization", t);
			}
			if(networkSiteUserResult.getNetwork().size()<=0){
				throw new WebApplicationException(Response.status(Status.NOT_FOUND).entity(networkSiteUserResult).build());
			}
			return networkSiteUserResult;
		}
}
