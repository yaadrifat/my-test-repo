package com.velos.webservices;

import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.velos.services.OperationException;
import com.velos.services.OperationRolledBackException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.OrganizationDetail;
import com.velos.services.model.OrganizationSearch;
import com.velos.services.model.OrganizationSearchResults;

@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
@WebService(targetNamespace="http://velos.com/services/")
public interface OrganizationSEI {
	
	@POST
	@Path("/createorganization")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public ResponseHolder createOrganization(OrganizationDetail organizationDetail)throws OperationException, OperationRolledBackException;
	
	@POST
	@Path("/updateOrganization")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public ResponseHolder updateOrganisation(OrganizationDetail organizationDetail) throws OperationException, OperationRolledBackException;

	@POST
	@Path("/searchOrganizations")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public OrganizationSearchResults searchOrganisations(OrganizationSearch organizationSearch) throws OperationException;
}
