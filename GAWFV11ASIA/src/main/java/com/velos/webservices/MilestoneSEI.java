package com.velos.webservices;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.Milestone;
import com.velos.services.model.MilestoneList;
import com.velos.services.model.StudyIdentifier;

@Produces("application/json")
@Consumes
@WebService(targetNamespace="http://velos.com/services/")
public interface MilestoneSEI {
	
	@GET
	@Path("/getMilestone/")
	@WebResult(name="Milestones")
	public Milestone getStudyMilestones(
			@WebParam(name="StudyIdentifier")StudyIdentifier studyIdent)
	throws OperationException;
	
	@POST
	@Path("/createmmilestones/")
	@WebResult(name="Response")
	public ResponseHolder createMMilestones(
			@WebParam(name="Milestones")
			MilestoneList milestones
			)
	throws OperationException;

}
