/**
 * 
 */
package com.velos.webservices;

import java.util.List;

import javax.jws.WebService;
import javax.jws.WebResult;
import javax.jws.WebParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import com.velos.services.OperationException;
import com.velos.services.OperationRolledBackException;

import com.velos.services.ResponseHolder;
import com.velos.services.model.StudyIdentifier;

/**
 * @author dylan
 *
 */
@WebService(
		 targetNamespace="http://velos.com/services/")
public interface WorkflowSEI {
	
	@POST
	@Path("/workflowservice/")
	@WebResult(name = "Response")
	public abstract ResponseHolder calculateWorkFlow(
			@WebParam(name = "StudyIdentifier")
			StudyIdentifier id)
		throws OperationException, OperationRolledBackException;
}