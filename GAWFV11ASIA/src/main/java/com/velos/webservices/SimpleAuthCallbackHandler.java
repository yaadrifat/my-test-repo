/**
 * 
 */
package com.velos.webservices;

import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.UnsupportedCallbackException;

/**
 * @author dylan
 * 
 */
public class SimpleAuthCallbackHandler implements CallbackHandler {
	public SimpleAuthCallbackHandler(String username) {
		this.username = username;

	}

	String username;

	public void handle(Callback[] callbacks) throws IOException,
			UnsupportedCallbackException {
		for (Callback callback : callbacks) {
			if (callback instanceof NameCallback) {
				((NameCallback) callback).setName(username);
			}

		}

	}

}
