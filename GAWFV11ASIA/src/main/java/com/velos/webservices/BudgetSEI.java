package com.velos.webservices;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.BudgetDetail;
import com.velos.services.model.BudgetIdentifier;
import com.velos.services.model.BudgetStatus;
import com.velos.services.model.CalendarNameIdentifier;
import com.velos.services.model.StudyIdentifier;

@WebService(
		 targetNamespace="http://velos.com/services/")
@Produces({"application/xml", "application/json", "application/javascript"})
public interface BudgetSEI {
	@POST
	@Path("/status/")
	@Consumes("application/json")
	@WebResult(name = "BudgetStatus")
	public abstract BudgetStatus getBudgetStatus(
			@WebParam(name = "BudgetIdentifier")
			BudgetIdentifier budgetIdentifier)
			
		throws OperationException;
	
	@GET
	@Path("/getBudget/")
	@WebResult(name="Budget")
	public BudgetDetail getStudyCalBudget(
			@WebParam(name="StudyIdentifier")
			StudyIdentifier studyIdent,
			@WebParam(name="CalendarNameIdentifier")
			CalendarNameIdentifier CalIdent
	)
	throws OperationException;
	
	@POST
	@Path("/createBudgets/")
	@WebResult(name="Response")
	public ResponseHolder createStudyCalBudget(
			@WebParam(name="Budget")
			BudgetDetail budgetDetail
			)
	throws OperationException;

}
