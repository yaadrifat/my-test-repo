package com.velos.webservices;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import org.apache.log4j.Logger;

import com.velos.services.AbstractService;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.client.OutboundMessageClient;
import com.velos.services.model.ChangesList;

/**
 * WebServices class dealing with all JMSMessage Services operations.
 * @author Parminder Singh
 *
 */
@WebService(
		serviceName = "OutboundMessageService", 
		endpointInterface = "com.velos.webservices.OutboundMessageSEI", 
		targetNamespace = "http://velos.com/services/")	 
public class OutboundMessageWS implements OutboundMessageSEI {
	
	private static Logger logger = Logger.getLogger(OutboundMessageWS.class.getName());
	ResponseHolder response = new ResponseHolder();
	
	@Resource
	private WebServiceContext context;
	

	public OutboundMessageWS() {
	}
	
	public ChangesList getOutboundMessages(Date fromDate,Date toDate,String moduleName,String calledFrom) throws OperationException{
		Map<String, List<String>> requestHeaders = (Map<String, List<String>>)context.getMessageContext().get(MessageContext.HTTP_REQUEST_HEADERS);
		if(requestHeaders.get("remoteaddr")!=null)
			AbstractService.IP_ADDRESS_FIELD_VALUE =(String)requestHeaders.get("remoteaddr").get(0);
		else{
			HttpServletRequest request = (HttpServletRequest)context.getMessageContext().get(MessageContext.SERVLET_REQUEST);
			AbstractService.IP_ADDRESS_FIELD_VALUE =request.getRemoteAddr();
		}
		ChangesList changesList = OutboundMessageClient.getOutboundMessages(fromDate,toDate,moduleName,calledFrom);
		
		return changesList;
	}
	

}
