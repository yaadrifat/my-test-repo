/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.velos.login.jwt;

//import org.bson.types.String;
//import org.joda.time.Long;




/**
 *
 * @author Ajit Parmar
 */
public class TokenInfo {
    private String userId;
    private String password;
    private Long issued;
    private Long expires;
    
    public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
    public String getPassword() {
        return password;
    }
     public void setPassword(String password) {
        this.password = password;
    }
    public Long getIssued() {
        return issued;
    }
    public void setIssued(Long issued) {
        this.issued = issued;
    }
    public Long getExpires() {
        return expires;
    }
    public void setExpires(Long expires) {
        this.expires = expires;
    }
}