package com.velos.base;

//import com.velos.eres.service.util.Rlog;
import org.apache.log4j.Logger;

/**
 * Represents any problem encountered during processing of a message by an
 * Application.
 * 
 * @author Vishal Abrol
 */
public class ApplicationException extends Exception {
    private static Logger logger = Logger.getLogger(ApplicationException.class
            .getName());

    /**
     * Constructs an instance of <code>ApplicationException</code> with the
     * specified detail message.
     */
    public ApplicationException(String msg) {
        super(msg);
        logger.fatal(msg);
    }
}
