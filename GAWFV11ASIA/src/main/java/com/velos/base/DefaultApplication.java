package com.velos.base;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.StringUtil;

/**
 * This class Implements Interface <code>Application</code>. If no other
 * ApplicationExtension are specified in configuration class for a particular
 * event, this DefaultApplication will take over the control and process the
 * message
 * 
 * @see this.processMessage()
 * @author Vishal Abrol
 */
public class DefaultApplication implements Application {
    private static Logger logger = Logger.getLogger(DefaultApplication.class
            .getName());

    private ArrayList inList;

    private ArrayList retValList;

    private InterfaceConfig cfgClass;

    private int currentCol;

    DataHolder dataHolder = DataHolder.getDataHolder();

    // constructor

    public DefaultApplication() {
        inList = new ArrayList();
        retValList = new ArrayList();
        cfgClass = new InterfaceConfig();
        // This variable is used to keep track to Message column processed
        // currently
        // This is sent to logger to display the position of field in the
        // message
        currentCol = -1;
    }

    /**
     * This method does all the processing with the message received.This method
     * will be looped based on the number of elements in the dataset.
     * 
     * @param <code>ArrayList</code> - in, contains all the attributes of the
     *            first element in dataset. e.g. In case of files, 'in' will
     *            contain all the data values in current row of the file.
     * @param <code>InterfaceConfig</code> - contains all the mapping retrieved
     *            from the file.
     * @param <code>java.sql.Connection</code>, Connection object already
     *            initiated and should be used to send request to the database.
     * @return int - status of the processed message. Returns '-1' ,if there is
     *         an error processing the record.
     */
    public int processMessage(ArrayList in, InterfaceConfig cfg, Connection conn)
            throws ApplicationException {
        try {
            PreparedStatement pstmt = null;
            ResultSet rs = null;
            String insertSql = "", updateSql = "";
            String whereCol = "", setValue = "", pkValue = "";
            //String[] colArray = cfg.getColList();
            String[] colArray = null;
            String selectSql = "";
            this.cfgClass = cfg;
            this.inList = in;
            String whereClause = "";
            ArrayList whereList = null;
            boolean requiredCol = false;
            retValList.clear();
            int count = 0;
            int colArrLen = 0;
            insertSql = (cfg.getInsertSql()).toString();
            updateSql = (cfg.getUpdateSql()).toString();
            selectSql = (cfg.getSelectSql()).toString();
            whereClause = cfg.getWhere();
            whereClause = (whereClause == null) ? "" : whereClause;
            logger.debug("Normalized Message=" + in);
            try {
                // Rlog.debug("common","SelectSQL in
                // ProcessformData"+selectSql);
                // Check if any where clause specified,if not,all the records
                // are for insert
                logger.debug("whereClause specified for the event"
                        + whereClause + "::length: " + whereClause.length());
                if (whereClause.length() > 0) {
                    selectSql = processSQL(selectSql, "bind");
                    if (selectSql == null)
                        throw new ApplicationException(
                                "Fatal error in Select statement created. Please check the configuration.");
                    // Rlog.debug("common","SelectSQL after Processing in
                    // ProcessformData"+selectSql);
                    // pstmt.setInt(1,id);
                    logger.debug("Finding if record is for update or insert");
                    String dbOut = getDBValue(selectSql, retValList, conn);
                    if (dbOut != null)
                        if (dbOut.equals("error")) {
                            logger
                                    .fatal("Error in executing select statement.");
                            logger
                                    .info("Error Validating Messge with Database.Error:VEL-CRT-01 - Velos Internal Error Code.Please Contact Velos Support.");
                            return -1;

                        } else {
                            if (dbOut.length() == 0)
                                dbOut = "-1";
                            count = new Integer(dbOut).intValue();

                        }
                } // end for where clause
                else {
                    count = 0;
                }
                logger.debug("Found ID ~" + count + "~" + in.get(0));

                /*
                 * Process insertSQL/updateSQL to replace any filecols with bind
                 * variables
                 */
                // Rlog.debug("common","BEFORE-BIND InsertSQL for
                // ProcessFormdata"+insertSql);
                // Rlog.debug("common","BEFORE-BIND updateSQL for
                // ProcessFormdata"+updateSql);
                ArrayList tempList = null;
                int numOfIter = 0;
                // Rlog.debug("common","Select statement output is " + count);
                if (count > 0) {
                    logger
                            .info("Record already exists, system will update the data for the row");
                    colArray=cfg.getPartColList();
                    colArrLen=colArray.length;
                    updateSql = processSQL(updateSql, "bind");
                    pkValue = (new Integer(count)).toString();
                    numOfIter = StringUtil.countOf(updateSql, "?");
                    tempList = new ArrayList(retValList);
                    if (whereClause.length() > 0) {
                        whereClause = processSQL(whereClause, "bind");
                        whereList = new ArrayList(retValList);
                    }
                    if (whereClause.length() > 0)
                        updateSql = updateSql + " where " + whereClause;
                    updateSql = StringUtil.decodeString(updateSql);
                    pstmt = conn.prepareStatement(updateSql);
                    // //Rlog.debug("common","we will do update"+updateSql);
                }

                else {
                    logger.info("New Record, system will insert a new row");
                    colArray=cfg.getColList();
                    colArrLen=colArray.length;
                    // reset the value of pk,which stores the primary key
                    // retrived if the reocrd exist
                    pkValue = "";

                    insertSql = processSQL(insertSql, "bind");
                    numOfIter = StringUtil.countOf(insertSql, "?");
                    tempList = new ArrayList(retValList);
                    insertSql = StringUtil.decodeString(insertSql);
                    insertSql = StringUtil.replace(insertSql, "[VELPK]", "");
                    pstmt = conn.prepareStatement(insertSql.toString());
                    // //Rlog.debug("common","we will do insert"+insertSql);
                    logger.debug("templist populated:" + tempList);
                }

                logger.debug("FINAL-Insert-SQL" + insertSql);
                logger.debug("FINAL-Update-SQL" + updateSql);

                pstmt.clearParameters();
                int numOfTimes = 0, velSQLIndx = -1, velExprIndx = -1;
                String tempStr = "";
                int currTmpIndex = 0;
                int arrCount = 0; // this varaible keep track of current array

                // count
                for (int i = 0; i < numOfIter; i++) {
                    // Rlog.debug("common","VALUE of i" + i +
                    // "arrcount"+arrCount + "ColArrLen" + colArrLen+" numOfIter
                    // " + numOfIter );

                    tempStr = colArray[arrCount];
                    
                    logger
                            .debug("Processing  value specified in interface Configuration"
                                    + tempStr);
                    velSQLIndx = tempStr.indexOf("[VELSQL]");
                    velExprIndx = tempStr.indexOf("[VELEXPR]");
                    /* If the current processed field is required Field. */

                    if ((tempStr.indexOf("[VELREQUIRED]")) >= 0) {
                        logger.debug("Set the requiredcol value");
                        requiredCol = true;
                        tempStr = StringUtil.replace(tempStr, "[VELREQUIRED]",
                                "");
                    } else {
                        requiredCol = false;
                    }

                    if (velSQLIndx >= 0) {
                        logger.debug("Processing SQL specified" + tempStr);
                        tempStr = StringUtil.replace(tempStr, "[VELSQL]", "");
                        if (tempStr.indexOf("[VELPK]") >= 0)
                            tempStr = StringUtil
                                    .replace(tempStr, "[VELPK]", "");
                        logger
                                .debug("Processing SQL specified-After removing keywords"
                                        + tempStr);
                        tempStr = processSQL(tempStr, "bind");
                        tempStr = StringUtil.decodeString(tempStr);
                        // Rlog.debug("common","tempStr after Processing in
                        // ProcessformData"+tempStr);
                        tempStr = getDBValue(tempStr, retValList, conn);
                        logger.debug("tempStr after Processing in getDBValue"
                                + tempStr + " retValList " + retValList);
                        if (tempStr.equals("error")) {
                            logger
                                    .info("Error Mapping values"
                                            + retValList
                                            + ". VEL-CRT-02 - Velos Internal Error Code.Please Contact Velos Support.");
                            return -1;

                        }
                        logger.debug(" requiredCol= " + requiredCol
                                + "tempStr.length?" + tempStr.length());
                        if ((requiredCol) && (tempStr.length() == 0)) {
                            logger
                                    .info(" No matching value found for mandatory column at position '"
                                            + (currentCol)+ "'. Values used to map:"
                                            + retValList);
                            // logger.info(" Required Data missing for
                            // "+retValList);
                            return -1;

                        }
                        logger.debug("Setting the value: " + tempStr);
                        pstmt.setString(i + 1, tempStr);
                        int takemeout = i + 1;
                        // Rlog.debug("common","Value set at i+1 in
                        // SQLEXPR"+takemeout);
                    } else if (velExprIndx >= 0) {
                        logger.debug("Processing Expression specified"
                                + tempStr);
                        if ((tempStr.indexOf("[VELREQUIRED]")) >= 0) {
                            logger.debug("Set the requiredcol value");
                            requiredCol = true;
                            tempStr = StringUtil.replace(tempStr, "[VELREQUIRED]",
                                    "");
                        } else {
                            requiredCol = false;
                        }

                        tempStr = StringUtil.replace(tempStr, "[VELEXPR]", "");
                        numOfTimes = StringUtil.countOf(tempStr, "[VELFILECOL");
                        // Rlog.debug("common","Values in
                        // arraylist"+tempList+"numofTimes"+numOfTimes);
                        logger.debug("tempList " + tempList);
                        for (int z = 0; z < numOfTimes; z++) {
                            setValue = (String) tempList.get(z + currTmpIndex);
                            logger
                                    .debug("Setting the value:(will remove quotes first) "
                                            + setValue);
                            pstmt.setString(i + 1 + z, setValue);
                            int takemeout = i + 1 + z;
                            // Rlog.debug("common","Value set at
                            // i+1+z"+takemeout+"VALue set was "
                            // +(String)tempList.get(z+currTmpIndex) );
                        }
                        currTmpIndex = (currTmpIndex + numOfTimes);

                        // (?,CONCAT(?,?),to_char((to_date(?)),'mm/dd/yyyy'),?,?,?,?,?,?)
                        // col1=?,col2=CONCAT(?,?),col3=to_char((to_date(?)),'mm/dd/yyyy'),col4=?,col5=?,col6=?,col7=?,col8=?,col9=?
                        // where to_date(col3,'mm/dd/yyyy')=to_date(?)
                        i = ((i + numOfTimes) - 1);

                    } else {
                        logger.debug("Binding File values" + tempStr);
                        if (tempStr.indexOf("[VELPK]") >= 0)
                            tempStr = StringUtil
                                    .replace(tempStr, "[VELPK]", "");
                        if (tempStr.indexOf("[VELFILECOL") >= 0) {
                            // Parse the current occurence to populet currentCol

                            setValue = (String) in.get(ParseCol(tempStr));
                        } else {
                            setValue = tempStr;
                        }

                        if ((requiredCol == true) && (setValue.length() == 0)) {
                            logger
                                    .info(" Data missing for mandatory column at position:"
                                            + (currentCol)
                                            + ". Current Value"
                                            + setValue);
                            return -1;
                        }
                        logger
                                .debug("Setting the value:(will remove quotes first) "
                                        + setValue);
                        pstmt.setString(i + 1, cfg.StripEnclosedChar(setValue));
                        int takemeout = i + 1;
                        // logger.debug("Value set at i+1+z"+takemeout);
                    }
                    arrCount++;
                }
                // this for loop sets the bind variables for where clause
                if (whereList != null) {
                    logger.debug("Setting the whereList" + whereList);
                    for (int i = 0; i < (whereList.size()); i++) {
                        int takemeout = i + colArrLen + tempList.size();
                        // Rlog.debug("common","whereList"+takemeout);
                        // Rlog.debug("common","whereList-take me
                        // out"+takemeout+" ColArrLen "+colArrLen+ "tempList
                        // "+tempList);
                        pstmt.setString(i + numOfIter + 1, (String) whereList
                                .get(i));

                    }
                }
                logger.info("Sending  the values to the database");
                int output = pstmt.executeUpdate();
                conn.commit();
                if (output >= 0) {
                    logger.info("Record updated successfully");
                    if (count > 0)
                        dataHolder.addToUpdateCount();
                    else
                        dataHolder.addToInsertCount();

                }
                logger.debug("Output after update" + output);
                if (output < 0)
                    return -1;
            } catch (SQLException e) {
                dataHolder.setMSG_LEVEL("BAD");
                logger.fatal("Error processing DB Request in ProcessMessage"
                        + e + e.getMessage());
                logger.info("Error Processing Data"
                            + " VEL-CRT-01 - Velos Internal Error Code.Please Contact Velos Support.");
                dataHolder.setMSG_LEVEL("");
                e.printStackTrace();
                return -1;
            } catch (Exception e) {
                dataHolder.setMSG_LEVEL("BAD");
                logger.fatal("Error processing DB Request in ProcessMessage."
                        + e + e.getMessage());
                logger.info("Internal Error Processing Data"
                        + " VEL-CRT-01 - Velos Internal Error Code.Please Contact Velos Support.");
                dataHolder.setMSG_LEVEL("");
                e.printStackTrace();
                return -1;
            } finally {
                try {
                    if (pstmt != null)
                        pstmt.close();
                } catch (Exception e) {
                }
                try {
                    if (rs != null)
                        rs.close();
                } catch (Exception e) {
                }

            }
        } catch (Exception e) {
            
            return -1;
        }
        return 1;
    }

    private int ParseCol(String inputStr) {
        String tmpStr = "";
        int numToReturn = 0;
        int index = inputStr.indexOf("[VELFILECOL");
        tmpStr = inputStr.substring(index, inputStr.indexOf("]"));
        tmpStr = StringUtil.replace(inputStr, "[VELFILECOL", "");
        numToReturn = ((new Integer(tmpStr.substring(0,(tmpStr.length() - 1))).intValue()) - 1);
        currentCol = numToReturn + 1;
        return (numToReturn);
    }

    /**
     * This method process a SQL Stting and replace dataobject attribute
     * reference to SQL bind varaibles. Also it populate an arraylist with all
     * the values sequenced with bind variables..
     * 
     * @param statement -
     *            a SQL String to process.
     * @param mode -
     *            whether to replace the dataobject attribute reference with SQL
     *            bind variables or actual values
     * @return <code>String</code> - processed SQL.
     */
    public String processSQL(String statement, String mode) {
        try {
            int index = -1, velIndex = -1, endIndex = -1, last_index = 0;
            String whereCol = "";
            String replaceWith = "";
            logger.debug("Process SQL Statement - " + statement);
            StringBuffer _temp_stmt = new StringBuffer(statement);
            boolean finished = false;
            if (mode == null)
                mode = "";
            mode = (mode.length() == 0) ? "bind" : mode;// othder mode is value
            retValList.clear();

            while (!finished) {
                logger.debug("in statement\n" + _temp_stmt.toString());
                // velIndex= (statement).indexOf("[VELFILECOL", last_index);
                velIndex = (_temp_stmt.toString()).indexOf("[VELFILECOL");
                logger.debug("found" + velIndex);
                if (velIndex >= 0) // if found next match
                {
                    // copy substring from last index to new index

                    endIndex = (_temp_stmt.toString()).indexOf("]", velIndex);

                    whereCol = (_temp_stmt.toString()).substring(velIndex,
                            endIndex + 1);

                    replaceWith = cfgClass.StripEnclosedChar((String) inList
                            .get(ParseCol(whereCol)));

                    // _temp_stmt.append(statement.substring(last_index,endIndex+1));

                    if (mode.equals("bind")) {

                        _temp_stmt = _temp_stmt.replace(velIndex, endIndex + 1,
                                "?");
                        // statement=StringUtil.replace(statement,whereCol,"?");
                    }

                    else {
                        logger.debug("REMOVEME::velsIndex:" + velIndex
                                + "endIndex:" + endIndex + 1);
                        _temp_stmt = _temp_stmt.replace(velIndex, endIndex + 1,
                                "'" + replaceWith + "'");
                        // statement=StringUtil.replace(_temp_stmt.toString(),whereCol,"'"+replaceWith+"'");
                    }
                    retValList.add(replaceWith.trim());
                    endIndex = endIndex + 1;
                    last_index = endIndex;

                } else {
                    // copy from last index to end of string

                    finished = true;
                }
            }
            logger.debug("REMOVEME::" + _temp_stmt.toString());
            return _temp_stmt.toString();

        } catch (Exception ex) {
            dataHolder.setMSG_LEVEL("BAD");
            logger.fatal("FATAL Error" + ex + ex.getMessage());
            logger.info("Internal Error Processing Data"
                    + " VEL-CRT-01 - Velos Internal Error Code.Please Contact Velos Support.");
            dataHolder.setMSG_LEVEL("");
            ex.printStackTrace();
            return null;
        }

    }

    /**
     * This method is also defined in DBEngine now.This method can be
     * <b>deprecated</b> so please use <b><code>DBEngine.getDBValue</code></b>
     * instead.
     * 
     * @param <code>String</code> - sql to execute on DB.
     * @param <code>ArrayList</code> - contains values replacing SQL bind
     *            variables in sql.
     * @param <code>Connection</code> - java.sql.Connection object to connect to
     *            DB.
     * 
     * @return <code>String</code> - value returned after processing SQL.
     *         return "error" if erro occurs in processing request.
     */
    private String getDBValue(String sql, ArrayList valList, Connection conn) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String output = null;
        String setValue = "";
        try {
            pstmt = conn.prepareStatement(sql);
            // Rlog.debug("common","preparedstatement in
            // ProcessformData:getDBValue"+pstmt);
            // pstmt.setInt(1,id);
            logger.debug("Processing SQL" + sql + "Bind Values" + valList);
            for (int i = 0; i < valList.size(); i++) {
                setValue = (String) valList.get(i);
                // [VELDEFAULT] is used to specify what values to be insertwed
                // if none is received in the file.
                if (setValue.length() == 0)
                    setValue = "[VELDEFAULT]";
                pstmt.setString(i + 1, setValue);
            }

            rs = pstmt.executeQuery();
            while (rs.next()) {
                output = (rs.getString(1));
            }
            if (output == null)
                output = "";

        } catch (SQLException e) {
            dataHolder.setMSG_LEVEL("BAD");
            logger
                    .fatal("SQLException processing DB Request in ProcessFormdata."
                            + e + e.getMessage());
            logger.info("Internal Error Processing Data"
                    + " VEL-CRT-01 - Velos Internal Error Code.Please Contact Velos Support.");
            dataHolder.setMSG_LEVEL("");
            e.printStackTrace();
            return "error";
        } catch (Exception ex) {
            dataHolder.setMSG_LEVEL("BAD");
            logger.fatal("Exception processing DB Request in ProcessFormdata."
                    + ex + ex.getMessage());
            logger.info("Internal Error Processing Data"
                    + " VEL-CRT-01 - Velos Internal Error Code.Please Contact Velos Support.");
            dataHolder.setMSG_LEVEL("");
            ex.printStackTrace();
            return "error";
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (rs != null)
                    rs.close();
            } catch (Exception e) {
            }

        }
        return output;
    }

}
