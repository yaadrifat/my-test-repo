package com.velos.base.extension;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.velos.base.Application;
import com.velos.base.DataHolder;
import com.velos.base.InterfaceConfig;
import com.velos.base.dbutil.DBEngine;
import com.velos.eres.service.util.EJBUtil;

public class AdvEventImport implements Application {
    private static Logger logger = Logger.getLogger(AdvEventImport.class
            .getName());

    Connection connClass;

    ArrayList cId;

    ArrayList cDesc;

    ArrayList cIdInfo;

    ArrayList cDescInfo;

    String outcomeStr;

    // Outcome Type Flag
    ArrayList cFlag;

    // Additional information
    ArrayList addInfo;
    
    DataHolder dataHolder=null;

    public AdvEventImport() {
        Connection connClass = null;
        cId = new ArrayList();
        cDesc = new ArrayList();
        String outcomeStr = "";
        cFlag = new ArrayList();
        addInfo = new ArrayList();
        cIdInfo = new ArrayList();
        cDescInfo = new ArrayList();
        dataHolder = DataHolder.getDataHolder();
    }

    public int processMessage(ArrayList in, InterfaceConfig cfg, Connection conn) {

        PreparedStatement pstmt = null;
        PreparedStatement pstmt_advinfo = null;
        ResultSet rs = null;
        this.connClass = conn;
        ArrayList out = new ArrayList(), bindList = new ArrayList();
        String mode = "", setupUser = "";
        String clientStr="PCF";
        String sql = "", customCode = "", inVal = "";
        int pk_adveve = -1, output = -1;
        logger
                .debug("********************************************************************");
        logger.debug("		Processing File Row" + in);
        logger
                .debug("********************************************************************");
        try {
            conn.setAutoCommit(false);
            logger.debug("Check if the Adverse event record already exist ");
            customCode = (cfg.StripEnclosedChar((String) in.get(0))).trim();
            /*
             * Add person code(once in insert mode) twice as it is used as first
             * 2 bind variables in populating er_per
             */
            bindList.add(customCode);
            
            logger
                    .debug("Setting bind variable value to check if the record is for update/insert: "
                            + customCode);
            out.add(customCode);
            inVal = DBEngine
                    .getDBValue(
                            "select pk_adveve from esch.sch_adverseve where lower(custom_col_interface)=lower(?)",
                            out);
            out.clear();
            logger.debug("Returned value for pk" + inVal);
            if (inVal.length() > 0)
                pk_adveve = EJBUtil.stringToNum(inVal);
            if (pk_adveve > 0) {
                dataHolder.addToUpdateCount();
                sql = "update esch.sch_adverseve set fk_codlst_aetype=?,ae_desc=?,ae_stdate=to_date(?,'mm/dd/yyyy'),ae_enddate=to_date(?,'mm/dd/yyyy'),"
                        + "ae_enterby=?,ae_outtype=?,ae_addinfo=?,last_modified_date=sysdate,last_modified_by=? "
                        + ",fk_study=?,fk_per=?,ae_grade=?,ae_name=?,ae_relationship=? where custom_col_interface=? ";
                logger
                        .info("Record already exist for this adverse event."                           
                                + " Update the record with the data. ");
            } else {
                dataHolder.addToInsertCount();
                sql = "insert into esch.sch_adverseve(fk_codlst_aetype,ae_desc,ae_stdate,ae_enddate,"
                        + "ae_enterby,ae_outtype,ae_addinfo,created_on,creator,fk_study,fk_per,ae_grade,ae_name,ae_relationship,custom_col_interface,pk_adveve) values"
                        + "(?,?,to_date(?,'mm/dd/yyyy'),to_date(?,'mm/dd/yyyy'),?,?,?,sysdate,?,?,?,?,?,?,?,?)";
                logger.info("No Record exist for this Adverse Event. Inserting a new record");
            }

            pstmt = conn.prepareStatement(sql);
            pstmt.clearParameters();

            inVal = cfg.StripEnclosedChar((String) in.get(19));
            if (inVal.length() == 0)
                inVal = "[VELDEFAULT]";
            logger.debug("Processing value: " + inVal);
            // out.add(inVal);
            if (inVal.equals("Y"))
                inVal = DBEngine.getDBValue(
                        "select pk_codelst from esch.sch_codelst where codelst_type='adve_type' "
                                + " and codelst_subtyp='al_sadve'", out);
            else
                inVal = DBEngine.getDBValue(
                        "select pk_codelst from esch.sch_codelst where codelst_type='adve_type' "
                                + " and codelst_subtyp='al_adve'", out);
            pstmt.setString(1, inVal);
            out.clear();

            // set adverse event description
            inVal = cfg.StripEnclosedChar((String) in.get(9));
            logger.debug("Processing Value: " + inVal);
            // if (inVal.length()==0) return -1;
            pstmt.setString(2, inVal);

            // Add again
            bindList.add(customCode);

            // Set Start Date adverse event
            inVal = cfg.StripEnclosedChar((String) in.get(5));
            logger.debug("Processing Value: " + inVal);
            pstmt.setString(3, inVal);

            // set End Date adverse events
            inVal = cfg.StripEnclosedChar((String) in.get(6));
            logger.debug("Processing Value: " + inVal);
            pstmt.setString(4, inVal);

            // aE entered by
            inVal = "interfaceuser";
            logger.debug("Processing value: " + inVal);
            out.add(inVal);
            inVal = DBEngine.getDBValue("select mapping_lmapfld"
                    + " from er_mapping where (?)= (mapping_rmapfld) "
                    + "and mapping_type='user' and mapping_customcol='"+clientStr+"'", out);
            pstmt.setString(5, inVal);
            // clear the parameter arraylist for next run,if any
            out.clear();

            // set ae_outtype
            int index = -1;
            inVal = cfg.StripEnclosedChar((String) in.get(16));
            logger.debug("Processing value: " + inVal);
            getCodeValues("outcome", conn);
            if ((inVal.toLowerCase()).equals("yes")) {

                index = cDesc.indexOf("al_interven");
                logger.debug("Index found for al_interven" + index);
                if (index >= 0)
                    cFlag.set(index, "1");
            }
            pstmt.setString(6, createOutcome());
            // set ae_addinfo
            index = -1;
            inVal = cfg.StripEnclosedChar((String) in.get(20));
            logger.debug("Processing value: " + inVal);
            getCodeAddInfo("adve_info", conn);

            if ((inVal.toLowerCase()).equals("yes")) {
                index = cDescInfo.indexOf("adv_dlt");
                if (index >= 0)
                    addInfo.set(index, "1");

            }
            pstmt.setString(7, createAddInfo());

            // set creator/last_modified_by
            inVal = "interfaceuser";
            logger.debug("Processing value: " + inVal);
            out.add(inVal);
            inVal = DBEngine.getDBValue("select mapping_lmapfld"
                    + " from er_mapping where (?)= (mapping_rmapfld) "
                    + "and mapping_type='user' and mapping_customcol='"+clientStr+"'", out);
            setupUser = inVal;
            pstmt.setString(8, inVal);
            // clear the parameter arraylist for next run,if any
            out.clear();

            // fk_study
            inVal = cfg.StripEnclosedChar((String) in.get(2));
            if (inVal.length() == 0)
                inVal = "[VELDEFAULT]";
            logger.debug("Processing value: " + inVal);
            out.add(inVal);
            inVal = DBEngine.getDBValue("select mapping_lmapfld"
                    + " from er_mapping where (?)= (mapping_rmapfld) "
                    + " and mapping_type='studymap' and mapping_customcol='"+clientStr+"'", out);
            pstmt.setString(9, inVal);

            // clear the parameter arraylist for next run,if any
            out.clear();

            // fk_per
            inVal = cfg.StripEnclosedChar((String) in.get(3));
            if (inVal.length() == 0)
                inVal = "[VELDEFAULT]";
            logger.debug("Processing value: " + inVal);
            out.add(inVal);
            inVal = DBEngine
                    .getDBValue(
                            "select pk_per"
                                    + " from er_per where (?)= (per_code) and fk_account=(select mapping_lmapfld from er_mapping where mapping_rmapfld='interfaceaccount' and mapping_type='account' and mapping_customcol='PCF') ",
                            out);
            pstmt.setString(10, inVal);

            // clear the parameter arraylist for next run,if any
            out.clear();

            // ae_grade
            inVal = cfg.StripEnclosedChar((String) in.get(10));
            logger.debug("Processing Value: " + inVal);
            pstmt.setString(11, inVal);

            // ae_name
            inVal = cfg.StripEnclosedChar((String) in.get(8));
            logger.debug("Processing Value: " + inVal);
            pstmt.setString(12, inVal);

            // set toxicity relationship
            out.clear();
            inVal = cfg.StripEnclosedChar((String) in.get(13));
            logger.debug("Processing Value: " + inVal);
            if (inVal.length() == 0)
                inVal = "[VELDEFAULT]";
            logger.debug("Processing value: " + inVal);
            out.add(inVal);
            inVal = DBEngine.getDBValue("select mapping_lmapfld"
                    + " from er_mapping where (?)= (mapping_rmapfld) "
                    + " and mapping_type='toxicity_relation' and mapping_customcol='"+clientStr+"'", out);
            pstmt.setString(13, inVal);

            // set custom col
            inVal = cfg.StripEnclosedChar((String) in.get(0));
            logger.debug("Processing Value: " + inVal);
            pstmt.setString(14, inVal);

            if (pk_adveve > 0) {
                mode = "M";
            } else {
                // Generate pk,only in case of new records
                out.clear();
                inVal = DBEngine.getDBValue(
                        "select esch.seq_sch_adverseve.nextval" + " from dual",
                        out);
                pstmt.setString(15, inVal);
                pk_adveve = EJBUtil.stringToNum(inVal);
                mode = "N";
            }

            logger.debug("Updating record...");
            output = pstmt.executeUpdate();
            if (output >= 0)
                conn.commit();
            

            if (output >= 0)
            // update advinfo table
            {

                String notifySQL = "";
                out.clear();
                int pk_advinfo = 0;
                String pkadvStr = EJBUtil
                        .integerToString(new Integer(pk_adveve));
                String pkadvinfoStr = "";
                for (int i = 0; i < cId.size(); i++) {
                    try {

                        out.add(cId.get(i));
                        out.add(pkadvStr);
                        inVal = DBEngine
                                .getDBValue(
                                        "select pk_advinfo"
                                                + " from esch.sch_advinfo where (?)= (fk_codelst_info) "
                                                + "and fk_adverse=? and advinfo_type='O'",
                                        out);
                        out.clear();
                        if (inVal.length() > 0)
                            pk_advinfo = EJBUtil.stringToNum(inVal);
                        pkadvinfoStr = inVal;

                        if (pk_advinfo > 0)
                            notifySQL = "update esch.sch_advinfo set fk_adverse=?,fk_codelst_info=?,advinfo_value=?,advinfo_type=?,last_modified_by=?,last_modified_date=sysdate "
                                    + " where pk_advinfo=? ";
                        else
                            notifySQL = "insert into esch.sch_advinfo(fk_adverse,fk_codelst_info,advinfo_value,advinfo_type,creator,created_on,pk_advinfo) "
                                    + "  values(?,?,?,?,?,sysdate,esch.seq_sch_advinfo.nextval)";

                        logger.debug("SQL used for advinfo" + notifySQL);
                        pstmt_advinfo = conn.prepareStatement(notifySQL);
                        pstmt_advinfo.clearParameters();

                        pstmt_advinfo.setString(1, pkadvStr);
                        pstmt_advinfo.setString(2, (String) cId.get(i));
                        pstmt_advinfo.setString(3, (String) cFlag.get(i));
                        pstmt_advinfo.setString(4, "O");

                        pstmt_advinfo.setString(5, setupUser);
                        // clear the parameter arraylist for next run,if any
                        out.clear();

                        if (pk_advinfo > 0)
                            pstmt_advinfo.setString(6, pkadvinfoStr);

                        output = pstmt_advinfo.executeUpdate();

                    } catch (Exception e) {
                        dataHolder.setMSG_LEVEL("BAD");
                        logger
                                .fatal("Exception occured while updating adv_info table"
                                        + e.getMessage());
                        logger.info("Exception occured while updating adv_info table"
                                + e.getMessage());
                        dataHolder.setMSG_LEVEL("");
                        return -1;
                    } finally {
                        try {
                            if (pstmt_advinfo != null)
                                pstmt_advinfo.close();
                        } catch (SQLException e) {
                            logger.fatal("Error in closing open cursors"
                                    + e.getMessage());
                        }
                    }

                }
                if (output >= 0)
                    conn.commit();

                for (int i = 0; i < cIdInfo.size(); i++) {
                    try {
                        pstmt_advinfo.clearParameters();
                        out.add(cIdInfo.get(i));
                        out.add(pkadvStr);
                        inVal = DBEngine
                                .getDBValue(
                                        "select pk_advinfo"
                                                + " from esch.sch_advinfo where (?)= (fk_codelst_info) "
                                                + "and fk_adverse=? and advinfo_type='A'",
                                        out);
                        out.clear();
                        if (inVal.length() > 0)
                            pk_advinfo = EJBUtil.stringToNum(inVal);
                        pkadvinfoStr = inVal;

                        if (pk_advinfo > 0)
                            notifySQL = "update esch.sch_advinfo set fk_adverse=?,fk_codelst_info=?,advinfo_value=?,advinfo_type=?,last_modified_by=?,last_modified_date=sysdate "
                                    + " where pk_advinfo=? ";
                        else
                            notifySQL = "insert into esch.sch_advinfo(fk_adverse,fk_codelst_info,advinfo_value,advinfo_type,creator,created_on,pk_advinfo) "
                                    + "  values(?,?,?,?,?,sysdate,esch.seq_sch_advinfo.nextval)";

                        logger.debug("SQL used for advinfo" + notifySQL);
                        pstmt_advinfo = conn.prepareStatement(notifySQL);
                        pstmt_advinfo.clearParameters();

                        pstmt_advinfo.setString(1, new Integer(pk_adveve)
                                .toString());
                        pstmt_advinfo.setString(2, (String) cIdInfo.get(i));
                        pstmt_advinfo.setString(3, (String) addInfo.get(i));
                        pstmt_advinfo.setString(4, "A");

                        pstmt_advinfo.setString(5, setupUser);
                        // clear the parameter arraylist for next run,if any
                        out.clear();

                        if (pk_advinfo > 0)
                            pstmt_advinfo.setString(6, pkadvinfoStr);
                        output = pstmt_advinfo.executeUpdate();

                    } catch (Exception e) {
                        dataHolder.setMSG_LEVEL("BAD");
                        logger.fatal("Error updating Additionl information"
                                + e.getMessage());
                        dataHolder.setMSG_LEVEL("");
                        return -1;
                    } finally {
                        try {
                            if (pstmt_advinfo != null)
                                pstmt_advinfo.close();
                        } catch (SQLException e) {
                            logger.fatal("Error in closing open cursors"
                                    + e.getMessage());
                        }
                    }

                }

            }
            if (output >= 0) {
                conn.commit();
                logger
                .info("Advesre Event:Record processed sucessfully with update status"
                        + output);   
            }
            // end for if (output>=0)

        } catch (SQLException e) {
            dataHolder.setMSG_LEVEL("BAD");
            logger.fatal("Error Processing the record :" + e.getMessage());
            logger.info("Error Processing the record - " + e.getMessage());
            dataHolder.setMSG_LEVEL("");
            try {
                conn.rollback();
            } catch (SQLException se) {
            }
            return -1;
        } catch (Exception e) {
            dataHolder.setMSG_LEVEL("BAD");
            logger.fatal("Exception Processing the record :" + e.getMessage());
            logger.fatal("Exception Processing the record - " + e.getMessage());
            dataHolder.setMSG_LEVEL("");
            try {
                conn.rollback();
            } catch (SQLException se) {
            }
            return -1;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
                // if (pstmt_advinfo != null) pstmt_advinfo.close();

            } catch (SQLException e) {
                logger.error("Error in closing the PreparedStatement Object"
                        + e.getMessage());
            }

        }
        return output;
    }

    public boolean getCodeValues(String cType, Connection conn) {
        int rows = 0;
        PreparedStatement pstmt = null;
        cFlag.clear();
        cId.clear();
        cDesc.clear();
        ResultSet rs = null;
        // set code type
        try {

            pstmt = conn
                    .prepareStatement("select PK_CODELST, trim(CODELST_SUBTYP) as CODELST_SUBTYP , CODELST_DESC , CODELST_SEQ  from esch.sch_codelst where rtrim(CODELST_TYPE) = ? order by  CODELST_SEQ");
            pstmt.setString(1, cType);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                setCDesc(rs.getString("CODELST_SUBTYP"));
                setCId(rs.getString("PK_CODELST"));
                setCFlag("0");

                rows++;
            }
            return true;
        } catch (SQLException ex) {
            dataHolder.setMSG_LEVEL("BAD");
            logger.fatal("EXCEPTION IN FETCHING FROM Codelist table " + ex);
            logger.info("EXCEPTION IN FETCHING FROM Codelist table " + ex);
            dataHolder.setMSG_LEVEL("");
            return false;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
                if (rs != null)
                    rs.close();
            } catch (Exception e) {
            }

        }
    }

    public boolean getCodeAddInfo(String cType, Connection conn) {
        int rows = 0;
        PreparedStatement pstmt = null;
        addInfo.clear();
        cDescInfo.clear();
        cIdInfo.clear();
        ResultSet rs = null;
        // set code type
        try {

            pstmt = conn
                    .prepareStatement("select PK_CODELST, trim(CODELST_SUBTYP) as CODELST_SUBTYP, CODELST_DESC , CODELST_SEQ  from esch.sch_codelst where rtrim(CODELST_TYPE) = ? order by  CODELST_SEQ");
            pstmt.setString(1, cType);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                setCDescInfo(rs.getString("CODELST_SUBTYP"));
                setCIdInfo(rs.getString("PK_CODELST"));
                addInfo.add("0");
                rows++;
            }

            return true;
        } catch (SQLException ex) {
            dataHolder.setMSG_LEVEL("BAD");
            logger.fatal("EXCEPTION IN FETCHING FROM Codelist table " + ex);
            dataHolder.setMSG_LEVEL("");
            return false;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
                if (rs != null)
                    rs.close();

            } catch (Exception e) {
            }

        }
    }

    public String createOutcome() {
        String outStr = "";

        logger.debug("creating Outcome String to update");
        for (int i = 0; i < getCFlag().size(); i++) {
            outStr = outStr + cFlag.get(i);
        }
        return outStr;

    }

    public String createAddInfo() {
        String outStr = "";

        logger.debug("creating Additional Information String to update");
        for (int i = 0; i < addInfo.size(); i++) {
            outStr = outStr + addInfo.get(i);
        }
        return outStr;

    }

    /**
     * Returns the value of cId.
     */
    public ArrayList getCId() {
        return cId;
    }

    /**
     * Sets the value of cId.
     * 
     * @param cId
     *            The value to assign cId.
     */
    public void setCId(ArrayList cId) {
        this.cId = cId;
    }

    public void setCId(String cId) {
        this.cId.add(cId);
    }

    /**
     * Returns the value of cDesc.
     */
    public ArrayList getCDesc() {
        return cDesc;
    }

    /**
     * Sets the value of cDesc.
     * 
     * @param cDesc
     *            The value to assign cDesc.
     */
    public void setCDesc(ArrayList cDesc) {
        this.cDesc = cDesc;
    }

    public void setCDesc(String cDesc) {
        this.cDesc.add(cDesc);
    }

    /**
     * Returns the value of outcomeStr.
     */
    public String getOutcomeStr() {
        return outcomeStr;
    }

    /**
     * Sets the value of outcomeStr.
     * 
     * @param outcomeStr
     *            The value to assign outcomeStr.
     */
    public void setOutcomeStr(String outcomeStr) {
        this.outcomeStr = outcomeStr;
    }

    /**
     * Returns the value of cFlag.
     */
    public ArrayList getCFlag() {
        return cFlag;
    }

    /**
     * Sets the value of cFlag.
     * 
     * @param cFlag
     *            The value to assign cFlag.
     */
    public void setCFlag(ArrayList cFlag) {
        this.cFlag = cFlag;
    }

    public void setCFlag(String cFlag) {
        this.cFlag.add(cFlag);
    }

    /**
     * Returns the value of CIdInfo.
     */
    public ArrayList getCIdInfo() {
        return cIdInfo;
    }

    /**
     * Sets the value of CIdInfo.
     * 
     * @param CIdInfo
     *            The value to assign CIdInfo.
     */
    public void setCIdInfo(ArrayList cIdInfo) {
        this.cIdInfo = cIdInfo;
    }

    public void setCIdInfo(String cInfo) {
        this.cIdInfo.add(cInfo);
    }

    /**
     * Returns the value of CDescInfo.
     */
    public ArrayList getCDescInfo() {
        return cDescInfo;
    }

    /**
     * Sets the value of CDescInfo.
     * 
     * @param CDescInfo
     *            The value to assign CDescInfo.
     */
    public void setCDescInfo(ArrayList cDescInfo) {
        this.cDescInfo = cDescInfo;
    }

    public void setCDescInfo(String CDesc) {
        this.cDescInfo.add(cDesc);
    }

}
