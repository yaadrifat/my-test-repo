package com.velos.base.component;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.mule.umo.UMOEventContext;
import org.mule.umo.UMOMessage;
import org.mule.umo.lifecycle.Callable;
import org.mule.util.TemplateParser;

public class NotifyFileReceivedSmtpAPI implements Callable{

	private String emailTextFile;
	private TemplateParser parser = TemplateParser.createAntStyleParser();
	private String emailText;


	public Object onCall(UMOEventContext eventContext) throws Exception {
		UMOMessage msg;

		msg=eventContext.getMessage();
		Map props = new HashMap();

        	//Will replace ${filename} in the email text
        	props.put("filename", msg.getProperty("originalFilename"));

		return parser.parse(props, emailText);
	}

	private String readFile(String path) throws IOException
	{
		FileReader fr = new FileReader(path);
		BufferedReader br=new BufferedReader(fr);

		String tmp;
		String email="";
		while((tmp=br.readLine())!=null)
		{
			email=email.concat(tmp)+"\r\n";
		}
		return email;
	}

	public String getEmailTextFile() {
		return this.emailTextFile;
	}

	public void setEmailTextFile(String emailTextFile) throws IOException {
		this.emailTextFile = emailTextFile;
		this.emailText=this.readFile(this.emailTextFile);
	}

}