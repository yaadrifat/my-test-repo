package com.velos.service; 
 

import com.velos.eres.service.util.EJBUtil;
import com.velos.impex.dispatcher.ReverseA2ADispatcher;

import java.lang.management.ManagementFactory;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.jboss.system.ServiceMBeanSupport;

@Singleton
@Startup
public class A2AService extends ServiceMBeanSupport implements A2AServiceMBean { 

	private MBeanServer platformMBeanServer;
	private ObjectName objectName = null;
    
	@PostConstruct
    public void registerInJMX() {
		if(EJBUtil.checkMBeanAccess(this.getClass().getCanonicalName())){
		    try {
		    	
		      objectName = new ObjectName("XRayMonitoring:type=" + this.getClass().getName());
		      platformMBeanServer = ManagementFactory.getPlatformMBeanServer();
		      platformMBeanServer.registerMBean(this, objectName);
			  } catch (Exception e) {
			      throw new IllegalStateException("Problem during registration of Monitoring into JMX:" + e);
			  }
	        ReverseA2ADispatcher.startDispatcher();
		}
 
  } 
	@PreDestroy
	  public void unregisterFromJMX() {
		  if(EJBUtil.checkMBeanAccess(this.getClass().getCanonicalName())){
		      try {
		          platformMBeanServer.unregisterMBean(this.objectName);
		      } catch (Exception e) {
		          throw new IllegalStateException("Problem during unregistration of Monitoring into JMX:" + e);
		      }
	  
		      ReverseA2ADispatcher.stopDispatcher();
		  }
  } 
   
  } 