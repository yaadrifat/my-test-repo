package com.velos.controller;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.web.user.UserJB;

public class SessionListener implements HttpSessionListener {	
	int currUserId = 0;
	public void sessionCreated(HttpSessionEvent se) {
		synchronized (this) {/* INF-22330 06-Aug-2012 -Sudhir */
			try {
				 HttpSession session = se.getSession();
				 String sessUserId = (String) session.getAttribute("userId");
				 int userId = EJBUtil.stringToNum(sessUserId);
				 currUserId = userId;
			} catch(NullPointerException ne){
				 Rlog.debug("Session",
		                    "In sessionCreated Listener Session Object Not Found" );
			}catch(ClassCastException cse){
				 Rlog.debug("user",
						 "In sessionCreated Listener UserJB Object Not Found in Session " );
			}
		}
	}

	public void sessionDestroyed(HttpSessionEvent se) {
		synchronized (this) {/* INF-22330 06-Aug-2012 -Sudhir */
			try {
				HttpSession session = se.getSession();
				if (session.getAttribute("currentUser") != null) {
					UserJB userJB = (UserJB) session.getAttribute("currentUser");
					int userId = currUserId;
					if(userJB!=null){
							userJB.setUserId(userId);
							userJB.getUserDetails();
						if(session.getId()!=null && !(userJB.getUserCurrntSsID().equalsIgnoreCase(session.getId().toString()))){
							userJB.setUserLoginFlag("1");
							userJB.setUserCurrntSsID(session.getId());
						}
						else{// if(session.getId().toString()==null){
							userJB.setUserLoginFlag("0");
							userJB.setUserCurrntSsID("");
						}
						userJB.updateUser();
					}
				}
				
				
			} catch(NullPointerException ne){
				 Rlog.debug("Session",
		                    "In sessionDestroyed Listener Session Object Not Found" );
			}catch(ClassCastException cse){
				 Rlog.debug("user",
						 "In sessionDestroyed Listener UserJB Object Not Found in Session " );
			}
		}
	}
}

