package com.velos.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;

import com.velos.eres.barcode.VBarcode;
import com.velos.eres.business.common.LabelDao;
import com.velos.eres.business.common.ProvisoMapDao;
import com.velos.eres.business.common.ReportDaoNew;
import com.velos.eres.business.common.SpecimenStatusDao;
import com.velos.eres.service.util.Configuration;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.Security;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.codelst.CodelstJB;
import com.velos.eres.web.patProt.PatProtJB;
import com.velos.eres.web.person.PersonJB;
import com.velos.eres.web.specimen.SpecimenJB;
import com.velos.eres.web.specimenStatus.SpecimenStatusJB;
import com.velos.eres.web.storage.StorageJB;
import com.velos.eres.web.study.StudyJB;

@SuppressWarnings("serial")
public class PdfController extends HttpServlet {
    private FopFactory fopFactory = FopFactory.newInstance();
    private TransformerFactory tFactory = TransformerFactory.newInstance();
    private static String staticImagePath = null;
    private static String tempImagePath = null; 
    
    public void init() {
        Configuration.readReportParam(Configuration.ERES_HOME + "eresearch.xml");
        Configuration.readSettings();
        tempImagePath = Configuration.getTempImagePath();
        staticImagePath = Configuration.getStaticImagePath();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) 
           throws IOException, ServletException {

        // Security check - Make sure the session in request has same ID as the ID stored in cookies
        if (null == request.getHeader("referer")) {
            showForbiddenMessage(response);
            return;
        }
        Cookie[] cookies = request.getCookies();
        if (cookies == null) {
            showForbiddenMessage(response);
            return;
        }
        for (int iX=0; iX<cookies.length; iX++) {
            if ("JSESSIONID".equals(cookies[iX].getName())) {
                String JSessID = cookies[iX].getValue();
                if(JSessID.contains(".")){
            		int dotIndex = JSessID.indexOf(".");
            		JSessID = JSessID.substring(0, dotIndex);
	            	if (!JSessID.equals(request.getSession(true).getId())) {
	                	showForbiddenMessage(response);
	                    break;
	                }
            	}else{
	                if (!JSessID.equals(request.getSession(true).getId())) {
	                    showForbiddenMessage(response);
	                    return;
	                }
	                break;
            	}
            }
        }
        if (request.getParameter("key") == null) {
            showForbiddenMessage(response);
            return;
        }
        String assembledKey = "";
        try {
            String incomingKey = request.getParameter("key");
            String [] chunks = new String[incomingKey.length() / 3];
            int index = 0;
            for (int iX=0; iX+2<incomingKey.length(); ) {
                chunks[index++] = incomingKey.substring(iX,iX+3);
                iX = iX + 3;
            }
            char[] chs = new char[chunks.length];
            for (int iX=0; iX<chs.length; iX++) {
                chs[iX] = (char)Integer.parseInt(chunks[iX]);
            }
            assembledKey = new String(chs);
        } catch(Exception e) {
            Rlog.fatal("fileDownload", "Exception "+e);
        }
        String sessId = Security.decrypt(assembledKey);
        if ("error".equals(sessId)) {
            showForbiddenMessage(response);
            return;
        }
        if (!request.getSession().getId().startsWith(sessId)) {
            showForbiddenMessage(response);
            return;
        }
        // -- End of security check

        if (staticImagePath == null ||
                tempImagePath == null) {
            Configuration.readReportParam(Configuration.ERES_HOME + "eresearch.xml");
            Configuration.readSettings();
            tempImagePath = Configuration.getTempImagePath();
            staticImagePath = Configuration.getStaticImagePath();
        }

        String foText = null;
        // Our main processing method
        try {
            foText = processFoText(request);
        } catch(Exception e) {
            Rlog.fatal("pdf", "Error occurred while generating PDF "+e);
        }
        if (foText == null) {
            showErrorMessage(response);
            return;
        }

        try {
            // Generate PDF from FO
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, out);
            Transformer transformer = tFactory.newTransformer();
            Source src = new StreamSource(new StringReader(foText));
            Result res = new SAXResult(fop.getDefaultHandler());
            transformer.transform(src, res);

            // Prepare response
            response.setHeader("Expires", "0");
            response.setHeader("Cache-Control",
              "must-revalidate, post-check=0, pre-check=0");
            response.setHeader("Pragma", "public");
            response.setContentType("application/pdf");
            response.setContentLength(out.size());
            
            // Send content to Browser
            response.getOutputStream().write(out.toByteArray());
            response.getOutputStream().flush();
            
        } catch (Exception ex) {
            Rlog.fatal("pdf", "Error occurred while generating PDF "+ex);
            showErrorMessage(response);
        }
        
    }

    private String processFoText(HttpServletRequest request)
            throws Exception {
        String labelType = request.getParameter("labelType");
        String repIdStr = StringUtil.htmlEncodeXss(request.getParameter("repId"));
        String output = null;
        if (labelType != null && labelType.startsWith("proviso")) {
            output = processProviso(request);
        } else if (labelType != null && labelType.startsWith("storage")) {
            output = processStorage(request);
        } else if (repIdStr != null) {
            output = processReportLabels(request);
        } else {
            output = processSpecimen(request);
        }
        return output;    
    } 
        
    private String processReportLabels(HttpServletRequest request)
    throws Exception {
        String inParam = request.getParameter("repId");
        String[] inParams = null;
        if (inParam != null) {
            inParams = inParam.split("\\|");
        }
        if (inParams == null) {
            inParams = new String[1];
            inParams[0] = "0";
        }
        String[] paramKeys = new String[inParams.length-1];
        String[] paramValues = new String[inParams.length-1];
        for (int iX=1; iX<inParams.length; iX++) {
            String[] tmps = inParams[iX].split("=");
            paramKeys[iX-1] = "" + tmps[0];
            paramValues[iX-1] = "" + tmps[1];
        }
        int repId = EJBUtil.stringToNum(inParams[0]);
        try {
            ReportDaoNew rDao = new ReportDaoNew();
            rDao.setFilterKey(paramKeys);
            rDao.setFilterValue(paramValues);
            int success = rDao.createReport(repId, 0);
            ArrayList repXml = rDao.getRepXml();
            String xml = null;
            if (repXml != null && repXml.size() > 0) {
                xml = (String)repXml.get(0);
                if (xml == null) { xml = ""; }
                xml = StringUtil.replace(xml,"&amp;#","&#");
            } else {
                Rlog.fatal("PDF","Error in getting XML");
            }

            // Merge Report XML and XSL to produce XSL-FO
            rDao.getRepXsl(repId);
            ArrayList xslList = rDao.getXsls();
            if (xslList == null || xslList.size() == 0) { return null; }
            return transformXML(xml, ((String)xslList.get(0)).replaceAll("\\{VEL_IMAGE\\}", staticImagePath.replaceAll("\\\\", "/")));
        } catch(Exception e) {
            throw e;
        }
    }
    
    private String processProviso(HttpServletRequest request)
    throws Exception {
        try {
        	int fk_submission = EJBUtil.stringToNum(request.getParameter("submissionPK"));
        	int fk_submission_board =EJBUtil.stringToNum(request.getParameter("submissionBoardPK"));
        	int fk_submission_status = EJBUtil.stringToNum(request.getParameter("pksubmissionStatus"));
        	 ProvisoMapDao pDao = new ProvisoMapDao();
        	int fk_submission_stat = pDao.getFKSubmissionStatus(fk_submission,fk_submission_board,fk_submission_status);
        	
        	
        	
            String[] paramKeys = {"submissionPK", "submissionBoardPK","pksubmissionStatus","fk_submission_stat"};
            String[] paramValues = new String[4];
            paramValues[0] = ""+EJBUtil.stringToNum(request.getParameter("submissionPK"));
            paramValues[1] = ""+EJBUtil.stringToNum(request.getParameter("submissionBoardPK"));
            paramValues[2] = ""+EJBUtil.stringToNum(request.getParameter("pksubmissionStatus"));
            paramValues[3] = ""+fk_submission_stat ;
            
            String accountId = ""+EJBUtil.stringToNum(request.getParameter("accountId"));
            String fkReviewBoard = ""+EJBUtil.stringToNum(request.getParameter("fkReviewBoard"));
            String reviewType = ""+EJBUtil.stringToNum(request.getParameter("reviewType"));
            String submissionStatus = ""+EJBUtil.stringToNum(request.getParameter("submissionStatus"));
            
           
            final int provisoReport = pDao.getFKReport(accountId, fkReviewBoard, 
                    reviewType, submissionStatus);
            ReportDaoNew rDao = new ReportDaoNew();
            rDao.setFilterKey(paramKeys);
            rDao.setFilterValue(paramValues);
            int success = rDao.createReport(provisoReport, 0);
            ArrayList repXml = rDao.getRepXml();
            String xml = null;
            if (repXml != null && repXml.size() > 0) {
                xml = (String)repXml.get(0);
                if (xml == null) { xml = ""; }
                xml = StringUtil.replace(xml,"&amp;#","&#");
            } else {
                Rlog.fatal("PDF","Error in getting XML");
            }
            // System.out.println("xml="+xml);

            // Merge Report XML and XSL to produce XSL-FO
            rDao.getFoXsl(provisoReport);
            ArrayList xslList = rDao.getXsls();
            if (xslList == null || xslList.size() == 0) { return null; }
            return transformXML(xml, ((String)xslList.get(0)).replaceAll("\\{VEL_IMAGE\\}", staticImagePath.replaceAll("\\\\", "/")));
        } catch(Exception e) {
            Rlog.fatal("pdf","Error in processProviso: "+e);
            throw e;
        }
    }
    
    private String processStorage(HttpServletRequest request)
    throws Exception {
        if (request.getParameter("selPks") == null) {
            return null;
        }
        String templateType = StringUtil.htmlEncodeXss(request.getParameter("templateType"));
        String templatePk = request.getParameter("templatePk");
        if (templateType == null && templatePk == null) {
            templateType = "default";
        }
        LabelDao ldao = new LabelDao();
        String foText = null;
        if (templatePk == null) {
            foText = ldao.getTemplateFormat(templateType);
        } else {
            foText = ldao.getTemplateFormatByPk(EJBUtil.stringToNum(templatePk));
        }
        try {
            String selPks = StringUtil.htmlEncodeXss(request.getParameter("selPks"));
            String[] barcodes = StringUtil.strSplit(selPks, ",", true);
            String endTag = "</fo:page-sequence>";
            int beginIndex = foText.indexOf("<fo:page-sequence");
            int endIndex = foText.indexOf(endTag)+endTag.length();
            String pageSequenceOld = foText.substring(beginIndex, endIndex);
            int numPages = barcodes.length;
            if (numPages == 0) { numPages = 1; }
            StorageJB storageJB = new StorageJB();
            CodelstJB codelstJB = new CodelstJB();
            VBarcode vBarcode = new VBarcode();

            // Generate as many pages as needed and do replacements
            StringBuffer replacementPages = new StringBuffer();
            String pageSequences[] = new String[numPages];
            String date = Long.toHexString((Calendar.getInstance().getTime()).getTime());
            for(int iX=0; iX<barcodes.length; iX++) {
                pageSequences[iX] = "" + pageSequenceOld;
                pageSequences[iX] = pageSequences[iX].replaceAll("initial-page-number=\"1\"", 
                        "initial-page-number=\""+(iX+1)+"\"");
                // Pull info from DB
                storageJB.setPkStorage(Integer.parseInt(barcodes[iX]));
                storageJB.getStorageDetails();
                String storageId = storageJB.getStorageId();
                
                // 06/14/2011  #INVP3.1 @Ankit 
                if(pageSequenceOld.indexOf("{VEL_PDF417_BARCODE}")>0)
                {
                	vBarcode.generatePDF417Barcode(storageId, date);
                }
                else if(pageSequenceOld.indexOf("{VEL_DATA_MATRIX_BARCODE}")>0)
                {
                	vBarcode.generateDataMatrixBarcode(storageId, date);
                }
                else
                {
                	vBarcode.generateBarcode(storageId, date);
                }
                
                String storageTypePK = storageJB.getFkCodelstStorageType();
                String storageTypeDesc = codelstJB.getCodeDescription(EJBUtil.stringToNum(storageTypePK));
                // 06/14/2011  #INVP3.1 @Ankit 
                if(pageSequenceOld.indexOf("{VEL_PDF417_BARCODE}")>0)
                {
                	pageSequences[iX] = pageSequences[iX].replaceAll("\\{VEL_PDF417_BARCODE\\}", tempImagePath.replaceAll("\\\\", "/")+storageId+"_"+date+".jpg");
                }
                else if(pageSequenceOld.indexOf("{VEL_DATA_MATRIX_BARCODE}")>0)
                {
                	pageSequences[iX] = pageSequences[iX].replaceAll("\\{VEL_DATA_MATRIX_BARCODE\\}", tempImagePath.replaceAll("\\\\", "/")+storageId+"_"+date+".jpg");
                }
                else
                {
                	pageSequences[iX] = pageSequences[iX].replaceAll("\\{VEL_BARCODE\\}", tempImagePath.replaceAll("\\\\", "/")+storageId+"_"+date+".jpg");
                }
                replacementPages.append(
                        pageSequences[iX].replaceAll("\\{VEL_IMAGE\\}", staticImagePath.replaceAll("\\\\", "/"))
                        .replaceAll("\\{VEL_STORAGE_TYPE\\}", storageTypeDesc)
                        .replaceAll("\\{VEL_STORAGE_ID\\}", storageId)
                );
            }
            return foText.replace(pageSequenceOld, replacementPages.toString());
        } catch(Exception e) {
            throw e;
        }
    }
    
    private String processSpecimen(HttpServletRequest request)
    throws Exception {
        if (request.getParameter("selPks") == null) {
            return null;
        }
        String templateType = StringUtil.htmlEncodeXss(request.getParameter("templateType"));
        String templatePk = request.getParameter("templatePk");
        if (templateType == null && templatePk == null) {
            templateType = "default";
        }
        LabelDao ldao = new LabelDao();
        String foText = null;
        if (templatePk == null) {
            foText = ldao.getTemplateFormat(templateType);
        } else {
            foText = ldao.getTemplateFormatByPk(EJBUtil.stringToNum(templatePk));
        }
        try {
            String selPks = StringUtil.htmlEncodeXss(request.getParameter("selPks"));
            String[] barcodes = StringUtil.strSplit(selPks, ",", true);
            String endTag = "</fo:page-sequence>";
            int beginIndex = foText.indexOf("<fo:page-sequence");
            int endIndex = foText.indexOf(endTag)+endTag.length();
            String pageSequenceOld = foText.substring(beginIndex, endIndex);
            int numPages = barcodes.length;
            if (numPages == 0) { numPages = 1; }
            SpecimenJB specJB = new SpecimenJB();
            SpecimenStatusJB specStatusJB = new SpecimenStatusJB();
            StorageJB storageJB = new StorageJB();
            CodelstJB codelstJB = new CodelstJB();
            PersonJB personJB = new PersonJB();
            StudyJB studyJB=new StudyJB();
            VBarcode vBarcode = new VBarcode();
            PatProtJB pProtJB = new PatProtJB();

            // Generate as many pages as needed and do replacements
            StringBuffer replacementPages = new StringBuffer();
            String pageSequences[] = new String[numPages];
            String date = Long.toHexString((Calendar.getInstance().getTime()).getTime());
            SpecimenStatusDao specStatusDao = null;

            for(int iX=0; iX<barcodes.length; iX++) {
                pageSequences[iX] = "" + pageSequenceOld;
                pageSequences[iX] = pageSequences[iX].replaceAll("initial-page-number=\"1\"", 
                        "initial-page-number=\""+(iX+1)+"\"");
                // Pull info from DB
                specJB.setPkSpecimen(Integer.parseInt(barcodes[iX]));
                specJB.getSpecimenDetails();
                String specId = StringUtil.trueValue(specJB.getSpecimenId());
                // 06/14/2011  #INVP3.1 @Ankit 
                if(pageSequenceOld.indexOf("{VEL_PDF417_BARCODE}")>0)
                {
                	vBarcode.generatePDF417Barcode(specId, date);
                }
                else if(pageSequenceOld.indexOf("{VEL_DATA_MATRIX_BARCODE}")>0)
                {
                	vBarcode.generateDataMatrixBarcode(specId, date);
                }
                else
                {
                	vBarcode.generateBarcode(specId, date);
                }
                                
                specStatusDao = specStatusJB.getLatestSpecimenStaus(Integer.parseInt(barcodes[iX]));
                ArrayList statusDescArray = specStatusDao.getStatusDesc();
                String latestSpecStatus = null;
                if (statusDescArray != null && statusDescArray.size() > 0) {
                    latestSpecStatus = StringUtil.trueValue((String) statusDescArray.get(0));
                } else {
                    latestSpecStatus = "";
                }
                String patPk = StringUtil.trueValue(specJB.getFkPer());
                String specTypePK = specJB.getSpecType();
                String specTypeDesc = StringUtil.trueValue(codelstJB.getCodeDescription(
                        EJBUtil.stringToNum(specTypePK)));
                String percode = null;

                if(! StringUtil.isEmpty(patPk)) {
                    personJB.setPersonPKId(Integer.parseInt(patPk));
                    personJB.getPersonDetails();
                    percode = StringUtil.trueValue(personJB.getPersonPId());
                    percode=percode.replace("$","\\$");
                    percode=StringEscapeUtils.escapeHtml(percode);
                } else {
                    percode = "";
                    patPk = "";
                }
                
                String specCollDateTime = specJB.getSpecCollDate();
                String specCollDate = null;
                if (!StringUtil.isEmpty(specCollDateTime)) {
                    Date sDate = DateUtil.stringToDate(specCollDateTime);
                    specCollDate = StringUtil.trueValue(DateUtil.dateToString(sDate));
                } else {
                    specCollDate = "";
                }
                String storageId = null;
                if (!StringUtil.isEmpty(specJB.getFkStorage())) {
                    storageJB.setPkStorage(Integer.parseInt(specJB.getFkStorage()));
                    storageJB.getStorageDetails();
                    storageId = StringUtil.trueValue(storageJB.getStorageId());
                } else {
                    storageId = "";
                }
                String specAltId = null;
                if (!StringUtil.isEmpty(specJB.getFkStorage())) {
                    specAltId = StringUtil.trueValue(specJB.getSpecAltId());
                } else {
                    specAltId = "";
                }
                String studynum = "";
                String studyphase = "";
                String phase_desc="";
                String studyPk = StringUtil.trueValue(specJB.getFkStudy());
                if(! StringUtil.isEmpty(studyPk)) {
                    studyJB.setId(Integer.parseInt(studyPk));
                    studyJB.getStudyDetails();
                    studynum = StringUtil.trueValue(studyJB.getStudyNumber());
                    studyphase = StringUtil.trueValue(studyJB.getStudyPhase());
                    phase_desc=StringUtil.trueValue(codelstJB.getCodeDescription(Integer.parseInt(studyphase)));
                }
                String patStudyId = null;
                if (!StringUtil.isEmpty(specJB.getFkStudy()) &&
                        !StringUtil.isEmpty(specJB.getFkPer())) {
                    pProtJB.findCurrentPatProtDetails(EJBUtil.stringToNum(specJB.getFkStudy()), 
                            EJBUtil.stringToNum(specJB.getFkPer()));
                    patStudyId = StringUtil.trueValue(pProtJB.getPatStudyId());
                } else {
                    patStudyId = "";
                }
                String pathologicalStatus = null;
                if (!StringUtil.isEmpty(specJB.getSpecPathologyStat())) {
                    pathologicalStatus = StringUtil.trueValue(codelstJB.getCodeDescription(
                            EJBUtil.stringToNum(specJB.getSpecPathologyStat())));
                } else {
                    pathologicalStatus = "";
                }
                String collectedAmount = null;
                if (!StringUtil.isEmpty(specJB.getSpecBaseOrigQuant())) {
                    collectedAmount = specJB.getSpecBaseOrigQuant();
                    if (!StringUtil.isEmpty(specJB.getSpecBaseOrigQUnit())) {
                        collectedAmount += " " 
                            + StringUtil.trueValue(codelstJB.getCodeDescription(
                                    EJBUtil.stringToNum(specJB.getSpecBaseOrigQUnit())));
                    }
                } else {
                    collectedAmount = "";
                }
                
                // Set specJB to parent from here downward
                String parentSpec = null;
                if (!StringUtil.isEmpty(specJB.getFkSpec())) {
                    specJB.setPkSpecimen(Integer.parseInt(specJB.getFkSpec()));
                    specJB.getSpecimenDetails();
                    parentSpec = StringUtil.trueValue(specJB.getSpecimenId());
                } else {
                    parentSpec = "";
                }
                String specIdEscape =  StringUtil.encodeFileName(specId);
                // Done pulling info; now replace
                
                // 06/14/2011  #INVP3.1 @Ankit 
                if(pageSequenceOld.indexOf("{VEL_PDF417_BARCODE}")>0)
                {
                	pageSequences[iX] = pageSequences[iX].replaceAll("\\{VEL_PDF417_BARCODE\\}", tempImagePath.replaceAll("\\\\", "/")+specIdEscape+"_"+date+".jpg");
                }
                else if(pageSequenceOld.indexOf("{VEL_DATA_MATRIX_BARCODE}")>0)
                {
                	pageSequences[iX] = pageSequences[iX].replaceAll("\\{VEL_DATA_MATRIX_BARCODE\\}", tempImagePath.replaceAll("\\\\", "/")+specIdEscape+"_"+date+".jpg");
                }
                else
                {
                	pageSequences[iX] = pageSequences[iX].replaceAll("\\{VEL_BARCODE\\}", tempImagePath.replaceAll("\\\\", "/")+specIdEscape+"_"+date+".jpg");
                }
                replacementPages.append(
                        pageSequences[iX].replaceAll("\\{VEL_PATCODE\\}", percode)
                        .replaceAll("\\{VEL_STUDY_NUMBER\\}", studynum)
                        .replaceAll("\\{VEL_STUDY_PHASE\\}",phase_desc)
                        .replaceAll("\\{VEL_IMAGE\\}", staticImagePath.replaceAll("\\\\", "/"))
                        .replaceAll("\\{VEL_PAT_PK\\}", patPk)
                        .replaceAll("\\{VEL_SAMPLE_TYPE\\}", specTypeDesc)
                        .replaceAll("\\{VEL_COLL_DATE\\}", specCollDate)
                        .replaceAll("\\{VEL_COLL_DATE_TIME\\}", specCollDateTime)
                        .replaceAll("\\{VEL_LATEST_STAT\\}", latestSpecStatus)
                        .replaceAll("\\{VEL_PARENT_SPEC\\}", parentSpec)
                        .replaceAll("\\{VEL_STORAGE_ID\\}", storageId)
                        .replaceAll("\\{VEL_SPEC_ID\\}", specId)
                        .replaceAll("\\{VEL_SPEC_ALT_ID\\}", specAltId)
                        .replaceAll("\\{VEL_PAT_STUDY_ID\\}", patStudyId)
                        .replaceAll("\\{VEL_PATHOLOGICAL\\}", pathologicalStatus)
                        .replaceAll("\\{VEL_COLL_AMT\\}", collectedAmount)
                );
            }
            return foText.replace(pageSequenceOld, replacementPages.toString());
        } catch(Exception e) {
            throw e;
        }
    }
    
    private String transformXML(String xml, String xsl)
    throws Exception {
        Reader mR = new StringReader(xml);
        Reader sR = new StringReader(xsl);
        Source xmlSource = new StreamSource(mR);
        Source xslSource = new StreamSource(sR);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        TransformerFactory tFactory = TransformerFactory.newInstance();
        Transformer transformer = tFactory.newTransformer(xslSource);
        transformer.transform(xmlSource, new StreamResult(out));
        // System.out.println("XSL-FO="+out.toString("UTF-8"));
        return out.toString("UTF-8");
    }
    
    private void showErrorMessage(HttpServletResponse response) {
        try {
            ServletOutputStream out = response.getOutputStream();
            out.println("<html><head><title></title></head>");
            out.println("<body><h1>An error occurred while generating PDF</h1></body></html>");
            out.flush();
        } catch (IOException e) {
        }
    }

    private void showEmptyMessage(HttpServletResponse response) {
        try {
            ServletOutputStream out = response.getOutputStream();
            out.println("<html><head><title></title></head>");
            out.println("<body><h1>Page not found</h1></body></html>");
            out.flush();
        } catch (IOException e) {
        }
    }

    private void showForbiddenMessage(HttpServletResponse resp) {
        try {
            ServletOutputStream out = resp.getOutputStream();
            out.println("<html><head><title>Forbidden</title></head>");
            out.println("<body>");
            out.println("<h1>Forbidden</h1>");
            out.println("<p>You do not have permission to access this server or file.</p>");
            out.println("</body>");
            out.println("</html>");
            out.flush();
        } catch (IOException e) {
        }
    }
    
}