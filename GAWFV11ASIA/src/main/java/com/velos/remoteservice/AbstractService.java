package com.velos.remoteservice;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.web.common.CommonJB;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;

import com.velos.eres.business.common.CtrlDao;
import com.velos.eres.business.common.SettingsDao;

import com.velos.remoteservice.VelosServiceException;

public abstract class AbstractService implements IRemoteService {
	public static final String REMOTE_SERVICE_LOG_CAT = "com.velos.remoteservice";
	protected int loggedInUserId;
	protected Integer accountPK;
	static URI endpointURI = null;
	
	protected void logAudit(String message){
		System.out.println(message);
		AuditLogger.logAudit(
				this.getClass(), 
				AuditLogger.Severity.NORMAL, 
				endpointURI, 
				loggedInUserId,
				message);
		
	}

	public abstract String getEndpointKey();

	
	public URI getEndpointURI() throws VelosServiceException{
		if (endpointURI == null){
			String endpointControlKey = getEndpointKey();
			Rlog.debug(REMOTE_SERVICE_LOG_CAT, "Getting endpoint url for: " + endpointControlKey);
			

			String endpointStr = getAccountSetting(endpointControlKey);
			if (endpointStr == null || endpointStr.equals("")){
				throw new VelosServiceException("endpoint String for " + 
												endpointControlKey + 
												" could not be found in Control Table"); 
			}
			try {
				endpointURI = new URI(endpointStr);
			} catch (URISyntaxException e) {
				throw new VelosServiceException(
						"endpoint String from Control Table cannot be converted to URI");
			}
		}
		return endpointURI;
	}
	
	/**
	 * Retrieves a setting from the er_settings table, assuming this is an account
	 * setting, and that the account corresponds to the previously set accountPK 
	 * @param key
	 * @return
	 */
	protected String getAccountSetting(String key){
		if (accountPK == null){
			throw new VelosServiceException("getAccountSetting called, but account not set in AbstractService");
		}
		CommonJB commonBean = new CommonJB();
		SettingsDao settingsDao = 
			commonBean.retrieveSettings(accountPK, 1);
		int x = 0;
		for (Object settingKey : settingsDao.getSettingKeyword()){
			if(key.equalsIgnoreCase((String)settingKey)){
				return
					(String)settingsDao.getSettingValue().get(x);
			}
			x++;
		}
		return null;

	}

	/**
	 * Retrieves a value from the control table
	 * @param controlKey
	 * @return
	 * @throws VelosServiceException
	 */
	protected String getControlValue(String controlKey)
		throws VelosServiceException{
		String value = "";
		try{
			CtrlDao ctrl = new CtrlDao();
			value = ctrl.getControlValue(controlKey);
		}
		catch(Throwable t){
			throw new VelosServiceException("Error getting control value", t);
		}
		return value;
		
	}
	
	public int getAccountPK(){
		return accountPK;
	}
	
	public void setAccountPK(int accountPK){
		this.accountPK = accountPK;
	}
	public void setLoggedInUserId(int loggedInUserId){
		this.loggedInUserId = loggedInUserId;
	}
}
