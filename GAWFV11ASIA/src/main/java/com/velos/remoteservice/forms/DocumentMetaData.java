/**
 * 
 */
package com.velos.remoteservice.forms;

import java.net.URI;
import java.util.Date;
import java.util.HashMap;



/**
 * Class for storing information about a document. This class does not
 * contain the content itself. In general, data from a list of DocuemntMetaData 
 * instances will be presented to a user as a list, and the user will use that data
 * to select a document for retrieval.
 * 
 * @author dylan
 *
 */
public class DocumentMetaData {
	private String name;
	private String description;
	private String type;
	private URI location;
	private Date lastRevisionDate;
	private String revisionLabel;
	private HashMap<String, Object> customProperties;
	private String contentType;
	
	/**
	 * 
	 * @param name The name of the document.
	 * @param description A description of the document
	 * @param contentType MIME encoding type of the docuemnt. (e.g. "application/pdf")
	 * @param type A string identifiying the type of document
	 * @param location URI identifying the location of the document.
	 * @param lastRevisionDate Lastest (current) revision date of the document
	 * @param revisionLabel Latest (current) revision label of the document.
	 * @param contentType Mimetype of the document
	 * @param customProperties map of custom properties that can be added to this instance
	 */
	public DocumentMetaData(
			String name, 
			String description, 
			String contentType,
			String type,
			URI location, 
			Date lastRevisionDate, 
			String revisionLabel,
			HashMap<String, Object> customProperties) {
		
		super();
		this.name = name;
		this.description = description;
		this.contentType = contentType;
		this.type = type;
		this.location = location;
		this.lastRevisionDate = lastRevisionDate;
		this.revisionLabel = revisionLabel;
		this.customProperties = customProperties;
	}
	
	/**
	 * The name of the document.
	 * @return
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * A description of the document
	 * @return
	 */
	public String getDescription() {
		return description;
	}
	
	public String getContentType(){
		return contentType;
	}
	
	/**
	 * A string identifiying the type of document
	 * @return
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * URI identifying the location of the document.
	 * See http://www.ietf.org/rfc/rfc3986.txt Note
	 * that URIs must contain a scheme and path. The scheme 
	 * and the path can be customized. Keep in mind that this field
	 * will be used by a plugin at various points to retrieve a document.
	 * You can make up your own format for scheme and location, but 
	 * you need to be able to parse them. 
	 * @return
	 */
	public URI getLocation() {
		return location;
	}
	
	/**
	 * Lastest (current) revision date of the document
	 * @return
	 */
	public Date getLastRevisionDate() {
		return lastRevisionDate;
	}
	
	/**
	 * Latest (current) revision label of the document. Not
	 * all remote systems will store revision labels, but
	 * it's here if you need it.
	 * @return
	 */
	public String getRevisionLabel() {
		return revisionLabel;
	}

	
	public HashMap<String, Object> getCustomProperties(){
		return customProperties;
	}
}
