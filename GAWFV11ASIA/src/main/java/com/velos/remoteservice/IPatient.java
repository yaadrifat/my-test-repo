package com.velos.remoteservice;

import java.util.Date;

@Deprecated
public interface IPatient {

//	/** select * from er_codelst where codelst_type = 'gender' */
//	public enum Gender {MALE, FEMALE, UNKNOWN, OTHER}
//	
//	/** select *  from er_codelst where codelst_type = 'ethnicity'  */
//	public enum Ethnicity{
//		NONHISPANIC (7107), 
//		NOTREPORTED (7108), 
//		HISPANIC (412), 
//		UNKNOWN (6201);
//		
//	
//		private int pk_codelst;
//		
//		Ethnicity(int pk_codelst){
//			this.pk_codelst = pk_codelst;
//		}
//	}
//	
//	/** select * from er_codelst where codelst_type = 'race' */
//	public enum Race{ASIAN, WHITE, NOT_REP, INDALA, BLKAFR, HWNISL, UNKNOWN}
//	
//	/** select *  from er_codelst where codelst_type='ptst_survival'  */
//	public enum SurvivalStatus {ALIVE_NED, ALIVE_DISEASE, ALIVE_DISALIVE, DEAD, LOST_FOLLOWUP}
//	
	public String getFirstName();
	
	public String getLastName();
	
	public String getGender();
	
	public String getRace();
	
	public String getEthnicity();
	
	public String getPrimaryFacility();
	
	public Date getDOB();
}
