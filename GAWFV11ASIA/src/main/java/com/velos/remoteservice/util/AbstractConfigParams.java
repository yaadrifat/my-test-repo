/**
 * 
 */
package com.velos.remoteservice.util;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dylan
 *
 */
public abstract class AbstractConfigParams {
	private static Map<String, String> params = new HashMap<String, String>();
	
	/**
	 * Reads parameters from the various sources of parameter. A particular
	 * parameter could be defined in a parameter file, the database, java system
	 * parameters or environment variables. This method will investigate each of these
	 * sources.
	 * 
	 * If a parameter is defined in more than one, it will be overridden by the 
	 * later source. Precedence order is:
	 * <ol>
	 * 	<li>file</li>
	 * 	<li>database</li>
	 * 	<li>system</li>
	 * 	<li>environment<li>
	 * </ol>
	 * 
	 * So, if the parameter named "foo" is defined the database and in the enviornment,
	 * the value from the environment will be populated.
	 */
	protected void setAllParams(){
		readFile(params);
		readDB(params);
		readSystem(params);
		readEnvironment(params);
	}
	
	public static String getParam(String key){
		return params.get(key);
	}
	
	protected  void readFile(Map<String, String> params){
		
	}
	
	protected  void readDB(Map<String, String> params){
		
	}
	
	protected  void readSystem(Map<String, String> params){
		
	}
	
	protected  void readEnvironment(Map<String, String> params){
		
	}
}
