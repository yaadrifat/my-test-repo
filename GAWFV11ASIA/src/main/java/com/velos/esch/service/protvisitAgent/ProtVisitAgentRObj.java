/*
 * Classname : ProtVisitAgentRObj
 *
 * Version information: 1.0
 *
 * Date: 09/22/2004
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Sam Varadarajan
 */

package com.velos.esch.service.protvisitAgent;

/* Import Statements */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import javax.ejb.Remote;

import com.velos.esch.business.common.ProtVisitDao;
import com.velos.esch.business.common.ScheduleDao;
import com.velos.esch.business.protvisit.impl.ProtVisitBean;
import com.velos.esch.web.protvisit.ProtVisitResponse;

/* End of Import Statements */

/**
 * Remote interface for ProtVisitAgent session EJB
 *
 * @author Sam Varadarajan
 */
@Remote
public interface ProtVisitAgentRObj {

    ProtVisitBean getProtVisitDetails(int visitId);

    public int setProtVisitDetails(ProtVisitBean vdsk);

    public int updateProtVisit(ProtVisitBean vdsk);

    public int removeProtVisit(int visit_id);
    
    public int removeProtVisit(int visit_id,Hashtable<String, String> args);

    public ProtVisitDao getProtocolVisits(int protocol_id);
    public ProtVisitDao getProtocolVisits(int protocol_id,int initVisitSize,int limitVisitSize);
    public ProtVisitDao getProtocolVisits(int protocol_id, String search);//JM: 16Apr2008

    //This method is not used anymore. Bug #6631
    public int pushNoIntervalVisitsOut(int protocol_visit_id, String ip, int userId);
    public int generateRipple(int protocol_visit_id,String calledFrom);
    public int getMaxVisitNo(int protocol_id);

    // public int DeleteProtocolVisits(String chain_id) ;
    // public EventdefDao getVisitEvents(int patProtId,int visit) throws
    // RemoteException;
    
    // Added by Manimaran for the Enhancement #C8.3
    public int copyVisit(int protocolId,int frmVisitId, int visitId,String calledFrom, String usr, String ipAdd);
    public ScheduleDao getVisitsByMonth(int patProdId, HashMap paramMap);
    //BK,May/20/2011,Fixed #6014
    /**
     * This method validates the duration against day zero visits in the calendar.
     * @param duration The Calendar duration
     * @param protocolId Calendar Identifier
     * @param checkDayZero flag for day zero check.
     *        1-Check for day zero visit.
     *        2-Check for last day visit.
     * @param visitId visit Identifier
     * @return integer 
     */
    public int validateDayZeroVisit(int duration,int protocolId,int checkDayZero,int visitId);

    public ProtVisitResponse validateVisits(String dataGridString,String UpdateString,ProtVisitBean psk,int duration,String calStatus,
    	    String calType,String deleteString,Hashtable<String, String> args);    //Ak:Fixed BUG#6995
    
    /** BK PCAL 20071
     * This method add and modify visits in a calendar.
     **/    
    
    //Modified for INF-18183 and for BUG#7224 : Raviesh
    public ProtVisitResponse manageVisits(String dataGridString,String UpdateString,ProtVisitBean psk,int duration,String calStatus,
    	    String calType,String deleteString,Hashtable<String, String> args);    //Ak:Fixed BUG#6995
    
    public void copyDisplacementsFromVisitsToEvents(ArrayList<Integer> updatedVisitList, int protocolId, String calType);
    public void updateFirstAndPreviousVisit(String dataGridString,String UpdateString,ProtVisitBean psk,int duration,String calStatus,
    	    String calType,String deleteString,Hashtable<String, String> args);

}