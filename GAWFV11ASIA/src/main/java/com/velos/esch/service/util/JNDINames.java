/*
 * $Id: JNDINames.java,v 1.10 2016/11/02 09:31:06 eperumal Exp $
 * Copyright 2000 Velos, Inc. All rights reserved.
 */

package com.velos.esch.service.util;

/**
 * This class is to be used as a central repository to store the Internal JNDI
 * names of various components. Any sort of change at this location should also
 * reflect in the Deployment Descriptor
 */

public interface JNDINames {
	
	public final static String prefixEsch = "ejb:";

	public final static String AdvEveAgentHome = prefixEsch+"velos/velos-ejb-esch/AdvEveAgentBean!com.velos.esch.service.advEveAgent.AdvEveAgentRObj";
	public final static String AdvInfoAgentHome = prefixEsch+"velos/velos-ejb-esch/AdvInfoAgentBean!com.velos.esch.service.advInfoAgent.AdvInfoAgentRObj";
	public final static String AdvNotifyAgentHome = prefixEsch+"velos/velos-ejb-esch/AdvNotifyAgentBean!com.velos.esch.service.advNotifyAgent.AdvNotifyAgentRObj";
	public final static String AlertNotifyAgentHome = prefixEsch+"velos/velos-ejb-esch/AlertNotifyAgentBean!com.velos.esch.service.alertNotifyAgent.AlertNotifyAgentRObj";
	public final static String BgtApndxAgentHome = prefixEsch+"velos/velos-ejb-esch/BgtApndxAgentBean!com.velos.esch.service.bgtApndxAgent.BgtApndxAgentRObj";
	public final static String BgtSectionAgentHome = prefixEsch+"velos/velos-ejb-esch/BgtSectionAgentBean!com.velos.esch.service.bgtSectionAgent.BgtSectionAgentRObj";
	public final static String BudgetAgentHome = prefixEsch+"velos/velos-ejb-esch/BudgetAgentBean!com.velos.esch.service.budgetAgent.BudgetAgentRObj";
	public final static String BudgetcalAgentHome = prefixEsch+"velos/velos-ejb-esch/BudgetcalAgentBean!com.velos.esch.service.budgetcalAgent.BudgetcalAgentRObj";
	public final static String BudgetUsersAgentHome = prefixEsch+"velos/velos-ejb-esch/BudgetUsersAgentBean!com.velos.esch.service.budgetUsersAgent.BudgetUsersAgentRObj";
	public final static String CrfAgentHome = prefixEsch+"velos/velos-ejb-esch/CrfAgentBean!com.velos.esch.service.crfAgent.CrfAgentRObj";
	public final static String CrflibAgentHome = prefixEsch+"velos/velos-ejb-esch/CrflibAgentBean!com.velos.esch.service.crflibAgent.CrflibAgentRObj";
	public final static String CrfNotifyAgentHome = prefixEsch+"velos/velos-ejb-esch/CrfNotifyAgentBean!com.velos.esch.service.crfNotifyAgent.CrfNotifyAgentRObj";
	public final static String CrfStatAgentHome = prefixEsch+"velos/velos-ejb-esch/CrfStatAgentBean!com.velos.esch.service.crfStatAgent.CrfStatAgentRObj";
	public final static String DocrefAgentHome = prefixEsch+"velos/velos-ejb-esch/DocrefAgentBean!com.velos.esch.service.docrefAgent.DocrefAgentRObj";
	public final static String EventAssocAgentHome = prefixEsch+"velos/velos-ejb-esch/EventAssocAgentBean!com.velos.esch.service.eventassocAgent.EventAssocAgentRObj";
	public final static String EventcostAgentHome = prefixEsch+"velos/velos-ejb-esch/EventcostAgentBean!com.velos.esch.service.eventcostAgent.EventcostAgentRObj";
	public final static String EventCrfAgentHome = prefixEsch+"velos/velos-ejb-esch/EventCrfAgentBean!com.velos.esch.service.eventCrfAgent.EventCrfAgentRObj";
	public final static String EventdefAgentHome = prefixEsch+"velos/velos-ejb-esch/EventdefAgentBean!com.velos.esch.service.eventdefAgent.EventdefAgentRObj";
	public final static String EventdocAgentHome = prefixEsch+"velos/velos-ejb-esch/EventdocAgentBean!com.velos.esch.service.eventdocAgent.EventdocAgentRObj";
	public final static String EventKitAgentHome = prefixEsch+"velos/velos-ejb-esch/EventKitAgentBean!com.velos.esch.service.eventKitAgent.EventKitAgentRObj";
	public final static String EventResourceAgentHome = prefixEsch+"velos/velos-ejb-esch/EventResourceAgentBean!com.velos.esch.service.eventresourceAgent.EventResourceAgentRObj";
	public final static String EventStatAgentHome = prefixEsch+"velos/velos-ejb-esch/EventStatAgentBean!com.velos.esch.service.eventstatAgent.EventStatAgentRObj";
	public final static String EventuserAgentHome = prefixEsch+"velos/velos-ejb-esch/EventuserAgentBean!com.velos.esch.service.eventuserAgent.EventuserAgentRObj";
	public final static String LineitemAgentHome = prefixEsch+"velos/velos-ejb-esch/LineitemAgentBean!com.velos.esch.service.lineitemAgent.LineitemAgentRObj";
	public final static String ProtVisitAgentHome = prefixEsch+"velos/velos-ejb-esch/ProtVisitAgentBean!com.velos.esch.service.protvisitAgent.ProtVisitAgentRObj";
	public final static String PortalFormsAgentHome = prefixEsch+"velos/velos-ejb-esch/PortalFormsAgentBean!com.velos.esch.service.portalFormsAgent.PortalFormsAgentRObj";
	public final static String SubCostItemAgentHome = prefixEsch+"velos/velos-ejb-esch/SubCostItemAgentBean!com.velos.esch.service.subCostItemAgent.SubCostItemAgentRObj";
	public final static String SubCostItemVisitAgentHome = prefixEsch+"velos/velos-ejb-esch/SubCostItemVisitAgentBean!com.velos.esch.service.subCostItemVisitAgent.SubCostItemVisitAgentRObj";
	public final static String AuditRowEschAgentHome = prefixEsch+"velos/velos-ejb-esch/AuditRowEschAgentBean!com.velos.esch.audit.service.AuditRowEschAgent";


    /* JNDI names of EJB home objects */
    public static final String EVENTDEFAGENT_EJBHOME = "EventdefAgentBean";

    public static final String EVENTDEF_EJBHOME = "EventdefBean";

    // public static final String SCHEDULEAGENT_EJBHOME = "ScheduleAgentBean";
    // public static final String SCHEDULE_EJBHOME = "ScheduleBean";
    public static final String EVENTCOSTAGENT_EJBHOME = "EventcostAgentBean";

    public static final String EVENTCOST_EJBHOME = "EventcostBean";

    public static final String EVENTASSOCAGENT_EJBHOME = "EventAssocAgentBean";

    public static final String EVENTASSOC_EJBHOME = "EventAssocBean";

    public static final String EVENTUSERAGENT_EJBHOME = "EventuserAgentBean";

    public static final String EVENTUSER_EJBHOME = "EventuserBean";

    public static final String EVENTDOCAGENT_EJBHOME = "EventdocAgentBean";

    public static final String EVENTDOC_EJBHOME = "EventdocBean";

    public static final String CRFAGENT_EJBHOME = "CrfAgentBean";

    public static final String CRF_EJBHOME = "CrfBean";

    public static final String ADVEVEAGENT_EJBHOME = "AdvEveAgentBean";

    public static final String ADVEVE_EJBHOME = "AdvEveBean";

    public static final String ADVINFOAGENT_EJBHOME = "AdvInfoAgentBean";

    public static final String ADVINFO_EJBHOME = "AdvInfoBean";

    public static final String ADVNOTIFYAGENT_EJBHOME = "AdvNotifyAgentBean";

    public static final String ADVNOTIFY_EJBHOME = "AdvNotifyBean";

    public static final String CRFNOTIFYAGENT_EJBHOME = "CrfNotifyAgentBean";

    public static final String CRFNOTIFY_EJBHOME = "CrfNotifyBean";

    public static final String ALERTNOTIFYAGENT_EJBHOME = "AlertNotifyAgentBean";

    public static final String ALERTNOTIFY_EJBHOME = "AlertNotifyBean";

    public static final String CRFSTATAGENT_EJBHOME = "CrfStatAgentBean";

    public static final String CRFSTAT_EJBHOME = "CrfStatBean";

    public static final String CRFLIBAGENT_EJBHOME = "CrflibAgentBean";

    public static final String CRFLIB_EJBHOME = "CrflibBean";

    public static final String BUDGETAGENT_EJBHOME = "BudgetAgentBean";

    public static final String BUDGET_EJBHOME = "BudgetBean";

    public static final String BUDGETCALAGENT_EJBHOME = "BudgetcalAgentBean";

    public static final String BUDGETCAL_EJBHOME = "BudgetcalBean";

    public static final String BUDGETUSERSAGENT_EJBHOME = "BudgetUsersAgentBean";

    public static final String BUDGETUSERS_EJBHOME = "BudgetUsersBean";

    public static final String BGTSECTIONAGENT_EJBHOME = "BgtSectionAgentBean";

    public static final String BGTSECTION_EJBHOME = "BgtSectionBean";

    public static final String BGTAPNDXAGENT_EJBHOME = "BgtApndxAgentBean";

    public static final String BGTAPNDX_EJBHOME = "BgtApndxBean";

    public static final String DOCREF_EJBHOME = "DocrefBean";

    public static final String ESCHEDULING_DATASOURCE = "java:comp/env/esch";

    public static final String LINEITEMAGENT_EJBHOME = "LineitemAgentBean";

    public static final String LINEITEM_EJBHOME = "LineitemBean";

    public static final String PROTVISIT_EJBHOME = "ProtVisitBean"; // SV,

    // 9/22/2004

    public static final String PROTVISITAGENT_EJBHOME = "ProtVisitAgentBean"; // SV,
    // 9/22/2004

//  Added by Manimaran on 26,June2006
    public static final String EVENTCRFAGENT_EJBHOME = "EventCrfAgentBean";


    //JM: 15Feb2008
    public static final String EVENTRESOURCEAGENT_EJBHOME = "EventResourceAgentBean";

    public static final String EVENTRESOURCE_EJBHOME = "EventResourceBean";

    public static final String EVENTSTAT_EJBHOME = "EventStatBean"; //JM: 28Aug2008

    public static final String PORTALFORMSAGENT_EJBHOME = "PortalFormsBean"; //JM: 03DEC2010
}