package com.velos.esch.service.docrefAgent.impl;

import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.velos.esch.business.docref.impl.DocrefBean;
import com.velos.esch.service.docrefAgent.DocrefAgentRObj;

@Stateless
public class DocrefAgentBean implements DocrefAgentRObj {

    @PersistenceContext(unitName = "esch")
    protected EntityManager em;

    public int setDocRefDetails(DocrefBean docrefBean) {
        em.merge(docrefBean);
        return 0;
    }

    public int remove(DocrefBean docref) {
        em.remove(docref);
        return 0;
    }

    public int remove(int docrefId) {

        int output = 0;
        try {
            Query querySiteIdentifier = em.createNamedQuery("findEventdocId");
            querySiteIdentifier.setParameter("pkDocs", new Integer(docrefId));
            ArrayList list = (ArrayList) querySiteIdentifier.getResultList();
            if (list == null)
                list = new ArrayList();
            System.out
                    .println("SiteAgentBean.setSiteDetails SiteStateKeeper:enum_velos "
                            + list);
            // DocrefAgentRObj docref = EJBUtil.getDocrefAgentHome();

            for (int i = 0; i < list.size(); i++) {
                em.remove((DocrefBean) list.get(i));
            }
            // em.remove(docref);
            return 0;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return -2;
        }

    }

}
