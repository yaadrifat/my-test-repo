/* 
 * Classname			LineitemAgentBean.class
 * 
 * Version information
 *
 * Date					03/28/2002
 *
 * Author 				Sajal
 * Copyright notice
 */

package com.velos.esch.service.lineitemAgent.impl;

/* IMPORT STATEMENTS */

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.eres.service.util.NumberUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.esch.business.common.LineitemDao;
import com.velos.esch.business.lineitem.impl.LineitemBean;
import com.velos.esch.service.lineitemAgent.LineitemAgentRObj;
import com.velos.esch.service.util.Rlog;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.esch.business.audit.impl.AuditBean;
import com.velos.eres.service.util.DateUtil;
import javax.annotation.Resource;
import javax.ejb.SessionContext;

/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the entity CMP LineitemBean.
 * 
 */
@Stateless
public class LineitemAgentBean implements LineitemAgentRObj {
    @PersistenceContext(unitName = "esch")
    protected EntityManager em;
    @Resource private SessionContext context;

    /**
     * Calls getLineitemDetails() on Lineitem Entity Bean. Looks up on the
     * Lineitem Id parameter passed in and constructs and returns a Lineitem
     * state keeper. containing all the summary details of the Lineitem<br>
     * 
     * @param LineitemId
     *            the Lineitem Id
     * @see LineitemBean
     */
    public LineitemBean getLineitemDetails(int lineitemId) {

        try {
            return (LineitemBean) em.find(LineitemBean.class, new Integer(
                    lineitemId));

        } catch (Exception e) {
            Rlog.debug("Lineitem",
                    "Error in getLineitemDetails() in LineitemAgentBean" + e);
            return null;
        }

    }

    /**
     * Calls setLineitemDetails() on Budget Appendix Entity Bean
     * BudgetBean.Creates a new Budget Appendix with the values in the
     * StateKeeper object.
     * 
     * @param budget
     *            appendix state keeper containing budget appendix attributes
     *            for the new Budget appendix.
     * @return int - pkLineItem if successful; -2 for Exception;
     */

    public int setLineitemDetails(LineitemBean lsk) {
        int ret = 0;
        try {
            em.merge(lsk);
            ret = lsk.getLineitemId();
        } catch (Exception e) {
            Rlog.debug("Lineitem",
                    "Error in setLineitemDetails() in LineitemAgentBean" + e);
            return -2;
        }
        return ret;

    }

    public int updateLineitem(LineitemBean lsk) {

        LineitemBean retrieved = null;
        int output;

        try {

            retrieved = em.find(LineitemBean.class, lsk.getLineitemId());

            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updateLineitem(lsk);

        } catch (Exception e) {
            Rlog.debug("Lineitem",
                    "Error in updateLineitem in LineitemAgentBean " + e);
            return -2;
        }
        return output;
    }
    
    // Overloaded for INF-18183 ::: Akshi
    public int updateLineitem(LineitemBean lsk,Hashtable<String, String> args) {

        LineitemBean retrieved = null;
        int output;

        try {
        	
            retrieved = em.find(LineitemBean.class, lsk.getLineitemId());

            if (retrieved == null) {
                return -2;
            }
            
            AuditBean audit=null;
            String currdate =DateUtil.getCurrentDate();
            String userID=(String)args.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
            String ipAdd=(String)args.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
            String reason=(String)args.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
            String appmodule=(String)args.get(AuditUtils.APP_MODULE); /*Fetches the application module name from the Hashtable*/
            String getRidValue= AuditUtils.getRidValue("SCH_LINEITEM","esch","PK_LINEITEM="+lsk.getLineitemId());/*Fetches the RID/PK_VALUE*/ 
            audit = new AuditBean("SCH_LINEITEM",String.valueOf(lsk.getLineitemId()),getRidValue,
        			userID,currdate,appmodule,ipAdd,reason);/*POPULATE THE AUDIT BEAN*/ 
            em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
            
            output = retrieved.updateLineitem(lsk);

        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.debug("Lineitem",
                    "Error in updateLineitem in LineitemAgentBean " + e);
            return -2;
        }
        return output;
    }

    public LineitemDao getLineitemsOfBgtcal(int bgtcalId) {
        Rlog.debug("Lineitem", "LineitemJB.getLineitemsOfBgtcal line AAA");
        LineitemDao lineitemDao = new LineitemDao();
        try {

            lineitemDao.getLineitemsOfBgtcal(bgtcalId);
        } catch (Exception e) {
            Rlog.fatal("Lineitem", "LineitemJB.getLineitemsOfBgtcal " + e);
        }
        return lineitemDao;
    }

    /**
     * Saves the line items of Patient Budget
     */
    public int updateAllLineitems(ArrayList lineitemIds, ArrayList discounts,
            ArrayList unitCosts, ArrayList noUnits, ArrayList researchCosts,
            ArrayList stdCareCosts, ArrayList categories,
            ArrayList applyIndirects, String ipAdd, String usr, ArrayList lineitemTotalCost, ArrayList lineItemSponsorAmount, ArrayList lineItemVariance)

    {
        int i = 0;
        int rows = lineitemIds.size();
        int lineitemId = 0;
        String sponsorCost = "";
        String clinicCost = "";
        String otherCost = "";
        LineitemBean lsk = null;
        try {
            for (i = 0; i < rows; i++) {
                lineitemId = StringUtil.stringToNum((String) lineitemIds.get(i));
                lsk = this.getLineitemDetails(lineitemId);
                Rlog.debug("Lineitem", " LineitemAgentbean " + lineitemId);

                lsk.setLineitemInCostDisc((String) discounts.get(i));

                lsk.setLineitemSponsorUnit((String) unitCosts.get(i));

                lsk.setLineitemClinicNOfUnit((String) noUnits.get(i));

                Rlog.debug("Lineitem", " (String) researchCosts.get(i) "
                        + (String) researchCosts.get(i));
                lsk.setLineitemResCost((String) researchCosts.get(i));

                Rlog.debug("Lineitem", " (String) stdCareCosts.get(i) "
                        + (String) stdCareCosts.get(i));
                lsk.setLineitemStdCareCost((String) stdCareCosts.get(i));

                lsk.setLineitemCategory((String) categories.get(i));
                lsk.setLineitemAppIndirects((String) applyIndirects.get(i));

                // lsk.setLineitemInvCost((String) invCosts.get(i));

                // lsk.setLineitemOtherCost((String) otherCosts.get(i));
                
                lsk.setLineitemTotalCost((String) lineitemTotalCost.get(i) );
                
                //Added by IA 11.03.2006
                
                lsk.setLineItemSponsorAmount((String) lineItemSponsorAmount.get(i) );
                
                lsk.setLineItemVariance((String) lineItemVariance.get(i) );

                //end added
                
              //  lsk.setModifiedBy(usr);
                
                //JM: 16Jan2008 : #3010
                lsk.setModifiedBy(usr);
                
                lsk.setIpAdd(ipAdd);
                if (this.updateLineitem(lsk) == -2) {
                    return -2;
                }
            }
        } catch (Exception e) {
            Rlog.debug("Lineitem",
                    "Error in updateAllLineitems in LineitemAgentBean " + e);
            return -2;
        }
        return 0;
    }

    /**
     * Delete section from budget
     * 
     * @return 0 for successful delete, -2 in case of exception
     */

    public int lineitemDelete(int lineitemId) {

        int ret = 0;
        try {

            LineitemBean lineitemR = em.find(LineitemBean.class, new Integer(
                    lineitemId));
            em.remove(lineitemR);
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            Rlog.fatal("lineitem", "EXCEPTION IN REMOVING LINEITEM" + e);
            return -2;
        }
    }

    public int setRepeatLineitems(String[] lineitemNames,
            String[] lineitemDescs, String[] lineitemCpts,
            String[] lineitemRepeatOpts, String[] categories, String[] tmids,
            String[] cdms, String bgtSectionId, String budgetCalId,
            String ipAdd, String usr) {

        LineitemDao lineitemDao = new LineitemDao();
        try {

            lineitemDao.setRepeatLineitems(lineitemNames, lineitemDescs,
                    lineitemCpts, lineitemRepeatOpts, categories, tmids, cdms,
                    bgtSectionId, budgetCalId, ipAdd, usr);

        } catch (Exception e) {
            Rlog.fatal("Lineitem", "LineitemJB.setRepaetLineitems " + e);
            return -1;
        }
        return 0;

        /*
         * Rlog.debug("Lineitem","SetAllLineitems in LineitemAgentBean line 1" +
         * bgtSectionId + "**" + ipAdd + "**" +usr); int i = 0; int rows =
         * lineitemNames.size(); int pkline = 0; String lineitemid = ""; int
         * ilineitemid = 0; ArrayList lineItemIds = new ArrayList(); LineitemDao
         * lineDao = new LineitemDao(); LineitemStateKeeper lsk = new
         * LineitemStateKeeper(); try { for(i=0;i<rows;i++) {
         * lsk.setLineitemName((String) lineitemNames.get(i));
         * lsk.setLineitemDesc((String) lineitemDescs.get(i));
         * lsk.setLineitemCptCode((String) lineitemCpts.get(i));
         * lsk.setLineitemRepeat((String) lineitemRepeatOpts.get(i));
         * lsk.setLineitemBgtSection(bgtSectionId); lsk.setLineitemDelFlag("N");
         * lsk.setLineitemInPerSec("0"); lsk.setModifiedBy(usr);
         * lsk.setIpAdd(ipAdd); ilineitemid = this.setLineitemDetails(lsk); if (
         * ilineitemid == -2){ return -2; } else { lineitemid =
         * Integer.toString(ilineitemid); Rlog.debug("Lineitem","SetAllLineitems
         * lineItemid from deatils" + lineitemid); lineItemIds.add(lineitemid) ; } }
         * Rlog.debug("Lineitem","SetAllLineitems in LineitemAgentBean
         * lineItemIds" + lineItemIds); Rlog.debug("Lineitem","SetAllLineitems
         * in LineitemAgentBean bgtSectionId" + bgtSectionId);
         * Rlog.debug("Lineitem","SetAllLineitems in LineitemAgentBean budgetId" +
         * budgetCalId); Rlog.debug("Lineitem","SetAllLineitems in
         * LineitemAgentBean lineitemRepeatOpts" + lineitemRepeatOpts); pkline =
         * lineDao.insertRepeatLineItems(EJBUtil.stringToNum(budgetCalId),EJBUtil.stringToNum(bgtSectionId),lineItemIds,lineitemRepeatOpts); }
         * catch(Exception e) { Rlog.fatal("Lineitem","Error in setAllLineitems
         * in LineitemAgentBean " + e); return -2; } return 0;
         */
    }

    /**
     * Calls insertPersonnelCost() method in lineitem dao
     * 
     * @param int
     *            containing budget id
     * @param String
     *            Array containing lineitem names
     * @param String
     *            Array containing rates which is inserted in
     *            lineitem_sponsororunit
     * @param String
     *            Array containing lineitems inclusion type, 1- in personnel
     *            cost , 0- in all sections. this is inserted in
     *            lineitem_inpersec column
     * @param String
     *            Array containing notes
     * @param String
     *            containing creator
     * @param String
     *            containing ip address
     * @return lineitem dao;
     */

    public LineitemDao insertPersonnelCost(int budgetCalId, String[] perTypes,
            String[] rates, String[] inclusions, String[] notes,
            String[] categories, String[] tmids, String[] cdms, int creator,
            String ipAdd) {
        Rlog.debug("Lineitem", "LineitemJB.insertPersonnelCost in agentbean");

        LineitemDao lineitemDao = new LineitemDao();
        try {

            lineitemDao.insertPersonnelCost(budgetCalId, perTypes, rates,
                    inclusions, notes, categories, tmids, cdms, creator, ipAdd);

        } catch (Exception e) {
            Rlog.fatal("Lineitem", "LineitemJB.insertPersonnelCost " + e);
        }
        return lineitemDao;
    }

    /**
     * Calls getLineitemsOfDefaultSection() method in lineitem dao
     * 
     * @param int
     *            containing budget Cal id
     * @return lineitem dao;
     */

    public LineitemDao getLineitemsOfDefaultSection(int budgetCalId) {

        LineitemDao lineitemDao = new LineitemDao();
        try {

            lineitemDao.getLineitemsOfDefaultSection(budgetCalId);

        } catch (Exception e) {
            Rlog.fatal("Lineitem", "LineitemJB.getLineitemsOfBgtcal " + e);
        }
        return lineitemDao;
    }

    /**
     * Calls updatePersonnelCost() method in lineitem dao
     * 
     * @param int
     *            containing budget cal id
     * @param String
     *            Array containing default section's lineitem ids
     * @param String
     *            Array containing lineitem names
     * @param String
     *            Array containing rates which is inserted in
     *            lineitem_sponsororunit
     * @param String
     *            Array containing lineitems inclusion type, 1- in personnel
     *            cost , 0- in all sections. this is inserted in
     *            lineitem_inpersec column
     * @param String
     *            Array containing notes
     * @param String
     *            containing creator
     * @param String
     *            containing ip address
     * @return lineitem dao;
     */

    public LineitemDao updatePersonnelCost(int budgetCalId, String[] parentIds,
            String[] personnelTypes, String[] rates, String[] inclusions,
            String[] notes, String[] applyTypes, String[] categories,
            String[] tmids, String[] cdms, int creator, String ipAdd) {

        LineitemDao lineitemDao = new LineitemDao();
        try {

            lineitemDao.updatePersonnelCost(budgetCalId, parentIds,
                    personnelTypes, rates, inclusions, notes, applyTypes,
                    categories, tmids, cdms, creator, ipAdd);

        } catch (Exception e) {
            Rlog.fatal("Lineitem", "LineitemJB.updatePersonnelCost " + e);
        }
        return lineitemDao;
    }

    /**
     * Calls deleteLineitemPerCost() method in lineitem dao
     * 
     * @param int
     *            lineitem id
     * @param int
     *            last modified by
     * @param String
     *            ip address
     * @return lineitem dao;
     */

    public LineitemDao deleteLineitemPerCost(int lineitemId, int applyType,
            int lastModifiedBy, String ipAdd) {
        Rlog.debug("Lineitem", "LineitemJB.agent bean deleteLineitemPerCost");
        LineitemDao lineitemDao = new LineitemDao();
        try {
            lineitemDao.deleteLineitemPerCost(lineitemId, applyType,
                    lastModifiedBy, ipAdd);

        } catch (Exception e) {
            Rlog.fatal("Lineitem", "LineitemJB.deleteLineitemPerCost " + e);
        }
        return lineitemDao;

    }
    
    //Overloaded for INF-18183 ::: Ankit
    public LineitemDao deleteLineitemPerCost(int lineitemId, int applyType,
            int lastModifiedBy, String ipAdd, Hashtable<String, String> auditInfo) {
        Rlog.debug("Lineitem", "LineitemJB.agent bean deleteLineitemPerCost");
        LineitemDao lineitemDao = new LineitemDao();
        try {
     	 
        	 AuditBean audit=null; //Audit Bean for AppDelete
             String currdate =DateUtil.getCurrentDate(); /*gets the current date from DateUtil*/
             String userID=(String)auditInfo.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
             String ipAddAudit=(String)auditInfo.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
             String reason=(String)auditInfo.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
             String moduleName = (String)auditInfo.get(AuditUtils.APP_MODULE);
             String getRidValue= AuditUtils.getRidValue("SCH_LINEITEM","esch","pk_lineitem="+lineitemId);/*Fetches the RID/PK_VALUE*/ 
         	 audit = new AuditBean("SCH_LINEITEM",String.valueOf(lineitemId),getRidValue,
         			userID,currdate,moduleName,ipAddAudit,reason);/*POPULATE THE AUDIT BEAN*/ 
         	 em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
         	
            int retVal = lineitemDao.deleteLineitemPerCost(lineitemId, applyType,
                    lastModifiedBy, ipAdd);
            if(retVal<-2){
            	context.setRollbackOnly();
            }
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("Lineitem", "LineitemJB.deleteLineitemPerCost " + e);
        }
        return lineitemDao;

    }
    
    /**
     * Calls updateRepeatLineitems() method in lineitem dao
     * 
     */

    public int updateRepeatLineitems(int budgetCalId, String[] parentIds,
            String[] lineitemNames, String[] lineitemDescs,
            String[] lineitemCpts, String[] lineitemRepeat,
            String[] applyTypes, String[] categories, String[] tmids,
            String[] cdms, int creator, String ipAdd) {

        LineitemDao lineitemDao = new LineitemDao();
        try {
        	
        	lineitemDao.updateRepeatLineitems(budgetCalId, parentIds,
                    lineitemNames, lineitemDescs, lineitemCpts, lineitemRepeat,
                    applyTypes, categories, tmids, cdms, creator, ipAdd);

        } catch (Exception e) {
            Rlog.fatal("Lineitem", "LineitemJB.updateRepeatLineitems " + e);
            return -1;
        }
        return 0;
    }

    /**
     * Calls getLineitemsOfRepeatDefaultSec() method in lineitem dao
     * 
     * @param int
     *            containing budget Cal id
     * @return lineitem dao;
     */

    public LineitemDao getLineitemsOfRepeatDefaultSec(int budgetCalId) {

        LineitemDao lineitemDao = new LineitemDao();
        try {

            lineitemDao.getLineitemsOfRepeatDefaultSec(budgetCalId);

        } catch (Exception e) {
            Rlog.fatal("Lineitem", "LineitemJB.getLineitemsOfRepeatDefaultSec "
                    + e);
        }
        return lineitemDao;
    }

    /**
     * Inserts multiple lineitems to sections
     * 
     * @param lineitemDao
     * @return int
     */
    public int insertAllLineItems(ArrayList lineitemNames,
            ArrayList lineitemDescs, ArrayList lineitemCpts,
            ArrayList category, ArrayList TMIDs, ArrayList CDMs,
            String bgtSectionId, String ipAdd, String usr,Hashtable htMoreData) {
        try {
        	

        	System.out.println("Inside insert all line items ..");
            int ret = 0;
            int seq = 0;
            int retVal = 0;
            int rows = lineitemNames.size();
            
            int ilineitemid = 0;
            String bSection="false";
            String lineiteminpersec = "0";
            
            ArrayList costTypes = new ArrayList();
    		ArrayList unitCosts = new ArrayList();
    		ArrayList numUnits = new ArrayList();
    		ArrayList applyIndirects = new ArrayList();
    		ArrayList applyDiscounts= new ArrayList();
    		ArrayList sectionLineItemPKs = new ArrayList();
    		ArrayList sponsorAmounts= new ArrayList();
    		ArrayList applyPatientChecked = new ArrayList();
    		String templateType = "";
    		
            if (htMoreData != null)
            {
            	costTypes = (ArrayList)htMoreData.get("costTypes");
        		unitCosts = (ArrayList)htMoreData.get("unitCosts");
        		numUnits = (ArrayList)htMoreData.get("numUnits");
        		applyIndirects = (ArrayList)htMoreData.get("applyIndirects");
        		applyDiscounts= (ArrayList)htMoreData.get("applyDiscounts");
        		sectionLineItemPKs = (ArrayList)htMoreData.get("sectionLineItemPKs");
        		bSection= (String)htMoreData.get("bSection");
        		
        		sponsorAmounts= (ArrayList)htMoreData.get("sponsorAmounts");
        		//Added by:Yogendra||Date:12/12/12||Enhancement:FIN-22469
        		applyPatientChecked = (ArrayList)htMoreData.get("hidApplyPatientChecked");
        		//Added for Bug#13903 : Raviesh
        		templateType = (String)htMoreData.get("templateType");
            }
            
            for (int i = 0; i < rows; i++) {
            	
            	LineitemBean lsk = new LineitemBean();
            	
            	if (bSection.equals("true"))
                {
            		lsk  = getLineitemDetails(StringUtil.stringToNum((String)sectionLineItemPKs.get(i)));
            		lineiteminpersec = lsk.getLineitemInPerSec();
            		
            		if (StringUtil.isEmpty(lineiteminpersec))
            		{
            			lineiteminpersec="0";
            		}
                }
            	else
            	{
            		
            		lineiteminpersec="0";
            	}
            	
            	
                lsk.setLineitemName((String) lineitemNames.get(i));
                
                
                if (lineiteminpersec.equals("0"))
                {
                	lsk.setLineitemDesc((String) lineitemDescs.get(i));
                }
                else
                {
                	lsk.setLineitemNotes((String) lineitemDescs.get(i));
                }
                
                lsk.setLineitemCptCode((String) lineitemCpts.get(i));
                lsk.setLineitemCategory((String) category.get(i));
                lsk.setLineitemTMID((String) TMIDs.get(i));
                lsk.setLineitemCDM((String) CDMs.get(i));
                lsk.setLineitemBgtSection(bgtSectionId);
                  
                
                lsk.setIpAdd(ipAdd);
            	//Added by Ia 11.04.2006 bug#2792 default sponsor unit = 1 
                lsk.setLineitemClinicNOfUnit("1.00");
				//Addded
                
                
                if (costTypes != null && (costTypes.size() > i) )
                {
                	lsk.setLineItemCostType((String) costTypes.get(i) );
                	
                	lsk.setLineitemSponsorUnit((String) unitCosts.get(i) );
                	lsk.setLineitemClinicNOfUnit((String) numUnits.get(i));
                	
                	lsk.setLineitemAppIndirects((String) applyIndirects.get(i) );
                	lsk.setLineitemInCostDisc((String) applyDiscounts.get(i));
                	
                	// Ankit: Bug-11399 Date- 18 Sep 2012
                	BigDecimal bd1 = new BigDecimal(String.valueOf(unitCosts.get(i)));
                    BigDecimal bd2 = new BigDecimal(Float.parseFloat(String.valueOf(numUnits.get(i))));
                    // SM bug #14739
                    double doubleOtherCost = (bd1.multiply(bd2)).doubleValue();
                    lsk.setLineitemOtherCost(""+NumberUtil.roundOffNo(doubleOtherCost));

                	lsk.setLineItemSponsorAmount( (String) sponsorAmounts.get(i));
                    //Added by:Yogendra||Date:12/12/12||Enhancement:FIN-22469
                    //Modified for Bug#13903 : Raviesh
                    if (templateType.equals("C")){
                    String isPatientCountApplied="0";
	                    isPatientCountApplied = (String)applyPatientChecked.get(i);
                		lsk.setLineitemApplyPatientCount(isPatientCountApplied);
                    }
                }
                
                if (bSection.equals("false"))
                {
                	lsk.setCreator(usr);
                	lsk.setLineitemDelFlag("N");
                    lsk.setLineitemInPerSec("0");
                    
                	ilineitemid = this.setLineitemDetails(lsk);
                }
                else //bSection=true, needs update
                {
                	lsk.setModifiedBy(usr);
                	//set the PK to the bean and update
                	this.updateLineitem(lsk);
                }

                Rlog.debug("Lineitem",
                        "LineitemAgentBean.INSIDE ROWS AFTER SETTING###" + ret);

            }

            return ret;
        } catch (Exception e) {
            Rlog.fatal("formsection",
                    "Exception In insertNewSections in FormSecAgentBean " + e);
            return -2;
        }

    }

    public int deleteSelectedLineitems(ArrayList lineitemIds,
            int lastModifiedBy, String ipAdd) {

        Rlog.debug("Lineitem", "LineitemJB.agent bean deleteSelectedLineitems");
        LineitemDao lineitemDao = new LineitemDao();
        try {
            lineitemDao.deleteSelectedLineitems(lineitemIds, lastModifiedBy,
                    ipAdd);

        } catch (Exception e) {
            Rlog.fatal("Lineitem", "LineitemJB.deleteSelectedLineitems " + e);
            return -1;
        }
        return 1;

    }
    //Overloaded for INF-18183 ::: Ankit
    public int deleteSelectedLineitems(ArrayList lineitemIds,
            int lastModifiedBy, String ipAdd, Hashtable<String, String> auditInfo) {

        Rlog.debug("Lineitem", "LineitemJB.agent bean deleteSelectedLineitems");
        LineitemDao lineitemDao = new LineitemDao();
        try {
        	
        	String Id = "", IdList = "";
            for (int i = 0; i < lineitemIds.size(); i++) {
                Id = (String) lineitemIds.get(i);
                Id = (Id == null) ? "" : Id;
                if (IdList.length() == 0)
                    IdList = IdList + Id;
                else
                    IdList = IdList + "," + Id;
            }
            IdList = "pk_lineitem in (" + IdList + ")";
            String pkVal = "";
        	String ridVal = "";
            AuditBean audit=null;
            String currdate =DateUtil.getCurrentDate();
            String userID=(String)auditInfo.get(AuditUtils.USER_ID_KEY); 
            String ipAddAudit=(String)auditInfo.get(AuditUtils.IP_ADD_KEY); 
            String reason=(String)auditInfo.get(AuditUtils.REASON_FOR_DEL);
            String moduleName = (String)auditInfo.get(AuditUtils.APP_MODULE);
            Hashtable<String, ArrayList<String>> rowValues = AuditUtils.getRowValues("SCH_LINEITEM",IdList,"esch");/*Fetches the RID/PK_VALUE*/
            ArrayList<String> rowPK  = (ArrayList<String>)rowValues.get(AuditUtils.ROW_PK_KEY);
    		ArrayList<String> rowRID  = (ArrayList<String>)rowValues.get(AuditUtils.ROW_RID_KEY);
    		
    		for(int i=0; i<rowPK.size(); i++)
			{
    			pkVal = rowPK.get(i);
    			ridVal = rowRID.get(i);
    			audit = new AuditBean("SCH_LINEITEM",pkVal,ridVal,
        			userID,currdate,moduleName,ipAddAudit,reason);
    			em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
			}
        	
            lineitemDao.deleteSelectedLineitems(lineitemIds, lastModifiedBy,
                    ipAdd);

        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("Lineitem", "LineitemJB.deleteSelectedLineitems " + e);
            return -1;
        }
        return 1;

    }
    
    public LineitemDao getSectionLineitems(int sectionPK)
    {
    	
    	LineitemDao lineitemDao = new LineitemDao();
        try {

            lineitemDao.getSectionLineitems(sectionPK);

        } catch (Exception e) {
            Rlog.fatal("Lineitem", "LineitemJB.getSectionLineitems "
                    + e);
        }
        return lineitemDao;

    }
    public int removeIndirects(int budgetId)
    {
    	int ret=0;
    	LineitemDao lineitemDao = new LineitemDao();
        try {

           ret = lineitemDao.removeIndirects(budgetId);

        } catch (Exception e) {
            Rlog.fatal("Lineitem", "LineitemJB.removeIndirects "
                    + e);
        }
        return ret;

    }
    
}// end of class
