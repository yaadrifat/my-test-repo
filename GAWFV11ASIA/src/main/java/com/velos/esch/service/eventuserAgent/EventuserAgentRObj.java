/*
 * Classname : EventuserAgentRObj
 * 
 * Version information: 1.0
 *
 * Date: 06/21/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh Khurana
 */

package com.velos.esch.service.eventuserAgent;

/* Import Statements */
import java.util.Hashtable;

import javax.ejb.Remote;

import com.velos.esch.business.common.EventuserDao;
import com.velos.esch.business.eventuser.impl.EventuserBean;

/* End of Import Statements */
@Remote
public interface EventuserAgentRObj {

    EventuserBean getEventuserDetails(int eventuserId);

    public int setEventuserDetails(EventuserBean eusk);
    
    public int setEventuserDetailsSingle(EventuserBean eusk);

    public int updateEventuser(EventuserBean eusk);
   
    
    public int removeEventusers(int eventId, String eventType);
    
    // removeEventusers() Overloaded for INF-18183 ::: Raviesh
    public int removeEventusers(int eventId, String eventType,Hashtable<String, String> args);
      
    
    public int removeEventuser(int eventuserId);
    
    // removeEventuser() Overloaded for INF-18183 ::: Raviesh
    public int removeEventuser(int eventuserId,Hashtable<String, String> args);
    
    public EventuserDao getEventRoles(int eventId);

    public EventuserDao getEventUsers(int eventId);

    public EventuserDao getAllRolesForEvent(int eventId);

}