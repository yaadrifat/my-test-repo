/*
 * Classname : BudgetAgentBean
 * 
 * Version information: 1.0
 *
 * Date: 03/20/2002
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh Khurana
 */

package com.velos.esch.service.budgetAgent.impl;

/* IMPORT STATEMENTS */

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import com.aithent.audittrail.reports.AuditUtils;
import com.velos.esch.business.audit.impl.AuditBean;
import com.velos.eres.service.util.DateUtil;
import java.util.Hashtable;

import com.velos.esch.business.budget.impl.BudgetBean;
import com.velos.esch.business.common.BudgetDao;
import com.velos.esch.service.budgetAgent.BudgetAgentRObj;
import com.velos.esch.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the entity CMP BudgetBean.<br>
 * <br>
 * 
 */
@Stateless
public class BudgetAgentBean implements BudgetAgentRObj {

    @PersistenceContext(unitName = "esch")
    protected EntityManager em;
    @Resource private SessionContext context;

    /**
     * Calls getBudgetDetails() on Budget Entity Bean BudgetBean. Looks up on
     * the Budget id parameter passed in and constructs and returns a Budget
     * state holder. containing all the summary details of the Budget<br>
     * 
     * @param budgetId
     *            the Budget id
     * @ee BudgetBean
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public BudgetBean getBudgetDetails(int budgetId) {

        try {
            return (BudgetBean) em
                    .find(BudgetBean.class, new Integer(budgetId));
        } catch (Exception e) {
            Rlog.fatal("budget",
                    "Error in getBudgetDetails() in BudgetAgentBean" + e);
            return null;
        }

    }

    /**
     * Calls setBudgetDetails() on Budget Entity Bean BudgetBean.Creates a new
     * Budget with the values in the StateKeeper object.
     * 
     * @param budget
     *            a budget state keeper containing budget attributes for the new
     *            Budget.
     * @return int - 0 if successful; -2 for Exception;
     */

    public int setBudgetDetails(BudgetBean bdsk) {
        try {
            BudgetBean bud = new BudgetBean();
            bud.updateBudget(bdsk);
            em.persist(bud);

            return (bud.getBudgetId());

        } catch (Exception e) {
            Rlog.fatal("budget",
                    "Error in setBudgetDetails() in BudgetAgentBean " + e);
        }
        return 0;
    }

    public int updateBudget(BudgetBean bdsk) {

        BudgetBean retrieved = null; // Budget Entity Bean Remote Object
        int output;

        try {

            retrieved = (BudgetBean) em.find(BudgetBean.class, new Integer(bdsk
                    .getBudgetId()));

            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updateBudget(bdsk);
            em.merge(retrieved);
        } catch (Exception e) {
            Rlog
                    .fatal("budget", "Error in updateBudget in BudgetAgentBean"
                            + e);
            return -2;
        }
        return output;
    }
    
    // Overloaded for INF-18183 ::: Akshi
    public int updateBudget(BudgetBean bdsk,Hashtable<String, String> args) {

        BudgetBean retrieved = null; // Budget Entity Bean Remote Object
        int output;

        try {
        	retrieved = (BudgetBean) em.find(BudgetBean.class, new Integer(bdsk
                    .getBudgetId()));

            if (retrieved == null) {
                return -2;
            }
            
            AuditBean audit=null;
            String currdate =DateUtil.getCurrentDate();
            String userID=(String)args.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
            String ipAdd=(String)args.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
            String reason=(String)args.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
            String appmodule=(String)args.get(AuditUtils.APP_MODULE); /*Fetches the application module name from the Hashtable*/
            String getRidValue= AuditUtils.getRidValue("SCH_BUDGET","esch","PK_BUDGET="+bdsk.getBudgetId());/*Fetches the RID/PK_VALUE*/ 
            audit = new AuditBean("SCH_BUDGET",String.valueOf(bdsk.getBudgetId()),getRidValue,
        			userID,currdate,appmodule,ipAdd,reason);/*POPULATE THE AUDIT BEAN*/ 
            em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/ 
            
            output = retrieved.updateBudget(bdsk);
            em.merge(retrieved);
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog
                    .fatal("budget", "Error in updateBudget in BudgetAgentBean"
                            + e);
            return -2;
        }
        return output;
    }

    public BudgetDao getBudgetInfo(int budgetId) {

        BudgetDao budgetDao = new BudgetDao();
        try {
            budgetDao.getBudgetInfo(budgetId);
        } catch (Exception e) {
            Rlog.fatal("account", "Error in getAccounts() in AccountAgentBean "
                    + e);
        }
        return budgetDao;
    }

    public BudgetDao getBudgetsForUser(int userId) {
        BudgetDao budgetDao = new BudgetDao();
        try {
            budgetDao.getBudgetsForUser(userId);
            Rlog.debug("budget",
                    "In getBudgetsForUser in BudgetAgentBean line CCC");

        } catch (Exception e) {
            Rlog.fatal("budget", "Error in getBudgetsForUser BudgetAgentBean "
                    + e);
        }
        return budgetDao;
    }

    public BudgetDao getCombBudgetForStudy(int userId, int studyId) {
        BudgetDao budgetDao = new BudgetDao();
        try {
            budgetDao.getCombBudgetForStudy(userId, studyId);
            Rlog.debug("budget",
                    "In getCombBudgetForStudy in BudgetAgentBean line CCC");

        } catch (Exception e) {
            Rlog.fatal("budget", "Error in getCombBudgetForStudy BudgetAgentBean "
                    + e);
        }
        return budgetDao;
    }
    
    
    public int copyBudget(int oldBudget, String newBudName, String newBudVer,
            int creator, String ipAdd) {
        int ret = -1;

        Rlog.debug("budget", "In copyBudget before CCC");
        BudgetDao budgetDao = new BudgetDao();
        Rlog.debug("budget", "In copyBudget budgetDao" + budgetDao);

        try {
            Rlog.debug("budget", "In copyBudget in BudgetAgentBean line CCC");

            ret = budgetDao.copyBudget(oldBudget, newBudName, newBudVer,
                    creator, ipAdd);

        } catch (Exception e) {
            Rlog.fatal("budget", "Error in copyBudget BudgetAgentBean " + e);
            return -1;
        }
        return ret;
    }

    /**
     * Calls copyBudgetForTemplate() on vudgetDao; *
     * 
     * @param oldbudgetId
     * @param budgetId
     * @param creator
     * @param ipAdd
     * @return 0 if successful
     * @see BudgetDao
     */

    public int copyBudgetForTemplate(int oldBudget, int budgetId, int creator,
            String ipAdd) {
        int ret = -1;

        BudgetDao budgetDao = new BudgetDao();
        try {
            Rlog.debug("budget", "In copyBudgetForTemplate in BudgetAgentBean");

            ret = budgetDao.copyBudgetForTemplate(oldBudget, budgetId, creator,
                    ipAdd);

        } catch (Exception e) {
            Rlog.fatal("budget",
                    "Error in copyBudgetForTemplate BudgetAgentBean " + e);
            return -1;
        }
        return ret;
    }

    public BudgetDao getBgtTemplateSubType(int bgtTemplate, int budgetId) {

        int ret = -1;

        BudgetDao budgetDao = new BudgetDao();
        try {
            Rlog.debug("budget", "In getBgtTemplateSubType in BudgetAgentBean");

            budgetDao.getBgtTemplateSubType(bgtTemplate, budgetId);

        } catch (Exception e) {
            Rlog.fatal("budget",
                    "Error in copyBudgetForTemplate BudgetAgentBean " + e);

        }
        return budgetDao;

    }
    
    /**
     * Get Default Budget PK For a Calendar
     * 
     * @param PKProtocol Protocol ID
     */

    public int getDefaultBudgetForCalendar(int PKProtocol) {

        int budgetPK= 0;

        BudgetDao budgetDao = new BudgetDao();
        try {
            
        	budgetPK = budgetDao.getDefaultBudgetForCalendar(PKProtocol);

        } catch (Exception e) {
            Rlog.fatal("budget",
                    "Exception in getDefaultBudgetForCalendar(int PKProtocol) " + e);

        }
        return budgetPK;

    }

}// end of class
