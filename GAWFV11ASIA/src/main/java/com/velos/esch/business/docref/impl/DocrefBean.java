/*
 * Classname : DocrefBean
 * 
 * Version information: 1.0
 *
 * Date: 07/03/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh Khurana 
 */

package com.velos.esch.business.docref.impl;

/* Import Statements */
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.esch.service.util.Rlog;
import com.velos.esch.service.util.StringUtil;

/* End of Import Statements */
@Entity
@Table(name = "sch_eventdoc")
@NamedQuery(name = "findEventdocId", query = "SELECT OBJECT(docref) FROM DocrefBean docref where trim(pk_docs)=:pkDocs")
public class DocrefBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3257571685124159801L;

    /**
     * docrefId
     */
    public int docrefId;

    /**
     * userId
     */
    public Integer docId;

    /**
     * eventId
     */

    public Integer eventId;

    /*
     * the record creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * the IP Address
     */
    public String ipAdd;

    // GETTER SETTER METHODS
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "sch_eventdoc_seq", allocationSize=1)
    @Column(name = "PK_EVENTDOC")
    public int getDocrefId() {
        return this.docrefId;
    }

    public void setDocrefId(int docrefId) {

        this.docrefId = docrefId;
    }

    @Column(name = "PK_DOCS")
    public String getDocId() {
        return StringUtil.integerToString(this.docId);

    }

    public void setDocId(String docId) {
        if (docId != null) {
            this.docId = Integer.valueOf(docId);
        }
    }

    @Column(name = "FK_EVENT")
    public String getEventId() {
        return StringUtil.integerToString(this.eventId);
    }

    public void setEventId(String eventId) {
        if (eventId != null) {
            this.eventId = Integer.valueOf(eventId);
        }
    }

    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // / END OF GETTER SETTER METHODS

    /**
     * sets up a state keeper containing details of the docref
     * 
     * @param drsk
     */

    /*
     * public void setDocrefStateKeeper(String documentId, String eveId, String
     * creator, String modifiedBy, String ipAdd) { GenerateId Id = null;
     * 
     * try {
     * 
     * Connection conn = null; conn = getConnection(); docrefId =
     * Id.getId("sch_eventdoc_seq", conn); conn.close();
     * 
     * setDocId(documentId); setEventId(eveId); setCreator(creator);
     * setModifiedBy(modifiedBy); setIpAdd(ipAdd); } catch (Exception e) {
     * System.out.println("Error in setDocrefStateKeeper() in DocrefBean " + e); } }
     */

    /**
     * updates the docref details
     * 
     * @param drsk
     * @return 0 if successful; -2 for exception
     */
    public int updateDocref(String documentId, String eveId, String creator,
            String modifiedBy, String ipAdd) {
        try {

            setDocId(documentId);
            setEventId(eveId);
            setCreator(creator);
            setModifiedBy(modifiedBy);
            setIpAdd(ipAdd);

            return 0;
        } catch (Exception e) {
            Rlog.fatal("docref", " error in DocrefBean.updateDocref : " + e);
            return -2;
        }
    }

    public DocrefBean() {

    }

    public DocrefBean(int docrefId, String docId, String eventId,
            String creator, String modifiedBy, String ipAdd) {
        super();
        // TODO Auto-generated constructor stub
        setDocrefId(docrefId);
        setDocId(docId);
        setEventId(eventId);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
    }

}// end of class
