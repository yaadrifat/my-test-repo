/*
 * Classname : BgtApndxFile
 * 
 * Version information: 1.0
 *
 * Copyright notice: Velos, Inc
 * date: 04/16/2002
 *
 * Author: sonia sahni
 *
 */

package com.velos.esch.business.common;

/* import statements */
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import com.velos.eres.business.common.CommonDAO;

import oracle.jdbc.OracleResultSet;
import oracle.sql.BLOB;

import com.velos.esch.service.util.Rlog;

/* import statements */

/**
 * 
 * 
 * @author Sonia Sahni
 * @vesion 1.0
 */

public class BgtApndxFile extends CommonDAO implements java.io.Serializable {

    // End of Getter Setter methods

    /**
     * Default Constructor
     * 
     * 
     */

    public BgtApndxFile() {
    }

    /**
     * Gets code list data for a code type and populates the class attributes
     * 
     * @param c
     *            Code Type
     */

    public boolean saveFile(String filPath, int apndxPK, long fileSize) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        int filekb = 0;

        filekb = (int) fileSize / 1024;

        // set code type
        try {
            conn = getSchConnection();

            conn.setAutoCommit(false);

            Rlog.debug("bgtapndx", "GOT FILE PATH " + filPath);

            File file = new File(filPath);
            // fileSize = file.length();
            InputStream istr = new FileInputStream(file);

            Rlog.debug("bgtapndx", "FILE OBJ CREATED" + file);

            // insert empty blob for the first time
            pstmt = conn
                    .prepareStatement("Update SCH_BGTAPNDX set BGTAPNDX_FILEOBJ = empty_blob() where PK_BGTAPNDX = ?");

            pstmt.setInt(1, apndxPK);
            pstmt.execute();
            pstmt.close();

            // get the empty blob from the row saved above

            Statement b_stmt = conn.createStatement();
            ResultSet savedBlob = b_stmt
                    .executeQuery("SELECT BGTAPNDX_FILEOBJ from SCH_BGTAPNDX WHERE PK_BGTAPNDX  = "
                            + apndxPK + " FOR UPDATE");

            // Retrieve BLOB streams and load the files

            if (savedBlob.next()) {

                Rlog.debug("bgtapndx", "got blobrow");
                // Get the BLOB locator and open output stream for the BLOB
                BLOB filBLOB = ((OracleResultSet) savedBlob).getBLOB(1);
                OutputStream l_blobOutputStream = filBLOB
                        .getBinaryOutputStream();

                byte[] l_buffer = new byte[10 * 1024];

                int l_nread = 0; // Number of bytes read
                while ((l_nread = istr.read(l_buffer)) != -1)
                    // Read from file
                    l_blobOutputStream.write(l_buffer, 0, l_nread); // Write to
                // BLOB

                // Close both streams
                istr.close();
                l_blobOutputStream.close();
            }
            conn.commit();
            savedBlob.close();
            b_stmt.close();
            // ////

            // delete the temporary file that was created on the server
            Rlog.debug("bgtapndx", "Trying to delete temp file");
            if (file.delete()) {
                Rlog.debug("bgtapndx", "Temporary File Deleted");
            }

            return true;
        } catch (Exception ex) {
            Rlog.debug("bgtapndx", "EXCEPTION IN SAVING FILE" + ex);
            return false;
        } finally {
            // try{
            // if (pstmt != null) pstmt.close();
            // }catch (Exception e) { }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }

    public boolean readFile(String path, int apndxPK) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        // set code type
        try {
            conn = getSchConnection();

            // String s = "newtest.bmp";

            // Configuration.readAppendixParam(Configuration.ERES_HOME +
            // "eresearch.xml");

            // s=EJBUtil.getActualPath(Configuration.UPLOADFOLDER, s);

            Rlog.debug("bgtapndx", "GOT FILE PATH " + path);

            pstmt = conn
                    .prepareStatement("Select BGTAPNDX_FILEOBJ from SCH_BGTAPNDX where PK_BGTAPNDX = ?");

            pstmt.setInt(1, apndxPK);

            ResultSet rs = pstmt.executeQuery();

            if (rs.next()) {
                // get binary stream data from result set
                InputStream dbFile = (rs.getBlob(1)).getBinaryStream();

                // open operating system file for storing the image
                FileOutputStream os = new FileOutputStream(path);

                Rlog.debug("bgtapndx", "FILE OBJ CREATED");

                // fetch-write loop, fetch image data from rs, write to file
                int i;
                while ((i = dbFile.read()) != -1)
                    os.write(i);

                // Close the destination output stream
                os.close();
            }

            return true;
        } catch (Exception ex) {
            Rlog.debug("bgtapndx",
                    "EXCEPTION IN SAVING FILE FROMCONTROL TABLE " + ex);
            return false;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }

}
