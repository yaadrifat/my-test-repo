package com.velos.esch.business.common;

import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.apache.commons.lang.StringUtils;
import com.velos.eres.service.util.StringUtil;
import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.service.util.Rlog;

import edu.emory.mathcs.backport.java.util.Arrays;

public class AuditRowEschDao extends CommonDAO implements java.io.Serializable {
	
	public AuditRowEschDao() {    	
    
    }
	
	  public ArrayList getRID(String[] pkey, String tableName, String pkColumnName) {
	    	 PreparedStatement pstmt = null;
	         Connection conn = null;
             ArrayList rowValues = new ArrayList();

	         String sql="";
	         ResultSet rs=null;
	         try{
	        	 Array sqlArray = null;
	        	 if (StringUtil.isEmpty(tableName) ||
	        		StringUtil.isEmpty(pkColumnName) || pkey.length < 0){
	        		   return rowValues;
	        	 }else{
	        		
		        	 conn = getConnection();

		        	 String parameters = StringUtils.join(Arrays.asList(pkey).iterator(),",");  
		        	 
		        	 pstmt =conn.prepareStatement("SELECT RID FROM esch."+ tableName +" WHERE "+pkColumnName+" in ("+parameters+")");  
		             pstmt.setFetchSize(500);
		        	 ResultSet rs1 = pstmt.executeQuery();
		          
		             
		       
		             while (rs1.next()) {
		                 rowValues.add(new Integer(rs1.getInt("RID")));
		             }  
		             
		             
		             System.out.println(rowValues);
		             return rowValues;
		             
	        	 }
	        	
	            
	         }catch (SQLException ex) {
	             Rlog.fatal("","AuditRowEschDao.getRID EXCEPTION IN READING "+ tableName +" table "+ ex);
	             return rowValues;
	         } finally {
	             try {
	                 if (pstmt != null)
	                     pstmt.close();
	             } catch (Exception e) {
	             }
	             try {
	                 if (conn != null)
	                     conn.close();
	             } catch (Exception e) {
	             }
	         }
	    }

	  public ArrayList getRID(Integer[] pkey, String tableName, String pkColumnName) {
	    	 PreparedStatement pstmt = null;
	         Connection conn = null;
          ArrayList rowValues = new ArrayList();

	         String sql="";
	         ResultSet rs=null;
	         try{
	        	 Array sqlArray = null;
	        	 if (StringUtil.isEmpty(tableName) ||
	        		StringUtil.isEmpty(pkColumnName) || pkey.length < 0){
	        		   return rowValues;
	        	 }else{
	        		
		        	 conn = getConnection();
      
		        	 String parameters = StringUtils.join(Arrays.asList(pkey).iterator(),",");  

		        	 
		        	 pstmt =conn.prepareStatement("SELECT RID FROM esch."+ tableName +" WHERE "+pkColumnName+" in ("+parameters+")");  
		             pstmt.setFetchSize(500);

		        	 ResultSet rs1 = pstmt.executeQuery();
		          
		             
		       
		             while (rs1.next()) {
		                 rowValues.add(new Integer(rs1.getInt("RID")));
		             }  
		             
		             
		             System.out.println(rowValues);
		             return rowValues;
		             
	        	 }
	        	
	            
	         }catch (SQLException ex) {
	             Rlog.fatal("","AuditRowEschDao.getRID EXCEPTION IN READING "+ tableName +" table "+ ex);
	             return rowValues;
	         } finally {
	             try {
	                 if (pstmt != null)
	                     pstmt.close();
	             } catch (Exception e) {
	             }
	             try {
	                 if (conn != null)
	                     conn.close();
	             } catch (Exception e) {
	             }
	         }
	    }

	  public ArrayList getRID(Integer pkey, String tableName, String pkColumnName) {
	    	 PreparedStatement pstmt = null;
	         Connection conn = null;
       ArrayList rowValues = new ArrayList();

	         String sql="";
	         ResultSet rs=null;
	         try{
	        	 Array sqlArray = null;
	        	 
	        	
	        		
		        	 conn = getConnection();
   
		        	 //String parameters = StringUtils.join(Arrays.asList(pkey).iterator(),",");  

		        	 
		        	 pstmt =conn.prepareStatement("SELECT RID FROM esch."+ tableName +" WHERE "+pkColumnName);  
		             pstmt.setFetchSize(500);

		        	 ResultSet rs1 = pstmt.executeQuery();
		          
		             
		       
		             while (rs1.next()) {
		                 rowValues.add(new Integer(rs1.getInt("RID")));
		             }  
		             
		             
		             System.out.println(rowValues);
		             return rowValues;
		             
	       
	        	
	            
	         }catch (SQLException ex) {
	             Rlog.fatal("","AuditRowEschDao.getRID EXCEPTION IN READING "+ tableName +" table "+ ex);
	             return rowValues;
	         } finally {
	             try {
	                 if (pstmt != null)
	                     pstmt.close();
	             } catch (Exception e) {
	             }
	             try {
	                 if (conn != null)
	                     conn.close();
	             } catch (Exception e) {
	             }
	         }
	    }
	  
	  
	  public ArrayList findletst(Integer[] pkey, int userid) {
	    	 PreparedStatement pstmt = null;
	         Connection conn = null;
          ArrayList rowValues = new ArrayList();


	         String sql="";
	         ResultSet rs=null;
	         try{
	        
	        	 if (pkey.length < 0){
	        		   return rowValues;
	        	 }else{
	        		
		        	 conn = getConnection();
	        	
		        	 String parameters = StringUtils.join(Arrays.asList(pkey).iterator(),",");  

		        	 
		     pstmt =conn.prepareStatement("SELECT rid,"+
           "MAX(RAID) KEEP(DENSE_RANK FIRST ORDER BY RAID DESC) RAID "+ 
           "FROM ESCH.AUDIT_ROW where  rid in("+parameters+") and user_Name like '"+userid+"%'"+
           "GROUP BY rid ORDER BY 1 DESC");  
             pstmt.setFetchSize(500);

		     rs = pstmt.executeQuery();
		          
		        while (rs.next()){
	                 rowValues.add(new Integer(rs.getInt("RAID")));

		        }
	             System.out.println(rowValues);
    
	             return rowValues;
		          
	        	 }
	        	
	            
	         }catch (SQLException ex) {
	             Rlog.fatal("","AuditRowEschDao.getRID EXCEPTION IN READING "+ userid +" table "+ ex);
	             return rowValues;
	         } finally {
	             try {
	                 if (pstmt != null)
	                     pstmt.close();
	             } catch (Exception e) {
	             }
	             try {
	                 if (conn != null)
	                     conn.close();
	             } catch (Exception e) {
	             }
	         }
	    }
	
	  public int setReasonForChange(Integer[] raid, String remarks) {
	   	 	PreparedStatement pstmt = null;
	        Connection conn = null;

	        int ret=0;
	        String sql="";
	        try{
	       	 if (StringUtil.isEmpty(remarks) || raid.length < 0){
	       		 ret = -1;
	       	 }else{
	        	 conn = getConnection();
	         	conn.setAutoCommit(false);
	         	
	        	 String parameters = StringUtils.join(Arrays.asList(raid).iterator(),",");  

	        	 sql="UPDATE esch.AUDIT_COLUMN set REMARKS ='"+ remarks +"' WHERE RAID in("+parameters
	        	 +") AND (REMARKS IS NULL OR LENGTH(REMARKS)=0)";
	        	 pstmt = conn.prepareStatement(sql);
	       		 ret=pstmt.executeUpdate();
	       		 conn.commit();
	       		
	       		 ret = 1;
	       		conn.setAutoCommit(true);
	       	 }
	       	 return ret;
	        }catch (SQLException ex) {
	            Rlog.fatal("","AuditRowEschDao.getRID EXCEPTION IN UPDATING AUDIT_COLUMN table " + ex);
	            return ret;
	        } finally {
	            try {
	                if (pstmt != null)
	                    pstmt.close();
	            } catch (Exception e) {
	            }
	            try {
	                if (conn != null)
	                    conn.close();
	            } catch (Exception e) {
	            }

	        }
	   	
	   }
    public int getRID(int pkey, String tableName, String pkColumnName) {
    	 PreparedStatement pstmt = null;
         Connection conn = null;
         int retInt=0;
         String sql="";
         try{
        	 if (StringUtil.isEmpty(tableName) ||
        		StringUtil.isEmpty(pkColumnName) || pkey < 0){
        		 retInt = -1;
        	 }else{
	        	 conn = getConnection();
	        	 sql="SELECT RID FROM esch."+ tableName +" WHERE "+pkColumnName+" = LPAD(to_char(" + pkey +"),10,'0')";
	        	 pstmt = conn.prepareStatement(sql);
	             ResultSet rs = pstmt.executeQuery();

	             while (rs.next()) {
	            	 retInt = (new Integer(rs.getInt("RID")));
	            	 break;
	             }
        	 }
        	 return retInt;
         }catch (SQLException ex) {
             Rlog.fatal("","AuditRowEschDao.getRID EXCEPTION IN READING "+ tableName +" table "+ ex);
             return retInt;
         } finally {
             try {
                 if (pstmt != null)
                     pstmt.close();
             } catch (Exception e) {
             }
             try {
                 if (conn != null)
                     conn.close();
             } catch (Exception e) {
             }
         }
    }
    
    
	public int setReasonForChange(int raid, String remarks) {
   	 	PreparedStatement pstmt = null;
        Connection conn = null;
        
        int ret=0;
        String sql="";
        try{
       	 if (StringUtil.isEmpty(remarks) || raid < 0){
       		 ret = -1;
       	 }else{
        	 conn = getConnection();
         	conn.setAutoCommit(false);
        	 sql="UPDATE esch.AUDIT_COLUMN set REMARKS ='"+ remarks +"' WHERE RAID ="+raid
        	 +" AND (REMARKS IS NULL OR LENGTH(REMARKS)=0)";
        	 pstmt = conn.prepareStatement(sql);
       		 ret=pstmt.executeUpdate();
       		 conn.commit();
       		 ret = 1;
       		conn.setAutoCommit(true);
       	 }
       	 return ret;
        }catch (SQLException ex) {
            Rlog.fatal("","AuditRowEschDao.getRID EXCEPTION IN UPDATING AUDIT_COLUMN table " + ex);
            return ret;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
   	
   }
	public int updateEschAuditRow(String schema,String userName,ArrayList rid,String action){
		Connection con=null;
		try{
		con=AuditUtils.updateAuditROw(schema, userName, rid, action);
		AuditUtils.commitOrRollback(con, true);
		}catch(Exception ex){
			try{
        		AuditUtils.commitOrRollback(con, false);
        	}catch(Exception e){
        		Rlog.fatal("formlib","Exception occurs at AuditUtils.commitOrRollback()" + e);
        	}
            return -2;	
		}
		return 0;
	}
}  
