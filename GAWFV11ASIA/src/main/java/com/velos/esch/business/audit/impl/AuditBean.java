/*
 * Classname			AuditBean.class
 *
 * Version information
 *
 * Date					08/22/2011
 *
 * Author 			Yogesh Kumar
 * Copyright notice
 */

package com.velos.esch.business.audit.impl;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.StringUtil;

/**
 * The Audit Delete entity bean.<br>
 * <br>
 *
 * @author
 */
@Entity
@Table(schema="ESCH", name = "AUDIT_DELETELOG")

public class AuditBean implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 5229025033944755402L;

	/**
     * the primary key 
     */
    public Integer id;

    /**
     * Name of the table from which row is deleted
     */
    public String tableName;
    /**
     * PK value of the row being deleted
     */
    public Integer tablePK;

    /**
     * RID of the row being deleted
     */

    public Integer tableRID;

    /**
     * User ID deleting the row.
     */

    public String deletedBy = "";

    /**
     * Date on which data is deleted
     */
    public java.util.Date deletedOn;

    /**
     * Module from which data is deleted.
     */
    
    public String appModule = "";
    /**
     * IP Address from which data is deleted.
     */
    
    public String ipAdd = "";
    
    /**
     * Reason why the row was deleted.
     */
    public String reason = "";
    
    public AuditBean() {
    }

    public AuditBean(String tableName,String tablePK, String tableRID, 
    		String deletedBy, String deletedOn,String appModule , String ipAdd, String reason) {
    	
    	setTableName(tableName);
        setIpAdd(ipAdd);
        setTablePK(tablePK);
        setTableRID(tableRID);
        setDeletedBy(deletedBy);
        setDeletedOn(deletedOn);
        setAppModule(appModule);
        setReason(reason);
    }

    // GETTER SETTER METHODS
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "ESCH.SEQ_AUDIT_DELETELOG", allocationSize=1)
    @Column(name = "PK_APP_DL")
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "TABLE_NAME")
    public String getTableName() {
        return this.tableName;
    }

    /**
     *
     *
     * @param account
     */
    public void setTableName(String tableName) {
        this.tableName = tableName;
    }
    
      
    @Column(name = "TABLE_PK")
    public String getTablePK() {
        return StringUtil.integerToString(this.tablePK);
    }

    public void setTablePK(String tablePK) {

        this.tablePK = StringUtil.stringToInteger(tablePK);
    }
    @Column(name = "TABLE_RID")
     public String getTableRID() {
        return StringUtil.integerToString(this.tableRID);
    }

    public void setTableRID(String tableRID) {

        this.tableRID = StringUtil.stringToInteger(tableRID);
    }
    
    @Column(name = "DELETED_BY")
    public String getDeletedBy() {
        return this.deletedBy;
    }

    public void setDeletedBy(String deletedBy) {

        this.deletedBy = deletedBy;
    }
    
    @Column(name = "DELETED_ON")
    public Date getDeletedOn() {
        return this.deletedOn;
    }

    public void setDeletedOn(Date deletedOn) {
        this.deletedOn = deletedOn;
    }
    
    public void setDeletedOn(String deletedOn) {

    	setDeletedOn(DateUtil.stringToDate(deletedOn,
                null));
    }

    @Column(name = "APP_MODULE")
    public String getAppModule() {
        return this.appModule;
    }

    public void setAppModule(String appModule) {
        this.appModule = appModule;
    }
    
    @Column(name = "IP_ADDRESS")
    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }
    
    @Column(name = "REASON_FOR_DELETION")
    public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}


     // END OF GETTER SETTER METHODS

    /**
     *
     * /** updates the study details
     *
     * @param ssh
     * @return 0 if successful; -2 for exception
     */

    
}// end of class

