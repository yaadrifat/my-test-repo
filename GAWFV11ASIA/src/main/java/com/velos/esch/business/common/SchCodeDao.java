/*
 * Classname : SchCodeDao
 *
 * Version information: 1.0
 *
 * Copyright notice: Velos, Inc
 * date: 08/01/2001
 *
 * Author: sonia sahni
 * Data Access class for codelist data
 */

package com.velos.esch.business.common;

/* import statements */
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.StringTokenizer;
import org.json.*;

import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.business.common.ERSQLS;
import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.StringUtil;
import com.velos.esch.service.util.Rlog;
import com.velos.eres.service.util.EJBUtil;

/* import statements */

/**
 * Data Access Class for all Code List Data
 *
 * @author Sonia Sahni
 * @vesion %I%, %G%
 */
/*
 * *************************History********************************* Modified by
 * :Sonia Modified On:06/11/2004 Comment: 1. Added new method resetObject 2.
 * Modified javaDoc comments
 * *************************END****************************************
 */
public class SchCodeDao extends CommonDAO implements java.io.Serializable {
    private ArrayList cId;

    private ArrayList cDesc;

    private ArrayList cSeq;

    private ArrayList cSubType;

    private String cType;

    private int cRows;

    private ArrayList codeHide;

    private String forGroup ;

    private int hiddenRows;

    // GETTER SETTER METHIODS

    public ArrayList getCId() {
        return this.cId;
    }

    public void setCId(ArrayList cId) {
        this.cId = cId;
    }

    public ArrayList getCDesc() {
        return this.cDesc;
    }

    public void setCDesc(ArrayList cDesc) {
        this.cDesc = cDesc;
    }

    public ArrayList getCSeq() {
        return this.cSeq;
    }

    public void setCSeq(ArrayList cSeq) {
        this.cSeq = cSeq;
    }

    public ArrayList getCSubType() {
        return this.cSubType;
    }

    public void setCSubType(ArrayList cSubType) {
        this.cSubType = cSubType;
    }

    public int getCRows() {
        return this.cRows;
    }

    public void setCRows(int cRows) {
        this.cRows = cRows;
    }

    public String getCType() {
        return this.cType;
    }

    public void setCType(String cType) {
        this.cType = cType;
    }

    public void setCDesc(String sDesc) {
        this.cDesc.add(sDesc);
    }

    public void setCId(Integer sCId) {
        this.cId.add(sCId);
    }

    public void setCSeq(Integer sSeq) {
        this.cSeq.add(sSeq);
    }

    public void setCSubType(String cSubType) {
    	String sub = "";

    	sub = cSubType;

    	if (!StringUtil.isEmpty(sub))
    	{

    		sub = sub.trim();
    	}

        this.cSubType.add(sub);
    }

    // End of Getter Setter methods

    /**
     * Default Constructor
     *
     */
    public SchCodeDao() {
        cId = new ArrayList();
        cDesc = new ArrayList();
        cSeq = new ArrayList();
        cSubType = new ArrayList();
        codeHide= new ArrayList();
    }

    /**
     * Gets code list data for a code type and account and populates the class
     * attributes
     *
     * @param cType
     *            Code Type
     * @param usrAccount
     *            user account
     * @return Returns true if successful, false if unsuccessful
     */

    public boolean getCodeValues(String cType, int usrAccount) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        // set code type
        setCType(cType);
        try {
            conn = getConnection();
            pstmt = conn
                    .prepareStatement("select PK_CODELST, trim(CODELST_SUBTYP) CODELST_SUBTYP, CODELST_DESC , CODELST_SEQ,codelst_hide  from sch_codelst where rtrim(CODELST_TYPE) = ? and (fk_account = 0 Or fk_account is null Or fk_account = ?) order by  CODELST_SEQ");

            pstmt.setString(1, getCType());
            pstmt.setInt(2, usrAccount);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setCDesc(rs.getString("CODELST_DESC"));
                setCId(new Integer(rs.getInt("PK_CODELST")));
                setCSeq(new Integer(rs.getInt("CODELST_SEQ")));
                setCSubType(rs.getString("CODELST_SUBTYP"));
                setCodeHide(rs.getString("CODELST_HIDE"));

                rows++;
            }
            setCRows(rows);
            return true;
        } catch (SQLException ex) {
            Rlog.debug("common", "EXCEPTION IN FETCHING FROMCONTROL TABLE "
                    + ex);
            return false;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }

    /**
     * Gets code list data for a code type and populates the class attributes
     *
     * @param cType
     *            Code Type
     * @return Returns true if successful, false if unsuccessful
     */

    public boolean getCodeValues(String cType) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        // set code type
        setCType(cType);
        try {
            conn = getConnection();
            pstmt = conn
                    .prepareStatement("select PK_CODELST, trim(CODELST_SUBTYP) CODELST_SUBTYP, CODELST_DESC , CODELST_SEQ,codelst_hide  from sch_codelst where rtrim(CODELST_TYPE) = ? AND CODELST_HIDE='N' order by  CODELST_SEQ");
            pstmt.setString(1, getCType());
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setCDesc(rs.getString("CODELST_DESC"));
                setCId(new Integer(rs.getInt("PK_CODELST")));
                setCSeq(new Integer(rs.getInt("CODELST_SEQ")));
                setCSubType(rs.getString("CODELST_SUBTYP"));
                setCodeHide(rs.getString("CODELST_HIDE"));
                rows++;
            }
            setCRows(rows);
            return true;
        } catch (SQLException ex) {
            // Debug.println("EXCEPTION IN FETCHING FROMCONTROL TABLE " + ex);
            Rlog.fatal("common", "EXCEPTION IN FETCHING FROMCONTROL TABLE "
                    + ex);
            return false;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }


    /**
     * Returns an HTML string for a dropdown with values from the class
     * attributes
     *
     * @param dName
     *            name of the select tag for the dropdown
     * @return HTML string for a dropdown
     */

    public String toPullDown(String dName) {
        StringBuffer mainStr = new StringBuffer();
        Rlog.debug("common", "SIZE IS" + cDesc.size());
        try {
            int counter = 0;
            String hideStr = "";

            getHiddenCodelstDataForUser();

            mainStr.append("<SELECT NAME=" + dName + ">");
            if (!(dName.equals("cur")) || !(dName == "cur"))
                mainStr.append("<OPTION value='' SELECTED>"+LC.L_Select_AnOption+" </OPTION>");

                for (counter = 0; counter <= cDesc.size() - 1; counter++) {

                	hideStr = getCodeHide(counter) ;

                	if (hideStr.equals("N"))
                	{
                 		mainStr.append("<OPTION data-subtype='"+ cSubType.get(counter) +"' value = " + cId.get(counter) + ">"+ cDesc.get(counter) + "</OPTION>");
                	}
                }

            mainStr.append("</SELECT>");
        } catch (Exception e) {
            return "Exception in toPullDown" + e;
        }
        return mainStr.toString();
    }

    /**
     * Returns an JSON string for creating a YUI dropdown 
     * @return JSON string for a YUI dropdown
     */

    public String toJSON() {
    	JSONObject jsObj = new JSONObject();

        try {          
            getHiddenCodelstDataForUser();
                
           	jsObj.put("codeId", this.cId);
           	jsObj.put("codeDesc", this.cDesc);
            
        } catch (Exception e) {
            return "Exception in toPullDown" + e;
        }
        return jsObj.toString();
    }

    
    /**
     * Returns an HTML string for a dropdown with values from the class
     * attributes
     *
     * @param dName
     *            name of the select tag for the dropdown
     * @param selValue
     *            id of selected value
     * @return HTML string for a dropdown
     */

    public String toPullDown(String dName, int selValue) {
        Integer codeId = null;
        StringBuffer mainStr = new StringBuffer();
        Rlog.debug("common", "SIZE IS" + cDesc.size());
        try {
            int counter = 0;
            String hideStr = "";

            getHiddenCodelstDataForUser();

            mainStr.append("<SELECT NAME=" + dName + ">");
            Integer val = new java.lang.Integer(selValue);

            Rlog.debug("common", "* " + val.toString() + "*");

            if (val.toString().equals("") || val.toString().equals(null)
                    || val.toString().equals("0")) {
                Rlog.debug("common", "* " + val.toString() + "*");
                mainStr
                        .append("<OPTION value ='' SELECTED>Select  an Option</OPTION>");
            }

            for (counter = 0; counter <= cDesc.size() - 1; counter++) {
                codeId = (Integer) cId.get(counter);
                if (selValue == codeId.intValue()) {
                    mainStr.append("<OPTION data-subtype='"+ cSubType.get(counter) +"' value = " + codeId + " SELECTED>"
                            + cDesc.get(counter) + "</OPTION>");
                } else
                {

                	hideStr = getCodeHide(counter) ;

                	if (hideStr.equals("N"))
                	{
                		if("unlock".equals(cSubType.get(counter)))
                			continue;
                		mainStr.append("<OPTION data-subtype='"+ cSubType.get(counter) +"' value = " + codeId + ">"+ cDesc.get(counter) + "</OPTION>");
                	}
                }
            }
            mainStr.append("</SELECT>");
        } catch (Exception e) {
            return "Exception in toPullDown" + e;
        }
        return mainStr.toString();
    }

    // SV, 6/26/04, added as part of fix for bug #1679
    /**
     * Returns an HTML string for a dropdown with values from the class
     * attributes, enabled/disabled(For use in view mode), based on enabled
     * flag.
     *
     * @param dName
     *            name of the select tag for the dropdown
     * @param selValue
     *            id of selected value
     * @param enabled
     *            flag to enable/disable droplist.
     * @return HTML string for a dropdown
     */

    public String toPullDown(String dName, int selValue, boolean enabled) {
        Integer codeId = null;
        StringBuffer mainStr = new StringBuffer();
        String hideStr="";

        Rlog.debug("common", "SIZE IS" + cDesc.size());
        try {

        	getHiddenCodelstDataForUser();

            int counter = 0;
            if (enabled)
                mainStr.append("<SELECT NAME=" + dName + ">");
            else
                mainStr.append("<SELECT NAME=" + dName + " disabled >");

            Integer val = new java.lang.Integer(selValue);

            Rlog.debug("common", "* " + val.toString() + "*");

            if (val.toString().equals("") || val.toString().equals(null)
                    || val.toString().equals("0")) {
                Rlog.debug("common", "* " + val.toString() + "*");
                mainStr
                        .append("<OPTION value ='' SELECTED>Select  an Option</OPTION>");
            }

            for (counter = 0; counter <= cDesc.size() - 1; counter++) {
                codeId = (Integer) cId.get(counter);
                if (selValue == codeId.intValue()) {
                    mainStr.append("<OPTION data-subtype='"+ cSubType.get(counter) +"' value = " + codeId + " SELECTED>"
                            + cDesc.get(counter) + "</OPTION>");
                } else {
                	hideStr = getCodeHide(counter) ;

                	if (hideStr.equals("N"))
                	{
                		if("unlock".equals(cSubType.get(counter)))
                			continue;
                		mainStr.append("<OPTION data-subtype='"+ cSubType.get(counter) +"' value = " + codeId + ">"+ cDesc.get(counter) + "</OPTION>");
                	}
                }
            }
            mainStr.append("</SELECT>");
        } catch (Exception e) {
            return "Exception in toPullDown" + e;
        }
        return mainStr.toString();
    }

    /** *********************************************************************** */
    // by sonia sahni - populates arralists for code data for code PK
    /**
     * Gets code list data for a code id and populates the class attributes
     *
     * @param id
     *            Code Id
     * @return Returns true if successful, false if unsuccessful
     */
    public boolean getCodeValuesById(int id) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            pstmt = conn
                    .prepareStatement("select PK_CODELST, trim(CODELST_SUBTYP) CODELST_SUBTYP, CODELST_DESC , CODELST_SEQ,codelst_hide  from sch_codelst where pk_codelst = ? order by  CODELST_SEQ");
            pstmt.setInt(1, id);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setCDesc(rs.getString("CODELST_DESC"));
                setCId(new Integer(rs.getInt("PK_CODELST")));
                setCSeq(new Integer(rs.getInt("CODELST_SEQ")));
                setCSubType(rs.getString("CODELST_SUBTYP"));
                setCodeHide(rs.getString("CODELST_HIDE"));
                rows++;
            }
            setCRows(rows);
            return true;
        } catch (SQLException ex) {
            Rlog.debug("common", "EXCEPTION IN FETCHING FROMCONTROL TABLE "
                    + ex);
            return false;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }

    /** ********************************************************************** */

    /**
     * Gets code list data for a code type and account and populates the class
     * attributes
     *
     * @param usrAccount
     *            user account
     * @param codelstType
     *            Code Type
     * @return Returns true if successful, false if unsuccessful
     */

    public void getCodelstData(int accId, String codelstType) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            String sql = "select codelst_desc, " + "pk_codelst, "
                    + "codelst_seq, " + "trim(CODELST_SUBTYP) CODELST_SUBTYP, codelst_hide from sch_codelst "
                    + "where fk_account = " + accId + "and codelst_type = '"
                    + codelstType + "'";
            Rlog.debug("codelst",
                    "In CodeDao.getCodelstData The prepared statement is :"
                            + sql);
            pstmt = conn.prepareStatement(sql);
            // pstmt.setInt(1,accId);
            // pstmt.setString(2,codelstType);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setCDesc(rs.getString("CODELST_DESC"));
                setCId(new Integer(rs.getInt("PK_CODELST")));
                setCSeq(new Integer(rs.getInt("CODELST_SEQ")));
                setCSubType(rs.getString("CODELST_SUBTYP"));
                setCodeHide(rs.getString("CODELST_HIDE"));
                rows++;
            }
            Rlog.debug("codelst",
                    "In CodeDao.getCodelstData Number of rows are:" + rows);
            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("codelst", "EXCEPTION IN FETCHING FROM CODE LIST " + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }

    /**
     * Returns code description for a code id from the database
     *
     * @param codeId
     *            Code Id
     * @return returns code description
     */

    public String getCodeDescription(int codeId) {
        int rows = 0;
        String desc = null;

        PreparedStatement pstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            String sql = "select codelst_desc " + "from sch_codelst "
                    + "where pk_codelst = " + codeId;

            Rlog.debug("codelst",
                    "In CodeDao.getCodelstDesc The prepared statement is :"
                            + sql);

            pstmt = conn.prepareStatement(sql);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                desc = rs.getString("CODELST_DESC");
            }

            Rlog.debug("codelst",
                    "In CodeDao.getCodelstDesc Number of rows are:" + rows);
        } catch (SQLException ex) {
            Rlog.fatal("codelst", "EXCEPTION IN FETCHING FROM CODE LST TABLE "
                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

        return desc;
    }

    /**
     * Returns code description for a code id from object's data
     *
     * @param id
     *            Code Id
     * @return returns code description
     */

    public String getDesc(String id) {

        int ret = 0;
        String desc = "";

        try {
            Rlog.debug("codelst", "size of ids *" + cId.size() + "*");

            ret = cId.indexOf(new Integer(id));

            Rlog.debug("codelst", "return value*" + ret + "*");

            if (ret >= 0) {
                desc = (String) cDesc.get(ret);
            } else {
                desc = "-";
            }

        } catch (Exception e) {
            Rlog.fatal("codelst", "EXCEPTION IN ID SEARCH" + e);
        }

        return desc;
    }



    /** Rohit for Bug # 5098
     * Returns SubType for a code id from object's data
     *
     * @param id
     *            Code Id
     * @return returns SubType
     */

    public String getSubType(String id) {

        int ret = 0;
        String subtype = "";

        try {
            Rlog.debug("codelst", "size of ids *" + cId.size() + "*");

            ret = cId.indexOf(new Integer(id));

            Rlog.debug("codelst", "return value*" + ret + "*");

            if (ret >= 0) {
            	subtype = (String) cSubType.get(ret);
            } else {
            	subtype = "-";
            }

        } catch (Exception e) {
            Rlog.fatal("codelst", "EXCEPTION IN ID SEARCH" + e);
        }

        return subtype;
    }




    /**
     * Returns Code Primary key
     *
     * @param codeType
     *            Code Type
     * @param codeDesc
     *            Code Description
     * @param codeSubType
     *            Code SubType
     * @return code primary key/code id
     */

    public int getCodePKByDesc(String codeType, String codeDesc,
            String codeSubType) {

        int rows = 0;
        int codePK = 0;

        PreparedStatement pstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            String sql = "select pk_codelst  from sch_codelst Where trim(upper(CODELST_TYPE)) = trim(upper(?)) and  trim(upper(CODELST_SUBTYP)) = trim(upper(?)) and trim(upper(CODELST_DESC)) = trim(upper(?))";

            pstmt = conn.prepareStatement(sql);

            pstmt.setString(1, codeType);
            pstmt.setString(2, codeSubType);
            pstmt.setString(3, codeDesc);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                codePK = rs.getInt("PK_CODELST");
                rows++;
            }

            Rlog.debug("codelst",
                    "In CodeDao.getCodelstDesc Number of rows are:" + rows);

            return codePK;

        } catch (SQLException ex) {
            Rlog.fatal("codelst", "EXCEPTION IN FETCHING FROM CODE LST TABLE "
                    + ex);
            return 0;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    /**
     * Resets the DAO object's fields to default values.
     */
    // by sonia sahni 06/11/04
    public void resetObject() {
        cId.clear();
        cDesc.clear();
        cSeq.clear();
        cSubType.clear();
        cType = null;
        cRows = 0;
        codeHide.clear();//JM: 08Aug2010, #3356
    }

    /**
     * Returns Code Primary key
     *
     * @param codeType
     *            Code Type
     * @param codeSubType
     *            Code SubType
     * @return code primary key/code id
     */
    public int getCodeId(String codeType, String codeSubType) {
        int rows = 0;
        int codePK = 0;

        PreparedStatement pstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            String sql = "select pk_codelst, codelst_desc  from sch_codelst "
                    + "where trim(upper(CODELST_TYPE)) = trim(upper(?)) "
                    + "and  trim(upper(CODELST_SUBTYP)) = trim(upper(?))";

            pstmt = conn.prepareStatement(sql);

            pstmt.setString(1, codeType);
            pstmt.setString(2, codeSubType);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                codePK = rs.getInt("PK_CODELST");
                rows++;
            }

            Rlog.debug("codelst", "In CodeDao.getCodeId Number of rows are:"
                    + rows);

            return codePK;

        } catch (SQLException ex) {
            Rlog.fatal("codelst", "EXCEPTION IN FETCHING FROM CODE LST TABLE "
                    + ex);
            return 0;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    /**
     * Gets the codeId attribute of the CodeDao object
     *
     * @param type
     *            Code Type , maps to codelst_type in er_codelst
     * @param desc
     *            Code Description , maps to codelst_desc in sch_codelst
     * @return The codeId value
     */
    public int getCodeIdFromDesc(String type, String desc) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        int codeId = 0;
        try {
            conn = getConnection();
            String sql = " select PK_CODELST from sch_codelst where lower(trim(CODELST_TYPE)) = lower(trim(?)) "
                    + " and lower(trim(CODELST_DESC)) = lower(trim(?))";
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, type);
            pstmt.setString(2, desc);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                codeId = rs.getInt("PK_CODELST");
            }
            return codeId;
        } catch (SQLException ex) {
            Rlog.fatal("codelst",
                    "getCodeIdfromDesc :EXCEPTION IN FETCHING FROM SCH_CODELST TABLE "
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
        return codeId;
    }

    /**
     * Returns HTML string for a dropdown with values.
     *
     * @param String
     *            dName name of the select tag for the dropdown
     * @param int
     *            selValue id of selected value
     * @param int
     *            withSelect
     * @return HTML string for a dropdown
     */

    public String toPullDown(String dName, int selValue, int withSelect) {
        Integer codeId = null;
        StringBuffer mainStr = new StringBuffer();
        // Debug.println("SIZE IS" +cDesc.size());
        String hideStr = "";

        try {

        	getHiddenCodelstDataForUser();
            int counter = 0;
            mainStr.append("<SELECT NAME=" + dName + ">");
            Integer val = new java.lang.Integer(selValue);

            for (counter = 0; counter <= cDesc.size() - 1; counter++) {
                codeId = (Integer) cId.get(counter);
                if (selValue == codeId.intValue()) {
                    mainStr.append("<OPTION data-subtype='"+ cSubType.get(counter) +"' value = " + codeId + " SELECTED>"+ cDesc.get(counter) + "</OPTION>");
                } else {

                	hideStr = getCodeHide(counter) ;

                	if (hideStr.equals("N"))
                	{
                		mainStr.append("<OPTION data-subtype='"+ cSubType.get(counter) +"' value = " + codeId + ">"+ cDesc.get(counter) + "</OPTION>");
                	}
                }
            }

            if (val.toString().equals("") || val.toString().equals(null)
                    || val.toString().equals("0")) {
                mainStr
                        .append("<OPTION value ='' SELECTED>"+LC.L_Select_AnOption+"</OPTION>");
            } else
                mainStr.append("<OPTION value ='' >"+LC.L_Select_AnOption+"</OPTION>");

            mainStr.append("</SELECT>");
        } catch (Exception e) {
            return "Exception in toPullDown" + e;
        }
        return mainStr.toString();
    }

    //get database timezone

    public String getDefaultTimeZone() {
        int rows = 0;
        String sql = "";
        PreparedStatement pstmt = null;
        Connection conn = null;
        String returnVal = "";

        try {
            conn = getConnection();
            sql = "Select TZ_VALUE, TZ_DESCRIPTION, TZ_NAME, PK_TZ from sch_timezones ,sch_codelst where codelst_type='DBTimezone' and codelst_subtyp = pk_tz ";
            pstmt = conn.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setCDesc(rs.getString("TZ_DESCRIPTION"));

                returnVal = rs.getString("TZ_DESCRIPTION");


                setCId(new Integer(rs.getInt("PK_TZ")));
                setCSubType(rs.getString("TZ_NAME"));
                rows++;
            }
            setCRows(rows);
            return returnVal;
        } catch (SQLException ex) {
            Rlog.fatal("codelst",
                    "EXCEPTION IN FETCHING FROM TIME ZONES TABLE " + ex);
            return returnVal;
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
    }


	public ArrayList getCodeHide() {
		return codeHide;
	}

	public void setCodeHide(ArrayList codeHide) {
		this.codeHide = codeHide;
	}

	public void setCodeHide(String cHide) {
		String hide = "";

		if (StringUtil.isEmpty(cHide))
		{
			hide= "N";

		}else
		{
			hide = cHide;
		}
		this.codeHide.add(hide);
	}

	public String getCodeHide(int index) {

		String hideStr = "";

		if (codeHide != null && index < codeHide.size() )
    	{
    		hideStr = (String )codeHide.get(index);

    	}
    	else
    	{
    		hideStr = "N";
    	}

		return hideStr ;
	}

	/** Works when following CodeList attributes are set: forGroup and cType
     *
     *
     * forGroup - the user's group for which hidden codelist PKs should be retrieved
     * cType - codelist type key
     * */
    public void getHiddenCodelstDataForUser() {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        int pos = -1;



        if ((! StringUtil.isEmpty( this.cType)) && (!StringUtil.isEmpty( this.forGroup)) && this.cId != null && this.cId.size() > 0 )
        {
        try {
            conn = getConnection();
            String sql = "select FK_CODELST from ER_CODELST_HIDE where CODELST_TYPE = ? and  fk_grp = ? and CODELST_HIDE_TABLE = ?";

            pstmt = conn.prepareStatement(sql);

            pstmt.setString(1, this.cType);
            pstmt.setString(2, this.forGroup);
            pstmt.setInt(3, 2);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                 Integer hId = null;

                 hId = new Integer(rs.getInt("FK_CODELST"));
                 pos = this.cId.indexOf(hId);

                 if (pos >= 0) //the hidden code exists
                 {
                	 //replace the 'hidden' code for this codelist item
                	 if (pos < this.codeHide.size())
                	 {

                		 this.codeHide.set(pos,"Y");
                	 }
                 }

            }

        } catch (SQLException ex) {
            Rlog.fatal("codelst", "EXCEPTION IN getHiddenCodelstDataForUser"
                    + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
        }

    }

	public String getForGroup() {
		return forGroup;
	}

	public void setForGroup(String forGroup) {
		this.forGroup = forGroup;
	}

//JM: 02Jul2008 #3586
    /**
     * Description of the Method
     *
     * @param dName
     *            Description of the Parameter
     * @param selValue
     *            Description of the Parameter
     * @param prop
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public String toPullDown(String dName, int selValue, String prop) {
        Integer codeId = null;
        String hideStr = "";
        boolean selectedFlag = false;


        StringBuffer mainStr = new StringBuffer();


        try {

        	getHiddenCodelstDataForUser();

            int counter = 0;
            mainStr.append("<SELECT NAME=" + dName + " " + prop + ">");
            Integer val = new java.lang.Integer(selValue);

            for (counter = 0; counter <= cDesc.size() - 1; counter++) {
                codeId = (Integer) cId.get(counter);

                if (selValue == codeId.intValue())
                {
                    mainStr.append("<OPTION data-subtype='"+ cSubType.get(counter) +"'value = " + codeId + " SELECTED>" + cDesc.get(counter) + "</OPTION>");
                    selectedFlag = true;
                }
                else
                {

                	hideStr = getCodeHide(counter) ;

                	if (hideStr.equals("N"))
                	{
                		mainStr.append("<OPTION data-subtype='"+ cSubType.get(counter) +"' value = " + codeId + ">"+ cDesc.get(counter) + "</OPTION>");
                	}

                }
            }
            if (val.toString().equals("") || val.toString().equals(null)
                    || val.toString().equals("0") || selectedFlag == false ) {
                mainStr
                        .append("<OPTION value ='' SELECTED>"+LC.L_Select_AnOption+"</OPTION>");
            } else {
                mainStr.append("<OPTION value ='' >"+LC.L_Select_AnOption+"</OPTION>");
            }

            mainStr.append("</SELECT>");
        } catch (Exception e) {
            return "Exception in toPullDown" + e;
        }
        return mainStr.toString();
    }

    public String toPullDownWithShortDescription(String dName, int selValue, String prop) {
        Integer codeId = null;
        String hideStr = "";
        boolean selectedFlag = false;
        StringBuffer mainStr = new StringBuffer();

        try {
            getHiddenCodelstDataForUser();
            int counter = 0;
            mainStr.append("<SELECT id='" + dName + "' NAME=" + dName + " " + prop + ">");
            Integer val = new java.lang.Integer(selValue);

            for (counter = 0; counter <= cSubType.size() - 1; counter++) {
                codeId = (Integer) cId.get(counter);

                if (selValue == codeId.intValue())
                {
                    mainStr.append("<OPTION data-subtype='"+ cSubType.get(counter) +"' value = " + codeId + " SELECTED>" + cSubType.get(counter) + "</OPTION>");
                    selectedFlag = true;
                }
                else
                {
                    hideStr = getCodeHide(counter) ;
                    if (hideStr.equals("N"))
                    {
                        mainStr.append("<OPTION data-subtype='"+ cSubType.get(counter) +"' value = " + codeId + ">"+ cSubType.get(counter) + "</OPTION>");
                    }
                }
            }
            if (val.toString().equals("") || val.toString().equals(null)
                    || val.toString().equals("0") || selectedFlag == false ) {
                mainStr
                        .append("<OPTION value ='' SELECTED>"+LC.L_Select_AnOption+"</OPTION>");
            } else {
                mainStr.append("<OPTION value ='' >"+LC.L_Select_AnOption+"</OPTION>");
            }
            mainStr.append("</SELECT>");
        } catch (Exception e) {
            return "Exception in toPullDown" + e;
        }
        return mainStr.toString();
    }

    /**  removes arraylists entries for a subtype*/
    public void removeForSubType(String subtype)
    {
    	int idx=-1;

    	if (this.cSubType !=null )
    	{
    		idx = this.cSubType.indexOf(subtype);

    		if (idx >=0)
    		{
    			//remove from all arrayLists including the subtype

    			if (this.cDesc.size() > idx)
    			{
    				this.cDesc.remove(idx);
    			}

    			if (this.cId.size() > idx)
    			{
    				this.cId.remove(idx);
    			}

    			if (this.codeHide.size() > idx)
    			{
    				this.codeHide.remove(idx);
    			}

    			if (this.cSeq.size() > idx)
    			{
    				this.cSeq.remove(idx);
    			}

    			if (this.cSubType.size() > idx)
    			{
    				this.cSubType.remove(idx);
    			}


    		}

    	}


    }
//////////////////////////////////////////////////

    /**
     * Gets code list data for a code type and study role . populates the class attributes.
     * if role is empty, gets values without role specified - custimcol=default_data
     *
     * @param codeType
     * @param roleCodelstPK Primary key of codelist record for the role. Where study role is not available, it is blank
     * @return boolean
     */

    
    public boolean getCodeValuesForStudyRole(String codeType,String roleCodelstPK)
    {
    	String roleSubtype = "";
    	int codePk = 0;
    	int rows = 0;


    	codePk = EJBUtil.stringToNum(roleCodelstPK);

    	if (codePk >0 )
    	{
    		//KM-#5882
    		roleSubtype  = getCodeSubtypeRole(codePk );

    		if (! StringUtil.isEmpty(roleSubtype))
    		{

    			getCodeValuesFilterStudyRole(codeType, roleSubtype) ;

    			rows = getCRows() ;

    			if (getCRows() <= 0 || getCRows() == getHiddenRows() )
    			{

    				rows = 0;
    			}

    		}
    		else
    		{
    			rows = 0;
    		}
    	}
    	else
    	{

    		rows = 0;
    	}

    	if (rows ==0) //get Default Data where custom col is null
    	{
    		getCodeValuesFilterStudyRole(codeType, "default_data") ;

    	}

    	return true;
    }

    /**
     * Populates the object with code list data for a code_type and its parent code
     * Method to be used to establist parent child relationships between two codes
     * Supports multiple values in codelst_custom_col1 (values must be comma separated)
     *
     * @param codeType
     *            Code Type of the child code
     * @param customCol1
     *            will be matched with custom_col1
         *
     */
    public void getCodeValuesFilterCustom1(String childCodeType, String customCol1) {

        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String rowHide="";
        // set code type
        setCType(childCodeType);
        try {
        	if (StringUtil.isEmpty(customCol1))
        	{
        		customCol1="";
        	}

        	customCol1 = "%," + customCol1 +",%";

            conn = getConnection();
            pstmt = conn.prepareStatement(com.velos.eres.business.common.ERSQLS.schCodeLstWithCustom_1);


            pstmt.setString(1, getCType());
            pstmt.setString(2, customCol1);
            ResultSet rs = pstmt.executeQuery();


            while (rs.next()) {
                setCDesc(rs.getString("CODELST_DESC"));
                setCId(new Integer(rs.getInt("PK_CODELST")));
                setCSeq(new Integer(rs.getInt("CODELST_SEQ")));
                setCSubType(rs.getString("CODELST_SUBTYP"));

                setCodeHide(rs.getString("CODELST_HIDE"));

                rowHide= rs.getString("CODELST_HIDE");

                if (! StringUtil.isEmpty(rowHide) && rowHide.equals("Y"))
                {
                	incrementHiddenRows();
                }

                rows++;
            }

            setCRows(rows);

        } catch (SQLException ex) {
            Rlog.fatal("codelst", "EXCEPTION IN FETCHING FROM SCH_CODELST-schCodeLstWithCustom_1" + ex);

        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
    }


    /**
     * public String getCodeSubType (int codeId)  - returns code_subtyp for a codeid
     *
     * @param codeId
     * @return The codeSubtype value
     */

    public String getCodeSubtype(int codeId) {
        String subtyp = null;

        PreparedStatement pstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();

            String sql = "select trim(CODELST_SUBTYP) CODELST_SUBTYP from sch_codelst  where pk_codelst = ?";

            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, codeId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
         	   subtyp = rs.getString("codelst_subtyp");

            }

        } catch (SQLException ex) {
            Rlog.fatal("codelst", "EXCEPTION IN FETCHING FROM CODE LIST TABLE:getCodeSubtype: "
                    + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }

        return subtyp;
    }

    
    /**
     * public String getCodeSubTypeRole (int codeId)  - returns code_subtyp for a codeid
     *
     * @param codeId
     * @return The codeSubtype value
     */
    
    //KM - #5882
    public String getCodeSubtypeRole(int codeId) {
        String subtyp = null;

        PreparedStatement pstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();

            String sql = "select trim(CODELST_SUBTYP) CODELST_SUBTYP from er_codelst  where pk_codelst = ?";

            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, codeId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
         	   subtyp = rs.getString("codelst_subtyp");

            }

        } catch (SQLException ex) {
            Rlog.fatal("codelst", "EXCEPTION IN FETCHING FROM CODE LIST TABLE:getCodeSubtypeRole: "
                    + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }

        return subtyp;
    }
    

    public int getHiddenRows() {
		return hiddenRows;
	}

	public void setHiddenRows(int hiddenRows) {
		this.hiddenRows = hiddenRows;
	}

	public void incrementHiddenRows() {
		this.hiddenRows = this.hiddenRows+1;
	}

	/**
     * Populates the object with code list data for a code_type and a study yteam role
     * Supports multiple values in codelst_study_role (values must be comma separated). For codes where study team role is not specified, a value
     * of default_data should be entered.
     *
     * @param codeType
     *            Code Type of the child code
     * @param role
     *            will be matched with codelst_study_role
     *
     */
    
	public boolean isAllowedUnlock(String studyRoleCodePk)
	{
		String str=getCodeSubtypeRole(StringUtil.stringToNum(studyRoleCodePk));
		System.out.println("SchCodeDao.isAllowedUnlock :\n studyRoleCodePk: "+studyRoleCodePk+"\nsubType:"+str+":");
		return matchCodeListType(str);
	}
	public boolean isSoftLock(int id)
	{ 
		if(id>0 && getCodeSubtype(id).equalsIgnoreCase("soft_lock"))
		{ return true;
		}
	return false;	
	}
    public boolean matchCodeListType(String subType)
    {
    	int noTRole=0;
    	noTRole=isTeamRoleNotRequired();
    	System.out.println("noTrole-"+noTRole);
    	if(noTRole==0){
    		return true;
    	}
    	StringTokenizer roles=null;
        PreparedStatement pstmt = null;
        Connection conn = null;

        try {
        	conn = getConnection();
        	pstmt = conn.prepareStatement("select trim(CODELST_STUDY_ROLE) CODELST_STUDY_ROLE from sch_codelst where CODELST_TYPE = 'fillformstat' and CODELST_SUBTYP='unlock' and codelst_hide<>'Y' "
            		/*and codelst_hide=?*/+" order by   CODELST_SEQ"); /*  and codelst_hide<>'y' has been added if Unlock is  hidden  from front end  */
            System.out.println("-------------------------------------------------------------------------\n"
        	+"select trim(CODELST_STUDY_ROLE) CODELST_STUDY_ROLE from sch_codelst where CODELST_TYPE = 'fillformstat' and CODELST_SUBTYP='unlock' "
                 /*and codelst_hide=?*/+" order by   CODELST_SEQ");      
               ResultSet  rs = pstmt.executeQuery();
                if(rs.next())
                {
                	roles = new StringTokenizer(rs.getString("CODELST_STUDY_ROLE"),",");
                	while(roles.hasMoreTokens())
                	{
                		if(roles.nextToken().equals(subType))
                		{
                			return true;
                		}
                	}
                }
                else{
                	return isSoftLockAllowed(subType);
                	//return false;
                }
        	
        } catch (SQLException ex) {
        	System.out.println(ex.getMessage());
            Rlog.fatal("codelst", "EXCEPTION IN FETCHING FROMCONTROL TABLE "
                    + ex);
            return false;
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
    	
    	return false;
    }

    
	public void getCodeValuesFilterStudyRole(String childCodeType, String role) {

        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String rowHide="";
        // set code type
        setCType(childCodeType);
        try {
        	if (StringUtil.isEmpty(role))
        	{
        		role="";
        	}

        	role = "%," + role +",%";



            conn = getConnection();



            pstmt = conn.prepareStatement(ERSQLS.schCodeLstWithStudyRole);

            pstmt.setString(1, getCType());
            pstmt.setString(2, role);
            ResultSet rs = pstmt.executeQuery();


            while (rs.next()) {
                setCDesc(rs.getString("CODELST_DESC"));
                setCId(new Integer(rs.getInt("PK_CODELST")));
                setCSeq(new Integer(rs.getInt("CODELST_SEQ")));
                setCSubType(rs.getString("CODELST_SUBTYP"));

                setCodeHide(rs.getString("CODELST_HIDE"));

                rowHide= rs.getString("CODELST_HIDE");

                if (! StringUtil.isEmpty(rowHide) && rowHide.equals("Y"))
                {
                	incrementHiddenRows();
                }

                rows++;
            }
            setCRows(rows);

        } catch (SQLException ex) {
            Rlog.fatal("codelst", "EXCEPTION IN FETCHING FROM SCH_CODELST-getCodeValuesFilterStudyRole" + ex);

        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
    }
    
  /**
   * 
   * @param cType
   * @return
   */
   public String toPullDownCodeListchckBox(String cType) {
       StringBuffer mainStr = new StringBuffer();

       PreparedStatement pstmt = null;
       Connection conn = null;

       // set code type
       setCType(cType);
       try {
           conn = getConnection();
           pstmt = conn.prepareStatement("select distinct PK_CODELST, trim(CODELST_SUBTYP) CODELST_SUBTYP, CODELST_DESC , CODELST_SEQ,codelst_hide  from sch_codelst where rtrim(CODELST_TYPE) = rtrim(?) order by  CODELST_SEQ");
           pstmt.setString(1, getCType());
           ResultSet rs = pstmt.executeQuery();
           mainStr.append("<input class = \"allcovTypeOpt\" id=\"CovType\" type=\"checkbox\" id=\"CovType\" name=\"CovType\" value=\""+LC.L_All+"\"/>"+LC.L_All+"&nbsp;");
           int counter=1;
           while (rs.next()) {
        	   if(counter%5!=0)
        		   mainStr.append("<input class = \"covTypeOpt\" type=\"checkbox\" id=\"CovType\" name=\"CovType\" value=\""+new Integer(rs.getInt("PK_CODELST"))+"\"/>"+rs.getString("CODELST_SUBTYP")+"&nbsp;");
        	   else
        		   mainStr.append("<br><input class = \"covTypeOpt\" type=\"checkbox\" id=\"CovType\" name=\"CovType\" value=\""+new Integer(rs.getInt("PK_CODELST"))+"\"/>"+rs.getString("CODELST_SUBTYP"));        		   
        	   counter++;
           }
               
       }catch (SQLException ex) {
           Rlog.fatal("common", "EXCEPTION IN FETCHING FROMCONTROL TABLE "
                   + ex);          
       }catch (Exception e) {
    	   Rlog.fatal("common","Exception in toPullDown" + e);
       }finally {
           try {
               if (pstmt != null)
                   pstmt.close();
           } catch (Exception e) {
           }
           try {
               if (conn != null)
                   conn.close();
           } catch (Exception e) {
           }
       }
       return mainStr.toString();
   }
public int isTeamRoleNotRequired(){
	System.out.println("isTeamRoleNotRequired");
	Connection conn = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    String sql="";
    int TRoleFlag=0;
    try{
    	conn = CommonDAO.getConnection();
    	sql=" select count(*) AS TROLE_FLAG from sch_codelst where codelst_type='fillformstat' and codelst_hide <>'Y' and codelst_study_role <>'default_data' and codelst_study_role  <> 'default_data,' ";
		pstmt = conn.prepareStatement(sql);
		rs=pstmt.executeQuery();
		while (rs.next()) {
			TRoleFlag=rs.getInt("TROLE_FLAG");
		}
    }catch(SQLException sqlEx){
    	sqlEx.printStackTrace();
    }finally{
    	try {
			rs.close();
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }
	return TRoleFlag;
}
public boolean isSoftLockAllowed(String subType){
	Connection conn = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    String sql="";
    StringTokenizer roles=null;
    try{
    	conn = CommonDAO.getConnection();
    	sql=" select trim(CODELST_STUDY_ROLE) CODELST_STUDY_ROLE from sch_codelst where CODELST_TYPE = 'fillformstat' and CODELST_SUBTYP='soft_lock' order by   CODELST_SEQ ";
		pstmt = conn.prepareStatement(sql);
		rs=pstmt.executeQuery();
		while (rs.next()) {
			roles = new StringTokenizer(rs.getString("CODELST_STUDY_ROLE"),",");
         	while(roles.hasMoreTokens())
         	{
         		if(roles.nextToken().equals(subType))
         		{
         			System.out.println("------------------------------"+
         					"-----------------------------Code Dao .matchCodeListType--- returning true.----------\n");
         			return true;
         		}
         	}
		}
    }catch(SQLException sqlEx){
    	sqlEx.printStackTrace();
    }finally{
    	try {
			rs.close();
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }
	
	return false;
}
public int   getStudyRolePK(int studyId ,int  userId){
	int codePk=0;
	 PreparedStatement pstmt = null;
        Connection conn = null;
        String subType=null;
        String sql="";
        try {
        	conn = getConnection();
        	sql="select fk_codelst_tmrole from er_studyteam where fk_user=? and fk_study=?";
            pstmt = conn.prepareStatement(sql);
            System.out.println("----------------------------------------------------------------\n"
            		+"select fk_codelst_tmrole from er_studyteam where fk_user=? and fk_study=?");
            pstmt.setInt(1, userId);
            pstmt.setInt(2,studyId);
            ResultSet rs = pstmt.executeQuery();
            if(rs.next()){
            	codePk=rs.getInt("fk_codelst_tmrole")	;
            	
            }else{
            	pstmt = conn.prepareStatement(" SELECT fk_codelst_st_role, (SELECT codelst_subtyp FROM sch_codelst WHERE pk_codelst=fk_codelst_st_role  ) AS sup_codelst_subtyp FROM er_user, er_grps WHERE pk_user     ="+userId+" AND fk_grp_default=pk_grp");
            	 rs = pstmt.executeQuery();
            	 while(rs.next()){
            		 codePk=rs.getInt("fk_codelst_st_role") ;
            	 }
            }
        }catch (SQLException ex) {
            // Debug.println("EXCEPTION IN FETCHING FROMCONTROL TABLE " + ex);
            Rlog.fatal("codelst", "EXCEPTION IN FETCHING FROMCONTROL TABLE "
                    + ex);
            return 0;
           
        }finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
	return codePk;
}

public boolean getCodeValuesMilesCust(String cType, int usrAccount) {
    int rows = 0;
    PreparedStatement pstmt = null;
    Connection conn = null;

    // set code type
    setCType(cType);
    try {
        conn = getConnection();
        pstmt = conn
                .prepareStatement("select PK_CODELST, trim(CODELST_SUBTYP) CODELST_SUBTYP, CODELST_DESC , CODELST_SEQ,codelst_hide  from sch_codelst where rtrim(CODELST_TYPE) = ? and CODELST_HIDE='N' and (fk_account = 0 Or fk_account is null Or fk_account = ?) order by  CODELST_SEQ");

        pstmt.setString(1, getCType());
        pstmt.setInt(2, usrAccount);

        ResultSet rs = pstmt.executeQuery();

        while (rs.next()) {
            setCDesc(rs.getString("CODELST_DESC"));
            setCId(new Integer(rs.getInt("PK_CODELST")));
            setCSeq(new Integer(rs.getInt("CODELST_SEQ")));
            setCSubType(rs.getString("CODELST_SUBTYP"));
            setCodeHide(rs.getString("CODELST_HIDE"));

            rows++;
        }
        setCRows(rows);
        return true;
    } catch (SQLException ex) {
        Rlog.debug("common", "EXCEPTION IN FETCHING FROMCONTROL TABLE "
                + ex);
        return false;
    } finally {
        try {
            if (pstmt != null)
                pstmt.close();
        } catch (Exception e) {
        }
        try {
            if (conn != null)
                conn.close();
        } catch (Exception e) {
        }
    }
}

public boolean getCodeValuesForStudyRoleHideCustom(String codeType,String roleCodelstPK)
{
	String roleSubtype = "";
	int codePk = 0;
	int rows = 0;


	codePk = EJBUtil.stringToNum(roleCodelstPK);

	if (codePk >0 )
	{
		//KM-#5882
		roleSubtype  = getCodeSubtypeRole(codePk );

		if (! StringUtil.isEmpty(roleSubtype))
		{

			getCodeValuesFilterStudyRoleHideCustom(codeType, roleSubtype) ;

			rows = getCRows() ;

			if (getCRows() <= 0 || getCRows() == getHiddenRows() )
			{

				rows = 0;
			}

		}
		else
		{
			rows = 0;
		}
	}
	else
	{

		rows = 0;
	}

	if (rows ==0) //get Default Data where custom col is null
	{
		getCodeValuesFilterStudyRoleHideCustom(codeType, "default_data") ;

	}

	return true;
}

public void getCodeValuesFilterStudyRoleHideCustom(String childCodeType, String role) {

    int rows = 0;
    PreparedStatement pstmt = null;
    Connection conn = null;
    String rowHide="";
    // set code type
    setCType(childCodeType);
    try {
    	if (StringUtil.isEmpty(role))
    	{
    		role="";
    	}

    	role = "%," + role +",%";



        conn = getConnection();



        pstmt = conn.prepareStatement("select PK_CODELST, CODELST_SUBTYP, CODELST_DESC , CODELST_SEQ  ,codelst_hide from sch_codelst " +
        		" where rtrim(CODELST_TYPE) = ? and codelst_hide='N' and (',' || CODELST_STUDY_ROLE || ',' like ?) order by   CODELST_SEQ ");
  
        pstmt.setString(1, getCType());
        pstmt.setString(2, role);
        ResultSet rs = pstmt.executeQuery();


        while (rs.next()) {
            setCDesc(rs.getString("CODELST_DESC"));
            setCId(new Integer(rs.getInt("PK_CODELST")));
            setCSeq(new Integer(rs.getInt("CODELST_SEQ")));
            setCSubType(rs.getString("CODELST_SUBTYP"));

            setCodeHide(rs.getString("CODELST_HIDE"));

            rowHide= rs.getString("CODELST_HIDE");

            if (! StringUtil.isEmpty(rowHide) && rowHide.equals("Y"))
            {
            	incrementHiddenRows();
            }

            rows++;
        }
        setCRows(rows);

    } catch (SQLException ex) {
        Rlog.fatal("codelst", "EXCEPTION IN FETCHING FROM SCH_CODELST-getCodeValuesFilterStudyRole" + ex);

    } finally {
        try {
            if (pstmt != null) {
                pstmt.close();
            }
        } catch (Exception e) {
        }
        try {
            if (conn != null) {
                conn.close();
            }
        } catch (Exception e) {
        }
    }
}

}
