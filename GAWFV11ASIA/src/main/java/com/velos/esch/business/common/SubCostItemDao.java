/*
 *  Classname : SubCostItemDao
 *
 * Version information : 1.0
 *
 * Date: 01/19/2011
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Sampada
 */

package com.velos.esch.business.common;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import com.velos.eres.service.util.StringUtil;
import com.velos.esch.service.util.Rlog;

/* End of Import Statements */

public class SubCostItemDao extends com.velos.esch.business.common.CommonDAO implements java.io.Serializable {

    ArrayList itemIds;

    ArrayList calendarIds;
   
    ArrayList itemCategory;
    
    ArrayList itemNames;
    
    ArrayList itemCosts;

    ArrayList itemCostTypes;
    
    ArrayList itemUnits;
    
    ArrayList flags; //Flag to separate Item from Visit
    
    ArrayList fkVisits; 
    
    ArrayList creators;
    
    ArrayList studyNum;

    ArrayList calName;   
    
    ArrayList visitName; // name of defined visit.

    // //////////////////////////

    public SubCostItemDao() {
        itemIds = new ArrayList();
        calendarIds = new ArrayList();
        itemCategory = new ArrayList();
        itemNames = new ArrayList();
        itemCosts = new ArrayList();
        itemCostTypes = new ArrayList();
        itemUnits = new ArrayList();
        flags = new ArrayList();
        fkVisits = new ArrayList();
        creators = new ArrayList();
        studyNum = new ArrayList();
        calName = new ArrayList();
        visitName = new ArrayList();
        fkVisits = new ArrayList();
    }

    // Getter and Setter methods

    public ArrayList getItemIds() {
        return this.itemIds;
    }

    public void setItemIds(ArrayList itemIds) {
        this.itemIds = itemIds;
    }
    
    public void setItemIds(Integer item_id) {
        itemIds.add(item_id);
    }
    
    public ArrayList getCalendarIds() {
        return this.calendarIds;
    }

    public void setCalendarIds(ArrayList calendarIds) {
        this.calendarIds = calendarIds;
    }
    
    public void setCalendarIds(Integer calendarId) {
    	calendarIds.add(calendarId);
    }
    
    public ArrayList getItemNames() {
        return this.itemNames;
    }

    public void setItemNames(ArrayList itemNames) {
        this.itemNames = itemNames;
    }

    public void setItemNames(String name) {
    	itemNames.add(name);
    }

    public ArrayList getItemCosts() {
        return this.itemCosts;
    }
    
    public void setItemCosts(ArrayList costs) {
        this.itemCosts = costs;
    }
    
    public void setItemCosts(String cost) {
    	itemCosts.add(cost);
    }   
    
    public ArrayList getItemCostTypes() {
        return this.itemCostTypes;
    }

    public void setItemCostTypes(ArrayList costTypes) {
        this.itemCostTypes = costTypes;
    }
    
    public void setCostDescs(String costType) {
    	itemCostTypes.add(costType);
    }

    public ArrayList getItemUnits() {
        return this.itemUnits;
    }
    
    public void setItemUnits(ArrayList units) {
        this.itemUnits = units;
    }

    public void setItemUnits(String unit) {
    	itemUnits.add(unit);
    }
        
    public ArrayList getFlags() {
        return this.flags;
    }

    public void setFlag(String flag) {
    	flags.add(flag);
    }

    public void setFlag(ArrayList flags) {
        this.flags = flags;
    }
    
    public ArrayList getVisitIds() {
        return (this.fkVisits);
    }

    public void setVisitId(ArrayList visitIds) {
        this.fkVisits = visitIds;
    }

    public void setVisitId(Integer eventVisit) {
        this.fkVisits.add(eventVisit);
    }

    public ArrayList getCreators() {
        return this.creators;
    }

    public void setCreators(ArrayList creators) {
        this.creators = creators;
    }

    public void setCreators(String creator) {
        this.creators.add(creator);
    }    
       
    public ArrayList getVisitName() {
        return (this.visitName);
    }

    public void setVisitName(ArrayList visitName) {
        this.visitName = visitName;
    }

    public void setVisitName(String visitName) {
        this.visitName.add(visitName);
    }

    public ArrayList getStudyNum() {
        return this.studyNum;
    }

    public void setStudyNum(ArrayList studyNum) {
        this.studyNum = studyNum;

    }
    public void setStudyNums(String studynum) {
        studyNum.add(studynum);
    }
    
    public ArrayList getCalName() {
        return this.calName;
    }

    public void setCalName(ArrayList calName) {
        this.calName = calName;
    }

    public void setCalNames(String calname) {
        calName.add(calname);
    }

    public ArrayList getFkVisits() {
        return fkVisits;
    }

    public void setFkVisit(ArrayList fkVisits) {
        this.fkVisits = fkVisits;
    }

    public void setFkVisit(String fkVisit) {
        this.fkVisits.add(fkVisit);
    }

    public ArrayList getItemCategory() {
        return this.itemCategory;
    }

    public void setItemCategory(ArrayList itemCategory) {
        this.itemCategory = itemCategory;
    }

    public void setItemCategory(String itemCat) {
    	itemCategory.add(itemCat);
    }

	// end of getter and setter methods

    public void getProtSelectedAndGroupedItems(int protocolId) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            StringBuffer sql = new StringBuffer();
            sql.append(" SELECT 0 as PK_SUBCOST_ITEM,fk_protocol as FK_CALENDAR, ")
            .append(" visit_name as NAME, 0 as COST, 0 as UNIT,")
            .append(" 'V' AS flag, pk_protocol_visit AS FK_VISIT, VISIT_NO, DISPLACEMENT, ")
            .append(" 0 as FK_CODELST_CATEGORY")
            .append(" FROM esch.sch_protocol_visit WHERE fk_protocol = ? ")
            .append(" UNION ")
            .append(" SELECT ")
            .append(" PK_SUBCOST_ITEM, FK_CALENDAR,")
            .append(" SUBCOST_ITEM_NAME as NAME, SUBCOST_ITEM_COST as COST, SUBCOST_ITEM_UNIT as UNIT, ")
            .append(" 'I' AS flag, v.FK_PROTOCOL_VISIT as FK_VISIT, 0 AS VISIT_NO, 0 AS DISPLACEMENT, ")
            .append(" FK_CODELST_CATEGORY")
            .append(" FROM esch.SCH_SUBCOST_ITEM i left outer join esch.SCH_SUBCOST_ITEM_VISIT v on")
            .append(" (i.PK_SUBCOST_ITEM = v.FK_SUBCOST_ITEM) WHERE i.FK_CALENDAR = ? ")
            .append(" ORDER BY flag DESC, VISIT_NO, PK_SUBCOST_ITEM, NAME ");/*YK Bug 5821 & # 5818*/
            pstmt = conn.prepareStatement(sql.toString());
            pstmt.setInt(1, protocolId);
            pstmt.setInt(2, protocolId);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                setItemIds(Integer.parseInt(StringUtil.trueValue(rs.getString("PK_SUBCOST_ITEM")).trim()));
                setCalendarIds(Integer.valueOf(rs.getString("FK_CALENDAR")));
                setItemNames(rs.getString("NAME"));
                setItemCosts(rs.getString("COST"));
                setItemUnits(rs.getString("UNIT"));
                setFlag(rs.getString("flag"));
                String sValue = rs.getString("FK_VISIT");
                if (sValue == null) { sValue = "0"; }
                setVisitId(new Integer (sValue.trim()));
                //setDisplacement(rs.getString("DISPLACEMENT"));
                setItemCategory(rs.getString("FK_CODELST_CATEGORY"));
                
            }
        } catch (Exception ex) {
            Rlog.fatal("subCostItem",
                       "SubCostItemDao.getProtSelectedAndGroupedItems: "+ ex);
        } finally {
            try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
            try { if (conn != null) conn.close(); } catch (Exception e) {}
        }
    }

    //KM-#5949
    public int deleteSubCostItems(ArrayList visitIds, ArrayList itemIds, int user) {

        CallableStatement cstmt = null;
        Rlog.debug("subCostItem",
                "In deleteSubCostItems in SubCostItemDao line number 0");
        int newProtocol = -1;
        
        Connection conn = null;
        try {
        	conn = getConnection();

            ArrayDescriptor inVisitIds = ArrayDescriptor.createDescriptor("ARRAY_STRING", conn);
            ARRAY delVisitIdsArray = new ARRAY(inVisitIds, conn, visitIds.toArray());

            ArrayDescriptor inItemIds = ArrayDescriptor.createDescriptor("ARRAY_STRING", conn);
            ARRAY delItemIdsArray = new ARRAY(inItemIds, conn, itemIds.toArray());

            cstmt = conn.prepareCall("{call pkg_subCostItem.sp_delete_visit_subCostItems(?,?,?)}");

            cstmt.setArray(1, delVisitIdsArray);
            cstmt.setArray(2, delItemIdsArray);
            //KM-#5949
            cstmt.setInt(3, user);
            cstmt.execute();
            newProtocol = 0;
        }

        catch (SQLException ex) {
            Rlog
                    .fatal(
                            "eventdef","In deleteEvtOrVisits() EXCEPTION IN calling the proc..."
                                    + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return newProtocol;
    }

//end of class
}
