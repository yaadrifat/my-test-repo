/*
 * Classname : EventcostBean
 * 
 * Version information: 1.0
 *
 * Date: 06/20/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh Khurana 
 */

package com.velos.esch.business.eventcost.impl;

/* Import Statements */
import java.io.Serializable;
import java.text.DecimalFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.esch.service.util.Rlog;
import com.velos.esch.service.util.StringUtil;

/* End of Import Statements */
@Entity
@Table(name = "SCH_EVENTCOST")
public class EventcostBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3256719576581879863L;

    /**
     * eventcostId
     */
    public int eventcostId;

    /**
     * eventcostValue
     */
    public Double eventcostValue;

    /**
     * eventId
     */
    public Integer eventId;

    /**
     * eventcostDescId
     */
    public Integer eventcostDescId;

    /**
     * currencyId
     */
    public Integer currencyId;

    /*
     * the record creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * the IP Address
     */
    public String ipAdd;

    // GETTER SETTER METHODS
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SCH_EVENTCOST_SEQ", allocationSize=1)
    @Column(name = "PK_EVENTCOST")
    public int getEventcostId() {
        return this.eventcostId;
    }

    public void setEventcostId(int eventcostId) {
        this.eventcostId = eventcostId;
    }

    @Column(name = "EVENTCOST_VALUE")
    public String getEventcostValue() {
        String ret = "";

        try {
            Rlog.debug("eventcost",
                    "*************EventcostBean.getEventcostValue eventcostValue "
                            + eventcostValue);

            // double db = eventcostValue.doubleValue();

            DecimalFormat df = new DecimalFormat("#########0.00");
            Rlog.debug("eventcost",
                    "*************EventcostBean.getEventcostValue after formatting "
                            + df.format(eventcostValue));
            ret = df.format(eventcostValue);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return ret;

    }

    public void setEventcostValue(String eventcostValue) {
        if (eventcostValue != null) {
            this.eventcostValue = new Double(eventcostValue);
        }
    }

    @Column(name = "FK_EVENT")
    public String getEventId() {
        return StringUtil.integerToString(this.eventId);
    }

    public void setEventId(String eventId) {
        if (eventId != null) {
            this.eventId = Integer.valueOf(eventId);
        }
    }

    @Column(name = "FK_COST_DESC")
    public String getEventcostDescId() {
        return StringUtil.integerToString(this.eventcostDescId);
    }

    public void setEventcostDescId(String eventcostDescId) {
        if (eventcostDescId != null) {
            this.eventcostDescId = Integer.valueOf(eventcostDescId);
        }
    }

    @Column(name = "FK_CURRENCY")
    public String getCurrencyId() {
        return StringUtil.integerToString(this.currencyId);
    }

    public void setCurrencyId(String currencyId) {
        if (currencyId != null) {
            this.currencyId = Integer.valueOf(currencyId);
        }
    }

    @Column(name = "Creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // / END OF GETTER SETTER METHODS

    /**
     * returns the state holder object associated with this eventcost
     */

    /*
     * public EventcostStateKeeper getEventcostStateKeeper() {
     * Rlog.debug("eventcost", "EventcostBean.getEventcostStateKeeper"); return
     * new EventcostStateKeeper(getEventcostId(), getEventcostValue(),
     * getEventId(), getEventcostDescId(), getCurrencyId(), getCreator(),
     * getModifiedBy(), getIpAdd()); }
     */

    /**
     * sets up a state keeper containing details of the eventcost
     * 
     * @param ecsk
     */
    /*
     * public void setEventcostStateKeeper(EventcostStateKeeper ecsk) {
     * GenerateId Id = null;
     * 
     * try {
     * 
     * Connection conn = null; conn = getConnection(); Rlog.debug("eventcost",
     * "EventcostBean.setEventcostStateKeeper() Connection :" + conn);
     * eventcostId = Id.getId("SCH_EVENTCOST_SEQ", conn); conn.close();
     * 
     * setEventcostValue(ecsk.getEventcostValue());
     * setEventId(ecsk.getEventId());
     * setEventcostDescId(ecsk.getEventcostDescId());
     * setCurrencyId(ecsk.getCurrencyId()); setCreator(ecsk.getCreator());
     * setModifiedBy(ecsk.getModifiedBy()); setIpAdd(ecsk.getIpAdd()); } catch
     * (Exception e) { System.out .println("Error in setEventcostStateKeeper()
     * in EventcostBean " + e); } }
     */

    /**
     * updates the eventcost details
     * 
     * @param ecsk
     * @return 0 if successful; -2 for exception
     */
    public int updateEventcost(EventcostBean ecsk) {
        try {

            setEventcostValue(ecsk.getEventcostValue());
            setEventId(ecsk.getEventId());
            setEventcostDescId(ecsk.getEventcostDescId());
            setCurrencyId(ecsk.getCurrencyId());
            setCreator(ecsk.getCreator());
            setModifiedBy(ecsk.getModifiedBy());
            setIpAdd(ecsk.getIpAdd());

            return 0;
        } catch (Exception e) {
            Rlog.fatal("eventcost",
                    " error in EventcostBean.updateEventcost : " + e);
            return -2;
        }
    }

    public EventcostBean() {

    }

    public EventcostBean(int eventcostId, String eventcostValue,
            String eventId, String eventcostDescId, String currencyId,
            String creator, String modifiedBy, String ipAdd) {
        super();
        // TODO Auto-generated constructor stub
        setEventcostId(eventcostId);
        setEventcostValue(eventcostValue);
        setEventId(eventId);
        setEventcostDescId(eventcostDescId);
        setCurrencyId(currencyId);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
    }

}// end of class
