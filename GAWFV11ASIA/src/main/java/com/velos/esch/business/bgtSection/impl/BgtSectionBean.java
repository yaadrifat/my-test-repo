/*
 * Classname : BgtSectionBean
 * 
 * Version information: 1.0
 *
 * Date: 3/21/2002
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Sonia Sahni */

package com.velos.esch.business.bgtSection.impl;

/* Import Statements */

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.esch.service.util.Rlog;
import com.velos.esch.service.util.StringUtil;

/* End of Import Statements */
@Entity
@Table(name = "sch_bgtsection")
public class BgtSectionBean implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 3546356219531769905L;

    /**
     * bgtSection id
     */
    public int bgtSectionId;

    /**
     * buget calendar
     */
    public Integer bgtCal;

    /**
     * bgtSection Name
     */
    public String bgtSectionName;

    /**
     * bgtSection Visit
     */

    public String bgtSectionVisit;

    /**
     * bgtSection fkVisit
     */
    public Integer fkVisit;

    /**
     * PbgtSectionSequence
     */
    public Integer bgtSectionSequence;

    /**
     * Delete Flag (Whether the record has been deleted logically or not)
     */
    public String bgtSectionDelFlag;

    /*
     * the record creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * the IP Address
     */
    public String ipAdd;

    /*
     * budget section patient number
     */

    public String bgtSectionPatNo;

    /*
     * budget personnel section flag
     */

    public Integer bgtSectionPersonlFlag;

    /*
     * budget section type - One time fees, per patient fees
     */

    public String bgtSectionType;

    /*
     * budget section notes
     */

    public String bgtSectionNotes;
    
    //Added by IA 9/18/2006
    
    /*  Section Resarch Total ( cost/patient)
	
    */
    public String srtCostTotal;          


    /* Section Research Total ( Total Cost)
    	
    */
    public String srtCostGrandTotal;     


    /* Section SOC Total ( cost/patient)
    	
    */
    public String socCostTotal;          


    /* Section SCO Total ( Total Cost)
    	
    */
    public String socCostGrandTotal ; 
    
    //End added by IA 9/18/2006
    
    //Added by IA 11.03.2006
    
    public String srtCostSponsorAmount;          


    /* Section SCO Total ( Total Cost)
    	
    */

    public String srtCostVariance;          


    /* Section SCO Total ( Total Cost)
    	
    */

    
    public String socCostSponsorAmount;          


    /* Section SCO Total ( Total Cost)
    	
    */

    
    public String socCostVariance;          


    /* Section SCO Total ( Total Cost)
    	
    */

    
    //end added 11.03.2006
    

    // GETTER SETTER METHODS
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_SCH_BGTSECTION", allocationSize=1)
    @Column(name = "PK_BUDGETSEC")
    public int getBgtSectionId() {
        return this.bgtSectionId;
    }

    public void setBgtSectionId(int bgtSectionId) {
        this.bgtSectionId = bgtSectionId;
    }

    /**
     * @return The Id of the creator of the record
     */
    @Column(name = "FK_BGTCAL")
    public String getBgtCal() {
        return StringUtil.integerToString(this.bgtCal);
    }

    /**
     * @param bgtcal
     *            The Id of the protocol cal associated with budget
     */
    public void setBgtCal(String bgtCal) {
        if (bgtCal != null) {
            this.bgtCal = Integer.valueOf(bgtCal);
        }
    }

    /**
     * @return budget section Name
     */
    @Column(name = "BGTSECTION_NAME")
    public String getBgtSectionName() {
        return this.bgtSectionName;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setBgtSectionName(String bgtSectionName) {
        this.bgtSectionName = bgtSectionName;
    }

    /**
     * @return budget section visit
     */
    @Column(name = "BGTSECTION_VISIT")
    public String getBgtSectionVisit() {
        return this.bgtSectionVisit;
    }

    /**
     * @param section
     *            visit
     */
    public void setBgtSectionVisit(String bgtSectionVisit) {
        this.bgtSectionVisit = bgtSectionVisit;
    }

    /**
     * @return budget section fkVisit
     */
    @Column(name = "FK_VISIT")
    public Integer getBgtSectionFkVisit() {
        return this.fkVisit;
    }

    /**
     * @param section
     *            visit
     */
    public void setBgtSectionFkVisit(Integer fkVisit) {
        this.fkVisit = fkVisit;
    }

    @Column(name = "BGTSECTION_DELFLAG")
    public String getBgtSectionDelFlag() {
        return this.bgtSectionDelFlag;
    }

    public void setBgtSectionDelFlag(String bgtSectionDelFlag) {
        this.bgtSectionDelFlag = bgtSectionDelFlag;
    }

    /**
     * @return The Id of the creator of the record
     */
    @Column(name = "BGTSECTION_SEQUENCE")
    public String getBgtSectionSequence() {
        return StringUtil.integerToString(this.bgtSectionSequence);
    }

    /**
     * @param bgtcal
     *            The Id of the protocol cal associated with budget
     */

    public void setBgtSectionSequence(String bgtSectionSequence) {
        if (bgtSectionSequence != null) {
            this.bgtSectionSequence = Integer.valueOf(bgtSectionSequence);
        }
    }

    /**
     * @return The Id of the creator of the record
     */
    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    /**
     * @return Modified By
     */
    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modified
     *            by
     */
    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    /**
     * @return IP Address
     */
    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
     * @return Patient Count
     */
    @Column(name = "BGTSECTION_PATNO")
    public String getBgtSectionPatNo() {
        return this.bgtSectionPatNo;
    }

    /**
     * @param bgtSectionPatNo
     *            budget section patient count
     */
    public void setBgtSectionPatNo(String bgtSectionPatNo) {

        this.bgtSectionPatNo = bgtSectionPatNo;

    }

    /**
     * @return budget section personnel flag
     */
    @Column(name = "BGTSECTION_PERSONLFLAG")
    public String getBgtSectionPersonlFlag() {
        return StringUtil.integerToString(this.bgtSectionPersonlFlag);
    }

    /**
     * @param bgtSectionPersonlFlag
     *            budget section personnel flag
     */
    public void setBgtSectionPersonlFlag(String bgtSectionPersonlFlag) {
        if (bgtSectionPersonlFlag != null) {
            this.bgtSectionPersonlFlag = Integer.valueOf(bgtSectionPersonlFlag);
        }
    }

    /**
     * @return Budget Section Type
     */
    @Column(name = "BGTSECTION_TYPE")
    public String getBgtSectionType() {
        return this.bgtSectionType;
    }

    /**
     * @param bgtSectionType
     *            Budget Section Type
     */
    public void setBgtSectionType(String bgtSectionType) {
        this.bgtSectionType = bgtSectionType;
    }

    /**
     * @return Budget Section Notes
     */
    @Column(name = "bgtsection_notes")
    public String getBgtSectionNotes() {
        return this.bgtSectionNotes;
    }

    /**
     * @param bgtSectionNotes
     *            Budget Section Notes
     */
    public void setBgtSectionNotes(String bgtSectionNotes) {
        this.bgtSectionNotes = bgtSectionNotes;
    }
    
    // Added by IA 9/28/2006
    
  	@Column(name = "SRT_COST_TOTAL")
    public String getSrtCostTotal() {

        return this.srtCostTotal;

    }

    public void setSrtCostTotal(String srtCostTotal) {

        if (srtCostTotal != null) {
            this.srtCostTotal = srtCostTotal;
        }
    }
    
  	@Column(name = "SRT_COST_GRANDTOTAL")
    public String getSrtCostGrandTotal() {

        return this.srtCostGrandTotal;

    }

    public void setSrtCostGrandTotal(String srtCostGrandTotal) {

        if (srtCostGrandTotal != null) {
            this.srtCostGrandTotal = srtCostGrandTotal;
        }
    }
    
    
  	@Column(name = "SOC_COST_TOTAL")
    public String getSocCostTotal() {

        return this.socCostTotal;

    }

    public void setSocCostTotal(String socCostTotal) {

        if (socCostTotal != null) {
            this.socCostTotal = socCostTotal;
        }
    }
    
    
  	@Column(name = "SOC_COST_GRANDTOTAL")
    public String getSocCostGrandTotal() {

        return this.socCostGrandTotal;

    }

    public void setSocCostGrandTotal(String socCostGrandTotal) {

        if (socCostGrandTotal != null) {
            this.socCostGrandTotal = socCostGrandTotal;
        }
    }
    
    
    //End added by IA 9/18/2006
    
    
    //Added by IA 11.03.2006  Comparative Budget
 	@Column(name = "SRT_COST_SPONSOR")
    public String getSrtCostSponsorAmount() {

        return this.srtCostSponsorAmount;

    }

    public void setSrtCostSponsorAmount(String srtCostSponsorAmount) {

        if (srtCostSponsorAmount != null) {
            this.srtCostSponsorAmount= srtCostSponsorAmount;
        }
    }
    
 	@Column(name = "SRT_COST_VARIANCE")
    public String getSrtCostVariance() {

        return this.srtCostVariance;

    }

    public void setSrtCostVariance(String srtCostVariance) {

        if (srtCostVariance != null) {
            this.srtCostVariance = srtCostVariance;
        }
    }
    
 	@Column(name = "SOC_COST_SPONSOR")
    public String getSocCostSponsorAmount() {

        return this.socCostSponsorAmount;

    }

    public void setSocCostSponsorAmount(String socCostSponsorAmount) {

        if (socCostSponsorAmount != null) {
            this.socCostSponsorAmount = socCostSponsorAmount;
        }
    }
    
	@Column(name = "SOC_COST_VARIANCE")
    public String getSocCostVariance() {

        return this.socCostVariance;

    }

    public void setSocCostVariance(String socCostVariance) {

        if (socCostVariance != null) {
            this.socCostVariance = socCostVariance;
        }
    }
    //end added 11.03.2006
    
    
    
    // / END OF GETTER SETTER METHODS

    /**
     * returns the state holder object associated with this bgtSection
     */

    /*
     * public BgtSectionStateKeeper getBgtSectionStateKeeper() {
     * Rlog.debug("bgtSection", "BgtSectionBean.getBgtSectionStateKeeper");
     * return new BgtSectionStateKeeper(getBgtSectionId(), getBgtCal(),
     * getBgtSectionName(), getBgtSectionVisit(), getBgtSectionDelFlag(),
     * getBgtSectionSequence(), getCreator(), getModifiedBy(), getIpAdd(),
     * getBgtSectionPatNo(), getBgtSectionPersonlFlag(), getBgtSectionType(),
     * getBgtSectionNotes()); }
     */

    /**
     * sets up a state keeper containing details of the bgtSection
     * 
     * @param edsk
     */

    /*
     * public void setBgtSectionStateKeeper(BgtSectionStateKeeper edsk) {
     * GenerateId bId = null;
     * 
     * try { Connection conn = null; conn = getConnection();
     * Rlog.debug("bgtSection", "BgtSectionBean.setBgtSectionStateKeeper()
     * Connection :" + conn);
     * 
     * bgtSectionId = bId.getId(, conn); conn.close();
     * 
     * setBgtCal(edsk.getBgtCal()); setBgtSectionName(edsk.getBgtSectionName());
     * setBgtSectionVisit(edsk.getBgtSectionVisit());
     * setBgtSectionDelFlag(edsk.getBgtSectionDelFlag());
     * setBgtSectionSequence(edsk.getBgtSectionSequence());
     * setCreator(edsk.getCreator()); setModifiedBy(edsk.getModifiedBy());
     * setIpAdd(edsk.getIpAdd()); setBgtSectionPatNo(edsk.getBgtSectionPatNo());
     * setBgtSectionPersonlFlag(edsk.getBgtSectionPersonlFlag());
     * setBgtSectionType(edsk.getBgtSectionType());
     * setBgtSectionNotes(edsk.getBgtSectionNotes());
     * 
     * Rlog.debug("bgtSection", "bgtSection.setbgtSectionStateKeeper() :" +
     * bgtSectionId); } catch (Exception e) { Rlog.fatal("bgtSection", "Error" +
     * e); } }
     */

    /**
     * updates the bgtSection details
     * 
     * @param edsk
     * @return 0 if successful; -2 for exception
     */
    public int updateBgtSection(BgtSectionBean edsk) {
        try {

            setBgtSectionId(edsk.getBgtSectionId());
            setBgtCal(edsk.getBgtCal());
            setBgtSectionName(edsk.getBgtSectionName());
            setBgtSectionVisit(edsk.getBgtSectionVisit());
            setBgtSectionFkVisit(edsk.getBgtSectionFkVisit());
            setBgtSectionDelFlag(edsk.getBgtSectionDelFlag());
            setBgtSectionSequence(edsk.getBgtSectionSequence());
            setCreator(edsk.getCreator());
            setModifiedBy(edsk.getModifiedBy());
            setIpAdd(edsk.getIpAdd());
            setBgtSectionPatNo(edsk.getBgtSectionPatNo());
            setBgtSectionPersonlFlag(edsk.getBgtSectionPersonlFlag());
            setBgtSectionType(edsk.getBgtSectionType());
            setBgtSectionNotes(edsk.getBgtSectionNotes());
            setSrtCostTotal(edsk.getSrtCostTotal());
            setSrtCostGrandTotal(edsk.getSrtCostGrandTotal());
            setSocCostTotal(edsk.getSocCostTotal());
            setSocCostGrandTotal(edsk.getSocCostGrandTotal());
            setSocCostSponsorAmount(edsk.getSocCostSponsorAmount());
            setSrtCostSponsorAmount(edsk.getSrtCostSponsorAmount());
            setSrtCostVariance(edsk.getSrtCostVariance());
            setSocCostVariance(edsk.getSocCostVariance());
            
            

            return 0;
        } catch (Exception e) {
            Rlog.fatal("bgtSection",
                    " error in BgtSectionBean.updateBgtSection : " + e);
            return -2;
        }
    }

    public BgtSectionBean() {

    }

    public BgtSectionBean(int bgtSectionId, String bgtCal,
            String bgtSectionName, String bgtSectionVisit, Integer fkVisit,
            String bgtSectionSequence, String bgtSectionDelFlag,
            String creator, String modifiedBy, String ipAdd,
            String bgtSectionPatNo, String bgtSectionPersonlFlag,
            String bgtSectionType, String bgtSectionNotes, String srtCostTotal, 
            String srtCostGrandTotal, String socCostTotal, String socCostGrandTotal, 
            String srtSponsorAmount, String srtVariance, String socSponsorAmount, String socVariance) {
        super();
        // TODO Auto-generated constructor stub
        try {
        setBgtSectionId(bgtSectionId);
        setBgtCal(bgtCal);
        setBgtSectionName(bgtSectionName);
        setBgtSectionVisit(bgtSectionVisit);
        setBgtSectionFkVisit(fkVisit);
        setBgtSectionSequence(bgtSectionSequence);
        setBgtSectionDelFlag(bgtSectionDelFlag);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setBgtSectionPatNo(bgtSectionPatNo);
        setBgtSectionPersonlFlag(bgtSectionPersonlFlag);
        setBgtSectionType(bgtSectionType);
        setBgtSectionNotes(bgtSectionNotes);
        setSrtCostTotal(srtCostTotal);
        setSrtCostGrandTotal(srtCostGrandTotal);
        setSocCostTotal(socCostTotal);
        setSocCostGrandTotal(socCostGrandTotal);
        setSrtCostSponsorAmount(srtSponsorAmount);
        setSrtCostVariance(srtVariance);
        setSocCostSponsorAmount(socSponsorAmount);
        setSocCostVariance(socVariance);
        } catch (Exception e) {
            Rlog.fatal("bgtSection",
                    " error in BgtSectionBean.New : " + e);
        }
        
        
    }

}// end of class
