/*
 * Classname 			:	eventCrfJB
 * 
 * Version information 	: 	1.0
 *
 * Date 				:	06/26/2006
 * 
 * Copyright notice		: 	Velos Inc
 * 
 * Author				:	Gopu
 */

package com.velos.esch.web.eventCrf;

/* IMPORT STATEMENTS */

import java.util.Hashtable;

import com.velos.esch.business.common.CrfDao;
import com.velos.esch.business.eventCrf.impl.EventCrfBean;
import com.velos.esch.service.eventCrfAgent.EventCrfAgentRObj;
import com.velos.esch.service.util.EJBUtil;
import com.velos.esch.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * Client side bean for Eventcrf module. This class is used for any kind of
 * manipulation for the EventCrf.
 * 
 */

public class EventCrfJB {

    /**
     * Event Crf Id
     */
    private int eventCrfId;

    /**
     * FK_Form
     */
    private String fkForm;

    /**
     * FK_Event
     */
    private int fkEvent;

    /**
     * Form Type
     */
    private String formType;

    /**
     * Other Link
     */
    private String otherLink;

    /**
     * Event CRF creator
     */
    private String eventCrfCreator;

    /**
     * Event CRF last modified by
     */
    private String eventCrfLastModBy;

    /**
     * IP address
     */
    private String eventCrfIPAdd;

    // GETTER METHODS

    public int getEventCrfId() {
        return this.eventCrfId;
    }
    
    public String getFkForm() {
        return this.fkForm;
    }
    
    public int getFkEvent() {
        return this.fkEvent;
    }
    
        
    public String getFormType() {
        return this.formType;
    }
    
    public String  getOtherLink() {
        return this.otherLink;
    }
    
    public String getEventCrfCreator() {
        return this.eventCrfCreator;
    }
    
    public String getEventCrfLastModBy() {
        return this.eventCrfLastModBy;
    }
    
    public String getEventCrfIpAdd() {
        return this.eventCrfIPAdd;
    }
    
    
    
    
    // SETTER METHODS

    public void setEventCrfId(int eventCrfId) {
        this.eventCrfId = eventCrfId;
    }

    public void setFkForm(String fkForm) {
        this.fkForm = fkForm;
    }

    public void setFkEvent(int fkEvent) {
        this.fkEvent = fkEvent;
    }

    public void setFormType(String formType) {
        this.formType = formType;
    }

     public void setOtherLink(String otherLink) {
        this.otherLink = otherLink;
    }

    public void setEventCrfCreator(String eventCrfCreator) {
        this.eventCrfCreator = eventCrfCreator;
    }

    public void setEventCrfLastModBy(String eventCrfLastModBy) {
        this.eventCrfLastModBy = eventCrfLastModBy;
    }

    public void setEventCrfIpAdd(String eventCrfIPAdd) {
        this.eventCrfIPAdd = eventCrfIPAdd;
    }
    
    // END OF GETTER SETTER METHODS

    
    
    /**
     * Defines an EventCrfJB object with the specified eventCrf Id
     */
    public EventCrfJB(int eventCrfId) {
        setEventCrfId(eventCrfId);
    }

    /**
     * Defines an EventCrfJB object with default values for fields
     */
    public EventCrfJB() {
        Rlog.debug("eventcrf", "EventCrfJB.EventCrfJB() ");
    };

    /**
     * Defines an CatLibJB object with the specified values for the fields
     * 
     */
    public EventCrfJB(int eventCrfId, String fkForm, int fkEvent,
            String formType, String otherLink, String eventCrfCreator,
            String eventCrfLastModBy, String eventCrfIPAdd) {
    		setEventCrfId(eventCrfId);
    		setFkForm(fkForm);
    		setFkEvent(fkEvent);
    		setFormType(formType);
    		setOtherLink(otherLink);
    		setEventCrfCreator(eventCrfCreator);
    		setEventCrfLastModBy(eventCrfLastModBy);
    		setEventCrfIpAdd(eventCrfIPAdd);
    		Rlog.debug("eventcrf", "EventCrfJB.EventCrfJB(all parameters)");
    }

    /**
     * Retrieves the details of an crf in eventCrfStateKeeper Object
     * 
     * @return eventCrfStateKeeper consisting of the eventcrf details
     * @see eventCrfStateKeeper
     */
    public EventCrfBean getEventCrfDetails() {
        EventCrfBean ecsk = null;
        try {
            EventCrfAgentRObj eventCrfAgentRObj = EJBUtil.getEventCrfAgentHome();
            ecsk = eventCrfAgentRObj.getEventCrfDetails(this.eventCrfId);
            Rlog.debug("eventcrf", "In EventCrfJB.getEventCrfDetails() " + ecsk);
        } catch (Exception e) {
            Rlog
                    .fatal("eventcrf", "Error in getEventCrfDetails() in EventCrfJB "
                            + e);
        }
        if (ecsk != null) {
            this.eventCrfId = ecsk.getEventCrfId();
            this.fkForm = ecsk.getFkForm();
            this.fkEvent = ecsk.getFkEvent();
            this.formType = ecsk.getFormType();
            this.otherLink = ecsk.getOtherLink();
            this.eventCrfCreator = ecsk.getEventCrfCreator();
            this.eventCrfLastModBy = ecsk.getEventCrfLastModBy();
            this.eventCrfIPAdd = ecsk.getEventCrfIpAdd();
           
        }
        return ecsk;
    }

    public int setEventCrfDetails() {
        int output = 0;
        try {
            EventCrfAgentRObj eventCrfAgentRObj = EJBUtil.getEventCrfAgentHome();
            output = eventCrfAgentRObj.setEventCrfDetails(this
                    .createEventCrfStateKeeper());
            this.setEventCrfId(output);
            Rlog.debug("eventcrf", "EventCrfJB.setEventCrfDetails()");

        } catch (Exception e) {
            Rlog
                    .fatal("eventcrf", "Error in setEventCrfDetails() in EventCrfJB "
                            + e);
            return -2;

        }
        return output;
    }

    /**
     * Saves the modified details of the category to the database
     * 
     * @return 0 - If update succeeds <BR>
     *         -2 - If update fails
     */
    
    public int updateEventCrf() {
        int output;
        try {
        	EventCrfAgentRObj eventCrfAgentRObj = EJBUtil.getEventCrfAgentHome();
            output = eventCrfAgentRObj.updateEventCrf(this
                    .createEventCrfStateKeeper());
        } catch (Exception e) {
            Rlog.debug("eventcrf",
                    "EXCEPTION IN SETTING EVENTCRF DETAILS TO  SESSION BEAN"
                            + e.getMessage());

            return -2;
        }
        return output;
    }
    

    /**
     * Uses the values of the current EventCrfJB object to create an
     * EventCrfStateKeeper Object
     * 
     * @return EventCrfStateKeeper object with all the details of the eventcrf
     * @see EventCrfStateKeeper
     */
    public EventCrfBean createEventCrfStateKeeper() {
        Rlog.debug("eventcrf", "EventCrfJB.createEventCrfStateKeeper ");
        return new EventCrfBean(this.eventCrfId, this.fkForm, this.fkEvent,
                this.formType, this.otherLink, this.eventCrfCreator,
                this.eventCrfLastModBy, this.eventCrfIPAdd);
    }
//  Method Added by Gopu for May-June Enhancement #F1 Calendar to fomr linking
    /**
     * Get the values of the Event CRF Forms
     * @param eventId 
     * @param dispType
     * @return cfrDao
     */
    public CrfDao getEventCrfForms(int eventId,String dispType){
    	CrfDao crfDao=null;
         try {
         	EventCrfAgentRObj eventCrfAgentRObj = EJBUtil.getEventCrfAgentHome();
         	crfDao = (CrfDao)eventCrfAgentRObj.getEventCrfForms(eventId,dispType);
         } catch (Exception e) {
             Rlog.debug("eventcrf",
                     "EXCEPTION IN SETTING EVENTCRFFORM DETAILS TO  SESSION BEAN"
                             + e.getMessage());
         }
      	return crfDao;
    }
//  Method Added by Gopu for May-June Enhancement #F1 Calendar to fomr linking
    /**
     * Delete the Event CRF 
     * @param eventId
     * 
     */
    
 // Overloaded for INF-18183 ::: Akshi
    public void deleteEventCrf(int eventId, Hashtable<String, String> args){
         try {
         	EventCrfAgentRObj eventCrfAgentRObj = EJBUtil.getEventCrfAgentHome();
         	eventCrfAgentRObj.deleteEventCrf(eventId,args);
         } catch (Exception e) {
             Rlog.debug("EventCrfJb",
                     "Exception in setting deleteEventCrf Details to Session Bean"
                             + e.getMessage());
         }
    }
    
    public void deleteEventCrf(int eventId){
        try {
        	EventCrfAgentRObj eventCrfAgentRObj = EJBUtil.getEventCrfAgentHome();
        	eventCrfAgentRObj.deleteEventCrf(eventId);
        } catch (Exception e) {
            Rlog.debug("EventCrfJb",
                    "Exception in setting deleteEventCrf Details to Session Bean"
                            + e.getMessage());
        }
   }
    
    /** Returns 'Visit Mark Done' prompt Flag = 1 if all the CRfs are filled for the scheduled event record 
     *  and if the event has no marked
     * status. It will return 0 if the event already has a status. 
     * @param schEventId PK to identify scheduled event (sch_events1 pk)
     * @return 1 if application should prompt for marking 'events done', 0 - for no prompts
     */
    public int getEventMarkDoneFlagWhenAllCRFsFilled(int schEventId)
    {
    	 try {
          	EventCrfAgentRObj eventCrfAgentRObj = EJBUtil.getEventCrfAgentHome();
          	return eventCrfAgentRObj.getEventMarkDoneFlagWhenAllCRFsFilled(schEventId);
          } catch (Exception e) {
              Rlog.debug("EventCrfJb",
                      "Exception in getEventMarkDoneFlagWhenAllCRFsFilled"
                              + e.getMessage());
              return 0;
          }
    	
    }
    
}
