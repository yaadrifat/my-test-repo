/*
 * Classname : EventuserJB
 * 
 * Version information: 1.0 
 *
 * Date: 06/21/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh Khurana 
 */

package com.velos.esch.web.eventuser;

/* IMPORT STATEMENTS */

import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;

import com.velos.esch.business.common.EventdefDao;
import com.velos.esch.business.common.EventuserDao;
import com.velos.esch.business.eventuser.impl.EventuserBean;
import com.velos.esch.service.eventuserAgent.EventuserAgentRObj;
import com.velos.esch.service.util.EJBUtil;
import com.velos.esch.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

public class EventuserJB {

    /**
     * eventuserId
     */
    private int eventuserId;

    /**
     * userId
     */
    private String userId;

    /**
     * eventuserType
     */
    private String userType;

    /**
     * eventId
     */
    private String eventId;

    /**
     * user ids
     */
    private ArrayList userIds;

    /*
     * creator
     */
    private String creator;

    /*
     * last modified by
     */
    private String modifiedBy;

    /*
     * last modified Date
     */
    private Date lastModifiedDate;

    /*
     * IP Address
     */
    private String ipAdd;

    /*
     * event user duration
     */
    private String duration;

    /*
     * event user notes
     */

    private String notes;

    /*
     * event user duration
     */
    private ArrayList evDuration;

    /*
     * event user notes
     */
    private ArrayList evNotes;
    
    /*
     * processed primary keys
     */
    private ArrayList processedPKs;
    
    
    

    // GETTER SETTER METHODS

    // Start of Getter setter methods

    public int getEventuserId() {
        return this.eventuserId;
    }

    public void setEventuserId(int eventuserId) {
        this.eventuserId = eventuserId;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserType() {
        return this.userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getEventId() {
        return this.eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public ArrayList getUserIds() {
        return this.userIds;
    }

    public void setUserIds(ArrayList userIds) {
        this.userIds = userIds;
    }

    /**
     * @return Creator
     */
    public String getCreator() {
        return this.creator;
    }

    /**
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return Last Modified By
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param modifiedBy
     *            The value that is required to be registered as record last
     *            modified by
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
    
    /**
     *  @return Last Modified Date
     */
    public Date getLastModifiedDate() {
        return this.lastModifiedDate;
    }
    /**
     * @param lastModifiedDate
     *            The value that is required to be registered as record last
     *            modified Date
     */
    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }
    
    /**
     * @return IP Address
     */
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    public String getNotes() {
        return this.notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getDuration() {
        return this.duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public ArrayList getEvNotes() {
        return this.evNotes;
    }

    public void setEvNotes(ArrayList evNotes) {
        this.evNotes = evNotes;
    }

    public ArrayList getEvDuration() {
        return this.evDuration;
    }

    public void setEvDuration(ArrayList evDuration) {
        this.evDuration = evDuration;
    }

    // END OF GETTER SETTER METHODS

    /**
     * Constructor; Accepts Eventuser Id
     * 
     * @param eventuserId
     *            Id of the Eventuser
     */

    public EventuserJB(int eventuserId) {
        setEventuserId(eventuserId);
    }

    /**
     * Default Constructor
     * 
     */
    public EventuserJB() {
        Rlog.debug("eventuser", "EventuserJB.EventuserJB() ");
        
        processedPKs = new ArrayList();
    }

    public EventuserJB(int eventuserId, String userId, String userType,
            String eventId, ArrayList userIds, String creator,
            String modifiedBy,Date lastModifiedDate, String ipAdd, ArrayList evDuration,
            ArrayList evNotes, String duration, String notes)

    {

        setEventuserId(eventuserId);
        setUserId(userId);
        setUserType(userType);
        setEventId(eventId);
        setUserIds(userIds);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setLastModifiedDate(lastModifiedDate);
        setIpAdd(ipAdd);
        setEvDuration(evDuration);
        setEvNotes(evNotes);
        setDuration(duration);
        setNotes(notes);

        Rlog.debug("eventuser", "EventuserJB.EventuserJB(all parameters)");
    }

    /**
     * Calls getEventuserDetails() of Eventuser Session Bean: EventuserAgentBean
     * 
     * 
     * @return The Details of the eventuser in the Eventuser StateKeeper Object
     * @See EventuserAgentBean
     */

    public EventuserBean getEventuserDetails() {
        EventuserBean eusk = null;

        try {

            EventuserAgentRObj eventuserAgent = EJBUtil.getEventuserAgentHome();
            eusk = eventuserAgent.getEventuserDetails(this.eventuserId);
            Rlog.debug("eventuser",
                    "EventuserJB.getEventuserDetails() EventuserStateKeeper "
                            + eusk);
        } catch (Exception e) {
            Rlog.debug("eventuser",
                    "Error in getEventuserDetails() in EventuserJB " + e);
        }
        if (eusk != null) {
            this.eventuserId = eusk.getEventuserId();
            this.userId = eusk.getUserId();
            this.userType = eusk.getUserType();
            this.eventId = eusk.getEventId();
            this.creator = eusk.getCreator();
            this.modifiedBy = eusk.getModifiedBy();
            this.ipAdd = eusk.getIpAdd();
            setEvDuration(eusk.getEvDuration());
            setEvNotes(eusk.getEvNotes());
            setDuration(eusk.getDuration());
            setNotes(eusk.getNotes());

        }
        return eusk;
    }

    /**
     * Calls setEventuserDetails() of Eventuser Session Bean: EventuserAgentBean
     * 
     */
    /*Modified by Sonia Abrol, 06/08/06 
     * update users one at a time so that we can use the 'event user primary keys' for propagation
     * 
     * */
    public  void setEventuserDetails() {
    	
        try {
        	
        	String userTyp = "";
        	
            EventuserAgentRObj eventuserAgent = EJBUtil.getEventuserAgentHome();
            
            userTyp = this.getUserType();
            int eventuserPK = 0;
            
            if (userTyp.equals("R"))
            {
            	Rlog.debug("eventuser", "EventuserJB.setEventuserDetails()");	
            	this.eventuserId = eventuserAgent.setEventuserDetails(this
                        .createEventuserStateKeeper());	
            	
            }
            else // for user or message user
            {
            	ArrayList uIds = getUserIds();
            	
            	 for (int i = 0; i < uIds.size(); i++)
            	 {
            		 EventuserBean eEB = new EventuserBean(); 
                 	 eEB = this.createEventuserStateKeeper();
                 	
                     String uId = (String) uIds.get(i);
                     eEB.setUserId(uId);
                     eventuserPK  = eventuserAgent.setEventuserDetailsSingle(eEB);
                     this.eventuserId = eventuserPK;
                     System.out.println("new eventuserPK" + eventuserPK + "*");
                     processedPKs.add(new Integer(eventuserPK));
                     
            	 }
            	
            	
            }
            
            
        } catch (Exception e) {
            Rlog.debug("eventuser",
                    "Error in setEventuserDetails() in EventuserJB " + e);
        }
        
    }
    
    public void setEventuserDetailsSingle() {

        try {

            EventuserAgentRObj eventuserAgent = EJBUtil.getEventuserAgentHome();
            this.eventuserId = eventuserAgent.setEventuserDetailsSingle(this
                    .createEventuserStateKeeper());
            Rlog.debug("eventuser", "EventuserJB.setEventuserDetailsSingle()");
        } catch (Exception e) {
            Rlog.debug("eventuser",
                    "Error in setEventuserDetailsSingle() in EventuserJB " + e);
        }
    }

    /**
     * Calls updateEventuser() of Eventuser Session Bean: EventuserAgentBean
     * 
     * @return
     */
    public int updateEventuser() {
        int output;
        try {

            EventuserAgentRObj eventuserAgentRObj = EJBUtil
                    .getEventuserAgentHome();
            output = eventuserAgentRObj.updateEventuser(this
                    .createEventuserStateKeeper());
        } catch (Exception e) {
            Rlog.debug("eventuser",
                    "EXCEPTION IN SETTING USER DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    /**
     * Calls updateEventuser() of Eventuser Session Bean: EventuserAgentBean
     * 
     * @return
     */
    /* Modified by Sonia Abrol, removed hardcoding of 'event_user'. This way procedure can distinguish  exactly what to propagate - message user, 
     * event user or role */
    
    public int propagateEventuser(int protocol_id, String propagateInVisitFlag,
            String propagateInEventFlag, String mode, String calType, String recordType) {
        int output = 0;
        EventuserBean usk;
        int newId  = 0;
        EventdefDao eventdefdao = new EventdefDao();
        try {

            EventuserAgentRObj EventuserAgentRObj = EJBUtil
                    .getEventuserAgentHome();
            usk = this.createEventuserStateKeeper();
            
            if (! (this.getUserType().equals("R")) )
            {
            	//propagate for each processed key
               if (! mode.equals("D"))
	            	{	
	            	if (this.processedPKs != null)
	            	{
		            	for (int i=0; i<this.processedPKs.size(); i++)
		            	{
		            		newId = ((Integer) this.processedPKs.get(i)).intValue();
		            		
		            		System.out.println("propagating new eventuserPK" + newId + "*");
		            		 
		            		output = eventdefdao.propagateEventUpdates(protocol_id, EJBUtil
		                            .stringToNum(usk.getEventId()), recordType, newId,
		                            propagateInVisitFlag, propagateInEventFlag, mode, calType);
		            	}	
	            	}
	            	
	            }
               else
               {
            	   //delete mode
            	   
            	   newId = this.getEventuserId();
            		System.out.println("propagating DELETE eventuserPK" + newId + "*");
           		 
            		output = eventdefdao.propagateEventUpdates(protocol_id, EJBUtil
                            .stringToNum(this.getEventId()), recordType, newId,
                            propagateInVisitFlag, propagateInEventFlag, mode, calType);
            	   
               }
            }
            else
            {
            	
            	
            	if (mode.equals("D"))
            	{
            		
            		System.out.println("propagating deleted resources" );
            		output = eventdefdao.propagateEventUpdates(protocol_id, EJBUtil
                            .stringToNum(this.getEventId()), recordType, this.getEventuserId() ,
                            propagateInVisitFlag, propagateInEventFlag, mode, calType);
            	}
            	else
            	{
            		
            		newId = 0;
                	System.out.println("propagating new resources" );
                	
            		output = eventdefdao.propagateEventUpdates(protocol_id, EJBUtil
                            .stringToNum(usk.getEventId()), recordType, newId,
                            propagateInVisitFlag, propagateInEventFlag, mode, calType);
            		
            	}
            	
            	
            }
            // 
            
        } catch (Exception e) {
            Rlog.debug("Eventuser",
                    "EXCEPTION IN SETTING USER DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output;
    }


    
    public int removeEventusers(int eventId, String eventType) {
        int output = 0;
        try {

            EventuserAgentRObj eventuserAgentRObj = EJBUtil
                    .getEventuserAgentHome();
            output = eventuserAgentRObj.removeEventusers(eventId,eventType);
        } catch (Exception e) {
            Rlog.debug("eventuser", "EXCEPTION IN DELETING THE CHILD NODES");
            e.printStackTrace();
            return -2;
        }
        return output;
    }
    
    
    // removeEventusers() Overloaded for INF-18183 ::: Raviesh
    public int removeEventusers(int eventId, String eventType,Hashtable<String, String> args) {
        int output = 0;
        try {

            EventuserAgentRObj eventuserAgentRObj = EJBUtil
                    .getEventuserAgentHome();
            output = eventuserAgentRObj.removeEventusers(eventId,eventType,args);
        } catch (Exception e) {
            Rlog.debug("eventuser", "EXCEPTION IN DELETING THE CHILD NODES");
            e.printStackTrace();
            return -2;
        }
        return output;
    }


    
    /*
     * removes a single user
     */


    public int removeEventuser(int eventuserId) {
        int output = 0;
        try {

            EventuserAgentRObj eventuserAgentRObj = EJBUtil
                    .getEventuserAgentHome();
            output = eventuserAgentRObj.removeEventuser(eventuserId);
        } catch (Exception e) {
            Rlog.debug("eventuser", "EXCEPTION IN DELETING THE CHILD NODES");
            e.printStackTrace();
            return -2;
        }
        return output;
    }


    // removeEventuser() Overloaded for INF-18183 ::: Raviesh
    public int removeEventuser(int eventuserId,Hashtable<String, String> args) {
        int output = 0;
        try {

            EventuserAgentRObj eventuserAgentRObj = EJBUtil
                    .getEventuserAgentHome();
            output = eventuserAgentRObj.removeEventuser(eventuserId,args);
        } catch (Exception e) {
            Rlog.debug("eventuser", "EXCEPTION IN DELETING THE CHILD NODES");
            e.printStackTrace();
            return -2;
        }
        return output;
    }
    
    
    // //

    public EventuserDao getEventRoles(int eventId) {
        EventuserDao evd = new EventuserDao();

        try {
            EventuserAgentRObj eventuserAgentRObj = EJBUtil
                    .getEventuserAgentHome();
            evd = eventuserAgentRObj.getEventRoles(eventId);
        } catch (Exception e) {
            Rlog.fatal("eventuser", "EXCEPTION IN getEventRoles");
            e.printStackTrace();
            return null;
        }
        return evd;
    }

    // /

    // //

    public EventuserDao getEventUsers(int eventId) {
        EventuserDao evd = new EventuserDao();

        try {
            EventuserAgentRObj eventuserAgentRObj = EJBUtil
                    .getEventuserAgentHome();
            evd = eventuserAgentRObj.getEventUsers(eventId);
        } catch (Exception e) {
            Rlog.fatal("eventuser", "EXCEPTION IN getEventRoles");
            e.printStackTrace();
            return null;
        }
        return evd;
    }

    // /

    public EventuserDao getAllRolesForEvent(int eventId) {
        EventuserDao evd = new EventuserDao();

        try {

            EventuserAgentRObj eventuserAgentRObj = EJBUtil
                    .getEventuserAgentHome();
            evd = eventuserAgentRObj.getAllRolesForEvent(eventId);
        } catch (Exception e) {
            Rlog.fatal("eventuser", "EXCEPTION IN getEventRoles");
            e.printStackTrace();
            return null;
        }
        return evd;
    }

    /**
     * 
     * 
     * @return the Eventuser StateKeeper Object with the current values of the
     *         Bean
     */

    public EventuserBean createEventuserStateKeeper() {
        Rlog.debug("eventuser", "EventuserJB.createEventuserStateKeeper ");

        return new EventuserBean(eventuserId, userId, userType, eventId,
                creator, modifiedBy, lastModifiedDate, ipAdd, duration, notes, evNotes,
                evDuration, userIds);
    }

    /*
     * generates a String of XML for the eventuser details entry form.<br><br>
     * NOT IN USE
     */

    public String toXML() {
        Rlog.debug("eventuser", "EventuserJB.toXML()");
        return "test";
        // to be implemented later
        /*
         * return new String("<?xml version=\"1.0\"?>" + "<?cocoon-process
         * type=\"xslt\"?>" + "<?xml-stylesheet
         * href=\"/stylesheet/form-html.xsl\" type=\"text/xsl\"?>" + "<?xml-stylesheet
         * href=\"/stylesheet/form-wml.xsl\" type=\"text/xsl\" media=\"wap\"?>" + "<head>" + "</head>" + "<input
         * type=\"hidden\" name=\"eventuserId\" value=\"" +
         * this.getEventuserId()+ "\" size=\"10\"/>" + "<input type=\"text\"
         * name=\"eventuserCodelstJobtype\" value=\"" +
         * this.getEventuserCodelstJobtype() + "\" size=\"10\"/>" + "<input
         * type=\"text\" name=\"eventuserAccountId\" value=\"" +
         * this.getEventuserAccountId() + "\" size=\"10\"/>" + "<input
         * type=\"text\" name=\"eventuserSiteId\" value=\"" +
         * this.getEventuserSiteId() + "\" size=\"10\"/>" + "<input
         * type=\"text\" name=\"eventuserPerAddressId\" value=\"" +
         * this.getEventuserPerAddressId() + "\" size=\"38\"/>" + "<input
         * type=\"text\" name=\"eventuserLastName\" value=\"" +
         * this.getEventuserLastName() + "\" size=\"30\"/>" + "<input
         * type=\"text\" name=\"eventuserFirstName\" value=\"" +
         * this.getEventuserFirstName() + "\" size=\"30\"/>" + "<input
         * type=\"text\" name=\"eventuserMidName \" value=\"" +
         * this.getEventuserMidName() + "\" size=\"30\"/>" + "<input
         * type=\"text\" name=\"eventuserWrkExp\" value=\"" +
         * this.getEventuserWrkExp() + "\" size=\"100\"/>" + "<input
         * type=\"text\" name=\"eventuserPhaseInv\" value=\"" +
         * this.getEventuserPhaseInv() + "\" size=\"100\"/>" + "<input
         * type=\"text\" name=\"eventuserSessionTime\" value=\"" +
         * this.getEventuserSessionTime() + "\" size=\"3\"/>" + "<input
         * type=\"text\" name=\"eventuserLoginName\" value=\"" +
         * this.getEventuserLoginName() + "\" size=\"20\"/>" + "<input
         * type=\"text\" name=\"eventuserPwd\" value=\"" +
         * this.getEventuserPwd() + "\" size=\"20\"/>" + "<input type=\"text\"
         * name=\"eventuserSecQues\" value=\"" + this.getEventuserSecQues() +
         * "\" size=\"150\"/>" + "<input type=\"text\" name=\"eventuserAnswer\"
         * value=\"" + this.getEventuserAnswer() + "\" size=\"150\"/>" + "<input
         * type=\"text\" name=\"eventuserStatus\" value=\"" +
         * this.getEventuserStatus() + "\" size=\"1\"/>" + "<input
         * type=\"text\" name=\"eventuserCodelstSpl\" value=\"" +
         * this.getEventuserCodelstSpl() + "\" size=\"10\"/>" + "<input
         * type=\"text\" name=\"eventuserGrpDefault\" value=\"" +
         * this.getEventuserGrpDefault() + "\" size=\"10\"/>" + "</form>" );
         */

    }// end of method

}// end of class
