/*
 * Classname : AdvInfoJB
 * 
 * Version information: 1.0 
 *
 * Date: 12/10/2002
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Arvind
 */

package com.velos.esch.web.advInfo;

/* IMPORT STATEMENTS */

import com.velos.esch.business.advInfo.impl.AdvInfoBean;
import com.velos.esch.service.advInfoAgent.AdvInfoAgentRObj;
import com.velos.esch.service.util.EJBUtil;
import com.velos.esch.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * AdvInfoJB is a client side Java Bean for Adverse Events Info business
 * components. The class implements the Business Delegate pattern to reduce
 * coupling between presentation-tier clients and business services.
 * 
 * <p>
 * In eResearch Architecture, the class resides at the Application layer.
 * </p>
 * 
 * @author Arvind Kumar
 * @version %I%, %G%
 */

public class AdvInfoJB {

    /**
     * adverse event info id
     */
    private int advInfoId;

    /**
     * adverse event id
     */
    private String advInfoAdverseId;

    /**
     * info code
     */
    private String advInfoCodelstInfoId;

    /**
     * info value
     */
    private String advInfoValue;

    /**
     * info type
     */
    private String advInfoType;

    /** creator */

    private String creator;

    /**
     * modified By
     */
    private String modifiedBy;

    /**
     * IP Address
     */
    private String ipAdd;

    // GETTER SETTER METHODS

    /**
     * Returns an id for the adverse event info
     * 
     * @return the adverse event info id
     */

    public int getAdvInfoId() {
        return this.advInfoId;
    }

    /**
     * Sets the id for the adverse event info
     * 
     * @param advInfoId
     *            the adverse event info id
     */
    public void setAdvInfoId(int advInfoId) {
        this.advInfoId = advInfoId;
    }

    /**
     * Gets id of the parent adverse event
     * 
     * @return id of the parent adverse event
     */

    public String getAdvInfoAdverseId() {
        return this.advInfoAdverseId;
    }

    /**
     * Sets the id for the parent adverse event
     * 
     * @param advInfoAdverseId
     *            the parent adverse event
     */
    public void setAdvInfoAdverseId(String advInfoAdverseId) {
        this.advInfoAdverseId = advInfoAdverseId;
    }

    /**
     * Gets adverse event info code
     * 
     * @return id of the adverse event info code
     */
    public String getAdvInfoCodelstInfoId() {
        return this.advInfoCodelstInfoId;
    }

    /**
     * Sets adverse event info code
     * 
     * @param advInfoCodelstInfoId
     *            the adverse event info code
     */
    public void setAdvInfoCodelstInfoId(String advInfoCodelstInfoId) {
        this.advInfoCodelstInfoId = advInfoCodelstInfoId;
    }

    /**
     * Gets adverse event info value
     * 
     * @return id of the adverse event info value
     */
    public String getAdvInfoValue() {
        return this.advInfoValue;
    }

    /**
     * Sets adverse event info value
     * 
     * @param advInfoValue
     *            the adverse event info value
     */
    public void setAdvInfoValue(String advInfoValue) {
        this.advInfoValue = advInfoValue;
    }

    /**
     * Gets adverse event info type
     * 
     * @return id of the adverse event info type
     */

    public String getAdvInfoType() {
        return this.advInfoType;
    }

    /**
     * Sets adverse event info type
     * 
     * @param advInfoType
     *            the adverse event info type
     */
    public void setAdvInfoType(String advInfoType) {
        this.advInfoType = advInfoType;
    }

    /**
     * Gets the user who created the adverse event info
     * 
     * @return the user who created the adverse event info
     */

    public String getCreator() {
        return this.creator;
    }

    /**
     * Sets the user who created the adverse event info
     * 
     * @param creator
     *            the user who created the adverse event info
     */

    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * Gets the user who modified the adverse event info
     * 
     * @return the user who modified the adverse event info
     */

    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * Sets the user who modified the adverse event info
     * 
     * @param modifiedBy
     *            the user who modified the adverse event info
     */

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * Gets the IP address of the user who created/modified the adverse event
     * info
     * 
     * @return IP address
     */

    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * Sets the IP address of the user who created/modified the adverse event
     * info
     * 
     * @param ipAdd
     *            IP address
     */

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // END OF GETTER SETTER METHODS

    /**
     * Class constructor: creates an adverse event info object for an adverse
     * event info id
     * 
     * @param advInfoId
     *            adverse event info id
     */

    public AdvInfoJB(int advInfoId) {
        setAdvInfoId(advInfoId);
    }

    /**
     * Default Constructor
     */
    public AdvInfoJB() {
        Rlog.debug("advInfo", "AdvInfoJB.AdvInfoJB() ");
    }

    /**
     * Class constructor: full argument constructor
     * 
     * @param advInfoId
     *            adverse event info id
     * @param advInfoAdverseId
     *            adverse event id
     * @param advInfoCodelstInfoId
     *            info code
     * @param advInfoValue
     *            info value
     * @param advInfoType
     *            info type
     * @param creator
     *            creator
     * @param modifiedBy
     *            modified By
     * @param ipAdd
     *            IP Address
     */

    public AdvInfoJB(int advInfoId, String advInfoAdverseId,
            String advInfoCodelstInfoId, String advInfoValue,
            String advInfoType, String creator, String modifiedBy, String ipAdd) {
        setAdvInfoId(advInfoId);
        setAdvInfoAdverseId(advInfoAdverseId);
        setAdvInfoCodelstInfoId(advInfoCodelstInfoId);
        setAdvInfoValue(advInfoValue);
        setAdvInfoType(advInfoType);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        Rlog.debug("advInfo", "AdvInfoJB.AdvInfoJB(all parameters)");
    }

    /**
     * Populates itself with the adverse event info's details using its
     * attribute : advInfoId Calls getAdvInfoDetails() on the Adverse Event Info
     * Session Bean: AdvInfoAgentBean. The session bean method returns an
     * AdvInfoStateKeeper object that is used by this method to populate the
     * object's attributes.
     * 
     * @see AdvInfoStateKeeper
     */

    public AdvInfoBean getAdvInfoDetails() {
        AdvInfoBean edsk = null;

        try {
            AdvInfoAgentRObj advInfoAgent = EJBUtil.getAdvInfoAgentHome();
            edsk = advInfoAgent.getAdvInfoDetails(this.advInfoId);
            Rlog.debug("advEve",
                    "AdvInfoJB.getAdvInfoDetails() AdvInfoStateKeeper " + edsk);
        } catch (Exception e) {
            Rlog.debug("advInfo", "Error in getAdvInfoDetails() in AdvInfoJB "
                    + e);
        }
        if (edsk != null) {
            this.advInfoId = edsk.getAdvInfoId();
            this.advInfoAdverseId = edsk.getAdvInfoAdverseId();
            this.advInfoCodelstInfoId = edsk.getAdvInfoCodelstInfoId();
            this.advInfoValue = edsk.getAdvInfoValue();
            this.advInfoType = edsk.getAdvInfoType();
            this.creator = edsk.getCreator();
            this.modifiedBy = edsk.getModifiedBy();
            this.ipAdd = edsk.getIpAdd();
        }
        return edsk;
    }

    /**
     * Creates new adverse event info using the object's attributes. Calls
     * setAdvInfoDetails() on the Adverse Event Info Session Bean:
     * AdvInfoAgentBean.
     */
    public void setAdvInfoDetails() {

        try {

            AdvInfoAgentRObj advInfoAgent = EJBUtil.getAdvInfoAgentHome();
            Rlog.debug("advInfo", "AdvInfoJB.advInfoAgentHome.create() END");
            this.setAdvInfoId(advInfoAgent.setAdvInfoDetails(this
                    .createAdvInfoStateKeeper()));
            Rlog.debug("advInfo", "AdvInfoJB.setAdvInfoDetails()");
        } catch (Exception e) {
            Rlog.fatal("advInfo", "Error in setAdvInfoDetails() in AdvInfoJB "
                    + e);
        }
    }

    /**
     * Updates adverse event info using the object's attributes. Calls
     * updateAdvInfo() on the Adverse Event Info Session Bean: AdvEveInfoBean.
     * 
     * @return success flag
     *         <ul>
     *         <li> 0 if successful</li>
     *         <li> -2 in case of a failure</li>
     *         </ul>
     */

    public int updateAdvInfo() {
        int output;
        try {
            AdvInfoAgentRObj advInfoAgentRObj = EJBUtil.getAdvInfoAgentHome();
            output = advInfoAgentRObj.updateAdvInfo(this
                    .createAdvInfoStateKeeper());
        } catch (Exception e) {
            Rlog.fatal("advInfo",
                    "EXCEPTION IN SETTING AdvInfo DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    /**
     * Returns an AdvInfoStateKeeper object with the current values of the Bean
     * 
     * @return AdvInfoStateKeeper
     * @see AdvInfoStateKeeper
     */
    public AdvInfoBean createAdvInfoStateKeeper() {

        return new AdvInfoBean(advInfoId, advInfoAdverseId,
                advInfoCodelstInfoId, advInfoValue, advInfoType, creator,
                modifiedBy, ipAdd);
    }

}// end of class
