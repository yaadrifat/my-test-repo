/*
 * Classname : EventResourceJB
 *
 * Version information: 1.0
 *
 * Date: 02/15/2008
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Jnanamay Majumdar
 */

package com.velos.esch.web.eventresource;
import java.util.Hashtable;
import com.velos.esch.business.common.EventInfoDao;
import com.velos.esch.business.common.EventdefDao;
import com.velos.esch.business.eventdef.impl.EventdefBean;
import com.velos.esch.service.eventdefAgent.EventdefAgentRObj;
import com.velos.esch.service.util.EJBUtil;
import com.velos.esch.service.util.Rlog;

/* IMPORT STATEMENTS */

import java.util.ArrayList;

//import com.velos.esch.business.common.EventdefDao;
import com.velos.esch.business.common.EventResourceDao;
import com.velos.esch.business.eventresource.impl.EventResourceBean;
import com.velos.esch.service.eventresourceAgent.EventResourceAgentRObj;
import com.velos.esch.service.util.EJBUtil;
import com.velos.esch.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

public class EventResourceJB {

	/**
     * pk of the table SCH_EVRES_TRACK
     */

    private int evtResTrackId; //pk_evres_track



    /**
     * fk_eventstat
     */
    private String evtStat; //fk_eventstat


    /**
     * fk_codelst_role
     */

    private String evtRole; //fk_codelst_role


    /*
     * event resource duration
     */
    private String evtTrackDuration; //evres_track_duration;


    /*
     * creator
     */
    private String creator;

    /*
     * last modified by
     */
    private String modifiedBy;

    /*
     * IP Address
     */
    private String ipAdd;


    private ArrayList evtRoles;

    private ArrayList evtTrackDurations;


    // GETTER SETTER METHODS

    // Start of Getter setter methods

    public int getEvtResTrackId() {
        return this.evtResTrackId;
    }

    public void setEvtResTrackId(int evtResTrackId) {
        this.evtResTrackId = evtResTrackId;
    }


    public String getEvtStat() {
        return this.evtStat;
    }

    public void setEvtStat(String evtStat) {
        this.evtStat = evtStat;
    }


    public String getEvtRole() {
        return this.evtRole;
    }

    public void setEvtRole(String evtRole) {
        this.evtRole = evtRole;
    }


    public String getEvtTrackDuration() {
        return this.evtTrackDuration;
    }

    public void setEvtTrackDuration(String evtTrackDuration) {
        this.evtTrackDuration = evtTrackDuration;
    }

    /**
     * @return Creator
     */
    public String getCreator() {
        return this.creator;
    }

    /**
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return Last Modified By
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param modifiedBy
     *            The value that is required to be registered as record last
     *            modified by
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return IP Address
     */
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    public ArrayList getEvtRoles() {
        return this.evtRoles;
    }

    public void setEvtRoles(ArrayList evtRoles) {
        this.evtRoles = evtRoles;
    }


    public ArrayList getEvtTrackDurations() {
        return this.evtTrackDurations;
    }

    public void setEvtTrackDurations(ArrayList evtTrackDurations) {
        this.evtTrackDurations = evtTrackDurations;
    }
    // END OF GETTER SETTER METHODS

    /**
     * Constructor; Accepts Event Tracking Id
     *
     * @param evtResTrackId
     *            Id of the SCH_EVRES_TRACK
     */

    public EventResourceJB(int evtResTrackId) {
    	setEvtResTrackId(evtResTrackId);
    }

    /**
     * Default Constructor
     *
     */
    public EventResourceJB() {
        Rlog.debug("eventresource", "EventresourceJB.EventResourceJB() ");
    }

    public EventResourceJB(int evtResTrackId, String evtStat, String evtRole, String evtTrackDuration,
    		String creator, String modifiedBy, String ipAdd, ArrayList evtTrackDurations)

    {
        setEvtResTrackId(evtResTrackId);
        setEvtStat(evtStat);
        setEvtRole(evtRole);
        setEvtTrackDuration(evtTrackDuration);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setEvtTrackDurations(evtTrackDurations);
        Rlog.debug("eventresource", "EventresourceJB.EventResourceJB(full params...)");
    }

    /**
     * Calls getEventresourceDetails() of EventResource Session Bean: EventResourceAgentBean
     *
     *
     * @return The Details of the eventresource in the EventResource StateKeeper Object
     * @See EventResourceAgentBean
     */

    public EventResourceBean getEventResourceDetails() {
    	EventResourceBean ersk = null;

        try {

        	EventResourceAgentRObj eventResAgent = EJBUtil.getEventResourceAgentHome();
        	ersk = eventResAgent.getEventResourceDetails(this.evtResTrackId);
            Rlog.debug("eventresource",
                    "EventResourceJB.getEventresourceDetails() EventresourceStateKeeper "
                            + ersk);
        } catch (Exception e) {
            Rlog.debug("eventresource",
                    "Error in getEventresourceDetails() in EventResourceJB " + e);
        }
        if (ersk != null) {
            this.evtResTrackId = ersk.getEvtResTrackId();
            this.evtStat = ersk.getEvtStat();
            this.evtRole = ersk.getEvtRole();
            this.evtTrackDuration = ersk.getEvtTrackDuration();
            this.creator = ersk.getCreator();
            this.modifiedBy = ersk.getModifiedBy();
            this.ipAdd = ersk.getIpAdd();
            setEvtTrackDurations(ersk.getEvtTrackDurations());

        }
        return ersk;
    }

    /**
     * Calls setEventResourceDetails() of EventResource Session Bean: EventResourceAgentBean
     *
  	 */
    public  void setEventResourceDetails() {

            int output = 0;
            try {

            	EventResourceAgentRObj eventResourceAgent = EJBUtil.getEventResourceAgentHome();
                output = eventResourceAgent.setEventResourceDetails(this.createEventResourceStateKeeper());
                this.setEvtResTrackId(output);
                Rlog.debug("eventresource", "EventResourceJB.setEventResourceDetails()");

            } catch (Exception e) {
            	Rlog.debug("eventresource",
                    "Error in setEventResourceDetails() in EventResourceJB " + e);
            }

        }


    /**
     * Calls updateEventResource() of EventResource Session Bean: EventResourceAgentBean
     *
     * @return
     */
    /*public int updateEventResource() {
        int output;
        try {

            EventResourceAgentRObj eventResourceAgent = EJBUtil.getEventResourceAgentHome();
            output = eventResourceAgent.updateEventResource(this.createEventResourceStateKeeper());
        } catch (Exception e) {
            Rlog.debug("eventresource",
                    "EXCEPTION IN SETTING USER DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output;
    }*/




    /************ VVI
    public int removeEventResources(int eventId, String roleId) {
    	****************

    	For the time being delete the existings later do sth
    	*/

    	
       // Overloaded for INF-18183 ::: Akshi
       public int removeEventResources(int eventId, Hashtable<String, String> args) {
        int output = 0;
        try {

        	EventResourceAgentRObj eventResourceAgent = EJBUtil.getEventResourceAgentHome();
            output = eventResourceAgent.removeEventResources(eventId,args);

        } catch (Exception e) {
            Rlog.debug("eventresource", "EXCEPTION IN DELETING THE CHILD NODES");
            e.printStackTrace();
            return -2;
        }
        return output;
    }
       
      public int removeEventResources(int eventId) {
           int output = 0;
           try {

        	   EventResourceAgentRObj eventResourceAgent = EJBUtil.getEventResourceAgentHome();
               output = eventResourceAgent.removeEventResources(eventId);

           } catch (Exception e) {
        	   Rlog.debug("eventresource", "EXCEPTION IN DELETING THE CHILD NODES");
               e.printStackTrace();
               return -2;
           }
           return output;

       }


     /**
     *
     *
     * @return the EventResource StateKeeper Object with the current values of the Entity Bean
     *
     */

    public EventResourceBean createEventResourceStateKeeper() {
        Rlog.debug("eventresource", "EventResourceJB.createEventResourceStateKeeper ");

        return new EventResourceBean(evtResTrackId, evtStat, evtRole, evtTrackDuration,
        		creator, modifiedBy, ipAdd, evtRoles, evtTrackDurations);
    }

    //to find the pk of the event_status
    public int getStatusIdOfTheEvent(int eventId) {
        int output = 0;
        try {

        	EventResourceAgentRObj eventResourceAgent = EJBUtil.getEventResourceAgentHome();
            output = eventResourceAgent.getStatusIdOfTheEvent(eventId);

        } catch (Exception e) {
            Rlog.debug("eventresource", "EXCEPTION IN getStatusIdOfTheEvent");
            e.printStackTrace();
            return -2;
        }
        return output;
    }


    public EventResourceDao getAllRolesForEvent(int eventId){
    EventResourceDao evResDao = new EventResourceDao();
        try {

        	EventResourceAgentRObj eventResourceAgent = EJBUtil.getEventResourceAgentHome();
        	evResDao = eventResourceAgent.getAllRolesForEvent(eventId);
        	Rlog.debug("eventresource", "In EventResourceJB.getAllRolesForEvent() getting exception...");

        } catch (Exception e) {
            Rlog.debug("eventresource", "EXCEPTION IN getStatusIdOfTheEvent");
            e.printStackTrace();

        }
        return evResDao;
    }




}// end of class
