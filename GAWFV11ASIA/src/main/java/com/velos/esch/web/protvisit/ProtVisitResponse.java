package com.velos.esch.web.protvisit;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap; 

public class ProtVisitResponse implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * visit id 
	 */
	int visitId;
	
	/**
	 * visitName. 
	 */
	String visitName;
	
	int rowNumber;
	
	HashMap<Integer, Integer> copyVisitMap= new HashMap<Integer, Integer>(); //Ak:Added for enhancement PCAL-20801
	
	ArrayList<Integer> updatedVisitList;
	
	/**
	 * Parent visit id 
	 */
	int parentVisit;
	
	/**
	 * Fake visit count 
	 */
	int fakeCount;
	
	/**
	 * Column Key
	 */
	String columnKey;
	
	public HashMap<Integer, Integer> getCopyVisitMap() {
		return copyVisitMap;
	}
	public void setCopyVisitMap(HashMap<Integer, Integer> copyVisitMap) {
		this.copyVisitMap = copyVisitMap;
	}
	public int getVisitId() {
		return visitId;
	}
	public void setVisitId(int visitId) {
		this.visitId = visitId;
	}
	public String getVisitName() {
		return visitName;
	}
	public void setVisitName(String visitName) {
		this.visitName = visitName;
	}
	public int getRowNumber() {
		return rowNumber;
	}
	public void setRowNumber(int rowNumber) {
		this.rowNumber = rowNumber;
	}
	public ArrayList<Integer> getUpdatedVisitList() {
		return updatedVisitList;
	}
	public void setUpdatedVisitList(ArrayList<Integer> updatedVisitList) {
		this.updatedVisitList = updatedVisitList;
	}
	
	public int getParentVisit() {
		return parentVisit;
	}
	public void setParentVisit(int parentVisit) {
		this.parentVisit = parentVisit;
	}
	
	public int getFakeCount() {
		return fakeCount;
	}
	public void setFakeCount(int fakeCount) {
		this.fakeCount = fakeCount;
	}
	
	public String getColumnKey() {
		return columnKey;
	}
	public void setColumnKey(String columnKey) {
		this.columnKey = columnKey;
	}
}
