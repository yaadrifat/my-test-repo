/*
 * Classname : PortalFormsBean
 * 
 * Version information: 1.0
 *
 * Date: 03DEC2010
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Jnanamay Majumdar
 */

package com.velos.esch.web.portalForms;

/* IMPORT STATEMENTS */




import java.util.ArrayList;
import java.util.Hashtable;
import com.velos.esch.business.common.PortalFormsDao;
import com.velos.esch.business.portalForms.impl.PortalFormsBean;
import com.velos.esch.service.portalFormsAgent.PortalFormsAgentRObj;
import com.velos.esch.service.util.EJBUtil;
import com.velos.esch.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * Client side Java Bean for PortalForms
 * 
 * @author Jnanamay Majumdar
 * @version 1.0 12/03/2010
 * 
 */

public class PortalFormsJB {

    /**
     * portalForm id
     */
    private int portalFormId;

    /**
     * portal id
     */
    private String portalId;

    /**
     * calendar id  number
     */
    private String caledarId;

    /**
     * event id
     */

    private String eventId;

	 /**
     * form id
     */
	private String formId;

    /*
     * creator
     */
    private String creator;

    /*
     * creator
     */
    private String modifiedBy;

    /*
     * IP Address
     */
    private String ipAdd;

    
    // GETTER SETTER METHODS

    public int getPortalFormId() {
        return this.portalFormId;
    }

    public void setPortalFormId(int portalFormId) {
        this.portalFormId = portalFormId;
    }

    public String getPortalId() {
        return this.portalId;
    }

    public void setPortalId(String portalId) {
        this.portalId = portalId;
    }

    public String getCaledarId() {
        return this.caledarId;
    }

    public void setCaledarId(String caledarId) {
        this.caledarId = caledarId;
    }

    public String getEventId() {
        return this.eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getFormId() {
        return this.formId;
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }

    /**
     * @return Creator
     */
    public String getCreator() {
        return this.creator;
    }

    /**
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return IP Address
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return IP Address
     */
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // END OF GETTER SETTER METHODS

    /**
     * Constructor; Accepts port forms Id
     * 
     * @param crflibId
     *            Id of the Crflib
     */
    public PortalFormsJB(int portalFormId) {
        setPortalFormId(portalFormId);
    }

    /**
     * Default Constructor
     * 
     */
    public PortalFormsJB() {
        Rlog.debug("portalforms", "PortalFormsJB.PortalFormsJB() ");
    }

    public PortalFormsJB(int portalFormId, String portalId, String caledarId, String eventId, 
		String formId, String creator, String modifiedBy, String ipAdd) {

        setPortalFormId(portalFormId);
		setPortalId(portalId);
		setCaledarId(caledarId);
		setEventId(eventId);
		setFormId(formId);		
		setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        Rlog.debug("portalforms", "PortalFormsJB.PortalFormsJB(all parameters)");
    }

    
    /**
     * 
     * 
     * @return PortalFormsBean, an object which holds the Portal Forms details
     */
    public PortalFormsBean getPortalFormDetails(){
    	
    	PortalFormsBean pfsk = null;

        try {            
        	PortalFormsAgentRObj portalFormsAgentRObj = EJBUtil.getPortalFormsAgentHome();
            pfsk = portalFormsAgentRObj.getPortalFormDetails(this.portalFormId);

            Rlog.debug("portalforms","PortalFormsJB.getPortalFormDetails() PortalFormsStateKeeper " + pfsk);
        } catch (Exception e) {
            Rlog.fatal("portalforms", "Error in getPortalFormDetails() in PortalFormsJB " + e);
        }
        if (pfsk != null) {
            
            this.portalFormId = pfsk.getPortalFormId();
            this.portalId = pfsk.getPortalId();
            this.caledarId = pfsk.getCaledarId();
            this.eventId = pfsk.getEventId();
            this.formId = pfsk.getFormId();    
            this.creator = pfsk.getCreator();
            this.modifiedBy = pfsk.getModifiedBy();
            this.ipAdd = pfsk.getIpAdd();
        }

        return pfsk;

    }
    
    
    /**
     * Calls setPortalForms() of Portal Forms Session Bean: PortalFormsAgentBean
     * 
     * @return
     */
    public void setPortalForms() {       
        try {

            PortalFormsAgentRObj portalFormsAgentRObj = EJBUtil.getPortalFormsAgentHome();
            this.setPortalFormId(portalFormsAgentRObj.setPortalForms(this
                    .createPortalFormsStateKeeper()));
        } catch (Exception e) {
            Rlog.fatal("portalforms",
                    "EXCEPTION IN SETTING setPortalForms DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            
        }
       
    }
    
    
    
    /**
     * Calls updatePortalForms() of Portal Forms Session Bean: PortalFormsAgentBean
     * 
     * @return
     */
    public int updatePortalForms() {
        int output;
        try {

            PortalFormsAgentRObj portalFormsAgentRObj = EJBUtil.getPortalFormsAgentHome();
            output = portalFormsAgentRObj.updatePortalForms(this
                    .createPortalFormsStateKeeper());
        } catch (Exception e) {
            Rlog.fatal("portalforms",
                    "EXCEPTION IN SETTING updatePortalForms DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output;
    }

	/**
     * 
     * 
     * @return the Portal Forms StateKeeper Object with the current values of the Bean
     */

    public PortalFormsBean createPortalFormsStateKeeper() {

        return new PortalFormsBean(portalFormId, portalId, caledarId, eventId, 
		formId, creator, modifiedBy, ipAdd);
    }
    
    public PortalFormsDao getCheckedFormsForPortalCalendar(int calId, int portalId) {
    	
    	
    	PortalFormsDao pfDao = new PortalFormsDao();
        try {
            Rlog.debug("portalforms", "PortalFormsJB.getCheckedFormsForPortalCalendar starting");
            
            PortalFormsAgentRObj portalFormsAgentRObj = EJBUtil.getPortalFormsAgentHome();
            pfDao = portalFormsAgentRObj.getCheckedFormsForPortalCalendar(calId, portalId);

            return pfDao;
        } catch (Exception e) {
            Rlog.fatal("portalforms",
                    "getCheckedFormsForPortalCalendar() in PortalFormsJB "
                            + e);
        }
        return pfDao;
    	
    }
    
    public int removePortalFormId(int pfId){
    	int output;
        try {

            PortalFormsAgentRObj portalFormsAgentRObj = EJBUtil.getPortalFormsAgentHome();
            output = portalFormsAgentRObj.removePortalFormId(pfId);
        } catch (Exception e) {
            Rlog.fatal("portalforms",
                    "EXCEPTION IN SETTING removePortalFormId DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output; 	
    	
    }
       
    // removePortalFormId() Overloaded for INF-18183 ::: Raviesh
    public int removePortalFormId(int pfId,Hashtable<String, String> args){
    	int output;
        try {

            PortalFormsAgentRObj portalFormsAgentRObj = EJBUtil.getPortalFormsAgentHome();
            output = portalFormsAgentRObj.removePortalFormId(pfId,args);
        } catch (Exception e) {
            Rlog.fatal("portalforms",
                    "EXCEPTION IN SETTING removePortalFormId DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output; 	
    	
    }       
    

    
    public int removeCalFromPortal(int potalId, int calId){
    	int output;
        try {

            PortalFormsAgentRObj portalFormsAgentRObj = EJBUtil.getPortalFormsAgentHome();
            output = portalFormsAgentRObj.removeCalFromPortal(potalId, calId);
        } catch (Exception e) {
            Rlog.fatal("portalforms",
                    "EXCEPTION IN SETTING removeCalFromPortal DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output; 	
    	
    }
    // Overloaded for INF-18183 ::: AGodara
    public int removeCalFromPortal(int potalId, int calId,Hashtable<String, String> auditInfo){
    	int output;
        try {

            PortalFormsAgentRObj portalFormsAgentRObj = EJBUtil.getPortalFormsAgentHome();
            output = portalFormsAgentRObj.removeCalFromPortal(potalId, calId,auditInfo);
        } catch (Exception e) {
            Rlog.fatal("portalforms",
                    "EXCEPTION IN SETTING removeCalFromPortal DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output; 	
    	
    }
    
    // PP-18306  AGodara
    public int removeMulCalForms(int potalId, String calIds,Hashtable<String, String> auditInfo){
    	int output;
        try {

            PortalFormsAgentRObj portalFormsAgentRObj = EJBUtil.getPortalFormsAgentHome();
            output = portalFormsAgentRObj.removeMulCalForms(potalId, calIds,auditInfo);
        } catch (Exception e) {
            Rlog.fatal("portalforms",
                    "EXCEPTION IN SETTING removeCalFromPortal DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output; 	
    	
    }
    
    
    public PortalFormsDao getPortalFormsDAO(int eventId1,ArrayList<String> selId){
    	
    	PortalFormsDao pfDao ;
        try {
    	    PortalFormsAgentRObj portalFormsAgentRObj = EJBUtil.getPortalFormsAgentHome();
    	    pfDao = portalFormsAgentRObj.getPortalFormsDAO(eventId1,selId);
        } catch (Exception e) {
            Rlog.fatal("PortalFormsJB",
                    "EXCEPTION IN getPortalFormsList");
            e.printStackTrace();
            return null;
        }
        return pfDao; 	
    	
    }
    
    public int setPortalFormsDAO(PortalFormsDao pfDao){
    	int output;
        try {

            PortalFormsAgentRObj portalFormsAgentRObj = EJBUtil.getPortalFormsAgentHome();
            output = portalFormsAgentRObj.setPortalFormsDAO(pfDao);
        } catch (Exception e) {
            Rlog.fatal("PortalFormsJB",
                    "EXCEPTION IN  savePortalFormsList");
            e.printStackTrace();
            return -2;
        }
        return output; 	
    	
    }

    
}// end of class
