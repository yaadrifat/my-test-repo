/*
 * Classname : CrfJB
 * 
 * Version information: 1.0 
 *
 * Date: 11/23/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Arvind
 */

package com.velos.esch.web.crfNotify;

/* IMPORT STATEMENTS */

import com.velos.esch.business.common.CrfNotifyDao;
import com.velos.esch.business.crfNotify.impl.CrfNotifyBean;
import com.velos.esch.service.crfNotifyAgent.CrfNotifyAgentRObj;
import com.velos.esch.service.util.EJBUtil;
import com.velos.esch.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * Client side Java Bean for CRF NOTIFY
 * 
 * @author Arvind
 * @version 1.0 11/27/2001
 * 
 */

public class CrfNotifyJB {

    /**
     * crf Notify
     */
    private int crfNotifyId;

    /**
     * Crf id
     */
    private String crfNotifyCrfId;

    /**
     * crf number
     */
    private String crfNotifyCodelstNotStatId;

    /**
     * crf name
     */

    private String crfNotifyUsersTo;

    /**
     * crf name
     */

    private String crfNotifyNotes;

    private String crfNotStudyId;

    private String crfNotProtocolId;

    private String crfNotPatProtId;

    private String crfNotGlobalFlag;

    /*
     * creator
     */
    private String creator;

    /*
     * modified by
     */
    private String modifiedBy;

    /*
     * IP Address
     */
    private String ipAdd;

    // GETTER SETTER METHODS

    public int getCrfNotifyId() {
        return this.crfNotifyId;
    }

    public void setCrfNotifyId(int crfNotifyId) {
        this.crfNotifyId = crfNotifyId;
    }

    public String getCrfNotifyCrfId() {
        return this.crfNotifyCrfId;
    }

    public void setCrfNotifyCrfId(String crfNotifyCrfId) {
        this.crfNotifyCrfId = crfNotifyCrfId;
    }

    public String getCrfNotifyCodelstNotStatId() {
        return this.crfNotifyCodelstNotStatId;
    }

    public void setCrfNotifyCodelstNotStatId(String crfNotifyCodelstNotStatId) {
        this.crfNotifyCodelstNotStatId = crfNotifyCodelstNotStatId;
    }

    public String getCrfNotifyUsersTo() {
        return this.crfNotifyUsersTo;
    }

    public void setCrfNotifyUsersTo(String crfNotifyUsersTo) {
        this.crfNotifyUsersTo = crfNotifyUsersTo;
    }

    public String getCrfNotifyNotes() {
        return this.crfNotifyNotes;
    }

    public void setCrfNotifyNotes(String crfNotifyNotes) {
        this.crfNotifyNotes = crfNotifyNotes;
    }

    public String getCrfNotStudyId() {
        return this.crfNotStudyId;
    }

    public void setCrfNotStudyId(String crfNotStudyId) {
        this.crfNotStudyId = crfNotStudyId;
    }

    public String getCrfNotProtocolId() {
        return this.crfNotProtocolId;
    }

    public void setCrfNotProtocolId(String crfNotProtocolId) {
        this.crfNotProtocolId = crfNotProtocolId;
    }

    public String getCrfNotPatProtId() {
        return this.crfNotPatProtId;
    }

    public void setCrfNotPatProtId(String crfNotPatProtId) {
        this.crfNotPatProtId = crfNotPatProtId;
    }

    public String getCrfNotGlobalFlag() {
        return this.crfNotGlobalFlag;
    }

    public void setCrfNotGlobalFlag(String crfNotGlobalFlag) {
        this.crfNotGlobalFlag = crfNotGlobalFlag;
    }

    /**
     * @return Creator
     */
    public String getCreator() {
        return this.creator;
    }

    /**
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return Modified By
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return IP Address
     */
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // END OF GETTER SETTER METHODS

    /**
     * Constructor; Accepts Crf Id
     * 
     * @param crfId
     *            Id of the Crf
     */
    public CrfNotifyJB(int crfNotifyId) {
        setCrfNotifyId(crfNotifyId);
    }

    /**
     * Default Constructor
     * 
     */
    public CrfNotifyJB() {
        Rlog.debug("crfNotify", "CrfNotifyJB.CrfNotifyJB() ");
    }

    public CrfNotifyJB(int crfNotifyId, String crfNotifyCrfId,
            String crfNotifyCodelstNotStatId, String crfNotifyUsersTo,
            String crfNotifyNotes, String crfNotStudyId,
            String crfNotProtocolId, String crfNotPatProtId,
            String crfNotGlobalFlag, String creator, String modifiedBy,
            String ipAdd) {

        setCrfNotifyId(crfNotifyId);
        setCrfNotifyCrfId(crfNotifyCrfId);
        setCrfNotifyCodelstNotStatId(crfNotifyCodelstNotStatId);
        setCrfNotifyUsersTo(crfNotifyUsersTo);
        setCrfNotifyNotes(crfNotifyNotes);
        setCrfNotStudyId(crfNotStudyId);
        setCrfNotProtocolId(crfNotProtocolId);
        setCrfNotPatProtId(crfNotPatProtId);
        setCrfNotGlobalFlag(crfNotGlobalFlag);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        Rlog.debug("crfNotify", "CrfNotifyJB.CrfNotifyJB(all parameters)");
    }

    /**
     * Calls getCrfDetails() of CrfNotify Session Bean: CrfNotifyAgentBean
     * 
     * 
     * @return The Details of the crf in the CrfNotify StateKeeper Object
     * @See CrfNotifyAgentBean
     */

    public CrfNotifyBean getCrfNotifyDetails() {
        CrfNotifyBean edsk = null;

        try {

            CrfNotifyAgentRObj crfNotifyAgent = EJBUtil.getCrfNotifyAgentHome();
            edsk = crfNotifyAgent.getCrfNotifyDetails(this.crfNotifyId);
            Rlog.debug("crfNotify",
                    "CrfNotifyJB.getCrfNotifyDetails() CrfNotifyStateKeeper "
                            + edsk);
        } catch (Exception e) {
            Rlog.debug("crfNotify",
                    "Error in getCrfNotifyDetails() in CrfNotifyJB " + e);
        }
        if (edsk != null) {
            this.crfNotifyId = edsk.getCrfNotifyId();
            this.crfNotifyCrfId = edsk.getCrfNotifyCrfId();
            this.crfNotifyCodelstNotStatId = edsk
                    .getCrfNotifyCodelstNotStatId();
            this.crfNotifyUsersTo = edsk.getCrfNotifyUsersTo();
            this.crfNotifyNotes = edsk.getCrfNotifyNotes();
            this.crfNotStudyId = edsk.getCrfNotStudyId();
            this.crfNotProtocolId = edsk.getCrfNotProtocolId();
            this.crfNotPatProtId = edsk.getCrfNotPatProtId();
            this.crfNotGlobalFlag = edsk.getCrfNotGlobalFlag();
            this.creator = edsk.getCreator();
            this.modifiedBy = edsk.getModifiedBy();
            this.ipAdd = edsk.getIpAdd();
        }
        return edsk;
    }

    /**
     * Calls setCrfNotifyDetails() of CrfNotify Session Bean: CrfNotifyAgentBean
     * 
     */

    public void setCrfNotifyDetails() {

        try {

            CrfNotifyAgentRObj crfNotifyAgent = EJBUtil.getCrfNotifyAgentHome();
            this.setCrfNotifyId(crfNotifyAgent.setCrfNotifyDetails(this
                    .createCrfNotifyStateKeeper()));
            Rlog.debug("crfNotify", "CrfNotifyJB.setCrfNotifyDetails()");
        } catch (Exception e) {
            Rlog.fatal("crfNotify",
                    "Error in setCrfNotifyDetails() in CrfNotifyJB " + e);
        }
    }

    /**
     * Calls updateCrfNotify() of CrfNotify Session Bean: CrfNotifyAgentBean
     * 
     * @return
     */
    public int updateCrfNotify() {
        int output;
        try {

            CrfNotifyAgentRObj crfNotifyAgentRObj = EJBUtil
                    .getCrfNotifyAgentHome();
            output = crfNotifyAgentRObj.updateCrfNotify(this
                    .createCrfNotifyStateKeeper());
        } catch (Exception e) {
            Rlog.fatal("crfNotify",
                    "EXCEPTION IN SETTING CRF NOTIFY DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    /**
     * 
     * 
     * @return the Crf Notify StateKeeper Object with the current values of the
     *         Bean
     */

    public CrfNotifyBean createCrfNotifyStateKeeper() {
        Rlog.debug("crfNotify", "CrfNotifyJB.createCrfNotifyStateKeeper ");

        return new CrfNotifyBean(crfNotifyId, crfNotifyCrfId,
                crfNotifyCodelstNotStatId, crfNotifyUsersTo, crfNotifyNotes,
                crfNotStudyId, crfNotProtocolId, crfNotPatProtId,
                crfNotGlobalFlag, creator, modifiedBy, ipAdd);
    }

    public CrfNotifyDao getCrfNotifyValues(int patProtId) {
        CrfNotifyDao crfNotifyDao = new CrfNotifyDao();
        try {

            CrfNotifyAgentRObj crfNotifyAgent = EJBUtil.getCrfNotifyAgentHome();
            Rlog.debug("crfNotify",
                    "CrfNotifyJB.getCrfNotifyValues after remote");
            crfNotifyDao = crfNotifyAgent.getCrfNotifyValues(patProtId);
            Rlog.debug("crfNotify", "CrfNotifyJB.getCrfNotifyValues after Dao");
        } catch (Exception e) {
            Rlog.fatal("crfNotify",
                    "Error in getCrfNotifyValues in CrfNotifyJB " + e);
        }
        return crfNotifyDao;
    }

    public CrfNotifyDao getCrfNotifyValues(int FK_STUDY, int FK_PROTOCOL) {
        CrfNotifyDao crfNotifyDao = new CrfNotifyDao();
        try {

            CrfNotifyAgentRObj crfNotifyAgent = EJBUtil.getCrfNotifyAgentHome();
            Rlog.debug("crfNotify",
                    "CrfNotifyJB.getCrfNotifyValues after remote");
            crfNotifyDao = crfNotifyAgent.getCrfNotifyValues(FK_STUDY,
                    FK_PROTOCOL);
            Rlog.debug("crfNotify", "CrfNotifyJB.getCrfNotifyValues after Dao");
        } catch (Exception e) {
            Rlog.fatal("crfNotify",
                    "Error in getCrfNotifyValues in CrfNotifyJB " + e);
        }
        return crfNotifyDao;
    }

}// end of class
