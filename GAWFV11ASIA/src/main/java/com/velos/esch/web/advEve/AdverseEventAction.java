package com.velos.esch.web.advEve;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.velos.eres.business.common.TeamDao;
import com.velos.eres.business.moreDetails.impl.MoreDetailsBean;
import com.velos.eres.service.util.Configuration;
import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.MC;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.moreDetails.MoreDetailsJB;
import com.velos.eres.web.person.PersonJB;
import com.velos.eres.web.studyRights.StudyRightsJB;
import com.velos.eres.web.userSite.UserSiteJB;
import com.velos.esch.audit.web.AuditRowEschJB;
import com.velos.esch.web.advInfo.AdvInfoJB;
import com.velos.esch.web.advNotify.AdvNotifyJB;

public class AdverseEventAction extends ActionSupport  {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2409203273383175589L;
	private HttpServletRequest request = null;
	private HttpServletResponse response = null;

	public static final String DATA_NOT_SAVED = "dataNotSaved";
	public static final String DATA_NOT_DELETED = "dataNotDeleted";
	public static final String ESIGN_DOES_NOT_MATCH = "esignDoesNotMatch";
	public static final String USERNAME_DOES_NOT_MATCH = "userNameDoesNotMatch";
	private Map errorMap = null;
	private Collection errorAction = null;
	
	public AdverseEventAction() {
		ActionContext ac = ActionContext.getContext();
		request = (HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
		response = (HttpServletResponse)ac.get(ServletActionContext.HTTP_RESPONSE);
	}

	public void setErrorMap(Map errorMap) {
		this.errorMap = errorMap;
	}

	public Map getErrorMap() {
		return errorMap;
	}

	HashMap<String, Object> args = new HashMap<String, Object>();
	
	private void loadAEParameterMap(){
		
		HttpSession tSession = request.getSession(true);

		
		String eSign = request.getParameter("eSign");
		args.put("eSign", eSign);
		
		String oldESign = (String) tSession.getAttribute("eSign");
		args.put("oldESign", oldESign);
		
		if(!oldESign.equals(eSign)) {
			throw new IllegalArgumentException(ESIGN_DOES_NOT_MATCH);
		}

		String userLogName = request.getParameter("userLogName");
		userLogName = StringUtil.isEmpty(userLogName)? "" : userLogName;
		args.put("userLogName", userLogName);
		
		String oldUserId = (String) tSession.getAttribute("loginname");
		if("L2_ON".equals(LC.L_Auth2_Switch) && !oldUserId.equals(userLogName)){
			throw new IllegalArgumentException(USERNAME_DOES_NOT_MATCH);
		}
		
		String userId = (String) tSession.getAttribute("userId");
		args.put("userId", userId);

		String ipAdd = (String) tSession.getAttribute("ipAdd");
		args.put("ipAdd", ipAdd);
		
		
		String mode= request.getParameter("mode");
		args.put("mode", mode);
		
		String eventId= request.getParameter("eventId")==null?"":request.getParameter("eventId");
		args.put("eventId", eventId);
		
		
		
		
		String adve_type = request.getParameter("adve_type");
		args.put("adve_type", adve_type);
		
		String adve_relation = request.getParameter("adve_relation");
		if (adve_relation == null || adve_relation.equals("null") ) adve_relation ="";
		args.put("adve_relation", adve_relation);
		
		String adve_recovery = request.getParameter("adve_recovery");
		if (adve_recovery == null || adve_recovery.equals("null") ) adve_recovery ="";
		args.put("adve_recovery", adve_recovery);

		String meddracode=request.getParameter("MedDRAcode");
		if(meddracode==null) meddracode="";
		args.put("MedDRAcode", meddracode);

		String advdictionary=request.getParameter("advDictionary");
		if(advdictionary==null) advdictionary="";
		args.put("advDictionary", advdictionary);

		String descripton = request.getParameter("descripton")==null?"":request.getParameter("descripton");
		//JM: 26Oct2009: 4144
		if (descripton.length()>2000){
			descripton=descripton.substring(0,2000);
		}
		args.put("descripton", descripton);
		
		String treatment = request.getParameter("treatment")==null?"":request.getParameter("treatment");
		args.put("treatment", treatment);

		String startDt = request.getParameter("startDt");
		args.put("startDt", startDt);

		String stopDt = request.getParameter("stopDt")==null?"":request.getParameter("stopDt");
		args.put("stopDt", stopDt);

		String enteredBy = request.getParameter("enteredBy");
		args.put("enteredBy", enteredBy);

		String reportedBy = request.getParameter("reportedBy");
		reportedBy = (reportedBy==null)?"":reportedBy;
		args.put("reportedBy", reportedBy);

		String aediscoveryDt = request.getParameter("aediscoveryDt")==null?"":request.getParameter("aediscoveryDt");
		args.put("aediscoveryDt", aediscoveryDt);

		String aeloggedDt = request.getParameter("aeloggedDt")==null?"":request.getParameter("aeloggedDt");
		args.put("aeloggedDt", aeloggedDt);

		String linkedToName = request.getParameter("linkedToName")==null?"":request.getParameter("linkedToName");
		args.put("linkedToName", linkedToName);

		String outcomeString = request.getParameter("outcomeString")==null?"":request.getParameter("outcomeString");
		args.put("outcomeString", outcomeString);

		String outcomeDt = request.getParameter("outcomeDt")==null?"":request.getParameter("outcomeDt");
		args.put("outcomeDt", outcomeDt);

		String addInfoString = request.getParameter("addInfoString")==null?"":request.getParameter("addInfoString");
		args.put("addInfoString", addInfoString);

		String advNotifyString = request.getParameter("advNotifyString")==null?"":request.getParameter("advNotifyString");
		args.put("advNotifyString", advNotifyString);

		String notes = request.getParameter("notes")==null?"":request.getParameter("notes");
		//JM: 26Oct2009: 4144
		if (notes.length()>4000){
			notes=notes.substring(0,4000);
		}
		args.put("notes", notes);

		String adveventId=request.getParameter("adveventId")==null?"":request.getParameter("adveventId");
		args.put("adveventId", adveventId);

		String patProtId= request.getParameter("patProtId")==null?"":request.getParameter("patProtId");
		args.put("patProtId", patProtId);

		String studyNum = request.getParameter("studyNum")==null?"":request.getParameter("studyNum");
		args.put("studyNum", studyNum);
		
		String outcomeLen = request.getParameter("outcomeLen")==null?"":request.getParameter("outcomeLen");
		args.put("outcomeLen", outcomeLen);

		String addInfoLen = request.getParameter("addInfoLen")==null?"":request.getParameter("addInfoLen");
		args.put("addInfoLen", addInfoLen);

		String advNotifyLen = request.getParameter("advNotifyLen")==null?"":request.getParameter("advNotifyLen");
		args.put("advNotifyLen", advNotifyLen);

		String outaction= request.getParameter("outaction");
		if (outaction == null || outaction.equals("null")) outaction ="";
		args.put("outaction", outaction);

		String outcomeIds[] = request.getParameterValues("outcomeId");
		args.put("outcomeIds", outcomeIds);

		String advNotifyCodeIds[] = request.getParameterValues("advNotifyCodeId");
		args.put("advNotifyCodeIds", advNotifyCodeIds);

		String advNotifyIds[] = request.getParameterValues("advNotifyId");
		args.put("advNotifyIds", advNotifyIds);

		String advNotifyDates[] = request.getParameterValues("advNotifyDates");
		args.put("advNotifyDates", advNotifyDates);
		
		String addInfoIds[] = request.getParameterValues("addInfoId");
		args.put("addInfoIds", addInfoIds);
		
		String advInfoOutIds[] = request.getParameterValues("advInfoOutId");
		args.put("advInfoOutIds", advInfoOutIds);
		
		String advInfoAddIds[] = request.getParameterValues("advInfoAddId");
		args.put("advInfoAddIds", advInfoAddIds);
				
		String death = request.getParameter("death")==null?"":request.getParameter("death");
		args.put("death", death);

		String studyId =request.getParameter("studyId")==null?"":request.getParameter("studyId");
		args.put("studyId", studyId);

		String statDesc=request.getParameter("statDesc")==null?"":request.getParameter("statDesc");
		args.put("statDesc", statDesc);

		String statid = request.getParameter("statid")==null?"":request.getParameter("statid");
		args.put("statid", statid);

		String studyVer = request.getParameter("studyVer")==null?"":request.getParameter("studyVer");
		args.put("studyVer", studyVer);

		String patientCode = request.getParameter("patientCode")==null?"":request.getParameter("patientCode");
		args.put("patientCode", patientCode);

		String advName = request.getParameter("advName")==null?"":request.getParameter("advName");
		args.put("advName", advName);

		String grade = request.getParameter("grade")==null?"":request.getParameter("grade");
		args.put("grade", grade);

		/* Splitting the fields Name and grade
		 * String gradeText = request.getParameter("advGradeText");
		args.put("advGradeText", gradeText);
*/
		String formStatus = request.getParameter("formStatus");
		if( formStatus == null || formStatus.equals("null")) formStatus = "";
		args.put("formStatus", formStatus);

		String outcomeNotes=request.getParameter("outcomeNotes")==null?"":request.getParameter("outcomeNotes");
		args.put("outcomeNotes", outcomeNotes);

		String pkey = request.getParameter("pkey")==null?"":request.getParameter("pkey");
		args.put("pkey", pkey);

		String visit=request.getParameter("visit")==null?"":request.getParameter("visit");
		args.put("visit", visit);

		String remarks = request.getParameter("remarks")==null?"":request.getParameter("remarks");
		args.put("remarks", remarks);
		
		String aeCategory = request.getParameter("aeCategory")==null?"":request.getParameter("aeCategory");
		args.put("aeCategory", aeCategory);

		String aeToxicity = request.getParameter("aeToxicity")==null?"":request.getParameter("aeToxicity");
		args.put("aeToxicity", aeToxicity);

		String aeToxicityDesc = request.getParameter("aeToxicityDesc")==null?"":request.getParameter("aeToxicityDesc");
		args.put("aeToxicityDesc", aeToxicityDesc);

		String aeGradeDesc = request.getParameter("aeGradeDesc")==null?"":request.getParameter("aeGradeDesc");
		args.put("aeGradeDesc", aeGradeDesc);

		

		
	}

	public int updateAdverseEvent(){
		try{
			this.loadAEParameterMap();
		} catch (IllegalArgumentException e){
		
		e.printStackTrace();
			if (ESIGN_DOES_NOT_MATCH.equals(e.getMessage())){
				return -1;
			} else if (USERNAME_DOES_NOT_MATCH.equals(e.getMessage())){
				return -2;
			}
		}

		int success = updateAEData(args);
		return success;
	}

	private int updateAEData(HashMap<String, Object> args){
		int success = 0;
		String mode =(String) args.get("mode");

		String advNotifyString = (String) args.get("advNotifyString");

		String adveventId=(String) args.get("adveventId");
		
		int advId=0;
		String advNotifyId = "";
		
		char strIndex='0';
		
		String outcomeLen = (String) args.get("outcomeLen");
		String addInfoLen = (String) args.get("addInfoLen");
		String advNotifyLen = (String) args.get("advNotifyLen");
		String outcomeString = (String) args.get("outcomeString");
		String addInfoString =  (String) args.get("addInfoString");
		
		String outcomeIds[] = (String []) args.get("outcomeIds");
		String advNotifyCodeIds[] = (String []) args.get("advNotifyCodeIds");
		String advNotifyIds[] = (String []) args.get("advNotifyIds");
		String advNotifyDates[] = (String []) args.get("advNotifyDates");
		String addInfoIds[] = (String []) args.get("addInfoIds");
		String advInfoOutIds[] = (String []) args.get("advInfoOutIds");
		String advInfoAddIds[] = (String []) args.get("advInfoAddIds");;
		
		String advInfoOutcomeType = "O";
		String advInfoAddInfo = "A";
		
		String advName = (String) args.get("advName");
		String grade = (String) args.get("grade");

		if (mode.equals("M")){
			advId=StringUtil.stringToNum(adveventId);
		}
		
		String msg="";
		
		String ipAdd = (String) args.get("ipAdd");
		String userId = (String) args.get("userId");

		AdvEveJB adveventB = new AdvEveJB();
		if (mode.equals("M")){
			adveventB.setAdvEveId(advId);
		}

		adveventB.setAdvEveEvents1Id((String) args.get("eventId"));
		adveventB.setAdvEveCodelstAeTypeId((String) args.get("adve_type"));
		
		adveventB.setAdvEveRelationship((String) args.get("adve_relation"));
		adveventB.setAdvEveRecoveryDesc((String) args.get("adve_recovery"));

		adveventB.setAdvEveMedDRA((String) args.get("MedDRAcode"));
		adveventB.setAdvEveDictionary((String) args.get("advDictionary"));
		
		adveventB.setAdvEveDesc((String) args.get("descripton"));
		adveventB.setAdvEveTreatment((String) args.get("treatment"));
		adveventB.setAdvEveStDate((String) args.get("startDt"));
		adveventB.setAdvEveEndDate((String) args.get("stopDt"));
		
		adveventB.setFkStudy((String) args.get("studyId"));
		adveventB.setFkPer((String) args.get("pkey"));
		adveventB.setAdvEveEnterBy((String) args.get("enteredBy"));
		
		adveventB.setAdvEveReportedBy((String) args.get("reportedBy"));
		adveventB.setAdvEveLinkedTo((String) args.get("linkedToName"));
		adveventB.setAdvEveDiscvryDate((String) args.get("aediscoveryDt"));
		adveventB.setAdvEveLoggedDate((String) args.get("aeloggedDt"));

		adveventB.setAdvEveOutType(outcomeString);
		adveventB.setAdvEveOutDate((String) args.get("outcomeDt"));
		adveventB.setAdvEveAddInfo(addInfoString);
		adveventB.setAdvEveNotes((String) args.get("notes"));
		adveventB.setAdvEveOutNotes((String) args.get("outcomeNotes"));
		adveventB.setFormStatus((String) args.get("formStatus"));
		adveventB.setFkOutcomeAction((String) args.get("outaction"));
		
		adveventB.setAeCategory((String) args.get("aeCategory"));
		adveventB.setAeToxicity((String) args.get("aeToxicity"));
		adveventB.setAeToxicityDesc((String) args.get("aeToxicityDesc"));
		adveventB.setAeGradeDesc((String) args.get("aeGradeDesc"));

		
		/*Splitting the fields Name and grade
		 * if (StringUtil.isEmpty((String) args.get("advGradeText"))){
			grade = null ;
			advName = "";
		}*/

		adveventB.setAdvEveGrade(grade);
		adveventB.setAdvEveName(advName);

		int ret = 2;
		int cnt = 0;
		String outcomeId = "";
		String addInfoId = "";
		String advInfoOutId = "";
		String advInfoAddId = "";
		String advNotifyDate = "";
		String advNotifyCodeId = "";
		   
		if (mode.equals("M")) {
			adveventB.setModifiedBy(userId);
			adveventB.setIpAdd(ipAdd);
			ret = adveventB.updateAdvEve();

			String remarks = (String) args.get("remarks");
			if (!StringUtil.isEmpty(remarks)){
				AuditRowEschJB auditJB = new AuditRowEschJB();
				auditJB.setReasonForChangeOfAdverseEvent(advId,StringUtil.stringToNum(userId),remarks);
			}
			AdvInfoJB advinfoB = null;

			for(cnt =0;cnt< StringUtil.stringToNum(outcomeLen); cnt++ ){
				outcomeId = outcomeIds[cnt];
				advInfoOutId = advInfoOutIds[cnt];
				strIndex=(StringUtil.isEmpty(outcomeString))?'0':outcomeString.charAt(cnt);

				advinfoB = new AdvInfoJB();
				if(advInfoOutId != null){
					advinfoB.setAdvInfoId(StringUtil.stringToNum(advInfoOutId));
				}
				advinfoB.setAdvInfoAdverseId(adveventId);
				advinfoB.setAdvInfoCodelstInfoId(outcomeId);
				advinfoB.setAdvInfoType(advInfoOutcomeType);
				advinfoB.setAdvInfoValue(""+strIndex);
				advinfoB.setCreator(userId);
				advinfoB.setModifiedBy(userId);
				advinfoB.setIpAdd(ipAdd);
				advinfoB.setAdvInfoDetails();
				advinfoB.updateAdvInfo();
				
				if (!StringUtil.isEmpty(remarks)){
					AuditRowEschJB auditJB = new AuditRowEschJB();
					auditJB.setReasonForChangeOfAdverseEventInfo(StringUtil.stringToNum(advInfoOutId),StringUtil.stringToNum(userId),remarks);
				}
			}
			
			for(cnt =0;cnt< StringUtil.stringToNum(addInfoLen); cnt++ ){
				addInfoId = addInfoIds[cnt];
				advInfoAddId = advInfoAddIds[cnt];
				strIndex=(StringUtil.isEmpty(addInfoString))?'0':addInfoString.charAt(cnt);
				
				advinfoB = new AdvInfoJB();
				if(advInfoAddId != null){
					advinfoB.setAdvInfoId(StringUtil.stringToNum(advInfoAddId));
				}
				
				advinfoB.setAdvInfoAdverseId(adveventId);
				advinfoB.setAdvInfoCodelstInfoId(addInfoId);
				advinfoB.setAdvInfoType(advInfoAddInfo);
				advinfoB.setAdvInfoValue(""+strIndex);
				advinfoB.setCreator(userId);
				advinfoB.setModifiedBy(userId);
				advinfoB.setIpAdd(ipAdd);

				advinfoB.updateAdvInfo();

				if (!StringUtil.isEmpty(remarks)){
					AuditRowEschJB auditJB = new AuditRowEschJB();
					auditJB.setReasonForChangeOfAdverseEventInfo(StringUtil.stringToNum(advInfoAddId),StringUtil.stringToNum(userId),remarks);
				}
			}

			AdvNotifyJB advnotifyB = null;
			
			for(cnt =0;cnt< StringUtil.stringToNum(advNotifyLen); cnt++ ){
				
				advNotifyCodeId = advNotifyCodeIds[cnt];
				advNotifyId = advNotifyIds[cnt];
				strIndex=(StringUtil.isEmpty(advNotifyString))?'0':advNotifyString.charAt(cnt);
				
				advNotifyDate = "notifyDate"+cnt;
				advNotifyDate = request.getParameter(advNotifyDate);
				
				
				advnotifyB = new AdvNotifyJB();
				advnotifyB.setAdvNotifyId(StringUtil.stringToNum(advNotifyId));
				advnotifyB.setAdvNotifyAdverseId(request.getParameter("adveventId"));
				advnotifyB.setAdvNotifyCodelstNotTypeId(advNotifyCodeId);
				advnotifyB.setAdvNotifyDate(advNotifyDate);
				//advnotifyB.setAdvNotifyNotes(notes);
				advnotifyB.setAdvNotifyValue(""+strIndex);
				advnotifyB.setCreator(userId);
				advnotifyB.setModifiedBy(userId);
				advnotifyB.setIpAdd(ipAdd);

				advnotifyB.updateAdvNotify();

				if (!StringUtil.isEmpty(remarks)){
					AuditRowEschJB auditJB = new AuditRowEschJB();
					auditJB.setReasonForChangeOfAdverseEventNotify(StringUtil.stringToNum(advNotifyId),StringUtil.stringToNum(userId),remarks);
				}
			}
		}  else {
			adveventB.setCreator(userId);
			adveventB.setIpAdd(ipAdd);
			adveventId = StringUtil.integerToString(adveventB.setAdvEveDetails());

			AdvInfoJB advinfoB = null;

			for(cnt =0;cnt< StringUtil.stringToNum(outcomeLen); cnt++ ){
				outcomeId = outcomeIds[cnt];
				strIndex=(StringUtil.isEmpty(outcomeString))?'0':outcomeString.charAt(cnt);

				advinfoB = new AdvInfoJB();
				advinfoB.setAdvInfoAdverseId(adveventId);
				advinfoB.setAdvInfoCodelstInfoId(outcomeId);
				advinfoB.setAdvInfoType(advInfoOutcomeType);
				advinfoB.setAdvInfoValue(""+strIndex);
				advinfoB.setCreator(userId);
				advinfoB.setIpAdd(ipAdd);

				advinfoB.setAdvInfoDetails();
			}

			for(cnt =0;cnt< StringUtil.stringToNum(addInfoLen); cnt++ ){
				addInfoId = addInfoIds[cnt];
				strIndex=(StringUtil.isEmpty(addInfoString))?'0':addInfoString.charAt(cnt);
				
				advinfoB = new AdvInfoJB();
				
				advinfoB.setAdvInfoAdverseId(adveventId);
				advinfoB.setAdvInfoCodelstInfoId(addInfoId);
				advinfoB.setAdvInfoType(advInfoAddInfo);
				advinfoB.setAdvInfoValue(""+strIndex);
				advinfoB.setCreator(userId);
				advinfoB.setIpAdd(ipAdd);

				advinfoB.setAdvInfoDetails();
			}

			AdvNotifyJB advnotifyB = null;

			for(cnt =0;cnt< StringUtil.stringToNum(advNotifyLen); cnt++ ){
				advNotifyCodeId = advNotifyCodeIds[cnt];
				strIndex=(StringUtil.isEmpty(advNotifyString))?'0':advNotifyString.charAt(cnt);
				
				advNotifyDate = "notifyDate"+cnt;
				
				advNotifyDate = request.getParameter(advNotifyDate);
				
				advnotifyB = new AdvNotifyJB();
				
				advnotifyB.setAdvNotifyAdverseId(adveventId);
				advnotifyB.setAdvNotifyCodelstNotTypeId(advNotifyCodeId);
				advnotifyB.setAdvNotifyDate(advNotifyDate);
				//advnotifyB.setAdvNotifyNotes(notes);
				advnotifyB.setAdvNotifyValue(""+strIndex);
				advnotifyB.setCreator(userId);
				advnotifyB.setIpAdd(ipAdd);

				advnotifyB.setAdvNotifyDetails();

			}
		}

		createUpdateMoreAEDetails(adveventId);
		
		if (ret == 0) {
			msg = MC.M_AdvEvtDet_SvdSucc;/*msg = "Adverse Event details saved successfully";*****/
		} else {
			msg = MC.M_AdvEvtDet_NotSvd;/*msg = "Adverse Event details not saved";*****/
		}
		return StringUtil.stringToInteger(adveventId);
	}
	
	private int createUpdateMoreAEDetails(String adveventId){

		int success = 0;
		
		try{
			
			HttpSession tSession = request.getSession(true);
			String userId = (String) tSession.getAttribute("userId");
			String ipAdd = (String)tSession.getAttribute("ipAdd");
			String defUserGroup = (String) tSession.getAttribute("defUserGroup");
			
			ArrayList arRecordType = new ArrayList();
			ArrayList arAlternateId = new ArrayList();
			ArrayList arAlternateValues = new ArrayList();
			ArrayList arId = new ArrayList();

			Enumeration params = request.getParameterNames();
			
			while (params.hasMoreElements()) {
			    
				String param = (String)params.nextElement();
			    
				if (param.startsWith("alternateId") && (!param.endsWith("Checks"))) {
			    	
					String paramVal = request.getParameter(param);
			    	
					if (null != paramVal){
			    		String mdId = param.substring(param.indexOf("alternateId")+("alternateId".length()));
			    		arAlternateId.add(mdId);
			    		arAlternateValues.add(paramVal);
			    		paramVal = request.getParameter("recordType"+mdId);
				    	if (null != paramVal){
				    		arRecordType.add(paramVal);
				    	}
				    	paramVal = request.getParameter("id"+mdId);
				    	if (null != paramVal){
				    		arId.add(paramVal);
				    	}
			      	}
			    }
			}

			MoreDetailsBean mdbMain = new MoreDetailsBean();

			ArrayList alElementData = arAlternateValues;

				int len = alElementData.size();
				String modName = request.getParameter("modName");
				
				for ( int j = 0 ; j< len ; j++)
				{
					MoreDetailsBean mdbTemp = new MoreDetailsBean();
					
					if   ( ( !(StringUtil.isEmpty ((String)arAlternateValues.get(j))) &&  "N".equals((String)arRecordType.get(j))) || "M".equals((String)arRecordType.get(j)))
					{
						mdbTemp.setId(StringUtil.stringToNum((String)arId.get(j)));
						mdbTemp.setModId(adveventId) ;
						mdbTemp.setModName(modName) ;
						mdbTemp.setModElementId((String)arAlternateId.get(j));
						mdbTemp.setModElementData((String)arAlternateValues.get(j));
						mdbTemp.setRecordType((String)arRecordType.get(j));
						if ("N".equals((String)arRecordType.get(j)))
						{
							mdbTemp.setCreator(userId);
						}else
						{
							mdbTemp.setModifiedBy(userId);
						}
						mdbTemp.setIpAdd(ipAdd);
						mdbMain.setMoreDetailBeans(mdbTemp);
					}
				}
				MoreDetailsJB moreDetails = new MoreDetailsJB();
				
				success = moreDetails.createMultipleMoreDetails(mdbMain,defUserGroup);
				
		} catch (Exception e){
			e.printStackTrace();
			success = 0;
		}
		return success;
		}
	
	public String updateAdverseEventScreen(){
		int adevntId = -1;
		try{
			adevntId = updateAdverseEvent();
			
			if (adevntId < 0){
				Map<String, String> errorMap = new HashMap<String, String>();
				switch (adevntId){
				case -1:
					if ("userpxd".equals(Configuration.eSignConf)) {
						errorMap.put(ESIGN_DOES_NOT_MATCH, MC.M_EtrWrgPassword_Svg);
						break;
					}
					else
					{
						errorMap.put(ESIGN_DOES_NOT_MATCH,MC.M_EtrWrgEsign_Svg);
					}
					break;
				case -2:
					errorMap.put(USERNAME_DOES_NOT_MATCH, MC.M_WrongUserName_EnterAgain);
					break;
				default:
					errorMap.put(DATA_NOT_SAVED, MC.M_AdvEvtDet_NotSvd);
					break;
				}
				this.errorMap=errorMap;
				return INPUT;
			}
			
			//success = createUpdateMoreAEDetails("UPDATE");
			
			/*if (success < 0){
				Map<String, String> errorMap = new HashMap<String, String>();
				errorMap.put(DATA_NOT_SAVED, "More Adverse Event Details not saved!");
				this.errorMap=errorMap;
				return INPUT;
			}*/
		} catch (Exception e){
			e.printStackTrace();
			Map<String, String> errorMap = new HashMap<String, String>();
			errorMap.put(DATA_NOT_SAVED, MC.M_DataNotSvd_Succ);
			this.errorMap=errorMap;
			return INPUT;
		}
		
		this.errorMap = new HashMap<String, Object>();
		HttpSession tSession = request.getSession(true);
		
		tSession.setAttribute("pkey", args.get("pkey"));
		tSession.setAttribute("adveventId",adevntId);
		
		return SUCCESS;
	}
		
}