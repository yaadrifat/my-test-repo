package com.velos.services.study;


import java.util.List;
import java.util.Map;

import javax.ejb.SessionContext;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.siteAgent.SiteAgentRObj;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.group.GroupJB;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.map.MultipleObjectsFoundException;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.StudySearch;
import com.velos.services.util.CodeCache;
import com.velos.services.util.ObjectLocator;

public class StudyServiceHelper extends CommonDAO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8892283816684237784L;
	private static Logger logger = Logger.getLogger(StudyServiceHelper.class);
	
public List<StudySearch> searchStudy(StudySearch studySearch,Map<String, Object> parameters) throws MultipleObjectsFoundException, OperationException {
		
		SessionContext sessionContext= (SessionContext) parameters.get("sessionContext");
		UserAgentRObj userAgent=(UserAgentRObj) parameters.get("userAgent");
		SiteAgentRObj siteAgent=(SiteAgentRObj) parameters.get("siteAgent");
		ObjectMapService objectMapService= (ObjectMapService) parameters.get("objectMapService");
		UserBean callingUser=(UserBean) parameters.get("callingUser");
		

		CodeDao codeDao=new CodeDao();   	
		int spacePos = -1;
		    String searchFilter="",searchFname="",searchLname="";
		    String stdnumb= "";
		    String searchCriteria = "";
		 	String tarea=  "";
		 	String dmg= "";
		 	String currentStatus="";
		 	String stdstatus= "";
		 	String stdphase= "";
		 	String org="";
		 	String division = "";
		 	String morestddetails = "";
		 	String msdCodeSubType="";//Added by Rajasekhar Reddy
		    if(studySearch!=null)
		    {
		    	System.out.println("Study Search object is "+studySearch);
			     stdnumb= StringUtil.trueValue(studySearch.getStudyNumber()==null?"":studySearch.getStudyNumber());
			     searchCriteria = StringUtil.trueValue(studySearch.getStudyTitle());//Study Title only
			 	 tarea=  StringUtil.trueValue(studySearch.getTherapeuticArea()!=null&&studySearch.getTherapeuticArea().getCode()!=null?codeDao.getCodeDesc(codeDao.getCodeId("", studySearch.getTherapeuticArea().getCode())):"" );
			 	 if(studySearch.getPrincipalInvestigator()!=null)
			 	 {
			 		 UserBean userBean=ObjectLocator.userBeanFromIdentifier(callingUser, studySearch.getPrincipalInvestigator(), userAgent, sessionContext, objectMapService);
			 		 dmg= StringUtil.trueValue(userBean.getUserId().toString()).trim();
			 	 }
			 	 if(studySearch.getCurrentStudyStatus()!=null&&studySearch.getCurrentStudyStatus().getCode()!=null)
			 	 {
			 		 int curStatPK=codeDao.getCodeId(CodeCache.CODE_TYPE_STATUS, studySearch.getCurrentStudyStatus().getCode());
			 		 currentStatus=StringUtil.trueValue(""+curStatPK);
			 	 }
			 	if(studySearch.getDisplayedStudyStatus()!=null&&studySearch.getDisplayedStudyStatus().getCode()!=null)
			 	 {
			 		stdstatus= StringUtil.trueValue(studySearch.getDisplayedStudyStatus().getCode().toString());
			 	 }
			 	if(studySearch.getPhase()!=null&&studySearch.getPhase().getCode()!=null)
			 	 {
			 		stdphase= StringUtil.trueValue(studySearch.getPhase().getCode().toString());
			 	 }
			 	 if(studySearch.getStudyOrganization()!=null)
			 	 {
			 		 int orgPk=ObjectLocator.sitePKFromIdentifier(callingUser, studySearch.getStudyOrganization(), sessionContext, objectMapService);
			 		 org=StringUtil.trueValue(""+orgPk);
			 	 }
			 	if(studySearch.getDivision()!=null&&studySearch.getDivision().getCode()!=null)
			 	 {
			 		division= StringUtil.trueValue(studySearch.getDivision().getCode().toString());
			 	 }
			 	 if(studySearch.getMoreStudyDetails()!=null){
			 		 morestddetails = StringUtil.trueValue(studySearch.getMoreStudyDetails());
			 	 }
			 	 //Added by Rajasekhar Reddy
			 	 if(studySearch.getMsdCodeSubType()!=null){
			 		msdCodeSubType = StringUtil.trueValue(studySearch.getMsdCodeSubType());
			 		System.out.println("MSD Codelst Sub Type = "+msdCodeSubType);
			 		System.out.println("MSD Value = "+morestddetails);
			 	 }
			 	//Added by Rajasekhar Reddy
		    }
		 	String userId= StringUtil.trueValue(callingUser.getUserId().toString());
		 	String accountId= StringUtil.trueValue(callingUser.getUserAccountId());
		 	
		 	
		 	
		 	//String team=  StringUtil.trueValue(request.getParameter("team")).trim();
		 	//String sec=StringUtil.trueValue(request.getParameter("sec"));
		 	//String kywrd= StringUtil.trueValue(request.getParameter("kywrd"));
		 	
		 	//String appndx= StringUtil.trueValue(request.getParameter("appndx"));
		 	//String stdtype= StringUtil.trueValue(request.getParameter("stdtype"));
		 	
		 	
		 	//String sponsor= StringUtil.trueValue(request.getParameter("sponsor"));
		 	//String psites= StringUtil.trueValue(request.getParameter("psites"));
		 	
		 	//String excldprmtclsr =  StringUtil.trueValue(request.getParameter("excldprmtclsr"));
		 	//String pmtclsId=StringUtil.trueValue(request.getParameter("pmtclsId"));
		 	// Added by Ganapathy on 04-20-05 for Organization Search
		 	
		 	//FIX #5685 -Introduced for a PI filter
		 	String onlyPIsearchFilter = "";
		 	
			GroupJB groupJB = new GroupJB();
			String superuserRights =  StringUtil.trueValue(groupJB.getDefaultStudySuperUserRights(userId));
		 	
		 	//String statusType=StringUtil.trueValue(request.getParameter("statusType"));
		 	//String diseaseSite=StringUtil.trueValue(request.getParameter("diseaseSite"));
		 	//String agent_device=StringUtil.trueValue(request.getParameter("agent_device"));
		 	//String researchType = StringUtil.trueValue(request.getParameter("researchType"));
		 	
			//String studysites = StringUtil.trueValue(request.getParameter("studysites"));
			//String excldinactive = StringUtil.trueValue(request.getParameter("excldinactive"));
		 	
		     String studySql="";
		     String countSql;
		     	
	 
		   
		   
		   CodeDao  cd = new CodeDao();
		   String codeLst = cd.getValForSpecificSubType();

		   //System.out.println("codeLst" + codeLst);



		   		//------------Added on Ganapathy  on 04-22-05 for Organization Search-------------------
		   		if (org.length()>0) {            
		   			
		   	              
		   	                if (searchFilter.length()>0 ){
		   	            searchFilter= searchFilter + " and lower(d.SITE_NAME) like lower( '%" + org + "%')" ; 
		   	            
		   	                    } //end if searchfilter-org
		   	            else {
		   	            searchFilter= " where lower(d.SITE_NAME) like lower( '%" + org + "%')" ;     
		   	            
		   	            } //end else org

		   	        } //end org			
		   			 	
		   	        if (tarea.length()>0) { 
		   	            
		   	            if (searchFilter.length()>0 ) {
		   	            searchFilter= searchFilter + " and lower(study_tarea) like lower( '%" + tarea + "%')" ; 
		   	            
		   	            } //end if searchfilter-tarea
		   	            else  {
		   	            searchFilter= " where lower(study_tarea) like lower( '%" + tarea + "%')" ;     
		   	            
		   	            } //end else  searchfilter-tarea
		   	        } //end tarea
		   	        
		   	        
		   	        /*if (diseaseSite.length()>0) {
		   	        	
		   	        	if (searchFilter.length()>0 ){
		 	   	      	   searchFilter= searchFilter +   " and " ;
		 	   	      }else
		 	   	      	  {
		 	   	      	   searchFilter= searchFilter +   " Where " ;
		 	   	      	   
		 	   	      	  }
		   	        	
		   	        	searchFilter = searchFilter + "   lower(STUDY_DISEASE_SITE) like lower( '%" + diseaseSite + "%')" ;
		   	        	
		   	             
		   	        }*/
		   	        
		   	      if (division.length()>0) {
		   	        	
		   	        	if (searchFilter.length()>0 ){
		 	   	      	   searchFilter= searchFilter +   " and " ;
		 	   	      }else
		 	   	      	  {
		 	   	      	   searchFilter= searchFilter +   " Where " ;
		 	   	      	   
		 	   	      	  }
		   	        	
		   	        	searchFilter = searchFilter + "   lower(STUDY_DIVISION_desc) like lower( '%" + division + "%')" ;
		   	        	
		   	             
		   	        }
		   	      
		   	  /* if (researchType.length()>0) {
	  	        	
	  	        	if (searchFilter.length()>0 ){
		   	      	   searchFilter= searchFilter +   " and " ;
		   	      }else
		   	      	  {
		   	      	   searchFilter= searchFilter +   " Where " ;
		   	      	   
		   	      	  }
	  	        	
	  	        	searchFilter = searchFilter + "   lower(study_restype) like lower( '%" + researchType + "%')" ;
	  	        	
	  	             
	  	        }
		   	     
		   	     if (agent_device.length()>0) {
		   	        	
		   	        	if (searchFilter.length()>0 ){
		 	   	      	   searchFilter= searchFilter +   " and " ;
		 	   	      }else
		 	   	      	  {
		 	   	      	   searchFilter= searchFilter +   " Where " ;
		 	   	      	   
		 	   	      	  }
		   	        	
		   	        	searchFilter = searchFilter + "   lower(agent_device) like lower( '%" + agent_device + "%')" ;
		   	        	
		   	             
		   	        }
		   	     
		   	  //include study sites filter
			   	 
		 	   	if (studysites.length()>0) {
		 	   		
		 	   	if (searchFilter.length()>0 ){
		   	      	   searchFilter= searchFilter +   " and " ;
		   	      }else
		   	      	  {
		   	      	   searchFilter= searchFilter +   " Where " ;
		   	      	   
		   	      	  }
		 	   		
		 	   		searchFilter = searchFilter + " lower(study_sites) like lower('%"+studysites+"%')  " ;
		 	   	}
		   	     
		   	        if (kywrd.length()>0) {
		   	             if (searchFilter.length()>0 ) {
		   	            searchFilter= searchFilter + " and lower(d.study_keywrds) like lower( '%" + kywrd + "%')" ; 
		   	            
		   	                 } //end if searchfilter-kywrd
		   	            else {
		   	            searchFilter= " where lower(d.study_keywrds) like lower( '%" + kywrd + "%')" ;     
		   	            
		   	            } //end else kywrd
		   	        } //end kywrd
		   	        */
		   	        if (! StringUtil.isEmpty(searchCriteria)) //search in title only
		   	        {
		   	        	
		   	        	if (searchFilter.length()>0 ) {
			   	            searchFilter= searchFilter + " and  " ; 
			   	            
			   	                 } 
			   	            else {
			   	            	searchFilter= searchFilter + " Where " ;
			   	            
			   	            } //end else 
		   	        	
		   	         //17/5/2010 - bug no 4000 fixed by BK 
		   	 		    /*String newSearchCriteria = StringUtil.replace(searchCriteria,"'","''");	
		   	         	searchFilter= searchFilter+ "  ( lower(d.study_keywrds) like lower( '%" + newSearchCriteria + "%')  " +
		   	         			" or lower(d.study_number) like lower( '%" + newSearchCriteria + "%') or " +
		   	         					" lower(d.study_title) like lower( '%" + newSearchCriteria + "%')   )" ;*/
		   	        	searchFilter= searchFilter+ "lower(d.study_title) like lower( '%" + searchCriteria + "%')" ;
		   	        	
		   	        	
		   	        }
		   	        
		   	        //incase of search of Principal Investigator, the name should be searched in study summary, other and study team
		   	        if (dmg.length()>0){
		   	        	
		   	        	spacePos = dmg.lastIndexOf(" ");

				   	  	   if (spacePos==-1) 
				   	  	   {
				   	  	     searchLname = dmg.toLowerCase().trim();
				   	  	     searchFname = dmg.toLowerCase().trim();
				   	  	   }
				   	  	   else 
				   	  	   {
				   	  	     searchFname = dmg.substring(0,spacePos);  
				   	  	     searchLname = dmg.substring(spacePos);
				   	  	     
				   	  	     searchFname = searchFname.toLowerCase().trim();
				   	  	     searchLname = searchLname.toLowerCase().trim();  
				   	  	   }

		   	            //FIX #5685
				   	  	onlyPIsearchFilter = "  and ( exists (select * from er_user xx where xx.pk_user = a.study_prinv and"
		   	       		+ " (lower(xx.usr_firstname) like ('%"+searchFname+"%') or lower(xx.usr_lastname) like ('%"+searchLname+"%') ) ) or"
		   	       		+ " lower(a.study_otherprinv) like lower('%"+dmg+"%') )";
		   	    		 
		   	        }//dmg end
		   	        
		   	        /*if (stdtype.length()>0) { 
		   	                if (searchFilter.length()>0 ){
		   	            searchFilter= searchFilter + " and lower(d.study_type) like lower( '%" + stdtype + "%')" ; 

		   	                    } //end searchfilter-stdtype
		   	            else {
		   	            searchFilter= " where lower(d.study_type) like lower( '%" + stdtype + "%')" ;     
		   	            
		   	            }//else searchfilter-stdtype
		   	        } //end stdtype
		   	        
		   	        if (sponsor.length()>0) { 
		   	                if (searchFilter.length()>0 ){
		   	            searchFilter= searchFilter + " and  (lower(d.study_sponsor) like lower( '%" + sponsor + "%')" ;
		   	             
		   	                    } // end searchfilter-sponsor
		   	            else {
		   	            searchFilter= " where ( lower(d.study_sponsor) like lower( '%" + sponsor + "%')" ;     
		   	            
		   	            
		   	            } //end else sponsor
		   	            
		   	            searchFilter= searchFilter + " or lower(f_codelst_desc(d.fk_codelst_sponsor)) like lower( '%" + sponsor + "%') ";
		   	            searchFilter= searchFilter + " or lower(d.study_sponsorid) like lower( '%" + sponsor + "%') ) " ;
		   	            
		   	            
		   	            
		   	        } //end sponsor
*/		   	        
		   	        if (stdnumb.length()>0) { 
		   	                if (searchFilter.length()>0 ){
		   	            searchFilter= searchFilter + " and lower(d.study_number) like lower( '%" + stdnumb + "%')" ; 
		   	            
		   	                    } //end if searchfilter-stdnumb
		   	            else {
		   	            searchFilter= " where lower(d.study_number) like lower( '%" + stdnumb + "%')" ;     
		   	            
		   	            } //end else stdnumb
		   	        } //end stdnumb
		   	        
		   	        
		   	        /*if (psites.length()>0) { 
		   	                if (searchFilter.length()>0 )
		   	                {
		   	            searchFilter= searchFilter + " and lower(d.study_partcntr) like lower( '%" + psites+ "%')" ; 
		   	            
		   	                    } //end if searchfilter-psites
		   	            else {
		   	            searchFilter= " where lower(d.study_partcntr) like lower( '%" + psites + "%')" ;     
		   	            
		   	            } //end else psites
		   	        } //end psites
		   	        
		   	        if (team.length()>0) 
		   	        {
		   	        	  spacePos = team.lastIndexOf(" ");

				   	  	   if (spacePos==-1) 
				   	  	   {
				   	  	     searchLname = team.toLowerCase().trim();
				   	  	     searchFname = team.toLowerCase().trim();
				   	  	   }
				   	  	   else 
				   	  	   {
				   	  	     searchLname = team.substring(spacePos);  
				   	  	     searchFname = team.substring(0,spacePos);
				   	  	     
				   	  	     searchFname = searchFname.toLowerCase().trim();
				   	  	     searchLname = searchLname.toLowerCase().trim();  
				   	  	   }
		   	        
				   	        if (searchFilter.length()>0 )
				   	        {
				   	        	searchFilter= searchFilter + " and  " ;
				   	        }
				   	        else
				   	        {
				   	        	searchFilter= searchFilter + " Where   " ;
				   	        	
				   	        }
			 	   	        	     
				   	        	searchFilter= searchFilter + "  ( exists (select * from er_studyteam,er_user where fk_study = pk_study and fk_user = pk_user and "+
				   			"( lower(usr_firstname) like lower('%"+searchFname+"%') or lower(usr_lastname) like  lower('%"+searchLname+"%') or "+
	                        "  lower(usr_firstname) like lower('%"+searchLname+"%') or lower(usr_lastname) like  lower('%"+searchFname+"%') "+
				   			"  ) ) ) ";
				   			
		   	             
		   	        } //end team
		   	        
		   	    if (sec.length()>0){ //modified by sonia abrol 02/02/06, to append the filter to main chdSql
		   	        
		   	    	if (searchFilter.length()>0 ){
		   	      	   searchFilter= searchFilter +   " and " ;
		   	      }else
		   	      	  {
		   	      	   searchFilter= searchFilter +   " Where " ;
		   	      	   
		   	      	  } 
		   	    		   	      	  
		   	            searchFilter= searchFilter
		   	            + " exists ( select * from er_studysec asec where asec.fk_study = pk_study and (lower(asec.studysec_name) like lower('%" + sec + "%') or lower(asec.studysec_text) like lower('%" + sec + "%')  )) "    ;
		   	        
		   	        
		   		} //end sec
		   	     */   

		   	/********************************************************************************** */

		   	if (morestddetails.length()>0) {     
		   	    
		   	            
		   	            if (searchFilter.length()>0 ){
		   	      	   searchFilter= searchFilter +   " and " ;
		   	      }else
		   	      	  {
		   	      	   searchFilter= searchFilter +   " Where " ;
		   	      	   
		   	      	  } 
		   	            //Modified for searching study number in MSD page
		   	         searchFilter= searchFilter +	" exists ( select * from er_studyid i, er_Codelst c where 	pk_study = i.fk_study and " + 	
				   				" c.codelst_type = 'studyidtype' and i.fk_codelst_idtype = c.pk_codelst and c.codelst_subtyp='"+msdCodeSubType+"' " +
				   				" and ((lower(i.studyid_id) = lower('" + morestddetails+ "'))  or ((lower(c.codelst_desc) = lower('" + morestddetails+ "')) and i.studyid_id = 'Y') )  )";
				   					
		   	         //Ashu modified for BUG#5812(9Feb11).
		   	            /*searchFilter= searchFilter +	" exists ( select * from er_studyid i, er_Codelst c where 	pk_study = i.fk_study and " + 	
		   				" c.codelst_type = 'studyidtype' and i.fk_codelst_idtype = c.pk_codelst and c.codelst_subtyp='"+msdCodeSubType+"' " +
		   				" and ((lower(i.studyid_id) like lower('%" + morestddetails+ "%'))  or ((lower(c.codelst_desc) like lower('%" + morestddetails+ "%')) and i.studyid_id = 'Y') )  )";
		   					*/
		   	            
		   	        } //end morestddetails



		   	/*********************************************************************************** */
	 	   	 
		   	/*if (statusType.length()>0) {
		   	      
		   	      
		   	       if (searchFilter.length()>0 ){
		   	      	   searchFilter= searchFilter +   " and " ;
		   	      	   
		   	      }else
		   	      	  {
		   	      	   searchFilter= searchFilter +   " Where " ;
		   	      	   
		   	      	  } 
		   	      	  
		   	     searchFilter=searchFilter + " (lower(status_type) like lower('%" + statusType+ "%') ) " ;
		   	       
		   	 }
		   	 
		   	  if (appndx.length()>0) 
		   	  {
		   	      
		   	      
		   	      if (searchFilter.length()>0 ){
		   	      	   searchFilter= searchFilter +   " and " ;
		   	      	   
		   	      }else
		   	      	  {
		   	      	   searchFilter= searchFilter +   " Where " ;
		   	      	   
		   	      	  } 
		   	           searchFilter= searchFilter +   " exists ( select * from er_studyapndx aapndx,er_studyver ver  where pk_study = ver.fk_study " + 
		   	  	" and ver.pk_studyver = aapndx.fk_studyver and ( lower(aapndx.studyapndx_uri) like lower('%"+appndx+"%') or     lower(aapndx.studyapndx_desc) like lower('%"+appndx +"%')"+
		   	 " or     lower(aapndx.studyapndx_uri) like lower('%"+appndx +"%') ))";
		   		
		   		  
		   	 } // end if for appndx     
		   	  */      

		   

   	  studySql= "SELECT" +
		  		"  distinct pk_study as rowcount,pk_study,study_number,study_title,phase,tarea,site_name,status,status_subtype," +
		  		"  studystat_note,study_actualdt as study_actualdt_datesort, studystat_date as studystat_date_datesort, "+
		   	 	"  (nvl((select study_team_rights from er_studyteam where fk_user = " + userId +" and fk_study = pk_study  and study_team_usr_type = 'D'),'"+superuserRights+"' ) )  study_team_rights  ," +
		   	 	"  sponsor_name , study_restype,STUDY_DIVISION_desc,STUDY_DISEASE_SITE, PI_NAME, study_type ,agent_device ,study_sites,status_type ,current_stat_desc ," +
		   		"  patcount as patcount_numsort, ae_count as ae_count_numsort, sae_count as sae_count_numsort" +
		   		"  FROM" +
		   		"  ( " + 
		   		" 	 	SELECT  " +
		   		"			 a.study_title as study_title, a.pk_study,  " +
		   		"			 (select ER_CODELST.PK_CODELST from ER_CODELST where ER_CODELST.PK_CODELST=a.fk_codelst_phase) phase,"+
		   		"			 (select ER_CODELST.PK_CODELST from ER_CODELST where ER_CODELST.PK_CODELST=a.fk_codelst_tarea) tarea,  " +
		   		"			 a.fk_codelst_sponsor  sponsor_name," +
		   		"			 a.STUDY_DIVISION STUDY_DIVISION_desc," +
		   		"			 F_Getdis_Site(STUDY_DISEASE_SITE) STUDY_DISEASE_SITE ," +
		   		"			 study_prodname as agent_device," +
		   		" 			 a.pk_study study_sites, " +
		   		"			 (select xx.fk_codelst_studystat from er_studystat xx where xx.fk_study = a.pk_study and xx.current_stat = 1  and rownum < 2) current_stat_desc, " +
		   		"			 ( select count(distinct fk_per) from er_patprot pp where pp.fk_study = a.pk_study and patprot_stat = 1) patcount, " +
		   		"			 ( select count(pk_adveve) from sch_adverseve pp where pp.fk_study = a.pk_study ) ae_count, " +
		   		"			 ( select count(pk_adveve) from sch_adverseve pp , sch_codelst where pp.fk_study = a.pk_study and pk_codelst = fk_codlst_aetype and trim(codelst_subtyp) = 'al_sadve') sae_count, " +
		   		"			 (case " +
		   		" 					 when (e.fk_site=(select fk_siteid from er_user where pk_user="+userId+") and e.fk_codelst_studystat  in ("+codeLst+")) " +
		   		" 					 then (select site_name from er_site where pk_site=e.fk_site) else '-' end) as site_name, " +
		   		"			 ( case " +
		   		"					 when (select max(studystat_date) from er_studystat r  where fk_study=e.fk_study and e.fk_codelst_studystat  in("+codeLst+") and fk_site=(select fk_siteid from er_user where pk_user="+userId+") ) " +
		   		" 					 is not null " +
		   		"					 then  (select PK_CODELST from er_codelst where pk_codelst in("+codeLst+") and pk_codelst = e.fk_codelst_studystat)   else  0 end) as status, " +
		   		"			 trim(c.CODELST_SUBTYP) status_subtype," +
		   		"			 (case " +
		   		"					 when (e.fk_site=(select fk_siteid from er_user where pk_user="+userId+") and e.fk_codelst_studystat  in ("+codeLst+")) " +
		   		"					 then e.studystat_note else '-' end) as studystat_note, " +
		   		"			 a.study_actualdt, " +
		   		" 			(case " +
		   		"					 when (e.fk_site=(select fk_siteid from er_user where pk_user="+userId+") and e.fk_codelst_studystat  in ("+codeLst+")) " +
		   		" 					 then nvl(F_Get_Codelstdesc(e.status_type),'-')  else '-' end) as status_type, " +
		   		" 			(case " +
		   		" 					 when (e.fk_site=(select fk_siteid from er_user where pk_user="+userId+") and e.fk_codelst_studystat  in ("+codeLst+")) " +
		   		" 					 then to_char(e.studystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) else ' ' end) as studystat_date," +
		   		"			 a.study_number , a.study_obj_clob, a.study_sum_clob, a.study_keywrds, "+
		   	    "			 (select pk_user from er_user where pk_user = a.study_prinv) PI_name, " + 
		   	    " 			 a.study_otherprinv, " +      
		   	    "			 (select codelst_desc from er_codelst where pk_codelst =a.fk_codelst_tarea) study_tarea,"+
		   	    " 			 a.study_partcntr,"+
		   	    "			 (select codelst_desc from er_codelst where pk_codelst = a.FK_CODELST_restype) study_restype,"+
		   	    "		     (select codelst_desc from er_codelst where pk_codelst = FK_CODELST_TYPE) study_type," +
		   	    "			 a.study_sponsor,a.fk_codelst_sponsor,a.study_sponsorid  " + 
		   	    " 		FROM " +
		   	    "			er_study a ,  er_Codelst  c, er_studystat e     "+
		   	    "		WHERE " +
		   	    "			a.pk_study = e.fk_study   ";

   	  studySql = studySql + 
   			  	" 			and a.fk_account = " + accountId + 
   			  	" 			and  ( exists " +
   			  	"					( select * from er_studyteam where fk_study = pk_study and fk_user = "+userId+" and  nvl(study_team_usr_type,'D')='D') " +
   			  	"					or " +
   			  	"					pkg_superuser.F_Is_Superuser("+userId+", pk_study) = 1 )  " 
   			  	+ onlyPIsearchFilter;
		   	
   	  studySql = studySql + 
   			  	" 			and e.FK_CODELST_STUDYSTAT = c.pk_codelst and c.codelst_type = 'studystat'  " +
   			  	"			and e.pk_studystat = " +
   			  	"				 (CASE " +
   			  	"						WHEN (SELECT COUNT(*) FROM ER_STUDYSTAT WHERE fk_study = e.fk_study AND fk_codelst_studystat IN ( "+codeLst+") and fk_site=(select fk_siteid from er_user where pk_user="+userId+")) > 0 " +
   			  	"						THEN (SELECT MAX(pk_studystat) FROM ER_STUDYSTAT WHERE fk_study = e.fk_study and fk_site=(select fk_siteid from er_user where pk_user="+userId+")  AND fk_codelst_studystat IN ( "+codeLst+")  AND studystat_date = (SELECT MAX(studystat_date) FROM ER_STUDYSTAT WHERE  fk_codelst_studystat IN ( "+codeLst+")   AND fk_study = e.fk_study and fk_site=(select fk_siteid from er_user where pk_user="+userId+"))) " +
   			  	" 						WHEN (SELECT COUNT(*) FROM ER_STUDYSTAT WHERE fk_study = e.fk_study AND fk_codelst_studystat NOT IN ("+codeLst+")  and fk_site=(select fk_siteid from er_user where pk_user="+userId+")) > 0 " +
   			  	"						THEN (SELECT MAX(pk_studystat) FROM ER_STUDYSTAT WHERE fk_study = e.fk_study  and fk_site=(select fk_siteid from er_user where pk_user="+userId+")  AND fk_codelst_studystat NOT IN ("+codeLst+") AND studystat_date = (SELECT MAX(studystat_date) FROM ER_STUDYSTAT WHERE  fk_codelst_studystat NOT IN ("+codeLst+")   AND fk_study = e.fk_study and fk_site=(select fk_siteid from er_user where pk_user="+userId+")))  " +
   			  	"		 		 ELSE " +
   			  	"					   (SELECT MAX(pk_studystat) FROM ER_STUDYSTAT WHERE fk_study = e.fk_study) END)";
		   	  
		   	//modified by sonia abrol, to filter studies with permanent cl. status (even if its not the latest status)
		   	 
		   	//Modified by Manimaran, to fix the Bug2551-Advance Search, option Exclude <Closed> Status should consider login user default Org.
		   	 /*if (excldprmtclsr.length()>0){
		   	studySql = studySql + " and not exists (select * from er_studystat where fk_study = pk_study  and fk_codelst_studystat = "+pmtclsId+ " and fk_site=(select fk_siteid from er_user where pk_user="+userId+")) ";
		   	}
		   	
		   	 if (excldinactive.length()>0){
			   	studySql = studySql + " and STUDY_ACTUALDT is not null ";
			   	} 
		   	*/
		   	if (stdphase.length()>0) { 
	            studySql= studySql + " and  fk_codelst_phase = " + stdphase  ; 
	             
	       } //end stdphase
		   	
		   	if (stdstatus.length()>0) {
		   	  
		   		studySql = studySql + " and e.fk_codelst_studystat = " + stdstatus + "  and e.fk_site=(select fk_siteid from er_user where pk_user="+userId+")" ;
		   	       
		   	 } // end if for stdstatus
		   	
		   	if (currentStatus.length()>0) 
		   	{
		        	
		   		studySql = studySql + "   and  exists ( select * from er_studystat ss where " +
		   				" ss.fk_study = a.pk_study and ss.current_stat = 1  and fk_codelst_studystat = "+ currentStatus + " )" ;
		        	  
		     }
		     
		   	 
		   	 
	       
		   		   	
		   	 
		   	//-- anu 27th may
		   	//Modified by Manimaran to fix the Bug2706 
		   	studySql = studySql +  ")d " +  StringUtil.htmlUnicodePoint(searchFilter) ; 
		   	     
		   	//countSql= "select count(*) from ( " + studySql  + " )";
		    System.out.println("SQL is "+studySql);
		    return StudyServiceDAO.searchStudy(studySql,parameters);
		
		
	}


public String getStudyNum(UserBean callingUser,ResponseHolder response) throws OperationException{
	String mainSql= "select ac_autogen_study from er_account where pk_account = "+callingUser.getUserAccountId();
	String studyNum = "";
	Integer autoGenFlag = 0;
	autoGenFlag = StudyServiceDAO.getStudyNumAutoGenFlag(mainSql, callingUser.getUserAccountId());
	if(autoGenFlag==1){
		studyNum = StudyServiceDAO.getStudyNum(callingUser.getUserAccountId(),response);
	}
	return studyNum;
}

	 
	
}
