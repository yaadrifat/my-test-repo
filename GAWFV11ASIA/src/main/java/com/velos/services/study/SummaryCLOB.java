/**
 * 
 */
package com.velos.services.study;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * The SummaryCLOB EJB exists to persist Clob fields from
 * er_study. eResearch persists them in a separate transaction through
 * a stored procedure. This creates issues in the service layer as 
 * all persistence is done in the container-managed transaction. So,
 * instead of going through the stored procedure for these fields, 
 * we create EJBs to handle them.
 * 
 * @author dylan
 *
 */
@Entity
@Table(name = "er_study")
public class SummaryCLOB implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7830294815123978271L;
	private Integer studyPK;
	private String objective;
	private String summary;
	
	public SummaryCLOB(){
		
	}
	
	@Id
	@Column(name = "PK_STUDY")
	public Integer getStudyPK() {
		return studyPK;
	}
	public void setStudyPK(Integer studyPK) {
		this.studyPK = studyPK;
	}
	
	/*@Lob
	@Basic(fetch=FetchType.EAGER)	
	@Column(name = "STUDY_OBJ_CLOB")*/
	@Transient
	public String getObjective() {
		return objective;
	}
	public void setObjective(String objective) {
		this.objective = objective;
	}
	
	/*@Lob
	@Basic(fetch=FetchType.EAGER)	
	@Column(name = "STUDY_SUM_CLOB")*/
	@Transient
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	
	
}
