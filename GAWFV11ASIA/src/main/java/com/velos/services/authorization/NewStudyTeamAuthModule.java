package com.velos.services.authorization;

public class NewStudyTeamAuthModule extends TeamAuthModule{
	public NewStudyTeamAuthModule() {
		super();
		
		//populate hashtable with default rights that a
		//user inherits during study creation (before study team
		//has been created.
		rightsTable.put(STUDYSUM, 0);
		rightsTable.put(STUDYVER, 0);
		rightsTable.put(STUDYSEC, 0);
		rightsTable.put(STUDYAPNDX, 0);
		rightsTable.put(STUDYCAL, 0);
		rightsTable.put(STUDYPTRACK, PRIVILEGE_NEW);		
		rightsTable.put(STUDYREP, 0);
		rightsTable.put(STUDYTEAM, PRIVILEGE_NEW);
		rightsTable.put(STUDYEUSR, 0);
		rightsTable.put(STUDYNOTIFY, 0);
		rightsTable.put(MILESTONES, 0);
		rightsTable.put(STUDYMPAT, 0);
		rightsTable.put(STUDYVPDET, 0);
		rightsTable.put(STUDYFRMMANAG, 0);
		rightsTable.put(STUDYFRMACC, 0);
	}

}
