/**
 * 
 */
package com.velos.services.authorization;

import java.util.ArrayList;
import java.util.Hashtable;

import com.velos.eres.business.common.CtrlDao;
import com.velos.eres.business.common.TeamDao;


/**
 * Class for testing authorization information obtained from a user's study team
 * membership.
 * 
 * Each instantiation of this module grabs a user's privilages for a certain set
 * of rights.
 * 
 * {@code 
		//populate team auth module and pass it to the various
		//internal methods that require access to the various privileges
		//that you suck up in the contstructor
		TeamAuthModule teamAuth = 
			new TeamAuthModule(callingUser.getUserId(), studyPK);
		
		int studySummaryAuth = teamAuth.getStudySummaryPrivileges();
		
		boolean hasViewStudySummaryPermission = 
			TeamAuthModule.hasViewPermission(studySummaryAuth);
 * }
 * 
 * Group Privileges can be set by membership in a Group. Group membership is at
 * a single level; a group cannot contain a group.
 * 
 * Group privileges are always set for a user based on the user's default group
 * (er_user.fk_grp_default). If a user belongs to multiple groups, authorization
 * is only checked based on the default group. Super User Groups
 * 
 * Each group can has a super-user designation. Within that group, the
 * privileges for super users can be defined. The list of privileges for super
 * users mirrors the privileges for groups. So, essentially, a super user is
 * like a role within a group. User's that are super users in the group inherit
 * the group's super user privileges. Super user privileges follow regular group
 * privileges in that they only apply to the user's default group.
 * 
 * @author dylan
 * 
 */
public class TeamAuthModule extends AbstractAuthModule {

public static final String STUDYSUM = "STUDYSUM"; //Study Summary
public static final String STUDYVER = "STUDYVER"; //Versions
public static final String STUDYSEC = "STUDYSEC"; //Sections
public static final String STUDYAPNDX = "STUDYAPNDX"; //Appendix
public static final String STUDYCAL = "STUDYCAL"; //Study Setup / Notifications / Admin Schedule
public static final String STUDYPTRACK = "STUDYPTRACK"; //Study Status
public static final String STUDYREP = "STUDYREP"; //View Study Reports
public static final String STUDYTEAM = "STUDYTEAM"; //Study Team
public static final String STUDYEUSR = "STUDYEUSR"; //Associate External Users
public static final String STUDYNOTIFY = "STUDYNOTIFY"; //Study Notifications
public static final String MILESTONES = "MILESTONES"; //Milestones
public static final String STUDYMPAT = "STUDYMPAT"; //Manage Patients
public static final String STUDYVPDET = "STUDYVPDET"; //View Complete Patient Data
public static final String STUDYFRMMANAG = "STUDYFRMMANAG"; //Study Form Management
public static final String STUDYFRMACC = "STUDYFRMACC"; //Study Forms Access

	private boolean accessToStudy = false; 
	//KM-#5531- Checking new study team authorization while creating new study.
	protected Hashtable<String, Integer> rightsTable = new Hashtable<String, Integer>();;
	
	protected TeamAuthModule(){}
	
	public TeamAuthModule(int userPK, int studyPK){
		
		TeamDao teamDao = new TeamDao();
		teamDao.getTeamRights(studyPK, userPK);
		
		ArrayList tIds = teamDao.getTeamIds();

		if (tIds.size() > 0) {
			
			ArrayList teamRights = teamDao.getTeamRights();
	
			//TeamDao creates a union of team rights and super user rights
			//and places it into the first one.
			String fullRightsStringForStudy = (String)teamRights.get(0);
			
			CtrlDao cntrl = new CtrlDao() ;
	        cntrl.getControlValues("study_rights");
	        ArrayList cVal = cntrl.getCValue();
	          
	        //build up the table, filtering out headers, that are in there
	        //for display only.
	        for (int counter = 0; counter <  fullRightsStringForStudy.length() ; counter++) {
	        	String privilegeName = (String)cVal.get(counter);
	        	if (!privilegeName.startsWith(HEADER_PREFIX)){
	        		Integer privilegeInt = 
	        			new Integer(String.valueOf(fullRightsStringForStudy.charAt(counter)));
	
		        	rightsTable.put(
		        			privilegeName,
		        			privilegeInt); 
	        	}
	        	

	        }	
	        
	        accessToStudy = true;
		}
	}
	
	/**
	 * Returns the bitflag containing the user's study summary privileges.	
	 * @return
	 */
	public Integer getStudySummaryPrivileges(){
		return getRights(STUDYSUM);
	}
	
	/**
	 * Returns the bitflag containing the user's study version privileges.	
	 * @return
	 */
	public Integer  getStudyVersionPrivileges(){
		return getRights(STUDYVER);
	}
	
	/**
	 * Returns the bitflag containing the user's study section privileges.	
	 * @return
	 */
	public Integer  getStudySectionPrivileges(){
		return getRights(STUDYSEC);
	}
	
	/**
	 * Returns the bitflag containing the user's study appendix privileges.	
	 * @return
	 */
	public Integer  getStudyAppendixSectionPrivileges(){
		return getRights(STUDYAPNDX);
	}
	
	/**
	 * Returns the bitflag containing the user's study setup, notifications, admin schedule privileges.	
	 * @return
	 */
	public Integer  getStudySetupPrivileges(){
		return getRights(STUDYCAL);
	}
	
	/**
	 * Returns the bitflag containing the user's study status privileges.	
	 * @return
	 */
	public Integer  getStudyStatusPrivileges(){
		return getRights(STUDYPTRACK);
	}
	
	/**
	 * Returns the bitflag containing the user's study reports privileges.	
	 * @return
	 */
	public Integer  getStudyReportsPrivileges(){
		return getRights(STUDYREP);
	}
	
	/**
	 * Returns the bitflag containing the user's study team privileges.	
	 * @return
	 */
	public Integer  getStudyTeamPrivileges(){
		return getRights(STUDYTEAM);
	}
	
	/**
	 * Returns the bitflag containing the user's study associate external users privileges.	
	 * @return
	 */
	public Integer  getStudyAssociateExternalUsersPrivileges(){
		return getRights(STUDYEUSR);
	}
	
	/**
	 * Returns the bitflag containing the user's study associate study notifications privileges.	
	 * @return
	 */
	public Integer  getStudyNotificationsPrivileges(){
		return getRights(STUDYNOTIFY);
	}
	
	/**
	 * Returns the bitflag containing the user's  milestones privileges.	
	 * @return
	 */
	public Integer  getStudyMilestonesPrivileges(){
		return getRights(MILESTONES);
	}
	
	/**
	 * Returns the bitflag containing the user's manage patients privileges.	
	 * @return
	 */
	public Integer  getPatientManagePrivileges(){
		return getRights(STUDYMPAT);
	}
	
	/**
	 * Returns the bitflag containing the user's view patient data privileges.	
	 * @return
	 */
	public Integer  getPatientViewDataPrivileges(){
		return getRights(STUDYVPDET);
	}
	
	/**
	 * Returns the bitflag containing the user's view patient data privileges.	
	 * @return
	 */
	public Integer  getFormsStudyManagementPrivileges(){
		return getRights(STUDYFRMMANAG);
	}
	/**
	 * Returns the bitflag containing the user's view patient data privileges.	
	 * @return
	 */
	public Integer  getFormsStudyAccessPrivileges(){
		return getRights(STUDYFRMACC);
	}
	
	public boolean hasAccessToStudy()
	{
		return accessToStudy; 
	}
	
	private Integer getRights(String key){
		if (rightsTable.containsKey(key)){
			return rightsTable.get(key);
		}
		return 0;
	}
}
