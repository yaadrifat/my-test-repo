
package com.velos.services;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@XmlAccessorType(XmlAccessType.FIELD)
public class Issue implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 637426948637267817L;

    public enum IssueSeverity {

		WARNING,
		FATAL

	}

    protected IssueSeverity severity; 

	protected IssueTypes type;

    protected String message;

 
    public Issue(){
    	
    }
    
    public Issue(IssueTypes type) {
		super();
		this.type = type;
	}
    
    public Issue(IssueTypes type, String message) {
		super();
		this.type = type;
		this.message = message;
	}

	/**
     * Gets the value of the severity property.
     * 
     * @return
     *     possible object is
     *     {@link IssueSeverity }
     *     
     */
    IssueSeverity getSeverity() {
        return severity;
    }

    /**
     * Sets the value of the severity property.
     * 
     * @param value
     *     allowed object is
     *     {@link IssueSeverity }
     *     
     */
    void setSeverity(IssueSeverity value) {
        this.severity = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public IssueTypes getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(IssueTypes type) {
        this.type = type;
    }

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessage() {
        return message; 
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessage(String value) {
        this.message = value;
    }

    @Override
    public String toString(){
    	return type.getMessage() + ": " + message;
    }
}
