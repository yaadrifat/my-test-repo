package com.velos.services.version;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.SessionContext;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.xml.sax.SAXException;

import com.velos.eres.business.appendix.impl.StudyApndxPojo;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.util.Configuration;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.EnvUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.services.AbstractService;
import com.velos.services.CodeNotFoundException;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.ValidationException;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.Code;
import com.velos.services.model.StudyApndxIdentifier;
import com.velos.services.model.StudyAppendix;
import com.velos.services.model.StudyStatusHistory;
import com.velos.services.model.StudyVerStatusIdentifier;
import com.velos.services.model.StudyVersion;
import com.velos.services.model.StudyVersionIdentifier;
import com.velos.eres.business.statusHistory.impl.StatusHistoryBean;
import com.velos.eres.business.studyVer.impl.StudyVerBean;
import com.velos.services.model.Versions;
import com.velos.services.util.CodeCache;
import com.velos.services.util.HibernateUtil;


public class VersionServiceDao extends AbstractService{
	private static Logger logger = Logger.getLogger(VersionServiceDao.class); 
	
	
	
	@SuppressWarnings("unchecked")
	public Versions getVersion(Integer studyId,Map<String, Object> parameters,boolean currentStatus) throws IOException, SAXException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException, SQLException{
		ResponseHolder responseHolder = (ResponseHolder) parameters.get("ResponseHolder");
		UserBean callingUser =	(UserBean)parameters.get("callingUser");
		ObjectMapService objectMapService = (ObjectMapService)parameters.get("objectMapService");
		Versions versions = new Versions();
		StudyVerBean studyVersionPojo=new StudyVerBean(); 
		StudyVersion studyVersion=null;;
		StudyApndxPojo studyApndxPojo=new StudyApndxPojo();
		List<StatusHistoryBean> studyStatusHistoryPojoList=new ArrayList<StatusHistoryBean>();
		List<StudyVerBean> versionList = new ArrayList<StudyVerBean>();
		List<StudyVersion> studyVersionList=new ArrayList<StudyVersion>();
		List<StudyAppendix> studyAppendixList=null;
		List<StudyStatusHistory> studyStatusHistoryList=null;
		StudyStatusHistory studyStatusHistory=null;
		StudyAppendix studyAppendix=null;
		StatusHistoryBean studyStatusHistoryPojo=new StatusHistoryBean();
		//List<StudyStatusHistoryPojo> studyHistoryList = new ArrayList<StudyStatusHistoryPojo>();
		Session session = HibernateUtil.eresSessionFactory.openSession(); 
		StudyVersionIdentifier studyVerIdent = new StudyVersionIdentifier();
		StudyApndxIdentifier studyApndxIdent = new StudyApndxIdentifier();
		StudyVerStatusIdentifier studyVerStatIdent = new StudyVerStatusIdentifier();
		Criteria criteria = session.createCriteria(StudyVerBean.class);
		criteria.add(Restrictions.eq("studyVerStudyId", StringUtil.integerToString(studyId))).addOrder(Order.asc("studyVerId"));
		versionList=criteria.list();
		List<StudyApndxPojo> apndxListPojo = new ArrayList<StudyApndxPojo>();
		Iterator<StudyVerBean> verItr = versionList.iterator();
		
		while(verItr.hasNext()){
			
			studyVersion=new StudyVersion();
			studyVersionPojo=new StudyVerBean();
			studyVersionPojo=verItr.next();
			
			studyVerIdent=new StudyVersionIdentifier();
			studyVerIdent.setPK(studyVersionPojo.getStudyVerId());
			ObjectMap objMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY_VERSION, studyVersionPojo.getStudyVerId()); 
			studyVerIdent.setOID(objMap.getOID());
			studyVerIdent.setSTUDYVER_NUMBER(studyVersionPojo.getStudyVerNumber());
			
			studyVersion.setStudyVerIdentifier(studyVerIdent);				

			if(studyVersionPojo.getVersionDate()!=null)
				studyVersion.setSTUDYVER_DATE(studyVersionPojo.getVersionDate());
			if(studyVersionPojo.getStudyVerNotes()!=null)
				studyVersion.setSTUDYVER_NOTES(studyVersionPojo.getStudyVerNotes());

			if(studyVersionPojo.getStudyVerStatus()!=null)
			    studyVersion.setSTUDYVER_STATUS(studyVersionPojo.getStudyVerStatus());
				
			CodeCache codeCache = CodeCache.getInstance();
			if(studyVersionPojo.getStudyVercat()!=null && callingUser.getUserAccountId()!=null){
			Code studyVerCat = codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_STUDY_VERCAT, studyVersionPojo.getStudyVercat(),StringUtil.stringToInteger(callingUser.getUserAccountId()));
			studyVersion.setSTUDYVER_CATEGORY(studyVerCat);
			}
			if(studyVersionPojo.getStudyVertype()!=null && callingUser.getUserAccountId()!=null){
			Code studyVerType = codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_STUDY_VER, studyVersionPojo.getStudyVertype(),StringUtil.stringToInteger(callingUser.getUserAccountId()));
			studyVersion.setSTUDYVER_TYPE(studyVerType);
			}
			
			
			//=========for Study appendex==============
			
			criteria=session.createCriteria(StudyApndxPojo.class);
			criteria.add(Restrictions.eq("FK_STUDYVER", studyVersionPojo.getStudyVerId())).addOrder(Order.asc("PK_STUDYAPNDX"));
			apndxListPojo=criteria.list();
			
			
			String studyApndxFilePath="";
			String studyApndxFileName="";
			String destinationFilePath="";
			Iterator<StudyApndxPojo> apndxItr=apndxListPojo.iterator();
			studyAppendixList=new ArrayList<StudyAppendix>();
			while(apndxItr.hasNext()){
				studyAppendix=new StudyAppendix();
				studyApndxPojo=apndxItr.next();
				studyApndxIdent = new StudyApndxIdentifier();
				studyApndxIdent.setPK(studyApndxPojo.getPK_STUDYAPNDX());
				objMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY_APNDX, studyApndxPojo.getPK_STUDYAPNDX()); 
				studyApndxIdent.setOID(objMap.getOID());
				studyAppendix.setStudyApndxIdent(studyApndxIdent);
				 
				 if(studyApndxPojo.getSTUDYAPNDX_DESC()!=null)
					 studyAppendix.setSTUDYAPNDX_DESC(studyApndxPojo.getSTUDYAPNDX_DESC());
				 if(studyApndxPojo.getSTUDYAPNDX_FILE()!=null)
					 studyAppendix.setSTUDYAPNDX_FILE(studyApndxPojo.getSTUDYAPNDX_FILE());
				 if(studyApndxPojo.getSTUDYAPNDX_FILEOBJ()!=null)
					 studyAppendix.setSTUDYAPNDX_FILEOBJ(studyApndxPojo.getSTUDYAPNDX_FILEOBJ());
				 if(studyApndxPojo.getSTUDYAPNDX_FILESIZE()!=null)
					 studyAppendix.setSTUDYAPNDX_FILESIZE(studyApndxPojo.getSTUDYAPNDX_FILESIZE());
				 if(studyApndxPojo.getSTUDYAPNDX_PUBFLAG()!=null)
					 studyAppendix.setSTUDYAPNDX_PUBFLAG(studyApndxPojo.getSTUDYAPNDX_PUBFLAG());
				 if(studyApndxPojo.getSTUDYAPNDX_TYPE()!=null)
					 studyAppendix.setSTUDYAPNDX_TYPE(studyApndxPojo.getSTUDYAPNDX_TYPE());
				 if(studyApndxPojo.getSTUDYAPNDX_URI()!=null)
					 studyAppendix.setSTUDYAPNDX_URI(studyApndxPojo.getSTUDYAPNDX_URI());
				 
				 studyAppendixList.add(studyAppendix);
				 studyVersion.setStudyAppendixList(studyAppendixList);
				 
			}
			
			
			//=========for study history================
			
			criteria = session.createCriteria(StatusHistoryBean.class);
			if(currentStatus){
				DetachedCriteria maxQuery = DetachedCriteria.forClass(StatusHistoryBean.class);
				maxQuery.add(Restrictions.eq("statusModuleId", StringUtil.integerToString(studyVersionPojo.getStudyVerId())));
				maxQuery.add(Restrictions.eq("statusModuleTable", "er_studyver"));
				maxQuery.setProjection(Projections.max("statusStartDate"));
				DetachedCriteria maxQuery1 = DetachedCriteria.forClass(StatusHistoryBean.class);
				maxQuery1.add(Restrictions.eq("statusModuleId", StringUtil.integerToString(studyVersionPojo.getStudyVerId())));
				maxQuery1.add(Restrictions.eq("statusModuleTable", "er_studyver"));
				maxQuery1.add(Property.forName("statusStartDate").eq(maxQuery));
				maxQuery1.setProjection(Projections.max("statusId"));
				criteria.add(Restrictions.eq("statusModuleId", StringUtil.integerToString(studyVersionPojo.getStudyVerId()))).add(Restrictions.eq("statusModuleTable", "er_studyver")).add(Property.forName("statusId").eq(maxQuery1));}
			else{
				criteria.add(Restrictions.eq("statusModuleId", StringUtil.integerToString(studyVersionPojo.getStudyVerId()))).add(Restrictions.eq("statusModuleTable", "er_studyver")).addOrder(Order.asc("statusId"));}
			studyStatusHistoryPojoList= criteria.list();
			Iterator<StatusHistoryBean> historyItr=studyStatusHistoryPojoList.iterator();
			studyStatusHistoryList=new ArrayList<StudyStatusHistory>();
			while(historyItr.hasNext()){
				studyStatusHistory=new StudyStatusHistory();
				studyStatusHistoryPojo=historyItr.next();
				studyVerStatIdent = new StudyVerStatusIdentifier();
				studyVerStatIdent.setPK(studyStatusHistoryPojo.getStatusId());
				objMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDYVER_HISTORY, studyStatusHistoryPojo.getStatusId()); 
				studyVerStatIdent.setOID(objMap.getOID());
				studyStatusHistory.setStudyVerStatIdent(studyVerStatIdent);
				if(studyStatusHistoryPojo.getRecordType()!=null)
					studyStatusHistory.setRECORD_TYPE(studyStatusHistoryPojo.getRecordType());
				if(studyStatusHistoryPojo.getStatusCustom1()!=null)
					studyStatusHistory.setSTATUS_CUSTOM1(studyStatusHistoryPojo.getStatusCustom1());
				if(studyStatusHistoryPojo.getStatusStartDate()!=null)
					studyStatusHistory.setSTATUS_DATE(studyStatusHistoryPojo.getStatusStartDate());
				if(studyStatusHistoryPojo.getStatusEndDate()!=null)
					studyStatusHistory.setSTATUS_END_DATE(studyStatusHistoryPojo.getStatusEndDate());
				if(studyStatusHistoryPojo.getIsCurrentStat()!=null)
					studyStatusHistory.setSTATUS_ISCURRENT(StringUtil.stringToInteger(studyStatusHistoryPojo.getIsCurrentStat()));
				if(studyStatusHistoryPojo.getStatusNotes()!=null)
					studyStatusHistory.setSTATUS_NOTES(studyStatusHistoryPojo.getStatusNotes());
			/*	if(studyStatusHistoryPojo.getSTATUSENDDATE()!=null)
					studyStatusHistory.setSTATUSENDDATE(studyStatusHistoryPojo.getSTATUSENDDATE());			
			*/	
				if(CodeCache.CODE_TYPE_VER_STAT!=null && studyStatusHistoryPojo.getStatusCodelstId()!=null && callingUser.getUserAccountId()!=null){
				Code studyVerCat = codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_VER_STAT, studyStatusHistoryPojo.getStatusCodelstId(),StringUtil.stringToInteger(callingUser.getUserAccountId()));
				studyStatusHistory.setPK_CODELST_STAT(studyVerCat);
				}
				studyStatusHistoryList.add(studyStatusHistory);
				studyVersion.setStudyStatusHistory(studyStatusHistoryList);
			}
			studyVersionList.add(studyVersion);
		}
		
		versions.setVersion(studyVersionList);
		return versions;
		
		
	}
	
public String[] saveVersions(Integer studyPK,Versions versions, Map<String, Object> parameters,List<Issue> validationIssues) 
throws  IOException, SAXException,Exception{
	
	int typ=EnvUtil.getSystemType();
	String[] retStrings = new String[versions.getVersion().size()];
		int i=0;
		ObjectMapService objectMapService = (ObjectMapService) parameters.get("objectMapService");
		UserAgentRObj userAgent = (UserAgentRObj) parameters.get("userAgent"); 
		SessionContext sessionContext  = (SessionContext) parameters.get("sessionContext"); 
		ResponseHolder responseHolder = (ResponseHolder) parameters.get("ResponseHolder");
		UserBean callingUser = (UserBean) parameters.get("callingUser");
		Session session = (Session) HibernateUtil.eresSessionFactory.openSession();
		
		StudyVerBean stdVerPojo=null;
		StudyVersion stdVer=new StudyVersion();
		
		StudyApndxPojo stdApndxPojo=null;
		StudyAppendix stdApndx=new StudyAppendix();
		
		StatusHistoryBean stdHistoryPojo = null;
		StudyStatusHistory stdHistory = new StudyStatusHistory();
		
		List<StudyVersion> versionList = new ArrayList<StudyVersion>();
		List<StudyAppendix> studyAppendixList = new ArrayList<StudyAppendix>(); 
		List<StudyStatusHistory> studyStatusHistoryList = new ArrayList<StudyStatusHistory>();
		
		versionList=versions.getVersion();
		Iterator<StudyVersion> verItr=versionList.iterator();
		Transaction tx = null;
		
		try{
			
			while(verItr.hasNext()){
			try{
			tx = session.beginTransaction();
			stdVer=verItr.next();
			stdVerPojo= new StudyVerBean();
			if(studyPK>0 && stdVer.getStudyVerIdentifier()!=null){
			if(duplicateCheck(stdVer.getStudyVerIdentifier(),studyPK)>0){
				validationIssues.add(new Issue(IssueTypes.DUPLICATE_STUDYVER_NUMBER, "Study Version Already Exists. for StudyVersion Number : "+stdVer.getStudyVerIdentifier().getSTUDYVER_NUMBER()));
				continue;
			}}
			if(studyPK!=null)
			stdVerPojo.setStudyVerStudyId(StringUtil.integerToString(studyPK));
			stdVerPojo.setIpAdd(IP_ADDRESS_FIELD_VALUE);
			stdVerPojo.setCreator(StringUtil.integerToString(callingUser.getUserId()));
			if(stdVer.getSTUDYVER_DATE()!=null)
			stdVerPojo.setVersionDate(stdVer.getSTUDYVER_DATE());
			stdVerPojo.setStudyVerNumber(stdVer.getStudyVerIdentifier().getSTUDYVER_NUMBER());
			if(stdVer.getSTUDYVER_NOTES()!=null)
			stdVerPojo.setStudyVerNotes(stdVer.getSTUDYVER_NOTES());
			
			if(stdVer.getSTUDYVER_STATUS()!=null)
			stdVerPojo.setStudyVerStatus(stdVer.getSTUDYVER_STATUS());
			try{
				if(stdVer.getSTUDYVER_CATEGORY()!=null && CodeCache.CODE_TYPE_STUDY_VERCAT!=null && callingUser!=null)
					stdVerPojo.setStudyVercat(StringUtil.integerToString(Integer.parseInt(dereferenceCodeStr(stdVer.getSTUDYVER_CATEGORY(), CodeCache.CODE_TYPE_STUDY_VERCAT, callingUser))));	
				}catch(CodeNotFoundException e){
					validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Study Verson Category Code Not Found: "));
					continue;
			}
			try{
				if(stdVer.getSTUDYVER_TYPE()!=null && CodeCache.CODE_TYPE_STUDY_VER!=null && callingUser!=null)
					stdVerPojo.setStudyVertype(StringUtil.integerToString(Integer.parseInt(dereferenceCodeStr(stdVer.getSTUDYVER_TYPE(), CodeCache.CODE_TYPE_STUDY_VER, callingUser))));	
			}catch(CodeNotFoundException e){
				validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Study Verson Type Code Not Found: "));
				continue;
			}
			if(stdVerPojo.getStudyVercat()==null){
				validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Study Verson Category may not be null."));
				continue;
			}
			if(stdVerPojo.getStudyVerStatus()==null){
				validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Study Verson Status may not be null."));
				continue;
			}
			session.saveOrUpdate(stdVerPojo);
			tx.commit();
			studyAppendixList=stdVer.getStudyAppendix();
			
			String studyApndxFilePath="";
			String studyApndxFileName="";
			String destinationFilePath="";
			File dest=null;
			if(studyAppendixList!=null && studyAppendixList.size()>0){
			tx = session.beginTransaction();
			Iterator<StudyAppendix> apndxList=studyAppendixList.iterator();
			while(apndxList.hasNext()){
				
				stdApndx = apndxList.next();
				stdApndxPojo=new StudyApndxPojo();
				
				studyApndxFilePath=stdApndx.getSTUDYAPNDX_FILE();
				studyApndxFileName=stdApndx.getSTUDYAPNDX_URI();
				int absoluteIndex=studyApndxFileName.lastIndexOf(".");
				String fname= studyApndxFileName.substring(0, absoluteIndex);
				
				String newstudyApndxFileName = studyApndxFileName.substring(absoluteIndex);
				String finaName=fname.concat("_"+String.valueOf(System.currentTimeMillis()));
				finaName=finaName.concat(newstudyApndxFileName);
				File f=null;
				f=new File(finaName);
				
					Configuration.readAppendixParam(Configuration.ERES_HOME + "eresearch.xml");
				String destFilePath=Configuration.DOWNLOADFOLDER;//UPLOADFOLDER;//HibernateUtilHelper.getERESConfigFilePath();
				String dbFilePath="";

						if(typ==0){						
							dest = new File(destFilePath+"\\"+f);
							dbFilePath=destFilePath.concat("\\"+studyApndxFileName);
						}else if(typ==1){
							dest = new File(destFilePath+"//"+f);
							dbFilePath=destFilePath.concat("//"+studyApndxFileName);
						}
						
						destinationFilePath = dest.getAbsolutePath();
						long start = System.nanoTime();
						if(stdApndx.getSTUDYAPNDX_FILEOBJ()!=null && !stdApndx.getSTUDYAPNDX_FILEOBJ().equals("")){
						copyFileUsingFileStreams(stdApndx.getSTUDYAPNDX_FILEOBJ(), dest);
						System.out.println("Time taken by FileStreams Copy = "
								+ (System.nanoTime() - start));
						}

				if(studyPK!=null)
				stdApndxPojo.setFK_STUDY(studyPK);
				stdApndxPojo.setIP_ADD(IP_ADDRESS_FIELD_VALUE);
				stdApndxPojo.setCREATOR(callingUser.getUserId());
				stdApndxPojo.setCREATED_ON(new Date());
				if(stdVerPojo.getStudyVerId()>0)
				stdApndxPojo.setFK_STUDYVER(stdVerPojo.getStudyVerId());
				if(stdApndx.getSTUDYAPNDX_DESC()!=null)
				stdApndxPojo.setSTUDYAPNDX_DESC(stdApndx.getSTUDYAPNDX_DESC());
				if(stdApndx.getSTUDYAPNDX_FILE()!=null)
				stdApndxPojo.setSTUDYAPNDX_FILE(dest.toString());
				if(stdApndx.getSTUDYAPNDX_FILESIZE()!=null)
				stdApndxPojo.setSTUDYAPNDX_FILESIZE(stdApndx.getSTUDYAPNDX_FILESIZE());
				if(stdApndx.getSTUDYAPNDX_PUBFLAG()!=null)
				stdApndxPojo.setSTUDYAPNDX_PUBFLAG(stdApndx.getSTUDYAPNDX_PUBFLAG());
				if(stdApndx.getSTUDYAPNDX_TYPE()!=null)
				stdApndxPojo.setSTUDYAPNDX_TYPE(stdApndx.getSTUDYAPNDX_TYPE());
				if(stdApndx.getSTUDYAPNDX_URI()!=null)
				stdApndxPojo.setSTUDYAPNDX_URI(stdApndx.getSTUDYAPNDX_URI());
				if(stdApndx.getSTUDYAPNDX_FILEOBJ()!=null)
				stdApndxPojo.setSTUDYAPNDX_FILEOBJ(stdApndx.getSTUDYAPNDX_FILEOBJ());
				session.saveOrUpdate(stdApndxPojo);
				
						}
			tx.commit();
				}
			studyStatusHistoryList=stdVer.getStudyStatusHistory();
			if(studyStatusHistoryList!=null && studyStatusHistoryList.size()>0){
			Iterator<StudyStatusHistory> historyItr=studyStatusHistoryList.iterator();
			tx = session.beginTransaction();
			while(historyItr.hasNext()){
				stdHistory=historyItr.next();
				stdHistoryPojo=new StatusHistoryBean();
				if(stdHistory.getRECORD_TYPE()!=null)
				stdHistoryPojo.setRecordType(stdHistory.getRECORD_TYPE());
				if(stdHistory.getSTATUS_CUSTOM1()!=null)
				stdHistoryPojo.setStatusCustom1(stdHistory.getSTATUS_CUSTOM1());
				if(stdHistory.getSTATUS_DATE()!=null)
				stdHistoryPojo.setStatusStartDate(stdHistory.getSTATUS_DATE());
				if(stdHistory.getSTATUS_ISCURRENT()!=null)
				stdHistoryPojo.setIsCurrentStat(StringUtil.integerToString(stdHistory.getSTATUS_ISCURRENT()));
				if(stdVerPojo.getStudyVerId()>0)
				stdHistoryPojo.setStatusModuleId(StringUtil.integerToString(stdVerPojo.getStudyVerId()));
				stdHistoryPojo.setStatusModuleTable("er_studyver");
				if(stdHistory.getSTATUS_NOTES()!=null)
				stdHistoryPojo.setStatusNotes(stdHistory.getSTATUS_NOTES());
				stdHistoryPojo.setIpAdd(IP_ADDRESS_FIELD_VALUE);
				stdHistoryPojo.setCreator(StringUtil.integerToString(callingUser.getUserId()));
				stdHistoryPojo.setStatusEnteredBy(StringUtil.integerToString(callingUser.getUserId()));
				if(stdHistory.getSTATUS_END_DATE()!=null)
				stdHistoryPojo.setStatusEndDate(stdHistory.getSTATUS_END_DATE());
				try{
				if(stdHistory.getPK_CODELST_STAT()!=null && CodeCache.CODE_TYPE_VER_STAT!=null && callingUser!=null)
					stdHistoryPojo.setStatusCodelstId(StringUtil.integerToString(Integer.parseInt(dereferenceCodeStr(stdHistory.getPK_CODELST_STAT(), CodeCache.CODE_TYPE_VER_STAT, callingUser))));
				
				}catch(CodeNotFoundException e){}
				session.saveOrUpdate(stdHistoryPojo);
				
					}
			tx.commit();
			}
			retStrings[i]=String.valueOf(stdVerPojo.getStudyVerId());
			i++;
			if (validationIssues.size() > 0){
				((ResponseHolder) parameters.get("ResponseHolder"))
				.getIssues().addAll(validationIssues); 
				
				if (logger.isDebugEnabled()) logger.debug("Validation Issues found for VersionDetails");
				throw new ValidationException();
			}
			}
			catch(ValidationException e)
			{
				System.out.println(((ResponseHolder) parameters.get("ResponseHolder"))
						.getIssues().getIssue().get(0));
				continue;
			}
			}
			
			
		}catch(HibernateException e){
			if(tx!=null)
				tx.rollback();
			logger.error("****************ROLLING BACK Inserts ==============>>>>HibernateException**************"+e.getMessage());
			e.printStackTrace();
		}
		finally{
			HibernateUtil.closeSession(session);
		}
		return retStrings;
}

private static Integer duplicateCheck(StudyVersionIdentifier studyVerIdentifier,Integer studyPk){
	Integer studyVerPk = 0;
	Session session = (Session) HibernateUtil.eresSessionFactory.openSession();
	StudyVerBean studyVerPojo = new StudyVerBean();
	try{
	Criteria criteria = session.createCriteria(StudyVerBean.class);
	criteria.add(Restrictions.eq("studyVerStudyId", StringUtil.integerToString(studyPk)));
	if(studyVerIdentifier!=null && studyVerIdentifier.getSTUDYVER_NUMBER()!=null)
	criteria.add(Restrictions.eq("studyVerNumber", studyVerIdentifier.getSTUDYVER_NUMBER()));
	studyVerPojo = (StudyVerBean)criteria.uniqueResult();
	if(studyVerPojo!=null)
		studyVerPk = studyVerPojo.getStudyVerId();
		
	}catch(HibernateException e){
	e.printStackTrace();
		}
	finally{
	HibernateUtil.closeSession(session);
	}
	return studyVerPk;
}

private static void copyFileUsingFileStreams(byte[] source, File dest)
throws IOException {
FileOutputStream output = null;
try {
output = new FileOutputStream(dest);
output.write(source);
} finally {
output.close();
}
}

private static Blob convertByteArrayToBlob(byte[] bytes,Session session) throws SQLException
{  Blob blob;
	
    blob  = Hibernate.getLobCreator(session).createBlob(bytes);
   
	return blob;
}

private static byte[] convertBlobToByteArray(Blob blob) throws SQLException
{  byte[] bytes;
	
	int blobLength = (int) blob.length();
     bytes = blob.getBytes(1, blobLength);
    
	return bytes;
}
private static byte[] getFileAsStream(File source)
throws IOException {
	
FileInputStream fileInputStream = null;
byte[] buf = new byte[(int) source.length()];
try {
fileInputStream = new FileInputStream(source);
fileInputStream.read(buf);
} finally {
	fileInputStream.close();
}
return buf;
}

}
