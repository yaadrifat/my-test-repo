package com.velos.services.protocolsubmission;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Hashtable;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.apache.log4j.Logger;

import com.google.gson.JsonArray;
import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.EIRBDao;
import com.velos.eres.business.common.StudyDao;
import com.velos.eres.business.common.StudyIdDao;
import com.velos.eres.business.common.StudyVerDao;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.compliance.web.ComplianceJB;
import com.velos.eres.compliance.web.ProtocolChangeLog;
import com.velos.eres.service.grpRightsAgent.GrpRightsAgentRObj;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.util.Configuration;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.statusHistory.StatusHistoryJB;
import com.velos.eres.web.study.StudyJB;
import com.velos.eres.web.studyId.StudyIdJB;
import com.velos.eres.web.studyVer.StudyVerJB;
import com.velos.eres.web.submission.SubmissionBoardJB;
import com.velos.eres.web.submission.SubmissionJB;
import com.velos.eres.web.submission.SubmissionStatusJB;
import com.velos.eres.widget.business.common.ResubmDraftDao;
import com.velos.eres.widget.business.common.UIFlxPageDao;
import com.velos.eres.widget.service.util.FlxPageArchive;
import com.velos.services.AbstractService;
import com.velos.services.AuthorizationException;
import com.velos.services.CRUDAction;
import com.velos.services.CompletedAction;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.authorization.GroupAuthModule;
import com.velos.services.authorization.TeamAuthModule;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.ReviewBoardId;
import com.velos.services.model.ReviewBoardsIdentifier;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.util.ObjectLocator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Stateless
@Remote(ProtocolSubmissionService.class)
public class ProtocolSubmissionServiceImpl extends AbstractService implements ProtocolSubmissionService{

	private static Logger logger = Logger.getLogger(ProtocolSubmissionServiceImpl.class.getName());
	
	@EJB
	private ObjectMapService objectMapService;
	@EJB
	private UserAgentRObj userAgent;
	@EJB
	GrpRightsAgentRObj groupRightsAgent;
	@Resource 
	private SessionContext sessionContext;
	EIRBDao eIrbDao = new EIRBDao();
	String comments = "Submission made by call to the web service (externally from the application)";
	ArrayList boardNameList = null;
	ArrayList boardIdList = null;
	StringBuffer submitToBoards =new StringBuffer(); 
	@Override
	public ResponseHolder submitProtocolVersion(ReviewBoardsIdentifier reviewBoardsIdentifier) throws OperationException {
		// TODO Auto-generated method stub
		try{
			int entityId = 0;
			if(reviewBoardsIdentifier.getStudyIdentifier()==null || (StringUtil.isEmpty(reviewBoardsIdentifier.getStudyIdentifier().getOID()) 
					&& (reviewBoardsIdentifier.getStudyIdentifier().getPK()==null || reviewBoardsIdentifier.getStudyIdentifier().getPK()<=0) 
					&& StringUtil.isEmpty(reviewBoardsIdentifier.getStudyIdentifier().getStudyNumber())))
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid studyIdentifier is required"));
			throw new OperationException();
			}
			GroupAuthModule groupAuth = 
				new GroupAuthModule(callingUser, groupRightsAgent);

			Integer manageProtocolPriv = 
				groupAuth.getAppManageProtocolsPrivileges();
			System.out.println("manageProtocolPriv:::--->"+manageProtocolPriv);
			boolean hasViewManageProt = 
				GroupAuthModule.hasViewPermission(manageProtocolPriv);
			System.out.println("hasNewManageProt:::--->"+hasViewManageProt);
			if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
			if (!hasViewManageProt){
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to view studies"));
				throw new AuthorizationException("User Not Authorized to edit studies");
			}
			Integer entityIdObj=locateStudyPKLcl(reviewBoardsIdentifier.getStudyIdentifier());
			
			if(entityIdObj == null || entityIdObj.equals(0)){
				addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found for StudyIdentifier OID:" + reviewBoardsIdentifier.getStudyIdentifier().getOID() + " StudyNumber " + reviewBoardsIdentifier.getStudyIdentifier().getStudyNumber()));
				throw new OperationException();
			}else{
				entityId=entityIdObj.intValue();
			}
			TeamAuthModule teamAuth = 
					new TeamAuthModule(callingUser.getUserId(), entityId);
				
				int studySummaryAuth = teamAuth.getStudySummaryPrivileges();
				boolean hasViewStudySummaryPermission = 
					TeamAuthModule.hasViewPermission(studySummaryAuth);
			if (!hasViewStudySummaryPermission){
				addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User Not Authorized to view studies"));
				throw new AuthorizationException("User Not Authorized to edit studies");
			}
			StudyJB studyJB = new StudyJB();
			studyJB.setId(entityId);
			studyJB.getStudyDetails();
			if(studyJB.getStudyIsLocked()!=null && studyJB.getStudyIsLocked().equals("1")){
				addIssue(new Issue(IssueTypes.CANNOT_SUBMIT_PROTOCOL, "This protocol cannot be submitted at this time while review is pending."));
				throw new OperationException();
			}
			String grpId = callingUser.getUserGrpDefault();
			String accountId = callingUser.getUserAccountId();
			int recentSubmPk=0;
			boolean studyLockDone =false; 
			if(reviewBoardsIdentifier!=null && reviewBoardsIdentifier.getReviewBoardId().size()>0){
				for(ReviewBoardId reviewBoard:reviewBoardsIdentifier.getReviewBoardId()){
					Integer reviewBoardPK = 0;
					System.out.println("reviewBoard.getPK()--->"+reviewBoard.getPK());
					eIrbDao.getReviewBoards(EJBUtil.stringToNum(accountId),EJBUtil.stringToNum(grpId));
					boardNameList = eIrbDao.getBoardNameList();
				    boardIdList = eIrbDao.getBoardIdList();
				    for(Object o : eIrbDao.getBoardIdList()){
				    	System.out.println("Review board---> "+o.toString());
				    }
				    for(Object o : eIrbDao.getBoardNameList()){
				    	System.out.println("Review board Name---> "+o.toString());
				    }
				    int index=0;
				    if(reviewBoard.getPK()!=null && boardIdList.contains(String.valueOf(reviewBoard.getPK()))){
						reviewBoardPK = reviewBoard.getPK();
						index=boardIdList.indexOf(String.valueOf(reviewBoard.getPK()));
					}else if(reviewBoard.getReviewBoardName()!=null && !reviewBoard.getReviewBoardName().equals("")){
						index= boardNameList.indexOf(reviewBoard.getReviewBoardName());
						//System.out.println("index===="+index);
						if(index!=-1){
							reviewBoardPK = Integer.parseInt(boardIdList.get(index).toString());
						}
					}
					if(reviewBoardPK.intValue()==0){
						StringBuffer errorMessage = new StringBuffer("Review Board not found for:");
						if(reviewBoard.getReviewBoardName()!=null && !reviewBoard.getReviewBoardName().equals("")){
							errorMessage.append( " Name: "+reviewBoard.getReviewBoardName());
						}else if(reviewBoard.getPK()!=null){
							errorMessage.append( " PK: "+reviewBoard.getPK());
						}
						addIssue(new Issue(IssueTypes.BOARD_NOT_FOUND, errorMessage.toString()));
						continue;
						//throw new OperationException();
					}
				    int reviewBoardId=0;
				    if(reviewBoardPK.intValue()!=0){
				    	reviewBoardId=reviewBoardPK.intValue();
				    }
				    System.out.println("Total Boards:::"+boardNameList.size());
				    //Check Submission for the incoming review board
				    boolean checkSubmission = false;
				    if(reviewBoardId!=0 && boardIdList.contains(String.valueOf(reviewBoardId))){
				    	EIRBDao eIrbDaoLogic = new EIRBDao();
				    	eIrbDaoLogic.checkSubmission(entityId, EJBUtil
				                .stringToNum(boardIdList.get(index).toString()));
				        ArrayList resultFlags = eIrbDaoLogic.getResultFlags();
				        ArrayList resultTexts = eIrbDaoLogic.getResultTexts();
				        ArrayList testTexts = eIrbDaoLogic.getTestTexts();
				        if(resultFlags.size()>0){
				        	int isApplicationComplete = -2;
				            boolean thereIsAPass = false;
				            for(int iResult=0; iResult<resultFlags.size(); iResult++) {
				            		            	
				                if ( ((Integer)resultFlags.get(iResult)).intValue() == -1 ) {
				                    isApplicationComplete = -1;
				                } else if ( ((Integer)resultFlags.get(iResult)).intValue() == 0 ) {
				                    thereIsAPass = true;
				                }
				            }
				            if (isApplicationComplete == -2 && thereIsAPass) {
				                isApplicationComplete = 0;
				            }
				            
				            if(isApplicationComplete==0){
				            	checkSubmission=true;
				            }else{
				            	StringBuffer errorMessage = new StringBuffer("Pre-Conditions for this review board not Met ");
								if(reviewBoard.getReviewBoardName()!=null && !reviewBoard.getReviewBoardName().equals("")){
									errorMessage.append( " Name: "+reviewBoard.getReviewBoardName());
								}else if(reviewBoard.getPK()!=null){
									errorMessage.append( " PK: "+reviewBoard.getPK());
								}
								errorMessage.append(", Please check submission for More Details");
				            	addIssue(new Issue(IssueTypes.BOARD_CRITERIA_NOT_MET,errorMessage.toString()));
				            	continue;
				            	//throw new OperationException();
				            }
				        }
				    }else{
				    	StringBuffer errorMessage = new StringBuffer("User Account not have access for Review board ");
				    	if(reviewBoard.getPK()!=null){
				    		errorMessage.append("PK: "+reviewBoard.getPK());
				    	}else if(reviewBoard.getReviewBoardName()!=null && !reviewBoard.getReviewBoardName().equals("")){
				    		errorMessage.append("Name: "+reviewBoard.getReviewBoardName());
				    	}
				    	addIssue(new Issue(IssueTypes.REVIEW_BOARD_AUTHORIZATION, "User Account not have access to Review board"));
						throw new AuthorizationException("User Account not have access to Review board");
				    }
				    
				    //Freeze Study before Submission
				    if(checkSubmission){
				    	ProtocolChangeLog protocolChangeLog = new ProtocolChangeLog();
				    	JSONArray attachDiffList = new JSONArray();
				    	JSONArray protocolDiffList = new JSONArray();
				    	JSONArray formDiffList = new JSONArray();
				    	UIFlxPageDao uiFlxPageDao = new UIFlxPageDao();
						int majVersion = uiFlxPageDao.getHighestFlexPageVersion("er_study",  studyJB.getId());
				    	double fullVersion=0.00;
				    	int newSubmId = 0;
				    	boolean updateChangesFlag=false;
				    	SubmissionJB submB = new SubmissionJB();
				    	SubmissionStatusJB submStatusB=new SubmissionStatusJB();
				    	FlxPageArchive archive = new FlxPageArchive();
				    	
				    	int logBoardId = 0;
				    	String boardName = "";
				    	if(reviewBoard.getReviewBoardName()!=null && !reviewBoard.getReviewBoardName().equals("")){
				    		boardName = reviewBoard.getReviewBoardName();
				    	}else if(reviewBoard.getPK()!=null){
				    		int indx=boardIdList.indexOf(String.valueOf(reviewBoard.getPK()));
				    		//System.out.println("indx===="+indx);
				    		boardName=boardNameList.get(indx).toString();
				    	}
				    	//System.out.println("boardName===="+boardName);
				    	CodeDao logBoardCode = new CodeDao();
				    	logBoardId=logBoardCode.getCodeIdFromDesc("rev_board", boardName);
				    	//System.out.println("logBoardId===="+logBoardId);
				    	if(logBoardId==0){
				    		StringBuffer errorMessage = new StringBuffer("Codelist configuration missing for Review board ");
					    	if(reviewBoard.getPK()!=null){
					    		errorMessage.append("PK: "+reviewBoard.getPK());
					    	}else if(reviewBoard.getReviewBoardName()!=null && !reviewBoard.getReviewBoardName().equals("")){
					    		errorMessage.append("Name: "+reviewBoard.getReviewBoardName());
					    	}
					    	addIssue(new Issue(IssueTypes.REVIEW_BOARD_AUTHORIZATION, errorMessage.toString()));
							throw new AuthorizationException(errorMessage.toString());
				    	}
				    	if(!studyLockDone){
				    		//System.out.println("majVersion===="+majVersion);
				    		if(majVersion>0){
				    			String defUserGroup = callingUser.getUserGrpDefault();
								StudyIdJB studyIdJB = new StudyIdJB();
								StudyIdDao sidDao = new StudyIdDao();
								sidDao = studyIdJB.getStudyIds(entityId, defUserGroup);
								HashMap<String, Object> paramMap = new HashMap<String, Object>();
								paramMap.put("userId", (callingUser.getUserId().equals(null) ? null : Integer.valueOf(callingUser.getUserId())));
								paramMap.put("accountId", (callingUser.getUserAccountId().equals(null) ? null : Integer.valueOf(callingUser.getUserAccountId())));
								paramMap.put("studyId", Integer.valueOf(entityId));
								paramMap.put("defUserGroup", callingUser.getUserGrpDefault());
								paramMap.put("attachFieldDetails", attachDiffList.toString());
								
						    	String fullVersion1 = archive.createOrUpdateResubmissionDraft(studyJB, sidDao, paramMap);
						    	System.out.println("fullVersion1===="+fullVersion1);
							    double versionNum = Double.valueOf(fullVersion1).doubleValue();
							    // Freeze all categories of this version of this study		
							    archive.freezeAttachments(entityId, paramMap, versionNum);

						    	/* Add form locking code here. */

							    //Locking the Study as Version is frozen
							    StudyDao studyDao = new StudyDao();
							    studyDao.lockUnlockStudy(entityId, 1);
					    		recentSubmPk=submB.getNewAmendSubmissionByFkStudy(String.valueOf(entityId));
					    		attachDiffList = protocolChangeLog.fetchAttachProtocolVersionDiff(entityId, callingUser.getUserGrpDefault());
								protocolDiffList = protocolChangeLog.fetchProtocolVersionDiff(entityId, callingUser.getUserGrpDefault());
								formDiffList = protocolChangeLog.fetchFormResponseDiff(entityId, callingUser.getUserGrpDefault());
								attachDiffList = addAutomatedResponse(attachDiffList,logBoardId,"document");
								protocolDiffList = addAutomatedResponse(protocolDiffList,logBoardId,"protocol");
								formDiffList = addAutomatedResponse(formDiffList,logBoardId,"forms");
								
								int retId=0;
								
								retId=saveResubmissionDraft(attachDiffList,protocolDiffList,formDiffList,entityId);
								
								//System.out.println("retId====="+retId);
								//System.out.println("After Calling FlxPageArchive");
								
				    		}
							fullVersion = archive.lockStudy(entityId,callingUser);
							if(fullVersion>0.00){
				    		 
				    	 
					    //Create New Submission
					    
					    submB.setStudy(String.valueOf(entityId));
					    submB.setCreator(String.valueOf(callingUser.getUserId()));
					     newSubmId = submB.createSubmission();
					     
					     EIRBDao eIrbDao = new EIRBDao();
					        eIrbDao.getCurrentOverallStatus(entityId, newSubmId);
					        String currentOverallStatus = eIrbDao.getCurrentOverallStatus();
					        // If the overall status does not already exist for this study, create a new one
					        if (currentOverallStatus == null || currentOverallStatus.length() == 0) {
					            submStatusB.setFkSubmission(String.valueOf(newSubmId));
					            submStatusB.setFkSubmissionBoard(null); // For overall status, this is null
					            //submStatusB.setSubmissionEnteredBy(usr);
					            submStatusB.setCreator(String.valueOf(callingUser.getUserId()));
					            submStatusB.setLastModifiedBy(String.valueOf(callingUser.getUserId()));
					            if(callingUser.getIpAdd()!=null){
					            	submStatusB.setIpAdd(callingUser.getIpAdd());
					            }
					            submStatusB.setSubmissionStatusDate(null);
					            submStatusB.setSubmissionAssignedTo(null);
					            submStatusB.setSubmissionCompletedBy(null);
					            submStatusB.setSubmissionNotes(null);
					         //   submStatusB.setSubmissionStatusBySubtype("submitted");

					            int newSubmStatusId = submStatusB.createSubmissionStatus();
				    	 }
					        updateChangesFlag = true;
					        studyLockDone=true;
					        
				    	 }
				    	}
					    //Map submission to SubmissionBoard 
					    SubmissionBoardJB submBoardB = new SubmissionBoardJB();
					    submBoardB.setFkSubmission(String.valueOf(newSubmId));
					    submBoardB.setFkReviewBoard(String.valueOf(reviewBoardId));
					    submBoardB.setCreator(String.valueOf(callingUser.getUserId()));
					    submBoardB.setFkReviewMeeting(null);
					    submBoardB.setSubmissionReviewer(null);
					    submBoardB.setSubmissionReviewType(null);
					    
					    Hashtable ht = submBoardB.createSubmissionBoard();
					    
					    int newSubmBoardId = ((Integer)ht.get("id")).intValue();
					    
					    //Creating submission status for the Submission
					    if(fullVersion==1.00){
					    	
					    	submStatusB.setFkSubmission(String.valueOf(newSubmId));
				            submStatusB.setFkSubmissionBoard(String.valueOf(newSubmBoardId));
				            submStatusB.setSubmissionEnteredBy(String.valueOf(callingUser.getUserId()));
				            submStatusB.setCreator(String.valueOf(callingUser.getUserId()));
				            submStatusB.setSubmissionStatusDate(null);
				            submStatusB.setSubmissionAssignedTo(null);
				            submStatusB.setSubmissionCompletedBy(null);
				            submStatusB.setSubmissionNotes(null);
						    if (ht.containsKey("previouslySubmitted")) {
						        submStatusB.setSubmissionStatusBySubtype("resubmitted");
						    } else {
						        submStatusB.setSubmissionStatusBySubtype("submitted");
						    }
				            
				            int newSubmStatusIdForBoard = submStatusB.createSubmissionStatus();
				            if(newSubmStatusIdForBoard>1){
				            	EIRBDao eirbDao = new EIRBDao();
				            	eirbDao.updateIscurrentForFinalOutcomeTab(newSubmId,newSubmBoardId);
				                }
					    }else{
					    	System.out.println("Inside else");
					    	int submissionPK = submB.getNewAmendSubmissionByFkStudy(String.valueOf(entityId));
					    	CodeDao codePiRespReq = new CodeDao();
					    	String oldCurrentStatus = submStatusB.getPreviousCurrentSubmissionStatus(submissionPK,newSubmBoardId); 
				    		/*int currentStatusPK = submStatusB.getCurrentSubmissionStatusPK(submissionPK, newSubmBoardId);
				    		submStatusB.getSubmissionStatusDetails(currentStatusPK);
				    		System.out.println("submissionPK---->"+submissionPK);
				    		System.out.println("newSubmBoardId---->"+newSubmBoardId);
				    		System.out.println("currentStatusPK---->"+currentStatusPK);*/
				    		int currentStatus=0;
				    		/*if(currentStatusPK > 0){
				    			currentStatus=submStatusB.getFkSubmissionStatus();
				    		}*/
					    	if(codePiRespReq.getCodeId("subm_status","pi_resp_req")==currentStatus){
					    		
					    		String statusDate = DateUtil.dateToString(Calendar.getInstance().getTime());
					    	    
					    		String piRespondedStatus = "";
					    		 
					    		    
					    	    CodeDao codeDao = new CodeDao();
					         	piRespondedStatus =  String.valueOf(codeDao.getCodeId("subm_status", "pi_respond"));
					         	
					    	    
					    		// set to subm status bean and create new status for PI responded
					            submStatusB.setFkSubmission(String.valueOf(newSubmId));
			            		submStatusB.setFkSubmissionBoard(String.valueOf(newSubmBoardId));
					            submStatusB.setFkSubmissionStatus(piRespondedStatus);
					            submStatusB.setSubmissionEnteredBy(String.valueOf(callingUser.getUserId()));
					            submStatusB.setSubmissionStatusDate(statusDate);
					            submStatusB.setSubmissionNotes("Generic overall comment");
					            submStatusB.setCreator(String.valueOf(callingUser.getUserId()));
					            if(submStatusB.getSubmissionAssignedTo()!=null){
					            	submStatusB.setSubmissionAssignedTo(submStatusB.getSubmissionAssignedTo());
					            }
					            submStatusB.setSubmissionCompletedBy(null);
					            int draftSubmStatusId = submStatusB.createSubmissionStatus();
					            
					    				            
					            
					            // set to subm status bean and create new
					            submStatusB.setFkSubmission(String.valueOf(newSubmId));
			            		submStatusB.setFkSubmissionBoard(String.valueOf(newSubmBoardId));
					            submStatusB.setFkSubmissionStatus(oldCurrentStatus);
					            submStatusB.setIsCurrent(1);
					            submStatusB.setSubmissionEnteredBy(String.valueOf(callingUser.getUserId()));
					            submStatusB.setSubmissionStatusDate(statusDate);
					            submStatusB.setSubmissionNotes(null);
					            submStatusB.setCreator(String.valueOf(callingUser.getUserId()));
					            if(submStatusB.getSubmissionAssignedTo()!=null){
					            	submStatusB.setSubmissionAssignedTo(submStatusB.getSubmissionAssignedTo());
					            }
					            submStatusB.setSubmissionCompletedBy(null);
					            int draftSubmStatusId1 = submStatusB.createSubmissionStatus();
					            
					    	}else{
					    		
					    		submStatusB.setFkSubmission(String.valueOf(newSubmId));
					            submStatusB.setFkSubmissionBoard(String.valueOf(newSubmBoardId));
					            submStatusB.setSubmissionEnteredBy(String.valueOf(callingUser.getUserId()));
					            submStatusB.setCreator(String.valueOf(callingUser.getUserId()));
					            submStatusB.setSubmissionStatusDate(null);
					            submStatusB.setSubmissionAssignedTo(null);
					            submStatusB.setSubmissionCompletedBy(null);
					            submStatusB.setSubmissionNotes(null);
					            
							    if (ht.containsKey("previouslySubmitted")) { 
							    	
							        submStatusB.setSubmissionStatusBySubtype("resubmitted");
							        
							    } else {
							        submStatusB.setSubmissionStatusBySubtype("submitted");
							    }
					            
					            int newSubmStatusIdForBoard = submStatusB.createSubmissionStatus();
					            if(newSubmStatusIdForBoard>1){
					            	EIRBDao eirbDao = new EIRBDao();
					            	eirbDao.updateIscurrentForFinalOutcomeTab(newSubmId,newSubmBoardId);
					                }
					            
					            if(newSubmId > 0){
					            	CodeDao cdao = new CodeDao();
					            	int amendType=cdao.getCodeId("submission", "study_amend");
					            	int tmpnewSubmId = submB.updateAmendFlagSubmission(newSubmId,amendType);
					            	System.out.println("newSubmId:::::"+newSubmId);
					            	System.out.println("tmpnewSubmId:::::"+tmpnewSubmId);
					            	
					            }
					    	}
					    }
					    saveResubmissionMemo(entityId,callingUser,logBoardId,newSubmId,attachDiffList,protocolDiffList,formDiffList);
					    if(submitToBoards.toString().equals("")){
					    	submitToBoards.append(String.valueOf(reviewBoardId));
					    }else{
					    	submitToBoards.append(","+String.valueOf(reviewBoardId));
					    }
					    if(!submitToBoards.toString().equals("") && recentSubmPk>0){
					    	eIrbDao.updateSubmitToBoard(recentSubmPk,submitToBoards.toString());
					    }
					    response.addAction(new CompletedAction(String.valueOf(newSubmId), CRUDAction.CREATE));
				    }
				    }
				
			}else{
				StringBuffer errorMessage = new StringBuffer("Review Board Information Missing");
				addIssue(new Issue(IssueTypes.BOARD_NOT_FOUND, errorMessage.toString()));
				throw new OperationException();
			}
			
		}catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("ProtocolSubmissionServiceImpl submitProtocolVersion", e);
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("ProtocolSubmissionServiceImpl submitProtocolVersion", t);

		}
		return response;
	}
	private void saveResubmissionMemo(int entityId, UserBean callingUser, int revBoardId, int newSubmId,
			JSONArray attachDiffList, JSONArray protocolDiffList, JSONArray formDiffList) {
		// TODO Auto-generated method stub
		
		
		String attachDiffDetails = "[]";
		if(attachDiffList.length()>0){
			attachDiffDetails = attachDiffList.toString();
		}
		String protocolDiffDetails = "[]";
		if(protocolDiffList.length()>0){
			for (int i=0; i < protocolDiffList.length(); i++){
				try {
					JSONObject obj = (JSONObject)protocolDiffList.get(i);
					obj.remove("fieldLabel");
					obj.remove("fieldRow");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			protocolDiffDetails = protocolDiffList.toString();
		}
		String formDiffDetails = "[]";
		if(formDiffList.length()>0){
			formDiffDetails = formDiffList.toString();
		}
		UIFlxPageDao uiFlxPageDao = new UIFlxPageDao();
		int version = uiFlxPageDao.getHighestFlexPageVersion("er_study", entityId);
		uiFlxPageDao.setComments(comments);
        uiFlxPageDao.updateCurrentPageVersionChanges(
                                "er_study", entityId,
                                protocolDiffDetails, newSubmId);
        
        uiFlxPageDao.updateAttachPageDiffFlexPageVersion(entityId,attachDiffDetails);
        System.out.println("formRespDetails----------==========="+formDiffDetails);
        uiFlxPageDao.updateFormPageDiffFlexPageVersion(entityId, formDiffDetails);//saving Form response log
        uiFlxPageDao.updateStatOfLatestVersion("er_study", entityId, "submitting"); // marker to show this version was submitted
	}
	public int saveResubmissionDraft(JSONArray attachDiffList,JSONArray protocolDiffList,JSONArray formDiffList,int entityId){
		int retId =0;
		String attachDiffDetails = "[]";
		UIFlxPageDao uiFlxPageDao = new UIFlxPageDao();
	    int version = uiFlxPageDao.getHighestFlexPageVersion("er_study", entityId);
	    if(version>0){
		if(attachDiffList.length()>0){
			for (int i=0; i < attachDiffList.length(); i++){
				try {
					JSONObject obj = (JSONObject)attachDiffList.get(i);
					obj.remove("fieldLabel");
					obj.remove("fieldRow");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			attachDiffDetails = attachDiffList.toString();
		}
		String protocolDiffDetails = "[]";
		if(protocolDiffList.length()>0){
			for (int i=0; i < protocolDiffList.length(); i++){
				try {
					JSONObject obj = (JSONObject)protocolDiffList.get(i);
					obj.remove("fieldLabel");
					obj.remove("fieldRow");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			protocolDiffDetails = protocolDiffList.toString();
		}
		String formDiffDetails = "[]";
		if(formDiffList.length()>0){
			for (int i=0; i < formDiffList.length(); i++){
				try {
					JSONObject obj = (JSONObject)formDiffList.get(i);
					obj.remove("fieldLabel");
					obj.remove("fieldRow");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			formDiffDetails = formDiffList.toString();
		}
		
		ResubmDraftDao resubmDraftDao = new ResubmDraftDao();
        resubmDraftDao.updateCurrentResubmDraftChanges("er_study", entityId, protocolDiffDetails, attachDiffDetails, formDiffDetails, comments);
        System.out.println("After updateCurrentResubmDraftChanges");
        retId = resubmDraftDao.updateStatOfLatestVersion("er_study", entityId, "submitting");
	    }
		return retId;
	}
	public JSONArray addAutomatedResponse(JSONArray verDiffArray, int revBoardId, String type){
		JSONArray tmpJSONArray = verDiffArray;
		
		for (int i=0; i < tmpJSONArray.length(); i++){
		    try {
				JSONObject obj = (JSONObject)tmpJSONArray.get(i);
				if(type.equals("protocol") || type.equals("forms")){
					obj.put("notes", comments);
					obj.put("revBoard", String.valueOf(revBoardId));
				}else if(type.equals("document")){
					if(obj.getString("fieldLabel").equals("Attachment_New")){
						obj.put("notes1", comments);
						obj.put("revBoard1", String.valueOf(revBoardId));
					}else if(obj.getString("fieldLabel").equals("Attachment_Deleted")){
						obj.put("notes2", comments);
						obj.put("revBoard2", String.valueOf(revBoardId));
					}
				}
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return tmpJSONArray; 
	}
	private Integer locateStudyPKLcl(StudyIdentifier studyIdentifier) 
			throws OperationException{
				//Virendra:#6123,added OID in if clause
				if(studyIdentifier==null || (StringUtil.isEmpty(studyIdentifier.getOID()) 
						&& (studyIdentifier.getPK()==null || studyIdentifier.getPK()<=0) 
						&& StringUtil.isEmpty(studyIdentifier.getStudyNumber())))
				{
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid studyIdentifier is required to addStudyPatientStatus"));
					throw new OperationException();
				}
				Integer studyPK =
						ObjectLocator.studyPKFromIdentifier(this.callingUser, studyIdentifier,objectMapService);
				if (studyPK == null || studyPK.intValue() == 0){
					StringBuffer errorMessage = new StringBuffer("Study not found for:"); 
					if(studyIdentifier.getOID() != null && studyIdentifier.getOID().length() > 0 )
					{
						errorMessage.append(" OID: " + studyIdentifier.getOID()); 
					}
					if(studyIdentifier.getStudyNumber() != null && studyIdentifier.getStudyNumber().length() > 0)
					{
						errorMessage.append( " Study Number: " + studyIdentifier.getStudyNumber()); 
					}
					addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, errorMessage.toString()));
					throw new OperationException();
				}
				return studyPK;
			}
	
	@AroundInvoke
	public Object myInterceptor(InvocationContext ctx) throws Exception {
		response = new ResponseHolder();
		callingUser = 
			getLoggedInUser(
					sessionContext,
					userAgent);
		return ctx.proceed();

	}

}