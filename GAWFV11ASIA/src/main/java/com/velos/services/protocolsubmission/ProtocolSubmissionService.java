package com.velos.services.protocolsubmission;

import javax.ejb.Remote;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.ReviewBoardsIdentifier;
import com.velos.services.model.StudyIdentifier;

@Remote
public interface ProtocolSubmissionService {
	
	public ResponseHolder submitProtocolVersion(ReviewBoardsIdentifier reviewBoardsIdentifier)
			throws OperationException;

}
