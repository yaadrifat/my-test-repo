package com.velos.services.checksubmission;

import javax.ejb.Remote;

import com.velos.services.OperationException;
import com.velos.services.model.ReviewBoards;
import com.velos.services.model.StudyIdentifier;
@Remote
public interface SubmissionLogicService {

	public ReviewBoards getStudyCheckAndSubmitStatusResponse(StudyIdentifier studyIdentifier) throws OperationException;

}
