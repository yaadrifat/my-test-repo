package com.velos.services.milestone;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.SessionContext;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.xml.sax.SAXException;

import com.velos.eres.business.milestone.impl.MilestoneBean;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.study.StudyJB;
import com.velos.esch.business.bgtSection.impl.BgtSectionBean;
import com.velos.esch.business.budget.impl.BudgetBean;
import com.velos.esch.business.budgetcal.impl.BudgetcalBean;
import com.velos.esch.business.eventassoc.impl.EventAssocBean;
import com.velos.esch.business.lineitem.impl.LineitemBean;
import com.velos.esch.business.protvisit.impl.ProtVisitBean;
import com.velos.esch.web.bgtSection.BgtSectionJB;
import com.velos.esch.web.budget.BudgetJB;
import com.velos.esch.web.eventassoc.EventAssocJB;
import com.velos.esch.web.lineitem.LineitemJB;
import com.velos.esch.web.protvisit.ProtVisitJB;
import com.velos.services.Issue;
import com.velos.services.ResponseHolder;
import com.velos.services.ValidationException;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.BgtCalNameIdentifier;
import com.velos.services.model.BgtSectionNameIdentifier;
import com.velos.services.model.BudgetIdentifier;
import com.velos.services.model.CalendarNameIdentifier;
import com.velos.services.model.Code;
import com.velos.services.model.EventNameIdentifier;
import com.velos.services.model.LineItemNameIdentifier;
import com.velos.services.model.Milestone;
import com.velos.services.model.MilestoneIdentifier;
import com.velos.services.model.MilestoneVDAPojo;
import com.velos.services.model.Milestonee;
import com.velos.services.model.VisitNameIdentifier;
import com.velos.services.util.CodeCache;
import com.velos.services.util.HibernateUtil;


public class MilestoneServiceDao {
	private static Logger logger = Logger.getLogger(MilestoneServiceDao.class); 
	@SuppressWarnings("unchecked")
	public List<MilestoneVDAPojo> getMilestoneDetails(String studyId,Map<String, Object> parameters) throws IOException, SAXException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException{
		ObjectMapService objectMapService = (ObjectMapService)parameters.get("objectMapService");
		UserBean callingUser =	(UserBean)parameters.get("callingUser");
		List<MilestoneBean> mileStones = new ArrayList<MilestoneBean>();
		List<MilestoneVDAPojo> mileStoneList = new ArrayList<MilestoneVDAPojo>();
		MilestoneVDAPojo milestone = null;
		MilestoneIdentifier mileIdent = null;
		//MilestoneBean mBean = new MilestoneBean();
		Session session = (Session) HibernateUtil.eresSessionFactory.openSession();
		Criteria creteria = session.createCriteria(MilestoneBean.class);
		creteria.add(Restrictions.eq("milestoneStudyId", studyId)).add(Restrictions.ne("milestoneDelFlag", "Y")).addOrder(Order.asc("milestoneType")).addOrder(Order.asc("id"));
		mileStones=(List<MilestoneBean>)creteria.list();
		HibernateUtil.closeSession(session);
		CalendarNameIdentifier calendarNameIdent=null;
		EventNameIdentifier eventNameIdent=null;
		VisitNameIdentifier visitNameIdent=null;
		BudgetIdentifier budgetIdentifier=null;
		BgtCalNameIdentifier bgtCalNameIdentifier=null;
		BgtSectionNameIdentifier bgtSectionNameIdentifier=null;
		LineItemNameIdentifier lineItemNameIdentifier=null;
		String calAssocto = "";
		for(MilestoneBean mBean : mileStones){
			milestone = new MilestoneVDAPojo();
			mileIdent = new MilestoneIdentifier();
			//mileIdent.setMILESTONE_TYPE(mBean.getMilestoneType());
			mileIdent.setOID(objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_MILESTONE, mBean.getId()).getOID());
			mileIdent.setPK(mBean.getId());
			//milestone.setMilestoneIdent(mileIdent);
			milestone.setPK_MILESTONE(StringUtil.integerToString(mBean.getId()));
			if(mBean.getMilestoneType().equals(MilestoneHandler.PATIENT_MILESTONE))
				milestone.setMSRUL_CATEGORY("Patient Milestone");
			else if(mBean.getMilestoneType().equals(MilestoneHandler.VISIT_MILESTONE))
				milestone.setMSRUL_CATEGORY("Visit Milestone");
			else if(mBean.getMilestoneType().equals(MilestoneHandler.EVENT_MILESTONE))
				milestone.setMSRUL_CATEGORY("Event Milestone");
			else if(mBean.getMilestoneType().equals(MilestoneHandler.STUDY_MILESTONE))
				milestone.setMSRUL_CATEGORY("Study Milestone");
			else
				milestone.setMSRUL_CATEGORY("Additional Milestone");
			milestone.setFK_STUDY(studyId);
			StudyJB studyJB=new StudyJB();
			studyJB.setId(StringUtil.stringToInteger(studyId));
			studyJB.getStudyDetails();
			if(studyJB!=null){
			milestone.setSTUDY_NUMBER(studyJB.getStudyNumber());
			milestone.setSTUDY_TITLE(studyJB.getStudyTitle());
			}
			milestone.setMILESTONE_ACHIEVEDCOUNT(mBean.getMilestoneAchievedCount());
			milestone.setMILESTONE_ISACTIVE(mBean.getMilestoneIsActive());
			milestone.setMILESTONE_AMOUNT(mBean.getMilestoneAmount());
			milestone.setMSRUL_PT_COUNT(mBean.getMilestoneCount());
			milestone.setMILESTONE_DATE_FROM(mBean.getMilestoneDtFrom());
			milestone.setMILESTONE_DATE_TO(mBean.getMilestoneDtTo());
			milestone.setMILESTONE_DESCRIPTION(mBean.getMilestoneDescription());
			milestone.setMILESTONE_HOLDBACK(StringUtil.floatToString(mBean.getHoldBack()));
			milestone.setMSRUL_LIMIT(mBean.getMilestoneLimit());
			CodeCache codeCache = CodeCache.getInstance();
			if(mBean.getMilestoneStatFK()!=null && callingUser.getUserAccountId()!=null){
				Code mileStatFkCat = codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_MILESTONE_STATUS, mBean.getMilestoneStatFK(),StringUtil.stringToInteger(callingUser.getUserAccountId()));
				//milestone.setPK_CODELST_MILESTONE_STAT(mileStatFkCat);
				if(mileStatFkCat!=null){
				milestone.setMILESTAT_SUBTYP(mileStatFkCat.getCode());
				milestone.setMSRUL_STATUS(mileStatFkCat.getDescription());}
				}
			if(mBean.getMilestoneType().equals("AM")){
			if(mBean.getMilestoneStatus()!=null && callingUser.getUserAccountId()!=null){
				Code mileStatCat = codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_MILESTONE_STATUS, mBean.getMilestoneStatus(),StringUtil.stringToInteger(callingUser.getUserAccountId()));
				//milestone.setMILESTONE_STATUS(mileStatCat);
				if(mileStatCat!=null){
				milestone.setMILEPS_SUBTYP(mileStatCat.getCode());
				milestone.setMSRUL_PT_STATUS(mileStatCat.getDescription());}
				}
			if(mBean.getMilestoneEvent()!=null){
				eventNameIdent= new EventNameIdentifier();
				eventNameIdent.setPK(StringUtil.stringToInteger(mBean.getMilestoneEvent()));
				eventNameIdent.setOID(objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY_CALENDAR, StringUtil.stringToInteger(mBean.getMilestoneEvent())).getOID());
				EventAssocJB assocJB = new EventAssocJB();
				assocJB.setEvent_id(StringUtil.stringToInteger(mBean.getMilestoneEvent()));
				assocJB.getEventAssocDetails();
				if(assocJB.getName()!=null){
					eventNameIdent.setEventName(assocJB.getName());
				}
				//milestone.setEventNameIdent(eventNameIdent);
				milestone.setEVENT_NAME(assocJB.getName());
				milestone.setFK_EVENTASSOC(mBean.getMilestoneEvent());
				int calId = StringUtil.stringToInteger(assocJB.getChain_id());
				int visitId= StringUtil.stringToInteger(assocJB.getEventVisit());
				assocJB = new EventAssocJB();
				calAssocto="";
				assocJB.setEvent_id(calId);
				assocJB.getEventAssocDetails();
				calendarNameIdent= new CalendarNameIdentifier();
				calendarNameIdent.setPK(assocJB.getEvent_id());
				calendarNameIdent.setOID(objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY_CALENDAR, assocJB.getEvent_id()).getOID());
				if(assocJB.getName()!=null){
					calendarNameIdent.setCalendarName(assocJB.getName());
					calendarNameIdent.setCalendarAssocTo(assocJB.getCalAssocTo());
					calAssocto = assocJB.getCalAssocTo();
				}
				//milestone.setCalendarNameIdent(calendarNameIdent);
				milestone.setMS_PROT_CALASSOCTO(assocJB.getCalAssocTo());
				milestone.setMSRUL_PROT_CAL(assocJB.getName());
				ProtVisitJB protJb = new ProtVisitJB();
				protJb.setVisit_id(visitId);
				protJb.getProtVisitDetails();
				visitNameIdent = new VisitNameIdentifier();
				visitNameIdent.setPK(protJb.getVisit_id());
				visitNameIdent.setOID(objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_VISIT, protJb.getVisit_id()).getOID());
				if(protJb.getName()!=null)
					visitNameIdent.setVisitName(protJb.getName());
				//milestone.setVisitNameIdent(visitNameIdent);
				milestone.setFK_VISIT(StringUtil.integerToString(protJb.getVisit_id()));
				milestone.setMILESTONE_VISIT(protJb.getName());
				
			 }
			}else if(mBean.getMilestoneType().equals("SM")){
				if(mBean.getMilestoneStatus()!=null && callingUser.getUserAccountId()!=null){
					Code mileStatCat = codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_STATUS, mBean.getMilestoneStatus(),StringUtil.stringToInteger(callingUser.getUserAccountId()));
					//milestone.setMILESTONE_STATUS(mileStatCat);
					if(mileStatCat!=null){
					milestone.setMILEPS_SUBTYP(mileStatCat.getCode());
					milestone.setMSRUL_PT_STATUS(mileStatCat.getDescription());}
					}
			}else{
				if(mBean.getMilestoneStatus()!=null && callingUser.getUserAccountId()!=null){
					Code mileStatCat = codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_PATPROT_STATUS, mBean.getMilestoneStatus(),StringUtil.stringToInteger(callingUser.getUserAccountId()));
					//milestone.setMILESTONE_STATUS(mileStatCat);
					if(mileStatCat!=null){
					milestone.setMILEPS_SUBTYP(mileStatCat.getCode());
					milestone.setMSRUL_PT_STATUS(mileStatCat.getDescription());}
					}
			}
			if(mBean.getMilestoneType().equals("EM")){
			if(mBean.getMilestoneRuleId()!=null && callingUser.getUserAccountId()!=null){
				Code mileRuleCat = codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_EM_RULE, mBean.getMilestoneRuleId(),StringUtil.stringToInteger(callingUser.getUserAccountId()));
				//milestone.setPK_CODELST_RULE(mileRuleCat);
				if(mileRuleCat!=null){
				milestone.setMILERULE_SUBTYP(mileRuleCat.getCode());
				milestone.setMSRUL_MS_RULE(mileRuleCat.getDescription());}
				}
			}else{
				if(mBean.getMilestoneRuleId()!=null && callingUser.getUserAccountId()!=null){
					Code mileRuleCat = codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_VM_RULE, mBean.getMilestoneRuleId(),StringUtil.stringToInteger(callingUser.getUserAccountId()));
					//milestone.setPK_CODELST_RULE(mileRuleCat);
					if(mileRuleCat!=null){
					milestone.setMILERULE_SUBTYP(mileRuleCat.getCode());
					milestone.setMSRUL_MS_RULE(mileRuleCat.getDescription());}
				}
			}
			if(mBean.getMilestoneEventStatus()!=null && callingUser.getUserAccountId()!=null){
				Code mileEventStatCat = codeCache.getSchCodeSubTypeByPK(CodeCache.CODE_TYPE_EVENT_STATUS_TYPE, StringUtil.stringToInteger(mBean.getMilestoneEventStatus()),StringUtil.stringToInteger(callingUser.getUserAccountId()));
				//milestone.setMILESTONE_EVENTSTATUS(mileEventStatCat);
				if(mileEventStatCat!=null){
				milestone.setMILESTONE_EVENTSTATUS(mBean.getMilestoneEventStatus());
				milestone.setMILEVISIT_SUBTYP(mileEventStatCat.getCode());
				milestone.setMSRUL_EVENT_STAT(mileEventStatCat.getDescription());}
				}
			if(mBean.getMilestonePayFor()!=null && callingUser.getUserAccountId()!=null){
				Code milePayForCat = codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_PAY_FOR, mBean.getMilestonePayFor(),StringUtil.stringToInteger(callingUser.getUserAccountId()));
				//milestone.setMILESTONE_PAYFOR(milePayForCat);
				if(milePayForCat!=null){
				milestone.setMILEPAYFR_SUBTYPE(milePayForCat.getCode());
				milestone.setMSRUL_PAY_FOR(milePayForCat.getDescription());}
				}
			if(mBean.getMilestonePayType()!=null && callingUser.getUserAccountId()!=null){
				Code milePayTypeCat = codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_PAY_TYPE, mBean.getMilestonePayType(),StringUtil.stringToInteger(callingUser.getUserAccountId()));
				//milestone.setMILESTONE_PAYTYPE(milePayTypeCat);
				if(milePayTypeCat!=null){
				milestone.setMILEPAY_SUBTYP(milePayTypeCat.getCode());
				milestone.setMSRUL_PAY_TYPE(milePayTypeCat.getDescription());}
				}
			//if(mBean.get!=null && callingUser.getUserAccountId()!=null){
				//Code milePatStudyStatCat = codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_EVENT_STATUS_TYPE, mBean.getMilestoneEventStatus(),StringUtil.stringToInteger(callingUser.getUserAccountId()));
				//milestone.setMILESTONE_PATSTUDY_STAT(milePatStudyStatCat);
			//	}
			calAssocto="";
			if(mBean.getMilestoneCalId()!=null){
			calendarNameIdent= new CalendarNameIdentifier();
			calendarNameIdent.setPK(StringUtil.stringToInteger(mBean.getMilestoneCalId()));
			calendarNameIdent.setOID(objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY_CALENDAR, StringUtil.stringToInteger(mBean.getMilestoneCalId())).getOID());
			EventAssocJB assocJB = new EventAssocJB();
			assocJB.setEvent_id(StringUtil.stringToInteger(mBean.getMilestoneCalId()));
			assocJB.getEventAssocDetails();
			if(assocJB.getName()!=null){
				calendarNameIdent.setCalendarName(assocJB.getName());
				calendarNameIdent.setCalendarAssocTo(assocJB.getCalAssocTo());
				calAssocto = assocJB.getCalAssocTo();
			}
			//milestone.setCalendarNameIdent(calendarNameIdent);
			milestone.setMS_PROT_CALASSOCTO(assocJB.getCalAssocTo());
			milestone.setMSRUL_PROT_CAL(assocJB.getName());
			}
			if(mBean.getMilestoneVisitFK()!=null){
			visitNameIdent = new VisitNameIdentifier();
			visitNameIdent.setPK(StringUtil.stringToInteger(mBean.getMilestoneVisitFK()));
			visitNameIdent.setOID(objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_VISIT, StringUtil.stringToInteger(mBean.getMilestoneVisitFK())).getOID());
			ProtVisitJB protJb = new ProtVisitJB();
			protJb.setVisit_id(StringUtil.stringToInteger(mBean.getMilestoneVisitFK()));
			protJb.getProtVisitDetails();
			if(protJb.getName()!=null)
				visitNameIdent.setVisitName(protJb.getName());
			//milestone.setVisitNameIdent(visitNameIdent);
			milestone.setFK_VISIT(mBean.getMilestoneVisitFK());
			milestone.setMILESTONE_VISIT(protJb.getName());
			}
			if(mBean.getMilestoneEvent()!=null){
				eventNameIdent= new EventNameIdentifier();
				eventNameIdent.setPK(StringUtil.stringToInteger(mBean.getMilestoneEvent()));
				eventNameIdent.setOID(objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY_CALENDAR, StringUtil.stringToInteger(mBean.getMilestoneEvent())).getOID());
				EventAssocJB assocJB = new EventAssocJB();
				assocJB.setEvent_id(StringUtil.stringToInteger(mBean.getMilestoneEvent()));
				assocJB.getEventAssocDetails();
				if(assocJB.getName()!=null){
					eventNameIdent.setEventName(assocJB.getName());
				}
				//milestone.setEventNameIdent(eventNameIdent);
				milestone.setEVENT_NAME(assocJB.getName());
				milestone.setFK_EVENTASSOC(mBean.getMilestoneEvent());
				}
			if(mBean.getFk_Budget()!=null){
				budgetIdentifier= new BudgetIdentifier();
				budgetIdentifier.setPK(StringUtil.stringToInteger(mBean.getFk_Budget()));
				budgetIdentifier.setOID(objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_BUDGET, StringUtil.stringToInteger(mBean.getFk_Budget())).getOID());
				BudgetJB bgtJB = new BudgetJB();
				bgtJB.setBudgetId(StringUtil.stringToInteger(mBean.getFk_Budget()));
				bgtJB.getBudgetDetails();
				if(bgtJB.getBudgetName()!=null){
					budgetIdentifier.setBudgetName(bgtJB.getBudgetName());
					budgetIdentifier.setBudgetVersion(bgtJB.getBudgetVersion());
				}
				//milestone.setBudgetIdentifier(budgetIdentifier);
				milestone.setBUDGET_NAME(bgtJB.getBudgetName());
				milestone.setFK_BUDGET(mBean.getFk_Budget());
				}
			if(mBean.getFk_BgtCal()!=null){
				bgtCalNameIdentifier= new BgtCalNameIdentifier();
				bgtCalNameIdentifier.setPK(StringUtil.stringToInteger(mBean.getFk_BgtCal()));
				bgtCalNameIdentifier.setOID(objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY_CALENDAR, StringUtil.stringToInteger(mBean.getFk_BgtCal())).getOID());
				EventAssocJB assocJB = new EventAssocJB();
				assocJB.setEvent_id(StringUtil.stringToInteger(mBean.getFk_BgtCal()));
				assocJB.getEventAssocDetails();
				if(assocJB.getName()!=null){
					bgtCalNameIdentifier.setBgtCalName(assocJB.getName());
				}
				//milestone.setBgtCalNameIdentifier(bgtCalNameIdentifier);
				milestone.setFK_BGTCAL(mBean.getFk_BgtCal());
				milestone.setCALENDAR_NAME(assocJB.getName());
				}
			
			if(mBean.getFk_BgtSection()!=null){
				bgtSectionNameIdentifier= new BgtSectionNameIdentifier();
				bgtSectionNameIdentifier.setPK(StringUtil.stringToInteger(mBean.getFk_BgtSection()));
				bgtSectionNameIdentifier.setOID(objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_BUDGET_SECTION, StringUtil.stringToInteger(mBean.getFk_BgtSection())).getOID());
				BgtSectionJB bgtSecJB = new BgtSectionJB();
				bgtSecJB.setBgtSectionId(StringUtil.stringToInteger(mBean.getFk_BgtSection()));
				bgtSecJB.getBgtSectionDetails();
				if(bgtSecJB.getBgtSectionName()!=null){
					bgtSectionNameIdentifier.setBgtSectionName(bgtSecJB.getBgtSectionName());
				}
				//milestone.setBgtSectionNameIdentifier(bgtSectionNameIdentifier);
				milestone.setBUDGET_SECTION_NAME(bgtSecJB.getBgtSectionName());
				milestone.setFK_BGTSECTION(mBean.getFk_BgtSection());
				}
			
			if(mBean.getFk_LineItem()!=null){
				lineItemNameIdentifier= new LineItemNameIdentifier();
				lineItemNameIdentifier.setPK(StringUtil.stringToInteger(mBean.getFk_LineItem()));
				lineItemNameIdentifier.setOID(objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_BUDGET_LINEITEM, StringUtil.stringToInteger(mBean.getFk_LineItem())).getOID());
				LineitemJB lineJB = new LineitemJB();
				lineJB.setLineitemId(StringUtil.stringToInteger(mBean.getFk_LineItem()));
				lineJB.getLineitemDetails();
				if(lineJB.getLineitemName()!=null){
					lineItemNameIdentifier.setLineItemName(lineJB.getLineitemName());
				}
				//milestone.setLineItemNameIdentifier(lineItemNameIdentifier);
				milestone.setFK_LINEITEM(mBean.getFk_LineItem());
				milestone.setLINEITEM_NAME(lineJB.getLineitemName());
				}
			
			mileStoneList.add(milestone);
		}
		return mileStoneList;
	}
	
public String[] saveMileStones(Integer studyPK,List<Milestonee> milestones, Map<String, Object> parameters) throws IOException{
		String[] retStrings = new String[milestones.size()];
		int i=0;
		ObjectMapService objectMapService = (ObjectMapService) parameters.get("objectMapService");
		UserAgentRObj userAgent = (UserAgentRObj) parameters.get("userAgent"); 
		SessionContext sessionContext  = (SessionContext) parameters.get("sessionContext"); 
		ResponseHolder responseHolder = (ResponseHolder) parameters.get("ResponseHolder");
		UserBean callingUser = (UserBean) parameters.get("callingUser");
		Session session = (Session) HibernateUtil.eresSessionFactory.openSession();
		MilestoneBean milePojo= null;
		List<Issue> validationIssues = new ArrayList<Issue>();
		try{
			Transaction tx = session.beginTransaction();
			for(Milestonee mPojo:milestones){
				
				try{
					milePojo = new MilestoneBean();
					MilestoneHandler.validateMilestoneDetails(studyPK,mPojo,milePojo, callingUser, parameters,validationIssues); 
					
				}
				catch(ValidationException e)
				{
					System.out.println(((ResponseHolder) parameters.get("ResponseHolder"))
							.getIssues().getIssue().get(0));
					continue;
				}
				session.saveOrUpdate(milePojo);
				retStrings[i]=String.valueOf(milePojo.getId());
				i++;
			}
			tx.commit();
			
		}finally{
			HibernateUtil.closeSession(session);
		}
		
		return retStrings;
	}

public static Integer getCalenderPKByXMLInputString(String CalName,String callAssocTo, String pk) throws  IOException{
		int pk_cal=0;
		logger.info("Getting target database connection");
		Session session=HibernateUtil.eschSessionFactory.openSession();
		logger.info("Connection Established");
		try{
		Criteria creteria = session.createCriteria(EventAssocBean.class);
		creteria.add(Restrictions.eq("name", CalName)).add(Restrictions.eq("event_type", "P"))
		.add(Restrictions.eq("chain_id", pk)).add(Restrictions.eq("calAssocTo", callAssocTo));;
		//Projection p = Projections.property("EVENT_ID");
		//creteria =creteria.setProjection(p);
		EventAssocBean eventAssoc = (EventAssocBean) creteria.uniqueResult();
		if(eventAssoc!=null)
			pk_cal=eventAssoc.getEvent_id();
		}finally{
			HibernateUtil.closeSession(session);
		}
		return pk_cal;
	}

public static Integer getCalenderPKByXMLInputString(CalendarNameIdentifier CalId,String pk) throws  IOException{
	int pk_cal=0;
	logger.info("Getting target database connection");
	Session session=HibernateUtil.eschSessionFactory.openSession();
	logger.info("Connection Established");
	try{
	Criteria creteria = session.createCriteria(EventAssocBean.class);
	if(CalId.getPK()!=null && CalId.getPK()!=0){
		creteria.add(Restrictions.eq("event_id", CalId.getPK()));
	}else{
	creteria.add(Restrictions.eq("name", CalId.getCalendarName())).add(Restrictions.eq("event_type", "P"))
	.add(Restrictions.eq("chain_id", pk)).add(Restrictions.eq("calAssocTo", CalId.getCalendarAssocTo()));
	}
	//Projection p = Projections.property("EVENT_ID");
	//creteria =creteria.setProjection(p);
	EventAssocBean eventAssoc = (EventAssocBean) creteria.uniqueResult();
	if(eventAssoc!=null)
		pk_cal=eventAssoc.getEvent_id();
	}finally{
		HibernateUtil.closeSession(session);
	}
	return pk_cal;
}


public static Integer getVisitPKByXMLInputString(String input, String pk) throws  IOException{
		int pk_protocol_visit=0;
		logger.info("Getting target database connection");
		Session session=HibernateUtil.eschSessionFactory.openSession();
		logger.info("Connection Established");
		try{
		Criteria creteria = session.createCriteria(ProtVisitBean.class);
		creteria.add(Restrictions.eq("protocol_id", StringUtil.stringToInteger(pk))).add(Restrictions.eq("name", input));
		//Projection p = Projections.property("PK_PROTOCOL_VISIT");
		//creteria =creteria.setProjection(p);
		ProtVisitBean schProtVisit = (ProtVisitBean) creteria.uniqueResult();
		if(schProtVisit!=null)
			pk_protocol_visit=schProtVisit.getVisit_id();
		}finally{
			HibernateUtil.closeSession(session);
		}
		return pk_protocol_visit;
	}

public static Integer getVisitPKByXMLInputString(VisitNameIdentifier visitId, String pk) throws  IOException{
	int pk_protocol_visit=0;
	logger.info("Getting target database connection");
	Session session=HibernateUtil.eschSessionFactory.openSession();
	logger.info("Connection Established");
	try{
	Criteria creteria = session.createCriteria(ProtVisitBean.class);
	if(visitId.getPK()!=null && visitId.getPK()!=0){
		creteria.add(Restrictions.eq("visit_id", visitId.getPK()));
	}else{
	creteria.add(Restrictions.eq("protocol_id", StringUtil.stringToInteger(pk))).add(Restrictions.eq("name", visitId.getVisitName()));
	}
	//Projection p = Projections.property("PK_PROTOCOL_VISIT");
	//creteria =creteria.setProjection(p);
	ProtVisitBean schProtVisit = (ProtVisitBean) creteria.uniqueResult();
	if(schProtVisit!=null)
		pk_protocol_visit=schProtVisit.getVisit_id();
	}finally{
		HibernateUtil.closeSession(session);
	}
	return pk_protocol_visit;
}

public static Integer getEventPKByXMLInputString(String input,String event_id,String fk_visit) throws  IOException{
	int pk_event=0;
		logger.info("Getting target database connection");
		Session session=HibernateUtil.eschSessionFactory.openSession();
		logger.info("Connection Established");
		try{
		Criteria creteria = session.createCriteria(EventAssocBean.class);
		creteria.add(Restrictions.eq("chain_id", event_id)).add(Restrictions.eq("event_type", "A"))
		.add(Restrictions.eq("name", input)).add(Restrictions.eq("eventVisit", fk_visit));
		//Projection p = Projections.property("EVENT_ID");
		//creteria =creteria.setProjection(p);
		EventAssocBean eventAssoc = (EventAssocBean) creteria.uniqueResult();
		if(eventAssoc!=null)
			pk_event=eventAssoc.getEvent_id();
		}finally{
			HibernateUtil.closeSession(session);
		}
	return pk_event;
	}

public static Integer getEventPKByXMLInputString(EventNameIdentifier eventId,String event_id,String fk_visit) throws  IOException{
	int pk_event=0;
		logger.info("Getting target database connection");
		Session session=HibernateUtil.eschSessionFactory.openSession();
		logger.info("Connection Established");
		try{
		Criteria creteria = session.createCriteria(EventAssocBean.class);
		if(eventId.getPK()!=null && eventId.getPK()!=0){
		creteria.add(Restrictions.eq("event_id", StringUtil.integerToString(eventId.getPK())));
		}else{
		creteria.add(Restrictions.eq("chain_id", event_id)).add(Restrictions.eq("event_type", "A"))
		.add(Restrictions.eq("name", eventId.getEventName())).add(Restrictions.eq("eventVisit", fk_visit));
		}
		//Projection p = Projections.property("EVENT_ID");
		//creteria =creteria.setProjection(p);
		EventAssocBean eventAssoc = (EventAssocBean) creteria.uniqueResult();
		if(eventAssoc!=null)
			pk_event=eventAssoc.getEvent_id();
		}finally{
			HibernateUtil.closeSession(session);
		}
	return pk_event;
	}

public static Integer getEventPKByXMLAMInputString(String cal,String event_id,String fk_visit) throws  IOException{
	int pk_event=0;
		logger.info("Getting target database connection");
		Session session=HibernateUtil.eschSessionFactory.openSession();
		logger.info("Connection Established");
		try{
		Criteria creteria = session.createCriteria(EventAssocBean.class);
		creteria.add(Restrictions.eq("chain_id", event_id)).add(Restrictions.eq("event_type", "U"))
		.add(Restrictions.eq("name", cal)).add(Restrictions.eq("eventVisit", fk_visit));
		//Projection p = Projections.property("EVENT_ID");
		//creteria =creteria.setProjection(p);
		EventAssocBean eventAssoc = (EventAssocBean) creteria.list().get(0);
		if(eventAssoc!=null)
			pk_event=eventAssoc.getEvent_id();
		}finally{
			HibernateUtil.closeSession(session);
		}
	return pk_event;
	}

public static Integer getEventPKByXMLAMInputString(EventNameIdentifier eventId,String event_id,String fk_visit) throws  IOException{
	int pk_event=0;
		logger.info("Getting target database connection");
		Session session=HibernateUtil.eschSessionFactory.openSession();
		logger.info("Connection Established");
		try{
		Criteria creteria = session.createCriteria(EventAssocBean.class);
		if(eventId.getPK()!=null && eventId.getPK()!=0){
		creteria.add(Restrictions.eq("event_id", StringUtil.integerToString(eventId.getPK())));
		}else{
		creteria.add(Restrictions.eq("chain_id", event_id)).add(Restrictions.eq("event_type", "U"))
		.add(Restrictions.eq("name", eventId.getEventName())).add(Restrictions.eq("eventVisit", fk_visit));
		}
		//Projection p = Projections.property("EVENT_ID");
		//creteria =creteria.setProjection(p);
		EventAssocBean eventAssoc = (EventAssocBean) creteria.uniqueResult();
		if(eventAssoc!=null)
			pk_event=eventAssoc.getEvent_id();
		}finally{
			HibernateUtil.closeSession(session);
		}
	return pk_event;
	}

public static Integer getBudgetPKByXMLInputString(String input, String pk) throws  IOException{
	// select pk_budget from sch_budget where budget_name='?' and fk_study=pk_study; --get pk_budget
		int pk_budget=0;
		logger.info("Getting target database connection");
		Session session=HibernateUtil.eschSessionFactory.openSession();
		logger.info("Connection Established");
		try{
		Criteria creteria = session.createCriteria(BudgetBean.class);
		creteria.add(Restrictions.eq("budgetStudyId", pk)).add(Restrictions.eq("budgetName", input));
		//Projection p = Projections.property("PK_BUDGET");
		//creteria =creteria.setProjection(p);
		BudgetBean schBudget = (BudgetBean) creteria.uniqueResult();
		if(schBudget!=null)
			pk_budget=schBudget.getBudgetId();
		}finally{
			HibernateUtil.closeSession(session);
		}
		return pk_budget;
	}

public static Integer getBudgetFKCal(String pkBudget, String pkCal) throws  IOException{
		int pk_bgtcal=0;
		logger.info("Getting target database connection");
		Session session=HibernateUtil.eschSessionFactory.openSession();
		logger.info("Connection Established");
		try{
		Criteria creteria = session.createCriteria(BudgetcalBean.class);
		creteria.add(Restrictions.eq("budgetProtType", "S")).add(Restrictions.eq("budgetId", pkBudget))
		.add(Restrictions.eq("budgetProtId", pkCal));
		//Projection p = Projections.property("PK_BGTCAL");
		//creteria =creteria.setProjection(p);
		BudgetcalBean schBudgetCal = (BudgetcalBean) creteria.uniqueResult();
		if(schBudgetCal!=null)
			pk_bgtcal=schBudgetCal.getBudgetcalId();
		}finally{
			HibernateUtil.closeSession(session);
		}
	return pk_bgtcal;
	}

public static Integer getBudgetFKCalAM(String pkBudget) throws  IOException{
	int pk_bgtcal=0;
	logger.info("Getting target database connection");
	Session session=HibernateUtil.eschSessionFactory.openSession();
	logger.info("Connection Established");
	try{
	Criteria creteria = session.createCriteria(BudgetcalBean.class);
	creteria.add(Restrictions.eq("budgetId", pkBudget)).add(Restrictions.isNull("budgetProtType")).add(Restrictions.isNull("budgetProtId"));
	//Projection p = Projections.property("PK_BGTCAL");
	//creteria =creteria.setProjection(p);
	BudgetcalBean schBudgetCal = (BudgetcalBean) creteria.uniqueResult();
	if(schBudgetCal!=null)
		pk_bgtcal=schBudgetCal.getBudgetcalId();
	}finally{
		HibernateUtil.closeSession(session);
	}
return pk_bgtcal;
}


public static Integer getBudgetSectionPKByXMLInputStr(String input, String pkBudgetCal) throws  IOException{
		int pk_budgetSec=0;
		logger.info("Getting target database connection");
		Session session=HibernateUtil.eschSessionFactory.openSession();
		logger.info("Connection Established");
		try{
		Criteria creteria = session.createCriteria(BgtSectionBean.class);
		creteria.add(Restrictions.eq("bgtCal", pkBudgetCal)).add(Restrictions.eq("bgtSectionName", input));
		//Projection p = Projections.property("PK_BUDGETSEC");
		//creteria =creteria.setProjection(p);
		BgtSectionBean schBudgetSec = (BgtSectionBean) creteria.uniqueResult();
		if(schBudgetSec!=null)
			pk_budgetSec=schBudgetSec.getBgtSectionId();
		
		}finally{
			HibernateUtil.closeSession(session);
		}
	return pk_budgetSec;
	}



public static Integer getLineItemPKByXMLInputStr(String input, String pkBudgetSec) throws  IOException{
		int pk_lineItem=0;
		logger.info("Getting target database connection");
		Session session=HibernateUtil.eschSessionFactory.openSession();
		logger.info("Connection Established");
		try{
		Criteria creteria = session.createCriteria(LineitemBean.class);
		creteria.add(Restrictions.eq("lineitemBgtSection", pkBudgetSec)).add(Restrictions.eq("lineitemName", input));
		//Projection p = Projections.property("PK_LINEITEM");
		//creteria =creteria.setProjection(p);
		LineitemBean schLineItem = (LineitemBean) creteria.uniqueResult();
		if(schLineItem!=null)
			pk_lineItem=schLineItem.getLineitemId();
		
		}finally{
			HibernateUtil.closeSession(session);
		}
	return pk_lineItem;
	}

}
