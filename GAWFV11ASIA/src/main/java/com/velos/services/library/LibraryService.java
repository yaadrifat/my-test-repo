package com.velos.services.library;
/**
 * @author - Tarandeep Singh Bali
 */

import javax.ejb.Remote;

import com.velos.services.OperationException;
import com.velos.services.model.EventSearch;
import com.velos.services.model.LibraryEvents;

@Remote
public interface LibraryService {
	
	public LibraryEvents searchEvent(EventSearch eventSearch, Integer pageNumber, Integer pageSize) throws OperationException;

}
