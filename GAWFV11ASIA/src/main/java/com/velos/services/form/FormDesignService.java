/**
 * Created On Sep 2, 2011
 */
package com.velos.services.form;

import java.util.List;

import javax.ejb.Remote;

import com.velos.services.OperationException;
import com.velos.services.model.AccountIdentifier;
import com.velos.services.model.FormDesign;
import com.velos.services.model.FormDisplayTypeIdentifier;
import com.velos.services.model.FormIdentifier;
import com.velos.services.model.FormList;
import com.velos.services.model.LinkedFormDesign;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.StudyFormDesign;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyPatientFormDesign;
import com.velos.services.model.StudyPatientScheduleFormDesign;


/**
 * @author Kanwaldeep
 *
 */
@Remote
public interface FormDesignService {
	
	public FormDesign getLibraryFormDesign(FormIdentifier formIdentifier, boolean includeFormatting, String formName)
	throws OperationException; 
	
	public StudyFormDesign getStudyFormDesign(FormIdentifier formIdentifier, StudyIdentifier studyIdentifier, String formName, boolean includeFormatting)
	throws OperationException; 
	
	public LinkedFormDesign getAccountFormDesign(FormIdentifier formIdentifier, AccountIdentifier accountIdentifier, FormDisplayTypeIdentifier formDisplayTypeIdentifier, String formName, boolean includeFormatting)
	throws OperationException; 
	public LinkedFormDesign getPatientFormDesign(FormIdentifier formIdentifier,String formName, boolean includeFormatting)
			throws OperationException;
	
	public StudyPatientFormDesign getStudyPatientFormDesign(FormIdentifier formIdentifier, StudyIdentifier studyIdentifier, String formName, boolean includeFormatting)
	throws OperationException; 
	
	public StudyPatientScheduleFormDesign getStudyPatientScheduleFormDesign(FormIdentifier formIdentifier, StudyIdentifier studyIdentifier, String formName, boolean includeFormatting)
	throws OperationException; 
	
	public 	FormList getAllFormsForStudy(StudyIdentifier studyIdentifier)
	throws OperationException; 

	public FormList getListOfStudyPatientForms(PatientIdentifier patientIdentifier, StudyIdentifier studyIdentifier, int maxNumberOfResults, boolean formHasResponses) 
	throws OperationException;
	
	public FormList getListOfPatientForms(PatientIdentifier patientIdentifier, boolean formHasResponses, boolean ignoreDisplayInPatFlag, int pageNumber, int pageSize) 
	throws OperationException; 
}
