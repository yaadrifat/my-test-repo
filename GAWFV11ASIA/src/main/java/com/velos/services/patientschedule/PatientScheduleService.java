package com.velos.services.patientschedule;

import java.util.Date;

import javax.ejb.Remote;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.EventIdentifier;
import com.velos.services.model.EventStatus;
import com.velos.services.model.EventStatusHistory;
import com.velos.services.model.MEventStatuses;
import com.velos.services.model.MPatientScheduleList;
import com.velos.services.model.MPatientSchedules;
import com.velos.services.model.PatientProtocolIdentifier;
import com.velos.services.model.PatientSchedule;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientSchedules;
import com.velos.services.model.ScheduleEventStatuses;
import com.velos.services.model.SitesOfService;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.VisitIdentifier;
/**
 * Remote Interface declaring methods from service implementation 
 * @author Tarandeep Singh Bali
 *
 */
@Remote
public interface PatientScheduleService{
	/**
	 * 
	 * @param patientId
	 * @param studyIdentifier
	 * @return PatSchedule
	 * @throws OperationException
	 */
	
    public PatientSchedules getPatientScheduleList(PatientIdentifier patientId, StudyIdentifier studyIdentifier) throws OperationException;
    
    public PatientSchedule getPatientSchedule(PatientProtocolIdentifier scheduleOID,VisitIdentifier visitIdentifier,String visitName) throws OperationException;
    
    public PatientSchedule getPatientScheduleVisits(PatientProtocolIdentifier scheduleOID) throws OperationException;
    
    public PatientSchedule getCurrentPatientSchedule(PatientIdentifier patientId, StudyIdentifier studyIdentifier,Date startDate,Date endDate) throws OperationException;
    
    public ResponseHolder addScheduleEventStatus(EventIdentifier eventIdentifier, EventStatus eventStatus) throws OperationException;
    
    public SitesOfService getSitesOfService() throws OperationException;

    
    public ResponseHolder updateMEventStatus(ScheduleEventStatuses scheduleEventStatusIdentifiers) throws OperationException;

	public ResponseHolder createMEventStatus(MEventStatuses mEventStatuses) throws OperationException;
	
	public MPatientScheduleList addMPatientSchedules(MPatientSchedules mPatientSchedules) throws OperationException;
	
	public EventStatusHistory getEventStatusHistory(EventIdentifier eventIdentifier, String sortBy, String orderBy) throws OperationException;
    }
