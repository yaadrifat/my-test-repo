/**
 * Created On Jan 16, 2013
 */
package com.velos.services.patientschedule;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import org.apache.log4j.Logger;

import com.velos.eres.audit.web.AuditRowEresJB;
import com.velos.eres.service.grpRightsAgent.GrpRightsAgentRObj;
import com.velos.eres.service.studySiteAgent.StudySiteAgentRObj;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.userSiteAgent.UserSiteAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.esch.business.eventdef.impl.EventdefBean;
import com.velos.esch.service.eventassocAgent.EventAssocAgentRObj;
import com.velos.esch.service.util.EJBUtil;
import com.velos.services.AbstractService;
import com.velos.services.AuthorizationException;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.authorization.GroupAuthModule;
import com.velos.services.authorization.TeamAuthModule;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.CalendarIdentifier;
import com.velos.services.model.MultipleEvents;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientProtocolIdentifier;
import com.velos.services.model.ScheduleEventIdentifier;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.VisitIdentifier;
import com.velos.services.util.CodeCache;
import com.velos.services.util.ObjectLocator;

/**
 * @author Kanwaldeep
 *
 */
@Remote
@Stateless
public class PatientScheduleServiceBMTImpl
extends AbstractService implements PatientScheduleServiceBMT{

	@EJB
	private ObjectMapService objectMapService;
	
	@EJB
	private EventAssocAgentRObj eventAssocRObj;
	
	@EJB
	private UserAgentRObj userAgent;
	
	@EJB
	GrpRightsAgentRObj groupRightsAgent;
	
	@EJB
	private UserSiteAgentRObj userSiteAgent;
	
	@EJB
	private StudySiteAgentRObj studySiteAgent;

	@Resource 
	private SessionContext sessionContext;
	private static Logger logger = Logger.getLogger(PatientScheduleServiceBMTImpl.class.getName());

	public ResponseHolder addUnscheduledEvent(PatientProtocolIdentifier scheduleID, StudyIdentifier studyIdentifier, PatientIdentifier patientIdentifier, MultipleEvents multipleEventIdentifiers,VisitIdentifier visitIdentifier) throws OperationException {

		UserTransaction tx = sessionContext.getUserTransaction();
		Integer eventPK = 0;
		Integer visitPK = 0;
		Integer patProtPK = 0;
		Integer personPK = 0;
		Integer studyPK = 0;
		Integer eventSOS = 0;
		int protocolID = 0;
		int newEventLength = 0;

		String cost = "0";
		String displ = "0";
		String oldEventCoverageType = "";
		String[] reasonForCoverageChange = new String[1000];
		String [] eventSOSId = new String[1000];
		//String[] strArrEventIds = new String[1000];
		String[] eventStatusPKs = new String[1000];
		String [] eventStartDates = new String[1000];
	    String [] eventNotes = new String[1000];//Bug Fix : 14879
	    String [] eventCoverageTypes = new String[1000];
	    String[] eventStatusValidFrom = new String[1000];

		try{
			
			GroupAuthModule groupAuthModule = new GroupAuthModule(callingUser, groupRightsAgent);
			Integer managePatientPriv = groupAuthModule.getAppManagePatientsPrivileges();			
			if(!GroupAuthModule.hasViewPermission(managePatientPriv))
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to edit Patient data"));
				throw new AuthorizationException("User Not Authorized to edit Patient data");
			}
		
			
			newEventLength = multipleEventIdentifiers.getEvent().size();
			String[][] event_disp = new String[newEventLength][5];
			String [] remarks = new String[multipleEventIdentifiers.getEvent().size()];

			if(scheduleID != null && ((scheduleID.getOID() != null && scheduleID.getOID().length() > 0)
					|| (scheduleID.getPK() != null && scheduleID.getPK() >0))){

				if(scheduleID.getPK() != null && scheduleID.getPK() > 0)
				{
					patProtPK = scheduleID.getPK();
					//Bug Fix : 14958
					if(patProtPK == null || patProtPK == 0 || !PatientScheduleDAO.ifScheduleExists(patProtPK))
					{ 
						addIssue(new Issue(IssueTypes.PATIENT_SCHEDULE_NOT_FOUND,				
								"Patient Schedule not found for Schedule Identifier : PK " + scheduleID.getPK())); 
						throw new OperationException("Patient Schedule not found for Schedule Identifier : PK " + scheduleID.getPK()); 
					}

				}else{

					patProtPK = objectMapService.getObjectPkFromOID(scheduleID.getOID()); 
					if(patProtPK == null || patProtPK == 0 || !PatientScheduleDAO.ifScheduleExists(patProtPK))
					{ 
						addIssue(new Issue(IssueTypes.PATIENT_SCHEDULE_NOT_FOUND,				
								"Patient Schedule not found for Schedule Identifier : OID " + scheduleID.getOID())); 
						throw new OperationException("Patient Schedule not found for Schedule Identifier : OID " + scheduleID.getOID()); 
					}
				}
			}else if(studyIdentifier != null && patientIdentifier !=null 
					&& (studyIdentifier.getPK() != null || studyIdentifier.getOID() != null || studyIdentifier.getStudyNumber() != null)
					&& (patientIdentifier.getOID() != null || patientIdentifier.getPK() != null || (patientIdentifier.getPatientId() != null && patientIdentifier.getOrganizationId() != null))){
				
				studyPK = ObjectLocator.studyPKFromIdentifier(callingUser, studyIdentifier, objectMapService);
				if(studyPK == null || studyPK == 0 || !PatientScheduleDAO.ifStudyExists(studyPK))
				{
					addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found for StudyIdentifier provided"));
					throw new OperationException();
				}
				personPK = ObjectLocator.personPKFromPatientIdentifier(callingUser, patientIdentifier, objectMapService);
				if(personPK == null || personPK == 0)
				{
					addIssue(new Issue(IssueTypes.PATIENT_NOT_FOUND, "Patient not found for PatientIdentifier provided"));
					throw new OperationException();
				}
				
				patProtPK = PatientScheduleDAO.getCurrentSchedule(studyPK, personPK);
				
			}else{
				addIssue(new Issue(IssueTypes.PATIENT_SCHEDULE_IDENTIFIER_NOT_FOUND, "Valid Schedule Identifier or StudyIdentifier and PatientIdentifier is required to add the Unscheduled Event")); 
				throw new OperationException("Valid Schedule Identifier or StudyIdentfier and PatientIdentifier is required to add the Unscheduled Event"); 
			}

			if(personPK == 0) personPK = PatientScheduleDAO.getPersonPK(patProtPK);
			
			if(studyPK == 0) studyPK = PatientScheduleDAO.getStudyPKFromPatProt(patProtPK);
			//check Study Rights 
			TeamAuthModule teamAuthModule = new TeamAuthModule(callingUser.getUserId(), studyPK);
			Integer studyPatientManagementRight = teamAuthModule.getPatientManagePrivileges();
			if(!TeamAuthModule.hasEditPermission(studyPatientManagementRight))
			{
				addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User is not authorized to addUnscheduledEvent for this Study")); 
				throw new OperationException(); 
			}
			
			//check Patient Rights
			if(userSiteAgent.getMaxRightForStudyPatient(callingUser.getUserId(), personPK, studyPK) ==0)
				{
				
				addIssue(new Issue(IssueTypes.ORGANIZATION_AUTHORIZATION, "User not authorized to update Schedule for this Study and Patient")); 
				throw new OperationException();
				
				}
			
			if(visitIdentifier != null && ((visitIdentifier.getOID() != null && visitIdentifier.getOID().length() > 0)
					|| (visitIdentifier.getPK() != null && visitIdentifier.getPK() >0))){

				if(visitIdentifier.getPK() != null && visitIdentifier.getPK() > 0)
				{
					visitPK = visitIdentifier.getPK();
					if(visitPK == null || visitPK == 0 || !PatientScheduleDAO.ifVisitExists(visitPK))
					{ 
						addIssue(new Issue(IssueTypes.VISIT_NOT_FOUND,				
								"Visit not found for Visit Identifier : PK " + visitPK)); 
						throw new OperationException("Visit not found for Visit Identifier : PK " + visitPK); 
					}

				}else{

					visitPK = objectMapService.getObjectPkFromOID(visitIdentifier.getOID());
					//BUg Fix : 15013
					if(visitPK == null || visitPK == 0 || !PatientScheduleDAO.ifVisitExists(visitPK))
					{ 
						addIssue(new Issue(IssueTypes.VISIT_NOT_FOUND,				
								"Visit not found for Visit Identifier : OID " + visitIdentifier.getOID())); 
						throw new OperationException("Visit not found for Visit Identifier : OID " + visitIdentifier.getOID()); 
					}				
				}
			}
			else{
				addIssue(new Issue(IssueTypes.VISIT_IDENTIFIER_INVALID, "Valid Visit Identifier is required to add the Unscheduled Event")); 
				throw new OperationException("Valid Visit Identifier is required to add the Unscheduled Event"); 
			}

			protocolID = PatientScheduleDAO.getProtocolID(visitPK);

			int max_seq = eventAssocRObj.getMaxSeqForVisitEvents(visitPK,"null", "event_assoc" );


			cost = String.valueOf(eventAssocRObj.getMaxCost(EJBUtil.integerToString(protocolID)));

			int costNum = EJBUtil.stringToNum(cost);

			if (cost.equals("-1")) {

				costNum=0;

			}

			else{

				costNum++;

			}

			int counterForEvent_disp = -1;
			String[] strArrEventIds = new String[multipleEventIdentifiers.getEvent().size()];
			for(int i = 0; i < multipleEventIdentifiers.getEvent().size(); i++){

				counterForEvent_disp = counterForEvent_disp+1;

				cost=String.valueOf(costNum);
				
				if(PatientScheduleDAO.ifFDAStudy(studyPK)){//Bug Fix : 16503
					if(multipleEventIdentifiers.getEvent().get(i).getEventAttributes().getEventStatus().getReasonForChange() == null 
							|| StringUtil.isEmpty(multipleEventIdentifiers.getEvent().get(i).getEventAttributes().getEventStatus().getReasonForChange())){
			            if (logger.isDebugEnabled()) logger.debug("Reason For Change FDA Audit field is required");
			            addIssue(
			                    new Issue(IssueTypes.DATA_VALIDATION, 
			                            "Please enter the value for the Reason for Change FDA Audit field"));
			            throw new AuthorizationException("Please enter the value for the Reason for Change FDA Audit field");
					}
				}


				if(multipleEventIdentifiers.getEvent().get(i) != null && multipleEventIdentifiers.getEvent().get(i).getEventIdentifier() != null 
						&& ((multipleEventIdentifiers.getEvent().get(i).getEventIdentifier().getOID() != null && 
						multipleEventIdentifiers.getEvent().get(i).getEventIdentifier().getOID().length() > 0) || (multipleEventIdentifiers.getEvent().get(i).getEventIdentifier().getPK() != null && 
								multipleEventIdentifiers.getEvent().get(i).getEventIdentifier().getPK() >0))){

					if(multipleEventIdentifiers.getEvent().get(i).getEventIdentifier().getPK() != null && multipleEventIdentifiers.getEvent().get(i).getEventIdentifier().getPK() > 0)
					{
						eventPK = multipleEventIdentifiers.getEvent().get(i).getEventIdentifier().getPK();
						if(eventPK == null || eventPK == 0)
						{ 
							addIssue(new Issue(IssueTypes.EVENT_NOT_FOUND,				
									"Event not found for Event Identifier : PK " + multipleEventIdentifiers.getEvent().get(i).getEventIdentifier().getPK())); 
							throw new OperationException("Event not found for Event Identifier : PK " + multipleEventIdentifiers.getEvent().get(i).getEventIdentifier().getPK()); 
						}
					}else{

						eventPK = objectMapService.getObjectPkFromOID(multipleEventIdentifiers.getEvent().get(i).getEventIdentifier().getOID()); 
						if(eventPK == null || eventPK == 0)
						{ 
							addIssue(new Issue(IssueTypes.EVENT_NOT_FOUND,				
									"Event not found for Event Identifier : OID " + multipleEventIdentifiers.getEvent().get(i).getEventIdentifier().getOID())); 
							throw new OperationException("Event not found for Event Identifier : OID " + multipleEventIdentifiers.getEvent().get(i).getEventIdentifier().getOID()); 
						}
					}
					//Bug Fix : 14876
					if(!PatientScheduleDAO.ifEventExists(eventPK)){
						addIssue(new Issue(IssueTypes.EVENT_NOT_FOUND,				
								"Event not found for Event Identifier : PK " + multipleEventIdentifiers.getEvent().get(i).getEventIdentifier().getPK())); 
						throw new OperationException("Event not found for Event Identifier : PK " + multipleEventIdentifiers.getEvent().get(i).getEventIdentifier().getPK()); 
					}

					event_disp[counterForEvent_disp][0]=EJBUtil.integerToString(eventPK);
					event_disp[counterForEvent_disp][1]=displ;
					event_disp[counterForEvent_disp][2]=cost;
					event_disp[counterForEvent_disp][3]=EJBUtil.integerToString(visitPK);
					event_disp[counterForEvent_disp][4] = PatientScheduleDAO.getCategoryName(eventPK);

					costNum++;

				}
				else{
					addIssue(new Issue(IssueTypes.EVENT_IDENTIFIER_INAVLID, "Valid EventIdentifier is required to add the Unscheduled event")); 
					throw new OperationException("Valid EventIdentifier is required to add the Unscheduled Event"); 
				}
				
				
				if(multipleEventIdentifiers.getEvent().get(i).getEventAttributes().getEventStatus().getEventStatusCode() == null || StringUtil.isEmpty(multipleEventIdentifiers.getEvent().get(i).getEventAttributes().getEventStatus().getEventStatusCode().getCode())){
		            if (logger.isDebugEnabled()) logger.debug("Event Status Code is required");
		            addIssue(
		                    new Issue(IssueTypes.DATA_VALIDATION, 
		                            "Please enter a valid Event Status Code"));
		            throw new OperationException("Please enter a valid Event Status Code");
				}
				//Bug Fix : 15118
				try{
					eventStatusPKs[i] = EJBUtil.integerToString(dereferenceSchCode(multipleEventIdentifiers.getEvent().get(i).getEventAttributes().getEventStatus().getEventStatusCode(), CodeCache.CODE_TYPE_EVENT_STATUS_TYPE, callingUser));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					if (logger.isDebugEnabled()) logger.debug("Valid Event Status Code is required");
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Please enter a valid Event Status Code"));
					throw new OperationException("Please enter a valid Event Status Code");
				}
				//Bug Fix : 15151
				if(multipleEventIdentifiers.getEvent().get(i).getEventAttributes().getEventStartDate() == null 
						|| StringUtil.isEmpty(multipleEventIdentifiers.getEvent().get(i).getEventAttributes().getEventStartDate().toString())){
		            if (logger.isDebugEnabled()) logger.debug("Event Start Date is required");
		            addIssue(
		                    new Issue(IssueTypes.DATA_VALIDATION, 
		                            "Please enter a valid Event Start Date"));
		            throw new OperationException("Please enter a valid Event Start Date");
				}
				
				eventStartDates[i] = DateUtil.dateToSqlDate(multipleEventIdentifiers.getEvent().get(i).getEventAttributes().getEventStartDate()).toString();
				
				String statusValidFrom = null;
				if(multipleEventIdentifiers.getEvent().get(i).getEventAttributes().getEventStatus().getStatusValidFrom() != null)
				{
						statusValidFrom = DateUtil.dateToString(multipleEventIdentifiers.getEvent().get(i).getEventAttributes().getEventStatus().getStatusValidFrom());
				}
				eventStatusValidFrom[i] = statusValidFrom; 
				
				eventNotes[i] = multipleEventIdentifiers.getEvent().get(i).getEventAttributes().getEventStatus().getNotes();
				
				//Bug Fix : 14631 && 15012
				if((multipleEventIdentifiers.getEvent().get(i).getEventAttributes().getEventStatus().getCoverageType() == null) || 
						(multipleEventIdentifiers.getEvent().get(i).getEventAttributes().getEventStatus().getSiteOfService() == null) || 
						(multipleEventIdentifiers.getEvent().get(i).getEventAttributes().getEventStatus().getNotes() ==  null)){
				    EventdefBean eventDefBean = new EventdefBean();
					eventDefBean = PatientScheduleDAO.getEventDef(eventPK);
					oldEventCoverageType = eventDefBean.getEventCoverageType();
					if(multipleEventIdentifiers.getEvent().get(i).getEventAttributes().getEventStatus().getCoverageType() == null){
						eventCoverageTypes[i] = eventDefBean.getEventCoverageType();
						if(multipleEventIdentifiers.getEvent().get(i).getEventAttributes().getEventStatus().getReasonForChangeCoverType() != null || 
								!StringUtil.isEmpty(multipleEventIdentifiers.getEvent().get(i).getEventAttributes().getEventStatus().getReasonForChangeCoverType())){
				             if (logger.isDebugEnabled()) logger.debug("Cannot enter new Reason For Coverage Change without changing the Coverage Type");
				             addIssue(
				                     new Issue(IssueTypes.DATA_VALIDATION, 
				                             "Cannot enter new Reason For Coverage Change without changing the Coverage Type"));
				             throw new AuthorizationException("Cannot enter new Reason For Coverage Change without changing the Coverage Type");
						}
						reasonForCoverageChange[i] = eventDefBean.getReasonForCoverageChange();
					}
					
					if(multipleEventIdentifiers.getEvent().get(i).getEventAttributes().getEventStatus().getSiteOfService() == null){
						eventSOSId[i] = eventDefBean.getEventSOSId();
					}
					if(multipleEventIdentifiers.getEvent().get(i).getEventAttributes().getEventStatus().getNotes() == null){
						eventNotes[i] = eventDefBean.getNotes();
					}
				}
				
			if((multipleEventIdentifiers.getEvent().get(i).getEventAttributes().getEventStatus().getCoverageType() != null) 
					&& !StringUtil.isEmpty(multipleEventIdentifiers.getEvent().get(i).getEventAttributes().getEventStatus().getCoverageType().getCode())){
				//bug Fix : 15115
				try{
					eventCoverageTypes[i] = EJBUtil.integerToString(dereferenceSchCode(multipleEventIdentifiers.getEvent().get(i).getEventAttributes().getEventStatus().getCoverageType(), CodeCache.CODE_TYPE_COVERAGE_TYPE, callingUser));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					if (logger.isDebugEnabled()) logger.debug("Valid Coverage Type is required");
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Please enter a valid Coverage Type"));
					throw new OperationException("Please enter a valid Coverage Type");
				}
				if((multipleEventIdentifiers.getEvent().get(i).getEventAttributes().getEventStatus().getReasonForChangeCoverType() == null || 
						StringUtil.isEmpty(multipleEventIdentifiers.getEvent().get(i).getEventAttributes().getEventStatus().getReasonForChangeCoverType())) 
						&& (eventCoverageTypes[i].compareTo(oldEventCoverageType) != 0)){
		             if (logger.isDebugEnabled()) logger.debug("Reason For Coverage Change is Required");
		             addIssue(
		                     new Issue(IssueTypes.DATA_VALIDATION, 
		                             "Please enter the Reason For The Coverage Change"));
		             throw new AuthorizationException("Please enter the Reason For The Coverage Change");
				}
				reasonForCoverageChange[i] = multipleEventIdentifiers.getEvent().get(i).getEventAttributes().getEventStatus().getReasonForChangeCoverType();
			}
			
			//Bug Fix : 14630 && 14629
			if((multipleEventIdentifiers.getEvent().get(i).getEventAttributes().getEventStatus().getSiteOfService() !=null) 
					&& (!StringUtil.isEmpty(multipleEventIdentifiers.getEvent().get(i).getEventAttributes().getEventStatus().getSiteOfService().getSiteName())
					|| !StringUtil.isEmpty(multipleEventIdentifiers.getEvent().get(i).getEventAttributes().getEventStatus().getSiteOfService().getOID())
					|| !StringUtil.isEmpty(multipleEventIdentifiers.getEvent().get(i).getEventAttributes().getEventStatus().getSiteOfService().getSiteAltId()))){	
				try {
					eventSOS = ObjectLocator.sitePKFromIdentifier(callingUser, multipleEventIdentifiers.getEvent().get(i).getEventAttributes().getEventStatus().getSiteOfService(), sessionContext, objectMapService);
					//eventSOSId[i] = EJBUtil.integerToString(ObjectLocator.sitePKFromIdentifier(callingUser, multipleEventIdentifiers.getEvent().get(i).getEventAttributes().getEventStatus().getSiteOfService(), sessionContext, objectMapService));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					if (logger.isDebugEnabled()) logger.debug("Event Site of Service is required");
					addIssue(new Issue(IssueTypes.INVALID_SITE_OF_SERVICE, "Invalid site of service"));
					throw new OperationException("Invalid site of service");
				}

				if (eventSOS == null || eventSOS == 0){
					addIssue(new Issue(IssueTypes.INVALID_SITE_OF_SERVICE, "Invalid site of service"));
					throw new OperationException("Invalid site of service");
				}
				//15117
				eventSOSId[i] = EJBUtil.integerToString(eventSOS);
			}
			//Bug Fix : 14612
			if(multipleEventIdentifiers.getEvent().get(i).getEventAttributes().getEventStatus().getStatusValidFrom() == null 
					|| StringUtil.isEmpty(multipleEventIdentifiers.getEvent().get(i).getEventAttributes().getEventStatus().getStatusValidFrom().toString())){
	            if (logger.isDebugEnabled()) logger.debug("Status Valid From field is required");
	            addIssue(
	                    new Issue(IssueTypes.DATA_VALIDATION, 
	                            "Please enter the value for the Status Valid From field"));
	            throw new AuthorizationException("Please enter the value for the Status Valid From field");
			}
			remarks[i] = multipleEventIdentifiers.getEvent().get(i).getEventAttributes().getEventStatus().getReasonForChange();
			}

			tx.begin();
			strArrEventIds = eventAssocRObj.updateUnscheduledEvents(EJBUtil.integerToString(protocolID),event_disp,0,0, max_seq, EJBUtil.integerToString(callingUser.getUserId()), AbstractService.IP_ADDRESS_FIELD_VALUE);
			tx.commit();

			String[] strArrSchEventIds = PatientScheduleDAO.updateUnscheduledEvents1(personPK, patProtPK ,protocolID,callingUser.getUserId(), 
					                                                                 AbstractService.IP_ADDRESS_FIELD_VALUE, eventStatusPKs, eventStartDates, 
					                                                                 eventNotes, eventCoverageTypes, eventSOSId, reasonForCoverageChange, eventStatusValidFrom,//Bug Fix : 14611
					                                                                 strArrEventIds);
			
			for(int i = 0; i < multipleEventIdentifiers.getEvent().size(); i++){
					
				if (!StringUtil.isEmpty(remarks[i])){
					AuditRowEresJB auditJB = new AuditRowEresJB();
					auditJB.setReasonForChangeOfPatProt(patProtPK, callingUser.getUserId(), remarks[i]);
				}
			}
			
			for(String x:strArrSchEventIds)
			{
				ScheduleEventIdentifier identifier = new ScheduleEventIdentifier(); 
				identifier.setPK(Integer.parseInt(x));
				//Bug Fix : 14877
				identifier.setOID(objectMapService.getOrCreateObjectMapFromPK("sch_events1", Integer.parseInt(x)).getOID());
				response.addObjectCreatedAction(identifier);


			}


 

		}	catch(OperationException e){
			//	sessionContext.setRollbackOnly();
			try {
				tx.rollback();
			} catch (IllegalStateException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (SecurityException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (SystemException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if (logger.isDebugEnabled()) logger.debug("PatientScheduleServiceImpl create", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			try {
				tx.rollback();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("PatientScheduleServiceImpl create", t);
			throw new OperationException(t);
		}


		return response;
	}
	
	
	public ResponseHolder addUnscheduledEvent(CalendarIdentifier scheduleID, MultipleEvents events, VisitIdentifier visitIdentifier) throws OperationException {

		UserTransaction tx = sessionContext.getUserTransaction();
		Integer eventPK = 0;
		Integer visitPK = 0;
		Integer adminSchedulePK = 0; 
		Integer studyPK = 0;
		Integer eventSOS = 0;
		int newEventLength = 0;

		String cost = "0";
		String displ = "0";
		String oldEventCoverageType = "";
		String[] reasonForCoverageChange = new String[1000];
		String [] eventSOSId = new String[1000];
		String[] strArrEventIds = new String[1000];
		String[] eventStatusPKs = new String[1000];
		String [] eventStartDates = new String[1000];
	    String [] eventNotes = new String[1000];//Bug Fix : 14879
	    String [] eventCoverageTypes = new String[1000];
	    String[] eventStatusValidFrom = new String[1000];

		try{
			
			GroupAuthModule groupAuthModule = new GroupAuthModule(callingUser, groupRightsAgent);
			Integer manageProtocolPriv = groupAuthModule.getAppManageProtocolsPrivileges();			
			if(!GroupAuthModule.hasViewPermission(manageProtocolPriv))
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to edit study data"));
				throw new AuthorizationException("User Not Authorized to edit Patient data");
			}
			
			if(scheduleID == null)
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Schedule PK or OID is needed to identify Study Admin Calendar"));
				throw new OperationException("Schedule PK or OID is needed to identify Study Admin Calendar");
			}
			if(scheduleID.getPK() != null && scheduleID.getPK() > 0)
			{
				adminSchedulePK = scheduleID.getPK(); 
			}else if(scheduleID.getOID() != null && scheduleID.getOID().length() > 0)
			{
				adminSchedulePK = objectMapService.getObjectPkFromOID(scheduleID.getOID());
			}else
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Schedule PK or OID is needed to identify Study Admin Calendar"));
				throw new OperationException("Schedule PK or OID is needed to identify Study Admin Calendar");
			}
			
			
			studyPK = PatientScheduleDAO.getStudyPKForAdminCal(adminSchedulePK);
			
			if(studyPK == null || studyPK == 0)
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Schedule is not valid Study Admin Schedule"));//Bug Fix : 15123
				throw new OperationException("Schedule is not valid Study Admin Schedule");
			}
		
			
			newEventLength = events.getEvent().size();
			String[][] event_disp = new String[newEventLength][5];
			
			//check Study Rights 
			TeamAuthModule teamAuthModule = new TeamAuthModule(callingUser.getUserId(), studyPK);
			Integer studySetUpRight = teamAuthModule.getStudySetupPrivileges();
			if(!TeamAuthModule.hasEditPermission(studySetUpRight))
			{
				addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User is not authorized to addUnscheduledEvent for this Study")); 
				throw new OperationException(); 
			}
			
			if(visitIdentifier != null && ((visitIdentifier.getOID() != null && visitIdentifier.getOID().length() > 0)
					|| (visitIdentifier.getPK() != null && visitIdentifier.getPK() >0))){

				if(visitIdentifier.getPK() != null && visitIdentifier.getPK() > 0)
				{
					visitPK = visitIdentifier.getPK();
					if(visitPK == null || visitPK == 0)
					{ 
						addIssue(new Issue(IssueTypes.VISIT_NOT_FOUND,				
								"Visit not found for Visit Identifier : OID " + visitIdentifier.getOID())); 
						throw new OperationException("Visit not found for Visit Identifier : OID " + visitIdentifier.getOID()); 
					}

				}else{

					visitPK = objectMapService.getObjectPkFromOID(visitIdentifier.getOID()); 
					if(visitPK == null || visitPK == 0)
					{ 
						addIssue(new Issue(IssueTypes.VISIT_NOT_FOUND,				
								"Visit not found for Visit Identifier : OID " + visitIdentifier.getOID())); 
						throw new OperationException("Visit not found for Visit Identifier : OID " + visitIdentifier.getOID()); 
					}
				}
				
				if(!PatientScheduleDAO.isVisitValidForAdminSchedule(visitPK, adminSchedulePK))
				{
					addIssue(new Issue(IssueTypes.VISIT_IDENTIFIER_INVALID, "Valid Visit Identifier is required to add the Unscheduled Event")); 
					throw new OperationException("Valid Visit Identifier is required to add the Unscheduled Event"); 
				}
			}
			else{
				addIssue(new Issue(IssueTypes.VISIT_IDENTIFIER_INVALID, "Valid Visit Identifier is required to add the Unscheduled Event")); 
				throw new OperationException("Valid Visit Identifier is required to add the Unscheduled Event"); 
			}

		//	protocolID = PatientScheduleDAO.getProtocolID(visitPK);

			int max_seq = eventAssocRObj.getMaxSeqForVisitEvents(visitPK,"null", "event_assoc" );


			cost = String.valueOf(eventAssocRObj.getMaxCost(EJBUtil.integerToString(adminSchedulePK)));

			int costNum = EJBUtil.stringToNum(cost);

			if (cost.equals("-1")) {

				costNum=0;

			}

			else{

				costNum++;

			}

			int counterForEvent_disp = -1;

			for(int i = 0; i < events.getEvent().size(); i++){

				counterForEvent_disp = counterForEvent_disp+1;

				cost=String.valueOf(costNum);


				if(events.getEvent().get(i).getEventIdentifier() != null && ((events.getEvent().get(i).getEventIdentifier().getOID() != null && 
						events.getEvent().get(i).getEventIdentifier().getOID().length() > 0) || (events.getEvent().get(i).getEventIdentifier().getPK() != null && 
								events.getEvent().get(i).getEventIdentifier().getPK() >0))){

					if(events.getEvent().get(i).getEventIdentifier().getPK() != null && events.getEvent().get(i).getEventIdentifier().getPK() > 0)
					{
						eventPK = events.getEvent().get(i).getEventIdentifier().getPK();
						if(eventPK == null || eventPK == 0)
						{ 
							addIssue(new Issue(IssueTypes.EVENT_NOT_FOUND,				
									"Event not found for Event Identifier : PK " + events.getEvent().get(i).getEventIdentifier().getPK())); 
							throw new OperationException("Event not found for Event Identifier : PK " + events.getEvent().get(i).getEventIdentifier().getPK()); 
						}
					}else{

						eventPK = objectMapService.getObjectPkFromOID(events.getEvent().get(i).getEventIdentifier().getOID()); 
						if(eventPK == null || eventPK == 0)
						{ 
							addIssue(new Issue(IssueTypes.EVENT_NOT_FOUND,				
									"Event not found for Event Identifier : OID " + events.getEvent().get(i).getEventIdentifier().getOID())); 
							throw new OperationException("Event not found for Event Identifier : OID " + events.getEvent().get(i).getEventIdentifier().getOID()); 
						}
					}
					
					if(!PatientScheduleDAO.ifEventExists(eventPK)){
						addIssue(new Issue(IssueTypes.EVENT_NOT_FOUND,				
								"Event not found for Event Identifier : PK " + events.getEvent().get(i).getEventIdentifier().getPK())); 
						throw new OperationException("Event not found for Event Identifier : PK " + events.getEvent().get(i).getEventIdentifier().getPK()); 
					}

					event_disp[counterForEvent_disp][0]=EJBUtil.integerToString(eventPK);
					event_disp[counterForEvent_disp][1]=displ;
					event_disp[counterForEvent_disp][2]=cost;
					event_disp[counterForEvent_disp][3]=EJBUtil.integerToString(visitPK);
					event_disp[counterForEvent_disp][4] = PatientScheduleDAO.getCategoryName(eventPK);

					costNum++;

				}
				else{
					addIssue(new Issue(IssueTypes.EVENT_IDENTIFIER_INAVLID, "Valid EventIdentifier is required to add the Unscheduled event")); 
					throw new OperationException("Valid EventIdentifier is required to add the Unscheduled Event"); 
				}
				
				if(events.getEvent().get(i).getEventAttributes() == null)
				{
					  addIssue(
			                    new Issue(IssueTypes.DATA_VALIDATION, 
			                            "EventAttributes are missing"));
			            throw new OperationException("Please enter a valid EventAttributes");
				}
				
				if(events.getEvent().get(i).getEventAttributes().getEventStatus().getEventStatusCode() == null || StringUtil.isEmpty(events.getEvent().get(i).getEventAttributes().getEventStatus().getEventStatusCode().getCode())){
		            if (logger.isDebugEnabled()) logger.debug("Event Status Code is required");
		            addIssue(
		                    new Issue(IssueTypes.DATA_VALIDATION, 
		                            "Please enter a valid Event Status Code"));
		            throw new AuthorizationException("Please enter a valid Event Status Code");
				}
				//Bug Fix : 15140
				try{
					eventStatusPKs[i] = EJBUtil.integerToString(dereferenceSchCode(events.getEvent().get(i).getEventAttributes().getEventStatus().getEventStatusCode(), CodeCache.CODE_TYPE_EVENT_STATUS_TYPE, callingUser));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					if (logger.isDebugEnabled()) logger.debug("Valid Event Status Code is required");
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Please enter a valid Event Status Code"));
					throw new OperationException("Please enter a valid Event Status Code");
				}
				
				if(events.getEvent().get(i).getEventAttributes().getEventStartDate() == null 
						|| StringUtil.isEmpty(events.getEvent().get(i).getEventAttributes().getEventStartDate().toString())){
		            if (logger.isDebugEnabled()) logger.debug("Event Start Date is required");
		            addIssue(
		                    new Issue(IssueTypes.DATA_VALIDATION, 
		                            "Please enter a valid Event Start Date"));
		            throw new OperationException("Please enter a valid Event Start Date");
				}
				
				eventStartDates[i] = DateUtil.dateToSqlDate(events.getEvent().get(i).getEventAttributes().getEventStartDate()).toString();
				String statusValidFrom = null;
				if(events.getEvent().get(i).getEventAttributes().getEventStatus().getStatusValidFrom() != null)
				{
						statusValidFrom = DateUtil.dateToString(events.getEvent().get(i).getEventAttributes().getEventStatus().getStatusValidFrom());
				}
				eventStatusValidFrom[i] = statusValidFrom; 
				eventNotes[i] = events.getEvent().get(i).getEventAttributes().getEventStatus().getNotes();

				if((events.getEvent().get(i).getEventAttributes().getEventStatus().getCoverageType() == null) || 
						(events.getEvent().get(i).getEventAttributes().getEventStatus().getSiteOfService() == null) || 
						(events.getEvent().get(i).getEventAttributes().getEventStatus().getNotes() ==  null)){
				    EventdefBean eventDefBean = new EventdefBean();
					eventDefBean = PatientScheduleDAO.getEventDef(eventPK);
					oldEventCoverageType = eventDefBean.getEventCoverageType();
					if(events.getEvent().get(i).getEventAttributes().getEventStatus().getCoverageType() == null){
						eventCoverageTypes[i] = eventDefBean.getEventCoverageType();
						if(events.getEvent().get(i).getEventAttributes().getEventStatus().getReasonForChangeCoverType() != null || 
								!StringUtil.isEmpty(events.getEvent().get(i).getEventAttributes().getEventStatus().getReasonForChangeCoverType())){
				             if (logger.isDebugEnabled()) logger.debug("Cannot enter new Reason For Coverage Change without changing the Coverage Type");
				             addIssue(
				                     new Issue(IssueTypes.DATA_VALIDATION, 
				                             "Cannot enter new Reason For Coverage Change without changing the Coverage Type"));
				             throw new AuthorizationException("Cannot enter new Reason For Coverage Change without changing the Coverage Type");
						}
						reasonForCoverageChange[i] = eventDefBean.getReasonForCoverageChange();
					}
					
					if(events.getEvent().get(i).getEventAttributes().getEventStatus().getSiteOfService() == null){
						eventSOSId[i] = eventDefBean.getEventSOSId();
					}
					if(events.getEvent().get(i).getEventAttributes().getEventStatus().getNotes() == null){
						eventNotes[i] = eventDefBean.getNotes();
					}
				}
				
				if((events.getEvent().get(i).getEventAttributes().getEventStatus().getCoverageType() != null) 
						&& !StringUtil.isEmpty(events.getEvent().get(i).getEventAttributes().getEventStatus().getCoverageType().getCode())){
					try{
						eventCoverageTypes[i] = EJBUtil.integerToString(dereferenceSchCode(events.getEvent().get(i).getEventAttributes().getEventStatus().getCoverageType(), CodeCache.CODE_TYPE_COVERAGE_TYPE, callingUser));
					} catch (Exception e) {
						// TODO Auto-generated catch block
						if (logger.isDebugEnabled()) logger.debug("Valid Coverage Type is required");
						addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Please enter a valid Coverage Type"));
						throw new OperationException("Please enter a valid Coverage Type");
					}
					if((events.getEvent().get(i).getEventAttributes().getEventStatus().getReasonForChangeCoverType() == null || 
							StringUtil.isEmpty(events.getEvent().get(i).getEventAttributes().getEventStatus().getReasonForChangeCoverType())) 
							&& (eventCoverageTypes[i].compareTo(oldEventCoverageType) != 0)){
			             if (logger.isDebugEnabled()) logger.debug("Reason For Coverage Change is Required");
			             addIssue(
			                     new Issue(IssueTypes.DATA_VALIDATION, 
			                             "Please enter the Reason For The Coverage Change"));
			             throw new AuthorizationException("Please enter the Reason For The Coverage Change");
					}
					reasonForCoverageChange[i] = events.getEvent().get(i).getEventAttributes().getEventStatus().getReasonForChangeCoverType();
				}
				
				if((events.getEvent().get(i).getEventAttributes().getEventStatus().getSiteOfService() !=null) 
						&& (!StringUtil.isEmpty(events.getEvent().get(i).getEventAttributes().getEventStatus().getSiteOfService().getSiteName())
						|| !StringUtil.isEmpty(events.getEvent().get(i).getEventAttributes().getEventStatus().getSiteOfService().getOID())
						|| !StringUtil.isEmpty(events.getEvent().get(i).getEventAttributes().getEventStatus().getSiteOfService().getSiteAltId()))){	
					try {
						eventSOS = ObjectLocator.sitePKFromIdentifier(callingUser, events.getEvent().get(i).getEventAttributes().getEventStatus().getSiteOfService(), sessionContext, objectMapService);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						if (logger.isDebugEnabled()) logger.debug("Event Status Code is required");
						addIssue(new Issue(IssueTypes.INVALID_SITE_OF_SERVICE, "Invalid site of service"));
						throw new OperationException("Invalid site of service");
					}

					if (eventSOS == null || eventSOS == 0){
						addIssue(new Issue(IssueTypes.INVALID_SITE_OF_SERVICE, "Invalid site of service"));
						throw new OperationException("Invalid site of service");
					}
					eventSOSId[i] = EJBUtil.integerToString(eventSOS);
				}
				
				if(events.getEvent().get(i).getEventAttributes().getEventStatus().getStatusValidFrom() == null 
						|| StringUtil.isEmpty(events.getEvent().get(i).getEventAttributes().getEventStatus().getStatusValidFrom().toString())){
		            if (logger.isDebugEnabled()) logger.debug("Status Valid From field is required");
		            addIssue(
		                    new Issue(IssueTypes.DATA_VALIDATION, 
		                            "Please enter the value for the Status Valid From field"));
		            throw new AuthorizationException("Please enter the value for the Status Valid From field");
				}

			}

			tx.begin();
			strArrEventIds = eventAssocRObj.updateUnscheduledEvents(EJBUtil.integerToString(adminSchedulePK),event_disp,0,0, max_seq, EJBUtil.integerToString(callingUser.getUserId()), AbstractService.IP_ADDRESS_FIELD_VALUE);
			tx.commit();

			String[] strArrSchEventIds = PatientScheduleDAO.updateAdminUnscheduledEvents(adminSchedulePK,callingUser.getUserId(), AbstractService.IP_ADDRESS_FIELD_VALUE, 
					                                                                     eventStatusPKs, eventStartDates, eventNotes, eventCoverageTypes, 
					                                                                     eventSOSId, reasonForCoverageChange, strArrEventIds, eventStatusValidFrom);
			for(String x:strArrSchEventIds)
			{
				ScheduleEventIdentifier identifier = new ScheduleEventIdentifier(); 
				identifier.setPK(Integer.parseInt(x));
				identifier.setOID(objectMapService.getOrCreateObjectMapFromPK("sch_events1", Integer.parseInt(x)).getOID());
				response.addObjectCreatedAction(identifier);


			}


 

		}	catch(OperationException e){
			//	sessionContext.setRollbackOnly();
			try {
				tx.rollback();
			} catch (IllegalStateException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (SecurityException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (SystemException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if (logger.isDebugEnabled()) logger.debug("PatientScheduleServiceImpl create", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			try {
				tx.rollback();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("PatientScheduleServiceImpl create", t);
			throw new OperationException(t);
		}


		return response;
	}
	
	
	@AroundInvoke
	public Object myInterceptor(InvocationContext ctx) throws Exception {
		response = new ResponseHolder();
		callingUser = 
			getLoggedInUser(
					sessionContext,
					userAgent);
		return ctx.proceed();
	}

}
