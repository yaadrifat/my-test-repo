/**
 * Created On Jan 16, 2013
 */
package com.velos.services.patientschedule;

import javax.ejb.Remote;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.CalendarIdentifier;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.MultipleEvents;
import com.velos.services.model.PatientProtocolIdentifier;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.VisitIdentifier;

/**
 * @author Kanwaldeep
 *
 */

@Remote
public interface PatientScheduleServiceBMT {
	
	public ResponseHolder addUnscheduledEvent(PatientProtocolIdentifier scheduleID, StudyIdentifier studyIdentifier, PatientIdentifier patientIdentifier, MultipleEvents eventIdentifiers, VisitIdentifier visitIdentifier) throws OperationException;
	public ResponseHolder addUnscheduledEvent(CalendarIdentifier scheduleID, MultipleEvents events, VisitIdentifier visitIdentifier) throws OperationException;

}
