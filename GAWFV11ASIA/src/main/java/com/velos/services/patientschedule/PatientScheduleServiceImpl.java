package com.velos.services.patientschedule;

import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.StudySiteDao;
import com.velos.eres.business.patProt.impl.PatProtBean;
import com.velos.eres.business.study.impl.StudyBean;
import com.velos.eres.service.accountAgent.AccountAgentRObj;
import com.velos.eres.service.grpRightsAgent.GrpRightsAgentRObj;
import com.velos.eres.service.patFacilityAgent.PatFacilityAgentRObj;
import com.velos.eres.service.patProtAgent.PatProtAgentRObj;
import com.velos.eres.service.personAgent.PersonAgentRObj;
import com.velos.eres.service.studyAgent.StudyAgent;
import com.velos.eres.service.studySiteAgent.StudySiteAgentRObj;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.userSiteAgent.UserSiteAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.patProt.PatProtJB;
import com.velos.esch.audit.web.AuditRowEschJB;
import com.velos.esch.business.common.EventdefDao;
import com.velos.esch.business.eventdef.impl.EventdefBean;
import com.velos.esch.service.util.EJBUtil;
import com.velos.esch.web.eventdef.EventdefJB;
import com.velos.esch.web.eventresource.EventResourceJB;
import com.velos.esch.web.eventstat.EventStatJB;
import com.velos.services.AbstractService;
import com.velos.services.AuthorizationException;
import com.velos.services.CRUDAction;
import com.velos.services.CompletedAction;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.authorization.GroupAuthModule;
import com.velos.services.authorization.TeamAuthModule;
import com.velos.services.map.MultipleObjectsFoundException;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.CalendarIdentifier;
import com.velos.services.model.EventIdentifier;
import com.velos.services.model.EventStatus;
import com.velos.services.model.EventStatusHistory;
import com.velos.services.model.EventStatusIdentifier;
import com.velos.services.model.MEventStatus;
import com.velos.services.model.MEventStatuses;
import com.velos.services.model.MPatientSchedule;
import com.velos.services.model.MPatientScheduleList;
import com.velos.services.model.MPatientSchedules;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientProtocolIdentifier;
import com.velos.services.model.PatientSchedule;
import com.velos.services.model.PatientSchedules;
import com.velos.services.model.ScheduleEventIdentifier;
import com.velos.services.model.ScheduleEventStatuses;
import com.velos.services.model.SitesOfService;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.VisitIdentifier;
import com.velos.services.studypatient.StudyPatientDAO;
import com.velos.services.util.CodeCache;
import com.velos.services.util.ObjectLocator;
/**
 * @author Tarandeep Singh Bali
 *
 */
@Stateless
@Remote(PatientScheduleService.class)
public class PatientScheduleServiceImpl 
extends AbstractService 
implements PatientScheduleService{

@EJB
private ObjectMapService objectMapService;
@EJB
GrpRightsAgentRObj groupRightsAgent;
@EJB
private UserAgentRObj userAgent;
@EJB
private StudySiteAgentRObj studySiteAgent;
@EJB
private UserSiteAgentRObj userSiteAgent;
@EJB
private PatFacilityAgentRObj patFacilityAgent;
@EJB
private PersonAgentRObj personAgentBean;
@EJB
private AccountAgentRObj accountAgent;
@EJB
private PatProtAgentRObj patProtAgent;
@EJB
private StudyAgent studyAgent;

@Resource 
private SessionContext sessionContext;
	
	private static Logger logger = Logger.getLogger(PatientScheduleServiceImpl.class.getName());
	/**
	 * 
	 */
				
	public PatientSchedules getPatientScheduleList(PatientIdentifier patientId, StudyIdentifier studyIdentifier)
			throws OperationException {
		
		PatientSchedules patientSchedules = new PatientSchedules();
		
		try{
			GroupAuthModule groupAuth = 
					new GroupAuthModule(callingUser, groupRightsAgent);
				Integer manageProtocolPriv = 
					groupAuth.getAppManagePatientsPrivileges();
				
				boolean hasViewManageProt = 
					GroupAuthModule.hasViewPermission(manageProtocolPriv);
				
				if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
				if (!hasViewManageProt){
					addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to view Patient data"));
					throw new AuthorizationException("User Not Authorized to view Patient data");
				}
				
			if(studyIdentifier == null)//Bug Fix : 16211
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid StudyIdentifier is required")); 
				throw new OperationException(); 
			}
			Integer studyPK =
				ObjectLocator.studyPKFromIdentifier(callingUser, studyIdentifier,objectMapService);
				
			if (studyPK == null || studyPK==0){
				addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Patient study not found"));
				throw new OperationException("Patient study not found");
			}
			
			if(patientId == null)
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid PatientIdentifier is required")); 
				throw new OperationException(); 
			}
			if((patientId.getOID() == null || patientId.getOID().length() == 0) 
					&& ((patientId.getPatientId() == null || patientId.getPatientId().length() == 0)
							|| (patientId.getOrganizationId() == null) ) && patientId.getPK() == null )
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid PatientIdentifier with OID or PatientID and OrganizationIdentifier is required"));
				throw new OperationException(); 
			}
			
			
			
			Integer personPK = ObjectLocator.personPKFromPatientIdentifier(
					callingUser, 
					patientId, 
					objectMapService);
			
			if (personPK == null || personPK == 0){
				addIssue(new Issue(IssueTypes.PATIENT_NOT_FOUND, "Study Patient not found"));
				throw new OperationException("Patient not found");
			}
			
			Integer studyID = ObjectLocator.studyPKFromIdentifier(callingUser, studyIdentifier, objectMapService);
			PatProtBean  patProtBean = patProtAgent.findCurrentPatProtDetails(studyID, personPK); 
			
			if(patProtBean.getPatProtId() == 0)
			{
				addIssue(new Issue(IssueTypes.PATIENT_NOT_ONSTUDY, "Given patient is not linked to study provided")); 
				throw new OperationException(); 
			}
			TeamAuthModule teamAuthModule = new TeamAuthModule(callingUser.getUserId(), studyPK);
			int studyTeamPrivileges = teamAuthModule.getStudyTeamPrivileges();
			if (!TeamAuthModule.hasViewPermission(studyTeamPrivileges)){
				addIssue(
						new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, 
								"User does not have view permission to view Study Team"));
				throw new AuthorizationException("User does not have view permission to view Study Team");
			}
			
			StudySiteDao studySiteDao = studySiteAgent.getSitesForStatAndEnrolledPat(studyPK, 
			        callingUser.getUserId(), 0);
			ArrayList siteIds = studySiteDao.getSiteIds();
			Integer count = 0;
			/*ArrayList<StudyPatient> listCompleteStudyPatient = new ArrayList<StudyPatient>();
			if (siteIds != null && siteIds.size() > 0) {
				for(int i = 0 ; i< siteIds.size(); i++){
					sitePK = (Integer)siteIds.get(i);
					listCompleteStudyPatient.addAll((ArrayList<StudyPatient>) StudyPatientDAO.
							getStudyPatientByStudyPK(studyPK, sitePK));
				}
			}*/
			count = StudyPatientDAO.getStudyPatientCountByStudyPK(studyPK, siteIds);
			if(count==0){
				addIssue(
						new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION,
								"User does not have view permission for the Patient data"));
				throw new AuthorizationException("User does not have the view permission for the Patient data");
			}
			
			Integer patFacilityRights = userSiteAgent.getUserPatientFacilityRight(callingUser.getUserId(), personPK);
			if (GroupAuthModule.hasViewPermission(patFacilityRights) == false) {
             if (logger.isDebugEnabled()) logger.debug("User not authorized to access data of this patient");
             addIssue(
                     new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION, 
                             "User not authorized to access data of this patient"));
             throw new AuthorizationException("User not authorized to access data of this patient");
			}
			
			int studyStatusPrivileges = teamAuthModule.getStudyStatusPrivileges();
			//Add Study Patient Access Validation:Tarandeep Singh Bali 
			int patientManagePrivileges = teamAuthModule.getPatientManagePrivileges();
			if ((!TeamAuthModule.hasViewPermission(studyStatusPrivileges) || (!TeamAuthModule.hasViewPermission(patientManagePrivileges)))){
				if (logger.isDebugEnabled()) logger.debug("user does not have view permission to view study Status");
			
				addIssue(
						new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, 
								"User does not have view permission to view Manage Patient details"));
				throw new AuthorizationException("User does not have view permission to view the Manage Patient details");
			}
			
			//PatientScheduleDAO call for PatientSchedule with primary info
		   patientSchedules = PatientScheduleDAO.getPatientScheduleList(personPK, studyPK);
			
			
			PatientScheduleDAO patScheduleDAO = new PatientScheduleDAO();
			return patientSchedules;
			
		}
		catch(OperationException e){
	//		sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", t);
			throw new OperationException(t);
		}

	}
	
	public PatientSchedule getPatientSchedule(PatientProtocolIdentifier scheduleIdentifier,VisitIdentifier visitIdentifier,String visitName)
		throws OperationException{
			try{
				int patProtPK = 0;
				int studyPK = 0;
				Integer visitPK = 0;
				Integer personPK = 0;
				PatientSchedule patientSchedule = new PatientSchedule();
				GroupAuthModule groupAuth = 
						new GroupAuthModule(callingUser, groupRightsAgent);
					Integer manageProtocolPriv = 
						groupAuth.getAppManagePatientsPrivileges();
					
					boolean hasViewManageProt = 
						GroupAuthModule.hasViewPermission(manageProtocolPriv);
					
					if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
					if (!hasViewManageProt){
						addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to view Patient data"));
						throw new AuthorizationException("User Not Authorized to view Patient data");
					}
					if(scheduleIdentifier != null && scheduleIdentifier.getPK() != null && scheduleIdentifier.getPK() > 0)
					{
						patProtPK = scheduleIdentifier.getPK();
						if(!PatientScheduleDAO.ifScheduleExists(patProtPK)){//Bug Fix : 16298
							addIssue(new Issue(IssueTypes.SCHEDULE_NOT_FOUND,				
									"Schedule not found for the provided ScheduleIdentifier")); 
							throw new OperationException("Schedule not found for the provided ScheduleIdentifier");
						}
					}else if(scheduleIdentifier != null && scheduleIdentifier.getOID() != null && scheduleIdentifier.getOID().length() > 0)
					{
						ObjectMap objectMap = objectMapService.getObjectMapFromId(scheduleIdentifier.getOID()); 
						
						if(objectMap == null || !(objectMap.getTableName().equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_PATIENT_SCHEDULE)))
						{
							addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Please send valid ScheduleIdentifier"));
							throw new OperationException();
						}
				        
						patProtPK = objectMap.getTablePK();
						if(!PatientScheduleDAO.ifScheduleExists(patProtPK)){//Bug Fix : 16298
							addIssue(new Issue(IssueTypes.SCHEDULE_NOT_FOUND,				
									"Schedule not found for the provided ScheduleIdentifier")); 
							throw new OperationException("Schedule not found for the provided ScheduleIdentifier");
						}
					}else
					{
						addIssue(new Issue(IssueTypes.PATIENT_SCHEDULE_IDENTIFIER_NOT_FOUND, "Valid ScheduleIdentifier is required to get Patient Schedule")); 
						throw new OperationException(); 
					}
						
					
					
					studyPK = PatientScheduleDAO.getStudyPKFromPatProt(patProtPK);
						TeamAuthModule teamAuthModule = new TeamAuthModule(callingUser.getUserId(), studyPK);
						int studyTeamPrivileges = teamAuthModule.getStudyTeamPrivileges();
						if (!TeamAuthModule.hasViewPermission(studyTeamPrivileges)){
							addIssue(
									new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, 
											"User does not have view permission to view Study Team"));
							throw new AuthorizationException("User does not have view permission to view Study Team");
						}
						//Bug Fix : 12463 && 12467
						int patientManageViewPrivileges = teamAuthModule.getPatientManagePrivileges();
						if (!TeamAuthModule.hasViewPermission(patientManageViewPrivileges)){
							addIssue(
									new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION,
											"User does not have view permission to view the Patient data"));
							throw new AuthorizationException("User does not have the view permission to view the Patient data");
						}
            			StudySiteDao studySiteDao = studySiteAgent.getSitesForStatAndEnrolledPat(studyPK, 
            			        callingUser.getUserId(), 0);
            			ArrayList siteIds = studySiteDao.getSiteIds();
            			Integer count = 0;
            			/*ArrayList<StudyPatient> listCompleteStudyPatient = new ArrayList<StudyPatient>();
            			if (siteIds != null && siteIds.size() > 0) {
            				for(int i = 0 ; i< siteIds.size(); i++){
            					sitePK = (Integer)siteIds.get(i);
            					listCompleteStudyPatient.addAll((ArrayList<StudyPatient>) StudyPatientDAO.
            							getStudyPatientByStudyPK(studyPK, sitePK));
            				}
            			}*/
            			count = StudyPatientDAO.getStudyPatientCountByStudyPK(studyPK, siteIds);
            			if(count==0){
							addIssue(
									new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION,
											"User does not have view permission for the Patient data"));
							throw new AuthorizationException("User does not have the view permission for the Patient data");
            			}
						
			           personPK = PatientScheduleDAO.getPersonPK(patProtPK);
			           
						if (personPK == null || personPK == 0){
							addIssue(new Issue(IssueTypes.PATIENT_NOT_FOUND, "Study Patient not found"));
							throw new OperationException("Patient not found");
						}
						
						PatProtBean  patProtBean = patProtAgent.findCurrentPatProtDetails(studyPK, personPK); 
						
						if(patProtBean.getPatProtId() == 0)
						{
							addIssue(new Issue(IssueTypes.PATIENT_NOT_ONSTUDY, "Given patient is not linked to study provided")); 
							throw new OperationException(); 
						}
						patProtPK = patProtBean.getPatProtId();
						Integer patFacilityRights = userSiteAgent.getUserPatientFacilityRight(callingUser.getUserId(), personPK);
						if (GroupAuthModule.hasViewPermission(patFacilityRights) == false) {
			             if (logger.isDebugEnabled()) logger.debug("User not authorized to access data of this patient");
			             addIssue(
			                     new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION, 
			                             "User not authorized to access data of this patient"));
			             throw new AuthorizationException("User not authorized to access data of this patient");
						}
						if(visitIdentifier != null && ((visitIdentifier.getOID() != null && visitIdentifier.getOID().length() > 0)
								|| (visitIdentifier.getPK() != null && visitIdentifier.getPK() >0))){

							if(visitIdentifier.getPK() != null && visitIdentifier.getPK() > 0)
							{
								visitPK = visitIdentifier.getPK();
								if(visitPK == null || visitPK == 0 || !PatientScheduleDAO.ifVisitExists(visitPK))
								{ 
									addIssue(new Issue(IssueTypes.VISIT_NOT_FOUND,				
											"Visit not found for Visit Identifier : PK " + visitPK)); 
									throw new OperationException("Visit not found for Visit Identifier : PK " + visitPK); 
								}

							}else{

								visitPK = objectMapService.getObjectPkFromOID(visitIdentifier.getOID());
								//BUg Fix : 15013
								if(visitPK == null || visitPK == 0 || !PatientScheduleDAO.ifVisitExists(visitPK))
								{ 
									addIssue(new Issue(IssueTypes.VISIT_NOT_FOUND,				
											"Visit not found for Visit Identifier : OID " + visitIdentifier.getOID())); 
									throw new OperationException("Visit not found for Visit Identifier : OID " + visitIdentifier.getOID()); 
								}				
							}
						}
					    patientSchedule = PatientScheduleDAO.getPatientSchedule(patProtPK,visitPK,visitName);
				
				return patientSchedule;
				
				
				
			}		catch(OperationException e){
		//		sessionContext.setRollbackOnly();
				if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", e);
				e.setIssues(response.getIssues());
				throw e;
			}
			catch(Throwable t){
				this.addUnknownThrowableIssue(t);
				if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", t);
				throw new OperationException(t);
			}
		}
	
	public PatientSchedule getPatientScheduleVisits(PatientProtocolIdentifier scheduleIdentifier)
	throws OperationException{
		try{
			int patProtPK = 0;
			int studyPK = 0;
			Integer personPK = 0;
			PatientSchedule patientSchedule = new PatientSchedule();
			GroupAuthModule groupAuth = 
					new GroupAuthModule(callingUser, groupRightsAgent);
				Integer manageProtocolPriv = 
					groupAuth.getAppManagePatientsPrivileges();
				
				boolean hasViewManageProt = 
					GroupAuthModule.hasViewPermission(manageProtocolPriv);
				
				if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
				if (!hasViewManageProt){
					addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to view Patient data"));
					throw new AuthorizationException("User Not Authorized to view Patient data");
				}
				if(scheduleIdentifier != null && scheduleIdentifier.getPK() != null && scheduleIdentifier.getPK() > 0)
				{
					patProtPK = scheduleIdentifier.getPK();
					if(!PatientScheduleDAO.ifScheduleExists(patProtPK)){//Bug Fix : 16298
						addIssue(new Issue(IssueTypes.SCHEDULE_NOT_FOUND,				
								"Schedule not found for the provided ScheduleIdentifier")); 
						throw new OperationException("Schedule not found for the provided ScheduleIdentifier");
					}
				}else if(scheduleIdentifier != null && scheduleIdentifier.getOID() != null && scheduleIdentifier.getOID().length() > 0)
				{
					ObjectMap objectMap = objectMapService.getObjectMapFromId(scheduleIdentifier.getOID()); 
					
					if(objectMap == null || !(objectMap.getTableName().equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_PATIENT_SCHEDULE)))
					{
						addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Please send valid ScheduleIdentifier"));
						throw new OperationException();
					}
			        
					patProtPK = objectMap.getTablePK();
					if(!PatientScheduleDAO.ifScheduleExists(patProtPK)){//Bug Fix : 16298
						addIssue(new Issue(IssueTypes.SCHEDULE_NOT_FOUND,				
								"Schedule not found for the provided ScheduleIdentifier")); 
						throw new OperationException("Schedule not found for the provided ScheduleIdentifier");
					}
				}else
				{
					addIssue(new Issue(IssueTypes.PATIENT_SCHEDULE_IDENTIFIER_NOT_FOUND, "Valid ScheduleIdentifier is required to get Patient Schedule")); 
					throw new OperationException(); 
				}
					
				
				
				studyPK = PatientScheduleDAO.getStudyPKFromPatProt(patProtPK);
					TeamAuthModule teamAuthModule = new TeamAuthModule(callingUser.getUserId(), studyPK);
					int studyTeamPrivileges = teamAuthModule.getStudyTeamPrivileges();
					if (!TeamAuthModule.hasViewPermission(studyTeamPrivileges)){
						addIssue(
								new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, 
										"User does not have view permission to view Study Team"));
						throw new AuthorizationException("User does not have view permission to view Study Team");
					}
					//Bug Fix : 12463 && 12467
					int patientManageViewPrivileges = teamAuthModule.getPatientManagePrivileges();
					if (!TeamAuthModule.hasViewPermission(patientManageViewPrivileges)){
						addIssue(
								new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION,
										"User does not have view permission to view the Patient data"));
						throw new AuthorizationException("User does not have the view permission to view the Patient data");
					}
        			StudySiteDao studySiteDao = studySiteAgent.getSitesForStatAndEnrolledPat(studyPK, 
        			        callingUser.getUserId(), 0);
        			ArrayList siteIds = studySiteDao.getSiteIds();
        			Integer count = 0;
        			/*ArrayList<StudyPatient> listCompleteStudyPatient = new ArrayList<StudyPatient>();
        			if (siteIds != null && siteIds.size() > 0) {
        				for(int i = 0 ; i< siteIds.size(); i++){
        					sitePK = (Integer)siteIds.get(i);
        					listCompleteStudyPatient.addAll((ArrayList<StudyPatient>) StudyPatientDAO.
        							getStudyPatientByStudyPK(studyPK, sitePK));
        				}
        			}*/
        			count = StudyPatientDAO.getStudyPatientCountByStudyPK(studyPK, siteIds);
        			if(count==0){
						addIssue(
								new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION,
										"User does not have view permission for the Patient data"));
						throw new AuthorizationException("User does not have the view permission for the Patient data");
        			}
					
		           personPK = PatientScheduleDAO.getPersonPK(patProtPK);
		           
					if (personPK == null || personPK == 0){
						addIssue(new Issue(IssueTypes.PATIENT_NOT_FOUND, "Study Patient not found"));
						throw new OperationException("Patient not found");
					}
					
					PatProtBean  patProtBean = patProtAgent.findCurrentPatProtDetails(studyPK, personPK); 
					
					if(patProtBean.getPatProtId() == 0)
					{
						addIssue(new Issue(IssueTypes.PATIENT_NOT_ONSTUDY, "Given patient is not linked to study provided")); 
						throw new OperationException(); 
					}
					patProtPK = patProtBean.getPatProtId();
					Integer patFacilityRights = userSiteAgent.getUserPatientFacilityRight(callingUser.getUserId(), personPK);
					if (GroupAuthModule.hasViewPermission(patFacilityRights) == false) {
		             if (logger.isDebugEnabled()) logger.debug("User not authorized to access data of this patient");
		             addIssue(
		                     new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION, 
		                             "User not authorized to access data of this patient"));
		             throw new AuthorizationException("User not authorized to access data of this patient");
					}
				    patientSchedule = PatientScheduleDAO.getPatientScheduleVisits(patProtPK);
			
			return patientSchedule;
			
			
			
		}		catch(OperationException e){
	//		sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", t);
			throw new OperationException(t);
		}
	}
	// Navneet
	public PatientSchedule getCurrentPatientSchedule(PatientIdentifier patientIdentifier, StudyIdentifier studyIdentifier,Date startDate,Date endDate)
			throws OperationException{
				try{
					
				Map<String, Object> parameters = new HashMap<String, Object>();
				parameters.put("response", this.response);
				
					Integer studyPK = 0;
					Integer personPK = 0;
					PatientSchedule currentPatientSchedule = new PatientSchedule();
					
					GroupAuthModule groupAuth = 
							new GroupAuthModule(callingUser, groupRightsAgent);
						Integer manageProtocolPriv = 
							groupAuth.getAppManagePatientsPrivileges();
						
						boolean hasViewManageProt = 
							GroupAuthModule.hasViewPermission(manageProtocolPriv);
						
						if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
						if (!hasViewManageProt){
							addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to view Patient data"));
							throw new AuthorizationException("User Not Authorized to view Patient data");
						}
						
						if( studyIdentifier == null || (studyIdentifier.getPK() == null && (StringUtil.isEmpty(studyIdentifier.getOID()))
								&& (StringUtil.isEmpty(studyIdentifier.getStudyNumber()))))
							{
								addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid StudyIdentifier is required to get Patient Schedule"));
								throw new OperationException();
							}
						
						if(patientIdentifier == null || (patientIdentifier.getPK() == null &&(StringUtil.isEmpty(patientIdentifier.getOID()))
								&& (StringUtil.isEmpty(patientIdentifier.getPatientId()))
								&&(patientIdentifier.getOrganizationId()== null || (StringUtil.isEmpty(patientIdentifier.getOrganizationId().getOID()) && StringUtil.isEmpty(patientIdentifier.getOrganizationId().getSiteName())))))
							{
								addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid PatientIdentifier with OID or PatientID and OrganizationIdentifier is required"));
								throw new OperationException();
							}
						   
//						    if((patientIdentifier.getOID() == null || patientIdentifier.getOID().length() == 0) 
//								&& ((patientIdentifier.getPatientId() == null || patientIdentifier.getPatientId().length() == 0)
//										|| (patientIdentifier.getOrganizationId() == null) ) )
//						   {
//							  addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid PatientIdentifier with OID or PatientID and OrganizationIdentifier is required"));
//							  throw new OperationException(); 
//						   }

						    
							studyPK =
									ObjectLocator.studyPKFromIdentifier(callingUser, studyIdentifier,objectMapService);
									
								if (studyPK == null || studyPK==0 || !PatientScheduleDAO.ifStudyExists(studyPK)){ //Bug Fix : 16303
									addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Patient study not found"));
									throw new OperationException("Patient study not found");
								}
							 personPK = ObjectLocator.personPKFromPatientIdentifier(
									callingUser, 
									patientIdentifier, 
									objectMapService);

							TeamAuthModule teamAuthModule = new TeamAuthModule(callingUser.getUserId(), studyPK);
							int studyTeamPrivileges = teamAuthModule.getStudyTeamPrivileges();
							if (!TeamAuthModule.hasViewPermission(studyTeamPrivileges)){
								addIssue(
										new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, 
												"User does not have view permission to view Study Team"));
								throw new AuthorizationException("User does not have view permission to view Study Team");
							}//Bug Fix : 12439
							
							int patientManageViewPrivileges = teamAuthModule.getPatientManagePrivileges();
							if (!TeamAuthModule.hasViewPermission(patientManageViewPrivileges)){
								addIssue(
										new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION,
												"User does not have view permission for the Patient data"));
								throw new AuthorizationException("User does not have the view permission for the Patient data");
							}//Bug Fix : 12440
							
							int patientViewDataPrivileges = teamAuthModule.getPatientViewDataPrivileges();
							if (!TeamAuthModule.hasViewPermission(patientViewDataPrivileges)){
								addIssue(
										new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION,
												"User does not have view permission for the Patient data"));
								throw new AuthorizationException("User does not have the view permission for the Patient data");
							}
                            //Bug Fix : 12796
                			StudySiteDao studySiteDao = studySiteAgent.getSitesForStatAndEnrolledPat(studyPK, 
                			        callingUser.getUserId(), 0);
                			ArrayList siteIds = studySiteDao.getSiteIds();
                			Integer count = 0;
                			/*ArrayList<StudyPatient> listCompleteStudyPatient = new ArrayList<StudyPatient>();
                			if (siteIds != null && siteIds.size() > 0) {
                				for(int i = 0 ; i< siteIds.size(); i++){
                					sitePK = (Integer)siteIds.get(i);
                					listCompleteStudyPatient.addAll((ArrayList<StudyPatient>) StudyPatientDAO.
                							getStudyPatientByStudyPK(studyPK, sitePK));
                				}
                			}*/
                			count = StudyPatientDAO.getStudyPatientCountByStudyPK(studyPK, siteIds);
                			if(count==0){
								addIssue(
										new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION,
												"User does not have view permission for the Patient data"));
								throw new AuthorizationException("User does not have the view permission for the Patient data");
                			}
							
							Integer patFacilityRights = userSiteAgent.getUserPatientFacilityRight(callingUser.getUserId(), personPK);
							if (GroupAuthModule.hasViewPermission(patFacilityRights) == false) {
				             if (logger.isDebugEnabled()) logger.debug("User not authorized to access data of this patient");
				             addIssue(
				                     new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION, 
				                             "User not authorized to access data of this patient"));
				             throw new AuthorizationException("User not authorized to access data of this patient");
							}
									           
							if (personPK == null || personPK == 0){
								addIssue(new Issue(IssueTypes.PATIENT_NOT_FOUND, "Study Patient not found"));
								throw new OperationException("Patient not found");
							}
							
							PatProtBean  patProtBean = patProtAgent.findCurrentPatProtDetails(studyPK, personPK); 
							
							if(patProtBean.getPatProtId() == 0)
							{
								addIssue(new Issue(IssueTypes.PATIENT_NOT_ONSTUDY, "Given patient is not linked to study provided")); 
								throw new OperationException(); 
							}
							
							String parseStartdate = DateUtil.dateToString(startDate);
							String parseEndDate = DateUtil.dateToString(endDate);
													
						    currentPatientSchedule = PatientScheduleDAO.getCurrentPatientSchedule(personPK, studyPK,parseStartdate, parseEndDate,parameters);
						    currentPatientSchedule.setEndDate(endDate);
						    currentPatientSchedule.setStartDate(startDate);
						    return currentPatientSchedule;
					
					
				}		catch(OperationException e){
			//		sessionContext.setRollbackOnly();
					if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", e);
					e.setIssues(response.getIssues());
					throw e;
				}
				catch(Throwable t){
					this.addUnknownThrowableIssue(t);
					if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", t);
					throw new OperationException(t);
				}
			}
		
	
	public ResponseHolder addScheduleEventStatus(EventIdentifier eventIdentifier, EventStatus eventStatus) throws OperationException{
		EventStatJB eventStatB = new EventStatJB();
		EventResourceJB eventResB = new EventResourceJB();
        EventdefJB eventdefB = new EventdefJB();
		EventStatusIdentifier eventStatusIdentifier = new EventStatusIdentifier();
		Integer eventPK = 0;
		Integer patProtPK = 0;
		Integer studyPK = 0;
		Integer personPK = 0;
		int execBy = 0;
		Integer eventSOSId = 0;
		Integer statusCodelstPK = 0;
		int eventStatusPK = 0;
		int oldStatus = 0;
		int eventCoverageType = 0;
		int oldEventCoverageType = 0;
		String notes = "";
		String reasonForCoverageChange = "";
		String remarks = "";
		String mode = "N";
		Date execOn;
		
		try{
		
		GroupAuthModule groupAuth = 
				new GroupAuthModule(callingUser, groupRightsAgent);
			Integer manageProtocolPriv = 
				groupAuth.getAppManagePatientsPrivileges();
			
			boolean hasViewManageProt = 
				GroupAuthModule.hasViewPermission(manageProtocolPriv);
			
			if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
			if (!hasViewManageProt){
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to view Patient data"));
				throw new AuthorizationException("User Not Authorized to view Patient data");
			}
		
			if(eventIdentifier != null && ((eventIdentifier.getOID() != null && eventIdentifier.getOID().length() > 0)
					|| (eventIdentifier.getPK() != null && eventIdentifier.getPK() >0))){
				
				if(eventIdentifier.getPK() != null && eventIdentifier.getPK() > 0)
				{
					eventPK = eventIdentifier.getPK();
					if(eventPK == null || eventPK == 0)
					{ 
						addIssue(new Issue(IssueTypes.EVENT_NOT_FOUND,				
							"Event not found for Event Identifier : PK " + eventIdentifier.getPK())); 
						throw new OperationException("Event not found for Event Identifier : PK " + eventIdentifier.getPK()); 
					}
					
				}else{
					
					eventPK = objectMapService.getObjectPkFromOID(eventIdentifier.getOID()); 
					if(eventPK == null || eventPK == 0)
					{ 
						addIssue(new Issue(IssueTypes.EVENT_NOT_FOUND,				
							"Event not found for Event Identifier : OID " + eventIdentifier.getOID())); 
						throw new OperationException("Event not found for Event Identifier : OID " + eventIdentifier.getOID()); 
					}
				}
			}
			else{
				addIssue(new Issue(IssueTypes.EVENT_IDENTIFIER_INAVLID, "Valid Event Identifier is required to add the Unscheduled Event")); 
				throw new OperationException("Valid Event Identifier is required to add the Unscheduled Event"); 
			}
		
		patProtPK = PatientScheduleDAO.getPatProtPK(eventPK);
		
		//Bug Fix : 12477
		if(patProtPK == null || patProtPK == 0)
		{ 
			addIssue(new Issue(IssueTypes.PATIENT_SCHEDULE_DOES_NOT_EXIST,				
				"Patient Schedule Does Not Exist")); 
			throw new OperationException("Patient Schedule Does Not Exist "); 
		}
		
		studyPK = PatientScheduleDAO.getStudyPKFromPatProt(patProtPK);
		personPK = PatientScheduleDAO.getPersonPK(patProtPK);//Bug Fix : 13053
		TeamAuthModule teamAuthModule = new TeamAuthModule(callingUser.getUserId(), studyPK);
		int studyTeamPrivileges = teamAuthModule.getStudyTeamPrivileges();
		if (!TeamAuthModule.hasViewPermission(studyTeamPrivileges)){
			addIssue(
					new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, 
							"User does not have view permission to view Study Team"));
			throw new AuthorizationException("User does not have view permission to view Study Team");
		}
		
		int patientManageViewPrivileges = teamAuthModule.getPatientManagePrivileges();
		if (!TeamAuthModule.hasViewPermission(patientManageViewPrivileges)){
			addIssue(
					new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION,
							"User does not have view permission to edit the Patient data"));
			throw new AuthorizationException("User does not have the view permission to edit the Patient data");
		}
		
		int patientManageEditPrivileges = teamAuthModule.getPatientManagePrivileges();
		if (!TeamAuthModule.hasEditPermission(patientManageEditPrivileges)){
			addIssue(
					new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION,
							"User does not have edit permission to edit the Patient data"));
			throw new AuthorizationException("User does not have the edit permission to edit the Patient data");
		}
		
		StudySiteDao studySiteDao = studySiteAgent.getSitesForStatAndEnrolledPat(studyPK, 
		        callingUser.getUserId(), 0);
		ArrayList siteIds = studySiteDao.getSiteIds();
		Integer count = 0;
		/*ArrayList<StudyPatient> listCompleteStudyPatient = new ArrayList<StudyPatient>();
		if (siteIds != null && siteIds.size() > 0) {
			for(int i = 0 ; i< siteIds.size(); i++){
				sitePK = (Integer)siteIds.get(i);
				listCompleteStudyPatient.addAll((ArrayList<StudyPatient>) StudyPatientDAO.
						getStudyPatientByStudyPK(studyPK, sitePK));
			}
		}*/
		count = StudyPatientDAO.getStudyPatientCountByStudyPK(studyPK, siteIds);
		if(count==0){
			addIssue(
					new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION,
							"User does not have view permission for the Patient data"));
			throw new AuthorizationException("User does not have the view permission for the Patient data");
		}
		
		Integer patFacilityRights = userSiteAgent.getUserPatientFacilityRight(callingUser.getUserId(), personPK);
		if (GroupAuthModule.hasViewPermission(patFacilityRights) == false) {
         if (logger.isDebugEnabled()) logger.debug("User not authorized to access data of this patient");
         addIssue(
                 new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION, 
                         "User not authorized to access data of this patient"));
         throw new AuthorizationException("User not authorized to access data of this patient");
		}
        
        
			if (personPK == null || personPK == 0){
				addIssue(new Issue(IssueTypes.PATIENT_NOT_FOUND, "Study Patient not found"));
				throw new OperationException("Patient not found");
			}
			
			PatProtBean  patProtBean = patProtAgent.findCurrentPatProtDetails(studyPK, personPK); 
			
			if(patProtBean.getPatProtId() == 0)
			{
				addIssue(new Issue(IssueTypes.PATIENT_NOT_ONSTUDY, "Given patient is not linked to study provided")); 
				throw new OperationException(); 
			}
		//Bug Fix : 12416
		if(eventStatus.getStatusValidFrom() == null || StringUtil.isEmpty(eventStatus.getStatusValidFrom().toString())){
            if (logger.isDebugEnabled()) logger.debug("Status Valid From field is required");
            addIssue(
                    new Issue(IssueTypes.DATA_VALIDATION, 
                            "Please enter the value for the Status Valid From field"));
            throw new AuthorizationException("Please enter the value for the Status Valid From field");
		}
		//Bug Fix : 12380 && 16319
		if(PatientScheduleDAO.ifFDAStudy(studyPK)){
			if(eventStatus.getReasonForChange() == null 
					|| StringUtil.isEmpty(eventStatus.getReasonForChange())){
	            if (logger.isDebugEnabled()) logger.debug("Reason For Change FDA Audit field is required");
	            addIssue(
	                    new Issue(IssueTypes.DATA_VALIDATION, 
	                            "Please enter the value for the Reason for Change FDA Audit field"));
	            throw new AuthorizationException("Please enter the value for the Reason for Change FDA Audit field");
			}
		}
		
		//Bug Fix : 12414
		if(eventStatus.getEventStatusCode() == null || StringUtil.isEmpty(eventStatus.getEventStatusCode().getCode())){
            if (logger.isDebugEnabled()) logger.debug("Event Status Code is required");
            addIssue(
                    new Issue(IssueTypes.DATA_VALIDATION, 
                            "Please enter a valid Event Status Code"));
            throw new AuthorizationException("Please enter a valid Event Status Code");
		}
				
	    execOn = eventStatus.getStatusValidFrom();
	    java.sql.Date protEndDate = DateUtil.dateToSqlDate(execOn);
		execBy = callingUser.getUserId();
		oldStatus = PatientScheduleDAO.getOldEventStatus(eventPK);
		try{//Bug Fix : 12414
			statusCodelstPK = dereferenceSchCode(eventStatus.getEventStatusCode(), CodeCache.CODE_TYPE_EVENT_STATUS_TYPE, callingUser);
		}catch(OperationException e){
            if (logger.isDebugEnabled()) logger.debug("Event Status Code is required");
            addIssue(
                    new Issue(IssueTypes.DATA_VALIDATION, 
                            "Please enter a valid Event Status Code"));
            throw new AuthorizationException("Please enter a valid Event Status Code");
			
		}
	    EventdefBean eventDefBean = new EventdefBean();
		eventDefBean = PatientScheduleDAO.getOldEventDetails(eventPK);
		oldEventCoverageType = EJBUtil.stringToInteger(eventDefBean.getEventCoverageType());
		//Bug Fix : 12377	
		if((eventStatus.getCoverageType() == null) || (eventStatus.getSiteOfService() == null) || (eventStatus.getNotes() ==  null)){
//			    EventdefBean eventDefBean = new EventdefBean();
//				eventDefBean = PatientScheduleDAO.getOldEventDetails(eventPK);
//				oldEventCoverageType = EJBUtil.stringToInteger(eventDefBean.getEventCoverageType());
				if(eventStatus.getCoverageType() == null){
					eventCoverageType = EJBUtil.stringToNum(eventDefBean.getEventCoverageType());
					if(eventStatus.getReasonForChangeCoverType() != null || !StringUtil.isEmpty(eventStatus.getReasonForChangeCoverType())){
			             if (logger.isDebugEnabled()) logger.debug("Cannot enter new Reason For Coverage Change without changing the Coverage Type");
			             addIssue(
			                     new Issue(IssueTypes.DATA_VALIDATION, 
			                             "Cannot enter new Reason For Coverage Change without changing the Coverage Type"));
			             throw new AuthorizationException("Cannot enter new Reason For Coverage Change without changing the Coverage Type");
					}
					reasonForCoverageChange = eventDefBean.getReasonForCoverageChange();
				}
				//Bug Fix : 12415
				if(eventStatus.getSiteOfService() == null){
					eventSOSId = EJBUtil.stringToNum(eventDefBean.getEventSOSId());
				}
				if(eventStatus.getNotes() == null){
					notes = eventDefBean.getNotes();
				}
			}
		//Bug Fix : 12377	
		if((eventStatus.getCoverageType() != null) && !StringUtil.isEmpty(eventStatus.getCoverageType().getCode())){
			eventCoverageType = dereferenceSchCode(eventStatus.getCoverageType(), CodeCache.CODE_TYPE_COVERAGE_TYPE, callingUser);
			if((eventStatus.getReasonForChangeCoverType() == null || StringUtil.isEmpty(eventStatus.getReasonForChangeCoverType())) 
					&& (eventCoverageType != oldEventCoverageType)){
	             if (logger.isDebugEnabled()) logger.debug("Reason For Coverage Change is Required");
	             addIssue(
	                     new Issue(IssueTypes.DATA_VALIDATION, 
	                             "Please enter the Reason For The Coverage Change"));
	             throw new AuthorizationException("Please enter the Reason For The Coverage Change");
			}
			reasonForCoverageChange = eventStatus.getReasonForChangeCoverType();
		}
		
		if((eventStatus.getSiteOfService() !=null) && (!StringUtil.isEmpty(eventStatus.getSiteOfService().getSiteName())
				                                   || !StringUtil.isEmpty(eventStatus.getSiteOfService().getOID())
				                                   || !StringUtil.isEmpty(eventStatus.getSiteOfService().getSiteAltId()))){	
				try {//BUg Fix : 12615
					eventSOSId = ObjectLocator.sitePKFromIdentifier(callingUser, eventStatus.getSiteOfService(), sessionContext, objectMapService);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					if (logger.isDebugEnabled()) logger.debug("Event Status Code is required");
					addIssue(new Issue(IssueTypes.INVALID_SITE_OF_SERVICE, "Invalid site of service"));
					throw new OperationException("Invalid site of service");
				}
				
				//Bug Fix : 13137
				if (eventSOSId == null || eventSOSId == 0){
					addIssue(new Issue(IssueTypes.INVALID_SITE_OF_SERVICE, "Invalid site of service"));
					throw new OperationException("Invalid site of service");
				}
			}

		
		if(eventStatus.getNotes() != null && !StringUtil.isEmpty(eventStatus.getNotes())){
			notes = eventStatus.getNotes();
		}
		
		remarks = eventStatus.getReasonForChange();
		
		eventdefB.MarkDone(eventPK.intValue(), notes, protEndDate, execBy, statusCodelstPK, oldStatus, callingUser.getIpAdd(), mode, eventSOSId, eventCoverageType, reasonForCoverageChange);
        
		String eventPKString = eventPK.toString();
		int len_evt_id = eventPKString.length();
		int len_pad = 10 - len_evt_id ;
		String len_pad_str = "";

		for (int k=0; k < len_pad; k++ ){
			len_pad_str = len_pad_str + "0";
		}
		eventPKString = len_pad_str + eventPKString;
		
		eventStatB.setEventStatDate(DateUtil.dateToString(eventStatus.getStatusValidFrom()));
		eventStatB.setEvtStatNotes(eventStatus.getNotes());
		eventStatB.setFkEvtStat(eventPKString);
		eventStatB.setEvtStatus(statusCodelstPK.toString());
		eventStatB.setCreator(StringUtil.integerToString(callingUser.getUserId()));
		eventStatB.setIpAdd(IP_ADDRESS_FIELD_VALUE);
		eventStatB.setEventStatDetails();
		//Bug Fix : 12380
		if (!StringUtil.isEmpty(remarks)){
			AuditRowEschJB schAuditJB = new AuditRowEschJB();
			schAuditJB.setReasonForChangeOfScheduleEvent(eventPK, 
					callingUser.getUserId(), remarks);
			boolean excludeCurrent = (!mode.equals("M"))?true:false;
			schAuditJB.setReasonForChangeOfScheduleEventStatus(eventPK, callingUser.getUserId(), remarks, excludeCurrent);
		}
		
		eventStatusPK  = eventResB.getStatusIdOfTheEvent(eventPK);
		ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_EVENT_STATUS_TABLE, eventStatusPK);
		eventStatusIdentifier.setOID(map.getOID());
		eventStatusIdentifier.setPK(eventStatusPK);
		response.addObjectCreatedAction(eventStatusIdentifier);
		}	catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", t);
			throw new OperationException(t);
		}
		
		return response;
	}
	//Tarandeep
	public SitesOfService getSitesOfService() throws OperationException{
		SitesOfService sitesOfService = new SitesOfService();
		int accountPK = 0;
		try{
			
			if (callingUser.getUserId() == null || callingUser.getUserId() == 0){
				
				addIssue(new Issue(IssueTypes.USER_NOT_FOUND,"Valid calling user required for getting the Sites of Service"));
				throw new OperationException();
			}
			
			accountPK = EJBUtil.stringToNum(callingUser.getUserAccountId());
			sitesOfService = PatientScheduleDAO.getSitesOfService(accountPK);
			return sitesOfService;
		}
		catch(OperationException e){
			//sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", t);
			throw new OperationException(t);
		}
		
	}
	
	//Tarandeep Singh
	public ResponseHolder updateMEventStatus(ScheduleEventStatuses eventStatuses) throws OperationException{
		
		Integer eventPK;
		Integer eventSOS = 0;
		Integer eventStatus = 0;
		Integer patProtPK = 0;
		Integer studyPK = 0;
		
		String oldEventCoverageType = "";
		String mode = "M";
		String [] eventCoverageTypes = new String[1000];
		String [] eventStats        = new String[1000];
		String[] reasonForCoverageChange = new String[1000];
		String [] eventSOSId = new String[1000];
		String [] eventNotes = new String[1000];
		String [] eventStatusDates = new String[1000];
		
		try{
			GroupAuthModule groupAuth = 
					new GroupAuthModule(callingUser, groupRightsAgent);
				Integer manageProtocolPriv = 
					groupAuth.getAppManagePatientsPrivileges();
				
				boolean hasViewManageProt = 
					GroupAuthModule.hasViewPermission(manageProtocolPriv);
				//Bug Fix : 15295
				if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
				if (!hasViewManageProt){
					addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to view Patient data"));
					throw new AuthorizationException("User Not Authorized to view Patient data");
				}
			String [] eventPKs = new String[eventStatuses.getScheduleEventStatInfo().size()];
			String [] remarks = new String[eventStatuses.getScheduleEventStatInfo().size()];
			for(int i = 0; i < eventStatuses.getScheduleEventStatInfo().size(); i++){
				
				if(eventStatuses.getScheduleEventStatInfo().get(i).getScheduleEventStatus().getStatusValidFrom() == null 
						|| StringUtil.isEmpty(eventStatuses.getScheduleEventStatInfo().get(i).getScheduleEventStatus().getStatusValidFrom().toString())){
		            if (logger.isDebugEnabled()) logger.debug("Status Valid From field is required");
		            addIssue(
		                    new Issue(IssueTypes.DATA_VALIDATION, 
		                            "Please enter the value for the Status Valid From field"));
		            throw new AuthorizationException("Please enter the value for the Status Valid From field");
				}
				
				if(eventStatuses.getScheduleEventStatInfo().get(i).getEventIdentifier() != null 
						&& ((eventStatuses.getScheduleEventStatInfo().get(i).getEventIdentifier().getOID() != null 
						&& eventStatuses.getScheduleEventStatInfo().get(i).getEventIdentifier().getOID().length() > 0)
						|| (eventStatuses.getScheduleEventStatInfo().get(i).getEventIdentifier().getPK() != null 
						&& eventStatuses.getScheduleEventStatInfo().get(i).getEventIdentifier().getPK() >0))){
					
					if(eventStatuses.getScheduleEventStatInfo().get(i).getEventIdentifier().getPK() != null 
							&& eventStatuses.getScheduleEventStatInfo().get(i).getEventIdentifier().getPK() > 0)
					{
						eventPK = eventStatuses.getScheduleEventStatInfo().get(i).getEventIdentifier().getPK();
						if(eventPK == null || eventPK == 0)
						{ 
							addIssue(new Issue(IssueTypes.EVENT_NOT_FOUND,				
								"Event not found for Event Identifier : PK " 
							     + eventStatuses.getScheduleEventStatInfo().get(i).getEventIdentifier().getPK())); 
							throw new OperationException("Event not found for Event Identifier : PK " 
							     + eventStatuses.getScheduleEventStatInfo().get(i).getEventIdentifier().getPK()); 
						}
						
					}else{
						
						eventPK = objectMapService.getObjectPkFromOID(eventStatuses.getScheduleEventStatInfo().get(i).getEventIdentifier().getOID()); 
						if(eventPK == null || eventPK == 0)
						{ 
							addIssue(new Issue(IssueTypes.EVENT_NOT_FOUND,				
									"Event not found for Event Identifier : OID " 
										     + eventStatuses.getScheduleEventStatInfo().get(i).getEventIdentifier().getOID())); 
										throw new OperationException("Event not found for Event Identifier : OID " 
										     + eventStatuses.getScheduleEventStatInfo().get(i).getEventIdentifier().getOID()); 
						}
					}
				}
				else{
					addIssue(new Issue(IssueTypes.EVENT_IDENTIFIER_INAVLID, "Valid Event Identifier is required to update the Event Status")); 
					throw new OperationException("Valid Event Identifier is required to update the Event Status"); 
				}
				
				patProtPK = PatientScheduleDAO.getPatProtPK(eventPK);
				
				studyPK = PatientScheduleDAO.getStudyPKFromPatProt(patProtPK);
				
				if(studyPK == null || studyPK == 0 || !PatientScheduleDAO.ifStudyExists(studyPK))
				{
					addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found"));
					throw new OperationException();
				}
				
				//Bug Fix : 15305
				if(PatientScheduleDAO.ifFDAStudy(studyPK)){
					if(eventStatuses.getScheduleEventStatInfo().get(i).getScheduleEventStatus().getReasonForChangeFDAAudit() == null 
							|| StringUtil.isEmpty(eventStatuses.getScheduleEventStatInfo().get(i).getScheduleEventStatus().getReasonForChangeFDAAudit())){
			            if (logger.isDebugEnabled()) logger.debug("Reason For Change FDA Audit field is required");
			            addIssue(
			                    new Issue(IssueTypes.DATA_VALIDATION, 
			                            "Please enter the value for the Reason for Change FDA Audit field"));
			            throw new AuthorizationException("Please enter the value for the Reason for Change FDA Audit field");
					}
				}
				//Bug Fix : 15301
				TeamAuthModule teamAuthModule = new TeamAuthModule(callingUser.getUserId(), studyPK);
				int studyTeamPrivileges = teamAuthModule.getStudyTeamPrivileges();
				if (!TeamAuthModule.hasViewPermission(studyTeamPrivileges)){
					addIssue(
							new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, 
									"User does not have view permission to view Study Team"));
					throw new AuthorizationException("User does not have view permission to view Study Team");
				}
				
				int patientManageViewPrivileges = teamAuthModule.getPatientManagePrivileges();
				if (!TeamAuthModule.hasViewPermission(patientManageViewPrivileges)){
					addIssue(
							new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION,
									"User does not have view permission to edit the Patient data"));
					throw new AuthorizationException("User does not have the view permission to edit the Patient data");
				}
				
				int patientManageEditPrivileges = teamAuthModule.getPatientManagePrivileges();
				if (!TeamAuthModule.hasEditPermission(patientManageEditPrivileges)){
					addIssue(
							new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION,
									"User does not have edit permission to edit the Patient data"));
					throw new AuthorizationException("User does not have the edit permission to edit the Patient data");
				}
				
				eventPKs[i] = EJBUtil.integerToString(eventPK);
			    EventdefBean eventDefBean = new EventdefBean();
				eventDefBean = PatientScheduleDAO.getEventDef(eventPK);
				oldEventCoverageType = eventDefBean.getEventCoverageType();
				if((eventStatuses.getScheduleEventStatInfo().get(i).getScheduleEventStatus().getCoverageType() == null) || 
						(eventStatuses.getScheduleEventStatInfo().get(i).getScheduleEventStatus().getSiteOfService() == null) || 
						(eventStatuses.getScheduleEventStatInfo().get(i).getScheduleEventStatus().getNotes() ==  null)){
//				    EventdefBean eventDefBean = new EventdefBean();
//					eventDefBean = PatientScheduleDAO.getEventDef(eventPK);
//					oldEventCoverageType = eventDefBean.getEventCoverageType();
					if(eventStatuses.getScheduleEventStatInfo().get(i).getScheduleEventStatus().getCoverageType() == null){
						eventCoverageTypes[i] = eventDefBean.getEventCoverageType();
						if(eventStatuses.getScheduleEventStatInfo().get(i).getScheduleEventStatus().getReasonForChangeCoverType() != null || 
								!StringUtil.isEmpty(eventStatuses.getScheduleEventStatInfo().get(i).getScheduleEventStatus().getReasonForChangeCoverType())){
				             if (logger.isDebugEnabled()) logger.debug("Cannot enter new Reason For Coverage Change without changing the Coverage Type");
				             addIssue(
				                     new Issue(IssueTypes.DATA_VALIDATION, 
				                             "Cannot enter new Reason For Coverage Change without changing the Coverage Type"));
				             throw new AuthorizationException("Cannot enter new Reason For Coverage Change without changing the Coverage Type");
						}
						reasonForCoverageChange[i] = eventDefBean.getReasonForCoverageChange();
					}
					
					if(eventStatuses.getScheduleEventStatInfo().get(i).getScheduleEventStatus().getSiteOfService() == null){
						eventSOSId[i] = eventDefBean.getEventSOSId();
					}
					if(eventStatuses.getScheduleEventStatInfo().get(i).getScheduleEventStatus().getNotes() == null){
						eventNotes[i] = eventDefBean.getNotes();
					}
				}
				
				eventNotes[i] = eventStatuses.getScheduleEventStatInfo().get(i).getScheduleEventStatus().getNotes();
				
				if((eventStatuses.getScheduleEventStatInfo().get(i).getScheduleEventStatus().getCoverageType() != null) 
						&& !StringUtil.isEmpty(eventStatuses.getScheduleEventStatInfo().get(i).getScheduleEventStatus().getCoverageType().getCode())){
					try{
						eventCoverageTypes[i] = EJBUtil.integerToString(dereferenceSchCode(eventStatuses.getScheduleEventStatInfo().get(i).getScheduleEventStatus().getCoverageType(), CodeCache.CODE_TYPE_COVERAGE_TYPE, callingUser));
					}catch (Exception e) {
						// TODO Auto-generated catch block
						if (logger.isDebugEnabled()) logger.debug("Coverage Type not found");
						addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Please enter a valid Coverage Type"));
						throw new OperationException("Please enter a valid Coverage Type");
					}
					try{//Bug Fix : 15300
						if((eventStatuses.getScheduleEventStatInfo().get(i).getScheduleEventStatus().getReasonForChangeCoverType() == null || 
								StringUtil.isEmpty(eventStatuses.getScheduleEventStatInfo().get(i).getScheduleEventStatus().getReasonForChangeCoverType())) 
								&& (eventCoverageTypes[i].compareTo(oldEventCoverageType) != 0)){
				             if (logger.isDebugEnabled()) logger.debug("Reason For Coverage Change is Required");
				             addIssue(
				                     new Issue(IssueTypes.DATA_VALIDATION, 
				                             "Please enter the Reason For The Coverage Change"));
				             throw new AuthorizationException("Please enter the Reason For The Coverage Change");
						}
					}catch(Exception e){
			             if (logger.isDebugEnabled()) logger.debug("Reason For Coverage Change is Required");
			             addIssue(
			                     new Issue(IssueTypes.DATA_VALIDATION, 
			                             "Please enter the Reason For The Coverage Change"));
			             throw new AuthorizationException("Please enter the Reason For The Coverage Change");
					}
					reasonForCoverageChange[i] = eventStatuses.getScheduleEventStatInfo().get(i).getScheduleEventStatus().getReasonForChangeCoverType();
				}
				
				if((eventStatuses.getScheduleEventStatInfo().get(i).getScheduleEventStatus().getEventStatusCode() != null) 
						&& !StringUtil.isEmpty(eventStatuses.getScheduleEventStatInfo().get(i).getScheduleEventStatus().getEventStatusCode().getCode())){
					try{
						eventStats[i] = EJBUtil.integerToString(dereferenceSchCode(eventStatuses.getScheduleEventStatInfo().get(i).getScheduleEventStatus().getEventStatusCode(), CodeCache.CODE_TYPE_EVENT_STATUS_TYPE, callingUser));
					}catch (Exception e) {
						// TODO Auto-generated catch block
						if (logger.isDebugEnabled()) logger.debug("Coverage Type not found");
						addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Please enter a valid Coverage Type"));
						throw new OperationException("Please enter a valid Coverage Type");
					}
				}else{
					if (logger.isDebugEnabled()) logger.debug("Event Status Can Not Found");
					addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Please enter a valid Event Status"));
					throw new OperationException("Please enter a valid Event Status");
				}
				
				if((eventStatuses.getScheduleEventStatInfo().get(i).getScheduleEventStatus().getSiteOfService() !=null) 
						&& (!StringUtil.isEmpty(eventStatuses.getScheduleEventStatInfo().get(i).getScheduleEventStatus().getSiteOfService().getSiteName())
						|| !StringUtil.isEmpty(EJBUtil.integerToString(eventStatuses.getScheduleEventStatInfo().get(i).getScheduleEventStatus().getSiteOfService().getPK()))
						|| !StringUtil.isEmpty(eventStatuses.getScheduleEventStatInfo().get(i).getScheduleEventStatus().getSiteOfService().getOID())
						|| !StringUtil.isEmpty(eventStatuses.getScheduleEventStatInfo().get(i).getScheduleEventStatus().getSiteOfService().getSiteAltId()))){	
					try {
						eventSOS = ObjectLocator.sitePKFromIdentifier(callingUser, eventStatuses.getScheduleEventStatInfo().get(i).getScheduleEventStatus().getSiteOfService(), sessionContext, objectMapService);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						if (logger.isDebugEnabled()) logger.debug("Invalid site of service");
						addIssue(new Issue(IssueTypes.INVALID_SITE_OF_SERVICE, "Invalid site of service"));
						throw new OperationException("Invalid site of service");
					}

					if (eventSOS == null || eventSOS == 0 || !PatientScheduleDAO.ifSiteExists(eventSOS)){
						addIssue(new Issue(IssueTypes.INVALID_SITE_OF_SERVICE, "Invalid site of service"));
						throw new OperationException("Invalid site of service");
					}
					
					eventSOSId[i] = EJBUtil.integerToString(eventSOS);
				}
				
				eventStatusDates[i] = DateUtil.dateToString(eventStatuses.getScheduleEventStatInfo().get(i).getScheduleEventStatus().getStatusValidFrom());
				
				remarks[i] = eventStatuses.getScheduleEventStatInfo().get(i).getScheduleEventStatus().getReasonForChangeFDAAudit();
			}
			
			String[] strArrSchEventIds = PatientScheduleDAO.updateMEventStatus(eventPKs, eventNotes, eventStatusDates, callingUser.getUserId(), 
					                                                           AbstractService.IP_ADDRESS_FIELD_VALUE, mode, eventSOSId, 
					                                                           eventCoverageTypes, reasonForCoverageChange,eventStats);
			
			for(int i = 0; i < eventStatuses.getScheduleEventStatInfo().size(); i++){
				
				if (!StringUtil.isEmpty(remarks[i])){
					AuditRowEschJB schAuditJB = new AuditRowEschJB();
					schAuditJB.setReasonForChangeOfScheduleEvent(StringUtil.stringToNum(eventPKs[i]), 
							EJBUtil.stringToNum(EJBUtil.integerToString(callingUser.getUserId())), remarks[i]);
					boolean excludeCurrent = (!mode.equals("M"))?true:false;
					schAuditJB.setReasonForChangeOfScheduleEventStatus(StringUtil.stringToNum(eventPKs[i]), 
							EJBUtil.stringToNum(EJBUtil.integerToString(callingUser.getUserId())), remarks[i], excludeCurrent);
				}
				
			}
			
			for(String x:strArrSchEventIds)
			{
				EventStatusIdentifier identifier = new EventStatusIdentifier(); 
				identifier.setPK(Integer.parseInt(x));
				identifier.setOID(objectMapService.getOrCreateObjectMapFromPK("sch_evenstat", Integer.parseInt(x)).getOID());
				response.addAction(new CompletedAction("Event Status with PK :" + Integer.parseInt(x) + " and OID :"+ 
				                                        objectMapService.getOrCreateObjectMapFromPK("sch_evenstat", Integer.parseInt(x)).getOID() +
		    	                                        "  updated successfully", CRUDAction.UPDATE));


			}
			
			
			
		}catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("PatientScheduleServiceImpl create", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("PatientScheduleServiceImpl create", t);
			throw new OperationException(t);
		}
		
		return response;
	}
	
	@AroundInvoke
	public Object myInterceptor(InvocationContext ctx) throws Exception {
		response = new ResponseHolder();
		callingUser = 
			getLoggedInUser(
					sessionContext,
					userAgent);
		return ctx.proceed();

	}
		
	public ResponseHolder createMEventStatus(MEventStatuses multipleeventStatuses) throws OperationException
	{
		EventResourceJB eventResB = new EventResourceJB();
        EventStatusIdentifier eventStatusIdentifier = null;
		Integer eventPK = 0;
		Integer patProtPK = 0;
		Integer studyPK = 0;
		Integer personPK = 0;
		Integer eventSOSId = 0;
		Integer statusCodelstPK = 0;
		int eventStatusPK = 0;
		int eventCoverageType = 0;
		int oldEventCoverageType = 0;
		String notes = "";
		String reasonForCoverageChange = "";
		Map<String, Object> inputs=null;
		
		//List<MEventStatus> eventStatusList = new ArrayList<MEventStatus>();
		List<Map<String, Object>> inputsList=new ArrayList<Map<String,Object>>();
		EventStatus eventStatus=null;
		ScheduleEventIdentifier eventIdentifier=null;
		GroupAuthModule groupAuth =new GroupAuthModule(callingUser, groupRightsAgent);
		Integer manageProtocolPriv =groupAuth.getAppManagePatientsPrivileges();
		boolean hasViewManageProt =GroupAuthModule.hasViewPermission(manageProtocolPriv);
		if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
		if (!hasViewManageProt)
		{
			addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to view Patient data"));
			throw new AuthorizationException("User Not Authorized to view Patient data");
		}
		
		try
		{
		
			for(MEventStatus mEventStatus : multipleeventStatuses.getMeventStatus() )
			{
				eventStatus=mEventStatus.getEventStatus();
				eventIdentifier=mEventStatus.getEventIdentifier();
				if(eventIdentifier != null && ((eventIdentifier.getOID() != null && eventIdentifier.getOID().length() > 0)
							|| (eventIdentifier.getPK() != null && eventIdentifier.getPK() >0)))
				{
					if(eventIdentifier.getPK() != null && eventIdentifier.getPK() > 0)
					{
						eventPK = eventIdentifier.getPK();
					}
					else
					{
						eventPK = objectMapService.getObjectPkFromOID(eventIdentifier.getOID()); 
					}
					if(eventPK == null || eventPK == 0)
					{ 
						addIssue(new Issue(IssueTypes.EVENT_NOT_FOUND,"Event not found for Event Identifier : OID " + eventIdentifier.getOID()+" PK : "+eventIdentifier.getPK()));
						continue;
						//throw new OperationException("Event not found for Event Identifier : OID " + eventIdentifier.getOID()); 
					}
				}
				else
				{
					addIssue(new Issue(IssueTypes.EVENT_IDENTIFIER_INAVLID, "Valid Event Identifier is required to add the Scheduled Event"));
					continue;
					//throw new OperationException("Valid Event Identifier is required to add the Scheduled Event"); 
				}
				patProtPK = PatientScheduleDAO.getPatProtPK(eventPK);
				if(patProtPK == null || patProtPK == 0)
				{ 
					addIssue(new Issue(IssueTypes.PATIENT_SCHEDULE_DOES_NOT_EXIST,"Patient Schedule Does Not Exist for Event Identifier : OID " + eventIdentifier.getOID()+" PK : "+eventIdentifier.getPK())); 
					continue;
					//throw new OperationException("Patient Schedule Does Not Exist "); 
				}
				studyPK = PatientScheduleDAO.getStudyPKFromPatProt(patProtPK);
				personPK = PatientScheduleDAO.getPersonPK(patProtPK);
				if (personPK == null || personPK == 0)
				{
					addIssue(new Issue(IssueTypes.PATIENT_NOT_FOUND, "Study Patient not found for Event Identifier : OID " + eventIdentifier.getOID()+" PK : "+eventIdentifier.getPK()));
					continue;
					//throw new OperationException("Patient not found");
				}
				TeamAuthModule teamAuthModule = new TeamAuthModule(callingUser.getUserId(), studyPK);
				//Study team privileges (view)
				int studyTeamPrivileges = teamAuthModule.getStudyTeamPrivileges();
				if (!TeamAuthModule.hasViewPermission(studyTeamPrivileges))
				{
					addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION,"User does not have view permission to view Study Team for Event Identifier : OID " + eventIdentifier.getOID()+" PK : "+eventIdentifier.getPK()));
					continue;
					//throw new AuthorizationException("User does not have view permission to view Study Team");
				}
				//Patient manage privileges (edit)
				int patientManageViewPrivileges = teamAuthModule.getPatientManagePrivileges();
				if (!TeamAuthModule.hasEditPermission(patientManageViewPrivileges)){
					addIssue(new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION,"User does not have view permission to edit the Patient data for Event Identifier : OID " + eventIdentifier.getOID()+" PK : "+eventIdentifier.getPK()));
					continue;
					//throw new AuthorizationException("User does not have the view permission to edit the Patient data");
				}
				//-------------------
				StudySiteDao studySiteDao = studySiteAgent.getSitesForStatAndEnrolledPat(studyPK,callingUser.getUserId(), 0);
				ArrayList siteIds = studySiteDao.getSiteIds();
				Integer count = 0;
    			/*ArrayList<StudyPatient> listCompleteStudyPatient = new ArrayList<StudyPatient>();
    			if (siteIds != null && siteIds.size() > 0) {
    				for(int i = 0 ; i< siteIds.size(); i++){
    					sitePK = (Integer)siteIds.get(i);
    					listCompleteStudyPatient.addAll((ArrayList<StudyPatient>) StudyPatientDAO.
    							getStudyPatientByStudyPK(studyPK, sitePK));
    				}
    			}*/
    			count = StudyPatientDAO.getStudyPatientCountByStudyPK(studyPK, siteIds);
    			if(count==0){
					addIssue(new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION,"User does not have view permission for the Patient data for Event Identifier : OID " + eventIdentifier.getOID()+" PK : "+eventIdentifier.getPK()));
					continue;
					//throw new AuthorizationException("User does not have the view permission for the Patient data");
				}
				//----------------------
				Integer patFacilityRights = userSiteAgent.getUserPatientFacilityRight(callingUser.getUserId(), personPK);
				if (GroupAuthModule.hasViewPermission(patFacilityRights) == false)
				{
					if (logger.isDebugEnabled()) logger.debug("User not authorized to access data of this patient");
					addIssue(new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION,"User not authorized to access data of this patient for Event Identifier : OID " + eventIdentifier.getOID()+" PK : "+eventIdentifier.getPK()));
					continue;
					//throw new AuthorizationException("User not authorized to access data of this patient");
				}
				PatProtBean  patProtBean = patProtAgent.findCurrentPatProtDetails(studyPK, personPK); 
				if(patProtBean.getPatProtId() == 0)
				{
					addIssue(new Issue(IssueTypes.PATIENT_NOT_ONSTUDY, "Patient is not linked to study provided for Event Identifier : OID " + eventIdentifier.getOID()+" PK : "+eventIdentifier.getPK())); 
					continue;
					//throw new OperationException(); 
				}
				if(eventStatus.getStatusValidFrom() == null || StringUtil.isEmpty(eventStatus.getStatusValidFrom().toString())){
		            if (logger.isDebugEnabled()) logger.debug("Status Valid From field is required");
		            addIssue(new Issue(IssueTypes.DATA_VALIDATION,"Please enter the value for the field : Status Valid From, for Event Identifier : OID " + eventIdentifier.getOID()+" PK : "+eventIdentifier.getPK()));
		            continue;
					//throw new AuthorizationException("Please enter the value for the Status Valid From field");
				}
				if(eventStatus.getEventStatusCode() == null || StringUtil.isEmpty(eventStatus.getEventStatusCode().getCode())){
		            if (logger.isDebugEnabled()) logger.debug("Event Status Code is required");
		            addIssue(new Issue(IssueTypes.DATA_VALIDATION,"Please enter a valid Event Status Code for Event Identifier : OID " + eventIdentifier.getOID()+" PK : "+eventIdentifier.getPK()));
		            continue;
					//throw new AuthorizationException("Please enter a valid Event Status Code");
				}
				try
				{
					statusCodelstPK = dereferenceSchCode(eventStatus.getEventStatusCode(), CodeCache.CODE_TYPE_EVENT_STATUS_TYPE, callingUser);
				}
				catch(OperationException e)
				{
		            if (logger.isDebugEnabled()) logger.debug("Event Status Code is required");
		            addIssue(new Issue(IssueTypes.CODE_NOT_FOUND,"Please enter a valid Event Status Code for Event Identifier : OID " + eventIdentifier.getOID()+" PK : "+eventIdentifier.getPK()));
		            continue;
					//throw new AuthorizationException("Please enter a valid Event Status Code");
				}
				if((eventStatus.getCoverageType() == null) || (eventStatus.getSiteOfService() == null))
				{
				    EventdefBean eventDefBean = new EventdefBean();
					eventDefBean = PatientScheduleDAO.getOldEventDetails(eventPK);
					oldEventCoverageType = StringUtil.stringToInteger(eventDefBean.getEventCoverageType());
					if(eventStatus.getCoverageType() == null)
					{
						eventCoverageType = StringUtil.stringToNum(eventDefBean.getEventCoverageType());
						if(eventStatus.getReasonForChangeCoverType() != null || !StringUtil.isEmpty(eventStatus.getReasonForChangeCoverType()))
						{
							if (logger.isDebugEnabled()) logger.debug("Cannot enter new Reason For Coverage Change without changing the Coverage Type");
					        addIssue(new Issue(IssueTypes.DATA_VALIDATION,"Cannot enter new Reason For Coverage Change without changing the Coverage Type for Event Identifier : OID " + eventIdentifier.getOID()+" PK : "+eventIdentifier.getPK()));
					        continue;
							//throw new AuthorizationException("Cannot enter new Reason For Coverage Change without changing the Coverage Type");
					    }
						reasonForCoverageChange = eventDefBean.getReasonForCoverageChange();
					}
					if(eventStatus.getSiteOfService() == null)
					{
						eventSOSId = StringUtil.stringToNum(eventDefBean.getEventSOSId());
					}
				}
				if((eventStatus.getCoverageType() != null) && !StringUtil.isEmpty(eventStatus.getCoverageType().getCode()))
				{
					eventCoverageType = dereferenceSchCode(eventStatus.getCoverageType(), CodeCache.CODE_TYPE_COVERAGE_TYPE, callingUser);
					if((eventStatus.getReasonForChangeCoverType() == null || StringUtil.isEmpty(eventStatus.getReasonForChangeCoverType())) 
							&& (eventCoverageType != oldEventCoverageType))
					{
			             if (logger.isDebugEnabled()) logger.debug("Reason For Coverage Change is Required");
			             addIssue(new Issue(IssueTypes.DATA_VALIDATION,"Please enter the Reason For The Coverage Change for Event Identifier : OID " + eventIdentifier.getOID()+" PK : "+eventIdentifier.getPK()));
			             continue;
							//throw new AuthorizationException("Please enter the Reason For The Coverage Change");
					}
					reasonForCoverageChange = eventStatus.getReasonForChangeCoverType();
				}
				
				if((eventStatus.getSiteOfService() !=null) && (!StringUtil.isEmpty(eventStatus.getSiteOfService().getSiteName())
						                                   || !StringUtil.isEmpty(eventStatus.getSiteOfService().getOID())
						                                   || !StringUtil.isEmpty(eventStatus.getSiteOfService().getSiteAltId()))){	
						try {
							eventSOSId = ObjectLocator.sitePKFromIdentifier(callingUser, eventStatus.getSiteOfService(), sessionContext, objectMapService);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							if (logger.isDebugEnabled()) logger.debug("Event Status Code is required");
							addIssue(new Issue(IssueTypes.INVALID_SITE_OF_SERVICE, "Invalid site of service for Event Identifier : OID " + eventIdentifier.getOID()+" PK : "+eventIdentifier.getPK()));
							continue;
							//throw new OperationException("Invalid site of service");
						}
						if (eventSOSId == null || eventSOSId == 0){
							addIssue(new Issue(IssueTypes.INVALID_SITE_OF_SERVICE, "Invalid site of service for Event Identifier : OID " + eventIdentifier.getOID()+" PK : "+eventIdentifier.getPK()));
							continue;
							//throw new OperationException("Invalid site of service");
						}
					}
				if(eventStatus.getNotes() != null && !StringUtil.isEmpty(eventStatus.getNotes())){
					notes = eventStatus.getNotes();
				}
				else
				{
					notes="";
				}
				String eventPKString = eventPK.toString();
				int len_evt_id = eventPKString.length();
				int len_pad = 10 - len_evt_id ;
				String len_pad_str = "";
				for (int k=0; k < len_pad; k++ ){
					len_pad_str = len_pad_str + "0";
				}
				eventPKString = len_pad_str + eventPKString;
				inputs=null;
				inputs=new HashMap<String, Object>();
				inputs.put("event_id",eventPK.intValue() );
				inputs.put("notes",notes );
				inputs.put("exe_date",eventStatus.getStatusValidFrom());
				//inputs.put("exe_by", execBy);
				inputs.put("status",statusCodelstPK );
				inputs.put("oldStatus",PatientScheduleDAO.getOldEventStatus(eventPK));
				//inputs.put("ipAdd",callingUser.getIpAdd() );
				//inputs.put("mode", mode);
				inputs.put("eventSOSId", eventSOSId);
				inputs.put("eventCoverType", eventCoverageType);
				inputs.put("reasonForCoverageChange", eventStatus.getReasonForChangeCoverType());
				inputs.put("eventPKString", eventPKString);
				inputs.put("reasonForChange", eventStatus.getReasonForChange());
				//eventdefB.MarkDone(eventPK.intValue(), notes, protEndDate, execBy, statusCodelstPK, oldStatus, callingUser.getIpAdd(), mode, eventSOSId, eventCoverageType, reasonForCoverageChange);
				inputsList.add(inputs);
				//eventStatusList.add(mEventStatus);
			}
			PatientScheduleDAO patientScheduleDAO=new PatientScheduleDAO();
			//multipleeventStatuses.setMeventStatus(eventStatusList);
			int [] numUpdates=patientScheduleDAO.createMEventStatus(callingUser,inputsList);
			ObjectMap map=null;
			for (int i=0; i < numUpdates.length; i++)
			{            
				if (numUpdates[i] == Statement.SUCCESS_NO_INFO || numUpdates[i] >=0)
			    {
					eventStatusPK  = eventResB.getStatusIdOfTheEvent((Integer) inputsList.get(i).get("event_id"));
					map=null;
					map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_EVENT_STATUS_TABLE, eventStatusPK);
					eventStatusIdentifier=null;
					eventStatusIdentifier=new EventStatusIdentifier();
					eventStatusIdentifier.setOID(map.getOID());
					eventStatusIdentifier.setPK(eventStatusPK);
					response.addObjectCreatedAction(eventStatusIdentifier);
				}
			    else
			    {
			    	response.addIssue(new Issue(IssueTypes.EVENT_ERROR_CREATE_STATUS));
				}
			}
		}	catch(OperationException e){
				//sessionContext.setRollbackOnly();
				if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", e);
				e.setIssues(response.getIssues());
				throw e;
			}
			catch(Throwable t){
				this.addUnknownThrowableIssue(t);
				if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", t);
				throw new OperationException(t);
			}
		return response;
	}
	
	
	//Tarandeep Singh Bali
	public MPatientScheduleList addMPatientSchedules(MPatientSchedules mPatientSchedules) 
			throws OperationException{
		
		Integer patientPK = 0;
		Integer calPK = 0;
		Integer studyPK = 0;
		Integer visitPK = 0;
		
		String calName = "";
		
		String[] PatPKs = new String[mPatientSchedules.getmPatientSchedule().size()];
		String[] CalPKs = new String[mPatientSchedules.getmPatientSchedule().size()];
		String[] startDates = new String[mPatientSchedules.getmPatientSchedule().size()];
		String[] studyIdentifiers = new String[mPatientSchedules.getmPatientSchedule().size()];
		String[] visitPKs = new String[mPatientSchedules.getmPatientSchedule().size()];
		
		for(int i = 0; i < mPatientSchedules.getmPatientSchedule().size(); i++){
			
			if(mPatientSchedules.getmPatientSchedule().get(i).getPatientIdentifier().getOID() != null 
					|| mPatientSchedules.getmPatientSchedule().get(i).getPatientIdentifier().getPK() != null 
					|| (mPatientSchedules.getmPatientSchedule().get(i).getPatientIdentifier().getPatientId() != null 
					&& mPatientSchedules.getmPatientSchedule().get(i).getPatientIdentifier().getOrganizationId() != null)){
				
				
				try {
					patientPK = ObjectLocator.personPKFromPatientIdentifier(callingUser, 
							mPatientSchedules.getmPatientSchedule().get(i).getPatientIdentifier(), objectMapService);
				} catch (MultipleObjectsFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
				if(patientPK == 0 || patientPK== null || !PatientScheduleDAO.ifPatientExists(patientPK)){
					addIssue(new Issue(IssueTypes.PATIENT_NOT_FOUND,				
							"Patient not found for Patient Identifier")); 
					throw new OperationException("Patient not found for Patient Identifier"); 
				}

			}
			else{
				addIssue(new Issue(IssueTypes.PATIENT_IDENTIFIER_INVALID, "Valid Patient Identifier is required to add the Patient Demographics")); 
				throw new OperationException("Valid EventIdentifier is required to add the Patient Demographics"); 
			}
			
			PatPKs[i] = EJBUtil.integerToString(patientPK);
			
			if(mPatientSchedules.getmPatientSchedule().get(i).getCalendarIdentifier() != null){
				if((mPatientSchedules.getmPatientSchedule().get(i).getCalendarIdentifier().getPK() != null) && 
						(mPatientSchedules.getmPatientSchedule().get(i).getCalendarIdentifier().getPK() > 0)){
					calPK = mPatientSchedules.getmPatientSchedule().get(i).getCalendarIdentifier().getPK();
				}
				else{
					calPK = objectMapService.getObjectPkFromOID(mPatientSchedules.getmPatientSchedule().get(i).getCalendarIdentifier().getOID());
				}
				
				if(calPK == null || calPK == 0){
					addIssue(new Issue(IssueTypes.CALENDAR_NOT_FOUND,				
							"Calendar not found for the provided Calendar Identifier")); 
					throw new OperationException("Calendar not found for the provided Calendar Identifier"); 
				}
			}else if(mPatientSchedules.getmPatientSchedule().get(i).getStudyIdentifier() != null && mPatientSchedules.getmPatientSchedule().get(i) !=null 
					&& (mPatientSchedules.getmPatientSchedule().get(i).getStudyIdentifier().getPK() != null 
					|| mPatientSchedules.getmPatientSchedule().get(i).getStudyIdentifier().getOID() != null 
					|| mPatientSchedules.getmPatientSchedule().get(i).getStudyIdentifier().getStudyNumber() != null)
					&& (mPatientSchedules.getmPatientSchedule().get(i).getStudyIdentifier().getOID() != null 
					|| mPatientSchedules.getmPatientSchedule().get(i).getStudyIdentifier().getPK() != null)
					&& mPatientSchedules.getmPatientSchedule().get(i).getCalendarName() != null){
				
				    studyPK = ObjectLocator.studyPKFromIdentifier(callingUser, mPatientSchedules.getmPatientSchedule().get(i).getStudyIdentifier(), objectMapService);
				    
					if(studyPK == null || studyPK == 0 || !PatientScheduleDAO.ifStudyExists(studyPK))
					{
						addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found for StudyIdentifier provided"));
						throw new OperationException();
					}
					
					calName  = mPatientSchedules.getmPatientSchedule().get(i).getCalendarName();
					
					calPK = PatientScheduleDAO.getCalPK(studyPK, calName);
			
			}else{
				addIssue(new Issue(IssueTypes.CALENDAR_NOT_FOUND, "Valid Calendar Identifier or StudyIdentifier and Calendar Name is required to add the Patient Demographics")); 
				throw new OperationException("Valid CalendarIdentifier or StudyIdentfier and Calendar Name is required to add the Patient Demographics"); 
			}
			
			if(mPatientSchedules.getmPatientSchedule().get(i).getStudyIdentifier()==null || (StringUtil.isEmpty(mPatientSchedules.getmPatientSchedule().get(i).getStudyIdentifier().getOID()) 
					&& (mPatientSchedules.getmPatientSchedule().get(i).getStudyIdentifier().getPK()==null || mPatientSchedules.getmPatientSchedule().get(i).getStudyIdentifier().getPK()<=0) 
					&& StringUtil.isEmpty(mPatientSchedules.getmPatientSchedule().get(i).getStudyIdentifier().getStudyNumber())))
			{
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid studyIdentifier is required to generate Schedule"));
			throw new OperationException();
			}
			
			studyPK = locateStudyPK(mPatientSchedules.getmPatientSchedule().get(i).getStudyIdentifier());
			if(studyPK == null || studyPK == 0){
				addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found for StudyIdentifier OID:" + mPatientSchedules.getmPatientSchedule().get(i).getStudyIdentifier().getOID() + " StudyNumber " + mPatientSchedules.getmPatientSchedule().get(i).getStudyIdentifier().getStudyNumber()));
				throw new OperationException();
			}
			
			StudyBean sb = studyAgent.getStudyDetails(studyPK);
			if (sb == null || (! sb.getAccount().equals(callingUser.getUserAccountId()))){
				StringBuffer errorMessage = new StringBuffer("Study not found for:"); 
				if(mPatientSchedules.getmPatientSchedule().get(i).getStudyIdentifier().getOID() != null && mPatientSchedules.getmPatientSchedule().get(i).getStudyIdentifier().getOID().length() > 0 )
				{
					errorMessage.append(" OID:" + mPatientSchedules.getmPatientSchedule().get(i).getStudyIdentifier().getOID()); 
				}
				if(mPatientSchedules.getmPatientSchedule().get(i).getStudyIdentifier().getStudyNumber() != null && mPatientSchedules.getmPatientSchedule().get(i).getStudyIdentifier().getStudyNumber().length() > 0)
				{
					errorMessage.append( " Study Number:" + mPatientSchedules.getmPatientSchedule().get(i).getStudyIdentifier().getStudyNumber()); 
				}
				if(!StringUtil.isEmpty(StringUtil.integerToString(mPatientSchedules.getmPatientSchedule().get(i).getStudyIdentifier().getPK())))
				{
					errorMessage.append( " Study Pk: " + mPatientSchedules.getmPatientSchedule().get(i).getStudyIdentifier().getPK());
				}
				Issue issue = new Issue(IssueTypes.STUDY_NOT_FOUND, errorMessage.toString());
				addIssue(issue);
				logger.error(issue);
				throw new OperationException(); 
			}
			
			String startDate = "";
			if(mPatientSchedules.getmPatientSchedule().get(i).getStartDate() != null){
				startDate = DateUtil.dateToString(mPatientSchedules.getmPatientSchedule().get(i).getStartDate());
			}
			
			startDates[i] = startDate;
			
			if(mPatientSchedules.getmPatientSchedule().get(i).getVisitIdentifier() != null 
					&& ((mPatientSchedules.getmPatientSchedule().get(i).getVisitIdentifier().getOID() != null 
					&& mPatientSchedules.getmPatientSchedule().get(i).getVisitIdentifier().getOID().length() > 0)
					|| (mPatientSchedules.getmPatientSchedule().get(i).getVisitIdentifier().getPK() != null 
					&& mPatientSchedules.getmPatientSchedule().get(i).getVisitIdentifier().getPK() >0))){

				if(mPatientSchedules.getmPatientSchedule().get(i).getVisitIdentifier().getPK() != null 
						&& mPatientSchedules.getmPatientSchedule().get(i).getVisitIdentifier().getPK() > 0)
				{
					visitPK = mPatientSchedules.getmPatientSchedule().get(i).getVisitIdentifier().getPK();
					if(visitPK == null || visitPK == 0 || !PatientScheduleDAO.ifVisitExists(visitPK))
					{ 
						addIssue(new Issue(IssueTypes.VISIT_NOT_FOUND,				
								"Visit not found for Visit Identifier : PK " + visitPK)); 
						throw new OperationException("Visit not found for Visit Identifier : PK " + visitPK); 
					}

				}else{

					visitPK = objectMapService.getObjectPkFromOID(mPatientSchedules.getmPatientSchedule().get(i).getVisitIdentifier().getOID());
					if(visitPK == null || visitPK == 0 || !PatientScheduleDAO.ifVisitExists(visitPK))
					{ 
						addIssue(new Issue(IssueTypes.VISIT_NOT_FOUND,				
								"Visit not found for Visit Identifier : OID " + mPatientSchedules.getmPatientSchedule().get(i).getVisitIdentifier().getOID())); 
						throw new OperationException("Visit not found for Visit Identifier : OID " + mPatientSchedules.getmPatientSchedule().get(i).getVisitIdentifier().getOID()); 
					}				
				}
			}
			
			Integer prevProtocolPK = 0;
			prevProtocolPK = PatientScheduleDAO.getProtocolPK(patientPK, studyPK);
			
			Integer patprotPK = 0;
			PatProtBean  patProtBean = new PatProtBean();
			patProtBean = patProtAgent.findCurrentPatProtDetails(studyPK, patientPK);
			if(patProtBean.getPatProtId() == 0)
			{
				addIssue(new Issue(IssueTypes.PATIENT_NOT_ONSTUDY, "Given patient is not linked to study provided")); 
				throw new OperationException(); 
			}else{
				patprotPK = patProtBean.getPatProtId();
			}
			
			EventdefDao eventDao = new EventdefDao();
			try{
			SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
	        Date parsed = format.parse(startDate);
	        java.sql.Date sqlDate = new java.sql.Date(parsed.getTime());
	        Integer days = null;
	        PatProtJB patPJB=new PatProtJB();
	        patPJB.setPatProtId(patprotPK);
	        patPJB.getPatProtDetails();
	        patPJB.setPatProtStartDt(startDate);
			patPJB.setPatProtProtocolId(StringUtil.integerToString(calPK));
			patPJB.setPatProtStat("1"); //Stands for active
			patPJB.setModifiedBy(StringUtil.integerToString(callingUser.getUserId()));
			patPJB.setIpAdd(IP_ADDRESS_FIELD_VALUE);
			patPJB.setPatProtStartDay(null);
	 	    patPJB.updatePatProt();
			eventDao.GenerateSchedule(calPK, patientPK, sqlDate, patprotPK, days, StringUtil.integerToString(callingUser.getUserId()), IP_ADDRESS_FIELD_VALUE);
			}catch(ParseException pe){
				pe.printStackTrace();
			}
		}
		
		MPatientSchedule mPatSched = new MPatientSchedule();
		List<MPatientSchedule> mPatSchedList = new ArrayList<MPatientSchedule>();
		PatientIdentifier patIdent = new PatientIdentifier();
		CalendarIdentifier calIdent = new CalendarIdentifier();
		patIdent.setPK(patientPK);
		patIdent.setOID(objectMapService.getOrCreateObjectMapFromPK("er_per", patientPK).getOID());
		calIdent.setPK(calPK);
		calIdent.setOID(objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY_CALENDAR, calPK).getOID());
		MPatientScheduleList mPatSchList = new MPatientScheduleList();
		mPatSched.setPatientIdentifier(patIdent);
		mPatSched.setCalendarIdentifier(calIdent);
		mPatSchedList.add(mPatSched);
		mPatSchList.setmPatientSchedule(mPatSchedList);
		
		return mPatSchList;
	}
	
	//Tarandeep Singh Bali
	
	public EventStatusHistory getEventStatusHistory(EventIdentifier eventIdentifier, String sortBy, String orderBy) throws OperationException{
		
		EventStatusHistory eventStatusHistory = new EventStatusHistory();
		Integer eventPK = 0;
		try{
			
			GroupAuthModule groupAuth = 
					new GroupAuthModule(callingUser, groupRightsAgent);
				Integer manageProtocolPriv = 
					groupAuth.getAppManagePatientsPrivileges();
				
				boolean hasViewManageProt = 
					GroupAuthModule.hasViewPermission(manageProtocolPriv);
				
				if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
				if (!hasViewManageProt){
					addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to view Patient data"));
					throw new AuthorizationException("User Not Authorized to view Patient data");
				}
			
			if(eventIdentifier != null && ((eventIdentifier.getOID() != null && eventIdentifier.getOID().length() > 0)
			   || (eventIdentifier.getPK() != null && eventIdentifier.getPK() >0))){
				
				if(eventIdentifier.getPK() != null && eventIdentifier.getPK() > 0)
				{
					eventPK = eventIdentifier.getPK();
					if(eventPK == null || eventPK == 0)
					{ 
						addIssue(new Issue(IssueTypes.EVENT_NOT_FOUND,				
							"Event not found for Event Identifier : PK " 
						     + eventIdentifier.getPK())); 
						throw new OperationException("Event not found for Event Identifier : PK " 
						     + eventIdentifier.getPK()); 
					}
					
				}else{
					
					eventPK = objectMapService.getObjectPkFromOID(eventIdentifier.getOID()); 
					if(eventPK == null || eventPK == 0)
					{ 
						addIssue(new Issue(IssueTypes.EVENT_NOT_FOUND,				
								"Event not found for Event Identifier : OID " 
									     + eventIdentifier.getOID())); 
									throw new OperationException("Event not found for Event Identifier : OID " 
									     + eventIdentifier.getOID()); 
					}
				}
			}
			else{
				addIssue(new Issue(IssueTypes.EVENT_IDENTIFIER_INAVLID, "Valid Event Identifier is required to update the Event Status")); 
				throw new OperationException("Valid Event Identifier is required to update the Event Status"); 
			}
			 
			Integer studyPK = PatientScheduleDAO.getStudyPKFromEventPK(eventPK);
			
			TeamAuthModule teamAuthModule = new TeamAuthModule(callingUser.getUserId(), studyPK);
			int studyTeamPrivileges = teamAuthModule.getStudyTeamPrivileges();
			if (!TeamAuthModule.hasViewPermission(studyTeamPrivileges)){
				addIssue(
						new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, 
								"User does not have view permission to view Study Team"));
				throw new AuthorizationException("User does not have view permission to view Study Team");
			}
			
			int patientManageViewPrivileges = teamAuthModule.getPatientManagePrivileges();
			if (!TeamAuthModule.hasViewPermission(patientManageViewPrivileges)){
				addIssue(
						new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION,
								"User does not have view permission to view the Patient data"));
				throw new AuthorizationException("User does not have the view permission to view the Patient data");
			}
			
			if(sortBy == null){
				sortBy = "1";
			}else if(sortBy != null){
				if(sortBy.compareTo("eventStatus") == 0){
					sortBy = "3";
				}
				else if(sortBy.compareTo("startDate") == 0){
					sortBy = "4";
				}
				else if(sortBy.compareTo("endDate") == 0){
					sortBy = "5";
				}
				else if(sortBy.compareTo("notes") == 0){
					sortBy = "6";
				}
				else if(sortBy.compareTo("firstName") == 0){
					sortBy = "9";
				}
				else if(sortBy.compareTo("lastName") == 0){
					sortBy = "10";
				}
				else{
					addIssue(new Issue(IssueTypes.INVALID_SORT_BY_OPTION,				
							"Sort By option not found for : " + sortBy)); 
					throw new OperationException("Sort By option not found for : " + sortBy);
				}
			}
			
			if(orderBy == null){
				orderBy = "asc";
			}else if(orderBy != null){
				if(orderBy.compareTo("ASCENDING") == 0){
					orderBy = "asc";
				}
				else if(orderBy.compareTo("DESCENDING") == 0){
					orderBy = "desc";
				}
				else{
					addIssue(new Issue(IssueTypes.INVALID_ORDER_BY_OPTION,				
							"Order By option not found for : " + orderBy)); 
					throw new OperationException("Order By option not found for : " + orderBy);
				}
			}
			
			
			eventStatusHistory = PatientScheduleDAO.getEventStatusHistory(eventPK, sortBy, orderBy);
			
		}	catch(OperationException e){
			//sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("PatientScheduleServiceImpl create", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("PatientScheduleServiceImpl create", t);
			throw new OperationException(t);
		}
		return eventStatusHistory;
	}
	
	private Integer locateStudyPK(StudyIdentifier studyIdent) 
	throws OperationException{
		//Virendra:#6123,added OID in if clause
		if(studyIdent==null || (StringUtil.isEmpty(studyIdent.getOID()) 
				&& (studyIdent.getPK()==null || studyIdent.getPK()<=0) 
				&& StringUtil.isEmpty(studyIdent.getStudyNumber())))
		{
		addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid studydentifier is required to addMilestones"));
		throw new OperationException();
		}
		
		Integer studyPK =
				ObjectLocator.studyPKFromIdentifier(this.callingUser, studyIdent,objectMapService);
		if (studyPK == null || studyPK == 0){
			StringBuffer errorMessage = new StringBuffer("Study not found for:"); 
			if(studyIdent.getOID() != null && studyIdent.getOID().length() > 0 )
			{
				errorMessage.append(" OID: " + studyIdent.getOID()); 
			}
			if(studyIdent.getStudyNumber() != null && studyIdent.getStudyNumber().length() > 0)
			{
				errorMessage.append( " Study Number: " + studyIdent.getStudyNumber()); 
			}
			addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, errorMessage.toString()));
			throw new OperationException();
		}
		return studyPK;
	}
}