package com.velos.services.patientschedule;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import com.velos.browser.Patient;
import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.business.common.StudyDao;
import com.velos.eres.business.common.TeamDao;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.study.StudyJB;
import com.velos.esch.business.common.SchCodeDao;
import com.velos.esch.business.eventdef.impl.EventdefBean;
import com.velos.esch.web.eventassoc.EventAssocJB;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.Code;
import com.velos.services.model.Event;
import com.velos.services.model.EventCRF;
import com.velos.services.model.EventCRFs;
import com.velos.services.model.EventStatus;
import com.velos.services.model.EventStatusDetails;
import com.velos.services.model.EventStatusHistory;
import com.velos.services.model.EventStatusIdentifier;
import com.velos.services.model.EventStatuses;
import com.velos.services.model.Events;
import com.velos.services.model.FormIdentifier;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientProtocolIdentifier;
import com.velos.services.model.PatientSchedule;
import com.velos.services.model.PatientScheduleSummary;
import com.velos.services.model.PatientSchedules;
import com.velos.services.model.ScheduleEventIdentifier;
import com.velos.services.model.SitesOfService;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyPatient;
import com.velos.services.model.User;
import com.velos.services.model.Visit;
import com.velos.services.model.VisitIdentifier;
import com.velos.services.model.Visits;
import com.velos.services.patientdemographics.PatientDemographicsDAO;
import com.velos.services.util.ServicesUtil;

/**
 * Data access object dealing with 
 * @author Tarandeep Singh Bali
 *
 */
public class PatientScheduleDAO extends CommonDAO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(PatientDemographicsDAO.class);	
	private static String getPatScheduleSql="Select distinct(p.PATPROT_START), p.PK_PATPROT, p.PATPROT_STAT," +
	                                        "e.name as PROTOCOL_NAME, pat.per_code as PERSON_CODE," +
                                            "st.STUDY_NUMBER from ER_PATPROT p, EVENT_ASSOC e, ER_PER pat, ER_STUDY st" +
	                                        " where p.fk_per = ? and p.fk_study = ? and p.fk_protocol=e.event_id" +
                                            " and pat.pk_per = p.fk_per and st.pk_study = p.fk_study";
	private static String getPatientScheduleSql="select  distinct(p.PATPROT_START), p.PATPROT_STAT, pat.per_code, pat.pk_per, site.pk_site, st.pk_study, st.study_number, " +
			                                    "(select e.name from event_assoc e where p.fk_protocol = e.event_id) as PROTOCOL_NAME, " + 
			                                    "v.pk_protocol_visit, v.VISIT_NAME, v.visit_no, v.win_before_number, v.win_before_unit, v.win_after_number, v.win_after_unit, " +
			                                    "s.event_id, e.name as EVENT_NAME, e.description, " +
			                                    "s.start_date_time, s.actual_schdate,(select min(sev.actual_schdate) from sch_events1 sev where sev.FK_VISIT=s.fk_visit and s.FK_PATPROT=sev.FK_PATPROT and sev.SESSION_ID=s.session_id) actual_schdatesort, " +
			                                    "TO_NUMBER(e.fuzzy_period) as EVENT_WIN_BEFORE, e.event_durationbefore, e.event_fuzzyafter as EVENT_WIN_AFTER, e.event_durationafter, " +
			                                    "code.codelst_desc as Event_Status, code.codelst_subtyp, " +
			                                    "s.service_site_id, (select site.site_name from er_site site where s.service_site_id = site.pk_site) as SITE_OF_SERVICE, " +
                                                "(select code.codelst_desc from sch_codelst code where s.fk_codelst_covertype = code.pk_codelst) as COVERAGE_DESC, " +
                                                "(select code.codelst_subtyp from sch_codelst code where s.fk_codelst_covertype = code.pk_codelst) as COVERAGE_CODE, " +
                                                "(select code.codelst_type from sch_codelst code where s.fk_codelst_covertype = code.pk_codelst) as COVERAGE_TYPE, " +
			                                    "c.fk_form, (Select f.form_name from er_formlib f where c.fk_form = f.pk_formlib) as FORM_NAME, estat.EVENTSTAT_NOTES NOTES,estat.EVENTSTAT_DT EVENTSTAT_DT,v.displacement,e.cost " +
			                                    "from er_patprot p, sch_events1 s, sch_protocol_visit v, event_assoc e, ESCH.sch_event_crf c, " + 
			                                    "er_per pat, er_site site, er_study st, sch_eventstat estat, sch_codelst code " +
			                                    "where p.pk_patprot = ? and p.pk_patprot = s.fk_patprot and s.fk_visit = v.pk_protocol_visit " +
			                                    "and s.fk_assoc = e.event_id and s.fk_assoc = c.fk_event(+) and p.fk_per = pat.pk_per " +
			                                    "and pat.fk_site = site.pk_site and p.fk_study = st.pk_study and s.event_id = estat.fk_event " +
			                                    "and estat.eventstat_enddt is null and estat.eventstat = code.pk_codelst ";
private static String getPatientScheduleVisitsSql="SELECT ev.FK_VISIT, v.VISIT_NAME, v.VISIT_NO, CASE (MIN (NVL(ev.ACTUAL_SCHDATE, to_date('01/01/3000', 'mm/dd/yyyy'))))  WHEN to_date('01/01/3000', 'mm/dd/yyyy') THEN NULL ELSE MIN (NVL(ev.ACTUAL_SCHDATE, to_date('01/01/3000', 'mm/dd/yyyy'))) END ACTUAL_SCHDATE, " +
												" CASE (MIN (NVL(ev.START_DATE_TIME, to_date('01/01/3000', 'mm/dd/yyyy')))) WHEN to_date('01/01/3000', 'mm/dd/yyyy') THEN NULL ELSE MIN (NVL(ev.START_DATE_TIME, to_date('01/01/3000', 'mm/dd/yyyy'))) END START_DATE_TIME, " + 
    											" v.WIN_BEFORE_NUMBER, v.WIN_BEFORE_UNIT, v.WIN_AFTER_NUMBER, v.WIN_AFTER_UNIT,  COUNT(ev.EVENT_ID) EVT_COUNT, (SELECT COUNT(*)  FROM sch_events1 ev1  WHERE ev1.FK_PATPROT = ?  AND ev1.FK_VISIT     = ev.FK_VISIT  AND ev1.ISCONFIRMED  =    (SELECT PK_CODELST " +
    											" FROM SCH_CODELST  WHERE CODELST_TYPE = 'eventstatus'  AND CODELST_SUBTYP = 'ev_done' )) DONE_COUNT, CASE  (SELECT COUNT(*)  FROM sch_events1 ev1  WHERE ev1.FK_PATPROT = ?  AND ev1.FK_VISIT  = ev.FK_VISIT AND ev1.ISCONFIRMED  = " +
    											" (SELECT PK_CODELST  FROM SCH_CODELST WHERE CODELST_TYPE = 'eventstatus' AND CODELST_SUBTYP = 'ev_notdone' ) AND TRUNC(ev1.ACTUAL_SCHDATE - sysdate) < 0)  WHEN 0 THEN 0 ELSE 1 END IS_PAST,fk_per,pk_patprot,PATPROT_START,PATPROT_STAT,v.fk_protocol,p.fk_study FROM SCH_EVENTS1 ev, er_patprot p, SCH_PROTOCOL_VISIT v  " +
    											" WHERE ev.FK_VISIT = v.PK_PROTOCOL_VISIT AND p.pk_patprot=ev.fk_patprot AND ev.FK_PATPROT = ? GROUP BY ev.FK_VISIT, v.VISIT_NO, v.VISIT_NAME, v.WIN_BEFORE_NUMBER, v.WIN_BEFORE_UNIT, v.WIN_AFTER_NUMBER, v.WIN_AFTER_UNIT,fk_per,pk_patprot,PATPROT_START,PATPROT_STAT,v.fk_protocol,p.fk_study ORDER BY ACTUAL_SCHDATE, v.VISIT_NO, ev.FK_VISIT nulls last  ";

/*	private static String getCurrentPatientScheduleSql="select  distinct(p.PATPROT_START), p.PATPROT_STAT, p.pk_patprot, pat.per_code, pat.pk_per, site.pk_site, st.pk_study, st.study_number, " +
                                                       "(select e.name from event_assoc e where p.fk_protocol = e.event_id) as PROTOCOL_NAME, " + 
                                                       "v.pk_protocol_visit, v.VISIT_NAME, v.visit_no, v.win_before_number, v.win_before_unit, v.win_after_number, v.win_after_unit, " +
                                                       "s.event_id, e.name as EVENT_NAME, e.description, " +
                                                       "s.start_date_time, s.actual_schdate, " +
                                                       "TO_NUMBER(e.fuzzy_period) as EVENT_WIN_BEFORE, e.event_durationbefore, e.event_fuzzyafter as EVENT_WIN_AFTER, e.event_durationafter, " +
                                                       "code.codelst_desc as Event_Status, code.codelst_subtyp, " +
                                                       "s.service_site_id, (select site.site_name from er_site site where s.service_site_id = site.pk_site) as SITE_OF_SERVICE, " +
                                                       "(select code.codelst_desc from sch_codelst code where s.fk_codelst_covertype = code.pk_codelst) as COVERAGE_DESC, " +
                                                       "(select code.codelst_subtyp from sch_codelst code where s.fk_codelst_covertype = code.pk_codelst) as COVERAGE_CODE, " +
                                                       "(select code.codelst_type from sch_codelst code where s.fk_codelst_covertype = code.pk_codelst) as COVERAGE_TYPE, " +
                                                       "c.fk_form, (Select f.form_name from er_formlib f where c.fk_form = f.pk_formlib) as FORM_NAME " +
                                                       "from er_patprot p, sch_events1 s, sch_protocol_visit v, event_assoc e, ESCH.sch_event_crf c, " + 
                                                       "er_per pat, er_site site, er_study st, sch_eventstat estat, sch_codelst code " +
                                                       "where p.fk_per = ? and p.fk_study = ? and p.patprot_stat = 1 and p.pk_patprot = s.fk_patprot and s.fk_visit = v.pk_protocol_visit " +
                                                       "and s.fk_assoc = e.event_id and s.fk_assoc = c.fk_event(+) and p.fk_per = pat.pk_per " +
                                                       "and pat.fk_site = site.pk_site and p.fk_study = st.pk_study and s.event_id = estat.fk_event " +
                                                       "and estat.eventstat_enddt is null and estat.eventstat = code.pk_codelst order by v.visit_no, s.event_id";*/
	private static String getCurrentPatientScheduleSql="select  distinct(p.PATPROT_START), p.PATPROT_STAT, p.pk_patprot, pat.per_code, pat.pk_per, site.pk_site, st.pk_study, st.study_number, " +
            "(select e.name from event_assoc e where p.fk_protocol = e.event_id) as PROTOCOL_NAME, " + 
            "v.pk_protocol_visit, v.VISIT_NAME, v.visit_no, v.win_before_number, v.win_before_unit, v.win_after_number, v.win_after_unit, " +
            "s.event_id, e.name as EVENT_NAME, e.description, " +
            "s.start_date_time, s.actual_schdate, " +
            "TO_NUMBER(e.fuzzy_period) as EVENT_WIN_BEFORE, e.event_durationbefore, e.event_fuzzyafter as EVENT_WIN_AFTER, e.event_durationafter, " +
            "code.codelst_desc as Event_Status, code.codelst_subtyp, " +
            "s.service_site_id, (select site.site_name from er_site site where s.service_site_id = site.pk_site) as SITE_OF_SERVICE, " +
            "(select code.codelst_desc from sch_codelst code where s.fk_codelst_covertype = code.pk_codelst) as COVERAGE_DESC, " +
            "(select code.codelst_subtyp from sch_codelst code where s.fk_codelst_covertype = code.pk_codelst) as COVERAGE_CODE, " +
            "(select code.codelst_type from sch_codelst code where s.fk_codelst_covertype = code.pk_codelst) as COVERAGE_TYPE, " +
            "c.fk_form as fk_form, (Select f.form_name from er_formlib f where c.fk_form = f.pk_formlib) as FORM_NAME, s.notes, estat.EVENTSTAT_DT " +
            "from er_patprot p, sch_events1 s, sch_protocol_visit v, event_assoc e, ESCH.sch_event_crf c, " + 
            "er_per pat, er_site site, er_study st, sch_eventstat estat, sch_codelst code " +
            "where p.fk_per = ? and p.fk_study = ? and p.patprot_stat = 1 and p.pk_patprot = s.fk_patprot and s.fk_visit = v.pk_protocol_visit " +
            "and s.fk_assoc = e.event_id and s.fk_assoc = c.fk_event(+) and p.fk_per = pat.pk_per " +
            "and pat.fk_site = site.pk_site and p.fk_study = st.pk_study and s.event_id = estat.fk_event " +
            "and estat.eventstat_enddt is null and estat.eventstat = code.pk_codelst ";
	
	private static String getSitesOfServiceSql = "select s.pk_site, s.site_name from er_site s, er_codelst c where s.fk_codelst_type = c.pk_codelst " +
		                                      "and c.codelst_type = 'site_type' and c.codelst_custom_col = 'service' and s.fk_account= ?";
	
	private static String getEventStatCodeSql = "SELECT pk_codelst FROM SCH_CODELST WHERE codelst_subtyp = 'ev_notdone'" +
			"";
	
	private static String getEventStatusHistorySql = "select pk_eventstat, eventstat,(select codelst_desc from sch_codelst where pk_codelst = eventstat) as event_status, " + 
                                                      "eventstat_dt, eventstat_enddt, eventstat_notes, creator as creator_pk, " +
                                                      "(select (usr_firstname || ' ' || usr_lastname) from er_user where pk_user = sch_eventstat.creator) as creator_name, " +
                                                      "(select usr_firstname from er_user where pk_user = sch_eventstat.creator) as fname, " + 
                                                      "(select usr_lastname from er_user where pk_user = sch_eventstat.creator) as lname " +
                                                      "from sch_eventstat where fk_event = ?"; 
	
	protected static ResponseHolder response = new ResponseHolder();
	Issue issue = new Issue();
	SchCodeDao schCodeDao = new SchCodeDao(); 
	//EventStatus evenStatus = new EventStatus();
	static String siteName = "";
	/**
	 * 
	 * @param personPk
	 * @param studyPk
	 * @return
	 * @throws OperationException
	 */
	
	  public enum WindowUnit {

		    H,
		    D,
		    W,
		    M,
		    Y
		  }
	
	public static PatientSchedules getPatientScheduleList(int personPk, int studyPk) throws OperationException {
		
		
		PreparedStatement pstmt = null;
		Connection conn = null;
		PatientSchedules patSchedules = new PatientSchedules();
		
	//	ArrayList<Integer> lstPatProtPK = new ArrayList<Integer>();
	//	ArrayList<PatientSchedule> lstPatientSchedule = new ArrayList<PatientSchedule>();
		
		try{
			
			conn = getConnection();

			if (logger.isDebugEnabled()) logger.debug(" sql:" + getPatScheduleSql);
			
			pstmt = conn.prepareStatement(getPatScheduleSql);
			pstmt.setInt(1, personPk);
			pstmt.setInt(2, studyPk);
			ResultSet rs = pstmt.executeQuery();
			ObjectMapService objectMapService = ServicesUtil.getObjectMapService(); 
			int ctr = 0; 
			
			while (rs.next()) {
				
				if(ctr == 0)
				{
					PatientIdentifier patientIdentifier = new PatientIdentifier();
					//create or get person object map for studyPatient
	
					ObjectMap obj = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PERSON, personPk);
					String strPerOID = obj.getOID();
					
					patientIdentifier.setPatientId(rs.getString("PERSON_CODE"));
					patientIdentifier.setOID(strPerOID);
					patientIdentifier.setPK(StringUtil.stringToInteger(rs.getString("PERSON_CODE")));
					patSchedules.setPatientIdentifier(patientIdentifier);
					
					
					StudyIdentifier studyIdentifier = new StudyIdentifier();
					
					ObjectMap objMapStudy = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY, studyPk);
					String strStudyOID = objMapStudy.getOID();
					studyIdentifier.setStudyNumber(rs.getString("STUDY_NUMBER"));
					studyIdentifier.setOID(strStudyOID);
					studyIdentifier.setPK(studyPk);
					patSchedules.setStudyIdentifier(studyIdentifier);
				}
				
				ctr++; 
				//Fixed:# 6059, reopened issue. initial assign null
				//and instantiate if schedule exists.
				PatientScheduleSummary patSchedule = new PatientScheduleSummary();
				
				StudyPatient studyPatient = new StudyPatient();
			
				
				//studyPatient.setStudyPatEnrollDate(rs.getDate("PATPROT_START"));
				//Viendra:#6061, for more than one schedule names
				patSchedule.setCalendarName(rs.getString("PROTOCOL_NAME")+","+(rs.getDate("PATPROT_START").toString()));
				patSchedule.setStartDate(rs.getDate("PATPROT_START"));
				patSchedule.setCurrent(rs.getString("PATPROT_STAT").equals("1") ? true : false);
				ObjectMap mapScheduleOID = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PATIENT_SCHEDULE, rs.getInt("PK_PATPROT"));
				PatientProtocolIdentifier patProtID = new PatientProtocolIdentifier();
				patProtID.setOID(mapScheduleOID.getOID());
				patProtID.setPK( rs.getInt("PK_PATPROT"));
				patSchedule.setScheduleIdentifier(patProtID);
				patSchedules.addPatientSchedule(patSchedule);
		
			}
			
			return patSchedules;
		}
		catch(Throwable t){
	
			t.printStackTrace();
			
			throw new OperationException();
			
		}
		finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}
		
	}
	
	
	public static PatientSchedule getPatientSchedule(int patProtPK,int visitPK,String visitName) throws OperationException{
		
		PreparedStatement pstmt = null;
		Connection conn = null;
		PatientSchedule patSchedule = null;
		ObjectMapService objectMapService = ServicesUtil.getObjectMapService();
		String orderBy=" order by actual_schdatesort,v.displacement,v.visit_no,e.cost ";
		StringBuffer sql = new StringBuffer();
		sql = sql.append(getPatientScheduleSql);
		if(visitPK>0)
			sql = sql.append(" and pk_protocol_visit = "+visitPK);
		if(visitPK<=0 && visitName!=null && !"".equals(visitName.trim()))
			sql = sql.append(" and VISIT_NAME = '"+visitName+"'");
		
		sql = sql.append(orderBy);
		try{
			conn = getConnection();
			if(logger.isDebugEnabled()) logger.debug("sql:" + sql.toString());
			pstmt = conn.prepareStatement(sql.toString());
			pstmt.setInt(1, patProtPK);
			ResultSet rs = pstmt.executeQuery();
			Visits visits = new Visits();
			Visit visit = new Visit();
			Events events = new Events();
			Event event = new Event(); 
			PatientScheduleSummary patientScheduleSummary = new PatientScheduleSummary();
			patSchedule = new PatientSchedule();
			PatientProtocolIdentifier patientProtocolIdentifier = new PatientProtocolIdentifier();
			PatientIdentifier patientIdentifier = new PatientIdentifier();
			StudyIdentifier studyIdentifier = new StudyIdentifier();
			ObjectMap mapOID = objectMapService.getOrCreateObjectMapFromPK(objectMapService.PERSISTENCE_UNIT_PATIENT_SCHEDULE, patProtPK); 
			patientProtocolIdentifier.setOID(mapOID.getOID());
			patientProtocolIdentifier.setPK(patProtPK);
			int ctr = 0;
			int visitCtr = 0;
			int curEventPK = 0;
			int visitWinBefore = 0;
			int visitWinAfter = 0;
			int eventWinAfter = 0;
			int eventWinBefore = 0;
            String scheduleDate = "";
            String suggestedDate = "";
			int visitFlag = 0;//Bug Fix : 12511 & 12407
			String visitWinBeforeUnit = "";
			String visitWinAfterUnit = "";
			String eventWinBeforeUnit = "";
			String eventWinAfterUnit = "";
            List<String> visitScheduleList = new ArrayList<String>();
            List<String> visitSuggList = new ArrayList<String>();
			while(rs.next()){

				VisitIdentifier visitIdentifier = new VisitIdentifier();
				if ((visitCtr == 0 || rs.getInt("PK_PROTOCOL_VISIT") != visitCtr)){
					visitSuggList = new ArrayList<String>();
					visitScheduleList = new ArrayList<String>();
					if(visitFlag > 0){//Bug Fix : 12511 & 12407
			            visit.setEvents(events);
			            visits.addVisit(visit);
					}
					visitFlag++;
					visit = new Visit();
					events = new Events();
					curEventPK = 0; 
					visitCtr = rs.getInt("PK_PROTOCOL_VISIT");
					visit.setVisitName(rs.getString("VISIT_NAME"));
					ObjectMap mapVisitOID = objectMapService.getOrCreateObjectMapFromPK(objectMapService.PERSISTENCE_UNIT_VISIT, rs.getInt("PK_PROTOCOL_VISIT"));
					visitIdentifier.setOID(mapVisitOID.getOID());
					visitIdentifier.setPK(rs.getInt("PK_PROTOCOL_VISIT"));
					visit.setVisitIdentifier(visitIdentifier);
					visitWinBefore = rs.getInt("WIN_BEFORE_NUMBER");
					visitWinAfter = rs.getInt("WIN_AFTER_NUMBER");
					if((visitWinBefore != 0) || (visitWinAfter != 0)){
						visitWinBeforeUnit = rs.getString("WIN_BEFORE_UNIT");
						visitWinAfterUnit = rs.getString("WIN_AFTER_UNIT");
						Calendar currBefore = Calendar.getInstance();
						Calendar currAfter = Calendar.getInstance();
						currBefore.setTime(rs.getDate("START_DATE_TIME"));
						currAfter.setTime(rs.getDate("START_DATE_TIME"));
						visit.setVisitWindow(PatientScheduleDAO.dateWindow(visitWinBeforeUnit, currBefore, visitWinBefore, visitWinAfterUnit, currAfter, visitWinAfter));
					}
			        
				}
			
				if(curEventPK == 0 || rs.getInt("EVENT_ID") != curEventPK)
				{
					curEventPK = rs.getInt("EVENT_ID"); 
					event = new Event();
					EventStatus eventStatus = new EventStatus();
					Code eventStatusCode = new Code();
					Code coverageType = new Code();
					ScheduleEventIdentifier scheduleEventIdentifier = new ScheduleEventIdentifier();
					OrganizationIdentifier organizationIdentifier = new OrganizationIdentifier();
				
					EventCRFs eventCRFs = new EventCRFs();
					if(rs.getString("ACTUAL_SCHDATE") != null) visitScheduleList.add(rs.getString("ACTUAL_SCHDATE"));//Bug Fix : 12406 && 12431
					if(rs.getString("ACTUAL_SCHDATE") != null) visitSuggList.add(rs.getString("START_DATE_TIME"));//BUg Fix : 12406 && 12431
					event.setEventName(rs.getString("EVENT_NAME"));
					event.setDescription(rs.getString("DESCRIPTION"));
					if(rs.getString("ACTUAL_SCHDATE") != null){
						event.setEventSuggestedDate(rs.getString("START_DATE_TIME"));
						event.setEventScheduledDate(rs.getString("ACTUAL_SCHDATE"));
					}
					else{
						event.setEventSuggestedDate("Not Defined");
						event.setEventScheduledDate("Not Defined");
					}
					eventWinBefore = rs.getInt("EVENT_WIN_BEFORE");
					eventWinAfter = rs.getInt("EVENT_WIN_AFTER");
					if((eventWinBefore != 0) || (eventWinAfter != 0)){
						eventWinBeforeUnit = rs.getString("EVENT_DURATIONBEFORE");
						eventWinAfterUnit = rs.getString("EVENT_DURATIONAFTER");
						Calendar currBefore = Calendar.getInstance();
						Calendar currAfter = Calendar.getInstance();
						currBefore.setTime(rs.getDate("START_DATE_TIME"));
						currAfter.setTime(rs.getDate("START_DATE_TIME"));
						System.out.println("currBefore value while calling the case :::" + currBefore.getTime());
						event.setEventWindow(PatientScheduleDAO.dateWindow(eventWinBeforeUnit, currBefore, eventWinBefore, eventWinAfterUnit, currAfter, eventWinAfter));
					}
					eventStatusCode.setCode(rs.getString("CODELST_SUBTYP"));
					eventStatusCode.setDescription(rs.getString("EVENT_STATUS"));
					eventStatus.setEventStatusCode(eventStatusCode);
					eventStatus.setStatusValidFrom(rs.getDate("EVENTSTAT_DT"));
					eventStatus.setNotes(rs.getString("NOTES"));
					event.setEventStatus(eventStatus);
					//Bug Fix : 12367
					if(rs.getInt("SERVICE_SITE_ID") > 0){
						ObjectMap mapSiteOID = objectMapService.getOrCreateObjectMapFromPK(objectMapService.PERSISTENCE_UNIT_ORGANIZATION, rs.getInt("SERVICE_SITE_ID"));
						organizationIdentifier.setOID(mapSiteOID.getOID());
						organizationIdentifier.setPK(rs.getInt("SERVICE_SITE_ID"));//Bug Fix : 16297
						organizationIdentifier.setSiteName(rs.getString("SITE_OF_SERVICE"));
						event.setSiteOfService(organizationIdentifier);
					}
					coverageType.setCode(rs.getString("COVERAGE_CODE"));
					coverageType.setDescription(rs.getString("COVERAGE_DESC"));
					coverageType.setType(rs.getString("COVERAGE_TYPE"));
					event.setCoverageType(coverageType);
					ObjectMap mapEventOID = objectMapService.getOrCreateObjectMapFromPK(objectMapService.PERSISTENCE_UNIT_EVENT, rs.getInt("EVENT_ID"));
					scheduleEventIdentifier.setOID(mapEventOID.getOID());
					scheduleEventIdentifier.setPK(rs.getInt("EVENT_ID"));
					event.setScheduleEventIdentifier(scheduleEventIdentifier);
					event.setEventCRFs(eventCRFs);
					events.addEvent(event);

				}
				
				
				if(rs.getInt("FK_FORM") != 0){
					FormIdentifier formIdentifier = new FormIdentifier();
					EventCRF eventCRF = new EventCRF();
					ObjectMap mapFormOID = objectMapService.getOrCreateObjectMapFromPK(objectMapService.PERSISTENCE_UNIT_FORM_LIBRARY, rs.getInt("FK_FORM"));
					formIdentifier.setOID(mapFormOID.getOID());
					formIdentifier.setPK( rs.getInt("FK_FORM"));
					eventCRF.setFormIdentifier(formIdentifier);
					eventCRF.setFormName(rs.getString("FORM_NAME"));
					event.getEventCRFs().addEventCRF(eventCRF);
				
				}

				if (ctr == 0){
					OrganizationIdentifier organizationIdentifier = new OrganizationIdentifier();
					patientScheduleSummary.setStartDate(rs.getDate("PATPROT_START"));
					patientScheduleSummary.setCalendarName(rs.getString("PROTOCOL_NAME") + "," + rs.getString("PATPROT_START").toString());
					patientScheduleSummary.setCurrent(rs.getString("PATPROT_STAT").equals("1") ? true:false);
					patientScheduleSummary.setScheduleIdentifier(patientProtocolIdentifier);
					patSchedule.setPatientScheduleSummary(patientScheduleSummary);
					ObjectMap mapPatOID = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PERSON, rs.getInt("PK_PER"));
					ObjectMap mapOrgOID = objectMapService.getOrCreateObjectMapFromPK(objectMapService.PERSISTENCE_UNIT_ORGANIZATION, rs.getInt("PK_SITE"));
					patientIdentifier.setPatientId(rs.getString("PER_CODE"));
					patientIdentifier.setOID(mapPatOID.getOID());
					patientIdentifier.setPK(rs.getInt("PK_PER"));
					organizationIdentifier.setOID(mapOrgOID.getOID());
					organizationIdentifier.setPK(rs.getInt("PK_SITE"));
					patientIdentifier.setOrganizationId(organizationIdentifier);
					patSchedule.setPatientIdentifier(patientIdentifier);
					
					ObjectMap mapStudyOID = objectMapService.getOrCreateObjectMapFromPK(objectMapService.PERSISTENCE_UNIT_STUDY, rs.getInt("PK_STUDY"));
					studyIdentifier.setStudyNumber(rs.getString("STUDY_NUMBER"));
					studyIdentifier.setOID(mapStudyOID.getOID());
					studyIdentifier.setPK(rs.getInt("PK_STUDY"));
					patSchedule.setStudyIdentifier(studyIdentifier);
					
					
				}
				ctr++;			
				//BUg Fix : 12406 && 12431
				if(visitScheduleList.isEmpty())
				{
					suggestedDate = "NOT DEFINED";
					scheduleDate = "NOT DEFINED";
						
				}else{
					suggestedDate = Collections.min(visitSuggList);
					scheduleDate = Collections.min(visitScheduleList);
				}
				visit.setVisitSuggestedDate(suggestedDate);
				visit.setVisitScheduledDate(scheduleDate);//Bug Fix : 12406
			}
			
			//Bug Fix : 12511 & 12407
			if(visitFlag > 0){

				visit.setEvents(events);
	            visits.addVisit(visit);
				patSchedule.setVisits(visits);
			}
			return patSchedule;
			
		}catch(Throwable t){
			
			t.printStackTrace();
			
			throw new OperationException();
		}
		
		finally{
			try{
				if(pstmt!=null){
					pstmt.close();
				}
			}catch(Exception e){
				
			}
			try{
				if(conn!=null){
					conn.close();
				}
			}catch(Exception e){
				
			}
		}
	
		
	}
	
public static PatientSchedule getPatientScheduleVisits(int patProtPK) throws OperationException{
		
		PreparedStatement pstmt = null;
		Connection conn = null;
		PatientSchedule patSchedule = null;
		ObjectMapService objectMapService = ServicesUtil.getObjectMapService();
		
		try{
			
			conn = getConnection();
			if(logger.isDebugEnabled()) logger.debug("sql:" + "getPatientScheduleVisitsSql");
			pstmt = conn.prepareStatement(getPatientScheduleVisitsSql);
			pstmt.setInt(1, patProtPK);
			pstmt.setInt(2, patProtPK);
			pstmt.setInt(3, patProtPK);
			ResultSet rs = pstmt.executeQuery();
			Visits visits = new Visits();
			Visit visit = new Visit();
			//Events events = new Events();
			//Event event = new Event(); 
			PatientScheduleSummary patientScheduleSummary = new PatientScheduleSummary();
			patSchedule = new PatientSchedule();
			PatientProtocolIdentifier patientProtocolIdentifier = new PatientProtocolIdentifier();
			PatientIdentifier patientIdentifier = new PatientIdentifier();
			StudyIdentifier studyIdentifier = new StudyIdentifier();
			ObjectMap mapOID = objectMapService.getOrCreateObjectMapFromPK(objectMapService.PERSISTENCE_UNIT_PATIENT_SCHEDULE, patProtPK); 
			patientProtocolIdentifier.setOID(mapOID.getOID());
			patientProtocolIdentifier.setPK(patProtPK);
			int ctr = 0;
			int visitCtr = 0;
			int curEventPK = 0;
			int visitWinBefore = 0;
			int visitWinAfter = 0;
			int eventWinAfter = 0;
			int eventWinBefore = 0;
            String scheduleDate = "";
            String suggestedDate = "";
			int visitFlag = 0;//Bug Fix : 12511 & 12407
			String visitWinBeforeUnit = "";
			String visitWinAfterUnit = "";
			String eventWinBeforeUnit = "";
			String eventWinAfterUnit = "";
            List<String> visitScheduleList = new ArrayList<String>();
            List<String> visitSuggList = new ArrayList<String>();
			while(rs.next()){

				VisitIdentifier visitIdentifier = new VisitIdentifier();
				if ((visitCtr == 0 || rs.getInt("FK_VISIT") != visitCtr)){
					visitSuggList = new ArrayList<String>();
					visitScheduleList = new ArrayList<String>();
					if(visitFlag > 0){//Bug Fix : 12511 & 12407
			            //visit.setEvents(events);
			            visits.addVisit(visit);
					}
					visitFlag++;
					visit = new Visit();
					//events = new Events();
					curEventPK = 0; 
					visitCtr = rs.getInt("FK_VISIT");
					visit.setVisitName(rs.getString("VISIT_NAME"));
					ObjectMap mapVisitOID = objectMapService.getOrCreateObjectMapFromPK(objectMapService.PERSISTENCE_UNIT_VISIT, rs.getInt("FK_VISIT"));
					visitIdentifier.setOID(mapVisitOID.getOID());
					visitIdentifier.setPK(rs.getInt("FK_VISIT"));
					visit.setVisitIdentifier(visitIdentifier);
					visitWinBefore = rs.getInt("WIN_BEFORE_NUMBER");
					visitWinAfter = rs.getInt("WIN_AFTER_NUMBER");
					if((visitWinBefore != 0) || (visitWinAfter != 0)){
						visitWinBeforeUnit = rs.getString("WIN_BEFORE_UNIT");
						visitWinAfterUnit = rs.getString("WIN_AFTER_UNIT");
						Calendar currBefore = Calendar.getInstance();
						Calendar currAfter = Calendar.getInstance();
						currBefore.setTime(rs.getDate("START_DATE_TIME"));
						currAfter.setTime(rs.getDate("START_DATE_TIME"));
						visit.setVisitWindow(PatientScheduleDAO.dateWindow(visitWinBeforeUnit, currBefore, visitWinBefore, visitWinAfterUnit, currAfter, visitWinAfter));
					}
					if(rs.getString("ACTUAL_SCHDATE") != null) visitScheduleList.add(rs.getString("ACTUAL_SCHDATE"));
					if(rs.getString("ACTUAL_SCHDATE") != null) visitSuggList.add(rs.getString("START_DATE_TIME"));
			        
				}
			
				/*if(curEventPK == 0 || rs.getInt("EVENT_ID") != curEventPK)
				{
					curEventPK = rs.getInt("EVENT_ID"); 
					event = new Event();
					EventStatus eventStatus = new EventStatus();
					Code eventStatusCode = new Code();
					Code coverageType = new Code();
					ScheduleEventIdentifier scheduleEventIdentifier = new ScheduleEventIdentifier();
					OrganizationIdentifier organizationIdentifier = new OrganizationIdentifier();
				
					EventCRFs eventCRFs = new EventCRFs();
					if(rs.getString("ACTUAL_SCHDATE") != null) visitScheduleList.add(rs.getString("ACTUAL_SCHDATE"));//Bug Fix : 12406 && 12431
					if(rs.getString("ACTUAL_SCHDATE") != null) visitSuggList.add(rs.getString("START_DATE_TIME"));//BUg Fix : 12406 && 12431
					event.setEventName(rs.getString("EVENT_NAME"));
					event.setDescription(rs.getString("DESCRIPTION"));
					if(rs.getString("ACTUAL_SCHDATE") != null){
						event.setEventSuggestedDate(rs.getString("START_DATE_TIME"));
						event.setEventScheduledDate(rs.getString("ACTUAL_SCHDATE"));
					}
					else{
						event.setEventSuggestedDate("Not Defined");
						event.setEventScheduledDate("Not Defined");
					}
					eventWinBefore = rs.getInt("EVENT_WIN_BEFORE");
					eventWinAfter = rs.getInt("EVENT_WIN_AFTER");
					if((eventWinBefore != 0) || (eventWinAfter != 0)){
						eventWinBeforeUnit = rs.getString("EVENT_DURATIONBEFORE");
						eventWinAfterUnit = rs.getString("EVENT_DURATIONAFTER");
						Calendar currBefore = Calendar.getInstance();
						Calendar currAfter = Calendar.getInstance();
						currBefore.setTime(rs.getDate("START_DATE_TIME"));
						currAfter.setTime(rs.getDate("START_DATE_TIME"));
						System.out.println("currBefore value while calling the case :::" + currBefore.getTime());
						event.setEventWindow(PatientScheduleDAO.dateWindow(eventWinBeforeUnit, currBefore, eventWinBefore, eventWinAfterUnit, currAfter, eventWinAfter));
					}
					eventStatusCode.setCode(rs.getString("CODELST_SUBTYP"));
					eventStatusCode.setDescription(rs.getString("EVENT_STATUS"));
					eventStatus.setEventStatusCode(eventStatusCode);
					eventStatus.setStatusValidFrom(rs.getDate("EVENTSTAT_DT"));
					eventStatus.setNotes(rs.getString("NOTES"));
					event.setEventStatus(eventStatus);
					//Bug Fix : 12367
					if(rs.getInt("SERVICE_SITE_ID") > 0){
						ObjectMap mapSiteOID = objectMapService.getOrCreateObjectMapFromPK(objectMapService.PERSISTENCE_UNIT_ORGANIZATION, rs.getInt("SERVICE_SITE_ID"));
						organizationIdentifier.setOID(mapSiteOID.getOID());
						organizationIdentifier.setPK(rs.getInt("SERVICE_SITE_ID"));//Bug Fix : 16297
						organizationIdentifier.setSiteName(rs.getString("SITE_OF_SERVICE"));
						event.setSiteOfService(organizationIdentifier);
					}
					coverageType.setCode(rs.getString("COVERAGE_CODE"));
					coverageType.setDescription(rs.getString("COVERAGE_DESC"));
					coverageType.setType(rs.getString("COVERAGE_TYPE"));
					event.setCoverageType(coverageType);
					ObjectMap mapEventOID = objectMapService.getOrCreateObjectMapFromPK(objectMapService.PERSISTENCE_UNIT_EVENT, rs.getInt("EVENT_ID"));
					scheduleEventIdentifier.setOID(mapEventOID.getOID());
					scheduleEventIdentifier.setPK(rs.getInt("EVENT_ID"));
					event.setScheduleEventIdentifier(scheduleEventIdentifier);
					event.setEventCRFs(eventCRFs);
					events.addEvent(event);

				}*/
				
				
				/*if(rs.getInt("FK_FORM") != 0){
					FormIdentifier formIdentifier = new FormIdentifier();
					EventCRF eventCRF = new EventCRF();
					ObjectMap mapFormOID = objectMapService.getOrCreateObjectMapFromPK(objectMapService.PERSISTENCE_UNIT_FORM_LIBRARY, rs.getInt("FK_FORM"));
					formIdentifier.setOID(mapFormOID.getOID());
					formIdentifier.setPK( rs.getInt("FK_FORM"));
					eventCRF.setFormIdentifier(formIdentifier);
					eventCRF.setFormName(rs.getString("FORM_NAME"));
					event.getEventCRFs().addEventCRF(eventCRF);
				
				}*/

				if (ctr == 0){
					//OrganizationIdentifier organizationIdentifier = new OrganizationIdentifier();
					patientScheduleSummary.setStartDate(rs.getDate("PATPROT_START"));
					EventAssocJB eventAssoc = new EventAssocJB();
					eventAssoc.setEvent_id(rs.getInt("FK_PROTOCOL"));
					eventAssoc.getEventAssocDetails();
					patientScheduleSummary.setCalendarName(eventAssoc.getName() + "," + rs.getString("PATPROT_START").toString());
					patientScheduleSummary.setCurrent(rs.getString("PATPROT_STAT").equals("1") ? true:false);
					patientScheduleSummary.setScheduleIdentifier(patientProtocolIdentifier);
					patSchedule.setPatientScheduleSummary(patientScheduleSummary);
					ObjectMap mapPatOID = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PERSON, rs.getInt("FK_PER"));
					//ObjectMap mapOrgOID = objectMapService.getOrCreateObjectMapFromPK(objectMapService.PERSISTENCE_UNIT_ORGANIZATION, rs.getInt("PK_SITE"));
					//patientIdentifier.setPatientId(rs.getString("PER_CODE"));
					patientIdentifier.setOID(mapPatOID.getOID());
					patientIdentifier.setPK(rs.getInt("FK_PER"));
					//organizationIdentifier.setOID(mapOrgOID.getOID());
					//organizationIdentifier.setPK(rs.getInt("PK_SITE"));
					//patientIdentifier.setOrganizationId(organizationIdentifier);
					patSchedule.setPatientIdentifier(patientIdentifier);
					
					ObjectMap mapStudyOID = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY, rs.getInt("FK_STUDY"));
					StudyJB study = new StudyJB();
					study.setId(rs.getInt("FK_STUDY"));
					study.getStudyDetails();
					studyIdentifier.setStudyNumber(study.getStudyNumber());
					studyIdentifier.setOID(mapStudyOID.getOID());
					studyIdentifier.setPK(rs.getInt("FK_STUDY"));
					patSchedule.setStudyIdentifier(studyIdentifier);
				
				}
				ctr++;			
				//BUg Fix : 12406 && 12431
				if(visitScheduleList.isEmpty())
				{
					suggestedDate = "NOT DEFINED";
					scheduleDate = "NOT DEFINED";
						
				}else{
					suggestedDate = Collections.min(visitSuggList);
					scheduleDate = Collections.min(visitScheduleList);
				}
				visit.setVisitSuggestedDate(suggestedDate);
				visit.setVisitScheduledDate(scheduleDate);//Bug Fix : 12406
			}
			
			//Bug Fix : 12511 & 12407
			if(visitFlag > 0){

				//visit.setEvents(events);
	            visits.addVisit(visit);
				patSchedule.setVisits(visits);
			}
			return patSchedule;
			
		}catch(Throwable t){
			
			t.printStackTrace();
			
			throw new OperationException();
		}
		
		finally{
			try{
				if(pstmt!=null){
					pstmt.close();
				}
			}catch(Exception e){
				
			}
			try{
				if(conn!=null){
					conn.close();
				}
			}catch(Exception e){
				
			}
		}
	
		
	}
	
	public static PatientSchedule getCurrentPatientSchedule(int personPK, int studyPK,String startDate,String endDate, Map<String, Object> parameters) throws OperationException{
		
		PreparedStatement pstmt = null;
		Connection conn = null;
		PatientSchedule patSchedule = null;
		ObjectMapService objectMapService = ServicesUtil.getObjectMapService();
		ResponseHolder resp=(ResponseHolder) parameters.get("response");
		try{
			StringBuffer PatientScheduleSql = new StringBuffer(getCurrentPatientScheduleSql);
			conn = getConnection();
			if(logger.isDebugEnabled()) logger.debug("sql:" + "getCurrentPatientScheduleSql");
			if(!StringUtil.isEmpty(startDate) && !StringUtil.isEmpty(endDate) )
			{	
				PatientScheduleSql.append("and s.actual_schdate BETWEEN to_date(?,'MM/dd/yyyy') AND to_date(?,'MM/dd/yyyy')");
				PatientScheduleSql.append("order by v.visit_no, s.event_id");
											
			}
			else
			{			
				PatientScheduleSql.append("order by v.visit_no, s.event_id");
					
			}
						
			pstmt = conn.prepareStatement(PatientScheduleSql.toString());
			pstmt.setInt(1, personPK);
			pstmt.setInt(2, studyPK);
			if(!StringUtil.isEmpty(startDate) && !StringUtil.isEmpty(endDate) )
			{	
				pstmt.setString(3, startDate);
				pstmt.setString(4, endDate);
			}	
			
			ResultSet rs = pstmt.executeQuery();
			Visits visits = new Visits();
			Visit visit = new Visit();
			Events events = new Events();
			Event event = new Event(); 
			PatientScheduleSummary patientScheduleSummary = new PatientScheduleSummary();
			patSchedule = new PatientSchedule();
			PatientProtocolIdentifier patientProtocolIdentifier = new PatientProtocolIdentifier();
			PatientIdentifier patientIdentifier = new PatientIdentifier();
			StudyIdentifier studyIdentifier = new StudyIdentifier();
          
			int ctr = 0;
			int visitCtr = 0;
			int curEventPK = 0;
			int visitWinBefore = 0;
			int visitWinAfter = 0;
			int eventWinAfter = 0;
			int eventWinBefore = 0;
            String scheduleDate = "";
            String suggestedDate = "";
            int visitFlag = 0;//Bug Fix : 12511 & 12407
			String visitWinBeforeUnit = "";
			String visitWinAfterUnit = "";
			String eventWinBeforeUnit = "";
			String eventWinAfterUnit = "";
            List<String> visitScheduleList = new ArrayList<String>();
            List<String> visitSuggList = new ArrayList<String>();
            Integer counter =0;
			while(rs.next()){
                int rsval = rs.getInt("PK_PROTOCOL_VISIT");
				VisitIdentifier visitIdentifier = new VisitIdentifier();
				if ((visitCtr == 0 || rs.getInt("PK_PROTOCOL_VISIT") != visitCtr)){
					visitSuggList = new ArrayList<String>();
					visitScheduleList = new ArrayList<String>();
					if(visitFlag > 0){//Bug Fix : 12511 & 12407
			            visit.setEvents(events);
			            visits.addVisit(visit);
					}
					visitFlag++;
					visit = new Visit();
					events = new Events();
					curEventPK = 0; 
					visitCtr = rs.getInt("PK_PROTOCOL_VISIT");
					visit.setVisitName(rs.getString("VISIT_NAME"));
					ObjectMap mapVisitOID = objectMapService.getOrCreateObjectMapFromPK(objectMapService.PERSISTENCE_UNIT_VISIT, rs.getInt("PK_PROTOCOL_VISIT"));
					visitIdentifier.setPK(rs.getInt("PK_PROTOCOL_VISIT"));
					visitIdentifier.setOID(mapVisitOID.getOID());
					visit.setVisitIdentifier(visitIdentifier);
					visitWinBefore = rs.getInt("WIN_BEFORE_NUMBER");
					visitWinAfter = rs.getInt("WIN_AFTER_NUMBER");
					if((visitWinBefore != 0) || (visitWinAfter != 0)){
						visitWinBeforeUnit = rs.getString("WIN_BEFORE_UNIT");
						visitWinAfterUnit = rs.getString("WIN_AFTER_UNIT");
						Calendar currBefore = Calendar.getInstance();
						Calendar currAfter = Calendar.getInstance();
						currBefore.setTime(rs.getDate("START_DATE_TIME"));
						currAfter.setTime(rs.getDate("START_DATE_TIME"));
						visit.setVisitWindow(PatientScheduleDAO.dateWindow(visitWinBeforeUnit, currBefore, visitWinBefore, visitWinAfterUnit, currAfter, visitWinAfter));
					}
	
				}

				if(curEventPK == 0 || rs.getInt("EVENT_ID") != curEventPK)
				{
					curEventPK = rs.getInt("EVENT_ID"); 
					event = new Event();
					EventStatus eventStatus = new EventStatus();
					Code eventStatusCode = new Code();
					Code coverageType = new Code();
					ScheduleEventIdentifier scheduleEventIdentifier = new ScheduleEventIdentifier();
					OrganizationIdentifier organizationIdentifier = new OrganizationIdentifier();//Bug Fix : 12402
				
					EventCRFs eventCRFs = new EventCRFs();
					if(rs.getString("ACTUAL_SCHDATE") != null) visitScheduleList.add(rs.getString("ACTUAL_SCHDATE"));//Bug Fix : 12406 && 12427
					if(rs.getString("ACTUAL_SCHDATE") != null) visitSuggList.add(rs.getString("START_DATE_TIME"));//BUg Fix : 12406 && 12427
					event.setEventName(rs.getString("EVENT_NAME"));
					event.setDescription(rs.getString("DESCRIPTION"));
					if(rs.getString("ACTUAL_SCHDATE") != null){
						event.setEventSuggestedDate(rs.getString("START_DATE_TIME"));
						event.setEventScheduledDate(rs.getString("ACTUAL_SCHDATE"));
					}
					else{
						event.setEventSuggestedDate("Not Defined");
						event.setEventScheduledDate("Not Defined");
					}
					eventWinBefore = rs.getInt("EVENT_WIN_BEFORE");
					eventWinAfter = rs.getInt("EVENT_WIN_AFTER");
					if((eventWinBefore != 0) || (eventWinAfter != 0)){
						eventWinBeforeUnit = rs.getString("EVENT_DURATIONBEFORE");
						eventWinAfterUnit = rs.getString("EVENT_DURATIONAFTER");
						Calendar currBefore = Calendar.getInstance();
						Calendar currAfter = Calendar.getInstance();
						currBefore.setTime(rs.getDate("START_DATE_TIME"));
						currAfter.setTime(rs.getDate("START_DATE_TIME"));
						event.setEventWindow(PatientScheduleDAO.dateWindow(eventWinBeforeUnit, currBefore, eventWinBefore, eventWinAfterUnit, currAfter, eventWinAfter));
					}
					eventStatusCode.setCode(rs.getString("CODELST_SUBTYP"));
					eventStatusCode.setDescription(rs.getString("EVENT_STATUS"));
					eventStatus.setEventStatusCode(eventStatusCode);
					eventStatus.setStatusValidFrom(rs.getDate("EVENTSTAT_DT"));
					event.setEventStatus(eventStatus);
					if(rs.getInt("SERVICE_SITE_ID") > 0){
						ObjectMap mapSiteOID = objectMapService.getOrCreateObjectMapFromPK(objectMapService.PERSISTENCE_UNIT_ORGANIZATION, rs.getInt("SERVICE_SITE_ID"));
						organizationIdentifier.setOID(mapSiteOID.getOID());
						organizationIdentifier.setPK(rs.getInt("SERVICE_SITE_ID"));
						organizationIdentifier.setSiteName(rs.getString("SITE_OF_SERVICE"));
						event.setSiteOfService(organizationIdentifier);
					}
					coverageType.setCode(rs.getString("COVERAGE_CODE"));
					coverageType.setDescription(rs.getString("COVERAGE_DESC"));
					coverageType.setType(rs.getString("COVERAGE_TYPE"));
					event.setCoverageType(coverageType);
					ObjectMap mapEventOID = objectMapService.getOrCreateObjectMapFromPK(objectMapService.PERSISTENCE_UNIT_EVENT, rs.getInt("EVENT_ID"));
					scheduleEventIdentifier.setOID(mapEventOID.getOID());
					scheduleEventIdentifier.setPK(rs.getInt("EVENT_ID"));
					event.setScheduleEventIdentifier(scheduleEventIdentifier);
					event.setEventCRFs(eventCRFs);
					event.setNotes(rs.getString("NOTES"));
					events.addEvent(event);

				}
				
				
				if(rs.getInt("FK_FORM") != 0){
					FormIdentifier formIdentifier = new FormIdentifier();
					EventCRF eventCRF = new EventCRF();
					ObjectMap mapFormOID = objectMapService.getOrCreateObjectMapFromPK(objectMapService.PERSISTENCE_UNIT_FORM_LIBRARY, rs.getInt("FK_FORM"));
					formIdentifier.setOID(mapFormOID.getOID());
					formIdentifier.setPK(rs.getInt("FK_FORM"));
					eventCRF.setFormIdentifier(formIdentifier);
					eventCRF.setFormName(rs.getString("FORM_NAME"));
					event.getEventCRFs().addEventCRF(eventCRF);
				
				}
		
				if (ctr == 0){
					OrganizationIdentifier organizationIdentifier = new OrganizationIdentifier();//Bug Fix : 12402
					ObjectMap mapOID = objectMapService.getOrCreateObjectMapFromPK(objectMapService.PERSISTENCE_UNIT_PATIENT_SCHEDULE, rs.getInt("PK_PATPROT"));
					patientProtocolIdentifier.setPK(rs.getInt("PK_PATPROT")); 
					patientProtocolIdentifier.setOID(mapOID.getOID());
					patientScheduleSummary.setStartDate(rs.getDate("PATPROT_START"));
					patientScheduleSummary.setCalendarName(rs.getString("PROTOCOL_NAME") + "," + rs.getString("PATPROT_START").toString());
					patientScheduleSummary.setCurrent(rs.getString("PATPROT_STAT").equals("1") ? true:false);
					patientScheduleSummary.setScheduleIdentifier(patientProtocolIdentifier);
					patSchedule.setPatientScheduleSummary(patientScheduleSummary);
					ObjectMap mapPatOID = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PERSON, rs.getInt("PK_PER"));
					ObjectMap mapOrgOID = objectMapService.getOrCreateObjectMapFromPK(objectMapService.PERSISTENCE_UNIT_ORGANIZATION, rs.getInt("PK_SITE"));
					patientIdentifier.setPK(rs.getInt("PK_PER")); 
					patientIdentifier.setPatientId(rs.getString("PER_CODE"));
					patientIdentifier.setOID(mapPatOID.getOID());
					organizationIdentifier.setPK(rs.getInt("PK_SITE")); 
					organizationIdentifier.setOID(mapOrgOID.getOID());
					patientIdentifier.setOrganizationId(organizationIdentifier);
					patSchedule.setPatientIdentifier(patientIdentifier);
					
					
					ObjectMap mapStudyOID = objectMapService.getOrCreateObjectMapFromPK(objectMapService.PERSISTENCE_UNIT_STUDY, rs.getInt("PK_STUDY"));
					
					studyIdentifier.setStudyNumber(rs.getString("STUDY_NUMBER"));
					studyIdentifier.setPK(rs.getInt("PK_STUDY"));
					studyIdentifier.setOID(mapStudyOID.getOID());
					patSchedule.setStudyIdentifier(studyIdentifier);
					
					
				}
				ctr++;
				//BUg Fix : 12406 && 12427
				if(visitScheduleList.isEmpty())
				{
					suggestedDate = "NOT DEFINED";
					scheduleDate = "NOT DEFINED";
						
				}else{
					suggestedDate = Collections.min(visitSuggList);
					scheduleDate = Collections.min(visitScheduleList);
				}
				visit.setVisitSuggestedDate(suggestedDate);
				visit.setVisitScheduledDate(scheduleDate);//Bug Fix : 12406
				counter++;
			}
			if(counter <= 0)
			{
			
				resp.addIssue(new Issue(IssueTypes.EVENT_NOT_FOUND, "No schedule found for the given startdate and enddate"));
		        throw new OperationException();
			
			}	
			
			//Bug Fix : 12511 & 12407
			if(visitFlag > 0){

				visit.setEvents(events);
	            visits.addVisit(visit);
				patSchedule.setVisits(visits);
			}
			return patSchedule;
			
		}catch(Throwable t){
			
			t.printStackTrace();
			
			throw new OperationException();
		}
		
		finally{
			try{
				if(pstmt!=null){
					pstmt.close();
				}
			}catch(Exception e){
				
			}
			try{
				if(conn!=null){
					conn.close();
				}
			}catch(Exception e){
				
			}
		}
	
		
	}
	
	
    public static SitesOfService getSitesOfService(int accountPK) throws OperationException{
    	
		PreparedStatement pstmt = null;
		Connection conn = null;
		SitesOfService sitesOfService = new SitesOfService();
		ObjectMapService objectMapService = ServicesUtil.getObjectMapService();
    	try{
			conn = getConnection();
			if(logger.isDebugEnabled()) logger.debug("sql:" + "getSitesOfServiceSql");
			pstmt = conn.prepareStatement(getSitesOfServiceSql);
			pstmt.setInt(1, accountPK);
			ResultSet rs = pstmt.executeQuery();
        	List<OrganizationIdentifier> sitesOfServiceList = new ArrayList<OrganizationIdentifier>();
        	while(rs.next()){
        		OrganizationIdentifier organizationIdentifier = new OrganizationIdentifier();
        		ObjectMap mapSiteOID = objectMapService.getOrCreateObjectMapFromPK(objectMapService.PERSISTENCE_UNIT_ORGANIZATION, rs.getInt("PK_SITE"));
        		organizationIdentifier.setPK(rs.getInt("PK_SITE"));
        		organizationIdentifier.setOID(mapSiteOID.getOID());
        		organizationIdentifier.setSiteName(rs.getString("SITE_NAME"));
        		sitesOfServiceList.add(organizationIdentifier);
        	}
        	sitesOfService.addAll(sitesOfServiceList);
        	return sitesOfService;
    	}catch(Throwable t){
    		t.printStackTrace();
    		throw new OperationException();
    	}
    	
    	finally{
    		try{
    			if(pstmt!=null){
    				pstmt.close();
    			}
    		}catch(Exception e){
    			
    		}
    		try{
    			if(conn!=null){
    				conn.close();
    			}
    		}catch(Exception e){
    			
    		}
    	}
    }
	
	
	/**
	 * 
	 * @param personPk
	 * @param StudyPk
	 * @return
	 * @throws OperationException
	 */
	public static ArrayList<Integer> getVisitPkPatProtPk(int personPK, int StudyPK, int patProtPK) throws OperationException{
		PreparedStatement pstmt = null;
	    Connection conn = null;
	    String sql = "";
	   
	    ArrayList<Integer> lstVisitPk = new ArrayList<Integer>();
	    try {
	    	conn = getConnection();
	
	        sql= "select distinct(ev1.fk_visit),p.pk_patprot,p.fk_protocol,ev.name as protocol_name, " +
	        		"p.patprot_start patprot_start, p.fk_study " +
	        		"from er_patprot p, event_assoc ev,SCH_EVENTS1 ev1 " +
	        		"where p.fk_study = ?  and  p.fk_per = ? and p.pk_patprot= ? and ev.event_id = p.fk_protocol " +
	        		"and ev1.fk_patprot = p.pk_patprot and ev.event_type = 'P'" +
	        		" order by pk_patprot desc";	
		          pstmt = conn.prepareStatement(sql);
		          pstmt.setInt(1, StudyPK);                      
		          pstmt.setInt(2, personPK);  
		          pstmt.setInt(3, patProtPK); 
	        ResultSet rs = pstmt.executeQuery();
	        
	          while (rs.next()) {
	          	
	          	Integer VisitPk = rs.getInt("fk_visit");
	          	lstVisitPk.add(VisitPk);
	          	
	          }
	          return lstVisitPk;
	          
	    } 
	    catch(Throwable t){
	    	t.printStackTrace();
	    	throw new OperationException();
	}
	finally {
		try {
			if (pstmt != null)
				pstmt.close();
		} catch (Exception e) {
		}
		try {
			if (conn != null)
				conn.close();
			} catch (Exception e) {
		}
	    } 
	
	}
	/**
	 * 
	 * @param VisitPk
	 * @param patProtPk
	 * @param userAccountPk
	 * @return
//	 * @throws OperationException
//	 */
//	public Visit getVisitsEvents(int visitPk, int patProtPk, int userAccountPk) throws OperationException{
//		PreparedStatement pstmt = null;
//	    Connection conn = null;
//	    Visit visit = new Visit();
//	    VisitIdentifier visitIdentifier = new VisitIdentifier();
//	    ArrayList<Event> lstEvent = new ArrayList<Event>();
//	    CodeCache codeCache = CodeCache.getInstance();
//	    
//	    ObjectMapService objectMapService = ServicesUtil.getObjectMapService(); 
//	  	ObjectMap obj2 = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_VISIT, visitPk);
//	  	String OID = obj2.getOID();
//	  	visitIdentifier.setOID(OID);
//	  	visit.setVisitIdentifier(visitIdentifier);
//	    
//	    try {
//	    	conn = getConnection();
//	
//	    	String scheduleSql=   "select ev.DESCRIPTION, to_char(START_DATE_TIME, 'yyyy-mm-dd') START_DATE_TIME, "
//				+ " nvl(evass.FUZZY_PERIOD,0) FUZZY_PERIOD ,"
//				+ " evass.EVENT_DURATIONBEFORE,evass.EVENT_FUZZYAFTER,evass.EVENT_DURATIONAFTER,"
//				+ " ev.EVENT_ID,estat.PK_EVENTSTAT, estat.EVENTSTAT_DT, estat.EVENTSTAT_NOTES,"//EVENTSTAT_DT,ev.SERVICE_SITE_ID,ev.REASON_FOR_COVERAGECHANGE,EVENTSTAT_NOTES,,
//				+ " ev.ISCONFIRMED, "
//				+ " ev.FK_ASSOC, "
//				+ " ev.NOTES, "
//				+ "	 to_char(ev.EVENT_EXEON, PKG_DATEUTIL.F_GET_DATEFORMAT) as EVENT_EXEON, "
//				+ "	to_char(ev.ACTUAL_SCHDATE, 'yyyy-mm-dd')  ACTUAL_SCHDATE, "
//				+ "	ev.EVENT_EXEBY, "
//				+ "	ev.ADVERSE_COUNT, "
//				+ " ev.visit,vis.pk_protocol_visit, "
//				+ " vis.visit_name," 
//				+ " vis.win_after_unit, vis.win_after_number, vis.win_before_unit,vis.win_before_number," 
//				+ "ev.fk_visit, "
//				+ " ev.status,ev.event_notes, (select count(*) from sch_event_kit where fk_event = ev.fk_assoc) kitCount ,pkg_gensch.f_get_event_roles(ev.fk_assoc) event_roles, vis.no_interval_flag interval_flag,  "
//				+ " ev.SERVICE_SITE_ID,ev.FACILITY_ID,ev.FK_CODELST_COVERTYPE, ev.REASON_FOR_COVERAGECHANGE "
//				+ " FROM SCH_EVENTS1 ev"
//				+ " , SCH_PROTOCOL_VISIT vis"
//				+ " , EVENT_ASSOC evass, SCH_EVENTSTAT estat"
//				+ " WHERE  ev.fk_patprot = ?" 
//				+ " and ev.fk_visit = ?" 
//				+ " and ev.fk_visit = vis.pk_protocol_visit "
//				+ " and evass.EVENT_ID = ev.FK_ASSOC and estat.FK_EVENT = ev.EVENT_ID "
//				//Virendra:Fixed #6117, modified query for event with latest event status
//				+ " and estat.pk_eventstat = (select max(estat1.pk_eventstat) "
//				+ "from sch_eventstat estat1 where estat1.fk_event = ev.event_id ) ";
//	    	
//		          pstmt = conn.prepareStatement(scheduleSql);
//		          pstmt.setInt(1, patProtPk);                      
//		          pstmt.setInt(2, visitPk);             
//	        ResultSet rs = pstmt.executeQuery();
//	        
//	          while (rs.next()) {
//	        	  
//	        	 
//	        	  
//	        	  visit.setVisitName(rs.getString("VISIT_NAME"));
//	        	  String suggestedStartDate = rs.getString("START_DATE_TIME");
//	        	  String actualScheduleDate =rs.getString("ACTUAL_SCHDATE");
//	        	  visit.setVisitSuggestedDate(suggestedStartDate);
//	        	  visit.setVisitScheduledDate(actualScheduleDate);
//	        	  //VISIT WINDOW
//	        	  Integer visitWinBeforeNumber = rs.getInt("WIN_BEFORE_NUMBER");
//	        	  String visitWinBeforeUnit = rs.getString("WIN_BEFORE_UNIT");
//	        	  Integer visitWinAfterNumber = rs.getInt("WIN_AFTER_NUMBER");
//	        	  String visitWinAfterUnit = rs.getString("WIN_AFTER_UNIT");
//	        	  
//	        	  StringBuffer visitWindowSB = new StringBuffer();
//	        	  String visitWindow = null;
//	        	 
//	        	  if (visitWinBeforeNumber != null && visitWinBeforeNumber != 0 &&
//	        			  visitWinBeforeUnit != null && visitWinBeforeUnit.length() > 0 && actualScheduleDate != null){
//		        	  Date beginDate = calculateDate(actualScheduleDate, -1*visitWinBeforeNumber,
//		        			  visitWinBeforeUnit);
//		        	  visitWindowSB.append(DateUtil.dateToString(beginDate)).append("~");
//	        	  }
//	        	  else{
//	        		  visitWindowSB = null;
//	        	  }
//	        	  if (visitWinAfterNumber != null && visitWinAfterNumber != 0 &&
//	        			  visitWinAfterUnit != null && visitWinAfterUnit.length() > 0 && actualScheduleDate != null){
//		        	  String endDate = calculateDate(actualScheduleDate, visitWinAfterNumber,
//		        			  visitWinAfterUnit);
//		        	  visitWindowSB.append(DateUtil.dateToString(endDate));
//	        	  }
//	        	  else{
//	        		  visitWindowSB = null;
//	        	  }
//	        	  if(visitWindowSB != null){
//		        	  visitWindow = visitWindowSB.toString();
//		        	  visit.setVisitWindow(visitWindow);
//	        	  }
//	        	  //VISIT WINDOW END
//	        	  ObjectMap obj = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_EVENT, rs.getInt("EVENT_ID"));
////	        	  String strOID = obj.getOID();
//	        	  Event patEvent = new Event();
//	        	  ScheduleEventIdentifier eventId = new ScheduleEventIdentifier();
//	        	  eventId.setOID(obj.getOID());
//	        	  patEvent.setScheduleEventIdentifier(eventId);
//	        	  
//	        	  patEvent.setEventSuggestedDate(suggestedStartDate);
//	        	  patEvent.setEventScheduledDate(actualScheduleDate);
//	        	  
//	        	  //setting eventstatus
//	        	
//	        	  /*
//	        	   * Code studyStatusCodeType = 
//						codeCache.getCodeSubTypeByPK(
//								CodeCache.CODE_TYPE_STATUS_TYPE, 
//								rs.getInt("STATUS_TYPE"),
//								userAccountPK1);
//	        	   * 
//	        	   * 
//	        	   * 
//	        	   * */
//	        	  EventStatus eventStatus = new EventStatus();
//	        	  Integer eventStatusPk = rs.getInt("ISCONFIRMED");
//	        	  Code eventStatusCode = 
//						codeCache.getSchCodeSubTypeByPK(
//								CodeCache.CODE_TYPE_EVENT_STATUS_TYPE, 
//								eventStatusPk,
//								userAccountPk);
//	        	  
//	        	  Code coverageTypeCode = 
//						codeCache.getSchCodeSubTypeByPK(
//								CodeCache.CODE_TYPE_COVERAGE_TYPE, 
//								rs.getInt("FK_CODELST_COVERTYPE"),
//								userAccountPk);
//	        	  
//	        	  //Virendra: Fixed#6118
//	        	  String reasonForChangeCoverType = rs.getString("REASON_FOR_COVERAGECHANGE");
//	        	  
//	        	  Date eventStatusDateValidFrom = rs.getDate("EVENTSTAT_DT");
//	        	  
//	        	  String eventStatusNotes = rs.getString("EVENTSTAT_NOTES");
//	        	  
////	        	  OrganizationIdentifier organizationIdService = new OrganizationIdentifier();
////	        	  Integer serviceSitePK = rs.getInt("SERVICE_SITE_ID");
//	        	  
//	        	  OrganizationIdentifier orgIdentifier = new OrganizationIdentifier();
//	        	  orgIdentifier.setSiteName(siteName);
//	        	  
//	        	  
//	        	  
//	        	  if(eventStatusCode != null){
//	        		  eventStatus.setEventStatusCode(eventStatusCode);
//	        	  }
//	        	  if(coverageTypeCode != null){
//	        		  eventStatus.setCoverageType(coverageTypeCode);
//	        	  }
//	        	 
//	        	  eventStatus.setStatusValidFrom(eventStatusDateValidFrom);
//	        	  eventStatus.setSiteOfService(orgIdentifier);
//	        	  eventStatus.setReasonForChangeCoverType(reasonForChangeCoverType);
//	        	  eventStatus.setNotes(eventStatusNotes);
//	        	  
//	        	  patEvent.setEventStatus(eventStatus);
//	        	
//	        	  
//	        	  patEvent.setDescription(rs.getString("DESCRIPTION"));
//	        	  Integer winBeforeNumber = rs.getInt("FUZZY_PERIOD");
//	        	  String winBeforeUnit = rs.getString("EVENT_DURATIONBEFORE");
//	        	  Integer winAfterNumber = rs.getInt("EVENT_FUZZYAFTER");
//	        	  String winAfterUnit = rs.getString("EVENT_DURATIONAFTER");
//	        	  //Duration dur = new Duration();
//	        	  //dur.setUnit(TimeUnits.);
//	        	  //EVENT WINDOW
//	        	  //String strActualDate = actualDate.toString();
//	        	  StringBuffer windowSB = new StringBuffer();
//	        	  String eventWindow = null;
//	        	 
//	        	  if (winBeforeNumber != null && winBeforeNumber != 0 &&
//	        			  winBeforeUnit != null && winBeforeUnit.length() > 0 && actualScheduleDate != null){
//		        	  Date beginDate = calculateDate(actualScheduleDate, -1*winBeforeNumber,
//		                        winBeforeUnit);
//		        	  windowSB.append(DateUtil.dateToString(beginDate)).append("~");
//	        	  }
//	        	  else{
//	        	  windowSB = null;
//	        	  }
//	        	  if (winAfterNumber != null && winAfterNumber != 0 &&
//	        			  winAfterUnit != null && winAfterUnit.length() > 0 && actualScheduleDate != null){
//		        	  Date endDate = calculateDate(actualScheduleDate, winAfterNumber,
//		                        winAfterUnit);
//		        	  windowSB.append(DateUtil.dateToString(endDate));
//	        	  }
//	        	  else{
//		        	  windowSB = null;
//	        	  }
//	        	  if(windowSB != null){
//		        	  eventWindow = windowSB.toString();
//		        	  patEvent.setEventWindow(eventWindow);
//	        	  }
//	        	  //EVENT WINDOW END
//	        	 
//	        	  //setting status coverage type
//	        	 
//	        	  if(coverageTypeCode != null){
//	        		  patEvent.setCoverageType(coverageTypeCode);
//	        	  }
//	        	  
//	        	  lstEvent.add(patEvent);
//	   //     	  visit.setEvents(lstEvent);
//	          }
//		} 
//		catch(Throwable t){
//			
//			t.printStackTrace();
//			throw new OperationException();
//	}
//	finally {
//		try {
//			if (pstmt != null)
//				pstmt.close();
//		} catch (Exception e) {
//		}
//		try {
//			if (conn != null)
//				conn.close();
//		} catch (Exception e) {
//		}
//		
//    	} 
//		return visit;
//	}
	/**
	 * 
	 * @param startDate
	 * @param offsetNumber
	 * @param offsetUnit
	 * @return
	 */
	 private Date calculateDate(Date startDate, Integer offsetNumber, String offsetUnit) {
		    Calendar cal1 = Calendar.getInstance();
	        cal1.setTime(startDate);
	        cal1.add(convertUnitToField(offsetUnit), offsetNumber);
	        return cal1.getTime();
    }
    /**
     * 
     * @param offsetUnit
     * @return
     */
    private int convertUnitToField(String offsetUnit) {
        if (offsetUnit == null) { return 0; }
        if ("D".equals(offsetUnit.toUpperCase())) { return Calendar.DAY_OF_YEAR; }
        if ("W".equals(offsetUnit.toUpperCase())) { return Calendar.WEEK_OF_YEAR; }
        if ("M".equals(offsetUnit.toUpperCase())) { return Calendar.MONTH; }
        if ("Y".equals(offsetUnit.toUpperCase())) { return Calendar.YEAR; }
        return 0;
    }
    /**
     * get Eventstatus with StatusPK 
     * @param statusPk
     * @return
     */
//	private EventStatus getEventStatus(int statusPk){
//		
//		  EventStatAgentRObj eventStatAgent = EJBUtil.getEventStatAgentHome();
//		  EventStatus eventStatus = new EventStatus();
//		  
//    	  EventStatBean eventStatBean =  eventStatAgent.getEventStatDetails(statusPk);
//    	 
//    	  eventStatus.setNotes(eventStatBean.getEvtStatNotes());
//    	  
//    	  String statusCodeSubType = schCodeDao.getCodeSubtype(statusPk);
//    	  String statusCodeDesc = schCodeDao.getCodeDescription(statusPk);
//    	  Code statusCode = new Code();
//    	  statusCode.setType(statusCodeSubType);
//    	  statusCode.setDescription(statusCodeDesc);
//    	  Date eventStatDate = eventStatBean.getEventStatDt();
//    	  eventStatus.setStatusValidFrom(eventStatDate);
//    	  
//    	  return eventStatus;
//	}
    public ArrayList<String> getScheduleNames(){
    	ArrayList<String> lstScheduleNames = new ArrayList<String>();
    	return lstScheduleNames;
    }
    
    private static  String dateWindow(String unitBefore, Calendar currBefore, int winBeforeNumber, String unitAfter, Calendar currAfter, int winAfterNumber){
    	
    	String window = "";
		WindowUnit windowUnit = WindowUnit.valueOf(unitBefore);
		switch(windowUnit){
		    
		case H : currBefore.add(Calendar.HOUR, -winBeforeNumber);
		           break;
		case D : currBefore.add(Calendar.DATE, -winBeforeNumber);
		           break;
		case W : currBefore.add(Calendar.WEEK_OF_MONTH, -winBeforeNumber);
		           break;
		case M : currBefore.add(Calendar.MONTH, -winBeforeNumber);
		           break;
		case Y : currBefore.add(Calendar.YEAR, -winBeforeNumber);
		           break;
		}
		windowUnit = WindowUnit.valueOf(unitAfter);
		switch(windowUnit){
		    
		case H : currAfter.add(Calendar.HOUR, winAfterNumber);
                   break;
		case D : currAfter.add(Calendar.DATE, winAfterNumber);
		           break;
		case W : currAfter.add(Calendar.WEEK_OF_MONTH, winAfterNumber);
		           break;
		case M : currAfter.add(Calendar.MONTH, winAfterNumber);
		           break;
		case Y : currAfter.add(Calendar.YEAR, winAfterNumber);
		           break;
		}
		return (currBefore.getTime().toString() + "-" + currAfter.getTime().toString());
		
    }
    
    public static int getStudyPKFromPatProt(Integer patprotPK) throws OperationException{
    	
    	String sql = "select fk_study from er_patprot where pk_patprot = ?";
    	int studyPK = 0;
    	PreparedStatement pstmt  = null;
    	Connection conn =  null;
    	try{
    		conn = getConnection();
    		pstmt = conn.prepareStatement(sql);
        	pstmt.setInt(1, patprotPK);
        	
        	ResultSet rs = pstmt.executeQuery();
        			
        	while(rs.next()){
        		studyPK = rs.getInt("FK_STUDY");
        	}
        	return studyPK;
    	}catch(Throwable t){
    		t.printStackTrace();
    		throw new OperationException();
    	}
    	
    	finally{
    		try{
    			if(pstmt!=null){
    				pstmt.close();
    			}
    		}catch(Exception e){
    			
    		}
    		try{
    			if(conn!=null){
    				conn.close();
    			}
    		}catch(Exception e){
    			
    		}
    	}
    }
    
    public static int getPersonPK(int patprotPK) throws OperationException{
    	
    	String sql = "select fk_per from er_patprot where pk_patprot = ?";
    	int personPK = 0;
    	PreparedStatement pstmt  = null;
    	Connection conn =  null;
    	try{
    		conn = getConnection();
    		pstmt = conn.prepareStatement(sql);
        	pstmt.setInt(1, patprotPK);
        	
        	ResultSet rs = pstmt.executeQuery();
        			
        	while(rs.next()){
        		personPK = rs.getInt("FK_PER");
        	}
        	return personPK;
    	}catch(Throwable t){
    		t.printStackTrace();
    		throw new OperationException();
    	}
    	
    	finally{
    		try{
    			if(pstmt!=null){
    				pstmt.close();
    			}
    		}catch(Exception e){
    			
    		}
    		try{
    			if(conn!=null){
    				conn.close();
    			}
    		}catch(Exception e){
    			
    		}
    	}
    }
   
    public static int getPatProtPK(int eventPK) throws OperationException{
    	
    	String sql = "select fk_patprot from sch_events1 where event_id = ?";
    	int patProtPK = 0;
    	PreparedStatement pstmt  = null;
    	Connection conn =  null;
    	try{
    		conn = getConnection();
    		pstmt = conn.prepareStatement(sql);
        	pstmt.setInt(1, eventPK);
        	
        	ResultSet rs = pstmt.executeQuery();
        			
        	while(rs.next()){
        		patProtPK = rs.getInt("FK_PATPROT");
        	}
        	return patProtPK;
    	}catch(Throwable t){
    		t.printStackTrace();
    		throw new OperationException();
    	}
    	
    	finally{
    		try{
    			if(pstmt!=null){
    				pstmt.close();
    			}
    		}catch(Exception e){
    			
    		}
    		try{
    			if(conn!=null){
    				conn.close();
    			}
    		}catch(Exception e){
    			
    		}
    	}
    }
    
    public static int getOldEventStatus(int eventPK) throws OperationException{
    	
    	String sql = "select isconfirmed from sch_events1 where event_id = ?";
    	int eventStatus = 0;
    	PreparedStatement pstmt  = null;
    	Connection conn =  null;
    	try{
    		conn = getConnection();
    		pstmt = conn.prepareStatement(sql);
        	pstmt.setInt(1, eventPK);
        	
        	ResultSet rs = pstmt.executeQuery();
        			
        	while(rs.next()){
        		eventStatus = rs.getInt("ISCONFIRMED");
        	}
        	return eventStatus;
    	}catch(Throwable t){
    		t.printStackTrace();
    		throw new OperationException();
    	}
    	
    	finally{
    		try{
    			if(pstmt!=null){
    				pstmt.close();
    			}
    		}catch(Exception e){
    			
    		}
    		try{
    			if(conn!=null){
    				conn.close();
    			}
    		}catch(Exception e){
    			
    		}
    	}
    }
	

    public ArrayList<Integer> getDeactivatedEvents(int patientId, int protId, java.sql.Date bookedOn){	
		PreparedStatement pstmt = null;
	    Connection conn = null;
	    String sql = "";
	   
	    ArrayList<Integer> lstEventPk = new ArrayList<Integer>();
	    try {
	    	conn = getConnection();
	
	        sql= "select EVENT_ID FROM SCH_EVENTS1 " 
	        	+ "WHERE PATIENT_ID= LPAD(TO_CHAR(?),10,'0') AND " 
	        	+ "SESSION_ID= LPAD(TO_CHAR(?),10,'0') AND bookedon=?";	
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, patientId);                      
			pstmt.setInt(2, protId);  
			pstmt.setDate(3, bookedOn); 
	        ResultSet rs = pstmt.executeQuery();
	        
			while (rs.next()) {
			  	Integer eventPk = rs.getInt("EVENT_ID");
			  	lstEventPk.add(eventPk);
			}
	    } 
	    catch(Exception e){
	    	e.printStackTrace();
	    	return null;
		}
		finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
				} catch (Exception e) {
			}
		}
		return lstEventPk;
	}
      
    //For Bug Fix : 12377
    public static EventdefBean getOldEventDetails(int eventPK) throws OperationException{
    	
    	String sql = "select fk_codelst_covertype, reason_for_coveragechange, notes, service_site_id from sch_events1 where event_id = ?";
    	EventdefBean eventDefBean = new EventdefBean();
    	PreparedStatement pstmt  = null;
    	Connection conn =  null;
    	try{
    		conn = getConnection();
    		pstmt = conn.prepareStatement(sql);
        	pstmt.setInt(1, eventPK);
        	
        	ResultSet rs = pstmt.executeQuery();
        			
        	while(rs.next()){
        		eventDefBean.setEventCoverageType(EJBUtil.integerToString(rs.getInt("FK_CODELST_COVERTYPE")));
        		eventDefBean.setEventSOSId(EJBUtil.integerToString(rs.getInt("SERVICE_SITE_ID")));
        		eventDefBean.setReasonForCoverageChange(rs.getString("REASON_FOR_COVERAGECHANGE"));
        		System.out.println("the valu for notes ::: " + rs.getString("NOTES"));
        		eventDefBean.setNotes(rs.getString("NOTES"));
        		
        	}
        	return eventDefBean;
    	}catch(Throwable t){
    		t.printStackTrace();
    		throw new OperationException();
    	}
    	
    	finally{
    		try{
    			if(pstmt!=null){
    				pstmt.close();
    			}
    		}catch(Exception e){
    			
    		}
    		try{
    			if(conn!=null){
    				conn.close();
    			}
    		}catch(Exception e){
    			
    		}
    	}
    }
    
    public static int getProtocolID(int visitPK) throws OperationException{
    	
    	String sql = "select fk_protocol from sch_protocol_visit where pk_protocol_visit = ?";
		int protocolID = 0;
    	PreparedStatement pstmt  = null;
    	Connection conn =  null;
    	try{
    		conn = getConnection();
    		pstmt = conn.prepareStatement(sql);
        	pstmt.setInt(1, visitPK);
        	
        	ResultSet rs = pstmt.executeQuery();
        			
        	while(rs.next()){
        		protocolID = rs.getInt("FK_PROTOCOL");		
        	}
        	return protocolID;
    	}catch(Throwable t){
    		t.printStackTrace();
    		throw new OperationException();
    	}
    	
    	finally{
    		try{
    			if(pstmt!=null){
    				pstmt.close();
    			}
    		}catch(Exception e){
    			
    		}
    		try{
    			if(conn!=null){
    				conn.close();
    			}
    		}catch(Exception e){
    			
    		}
    	}
    }
    
    public static String getCategoryName(int eventPK) throws OperationException{
    	
    	String sql = "select name from event_def where event_id = (select chain_id from event_def where event_id = ?)";
		String categoryName = "";
    	PreparedStatement pstmt  = null;
    	Connection conn =  null;
    	try{
    		conn = getConnection();
    		pstmt = conn.prepareStatement(sql);
        	pstmt.setInt(1, eventPK);
        	
        	ResultSet rs = pstmt.executeQuery();
        			
        	while(rs.next()){
        		categoryName = rs.getString("NAME");		
        	}
        	return categoryName;
    	}catch(Throwable t){
    		t.printStackTrace();
    		throw new OperationException();
    	}
    	
    	finally{
    		try{
    			if(pstmt!=null){
    				pstmt.close();
    			}
    		}catch(Exception e){
    			
    		}
    		try{
    			if(conn!=null){
    				conn.close();
    			}
    		}catch(Exception e){
    			
    		}
    	}
    }
    
   
	  public static String[] updateUnscheduledEvents1( int patId,int patProtId,  int calId, int userId, String ipAdd, String[] eventStatusPKs, 
			                                       String[] eventStartDate, String[] eventNotes, String[] eventCoverageType, 
			                                       String[] eventSOSId, String[] reasonForCoverageChange, String[] eventValidfrom, //String[] statusValidFrom,//Bug Fix : 14611
			                                       String[] eventsArr  ) throws SQLException {


		  CallableStatement cstmt = null;
	      Connection conn = null;
	      ARRAY schEvtArray =null;
	      String sql = "";
	      int ret = 0;
	      try {
	          conn = getSchConnection();

	 //         String[] schEventsArr = new String[eventsArr.length];

	          ArrayDescriptor evtIds = ArrayDescriptor.createDescriptor("ARRAY_STRING", conn);
	          ARRAY evtArray = new ARRAY(evtIds, conn, eventsArr);
	          ARRAY eventStatusArray = new ARRAY(evtIds, conn, eventStatusPKs);
	          ARRAY eventStartDateArray = new ARRAY(evtIds, conn, eventStartDate);
	          ARRAY eventNotesArray = new ARRAY(evtIds, conn, eventNotes);
	          ARRAY eventCoverageTypeArray = new ARRAY(evtIds, conn, eventCoverageType);
	          ARRAY eventSOSArray = new ARRAY(evtIds, conn, eventSOSId);
	          ARRAY reasonForCovChangeArray = new ARRAY(evtIds, conn, reasonForCoverageChange);
	          ARRAY evtValidfrom = new ARRAY(evtIds, conn, eventValidfrom); //Bug Fix : 14611
	          //ARRAY statusValidArray = new ARRAY(evtIds, conn, statusValidFrom);
	          
	      //    ArrayDescriptor schEvtIds = ArrayDescriptor.createDescriptor("ARRAY_STRING", conn);
	       //   ARRAY schEvtArray = new ARRAY(schEvtIds, conn, schEventsArr);
//sp_updt_unsch_evts1_enh(p_patId in number, p_patProtId in number, p_calId in number, p_user in number, p_ipAdd in varchar2,p_eventStatus IN NUMBER, p_evStartDate IN DATE, p_evtIds IN ARRAY_STRING) as
	          sql="{call pkg_calendar_enh.sp_updt_unsch_evts1_enh(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
		          cstmt = conn.prepareCall(sql);
		          cstmt.setInt(1, patId);

		          cstmt.setInt(2, patProtId);
		          cstmt.setInt(3, calId);
		          cstmt.setInt(4, userId);
		          cstmt.setString(5, ipAdd);
				  cstmt.setArray(6, eventStatusArray);
				  cstmt.setArray(7, eventStartDateArray);
				  cstmt.setArray(8, eventNotesArray);
				  cstmt.setArray(9, eventCoverageTypeArray);
				  cstmt.setArray(10, eventSOSArray);
				  cstmt.setArray(11, reasonForCovChangeArray);
				 // cstmt.setArray(12, statusValidArray);
		          cstmt.setArray(12, evtArray);
		          cstmt.setArray(13, evtValidfrom);
		          cstmt.registerOutParameter(14, Types.ARRAY, "ARRAY_STRING");

		          cstmt.execute();
		          
		         schEvtArray = (ARRAY) cstmt.getArray(14);
		         

		        
          } catch (SQLException e) {
//              Rlog.fatal("eventassoc",
//                      "In updateUnscheduledEvents1 in EventassocDao line EXCEPTION IN calling the procedure sp_updt_unsch_evts1"
//                              + e);
  			
  			if (logger.isDebugEnabled()) logger.debug("PatientScheduleServiceImpl create", e);
  			throw new SQLException(e);
              
              

          } finally {
              try {
                  if (cstmt != null)
                	  cstmt.close();
              } catch (Exception exception1) {
              }
              try {
                  if (conn != null)
                	  conn.close();
              } catch (Exception exception2) {
              }
          }
          
          return (String[]) schEvtArray.getArray();

	  }
	  
	  public static Integer getStudyPKForAdminCal(Integer calendarPK)
	  {
		  PreparedStatement stmt = null; 
		  ResultSet rs = null ; 
		  Connection conn = null; 
		  Integer studyPK = null;
		  
		  
		  try{
			  conn = getSchConnection();
			  stmt = conn.prepareStatement("select chain_id from event_assoc where event_id = ? AND UPPER(EVENT_TYPE) = ? and event_calassocto= ?");
			  
			  stmt.setInt(1, calendarPK);
			  stmt.setString(2, "P" );
			  stmt.setString(3, "S"); 
			  
			  rs = stmt.executeQuery();
			  
			  if(rs.next())
			  {
				  studyPK = rs.getInt(1);
			  }
			  
			  
			  
		  }catch(SQLException sqe)
		  {
				if (logger.isDebugEnabled()) logger.debug("PatientScheduleDAO getStudyPKForAdminCal", sqe);
			  
		  }finally
		  {
			   try {
	                  if (stmt != null)
	                	  stmt.close();
	              } catch (Exception exception1) {
	              }
	              try {
	                  if (conn != null)
	                	  conn.close();
	              } catch (Exception exception2) {
	              }
			  
		  }
		  
		  return studyPK; 
	  }


	public static String[] updateAdminUnscheduledEvents(Integer adminSchedulePK, Integer userId,
			String ipAddressFieldValue, String[] eventStatusPKs,
			String[] eventStartDate, String[] eventNotes, String[] eventCoverageType, 
            String[] eventSOSId, String[] reasonForCoverageChange, String[] strArrEventIds, String[] eventValidfrom) throws SQLException {

		  CallableStatement cstmt = null;
	      Connection conn = null;
	      ARRAY schEvtArray =null;
	      String sql = "";
	      int ret = 0;
	      try {
	          conn = getSchConnection();

	 //         String[] schEventsArr = new String[eventsArr.length];

	          ArrayDescriptor evtIds = ArrayDescriptor.createDescriptor("ARRAY_STRING", conn);
	          ARRAY evtArray = new ARRAY(evtIds, conn, strArrEventIds);
	          ARRAY eventStatusArray = new ARRAY(evtIds, conn, eventStatusPKs);
	          ARRAY eventStartDateArray = new ARRAY(evtIds, conn, eventStartDate);
	          ARRAY eventNotesArray = new ARRAY(evtIds, conn, eventNotes);
	          ARRAY eventCoverageTypeArray = new ARRAY(evtIds, conn, eventCoverageType);
	          ARRAY eventSOSArray = new ARRAY(evtIds, conn, eventSOSId);
	          ARRAY reasonForCovChangeArray = new ARRAY(evtIds, conn, reasonForCoverageChange);
	          ARRAY evtValidfrom = new ARRAY(evtIds, conn, eventValidfrom);
	          
	      //    ArrayDescriptor schEvtIds = ArrayDescriptor.createDescriptor("ARRAY_STRING", conn);
	       //   ARRAY schEvtArray = new ARRAY(schEvtIds, conn, schEventsArr);
//sp_updt_unsch_evts1_enh(p_patId in number, p_patProtId in number, p_calId in number, p_user in number, p_ipAdd in varchar2,p_eventStatus IN NUMBER, p_evStartDate IN DATE, p_evtIds IN ARRAY_STRING) as
	          sql="{call pkg_calendar_enh.sp_updt_admin_unsch_evts1_enh(?,?,?,?,?,?,?,?,?,?,?,?)}";
		          cstmt = conn.prepareCall(sql);
		          cstmt.setInt(1, adminSchedulePK);
		          cstmt.setInt(2, userId);
		          cstmt.setString(3, ipAddressFieldValue);
				  cstmt.setArray(4, eventStatusArray);
				  cstmt.setArray(5, eventStartDateArray);
				  cstmt.setArray(6, eventNotesArray);
				  cstmt.setArray(7, eventCoverageTypeArray);
				  cstmt.setArray(8, eventSOSArray);
				  cstmt.setArray(9, reasonForCovChangeArray);
		          cstmt.setArray(10, evtArray);
		          cstmt.setArray(11, evtValidfrom);
		          cstmt.registerOutParameter(12, Types.ARRAY, "ARRAY_STRING");

		          cstmt.execute();
		          
		         schEvtArray = (ARRAY) cstmt.getArray(12);
		         

		        
        } catch (SQLException e) {
//            Rlog.fatal("eventassoc",
//                    "In updateUnscheduledEvents1 in EventassocDao line EXCEPTION IN calling the procedure sp_updt_unsch_evts1"
//                            + e);
			
			if (logger.isDebugEnabled()) logger.debug("PatientScheduleServiceImpl create", e);
			e.printStackTrace();
		//	throw new SQLException(e);
            
            

        } finally {
            try {
                if (cstmt != null)
              	  cstmt.close();
            } catch (Exception exception1) {
            }
            try {
                if (conn != null)
              	  conn.close();
            } catch (Exception exception2) {
            }
        }
        
        return (String[]) schEvtArray.getArray();	}
	
	
	
	  public static String[] updateMEventStatus( String[] eventPKs, String[] eventNotes, String[] eventStatDates, int userId,
			  String ipAdd, String mode, String[] eventSOSId, String[] eventCoverageType, String[] reasonForCoverageChange, String[] eventStats) 
					  throws SQLException {


		  CallableStatement cstmt = null;
		  Connection conn = null;
		  String [] schEvtArray = new String[1000];
		  String sql = "";
		  int ret = 0;
		  int success = -1;
		  try {
			  conn = getSchConnection();

			  /*ArrayDescriptor evtIds = ArrayDescriptor.createDescriptor("ARRAY_STRING", conn);
	
			  ARRAY eventPKArray = new ARRAY(evtIds, conn, eventPKs);
			  ARRAY eventStatDateArray = new ARRAY(evtIds, conn, eventStatDates);
			  ARRAY eventNotesArray = new ARRAY(evtIds, conn, eventNotes);
			  ARRAY eventCoverageTypeArray = new ARRAY(evtIds, conn, eventCoverageType);
			  ARRAY eventSOSArray = new ARRAY(evtIds, conn, eventSOSId);
			  ARRAY reasonForCovChangeArray = new ARRAY(evtIds, conn, reasonForCoverageChange);*/
			  
			for(int i=0; i<eventPKs.length;i++){  
				Date st_date1 = DateUtil.stringToDate(eventStatDates[i]);
				java.sql.Date protEndDate = DateUtil.dateToSqlDate(st_date1);
			  sql="{call sp_markdone(?,?,?,?,?,?,?,?,?,?)}";

			  cstmt = conn.prepareCall(sql);
			  cstmt.setInt(1, StringUtil.stringToInteger(eventPKs[i]));
			  if(eventNotes[i]!=null)
				  cstmt.setString(2, eventNotes[i]);
			  else
				  cstmt.setString(2, "");
			  cstmt.setDate(3, protEndDate);
			  cstmt.setInt(4, userId);
			  cstmt.setInt(5, StringUtil.stringToInteger(eventStats[i]));
			  cstmt.setString(6, ipAdd);
			  cstmt.setString(7, mode);
			  if(eventSOSId[i]!=null)
				  cstmt.setInt(8, StringUtil.stringToInteger(eventSOSId[i]));
			  else
				  cstmt.setInt(8, 0);
			  if(eventCoverageType[i]!=null)
				  cstmt.setInt(9, StringUtil.stringToInteger(eventCoverageType[i]));
			  else
				  cstmt.setInt(9, 0);
			  if(reasonForCoverageChange[i]!=null)
				  cstmt.setString(10, reasonForCoverageChange[i]);
			  else
				  cstmt.setString(10, "");
			  cstmt.execute();
			}
			  success = 1;
		for(int i=0; i<eventPKs.length;i++){ 
			  cstmt = conn.prepareCall("{call SP_EVENTNOTIFY(?,?,?,?,?)}");
			  cstmt.setInt(1, StringUtil.stringToInteger(eventPKs[i]));
			  cstmt.setInt(2, StringUtil.stringToInteger(eventStats[i]));
			  cstmt.setInt(3, userId);
			  cstmt.setString(4, ipAdd);
			  cstmt.setInt(5, StringUtil.stringToInteger(eventStats[i]));

			  cstmt.execute();
		}
		success = 1;
		  } catch (SQLException e) {

			  if (logger.isDebugEnabled()) logger.debug("PatientScheduleServiceImpl create", e);
			  throw new SQLException(e);



		  } finally {
			  try {
				  if (cstmt != null)
					  cstmt.close();
			  } catch (Exception exception1) {
			  }
			  try {
				  if (conn != null)
					  conn.close();
			  } catch (Exception exception2) {
			  }
		  }

		  return (String[]) eventPKs;

	  }
	
	
    public static boolean ifEventExists(int eventPK) throws OperationException{
    	
    	String sql = "select count(*) as Count from event_def where event_id = ?";
    	PreparedStatement pstmt  = null;
    	Connection conn =  null;
    	boolean flag = false;
    	try{
    		conn = getConnection();
    		pstmt = conn.prepareStatement(sql);
        	pstmt.setInt(1, eventPK);
        	
        	ResultSet rs = pstmt.executeQuery();
        			
        	while(rs.next()){
        		if(rs.getInt("Count") ==1)
				   flag = true;
				else 
				   flag =  false;
        	}
        	return flag;
    	}catch(Throwable t){
    		t.printStackTrace();
    		throw new OperationException();
    	}
    	
    	finally{
    		try{
    			if(pstmt!=null){
    				pstmt.close();
    			}
    		}catch(Exception e){
    			
    		}
    		try{
    			if(conn!=null){
    				conn.close();
    			}
    		}catch(Exception e){
    			
    		}
    	}
    }
    
    
    public static boolean ifSiteExists(int sitePK) throws OperationException{
    	
    	String sql = "select count(*) as Count from er_site where pk_site = ?";
    	PreparedStatement pstmt  = null;
    	Connection conn =  null;
    	boolean flag = false;
    	try{
    		conn = getConnection();
    		pstmt = conn.prepareStatement(sql);
        	pstmt.setInt(1, sitePK);
        	
        	ResultSet rs = pstmt.executeQuery();
        			
        	while(rs.next()){
        		if(rs.getInt("Count") ==1)
				   flag = true;
				else 
				   flag =  false;
        	}
        	return flag;
    	}catch(Throwable t){
    		t.printStackTrace();
    		throw new OperationException();
    	}
    	
    	finally{
    		try{
    			if(pstmt!=null){
    				pstmt.close();
    			}
    		}catch(Exception e){
    			
    		}
    		try{
    			if(conn!=null){
    				conn.close();
    			}
    		}catch(Exception e){
    			
    		}
    	}
    }
    
    
    public static boolean ifVisitExists(int visitPK) throws OperationException{
    	
    	String sql = "select count(*) as Count from sch_protocol_visit where pk_protocol_visit = ?";
    	PreparedStatement pstmt  = null;
    	Connection conn =  null;
    	boolean flag = false;
    	try{
    		conn = getConnection();
    		pstmt = conn.prepareStatement(sql);
        	pstmt.setInt(1, visitPK);
        	
        	ResultSet rs = pstmt.executeQuery();
        			
        	while(rs.next()){
        		if(rs.getInt("Count") ==1)
				   flag = true;
				else 
				   flag =  false;
        	}
        	return flag;
    	}catch(Throwable t){
    		t.printStackTrace();
    		throw new OperationException();
    	}
    	
    	finally{
    		try{
    			if(pstmt!=null){
    				pstmt.close();
    			}
    		}catch(Exception e){
    			
    		}
    		try{
    			if(conn!=null){
    				conn.close();
    			}
    		}catch(Exception e){
    			
    		}
    	}
    }
    
    
    public static boolean ifScheduleExists(int patProtPK) throws OperationException{
    	
    	String sql = "select count(*) as Count from er_patprot where pk_patprot = ?";
    	PreparedStatement pstmt  = null;
    	Connection conn =  null;
    	boolean flag = false;
    	try{
    		conn = getConnection();
    		pstmt = conn.prepareStatement(sql);
        	pstmt.setInt(1, patProtPK);
        	
        	ResultSet rs = pstmt.executeQuery();
        			
        	while(rs.next()){
        		if(rs.getInt("Count") ==1)
				   flag = true;
				else 
				   flag =  false;
        	}
        	return flag;
    	}catch(Throwable t){
    		t.printStackTrace();
    		throw new OperationException();
    	}
    	
    	finally{
    		try{
    			if(pstmt!=null){
    				pstmt.close();
    			}
    		}catch(Exception e){
    			
    		}
    		try{
    			if(conn!=null){
    				conn.close();
    			}
    		}catch(Exception e){
    			
    		}
    	}
    }
    
    public static boolean ifStudyExists(int studyPK) throws OperationException{
    	
    	String sql = "select count(*) as Count from er_study where pk_study = ?";
    	PreparedStatement pstmt  = null;
    	Connection conn =  null;
    	boolean flag = false;
    	try{
    		conn = getConnection();
    		pstmt = conn.prepareStatement(sql);
        	pstmt.setInt(1, studyPK);
        	
        	ResultSet rs = pstmt.executeQuery();
        			
        	while(rs.next()){
        		if(rs.getInt("Count") ==1)
				   flag = true;
				else 
				   flag =  false;
        	}
        	return flag;
    	}catch(Throwable t){
    		t.printStackTrace();
    		throw new OperationException();
    	}
    	
    	finally{
    		try{
    			if(pstmt!=null){
    				pstmt.close();
    			}
    		}catch(Exception e){
    			
    		}
    		try{
    			if(conn!=null){
    				conn.close();
    			}
    		}catch(Exception e){
    			
    		}
    	}
    }
    
    
    public static boolean ifPatientExists(int patientPK) throws OperationException{
    	
    	String sql = "select count(*) as Count from er_per where pk_per = ?";
    	PreparedStatement pstmt  = null;
    	Connection conn =  null;
    	boolean flag = false;
    	try{
    		conn = getConnection();
    		pstmt = conn.prepareStatement(sql);
        	pstmt.setInt(1, patientPK);
        	
        	ResultSet rs = pstmt.executeQuery();
        			
        	while(rs.next()){
        		if(rs.getInt("Count") ==1)
				   flag = true;
				else 
				   flag =  false;
        	}
        	return flag;
    	}catch(Throwable t){
    		t.printStackTrace();
    		throw new OperationException();
    	}
    	
    	finally{
    		try{
    			if(pstmt!=null){
    				pstmt.close();
    			}
    		}catch(Exception e){
    			
    		}
    		try{
    			if(conn!=null){
    				conn.close();
    			}
    		}catch(Exception e){
    			
    		}
    	}
    }
    
    public static Integer getCurrentSchedule(Integer studyPK, Integer patientPK) throws OperationException
    {
    	String sql = "select p.pk_patprot from er_patprot p where p.fk_per = ? and p.fk_study = ? and p.patprot_stat = 1";
    	Integer schedulePK = null;
    	PreparedStatement stmt = null; 
    	Connection conn = null;
    	ResultSet rs = null;
    	try{
    		conn = getConnection();
    		stmt = conn.prepareStatement(sql);
    		stmt.setInt(1, patientPK);
    		stmt.setInt(2, studyPK);
    		
    		rs = stmt.executeQuery();
    		
    		while(rs.next())
    		{
    			schedulePK = rs.getInt(1);
    		}    		
    		
    	}catch(Throwable t){
        		t.printStackTrace();
        		throw new OperationException();
        	}
        	
        	finally{
        		try{
        			if(rs != null){
        				rs.close();
        			}
        		}catch(Exception e)
        		{
        			
        		}
        		
        
        		try{
        			if(stmt!=null){
        				stmt.close();
        			}
        		}catch(Exception e){
        			
        		}
        		try{
        			if(conn!=null){
        				conn.close();
        			}
        		}catch(Exception e){
        			
        		}
        	} 	
    	return schedulePK;
    }
    
    public Visit getVisit(Integer patprotPK, Integer scheventPk,ObjectMapService objectMapService) throws OperationException
    {
    	String sql = "SELECT v.pk_protocol_visit,v.VISIT_NAME FROM er_patprot p,sch_events1 s,sch_protocol_visit v,event_assoc e WHERE p.pk_patprot = ? AND p.pk_patprot = s.fk_patprot and s.fk_visit = v.pk_protocol_visit AND s.fk_assoc = e.event_id AND s.event_id = ?";
    	Visit visit = new Visit();
    	VisitIdentifier visitIdentifier = new VisitIdentifier();
    	PreparedStatement stmt = null; 
    	Connection conn = null;
    	ResultSet rs = null;
    	try{
    		conn = getConnection();
    		stmt = conn.prepareStatement(sql);
    		stmt.setInt(1, patprotPK);
    		stmt.setInt(2, scheventPk);
    		
    		rs = stmt.executeQuery();
    		
    		while(rs.next())
    		{
    			visit.setVisitName(rs.getString("VISIT_NAME"));
				ObjectMap mapVisitOID = objectMapService.getOrCreateObjectMapFromPK(objectMapService.PERSISTENCE_UNIT_VISIT, rs.getInt("PK_PROTOCOL_VISIT"));
				visitIdentifier.setPK(rs.getInt("PK_PROTOCOL_VISIT"));
				visitIdentifier.setOID(mapVisitOID.getOID());
				visit.setVisitIdentifier(visitIdentifier);
    		}    		
    		
    	}catch(Throwable t){
        		t.printStackTrace();
        		throw new OperationException();
        	}
        	
        	finally{
        		try{
        			if(rs != null){
        				rs.close();
        			}
        		}catch(Exception e)
        		{
        			
        		}
        		
        
        		try{
        			if(stmt!=null){
        				stmt.close();
        			}
        		}catch(Exception e){
        			
        		}
        		try{
        			if(conn!=null){
        				conn.close();
        			}
        		}catch(Exception e){
        			
        		}
        	} 	
    	return visit;
    }
    
    public Event getEvent(Integer patprotPK, Integer scheventPk,ObjectMapService objectMapService) throws OperationException
    {
    	String sql = "SELECT e.event_id ,e.name AS EVENT_NAME,s.start_date_time,s.actual_schdate,(SELECT MIN(sev.actual_schdate) FROM sch_events1 sev  WHERE sev.FK_VISIT=s.fk_visit AND s.FK_PATPROT  =sev.FK_PATPROT AND sev.SESSION_ID=s.session_id) actual_schdatesort,estat.EVENTSTAT_NOTES NOTES, s.service_site_id,(SELECT site.site_name FROM er_site site WHERE s.service_site_id = site.pk_site) AS SITE_OF_SERVICE,(SELECT code.codelst_desc FROM sch_codelst code WHERE s.fk_codelst_covertype = code.pk_codelst) AS COVERAGE_DESC,(SELECT code.codelst_subtyp FROM sch_codelst code WHERE s.fk_codelst_covertype = code.pk_codelst) AS COVERAGE_CODE,(SELECT code.codelst_type FROM sch_codelst code WHERE s.fk_codelst_covertype = code.pk_codelst) AS COVERAGE_TYPE,estat.EVENTSTAT_DT EVENTSTAT_DT,code.codelst_desc AS Event_Status,code.codelst_subtyp,s.EVENT_EXEON,s.ISCONFIRMED FROM er_patprot p,sch_events1 s,sch_protocol_visit v,sch_eventstat estat,sch_codelst code,event_assoc e WHERE  p.pk_patprot = ? AND p.pk_patprot = s.fk_patprot and s.fk_visit = v.pk_protocol_visit AND s.fk_assoc = e.event_id AND s.event_id = ? AND s.event_id = estat.fk_event AND estat.eventstat_enddt IS NULL AND estat.eventstat = code.pk_codelst ";
    	Event event = new Event();
    	EventStatus eventStatus = new EventStatus();
    	PreparedStatement stmt = null; 
    	Connection conn = null;
    	ResultSet rs = null;
    	try{
    		conn = getConnection();
    		stmt = conn.prepareStatement(sql);
    		stmt.setInt(1, patprotPK);
    		stmt.setInt(2, scheventPk);
    		
    		rs = stmt.executeQuery();
    		
    		while(rs.next())
    		{
    			event = new Event();
    			eventStatus = new EventStatus();
    			event.setEventName(rs.getString("EVENT_NAME"));
    			if(rs.getString("ACTUAL_SCHDATE") != null){
					event.setEventScheduledDate(rs.getString("ACTUAL_SCHDATE"));
				}
				else{
					event.setEventScheduledDate("Not Defined");
				}
    			event.setPK(rs.getInt("EVENT_ID"));
    			ObjectMap mapEventOID = objectMapService.getOrCreateObjectMapFromPK(objectMapService.PERSISTENCE_UNIT_STUDY_CALENDAR, rs.getInt("EVENT_ID"));
    			event.setOID(mapEventOID.getOID());
    			eventStatus.setStatusValidFrom(rs.getDate("EVENTSTAT_DT"));
    			eventStatus.setNotes(rs.getString("NOTES"));
    			Code eventStatusCode = new Code();

    			eventStatusCode.setCode(rs.getString("CODELST_SUBTYP"));
    			eventStatusCode.setDescription(rs.getString("EVENT_STATUS"));
    			eventStatus.setEventStatusCode(eventStatusCode);
    			event.setEventStatus(eventStatus);
    			
    		}    		
    		
    	}catch(Throwable t){
        		t.printStackTrace();
        		throw new OperationException();
        	}
        	
        	finally{
        		try{
        			if(rs != null){
        				rs.close();
        			}
        		}catch(Exception e)
        		{
        			
        		}
        		
        
        		try{
        			if(stmt!=null){
        				stmt.close();
        			}
        		}catch(Exception e){
        			
        		}
        		try{
        			if(conn!=null){
        				conn.close();
        			}
        		}catch(Exception e){
        			
        		}
        	} 	
    	return event;
    }
    
    //added for addMUnscheduledEvent
    public static EventdefBean getEventDef(int eventPK) throws OperationException{
    	
    	String sql = "select fk_codelst_covertype, reason_for_coveragechange, notes, service_site_id from event_def where event_id = ?";
    	EventdefBean eventDefBean = new EventdefBean();
    	PreparedStatement pstmt  = null;
    	Connection conn =  null;
    	try{
    		conn = getConnection();
    		pstmt = conn.prepareStatement(sql);
        	pstmt.setInt(1, eventPK);
        	
        	ResultSet rs = pstmt.executeQuery();
        			
        	while(rs.next()){
        		eventDefBean.setEventCoverageType(EJBUtil.integerToString(rs.getInt("FK_CODELST_COVERTYPE")));
        		eventDefBean.setEventSOSId(EJBUtil.integerToString(rs.getInt("SERVICE_SITE_ID")));
        		eventDefBean.setReasonForCoverageChange(rs.getString("REASON_FOR_COVERAGECHANGE"));
        		System.out.println("the valu for notes ::: " + rs.getString("NOTES"));
        		eventDefBean.setNotes(rs.getString("NOTES"));
        		
        	}
        	return eventDefBean;
    	}catch(Throwable t){
    		t.printStackTrace();
    		throw new OperationException();
    	}
    	
    	finally{
    		try{
    			if(pstmt!=null){
    				pstmt.close();
    			}
    		}catch(Exception e){
    			
    		}
    		try{
    			if(conn!=null){
    				conn.close();
    			}
    		}catch(Exception e){
    			
    		}
    	}
    }
    
    public static Map<Integer,Integer> getEventPK(int eventStatPK) throws OperationException{
    	
    	String sql = "select fk_event, eventstat from sch_eventstat where pk_eventstat = ?";
    	int eventPK = 0;
    	Map<Integer,Integer> mapEventStat = new HashMap<Integer,Integer>();
    	PreparedStatement pstmt  = null;
    	Connection conn =  null;
    	try{
    		conn = getConnection();
    		pstmt = conn.prepareStatement(sql);
        	pstmt.setInt(1, eventStatPK);
        	
        	ResultSet rs = pstmt.executeQuery();
        			
        	while(rs.next()){
        		
        		mapEventStat.put(1, rs.getInt("FK_EVENT"));
        		mapEventStat.put(2, rs.getInt("EVENTSTAT"));
        		
        	}
        	return mapEventStat;
    	}catch(Throwable t){
    		t.printStackTrace();
    		throw new OperationException();
    	}
    	
    	finally{
    		try{
    			if(pstmt!=null){
    				pstmt.close();
    			}
    		}catch(Exception e){
    			
    		}
    		try{
    			if(conn!=null){
    				conn.close();
    			}
    		}catch(Exception e){
    			
    		}
    	}
    }
    
    public int[] createMEventStatus(UserBean callingUser, List<Map<String, Object>> inputsList) throws OperationException
    {
      	
       //EventStatus eventStatus=null;
    	CallableStatement cstmt = null; 
    	Connection conn = null;
    	Map<String, Object> inputs=null;
    	try
    	{
    		conn = getSchConnection();
    		cstmt = conn.prepareCall("{call SP_CREATE_MUL_EVTS_STATUS(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
    		conn.setAutoCommit(false);
    		for(int i=0;i< inputsList.size();i++ )
    		{
    			//eventStatus=null;
    			inputs=null;
    			//eventStatus=multipleeventStatuses.getMeventStatus().get(i).getEventStatus(); 
    			inputs=inputsList.get(i);
    			cstmt.setInt(1, (Integer) inputs.get("event_id"));
    	        cstmt.setString(2, (String) inputs.get("notes"));
    	        cstmt.setDate(3,  DateUtil.dateToSqlDate((Date)inputs.get("exe_date")));
    	        //cstmt.setDate(3,  DateUtil.dateToSqlDate(eventStatus.getStatusValidFrom()));
    	        cstmt.setInt(4, callingUser.getUserId());
    	        cstmt.setInt(5, (Integer) inputs.get("status"));
    	        cstmt.setString(6, callingUser.getIpAdd());
    	        cstmt.setString(7, "N");
    	        cstmt.setInt(8, (Integer) inputs.get("eventSOSId"));
    	        cstmt.setInt(9, (Integer) inputs.get("eventCoverType"));
    	        cstmt.setString(10, (String) inputs.get("reasonForCoverageChange"));
    	        cstmt.setInt(11,(Integer) inputs.get("oldStatus"));
    	        cstmt.setString(12, (String) inputs.get("notes"));
    	        cstmt.setString(13, (String) inputs.get("eventPKString"));
    	        cstmt.setString(14, (String) inputs.get("reasonForChange"));
    	        cstmt.addBatch();
    		} 
    		int [] numUpdates=cstmt.executeBatch();  
    		conn.commit();
    		return numUpdates;
    		
    	  }catch(SQLException sqe)
    	  {
    		if (logger.isDebugEnabled()) logger.debug("PatientScheduleDAO getStudyPKForAdminCal", sqe);
    		sqe.printStackTrace();
    		throw new OperationException(sqe.getMessage());	  
    		}finally
    		  {
    			try {
    	                if (cstmt != null)
    	                    cstmt.close();
    	            } catch (Exception e) {
    	            }
    	            try {
    	                  if (conn != null)
    	                	  conn.close();
    	              } catch (Exception exception2) {
    	              }
    		}
    	}
    
    public static boolean isVisitValidForAdminSchedule(int visitPK, int schedulePK) throws OperationException
    {
    	String sql = "select pk_protocol_visit from sch_protocol_visit where fk_protocol = ? and pk_protocol_visit = ?"; 

    	PreparedStatement pstmt  = null;
    	Connection conn =  null;
    	ResultSet rs = null;
    	boolean isVisitValid = false;
    	try{
    		conn = getConnection();
    		pstmt = conn.prepareStatement(sql);
    		
    		pstmt.setInt(1, schedulePK);
    		pstmt.setInt(2, visitPK);
    		
    		rs = pstmt.executeQuery();
    		if(rs.next())
    		{
    			isVisitValid = true; 
    		}
    	}catch(Throwable t){
    		t.printStackTrace();
    		throw new OperationException();
    	}
    	
    	finally{
    		try{
    			if(pstmt!=null){
    				pstmt.close();
    			}
    		}catch(Exception e){
    			
    		}
    		try{
    			if(conn!=null){
    				conn.close();
    			}
    		}catch(Exception e){

    		}

    	}
    	
    	return isVisitValid; 
    }
    
    public static Integer getCalPK(Integer studyPK, String calName) throws OperationException{
    	
    	String sql = "select event_id from event_assoc where chain_id = ? and name = ?";
    	
    	PreparedStatement pstmt = null;
    	Connection conn = null;
    	ResultSet rs = null;
    	Integer calPK = 0;
    	try{
    		conn = getConnection();
    		pstmt = conn.prepareStatement(sql);
    		
    		pstmt.setInt(1, studyPK);
    		pstmt.setString(2, calName);
    		
    		rs = pstmt.executeQuery();
    		while(rs.next()){
    			calPK = rs.getInt("EVENT_ID");
    		}
    	}catch(Throwable t){
    		t.printStackTrace();
    		throw new OperationException();
    	}finally{
    		try{
    			if(pstmt != null){
    				pstmt.close();
    			}
    		}catch(Exception e){
    			
    		}try{
    			if(conn != null){
    				conn.close();
    			}
    		}catch(Exception e){
    			
    		}
    	}
    	return calPK;
    }
    
   
    public static Integer getProtocolPK(Integer patientPK, Integer studyPK) throws OperationException{
    	
    	String sql = "select fk_protocol from er_patprot where fk_per = ? and fk_study = ? and patprot_stat = 1";
    	
    	PreparedStatement pstmt = null;
    	Connection conn = null;
    	ResultSet rs = null;
    	Integer protocolPK = 0;
    	try{
    		conn = getConnection();
    		pstmt = conn.prepareStatement(sql);
    		
    		pstmt.setInt(1, patientPK);
    		pstmt.setInt(2, studyPK);
    		
    		rs = pstmt.executeQuery();
    		while(rs.next()){
    			protocolPK = rs.getInt("FK_PROTOCOL");
    		}
    	}catch(Throwable t){
    		t.printStackTrace();
    		throw new OperationException();
    	}finally{
    		try{
    			if(pstmt != null){
    				pstmt.close();
    			}
    		}catch(Exception e){
    			
    		}try{
    			if(conn != null){
    				conn.close();
    			}
    		}catch(Exception e){
    			
    		}
    	}
    	return protocolPK;
    }
    
    //Tarandeep Singh Bali
    public static EventStatusHistory getEventStatusHistory(Integer eventPK, String sortBy, String orderBy) throws OperationException{
    	
    	EventStatusHistory eventStatusHistory = new EventStatusHistory();
    	EventStatuses eventStatuses = new EventStatuses();
    	List<EventStatusDetails> eventStatusDetailsList = new ArrayList<EventStatusDetails>();
    	ObjectMapService objectMapService = ServicesUtil.getObjectMapService();
    	ObjectMap objMap;
    	PreparedStatement pstmt = null;
    	Connection conn = null;
    	ResultSet rs = null;
    	
    	try{
    		String getEventStatusHistorySql2 = getEventStatusHistorySql.concat("order by " + sortBy + " " +orderBy);
    		conn = getConnection();
    		pstmt = conn.prepareStatement(getEventStatusHistorySql2);
    		pstmt.setInt(1, eventPK);
//    		pstmt.setString(2, sortBy);
//    		pstmt.setString(3, orderBy);
    		rs = pstmt.executeQuery();
    		while(rs.next()){
    			EventStatusDetails eventStatusDetails = new EventStatusDetails();
    			EventStatusIdentifier eventStatusIdentifier = new EventStatusIdentifier();
    			EventStatus eventStatus = new EventStatus();
    			User user = new User();
    			eventStatusIdentifier.setPK(rs.getInt("PK_EVENTSTAT"));
    			objMap = objectMapService.getOrCreateObjectMapFromPK(objectMapService.PERSISTENCE_UNIT_EVENT_STATUS_TABLE, rs.getInt("PK_EVENTSTAT"));
    			eventStatusIdentifier.setOID(objMap.getOID());
    			eventStatusDetails.setEventStatusIdentifier(eventStatusIdentifier);
    			eventStatus.setEventName(rs.getString("EVENT_STATUS"));
    			eventStatusDetails.setEventStatus(eventStatus);
    			eventStatusDetails.setStartDate(rs.getDate("EVENTSTAT_DT"));
    			eventStatusDetails.setEndDate(rs.getDate("EVENTSTAT_ENDDT"));
    			user.setPK(rs.getInt("CREATOR_PK"));
    			user.setFirstName(rs.getString("FNAME"));
    			user.setLastName(rs.getString("LNAME"));
    			eventStatusDetails.setUser(user);
    			eventStatusDetailsList.add(eventStatusDetails);
    		}
    		eventStatuses.setEventStatusDetails(eventStatusDetailsList);
    		eventStatusHistory.setEventStatuses(eventStatuses);
    	}catch(Throwable t){
    		t.printStackTrace();
    		throw new OperationException();
    	}finally{
    		try{
    			if(pstmt != null){
    				pstmt.close();
    			}
    		}catch(Exception e){
    			
    		}try{
    			if(conn != null){
    				conn.close();
    			}
    		}catch(Exception e){
    			
    		}
    	}
    	return eventStatusHistory;
    }
    
public static Integer getStudyPKFromEventPK(Integer eventPK) throws OperationException{
    	
    	String sql = "select chain_id from event_assoc where event_id = (select chain_id from event_assoc where event_id = (select fk_assoc from sch_events1 where event_id = ?))";
    	
    	PreparedStatement pstmt = null;
    	Connection conn = null;
    	ResultSet rs = null;
    	Integer studyPK = 0;
    	try{
    		conn = getConnection();
    		pstmt = conn.prepareStatement(sql);
    		
    		pstmt.setInt(1, eventPK);
    		
    		rs = pstmt.executeQuery();
    		while(rs.next()){
    			studyPK = rs.getInt("CHAIN_ID");
    		}
    	}catch(Throwable t){
    		t.printStackTrace();
    		throw new OperationException();
    	}finally{
    		try{
    			if(pstmt != null){
    				pstmt.close();
    			}
    		}catch(Exception e){
    			
    		}try{
    			if(conn != null){
    				conn.close();
    			}
    		}catch(Exception e){
    			
    		}
    	}
    	return studyPK;
    }
//Bug Fix : 15305
public static boolean ifFDAStudy(int studyPK) throws OperationException{
	
	String sql = "select fda_regulated_study from er_study where pk_study = ?";
	PreparedStatement pstmt  = null;
	Connection conn =  null;
	boolean flag = false;
	try{
		conn = getConnection();
		pstmt = conn.prepareStatement(sql);
    	pstmt.setInt(1, studyPK);
    	
    	ResultSet rs = pstmt.executeQuery();
    			
    	while(rs.next()){
    		if(rs.getInt("FDA_REGULATED_STUDY") == 1)
			   flag = true;
			else 
			   flag =  false;
    	}
    	return flag;
	}catch(Throwable t){
		t.printStackTrace();
		throw new OperationException();
	}
	
	finally{
		try{
			if(pstmt!=null){
				pstmt.close();
			}
		}catch(Exception e){
			
		}
		try{
			if(conn!=null){
				conn.close();
			}
		}catch(Exception e){
			
		}
	}
 }

	public static JSONArray getPatientSchedulesByStudy(HttpServletRequest request, HashMap<String, String> jParams)
	{
		PreparedStatement pstmt = null;
	    Connection conn = null;
	    String FK_STUDY_KEY = "fk_study";
	    String STUDYNUMBER_KEY = "studyNumber";
	    String STUDYACCESS_KEY = "studyAccess";
	    String PATIENTACCESS_KEY = "patientAccess";
	    String FK_PER_KEY = "fk_per";
	    String PATIENTID_KEY = "patientId";
	    String PK_PATPROT_KEY = "patprotId";
	    String PATSTUDYID_KEY = "patientStudyId";
	    String PK_PATSTUDYSTATUS_KEY = "pk_patStudyStatus";
	    String PATSTUDYSTAT_DESC_KEY = "patStudyStatus";
	    String PATSTUDYSTAT_SUBTYP_KEY = "patStudyStatusSubType";
	    String PATSTUDYSTAT_REASON_KEY = "patStudyStatusReason";
	    String PATSTUDYSTAT_DATE_KEY = "patStudyStatusDate";
	    String PATSTUDYSTA_NOTE_KEY = "patStudyStatusNote";
	    String VISITNAME_KEY = "visitName";
	    String VISITDATE_KEY = "visitDate";
	    String VISITSTATUS_KEY = "visitStatus";
	    int rowCount = 0;
	    
		JSONArray jArray = new JSONArray();
	
		try {
	    	conn = getConnection();
	
	    	Patient pat=new Patient();
			pat.getAllSchedulesSQL(request);
			String mainSQL=pat.getMainSQL();
	
			String userId = jParams.get("userId");
			String studyIds = jParams.get("studyIds");
			if (!StringUtil.isEmpty(studyIds)){
				mainSQL += " AND fk_study in ("+ studyIds +")";
			}
	
			String sortBy = jParams.get("sortBy");
			sortBy = (PATSTUDYID_KEY.equals(sortBy))? "1" :  
						(STUDYNUMBER_KEY.equals(sortBy))? "2" : "0"; 
	
			switch (StringUtil.stringToNum(sortBy)){
				case 0:
					mainSQL += " ORDER BY per_code";
					break;
				case 1:
					mainSQL += " ORDER BY PATPROT_PATSTDID";
					break;
				case 2:
					mainSQL += " ORDER BY study_number";
					break;
			}
			String sortDir = jParams.get("sortDir");
			if (null != sortDir){
				mainSQL += " "+sortDir;
			}
	    	
			pstmt = conn.prepareStatement(mainSQL);
	        ResultSet rs = pstmt.executeQuery();
	
	        while (rs.next()) {
	        	rowCount++;
	        	if (rowCount > 10) break;
	
			  	String studyId = rs.getString("FK_STUDY");
				StudyDao studyDao = new StudyDao();
				studyDao.getStudy(StringUtil.stringToNum(studyId));
				
				JSONObject aSchedule = new JSONObject();
				aSchedule.put(FK_STUDY_KEY, studyId);
				aSchedule.put(STUDYNUMBER_KEY, String.valueOf(studyDao.getStudyNumbers().get(0)));			
				
		    	TeamDao tDao = new TeamDao();
		    	tDao.getTeamRights(StringUtil.stringToNum(studyId), StringUtil.stringToNum(userId));
		    	if ((tDao.getTeamIds()).size() == 0){
					aSchedule.put(STUDYACCESS_KEY, "0");
		    	} else {
					aSchedule.put(STUDYACCESS_KEY, "7");
					
					int patientRight = StringUtil.stringToNum(rs.getString("RIGHT_MASK"));
					aSchedule.put(PATIENTACCESS_KEY, ""+patientRight);
					
					String detail = "";
					detail = rs.getString("FK_PER");
					detail = (StringUtil.isEmpty(detail))? "": detail;
		    		aSchedule.put(FK_PER_KEY, detail);
		    		
		    		detail = rs.getString("PER_CODE");
					detail = (StringUtil.isEmpty(detail))? "": detail;
		    		aSchedule.put(PATIENTID_KEY, detail);
	
		    		detail = rs.getString("PK_PATPROT");
					detail = (StringUtil.isEmpty(detail))? "": detail;
					aSchedule.put(PK_PATPROT_KEY, detail);
					
					detail = rs.getString("PATPROT_PATSTDID");
					detail = (StringUtil.isEmpty(detail))? "": detail;
					aSchedule.put(PATSTUDYID_KEY, detail);
					
					detail = rs.getString("PK_PATSTUDYSTAT");
					detail = (StringUtil.isEmpty(detail))? "": detail;
					aSchedule.put(PK_PATSTUDYSTATUS_KEY, detail);
					
					detail = rs.getString("PATSTUDYSTAT_DESC");
					detail = (StringUtil.isEmpty(detail))? "": detail;
		    		aSchedule.put(PATSTUDYSTAT_DESC_KEY, detail);
		    		
		    		detail = rs.getString("PATSTUDYSTAT_SUBTYPE");
					detail = (StringUtil.isEmpty(detail))? "": detail;
		    		aSchedule.put(PATSTUDYSTAT_SUBTYP_KEY, detail);
		    		
		    		detail = rs.getString("PATSTUDYSTAT_REASON");
					detail = (StringUtil.isEmpty(detail))? "": detail;
		    		aSchedule.put(PATSTUDYSTAT_REASON_KEY, detail);
		    		
		    		detail = rs.getString("PATSTUDYSTAT_DATE_DATESORT");
					detail = (StringUtil.isEmpty(detail))? "": detail;
		    		aSchedule.put(PATSTUDYSTAT_DATE_KEY, detail.trim());
		    		
		    		detail = rs.getString("PATSTUDYSTAT_NOTE");
					detail = (StringUtil.isEmpty(detail))? "": detail;
		    		aSchedule.put(PATSTUDYSTA_NOTE_KEY, detail);
	
		    		detail = rs.getString("NEXT_VISIT_NAME");
					detail = (StringUtil.isEmpty(detail))? "": detail;
		    		aSchedule.put(VISITNAME_KEY, detail);
		    		
		    		detail = rs.getString("NEXT_VISIT_DATESORT");
					detail = (StringUtil.isEmpty(detail))? "": detail;
					if (detail.indexOf(" ") > -1)
						detail = ""+detail.subSequence(0, detail.indexOf(" "));
					detail = DateUtil.format2DateFormat(detail);
		    		aSchedule.put(VISITDATE_KEY, detail);
		    		
		    		aSchedule.put(VISITSTATUS_KEY, "1");
		    	}
		    	try {
					jArray.put(aSchedule);
				} catch(Exception e) {
					Rlog.fatal("patSchedule", "jArray.put error: "+e);
				}
			}
			request.setAttribute("gdtPatSchCount", ""+rowCount);
	        return jArray;
	    } 
	    catch(Exception e){
	    	e.printStackTrace();
	    	return null;
		}
		finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
				} catch (Exception e) {
			}
		}    	
	}
}
