package com.velos.services.client;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.milestone.MilestoneService;
import com.velos.services.model.Milestone;
import com.velos.services.model.MilestoneList;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.util.JNDINames;

public class MilestoneClient {
	private static ResponseHolder response = new ResponseHolder();
	
	private static MilestoneService getMilestoneRemote() throws OperationException
	{
		MilestoneService mileService = null;
		InitialContext ic;
		try
		{
			ic = new InitialContext();
			mileService =(MilestoneService) ic.lookup(JNDINames.MilestoneServiceImpl);
		}
		catch(NamingException e){
			throw new OperationException(e);
		}
		return mileService;
	}
	
	public static Milestone getStudyMilestones(StudyIdentifier studyIdent) throws OperationException
	{
		MilestoneService milestoneService = getMilestoneRemote();
		return milestoneService.getStudyMilestones(studyIdent);
	}
	
	public static ResponseHolder createMMilestones(MilestoneList milestone) throws OperationException {
		MilestoneService milestoneService = getMilestoneRemote();
		return milestoneService.createMMilestones(milestone);
	}

}
