package com.velos.services.client;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.Groups;
import com.velos.services.model.NetworkLevelSite;
import com.velos.services.model.NetworkSiteDetails;
import com.velos.services.model.NetworkSiteLevelDetails;
//import com.velos.services.model.NetworkSiteLevelSearchResult;
import com.velos.services.model.NetworkSiteSearchResults;
import com.velos.services.model.NetworkSiteUserDetails;
import com.velos.services.model.Networks;
import com.velos.services.model.NetworksDetails;
import com.velos.services.model.NonSystemUser;
import com.velos.services.model.OrganizationDetail;
import com.velos.services.model.OrganizationSearch;
import com.velos.services.model.OrganizationSearchResults;
import com.velos.services.model.Organizations;
import com.velos.services.model.SiteLevelDetails;
import com.velos.services.model.User;
import com.velos.services.model.User.UserStatus;
import com.velos.services.model.UserIdentifier;
import com.velos.services.model.UserNetworkSiteResults;
import com.velos.services.model.UserNetworkSites;
import com.velos.services.model.UserNetworksDetail;
import com.velos.services.model.UserSearch;
import com.velos.services.model.UserSearchResults;
import com.velos.services.user.UserService;
import com.velos.services.util.JNDINames;
import com.velos.services.model.NetworkUserDetail;
import com.velos.services.model.NetworkUserDetails;
import com.velos.services.model.NetworkUsersSearchResult;
/***
 * Client Class/Remote object for User Services 
 */
public class UserClient
{
	
	private static ResponseHolder response = new ResponseHolder();
	public static ResponseHolder changeUserStatus(UserIdentifier uId, UserStatus uStat)throws OperationException 
	{
		
		UserService userService= getUserRemote();
		if(userService == null){
			Issue issue = new Issue(IssueTypes.EXECPTION_GETTING_REMOTE_USER_SERVICES, 
					"Exception getting remote User service:"+userService);
			response.addIssue(issue);
			throw new OperationException();
		}
		return userService.changeUserStatus(uId, uStat);
	}
	public static Groups getAllGroups() throws OperationException 
	{
		UserService userService= getUserRemote();
		if(userService == null){
			Issue issue = new Issue(IssueTypes.EXECPTION_GETTING_REMOTE_USER_SERVICES, 
					"Exception getting remote User service:"+userService);
			response.addIssue(issue);
			throw new OperationException();
		}
		return userService.getAllGroups();
	}
	
	public static Groups getUserGroups(UserIdentifier userId) throws OperationException
	{
		UserService userService = getUserRemote();
		return userService.getUserGroups(userId);
		
	}
	
	public static ResponseHolder createNonSystemUser(NonSystemUser nonsystemuser)throws OperationException 
	{
		
		UserService userService= getUserRemote();
		if(userService == null){
			Issue issue = new Issue(IssueTypes.EXECPTION_GETTING_REMOTE_USER_SERVICES, 
					"Exception getting remote User service:"+userService);
			response.addIssue(issue);
			throw new OperationException();
		}
		return userService.createNonSystemUser(nonsystemuser);
	}

		
	private static UserService getUserRemote() throws OperationException
	{
		UserService userService = null;
		InitialContext ic;
		try
		{
			ic = new InitialContext();
			userService =(UserService) ic.lookup(JNDINames.UserServiceImpl);
		}
		catch(NamingException e){
			throw new OperationException(e);
		}
		return userService;
	}

	public static  Organizations getAllOrganizations() throws OperationException 
	{
		UserService userService= getUserRemote();
		if(userService == null){
			Issue issue = new Issue(IssueTypes.EXECPTION_GETTING_REMOTE_USER_SERVICES, 
					"Exception getting remote User service:"+userService);
			response.addIssue(issue);
			throw new OperationException();
		}
		return userService.getAllOrganizations();
	}
	
	public static ResponseHolder killUserSession(UserIdentifier userId)throws OperationException 
	{
		
		UserService userService= getUserRemote();
		if(userService == null){
			Issue issue = new Issue(IssueTypes.EXECPTION_GETTING_REMOTE_USER_SERVICES, 
					"Exception getting remote User service:"+userService);
			response.addIssue(issue);
			throw new OperationException();
		}
		return userService.killUserSession(userId);
	}
	public static ResponseHolder createUser(User user)throws OperationException 
	{
		
		UserService userService= getUserRemote();
		if(userService == null){
			Issue issue = new Issue(IssueTypes.EXECPTION_GETTING_REMOTE_USER_SERVICES, 
					"Exception getting remote User service:"+userService);
			response.addIssue(issue);
			throw new OperationException();
		}
		return userService.createUser(user);
	}
	public static ResponseHolder updateUserDetails(User user)throws OperationException 
	{
		
		UserService userService= getUserRemote();
		if(userService == null){
			Issue issue = new Issue(IssueTypes.EXECPTION_GETTING_REMOTE_USER_SERVICES, 
					"Exception getting remote User service:"+userService);
			response.addIssue(issue);
			throw new OperationException();
		}
		return userService.updateUserDetails(user);
	}
	public static ResponseHolder createOrganization(OrganizationDetail org)throws OperationException
	{
		UserService userService= getUserRemote();
		if(userService == null){
			Issue issue = new Issue(IssueTypes.EXECPTION_GETTING_REMOTE_USER_SERVICES, 
					"Exception getting remote User service:"+userService);
			response.addIssue(issue);
			throw new OperationException();
		}
		return userService.createOrganization(org);
	}
		
	public static ResponseHolder updateOrganisation(OrganizationDetail organizationDetail)throws OperationException 
	{
		
		UserService userService= getUserRemote();
		if(userService == null){
			Issue issue = new Issue(IssueTypes.EXECPTION_GETTING_REMOTE_USER_SERVICES, 
					"Exception getting remote User service:"+userService);
			response.addIssue(issue);
			throw new OperationException();
		}
		return userService.updateOrganisation(organizationDetail);
	}
	
	public static OrganizationSearchResults searchOrganisations(OrganizationSearch organizationSearch)throws OperationException 
	{
		
		UserService userService= getUserRemote();
		if(userService == null){
			Issue issue = new Issue(IssueTypes.EXECPTION_GETTING_REMOTE_USER_SERVICES, 
					"Exception getting remote User service:"+userService);
			response.addIssue(issue);
			throw new OperationException();
		}
		return userService.searchOrganisations(organizationSearch);
	}
	
	
	
	/**
	 * @param eSignature
	 * @return
	 */
	public static ResponseHolder checkESignature(String eSignature) throws OperationException{
	    UserService userService = getUserRemote(); 
	    if(userService == null)
	    {
	    	Issue issue = new Issue(IssueTypes.EXECPTION_GETTING_REMOTE_USER_SERVICES, 
					"Exception getting remote User service:"+userService);
			response.addIssue(issue);
			throw new OperationException();
	    }
		return userService.checkESignature(eSignature);
	}
	
	public static UserSearchResults searchUser(UserSearch userSearch) throws OperationException {
	    UserService userService = getUserRemote(); 
	    if(userService == null)
	    {
	    	Issue issue = new Issue(IssueTypes.EXECPTION_GETTING_REMOTE_USER_SERVICES, 
					"Exception getting remote User service:"+userService);
			response.addIssue(issue);
			throw new OperationException();
	    }
		return userService.searchUser(userSearch);
	}
	
	public static NetworkSiteSearchResults getNetworkSiteDetails(NetworkSiteDetails networkSiteDetails) throws OperationException {
	    UserService userService = getUserRemote(); 
	    if(userService == null)
	    {
	    	Issue issue = new Issue(IssueTypes.EXECPTION_GETTING_REMOTE_USER_SERVICES, 
					"Exception getting remote User service:"+userService);
			response.addIssue(issue);
			throw new OperationException();
	    }
		return userService.getNetworkSiteDetails(networkSiteDetails);
	}
	
	public static Networks searchNetworks(NetworksDetails networkD) throws OperationException {
	    UserService userService = getUserRemote(); 
	    if(userService == null)
	    {
	    	Issue issue = new Issue(IssueTypes.EXECPTION_GETTING_REMOTE_USER_SERVICES, 
					"Exception getting remote User service:"+userService);
			response.addIssue(issue);
			throw new OperationException(IssueTypes.EXECPTION_GETTING_REMOTE_USER_SERVICES+ 
					"Exception getting remote User service:"+userService);
	    }
		return userService.searchNetworks(networkD);
	}
	
	public static NetworkUsersSearchResult getUserNetwork(NetworkUserDetail networkuser) throws OperationException {
	    UserService userService = getUserRemote(); 
	    if(userService == null)
	    {
	    	Issue issue = new Issue(IssueTypes.EXECPTION_GETTING_REMOTE_USER_SERVICES, 
					"Exception getting remote User service:"+userService);
			response.addIssue(issue);
			throw new OperationException();
	    }
		return userService.networkuser(networkuser);
	}
	
	public static NetworkUsersSearchResult getNetworkUserDetails(NetworkUserDetails networkUserDetails) throws OperationException {
	    UserService userService = getUserRemote(); 
	    if(userService == null)
	    {
	    	Issue issue = new Issue(IssueTypes.EXECPTION_GETTING_REMOTE_USER_SERVICES, 
					"Exception getting remote User service:"+userService);
			response.addIssue(issue);
			throw new OperationException();
	    }
		return userService.getNetworkUserDetails(networkUserDetails);
	}
	public static NetworkSiteSearchResults getNetworkSiteChildren(SiteLevelDetails siteLevelDetails) throws OperationException {
		// TODO Auto-generated method stub
		UserService userService = getUserRemote(); 
	    if(userService == null)
	    {
	    	Issue issue = new Issue(IssueTypes.EXECPTION_GETTING_REMOTE_USER_SERVICES, 
					"Exception getting remote User service:"+userService);
			response.addIssue(issue);
			throw new OperationException();
	    }
		return userService.getNetworkSiteChildren(siteLevelDetails);
	}
	public static NetworkSiteSearchResults getNetworkSiteParent(SiteLevelDetails siteLevelDetails) throws OperationException {
		// TODO Auto-generated method stub
		UserService userService = getUserRemote(); 
	    if(userService == null)
	    {
	    	Issue issue = new Issue(IssueTypes.EXECPTION_GETTING_REMOTE_USER_SERVICES, 
					"Exception getting remote User service:"+userService);
			response.addIssue(issue);
			throw new OperationException();
	    }
		return userService.getNetworkSiteParent(siteLevelDetails);
	}
	
	public static NetworkSiteSearchResults getNetworkLevelSites(NetworkLevelSite networklevelsite) throws OperationException {
	    UserService userService = getUserRemote(); 
	    if(userService == null)
	    {
	    	Issue issue = new Issue(IssueTypes.EXECPTION_GETTING_REMOTE_USER_SERVICES, 
					"Exception getting remote User service:"+userService);
			response.addIssue(issue);
			throw new OperationException();
	    }
		return userService.getNetworkLevelSites(networklevelsite);
	}	
		
	public static NetworkSiteSearchResults getNetworkSiteLevel(NetworkSiteLevelDetails networkSiteLevelDetails) throws OperationException {
	    UserService userService = getUserRemote(); 
	    if(userService == null)
	    {
	    	Issue issue = new Issue(IssueTypes.EXECPTION_GETTING_REMOTE_USER_SERVICES, 
					"Exception getting remote User service:"+userService);
			response.addIssue(issue);
			throw new OperationException();
	    }
		return userService.getNetworkSiteLevel(networkSiteLevelDetails);
	}
	
	public static UserNetworkSiteResults getUserNetworkSites(UserNetworkSites userNetworkSites) throws OperationException {
	    UserService userService = getUserRemote(); 
	    if(userService == null)
	    {
	    	Issue issue = new Issue(IssueTypes.EXECPTION_GETTING_REMOTE_USER_SERVICES, 
					"Exception getting remote User service:"+userService);
			response.addIssue(issue);
			throw new OperationException();
	    }
		return userService.getUserNetworkSites(userNetworkSites);
	}
	public static NetworkUsersSearchResult getUserNetworks(UserNetworksDetail networkuser) throws OperationException {
	    UserService userService = getUserRemote(); 
	    if(userService == null)
	    {
	    	Issue issue = new Issue(IssueTypes.EXECPTION_GETTING_REMOTE_USER_SERVICES, 
					"Exception getting remote User service:"+userService);
			response.addIssue(issue);
			throw new OperationException();
	    }
		return userService.networkusers(networkuser);
	}
	public static UserNetworkSiteResults getNetworkUser(NetworkSiteUserDetails networkSiteUserDetails)
			throws OperationException {
		UserService userService = getUserRemote();
		if (userService == null) {
			Issue issue = new Issue(IssueTypes.EXECPTION_GETTING_REMOTE_USER_SERVICES,
					"Exception getting remote User service:" + userService);
			response.addIssue(issue);
			throw new OperationException();
		}
		return userService.getNetworkUser(networkSiteUserDetails);
	}
	
	public static void refreshMenuTabs() throws OperationException
	{
		UserService userService= getUserRemote();
		if(userService == null){
			Issue issue = new Issue(IssueTypes.EXECPTION_GETTING_REMOTE_USER_SERVICES, 
					"Exception getting remote User service:"+userService);
			response.addIssue(issue);
			throw new OperationException();
		}
		userService.refreshMenuTabs();
		
	}
}