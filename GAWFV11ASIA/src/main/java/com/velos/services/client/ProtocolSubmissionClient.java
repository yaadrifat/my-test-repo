package com.velos.services.client;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.ReviewBoardsIdentifier;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.protocolsubmission.ProtocolSubmissionService;
import com.velos.services.util.JNDINames;

public class ProtocolSubmissionClient {

	public static ResponseHolder submitProtocolVersion(ReviewBoardsIdentifier reviewBoardsIdentifier)
	throws OperationException{
		ProtocolSubmissionService psService = getProtcolSubmissionHome();
		return psService.submitProtocolVersion(reviewBoardsIdentifier);
	}
	public static ProtocolSubmissionService getProtcolSubmissionHome(){
		ProtocolSubmissionService psService=null;
		InitialContext ic;
		try{
			ic = new InitialContext();
			psService = (ProtocolSubmissionService) ic.lookup(JNDINames.ProtocolSubmissionHome);
		}catch(NamingException ne){
			ne.printStackTrace();
		}
		return psService;
	}
}
