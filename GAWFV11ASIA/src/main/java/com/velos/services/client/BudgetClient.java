package com.velos.services.client;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.budget.BudgetService;
import com.velos.services.model.BudgetDetail;
import com.velos.services.model.BudgetIdentifier;
import com.velos.services.model.BudgetStatus;
import com.velos.services.model.CalendarNameIdentifier;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.util.JNDINames;

public class BudgetClient {
	
	private static BudgetService getBudgetRemote()
	throws OperationException{
		
	BudgetService BudgetRemote = null;
	InitialContext ic;
	try{
		ic = new InitialContext();
		BudgetRemote =
			(BudgetService) ic.lookup(JNDINames.BudgetServiceImpl);
		}
		catch(NamingException e){
			throw new OperationException(e);
		}
		return BudgetRemote;
	}
	
	public static BudgetStatus getBudgetStatus(BudgetIdentifier budgetIdentifier) throws OperationException{
		BudgetService service = getBudgetRemote(); 
		return service.getBudgetStatus(budgetIdentifier);
	}
	
	public static BudgetDetail getStudyCalBudget(StudyIdentifier studyIdent,CalendarNameIdentifier CalIdent) throws OperationException
	{
		BudgetService budgetService = getBudgetRemote();
		return budgetService.getStudyCalBudget(studyIdent,CalIdent);
		 
	}
	
	public static ResponseHolder createStudyCalBudget(BudgetDetail budgetDetail) throws OperationException {
		BudgetService budgetService = getBudgetRemote();
		return budgetService.createStudyCalBudget(budgetDetail);
	}

}
