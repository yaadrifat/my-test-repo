/**
 * Created On Mar 16, 2011
 */
package com.velos.services.outbound;

import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicSubscriber;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;

import com.velos.services.OperationException;
import com.velos.services.monitoring.MessageLog;
import com.velos.services.monitoring.MessageService;
import com.velos.services.util.JNDINames;

/**
 * @author Kanwaldeep
 *
 */
public class Auditor implements MessageListener,ExceptionListener, Runnable{
	
	private static Logger logger = Logger.getLogger(Auditor.class); 
	
	private static Map<Thread, Auditor> auditorThread = new HashMap<Thread, Auditor>(); 
	
	private MessageLog messageLog; 
	
	private MessageService messageService; 
	
	private String topicJNDI; 
	
	private String connectionJNDI; 
	
	private String subscriberName = "Auditor"; 
	
	private String clientID = "Auditor"; 
	
	private String username; 
	
	private String password; 
	
	private Connection conn; 
	
	private boolean runningStatus = true;  

	public Auditor(){
	
	}

	/* (non-Javadoc)
	 * @see javax.jms.MessageListener#onMessage(javax.jms.Message)
	 */
	public void onMessage(Message message) {
		if (message instanceof TextMessage) {
            
        	TextMessage xml = (TextMessage) message; 
        	String xmlString = null;
			try {		
				xmlString = xml.getText();			
				messageLog = new MessageLog(); 
				messageLog.setBody(xmlString); 
				messageLog.setEndpoint(topicJNDI); 
				messageLog.setSuccessFlag(true); 
				messageLog.setRoute("OUTBOUND"); 
				messageLog.setTime(new Date(xml.getJMSTimestamp())); 
				messageLog.setMessageId(xml.getJMSMessageID()); 
				messageLog.setOperation("OutBound message"); 
				messageLog.setDuration(System.currentTimeMillis() - xml.getJMSTimestamp());
				messageService = getMessageService(); 
				messageService.setMessageDetails(messageLog); 
				
			} catch (JMSException e) {
			
				e.printStackTrace();
			} catch (NamingException e) {
				
				e.printStackTrace();
			}catch(Exception e){
				
			}

            System.out.println("Auditor durable subscription got Outbound message. The information is:" +xmlString);
	}
	}

	/* (non-Javadoc)
	 * @see javax.jms.ExceptionListener#onException(javax.jms.JMSException)
	 */
	public void onException(JMSException arg0) {

			System.out.println(arg0); 
		
	}
	
	private MessageService getMessageService() throws NamingException{
		if (messageService == null){
			InitialContext ic;
			try{
				ic = new InitialContext();
				messageService =
					(MessageService) ic.lookup(JNDINames.MessageServiceImpl);
				 
			}
			catch(NamingException e){
				throw e;
			}
		}
		return messageService;
	}
	
	
	public static void startAuditor()
	{
		MessagingAccountDAO dao = new MessagingAccountDAO(); 
		try {
			List<String> auditorinfo = dao.getTopicInformationForSubscribers();
			System.out.println("Starting auditor for ");
			
			int j = 0; 
			String topicName = ""; 
			while(j < auditorinfo.size())
			{
				
				Auditor auditor = new Auditor();
				topicName = auditorinfo.get(j++); 
				auditor.topicJNDI = auditorinfo.get(j++);
				auditor.connectionJNDI = auditorinfo.get(j++); 
				auditor.username = auditorinfo.get(j++); 
				auditor.password = auditorinfo.get(j++); 	
				System.out.println("Starting auditor for " + auditor.topicJNDI + " " + auditor.connectionJNDI); 
				
				Thread t = new Thread(auditor);
				auditorThread.put(t, auditor); 				
				t.start(); 
			}
			
		} catch (SQLException e) {
			logger.fatal("Unable to start Auditor"); 
		} 
		
	}	
	
	public static void stopAuditor()
	{
		Iterator<Thread> threadItr = auditorThread.keySet().iterator(); 
		
		while(threadItr.hasNext())
		{
			Thread thread = threadItr.next(); 
//			try {
//				thread.join();
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} 
			Auditor auditor = auditorThread.get(thread); 
			auditor.stop(); 			
		}
		auditorThread.clear(); 
		
	}


	
	public void startSubscription()
	{
		
		try {
			InitialContext context = new InitialContext(); 
			ConnectionFactory factory = (ConnectionFactory) context.lookup(connectionJNDI); 
			conn = factory.createConnection(username, password); 			
			conn.setClientID(clientID+topicJNDI);		
			Session session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE );		
			Topic topic = (Topic) context.lookup(topicJNDI);
			TopicSubscriber sub = session.createDurableSubscriber(topic,subscriberName+topicJNDI);
			sub.setMessageListener(this); 
			conn.setExceptionListener(this); 
			conn.start(); 
			while(runningStatus) // listen messages
			{
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				} 
			}
		} catch (JMSException e) {			
			e.printStackTrace();
		} catch (NamingException e) {			
			e.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} 
		startSubscription(); 
		
	}
	
	private void stop()
	{
		runningStatus = false; 
		if(conn != null)
		{
			try {
				conn.close();
			} catch (JMSException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}
	}
	

}
