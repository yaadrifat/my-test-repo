package com.velos.services.outbound;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.CommonDAO;

/**MessagingAccountDAO provided the account related information 
 * @author Kanwaldeep
 * 
 */
public class MessagingAccountDAO extends CommonDAO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1117268843957635365L;

	private static Logger logger = Logger.getLogger(MessagingAccountDAO.class);

	private static String accountSettings = "select SETTINGS_MODNUM, SETTINGS_VALUE from er_settings where settings_modname=? and settings_keyword = ?";

	private static String topicJNDIQuery = "select fk_account, topic_name, topic_jndi, root_tablenames, topic_connection_jndi from er_msg_topics  where fk_account = ? ";
	
	private static String topicJNDIAll = "select distinct topic_name, topic_jndi, topic_connection_jndi, topic_conn_username, topic_conn_password from er_msg_topics"; 

	/**
	 * @return Map of Account Number and interval to run Daemon.
	 */
	public Map<Integer, Integer> getDataTimerTaskSetup() throws SQLException {
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		PreparedStatement stmt = null;
		ResultSet rs = null;

		Connection conn = getConnection();

		try {

			stmt = conn.prepareStatement(accountSettings);

			stmt.setString(1, "1"); // Account Level Settings
			stmt.setString(2, MessagingConstants.OUTBOUND_MESSAGE_INTERVAL);

			rs = stmt.executeQuery();

			while (rs.next()) {
				// Map for account_number - outbound messaging frequency
				map.put(rs.getInt("settings_modnum"), rs.getInt("settings_value"));
			}

		} catch (SQLException sqe) {
			logger.error(
					"Failed to retrieve data for initializing OutBound Messaging",
					sqe);
			throw sqe; 
		} finally {
			
			
			try {
							
				if (rs != null) rs.close();
				if (stmt != null) stmt.close();
			
			} catch (SQLException sqe) {
				logger.error("Unable to close resultSet or Connection " ,  sqe);
			}
			returnConnection(conn); 
		}

		return map;

	}

	/**
	 * @param accountFk
	 * @return Map of rootTableNames and MessagePublisher.
	 */
	public Map<String, MessagePublisher> getTopicInformation(int accountFk) throws SQLException{
		ResultSet rs = null;
		PreparedStatement stmt = null;
		Connection conn = null; 
		Map<String, MessagePublisher> map = new HashMap<String, MessagePublisher>();
		try {
			conn = getConnection(); 
			stmt = conn.prepareStatement(topicJNDIQuery);
			stmt.setInt(1, accountFk);
			rs = stmt.executeQuery();

			while (rs.next()) {
				MessagePublisher publisher = new MessagePublisher(
						rs.getString("topic_connection_jndi"),
						rs.getString("topic_jndi"));
				map.put(rs.getString("root_tablenames"), publisher);
			}
		} catch (SQLException e) {
			logger.error("Unable to populate map for Topic Information ", e); 
			throw e;
		} finally {
			
			try {
				if(rs != null) rs.close();
				if(stmt != null) stmt.close(); 
			} catch (SQLException e) {
				logger.error("Unable to close resultSet or Connection " ,  e);
			} 
			returnConnection(conn); 
		}

		return map;
	}
	
	/**
	 * @return Map of rootTableNames and MessagePublisher.
	 */
	public List<String> getTopicInformationForSubscribers() throws SQLException{
		ResultSet rs = null;
		PreparedStatement stmt = null;
		Connection conn = null; 
		List<String> rsList = new ArrayList<String>();
		try {
			conn = getConnection(); 
			stmt = conn.prepareStatement(topicJNDIAll);			
			rs = stmt.executeQuery();

			while (rs.next()) {
				
				rsList.add(rs.getString("topic_name")); 
				rsList.add(rs.getString("topic_jndi")); 
				rsList.add(rs.getString("topic_connection_jndi")); 
				rsList.add(rs.getString("topic_conn_username")); 
				rsList.add(rs.getString("topic_conn_password")); 				
				
			}
		} catch (SQLException e) {
			logger.error("Unable to populate Topic Information for subscribers", e); 
			throw e; 
		} finally {
			
			try {
				if(rs != null) rs.close();
				if(stmt != null) stmt.close(); 
			} catch (SQLException e) {
				logger.error("Unable to close resultSet or Connection " ,  e);
			} 
			returnConnection(conn); 
		}

		return rsList;
	}

}
