package com.velos.services.mashup;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.CommonDAO;
import com.velos.services.OperationException;
import com.velos.services.library.LibraryServiceDAO;

public class StudyMashupDAO extends CommonDAO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8712684938277524712L;
	private static Logger logger = Logger.getLogger(StudyMashupDAO.class);
	
	private Integer formLibPK; 
	
	private Integer studyPK; 
	
	public static Integer getStudyFormResponsePK(Integer formPK) throws OperationException{

		String sql = "select fk_filledform from er_formslinear where fk_form = ?";

		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		Integer formResponsePK = 0;
		try{
			conn = getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setInt(1, formPK);

			rs = pstmt.executeQuery();
			while(rs.next()){
				formResponsePK = rs.getInt("FK_FILLEDFORM");
			}
		}catch(Throwable t){
			t.printStackTrace();
			throw new OperationException();
		}finally{
			try{
				if(pstmt != null){
					pstmt.close();
				}
			}catch(Exception e){

			}try{
				if(conn != null){
					conn.close();
				}
			}catch(Exception e){

			}
		}
		return formResponsePK;
	}
	
	
	public void populateFormIDForStudyFilledFormID(Integer filledFormID)
	{
		Integer formLibID = 0; 
		PreparedStatement stmt = null ; 
		ResultSet rs = null; 
		Connection conn = null; 
		
		try
		{
			String sql = "select a.fk_formlib, a.fk_study from er_studyforms a where pk_studyforms = ?"; 
			
			conn = getConnection(); 
			
			stmt = conn.prepareStatement(sql); 
			stmt.setInt(1, filledFormID); 
			
		   rs = stmt.executeQuery(); 
		   
		   while(rs.next())
		   {
			   formLibPK = rs.getInt(1);     		   
			   studyPK = rs.getInt(2); 
		    		   
		   }
			
			
		}catch(SQLException sqe)
		{
			logger.error(sqe.getStackTrace()); 
		}finally
		{

			if(rs != null)
			{
				try
				{
					rs.close(); 
				}catch(SQLException sqe)
				{}
			}

			if(stmt != null)
			{
				try{
					stmt.close(); 
				}catch(SQLException sqe){}
			}

			if(conn != null)
			{
				returnConnection(conn); 
			} 
		}
	}
	
    public Integer getFormLibIDforFilledForm()
    {
    	return formLibPK; 
    }
    
    public Integer getStudyPKForFilledForm()
    {
    	return studyPK; 
    }
    
    
	public static Integer getFieldPK(Integer formPK, String fieldID) throws OperationException{

		String sql = "select mp_pkfld from er_mapform where fk_form = ? and mp_uid = ?";

		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		Integer fieldPK = 0;
		try{
			conn = getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setInt(1, formPK);
			pstmt.setString(2, fieldID);

			rs = pstmt.executeQuery();
			while(rs.next()){
				fieldPK = rs.getInt("MP_PKFLD");
			}
		}catch(Throwable t){
			t.printStackTrace();
			throw new OperationException();
		}finally{
			try{
				if(pstmt != null){
					pstmt.close();
				}
			}catch(Exception e){

			}try{
				if(conn != null){
					conn.close();
				}
			}catch(Exception e){

			}
		}
		return fieldPK;
	}
	
    public static boolean ifStudyExists(int studyPK) throws OperationException{
    	
    	String sql = "select count(*) as Count from er_study where pk_study = ?";
    	PreparedStatement pstmt  = null;
    	Connection conn =  null;
    	boolean flag = false;
    	try{
    		conn = getConnection();
    		pstmt = conn.prepareStatement(sql);
        	pstmt.setInt(1, studyPK);
        	
        	ResultSet rs = pstmt.executeQuery();
        			
        	while(rs.next()){
        		if(rs.getInt("Count") ==1)
				   flag = true;
				else 
				   flag =  false;
        	}
        	return flag;
    	}catch(Throwable t){
    		t.printStackTrace();
    		throw new OperationException();
    	}
    	
    	finally{
    		try{
    			if(pstmt!=null){
    				pstmt.close();
    			}
    		}catch(Exception e){
    			
    		}
    		try{
    			if(conn!=null){
    				conn.close();
    			}
    		}catch(Exception e){
    			
    		}
    	}
    }
    
    public static boolean ifFormExists(int formPK) throws OperationException{
    	
    	String sql = "select count(*) as Count from er_formlib where pk_formlib = ?";
    	PreparedStatement pstmt  = null;
    	Connection conn =  null;
    	boolean flag = false;
    	try{
    		conn = getConnection();
    		pstmt = conn.prepareStatement(sql);
        	pstmt.setInt(1, formPK);
        	
        	ResultSet rs = pstmt.executeQuery();
        			
        	while(rs.next()){
        		if(rs.getInt("Count") ==1)
				   flag = true;
				else 
				   flag =  false;
        	}
        	return flag;
    	}catch(Throwable t){
    		t.printStackTrace();
    		throw new OperationException();
    	}
    	
    	finally{
    		try{
    			if(pstmt!=null){
    				pstmt.close();
    			}
    		}catch(Exception e){
    			
    		}
    		try{
    			if(conn!=null){
    				conn.close();
    			}
    		}catch(Exception e){
    			
    		}
    	}
    }

}
