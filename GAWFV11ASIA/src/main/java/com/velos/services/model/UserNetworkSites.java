package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.velos.services.ResponseHolder;
@XmlRootElement(name="UserNetworkSites")
@XmlAccessorType(XmlAccessType.FIELD)
public class UserNetworkSites implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1475038510992509642L;
	
	private String networkName;
	private Integer networkPK;
	private String userLoginName;
	private Integer userPK;
	
	public String getNetworkName() {
		if(networkName!=null  && !networkName.equals("") && networkName.contains("'")){
			try{
				char c =networkName.charAt(networkName.indexOf("'")+1);
				if(c!='\''){
					networkName=networkName.replaceAll("'", "''");
				}
				}catch(IndexOutOfBoundsException e){
					//do nothing
				}
		}
		return networkName;
	}
	public void setNetworkName(String network_Name) {
		networkName = network_Name;
	}
	public Integer getNetworkPK() {
		return networkPK;
	}
	public void setNetworkPK(Integer network_PK) {
		networkPK = network_PK;
	}
	public String getUserLoginName() {
		return userLoginName;
	}
	public void setUserLoginName(String userName) {
		this.userLoginName = userName;
	}
	public Integer getUserPK() {
		return userPK;
	}
	public void setUserPK(Integer userPK) {
		this.userPK = userPK;
	}
	
}
