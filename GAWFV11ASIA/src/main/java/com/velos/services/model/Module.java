package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Tarandeep Singh Bali
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Module")
public class Module implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 906697761561425334L;
	
	protected String moduleName;
	protected FieldID fieldID;
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public FieldID getFieldID() {
		return fieldID;
	}
	public void setFieldID(FieldID fieldID) {
		this.fieldID = fieldID;
	}

	
	

}
