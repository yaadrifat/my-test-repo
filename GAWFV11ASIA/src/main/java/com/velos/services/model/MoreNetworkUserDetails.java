package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.namespace.QName;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="MoreNetworkUserDetails")
public class MoreNetworkUserDetails extends NVPair{

	private static final long serialVersionUID = 6672597953158479182L;

	protected String networkLevel;
	
	public MoreNetworkUserDetails(){
		
	}
	
	public String getNetworkLevel(){
		return networkLevel;
	}
	
	public void setNetworkLevel(String networkLevel){
		this.networkLevel = networkLevel;
	}
	
}
