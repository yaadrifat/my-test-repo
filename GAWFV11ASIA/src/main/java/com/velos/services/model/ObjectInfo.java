/**
 * Created On Mar 15, 2013
 */
package com.velos.services.model;

import java.io.Serializable;

/**
 * @author Kanwaldeep
 *
 */
public class ObjectInfo implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5502246621757379856L;
	private int tablePk; 
	private String tableName;
	public int getTablePk() {
		return tablePk;
	}
	public void setTablePk(int tablePk) {
		this.tablePk = tablePk;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	} 
}
