package com.velos.services.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="BudgetCalendar")
@XmlAccessorType (XmlAccessType.FIELD)
public class BgtCalPojo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String BGTCAL_PROTTYPE;
	
	private String BGTCAL_DELFLAG;
	
	private Double BGTCAL_SPONSOROHEAD;
	
	private Double BGTCAL_CLINICOHEAD;
	
	private String BGTCAL_SPONSORFLAG;
	
	private String BGTCAL_CLINICFLAG;
	
	private Double BGTCAL_INDIRECTCOST;
	
	private Double BGTCAL_FRGBENEFIT;
	
	private Integer BGTCAL_FRGFLAG;
	
	private Double BGTCAL_DISCOUNT;
	
	private Integer BGTCAL_DISCOUNTFLAG;
	
	private Integer BGTCAL_EXCLDSOCFLAG;
	
	private String GRAND_RES_COST;
	
	private String GRAND_RES_TOTALCOST;
	
	private String GRAND_SOC_COST;
	
	private String GRAND_SOC_TOTALCOST;
	
	private String GRAND_TOTAL_COST;
	
	private String GRAND_TOTAL_TOTALCOST;
	
	private String GRAND_SALARY_COST;
	
	private String GRAND_SALARY_TOTALCOST;
	
	private String GRAND_FRINGE_COST;
	
	private String GRAND_FRINGE_TOTALCOST;
	
	private String GRAND_DISC_COST;
	
	private String GRAND_DISC_TOTALCOST;
	
	private String GRAND_IND_COST;
	
	private String GRAND_IND_TOTALCOST;
	
	private String GRAND_RES_SPONSOR;
	
	private String GRAND_RES_VIARIANCE;
	
	private String GRAND_RES_VARIANCE;
	
	private String GRAND_SOC_SPONSOR;
	
	private String GRAND_SOC_VARIANCE;
	
	private String GRAND_TOTAL_SPONSOR;
	
	private String GRAND_TOTAL_VARIANCE;
	
	private Double BGTCAL_SP_OVERHEAD;
	
	private Integer BGTCAL_SP_OVERHEAD_FLAG;
	private CalendarNameIdentifier calNameIdent;
	private BudgetCalIdentifier budgetCalIdent;
	private BgtSectionDetail budgetSections;
	public String getBGTCAL_PROTTYPE() {
		return BGTCAL_PROTTYPE;
	}
	public void setBGTCAL_PROTTYPE(String bGTCALPROTTYPE) {
		BGTCAL_PROTTYPE = bGTCALPROTTYPE;
	}
	public String getBGTCAL_DELFLAG() {
		return BGTCAL_DELFLAG;
	}
	public void setBGTCAL_DELFLAG(String bGTCALDELFLAG) {
		BGTCAL_DELFLAG = bGTCALDELFLAG;
	}
	public Double getBGTCAL_SPONSOROHEAD() {
		return BGTCAL_SPONSOROHEAD;
	}
	public void setBGTCAL_SPONSOROHEAD(Double bGTCALSPONSOROHEAD) {
		BGTCAL_SPONSOROHEAD = bGTCALSPONSOROHEAD;
	}
	public Double getBGTCAL_CLINICOHEAD() {
		return BGTCAL_CLINICOHEAD;
	}
	public void setBGTCAL_CLINICOHEAD(Double bGTCALCLINICOHEAD) {
		BGTCAL_CLINICOHEAD = bGTCALCLINICOHEAD;
	}
	public String getBGTCAL_SPONSORFLAG() {
		return BGTCAL_SPONSORFLAG;
	}
	public void setBGTCAL_SPONSORFLAG(String bGTCALSPONSORFLAG) {
		BGTCAL_SPONSORFLAG = bGTCALSPONSORFLAG;
	}
	public String getBGTCAL_CLINICFLAG() {
		return BGTCAL_CLINICFLAG;
	}
	public void setBGTCAL_CLINICFLAG(String bGTCALCLINICFLAG) {
		BGTCAL_CLINICFLAG = bGTCALCLINICFLAG;
	}
	public Double getBGTCAL_INDIRECTCOST() {
		return BGTCAL_INDIRECTCOST;
	}
	public void setBGTCAL_INDIRECTCOST(Double bGTCALINDIRECTCOST) {
		BGTCAL_INDIRECTCOST = bGTCALINDIRECTCOST;
	}
	public Double getBGTCAL_FRGBENEFIT() {
		return BGTCAL_FRGBENEFIT;
	}
	public void setBGTCAL_FRGBENEFIT(Double bGTCALFRGBENEFIT) {
		BGTCAL_FRGBENEFIT = bGTCALFRGBENEFIT;
	}
	public Integer getBGTCAL_FRGFLAG() {
		return BGTCAL_FRGFLAG;
	}
	public void setBGTCAL_FRGFLAG(Integer bGTCALFRGFLAG) {
		BGTCAL_FRGFLAG = bGTCALFRGFLAG;
	}
	public Double getBGTCAL_DISCOUNT() {
		return BGTCAL_DISCOUNT;
	}
	public void setBGTCAL_DISCOUNT(Double bGTCALDISCOUNT) {
		BGTCAL_DISCOUNT = bGTCALDISCOUNT;
	}
	public Integer getBGTCAL_DISCOUNTFLAG() {
		return BGTCAL_DISCOUNTFLAG;
	}
	public void setBGTCAL_DISCOUNTFLAG(Integer bGTCALDISCOUNTFLAG) {
		BGTCAL_DISCOUNTFLAG = bGTCALDISCOUNTFLAG;
	}
	public Integer getBGTCAL_EXCLDSOCFLAG() {
		return BGTCAL_EXCLDSOCFLAG;
	}
	public void setBGTCAL_EXCLDSOCFLAG(Integer bGTCALEXCLDSOCFLAG) {
		BGTCAL_EXCLDSOCFLAG = bGTCALEXCLDSOCFLAG;
	}
	public String getGRAND_RES_COST() {
		return GRAND_RES_COST;
	}
	public void setGRAND_RES_COST(String gRANDRESCOST) {
		GRAND_RES_COST = gRANDRESCOST;
	}
	public String getGRAND_RES_TOTALCOST() {
		return GRAND_RES_TOTALCOST;
	}
	public void setGRAND_RES_TOTALCOST(String gRANDRESTOTALCOST) {
		GRAND_RES_TOTALCOST = gRANDRESTOTALCOST;
	}
	public String getGRAND_SOC_COST() {
		return GRAND_SOC_COST;
	}
	public void setGRAND_SOC_COST(String gRANDSOCCOST) {
		GRAND_SOC_COST = gRANDSOCCOST;
	}
	public String getGRAND_SOC_TOTALCOST() {
		return GRAND_SOC_TOTALCOST;
	}
	public void setGRAND_SOC_TOTALCOST(String gRANDSOCTOTALCOST) {
		GRAND_SOC_TOTALCOST = gRANDSOCTOTALCOST;
	}
	public String getGRAND_TOTAL_COST() {
		return GRAND_TOTAL_COST;
	}
	public void setGRAND_TOTAL_COST(String gRANDTOTALCOST) {
		GRAND_TOTAL_COST = gRANDTOTALCOST;
	}
	public String getGRAND_TOTAL_TOTALCOST() {
		return GRAND_TOTAL_TOTALCOST;
	}
	public void setGRAND_TOTAL_TOTALCOST(String gRANDTOTALTOTALCOST) {
		GRAND_TOTAL_TOTALCOST = gRANDTOTALTOTALCOST;
	}
	public String getGRAND_SALARY_COST() {
		return GRAND_SALARY_COST;
	}
	public void setGRAND_SALARY_COST(String gRANDSALARYCOST) {
		GRAND_SALARY_COST = gRANDSALARYCOST;
	}
	public String getGRAND_SALARY_TOTALCOST() {
		return GRAND_SALARY_TOTALCOST;
	}
	public void setGRAND_SALARY_TOTALCOST(String gRANDSALARYTOTALCOST) {
		GRAND_SALARY_TOTALCOST = gRANDSALARYTOTALCOST;
	}
	public String getGRAND_FRINGE_COST() {
		return GRAND_FRINGE_COST;
	}
	public void setGRAND_FRINGE_COST(String gRANDFRINGECOST) {
		GRAND_FRINGE_COST = gRANDFRINGECOST;
	}
	public String getGRAND_FRINGE_TOTALCOST() {
		return GRAND_FRINGE_TOTALCOST;
	}
	public void setGRAND_FRINGE_TOTALCOST(String gRANDFRINGETOTALCOST) {
		GRAND_FRINGE_TOTALCOST = gRANDFRINGETOTALCOST;
	}
	public String getGRAND_DISC_COST() {
		return GRAND_DISC_COST;
	}
	public void setGRAND_DISC_COST(String gRANDDISCCOST) {
		GRAND_DISC_COST = gRANDDISCCOST;
	}
	public String getGRAND_DISC_TOTALCOST() {
		return GRAND_DISC_TOTALCOST;
	}
	public void setGRAND_DISC_TOTALCOST(String gRANDDISCTOTALCOST) {
		GRAND_DISC_TOTALCOST = gRANDDISCTOTALCOST;
	}
	public String getGRAND_IND_COST() {
		return GRAND_IND_COST;
	}
	public void setGRAND_IND_COST(String gRANDINDCOST) {
		GRAND_IND_COST = gRANDINDCOST;
	}
	public String getGRAND_IND_TOTALCOST() {
		return GRAND_IND_TOTALCOST;
	}
	public void setGRAND_IND_TOTALCOST(String gRANDINDTOTALCOST) {
		GRAND_IND_TOTALCOST = gRANDINDTOTALCOST;
	}
	public String getGRAND_RES_SPONSOR() {
		return GRAND_RES_SPONSOR;
	}
	public void setGRAND_RES_SPONSOR(String gRANDRESSPONSOR) {
		GRAND_RES_SPONSOR = gRANDRESSPONSOR;
	}
	public String getGRAND_RES_VIARIANCE() {
		return GRAND_RES_VIARIANCE;
	}
	public void setGRAND_RES_VIARIANCE(String gRANDRESVIARIANCE) {
		GRAND_RES_VIARIANCE = gRANDRESVIARIANCE;
	}
	public String getGRAND_RES_VARIANCE() {
		return GRAND_RES_VARIANCE;
	}
	public void setGRAND_RES_VARIANCE(String gRANDRESVARIANCE) {
		GRAND_RES_VARIANCE = gRANDRESVARIANCE;
	}
	public String getGRAND_SOC_SPONSOR() {
		return GRAND_SOC_SPONSOR;
	}
	public void setGRAND_SOC_SPONSOR(String gRANDSOCSPONSOR) {
		GRAND_SOC_SPONSOR = gRANDSOCSPONSOR;
	}
	public String getGRAND_SOC_VARIANCE() {
		return GRAND_SOC_VARIANCE;
	}
	public void setGRAND_SOC_VARIANCE(String gRANDSOCVARIANCE) {
		GRAND_SOC_VARIANCE = gRANDSOCVARIANCE;
	}
	public String getGRAND_TOTAL_SPONSOR() {
		return GRAND_TOTAL_SPONSOR;
	}
	public void setGRAND_TOTAL_SPONSOR(String gRANDTOTALSPONSOR) {
		GRAND_TOTAL_SPONSOR = gRANDTOTALSPONSOR;
	}
	public String getGRAND_TOTAL_VARIANCE() {
		return GRAND_TOTAL_VARIANCE;
	}
	public void setGRAND_TOTAL_VARIANCE(String gRANDTOTALVARIANCE) {
		GRAND_TOTAL_VARIANCE = gRANDTOTALVARIANCE;
	}
	public Double getBGTCAL_SP_OVERHEAD() {
		return BGTCAL_SP_OVERHEAD;
	}
	public void setBGTCAL_SP_OVERHEAD(Double bGTCALSPOVERHEAD) {
		BGTCAL_SP_OVERHEAD = bGTCALSPOVERHEAD;
	}
	public Integer getBGTCAL_SP_OVERHEAD_FLAG() {
		return BGTCAL_SP_OVERHEAD_FLAG;
	}
	public void setBGTCAL_SP_OVERHEAD_FLAG(Integer bGTCALSPOVERHEADFLAG) {
		BGTCAL_SP_OVERHEAD_FLAG = bGTCALSPOVERHEADFLAG;
	}
	public CalendarNameIdentifier getCalNameIdent() {
		return calNameIdent;
	}
	public void setCalNameIdent(CalendarNameIdentifier calNameIdent) {
		this.calNameIdent = calNameIdent;
	}
	public BudgetCalIdentifier getBudgetCalIdent() {
		return budgetCalIdent;
	}
	public void setBudgetCalIdent(BudgetCalIdentifier budgetCalIdent) {
		this.budgetCalIdent = budgetCalIdent;
	}
	public BgtSectionDetail getBudgetSections() {
		return budgetSections;
	}
	public void setBudgetSections(BgtSectionDetail budgetSections) {
		this.budgetSections = budgetSections;
	}
	
}
