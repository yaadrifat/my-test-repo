
package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="ServiceObjects")

/**
 * Class that identifies all transfer objects that can be identified using 
 * an {@link SimpleIdentifer} object.
 */
public abstract class ServiceObjects implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5155122555732048405L;


 }
