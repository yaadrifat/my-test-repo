/**
 * Created On May 18, 2011
 */
package com.velos.services.model;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Kanwaldeep
 *
 */
@XmlRootElement(name="Events")
@XmlAccessorType(XmlAccessType.FIELD)
public class CalendarEvents extends ServiceObjects{
	
	private static final long serialVersionUID = 8267101079760170789L;
	private List<CalendarEvent> event = new ArrayList<CalendarEvent>();

	/**
	 * @param events the events to set
	 */
	public void setEvent(List<CalendarEvent> event) {
		this.event = event;
	}

	/**
	 * @return the events
	 */
	@NotNull
	@Valid
	public List<CalendarEvent> getEvent() {
		return event;
	} 
	
	public void setEvent(CalendarEvent eventIn)
	{
		event.add(eventIn); 
	}
	

}
