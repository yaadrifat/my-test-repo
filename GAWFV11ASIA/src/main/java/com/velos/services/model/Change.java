package com.velos.services.model;

import java.io.Serializable;

import com.velos.services.CRUDAction;

public class Change implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5436438723953511007L;
	protected CRUDAction action; 
	protected String module;
	protected String timeStamp;
	protected StudyIdentifier identifier;
	protected String statusSubType;
	protected String statusCodeDesc;
	protected UserIdentifier UserIdentifier;
	protected String teamRoleType;
	protected String teamRoleDesc;
	protected String formType;
	protected String formResponseType;
	
	
	public CRUDAction getAction() {
		return action;
	}
	
	public void setAction(CRUDAction action) {
		this.action = action;
	}
	
	public String getModule() {
		return module;
	}
	
	public void setModule(String module) {
		this.module = module;
	}
	
	public StudyIdentifier getIdentifier() {
		return this.identifier;
	}

	public void setIdentifier(StudyIdentifier identifier) {
		this.identifier = identifier;
	}

	public void setTimeStamp(String timeStamp)
	{
		this.timeStamp = timeStamp; 
	}
	
	public String getTimeStamp()
	{
		return timeStamp; 
	}
	public String getStatusSubType() {
		return statusSubType;
	}
	public void setStatusSubType(String statusSubType) {
		this.statusSubType = statusSubType;
	}
	public String getStatusCodeDesc() {
		return statusCodeDesc;
	}
	public void setStatusCodeDesc(String statusCodeDesc) {
		this.statusCodeDesc = statusCodeDesc;
	}
	public UserIdentifier getUserIdentifier() {
		return UserIdentifier;
	}
	public void setUserIdentifier(UserIdentifier userIdentifier) {
		UserIdentifier = userIdentifier;
	}
	public String getTeamRoleType() {
		return teamRoleType;
	}
	public void setTeamRoleType(String teamRoleType) {
		this.teamRoleType = teamRoleType;
	}
	public String getTeamRoleDesc() {
		return teamRoleDesc;
	}
	public void setTeamRoleDesc(String teamRoleDesc) {
		this.teamRoleDesc = teamRoleDesc;
	}
	public String getFormType() {
		return formType;
	}
	public void setFormType(String formType) {
		this.formType = formType;
	}

	public String getFormResponseType() {
		return formResponseType;
	}

	public void setFormResponseType(String formResponseType) {
		this.formResponseType = formResponseType;
	}
}