package com.velos.services.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="BgtCalName")
public class BgtCalNameIdentifier extends SimpleIdentifier{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String bgtCalName;

	public String getBgtCalName() {
		return bgtCalName;
	}

	public void setBgtCalName(String bgtCalName) {
		this.bgtCalName = bgtCalName;
	}

}
