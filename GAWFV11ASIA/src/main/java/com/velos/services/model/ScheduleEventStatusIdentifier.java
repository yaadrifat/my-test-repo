package com.velos.services.model;

import java.io.Serializable;

/**
 * 
 * @author Tarandeep Singh Bali
 *
 */
public class ScheduleEventStatusIdentifier implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1528356718817701833L;
	
	private EventIdentifier eventIdentifier;
    private ScheduleEventStatus scheduleEventStatus;
	public EventIdentifier getEventIdentifier() {
		return eventIdentifier;
	}
	public void setEventIdentifier(EventIdentifier eventIdentifier) {
		this.eventIdentifier = eventIdentifier;
	}
	public ScheduleEventStatus getScheduleEventStatus() {
		return scheduleEventStatus;
	}
	public void setScheduleEventStatus(ScheduleEventStatus scheduleEventStatus) {
		this.scheduleEventStatus = scheduleEventStatus;
	}

		

}
