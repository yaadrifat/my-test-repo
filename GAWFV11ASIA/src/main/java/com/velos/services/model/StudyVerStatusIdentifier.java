package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="StudyVerStatusIdentifier")
@XmlAccessorType (XmlAccessType.FIELD)
public class StudyVerStatusIdentifier extends SimpleIdentifier implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
}
