package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.namespace.QName;


import javax.xml.bind.annotation.XmlRootElement;
//@XmlRootElement(name="Networks")
@XmlRootElement(name="NetworksDetailsResult")
@XmlAccessorType(XmlAccessType.FIELD)
public class NetworksDetailsResult implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8232983964685718366L;
	
public static QName ORG_MORE_DETAILS_NS = new QName("www.velos.com","OrganizationMoreDetails",	"Map");
	
	
	private NetworkIdentifier networkIdentifier;
	private String name; 
	private Code networkStatus; 
	private Code relationshipType; 
	private String CTEPID;
	protected List<NVPair> networkMoreDetails;
	public NetworkIdentifier getNetworkIdentifier() {
		return networkIdentifier;
	}
	public void setNetworkIdentifier(NetworkIdentifier networkIdentifier) {
		this.networkIdentifier = networkIdentifier;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		name = name;
	}
	
	public Code getNetworkStatus() {
		return networkStatus;
	}

	public void setNetworkStatus(Code networkStatus) {
		networkStatus = networkStatus;
	}
	/*public String getNetworkStatus() {
		return NetworkStatus;
	}

	public void setNetworkStatus(String networkStatus) {
		NetworkStatus = networkStatus;
	}*/
	
	public Code getRelationShip_Type() {
		return relationshipType;
	}

	public void setRelationShip_Type(Code relationShip_Type) {
		relationshipType = relationShip_Type;
	}
	
	/*public String getRelationShip_Type() {
		return RelationShip_Type;
	}

	public void setRelationShip_Type(String relationShip_Type) {
		RelationShip_Type = relationShip_Type;
	}*/
	
	public String getCTEPID() {
		return CTEPID;
	}

	public void setCTEPID(String CCTEPID) {
		CTEPID = CCTEPID;
	}
	

	public List<NVPair> getMoreNetworkDetails() {
		return networkMoreDetails;
	}
	public void setMoreNetworkDetails(List<NVPair> NetworkMoreDetails) {
		this.networkMoreDetails = NetworkMoreDetails;
	}
	
	
}
