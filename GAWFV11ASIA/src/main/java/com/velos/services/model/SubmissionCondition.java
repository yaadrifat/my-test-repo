package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="submissionCondition")
public class SubmissionCondition implements Serializable{

	protected String conditionName;
	protected int conditionMeets;
	protected String conditionResult;
	
	public String getConditionResult() {
		return conditionResult;
	}
	public void setConditionResult(String conditionResult) {
		this.conditionResult = conditionResult;
	}
	public String getConditionName() {
		return conditionName;
	}
	public void setConditionName(String conditionName) {
		this.conditionName = conditionName;
	}
	public int getConditionMeets() {
		return conditionMeets;
	}
	public void setConditionMeets(int conditionMeets) {
		this.conditionMeets = conditionMeets;
	}
	
}
