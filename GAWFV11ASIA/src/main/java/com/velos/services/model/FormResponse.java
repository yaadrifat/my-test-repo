/**
 * Created On Nov 8, 2011
 */
package com.velos.services.model;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Kanwaldeep
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="FormResponse")
public class FormResponse extends ServiceObjects {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8307679178953788231L;
	
	
	protected FormFieldResponses formFieldResponses; 
	protected FormIdentifier formId;
	protected Date formFillDt; 
	protected Code formStatus;
	protected Integer formVersion;
	public FormFieldResponses getFormFieldResponses() {
		return formFieldResponses;
	}
	public void setFormFieldResponses(FormFieldResponses formFieldResponses) {
		this.formFieldResponses = formFieldResponses;
	}
	public FormIdentifier getFormId() {
		return formId;
	}
	public void setFormId(FormIdentifier formId) {
		this.formId = formId;
	}
	public Date getFormFillDt() {
		return formFillDt;
	}
	public void setFormFillDt(Date formFillDt) {
		this.formFillDt = formFillDt;
	}
	
	@NotNull
	public Code getFormStatus() {
		return formStatus;
	}
	public void setFormStatus(Code formStatus) {
		this.formStatus = formStatus;
	}
	public Integer getFormVersion() {
		return formVersion;
	}
	public void setFormVersion(Integer formVersion) {
		this.formVersion = formVersion;
	} 

}
