package com.velos.services.model;

public class SiteIdentifier extends SimpleIdentifier{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5017766075798150481L;
	private String name;
	
	public String getName() {
		return name;
	}
	public void setName(String Name) {
		name = Name;
	}
}
