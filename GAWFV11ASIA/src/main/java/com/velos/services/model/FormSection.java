/**
 * Created On Aug 25, 2011
 */
package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Kanwaldeep
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Section")
public class FormSection implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7591634901660447205L;
	private FormSectionIdentifier sectionIdentifier;  
	private String secName; 
	private String secSequence; 
	private boolean isTabular; 
	private Integer repeated; 
	private FormFields fields;
	private boolean offline; 

	/**
	 * @param fields the fields to set
	 */
	public void setFields(FormFields fields) {
		this.fields = fields;
	}

	/**
	 * @return the fields
	 */
	public FormFields getFields() {
		return fields;
	}

	public String getSecName() {
		return secName;
	}

	public void setSecName(String secName) {
		this.secName = secName;
	}

	public String getSecSequence() {
		return secSequence;
	}

	public void setSecSequence(String secSequence) {
		this.secSequence = secSequence;
	}

	public boolean isTabular() {
		return isTabular;
	}

	public void setTabular(boolean isTabular) {
		this.isTabular = isTabular;
	}

	public Integer getRepeated() {
		return repeated;
	}

	public void setRepeated(Integer repeated) {
		this.repeated = repeated;
	}

	public boolean isOffline() {
		return offline;
	}

	public void setOffline(boolean offline) {
		this.offline = offline;
	}

	/**
	 * @param sectionIdentifier the sectionIdentifier to set
	 */
	public void setSectionIdentifier(FormSectionIdentifier sectionIdentifier) {
		this.sectionIdentifier = sectionIdentifier;
	}

	/**
	 * @return the sectionIdentifier
	 */
	public FormSectionIdentifier getSectionIdentifier() {
		return sectionIdentifier;
	} 	

}
