package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Organizations implements Serializable {
	
	private static final long serialVersionUID = -3685161708201790145L;
	
	protected List<OrganizationIdentifier> organization = new ArrayList<OrganizationIdentifier>();

	public List<OrganizationIdentifier> getOrganization() {
		return organization;
	}

	public void setOrganization(List<OrganizationIdentifier> organization) {
		this.organization = organization;
	}
	public void add(OrganizationIdentifier organization) {
		this.organization.add(organization);
	}

	public void addAll(List<OrganizationIdentifier> organization) {
		this.organization = organization;
	}
	

}
