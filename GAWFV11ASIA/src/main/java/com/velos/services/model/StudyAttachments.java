package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="StudyAttachments")
public class StudyAttachments implements Serializable {
	private static final long serialVersionUID = 6433295576449890336L;
	private List<StudyAttachment> studyAttachment = new ArrayList<StudyAttachment>();
	public List<StudyAttachment> getStudyAttachment() {
		return studyAttachment;
	}
	public void setStudyAttachment(List<StudyAttachment> studyAttachment) {
		this.studyAttachment = studyAttachment;
	}
	public void addStudyAttachment(StudyAttachment attachment) {
		this.studyAttachment.add(attachment);
	}
}
