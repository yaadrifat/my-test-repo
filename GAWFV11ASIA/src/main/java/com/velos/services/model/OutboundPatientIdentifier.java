package com.velos.services.model;

import java.io.Serializable;

public class OutboundPatientIdentifier implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3796288527656547205L;
	private String patientId;
	private OutboundOrganizationIdentifier organizationId;
	public String getPatientId() {
		return patientId;
	}
	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}
	public OutboundOrganizationIdentifier getOrganizationId() {
		return organizationId;
	}
	public void setOrganizationId(OutboundOrganizationIdentifier organizationId) {
		this.organizationId = organizationId;
	}
	
}
