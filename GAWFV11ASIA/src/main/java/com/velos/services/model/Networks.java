package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.velos.services.ResponseHolder;
@XmlRootElement(name="networks")
@XmlAccessorType(XmlAccessType.FIELD)
public class Networks  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8031445360320287120L;
	private ResponseHolder response;
	
	

	protected List<NetworkSiteDetailsResult> network = new ArrayList<NetworkSiteDetailsResult>();


	public List<NetworkSiteDetailsResult> getNetwork() {
		return network;
	}


	public void setNetwork(List<NetworkSiteDetailsResult> Network) {
		this.network = Network;
	}

	public void addNetwork(NetworkSiteDetailsResult NetworksDetailsResult){
		this.network.add(NetworksDetailsResult);
	}
	public ResponseHolder getResponse() {
		return response;
	}


	public void setResponse(ResponseHolder response) {
		this.response = response;
	}
	
}
