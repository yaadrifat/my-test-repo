package com.velos.services.model;

import java.io.Serializable;

/**
 * 
 * @author Tarandeep Singh Bali
 *
 */

public class MultipleEvent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4852741458139679069L;
	
	private EventIdentifier eventIdentifier;
	private EventAttributes eventAttributes;
	public EventIdentifier getEventIdentifier() {
		return eventIdentifier;
	}
	public void setEventIdentifier(EventIdentifier eventIdentifier) {
		this.eventIdentifier = eventIdentifier;
	}
	public EventAttributes getEventAttributes() {
		return eventAttributes;
	}
	public void setEventAttributes(EventAttributes eventAttributes) {
		this.eventAttributes = eventAttributes;
	}

}
