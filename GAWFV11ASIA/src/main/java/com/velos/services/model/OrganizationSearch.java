package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.velos.services.model.FormField.SortOrder;
import com.velos.services.model.UserSearch.UserSearchOrderBy;
@XmlRootElement(name="OrganizationSearch")
@XmlAccessorType(XmlAccessType.FIELD)
public class OrganizationSearch implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1127486793081907233L;
	protected OrganizationIdentifier organizationIdent;
	protected List<MoreDetailValue> moreOrgDetails;
	private int pageNumber;
	private int pageSize;
	
	private OrgSearchOrderBy sortBy;
	private SortOrder sortOrder;
	
	public enum OrgSearchOrderBy {
		siteName,
		PK
	}
	public OrganizationIdentifier getOrganizationIdent() {
		return organizationIdent;
	}
	public void setOrganizationIdent(OrganizationIdentifier organizationIdent) {
		this.organizationIdent = organizationIdent;
	}
	public List<MoreDetailValue> getMoreOrgDetails() {
		return moreOrgDetails;
	}
	public void setMoreOrgDetails(List<MoreDetailValue> moreOrgDetails) {
		this.moreOrgDetails = moreOrgDetails;
	}
	public int getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public OrgSearchOrderBy getSortBy() {
		return sortBy;
	}
	public void setSortBy(OrgSearchOrderBy sortBy) {
		this.sortBy = sortBy;
	}
	public SortOrder getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}
}
