/**
 * 
 */
package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Marker interface for classes for all objects that provide unique identification for 
 * objects in the service framework.
 * @author dylan, Kanwaldeep
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="SimpleIdentifier")
public class SimpleIdentifier implements Serializable{
	

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3478077605819266163L;
	protected String OID;
	protected Integer PK;
	
	public SimpleIdentifier(){
		
	}
	
	public SimpleIdentifier(String OID) {
		super();
		this.OID = OID;
	}
	
	public String getOID() {
		return OID;
	}

	public void setOID(String OID) {
		this.OID = OID;
	}

	public Integer getPK() {
		return PK;
	}

	public void setPK(Integer PK) {
		this.PK = PK;
	}
	

	
	
	
}
