package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="StudyVersionIdentifier")
@XmlAccessorType (XmlAccessType.FIELD)
public class StudyVersionIdentifier extends SimpleIdentifier implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String STUDYVER_NUMBER;

	public String getSTUDYVER_NUMBER() {
		return STUDYVER_NUMBER;
	}

	public void setSTUDYVER_NUMBER(String sTUDYVERNUMBER) {
		STUDYVER_NUMBER = sTUDYVERNUMBER;
	}
}
