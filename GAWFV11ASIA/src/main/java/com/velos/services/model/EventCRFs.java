package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="EventCRFs")
public class EventCRFs implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5306796932221294793L;
	protected List<EventCRF> eventCRF = new ArrayList<EventCRF>();

	public List<EventCRF> getEventCRF() {
		return eventCRF;
	}

	public void setEventCRF(List<EventCRF> eventCRF) {
		this.eventCRF = eventCRF;
	}
	
	public void addEventCRF(EventCRF eventCRF){
		this.eventCRF.add(eventCRF);
	}

}
