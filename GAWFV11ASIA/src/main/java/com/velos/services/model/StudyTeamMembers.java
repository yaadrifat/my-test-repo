/**
 * Created On May 31, 2011
 */
package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Kanwaldeep
 *
 */
public class StudyTeamMembers implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6729817744437917411L;
	protected List<StudyTeamMember> studyTeamMember;
	
	protected StudyIdentifier parentIdentifier;
	
	@NotNull
	@Size(min=1)
	@Valid
	public List<StudyTeamMember> getStudyTeamMember() {
		return studyTeamMember;
	}
	public void setStudyTeamMember(List<StudyTeamMember> studyTeamMember) {
		this.studyTeamMember = studyTeamMember;
	} 
	
	public StudyIdentifier getParentIdentifier() {
		return parentIdentifier;
	}


	public void setParentIdentifier(StudyIdentifier parentIdentifier) {
		this.parentIdentifier = parentIdentifier;
	}
}
