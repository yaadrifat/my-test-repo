/**
 * Created On Sep 6, 2011
 */
package com.velos.services.model;

/**
 * @author Kanwaldeep
 *
 */
public class StudyFormRespns extends FormRespns{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6473030439276958183L;
	
	protected StudyIdentifier studyIdentifier;
	protected SimpleIdentifier systemID;
	
	public StudyIdentifier getStudyIdentifier() {
		return studyIdentifier;
	}
	public void setStudyIdentifier(StudyIdentifier studyIdentifier) {
		this.studyIdentifier = studyIdentifier;
	}
	public SimpleIdentifier getSystemID() {
		return systemID;
	}
	public void setSystemID(SimpleIdentifier systemID) {
		this.systemID = systemID;
	}
	
}
