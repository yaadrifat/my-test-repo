package com.velos.services.model;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Isaac
 *
 */
public class StudyIndIdes extends ServiceObjects {

	private static final long serialVersionUID = 2364942262852460785L;
	protected List<StudyIndIde> studyIndIde;
	
	protected StudyIdentifier parentIdentifier;
	
	@NotNull
	@Size(min=1)
	@Valid
	public List<StudyIndIde> getStudyIndIde() {
		return studyIndIde;
	}
	public void setStudyIndIde(List<StudyIndIde> studyIndIde) {
		this.studyIndIde = studyIndIde;
	} 
	
	/**
	 * Parent Identifier of a Study IND/IDE record is the studyIdentifier
	 * @return studyIdentifier of the enclosing study
	 */
	public StudyIdentifier getParentIdentifier() {
		return parentIdentifier;
	}


	public void setParentIdentifier(StudyIdentifier parentIdentifier) {
		this.parentIdentifier = parentIdentifier;
	}
}
