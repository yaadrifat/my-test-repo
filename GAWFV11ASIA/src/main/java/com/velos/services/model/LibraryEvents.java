package com.velos.services.model;
/*
 * Author : Tarandeep Singh Bali
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="LibraryEvents")
public class LibraryEvents implements Serializable {


	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8428014724195665842L;
	
	protected List<LibraryEvent> libraryEvent = new ArrayList<LibraryEvent>();

	public List<LibraryEvent> getLibraryEvent() {
		return libraryEvent;
	}

	public void setLibraryEvent(List<LibraryEvent> libraryEvent) {
		this.libraryEvent = libraryEvent;
	}
	
	public void addLibraryEvent(LibraryEvent libraryEvent){
		
		this.libraryEvent.add(libraryEvent);
	}

}
