package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "StudySearchResults")
public class StudySearchResults implements Serializable {

	private static final long serialVersionUID = 1844769611777533543L;
	protected List<StudySearch> studySearch = new ArrayList<StudySearch>();
	public List<StudySearch> getStudySearch() {
		return studySearch;
	}
	public void setStudySearch(List<StudySearch> studySearch) {
		this.studySearch = studySearch;
	}
	public void addStudySearch(StudySearch studySearch)
    {
    	this.studySearch.add(studySearch);
    }
}
