package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.velos.services.ResponseHolder;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="networks")
public class NetworkUsersSearchResult  implements Serializable{

	private static final long serialVersionUID = -6862455056198425071L;
	private ResponseHolder response;
	private List<UserNetwork> network = new ArrayList<UserNetwork>();
	private List<UserNetworkSite> networks = new ArrayList<UserNetworkSite>();

	public List<UserNetwork> getNetwork() {
		return network;
	}
	public void setNetwork(List<UserNetwork> network) {
		this.network = network;
	}

	public void addNetwork(UserNetwork network){
		this.network.add(network);
	}
	public ResponseHolder getResponse() {
		return response;
	}
	public void setResponse(ResponseHolder response) {
		this.response = response;
	}
	
	public List<UserNetworkSite> getNetworks() {
		return networks;
	}
	public void setNetworks(List<UserNetworkSite> networks) {
		this.networks = networks;
	}

	public void addNetworks(UserNetworkSite networks){
		this.networks.add(networks);
	}
 
}
