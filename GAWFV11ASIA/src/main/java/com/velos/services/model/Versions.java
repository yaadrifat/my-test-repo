package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

public class Versions implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private StudyIdentifier studyIdentifier;
	private List<StudyVersion> version;
	//private List<StudyAppendix> studyAppendix;
	//private List<StudyStatusHistory> stdStatusHistory;
	public StudyIdentifier getStudyIdentifier() {
		return studyIdentifier;
	}
	public void setStudyIdentifier(StudyIdentifier studyIdentifier) {
		this.studyIdentifier = studyIdentifier;
	}
	
	public Versions() {
		super();
	}
	public List<StudyVersion> getVersion() {
		return version;
	}
	public void setVersion(List<StudyVersion> version) {
		this.version = version;
	}
	/*public List<StudyAppendix> getStudyAppendix() {
		return studyAppendix;
	}
	public void setStudyAppendix(List<StudyAppendix> studyAppendix) {
		this.studyAppendix = studyAppendix;
	}
	
	public List<StudyStatusHistory> getStdStatusHistory() {
		return stdStatusHistory;
	}
	public void setStdStatusHistory(List<StudyStatusHistory> stdStatusHistory) {
		this.stdStatusHistory = stdStatusHistory;
	}*/
}
