package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="NetworkRole")
public class NetworkRole implements Serializable{


	private static final long serialVersionUID = 5843842350495429635L;
	private String name;
	private String PK;
	private String subType;
	private String roleType;
	private String roleCode;
	//private String rolestatus;
	private Code roleStatus;
	
		
		
		public void setName(String name){
			this.name=name;
		}
		
		public String getName(){
			return name;
		}
		
		
		/*public void setRoleStatus(String rolestatus){
			this.rolestatus=rolestatus;
			}

	   public String getRoleStatus(){
		  return rolestatus;
	    }*/
		
		public void setRoleStatus(Code rolestatus){
			this.roleStatus=rolestatus;
			}

		public Code getRoleStatus(){
		  return roleStatus;
	    }

		public String getPK() {
			return PK;
		}

		public void setPK(String pK) {
			PK = pK;
		}

		public String getSubType() {
			return subType;
		}

		public void setSubType(String subType) {
			this.subType = subType;
		}

		public String getRoleType() {
			return roleType;
		}

		public void setRoleType(String roleType) {
			this.roleType = roleType;
		}

		public String getRoleCode() {
			return roleCode;
		}

		public void setRoleCode(String roleCode) {
			this.roleCode = roleCode;
		}
	
	   
}
