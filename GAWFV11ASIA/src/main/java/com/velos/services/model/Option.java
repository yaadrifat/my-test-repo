package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Option")
public class Option implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7918270794677458933L;
	protected String display_Value;
	protected String data_Value;
	public String getDisplay_Value() {
		return this.display_Value;
	}
	public void setDisplay_Value(String displayValue) {
		this.display_Value = displayValue;
	}
	public String getData_Value() {
		return this.data_Value;
	}
	public void setData_Value(String dataValue) {
		this.data_Value = dataValue;
	}
	

}
