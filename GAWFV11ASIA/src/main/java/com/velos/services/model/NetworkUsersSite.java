package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="site")
public class NetworkUsersSite implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5647717652994226380L;
	private List<NetworkSitesUsers> site=new ArrayList<NetworkSitesUsers>();
	
	public List<NetworkSitesUsers> getSites() {
		return site;
	}
	public void setSites(List<NetworkSitesUsers> site) {
		this.site = site;
	}
}
