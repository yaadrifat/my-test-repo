package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="BudgetLineItemInfo")
@XmlAccessorType (XmlAccessType.FIELD)
public class BudgetLineItemPojo implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String LINEITEM_DESC;
	
	private String LINEITEM_SPONSORUNIT;
	
	private String LINEITEM_CLINICNOFUNIT;
	
	private String LINEITEM_OTHERCOST;
	
	private String LINEITEM_DELFLAG;
	
	private String LINEITEM_NOTES;
	
	private String LINEITEM_INPERSEC;
	
	private String LINEITEM_STDCARECOST;
	
	private String LINEITEM_RESCOST;
	
	private String LINEITEM_INVCOST;
	
	private String LINEITEM_INCOSTDISC;
	
	private String LINEITEM_CPTCODE;
	
	private String LINEITEM_REPEAT;
	
	private String LINEITEM_PARENTID;
	
	private String LINEITEM_APPLYINFUTURE;
	
	private Code PK_CODELST_CATEGORY;
	
	private String LINEITEM_TMID;
	
	private String LINEITEM_CDM;
	
	private String LINEITEM_APPLYINDIRECTS;
	
	private String LINEITEM_TOTALCOST;
	
	private String LINEITEM_SPONSORAMOUNT;
	
	private String LINEITEM_VARIANCE;
	
	private String LINEITEM_SEQ;
	
	private Code PK_CODELST_COST_TYPE;
	
	private String SUBCOST_ITEM_FLAG;
	
	private String LINEITEM_APPLYPATIENTCOUNT;
	private LineItemNameIdentifier lineItemIdent;
	private EventNameIdentfier eventNameIdent;
	public String getLINEITEM_DESC() {
		return LINEITEM_DESC;
	}
	public void setLINEITEM_DESC(String lINEITEMDESC) {
		LINEITEM_DESC = lINEITEMDESC;
	}
	public String getLINEITEM_SPONSORUNIT() {
		return LINEITEM_SPONSORUNIT;
	}
	public void setLINEITEM_SPONSORUNIT(String lINEITEMSPONSORUNIT) {
		LINEITEM_SPONSORUNIT = lINEITEMSPONSORUNIT;
	}
	public String getLINEITEM_CLINICNOFUNIT() {
		return LINEITEM_CLINICNOFUNIT;
	}
	public void setLINEITEM_CLINICNOFUNIT(String lINEITEMCLINICNOFUNIT) {
		LINEITEM_CLINICNOFUNIT = lINEITEMCLINICNOFUNIT;
	}
	public String getLINEITEM_OTHERCOST() {
		return LINEITEM_OTHERCOST;
	}
	public void setLINEITEM_OTHERCOST(String lINEITEMOTHERCOST) {
		LINEITEM_OTHERCOST = lINEITEMOTHERCOST;
	}
	public String getLINEITEM_DELFLAG() {
		return LINEITEM_DELFLAG;
	}
	public void setLINEITEM_DELFLAG(String lINEITEMDELFLAG) {
		LINEITEM_DELFLAG = lINEITEMDELFLAG;
	}
	public String getLINEITEM_NOTES() {
		return LINEITEM_NOTES;
	}
	public void setLINEITEM_NOTES(String lINEITEMNOTES) {
		LINEITEM_NOTES = lINEITEMNOTES;
	}
	public String getLINEITEM_INPERSEC() {
		return LINEITEM_INPERSEC;
	}
	public void setLINEITEM_INPERSEC(String lINEITEMINPERSEC) {
		LINEITEM_INPERSEC = lINEITEMINPERSEC;
	}
	public String getLINEITEM_STDCARECOST() {
		return LINEITEM_STDCARECOST;
	}
	public void setLINEITEM_STDCARECOST(String lINEITEMSTDCARECOST) {
		LINEITEM_STDCARECOST = lINEITEMSTDCARECOST;
	}
	public String getLINEITEM_RESCOST() {
		return LINEITEM_RESCOST;
	}
	public void setLINEITEM_RESCOST(String lINEITEMRESCOST) {
		LINEITEM_RESCOST = lINEITEMRESCOST;
	}
	public String getLINEITEM_INVCOST() {
		return LINEITEM_INVCOST;
	}
	public void setLINEITEM_INVCOST(String lINEITEMINVCOST) {
		LINEITEM_INVCOST = lINEITEMINVCOST;
	}
	public String getLINEITEM_INCOSTDISC() {
		return LINEITEM_INCOSTDISC;
	}
	public void setLINEITEM_INCOSTDISC(String lINEITEMINCOSTDISC) {
		LINEITEM_INCOSTDISC = lINEITEMINCOSTDISC;
	}
	public String getLINEITEM_CPTCODE() {
		return LINEITEM_CPTCODE;
	}
	public void setLINEITEM_CPTCODE(String lINEITEMCPTCODE) {
		LINEITEM_CPTCODE = lINEITEMCPTCODE;
	}
	public String getLINEITEM_REPEAT() {
		return LINEITEM_REPEAT;
	}
	public void setLINEITEM_REPEAT(String lINEITEMREPEAT) {
		LINEITEM_REPEAT = lINEITEMREPEAT;
	}
	public String getLINEITEM_PARENTID() {
		return LINEITEM_PARENTID;
	}
	public void setLINEITEM_PARENTID(String lINEITEMPARENTID) {
		LINEITEM_PARENTID = lINEITEMPARENTID;
	}
	public String getLINEITEM_APPLYINFUTURE() {
		return LINEITEM_APPLYINFUTURE;
	}
	public void setLINEITEM_APPLYINFUTURE(String lINEITEMAPPLYINFUTURE) {
		LINEITEM_APPLYINFUTURE = lINEITEMAPPLYINFUTURE;
	}
	public Code getPK_CODELST_CATEGORY() {
		return PK_CODELST_CATEGORY;
	}
	public void setPK_CODELST_CATEGORY(Code PKCODELSTCATEGORY) {
		PK_CODELST_CATEGORY = PKCODELSTCATEGORY;
	}
	public String getLINEITEM_TMID() {
		return LINEITEM_TMID;
	}
	public void setLINEITEM_TMID(String lINEITEMTMID) {
		LINEITEM_TMID = lINEITEMTMID;
	}
	public String getLINEITEM_CDM() {
		return LINEITEM_CDM;
	}
	public void setLINEITEM_CDM(String lINEITEMCDM) {
		LINEITEM_CDM = lINEITEMCDM;
	}
	public String getLINEITEM_APPLYINDIRECTS() {
		return LINEITEM_APPLYINDIRECTS;
	}
	public void setLINEITEM_APPLYINDIRECTS(String lINEITEMAPPLYINDIRECTS) {
		LINEITEM_APPLYINDIRECTS = lINEITEMAPPLYINDIRECTS;
	}
	public String getLINEITEM_TOTALCOST() {
		return LINEITEM_TOTALCOST;
	}
	public void setLINEITEM_TOTALCOST(String lINEITEMTOTALCOST) {
		LINEITEM_TOTALCOST = lINEITEMTOTALCOST;
	}
	public String getLINEITEM_SPONSORAMOUNT() {
		return LINEITEM_SPONSORAMOUNT;
	}
	public void setLINEITEM_SPONSORAMOUNT(String lINEITEMSPONSORAMOUNT) {
		LINEITEM_SPONSORAMOUNT = lINEITEMSPONSORAMOUNT;
	}
	public String getLINEITEM_VARIANCE() {
		return LINEITEM_VARIANCE;
	}
	public void setLINEITEM_VARIANCE(String lINEITEMVARIANCE) {
		LINEITEM_VARIANCE = lINEITEMVARIANCE;
	}
	public String getLINEITEM_SEQ() {
		return LINEITEM_SEQ;
	}
	public void setLINEITEM_SEQ(String lINEITEMSEQ) {
		LINEITEM_SEQ = lINEITEMSEQ;
	}
	public Code getPK_CODELST_COST_TYPE() {
		return PK_CODELST_COST_TYPE;
	}
	public void setPK_CODELST_COST_TYPE(Code PKCODELSTCOSTTYPE) {
		PK_CODELST_COST_TYPE = PKCODELSTCOSTTYPE;
	}
	public String getSUBCOST_ITEM_FLAG() {
		return SUBCOST_ITEM_FLAG;
	}
	public void setSUBCOST_ITEM_FLAG(String sUBCOSTITEMFLAG) {
		SUBCOST_ITEM_FLAG = sUBCOSTITEMFLAG;
	}
	public String getLINEITEM_APPLYPATIENTCOUNT() {
		return LINEITEM_APPLYPATIENTCOUNT;
	}
	public void setLINEITEM_APPLYPATIENTCOUNT(String lINEITEMAPPLYPATIENTCOUNT) {
		LINEITEM_APPLYPATIENTCOUNT = lINEITEMAPPLYPATIENTCOUNT;
	}
	
	public LineItemNameIdentifier getLineItemIdent() {
		return lineItemIdent;
	}
	public void setLineItemIdent(LineItemNameIdentifier lineItemIdent) {
		this.lineItemIdent = lineItemIdent;
	}
	public EventNameIdentfier getEventNameIdent() {
		return eventNameIdent;
	}
	public void setEventNameIdent(EventNameIdentfier eventNameIdent) {
		this.eventNameIdent = eventNameIdent;
	}
}
