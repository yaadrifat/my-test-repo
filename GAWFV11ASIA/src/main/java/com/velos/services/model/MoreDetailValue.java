package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="MoreStudyDetails")

public class MoreDetailValue extends MoreStudyDetails implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1264544800749911979L;
	
	protected Object value;

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

}
