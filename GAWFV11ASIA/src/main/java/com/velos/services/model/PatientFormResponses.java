/**
 * Created On October 28, 2012
 */
package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Raman
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="PatientFormResponses")

public class PatientFormResponses implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -728647392643291452L;
	private List<PatientFormResponse> patientFormResponses = new ArrayList<PatientFormResponse>();
	private Integer recordCount=0;
	
	public Integer getRecordCount() {
		return recordCount;
	}
	public void setRecordCount(Integer recordCount) {
		this.recordCount = recordCount;
	}
	public List<PatientFormResponse> getPatientFormResponses() {
		return patientFormResponses;
	}
	public void setPatientFormResponses(
			List<PatientFormResponse> patientFormResponses) {
		this.patientFormResponses = patientFormResponses;
	}
	
	public void addPatientFormResponse(PatientFormResponse formResponse)
	{
		this.patientFormResponses.add(formResponse); 
	}
}
