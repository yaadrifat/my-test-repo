package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="StudyAttachment")
public class StudyAttachment implements Serializable {
	private static final long serialVersionUID = -1938152207514942146L;
	private static final String uriPrefix = "/velos/servlet/Download?OID=";
	protected SimpleIdentifier attachmentIdentifier;
	protected String filename;
	protected String version;
	protected Code category;
	protected Code type;
	protected String description;
	protected String uri;
	
	public SimpleIdentifier getAttachmentIdentifier() {
		return attachmentIdentifier;
	}
	public void setAttachmentIdentifier(SimpleIdentifier attachmentIdentifier) {
		this.attachmentIdentifier = attachmentIdentifier;
		this.uri = uriPrefix + this.attachmentIdentifier.getOID();
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public Code getCategory() {
		return category;
	}
	public void setCategory(Code category) {
		this.category = category;
	}
	public Code getType() {
		return type;
	}
	public void setType(Code type) {
		this.type = type;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
}
