package com.velos.services.model;



import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name="StudyVersion")
@XmlAccessorType (XmlAccessType.FIELD)
public class StudyVersion implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private StudyVersionIdentifier studyVerIdentifier;
	
	private String STUDYVER_STATUS;
	
	private String STUDYVER_NOTES;
	
	private Integer ORIG_STUDY;
	
	private Date STUDYVER_DATE;
	
	private Code STUDYVER_CATEGORY;
	
	private Code STUDYVER_TYPE;
	
	private List<StudyAppendix> studyAppendix;
	
	private List<StudyStatusHistory> studyStatusHistory;

	public StudyVersionIdentifier getStudyVerIdentifier() {
		return studyVerIdentifier;
	}
	public void setStudyVerIdentifier(StudyVersionIdentifier studyVerIdentifier) {
		this.studyVerIdentifier = studyVerIdentifier;
	}
	public String getSTUDYVER_STATUS() {
		return STUDYVER_STATUS;
	}
	public void setSTUDYVER_STATUS(String sTUDYVERSTATUS) {
		STUDYVER_STATUS = sTUDYVERSTATUS;
	}
	public String getSTUDYVER_NOTES() {
		return STUDYVER_NOTES;
	}
	public void setSTUDYVER_NOTES(String sTUDYVERNOTES) {
		STUDYVER_NOTES = sTUDYVERNOTES;
	}
	public Integer getORIG_STUDY() {
		return ORIG_STUDY;
	}
	public void setORIG_STUDY(Integer oRIGSTUDY) {
		ORIG_STUDY = oRIGSTUDY;
	}
	public Date getSTUDYVER_DATE() {
		return STUDYVER_DATE;
	}
	public void setSTUDYVER_DATE(Date sTUDYVERDATE) {
		STUDYVER_DATE = sTUDYVERDATE;
	}
	public Code getSTUDYVER_CATEGORY() {
		return STUDYVER_CATEGORY;
	}
	public void setSTUDYVER_CATEGORY(Code sTUDYVERCATEGORY) {
		STUDYVER_CATEGORY = sTUDYVERCATEGORY;
	}
	public Code getSTUDYVER_TYPE() {
		return STUDYVER_TYPE;
	}
	public void setSTUDYVER_TYPE(Code sTUDYVERTYPE) {
		STUDYVER_TYPE = sTUDYVERTYPE;
	}
	public StudyVersion() {
		super();
	}
	public List<StudyAppendix> getStudyAppendix() {
		return studyAppendix;
	}
	public void setStudyAppendixList(List<StudyAppendix> studyAppendix) {
		this.studyAppendix = studyAppendix;
	}
	
	public void setStudyAppendix(List<StudyAppendix> studyAppendix) {
		this.studyAppendix = studyAppendix;
	}
	public List<StudyStatusHistory> getStudyStatusHistory() {
		return studyStatusHistory;
	}
	public void setStudyStatusHistory(List<StudyStatusHistory> studyStatusHistory) {
		this.studyStatusHistory = studyStatusHistory;
	}

}
