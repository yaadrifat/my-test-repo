/**
 * 
 */
package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**Identifies a visit in eResearch
 * @author dylan
 *
 */
@XmlRootElement(name="Visit")
@XmlAccessorType(XmlAccessType.FIELD)
public class Visit implements Serializable {

	protected VisitIdentifier visitIdentifier;
	protected String visitName;
	protected String visitDescription;
	protected Events events;
	protected boolean noIntervalDefined;
	protected String VisitWindow;
	
	protected String visitSuggestedDate;
	protected String visitScheduledDate;
	
	public VisitIdentifier getVisitIdentifier() {
		return visitIdentifier;
	}

	public void setVisitIdentifier(VisitIdentifier visitIdentifier) {
		this.visitIdentifier = visitIdentifier;
	}

	public String getVisitName() {
		return visitName;
	}

	public void setVisitName(String visitName) {
		this.visitName = visitName;
	}

	public String getVisitDescription() {
		return visitDescription;
	}

	public void setVisitDescription(String visitDescription) {
		this.visitDescription = visitDescription;
	}

	public Events getEvents() {
		return events;
	}

	public void setEvents(Events events) {
		this.events = events;
	}

	public boolean isNoIntervalDefined() {
		return noIntervalDefined;
	}

	public void setNoIntervalDefined(boolean noIntervalDefined) {
		this.noIntervalDefined = noIntervalDefined;
	}

	public String getVisitWindow() {
		return VisitWindow;
	}

	public void setVisitWindow(String visitWindow) {
		VisitWindow = visitWindow;
	}

	public String getVisitSuggestedDate() {
		return visitSuggestedDate;
	}

	public void setVisitSuggestedDate(String visitSuggestedDate) {
		this.visitSuggestedDate = visitSuggestedDate;
	}

	public String getVisitScheduledDate() {
		return visitScheduledDate;
	}

	public void setVisitScheduledDate(String visitScheduledDate) {
		this.visitScheduledDate = visitScheduledDate;
	}
	
	
}
