package com.velos.services.model;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="StudyNIHGrantInfo")
public class StudyNIHGrantInfo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
     * the NIH grant Serial Number
     */
	private String nihGrantSerial;
	
	 /**
     * the Funding Mechanism Type
     */
	private Code fundMech;
	
	/**
     * the Institude Code
     */
	private Code instCode;
	
	/**
     * the NCI PRogram Code
     */
	private Code programCode;
	@NotNull
	public String getNihGrantSerial() {
		return nihGrantSerial;
	}

	public void setNihGrantSerial(String nihGrantSerial) {
		this.nihGrantSerial = nihGrantSerial;
	}
	@NotNull
    @Valid
	public Code getFundMech() {
		return fundMech;
	}

	public void setFundMech(Code fundMech) {
		this.fundMech = fundMech;
	}
	@NotNull
    @Valid
	public Code getInstCode() {
		return instCode;
	}

	public void setInstCode(Code instCode) {
		this.instCode = instCode;
	}
	@NotNull
    @Valid
	public Code getProgramCode() {
		return programCode;
	}

	public void setProgramCode(Code programCode) {
		this.programCode = programCode;
	}
	
}
