package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="BudgetLineItems")
@XmlAccessorType (XmlAccessType.FIELD)
public class BgtLiniItemDetail implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<BudgetLineItemPojo> budgetLineItemInfo;
	
	public List<BudgetLineItemPojo> getBudgetLineItemInfo() {
		return budgetLineItemInfo;
	}
	public void setBudgetLineItemInfo(List<BudgetLineItemPojo> budgetLineItemInfo) {
		this.budgetLineItemInfo = budgetLineItemInfo;
	}
}
