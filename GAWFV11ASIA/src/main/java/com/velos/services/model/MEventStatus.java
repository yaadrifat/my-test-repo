/**
 * Created On Dec 13, 2012
 */
package com.velos.services.model;

import java.io.Serializable;

/**
 * @author Kanwaldeep
 *
 */
public class MEventStatus implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9185538186621852004L;
	protected ScheduleEventIdentifier eventIdentifier; 
	protected EventStatus eventStatus;
	public ScheduleEventIdentifier getEventIdentifier() {
		return eventIdentifier;
	}
	public void setEventIdentifier(ScheduleEventIdentifier eventIdentifier) {
		this.eventIdentifier = eventIdentifier;
	}
	public EventStatus getEventStatus() {
		return eventStatus;
	}
	public void setEventStatus(EventStatus eventStatus) {
		this.eventStatus = eventStatus;
	} 
}
