package com.velos.services.model;

import java.io.Serializable;

public class OutboundOrganizationIdentifier implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6689170574810277395L;
	protected String siteName;
	public String getSiteName() {
		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}	

}
