package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="site")
public class Site implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4998213421400864477L;
	private SiteIdentifier siteIdentifier;
	private String name;
	private String description;
	private Code siteStatus;
	private String relationshipPK;
	private Code siteRelationshipType;
	private String level;
	private String ctepId;
	
	protected List<NVPair> moreSiteDetailsFields;
	public List<Site> site;
	public Sites sites;
	
	public List<Site> getCSites(){
		return site;
	}
	
	public void setCSites(List<Site> sites){
		this.site = sites;
	}
	
	public Sites getSites(){
		return sites;
	}
	
	public void setSites(Sites sites){
		this.sites = sites;
	}
	
	public SiteIdentifier getSiteIdentifier() {
		return siteIdentifier;
	}
	public void setSiteIdentifier(SiteIdentifier siteIdentifier) {
		this.siteIdentifier = siteIdentifier;
	}
	public String getName() {
		return name;
	}
	public void setName(String Name) {
		name = Name;
	}
	public Code getSiteStatus() {
		return siteStatus;
	}
	public void setSiteStatus(Code SiteStatus) {
		siteStatus = SiteStatus;
	}
	public String getRelationshipPK() {
		return relationshipPK;
	}
	public void setRelationshipPK(String relationship_PK) {
		relationshipPK = relationship_PK;
	}
	public Code getSiteRelationShipType() {
		return siteRelationshipType;
	}
	public void setSiteRelationShipType(Code SiteRelationShipType) {
		siteRelationshipType = SiteRelationShipType;
	}
	/*public String getSiteStatus() {
		return SiteStatus;
	}
	public void setSiteStatus(String siteStatus) {
		SiteStatus = siteStatus;
	}*/
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public List<NVPair> getMoreSiteDetailsFields() {
		return moreSiteDetailsFields;
	}
	public void setMoreSiteDetailsFields(List<NVPair> moreSiteDetailsFields) {
		this.moreSiteDetailsFields = moreSiteDetailsFields;
	}

	public String getCtepId() {
		return ctepId;
	}

	public void setCtepId(String CTEP_ID) {
		ctepId = CTEP_ID;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
