/**
 * Created On Jul 5, 2011
 */
package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

/**
 * @author Kanwaldeep
 *
 */
public class EventIdentifiers implements Serializable{
	


	/**
	 * 
	 */
	private static final long serialVersionUID = 282942740923497015L;
	private List<EventIdentifier> eventIdentifier;

	public List<EventIdentifier> getEventIdentifier() {
		return eventIdentifier;
	}

	public void setEventIdentifier(List<EventIdentifier> eventIdentifier) {
		this.eventIdentifier = eventIdentifier;
	}
	
	public void addEventIdentifier(EventIdentifier eventIdentifier)
	{
		this.eventIdentifier.add(eventIdentifier); 
	}

}
