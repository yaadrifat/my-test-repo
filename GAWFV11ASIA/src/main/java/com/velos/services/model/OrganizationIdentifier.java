/**
 * 
 */
package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Subclass of {@link SimpleIdentifier} that identifies organizations. Sites can be
 * identified by either a {@link #siteName} of {@link #siteAltId}
 * @author dylan
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="OrganizationIdentifier")
public class OrganizationIdentifier
	extends SimpleIdentifier
	implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6761450731373800512L;
	
	
	protected String siteAltId;
	protected String siteName;
	protected String ctepId;
	
	public OrganizationIdentifier(){
		
	}
	
	public OrganizationIdentifier(String siteName,	String siteAltId, String ctepId) {
		this.siteName = siteName;
		this.siteAltId = siteAltId;
		this.ctepId = ctepId;
	}
	
	public OrganizationIdentifier(String siteName,	String siteAltId) {
		this.siteName = siteName;
		this.siteAltId = siteAltId;
	}

	public String getSiteAltId() {
		return siteAltId;
	}

	public void setSiteAltId(String siteAltId) {
		this.siteAltId = siteAltId;
	}
	
	public void setSiteName(String siteName) {
		this.siteName = siteName ;
	}
	public String getSiteName() {
		return siteName;
	}
	
	@Override
	public String toString(){
		return "Organization: " + siteName + " " + siteAltId + " " + ctepId;
	}

	public String getCtepId() {
		return ctepId;
	}

	public void setCtepId(String ctepId) {
		this.ctepId = ctepId;
	}

	
}
