/**
 * Created On Nov 9, 2011
 */
package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.namespace.QName;

/**
 * @author Kanwaldeep
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="FieldResponse")
public class FormFieldResponse implements Serializable{	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3510472588381084523L;

	protected String value;
	protected String systemID;
	protected String uniqueID;
	protected FieldIdentifier fieldIdentifier; 
	protected Options options;
	
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public FieldIdentifier getFieldIdentifier() {
		return fieldIdentifier;
	}
	public void setFieldIdentifier(FieldIdentifier fieldIdentifier) {
		this.fieldIdentifier = fieldIdentifier;
	}
	public String getSystemID() {
		return systemID;
	}
	public void setSystemID(String systemID) {
		this.systemID = systemID;
	}
	public String getUniqueID() {
		return uniqueID;
	}
	public void setUniqueID(String uniqueID) {
		this.uniqueID = uniqueID;
	}
	public Options getOptions() {
		return options;
	}
	public void setOptions(Options options) {
		this.options = options;
	} 
}
