package com.velos.services.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="BudgetSectionInfo")
@XmlAccessorType (XmlAccessType.FIELD)
public class BudgetSectionPojo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String BGTSECTION_VISIT;
	
	private String BGTSECTION_DELFLAG;
	
	private String BGTSECTION_SEQUENCE;
	
	private String BGTSECTION_TYPE;
	
	private String BGTSECTION_PATNO;
	
	private String BGTSECTION_NOTES;
	
	private String BGTSECTION_PERSONLFLAG;
	
	private String SRT_COST_TOTAL;
	
	private String SRT_COST_GRANDTOTAL;
	
	private String SOC_COST_TOTAL;
	
	private String SOC_COST_GRANDTOTAL;
	
	private String SRT_COST_SPONSOR;
	
	private String SRT_COST_VARIANCE;
	
	private String SOC_COST_SPONSOR;
	
	private String SOC_COST_VARIANCE;
	private VisitNameIdentifier visitIdent;
	private BgtSectionNameIdentifier bgtSectionName;
	private BgtLiniItemDetail lineItems;

	public String getBGTSECTION_VISIT() {
		return BGTSECTION_VISIT;
	}
	public void setBGTSECTION_VISIT(String bGTSECTIONVISIT) {
		BGTSECTION_VISIT = bGTSECTIONVISIT;
	}
	public String getBGTSECTION_DELFLAG() {
		return BGTSECTION_DELFLAG;
	}
	public void setBGTSECTION_DELFLAG(String bGTSECTIONDELFLAG) {
		BGTSECTION_DELFLAG = bGTSECTIONDELFLAG;
	}
	public String getBGTSECTION_SEQUENCE() {
		return BGTSECTION_SEQUENCE;
	}
	public void setBGTSECTION_SEQUENCE(String bGTSECTIONSEQUENCE) {
		BGTSECTION_SEQUENCE = bGTSECTIONSEQUENCE;
	}
	public String getBGTSECTION_TYPE() {
		return BGTSECTION_TYPE;
	}
	public void setBGTSECTION_TYPE(String bGTSECTIONTYPE) {
		BGTSECTION_TYPE = bGTSECTIONTYPE;
	}
	public String getBGTSECTION_PATNO() {
		return BGTSECTION_PATNO;
	}
	public void setBGTSECTION_PATNO(String bGTSECTIONPATNO) {
		BGTSECTION_PATNO = bGTSECTIONPATNO;
	}
	public String getBGTSECTION_NOTES() {
		return BGTSECTION_NOTES;
	}
	public void setBGTSECTION_NOTES(String bGTSECTIONNOTES) {
		BGTSECTION_NOTES = bGTSECTIONNOTES;
	}
	public String getBGTSECTION_PERSONLFLAG() {
		return BGTSECTION_PERSONLFLAG;
	}
	public void setBGTSECTION_PERSONLFLAG(String bGTSECTIONPERSONLFLAG) {
		BGTSECTION_PERSONLFLAG = bGTSECTIONPERSONLFLAG;
	}
	public String getSRT_COST_TOTAL() {
		return SRT_COST_TOTAL;
	}
	public void setSRT_COST_TOTAL(String sRTCOSTTOTAL) {
		SRT_COST_TOTAL = sRTCOSTTOTAL;
	}
	public String getSRT_COST_GRANDTOTAL() {
		return SRT_COST_GRANDTOTAL;
	}
	public void setSRT_COST_GRANDTOTAL(String sRTCOSTGRANDTOTAL) {
		SRT_COST_GRANDTOTAL = sRTCOSTGRANDTOTAL;
	}
	public String getSOC_COST_TOTAL() {
		return SOC_COST_TOTAL;
	}
	public void setSOC_COST_TOTAL(String sOCCOSTTOTAL) {
		SOC_COST_TOTAL = sOCCOSTTOTAL;
	}
	public String getSOC_COST_GRANDTOTAL() {
		return SOC_COST_GRANDTOTAL;
	}
	public void setSOC_COST_GRANDTOTAL(String sOCCOSTGRANDTOTAL) {
		SOC_COST_GRANDTOTAL = sOCCOSTGRANDTOTAL;
	}
	public String getSRT_COST_SPONSOR() {
		return SRT_COST_SPONSOR;
	}
	public void setSRT_COST_SPONSOR(String sRTCOSTSPONSOR) {
		SRT_COST_SPONSOR = sRTCOSTSPONSOR;
	}
	public String getSRT_COST_VARIANCE() {
		return SRT_COST_VARIANCE;
	}
	public void setSRT_COST_VARIANCE(String sRTCOSTVARIANCE) {
		SRT_COST_VARIANCE = sRTCOSTVARIANCE;
	}
	public String getSOC_COST_SPONSOR() {
		return SOC_COST_SPONSOR;
	}
	public void setSOC_COST_SPONSOR(String sOCCOSTSPONSOR) {
		SOC_COST_SPONSOR = sOCCOSTSPONSOR;
	}
	public String getSOC_COST_VARIANCE() {
		return SOC_COST_VARIANCE;
	}
	public void setSOC_COST_VARIANCE(String sOCCOSTVARIANCE) {
		SOC_COST_VARIANCE = sOCCOSTVARIANCE;
	}
	public BgtSectionNameIdentifier getBgtSectionName() {
		return bgtSectionName;
	}
	public void setBgtSectionName(BgtSectionNameIdentifier bgtSectionName) {
		this.bgtSectionName = bgtSectionName;
	}
	public VisitNameIdentifier getVisitIdent() {
		return visitIdent;
	}
	public void setVisitIdent(VisitNameIdentifier visitIdent) {
		this.visitIdent = visitIdent;
	}
	public BgtLiniItemDetail getLineItems() {
		return lineItems;
	}
	public void setLineItems(BgtLiniItemDetail lineItems) {
		this.lineItems = lineItems;
	}
	
}
