/**
 * Created On Sep 29, 2011
 */
package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.velos.services.util.EnumUtil;

/**
 * @author Kanwaldeep
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="NumberRange")
public class NumberRange implements Serializable{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -5745732130045245820L;
	private Operator firstOperator;  
	private Operator secondOperator; 
	private String firstValue; 
	private String secondValue; 
	private LogicalOperator logicOperator; 

		
	public enum LogicalOperator
	{
	
		AND("and"), 
		OR("or");
		

		private LogicalOperator(String value)
		{
			this.logicalOperator = value; 
		}
		
		private final String logicalOperator; 
		public String toString(){
			return logicalOperator;
		}
		
	}
	

	public enum Operator
	{
		GREATER_THAN(">"),
		GREATER_THAN_EQUAL_TO(">="),
		LESS_THAN("<"), 
		LESS_THAN_EQUAL_TO("<="); 


		private Operator(String operator)
		{
			this.operator  = operator; 
		}
		
		private String operator; 
		
		public String toString(){
			return operator;
		}

	}
	


	public Operator getFirstOperator() {
		return firstOperator;
	}

	public void setFirstOperator(Operator firstOperator) {
		this.firstOperator = firstOperator;
	}
	
	public void setFirstOperator(String firstOperator) {
		this.firstOperator = EnumUtil.getEnumType(Operator.class, firstOperator);
	}

	public Operator getSecondOperator() {
		return secondOperator;
	}

	public void setSecondOperator(Operator secondOperator) {
		this.secondOperator = secondOperator;
	}
	public void setSecondOperator(String secondOperator) {
		this.secondOperator = EnumUtil.getEnumType(Operator.class, secondOperator);
	}

	public String getFirstValue() {
		return firstValue;
	}

	public void setFirstValue(String firstValue) {
		this.firstValue = firstValue;
	}

	public String getSecondValue() {
		return secondValue;
	}

	public void setSecondValue(String secondValue) {
		this.secondValue = secondValue;
	}

	public LogicalOperator getLogicOperator() {
		return logicOperator;
	}

	public void setLogicOperator(LogicalOperator logicOperator) {
		this.logicOperator = logicOperator;
	}

	/**
	 * @param string
	 */
	public void setLogicOperator(String string) {
		this.logicOperator = EnumUtil.getEnumType(LogicalOperator.class, string); 
		
	}
	
}
