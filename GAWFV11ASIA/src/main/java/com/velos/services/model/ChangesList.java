package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ChangesList implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2092253926230398327L;
	
	protected List<Changes> changes = new ArrayList<Changes>();
	
	public List<Changes> getChanges() {
		return changes;
	}

	public void setChanges(List<Changes> changes) {
		this.changes = changes;
	}



	public void addChanges(Changes changes){
		this.changes.add(changes);
	}

}

