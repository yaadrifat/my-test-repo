package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Parminder
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="AccountFormResponses")
public class AccountFormResponses implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<AccountFormResponse> accountFormResponses = new ArrayList<AccountFormResponse>();
	private Integer recordCount=0;
	
	public Integer getRecordCount() {
		return recordCount;
	}
	public void setRecordCount(Integer recordCount) {
		this.recordCount = recordCount;
	}
	public List<AccountFormResponse> getAccountFormResponses() {
		return accountFormResponses;
	}
	public void setAccountFormResponses(
			List<AccountFormResponse> accountFormResponses) {
		this.accountFormResponses = accountFormResponses;
	}
	
	public void addAccountFormResponse(AccountFormResponse formResponse)
	{
		this.accountFormResponses.add(formResponse); 
	}

}
