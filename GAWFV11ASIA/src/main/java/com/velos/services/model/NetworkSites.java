package com.velos.services.model;

import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;



@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="NetworkSites")
public class NetworkSites implements Serializable{

	
	private static final long serialVersionUID = 4732997687729050740L;
	private SiteIdentifier siteIdentifier;
	private String sitename;
	private String sitelevel;
	private Code sitestatus;
	protected List<NVPair> moreUserDetailsFields;
	private Roles roles;
	private String CTEP_ID;
	public List<NetworkSites> site;
	private String Relationship_PK;
	private Code SiteRelationShipType;
	
	public List<NetworkSites> getSite() {
		return site;
	}
	public void setSite(List<NetworkSites> site) {
		this.site = site;
	}
	public SiteIdentifier getSiteIdentifier() {
		return siteIdentifier;
	}
	public void setSiteIdentifier(SiteIdentifier siteIdentifier) {
		this.siteIdentifier = siteIdentifier;
	}
	
	public void setName(String sitename){
		this.sitename=sitename;
	}
	
	public String getName(){
		return sitename;
	}
	
	public void setLevel(String sitelevel){
		this.sitelevel=sitelevel;
		}

    public String getLevel(){
	  return sitelevel;
     }
     public void setSiteStatus(Code sitestatus){
		this.sitestatus=sitestatus;
		}

    public Code getSiteStatus(){
	  return sitestatus;
    }
    public List<NVPair> getUserDetailsFields() {
		return moreUserDetailsFields;
	}
	public void setUserDetailsFields(List<NVPair> moreUserDetailsFields) {
		this.moreUserDetailsFields = moreUserDetailsFields;
	}
 
    public void setRoles(Roles roles){
		this.roles=roles;
		}

    public Roles getRole(){
	  return roles;
    }
	public String getCTEP_ID() {
		return CTEP_ID;
	}
	public void setCTEP_ID(String cTEP_ID) {
		CTEP_ID = cTEP_ID;
	}
	public String getRelationship_PK() {
		return Relationship_PK;
	}
	public void setRelationship_PK(String relationship_PK) {
		Relationship_PK = relationship_PK;
	}
	public Code getSiteRelationShipType() {
		return SiteRelationShipType;
	}
	public void setSiteRelationShipType(Code siteRelationShipType) {
		SiteRelationShipType = siteRelationShipType;
	}
  
}