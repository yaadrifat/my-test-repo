/**
 * Created On May 20, 2011
 */
package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Kanwaldeep
 *
 */
public class Costs implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<Cost> cost = new ArrayList<Cost>();

	/**
	 * @param cost the cost to set
	 */
	public void setCost(List<Cost> cost) {
		this.cost = cost;
	}

	/**
	 * @return the cost
	 */
	public List<Cost> getCost() {
		return cost;
	} 
	
	
	public void setCost(Cost cost)
	{
		this.cost.add(cost); 
	}

}
