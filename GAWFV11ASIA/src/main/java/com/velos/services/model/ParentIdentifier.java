/**
 * Created On Mar 22, 2011
 */
package com.velos.services.model;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Kanwaldeep
 * This class represents the Composite Key
 */
//@XmlAccessorType(XmlAccessType.FIELD)
//@XmlRootElement(name="ParentIdentifier")
public class ParentIdentifier implements Serializable{
	
	/**
     * 
     */
    private static final long serialVersionUID = -1927463741537259259L;
    protected List<SimpleIdentifier> id;
	
	public ParentIdentifier()
	{
		id = new ArrayList<SimpleIdentifier>(); 
	}
	public ParentIdentifier(List<SimpleIdentifier> id)
	{
		this.id = id; 
	}
	/**
	 * @param list of {@link com.velos.services.model.SimpleIdentifier} to set to represent composite key.
	 */
	public void setId(List<SimpleIdentifier> id) {
		this.id = id;
	}

	/**
	 * @return list of  {@link com.velos.services.model.SimpleIdentifier} to represent composite key.
	 */
	public List<SimpleIdentifier> getId() {
		return id;
	} 
	
	/**
	 * @param parentid
	 * @return boolean to represent add failed or succeeded.
	 */
	public boolean addIdentifier(SimpleIdentifier parentid)
	{
		return id.add(parentid);
	}

}
