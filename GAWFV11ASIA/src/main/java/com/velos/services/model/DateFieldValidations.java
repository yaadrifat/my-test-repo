/**
 * Created On Sep 30, 2011
 */
package com.velos.services.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Kanwaldeep
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DateFieldValidations")
public class DateFieldValidations extends FieldValidations{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -250850521876836899L;
	private boolean overrideDateValidation;
	private boolean futureDateAllowed =true;
	private boolean defaultDateToCurrent;
	public boolean isOverrideDateValidation() {
		return overrideDateValidation;
	}
	public void setOverrideDateValidation(boolean overrideDateValidation) {
		this.overrideDateValidation = overrideDateValidation;
	}
	public boolean isFutureDateAllowed() {
		return futureDateAllowed;
	}
	public void setFutureDateAllowed(boolean futureDateAllowed) {
		this.futureDateAllowed = futureDateAllowed;
	}
	public boolean isDefaultDateToCurrent() {
		return defaultDateToCurrent;
	}
	public void setDefaultDateToCurrent(boolean defaultDateToCurrent) {
		this.defaultDateToCurrent = defaultDateToCurrent;
	}	

}
