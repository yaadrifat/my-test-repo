package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Role")
public class Roles implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 281937487309955178L;
	/**
	 * 
	 */
	
	private List<NetworkRole> role=new ArrayList<NetworkRole>();
	
	public List<NetworkRole> getRoles() {
		return role;
	}
	public void setRoles(List<NetworkRole> role) {
		this.role = role;
	}
}
