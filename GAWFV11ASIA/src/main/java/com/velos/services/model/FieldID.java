package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

public class FieldID implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5895945991878773258L;
	
	private List<String> fieldID;
	
	public List<String> getFieldID() {
		return fieldID;
	}
	public void setFieldID(List<String> fieldID) {
		this.fieldID = fieldID;
	}

}
