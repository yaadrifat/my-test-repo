package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="BudgetSections")
@XmlAccessorType (XmlAccessType.FIELD)
public class BudgetCalDetail implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<BgtSectionDetail> budgetSections;
	public List<BgtSectionDetail> getBudgetSections() {
		return budgetSections;
	}
	public void setBudgetSections(List<BgtSectionDetail> budgetSections) {
		this.budgetSections = budgetSections;
	}
		
}
