package com.velos.services.model;

import java.io.Serializable;


public class OutboundStudyIdentifier implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6051879693029189683L;
	protected String studyNumber;

	public OutboundStudyIdentifier() {
		super();
	}
	
	public OutboundStudyIdentifier(String studyNumber) {
		this.studyNumber = studyNumber;
	}

	public String getStudyNumber() {
		return studyNumber;
	}
	public void setStudyNumber(String studyNumber) {
		this.studyNumber = studyNumber;
	}

	@Override
	public String toString(){
		return studyNumber;
	}
}
