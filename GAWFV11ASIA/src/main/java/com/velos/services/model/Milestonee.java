package com.velos.services.model;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name="milestone")
@XmlAccessorType (XmlAccessType.FIELD)
public class Milestonee 
implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    
	private MilestoneIdentifier milestoneIdent;
	
    private String MILESTONE_TYPE;
	
    private CalendarNameIdentifier calendarNameIdent;
	
    private Code PK_CODELST_RULE;
	
    private String MILESTONE_AMOUNT;

    private EventNameIdentfier eventNameIdent;
	
    private Integer MILESTONE_COUNT;
	
    private String MILESTONE_DELFLAG;
	
    private Integer MILESTONE_LIMIT;
	
    private VisitNameIdentifier visitNameIdent;
	
    private Code MILESTONE_PATSTUDY_STAT;
	
    private Code MILESTONE_PAYTYPE;
	
	private Code MILESTONE_PAYFOR;
	
	private Code MILESTONE_EVENTSTATUS;
	
    private BudgetIdentifier budgetIdentifier;
    
    private BgtCalNameIdentifier bgtCalNameIdentifier;
    
    private BgtSectionNameIdentifier bgtSectionNameIdentifier;
    
    private LineItemNameIdentifier lineItemNameIdentifier;
    
    private Code PK_CODELST_MILESTONE_STAT;
    
    private String MILESTONE_DESCRIPTION;
    
    private Date MILESTONE_DATE_FROM;
    
    private Date MILESTONE_DATE_TO;
    
    private String MILESTONE_HOLDBACK;
    
    public MilestoneIdentifier getMilestoneIdent() {
		return milestoneIdent;
	}
	public void setMilestoneIdent(MilestoneIdentifier milestoneIdent) {
		this.milestoneIdent = milestoneIdent;
	}
	public String getMILESTONE_TYPE() {
		return MILESTONE_TYPE;
	}
	public void setMILESTONE_TYPE(String mILESTONETYPE) {
		MILESTONE_TYPE = mILESTONETYPE;
	}
	public String getMILESTONE_AMOUNT() {
		return MILESTONE_AMOUNT;
	}
	public void setMILESTONE_AMOUNT(String mILESTONEAMOUNT) {
		MILESTONE_AMOUNT = mILESTONEAMOUNT;
	}
	public Integer getMILESTONE_COUNT() {
		return MILESTONE_COUNT;
	}
	public void setMILESTONE_COUNT(Integer mILESTONECOUNT) {
		MILESTONE_COUNT = mILESTONECOUNT;
	}
	public String getMILESTONE_DELFLAG() {
		return MILESTONE_DELFLAG;
	}
	public void setMILESTONE_DELFLAG(String mILESTONEDELFLAG) {
		MILESTONE_DELFLAG = mILESTONEDELFLAG;
	}
	public Integer getMILESTONE_LIMIT() {
		return MILESTONE_LIMIT;
	}
	public void setMILESTONE_LIMIT(Integer mILESTONELIMIT) {
		MILESTONE_LIMIT = mILESTONELIMIT;
	}
	public String getMILESTONE_DESCRIPTION() {
		return MILESTONE_DESCRIPTION;
	}
	public void setMILESTONE_DESCRIPTION(String mILESTONEDESCRIPTION) {
		MILESTONE_DESCRIPTION = mILESTONEDESCRIPTION;
	}
	public Date getMILESTONE_DATE_FROM() {
		return MILESTONE_DATE_FROM;
	}
	public void setMILESTONE_DATE_FROM(Date mILESTONEDATEFROM) {
		MILESTONE_DATE_FROM = mILESTONEDATEFROM;
	}
	public Date getMILESTONE_DATE_TO() {
		return MILESTONE_DATE_TO;
	}
	public void setMILESTONE_DATE_TO(Date mILESTONEDATETO) {
		MILESTONE_DATE_TO = mILESTONEDATETO;
	}
	public String getMILESTONE_HOLDBACK() {
		return MILESTONE_HOLDBACK;
	}
	public void setMILESTONE_HOLDBACK(String mILESTONE_HOLDBACK) {
		MILESTONE_HOLDBACK = mILESTONE_HOLDBACK;
	}
	public Code getPK_CODELST_RULE() {
		return PK_CODELST_RULE;
	}
	public void setPK_CODELST_RULE(Code pKCODELSTRULE) {
		PK_CODELST_RULE = pKCODELSTRULE;
	}
	public Code getMILESTONE_PATSTUDY_STAT() {
		return MILESTONE_PATSTUDY_STAT;
	}
	public void setMILESTONE_PATSTUDY_STAT(Code mILESTONE_PATSTUDY_STAT) {
		MILESTONE_PATSTUDY_STAT = mILESTONE_PATSTUDY_STAT;
	}
	public Code getMILESTONE_PAYTYPE() {
		return MILESTONE_PAYTYPE;
	}
	public void setMILESTONE_PAYTYPE(Code mILESTONEPAYTYPE) {
		MILESTONE_PAYTYPE = mILESTONEPAYTYPE;
	}
	public Code getMILESTONE_PAYFOR() {
		return MILESTONE_PAYFOR;
	}
	public void setMILESTONE_PAYFOR(Code mILESTONEPAYFOR) {
		MILESTONE_PAYFOR = mILESTONEPAYFOR;
	}
	public Code getMILESTONE_EVENTSTATUS() {
		return MILESTONE_EVENTSTATUS;
	}
	public void setMILESTONE_EVENTSTATUS(Code mILESTONEEVENTSTATUS) {
		MILESTONE_EVENTSTATUS = mILESTONEEVENTSTATUS;
	}
	public Code getPK_CODELST_MILESTONE_STAT() {
		return PK_CODELST_MILESTONE_STAT;
	}
	public void setPK_CODELST_MILESTONE_STAT(Code pKCODELSTMILESTONESTAT) {
		PK_CODELST_MILESTONE_STAT = pKCODELSTMILESTONESTAT;
	}
	
	public VisitNameIdentifier getVisitNameIdent() {
		return visitNameIdent;
	}
	public void setVisitNameIdent(VisitNameIdentifier visitNameIdent) {
		this.visitNameIdent = visitNameIdent;
	}
	
	public CalendarNameIdentifier getCalendarNameIdent() {
		return calendarNameIdent;
	}
	public void setCalendarNameIdent(CalendarNameIdentifier calendarNameIdent) {
		this.calendarNameIdent = calendarNameIdent;
	}
	public BudgetIdentifier getBudgetIdentifier() {
		return budgetIdentifier;
	}
	public void setBudgetIdentifier(BudgetIdentifier budgetIdentifier) {
		this.budgetIdentifier = budgetIdentifier;
	}
	public EventNameIdentfier getEventNameIdent() {
		return this.eventNameIdent;
	}
	public void setEventNameIdent(EventNameIdentfier eventNameIdent) {
		this.eventNameIdent = eventNameIdent;
	}
	public BgtCalNameIdentifier getBgtCalNameIdentifier() {
		return bgtCalNameIdentifier;
	}
	public void setBgtCalNameIdentifier(BgtCalNameIdentifier bgtCalNameIdentifier) {
		this.bgtCalNameIdentifier = bgtCalNameIdentifier;
	}
	public BgtSectionNameIdentifier getBgtSectionNameIdentifier() {
		return bgtSectionNameIdentifier;
	}
	public void setBgtSectionNameIdentifier(
			BgtSectionNameIdentifier bgtSectionNameIdentifier) {
		this.bgtSectionNameIdentifier = bgtSectionNameIdentifier;
	}
	public LineItemNameIdentifier getLineItemNameIdentifier() {
		return lineItemNameIdentifier;
	}
	public void setLineItemNameIdentifier(
			LineItemNameIdentifier lineItemNameIdentifier) {
		this.lineItemNameIdentifier = lineItemNameIdentifier;
	}
	public Milestonee() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
