/**
 * Created On May 31, 2011
 */
package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Kanwaldeep
 *
 */
public class StudyOrganizations implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1876809328393402092L;
	protected List<StudyOrganization> studyOrganizaton;
	
	protected StudyIdentifier parentIdentifier;

	@NotNull
	@Size(min=1)
	@Valid
	public List<StudyOrganization> getStudyOrganizaton() {
		return studyOrganizaton;
	}

	public void setStudyOrganizaton(List<StudyOrganization> studyOrganizaton) {
		this.studyOrganizaton = studyOrganizaton;
	} 
	
	public StudyIdentifier getParentIdentifier() {
		return parentIdentifier;
	}


	public void setParentIdentifier(StudyIdentifier parentIdentifier) {
		this.parentIdentifier = parentIdentifier;
	}

}
