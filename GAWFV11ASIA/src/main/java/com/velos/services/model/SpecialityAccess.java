/**
 * Created On Jun 20, 2011
 */
package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Kanwaldeep
 *
 */
@XmlRootElement(name="AccessToSpecialities")
@XmlAccessorType(XmlAccessType.FIELD)
public class SpecialityAccess implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6422526581620798835L;
	protected List<Code> speciality;

	public List<Code> getSpeciality() {
		return speciality;
	}

	public void setSpeciality(List<Code> speciality) {
		this.speciality = speciality;
	} 
	

}
