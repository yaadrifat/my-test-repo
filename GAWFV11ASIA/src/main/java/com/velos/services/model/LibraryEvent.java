package com.velos.services.model;
/*
 * Author: Tarandeep Singh Bali
 */

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="LibraryEvent")
public class LibraryEvent implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8025671125835081193L;
	
	protected EventIdentifier libraryEventIdentifier;
	protected String eventCategory;
	protected String eventName;
	protected String notes;
	protected String description;
	protected String cost;
	protected String additionalCode;
	protected String facility;
	protected String cptCode;
	
	public LibraryEvent(){
		
	}
	
	public EventIdentifier getLibraryEventIdentifier() {
		return libraryEventIdentifier;
	}

	public void setLibraryEventIdentifier(EventIdentifier libraryEventIdentifier) {
		this.libraryEventIdentifier = libraryEventIdentifier;
	}

	public String getEventCategory() {
		return eventCategory;
	}

	public void setEventCategory(String eventCategory) {
		this.eventCategory = eventCategory;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getAdditionalCode() {
		return additionalCode;
	}

	public void setAdditionalCode(String additionalCode) {
		this.additionalCode = additionalCode;
	}

	public String getFacility() {
		return facility;
	}

	public void setFacility(String facility) {
		this.facility = facility;
	}

	public String getCptCode() {
		return cptCode;
	}

	public void setCptCode(String cptCode) {
		this.cptCode = cptCode;
	}
	
	

//	public List<Cost> getCosts() {
//		return costs;
//	}
//
//	public void setCosts(List<Cost> costs) {
//		this.costs = costs;
//	}
	
	

}
