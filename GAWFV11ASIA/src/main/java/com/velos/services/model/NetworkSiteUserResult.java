package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.velos.services.ResponseHolder;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="networks")
public class NetworkSiteUserResult implements Serializable{


		private static final long serialVersionUID = -6862455056198427891L;
		private ResponseHolder response;
		private List<Network> network = new ArrayList<Network>();

			public ResponseHolder getResponse() {
			return response;
		}
		public void setResponse(ResponseHolder response) {
			this.response = response;
		}
		
		public List<Network> getNetworks() {
			return network;
		}
		public void setNetworks(List<Network> networks) {
			this.network = networks;
		}

		public void addNetworks(Network networks){
			this.network.add(networks);
		}

}
