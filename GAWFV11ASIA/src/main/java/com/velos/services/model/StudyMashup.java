package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * Data Transfer object carrying Study Mashup information. 
 * @author Tarandeep Singh Bali
 *
 */

@XmlRootElement(name="StudyMashup")
@XmlAccessorType(XmlAccessType.FIELD)
public class StudyMashup implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3761976235799386035L;
	
	protected Study study;
	protected StudyFormDesign studyFormDesign;
	
	public Study getStudy() {
		return study;
	}
	public void setStudy(Study study) {
		this.study = study;
	}
	public StudyFormDesign getStudyFormDesign() {
		return studyFormDesign;
	}
	public void setStudyFormDesign(StudyFormDesign studyFormDesign) {
		this.studyFormDesign = studyFormDesign;
	}
	
	
	
}
