/**
 * 
 */
package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.velos.services.model.FormField.SortOrder;
import com.velos.services.model.OrganizationSearch.OrgSearchOrderBy;

/**
 * @author dylan
 *
 */


@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="NetworkUserDetail")
public class NetworkUserDetail extends SimpleIdentifier	implements Serializable{
	
	private static final long serialVersionUID = 4765502071998196322L;
	private String userLoginName;
	protected List<NVPair> moreUserDetails;
	private NetworkSite networkdetails ;	

	

	public String getUserLoginName() {
		return userLoginName;
	}

	public void setUserLoginName(String userId) {
		this.userLoginName = userId;
	}	
	
	
	public List<NVPair> getmoreUserDetails() {
		return moreUserDetails;
	}
	
	public void setmoreUserDetails(List<NVPair> moreUserDetails) {
		this.moreUserDetails = moreUserDetails;
	}
	
	public NetworkSite getNetworkDetails() {
		return networkdetails;
	}


	public void setNetworkDetails(NetworkSite networkdetails) {
		this.networkdetails = networkdetails;
	}

	
	
	
	
	
}
