/**
 * 
 */
package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.velos.services.model.FormField.SortOrder;
import com.velos.services.model.OrganizationSearch.OrgSearchOrderBy;

/**
 * @author dylan
 *
 */


@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="NetworkUserDetails")
public class NetworkUserDetails implements Serializable{
	
	private static final long serialVersionUID = 4765502071998196322L;
	
	private NetworkSite networkDetails ;
	
	protected List<MoreNetworkUserDetails> moreNetworkUserDetails;

	private String userLoginName;
	
	private String userPK;
	
	private String rolePK;
		
	public List<MoreNetworkUserDetails> getMoreNetworkUserDetails() {
		return moreNetworkUserDetails;
	}
	
	public void setMoreNetworkUserDetails(List<MoreNetworkUserDetails> moreNetworkUserDetails) {
		this.moreNetworkUserDetails = moreNetworkUserDetails;
	}
	
	public NetworkSite getNetworkDetails() {
		return networkDetails;
	}


	public void setNetwork(NetworkSite networkdetails) {
		this.networkDetails = networkdetails;
	}

	public String getUserLoginName() {
		return userLoginName;
	}

	public void setUserLoginName(String userLoginName) {
		this.userLoginName = userLoginName;
	}

	public String getUserPK() {
		return userPK;
	}

	public void setUserPK(String userPK) {
		this.userPK = userPK;
	}

	public String getRolePK() {
		return rolePK;
	}

	public void setRolePK(String rolePK) {
		this.rolePK = rolePK;
	}
	
}
