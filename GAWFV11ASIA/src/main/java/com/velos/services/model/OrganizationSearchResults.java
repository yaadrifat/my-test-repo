package com.velos.services.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.velos.services.ResponseHolder;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="OrganizationSearchResults")
public class OrganizationSearchResults extends ResponseHolder{

	
    /**
	 * 
	 */
	private static final long serialVersionUID = 3335282992425101311L;

	protected List<OrganizationDetail> organization = new ArrayList<OrganizationDetail>();
	
	private Integer pageNumber = null;
	
	private Long pageSize = null;
	
	private Long totalCount = null;

	public List<OrganizationDetail> getOrganization() {
		return organization;
	}

	public void setOrganizations(List<OrganizationDetail> organization) {
		this.organization = organization;
	}
	
	public void addOrganization(OrganizationDetail organization){
		this.organization.add(organization);
	}

	public Integer getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Long getPageSize() {
		return pageSize;
	}

	public void setPageSize(Long pageSize) {
		this.pageSize = pageSize;
	}

	public Long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}
}
