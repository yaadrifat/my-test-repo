package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author Tarandeep Singh Bali
 *
 */

public class MultipleEvents implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5562387814040454982L;
	
	private List<MultipleEvent> event;

	public List<MultipleEvent> getEvent() {
		return event;
	}

	public void setEvent(List<MultipleEvent> event) {
		this.event = event;
	}
	
	

}
