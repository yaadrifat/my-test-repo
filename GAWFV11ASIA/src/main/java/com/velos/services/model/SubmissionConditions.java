package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="submissionConditions")
public class SubmissionConditions implements Serializable{

	protected List<SubmissionCondition> submissionCondition;

	public List<SubmissionCondition> getSubmissionCondition() {
		return submissionCondition;
	}

	public void setSubmissionCondition(List<SubmissionCondition> submissionCondition) {
		this.submissionCondition = submissionCondition;
	}

}
