package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="JMSPatientProtocolIdentifier")
/**
 * Subclass of {@link SimpleIdentifier} used to identify a {@link Study}
 */
public class OutboundPatientProtocolIdentifier implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3070664576423685733L;
	protected OutboundIdentifier id = new OutboundIdentifier();
	public OutboundIdentifier getId() {
		return id;
	}
	public void setId(OutboundIdentifier id) {
		this.id = id;
	}
}
