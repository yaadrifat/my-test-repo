package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="EventStatusHistory")
public class EventStatusHistory implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1035419524175981227L;
	
	private EventStatuses eventStatuses;

	public EventStatuses getEventStatuses() {
		return eventStatuses;
	}

	public void setEventStatuses(EventStatuses eventStatuses) {
		this.eventStatuses = eventStatuses;
	}
	
	

}
