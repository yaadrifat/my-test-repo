
package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;



@XmlRootElement(name="StudyAppendix")
@XmlAccessorType (XmlAccessType.FIELD)
public class StudyAppendix implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private StudyApndxIdentifier studyApndxIdent;
	
	private String STUDYAPNDX_TYPE;
	
	private String STUDYAPNDX_URI;
	
	private String STUDYAPNDX_DESC;
	
	private String STUDYAPNDX_PUBFLAG;
	
	private String STUDYAPNDX_FILE;
	
	private byte[] STUDYAPNDX_FILEOBJ;
	
	private Integer STUDYAPNDX_FILESIZE;
	
	private byte[] fileStream;
	
	
	public StudyApndxIdentifier getStudyApndxIdent() {
		return studyApndxIdent;
	}
	public void setStudyApndxIdent(StudyApndxIdentifier studyApndxIdent) {
		this.studyApndxIdent = studyApndxIdent;
	}
	public String getSTUDYAPNDX_TYPE() {
		return STUDYAPNDX_TYPE;
	}
	public void setSTUDYAPNDX_TYPE(String sTUDYAPNDXTYPE) {
		STUDYAPNDX_TYPE = sTUDYAPNDXTYPE;
	}
	public String getSTUDYAPNDX_URI() {
		return STUDYAPNDX_URI;
	}
	public void setSTUDYAPNDX_URI(String sTUDYAPNDXURI) {
		STUDYAPNDX_URI = sTUDYAPNDXURI;
	}
	public String getSTUDYAPNDX_DESC() {
		return STUDYAPNDX_DESC;
	}
	public void setSTUDYAPNDX_DESC(String sTUDYAPNDXDESC) {
		STUDYAPNDX_DESC = sTUDYAPNDXDESC;
	}
	public String getSTUDYAPNDX_PUBFLAG() {
		return STUDYAPNDX_PUBFLAG;
	}
	public void setSTUDYAPNDX_PUBFLAG(String sTUDYAPNDXPUBFLAG) {
		STUDYAPNDX_PUBFLAG = sTUDYAPNDXPUBFLAG;
	}
	
	public String getSTUDYAPNDX_FILE() {
		return STUDYAPNDX_FILE;
	}
	public void setSTUDYAPNDX_FILE(String sTUDYAPNDXFILE) {
		STUDYAPNDX_FILE = sTUDYAPNDXFILE;
	}

	public Integer getSTUDYAPNDX_FILESIZE() {
		return STUDYAPNDX_FILESIZE;
	}
	public void setSTUDYAPNDX_FILESIZE(Integer sTUDYAPNDXFILESIZE) {
		STUDYAPNDX_FILESIZE = sTUDYAPNDXFILESIZE;
	}
	public StudyAppendix() {
		super();
	}
	public byte[] getFileStream() {
		return fileStream;
	}
	public void setFileStream(byte[] fileStream) {
		this.fileStream = fileStream;
	}
	public byte[] getSTUDYAPNDX_FILEOBJ() {
		return STUDYAPNDX_FILEOBJ;
	}
	public void setSTUDYAPNDX_FILEOBJ(byte[] sTUDYAPNDXFILEOBJ) {
		STUDYAPNDX_FILEOBJ = sTUDYAPNDXFILEOBJ;
	}

}
