package com.velos.services.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Parminder Singh
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="FieldNameValIdentifier")
public class FieldNameValIdentifier extends FieldIdentifier {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected String systemID;
	protected String uniqueID;
	protected String value;
	protected Integer exactSearch;
	public String getSystemID() {
		return systemID;
	}
	public void setSystemID(String systemID) {
		this.systemID = systemID;
	}
	public String getUniqueID() {
		return uniqueID;
	}
	public void setUniqueID(String uniqueID) {
		this.uniqueID = uniqueID;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public Integer getExactSearch() {
		return exactSearch;
	}
	public void setExactSearch(Integer exactSearch) {
		this.exactSearch = exactSearch;
	}
}
