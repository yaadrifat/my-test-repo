/**
 * Created On October 17, 2012
 */
package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Raman
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="StudyPatientFormResponses")

public class StudyPatientFormResponses implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -728647392643291452L;
	private List<StudyPatientFormResponse> studyPatientFormResponses = new ArrayList<StudyPatientFormResponse>();
	private Integer recordCount=0;
	
	public Integer getRecordCount() {
		return recordCount;
	}
	public void setRecordCount(Integer recordCount) {
		this.recordCount = recordCount;
	}
	public List<StudyPatientFormResponse> getStudyPatientFormResponses() {
		return studyPatientFormResponses;
	}
	public void setStudyPatientFormResponses(
			List<StudyPatientFormResponse> studyPatientFormResponses) {
		this.studyPatientFormResponses = studyPatientFormResponses;
	}
	
	public void addStudyPatientFormResponse(StudyPatientFormResponse formResponse)
	{
		this.studyPatientFormResponses.add(formResponse); 
	}
}
