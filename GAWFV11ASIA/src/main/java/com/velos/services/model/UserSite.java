package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="site")
public class UserSite implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4998213421400864477L;
	private SiteIdentifier siteIdentifier;
	private String name;
	private String description;
	private String level;
	private Code siteStatus;
	private String ctepId;
	private String relationshipPK;
	private Code siteRelationshipType;
	private UserRoleIdentifier users;
	
	
	
	protected List<NVPair> moreSiteDetailsFields;
	public List<UserSite> site;
	public UserSites sites;
	
	public List<UserSite> getCSites(){
		return site;
	}
	
	public void setCSites(List<UserSite> sites){
		this.site = sites;
	}
	
	public UserSites getSites(){
		return sites;
	}
	
	public void setSites(UserSites sites){
		this.sites = sites;
	}
	
	public SiteIdentifier getSiteIdentifier() {
		return siteIdentifier;
	}
	public void setSiteIdentifier(SiteIdentifier siteIdentifier) {
		this.siteIdentifier = siteIdentifier;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public Code getSiteStatus() {
		return siteStatus;
	}
	public void setSiteStatus(Code SiteStatus) {
		siteStatus = SiteStatus;
	}
	public String getCtepId() {
		return ctepId;
	}

	public void setCtepId(String CTEP_ID) {
		ctepId = CTEP_ID;
	}

	public String getRelationshipPK() {
		return relationshipPK;
	}
	public void setRelationshipPK(String relationship_PK) {
		relationshipPK = relationship_PK;
	}
	
	public Code getSiteRelationShipType() {
		return siteRelationshipType;
	}
	public void setSiteRelationShipType(Code SiteRelationShipType) {
		siteRelationshipType = SiteRelationShipType;
	}

	public UserRoleIdentifier getUsers() {
		return users;
	}

	public void setUsers(UserRoleIdentifier users) {
		this.users = users;
	}
	
	/*public String getSiteStatus() {
		return SiteStatus;
	}
	public void setSiteStatus(String siteStatus) {
		SiteStatus = siteStatus;
	}*/
		
}
