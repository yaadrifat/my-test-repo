/**
 * Created On Mar 27, 2012
 */
package com.velos.services.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Kanwaldeep
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="StudyPatientScheduleFormResponse")
public class StudyPatientScheduleFormResponse extends StudyPatientFormResponse {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8538004107019456924L;
	private ScheduleEventIdentifier scheduleEventIdentifier;
	private Visit visit;
	private Event event;

	public ScheduleEventIdentifier getScheduleEventIdentifier() {
		return scheduleEventIdentifier;
	}

	public void setScheduleEventIdentifier(
			ScheduleEventIdentifier scheduleEventIdentifier) {
		this.scheduleEventIdentifier = scheduleEventIdentifier;
	}

	public Visit getVisit() {
		return visit;
	}

	public void setVisit(Visit visit) {
		this.visit = visit;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	} 
	

}
