package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="BgtTemplate")
@XmlAccessorType (XmlAccessType.FIELD)
public class BgtTemplate implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BudgetPojo budgetInfo;
	private BudgetIdentifier budgetIdentifier;
	
	public BudgetPojo getBudgetInfo() {
		return budgetInfo;
	}
	public void setBudgetInfo(BudgetPojo budgetInfo) {
		this.budgetInfo = budgetInfo;
	}
	public BudgetIdentifier getBudgetIdentifier() {
		return budgetIdentifier;
	}
	public void setBudgetIdentifier(BudgetIdentifier budgetIdentifier) {
		this.budgetIdentifier = budgetIdentifier;
	}
}
