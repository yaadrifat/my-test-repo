package com.velos.services.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="CalendarName")
public class CalendarNameIdentifier extends CalendarIdentifier{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected String calendarName;
	protected String calendarAssocTo;
	
	public String getCalendarName() {
		return calendarName;
	}
	public void setCalendarName(String calendarName) {
		this.calendarName = calendarName;
	}
	public String getCalendarAssocTo() {
		return calendarAssocTo;
	}
	public void setCalendarAssocTo(String calendarAssocTo) {
		this.calendarAssocTo = calendarAssocTo;
	}
	

}
