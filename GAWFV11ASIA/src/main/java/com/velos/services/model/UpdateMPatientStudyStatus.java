package com.velos.services.model;

import java.io.Serializable;

public class UpdateMPatientStudyStatus implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6619065994132618114L;
	protected PatientStudyStatusIdentifier patientStudyStatusIdentifier;
	protected PatientEnrollmentDetails patientEnrollmentDetails;
	public PatientEnrollmentDetails getPatientEnrollmentDetails() {
		return patientEnrollmentDetails;
	}
	public void setPatientEnrollmentDetails(
			PatientEnrollmentDetails patientEnrollmentDetails) {
		this.patientEnrollmentDetails = patientEnrollmentDetails;
	}
	public PatientStudyStatusIdentifier getPatientStudyStatusIdentifier() {
		return patientStudyStatusIdentifier;
	}
	public void setPatientStudyStatusIdentifier(
			PatientStudyStatusIdentifier patientStudyStatusIdentifier) {
		this.patientStudyStatusIdentifier = patientStudyStatusIdentifier;
	}
}
