/**
 * Created On Nov 9, 2011
 */
package com.velos.services.model;

/**
 * @author Kanwaldeep
 *
 */
public class AccountFormResponse extends FormResponse {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8029597666928291080L;
	
	protected AccountIdentifier accountIdentifier;
	
	protected AccountFormResponseIdentifier formFilledFormId;

	public AccountIdentifier getAccountIdentifier() {
		return accountIdentifier;
	}

	public void setAccountIdentifier(AccountIdentifier accountIdentifier) {
		this.accountIdentifier = accountIdentifier;
	}

	public AccountFormResponseIdentifier getFormFilledFormId() {
		return formFilledFormId;
	}

	public void setFormFilledFormId(AccountFormResponseIdentifier formFilledFormId) {
		this.formFilledFormId = formFilledFormId;
	} 	
	
	
	

}
