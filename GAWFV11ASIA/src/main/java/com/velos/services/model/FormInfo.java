/**
 * Created On Dec 2, 2011
 */
package com.velos.services.model;

import java.io.Serializable;

/**
 * @author Kanwaldeep
 *
 */
public class FormInfo implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8894529314531832875L;
	protected FormIdentifier formIdentifier; 
	protected String formName;
	protected String formDesc;
	protected Code formStatus;
	protected Integer dataCount;
	
	
	/**
	 * @return the dataCount
	 */
	public Integer getDataCount() {
		return dataCount;
	}
	/**
	 * @param dataCount the dataCount to set
	 */
	public void setDataCount(Integer dataCount) {
		this.dataCount = dataCount;
	}
	/**
	 * @return the formDesc
	 */
	public String getFormDesc() {
		return formDesc;
	}
	/**
	 * @param formDesc the formDesc to set
	 */
	public void setFormDesc(String formDesc) {
		this.formDesc = formDesc;
	}
	/**
	 * @return the formStatus
	 */
	public Code getFormStatus() {
		return formStatus;
	}
	/**
	 * @param formStatus the formStatus to set
	 */
	public void setFormStatus(Code formStatus) {
		this.formStatus = formStatus;
	}
	public FormIdentifier getFormIdentifier() {
		return formIdentifier;
	}
	public void setFormIdentifier(FormIdentifier formIdentifier) {
		this.formIdentifier = formIdentifier;
	}
	public String getFormName() {
		return formName;
	}
	public void setFormName(String formName) {
		this.formName = formName;
	}

}
