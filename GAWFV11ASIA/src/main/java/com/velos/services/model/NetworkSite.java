package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="NetworkSite")
@XmlAccessorType(XmlAccessType.FIELD)
public class NetworkSite implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8622738246315448208L;
	private String networkPK;
	private String networkName;
	private String siteName;
	private Code relationshipType;
	private String CTEP_ID;
	private String relationshipPK;
	private String sitePK;
	
	
	
	
	public String getNetworkName() {
		if(networkName!=null  && !networkName.equals("") && networkName.contains("'")){
			try{
				char c =networkName.charAt(networkName.indexOf("'")+1);
				if(c!='\''){
					networkName=networkName.replaceAll("'", "''");
				}
				}catch(IndexOutOfBoundsException e){
					//do nothing
				}
		}
		return networkName;
	}
	public void setNetworkName(String NetworkName) {
		networkName = NetworkName;
	}
	
	public Code getRelationshipType() {
		return relationshipType;
	}
	public void setRelationshipType(Code RelationshipType) {
		relationshipType = RelationshipType;
	}
	
	public String getSiteName() {
		if(siteName!=null  && !siteName.equals("") && siteName.contains("'")){
			try{
				char c =siteName.charAt(siteName.indexOf("'")+1);
				if(c!='\''){
					siteName=siteName.replaceAll("'", "''");
				}
				}catch(IndexOutOfBoundsException e){
					//do nothing
				}
		}
		return siteName;
	}
	public void setSiteName(String SiteName) {
		siteName = SiteName;
	}
	public String getNetworkPK() {
		return networkPK;
	}
	public void setNetworkPK(String NetworkPK) {
		networkPK = NetworkPK;
	}
	public String getCTEP_ID() {
		return CTEP_ID;
	}
	public void setCTEP_ID(String cTEP_ID) {
		CTEP_ID = cTEP_ID;
	}
	public String getRelationshipPK() {
		return relationshipPK;
	}
	public void setRelationshipPK(String relationShipPK) {
		this.relationshipPK = relationShipPK;
	}
	public String getSitePK() {
		return sitePK;
	}
	public void setSitePK(String SitePK) {
		sitePK = SitePK;
	}
	
	

}
