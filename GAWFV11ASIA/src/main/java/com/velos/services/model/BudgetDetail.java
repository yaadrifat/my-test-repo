package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name="Budget")
@XmlAccessorType (XmlAccessType.FIELD)
public class BudgetDetail implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BgtTemplate bgtTemplate;
	private BudgetPojo budgetInfo;
	private StudyIdentifier studyIdentifier;
	private CalendarNameIdentifier calendarIdentifier;
	private BudgetIdentifier budgetIdentifier;
	
	public BgtTemplate getBgtTemplate() {
		return bgtTemplate;
	}
	public void setBgtTemplate(BgtTemplate bgtTemplate) {
		this.bgtTemplate = bgtTemplate;
	}
	public BudgetPojo getBudgetInfo() {
		return budgetInfo;
	}
	public void setBudgetInfo(BudgetPojo budgetInfo) {
		this.budgetInfo = budgetInfo;
	}
	public StudyIdentifier getStudyIdentifier() {
		return studyIdentifier;
	}
	public void setStudyIdentifier(StudyIdentifier studyIdentifier) {
		this.studyIdentifier = studyIdentifier;
	}
	public CalendarNameIdentifier getCalendarIdentifier() {
		return calendarIdentifier;
	}
	public void setCalendarIdentifier(CalendarNameIdentifier calendarIdentifier) {
		this.calendarIdentifier = calendarIdentifier;
	}
	public BudgetIdentifier getBudgetIdentifier() {
		return budgetIdentifier;
	}
	public void setBudgetIdentifier(BudgetIdentifier budgetIdentifier) {
		this.budgetIdentifier = budgetIdentifier;
	}

}
