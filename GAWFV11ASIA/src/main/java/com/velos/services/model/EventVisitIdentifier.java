package com.velos.services.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Tarandeep Singh Bali
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="EventIdentifier")

public class EventVisitIdentifier extends EventNameIdentifier {
	

/**
	 * 
	 */
	private static final long serialVersionUID = -7066059236383347844L;

	
protected String eventVisitOID;
   
   public EventVisitIdentifier(){
		
	}
	   
   public String geteventVisitOID(){
	   
	   return eventVisitOID;
   }
   
   public void seteventVisitOID(String eventVisitOID){
	   
	   this.eventVisitOID=eventVisitOID;
   }
	
}