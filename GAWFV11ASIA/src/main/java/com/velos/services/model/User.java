package com.velos.services.model;

//import java.util.Date;
//
//import javax.validation.Valid;
//import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
//import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="User")
public class User 
    extends NonSystemUser
 
{


	/**
	 * 
	 */
	private static final long serialVersionUID = 7937258183441323999L;

	public enum UserStatus{
		ACTIVE("A"),
		BLOCKED("B"),
		DEACTIVATED("D");
		
		private String legacyString;
		
		UserStatus(String legacyString){
			this.legacyString = legacyString;
		}
		
		public String getLegacyString(){
			return legacyString;
		}
		
		public void setLegacyString(String legacyString) {
			this.legacyString = legacyString;
		}
	}

//	public enum Type{
//		SYSTEM,
//		NON_SYSTEM
//	}
//	
//	public enum SiteAccess{
//		ALL_SITES,
//		ONLY_CHILD_SITES
//	}
//	
//	private UserIdentifier userIdentifier;
//	
//    /**
//     * the user codelst jobtype
//     */
//    private Code userCodelstJobtype;
//
//
//
//    /**
//     * the user permanent AddressId
//     */
//    private ContactInfo userContactInfo;
//
//    /**
//     * the user lastName
//     */
//   private String userLastName;
//
//    /**
//     * the user firstName
//     */
//
//	  private String userFirstName;
//
//    /**
//     * the user mid Name
//     */
//
//    private String userMiddleName;
//
//    /**
//     * the user work experience
//     */
//
   private String userWorkExperience;
//
//    /**
//     * the user phase ivaluation
//     */
//
      private String userTrialPhaseInvolvement;
//
//    /**
//     * the user session Time out
//     */
//
//    //public Integer userSessionTime;
//
//    //moved to UserIdentifier member
////    /**
////     * the user login Name
////     */
////
     private String userLoginName;
//
//    /**
//     * the user password
//     */
//
     private String userPassword;
//
//    /**
//     * the user Secret Question
//     */
//
    private String userSecurityQuestion;

//    /**
//     * the user Answer
//     */
//
   private String userSecurityAnswer;
//
//    /**
//     * the user status
//     */
//
//    private UserStatus userStatus;
//
//    /**
//     * the user codelist primary Speciality
//     */
//
//    private Code userPrimarySpecialty;
//
//    /**
//     * the user Default group
//     */
   private GroupIdentifier userdefaultgroup;
   
  /* private NVPair moreUserDetails;*/
   
   
  /* private Groups userdefaultgroup; 
   
   
	public Groups getUserdefaultgroup() {
	return userdefaultgroup;
}
public void setUserdefaultgroup(Groups userdefaultgroup) {
	this.userdefaultgroup = userdefaultgroup;
}*/



	public GroupIdentifier getUserDefaultGroup() {
	return this.userdefaultgroup;
}
public void setUserDefaultGroup(GroupIdentifier userdefaultgroup) {
	this.userdefaultgroup = userdefaultgroup;
}

/*public NVPair getMoreUserdetail() {
	return moreUserDetails;
	}
	public void setMoreUserdetail(NVPair moreUserDetails) {
	this.moreUserDetails = moreUserDetails;
	}*/

	//
//    /**
//     * Time Zone
//     */
     private Code timeZoneId;
//
//    /*
//     * the record creator
//     */
//    private String creatorId;
//
//    /*
//     * last modified by
//     */
//    private String modifiedById;
//
//    /*
//     * the IP Address
//     */
//    private String ipAdd;
//
//    /*
//     * Pwd Expiry Date
//     */
//    private Date userPasswordExpiryDate = null;
//
//    /*
//     * Pwd Expiry Days
//     */
//    private Integer userPasswordExpiryDays;
//
//    /*
//     * Pwd Reminder to be sent on
//     */
//    private Date userPasswordReminderDate = null;
//
//    /*
//     * E-signature
//     */
     private String userESign;
//
//    /*
//     * E-signature Expiry Date
//     */
//    private Date userESignExpiryDate;
//
//    /*
//     * Pwd Expiry Days
//     */
//    private Integer userESignExpiryDays;
//
//    /*
//     * Pwd Reminder to be sent on
//     */
//    private Date userESignReminderDate;
//
//    /*
//     * User Login attempt count
//     */
//    private Integer userLoginAttemptCount;
//
//    /*
//     * User Login attempt count
//     */
//    private boolean userLoggedIn;
//
//    /*
//     * User Site Flag, A-access to all child organizations,S-access to specified
//     * organizations
//     * 
//     */
//    private String userSiteFlag;
//
//    /*
//     * User Type, N - Non system user,S - System User
//     * 
//     */
//    private Type userType;
//    
//    private Integer userLoginModule;
//    private String  userLoginModuleMap;
//
//    private Integer userSkin;
//    
//    private Integer userTheme;
//    
//    private Integer userHidden;//KM
//
//	public Code getUserCodelstJobtype() {
//		return userCodelstJobtype;
//	}
//
//	public void setUserCodelstJobtype(Code userCodelstJobtype) {
//		this.userCodelstJobtype = userCodelstJobtype;
//	}
//
//
//	public void setUserContactInfo(ContactInfo userContactInfo) {
//		this.userContactInfo = userContactInfo;
//	}
//
//	public String getUserLastName() {
//		return userLastName;
//	}
//
//	public void setUserLastName(String userLastName) {
//		this.userLastName = userLastName;
//	}
//
//	public String getUserFirstName() {
//		return userFirstName;
//	}
//
//	public void setUserFirstName(String userFirstName) {
//		this.userFirstName = userFirstName;
//	}
//
//	public String getUserMiddleName() {
//		return userMiddleName;
//	}
//
//	public void setUserMiddleName(String userMiddleName) {
//		this.userMiddleName = userMiddleName;
//	}
//
    protected Integer PK;
    
    protected Boolean hideUserInLookup;
    
    
    public Integer getPK() {
		return PK;
	}
	public void setPK(Integer pK) {
		PK = pK;
	}

	private Boolean sendNotifaction; 
     
	public Boolean isSendNotifaction() {
		return sendNotifaction;
	}
	public void setSendNotifaction(Boolean sendNotifaction) {
		this.sendNotifaction = sendNotifaction;
	}
	
	public Boolean getHideUserInLookup() {
		return hideUserInLookup;
	}
	public void setHideUserInLookup(Boolean hideUserInLookup) {
		this.hideUserInLookup = hideUserInLookup;
	}
	/*public boolean sendNotifaction() {
		return sendNotifaction;
	}
	public void setEmailUser(boolean sendNotifaction) {
		this.sendNotifaction = sendNotifaction;
	}*/
	public String getUserWorkExperience() {
		return userWorkExperience;
	}
	public void setUserWorkExperience(String userWorkExperience) {
		this.userWorkExperience = userWorkExperience;
	}

	public String getUserTrialPhaseInvolvement() {
		return userTrialPhaseInvolvement;
	}
	public void setUserTrialPhaseInvolvement(String userTrialPhaseInvolvement) {
		this.userTrialPhaseInvolvement = userTrialPhaseInvolvement;
	}

	 public String getUserLoginName() {
		return userLoginName;
	}

	public void setUserLoginName(String userLoginName) {
		this.userLoginName = userLoginName;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	public String getUserSecurityQuestion() {
		return userSecurityQuestion;
	}

	public void setUserSecurityQuestion(String userSecurityQuestion) {
		this.userSecurityQuestion = userSecurityQuestion;
     }

	public String getUserSecurityAnswer() {
		return userSecurityAnswer;
	}

	public void setUserSecurityAnswer(String userSecurityAnswer) {
		this.userSecurityAnswer = userSecurityAnswer;
	}
//
//	public UserStatus getUserStatus() {
//		return userStatus;
//	}
//
//	public void setUserStatus(UserStatus userStatus) {
//		this.userStatus = userStatus;
//	}
//
//	public Code getUserPrimarySpecialty() {
//		return userPrimarySpecialty;
//	}
//
//	public void setUserPrimarySpecialty(Code userPrimarySpecialty) {
//		this.userPrimarySpecialty = userPrimarySpecialty;
//	}
//
/*	public UserGroup getUserDefaultGroup() {
		return userDefaultGroup;
	}
	public void setUserDefaultGroup(UserGroup userDefaultGroup) {
		this.userDefaultGroup = userDefaultGroup;
    }
*/
	public Code getTimeZoneId() {
		return timeZoneId;
	}

	public void setTimeZoneId(Code timeZoneId) {
	this.timeZoneId = timeZoneId;
	}
//
//	public String getCreatorId() {
//		return creatorId;
//	}
//
//	public void setCreatorId(String creatorId) {
//		this.creatorId = creatorId;
//	}
//
//	public String getModifiedById() {
//		return modifiedById;
//	}
//
//	public void setModifiedById(String modifiedById) {
//		this.modifiedById = modifiedById;
//	}
//
//	public String getIpAdd() {
//		return ipAdd;
//	}
//
//	public void setIpAdd(String ipAdd) {
//		this.ipAdd = ipAdd;
//	}
//
//	public Date getUserPasswordExpiryDate() {
//		return userPasswordExpiryDate;
//	}
//
//	public void setUserPasswordExpiryDate(Date userPasswordExpiryDate) {
//		this.userPasswordExpiryDate = userPasswordExpiryDate;
//	}
//
//	public Integer getUserPasswordExpiryDays() {
//		return userPasswordExpiryDays;
//	}
//
//	public void setUserPasswordExpiryDays(Integer userPasswordExpiryDays) {
//		this.userPasswordExpiryDays = userPasswordExpiryDays;
//	}
//
//	public Date getUserPasswordReminderDate() {
//		return userPasswordReminderDate;
//	}
//
//	public void setUserPasswordReminderDate(Date userPasswordReminderDate) {
//		this.userPasswordReminderDate = userPasswordReminderDate;
//	}
//
	public String getUserESign() {
		return userESign;
	}

	public void setUserESign(String userESign) {
		this.userESign = userESign;
	}
//
//	public Date getUserESignExpiryDate() {
//		return userESignExpiryDate;
//	}
//
//	public void setUserESignExpiryDate(Date userESignExpiryDate) {
//		this.userESignExpiryDate = userESignExpiryDate;
//	}
//
//	public Integer getUserESignExpiryDays() {
//		return userESignExpiryDays;
//	}
//
//	public void setUserESignExpiryDays(Integer userESignExpiryDays) {
//		this.userESignExpiryDays = userESignExpiryDays;
//	}
//
//	public Date getUserESignReminderDate() {
//		return userESignReminderDate;
//	}
//
//	public void setUserESignReminderDate(Date userESignReminderDate) {
//		this.userESignReminderDate = userESignReminderDate;
//	}
//
//	public Integer getUserLoginAttemptCount() {
//		return userLoginAttemptCount;
//	}
//
//	public void setUserLoginAttemptCount(Integer userLoginAttemptCount) {
//		this.userLoginAttemptCount = userLoginAttemptCount;
//	}
//
//	public boolean isUserLoggedIn() {
//		return userLoggedIn;
//	}
//
//	public void setUserLoggedIn(boolean userLoggedIn) {
//		this.userLoggedIn = userLoggedIn;
//	}
//
//	public String getUserSiteFlag() {
//		return userSiteFlag;
//	}
//
//	public void setUserSiteFlag(String userSiteFlag) {
//		this.userSiteFlag = userSiteFlag;
//	}
//
//	public Type getUserType() {
//		return userType;
//	}
//
//	public void setUserType(Type userType) {
//		this.userType = userType;
//	}
//
//	public Integer getUserLoginModule() {
//		return userLoginModule;
//	}
//
//	public void setUserLoginModule(Integer userLoginModule) {
//		this.userLoginModule = userLoginModule;
//	}
//
//	public String getUserLoginModuleMap() {
//		return userLoginModuleMap;
//	}
//
//	public void setUserLoginModuleMap(String userLoginModuleMap) {
//		this.userLoginModuleMap = userLoginModuleMap;
//	}
//
//	public Integer getUserSkin() {
//		return userSkin;
//	}
//
//	public void setUserSkin(Integer userSkin) {
//		this.userSkin = userSkin;
//	}
//
//	public Integer getUserTheme() {
//		return userTheme;
//	}
//
//	public void setUserTheme(Integer userTheme) {
//		this.userTheme = userTheme;
//	}
//
//	public Integer getUserHidden() {
//		return userHidden;
//	}
//
//	public void setUserHidden(Integer userHidden) {
//		this.userHidden = userHidden;
//	}
//	
//	@NotNull
//	@Valid
//	public UserIdentifier getUserIdentifier() {
//		return userIdentifier;
//	}
//
//	public void setUserIdentifier(UserIdentifier userIdentifier) {
//		this.userIdentifier = userIdentifier;
//	}
//
//	public ContactInfo getUserContactInfo() {
//		return userContactInfo;
//	}

}
