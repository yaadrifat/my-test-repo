package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="StudyFormResponses")
public class StudyFormResponses implements Serializable {
	private static final long serialVersionUID = 5304422613761694409L;
	private List<StudyFormResponse> studyFormResponses = new ArrayList<StudyFormResponse>();
	private Integer recordCount = 0;
	
	public Integer getRecordCount() {
		return recordCount;
	}
	public void setRecordCount(Integer recordCount) {
		this.recordCount = recordCount;
	}
	public List<StudyFormResponse> getStudyFormResponses() {
		return studyFormResponses;
	}
	public void setStudyFormResponses(List<StudyFormResponse> studyFormResponses) {
		this.studyFormResponses = studyFormResponses;
	}
	public void addStudyFormResponse(StudyFormResponse formResponse) {
		this.studyFormResponses.add(formResponse); 
	}
}
