package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Site")
public class Sites implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5647717652994226380L;
	private List<Site> site=new ArrayList<Site>();
	
	public List<Site> getSites() {
		return site;
	}
	public void setSites(List<Site> site) {
		this.site = site;
	}
}
