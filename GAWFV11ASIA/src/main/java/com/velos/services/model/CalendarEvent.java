/**
 * Created On May 6, 2011
 */
package com.velos.services.model;

import javax.validation.Valid;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Kanwaldeep
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Event")
public class CalendarEvent extends ServiceObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7525742706833622294L;

	protected Costs costs;
	
	protected CalendarEventSummary calendarEventSummary; 
	
	
	/**
	 * 
	 */
	public CalendarEvent() {
		// TODO Auto-generated constructor stub
	}
	
	

	public Costs getCosts() {
		return costs;
	}


	public void setCosts(Costs costs) {
		this.costs = costs;
	}
	

	public void setParentIdentifier(ParentIdentifier parentIdentifier)
	{
		this.parentIdentifier = parentIdentifier; 
	}
	
	public ParentIdentifier getParentIdentifier()
	{
		return parentIdentifier; 
	}
	
	
	@Valid
	public CalendarEventSummary getCalendarEventSummary() {
		return calendarEventSummary;
	}



	public void setCalendarEventSummary(CalendarEventSummary calendarEventSummary) {
		this.calendarEventSummary = calendarEventSummary;
	}


	




}
