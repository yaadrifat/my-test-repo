package com.velos.services.model;

import java.io.Serializable;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**model class for patient schedule. depicts
 *  a schedule a patient has, on a study
 * @author Tarandeep Singh Bali
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="PatientSchedule")
public class PatientSchedule implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	public PatientSchedule(){}
	
	protected PatientScheduleSummary patientScheduleSummary;
	protected PatientIdentifier patientIdentifier;
	protected StudyIdentifier studyIdentifier;
	protected Visits visits;
	
	protected Date startDate;
	protected Date endDate;

	
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public PatientScheduleSummary getPatientScheduleSummary() {
		return patientScheduleSummary;
	}
	public void setPatientScheduleSummary(
			PatientScheduleSummary patientScheduleSummary) {
		this.patientScheduleSummary = patientScheduleSummary;
	}
	public PatientIdentifier getPatientIdentifier() {
		return patientIdentifier;
	}
	public void setPatientIdentifier(PatientIdentifier patientIdentifier) {
		this.patientIdentifier = patientIdentifier;
	}

	public StudyIdentifier getStudyIdentifier() {
		return studyIdentifier;
	}
	public void setStudyIdentifier(StudyIdentifier studyIdentifier) {
		this.studyIdentifier = studyIdentifier;
	}
	public Visits getVisits() {
		return visits;
	}
	public void setVisits(Visits visits) {
		this.visits = visits;
	}	

}