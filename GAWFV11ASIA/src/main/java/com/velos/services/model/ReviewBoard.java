package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="reviewBoard")
public class ReviewBoard implements Serializable{

	protected String name;
	protected SubmissionConditions submissionConditions;
	protected String lastSubmittedOn;
	protected int submissionReady;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public SubmissionConditions getSubmissionConditions() {
		return submissionConditions;
	}
	public void setSubmissionConditions(SubmissionConditions submissionConditions) {
		this.submissionConditions = submissionConditions;
	}
	public String getLastSubmittedOn() {
		return lastSubmittedOn;
	}
	public void setLastSubmittedOn(String lastSubmittedOn) {
		this.lastSubmittedOn = lastSubmittedOn;
	}
	public int getSubmissionReady() {
		return submissionReady;
	}
	public void setSubmissionReady(int submissionReady) {
		this.submissionReady = submissionReady;
	}
}
