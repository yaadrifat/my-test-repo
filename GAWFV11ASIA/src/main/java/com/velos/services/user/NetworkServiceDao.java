package com.velos.services.user;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import com.velos.eres.business.common.UserDao;
import com.velos.eres.service.userAgent.UserAgentRObj;
import org.apache.xerces.dom.ElementNSImpl;
import org.json.JSONObject;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.business.common.MoreDetailsDao;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.util.BrowserRows;
import com.velos.eres.service.util.Configuration;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.FilterUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.moreDetails.MoreDetailsJB;
import com.velos.services.CRUDAction;
import com.velos.services.CodeNotFoundException;
import com.velos.services.CompletedAction;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.map.MultipleObjectsFoundException;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.NetworkSiteDetails;
import com.velos.services.model.NetworkSiteLevelDetails;
import com.velos.services.model.NetworkSiteSearchResults;
import com.velos.services.model.NetworkSiteUserDetails;
import com.velos.services.model.Networks;
import com.velos.services.model.NetworksDetails;
import com.velos.services.model.Site;
import com.velos.services.model.SiteIdentifier;
import com.velos.services.model.SiteLevelDetails;
import com.velos.services.model.Sites;
import com.velos.services.model.UserIdentifier;
import com.velos.services.model.UserNetworkSiteResults;
import com.velos.services.model.UserNetworkSites;
import com.velos.services.model.UserNetworks;
import com.velos.services.model.UserNetworksDetail;
import com.velos.services.model.UserRoleIdentifier;
import com.velos.services.model.UserSite;
import com.velos.services.model.UserSites;
import com.velos.services.model.Code;
import com.velos.services.model.MoreNetworkUserDetails;
import com.velos.services.model.NVPair;
import com.velos.services.model.Network;
import com.velos.services.model.NetworkIdentifier;
import com.velos.services.model.NetworkLevelSite;
import com.velos.services.util.CodeCache;
import java.sql.Connection;
import com.velos.services.model.NetworkRole;
import com.velos.services.model.NetworkSite;
import com.velos.services.model.NetworkSites;
import com.velos.services.model.NetworkSitesUsers;
import com.velos.services.model.NetworkUserDetail;
import com.velos.services.model.NetworkUserDetails;
import com.velos.services.model.Roles;

public class NetworkServiceDao extends CommonDAO{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6495019937807678233L;
	
	public static final String KEY_ACCOUNT_ID = "accountId";
	
	public static final String KEY_PAGE_SIZE = "pageSize";
	public static final String KEY_ORDER_BY = "orderBy";
	public static final String KEY_ORDER_TYPE = "orderType";
	
	/*For Networks*/
	private static final String ASC_STR = "asc";
	private static final String EMPTY_STR = "";
	private static final String NULL_STR = "null";
	
	
	/*For Networks*/
	private ArrayList<String> NetWorksNameList = new ArrayList<String>();
	private ArrayList<String> NetWorksStatusList = new ArrayList<String>();
	private ArrayList<String> NetWorksRelationshipTypeList = new ArrayList<String>();
	private ArrayList<String> NetWorksCTEPIDList = new ArrayList<String>();
	private ArrayList<String> NetWorksPKList = new ArrayList<String>();
	private ArrayList<String> NetWorksTypeList = new ArrayList<String>();
	private ArrayList<String> NetWorksSubtypeList = new ArrayList<String>();
	private ArrayList<String> NetWorksRelationshipTypeTypeList = new ArrayList<String>();
	private ArrayList<String> NetWorksRelationshipTypeSubtypeList = new ArrayList<String>();
	
	/*For Networks*/
	private static final String COL_NET_NAME = "Network_Name";
	private static final String COL_STATUS_NAME = "Network_Status";
	private static final String COL_RELATIONSHITYPE_NAME = "NETWORK_RELATION";
	private static final String COL_CTEPID_NAME = "CTEP_ID";
	private static final String COL_NTWPK_NAME = "pk_nwsites";
	private static final String COL_STATUS_Type= "Network_Type";
	private static final String COL_STATUS_Subtype= "Network_Subtyp";
	private static final String NETWORK_RELATIONSHITYPE_TYPE_TYPE = "NETWORK_RELATION_TYPE";
	private static final String NETWORK_RELATIONSHITYPE_TYPE_SUBTYPE = "NETWORK_RELATION_SUBTYPE";
	
	
	/*For Networks*/
	public ArrayList<String> getNetWorksNameList() { return NetWorksNameList; }
	public ArrayList<String> getNetWorksStatusList() { return NetWorksStatusList; }
	public ArrayList<String> getNetWorksRelationshipTypeList() { return NetWorksRelationshipTypeList; }
	public ArrayList<String> getNetWorksCTEPIDList() { return NetWorksCTEPIDList; }
	public ArrayList<String> getNetWorksPKList() { return NetWorksPKList; }
	public ArrayList<String> getNetWorksTypeList() { return NetWorksTypeList; }
	public ArrayList<String> getNetWorksSubtypeList() { return NetWorksSubtypeList; }
	public ArrayList<String> getNetWorksRelationshipTypeTypeList() { return NetWorksRelationshipTypeTypeList; }
	public ArrayList<String> getNetWorksRelationshipTypeSubtypeList() { return NetWorksRelationshipTypeSubtypeList; }
	
	
	private Long totalCount = 0L;
	private Long pageSize = 0L;
	private ResponseHolder response=null;

	private List<String> pkNetList=new ArrayList<String>();
	private List<String> fkSiteList=new ArrayList<String>();
	private List<String> pkRelnShipType=new ArrayList<String>();
	private List<String> netNameList=new ArrayList<String>();
	private List<String> netInfoList=new ArrayList<String>();
	private List<String> ctepList = new ArrayList<String>();
	//private List<String> netStatList=new ArrayList<String>();
	private List<Code> netStatList=new ArrayList<Code>();
	private List<Code> siteRelnShipType=new ArrayList<Code>();
	private List<String> siteNameList=new ArrayList<String>();
	private List<String> siteInfoList=new ArrayList<String>();
	private List<String> netLevelList=new ArrayList<String>();
	private List<String> netUserList=new ArrayList<String>();
	private List<ArrayList<Site>> sitesList=new ArrayList<ArrayList<Site>>();
	private List<ArrayList<UserSite>> sitesLists=new ArrayList<ArrayList<UserSite>>();
	private List<ArrayList<NetworkSites>> UsersitesList=new ArrayList<ArrayList<NetworkSites>>();
	private List<ArrayList<NetworkSitesUsers>> NetworksitesusersList=new ArrayList<ArrayList<NetworkSitesUsers>>();
	private List<String> fkuserList=new ArrayList<String>();
	private List<String> pkNwUserList=new ArrayList<String>();
	private Map<String,String> relationShipPkMap = new HashMap<String,String>();

	private Integer pageNumber = 0;
	
	private String roleName = "";
	private String roleType = "";
	private String roleCode = "";
	private String roleDescription = "";
	
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getRoleType() {
		return roleType;
	}
	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}
	public String getRoleCode() {
		return roleCode;
	}
	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}
	public String getRoleDescription() {
		return roleDescription;
	}
	public void setRoleDescription(String roleDescription) {
		this.roleDescription = roleDescription;
	}

	public List<String> getPkNetList() {
		return pkNetList;
	}

	public List<String> getFkSiteList() {
		return fkSiteList;
	}

	public List<String> getPkRelnShipType() {
		return pkRelnShipType;
	}
	
	public List<String> getFkUserList() {
		return fkuserList;
	}
	public List<String> getNetNameList() {
		return netNameList;
	}
	public List<String> getNetInfoList() {
		return netInfoList;
	}
	
	public List<Code> getNetStatList() {
		return netStatList;
	}

	public List<Code> getSiteRelnShipType() {
		return siteRelnShipType;
	}
	/*public List<String> getNetStatList() {
		return netStatList;
	}*/
	
	public List<String> getSiteNameList() {
		return siteNameList;
	}
	
	public List<String> getSiteInfoList() {
		return siteInfoList;
	}
	
	public List<String> getNetLevelList() {
		return netLevelList;
	}
	public List<ArrayList<Site>> getSitesList() {
		return sitesList;
	}
	public List<String> getnetUserList() {
		return netUserList;
	}
	
	public Integer getPageNumber() {
		return pageNumber; 
		}
	public Long getTotalCount() {
		return totalCount;
	}
	public Long getPageSize() {
		return pageSize;
	}
public List<ArrayList<NetworkSites>> getSiteList() {
		return UsersitesList;
	}  

	public List<ArrayList<NetworkSitesUsers>> getSiteUsersList() {
		return NetworksitesusersList;
	} 


	public ResponseHolder getResponse() {
		return response;
	}
	
	public List<String> getpkNwUserList() {
		return pkNwUserList;
	}
	public void searchNetworkSites(NetworkSiteDetails networkSiteDetails, HashMap<String, Object> params) throws OperationException {
		Connection conn=null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int count=0;
		String iaccId = (String)params.get(KEY_ACCOUNT_ID);
		UserBean userBean=(UserBean)params.get("callingUser");
		String networkName = (networkSiteDetails.getNetwork_Name()==null)?"":networkSiteDetails.getNetwork_Name();
		String siteName = (networkSiteDetails.getSite_Name()==null)?"":networkSiteDetails.getSite_Name();
		String sitePk= (networkSiteDetails.getSite_PK()==null)?"":networkSiteDetails.getSite_PK();
		String networkPk = (networkSiteDetails.getNetwork_PK()==null)?"":networkSiteDetails.getNetwork_PK();
		String CTEP_ID = (networkSiteDetails.getCTEP_ID()==null)?"":networkSiteDetails.getCTEP_ID();
		String pkCode="";
		
		if(networkSiteDetails.getRelationship_Type() != null && networkSiteDetails.getRelationship_Type().getCode() != null){
			CodeDao cd = new CodeDao();
			pkCode = StringUtil.integerToString(cd.getCodeId(networkSiteDetails.getRelationship_Type().getType(), networkSiteDetails.getRelationship_Type().getCode()));
		}
		
		String str = "";
		String pk_main_network = "";
		String net_pks="";
		List<NVPair> moredetails=networkSiteDetails.getMoreSiteDetails();
		//Collection<NVPair> moreSiteDetails =networkSiteDetails.getMoreSiteDetails();
		//HashMap<String,Object> keySearch=new HashMap<String,Object>();
		
		if(moredetails!=null && moredetails.size()>0){
			net_pks=getNetPkMoreDetails(moredetails,"siteDetails");
			/*networkSiteDetails.setMoreSiteDetails((java.util.List<NVPair>) moreSiteDetails);
			if(moreSiteDetails!=null){
				NVPair getDetail=new NVPair();
				for(int iY=0;iY<networkSiteDetails.getMoreSiteDetails().size();iY++){
					getDetail = networkSiteDetails.getMoreSiteDetails().set(iY, getDetail);
					keySearch.put("key",getDetail.getKey()==null?"":getDetail.getKey());
					keySearch.put("value", getDetail.getValue()==null?"":getDetail.getValue());
				}
				moreSiteDetails.addAll(moreSiteDetails);	
			}*/
		}
		
		
		if(!(siteName.equals("")) || (networkSiteDetails.getRelationship_Type() != null && networkSiteDetails.getRelationship_Type().getCode() != null ) || (moredetails!=null && moredetails.size()>0)){
			str="SELECT distinct nvl(FK_NWSITES_MAIN,pk_nwsites) fk_nwsites_main FROM er_nwsites,er_site WHERE pk_site=fk_site and er_site.fk_account=? ";
			if(!siteName.equals("")){	
				str=str+" and lower(site_name) like lower('%"+siteName+"%') ";
			}
			if(!CTEP_ID.equals("")){
				str=str+" and lower(ctep_id) like lower('%"+CTEP_ID+"%') ";
			}
			if(net_pks.length()>0){
				
					str=str+" and pk_nwsites in ("+net_pks+") ";
				
			}
			if(!(pkCode.equals("0")) && !(pkCode.equals(""))){
				str=str+" and nw_membertype="+pkCode;
				}
			pk_main_network = pkMainNetwork(str,iaccId);
			System.out.println("pk_main_network==="+pk_main_network);
			System.out.println("str==="+str);
		}
		System.out.println("networkPk==="+networkPk);
		System.out.println("networkName==="+networkName);
		
		if(networkPk.equals("") && networkName.equals("") && networkSiteDetails.getRelationship_PK()!=null && !networkSiteDetails.getRelationship_PK().equals("")){
			String getPrimaryNetwork="select fk_nwsites_main FROM er_nwsites WHERE pk_nwsites="+ EJBUtil.stringToNum(networkSiteDetails.getRelationship_PK());
			try {
				conn = CommonDAO.getConnection();
				pstmt = conn.prepareStatement(getPrimaryNetwork);
				rs=pstmt.executeQuery();
				while(rs.next()){
					pk_main_network=String.valueOf(rs.getInt("fk_nwsites_main"));
				}
				System.out.println("new condition pk_main_network==="+pk_main_network);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		if(!networkPk.equals("") && (!pk_main_network.equals("0") || pk_main_network.equals("")) && (pk_main_network.indexOf(networkPk)>-1 || pk_main_network.equals(""))){
			pk_main_network=networkPk;
		}
		
		System.out.println("final pk_main_network==="+pk_main_network);
		
 	 	String str1 = "select pk_nwsites,fk_site,site_name ,site_name as network_name,ctep_id, er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden,(SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType,(SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,NW_LEVEL+1 as nw_level from er_nwsites,er_site,er_codelst "+
 	 				  " where pk_site=fk_site and pk_codelst=nw_status and er_site.fk_account=? and nw_level=0 ";
 	 	if(!(networkName.equals(""))){
 	 			 	 str1=str1+" and lower(site_name) like lower('%"+networkName+"%') ";
 	 		}
 	 	//if(networkName.equals("") && pk_main_network.length()>0)
 	 	if(pk_main_network.length()>0)
 	 	{
 	 		str1=str1+" and pk_nwsites in("+pk_main_network+") ";
 	 	}
 	 	System.out.println("str1==="+str1);
	try {
		conn = CommonDAO.getConnection();
		pstmt = conn.prepareStatement(str1);
		pstmt.setInt(1, StringUtil.stringToNum(iaccId));
		rs = pstmt.executeQuery();
		Code code=null;
		while (rs.next()) {
			code=new Code();
			pkNetList.add(String.valueOf(rs.getInt("pk_nwsites")));
			fkSiteList.add(String.valueOf(rs.getInt("fk_site")));
			if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
				pkRelnShipType.add(rs.getString("pk_nwsites"));
			}
			else{
				pkRelnShipType.add("0");
			}
			netNameList.add(rs.getString("network_name"));
			code.setType(rs.getString("stat_type"));
			code.setCode(rs.getString("code"));
			code.setDescription(rs.getString("description"));
			netStatList.add(code);
			if(rs.getString("SiteRelationshipType")!=null){
			code=new Code();
			code.setType(rs.getString("SiteRelationshipType"));
			code.setCode(rs.getString("SiteRelationshipSubType"));
			code.setDescription(rs.getString("SiteRelationshipDesc"));
			siteRelnShipType.add(code);
			}
			else{
				siteRelnShipType.add(null);
			}
			siteNameList.add(rs.getString("site_name"));
			netLevelList.add(String.valueOf(rs.getInt("nw_level")));
			if(rs.getString("ctep_id")!=null){
				ctepList.add(rs.getString("ctep_id"));
			}else{
				ctepList.add("");
			}
			count++;
		}
		if(count==0)
		{
			this.response=new ResponseHolder();
			this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
			this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "No data found")); 
			throw new OperationException();
		}
		
		String chlSql="";
		Site site=null;
		SiteIdentifier siteIdentifier=null;
		ObjectMap map=null;
		Site sitelvl2=null;
		Site sitelvl3=null;
		Site sitelvl4=null;
		Site sitelvl5=null;
		Site sitelvl6=null;
		Site sitelvl7=null;
		
		List<Site> siteslvl2=null;
		List<Site> siteslvl3=null;
		List<Site> siteslvl4=null;
		List<Site> siteslvl5=null;
		List<Site> siteslvl6=null;
		List<Site> siteslvl7=null;
		MoreDetailsJB mdJB=null;
		MoreDetailsDao mdDao;
		ArrayList idList = new ArrayList();
		ArrayList modElementDescList = new ArrayList();
		ArrayList modElementDataList = new ArrayList();
		ArrayList modElementKeysList = new ArrayList();
		List<NVPair> listNVPair=null;
		NVPair nvpair = null;
		List<Site> siteList=null;
		for(int i=0;i<pkNetList.size();i++){
			siteList=new ArrayList<Site>();
			listNVPair=new ArrayList<NVPair>();
			site=new Site();
			siteIdentifier=new SiteIdentifier();
			map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(fkSiteList.get(i)));
			siteIdentifier.setOID(map.getOID());
			siteIdentifier.setPK(Integer.valueOf(fkSiteList.get(i)));
			site.setSiteIdentifier(siteIdentifier);
			site.setLevel(String.valueOf(netLevelList.get(i)));
			site.setName(siteNameList.get(i));
			site.setSiteStatus(netStatList.get(i));
			if(!pkRelnShipType.get(i).equals("0") && siteRelnShipType.get(i)!=null){
			site.setRelationshipPK(pkRelnShipType.get(i));
			site.setSiteRelationShipType(siteRelnShipType.get(i));
			}
			if(!ctepList.get(i).equals("")){
				site.setCtepId(ctepList.get(i));
			}
			//if(moreSiteDetails==null){
			mdJB = new MoreDetailsJB();
			mdDao = new MoreDetailsDao();
			mdDao = mdJB.getMoreDetails(EJBUtil.stringToNum(pkNetList.get(i)),"org_"+(EJBUtil.stringToNum(netLevelList.get(i))-1),userBean.getUserGrpDefault());
			idList = mdDao.getId();
			modElementDescList = mdDao.getMdElementDescs();
			modElementDataList = mdDao.getMdElementValues();
			modElementKeysList = mdDao.getMdElementKeys();
			for(int iY=0; iY<idList.size(); iY++){
				if((Integer)idList.get(iY)>0){
				nvpair = new NVPair();
				nvpair.setKey((String) modElementKeysList.get(iY));
				nvpair.setValue((String)modElementDataList.get(iY));
				listNVPair.add(nvpair);
				}
			}
			if(net_pks.equals("")){
				net_pks= pkNetList.get(i).toString();
			}
			//}
			/*else{		
					listNVPair=new UserServiceImpl().getNetworkMoreDetailValue(pkNetList.get(i),keySearch,(EJBUtil.stringToNum(netLevelList.get(i))-1),iaccId);
				}*/
			
			site.setMoreSiteDetailsFields(listNVPair);
			//siteList.add(site);
			
			//chlSql="select pk_nwsites,fk_site,lower(site_name) site_name,er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden,nw_memberType,(SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType,(SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,(SELECT CODELST_HIDE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipHide, nw_level+1 as nw_level from er_nwsites,er_site,er_codelst where pk_site=fk_site and pk_codelst=nw_status "+
			//	" and er_site.fk_account=? and FK_NWSITES_MAIN=? ";
			String pks_childNtw="";
			
			//chlSql="select pk_nwsites,site_name from er_nwsites,er_site,er_codelst where pk_site=fk_site and pk_codelst=nw_status "+
			//		" and er_site.fk_account=? and FK_NWSITES_MAIN=? ";
			
			chlSql="select pk_nwsites, fk_nwsites, fk_site, site_name,ctep_id,er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden,(SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType,(SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,(SELECT CODELST_HIDE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipHide, nw_level+1 as nw_level "+
					" FROM er_nwsites,er_site,er_codelst WHERE pk_site=fk_site and pk_codelst=nw_status and er_site.fk_account=? AND pk_nwsites!=? ";
			
		if(!siteName.equals("")){
			chlSql=chlSql+" and lower(site_name) like lower('%"+siteName+"%') ";
		}
		
		if(!CTEP_ID.equals("")){
			chlSql=chlSql+" and lower(ctep_id) like lower('%"+CTEP_ID+"%') ";
		}
		
		if(moredetails!=null && moredetails.size()>0 && (net_pks.length()>0)){
			if(networkSiteDetails!=null && networkSiteDetails.getRelationship_PK()!=null && !networkSiteDetails.getRelationship_PK().equals(""))
			{
				chlSql=chlSql+" and pk_nwsites in ("+net_pks+","+networkSiteDetails.getRelationship_PK()+") ";
			}else{
				chlSql=chlSql+" and pk_nwsites in ("+net_pks+")";
			}
						
		}else{
			if(networkSiteDetails!=null && networkSiteDetails.getRelationship_PK()!=null && !networkSiteDetails.getRelationship_PK().equals(""))
			{
				chlSql=chlSql+" and pk_nwsites in ("+networkSiteDetails.getRelationship_PK()+") ";
			}
		}
		if(!sitePk.equals("")){
			chlSql=chlSql+" and fk_site ="+sitePk;
		}
		
		
		if(!(pkCode.equals("0")) && !(pkCode.equals(""))){
			chlSql=chlSql+" and nw_membertype="+pkCode;
 	 	}
		System.out.println("chlSql change==="+chlSql);
		chlSql=chlSql+" START WITH pk_nwsites=? CONNECT BY prior pk_nwsites=FK_NWSITES ORDER SIBLINGS BY lower(site_name)";
		pstmt = conn.prepareStatement(chlSql);
		pstmt.setInt(1, StringUtil.stringToNum(iaccId));
		pstmt.setInt(2, StringUtil.stringToNum(pkNetList.get(i)));
		pstmt.setInt(3, StringUtil.stringToNum(pkNetList.get(i)));
		rs = pstmt.executeQuery();
		siteslvl2=new ArrayList<Site>();
		siteslvl3=new ArrayList<Site>();
		siteslvl4=new ArrayList<Site>();
		siteslvl5=new ArrayList<Site>();
		siteslvl6=new ArrayList<Site>();
		siteslvl7=new ArrayList<Site>();
		while(rs.next()){
			
			if((!siteName.equals("") || !CTEP_ID.equals("") || !sitePk.equals("") || networkSiteDetails.getRelationship_PK()!=null || networkSiteDetails.getRelationship_Type()!=null) && (pk_main_network!=null && !pk_main_network.equals("0"))){
				pks_childNtw=pks_childNtw+rs.getInt("pk_nwsites")+",";
			}
			else{
				code=new Code();
				listNVPair=new ArrayList<NVPair>();
				siteIdentifier=new SiteIdentifier();
				map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(rs.getInt("fk_site")));
				siteIdentifier.setOID(map.getOID());
				siteIdentifier.setPK(Integer.valueOf(rs.getInt("fk_site")));
				code.setType(rs.getString("stat_type"));
				code.setCode(rs.getString("code"));
				code.setDescription(rs.getString("description"));
				mdJB = new MoreDetailsJB();
				mdDao = new MoreDetailsDao();
				mdDao = mdJB.getMoreDetails(rs.getInt("pk_nwsites"),"org_"+(rs.getInt("nw_level")-1),userBean.getUserGrpDefault());
				idList = mdDao.getId();
				modElementDescList = mdDao.getMdElementDescs();
				modElementDataList = mdDao.getMdElementValues();
				modElementKeysList = mdDao.getMdElementKeys();
				for(int iY=0; iY<idList.size(); iY++){
					if((Integer)idList.get(iY)>0){
					nvpair = new NVPair();
					nvpair.setKey((String) modElementKeysList.get(iY));
					nvpair.setValue((String)modElementDataList.get(iY));
					listNVPair.add(nvpair);
					}
				}
				
				if(rs.getInt("nw_level")==2){
					sitelvl2=new Site();
					siteslvl3=new ArrayList<Site>();
					sitelvl2.setSiteIdentifier(siteIdentifier);
					sitelvl2.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl2.setName(rs.getString("site_name"));
					sitelvl2.setSiteStatus(code);
					sitelvl2.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl2.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl2.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					sitelvl2.setMoreSiteDetailsFields(listNVPair);
					siteslvl2.add(sitelvl2);
					site.setCSites(siteslvl2);		
				}
				else if(rs.getInt("nw_level")==3){
					sitelvl3=new Site();
					siteslvl4=new ArrayList<Site>();
					sitelvl3.setSiteIdentifier(siteIdentifier);
					sitelvl3.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl3.setName(rs.getString("site_name"));
					sitelvl3.setSiteStatus(code);
					sitelvl3.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl3.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl3.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					sitelvl3.setMoreSiteDetailsFields(listNVPair);
					siteslvl3.add(sitelvl3);
					sitelvl2.setCSites(siteslvl3);
				}
				else if(rs.getInt("nw_level")==4){
					sitelvl4=new Site();
					siteslvl5=new ArrayList<Site>();
					sitelvl4.setSiteIdentifier(siteIdentifier);
					sitelvl4.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl4.setName(rs.getString("site_name"));
					sitelvl4.setSiteStatus(code);
					sitelvl4.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl4.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl4.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					sitelvl4.setMoreSiteDetailsFields(listNVPair);
					siteslvl4.add(sitelvl4);
					sitelvl3.setCSites(siteslvl4);
				}
				else if(rs.getInt("nw_level")==5){
						sitelvl5=new Site();
						siteslvl6=new ArrayList<Site>();
						sitelvl5.setSiteIdentifier(siteIdentifier);
						sitelvl5.setLevel(String.valueOf(rs.getInt("nw_level")));
						sitelvl5.setName(rs.getString("site_name"));
						sitelvl5.setSiteStatus(code);
						sitelvl5.setCtepId(rs.getString("ctep_id"));
						if(rs.getString("SiteRelationshipType")!=null){
						code=new Code();
						code.setType(rs.getString("SiteRelationshipType"));
						code.setCode(rs.getString("SiteRelationshipSubType"));
						code.setDescription(rs.getString("SiteRelationshipDesc"));
						sitelvl5.setSiteRelationShipType(code);
						}
						if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
							sitelvl5.setRelationshipPK(rs.getString("pk_nwsites"));
						}
						sitelvl5.setMoreSiteDetailsFields(listNVPair);						
						siteslvl5.add(sitelvl5);
						sitelvl4.setCSites(siteslvl5);
				}
				else if(rs.getInt("nw_level")==6){
					sitelvl6=new Site();
					siteslvl7=new ArrayList<Site>();
					sitelvl6.setSiteIdentifier(siteIdentifier);
					sitelvl6.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl6.setName(rs.getString("site_name"));
					sitelvl6.setSiteStatus(code);
					sitelvl6.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl6.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl6.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					sitelvl6.setMoreSiteDetailsFields(listNVPair);		
					siteslvl6.add(sitelvl6);
					sitelvl5.setCSites(siteslvl6);
				}
				else if(rs.getInt("nw_level")==7){
					sitelvl7=new Site();
					sitelvl7.setSiteIdentifier(siteIdentifier);
					sitelvl7.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl7.setName(rs.getString("site_name"));
					sitelvl7.setSiteStatus(code);
					sitelvl7.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl7.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl7.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					sitelvl7.setMoreSiteDetailsFields(listNVPair);
					siteslvl7.add(sitelvl7);
					sitelvl6.setCSites(siteslvl7);
				}
			}
		}
		pks_childNtw=pks_childNtw.length()>0?pks_childNtw.substring(0,pks_childNtw.length()-1):"0";
		
		if(!pks_childNtw.equals("0") && !pks_childNtw.equals(""))
		{
		chlSql="SELECT distinct pk_nwsites,site_name,fk_site,ctep_id,er_codelst.CODELST_DESC AS description,er_codelst.codelst_type AS stat_type,er_codelst.codelst_subtyp AS code,er_codelst.codelst_hide AS stat_hidden, "
				+" (SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType, "
				+" (SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,(SELECT CODELST_HIDE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipHide, "
				+" nw_level+1 AS nw_level FROM er_nwsites,er_site,er_codelst where fk_site=pk_site AND pk_codelst=nw_status AND er_site.fk_account=? and pk_nwsites <> ? "
				+" START WITH pk_nwsites in ("+pks_childNtw+") CONNECT BY PRIOR fk_nwsites=pk_nwsites ORDER BY nw_level";
		
		pstmt = conn.prepareStatement(chlSql);
		pstmt.setInt(1, StringUtil.stringToNum(iaccId));
		pstmt.setInt(2, StringUtil.stringToNum(pkNetList.get(i)));
		rs = pstmt.executeQuery();
		siteslvl2=new ArrayList<Site>();
		siteslvl3=new ArrayList<Site>();
		siteslvl4=new ArrayList<Site>();
		siteslvl5=new ArrayList<Site>();
		siteslvl6=new ArrayList<Site>();
		siteslvl7=new ArrayList<Site>();
		
		while (rs.next()) {
			
			code=new Code();
			listNVPair=new ArrayList<NVPair>();
			siteIdentifier=new SiteIdentifier();
			map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(rs.getInt("fk_site")));
			siteIdentifier.setOID(map.getOID());
			siteIdentifier.setPK(Integer.valueOf(rs.getInt("fk_site")));
			code.setType(rs.getString("stat_type"));
			code.setCode(rs.getString("code"));
			code.setDescription(rs.getString("description"));
			mdJB = new MoreDetailsJB();
			mdDao = new MoreDetailsDao();
			mdDao = mdJB.getMoreDetails(rs.getInt("pk_nwsites"),"org_"+(rs.getInt("nw_level")-1),userBean.getUserGrpDefault());
			idList = mdDao.getId();
			modElementDescList = mdDao.getMdElementDescs();
			modElementDataList = mdDao.getMdElementValues();
			modElementKeysList = mdDao.getMdElementKeys();
			for(int iY=0; iY<idList.size(); iY++){
				if((Integer)idList.get(iY)>0){
				nvpair = new NVPair();
				nvpair.setKey((String) modElementKeysList.get(iY));
				nvpair.setValue((String)modElementDataList.get(iY));
				listNVPair.add(nvpair);
				}
			}
			
			if(rs.getInt("nw_level")==2){
				sitelvl2=new Site();
				sitelvl2.setSiteIdentifier(siteIdentifier);
				sitelvl2.setLevel(String.valueOf(rs.getInt("nw_level")));
				sitelvl2.setName(rs.getString("site_name"));
				sitelvl2.setSiteStatus(code);
				sitelvl2.setCtepId(rs.getString("ctep_id"));
				if(rs.getString("SiteRelationshipType")!=null){
				code=new Code();
				code.setType(rs.getString("SiteRelationshipType"));
				code.setCode(rs.getString("SiteRelationshipSubType"));
				code.setDescription(rs.getString("SiteRelationshipDesc"));
				sitelvl2.setSiteRelationShipType(code);
				}
				if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
					sitelvl2.setRelationshipPK(rs.getString("pk_nwsites"));
				}
				sitelvl2.setMoreSiteDetailsFields(listNVPair);
				siteslvl2.add(sitelvl2);
				site.setCSites(siteslvl2);
			}
			else if(rs.getInt("nw_level")==3){
				sitelvl3=new Site();
				sitelvl3.setSiteIdentifier(siteIdentifier);
				sitelvl3.setLevel(String.valueOf(rs.getInt("nw_level")));
				sitelvl3.setName(rs.getString("site_name"));
				sitelvl3.setSiteStatus(code);
				sitelvl3.setCtepId(rs.getString("ctep_id"));
				if(rs.getString("SiteRelationshipType")!=null){
				code=new Code();
				code.setType(rs.getString("SiteRelationshipType"));
				code.setCode(rs.getString("SiteRelationshipSubType"));
				code.setDescription(rs.getString("SiteRelationshipDesc"));
				sitelvl3.setSiteRelationShipType(code);
				}
				if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
					sitelvl3.setRelationshipPK(rs.getString("pk_nwsites"));
				}
				sitelvl3.setMoreSiteDetailsFields(listNVPair);
				siteslvl3.add(sitelvl3);
				sitelvl2.setCSites(siteslvl3);
			}
			else if(rs.getInt("nw_level")==4){
				sitelvl4=new Site();
				sitelvl4.setSiteIdentifier(siteIdentifier);
				sitelvl4.setLevel(String.valueOf(rs.getInt("nw_level")));
				sitelvl4.setName(rs.getString("site_name"));
				sitelvl4.setSiteStatus(code);
				sitelvl4.setCtepId(rs.getString("ctep_id"));
				if(rs.getString("SiteRelationshipType")!=null){
				code=new Code();
				code.setType(rs.getString("SiteRelationshipType"));
				code.setCode(rs.getString("SiteRelationshipSubType"));
				code.setDescription(rs.getString("SiteRelationshipDesc"));
				sitelvl4.setSiteRelationShipType(code);
				}
				if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
					sitelvl4.setRelationshipPK(rs.getString("pk_nwsites"));
				}
				sitelvl4.setMoreSiteDetailsFields(listNVPair);
				siteslvl4.add(sitelvl4);
				sitelvl3.setCSites(siteslvl4);
			}
			else if(rs.getInt("nw_level")==5){
				sitelvl5=new Site();
				sitelvl5.setSiteIdentifier(siteIdentifier);
				sitelvl5.setLevel(String.valueOf(rs.getInt("nw_level")));
				sitelvl5.setName(rs.getString("site_name"));
				sitelvl5.setSiteStatus(code);
				sitelvl5.setCtepId(rs.getString("ctep_id"));
				if(rs.getString("SiteRelationshipType")!=null){
				code=new Code();
				code.setType(rs.getString("SiteRelationshipType"));
				code.setCode(rs.getString("SiteRelationshipSubType"));
				code.setDescription(rs.getString("SiteRelationshipDesc"));
				sitelvl5.setSiteRelationShipType(code);
				}
				if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
					sitelvl5.setRelationshipPK(rs.getString("pk_nwsites"));
				}
				sitelvl5.setMoreSiteDetailsFields(listNVPair);
				siteslvl5.add(sitelvl5);
				sitelvl4.setCSites(siteslvl5);
			}
			else if(rs.getInt("nw_level")==6){
				sitelvl6=new Site();
				sitelvl6.setSiteIdentifier(siteIdentifier);
				sitelvl6.setLevel(String.valueOf(rs.getInt("nw_level")));
				sitelvl6.setName(rs.getString("site_name"));
				sitelvl6.setSiteStatus(code);
				sitelvl6.setCtepId(rs.getString("ctep_id"));
				if(rs.getString("SiteRelationshipType")!=null){
				code=new Code();
				code.setType(rs.getString("SiteRelationshipType"));
				code.setCode(rs.getString("SiteRelationshipSubType"));
				code.setDescription(rs.getString("SiteRelationshipDesc"));
				sitelvl6.setSiteRelationShipType(code);
				}
				if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
					sitelvl6.setRelationshipPK(rs.getString("pk_nwsites"));
				}
				sitelvl6.setMoreSiteDetailsFields(listNVPair);
				siteslvl6.add(sitelvl6);
				sitelvl5.setCSites(siteslvl6);
			}
			else if(rs.getInt("nw_level")==7){
				sitelvl7=new Site();
				siteIdentifier=new SiteIdentifier();
				sitelvl7.setSiteIdentifier(siteIdentifier);
				sitelvl7.setLevel(String.valueOf(rs.getInt("nw_level")));
				sitelvl7.setName(rs.getString("site_name"));
				sitelvl7.setSiteStatus(code);
				sitelvl7.setCtepId(rs.getString("ctep_id"));
				if(rs.getString("SiteRelationshipType")!=null){
				code=new Code();
				code.setType(rs.getString("SiteRelationshipType"));
				code.setCode(rs.getString("SiteRelationshipSubType"));
				code.setDescription(rs.getString("SiteRelationshipDesc"));
				sitelvl7.setSiteRelationShipType(code);
				}
				if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
					sitelvl7.setRelationshipPK(rs.getString("pk_nwsites"));
				}
				sitelvl7.setMoreSiteDetailsFields(listNVPair);
				siteslvl7.add(sitelvl7);
				sitelvl6.setCSites(siteslvl7);
			}
			}
		chlSql="SELECT pk_nwsites,fk_nwsites,fk_site,site_name,ctep_id,er_codelst.CODELST_DESC AS description,er_codelst.codelst_type AS stat_type,er_codelst.codelst_subtyp AS code,er_codelst.codelst_hide AS stat_hidden, "
				+" (SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType, "
				+" (SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,(SELECT CODELST_HIDE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipHide, "
				+" nw_level+1 AS nw_level FROM er_nwsites,er_site,er_codelst where fk_site=pk_site AND pk_codelst=nw_status AND er_site.fk_account=? and pk_nwsites <> ? "
				+" START WITH pk_nwsites in ("+pks_childNtw+") CONNECT BY PRIOR pk_nwsites=fk_nwsites ORDER SIBLINGS BY lower(site_name)";
		pstmt = conn.prepareStatement(chlSql);
		pstmt.setInt(1, StringUtil.stringToNum(iaccId));
		pstmt.setInt(2, StringUtil.stringToNum(pks_childNtw));
		rs = pstmt.executeQuery();
		while (rs.next()) {
				
				code=new Code();
				listNVPair=new ArrayList<NVPair>();
				siteIdentifier=new SiteIdentifier();
				map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(rs.getInt("fk_site")));
				siteIdentifier.setOID(map.getOID());
				siteIdentifier.setPK(Integer.valueOf(rs.getInt("fk_site")));
				code.setType(rs.getString("stat_type"));
				code.setCode(rs.getString("code"));
				code.setDescription(rs.getString("description"));
				mdJB = new MoreDetailsJB();
				mdDao = new MoreDetailsDao();
				mdDao = mdJB.getMoreDetails(rs.getInt("pk_nwsites"),"org_"+(rs.getInt("nw_level")-1),userBean.getUserGrpDefault());
				idList = mdDao.getId();
				modElementDescList = mdDao.getMdElementDescs();
				modElementDataList = mdDao.getMdElementValues();
				modElementKeysList = mdDao.getMdElementKeys();
				for(int iY=0; iY<idList.size(); iY++){
					if((Integer)idList.get(iY)>0){
					nvpair = new NVPair();
					nvpair.setKey((String) modElementKeysList.get(iY));
					nvpair.setValue((String)modElementDataList.get(iY));
					listNVPair.add(nvpair);
					}
				}
				
				if(rs.getInt("nw_level")==3){
					sitelvl3=new Site();
					siteslvl4=new ArrayList<Site>();
					sitelvl3.setSiteIdentifier(siteIdentifier);
					sitelvl3.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl3.setName(rs.getString("site_name"));
					sitelvl3.setSiteStatus(code);
					sitelvl3.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl3.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl3.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					sitelvl3.setMoreSiteDetailsFields(listNVPair);
					siteslvl3.add(sitelvl3);
					sitelvl2.setCSites(siteslvl3);
				}
				else if(rs.getInt("nw_level")==4){
					sitelvl4=new Site();
					siteslvl5=new ArrayList<Site>();
					sitelvl4.setSiteIdentifier(siteIdentifier);
					sitelvl4.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl4.setName(rs.getString("site_name"));
					sitelvl4.setSiteStatus(code);
					sitelvl4.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl4.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl4.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					sitelvl4.setMoreSiteDetailsFields(listNVPair);
					siteslvl4.add(sitelvl4);
					sitelvl3.setCSites(siteslvl4);
				}
				else if(rs.getInt("nw_level")==5){
					sitelvl5=new Site();
					siteslvl6=new ArrayList<Site>();
					sitelvl5.setSiteIdentifier(siteIdentifier);
					sitelvl5.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl5.setName(rs.getString("site_name"));
					sitelvl5.setSiteStatus(code);
					sitelvl5.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl5.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl5.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					sitelvl5.setMoreSiteDetailsFields(listNVPair);
					siteslvl5.add(sitelvl5);
					sitelvl4.setCSites(siteslvl5);
				}
				else if(rs.getInt("nw_level")==6){
					sitelvl6=new Site();
					siteslvl7=new ArrayList<Site>();
					sitelvl6.setSiteIdentifier(siteIdentifier);
					sitelvl6.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl6.setName(rs.getString("site_name"));
					sitelvl6.setSiteStatus(code);
					sitelvl6.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl6.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl6.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					sitelvl6.setMoreSiteDetailsFields(listNVPair);
					siteslvl6.add(sitelvl6);
					sitelvl5.setCSites(siteslvl6);
				}
				else if(rs.getInt("nw_level")==7){
					sitelvl7=new Site();
					siteIdentifier=new SiteIdentifier();
					sitelvl7.setSiteIdentifier(siteIdentifier);
					sitelvl7.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl7.setName(rs.getString("site_name"));
					sitelvl7.setSiteStatus(code);
					sitelvl7.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl7.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl7.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					sitelvl7.setMoreSiteDetailsFields(listNVPair);
					siteslvl7.add(sitelvl7);
					sitelvl6.setCSites(siteslvl7);
				}
				}
		
		}siteList.add(site);
		sitesList.add((ArrayList<Site>) siteList);
		}

	}catch(OperationException ex){
		throw ex;
	}catch(SQLException ex){
		ex.printStackTrace();
	}catch (Exception e) {
		e.printStackTrace();
	} finally {
		try {
			rs.close();
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}



	
public void searchNetworks(NetworksDetails networksDetails,HashMap<String, Object> params, String from) throws OperationException {
		
	
	Connection conn=null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	int count=0;
	String str = "";
	String pk_main_network = "";
	String networkName = (networksDetails.getNetworkName()!=null)?networksDetails.getNetworkName():"";
	String sitePK = (networksDetails.getSitePK()!=null)?networksDetails.getSitePK():"";
	String siteName = (networksDetails.getSiteName()!=null)?networksDetails.getSiteName():"";
	String networkPK = (networksDetails.getNetworkPK()!=null)?networksDetails.getNetworkPK():"";
	String iaccId = params.get(KEY_ACCOUNT_ID).toString();
	
	
	if((networkName.equals("") && networkPK.equals("")) &&
			(!siteName.equals("") || !sitePK.equals(""))){
		String networkPKBySite = "select nvl(fk_nwsites_main,pk_nwsites) as fk_nwsites_main from er_nwsites,er_site where pk_site=fk_site";
		if(!siteName.equals("")){
			networkPKBySite=networkPKBySite+ " and lower(site_name) = lower('"+siteName+"')";
		}
		if(!sitePK.equals("")){
			networkPKBySite=networkPKBySite+ " and fk_site = "+StringUtil.stringToNum(sitePK);
		}
		
		networkPKBySite=networkPKBySite+" order by pk_nwsites";
		System.out.println("networkPKBySite==="+networkPKBySite);
		try {
			conn = CommonDAO.getConnection();
			pstmt = conn.prepareStatement(networkPKBySite);
			rs=pstmt.executeQuery();
			while(rs.next()){
				if(pk_main_network.equals("")){
					pk_main_network=String.valueOf(rs.getInt("fk_nwsites_main"));
				}else{
					pk_main_network=pk_main_network+","+String.valueOf(rs.getInt("fk_nwsites_main"));
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
	}else if(!networkPK.equals("")){
		pk_main_network=networkPK;
	}
try {
if (!networkName.equals("") || !pk_main_network.equals("")){
	System.out.println("final pk_main_network==="+pk_main_network);
	
	 	String str1 = "select pk_nwsites,fk_site,site_name ,site_info,site_info as network_info,nw_status,site_name as network_name,ctep_id, er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden,er_nwsites.NW_MEMBERTYPE as member_type,(SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType,(SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,nw_level from er_nwsites,er_site,er_codelst "+
	 				  " where pk_site=fk_site and pk_codelst=nw_status and er_site.fk_account=? and nw_level=0 ";
	 	if(!(networkName.equals(""))){
	 			 	 str1=str1+" and lower(site_name) like lower('%"+networkName+"%') ";
	 		}
	 	//if(networkName.equals("") && pk_main_network.length()>0)
	 	if(pk_main_network.length()>0)
	 	{
	 		str1=str1+" and pk_nwsites in("+pk_main_network+") ";
	 	}
	 	System.out.println("str1==="+str1);

	conn = CommonDAO.getConnection();
	pstmt = conn.prepareStatement(str1);
	pstmt.setInt(1, StringUtil.stringToNum(iaccId));
	rs = pstmt.executeQuery();
	Code code=null;
	while (rs.next()) {
		code=new Code();
		pkNetList.add(String.valueOf(rs.getInt("pk_nwsites")));
		fkSiteList.add(String.valueOf(rs.getInt("fk_site")));
		if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
			pkRelnShipType.add(rs.getString("pk_nwsites"));
		}
		else{
			pkRelnShipType.add("0");
		}
		netNameList.add(rs.getString("network_name"));
		netInfoList.add(rs.getString("network_info"));
		code.setPk(String.valueOf(rs.getInt("nw_status")));
		code.setType(rs.getString("stat_type"));
		code.setCode(rs.getString("code"));
		code.setDescription(rs.getString("description"));
		netStatList.add(code);
		if(rs.getString("SiteRelationshipType")!=null){
		code=new Code();
		code.setPk(String.valueOf(rs.getInt("member_type")));
		code.setType(rs.getString("SiteRelationshipType"));
		code.setCode(rs.getString("SiteRelationshipSubType"));
		code.setDescription(rs.getString("SiteRelationshipDesc"));
		siteRelnShipType.add(code);
		}
		else{
			siteRelnShipType.add(null);
		}
		siteNameList.add(rs.getString("site_name"));
		siteInfoList.add(rs.getString("site_info"));
		netLevelList.add(String.valueOf(rs.getInt("nw_level")));
		if(rs.getString("ctep_id")!=null){
			ctepList.add(rs.getString("ctep_id"));
		}else{
			ctepList.add("");
		}
		count++;
	}
	if(count==0)
	{
		this.response=new ResponseHolder();
		this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
		this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "No data found")); 
		throw new OperationException();
	}
	String chlSql="";
	Site site=null;
	SiteIdentifier siteIdentifier=null;
	ObjectMap map=null;
	
	UserBean userBean=(UserBean)params.get("callingUser");
	List<Site> siteslvl1=null;
	List<Site> siteslvl2=null;
	List<Site> siteslvl3=null;
	List<Site> siteslvl4=null;
	List<Site> siteslvl5=null;
	List<Site> siteslvl6=null;
	Sites psiteslvl0=null;
	Sites psiteslvl1=null;
	Sites psiteslvl2=null;
	Sites psiteslvl3=null;
	Sites psiteslvl4=null;
	Sites psiteslvl5=null;
	MoreDetailsJB mdJB=null;
	MoreDetailsDao mdDao;
	ArrayList idList = new ArrayList();
	ArrayList modElementDescList = new ArrayList();
	ArrayList modElementDataList = new ArrayList();
	ArrayList modElementKeysList = new ArrayList();
	List<NVPair> listNVPair=null;
	NVPair nvpair = null;
	List<Site> siteList=null;
	String net_pks = "";
	for(int i=0;i<pkNetList.size();i++){
		
		//siteList.add(site);
		
		//chlSql="select pk_nwsites,fk_site,lower(site_name) site_name,er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden,nw_memberType,(SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType,(SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,(SELECT CODELST_HIDE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipHide, nw_level+1 as nw_level from er_nwsites,er_site,er_codelst where pk_site=fk_site and pk_codelst=nw_status "+
		//	" and er_site.fk_account=? and FK_NWSITES_MAIN=? ";
		String pks_childNtw="";
		
		//chlSql="select pk_nwsites,site_name from er_nwsites,er_site,er_codelst where pk_site=fk_site and pk_codelst=nw_status "+
		//		" and er_site.fk_account=? and FK_NWSITES_MAIN=? ";
		
		
		List<String> relationshipPKList = new ArrayList<String>();
		if(!siteName.equals("") || !sitePK.equals("")){
		String getRelationshipPk = "select pk_nwsites from er_nwsites,er_site where (fk_nwsites_main="+StringUtil.stringToNum(pkNetList.get(i))+" or pk_nwsites="+StringUtil.stringToNum(pkNetList.get(i))+") and pk_site=fk_site";
		if(!siteName.equals("")){
			getRelationshipPk=getRelationshipPk+ " and lower(site_name) = lower('"+siteName+"')";
		}
		if(!sitePK.equals("")){
			getRelationshipPk=getRelationshipPk+ " and fk_site = "+StringUtil.stringToNum(sitePK);
		}
		
		getRelationshipPk=getRelationshipPk+" order by pk_nwsites";
		pstmt = conn.prepareStatement(getRelationshipPk);
		rs=pstmt.executeQuery();
		while(rs.next()){
			if(rs.getString("pk_nwsites")!=null){
				relationshipPKList.add(String.valueOf(rs.getInt("pk_nwsites")));
			}
			
		}
			System.out.println("getRelationshipPk==="+getRelationshipPk);
			
		
	}
	
	if(relationshipPKList.size()>0){
		
		siteList=new ArrayList<Site>();
		
		for(String relationPK: relationshipPKList){
			siteslvl1=new ArrayList<Site>();
			siteslvl2=new ArrayList<Site>();
			siteslvl3=new ArrayList<Site>();
			siteslvl4=new ArrayList<Site>();
			siteslvl5=new ArrayList<Site>();
			siteslvl6=new ArrayList<Site>();
			psiteslvl0=new Sites();
			psiteslvl1=new Sites();
			psiteslvl2=new Sites();
			psiteslvl3=new Sites();
			psiteslvl4=new Sites();
			psiteslvl5=new Sites();
			Site sitelvl1=null;
			Site sitelvl2=null;
			Site sitelvl3=null;
			Site sitelvl4=null;
			Site sitelvl5=null;
			Site sitelvl6=null;
			
			listNVPair=new ArrayList<NVPair>();
			site=new Site();
			siteIdentifier=new SiteIdentifier();
			map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(fkSiteList.get(i)));
			siteIdentifier.setOID(map.getOID());
			siteIdentifier.setPK(Integer.valueOf(fkSiteList.get(i)));
			site.setSiteIdentifier(siteIdentifier);
			site.setLevel(String.valueOf(netLevelList.get(i)));
			site.setName(siteNameList.get(i));
			site.setDescription(siteInfoList.get(i));
			site.setSiteStatus(netStatList.get(i));
			if(!pkRelnShipType.get(i).equals("0") && siteRelnShipType.get(i)!=null){
			site.setRelationshipPK(pkRelnShipType.get(i));
			site.setSiteRelationShipType(siteRelnShipType.get(i));
			
			}
			if(!ctepList.get(i).equals("")){
				site.setCtepId(ctepList.get(i));
			}
			
		
				pks_childNtw = relationPK;
			
			System.out.println("pks_childNtw==="+pks_childNtw);
			if(!pks_childNtw.equals("0") && !pks_childNtw.equals(""))
			{
			chlSql="SELECT distinct pk_nwsites,site_name,site_info,fk_site,ctep_id,er_codelst.CODELST_DESC AS description,er_codelst.codelst_type AS stat_type,er_codelst.codelst_subtyp AS code,er_codelst.codelst_hide AS stat_hidden, "
					+" nw_status, er_nwsites.NW_MEMBERTYPE as member_type, (SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType, "
					+" (SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,(SELECT CODELST_HIDE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipHide, "
					+" nw_level FROM er_nwsites,er_site,er_codelst where fk_site=pk_site AND pk_codelst=nw_status AND er_site.fk_account=? and pk_nwsites <> ? "
					+" and pk_nwsites in ("+ pks_childNtw +") ORDER BY nw_level";
			System.out.println("chlSql==="+chlSql);
			System.out.println("iaccId==="+iaccId);
			System.out.println("pkNetList.get(i)==="+pkNetList.get(i));
			pstmt = conn.prepareStatement(chlSql);
			pstmt.setInt(1, StringUtil.stringToNum(iaccId));
			pstmt.setInt(2, StringUtil.stringToNum(pkNetList.get(i)));
			rs = pstmt.executeQuery();
			
			
			while (rs.next()) {
				
				code=new Code();
				listNVPair=new ArrayList<NVPair>();
				siteIdentifier=new SiteIdentifier();
				map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(rs.getInt("fk_site")));
				siteIdentifier.setOID(map.getOID());
				siteIdentifier.setPK(Integer.valueOf(rs.getInt("fk_site")));
				code.setPk(String.valueOf(rs.getInt("nw_status")));
				code.setType(rs.getString("stat_type"));
				code.setCode(rs.getString("code"));
				code.setDescription(rs.getString("description"));
				mdJB = new MoreDetailsJB();
				mdDao = new MoreDetailsDao();
				mdDao = mdJB.getMoreDetails(rs.getInt("pk_nwsites"),"org_"+(rs.getInt("nw_level")-1),userBean.getUserGrpDefault());
				idList = mdDao.getId();
				modElementDescList = mdDao.getMdElementDescs();
				modElementDataList = mdDao.getMdElementValues();
				modElementKeysList = mdDao.getMdElementKeys();
				for(int iY=0; iY<idList.size(); iY++){
					if((Integer)idList.get(iY)>0){
					nvpair = new NVPair();
					nvpair.setKey((String) modElementKeysList.get(iY));
					nvpair.setValue((String)modElementDataList.get(iY));
					listNVPair.add(nvpair);
					}
				}
				
				if(rs.getInt("nw_level")==1){
					sitelvl1=new Site();
					sitelvl1.setSiteIdentifier(siteIdentifier);
					sitelvl1.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl1.setName(rs.getString("site_name"));
					sitelvl1.setDescription(rs.getString("site_info"));
					sitelvl1.setSiteStatus(code);
					sitelvl1.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setPk(rs.getString("member_type"));
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl1.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl1.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					siteslvl1.add(sitelvl1);
					//siteslvl2=new ArrayList<Site>();
					psiteslvl0.setSites(siteslvl1);
					site.setSites(psiteslvl0);
				}
				else if(rs.getInt("nw_level")==2){
					sitelvl2=new Site();
					sitelvl2.setSiteIdentifier(siteIdentifier);
					sitelvl2.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl2.setName(rs.getString("site_name"));
					sitelvl2.setDescription(rs.getString("site_info"));
					System.out.println("level 2 description==="+sitelvl2.getDescription());
					sitelvl2.setSiteStatus(code);
					sitelvl2.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setPk(rs.getString("member_type"));
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl2.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl2.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					siteslvl2.add(sitelvl2);
					//siteslvl3=new ArrayList<Site>();
					if(sitelvl1!=null){
						System.out.println("Inside If");
						psiteslvl1.setSites(siteslvl2);
						sitelvl1.setSites(psiteslvl1);
					}else{
						System.out.println("Inside Else");
						psiteslvl0.setSites(siteslvl2);
						site.setSites(psiteslvl0);
					}
					
					
				}
				else if(rs.getInt("nw_level")==3){
					sitelvl3=new Site();
					sitelvl3.setSiteIdentifier(siteIdentifier);
					sitelvl3.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl3.setName(rs.getString("site_name"));
					sitelvl3.setDescription(rs.getString("site_info"));
					System.out.println("level 3 description==="+sitelvl3.getDescription());
					sitelvl3.setSiteStatus(code);
					sitelvl3.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setPk(rs.getString("member_type"));
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl3.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl3.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					siteslvl3.add(sitelvl3);
					//siteslvl4=new ArrayList<Site>();
					if(sitelvl2!=null){
						psiteslvl2.setSites(siteslvl3);
						sitelvl2.setSites(psiteslvl2);
					}else{
						psiteslvl0.setSites(siteslvl3);
						site.setSites(psiteslvl0);
					}
				}
				else if(rs.getInt("nw_level")==4){
					sitelvl4=new Site();
					sitelvl4.setSiteIdentifier(siteIdentifier);
					sitelvl4.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl4.setName(rs.getString("site_name"));
					sitelvl4.setDescription(rs.getString("site_info"));
					sitelvl4.setSiteStatus(code);
					sitelvl4.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setPk(rs.getString("member_type"));
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl4.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl4.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					siteslvl4.add(sitelvl4);
					//siteslvl5=new ArrayList<Site>();
					if(sitelvl3!=null){
						psiteslvl3.setSites(siteslvl4);
						sitelvl3.setSites(psiteslvl3);
					}else{
						psiteslvl0.setSites(siteslvl4);
						site.setSites(psiteslvl0);
					}
					
				}
				else if(rs.getInt("nw_level")==5){
					sitelvl5=new Site();
					sitelvl5.setSiteIdentifier(siteIdentifier);
					sitelvl5.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl5.setName(rs.getString("site_name"));
					sitelvl5.setDescription(rs.getString("site_info"));
					sitelvl5.setSiteStatus(code);
					sitelvl5.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setPk(rs.getString("member_type"));
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl5.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl5.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					siteslvl5.add(sitelvl5);
					//siteslvl6=new ArrayList<Site>();
					if(sitelvl4!=null){
						psiteslvl4.setSites(siteslvl5);
						sitelvl4.setSites(psiteslvl4);
					}else{
						psiteslvl0.setSites(siteslvl5);
						site.setSites(psiteslvl0);
					}
				}
				else if(rs.getInt("nw_level")==6){
					sitelvl6=new Site();
					siteIdentifier=new SiteIdentifier();
					sitelvl6.setSiteIdentifier(siteIdentifier);
					sitelvl6.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl6.setName(rs.getString("site_name"));
					sitelvl6.setDescription(rs.getString("site_info"));
					sitelvl6.setSiteStatus(code);
					sitelvl6.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setPk(rs.getString("member_type"));
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl6.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl6.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					siteslvl6.add(sitelvl6);
					if(sitelvl5!=null){
						psiteslvl5.setSites(siteslvl6);
						sitelvl5.setSites(psiteslvl5);
					}else{
						psiteslvl0.setSites(siteslvl6);
						site.setSites(psiteslvl0);
					}
				}
				}
			}
			siteList.add(site);
		}
		sitesList.add((ArrayList<Site>) siteList);
		}else{
			siteList=new ArrayList<Site>();
			site=new Site();
			siteIdentifier=new SiteIdentifier();
			map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(fkSiteList.get(i)));
			siteIdentifier.setOID(map.getOID());
			siteIdentifier.setPK(Integer.valueOf(fkSiteList.get(i)));
			site.setSiteIdentifier(siteIdentifier);
			site.setLevel(String.valueOf(netLevelList.get(i)));
			site.setName(siteNameList.get(i));
			site.setDescription(siteInfoList.get(i));
			site.setSiteStatus(netStatList.get(i));
			if(!pkRelnShipType.get(i).equals("0") && siteRelnShipType.get(i)!=null){
			site.setRelationshipPK(pkRelnShipType.get(i));
			site.setSiteRelationShipType(siteRelnShipType.get(i));
			
			}
			if(!ctepList.get(i).equals("")){
				site.setCtepId(ctepList.get(i));
			}
			siteList.add(site);
			sitesList.add((ArrayList<Site>) siteList);
		}
	
	}
}else{
	this.response=new ResponseHolder();
	this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
	this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "No data found")); 
	throw new OperationException();
}
}catch(OperationException ex){
	throw ex;
}catch(SQLException ex){
	ex.printStackTrace();
}catch (Exception e) {
	e.printStackTrace();
} finally {
	try {
		rs.close();
		pstmt.close();
		conn.close();
	} catch (SQLException e) {
		e.printStackTrace();
	}
}

	}
 public void searchNetworkUsers(NetworkUserDetail networkuser, HashMap<String, Object> params) throws OperationException {
		Connection conn=null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ResultSet rs1=null;
		int userLoginPK = 0;
		int userPK=0;
		int userOIDPK=0;
		String pkCode="";
		int count=0;
		String net_pks="";
		ObjectMap map=null;
		UserBean userBeanPk = null;
		String networkName="";
		String networkPk="";
		String relationShipPK="";
		String sitePK="";
		String siteName="";
		UserAgentRObj userAgent = (UserAgentRObj) params.get("userAgent");
		UserBean userBean=(UserBean)params.get("callingUser");
		String iaccId = (String)params.get(KEY_ACCOUNT_ID);
		//System.out.println("networkuser.getNetworkDetails().getNetworkName()=="+networkuser.getNetworkDetails().getNetworkName());
	 if(networkuser.getNetworkDetails()!=null){
       if(networkuser.getNetworkDetails().getNetworkName()!=null){
		 networkName = networkuser.getNetworkDetails().getNetworkName()==null?"":networkuser.getNetworkDetails().getNetworkName();     
       }
       if(networkuser.getNetworkDetails().getNetworkPK()!=null){
  		 networkPk = networkuser.getNetworkDetails().getNetworkPK()==null?"":networkuser.getNetworkDetails().getNetworkPK();     
        }
       if(networkuser.getNetworkDetails().getRelationshipPK()!=null){
    	   relationShipPK = networkuser.getNetworkDetails().getRelationshipPK()==null?"":networkuser.getNetworkDetails().getRelationshipPK();     
          }
       if(networkName.equals("") && networkPk.equals("") && relationShipPK.equals("")){
			this.response=new ResponseHolder();
			this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
			this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid Network identifier is Required.")); 
			throw new OperationException();
		}
       if((networkuser.getNetworkDetails().getSiteName()!=null)|| (networkuser.getNetworkDetails().getRelationshipType() != null && networkuser.getNetworkDetails().getRelationshipType().getCode() != null)){
    
    	   if(networkName.equals("") && networkPk.equals("") && relationShipPK.equals("")){
    		    this.response=new ResponseHolder();
				this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Network identifier is mandatory.")); 
				throw new OperationException();
			}      
       }
       
       if(networkuser.getNetworkDetails().getSiteName()!=null){
    	   siteName = networkuser.getNetworkDetails().getSiteName()==null?"":networkuser.getNetworkDetails().getSiteName();
  		if(siteName.equals("")){
  			    this.response=new ResponseHolder();
				this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid Site Name is Required.")); 
				throw new OperationException();
  			}     
         }
       if(networkuser.getNetworkDetails().getSitePK()!=null){
    	   sitePK = networkuser.getNetworkDetails().getSitePK()==null?"":networkuser.getNetworkDetails().getSitePK();
  		if(sitePK.equals("")){
  			    this.response=new ResponseHolder();
				this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid Site PK is Required.")); 
				throw new OperationException();
  			}     
         }
       if(networkuser.getNetworkDetails().getRelationshipType() != null && networkuser.getNetworkDetails().getRelationshipType().getCode() != null)
		{
			try
			{
				CodeDao cd = new CodeDao();
		    	
		   pkCode = StringUtil.integerToString(cd.getCodeId(networkuser.getNetworkDetails().getRelationshipType().getType(), networkuser.getNetworkDetails().getRelationshipType().getCode()));
				if(pkCode.equals("0") ){
					    this.response=new ResponseHolder();
						this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
						this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Site Type Code Not Found:")); 
						throw new OperationException();
				}
			}
			catch(CodeNotFoundException e)
			{
				System.err.print(e.getMessage());
			}
			
		}
		
		}
     System.out.println("Here");
     if(networkuser.getPK()!=null){
    	int usr=networkuser.getPK();
    	userBeanPk = userAgent.getUserDetails(usr);
		if(userBeanPk==null){
			     this.response=new ResponseHolder();
				this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid User Object is required with User PK.")); 
				throw new OperationException();	
		}else{
			userPK = userBeanPk.getUserId();
		}
	}
				
		if(networkuser.getUserLoginName()!=null){
			UserDao userDao = new UserDao();
			userDao.getUserValuesByLoginName(networkuser.getUserLoginName());
			if(userDao.getUsrIds().size()==0){
				this.response=new ResponseHolder();
				this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid User Object is required with loginName.")); 
				throw new OperationException();	
			}else
			{
				userLoginPK = (Integer)userDao.getUsrIds().get(0);
			}
		}
	
			
	if (networkuser.getOID() != null){
				try{
		    ObjectMap objectMap =((ObjectMapService)params.get("objectMapService")).getObjectMapFromId(networkuser.getOID());
		    if(objectMap == null){
				this.response=new ResponseHolder();
				this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid User Object is required with OID.")); 
				throw new OperationException();
		    }
			if(objectMap != null){
				    UserBean userBeanFromOID = userAgent.getUserDetails(objectMap.getTablePK());
					if (userBeanFromOID != null && userBeanFromOID.getUserAccountId().equals(iaccId)) {
						userOIDPK=userBeanFromOID.getUserId();
					}
				}}catch(MultipleObjectsFoundException e)
				{
					System.err.print(e.getMessage());
				}
				
		}	
	if( userPK>0 && userLoginPK>0 ){
		if( userPK!=userLoginPK ){
			this.response=new ResponseHolder();
			this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
			this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "User PK is Not Matched with User Login Name.")); 
			throw new OperationException();	
		}}
	else if(userPK>0 && userOIDPK>0){
		
		if( userPK!=userOIDPK ){
			this.response=new ResponseHolder();
			this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
			this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "User PK is Not Matched with User OID.")); 
			throw new OperationException();	
		}}
	else if(userOIDPK>0 && userLoginPK>0 ){
		if( userOIDPK!=userLoginPK ){
			this.response=new ResponseHolder();
			this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
			this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "User Name is  Not Matched with User OID.")); 
			throw new OperationException();	
		}
	}
	
	List<NVPair> moredetails=networkuser.getmoreUserDetails();
	Collection<NVPair> moreUserDetails =networkuser.getmoreUserDetails();
	HashMap<String,Object> keySearch=new HashMap<String,Object>();
	
	if(moredetails!=null && moredetails.size()>0){
		 net_pks=getNetPkMoreDetails(networkuser.getmoreUserDetails(),"networkUserDetail");
		networkuser.setmoreUserDetails((java.util.List<NVPair>) moreUserDetails);
		if(moreUserDetails!=null){
			NVPair getDetail=new NVPair();
			for(int iY=0;iY<networkuser.getmoreUserDetails().size();iY++){
				getDetail = networkuser.getmoreUserDetails().set(iY, getDetail);
				keySearch.put("key",getDetail.getKey()==null?"":getDetail.getKey());
				keySearch.put("value", getDetail.getValue()==null?"":getDetail.getValue());
				keySearch.put("user","user");
			}
			moreUserDetails.addAll(moreUserDetails);	
		}
	}
	StringBuffer sqlbuf = new StringBuffer();
	String fk_user="";
	String FK_NWSITES_MAIN="";
	
	if(userOIDPK>0 || userLoginPK>0 || userPK>0 || (!siteName.equals("")) || (!networkPk.equals("")) || (!relationShipPK.equals("")) || (!sitePK.equals("")) || (networkuser.getNetworkDetails() !=null && networkuser.getNetworkDetails().getRelationshipType() != null && networkuser.getNetworkDetails().getRelationshipType().getCode() != null) || (moredetails!=null && moredetails.size()>0)){
	
	 sqlbuf.append(" SELECT distinct(nvl(ns.FK_NWSITES_MAIN,pk_nwsites)) as fk_nwsites_main FROM er_nwsites ns,er_nwusers nu,er_site s WHERE  nu.FK_NWSITES=ns.PK_NWSITES and ns.fk_site=s.pk_site and s.fk_account=? "); 			
 	 	
	if(!(siteName.equals(""))) {
		sqlbuf.append(" and lower(site_name) like lower('%"+siteName+"%')");
		 }
	if(!sitePK.equals("")){
		sqlbuf.append(" and er_nwsites.fk_site="+sitePK+"");
	}
	if(!(pkCode.equals(""))){
		sqlbuf.append(" and nw_membertype="+pkCode+"");
 	 	}
	if(!net_pks.equals("") && !net_pks.equals("0")){
		sqlbuf.append(" and nu.PK_NWUSERS in ("+net_pks+")");
	}
     if(userOIDPK>0 && userLoginPK>0 && userPK>0){
	   sqlbuf.append(" and fk_user="+userPK+"");
	 	}
	 else{
	 	 	if(userOIDPK>0){
	 	 		sqlbuf.append(" and fk_user="+userOIDPK+"");
	 	 	}
	 	 	if(userLoginPK>0){
	 	 		sqlbuf.append(" and fk_user="+userLoginPK+"");
	 	 	}
	 	 	if(userPK>0){
	 	 		sqlbuf.append(" and fk_user="+userPK+"");
	 	 	}
	  }
     if(networkPk.equals("") && relationShipPK.equals("")){
    FK_NWSITES_MAIN = pkMainNetwork(sqlbuf.toString(),iaccId);
     }else{
    	 if(!networkPk.equals("")){
    	 FK_NWSITES_MAIN= networkPk;
    	 }else{
    		 String getPrimaryNetwork="select fk_nwsites_main FROM er_nwsites WHERE pk_nwsites="+ EJBUtil.stringToNum(relationShipPK);
 			try {
 				pstmt = conn.prepareStatement(getPrimaryNetwork);
 				rs=pstmt.executeQuery();
 				while(rs.next()){
 					FK_NWSITES_MAIN=String.valueOf(rs.getInt("fk_nwsites_main"));
 				}
 				System.out.println("new condition pk_main_network==="+FK_NWSITES_MAIN);
 			}catch(Exception e){
 				e.printStackTrace();
 			}
    		 
    	 }
     }
	  }
	System.out.println("FK_NWSITES_MAIN==="+FK_NWSITES_MAIN);
		 System.out.println("sqlbuf==="+sqlbuf.toString());
	StringBuffer sqlbuf1 = new StringBuffer();
		
	 sqlbuf1.append( "SELECT pk_nwsites,fk_user,pk_nwusers,  fk_site,  nw_membertype,site_name,site_name as network_name,er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden,");
     sqlbuf1.append(" er_nwsites.NW_LEVEL+1 AS network_level FROM er_site,er_nwsites,er_nwusers,er_codelst WHERE pk_site=fk_site and pk_codelst=nw_status AND er_site.fk_account=? AND er_nwusers.fk_nwsites=er_nwsites.pk_nwsites");

 	 
 	if(!(networkName.equals(""))){
	 		sqlbuf1.append(" and lower(site_name) like lower('%"+networkName+"%') ");
 	}
 	if(!FK_NWSITES_MAIN.equalsIgnoreCase("") && !FK_NWSITES_MAIN.equals("0")){
 	 	sqlbuf1.append(" and pk_nwsites in ("+FK_NWSITES_MAIN+")");
 	}
 	 System.out.println("sqlbuf1==="+sqlbuf1.toString());
 	 System.out.println("network name=="+networkName);
	List<String> tmpPkNetList=new ArrayList<String>();
	List<String> tmpFkUserList=new ArrayList<String>();
	List<String> tmpFkSiteList=new ArrayList<String>();
	List<String> tmpPkNetUserList=new ArrayList<String>();
	try {
		pstmt = conn.prepareStatement(sqlbuf1.toString());
		pstmt.setInt(1, StringUtil.stringToNum(iaccId));
		rs = pstmt.executeQuery();
		
		while (rs.next()) {
			if(!tmpPkNetList.contains(String.valueOf(rs.getInt("pk_nwsites")))){
				tmpPkNetList.add(String.valueOf(rs.getInt("pk_nwsites")));
				tmpFkSiteList.add(String.valueOf(rs.getInt("fk_site")));
				tmpFkUserList.add(String.valueOf(rs.getInt("fk_user")));
				tmpPkNetUserList.add(String.valueOf(rs.getInt("pk_nwusers")));
				count++;
			}
		}
		if(count==0)
		{
			this.response=new ResponseHolder();
			this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
			this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Data is not found")); 
			throw new OperationException();
		}

	
			//pkNwUserList.add(String.valueOf(rs.getInt("pk_nwusers")));
			NetworkSites site=null;
			SiteIdentifier siteIdentifier=null;	
			NetworkSites sitelvl2=null;
			NetworkSites sitelvl3=null;
			NetworkSites sitelvl4=null;
			NetworkSites sitelvl5=null;
			NetworkSites sitelvl6=null;
			NetworkSites sitelvl7=null;
			
			List<NetworkSites> siteList=null;
			List<NetworkSites> siteslvl2=null;
			List<NetworkSites> siteslvl3=null;
			List<NetworkSites> siteslvl4=null;
			List<NetworkSites> siteslvl5=null;
			List<NetworkSites> siteslvl6=null;
			List<NetworkSites> siteslvl7=null;
			for(int i=0;i<tmpPkNetList.size();i++){
				
				/*site=new NetworkSites();
				siteList=new ArrayList<NetworkSites>();
				siteIdentifier=new SiteIdentifier();
				map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(fkSiteList.get(i)));
				siteIdentifier.setOID(map.getOID());
				siteIdentifier.setPK(Integer.valueOf(fkSiteList.get(i)));
				site.setSiteIdentifier(siteIdentifier);
				site.setName(siteNameList.get(i));
				site.setLevel(String.valueOf(netLevelList.get(i)));
				site.setSiteStatus(netStatList.get(i));
	            fk_user=netUserList.get(i);
				HashMap<String,Object> param=new HashMap<String,Object>();
				param.put("networkuser", networkuser);
				param.put("site", site);
				param.put("keySearch", keySearch);
				param.put("iaccId", iaccId);
				param.put("parent", "parent");
				param.put("fk_user", fk_user);
				
				// calling function for Main Network UserMoreDetails Fields.
				networkUserMoreDetails(EJBUtil.stringToNum(pkNwUserList.get(i)),"user_"+(EJBUtil.stringToNum(netLevelList.get(i))-1),userBean.getUserGrpDefault(),param);
				siteList.add(site);
				//Calling function for Main Network User Roles.
				networkRoles(pkNetList.get(i),fk_user,iaccId,site);*/
				
				
				StringBuffer sqlbuf2 = new StringBuffer();
				sqlbuf2.append("SELECT distinct pk_nwsites,  fk_site,  site_name, ctep_id,   site_name AS network_name, er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden,er_nwsites.NW_LEVEL+1 AS network_level FROM er_nwsites,er_site,er_nwusers,er_codelst WHERE pk_site=fk_site and pk_codelst=nw_status AND er_site.fk_account=? "); 
				if(!(siteName.equals(""))) {
					sqlbuf2.append(" and lower(site_name) like lower('%"+siteName+"%')");
					 }
				if(!sitePK.equals("")){
					sqlbuf2.append(" and er_nwsites.fk_site="+sitePK+"");
				}
				if(!(pkCode.equals(""))){
					sqlbuf2.append(" and nw_membertype="+pkCode+"");
			 	 	}
				if(!net_pks.equals("") && !net_pks.equals("0")){
					sqlbuf2.append(" and PK_NWUSERS in ("+net_pks+")");
				}
				if(!(relationShipPK.equals("")))
				{
					sqlbuf2.append(" and pk_nwsites in ("+relationShipPK+") ");
				}
			     if(userOIDPK>0 && userLoginPK>0 && userPK>0){
				   sqlbuf2.append(" and fk_user="+userPK+"");
				 	}
				 else{
				 	 	if(userOIDPK>0){
				 	 		sqlbuf2.append(" AND pk_nwsites in (select fk_nwsites from er_nwusers where fk_user="+userOIDPK+")");
				 	 	}
				 	 	if(userLoginPK>0){
				 	 		sqlbuf2.append(" AND pk_nwsites in (select fk_nwsites from er_nwusers where fk_user="+userLoginPK+")");
				 	 	}
				 	 	if(userPK>0){
				 	 		sqlbuf2.append(" AND pk_nwsites in (select fk_nwsites from er_nwusers where fk_user="+userPK+")");
				 	 	}
				  }
				sqlbuf2.append(" START WITH er_nwsites.pk_nwsites in (?) CONNECT BY PRIOR er_nwsites.pk_nwsites=er_nwsites.fk_nwsites ORDER SIBLINGS BY lower(site_name)");
				
				System.out.println("sqlbuf2==="+sqlbuf2.toString());
				pstmt = conn.prepareStatement(sqlbuf2.toString());
					pstmt.setInt(1, StringUtil.stringToNum(iaccId));
					pstmt.setInt(2, StringUtil.stringToNum(tmpPkNetList.get(i)));
					rs = pstmt.executeQuery();
					String pkNetList1="";
					siteList=new ArrayList<NetworkSites>();
					siteslvl2=new ArrayList<NetworkSites>();
					siteslvl3=new ArrayList<NetworkSites>();
					siteslvl4=new ArrayList<NetworkSites>();
					siteslvl5=new ArrayList<NetworkSites>();
					siteslvl6=new ArrayList<NetworkSites>();
					siteslvl7=new ArrayList<NetworkSites>();
					if(userOIDPK>0){
			 	 		fk_user=String.valueOf(userOIDPK);
			 	 	}
					else if(userLoginPK>0){
			 	 		fk_user=String.valueOf(userLoginPK);
			 	 	}
					else if(userPK>0){
			 	 		fk_user=String.valueOf(userPK);
			 	 	}else{
			 	 		fk_user="";
			 	 	}
					String fk_site=tmpFkSiteList.get(i);
					String pk_nwusers= "";
					Code code=null;
					String pks_childNtw= "";
					while (rs.next()) {
						if(userOIDPK>0 || userLoginPK>0 || userPK>0 || (!siteName.equals("")) || (!networkPk.equals("")) || (!relationShipPK.equals("")) || (!sitePK.equals("")) || (networkuser.getNetworkDetails() !=null && networkuser.getNetworkDetails().getRelationshipType() != null && networkuser.getNetworkDetails().getRelationshipType().getCode() != null) || (moredetails!=null && moredetails.size()>0)){
							pks_childNtw=pks_childNtw+rs.getInt("pk_nwsites")+",";
						}else{
						System.out.println("Site Name==="+rs.getString("site_name"));
						code=new Code();
						pkNetList1=(String.valueOf(rs.getInt("pk_nwsites")));
						site=new NetworkSites();
						siteIdentifier=new SiteIdentifier();
						map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(rs.getInt("fk_site")));
						siteIdentifier.setOID(map.getOID());
						siteIdentifier.setPK(Integer.valueOf(rs.getInt("fk_site")));
						
						if(rs.getString("network_level").equals("1")){
							
							pkNetList.add(String.valueOf(rs.getInt("pk_nwsites")));
							fkSiteList.add(String.valueOf(rs.getInt("fk_site")));
							netNameList.add(rs.getString("network_name"));
							code.setCode(rs.getString("code"));
							code.setType(rs.getString("stat_type"));
							code.setDescription(rs.getString("description"));
							//code.setHidden(rs.getString("stat_hidden").equals("N")?false:true);
							netStatList.add(code);
							
							//netStatList.add(rs.getString("description"));
							siteNameList.add(rs.getString("site_name"));
							netLevelList.add(String.valueOf(rs.getInt("network_level")));
							
						
							site.setSiteIdentifier(siteIdentifier);
							site.setLevel(String.valueOf(rs.getInt("network_level")));
							site.setName(rs.getString("site_name"));
							
							//code.setHidden(rs.getString("stat_hidden").equals("N")?false:true);
							site.setSiteStatus(code);
							//site.setSiteStatus(rs.getString("description"));	
							HashMap<String,Object> param=new HashMap<String,Object>();
								param.put("networkuser", networkuser);
								param.put("site", site);
								param.put("keySearch", keySearch);
								param.put("iaccId", iaccId);
								pk_nwusers=networkRoles(pkNetList1,fk_user,iaccId,site);
								// calling function for Child Network UserMoreDetails Fields.
								if(!pk_nwusers.equals("")){
									networkUserMoreDetails(Integer.parseInt(pk_nwusers),"user_"+(rs.getInt("network_level")-1),userBean.getUserGrpDefault(),param);
								}
								
								//Calling funcion for Child Network User Roles.
								
							
						}
						if(rs.getInt("network_level")==2){
							sitelvl2=new NetworkSites();
							siteslvl3=new ArrayList<NetworkSites>();
							sitelvl2.setSiteIdentifier(siteIdentifier);
							sitelvl2.setLevel(String.valueOf(rs.getInt("network_level")));
							sitelvl2.setName(rs.getString("site_name"));
							sitelvl2.setSiteStatus(code);
							sitelvl2.setCTEP_ID(rs.getString("ctep_id"));
							if(rs.getString("code")!=null){
							code=new Code();
							code.setCode(rs.getString("code"));
							code.setType(rs.getString("stat_type"));
							code.setDescription(rs.getString("description"));
							sitelvl2.setSiteRelationShipType(code);
							}
							if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
								sitelvl2.setRelationship_PK(rs.getString("pk_nwsites"));
							}
							HashMap<String,Object> param=new HashMap<String,Object>();
							param.put("networkuser", networkuser);
							param.put("site", site);
							param.put("keySearch", keySearch);
							param.put("iaccId", iaccId);
							pk_nwusers=networkRoles(pkNetList1,fk_user,iaccId,sitelvl2);
							// calling function for Child Network UserMoreDetails Fields.
							if(!pk_nwusers.equals("")){
								networkUserMoreDetails(Integer.parseInt(pk_nwusers),"user_"+(rs.getInt("network_level")-1),userBean.getUserGrpDefault(),param);
							}
							siteslvl2.add(sitelvl2);
							site.setSite(siteslvl2);		
						}
						else if(rs.getInt("network_level")==3){
							sitelvl3=new NetworkSites();
							siteslvl4=new ArrayList<NetworkSites>();
							sitelvl3.setSiteIdentifier(siteIdentifier);
							sitelvl3.setLevel(String.valueOf(rs.getInt("network_level")));
							sitelvl3.setName(rs.getString("site_name"));
							sitelvl3.setSiteStatus(code);
							sitelvl3.setCTEP_ID(rs.getString("ctep_id"));
							if(rs.getString("code")!=null){
							code=new Code();
							code.setCode(rs.getString("code"));
							code.setType(rs.getString("stat_type"));
							code.setDescription(rs.getString("description"));
							sitelvl3.setSiteRelationShipType(code);
							}
							if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
								sitelvl3.setRelationship_PK(rs.getString("pk_nwsites"));
							}
							HashMap<String,Object> param=new HashMap<String,Object>();
							param.put("networkuser", networkuser);
							param.put("site", site);
							param.put("keySearch", keySearch);
							param.put("iaccId", iaccId);
							pk_nwusers=networkRoles(pkNetList1,fk_user,iaccId,sitelvl3);
							// calling function for Child Network UserMoreDetails Fields.
							if(!pk_nwusers.equals("")){
								networkUserMoreDetails(Integer.parseInt(pk_nwusers),"user_"+(rs.getInt("network_level")-1),userBean.getUserGrpDefault(),param);
							}
							siteslvl3.add(sitelvl3);
							sitelvl2.setSite(siteslvl3);
						}
						else if(rs.getInt("network_level")==4){
							sitelvl4=new NetworkSites();
							siteslvl5=new ArrayList<NetworkSites>();
							sitelvl4.setSiteIdentifier(siteIdentifier);
							sitelvl4.setLevel(String.valueOf(rs.getInt("network_level")));
							sitelvl4.setName(rs.getString("site_name"));
							sitelvl4.setSiteStatus(code);
							sitelvl4.setCTEP_ID(rs.getString("ctep_id"));
							if(rs.getString("code")!=null){
							code=new Code();
							code.setCode(rs.getString("code"));
							code.setType(rs.getString("stat_type"));
							code.setDescription(rs.getString("description"));
							sitelvl4.setSiteRelationShipType(code);
							}
							if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
								sitelvl4.setRelationship_PK(rs.getString("pk_nwsites"));
							}
							HashMap<String,Object> param=new HashMap<String,Object>();
							param.put("networkuser", networkuser);
							param.put("site", site);
							param.put("keySearch", keySearch);
							param.put("iaccId", iaccId);
							pk_nwusers=networkRoles(pkNetList1,fk_user,iaccId,sitelvl4);
							// calling function for Child Network UserMoreDetails Fields.
							if(!pk_nwusers.equals("")){
								networkUserMoreDetails(Integer.parseInt(pk_nwusers),"user_"+(rs.getInt("network_level")-1),userBean.getUserGrpDefault(),param);
							}
							siteslvl4.add(sitelvl4);
							sitelvl3.setSite(siteslvl4);
						}
						else if(rs.getInt("network_level")==5){
								sitelvl5=new NetworkSites();
								siteslvl6=new ArrayList<NetworkSites>();
								sitelvl5.setSiteIdentifier(siteIdentifier);
								sitelvl5.setLevel(String.valueOf(rs.getInt("network_level")));
								sitelvl5.setName(rs.getString("site_name"));
								sitelvl5.setSiteStatus(code);
								sitelvl5.setCTEP_ID(rs.getString("ctep_id"));
								if(rs.getString("code")!=null){
								code=new Code();
								code.setCode(rs.getString("code"));
								code.setType(rs.getString("stat_type"));
								code.setDescription(rs.getString("description"));
								sitelvl5.setSiteRelationShipType(code);
								}
								if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
									sitelvl5.setRelationship_PK(rs.getString("pk_nwsites"));
								}
								HashMap<String,Object> param=new HashMap<String,Object>();
								param.put("networkuser", networkuser);
								param.put("site", site);
								param.put("keySearch", keySearch);
								param.put("iaccId", iaccId);
								pk_nwusers=networkRoles(pkNetList1,fk_user,iaccId,sitelvl5);
								// calling function for Child Network UserMoreDetails Fields.
								if(!pk_nwusers.equals("")){
									networkUserMoreDetails(Integer.parseInt(pk_nwusers),"user_"+(rs.getInt("network_level")-1),userBean.getUserGrpDefault(),param);
								}
								siteslvl5.add(sitelvl5);
								sitelvl4.setSite(siteslvl5);
						}
						else if(rs.getInt("network_level")==6){
							sitelvl6=new NetworkSites();
							siteslvl7=new ArrayList<NetworkSites>();
							sitelvl6.setSiteIdentifier(siteIdentifier);
							sitelvl6.setLevel(String.valueOf(rs.getInt("network_level")));
							sitelvl6.setName(rs.getString("site_name"));
							sitelvl6.setSiteStatus(code);
							sitelvl6.setCTEP_ID(rs.getString("ctep_id"));
							if(rs.getString("code")!=null){
							code=new Code();
							code.setCode(rs.getString("code"));
							code.setType(rs.getString("stat_type"));
							code.setDescription(rs.getString("description"));
							sitelvl6.setSiteRelationShipType(code);
							}
							if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
								sitelvl6.setRelationship_PK(rs.getString("pk_nwsites"));
							}
							HashMap<String,Object> param=new HashMap<String,Object>();
							param.put("networkuser", networkuser);
							param.put("site", site);
							param.put("keySearch", keySearch);
							param.put("iaccId", iaccId);
							pk_nwusers=networkRoles(pkNetList1,fk_user,iaccId,sitelvl6);
							// calling function for Child Network UserMoreDetails Fields.
							if(!pk_nwusers.equals("")){
								networkUserMoreDetails(Integer.parseInt(pk_nwusers),"user_"+(rs.getInt("network_level")-1),userBean.getUserGrpDefault(),param);
							}
							siteslvl6.add(sitelvl6);
							sitelvl5.setSite(siteslvl6);
						}
						else if(rs.getInt("network_level")==7){
							sitelvl7=new NetworkSites();
							sitelvl7.setSiteIdentifier(siteIdentifier);
							sitelvl7.setLevel(String.valueOf(rs.getInt("network_level")));
							sitelvl7.setName(rs.getString("site_name"));
							sitelvl7.setSiteStatus(code);
							sitelvl7.setCTEP_ID(rs.getString("ctep_id"));
							if(rs.getString("code")!=null){
							code=new Code();
							code.setCode(rs.getString("code"));
							code.setType(rs.getString("stat_type"));
							code.setDescription(rs.getString("description"));
							sitelvl7.setSiteRelationShipType(code);
							}
							if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
								sitelvl7.setRelationship_PK(rs.getString("pk_nwsites"));
							}
							HashMap<String,Object> param=new HashMap<String,Object>();
							param.put("networkuser", networkuser);
							param.put("site", site);
							param.put("keySearch", keySearch);
							param.put("iaccId", iaccId);
							pk_nwusers=networkRoles(pkNetList1,fk_user,iaccId,sitelvl7);
							// calling function for Child Network UserMoreDetails Fields.
							if(!pk_nwusers.equals("")){
								networkUserMoreDetails(Integer.parseInt(pk_nwusers),"user_"+(rs.getInt("network_level")-1),userBean.getUserGrpDefault(),param);
							}
							siteslvl7.add(sitelvl7);
							sitelvl6.setSite(siteslvl7);
						}
						
						}
						pks_childNtw=pks_childNtw.length()>0?pks_childNtw.substring(0,pks_childNtw.length()-1):"0";
						if(!pks_childNtw.equals("0") && !pks_childNtw.equals(""))
						{
							StringBuffer sqlbuf3 = new StringBuffer();
							sqlbuf3.append("SELECT distinct pk_nwsites,  fk_site,  site_name, ctep_id,   site_name AS network_name, er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden,er_nwsites.NW_LEVEL+1 AS network_level FROM er_nwsites,er_site,er_nwusers,er_codelst WHERE pk_site=fk_site and pk_codelst=nw_status AND er_site.fk_account=? "); 
							sqlbuf3.append(" START WITH er_nwsites.pk_nwsites in ("+pks_childNtw+") CONNECT BY PRIOR er_nwsites.fk_nwsites=er_nwsites.pk_nwsites ORDER BY network_level");
							System.out.println("sqlbuf3==="+sqlbuf3.toString());
							pstmt = conn.prepareStatement(sqlbuf3.toString());
								pstmt.setInt(1, StringUtil.stringToNum(iaccId));
								rs = pstmt.executeQuery();
								pkNetList1="";
								siteList=new ArrayList<NetworkSites>();
								siteslvl2=new ArrayList<NetworkSites>();
								siteslvl3=new ArrayList<NetworkSites>();
								siteslvl4=new ArrayList<NetworkSites>();
								siteslvl5=new ArrayList<NetworkSites>();
								siteslvl6=new ArrayList<NetworkSites>();
								siteslvl7=new ArrayList<NetworkSites>();
								
								pk_nwusers= "";
								code=null;
								pks_childNtw= "";
								site=new NetworkSites();
								while (rs.next()) {
									System.out.println("Site Name==="+rs.getString("site_name"));
									code=new Code();
									pkNetList1=(String.valueOf(rs.getInt("pk_nwsites")));
									fk_site=String.valueOf(rs.getInt("fk_site"));
									siteIdentifier=new SiteIdentifier();
									map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(rs.getInt("fk_site")));
									siteIdentifier.setOID(map.getOID());
									siteIdentifier.setPK(Integer.valueOf(rs.getInt("fk_site")));
									
									if(rs.getString("network_level").equals("1")){
										
										pkNetList.add(String.valueOf(rs.getInt("pk_nwsites")));
										fkSiteList.add(String.valueOf(rs.getInt("fk_site")));
										netNameList.add(rs.getString("network_name"));
										code.setCode(rs.getString("code"));
										code.setType(rs.getString("stat_type"));
										code.setDescription(rs.getString("description"));
										//code.setHidden(rs.getString("stat_hidden").equals("N")?false:true);
										netStatList.add(code);
										
										//netStatList.add(rs.getString("description"));
										siteNameList.add(rs.getString("site_name"));
										netLevelList.add(String.valueOf(rs.getInt("network_level")));
										
									
										site.setSiteIdentifier(siteIdentifier);
										site.setLevel(String.valueOf(rs.getInt("network_level")));
										site.setName(rs.getString("site_name"));
										
										//code.setHidden(rs.getString("stat_hidden").equals("N")?false:true);
										site.setSiteStatus(code);
										//site.setSiteStatus(rs.getString("description"));	
										HashMap<String,Object> param=new HashMap<String,Object>();
										param.put("networkuser", networkuser);
										param.put("site", site);
										param.put("keySearch", keySearch);
										param.put("iaccId", iaccId);
										pk_nwusers=networkRoles(pkNetList1,fk_user,iaccId,site);
										// calling function for Child Network UserMoreDetails Fields.
										if(!pk_nwusers.equals("")){
											networkUserMoreDetails(Integer.parseInt(pk_nwusers),"user_"+(rs.getInt("network_level")-1),userBean.getUserGrpDefault(),param);
										}
										
									}
									if(rs.getInt("network_level")==2){
										sitelvl2=new NetworkSites();
										siteslvl3=new ArrayList<NetworkSites>();
										sitelvl2.setSiteIdentifier(siteIdentifier);
										sitelvl2.setLevel(String.valueOf(rs.getInt("network_level")));
										sitelvl2.setName(rs.getString("site_name"));
										sitelvl2.setSiteStatus(code);
										sitelvl2.setCTEP_ID(rs.getString("ctep_id"));
										if(rs.getString("code")!=null){
										code=new Code();
										code.setCode(rs.getString("code"));
										code.setType(rs.getString("stat_type"));
										code.setDescription(rs.getString("description"));
										sitelvl2.setSiteRelationShipType(code);
										}
										if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
											sitelvl2.setRelationship_PK(rs.getString("pk_nwsites"));
										}
										System.out.println("Out");
										HashMap<String,Object> param=new HashMap<String,Object>();
										param.put("networkuser", networkuser);
										param.put("site", site);
										param.put("keySearch", keySearch);
										param.put("iaccId", iaccId);
										pk_nwusers=networkRoles(pkNetList1,fk_user,iaccId,sitelvl2);
										// calling function for Child Network UserMoreDetails Fields.
										if(!pk_nwusers.equals("")){
											networkUserMoreDetails(Integer.parseInt(pk_nwusers),"user_"+(rs.getInt("network_level")-1),userBean.getUserGrpDefault(),param);
										}
										siteslvl2.add(sitelvl2);
										site.setSite(siteslvl2);		
									}
									else if(rs.getInt("network_level")==3){
										sitelvl3=new NetworkSites();
										siteslvl4=new ArrayList<NetworkSites>();
										sitelvl3.setSiteIdentifier(siteIdentifier);
										sitelvl3.setLevel(String.valueOf(rs.getInt("network_level")));
										sitelvl3.setName(rs.getString("site_name"));
										sitelvl3.setSiteStatus(code);
										sitelvl3.setCTEP_ID(rs.getString("ctep_id"));
										if(rs.getString("code")!=null){
										code=new Code();
										code.setCode(rs.getString("code"));
										code.setType(rs.getString("stat_type"));
										code.setDescription(rs.getString("description"));
										sitelvl3.setSiteRelationShipType(code);
										}
										if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
											sitelvl3.setRelationship_PK(rs.getString("pk_nwsites"));
										}
										System.out.println("Out");
										HashMap<String,Object> param=new HashMap<String,Object>();
										param.put("networkuser", networkuser);
										param.put("site", site);
										param.put("keySearch", keySearch);
										param.put("iaccId", iaccId);
										pk_nwusers=networkRoles(pkNetList1,fk_user,iaccId,sitelvl3);
										// calling function for Child Network UserMoreDetails Fields.
										if(!pk_nwusers.equals("")){
											networkUserMoreDetails(Integer.parseInt(pk_nwusers),"user_"+(rs.getInt("network_level")-1),userBean.getUserGrpDefault(),param);
										}
										siteslvl3.add(sitelvl3);
										sitelvl2.setSite(siteslvl3);
									}
									else if(rs.getInt("network_level")==4){
										sitelvl4=new NetworkSites();
										siteslvl5=new ArrayList<NetworkSites>();
										sitelvl4.setSiteIdentifier(siteIdentifier);
										sitelvl4.setLevel(String.valueOf(rs.getInt("network_level")));
										sitelvl4.setName(rs.getString("site_name"));
										sitelvl4.setSiteStatus(code);
										sitelvl4.setCTEP_ID(rs.getString("ctep_id"));
										if(rs.getString("code")!=null){
										code=new Code();
										code.setCode(rs.getString("code"));
										code.setType(rs.getString("stat_type"));
										code.setDescription(rs.getString("description"));
										sitelvl4.setSiteRelationShipType(code);
										}
										if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
											sitelvl4.setRelationship_PK(rs.getString("pk_nwsites"));
										}
										HashMap<String,Object> param=new HashMap<String,Object>();
										param.put("networkuser", networkuser);
										param.put("site", site);
										param.put("keySearch", keySearch);
										param.put("iaccId", iaccId);
										pk_nwusers=networkRoles(pkNetList1,fk_user,iaccId,sitelvl4);
										// calling function for Child Network UserMoreDetails Fields.
										if(!pk_nwusers.equals("")){
											networkUserMoreDetails(Integer.parseInt(pk_nwusers),"user_"+(rs.getInt("network_level")-1),userBean.getUserGrpDefault(),param);
										}
										siteslvl4.add(sitelvl4);
										sitelvl3.setSite(siteslvl4);
									}
									else if(rs.getInt("network_level")==5){
											sitelvl5=new NetworkSites();
											siteslvl6=new ArrayList<NetworkSites>();
											sitelvl5.setSiteIdentifier(siteIdentifier);
											sitelvl5.setLevel(String.valueOf(rs.getInt("network_level")));
											sitelvl5.setName(rs.getString("site_name"));
											sitelvl5.setSiteStatus(code);
											sitelvl5.setCTEP_ID(rs.getString("ctep_id"));
											if(rs.getString("code")!=null){
											code=new Code();
											code.setCode(rs.getString("code"));
											code.setType(rs.getString("stat_type"));
											code.setDescription(rs.getString("description"));
											sitelvl5.setSiteRelationShipType(code);
											}
											if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
												sitelvl5.setRelationship_PK(rs.getString("pk_nwsites"));
											}
											HashMap<String,Object> param=new HashMap<String,Object>();
											param.put("networkuser", networkuser);
											param.put("site", site);
											param.put("keySearch", keySearch);
											param.put("iaccId", iaccId);
											pk_nwusers=networkRoles(pkNetList1,fk_user,iaccId,sitelvl5);
											// calling function for Child Network UserMoreDetails Fields.
											if(!pk_nwusers.equals("")){
												networkUserMoreDetails(Integer.parseInt(pk_nwusers),"user_"+(rs.getInt("network_level")-1),userBean.getUserGrpDefault(),param);
											}
											siteslvl5.add(sitelvl5);
											sitelvl4.setSite(siteslvl5);
									}
									else if(rs.getInt("network_level")==6){
										sitelvl6=new NetworkSites();
										siteslvl7=new ArrayList<NetworkSites>();
										sitelvl6.setSiteIdentifier(siteIdentifier);
										sitelvl6.setLevel(String.valueOf(rs.getInt("network_level")));
										sitelvl6.setName(rs.getString("site_name"));
										sitelvl6.setSiteStatus(code);
										sitelvl6.setCTEP_ID(rs.getString("ctep_id"));
										if(rs.getString("code")!=null){
										code=new Code();
										code.setCode(rs.getString("code"));
										code.setType(rs.getString("stat_type"));
										code.setDescription(rs.getString("description"));
										sitelvl6.setSiteRelationShipType(code);
										}
										if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
											sitelvl6.setRelationship_PK(rs.getString("pk_nwsites"));
										}
										HashMap<String,Object> param=new HashMap<String,Object>();
										param.put("networkuser", networkuser);
										param.put("site", site);
										param.put("keySearch", keySearch);
										param.put("iaccId", iaccId);
										pk_nwusers=networkRoles(pkNetList1,fk_user,iaccId,sitelvl6);
										// calling function for Child Network UserMoreDetails Fields.
										if(!pk_nwusers.equals("")){
											networkUserMoreDetails(Integer.parseInt(pk_nwusers),"user_"+(rs.getInt("network_level")-1),userBean.getUserGrpDefault(),param);
										}
										siteslvl6.add(sitelvl6);
										sitelvl5.setSite(siteslvl6);
									}
									else if(rs.getInt("network_level")==7){
										sitelvl7=new NetworkSites();
										sitelvl7.setSiteIdentifier(siteIdentifier);
										sitelvl7.setLevel(String.valueOf(rs.getInt("network_level")));
										sitelvl7.setName(rs.getString("site_name"));
										sitelvl7.setSiteStatus(code);
										sitelvl7.setCTEP_ID(rs.getString("ctep_id"));
										if(rs.getString("code")!=null){
										code=new Code();
										code.setCode(rs.getString("code"));
										code.setType(rs.getString("stat_type"));
										code.setDescription(rs.getString("description"));
										sitelvl7.setSiteRelationShipType(code);
										}
										if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
											sitelvl7.setRelationship_PK(rs.getString("pk_nwsites"));
										}
										HashMap<String,Object> param=new HashMap<String,Object>();
										param.put("networkuser", networkuser);
										param.put("site", site);
										param.put("keySearch", keySearch);
										param.put("iaccId", iaccId);
										pk_nwusers=networkRoles(pkNetList1,fk_user,iaccId,sitelvl7);
										// calling function for Child Network UserMoreDetails Fields.
										if(!pk_nwusers.equals("")){
											networkUserMoreDetails(Integer.parseInt(pk_nwusers),"user_"+(rs.getInt("network_level")-1),userBean.getUserGrpDefault(),param);
										}
										siteslvl7.add(sitelvl7);
										sitelvl6.setSite(siteslvl7);
									}

								}
						}
						siteList.add(site);
			}
						
					UsersitesList.add((ArrayList<NetworkSites>) siteList);
					
				
			
			}
	}catch(OperationException ex){
		throw ex;
	}
	catch (Exception e) {
		e.printStackTrace();
	} finally {
		try {
			rs.close();
			if(rs1 != null){
			rs1.close();
		     }
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
	
	public String getNetPkMoreDetails(List<NVPair> moredetails,String flag){
		
		Connection conn=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		String key="";
		String value="";
		String net_pks="";
		String sql="";

		try{
			conn = CommonDAO.getConnection();
			for(NVPair nvpair:moredetails){
				key=nvpair.getKey();
				
				ElementNSImpl val=(ElementNSImpl)nvpair.getValue();
				if(val!=null){
				NodeList node= val.getChildNodes();
		        for(int j=0;j< node.getLength();j++)
		           {
		        	  Node node1=node.item(j);
		        	  value=node1.getNodeValue();
		           }
				}
				StringBuffer sqlbuf = new StringBuffer();
					 
						 sqlbuf.append("SELECT fk_modpk FROM ER_MOREDETAILS,ER_CODELST WHERE MD_MODELEMENTPK=PK_CODELST AND CODELST_SUBTYP=? and codelst_type");
						 
						if(flag.equalsIgnoreCase("siteDetails")){
							 sqlbuf.append(" like 'org/__' escape '/' ");
						 }
						 if(flag.equalsIgnoreCase("networkUserDetail")){
							 sqlbuf.append(" like 'user/__' escape '/' ");
						 }
						 if(!value.equalsIgnoreCase("")){
						 sqlbuf.append(" AND  lower(MD_MODELEMENTDATA) LIKE lower('%"+value+"%')");
						 }
				System.out.println("Inside getNetPkMoreDetails sqlbuf==="+sqlbuf.toString());
				System.out.println("Key==="+key);
		        pstmt = conn.prepareStatement(sqlbuf.toString());
				pstmt.setString(1,key);
				rs = pstmt.executeQuery();
				while(rs.next()){
					net_pks=net_pks+rs.getString("fk_modpk")+",";
				}	
			}
			net_pks=net_pks.length()>0?net_pks.substring(0,net_pks.length()-1):"0";
			}
			catch(SQLException ex){
				ex.printStackTrace();
			}
			finally{
				try {
					rs.close();
					pstmt.close();
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		
		return net_pks;
	}
	public String pkMainNetwork(String sql,String iaccId){
		
		Connection conn=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		String pk_main_network="";
		try {
			conn = CommonDAO.getConnection();
			pstmt = conn.prepareStatement(sql);
			if(iaccId != null){
			pstmt.setInt(1, StringUtil.stringToNum(iaccId));
			}
			rs = pstmt.executeQuery();
		
			while (rs.next()) {
				pk_main_network=pk_main_network+rs.getInt("fk_nwsites_main")+",";
			}
			pk_main_network=pk_main_network.length()>0?pk_main_network.substring(0,pk_main_network.length()-1):"0";
		}catch(SQLException ex){
			ex.printStackTrace();
		}
		finally{
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return pk_main_network;
	}
	
	
public String networkRoles(String pkNetwork,String fk_user,String iaccId,NetworkSites site){
		
		Connection conn=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		String pkNwUsers = "";
		try {
			conn = CommonDAO.getConnection();

			StringBuffer sqlbuff = new StringBuffer();
			sqlbuff.append("SELECT pk_nwsites,pk_nwusers,fk_user,(select codelst_subtyp from er_codelst where pk_codelst=NWU_MEMBERTROLE) as role_subtype, NWU_MEMBERTROLE as role_PK, f_codelst_desc(NWU_MEMBERTROLE) AS role_name,cd.CODELST_DESC   AS description,cd.codelst_type   AS stat_type, cd.codelst_subtyp AS code,  cd.codelst_hide   AS stat_hidden FROM er_nwusers nwu, er_nwsites ns,er_site s,er_codelst cd WHERE nwu.FK_NWSITES=ns.PK_NWSITES and ns.PK_NWSITES=? AND  s.FK_ACCOUNT=? AND s.PK_SITE=ns.FK_SITE AND pk_codelst=nwu.NWU_STATUS  ");
			if(!fk_user.equalsIgnoreCase("")){
			sqlbuff.append(" and nwu.FK_USER= "+fk_user+"");
			}
			System.out.println("1st==="+sqlbuff.toString());
			System.out.println("pkNetwork==="+pkNetwork);
			pstmt = conn.prepareStatement(sqlbuff.toString());
			pstmt.setInt(1, StringUtil.stringToNum(pkNetwork));
			pstmt.setInt(2, StringUtil.stringToNum(iaccId));
			rs=pstmt.executeQuery();
			
			NetworkRole role=null;
			Roles roles=new Roles();
			Code code=null;
			List<NetworkRole> rolelist=new ArrayList<NetworkRole>();
			
			while(rs.next()){
				 code=new Code();
				 pkNwUsers= String.valueOf(rs.getInt("pk_nwusers"));
			    role=new NetworkRole();
			    role.setName(rs.getString("role_name"));
			    role.setPK(String.valueOf(rs.getInt("role_PK")));
			    role.setSubType(rs.getString("role_subtype"));
			    code.setCode(rs.getString("code"));
				code.setType(rs.getString("stat_type"));
				code.setDescription(rs.getString("description"));
				role.setRoleStatus(code);
				rolelist.add(role);		
			}
			if(!pkNwUsers.equals("")){
			StringBuffer sqlbuff1 = new StringBuffer();
			
			sqlbuff1.append("SELECT f_codelst_desc(NWU_MEMBERADDLTROLE) AS role_name,(select codelst_subtyp from er_codelst where pk_codelst=NWU_MEMBERADDLTROLE) as role_subtype, NWU_MEMBERADDLTROLE as role_PK, cd.CODELST_DESC AS description,  cd.codelst_type AS stat_type,  cd.codelst_subtyp AS code,  cd.codelst_hide AS stat_hidden FROM ER_NWUSERS_ADDNLROLES nwadr, er_codelst cd WHERE nwadr.FK_NWUSERS=?	AND cd.pk_codelst      =nwadr.NWU_ADTSTATUS");
			
			pstmt = conn.prepareStatement(sqlbuff1.toString());
			pstmt.setInt(1, StringUtil.stringToNum(pkNwUsers));
			rs=pstmt.executeQuery();
			System.out.println("2nd==="+sqlbuff.toString());
			System.out.println("pkNwUsers==="+pkNwUsers);
			while(rs.next()){
				 code=new Code();
			    role=new NetworkRole();
			    role.setName(rs.getString("role_name"));
			    role.setPK(String.valueOf(rs.getInt("role_PK")));
			    role.setSubType(rs.getString("role_subtype"));
			    code.setCode(rs.getString("code"));
				code.setType(rs.getString("stat_type"));
				code.setDescription(rs.getString("description"));
				role.setRoleStatus(code);
				rolelist.add(role);		
			}
			}
			roles.setRoles(rolelist);
			site.setRoles(roles);
				
			
			
		}catch(SQLException ex){
			ex.printStackTrace();
		}
		finally{
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return pkNwUsers;
		
	}

public void networkUserMoreDetails(int pkNWuser,String modName,String defUserGroup,HashMap<String,Object> param) throws OperationException{
	
	MoreDetailsJB mdJB=null;
	MoreDetailsDao mdDao;
	List<NVPair> listNVPair=new ArrayList<NVPair>();
	NVPair nvpair = null;
	ArrayList idList = new ArrayList();
	ArrayList modElementDescList = new ArrayList();
	ArrayList modElementDataList = new ArrayList();
	ArrayList modElementKeysList = new ArrayList();
	NetworkUserDetail networkuser=(NetworkUserDetail) param.get("networkuser");
	String iaccId = (String)param.get("iaccId");
	NetworkSites site =(NetworkSites) param.get("site");
	HashMap<String,Object> KeySearch=(HashMap<String, Object>) param.get("keySearch");
	Collection<NVPair> moreUserDetails =networkuser.getmoreUserDetails();
	System.out.println("Outside If");
	if(moreUserDetails==null){
		System.out.println("Inside If");
		mdJB = new MoreDetailsJB();
		mdDao = new MoreDetailsDao();
		System.out.println("pkNWuser==="+pkNWuser);
		System.out.println("modName==="+modName);
		System.out.println("defUserGroup==="+defUserGroup);
		mdDao = mdJB.getMoreDetails(pkNWuser,modName,defUserGroup);
		idList = mdDao.getId();
		modElementDescList = mdDao.getMdElementDescs();
		modElementDataList = mdDao.getMdElementValues();
		modElementKeysList = mdDao.getMdElementKeys();
		for(int iY=0; iY<idList.size(); iY++){
			if((Integer)idList.get(iY)>0){
			nvpair = new NVPair();
			nvpair.setKey((String) modElementKeysList.get(iY));
			nvpair.setValue((String)modElementDataList.get(iY));
			listNVPair.add(nvpair);
			}
		}
		}
		else{
			
			listNVPair=new UserServiceImpl().getNetworkMoreDetailValue(EJBUtil.integerToString(pkNWuser),KeySearch,StringUtil.stringToNum(modName.substring(modName.lastIndexOf("_")+1)),iaccId);
		}
		site.setUserDetailsFields(listNVPair);
	
	
}

@SuppressWarnings("resource")
public void searchNetworkUsersDetails(NetworkUserDetails networkuser, HashMap<String, Object> params) throws OperationException {
	Connection conn=null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	ResultSet rs1=null;
	int userPkCountN=0;
	int userPkCountS=0;
	String pkCode="";
	int count=0;
	String net_pks="";
	ObjectMap map=null;
	UserBean userBean = null;
	String networkName="";
	String siteName="";
	String networkPK = "";
	String sitePK = "";
	String relationShipPK = "";
	UserAgentRObj userAgent = (UserAgentRObj) params.get("userAgent");
	String iaccId = (String)params.get(KEY_ACCOUNT_ID);
	String CTEP_ID="";
	
	String relation_pks="";
	
	if((networkuser.getUserPK()!=null && !networkuser.getUserPK().equals("")) || 
			(networkuser.getUserLoginName()!=null && !networkuser.getUserLoginName().equals("")) || 
			(networkuser.getRolePK()!=null && !networkuser.getRolePK().equals(""))){
		relation_pks=getRelationPksFromUser(networkuser);
		if(relation_pks.equals("")){
			this.response=new ResponseHolder();
			this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "User or Role Identifier Not Found")); 
			throw new OperationException();
		}
	}
	System.out.println("relation_pks==="+relation_pks);
	if(networkuser.getNetworkDetails()!=null){
		
    if(networkuser.getNetworkDetails().getNetworkName() != null ){
    	networkName = networkuser.getNetworkDetails().getNetworkName()==null?"":networkuser.getNetworkDetails().getNetworkName();
    }
    if(networkuser.getNetworkDetails().getNetworkPK() != null ){
    	networkPK = networkuser.getNetworkDetails().getNetworkPK()==null?"":networkuser.getNetworkDetails().getNetworkPK();
    	System.out.println("networkPK=="+networkPK);
    }
    if(networkuser.getNetworkDetails().getSitePK() != null ){
    	sitePK = networkuser.getNetworkDetails().getSitePK()==null?"":networkuser.getNetworkDetails().getSitePK();
    }
    if(networkuser.getNetworkDetails().getRelationshipPK() != null ){
    	relationShipPK = networkuser.getNetworkDetails().getRelationshipPK()==null?"":networkuser.getNetworkDetails().getRelationshipPK();
    }
    if(networkuser.getNetworkDetails().getSiteName() != null ){
    	siteName = networkuser.getNetworkDetails().getSiteName()==null?"":networkuser.getNetworkDetails().getSiteName();
    }
    if(networkuser.getNetworkDetails().getCTEP_ID() != null ){
    	CTEP_ID = networkuser.getNetworkDetails().getCTEP_ID()==null?"":networkuser.getNetworkDetails().getCTEP_ID();
    }
	if(networkuser.getNetworkDetails().getRelationshipType() != null && networkuser.getNetworkDetails().getRelationshipType().getCode() != null)
	{
		 CodeDao cd = new CodeDao();
		 pkCode = StringUtil.integerToString(cd.getCodeId(networkuser.getNetworkDetails().getRelationshipType().getType(), networkuser.getNetworkDetails().getRelationshipType().getCode()));
	}
	if(networkName.equals("") && networkPK.equals("")){
		this.response=new ResponseHolder();
		this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
		this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Network Identifier is Required.")); 
		throw new OperationException();
	}
	}
 
 if(networkuser.getMoreNetworkUserDetails()!=null){
	 net_pks=getNetPkMoreNetworkUsersDetails(networkuser.getMoreNetworkUserDetails(),"networkUserDetail");
 }
 if(!relationShipPK.equals("") && !relation_pks.equals("")){
	 relationShipPK=relationShipPK+","+relation_pks;
 }else if(!relation_pks.equals("")){
	 relationShipPK=relation_pks;
 }
 System.out.println("relationShipPK==="+relationShipPK);
 if(networkName.equals("") && networkPK.equals("") && sitePK.equals("") && relationShipPK.equals("") && siteName.equals("") && (pkCode.equals("")) && (networkuser.getMoreNetworkUserDetails() == null)){
	this.response=new ResponseHolder();
	this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
	this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid data is Required.")); 
	throw new OperationException();
 }     
			
	

StringBuffer sqlbuf = new StringBuffer();
String fk_user="";
String FK_NWSITES_MAIN="";
String str="";
String pk_main_network="";

if(!(siteName.equals("")) || !(sitePK.equals("")) || (!(pkCode.equals(""))) || !(networkPK.equals("")) || !(relationShipPK.equals(""))){
	str="SELECT distinct nvl(FK_NWSITES_MAIN,pk_nwsites) fk_nwsites_main FROM er_nwsites,er_site WHERE pk_site=fk_site and er_site.fk_account=? ";
	
	if(!(siteName.equals(""))){
		str=str+" and lower(site_name) like lower('%"+siteName+"%') ";
	}
	if(!(sitePK.equals(""))){
		str=str+" and fk_site="+sitePK;
	}
	if(!(CTEP_ID.equals(""))){
		str=str+" and lower(ctep_id) like lower('%"+CTEP_ID+"%') ";
	}
	if(!(pkCode.equals("0")) && !(pkCode.equals(""))){
		str=str+" and nw_membertype="+pkCode;
		}
	System.out.println("Inside networkPK==="+networkPK);
	if(networkPK.equals("") && relationShipPK.equals("")){
		pk_main_network = pkMainNetwork(str,iaccId);	
	}else{
		if(!networkPK.equals("")){
			pk_main_network=networkPK;
			System.out.println("pk_main_network Inside If==="+pk_main_network);
		}else{
			
			String getPrimaryNetwork="select rowtocol('select distinct nvl(fk_nwsites_main,pk_nwsites) FROM er_nwsites WHERE pk_nwsites in ("+relationShipPK+")') as fk_nwsites_main from dual";
			System.out.println("getPrimaryNetwork sql==="+getPrimaryNetwork);
 			try {
 				conn = CommonDAO.getConnection();
 				pstmt = conn.prepareStatement(getPrimaryNetwork);
 				rs=pstmt.executeQuery();
 				while(rs.next()){
 					if(rs.getString("fk_nwsites_main")!=null){
 						pk_main_network=rs.getString("fk_nwsites_main");
 					}
 				}
 				System.out.println("new condition pk_main_network==="+pk_main_network);
 			}catch(Exception e){
 				e.printStackTrace();
 			}
		}
	}
}

if((networkuser.getMoreNetworkUserDetails()!=null && networkuser.getMoreNetworkUserDetails().size()>0) && ( (networkName.equals("")) && (CTEP_ID.equals("")) && (networkPK.equals("")) && (sitePK.equals("")) && (relationShipPK.equals("")) && (siteName.equals("")) && ((pkCode.equals(""))))){
	String sql="select nvl(er_nwsites.fk_nwsites_main,pk_nwsites) as fk_nwsites_main  from er_nwsites, er_nwusers where pk_nwusers in ("+net_pks+") and er_nwusers.fk_nwsites=pk_nwsites";
	
	pk_main_network = pkMainNetwork(sql,null);
	
}
net_pks=net_pks.equals("")?net_pks:"";
System.out.println("pk_main_network==="+pk_main_network);
	String str1 = "select pk_nwsites,fk_site,site_name ,site_name as network_name,NW_LEVEL+1 as nw_level,er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden, "
			+" (SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType, "
			+" (SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,(SELECT CODELST_HIDE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipHide, CTEP_ID "
			+ "from er_nwsites,er_site,er_codelst "+
				  "where pk_site=fk_site and pk_codelst=nw_status and er_site.fk_account=? and nw_level=0 ";
	if(!(networkName.equals(""))){
			 	 str1=str1+" and lower(site_name) like lower('%"+networkName+"%') ";
		}
	//if(networkName.equals("") && pk_main_network.length()>0)
	if(pk_main_network.length()>0)
	{
		str1=str1+" and pk_nwsites in("+pk_main_network+") ";
	}
try {
	System.out.println("str1==="+str1.toString());
conn = CommonDAO.getConnection();
pstmt = conn.prepareStatement(str1);
pstmt.setInt(1, StringUtil.stringToNum(iaccId));
rs = pstmt.executeQuery();
Code code=null;
Code code1=null;
while (rs.next()) {
	System.out.println("Inside While");
	code = new Code();
	pkNetList.add(String.valueOf(rs.getInt("pk_nwsites")));
	fkSiteList.add(String.valueOf(rs.getInt("fk_site")));
	netNameList.add(rs.getString("network_name"));
	code.setType(rs.getString("stat_type"));
	code.setCode(rs.getString("code"));
	code.setDescription(rs.getString("description"));
	netStatList.add(code);
	ctepList.add(rs.getString("CTEP_ID"));
	if(rs.getString("SiteRelationshipType")!=null){
		code1=new Code();
		code1.setType(rs.getString("SiteRelationshipType"));
		code1.setCode(rs.getString("SiteRelationshipSubType"));
		code1.setDescription(rs.getString("SiteRelationshipDesc"));
	}
	siteNameList.add(rs.getString("site_name"));
	netLevelList.add(String.valueOf(rs.getInt("nw_level")));
	count++;
}
System.out.println("count==="+count);
if(count==0)
{
	this.response=new ResponseHolder();
	this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
	this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Network not found")); 
	throw new OperationException();
}

	NetworkSitesUsers site=null;
	List<NetworkSitesUsers> siteList=null;
	SiteIdentifier siteIdentifier=null;	
	ObjectMapService objectMapService;
	
	for(int i=0;i<pkNetList.size();i++){
		site=new NetworkSitesUsers();
		siteList=new ArrayList<NetworkSitesUsers>();
		siteIdentifier=new SiteIdentifier();
		map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(fkSiteList.get(i)));
		siteIdentifier.setOID(map.getOID());
		siteIdentifier.setPK(Integer.valueOf(fkSiteList.get(i)));
		siteIdentifier.setName(siteNameList.get(i).toString());
		site.setSiteIdentifier(siteIdentifier);
		site.setLevel(String.valueOf(netLevelList.get(i)));
		site.setSiteStatus(netStatList.get(i));
		site.setCTEP_ID(ctepList.get(i).toString());
		if(code1!=null){
			site.setSiteRelationShipType(code1);
		}
		objectMapService = ((ObjectMapService)params.get("objectMapService"));
		params.put("objectMapService", objectMapService);
		userPkCountN = networkUsersRoles(pkNetList.get(i),iaccId,site,params,net_pks);
		System.out.println("userPkCountN==="+userPkCountN);
		String pks_childNtw="";
		if(!(CTEP_ID.equals("")) || !(relationShipPK.equals("")) || !(siteName.equals("")) || !(sitePK.equals("")) || (!(pkCode.equals("")))){
			
			StringBuffer sqlbuf2 = new StringBuffer();
		sqlbuf2.append("SELECT pk_nwsites, fk_site,  site_name , site_name AS network_name, er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden,er_nwsites.NW_LEVEL+1 AS nw_level, "
				+" (SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType, "
				+" (SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,(SELECT CODELST_HIDE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipHide "
				+ "FROM er_nwsites,er_site,er_codelst WHERE pk_site=fk_site AND er_site.fk_account=? AND (er_nwsites.FK_NWSITES_MAIN =? or er_nwsites.PK_NWSITES =?) and pk_codelst=nw_status ");
		if(!(pkCode.equals(""))){
 	 		sqlbuf2.append(" and nw_membertype="+pkCode+"");
 	 	}
		if(!(siteName.equals("")))
	    {
		sqlbuf2.append(" and lower(site_name) like lower('%" +siteName+ "%')");
		 }
		if(!sitePK.equals("")){
			sqlbuf2.append(" and er_nwsites.fk_site="+sitePK+"");
		}
		if(!(CTEP_ID.equals(""))){
			sqlbuf2.append(" and lower(ctep_id) like lower('%"+CTEP_ID+"%') ");
		}
		if(!(relationShipPK.equals("")))
		{
			if(!relation_pks.equals("")){
				sqlbuf2.append(" and pk_nwsites in ("+relationShipPkMap.get(pkNetList.get(i)).toString()+") ");
			}else{
				sqlbuf2.append(" and pk_nwsites in ("+relationShipPK+") ");
			}
		}
		System.out.println("sqlbuf2==="+sqlbuf2.toString());
		System.out.println("pkNetList.get(i)==="+pkNetList.get(i));
		
		pstmt = conn.prepareStatement(sqlbuf2.toString());
		pstmt.setInt(1, StringUtil.stringToNum(iaccId));
		pstmt.setInt(2, StringUtil.stringToNum(pkNetList.get(i)));
		pstmt.setInt(3, StringUtil.stringToNum(pkNetList.get(i)));
		rs = pstmt.executeQuery();
	
		
		while (rs.next()) {
			pks_childNtw=pks_childNtw+rs.getInt("pk_nwsites")+",";
		}
		pks_childNtw=pks_childNtw.length()>0?pks_childNtw.substring(0,pks_childNtw.length()-1):"0";
	}
		
		if(!pks_childNtw.equals("0") && !pks_childNtw.equals("")){
			
			System.out.println("Inside If");
		NetworkSitesUsers sitelvl2=null;
		NetworkSitesUsers sitelvl3=null;
		NetworkSitesUsers sitelvl4=null;
		NetworkSitesUsers sitelvl5=null;
		NetworkSitesUsers sitelvl6=null;
		NetworkSitesUsers sitelvl7=null;

		List<NetworkSitesUsers> siteslvl2=null;
		List<NetworkSitesUsers> siteslvl3=null;
		List<NetworkSitesUsers> siteslvl4=null;
		List<NetworkSitesUsers> siteslvl5=null;
		List<NetworkSitesUsers> siteslvl6=null;
		List<NetworkSitesUsers> siteslvl7=null;	
		StringBuffer sqlbuf3 = new StringBuffer();
		sqlbuf3.append("SELECT pk_nwsites, fk_site,  site_name ,ctep_id, site_name AS network_name, er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden,er_nwsites.NW_LEVEL+1 AS nw_level, "
				+" (SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType, "
				+" (SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,(SELECT CODELST_HIDE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipHide "
				+ "FROM er_nwsites,er_site,er_codelst WHERE pk_site=fk_site AND er_site.fk_account=? AND er_nwsites.FK_NWSITES_MAIN =? and pk_codelst=nw_status "); 
		
		sqlbuf3.append(" and pk_nwsites <> ? START WITH er_nwsites.pk_nwsites in (?) CONNECT BY PRIOR er_nwsites.fk_nwsites=er_nwsites.pk_nwsites ORDER BY nw_level");
		System.out.println("sqlbuf3==="+sqlbuf3.toString());
		System.out.println("pks_childNtw==="+pks_childNtw);
		System.out.println("pkNetList.get(i)==="+pkNetList.get(i));
		    pstmt = conn.prepareStatement(sqlbuf3.toString());
			pstmt.setInt(1, StringUtil.stringToNum(iaccId));
			pstmt.setInt(2, StringUtil.stringToNum(pkNetList.get(i)));
			pstmt.setInt(3, StringUtil.stringToNum(pkNetList.get(i)));
			pstmt.setInt(4, StringUtil.stringToNum(pks_childNtw));
			rs = pstmt.executeQuery();
			String pkNetList1="";
			Code cod=null;
			siteslvl2=new ArrayList<NetworkSitesUsers>();
			siteslvl3=new ArrayList<NetworkSitesUsers>();
			siteslvl4=new ArrayList<NetworkSitesUsers>();
			siteslvl5=new ArrayList<NetworkSitesUsers>();
			siteslvl6=new ArrayList<NetworkSitesUsers>();
			siteslvl7=new ArrayList<NetworkSitesUsers>();
			
			while (rs.next()) {
				
				code=new Code();
				siteIdentifier=new SiteIdentifier();
				map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(rs.getInt("fk_site")));
				siteIdentifier.setOID(map.getOID());
				siteIdentifier.setPK(Integer.valueOf(rs.getInt("fk_site")));
				siteIdentifier.setName(rs.getString("site_name"));
				code.setType(rs.getString("stat_type"));
				code.setCode(rs.getString("code"));
				code.setDescription(rs.getString("description"));
				
				System.out.println("site name==="+rs.getString("site_name"));
				if(rs.getInt("nw_level")==2){
					sitelvl2=new NetworkSitesUsers();
					siteslvl3=new ArrayList<NetworkSitesUsers>();
					sitelvl2.setSiteIdentifier(siteIdentifier);
					sitelvl2.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl2.setSiteStatus(code);
					sitelvl2.setCTEP_ID(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
						code=new Code();
						code.setType(rs.getString("SiteRelationshipType"));
						code.setCode(rs.getString("SiteRelationshipSubType"));
						code.setDescription(rs.getString("SiteRelationshipDesc"));
						sitelvl2.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl2.setRelationship_PK(rs.getString("pk_nwsites"));
					}
					String pk_nwsites=rs.getString("pk_nwsites");
					userPkCountS=networkUsersRoles(pk_nwsites,iaccId,sitelvl2,params,"");
					siteslvl2.add(sitelvl2);
					site.setSites(siteslvl2);
				}
				else if(rs.getInt("nw_level")==3){
					sitelvl3=new NetworkSitesUsers();
					siteslvl4=new ArrayList<NetworkSitesUsers>();
					sitelvl3.setSiteIdentifier(siteIdentifier);
					sitelvl3.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl3.setSiteStatus(code);
					sitelvl3.setCTEP_ID(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
						code=new Code();
						code.setType(rs.getString("SiteRelationshipType"));
						code.setCode(rs.getString("SiteRelationshipSubType"));
						code.setDescription(rs.getString("SiteRelationshipDesc"));
						sitelvl3.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl3.setRelationship_PK(rs.getString("pk_nwsites"));
					}
					String pk_nwsites=rs.getString("pk_nwsites");
					userPkCountS=networkUsersRoles(pk_nwsites,iaccId,sitelvl3,params,"");
					siteslvl3.add(sitelvl3);
					sitelvl2.setSites(siteslvl3);
				}
				else if(rs.getInt("nw_level")==4){
					sitelvl4=new NetworkSitesUsers();
					siteslvl5=new ArrayList<NetworkSitesUsers>();
					sitelvl4.setSiteIdentifier(siteIdentifier);
					sitelvl4.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl4.setSiteStatus(code);
					sitelvl4.setCTEP_ID(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
						code=new Code();
						code.setType(rs.getString("SiteRelationshipType"));
						code.setCode(rs.getString("SiteRelationshipSubType"));
						code.setDescription(rs.getString("SiteRelationshipDesc"));
						sitelvl4.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl4.setRelationship_PK(rs.getString("pk_nwsites"));
					}
					String pk_nwsites=rs.getString("pk_nwsites");
					userPkCountS=networkUsersRoles(pk_nwsites,iaccId,sitelvl4,params,"");
					siteslvl4.add(sitelvl4);
					sitelvl3.setSites(siteslvl4);
				}
				else if(rs.getInt("nw_level")==5){
					sitelvl5=new NetworkSitesUsers();
					siteslvl6=new ArrayList<NetworkSitesUsers>();
					sitelvl5.setSiteIdentifier(siteIdentifier);
					sitelvl5.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl5.setSiteStatus(code);
					sitelvl5.setCTEP_ID(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
						code=new Code();
						code.setType(rs.getString("SiteRelationshipType"));
						code.setCode(rs.getString("SiteRelationshipSubType"));
						code.setDescription(rs.getString("SiteRelationshipDesc"));
						sitelvl5.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl5.setRelationship_PK(rs.getString("pk_nwsites"));
					}
					String pk_nwsites=rs.getString("pk_nwsites");
					userPkCountS=networkUsersRoles(pk_nwsites,iaccId,sitelvl5,params,"");
					siteslvl5.add(sitelvl5);
					sitelvl4.setSites(siteslvl5);
				}
				else if(rs.getInt("nw_level")==6){
					sitelvl6=new NetworkSitesUsers();
					siteslvl7=new ArrayList<NetworkSitesUsers>();
					sitelvl6.setSiteIdentifier(siteIdentifier);
					sitelvl6.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl6.setSiteStatus(code);
					sitelvl6.setCTEP_ID(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
						code=new Code();
						code.setType(rs.getString("SiteRelationshipType"));
						code.setCode(rs.getString("SiteRelationshipSubType"));
						code.setDescription(rs.getString("SiteRelationshipDesc"));
						sitelvl6.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl6.setRelationship_PK(rs.getString("pk_nwsites"));
					}
					
					String pk_nwsites=rs.getString("pk_nwsites");
					userPkCountS=networkUsersRoles(pk_nwsites,iaccId,sitelvl6,params,"");
					siteslvl6.add(sitelvl6);
					sitelvl5.setSites(siteslvl6);
				}
				else if(rs.getInt("nw_level")==7){
					sitelvl7=new NetworkSitesUsers();
					siteIdentifier=new SiteIdentifier();
					sitelvl7.setSiteIdentifier(siteIdentifier);
					sitelvl7.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl7.setSiteStatus(code);
					sitelvl7.setCTEP_ID(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
						code=new Code();
						code.setType(rs.getString("SiteRelationshipType"));
						code.setCode(rs.getString("SiteRelationshipSubType"));
						code.setDescription(rs.getString("SiteRelationshipDesc"));
						sitelvl7.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl7.setRelationship_PK(rs.getString("pk_nwsites"));
					}
					String pk_nwsites=rs.getString("pk_nwsites");
					userPkCountS=networkUsersRoles(pk_nwsites,iaccId,sitelvl7,params,"");
					siteslvl7.add(sitelvl7);
					sitelvl6.setSites(siteslvl7);
				}
			}
				StringBuffer sqlbuf4 = new StringBuffer();
				sqlbuf4.append("SELECT pk_nwsites, fk_site,  site_name ,ctep_id, site_name AS network_name, er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden,er_nwsites.NW_LEVEL+1 AS nw_level, "
						+" (SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType, "
						+" (SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,(SELECT CODELST_HIDE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipHide "
						+ "FROM er_nwsites,er_site,er_codelst WHERE pk_site=fk_site AND er_site.fk_account=? AND er_nwsites.FK_NWSITES_MAIN =? and pk_codelst=nw_status "); 
				
				sqlbuf4.append(" and pk_nwsites not in (?) START WITH er_nwsites.pk_nwsites in (?) CONNECT BY PRIOR er_nwsites.pk_nwsites=er_nwsites.fk_nwsites ORDER SIBLINGS BY lower(site_name)");
				System.out.println("sqlbuf4==="+sqlbuf4.toString());
				System.out.println("pks_childNtw==="+pks_childNtw);
			pstmt = conn.prepareStatement(sqlbuf4.toString());
			pstmt.setInt(1, StringUtil.stringToNum(iaccId));
			pstmt.setInt(2, StringUtil.stringToNum(pkNetList.get(i)));
			pstmt.setInt(3, StringUtil.stringToNum(pks_childNtw));
			pstmt.setInt(4, StringUtil.stringToNum(pks_childNtw));
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
					
					code=new Code();
					siteIdentifier=new SiteIdentifier();
					map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(rs.getInt("fk_site")));
					siteIdentifier.setOID(map.getOID());
					siteIdentifier.setPK(Integer.valueOf(rs.getInt("fk_site")));
					siteIdentifier.setName(rs.getString("site_name"));
					code.setType(rs.getString("stat_type"));
					code.setCode(rs.getString("code"));
					code.setDescription(rs.getString("description"));
					
					if(rs.getInt("nw_level")==2){
						sitelvl2=new NetworkSitesUsers();
						siteslvl3=new ArrayList<NetworkSitesUsers>();
						sitelvl2.setSiteIdentifier(siteIdentifier);
						sitelvl2.setLevel(String.valueOf(rs.getInt("nw_level")));
						sitelvl2.setSiteStatus(code);
						sitelvl2.setCTEP_ID(rs.getString("ctep_id"));
						if(rs.getString("SiteRelationshipType")!=null){
							code=new Code();
							code.setType(rs.getString("SiteRelationshipType"));
							code.setCode(rs.getString("SiteRelationshipSubType"));
							code.setDescription(rs.getString("SiteRelationshipDesc"));
							sitelvl2.setSiteRelationShipType(code);
						}
						if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
							sitelvl2.setRelationship_PK(rs.getString("pk_nwsites"));
						}
						String pk_nwsites=rs.getString("pk_nwsites");
						userPkCountS=networkUsersRoles(pk_nwsites,iaccId,sitelvl2,params,"");
						siteslvl2.add(sitelvl2);
						site.setSites(siteslvl2);
					}
					else if(rs.getInt("nw_level")==3){
						sitelvl3=new NetworkSitesUsers();
						siteslvl4=new ArrayList<NetworkSitesUsers>();
						sitelvl3.setSiteIdentifier(siteIdentifier);
						sitelvl3.setLevel(String.valueOf(rs.getInt("nw_level")));
						sitelvl3.setSiteStatus(code);
						sitelvl3.setCTEP_ID(rs.getString("ctep_id"));
						if(rs.getString("SiteRelationshipType")!=null){
							code=new Code();
							code.setType(rs.getString("SiteRelationshipType"));
							code.setCode(rs.getString("SiteRelationshipSubType"));
							code.setDescription(rs.getString("SiteRelationshipDesc"));
							sitelvl3.setSiteRelationShipType(code);
						}
						if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
							sitelvl3.setRelationship_PK(rs.getString("pk_nwsites"));
						}
						String pk_nwsites=rs.getString("pk_nwsites");
						userPkCountS=networkUsersRoles(pk_nwsites,iaccId,sitelvl3,params,"");
						siteslvl3.add(sitelvl3);
						sitelvl2.setSites(siteslvl3);
					}
					else if(rs.getInt("nw_level")==4){
						sitelvl4=new NetworkSitesUsers();
						siteslvl5=new ArrayList<NetworkSitesUsers>();
						sitelvl4.setSiteIdentifier(siteIdentifier);
						sitelvl4.setLevel(String.valueOf(rs.getInt("nw_level")));
						sitelvl4.setSiteStatus(code);
						sitelvl4.setCTEP_ID(rs.getString("ctep_id"));
						if(rs.getString("SiteRelationshipType")!=null){
							code=new Code();
							code.setType(rs.getString("SiteRelationshipType"));
							code.setCode(rs.getString("SiteRelationshipSubType"));
							code.setDescription(rs.getString("SiteRelationshipDesc"));
							sitelvl4.setSiteRelationShipType(code);
						}
						if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
							sitelvl4.setRelationship_PK(rs.getString("pk_nwsites"));
						}
						String pk_nwsites=rs.getString("pk_nwsites");
						userPkCountS=networkUsersRoles(pk_nwsites,iaccId,sitelvl4,params,"");
						siteslvl4.add(sitelvl4);
						sitelvl3.setSites(siteslvl4);
					}
					else if(rs.getInt("nw_level")==5){
						sitelvl5=new NetworkSitesUsers();
						siteslvl6=new ArrayList<NetworkSitesUsers>();
						sitelvl5.setSiteIdentifier(siteIdentifier);
						sitelvl5.setLevel(String.valueOf(rs.getInt("nw_level")));
						sitelvl5.setSiteStatus(code);
						sitelvl5.setCTEP_ID(rs.getString("ctep_id"));
						if(rs.getString("SiteRelationshipType")!=null){
							code=new Code();
							code.setType(rs.getString("SiteRelationshipType"));
							code.setCode(rs.getString("SiteRelationshipSubType"));
							code.setDescription(rs.getString("SiteRelationshipDesc"));
							sitelvl5.setSiteRelationShipType(code);
						}
						if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
							sitelvl5.setRelationship_PK(rs.getString("pk_nwsites"));
						}
						String pk_nwsites=rs.getString("pk_nwsites");
						userPkCountS=networkUsersRoles(pk_nwsites,iaccId,sitelvl5,params,"");
						siteslvl5.add(sitelvl5);
						sitelvl4.setSites(siteslvl5);
					}
					else if(rs.getInt("nw_level")==6){
						sitelvl6=new NetworkSitesUsers();
						siteslvl7=new ArrayList<NetworkSitesUsers>();
						sitelvl6.setSiteIdentifier(siteIdentifier);
						sitelvl6.setLevel(String.valueOf(rs.getInt("nw_level")));
						sitelvl6.setSiteStatus(code);
						sitelvl6.setCTEP_ID(rs.getString("ctep_id"));
						if(rs.getString("SiteRelationshipType")!=null){
							code=new Code();
							code.setType(rs.getString("SiteRelationshipType"));
							code.setCode(rs.getString("SiteRelationshipSubType"));
							code.setDescription(rs.getString("SiteRelationshipDesc"));
							sitelvl6.setSiteRelationShipType(code);
						}
						if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
							sitelvl6.setRelationship_PK(rs.getString("pk_nwsites"));
						}
						String pk_nwsites=rs.getString("pk_nwsites");
						userPkCountS=networkUsersRoles(pk_nwsites,iaccId,sitelvl6,params,"");
						siteslvl6.add(sitelvl6);
						sitelvl5.setSites(siteslvl6);
					}
					else if(rs.getInt("nw_level")==7){
						sitelvl7=new NetworkSitesUsers();
						sitelvl7.setSiteIdentifier(siteIdentifier);
						sitelvl7.setLevel(String.valueOf(rs.getInt("nw_level")));
						sitelvl7.setSiteStatus(code);
						sitelvl7.setCTEP_ID(rs.getString("ctep_id"));
						if(rs.getString("SiteRelationshipType")!=null){
							code=new Code();
							code.setType(rs.getString("SiteRelationshipType"));
							code.setCode(rs.getString("SiteRelationshipSubType"));
							code.setDescription(rs.getString("SiteRelationshipDesc"));
							sitelvl7.setSiteRelationShipType(code);
						}
						if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
							sitelvl7.setRelationship_PK(rs.getString("pk_nwsites"));
						}
						String pk_nwsites=rs.getString("pk_nwsites");
						userPkCountS=networkUsersRoles(pk_nwsites,iaccId,sitelvl7,params,"");
						siteslvl7.add(sitelvl7);
						sitelvl6.setSites(siteslvl7);
					}
					
			
			}
		}
		
			else{
				System.out.println("Inside else");
				StringBuffer sqlbuf3 = new StringBuffer();
				sqlbuf3.append("SELECT pk_nwsites, fk_site,  site_name ,ctep_id, site_name AS network_name, er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden,er_nwsites.NW_LEVEL+1 AS nw_level, "
						+" (SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType, "
						+" (SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,(SELECT CODELST_HIDE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipHide "
						+ "FROM er_nwsites,er_site,er_codelst WHERE pk_site=fk_site AND er_site.fk_account=? AND er_nwsites.FK_NWSITES_MAIN =? and pk_codelst=nw_status "); 
				
				sqlbuf3.append(" and pk_nwsites <> ? START WITH er_nwsites.pk_nwsites in (?) CONNECT BY PRIOR er_nwsites.pk_nwsites=er_nwsites.fk_nwsites ORDER SIBLINGS BY lower(site_name)");
				System.out.println("sqlbuf3==="+sqlbuf3.toString());
				System.out.println("pkNetList.get(i)==="+pkNetList.get(i));
				    pstmt = conn.prepareStatement(sqlbuf3.toString());
					pstmt.setInt(1, StringUtil.stringToNum(iaccId));
					pstmt.setInt(2, StringUtil.stringToNum(pkNetList.get(i)));
					pstmt.setInt(3, StringUtil.stringToNum(pkNetList.get(i)));
					pstmt.setInt(4, StringUtil.stringToNum(pkNetList.get(i)));
					rs = pstmt.executeQuery();
					String pkNetList1="";
					Code cod=null;
					NetworkSitesUsers sitelvl2=null;
					NetworkSitesUsers sitelvl3=null;
					NetworkSitesUsers sitelvl4=null;
					NetworkSitesUsers sitelvl5=null;
					NetworkSitesUsers sitelvl6=null;
					NetworkSitesUsers sitelvl7=null;

					List<NetworkSitesUsers> siteslvl2=null;
					List<NetworkSitesUsers> siteslvl3=null;
					List<NetworkSitesUsers> siteslvl4=null;
					List<NetworkSitesUsers> siteslvl5=null;
					List<NetworkSitesUsers> siteslvl6=null;
					List<NetworkSitesUsers> siteslvl7=null;	
					siteslvl2=new ArrayList<NetworkSitesUsers>();
					siteslvl3=new ArrayList<NetworkSitesUsers>();
					siteslvl4=new ArrayList<NetworkSitesUsers>();
					siteslvl5=new ArrayList<NetworkSitesUsers>();
					siteslvl6=new ArrayList<NetworkSitesUsers>();
					siteslvl7=new ArrayList<NetworkSitesUsers>();
					while (rs.next()) {
						
						code=new Code();
						siteIdentifier=new SiteIdentifier();
						map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(rs.getInt("fk_site")));
						siteIdentifier.setOID(map.getOID());
						siteIdentifier.setPK(Integer.valueOf(rs.getInt("fk_site")));
						siteIdentifier.setName(rs.getString("site_name"));
						code.setType(rs.getString("stat_type"));
						code.setCode(rs.getString("code"));
						code.setDescription(rs.getString("description"));
						
						if(rs.getInt("nw_level")==2){
							sitelvl2=new NetworkSitesUsers();
							siteslvl3=new ArrayList<NetworkSitesUsers>();
							sitelvl2.setSiteIdentifier(siteIdentifier);
							sitelvl2.setLevel(String.valueOf(rs.getInt("nw_level")));
							sitelvl2.setSiteStatus(code);
							sitelvl2.setCTEP_ID(rs.getString("ctep_id"));
							if(rs.getString("SiteRelationshipType")!=null){
								code=new Code();
								code.setType(rs.getString("SiteRelationshipType"));
								code.setCode(rs.getString("SiteRelationshipSubType"));
								code.setDescription(rs.getString("SiteRelationshipDesc"));
								sitelvl2.setSiteRelationShipType(code);
							}
							if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
								sitelvl2.setRelationship_PK(rs.getString("pk_nwsites"));
							}
							String pk_nwsites=rs.getString("pk_nwsites");
							userPkCountS=networkUsersRoles(pk_nwsites,iaccId,sitelvl2,params,"");
							siteslvl2.add(sitelvl2);
							site.setSites(siteslvl2);
						}
						else if(rs.getInt("nw_level")==3){
							sitelvl3=new NetworkSitesUsers();
							siteslvl4=new ArrayList<NetworkSitesUsers>();
							sitelvl3.setSiteIdentifier(siteIdentifier);
							sitelvl3.setLevel(String.valueOf(rs.getInt("nw_level")));
							sitelvl3.setSiteStatus(code);
							sitelvl3.setCTEP_ID(rs.getString("ctep_id"));
							if(rs.getString("SiteRelationshipType")!=null){
								code=new Code();
								code.setType(rs.getString("SiteRelationshipType"));
								code.setCode(rs.getString("SiteRelationshipSubType"));
								code.setDescription(rs.getString("SiteRelationshipDesc"));
								sitelvl3.setSiteRelationShipType(code);
							}
							if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
								sitelvl3.setRelationship_PK(rs.getString("pk_nwsites"));
							}
							String pk_nwsites=rs.getString("pk_nwsites");
							userPkCountS=networkUsersRoles(pk_nwsites,iaccId,sitelvl3,params,"");
							siteslvl3.add(sitelvl3);
							sitelvl2.setSites(siteslvl3);
						}
						else if(rs.getInt("nw_level")==4){
							sitelvl4=new NetworkSitesUsers();
							siteslvl5=new ArrayList<NetworkSitesUsers>();
							sitelvl4.setSiteIdentifier(siteIdentifier);
							sitelvl4.setLevel(String.valueOf(rs.getInt("nw_level")));
							sitelvl4.setSiteStatus(code);
							sitelvl4.setCTEP_ID(rs.getString("ctep_id"));
							if(rs.getString("SiteRelationshipType")!=null){
								code=new Code();
								code.setType(rs.getString("SiteRelationshipType"));
								code.setCode(rs.getString("SiteRelationshipSubType"));
								code.setDescription(rs.getString("SiteRelationshipDesc"));
								sitelvl4.setSiteRelationShipType(code);
							}
							if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
								sitelvl4.setRelationship_PK(rs.getString("pk_nwsites"));
							}
							String pk_nwsites=rs.getString("pk_nwsites");
							userPkCountS=networkUsersRoles(pk_nwsites,iaccId,sitelvl4,params,"");
							siteslvl4.add(sitelvl4);
							sitelvl3.setSites(siteslvl4);
						}
						else if(rs.getInt("nw_level")==5){
							sitelvl5=new NetworkSitesUsers();
							siteslvl6=new ArrayList<NetworkSitesUsers>();
							sitelvl5.setSiteIdentifier(siteIdentifier);
							sitelvl5.setLevel(String.valueOf(rs.getInt("nw_level")));
							sitelvl5.setSiteStatus(code);
							sitelvl5.setCTEP_ID(rs.getString("ctep_id"));
							if(rs.getString("SiteRelationshipType")!=null){
								code=new Code();
								code.setType(rs.getString("SiteRelationshipType"));
								code.setCode(rs.getString("SiteRelationshipSubType"));
								code.setDescription(rs.getString("SiteRelationshipDesc"));
								sitelvl5.setSiteRelationShipType(code);
							}
							if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
								sitelvl5.setRelationship_PK(rs.getString("pk_nwsites"));
							}
							String pk_nwsites=rs.getString("pk_nwsites");
							userPkCountS=networkUsersRoles(pk_nwsites,iaccId,sitelvl5,params,"");
							siteslvl5.add(sitelvl5);
							sitelvl4.setSites(siteslvl5);
						}
						else if(rs.getInt("nw_level")==6){
							sitelvl6=new NetworkSitesUsers();
							siteslvl7=new ArrayList<NetworkSitesUsers>();
							sitelvl6.setSiteIdentifier(siteIdentifier);
							sitelvl6.setLevel(String.valueOf(rs.getInt("nw_level")));
							sitelvl6.setSiteStatus(code);
							sitelvl6.setCTEP_ID(rs.getString("ctep_id"));
							if(rs.getString("SiteRelationshipType")!=null){
								code=new Code();
								code.setType(rs.getString("SiteRelationshipType"));
								code.setCode(rs.getString("SiteRelationshipSubType"));
								code.setDescription(rs.getString("SiteRelationshipDesc"));
								sitelvl6.setSiteRelationShipType(code);
							}
							String pk_nwsites=rs.getString("pk_nwsites");
							userPkCountS=networkUsersRoles(pk_nwsites,iaccId,sitelvl6,params,"");
							if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
								sitelvl6.setRelationship_PK(rs.getString("pk_nwsites"));
							}
							siteslvl6.add(sitelvl6);
							sitelvl5.setSites(siteslvl6);
						}
						else if(rs.getInt("nw_level")==7){
							sitelvl7=new NetworkSitesUsers();
							siteIdentifier=new SiteIdentifier();
							sitelvl7.setSiteIdentifier(siteIdentifier);
							sitelvl7.setLevel(String.valueOf(rs.getInt("nw_level")));
							sitelvl7.setSiteStatus(code);
							sitelvl7.setCTEP_ID(rs.getString("ctep_id"));
							if(rs.getString("SiteRelationshipType")!=null){
								code=new Code();
								code.setType(rs.getString("SiteRelationshipType"));
								code.setCode(rs.getString("SiteRelationshipSubType"));
								code.setDescription(rs.getString("SiteRelationshipDesc"));
								sitelvl7.setSiteRelationShipType(code);
							}
							if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
								sitelvl7.setRelationship_PK(rs.getString("pk_nwsites"));
							}
							String pk_nwsites=rs.getString("pk_nwsites");
							userPkCountS=networkUsersRoles(pk_nwsites,iaccId,sitelvl7,params,"");
							siteslvl7.add(sitelvl7);
							sitelvl6.setSites(siteslvl7);
						}
						
					}
			}
		System.out.println("Here");
		siteList.add(site);
			/*while (rs.next()) {
				cod = new Code();
				pkNetList1=(String.valueOf(rs.getInt("pk_nwsites")));
				site=new NetworkSitesUsers();
				siteIdentifier=new SiteIdentifier();
				map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(rs.getInt("fk_site")));
				siteIdentifier.setOID(map.getOID());
				siteIdentifier.setPK(Integer.valueOf(rs.getInt("fk_site")));
				site.setSiteIdentifier(siteIdentifier);
				site.setLevel(String.valueOf(rs.getInt("network_level")));
				site.setName(rs.getString("site_name"));
				//site.setSiteStatus(rs.getString("network_status"));
				cod.setType(rs.getString("stat_type"));
				cod.setCode(rs.getString("code"));
				cod.setDescription(rs.getString("description"));
				site.setSiteStatus(cod);
				siteList.add(site);
				userPkCountS = networkUsersRoles(pkNetList1,iaccId,site,params,net_pks);
			
			}*/
		
				
		/*if(userPkCountN==0 && userPkCountS==0){
			this.response=new ResponseHolder();
			this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
			this.response.addIssue(new Issue(IssueTypes.USER_NOT_FOUND, "No User is found in this network.")); 
			throw new OperationException();
		}else{*/
			NetworksitesusersList.add((ArrayList<NetworkSitesUsers>) siteList);
		/*}*/
			
	
	

}}catch(OperationException ex){
	throw ex;
}
catch (Exception e) {
	e.printStackTrace();
} finally {
	try {
		rs.close();
		if(rs1 != null){
		rs1.close();
	     }
		pstmt.close();
		conn.close();
	} catch (SQLException e) {
		e.printStackTrace();
	}
}
}


public void getNetworkSiteLevel(NetworkSiteLevelDetails networkSiteLevelDetails, HashMap<String, Object> params) throws OperationException {
	Connection conn=null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	String iaccId = (String)params.get(KEY_ACCOUNT_ID);
	//UserBean userBean=(UserBean)params.get("callingUser");
	String networkName = (networkSiteLevelDetails.getNetworkName()==null)?"":networkSiteLevelDetails.getNetworkName();
	String siteName = (networkSiteLevelDetails.getSiteName()==null)?"":networkSiteLevelDetails.getSiteName();
	String sitePk= (networkSiteLevelDetails.getSitePK()==null)?"":networkSiteLevelDetails.getSitePK();
	String networkPk = (networkSiteLevelDetails.getNetworkPK()==null)?"":networkSiteLevelDetails.getNetworkPK();
	
	String str = "";
	String pk_main_network = "";
	
try {
	
	str="SELECT distinct nvl(FK_NWSITES_MAIN,pk_nwsites) fk_nwsites_main FROM er_nwsites,er_site WHERE er_site.fk_account=? and pk_site=fk_site ";
	
	if(!(networkPk.equals(""))){
		str=str+" and nvl(FK_NWSITES_MAIN,pk_nwsites)="+networkPk;
	}
	
	if(!networkName.equals("")){
		str=str+"and nvl(FK_NWSITES_MAIN,pk_nwsites) in (select pk_nwsites from er_nwsites,er_site where pk_site=fk_site and site_name like '"+networkName+"' and nw_level=0)";
	}
	
	if(!(sitePk.equals(""))){
		str=str +" and fk_site="+sitePk;
	}
	
	if(!(siteName.equals(""))){
		str=str +" and site_name like '"+siteName+"'";
	}
	
	pk_main_network = pkMainNetwork(str,iaccId);
	
	if(pk_main_network.equals("0")){
		this.response=new ResponseHolder();
		this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
		this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "No data found")); 
		throw new OperationException();
	}
	
	 String str1 = "select pk_nwsites,fk_site,site_name ,site_name as network_name,ctep_id,nvl(site_info,'-') as network_info, er_codelst.pk_codelst as pkStat, er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden, (SELECT pk_codelst FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) as pkRelnType, (SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType,(SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType, nw_level from er_nwsites,er_site,er_codelst "+
	 			" where pk_site=fk_site and pk_codelst=nw_status and er_site.fk_account=? and nw_level=0 ";
	 		 	
	 	if(pk_main_network.length()>0 && !pk_main_network.equals("0"))
	 	{
	 		str1=str1+" and pk_nwsites in("+pk_main_network+") ";
	 	}

	conn = CommonDAO.getConnection();
	pstmt = conn.prepareStatement(str1);
	pstmt.setInt(1, StringUtil.stringToNum(iaccId));
	rs = pstmt.executeQuery();
	Code code=null;
	while (rs.next()) {
		code=new Code();
		pkNetList.add(String.valueOf(rs.getInt("pk_nwsites")));
		fkSiteList.add(String.valueOf(rs.getInt("fk_site")));
		if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
			pkRelnShipType.add(rs.getString("pk_nwsites"));
		}
		else{
			pkRelnShipType.add("0");
		}
		netNameList.add(rs.getString("network_name"));
		netInfoList.add(rs.getString("network_info"));
		code.setPk(rs.getString("pkStat"));
		code.setType(rs.getString("stat_type"));
		code.setCode(rs.getString("code"));
		code.setDescription(rs.getString("description"));
		netStatList.add(code);
		if(rs.getString("SiteRelationshipType")!=null){
		code=new Code();
		code.setPk(rs.getString("pkRelnType"));
		code.setType(rs.getString("SiteRelationshipType"));
		code.setCode(rs.getString("SiteRelationshipSubType"));
		code.setDescription(rs.getString("SiteRelationshipDesc"));
		siteRelnShipType.add(code);
		}
		else{
			siteRelnShipType.add(null);
		}
		siteNameList.add(rs.getString("site_name"));
		netLevelList.add(String.valueOf(rs.getInt("nw_level")));
		if(rs.getString("ctep_id")!=null){
			ctepList.add(rs.getString("ctep_id"));
		}else{
			ctepList.add("");
		}
	}
	
	String chlSql="";
	Site site=null;
	SiteIdentifier siteIdentifier=null;
	ObjectMap map=null;
	Site sitelvl=null;
	Sites sites=null;
	List<Site> sitesLvlList=null;
	List<Site> siteList=null;
	for(int i=0;i<pkNetList.size();i++){
		siteList=new ArrayList<Site>();
		site=new Site();
		siteIdentifier=new SiteIdentifier();
		map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(fkSiteList.get(i)));
		siteIdentifier.setOID(map.getOID());
		siteIdentifier.setPK(Integer.valueOf(fkSiteList.get(i)));
		site.setSiteIdentifier(siteIdentifier);
		site.setLevel(String.valueOf(netLevelList.get(i)));
		site.setName(siteNameList.get(i));
		if(!netInfoList.get(i).equals("-")){
			site.setDescription(netInfoList.get(i));
		}
		site.setSiteStatus(netStatList.get(i));
		if(!pkRelnShipType.get(i).equals("0") && siteRelnShipType.get(i)!=null){
		site.setRelationshipPK(pkRelnShipType.get(i));
		site.setSiteRelationShipType(siteRelnShipType.get(i));
		}
		if(!ctepList.get(i).equals("")){
			site.setCtepId(ctepList.get(i));
		}

		chlSql="select pk_nwsites, fk_nwsites, fk_site, site_name,ctep_id,nvl(site_info,'-') as site_info, er_codelst.pk_codelst as pkStat, er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden,(SELECT pk_codelst FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) as pkRelnType, (SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType,(SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,(SELECT CODELST_HIDE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipHide, nw_level "+
				" FROM er_nwsites,er_site,er_codelst WHERE pk_site=fk_site and pk_codelst=nw_status and er_site.fk_account=? ";
	
	if(!sitePk.equals("")){
		chlSql=chlSql+" and fk_site ="+sitePk;
	}
	
	if(!siteName.equals("")){
		chlSql=chlSql+" and site_name like '"+siteName+"'";
	}
	
	chlSql=chlSql+" START WITH pk_nwsites=? CONNECT BY prior pk_nwsites=FK_NWSITES ORDER SIBLINGS BY lower(site_name)";
	pstmt = conn.prepareStatement(chlSql);
	pstmt.setInt(1, StringUtil.stringToNum(iaccId));
	pstmt.setInt(2, StringUtil.stringToNum(pkNetList.get(i)));
	rs = pstmt.executeQuery();
	
	sitesLvlList=new ArrayList<Site>();
	while(rs.next()){		
		    sites=new Sites();
			code=new Code();
			siteIdentifier=new SiteIdentifier();
			map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(rs.getInt("fk_site")));
			siteIdentifier.setOID(map.getOID());
			siteIdentifier.setPK(Integer.valueOf(rs.getInt("fk_site")));
			code.setPk(rs.getString("pkStat"));
			code.setType(rs.getString("stat_type"));
			code.setCode(rs.getString("code"));
			code.setDescription(rs.getString("description"));
			sitelvl=new Site();
			sitelvl.setSiteIdentifier(siteIdentifier);
			sitelvl.setLevel(String.valueOf(rs.getInt("nw_level")));
			sitelvl.setName(rs.getString("site_name"));
			if(!rs.getString("site_info").equals("-")){
				sitelvl.setDescription(rs.getString("site_info"));
			}
			sitelvl.setSiteStatus(code);
			sitelvl.setCtepId(rs.getString("ctep_id"));
			if(rs.getString("SiteRelationshipType")!=null){
				code=new Code();
				code.setPk(rs.getString("pkRelnType"));
				code.setType(rs.getString("SiteRelationshipType"));
				code.setCode(rs.getString("SiteRelationshipSubType"));
				code.setDescription(rs.getString("SiteRelationshipDesc"));
				sitelvl.setSiteRelationShipType(code);
			}
			if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
				sitelvl.setRelationshipPK(rs.getString("pk_nwsites"));
			}
			sitesLvlList.add(sitelvl);
			sites.setSites(sitesLvlList);
			site.setSites(sites);
		}
	siteList.add(site);
	sitesList.add((ArrayList<Site>) siteList);
	}

}catch(OperationException ex){
	throw ex;
}catch (SQLException e) {
	e.printStackTrace();
}catch (Exception e) {
	e.printStackTrace();
} finally {
	try {
		if(rs!=null)
		rs.close();
		if(pstmt!=null)
		pstmt.close();
		if(conn!=null)
		conn.close();
	} catch (SQLException e) {
		e.printStackTrace();
	}
}
}


private String getRelationPksFromUser(NetworkUserDetails networkuser) {
	// TODO Auto-generated method stub
	Connection conn=null;
	PreparedStatement pstmt=null;
	ResultSet rs=null;
	String relationPKs = "";
	try {
		conn = CommonDAO.getConnection();
		StringBuffer sqlbuff = new StringBuffer();
		System.out.println("After");
		sqlbuff.append("select distinct nvl(er_nwsites.FK_NWSITES_MAIN,pk_nwsites) as FK_NWSITES_MAIN,er_nwusers.FK_NWSITES as fk_nwsites from er_nwusers,ER_NWUSERS_ADDNLROLES,er_nwsites where er_nwusers.fk_nwsites is not null and er_nwsites.pk_nwsites=er_nwusers.FK_NWSITES");
		if(networkuser.getUserPK()!=null && !networkuser.getUserPK().equals("")){
			sqlbuff.append(" and fk_user="+networkuser.getUserPK());
		}
		if(networkuser.getUserLoginName()!=null && !networkuser.getUserLoginName().equals("")){
			sqlbuff.append(" and fk_user=(select pk_user from er_user where usr_logname='"+networkuser.getUserLoginName()+"')");
		}
		if(networkuser.getRolePK()!=null && !networkuser.getRolePK().equals("")){
			sqlbuff.append(" and (NWU_MEMBERTROLE="+networkuser.getRolePK()+" or NWU_MEMBERADDLTROLE="+networkuser.getRolePK()+")");
		}
		
		System.out.println("getRelationPksFromUser==="+sqlbuff.toString());
		pstmt = conn.prepareStatement(sqlbuff.toString());
		rs=pstmt.executeQuery();
		List<String> mainNetwork=new ArrayList<String>();
		while(rs.next()){
			if(!mainNetwork.contains(rs.getString("FK_NWSITES_MAIN"))){
				System.out.println("Incoming FK_NWSITES_MAIN=="+rs.getString("FK_NWSITES_MAIN"));
				mainNetwork.add(rs.getString("FK_NWSITES_MAIN"));
				if(relationPKs.equals("")){
					relationPKs=rs.getString("fk_nwsites");
				}else{
					relationPKs=relationPKs+","+rs.getString("fk_nwsites");
				}
				relationShipPkMap.put(rs.getString("FK_NWSITES_MAIN"), rs.getString("fk_nwsites"));
			}
		}
	}catch(Exception e){
		e.printStackTrace();
	}
	return relationPKs;
}
public String getNetPkMoreNetworkUsersDetails(List<MoreNetworkUserDetails> moredetails,String flag){
	
	Connection conn=null;
	PreparedStatement pstmt=null;
	ResultSet rs=null;
	String key="";
	String value="";
	String net_pks="";
	String sql="";

	try{
		conn = CommonDAO.getConnection();
		for(NVPair nvpair:moredetails){
			key=nvpair.getKey();
			
			ElementNSImpl val=(ElementNSImpl)nvpair.getValue();
			if(val!=null){
			NodeList node= val.getChildNodes();
	        for(int j=0;j< node.getLength();j++)
	           {
	        	  Node node1=node.item(j);
	        	  value=node1.getNodeValue();
	           }
			}
			StringBuffer sqlbuf = new StringBuffer();
				 
					 sqlbuf.append("SELECT fk_modpk FROM ER_MOREDETAILS,ER_CODELST WHERE MD_MODELEMENTPK=PK_CODELST AND CODELST_SUBTYP=? and (codelst_type");
					 
					if(flag.equalsIgnoreCase("siteDetails")){
						 sqlbuf.append(" like 'org/__' escape '/' or codelst_type='org')");
					 }
					 if(flag.equalsIgnoreCase("networkUserDetail")){
						 sqlbuf.append(" like 'user/__' escape '/' or codelst_type='user')");
					 }
					 if(!value.equalsIgnoreCase("")){
					 sqlbuf.append(" AND  lower(MD_MODELEMENTDATA) LIKE lower('%"+value+"%')");
					 }
			System.out.println("Inside getNetPkMoreNetworkUsersDetails");
			System.out.println("sqlbuf==="+sqlbuf.toString());
			System.out.println("key==="+key);
	        pstmt = conn.prepareStatement(sqlbuf.toString());
			pstmt.setString(1,key);
			rs = pstmt.executeQuery();
			while(rs.next()){
				net_pks=net_pks+rs.getString("fk_modpk")+",";
			}	
		}
		net_pks=net_pks.length()>0?net_pks.substring(0,net_pks.length()-1):"0";
		}
		catch(SQLException ex){
			ex.printStackTrace();
		}
		finally{
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	
	return net_pks;
}

public int networkUsersRoles(String pkNetwork,String iaccId,NetworkSitesUsers site,HashMap<String, Object> params,String nUser_pks){
	Connection conn=null;
	PreparedStatement pstmt=null;
	ResultSet rs=null;
	int userPkCount = 0;
	try {
		conn = CommonDAO.getConnection();

		StringBuffer sqlbuff = new StringBuffer();
		
		sqlbuff.append("SELECT pk_nwsites,pk_nwusers,fk_user,usr_logname,usr_firstname,usr_lastname,NWU_MEMBERTROLE,f_codelst_desc(NWU_MEMBERTROLE) AS role_name,c.CODELST_DESC as description,c.codelst_type as stat_type,c.codelst_subtyp as code,c.codelst_hide as stat_hidden FROM er_nwusers nwu, er_nwsites ns,er_site s,er_user u,er_codelst c WHERE nwu.FK_NWSITES=ns.PK_NWSITES and ns.PK_NWSITES=? AND  s.FK_ACCOUNT=? AND s.PK_SITE=ns.FK_SITE AND nwu.fk_user=u.pk_user AND c.pk_codelst=nwu.NWU_STATUS ");
		if(nUser_pks.length()>0){
			sqlbuff.append(" and pk_nwusers in ("+nUser_pks+") ");
		}
		
		System.out.println("sqlbuff==="+sqlbuff.toString());
		System.out.println("pkNetwork==="+pkNetwork);
		System.out.println("nUser_pks==="+nUser_pks);
		pstmt = conn.prepareStatement(sqlbuff.toString());
		pstmt.setInt(1, StringUtil.stringToNum(pkNetwork));
		pstmt.setInt(2, StringUtil.stringToNum(iaccId));
		rs=pstmt.executeQuery();
		
		UserRoleIdentifier user = null;
		UserRoleIdentifier user1 = new UserRoleIdentifier();
		UserIdentifier uid = null;
		NetworkRole roles=null;
		
		/*Roles role=new Roles();
		List<NetworkRole> rolelist=new ArrayList<NetworkRole>();
		List<UserIdentifier> userlist=new ArrayList<UserIdentifier>();*/
		List<UserRoleIdentifier> userrolelist=new ArrayList<UserRoleIdentifier>();
		Code code = null;
		int pk_nwusers=0;
		while(rs.next()){
			
			code = new Code();
			user = new UserRoleIdentifier();
			uid = new UserIdentifier();
			List<NetworkRole> ntwUsrRoles= new ArrayList<NetworkRole>();
			user.setLoginName(rs.getString("usr_logname"));
			user.setFirstName(rs.getString("usr_firstname"));
			user.setLastName(rs.getString("usr_lastname"));
			uid.setPK(rs.getInt("fk_user"));
			ObjectMap map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_USER, Integer.valueOf(uid.getPK()));
			uid.setOID(map.getOID());
			
			user.setUid(uid);
			
			if(rs.getString("NWU_MEMBERTROLE")!=null){
			    roles=new NetworkRole();
			    roles.setName(rs.getString("role_name"));
			    roles.setPK(rs.getString("NWU_MEMBERTROLE"));
			    code.setType(rs.getString("stat_type"));
				code.setCode(rs.getString("code"));
				code.setDescription(rs.getString("description"));
				
				roles.setRoleStatus(code);
				ntwUsrRoles.add(roles);
			}
			
			pk_nwusers=rs.getInt("pk_nwusers");
			networkUsersAdtnlRoles(pk_nwusers,ntwUsrRoles);
			user.setRole(ntwUsrRoles);
			userrolelist.add(user);		
			user1.setUser(userrolelist);
			site.setUserRoleIdentifier(user1);
			userPkCount++;
		}
			
		
	}catch(SQLException ex){
		ex.printStackTrace();
	}
	finally{
		try {
			rs.close();
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	return userPkCount;
}

public int networkUsersRoles(String pkNetwork,String iaccId,UserSite site,HashMap<String, Object> params,String nUser_pks){
	Connection conn=null;
	PreparedStatement pstmt=null;
	ResultSet rs=null;
	int userPkCount = 0;
	try {
		conn = CommonDAO.getConnection();

		StringBuffer sqlbuff = new StringBuffer();
		
		sqlbuff.append("SELECT pk_nwsites,pk_nwusers,fk_user,usr_logname,nwu.NWU_STATUS as nwu_status,usr_firstname,usr_lastname,(select codelst_type from er_codelst where pk_codelst=NWU_MEMBERTROLE) as roleType, (select codelst_subtyp from er_codelst where pk_codelst=NWU_MEMBERTROLE) as roleCode, NWU_MEMBERTROLE,f_codelst_desc(NWU_MEMBERTROLE) AS role_name,c.CODELST_DESC as description,c.codelst_type as stat_type,c.codelst_subtyp as code,c.codelst_hide as stat_hidden FROM er_nwusers nwu, er_nwsites ns,er_site s,er_user u,er_codelst c WHERE nwu.FK_NWSITES=ns.PK_NWSITES and ns.PK_NWSITES=? AND  s.FK_ACCOUNT=? AND s.PK_SITE=ns.FK_SITE AND nwu.fk_user=u.pk_user AND c.pk_codelst=nwu.NWU_STATUS ");
		if(!nUser_pks.equals("")){
			sqlbuff.append(" and fk_user="+nUser_pks);
		}
		
		System.out.println("sqlbuff==="+sqlbuff.toString());
		System.out.println("pkNetwork==="+pkNetwork);
		pstmt = conn.prepareStatement(sqlbuff.toString());
		pstmt.setInt(1, StringUtil.stringToNum(pkNetwork));
		pstmt.setInt(2, StringUtil.stringToNum(iaccId));
		rs=pstmt.executeQuery();
		
		UserRoleIdentifier user = null;
		UserRoleIdentifier user1 = new UserRoleIdentifier();
		UserIdentifier uid = null;
		Roles roles =null;
		NetworkRole role =null;
		
		/*Roles role=new Roles();
		List<NetworkRole> rolelist=new ArrayList<NetworkRole>();
		List<UserIdentifier> userlist=new ArrayList<UserIdentifier>();*/
		List<UserRoleIdentifier> userrolelist=new ArrayList<UserRoleIdentifier>();
		Code code = null;
		int pk_nwusers=0;
		while(rs.next()){
			
			code = new Code();
			user = new UserRoleIdentifier();
			uid = new UserIdentifier();
			List<NetworkRole> ntwUsrRoles= new ArrayList<NetworkRole>();
			user.setLoginName(rs.getString("usr_logname"));
			user.setFirstName(rs.getString("usr_firstname"));
			user.setLastName(rs.getString("usr_lastname"));
			user.setPK(String.valueOf(rs.getInt("fk_user")));
			/*ObjectMap map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_USER, Integer.valueOf(uid.getPK()));
			uid.setOID(map.getOID());
			
			user.setUid(uid);*/
			
			if(rs.getString("NWU_MEMBERTROLE")!=null){
			    
			    role= new NetworkRole();
			    role.setName(rs.getString("role_name"));
			    role.setPK(rs.getString("NWU_MEMBERTROLE"));
			    role.setRoleType(rs.getString("roleType"));
			    role.setRoleCode(rs.getString("roleCode"));
			    code.setType(rs.getString("stat_type"));
				code.setCode(rs.getString("code"));
				code.setDescription(rs.getString("description"));
				code.setPk(String.valueOf(rs.getInt("nwu_status")));
				role.setRoleStatus(code);
				ntwUsrRoles.add(role);
			}
			
			pk_nwusers=rs.getInt("pk_nwusers");
			networkUsersAdtnlRoles(pk_nwusers,ntwUsrRoles);
			if(ntwUsrRoles.size()>0){
				roles=new Roles();
				roles.setRoles(ntwUsrRoles);
				user.setRoles(roles);
			}
			userrolelist.add(user);		
			user1.setUser(userrolelist);
			site.setUsers(user1);
			userPkCount++;
		}
			
		
	}catch(SQLException ex){
		ex.printStackTrace();
	}
	finally{
		try {
			rs.close();
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	return userPkCount;
}

public void networkUsersAdtnlRoles(int pkNWUsers){
	
	Connection conn=null;
	PreparedStatement pstmt=null;
	ResultSet rs=null;
	NetworkServiceDao nwdao = null;
	try {
		conn = CommonDAO.getConnection();

		StringBuffer sqlbuff = new StringBuffer();
		
		sqlbuff.append("select f_codelst_desc(NWU_MEMBERADDLTROLE) name,(select codelst_type from er_codelst where pk_codelst=nwu_adtstatus) type, f_codelst_subtyp(nwu_adtstatus) code, f_codelst_desc(nwu_adtstatus) description from ER_NWUSERS_ADDNLROLES where FK_NWUSERS=?  ");
		
		pstmt = conn.prepareStatement(sqlbuff.toString());
		pstmt.setInt(1, pkNWUsers);
		rs=pstmt.executeQuery();
		
		while(rs.next()){
			nwdao.setRoleName(rs.getString("role_name"));
			nwdao.setRoleType(rs.getString("stat_type"));
			nwdao.setRoleCode(rs.getString("code"));
			nwdao.setRoleDescription(rs.getString("description"));		
		}
			
		
	}catch(SQLException ex){
		ex.printStackTrace();
	}
	finally{
		try {
			rs.close();
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
}

public void networkUsersAdtnlRoles(int pkNWUsers,List<NetworkRole> ntwUsrRoles){
	
	Connection conn=null;
	PreparedStatement pstmt=null;
	ResultSet rs=null;
	NetworkRole roles=null;
	Code code = new Code();
	try {
		conn = CommonDAO.getConnection();

		StringBuffer sqlbuff = new StringBuffer();
		
		sqlbuff.append("select NWU_MEMBERADDLTROLE,f_codelst_desc(NWU_MEMBERADDLTROLE) name,(select codelst_type from er_codelst where pk_codelst=nwu_adtstatus) type, f_codelst_subtyp(nwu_adtstatus) code, f_codelst_desc(nwu_adtstatus) description, nwu_adtstatus, (select codelst_type from er_codelst where pk_codelst=NWU_MEMBERADDLTROLE) as roleType, (select codelst_subtyp from er_codelst where pk_codelst=NWU_MEMBERADDLTROLE) as roleCode from ER_NWUSERS_ADDNLROLES where FK_NWUSERS=?  ");
		
		pstmt = conn.prepareStatement(sqlbuff.toString());
		pstmt.setInt(1, pkNWUsers);
		rs=pstmt.executeQuery();
		
		while(rs.next()){
			if(rs.getString("NWU_MEMBERADDLTROLE")!=null){
			code = new Code();
			roles=new NetworkRole();
			roles.setName(rs.getString("name"));
			roles.setPK(rs.getString("NWU_MEMBERADDLTROLE"));
			roles.setRoleType(rs.getString("roleType"));
		    roles.setRoleCode(rs.getString("roleCode"));
			code.setCode(rs.getString("code"));
			code.setType(rs.getString("type"));
			code.setDescription(rs.getString("description"));
			code.setPk(String.valueOf(rs.getInt("nwu_adtstatus")));
			roles.setRoleStatus(code);
			ntwUsrRoles.add(roles);
			}
		}
			
		
	}catch(SQLException ex){
		ex.printStackTrace();
	}
	finally{
		try {
			rs.close();
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
}

public void getNetworkSiteParent(SiteLevelDetails siteLevelDetails, Map<String, Object> parameters) throws OperationException{
	// TODO Auto-generated method stub
	Connection conn=null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	int count=0;
	String str = "";
	String pk_main_network = "";
	String networkPK = (siteLevelDetails.getNetworkPK()!=null)?siteLevelDetails.getNetworkPK():"";
	String networkName = (siteLevelDetails.getNetworkName()!=null)?siteLevelDetails.getNetworkName():"";
	String sitePK = (siteLevelDetails.getSitePK()!=null)?siteLevelDetails.getSitePK():"";
	String siteName = (siteLevelDetails.getSiteName()!=null)?siteLevelDetails.getSiteName():"";
	String level = (siteLevelDetails.getLevel()!=null)?siteLevelDetails.getLevel():"";
	String relationshipPK = (siteLevelDetails.getRelationshipPK()!=null)?siteLevelDetails.getRelationshipPK():"";
	String iaccId = parameters.get(KEY_ACCOUNT_ID).toString();
	if(!relationshipPK.equals("")){
		String getPrimaryNetwork="select nvl(fk_nwsites_main,0) as fk_nwsites_main,nw_level FROM er_nwsites WHERE pk_nwsites="+ EJBUtil.stringToNum(relationshipPK);
		try {
			conn = CommonDAO.getConnection();
			pstmt = conn.prepareStatement(getPrimaryNetwork);
			rs=pstmt.executeQuery();
			while(rs.next()){
				pk_main_network=String.valueOf(rs.getInt("fk_nwsites_main"));
				level=String.valueOf(rs.getInt("nw_level"));
			}
			System.out.println("new condition pk_main_network==="+pk_main_network);
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	if(level.equals("0")){
		this.response=new ResponseHolder();
		this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
		this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Parent site details not applicable for root network site")); 
		throw new OperationException();
	}
	if(pk_main_network.equals("") && !networkPK.equals("")){
		pk_main_network = networkPK;
	}
	System.out.println("final pk_main_network==="+pk_main_network);
	
	 	String str1 = "select pk_nwsites,fk_site,site_name ,site_info,site_info as network_info,nw_status,site_name as network_name,ctep_id, er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden,er_nwsites.NW_MEMBERTYPE as member_type,(SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType,(SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,nw_level from er_nwsites,er_site,er_codelst "+
	 				  " where pk_site=fk_site and pk_codelst=nw_status and er_site.fk_account=? and nw_level=0 ";
	 	if(!(networkName.equals(""))){
	 			 	 str1=str1+" and lower(site_name) = lower('"+networkName+"') ";
	 		}
	 	//if(networkName.equals("") && pk_main_network.length()>0)
	 	if(pk_main_network.length()>0)
	 	{
	 		str1=str1+" and pk_nwsites in("+pk_main_network+") ";
	 	}
	 	System.out.println("str1==="+str1);
try {
	conn = CommonDAO.getConnection();
	pstmt = conn.prepareStatement(str1);
	pstmt.setInt(1, StringUtil.stringToNum(iaccId));
	rs = pstmt.executeQuery();
	Code code=null;
	while (rs.next()) {
		code=new Code();
		pkNetList.add(String.valueOf(rs.getInt("pk_nwsites")));
		fkSiteList.add(String.valueOf(rs.getInt("fk_site")));
		if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
			pkRelnShipType.add(rs.getString("pk_nwsites"));
		}
		else{
			pkRelnShipType.add("0");
		}
		netNameList.add(rs.getString("network_name"));
		netInfoList.add(rs.getString("network_info"));
		code.setPk(String.valueOf(rs.getInt("nw_status")));
		code.setType(rs.getString("stat_type"));
		code.setCode(rs.getString("code"));
		code.setDescription(rs.getString("description"));
		netStatList.add(code);
		if(rs.getString("SiteRelationshipType")!=null){
		code=new Code();
		code.setPk(String.valueOf(rs.getInt("member_type")));
		code.setType(rs.getString("SiteRelationshipType"));
		code.setCode(rs.getString("SiteRelationshipSubType"));
		code.setDescription(rs.getString("SiteRelationshipDesc"));
		siteRelnShipType.add(code);
		}
		else{
			siteRelnShipType.add(null);
		}
		siteNameList.add(rs.getString("site_name"));
		siteInfoList.add(rs.getString("site_info"));
		netLevelList.add(String.valueOf(rs.getInt("nw_level")));
		if(rs.getString("ctep_id")!=null){
			ctepList.add(rs.getString("ctep_id"));
		}else{
			ctepList.add("");
		}
		count++;
	}
	if(count==0)
	{
		this.response=new ResponseHolder();
		this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
		this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "No data found")); 
		throw new OperationException();
	}
	String chlSql="";
	Site site=null;
	SiteIdentifier siteIdentifier=null;
	ObjectMap map=null;
	Site sitelvl1=null;
	Site sitelvl2=null;
	Site sitelvl3=null;
	Site sitelvl4=null;
	Site sitelvl5=null;
	Site sitelvl6=null;
	UserBean userBean=(UserBean)parameters.get("callingUser");
	List<Site> siteslvl1=null;
	List<Site> siteslvl2=null;
	List<Site> siteslvl3=null;
	List<Site> siteslvl4=null;
	List<Site> siteslvl5=null;
	List<Site> siteslvl6=null;
	Sites psiteslvl0=null;
	Sites psiteslvl1=null;
	Sites psiteslvl2=null;
	Sites psiteslvl3=null;
	Sites psiteslvl4=null;
	Sites psiteslvl5=null;
	MoreDetailsJB mdJB=null;
	MoreDetailsDao mdDao;
	ArrayList idList = new ArrayList();
	ArrayList modElementDescList = new ArrayList();
	ArrayList modElementDataList = new ArrayList();
	ArrayList modElementKeysList = new ArrayList();
	List<NVPair> listNVPair=null;
	NVPair nvpair = null;
	List<Site> siteList=null;
	String net_pks = "";
	for(int i=0;i<pkNetList.size();i++){
		
		//siteList.add(site);
		
		//chlSql="select pk_nwsites,fk_site,lower(site_name) site_name,er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden,nw_memberType,(SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType,(SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,(SELECT CODELST_HIDE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipHide, nw_level+1 as nw_level from er_nwsites,er_site,er_codelst where pk_site=fk_site and pk_codelst=nw_status "+
		//	" and er_site.fk_account=? and FK_NWSITES_MAIN=? ";
		String pks_childNtw="";
		
		//chlSql="select pk_nwsites,site_name from er_nwsites,er_site,er_codelst where pk_site=fk_site and pk_codelst=nw_status "+
		//		" and er_site.fk_account=? and FK_NWSITES_MAIN=? ";
		
		
		List<String> relationshipPKList = new ArrayList<String>();
		if(relationshipPK.equals("")){
		String getRelationshipPk = "select pk_nwsites from er_nwsites,er_site where (fk_nwsites_main="+StringUtil.stringToNum(pkNetList.get(i))+" or fk_nwsites_main is null) and pk_site=fk_site";
		if(!siteName.equals("")){
			getRelationshipPk=getRelationshipPk+ " and lower(site_name) = lower('"+siteName+"')";
		}
		if(!sitePK.equals("")){
			getRelationshipPk=getRelationshipPk+ " and fk_site = "+StringUtil.stringToNum(sitePK);
		}
		if(!level.equals("")){
			getRelationshipPk=getRelationshipPk+ " and nw_level = "+(StringUtil.stringToNum(level));
		}
		getRelationshipPk=getRelationshipPk+" order by pk_nwsites";
		try {
			pstmt = conn.prepareStatement(getRelationshipPk);
			rs=pstmt.executeQuery();
			while(rs.next()){
				if(rs.getString("pk_nwsites")!=null){
					relationshipPKList.add(String.valueOf(rs.getInt("pk_nwsites")));
				}
				
			}
			System.out.println("getRelationshipPk==="+getRelationshipPk);
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}else{
		relationshipPKList.add(relationshipPK);
	}
	String immediateParentPKs = "";
	
	if(relationshipPKList.size()>0){
		
		siteList=new ArrayList<Site>();
		
		for(String relationPK: relationshipPKList){
			siteslvl1=new ArrayList<Site>();
			siteslvl2=new ArrayList<Site>();
			siteslvl3=new ArrayList<Site>();
			siteslvl4=new ArrayList<Site>();
			siteslvl5=new ArrayList<Site>();
			siteslvl6=new ArrayList<Site>();
			psiteslvl0=new Sites();
			psiteslvl1=new Sites();
			psiteslvl2=new Sites();
			psiteslvl3=new Sites();
			psiteslvl4=new Sites();
			psiteslvl5=new Sites();
			
			listNVPair=new ArrayList<NVPair>();
			site=new Site();
			siteIdentifier=new SiteIdentifier();
			map = ((ObjectMapService)parameters.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(fkSiteList.get(i)));
			siteIdentifier.setOID(map.getOID());
			siteIdentifier.setPK(Integer.valueOf(fkSiteList.get(i)));
			site.setSiteIdentifier(siteIdentifier);
			site.setLevel(String.valueOf(netLevelList.get(i)));
			site.setName(siteNameList.get(i));
			site.setDescription(siteInfoList.get(i));
			site.setSiteStatus(netStatList.get(i));
			if(!pkRelnShipType.get(i).equals("0") && siteRelnShipType.get(i)!=null){
			site.setRelationshipPK(pkRelnShipType.get(i));
			site.setSiteRelationShipType(siteRelnShipType.get(i));
			
			}
			if(!ctepList.get(i).equals("")){
				site.setCtepId(ctepList.get(i));
			}
			//if(moreSiteDetails==null){
			mdJB = new MoreDetailsJB();
			mdDao = new MoreDetailsDao();
			mdDao = mdJB.getMoreDetails(EJBUtil.stringToNum(pkNetList.get(i)),"org_"+EJBUtil.stringToNum(netLevelList.get(i)),userBean.getUserGrpDefault());
			idList = mdDao.getId();
			modElementDescList = mdDao.getMdElementDescs();
			modElementDataList = mdDao.getMdElementValues();
			modElementKeysList = mdDao.getMdElementKeys();
			for(int iY=0; iY<idList.size(); iY++){
				if((Integer)idList.get(iY)>0){
				nvpair = new NVPair();
				nvpair.setKey((String) modElementKeysList.get(iY));
				nvpair.setValue((String)modElementDataList.get(iY));
				listNVPair.add(nvpair);
				}
			}
			if(net_pks.equals("")){
				net_pks= pkNetList.get(i).toString();
			}
			//}
			/*else{		
					listNVPair=new UserServiceImpl().getNetworkMoreDetailValue(pkNetList.get(i),keySearch,(EJBUtil.stringToNum(netLevelList.get(i))-1),iaccId);
				}*/
			
			site.setMoreSiteDetailsFields(listNVPair);
		String getImmediateParentPKs = "select rowtocol('select nvl(fk_nwsites,pk_nwsites) from er_nwsites where (fk_nwsites_main="+StringUtil.stringToNum(pkNetList.get(i))+" or fk_nwsites_main is null) and pk_nwsites in ("+relationPK+") order by fk_nwsites') as fk_nwsites from dual";
		try {
			pstmt = conn.prepareStatement(getImmediateParentPKs);
			rs=pstmt.executeQuery();
			while(rs.next()){
				immediateParentPKs=rs.getString("fk_nwsites");
			}
			System.out.println("getimmediateParentPKs==="+getImmediateParentPKs);
			System.out.println("immediateParentPKs==="+immediateParentPKs);
		}catch(Exception e){
			e.printStackTrace();
		}
	
			if(!immediateParentPKs.equals("")){
				pks_childNtw = relationPK + "," + immediateParentPKs;
			}else{
				pks_childNtw = relationPK;
			}
			System.out.println("pks_childNtw==="+pks_childNtw);
			if(!pks_childNtw.equals("0") && !pks_childNtw.equals(""))
			{
			chlSql="SELECT distinct pk_nwsites,site_name,site_info,fk_site,ctep_id,er_codelst.CODELST_DESC AS description,er_codelst.codelst_type AS stat_type,er_codelst.codelst_subtyp AS code,er_codelst.codelst_hide AS stat_hidden, "
					+" nw_status, er_nwsites.NW_MEMBERTYPE as member_type, (SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType, "
					+" (SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,(SELECT CODELST_HIDE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipHide, "
					+" nw_level FROM er_nwsites,er_site,er_codelst where fk_site=pk_site AND pk_codelst=nw_status AND er_site.fk_account=? and pk_nwsites <> ? "
					+" and pk_nwsites in ("+ pks_childNtw +") ORDER BY nw_level";
			System.out.println("chlSql==="+chlSql);
			System.out.println("iaccId==="+iaccId);
			System.out.println("pkNetList.get(i)==="+pkNetList.get(i));
			pstmt = conn.prepareStatement(chlSql);
			pstmt.setInt(1, StringUtil.stringToNum(iaccId));
			pstmt.setInt(2, StringUtil.stringToNum(pkNetList.get(i)));
			rs = pstmt.executeQuery();
			
			
			while (rs.next()) {
				
				code=new Code();
				listNVPair=new ArrayList<NVPair>();
				siteIdentifier=new SiteIdentifier();
				map = ((ObjectMapService)parameters.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(rs.getInt("fk_site")));
				siteIdentifier.setOID(map.getOID());
				siteIdentifier.setPK(Integer.valueOf(rs.getInt("fk_site")));
				code.setPk(String.valueOf(rs.getInt("nw_status")));
				code.setType(rs.getString("stat_type"));
				code.setCode(rs.getString("code"));
				code.setDescription(rs.getString("description"));
				mdJB = new MoreDetailsJB();
				mdDao = new MoreDetailsDao();
				mdDao = mdJB.getMoreDetails(rs.getInt("pk_nwsites"),"org_"+rs.getInt("nw_level"),userBean.getUserGrpDefault());
				idList = mdDao.getId();
				modElementDescList = mdDao.getMdElementDescs();
				modElementDataList = mdDao.getMdElementValues();
				modElementKeysList = mdDao.getMdElementKeys();
				for(int iY=0; iY<idList.size(); iY++){
					if((Integer)idList.get(iY)>0){
					nvpair = new NVPair();
					nvpair.setKey((String) modElementKeysList.get(iY));
					nvpair.setValue((String)modElementDataList.get(iY));
					listNVPair.add(nvpair);
					}
				}
				
				if(rs.getInt("nw_level")==1){
					sitelvl1=new Site();
					sitelvl1.setSiteIdentifier(siteIdentifier);
					sitelvl1.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl1.setName(rs.getString("site_name"));
					sitelvl1.setDescription(rs.getString("site_info"));
					sitelvl1.setSiteStatus(code);
					sitelvl1.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setPk(rs.getString("member_type"));
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl1.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl1.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					sitelvl1.setMoreSiteDetailsFields(listNVPair);
					siteslvl1.add(sitelvl1);
					//siteslvl2=new ArrayList<Site>();
					psiteslvl0.setSites(siteslvl1);
					site.setSites(psiteslvl0);
				}
				else if(rs.getInt("nw_level")==2){
					sitelvl2=new Site();
					sitelvl2.setSiteIdentifier(siteIdentifier);
					sitelvl2.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl2.setName(rs.getString("site_name"));
					sitelvl2.setDescription(rs.getString("site_info"));
					System.out.println("level 2 description==="+sitelvl2.getDescription());
					sitelvl2.setSiteStatus(code);
					sitelvl2.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setPk(rs.getString("member_type"));
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl2.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl2.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					sitelvl2.setMoreSiteDetailsFields(listNVPair);
					siteslvl2.add(sitelvl2);
					//siteslvl3=new ArrayList<Site>();
					if(sitelvl1!=null){
						psiteslvl1.setSites(siteslvl2);
						sitelvl1.setSites(psiteslvl1);
					}else{
						psiteslvl0.setSites(siteslvl2);
						site.setSites(psiteslvl0);
					}
					
					
				}
				else if(rs.getInt("nw_level")==3){
					sitelvl3=new Site();
					sitelvl3.setSiteIdentifier(siteIdentifier);
					sitelvl3.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl3.setName(rs.getString("site_name"));
					sitelvl3.setDescription(rs.getString("site_info"));
					System.out.println("level 3 description==="+sitelvl3.getDescription());
					sitelvl3.setSiteStatus(code);
					sitelvl3.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setPk(rs.getString("member_type"));
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl3.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl3.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					sitelvl3.setMoreSiteDetailsFields(listNVPair);
					siteslvl3.add(sitelvl3);
					//siteslvl4=new ArrayList<Site>();
					if(sitelvl2!=null){
						psiteslvl2.setSites(siteslvl3);
						sitelvl2.setSites(psiteslvl2);
					}else{
						psiteslvl0.setSites(siteslvl3);
						site.setSites(psiteslvl0);
					}
				}
				else if(rs.getInt("nw_level")==4){
					sitelvl4=new Site();
					sitelvl4.setSiteIdentifier(siteIdentifier);
					sitelvl4.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl4.setName(rs.getString("site_name"));
					sitelvl4.setDescription(rs.getString("site_info"));
					sitelvl4.setSiteStatus(code);
					sitelvl4.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setPk(rs.getString("member_type"));
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl4.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl4.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					sitelvl4.setMoreSiteDetailsFields(listNVPair);
					siteslvl4.add(sitelvl4);
					//siteslvl5=new ArrayList<Site>();
					if(sitelvl3!=null){
						psiteslvl3.setSites(siteslvl4);
						sitelvl3.setSites(psiteslvl3);
					}else{
						psiteslvl0.setSites(siteslvl4);
						site.setSites(psiteslvl0);
					}
					
				}
				else if(rs.getInt("nw_level")==5){
					sitelvl5=new Site();
					sitelvl5.setSiteIdentifier(siteIdentifier);
					sitelvl5.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl5.setName(rs.getString("site_name"));
					sitelvl5.setDescription(rs.getString("site_info"));
					sitelvl5.setSiteStatus(code);
					sitelvl5.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setPk(rs.getString("member_type"));
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl5.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl5.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					sitelvl5.setMoreSiteDetailsFields(listNVPair);
					siteslvl5.add(sitelvl5);
					//siteslvl6=new ArrayList<Site>();
					if(sitelvl4!=null){
						psiteslvl4.setSites(siteslvl5);
						sitelvl4.setSites(psiteslvl4);
					}else{
						psiteslvl0.setSites(siteslvl5);
						site.setSites(psiteslvl0);
					}
				}
				else if(rs.getInt("nw_level")==6){
					sitelvl6=new Site();
					siteIdentifier=new SiteIdentifier();
					sitelvl6.setSiteIdentifier(siteIdentifier);
					sitelvl6.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl6.setName(rs.getString("site_name"));
					sitelvl6.setDescription(rs.getString("site_info"));
					sitelvl6.setSiteStatus(code);
					sitelvl6.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("SiteRelationshipType")!=null){
					code=new Code();
					code.setPk(rs.getString("member_type"));
					code.setType(rs.getString("SiteRelationshipType"));
					code.setCode(rs.getString("SiteRelationshipSubType"));
					code.setDescription(rs.getString("SiteRelationshipDesc"));
					sitelvl6.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl6.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					sitelvl6.setMoreSiteDetailsFields(listNVPair);
					siteslvl6.add(sitelvl6);
					if(sitelvl5!=null){
						psiteslvl5.setSites(siteslvl6);
						sitelvl5.setSites(psiteslvl5);
					}else{
						psiteslvl0.setSites(siteslvl6);
						site.setSites(psiteslvl0);
					}
				}
				}
			}
			siteList.add(site);
		}
		sitesList.add((ArrayList<Site>) siteList);
		}
	
	}
}catch(OperationException ex){
	throw ex;
}catch(SQLException ex){
	ex.printStackTrace();
}catch (Exception e) {
	e.printStackTrace();
} finally {
	try {
		rs.close();
		pstmt.close();
		conn.close();
	} catch (SQLException e) {
		e.printStackTrace();
	}
}
}
public void getNetworkSiteChildren(SiteLevelDetails siteLevelDetails, Map<String, Object> parameters) throws OperationException{
	// TODO Auto-generated method stub
	Connection conn=null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	int count=0;
	String str = "";
	String pk_main_network = "";
	String networkPK = (siteLevelDetails.getNetworkPK()!=null)?siteLevelDetails.getNetworkPK():"";
	String networkName = (siteLevelDetails.getNetworkName()!=null)?siteLevelDetails.getNetworkName():"";
	String sitePK = (siteLevelDetails.getSitePK()!=null)?siteLevelDetails.getSitePK():"";
	String siteName = (siteLevelDetails.getSiteName()!=null)?siteLevelDetails.getSiteName():"";
	String level = (siteLevelDetails.getLevel()!=null)?siteLevelDetails.getLevel():"";
	String relationshipPK = (siteLevelDetails.getRelationshipPK()!=null)?siteLevelDetails.getRelationshipPK():"";
	String iaccId = parameters.get(KEY_ACCOUNT_ID).toString();
	if(!relationshipPK.equals("")){
		String getPrimaryNetwork="select fk_nwsites_main,nw_level FROM er_nwsites WHERE pk_nwsites="+ EJBUtil.stringToNum(relationshipPK);
		try {
			conn = CommonDAO.getConnection();
			pstmt = conn.prepareStatement(getPrimaryNetwork);
			rs=pstmt.executeQuery();
			while(rs.next()){
				pk_main_network=String.valueOf(rs.getInt("fk_nwsites_main"));
				level=String.valueOf(rs.getInt("nw_level"));
			}
			System.out.println("new condition pk_main_network==="+pk_main_network);
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	if(pk_main_network.equals("") && !networkPK.equals("")){
		pk_main_network = networkPK;
	}
	System.out.println("final pk_main_network==="+pk_main_network);
	
	 	String str1 = "select pk_nwsites,fk_site,site_name ,site_info, site_info as network_info,nw_status,site_name as network_name,ctep_id, er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden,er_nwsites.NW_MEMBERTYPE as member_type,(SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType,(SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,nw_level from er_nwsites,er_site,er_codelst "+
	 				  " where pk_site=fk_site and pk_codelst=nw_status and er_site.fk_account=? and nw_level=0 ";
	 	if(!(networkName.equals(""))){
	 			 	 str1=str1+" and lower(site_name) = lower('"+networkName+"') ";
	 		}
	 	//if(networkName.equals("") && pk_main_network.length()>0)
	 	if(pk_main_network.length()>0)
	 	{
	 		str1=str1+" and pk_nwsites in("+pk_main_network+") ";
	 	}
	 	System.out.println("str1==="+str1);
try {
	conn = CommonDAO.getConnection();
	pstmt = conn.prepareStatement(str1);
	pstmt.setInt(1, StringUtil.stringToNum(iaccId));
	rs = pstmt.executeQuery();
	Code code=null;
	while (rs.next()) {
		code=new Code();
		pkNetList.add(String.valueOf(rs.getInt("pk_nwsites")));
		fkSiteList.add(String.valueOf(rs.getInt("fk_site")));
		if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
			pkRelnShipType.add(rs.getString("pk_nwsites"));
		}
		else{
			pkRelnShipType.add("0");
		}
		netNameList.add(rs.getString("network_name"));
		netInfoList.add(rs.getString("network_info"));
		code.setPk(String.valueOf(rs.getInt("nw_status")));
		code.setType(rs.getString("stat_type"));
		code.setCode(rs.getString("code"));
		code.setDescription(rs.getString("description"));
		netStatList.add(code);
		if(rs.getString("SiteRelationshipType")!=null){
		code=new Code();
		code.setPk(String.valueOf(rs.getInt("member_type")));
		code.setType(rs.getString("SiteRelationshipType"));
		code.setCode(rs.getString("SiteRelationshipSubType"));
		code.setDescription(rs.getString("SiteRelationshipDesc"));
		siteRelnShipType.add(code);
		}
		else{
			siteRelnShipType.add(null);
		}
		siteNameList.add(rs.getString("site_name"));
		siteInfoList.add(rs.getString("site_info"));
		netLevelList.add(String.valueOf(rs.getInt("nw_level")));
		if(rs.getString("ctep_id")!=null){
			ctepList.add(rs.getString("ctep_id"));
		}else{
			ctepList.add("");
		}
		count++;
	}
	if(count==0)
	{
		this.response=new ResponseHolder();
		this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
		this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "No data found")); 
		throw new OperationException();
	}
	String chlSql="";
	Site site=null;
	SiteIdentifier siteIdentifier=null;
	ObjectMap map=null;
	Site sitelvl1=null;
	Site sitelvl2=null;
	Site sitelvl3=null;
	Site sitelvl4=null;
	Site sitelvl5=null;
	Site sitelvl6=null;
	UserBean userBean=(UserBean)parameters.get("callingUser");
	List<Site> siteslvl1=null;
	List<Site> siteslvl2=null;
	List<Site> siteslvl3=null;
	List<Site> siteslvl4=null;
	List<Site> siteslvl5=null;
	List<Site> siteslvl6=null;
	Sites psiteslvl0=null;
	Sites psiteslvl1=null;
	Sites psiteslvl2=null;
	Sites psiteslvl3=null;
	Sites psiteslvl4=null;
	Sites psiteslvl5=null;
	MoreDetailsJB mdJB=null;
	MoreDetailsDao mdDao;
	ArrayList idList = new ArrayList();
	ArrayList modElementDescList = new ArrayList();
	ArrayList modElementDataList = new ArrayList();
	ArrayList modElementKeysList = new ArrayList();
	List<NVPair> listNVPair=null;
	NVPair nvpair = null;
	List<Site> siteList=null;
	String net_pks = "";
	for(int i=0;i<pkNetList.size();i++){
		
		//siteList.add(site);
		
		//chlSql="select pk_nwsites,fk_site,lower(site_name) site_name,er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden,nw_memberType,(SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType,(SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,(SELECT CODELST_HIDE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipHide, nw_level+1 as nw_level from er_nwsites,er_site,er_codelst where pk_site=fk_site and pk_codelst=nw_status "+
		//	" and er_site.fk_account=? and FK_NWSITES_MAIN=? ";
		String pks_childNtw="";
		
		//chlSql="select pk_nwsites,site_name from er_nwsites,er_site,er_codelst where pk_site=fk_site and pk_codelst=nw_status "+
		//		" and er_site.fk_account=? and FK_NWSITES_MAIN=? ";
		
	List<String> relationshipPKList = new ArrayList<String>();
		if(relationshipPK.equals("")){
		String getRelationshipPk = "select pk_nwsites from er_nwsites,er_site where (fk_nwsites_main="+StringUtil.stringToNum(pkNetList.get(i))+" or fk_nwsites_main is null) and pk_site=fk_site";
		if(!siteName.equals("")){
			getRelationshipPk=getRelationshipPk+ " and lower(site_name) = lower('"+siteName+"')";
		}
		if(!sitePK.equals("")){
			getRelationshipPk=getRelationshipPk+ " and fk_site = "+StringUtil.stringToNum(sitePK);
		}
		if(!level.equals("")){
			getRelationshipPk=getRelationshipPk+ " and nw_level = "+(StringUtil.stringToNum(level));
		}
		getRelationshipPk=getRelationshipPk+ " order by pk_nwsites";
		try {
			pstmt = conn.prepareStatement(getRelationshipPk);
			rs=pstmt.executeQuery();
			while(rs.next()){
				relationshipPKList.add(String.valueOf(rs.getInt("pk_nwsites")));
			}
			System.out.println("getRelationshipPk==="+getRelationshipPk);
			System.out.println("relationshipPK size==="+relationshipPKList.size());
		}catch(Exception e){
			e.printStackTrace();
		}
	}else{
		relationshipPKList.add(relationshipPK);
	}
		
	if(relationshipPKList.size()>0){
		siteList=new ArrayList<Site>();
		
		for(String relationPK: relationshipPKList){
			siteslvl1=new ArrayList<Site>();
			siteslvl2=new ArrayList<Site>();
			siteslvl3=new ArrayList<Site>();
			siteslvl4=new ArrayList<Site>();
			siteslvl5=new ArrayList<Site>();
			siteslvl6=new ArrayList<Site>();
			psiteslvl0=new Sites();
			psiteslvl1=new Sites();
			psiteslvl2=new Sites();
			psiteslvl3=new Sites();
			psiteslvl4=new Sites();
			psiteslvl5=new Sites();
			
			listNVPair=new ArrayList<NVPair>();
			site=new Site();
			siteIdentifier=new SiteIdentifier();
			map = ((ObjectMapService)parameters.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(fkSiteList.get(i)));
			siteIdentifier.setOID(map.getOID());
			siteIdentifier.setPK(Integer.valueOf(fkSiteList.get(i)));
			site.setSiteIdentifier(siteIdentifier);
			site.setLevel(String.valueOf(netLevelList.get(i)));
			site.setName(siteNameList.get(i));
			site.setDescription(siteInfoList.get(i));
			site.setSiteStatus(netStatList.get(i));
			if(!pkRelnShipType.get(i).equals("0") && siteRelnShipType.get(i)!=null){
			site.setRelationshipPK(pkRelnShipType.get(i));
			site.setSiteRelationShipType(siteRelnShipType.get(i));
			
			}
			if(!ctepList.get(i).equals("")){
				site.setCtepId(ctepList.get(i));
			}
			//if(moreSiteDetails==null){
			mdJB = new MoreDetailsJB();
			mdDao = new MoreDetailsDao();
			mdDao = mdJB.getMoreDetails(EJBUtil.stringToNum(pkNetList.get(i)),"org_"+EJBUtil.stringToNum(netLevelList.get(i)),userBean.getUserGrpDefault());
			idList = mdDao.getId();
			modElementDescList = mdDao.getMdElementDescs();
			modElementDataList = mdDao.getMdElementValues();
			modElementKeysList = mdDao.getMdElementKeys();
			for(int iY=0; iY<idList.size(); iY++){
				if((Integer)idList.get(iY)>0){
				nvpair = new NVPair();
				nvpair.setKey((String) modElementKeysList.get(iY));
				nvpair.setValue((String)modElementDataList.get(iY));
				listNVPair.add(nvpair);
				}
			}
			if(net_pks.equals("")){
				net_pks= pkNetList.get(i).toString();
			}
			//}
			/*else{		
					listNVPair=new UserServiceImpl().getNetworkMoreDetailValue(pkNetList.get(i),keySearch,(EJBUtil.stringToNum(netLevelList.get(i))-1),iaccId);
				}*/
			
			site.setMoreSiteDetailsFields(listNVPair);

			
			String immediateChildPKs = "";	
			String getImmediateChildPKs = "select rowtocol('select pk_nwsites from er_nwsites where fk_nwsites_main="+StringUtil.stringToNum(pkNetList.get(i))+" and fk_nwsites in ("+relationPK+") order by pk_nwsites') as pk_nwsites from dual";
				try {
					pstmt = conn.prepareStatement(getImmediateChildPKs);
					rs=pstmt.executeQuery();
					while(rs.next()){
						if(rs.getString("pk_nwsites")!=null){
							immediateChildPKs=rs.getString("pk_nwsites");
						}
					}
					System.out.println("getimmediateChildPKs==="+getImmediateChildPKs);
					System.out.println("immediateChildPKs==="+immediateChildPKs);
				}catch(Exception e){
					e.printStackTrace();
				}
			
					if(!immediateChildPKs.equals("")){
						pks_childNtw = relationPK + "," + immediateChildPKs;
					}else{
						pks_childNtw = relationPK;
					}
					System.out.println("pks_childNtw==="+pks_childNtw);
					if(!pks_childNtw.equals("0") && !pks_childNtw.equals(""))
					{
					chlSql="SELECT distinct pk_nwsites,site_name,site_info,fk_site,ctep_id,er_codelst.CODELST_DESC AS description,er_codelst.codelst_type AS stat_type,er_codelst.codelst_subtyp AS code,er_codelst.codelst_hide AS stat_hidden, "
							+" nw_status, er_nwsites.NW_MEMBERTYPE as member_type, (SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType, "
							+" (SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,(SELECT CODELST_HIDE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipHide, "
							+" nw_level FROM er_nwsites,er_site,er_codelst where fk_site=pk_site AND pk_codelst=nw_status AND er_site.fk_account=? and pk_nwsites <> ? "
							+" and pk_nwsites in ("+ pks_childNtw +") ORDER BY nw_level";
					System.out.println("chlSql==="+chlSql);
					System.out.println("iaccId==="+iaccId);
					System.out.println("pkNetList.get(i)==="+pkNetList.get(i));
					pstmt = conn.prepareStatement(chlSql);
					pstmt.setInt(1, StringUtil.stringToNum(iaccId));
					pstmt.setInt(2, StringUtil.stringToNum(pkNetList.get(i)));
					rs = pstmt.executeQuery();
					
					
					while (rs.next()) {
						
						code=new Code();
						listNVPair=new ArrayList<NVPair>();
						siteIdentifier=new SiteIdentifier();
						map = ((ObjectMapService)parameters.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(rs.getInt("fk_site")));
						siteIdentifier.setOID(map.getOID());
						siteIdentifier.setPK(Integer.valueOf(rs.getInt("fk_site")));
						code.setPk(String.valueOf(rs.getInt("nw_status")));
						code.setType(rs.getString("stat_type"));
						code.setCode(rs.getString("code"));
						code.setDescription(rs.getString("description"));
						mdJB = new MoreDetailsJB();
						mdDao = new MoreDetailsDao();
						mdDao = mdJB.getMoreDetails(rs.getInt("pk_nwsites"),"org_"+rs.getInt("nw_level"),userBean.getUserGrpDefault());
						idList = mdDao.getId();
						modElementDescList = mdDao.getMdElementDescs();
						modElementDataList = mdDao.getMdElementValues();
						modElementKeysList = mdDao.getMdElementKeys();
						for(int iY=0; iY<idList.size(); iY++){
							if((Integer)idList.get(iY)>0){
							nvpair = new NVPair();
							nvpair.setKey((String) modElementKeysList.get(iY));
							nvpair.setValue((String)modElementDataList.get(iY));
							listNVPair.add(nvpair);
							}
						}
						
						if(rs.getInt("nw_level")==1){
							sitelvl1=new Site();
							sitelvl1.setSiteIdentifier(siteIdentifier);
							sitelvl1.setLevel(String.valueOf(rs.getInt("nw_level")));
							sitelvl1.setName(rs.getString("site_name"));
							sitelvl1.setDescription(rs.getString("site_info"));
							sitelvl1.setSiteStatus(code);
							sitelvl1.setCtepId(rs.getString("ctep_id"));
							if(rs.getString("SiteRelationshipType")!=null){
							code=new Code();
							code.setPk(rs.getString("member_type"));
							code.setType(rs.getString("SiteRelationshipType"));
							code.setCode(rs.getString("SiteRelationshipSubType"));
							code.setDescription(rs.getString("SiteRelationshipDesc"));
							sitelvl1.setSiteRelationShipType(code);
							}
							if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
								sitelvl1.setRelationshipPK(rs.getString("pk_nwsites"));
							}
							sitelvl1.setMoreSiteDetailsFields(listNVPair);
							siteslvl1.add(sitelvl1);
							//siteslvl2=new ArrayList<Site>();
							psiteslvl0.setSites(siteslvl1);
							site.setSites(psiteslvl0);
						}
						else if(rs.getInt("nw_level")==2){
							sitelvl2=new Site();
							sitelvl2.setSiteIdentifier(siteIdentifier);
							sitelvl2.setLevel(String.valueOf(rs.getInt("nw_level")));
							sitelvl2.setName(rs.getString("site_name"));
							sitelvl2.setDescription(rs.getString("site_info"));
							System.out.println("level 2 description==="+sitelvl2.getDescription());
							sitelvl2.setSiteStatus(code);
							sitelvl2.setCtepId(rs.getString("ctep_id"));
							if(rs.getString("SiteRelationshipType")!=null){
							code=new Code();
							code.setPk(rs.getString("member_type"));
							code.setType(rs.getString("SiteRelationshipType"));
							code.setCode(rs.getString("SiteRelationshipSubType"));
							code.setDescription(rs.getString("SiteRelationshipDesc"));
							sitelvl2.setSiteRelationShipType(code);
							}
							if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
								sitelvl2.setRelationshipPK(rs.getString("pk_nwsites"));
							}
							sitelvl2.setMoreSiteDetailsFields(listNVPair);
							siteslvl2.add(sitelvl2);
							//siteslvl3=new ArrayList<Site>();
							if(sitelvl1!=null){
								psiteslvl1.setSites(siteslvl2);
								sitelvl1.setSites(psiteslvl1);
							}else{
								psiteslvl0.setSites(siteslvl2);
								site.setSites(psiteslvl0);
							}
							
							
						}
						else if(rs.getInt("nw_level")==3){
							sitelvl3=new Site();
							sitelvl3.setSiteIdentifier(siteIdentifier);
							sitelvl3.setLevel(String.valueOf(rs.getInt("nw_level")));
							sitelvl3.setName(rs.getString("site_name"));
							sitelvl3.setDescription(rs.getString("site_info"));
							System.out.println("level 3 description==="+sitelvl3.getDescription());
							sitelvl3.setSiteStatus(code);
							sitelvl3.setCtepId(rs.getString("ctep_id"));
							if(rs.getString("SiteRelationshipType")!=null){
							code=new Code();
							code.setPk(rs.getString("member_type"));
							code.setType(rs.getString("SiteRelationshipType"));
							code.setCode(rs.getString("SiteRelationshipSubType"));
							code.setDescription(rs.getString("SiteRelationshipDesc"));
							sitelvl3.setSiteRelationShipType(code);
							}
							if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
								sitelvl3.setRelationshipPK(rs.getString("pk_nwsites"));
							}
							sitelvl3.setMoreSiteDetailsFields(listNVPair);
							siteslvl3.add(sitelvl3);
							//siteslvl4=new ArrayList<Site>();
							if(sitelvl2!=null){
								psiteslvl2.setSites(siteslvl3);
								sitelvl2.setSites(psiteslvl2);
							}else{
								psiteslvl0.setSites(siteslvl3);
								site.setSites(psiteslvl0);
							}
						}
						else if(rs.getInt("nw_level")==4){
							sitelvl4=new Site();
							sitelvl4.setSiteIdentifier(siteIdentifier);
							sitelvl4.setLevel(String.valueOf(rs.getInt("nw_level")));
							sitelvl4.setName(rs.getString("site_name"));
							sitelvl4.setDescription(rs.getString("site_info"));
							sitelvl4.setSiteStatus(code);
							sitelvl4.setCtepId(rs.getString("ctep_id"));
							if(rs.getString("SiteRelationshipType")!=null){
							code=new Code();
							code.setPk(rs.getString("member_type"));
							code.setType(rs.getString("SiteRelationshipType"));
							code.setCode(rs.getString("SiteRelationshipSubType"));
							code.setDescription(rs.getString("SiteRelationshipDesc"));
							sitelvl4.setSiteRelationShipType(code);
							}
							if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
								sitelvl4.setRelationshipPK(rs.getString("pk_nwsites"));
							}
							sitelvl4.setMoreSiteDetailsFields(listNVPair);
							siteslvl4.add(sitelvl4);
							//siteslvl5=new ArrayList<Site>();
							if(sitelvl3!=null){
								psiteslvl3.setSites(siteslvl4);
								sitelvl3.setSites(psiteslvl3);
							}else{
								psiteslvl0.setSites(siteslvl4);
								site.setSites(psiteslvl0);
							}
							
						}
						else if(rs.getInt("nw_level")==5){
							sitelvl5=new Site();
							sitelvl5.setSiteIdentifier(siteIdentifier);
							sitelvl5.setLevel(String.valueOf(rs.getInt("nw_level")));
							sitelvl5.setName(rs.getString("site_name"));
							sitelvl5.setDescription(rs.getString("site_info"));
							sitelvl5.setSiteStatus(code);
							sitelvl5.setCtepId(rs.getString("ctep_id"));
							if(rs.getString("SiteRelationshipType")!=null){
							code=new Code();
							code.setPk(rs.getString("member_type"));
							code.setType(rs.getString("SiteRelationshipType"));
							code.setCode(rs.getString("SiteRelationshipSubType"));
							code.setDescription(rs.getString("SiteRelationshipDesc"));
							sitelvl5.setSiteRelationShipType(code);
							}
							if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
								sitelvl5.setRelationshipPK(rs.getString("pk_nwsites"));
							}
							sitelvl5.setMoreSiteDetailsFields(listNVPair);
							siteslvl5.add(sitelvl5);
							//siteslvl6=new ArrayList<Site>();
							if(sitelvl4!=null){
								psiteslvl4.setSites(siteslvl5);
								sitelvl4.setSites(psiteslvl4);
							}else{
								psiteslvl0.setSites(siteslvl5);
								site.setSites(psiteslvl0);
							}
						}
						else if(rs.getInt("nw_level")==6){
							sitelvl6=new Site();
							siteIdentifier=new SiteIdentifier();
							sitelvl6.setSiteIdentifier(siteIdentifier);
							sitelvl6.setLevel(String.valueOf(rs.getInt("nw_level")));
							sitelvl6.setName(rs.getString("site_name"));
							sitelvl6.setDescription(rs.getString("site_info"));
							sitelvl6.setSiteStatus(code);
							sitelvl6.setCtepId(rs.getString("ctep_id"));
							if(rs.getString("SiteRelationshipType")!=null){
							code=new Code();
							code.setPk(rs.getString("member_type"));
							code.setType(rs.getString("SiteRelationshipType"));
							code.setCode(rs.getString("SiteRelationshipSubType"));
							code.setDescription(rs.getString("SiteRelationshipDesc"));
							sitelvl6.setSiteRelationShipType(code);
							}
							if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
								sitelvl6.setRelationshipPK(rs.getString("pk_nwsites"));
							}
							sitelvl6.setMoreSiteDetailsFields(listNVPair);
							siteslvl6.add(sitelvl6);
							if(sitelvl5!=null){
								psiteslvl5.setSites(siteslvl6);
								sitelvl5.setSites(psiteslvl5);
							}else{
								psiteslvl0.setSites(siteslvl6);
								site.setSites(psiteslvl0);
							}
						}
						}
					}
					siteList.add(site);
				}
		
		sitesList.add((ArrayList<Site>) siteList);
		}
	}
}catch(OperationException ex){
	throw ex;
}catch(SQLException ex){
	ex.printStackTrace();
}catch (Exception e) {
	e.printStackTrace();
} finally {
	try {
		rs.close();
		pstmt.close();
		conn.close();
	} catch (SQLException e) {
		e.printStackTrace();
	}
}
}

public void getNetworkLevelSites(NetworkLevelSite networklevelsite, HashMap<String, Object> params) throws OperationException {

	UserAgentRObj userAgent = (UserAgentRObj) params.get("userAgent");
	String networkName = (String) params.get("networkName");
	String networkPk = (String) params.get("networkPk");
	String networkLevel = (String) params.get("networkLevel");
	UserBean userBean=(UserBean)params.get("callingUser");
	String iaccId = (String)params.get(KEY_ACCOUNT_ID);
	Connection conn=null;
	PreparedStatement pstmt=null;
	ResultSet rs=null;
	NetworkRole roles=null;
	Code code = new Code();
	int count=0;
	int count1=0;
	String pk_main_network="";
	
	try {
		conn = CommonDAO.getConnection();

		
		StringBuffer sqlbuff = new StringBuffer();
		
		sqlbuff.append(" SELECT distinct nvl(FK_NWSITES_MAIN,pk_nwsites) fk_nwsites_main FROM er_nwsites,er_site WHERE er_site.fk_account=? and pk_site=fk_site ");
		
		if(!(networkPk.equals(""))){
			sqlbuff.append(" and nvl(FK_NWSITES_MAIN,pk_nwsites)="+networkPk+"");
		}
		
		if(!networkName.equals("") && networkPk.equals("")){
			sqlbuff.append(" and nvl(FK_NWSITES_MAIN,pk_nwsites) in (select pk_nwsites from er_nwsites,er_site where pk_site=fk_site and site_name like '"+networkName+"' and nw_level=0)" );
		}
		if(!networkLevel.equals("")){
			sqlbuff.append(" and nw_level="+networkLevel+" ");
		}
		
		
		pk_main_network = pkMainNetwork(sqlbuff.toString(),iaccId);
		
		if(pk_main_network.equals("0")){
			this.response=new ResponseHolder();
			this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
			this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "No data found")); 
			throw new OperationException();
		}
		
		StringBuffer sqlbuff1 = new StringBuffer();
		sqlbuff1.append(" SELECT pk_nwsites, fk_site,site_info as network_info, site_name ,(SELECT CODELST_DESC  FROM er_codelst  WHERE PK_CODELST=er_nwsites.NW_STATUS )   network_status,site_name AS network_name, ctep_id, er_codelst.CODELST_DESC   AS description,  er_codelst.codelst_type   AS stat_type, er_codelst.codelst_subtyp AS code, er_codelst.codelst_hide   AS stat_hidden, (SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE)");
		sqlbuff1.append("  AS SiteRelationshipDesc, (SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE ) AS SiteRelationshipType, (SELECT CODELST_SUBTYP FROM er_codelst  WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE)   AS SiteRelationshipSubType,  NW_LEVEL AS nw_level FROM er_nwsites, er_site, er_codelst  where pk_site=fk_site and pk_codelst=nw_status and er_site.fk_account=? and nw_level=0");  
				 	
		 	if(pk_main_network.length()>0 && !pk_main_network.equals("0"))
		 	{
		 		sqlbuff1.append(" and pk_nwsites in("+pk_main_network+") ");
		 	}

	
		pstmt = conn.prepareStatement(sqlbuff1.toString());
		pstmt.setInt(1, StringUtil.stringToNum(iaccId));
		rs=pstmt.executeQuery();
		Network network;
		while(rs.next()){
			network=new Network();
			code=new Code();
			pkNetList.add(String.valueOf(rs.getInt("pk_nwsites")));
			fkSiteList.add(String.valueOf(rs.getInt("fk_site")));
			netNameList.add(rs.getString("network_name"));
			netInfoList.add(rs.getString("network_info"));
			code.setType(rs.getString("stat_type"));
			code.setCode(rs.getString("code"));
			code.setDescription(rs.getString("description"));
			netStatList.add(code);
			if(rs.getString("SiteRelationshipType")!=null){
			code=new Code();
			code.setType(rs.getString("SiteRelationshipType"));
			code.setCode(rs.getString("SiteRelationshipSubType"));
			code.setDescription(rs.getString("SiteRelationshipDesc"));
			siteRelnShipType.add(code);
			}
			else{
				siteRelnShipType.add(null);
			}
			siteNameList.add(rs.getString("site_name"));
			netLevelList.add(String.valueOf(rs.getInt("nw_level")));
			siteInfoList.add(rs.getString("network_info"));
			if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
				pkRelnShipType.add(rs.getString("pk_nwsites"));
			}
			else{
				pkRelnShipType.add("0");
			}
			if(rs.getString("ctep_id")!=null){
				ctepList.add(rs.getString("ctep_id"));
			}else{
				ctepList.add("");
			}
			count++;
			
			
		}
		if(count==0)
		{
			this.response=new ResponseHolder();
			this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
			this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "No data found")); 
			throw new OperationException();
		}	
		List<Site> siteList=null;
		Site site=null;
		SiteIdentifier siteIdentifier=null;
		ObjectMap map=null;
		String child_Pk="";
		for(int i=0;i<pkNetList.size();i++){
			siteList=new ArrayList<Site>();
			site=new Site();
			siteIdentifier=new SiteIdentifier();
			map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(fkSiteList.get(i)));
			siteIdentifier.setOID(map.getOID());
			siteIdentifier.setPK(Integer.valueOf(fkSiteList.get(i)));
			site.setSiteIdentifier(siteIdentifier);
			site.setLevel(String.valueOf(netLevelList.get(i)));
			site.setDescription(siteInfoList.get(i));
			site.setName(siteNameList.get(i));
			site.setSiteStatus(netStatList.get(i));
			if(!pkRelnShipType.get(i).equals("0") && siteRelnShipType.get(i)!=null){
				site.setRelationshipPK(pkRelnShipType.get(i));
				site.setSiteRelationShipType(siteRelnShipType.get(i));
				}
			if(!ctepList.get(i).equals("")){
				site.setCtepId(ctepList.get(i));
			}
		StringBuffer sqlbuff2 = new StringBuffer();
		sqlbuff2.append("select pk_nwsites, fk_nwsites, fk_site,site_info, site_name,ctep_id, nw_status,er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden,(SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType,(SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,(SELECT CODELST_HIDE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipHide, nw_level as nw_level, NW_MEMBERTYPE as relationShip_pk ");
		sqlbuff2.append(" FROM er_nwsites,er_site,er_codelst WHERE pk_site=fk_site and pk_codelst=nw_status and er_site.fk_account=? AND  er_nwsites.FK_NWSITES_MAIN=? ");
		if(!networkLevel.equals("")){
			sqlbuff2.append(" and nw_level="+networkLevel+" ");
		}
		
		pstmt = conn.prepareStatement(sqlbuff2.toString());
		pstmt.setInt(1, StringUtil.stringToNum(iaccId));
		pstmt.setInt(2, StringUtil.stringToNum(pkNetList.get(i)));
		rs=pstmt.executeQuery();
		
		Site site1=null;
		Sites sites=null;
		List<Site> sitelst=null;
		sitelst=new ArrayList<Site>();
		child_Pk="";
		while (rs.next()) {
			site1=new Site();
			sites=new Sites();
			code=new Code();
			siteIdentifier=new SiteIdentifier();
			map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(rs.getInt("fk_site")));
			siteIdentifier.setOID(map.getOID());
			siteIdentifier.setPK(Integer.valueOf(rs.getInt("fk_site")));
			site1.setLevel(String.valueOf(rs.getInt("nw_level")));
			site1.setSiteIdentifier(siteIdentifier);
			site1.setName(rs.getString("site_name"));
			site1.setDescription(rs.getString("site_info"));
			code.setPk(String.valueOf(rs.getInt("nw_status")));
			code.setType(rs.getString("stat_type"));
			code.setCode(rs.getString("code"));
			code.setDescription(rs.getString("description"));
			site1.setSiteStatus(code);
			site1.setCtepId(rs.getString("ctep_id"));
			
		if(rs.getString("SiteRelationshipType")!=null){
		code=new Code();
		code.setType(rs.getString("SiteRelationshipType"));
		code.setCode(rs.getString("SiteRelationshipSubType"));
		code.setDescription(rs.getString("SiteRelationshipDesc"));
		code.setPk(rs.getString("relationShip_pk"));
		site1.setSiteRelationShipType(code);
		}
		if(rs.getString("pk_nwsites")!=null){
			site1.setRelationshipPK(rs.getString("pk_nwsites"));
		}
		
		sitelst.add(site1);
		sites.setSites(sitelst);
		site.setSites(sites);
		}
		
		siteList.add(site);
		sitesList.add((ArrayList<Site>) siteList);
		
		
		}
	
	}catch(OperationException ex){
		throw ex;
	}catch(SQLException ex){
		ex.printStackTrace();
	}catch (Exception e) {
		e.printStackTrace();
	} finally {
		try {
			if(rs!=null)
			rs.close();
			if(pstmt!=null)
			pstmt.close();
			if(conn!=null)
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}

public UserNetworkSiteResults userNetworkSites(UserNetworkSites userNetworkSites, HashMap<String, Object> params) throws OperationException {
	UserNetworkSiteResults networkSiteSearchResults = new UserNetworkSiteResults();
	Connection conn=null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	Connection conn1=null;
	PreparedStatement pstmt1 = null;
	ResultSet rs1 = null;
	int count=0;
	String iaccId = (String)params.get(KEY_ACCOUNT_ID);
	UserBean userBean=(UserBean)params.get("callingUser");
	UserBean userBeanPk = null;
	UserAgentRObj userAgent = (UserAgentRObj) params.get("userAgent");
	String networkName = (userNetworkSites.getNetworkName()==null)?"":userNetworkSites.getNetworkName();
	String userName = (userNetworkSites.getUserLoginName()==null)?"":userNetworkSites.getUserLoginName();
	int userPk= (userNetworkSites.getUserPK()==null)?0:userNetworkSites.getUserPK();
	int networkPk = (userNetworkSites.getNetworkPK()==null)?0:userNetworkSites.getNetworkPK();
	String pkCode="";
	int userPK=0;
	String userNAME="";
	String str = "";
	String pk_main_network = "";
	int row=0;
	
try {
	
	if(userPk!=0){
    	userBeanPk = userAgent.getUserDetails(userPk);
		if(userBeanPk==null){
			if(userName.equals("")){
			    this.response=new ResponseHolder();
				this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "User PK not found.")); 
				throw new OperationException();	
			}
		}else{
			userNAME=userBeanPk.getUserLoginName();
			if(!userNAME.equalsIgnoreCase(userName) && !userName.equals("") && !userNAME.equals("")){
				this.response=new ResponseHolder();
				this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "User Login name does not match with User PK.")); 
				throw new OperationException();	
			}
		}
	}
				
		if(!userName.equals("")){
			UserDao userDao = new UserDao();
			userDao.getUserValuesByLoginName(userName);
			if(userDao.getUsrIds().size()==0){
				this.response=new ResponseHolder();
				this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "User LoginName not found.")); 
				throw new OperationException();	
			}else
			{
				userPK = (Integer)userDao.getUsrIds().get(0);
				if(userPK!=userPk && userPk!=0 && userPK!=0){
					this.response=new ResponseHolder();
					this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
					this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "User PK does not match with User login name.")); 
					throw new OperationException();	
				}
			}
		}
	
	
	str="SELECT distinct nvl(nws.FK_NWSITES_MAIN,nws.pk_nwsites) fk_nwsites_main FROM er_nwsites nws, er_site s, er_nwusers nwu, er_user u WHERE s.fk_account=? AND s.pk_site=nws.fk_site AND nws.pk_nwsites=nwu.FK_NWSITES AND nwu.fk_user=u.pk_user ";
	
	if(networkPk!=0){
		str=str+" and nvl(nws.FK_NWSITES_MAIN,nws.pk_nwsites)="+networkPk;
	}
	
	if(!networkName.equals("") && networkPk==0){
		str=str+"and nvl(nws.FK_NWSITES_MAIN,nws.pk_nwsites) in (select ns.pk_nwsites from er_nwsites ns, er_site es where es.pk_site=ns.fk_site and es.site_name like '"+networkName+"' and ns.nw_level=0)";
	}
	if(userPk!=0){
		str=str+"AND u.pk_user="+userPk;
	}
	if(!userName.equals("")){
		str=str+"AND u.USR_LOGNAME='"+userName+"' ";
	}
	
	pk_main_network = pkMainNetwork(str,iaccId);
	
	if(pk_main_network.equals("0") && networkPk!=0 && networkName.equals("")){
		this.response=new ResponseHolder();
		this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
		this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Network PK not found")); 
		throw new OperationException();
	}else if(pk_main_network.equals("0") && !networkName.equals("") && networkPk==0){
		this.response=new ResponseHolder();
		this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
		this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Network Name not found")); 
		throw new OperationException();
	}else if(pk_main_network.equals("0")){
		this.response=new ResponseHolder();
		this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
		this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Network not found")); 
		throw new OperationException();
	}
	
	 String str1 = "select pk_nwsites,fk_site,site_name ,site_name as network_name,ctep_id,nvl(site_info,'-') as network_info, er_codelst.pk_codelst as pkStat, er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden, (SELECT pk_codelst FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) as pkRelnType, (SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType,(SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType, nw_level from er_nwsites,er_site,er_codelst "+
	 			" where pk_site=fk_site and pk_codelst=nw_status and er_site.fk_account=? and nw_level=0 ";
	 		 	
	 	if(pk_main_network.length()>0 && !pk_main_network.equals("0"))
	 	{
	 		str1=str1+" and pk_nwsites in("+pk_main_network+") ";
	 	}
	 	if(!networkName.equals("")){
	 		str1=str1+" and site_name like '"+networkName+"'";
	 	}

	conn = CommonDAO.getConnection();
	pstmt = conn.prepareStatement(str1);
	pstmt.setInt(1, StringUtil.stringToNum(iaccId));
	rs = pstmt.executeQuery();
	Code code=null;
	while (rs.next()) {
		code=new Code();
		pkNetList.add(String.valueOf(rs.getInt("pk_nwsites")));
		fkSiteList.add(String.valueOf(rs.getInt("fk_site")));
		if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
			pkRelnShipType.add(rs.getString("pk_nwsites"));
		}
		else{
			pkRelnShipType.add("0");
		}
		netNameList.add(rs.getString("network_name"));
		netInfoList.add(rs.getString("network_info"));
		code.setPk(rs.getString("pkStat"));
		code.setType(rs.getString("stat_type"));
		code.setCode(rs.getString("code"));
		code.setDescription(rs.getString("description"));
		netStatList.add(code);
		if(rs.getString("SiteRelationshipType")!=null){
		code=new Code();
		code.setPk(rs.getString("pkRelnType"));
		code.setType(rs.getString("SiteRelationshipType"));
		code.setCode(rs.getString("SiteRelationshipSubType"));
		code.setDescription(rs.getString("SiteRelationshipDesc"));
		siteRelnShipType.add(code);
		}
		else{
			siteRelnShipType.add(null);
		}
		siteNameList.add(rs.getString("site_name"));
		netLevelList.add(String.valueOf(rs.getInt("nw_level")));
		if(rs.getString("ctep_id")!=null){
			ctepList.add(rs.getString("ctep_id"));
		}else{
			ctepList.add("");
		}
		count=count+1;
	}
	System.out.println("count = " +count);
	if(count==0 && !networkName.equals("")){
		this.response=new ResponseHolder();
		this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
		this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Network not found")); 
		throw new OperationException();
	}
	
	
		
	
	
	String chlSql="";
	UserSite site=null;
	SiteIdentifier siteIdentifier=null;
	ObjectMap map=null;
	UserSite sitelvl=null;
	UserSites sites=null;
	List<UserSite> sitesLvlList=null;
	List<UserSite> siteList=null;
	UserNetworks network = null;
	UserSites sites1=null;
	for(int i=0;i<pkNetList.size();i++){
		System.out.println("i = "+i);
		row=0;
		siteList=new ArrayList<UserSite>();
		site=new UserSite();
		siteIdentifier=new SiteIdentifier();
		map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(fkSiteList.get(i)));
		siteIdentifier.setOID(map.getOID());
		siteIdentifier.setPK(Integer.valueOf(fkSiteList.get(i)));
		site.setSiteIdentifier(siteIdentifier);
		site.setLevel(String.valueOf(netLevelList.get(i)));
		site.setName(siteNameList.get(i));
		if(!netInfoList.get(i).equals("-")){
			site.setDescription(netInfoList.get(i));
		}
		site.setSiteStatus(netStatList.get(i));
		if(!pkRelnShipType.get(i).equals("0") && siteRelnShipType.get(i)!=null){
		site.setRelationshipPK(pkRelnShipType.get(i));
		site.setSiteRelationShipType(siteRelnShipType.get(i));
		}
		if(!ctepList.get(i).equals("")){
			site.setCtepId(ctepList.get(i));
		}

		chlSql="SELECT nws.pk_nwsites,nws.nw_membertype relationship_pk, f_codelst_desc(nws.nw_membertype) relationship_desc, "
				+ "f_codelst_subtyp(nws.nw_membertype) relationship_subtyp, (select codelst_type from er_codelst where "
		 		+ "pk_codelst=nws.NW_MEMBERTYPE) relationship_type, s.site_name network_name, s.pk_site fk_site, nvl(s.site_info,'-') description, "
		 		+ "nws.nw_level nw_level,nws.nw_status, f_codelst_desc(nws.nw_status) nw_status_desc, "
		 		+ "f_codelst_subtyp(nws.nw_status) nw_status_subtyp, (select codelst_type from er_codelst where pk_codelst=nws.nw_status) nw_status_type, "
		 		+ "s.ctep_id  FROM er_nwsites nws, er_nwusers nwu, er_user u, er_site s WHERE nws.pk_nwsites  = nwu.FK_NWSITES "
		 		+ "AND nws.fk_site = s.pk_site AND nwu.fk_user = u.pk_user AND (u.pk_user = ? OR u.USR_LOGNAME =?) "
		 		+ "AND (nws.pk_nwsites in (?) OR nws.FK_NWSITES_MAIN in (?)) AND s.fk_account=? order by nw_level";
																													
	pstmt = conn.prepareStatement(chlSql);
	pstmt.setInt(1, userPk);
	pstmt.setString(2, userName);
	pstmt.setInt(3, StringUtil.stringToNum(pkNetList.get(i)));
	pstmt.setInt(4, StringUtil.stringToNum(pkNetList.get(i)));
	/*if(!networkName.equals("")){
		pstmt.setString(5, networkName);
	}else{
		pstmt.setString(5, netNameList.get(i));
	}*/
	pstmt.setInt(5, StringUtil.stringToNum(iaccId));
	rs = pstmt.executeQuery();
	
	sitesLvlList=new ArrayList<UserSite>();
	while(rs.next()){

		    sites=new UserSites();
			code=new Code();
			siteIdentifier=new SiteIdentifier();
			map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(rs.getInt("fk_site")));
			siteIdentifier.setOID(map.getOID());
			siteIdentifier.setPK(Integer.valueOf(rs.getInt("fk_site")));
			code.setPk(rs.getString("nw_status"));
			code.setType(rs.getString("nw_status_type"));
			code.setCode(rs.getString("nw_status_subtyp"));
			code.setDescription(rs.getString("nw_status_desc"));
			sitelvl=new UserSite();
			sitelvl.setSiteIdentifier(siteIdentifier);
			sitelvl.setLevel(String.valueOf(rs.getInt("nw_level")));
			sitelvl.setName(rs.getString("network_name"));
			if(!rs.getString("description").equals("-")){
				sitelvl.setDescription(rs.getString("description"));
			}
			sitelvl.setSiteStatus(code);
			sitelvl.setCtepId(rs.getString("ctep_id"));
			if(rs.getString("relationship_type")!=null){
				code=new Code();
				code.setPk(rs.getString("relationship_pk"));
				code.setType(rs.getString("relationship_type"));
				code.setCode(rs.getString("relationship_subtyp"));
				code.setDescription(rs.getString("relationship_desc"));
				sitelvl.setSiteRelationShipType(code);
			}
			if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
				sitelvl.setRelationshipPK(rs.getString("pk_nwsites"));
			}
			sitesLvlList.add(sitelvl);
			sites.setSites(sitesLvlList);
			site.setSites(sites);
			row++;
		}
	if(row==0 && network==null && i==pkNetList.size()-1)
	{
		this.response=new ResponseHolder();
		this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
		this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "No data found")); 
		throw new OperationException();
	}	
	if(row>0){
	siteList.add(site);
	sitesLists.add((ArrayList<UserSite>) siteList);
	
	
			network = new UserNetworks();
			sites1 = new UserSites();
			network.setName(netNameList.get(i));
			NetworkIdentifier netIdentifier = new NetworkIdentifier();
			ObjectMap map1 = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_NETWORK, Integer.valueOf(pkNetList.get(i)));
			netIdentifier.setOID(map1.getOID());
			netIdentifier.setPK(Integer.valueOf(pkNetList.get(i)));
			network.setNetworkIdentifier(netIdentifier);
			network.setNetworkStatus(netStatList.get(i));
			network.setNetworkRelationShipType(siteRelnShipType.get(i));
			sites1.setSites(sitesLists.get(i));
			network.setSites(sites1);
			networkSiteSearchResults.addNetwork(network);
	}
	}
	

}catch(OperationException ex){
	throw ex;
}catch (SQLException e) {
	e.printStackTrace();
}catch (Exception e) {
	e.printStackTrace();
} finally {
	try {
		if(rs!=null)
		rs.close();
		if(pstmt!=null)
		pstmt.close();
		if(conn!=null)
		conn.close();
	} catch (SQLException e) {
		e.printStackTrace();
	}
}
return networkSiteSearchResults;

	
}
public void searchUserNetworks(UserNetworksDetail networkuser, HashMap<String, Object> params) throws OperationException {
	Connection conn=null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	ResultSet rs1=null;
	int userLoginPK = 0;
	int userPK=0;
	int userOIDPK=0;
	String pkCode="";
	int count=0;
	String net_pks="";
	String userLoginName="";
	ObjectMap map=null;
	UserBean userBeanPk = null;
	String networkName="";
	String networkPk="";
	String relationShipPK="";
	String sitePK="";
	String siteName="";
	UserAgentRObj userAgent = (UserAgentRObj) params.get("userAgent");
	UserBean userBean=(UserBean)params.get("callingUser");
	String iaccId = (String)params.get(KEY_ACCOUNT_ID);
	//System.out.println("networkuser.getNetworkDetails().getNetworkName()=="+networkuser.getNetworkDetails().getNetworkName());
	NetworkServiceDao networks=new NetworkServiceDao();
System.out.println("Here");

if(networkuser.getPK()!=null ){
	int usr=networkuser.getPK();
	userBeanPk = userAgent.getUserDetails(usr);
	if(userBeanPk==null ){
		if(usr!=0)
		{
		     this.response=new ResponseHolder();
			this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
			this.response.addIssue(new Issue(IssueTypes.USER_NOT_FOUND, "User does not exist.")); 
			throw new OperationException();	
	}
	}else{
		userPK = userBeanPk.getUserId();
	}
}
userLoginName=networkuser.getUserLoginName()==null?"":networkuser.getUserLoginName();
	if(!userLoginName.equals("")){
		UserDao userDao = new UserDao();
		userDao.getUserValuesByLoginName(networkuser.getUserLoginName());
		if(userDao.getUsrIds().size()==0){
			this.response=new ResponseHolder();
			this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
			this.response.addIssue(new Issue(IssueTypes.USER_NOT_FOUND, "User does not exist.")); 
			throw new OperationException();	
		}else
		{
			userLoginPK = (Integer)userDao.getUsrIds().get(0);
		}
	}
//	if (networkuser.getOID() != null){
//		try{
//    ObjectMap objectMap =((ObjectMapService)params.get("objectMapService")).getObjectMapFromId(networkuser.getOID());
//    if(objectMap == null){
//		this.response=new ResponseHolder();
//		this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
//		this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid User Object is required with OID.")); 
//		throw new OperationException();
//    }
//	if(objectMap != null){
//		    UserBean userBeanFromOID = userAgent.getUserDetails(objectMap.getTablePK());
//			if (userBeanFromOID != null && userBeanFromOID.getUserAccountId().equals(iaccId)) {
//				userOIDPK=userBeanFromOID.getUserId();
//			}
//	}}catch(MultipleObjectsFoundException e)
//		{
//			System.err.print(e.getMessage());
//		}
//		
//}	
if( userPK>0 && userLoginPK>0 ){
	if( userPK!=userLoginPK ){
		this.response=new ResponseHolder();
		this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
		this.response.addIssue(new Issue(IssueTypes.USER_NOT_FOUND, "User PK is not matched with User Login Name.")); 
		throw new OperationException();	
	}}
//else if(userPK>0 && userOIDPK>0){
//	
//	if( userPK!=userOIDPK ){
//		this.response=new ResponseHolder();
//		this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
//		this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "User PK is Not Matched with User OID.")); 
//		throw new OperationException();	
//	}}
//else if(userOIDPK>0 && userLoginPK>0 ){
//	if( userOIDPK!=userLoginPK ){
//		this.response=new ResponseHolder();
//		this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
//		this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "User Name is  Not Matched with User OID.")); 
//		throw new OperationException();	
//	}
//}
//

StringBuffer sqlbuf = new StringBuffer();
String fk_user="";
String FK_NWSITES_MAIN="";
try
{
 sqlbuf.append("SELECT distinct case when nws.FK_NWSITES_MAIN is not null then nws.FK_NWSITES_MAIN else nws.pk_nwsites end as fk_nwsites_main FROM er_nwsites nws,  er_nwusers nwu,  er_user u WHERE nws.pk_nwsites=nwu.FK_NWSITES AND nwu.fk_user=u.pk_user AND (u.pk_user="+userPK+" OR u.USR_LOGNAME='"+userLoginName + "') and fk_account=? "  ); 			
 System.out.println("sqlbuf==="+sqlbuf.toString());
 FK_NWSITES_MAIN = pkMainNetwork(sqlbuf.toString(),iaccId);

}
catch (Exception e) {
	e.printStackTrace();
}
StringBuffer sqlbuf1 = new StringBuffer();
//FK_NWSITES_MAIN = pkMainNetwork(sqlbuf.toString(),iaccId);

 try
 {
	 sqlbuf1.append("SELECT ns.pk_nwsites,s.site_name  AS network_name,ns.NW_MEMBERTYPE, f_codelst_desc(ns.NW_MEMBERTYPE) relationship_desc,f_codelst_subtyp(ns.NW_MEMBERTYPE) AS code, (select codelst_type from er_codelst where pk_codelst=ns.NW_MEMBERTYPE) relationship_type, ns.nw_status, f_codelst_desc(ns.nw_status) status_desc, f_codelst_subtyp(ns.nw_status) status_subtyp,(select codelst_type from er_codelst where pk_codelst=ns.nw_status) status_type FROM er_nwsites ns, er_site s WHERE pk_nwsites IN ( "+ FK_NWSITES_MAIN+" )and s.fk_account="+iaccId+" and s.pk_site=ns.fk_site");

	 System.out.println("sqlbuf1==="+sqlbuf1.toString());
	 System.out.println("network name=="+networkName);
	 conn = CommonDAO.getConnection();
	 pstmt = conn.prepareStatement(sqlbuf1.toString());
		rs = pstmt.executeQuery();
		Code code=null;
		while (rs.next()) {
			code=new Code();
			pkNetList.add(String.valueOf(rs.getInt("pk_nwsites")));
			if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
				pkRelnShipType.add(rs.getString("pk_nwsites"));
			}
			else{
				pkRelnShipType.add("0");
			}
			netNameList.add(rs.getString("network_name"));
			//netInfoList.add(rs.getString("network_info"));
			code.setPk(rs.getString("nw_status"));
			code.setType(rs.getString("status_type"));
			code.setCode(rs.getString("status_subtyp"));
			code.setDescription(rs.getString("status_desc"));
			netStatList.add(code);
			if(rs.getString("relationship_type")!=null){
			code=new Code();
			code.setPk(rs.getString("NW_MEMBERTYPE"));
			code.setType(rs.getString("relationship_type"));
			code.setCode(rs.getString("code"));
			code.setDescription(rs.getString("relationship_desc"));
			siteRelnShipType.add(code);
			}
			else{
				siteRelnShipType.add(null);
			}
			//siteNameList.add(rs.getString("site_name"));
			//netLevelList.add(String.valueOf(rs.getInt("nw_level")));
		//	if(rs.getString("ctep_id")!=null){
		//		ctepList.add(rs.getString("ctep_id"));
		//	}else{
		//		ctepList.add("");
		//	}
			count++;
		}
		
		
	if(count==0)
	{
		this.response=new ResponseHolder();
		this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
		this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "No data found")); 
		throw new OperationException();
	}
	}
 catch (SQLException e) {
		e.printStackTrace();
	}

finally{
	try {
		rs.close();
		if(rs1 != null){
		rs1.close();
	     }
		pstmt.close();
		conn.close();
	} catch (SQLException e) {
		e.printStackTrace();
	}
}
}

public UserNetworkSiteResults getNetworkUser(NetworkSiteUserDetails networkSiteUserDetails, HashMap<String, Object> params) throws OperationException{
	Connection conn=null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	ResultSet rs1=null;
	int userPkCountN=0;
	int userPkCountS=0;
	String pkCode="";
	int count=0;
	String net_pks="";
	ObjectMap map=null;
	UserBean userBean = null;
	String networkName="";
	String siteName="";
	String networkPK = "";
	String sitePK = "";
	String relationShipPK = "";
	UserAgentRObj userAgent = (UserAgentRObj) params.get("userAgent");
	String iaccId = (String)params.get(KEY_ACCOUNT_ID);
	String CTEP_ID="";
	String level="";
	String userPK="";
	String userLoginName="";
	String userLoginPK="";
	String relation_pks="";
	UserNetworkSiteResults networkSiteSearchResults = new UserNetworkSiteResults();
	
	System.out.println("relation_pks==="+relation_pks);
	if(networkSiteUserDetails!=null){
		
    if(networkSiteUserDetails.getNetworkName() != null && !networkSiteUserDetails.getNetworkName().equals("")){
    	networkName = networkSiteUserDetails.getNetworkName();
    }
    if(networkSiteUserDetails.getNetworkPK() != null && !networkSiteUserDetails.getNetworkPK().equals("")){
    	networkPK = networkSiteUserDetails.getNetworkPK();
    	System.out.println("networkPK=="+networkPK);
    }
    if(networkSiteUserDetails.getSitePK() != null && !networkSiteUserDetails.getSitePK().equals("")){
    	sitePK = networkSiteUserDetails.getSitePK();
    }
    if(networkSiteUserDetails.getRelationshipPK() != null && !networkSiteUserDetails.getRelationshipPK().equals("")){
    	relationShipPK = networkSiteUserDetails.getRelationshipPK();
    }
    if(networkSiteUserDetails.getSiteName() != null && !networkSiteUserDetails.getSiteName().equals("")){
    	siteName = networkSiteUserDetails.getSiteName();
    }
    if(networkSiteUserDetails.getLevel()!=null && !networkSiteUserDetails.getLevel().equals("")){
    	level = networkSiteUserDetails.getLevel();
    }
    if(networkSiteUserDetails.getUserPK()!=null){
    	int usr=EJBUtil.stringToNum(networkSiteUserDetails.getUserPK());
    	UserBean userBeanPk = userAgent.getUserDetails(usr);
		if(userBeanPk==null){
			     this.response=new ResponseHolder();
				this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				this.response.addIssue(new Issue(IssueTypes.USER_NOT_FOUND, "UserPK not exists")); 
				throw new OperationException();	
		}else{
			userPK = String.valueOf(userBeanPk.getUserId());
		}
	}
				
		if(networkSiteUserDetails.getUserLoginName()!=null){
			UserDao userDao = new UserDao();
			userDao.getUserValuesByLoginName(networkSiteUserDetails.getUserLoginName());
			if(userDao.getUsrIds().size()==0){
				this.response=new ResponseHolder();
				this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
				this.response.addIssue(new Issue(IssueTypes.USER_NOT_FOUND, "UserLoginName not exists")); 
				throw new OperationException();	
			}else
			{
				if(userPK.equals("")){
					userPK = userDao.getUsrIds().get(0).toString();
				}else if(!userPK.equals(userDao.getUsrIds().get(0).toString())){
					this.response=new ResponseHolder();
					this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
					this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "UserLoginName not matching with UserPK")); 
					throw new OperationException();	
				}
			}
		}
    
	if(networkName.equals("") && networkPK.equals("") && relationShipPK.equals("")){
		this.response=new ResponseHolder();
		this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
		this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Network Identifier or relationshipPK is Required.")); 
		throw new OperationException();
	}
	}
 


 if(networkName.equals("") && networkPK.equals("") && sitePK.equals("") && siteName.equals("") && relationShipPK.equals("") && level.equals("") && userPK.equals("")){
	this.response=new ResponseHolder();
	this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
	this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid data is Required.")); 
	throw new OperationException();
 }     
			
	

StringBuffer sqlbuf = new StringBuffer();
String fk_user="";
String FK_NWSITES_MAIN="";
String str="";
String pk_main_network="";


		if(!networkPK.equals("")){
			pk_main_network=networkPK;
			System.out.println("pk_main_network Inside If==="+pk_main_network);
		}else if(!relationShipPK.equals("")){
			
			String getPrimaryNetwork="select rowtocol('select distinct nvl(fk_nwsites_main,pk_nwsites) FROM er_nwsites WHERE pk_nwsites in ("+relationShipPK+")') as fk_nwsites_main from dual";
			System.out.println("getPrimaryNetwork sql==="+getPrimaryNetwork);
 			try {
 				conn = CommonDAO.getConnection();
 				pstmt = conn.prepareStatement(getPrimaryNetwork);
 				rs=pstmt.executeQuery();
 				while(rs.next()){
 					if(rs.getString("fk_nwsites_main")!=null){
 						pk_main_network=rs.getString("fk_nwsites_main");
 					}
 				}
 				System.out.println("new condition pk_main_network==="+pk_main_network);
 			}catch(Exception e){
 				e.printStackTrace();
 			}
		}
	



net_pks=net_pks.equals("")?net_pks:"";
System.out.println("pk_main_network==="+pk_main_network);
	String str1 = "select pk_nwsites,fk_site,site_name ,nw_status,site_info,site_name as network_name,er_nwsites.nw_level as nw_level,er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden, nw_membertype,"
			+" (SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType, "
			+" (SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,(SELECT CODELST_HIDE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipHide, CTEP_ID "
			+ "from er_nwsites,er_site,er_codelst "+
				  "where pk_site=fk_site and pk_codelst=nw_status and er_site.fk_account=? ";
	if(!(networkName.equals(""))){
			 	 str1=str1+" and lower(site_name) like lower('"+networkName+"') ";
		}
	if(level.equals("0")){
		str1 = str1+" and er_nwsites.nw_level="+level;
		if(!userPK.equals("")){
			
		}
	}
	//if(networkName.equals("") && pk_main_network.length()>0)
	if(pk_main_network.length()>0)
	{
		str1=str1+" and pk_nwsites in("+pk_main_network+") ";
	}
try {
	System.out.println("str1==="+str1.toString());
conn = CommonDAO.getConnection();
pstmt = conn.prepareStatement(str1);
pstmt.setInt(1, StringUtil.stringToNum(iaccId));
rs = pstmt.executeQuery();
Code code=null;
Code code1=null;
while (rs.next()) {
	System.out.println("Inside While");
	code = new Code();
	pkNetList.add(String.valueOf(rs.getInt("pk_nwsites")));
	fkSiteList.add(String.valueOf(rs.getInt("fk_site")));
	netNameList.add(rs.getString("network_name"));
	netInfoList.add(rs.getString("site_info"));
	if(rs.getString("nw_status")!=null){
	code.setType(rs.getString("stat_type"));
	code.setCode(rs.getString("code"));
	code.setDescription(rs.getString("description"));
	code.setPk(String.valueOf(rs.getInt("nw_status")));
	netStatList.add(code);
	}
	ctepList.add(rs.getString("CTEP_ID"));
	if(rs.getString("SiteRelationshipType")!=null){
		code1=new Code();
		code1.setType(rs.getString("SiteRelationshipType"));
		code1.setCode(rs.getString("SiteRelationshipSubType"));
		code1.setDescription(rs.getString("SiteRelationshipDesc"));
		siteRelnShipType.add(code1);
	}
	
	siteNameList.add(rs.getString("site_name"));
	netLevelList.add(String.valueOf(rs.getInt("nw_level")));
	count++;
}
System.out.println("count==="+count);
if(count==0)
{
	this.response=new ResponseHolder();
	this.response.addAction(new CompletedAction("Network Issues", CRUDAction.RETRIEVE));
	this.response.addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Network not found")); 
	throw new OperationException();
}

String chlSql="";
UserSite site=null;
SiteIdentifier siteIdentifier=null;
ObjectMapService objectMapService=null;
UserSite sitelvl=null;
UserSites sites=null;
List<UserSite> sitesLvlList=null;
List<UserSite> siteList=null;
UserNetworks network = null;
UserSites sites1=null;
	
	for(int i=0;i<pkNetList.size();i++){
		site=new UserSite();
		siteList=new ArrayList<UserSite>();
		siteIdentifier=new SiteIdentifier();
		map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(fkSiteList.get(i)));
		siteIdentifier.setOID(map.getOID());
		siteIdentifier.setPK(Integer.valueOf(fkSiteList.get(i)));
		site.setName(siteNameList.get(i).toString());
		site.setSiteIdentifier(siteIdentifier);
		site.setLevel(String.valueOf(netLevelList.get(i)));
		site.setSiteStatus(netStatList.get(i));
		site.setCtepId(ctepList.get(i));
		site.setDescription(netInfoList.get(i));
		if(code1!=null){
			site.setSiteRelationShipType(code1);
		}
		objectMapService = ((ObjectMapService)params.get("objectMapService"));
		params.put("objectMapService", objectMapService);
		System.out.println("userPkCountN==="+userPkCountN);
		String pks_childNtw="";
			
			StringBuffer sqlbuf2 = new StringBuffer();
		sqlbuf2.append("SELECT distinct(pk_nwsites) as pk_nwsites FROM er_nwsites,er_site,er_codelst,er_nwusers nu WHERE pk_site=fk_site AND er_site.fk_account=? AND (er_nwsites.FK_NWSITES_MAIN =? or er_nwsites.PK_NWSITES =?) and pk_codelst=nw_status and nu.fk_nwsites=pk_nwsites ");
		if(!level.equals("0")){
			if(!(siteName.equals("")))
		    {
			sqlbuf2.append(" and lower(site_name) like lower('" +siteName+ "')");
			 }
			if(!sitePK.equals("")){
				sqlbuf2.append(" and er_nwsites.fk_site="+sitePK+"");
			}
		}
		if(!relationShipPK.equals("")){
			sqlbuf2.append(" and pk_nwsites="+relationShipPK+"");
		}
		if(!level.equals("")){
			sqlbuf2.append(" and er_nwsites.nw_level="+level+"");
		}
		if(!userPK.equals("")){
			sqlbuf2.append(" and nu.fk_user="+userPK+"");
		}
		
		
		
		System.out.println("sqlbuf2==="+sqlbuf2.toString());
		System.out.println("pkNetList.get(i)==="+pkNetList.get(i));
		
		pstmt = conn.prepareStatement(sqlbuf2.toString());
		pstmt.setInt(1, StringUtil.stringToNum(iaccId));
		pstmt.setInt(2, StringUtil.stringToNum(pkNetList.get(i)));
		pstmt.setInt(3, StringUtil.stringToNum(pkNetList.get(i)));
		rs = pstmt.executeQuery();
	
		while (rs.next()) {
			pks_childNtw=pks_childNtw+rs.getInt("pk_nwsites")+",";
		}
		pks_childNtw=pks_childNtw.length()>0?pks_childNtw.substring(0,pks_childNtw.length()-1):"0";
		System.out.println("pks_childNtw==="+pks_childNtw);
		if(!pks_childNtw.equals("0")){
		
		StringBuffer sqlbuf3 = new StringBuffer();
		sqlbuf3.append("SELECT distinct pk_nwsites, fk_site,  site_name ,site_info,ctep_id, site_name AS network_name,nw_status, er_codelst.CODELST_DESC as description,er_codelst.codelst_type as stat_type,er_codelst.codelst_subtyp as code,er_codelst.codelst_hide as stat_hidden,er_nwsites.NW_LEVEL AS nw_level,er_nwsites.NW_MEMBERTYPE as relationShip_pk, "
				+" (SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipDesc,(SELECT CODELST_TYPE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipType, "
				+" (SELECT CODELST_SUBTYP FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipSubType,(SELECT CODELST_HIDE FROM er_codelst WHERE PK_CODELST=er_nwsites.NW_MEMBERTYPE) AS SiteRelationshipHide "
				+ "FROM er_nwsites,er_site,er_codelst,er_nwusers nu WHERE pk_site=fk_site AND er_site.fk_account=? AND (er_nwsites.FK_NWSITES_MAIN =? OR er_nwsites.pk_nwsites=?) and pk_codelst=nw_status and nu.fk_nwsites=er_nwsites.pk_nwsites "); 
		if(!userPK.equals("")){
			sqlbuf3.append(" and nu.fk_user="+userPK+"");
		}
		sqlbuf3.append(" and pk_nwsites in("+pks_childNtw+")");
		
		System.out.println("sqlbuf3==="+sqlbuf3.toString());
		System.out.println("pkNetList.get(i)==="+pkNetList.get(i));
		    pstmt = conn.prepareStatement(sqlbuf3.toString());
			pstmt.setInt(1, StringUtil.stringToNum(iaccId));
			pstmt.setInt(2, StringUtil.stringToNum(pkNetList.get(i)));
			pstmt.setInt(3, StringUtil.stringToNum(pkNetList.get(i)));
			rs = pstmt.executeQuery();
			sitesLvlList=new ArrayList<UserSite>();
			while(rs.next()){

				    sites=new UserSites();
					code=new Code();
					siteIdentifier=new SiteIdentifier();
					map = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, Integer.valueOf(rs.getInt("fk_site")));
					siteIdentifier.setOID(map.getOID());
					siteIdentifier.setPK(Integer.valueOf(rs.getInt("fk_site")));
					code.setPk(rs.getString("nw_status"));
					code.setType(rs.getString("stat_type"));
					code.setCode(rs.getString("code"));
					code.setDescription(rs.getString("description"));
					sitelvl=new UserSite();
					sitelvl.setSiteIdentifier(siteIdentifier);
					sitelvl.setLevel(String.valueOf(rs.getInt("nw_level")));
					sitelvl.setName(rs.getString("network_name"));
					if(rs.getString("site_info")!=null){
						sitelvl.setDescription(rs.getString("site_info"));
					}
					if(!userPK.equals("")){
						networkUsersRoles(rs.getString("pk_nwsites"),iaccId,sitelvl,params,userPK);
					}else{
						networkUsersRoles(rs.getString("pk_nwsites"),iaccId,sitelvl,params,"");
					}
					
					sitelvl.setSiteStatus(code);
					sitelvl.setCtepId(rs.getString("ctep_id"));
					if(rs.getString("relationship_pk")!=null){
						code=new Code();
						code.setPk(rs.getString("relationship_pk"));
						code.setType(rs.getString("SiteRelationshipType"));
						code.setCode(rs.getString("SiteRelationshipSubType"));
						code.setDescription(rs.getString("SiteRelationshipDesc"));
						sitelvl.setSiteRelationShipType(code);
					}
					if(rs.getString("pk_nwsites")!=null && !rs.getString("pk_nwsites").equals("0")){
						sitelvl.setRelationshipPK(rs.getString("pk_nwsites"));
					}
					sitesLvlList.add(sitelvl);
					sites.setSites(sitesLvlList);
					site.setSites(sites);
					
				}
			
			
			siteList.add(site);
			sitesLists.add((ArrayList<UserSite>) siteList);
			
			
					network = new UserNetworks();
					sites1 = new UserSites();
					network.setName(netNameList.get(i));
					NetworkIdentifier netIdentifier = new NetworkIdentifier();
					ObjectMap map1 = ((ObjectMapService)params.get("objectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_NETWORK, Integer.valueOf(pkNetList.get(i)));
					netIdentifier.setOID(map1.getOID());
					netIdentifier.setPK(Integer.valueOf(pkNetList.get(i)));
					network.setNetworkIdentifier(netIdentifier);
					if(netStatList.size()>0 && netStatList.get(i)!=null){
					network.setNetworkStatus(netStatList.get(i));
					}
					if(code1!=null){
					network.setNetworkRelationShipType(code1);
					}
					sites1.setSites(sitesLists.get(sitesLists.size()-1));
					network.setSites(sites1);
					
						networkSiteSearchResults.addNetwork(network);
					}
					
					
			
	}
}catch(OperationException ex){
	throw ex;
}
catch (Exception e) {
	e.printStackTrace();
} finally {
	try {
		rs.close();
		if(rs1 != null){
		rs1.close();
	     }
		pstmt.close();
		conn.close();
	} catch (SQLException e) {
		e.printStackTrace();
	}
}
return networkSiteSearchResults;
	
}
}

