package com.velos.services.patientdemographics;

/*
 * Author : Raman
 */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.business.person.impl.PersonBean;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.util.StringUtil;
import com.velos.services.AbstractService;
import com.velos.services.OperationException;

public class UpdatePatientDemographicsDAO extends CommonDAO{
	
	/**
	 * DataAccess object for UpdatePatientDemographics Services 
	 * 
	 */
	
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(UpdatePatientDemographicsDAO.class);
	
	public static int [] UpdateMPatientDemographics(List<PersonBean> perBeanList,Map<String, Object> parameters) throws OperationException, SQLException
	{
		UserBean callingUser = (UserBean)parameters.get("callingUser");
		PreparedStatement pstmt = null;
		Connection conn = null;
		SimpleDateFormat sf=new SimpleDateFormat("MM/dd/yyyy");
		try{
			
			conn = getConnection();
			conn.setAutoCommit(false);
			pstmt = conn.prepareStatement("UPDATE EPAT.PERSON SET" +
					" PERSON_CODE              = ?,"+
					//" PERSON_ALTID             = ?,"+
					" PERSON_LNAME             = ?,"+
					" PERSON_FNAME             = ?,"+
					" PERSON_MNAME             = ?,"+
					//" PERSON_SUFFIX            = ?,"+
					//" PERSON_PREFIX            = ?,"+
					//" PERSON_DEGREE            = ?,"+
					" PERSON_DOB               = to_date(?, 'MM/dd/yyyy'), "+
					" FK_CODELST_GENDER        = ?,"+
					//" PERSON_AKA               = ?,"+
					" FK_CODELST_RACE          = ?,"+
					//" FK_CODELST_PRILANG       = ?,"+
					" FK_CODELST_MARITAL       = ?,"+
					//" FK_ACCOUNT               = ?,"+
					//" PERSON_SSN               = ?,"+
					//" PERSON_DRIV_LIC          = ?,"+
					//" PERSON_ETHGRP            = ?,"+
					//" PERSON_BIRTH_PLACE       = ?,"+
					//" PERSON_MULTI_BIRTH       = ?,"+
					//" FK_CODELST_CITIZEN       = ?,"+
					//" PERSON_MILVET            = ?,"+
					//" FK_CODELST_NATIONALITY   = ?,"+
					" PERSON_DEATHDT           = to_date(?, 'MM/dd/yyyy'), "+
					//" FK_SITE                  = ?,"+
					//" CREATED_ON               = ?,"+
					" FK_CODELST_EMPLOYMENT    = ?,"+
					" FK_CODELST_EDU           = ?,"+
					" FK_CODELST_PSTAT         = ?,"+
					" FK_CODELST_BLOODGRP      = ?,"+
					" PERSON_ADDRESS1          = ?,"+
					" PERSON_ADDRESS2          = ?,"+
					" PERSON_CITY              = ?,"+
					" PERSON_STATE             = ?,"+
					" PERSON_ZIP               = ?,"+
					" PERSON_COUNTRY           = ?,"+
					" PERSON_HPHONE            = ?,"+
					" PERSON_BPHONE            = ?,"+
					" PERSON_EMAIL             = ?,"+
					" PERSON_COUNTY            = ?,"+
					//" PERSON_REGDATE           = ?,"+
					//" PERSON_REGBY             = ?,"+
					//" FK_CODLST_NTYPE          = ?,"+
					//" PERSON_MOTHER_NAME       = ?,"+
					//" FK_PERSON_MOTHER         = ?,"+
					//" FK_CODELST_RELIGION      = ?,"+
					//" RID                      = ?,"+
					//" CREATOR                  = ?,"+
					" LAST_MODIFIED_BY         = ?,"+
					//" LAST_MODIFIED_DATE       = ?,"+
					" IP_ADD                   = ?,"+
					" FK_TIMEZONE              = ?,"+
					//" PERSON_PHYOTHER          = ?,"+
					//" PERSON_ORGOTHER          = ?,"+
					//" PERSON_SPLACCESS         = ?,"+
					" FK_CODELST_ETHNICITY     = ?,"+
					" PERSON_ADD_RACE          = ?,"+
					" PERSON_ADD_ETHNICITY     = ?,"+
					" FK_CODELST_PAT_DTH_CAUSE = ?,"+
					" CAUSE_OF_DEATH_OTHER     = ?"+
					//" PAT_FACILITYID           = ?,"+
					//" PERSON_NOTES_CLOB        = ?,"+
					" WHERE  PK_PERSON         = ?");
			for(PersonBean personBean: perBeanList)
			{
				pstmt.setString(1, personBean.getPersonPId());
				pstmt.setString(2, personBean.getPersonLname());
				pstmt.setString(3, personBean.getPersonFname());
				pstmt.setString(4, personBean.getPersonMname());
				pstmt.setString(5, sf.format(personBean.getPersonDb()));  //
				pstmt.setString(6, personBean.getPersonGender());
				pstmt.setString(7, personBean.getPersonRace());
				pstmt.setString(8, personBean.getPersonMarital());
				pstmt.setString(9, null!=personBean.getPersonDeathDt()? sf.format(personBean.getPersonDeathDt()):"");  //
				pstmt.setString(10, personBean.getPersonEmployment());
				pstmt.setString(11, personBean.getPersonEducation());
				pstmt.setString(12, personBean.getPersonStatus());
				pstmt.setString(13, personBean.getPersonBloodGr());
				pstmt.setString(14, personBean.getPersonAddress1());
				pstmt.setString(15, personBean.getPersonAddress2());
				pstmt.setString(16, personBean.getPersonCity());
				pstmt.setString(17, personBean.getPersonState());
				pstmt.setString(18, personBean.getPersonZip());
				pstmt.setString(19, personBean.getPersonCountry());
				pstmt.setString(20, personBean.getPersonHphone());
				pstmt.setString(21, personBean.getPersonBphone());
				pstmt.setString(22, personBean.getPersonEmail());
				pstmt.setString(23, personBean.getPersonCounty());
				pstmt.setString(24, StringUtil.integerToString(callingUser.getUserId()));
				pstmt.setString(25, AbstractService.IP_ADDRESS_FIELD_VALUE);
				pstmt.setString(26, personBean.getTimeZoneId());
				pstmt.setString(27, personBean.getPersonEthnicity());
				pstmt.setString(28, personBean.getPersonAddRace());
				pstmt.setString(29, personBean.getPersonAddEthnicity());
				pstmt.setString(30, personBean.getPatDthCause());
				pstmt.setString(31, personBean.getDthCauseOther());
				
				pstmt.setInt(32, personBean.getPersonPKId());
				
				pstmt.addBatch();
			}
			int [] numUpdates=pstmt.executeBatch();
			conn.commit();
			return numUpdates;
			
		}catch(SQLException se){
	
			se.printStackTrace();
			//conn.rollback();
			throw new OperationException(se.getMessage());
			
		}
		finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}

		}
	}
	
	
	
}