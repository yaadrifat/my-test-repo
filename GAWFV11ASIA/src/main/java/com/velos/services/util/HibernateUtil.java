package com.velos.services.util;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.velos.services.util.HibernateUtilHelper;

public class HibernateUtil {

	
	private static Logger logger = Logger.getLogger(HibernateUtil.class);
	public static final SessionFactory vdaSessionFactory;
	public static final SessionFactory eschSessionFactory;
	public static final SessionFactory eresSessionFactory;
	private static File vdaLocation = new File(HibernateUtilHelper.getVDAConfigFilePath());
	private static File eresLocation = new File(HibernateUtilHelper.getERESConfigFilePath());
	private static File eschLocation = new File(HibernateUtilHelper.getESCHConfigFilePath());
	private static File propertyFileLocation = new File(HibernateUtilHelper.getPropertyFilePath());
	static{
		
		try {
		
		 Configuration configuration = new Configuration();
		 Properties properties = new Properties();
		 properties.load(new FileInputStream(propertyFileLocation));
		 String url=properties.getProperty("jdbc-vda.proxool.driver-url");
		 String userName=properties.getProperty("jdbc-vda.user");
		 String password=properties.getProperty("jdbc-vda.password");
		 properties.setProperty("hibernate.connection.url", url);
		 properties.setProperty("hibernate.connection.username", userName);
		 properties.setProperty("hibernate.connection.password", password);
		 configuration.setProperties(properties);
		 vdaSessionFactory =  configuration.mergeProperties(properties).configure(vdaLocation).buildSessionFactory();
		 //vdaSessionFactory = new AnnotationConfiguration().configure(vdaLocation).buildSessionFactory();
		 configuration = new Configuration();
		 properties = new Properties();
		 properties.load(new FileInputStream(propertyFileLocation));
		 url=properties.getProperty("jdbc-eres.proxool.driver-url");
		 userName=properties.getProperty("jdbc-eres.user");
		 password=properties.getProperty("jdbc-eres.password");
		 properties.setProperty("hibernate.connection.url", url);
		 properties.setProperty("hibernate.connection.username", userName);
		 properties.setProperty("hibernate.connection.password", password);
		 configuration.setProperties(properties);
		 eresSessionFactory =  configuration.mergeProperties(properties).configure(eresLocation).buildSessionFactory();
		 //eresSessionFactory = new AnnotationConfiguration().configure(eresLocation).buildSessionFactory();
		 configuration = new Configuration();
		 properties = new Properties();
		 properties.load(new FileInputStream(propertyFileLocation));
		 url=properties.getProperty("jdbc-esch.proxool.driver-url");
		 userName=properties.getProperty("jdbc-esch.user");
		 password=properties.getProperty("jdbc-esch.password");
		 properties.setProperty("hibernate.connection.url", url);
		 properties.setProperty("hibernate.connection.username", userName);
		 properties.setProperty("hibernate.connection.password", password);
		 configuration.setProperties(properties);
		 eschSessionFactory =  configuration.mergeProperties(properties).configure(eschLocation).buildSessionFactory();
		 //eschSessionFactory = new AnnotationConfiguration().configure(eschLocation).buildSessionFactory();
		} catch (Throwable e) {
			logger.error("SessionFactory creation failed ======= " +e);
			System.err.println("SessionFactory creation failed" + e);
            throw new ExceptionInInitializerError(e);
		}
		
	}
	
	public static void closeSession(Session s) throws HibernateException {
			if (s != null)
				s.close();
    }
	
}
