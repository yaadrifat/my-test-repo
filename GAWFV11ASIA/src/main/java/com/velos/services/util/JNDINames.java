package com.velos.services.util;

public class JNDINames {
	public final static String prefix = "velos/";
	public final static String suffix = "/remote";
	
	public final static String MessageServiceImpl = "ejb:velos/velos-ejb-eres/MessageServiceImpl!com.velos.services.monitoring.MessageService";
	public final static String ObjectMapServiceImpl = "ejb:velos/velos-ejb-eres/ObjectMapServiceImpl!com.velos.services.map.ObjectMapService";
	public final static String SessionServiceImpl = "ejb:velos/velos-ejb-eres/SessionServiceImpl!com.velos.services.monitoring.SessionService";
	public final static String StudyServiceImpl = "ejb:velos/velos-ejb-eres/StudyServiceImpl!com.velos.services.study.StudyService";
	public final static String StudyCalendarServiceImpl = "ejb:velos/velos-ejb-esch/StudyCalendarServiceImpl!com.velos.services.calendar.StudyCalendarService"; 
	public final static String PatientDemographicsServiceImpl = "ejb:velos/velos-ejb-eres/PatientDemographicsServiceImpl!com.velos.services.patientdemographics.PatientDemographicsService";
	public final static String PatientStudyServiceImpl = "ejb:velos/velos-ejb-eres/PatientStudyServiceImpl!com.velos.services.patientstudy.PatientStudyService"; 
	public final static String StudyPatientServiceImpl = "ejb:velos/velos-ejb-eres/StudyPatientServiceImpl!com.velos.services.studypatient.StudyPatientService"; 
	public final static String PatientScheduleServiceImpl= "ejb:velos/velos-ejb-esch/PatientScheduleServiceImpl!com.velos.services.patientschedule.PatientScheduleService"; 
	public final static String FormDesignServiceImpl= "ejb:velos/velos-ejb-eres/FormDesignServiceImpl!com.velos.services.form.FormDesignService";
	public final static String FormResponseServiceImpl = "ejb:velos/velos-ejb-eres/FormResponseServiceImpl!com.velos.services.formresponse.FormResponseService"; 
	public final static String BudgetServiceImpl = "ejb:velos/velos-ejb-eres/BudgetServiceImpl!com.velos.services.budget.BudgetService";
	public final static String UserServiceImpl = "ejb:velos/velos-ejb-eres/UserServiceImpl!com.velos.services.user.UserService";
	public final static String SearchPatientServiceImpl = "ejb:velos/velos-ejb-eres/SearchPatientServiceImpl!com.velos.services.searchpatient.SearchPatientService"; 
	public final static String UpdatePatientDemographicsServiceImpl = "ejb:velos/velos-ejb-eres/UpdatePatientDemographicsServiceImpl!com.velos.services.patientdemographics.UpdatePatientDemographicsService"; 
	public final static String LibraryServiceImpl = "ejb:velos/velos-ejb-eres/LibraryServiceImpl!com.velos.services.library.LibraryService";
	public final static String PatientFacilityServiceImpl = "ejb:velos/velos-ejb-eres/PatientFacilityServiceImpl!com.velos.services.patientdemographics.PatientFacilityService";
	public final static String CodeListServiceImpl = "ejb:velos/velos-ejb-eres/CodeListServiceImpl!com.velos.services.systemadmin.CodeListService"; 
	public final static String PatientScheduleServiceBMTImpl = "ejb:velos/velos-ejb-esch/PatientScheduleServiceBMTImpl!com.velos.services.patientschedule.PatientScheduleServiceBMT";
	public final static String StudyMashupServiceImpl = "ejb:velos/velos-ejb-eres/StudyMashupServiceImpl!com.velos.services.mashup.StudyMashupService";
	public final static String MilestoneServiceImpl = "ejb:velos/velos-ejb-eres/MilestoneServiceImpl!com.velos.services.milestone.MilestoneService";
	public final static String VersionServiceImpl = "ejb:velos/velos-ejb-eres/VersionServiceImpl!com.velos.services.version.VersionService";
	public final static String WorkflowServiceImpl = "ejb:velos/velos-ejb-eres/WorkflowServiceImpl!com.velos.services.workflow.WorkflowService";
	public final static String SubmissionLogicHome = "ejb:velos/velos-ejb-eres/SubmissionLogicServiceImpl!com.velos.services.checksubmission.SubmissionLogicService";
	public final static String ProtocolSubmissionHome = "ejb:velos/velos-ejb-eres/ProtocolSubmissionServiceImpl!com.velos.services.protocolsubmission.ProtocolSubmissionService";
	public final static String OutboundMessageServiceImpl = "ejb:velos/velos-ejb-eres/OutboundMessageServiceImpl!com.velos.services.OutboundMessage.OutboundMessageService";
}
