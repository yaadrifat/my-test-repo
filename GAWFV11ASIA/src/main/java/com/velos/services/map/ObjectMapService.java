/**
 * 
 */
package com.velos.services.map;


import javax.ejb.Remote;

import com.velos.services.model.ObjectInfo;

/**
 * @author dylan
 *
 */ 


@Remote
public interface ObjectMapService {

	public static final String PERSISTENCE_UNIT_STUDY = "er_study";
	public static final String PERSISTENCE_UNIT_ACCOUNT = "er_account";
	public static final String PERSISTENCE_UNIT_USER = "er_user";
	public static final String PERSISTENCE_UNIT_GROUPS = "er_grps";
	public static final String PERSISTENCE_UNIT_ORGANIZATION ="er_site";
	public static final String PERSISTENCE_UNIT_NETWORK ="er_nwsites";
	public static final String PERSISTENCE_UNIT_STUDY_STATUS = "er_studystat";
	public static final String PERSISTENCE_UNIT_PERSON = "person"; 
	public static final String PERSISTENCE_UNIT_EVENT_STATUS = "sch_codelst";
	public static final String PERSISTENCE_UNIT_EVENT = "sch_event1";
	public static final String PERSISTENCE_UNIT_PATPROT = "er_patprot";
	public static final String PERSISTENCE_UNIT_VISIT= "sch_protocol_visit";
	public static final String PERSISTENCE_UNIT_STUDY_CALENDAR="event_assoc";
	public static final String PERSISTENCE_UNIT_EVENT_COST="sch_eventcost";
	public static final String PERSISTENCE_UNIT_CALENDAR_BUDGET="sch_budget";
	public static final String PERSISTENCE_UNIT_PATSTUDYSTAT="er_patstudystat"; 
	public static final String PERSISTENCE_UNIT_CALENDAR_STATUS = "sch_protstat"; 
	public static final String PERSISTENCE_UNIT_STUDY_TEAM = "er_studyteam";
	public static final String PERSISTENCE_UNIT_MORESTUDY_DETAILS = "er_studyid";
	public static final String PERSISTENCE_UNIT_BUDGET = "sch_budget";
	public static final String PERSISTENCE_UNIT_LIBRARY_EVENTS = "event_def";
	
	public static final String PERSISTENCE_UNIT_FIELD_LIBRARY = "er_fldlib";
	public static final String PERSISTENCE_UNIT_FORM_SECTION = "er_formsec";
	public static final String PERSISTENCE_UNIT_FORM_LIBRARY = "er_formlib"; 
	public static final String PERSISTENCE_UNIT_FORM_FIELD = "er_formfld"; 
	
	public static final String PERSISTENCE_UNIT_STUDY_FORM_RESPONSE = "er_studyforms"; 
	public static final String PERSISTENCE_UNIT_PATIENT_FORM_RESPONSE = "er_patforms";
	public static final String PERSISTENCE_UNIT_STUDY_ORGANIZATION_ID = "er_studysites"; //Bug Fix : 7978 : Tarandeep Singh Bali
	public static final String PERSISTENCE_UNIT_PATIENT_SCHEDULE = "erv_patsch";
	public static final String PERSISTENCE_UNIT_EVENT_STATUS_TABLE = "sch_eventstat";
	public static final String PERSISTENCE_UNIT_PATIENT_FACILITY ="er_patfacility";
	public static final String PERSISTENCE_UNIT_ACCOUNT_FORM_RESPONSE = "er_acctforms"; 
	public static final String PERSISTENCE_UNIT_MILESTONE = "er_milestone";
	public static final String PERSISTENCE_UNIT_BUDGET_CALENDAR="sch_bgtcal";
	public static final String PERSISTENCE_UNIT_BUDGET_SECTION="sch_bgtsection";
	public static final String PERSISTENCE_UNIT_BUDGET_LINEITEM="sch_lineitem";
	public static final String PERSISTENCE_UNIT_STUDY_VERSION="er_studyver";
	public static final String PERSISTENCE_UNIT_STUDY_APNDX="er_studyapndx";
	public static final String PERSISTENCE_UNIT_STUDYVER_HISTORY="er_status_history";
	
	public ObjectMap getObjectMapFromId(String id) 
		throws MultipleObjectsFoundException;
	
//	public ObjectMap createObjectMap(String tableName, Integer tablePK); 
	
	public ObjectMap getOrCreateObjectMapFromPK(String table_name, Integer tablePK);
	//virendra
	public Integer getObjectPkFromOID(String OID);
	
	public ObjectInfo getObjectInfoFromOID(String OID);
}
