package com.velos.services.OutboundMessage;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.xml.bind.JAXBElement;

import org.apache.log4j.Logger;

import com.velos.eres.service.grpRightsAgent.GrpRightsAgentRObj;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.util.StringUtil;
import com.velos.esch.web.budget.BudgetJB;
import com.velos.esch.web.eventassoc.EventAssocJB;
import com.velos.services.AbstractService;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.Change;
import com.velos.services.model.Changes;
import com.velos.services.model.ChangesList;
import com.velos.services.model.OutboundOrganizationIdentifier;
import com.velos.services.model.OutboundPatientIdentifier;
import com.velos.services.model.OutboundPatientProtocolIdentifier;
import com.velos.services.model.OutboundStudyIdentifier;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.UserIdentifier;
import com.velos.services.outbound.MessageQueueBean;
import com.velos.services.outbound.MessagingIdentifierHelperDAO;
import com.velos.services.util.ServicesUtil;

@Stateless
@Remote(OutboundMessageService.class)
public class OutboundMessageServiceImpl extends AbstractService implements OutboundMessageService{
	
	private static Logger logger = Logger.getLogger(OutboundMessageServiceImpl.class.getName());
	@EJB
	private ObjectMapService objectMapService;
	@EJB
	private UserAgentRObj userAgent;
	@EJB
	GrpRightsAgentRObj groupRightsAgent;
	@Resource 
	private SessionContext sessionContext;

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ChangesList getOutboundMessages(Date fromDate,Date toDate,String messageModule,String calledFrom) throws OperationException{
		OutboundMessageDAO dao = new OutboundMessageDAO();
		ChangesList changesList = new ChangesList();
	try
	{
		Changes changes = null;
		Change change = null;
		ObjectMap objectMap = null;
		String messagePk= new String();
		if(!StringUtil.isEmpty(messageModule) && !Arrays.asList(OutboundMessageDAO.moduleName).contains(messageModule))
		{
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid messageModule is required to getOutboundMessages"));
			throw new OperationException();
		}
		List<MessageQueueBean> messageQueueList = dao.getDataForProcessing(fromDate,toDate,StringUtil.stringToNum(callingUser.getUserAccountId()),messageModule,calledFrom);
		List<JAXBElement> additionalInformation =null;
		for (MessageQueueBean bean : messageQueueList) {
			changes = new Changes();
			change = new Change();
			// Composite key 
			String[] currentRootFKArray = bean.getRootFK().split(","); 
			String[] currentRootTableNameArray = bean.getRootTableName().split(","); 
			/*List<SimpleIdentifier> parentIDList = new ArrayList<SimpleIdentifier>();*/ 
			// get OID for parent
			OutboundPatientProtocolIdentifier patProtId = new OutboundPatientProtocolIdentifier();
			for(int i = 0; i < currentRootFKArray.length ; i++)
			{
				//SimpleIdentifier parentID = new SimpleIdentifier();
				patProtId = getIdentifierObject(Integer.parseInt(currentRootFKArray[i]),
						currentRootTableNameArray[i]);
				//parentIDList.add(parentID); 
			}
			if("".equals(messagePk))
				messagePk = bean.getMessagePK().toString();
			else
				messagePk = messagePk+","+bean.getMessagePK().toString();
			changes.setParentIdentifier(patProtId);
			
			change.setAction(bean.getAction());
			change.setModule(bean.getModule());
			change.setTimeStamp(bean.getCreatedOn()); 
			additionalInformation = new ArrayList<JAXBElement>();
			additionalInformation = dao.getAddtionalInformation(bean.getMessagePK());
			if(bean.getModule().equalsIgnoreCase("study_status")){
				for(int i=0;i<additionalInformation.size();i++){
					if(additionalInformation.get(i).getName().getLocalPart().equals("statusSubType") && additionalInformation.get(i).getValue() != null)
						change.setStatusSubType(additionalInformation.get(i).getValue().toString());
					if(additionalInformation.get(i).getName().getLocalPart().equals("statusCodeDesc") && additionalInformation.get(i).getValue() != null)
						change.setStatusCodeDesc(additionalInformation.get(i).getValue().toString());
				}
				StudyIdentifier studyStatusIdent = new StudyIdentifier();
				objectMap = objectMapService.getOrCreateObjectMapFromPK(
						bean.getTableName(), bean.getTablePK());
				studyStatusIdent.setOID(objectMap.getOID());
				change.setIdentifier(studyStatusIdent);
			}else if(bean.getModule().equalsIgnoreCase("patstudy_status")){
				for(int i=0;i<additionalInformation.size();i++){
					if(additionalInformation.get(i).getName().getLocalPart().equals("statusSubType") && additionalInformation.get(i).getValue() != null)
						change.setStatusSubType(additionalInformation.get(i).getValue().toString());
					if(additionalInformation.get(i).getName().getLocalPart().equals("statusCodeDesc") && additionalInformation.get(i).getValue() != null)
						change.setStatusCodeDesc(additionalInformation.get(i).getValue().toString());
				}
				StudyIdentifier studyStatusIdent = new StudyIdentifier();
				objectMap = objectMapService.getOrCreateObjectMapFromPK(
						bean.getTableName(), bean.getTablePK());
				studyStatusIdent.setOID(objectMap.getOID());
				change.setIdentifier(studyStatusIdent);
				
			}else if(bean.getModule().equalsIgnoreCase("form_status")){
				for(int i=0;i<additionalInformation.size();i++){
					if(additionalInformation.get(i).getName().getLocalPart().equals("statusSubType") && additionalInformation.get(i).getValue() != null)
						change.setStatusSubType(additionalInformation.get(i).getValue().toString());
					if(additionalInformation.get(i).getName().getLocalPart().equals("statusCodeDesc") && additionalInformation.get(i).getValue() != null)
						change.setStatusCodeDesc(additionalInformation.get(i).getValue().toString());
					if(additionalInformation.get(i).getName().getLocalPart().equals("formType") && additionalInformation.get(i).getValue() != null)
						change.setFormType(additionalInformation.get(i).getValue().toString());
				}
				StudyIdentifier studyStatusIdent = new StudyIdentifier();
				objectMap = objectMapService.getOrCreateObjectMapFromPK(
						bean.getTableName(), bean.getTablePK());
				studyStatusIdent.setOID(objectMap.getOID());
				change.setIdentifier(studyStatusIdent);
				
				if(ObjectMapService.PERSISTENCE_UNIT_PATIENT_FORM_RESPONSE.equalsIgnoreCase(bean.getTableName())){
					MessagingIdentifierHelperDAO helper = new MessagingIdentifierHelperDAO();
					String formResType = helper.getFormResType(bean.getTablePK());
					if("PS".equals(formResType))
						change.setFormResponseType("Patient Schedule");
					else if("SP".equals(formResType))
						change.setFormResponseType("Study Patient");
					else
						change.setFormResponseType("Patient");
				}else if(ObjectMapService.PERSISTENCE_UNIT_STUDY_FORM_RESPONSE.equalsIgnoreCase(bean.getTableName())){
					change.setFormResponseType("Study");
				}else if(ObjectMapService.PERSISTENCE_UNIT_ACCOUNT_FORM_RESPONSE.equalsIgnoreCase(bean.getTableName())){
					change.setFormResponseType("Account");
				}
			}else if(bean.getModule().equalsIgnoreCase("calendar_status")){
				for(int i=0;i<additionalInformation.size();i++){
					if(additionalInformation.get(i).getName().getLocalPart().equals("statusSubType") && additionalInformation.get(i).getValue() != null)
						change.setStatusSubType(additionalInformation.get(i).getValue().toString());
					if(additionalInformation.get(i).getName().getLocalPart().equals("statusCodeDesc") && additionalInformation.get(i).getValue() != null)
						change.setStatusCodeDesc(additionalInformation.get(i).getValue().toString());
				}
				StudyIdentifier studyStatusIdent = new StudyIdentifier();
				objectMap = objectMapService.getOrCreateObjectMapFromPK(
						bean.getTableName(), bean.getTablePK());
				studyStatusIdent.setOID(objectMap.getOID());
				change.setIdentifier(studyStatusIdent);
				
			}else if(bean.getModule().equalsIgnoreCase("budget_status")){
				for(int i=0;i<additionalInformation.size();i++){
					if(additionalInformation.get(i).getName().getLocalPart().equals("statusSubType") && additionalInformation.get(i).getValue() != null)
						change.setStatusSubType(additionalInformation.get(i).getValue().toString());
					if(additionalInformation.get(i).getName().getLocalPart().equals("statusCodeDesc") && additionalInformation.get(i).getValue() != null)
						change.setStatusCodeDesc(additionalInformation.get(i).getValue().toString());
				}
				StudyIdentifier studyStatusIdent = new StudyIdentifier();
				objectMap = objectMapService.getOrCreateObjectMapFromPK(
						bean.getTableName(), bean.getTablePK());
				studyStatusIdent.setOID(objectMap.getOID());
				change.setIdentifier(studyStatusIdent);
			}else if(bean.getModule().equalsIgnoreCase("study_team")){
				for(int i=0;i<additionalInformation.size();i++){
					if(additionalInformation.get(i).getName().getLocalPart().equals("teamRoleType") && additionalInformation.get(i).getValue() != null)
						change.setTeamRoleType(additionalInformation.get(i).getValue().toString());
					if(additionalInformation.get(i).getName().getLocalPart().equals("teamRoleDesc") && additionalInformation.get(i).getValue() != null)
						change.setTeamRoleDesc(additionalInformation.get(i).getValue().toString());
					if(additionalInformation.get(i).getName().getLocalPart().equals("UserIdentifier") && additionalInformation.get(i).getValue() != null){
						objectMap = objectMapService.getOrCreateObjectMapFromPK(
								ObjectMapService.PERSISTENCE_UNIT_USER, Integer.parseInt(additionalInformation.get(i).getValue().toString()));
						UserIdentifier userIdentifier = new UserIdentifier(); 
						userIdentifier.setOID(objectMap.getOID()); 
						MessagingIdentifierHelperDAO helper = new MessagingIdentifierHelperDAO();
						Map<String, String> userMap = helper.getUserInformation(Integer.parseInt(additionalInformation.get(i).getValue().toString())); 
						
						userIdentifier.setFirstName(userMap.get("firstname")); 
						userIdentifier.setLastName(userMap.get("lastname")); 
						userIdentifier.setUserLoginName(userMap.get("logname"));
						change.setUserIdentifier(userIdentifier);
					}
				}
				StudyIdentifier studyStatusIdent = new StudyIdentifier();
				objectMap = objectMapService.getOrCreateObjectMapFromPK(
						bean.getTableName(), bean.getTablePK());
				studyStatusIdent.setOID(objectMap.getOID());
				change.setIdentifier(studyStatusIdent);
				
			}else if(bean.getModule().equalsIgnoreCase("study_summary")){
				StudyIdentifier studyIdentifier = new StudyIdentifier();
				objectMap = objectMapService.getOrCreateObjectMapFromPK(
						bean.getTableName(), bean.getTablePK());
				studyIdentifier.setOID(objectMap.getOID());
				MessagingIdentifierHelperDAO helper = new MessagingIdentifierHelperDAO();			
				studyIdentifier.setStudyNumber(helper.getStudyNumber(bean.getTablePK()));
				change.setIdentifier(studyIdentifier);
				
			}
			//childIdentifier = MessageProcessor.getIdentifierObject(bean.getTablePK(), bean.getTableName());
			//change.setIdentifier(childIdentifier);
			//change.setAdditionalInformation(dao.getAddtionalInformation(bean.getMessagePK())); 
			changes.getChange().add(change); 
			changesList.addChanges(changes);
		}
		
		dao.insertIntoMSGQueueTrackTable(calledFrom,messagePk);
		
		
	}
	catch(OperationException e){
		e.printStackTrace();
		if (logger.isDebugEnabled()) logger.debug("OutboundMessageServiceImpl retrieved ", e);
		e.setIssues(response.getIssues());
		throw e;
	}
	catch(Throwable t){
		this.addUnknownThrowableIssue(t);
		if (logger.isDebugEnabled()) logger.debug("OutboundMessageServiceImpl retrieved", t);
		throw new OperationException(t);
	}
	return changesList;
	}
	
	public static OutboundPatientProtocolIdentifier getIdentifierObject(int tablePK,
			String tableName) {
			ObjectMapService objectMapService = ServicesUtil.getObjectMapService();
			ObjectMap objectMap = objectMapService.getOrCreateObjectMapFromPK(
				tableName, tablePK);
			
			// Get Identifier for Study
		if (tableName.equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_STUDY)) {

			OutboundPatientProtocolIdentifier studyID = new OutboundPatientProtocolIdentifier();
			studyID.getId().setOID(objectMap.getOID());
			// TODO get Study Number for ID
			MessagingIdentifierHelperDAO helper = new MessagingIdentifierHelperDAO();			
			studyID.getId().setStudyNumber(helper.getStudyNumber(tablePK));
			return studyID;
		}
		/*if(tableName.equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_USER))
		{
			UserIdentifier userIdentifier = new UserIdentifier(); 
			userIdentifier.setOID(objectMap.getOID()); 
			MessagingIdentifierHelperDAO helper = new MessagingIdentifierHelperDAO();
			Map<String, String> userMap = helper.getUserInformation(tablePK); 
			
			userIdentifier.setFirstName(userMap.get("firstname")); 
			userIdentifier.setLastName(userMap.get("lastname")); 
			userIdentifier.setUserLoginName(userMap.get("logname")); 
			
			return userIdentifier; 
			
		}*/
		
		// Get Identifier for study status
		if (tableName.equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_STUDY_STATUS)) {
			OutboundPatientProtocolIdentifier statusIdentifier = new OutboundPatientProtocolIdentifier();
			statusIdentifier.getId().setOID(objectMap.getOID());
			return statusIdentifier;
		}
		
		//Get Identifier for Study Calendar
		if(tableName.equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_STUDY_CALENDAR))
		{
			OutboundPatientProtocolIdentifier calendarIdentifier = new OutboundPatientProtocolIdentifier(); 
			calendarIdentifier.getId().setOID(objectMap.getOID()); 
			EventAssocJB eventAssocJB  = new EventAssocJB();
			eventAssocJB.setEvent_id(tablePK);
			eventAssocJB.getEventAssocDetails();
			if(!StringUtil.isEmpty(eventAssocJB.getName())){
			calendarIdentifier.getId().setCalendarName(eventAssocJB.getName());
			MessagingIdentifierHelperDAO helper = new MessagingIdentifierHelperDAO();
			
			OutboundStudyIdentifier studyIdent = new OutboundStudyIdentifier();
			studyIdent.setStudyNumber(helper.getStudyNumber(StringUtil.stringToInteger(eventAssocJB.getChain_id())));
			calendarIdentifier.getId().setStudyIdentifier(studyIdent);
			}
			return calendarIdentifier; 
		}
		//Get Identifier for Calendar Status
		if(tableName.equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_CALENDAR_STATUS))
		{
			OutboundPatientProtocolIdentifier calendarStatusIdentifier = new OutboundPatientProtocolIdentifier(); 
			calendarStatusIdentifier.getId().setOID(objectMap.getOID()); 
			return calendarStatusIdentifier; 
		}
		
		if(tableName.equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_STUDY_TEAM))
		{
			OutboundPatientProtocolIdentifier studyTeamIdentifier = new OutboundPatientProtocolIdentifier(); 
			studyTeamIdentifier.getId().setOID(objectMap.getOID()); 
			return studyTeamIdentifier; 
			
		}
		
		if(tableName.equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_MORESTUDY_DETAILS))
		{
			OutboundPatientProtocolIdentifier moreStudyDetailsIdentifier = new OutboundPatientProtocolIdentifier(); 
			moreStudyDetailsIdentifier.getId().setOID(objectMap.getOID()); 
			return moreStudyDetailsIdentifier; 
		}
		
		if(tableName.equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_BUDGET))
		{
			OutboundPatientProtocolIdentifier budgetIdentifier = new OutboundPatientProtocolIdentifier(); 
			budgetIdentifier.getId().setOID(objectMap.getOID());
			BudgetJB budgetJB = new BudgetJB();
			budgetJB.setBudgetId(tablePK);
			budgetJB.getBudgetDetails();
			if(!StringUtil.isEmpty(budgetJB.getBudgetName()))
			budgetIdentifier.getId().setBudgetName(budgetJB.getBudgetName());
			return budgetIdentifier; 
		}
		
		if(tableName.equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_PATIENT_FORM_RESPONSE))
		{
			OutboundPatientProtocolIdentifier patientFormResponseIdentifier = new OutboundPatientProtocolIdentifier(); 
			patientFormResponseIdentifier.getId().setOID(objectMap.getOID());
			MessagingIdentifierHelperDAO helper = new MessagingIdentifierHelperDAO();			
			patientFormResponseIdentifier.getId().setFormName(helper.getFormName(tablePK));
			return patientFormResponseIdentifier;
			
		}
		
		if(tableName.equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_STUDY_FORM_RESPONSE))
		{
			OutboundPatientProtocolIdentifier studyFormResponseIdentifier=new OutboundPatientProtocolIdentifier();
			studyFormResponseIdentifier.getId().setOID(objectMap.getOID());
			MessagingIdentifierHelperDAO helper = new MessagingIdentifierHelperDAO();			
			studyFormResponseIdentifier.getId().setFormName(helper.getFormName(tablePK));
			return studyFormResponseIdentifier;
			
		}
		
		if(tableName.equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_ACCOUNT_FORM_RESPONSE))
		{
			OutboundPatientProtocolIdentifier accountFormResponseIdentifier=new OutboundPatientProtocolIdentifier();
			accountFormResponseIdentifier.getId().setOID(objectMap.getOID());
			MessagingIdentifierHelperDAO helper = new MessagingIdentifierHelperDAO();			
			accountFormResponseIdentifier.getId().setFormName(helper.getFormName(tablePK));
			return accountFormResponseIdentifier;
			
		}
	
		if(tableName.equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_FORM_LIBRARY))
		{
			OutboundPatientProtocolIdentifier formIdentifier=new OutboundPatientProtocolIdentifier();
			formIdentifier.getId().setOID(objectMap.getOID()); 
			MessagingIdentifierHelperDAO helper = new MessagingIdentifierHelperDAO();			
			formIdentifier.getId().setFormName(helper.getFormName(tablePK));
			return formIdentifier;
			
		}
		
		if (tableName.equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_PATPROT))
	    {

			OutboundStudyIdentifier studyIdentifier = new OutboundStudyIdentifier();
	      

	      MessagingIdentifierHelperDAO helper = new MessagingIdentifierHelperDAO();
	      studyIdentifier.setStudyNumber(helper.getPatientAssocStudyNumber(tablePK));
	      
	      OutboundOrganizationIdentifier oid = new OutboundOrganizationIdentifier();
	      oid.setSiteName(helper.getSiteName(tablePK));
	      
	      OutboundPatientIdentifier patientId = new OutboundPatientIdentifier();
	      patientId.setPatientId(helper.getPatientId(tablePK));
	      patientId.setOrganizationId(oid);
	      
	      OutboundPatientProtocolIdentifier patientProtocolIdentifier = new OutboundPatientProtocolIdentifier();
	      
	      patientProtocolIdentifier.getId().setOID(objectMap.getOID());
			// TODO get Study Number for ID
	      patientProtocolIdentifier.getId().setStudyIdentifier(studyIdentifier);
	      patientProtocolIdentifier.getId().setPatientIdentifier(patientId);
			return patientProtocolIdentifier;
	    }
		
		if (tableName.equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_PATSTUDYSTAT))
	    {
		OutboundPatientProtocolIdentifier patientStudyStatusIdentifier = new OutboundPatientProtocolIdentifier();
	      patientStudyStatusIdentifier.getId().setOID(objectMap.getOID());
	      
	      return patientStudyStatusIdentifier;
	    }
		
		OutboundPatientProtocolIdentifier simpleIdentifier = new OutboundPatientProtocolIdentifier(); 
		simpleIdentifier.getId().setOID(objectMap.getOID()); 
		return simpleIdentifier;
		
	}
	
	@AroundInvoke
	public Object myInterceptor(InvocationContext ctx) throws Exception {
		response = new ResponseHolder();
		callingUser = 
			getLoggedInUser(
					sessionContext,
					userAgent);
		return ctx.proceed();

	}
}
