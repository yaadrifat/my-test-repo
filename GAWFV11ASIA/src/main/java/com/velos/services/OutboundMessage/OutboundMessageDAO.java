package com.velos.services.OutboundMessage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.services.outbound.MessageQueueBean;
import com.velos.services.outbound.MessagingConstants;

public class OutboundMessageDAO extends CommonDAO{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3234250653949699217L;
	
	public static final String[] moduleName = {"study_status","calendar_status","budget_status","study_team","study_summary","form_status","patstudy_status"};

	private static Logger logger = Logger.getLogger(OutboundMessageDAO.class);
	
	private static String getDataForProcessingSQL ="select pk_message, tablename, table_pk, module, action, root_pk, fk_account, processed_flag, root_tablename, to_char(created_on, PKG_DATEUTIL.f_get_dateformat||' '||PKG_DATEUTIL.f_get_timeformat) create_on_string from ER_MSG_QUEUESVC where (processed_flag = ? or processed_flag = ?) and fk_account = ? AND created_on>(select NVL(MAX(TIMESTAMP),TO_DATE(PKG_DATEUTIL.f_get_null_date_str,PKG_DATEUTIL.F_GET_DATEFORMAT)) from ER_MSGQUEUETRACK where MESSAGE_TYPE=?) order by root_pk, table_pk, action, create_on_string desc";
	
	private static String getAdditionalInformationSQL = "select ELEMENT_NAME, ELEMENT_VALUE from ER_MSG_ADDINFOSVC where FK_MSG_QUEUE = ?"; 
	
	private static String deleteFromAddInfoSQL = "delete from ER_MSG_ADDINFOSVC where FK_MSG_QUEUE = ?";
	
	private static String insertIntoMSGQueueTrackTableSQL = "insert into ER_MSGQUEUETRACK (PK_MESSAGE_TRACK, MESSAGE_TYPE,MESSAGEPKLIST) VALUES (SEQ_ER_MSGQUEUETRACK.nextval,?,?)";
	
	public List<MessageQueueBean> getDataForProcessing(Date fromDate,Date toDate,int accountFK,String messageModule,String messageType)
	{
		 List<MessageQueueBean> list = new ArrayList<MessageQueueBean>(); 
		Connection conn = null; 
		PreparedStatement stmt = null; 
		ResultSet rs = null ;
		StringBuffer query = new StringBuffer();
		try
		{
			conn = getConnection(); 
			query.append("select pk_message, tablename, table_pk, module, action, root_pk, fk_account, processed_flag, root_tablename, to_char(created_on, PKG_DATEUTIL.f_get_dateformat||' '||PKG_DATEUTIL.f_get_timeformat) create_on_string from ER_MSG_QUEUESVC where (processed_flag = ? or processed_flag = ?) and fk_account = ? ");
			if(!StringUtil.isEmpty(messageModule))
				query.append("and MODULE = ? ");
			if(fromDate==null && toDate==null)
				query.append("AND created_on>(select NVL(MAX(TIMESTAMP),TO_DATE(PKG_DATEUTIL.f_get_null_date_str,PKG_DATEUTIL.F_GET_DATETIMEFORMAT)) from ER_MSGQUEUETRACK where MESSAGE_TYPE=?) ");
			else if(fromDate!=null && toDate==null)
				query.append("AND created_on between ? and TO_TIMESTAMP(PKG_DATEUTIL.f_get_future_null_date_str,PKG_DATEUTIL.F_GET_DATETIMEFORMAT) ");
			else if(fromDate==null && toDate!=null)
				query.append("AND created_on between TO_TIMESTAMP(PKG_DATEUTIL.f_get_null_date_str,PKG_DATEUTIL.F_GET_DATETIMEFORMAT) and ? ");
			else
				query.append("AND created_on between ? and ? ");
			query.append("order by root_pk, table_pk, action, create_on_string desc ");
			stmt = conn.prepareStatement(query.toString()); 
			stmt.setInt(1, MessagingConstants.MESSAGE_STATUS_NEW); 
			stmt.setInt(2, MessagingConstants.MESSAGE_STATUS_RE_PROCESS);
			stmt.setInt(3, accountFK);
			if(!StringUtil.isEmpty(messageModule)){
			stmt.setString(4, messageModule);
			if(fromDate==null && toDate==null)
				stmt.setString(5, messageType);
			else if(fromDate!=null && toDate==null)
				stmt.setTimestamp(5, DateUtil.dateToSqlTimestamp(fromDate));
			else if(fromDate==null && toDate!=null)
				stmt.setTimestamp(5, DateUtil.dateToSqlTimestamp(toDate));
			else{
				stmt.setTimestamp(5, DateUtil.dateToSqlTimestamp(fromDate));
				stmt.setTimestamp(6, DateUtil.dateToSqlTimestamp(toDate));
			  }
			}else{
				if(fromDate==null && toDate==null)
					stmt.setString(4, messageType);
				else if(fromDate!=null && toDate==null)
					stmt.setTimestamp(4, DateUtil.dateToSqlTimestamp(fromDate));
				else if(fromDate==null && toDate!=null)
					stmt.setTimestamp(4, DateUtil.dateToSqlTimestamp(toDate));
				else{
					stmt.setTimestamp(4, DateUtil.dateToSqlTimestamp(fromDate));
					stmt.setTimestamp(5, DateUtil.dateToSqlTimestamp(toDate));
				  }
				
			}
			
			
			rs = stmt.executeQuery(); 
		
			while(rs.next())
			{
				MessageQueueBean bean  = new MessageQueueBean(); 
				bean.setMessagePK(rs.getInt("pk_message"));
				bean.setTableName(rs.getString("tablename")); 
				bean.setTablePK(rs.getInt("table_pk")); 
				bean.setModule(rs.getString("module")); 
				bean.setAction(rs.getString("action")); 
			//	bean.setFieldsUpdated(rs.getString("fields_updated")); 
				bean.setRootFK(rs.getString("root_pk")); 
				bean.setAccountFK(rs.getInt("fk_account")); 
				bean.setProcessedFlag(rs.getInt("processed_flag"));
				bean.setRootTableName(rs.getString("root_tablename"));
				bean.setCreatedOn(rs.getString("create_on_string"));
				list.add(bean); 
				
			}
			
			
		}catch(SQLException sqe)
		{
			sqe.printStackTrace(); 
			logger.log(Level.ERROR, sqe);
		}
		finally
		{			
			closeRS(rs); 
			closeStatement(stmt); 
			returnConnection(conn); 
		}
		
		return list; 		
	}
	
public List<JAXBElement> getAddtionalInformation(int messagePK) {		

		
		Connection conn = null; 
		PreparedStatement stmt = null; 
		ResultSet rs = null ;
		List<JAXBElement> elements = new ArrayList<JAXBElement>(); 
	//	StringBuffer xmlElements = new StringBuffer(); 
		
		try
		{
			conn = getConnection(); 
			
			stmt = conn.prepareStatement(getAdditionalInformationSQL); 
			stmt.setInt(1, messagePK); 	
			
			rs = stmt.executeQuery(); 
		
			while(rs.next())
			{
				/*if(rs.getString(1).equalsIgnoreCase("UserIdentifier"))
				{
					elements.add(new JAXBElement(new QName(rs.getString(1)), UserIdentifier.class, rs.getString(2))); 
				}else
				{*/
					elements.add(new JAXBElement(new QName(rs.getString(1)), String.class, rs.getString(2))); 
				//}
			}
			
			
		}catch(SQLException sqe)
		{
			sqe.printStackTrace(); 
			logger.log(Level.ERROR, sqe);
		}
		finally
		{			
			closeRS(rs); 
			closeStatement(stmt); 
			returnConnection(conn); 
		}
		
		return elements	; 
		
	}

	public void insertIntoMSGQueueTrackTable(String messageType,String messagePk) throws SQLException{
	Connection conn = null; 
	PreparedStatement stmt = null ; 
	
	try
	{
		conn = getConnection();
		stmt = conn.prepareStatement(insertIntoMSGQueueTrackTableSQL); 
		
			stmt.setString(1, messageType);
			stmt.setString(2, messagePk);
			stmt.executeUpdate();
		
	}finally
	{
		closeStatement(stmt); 
		returnConnection(conn); 
	}
	
	}

public void closeRS(ResultSet rs)
{
	try {
		if(rs != null) rs.close(); 			
	} catch (SQLException e) {
		logger.error("Unable to close ResultSet", e); 
		e.printStackTrace();
	} 
}

public void closeStatement(PreparedStatement stmt)
{
	try{
		if(stmt !=null) stmt.close();			 
	}catch(SQLException e){
		logger.error("Unable to close PreparedStatement", e); 
	}
	
}

public void closeConnection(Connection conn)
{
	try{
		if(conn != null) conn.close(); 
	}catch(SQLException e)
	{
		logger.error("Unable to close Connection", e); 
	}
}

}
