package com.velos.services.OutboundMessage;

import java.util.Date;

import javax.ejb.Remote;

import com.velos.services.OperationException;
import com.velos.services.model.ChangesList;

@Remote
public interface OutboundMessageService {
	
	public ChangesList getOutboundMessages(Date fromDate,Date toDate,String messageModule,String calledFrom) throws OperationException;

}
