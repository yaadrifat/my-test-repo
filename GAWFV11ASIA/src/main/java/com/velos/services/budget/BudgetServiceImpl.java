package com.velos.services.budget;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import com.velos.eres.business.common.TeamDao;
import com.velos.eres.business.study.impl.StudyBean;
import com.velos.eres.service.grpRightsAgent.GrpRightsAgentRObj;
import com.velos.eres.service.studyAgent.StudyAgent;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.esch.business.budget.impl.BudgetBean;
import com.velos.esch.business.common.BudgetUsersDao;
import com.velos.esch.business.eventassoc.impl.EventAssocBean;
import com.velos.esch.service.budgetAgent.BudgetAgentRObj;
import com.velos.esch.service.eventassocAgent.EventAssocAgentRObj;
import com.velos.services.AbstractService;
import com.velos.services.AuthorizationException;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.OperationRolledBackException;
import com.velos.services.ResponseHolder;
import com.velos.services.ValidationException;
import com.velos.services.authorization.BudgetAuthModule;
import com.velos.services.authorization.GroupAuthModule;
import com.velos.services.authorization.TeamAuthModule;
import com.velos.services.map.MultipleObjectsFoundException;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.BudgetDetail;
import com.velos.services.model.BudgetIdentifier;
import com.velos.services.model.BudgetPojo;
import com.velos.services.model.BudgetStatus;
import com.velos.services.model.CalendarNameIdentifier;
import com.velos.services.model.Code;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.UserIdentifier;
import com.velos.services.util.CodeCache;
import com.velos.services.util.ObjectLocator;

@Stateless
@Remote(BudgetService.class)
public class BudgetServiceImpl extends AbstractService implements BudgetService{
	
	@EJB
	private UserAgentRObj userAgent;
	@EJB
	private ObjectMapService objectMapService;
	@EJB
	private BudgetAgentRObj budgetAgent; 
	@EJB
	GrpRightsAgentRObj groupRightsAgent;
	
	@EJB
	private StudyAgent studyAgent;
	@EJB
	private EventAssocAgentRObj eventAssocAgent;

	
	
	
	@Resource
	private SessionContext sessionContext; 
	
	private static Logger logger = Logger.getLogger(BudgetServiceImpl.class.getName());

	/**
	 * 
	 * @param studyIdentifier
	 * @param budgetIdentifier
	 * @return
	 * @throws OperationException
	 */
	public BudgetStatus getBudgetStatus(BudgetIdentifier budgetIdentifier) throws OperationException{
		BudgetStatus budgetStatus= null;
		try{
			Integer budgetPK = locateBudgetPK(budgetIdentifier);
			
			if(budgetPK == 0 ){
				Issue issue = new Issue(IssueTypes.BUDGET_NOT_FOUND, "For Budget ID : "+budgetIdentifier.getOID());
				addIssue(issue);
				logger.error(issue);
				throw new OperationException(issue.toString());
				
			}
			budgetStatus = getBudgetStatus(budgetPK);
		
		} 
		catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("StudyServiceImpl removeStudyOrganization", e);
			throw new OperationRolledBackException(response.getIssues());
	
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyServiceImpl removeStudyOrganization", t);
			throw new OperationRolledBackException(response.getIssues());
	
		}
		
		return budgetStatus;
	}

	/**
	 * 
	 * @param budgetName
	 * @return
	 * @throws OperationException 
	 */
	private BudgetStatus getBudgetStatus(Integer budgetPK) throws OperationException {
		
		BudgetBean budgetBean = budgetAgent.getBudgetDetails(budgetPK);
		checkBudgetStatusPermissions(budgetBean); 
		BudgetStatus budgetStatus = new BudgetStatus();
		BudgetIdentifier budgetIdentifier = new BudgetIdentifier();
		ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_CALENDAR_BUDGET, budgetPK);
		
		budgetIdentifier.setBudgetName(budgetBean.getBudgetName());
		budgetIdentifier.setBudgetVersion(budgetBean.getBudgetVersion()); 
		budgetIdentifier.setPK(budgetPK);
		budgetIdentifier.setOID(map.getOID());
		
		budgetStatus.setBudgetIdentifier(budgetIdentifier);
		
		CodeCache codeCache = CodeCache.getInstance();
		
		Code budgetStatusCode = 
			codeCache.getSchCodeSubTypeByPK(
					CodeCache.CODE_CALENDAR_BUDGET_STATUS_TYPE, 
					EJBUtil.stringToInteger(budgetBean.getBudgetCodeListStatus()),
					callingUser.getUserId());
		budgetStatus.setBudgetStatus(budgetStatusCode);
		
		UserIdentifier usrBudgetStatusChangedBy = getUserIdFromPKStr(budgetBean.getModifiedBy(), objectMapService, userAgent);
		budgetStatus.setStatusChangedBy(usrBudgetStatusChangedBy);
		budgetStatus.setStatusDate(budgetBean.getLastModifiedDt());
		
		return budgetStatus;
		
	}
	private Integer locateBudgetPK(BudgetIdentifier budgetIdentifier) 
	throws OperationException{
		Integer budgetPK = null; 
		if  (budgetIdentifier == null ||
				((budgetIdentifier.getPK() == null || budgetIdentifier.getPK()<=0) &&(budgetIdentifier.getOID() == null || budgetIdentifier.getOID().length() ==0) && 
						(budgetIdentifier.getBudgetName() == null || budgetIdentifier.getBudgetName().length() == 0) ) ){
			
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid Budget Identifier : OID or Budget Name or Budget PK is required"));
			throw new ValidationException();
		}
		try
		{
			budgetPK =
				ObjectLocator.budgetPKFromIdentifier(callingUser, budgetIdentifier,objectMapService);
		}catch(MultipleObjectsFoundException mobe)
		{
			addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND, "Multiple Budgets found"));
			throw new OperationException(); 
		}
		
		
		if (budgetPK == null || budgetPK == 0){
			StringBuffer errorMessage = new StringBuffer("Budget not found for:"); 
			if(budgetIdentifier.getOID() != null && budgetIdentifier.getOID().length() > 0 )
			{
				errorMessage.append(" OID:" + budgetIdentifier.getOID()); 
			}
			else if(budgetIdentifier.getBudgetName() != null && budgetIdentifier.getBudgetName().length() > 0)
			{
				errorMessage.append( " Budget name:" + budgetIdentifier.getBudgetName()); 
				if(budgetIdentifier.getBudgetVersion() != null && budgetIdentifier.getBudgetVersion().length() > 0)
				{
					errorMessage.append(" BudgetVersion:" + budgetIdentifier.getBudgetVersion()); 
				}
			}
			addIssue(new Issue(IssueTypes.BUDGET_NOT_FOUND, errorMessage.toString()));
			throw new OperationException();
		}
		return budgetPK;
	}
	
	
	private void checkGroupProtocolViewPermissions() throws OperationException
	{
		//Get the user's permission for viewing protocols
		GroupAuthModule groupAuth = 
			new GroupAuthModule(callingUser, groupRightsAgent);

		Integer manageProtocolPriv = 
			groupAuth.getAppManageProtocolsPrivileges();

		boolean hasViewManageProt = 
			GroupAuthModule.hasViewPermission(manageProtocolPriv);
		if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
		if (!hasViewManageProt){
			addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User not Authorized to view budgets status and cant access Budgets"));
			throw new AuthorizationException("User not Authorized to view budgets status and cant access Budgets");
		}
	}
	
	private void checkStudyTeamViewPermissions(int studyPK) throws OperationException
	{
		TeamAuthModule teamAuth = 
			new TeamAuthModule(callingUser.getUserId(), studyPK );
		Integer manageStudySetup = teamAuth.getStudySetupPrivileges(); 
		
		boolean hasViewStudyCalPermissions = TeamAuthModule.hasViewPermission(manageStudySetup); 
		if(logger.isDebugEnabled()) logger.debug("user study team - STUDYCAL priv :" + manageStudySetup); 
		if(!hasViewStudyCalPermissions)
		{
			addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User not Authorized to view budgets status and cant access Budgets"));
			throw new AuthorizationException("User not authorized to view budgets status and cant access Budgets");
		}
		
		
	}
	private void checkGroupBudgetViewPermissions() throws OperationException
	{
		//Get the user's permission for viewing budget
		GroupAuthModule groupAuth = 
			new GroupAuthModule(callingUser, groupRightsAgent);

		Integer manageBudgetPriv = 
			groupAuth.getAppBudgetPrivileges();

		boolean hasViewManageBudget = 
			GroupAuthModule.hasViewPermission(manageBudgetPriv);
		if (logger.isDebugEnabled()) logger.debug("user manage budget priv: " + manageBudgetPriv);
		if (!hasViewManageBudget){
			addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User not authorized to view budgets status and cant access Budgets"));
			throw new AuthorizationException("User not Authorized to view Budget Status");
		}
	}
	
	
	private void checkBudgetDetailsPermissions(BudgetBean budgetBean) throws OperationException
	{
		
		if(budgetBean.getBudgetCombinedFlag() != null && budgetBean.getBudgetCombinedFlag().equalsIgnoreCase("Y"))
		{
			Integer studyPK = null; 
			if(budgetBean.getBudgetStudyId() != null){
				studyPK = EJBUtil.stringToInteger(budgetBean.getBudgetStudyId());				
			}
			checkGroupProtocolViewPermissions(); 
			checkStudyTeamViewPermissions(studyPK); 
		}else
		{
			checkGroupBudgetViewPermissions(); 
		}
		
		BudgetAuthModule budgetAuth = new BudgetAuthModule(budgetBean, callingUser);
		Integer budgetPriv = budgetAuth.getBudgetDetailsPriviledges(); 
		if(!BudgetAuthModule.hasViewPermission(budgetPriv)) 
		{
			addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User not authorized to view budgets status and cant access Budgets"));
			throw new AuthorizationException("User not Authorized to view Budget Status");
		}
		
		
	}
	
	
	private void checkBudgetStatusPermissions(BudgetBean budgetBean) throws OperationException
	{

		boolean hasAccess  = false; 
		GroupAuthModule groupAuth = new GroupAuthModule(callingUser, groupRightsAgent); 
		Integer grpBudget =  groupAuth.getAppBudgetPrivileges(); 
		boolean hasViewBudgetGrpRights = GroupAuthModule.hasViewPermission(grpBudget);
		if(hasViewBudgetGrpRights)
		{
			String scope = budgetBean.getBudgetRScope(); 
			if(!StringUtil.isEmpty(scope))
			{
				if(scope.equalsIgnoreCase("A"))
				{
					if(budgetBean.getBudgetAccountId().equalsIgnoreCase(callingUser.getUserAccountId()))
					{
						hasAccess = true; 								
					}
				}else if(scope.equalsIgnoreCase("O"))
				{
					if(budgetBean.getBudgetSiteId().equalsIgnoreCase(callingUser.getUserSiteId()))
					{
						hasAccess = true; 							
					}
				}else if(scope.equalsIgnoreCase("S"))
				{
					// check if user is in studyTeam. 
					String studyID = budgetBean.getBudgetStudyId(); 
					TeamDao teamDao = new TeamDao(); 
					teamDao.findUserInTeam(EJBUtil.stringToNum(budgetBean.getBudgetStudyId()),callingUser.getUserId());
					if (teamDao.getCRows() > 0) {
						hasAccess = true; 							
					}else
					{
						teamDao.getSuperUserTeam(EJBUtil.stringToNum(budgetBean.getBudgetStudyId())); 
						if(teamDao.getUserIds().contains(callingUser.getUserId()))
						{

							hasAccess = true;  									
						}
					}

				}
			}
			
			//individual user access right overrides grpRight and scopeRight - not applicable to combined Budgets
			if(budgetBean.getBudgetCombinedFlag() == null)
			{
				BudgetUsersDao bgtUsersDao = new BudgetUsersDao();	            
	            String userRightStr = bgtUsersDao.getBudgetUserRight(budgetBean.getBudgetId(), callingUser.getUserId());
			    if (!StringUtil.isEmpty(userRightStr)) {
			    	hasAccess = true;  			    
			    }	           
			}		
		}else if(budgetBean.getBudgetCombinedFlag() != null && budgetBean.getBudgetCombinedFlag().equalsIgnoreCase("Y"))
		{
			Integer studyPK = null; 
			if(budgetBean.getBudgetStudyId() != null){
				studyPK = EJBUtil.stringToInteger(budgetBean.getBudgetStudyId());				
			}
			checkGroupProtocolViewPermissions(); 
			checkStudyTeamViewPermissions(studyPK); 
			hasAccess = true; 
			
		}
		
		if(!hasAccess)
		{
			addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User not authorized to view budgets status and cant access Budgets"));
			throw new AuthorizationException("User not Authorized to view Budget Status");
		}
		
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public BudgetDetail getStudyCalBudget(StudyIdentifier studyIdent,CalendarNameIdentifier CalIdent)
			throws OperationException {
		try
		{  
			if(studyIdent==null || (StringUtil.isEmpty(studyIdent.getOID()) 
					&& (studyIdent.getPK()==null || studyIdent.getPK()<=0) 
					&& StringUtil.isEmpty(studyIdent.getStudyNumber())))
			{
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid studyIdentifier is required to get Budgets"));
			throw new OperationException();
			}
			
			Integer studyPK = locateStudyPK(studyIdent);
			if(studyPK == null || studyPK == 0){
				addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found for StudyIdentifier OID:" + studyIdent.getOID() + " StudyNumber " + studyIdent.getStudyNumber()));
				throw new OperationException();
			}

			StudyBean sb = studyAgent.getStudyDetails(studyPK);
			
			if (sb == null || (! sb.getAccount().equals(callingUser.getUserAccountId()))){
				StringBuffer errorMessage = new StringBuffer("Study not found for:"); 
				if(studyIdent.getOID() != null && studyIdent.getOID().length() > 0 )
				{
					errorMessage.append(" OID:" + studyIdent.getOID()); 
				}
				if(studyIdent.getStudyNumber() != null && studyIdent.getStudyNumber().length() > 0)
				{
					errorMessage.append( " Study Number:" + studyIdent.getStudyNumber()); 
				}
				if(!StringUtil.isEmpty(StringUtil.integerToString(studyIdent.getPK())))
				{
					errorMessage.append( " Study Pk: " + studyIdent.getPK());
				}
				Issue issue = new Issue(IssueTypes.STUDY_NOT_FOUND, errorMessage.toString());
				addIssue(issue);
				logger.error(issue);
				throw new OperationException(); 
			}
			
			studyIdent.setPK(studyPK);
			ObjectMap objMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY, studyPK); 
			studyIdent.setOID(objMap.getOID()); 
			
			if(CalIdent==null || (StringUtil.isEmpty(CalIdent.getOID()) 
					&& (CalIdent.getPK()==null || CalIdent.getPK()<=0) 
					&& StringUtil.isEmpty(CalIdent.getCalendarName()) && StringUtil.isEmpty(CalIdent.getCalendarAssocTo())))
			{
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid CalendarNameIdentifier is required to get Budgets"));
			throw new OperationException();
			}
			
			Integer calPK = locateCalPK(CalIdent,studyPK);
			if(calPK == null || calPK == 0){
				addIssue(new Issue(IssueTypes.CALENDAR_NOT_FOUND, "Calendar not found for CalendarNameIdentifier OID:" + CalIdent.getOID() + " CalendarName " + CalIdent.getCalendarName()));
				throw new OperationException();
			}
			
			CalIdent.setPK(calPK);
			objMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY_CALENDAR, calPK); 
			CalIdent.setOID(objMap.getOID()); 

			
			
			GroupAuthModule groupAuth = 
				new GroupAuthModule(this.callingUser, groupRightsAgent);

			Integer manageBudgetPriv = 
				groupAuth.getAppBudgetPrivileges();

			boolean hasViewManageBudget = 
				GroupAuthModule.hasViewPermission(manageBudgetPriv);
			if (logger.isDebugEnabled()) logger.debug("user manage Budget priv: " + manageBudgetPriv);
			if (!hasViewManageBudget){
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to view Study Calendar Budgets"));
				throw new AuthorizationException("User Not Authorized to view study Calendar Budgets");
			}
		  	    

	      
			Map<String, Object> parameters = new HashMap<String, Object>(); 

			parameters.put("budgetAgent", budgetAgent);
			parameters.put("sessionContext", sessionContext); 
			parameters.put("userAgent", userAgent);
			parameters.put("objectMapService", objectMapService);
			parameters.put("callingUser", callingUser);
			parameters.put("ResponseHolder", response); 
			parameters.put("studyAgent", studyAgent);
	        BudgetDetail budgets=new BudgetDetail();
	        budgets=BudgetServiceDAO.getStudyCalBudget(studyIdent,CalIdent,parameters);
	        return budgets;
		}
	        catch(OperationException e){
				e.printStackTrace();
				if (logger.isDebugEnabled()) logger.debug("BudgetServiceImpl retrieved ", e);
				e.setIssues(response.getIssues());
				throw e;
			}
			catch(Throwable t){
				this.addUnknownThrowableIssue(t);
				if (logger.isDebugEnabled()) logger.debug("BudgetServiceImpl retrieved", t);
				throw new OperationException(t);
			}
	}
	
	@Override
	public ResponseHolder createStudyCalBudget(BudgetDetail budgetDetail)
			throws OperationException {
		
		BudgetPojo schBudget = new BudgetPojo();
		schBudget = budgetDetail.getBudgetInfo();
		StudyIdentifier studyIdent=new StudyIdentifier();
		studyIdent = budgetDetail.getStudyIdentifier();
		CalendarNameIdentifier calIdent=new CalendarNameIdentifier();
		calIdent=budgetDetail.getCalendarIdentifier();
		BudgetIdentifier budgetIdentifier=new BudgetIdentifier();
		budgetIdentifier = budgetDetail.getBudgetIdentifier();
		
		try
		{
		
			System.out.println(" ---inside the createBudgets method----");
			//--------Checking group authorization-------------//
			GroupAuthModule groupAuth = 
				new GroupAuthModule(this.callingUser, groupRightsAgent);

			Integer manageBudgetPriv = 
				groupAuth.getAppBudgetPrivileges();
			if(!GroupAuthModule.hasNewPermission(manageBudgetPriv))
			{
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User is not authorized to Add Study Calendar Budget details")); 
				throw new AuthorizationException(); 
			}
			if(schBudget == null)
			{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid BudgetInfo is required")); 
				throw new OperationException(); 
			}
			
		
			Map<String, Object> parameters = new HashMap<String, Object>(); 

			parameters.put("budgetAgent", budgetAgent);
			parameters.put("sessionContext", sessionContext); 
			parameters.put("userAgent", userAgent);
			parameters.put("objectMapService", objectMapService);
			parameters.put("callingUser", callingUser);
			parameters.put("ResponseHolder", response); 
			parameters.put("studyAgent", studyAgent);

			if(studyIdent==null || (StringUtil.isEmpty(studyIdent.getOID()) 
					&& (studyIdent.getPK()==null || studyIdent.getPK()<=0) 
					&& StringUtil.isEmpty(studyIdent.getStudyNumber())))
			{
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid StudyIdentifier is required to Create Study Calendar Budget"));
			throw new OperationException();
			}
			
			StudyBean studyBean = null;
			Integer studyPK =null;
				
				// get StudyPK for checking Team rights for Study
				studyPK=null;
				studyPK = locateStudyPK(studyIdent);
				if(studyPK == null || studyPK == 0){
					addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found for StudyIdentifier OID:" + studyIdent.getOID() + " StudyNumber " + studyIdent.getStudyNumber()));
					throw new OperationException();
				}

				
				studyBean =null;
				studyBean = studyAgent.getStudyDetails(studyPK);
				if(studyBean==null)
				{
					((ResponseHolder)parameters.get("ResponseHolder")).addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND,"Study Not found"));
					throw new OperationException(); 
				}
				studyIdent.setPK(studyPK);
				ObjectMap objMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY, studyPK); 
				studyIdent.setOID(objMap.getOID());
				
				if(calIdent==null || (StringUtil.isEmpty(calIdent.getOID()) 
						&& (calIdent.getPK()==null || calIdent.getPK()<=0) 
						&& StringUtil.isEmpty(calIdent.getCalendarName()) && StringUtil.isEmpty(calIdent.getCalendarAssocTo())))
				{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid CalendarNameIdentifier is required to Create Study Calendar Budget"));
				throw new OperationException();
				}
				EventAssocBean eventAssocBn = null;
				
				Integer calPK = locateCalPK(calIdent,studyPK);
				if(calPK == null || calPK == 0){
					addIssue(new Issue(IssueTypes.CALENDAR_NOT_FOUND, "Calendar not found for CalendarNameIdentifier OID:" + calIdent.getOID() + " CalendarName " + calIdent.getCalendarName()));
					throw new OperationException();
				}
				
				eventAssocBn = eventAssocAgent.getEventAssocDetails(calPK);
				
				if(eventAssocBn==null)
				{
					((ResponseHolder)parameters.get("ResponseHolder")).addIssue(new Issue(IssueTypes.CALENDAR_NOT_FOUND,"Calendar not found"));
					throw new OperationException(); 
				}
				Integer bgtTemp =  (eventAssocBn.getBudgetTemplate()==null)?0:StringUtil.stringToInteger(eventAssocBn.getBudgetTemplate());
				
				if(bgtTemp>0){
					addIssue(new Issue(IssueTypes.BUDGET_ALREADY_EXISTS, "Budget already exists with Calendar:: " + calIdent.getCalendarName()));
					throw new OperationException();
				}
				
				calIdent.setPK(calPK);
				objMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY_CALENDAR, calPK); 
				calIdent.setOID(objMap.getOID()); 
				
				budgetDetail.setCalendarIdentifier(calIdent);
				budgetDetail.setStudyIdentifier(studyIdent);
				
				if(budgetIdentifier==null || (StringUtil.isEmpty(budgetIdentifier.getOID()) 
						&& (budgetIdentifier.getPK()==null || budgetIdentifier.getPK()<=0) 
						&& StringUtil.isEmpty(budgetIdentifier.getBudgetName())))
				{
				addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid BudgetIdentifier is required to Create Study Calendar Budget"));
				throw new OperationException();
				}
				if(budgetIdentifier.getBudgetName()==null){
					addIssue(new Issue(IssueTypes.DATA_VALIDATION ,"Budget Name is a mandatory."));
					throw new OperationException();	
				}
				BudgetServiceDAO bgtDao = new BudgetServiceDAO();
				Integer bgtPk = bgtDao.locateBudgetPK(budgetIdentifier.getBudgetName(),budgetIdentifier.getBudgetVersion(),callingUser.getUserAccountId());
				if(bgtPk > 0){
					addIssue(new Issue(IssueTypes.DUPLICATE_BUDGET_NAME, "Duplicate Budget Name for BudgetIdentifier of BudgetDetail OID:" + budgetIdentifier.getOID() + " BudgetName " + budgetIdentifier.getBudgetName()+ " BudgetVersion " + budgetIdentifier.getBudgetVersion()));
					throw new OperationException();
				}
				
			BudgetServiceDAO handler = new BudgetServiceDAO(); 
			String value=handler.saveStudyCalBudget(budgetDetail,parameters);
			int ret=StringUtil.stringToNum(value);
			System.out.println("Returned is "+ret);
				if(value!=null)
				{
					ObjectMap budgetMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_BUDGET, ret); 
					budgetIdentifier = new BudgetIdentifier(); 
					budgetIdentifier.setOID(budgetMap.getOID()); 
					budgetIdentifier.setPK(ret);
					response.addObjectCreatedAction(budgetIdentifier);
				}
				else
				{
					response.addIssue(new Issue(IssueTypes.ERROR_CREATE_BUDGET, value));
				}
			}
			
		
		catch(OperationException e){
			if (logger.isDebugEnabled()) logger.debug("BudgetServiceImpl createBudgets", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("BudgetServiceImpl createBudgtes", t);
			throw new OperationException(t);
		}
		// TODO Auto-generated method stub
		return response;
	}
	
	private Integer locateCalPK(CalendarNameIdentifier calIdent, Integer studyPK) 
	throws OperationException, HibernateException, IOException{
		//Virendra:#6123,added OID in if clause
		if(calIdent==null || (StringUtil.isEmpty(calIdent.getOID()) 
				&& (calIdent.getPK()==null || calIdent.getPK()<=0) 
				&& StringUtil.isEmpty(calIdent.getCalendarName()) && StringUtil.isEmpty(calIdent.getCalendarAssocTo())))
		{
		addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid calendarNamedentifier is required to get Budget"));
		throw new OperationException();
		}
		
		Integer calPK =
				ObjectLocator.calPKFromIdentifier(this.callingUser, calIdent,studyPK,objectMapService);
		if (calPK == null || calPK == 0){
			StringBuffer errorMessage = new StringBuffer("Calendar not found for:"); 
			if(calIdent.getOID() != null && calIdent.getOID().length() > 0 )
			{
				errorMessage.append(" OID: " + calIdent.getOID()); 
			}
			if(calIdent.getCalendarName() != null && calIdent.getCalendarName().length() > 0)
			{
				errorMessage.append( " Calendar Name: " + calIdent.getCalendarName()); 
			}
			addIssue(new Issue(IssueTypes.CALENDAR_NOT_FOUND, errorMessage.toString()));
			throw new OperationException();
		}
		return calPK;
	}
	
	private Integer locateStudyPK(StudyIdentifier studyIdent) 
	throws OperationException{
		//Virendra:#6123,added OID in if clause
		if(studyIdent==null || (StringUtil.isEmpty(studyIdent.getOID()) 
				&& (studyIdent.getPK()==null || studyIdent.getPK()<=0) 
				&& StringUtil.isEmpty(studyIdent.getStudyNumber())))
		{
		addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid studydentifier is required to add Budget"));
		throw new OperationException();
		}
		
		Integer studyPK =
				ObjectLocator.studyPKFromIdentifier(this.callingUser, studyIdent,objectMapService);
		if (studyPK == null || studyPK == 0){
			StringBuffer errorMessage = new StringBuffer("Study not found for:"); 
			if(studyIdent.getOID() != null && studyIdent.getOID().length() > 0 )
			{
				errorMessage.append(" OID: " + studyIdent.getOID()); 
			}
			if(studyIdent.getStudyNumber() != null && studyIdent.getStudyNumber().length() > 0)
			{
				errorMessage.append( " Study Number: " + studyIdent.getStudyNumber()); 
			}
			addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, errorMessage.toString()));
			throw new OperationException();
		}
		return studyPK;
	}
	
	@AroundInvoke
	public Object myInterceptor(InvocationContext ctx) throws Exception {
		response = new ResponseHolder();
		callingUser = 
			getLoggedInUser(
					sessionContext,
					userAgent);
		return ctx.proceed();

	}
	
}
