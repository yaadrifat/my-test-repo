package com.velos.services.budget;

import javax.ejb.Remote;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.BudgetDetail;
import com.velos.services.model.BudgetIdentifier;
import com.velos.services.model.BudgetStatus;
import com.velos.services.model.CalendarNameIdentifier;
import com.velos.services.model.StudyIdentifier;

@Remote
public interface BudgetService {
	
	public BudgetStatus getBudgetStatus(BudgetIdentifier budgetIdentifier) throws OperationException;
	
	public BudgetDetail getStudyCalBudget(StudyIdentifier studyIdent,CalendarNameIdentifier CalIdent)throws OperationException;

	public ResponseHolder createStudyCalBudget(BudgetDetail budgetDetail)throws OperationException;

}
