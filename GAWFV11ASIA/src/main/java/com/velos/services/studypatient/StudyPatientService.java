package com.velos.services.studypatient;

import javax.ejb.Remote;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.CreateMultiPatientStudyStatuses;
import com.velos.services.model.Patient;
import com.velos.services.model.PatientEnrollmentDetails;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientStudyStatusIdentifier;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyPatientResults;
import com.velos.services.model.StudyPatientSearch;
import com.velos.services.model.StudyPatientStatuses;
import com.velos.services.model.UpdateMPatientStudyStatuses;
/**
 * Remote Interface with declaration of methods in Service Implementation.
 * @author Virendra
 *
 */
@Remote
public interface StudyPatientService{
	/**
	 * 
	 * @param studyId
	 * @return
	 * @throws OperationException
	 */
	public StudyPatientResults getStudyPatients(StudyPatientSearch StudyPatSearch) throws OperationException;

	/**
	 * Enrolls Patient to Study
	 * 
	 * @param patientIdentifier identifies patient to be enrolled
	 * 
	 * @param studyIdentifier identifies study to which patient has to be enrolled
	 * 
	 * @param patientEntrollmentDetails gives detailed information about patient status on study
	 * 
	 * @return A ResponseHolder instance that may hold information about database persisting 
	 * actions that occurred as a result of this operation
	 * 
	 * @throws OperationException
	 */
	public ResponseHolder enrollPatientToStudy(
			PatientIdentifier patientIdentifier, StudyIdentifier studyIdentifier, PatientEnrollmentDetails patientEntrollmentDetails) throws OperationException;
	
	
	/**
	 * Creates Patient and enroll created Patient to Study
	 * 
	 * @param Patient identifies patient to be created and enrolled to study
	 * 
	 * @param studyIdentifier identifies study to which patient has to be enrolled
	 * 
	 * @param patientEntrollmentDetails gives detailed information about patient status on study
	 * 
	 * @return A ResponseHolder instance that may hold information about database persisting 
	 * actions that occurred as a result of this operation
	 * 
	 * @throws OperationException
	 */
	public ResponseHolder createAndEnrollPatient(Patient patient, StudyIdentifier studyIdentifier, PatientEnrollmentDetails patientEnrollmentDetails)
	throws OperationException; 
	
	
	/**
	 * Deletes a study patient status 
	 * 
	 * @param PatientStudyStatusIdentifier
	 * 
	 * @return A ResponseHolder instance that may hold information about database persisting 
	 * actions that occurred as a result of this operation
	 * 
	 * @throws OperationException
	 */
	public ResponseHolder deleteStudyPatientStatus(PatientStudyStatusIdentifier patientStudyStatusIdentifier,String reasonForDelete) 
			throws OperationException;
	
	
	/**
	 * Updates a study patient status.
	 * @param patientStudyStatusIdentifier
	 * @param patientEnrollmentDetails
	 * @return
	 * @throws OperationException
	 */
	public ResponseHolder updateStudyPatientStatus(PatientStudyStatusIdentifier patientStudyStatusIdentifier,PatientEnrollmentDetails patientEnrollmentDetails)
	throws OperationException;

	
	public StudyPatientStatuses getStudyPatientStatusHistory(StudyIdentifier studyIdentifier, PatientIdentifier patientIdentifier)
	throws OperationException; 
	
	public ResponseHolder addStudyPatientStatus(PatientEnrollmentDetails studyPatientStatusDetails, 
			PatientIdentifier patientIdentifier, 
			StudyIdentifier studyIdentifier)
	throws OperationException;
	
	/**
	 * @param patientStudyStatusIdentifier
	 * @return PatientEnrollmentDetails
	 * @throws OperationException
	 */
	public PatientEnrollmentDetails getStudyPatientStatus(PatientStudyStatusIdentifier patientStudyStatusIdentifier)
	throws OperationException;

	
	public ResponseHolder createMPatientStudyStatus(CreateMultiPatientStudyStatuses createMPatientStudyStatuses)
			throws OperationException;
	

	
	/**
	 * Updates a study multiple patient status.
	 * @param updateMPatientStudyStatuses
	 * @return
	 * @throws OperationException
	 */
	public ResponseHolder updateMPatientStudyStatus(UpdateMPatientStudyStatuses updateMPatientStudyStatuses)
			throws OperationException;

}