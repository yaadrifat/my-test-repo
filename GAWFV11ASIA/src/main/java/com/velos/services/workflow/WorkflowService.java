package com.velos.services.workflow;

import javax.ejb.Remote;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.StudyIdentifier;
@Remote
public interface WorkflowService {
	/**
	 * Calculate workflow for a study
	 * 
	 * @param studyId Identifies the study to calculate
	 * 
	 * @return A ResponseHolder instance that may hold information about database persisting 
	 * actions that occurred as a result of this operation.
	 * 
	 * @throws OperationException
	 */
	public ResponseHolder calculateWorkFlow(StudyIdentifier studyId) throws OperationException;
}
