package com.velos.validator;

import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.web.formLib.FormLibJB;

public class FormValidator extends Validator {

	public FormValidator() {

	}

	public FormValidator(String validateField, String data, String targetVal) {
		super(validateField, data, targetVal);

	}

	public boolean validate() {
		String operator;
		boolean validateFlag = false;
		int ret;
		if (validateFld.equals("fldvalidate")) {
			FormLibJB formLibJB = new FormLibJB();
			ret = formLibJB.validateFormField(
					EJBUtil.stringToNum(dataArray[1]), EJBUtil
							.stringToNum(dataArray[2]), dataArray[3], EJBUtil
							.stringToNum(dataArray[4]), EJBUtil
							.stringToNum(dataArray[5]), EJBUtil
							.stringToNum(dataArray[6]), targetValue,
					dataArray[0]);
			if (ret > 0)
				validateFlag = true;
			else
				validateFlag = false;

		}
		if (validateFld.equals("pfldvalidate")) {
			String validateMsg="";
			FormLibJB formLibJB = new FormLibJB();
			validateMsg = formLibJB.validateFormField(
					EJBUtil.stringToNum(dataArray[0]),dataArray[1], EJBUtil
							.stringToNum(dataArray[2]), EJBUtil
							.stringToNum(dataArray[3]), EJBUtil
							.stringToNum(dataArray[4]),targetValue);
			validateMsg=(validateMsg==null)?"":validateMsg;
			
			if (validateMsg.length()>0) ret=0;
			else ret=1;
			
			if (ret > 0) {
				validateFlag = true;
				
			}
			
			else {
				validateFlag = false;
				this.setValidationMessage(validateMsg);
			}

		}

		

		return validateFlag;
	}

}