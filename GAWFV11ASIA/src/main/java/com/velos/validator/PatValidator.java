package com.velos.validator;

import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.web.pref.PrefJB;
import com.velos.eres.web.user.UserJB;



public class PatValidator extends Validator
{
  

public PatValidator()
 {
  
 }
 public PatValidator(String validateField,String data,String targetVal)
 {
	 super(validateField,data,targetVal);
     
     
 }
 public boolean validate()
 {
	 if (validateFld.equals("patid")) {
	 boolean codeExists;
	 PrefJB prefB =new PrefJB();
     codeExists=prefB.patientCodeExists(targetValue,dataArray[0]);
     codeExists=!codeExists;
          return codeExists;
	 }
	 if (validateFld.equals("fldob")) {
		 boolean patExists;
		 PrefJB prefB =new PrefJB();
		 patExists=prefB.patientCheck(dataArray[0], dataArray[1], dataArray[2],dataArray[3],dataArray[4]);
	     patExists=!patExists;
	     return patExists;
		 }
	 
	 return false;
     
 }
 
 
 
 
 public String getDataSet() {
     return dataSet;
 }
 public void setDataSet(String dataSet) {
     this.dataSet = dataSet;
 }
 public String getValidateFld() {
     return validateFld;
 }
 public void setValidateFld(String validateFld) {
     this.validateFld = validateFld;
 }
 
}