package com.velos.eres.querymanagement.business;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.service.util.Rlog;

public class QueryManagementDAO {
	public ArrayList<HashMap<String, Object>> fetchFormQueries(
			HashMap<String, String> paramMap, int dispLength, int dispStart, int sortCol, String sortDir) {
				
		String accountId = paramMap.get("accountId");
		String userId = paramMap.get("userId");
		String sSearch=paramMap.get("sSearch");
		String studyIds = paramMap.get("studyIds");
		String orgIds  = paramMap.get("orgIds");
		String formIds = paramMap.get("formId");
		String formStatusIds = paramMap.get("formStatusId");
		String dataEntryDateFrom = paramMap.get("dataEntryDateFrom");
		String dataEntryDateTo = paramMap.get("dataEntryDateTo");
		String patientIds = paramMap.get("patientId");
		String patientStdIds = paramMap.get("patientStdId");
		String patientCalIds = paramMap.get("patientCalId");
		String patientVisitIds = paramMap.get("patientVisitId");
		String patientEventIds = paramMap.get("patientEventId");
		String eventDate = paramMap.get("eventDate");
		String queryCreatorIds = paramMap.get("queryCreatorId");
		String selectedTotalRespQuery = paramMap.get("selectedTotalRespQuery");
		String selectedqueryStatuess = paramMap.get("selectedqueryStatuess");
		String queryStatusIds = paramMap.get("queryStatusId");		
		String noFilter = paramMap.get("noFilter");
		String qryStatusId = paramMap.get("qryStatusId");
		String qryStatusRed = paramMap.get("qryStatusRed");
		String qryStatusOrange = paramMap.get("qryStatusOrange");
		String qryStatusYellow = paramMap.get("qryStatusYellow");
		String qryStatusPurple = paramMap.get("qryStatusPurple");
		String qryStatusWhite = paramMap.get("qryStatusWhite");
		String qryStatusGreen = paramMap.get("qryStatusGreen");
		ArrayList<HashMap<String, Object>> maplist = new ArrayList<HashMap<String, Object>>();
		int firstRec = dispStart;
		int LastRec = dispLength+dispStart;
		//sortCol++;
		StringBuffer sqlBuffer = new StringBuffer();
		StringBuffer sqlOrderBy=new StringBuffer();
		StringBuffer filterRight=new StringBuffer();
		if(!"".equalsIgnoreCase(studyIds)){
		if("0".equalsIgnoreCase(orgIds)){ 
			//Bug id #21071 fixed by Rashi Starts
			filterRight.append(") where   ( 0 < pkg_superuser.F_Is_Superuser("+userId+",PK_STUDY))") ;
				sqlBuffer.append("Select b.*,rownum rnum from(SELECT setColor,total_queries,DECODE(FK_CODELST_QUERYSTATUS,(SELECT pk_codelst FROM er_codelst WHERE codelst_type  ='query_status' AND codelst_subtyp  ='closed'),'(No Aged Query)', NVL((SELECT codelst_desc FROM er_codelst WHERE PK_CODELST = FK_CODELST_QUERYSTATUS),'-')) query_status,(CASE  WHEN days_open is null THEN 0 ELSE days_open END) days_open  , study_number, '-' AS site_name,'-' AS PATSTDID,form_name,(SELECT codelst_desc FROM er_codelst WHERE PK_CODELST = Form_Status_id) form_status, TO_CHAR(Data_Entry_Date,PKG_DATEUTIL.F_GET_DATEFORMAT) data_entry_date, TO_CHAR(modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT) modified_date,(SELECT USR_FIRSTNAME||', '||USR_LASTNAME FROM er_user WHERE pk_user=data_entered_by) data_entered_by,'-' AS Calender_name,'-' AS VISIT_NAME,'-' AS event_name,'-' AS event_end_date,PK_STUDYFORMS AS pk_form,FK_FORMLIBVER,pk_study,  fk_form,fmod,'S' AS formtype,'-' pk_patprot,'-' AS fk_site,'-' AS fk_per,'-' AS EVENT_ID,NVL((SELECT codelst_desc FROM er_codelst WHERE PK_CODELST = aa.FK_CODELST_QUERYTYPE),'-') query_type,(select lf_entrychar   from er_linkedforms lf where aa.fk_form=lf.fk_formlib) as lf_entrychar,form_Status_id,(SELECT codelst_subtyp FROM er_codelst WHERE pk_codelst=Form_Status_id) AS sub_type,");
				sqlBuffer.append("(Select codelst_subtyp from er_codelst cl,er_formlib fl where cl.pk_codelst=fl.form_status and fl.pk_formlib=fk_form) as form_stat_subtyp FROM er_study,(SELECT DISTINCT NVL((SELECT FK_CODELST_QUERYSTATUS FROM ER_FORMQUERYSTATUS WHERE PK_FORMQUERYSTATUS=f.PK_MaxFQSTAT),DECODE(total_queries,0,0,(SELECT pk_codelst  FROM er_codelst  WHERE codelst_type  ='query_status' AND codelst_subtyp  ='closed'))) FK_CODELST_QUERYSTATUS,(SELECT FK_CODELST_QUERYTYPE FROM ER_FORMQUERYSTATUS WHERE PK_FORMQUERYSTATUS= f.pk_maxfqstat) FK_CODELST_QUERYTYPE, total_queries, (SELECT TRUNC(sysdate) - to_date(min(ENTERED_ON)) FROM ER_FORMQUERYSTATUS   WHERE FK_FORMQUERY=f.pk_minquery     ) days_open, PK_STUDYFORMS,FK_FORMLIBVER,f.fk_study,eres.f_getcolorflag(PK_STUDYFORMS,f.fk_study,2) AS setColor,       ");
				sqlBuffer.append(" pk_formlib AS fk_form, form_name,STUDYFORMS_FILLDATE AS Data_Entry_Date,FORM_COMPLETED AS Form_Status_id,f.LAST_MODIFIED_DATE modified_date,f.CREATOR data_entered_by,CASE WHEN ( f.CREATED_ON IS NOT NULL AND f.LAST_MODIFIED_DATE IS NULL) THEN f.CREATED_ON ELSE f.LAST_MODIFIED_DATE END AS fmod FROM   ER_FORMLIB e, (SELECT a.pk_formquery PK_Minquery ,PK_FORMQUERYSTATUS PK_MaxFQSTAT,QUERYMODULE_LINKEDTO,(SELECT COUNT(*) FROM ER_FORMQUERY a WHERE a.FK_QUERYMODULE = PK_STUDYFORMS and querymodule_linkedto=2) total_queries,PK_STUDYFORMS,f.fk_study,fk_formlib,STUDYFORMS_FILLDATE,FORM_COMPLETED,FK_FORMLIBVER,f.LAST_MODIFIED_DATE,f.CREATED_ON,f.CREATOR from ER_STUDYFORMS f,(select a.*,max(b.PK_FORMQUERYSTATUS) PK_FORMQUERYSTATUS from er_formquerystatus b,(select min(PK_FORMQUERY) PK_FORMQUERY, FK_QUERYMODULE,QUERYMODULE_LINKEDTO from er_formquery fq	where (SELECT COUNT(*) FROM er_formquerystatus fqs1 WHERE fqs1.fk_formquery = fq.PK_FORMQUERY AND fqs1.PK_FORMQUERYSTATUS=(SELECT MAX(PK_FORMQUERYSTATUS) FROM er_formquerystatus fqs2 WHERE fqs2.fk_formquery = fqs1.fk_formquery) AND fqs1.fk_codelst_querystatus=(SELECT pk_codelst FROM er_codelst WHERE codelst_type  ='query_status' AND codelst_subtyp  ='closed'))=0 And QUERYMODULE_LINKEDTO=2	group by FK_QUERYMODULE,QUERYMODULE_LINKEDTO) a where a.PK_FORMQUERY=b.FK_FORMQUERY group by PK_FORMQUERY,FK_QUERYMODULE,QUERYMODULE_LINKEDTO) a WHERE a.FK_QUERYMODULE(+) = PK_STUDYFORMS AND a.QUERYMODULE_LINKEDTO(+) =2 AND f.fk_study         IN("+studyIds+") AND f.RECORD_TYPE <> 'D' ) f WHERE pk_formlib = f.fk_formlib AND e.pk_formlib NOT IN (SELECT lf.fk_formlib FROM er_linkedforms lf WHERE e.pk_formlib = lf.fk_formlib AND (lf.RECORD_TYPE = 'D' OR LF_HIDE = 1) AND (SELECT COUNT(1) FROM ER_STUDYFORMS sf2 WHERE sf2.RECORD_TYPE <> 'D' AND sf2.FK_FORMLIB = lf.fk_formlib)< 1)   )aa WHERE pk_study = aa.fk_study AND (pkg_util.f_getStudyRight( ");
				sqlBuffer.append("pkg_studystat.F_GET_USERRIGHTS_FOR_STUDY("+userId+" , pk_study),(SELECT CTRL_SEQ FROM er_ctrltab WHERE CTRL_KEY  = 'study_rights' AND upper(ctrl_value) = 'STUDYFRMACC')) > 0 or study_actualdt is null) and ( 0 < pkg_superuser.F_Is_Superuser("+userId+",PK_STUDY))");
			//Bug id #21071 Ends
				if(!"".equalsIgnoreCase(formIds))
					sqlBuffer.append(" AND fk_form  IN ("+formIds+") ");
				if(!"".equalsIgnoreCase(formStatusIds))
					sqlBuffer.append(" AND Form_Status_id  IN ("+formStatusIds+") ");
				if(!"".equalsIgnoreCase(dataEntryDateFrom) && "".equalsIgnoreCase(dataEntryDateTo))
					sqlBuffer.append(" and data_entry_date between TO_DATE('"+dataEntryDateFrom+"',PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE(PKG_DATEUTIL.f_get_future_null_date_str,PKG_DATEUTIL.F_GET_DATEFORMAT) ");
				if("".equalsIgnoreCase(dataEntryDateFrom) && !"".equalsIgnoreCase(dataEntryDateTo))
					sqlBuffer.append(" and data_entry_date between TO_DATE(PKG_DATEUTIL.f_get_null_date_str,PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE('"+dataEntryDateTo+"',PKG_DATEUTIL.F_GET_DATEFORMAT) ");
				if(!"".equalsIgnoreCase(dataEntryDateFrom) && !"".equalsIgnoreCase(dataEntryDateTo))
					sqlBuffer.append(" and data_entry_date between TO_DATE('"+dataEntryDateFrom+"',PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE('"+dataEntryDateTo+"',PKG_DATEUTIL.F_GET_DATEFORMAT) ");
				if(!"".equalsIgnoreCase(queryCreatorIds))
					sqlBuffer.append(" AND data_entered_by  IN ("+queryCreatorIds+") ");
				if(!"".equalsIgnoreCase(queryStatusIds))
					sqlBuffer.append(" AND FK_CODELST_QUERYSTATUS  IN ("+queryStatusIds+") ");
				if(!"".equalsIgnoreCase(selectedqueryStatuess))
					sqlBuffer.append(" AND days_open  >("+selectedqueryStatuess+") ");
				if(!"".equalsIgnoreCase(selectedTotalRespQuery))
					sqlBuffer.append(" AND total_queries  >("+selectedTotalRespQuery+") ");
				if(sortCol==8)//Form_Name
				{
					sqlOrderBy.append("ORDER BY LOWER(form_name) "+sortDir);
				}
				else if(sortCol==10)//Data_Entry_Date
				{
					sqlOrderBy.append("ORDER BY  TO_DATE(data_entry_date,PKG_DATEUTIL.F_GET_DATEFORMAT) "+sortDir);
				}
				else if(sortCol==11)//Modified_Date
				{
					sqlOrderBy.append("ORDER BY  TO_DATE(modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT) "+sortDir);
				} 
				else{
					sqlOrderBy.append(" ORDER BY "+sortCol+" "+sortDir);		}										
			    //sqlBuffer.append("where  FORM_STATUS IN  (select   cl2.codelst_desc from er_studyteam st  ,er_codelst cl,  er_codelst cl2  where cl.pk_codelst = fk_codelst_tmrole and cl2.codelst_study_role like '%' ||  cl.codelst_subtyp  || '%' and fk_user ="+userId+"and fk_study  IN (" + studyIds +") and  cl2.codelst_type='fillformstat' and  cl2.codelst_hide='N')");
			}
			
			else if("".equalsIgnoreCase(orgIds)){

				filterRight.append(") where (0 < pkg_user.f_chk_studyright_using_pat(FK_PER,PK_STUDY, "+userId+"))") ;
					sqlBuffer.append("Select b.*,rownum rnum from(SELECT eres.f_getcolorflag(aa.pk_patforms,aa.fk_study,4) AS setColor,total_queries,DECODE(FK_CODELST_QUERYSTATUS,(SELECT pk_codelst FROM er_codelst WHERE codelst_type  ='query_status' AND codelst_subtyp  ='closed'),'(No Aged Query)', NVL((SELECT codelst_desc FROM er_codelst WHERE PK_CODELST = FK_CODELST_QUERYSTATUS),'-')) query_status,(CASE  WHEN days_open is null THEN 0 ELSE days_open END) days_open  ,study_number,(SELECT site_name FROM er_site WHERE pk_site = fk_site) AS site_name,PATSTDID,form_name,(SELECT codelst_desc FROM er_codelst WHERE PK_CODELST = Form_Status_id) form_status,TO_CHAR(Data_Entry_Date,PKG_DATEUTIL.F_GET_DATEFORMAT) data_entry_date,TO_CHAR(modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT) modified_date,(SELECT USR_FIRSTNAME||', '||USR_LASTNAME FROM er_user WHERE pk_user=data_entered_by) data_entered_by,(SELECT Name FROM event_assoc WHERE event_id=pk_calender) Calender_name,VISIT_NAME,event_name,TO_CHAR(event_end_date,PKG_DATEUTIL.F_GET_DATEFORMAT) event_end_date,pk_patforms AS pk_form,        FK_FORMLIBVER,pk_study,fk_form,fmod,'P' AS formtype,pk_patprot,fk_site,fk_per,EVENT_ID,NVL((SELECT codelst_desc FROM er_codelst WHERE PK_CODELST = aa.FK_CODELST_QUERYTYPE),'-') query_type,(select lf_entrychar   from er_linkedforms lf where aa.fk_form=lf.fk_formlib) as lf_entrychar,form_Status_id,(SELECT codelst_subtyp FROM er_codelst WHERE pk_codelst=Form_Status_id) AS sub_type,(Select codelst_subtyp from er_codelst cl,er_formlib fl where cl.pk_codelst=fl.form_status and fl.pk_formlib=fk_form) as form_stat_subtyp FROM er_study,(SELECT DISTINCT NVL((SELECT FK_CODELST_QUERYSTATUS FROM ER_FORMQUERYSTATUS WHERE PK_FORMQUERYSTATUS=f.PK_MaxFQSTAT),DECODE(total_queries,0,0,(SELECT pk_codelst  FROM er_codelst  WHERE codelst_type  ='query_status' AND codelst_subtyp  ='closed'))) FK_CODELST_QUERYSTATUS,(SELECT FK_CODELST_QUERYTYPE FROM ER_FORMQUERYSTATUS WHERE PK_FORMQUERYSTATUS= f.pk_maxfqstat) FK_CODELST_QUERYTYPE, total_queries,(SELECT TRUNC(sysdate) - to_date(min(ENTERED_ON)) FROM ER_FORMQUERYSTATUS WHERE FK_FORMQUERY=f.pk_minquery     ) days_open, ");
					sqlBuffer.append("pk_patforms,pk_patprot,f.FK_FORMLIBVER,f.fk_study,f.fk_per,(select NVL(patprot_patstdid,'<I>(Patient removed from study)</I>') from er_patprot where pk_patprot=  (select max(pk_patprot) from er_patprot where fk_study=f.fk_study and fk_per= f.fk_per)) PATSTDID,FK_SITE_ENROLLING AS fk_site,k.fk_protocol AS pk_calender, j.EVENT_ID  AS EVENT_ID,j.NAME  AS event_name, h.ACTUAL_SCHDATE AS event_end_date, h.fk_visit  AS VISIT_ID, k.VISIT_NAME  AS VISIT_NAME, ");
					sqlBuffer.append("pk_formlib AS fk_form, form_name, PATFORMS_FILLDATE AS Data_Entry_Date,FORM_COMPLETED AS Form_Status_id, f.LAST_MODIFIED_DATE modified_date,f.CREATOR data_entered_by,CASE WHEN (f.CREATED_ON IS NOT NULL AND f.LAST_MODIFIED_DATE IS NULL) THEN f.CREATED_ON ELSE f.LAST_MODIFIED_DATE END AS fmod FROM ER_FORMLIB e, (SELECT a.pk_formquery PK_Minquery ,PK_FORMQUERYSTATUS PK_MaxFQSTAT,QUERYMODULE_LINKEDTO,(SELECT COUNT(*) FROM ER_FORMQUERY a WHERE a.FK_QUERYMODULE = f.PK_PATFORMS and querymodule_linkedto=4) total_queries,pk_patforms, pk_patprot,g.fk_study,fk_formlib,f.fk_per,PATPROT_PATSTDID,FK_SITE_ENROLLING,PATFORMS_FILLDATE,FORM_COMPLETED,FK_FORMLIBVER, f.LAST_MODIFIED_DATE, f.CREATED_ON,  f.CREATOR,FK_SCH_EVENTS1  from ER_PATFORMS f,  ER_PATPROT g,(select a.*,max(b.PK_FORMQUERYSTATUS) PK_FORMQUERYSTATUS from er_formquerystatus b,(select min(PK_FORMQUERY) PK_FORMQUERY, FK_QUERYMODULE,QUERYMODULE_LINKEDTO from er_formquery fq where (SELECT COUNT(*) FROM er_formquerystatus fqs1 WHERE fqs1.fk_formquery = fq.PK_FORMQUERY AND fqs1.PK_FORMQUERYSTATUS=(SELECT MAX(PK_FORMQUERYSTATUS) FROM er_formquerystatus fqs2 WHERE fqs2.fk_formquery = fqs1.fk_formquery) AND fqs1.fk_codelst_querystatus=(SELECT pk_codelst FROM er_codelst WHERE codelst_type  ='query_status' AND codelst_subtyp  ='closed'))=0 And QUERYMODULE_LINKEDTO=4 group by FK_QUERYMODULE,QUERYMODULE_LINKEDTO) a where a.PK_FORMQUERY=b.FK_FORMQUERY group by PK_FORMQUERY,FK_QUERYMODULE,QUERYMODULE_LINKEDTO) a  WHERE g.pk_patprot   = f.fk_patprot  AND f.fk_per       = g.fk_per  AND a.FK_QUERYMODULE(+) = PK_PATFORMS  AND a.QUERYMODULE_LINKEDTO(+) =4  AND g.fk_study         IN ("+studyIds+")  AND f.RECORD_TYPE <> 'D'  AND f.FK_PER            > 0  ) f,esch.sch_events1 h, esch.event_assoc j,esch.SCH_PROTOCOL_VISIT k  WHERE pk_formlib = f.fk_formlib AND e.pk_formlib NOT IN (SELECT lf.fk_formlib  FROM er_linkedforms lf  WHERE e.pk_formlib  = lf.fk_formlib AND (lf.RECORD_TYPE = 'D' OR LF_HIDE = 1) AND (SELECT COUNT(1) FROM er_patforms pf2  WHERE pf2.RECORD_TYPE <> 'D' AND pf2.FK_FORMLIB = lf.fk_formlib AND pf2.fk_patprot = f.pk_patprot) < 1 ) AND e.pk_formlib NOT IN (SELECT LF.FK_FORMLIB FROM ER_SETTINGS, ER_LINKEDFORMS lf WHERE SETTINGS_KEYWORD = 'FORM_HIDE' AND SETTINGS_MODNAME = 3 AND SETTINGS_MODNUM = f.fk_study AND SETTINGS_VALUE = lf.pk_lf AND (SELECT COUNT(1) FROM er_patforms pf2 WHERE pf2.RECORD_TYPE <> 'D' AND pf2.FK_FORMLIB = lf.fk_formlib AND pf2.fk_patprot = f.pk_patprot) < 1) AND j.event_id(+)= h.fk_assoc AND LPAD(f.FK_SCH_EVENTS1,10,0) = h.EVENT_ID(+) AND h.fk_visit                  = k.PK_PROTOCOL_VISIT(+) AND (pkg_util.f_getStudyRight(pkg_studystat.F_GET_USERRIGHTS_FOR_STUDY("+userId+" , f.fk_study),(SELECT CTRL_SEQ FROM er_ctrltab WHERE CTRL_KEY = 'study_rights' AND upper(ctrl_value) = 'STUDYMPAT')) > 0) and (0 < pkg_user.f_chk_studyright_using_pat(f.FK_PER,f.FK_STUDY, "+userId+"))) aa WHERE pk_study = aa.fk_study   ");
				
					if(!"".equalsIgnoreCase(formIds))
						sqlBuffer.append(" AND fk_form  IN ("+formIds+") ");
					if(!"".equalsIgnoreCase(formStatusIds))
						sqlBuffer.append(" AND Form_Status_id  IN ("+formStatusIds+") ");
					if(!"".equalsIgnoreCase(dataEntryDateFrom) && "".equalsIgnoreCase(dataEntryDateTo))
						sqlBuffer.append(" and data_entry_date between TO_DATE('"+dataEntryDateFrom+"',PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE(PKG_DATEUTIL.f_get_future_null_date_str,PKG_DATEUTIL.F_GET_DATEFORMAT) ");
					if("".equalsIgnoreCase(dataEntryDateFrom) && !"".equalsIgnoreCase(dataEntryDateTo))
						sqlBuffer.append(" and data_entry_date between TO_DATE(PKG_DATEUTIL.f_get_null_date_str,PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE('"+dataEntryDateTo+"',PKG_DATEUTIL.F_GET_DATEFORMAT) ");
					if(!"".equalsIgnoreCase(dataEntryDateFrom) && !"".equalsIgnoreCase(dataEntryDateTo))
						sqlBuffer.append(" and data_entry_date between TO_DATE('"+dataEntryDateFrom+"',PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE('"+dataEntryDateTo+"',PKG_DATEUTIL.F_GET_DATEFORMAT) ");
					if(!"".equalsIgnoreCase(patientIds))
						sqlBuffer.append(" AND fk_per  IN ("+patientIds+") ");
					if(!"".equalsIgnoreCase(patientStdIds))
						sqlBuffer.append(" AND fk_per   IN ("+patientStdIds+") ");
					if(!"".equalsIgnoreCase(patientCalIds))
						sqlBuffer.append(" AND pk_calender  IN ("+patientCalIds+") ");
					if(!"".equalsIgnoreCase(patientVisitIds))
						sqlBuffer.append(" AND VISIT_ID  IN ("+patientVisitIds+") ");
					if(!"".equalsIgnoreCase(patientEventIds))
						sqlBuffer.append(" AND EVENT_ID  IN ("+patientEventIds+") ");
					if(!"".equalsIgnoreCase(eventDate))
						sqlBuffer.append(" AND event_end_date = TO_DATE('"+eventDate+"',PKG_DATEUTIL.F_GET_DATEFORMAT) ");
					if(!"".equalsIgnoreCase(queryCreatorIds))
						sqlBuffer.append(" AND data_entered_by  IN ("+queryCreatorIds+") ");
					if(!"".equalsIgnoreCase(queryStatusIds))
						sqlBuffer.append(" AND FK_CODELST_QUERYSTATUS  IN ("+queryStatusIds+") ");
					if(!"".equalsIgnoreCase(selectedTotalRespQuery))
						sqlBuffer.append(" AND total_queries  >("+selectedTotalRespQuery+") ");
					if(!"".equalsIgnoreCase(selectedqueryStatuess))
						sqlBuffer.append(" AND days_open  >("+selectedqueryStatuess+") ");
					if(sortCol==6)//Enrolling site
					{
						sqlOrderBy.append("ORDER BY LOWER(site_name) "+sortDir);
					}
					else if(sortCol==7)//PATSTDID
					{
						sqlOrderBy.append("ORDER BY LOWER(patstdid) "+sortDir);
					}
					else if(sortCol==8)//Form_Name
					{
						sqlOrderBy.append("ORDER BY LOWER(form_name) "+sortDir);
					}
					else if(sortCol==10)//Data_Entry_Date
					{
						sqlOrderBy.append("ORDER BY  TO_DATE(data_entry_date,PKG_DATEUTIL.F_GET_DATEFORMAT) "+sortDir);
					}
					else if(sortCol==11)//Modified_Date
					{
						sqlOrderBy.append("ORDER BY  TO_DATE(modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT) "+sortDir);
					} 
					else if(sortCol==13)//Calendar
					{
						sqlOrderBy.append("ORDER BY  LOWER(calender_name) "+sortDir);
					}
					else if(sortCol==14)//Visit
					{
						sqlOrderBy.append("ORDER BY  LOWER(visit_name) "+sortDir);
					}
					else if(sortCol==15)//Event
					{
						sqlOrderBy.append("ORDER BY  LOWER(event_name) "+sortDir);
					}
					else if(sortCol==16)//EventDate
					{
						sqlOrderBy.append("ORDER BY  TO_DATE(event_end_date,PKG_DATEUTIL.F_GET_DATEFORMAT) "+sortDir);
					}
					else{
						sqlOrderBy.append(" ORDER BY "+sortCol+" "+sortDir);}
				
			}
			
			
			else{
				filterRight.append(") where (0 < pkg_user.f_chk_studyright_using_pat(FK_PER,PK_STUDY, "+userId+"))") ;
			//Bug id #21071 fixed by Rashi Starts
				sqlBuffer.append("Select b.*,rownum rnum from(SELECT eres.f_getcolorflag(aa.pk_patforms,aa.fk_study,4) AS setColor,total_queries,DECODE(FK_CODELST_QUERYSTATUS,(SELECT pk_codelst FROM er_codelst WHERE codelst_type  ='query_status' AND codelst_subtyp  ='closed'),'(No Aged Query)', NVL((SELECT codelst_desc FROM er_codelst WHERE PK_CODELST = FK_CODELST_QUERYSTATUS),'-')) query_status,(CASE  WHEN days_open is null THEN 0 ELSE days_open END) days_open  ,study_number,(SELECT site_name FROM er_site WHERE pk_site = fk_site) AS site_name,PATSTDID,form_name,(SELECT codelst_desc FROM er_codelst WHERE PK_CODELST = Form_Status_id) form_status,TO_CHAR(Data_Entry_Date,PKG_DATEUTIL.F_GET_DATEFORMAT) data_entry_date,TO_CHAR(modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT) modified_date,(SELECT USR_FIRSTNAME||', '||USR_LASTNAME FROM er_user WHERE pk_user=data_entered_by) data_entered_by,(SELECT Name FROM event_assoc WHERE event_id=pk_calender) Calender_name,VISIT_NAME,event_name,TO_CHAR(event_end_date,PKG_DATEUTIL.F_GET_DATEFORMAT) event_end_date,pk_patforms AS pk_form,        FK_FORMLIBVER,pk_study,fk_form,fmod,'P' AS formtype,pk_patprot,fk_site,fk_per,EVENT_ID,NVL((SELECT codelst_desc FROM er_codelst WHERE PK_CODELST = aa.FK_CODELST_QUERYTYPE),'-') query_type,(select lf_entrychar   from er_linkedforms lf where aa.fk_form=lf.fk_formlib) as lf_entrychar,form_Status_id,(SELECT codelst_subtyp FROM er_codelst WHERE pk_codelst=Form_Status_id) AS sub_type,(Select codelst_subtyp from er_codelst cl,er_formlib fl where cl.pk_codelst=fl.form_status and fl.pk_formlib=fk_form) as form_stat_subtyp FROM er_study,(SELECT DISTINCT NVL((SELECT FK_CODELST_QUERYSTATUS FROM ER_FORMQUERYSTATUS WHERE PK_FORMQUERYSTATUS=f.PK_MaxFQSTAT),DECODE(total_queries,0,0,(SELECT pk_codelst  FROM er_codelst  WHERE codelst_type  ='query_status' AND codelst_subtyp  ='closed'))) FK_CODELST_QUERYSTATUS,(SELECT FK_CODELST_QUERYTYPE FROM ER_FORMQUERYSTATUS WHERE PK_FORMQUERYSTATUS= f.pk_maxfqstat) FK_CODELST_QUERYTYPE,total_queries, (SELECT TRUNC(sysdate) - to_date(min(ENTERED_ON))   FROM ER_FORMQUERYSTATUS   WHERE FK_FORMQUERY=f.pk_minquery     ) days_open, ");
				sqlBuffer.append("pk_patforms,pk_patprot,f.FK_FORMLIBVER,f.fk_study,f.fk_per,(select NVL(patprot_patstdid,'<I>(Patient removed from study)</I>') from er_patprot where pk_patprot=  (select max(pk_patprot) from er_patprot where fk_study=f.fk_study and fk_per= f.fk_per)) PATSTDID,FK_SITE_ENROLLING AS fk_site,k.fk_protocol AS pk_calender,j.EVENT_ID  AS EVENT_ID, j.NAME  AS event_name, h.ACTUAL_SCHDATE AS event_end_date,h.fk_visit  AS VISIT_ID,k.VISIT_NAME  AS VISIT_NAME, ");
				sqlBuffer.append("pk_formlib AS fk_form, form_name, PATFORMS_FILLDATE AS Data_Entry_Date,FORM_COMPLETED AS Form_Status_id, f.LAST_MODIFIED_DATE modified_date,f.CREATOR data_entered_by,CASE WHEN (f.CREATED_ON IS NOT NULL AND f.LAST_MODIFIED_DATE IS NULL) THEN f.CREATED_ON ELSE f.LAST_MODIFIED_DATE END AS fmod FROM ER_FORMLIB e, (SELECT a.pk_formquery PK_Minquery ,PK_FORMQUERYSTATUS PK_MaxFQSTAT,QUERYMODULE_LINKEDTO,(SELECT COUNT(*) FROM ER_FORMQUERY a WHERE a.FK_QUERYMODULE = f.PK_PATFORMS and querymodule_linkedto=4) total_queries,pk_patforms, pk_patprot,g.fk_study,fk_formlib,f.fk_per,PATPROT_PATSTDID,FK_SITE_ENROLLING,PATFORMS_FILLDATE,FORM_COMPLETED,FK_FORMLIBVER, f.LAST_MODIFIED_DATE, f.CREATED_ON,  f.CREATOR,FK_SCH_EVENTS1  from ER_PATFORMS f,  ER_PATPROT g,(select a.*,max(b.PK_FORMQUERYSTATUS) PK_FORMQUERYSTATUS from er_formquerystatus b,(select min(PK_FORMQUERY) PK_FORMQUERY, FK_QUERYMODULE,QUERYMODULE_LINKEDTO from er_formquery fq	where (SELECT COUNT(*) FROM er_formquerystatus fqs1 WHERE fqs1.fk_formquery = fq.PK_FORMQUERY AND fqs1.PK_FORMQUERYSTATUS=(SELECT MAX(PK_FORMQUERYSTATUS) FROM er_formquerystatus fqs2 WHERE fqs2.fk_formquery = fqs1.fk_formquery) AND fqs1.fk_codelst_querystatus=(SELECT pk_codelst FROM er_codelst WHERE codelst_type  ='query_status' AND codelst_subtyp  ='closed'))=0 And QUERYMODULE_LINKEDTO=4	group by FK_QUERYMODULE,QUERYMODULE_LINKEDTO) a where a.PK_FORMQUERY=b.FK_FORMQUERY group by PK_FORMQUERY,FK_QUERYMODULE,QUERYMODULE_LINKEDTO) a  WHERE g.pk_patprot   = f.fk_patprot  AND f.fk_per       = g.fk_per  AND a.FK_QUERYMODULE(+) = PK_PATFORMS  AND a.QUERYMODULE_LINKEDTO(+) =4  AND g.fk_study         IN ("+studyIds+")  AND f.RECORD_TYPE <> 'D'  AND f.FK_PER            > 0  ) f,esch.sch_events1 h, esch.event_assoc j,esch.SCH_PROTOCOL_VISIT k WHERE pk_formlib = f.fk_formlib AND e.pk_formlib NOT IN (SELECT lf.fk_formlib  FROM er_linkedforms lf  WHERE e.pk_formlib  = lf.fk_formlib AND (lf.RECORD_TYPE = 'D' OR LF_HIDE = 1) AND (SELECT COUNT(1) FROM er_patforms pf2  WHERE pf2.RECORD_TYPE <> 'D' AND pf2.FK_FORMLIB = lf.fk_formlib AND pf2.fk_patprot = f.pk_patprot) < 1 ) AND e.pk_formlib NOT IN (SELECT LF.FK_FORMLIB FROM ER_SETTINGS, ER_LINKEDFORMS lf WHERE SETTINGS_KEYWORD = 'FORM_HIDE' AND SETTINGS_MODNAME = 3 AND SETTINGS_MODNUM = f.fk_study AND SETTINGS_VALUE = lf.pk_lf AND (SELECT COUNT(1) FROM er_patforms pf2 WHERE pf2.RECORD_TYPE <> 'D' AND pf2.FK_FORMLIB = lf.fk_formlib AND pf2.fk_patprot = f.pk_patprot) < 1) AND j.event_id(+)= h.fk_assoc AND LPAD(f.FK_SCH_EVENTS1,10,0) = h.EVENT_ID(+) AND h.fk_visit = k.PK_PROTOCOL_VISIT(+) AND (pkg_util.f_getStudyRight(pkg_studystat.F_GET_USERRIGHTS_FOR_STUDY("+userId+" , f.fk_study),(SELECT CTRL_SEQ FROM er_ctrltab WHERE CTRL_KEY = 'study_rights' AND upper(ctrl_value) = 'STUDYMPAT')) > 0)  and (0 < pkg_user.f_chk_studyright_using_pat(f.FK_PER,f.FK_STUDY, "+userId+"))) aa WHERE pk_study = aa.fk_study  AND fk_site IN ("+orgIds+") ");
			//Bug id #21071 Ends
				if(!"".equalsIgnoreCase(formIds))
					sqlBuffer.append(" AND fk_form  IN ("+formIds+") ");
				if(!"".equalsIgnoreCase(formStatusIds))
					sqlBuffer.append(" AND Form_Status_id  IN ("+formStatusIds+") ");
				if(!"".equalsIgnoreCase(dataEntryDateFrom) && "".equalsIgnoreCase(dataEntryDateTo))
					sqlBuffer.append(" and data_entry_date between TO_DATE('"+dataEntryDateFrom+"',PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE(PKG_DATEUTIL.f_get_future_null_date_str,PKG_DATEUTIL.F_GET_DATEFORMAT) ");
				if("".equalsIgnoreCase(dataEntryDateFrom) && !"".equalsIgnoreCase(dataEntryDateTo))
					sqlBuffer.append(" and data_entry_date between TO_DATE(PKG_DATEUTIL.f_get_null_date_str,PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE('"+dataEntryDateTo+"',PKG_DATEUTIL.F_GET_DATEFORMAT) ");
				if(!"".equalsIgnoreCase(dataEntryDateFrom) && !"".equalsIgnoreCase(dataEntryDateTo))
					sqlBuffer.append(" and data_entry_date between TO_DATE('"+dataEntryDateFrom+"',PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE('"+dataEntryDateTo+"',PKG_DATEUTIL.F_GET_DATEFORMAT) ");
				if(!"".equalsIgnoreCase(patientIds))
					sqlBuffer.append(" AND fk_per  IN ("+patientIds+") ");
				if(!"".equalsIgnoreCase(patientStdIds))
					sqlBuffer.append(" AND fk_per   IN ("+patientStdIds+") ");
				if(!"".equalsIgnoreCase(patientCalIds))
					sqlBuffer.append(" AND pk_calender  IN ("+patientCalIds+") ");
				if(!"".equalsIgnoreCase(patientVisitIds))
					sqlBuffer.append(" AND VISIT_ID  IN ("+patientVisitIds+") ");
				if(!"".equalsIgnoreCase(patientEventIds))
					sqlBuffer.append(" AND EVENT_ID  IN ("+patientEventIds+") ");
				if(!"".equalsIgnoreCase(eventDate))
					sqlBuffer.append(" AND event_end_date = TO_DATE('"+eventDate+"',PKG_DATEUTIL.F_GET_DATEFORMAT) ");
				if(!"".equalsIgnoreCase(queryCreatorIds))
					sqlBuffer.append(" AND data_entered_by  IN ("+queryCreatorIds+") ");
				if(!"".equalsIgnoreCase(queryStatusIds))
					sqlBuffer.append(" AND FK_CODELST_QUERYSTATUS  IN ("+queryStatusIds+") ");
				if(!"".equalsIgnoreCase(selectedTotalRespQuery))
					sqlBuffer.append(" AND total_queries  >("+selectedTotalRespQuery+") ");
				if(!"".equalsIgnoreCase(selectedqueryStatuess))
					sqlBuffer.append(" AND days_open  >("+selectedqueryStatuess+") ");
				if(sortCol==7)//PATSTDID
				{
					sqlOrderBy.append("ORDER BY LOWER(patstdid) "+sortDir);
				}
				else if(sortCol==8)//Form_Name
				{
					sqlOrderBy.append("ORDER BY LOWER(form_name) "+sortDir);
				}
				else if(sortCol==10)//Data_Entry_Date
				{
					sqlOrderBy.append("ORDER BY  TO_DATE(data_entry_date,PKG_DATEUTIL.F_GET_DATEFORMAT) "+sortDir);
				}
				else if(sortCol==11)//Modified_Date
				{
					sqlOrderBy.append("ORDER BY  TO_DATE(modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT) "+sortDir);
				}
				else if(sortCol==13)//Calendar
				{
					sqlOrderBy.append("ORDER BY  LOWER(calender_name) "+sortDir);
				}
				else if(sortCol==14)//Visit
				{
					sqlOrderBy.append("ORDER BY  LOWER(visit_name) "+sortDir);
				}
				else if(sortCol==15)//Event
				{
					sqlOrderBy.append("ORDER BY  LOWER(event_name) "+sortDir);
				}
				else if(sortCol==16)//EventDate
				{
					sqlOrderBy.append("ORDER BY  TO_DATE(event_end_date,PKG_DATEUTIL.F_GET_DATEFORMAT) "+sortDir);
				}
				else{
					sqlOrderBy.append(" ORDER BY "+sortCol+" "+sortDir);}
			}
			
		/*else if(!"".equalsIgnoreCase(orgIds)){
			//Bug id #21071 fixed by Rashi Starts
			sqlBuffer.append("Select * from(SELECT total_queries,NVL((SELECT codelst_desc  FROM er_codelst WHERE PK_CODELST = c.FK_CODELST_QUERYSTATUS),'-') query_status,days_open,0 AS target_days,'-' AS study_number,(SELECT site_name FROM er_site WHERE pk_site = c.fk_site) AS site_name, '-' AS PATSTDID,form_name,(SELECT codelst_desc FROM er_codelst WHERE PK_CODELST = Form_Status_id) form_status,0 AS form_target_days ,  TO_CHAR(c.PATFORMS_FILLDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) AS Data_Entry_Date,TO_CHAR(c.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) modified_date,(SELECT USR_FIRSTNAME||', '||USR_LASTNAME FROM er_user WHERE pk_user=c.CREATOR) data_entered_by,'-' AS Calender_name,'-' AS VISIT_NAME,'-' AS event_name,'-' AS event_end_date,pk_patforms AS pk_form,NVL((SELECT codelst_desc FROM er_codelst WHERE PK_CODELST = c.FK_CODELST_QUERYTYPE),'-') query_type,'P' AS formtype,''  AS pk_patprot,FK_FORMLIBVER,'-' AS fk_study, ");
			sqlBuffer.append("fk_per,FK_SITE,'-' AS pk_calender,'-' AS EVENT_ID,'-' AS VISIT_NO,formcount,pk_formlib AS fk_form,fmod,form_Status_id,(SELECT codelst_subtyp FROM er_codelst WHERE pk_codelst=form_status) AS sub_type from (SELECT DISTINCT (SELECT FK_CODELST_QUERYSTATUS FROM ER_FORMQUERYSTATUS WHERE PK_FORMQUERYSTATUS= (SELECT MIN(PK_FORMQUERYSTATUS) FROM ER_FORMQUERYSTATUS b, ER_FORMQUERY a WHERE b.fk_formquery = a.pk_formquery AND a.FK_QUERYMODULE = pk_patforms)) FK_CODELST_QUERYSTATUS,(SELECT FK_CODELST_QUERYTYPE FROM ER_FORMQUERYSTATUS WHERE PK_FORMQUERYSTATUS= (SELECT MIN(PK_FORMQUERYSTATUS) FROM ER_FORMQUERYSTATUS b, ER_FORMQUERY a WHERE b.fk_formquery = a.pk_formquery  AND a.FK_QUERYMODULE = pk_patforms ) ) FK_CODELST_QUERYTYPE,(SELECT COUNT(*)FROM ER_FORMQUERY a WHERE a.FK_QUERYMODULE = pk_patforms) total_queries,(SELECT TRUNC(sysdate) - to_date(ENTERED_ON) FROM ER_FORMQUERYSTATUS  WHERE PK_FORMQUERYSTATUS= (SELECT MIN(PK_FORMQUERYSTATUS)  FROM ER_FORMQUERYSTATUS b, ER_FORMQUERY a WHERE b.fk_formquery = a.pk_formquery AND a.FK_QUERYMODULE = pk_patforms)) days_open,form_name,FORM_COMPLETED,PATFORMS_FILLDATE,f.LAST_MODIFIED_DATE, ");
			sqlBuffer.append("f.CREATOR,pk_patforms,FK_FORMLIBVER,f.fk_per,FK_SITE,(SELECT COUNT(*) FROM ER_PATFORMS pf WHERE pk_formlib = pf.fk_formlib AND pf.fk_per = f.fk_per) formcount, pk_formlib, CASE WHEN (f.CREATED_ON IS NOT NULL AND f.LAST_MODIFIED_DATE IS NULL) THEN f.CREATED_ON  ELSE f.LAST_MODIFIED_DATE END AS fmod FROM ER_FORMLIB e, ER_PATFORMS f,ER_per g WHERE g.fk_account = "+accountId+" AND pk_formlib = f.fk_formlib AND e.pk_formlib NOT IN (SELECT lf.fk_formlib FROM er_linkedforms lf WHERE e.pk_formlib  = lf.fk_formlib AND (lf.RECORD_TYPE = 'D' OR LF_HIDE = 1) AND (SELECT COUNT(1)  FROM er_patforms pf2  WHERE pf2.RECORD_TYPE <> 'D' AND pf2.FK_FORMLIB = lf.fk_formlib AND pf2.fk_per = g.pk_per) < 1) AND f.fk_per = g.pk_per AND f.fk_patprot IS NULL AND fk_site IN ("+orgIds+") ");
			sqlBuffer.append("AND f.RECORD_TYPE <> 'D') c ");
			//Bug id #21071 Ends
			if(!"".equalsIgnoreCase(formIds))
				sqlBuffer.append(" AND pk_formlib  IN ("+formIds+") ");
			if(!"".equalsIgnoreCase(formStatusIds))
				sqlBuffer.append(" AND FORM_COMPLETED  IN ("+formStatusIds+") ");
			if(!"".equalsIgnoreCase(dataEntryDateFrom) && "".equalsIgnoreCase(dataEntryDateTo))
				sqlBuffer.append(" and PATFORMS_FILLDATE between TO_DATE('"+dataEntryDateFrom+"',PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE(PKG_DATEUTIL.f_get_future_null_date_str,PKG_DATEUTIL.F_GET_DATEFORMAT) ");
			if("".equalsIgnoreCase(dataEntryDateFrom) && !"".equalsIgnoreCase(dataEntryDateTo))
				sqlBuffer.append(" and PATFORMS_FILLDATE between TO_DATE(PKG_DATEUTIL.f_get_null_date_str,PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE('"+dataEntryDateTo+"',PKG_DATEUTIL.F_GET_DATEFORMAT) ");
			if(!"".equalsIgnoreCase(dataEntryDateFrom) && !"".equalsIgnoreCase(dataEntryDateTo))
				sqlBuffer.append(" and PATFORMS_FILLDATE between TO_DATE('"+dataEntryDateFrom+"',PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE('"+dataEntryDateTo+"',PKG_DATEUTIL.F_GET_DATEFORMAT) ");
			if(!"".equalsIgnoreCase(patientIds))
				sqlBuffer.append(" AND fk_per  IN ("+patientIds+") ");
			if(!"".equalsIgnoreCase(queryCreatorIds))
				sqlBuffer.append(" AND CREATOR  IN ("+queryCreatorIds+") ");
			if(!"".equalsIgnoreCase(queryStatusIds))
				sqlBuffer.append(" AND FK_CODELST_QUERYSTATUS  IN ("+queryStatusIds+") ");
			if(!"".equalsIgnoreCase(selectedTotalRespQuery))
				sqlBuffer.append(" AND total_queries  >("+selectedTotalRespQuery+") ");
			if(!"".equalsIgnoreCase(selectedqueryStatuess))
				sqlBuffer.append(" AND days_open  >("+selectedqueryStatuess+") ");
			
			sqlBuffer.append(" ORDER BY "+sortCol+" "+sortDir+" ) ");
		}
	else{
		sqlBuffer.append("Select * from(Select total_queries,NVL((SELECT codelst_desc  FROM er_codelst WHERE PK_CODELST = c.FK_CODELST_QUERYSTATUS),'-') query_status,NVL((SELECT codelst_desc FROM er_codelst WHERE PK_CODELST = c.FK_CODELST_QUERYTYPE),'-') query_type,days_open,0   AS target_days, '-' AS pk_study,'-' AS study_number ,'-' AS site_name, '-' AS PATSTDID,form_name,(SELECT codelst_desc FROM er_codelst WHERE PK_CODELST = c.Form_Status_id) form_status,0 AS form_target_days,TO_CHAR(c.ACCTFORMS_FILLDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) AS Data_Entry_Date, TO_CHAR(c.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) modified_date,(SELECT USR_FIRSTNAME||', '||USR_LASTNAME FROM er_user WHERE pk_user=c.CREATOR) data_entered_by,'-' AS Calender_name,'-' AS VISIT_NAME,'-' AS event_name,'-' AS event_end_date,pk_form,formtype,FK_FORMLIBVER,FK_ACCOUNT,pk_formlib AS fk_form,'-' AS pk_patprot,'-' AS fk_site,'-' AS fk_per,'-' AS EVENT_ID,fmod,(SELECT MIN (pk_formquery) FROM er_formquery WHERE fk_querymodule=c.PK_form ) AS pk_formquery,(SELECT fld_name  FROM er_fldlib WHERE pk_field=(SELECT fk_field FROM er_formquery WHERE pk_formquery=(SELECT MIN (pk_formquery) FROM er_formquery WHERE fk_querymodule=c.PK_form ) )) AS fld_name, (SELECT lf_entrychar FROM er_linkedforms lf WHERE c.pk_formlib=lf.fk_formlib ) AS lf_entrychar,form_Status_id,(SELECT codelst_subtyp FROM er_codelst WHERE pk_codelst=form_status) AS sub_type from (SELECT DISTINCT (Select FK_CODELST_QUERYSTATUS from ER_FORMQUERYSTATUS where PK_FORMQUERYSTATUS=(select min(PK_FORMQUERYSTATUS) ");
		sqlBuffer.append("from ER_FORMQUERYSTATUS b,ER_FORMQUERY a where b.fk_formquery = a.pk_formquery AND a.FK_QUERYMODULE = PK_ACCTFORMS)) FK_CODELST_QUERYSTATUS,(SELECT FK_CODELST_QUERYTYPE FROM ER_FORMQUERYSTATUS WHERE PK_FORMQUERYSTATUS= (SELECT MIN(PK_FORMQUERYSTATUS) FROM ER_FORMQUERYSTATUS b, ER_FORMQUERY a WHERE b.fk_formquery = a.pk_formquery  AND a.FK_QUERYMODULE = PK_ACCTFORMS ) ) FK_CODELST_QUERYTYPE,(SELECT count(*)FROM ER_FORMQUERY a WHERE a.FK_QUERYMODULE = PK_ACCTFORMS) total_queries,(SELECT trunc(sysdate) - to_date(ENTERED_ON) FROM ER_FORMQUERYSTATUS WHERE PK_FORMQUERYSTATUS= (SELECT MIN(PK_FORMQUERYSTATUS) FROM ER_FORMQUERYSTATUS b,ER_FORMQUERY a WHERE b.fk_formquery = a.pk_formquery AND a.FK_QUERYMODULE = PK_ACCTFORMS)) days_open,form_name, ACCTFORMS_FILLDATE, f.LAST_MODIFIED_DATE, f.CREATOR, PK_ACCTFORMS AS pk_form, 'A' AS formtype, FORM_COMPLETED, FK_FORMLIBVER, f.FK_ACCOUNT, pk_formlib, CASE  WHEN (f.CREATED_ON IS NOT NULL AND f.LAST_MODIFIED_DATE IS NULL) THEN f.CREATED_ON  ELSE f.LAST_MODIFIED_DATE END AS fmod FROM ER_FORMLIB e,ER_ACCTFORMS f WHERE f.fk_account ="+accountId+" AND pk_formlib = f.fk_formlib AND f.RECORD_TYPE <> 'D' AND e.pk_formlib NOT IN (SELECT lf.fk_formlib FROM er_linkedforms lf WHERE e.pk_formlib = lf.fk_formlib AND (lf.RECORD_TYPE = 'D' OR LF_HIDE = 1))) c where 1 = 1  ");
		if(!"".equalsIgnoreCase(formIds))
			sqlBuffer.append(" AND pk_formlib  IN ("+formIds+") ");
		if(!"".equalsIgnoreCase(formStatusIds))
			sqlBuffer.append(" AND FORM_COMPLETED  IN ("+formStatusIds+") ");
		if(!"".equalsIgnoreCase(dataEntryDateFrom) && "".equalsIgnoreCase(dataEntryDateTo))
			sqlBuffer.append(" and ACCTFORMS_FILLDATE between TO_DATE('"+dataEntryDateFrom+"',PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE(PKG_DATEUTIL.f_get_future_null_date_str,PKG_DATEUTIL.F_GET_DATEFORMAT) ");
		if("".equalsIgnoreCase(dataEntryDateFrom) && !"".equalsIgnoreCase(dataEntryDateTo))
			sqlBuffer.append(" and ACCTFORMS_FILLDATE between TO_DATE(PKG_DATEUTIL.f_get_null_date_str,PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE('"+dataEntryDateTo+"',PKG_DATEUTIL.F_GET_DATEFORMAT) ");
		if(!"".equalsIgnoreCase(dataEntryDateFrom) && !"".equalsIgnoreCase(dataEntryDateTo))
			sqlBuffer.append(" and ACCTFORMS_FILLDATE between TO_DATE('"+dataEntryDateFrom+"',PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE('"+dataEntryDateTo+"',PKG_DATEUTIL.F_GET_DATEFORMAT) ");
		if(!"".equalsIgnoreCase(queryCreatorIds))
			sqlBuffer.append(" AND CREATOR  IN ("+queryCreatorIds+") ");
		if(!"".equalsIgnoreCase(queryStatusIds))
			sqlBuffer.append(" AND FK_CODELST_QUERYSTATUS  IN ("+queryStatusIds+") ");
		if(!"".equalsIgnoreCase(selectedTotalRespQuery))
			sqlBuffer.append(" AND total_queries  >("+selectedTotalRespQuery+") ");
		if(!"".equalsIgnoreCase(selectedqueryStatuess))
			sqlBuffer.append(" AND days_open  >("+selectedqueryStatuess+") ");
		
		sqlBuffer.append(" ORDER BY "+sortCol+" "+sortDir+" ) ");
		}*/
		
		boolean andFlag =false;
		String whereClause="";
		String searchClause="";
		String searchingSql="(LOWER(QUERY_STATUS) like LOWER('%"+sSearch+"%') or LOWER(days_open) like LOWER('%"+sSearch+"%') or LOWER(site_name) like LOWER('%"+sSearch+"%')"
				+ " or LOWER(study_number) like LOWER('%"+sSearch+"%') or LOWER(PATSTDID) like LOWER('%"+sSearch+"%') or LOWER(form_name) like LOWER('%"+sSearch+"%')"
						+ "or LOWER(form_status) like LOWER('%"+sSearch+"%') or LOWER(data_entry_date) like LOWER('%"+sSearch+"%') or LOWER(modified_date) like LOWER('%"+sSearch+"%') "
								+ "or LOWER(data_entered_by) like LOWER('%"+sSearch+"%') or LOWER(Calender_name) like LOWER('%"+sSearch+"%')"
										+ " or LOWER(VISIT_NAME) like LOWER('%"+sSearch+"%')  or LOWER(event_name) like LOWER('%"+sSearch+"%')  or LOWER(event_end_date) like LOWER('%"+sSearch+"%') or lower(pk_form) like LOWER('%"+sSearch+"%'))";
		if(null != qryStatusRed && !qryStatusRed.equals("")){
			whereClause+="  setcolor='red' or";
			andFlag=true;
		}
		if(null != qryStatusOrange && !qryStatusOrange.equals("")){
			whereClause+=" setcolor='orange' or";
			andFlag=true;
		}
		if(null != qryStatusYellow && !qryStatusYellow.equals("")){
			whereClause+=" setcolor='yellow' or";
			andFlag=true;
		}
		if(null != qryStatusPurple && !qryStatusPurple.equals("")){
			whereClause+="  setcolor='purple' or";
			andFlag=true;
		}
		if(null != qryStatusWhite && !qryStatusWhite.equals("")){
			whereClause+="  setcolor='white' or";
			andFlag=true;
		}
		if(null != qryStatusGreen && !qryStatusGreen.equals("")){
			whereClause+="  setcolor='green' or";
			andFlag=true;
		}
		if(andFlag){
			whereClause=whereClause.substring(0,whereClause.length()-2);
					whereClause=	" where  ("+whereClause+")";
			}
		if(!sSearch.equals("")){
			if(andFlag)
			searchClause=" and "+searchingSql;
			else
				searchClause=" where "+searchingSql;
		}
		//sqlBuffer.append(whereClause);
		
		
		StringBuffer sqlcomp = new StringBuffer();
		sqlcomp.append("select * from ( select a.*,");
		if("0".equalsIgnoreCase(orgIds))
			sqlcomp.append("7 as orgrights");
		else
			sqlcomp.append("(SELECT NVL(MAX(USERSITE_RIGHT),0) USERSITE_RIGHT FROM er_usersite usr,er_patfacility fac WHERE fac.fk_per = a.fk_per AND fac.patfacility_accessright > 0 AND fac.fk_site = usr.fk_site AND usr.fk_user = "+userId+") as orgrights");
		sqlcomp.append(" FROM ("+sqlBuffer+sqlOrderBy+" ) b");
		//sqlcomp.append(") a WHERE ROWNUM <= "+LastRec+" ) WHERE rnum >="+firstRec);
		StringBuffer sqlcount = new StringBuffer();
		sqlcount.append("Select count(*) from ( " + sqlBuffer +sqlOrderBy+" ) b "+whereClause+searchClause+" )a ");
	
		HashMap<String, Object> dataMap = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		int rowcount=0;

		try {
			conn = CommonDAO.getConnection();
			pstmt = conn.prepareStatement(sqlcount.toString());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				rowcount = rs.getInt("count(*)");
			}
			if(LastRec==-1){LastRec=rowcount;}
			sqlcomp.append(whereClause+ searchClause+" ) a where rnum <= "+LastRec+" "+ "  ) WHERE rnum >"+firstRec);
			pstmt = conn.prepareStatement(sqlcomp.toString());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				dataMap = new HashMap<String, Object>();
				dataMap.put("rows", rowcount);
				if(null != qryStatusRed && !qryStatusRed.equals(""))
				{   //For red icon: open and re-opened
//					if((rs.getString("query_status").equalsIgnoreCase("Open") 
//							|| rs.getString("query_status").equalsIgnoreCase("Re-opened")) 
//							&& (rs.getString("query_type").equalsIgnoreCase("High Priority")))
					if(rs.getString("setcolor").equals("red"))
					{
					dataMap.put("query_status", rs.getString("query_status"));
					dataMap.put("total_queries", rs.getString("total_queries")); 
					dataMap.put("days_open", rs.getInt("days_open"));
					//dataMap.put("target_days", rs.getInt("target_days"));
					dataMap.put("study_number", rs.getString(("study_number")));
					dataMap.put("organization", rs.getString(("site_name")));
					dataMap.put("patient_study_id", rs.getString(("patstdid")));
					dataMap.put("form_name", rs.getString(("form_name")));
					dataMap.put("form_status", rs.getString(("form_status")));
					//dataMap.put("form_target_days", rs.getInt("form_target_days"));
					dataMap.put("data_entry_date", rs.getString(("data_entry_date")));
					dataMap.put("last_modified_date", rs.getString(("modified_date")));
					dataMap.put("data_entered_by", rs.getString(("data_entered_by")));
					dataMap.put("calender", rs.getString(("Calender_name")));
					dataMap.put("visit", rs.getString(("visit_name")));
					dataMap.put("event", rs.getString(("event_name")));
					dataMap.put("event_date", rs.getString(("event_end_date")));
					dataMap.put("response_id", rs.getString(("pk_form")));
					dataMap.put("formtype", rs.getString(("formtype")));
					dataMap.put("pk_study", rs.getString(("pk_study")));
					dataMap.put("fk_form", rs.getString(("fk_form")));
					dataMap.put("fk_per", rs.getString(("fk_per")));
					//dataMap.put("pk_formquery", rs.getString(("pk_formquery")));
					//dataMap.put("fld_name", rs.getString(("fld_name")));
					dataMap.put("FK_FORMLIBVER", rs.getString(("FK_FORMLIBVER")));
					dataMap.put("pk_patprot", rs.getString(("pk_patprot")));
					dataMap.put("lf_entrychar", rs.getString(("lf_entrychar")));
					dataMap.put("sub_type", rs.getString(("sub_type")));
					dataMap.put("form_stat_subtyp",rs.getString("form_stat_subtyp"));
					dataMap.put("orgrights",rs.getString("orgrights"));
					dataMap.put("form_Status_id",rs.getInt(("form_Status_id")));
					dataMap.put("editboxPlaceHolder", "");
					dataMap.put("auditboxPlaceHolder", "");
					dataMap.put("trackChangesPlaceHolder", "");
					dataMap.put("checkboxPlaceHolder", "");
					//dataMap.put("fk_formquery", rs.getString(("fk_formquery")));
					//dataMap.put("fk_querymodule", rs.getString(("fk_querymodule")));
					dataMap.put("query_type", rs.getString("query_type"));
					dataMap.put("flg_color", "red");
					dataMap.put("sortCol", sortCol);
					dataMap.put("sortDir", sortDir);
					//System.out.println("@222query type red===>>>"+rs.getString("query_type"));
					maplist.add(dataMap);
					} //red icon: open and re-opened end
								
				}
					//For orange icon start
					
					if(null != qryStatusOrange && !qryStatusOrange.equals("")) {
//							&& qryStatusOrange.equalsIgnoreCase("Ready for Monitor Review")
//							&& rs.getString("query_status").equalsIgnoreCase("Ready for Monitor Review")
//							 && (rs.getString("query_type").equalsIgnoreCase("High Priority"))) 
				if(rs.getString("setcolor").equals("orange"))
					{					
					dataMap.put("query_status", rs.getString("query_status"));
					dataMap.put("total_queries", rs.getString("total_queries")); 
					dataMap.put("days_open", rs.getInt("days_open"));
					//dataMap.put("target_days", rs.getInt("target_days"));
					dataMap.put("study_number", rs.getString(("study_number")));
					dataMap.put("organization", rs.getString(("site_name")));
					dataMap.put("patient_study_id", rs.getString(("patstdid")));
					dataMap.put("form_name", rs.getString(("form_name")));
					dataMap.put("form_status", rs.getString(("form_status")));
					//dataMap.put("form_target_days", rs.getInt("form_target_days"));
					dataMap.put("data_entry_date", rs.getString(("data_entry_date")));
					dataMap.put("last_modified_date", rs.getString(("modified_date")));
					dataMap.put("data_entered_by", rs.getString(("data_entered_by")));
					dataMap.put("calender", rs.getString(("Calender_name")));
					dataMap.put("visit", rs.getString(("visit_name")));
					dataMap.put("event", rs.getString(("event_name")));
					dataMap.put("event_date", rs.getString(("event_end_date")));
					dataMap.put("response_id", rs.getString(("pk_form")));
					dataMap.put("formtype", rs.getString(("formtype")));
					dataMap.put("pk_study", rs.getString(("pk_study")));
					dataMap.put("fk_form", rs.getString(("fk_form")));
					dataMap.put("fk_per", rs.getString(("fk_per")));
					//dataMap.put("pk_formquery", rs.getString(("pk_formquery")));
					//dataMap.put("fld_name", rs.getString(("fld_name")));
					dataMap.put("FK_FORMLIBVER", rs.getString(("FK_FORMLIBVER")));
					dataMap.put("pk_patprot", rs.getString(("pk_patprot")));
					dataMap.put("lf_entrychar", rs.getString(("lf_entrychar")));
					dataMap.put("form_Status_id",rs.getInt(("form_Status_id")));
					dataMap.put("sub_type", rs.getString(("sub_type")));
					dataMap.put("form_stat_subtyp",rs.getString("form_stat_subtyp"));
					dataMap.put("orgrights",rs.getString("orgrights"));
					dataMap.put("editboxPlaceHolder", "");
					dataMap.put("auditboxPlaceHolder", "");
					dataMap.put("trackChangesPlaceHolder", "");
					dataMap.put("checkboxPlaceHolder", "");
					//dataMap.put("fk_formquery", rs.getString(("fk_formquery")));
					//dataMap.put("fk_querymodule", rs.getString(("fk_querymodule")));
					dataMap.put("query_type", rs.getString("query_type"));
					dataMap.put("flg_color", "orange");
					dataMap.put("sortCol", sortCol);
					dataMap.put("sortDir", sortDir);
					//System.out.println("query type filter Orange===>>>"+rs.getString("query_type"));
					maplist.add(dataMap);
					
				}}	//For orange icon end
					
                      //For yellow icon start
					
					if(null != qryStatusYellow && !qryStatusYellow.equals("") ){
//							&& qryStatusYellow.equalsIgnoreCase("Ready for Monitor Review")
//							&& rs.getString("query_status").equalsIgnoreCase("Ready for Monitor Review")
//							 && (rs.getString("query_type").equalsIgnoreCase("Normal"))) 
				if(rs.getString("setcolor").equals("yellow"))
					{					
					dataMap.put("query_status", rs.getString("query_status"));
					dataMap.put("total_queries", rs.getString("total_queries")); 
					dataMap.put("days_open", rs.getInt("days_open"));
					//dataMap.put("target_days", rs.getInt("target_days"));
					dataMap.put("study_number", rs.getString(("study_number")));
					dataMap.put("organization", rs.getString(("site_name")));
					dataMap.put("patient_study_id", rs.getString(("patstdid")));
					dataMap.put("form_name", rs.getString(("form_name")));
					dataMap.put("form_status", rs.getString(("form_status")));
					//dataMap.put("form_target_days", rs.getInt("form_target_days"));
					dataMap.put("data_entry_date", rs.getString(("data_entry_date")));
					dataMap.put("last_modified_date", rs.getString(("modified_date")));
					dataMap.put("data_entered_by", rs.getString(("data_entered_by")));
					dataMap.put("calender", rs.getString(("Calender_name")));
					dataMap.put("visit", rs.getString(("visit_name")));
					dataMap.put("event", rs.getString(("event_name")));
					dataMap.put("event_date", rs.getString(("event_end_date")));
					dataMap.put("response_id", rs.getString(("pk_form")));
					dataMap.put("formtype", rs.getString(("formtype")));
					dataMap.put("pk_study", rs.getString(("pk_study")));
					dataMap.put("fk_form", rs.getString(("fk_form")));
					dataMap.put("fk_per", rs.getString(("fk_per")));
					//dataMap.put("pk_formquery", rs.getString(("pk_formquery")));
					//dataMap.put("fld_name", rs.getString(("fld_name")));
					dataMap.put("FK_FORMLIBVER", rs.getString(("FK_FORMLIBVER")));
					dataMap.put("pk_patprot", rs.getString(("pk_patprot")));
					dataMap.put("lf_entrychar", rs.getString(("lf_entrychar")));
					dataMap.put("form_Status_id",rs.getInt(("form_Status_id")));
					dataMap.put("sub_type", rs.getString(("sub_type")));
					dataMap.put("form_stat_subtyp",rs.getString("form_stat_subtyp"));
					dataMap.put("orgrights",rs.getString("orgrights"));
					dataMap.put("editboxPlaceHolder", "");
					dataMap.put("auditboxPlaceHolder", "");
					dataMap.put("trackChangesPlaceHolder", "");
					dataMap.put("checkboxPlaceHolder", "");
					//dataMap.put("fk_formquery", rs.getString(("fk_formquery")));
					//dataMap.put("fk_querymodule", rs.getString(("fk_querymodule")));
					dataMap.put("query_type", rs.getString("query_type"));
					dataMap.put("flg_color", "yellow");
					dataMap.put("sortCol", sortCol);
					dataMap.put("sortDir", sortDir);
					//System.out.println("query type filter Yellow===>>>"+rs.getString("query_type"));
					maplist.add(dataMap);
					
				}}	//For yellow icon end
					
				//For Purple icon start
					if(null != qryStatusPurple && !qryStatusPurple.equals(""))
					{   //For red icon: open and re-opened collapse prevented now						
//						if((rs.getString("query_status").equalsIgnoreCase("Open") 
//								|| rs.getString("query_status").equalsIgnoreCase("Re-opened"))
//								&& (rs.getString("query_type").equalsIgnoreCase("Normal")))
						if(rs.getString("setcolor").equals("purple"))
						{
						dataMap.put("query_status", rs.getString("query_status"));
						dataMap.put("total_queries", rs.getString("total_queries")); 
						dataMap.put("days_open", rs.getInt("days_open"));
						//dataMap.put("target_days", rs.getInt("target_days"));
						dataMap.put("study_number", rs.getString(("study_number")));
						dataMap.put("organization", rs.getString(("site_name")));
						dataMap.put("patient_study_id", rs.getString(("patstdid")));
						dataMap.put("form_name", rs.getString(("form_name")));
						dataMap.put("form_status", rs.getString(("form_status")));
						//dataMap.put("form_target_days", rs.getInt("form_target_days"));
						dataMap.put("data_entry_date", rs.getString(("data_entry_date")));
						dataMap.put("last_modified_date", rs.getString(("modified_date")));
						dataMap.put("data_entered_by", rs.getString(("data_entered_by")));
						dataMap.put("calender", rs.getString(("Calender_name")));
						dataMap.put("visit", rs.getString(("visit_name")));
						dataMap.put("event", rs.getString(("event_name")));
						dataMap.put("event_date", rs.getString(("event_end_date")));
						dataMap.put("response_id", rs.getString(("pk_form")));
						dataMap.put("formtype", rs.getString(("formtype")));
						dataMap.put("pk_study", rs.getString(("pk_study")));
						dataMap.put("fk_form", rs.getString(("fk_form")));
						dataMap.put("fk_per", rs.getString(("fk_per")));
						//dataMap.put("pk_formquery", rs.getString(("pk_formquery")));
						//dataMap.put("fld_name", rs.getString(("fld_name")));
						dataMap.put("FK_FORMLIBVER", rs.getString(("FK_FORMLIBVER")));
						dataMap.put("pk_patprot", rs.getString(("pk_patprot")));
						dataMap.put("lf_entrychar", rs.getString(("lf_entrychar")));
						dataMap.put("form_Status_id",rs.getInt(("form_Status_id")));
						dataMap.put("sub_type", rs.getString(("sub_type")));
						dataMap.put("form_stat_subtyp",rs.getString("form_stat_subtyp"));
						dataMap.put("orgrights",rs.getString("orgrights"));
						dataMap.put("editboxPlaceHolder", "");
						dataMap.put("auditboxPlaceHolder", "");
						dataMap.put("trackChangesPlaceHolder", "");
						dataMap.put("checkboxPlaceHolder", "");
						//dataMap.put("fk_formquery", rs.getString(("fk_formquery")));
						//dataMap.put("fk_querymodule", rs.getString(("fk_querymodule")));
						dataMap.put("query_type", rs.getString("query_type"));
						dataMap.put("flg_color", "purple");
						dataMap.put("sortCol", sortCol);
						dataMap.put("sortDir", sortDir);
						//System.out.println("@354query type purple===>>>"+rs.getString("query_type"));
						maplist.add(dataMap);
						} //red icon: open and re-opened end
									
					}	
					
			   /// For Purple icon end	
					
                           //for white icon start
					
					if(null != qryStatusWhite && !qryStatusWhite.equals(""))
					{  
//						if(rs.getString("total_queries").equalsIgnoreCase("0") 
//							&& rs.getString("query_status").equalsIgnoreCase("-")
//							&& (rs.getString("query_type").equalsIgnoreCase("-"))) 
						if(rs.getString("setcolor").equals("white"))
					{
						
						dataMap.put("query_status", rs.getString("query_status"));
						dataMap.put("total_queries", rs.getString("total_queries")); 
						dataMap.put("days_open", rs.getInt("days_open"));
						//dataMap.put("target_days", rs.getInt("target_days"));
						dataMap.put("study_number", rs.getString(("study_number")));
						dataMap.put("organization", rs.getString(("site_name")));
						dataMap.put("patient_study_id", rs.getString(("patstdid")));
						dataMap.put("form_name", rs.getString(("form_name")));
						dataMap.put("form_status", rs.getString(("form_status")));
						//dataMap.put("form_target_days", rs.getInt("form_target_days"));
						dataMap.put("data_entry_date", rs.getString(("data_entry_date")));
						dataMap.put("last_modified_date", rs.getString(("modified_date")));
						dataMap.put("data_entered_by", rs.getString(("data_entered_by")));
						dataMap.put("calender", rs.getString(("Calender_name")));
						dataMap.put("visit", rs.getString(("visit_name")));
						dataMap.put("event", rs.getString(("event_name")));
						dataMap.put("event_date", rs.getString(("event_end_date")));
						dataMap.put("response_id", rs.getString(("pk_form")));
						dataMap.put("formtype", rs.getString(("formtype")));
                        dataMap.put("pk_study", rs.getString(("pk_study")));
						dataMap.put("fk_form", rs.getString(("fk_form")));
						dataMap.put("fk_per", rs.getString(("fk_per")));
						//dataMap.put("pk_formquery", rs.getString(("pk_formquery")));
						//System.out.println("formQuery in queryManagementDao-"+rs.getString("pk_formquery"));
						//dataMap.put("fld_name", rs.getString(("fld_name")));
						//System.out.println("fld_name in queryManagementDao-"+rs.getString("fld_name"));
						dataMap.put("FK_FORMLIBVER", rs.getString(("FK_FORMLIBVER")));
						dataMap.put("pk_patprot", rs.getString(("pk_patprot")));
						dataMap.put("lf_entrychar", rs.getString(("lf_entrychar")));
						dataMap.put("form_Status_id",rs.getInt(("form_Status_id")));
						dataMap.put("sub_type", rs.getString(("sub_type")));
						dataMap.put("form_stat_subtyp",rs.getString("form_stat_subtyp"));
						dataMap.put("orgrights",rs.getString("orgrights"));
						dataMap.put("editboxPlaceHolder", "");
						dataMap.put("auditboxPlaceHolder", "");
						dataMap.put("trackChangesPlaceHolder", "");
						dataMap.put("checkboxPlaceHolder", "");
//						dataMap.put("fk_formquery", rs.getString(("fk_formquery")));
//						dataMap.put("fk_querymodule", rs.getString(("fk_querymodule")));
						dataMap.put("query_type", rs.getString("query_type"));
						dataMap.put("flg_color", "white");
						dataMap.put("sortCol", sortCol);
						dataMap.put("sortDir", sortDir);
						//System.out.println("@387query type white===>>>"+rs.getString("query_type"));
						maplist.add(dataMap);
					} 
									
					}
					
					//for white icon end	
					//For Green icon start
						if(null != qryStatusGreen && !qryStatusGreen.equals("")){
//								&& qryStatusGreen.equalsIgnoreCase("Resolved") 
//								&& rs.getString("query_status").equalsIgnoreCase("Resolved"))
					if(rs.getString("setcolor").equals("green"))
						{   //For red icon: open and re-opened
							
							dataMap.put("query_status", rs.getString("query_status"));
							dataMap.put("total_queries", rs.getString("total_queries")); 
							dataMap.put("days_open", rs.getInt("days_open"));
							//dataMap.put("target_days", rs.getInt("target_days"));
							dataMap.put("study_number", rs.getString(("study_number")));
							dataMap.put("organization", rs.getString(("site_name")));
							dataMap.put("patient_study_id", rs.getString(("patstdid")));
							dataMap.put("form_name", rs.getString(("form_name")));
							dataMap.put("form_status", rs.getString(("form_status")));
							//dataMap.put("form_target_days", rs.getInt("form_target_days"));
							dataMap.put("data_entry_date", rs.getString(("data_entry_date")));
							dataMap.put("last_modified_date", rs.getString(("modified_date")));
							dataMap.put("data_entered_by", rs.getString(("data_entered_by")));
							dataMap.put("calender", rs.getString(("Calender_name")));
							dataMap.put("visit", rs.getString(("visit_name")));
							dataMap.put("event", rs.getString(("event_name")));
							dataMap.put("event_date", rs.getString(("event_end_date")));
							dataMap.put("response_id", rs.getString(("pk_form")));
							dataMap.put("formtype", rs.getString(("formtype")));
							dataMap.put("pk_study", rs.getString(("pk_study")));
							dataMap.put("fk_form", rs.getString(("fk_form")));
							dataMap.put("fk_per", rs.getString(("fk_per")));
							//dataMap.put("pk_formquery", rs.getString(("pk_formquery")));
							//dataMap.put("fld_name", rs.getString(("fld_name")));
							dataMap.put("FK_FORMLIBVER", rs.getString(("FK_FORMLIBVER")));
							dataMap.put("pk_patprot", rs.getString(("pk_patprot")));
							dataMap.put("lf_entrychar", rs.getString(("lf_entrychar")));
							dataMap.put("form_Status_id",rs.getInt(("form_Status_id")));
							dataMap.put("sub_type", rs.getString(("sub_type")));
							dataMap.put("form_stat_subtyp",rs.getString("form_stat_subtyp"));
							dataMap.put("orgrights",rs.getString("orgrights"));
							dataMap.put("editboxPlaceHolder", "");
							dataMap.put("auditboxPlaceHolder", "");
							dataMap.put("trackChangesPlaceHolder", "");
							dataMap.put("checkboxPlaceHolder", "");
							//dataMap.put("fk_formquery", rs.getString(("fk_formquery")));
							//dataMap.put("fk_querymodule", rs.getString(("fk_querymodule")));
							dataMap.put("query_type", rs.getString("query_type"));
							dataMap.put("flg_color", "green");
							dataMap.put("sortCol", sortCol);
							dataMap.put("sortDir", sortDir);
							//System.out.println("query type filter Green===>>>"+rs.getString("query_type"));
							maplist.add(dataMap);
							//red icon: open and re-opened end
										
						}	}
						
				   /// For Green icon end	
			else if(noFilter.equalsIgnoreCase("true")) 
					{
					dataMap.put("query_status", rs.getString("query_status"));
					dataMap.put("total_queries", rs.getString("total_queries")); 
					dataMap.put("days_open", rs.getInt("days_open"));
					//dataMap.put("target_days", rs.getInt("target_days"));
					dataMap.put("study_number", rs.getString(("study_number")));
					dataMap.put("organization", rs.getString(("site_name")));
					dataMap.put("patient_study_id", rs.getString(("patstdid")));
					dataMap.put("form_name", rs.getString(("form_name")));
					dataMap.put("form_status", rs.getString(("form_status")));
					//dataMap.put("form_target_days", rs.getInt("form_target_days"));
					dataMap.put("data_entry_date", rs.getString(("data_entry_date")));
					dataMap.put("last_modified_date", rs.getString(("modified_date")));
					dataMap.put("data_entered_by", rs.getString(("data_entered_by")));
					dataMap.put("calender", rs.getString(("Calender_name")));
					dataMap.put("visit", rs.getString(("visit_name")));
					dataMap.put("event", rs.getString(("event_name")));
					dataMap.put("event_date", rs.getString(("event_end_date")));
					dataMap.put("response_id", rs.getString(("pk_form")));
					dataMap.put("formtype", rs.getString(("formtype")));
					dataMap.put("pk_study", rs.getString(("pk_study")));
					dataMap.put("fk_form", rs.getString(("fk_form")));
					dataMap.put("fk_per", rs.getString(("fk_per")));
					//dataMap.put("pk_formquery", rs.getString(("pk_formquery")));
					//dataMap.put("fld_name", rs.getString(("fld_name")));
					dataMap.put("FK_FORMLIBVER", rs.getString(("FK_FORMLIBVER")));
					dataMap.put("pk_patprot", rs.getString(("pk_patprot")));
					dataMap.put("lf_entrychar", rs.getString(("lf_entrychar")));
					dataMap.put("form_Status_id",rs.getInt(("form_Status_id")));
					dataMap.put("sub_type", rs.getString(("sub_type")));
					dataMap.put("form_stat_subtyp",rs.getString("form_stat_subtyp"));
					dataMap.put("orgrights",rs.getString("orgrights"));
					dataMap.put("editboxPlaceHolder", "");
					dataMap.put("auditboxPlaceHolder", "");
					dataMap.put("trackChangesPlaceHolder", "");
					dataMap.put("checkboxPlaceHolder", "");
					//dataMap.put("fk_formquery", rs.getString(("fk_formquery")));
					//dataMap.put("fk_querymodule", rs.getString(("fk_querymodule")));
					dataMap.put("query_type", rs.getString("query_type"));
					dataMap.put("sortCol", sortCol);
					dataMap.put("sortDir", sortDir);
					//if(rs.getString("query_status").equalsIgnoreCase("Resolved"))
					if(rs.getString("setcolor").equals("green"))
					{
						dataMap.put("flg_color", "green");
						//System.out.println("green color flag for===>>>"+rs.getString("query_type"));
					}
					else if(rs.getString("setcolor").equals("red"))
						//if((rs.getString("query_status").equalsIgnoreCase("Open") || rs.getString("query_status").equalsIgnoreCase("Re-opened")) && rs.getString("query_type").equalsIgnoreCase("High Priority"))
					{
						dataMap.put("flg_color","red");
						//System.out.println("red color flag for===>>>"+rs.getString("query_type"));
					}
					
					 else if(rs.getString("setcolor").equals("purple"))
						//if((rs.getString("query_status").equalsIgnoreCase("Re-opened") || rs.getString("query_status").equalsIgnoreCase("Open")) && rs.getString("query_type").equalsIgnoreCase("Normal"))
					{
						dataMap.put("flg_color", "purple");
						//System.out.println("purple color flag for===>>>"+rs.getString("query_type"));
					}
					else if(rs.getString("setcolor").equals("orange")) 
						//if((rs.getString("query_status").equalsIgnoreCase("Ready for Monitor Review")) && rs.getString("query_type").equalsIgnoreCase("High Priority"))
					{
						dataMap.put("flg_color", "orange");
						//System.out.println("orange color flag for===>>>"+rs.getString("query_type"));
					}
					else if(rs.getString("setcolor").equals("yellow")) 
						//if((rs.getString("query_status").equalsIgnoreCase("Ready for Monitor Review")) && rs.getString("query_type").equalsIgnoreCase("Normal"))
					{
						dataMap.put("flg_color", "yellow");
						//System.out.println("yellow color flag for===>>>"+rs.getString("query_type"));
					}
					else if(rs.getString("setcolor").equals("white")) 
						//if((rs.getString("query_status").equalsIgnoreCase("-")) && rs.getString("query_type").equalsIgnoreCase("-") && rs.getString("total_queries").equalsIgnoreCase("0"))
					{
						dataMap.put("flg_color", "white");
						//System.out.println("white color flag for default===>>>"+rs.getString("query_type"));
					}
					else
					{   //when no match condition found
						dataMap.put("flg_color", "");
					}
										
					maplist.add(dataMap);
				}
			}
			if(maplist.size()!=0){
				//System.out.println("list of maplist-"+maplist.size());
				maplist.get(0).put("rows", rowcount);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		}
		return maplist;

	}
			
	
	public ArrayList<HashMap<String, Object>> fetchAdvEventQueriesData(
			HashMap<String, String> paramMap, int dispLength, int dispStart, int sortCol, String sortDir) {

		String accountId = paramMap.get("accountId");
		// String formStatus = paramMap.get("formStatus");
		String sSearch=paramMap.get("sSearch");
		String userId = paramMap.get("userId");
		String studyIds = paramMap.get("studyIds");
		String orgIds  = paramMap.get("orgIds");
		String formIds = paramMap.get("formId");
		String advEventStatId = paramMap.get("advEventStatId");
		String dataEntryDateFrom = paramMap.get("dataEntryDateFrom");
		String dataEntryDateTo = paramMap.get("dataEntryDateTo");
		String patientIds = paramMap.get("patientId");
		String patientStdIds = paramMap.get("patientStdId");
		String queryCreatorIds = paramMap.get("queryCreatorId");
		String selectedTotalRespQuery = paramMap.get("selectedTotalRespQuery");
		String selectedqueryStatuess = paramMap.get("selectedqueryStatuess");
		String queryStatusIds = paramMap.get("queryStatusId");
		String noFilter = paramMap.get("noFilter");
		String qryStatusRed = paramMap.get("qryStatusRed");
		String qryStatusOrange = paramMap.get("qryStatusOrange");
		String qryStatusYellow = paramMap.get("qryStatusYellow");
		String qryStatusPurple = paramMap.get("qryStatusPurple");
		String qryStatusWhite = paramMap.get("qryStatusWhite");
		String qryStatusGreen = paramMap.get("qryStatusGreen");
		int firstRec = dispStart;
		int LastRec = dispLength+dispStart;
		//if(sortCol>2)
		//sortCol = sortCol-2;
		StringBuffer sqlBuffer = new StringBuffer();
		/* add some attribute like as pk_study,pk_patprot,fk_per,fk_codelst_stat by Vivek */
		//Bug id #21071 fixed by Rashi Starts
		sqlBuffer.append("SELECT aa.setColor,aa.total_queries, DECODE(aa.FK_CODELST_QUERYSTATUS,(SELECT pk_codelst FROM er_codelst WHERE codelst_type  ='query_status' AND codelst_subtyp  ='closed'),'(No Aged Query)', NVL((SELECT codelst_desc FROM er_codelst WHERE PK_CODELST = aa.FK_CODELST_QUERYSTATUS),'-')) query_status1,(CASE WHEN days_open IS NULL THEN 0 ELSE days_open END) days_open, aa.ae_treatment_course,aa.studyNum,aa.organization,aa.patStdId,aa.adv_Type,aa.adv_Cat,aa.AE_TOXICITY,aa.adv_Grade,aa.meddra_code,aa.DICTIONARY,aa.attribution,aa.create_on,aa.adv_Sdate,aa.adv_Edate,aa.last_modified_date,aa.ae_discvrydate,aa.ae_loggeddate,aa.entered_by,aa.reported_by,aa.AE_OUTTYPE,aa.action,aa.AE_OUTNOTES,aa.AE_RECOVERY_DESC,(SELECT codelst_desc FROM sch_codelst WHERE pk_codelst=form_status) AS AE_STATUS,aa.adv_id,aa.pk_patprot,aa.fk_per,aa.FORM_STATUS,aa.statid,aa.pk_study, aa.adv_TypeId, (SELECT MIN (pk_formquery) FROM er_formquery  WHERE fk_querymodule=aa.adv_id) AS pk_formquery,(SELECT fk_field FROM er_formquery WHERE pk_formquery=(SELECT MIN (pk_formquery) FROM er_formquery WHERE fk_querymodule=aa.adv_id) ) AS field_id,aa.sub_type from " +
				"(SELECT eres.f_getcolorflag(a.pk_adveve,e.pk_study,8) AS setColor,a.ae_treatment_course,e.study_number as studyNum,f.site_name as organization,d.PATPROT_PATSTDID as patStdId,b.CODELST_DESC AS adv_Type,a.AE_CATEGORY as adv_Cat,AE_TOXICITY,a.AE_GRADE AS adv_Grade,MEDDRA as meddra_code,DICTIONARY,(SELECT DISTINCT codelst_desc FROM sch_codelst WHERE sch_codelst.codelst_type='adve_relation' AND sch_codelst.PK_CODELST =a.AE_RELATIONSHIP) AS attribution, "+
				"TO_CHAR(a.CREATED_ON,pkg_dateUtil.f_get_dateformat) create_on,TO_CHAR(a.ae_stdate,pkg_dateUtil.f_get_dateformat) adv_Sdate,TO_CHAR(a.ae_enddate,pkg_dateUtil.f_get_dateformat) adv_Edate,TO_CHAR(a.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) last_modified_date,TO_CHAR(AE_DISCVRYDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) ae_discvrydate,TO_CHAR(AE_LOGGEDDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) ae_loggeddate,c.usr_firstname||', '||c.usr_lastname as entered_by,(select USR_FIRSTNAME||', '||USR_LASTNAME from er_user where pk_user=a.ae_reportedby) as reported_by, "+
				"ESCH.F_GETOUTCOMETYPENAME(AE_OUTTYPE) AS AE_OUTTYPE,(SELECT codelst_desc FROM sch_codelst WHERE pk_codelst = FK_CODELST_OUTACTION) action,AE_OUTNOTES, (SELECT codelst_desc FROM sch_codelst WHERE pk_codelst = AE_RECOVERY_DESC) as AE_RECOVERY_DESC,a.pk_adveve AS adv_Id,d.pk_patprot AS pk_patprot,d.fk_per AS fk_per,FORM_STATUS,g.fk_codelst_stat as statid,e.pk_study AS pk_study,a.FK_CODLST_AETYPE AS adv_TypeId," +
				"( select COUNT(*) FROM ER_FORMQUERY  WHERE FK_QUERYMODULE = a.pk_adveve and querymodule_linkedto=8) total_queries,"
				 +
				"NVL((SELECT FK_CODELST_QUERYSTATUS FROM ER_FORMQUERYSTATUS WHERE PK_FORMQUERYSTATUS=h.PK_FORMQUERYSTATUS),DECODE((SELECT COUNT(*) FROM ER_FORMQUERY  WHERE FK_QUERYMODULE    = a.pk_adveve  AND querymodule_linkedto=8),0,0,(SELECT pk_codelst  FROM er_codelst  WHERE codelst_type  ='query_status' AND codelst_subtyp  ='closed'))) FK_CODELST_QUERYSTATUS,(SELECT TRUNC(sysdate) - to_date(min(ENTERED_ON)) FROM ER_FORMQUERYSTATUS WHERE FK_FORMQUERY=h.PK_FORMQUERY ) days_open,(select replace(codelst_subtyp,' ','') from sch_codelst where pk_codelst=form_status) AS sub_type FROM sch_adverseve a, " +
				"sch_codelst b, ERES.er_user c, ERES.er_patprot d, ERES.er_study e, eres.er_site f,ERES.er_patstudystat g,(select a.*,max(b.PK_FORMQUERYSTATUS) PK_FORMQUERYSTATUS from er_formquerystatus b,(select min(PK_FORMQUERY) PK_FORMQUERY, FK_QUERYMODULE,QUERYMODULE_LINKEDTO from er_formquery fq  where (SELECT COUNT(*) FROM er_formquerystatus fqs1 WHERE fqs1.fk_formquery = fq.PK_FORMQUERY AND fqs1.PK_FORMQUERYSTATUS=(SELECT MAX(PK_FORMQUERYSTATUS) FROM er_formquerystatus fqs2 WHERE fqs2.fk_formquery = fqs1.fk_formquery) AND fqs1.fk_codelst_querystatus=(SELECT pk_codelst FROM er_codelst WHERE codelst_type  ='query_status' AND codelst_subtyp  ='closed'))=0 And QUERYMODULE_LINKEDTO=8	group by FK_QUERYMODULE,QUERYMODULE_LINKEDTO) a where a.PK_FORMQUERY=b.FK_FORMQUERY group by PK_FORMQUERY,FK_QUERYMODULE,QUERYMODULE_LINKEDTO) h ");
		sqlBuffer.append("WHERE e.fk_account="+accountId+" AND e.pk_study = a.fk_study AND a.fk_per = d.fk_per AND a.fk_study = d.fk_study and g.fk_study=e.pk_study and g.fk_per=a.fk_per AND h.FK_QUERYMODULE(+) = a.pk_adveve  AND h.QUERYMODULE_LINKEDTO(+) = 8 AND g.current_stat=1 AND d.PATPROT_STAT <> 0 AND a.FK_CODLST_AETYPE=b.pk_codelst ");
		if(!"".equalsIgnoreCase(studyIds))
			sqlBuffer.append(" AND pk_study  IN ("+studyIds+") ");
		if(!"".equalsIgnoreCase(orgIds))
			sqlBuffer.append(" AND FK_SITE_ENROLLING  IN ("+orgIds+") ");
		if(!"".equalsIgnoreCase(formIds))
			sqlBuffer.append(" AND a.FK_CODLST_AETYPE  IN ("+formIds+") ");
		if(!"".equalsIgnoreCase(advEventStatId))
			sqlBuffer.append(" AND FORM_STATUS  IN ("+advEventStatId+") ");
		if(!"".equalsIgnoreCase(dataEntryDateFrom) && "".equalsIgnoreCase(dataEntryDateTo))
			sqlBuffer.append(" and a.ae_stdate between TO_DATE('"+dataEntryDateFrom+"',PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE(PKG_DATEUTIL.f_get_future_null_date_str,PKG_DATEUTIL.F_GET_DATEFORMAT) ");
		if("".equalsIgnoreCase(dataEntryDateFrom) && !"".equalsIgnoreCase(dataEntryDateTo))
			sqlBuffer.append(" and a.ae_stdate between TO_DATE(PKG_DATEUTIL.f_get_null_date_str,PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE('"+dataEntryDateTo+"',PKG_DATEUTIL.F_GET_DATEFORMAT) ");
		if(!"".equalsIgnoreCase(dataEntryDateFrom) && !"".equalsIgnoreCase(dataEntryDateTo))
			sqlBuffer.append(" and a.ae_stdate between TO_DATE('"+dataEntryDateFrom+"',PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE('"+dataEntryDateTo+"',PKG_DATEUTIL.F_GET_DATEFORMAT) ");
		if(!"".equalsIgnoreCase(patientIds))
			sqlBuffer.append(" AND a.fk_per  IN ("+patientIds+") ");
		
		//if(!"".equalsIgnoreCase(queryStatusIds))
			//sqlBuffer.append(" AND FK_CODELST_QUERYSTATUS  IN ("+queryStatusIds+") ");
		
		if(!"".equalsIgnoreCase(patientStdIds))
			sqlBuffer.append(" AND a.fk_per  IN ("+patientStdIds+") ");
		if(!"".equalsIgnoreCase(queryCreatorIds))
			sqlBuffer.append(" AND c.pk_user  IN ("+queryCreatorIds+") ");
		sqlBuffer.append(" AND (pkg_util.f_getStudyRight(pkg_studystat.F_GET_USERRIGHTS_FOR_STUDY("+userId+" , pk_study), ");
		sqlBuffer.append(" (SELECT CTRL_SEQ  FROM er_ctrltab  WHERE CTRL_KEY = 'study_rights'  AND upper(ctrl_value)   = 'STUDYMPAT'))  > 0) AND (0   < pkg_user.f_chk_studyright_using_pat(a.FK_PER,a.FK_STUDY, "+userId+")) ");
		sqlBuffer.append("AND a.AE_ENTERBY = c.pk_user AND f.pk_site = d.fk_site_enrolling ");
		
		sqlBuffer.append(")aa where 1=1");
		
		if(!"".equalsIgnoreCase(queryStatusIds))
			sqlBuffer.append(" AND FK_CODELST_QUERYSTATUS  IN ("+queryStatusIds+") ");
		if(!"".equalsIgnoreCase(selectedqueryStatuess))
			sqlBuffer.append(" and days_open   >("+selectedqueryStatuess+") ");		
		if(!"".equalsIgnoreCase(selectedTotalRespQuery))
			sqlBuffer.append(" AND total_queries  >("+selectedTotalRespQuery+") ");
		boolean andFlag =false;
		String whereClause="";
		if(null != qryStatusRed && !qryStatusRed.equals("")){
			whereClause+="  setcolor='red' or";
			andFlag=true;
		}
		if(null != qryStatusOrange && !qryStatusOrange.equals("")){
			whereClause+=" setcolor='orange' or";
			andFlag=true;
		}
		if(null != qryStatusYellow && !qryStatusYellow.equals("")){
			whereClause+=" setcolor='yellow' or";
			andFlag=true;
		}
		if(null != qryStatusPurple && !qryStatusPurple.equals("")){
			whereClause+="  setcolor='purple' or";
			andFlag=true;
		}
		if(null != qryStatusWhite && !qryStatusWhite.equals("")){
			whereClause+="  setcolor='white' or";
			andFlag=true;
		}
		if(null != qryStatusGreen && !qryStatusGreen.equals("")){
			whereClause+="  setcolor='green' or";
			andFlag=true;
		}
		if(andFlag){
			whereClause=whereClause.substring(0,whereClause.length()-2);
			whereClause=	"    and ("+whereClause+")";
			}
		sqlBuffer.append(whereClause);
		if(sortCol==5)//Treatment Course
		{
			sqlBuffer.append("ORDER BY LOWER(ae_treatment_course) "+sortDir+" ");
		}
		else if(sortCol==6)//Study Number
		{
			sqlBuffer.append("ORDER BY LOWER(studyNum) "+sortDir+" ");
		}
		else if(sortCol==7)//Enrolling Site

		{
			sqlBuffer.append("ORDER BY LOWER(organization) "+sortDir+" ");
		}
		else if(sortCol==8)//patstdid

		{
			sqlBuffer.append("ORDER BY LOWER(patstdid) "+sortDir+" ");
		}
		else if(sortCol==10)//category
		{
			sqlBuffer.append("ORDER BY LOWER(adv_cat) "+sortDir+" ");
		}
		else if(sortCol==11)//Toxicity
		{
			sqlBuffer.append("ORDER BY LOWER(ae_toxicity) "+sortDir+" ");
		}
		else if(sortCol==13)//Meddra Code
		{
			sqlBuffer.append("ORDER BY LOWER(meddra_code) "+sortDir+" ");
		}
		else if(sortCol==14)//Dictionary
		{
			sqlBuffer.append("ORDER BY LOWER(dictionary) "+sortDir+" ");
		}
		else if(sortCol==15)//Attribution
		{
			sqlBuffer.append("ORDER BY LOWER(attribution) "+sortDir+" ");
		}
		else if(sortCol==16)//create_on
		{
			sqlBuffer.append("ORDER BY  TO_DATE(create_on,PKG_DATEUTIL.F_GET_DATEFORMAT) "+sortDir+" ");
		}
		else if(sortCol==17)//adv_sdate
		{
			sqlBuffer.append("ORDER BY  TO_DATE(adv_sdate,PKG_DATEUTIL.F_GET_DATEFORMAT) "+sortDir+" ");
		}
		else if(sortCol==18)//Stop date
		{
			sqlBuffer.append("ORDER BY  TO_DATE(adv_edate,PKG_DATEUTIL.F_GET_DATEFORMAT) "+sortDir+" ");
		}
		else if(sortCol==19)//last_modified_date
		{
			sqlBuffer.append("ORDER BY  TO_DATE(last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT) "+sortDir+" ");
		}
		else if(sortCol==20)//Discovery Date
		{
			sqlBuffer.append("ORDER BY  TO_DATE(ae_discvrydate,PKG_DATEUTIL.F_GET_DATEFORMAT) "+sortDir+" ");
		}
		else if(sortCol==21)//Log Date
		{
			sqlBuffer.append("ORDER BY  TO_DATE(ae_loggeddate,PKG_DATEUTIL.F_GET_DATEFORMAT) "+sortDir+" ");
		}
		else if(sortCol==22)//Entered By
		{
			sqlBuffer.append("ORDER BY  LOWER(entered_by) "+sortDir+" ");
		}
		else if(sortCol==23)//Reported By
		{
			sqlBuffer.append("ORDER BY  LOWER(reported_by) "+sortDir+" ");
		}
		else if(sortCol==24)//Outcome Type
		{
			sqlBuffer.append("ORDER BY  LOWER(AE_OUTTYPE) "+sortDir+" ");
		}
		else if(sortCol==25)//Action
		{
			sqlBuffer.append("ORDER BY  LOWER(action) "+sortDir+" ");
		}
		else if(sortCol==26)//Outcome Notes
		{
			sqlBuffer.append("ORDER BY  LOWER(ae_outnotes) "+sortDir+" ");
		}
		else if(sortCol==27)//Recover Description
		{
			sqlBuffer.append("ORDER BY  LOWER(ae_recovery_desc) "+sortDir+" ");
		}
		else{
		sqlBuffer.append(" ORDER BY "+sortCol+" "+sortDir);}
		String searchClause="";
		String searchingSql="(LOWER(total_queries) like LOWER('%"+sSearch+"%') or LOWER(query_status1) like LOWER('%"+sSearch+"%') or LOWER(days_open) like LOWER('%"+sSearch+"%')"
				+ " or LOWER(ae_treatment_course) like LOWER('%"+sSearch+"%') or LOWER(studyNum) like LOWER('%"+sSearch+"%') or LOWER(organization) like LOWER('%"+sSearch+"%')"
				+ " or LOWER(patStdId) like LOWER('%"+sSearch+"%') or LOWER(adv_Type) like LOWER('%"+sSearch+"%') or LOWER(adv_Cat) like LOWER('%"+sSearch+"%') "
				+ " or LOWER(AE_TOXICITY) like LOWER('%"+sSearch+"%') or LOWER(adv_Grade) like LOWER('%"+sSearch+"%')"
				+ " or LOWER(meddra_code) like LOWER('%"+sSearch+"%')  or LOWER(attribution) like LOWER('%"+sSearch+"%')  or LOWER(create_on) like LOWER('%"+sSearch+"%') "
				+ " or lower(adv_Sdate) like LOWER('%"+sSearch+"%') or lower(adv_Edate) like LOWER('%"+sSearch+"%') or lower(last_modified_date) like LOWER('%"+sSearch+"%')"
				+ " or lower(ae_discvrydate) like LOWER('%"+sSearch+"%') or lower(ae_loggeddate) like LOWER('%"+sSearch+"%') or lower(entered_by) like LOWER('%"+sSearch+"%')"
				+ " or lower(reported_by) like LOWER('%"+sSearch+"%') or lower(AE_OUTTYPE) like LOWER('%"+sSearch+"%') or lower(action) like LOWER('%"+sSearch+"%')"
				+ " or lower(AE_OUTNOTES) like LOWER('%"+sSearch+"%') or lower(AE_RECOVERY_DESC) like LOWER('%"+sSearch+"%') or lower(AE_STATUS) like LOWER('%"+sSearch+"%') or lower(adv_id) like LOWER('%"+sSearch+"%'))";
		
		if(!sSearch.equals("")){
			searchClause="where "+searchingSql;	
		}
		
		StringBuffer sqlcomp = new StringBuffer();
		sqlcomp.append("select * from ( select a.*, rownum rnum FROM ("+sqlBuffer);
		
		
        StringBuffer sqlcount = new StringBuffer();
		sqlcount.append("Select count(*) from (select a1.* from ( " + sqlBuffer + " )a1 " +searchClause+ ")");

		ArrayList<HashMap<String, Object>> maplist = new ArrayList<HashMap<String, Object>>();
		HashMap<String, Object> dataMap = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		int rowcount=0;

		try {
			conn = CommonDAO.getConnection();
			pstmt = conn.prepareStatement(sqlcount.toString());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				rowcount = rs.getInt("count(*)");
			}
			if(LastRec==-1){LastRec=rowcount;}
			
			sqlcomp.append(") a WHERE ROWNUM <= "+LastRec+" and "+searchingSql+" ) WHERE rnum >"+firstRec);
			pstmt = conn.prepareStatement(sqlcomp.toString());
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				
				dataMap = new HashMap<String, Object>();
				if(null != qryStatusRed && !qryStatusRed.equals("")){
					if(rs.getString("setcolor").equals("red"))
					{	
				
				dataMap.put("rows", rowcount);
				dataMap.put("query_status", "-");
				dataMap.put("status_days", "0");
				dataMap.put("total_queries",rs.getString(("total_queries")));
				dataMap.put("pk_patprot", rs.getString(("pk_patprot")));
				dataMap.put("fk_per", rs.getString(("fk_per")));
				dataMap.put("pk_study", rs.getString(("pk_study")));
				dataMap.put("adv_type", rs.getString(("adv_Type")));
				dataMap.put("adv_grade", rs.getString(("adv_Grade")));
				dataMap.put("query_status1", rs.getString(("query_status1")));
				dataMap.put("days_open",rs.getString(("days_open")));
				//dataMap.put("target_days",rs.getInt(("target_days")));
				dataMap.put("ae_treatment_course",rs.getString(("ae_treatment_course")));
				dataMap.put("studynum", rs.getString(("studyNum")));
				dataMap.put("organization", rs.getString(("organization")));
				dataMap.put("patstdid", rs.getString(("patStdId")));
				dataMap.put("adv_cat", rs.getString(("adv_Cat")));
				dataMap.put("AE_TOXICITY", rs.getString(("AE_TOXICITY")));
				dataMap.put("meddra_code", rs.getString(("meddra_code")));
				dataMap.put("DICTIONARY", rs.getString(("DICTIONARY")));
				dataMap.put("AE_DISCVRYDATE", rs.getString(("AE_DISCVRYDATE")));
				dataMap.put("AE_LOGGEDDATE", rs.getString(("AE_LOGGEDDATE")));
				dataMap.put("entered_by", rs.getString(("entered_by")));
				dataMap.put("reported_by", rs.getString(("reported_by")));
				dataMap.put("AE_OUTTYPE", rs.getString(("AE_OUTTYPE")));
				dataMap.put("attribution", rs.getString(("attribution")));
				dataMap.put("action", rs.getString(("action")));
				dataMap.put("AE_OUTNOTES", rs.getString(("AE_OUTNOTES")));
				dataMap.put("AE_RECOVERY_DESC", rs.getString(("AE_RECOVERY_DESC")));
				dataMap.put("AE_STATUS", rs.getString(("AE_STATUS")));
				dataMap.put("adv_Sdate", rs.getString(("adv_Sdate")));
				dataMap.put("adv_Edate", rs.getString(("adv_Edate")));
				dataMap.put("adv_Id", rs.getString(("adv_Id")));
				dataMap.put("create_on", rs.getString(("create_on")));
				dataMap.put("last_modified_date", rs.getString(("last_modified_date")));
				dataMap.put("statid", rs.getString(("statid")));
				dataMap.put("pk_formquery", rs.getString(("pk_formquery")));
				dataMap.put("field_id", rs.getString(("field_id")));
				dataMap.put("editboxPlaceHolder", "");
				dataMap.put("response_id", rs.getString(("adv_Id")));
				/*dataMap.put("auditboxPlaceHolder", "");
				dataMap.put("trackChangesPlaceHolder", "");*/
				dataMap.put("checkboxPlaceHolder", "");
				dataMap.put("flg_color", "red");
				dataMap.put("sub_type",rs.getString(("sub_type")));
				dataMap.put("sortCol", sortCol);
				dataMap.put("sortDir", sortDir);
				maplist.add(dataMap);
				//rowcount++;
			}
				}
				if(null != qryStatusOrange && !qryStatusOrange.equals("")) {
					if(rs.getString("setcolor").equals("orange"))
					{
						dataMap.put("rows", rowcount);
						dataMap.put("query_status", "-");
						dataMap.put("status_days", "0");
						dataMap.put("total_queries", rs.getString(("total_queries")));
						dataMap.put("pk_patprot", rs.getString(("pk_patprot")));
						dataMap.put("fk_per", rs.getString(("fk_per")));
						dataMap.put("pk_study", rs.getString(("pk_study")));
						dataMap.put("adv_type", rs.getString(("adv_Type")));
						dataMap.put("adv_grade", rs.getString(("adv_Grade")));
						dataMap.put("query_status1", rs.getString(("query_status1")));
						dataMap.put("days_open",rs.getString(("days_open")));
						//dataMap.put("target_days",rs.getInt(("target_days")));
						dataMap.put("ae_treatment_course",rs.getString(("ae_treatment_course")));
						dataMap.put("studynum", rs.getString(("studyNum")));
						dataMap.put("organization", rs.getString(("organization")));
						dataMap.put("patstdid", rs.getString(("patStdId")));
						dataMap.put("adv_cat", rs.getString(("adv_Cat")));
						dataMap.put("AE_TOXICITY", rs.getString(("AE_TOXICITY")));
						dataMap.put("meddra_code", rs.getString(("meddra_code")));
						dataMap.put("DICTIONARY", rs.getString(("DICTIONARY")));
						dataMap.put("AE_DISCVRYDATE", rs.getString(("AE_DISCVRYDATE")));
						dataMap.put("AE_LOGGEDDATE", rs.getString(("AE_LOGGEDDATE")));
						dataMap.put("entered_by", rs.getString(("entered_by")));
						dataMap.put("reported_by", rs.getString(("reported_by")));
						dataMap.put("AE_OUTTYPE", rs.getString(("AE_OUTTYPE")));
						dataMap.put("attribution", rs.getString(("attribution")));
						dataMap.put("action", rs.getString(("action")));
						dataMap.put("AE_OUTNOTES", rs.getString(("AE_OUTNOTES")));
						dataMap.put("AE_RECOVERY_DESC", rs.getString(("AE_RECOVERY_DESC")));
						dataMap.put("AE_STATUS", rs.getString(("AE_STATUS")));
						dataMap.put("adv_Sdate", rs.getString(("adv_Sdate")));
						dataMap.put("adv_Edate", rs.getString(("adv_Edate")));
						dataMap.put("adv_Id", rs.getString(("adv_Id")));
						dataMap.put("create_on", rs.getString(("create_on")));
						dataMap.put("last_modified_date", rs.getString(("last_modified_date")));
						dataMap.put("statid", rs.getString(("statid")));
						dataMap.put("pk_formquery", rs.getString(("pk_formquery")));
						dataMap.put("field_id", rs.getString(("field_id")));
						dataMap.put("editboxPlaceHolder", "");
						dataMap.put("response_id", rs.getString(("adv_Id")));
						/*dataMap.put("auditboxPlaceHolder", "");
						dataMap.put("trackChangesPlaceHolder", "");*/
						dataMap.put("checkboxPlaceHolder", "");
						dataMap.put("flg_color", "orange");
						dataMap.put("sub_type",rs.getString(("sub_type")));
						dataMap.put("sortCol", sortCol);
						dataMap.put("sortDir", sortDir);
						maplist.add(dataMap);	
					}
				}
				if(null != qryStatusYellow && !qryStatusYellow.equals("") ){
					if(rs.getString("setcolor").equals("yellow"))
					{
						dataMap.put("rows", rowcount);
						dataMap.put("query_status", "-");
						dataMap.put("status_days", "0");
						dataMap.put("total_queries", rs.getString(("total_queries")));
						dataMap.put("pk_patprot", rs.getString(("pk_patprot")));
						dataMap.put("fk_per", rs.getString(("fk_per")));
						dataMap.put("pk_study", rs.getString(("pk_study")));
						dataMap.put("adv_type", rs.getString(("adv_Type")));
						dataMap.put("adv_grade", rs.getString(("adv_Grade")));
						dataMap.put("query_status1", rs.getString(("query_status1")));
						dataMap.put("days_open",rs.getString(("days_open")));
						//dataMap.put("target_days",rs.getInt(("target_days")));
						dataMap.put("ae_treatment_course",rs.getString(("ae_treatment_course")));
						dataMap.put("studynum", rs.getString(("studyNum")));
						dataMap.put("organization", rs.getString(("organization")));
						dataMap.put("patstdid", rs.getString(("patStdId")));
						dataMap.put("adv_cat", rs.getString(("adv_Cat")));
						dataMap.put("AE_TOXICITY", rs.getString(("AE_TOXICITY")));
						dataMap.put("meddra_code", rs.getString(("meddra_code")));
						dataMap.put("DICTIONARY", rs.getString(("DICTIONARY")));
						dataMap.put("AE_DISCVRYDATE", rs.getString(("AE_DISCVRYDATE")));
						dataMap.put("AE_LOGGEDDATE", rs.getString(("AE_LOGGEDDATE")));
						dataMap.put("entered_by", rs.getString(("entered_by")));
						dataMap.put("reported_by", rs.getString(("reported_by")));
						dataMap.put("AE_OUTTYPE", rs.getString(("AE_OUTTYPE")));
						dataMap.put("attribution", rs.getString(("attribution")));
						dataMap.put("action", rs.getString(("action")));
						dataMap.put("AE_OUTNOTES", rs.getString(("AE_OUTNOTES")));
						dataMap.put("AE_RECOVERY_DESC", rs.getString(("AE_RECOVERY_DESC")));
						dataMap.put("AE_STATUS", rs.getString(("AE_STATUS")));
						dataMap.put("adv_Sdate", rs.getString(("adv_Sdate")));
						dataMap.put("adv_Edate", rs.getString(("adv_Edate")));
						dataMap.put("adv_Id", rs.getString(("adv_Id")));
						dataMap.put("create_on", rs.getString(("create_on")));
						dataMap.put("last_modified_date", rs.getString(("last_modified_date")));
						dataMap.put("statid", rs.getString(("statid")));
						dataMap.put("pk_formquery", rs.getString(("pk_formquery")));
						dataMap.put("field_id", rs.getString(("field_id")));
						dataMap.put("editboxPlaceHolder", "");
						dataMap.put("response_id", rs.getString(("adv_Id")));
						/*dataMap.put("auditboxPlaceHolder", "");
						dataMap.put("trackChangesPlaceHolder", "");*/
						dataMap.put("checkboxPlaceHolder", "");
						dataMap.put("flg_color", "yellow");
						dataMap.put("sub_type",rs.getString(("sub_type")));
						dataMap.put("sortCol", sortCol);
						dataMap.put("sortDir", sortDir);
						maplist.add(dataMap);
					}
				}
				if(null != qryStatusPurple && !qryStatusPurple.equals(""))
				{ 
					if(rs.getString("setcolor").equals("purple"))
					{
						dataMap.put("rows", rowcount);
						dataMap.put("query_status", "-");
						dataMap.put("status_days", "0");
						dataMap.put("total_queries", rs.getString(("total_queries")));
						dataMap.put("pk_patprot", rs.getString(("pk_patprot")));
						dataMap.put("fk_per", rs.getString(("fk_per")));
						dataMap.put("pk_study", rs.getString(("pk_study")));
						dataMap.put("adv_type", rs.getString(("adv_Type")));
						dataMap.put("adv_grade", rs.getString(("adv_Grade")));
						dataMap.put("query_status1", rs.getString(("query_status1")));
						dataMap.put("days_open",rs.getString(("days_open")));
						//dataMap.put("target_days",rs.getInt(("target_days")));
						dataMap.put("ae_treatment_course",rs.getString(("ae_treatment_course")));
						dataMap.put("studynum", rs.getString(("studyNum")));
						dataMap.put("organization", rs.getString(("organization")));
						dataMap.put("patstdid", rs.getString(("patStdId")));
						dataMap.put("adv_cat", rs.getString(("adv_Cat")));
						dataMap.put("AE_TOXICITY", rs.getString(("AE_TOXICITY")));
						dataMap.put("meddra_code", rs.getString(("meddra_code")));
						dataMap.put("DICTIONARY", rs.getString(("DICTIONARY")));
						dataMap.put("AE_DISCVRYDATE", rs.getString(("AE_DISCVRYDATE")));
						dataMap.put("AE_LOGGEDDATE", rs.getString(("AE_LOGGEDDATE")));
						dataMap.put("entered_by", rs.getString(("entered_by")));
						dataMap.put("reported_by", rs.getString(("reported_by")));
						dataMap.put("AE_OUTTYPE", rs.getString(("AE_OUTTYPE")));
						dataMap.put("attribution", rs.getString(("attribution")));
						dataMap.put("action", rs.getString(("action")));
						dataMap.put("AE_OUTNOTES", rs.getString(("AE_OUTNOTES")));
						dataMap.put("AE_RECOVERY_DESC", rs.getString(("AE_RECOVERY_DESC")));
						dataMap.put("AE_STATUS", rs.getString(("AE_STATUS")));
						dataMap.put("adv_Sdate", rs.getString(("adv_Sdate")));
						dataMap.put("adv_Edate", rs.getString(("adv_Edate")));
						dataMap.put("adv_Id", rs.getString(("adv_Id")));
						dataMap.put("create_on", rs.getString(("create_on")));
						dataMap.put("last_modified_date", rs.getString(("last_modified_date")));
						dataMap.put("statid", rs.getString(("statid")));
						dataMap.put("pk_formquery", rs.getString(("pk_formquery")));
						dataMap.put("field_id", rs.getString(("field_id")));
						dataMap.put("editboxPlaceHolder", "");
						dataMap.put("response_id", rs.getString(("adv_Id")));
						/*dataMap.put("auditboxPlaceHolder", "");
						dataMap.put("trackChangesPlaceHolder", "");*/
						dataMap.put("checkboxPlaceHolder", "");
						dataMap.put("flg_color", "purple");
						dataMap.put("sub_type",rs.getString(("sub_type")));
						dataMap.put("sortCol", sortCol);
						dataMap.put("sortDir", sortDir);
						maplist.add(dataMap);
					}
				}
				if(null != qryStatusWhite && !qryStatusWhite.equals(""))
				{ 
					if(rs.getString("setcolor").equals("white"))
					{
						dataMap.put("rows", rowcount);
						dataMap.put("query_status", "-");
						dataMap.put("status_days", "0");
						dataMap.put("total_queries",rs.getString(("total_queries")));
						dataMap.put("pk_patprot", rs.getString(("pk_patprot")));
						dataMap.put("fk_per", rs.getString(("fk_per")));
						dataMap.put("pk_study", rs.getString(("pk_study")));
						dataMap.put("adv_type", rs.getString(("adv_Type")));
						dataMap.put("adv_grade", rs.getString(("adv_Grade")));
						dataMap.put("query_status1", rs.getString(("query_status1")));
						dataMap.put("days_open",rs.getString(("days_open")));
						//dataMap.put("target_days",rs.getInt(("target_days")));
						dataMap.put("ae_treatment_course",rs.getString(("ae_treatment_course")));
						dataMap.put("studynum", rs.getString(("studyNum")));
						dataMap.put("organization", rs.getString(("organization")));
						dataMap.put("patstdid", rs.getString(("patStdId")));
						dataMap.put("adv_cat", rs.getString(("adv_Cat")));
						dataMap.put("AE_TOXICITY", rs.getString(("AE_TOXICITY")));
						dataMap.put("meddra_code", rs.getString(("meddra_code")));
						dataMap.put("DICTIONARY", rs.getString(("DICTIONARY")));
						dataMap.put("AE_DISCVRYDATE", rs.getString(("AE_DISCVRYDATE")));
						dataMap.put("AE_LOGGEDDATE", rs.getString(("AE_LOGGEDDATE")));
						dataMap.put("entered_by", rs.getString(("entered_by")));
						dataMap.put("reported_by", rs.getString(("reported_by")));
						dataMap.put("AE_OUTTYPE", rs.getString(("AE_OUTTYPE")));
						dataMap.put("attribution", rs.getString(("attribution")));
						dataMap.put("action", rs.getString(("action")));
						dataMap.put("AE_OUTNOTES", rs.getString(("AE_OUTNOTES")));
						dataMap.put("AE_RECOVERY_DESC", rs.getString(("AE_RECOVERY_DESC")));
						dataMap.put("AE_STATUS", rs.getString(("AE_STATUS")));
						dataMap.put("adv_Sdate", rs.getString(("adv_Sdate")));
						dataMap.put("adv_Edate", rs.getString(("adv_Edate")));
						dataMap.put("adv_Id", rs.getString(("adv_Id")));
						dataMap.put("create_on", rs.getString(("create_on")));
						dataMap.put("last_modified_date", rs.getString(("last_modified_date")));
						dataMap.put("statid", rs.getString(("statid")));
						dataMap.put("pk_formquery", rs.getString(("pk_formquery")));
						dataMap.put("field_id", rs.getString(("field_id")));
						dataMap.put("editboxPlaceHolder", "");
						dataMap.put("response_id", rs.getString(("adv_Id")));
						/*dataMap.put("auditboxPlaceHolder", "");
						dataMap.put("trackChangesPlaceHolder", "");*/
						dataMap.put("checkboxPlaceHolder", "");
						dataMap.put("flg_color", "white");
						dataMap.put("sub_type",rs.getString(("sub_type")));
						dataMap.put("sortCol", sortCol);
						dataMap.put("sortDir", sortDir);
						maplist.add(dataMap);	
					}
				}
				if(null != qryStatusGreen && !qryStatusGreen.equals("")){
					if(rs.getString("setcolor").equals("green"))
					{ 
						dataMap.put("rows", rowcount);
						dataMap.put("query_status", "-");
						dataMap.put("status_days", "0");
						dataMap.put("total_queries", rs.getString(("total_queries")));
						dataMap.put("pk_patprot", rs.getString(("pk_patprot")));
						dataMap.put("fk_per", rs.getString(("fk_per")));
						dataMap.put("pk_study", rs.getString(("pk_study")));
						dataMap.put("adv_type", rs.getString(("adv_Type")));
						dataMap.put("adv_grade", rs.getString(("adv_Grade")));
						dataMap.put("query_status1", rs.getString(("query_status1")));
						dataMap.put("days_open",rs.getString(("days_open")));
						//dataMap.put("target_days",rs.getInt(("target_days")));
						dataMap.put("ae_treatment_course",rs.getString(("ae_treatment_course")));
						dataMap.put("studynum", rs.getString(("studyNum")));
						dataMap.put("organization", rs.getString(("organization")));
						dataMap.put("patstdid", rs.getString(("patStdId")));
						dataMap.put("adv_cat", rs.getString(("adv_Cat")));
						dataMap.put("AE_TOXICITY", rs.getString(("AE_TOXICITY")));
						dataMap.put("meddra_code", rs.getString(("meddra_code")));
						dataMap.put("DICTIONARY", rs.getString(("DICTIONARY")));
						dataMap.put("AE_DISCVRYDATE", rs.getString(("AE_DISCVRYDATE")));
						dataMap.put("AE_LOGGEDDATE", rs.getString(("AE_LOGGEDDATE")));
						dataMap.put("entered_by", rs.getString(("entered_by")));
						dataMap.put("reported_by", rs.getString(("reported_by")));
						dataMap.put("AE_OUTTYPE", rs.getString(("AE_OUTTYPE")));
						dataMap.put("attribution", rs.getString(("attribution")));
						dataMap.put("action", rs.getString(("action")));
						dataMap.put("AE_OUTNOTES", rs.getString(("AE_OUTNOTES")));
						dataMap.put("AE_RECOVERY_DESC", rs.getString(("AE_RECOVERY_DESC")));
						dataMap.put("AE_STATUS", rs.getString(("AE_STATUS")));
						dataMap.put("adv_Sdate", rs.getString(("adv_Sdate")));
						dataMap.put("adv_Edate", rs.getString(("adv_Edate")));
						dataMap.put("adv_Id", rs.getString(("adv_Id")));
						dataMap.put("create_on", rs.getString(("create_on")));
						dataMap.put("last_modified_date", rs.getString(("last_modified_date")));
						dataMap.put("statid", rs.getString(("statid")));
						dataMap.put("pk_formquery", rs.getString(("pk_formquery")));
						dataMap.put("field_id", rs.getString(("field_id")));
						dataMap.put("editboxPlaceHolder", "");
						dataMap.put("response_id", rs.getString(("adv_Id")));
						/*dataMap.put("auditboxPlaceHolder", "");
						dataMap.put("trackChangesPlaceHolder", "");*/
						dataMap.put("checkboxPlaceHolder", "");
						dataMap.put("flg_color", "green");
						dataMap.put("sub_type",rs.getString(("sub_type")));
						dataMap.put("sortCol", sortCol);
						dataMap.put("sortDir", sortDir);
						maplist.add(dataMap);
					}
				}
				else if(noFilter.equalsIgnoreCase("true")) 
				{
					dataMap.put("rows", rowcount);
					dataMap.put("query_status", "-");
					dataMap.put("status_days", "0");
					dataMap.put("total_queries", rs.getString(("total_queries")));
					dataMap.put("pk_patprot", rs.getString(("pk_patprot")));
					dataMap.put("fk_per", rs.getString(("fk_per")));
					dataMap.put("pk_study", rs.getString(("pk_study")));
					dataMap.put("adv_type", rs.getString(("adv_Type")));
					dataMap.put("adv_grade", rs.getString(("adv_Grade")));
					dataMap.put("query_status1", rs.getString(("query_status1")));
					dataMap.put("days_open",rs.getString(("days_open")));
					//dataMap.put("target_days",rs.getInt(("target_days")));
					dataMap.put("ae_treatment_course",rs.getString(("ae_treatment_course")));
					dataMap.put("studynum", rs.getString(("studyNum")));
					dataMap.put("organization", rs.getString(("organization")));
					dataMap.put("patstdid", rs.getString(("patStdId")));
					dataMap.put("adv_cat", rs.getString(("adv_Cat")));
					dataMap.put("AE_TOXICITY", rs.getString(("AE_TOXICITY")));
					dataMap.put("meddra_code", rs.getString(("meddra_code")));
					dataMap.put("DICTIONARY", rs.getString(("DICTIONARY")));
					dataMap.put("AE_DISCVRYDATE", rs.getString(("AE_DISCVRYDATE")));
					dataMap.put("AE_LOGGEDDATE", rs.getString(("AE_LOGGEDDATE")));
					dataMap.put("entered_by", rs.getString(("entered_by")));
					dataMap.put("reported_by", rs.getString(("reported_by")));
					dataMap.put("AE_OUTTYPE", rs.getString(("AE_OUTTYPE")));
					dataMap.put("attribution", rs.getString(("attribution")));
					dataMap.put("action", rs.getString(("action")));
					dataMap.put("AE_OUTNOTES", rs.getString(("AE_OUTNOTES")));
					dataMap.put("AE_RECOVERY_DESC", rs.getString(("AE_RECOVERY_DESC")));
					dataMap.put("AE_STATUS", rs.getString(("AE_STATUS")));
					dataMap.put("adv_Sdate", rs.getString(("adv_Sdate")));
					dataMap.put("adv_Edate", rs.getString(("adv_Edate")));
					dataMap.put("adv_Id", rs.getString(("adv_Id")));
					dataMap.put("create_on", rs.getString(("create_on")));
					dataMap.put("last_modified_date", rs.getString(("last_modified_date")));
					dataMap.put("statid", rs.getString(("statid")));
					dataMap.put("pk_formquery", rs.getString(("pk_formquery")));
					dataMap.put("field_id", rs.getString(("field_id")));
					dataMap.put("editboxPlaceHolder", "");
					dataMap.put("response_id", rs.getString(("adv_Id")));					
					/*dataMap.put("auditboxPlaceHolder", "");
					dataMap.put("trackChangesPlaceHolder", "");*/
					dataMap.put("checkboxPlaceHolder", "");
					dataMap.put("sub_type",rs.getString(("sub_type")));
					dataMap.put("sortCol", sortCol);
					dataMap.put("sortDir", sortDir);
					if(rs.getString("setcolor").equals("green"))
					{
						dataMap.put("flg_color", "green");
						//System.out.println("green color flag for===>>>"+rs.getString("query_type"));
					}
					else if(rs.getString("setcolor").equals("red"))
						//if((rs.getString("query_status").equalsIgnoreCase("Open") || rs.getString("query_status").equalsIgnoreCase("Re-opened")) && rs.getString("query_type").equalsIgnoreCase("High Priority"))
					{
						dataMap.put("flg_color","red");
						//System.out.println("red color flag for===>>>"+rs.getString("query_type"));
					}
					
					 else if(rs.getString("setcolor").equals("purple"))
						//if((rs.getString("query_status").equalsIgnoreCase("Re-opened") || rs.getString("query_status").equalsIgnoreCase("Open")) && rs.getString("query_type").equalsIgnoreCase("Normal"))
					{
						dataMap.put("flg_color", "purple");
						//System.out.println("purple color flag for===>>>"+rs.getString("query_type"));
					}
					else if(rs.getString("setcolor").equals("orange")) 
						//if((rs.getString("query_status").equalsIgnoreCase("Ready for Monitor Review")) && rs.getString("query_type").equalsIgnoreCase("High Priority"))
					{
						dataMap.put("flg_color", "orange");
						//System.out.println("orange color flag for===>>>"+rs.getString("query_type"));
					}
					else if(rs.getString("setcolor").equals("yellow")) 
						//if((rs.getString("query_status").equalsIgnoreCase("Ready for Monitor Review")) && rs.getString("query_type").equalsIgnoreCase("Normal"))
					{
						dataMap.put("flg_color", "yellow");
						//System.out.println("yellow color flag for===>>>"+rs.getString("query_type"));
					}
					else if(rs.getString("setcolor").equals("white")) 
						//if((rs.getString("query_status").equalsIgnoreCase("-")) && rs.getString("query_type").equalsIgnoreCase("-") && rs.getString("total_queries").equalsIgnoreCase("0"))
					{
						dataMap.put("flg_color", "white");
						//System.out.println("white color flag for default===>>>"+rs.getString("query_type"));
					}
					else
					{   //when no match condition found
						dataMap.put("flg_color", "");
					}
										
					maplist.add(dataMap);	
				}
				}
			if(maplist.size()!=0){
				maplist.get(0).put("rows", rowcount);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return maplist;

	}
	
	
	public ArrayList<HashMap<String, Object>> fetchPatStdStatesData(
			HashMap<String, String> paramMap, int dispLength, int dispStart, int sortCol, String sortDir) {

		String accountId = paramMap.get("accountId");
		String sSearch=paramMap.get("sSearch");
		String userId = paramMap.get("userId");
		String studyIds = paramMap.get("studyIds");
		String formIds = paramMap.get("formId");
		String orgIds  = paramMap.get("orgIds");
		String dataEntryDateFrom = paramMap.get("dataEntryDateFrom");
		String dataEntryDateTo = paramMap.get("dataEntryDateTo");
		String patientIds = paramMap.get("patientId");
		String patientStdIds = paramMap.get("patientStdId");
		String queryCreatorIds = paramMap.get("queryCreatorId");
		String selectedTotalRespQuery = paramMap.get("selectedTotalRespQuery");
		String selectedqueryStatuess = paramMap.get("selectedqueryStatuess");
		String queryStatusIds = paramMap.get("queryStatusId");
		String noFilter = paramMap.get("noFilter");
		String qryStatusRed = paramMap.get("qryStatusRed");
		String qryStatusOrange = paramMap.get("qryStatusOrange");
		String qryStatusYellow = paramMap.get("qryStatusYellow");
		String qryStatusPurple = paramMap.get("qryStatusPurple");
		String qryStatusWhite = paramMap.get("qryStatusWhite");
		String qryStatusGreen = paramMap.get("qryStatusGreen");
		int firstRec = dispStart;
		int LastRec = dispLength+dispStart;
		//if(sortCol>2)
		//sortCol = sortCol-2;
		

		//Bug id #21071 fixed by Rashi Starts
		StringBuffer sqlBuffer = new StringBuffer();
		sqlBuffer.append("select setColor,total_queries,DECODE(aa.FK_CODELST_QUERYSTATUS,(SELECT pk_codelst FROM er_codelst WHERE codelst_type  ='query_status' AND codelst_subtyp  ='closed'),'(No Aged Query)', NVL((SELECT codelst_desc FROM er_codelst WHERE PK_CODELST = aa.FK_CODELST_QUERYSTATUS),'-')) query_status1,(CASE WHEN days_open IS NULL THEN 0 ELSE days_open END) days_open, study_number,Org,PATSTDID,Stat,reason_desc,PATSTUDYSTAT_DATE,CURRENT_STAT,created_on,entered_by,last_modified_date,last_modified_by,fk_per,statid,pk_study,(SELECT MIN (pk_formquery) FROM er_formquery  WHERE fk_querymodule=aa.statid and querymodule_linkedto=9 ) AS pk_formquery,(SELECT fk_field  FROM er_formquery WHERE pk_formquery=(SELECT MIN (pk_formquery)  FROM er_formquery WHERE fk_querymodule=aa.statid and querymodule_linkedto=9) ) AS field_id,View_right,site_id from (SELECT eres.f_getcolorflag(ep.pk_patstudystat,pk_study,9) AS setColor,study_number,  (SELECT pk_site FROM er_site WHERE epa.fk_site_enrolling = pk_site) AS site_id,(SELECT site_name FROM er_site WHERE epa.fk_site_enrolling = pk_site) as Org,PATPROT_PATSTDID AS PATSTDID,er_codelst.codelst_desc as Stat,(SELECT codelst_desc FROM er_codelst WHERE pk_codelst = patstudystat_reason) reason_desc,TO_CHAR(PATSTUDYSTAT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT) as PATSTUDYSTAT_DATE,DECODE(ep.CURRENT_STAT,1,'Y','N') as CURRENT_STAT,TO_CHAR(ep.created_on, PKG_DATEUTIL.F_GET_DATEFORMAT) AS created_on,(select USR_FIRSTNAME||', '||USR_LASTNAME from er_user where pk_user=ep.creator) AS entered_by,TO_CHAR(ep.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT) AS last_modified_date,(select USR_FIRSTNAME||', '||USR_LASTNAME from er_user where pk_user=ep.last_modified_by) AS last_modified_by,epa.fk_per AS fk_per,ep.pk_patstudystat AS statid,pk_study,(SELECT COUNT(*)FROM ER_FORMQUERY WHERE FK_QUERYMODULE =   ep.pk_patstudystat and querymodule_linkedto=9) total_queries, NVL((SELECT FK_CODELST_QUERYSTATUS FROM ER_FORMQUERYSTATUS WHERE PK_FORMQUERYSTATUS=h.PK_FORMQUERYSTATUS),DECODE((SELECT COUNT(*) FROM ER_FORMQUERY  WHERE FK_QUERYMODULE    = ep.pk_patstudystat  AND querymodule_linkedto=9),0,0,(SELECT pk_codelst  FROM er_codelst  WHERE codelst_type  ='query_status' AND codelst_subtyp  ='closed'))) FK_CODELST_QUERYSTATUS, (SELECT TRUNC(sysdate) - to_date(min(ENTERED_ON)) FROM ER_FORMQUERYSTATUS WHERE FK_FORMQUERY=h.PK_FORMQUERY ) days_open, (SELECT user_study_site_rights FROM er_study_site_rights WHERE fk_study=pk_study AND fk_site   =(SELECT pk_site FROM er_site WHERE epa.fk_site_enrolling = pk_site) AND fk_user   = "+userId+") AS View_right ");
		sqlBuffer.append("FROM  er_patstudystat ep, er_Study, er_patprot epa, er_codelst,(select a.*,max(b.PK_FORMQUERYSTATUS) PK_FORMQUERYSTATUS from er_formquerystatus b,(select min(PK_FORMQUERY) PK_FORMQUERY, FK_QUERYMODULE,QUERYMODULE_LINKEDTO from er_formquery fq	where (SELECT COUNT(*) FROM er_formquerystatus fqs1 WHERE fqs1.fk_formquery = fq.PK_FORMQUERY AND fqs1.PK_FORMQUERYSTATUS=(SELECT MAX(PK_FORMQUERYSTATUS) FROM er_formquerystatus fqs2 WHERE fqs2.fk_formquery = fqs1.fk_formquery) AND fqs1.fk_codelst_querystatus=(SELECT pk_codelst FROM er_codelst WHERE codelst_type  ='query_status' AND codelst_subtyp  ='closed'))=0  And QUERYMODULE_LINKEDTO=9	group by FK_QUERYMODULE,QUERYMODULE_LINKEDTO) a where a.PK_FORMQUERY=b.FK_FORMQUERY group by PK_FORMQUERY,FK_QUERYMODULE,QUERYMODULE_LINKEDTO) h where er_study.FK_ACCOUNT = "+accountId+" AND  FK_CODELST_STAT = pk_codelst and pk_study = ep.fk_study and pk_study = epa.fk_study  and h.FK_QUERYMODULE(+)= pk_patstudystat and  h.QUERYMODULE_LINKEDTO(+) =9 and epa.fk_per = ep.fk_per and epa.patprot_stat = '1' ");
		
		
		//Bug id #21071 Ends
		if(!"".equalsIgnoreCase(studyIds))
			sqlBuffer.append(" AND pk_study  IN ("+studyIds+") ");
		if(!"".equalsIgnoreCase(orgIds))
			sqlBuffer.append(" AND epa.fk_site_enrolling  IN ("+orgIds+") ");
		if(!"".equalsIgnoreCase(formIds))
			sqlBuffer.append("  AND ep.fk_codelst_stat IN ("+ formIds+") ");
		if(!"".equalsIgnoreCase(dataEntryDateFrom) && "".equalsIgnoreCase(dataEntryDateTo))
			sqlBuffer.append(" and PATSTUDYSTAT_DATE between TO_DATE('"+dataEntryDateFrom+"',PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE(PKG_DATEUTIL.f_get_future_null_date_str,PKG_DATEUTIL.F_GET_DATEFORMAT) ");
		if("".equalsIgnoreCase(dataEntryDateFrom) && !"".equalsIgnoreCase(dataEntryDateTo))
			sqlBuffer.append(" and PATSTUDYSTAT_DATE between TO_DATE(PKG_DATEUTIL.f_get_null_date_str,PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE('"+dataEntryDateTo+"',PKG_DATEUTIL.F_GET_DATEFORMAT) ");
		if(!"".equalsIgnoreCase(dataEntryDateFrom) && !"".equalsIgnoreCase(dataEntryDateTo))
			sqlBuffer.append(" and PATSTUDYSTAT_DATE between TO_DATE('"+dataEntryDateFrom+"',PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE('"+dataEntryDateTo+"',PKG_DATEUTIL.F_GET_DATEFORMAT) ");
		if(!"".equalsIgnoreCase(patientIds))
			sqlBuffer.append(" AND epa.fk_per  IN ("+patientIds+") ");
		if(!"".equalsIgnoreCase(patientStdIds))
			sqlBuffer.append(" AND epa.fk_per  IN ("+patientStdIds+") ");
		if(!"".equalsIgnoreCase(queryCreatorIds))
			sqlBuffer.append(" AND ep.creator  IN ("+queryCreatorIds+") ");
		sqlBuffer.append(")aa where (view_right is null or view_right=1)");
		
		if(!"".equalsIgnoreCase(queryStatusIds))
			sqlBuffer.append(" AND FK_CODELST_QUERYSTATUS  IN ("+queryStatusIds+") ");
		if(!"".equalsIgnoreCase(selectedqueryStatuess))
			sqlBuffer.append(" and days_open  >("+selectedqueryStatuess+") ");
		if(!"".equalsIgnoreCase(selectedTotalRespQuery))
			sqlBuffer.append(" and total_queries  >("+selectedTotalRespQuery+") ");
		boolean andFlag =false;
		String whereClause="";
		if(null != qryStatusRed && !qryStatusRed.equals("")){
			whereClause+="  setcolor='red' or";
			andFlag=true;
		}
		if(null != qryStatusOrange && !qryStatusOrange.equals("")){
			whereClause+=" setcolor='orange' or";
			andFlag=true;
		}
		if(null != qryStatusYellow && !qryStatusYellow.equals("")){
			whereClause+=" setcolor='yellow' or";
			andFlag=true;
		}
		if(null != qryStatusPurple && !qryStatusPurple.equals("")){
			whereClause+="  setcolor='purple' or";
			andFlag=true;
		}
		if(null != qryStatusWhite && !qryStatusWhite.equals("")){
			whereClause+="  setcolor='white' or";
			andFlag=true;
		}
		if(null != qryStatusGreen && !qryStatusGreen.equals("")){
			whereClause+="  setcolor='green' or";
			andFlag=true;
		}
		if(andFlag){
			whereClause=whereClause.substring(0,whereClause.length()-2);
			whereClause=	"  and ("+whereClause+")";
			}
		sqlBuffer.append(whereClause);
		if(sortCol==5)//Study Number
		{
			sqlBuffer.append("ORDER BY LOWER(study_number) "+sortDir+" ");
		}
		else if(sortCol==6)//Enrolling Site

		{
			sqlBuffer.append("ORDER BY LOWER(org) "+sortDir+" ");
		}
		else if(sortCol==7)//patstdid

		{
			sqlBuffer.append("ORDER BY LOWER(patstdid) "+sortDir+" ");
		}
		else if(sortCol==9)//Status Reason
		{
			sqlBuffer.append("ORDER BY LOWER(reason_desc) "+sortDir+" ");
		}
		else if(sortCol==10)//patstudystat_date
		{
			sqlBuffer.append("ORDER BY  TO_DATE(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) "+sortDir+" ");
		}
		else if(sortCol==12)//created_on
		{
			sqlBuffer.append("ORDER BY  TO_DATE(created_on,PKG_DATEUTIL.F_GET_DATEFORMAT) "+sortDir+" ");
		}
		else if(sortCol==13)//Entered By
		{
			sqlBuffer.append("ORDER BY LOWER(entered_by) "+sortDir+" ");
		}
		else if(sortCol==14)//last_modified_date
		{
			sqlBuffer.append("ORDER BY  TO_DATE(last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT) "+sortDir+" ");
		}
		else if(sortCol==15)//Last Modified By
		{			
				sqlBuffer.append("ORDER BY LOWER(last_modified_by) "+sortDir+" ");		
		}
		else{
		sqlBuffer.append(" ORDER BY "+sortCol+" "+sortDir);}
		
		String searchClause="";
		String searchingSql="(LOWER(total_queries) like LOWER('%"+sSearch+"%') or LOWER(query_status1) like LOWER('%"+sSearch+"%') or LOWER(days_open) like LOWER('%"+sSearch+"%')"
				+ " or LOWER(study_number) like LOWER('%"+sSearch+"%') or LOWER(Org) like LOWER('%"+sSearch+"%')"
				+ " or LOWER(PATSTDID) like LOWER('%"+sSearch+"%') or LOWER(Stat) like LOWER('%"+sSearch+"%') or LOWER(reason_desc) like LOWER('%"+sSearch+"%') "
				+ " or LOWER(PATSTUDYSTAT_DATE) like LOWER('%"+sSearch+"%') or LOWER(CURRENT_STAT) like LOWER('%"+sSearch+"%')"
				+ " or LOWER(created_on) like LOWER('%"+sSearch+"%')  or LOWER(entered_by) like LOWER('%"+sSearch+"%')  or LOWER(last_modified_date) like LOWER('%"+sSearch+"%') "
				+ " or lower(last_modified_by) like LOWER('%"+sSearch+"%'))" ;
		
		if(!sSearch.equals("")){
			searchClause="where "+searchingSql;	
		}
		
		StringBuffer sqlcomp = new StringBuffer();
		sqlcomp.append("select * from ( select a.*, rownum rnum FROM ("+sqlBuffer);
		//sqlcomp.append(") a WHERE ROWNUM <= "+LastRec+" ) WHERE rnum >"+firstRec);
		StringBuffer sqlcount = new StringBuffer();
		sqlcount.append("Select count(*) from ( select a1.* from ( " + sqlBuffer + " )a1 "+ searchClause+" )");
		ArrayList<HashMap<String, Object>> maplist = new ArrayList<HashMap<String, Object>>();
		HashMap<String, Object> dataMap = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		int rowcount=0;
		
		try {
			conn = CommonDAO.getConnection();
			pstmt = conn.prepareStatement(sqlcount.toString());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				rowcount = rs.getInt("count(*)");
			}
			if(LastRec==-1){LastRec=rowcount;}
			sqlcomp.append(") a WHERE ROWNUM <= "+LastRec+" and "+searchingSql+" ) WHERE rnum >"+firstRec);
			pstmt = conn.prepareStatement(sqlcomp.toString());
			rs = pstmt.executeQuery();

			while (rs.next()) {
				dataMap = new HashMap<String, Object>();
				if(null != qryStatusRed && !qryStatusRed.equals("")){
					if(rs.getString("setcolor").equals("red"))
					{
				dataMap.put("rows", rowcount);
				dataMap.put("Query_Status", "-");
				dataMap.put("Status_Days", "0");
				dataMap.put("Total_Queries", rs.getString(("total_queries")));
				dataMap.put("pk_study", rs.getString(("pk_study")));
				dataMap.put("query_status1", rs.getString(("query_status1")));
				dataMap.put("days_open",rs.getString(("days_open")));
				//dataMap.put("target_days",rs.getInt(("target_days")));
				dataMap.put("study_number", rs.getString(("study_number")));
				dataMap.put("fk_per", rs.getString(("fk_per")));
				dataMap.put("PATSTDID", rs.getString(("PATSTDID")));
				dataMap.put("statid", rs.getString(("statid")));
				dataMap.put("PATSTUDYSTAT_DATE", rs.getString(("PATSTUDYSTAT_DATE")));
				dataMap.put("created_on", rs.getString(("created_on")));
				dataMap.put("entered_by", rs.getString(("entered_by")));
				dataMap.put("last_modified_date", rs.getString(("last_modified_date")));
				dataMap.put("last_modified_by", rs.getString(("last_modified_by")));
				
				dataMap.put("Org", rs.getString(("Org")));
				dataMap.put("site_id", rs.getString(("site_id")));
				dataMap.put("Stat", rs.getString(("Stat")));
				
				dataMap.put("reason_desc", rs.getString(("reason_desc")));
				dataMap.put("CURRENT_STAT", rs.getString(("CURRENT_STAT")));
				dataMap.put("editboxPlaceHolder", "");
				//dataMap.put("checkboxPlaceHolder", "");
				dataMap.put("pk_formquery", rs.getString(("pk_formquery")));
				dataMap.put("field_id", rs.getString(("field_id")));
				dataMap.put("response_id", rs.getString(("statid")));
				dataMap.put("flg_color", "red");
				dataMap.put("sortCol", sortCol);
				dataMap.put("sortDir", sortDir);
				maplist.add(dataMap);
				//rowcount++;
					}
					}
				if(null != qryStatusOrange && !qryStatusOrange.equals("")) {
					if(rs.getString("setcolor").equals("orange"))
					{
						dataMap.put("rows", rowcount);
						dataMap.put("Query_Status", "-");
						dataMap.put("Status_Days", "0");
						dataMap.put("Total_Queries", rs.getString(("total_queries")));
						dataMap.put("pk_study", rs.getString(("pk_study")));
						dataMap.put("query_status1", rs.getString(("query_status1")));
						dataMap.put("days_open",rs.getString(("days_open")));
						//dataMap.put("target_days",rs.getInt(("target_days")));
						dataMap.put("study_number", rs.getString(("study_number")));
						dataMap.put("fk_per", rs.getString(("fk_per")));
						dataMap.put("PATSTDID", rs.getString(("PATSTDID")));
						dataMap.put("statid", rs.getString(("statid")));
						dataMap.put("PATSTUDYSTAT_DATE", rs.getString(("PATSTUDYSTAT_DATE")));
						dataMap.put("created_on", rs.getString(("created_on")));
						dataMap.put("entered_by", rs.getString(("entered_by")));
						dataMap.put("last_modified_date", rs.getString(("last_modified_date")));
						dataMap.put("last_modified_by", rs.getString(("last_modified_by")));
						
						dataMap.put("Org", rs.getString(("Org")));
						dataMap.put("site_id", rs.getString(("site_id")));
						dataMap.put("Stat", rs.getString(("Stat")));
						
						dataMap.put("reason_desc", rs.getString(("reason_desc")));
						dataMap.put("CURRENT_STAT", rs.getString(("CURRENT_STAT")));
						dataMap.put("editboxPlaceHolder", "");
						//dataMap.put("checkboxPlaceHolder", "");
						dataMap.put("pk_formquery", rs.getString(("pk_formquery")));
						dataMap.put("field_id", rs.getString(("field_id")));
						dataMap.put("response_id", rs.getString(("statid")));
						dataMap.put("flg_color", "orange");
						dataMap.put("sortCol", sortCol);
						dataMap.put("sortDir", sortDir);
						maplist.add(dataMap);	
					}
				}
				if(null != qryStatusYellow && !qryStatusYellow.equals("") ){
					if(rs.getString("setcolor").equals("yellow"))
					{
						dataMap.put("rows", rowcount);
						dataMap.put("Query_Status", "-");
						dataMap.put("Status_Days", "0");
						dataMap.put("Total_Queries", rs.getString(("total_queries")));
						dataMap.put("pk_study", rs.getString(("pk_study")));
						dataMap.put("query_status1", rs.getString(("query_status1")));
						dataMap.put("days_open",rs.getString(("days_open")));
						//dataMap.put("target_days",rs.getInt(("target_days")));
						dataMap.put("study_number", rs.getString(("study_number")));
						dataMap.put("fk_per", rs.getString(("fk_per")));
						dataMap.put("PATSTDID", rs.getString(("PATSTDID")));
						dataMap.put("statid", rs.getString(("statid")));
						dataMap.put("PATSTUDYSTAT_DATE", rs.getString(("PATSTUDYSTAT_DATE")));
						dataMap.put("created_on", rs.getString(("created_on")));
						dataMap.put("entered_by", rs.getString(("entered_by")));
						dataMap.put("last_modified_date", rs.getString(("last_modified_date")));
						dataMap.put("last_modified_by", rs.getString(("last_modified_by")));
						
						dataMap.put("Org", rs.getString(("Org")));
						dataMap.put("site_id", rs.getString(("site_id")));
						dataMap.put("Stat", rs.getString(("Stat")));
						
						dataMap.put("reason_desc", rs.getString(("reason_desc")));
						dataMap.put("CURRENT_STAT", rs.getString(("CURRENT_STAT")));
						dataMap.put("editboxPlaceHolder", "");
						//dataMap.put("checkboxPlaceHolder", "");
						dataMap.put("pk_formquery", rs.getString(("pk_formquery")));
						dataMap.put("field_id", rs.getString(("field_id")));
						dataMap.put("response_id", rs.getString(("statid")));
						dataMap.put("flg_color", "yellow");
						dataMap.put("sortCol", sortCol);
						dataMap.put("sortDir", sortDir);
						maplist.add(dataMap);	
					}
				}
				if(null != qryStatusPurple && !qryStatusPurple.equals(""))
				{ 
					if(rs.getString("setcolor").equals("purple"))
					{
						dataMap.put("rows", rowcount);
						dataMap.put("Query_Status", "-");
						dataMap.put("Status_Days", "0");
						dataMap.put("Total_Queries", rs.getString(("total_queries")));
						dataMap.put("pk_study", rs.getString(("pk_study")));
						dataMap.put("query_status1", rs.getString(("query_status1")));
						dataMap.put("days_open",rs.getString(("days_open")));
						//dataMap.put("target_days",rs.getInt(("target_days")));
						dataMap.put("study_number", rs.getString(("study_number")));
						dataMap.put("fk_per", rs.getString(("fk_per")));
						dataMap.put("PATSTDID", rs.getString(("PATSTDID")));
						dataMap.put("statid", rs.getString(("statid")));
						dataMap.put("PATSTUDYSTAT_DATE", rs.getString(("PATSTUDYSTAT_DATE")));
						dataMap.put("created_on", rs.getString(("created_on")));
						dataMap.put("entered_by", rs.getString(("entered_by")));
						dataMap.put("last_modified_date", rs.getString(("last_modified_date")));
						dataMap.put("last_modified_by", rs.getString(("last_modified_by")));
						
						dataMap.put("Org", rs.getString(("Org")));
						dataMap.put("site_id", rs.getString(("site_id")));
						dataMap.put("Stat", rs.getString(("Stat")));
						
						dataMap.put("reason_desc", rs.getString(("reason_desc")));
						dataMap.put("CURRENT_STAT", rs.getString(("CURRENT_STAT")));
						dataMap.put("editboxPlaceHolder", "");
						//dataMap.put("checkboxPlaceHolder", "");
						dataMap.put("pk_formquery", rs.getString(("pk_formquery")));
						dataMap.put("field_id", rs.getString(("field_id")));
						dataMap.put("response_id", rs.getString(("statid")));
						dataMap.put("flg_color", "purple");
						dataMap.put("sortCol", sortCol);
						dataMap.put("sortDir", sortDir);
						maplist.add(dataMap);
					}
				}
				if(null != qryStatusWhite && !qryStatusWhite.equals(""))
				{ 
					if(rs.getString("setcolor").equals("white"))
					{
						dataMap.put("rows", rowcount);
						dataMap.put("Query_Status", "-");
						dataMap.put("Status_Days", "0");
						dataMap.put("Total_Queries", rs.getString(("total_queries")));
						dataMap.put("pk_study", rs.getString(("pk_study")));
						dataMap.put("query_status1", rs.getString(("query_status1")));
						dataMap.put("days_open",rs.getString(("days_open")));
						//dataMap.put("target_days",rs.getInt(("target_days")));
						dataMap.put("study_number", rs.getString(("study_number")));
						dataMap.put("fk_per", rs.getString(("fk_per")));
						dataMap.put("PATSTDID", rs.getString(("PATSTDID")));
						dataMap.put("statid", rs.getString(("statid")));
						dataMap.put("PATSTUDYSTAT_DATE", rs.getString(("PATSTUDYSTAT_DATE")));
						dataMap.put("created_on", rs.getString(("created_on")));
						dataMap.put("entered_by", rs.getString(("entered_by")));
						dataMap.put("last_modified_date", rs.getString(("last_modified_date")));
						dataMap.put("last_modified_by", rs.getString(("last_modified_by")));
						
						dataMap.put("Org", rs.getString(("Org")));
						dataMap.put("site_id", rs.getString(("site_id")));
						dataMap.put("Stat", rs.getString(("Stat")));
						
						dataMap.put("reason_desc", rs.getString(("reason_desc")));
						dataMap.put("CURRENT_STAT", rs.getString(("CURRENT_STAT")));
						dataMap.put("editboxPlaceHolder", "");
						//dataMap.put("checkboxPlaceHolder", "");
						dataMap.put("pk_formquery", rs.getString(("pk_formquery")));
						dataMap.put("field_id", rs.getString(("field_id")));
						dataMap.put("response_id", rs.getString(("statid")));
						dataMap.put("flg_color", "white");
						dataMap.put("sortCol", sortCol);
						dataMap.put("sortDir", sortDir);
						maplist.add(dataMap);
					}
				}
				if(null != qryStatusGreen && !qryStatusGreen.equals("")){
					if(rs.getString("setcolor").equals("green"))
					{ 
						dataMap.put("rows", rowcount);
						dataMap.put("Query_Status", "-");
						dataMap.put("Status_Days", "0");
						dataMap.put("Total_Queries", rs.getString(("total_queries")));
						dataMap.put("pk_study", rs.getString(("pk_study")));
						dataMap.put("query_status1", rs.getString(("query_status1")));
						dataMap.put("days_open",rs.getString(("days_open")));
						//dataMap.put("target_days",rs.getInt(("target_days")));
						dataMap.put("study_number", rs.getString(("study_number")));
						dataMap.put("fk_per", rs.getString(("fk_per")));
						dataMap.put("PATSTDID", rs.getString(("PATSTDID")));
						dataMap.put("statid", rs.getString(("statid")));
						dataMap.put("PATSTUDYSTAT_DATE", rs.getString(("PATSTUDYSTAT_DATE")));
						dataMap.put("created_on", rs.getString(("created_on")));
						dataMap.put("entered_by", rs.getString(("entered_by")));
						dataMap.put("last_modified_date", rs.getString(("last_modified_date")));
						dataMap.put("last_modified_by", rs.getString(("last_modified_by")));
						
						dataMap.put("Org", rs.getString(("Org")));
						dataMap.put("site_id", rs.getString(("site_id")));
						dataMap.put("Stat", rs.getString(("Stat")));
						
						dataMap.put("reason_desc", rs.getString(("reason_desc")));
						dataMap.put("CURRENT_STAT", rs.getString(("CURRENT_STAT")));
						dataMap.put("editboxPlaceHolder", "");
						//dataMap.put("checkboxPlaceHolder", "");
						dataMap.put("pk_formquery", rs.getString(("pk_formquery")));
						dataMap.put("field_id", rs.getString(("field_id")));
						dataMap.put("response_id", rs.getString(("statid")));
						dataMap.put("flg_color", "green");
						dataMap.put("sortCol", sortCol);
						dataMap.put("sortDir", sortDir);
						maplist.add(dataMap);
					}
				}
				else if(noFilter.equalsIgnoreCase("true")) 
				{
					dataMap.put("rows", rowcount);
					dataMap.put("Query_Status", "-");
					dataMap.put("Status_Days", "0");
					dataMap.put("Total_Queries", rs.getString(("total_queries")));
					dataMap.put("pk_study", rs.getString(("pk_study")));
					dataMap.put("query_status1", rs.getString(("query_status1")));
					dataMap.put("days_open",rs.getString(("days_open")));
					//dataMap.put("target_days",rs.getInt(("target_days")));
					dataMap.put("study_number", rs.getString(("study_number")));
					dataMap.put("fk_per", rs.getString(("fk_per")));
					dataMap.put("PATSTDID", rs.getString(("PATSTDID")));
					dataMap.put("statid", rs.getString(("statid")));
					dataMap.put("PATSTUDYSTAT_DATE", rs.getString(("PATSTUDYSTAT_DATE")));
					dataMap.put("created_on", rs.getString(("created_on")));
					dataMap.put("entered_by", rs.getString(("entered_by")));
					dataMap.put("last_modified_date", rs.getString(("last_modified_date")));
					dataMap.put("last_modified_by", rs.getString(("last_modified_by")));
					
					dataMap.put("Org", rs.getString(("Org")));
					dataMap.put("site_id", rs.getString(("site_id")));
					dataMap.put("Stat", rs.getString(("Stat")));
					
					dataMap.put("reason_desc", rs.getString(("reason_desc")));
					dataMap.put("CURRENT_STAT", rs.getString(("CURRENT_STAT")));
					dataMap.put("editboxPlaceHolder", "");
					//dataMap.put("checkboxPlaceHolder", "");
					dataMap.put("pk_formquery", rs.getString(("pk_formquery")));
					dataMap.put("field_id", rs.getString(("field_id")));
					dataMap.put("response_id", rs.getString(("statid")));
					dataMap.put("sortCol", sortCol);
					dataMap.put("sortDir", sortDir);
					if(rs.getString("setcolor").equals("green"))
					{
						dataMap.put("flg_color", "green");
						//System.out.println("green color flag for===>>>"+rs.getString("query_type"));
					}
					else if(rs.getString("setcolor").equals("red"))
						//if((rs.getString("query_status").equalsIgnoreCase("Open") || rs.getString("query_status").equalsIgnoreCase("Re-opened")) && rs.getString("query_type").equalsIgnoreCase("High Priority"))
					{
						dataMap.put("flg_color","red");
						//System.out.println("red color flag for===>>>"+rs.getString("query_type"));
					}
					
					 else if(rs.getString("setcolor").equals("purple"))
						//if((rs.getString("query_status").equalsIgnoreCase("Re-opened") || rs.getString("query_status").equalsIgnoreCase("Open")) && rs.getString("query_type").equalsIgnoreCase("Normal"))
					{
						dataMap.put("flg_color", "purple");
						//System.out.println("purple color flag for===>>>"+rs.getString("query_type"));
					}
					else if(rs.getString("setcolor").equals("orange")) 
						//if((rs.getString("query_status").equalsIgnoreCase("Ready for Monitor Review")) && rs.getString("query_type").equalsIgnoreCase("High Priority"))
					{
						dataMap.put("flg_color", "orange");
						//System.out.println("orange color flag for===>>>"+rs.getString("query_type"));
					}
					else if(rs.getString("setcolor").equals("yellow")) 
						//if((rs.getString("query_status").equalsIgnoreCase("Ready for Monitor Review")) && rs.getString("query_type").equalsIgnoreCase("Normal"))
					{
						dataMap.put("flg_color", "yellow");
						//System.out.println("yellow color flag for===>>>"+rs.getString("query_type"));
					}
					else if(rs.getString("setcolor").equals("white")) 
						//if((rs.getString("query_status").equalsIgnoreCase("-")) && rs.getString("query_type").equalsIgnoreCase("-") && rs.getString("total_queries").equalsIgnoreCase("0"))
					{
						dataMap.put("flg_color", "white");
						//System.out.println("white color flag for default===>>>"+rs.getString("query_type"));
					}
					else
					{   //when no match condition found
						dataMap.put("flg_color", "");
					}
										
					maplist.add(dataMap);
				}
			}
			if(maplist.size()!=0){
				maplist.get(0).put("rows", rowcount);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return maplist;

	}	
	
	public ArrayList<HashMap<String, Object>> fetchPatDemographicsData(
			HashMap<String, String> paramMap, int dispLength, int dispStart, int sortCol, String sortDir) {
		 String accountId = paramMap.get("accountId");
		 String userId = paramMap.get("userId");
		 String orgIds  = paramMap.get("orgIds");
		 String dataEntryDateFrom = paramMap.get("dataEntryDateFrom");
		 String dataEntryDateTo = paramMap.get("dataEntryDateTo");
		 String patientId = paramMap.get("patientId");
		 String queryCreatorIds = paramMap.get("queryCreatorId");
		
		int firstRec = dispStart;
		int LastRec = dispLength+dispStart;
		if(sortCol>2)
		sortCol = sortCol-2;
		StringBuffer sqlBuffer = new StringBuffer();
		sqlBuffer.append("SELECT DISTINCT pkg_util.f_join(cursor(select study_number from er_study s, er_patprot p where s.pk_study =  p.fk_study and p.patprot_stat <>0 and p.fk_per = a.pk_person), ',') AS study_number,site.site_name organization,TO_CHAR(PERSON_REGDATE, PKG_DATEUTIL.F_GET_DATEFORMAT) regd_date, a.PERSON_CODE patient_id,a.PAT_FACILITYID facility_id, a.person_fname mask_pfname,a.person_lname mask_plname,TO_CHAR(a.person_dob,PKG_DATEUTIL.F_GET_DATEFORMAT) dob,(SELECT ER_CODELST.CODELST_DESC FROM ER_CODELST WHERE ER_CODELST.PK_CODELST=a.FK_CODELST_GENDER) Person_gender, ");
		sqlBuffer.append("TO_CHAR(a.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT) created_on,(select USR_FIRSTNAME||', '||USR_LASTNAME from er_user where pk_user=a.CREATOR) as CREATOR,TO_CHAR(a.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT) last_modified_date,(select USR_FIRSTNAME||', '||USR_LASTNAME from er_user where pk_user=a.LAST_MODIFIED_BY) as LAST_MODIFIED_BY, a.rowid AS rowcount,lower(PERSON_CODE) person_code_lower, a.person_code ,(SELECT COUNT(fk_study) FROM er_patprot e WHERE e.fk_per = a.PK_PERSON AND e.PATPROT_ENROLDT IS NOT NULL AND PATPROT_STAT = 1) COUNT, a.pk_person,floor(NVL(a.person_deathdt,sysdate) - a.person_dob) no_of_days_diff, ");
		sqlBuffer.append("DECODE(a.person_deathdt,NULL,pkg_util.f_datediff(a.person_dob),pkg_util.f_datediff(a.person_dob,a.person_deathdt)) mask_person_dob,(SELECT ER_CODELST.CODELST_DESC FROM ER_CODELST WHERE ER_CODELST.PK_CODELST=a.FK_CODELST_PSTAT) person_status, ");
		sqlBuffer.append("TO_CHAR( person_deathdt ,PKG_DATEUTIL.F_GET_DATEFORMAT) person_deathdt FROM epat.person a, ER_PATFACILITY fac, er_usersite usr, er_site site ");
		sqlBuffer.append("WHERE a.fk_account  ="+accountId+" AND fk_user  ="+userId+" AND usersite_right  >=4 AND pk_site  = a.fk_site AND usr.fk_site = fac.fk_site ");
		sqlBuffer.append("AND fac.patfacility_accessright > 0 AND fk_per = pk_person ");
		if(!"".equalsIgnoreCase(orgIds))
			sqlBuffer.append(" AND a.fk_site  IN ("+orgIds+") ");
		if(!"".equalsIgnoreCase(dataEntryDateFrom) && "".equalsIgnoreCase(dataEntryDateTo))
			sqlBuffer.append(" and PERSON_REGDATE between TO_DATE('"+dataEntryDateFrom+"',PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE(PKG_DATEUTIL.f_get_future_null_date_str,PKG_DATEUTIL.F_GET_DATEFORMAT) ");
		if("".equalsIgnoreCase(dataEntryDateFrom) && !"".equalsIgnoreCase(dataEntryDateTo))
			sqlBuffer.append(" and PERSON_REGDATE between TO_DATE(PKG_DATEUTIL.f_get_null_date_str,PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE('"+dataEntryDateTo+"',PKG_DATEUTIL.F_GET_DATEFORMAT) ");
		if(!"".equalsIgnoreCase(dataEntryDateFrom) && !"".equalsIgnoreCase(dataEntryDateTo))
			sqlBuffer.append(" and PERSON_REGDATE between TO_DATE('"+dataEntryDateFrom+"',PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE('"+dataEntryDateTo+"',PKG_DATEUTIL.F_GET_DATEFORMAT) ");
		if(!"".equalsIgnoreCase(patientId))
			sqlBuffer.append(" AND a.pk_person  IN ("+patientId+") ");
		if(!"".equalsIgnoreCase(queryCreatorIds))
			sqlBuffer.append(" AND a.CREATOR  IN ("+queryCreatorIds+") ");
		//sqlBuffer.append("AND EXISTS (SELECT * FROM er_patprot b WHERE a.pk_person = b.fk_per AND b.fk_study = 163 ) ");
		sqlBuffer.append("ORDER BY "+sortCol+" "+sortDir);
		StringBuffer sqlcomp = new StringBuffer();
		sqlcomp.append("select * from ( select a.*, rownum rnum FROM ("+sqlBuffer);
		//sqlcomp.append(") a WHERE ROWNUM <= "+LastRec+" ) WHERE rnum >"+firstRec);
		StringBuffer sqlcount = new StringBuffer();
		sqlcount.append("Select count(*) from ( " + sqlBuffer + " )");
		
		ArrayList<HashMap<String, Object>> maplist = new ArrayList<HashMap<String, Object>>();
		HashMap<String, Object> dataMap = null;
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		int rowcount=0;
		
		try {
			conn = CommonDAO.getConnection();
			pstmt = conn.prepareStatement(sqlcount.toString());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				rowcount = rs.getInt("count(*)");
			}
			if(LastRec==-1){LastRec=rowcount;}
			sqlcomp.append(") a WHERE ROWNUM <= "+LastRec+" ) WHERE rnum >"+firstRec);
			pstmt = conn.prepareStatement(sqlcomp.toString());
			rs = pstmt.executeQuery();

			while (rs.next()) {
				dataMap = new HashMap<String, Object>();
				
				dataMap.put("rows", rowcount);
				dataMap.put("Query_Status", "-");
				dataMap.put("Status_Days", "0");
				dataMap.put("Total_Queries", "0");
				dataMap.put("study_number", rs.getString(("study_number")));
				dataMap.put("organization", rs.getString(("organization")));
				dataMap.put("reg_date", rs.getString(("regd_date")));
				dataMap.put("created_on", rs.getString(("CREATED_ON")));
				dataMap.put("entered_by", rs.getString(("CREATOR")));
				dataMap.put("last_modified_date", rs.getString(("last_modified_date")));
				dataMap.put("last_modified_by", rs.getString(("last_modified_by")));
				
				dataMap.put("patient_id", rs.getString(("patient_id")));
				dataMap.put("facility_id", rs.getString(("facility_id")));
				
				dataMap.put("fname", rs.getString(("mask_pfname")));
				dataMap.put("lname", rs.getString(("mask_plname")));
				dataMap.put("dob", rs.getString(("dob")));
				dataMap.put("gender", rs.getString(("Person_gender")));
				dataMap.put("editboxPlaceHolder", "");
				//dataMap.put("auditboxPlaceHolder", "");
				//dataMap.put("checkboxPlaceHolder", "");

				maplist.add(dataMap);
				//rowcount++;
			}
			if(maplist.size()!=0){
				maplist.get(0).put("rows", rowcount);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return maplist;

	}
	
	public int updateBulkFormStatusData(HashMap<String, String> paramMap) {
		String accountId = paramMap.get("accountId");
		String userId = paramMap.get("userId");
		String filledFormIds = paramMap.get("filledFormIds");
		String tableName = paramMap.get("tableName");
		String pkTable = paramMap.get("pkTable");
		String filledFormStat = paramMap.get("filledFormStat");
		Integer tabSelected = Integer.parseInt(paramMap.get("tabSelected"));
		String formid="";
		String filledformid="";
		String[] splitfillformid=filledFormIds.split(",");
        String sql = null;
        int check=0;
        int output = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
        	conn = CommonDAO.getConnection();
        	conn.setAutoCommit(false);
        	if(tabSelected==0){
        		for(int i=0;i<splitfillformid.length;i++){
        			filledformid=splitfillformid[i].substring(0,splitfillformid[i].indexOf("*"));
        			formid=splitfillformid[i].substring(splitfillformid[i].indexOf("*")+1,splitfillformid[i].length());	
            sql = "UPDATE "+tableName+" SET FORM_COMPLETED = "+filledFormStat+" where "+pkTable+"="+filledformid+" and fk_formlib="+formid;
            pstmt = conn.prepareStatement(sql);
            check=pstmt.executeUpdate();
            if(check>0){
            sql = "UPDATE ER_FORMSLINEAR SET FORM_COMPLETED = "+filledFormStat+" where FK_FILLEDFORM="+filledformid+" and fk_form="+formid;
            pstmt = conn.prepareStatement(sql);
            check=pstmt.executeUpdate();}
        		}
        	}
        	else{
        		sql = "UPDATE "+tableName+" SET FORM_STATUS = "+filledFormStat+" where "+pkTable+" IN ("+filledFormIds+")";
                pstmt = conn.prepareStatement(sql);
                check=pstmt.executeUpdate();
        	}
            if(check>0){
            	output=1;
            }else{
            	output=-1;
            }
            return output;

        } catch (SQLException ex) {
        	try {
				conn.rollback();
			} catch (SQLException e) {
				e.printStackTrace();
			}
            Rlog.fatal("NewDashboard",
                    "QueryManagementDAO.updateBulkFormStatusData EXCEPTION IN DAO" + ex);
            ex.printStackTrace();
            return -1;
        } finally {
            try {
            	conn.commit();
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }
		
}
//@ Hemant line#(580,659-660) fixed for Bug# 21122,21123
