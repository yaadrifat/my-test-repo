package com.velos.eres.business.common;

public class CtrpPatientAccrualDao implements java.io.Serializable{
	
	private Integer fkPer;	
	private String maskPersonLName;	
	private String siteId;
	private String nciTrailIden;
	
	private String personZip;
	private String personCountry;
	private String personDOB ;
	private String personGender; 
	
	private String personRegDate;
	
	private String siteName;
	private String rightMask;
	private String studyNumber;
	private String pkPatProt;
	private String fkStudy;
	private String lowerPerCode;
	private String perCode;
	private String patStudyStatDesc;
	private String patStudyStatSubtype;
	private String pkPatStudyStat;
	private String patStudyCurStat;
	private String patStudyStatDateDateSort;
	private String patStudyStatNote;
	private String patStudyStatReason;
	private String patProtEnroldtDatesort;
	private String lowerLastVisit;
	private String lastVisitName;
	private String curVisit;
	private String curVisitDate;
	private String nextVisitDateSort;
	private String maskPatnameLower;
	private String lowerPatStdId;
	private String patProtPatstdid;
	private String maskPersonPhone;
	private String maskPatAddress;
	private String patRace;
	private String patEthnicity;
	private String personNotes;
	private String maskLowerplName;
	private String maskLowerpfName;
	private String assignedtoName;
	private String physicianName;
	private String enrolledbyName;
	private String treatIngorgName;
	private String currentTxArm;
	private String aeCountNumsort;
	private String saeCountNumsort;
	private String formCountNumsort;
	private String subjectDiseaseCode;
	private Integer dmgrphReportable;
	
	private Integer patEnrSiteOnStdTeam;
	
	
	/**
	 * @return the fkPer
	 */
	public Integer getFkPer() {
		return fkPer;
	}
	/**
	 * @param fkPer the fkPer to set
	 */
	public void setFkPer(Integer fkPer) {
		this.fkPer = fkPer;
	}
	/**
	 * @return the maskPersonLName
	 */
	public String getMaskPersonLName() {
		return maskPersonLName;
	}
	/**
	 * @param maskPersonLName the maskPersonLName to set
	 */
	public void setMaskPersonLName(String maskPersonLName) {
		this.maskPersonLName = maskPersonLName;
	}
	/**
	 * @return the siteId
	 */
	public String getSiteId() {
		return siteId;
	}
	/**
	 * @param siteId the siteId to set
	 */
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	/**
	 * @return the nciTrailIden
	 */
	public String getNciTrailIden() {
		return nciTrailIden;
	}
	/**
	 * @param nciTrailIden the nciTrailIden to set
	 */
	public void setNciTrailIden(String nciTrailIden) {
		this.nciTrailIden = nciTrailIden;
	}
	/**
	 * @return the siteName
	 */
	public String getSiteName() {
		return siteName;
	}
	/**
	 * @param siteName the siteName to set
	 */
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	/**
	 * @return the rightMask
	 */
	public String getRightMask() {
		return rightMask;
	}
	/**
	 * @param rightMask the rightMask to set
	 */
	public void setRightMask(String rightMask) {
		this.rightMask = rightMask;
	}
	/**
	 * @return the studyNumber
	 */
	public String getStudyNumber() {
		return studyNumber;
	}
	/**
	 * @param studyNumber the studyNumber to set
	 */
	public void setStudyNumber(String studyNumber) {
		this.studyNumber = studyNumber;
	}
	/**
	 * @return the pkPatProt
	 */
	public String getPkPatProt() {
		return pkPatProt;
	}
	/**
	 * @param pkPatProt the pkPatProt to set
	 */
	public void setPkPatProt(String pkPatProt) {
		this.pkPatProt = pkPatProt;
	}
	/**
	 * @return the fkStudy
	 */
	public String getFkStudy() {
		return fkStudy;
	}
	/**
	 * @param fkStudy the fkStudy to set
	 */
	public void setFkStudy(String fkStudy) {
		this.fkStudy = fkStudy;
	}
	/**
	 * @return the lowerPerCode
	 */
	public String getLowerPerCode() {
		return lowerPerCode;
	}
	/**
	 * @param lowerPerCode the lowerPerCode to set
	 */
	public void setLowerPerCode(String lowerPerCode) {
		this.lowerPerCode = lowerPerCode;
	}
	/**
	 * @return the perCode
	 */
	public String getPerCode() {
		return perCode;
	}
	/**
	 * @param perCode the perCode to set
	 */
	public void setPerCode(String perCode) {
		this.perCode = perCode;
	}
	/**
	 * @return the patStudyStatDesc
	 */
	public String getPatStudyStatDesc() {
		return patStudyStatDesc;
	}
	/**
	 * @param patStudyStatDesc the patStudyStatDesc to set
	 */
	public void setPatStudyStatDesc(String patStudyStatDesc) {
		this.patStudyStatDesc = patStudyStatDesc;
	}
	/**
	 * @return the patStudyStatSubtype
	 */
	public String getPatStudyStatSubtype() {
		return patStudyStatSubtype;
	}
	/**
	 * @param patStudyStatSubtype the patStudyStatSubtype to set
	 */
	public void setPatStudyStatSubtype(String patStudyStatSubtype) {
		this.patStudyStatSubtype = patStudyStatSubtype;
	}
	/**
	 * @return the pkPatStudyStat
	 */
	public String getPkPatStudyStat() {
		return pkPatStudyStat;
	}
	/**
	 * @param pkPatStudyStat the pkPatStudyStat to set
	 */
	public void setPkPatStudyStat(String pkPatStudyStat) {
		this.pkPatStudyStat = pkPatStudyStat;
	}
	/**
	 * @return the patStudyCurStat
	 */
	public String getPatStudyCurStat() {
		return patStudyCurStat;
	}
	/**
	 * @param patStudyCurStat the patStudyCurStat to set
	 */
	public void setPatStudyCurStat(String patStudyCurStat) {
		this.patStudyCurStat = patStudyCurStat;
	}
	/**
	 * @return the patStudyStatDateDateSort
	 */
	public String getPatStudyStatDateDateSort() {
		return patStudyStatDateDateSort;
	}
	/**
	 * @param patStudyStatDateDateSort the patStudyStatDateDateSort to set
	 */
	public void setPatStudyStatDateDateSort(String patStudyStatDateDateSort) {
		this.patStudyStatDateDateSort = patStudyStatDateDateSort;
	}
	/**
	 * @return the patStudyStatNote
	 */
	public String getPatStudyStatNote() {
		return patStudyStatNote;
	}
	/**
	 * @param patStudyStatNote the patStudyStatNote to set
	 */
	public void setPatStudyStatNote(String patStudyStatNote) {
		this.patStudyStatNote = patStudyStatNote;
	}
	/**
	 * @return the patStudyStatReason
	 */
	public String getPatStudyStatReason() {
		return patStudyStatReason;
	}
	/**
	 * @param patStudyStatReason the patStudyStatReason to set
	 */
	public void setPatStudyStatReason(String patStudyStatReason) {
		this.patStudyStatReason = patStudyStatReason;
	}
	/**
	 * @return the patProtEnroldtDatesort
	 */
	public String getPatProtEnroldtDatesort() {
		return patProtEnroldtDatesort;
	}
	/**
	 * @param patProtEnroldtDatesort the patProtEnroldtDatesort to set
	 */
	public void setPatProtEnroldtDatesort(String patProtEnroldtDatesort) {
		this.patProtEnroldtDatesort = patProtEnroldtDatesort;
	}
	/**
	 * @return the lowerLastVisit
	 */
	public String getLowerLastVisit() {
		return lowerLastVisit;
	}
	/**
	 * @param lowerLastVisit the lowerLastVisit to set
	 */
	public void setLowerLastVisit(String lowerLastVisit) {
		this.lowerLastVisit = lowerLastVisit;
	}
	/**
	 * @return the lastVisitName
	 */
	public String getLastVisitName() {
		return lastVisitName;
	}
	/**
	 * @param lastVisitName the lastVisitName to set
	 */
	public void setLastVisitName(String lastVisitName) {
		this.lastVisitName = lastVisitName;
	}
	/**
	 * @return the curVisit
	 */
	public String getCurVisit() {
		return curVisit;
	}
	/**
	 * @param curVisit the curVisit to set
	 */
	public void setCurVisit(String curVisit) {
		this.curVisit = curVisit;
	}
	/**
	 * @return the curVisitDate
	 */
	public String getCurVisitDate() {
		return curVisitDate;
	}
	/**
	 * @param curVisitDate the curVisitDate to set
	 */
	public void setCurVisitDate(String curVisitDate) {
		this.curVisitDate = curVisitDate;
	}
	/**
	 * @return the nextVisitDateSort
	 */
	public String getNextVisitDateSort() {
		return nextVisitDateSort;
	}
	/**
	 * @param nextVisitDateSort the nextVisitDateSort to set
	 */
	public void setNextVisitDateSort(String nextVisitDateSort) {
		this.nextVisitDateSort = nextVisitDateSort;
	}
	/**
	 * @return the maskPatnameLower
	 */
	public String getMaskPatnameLower() {
		return maskPatnameLower;
	}
	/**
	 * @param maskPatnameLower the maskPatnameLower to set
	 */
	public void setMaskPatnameLower(String maskPatnameLower) {
		this.maskPatnameLower = maskPatnameLower;
	}
	/**
	 * @return the lowerPatStdId
	 */
	public String getLowerPatStdId() {
		return lowerPatStdId;
	}
	/**
	 * @param lowerPatStdId the lowerPatStdId to set
	 */
	public void setLowerPatStdId(String lowerPatStdId) {
		this.lowerPatStdId = lowerPatStdId;
	}
	/**
	 * @return the patProtPatstdid
	 */
	public String getPatProtPatstdid() {
		return patProtPatstdid;
	}
	/**
	 * @param patProtPatstdid the patProtPatstdid to set
	 */
	public void setPatProtPatstdid(String patProtPatstdid) {
		this.patProtPatstdid = patProtPatstdid;
	}
	/**
	 * @return the maskPersonPhone
	 */
	public String getMaskPersonPhone() {
		return maskPersonPhone;
	}
	/**
	 * @param maskPersonPhone the maskPersonPhone to set
	 */
	public void setMaskPersonPhone(String maskPersonPhone) {
		this.maskPersonPhone = maskPersonPhone;
	}
	/**
	 * @return the maskPatAddress
	 */
	public String getMaskPatAddress() {
		return maskPatAddress;
	}
	/**
	 * @param maskPatAddress the maskPatAddress to set
	 */
	public void setMaskPatAddress(String maskPatAddress) {
		this.maskPatAddress = maskPatAddress;
	}
	/**
	 * @return the patRace
	 */
	public String getPatRace() {
		return patRace;
	}
	/**
	 * @param patRace the patRace to set
	 */
	public void setPatRace(String patRace) {
		this.patRace = patRace;
	}
	/**
	 * @return the patEthnicity
	 */
	public String getPatEthnicity() {
		return patEthnicity;
	}
	/**
	 * @param patEthnicity the patEthnicity to set
	 */
	public void setPatEthnicity(String patEthnicity) {
		this.patEthnicity = patEthnicity;
	}
	/**
	 * @return the personNotes
	 */
	public String getPersonNotes() {
		return personNotes;
	}
	/**
	 * @param personNotes the personNotes to set
	 */
	public void setPersonNotes(String personNotes) {
		this.personNotes = personNotes;
	}
	/**
	 * @return the maskLowerplName
	 */
	public String getMaskLowerplName() {
		return maskLowerplName;
	}
	/**
	 * @param maskLowerplName the maskLowerplName to set
	 */
	public void setMaskLowerplName(String maskLowerplName) {
		this.maskLowerplName = maskLowerplName;
	}
	/**
	 * @return the maskLowerpfName
	 */
	public String getMaskLowerpfName() {
		return maskLowerpfName;
	}
	/**
	 * @param maskLowerpfName the maskLowerpfName to set
	 */
	public void setMaskLowerpfName(String maskLowerpfName) {
		this.maskLowerpfName = maskLowerpfName;
	}
	/**
	 * @return the assignedtoName
	 */
	public String getAssignedtoName() {
		return assignedtoName;
	}
	/**
	 * @param assignedtoName the assignedtoName to set
	 */
	public void setAssignedtoName(String assignedtoName) {
		this.assignedtoName = assignedtoName;
	}
	/**
	 * @return the physicianName
	 */
	public String getPhysicianName() {
		return physicianName;
	}
	/**
	 * @param physicianName the physicianName to set
	 */
	public void setPhysicianName(String physicianName) {
		this.physicianName = physicianName;
	}
	/**
	 * @return the enrolledbyName
	 */
	public String getEnrolledbyName() {
		return enrolledbyName;
	}
	/**
	 * @param enrolledbyName the enrolledbyName to set
	 */
	public void setEnrolledbyName(String enrolledbyName) {
		this.enrolledbyName = enrolledbyName;
	}
	/**
	 * @return the treatIngorgName
	 */
	public String getTreatIngorgName() {
		return treatIngorgName;
	}
	/**
	 * @param treatIngorgName the treatIngorgName to set
	 */
	public void setTreatIngorgName(String treatIngorgName) {
		this.treatIngorgName = treatIngorgName;
	}
	/**
	 * @return the currentTxArm
	 */
	public String getCurrentTxArm() {
		return currentTxArm;
	}
	/**
	 * @param currentTxArm the currentTxArm to set
	 */
	public void setCurrentTxArm(String currentTxArm) {
		this.currentTxArm = currentTxArm;
	}
	/**
	 * @return the aeCountNumsort
	 */
	public String getAeCountNumsort() {
		return aeCountNumsort;
	}
	/**
	 * @param aeCountNumsort the aeCountNumsort to set
	 */
	public void setAeCountNumsort(String aeCountNumsort) {
		this.aeCountNumsort = aeCountNumsort;
	}
	/**
	 * @return the saeCountNumsort
	 */
	public String getSaeCountNumsort() {
		return saeCountNumsort;
	}
	/**
	 * @param saeCountNumsort the saeCountNumsort to set
	 */
	public void setSaeCountNumsort(String saeCountNumsort) {
		this.saeCountNumsort = saeCountNumsort;
	}
	/**
	 * @return the formCountNumsort
	 */
	public String getFormCountNumsort() {
		return formCountNumsort;
	}
	/**
	 * @param formCountNumsort the formCountNumsort to set
	 */
	public void setFormCountNumsort(String formCountNumsort) {
		this.formCountNumsort = formCountNumsort;
	}
	/**
	 * @return the personZip
	 */
	public String getPersonZip() {
		return personZip;
	}
	/**
	 * @param personZip the personZip to set
	 */
	public void setPersonZip(String personZip) {
		this.personZip = personZip;
	}
	/**
	 * @return the personCountry
	 */
	public String getPersonCountry() {
		return personCountry;
	}
	/**
	 * @param personCountry the personCountry to set
	 */
	public void setPersonCountry(String personCountry) {
		this.personCountry = personCountry;
	}
	/**
	 * @return the personDOB
	 */
	public String getPersonDOB() {
		return personDOB;
	}
	/**
	 * @param personDOB the personDOB to set
	 */
	public void setPersonDOB(String personDOB) {
		this.personDOB = personDOB;
	}
	/**
	 * @return the personGender
	 */
	public String getPersonGender() {
		return personGender;
	}
	/**
	 * @param personGender the personGender to set
	 */
	public void setPersonGender(String personGender) {
		this.personGender = personGender;
	}
	/**
	 * @return the personRegDate
	 */
	public String getPersonRegDate() {
		return personRegDate;
	}
	/**
	 * @param personRegDate the personRegDate to set
	 */
	public void setPersonRegDate(String personRegDate) {
		this.personRegDate = personRegDate;
	}
	/**
	 * @return the subjectDiseaseCode
	 */
	public String getSubjectDiseaseCode() {
		return subjectDiseaseCode;
	}
	/**
	 * @param subjectDiseaseCode the subjectDiseaseCode to set
	 */
	public void setSubjectDiseaseCode(String subjectDiseaseCode) {
		this.subjectDiseaseCode = subjectDiseaseCode;
	}
	/**
	 * @return the dmgrphReportable
	 */
	public Integer getDmgrphReportable() {
		return dmgrphReportable;
	}
	/**
	 * @param dmgrphReportable the dmgrphReportable to set
	 */
	public void setDmgrphReportable(Integer dmgrphReportable) {
		this.dmgrphReportable = dmgrphReportable;
	}
	public void setPatEnrSiteOnStdTeam(Integer patEnrSiteOnStdTeam) {
		this.patEnrSiteOnStdTeam = patEnrSiteOnStdTeam;
	}
	public Integer getPatEnrSiteOnStdTeam() {
		return patEnrSiteOnStdTeam;
	}
}
