/*
 * Classname			SpecimenDao.class
 *
 * Version information 	1.0
 *
 * Date					07/23/2007
 *
 * Copyright notice		Velos, Inc.
 *
 * Author 				Khader
 */

package com.velos.eres.business.common;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;

import com.aithent.file.uploadDownload.EJBUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;


/**
 * SpecimenDao for getting specimen records
 *
 * @author    Khader
 * @version : 1.0 07/22/2007
 */


public class SpecimenDao extends CommonDAO implements java.io.Serializable {

	private ArrayList pkSpecimens;
    private ArrayList specimenIds;
    private ArrayList specCollDates;
    private ArrayList specTypes;
    private ArrayList statuss;
    private ArrayList specQntys;
    private ArrayList spectQuntyUnits;
    private ArrayList specOrigQntys;
    private ArrayList spectOrigQuntyUnits;
    private ArrayList specProcTypes;

    private ArrayList specTypesPK;
    private ArrayList specProcTypesPK;

    private ArrayList specLocations;
    private ArrayList specLocationDescs;
    private ArrayList specStatusDates;
    private ArrayList specQuntyUnitPk;
    private ArrayList specOrigQuntyUnitPk;
    private ArrayList statusTypes;
    
    private String codelstCustom;
    
    public SpecimenDao() {

    	pkSpecimens = new ArrayList();
    	specimenIds = new ArrayList();
    	specCollDates = new ArrayList();
    	specTypes = new ArrayList();
    	statuss = new ArrayList();
    	specQntys = new ArrayList();
    	spectQuntyUnits = new ArrayList();
    	specOrigQntys = new ArrayList();
    	spectOrigQuntyUnits = new ArrayList();
    	specProcTypes = new ArrayList();
    	specTypesPK = new ArrayList();
    	specProcTypesPK = new ArrayList();

    	specLocations = new ArrayList();
    	specLocationDescs = new ArrayList();
    	specStatusDates =  new ArrayList();
    	specQuntyUnitPk = new ArrayList();
    	specOrigQuntyUnitPk = new ArrayList();
    	statusTypes = new ArrayList();
   }


    //JM: 30Jan2008: Getter and Setter methods

    public ArrayList getSpecProcTypesPK() {
		return specProcTypesPK;
	}


	public void setSpecProcTypesPK(ArrayList specProcTypesPK) {
		this.specProcTypesPK = specProcTypesPK;
	}

	public void setSpecProcTypesPK(String specProcTypesP) {
		this.specProcTypesPK.add(specProcTypesP);
	}


	public ArrayList getSpecTypesPK() {
		return specTypesPK;
	}


	public void setSpecTypesPK(ArrayList specTypesPK) {
		this.specTypesPK = specTypesPK;
	}

	public void setSpecTypesPK(String specTypesP) {
		this.specTypesPK.add(specTypesP);
	}


	public ArrayList getPkSpecimens() {
        return this.pkSpecimens;
    }

    public void setPkSpecimens(ArrayList pkSpecimens) {
        this.pkSpecimens = pkSpecimens;
    }

    public void setPkSpecimens(Integer pkSpecimen) {
        this.pkSpecimens.add(pkSpecimen);
    }

    public ArrayList getSpecimenIds() {
        return this.specimenIds;
    }

    public void setSpecimenIds(ArrayList specimenIds) {
        this.specimenIds = specimenIds;
    }

    public void setSpecimenIds(String specimenId) {
        this.specimenIds.add(specimenId);
    }

    public ArrayList getSpecCollDates() {
        return this.specCollDates;
    }

    public void setSpecCollDates(ArrayList specCollDates) {
        this.specCollDates = specCollDates;
    }

    public void setSpecCollDates(String specCollDate) {
        this.specCollDates.add(specCollDate);
    }
    public ArrayList getSpecTypes() {
        return this.specTypes;
    }

    public void setSpecTypes(ArrayList specTypes) {
        this.specTypes = specTypes;
    }

    public void setSpecTypes(String specType) {
        this.specTypes.add(specType);
    }

    public ArrayList getStatuss() {
        return this.statuss;
    }

    public void setStatuss(ArrayList statuss) {
        this.statuss = statuss;
    }

    public void setStatuss(String status) {
        this.statuss.add(status);
    }
    public ArrayList getSpecQntys() {
        return this.specQntys;
    }

    public void setSpecQntys(ArrayList specQntys) {
        this.specQntys = specQntys;
    }

    public void setSpecQntys(String specQnty) {
        this.specQntys.add(specQnty);
    }

    public ArrayList getSpecOrigQntys() {
        return this.specOrigQntys;
    }

    public void setSpecOrigQntys(ArrayList specOQntys) {
        this.specOrigQntys = specOQntys;
    }

    public void setSpecOrigQntys(String specOQnty) {
        this.specOrigQntys.add(specOQnty);
    }

    public ArrayList getSpecOrigQuntyUnitPk() {
		return specOrigQuntyUnitPk;
	}

	public void setSpecOrigQuntyUnitPk(ArrayList specOrigQuntyUnitPk) {
		this.specQuntyUnitPk = specOrigQuntyUnitPk;
	}

	public void setSpecOrigQuntyUnitPk(String specOrigQuntyUnitP) {
		this.specOrigQuntyUnitPk.add(specOrigQuntyUnitP);
	}

    public ArrayList getSpecProcTypes() {
        return this.specProcTypes;
    }
    
    public String getCodelstCustom(){
    	return this.codelstCustom;
    }
    
    public void setSpecProcTypes(ArrayList specProcTypes) {
        this.specProcTypes = specProcTypes;
    }

    public void setSpecProcTypes(String specProcType) {
        this.specProcTypes.add(specProcType);
    }
    public ArrayList getSpectQuntyUnits() {
        return this.spectQuntyUnits;
    }

    public void setSpectQuntyUnits(ArrayList spectQuntyUnits) {
        this.spectQuntyUnits = spectQuntyUnits;
    }

    public void setSpectQuntyUnits(String spectQuntyUnit) {
        this.spectQuntyUnits.add(spectQuntyUnit);
    }

    public ArrayList getSpecOrigQuntyUnits() {
        return this.spectOrigQuntyUnits;
    }

    public void setSpecOrigQuntyUnits(ArrayList specOQuntyUnits) {
        this.spectOrigQuntyUnits = specOQuntyUnits;
    }

    public void setSpecOrigQuntyUnits(String specOQuntyUnit) {
        this.spectOrigQuntyUnits.add(specOQuntyUnit);
    }
    public ArrayList getStatusTypes() {
        return this.statusTypes;
    }

    public void setStatusTypes(ArrayList statusTypes) {
        this.statusTypes = statusTypes;
    }

    public void setStatusTypes(String statusType) {
        this.statusTypes.add(statusType);
    }
    
    public void setCodelstCustom(String codelstCustom) {
        this.codelstCustom=codelstCustom;
    }
    //JM: 30Jan2008:

    public void getSpecimenChilds(int specId, int accId) {

    	PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            pstmt = conn
            .prepareStatement("SELECT pk_specimen, spec_id, spec_collection_date, (SELECT codelst_desc FROM er_codelst WHERE pk_codelst = spec_type) spec_type, spec_type as spec_type_pk, spec_processing_type as spec_proc_type_pk, "
            						+ " spec_quantity_units as spec_quantity_units_pk ,fk_storage, (select storage_name from er_storage where pk_storage = spec.fk_storage) location_desc,  "
            			  			+" (SELECT codelst_desc FROM er_codelst WHERE pk_codelst = (SELECT fk_codelst_status FROM er_specimen_status ss WHERE ss.fk_specimen = spec.pk_specimen "
            			  			+" AND ss.pk_specimen_status = (SELECT MAX(pk_specimen_status) FROM er_specimen_status WHERE fk_specimen = spec.pk_specimen "
            			  			+" AND ss_date = (SELECT MAX(ss1.ss_date) FROM er_specimen_status ss1 WHERE ss1.fk_specimen = spec.pk_specimen) ) ) ) specimen_status, "
            			  			+" (SELECT fk_codelst_status FROM er_specimen_status ss WHERE ss.fk_specimen = spec.pk_specimen AND ss.pk_specimen_status =(SELECT MAX(pk_specimen_status) "
            			  			+" FROM er_specimen_status WHERE fk_specimen = spec.pk_specimen AND ss_date = (SELECT MAX(ss1.ss_date) FROM er_specimen_status ss1 WHERE ss1.fk_specimen = spec.pk_specimen )))fk_codelst_status, "
            			  			+" spec_original_quantity, (select codelst_desc FROM er_codelst WHERE pk_codelst = spec_quantity_units) quantity_units , "
            			  			+" spec_base_origq_units as spec_base_orig_qunits_pk ,"
            			  			+" spec_base_orig_quantity, (select codelst_desc FROM er_codelst WHERE pk_codelst = spec_base_origq_units) origquantity_units , "
            			  			+" (select codelst_desc FROM er_codelst WHERE pk_codelst =(select ss.SS_PROC_TYPE from er_specimen_Status ss  WHERE ss.fk_specimen = spec.pk_specimen "
            			  			+" and ss.pk_specimen_status = (select max(pk_specimen_status) from er_specimen_Status where fk_specimen = spec.pk_specimen "
            			  			+" and ss_date = (select max(ss1.ss_date ) from er_specimen_Status ss1 where ss1.fk_specimen = spec.pk_specimen))))	processing_type "
            			  			+" from er_specimen spec"
            			  			+" WHERE spec.fk_specimen =? and spec.fk_Account =? AND spec.spec_id IS NOT NULL order By lpad(spec.spec_id,10) ");

            pstmt.setInt(1, specId);
            pstmt.setInt(2, accId);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
            	setPkSpecimens(new Integer(rs.getInt("pk_specimen")));
            	setSpecimenIds(rs.getString("spec_id"));
            	setSpecCollDates(rs.getString("spec_collection_date"));
            	setSpecTypes(rs.getString("spec_type"));
            	setStatuss(rs.getString("specimen_status"));
            	setStatusTypes(rs.getString("fk_codelst_status"));
            	setSpecQntys(rs.getString("spec_original_quantity"));
            	setSpectQuntyUnits(rs.getString("quantity_units"));
            	setSpecProcTypes(rs.getString("processing_type"));
            	setSpecTypesPK(rs.getString("spec_type_pk"));
            	setSpecProcTypesPK(rs.getString("spec_proc_type_pk"));
            	setSpecQuntyUnitPk(rs.getString("spec_quantity_units_pk"));
            	setSpecLocationDescs(rs.getString("location_desc"));
            	setSpecLocations(rs.getString("fk_storage"));
            	setSpecOrigQntys(rs.getString("spec_base_orig_quantity"));
            	setSpecOrigQuntyUnitPk(rs.getString("spec_base_orig_qunits_pk"));
            	setSpecOrigQuntyUnits(rs.getString("origquantity_units"));
            }

       } catch (SQLException ex) {
            Rlog.fatal("specimen",
                    "SpecimenDao.getSpecimenChilds EXCEPTION IN FETCHING FROM Specimen table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }




    public int deleteSpecimens(String[] deleteIds) {
        int ret = 1;
        CallableStatement cstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();

            ArrayDescriptor inIds = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY deleteIdsArray = new ARRAY(inIds, conn, deleteIds);
            cstmt = conn
                    .prepareCall("{call SP_DELETE_SPECIMENS(?,?)}");
            Rlog.debug("storage",
                    "SpecimenDao:deleteSpecimen-after prepare call of sp_delete_specimens");
            cstmt.setArray(1, deleteIdsArray);
            Rlog.debug("specimen",
                    "SpecimenDao:deleteSpecimen-after prepare call of sp_delete_specimens"
                            + deleteIdsArray);

            cstmt.registerOutParameter(2, java.sql.Types.INTEGER);
            cstmt.execute();
            Rlog.debug("storage",
                    "SpecimenDao:deleteSpecimen-after execute of sp_delete_specimens");
            ret = cstmt.getInt(2);
            Rlog.debug("specimen", "SpecimenDao:deleteSpecimen-after execute of RETURN VALUE"
                    + ret);

            return ret;

        } catch (Exception e) {
            Rlog.fatal("storage",
                    "EXCEPTION in SpecimenDao:deleteSpecimens, excecuting Stored Procedure "
                            + e);
            return -1;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }


    /**
     * Auto-generates a specimen Id and concatenates the study Id and patient Id
     * in the format specified in ER_SETTINGS table for this account
     *
     * @param
     * String studyId
     * String patientId
     */
    public String getSpecimenIdAuto(String studyId, String patientId, int fkAccount) {
        String specId = StringUtil.trueValue(getSpecimenIdAuto());
        SettingsDao settingsDao = new SettingsDao();
        settingsDao.retrieveSettings(fkAccount, 1, "SPECID_FORMAT");
        ArrayList settingArray = settingsDao.getSettingValue();
        if (settingArray == null || settingArray.size() < 1) {
            return specId;
        }
        if (studyId == null || studyId.length() == 0) { studyId = "0"; }
        if (patientId == null || patientId.length() == 0) { patientId = "0"; }
        String format = (String)settingArray.get(0);
        if (format == null || format.length() == 0) {
            return specId;
        }
        return format.replaceAll("\\[VELSTUDY\\]", studyId)
            .replaceAll("\\[VELPAT\\]", patientId)
            .replaceAll("\\[VELSPECID\\]", specId);
    }

    // for INF 21676 ::: Panjvir
    public HashSet<String> getChildSpecimenId(int Parent_SpecId) {
  	  PreparedStatement pstmt = null;
        Connection conn = null;
        HashSet specimen=new HashSet();
        try {  
      	  conn = getConnection();
            pstmt = conn.prepareStatement("select spec_id PARENTID from er_specimen where fk_specimen =? ");
            pstmt.setInt(1, Parent_SpecId);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
          	  specimen.add(rs.getString("PARENTID"));
            }
            return specimen;
        } catch (Exception ex) {
            Rlog.fatal("specimen","SpecimenDao.getChildSlecimenId EXCEPTION IN FETCHING Child Specimens"+ ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
        return specimen;
  }
    
    /**
     * Gets the Specimen id sequence from the er_specimen table
     *
     * @param
     *
     *            JM: 24July2007
     */
    public String getSpecimenIdAuto() {

        PreparedStatement pstmt = null;
        Connection conn = null;
        String specimenNumber=null;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("select seq_er_specimen_id.nextval SPECIMENID from dual");
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

            	specimenNumber =  rs.getString("SPECIMENID");

            }
            return specimenNumber;

        } catch (Exception ex) {
            Rlog.fatal("specimen",
                    "SpecimenDao.getSpecimenId EXCEPTION IN FETCHING FROM seq_er_specimen.nextval"
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

        return specimenNumber;

    }

    //Get the parent Specimen Id

    public String getParentSpecimenId(int specId) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        String specimenId=null;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("select fk_specimen PARENTID from er_specimen where pk_specimen =? ");
            pstmt.setInt(1, specId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

            	specimenId =  rs.getString("PARENTID");

            }
            return specimenId;

        } catch (Exception ex) {
            Rlog.fatal("specimen",
                    "SpecimenDao.getParentSpecimenId EXCEPTION IN FETCHING FROM er_specimen table"
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

        return specimenId;

    }
    
    public float getParentCurrAmt(int specId){
    	
    	PreparedStatement pstmt = null;
        Connection conn = null;
        ResultSet rs = null;
        float currAmt = 0;
        String parentPk=getParentSpecimenId(specId);
        try {
        	if(parentPk!=null){
        		conn = getConnection();
        		pstmt = conn.prepareStatement("select SPEC_ORIGINAL_QUANTITY from er_specimen where pk_specimen = ? ");
        		pstmt.setInt(1, StringUtil.stringToNum(parentPk));

        		rs = pstmt.executeQuery();

        		while (rs.next()) {
        			currAmt =  rs.getFloat("SPEC_ORIGINAL_QUANTITY");
            	}
        	}
        	else{
        		currAmt=-1;
        	}
            
        } catch (Exception ex) {
            Rlog.fatal("specimen",
                    "SpecimenDao.getParentCurrAmt EXCEPTION IN FETCHING FROM er_specimen table"
                            + ex);
        } finally {	
        	try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {}      	
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
        return currAmt;	
    }
      
public void getQntyAndCodeCustomValues(int pk_specStat){
    	
    	PreparedStatement pstmt = null;
        Connection conn = null;
        ResultSet rs = null;
        StringBuffer str=new StringBuffer();
        String code_custom="";
        float qnty=0;
        try {
        		conn = getConnection();
        		str.append("SELECT nvl(SS_QUANTITY,0) qnty,(SELECT CODELST_CUSTOM_COL FROM ER_CODELST WHERE PK_CODELST = FK_CODELST_STATUS) SS_FLAG ");
        		str.append(" FROM ER_SPECIMEN_STATUS WHERE pk_specimen_status = ? AND FK_CODELST_STATUS NOT IN (SELECT PK_CODELST FROM er_Codelst ");
        		str.append(" WHERE CODELST_TYPE  = 'specimen_stat' AND CODELST_CUSTOM_COL1 ='process') ");
        		pstmt = conn.prepareStatement(str.toString());
        		pstmt.setInt(1, pk_specStat);
        		rs = pstmt.executeQuery();
        		while (rs.next()) {
        			qnty=rs.getFloat("qnty");
        			code_custom=rs.getString("SS_FLAG");
        			setSpecQntys(StringUtil.floatToString(qnty));
        			setCodelstCustom(code_custom);
            	}
            
        } catch (Exception ex) {
            Rlog.fatal("specimen",
                    "SpecimenDao.getQntyAndCodeCustomValues EXCEPTION IN FETCHING FROM er_specimen table"
                            + ex);
        } finally {	
        	try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {}      	
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
    }
    
    
    public void updateQnty(int pk_specStat,int specId,float qnty,String code_custom,String flag){
    	
    	PreparedStatement pstmt = null;
        Connection conn = null;
        ResultSet rs = null;
        StringBuffer str=new StringBuffer();
        ArrayList<String> quantity= new ArrayList<String>();
        try {
        		if(qnty==0 && code_custom.equals("")){
        			getQntyAndCodeCustomValues(pk_specStat);
        			quantity=this.getSpecQntys();
        			qnty=StringUtil.stringToFloat(quantity.get(0));
        			code_custom=this.getCodelstCustom()==null?"":this.getCodelstCustom();
        		}
        		conn = getConnection();
        		if(qnty!=0.0 && qnty!=0){
        			if(code_custom.equals("+")){
        				str=new StringBuffer();
        				str.append("update er_specimen set SPEC_ORIGINAL_QUANTITY=(SPEC_ORIGINAL_QUANTITY+"+qnty+") where pk_specimen = ?");
        				pstmt = conn.prepareStatement(str.toString());
                		pstmt.setInt(1, specId);
                		pstmt.executeUpdate();
                		if(!flag.equalsIgnoreCase("Delete")){
                			str=new StringBuffer();
                			str.append("update er_specimen set SPEC_ORIGINAL_QUANTITY=(SPEC_ORIGINAL_QUANTITY-"+qnty+") where pk_specimen = (select fk_specimen from er_specimen where pk_specimen=?)");
                			pstmt = conn.prepareStatement(str.toString());
                			pstmt.setInt(1, specId);
                			pstmt.executeUpdate();
                		}
        			}
        			else if(code_custom.equals("-")){
        				str=new StringBuffer();
        				str.append("update er_specimen set SPEC_ORIGINAL_QUANTITY=(SPEC_ORIGINAL_QUANTITY-"+qnty+") where pk_specimen = ?");
        				pstmt = conn.prepareStatement(str.toString());
                		pstmt.setInt(1, specId);
                		pstmt.executeUpdate();
                		if(!flag.equalsIgnoreCase("Add")){
                			str=new StringBuffer();
                			str.append("update er_specimen set SPEC_ORIGINAL_QUANTITY=(SPEC_ORIGINAL_QUANTITY+"+qnty+") where pk_specimen = (select fk_specimen from er_specimen where pk_specimen=?)");
                			pstmt = conn.prepareStatement(str.toString());
                			pstmt.setInt(1, specId);
                			pstmt.executeUpdate();
                		}
        			}
        		}
            
        } catch (Exception ex) {
            Rlog.fatal("specimen",
                    "SpecimenDao.updateQnty EXCEPTION IN FETCHING FROM er_specimen table"
                            + ex);
        } finally {	
        	try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {}      	
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
    }
    
public int getSpecimenLatestStat(int specId){
    	
    	PreparedStatement pstmt = null;
        Connection conn = null;
        ResultSet rs = null;
        int speclateststat = 0;
        try {
        		conn = getConnection();
        		pstmt = conn.prepareStatement("select FK_CODELST_STATUS from er_specimen_status where fk_specimen=? and pk_specimen_status in (select max(pk_specimen_status) from er_specimen_status where fk_specimen=?) ");
        		pstmt.setInt(1, specId);
        		pstmt.setInt(2, specId);

        		rs = pstmt.executeQuery();

        		while (rs.next()) {
        			speclateststat =  rs.getInt("FK_CODELST_STATUS");
            	}

            
        } catch (Exception ex) {
            Rlog.fatal("specimen_status",
                    "SpecimenDao.getSpecimenLatestStat EXCEPTION IN FETCHING FROM er_specimen_status table"
                            + ex);
        } finally {	
        	try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {}      	
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
        return speclateststat;	
    }


	public ArrayList getSpecLocationDescs() {
		return specLocationDescs;
	}


	public void setSpecLocationDescs(ArrayList specLocationDescs) {
		this.specLocationDescs = specLocationDescs;
	}

	public void setSpecLocationDescs(String specLocationDesc) {
		this.specLocationDescs.add(specLocationDesc);
	}


	public ArrayList getSpecLocations() {
		return specLocations;
	}


	public void setSpecLocations(ArrayList specLocations) {
		this.specLocations = specLocations;
	}

	public void setSpecLocations(String specLocation) {
		this.specLocations.add(specLocation);
	}


	public ArrayList getSpecStatusDates() {
		return specStatusDates;
	}


	public void setSpecStatusDates(ArrayList specStatusDates) {
		this.specStatusDates = specStatusDates;
	}

	public void setSpecStatusDates(String specStatusDate) {
		this.specStatusDates.add(specStatusDate);
	}



	public ArrayList getSpecQuntyUnitPk() {
		return specQuntyUnitPk;
	}


	public void setSpecQuntyUnitPk(ArrayList spectQuntyUnitPk) {
		this.specQuntyUnitPk = spectQuntyUnitPk;
	}

	public void setSpecQuntyUnitPk(String spectQuntyUnitP) {
		this.specQuntyUnitPk.add(spectQuntyUnitP);
	}
	public int prepareSample(int fk_schevents1,int pkStorageKit,String fieldActionId,String ipadd,int user) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        int specimenId=0;
        Object[] array = fieldActionId.split(",");
        System.out.println("array"+array);
        
        try {
            conn = getConnection();
            ArrayDescriptor inFilterKey = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING",conn);
            ARRAY filterKeyArray = new ARRAY(inFilterKey, conn, array);
            pstmt = conn.prepareStatement("call SP_PREPAREDSAMPLE(?,?,?,?,?)");
            pstmt.setInt(1, fk_schevents1);
            pstmt.setInt(2, pkStorageKit);
            pstmt.setArray(3, filterKeyArray);
            pstmt.setString(4, ipadd);
            pstmt.setInt(5, user);

             pstmt.execute();

            
            

        } catch (Exception ex) {
        	ex.printStackTrace();
            Rlog.fatal("specimen",
                    "SpecimenDao.prepareSample EXCEPTION IN FETCHING FROM er_specimen table"
                            + ex);
            return -2;
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

        return 0;

    }
	
public int getPrepSpcmnCount(int storagePk, int fk_sch_events1, String prepFlag){
    	
    	PreparedStatement pstmt = null;
        Connection conn = null;
        ResultSet rs = null;
        int spcount = 0;
        try {
        		conn = getConnection();
        		pstmt = conn.prepareStatement("select count(*) prepspcount from er_specimen where PREPAREKITID IN (select pk_storage from er_storage where fk_storage=?) and fk_sch_events1=? and prepflag=? ");
        		pstmt.setInt(1, storagePk);
        		pstmt.setInt(2, fk_sch_events1);
        		pstmt.setString(3, prepFlag);
        		rs = pstmt.executeQuery();

        		while (rs.next()) {
        			spcount =  rs.getInt("prepspcount");
            	}

            
        } catch (Exception ex) {
            Rlog.fatal("specimen",
                    "SpecimenDao.getPrepSpcmnCount EXCEPTION IN FETCHING FROM er_specimen table"
                            + ex);
        } finally {	
        	try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {}      	
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
        return spcount;	
    }
	
public int getPrepSpcmnStoragId(int storagePk, int fk_sch_events1){
	
	PreparedStatement pstmt = null;
    Connection conn = null;
    ResultSet rs = null;
    int spcount = 0;
    try {
    		conn = getConnection();
    		pstmt = conn.prepareStatement("select PREPAREKITID from er_specimen where PREPAREKITID=? and fk_sch_events1=? and prepflag='Y'");
    		pstmt.setInt(1, storagePk);
    		pstmt.setInt(2, fk_sch_events1);
    		rs = pstmt.executeQuery();

    		while (rs.next()) {
    			spcount =  rs.getInt("PREPAREKITID");
        	}

        
    } catch (Exception ex) {
        Rlog.fatal("specimen",
                "SpecimenDao.getPrepSpcmnStoragId EXCEPTION IN FETCHING FROM er_specimen table"
                        + ex);
    } finally {	
    	try {
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {}      	
        try {
            if (pstmt != null) {
                pstmt.close();
            }
        } catch (Exception e) {
        }
        try {
            if (conn != null) {
                conn.close();
            }
        } catch (Exception e) {
        }
    }
    return spcount;	
}


public void updateColNdCurQty(String  fieldActionIdAp, String fk_schevents1){
	
	PreparedStatement pstmt = null;
    Connection conn = null;
    ResultSet rs = null;
    StringBuffer str=new StringBuffer();
    ArrayList<Integer> pk_specimen= new ArrayList<Integer>();
    int amt=0;
    try {
    		conn = getConnection();
    		pstmt = conn.prepareStatement("SELECT pk_specimen,SPEC_EXPECTED_QUANTITY FROM er_specimen WHERE PREPAREKITID IN ("+fieldActionIdAp+") AND PREPFLAG ='Y' AND FK_SCH_EVENTS1=?");
    		pstmt.setInt(1, EJBUtil.stringToNum(fk_schevents1));
    		rs = pstmt.executeQuery();
    		
    		while (rs.next()) {
    			pk_specimen.add(rs.getInt("pk_specimen"));
        	}
    		
    		for(int i=0;i<pk_specimen.size();i++){
    			pstmt = conn.prepareStatement("SELECT pk_specimen,SPEC_EXPECTED_QUANTITY FROM er_specimen WHERE fk_specimen = ?");
    			pstmt.setInt(1, pk_specimen.get(i));
    			rs = pstmt.executeQuery();

    			while (rs.next()) {
    				amt=amt+rs.getInt("SPEC_EXPECTED_QUANTITY");
    				str=new StringBuffer();
    				str.append("update er_specimen set SPEC_ORIGINAL_QUANTITY=SPEC_EXPECTED_QUANTITY, SPEC_BASE_ORIG_QUANTITY=SPEC_EXPECTED_QUANTITY, SPEC_QUANTITY_UNITS=SPEC_EXPECTEDQ_UNITS, SPEC_BASE_ORIGQ_UNITS=SPEC_EXPECTEDQ_UNITS where pk_specimen = ?");
    				pstmt = conn.prepareStatement(str.toString());
    				pstmt.setInt(1, rs.getInt("pk_specimen"));
    				pstmt.executeUpdate();
        			}
    		
    			str=new StringBuffer();
    			str.append("update er_specimen set SPEC_ORIGINAL_QUANTITY=SPEC_EXPECTED_QUANTITY-"+amt+", SPEC_BASE_ORIG_QUANTITY=SPEC_EXPECTED_QUANTITY, SPEC_QUANTITY_UNITS=SPEC_EXPECTEDQ_UNITS, SPEC_BASE_ORIGQ_UNITS=SPEC_EXPECTEDQ_UNITS where pk_specimen = ?");
    			pstmt = conn.prepareStatement(str.toString());
    			pstmt.setInt(1, pk_specimen.get(i));
    			pstmt.executeUpdate();
    			amt=0;
    		}
 
    } catch (Exception ex) {
        Rlog.fatal("specimen",
                "SpecimenDao.updateColNdCurQty EXCEPTION IN FETCHING FROM er_specimen table"
                        + ex);
    } finally {	
    	try {
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {}      	
        try {
            if (pstmt != null) {
                pstmt.close();
            }
        } catch (Exception e) {
        }
        try {
            if (conn != null) {
                conn.close();
            }
        } catch (Exception e) {
        }
    }
}

	//end of class
}
