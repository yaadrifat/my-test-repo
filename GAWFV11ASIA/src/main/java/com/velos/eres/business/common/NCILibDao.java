/*
 * Classname : NCILibDao
 * 
 * Version information: 1.0
 *
 * Date 09/11/2003
 * 
 * Copyright notice: Velos Inc.
 *
 * Author: Sonia Sahni
 */

package com.velos.eres.business.common;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;

import oracle.jdbc.OracleTypes;

import com.velos.eres.service.util.Rlog;

/**
 * NCILibDao class resulting from implementation of the Data Access Object
 * pattern
 * 
 * @author Sonia Sahni
 * @verison 1.0 05/06/2002
 */

public class NCILibDao extends CommonDAO implements java.io.Serializable {
    /**
     * the Columns
     */
    private ArrayList toxicityName;

    private ArrayList toxicityGroup;

    private ArrayList toxicityDesc;

    private ArrayList toxicityGrade;

    private ArrayList toxicityGradeDesc;

    private ArrayList toxicityCategory;

    private int calculatedToxicityGrade;

    private int ulnUsed;

    private int llnUsed;

    // GETTER SETTER METHODS

    public int getCalculatedToxicityGrade() {
        return this.calculatedToxicityGrade;
    }

    public void setCalculatedToxicityGrade(int calculatedToxicityGrade) {
        this.calculatedToxicityGrade = calculatedToxicityGrade;
    }

    public ArrayList getToxicityName() {
        return this.toxicityName;
    }

    public void setToxicityName(ArrayList toxicityName) {
        this.toxicityName = toxicityName;
    }

    public void setToxicityName(String toxicityName) {
        this.toxicityName.add(toxicityName);
    }

    public ArrayList getToxicityGroup() {
        return this.toxicityGroup;
    }

    public void setToxicityGroup(ArrayList toxicityGroup) {
        this.toxicityGroup = toxicityGroup;
    }

    public void setToxicityGroup(String toxicityGroup) {
        this.toxicityGroup.add(toxicityGroup);
    }

    public ArrayList getToxicityDesc() {
        return this.toxicityDesc;
    }

    public void setToxicityDesc(ArrayList toxicityDesc) {
        this.toxicityDesc = toxicityDesc;
    }

    public void setToxicityDesc(String toxicityDesc) {
        this.toxicityDesc.add(toxicityDesc);
    }

    public ArrayList getToxicityGrade() {
        return this.toxicityGrade;
    }

    public void setToxicityGrade(ArrayList toxicityGrade) {
        this.toxicityGrade = toxicityGrade;
    }

    public void setToxicityGrade(String toxicityGrade) {
        this.toxicityGrade.add(toxicityGrade);
    }

    public ArrayList getToxicityGradeDesc() {
        return this.toxicityGradeDesc;
    }

    public void setToxicityGradeDesc(ArrayList toxicityGradeDesc) {
        this.toxicityGradeDesc = toxicityGradeDesc;
    }

    public void setToxicityGradeDesc(String toxicityGradeDesc) {
        this.toxicityGradeDesc.add(toxicityGradeDesc);
    }

    public ArrayList getToxicityCategory() {
        return this.toxicityCategory;
    }

    public void setToxicityCategory(ArrayList toxicityCategory) {
        this.toxicityCategory = toxicityCategory;
    }

    public void setToxicityCategory(String toxicityCategory) {
        this.toxicityCategory.add(toxicityCategory);
    }

    public int getUlnUsed() {
        return ulnUsed;
    }

    public void setUlnUsed(int ulnUsed) {
        this.ulnUsed = ulnUsed;
    }

    public void setLlnUsed(int llnUsed) {
        this.llnUsed = llnUsed;
    }

    public int getLlnUsed() {
        return llnUsed;
    }

    public NCILibDao() {
        toxicityName = new ArrayList();
        toxicityGroup = new ArrayList();
        toxicityDesc = new ArrayList();
        toxicityGrade = new ArrayList();
        toxicityGradeDesc = new ArrayList();
        toxicityCategory = new ArrayList();

    }

    public void getNCILib(String nciVersion) {

        CallableStatement cstmt = null;
        ResultSet rs;
        Connection conn = null;

        try {

            conn = getConnection();
            cstmt = conn.prepareCall("{call PKG_ADV.SP_GET_NCILIB(?,?)}");

            cstmt.setString(1, nciVersion);
            cstmt.registerOutParameter(2, OracleTypes.CURSOR);
            cstmt.execute();

            rs = (ResultSet) cstmt.getObject(2);

            if (rs != null) {
                while (rs.next()) {

                    setToxicityName(rs.getString("ADV_EVENT"));
                    setToxicityDesc(rs.getString("TOXICITY_DESC"));
                    setToxicityGroup(rs.getString("TOXICITY_GROUPID"));

                }
            } else // resultset is null
            {
            }

        }// end of try
        catch (Exception ex) {
            Rlog.fatal("common", "NCILibDao:Exception in calling getNCILib : "
                    + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    // end of method

    public void getToxicityGrade(int toxicityGroup, String labDate,
            String labUnit, int labSite, String labResult, int labLLN,
            int labULN, int userRangeFlag, String nciVersion, int patient) {

        CallableStatement cstmt = null;
        CallableStatement cstmt2 = null;

        ResultSet rs;
        ResultSet rs2;
        Connection conn = null;
        int grade = -1;

        int llnUsed = 0;
        int ulnUsed = 0;

        try {

            conn = getConnection();
            cstmt = conn
                    .prepareCall("{call PKG_ADV.SP_CALC_TOXICITY(?,?,?,?,?,?,?,?,?,?,?,?,?)}");

            cstmt.setInt(1, toxicityGroup);
            cstmt.setString(2, labDate);
            cstmt.setString(3, labUnit);
            cstmt.setInt(4, labSite);
            cstmt.setString(5, labResult);
            cstmt.setInt(6, labLLN);
            cstmt.setInt(7, labULN);
            cstmt.setInt(8, userRangeFlag);
            cstmt.setString(9, nciVersion);
            cstmt.setInt(10, patient);
            cstmt.registerOutParameter(11, java.sql.Types.INTEGER);
            cstmt.registerOutParameter(12, java.sql.Types.INTEGER);
            cstmt.registerOutParameter(13, java.sql.Types.INTEGER);

            cstmt.execute();

            grade = cstmt.getInt(11);

            ulnUsed = cstmt.getInt(12);
            llnUsed = cstmt.getInt(13);

            setCalculatedToxicityGrade(grade);
            setUlnUsed(ulnUsed);
            setLlnUsed(llnUsed);

            // get grade list for toxicity group

            cstmt2 = conn
                    .prepareCall("{call PKG_ADV.SP_GET_TOXICITY_GRADE_LIST(?,?,?)}");
            cstmt2.setString(1, nciVersion);
            cstmt2.setInt(2, toxicityGroup);
            cstmt2.registerOutParameter(3, OracleTypes.CURSOR);
            cstmt2.execute();

            rs2 = (ResultSet) cstmt2.getObject(3);

            if (rs2 != null) {
                while (rs2.next()) {

                    setToxicityName(rs2.getString("adv_event"));
                    setToxicityGrade(rs2.getString("grade"));
                    setToxicityGradeDesc(rs2.getString("description"));

                }
            } else // resultset is null
            {
            }

        }// end of try
        catch (Exception ex) {
            Rlog.fatal("common",
                    "NCILibDao:Exception in calling getToxicityGrade : " + ex);

        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
                if (cstmt2 != null)
                    cstmt2.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    // end of class
}
