/*
 * Classname			StdSiteAddInfoDao.class
 * 
 * Version information 	1.0
 *
 * Date					11/16/2004
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.Rlog;

/**
 * StudyIdDao for getting StdSiteAddInfo records
 * 
 * @author Anu Khanna
 * @version : 1.0 11/16/2004
 */

public class StdSiteAddInfoDao extends CommonDAO implements
        java.io.Serializable {
    private ArrayList id;

    private ArrayList addInfoType;

    private ArrayList addInfoTypesDesc;

    private ArrayList data;

    private ArrayList recordType;

    private ArrayList dispType;

    private ArrayList dispData;

    private int cRows;

    public StdSiteAddInfoDao() {
        id = new ArrayList();
        addInfoType = new ArrayList();
        addInfoTypesDesc = new ArrayList();
        data = new ArrayList();
        recordType = new ArrayList();
        dispType = new ArrayList();
        dispData = new ArrayList();

    }

    // Getter and Setter methods
    public void setId(ArrayList id) {
        this.id = id;
    }

    public void setAddInfoType(ArrayList addInfoType) {
        this.addInfoType = addInfoType;
    }

    public void setAddInfoTypesDesc(ArrayList addInfoTypesDesc) {
        this.addInfoTypesDesc = addInfoTypesDesc;
    }

    public void setData(ArrayList data) {
        this.data = data;
    }

    public ArrayList getId() {
        return id;
    }

    public ArrayList getAddInfoType() {
        return addInfoType;
    }

    public ArrayList getAddInfoTypesDesc() {
        return addInfoTypesDesc;
    }

    public ArrayList getData() {
        return data;
    }

    public int getCRows() {
        return this.cRows;
    }

    public void setCRows(int cRows) {
        this.cRows = cRows;
    }

    public void setId(Integer id) {
        this.id.add(id);
    }

    public void setAddInfoType(Integer addInfoTyp) {
        this.addInfoType.add(addInfoTyp);
    }

    public void setAddInfoTypesDesc(String addInfoTypeDesc) {
        this.addInfoTypesDesc.add(addInfoTypeDesc);
    }

    public void setData(String d) {
        this.data.add(d);
    }

    public void setRecordType(ArrayList recordType) {
        this.recordType = recordType;
    }

    public ArrayList getRecordType() {
        return recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType.add(recordType);
    }

    public void setDispType(ArrayList dispType) {
        this.dispType = dispType;
    }

    public ArrayList getDispType() {
        return dispType;
    }

    public void setDispType(String disp) {
        this.dispType.add(disp);
    }

    /**
     * Returns the value of dispData.
     * 
     * @return an ArrayList of dispData (String objects)
     */
    public ArrayList getDispData() {
        return dispData;
    }

    /**
     * Sets the value of dispData.
     * 
     * @param dispData
     *            The value to assign dispData.
     */
    public void setDispData(ArrayList dispData) {
        this.dispData = dispData;
    }

    /**
     * Add a value of dispData.
     * 
     * @param dispData
     *            The value to add to dispData.
     */
    public void setDispData(String data) {
        this.dispData.add(data);
    }

    // end of getter and setter methods

    /**
     * Gets all Study Site's additional information for a studySiteId.
     * 
     * @param studySitePK -
     *            study site id for which values are retrieved.
     */

    public void getStudySiteAddInfo(int studySitePk) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sbSQL = new StringBuffer();
        try {
            conn = getConnection();

            sbSQL
                    .append(" Select pk_studysites_addinfo, pk_codelst, codelst_desc, studysite_data,'M' record_type, codelst_seq,codelst_custom_col");
            sbSQL
                    .append(",codelst_custom_col1 from  er_study_site_add_info, er_codelst ");
            sbSQL
                    .append(" where fk_studysites = ? and  pk_codelst = FK_CODELST_SITEADDINFO  and ");
            sbSQL.append(" codelst_type = 'siteaddinfo' ");
            sbSQL.append("  UNION ");
            sbSQL
                    .append("  Select 0 pk_studysites_addinfo , pk_codelst, codelst_desc,' ' studysite_data ,'N' record_type, codelst_seq,codelst_custom_col");
            sbSQL
                    .append(",codelst_custom_col1 from  er_codelst  where pk_codelst not in ");
            sbSQL
                    .append("(Select FK_CODELST_SITEADDINFO from er_study_site_add_info where   fk_studysites = ? ) and ");
            sbSQL.append(" codelst_type = 'siteaddinfo' order by codelst_seq ");

            pstmt = conn.prepareStatement(sbSQL.toString());

            Rlog.debug("siteaddinfo", "SQL- " + sbSQL);
            pstmt.setInt(1, studySitePk);
            pstmt.setInt(2, studySitePk);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setId(new Integer(rs.getInt("pk_studysites_addinfo")));
                setAddInfoType(new Integer(rs.getInt("pk_codelst")));
                setAddInfoTypesDesc(rs.getString("codelst_desc"));
                setData(rs.getString("STUDYSITE_DATA"));
                setRecordType(rs.getString("record_type"));
                setDispType(rs.getString("codelst_custom_col"));
                setDispData(rs.getString("codelst_custom_col1"));
                rows++;
                Rlog.debug("siteaddinfo",
                        "stdSiteAddInfoDao.getStudySiteAddInfo rows " + rows);
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("siteaddinfo",
                    "StdSiteAddInfoDao.getStudySiteAddInfo EXCEPTION IN FETCHING rows"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    // end of class
}
