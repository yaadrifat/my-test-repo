/*
 * Classname			MilepaymentDao.class
 * 
 * Version information 	1.0
 *
 * Date					05/31/2002
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.Rlog;

/**
 * MilepaymentDao for getting milestone payments records
 * 
 * @author Sonika Talwar
 * @version : 1.0 05/31/2002
 */

public class MilepaymentDao extends CommonDAO implements java.io.Serializable {

    /**
     * the Milepayment Id
     */
    private int id;

    private ArrayList milepaymentIds;

    private ArrayList milepaymentStudyIds;

    private ArrayList milepaymentMileIds;

    private ArrayList milepaymentDates;

    private ArrayList milepaymentDescs;

    private ArrayList milepaymentAmounts;
    
    private ArrayList milepaymentTypes;

    private int cRows;

    public MilepaymentDao() {
        milepaymentIds = new ArrayList();
        milepaymentStudyIds = new ArrayList();
        milepaymentMileIds = new ArrayList();
        milepaymentDates = new ArrayList();
        milepaymentDescs = new ArrayList();
        milepaymentAmounts = new ArrayList();
        milepaymentTypes= new ArrayList();
    }

    // Getter and Setter methods

    public ArrayList getMilepaymentIds() {
        return this.milepaymentIds;
    }

    public void setMilepaymentIds(ArrayList milepaymentIds) {
        this.milepaymentIds = milepaymentIds;
    }

    public ArrayList getMilepaymentStudyIds() {
        return this.milepaymentStudyIds;
    }

    public void setMilepaymentStudyIds(ArrayList milepaymentStudyIds) {
        this.milepaymentStudyIds = milepaymentStudyIds;
    }

    public ArrayList getMilepaymentMileIds() {
        return this.milepaymentMileIds;
    }

    public void setMilepaymentMileIds(ArrayList milepaymentMileIds) {
        this.milepaymentMileIds = milepaymentMileIds;
    }

    public ArrayList getMilepaymentDates() {
        return this.milepaymentDates;
    }

    public void setMilepaymentDates(ArrayList milepaymentDates) {
        this.milepaymentDates = milepaymentDates;
    }

    public ArrayList getMilepaymentDescs() {
        return this.milepaymentDescs;
    }

    public void setMilepaymentDescs(ArrayList milepaymentDescs) {
        this.milepaymentDescs = milepaymentDescs;
    }

    public ArrayList getMilepaymentAmounts() {
        return this.milepaymentAmounts;
    }

    public void setMilepaymentAmounts(ArrayList milepaymentAmounts) {
        this.milepaymentAmounts = milepaymentAmounts;
    }

    public int getCRows() {
        return this.cRows;
    }

    public void setCRows(int cRows) {
        this.cRows = cRows;
    }

    public void setMilepaymentIds(Integer milepaymentId) {
        milepaymentIds.add(milepaymentId);
    }

    public void setMilepaymentStudyIds(String milepaymentStudyId) {
        milepaymentStudyIds.add(milepaymentStudyId);
    }

    public void setMilepaymentMileIds(String milepaymentMileId) {
        milepaymentMileIds.add(milepaymentMileId);
    }

    public void setMilepaymentDates(String milepaymentDate) {
        milepaymentDates.add(milepaymentDate);
    }

    public void setMilepaymentDescs(String milepaymentDesc) {
        milepaymentDescs.add(milepaymentDesc);
    }

    public void setMilepaymentAmounts(String milepaymentAmount) {
        milepaymentAmounts.add(milepaymentAmount);
    }

    // end of getter and setter methods

    /**
     * Gets all Milestone Payments for a study
     * 
     * @param StudyId
     *            Id of study for which we want the payments of milestones
     */

    public void getMilepaymentReceived(int studyId) {
        int rows = 0;
        String lsql = "";
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            lsql = "select PK_MILEPAYMENT, "
                    + "FK_MILESTONE, "
                    + "TO_CHAR(MILEPAYMENT_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) AS MILE_DATE, "
                    + "MILEPAYMENT_AMT AS MILE_AMT, "
                    + "MILEPAYMENT_DESC , codelst_desc milepayment_cat "
                    + "from ER_MILEPAYMENT , er_codelst where FK_STUDY = ? and NVL(MILEPAYMENT_DELFLAG,'Z') <> 'Y' and pk_codelst = milepayment_type order by MILEPAYMENT_DATE DESC";

            pstmt = conn.prepareStatement(lsql);
            pstmt.setInt(1, studyId);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setMilepaymentIds(new Integer(rs.getInt("PK_MILEPAYMENT")));
                setMilepaymentMileIds(rs.getString("FK_MILESTONE"));
                setMilepaymentDates(rs.getString("MILE_DATE"));
                // setMilepaymentAmounts(rs.getString("MILEPAYMENT_AMT"));
                setMilepaymentDescs(rs.getString("MILEPAYMENT_DESC"));
                setMilepaymentAmounts(rs.getString("MILE_AMT"));
                setMilepaymentTypes(rs.getString("milepayment_cat"));
                rows++;
                Rlog.debug("milepayment",
                        "MilepaymentDao.getMilepaymentReceived rows " + rows);
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "milepayment",
                            "MilepaymentDao.getMilepaymentReceived EXCEPTION IN FETCHING FROM Milepayment table"
                                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ArrayList getMilepaymentTypes() {
		return milepaymentTypes;
	}

	public void setMilepaymentTypes(ArrayList milepaymentTypes) {
		this.milepaymentTypes = milepaymentTypes;
	}
	
	public void setMilepaymentTypes(String milepaymentType) {
		this.milepaymentTypes.add(milepaymentType);
	}

	

    /**
     * deletes the payment details
     * 
     * @param inv payment PK
     *            Description of the Parameter
     */
    public int deletePaymentDetails(int paypk) {
        int ret= 0;
        
        PreparedStatement pstmt = null;
        Connection conn = null;
       
      
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("Delete from er_milepayment_details where fk_milepayment = ? ");
            pstmt.setInt(1, paypk);

           	if (pstmt.executeUpdate() >=0 )
           	{
           		ret = 0;
           	  Rlog.fatal("milepayment",
                      "milepaymentDao.deletePaymentDetails data deleted, returning 0");
           	}
           	else
           	{
           		ret = -1;
           		Rlog.fatal("milepayment",
                "milepaymentDao.deletePaymentDetails data not deleted -1");
           		
           	}
           	conn.commit();
            	
            return ret ;

        } catch (Exception ex) {
            Rlog.fatal("milepayment",
                    "milepaymentDao.deletePaymentDetails EXCEPTION IN deleting FROM er_milepayment_details table"
                            + ex);
            return ret; 
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }

	
}
