/*
 * Classname			MilestoneDao.class
 *
 * Version information 	1.0
 *
 * Date					05/31/2002
 *
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.business.common;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.velos.eres.business.milestone.impl.MilestoneBean;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.service.util.LC;
import com.velos.esch.business.common.SchCodeDao;

/**
 * MilestoneDao for getting milestone stones records
 *
 * @author Sonika Talwar
 * @version : 1.0 05/31/2002
 */

public class MilestoneDao extends CommonDAO implements java.io.Serializable {

    /**
     * the Milestone Id
     */
    private int id;

    
    private ArrayList<Float> holdBack;

    private ArrayList milestoneIds;
	private ArrayList<Integer> isInvoiceGenerated;
    
    private ArrayList isReconciledAmount;

    private ArrayList milestoneStudyIds;

    private ArrayList milestoneTypes;

    private ArrayList milestoneCalIds;

    private ArrayList milestoneCalDescs;

    private ArrayList milestoneRuleIds;

    private ArrayList milestoneAmounts;

    private ArrayList milestoneVisits;

    private ArrayList milestoneEvents;

    private ArrayList milestoneEventDescs;

    private ArrayList milestoneCounts;

    private ArrayList milestoneUsers;

    private ArrayList milestoneUserIds;

    private ArrayList milestoneRuleDescs;

    private ArrayList milestoneDelFlags;

    private int cRows;

    int totalProtocolVisits = 0; // SV, 10/04, cal-enh

    private ArrayList protocolVisitNames;

    private ArrayList fkVisits; // SV, 11/01

    private ArrayList patientStatuses;

    private ArrayList milestoneAchievedCounts;

    private ArrayList milestoneLimits;

    private ArrayList milePaymentTypeIds;
    private ArrayList milestonePaymentTypes;

    private ArrayList milePaymentForIds;
    private ArrayList milestonePaymentFors;
    private ArrayList milestoneEventStatuses;

    //Added by Manimaran for Additional Milestones
    private ArrayList milestoneDescriptions;

    private ArrayList totalInvoicedAmounts;

    //Milestone Status Fk
    private ArrayList milestoneStatFK;
    //Milestone Event/Patient/Study Status FK
    private ArrayList milePrimaryStatusPK;
    
    //Milestone Patient Status FK
    private ArrayList mileSecondaryStatusPK;

    private ArrayList mileStoneSubType; /* YK 14DEC2010 :FIXED Bug #3379 */
    
    private Map<String, Integer> sopnsorAmountChkStatus;/*FIN-22375 Date :16 Aug 2012 By : Yogendra Pratap */

    private ArrayList milestoneDateFrom;
    
    private ArrayList milestoneDateTo;
    
    
    public ArrayList getMilePrimaryStatusPK() {
		return milePrimaryStatusPK;
	}

	public void setMilePrimaryStatusPK(ArrayList milePrimaryStatusPK) {
		this.milePrimaryStatusPK = milePrimaryStatusPK;
	}

	public void setMilePrimaryStatusPK(Integer milePrimaryStatusPK) {
		this.milePrimaryStatusPK.add(milePrimaryStatusPK);
	}
	
	 public ArrayList getMileSecondaryStatusPK() {
		return mileSecondaryStatusPK;
	}

	public void setMileSecondaryStatusPK(ArrayList mileSecondaryStatusPK) {
		this.mileSecondaryStatusPK = mileSecondaryStatusPK;
	}

	public void setMileSecondaryStatusPK(Integer mileSecondaryStatusPK) {
		this.mileSecondaryStatusPK.add(mileSecondaryStatusPK);
	}

  	//KM- To replace milestone_isactive column
	public ArrayList getMilestoneStatFK() {
		return milestoneStatFK;
	}
	
	/* YK 14DEC2010 :FIXED Bug #3379 */
	public ArrayList getMileStoneSubType() {
		return mileStoneSubType;
	}

	public void setMileStoneSubType(ArrayList mileStoneSubType) {
		this.mileStoneSubType = mileStoneSubType;
	}
	
	  public void setMileStoneSubType(String milestonesubType) {
		  mileStoneSubType.add(milestonesubType);
	    }
	  /* YK 14DEC2010 :FIXED Bug #3379 */
	public void setMilestoneStatFK(ArrayList milestoneStatFK) {
		this.milestoneStatFK = milestoneStatFK;
	}

	public void setMilestoneStatFK(Integer milestoneStatFK) {
		this.milestoneStatFK.add(milestoneStatFK);
	}

	public MilestoneDao() {
		holdBack=new ArrayList<Float>();
        milestoneIds = new ArrayList();
		isInvoiceGenerated=new ArrayList<Integer>();
        isReconciledAmount=new ArrayList<Integer>();
        milestoneStudyIds = new ArrayList();
        milestoneTypes = new ArrayList();
        milestoneCalIds = new ArrayList();
        milestoneRuleIds = new ArrayList();
        milestoneAmounts = new ArrayList();
        milestoneVisits = new ArrayList();
        milestoneEvents = new ArrayList();
        milestoneCounts = new ArrayList();
        milestoneUsers = new ArrayList();
        milestoneUserIds = new ArrayList();
        milestoneRuleDescs = new ArrayList();
        milestoneDelFlags = new ArrayList();
        milestoneCalDescs = new ArrayList();
        milestoneEventDescs = new ArrayList();
        protocolVisitNames = new ArrayList(); // SV, 10/04
        fkVisits = new ArrayList(); // SV, 11/1
        patientStatuses = new ArrayList();
        milestoneAchievedCounts = new ArrayList();
        milestoneLimits = new ArrayList();
        milePaymentTypeIds = new ArrayList();
        milestonePaymentTypes = new ArrayList();
        milePaymentForIds = new ArrayList();
        milestonePaymentFors = new ArrayList();
        milestoneEventStatuses = new ArrayList();
        milestoneDescriptions = new ArrayList();
        totalInvoicedAmounts = new ArrayList();
        milestoneStatFK = new ArrayList();
        mileStoneSubType = new ArrayList(); // YK 14Dec2010
        milePrimaryStatusPK = new ArrayList();
        mileSecondaryStatusPK = new ArrayList();
        sopnsorAmountChkStatus = new HashMap<String, Integer>();
        milestoneDateFrom = new ArrayList();        
        milestoneDateTo = new ArrayList();
    }

    // Getter and Setter methods

    public ArrayList getFkVisits() {
		return fkVisits;
	}

	public void setFkVisits(ArrayList fkVisits) {
		this.fkVisits = fkVisits;
	}

	public ArrayList getMilestoneAchievedCounts() {
		return milestoneAchievedCounts;
	}

	public void setMilestoneAchievedCounts(ArrayList milestoneAchievedCounts) {
		this.milestoneAchievedCounts = milestoneAchievedCounts;
	}

	public void setMilestoneAchievedCounts(String count) {
		this.milestoneAchievedCounts.add(count);
	}

	public void setProtocolVisitNames(ArrayList protocolVisitNames) {
		this.protocolVisitNames = protocolVisitNames;
	}

	public ArrayList getMilestoneIds() {
        return this.milestoneIds;
    }

    public void setMilestoneIds(ArrayList milestoneIds) {
        this.milestoneIds = milestoneIds;
    }

    public int getMilestoneIds(int rownum) {

        return ((Integer) this.milestoneIds.get(rownum)).intValue();

    }

    public ArrayList getMilestoneStudyIds() {
        return this.milestoneStudyIds;
    }

    public void setMilestoneStudyIds(ArrayList milestoneStudyIds) {
        this.milestoneStudyIds = milestoneStudyIds;
    }

    public String getMilestoneStudyIds(int rownum) {
        Rlog.debug("milestone", "milestoneDAO.getMilestoneStudyIds line rownum"
                + rownum);
        return new String((String) this.milestoneStudyIds.get(rownum));
    }

    public ArrayList getMilestoneTypes() {
        return this.milestoneTypes;
    }

    public void setMilestoneTypes(ArrayList milestoneTypes) {
        this.milestoneTypes = milestoneTypes;
    }

    public String getMilestoneTypes(int rownum) {
        return new String((String) this.milestoneTypes.get(rownum));
    }

    public ArrayList getMilestoneCalIds() {
        return this.milestoneCalIds;
    }

    public void setMilestoneCalIds(ArrayList milestoneCalIds) {
        this.milestoneCalIds = milestoneCalIds;
    }

    public String getMilestoneCalIds(int rownum) {
        return new String((String) this.milestoneCalIds.get(rownum));
    }

    public ArrayList getMilestoneRuleIds() {
        return this.milestoneRuleIds;
    }

    public void setMilestoneRuleIds(ArrayList milestoneRuleIds) {
        this.milestoneRuleIds = milestoneRuleIds;
    }

    public String getMilestoneRuleIds(int rownum) {
        return new String((String) this.milestoneRuleIds.get(rownum));
    }

    public ArrayList getMilestoneAmounts() {
        return this.milestoneAmounts;
    }

    public String getMilestoneAmounts(int rownum) {
        return new String((String) this.milestoneAmounts.get(rownum));
    }

    public String getProtocolVisitName(int rownum) {
        return ((String) this.protocolVisitNames.get(rownum));
    }

    public void setMilestoneAmounts(ArrayList milestoneAmounts) {
        this.milestoneAmounts = milestoneAmounts;
    }

    public ArrayList getMilestoneVisits() {
        return this.milestoneVisits;
    }

    public String getMilestoneVisits(int rownum) {
        return new String((String) this.milestoneVisits.get(rownum));

    }

    public void setMilestoneVisits(ArrayList milestoneVisits) {
        this.milestoneVisits = milestoneVisits;
    }

    public ArrayList getMilestoneEvents() {
        return this.milestoneEvents;
    }

    public String getMilestoneEvents(int rownum) {
        return new String((String) this.milestoneEvents.get(rownum));
    }

    public void setMilestoneEvents(ArrayList milestoneEvents) {
        this.milestoneEvents = milestoneEvents;
    }

    public ArrayList getMilestoneCounts() {
        return this.milestoneCounts;
    }

    public String getMilestoneCounts(int rownum) {
        return new String((String) this.milestoneCounts.get(rownum));
    }

    public void setMilestoneCounts(ArrayList milestoneCounts) {
        this.milestoneCounts = milestoneCounts;
    }

    public ArrayList getMilestoneUsers() {
        return this.milestoneUsers;
    }

    public String getMilestoneUsers(int rownum) {
        return new String((String) this.milestoneUsers.get(rownum));

    }

    public void setMilestoneUsers(ArrayList milestoneUsers) {
        this.milestoneUsers = milestoneUsers;
    }

    public ArrayList getMilestoneUserIds() {
        return this.milestoneUserIds;
    }

    public String getMilestoneUserIds(int rownum) {
        return new String((String) this.milestoneUserIds.get(rownum));

    }

    public void setMilestoneUserIds(ArrayList milestoneUserIds) {
        this.milestoneUserIds = milestoneUserIds;
    }

    public ArrayList getMilestoneRuleDescs() {
        return this.milestoneRuleDescs;
    }

    public void setMilestoneRuleDescs(ArrayList milestoneRuleDescs) {
        this.milestoneRuleDescs = milestoneRuleDescs;
    }

    public ArrayList getMilestoneDelFlags() {
        return this.milestoneDelFlags;
    }

    public String getMilestoneDelFlags(int rownum) {
        return new String((String) this.milestoneDelFlags.get(rownum));
    }

    public void setMilestoneDelFlags(ArrayList milestoneDelFlags) {
        this.milestoneDelFlags = milestoneDelFlags;
    }

    public ArrayList getMilestoneCalDescs() {
        return this.milestoneCalDescs;
    }

    public void setMilestoneCalDescs(ArrayList milestoneCalDescs) {
        this.milestoneCalDescs = milestoneCalDescs;
    }

    public ArrayList getMilestoneEventDescs() {
        return this.milestoneEventDescs;
    }

    public ArrayList getProtocolVisitNames() {
        return this.protocolVisitNames;
    }

    public ArrayList getFkVisit() {
        return this.fkVisits;
    }

    public String getFkVisit(int rownum) {
        return ((String) this.fkVisits.get(rownum));
    }

    public void setMilestoneEventDescs(ArrayList milestoneEventDescs) {
        this.milestoneEventDescs = milestoneEventDescs;
    }

    public int getCRows() {
        return this.cRows;
    }

    public void setCRows(int cRows) {
        this.cRows = cRows;
    }

    public void setMilestoneIds(Integer milestoneId) {
        milestoneIds.add(milestoneId);
    }

    public void setMilestoneStudyIds(String milestoneStudyId) {
        milestoneStudyIds.add(milestoneStudyId);
    }

    public void setMilestoneTypes(String milestoneType) {
        milestoneTypes.add(milestoneType);
    }

    public void setMilestoneCalIds(String milestoneCalId) {
        milestoneCalIds.add(milestoneCalId);
    }

    public void setMilestoneRuleIds(String milestoneRuleId) {
        milestoneRuleIds.add(milestoneRuleId);
    }

    public void setMilestoneAmounts(String milestoneAmount) {
        milestoneAmounts.add(milestoneAmount);
    }

    public void setMilestoneVisits(String milestoneVisit) {
        milestoneVisits.add(milestoneVisit);
    }

    public void setMilestoneEvents(String milestoneEvent) {
        milestoneEvents.add(milestoneEvent);
    }

    public void setMilestoneCounts(String milestoneCount) {
        milestoneCounts.add(milestoneCount);
    }

    public void setMilestoneUsers(String milestoneUser) {
        milestoneUsers.add(milestoneUser);
    }

    public void setMilestoneUserIds(String milestoneUserId) {
        milestoneUserIds.add(milestoneUserId);
    }

    public void setMilestoneRuleDescs(String milestoneRuleDesc) {
        milestoneRuleDescs.add(milestoneRuleDesc);
    }

    public void setMilestoneDelFlags(String milestoneDelFlag) {
        milestoneDelFlags.add(milestoneDelFlag);
    }

    public void setMilestoneCalDescs(String milestoneCalDesc) {
        milestoneCalDescs.add(milestoneCalDesc);
    }

    public void setMilestoneEventDescs(String milestoneEventDesc) {
        milestoneEventDescs.add(milestoneEventDesc);
    }

    public void setProtocolVisitNames(String protocolVisitName) {
        protocolVisitNames.add(protocolVisitName);
    }

    public void setFkVisits(String fkVisit) {
        fkVisits.add(fkVisit);
    }


    //Added by Manimaran for Additional Milestone.
    public ArrayList getMilestoneDescriptions(){
    	return milestoneDescriptions;
    }

    public void setMilestoneDescriptions(String milestoneDescription) {
    	this.milestoneDescriptions.add(milestoneDescription);
    }

    public void setMilestoneDescriptions(ArrayList milestoneDescriptions) {
    	this.milestoneDescriptions =  milestoneDescriptions;
    }
    
    public void setSopnsorAmountChkStatus(Map<String, Integer> sopnsorAmountChkStatus) {
		this.sopnsorAmountChkStatus = sopnsorAmountChkStatus;
	}

	public Map<String, Integer> getSopnsorAmountChkStatus() {
		return sopnsorAmountChkStatus;
	}

    // end of getter and setter methods

    /**
     * Gets all Milestone details for a study
     *
     * @param MilestoneId
     *            Id of study for which we want the stones of milestones
     */

    public void getMilestoneRows(int studyId) {
        String lsql = null;
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        int totalProtVisits = 0;

        try {
            conn = getConnection();


            lsql = "select a.PK_MILESTONE,  "
                    + "a.MILESTONE_TYPE, "
                    + "a.FK_CAL, "
                    + " null cal_desc, "
                    + "a.FK_CODELST_RULE, "
                    + " ' '  rule_desc ,"
                    + "a.MILESTONE_AMOUNT AS MILESTONE_AMOUNT, "
                    + "a.MILESTONE_VISIT, "
                    + "a.FK_EVENTASSOC, "
                    + "null event_desc, "
                    + "a.MILESTONE_COUNT, "
                    + "usr_lst(a.milestone_usersto) as usersto, "
                    + "a.milestone_usersto usertoIds "
                    + ", null fk_visit "
                    + ", '' visit_name , codelst_desc patientStatus  "
                    + "from ER_MILESTONE a , er_codelst b "
                    + "where "
                    + "a.FK_STUDY = ? "
                    + "and a.MILESTONE_TYPE in ( 'PM','SM') "
                    + "and NVL(a.MILEstone_DELFLAG,'Z') <> 'Y' and b.pk_codelst = a.milestone_status "
                    + "UNION "
                    + "select a.PK_MILESTONE, "
                    + "a.MILESTONE_TYPE, "
                    + "a.FK_CAL, "
                    + "c.name  cal_desc, "
                    + "a.FK_CODELST_RULE, "
                    + "b.codelst_desc  rule_desc, "
                    + "a.MILESTONE_AMOUNT AS MILESTONE_AMOUNT, "
                    + "a.MILESTONE_VISIT, "
                    + "a.FK_EVENTASSOC, "
                    + "null event_desc, "
                    + "a.MILESTONE_COUNT, "
                    + "usr_lst(a.milestone_usersto) as usersto, "
                    + "a.milestone_usersto usertoIds "
                    + ", a.fk_visit fk_visit "
                    + ", nvl((select v.visit_name from sch_protocol_visit  v where a.fk_visit = v.pk_protocol_visit ),'All') visit_name , (select codelst_desc from er_codelst where pk_codelst = milestone_status ) patientStatus  "
                    + "from ER_MILESTONE a, "
                    + "er_codelst b , "
                    + "event_Assoc  c "
                   + "where "
                    + "a.FK_STUDY = ? "
                    + "and a.MILESTONE_TYPE = 'VM' "
                    + "and a.FK_CODELST_RULE = b.pk_codelst  "
                    + "and a.fk_cal  = c.event_id "
                    + "and NVL(a.MILEstone_DELFLAG,'Z') <> 'Y' "
                    + "UNION "
                    + "select a.PK_MILESTONE,  "
                    + "a.MILESTONE_TYPE, "
                    + "a.FK_CAL, "
                    + "d.name  cal_desc,"
                    + "a.FK_CODELST_RULE, "
                    + "b.codelst_desc  rule_desc, "
                    + "a.MILESTONE_AMOUNT AS MILESTONE_AMOUNT, "
                    + "a.MILESTONE_VISIT, " + "a.FK_EVENTASSOC, "
                    + "c.name event_desc, " + "a.MILESTONE_COUNT, "
                    + "usr_lst(a.milestone_usersto) as usersto, "
                    + "a.milestone_usersto usertoIds "
                    + ", a.fk_visit fk_visit ,v.visit_name visit_name, (select codelst_desc from er_codelst where pk_codelst = milestone_status ) patientStatus  "
                    + "from ER_MILESTONE a, er_codelst b , "
                    + "event_Assoc  c,event_Assoc  d, "
                    + "sch_protocol_visit  v where "
                    + "a.FK_STUDY = ?  " + "and a.MILESTONE_TYPE = 'EM' "
                    + "and a.FK_CODELST_RULE = b.pk_codelst  "
                    + "and a.fk_cal  = d.event_id "
                    + "and a.FK_EVENTASSOC  = c.event_id "
                    + "and NVL(a.MILEstone_DELFLAG,'Z') <> 'Y' "
                    + "and c.fk_visit = v.pk_protocol_visit "
                    + "order by MILESTONE_TYPE ";

            Rlog.debug("milestone", lsql);

            pstmt = conn.prepareStatement(lsql);
            pstmt.setInt(1, studyId);
            pstmt.setInt(2, studyId);
            pstmt.setInt(3, studyId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setMilestoneIds(new Integer(rs.getInt("PK_MILESTONE")));
                setMilestoneTypes(rs.getString("MILESTONE_TYPE"));
                setMilestoneCalIds(rs.getString("FK_CAL"));
                setMilestoneCalDescs(rs.getString("CAL_DESC"));
                setMilestoneRuleIds(String.valueOf(rs
                        .getString("FK_CODELST_RULE")));
                setMilestoneRuleDescs(rs.getString("RULE_DESC"));
                // setMilestoneAmounts(String.valueOf(rs.getString("MILESTONE_AMOUNT")));
                setMilestoneAmounts(rs.getString("MILESTONE_AMOUNT"));
                setMilestoneVisits(String.valueOf(rs
                        .getString("MILESTONE_VISIT")));
                setMilestoneEvents(String
                        .valueOf(rs.getString("FK_EVENTASSOC")));
                setMilestoneEventDescs(rs.getString("EVENT_DESC"));
                setMilestoneCounts(rs.getString("MILESTONE_COUNT"));
                setMilestoneUsers(rs.getString("USERSTO"));
                setMilestoneUserIds(rs.getString("USERTOIDS"));
                setFkVisits(rs.getString("FK_VISIT")); // SV, 11/01
                setProtocolVisitNames(rs.getString("VISIT_NAME")); // SV, 11/01
                setPatientStatuses(rs.getString("patientStatus"));

                rows++;
                Rlog.debug("milestone", "MilestoneDao.getMilestoneRows rows "
                        + rows);
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("milestone",
                    "MilestoneDao.getMilestoneRows EXCEPTION IN FETCHING FROM Milestone table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    // ///
    /*Modified by Sonia Abrol, 10/24/2005, it was not required to check totl visit count , events will always be
     * associated with some visit. Also, one visit can be repeated in more than one milestone rule*/
    public void getEventsForNew(int studyId, int calId) {
        String lsql = null;
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer strSql = new StringBuffer();

        try {
                 conn = getConnection();

                 strSql.append("Select fk_visit,visit_name ,event_id,name ");
                 strSql.append(" from EVENT_ASSOC   assoc, SCH_PROTOCOL_VISIT  vis ");
                 strSql.append(" Where  chain_id  = ? and  EVENT_TYPE='A' and fk_visit = pk_protocol_visit order by lower(name),lower(visit_name)");




            Rlog.debug("milestone", strSql.toString());

            pstmt = conn.prepareStatement(strSql.toString());

            pstmt.setInt(1,calId);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

            	 setFkVisits(rs.getString("fk_visit"));
                 setProtocolVisitNames(rs.getString("visit_name"));

                 setMilestoneEvents(String.valueOf(rs.getInt("event_id")));

                  setMilestoneEventDescs(rs.getString("name") + "["+ rs.getString("visit_name") +"]");

                  Rlog.debug("milestone", "after 3 stmt");
                  rows++;
                Rlog.debug("milestone", "MilestoneDao.getEventsForNew rows "
                        + rows);

            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("milestone",
                    "MilestoneDao.getEventsForNew EXCEPTION IN DAO" + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public void getEventsForNewVisitMilestone(int studyId, int calId) {
        String lsql = null;
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            totalProtocolVisits = getTotalProtocolVisits(calId);
            //System.out.println("totalProtocolVisits="                    + new Integer(totalProtocolVisits));


            conn = getConnection();

            if (totalProtocolVisits > 0) {

                lsql = "Select visit,event_id,name, visit_name, fk_visit from "
                        + "(SELECT DISTINCT dense_rank () over "
                        + "(partition by d.mindisp order by "
                        + " (assoc.displacement - d.mindisp )) visit,"
                        + " event_id,name, visit_name, assoc.fk_visit fk_visit FROM EVENT_ASSOC  assoc, SCH_PROTOCOL_VISIT  vis, "
                        + "(select min(displacement) mindisp "
                        + " from  EVENT_ASSOC  where chain_id  = "
                        + calId
                        + " and EVENT_TYPE='A' and "
                        + "displacement > 0) d  WHERE CHAIN_ID  = "
                        + calId
                        + " and EVENT_TYPE='A' and "
                        + "assoc.DISPLACEMENT > 0 and assoc.FK_VISIT = vis.PK_PROTOCOL_VISIT ) a "
                        + "Where visit not in "
                        + "(select milestone_visit from er_milestone  where	fk_study = "
                        + studyId
                        + " and  fk_cal  = "
                        + calId
                        + " and MILESTONE_TYPE = 'VM'  and MILESTONE_DELFLAG <> 'Y') order by visit ";

            }

            else {

                lsql = "Select visit,event_id,name, visit_name, fk_visit from "
                        + "(SELECT DISTINCT dense_rank () over "
                        + "(partition by d.mindisp order by "
                        + " (displacement - d.mindisp )) visit,"
                        + " event_id,name, 'Visit ' visit_name, fk_visit FROM EVENT_ASSOC  , "
                        + "(select min(displacement) mindisp "
                        + " from  EVENT_ASSOC  where chain_id  = "
                        + calId
                        + " and EVENT_TYPE='A' and "
                        + "displacement > 0) d  WHERE CHAIN_ID  = "
                        + calId
                        + " and EVENT_TYPE='A' and "
                        + "DISPLACEMENT > 0) a "
                        + "Where visit not in "
                        + "(select milestone_visit from er_milestone  where	fk_study = "
                        + studyId
                        + " and  fk_cal  = "
                        + calId
                        + " and MILESTONE_TYPE = 'VM'  and MILESTONE_DELFLAG <> 'Y') order by visit ";

            }

            Rlog.debug("milestone", lsql);

            pstmt = conn.prepareStatement(lsql);

            // pstmt.setInt(1,calId);
            // pstmt.setInt(2,calId);
            // pstmt.setInt(3,studyId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setMilestoneVisits(String.valueOf(rs.getInt("visit")));
                Rlog.debug("milestone", "after 1 stmt");
                setMilestoneEvents(String.valueOf(rs.getInt("event_id")));
                Rlog.debug("milestone", "after 2 stmt");
                setMilestoneEventDescs(rs.getString("name"));
                Rlog.debug("milestone", "after 3 stmt");

                // SV, cal-enh, added protocol visits table and fields in.
                if (totalProtocolVisits > 0) {

                    setProtocolVisitNames(rs.getString("visit_name"));

                    //System.out.println("visit_name="                          + rs.getString("visit_name"));

                    setFkVisits(rs.getString("fk_visit")); // SV, 11/01

                    Rlog.debug("milestone", "after protocol visit name stmt");
                }

                rows++;
                Rlog.debug("milestone", "MilestoneDao.getEventsForNew rows "
                        + rows);
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("milestone",
                    "MilestoneDao.getEventsForNew EXCEPTION IN DAO" + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public void getMilestones(int studyId, String ruleType) {
        String lsql = null;
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            lsql = "Select a.MILESTONE_AMOUNT, a.MILESTONE_COUNT,"
                    + "b.codelst_desc  rule_desc" + "FROM ER_MILESTONE a, "
                    + "er_codelst b " + "WHERE a.FK_STUDY = ? "
                    + "and a.MILESTONE_TYPE = ? "
                    + "and a.FK_CODELST_RULE = b.pk_codelst";

            Rlog.debug("milestone", lsql);
            pstmt = conn.prepareStatement(lsql);
            pstmt.setInt(1, studyId);
            pstmt.setString(2, ruleType);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setMilestoneAmounts(String.valueOf(rs
                        .getInt("MILESTONE_AMOUNT")));
                Rlog.debug("milestone", "after 1 stmt");
                setMilestoneCounts(String.valueOf(rs.getInt("MILESTONE_COUNT")));
                Rlog.debug("milestone", "after 2 stmt");
                setMilestoneRuleDescs(rs.getString("rule_desc"));
                rows++;
                Rlog.debug("milestone", "MilestoneDao.getMilestones rows "
                        + rows);
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("milestone",
                    "MilestoneDao.getMilestones EXCEPTION IN DAO" + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Creates a milestoneStateKeeper onject with the attribute vales for a
     * given index
     *
     * @return milestoneStateKeeper
     */
    public MilestoneBean createMilestoneObj(int rownum) {
        MilestoneBean msk = new MilestoneBean();

        Rlog.debug("milestone", "milestoneDAO.createMilestoneObj rownum"
                + rownum);
        Rlog.debug("milestone",
                "milestoneDAO.createMilestoneObj milestoneStudyIds rownums"
                        + milestoneStudyIds.size());
        msk.setMilestoneStudyId(this.getMilestoneStudyIds(rownum));
        Rlog.debug("milestone", "milestoneDAO.createMilestoneObj line 1");
        msk.setMilestoneType(this.getMilestoneTypes(rownum));
        Rlog.debug("milestone", "milestoneDAO.createMilestoneObj line 2");
        msk.setMilestoneCalId(this.getMilestoneCalIds(rownum));
        Rlog.debug("milestone", "milestoneDAO.createMilestoneObj line 3");
        msk.setMilestoneRuleId(this.getMilestoneRuleIds(rownum));
        Rlog.debug("milestone", "milestoneDAO.createMilestoneObj line 4");
        msk.setMilestoneAmount(this.getMilestoneAmounts(rownum));
        Rlog.debug("milestone", "milestoneDAO.createMilestoneObj line 5");
        msk.setMilestoneVisit(this.getMilestoneVisits(rownum));
        Rlog.debug("milestone", "milestoneDAO.createMilestoneObj line 6");
        msk.setMilestoneEvent(this.getMilestoneEvents(rownum));
        Rlog.debug("milestone", "milestoneDAO.createMilestoneObj line 7");
        msk.setMilestoneDelFlag(this.getMilestoneDelFlags(rownum));
        Rlog.debug("milestone", "milestoneDAO.createMilestoneObj line 8");

        msk.setMilestoneVisitFK(this.getFkVisit(rownum)); // SV,. 11/1
        Rlog.debug("milestone", "milestoneDAO.createMilestoneObj line 9");

        Rlog.debug("milestone", "milestoneDAO.createMilestoneObj end");

        return msk;

    }

    // SV, 10/04/04, added as part of calendar visit related enhancements.
    public int getTotalProtocolVisits(int protocol_id) {
        // return this.visit_ids.size();
        PreparedStatement pstmt = null;
        Connection conn = null;

        int total_visits = 0;
        try {
            conn = getConnection();

            String mysql = "SELECT COUNT(*) TOTAL_VISITS"
                    + " FROM SCH_PROTOCOL_VISIT "
                    + " WHERE FK_PROTOCOL = ?";
//            System.out.println(mysql);

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, protocol_id);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                total_visits = rs.getInt("TOTAL_VISITS");
                break; // there is only one row!
            }
        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "protvisit",
                            "ProtVisitDao.getTotalVisits EXCEPTION IN FETCHING FROM SCH_PROTOCOL_VISIT table"
                                    + ex);
            return (0);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();

            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();

            } catch (Exception e) {
            }

        }
        // total_visits here will either be = count or 0..
        return (total_visits);

    }

    public String getMilestoneVisitName(int studyId, int calId, int visitNo) {
        // SV, 10/13/04, this assumes that visit # is going to be unique within
        // a calendar. If this is not the case, we need to populate and use
        // fk_visit.

        PreparedStatement pstmt = null;
        Connection conn = null;
        String visitName = "";
        try {
            conn = getConnection();

            String mysql = "SELECT distinct visit_name"
                    + " FROM SCH_PROTOCOL_VISIT  visit, "
                    + "from ER_MILESTONE mil, "
                    + " WHERE mil.fk_study = ? and "
                    + " mil.FK_VISIT = visit.PK_PROTOCOL_VISIT and "
                    + "  mil.FK_CAL = ?";

            //System.out.println(mysql);

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, studyId);
            pstmt.setInt(2, calId);
            pstmt.setInt(3, visitNo);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                visitName = rs.getString("VISIT_NAME");
                break; // there is only one row!
            }
        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "protvisit",
                            "ProtVisitDao.getMilestoneVisitName EXCEPTION IN FETCHING FROM SCH_PROTOCOL_VISIT table"
                                    + ex);
            return ("");
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();

            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();

            } catch (Exception e) {
            }

        }
        // total_visits here will either be = count or 0..
        return (visitName);

    }

    public String getMilestoneVisitName(int milestoneId) {
        // SV, 10/13/04, this assumes that visit # is going to be unique within
        // a calendar. If this is not the case, we need to populate and use
        // fk_visit.

        PreparedStatement pstmt = null;
        Connection conn = null;
        String visitName = "";
        try {
            conn = getConnection();

            String mysql = "SELECT distinct visit_name"
                    + " FROM SCH_PROTOCOL_VISIT  visit, "
                    + " ER_MILESTONE mil " + " WHERE mil.pk_milestone = ? and "
                    + " mil.FK_VISIT = visit.PK_PROTOCOL_VISIT";

            //System.out.println(mysql);

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, milestoneId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                visitName = rs.getString("VISIT_NAME");
                break; // there is only one row!
            }
        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "protvisit",
                            "ProtVisitDao.getMilestoneVisitName EXCEPTION IN FETCHING FROM SCH_PROTOCOL_VISIT table"
                                    + ex);
            return ("");
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();

            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();

            } catch (Exception e) {
            }

        }
        // total_visits here will either be = count or 0..
        return (visitName);

    }

	public ArrayList getPatientStatuses() {
		return patientStatuses;
	}

	public void setPatientStatuses(ArrayList patientStatuses) {
		this.patientStatuses = patientStatuses;
	}

	public void setPatientStatuses(String patientStatus) {
		this.patientStatuses.add(patientStatus);
	}

	/*Added by Sonia Abrol. 11/07/05 */
	/** Overloading getMilestoneRows(int studyId), to get milestones according to milestone type
	 * @param milestoneReceivableStatus for milestone receivable statuses: All receivables, outstanding receivables, un-invoiced receivables. pass an empty string
	 * <br> or 'A' for 'All', UI- for uninvoiced and 'OR' for outstanding
	 *
	 * */
	 public void getMilestoneRows(int studyId, String mileStoneType, String payType,String milestoneReceivableStatus, int forInvoicing) {
        String lsql = null;
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        StringBuffer sbSQL = new StringBuffer();
        String milestoneRuleDesc = "";
        String ruleDesc = "";
        String patientStatusDesc = "";
        String visitName = "";
        String eventName = "";
        String eventStatus = "";
        String patientCountString = "";
        String patientCount = "";
        String calName = "";
        String calassocto = "";

        String totalAmountInvColumn = " (select sum(nvl(amount_invoiced,0) + nvl(amount_holdback,0)) from er_invoice_detail det, er_invoice inv where det.fk_milestone = pk_milestone AND inv.pk_invoice = det.fk_inv and nvl(detail_type,'H')='H') amount_invoiced ";

        try {
            conn = getConnection();

            if (mileStoneType.equals("PM") || mileStoneType.equals("SM") || mileStoneType.equals("AM") )
            {
            	sbSQL.append("select  a.PK_MILESTONE,milestone_holdback,  ");
                sbSQL.append("a.MILESTONE_AMOUNT AS MILESTONE_AMOUNT, ");
               	sbSQL.append("a.MILESTONE_COUNT, ");
               	sbSQL.append(" usr_lst(a.milestone_usersto) as usersto, ");
               	sbSQL.append("a.milestone_usersto usertoIds,  milestone_achievedcount, " + totalAmountInvColumn + ",");

               	//Modified by Manimaran for Enh#FIN10
               	if (mileStoneType.equals("AM")){
               		sbSQL.append(" milestone_description");
               		sbSQL.append(" from ER_MILESTONE a ");
               	}
               	else
               	{
               		sbSQL.append(" b.codelst_desc patientStatus ,milestone_limit");
                	sbSQL.append(" from ER_MILESTONE a , er_codelst b ");
               	}


               	if (! StringUtil.isEmpty(payType))
            	{
            		sbSQL.append(" , er_codelst p ");

            	}

               	sbSQL.append( "where a.FK_STUDY = ? ");
               	sbSQL.append("and a.MILESTONE_TYPE  = '"+mileStoneType+"' ");


               	if (mileStoneType.equals("AM")){
               		sbSQL.append( "and NVL(a.MILEstone_DELFLAG,'Z') <> 'Y' ");
               	}
               	else
               	{
               	   	sbSQL.append( "and NVL(a.MILEstone_DELFLAG,'Z') <> 'Y' and b.pk_codelst = a.milestone_status ");
               	}


            }

            if (mileStoneType.equals("VM"))
            {
            	sbSQL.append("select  a.PK_MILESTONE,milestone_holdback, a.FK_CAL, c.name  cal_desc,  a.FK_CODELST_RULE, ");
            	sbSQL.append(" b.codelst_desc  rule_desc, a.MILESTONE_AMOUNT AS MILESTONE_AMOUNT, ");
            	sbSQL.append(" a.MILESTONE_COUNT, usr_lst(a.milestone_usersto) as usersto, a.milestone_usersto usertoIds, ");
            	sbSQL.append(" a.fk_visit fk_visit ,nvl((select v.visit_name from sch_protocol_visit  v where a.fk_visit = v.pk_protocol_visit ),'All') visit_name , ");
            	sbSQL.append(" (select codelst_desc from er_codelst where pk_codelst = milestone_status ) patientStatus , milestone_limit, ");
            	sbSQL.append(" (select codelst_desc from sch_codelst where pk_codelst = milestone_eventstatus ) eventStatus ,milestone_achievedcount");
            	sbSQL.append(" , c.event_calassocto, " + totalAmountInvColumn);
            	sbSQL.append(" from ER_MILESTONE a, er_codelst b , event_Assoc  c " );


            	if (! StringUtil.isEmpty(payType))
            	{
            		sbSQL.append(" , er_codelst p ");

            	}


            	sbSQL.append(" where a.FK_STUDY = ? and a.MILESTONE_TYPE = 'VM' ");
            	sbSQL.append(" and a.FK_CODELST_RULE = b.pk_codelst  and a.fk_cal  = c.event_id and NVL(a.MILEstone_DELFLAG,'Z') <> 'Y' ");
            }


            if (mileStoneType.equals("EM"))
            {
            	sbSQL.append("select  a.PK_MILESTONE,milestone_holdback,  a.FK_CAL, c.name  cal_desc,  a.FK_CODELST_RULE, ");
            	sbSQL.append(" b.codelst_desc  rule_desc, a.MILESTONE_AMOUNT AS MILESTONE_AMOUNT, a.FK_EVENTASSOC,");
            	sbSQL.append(" a.MILESTONE_COUNT, usr_lst(a.milestone_usersto) as usersto, a.milestone_usersto usertoIds, ");
            	sbSQL.append(" a.fk_visit fk_visit ,nvl((select v.visit_name from sch_protocol_visit  v where a.fk_visit = v.pk_protocol_visit ),'All') visit_name , ");
            	sbSQL.append(" (select codelst_desc from er_codelst where pk_codelst = milestone_status ) patientStatus , milestone_limit ,");
            	sbSQL.append(" (select codelst_desc from sch_codelst where pk_codelst = milestone_eventstatus ) eventStatus ,milestone_achievedcount, ");
            	sbSQL.append(" d.name event_desc , d.event_calassocto ," + totalAmountInvColumn);
            	sbSQL.append(" from ER_MILESTONE a, er_codelst b , event_Assoc  c , event_Assoc  d" );

            	if (! StringUtil.isEmpty(payType))
            	{
            		sbSQL.append(" , er_codelst p ");

            	}

            	sbSQL.append(" where a.FK_STUDY = ? and a.MILESTONE_TYPE = 'EM' and a.FK_EVENTASSOC  = d.event_id ");
            	sbSQL.append(" and a.FK_CODELST_RULE = b.pk_codelst  and a.fk_cal  = c.event_id and NVL(a.MILEstone_DELFLAG,'Z') <> 'Y' ");
            }


            if (! StringUtil.isEmpty(payType))
           	{
            	if("recinv".equals(payType)){
            		payType="pay";
            		sbSQL.append(" and p.pk_codelst = a.milestone_paytype and trim(p.codelst_subtyp) != ? ");}
            	else
            		sbSQL.append(" and p.pk_codelst = a.milestone_paytype and trim(p.codelst_subtyp) = ? ");

           	}

            if (milestoneReceivableStatus.equals("UI"))
            {

            	sbSQL.append(" and (select count(*) from er_mileachieved ac where ac.fk_milestone = pk_milestone and not exists (select * from er_invoice_detail det where det.fk_milestone = ac.fk_milestone and det.fk_mileachieved = ac.pk_mileachieved) ) > 0 ");
            }
            if(forInvoicing == 1)
            sbSQL.append(" and a.FK_CODELST_MILESTONE_STAT = (SELECT pk_codelst FROM er_codelst WHERE CODELST_TYPE = 'milestone_stat' AND CODELST_SUBTYP = 'A') ");

            Rlog.debug("getmilestoneSQL:", sbSQL.toString());

            pstmt = conn.prepareStatement(sbSQL.toString());
            pstmt.setInt(1, studyId);

            if (! StringUtil.isEmpty(payType))
           	{

            	pstmt.setString(2, payType);

           	}

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setMilestoneIds(new Integer(rs.getInt("PK_MILESTONE")));
                setTotalInvoicedAmounts(rs.getString("amount_invoiced") );
                setHoldBack(rs.getFloat("milestone_holdback"));	

                Rlog.debug("milestone", "MilestoneDao.getMilestoneRows rows a");
                patientCount =rs.getString("MILESTONE_COUNT");

                //Rlog.debug("milestone", "MilestoneDao.getMilestoneRows rows b");

                setMilestoneCounts(patientCount);
                setMilestoneUsers(rs.getString("USERSTO"));
                //Rlog.debug("milestone", "MilestoneDao.getMilestoneRows rows c");
                setMilestoneUserIds(rs.getString("USERTOIDS"));

                if (!mileStoneType.equals("AM")) {
                   Rlog.debug("milestone", "MilestoneDao.getMilestoneRows rows d");
                   setPatientStatuses(rs.getString("patientStatus"));
                   Rlog.debug("milestone", "MilestoneDao.getMilestoneRows rows f");
                   patientStatusDesc = rs.getString("patientStatus");

                }

                setMilestoneAmounts(rs.getString("MILESTONE_AMOUNT"));


                if (StringUtil.isEmpty(patientCount))
                {
                	patientCount = "";

                }

                if (StringUtil.isEmpty(patientStatusDesc))
                {

                patientStatusDesc = "";

                }
                else
                {
                	patientStatusDesc = "'" + patientStatusDesc  + "'";

                }



                if(mileStoneType.equals("AM"))
                {
                	setMilestoneDescriptions(rs.getString("MILESTONE_DESCRIPTION"));
                }


                if (mileStoneType.equals("EM") || mileStoneType.equals("VM"))
                {
                	setMilestoneCalIds(rs.getString("FK_CAL"));
                	setMilestoneCalDescs(rs.getString("CAL_DESC"));
                	setMilestoneRuleIds(String.valueOf(rs.getString("FK_CODELST_RULE")));
                	calName =  rs.getString("CAL_DESC");
                	setFkVisits(rs.getString("FK_VISIT")); // SV, 11/01
                    setProtocolVisitNames(rs.getString("VISIT_NAME")); // SV, 11/01
                    ruleDesc = rs.getString("RULE_DESC");
                    visitName = rs.getString("VISIT_NAME");

                    if (forInvoicing != 1){
                    	ruleDesc = ", Rule: " + ruleDesc;
                    }
                    eventStatus = rs.getString("eventStatus");
                    if (StringUtil.isEmpty(eventStatus))
                    	eventStatus = "";

                    calassocto = rs.getString("event_calassocto");

                    if (StringUtil.isEmpty(calassocto ))
                    {
                    	calassocto = "P";

                    }

                    //System.out.println("calassocto"+calassocto);

                }


                if (mileStoneType.equals("EM"))
                {
                	setMilestoneEvents(String.valueOf(rs.getString("FK_EVENTASSOC")));
                	setMilestoneEventDescs(rs.getString("EVENT_DESC"));
                	eventName = rs.getString("EVENT_DESC");

                }

                if (mileStoneType.equals("SM"))
                {
                	patientCountString = LC.L_Status+": "+ patientStatusDesc ;
                }

                else if (mileStoneType.equals("AM")) //KM--031808-3328
                {
                	patientCountString = rs.getString("MILESTONE_DESCRIPTION") ;
                }

                else
                {
                	if (!"S".equals(calassocto)){
		                 if (StringUtil.stringToNum(patientCount) <= 0 )
		                {
		                	patientCountString = LC.L_Every + " " + patientStatusDesc + " "+LC.Pat_Patient_Lower;
		                }else
		                {
		                	patientCountString = patientCount + " " + patientStatusDesc + " "+LC.Pat_Patients_Lower;
		                }
                	}

	                 if (calassocto.trim().equals("S"))
	                 {
	                	 patientCountString = "";

	                 }
                }

                 if (! StringUtil.isEmpty(eventName))
                 {
                	 eventName = ", "+LC.L_Event+" : " + eventName;
                 }


                if (mileStoneType.equals("EM") || mileStoneType.equals("VM"))
                {
                	if (!"S".equals(calassocto)){
	                	if (forInvoicing == 1){
	                		milestoneRuleDesc = ruleDesc +  eventStatus + " [" + patientCountString + "]";
	                	} else {
	                		milestoneRuleDesc = LC.L_Calendar +": " + calName + ", " + LC.L_Visit+": " + visitName + eventName + ruleDesc +  eventStatus + " [ " + patientCountString + "]";
	                	}
                	} else {
                		if (forInvoicing == 1){
	                		milestoneRuleDesc = ruleDesc +  eventStatus;
	                	} else {
	                		milestoneRuleDesc = LC.L_Calendar +": " + calName + ", " + LC.L_Visit+": " + visitName + eventName + ruleDesc +  eventStatus;
	                	}
                	}

                }
                else
                {
                	 milestoneRuleDesc =  patientCountString ;
                }

                setMilestoneRuleDescs(milestoneRuleDesc);

                rows++;
                Rlog.debug("milestone", "MilestoneDao.getMilestoneRows rows "+ rows);
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("milestone","MilestoneDao.getMilestoneRows EXCEPTION IN FETCHING FROM Milestone table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

	 
	 public void setHoldBack(Float holdBack) {
		 this.holdBack.add(holdBack);
	 }
	 
	 public ArrayList<Float> getHoldBack() {
			return holdBack;
	}

	public void setHoldBack(ArrayList<Float> holdBack) {
			this.holdBack = holdBack;
	}


//------------------------------------------------JM: 20MAR2008: ------------------------------------------------------------------------------------------------------//


	
	public void getMilestoneRows(int studyId, String mileStoneType, String payType,String milestoneReceivableStatus, String selStatus, String selMilePayType, String srchStrCalName, String srchStrVisitName, String srchStrEvtName, String inActiveFlag,  String orderBy, String orderType) {
        String lsql = null;
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        StringBuffer sbSQL = new StringBuffer();
        String milestoneRuleDesc = "";
        String ruleDesc = "";
        String patientStatusDesc = "";
        String visitName = "";
        String eventName = "";
        String eventStatus = "";
        String patientCountString = "";
        String patientCount = "";
        String calName = "";
        String calassocto = "";



        try {
            conn = getConnection();

            if (StringUtil.isEmpty(selStatus)){

            	selStatus = "";
            }

            if (StringUtil.isEmpty(selMilePayType)){

            	selMilePayType = "";

			}


            if (StringUtil.isEmpty(srchStrCalName)){

            	srchStrCalName = "";

			}


            if (StringUtil.isEmpty(srchStrVisitName)){

            	srchStrVisitName = "";

			}


            if (StringUtil.isEmpty(srchStrEvtName)){

            	srchStrEvtName = "";

			}


            if (StringUtil.isEmpty(inActiveFlag) || inActiveFlag.equals("null")){

            	inActiveFlag = "0";

			}



            if (mileStoneType.equals("PM") || mileStoneType.equals("SM") || mileStoneType.equals("AM") )
            {
            	sbSQL.append("select fk_codelst_milestone_stat, a.PK_MILESTONE,  ");//KM
                sbSQL.append("a.MILESTONE_AMOUNT AS MILESTONE_AMOUNT, ");
                //JM:19Mar08: #FIN13
                sbSQL.append("(select codelst_desc from er_codelst where pk_codelst = MILESTONE_PAYTYPE) payment_type, ");

               	sbSQL.append("a.MILESTONE_COUNT, ");
               	sbSQL.append(" usr_lst(a.milestone_usersto) as usersto, ");
               	sbSQL.append("a.milestone_usersto usertoIds,  milestone_achievedcount, ");

               	//Modified by Manimaran for Enh#FIN10
               	if (mileStoneType.equals("AM")){
               		sbSQL.append(" milestone_description");
               		sbSQL.append(" from ER_MILESTONE a ");
               	}
               	else
               	{
               		sbSQL.append(" b.codelst_desc patientStatus ,milestone_limit");
                	sbSQL.append(" from ER_MILESTONE a , er_codelst b ");
               	}


               	if (! StringUtil.isEmpty(payType))
            	{
            		sbSQL.append(" , er_codelst p ");

            	}

               	sbSQL.append( "where a.FK_STUDY = ? ");
               	sbSQL.append("and a.MILESTONE_TYPE  = '"+mileStoneType+"' ");


               	if (mileStoneType.equals("AM")){
               		sbSQL.append( "and NVL(a.MILEstone_DELFLAG,'Z') <> 'Y' ");
               	}else
               	{
               	   	sbSQL.append( "and NVL(a.MILEstone_DELFLAG,'Z') <> 'Y' and b.pk_codelst = a.milestone_status ");
               	}


            }

            if (mileStoneType.equals("VM"))
            {
            	sbSQL.append(" SELECT * FROM (select fk_codelst_milestone_stat, a.PK_MILESTONE,  a.FK_CAL, c.name  cal_desc,  a.FK_CODELST_RULE, ");
            	sbSQL.append(" b.codelst_desc  rule_desc, b.CODELST_SUBTYP  sub_type, a.MILESTONE_AMOUNT AS MILESTONE_AMOUNT, ");
            	//JM:19Mar08: #FIN13
                sbSQL.append("(select codelst_desc from er_codelst where pk_codelst = MILESTONE_PAYTYPE) payment_type, ");

            	sbSQL.append(" a.MILESTONE_COUNT, usr_lst(a.milestone_usersto) as usersto, a.milestone_usersto usertoIds, ");
            	sbSQL.append(" a.fk_visit fk_visit ,nvl((select v.visit_name from sch_protocol_visit  v where a.fk_visit = v.pk_protocol_visit ),'All') visit_name , ");
            	sbSQL.append(" (select codelst_desc from er_codelst where pk_codelst = milestone_status ) patientStatus , milestone_limit, ");
            	sbSQL.append(" (select codelst_desc from sch_codelst where pk_codelst = milestone_eventstatus ) eventStatus ,milestone_achievedcount ");
            	sbSQL.append(" ,c.event_calassocto from ER_MILESTONE a, er_codelst b , event_Assoc  c " );


            	if (! StringUtil.isEmpty(payType))
            	{
            		sbSQL.append(" , er_codelst p ");

            	}


            	sbSQL.append(" where a.FK_STUDY = ? and a.MILESTONE_TYPE = 'VM' ");
            	sbSQL.append(" and a.FK_CODELST_RULE = b.pk_codelst  and a.fk_cal  = c.event_id and NVL(a.MILEstone_DELFLAG,'Z') <> 'Y' )");

            }


            if (mileStoneType.equals("EM"))
            {
            	sbSQL.append(" SELECT * FROM (select  fk_codelst_milestone_stat, a.PK_MILESTONE,  a.FK_CAL, c.name  cal_desc,  a.FK_CODELST_RULE, ");
           		/* Bug#11399 29-Aug-2012 -Sudhir*/
            	sbSQL.append(" b.codelst_desc  rule_desc, b.CODELST_SUBTYP  sub_type, a.MILESTONE_AMOUNT AS MILESTONE_AMOUNT, ");

            	//JM:19Mar08: #FIN13
                sbSQL.append("(select codelst_desc from er_codelst where pk_codelst = a.MILESTONE_PAYTYPE) payment_type, ");

            	sbSQL.append(" a.FK_EVENTASSOC, a.MILESTONE_COUNT, usr_lst(a.milestone_usersto) as usersto, a.milestone_usersto usertoIds, ");
            	sbSQL.append(" a.fk_visit fk_visit ,nvl((select v.visit_name from sch_protocol_visit  v where a.fk_visit = v.pk_protocol_visit ),'All') visit_name , ");
            	sbSQL.append(" (select codelst_desc from er_codelst where pk_codelst = milestone_status ) patientStatus , milestone_limit ,");
            	sbSQL.append(" (select codelst_desc from sch_codelst where pk_codelst = milestone_eventstatus ) eventStatus ,milestone_achievedcount, ");
            	sbSQL.append(" d.name event_desc,c.event_calassocto ");
            	sbSQL.append(" from ER_MILESTONE a, er_codelst b , event_Assoc  c , event_Assoc  d" );

            	if (! StringUtil.isEmpty(payType))
            	{
            		sbSQL.append(" , er_codelst p ");

            	}

            	sbSQL.append(" where a.FK_STUDY = ? and a.MILESTONE_TYPE = 'EM' and a.FK_EVENTASSOC  = d.event_id ");
            	sbSQL.append(" and a.FK_CODELST_RULE = b.pk_codelst  and a.fk_cal  = c.event_id and NVL(a.MILEstone_DELFLAG,'Z') <> 'Y' )");

            }


            if (! StringUtil.isEmpty(payType))
           	{
           		sbSQL.append(" and p.pk_codelst = a.milestone_paytype and trim(p.codelst_subtyp) = ? ");

           	}

            if (milestoneReceivableStatus.equals("UI"))
            {

            	sbSQL.append(" and not exists (select * from er_invoice_detail det where det.fk_milestone = pk_milestone)  ");
            }

            Rlog.debug("milestone", sbSQL.toString());





            //JM: 20Mar2008: added
            if (inActiveFlag.equals("1")){//Exclude Inactive Items


            	if (mileStoneType.equals("EM") || mileStoneType.equals("VM"))
                {
            		sbSQL.append(" WHERE fk_codelst_milestone_stat <> (select pk_codelst from er_codelst where codelst_type ='milestone_stat' and codelst_subtyp='IA')");


                }else{
                	sbSQL.append(" and fk_codelst_milestone_stat <> (select pk_codelst from er_codelst where codelst_type ='milestone_stat' and codelst_subtyp='IA')" );

                }
            }


            if (inActiveFlag.equals("0")){


            	if (mileStoneType.equals("EM") || mileStoneType.equals("VM")){

            		sbSQL.append(" WHERE fk_codelst_milestone_stat is not null ");

                }else{
                	sbSQL.append(" and fk_codelst_milestone_stat is not null " );

                }
            }



            if (!selStatus.equals("")){

            	sbSQL.append(" and fk_codelst_milestone_stat = "+ selStatus );

            }

            if (!selMilePayType.equals("")){

            	if (mileStoneType.equals("EM") || mileStoneType.equals("VM")){

            		sbSQL.append(" and payment_type = (SELECT codelst_desc FROM er_codelst WHERE pk_codelst = "+ selMilePayType + ")");

                }else{
                	sbSQL.append(" and a.MILESTONE_PAYTYPE = "+ selMilePayType );

                }
            }


            if (!srchStrCalName.equals("")){
	        	sbSQL.append(" and lower(cal_desc) like (lower('%"+ srchStrCalName+"%'))");
            }


            if (!srchStrVisitName.equals("")){
            	sbSQL.append(" and lower(visit_name) like (lower('%"+ srchStrVisitName +"%'))");

            }

            if (!srchStrEvtName.equals("")){
	        	sbSQL.append(" and  lower(event_desc) like(lower('%"+ srchStrEvtName + "%'))");
            }

            /* YK 2MAY2011 :FIXED Bug #5689 */
            if ( ( orderBy.equals("event_desc") || orderBy.equals("visit_name")||orderBy.equals("cal_desc")) && (mileStoneType.equals("PM") || mileStoneType.equals("SM")) )
            {
            	orderBy = "patientStatus";
            }


            if (mileStoneType.equals("AM") && (orderBy.equals("patientStatus") || orderBy.equals("visit_name") ||	orderBy.equals("cal_desc") || orderBy.equals("event_desc")))
            {
            	orderBy = "MILESTONE_DESCRIPTION";
            }
            /* YK 13DEC2010 :FIXED Bug #3379 */
            /*Sorting VM and EM on the basis of eventStatus*/
            /* YK 20MAY2011 :FIXED Bug #3379 */
            if((mileStoneType.equals("VM") && (orderBy.equals("event_desc") || orderBy.equals("patientStatus"))) || ( mileStoneType.equals("EM") && orderBy.equals("patientStatus") )){
            	orderBy = "eventStatus";
            }
            /* YK 13DEC2010 :FIXED Bug #3379 */
            
            //Virendra:Fixed Bug No.#3379. Added lower()
            sbSQL.append( " order by lower(" +  orderBy + ") " + orderType);
           
            pstmt = conn.prepareStatement(sbSQL.toString());
            pstmt.setInt(1, studyId);

            if (! StringUtil.isEmpty(payType))
           	{

            	pstmt.setString(2, payType);

           	}

            String abc = sbSQL.toString();

            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {

            	eventStatus = "";
                setMilestoneIds(new Integer(rs.getInt("PK_MILESTONE")));
                //KM- Apr27
                setMilestoneStatFK(new Integer(rs.getInt("fk_codelst_milestone_stat")));
                //JM: 19Mar08:

                setMilestonePaymentTypes(rs.getString("payment_type") );

                setMilestoneAchievedCounts(rs.getString("milestone_achievedcount"));


                Rlog.debug("milestone", "MilestoneDao.getMilestoneRows rows a");
                patientCount =rs.getString("MILESTONE_COUNT");
                Rlog.debug("milestone", "MilestoneDao.getMilestoneRows rows b");
                setMilestoneCounts(patientCount);
                setMilestoneUsers(rs.getString("USERSTO"));
                Rlog.debug("milestone", "MilestoneDao.getMilestoneRows rows c");
                setMilestoneUserIds(rs.getString("USERTOIDS"));

                if (!mileStoneType.equals("AM")) {
                   Rlog.debug("milestone", "MilestoneDao.getMilestoneRows rows d");
                   setPatientStatuses(rs.getString("patientStatus"));
                   Rlog.debug("milestone", "MilestoneDao.getMilestoneRows rows f");
                   patientStatusDesc = rs.getString("patientStatus");

                }
                Rlog.debug("milestone", "MilestoneDao.getMilestoneRows rows e");
                setMilestoneAmounts(rs.getString("MILESTONE_AMOUNT"));
                Rlog.debug("milestone", "MilestoneDao.getMilestoneRows rows g");

                if (StringUtil.isEmpty(patientCount))
                {
                	patientCount = "";

                }


                if (StringUtil.isEmpty(patientStatusDesc))
                {

                patientStatusDesc = "";

                }
                else
                {
                	patientStatusDesc = "'" + patientStatusDesc  + "'";

                }



                if(mileStoneType.equals("AM"))
                {
                	setMilestoneDescriptions(rs.getString("MILESTONE_DESCRIPTION"));
                }


                if (mileStoneType.equals("EM") || mileStoneType.equals("VM"))
                {
                	setMilestoneCalIds(rs.getString("FK_CAL"));
                	setMilestoneCalDescs(rs.getString("CAL_DESC"));
                	setMilestoneRuleIds(String.valueOf(rs.getString("FK_CODELST_RULE")));
                	setMileStoneSubType(rs.getString("SUB_TYPE")); /* YK 14DEC2010 :FIXED Bug #3379 */
                	calName =  rs.getString("CAL_DESC");
                	setFkVisits(rs.getString("FK_VISIT")); // SV, 11/01
                    setProtocolVisitNames(rs.getString("VISIT_NAME")); // SV, 11/01
                    ruleDesc = rs.getString("RULE_DESC");


                    setMilestoneEventStatuses(rs.getString("eventStatus"));
                    eventStatus = rs.getString("eventStatus");
                    if (StringUtil.isEmpty(eventStatus))
                    	eventStatus = "";

                    calassocto = rs.getString("event_calassocto");
                    //11-Mar-2011 #5922 @Ankit Desc - 'null'treated as a condition.
                    if(calassocto == null)
                    {
                    	calassocto = "";
                    }
                    else if (StringUtil.isEmpty(calassocto))
                    {
                    	calassocto = "P";

                    }

                }


                if (mileStoneType.equals("EM"))
                {
                	setMilestoneEvents(String.valueOf(rs.getString("FK_EVENTASSOC")));
                	setMilestoneEventDescs(rs.getString("EVENT_DESC"));
                	eventName = rs.getString("EVENT_DESC");

                }

                if (mileStoneType.equals("SM"))
                {
                	patientCountString = LC.L_Status+": "+ patientStatusDesc ;
                }

                else
                {
                	if (!"S".equals(calassocto)){
		                 if (StringUtil.stringToNum(patientCount) <= 0)
		                {
		                	 patientCountString = LC.L_Every + " " + patientStatusDesc + " "+LC.Pat_Patient_Lower;
		                }else
		                {
		                	patientCountString = patientCount + " " + patientStatusDesc + " "+ LC.Pat_Patients_Lower;
		                }
                	}

	                 if (calassocto.equals("S"))
	                 {
	                	 patientCountString = "";

	                 }
                }




                if (mileStoneType.equals("EM") || mileStoneType.equals("VM"))
                {
                	 milestoneRuleDesc = ruleDesc +  eventStatus ;

                	 if (calassocto.equals("P"))
	                 {
                		 milestoneRuleDesc  = milestoneRuleDesc  +" [" + patientCountString + "]" ;
	                 }

                }
                else
                {
                	 milestoneRuleDesc =  patientCountString ;
                }

                setMilestoneRuleDescs(milestoneRuleDesc);

                rows++;
                Rlog.debug("milestone", "MilestoneDao.getMilestoneRows(int studyId, String mileStoneType, String payType,String milestoneReceivableStatus, String searchString,  String orderBy, String orderType) rows "+ rows);


            }

            setCRows(rows);

        } catch (SQLException ex) {
            Rlog.fatal("milestone","MilestoneDao.getMilestoneRows(7 params) EXCEPTION IN FETCHING FROM Milestone table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

//JM:

	/**
	 * Retrieves milestone for study.
	 * 
	 */
//BK DFIN13
	public void getMilestoneRowsForStudy(int studyId) {
        String lsql = null;
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        StringBuffer sbSQL = new StringBuffer();
        String mileStoneType="";
        String milestoneRuleDesc = "";
        String ruleDesc = "";
        String patientStatusDesc = "";
        String visitName = "";
        String eventName = "";
        String eventStatus = "";
        String patientCountString = "";
        String patientCount = "";
        String calName = "";
        String calassocto = "";



        try {
            conn = getConnection();
          //Fix #5968: sorting column based on created on date.
        	sbSQL.append("SELECT * FROM(select MILESTONE_TYPE, fk_codelst_milestone_stat, a.PK_MILESTONE, 0 FK_CAL, 0 FK_CODELST_RULE, ");
            sbSQL.append("CASE WHEN MILESTONE_TYPE = 'AM' THEN milestone_description ");
           	sbSQL.append("ELSE '' end as rule_desc, NVL(MILESTONE_AMOUNT,0) MILESTONE_AMOUNT, ");
            sbSQL.append("MILESTONE_PAYTYPE, (select codelst_desc from er_codelst where pk_codelst = MILESTONE_PAYTYPE) payment_type, ");
            sbSQL.append("NVL(MILESTONE_PAYFOR,0) MILESTONE_PAYFOR, (select codelst_desc from er_codelst where pk_codelst = MILESTONE_PAYFOR) payment_for, ");
           	sbSQL.append("a.MILESTONE_COUNT, usr_lst(a.milestone_usersto) as usersto, a.milestone_usersto usertoIds, ");
           	sbSQL.append(" nvl(milestone_achievedcount,0) milestone_achievedcount, ");
           	sbSQL.append("(SELECT b.codelst_desc FROM er_codelst b WHERE b.pk_codelst = FK_CODELST_MILESTONE_STAT) as mileStatus, ");
           	sbSQL.append("NVL(milestone_limit,'') MILESTONE_LIMIT, ");
           	sbSQL.append("0 as fk_visit, '' as visit_name, ");

           	sbSQL.append("CASE WHEN MILESTONE_TYPE = 'AM' THEN 0 ");
           	sbSQL.append("ELSE MILESTONE_STATUS END as fkStatus, (select codelst_desc from sch_codelst where pk_codelst = MILESTONE_STATUS) fkStatusDesc, ");
           	
           	sbSQL.append("0 as fkSecondStatus, '' as fkSecondStatusDesc, ");	
           	
           	sbSQL.append("'' as calendarType, 0 FK_EVENTASSOC, '' event_desc, ");
           	sbSQL.append("a.CREATED_ON CREATED_DATE, ");
           	sbSQL.append("a.MILESTONE_DATE_FROM MILESTONE_DATE_FROM, ");/* FIN-22373 09-Oct-2012 -Sudhir*/
           	sbSQL.append("a.MILESTONE_DATE_TO MILESTONE_DATE_TO ");/* FIN-22373 09-Oct-2012 -Sudhir*/
           	sbSQL.append("from ER_MILESTONE a ");
           	sbSQL.append("where a.FK_STUDY = ? ");
       		sbSQL.append("and NVL(a.MILEstone_DELFLAG,'Z') <> 'Y' ");
       		sbSQL.append("and a.MILESTONE_TYPE not in ('EM','VM') ");
       		
       		sbSQL.append("union ");

        	sbSQL.append("select a.MILESTONE_TYPE, fk_codelst_milestone_stat, a.PK_MILESTONE, a.FK_CAL, a.FK_CODELST_RULE, ");
        	sbSQL.append("(SELECT b.codelst_desc FROM er_codelst b WHERE a.FK_CODELST_RULE = b.pk_codelst) rule_desc, NVL(MILESTONE_AMOUNT,0) MILESTONE_AMOUNT, ");
        	sbSQL.append("MILESTONE_PAYTYPE, (select codelst_desc from er_codelst where pk_codelst = MILESTONE_PAYTYPE) payment_type, ");
            sbSQL.append("NVL(MILESTONE_PAYFOR,0) MILESTONE_PAYFOR, (select codelst_desc from er_codelst where pk_codelst = MILESTONE_PAYFOR) payment_for, ");
           	sbSQL.append("a.MILESTONE_COUNT, usr_lst(a.milestone_usersto) as usersto, a.milestone_usersto usertoIds, ");
        	sbSQL.append("nvl(milestone_achievedcount,0) milestone_achievedcount, ");
        	sbSQL.append("(select codelst_desc from er_codelst where pk_codelst = FK_CODELST_MILESTONE_STAT) mileStatus, NVL(milestone_limit,'') MILESTONE_LIMIT, ");
        	sbSQL.append("a.fk_visit fk_visit, nvl((select v.visit_name from sch_protocol_visit  v where a.fk_visit = v.pk_protocol_visit ),'All') visit_name, ");      	
        	
        	sbSQL.append("milestone_eventstatus fkStatus, (select codelst_desc from sch_codelst where pk_codelst = milestone_eventstatus ) fkStatusDesc, ");
        	
          	sbSQL.append("MILESTONE_STATUS as fkSecondStatus, (select codelst_desc from sch_codelst where pk_codelst = MILESTONE_STATUS) fkSecondStatusDesc, ");
           	
        	sbSQL.append("c.event_calassocto calendarType, 0 FK_EVENTASSOC, '' event_desc, ");
        	sbSQL.append("a.CREATED_ON CREATED_DATE, ");
        	
        	sbSQL.append("a.MILESTONE_DATE_FROM MILESTONE_DATE_FROM, ");/* FIN-22373 09-Oct-2012 -Sudhir*/
           	sbSQL.append("a.MILESTONE_DATE_TO MILESTONE_DATE_TO ");/* FIN-22373 09-Oct-2012 -Sudhir*/
           	
        	sbSQL.append("from ER_MILESTONE a, event_Assoc c " );
        	sbSQL.append("where a.FK_STUDY = ? and a.MILESTONE_TYPE = 'VM' ");
        	sbSQL.append("and a.fk_cal  = c.event_id and NVL(a.MILEstone_DELFLAG,'Z') <> 'Y'  ");

        	sbSQL.append("union ");
        	
        	sbSQL.append(" select a.MILESTONE_TYPE, fk_codelst_milestone_stat, a.PK_MILESTONE,  a.FK_CAL, a.FK_CODELST_RULE, ");
        	sbSQL.append(" (SELECT b.codelst_desc FROM er_codelst b WHERE a.FK_CODELST_RULE = b.pk_codelst) rule_desc, NVL(MILESTONE_AMOUNT,0) MILESTONE_AMOUNT, ");
        	sbSQL.append("MILESTONE_PAYTYPE, (select codelst_desc from er_codelst where pk_codelst = MILESTONE_PAYTYPE) payment_type, ");
            sbSQL.append("NVL(MILESTONE_PAYFOR,0) MILESTONE_PAYFOR, (select codelst_desc from er_codelst where pk_codelst = MILESTONE_PAYFOR) payment_for, ");
           	sbSQL.append("a.MILESTONE_COUNT, usr_lst(a.milestone_usersto) as usersto, a.milestone_usersto usertoIds, ");
        	sbSQL.append("nvl(milestone_achievedcount,0) milestone_achievedcount, ");
        	sbSQL.append("(select codelst_desc from er_codelst where pk_codelst = FK_CODELST_MILESTONE_STAT) mileStatus, NVL(milestone_limit,'') MILESTONE_LIMIT ,");
        	sbSQL.append("a.fk_visit fk_visit ,nvl((select v.visit_name from sch_protocol_visit  v where a.fk_visit = v.pk_protocol_visit ),'All') visit_name , ");

        	sbSQL.append("milestone_eventstatus fkStatus, (select codelst_desc from sch_codelst where pk_codelst = milestone_eventstatus ) fkStatusDesc, ");
        	
          	sbSQL.append("MILESTONE_STATUS as fkSecondStatus, (select codelst_desc from sch_codelst where pk_codelst = MILESTONE_STATUS) fkSecondStatusDesc, ");

        	sbSQL.append("c.event_calassocto calendarType, a.FK_EVENTASSOC,");        	
        	sbSQL.append("(select d.name from event_assoc d where a.FK_EVENTASSOC  = d.event_id ) event_desc, ");
        	sbSQL.append("a.CREATED_ON CREATED_DATE, ");
        	
        	sbSQL.append("a.MILESTONE_DATE_FROM MILESTONE_DATE_FROM, ");/* FIN-22373 09-Oct-2012 -Sudhir*/
           	sbSQL.append("a.MILESTONE_DATE_TO MILESTONE_DATE_TO ");/* FIN-22373 09-Oct-2012 -Sudhir*/
           	
        	sbSQL.append("from ER_MILESTONE a, event_Assoc c " );
        	sbSQL.append(" where a.FK_STUDY = ? and a.MILESTONE_TYPE = 'EM' ");
        	sbSQL.append(" and a.fk_cal  = c.event_id and NVL(a.MILEstone_DELFLAG,'Z') <> 'Y'  )order by CREATED_DATE ");


            /*if (inActiveFlag.equals("1")){//Exclude Inactive Items
               	sbSQL.append(" and fk_codelst_milestone_stat <> (select pk_codelst from er_codelst where codelst_type ='milestone_stat' and codelst_subtyp='IA')" );
            }

            if (inActiveFlag.equals("0")){
            	if (mileStoneType.equals("EM") || mileStoneType.equals("VM")){
            		sbSQL.append(" WHERE fk_codelst_milestone_stat is not null ");
                }else{
                	sbSQL.append(" and fk_codelst_milestone_stat is not null " );
                }
            }*/


            pstmt = conn.prepareStatement(sbSQL.toString());
            pstmt.setInt(1, studyId);
            pstmt.setInt(2, studyId);
            pstmt.setInt(3, studyId);

            String abc = sbSQL.toString();

            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {

            	eventStatus = "";

            	setMilestoneIds(new Integer(rs.getInt("PK_MILESTONE")));
            	mileStoneType = rs.getString("MILESTONE_TYPE");
            	setMilestoneTypes(mileStoneType);
            	setMilestoneRuleDescs(rs.getString("rule_desc"));
            	
            	setMilestoneCalIds(rs.getString("FK_CAL"));
            	setFkVisits(rs.getString("FK_VISIT"));
            	setProtocolVisitNames(rs.getString("VISIT_NAME"));
            	setMilestoneEvents(String.valueOf(rs.getString("FK_EVENTASSOC")));
            	setMilestoneEventDescs(rs.getString("EVENT_DESC"));
            	eventName = rs.getString("EVENT_DESC");
            	
            	setMilestoneAmounts(rs.getString("MILESTONE_AMOUNT"));
            	patientCount = rs.getString("MILESTONE_COUNT");
                if (StringUtil.isEmpty(patientCount))
                {
                	patientCount = "";
                	setMilestoneCounts("0");
                } else 
                	setMilestoneCounts(patientCount);
            	setMilestoneUsers(rs.getString("USERSTO"));
                setMilestoneUserIds(rs.getString("USERTOIDS"));
                setMilestoneAchievedCounts(rs.getString("milestone_achievedcount"));
                
                setMilePaymentTypeIds(rs.getString("MILESTONE_PAYTYPE"));
                setMilestonePaymentTypes(rs.getString("payment_type"));
                setMilePaymentForIds(rs.getString("MILESTONE_PAYFOR"));
                setMilestonePaymentFors(rs.getString("payment_for"));
                
                setMilestoneStatFK(new Integer(rs.getInt("fk_codelst_milestone_stat")));
                //Primary driving status
                setMilePrimaryStatusPK(rs.getInt("fkStatus"));
                setMilestoneEventStatuses(rs.getString("fkStatusDesc"));
                
                //Secondary driving status
                setMileSecondaryStatusPK(rs.getInt("fkSecondStatus"));
                setPatientStatuses(rs.getString("fkSecondStatusDesc"));
                
                patientStatusDesc = rs.getString("mileStatus");
                
               	setMilestoneRuleIds(String.valueOf(rs.getString("FK_CODELST_RULE")));
                setMilestoneDescriptions(rs.getString("mileStatus"));
                setMilestoneLimits(rs.getString("milestone_limit"));
                /* FIN-22373 09-Oct-2012 -Sudhir*/
                if(rs.getDate("MILESTONE_DATE_FROM")!=null)
                	setMilestoneDateFrom(DateUtil.dateToString(rs.getDate("MILESTONE_DATE_FROM")));
                else
                	setMilestoneDateFrom("");
                
                if(rs.getDate("MILESTONE_DATE_TO")!=null)
                	setMilestoneDateTo(DateUtil.dateToString(rs.getDate("MILESTONE_DATE_TO")));
                else
                	setMilestoneDateTo("");
                
                if (StringUtil.isEmpty(patientStatusDesc))
                {
                	patientStatusDesc = "";
                }
                else
                {
                	patientStatusDesc = "'" + patientStatusDesc  + "'";
                }

                ruleDesc = rs.getString("RULE_DESC");
                eventStatus = rs.getString("fkStatus");
                if (StringUtil.isEmpty(eventStatus))
                	eventStatus = "";

                calassocto = rs.getString("calendarType");

                if (StringUtil.isEmpty(calassocto))
                {
                   	calassocto = "P";
                }           	

                if (mileStoneType.equals("SM"))
                {
                	patientCountString = LC.L_Status+": "+ patientStatusDesc ;
                }

                else
                {
                	if (!"S".equals(calassocto)){
		                 if (StringUtil.stringToNum(patientCount) <= 0)
		                {
		                	 patientCountString = LC.L_Every + " " + patientStatusDesc + " "+LC.Pat_Patient_Lower;
		                }else
		                {
		               	 	patientCountString = patientCount + " " + patientStatusDesc + " "+ LC.Pat_Patients_Lower;
		                }
                	}

	                 if (calassocto.equals("S"))
	                 {
	                	 patientCountString = "";

	                 }
                }

                if (mileStoneType.equals("EM") || mileStoneType.equals("VM"))
                {
                	 milestoneRuleDesc = ruleDesc +  eventStatus ;

                	 if (calassocto.equals("P"))
	                 {
                		 milestoneRuleDesc  = milestoneRuleDesc  +" [" + patientCountString + "]" ;
	                 }

                }
                else
                {
                	 milestoneRuleDesc =  patientCountString ;
                }

                //setMilestoneRuleDescs(milestoneRuleDesc);

                rows++;
                Rlog.debug("milestone", "MilestoneDao.getMilestoneRowsForStudy(int studyId) rows "+ rows);


            }

            setCRows(rows);

        } catch (SQLException ex) {
            Rlog.fatal("milestone","MilestoneDao.getMilestoneRowsForStudy(1 param) EXCEPTION IN FETCHING FROM Milestone table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
	
	/*Added by Yogendra Pratap Singh 24/01/13 for INF-22500 */
	/**
	 * getMilestoneRowsForSearch method is used for searching the milestones through the provided 
	 * parameter
	 * @param studyId 
	 * @param paramHhMap
	 */
public void getMilestoneRowsForSearch(int studyId,HashMap<String,String> paramHhMap) {
        String lsql = null;
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        StringBuffer sbSQL = new StringBuffer();
        String mileStoneType="";
        String milestoneRuleDesc = "";
        String ruleDesc = "";
        String patientStatusDesc = "";
        String visitName = "";
        String eventName = "";
        String eventStatus = "";
        String patientCountString = "";
        String patientCount = "";
        String calName = "";
        String calassocto = "";
        
        String mileType 		= 	null;
        String selStatus 		= 	null;
        String selMilePayType 	= 	null;
        String srchStrCalName 	= 	null;
        String srchStrVisitName = 	null;
        String srchStrEvtName 	= 	null;
        String inActiveFlag 	=	null;
        String datefrom			=	null;
        String dateto			=	null;
        if(null!=paramHhMap){
	        mileType		 = 	paramHhMap.get("mileType");
	        selStatus 		 = 	paramHhMap.get("msStatus");
	        selMilePayType 	 = 	paramHhMap.get("msPayType");
	        srchStrCalName 	 = 	paramHhMap.get("calName");
	        srchStrVisitName = 	paramHhMap.get("visitName");
	        srchStrEvtName 	 = 	paramHhMap.get("evtName");
	        inActiveFlag 	 = 	paramHhMap.get("inactiveMiles");
	        datefrom		 = 	paramHhMap.get("datefrom");
	        dateto			 =	paramHhMap.get("dateto");
        }
        if(StringUtil.isEmpty(mileType)){
        	mileType = "";
        }
        if (StringUtil.isEmpty(selStatus)){
        	selStatus = "";
        }
        if (StringUtil.isEmpty(selMilePayType)){
        	selMilePayType = "";
		}
        if (StringUtil.isEmpty(srchStrCalName)){
        	srchStrCalName = "";
		}
        if (StringUtil.isEmpty(srchStrVisitName)){
        	srchStrVisitName = "";
		}
        if (StringUtil.isEmpty(srchStrEvtName)){
        	srchStrEvtName = "";
		}
        if (StringUtil.isEmpty(datefrom)){
        	datefrom = "";
        }
        if (StringUtil.isEmpty(dateto)){
        	dateto = "";
        }
        if (StringUtil.isEmpty(inActiveFlag) || inActiveFlag.equals("null")){
        	inActiveFlag = "0";
		}

        try {
            conn = getConnection();
            
            //sbSQL.append("SELECT * FROM(" );
            if(mileType.equals("PM") || mileType.equals("SM") || mileType.equals("AM") ){
            sbSQL.append("SELECT * FROM( select MILESTONE_TYPE,(select  count(*) as m from er_invoice_detail where fk_milestone=a.PK_MILESTONE) isInvoiceGenerated,(select count(*) as m from ER_MILEPAYMENT_DETAILS where MP_LINKTO_ID=a.PK_MILESTONE ) isReconciledAmount, fk_codelst_milestone_stat, a.PK_MILESTONE, 0 FK_CAL, ''  cal_desc, 0 FK_CODELST_RULE, ");
            sbSQL.append("CASE WHEN MILESTONE_TYPE = 'AM' THEN milestone_description ");
           	sbSQL.append("ELSE '' end as rule_desc, NVL(MILESTONE_AMOUNT,0) MILESTONE_AMOUNT,NVL(MILESTONE_HOLDBACK,0.00) MILESTONE_HOLDBACK, ");
            sbSQL.append("MILESTONE_PAYTYPE, (select codelst_desc from er_codelst where pk_codelst = MILESTONE_PAYTYPE) payment_type, ");
            sbSQL.append("NVL(MILESTONE_PAYFOR,0) MILESTONE_PAYFOR, (select codelst_desc from er_codelst where pk_codelst = MILESTONE_PAYFOR) payment_for, ");
           	sbSQL.append("a.MILESTONE_COUNT, usr_lst(a.milestone_usersto) as usersto, a.milestone_usersto usertoIds, ");
           	sbSQL.append(" nvl(milestone_achievedcount,0) milestone_achievedcount, ");
           	sbSQL.append("(SELECT b.codelst_desc FROM er_codelst b WHERE b.pk_codelst = FK_CODELST_MILESTONE_STAT) as mileStatus, ");
           	sbSQL.append("NVL(milestone_limit,'') MILESTONE_LIMIT, ");
           	sbSQL.append("0 as fk_visit, '' as visit_name, ");

           	sbSQL.append("CASE WHEN MILESTONE_TYPE = 'AM' THEN 0 ");
           	sbSQL.append("ELSE MILESTONE_STATUS END as fkStatus, (select codelst_desc from sch_codelst where pk_codelst = MILESTONE_STATUS) fkStatusDesc, ");
           	
           	sbSQL.append("0 as fkSecondStatus, '' as fkSecondStatusDesc, ");	
           	
           	sbSQL.append("'' as calendarType, 0 FK_EVENTASSOC, '' event_desc, ");
           	sbSQL.append("a.CREATED_ON CREATED_DATE, ");
           	sbSQL.append("a.MILESTONE_DATE_FROM MILESTONE_DATE_FROM, ");/* FIN-22373 09-Oct-2012 -Sudhir*/
           	sbSQL.append("a.MILESTONE_DATE_TO MILESTONE_DATE_TO ");/* FIN-22373 09-Oct-2012 -Sudhir*/
           	sbSQL.append("from ER_MILESTONE a ");
           	if (!StringUtil.isEmpty(srchStrCalName) || !StringUtil.isEmpty(srchStrEvtName)){
           		sbSQL.append(" , event_Assoc d  ");
            }
           	sbSQL.append("where a.FK_STUDY = ? ");
       		sbSQL.append("and NVL(a.MILEstone_DELFLAG,'Z') <> 'Y' ");
       		sbSQL.append("and a.MILESTONE_TYPE = '"+mileType+"'");
       		
       		/*Changes Done By Yogendra */
       		//MILESTONE Status
       		if(!StringUtil.isEmpty(selStatus)){
       		sbSQL.append(" AND FK_CODELST_MILESTONE_STAT="+selStatus);
       		}

       		//MILESTONE Pay Type
       		if(!StringUtil.isEmpty(selMilePayType)){
       			sbSQL.append(" AND MILESTONE_PAYTYPE="+selMilePayType);
       		}
       		//Calendar Name
       		if (!StringUtil.isEmpty(srchStrCalName) || !StringUtil.isEmpty(srchStrEvtName)){
       			sbSQL.append(" and a.FK_CAL  = d.event_id  ");
            }
       		//Visit Name
       		if(!StringUtil.isEmpty(srchStrVisitName)){
       			sbSQL.append(" AND  lower(NVL( (SELECT v.visit_name FROM sch_protocol_visit v WHERE a.fk_visit = v.pk_protocol_visit ),'All'))like lower('%"+srchStrVisitName.trim()+"%')");
       			if (StringUtil.isEmpty(srchStrEvtName) && StringUtil.isEmpty(srchStrCalName)){
       				sbSQL.append(" AND fk_visit <> 0 ");
       			}
       		}
       		
       		//Date Search Filter Change For Bug#15713 : By:Yogendra Pratap Singh
       		if (!StringUtil.isEmpty(datefrom) && !StringUtil.isEmpty(dateto)){
       			sbSQL.append( " AND ( (TRUNC((a.MILESTONE_DATE_FROM)) BETWEEN TRUNC((F_TO_DATE('"+datefrom+"'))) AND TRUNC(F_TO_DATE('"+dateto+"'))) " );
       			sbSQL.append( " AND TRUNC((a.MILESTONE_DATE_TO)) BETWEEN TRUNC((F_TO_DATE('"+datefrom+"'))) AND TRUNC(F_TO_DATE('"+dateto+"')) )" );
       			
       		}else{
       			if(!StringUtil.isEmpty(datefrom) && StringUtil.isEmpty(dateto)){
       				sbSQL.append( " AND trunc(a.MILESTONE_DATE_FROM) >= trunc(F_TO_DATE('"+datefrom+"')) ");
       			}else if(StringUtil.isEmpty(datefrom) && !StringUtil.isEmpty(dateto)){
       				sbSQL.append( " AND trunc(a.MILESTONE_DATE_TO) <= trunc(F_TO_DATE('"+dateto+"')) ");
       			}
       		}
       		
       		//Exclude Inactive Items
       		if (inActiveFlag.equals("1")){
       			sbSQL.append(" and fk_codelst_milestone_stat <> (select pk_codelst from er_codelst where codelst_type ='milestone_stat' and codelst_subtyp='IA')");
       		}
       		if (inActiveFlag.equals("0")){
       			sbSQL.append(" and fk_codelst_milestone_stat is not null ");
       		}
       		sbSQL.append(" ) order by CREATED_DATE ");
            }
       		//sbSQL.append("union ");

            if(mileType.equals("VM")  ){
        	sbSQL.append("SELECT * FROM( select a.MILESTONE_TYPE,(select count(*) as m from er_invoice_detail where fk_milestone=a.PK_MILESTONE) isInvoiceGenerated,(select count(*) as m from ER_MILEPAYMENT_DETAILS where MP_LINKTO_ID=a.PK_MILESTONE ) isReconciledAmount, fk_codelst_milestone_stat, a.PK_MILESTONE, a.FK_CAL, c.name  cal_desc, a.FK_CODELST_RULE, ");
        	sbSQL.append("(SELECT b.codelst_desc FROM er_codelst b WHERE a.FK_CODELST_RULE = b.pk_codelst) rule_desc, NVL(MILESTONE_AMOUNT,0) MILESTONE_AMOUNT,NVL(MILESTONE_HOLDBACK,0.00) MILESTONE_HOLDBACK, ");
        	sbSQL.append("MILESTONE_PAYTYPE, (select codelst_desc from er_codelst where pk_codelst = MILESTONE_PAYTYPE) payment_type, ");
            sbSQL.append("NVL(MILESTONE_PAYFOR,0) MILESTONE_PAYFOR, (select codelst_desc from er_codelst where pk_codelst = MILESTONE_PAYFOR) payment_for, ");
           	sbSQL.append("a.MILESTONE_COUNT, usr_lst(a.milestone_usersto) as usersto, a.milestone_usersto usertoIds, ");
        	sbSQL.append("nvl(milestone_achievedcount,0) milestone_achievedcount, ");
        	sbSQL.append("(select codelst_desc from er_codelst where pk_codelst = FK_CODELST_MILESTONE_STAT) mileStatus, NVL(milestone_limit,'') MILESTONE_LIMIT, ");
        	sbSQL.append("a.fk_visit fk_visit, nvl((select v.visit_name from sch_protocol_visit  v where a.fk_visit = v.pk_protocol_visit ),'All') visit_name, ");      	
        	
        	sbSQL.append("milestone_eventstatus fkStatus, (select codelst_desc from sch_codelst where pk_codelst = milestone_eventstatus ) fkStatusDesc, ");
        	
          	sbSQL.append("MILESTONE_STATUS as fkSecondStatus, (select codelst_desc from sch_codelst where pk_codelst = MILESTONE_STATUS) fkSecondStatusDesc, ");
           	
        	sbSQL.append("c.event_calassocto calendarType, 0 FK_EVENTASSOC, '' event_desc, ");
        	sbSQL.append("a.CREATED_ON CREATED_DATE, ");
        	
        	sbSQL.append("a.MILESTONE_DATE_FROM MILESTONE_DATE_FROM, ");/* FIN-22373 09-Oct-2012 -Sudhir*/
           	sbSQL.append("a.MILESTONE_DATE_TO MILESTONE_DATE_TO ");/* FIN-22373 09-Oct-2012 -Sudhir*/
           	
        	sbSQL.append("from ER_MILESTONE a, event_Assoc c " );
        	if(!StringUtil.isEmpty(srchStrEvtName)){
        		sbSQL.append(", event_Assoc d " );
        	}
        	sbSQL.append("where a.FK_STUDY = ? and a.MILESTONE_TYPE = 'VM' ");
        	sbSQL.append("and a.fk_cal  = c.event_id and NVL(a.MILEstone_DELFLAG,'Z') <> 'Y'  ");
        	
        	/*Changes Done By Yogendra */
       		//MILESTONE Status
       		if(!StringUtil.isEmpty(selStatus)){
       		sbSQL.append(" AND FK_CODELST_MILESTONE_STAT="+selStatus);
       		}
       		//MILESTONE Pay Type
       		if(!StringUtil.isEmpty(selMilePayType)){
       			sbSQL.append(" AND MILESTONE_PAYTYPE="+selMilePayType);
       		}
       		//Calendar Name
       		if (!StringUtil.isEmpty(srchStrCalName)){
       			sbSQL.append(" and lower(c.name) like (lower('%"+ srchStrCalName+"%'))");
            }
       		//Event Name
       		if(!StringUtil.isEmpty(srchStrEvtName)){
       			sbSQL.append(" and a.FK_EVENTASSOC = d.event_id  and  lower(d.name) like(lower('%"+ srchStrEvtName + "%'))");
       		}
       		//Visit Name
       		if(!StringUtil.isEmpty(srchStrVisitName)){
       			sbSQL.append(" AND  lower(NVL( (SELECT v.visit_name FROM sch_protocol_visit v WHERE a.fk_visit = v.pk_protocol_visit ),'All'))like lower('%"+srchStrVisitName.trim()+"%')");
       		}
       		
       		//Date Search Filter Change For Bug#15713 : By:Yogendra Pratap Singh
       		if (!StringUtil.isEmpty(datefrom) && !StringUtil.isEmpty(dateto)){
       			sbSQL.append( " AND ( (TRUNC((a.MILESTONE_DATE_FROM)) BETWEEN TRUNC((F_TO_DATE('"+datefrom+"'))) AND TRUNC(F_TO_DATE('"+dateto+"'))) " );
       			sbSQL.append( " AND TRUNC((a.MILESTONE_DATE_TO)) BETWEEN TRUNC((F_TO_DATE('"+datefrom+"'))) AND TRUNC(F_TO_DATE('"+dateto+"')) )" );
       			
       		}else{
       			if(!StringUtil.isEmpty(datefrom) && StringUtil.isEmpty(dateto)){
       				sbSQL.append( " AND trunc(a.MILESTONE_DATE_FROM) >= trunc(F_TO_DATE('"+datefrom+"')) ");
       			}else if(StringUtil.isEmpty(datefrom) && !StringUtil.isEmpty(dateto)){
       				sbSQL.append( " AND trunc(a.MILESTONE_DATE_TO) <= trunc(F_TO_DATE('"+dateto+"')) ");
       			}
       		}
       		
       		//Exclude Inactive Items
       		if (inActiveFlag.equals("1")){
       			sbSQL.append(" and fk_codelst_milestone_stat <> (select pk_codelst from er_codelst where codelst_type ='milestone_stat' and codelst_subtyp='IA')");
       		}
       		if (inActiveFlag.equals("0")){
       			sbSQL.append(" and fk_codelst_milestone_stat is not null ");
       		}
       		sbSQL.append(" ) order by CREATED_DATE ");
            }
        	//sbSQL.append("union ");
            if(mileType.equals("EM") ){
        	sbSQL.append("SELECT * FROM( select a.MILESTONE_TYPE,(select  count(*) as m from er_invoice_detail where fk_milestone=a.PK_MILESTONE) isInvoiceGenerated, (select count(*) as m from ER_MILEPAYMENT_DETAILS where MP_LINKTO_ID=a.PK_MILESTONE ) isReconciledAmount, fk_codelst_milestone_stat, a.PK_MILESTONE,  a.FK_CAL, c.name  cal_desc, a.FK_CODELST_RULE, ");
        	sbSQL.append(" (SELECT b.codelst_desc FROM er_codelst b WHERE a.FK_CODELST_RULE = b.pk_codelst) rule_desc, NVL(MILESTONE_AMOUNT,0) MILESTONE_AMOUNT,NVL(MILESTONE_HOLDBACK,0.00) MILESTONE_HOLDBACK, ");
        	sbSQL.append("MILESTONE_PAYTYPE, (select codelst_desc from er_codelst where pk_codelst = MILESTONE_PAYTYPE) payment_type, ");
            sbSQL.append("NVL(MILESTONE_PAYFOR,0) MILESTONE_PAYFOR, (select codelst_desc from er_codelst where pk_codelst = MILESTONE_PAYFOR) payment_for, ");
           	sbSQL.append("a.MILESTONE_COUNT, usr_lst(a.milestone_usersto) as usersto, a.milestone_usersto usertoIds, ");
        	sbSQL.append("nvl(milestone_achievedcount,0) milestone_achievedcount, ");
        	sbSQL.append("(select codelst_desc from er_codelst where pk_codelst = FK_CODELST_MILESTONE_STAT) mileStatus, NVL(milestone_limit,'') MILESTONE_LIMIT ,");
        	sbSQL.append("a.fk_visit fk_visit ,nvl((select v.visit_name from sch_protocol_visit  v where a.fk_visit = v.pk_protocol_visit ),'All') visit_name , ");

        	sbSQL.append("milestone_eventstatus fkStatus, (select codelst_desc from sch_codelst where pk_codelst = milestone_eventstatus ) fkStatusDesc, ");
        	
          	sbSQL.append("MILESTONE_STATUS as fkSecondStatus, (select codelst_desc from sch_codelst where pk_codelst = MILESTONE_STATUS) fkSecondStatusDesc, ");

        	sbSQL.append("c.event_calassocto calendarType, a.FK_EVENTASSOC,");        	
        	sbSQL.append("(select d.name from event_assoc d where a.FK_EVENTASSOC  = d.event_id ) event_desc, ");
        	sbSQL.append("a.CREATED_ON CREATED_DATE, ");
        	
        	sbSQL.append("a.MILESTONE_DATE_FROM MILESTONE_DATE_FROM, ");/* FIN-22373 09-Oct-2012 -Sudhir*/
           	sbSQL.append("a.MILESTONE_DATE_TO MILESTONE_DATE_TO ");/* FIN-22373 09-Oct-2012 -Sudhir*/
           	
        	sbSQL.append("from ER_MILESTONE a, event_Assoc c, event_Assoc d " );
        	sbSQL.append(" where a.FK_STUDY = ? and a.MILESTONE_TYPE = 'EM' ");
        	sbSQL.append(" AND a.FK_EVENTASSOC = d.event_id ");
        	sbSQL.append(" and a.fk_cal  = c.event_id and NVL(a.MILEstone_DELFLAG,'Z') <> 'Y' ");
            
        	/*Changes Done By Yogendra */
        	
       		//MILESTONE Status
       		if(!StringUtil.isEmpty(selStatus)){
       		sbSQL.append(" AND FK_CODELST_MILESTONE_STAT="+selStatus);
       		}
       		//MILESTONE Pay Type
       		if(!StringUtil.isEmpty(selMilePayType)){
       			sbSQL.append(" AND MILESTONE_PAYTYPE="+selMilePayType);
       		}
       		//Calendar Name
       		if (!srchStrCalName.equals("")){
	        	sbSQL.append(" and lower(c.name) like (lower('%"+ srchStrCalName+"%'))");
            }
       		//Event Name
       		if(!StringUtil.isEmpty(srchStrVisitName)){
       			sbSQL.append(" AND  lower(NVL( (SELECT v.visit_name FROM sch_protocol_visit v WHERE a.fk_visit = v.pk_protocol_visit ),'All'))like lower('%"+srchStrVisitName.trim()+"%')");
       		}
       		//Visit Name
       		if(!StringUtil.isEmpty(srchStrEvtName)){
       			sbSQL.append(" and  lower(d.name) like(lower('%"+ srchStrEvtName + "%'))");
       		}
       		
       		//Date Search Filter Change For Bug#15713 : By:Yogendra Pratap Singh
       		if (!StringUtil.isEmpty(datefrom) && !StringUtil.isEmpty(dateto)){
       			sbSQL.append( " AND ( (TRUNC((a.MILESTONE_DATE_FROM)) BETWEEN TRUNC((F_TO_DATE('"+datefrom+"'))) AND TRUNC(F_TO_DATE('"+dateto+"'))) " );
       			sbSQL.append( " AND TRUNC((a.MILESTONE_DATE_TO)) BETWEEN TRUNC((F_TO_DATE('"+datefrom+"'))) AND TRUNC(F_TO_DATE('"+dateto+"')) )" );
       			
       		}else{
       			if(!StringUtil.isEmpty(datefrom) && StringUtil.isEmpty(dateto)){
       				sbSQL.append( " AND trunc(a.MILESTONE_DATE_FROM) >= trunc(F_TO_DATE('"+datefrom+"')) ");
       			}else if(StringUtil.isEmpty(datefrom) && !StringUtil.isEmpty(dateto)){
       				sbSQL.append( " AND trunc(a.MILESTONE_DATE_TO) <= trunc(F_TO_DATE('"+dateto+"')) ");
       			}
       		}
       		
       		//Exclude Inactive Items
       		if (inActiveFlag.equals("1")){
       			sbSQL.append(" and fk_codelst_milestone_stat <> (select pk_codelst from er_codelst where codelst_type ='milestone_stat' and codelst_subtyp='IA')");
       		}
       		if (inActiveFlag.equals("0")){
       			sbSQL.append(" and fk_codelst_milestone_stat is not null ");
       		}
       		sbSQL.append(" ) order by CREATED_DATE ");
            }
          
            pstmt = conn.prepareStatement(sbSQL.toString());
            pstmt.setInt(1, studyId);
            /*pstmt.setInt(2, studyId);
            pstmt.setInt(3, studyId);*/

            String abc = sbSQL.toString();

            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {

            	eventStatus = "";

            	setMilestoneIds(new Integer(rs.getInt("PK_MILESTONE")));
            	mileStoneType = rs.getString("MILESTONE_TYPE");
            	setMilestoneTypes(mileStoneType);
            	setMilestoneRuleDescs(rs.getString("rule_desc"));
            	
            	setMilestoneCalIds(rs.getString("FK_CAL"));
            	setFkVisits(rs.getString("FK_VISIT"));
            	setProtocolVisitNames(rs.getString("VISIT_NAME"));
            	setMilestoneEvents(String.valueOf(rs.getString("FK_EVENTASSOC")));
            	setMilestoneEventDescs(rs.getString("EVENT_DESC"));
            	eventName = rs.getString("EVENT_DESC");
            	setIsInvoiceGenerated(rs.getInt("isInvoiceGenerated"));
            	setIsReconciledAmount(rs.getInt("isReconciledAmount"));
            	setMilestoneAmounts(rs.getString("MILESTONE_AMOUNT"));
            	
            	setHoldBack(rs.getFloat("MILESTONE_HOLDBACK"));
            	
            	patientCount = rs.getString("MILESTONE_COUNT");
                if (StringUtil.isEmpty(patientCount))
                {
                	patientCount = "";
                	setMilestoneCounts("0");
                } else 
                	setMilestoneCounts(patientCount);
            	setMilestoneUsers(rs.getString("USERSTO"));
                setMilestoneUserIds(rs.getString("USERTOIDS"));
                setMilestoneAchievedCounts(rs.getString("milestone_achievedcount"));
                
                setMilePaymentTypeIds(rs.getString("MILESTONE_PAYTYPE"));
                setMilestonePaymentTypes(rs.getString("payment_type"));
                setMilePaymentForIds(rs.getString("MILESTONE_PAYFOR"));
                setMilestonePaymentFors(rs.getString("payment_for"));
                
                setMilestoneStatFK(new Integer(rs.getInt("fk_codelst_milestone_stat")));
                //Primary driving status
                setMilePrimaryStatusPK(rs.getInt("fkStatus"));
                setMilestoneEventStatuses(rs.getString("fkStatusDesc"));
                
                //Secondary driving status
                setMileSecondaryStatusPK(rs.getInt("fkSecondStatus"));
                setPatientStatuses(rs.getString("fkSecondStatusDesc"));
                
                patientStatusDesc = rs.getString("mileStatus");
                
               	setMilestoneRuleIds(String.valueOf(rs.getString("FK_CODELST_RULE")));
                setMilestoneDescriptions(rs.getString("mileStatus"));
                setMilestoneLimits(rs.getString("milestone_limit"));

                if(rs.getDate("MILESTONE_DATE_FROM")!=null)
                	setMilestoneDateFrom(DateUtil.dateToString(rs.getDate("MILESTONE_DATE_FROM")));
                else
                	setMilestoneDateFrom("");
                
                if(rs.getDate("MILESTONE_DATE_TO")!=null)
                	setMilestoneDateTo(DateUtil.dateToString(rs.getDate("MILESTONE_DATE_TO")));
                else
                	setMilestoneDateTo("");
                
                if (StringUtil.isEmpty(patientStatusDesc))
                {
                	patientStatusDesc = "";
                }
                else
                {
                	patientStatusDesc = "'" + patientStatusDesc  + "'";
                }

                ruleDesc = rs.getString("RULE_DESC");
                eventStatus = rs.getString("fkStatus");
                if (StringUtil.isEmpty(eventStatus))
                	eventStatus = "";

                calassocto = rs.getString("calendarType");

                if (StringUtil.isEmpty(calassocto))
                {
                   	calassocto = "P";
                }           	

                if (mileStoneType.equals("SM"))
                {
                	patientCountString = LC.L_Status+": "+ patientStatusDesc ;
                }

                else
                {
                	if (!"S".equals(calassocto)){
		                 if (StringUtil.stringToNum(patientCount) <= 0)
		                {
		                	 patientCountString = LC.L_Every + " " + patientStatusDesc + " "+LC.Pat_Patient_Lower;
		                }else
		                {
		               	 	patientCountString = patientCount + " " + patientStatusDesc + " "+ LC.Pat_Patients_Lower;
		                }
                	}

	                 if (calassocto.equals("S"))
	                 {
	                	 patientCountString = "";

	                 }
                }

                if (mileStoneType.equals("EM") || mileStoneType.equals("VM"))
                {
                	 milestoneRuleDesc = ruleDesc +  eventStatus ;

                	 if (calassocto.equals("P"))
	                 {
                		 milestoneRuleDesc  = milestoneRuleDesc  +" [" + patientCountString + "]" ;
	                 }

                }
                else
                {
                	 milestoneRuleDesc =  patientCountString ;
                }


                rows++;
                Rlog.debug("milestone", "MilestoneDao.getMilestoneRowsForSearch(int studyId,HashMap) rows "+ rows);


            }

            setCRows(rows);

        } catch (SQLException ex) {
            Rlog.fatal("milestone","MilestoneDao.getMilestoneRowsForSearch(2 param) EXCEPTION IN FETCHING FROM Milestone table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

	
	/*Added by Sonia Abrol. 11/23/05 */
	/** gets milestone details for a milestone
	 * @param id
	 * */
	public void getMilestoneDetails(int milestoneId) {

        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        StringBuffer sbSQL = new StringBuffer();


        String patientCount = "";

        try {
            conn = getConnection();

            	sbSQL.append("select Pkg_Milestone_New.f_getMilestoneDesc( a.PK_MILESTONE) mileDesc,  ");
                sbSQL.append("a.MILESTONE_AMOUNT AS MILESTONE_AMOUNT, ");
               	sbSQL.append("a.MILESTONE_COUNT, ");
               	sbSQL.append(" usr_lst(a.milestone_usersto) as usersto, ");
               	sbSQL.append("a.milestone_usersto usertoIds,  ");
               	sbSQL.append(" milestone_limit ,milestone_holdback, milestone_achievedcount");
               	sbSQL.append(" from ER_MILESTONE a ");


               	sbSQL.append( "where a.PK_MILESTONE = ? ");



            Rlog.debug("milestone", sbSQL.toString());

            pstmt = conn.prepareStatement(sbSQL.toString());
            pstmt.setInt(1, milestoneId);



            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setMilestoneIds(new Integer(milestoneId));

                patientCount =rs.getString("MILESTONE_COUNT");

                setMilestoneCounts(patientCount);

                setMilestoneUsers(rs.getString("USERSTO"));

                setMilestoneUserIds(rs.getString("USERTOIDS"));

                setMilestoneAmounts(rs.getString("MILESTONE_AMOUNT"));
                
                setHoldBack(rs.getFloat("milestone_holdback")); 
                this.setMilestoneAchievedCounts(rs.getString("milestone_achievedcount"));

                setMilestoneRuleDescs(rs.getString("mileDesc"));

                rows++;
                Rlog.debug("milestone", "MilestoneDao.getMilestoneDetails rows "+ rows);
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("milestone","MilestoneDao.getMilestoneDetails EXCEPTION IN FETCHING FROM Milestone table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

	/* Sonia Abrol, 12/07/06
	 * Apply user notification settings of a milestone to all milestones in a study*/
    public int applyNotificationSettingsToAll(String milestoneID, String userIds, String studyId, String modifiedBy, String ipAdd ) {
        String lsql = null;
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            lsql = "update er_milestone set milestone_usersto = ?, last_modified_by = ? ,ip_add = ? where fk_study = ? and pk_milestone <> ? ";


            pstmt = conn.prepareStatement(lsql);
            pstmt.setString(1, userIds);
            pstmt.setString(2, modifiedBy);
            pstmt.setString(3, ipAdd);

            pstmt.setString(4, studyId);
            pstmt.setString(5, milestoneID);
            rows = pstmt.executeUpdate();


            setCRows(rows);
            return rows;

        } catch (SQLException ex) {
            Rlog.fatal("milestone",
                    "MilestoneDao.applyNotificationSettingsToAll EXCEPTION IN DAO" + ex);
            return -1;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }


    public static int createMultiMilestones(int userId, String ipAdd,MilestoneDao md, int pkbudget, int study,int bgtcalId) {

        Connection conn = null;
    	CallableStatement cstmt = null;
        int ret = 0;
        int count = 0;
        String mileType = "";
        int VMChecked = 0;
        int EMChecked = 0;
        int AMChecked = 0;
        String dateFrom = "";
        String dateTo = "";
        int milestoneStatus = 0;

        try {
            conn = getConnection();



            ArrayList arRule = new ArrayList();


            arRule = md.getMilestoneRuleIds();

            count = arRule.size();



            if (count > 0)
            {
            	ArrayList <Float> arrHoldback = new ArrayList<Float>();
            	ArrayList arevStatus = new ArrayList();
            	ArrayList arPatCount = new ArrayList();
            	ArrayList arPatStatus = new ArrayList();
            	ArrayList arLimit = new ArrayList();
            	ArrayList arPaymentType = new ArrayList();
            	ArrayList arPaymentFor = new ArrayList();
            	ArrayList arMilestoneType  = new ArrayList();
            	ArrayList arMilestoneDateFrom  = new ArrayList();
            	ArrayList arMilestoneDateTo  = new ArrayList();
            	ArrayList<Integer> milestoneStatusLst = new ArrayList<Integer>();
            	java.sql.Date df = null;
            	java.sql.Date dt = null;
            	/*FIN-22375 Date :16 Aug 2012 By : Yogendra Pratap */
            	/*Get the checked(as 1) and unchecked(as 0) status for the sponsor amount */
            	Map<String, Integer> sponsorAmountHm = md.getSopnsorAmountChkStatus();
            	VMChecked = sponsorAmountHm.get( "VMChecked" );
            	EMChecked = sponsorAmountHm.get( "EMChecked" );
            	AMChecked = sponsorAmountHm.get( "AMChecked" );



            	arevStatus = md.getMilestoneEventStatuses();
            	arPatCount = md.getMilestoneCounts();
            	arPatStatus = md.getPatientStatuses();
            	arLimit = md.getMilestoneLimits();
            	arPaymentType = md.getMilestonePaymentTypes();
            	arPaymentFor = md.getMilestonePaymentFors();
            	arMilestoneType = md.getMilestoneTypes();
            	arrHoldback = md.getHoldBack();
            	arMilestoneDateFrom = md.getMilestoneDateFrom();
            	arMilestoneDateTo = md.getMilestoneDateTo();
            	
            	milestoneStatusLst = md.getMilestoneStatFK();

	             for ( int c=0;c<count;c++)
	             {
	            	
	            	mileType = (String) arMilestoneType.get(c);

	            	cstmt = null;

	            	if (mileType.equals("VM"))
	            	{
	            		cstmt = conn.prepareCall("{call Pkg_Milestone_New.SP_CREATE_VM_MILESTONES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
	            	}
	            	else if (mileType.equals("EM"))
	            	{
	            		cstmt = conn.prepareCall("{call Pkg_Milestone_New.SP_CREATE_EM_MILESTONES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");


	            	}else if (mileType.equals("AM"))
	            	{
	            		cstmt = conn.prepareCall("{call Pkg_Milestone_New.SP_CREATE_AM_MILESTONES(?,?,?,?,?,?,?,?,?,?,?)}");

	            	}



	            	cstmt.setInt(1, pkbudget);
	            	cstmt.setInt(2, study);
	            	cstmt.setInt(3, userId);
	            	cstmt.setString(4, ipAdd);



	            	if (! mileType.equals("AM"))
	            	{
	            		cstmt.setInt(5, bgtcalId);
	            		cstmt.setString(6, (String) arRule.get(c));
	            		cstmt.setString(7, (String) arevStatus.get(c));
	            		cstmt.setString(8, (String) arPatCount.get(c));
	            		cstmt.setString(9, (String) arPatStatus.get(c));
	            		cstmt.setString(10, (String) arLimit.get(c));
	            		cstmt.setString(11, (String) arPaymentType.get(c));
		                cstmt.setString(12, (String) arPaymentFor.get(c));

		                /*FIN-22375 Date :16 Aug 2012 By : Yogendra Pratap */
		                /* Now set the values for checked and unchecked statuses 
		                 * values are :
		                 * 1 for checked 
		                 * 0 for unchecked */
		                if (mileType.equals("VM")){
		                	cstmt.setInt(13 , VMChecked);
		            	}else if (mileType.equals("EM")){
		            		cstmt.setInt(13 , EMChecked);
		            	}
		                
		                df = null;
		                dt = null;
		                
		                if( arMilestoneDateFrom.get(c)!= null &&  !((String)arMilestoneDateFrom.get(c)).equals("") )
		                	df = new java.sql.Date( DateUtil.stringToDate((String) arMilestoneDateFrom.get(c)).getTime());
		                if( arMilestoneDateTo.get(c)!= null && !((String)arMilestoneDateTo.get(c)).equals(""))
		                    dt = new java.sql.Date( DateUtil.stringToDate((String) arMilestoneDateTo.get(c)).getTime());
		                
		                cstmt.setDate(14, df);
		                cstmt.setDate(15, dt);
		                cstmt.setInt(16, milestoneStatusLst.get(c));
		                cstmt.setFloat(17, arrHoldback.get(c));
			            cstmt.registerOutParameter(18, java.sql.Types.INTEGER);
			            cstmt.execute();
			            ret = cstmt.getInt(18);


	            	}
	            	else
	            	{
	            		cstmt.setString(5, (String) arPaymentType.get(c));
		                cstmt.setString(6, (String) arPaymentFor.get(c));
			            cstmt.registerOutParameter(7, java.sql.Types.INTEGER);
			            cstmt.setInt(8, bgtcalId);
			            /*FIN-22375 Date :16 Aug 2012 By : Yogendra Pratap */
			            cstmt.setInt(9 , AMChecked);
			            cstmt.setInt(10, milestoneStatusLst.get(c));
			            cstmt.setFloat(11, arrHoldback.get(c));
			            cstmt.execute();
			            ret = cstmt.getInt(7);

	            	}

		            cstmt.close();
	             }
            }

            return ret;

        } catch (Exception e) {
            Rlog.fatal(
                            "milestone",
                            "EXCEPTION in MilestoneDao.createMultiMilestones, excecuting Stored Procedure "
                                    + e);
            return -1;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

	public ArrayList getMilestoneLimits() {
		return milestoneLimits;
	}

	public void setMilestoneLimits(ArrayList milestoneLimits) {
		this.milestoneLimits = milestoneLimits;
	}

	public void setMilestoneLimits(String milestoneLimit)
	{
		this.milestoneLimits.add(milestoneLimit);
	}

	public ArrayList getMilestonePaymentFors() {
		return milestonePaymentFors;
	}

	public void setMilestonePaymentFors(ArrayList milestonePaymentFors) {
		this.milestonePaymentFors = milestonePaymentFors;
	}

	public ArrayList getMilePaymentTypeIds() {
		return milePaymentTypeIds;
	}

	public void setMilePaymentTypeIds(ArrayList milePaymentTypeIds) {
		this.milePaymentTypeIds = milePaymentTypeIds;
	}

	public void setMilePaymentTypeIds(String milePaymentTypeId) {
		this.milePaymentTypeIds.add(milePaymentTypeId);
	}
	
	public ArrayList getMilestonePaymentTypes() {
		return milestonePaymentTypes;
	}

	public void setMilestonePaymentTypes(ArrayList milestonePaymentTypes) {
		this.milestonePaymentTypes = milestonePaymentTypes;
	}

	public void setMilestonePaymentTypes(String milestonePaymentType) {
		this.milestonePaymentTypes.add(milestonePaymentType);
	}

	public ArrayList getMilePaymentForIds() {
		return milePaymentForIds;
	}

	public void setMilePaymentForIds(ArrayList milePaymentForIds) {
		this.milePaymentForIds = milePaymentForIds;
	}

	public void setMilePaymentForIds(String milePaymentForId) {
		this.milePaymentForIds.add(milePaymentForId);
	}
	
	public void setMilestonePaymentFors(String milestonePaymentFor) {
		this.milestonePaymentFors.add(milestonePaymentFor);
	}

	public ArrayList getMilestoneEventStatuses() {
		return milestoneEventStatuses;
	}

	public void setMilestoneEventStatuses(ArrayList milestoneEventStatuses) {
		this.milestoneEventStatuses = milestoneEventStatuses;
	}

	public void setMilestoneEventStatuses(String milestoneEventStatus) {
		this.milestoneEventStatuses.add(milestoneEventStatus);
	}

	public ArrayList getTotalInvoicedAmounts() {
		return totalInvoicedAmounts;
	}

	public void setTotalInvoicedAmounts(ArrayList totalInvoicedAmounts) {
		this.totalInvoicedAmounts = totalInvoicedAmounts;
	}

	public void setTotalInvoicedAmounts(String totalInvoicedAmount) {
		this.totalInvoicedAmounts.add(totalInvoicedAmount);
	}


	public ArrayList<Integer> getIsInvoiceGenerated() {
		return isInvoiceGenerated;
	}

	public void setIsInvoiceGenerated(Integer isInvoiceGenerated) {
		this.isInvoiceGenerated.add(isInvoiceGenerated);
	}
	
	public void setIsInvoiceGenerated(ArrayList<Integer> isInvoiceGenerated) {
		this.isInvoiceGenerated = isInvoiceGenerated;
	}

	public void setIsReconciledAmount(Integer isReconciledAmount) {
		this.isReconciledAmount.add(isReconciledAmount);
	}
	
	public ArrayList getIsReconciledAmount() {
		return isReconciledAmount;
	}

	public void setIsReconciledAmount(ArrayList<Integer> isReconciledAmount) {
		this.isReconciledAmount = isReconciledAmount;
	}
	
	/** Sonia Abrol, 08/27/08
	 * delete milestione achievement records from er_mileachieved
	 * @param arPkmileAch Array of PKs to be deleted */

    public int deleteMSAch(String[] arPkmileAch ) {
        String sql = null;
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            sql = "delete from er_mileachieved where pk_mileachieved  = ?  ";


            pstmt = conn.prepareStatement(sql);

            for (int k = 0; k<arPkmileAch.length ; k++)
            {
	            pstmt.setString(1, arPkmileAch[k]);
	            pstmt.execute();
            }


            return 0;

        } catch (SQLException ex) {
            Rlog.fatal("milestone",
                    "MilestoneDao.deleteMSAch EXCEPTION IN DAO" + ex);
            ex.printStackTrace();
            return -1;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /*YK 29Mar2011 -DFIN20 */
    public void getStudyMileStoneForSetStatus(int studyId) {
        String lsql = null;
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        StringBuffer sbSQL = new StringBuffer();
    
        try {
            conn = getConnection();

            sbSQL.append("select a.PK_MILESTONE");
            sbSQL.append(" from ER_MILESTONE a ");
            sbSQL.append("where a.FK_STUDY = ? ");
            sbSQL.append(" and  a.FK_CODELST_MILESTONE_STAT not in (select PK_CODELST from ER_CODELST where CODELST_TYPE='milestone_stat' and CODELST_SUBTYP in ('A','IA')) ");
            sbSQL.append("and NVL(a.MILEstone_DELFLAG,'Z') <> 'Y' ");
            sbSQL.append("and a.MILESTONE_TYPE not in ('EM','VM') ");

            sbSQL.append("union ");

            sbSQL.append("SELECT * FROM (select a.PK_MILESTONE ");
            sbSQL.append(" from ER_MILESTONE a, event_Assoc c ");
            sbSQL.append("where a.FK_STUDY = ? and a.MILESTONE_TYPE = 'VM' ");
            sbSQL.append(" and  a.FK_CODELST_MILESTONE_STAT not in (select PK_CODELST from ER_CODELST where CODELST_TYPE='milestone_stat' and CODELST_SUBTYP in ('A','IA')) ");
            sbSQL.append("and a.fk_cal  = c.event_id and NVL(a.MILEstone_DELFLAG,'Z') <> 'Y' )");

            sbSQL.append("union ");

            sbSQL.append(" SELECT * FROM (select a.PK_MILESTONE");
            sbSQL.append(" from ER_MILESTONE a, event_Assoc c ");
            sbSQL.append(" where a.FK_STUDY = ? and a.MILESTONE_TYPE = 'EM' ");
            sbSQL.append(" and  a.FK_CODELST_MILESTONE_STAT not in (select PK_CODELST from ER_CODELST where CODELST_TYPE='milestone_stat' and CODELST_SUBTYP in ('A','IA')) ");
            sbSQL.append(" and a.fk_cal  = c.event_id and NVL(a.MILEstone_DELFLAG,'Z') <> 'Y' )");

            pstmt = conn.prepareStatement(sbSQL.toString());
            pstmt.setInt(1, studyId);
            pstmt.setInt(2, studyId);
            pstmt.setInt(3, studyId);

            String abc = sbSQL.toString();
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {

                setMilestoneIds(new Integer(rs.getInt("PK_MILESTONE")));
                rows++;
                Rlog.debug("milestone", "MilestoneDao.getStudyMileStoneForSetStatus(int studyId) rows "+ rows);
            }
            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("milestone","MilestoneDao.getStudyMileStoneForSetStatus(1 param) EXCEPTION IN FETCHING FROM Milestone table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }

    public void getMilestoneRowsEM(int studyId, String payType,String milestoneReceivableStatus , String CovType) {
		String lsql = null;
		int rows = 0;
		PreparedStatement pstmt = null;
		Connection conn = null;
		
		StringBuffer sbSQL = new StringBuffer();
		String milestoneRuleDesc = "";
		String ruleDesc = "";
		String patientStatusDesc = "";
		String visitName = "";
		String eventName = "";
		String eventStatus = "";
		String patientCountString = "";
		String patientCount = "";
		String calName = "";
		String calassocto = "";
		String CoverType = "";
		String totalAmountInvColumn = " (select sum(nvl(amount_invoiced,0) + nvl(amount_holdback,0)) from er_invoice_detail det, er_invoice inv where det.fk_milestone = pk_milestone AND inv.pk_invoice = det.fk_inv and nvl(detail_type,'H')='H') amount_invoiced ";

		try {
			conn = getConnection();

			sbSQL.append("select PK_MILESTONE, a.FK_CAL, ");
			sbSQL.append("(select c.name from event_Assoc c where c.chain_id = ev.event_id) cal_desc,  a.FK_CODELST_RULE, ");
			sbSQL.append(" b.codelst_desc  rule_desc, a.MILESTONE_AMOUNT AS MILESTONE_AMOUNT, a.MILESTONE_HOLDBACK AS MILESTONE_HOLDBACK, a.FK_EVENTASSOC,");
			sbSQL.append(" a.MILESTONE_COUNT, usr_lst(a.milestone_usersto) as usersto, a.milestone_usersto usertoIds, ");
			sbSQL.append(" a.fk_visit fk_visit ,nvl((select v.visit_name from sch_protocol_visit  v where a.fk_visit = v.pk_protocol_visit ),'All') visit_name , ");
			sbSQL.append(" (select codelst_desc from er_codelst where pk_codelst = milestone_status ) patientStatus , milestone_limit ,");
			sbSQL.append(" (select codelst_desc from sch_codelst where pk_codelst = milestone_eventstatus ) eventStatus ,milestone_achievedcount, ");
			sbSQL.append(" ev.name event_desc , (select d.event_calassocto from event_Assoc d where d.event_id = ev.chain_id) event_calassocto ,"
					+ totalAmountInvColumn+", ");
			sbSQL.append(" (Select trim(CODELST_DESC) from SCH_CODELST where PK_CODELST=ev.FK_CODELST_COVERTYPE) CoverType ");

			sbSQL.append(" from ER_MILESTONE a, er_codelst b, event_Assoc ev " );

			if (! StringUtil.isEmpty(payType)){
				sbSQL.append(" , er_codelst p ");
			}

			sbSQL.append(" where a.FK_STUDY = ? and a.MILESTONE_TYPE = 'EM' "); 
			sbSQL.append("and (a.FK_EVENTASSOC = ev.event_id) ");
			sbSQL.append("and a.FK_CODELST_RULE = b.pk_codelst and NVL(a.MILEstone_DELFLAG,'Z') <> 'Y' ");

			if (! StringUtil.isEmpty(payType)){
          		sbSQL.append(" and p.pk_codelst = a.milestone_paytype and trim(p.codelst_subtyp) = ? ");
          	}

			if (milestoneReceivableStatus.equals("UI")){
				sbSQL.append(" and (select count(*) from er_mileachieved ac where ac.fk_milestone = pk_milestone and not exists (select * from er_invoice_detail det where det.fk_milestone = ac.fk_milestone and det.fk_mileachieved = ac.pk_mileachieved) ) > 0 ");
			}

			Rlog.fatal("getMilestoneRowsEM SQL:", sbSQL.toString());

			pstmt = conn.prepareStatement(sbSQL.toString());
			pstmt.setInt(1, studyId);

			if (! StringUtil.isEmpty(payType)){
				pstmt.setString(2, payType);
          	}

			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				setMilestoneIds(new Integer(rs.getInt("PK_MILESTONE")));

				setTotalInvoicedAmounts(rs.getString("amount_invoiced") );

				Rlog.debug("milestone", "MilestoneDao.getMilestoneRowsEM rows a");
				patientCount =rs.getString("MILESTONE_COUNT");
				
				setMilestoneCounts(patientCount);
				setMilestoneUsers(rs.getString("USERSTO"));
				setMilestoneUserIds(rs.getString("USERTOIDS"));
				
				setMilestoneAmounts(rs.getString("MILESTONE_AMOUNT"));
				setHoldBack(rs.getFloat("MILESTONE_HOLDBACK"));
				if (StringUtil.isEmpty(patientCount)){
					patientCount = "";
				}

				calassocto = rs.getString("event_calassocto");

	           	if (StringUtil.isEmpty(calassocto )){
	           		calassocto = "P";
	           	}

	           	if (!"S".equals(calassocto)){
					if (StringUtil.stringToNum(patientCount) <= 0 ){
						patientCountString = LC.L_Every + " " + patientStatusDesc + " "+LC.Pat_Patient_Lower;
	                }else{
	                	patientCountString = patientCount + " " + patientStatusDesc + " "+LC.Pat_Patients_Lower;
	                }
	           	}

                if (calassocto.trim().equals("S")){
               	 patientCountString = "";
                }
                
				setPatientStatuses(rs.getString("patientStatus"));
	            patientStatusDesc = rs.getString("patientStatus");
	            
				if (StringUtil.isEmpty(patientStatusDesc)){
					patientStatusDesc = "";
				}else{
					patientStatusDesc = "'" + patientStatusDesc  + "'";
				}

	           	setMilestoneCalIds(rs.getString("FK_CAL"));
	           	setMilestoneCalDescs(rs.getString("CAL_DESC"));
	           	setMilestoneRuleIds(String.valueOf(rs.getString("FK_CODELST_RULE")));
	           	calName =  rs.getString("CAL_DESC");
	           	setFkVisits(rs.getString("FK_VISIT"));
	           	setProtocolVisitNames(rs.getString("VISIT_NAME"));
	           	ruleDesc = rs.getString("RULE_DESC");
	           	visitName = rs.getString("VISIT_NAME");
	           	eventStatus = rs.getString("eventStatus");
	           	if (StringUtil.isEmpty(eventStatus))
	           		eventStatus = "";

               	setMilestoneEvents(String.valueOf(rs.getString("FK_EVENTASSOC")));
               	setMilestoneEventDescs(rs.getString("EVENT_DESC"));
               	eventName = rs.getString("EVENT_DESC");
               	CoverType = rs.getString("CoverType");               	

                if (! StringUtil.isEmpty(eventName)){
                	eventName = LC.L_Event+" : " + eventName + ", ";
                }
           		milestoneRuleDesc = eventName + ruleDesc +  eventStatus;
                if (!"S".equals(calassocto)){
                	milestoneRuleDesc+= " [" + patientCountString + "] ";                	
                }

                setMilestoneRuleDescs(milestoneRuleDesc) ;
               
                rows++;
                Rlog.debug("milestone", "MilestoneDao.getMilestoneRowsEM rows "+ rows);
			}

			setCRows(rows);
		} catch (SQLException ex) {
			Rlog.fatal("milestone","MilestoneDao.getMilestoneRowsEM EXCEPTION IN FETCHING FROM Milestone table"
                           + ex);
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}
    }
    
    public void getMilestoneRowsEM(int studyId, String payType,String milestoneReceivableStatus , String CovType, int forInvoicing) {
		String lsql = null;
		int rows = 0;
		PreparedStatement pstmt = null;
		Connection conn = null;
		
		StringBuffer sbSQL = new StringBuffer();
		String milestoneRuleDesc = "";
		String ruleDesc = "";
		String patientStatusDesc = "";
		String visitName = "";
		String eventName = "";
		String eventStatus = "";
		String patientCountString = "";
		String patientCount = "";
		String calName = "";
		String calassocto = "";
		String CoverType = "";
		String totalAmountInvColumn = " (select sum(nvl(amount_invoiced,0) + nvl(amount_holdback,0)) from er_invoice_detail det, er_invoice inv where det.fk_milestone = pk_milestone AND inv.pk_invoice = det.fk_inv and nvl(detail_type,'H')='H') amount_invoiced ";

		try {
			conn = getConnection();

			sbSQL.append("select PK_MILESTONE, a.FK_CAL, ");
			sbSQL.append("(select c.name from event_Assoc c where c.chain_id = ev.event_id) cal_desc,  a.FK_CODELST_RULE, ");
			sbSQL.append(" b.codelst_desc  rule_desc, a.MILESTONE_AMOUNT AS MILESTONE_AMOUNT, a.MILESTONE_HOLDBACK AS MILESTONE_HOLDBACK, a.FK_EVENTASSOC,");
			sbSQL.append(" a.MILESTONE_COUNT, usr_lst(a.milestone_usersto) as usersto, a.milestone_usersto usertoIds, ");
			sbSQL.append(" a.fk_visit fk_visit ,nvl((select v.visit_name from sch_protocol_visit  v where a.fk_visit = v.pk_protocol_visit ),'All') visit_name , ");
			sbSQL.append(" (select codelst_desc from er_codelst where pk_codelst = milestone_status ) patientStatus , milestone_limit ,");
			sbSQL.append(" (select codelst_desc from sch_codelst where pk_codelst = milestone_eventstatus ) eventStatus ,milestone_achievedcount, ");
			sbSQL.append(" ev.name event_desc , (select d.event_calassocto from event_Assoc d where d.event_id = ev.chain_id) event_calassocto ,"
					+ totalAmountInvColumn+", ");
			sbSQL.append(" (Select trim(CODELST_DESC) from SCH_CODELST where PK_CODELST=ev.FK_CODELST_COVERTYPE) CoverType ");

			sbSQL.append(" from ER_MILESTONE a, er_codelst b, event_Assoc ev " );

			if (! StringUtil.isEmpty(payType)){
				sbSQL.append(" , er_codelst p ");
			}

			sbSQL.append(" where a.FK_STUDY = ? and a.MILESTONE_TYPE = 'EM' "); 
			sbSQL.append("and (a.FK_EVENTASSOC = ev.event_id) ");
			sbSQL.append("and a.FK_CODELST_RULE = b.pk_codelst and NVL(a.MILEstone_DELFLAG,'Z') <> 'Y' ");

			if (! StringUtil.isEmpty(payType)){
				if("recinv".equals(payType)){
					payType="pay";
					sbSQL.append(" and p.pk_codelst = a.milestone_paytype and trim(p.codelst_subtyp) != ? ");
				}else
					sbSQL.append(" and p.pk_codelst = a.milestone_paytype and trim(p.codelst_subtyp) = ? ");
          	}

			if (milestoneReceivableStatus.equals("UI")){
				sbSQL.append(" and (select count(*) from er_mileachieved ac where ac.fk_milestone = pk_milestone and not exists (select * from er_invoice_detail det where det.fk_milestone = ac.fk_milestone and det.fk_mileachieved = ac.pk_mileachieved) ) > 0 ");
			}
			if (forInvoicing == 1)
				sbSQL.append(" and a.FK_CODELST_MILESTONE_STAT = (SELECT pk_codelst FROM er_codelst WHERE CODELST_TYPE = 'milestone_stat' AND CODELST_SUBTYP = 'A') ");

			Rlog.fatal("getMilestoneRowsEM SQL:", sbSQL.toString());

			pstmt = conn.prepareStatement(sbSQL.toString());
			pstmt.setInt(1, studyId);

			if (! StringUtil.isEmpty(payType)){
				pstmt.setString(2, payType);
          	}

			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				setMilestoneIds(new Integer(rs.getInt("PK_MILESTONE")));

				setTotalInvoicedAmounts(rs.getString("amount_invoiced") );

				Rlog.debug("milestone", "MilestoneDao.getMilestoneRowsEM rows a");
				patientCount =rs.getString("MILESTONE_COUNT");
				
				setMilestoneCounts(patientCount);
				setMilestoneUsers(rs.getString("USERSTO"));
				setMilestoneUserIds(rs.getString("USERTOIDS"));
				
				setMilestoneAmounts(rs.getString("MILESTONE_AMOUNT"));
				setHoldBack(rs.getFloat("MILESTONE_HOLDBACK"));
				if (StringUtil.isEmpty(patientCount)){
					patientCount = "";
				}

				calassocto = rs.getString("event_calassocto");

	           	if (StringUtil.isEmpty(calassocto )){
	           		calassocto = "P";
	           	}

	           	if (!"S".equals(calassocto)){
					if (StringUtil.stringToNum(patientCount) <= 0 ){
						patientCountString = LC.L_Every + " " + patientStatusDesc + " "+LC.Pat_Patient_Lower;
	                }else{
	                	patientCountString = patientCount + " " + patientStatusDesc + " "+LC.Pat_Patients_Lower;
	                }
	           	}

                if (calassocto.trim().equals("S")){
               	 patientCountString = "";
                }
                
				setPatientStatuses(rs.getString("patientStatus"));
	            patientStatusDesc = rs.getString("patientStatus");
	            
				if (StringUtil.isEmpty(patientStatusDesc)){
					patientStatusDesc = "";
				}else{
					patientStatusDesc = "'" + patientStatusDesc  + "'";
				}

	           	setMilestoneCalIds(rs.getString("FK_CAL"));
	           	setMilestoneCalDescs(rs.getString("CAL_DESC"));
	           	setMilestoneRuleIds(String.valueOf(rs.getString("FK_CODELST_RULE")));
	           	calName =  rs.getString("CAL_DESC");
	           	setFkVisits(rs.getString("FK_VISIT"));
	           	setProtocolVisitNames(rs.getString("VISIT_NAME"));
	           	ruleDesc = rs.getString("RULE_DESC");
	           	visitName = rs.getString("VISIT_NAME");
	           	eventStatus = rs.getString("eventStatus");
	           	if (StringUtil.isEmpty(eventStatus))
	           		eventStatus = "";

               	setMilestoneEvents(String.valueOf(rs.getString("FK_EVENTASSOC")));
               	setMilestoneEventDescs(rs.getString("EVENT_DESC"));
               	eventName = rs.getString("EVENT_DESC");
               	CoverType = rs.getString("CoverType");               	

                if (! StringUtil.isEmpty(eventName)){
                	eventName = LC.L_Event+" : " + eventName + ", ";
                }
           		milestoneRuleDesc = eventName + ruleDesc +  eventStatus;
                if (!"S".equals(calassocto)){
                	milestoneRuleDesc+= " [" + patientCountString + "] ";                	
                }

                setMilestoneRuleDescs(milestoneRuleDesc) ;
               
                rows++;
                Rlog.debug("milestone", "MilestoneDao.getMilestoneRowsEM rows "+ rows);
			}

			setCRows(rows);
		} catch (SQLException ex) {
			Rlog.fatal("milestone","MilestoneDao.getMilestoneRowsEM EXCEPTION IN FETCHING FROM Milestone table"
                           + ex);
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}
    }

    public void getMilestoneRowsAM(int studyId, String payType,String milestoneReceivableStatus , String CovType) {
		String lsql = null;
		int rows = 0;
		PreparedStatement pstmt = null;
		Connection conn = null;
		
		StringBuffer milestonesSQL = new StringBuffer();
		StringBuffer patientRelMilestonesSQL = new StringBuffer();
		StringBuffer nonpatientRelMilestonesSQL = new StringBuffer();
		String milestoneRuleDesc = "";
		String ruleDesc = "";
		String patientStatusDesc = "";
		String visitName = "";
		String eventName = "";
		String eventStatus = "";
		String patientCount = "";
		String calName = "";
		String calassocto = "";
		String CoverType = "";
		String totalAmountInvColumn = " (select sum(nvl(amount_invoiced,0) + nvl(amount_holdback,0)) from er_invoice_detail det, er_invoice inv where det.fk_milestone = pk_milestone AND inv.pk_invoice = det.fk_inv and nvl(detail_type,'H')='H') amount_invoiced ";

		try {
			conn = getConnection();

			/*** Patient related Additional Milestones (Created by adding unscheduled events to patient schedule) ***/
			
			patientRelMilestonesSQL.append("select PK_MILESTONE, ev.name as EVENT_DESC, ");
			patientRelMilestonesSQL.append("a.MILESTONE_AMOUNT AS MILESTONE_AMOUNT, a.MILESTONE_HOLDBACK AS MILESTONE_HOLDBACK, ");
			patientRelMilestonesSQL.append("a.MILESTONE_COUNT AS MILESTONE_COUNT, ");
			patientRelMilestonesSQL.append(" usr_lst(a.milestone_usersto) as usersto, ");
			patientRelMilestonesSQL.append("a.milestone_usersto usertoIds,  milestone_achievedcount, " + totalAmountInvColumn );
			patientRelMilestonesSQL.append(", (Select trim(CODELST_DESC) from SCH_CODELST where PK_CODELST=s.FK_CODELST_COVERTYPE) as CoverType ");
			patientRelMilestonesSQL.append(", milestone_description ");
     		patientRelMilestonesSQL.append("from ER_MILESTONE a, event_Assoc ev, sch_events1 s ");

     		if (! StringUtil.isEmpty(payType)){
          		patientRelMilestonesSQL.append(", er_codelst p ");
          	}

         	patientRelMilestonesSQL.append("where a.FK_STUDY = ? ");
         	patientRelMilestonesSQL.append("and a.MILESTONE_TYPE  = 'AM' ");
         	patientRelMilestonesSQL.append("and (FK_EVENTASSOC = ev.event_id and ev.event_id = s.fk_assoc) ");
       		patientRelMilestonesSQL.append("and NVL(a.MILESTONE_DELFLAG,'Z') <> 'Y' ");
       		
       		if(null!=CovType && !CovType.equalsIgnoreCase("All"))
       			patientRelMilestonesSQL.append(" and s.FK_CODELST_COVERTYPE IN ("+CovType+") "); 	

			if (! StringUtil.isEmpty(payType)){
				if("recinv".equals(payType)){
					payType="pay";
					patientRelMilestonesSQL.append(" and p.pk_codelst = a.milestone_paytype and trim(p.codelst_subtyp) != ? ");
				}else
					patientRelMilestonesSQL.append(" and p.pk_codelst = a.milestone_paytype and trim(p.codelst_subtyp) = ? ");
          	}

			if (milestoneReceivableStatus.equals("UI")){
				patientRelMilestonesSQL.append(" and not exists (select * from er_invoice_detail det where det.fk_milestone = pk_milestone)  ");
			}
			patientRelMilestonesSQL.append(" and a.FK_CODELST_MILESTONE_STAT = (SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'milestone_stat' AND CODELST_SUBTYP = 'A') ");

			Rlog.fatal("patientRelMilestonesSQL:", patientRelMilestonesSQL.toString());

			/*** End of Patient related Additional Milestones ***/
            
            /*** Non Patient related Additional Milestones ***/
            nonpatientRelMilestonesSQL.append("select PK_MILESTONE, '' as EVENT_DESC, ");
            nonpatientRelMilestonesSQL.append("a.MILESTONE_AMOUNT AS MILESTONE_AMOUNT, a.MILESTONE_HOLDBACK AS MILESTONE_HOLDBACK,");
            nonpatientRelMilestonesSQL.append("a.MILESTONE_COUNT AS MILESTONE_COUNT, ");
            nonpatientRelMilestonesSQL.append(" usr_lst(a.milestone_usersto) as usersto, ");
            nonpatientRelMilestonesSQL.append("a.milestone_usersto usertoIds,  milestone_achievedcount, " + totalAmountInvColumn );
            nonpatientRelMilestonesSQL.append(",'-' as CoverType ");
            nonpatientRelMilestonesSQL.append(", milestone_description ");
            nonpatientRelMilestonesSQL.append(" from ER_MILESTONE a ");
     		                        
            if (! StringUtil.isEmpty(payType)){
            	nonpatientRelMilestonesSQL.append(" , er_codelst p ");
          	}
            
            nonpatientRelMilestonesSQL.append("where a.FK_STUDY = ? ");
            nonpatientRelMilestonesSQL.append("and a.MILESTONE_TYPE  = 'AM' ");
            nonpatientRelMilestonesSQL.append("and FK_EVENTASSOC IS NULL ");
            nonpatientRelMilestonesSQL.append("and NVL(a.MILESTONE_DELFLAG,'Z') <> 'Y' ");

			if (! StringUtil.isEmpty(payType)){
				if("pay".equals(payType)){
					nonpatientRelMilestonesSQL.append(" and p.pk_codelst = a.milestone_paytype and trim(p.codelst_subtyp) != ? ");
				}else
					nonpatientRelMilestonesSQL.append(" and p.pk_codelst = a.milestone_paytype and trim(p.codelst_subtyp) = ? ");
          	}

			if (milestoneReceivableStatus.equals("UI")){
				nonpatientRelMilestonesSQL.append(" and not exists (select * from er_invoice_detail det where det.fk_milestone = pk_milestone)  ");
			}
			nonpatientRelMilestonesSQL.append(" and a.FK_CODELST_MILESTONE_STAT = (SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'milestone_stat' AND CODELST_SUBTYP = 'A') ");

			Rlog.fatal("nonpatientRelMilestonesSQL:", nonpatientRelMilestonesSQL.toString());
			
            /*** End of Non Patient related Additional Milestones ***/
            
            milestonesSQL = patientRelMilestonesSQL
            	.append(" UNION ")
            	.append(nonpatientRelMilestonesSQL);
            
            Rlog.fatal("milestone", "MileAchievedDao.getAchievedMilestonesAM SQL" + milestonesSQL.toString());
            
			pstmt = conn.prepareStatement(patientRelMilestonesSQL.toString());
			int paramSeq = 1;
			pstmt.setInt(paramSeq, studyId); paramSeq++;

			if (! StringUtil.isEmpty(payType)){
				pstmt.setString(paramSeq, payType); paramSeq++;
          	}

			pstmt.setInt(paramSeq, studyId); paramSeq++;

			if (! StringUtil.isEmpty(payType)){
				pstmt.setString(paramSeq, payType); paramSeq++;
          	}

			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				setMilestoneIds(new Integer(rs.getInt("PK_MILESTONE")));

				setTotalInvoicedAmounts(rs.getString("amount_invoiced") );

				patientCount =rs.getString("MILESTONE_COUNT");
				setMilestoneCounts(patientCount);
				setMilestoneUsers(rs.getString("USERSTO"));
				setMilestoneUserIds(rs.getString("USERTOIDS"));
				setMilestoneAmounts(rs.getString("MILESTONE_AMOUNT"));
				setHoldBack(rs.getFloat("MILESTONE_HOLDBACK"));

				if (StringUtil.isEmpty(patientCount)){
					patientCount = "";
				}

				//setMilestoneDescriptions(rs.getString("MILESTONE_DESCRIPTION"));	
				milestoneRuleDesc = rs.getString("MILESTONE_DESCRIPTION");
				eventName = rs.getString("EVENT_DESC");
                if (! StringUtil.isEmpty(eventName)){
                	milestoneRuleDesc = LC.L_Unscheduled_Event + ": " + eventName +"";
                }
                setMilestoneRuleDescs(milestoneRuleDesc) ;
               
                rows++;
                Rlog.debug("milestone", "MilestoneDao.getMilestoneRowsAM rows "+ rows);
			}

			setCRows(rows);
		} catch (SQLException ex) {
			Rlog.fatal("milestone","MilestoneDao.getMilestoneRowsAM EXCEPTION IN FETCHING FROM Milestone table"
                           + ex);
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}
    }
   // end of class
   
    /* FIN-22373 09-Oct-2012 -Sudhir*/
	/**
	 * @param milestonedateFrom the milestonedateFrom to set
	 */
	public void setMilestoneDateFrom(String milestoneDateFrom) {
		this.milestoneDateFrom.add(milestoneDateFrom);
	}

	/**
	 * @return the milestonedateFrom
	 */
	public ArrayList getMilestoneDateFrom() {
		return milestoneDateFrom;
	}

	/**
	 * @param milestonedateTo the milestonedateTo to set
	 */
	public void setMilestoneDateTo(String milestoneDateTo) {
		this.milestoneDateTo.add(milestoneDateTo);		
	}

	/**
	 * @return the milestonedateTo
	 */
	public ArrayList getMilestoneDateTo() {
		return milestoneDateTo;
	}
	
	/* FIN-22373 09-Oct-2012 -Sudhir*/
	public void getMilestoneRows(int studyId, String mileStoneType, String payType,String milestoneReceivableStatus, String selStatus, String selMilePayType, String srchStrCalName, String srchStrVisitName, String srchStrEvtName, String inActiveFlag,  String orderBy, String orderType , String datefrom, String dateto) {

		String lsql = null;
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        StringBuffer sbSQL = new StringBuffer();
        String milestoneRuleDesc = "";
        String ruleDesc = "";
        String patientStatusDesc = "";
        String visitName = "";
        String eventName = "";
        String eventStatus = "";
        String patientCountString = "";
        String patientCount = "";
        String calName = "";
        String calassocto = "";



        try {
            conn = getConnection();

            if (StringUtil.isEmpty(datefrom)){

            	datefrom = "";
            }
            
            if (StringUtil.isEmpty(dateto)){

            	dateto = "";
            }
            
            if (StringUtil.isEmpty(selStatus)){

            	selStatus = "";
            }

            if (StringUtil.isEmpty(selMilePayType)){

            	selMilePayType = "";

			}


            if (StringUtil.isEmpty(srchStrCalName)){

            	srchStrCalName = "";

			}


            if (StringUtil.isEmpty(srchStrVisitName)){

            	srchStrVisitName = "";

			}


            if (StringUtil.isEmpty(srchStrEvtName)){

            	srchStrEvtName = "";

			}


            if (StringUtil.isEmpty(inActiveFlag) || inActiveFlag.equals("null")){

            	inActiveFlag = "0";

			}



            if (mileStoneType.equals("PM") || mileStoneType.equals("SM") || mileStoneType.equals("AM") )
            {
            	sbSQL.append("select fk_codelst_milestone_stat, a.PK_MILESTONE,  ");//KM
                sbSQL.append("a.MILESTONE_AMOUNT AS MILESTONE_AMOUNT, ");
                //JM:19Mar08: #FIN13
                sbSQL.append("(select codelst_desc from er_codelst where pk_codelst = MILESTONE_PAYTYPE) payment_type, ");

               	sbSQL.append("a.MILESTONE_COUNT, ");
               	sbSQL.append(" usr_lst(a.milestone_usersto) as usersto, ");
               	sbSQL.append("a.milestone_usersto usertoIds,  milestone_achievedcount, ");

               	//Modified by Manimaran for Enh#FIN10
               	if (mileStoneType.equals("AM")){
               		sbSQL.append(" milestone_description");
               		sbSQL.append(" from ER_MILESTONE a ");
               	}
               	else
               	{
               		sbSQL.append(" b.codelst_desc patientStatus ,milestone_limit , a.MILESTONE_DATE_FROM , a.MILESTONE_DATE_TO ");
                	sbSQL.append(" from ER_MILESTONE a , er_codelst b ");
               	}


               	if (! StringUtil.isEmpty(payType))
            	{
            		sbSQL.append(" , er_codelst p ");

            	}

               	sbSQL.append( "where a.FK_STUDY = ? ");
               	sbSQL.append("and a.MILESTONE_TYPE  = '"+mileStoneType+"' ");


               	if (mileStoneType.equals("AM")){
               		sbSQL.append( "and NVL(a.MILEstone_DELFLAG,'Z') <> 'Y' ");
               	}else
               	{
               	   	sbSQL.append( "and NVL(a.MILEstone_DELFLAG,'Z') <> 'Y' and b.pk_codelst = a.milestone_status ");
                	if (!StringUtil.isEmpty(datefrom)){
              		 sbSQL.append( " AND trunc(a.MILESTONE_DATE_FROM) >= trunc(To_Date('"+datefrom+"','MM/DD/YYYY')) ");
                    }
                   
                   if (!StringUtil.isEmpty(dateto)){
                  	 sbSQL.append( " AND trunc(a.MILESTONE_DATE_TO) <= trunc(To_Date('"+dateto+"','MM/DD/YYYY')) ");
                   }
               	}


            }

            if (mileStoneType.equals("VM"))
            {
            	sbSQL.append(" SELECT * FROM (select fk_codelst_milestone_stat, a.PK_MILESTONE,  a.FK_CAL, c.name  cal_desc,  a.FK_CODELST_RULE, ");
            	sbSQL.append(" b.codelst_desc  rule_desc, b.CODELST_SUBTYP  sub_type, a.MILESTONE_AMOUNT AS MILESTONE_AMOUNT, ");
            	//JM:19Mar08: #FIN13
                sbSQL.append("(select codelst_desc from er_codelst where pk_codelst = MILESTONE_PAYTYPE) payment_type, ");

            	sbSQL.append(" a.MILESTONE_COUNT, usr_lst(a.milestone_usersto) as usersto, a.milestone_usersto usertoIds, ");
            	sbSQL.append(" a.fk_visit fk_visit ,nvl((select v.visit_name from sch_protocol_visit  v where a.fk_visit = v.pk_protocol_visit ),'All') visit_name , ");
            	sbSQL.append(" (select codelst_desc from er_codelst where pk_codelst = milestone_status ) patientStatus , milestone_limit, ");
            	sbSQL.append(" (select codelst_desc from sch_codelst where pk_codelst = milestone_eventstatus ) eventStatus ,milestone_achievedcount, ");
            	sbSQL.append(" a.MILESTONE_DATE_FROM , a.MILESTONE_DATE_TO ");
            	sbSQL.append(" ,c.event_calassocto from ER_MILESTONE a, er_codelst b , event_Assoc  c " );


            	if (! StringUtil.isEmpty(payType))
            	{
            		sbSQL.append(" , er_codelst p ");

            	}


            	sbSQL.append(" where a.FK_STUDY = ? and a.MILESTONE_TYPE = 'VM' ");
            	sbSQL.append(" and a.FK_CODELST_RULE = b.pk_codelst  and a.fk_cal  = c.event_id and NVL(a.MILEstone_DELFLAG,'Z') <> 'Y' ");
            	if (!StringUtil.isEmpty(datefrom)){
           		 sbSQL.append( " AND trunc(a.MILESTONE_DATE_FROM) >= trunc(To_Date('"+datefrom+"','MM/DD/YYYY')) ");
                }
                
                if (!StringUtil.isEmpty(dateto)){
               	 sbSQL.append( " AND trunc(a.MILESTONE_DATE_TO) <= trunc(To_Date('"+dateto+"','MM/DD/YYYY')) ");
                }
                sbSQL.append(" ) ");
            }


            if (mileStoneType.equals("EM"))
            {
            	sbSQL.append(" SELECT * FROM (select  fk_codelst_milestone_stat, a.PK_MILESTONE,  a.FK_CAL, c.name  cal_desc,  a.FK_CODELST_RULE, ");
           		/* Bug#11399 29-Aug-2012 -Sudhir*/
            	sbSQL.append(" b.codelst_desc  rule_desc, b.CODELST_SUBTYP  sub_type, a.MILESTONE_AMOUNT AS MILESTONE_AMOUNT, ");

            	//JM:19Mar08: #FIN13
                sbSQL.append("(select codelst_desc from er_codelst where pk_codelst = a.MILESTONE_PAYTYPE) payment_type, ");

            	sbSQL.append(" a.FK_EVENTASSOC, a.MILESTONE_COUNT, usr_lst(a.milestone_usersto) as usersto, a.milestone_usersto usertoIds, ");
            	sbSQL.append(" a.fk_visit fk_visit ,nvl((select v.visit_name from sch_protocol_visit  v where a.fk_visit = v.pk_protocol_visit ),'All') visit_name , ");
            	sbSQL.append(" (select codelst_desc from er_codelst where pk_codelst = milestone_status ) patientStatus , milestone_limit ,");
            	sbSQL.append(" (select codelst_desc from sch_codelst where pk_codelst = milestone_eventstatus ) eventStatus ,milestone_achievedcount, ");
            	sbSQL.append(" d.name event_desc,c.event_calassocto, ");
            	sbSQL.append(" a.MILESTONE_DATE_FROM , a.MILESTONE_DATE_TO ");
            	sbSQL.append(" from ER_MILESTONE a, er_codelst b , event_Assoc  c , event_Assoc  d" );

            	if (! StringUtil.isEmpty(payType))
            	{
            		sbSQL.append(" , er_codelst p ");

            	}

            	sbSQL.append(" where a.FK_STUDY = ? and a.MILESTONE_TYPE = 'EM' and a.FK_EVENTASSOC  = d.event_id ");
            	sbSQL.append(" and a.FK_CODELST_RULE = b.pk_codelst  and a.fk_cal  = c.event_id and NVL(a.MILEstone_DELFLAG,'Z') <> 'Y' ");
            	if (!StringUtil.isEmpty(datefrom)){
           		 sbSQL.append( " AND trunc(a.MILESTONE_DATE_FROM) >= trunc(To_Date('"+datefrom+"','MM/DD/YYYY')) ");
                }
                
                if (!StringUtil.isEmpty(dateto)){
               	 sbSQL.append( " AND trunc(a.MILESTONE_DATE_TO) <= trunc(To_Date('"+dateto+"','MM/DD/YYYY')) ");
                }
                sbSQL.append(" ) ");
            }


            if (! StringUtil.isEmpty(payType))
           	{
           		sbSQL.append(" and p.pk_codelst = a.milestone_paytype and trim(p.codelst_subtyp) = ? ");

           	}

            if (milestoneReceivableStatus.equals("UI"))
            {

            	sbSQL.append(" and not exists (select * from er_invoice_detail det where det.fk_milestone = pk_milestone)  ");
            }

            Rlog.debug("milestone", sbSQL.toString());





            //JM: 20Mar2008: added
            if (inActiveFlag.equals("1")){//Exclude Inactive Items


            	if (mileStoneType.equals("EM") || mileStoneType.equals("VM"))
                {
            		sbSQL.append(" WHERE fk_codelst_milestone_stat <> (select pk_codelst from er_codelst where codelst_type ='milestone_stat' and codelst_subtyp='IA')");


                }else{
                	sbSQL.append(" and fk_codelst_milestone_stat <> (select pk_codelst from er_codelst where codelst_type ='milestone_stat' and codelst_subtyp='IA')" );

                }
            }


            if (inActiveFlag.equals("0")){


            	if (mileStoneType.equals("EM") || mileStoneType.equals("VM")){

            		sbSQL.append(" WHERE fk_codelst_milestone_stat is not null ");

                }else{
                	sbSQL.append(" and fk_codelst_milestone_stat is not null " );

                }
            }



            if (!selStatus.equals("")){

            	sbSQL.append(" and fk_codelst_milestone_stat = "+ selStatus );

            }

            if (!selMilePayType.equals("")){

            	if (mileStoneType.equals("EM") || mileStoneType.equals("VM")){

            		sbSQL.append(" and payment_type = (SELECT codelst_desc FROM er_codelst WHERE pk_codelst = "+ selMilePayType + ")");

                }else{
                	sbSQL.append(" and a.MILESTONE_PAYTYPE = "+ selMilePayType );

                }
            }


            if (!srchStrCalName.equals("")){
	        	sbSQL.append(" and lower(cal_desc) like (lower('%"+ srchStrCalName+"%'))");
            }


            if (!srchStrVisitName.equals("")){
            	sbSQL.append(" and lower(visit_name) like (lower('%"+ srchStrVisitName +"%'))");

            }

            if (!srchStrEvtName.equals("")){
	        	sbSQL.append(" and  lower(event_desc) like(lower('%"+ srchStrEvtName + "%'))");
            }

            /* YK 2MAY2011 :FIXED Bug #5689 */
            if ( ( orderBy.equals("event_desc") || orderBy.equals("visit_name")||orderBy.equals("cal_desc")) && (mileStoneType.equals("PM") || mileStoneType.equals("SM")) )
            {
            	orderBy = "patientStatus";
            }


            if (mileStoneType.equals("AM") && (orderBy.equals("patientStatus") || orderBy.equals("visit_name") ||	orderBy.equals("cal_desc") || orderBy.equals("event_desc")))
            {
            	orderBy = "MILESTONE_DESCRIPTION";
            }
            /* YK 13DEC2010 :FIXED Bug #3379 */
            /*Sorting VM and EM on the basis of eventStatus*/
            /* YK 20MAY2011 :FIXED Bug #3379 */
            if((mileStoneType.equals("VM") && (orderBy.equals("event_desc") || orderBy.equals("patientStatus"))) || ( mileStoneType.equals("EM") && orderBy.equals("patientStatus") )){
            	orderBy = "eventStatus";
            }
            /* YK 13DEC2010 :FIXED Bug #3379 */
            
           /* if (mileStoneType.equals("PM") || mileStoneType.equals("SM")){
            	
            	 if (!StringUtil.isEmpty(datefrom)){
            		 sbSQL.append( " AND trunc(a.MILESTONE_DATE_FROM) >= trunc(To_Date('"+datefrom+"','MM/DD/YYYY')) ");
                 }
                 
                 if (!StringUtil.isEmpty(dateto)){
                	 sbSQL.append( " AND trunc(a.MILESTONE_DATE_TO) <= trunc(To_Date('"+dateto+"','MM/DD/YYYY')) ");
                 }           	
            }*/
            
            //Virendra:Fixed Bug No.#3379. Added lower()
            sbSQL.append( " order by lower(" +  orderBy + ") " + orderType);
  
            pstmt = conn.prepareStatement(sbSQL.toString());
            pstmt.setInt(1, studyId);

            if (! StringUtil.isEmpty(payType))
           	{

            	pstmt.setString(2, payType);

           	}

            String abc = sbSQL.toString();

            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {

            	eventStatus = "";
                setMilestoneIds(new Integer(rs.getInt("PK_MILESTONE")));
                //KM- Apr27
                setMilestoneStatFK(new Integer(rs.getInt("fk_codelst_milestone_stat")));
                //JM: 19Mar08:

                setMilestonePaymentTypes(rs.getString("payment_type") );

                setMilestoneAchievedCounts(rs.getString("milestone_achievedcount"));


                Rlog.debug("milestone", "MilestoneDao.getMilestoneRows rows a");
                patientCount =rs.getString("MILESTONE_COUNT");
                Rlog.debug("milestone", "MilestoneDao.getMilestoneRows rows b");
                setMilestoneCounts(patientCount);
                setMilestoneUsers(rs.getString("USERSTO"));
                Rlog.debug("milestone", "MilestoneDao.getMilestoneRows rows c");
                setMilestoneUserIds(rs.getString("USERTOIDS"));

                if (!mileStoneType.equals("AM")) {
                   Rlog.debug("milestone", "MilestoneDao.getMilestoneRows rows d");
                   setPatientStatuses(rs.getString("patientStatus"));
                   Rlog.debug("milestone", "MilestoneDao.getMilestoneRows rows f");
                   patientStatusDesc = rs.getString("patientStatus");
                   
                   
                   Rlog.debug("milestone", "MilestoneDao.getMilestoneRows rows f");
                   setMilestoneDateFrom(DateUtil.dateToString(rs.getDate("MILESTONE_DATE_FROM")));
                   
                   Rlog.debug("milestone", "MilestoneDao.getMilestoneRows rows f");
                   setMilestoneDateTo(DateUtil.dateToString(rs.getDate("MILESTONE_DATE_TO")));

                }
                Rlog.debug("milestone", "MilestoneDao.getMilestoneRows rows e");
                setMilestoneAmounts(rs.getString("MILESTONE_AMOUNT"));
                Rlog.debug("milestone", "MilestoneDao.getMilestoneRows rows g");

                if (StringUtil.isEmpty(patientCount))
                {
                	patientCount = "";

                }


                if (StringUtil.isEmpty(patientStatusDesc))
                {

                patientStatusDesc = "";

                }
                else
                {
                	patientStatusDesc = "'" + patientStatusDesc  + "'";

                }



                if(mileStoneType.equals("AM"))
                {
                	setMilestoneDescriptions(rs.getString("MILESTONE_DESCRIPTION"));
                }


                if (mileStoneType.equals("EM") || mileStoneType.equals("VM"))
                {
                	setMilestoneCalIds(rs.getString("FK_CAL"));
                	setMilestoneCalDescs(rs.getString("CAL_DESC"));
                	setMilestoneRuleIds(String.valueOf(rs.getString("FK_CODELST_RULE")));
                	setMileStoneSubType(rs.getString("SUB_TYPE")); /* YK 14DEC2010 :FIXED Bug #3379 */
                	calName =  rs.getString("CAL_DESC");
                	setFkVisits(rs.getString("FK_VISIT")); // SV, 11/01
                    setProtocolVisitNames(rs.getString("VISIT_NAME")); // SV, 11/01
                    ruleDesc = rs.getString("RULE_DESC");


                    setMilestoneEventStatuses(rs.getString("eventStatus"));
                    eventStatus = rs.getString("eventStatus");
                    if (StringUtil.isEmpty(eventStatus))
                    	eventStatus = "";

                    calassocto = rs.getString("event_calassocto");
                    //11-Mar-2011 #5922 @Ankit Desc - 'null'treated as a condition.
                    if(calassocto == null)
                    {
                    	calassocto = "";
                    }
                    else if (StringUtil.isEmpty(calassocto))
                    {
                    	calassocto = "P";

                    }

                }


                if (mileStoneType.equals("EM"))
                {
                	setMilestoneEvents(String.valueOf(rs.getString("FK_EVENTASSOC")));
                	setMilestoneEventDescs(rs.getString("EVENT_DESC"));
                	eventName = rs.getString("EVENT_DESC");

                }

                
                if (mileStoneType.equals("SM"))
                {
                	patientCountString = LC.L_Status+": "+ patientStatusDesc ;
                }

                else
                {
                	if (!"S".equals(calassocto)){
    					if (StringUtil.stringToNum(patientCount) <= 0 ){
    						patientCountString = LC.L_Every + " " + patientStatusDesc + " "+LC.Pat_Patient_Lower;
    	                }else{
    	                	patientCountString = patientCount + " " + patientStatusDesc + " "+LC.Pat_Patients_Lower;
    	                }
    	           	}

                    if (calassocto.trim().equals("S")){
                    	patientCountString = "";
                    }
                }




                if (mileStoneType.equals("EM") || mileStoneType.equals("VM"))
                {
                	 milestoneRuleDesc = ruleDesc +  eventStatus ;

                	 if (calassocto.equals("P"))
	                 {
                		 milestoneRuleDesc  = milestoneRuleDesc  +" [ " + patientCountString + "]" ;
	                 }

                }
                else
                {
                	 milestoneRuleDesc =  patientCountString ;
                }

                setMilestoneRuleDescs(milestoneRuleDesc);

                rows++;
                Rlog.debug("milestone", "MilestoneDao.getMilestoneRows(int studyId, String mileStoneType, String payType,String milestoneReceivableStatus, String searchString,  String orderBy, String orderType) rows "+ rows);


            }

            setCRows(rows);

        } catch (SQLException ex) {
            Rlog.fatal("milestone","MilestoneDao.getMilestoneRows(7 params) EXCEPTION IN FETCHING FROM Milestone table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

/* FIN-22373 09-Oct-2012 -Sudhir*/
}
