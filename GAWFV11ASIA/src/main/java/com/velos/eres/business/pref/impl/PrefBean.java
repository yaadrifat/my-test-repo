/*


 * Classname : PrefBean


 * 


 * Version information: 1.0


 *


 * Date: 06/02/2001


 * 


 * Copyright notice: Velos, Inc


 *


 * Author: Sonia Sahni


 */

package com.velos.eres.business.pref.impl;

/* Import Statements */

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* End of Import Statements */

/**
 * 
 * 
 * The Pref CMP entity bean.<br>
 * <br>
 * 
 * 
 * @author Sonia Sahni
 * 
 * 
 * @vesion 1.0 06/02/2001
 * 
 * 
 * @ejbHome PrefHome
 * 
 * 
 * @ejbRemote PrefRObj
 * 
 * 
 */
@Entity
@Table(name = "er_per")
public class PrefBean implements Serializable

{

    /**
     * 
     */
    private static final long serialVersionUID = 4049636797816322100L;

    /**
     * 
     * 
     * the Pref PK Id
     * 
     * 
     */

    public int prefPKId;

    /**
     * 
     * 
     * the Pref ID
     * 
     * 
     */

    public String prefPId;

    /**
     * 
     * 
     * the Pref Account
     * 
     * 
     */

    public Integer prefAccount;

    /**
     * 
     * 
     * the Pref Location
     * 
     * 
     */

    public Integer prefLocation;

    public Integer timeZoneId;

    /*
     * the record creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * the IP Address
     */
    public String ipAdd;

    /**
     * the Patient Facility Id
     */
    public String patientFacilityId;

    // GETTER SETTER METHODS

    /**
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * @return PK Id for a pref
     * 
     * 
     */
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_PER", allocationSize=1)
    @Column(name = "pk_per")
    public int getPrefPKId() {

        return this.prefPKId;

    }

    public void setPrefPKId(int prefPk) {

        this.prefPKId = prefPk;

    }

    /**
     * 
     * 
     * @param prefPId
     * 
     * 
     */
    @Column(name = "per_code")
    public String getPrefPId()

    {

        return this.prefPId;

    }

    public void setPrefPId(String pPId)

    {

        this.prefPId = pPId;

    }

    /**
     * 
     * 
     * returns pref location
     * 
     * 
     * 
     * 
     * 
     * @return location
     * 
     * 
     */
    @Column(name = "fk_site")
    public String getPrefLocation()

    {

        return StringUtil.integerToString(this.prefLocation);

    }

    /**
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * @param Location
     * 
     * 
     */

    public void setPrefLocation(String pl)

    {

        if (pl != null) {

            this.prefLocation = Integer.valueOf(pl);

        }

    }

    /**
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * @return prefPId
     * 
     * 
     */
    @Column(name = "fk_account")
    public String getPrefAccount()

    {

        return StringUtil.integerToString(this.prefAccount);

    }

    /**
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * @param prefAccount
     * 
     * 
     */

    public void setPrefAccount(String pAccount)

    {

        if (pAccount != null) {

            this.prefAccount = Integer.valueOf(pAccount);

        }

    }

    @Column(name = "fk_timezone")
    public String getTimeZoneId() {
        return StringUtil.integerToString(this.timeZoneId);
    }

    public void setTimeZoneId(String timeZoneId) {
        if (timeZoneId != null) {
            this.timeZoneId = Integer.valueOf(timeZoneId);
        }
    }

    /**
     * @return The Id of the creator of the record
     */
    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    /**
     * @return The Id of the user modifying this record
     */
    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    /**
     * @return IP Address
     */
    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
	 * Returns the value of patientFacilityId.
	 */
	 @Column(name = "PAT_FACILITYID")
	public String getPatientFacilityId()
	{
		return patientFacilityId;
	}

	/**
	 * Sets the value of patientFacilityId.
	 * @param patientFacilityId The value to assign patientFacilityId.
	 */
	public void setPatientFacilityId(String patientFacilityId)
	{
		this.patientFacilityId = patientFacilityId;
	}

	
    // END OF GETTER SETTER METHODS

   
   

    /**
     * 
     * 
     * updates the Pref Record
     * 
     * 
     * 
     * 
     * 
     * @param psk
     *            PrefStateKeeper
     * 
     * 
     * @return int 0: in case of successful updation; -2 in caseof exception
     * 
     * 
     */

    public int updatePref(PrefBean psk) {

        try {

            setPrefLocation(psk.getPrefLocation());

            setPrefPId(psk.getPrefPId());

            setPrefAccount(psk.getPrefAccount());
            setTimeZoneId(psk.getTimeZoneId());
            setCreator(psk.getCreator());
            setModifiedBy(psk.getModifiedBy());
            setIpAdd(psk.getIpAdd());
	    setPatientFacilityId(psk.getPatientFacilityId());

            Rlog.debug("pref", "PrefBean.updatePref");

            return 0;

        } catch (Exception e) {

            Rlog.fatal("pref", " error in PrefBean.updatePref");

            return -2;

        }

    }

    public PrefBean() {

    }

    public PrefBean(int prefPKId, String prefPId, String prefAccount,
            String prefLocation, String timeZoneId, String creator,
            String modifiedBy, String ipAdd,String patientFacilityId) {
        super();
        // TODO Auto-generated constructor stub
        setPrefPKId(prefPKId);
        setPrefPId(prefPId);
        setPrefAccount(prefAccount);
        setPrefLocation(prefLocation);
        setTimeZoneId(timeZoneId);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
	setPatientFacilityId(patientFacilityId);
    }

}// end of class

