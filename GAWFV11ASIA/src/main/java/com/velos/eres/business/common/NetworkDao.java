package com.velos.eres.business.common;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import org.json.*;
import com.velos.eres.service.util.EnvUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.service.util.DateUtil;

import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

public class NetworkDao extends CommonDAO implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList networkIdList;
	private ArrayList networkSiteIdList;
	private ArrayList siteIdList;
	private ArrayList siteNameList;
	private ArrayList ctepIdList;
	private ArrayList networkLevelList;
	private ArrayList networkTypeIdList;
	private ArrayList networkTypeDescList;
	private ArrayList networkMainIdList;
	private ArrayList networkStatusIdList;
	private ArrayList networkStatusDescList;
	private ArrayList networkUsersList;
	private ArrayList networkMemeberTRoleList;
	private ArrayList networkMemeberADTRoleList;
	private ArrayList networkUsrPkList;
	private ArrayList networkUsrNameList;
	private ArrayList networkUsrStatusList;
	private ArrayList networkTRoleNameList;
	private ArrayList networkADTRoleNameList;
	private ArrayList networkUsrTypeList;
	private ArrayList networkUserStatusList;
	private String siteType;
	private String mainNetwork;
	private ArrayList <String> historyIds;
	private ArrayList <String> sntwStat;
	private ArrayList networkadditionalpklist;
	private ArrayList networkadditionalpkhistory;
	private ArrayList  networkadtstatus;
	private ArrayList ntusradtrole ;
	private ArrayList networkadtpkstat;
	private ArrayList networkADTLRoleNameList;
	private ArrayList networkADTLRoleStatus ;
	private String chld_ntw_ids;

	private int cRows;
	
	
	public NetworkDao() {
		networkIdList = new ArrayList();
		networkSiteIdList = new ArrayList();
		siteIdList = new ArrayList();
		networkLevelList = new ArrayList();
		networkTypeIdList = new ArrayList();
		networkMainIdList = new ArrayList();
		networkStatusIdList = new ArrayList();
		siteNameList = new ArrayList();
		ctepIdList = new ArrayList();
		networkTypeDescList = new ArrayList();
		networkStatusDescList = new ArrayList();
		networkUsersList=new ArrayList();
		networkMemeberTRoleList=new ArrayList();
		networkMemeberADTRoleList=new ArrayList();
		networkUsrPkList=new ArrayList();
		networkUsrNameList=new ArrayList();
		networkUsrStatusList=new ArrayList();
		networkTRoleNameList=new ArrayList();
		networkADTRoleNameList= new ArrayList();
		networkUsrTypeList = new ArrayList();
		historyIds = new ArrayList<String>();
		sntwStat = new ArrayList<String>();
		networkUserStatusList = new ArrayList();
		
		networkadditionalpklist = new ArrayList();
		
		networkadditionalpkhistory = new ArrayList();

		networkadtstatus = new ArrayList();
		
		ntusradtrole  = new ArrayList();
		
		networkadtpkstat = new ArrayList();
		networkADTLRoleNameList = new ArrayList();
		networkADTLRoleStatus = new ArrayList();
	}
	
	public ArrayList getNetworkIdList() {
		return networkIdList;
	}
	public void setNetworkIdList(ArrayList networkIdList) {
		this.networkIdList = networkIdList;
	}
	public ArrayList getNetworkSiteIdList() {
		return networkSiteIdList;
	}
	public void setNetworkSiteIdList(ArrayList networkSiteIdList) {
		this.networkSiteIdList = networkSiteIdList;
	}
	public ArrayList getSiteIdList() {
		return siteIdList;
	}
	public void setSiteIdList(ArrayList siteIdList) {
		this.siteIdList = siteIdList;
	}
	public ArrayList getNetworkLevelList() {
		return networkLevelList;
	}
	public void setNetworkLevelList(ArrayList networkLevelList) {
		this.networkLevelList = networkLevelList;
	}
	public ArrayList getNetworkTypeIdList() {
		return networkTypeIdList;
	}
	public void setNetworkTypeIdList(ArrayList networkTypeIdList) {
		this.networkTypeIdList = networkTypeIdList;
	}
	public ArrayList getNetworkMainIdList() {
		return networkMainIdList;
	}
	public void setNetworkMainIdList(ArrayList networkMainIdList) {
		this.networkMainIdList = networkMainIdList;
	}
	public ArrayList getNetworkStatusIdList() {
		return networkStatusIdList;
	}
	public void setNetworkStatusIdList(ArrayList networkStatusIdList) {
		this.networkStatusIdList = networkStatusIdList;
	}
	
	public ArrayList getNetworkStatusDescList() {
		return networkStatusDescList;
	}

	public void setNetworkStatusDescList(ArrayList networkStatusDescList) {
		this.networkStatusDescList = networkStatusDescList;
	}
	public ArrayList<String> getHistoryIdsList() {
	    	return this.historyIds;
	    }

	public void setHistoryIdsList(ArrayList<String> historyIds) {
	        this.historyIds = historyIds;
	    }

	public ArrayList getSiteNameList() {
		return siteNameList;
	}

	public void setSiteNameList(ArrayList siteNameList) {
		this.siteNameList = siteNameList;
	}

	public ArrayList getCtepIdList() {
		return ctepIdList;
	}

	public void setCtepIdList(ArrayList ctepIdList) {
		this.ctepIdList = ctepIdList;
	}

	public ArrayList<String> getStdNtwrkStatsList() {
	    	return this.sntwStat;
	    }

	public void setStdNtwrkStatsList(ArrayList<String> sntwStat) {
	        this.sntwStat = sntwStat;
	    }

	public ArrayList getNetworkTypeDescList() {
		return networkTypeDescList;
	}

	public void setNetworkTypeDescList(ArrayList networkTypeDescList) {
		this.networkTypeDescList = networkTypeDescList;
	}	
	public ArrayList getNetworkUsersList() {
		return networkUsersList;
	}
	public void setNetworkUsersList(ArrayList networkUsersList) {
		this.networkUsersList = networkUsersList;
	}
	public ArrayList getNetworkMemeberTRoleList() {
		return networkMemeberTRoleList;
	}

	public void setNetworkMemeberTRoleList(ArrayList networkMemeberTRoleList) {
		this.networkMemeberTRoleList = networkMemeberTRoleList;
	}
	
	public void setNetworkMemeberADTRoleList(ArrayList networkMemeberADTRoleList) {
		this.networkMemeberADTRoleList = networkMemeberADTRoleList;
	}
	
	public ArrayList getNetworkMemeberADTRoleList() {
		return networkMemeberADTRoleList;
	}
	public ArrayList getNetworkUsrPkList() {
		return networkUsrPkList;
	}
	public void setNetworkUsrPkList(ArrayList networkUsrPkList) {
		this.networkUsrPkList = networkUsrPkList;
	}
	public ArrayList getNetworkUsrNameList() {
		return networkUsrNameList;
	}
	public ArrayList getNetworkUsrStatusList(){
		return networkUsrStatusList;
	}
	public ArrayList getNetworkUserStatusList(){
		return networkUserStatusList;
	}
	public void setNetworkUsrNameList(ArrayList networkUsrNameList) {
		this.networkUsrNameList = networkUsrNameList;
	}	
	public void setNetworkUsrStatusList (ArrayList networkUsrStatusList) {
		this.networkUsrStatusList = networkUsrStatusList;
	}
	public void setNetworkUserStatusList(ArrayList networkUserStatusList){
		this.networkUserStatusList = networkUserStatusList;
	}
	public ArrayList getNetworkTRoleNameList() {
		return networkTRoleNameList;
	}
	public void setNetworkTRoleNameList(ArrayList networkTRoleNameList) {
		this.networkTRoleNameList = networkTRoleNameList;
	}
	public ArrayList getNetworkADTRoleNameList() {
		return networkADTRoleNameList;
	}
	public void setNetworkADTRoleNameList(ArrayList networkADTRoleNameList) {
		this.networkADTRoleNameList = networkADTRoleNameList;
	}
	
	public ArrayList getNetworkUsrTypeList() {
		return networkUsrTypeList;
	}

	public void setNetworkUsrTypeList(ArrayList networkUsrTypeList) {
		this.networkUsrTypeList = networkUsrTypeList;
	}

	public int getcRows() {
		return cRows;
	}

	public void setcRows(int cRows) {
		this.cRows = cRows;
	}
	public int getcRowsaddl() {
		return cRows;
	}

	public void setcRowsaddl(int cRows) {
		this.cRows = cRows;
	}

	public void setNetworkIdList(Integer networkId) {
		networkIdList.add(networkId);
	}
	
	
	public void setNetworkSiteIdList(String networkSiteId) {
		networkSiteIdList.add(networkSiteId);
	}
	
	public void setSiteIdList(String siteId) {
		siteIdList.add(siteId);
	}
	
	public void setNetworkLevelList(String networkLevel) {
		networkLevelList.add(networkLevel);
	}
	
	public void setNetworkTypeIdList(String networkTypeId) {
		networkTypeIdList.add(networkTypeId);
	}
	
	public void setNetworkMainIdList(String networkMainId) {
		networkMainIdList.add(networkMainId);
	}
	
	public void setNetworkStatusIdList(String networkStatusId) {
		networkStatusIdList.add(networkStatusId);
	}	
	
	public void setSiteNameList(String siteName) {
		siteNameList.add(siteName);
	}
	
	public void setCtepIdList(String ctepId) {
		ctepIdList.add(ctepId);
	}

	public void setNetworkTypeDescList(String networkTypeDesc) {
		networkTypeDescList.add(networkTypeDesc);
	}
	
	public void setNetworkStatusDescList(String networkStatusDesc) {
		networkStatusDescList.add(networkStatusDesc);
	}
	public void setNetworkUsersList(Integer networkUser) {
		networkUsersList.add(networkUser);
	}
	public void setNetworkMemeberTRoleList(Integer memberTRole) {
		networkMemeberTRoleList.add(memberTRole);
	}
	public void setNetworkMemeberADTRoleList(String memberADTRole) {
		networkMemeberADTRoleList.add(memberADTRole);
	}
	public void setNetworkUsrPkList(Integer ntusrPk) {
		networkUsrPkList.add(ntusrPk);
	}
	public void setNetworkUsrNameList(String ntUsrName) {
		networkUsrNameList.add(ntUsrName);
	}
	public void setNetworkUsrStatusList(String ntUsrStatus) {
		networkUsrStatusList.add(ntUsrStatus);
	}	
	public void setNetworkTRoleNameList(String ntTroleName) {
		networkTRoleNameList.add(ntTroleName);
	}
	public void setNetworkADTRoleNameList(String ntADTroleName) {
		networkADTRoleNameList.add(ntADTroleName);
	}
	public void setNetworkUsrTypeList(String ntUsrType) {
		networkUsrTypeList.add(ntUsrType);
	}
	
	public void setNetworkUserStatusList(int ntUserStatus){
		networkUserStatusList.add(ntUserStatus);
	}
	
	public void setHistoryIdsList(String histId) {
		historyIds.add(histId);
	}
	public void setStdNtwrkStatsList(String stdNtwStat) {
		sntwStat.add(stdNtwStat);
	}
	public String getSiteType() {
		return siteType;
	}

	public void setSiteType(String siteType) {
		this.siteType = siteType;
	}

	public String getMainNetwork() {
		return mainNetwork;
	}

	public void setMainNetwork(String mainNetwork) {
		this.mainNetwork = mainNetwork;
	}
	public void setNetworkAdlPkList(ArrayList networkadditionalpklist) {
		this.networkadditionalpklist = networkadditionalpklist;
	}
	public ArrayList getNetworkAdlPkList() {
		return networkadditionalpklist;
	}
	public void setNetworkAdlPkList(Integer ntusradlPk) {
		networkadditionalpklist.add(ntusradlPk);
	}
	
	public void setNetworkAdlPkHistory(ArrayList networkadditionalpkhistory) {
		this.networkadditionalpkhistory = networkadditionalpkhistory;
	}
	public ArrayList getNetworkAdlPkHistory() {
		return networkadditionalpkhistory;
	}
	public void setNetworkAdlPkHistory(Integer ntusradlPkhistory) {
		networkadditionalpkhistory.add(ntusradlPkhistory);
	}
	
	public void setNetworkAdtstatus(ArrayList networkadtstatus) {
		this.networkadtstatus = networkadtstatus;
	}
	public ArrayList getNetworkAdtstatus() {
		return networkadtstatus;
	}
	public void setNetworkAdtstatus(Integer ntusradtstatus) {
		networkadtstatus.add(ntusradtstatus);
	}
	
	public void setNetworkAdtrole(ArrayList ntusradtrole) {
		this.ntusradtrole = ntusradtrole;
	}
	public ArrayList getNetworkAdtrole() {
		return ntusradtrole;
	}
	public void setNetworkAdtrole(Integer ntusradtlrole) {
		ntusradtrole.add(ntusradtlrole);
	}	
	
	public void setNetworkAdtpkstatus(ArrayList networkadtpkstat) {
		this.networkadtpkstat = networkadtpkstat;
	}
	public ArrayList getNetworkAdtpkstatus() {
		return networkadtpkstat;
	}
	public void setNetworkAdtpkstatus(Integer ntusradtpks) {
		networkadtpkstat.add(ntusradtpks);
	}
	
	public void setNetworkADDTLRoleNameList(String ntadtTroleName) {
		networkADTLRoleNameList.add(ntadtTroleName);
	}
	
	public void setNetworkADDTLRoleNameList(ArrayList networkADTLRoleNameList) {
		this.networkADTLRoleNameList = networkADTLRoleNameList;
	}
	
public ArrayList getNetworkADDTLRoleNameList() {
		return networkADTLRoleNameList;
	}	
	
	public void setNetworkADDTLRoleStatus(String ntadtstatus) {
		networkADTLRoleStatus.add(ntadtstatus);
	}
	
	public void setNetworkADDTLRoleStatus(ArrayList networkADTLRoleStatus) {
		this.networkADTLRoleStatus = networkADTLRoleStatus;
	}
	
public ArrayList getNetworkADDTLRoleStatus() {
		return networkADTLRoleStatus;
	}  

	public String getChld_ntw_ids() {
		return chld_ntw_ids;
	}

	public void setChld_ntw_ids(String chld_ntw_ids) {
		this.chld_ntw_ids = chld_ntw_ids;
	}
	
	public void getNetworkValues(String networkType,Integer accountId,String moreParam) {
    	int rows = 0;
    	String searchByName="";
    	String calledFrom="";
    	String studyNtIds="";
    	String filter="";
    	String studyId="";
        PreparedStatement pstmt = null;
        Connection conn = null;
        String mainSql="";
        String childsql="";
        String networkId="";
        
        try {
        	JSONObject jsonObj=new JSONObject(moreParam);
        	if (jsonObj.has("searchByName")) {
        		if("child".equalsIgnoreCase(networkType)){
        			searchByName = "";
        		}else{
        			searchByName = jsonObj.getString("searchByName");
        			if(StringUtil.isEmpty(searchByName))
        				searchByName="";
        		}
             }
        	if (jsonObj.has("calledFrom")) {
        		calledFrom = jsonObj.getString("calledFrom");
            }
        	if (jsonObj.has("studyNtIds")) {
        		studyNtIds = jsonObj.getString("studyNtIds");
            }
        	if (jsonObj.has("filter")) {
        		filter = jsonObj.getString("filter");
            }
        	if (jsonObj.has("studyId")) {
        		studyId = jsonObj.getString("studyId");
            }
        	if(jsonObj.has("networkId")){
        		networkId=jsonObj.getString("networkId");
        	}
        	if (calledFrom.equals("studynetworkTabs"))
        		childsql=" AND pk_nwsites IN ("+studyNtIds+") ";
            conn = getConnection();
            if("parent".equals(networkType) && (searchByName.length()>0))
            {
            	if(calledFrom.equals("studynetworkTabs")){
            		mainSql="SELECT pk_nwsites,fk_site,site_name,nw_level,(select codelst_desc from er_codelst where pk_codelst=nw_membertype) network_memtypdesc,cd1.codelst_desc network_statdesc,nw_membertype,nw_status, sth.PK_STATUS,sth.codelst_desc FROM er_nwsites,er_site,er_codelst cd1, (SELECT FK_CODELST_STAT,PK_STATUS,status_modpk,status_modtable,codelst_desc FROM ER_STATUS_HISTORY,er_codelst WHERE pk_codelst = FK_CODELST_STAT AND status_iscurrent = 1 AND ER_STATUS_HISTORY.STATUS_PARENTMODPK = "+studyId+") sth  WHERE pk_site = fk_site AND pk_nwsites = sth.status_modpk(+) AND cd1.pk_codelst = NW_STATUS AND er_site.fk_account = ? AND (pk_nwsites IN (SELECT PK_NWSITES FROM er_nwsites WHERE connect_by_isleaf = 1 START WITH PK_NWSITES IN (SELECT b.pk_nwsites FROM er_nwsites b, (SELECT pk_nwsites FROM ER_NWSITES START WITH pk_nwsites IN (SELECT pk_nwsites FROM er_nwsites WHERE nw_level=0) CONNECT BY PRIOR pk_nwsites = FK_NWSITES) nw, er_site,er_codelst cd1 WHERE pk_site = fk_site AND nw.pk_nwsites = b.pk_nwsites AND cd1.pk_codelst = NW_STATUS AND er_site.fk_account = ? AND nw_level <> 0 ";
            	}
            	else{
            		mainSql="SELECT pk_nwsites,fk_site,site_name,nw_level,(select codelst_desc from er_codelst where pk_codelst=nw_membertype) network_memtypdesc,cd1.codelst_desc network_statdesc,ctep_id,nw_membertype,nw_status, sth.PK_STATUS FROM er_nwsites,er_site,er_codelst cd1, (SELECT FK_CODELST_STAT,PK_STATUS,status_modpk,status_modtable,codelst_desc FROM ER_STATUS_HISTORY,er_codelst WHERE pk_codelst = FK_CODELST_STAT AND status_iscurrent = 1 AND ER_STATUS_HISTORY.STATUS_PARENTMODPK is null and ER_STATUS_HISTORY.STATUS_MODTABLE='er_nwsites') sth WHERE pk_site = fk_site AND pk_nwsites = sth.status_modpk(+) AND cd1.pk_codelst = NW_STATUS AND er_site.fk_account = ? AND (pk_nwsites IN (SELECT PK_NWSITES FROM er_nwsites WHERE connect_by_isleaf = 1 START WITH PK_NWSITES IN (SELECT b.pk_nwsites FROM er_nwsites b, (SELECT pk_nwsites FROM ER_NWSITES START WITH pk_nwsites IN (SELECT pk_nwsites FROM er_nwsites WHERE nw_level=0) CONNECT BY PRIOR pk_nwsites = FK_NWSITES) nw, er_site,er_codelst cd1 WHERE pk_site = fk_site AND nw.pk_nwsites = b.pk_nwsites AND cd1.pk_codelst = NW_STATUS AND er_site.fk_account = ? AND nw_level <> 0 ";
            	}
            	if(filter.equalsIgnoreCase("name")){
            		mainSql+= "AND lower(site_name) LIKE LOWER( '%"+searchByName+"%')) CONNECT BY PRIOR FK_NWSITES=PK_NWSITES ) OR (lower(site_name) LIKE lower('%"+searchByName+"%'))) ";
            	}
            	else if(filter.equalsIgnoreCase("type")){
            		mainSql+= "AND (SELECT lower(codelst_desc) FROM er_codelst WHERE pk_codelst=nw_membertype) LIKE LOWER( '%"+searchByName+"%')) CONNECT BY PRIOR FK_NWSITES=PK_NWSITES ) OR ((SELECT lower(codelst_desc) FROM er_codelst WHERE pk_codelst=nw_membertype) LIKE lower ('%"+searchByName+"%'))) ";
            	}
            	else if(filter.equalsIgnoreCase("status")){
            		mainSql+= "AND lower(cd1.codelst_desc) LIKE LOWER( '%"+searchByName+"%')) CONNECT BY PRIOR FK_NWSITES=PK_NWSITES ) OR (lower(cd1.codelst_desc) LIKE lower ('%"+searchByName+"%'))) ";
            	}
            	else if(filter.equalsIgnoreCase("Networkstatus")){
            		mainSql+= "AND lower(sth.codelst_desc) LIKE LOWER( '%"+searchByName+"%')) CONNECT BY PRIOR FK_NWSITES=PK_NWSITES ) OR (lower(sth.codelst_desc) LIKE lower ('%"+searchByName+"%'))) ";
            	} 
            	else if(filter.equalsIgnoreCase("ctepid"))
            	{
            		mainSql+= "AND lower(ctep_id) LIKE LOWER( '%"+searchByName+"%')) CONNECT BY PRIOR FK_NWSITES=PK_NWSITES ) OR (lower(ctep_id) LIKE lower('%"+searchByName+"%'))) ";
            	}
            	else{
            		mainSql+= "AND (lower(site_name) LIKE LOWER( '%"+searchByName+"%'))) CONNECT BY PRIOR FK_NWSITES=PK_NWSITES ) OR (lower(site_name) LIKE lower('%"+searchByName+"%')))";
            	}
            	if (calledFrom.equals("studynetworkTabs"))
            		mainSql+="and pk_nwsites in (select to_number(substr(postfix, 1, instr(postfix, ',' ,1)-1)) from (select substr(study_network, instr(study_network, ',',1, level)+1) postfix from (select ','||study_network||',' study_network from er_study where pk_study="+studyId+") connect by instr(study_network, ',', 2, level) > 0))  AND nw_level = 0 ORDER BY site_name";
            	else	
            		mainSql+="AND nw_level = 0 and er_nwsites.PK_NWSITES in ("+networkId+") ORDER BY pk_nwsites";
            	pstmt = conn.prepareStatement(mainSql);
		        pstmt.setInt(1, accountId);
		        pstmt.setInt(2, accountId);
            }
            else
            {
		        if("parent".equals(networkType))
		        {
		        	if(null==searchByName ||"".equals(searchByName)){
		        		mainSql=" select pk_nwsites,fk_site,site_name,nw_level,(select codelst_desc from er_codelst where pk_codelst=nw_membertype) network_memtypdesc, cd1.codelst_desc network_statdesc,ctep_id,nw_membertype,nw_status,sth.PK_STATUS ";
		        		if(calledFrom.equals("studynetworkTabs")) mainSql=mainSql+" ,sth.FK_CODELST_STAT,sth.codelst_desc,sth.status_modtable ";
		        		mainSql=mainSql+" from er_nwsites,er_site, er_codelst cd1,(select FK_CODELST_STAT,PK_STATUS,status_modpk,status_modtable,codelst_desc from ER_STATUS_HISTORY,er_codelst where pk_codelst=FK_CODELST_STAT and status_iscurrent=1 ";
		        		if(calledFrom.equals("studynetworkTabs")) mainSql=mainSql+" and ER_STATUS_HISTORY.STATUS_PARENTMODPK="+studyId+") sth";
		        		else mainSql=mainSql+" and ER_STATUS_HISTORY.STATUS_PARENTMODPK is null and ER_STATUS_HISTORY.STATUS_MODTABLE='er_nwsites' ) sth";
		        		mainSql=mainSql+" where pk_site=fk_site and pk_nwsites = sth.status_modpk(+) ";
		        		if(calledFrom.equals("studynetworkTabs"))mainSql=mainSql+" and pk_nwsites in ("+studyNtIds+") ";
		        		mainSql=mainSql+ " and cd1.pk_codelst=NW_STATUS and er_site.fk_account=? and nw_level=0 ";
		        		if(calledFrom.equals("studynetworkTabs"))
		        			mainSql=mainSql+" order by lower(site_name)";
		        		else
		        			mainSql=mainSql+" and er_nwsites.PK_NWSITES in ("+networkId+") order by pk_nwsites";
		        	}
		        	else{
		        		mainSql="select pk_nwsites,fk_site,site_name,nw_level,(select codelst_desc from er_codelst where pk_codelst=nw_membertype) network_memtypdesc, cd1.codelst_desc network_statdesc,ctep_id,nw_membertype,nw_status from er_nwsites,er_site, er_codelst cd1 where pk_site=fk_site and cd1.pk_codelst=NW_STATUS and er_site.fk_account=? and nw_level=0 and";
		        		if(filter.equalsIgnoreCase("name")){
		        			mainSql+= "  (lower(site_name) LIKE lower('%"+searchByName+"%')) "+" order by  pk_nwsites";
		        		}
		        		else if(filter.equalsIgnoreCase("type")){
		        			mainSql+= " ((SELECT lower(codelst_desc) FROM er_codelst WHERE pk_codelst=nw_membertype) LIKE lower ('%"+searchByName+"%')) "+" order by  pk_nwsites";
		        		}
		        		else if(filter.equalsIgnoreCase("ctepid"))
		        		{
		        			mainSql+= " (lower(ctep_id)  like lower('%"+searchByName+"%')) ";
		        		}
		        		else{
		        			mainSql+= " (lower(site_name)  like lower('%"+searchByName+"%')) ";
		        			if(calledFrom.equals("studynetworkTabs"))
		        				mainSql+=" order by  site_name";
		        			else
		        				mainSql+=" and fk_nwsites_main in ("+networkId+") order by  pk_nwsites";
		        		}
		        	}
		        }
		        else
		        {
		        	if(null==searchByName ||"".equals(searchByName)){
		        		if(calledFrom.equals("studynetworkTabs"))
		        			mainSql="SELECT b.pk_nwsites,fk_nwsites,fk_site,site_name,nw_level,(select codelst_desc from er_codelst where pk_codelst=nw_membertype) network_memtypdesc, cd1.codelst_desc network_statdesc,nw_membertype,fk_nwsites_main,nw_status,nw.FK_CODELST_STAT,nw.PK_STATUS,nw.codelst_desc,nw.status_modtable FROM er_nwsites b, (select pk_nwsites,sth.FK_CODELST_STAT,sth.PK_STATUS,sth.codelst_desc,sth.status_modtable from (SELECT rownum rnum,pk_nwsites,site_name FROM ER_NWSITES,ER_SITE where ER_NWSITES.fk_site=ER_SITE.PK_SITE START WITH pk_nwsites IN (SELECT pk_nwsites FROM er_nwsites WHERE nw_level  =0 "+childsql+" ) CONNECT BY PRIOR pk_nwsites = FK_NWSITES order SIBLINGS by lower(SITE_NAME)) nw,(select FK_CODELST_STAT,PK_STATUS,status_modpk,status_modtable,codelst_desc from ER_STATUS_HISTORY,er_codelst where pk_codelst=FK_CODELST_STAT and status_iscurrent=1 and ER_STATUS_HISTORY.STATUS_PARENTMODPK="+studyId+") sth where pk_nwsites=status_modpk(+) order by rnum ) nw, er_site, er_codelst cd1 WHERE pk_site =fk_site AND nw.pk_nwsites =b.pk_nwsites AND cd1.pk_codelst =NW_STATUS AND er_site.fk_account=? AND nw_level<>0";
		        		else
		        			mainSql="select b.pk_nwsites,fk_nwsites,fk_site,site_name,nw_level,(select codelst_desc from er_codelst where pk_codelst=nw_membertype) network_memtypdesc, cd1.codelst_desc network_statdesc,ctep_id,nw_membertype,fk_nwsites_main,nw_status,nw.PK_STATUS from er_nwsites b,(select pk_nwsites,sth.FK_CODELST_STAT,sth.PK_STATUS,sth.codelst_desc,sth.status_modtable from (SELECT rownum rnum,pk_nwsites,SITE_NAME FROM ER_NWSITES,ER_SITE where ER_NWSITES.fk_site=ER_SITE.PK_SITE START WITH pk_nwsites IN (SELECT pk_nwsites FROM er_nwsites WHERE nw_level  =0 "+childsql+" ) CONNECT BY PRIOR pk_nwsites = FK_NWSITES order SIBLINGS by lower(SITE_NAME)) nw,(select FK_CODELST_STAT,PK_STATUS,status_modpk,status_modtable,codelst_desc from ER_STATUS_HISTORY,er_codelst where pk_codelst=FK_CODELST_STAT and status_iscurrent=1 and ER_STATUS_HISTORY.STATUS_PARENTMODPK is null and ER_STATUS_HISTORY.STATUS_MODTABLE='er_nwsites') sth where pk_nwsites=status_modpk(+) order by rnum ) nw,er_site, er_codelst cd1 where pk_site=fk_site and nw.pk_nwsites=b.pk_nwsites and cd1.pk_codelst=NW_STATUS and er_site.fk_account=? and nw_level<>0 and fk_nwsites_main in ("+networkId+") ";
		        	}
		        	else{
		        		mainSql="select b.pk_nwsites,fk_nwsites,fk_site,site_name,nw_level,(select codelst_desc from er_codelst where pk_codelst=nw_membertype) network_memtypdesc, cd1.codelst_desc network_statdesc,ctep_id,nw_membertype,fk_nwsites_main,nw_status from er_nwsites b,(SELECT pk_nwsites,site_name FROM ER_NWSITES,ER_SITE where ER_NWSITES.fk_site=ER_SITE.PK_SITE START WITH pk_nwsites in (select pk_nwsites from  er_nwsites where nw_level=0) CONNECT BY PRIOR pk_nwsites = FK_NWSITES order SIBLINGS by lower(SITE_NAME)) nw,er_site, er_codelst cd1 where pk_site=fk_site and nw.pk_nwsites=b.pk_nwsites and cd1.pk_codelst=NW_STATUS and er_site.fk_account=? and nw_level<>0 and lower(site_name) like LOWER( '%"+searchByName+"%')";
		        	}
		        }
		        pstmt = conn.prepareStatement(mainSql);
		        pstmt.setInt(1, accountId);
            }
	        ResultSet rs = pstmt.executeQuery();

	        while (rs.next()) {
	        	if("parent".equals(networkType)){
	        		setNetworkIdList(new Integer(rs.getInt("PK_NWSITES")));
	                setNetworkLevelList(rs.getString("NW_LEVEL"));
	                setNetworkTypeIdList(rs.getString("NW_MEMBERTYPE"));
	                setNetworkStatusIdList(rs.getString("NW_STATUS"));
	                setSiteIdList(rs.getString("FK_SITE"));
	                setSiteNameList(rs.getString("SITE_NAME"));
	                if(!calledFrom.equals("studynetworkTabs"))
	                {
	                	setCtepIdList(rs.getString("CTEP_ID"));
	                }
	                setNetworkTypeDescList(rs.getString("NETWORK_MEMTYPDESC"));
	                setNetworkStatusDescList(rs.getString("NETWORK_STATDESC"));
	                setHistoryIdsList(rs.getString("PK_STATUS"));
	        		
	        	}else{
	        		setNetworkIdList(new Integer(rs.getInt("PK_NWSITES")));
	                setNetworkSiteIdList(rs.getString("FK_NWSITES"));
	                setNetworkLevelList(rs.getString("NW_LEVEL"));
	                setNetworkTypeIdList(rs.getString("NW_MEMBERTYPE"));
	                setNetworkMainIdList(rs.getString("FK_NWSITES_MAIN"));
	                setNetworkStatusIdList(rs.getString("NW_STATUS"));
	                setSiteIdList(rs.getString("FK_SITE"));
	                setSiteNameList(rs.getString("SITE_NAME"));
	                setHistoryIdsList(rs.getString("PK_STATUS"));
	                if(!calledFrom.equals("studynetworkTabs"))
	                {
	                	setCtepIdList(rs.getString("CTEP_ID"));
	                }
	                setNetworkTypeDescList(rs.getString("NETWORK_MEMTYPDESC"));
	                setNetworkStatusDescList(rs.getString("NETWORK_STATDESC"));
	        	}
	        	if(calledFrom.equals("studynetworkTabs")){
	        		setStdNtwrkStatsList(rs.getString("codelst_desc"));
	        		//setHistoryIdsList(rs.getString("PK_STATUS"));
	        	}
	            rows++;
	        }

	        setcRows(rows);
	    } catch (SQLException ex) {
	    	ex.printStackTrace();
	        Rlog.fatal("network",
	                "NetworkDao.getNetworkValues EXCEPTION IN FETCHING FROM Network table"
	                        + ex);
	    }catch (Exception ex){
	    	ex.printStackTrace();	
	    } finally {
	        try {
	            if (pstmt != null)
	                pstmt.close();
	        } catch (Exception e) {
	        }
	        try {
	            if (conn != null)
	                conn.close();
	        } catch (Exception e) {
	        }

	    }

	}
	
	public void deleteNetwork(String userId, String IpAdd, Integer netId,Integer studyId){    	
	     
	    CallableStatement cstmt = null;
	    Connection conn = null; 
	     
	    
	     try{
	    
	   	conn  = EnvUtil.getConnection();
	   	cstmt = conn.prepareCall("{call DELETE_NETWORK(?,?,?,?,?)}");
	   	
	   	cstmt.setInt(1,netId);
	   	cstmt.setString(2,userId);
	   	cstmt.setString(3,IpAdd);
	   	cstmt.setString(4, "NA");
	   	cstmt.setInt(5,studyId);
	   	cstmt.execute();	
	    Rlog.debug("network", "NetworkDao.deleteNetwork()");
	    
	     }
	     catch(Exception e) {
	   			Rlog.fatal("Network","EXCEPTION in DELETE_NETWORK, excecuting Stored Procedure " + e);

	     
	     }
	     finally{
	   		try
	   		{
	   			if (cstmt!= null) cstmt.close();
	   		}
	   		catch (Exception e) 
	   		{
	   		}
	   		try 
	   		{
	   			if (conn!=null) conn.close();
	   		}
	   		catch (Exception e) {}
	     
	     }
	     
	    }
public void clear(){
	networkIdList.clear();
	networkSiteIdList.clear();
	siteIdList.clear();
	networkLevelList.clear();
	networkTypeIdList.clear();
	networkMainIdList.clear();
	networkStatusIdList.clear();
	siteNameList.clear();
	networkTypeDescList.clear();
}

public JSONArray  autocompleteUsersNetwork(String searchUser ,int account,int networkId){
	 JSONArray   maplist = new  JSONArray();
	 JSONObject dataMap = null;
	Connection conn = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    StringBuffer sqlBuffer = new StringBuffer();
    sqlBuffer.append("select pk_user,(usr_firstname || ' ' || usr_lastname) as name,add_email, usr_type from er_user, er_add where fk_Account=? and (lower(usr_firstname || ' ' || usr_lastname) LIKE lower('%"+searchUser+"%'))");
    sqlBuffer.append(" and pk_add=fk_peradd and pk_user not in (select fk_user from er_nwusers where fk_nwsites=?) AND usr_stat='A' AND usr_type !='X' order by lower(usr_firstname), lower(usr_lastname)");
	try {
		conn = CommonDAO.getConnection();
		pstmt = conn.prepareStatement(sqlBuffer.toString());
		pstmt.setInt(1, account);
		pstmt.setInt(2, networkId);
		rs = pstmt.executeQuery();

		while (rs.next()) {
			dataMap = new JSONObject();
			dataMap.put("key", rs.getInt(("pk_user")));
			if(rs.getString("add_email")==null)
				dataMap.put("value", rs.getString("name"));
			else
				dataMap.put("value", rs.getString("name")+"   ["+rs.getString("add_email")+"]");
			maplist.put(dataMap);
		}

	} catch (Exception e) {
		e.printStackTrace();
	} finally {
		try {
			rs.close();
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	return maplist;
}

public JSONArray autocompleteNetworks(String searchBy, int accountId, String existingNetIds){
	JSONArray mapList=new JSONArray();
	JSONObject dataMap=null;
	Connection conn = null;
	PreparedStatement pstmt=null;
	ResultSet rs=null;
	StringBuffer sql=new StringBuffer();
	sql.append("select pk_nwsites,fk_site,site_name as network_name,nvl((select codelst_desc from er_codelst where pk_codelst=NW_MEMBERTYPE),'-') as relnType from er_nwsites,er_site ");
	sql.append(" where pk_site=fk_site and er_site.fk_account=? and nw_level=0 ");
	sql.append(" and lower(site_name) like lower('%"+searchBy+"%') ");
	if(!existingNetIds.equals("") && !(searchBy.equals(""))){
		sql.append(" and pk_nwsites not in ("+existingNetIds+")");
	}
	sql.append(" order by lower(network_name)");
	
	try{
		conn=CommonDAO.getConnection();
		pstmt=conn.prepareStatement(sql.toString());
		pstmt.setInt(1, accountId);
		rs = pstmt.executeQuery();
		
		while(rs.next()){
			dataMap=new JSONObject();
			dataMap.put("relnType", rs.getString("relnType"));
			dataMap.put("network_name", rs.getString("network_name"));
			dataMap.put("pk_nwsites", rs.getInt("pk_nwsites"));
			mapList.put(dataMap);
		}
		
	}catch (Exception e) {
		e.printStackTrace();
	} finally {
		try {
			rs.close();
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	return mapList;
}

public JSONArray  autocompleteStudyNetwork(String searchNetwork ,int account,int studyId){
	 JSONArray   maplist = new  JSONArray();
	 JSONObject dataMap = null;
	Connection conn = null;
   PreparedStatement pstmt = null;
   ResultSet rs = null;
   StringBuffer sqlBuffer = new StringBuffer();
   String networkIds="";
   networkIds=getStudyNetworkIds(studyId);
   sqlBuffer.append("SELECT pk_nwsites,fk_site,site_name,(select codelst_desc from er_codelst where pk_codelst=nw_status) as nw_status FROM er_nwsites,er_site WHERE fk_Account=? AND lower(site_name) LIKE lower('%"+searchNetwork+"%') AND pk_site=fk_site ");
   sqlBuffer.append(" AND nw_level=0 AND nw_status <> (select pk_codelst from er_codelst where codelst_type='networkstat' and codelst_subtyp='inactive') ");
   if((networkIds!=null) && (!"".equals(networkIds))){
   sqlBuffer.append("AND pk_nwsites NOT IN ("+networkIds+") ");
   }
   sqlBuffer.append(" ORDER BY lower(site_name) ");
   try {
		conn = CommonDAO.getConnection();
		pstmt = conn.prepareStatement(sqlBuffer.toString());
		pstmt.setInt(1, account);
		rs = pstmt.executeQuery();
		while (rs.next()) {
			dataMap = new JSONObject();
			dataMap.put("key", rs.getInt("pk_nwsites"));
			dataMap.put("value", rs.getString("site_name")+" ["+rs.getString("nw_status")+"]");
			maplist.put(dataMap);
		}

	} catch (Exception e) {
		e.printStackTrace();
	} finally {
		try {
			rs.close();
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	return maplist;
}

public int saveUserNetwork(int networkId,int assignUserId,int userId){
	PreparedStatement pstmt = null;
	PreparedStatement pstmtS = null;
    Connection conn = null;
    String mainSql="";
    int operationFlag=0;
    String sql="";
    ResultSet rs = null;
    int nustatus = 0;
    try{
    sql="select pk_codelst from er_codelst where codelst_type='networkuserstat' and codelst_subtyp='Pending'";
    conn = getConnection();
    pstmtS = conn
            .prepareStatement(sql);
	rs = pstmtS.executeQuery();
	while (rs.next()) {
		nustatus=rs.getInt("pk_codelst");
	}
     mainSql="insert into er_nwusers (pk_nwusers,fk_nwsites,fk_user,creator,NWU_STATUS) values(ERES.SEQ_ER_NWUSERS.nextval,?,?,?,?)";
  
    	pstmt = conn
                .prepareStatement(mainSql);
    	
    	pstmt.setInt(1, networkId);
    	pstmt.setInt(2, assignUserId);
    	pstmt.setInt(3, userId);
    	pstmt.setInt(4, nustatus);
    	operationFlag=pstmt.executeUpdate();
    }catch(SQLException e){
    	Rlog.fatal("Network","EXCEPTION in saveUserNetwork of NetworkDao class " + e);
    } finally {
		try {
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	return operationFlag;
}
public void getNetworkUsers(int networkId,int accountId,String searchNWUser){
	int rows = 0;
	PreparedStatement pstmt = null;
    Connection conn = null;
    ResultSet rs = null;
    String mainSql="";
    if("".equals(searchNWUser))
    	mainSql="SELECT pk_nwusers,cd1.codelst_desc network_statdesc,a.fk_nwsites,fk_user,(usr_firstname||' '||usr_lastname) AS usr_name ,DECODE(USR_TYPE,'N','NS','AA') USR_TYPE,nwu_membertrole,nwu_memberaddtrole,(SELECT codelst_desc FROM er_codelst WHERE pk_codelst=nwu_membertrole) AS nw_trole,NWU_STATUS,sth.PK_STATUS PK_STATUS FROM er_nwusers a,er_user c,er_nwsites d,er_codelst cd1,(SELECT FK_CODELST_STAT,PK_STATUS,status_modpk,status_modtable,codelst_desc FROM ER_STATUS_HISTORY,er_codelst WHERE pk_codelst = FK_CODELST_STAT AND status_iscurrent = 1) sth  WHERE pk_user=fk_user and a.fk_nwsites=d.pk_nwsites and  usr_type not in ('X','P') and c.fk_account = ? and d.pk_nwsites=? and pk_nwusers= sth.status_modpk(+) and cd1.pk_codelst =NWU_STATUS and sth.status_modtable = 'er_nwusers' order by lower(usr_firstname),lower(usr_lastname)";
    else
    	mainSql="select * from (SELECT pk_nwusers,cd1.codelst_desc network_statdesc,a.fk_nwsites,fk_user,(usr_firstname||' '||usr_lastname) AS usr_name ,DECODE(USR_TYPE,'N','NS','AA') USR_TYPE,nwu_membertrole,nwu_memberaddtrole,(SELECT codelst_desc FROM er_codelst WHERE pk_codelst=nwu_membertrole) AS nw_trole, usr_firstname, usr_lastname,NWU_STATUS,sth.PK_STATUS PK_STATUS FROM er_nwusers a,er_user c,er_nwsites d,er_codelst cd1,(SELECT FK_CODELST_STAT,PK_STATUS,status_modpk,status_modtable,codelst_desc FROM ER_STATUS_HISTORY,er_codelst WHERE pk_codelst = FK_CODELST_STAT AND status_iscurrent = 1) sth  WHERE pk_user=fk_user and a.fk_nwsites=d.pk_nwsites and  usr_type not in ('X','P') and c.fk_account = ? and d.pk_nwsites=? and pk_nwusers= sth.status_modpk(+) and cd1.pk_codelst =NWU_STATUS and sth.status_modtable = 'er_nwusers')a where lower(a.usr_name) like lower('%"+searchNWUser+"%') order by lower(usr_firstname),lower(usr_lastname) ";
    try{
    	conn = getConnection();
    	pstmt = conn
                .prepareStatement(mainSql);
    	pstmt.setInt(1, accountId);
    	pstmt.setInt(2, networkId);
    	rs = pstmt.executeQuery();
    	while (rs.next()) {
    		setNetworkUsrPkList(rs.getInt("pk_nwusers"));
    		setNetworkUsersList(rs.getInt("fk_user"));
    		setNetworkMemeberTRoleList(rs.getInt("nwu_membertrole"));
    		setNetworkMemeberADTRoleList(rs.getString("nwu_memberaddtrole"));
    		setNetworkTRoleNameList(rs.getString("nw_trole"));
    		//setNetworkADTRoleNameList(rs.getString("nw_adtrole"));
    		setNetworkUsrNameList(rs.getString("usr_name"));
    		setNetworkUsrStatusList(rs.getString("network_statdesc"));    		
    		setNetworkUsrTypeList(rs.getString("usr_type"));
    		setNetworkUserStatusList(rs.getInt("NWU_STATUS"));
    		setHistoryIdsList(rs.getString("PK_STATUS"));
    		rows++;
    	}
    	setcRows(rows);
    }catch(SQLException e){
    	Rlog.fatal("Network","EXCEPTION in getNetworkUsers of NetworkDao class " + e);	
    } finally {
		try {
			rs.close();
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}

public void getAppndxNtOrgDetails(int networkId){
	PreparedStatement pstmt = null;
	Connection conn = null;
	ResultSet rs = null;
	String sql="";
	sql = "SELECT site_name,(select site_name from er_site  where pk_site=b.fk_site) as main_network,"+
			"cd1.codelst_desc as site_type,(select CODELST_DESC from ER_CODELST where PK_CODELST=a.nw_membertype) As relationship_type FROM er_nwsites a,"+
			"er_nwsites b, er_site, er_codelst cd1 WHERE a.pk_nwsites = ? and pk_site=a.fk_site and "+
			"fk_codelst_type=cd1.pk_codelst and nvl(a.fk_nwsites_main,a.pk_nwsites)=b.pk_nwsites";
	try{
		conn = getConnection();
		pstmt = conn.prepareStatement(sql);
    	pstmt.setInt(1, networkId);
    	rs = pstmt.executeQuery();
    	while(rs.next()){
    		setSiteNameList(rs.getString("site_name"));
    		setMainNetwork(rs.getString("main_network"));
    		setSiteType(rs.getString("site_type"));
    		setNetworkTypeDescList(rs.getString("relationship_type"));	
    	}
	}catch(SQLException e){
		Rlog.fatal("Network","EXCEPTION in updateUserNetwork of NetworkDao class " + e);
	}
}

public int  updateUserNetwork(int nwUserPk,int userTRole, int userid){
	PreparedStatement pstmt = null;
    Connection conn = null;
    String mainSql="";
    int operationFlag=0;
    mainSql="update  er_nwusers set NWU_MEMBERTROLE=?, LAST_MODIFIED_BY=? where PK_NWUSERS=?";
    try{
    	conn = getConnection();
    	pstmt = conn
                .prepareStatement(mainSql);
    	
    	pstmt.setInt(1, userTRole);
    	pstmt.setInt(2, userid);
    	pstmt.setInt(3, nwUserPk);
    	operationFlag=pstmt.executeUpdate();
    }catch(SQLException e){
    	Rlog.fatal("Network","EXCEPTION in updateUserNetwork of NetworkDao class " + e);
    } finally {
		try {
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	return operationFlag;
	
	
}

public int  updateUserAdditional(int nwUserPk,int userTRole,int userid){
	PreparedStatement pstmt = null;
    Connection conn = null;
    String mainSql="";
    int operationFlag=0;
    mainSql="update  ER_NWUSERS_ADDNLROLES set NWU_MEMBERADDLTROLE=?, LAST_MODIFIED_BY=? where PK_NWUSERS_ADDROLES=?";
    try{
    	conn = getConnection();
    	pstmt = conn.prepareStatement(mainSql);
    	
    	pstmt.setInt(1, userTRole);
    	pstmt.setInt(2, userid);
    	pstmt.setInt(3, nwUserPk);
    	operationFlag=pstmt.executeUpdate();
    }catch(SQLException e){
    	e.printStackTrace();
    	Rlog.fatal("Network","EXCEPTION in updateUserNetwork of NetworkDao class " + e);
    } finally {
		try {
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	return operationFlag;
	
	
}
public int  updateUserNetworkaD(int nwUserPk,String userTRole,int userid){
	PreparedStatement pstmt = null;
    Connection conn = null;
    String mainSql="";
    int operationFlag=0;
    mainSql="update  er_nwusers set NWU_MEMBERADDTROLE=?, LAST_MODIFIED_BY=? where PK_NWUSERS=?";
    try{
    	conn = getConnection();
    	pstmt = conn
                .prepareStatement(mainSql);
    	
    	pstmt.setString(1, userTRole);
    	pstmt.setInt(2, userid);
    	pstmt.setInt(3, nwUserPk);
    	operationFlag=pstmt.executeUpdate();
    }catch(SQLException e){
    	Rlog.fatal("Network","EXCEPTION in updateUserNetwork of NetworkDao class " + e);
    } finally {
		try {
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	return operationFlag;
	
	
}
public int  deleteUserNetwork(int nwUserPk,int userId,String ipAdd){
	CallableStatement cstmt = null;
    Connection conn = null; 
    int operationFlag=0;
    
     try{
    
   	conn  = EnvUtil.getConnection();
   	cstmt = conn.prepareCall("{call DELETE_NETWORK(?,?,?,?,?)}");
   	
   	cstmt.setInt(1,nwUserPk);
   	cstmt.setInt(2,userId);
   	cstmt.setString(3,ipAdd);
   	cstmt.setString(4, "NU");
   	cstmt.setInt(5,0);
   	cstmt.execute();
   	operationFlag=1;
    Rlog.debug("network", "NetworkDao.deleteUserNetwork()");
    
     }
     catch(Exception e) {
   			Rlog.fatal("Network","EXCEPTION in DELETE_NETWORK_USER, excecuting Stored Procedure " + e);
   			operationFlag=-1;
     
     }
     finally{
   		try
   		{
   			if (cstmt!= null) cstmt.close();
   		}
   		catch (Exception e) 
   		{
   		}
   		try 
   		{
   			if (conn!=null) conn.close();
   		}
   		catch (Exception e) {}
     
     }
     return operationFlag;
}
public int saveNetworkToStudy(int studyId,String networkId,String userId,String ipAdd){
	PreparedStatement pstmt = null;
    Connection conn = null;
    String mainSql="";
    int operationFlag=0;
    String networkIds="";
    networkIds=getStudyNetworkIds(studyId);
    if("".equals(networkIds)||networkIds==null)networkIds=networkId;
    else 
    	networkIds=networkIds+","+networkId;
    mainSql="update  er_study set study_network=? where pk_study=?";
    try{
    	conn = getConnection();
    	pstmt = conn
                .prepareStatement(mainSql);
    	
    	pstmt.setString(1, networkIds);
    	pstmt.setInt(2, studyId);
    	operationFlag=pstmt.executeUpdate();
    	updateStudyNetworkStatuses(userId,ipAdd,StringUtil.stringToInteger(networkId),studyId);
    	
    }catch(SQLException e){
    	Rlog.fatal("Network","EXCEPTION in saveNetworkToStudy of NetworkDao class " + e);
    } finally {
		try {
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	return operationFlag;
	
}
public String getStudyNetworkIds(int studyId){
	String netIds="";
	PreparedStatement pstmt = null;
    Connection conn = null;
    ResultSet rs = null;
    String mainSql="";
    int operationFlag=0;
    mainSql="select study_network from  er_study  where pk_study=?";
    try{
    	conn = getConnection();
    	pstmt = conn
                .prepareStatement(mainSql);
    	
    	
    	pstmt.setInt(1, studyId);
    	rs = pstmt.executeQuery();
    	while(rs.next()){
    		netIds=rs.getString("study_network");
    	}
    }catch(SQLException e){
    	Rlog.fatal("Network","EXCEPTION in saveNetworkToStudy of NetworkDao class " + e);
    } finally {
		try {
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	return netIds;
}

public Integer getStudyNetworkCount(int netId){
	int Count=0;
	PreparedStatement pstmt = null;
    Connection conn = null;
    ResultSet rs = null;
    String mainSql="";
    mainSql=" select count(*) as cnt from  ER_STATUS_HISTORY  where STATUS_MODPK=? and STATUS_MODTABLE='er_nwsites' and STATUS_PARENTMODPK is not null ";
    try{
    	conn = getConnection();
    	pstmt = conn
                .prepareStatement(mainSql);
    	
    	
    	pstmt.setInt(1, netId);
    	rs = pstmt.executeQuery();
    	while(rs.next()){
    		Count=rs.getInt("cnt");
    	}
    }catch(SQLException e){
    	Rlog.fatal("Network","EXCEPTION in getStudyNetworkCount of NetworkDao class " + e);
    } finally {
		try {
			rs.close();
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	return Count;
}

public void updateStudyNetworkStatuses(String userId, String IpAdd, Integer netId,Integer studyId){    	
    
    CallableStatement cstmt = null;
    Connection conn = null; 
     
    
     try{
    
   	conn  = EnvUtil.getConnection();
   	cstmt = conn.prepareCall("{call SP_UPDATESTNTSTAT(?,?,?,?)}");
   	
   	cstmt.setInt(1,netId);
   	cstmt.setInt(2,studyId);
   	cstmt.setString(3,userId);
   	cstmt.setString(4,IpAdd);
   	cstmt.execute();	
    Rlog.debug("network", "NetworkDao.updateStudyNetworkStatuses()");
    
     }
     catch(Exception e) {
   			Rlog.fatal("Network","EXCEPTION in SP_UPDATESTNTSTAT, excecuting Stored Procedure " + e);

     
     }
     finally{
   		try
   		{
   			if (cstmt!= null) cstmt.close();
   		}
   		catch (Exception e) 
   		{
   		}
   		try 
   		{
   			if (conn!=null) conn.close();
   		}
   		catch (Exception e) {}
     
     }
     
    }
public int deleteStudyNetwork(int studyId,int netWorkId,String userId, String ipAdd){
	String networkIds="";
	networkIds=getStudyNetworkIds(studyId);
	String []netWorkArray={};
	netWorkArray=networkIds.split(",");
	networkIds="";
	for(int i=0;i<netWorkArray.length;i++){
		if(netWorkArray.length==1)networkIds="";
		if(StringUtil.stringToNum(netWorkArray[i])==netWorkId)continue;
		networkIds=networkIds+netWorkArray[i]+",";
	}
	if(!"".equals(networkIds))
	networkIds=networkIds.substring(0, networkIds.length()-1);
	
	PreparedStatement pstmt = null;
    Connection conn = null;
    String mainSql="";
    int operationFlag=0;
    mainSql="update  er_study set study_network=? where pk_study=?";
    try{
    	conn = getConnection();
    	pstmt = conn
                .prepareStatement(mainSql);
    	
    	pstmt.setString(1, networkIds);
    	pstmt.setInt(2, studyId);
    	operationFlag=pstmt.executeUpdate();
    	deleteNetwork(userId,ipAdd,netWorkId,studyId);
    	
    }catch(SQLException e){
    	Rlog.fatal("Network","EXCEPTION in saveNetworkToStudy of NetworkDao class " + e);
    } finally {
		try {
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	return operationFlag;
}
public int  saveNetworkStatus(int siteId ,int statusId, String statusDate, String notes, int study_id){
	int ret=0;
	CallableStatement cstmt = null;
    Connection conn = null; 
    int operationFlag=0;
    java.util.Date statusEnteredDate=DateUtil.stringToDate(statusDate, null);
    
    try{
    	conn  = EnvUtil.getConnection();
       	cstmt = conn.prepareCall("{call SAVE_NETSTATUS(?,?,?,?,?,?)}");
       	cstmt.setInt(1,siteId);
       	cstmt.setInt(2,statusId);
       	cstmt.setDate(3,DateUtil.dateToSqlDate(statusEnteredDate));
       	cstmt.setString(4, notes);
       	cstmt.registerOutParameter(5,java.sql.Types.INTEGER);
       	cstmt.setInt(6, study_id);
       	cstmt.execute();
       	ret=cstmt.getInt(5);
    	
    }catch(Exception e) {
			Rlog.fatal("Network","EXCEPTION in DELETE_NETWORK_USER, excecuting Stored Procedure " + e);
			operationFlag=-1;
 
 }
 finally{
		try
		{
			if (cstmt!= null) cstmt.close();
		}
		catch (Exception e) 
		{
		}
		try 
		{
			if (conn!=null) conn.close();
		}
		catch (Exception e) {}
 
 }
    return ret;
}

public int additonalCheckFlag(int id,String ipAdd,int pkuser){
	int additonalId=0;
	int additonalrolestatus = 0;
	int operationFlag = 0;
	int PK_NWUSERS_ADDROLES= 0;
		 PreparedStatement pstmt = null;
	    Connection conn = null;
	    ResultSet rs = null;
	    String mainSql="";
	    mainSql = "select pk_codelst from er_codelst where codelst_type='networkuserstat' and codelst_subtyp='Pending'";
	    
	    try{
	    	conn = getConnection();
	    	pstmt = conn.prepareStatement(mainSql);
	    	
	    	 rs = pstmt.executeQuery();
	    	while(rs.next()){
	    		additonalrolestatus = rs.getInt("pk_codelst");	
	    	}
	    	
	    	mainSql="insert into ER_NWUSERS_ADDNLROLES (PK_NWUSERS_ADDROLES,FK_NWUSERS,NWU_ADTSTATUS,creator,IP_ADD) values(ERES.SEQ_ER_NWUSERS_ADDNLROLES.nextval,?,?,?,?)";
	    	pstmt = conn
	                .prepareStatement(mainSql);
	    	
	    	pstmt.setInt(1, id);
	    	pstmt.setInt(2, additonalrolestatus);
	    	pstmt.setInt(3, pkuser);
	    	pstmt.setString(4, ipAdd);
	    	operationFlag=pstmt.executeUpdate();
	    }
	catch(SQLException e){
		e.printStackTrace();
		Rlog.fatal("Network","EXCEPTION in additonalCheckFlag of NetworkDao class " + e);
	
    } finally {
		try {
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}}
	
	return operationFlag;
}
public String additonalCheckfetch(int id){
	 String PK_NWUSERS_ADDROLES= "";
	 String pk_status= "";
	 String Status = "";
		 
	 String addtionalval="";
	 
	 PreparedStatement pstmt = null;
	    Connection conn = null;
	    ResultSet rs = null;
	    String mainSql="";
	    
	    mainSql = "SELECT PK_NWUSERS_ADDROLES,pk_status,F_CODELST_DESC(NWU_MEMBERADDLTROLE) rolesdesc,F_CODELST_DESC(NWU_ADTSTATUS) role_status FROM ER_NWUSERS_ADDNLROLES,Er_STATUS_HISTORY sth WHERE PK_NWUSERS_ADDROLES =(SELECT MAX(PK_NWUSERS_ADDROLES)FROM ER_NWUSERS_ADDNLROLES WHERE FK_NWUSERS =? ) AND sth.status_modtable  = 'er_nwusers_addnlroles' AND sth.STATUS_MODPK     = PK_NWUSERS_ADDROLES AND sth.STATUS_ISCURRENT = 1  ";
	    try{
    	conn = getConnection();
    	pstmt = conn.prepareStatement(mainSql);
    	
    	pstmt.setInt(1, id);
    	rs = pstmt.executeQuery();
    	if(rs.next()){
    		PK_NWUSERS_ADDROLES = rs.getInt("PK_NWUSERS_ADDROLES")+"";
    		pk_status = rs.getInt("pk_status")+"";
    		Status = rs.getString("role_status");
    		
    	}
    	addtionalval =PK_NWUSERS_ADDROLES+","+pk_status+","+Status;
    	}
	    catch(SQLException e){
	    	e.printStackTrace();
			Rlog.fatal("Network","EXCEPTION in additonalCheckfetch of NetworkDao class " + e);
		
	    } finally {
			try {
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}}
	    
	return addtionalval;
	
}
 public String codelst_desc (String codelst_id){
	  PreparedStatement pstmt = null;
	    Connection conn = null;
	    ResultSet rs = null;
	    String mainSql="";
	  if(!codelst_id.equalsIgnoreCase("")){
		   int id = StringUtil.stringToInteger(codelst_id);
		  
		  mainSql = "select codelst_desc from er_codelst where pk_codelst=?" ;
		    try{
		    	conn = getConnection();
		    	pstmt = conn
		                .prepareStatement(mainSql);
		    	
		    	
		    	pstmt.setInt(1, id);
		    	 rs = pstmt.executeQuery();
		    	if(rs.next()){
		    		codelst_id = rs.getString("codelst_desc");	
		    	}
		    	
		    }catch(SQLException e){
		    	Rlog.fatal("Network","EXCEPTION in codelst_desc of NetworkDao class " + e);
		    } finally {
				try {
					pstmt.close();
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
	  
	  }
	  
	  
	  return codelst_id;
	 
 }
 
 public int  updateUserNetworkUserStatus(int nwUserPk,int status, String moduleTable, int userId){
	PreparedStatement pstmt = null;
    Connection conn = null;
    String mainSql="";
    int operationFlag=0;
    if(moduleTable.equals("er_nwusers")){
    	mainSql="update  er_nwusers set NWU_STATUS=?, LAST_MODIFIED_BY=? where PK_NWUSERS=?";
    }
    else{
    	mainSql="update  ER_NWUSERS_ADDNLROLES set NWU_ADTSTATUS=?, LAST_MODIFIED_BY=? where PK_NWUSERS_ADDROLES=?";
    }
    
    try{
    	conn = getConnection();
    	pstmt = conn
                .prepareStatement(mainSql);
    	
    	pstmt.setInt(1, status);
    	pstmt.setInt(2, userId);
    	pstmt.setInt(3, nwUserPk);
    	operationFlag=pstmt.executeUpdate();
    }catch(SQLException e){
    	Rlog.fatal("Network","EXCEPTION in updateUserNetworkUserStatus of NetworkDao class " + e);
    } finally {
		try {
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	return operationFlag;
	
	
}

 
 public void getNetworkAdditonalRoles(int networkuser){
	  int rows = 0;
		PreparedStatement pstmt = null;
	    Connection conn = null;
	    ResultSet rs = null;
	    String mainSql="";
		mainSql = "select PK_NWUSERS_ADDROLES,PK_STATUS,NVL(NWU_MEMBERADDLTROLE,0)NWU_MEMBERADDLTROLE,NWU_ADTSTATUS, F_CODELST_DESC(NWU_MEMBERADDLTROLE) as rolesdesc ,F_CODELST_DESC(NWU_ADTSTATUS) as role_status FROM ER_NWUSERS_ADDNLROLES,ER_STATUS_HISTORY sth WHERE FK_NWUSERS = ? AND sth.status_modtable = 'er_nwusers_addnlroles' and sth.STATUS_MODPK = PK_NWUSERS_ADDROLES and sth.STATUS_ISCURRENT = 1 ";
	   
	    try{
	    	conn = getConnection();
	    	pstmt = conn.prepareStatement(mainSql);
	    	pstmt.setInt(1, networkuser);
	    	rs = pstmt.executeQuery();
	    	while (rs.next()) {
	    		setNetworkAdlPkList(rs.getInt("PK_NWUSERS_ADDROLES"));
	    		setNetworkAdtstatus(rs.getInt("NWU_MEMBERADDLTROLE"));
	    		setNetworkAdtrole(rs.getInt("NWU_ADTSTATUS"));
	    		setNetworkADDTLRoleNameList(rs.getString("rolesdesc"));
	    		setNetworkADDTLRoleStatus(rs.getString("role_status"));
	    		setNetworkAdlPkHistory(rs.getInt("PK_STATUS"));
	    		rows++;
	    	}
	    	setcRowsaddl(rows);
	    }catch(SQLException e){
	    	e.printStackTrace();
	    	Rlog.fatal("Network","EXCEPTION in getNetworkAdditonalRoles of NetworkDao class " + e);	
	    } finally {
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
}
 
 
 public int  deleteUserNetworkaDAddRole(int nwUserPk){
	    PreparedStatement pstmt = null;
	    Connection conn = null;
	    String mainSql="";
	    int operationFlag=0;
	    try{
	    	mainSql="delete from ER_NWUSERS_ADDNLROLES where PK_NWUSERS_ADDROLES=?";
	    	conn = getConnection();
	    	pstmt = conn
	                .prepareStatement(mainSql);
	    	
	    	pstmt.setInt(1, nwUserPk);
	    	operationFlag=pstmt.executeUpdate();
	    }catch(SQLException e){
	    	Rlog.fatal("Network","EXCEPTION in deleteUserNetworkaDAddRole of NetworkDao class " + e);
	    } finally {
			try {
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return operationFlag;
		
		
	}
 
 public int  deleteStatusHistoryEnteries(int statusmodpk, String statusmodtable){
	    PreparedStatement pstmt = null;
	    Connection conn = null;
	    String mainSql="";
	    int operationFlag=0;
	    ResultSet rs = null;
	    int rid = 0;
	    
	    try{
	    	
	    	mainSql="select RID from er_status_history where status_modpk=? and status_modtable=?";
	    	conn = getConnection();
	    	pstmt = conn
	                .prepareStatement(mainSql);
	    	
	    	pstmt.setInt(1, statusmodpk);
	    	pstmt.setString(2, statusmodtable);
	    	rs=pstmt.executeQuery();
	    	while(rs.next()){
	    		rid = rs.getInt("RID");
	    	}
	    	
	    }catch(SQLException e){
	    	Rlog.fatal("Network","EXCEPTION in selecting rid of status history table in NetworkDao class " + e);
	    }finally {
			try {
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	    try{
	    	
	    	mainSql="delete from er_status_history where status_modpk=? and status_modtable=?";
	    	conn = getConnection();
	    	pstmt = conn
	                .prepareStatement(mainSql);
	    	
	    	pstmt.setInt(1, statusmodpk);
	    	pstmt.setString(2, statusmodtable);
	    	operationFlag=pstmt.executeUpdate();
	    }catch(SQLException e){
	    	Rlog.fatal("Network","EXCEPTION in deleteStatusHistoryEnteries of NetworkDao class " + e);
	    } finally {
			try {
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return rid;
		
		
	}
 
 
 public void  updateNetwrkSiteStatus(int nwsites_pk, int user){
		PreparedStatement pstmt = null;
	    Connection conn = null;
	    ResultSet rs=null;
	    StringBuffer mainsql=new StringBuffer();
	    int nwstatus=0;
	    String tableName="";
	    mainsql=mainsql.append("select FK_CODELST_STAT,STATUS_MODTABLE from ER_STATUS_HISTORY where STATUS_MODPK=? and STATUS_ISCURRENT='1' and STATUS_PARENTMODPK IS NULL");
	    try{
	    	conn=getConnection();
	    pstmt = conn.prepareStatement(mainsql.toString());
	    pstmt.setInt(1,nwsites_pk);
	    rs=pstmt.executeQuery();
	    if(rs.next()){
	    	nwstatus=rs.getInt("FK_CODELST_STAT");
	    	tableName=rs.getString("STATUS_MODTABLE");
	    }
	    
	    mainsql=new StringBuffer();
	    if(tableName.equals("er_nwsites")){
	    mainsql=mainsql.append("update ER_NWSITES SET NW_STATUS =?, LAST_MODIFIED_BY=? where pk_nwsites=? ");
	    }
	    else if(tableName.equals("er_nwusers")){
	    mainsql=mainsql.append("update ER_NWUSERS SET NWU_STATUS =?, LAST_MODIFIED_BY=? where PK_NWUSERS=? ");
	    }
	    else if(tableName.equals("er_nwusers_addnlroles")){
		    mainsql=mainsql.append("update er_nwusers_addnlroles SET NWU_ADTSTATUS =?, LAST_MODIFIED_BY=? where PK_NWUSERS_ADDROLES=? ");
		    }
	    pstmt = conn.prepareStatement(mainsql.toString());
	    pstmt.setInt(1, nwstatus);
	    pstmt.setInt(2, user);
	    pstmt.setInt(3, nwsites_pk);
	    pstmt.executeUpdate();
	    }catch(SQLException e){
	    	Rlog.fatal("Network","EXCEPTION in updateNetwrkSiteStatus of NetworkDao class " + e);
	    } finally {
			try {
				rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
 
 public int saveMultipleNetwork(String []siteIds,String level,String userId,String ip,String netTypId,String maninNtw,String ntw,String ntwStaus){
	 
	 System.out.println("find the methods of saveMultipleNetwork");
	  int ret = 0;
	  String chld_ids="";
	  Integer maninNtw1=StringUtil.stringToInteger(maninNtw);
	  Integer ntw1=StringUtil.stringToInteger(ntw);
      CallableStatement cstmt = null;
      Connection conn = null;
      try{

          conn = getConnection();
          ArrayDescriptor dSites = ArrayDescriptor.createDescriptor(
                  "ARRAY_STRING", conn);
          ARRAY sites = new ARRAY(dSites, conn, siteIds);
          cstmt = conn
                  .prepareCall("{call SP_INSERTMULNTW(?,?,?,?,?,?,?,?,?,?)}");
          cstmt.setArray(1, sites);
          cstmt.setInt(2, StringUtil.stringToInteger(level));
          cstmt.setInt(3, StringUtil.stringToInteger(userId));
          cstmt.setString(4, ip);
          cstmt.setInt(5, StringUtil.stringToInteger(netTypId));
          if(maninNtw1==null)
        	  cstmt.setNull(6, java.sql.Types.INTEGER);
          else
          cstmt.setInt(6, maninNtw1);
          
          if(ntw1==null)
        	  cstmt.setNull(7, java.sql.Types.INTEGER);
          else
          cstmt.setInt(7, ntw1);
          cstmt.setInt(8, StringUtil.stringToInteger(ntwStaus));
          cstmt.registerOutParameter(9, java.sql.Types.INTEGER);
          cstmt.registerOutParameter(10,java.sql.Types.VARCHAR);
          cstmt.execute();

          ret = cstmt.getInt(9);
          chld_ids=cstmt.getString(10);
          if(chld_ids!=null)
          {
        	  chld_ids=chld_ids.substring(0,chld_ids.length()-1);
              setChld_ntw_ids(chld_ids);
          }
          
          
      }catch(Exception ex){
    	  Rlog.fatal("lineitem",
                  "In insertMultiple network in saveMultipleNetwork line EXCEPTION IN calling the procedure"
                          + ex);
    	 ex.printStackTrace();
      }finally {
          try {
              if (cstmt != null)
                  cstmt.close();
          } catch (Exception e) {
          }
          try {
              if (conn != null)
                  conn.close();
          } catch (Exception e) {
          }
      }
      return ret;
 }
 
 public int getNetworkPKbyName(String networkName,int accountId){
	  PreparedStatement pstmt = null;
	    Connection conn = null;
	    ResultSet rs = null;
	    String mainSql="";
	    int pk_nwsites=0;
	    mainSql = "select pk_nwsites from er_nwsites where NW_LEVEL=0 and fk_site in (select pk_site from er_site where site_name=? and fk_account=?)" ;
		    try{
		    	conn = getConnection();
		    	pstmt = conn
		                .prepareStatement(mainSql);
		    	
		    	
		    	pstmt.setString(1, networkName);
		    	pstmt.setInt(2, accountId);
		    	 rs = pstmt.executeQuery();
		    	if(rs.next()){
		    		pk_nwsites = rs.getInt("pk_nwsites");	
		    	}
		    	
		    }catch(SQLException e){
		    	Rlog.fatal("Network","EXCEPTION in getNetworkPKbyName of NetworkDao class " + e);
		    } finally {
				try {
					pstmt.close();
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
	  
	  
	  return pk_nwsites;
	 
}
}
