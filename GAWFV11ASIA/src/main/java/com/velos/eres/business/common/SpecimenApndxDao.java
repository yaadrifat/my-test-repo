package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.Rlog;

public class SpecimenApndxDao extends CommonDAO implements java.io.Serializable {
	
    private ArrayList specApndxIds;

    private ArrayList fkSpecimenIds;

    private ArrayList specApndxDescs;

    private ArrayList specApndxUris;

    private ArrayList specApndxTypes;

    private int cRows;

    public SpecimenApndxDao() {

    	specApndxIds   = new ArrayList();
        fkSpecimenIds  = new ArrayList();
        specApndxDescs = new ArrayList();
        specApndxUris  = new ArrayList();
        specApndxTypes = new ArrayList();
       
    }

 

    public void getSpecimenApndxUris(int fkSpec) {
        Connection conn = null;
        String sqlString = null;
        PreparedStatement pstmt = null;

        try {

            conn = getConnection();
            sqlString = "select PK_SPECIMEN_APPNDX,"
                    + "FK_SPECIMEN,"
                    + "SA_DESC,"
                    + "SA_URI,"
                    + "SA_TYPE"
                    + " from er_specimen_appndx where FK_SPECIMEN = ? ";
            Rlog.debug("specapndx", "sql is" + sqlString);

            pstmt = conn.prepareStatement(sqlString);
            Rlog.debug("specapndx", "after prepareStatement");
            pstmt.setInt(1,fkSpec);
            Rlog.debug("specapndx", "after setting the study");

            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("specapndx", "after execute" + rs);

            while (rs.next()) {
                Rlog.debug("specapndx", "inside loop");
               
                setSpecApndxIds(new Integer(rs.getInt("PK_SPECIMEN_APPNDX")));
                setFkSpecimen(new Integer(rs.getInt("FK_SPECIMEN")));
            	setSpecApndxDescs(rs.getString("SA_DESC"));
            	setSpecApndxUris(rs.getString("SA_URI"));
            	setSpecApndxTypes(rs.getString("SA_TYPE"));
            	

            }
        } catch (SQLException e) {
            Rlog.fatal("specapndx",
                    "error in database fetch operation in getSpecimenApndxUris");
        } catch (Exception e) {
            Rlog.fatal("specapndx", "some error in getSpecimenApndxUris");

        } finally {
            try {
                pstmt.close();
            } catch (SQLException e) {
                Rlog.debug("specapndx",
                        "Could not close the prepared statement");
            }
            
            try {
                conn.close();
            } catch (SQLException e) {
                Rlog.debug("specapndx",
                        "Could not close the connection");
            }
           }

    }
    
    
    public int  updateSpecimenapndx(int userid, int specApndxId){
		String idWithName=null;
		
		 PreparedStatement pstmt = null;
		 int rowImpact=0;
		 Connection conn = null;
		
		
		 try{
			
			 conn = getConnection();
			 pstmt=conn.prepareStatement("UPDATE  ERES.ER_SPECIMEN_APPNDX SET LAST_MODIFIED_BY=? WHERE PK_SPECIMEN_APPNDX=?") ;
			 pstmt.setInt(1, userid);
			 pstmt.setInt(2, specApndxId);
			 rowImpact=pstmt.executeUpdate();
		 }catch(Exception ex){
			 Rlog.fatal("SpecimenApndxDao",
	                    "Error in Update LAST_MODIFIED_BY before delete Specimen Appendix "
	                            + ex); 
		 }
		 finally{
		   		try
		   		{
		   			//if (cstmt!= null) cstmt.close();
		   		}
		   		catch (Exception e) 
		   		{
		   		}
		   		try 
		   		{
		   			if (conn!=null) conn.close();
		   		}
		   		catch (Exception e) {}
		     
		     }
		 return rowImpact;
	}

    
   

   
	public ArrayList getFkSpecimen() {
		return fkSpecimenIds;
	}



	public void setFkSpecimen(ArrayList fkSpecimenIds) {
		this.fkSpecimenIds = fkSpecimenIds;
	}
   
	public void setFkSpecimen(Integer fkSpecimen) {
		fkSpecimenIds.add(fkSpecimen); 
		
	}


	public ArrayList getSpecApndxDescs() {
		return specApndxDescs;
	}



	public void setSpecApndxDescs(ArrayList specApndxDescs) {
		this.specApndxDescs = specApndxDescs;
	}
	public void setSpecApndxDescs(String specApndxDesc) {
		this.specApndxDescs.add(specApndxDesc); 
	}


	public ArrayList getSpecApndxIds() {
		return specApndxIds;
	}



	public void setSpecApndxIds(ArrayList specApndxIds) {
		this.specApndxIds = specApndxIds;
	}
	
	public void setSpecApndxIds(Integer specApndxId) {
		this.specApndxIds.add(specApndxId);
	}



	public ArrayList getSpecApndxTypes() {
		return specApndxTypes;
	}



	public void setSpecApndxTypes(ArrayList specApndxTypes) {
		this.specApndxTypes = specApndxTypes;
	}
	public void setSpecApndxTypes(String specApndxType) {
		this.specApndxTypes.add(specApndxType);
	}


	public ArrayList getSpecApndxUris() {
		return specApndxUris;
	}
    


	public void setSpecApndxUris(ArrayList specApndxUris) {
		this.specApndxUris = specApndxUris;
	}
	public void setSpecApndxUris(String specApndxUri) {
		this.specApndxUris.add(specApndxUri);
	}

}
