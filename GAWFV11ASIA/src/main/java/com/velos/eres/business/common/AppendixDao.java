package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

public class AppendixDao extends CommonDAO implements java.io.Serializable {
    private ArrayList appendixids;

    private ArrayList appendixstudyids;

    private ArrayList appendixStudyVerIds;

    private ArrayList appendixfile_uris;

    private ArrayList appendixdescriptions;

    private ArrayList appendixpubflags;

    private ArrayList appendixfiles;

    private ArrayList appendixtypes;

    private int cRows;

    public AppendixDao() {
        appendixids = new ArrayList();
        appendixstudyids = new ArrayList();
        appendixStudyVerIds = new ArrayList();
        appendixfile_uris = new ArrayList();
        appendixdescriptions = new ArrayList();
        appendixpubflags = new ArrayList();
        appendixfiles = new ArrayList();
        appendixtypes = new ArrayList();
    }

    // Getter and Setter methods
    public ArrayList getAppendixIds() {
        return this.appendixids;
    }

    public void setAppendixIds(ArrayList appendixids) {
        this.appendixids = appendixids;
    }

    public ArrayList getAppendixStudyIds() {
        return this.appendixstudyids;
    }

    public void setAppendixStudyIds(ArrayList appendixstudyids) {
        this.appendixstudyids = appendixstudyids;
    }

    public ArrayList getAppendixStudyVers() {
        return this.appendixStudyVerIds;
    }

    public void setAppendixStudyVers(ArrayList appendixStudyVerIds) {
        this.appendixStudyVerIds = appendixStudyVerIds;
    }

    public ArrayList getAppendixTypes() {
        return this.appendixtypes;
    }

    public void setAppendixTypes(ArrayList appendixtypes) {
        this.appendixtypes = appendixtypes;
    }

    public ArrayList getAppendixDescriptions() {
        return this.appendixdescriptions;
    }

    public void setAppendixDescriptions(ArrayList appendixdescriptions) {
        this.appendixdescriptions = appendixdescriptions;
    }

    public ArrayList getAppendixFile_Uris() {
        return this.appendixfile_uris;
    }

    public void setAppendixFile_Uris(ArrayList appendixfile_uris) {
        this.appendixfile_uris = appendixfile_uris;
    }

    public ArrayList getAppendixFiles() {
        return this.appendixfiles;
    }

    public void setAppendixFiles(ArrayList appendixfiles) {
        this.appendixfiles = appendixfiles;
    }

    public ArrayList getAppendixPubFlags() {
        return this.appendixpubflags;
    }

    public void setAppendixPubFlags(ArrayList appendixpubflags) {
        this.appendixpubflags = appendixpubflags;
    }

    public int getCRows() {
        return this.cRows;
    }

    public void setCRows(int cRows) {
        this.cRows = cRows;
    }

    public void setAppendixIds(Integer appendixid) {
        appendixids.add(appendixid);
    }

    public void setAppendixStudyIds(String appendixstudyid) {
        appendixstudyids.add(appendixstudyid);
    }

    public void setAppendixStudyVerIds(String appendixStudyVerId) {
        appendixStudyVerIds.add(appendixStudyVerId);
    }

    public void setAppendixTypes(String appendixtype) {
        appendixtypes.add(appendixtype);
    }

    public void setAppendixPubFlags(String appendixpubflag) {
        appendixpubflags.add(appendixpubflag);
    }

    public void setAppendixFile_Uris(String appendixfile_uri) {
        appendixfile_uris.add(appendixfile_uri);
    }

    public void setAppendixFiles(String appendixfile) {
        appendixfiles.add(appendixfile);
    }

    public void setAppendixDescriptions(String appendixdescription) {
        appendixdescriptions.add(appendixdescription);
    }

    // end of getter and setter methods

    public void getAppendixValues(int studyVerId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            pstmt = conn
                    .prepareStatement("Select pk_studyapndx, fk_study, fk_studyver, studyapndx_uri, studyapndx_desc, studyapndx_pubflag, studyapndx_file, studyapndx_type from er_studyapndx where fk_studyver = ? order by studyapndx_type");

            pstmt.setInt(1, studyVerId);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                Rlog.debug("appendix", "AppendixDao.id "
                        + rs.getInt("pk_studyapndx"));
                setAppendixIds(new Integer(rs.getInt("pk_studyapndx")));
                setAppendixStudyIds(rs.getString("fk_study"));
                setAppendixStudyVerIds(rs.getString("fk_studyver"));
                setAppendixFile_Uris(rs.getString("studyapndx_uri"));
                setAppendixDescriptions(rs.getString("studyapndx_desc"));
                setAppendixPubFlags(rs.getString("studyapndx_pubflag"));
                setAppendixFiles(rs.getString("studyapndx_file"));
                setAppendixTypes(rs.getString("studyapndx_type"));

                rows++;
                Rlog.debug("appendix", "AppendixDao.getAppendixValues row# "
                        + rows);

            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("Appendix",
                    "AppendixDao.getAppendixValues EXCEPTION IN FETCHING FROM Appendix table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    // ////

    public void getAppendixValuesPublic(int studyVerId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement("Select pk_studyapndx, "
                    + "studyapndx_uri, " + "studyapndx_desc, "
                    + "studyapndx_file, " + "studyapndx_type "
                    + "from er_studyapndx " + "where fk_studyver= ?  "
                    + "and studyapndx_pubflag = 'Y' "
                    + "order by studyapndx_type");

            pstmt.setInt(1, studyVerId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                Rlog.debug("appendix", "AppendixDao.id "
                        + rs.getInt("pk_studyapndx"));
                setAppendixIds(new Integer(rs.getInt("pk_studyapndx")));
                setAppendixFile_Uris(rs.getString("studyapndx_uri"));
                setAppendixDescriptions(rs.getString("studyapndx_desc"));
                setAppendixFiles(rs.getString("studyapndx_file"));
                setAppendixTypes(rs.getString("studyapndx_type"));

                rows++;
                Rlog.debug("appendix",
                        "AppendixDao.getAppendixValuesPublic row# " + rows);

            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("Appendix",
                    "AppendixDao.getAppendixValuesPublic EXCEPTION IN FETCHING FROM Appendix table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public void getAppendixValues(int studyVerId, String type) {
        Rlog.debug("appendix", "Value of studyVerid " + studyVerId);
        Rlog.debug("appendix", "Value of type " + type);
        // String test = "Select pk_studyapndx, fk_study, studyapndx_uri,
        // studyapndx_desc, studyapndx_pubflag, studyapndx_file, studyapndx_type
        // from er_studyapndx where fk_study="+studyid +" and studyapndx_type='"
        // +type+"'";
        // Rlog.debug("appendix", "Value of test " + test);

        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            //JM: 09Jul2008, order by added, #3462
            pstmt = conn
                    .prepareStatement("Select pk_studyapndx, fk_study, fk_studyver,studyapndx_uri, studyapndx_desc, studyapndx_pubflag, studyapndx_file, studyapndx_type from er_studyapndx where fk_studyver= ? and studyapndx_type= ?  order by trim(lower(studyapndx_uri)) asc");
            Rlog.debug("appendix", pstmt.toString());
            pstmt.setInt(1, studyVerId);
            pstmt.setString(2, type);

            ResultSet rs = pstmt.executeQuery();

            Rlog.debug("appendix", "executed");
            while (rs.next()) {
                Rlog.debug("appendix", "AppendixDao.id "
                        + rs.getInt("pk_studyapndx"));
                setAppendixIds(new Integer(rs.getInt("pk_studyapndx")));
                setAppendixStudyIds(rs.getString("fk_study"));
                setAppendixStudyVerIds(rs.getString("fk_studyver"));
                setAppendixFile_Uris(rs.getString("studyapndx_uri"));
                setAppendixDescriptions(rs.getString("studyapndx_desc"));
                setAppendixPubFlags(rs.getString("studyapndx_pubflag"));
                setAppendixFiles(rs.getString("studyapndx_file"));
                setAppendixTypes(rs.getString("studyapndx_type"));

                rows++;
                Rlog.debug("appendix", "AppendixDao.getAppendixValues row# "
                        + rows);

            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("Appendix",
                    "AppendixDao.getAppendixValues EXCEPTION IN FETCHING FROM Appendix table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /** gets latest document for version category */
    public void getLatestDocumentForCategory(String studyId,String versionCategory) {
 
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer versionSql = new StringBuffer();
        
        versionSql.append("select m.pk_studyapndx,o.studyapndx_uri,o.fk_studyver,o.studyapndx_desc,o.studyapndx_file,o.studyapndx_pubflag ");
        versionSql.append(" from ( Select max(pk_studyapndx) pk_studyapndx");
        
        versionSql.append(" from er_studyapndx x,er_studyver v where v.fk_study = ? and v.studyver_category = ? and ");
        versionSql.append(" fk_studyver= pk_studyver  and studyapndx_type='file' ) m, er_studyapndx o where m.pk_studyapndx = o.pk_studyapndx ");
        
        try {
            conn = getConnection();
            
            pstmt = conn
                    .prepareStatement(versionSql.toString());

            pstmt.setString(1, studyId);
            pstmt.setString(2, versionCategory);

            ResultSet rs = pstmt.executeQuery();

             
            while (rs.next()) {
                setAppendixIds(new Integer(rs.getInt("pk_studyapndx")));
                setAppendixStudyIds(studyId);
                setAppendixStudyVerIds(rs.getString("fk_studyver"));
                setAppendixFile_Uris(rs.getString("studyapndx_uri"));
                setAppendixDescriptions(rs.getString("studyapndx_desc"));
                setAppendixPubFlags(rs.getString("studyapndx_pubflag"));
                setAppendixFiles(rs.getString("studyapndx_file"));
                setAppendixTypes("file");

                rows++;
                 
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("Appendix",
                    "AppendixDao.getLatestDocumentForCategory EXCEPTION IN FETCHING FROM Appendix table"
                            + ex);
            ex.printStackTrace();
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    public void getLatestDocumentForCategory(String studyId,String versionCategory,String submissionPK) {
    	 
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer versionSql = new StringBuffer();
        
        versionSql.append("select m.pk_studyapndx,o.studyapndx_uri,o.fk_studyver,o.studyapndx_desc,o.studyapndx_file,o.studyapndx_pubflag ");
        versionSql.append(" from ( (SELECT pk_studyapndx ,v.fk_study, v.studyver_majorver,v.studyver_minorver  from er_studyapndx x,");
        versionSql.append(" er_studyver v   WHERE  fk_studyver = pk_studyver   AND studyapndx_type ='file' AND FK_STUDYVER =(select max(PK_STUDYVER) from er_studyver v,UI_FLX_PAGEVER pg where v.fk_study = ? ");
        versionSql.append(" and  studyver_category=  ? and  pg.page_mod_pk=v.fk_study  and pg.fk_submission=? and v.studyver_majorver= pg.PAGE_VER  and v.studyver_minorver= pg.PAGE_MINOR_VER  ))) m,");
        versionSql.append (" er_studyapndx o where m.pk_studyapndx = o.pk_studyapndx ");
      
        try {
            conn = getConnection();
            
            pstmt = conn
                    .prepareStatement(versionSql.toString());

            pstmt.setString(1, studyId);
            pstmt.setString(2, versionCategory);
            pstmt.setInt(3, EJBUtil.stringToNum(submissionPK));

            ResultSet rs = pstmt.executeQuery();

             
            while (rs.next()) {
                setAppendixIds(new Integer(rs.getInt("pk_studyapndx")));
                setAppendixStudyIds(studyId);
                setAppendixStudyVerIds(rs.getString("fk_studyver"));
                setAppendixFile_Uris(rs.getString("studyapndx_uri"));
                setAppendixDescriptions(rs.getString("studyapndx_desc"));
                setAppendixPubFlags(rs.getString("studyapndx_pubflag"));
                setAppendixFiles(rs.getString("studyapndx_file"));
                setAppendixTypes("file");

                rows++;
                 
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("Appendix",
                    "AppendixDao.getLatestDocumentForCategory EXCEPTION IN FETCHING FROM Appendix table"
                            + ex);
            ex.printStackTrace();
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    public void getLatestDocumentForSubmission(String studyId,String versionCategory,String submissionDate,String approvalDate) {
   	 
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer versionSql = new StringBuffer();
        
        versionSql.append("select pk_studyapndx,studyapndx_uri,fk_studyver,studyapndx_desc,studyapndx_file,studyapndx_pubflag ");
        versionSql.append("from er_studyapndx where pk_studyapndx=(select max(pk_studyapndx) from er_studyapndx where fk_study="+studyId+" and  ");
        versionSql.append("fk_studyver in (select pk_studyver from er_studyver where studyver_category="+versionCategory+") and created_on ");
        if(!submissionDate.equals("") && !approvalDate.equals("")){
        	versionSql.append(" BETWEEN TO_DATE('"+submissionDate+"', PKG_DATEUTIL.F_GET_DATETIMEFORMAT)");
        	versionSql.append(" AND TO_DATE('"+approvalDate+"', PKG_DATEUTIL.F_GET_DATETIMEFORMAT))");
		}else if(!submissionDate.equals("")){
			versionSql.append(" >= TO_DATE('"+submissionDate+"', PKG_DATEUTIL.F_GET_DATETIMEFORMAT))");
		}
      
        try {
            conn = getConnection();
            
            pstmt = conn
                    .prepareStatement(versionSql.toString());

            

            ResultSet rs = pstmt.executeQuery();

             
            while (rs.next()) {
                setAppendixIds(new Integer(rs.getInt("pk_studyapndx")));
                setAppendixStudyIds(studyId);
                setAppendixStudyVerIds(rs.getString("fk_studyver"));
                setAppendixFile_Uris(rs.getString("studyapndx_uri"));
                setAppendixDescriptions(rs.getString("studyapndx_desc"));
                setAppendixPubFlags(rs.getString("studyapndx_pubflag"));
                setAppendixFiles(rs.getString("studyapndx_file"));
                setAppendixTypes("file");

                rows++;
                 
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("Appendix",
                    "AppendixDao.getLatestDocumentForCategory EXCEPTION IN FETCHING FROM Appendix table"
                            + ex);
            ex.printStackTrace();
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    private static final String checkApndxDataSql = 
    		" select PK_STUDYAPNDX, STUDYAPNDX_URI, a.FK_STUDY, FK_STUDYVER, STUDYVER_CATEGORY, nvl(length(STUDYAPNDX_FILEOBJ), 0) LEN "+
    		" from ER_STUDYAPNDX a, ER_STUDYVER v where a.FK_STUDYVER=v.PK_STUDYVER and a.PK_STUDYAPNDX = ? ";
    public HashMap<String, Object> checkApndxData(int apndxId) {
    	HashMap<String, Object> retMap = new HashMap<String, Object>();
    	PreparedStatement pstmt = null;
        Connection conn = null;
        ResultSet rs = null;
        try {
            conn = getConnection();     
            pstmt = conn.prepareStatement(checkApndxDataSql);
            pstmt.setInt(1, apndxId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
            	retMap.put("len", rs.getInt("LEN"));
            	retMap.put("filename", rs.getString("STUDYAPNDX_URI"));
            	retMap.put("fkStudy", rs.getInt("FK_STUDY"));
            	retMap.put("category", rs.getInt("STUDYVER_CATEGORY"));
            }
        } catch(Exception e) {
        	Rlog.fatal("appendix", "Exception in AppendixDao.checkApndxData:"+e);
        } finally {
        	try { if (rs != null) rs.close(); } catch (Exception e) {}    	
        	try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}     
        	try { if (conn != null) conn.close(); } catch (Exception e) {}
        }
		return retMap;
	}
    
    private static final String fetchClosestApndxIds =
    	" select * from (select PK_STUDYAPNDX, STUDYAPNDX_URI, a.FK_STUDY, FK_STUDYVER, STUDYVER_CATEGORY, "+
    	" nvl(length(STUDYAPNDX_FILEOBJ), 0) LEN from ER_STUDYAPNDX a, ER_STUDYVER v where "+
    	" a.FK_STUDYVER=v.PK_STUDYVER and a.STUDYAPNDX_URI=? and a.FK_STUDY=? and STUDYVER_CATEGORY=?) "+
    	" where LEN > 0 order by PK_STUDYAPNDX desc ";
    public ArrayList<Integer> fetchClosestApndxIds(String filename, int fkStudy, int category) {
    	ArrayList<Integer> apndxIds = new ArrayList<Integer>();
    	PreparedStatement pstmt = null;
        Connection conn = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(fetchClosestApndxIds);
            pstmt.setString(1, filename);
            pstmt.setInt(2, fkStudy);
            pstmt.setInt(3, category);
            rs = pstmt.executeQuery();
            while (rs.next()) {
            	apndxIds.add(rs.getInt("PK_STUDYAPNDX"));
            }
        } catch(Exception e) {
        	Rlog.fatal("appendix", "Exception in AppendixDao.fetchClosestApndxIds:"+e);
        } finally {
        	try { if (rs != null) rs.close(); } catch (Exception e) {}   	
        	try { if (pstmt != null) pstmt.close(); } catch (Exception e) {} 
        	try { if (conn != null) conn.close(); } catch (Exception e) {}
        }
        return apndxIds;
    }
    
    private static final String copyAppendixSql =
    	" update er_studyapndx set STUDYAPNDX_FILEOBJ = (select STUDYAPNDX_FILEOBJ from er_studyapndx where PK_STUDYAPNDX = ?) "+
    	" where PK_STUDYAPNDX = ? ";
    public int copyAppendix(int fromApndxId, int toApndxId) {
    	PreparedStatement pstmt = null;
        Connection conn = null;
        int result = 0;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(copyAppendixSql);
            pstmt.setInt(1, fromApndxId);
            pstmt.setInt(2, toApndxId);
            result = pstmt.executeUpdate();      
        } catch(Exception e) {
        	Rlog.fatal("appendix", "Exception in AppendixDao.copyAppendix:"+e);
        	result = -1;
        } finally {
        	try { if (pstmt != null) pstmt.close(); } catch (Exception e) {} 
        	try { if (conn != null) conn.close(); } catch (Exception e) {}
        }
    	return result;
    }
}
