/*
 * Classname : StudyViewFile
 * 
 * Version information: 1.0
 *
 * Copyright notice: Velos, Inc
 * date: 08/11/2001
 *
 * Author: sonia sahni
 *
 */

package com.velos.eres.business.common;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import com.velos.eres.service.util.Rlog;

/* import statements */

/**
 * 
 * 
 * @author Sonia Sahni
 * @vesion 1.0
 */

public class StudyViewFile extends CommonDAO implements java.io.Serializable {

    // End of Getter Setter methods

    /**
     * Default Constructor
     * 
     * 
     */

    public StudyViewFile() {
    }

    public boolean saveFile(String filPath, int studyViewPK) {

    	Connection conn = null;
    	  PreparedStatement pstmt = null;
    	  File blob = null;
    	  FileInputStream in = null;
    	try {
    		conn = getConnection();
            blob = new File(filPath);
            in = new FileInputStream(blob);
            
            pstmt = conn.prepareStatement("update ER_STUDYVIEW set STUDYVIEW = ? where PK_STUDYVIEW = ? ");

            pstmt.setBinaryStream(1, in, (int)blob.length()); 
            pstmt.setInt(2, studyViewPK);
            pstmt.executeUpdate();
           
            blob.delete();
            return true;
        } catch (Exception ex) {
            Rlog.fatal("studyview", "EXCEPTION IN SAVING FILE" + ex);
            return false;
        } finally {
        	try {
				in.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
             try{
            	 if (pstmt != null) pstmt.close();
             }catch (Exception e) {e.printStackTrace(); }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {e.printStackTrace();
            }
        }
    }

    public String readFile(String path, int studyViewPK) {
        
        PreparedStatement pstmt = null;
        Connection conn = null;
        InputStream in = null;
        ByteArrayOutputStream out = null;
        
        try {
            conn = getConnection();
            pstmt = conn
                    .prepareStatement("Select STUDYVIEW from ER_STUDYVIEW where PK_STUDYVIEW = ? ");

            pstmt.setInt(1, studyViewPK);
            ResultSet rs = pstmt.executeQuery();
            
            Blob lob = null;
            while (rs.next()) {
                lob=rs.getBlob("STUDYVIEW");    
            }
            
            in = lob.getBinaryStream();
            out = new ByteArrayOutputStream();
                       
            int bufferSize = 1024;
            int length = (int) lob.length();

            byte[] buffer = new byte[bufferSize];
            while((length = in.read(buffer)) != -1)
            {
            	out.write(buffer,0,length);
            }
            String s = out.toString();
            return s;
        } catch (Exception ex) {
            return null;
        } finally {
        	try {
        		out.close();
				in.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
           
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {e.printStackTrace();
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {e.printStackTrace();
            }
        }
    }

}