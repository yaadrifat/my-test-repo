/*
 * Classname			StudySiteApndxBean.class
 * 
 * Version information
 *
 * Date					10/29/2004
 * 
 * Copyright notice Aithent
 */

package com.velos.eres.business.studySiteApndx.impl;

/**
 * 
 */
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

@Entity
@Table(name = "ER_STUDYSITES_APNDX")
public class StudySiteApndxBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5382879388712615327L;

    /**
     * the StudySiteApndx Id
     */
    public int studySiteApndxId;

    /**
     * the StudySite Id
     */
    public Integer studySiteId;

    /**
     * appendix type
     */
    public String type;

    /**
     * the url/file name
     */
    public String name;

    /**
     * appendix description
     */

    public String desc;

    public Integer fileSize;

    /**
     * the record creator
     */
    public Integer creator;

	/**
	 * Created On
	 */
	public Date createdOn;
    
    /**
     * last modified by
     */
    public Integer modifiedBy;

    /**
     * the IP Address
     */
    public String ipAdd;

    // GETTER SETTER METHODS
    /**
     * @return study site apndx id
     */

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_STUDYSITES_APNDX", allocationSize=1)
    @Column(name = "PK_STUDYSITES_APNDX")
    public int getStudySiteApndxId() {
        return this.studySiteApndxId;
    }

    public void setStudySiteApndxId(int id) {
        studySiteApndxId = id;
    }

    /**
     * @return study site id
     */
    @Column(name = "FK_STUDYSITES")
    public String getStudySiteId() {
        return StringUtil.integerToString(this.studySiteId);
    }

    /**
     * @param studySiteId
     *            study site id
     */
    public void setStudySiteId(String studySiteId) {
        if (studySiteId != null)
            this.studySiteId = Integer.valueOf(studySiteId);
    }

    /**
     * @return apndx type
     * 
     */
    @Column(name = "APNDX_TYPE")
    public String getAppendixType() {
        return this.type;
    }

    /**
     * @param type
     *            study site apndx type
     */
    public void setAppendixType(String type) {
        this.type = type;
    }

    /**
     * @return apndx name
     */
    @Column(name = "APNDX_NAME")
    public String getAppendixName() {
        return this.name;
    }

    /**
     * @param name
     *            study site apndx name
     */
    public void setAppendixName(String name) {
        this.name = name;
    }

    /**
     * @return apndx description
     */
    @Column(name = "APNDX_DESCRIPTION")
    public String getAppendixDescription() {
        return this.desc;
    }

    /**
     * @param desc
     *            study site apndx desc
     */
    public void setAppendixDescription(String desc) {
        this.desc = desc;
    }

    /**
     * @return file size
     */
    @Column(name = "APNDX_FILESIZE")
    public String getFileSize() {
        return StringUtil.integerToString(this.fileSize);
    }

    /**
     * @param file
     *            size study site apndx file size
     */
    public void setFileSize(String fileSize) {
        if (fileSize != null) {
            this.fileSize = Integer.valueOf(fileSize);
        }
    }

    /**
     * @return The Id of the creator of the record
     */
    @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

     /**
     * @return Date on which Creator has created the record
     */
    @Column(name = "CREATED_ON")
	public Date getCreatedOn() {
		return createdOn;
	}

     /**
     * @param Created On
     *            The Date on which Creator has created the record
     */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
    
    /**
     * @return The Id of the user modifying this record
     */
    @Column(name = "LAST_MODIFIED_BY")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    /**
     * @return IP Address
     */
    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // END OF GETTER SETTER METHODS

    public int updateStudySiteApndx(StudySiteApndxBean ask) {
        try {
            setStudySiteId(ask.getStudySiteId());
            setAppendixType(ask.getAppendixType());
            setAppendixName(ask.getAppendixName());
            setAppendixDescription(ask.getAppendixDescription());
            setFileSize(ask.getFileSize());
            setCreator(ask.getCreator());
            setModifiedBy(ask.getModifiedBy());
            setIpAdd(ask.getIpAdd());
            setCreatedOn(ask.getCreatedOn());
            Rlog.debug("studySiteApndx",
                    "StudySiteApndxBean.updateStudySiteApndx");
            return 0;
        } catch (Exception e) {
            Rlog.fatal("studySiteApndx",
                    " error in StudySiteApndxBean.updateStudySiteApndx");
            return -2;
        }
    }

    public StudySiteApndxBean() {
    }

    public StudySiteApndxBean(int studySiteApndxId, String studySiteId,
            String type, String name, String desc, String fileSize,
            String creator, String modifiedBy, String ipAdd,String createdOn) {
        // TODO Auto-generated constructor stub
    	createdOn = DateUtil.getCurrentDate();
        setStudySiteApndxId(studySiteApndxId);
        setStudySiteId(studySiteId);
        this.setAppendixType(type);
        this.setAppendixName(name);
        this.setAppendixDescription(desc);
        this.setFileSize(fileSize);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        this.setCreatedOn(DateUtil.stringToDate(createdOn));
    }

}// end of class
