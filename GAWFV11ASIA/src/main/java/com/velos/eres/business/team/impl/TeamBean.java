/* 
 * Classname			TeamBean.class
 * 
 * Version information
 *
 * Date					02/26/2001
 * 
 * Copyright notice
 */

package com.velos.eres.business.team.impl;

/**
 * @ejbHomeJNDIname ejb.Team
 */

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * The Team CMP entity bean.<br>
 * <br>
 * 
 * @author
 */
@Entity
@Table(name = "er_studyteam")
public class TeamBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3761122760600400951L;

    /**
     * the primary key of the study Team
     */
    public Integer id;

    /**
     * the Study ID
     */
    public Integer studyId;

    /**
     * the Study Team User
     */
    public Integer teamUser;

    /**
     * the Team user Role
     */
    public Integer teamUserRole;

    /**
     * the record creator
     */
    public Integer creator;

    /**
     * last modified by
     */
    public Integer modifiedBy;

    /**
     * the IP Address
     */
    public String ipAdd;

    /**
     * teamUserType
     */
    public String teamUserType;

    /**
     * siteFlag
     */
    public String siteFlag;
    
    /**
     * teamStatus
     */
    public String teamStatus;

    // GETTER SETTER METHODS
    public TeamBean() {

    }

    public TeamBean(Integer id, String studyId, String teamUser,
            String teamUserRole, String creator, String modifiedBy,
            String ipAdd, String teamUserType, String siteFlag, String teamStatus) {
        super();

        setId(id);
        setStudyId(studyId);
        setTeamUser(teamUser);
        setTeamUserRole(teamUserRole);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setTeamUserType(teamUserType);
        setSiteFlag(siteFlag);
        setTeamStatus(teamStatus);
    }

    /**
     * 
     * 
     * @return
     */
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_STUDYTEAM", allocationSize=1)
    @Column(name = "PK_STUDYTEAM")
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    // NO SETTER METHOD FOR PK REQUIRED

    /**
     * 
     * 
     * @return
     */
    @Column(name = "FK_STUDY")
    public String getStudyId() {
        return StringUtil.integerToString(this.studyId);

    }

    /**
     * 
     * 
     * @param studyId
     */
    public void setStudyId(String studyId) {

        if (studyId != null) {
            this.studyId = Integer.valueOf(studyId);
        }

    }

    /**
     * 
     * 
     * @return
     */
    @Column(name = "FK_USER")
    public String getTeamUser() {
        return StringUtil.integerToString(this.teamUser);
    }

    /**
     * 
     * 
     * @param user
     */
    public void setTeamUser(String user) {
        if (user != null) {
            this.teamUser = Integer.valueOf(user);
        }

    }

    /**
     * 
     * 
     * @return
     */
    @Column(name = "FK_CODELST_TMROLE")
    public String getTeamUserRole() {
        return StringUtil.integerToString(this.teamUserRole);

    }

    /**
     * 
     * 
     * @param role
     */
    public void setTeamUserRole(String role) {
        if (role != null) {
            this.teamUserRole = Integer.valueOf(role);
        }
    }

    /**
     * @return The Id of the creator of the record
     */
    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    /**
     * @return The Id of the user modifying this record
     */
    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    /**
     * @return IP Address
     */
    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
     * @return teamUserType
     */
    @Column(name = "STUDY_TEAM_USR_TYPE")
    public String getTeamUserType() {
        return this.teamUserType;
    }

    /**
     * @param teamUserType
     *            Team user type
     */
    public void setTeamUserType(String teamUserType) {
        this.teamUserType = teamUserType;
    }

    /**
     * @return siteFlag
     */
    @Column(name = "STUDY_SITEFLAG")
    public String getSiteFlag() {
        return this.siteFlag;
    }

    /**
     * @param study
     *            site flag
     */
    public void setSiteFlag(String siteFlag) {
        this.siteFlag = siteFlag;
    }

    
    //Added by Manimaran for the July-August Enhancement S4.
    /**
     * @return study team status
     */
    @Column(name = "STUDYTEAM_STATUS")
    public String getTeamStatus() {
        return this.teamStatus;
    }

    /**
     * @param study
     *            teamStatus
     */
    public void setTeamStatus(String teamStatus) {
        this.teamStatus = teamStatus;
    }
    // END OF GETTER SETTER METHODS

    /**
     * returns the state holder object associated with this study team
     * 
     * @return
     */
    // @Transient
    /*
     * public TeamStateKeeper getTeamStateKeeper() { Rlog.debug("team", "GET
     * STUDY TEAM KEEPER"); return (new TeamStateKeeper(getId(), getStudyId(),
     * getTeamUser(), getTeamUserRole(), getCreator(), getModifiedBy(),
     * getIpAdd(), getTeamUserType(), getSiteFlag())); }
     */
    /**
     * sets up a state holder containing details of the study Team
     */

    /**
     * 
     * 
     * @param tsk
     */
    /*
     * public void setTeamStateKeeper(TeamStateKeeper tsk) { Rlog.debug("team",
     * "IN Team STATE STATE HOLDER"); GenerateId genId = null; try { Connection
     * conn = null; Rlog.debug("team", "GOT STUDY Team ID" + id);
     * Rlog.debug("team", "GOT STUDY ID" + tsk.getStudyId());
     * setStudyId(tsk.getStudyId()); Rlog.debug("team", "GOT Team User" +
     * tsk.getTeamUser()); setTeamUser(tsk.getTeamUser());
     * setTeamUserRole(tsk.getTeamUserRole()); setCreator(tsk.getCreator());
     * setModifiedBy(tsk.getModifiedBy()); setIpAdd(tsk.getIpAdd());
     * setTeamUserType(tsk.getTeamUserType()); setSiteFlag(tsk.getSiteFlag()); }
     * catch (Exception e) { // System.out.println("EXCEPTION " + e);
     * Rlog.fatal("team", "EXCEPTION IN CREATING NEW STUDY TEAM" + e); } }
     */
    /**
     * update team data
     */

    public int updateTeam(TeamBean tsk) {
        Rlog.debug("team", "IN ENTITY BEAN - TEAM  STATE HOLDER");

        try {
            setStudyId(tsk.getStudyId());
            setTeamUser(tsk.getTeamUser());
            setTeamUserRole(tsk.getTeamUserRole());
            setCreator(tsk.getCreator());
            setModifiedBy(tsk.getModifiedBy());
            setIpAdd(tsk.getIpAdd());
            setTeamUserType(tsk.getTeamUserType());
            setSiteFlag(tsk.getSiteFlag());
            setTeamStatus(tsk.getTeamStatus());
            Rlog.debug("team", "SET THE VALUES");
            return 0;
        } catch (Exception e) {
            Rlog.fatal("team", "EXCEPTION IN UPDATING NEW TEAM " + e);
            return -2;
        }
    }

}// end of class

