/*
 * Classname			UserSiteBean
 * 
 * Version information	1.0
 *
 * Date					04/23/2003
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.business.userSite.impl;

/* Import Statements */
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* End of Import Statements */

/**
 * The UserSite CMP entity bean.<br>
 * <br>
 * 
 * @author Sajal
 * @version 1.0
 */
@Entity
@Table(name = "er_usersite")
@NamedQuery(name = "findUserSiteRight", query = "SELECT OBJECT(usite) FROM UserSiteBean usite where fk_user = :fkuser and fk_site = :fksite ")
public class UserSiteBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3256438110094177328L;

    /**
     * the user site Id
     */

    public int userSiteId;

    /**
     * the user site's user Id
     */
    public Integer userSiteUserId;

    /**
     * the user site's site Id
     */

    public Integer userSiteSiteId;

    /**
     * the user site right
     */

    public Integer userSiteRight;

    /*
     * the record creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * the IP Address
     */
    public String ipAdd;

    // setter getter methods over here

    /**
     * Returns the User Site Id
     * 
     * @return userSiteId
     */

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_USERSITE", allocationSize=1)
    @Column(name = "PK_USERSITE")
    public int getUserSiteId() {
        return this.userSiteId;
    }

    public void setUserSiteId(int usrSiteId) {
        this.userSiteId = usrSiteId;
    }

    /**
     * Returns the User Id from User Site
     * 
     * @return String
     */
    @Column(name = "FK_USER")
    public String getUserSiteUserId() {
        return StringUtil.integerToString(this.userSiteUserId);
    }

    /**
     * Sets the User Id in User Site
     * 
     * @param userSiteUserId
     *            User Id
     */
    public void setUserSiteUserId(String userSiteUserId) {
        if (userSiteUserId != null) {
            this.userSiteUserId = Integer.valueOf(userSiteUserId);
        }
    }

    /**
     * Returns the Site Id from User Site
     * 
     * @return String
     */
    @Column(name = "FK_SITE")
    public String getUserSiteSiteId() {
        return StringUtil.integerToString(this.userSiteSiteId);
    }

    /**
     * Sets the Site Id in User Site
     * 
     * @param userSiteSiteId
     *            Site Id
     */
    public void setUserSiteSiteId(String userSiteSiteId) {
        if (userSiteSiteId != null) {
            this.userSiteSiteId = Integer.valueOf(userSiteSiteId);
        }
    }

    /**
     * Returns the rights from the user site
     * 
     * @return String
     */
    @Column(name = "USERSITE_RIGHT")
    public String getUserSiteRight() {
        return StringUtil.integerToString(this.userSiteRight);
    }

    /**
     * Sets the rights in the user site
     * 
     * @param userSiteRight
     *            Rights
     */
    public void setUserSiteRight(String userSiteRight) {
        if (userSiteRight != null)
            this.userSiteRight = Integer.valueOf(userSiteRight);
    }

    /**
     * @return The Id of the creator of the record
     */
    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    /**
     * @return The Id of the user modifying this record
     */
    @Column(name = "LAST_MODIFIED_BY")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    /**
     * @return IP Address
     */
    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // END OF GETTER SETTER METHODS

    /**
     * Returns the state holder object associated with this user site
     * 
     * @return UserSiteStateKeeper
     */
    /*
     * public UserSiteStateKeeper getUserSiteStateKeeper() {
     * Rlog.debug("userSite", "GET User SITE KEEPER"); return (new
     * UserSiteStateKeeper(getUserSiteId(), getUserSiteUserId(),
     * getUserSiteSiteId(), getUserSiteRight(), getCreator(), getModifiedBy(),
     * getIpAdd())); }
     */

    /**
     * sets up a state holder containing details of the user site
     * 
     * @param ussk
     *            UserSiteStateKeeper
     */

    /*
     * public void setUserSiteStateKeeper(UserSiteStateKeeper ussk) {
     * Rlog.debug("userSite", "In UserSiteBean.setUserSiteStateKeeper");
     * GenerateId genId = null; try { Connection conn = null; conn =
     * getConnection(); Rlog.debug("userSite", "Connection :" + conn);
     * userSiteId = genId.getId("SEQ_ER_USERSITE", conn); conn.close();
     * Rlog.debug("userSite", "GOT User SITE ID" + userSiteId);
     * Rlog.debug("userSite", "GOT User SITE ID" + ussk.getUserSiteId());
     * 
     * setUserSiteUserId(ussk.getUserSiteUserId());
     * setUserSiteSiteId(ussk.getUserSiteSiteId());
     * setUserSiteRight(ussk.getUserSiteRight()); setCreator(ussk.getCreator());
     * setModifiedBy(ussk.getModifiedBy()); setIpAdd(ussk.getIpAdd());
     * Rlog.debug("userSite", "saving the data in the user site bean"); } catch
     * (Exception e) { Rlog.fatal("userSite", "EXCEPTION
     * UserSiteBean.setUserSiteStateKeeper" + e); } }
     */

    /**
     * Update the User Site State Keeper in User Site and returs 0 if success
     * else -2
     * 
     * @param ussk
     *            UserSiteStateKeeper
     * @return 0 or -2 depending on the updation
     */
    public int updateUserSite(UserSiteBean ussk) {
        try {
            setUserSiteUserId(ussk.getUserSiteUserId());
            setUserSiteSiteId(ussk.getUserSiteSiteId());
            setUserSiteRight(ussk.getUserSiteRight());
            setModifiedBy(ussk.getModifiedBy());
            setIpAdd(ussk.getIpAdd());
            setCreator(ussk.getCreator());
            Rlog.debug("userSite", "UserSiteBean.updateUserSite");
            return 0;
        } catch (Exception e) {
            Rlog.fatal("userSite", " error in UserSiteBean.updateUserSite");
            return -2;
        }
    }

    public UserSiteBean() {

    }

    public UserSiteBean(int userSiteId, String userSiteUserId,
            String userSiteSiteId, String userSiteRight, String creator,
            String modifiedBy, String ipAdd) {
        super();
        // TODO Auto-generated constructor stub
        setUserSiteId(userSiteId);
        setUserSiteUserId(userSiteUserId);
        setUserSiteSiteId(userSiteSiteId);
        setUserSiteRight(userSiteRight);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
    }

}// end of class
