package com.velos.eres.business.submission.impl;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name = "er_review_board")
@NamedQueries( {
    @NamedQuery(name = "findByFkCodelstReview", query = "SELECT OBJECT(rb) FROM ReviewBoardBean rb where " +
            " rb.fkCodelstRevboard = :fkCodelstRevboard")   
})
public class ReviewBoardBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer id; //pk_review_board
	private String reviewBoardName;
    private String  reviewBoardDescription;
    private Integer fkAccount;
    private String boardGroupAccess;
    private String boardMomLogic;
    private Integer  reviewBoardDefault;
    private Integer  fkCodelstRevboard;


    public ReviewBoardBean() {}
    
    // GETTER SETTER METHODS
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_REVIEW_BOARD", allocationSize=1)
    @Column(name = "PK_REVIEW_BOARD")
    public Integer getId() {
		return id;
	}
	
    public void setId(Integer id) {
		this.id = id;
	}
	
    @Column(name = "REVIEW_BOARD_NAME")
    public String getReviewBoardName() {
		return reviewBoardName;
	}


	public void setReviewBoardName(String reviewBoardName) {
		this.reviewBoardName = reviewBoardName;
	}

	@Column(name = "REVIEW_BOARD_DESCRIPTION")
	public String getReviewBoardDescription() {
		return reviewBoardDescription;
	}


	public void setReviewBoardDescription(String reviewBoardDescription) {
		this.reviewBoardDescription = reviewBoardDescription;
	}

	@Column(name = "FK_ACCOUNT")
	public Integer getFkAccount() {
		return fkAccount;
	}


	public void setFkAccount(Integer fkAccount) {
		this.fkAccount = fkAccount;
	}

	@Column(name = "BOARD_GROUP_ACCESS")
	public String getBoardGroupAccess() {
		return boardGroupAccess;
	}


	public void setBoardGroupAccess(String boardGroupAccess) {
		this.boardGroupAccess = boardGroupAccess;
	}

	@Column(name = "BOARD_MOM_LOGIC")
	public String getBoardMomLogic() {
		return boardMomLogic;
	}


	public void setBoardMomLogic(String boardMomLogic) {
		this.boardMomLogic = boardMomLogic;
	}

	@Column(name = "REVIEW_BOARD_DEFAULT")
	public Integer getReviewBoardDefault() {
		return reviewBoardDefault;
	}


	public void setReviewBoardDefault(Integer reviewBoardDefault) {
		this.reviewBoardDefault = reviewBoardDefault;
	}

	@Column(name = "FK_CODELST_REVBOARD")
	public Integer getFkCodelstRevboard() {
		return fkCodelstRevboard;
	}


	public void setFkCodelstRevboard(Integer fkCodelstRevboard) {
		this.fkCodelstRevboard = fkCodelstRevboard;
	}	
	
}
