/*
 * Classname			StudySecDao.class
 * 
 * Version information 	1.0
 *
 * Date					12/17/2014
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import com.velos.eres.service.util.Rlog;


/**
 * StudySecDao for getting studySection data
 * 
 * @author Mugdha Joshi
 * 
 */

public class StudySecDao extends CommonDAO implements java.io.Serializable {
    
    private static final long serialVersionUID = 1L;

	public HashMap<String, String> getStudySectionData(int studyPk) {
        
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sbSQL = new StringBuffer();
        HashMap<String, String> map = new HashMap<String, String>(); 
       
        try {
            conn = getConnection();
            sbSQL.append("select studysec_name, studysec_text from er_studysec where fk_study=?  ");                   
            pstmt = conn.prepareStatement(sbSQL.toString());
            Rlog.debug("studyId", "SQL- " + sbSQL);
            pstmt.setInt(1, studyPk);          
           
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
               map.put(rs.getString("studysec_name"), rs.getString("studysec_text"));
            }
        } catch (SQLException ex) {
            Rlog.fatal("StudySecDao", "EXCEPTION IN FETCHING getStudySectionData" + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return map;
    }
    
}
