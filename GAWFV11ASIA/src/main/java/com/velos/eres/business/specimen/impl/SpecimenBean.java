/*
 * Classname			SpecimenBean
 *
 * Version information	1.0
 *
 * Date					07/09/2007
 *
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.business.specimen.impl;

/* Import Statements */
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;


/* End of Import Statements */

/**
 * The Site CMP entity bean.<br>
 * <br>
 *
 * @author Jnanamay
 * @version 1.0
 */
//JM: 19Sep2007: modified @NamedQuery to include fk_accout
@Entity
@Table(name = "ER_SPECIMEN")
@NamedQuery(name = "findBySpecimenIdentifier", query = "SELECT OBJECT(specimen) FROM SpecimenBean specimen where FK_ACCOUNT = :fkAccount AND UPPER(trim(SPEC_ID)) = UPPER(trim(:specimenId))")


public class SpecimenBean implements Serializable {



    private static final long serialVersionUID = 3761410811205333553L;


    /**
     * the primary key of the Specimen bean
     */

    public int pkSpecimen;

    public String specimenId;

    public String specAltId;

    public String specDesc;

    public Float specOrgQnty;

    public Integer spectQuntyUnits;

    public Integer  specType;
    public Integer  fkStudy;
    public  java.util.Date  specCollDate;
    public Integer  fkPer;
    public Integer  specOwnerFkUser;
    public Integer  fkStorage;
    public Integer  fkSite;
    public Integer fkVisit;
    public Integer fkSchEvents;
    public String   specNote;
    public Integer fkSpec;
    public Integer specStorageTmp;
    public Integer specStorageTmpUnit;
    //public String specimenStat;

    //JM: 19Sep2007: fk_account added to have things account specific
    public String fkAccount;

    //KM
    public Integer specProcType;

    public Integer fkStrgKitComp;

    /*
     * the record creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * the IP Address
     */
   public String ipAdd;

   public String specUserPathologist;

   public String specUserSurgeon;

   public String specUserCollectingTech;
   public String specUserProcessingTech;
   public  java.util.Date  specRemovalDatetime;
   public  java.util.Date  specFreezeDatetime;


   public Integer specAnatomicSite;
   public Integer specTissueSide;
   public Integer specPathologyStat;
   public Integer fkStorageId;
   public Float	specExpectQuant;
   public Float specBaseOrigQuant;
   public Integer specExpectedQUnit;
   public Integer specBaseOrigQUnit;
   /**
    * SPEC_DISPOSITION
    */
   public Integer specDisposition;
   /**
    * SPEC_ENVT_CONS
    */
   public String specEnvtCons;



   public String prepFlag;
   
   
   public String prepKit;
   

//Get Set methods

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "seq_er_specimen", allocationSize=1)

    @Column(name = "PK_SPECIMEN")
    public int getPkSpecimen(){
    	return this.pkSpecimen;
    }
    public void setPkSpecimen( int pkSpecimen){
      	 this.pkSpecimen = pkSpecimen;
     }

    public void setSpecimenId(String specimenId){
    	this.specimenId = specimenId;
    }
    @Column(name = "SPEC_ID")
	public String getSpecimenId(){
		return this.specimenId;
	}

	public void setSpecAltId(String specAltId){
		this.specAltId = specAltId;
	}
	@Column(name = "SPEC_ALTERNATE_ID")
	public String getSpecAltId(){
		return this.specAltId;
	}

    public void setSpecDesc(String specDesc){
    	this.specDesc = specDesc;
    }
	@Column(name = "SPEC_DESCRIPTION")
    public String getSpecDesc(){
		return this.specDesc ;
	}

    public void setSpecOrgQnty(String specOrgQnty){
    	this.specOrgQnty = StringUtil.stringToFloat(specOrgQnty);
    }
	@Column(name = "SPEC_ORIGINAL_QUANTITY")
    public String getSpecOrgQnty(){
		return StringUtil.floatToString(this.specOrgQnty);

	}

    public void setSpectQuntyUnits(String spectQuntyUnits){
    	this.spectQuntyUnits = StringUtil.stringToInteger(spectQuntyUnits);
    }
	@Column(name = "SPEC_QUANTITY_UNITS")
    public String getSpectQuntyUnits(){
		return StringUtil.integerToString(this.spectQuntyUnits);
	}

    public void setSpecType(String specType){
    	this.specType = StringUtil.stringToInteger(specType);
    }
	@Column(name = "SPEC_TYPE")
    public String getSpecType(){
		return StringUtil.integerToString(this.specType);
	}

    public void setFkStudy(String fkStudy){
    	this.fkStudy = StringUtil.stringToInteger(fkStudy);
    }
	@Column(name = "FK_STUDY")
    public String getFkStudy(){
		return StringUtil.integerToString(this.fkStudy);
	}

	//Date --->used with the help of transient
	@Column(name = "SPEC_COLLECTION_DATE")
    public Date getSpecCollDt(){
		return this.specCollDate;
	}

	@Transient
	public String getSpecCollDate(){
		return DateUtil.dateToString(getSpecCollDt(), null);
	}

	    public void setSpecCollDt(Date specCollectionDt) {

	        this.specCollDate = specCollectionDt;
	    }

	    public void setSpecCollDate(String Date) {

	    	//setSpecCollDt(DateUtil.stringToDate(Date, null));
	    	setSpecCollDt(DateUtil.stringToTimeStamp(Date, null));
	    }


    public void setFkPer(String fkPer) {
    	this.fkPer = StringUtil.stringToInteger(fkPer);
    }
	@Column(name = "FK_PER")
	public String getFkPer() {
		return StringUtil.integerToString(this.fkPer);
	}

    public void setSpecOwnerFkUser(String specOwnerUser){
    	this.specOwnerFkUser = StringUtil.stringToInteger(specOwnerUser);
    }
	@Column(name = "SPEC_OWNER_FK_USER")
	public String getSpecOwnerFkUser(){
		return StringUtil.integerToString(this.specOwnerFkUser);
	}

    public void setFkStorage(String fkStorage){
    	this.fkStorage= StringUtil.stringToInteger(fkStorage);
    }
	@Column(name = "FK_STORAGE")
	public String getFkStorage(){
		return StringUtil.integerToString(this.fkStorage);
	}

    public void setFkSite( String fkSite){
    	this.fkSite= StringUtil.stringToInteger(fkSite);
    }
	@Column(name = "FK_SITE")
	public String getFkSite(){
		return StringUtil.integerToString(this.fkSite);
	}

    public void setFkVisit(String fkVisit){
    	this.fkVisit= StringUtil.stringToInteger(fkVisit);
    }
	@Column(name = "FK_VISIT")
	public String getFkVisit(){
		return StringUtil.integerToString(this.fkVisit);
	}

    public void setFkSchEvents(String fkSchEvents){
    	this.fkSchEvents= StringUtil.stringToInteger(fkSchEvents);
    }
	@Column(name = "FK_SCH_EVENTS1")
	public String getFkSchEvents(){
		return StringUtil.integerToString(this.fkSchEvents);
	}

	public void setSpecNote(String specNote){
    	this.specNote = specNote;
    }

	//@Lob(fetch=FetchType.EAGER, type=LobType.CLOB)
	//@Column(name = "SPEC_NOTES")
	@Transient
	public String getSpecNote(){
		return this.specNote;
	}


    public void setFkSpec(String fkSpec){
    	this.fkSpec= StringUtil.stringToInteger(fkSpec);
    }
	@Column(name = "FK_SPECIMEN")
	public String getFkSpec(){
		return StringUtil.integerToString(this.fkSpec);
	}

    public void setSpecStorageTmp(String specStorageTmp){
    	this.specStorageTmp= StringUtil.stringToInteger(specStorageTmp);
    }
	@Column(name = "SPEC_STORAGE_TEMP")
	public String getSpecStorageTmp(){
		return StringUtil.integerToString(this.specStorageTmp);
	}

    public void setSpecStorageTmpUnit(String specStorageTmpUnit){
    	this.specStorageTmpUnit= StringUtil.stringToInteger(specStorageTmpUnit);
    }
	@Column(name = "SPEC_STORAGE_TEMP_UNIT")
    public String getSpecStorageTmpUnit(){
		return StringUtil.integerToString(this.specStorageTmpUnit);
	}

    /*
	@Column(name = "SPECIMEN_STATUS")
    public String getSpecimenStatus() {
        return this.specimenStat;
    }

    public void setSpecimenStatus(String specimenStat) {
        this.specimenStat = specimenStat;
    }*/



     /**
     * @return The Id of the creator of the record
     */
    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {
        this.creator = StringUtil.stringToInteger(creator);
    }

    /**
     * @return The Id of the user modifying this record
     */
   @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
   public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = StringUtil.stringToInteger(modifiedBy);
    }

    /**
     * @return IP Address
     */
   @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
   public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }


   /**
    * @return Fk Account
    */
  @Column(name = "fk_account")
   public String getFkAccount() {
       return this.fkAccount;
   }

   /**
    * @param fkAccount
    *            The Fk Account of the logged in user
    */
  public void setFkAccount(String fkAccount) {
       this.fkAccount = fkAccount;
   }

  //KM:Added for child specimens.


   public void setSpecProcType(String specProcType) {
	   this.specProcType = StringUtil.stringToInteger(specProcType);
   }

   @Column(name ="SPEC_PROCESSING_TYPE")
   public String getSpecProcType(){
	   return StringUtil.integerToString(this.specProcType);
   }



   public void setFkStrgKitComp(String fkStrgKitComp) {
	   this.fkStrgKitComp = StringUtil.stringToInteger(fkStrgKitComp);
   }

   @Column(name ="FK_STORAGE_KIT_COMPONENT")
   public String getFkStrgKitComp(){
	   return StringUtil.integerToString(this.fkStrgKitComp);
   }


// empty constructor
    public SpecimenBean() {

    }

    //Parameterized constructor

    public SpecimenBean( int  pkSpecimen, String specimenId, String specAltId, String specDesc,  String specOrgQnty, String spectQuntyUnits,
    			    String  specType,  String  fkStudy,  String 	specCollDate, String  fkPer,  String  specOwnerFkUser, String  fkStorage,
    			    String  fkSite,  String fkVisit, String fkSchEvents, String notes, String fkSpec,  String specStorageTmp, String specStorageTmpUnit,
    			    String creator,String modifiedBy, String ipAdd, String fkAccount, String specProcType, String fkStrgKitComp,
    			    String surgeon,String procTech,String path, String collTech,String removalTime,String freezeTime, String specAnatomicSite,
    			    String specTissueSide, String specPathologyStat, String fkStorageId, String specExpectQuant,
    			    String specBaseOrigQuant,  String specExpectedQUnit, String specBaseOrigQUnit,String specDisposition,String specEnvtCons,String prepFlag,String prepKit) {



    	setPkSpecimen(pkSpecimen);
    	setSpecimenId(specimenId);
    	setSpecAltId(specAltId);
    	setSpecDesc(specDesc);
    	setSpecOrgQnty(specOrgQnty);
    	setSpectQuntyUnits(spectQuntyUnits);
    	setSpecType(specType);
    	setFkStudy(fkStudy);
    	setSpecCollDate(specCollDate);
    	setFkPer(fkPer);
    	setSpecOwnerFkUser(specOwnerFkUser);
    	setFkStorage(fkStorage);
    	setFkSite(fkSite);
    	setFkVisit(fkVisit);
    	setFkSchEvents(fkSchEvents);
    	//setSpecNote(notes);/////
    	setFkSpec(fkSpec);
    	setSpecStorageTmp(specStorageTmp);
    	setSpecStorageTmpUnit(specStorageTmpUnit);
    	//setSpecimenStatus(specimenStat);
       setCreator(creator);
       setModifiedBy(modifiedBy);
       setIpAdd(ipAdd);
       setFkAccount(fkAccount);
       setSpecProcType(specProcType);
       setFkStrgKitComp(fkStrgKitComp);

       setSpecUserSurgeon(surgeon);
       setSpecUserProcessingTech(procTech);
       setSpecUserPathologist(path);
       setSpecUserCollectingTech(collTech);
       setSpecRemovalDatetimeDt(removalTime);
       setSpecFreezeDatetimeDt(freezeTime);


	   setSpecAnatomicSite(specAnatomicSite);
	   setSpecTissueSide(specTissueSide);
	   setSpecPathologyStat(specPathologyStat);
	   setFkStorageId(fkStorageId);
	   setSpecExpectQuant(specExpectQuant);
	   setSpecBaseOrigQuant(specBaseOrigQuant);
	   setSpecExpectedQUnits(specExpectedQUnit);
	   setSpecBaseOrigQUnit(specBaseOrigQUnit);
	   //Added for INVP 2.8
	   setSpecDisposition(specDisposition);   
	   setSpecEnvtCons(specEnvtCons);
	   setprepFlag(prepFlag);
	   setprepKit(prepKit);
	   
	  


    }


        // END OF GETTER SETTER METHODS

    /**
     * Update the Site State Keeper in Specimen and returs 0 if success else -2
     *
     * @param ssk
     * @return 0 or -2 depending on the updation
     */
    public int updateSpecimen(SpecimenBean spsk) {
        try {


        	setPkSpecimen(spsk.getPkSpecimen());
        	setSpecimenId(spsk.getSpecimenId());
        	setSpecAltId(spsk.getSpecAltId());
        	setSpecDesc(spsk.getSpecDesc());
        	setSpecOrgQnty(spsk.getSpecOrgQnty());
        	setSpecBaseOrigQuant(spsk.getSpecBaseOrigQuant());

        	setSpectQuntyUnits(spsk.getSpectQuntyUnits());
        	setSpecType(spsk.getSpecType());
        	setFkStudy(spsk.getFkStudy());
        	setSpecCollDate(spsk.getSpecCollDate());
        	setFkPer(spsk.getFkPer());
        	setSpecOwnerFkUser(spsk.getSpecOwnerFkUser());
        	setFkStorage(spsk.getFkStorage());
        	setFkSite(spsk.getFkSite());
        	setFkVisit(spsk.getFkVisit());
        	setFkSchEvents(spsk.getFkSchEvents());
        	//setSpecNote(spsk.getSpecNote());
        	setFkSpec(spsk.getFkSpec());
        	setSpecStorageTmp(spsk.getSpecStorageTmp());
        	setSpecStorageTmpUnit(spsk.getSpecStorageTmpUnit());
        	//setSpecimenStatus(spsk.getSpecimenStatus());
            setCreator(spsk.getCreator());
            setModifiedBy(spsk.getModifiedBy());
            setIpAdd(spsk.getIpAdd());
            setFkAccount(spsk.getFkAccount());
            setSpecProcType(spsk.getSpecProcType());
            setFkStrgKitComp(spsk.getFkStrgKitComp());
            Rlog.debug("specimen", "SpecimenBean.updateSpecimen");

            setSpecUserSurgeon(spsk.getSpecUserSurgeon());
            setSpecUserProcessingTech(spsk.getSpecUserProcessingTech());
            setSpecUserPathologist(spsk.getSpecUserPathologist());
            setSpecUserCollectingTech(spsk.getSpecUserCollectingTech());
            setSpecRemovalDatetimeDt(spsk.getSpecRemovalDatetimeDt());
            setSpecFreezeDatetimeDt(spsk.getSpecFreezeDatetimeDt());



            setSpecAnatomicSite(spsk.getSpecAnatomicSite());
     	    setSpecTissueSide(spsk.getSpecTissueSide());
     	    setSpecPathologyStat(spsk.getSpecPathologyStat());
     	    setFkStorageId(spsk.getFkStorageId());
     	    setSpecExpectQuant(spsk.getSpecExpectQuant());
     	    setSpecBaseOrigQuant(spsk.getSpecBaseOrigQuant());
     		setSpecExpectedQUnits(spsk.getSpecExpectedQUnits());
     		setSpecBaseOrigQUnit(spsk.getSpecBaseOrigQUnit());
     		
     		//Added for INVP2.8
     		
     		 setSpecDisposition(spsk.getSpecDisposition());
     		 setSpecEnvtCons(spsk.getSpecEnvtCons());
     		 setprepFlag(spsk.getprepFlag());
     	     setprepKit(spsk.getprepKit());
     		





         //JM: 30Jan2008: need the parent Specimen Id to set the fk_specimen in the children Specimens...
            //return 0;
            return spsk.getPkSpecimen();

        } catch (Exception e) {
            Rlog.fatal("specimen", " error in SpecimenBean.updateSpecimen" + e);
            e.printStackTrace();
            return -2;
        }
    }

    @Column(name = "SPEC_FREEZE_TIME")
    public java.util.Date getSpecFreezeDatetime() {
    	return specFreezeDatetime;
    }
    public void setSpecFreezeDatetime(java.util.Date specFreezeDatetime) {
    	this.specFreezeDatetime = specFreezeDatetime;
    }


    @Transient
	public String getSpecFreezeDatetimeDt(){
		return DateUtil.dateToString(getSpecFreezeDatetime(),null);
	}



	    public void setSpecFreezeDatetimeDt(String Dte) {

	    	setSpecFreezeDatetime(DateUtil.stringToTimeStamp(Dte, null));
	    }

    @Column(name = "SPEC_REMOVAL_TIME")
    public java.util.Date getSpecRemovalDatetime() {
    	return specRemovalDatetime;
    }
    public void setSpecRemovalDatetime(java.util.Date specRemovalDatetime) {
    	this.specRemovalDatetime = specRemovalDatetime;
    }

    @Transient
	public String getSpecRemovalDatetimeDt(){
		return DateUtil.dateToString(getSpecRemovalDatetime(),null);
	}



	    public void setSpecRemovalDatetimeDt(String Date) {

	    	setSpecRemovalDatetime(DateUtil.stringToTimeStamp(Date, null));
	    }



    @Column(name = "FK_USER_COLLTECH")
    public String getSpecUserCollectingTech() {
    	return specUserCollectingTech;
    }
    public void setSpecUserCollectingTech(String specUserCollectingTech) {

    	if (StringUtil.isEmpty(specUserCollectingTech))
    	{
    		specUserCollectingTech = "";
    	}


    	this.specUserCollectingTech = specUserCollectingTech;
    }

    @Column(name = "FK_USER_PATH")
    public String getSpecUserPathologist() {
    	return specUserPathologist;
    }
    public void setSpecUserPathologist(String specUserPathologist) {

    	if (StringUtil.isEmpty(specUserPathologist))
    	{
    		specUserPathologist = "";
    	}


    	this.specUserPathologist = specUserPathologist;
    }

    @Column(name = "FK_USER_PROCTECH")
    public String getSpecUserProcessingTech() {
    	return specUserProcessingTech;
    }
    public void setSpecUserProcessingTech(String specUserProcessingTech) {

    	if (StringUtil.isEmpty(specUserProcessingTech))
    	{
    		specUserProcessingTech = "";
    	}


    	this.specUserProcessingTech = specUserProcessingTech;
    }

    @Column(name = "FK_USER_SURG")
    public String getSpecUserSurgeon() {
    	return specUserSurgeon;
    }
    public void setSpecUserSurgeon(String specUserSurgeon) {

    	if (StringUtil.isEmpty(specUserSurgeon))
    	{
    		specUserSurgeon = "";
    	}

    	this.specUserSurgeon = specUserSurgeon;
    }

    /* JM: 20May2009: added, enhacement #INVP2.7.1 */

    @Column(name = "SPEC_ANATOMIC_SITE")
    public String getSpecAnatomicSite() {
    	return StringUtil.integerToString(this.specAnatomicSite);
    }
    public void setSpecAnatomicSite(String specAnatomicSite) {

    	this.specAnatomicSite = StringUtil.stringToInteger(specAnatomicSite);
    }


    @Column(name ="SPEC_TISSUE_SIDE")
    public String getSpecTissueSide(){
 	   return StringUtil.integerToString(this.specTissueSide);
    }
    public void setSpecTissueSide(String specTissueSide) {
 	   this.specTissueSide = StringUtil.stringToInteger(specTissueSide);
    }


    @Column(name ="SPEC_PATHOLOGY_STAT")
    public String getSpecPathologyStat(){
 	   return StringUtil.integerToString(this.specPathologyStat);
    }
    public void setSpecPathologyStat(String specPathologyStat) {
 	   this.specPathologyStat = StringUtil.stringToInteger(specPathologyStat);
    }

	@Column(name ="FK_STORAGE_KIT")
    public String getFkStorageId(){
 	   return StringUtil.integerToString(this.fkStorageId);
    }
    public void setFkStorageId(String fkStorageId) {
 	   this.fkStorageId = StringUtil.stringToInteger(fkStorageId);
    }

    @Column(name = "SPEC_EXPECTED_QUANTITY")
    public String getSpecExpectQuant(){
		return StringUtil.floatToString(this.specExpectQuant);
	}
    public void setSpecExpectQuant(String specExpectQuant){
    	this.specExpectQuant = StringUtil.stringToFloat(specExpectQuant);
    }


    @Column(name = "spec_base_orig_quantity")
    public String getSpecBaseOrigQuant(){
		return StringUtil.floatToString(this.specBaseOrigQuant);
	}
    public void setSpecBaseOrigQuant(String specBaseOrigQuant){
    	this.specBaseOrigQuant = StringUtil.stringToFloat(specBaseOrigQuant);
    }


    @Column(name = "spec_expectedq_units")
    public String getSpecExpectedQUnits(){
  	   return StringUtil.integerToString(this.specExpectedQUnit);
    }
    public void setSpecExpectedQUnits(String specExpectedQUnit) {
  	   this.specExpectedQUnit = StringUtil.stringToInteger(specExpectedQUnit);
    }


    @Column(name = "spec_base_origq_units")
    public String getSpecBaseOrigQUnit(){
   	   return StringUtil.integerToString(this.specBaseOrigQUnit);
    }
    public void setSpecBaseOrigQUnit(String specBaseOrigQUnit) {
   	   this.specBaseOrigQUnit = StringUtil.stringToInteger(specBaseOrigQUnit);
    }
//   added for INVP 2.8.
    
    @Column(name = "SPEC_DISPOSITION")
    public String getSpecDisposition() {
    	return StringUtil.integerToString(this.specDisposition);
    }

    public void setSpecDisposition(String specDisposition) {
        this.specDisposition = StringUtil.stringToInteger(specDisposition);
    }
    
    @Column(name = "SPEC_ENVT_CONS")
    public String getSpecEnvtCons() {
    	return this.specEnvtCons;
    }

    public void setSpecEnvtCons(String specEnvtCons) {
        this.specEnvtCons = specEnvtCons;
    }

    @Column(name = "PREPFLAG")
    public String getprepFlag() {
    	return this.prepFlag;
    }

    public void setprepFlag(String prepFlag) {
        this.prepFlag = prepFlag;
    }

    
    @Column(name = "PREPAREKITID")
    public String getprepKit() {
    	return this.prepKit;
    }

    public void setprepKit(String prepKit) {
        this.prepKit = prepKit;
    }

    /* JM: 20May2009: added, enhacement #INVP2.7.1 */
}// end of class
