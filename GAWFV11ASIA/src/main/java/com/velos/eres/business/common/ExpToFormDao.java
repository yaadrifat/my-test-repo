/*
 * Classname : ExpToFormDao
 *
 * Version information: 1.0
 *
 * Date: 11/19/2003
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Sonia Sahni
 */

package com.velos.eres.business.common;

/* Import Statements */
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.sql.BatchUpdateException;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import oracle.jdbc.OracleResultSet;
import oracle.xml.parser.v2.DOMParser;
import oracle.xml.parser.v2.XMLDocument;
import oracle.xml.parser.v2.XMLElement;
import oracle.xml.parser.v2.XMLNode;
import oracle.xml.parser.v2.XMLText;

import org.w3c.dom.NodeList;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* End of Import Statements */

/**
 * The ExpToFormDao<br>
 * <br>
 * The class exposes methods to export forms from linear tables : er_imppatform,
 * er_impacctform, er_impstudyform to eResearch's filled form tables:
 * er_patforms, er_acctforms and er_studyforms respectively. A Utility class for
 * data migration.
 *
 * @author Sonia Sahni
 * @vesion %I%, %G%
 */
/*******************************************************************************
 * **************************History Modified by :Sonia Sahni Modified
 * On:06/18/2004 Comment: 1. Added JavaDoc comments for all methods 2.
 * Implemented changes related to form versioning 3. Implemented updation of
 * existing filledform records 4. Commented addQuote()since its not in use. We
 * can put in StringUtil later if required
 * *************************END****************************************
 */

public class ExpToFormDao extends CommonDAO implements java.io.Serializable {

    /**
     * Default Constructor
     */

    public ExpToFormDao() {
    }

    /**
     * Main() starts form export from linear tables : er_imppatform,
     * er_impacctform, er_impstudyform to eResearch's filled form tables:
     * er_patforms, er_acctforms and er_studyforms respectively.
     */
    public static void main(String arg[]) {

        try {
            ExpToFormDao exd = new ExpToFormDao();
            exd.exportFromLinear();

        } catch (Exception ioe) {
            System.out.println(ioe);

        }
    }

    /**
     * Exports forms from linear tables : er_imppatform, er_impacctform,
     * er_impstudyform to eResearch's filled form tables: er_patforms,
     * er_acctforms and er_studyforms respectively
     */
    /***************************************************************************
     * **************************History Modified by :Sonia Sahni Modified
     * On:06/18/2004 Comment: 1. Implemented changes related to form versioning
     * 2. Implemented updation of existing filledform records Modified by :
     * Vishal Abrol on 07/15/04 . Changed the method to implement custom_col in
     * er_imppatform and insert/update crieteria based on table er_imppkmap
     *
     * END*************************
     **************************************************************************/

    public void exportFromLinear() {

        // get distinct forms for export

        Connection conn = null;

        Statement stmtForms = null;
        Statement stmtPatForms = null;
        Statement stmtStudyForms = null;

        Statement stmtFormData = null;
        CallableStatement stmtAccount = null;

        CallableStatement stmtPat = null;
        CallableStatement stmtStudy = null;

        int formCount = 0;
        int fldCount = 0;

        int loopCounter = 0;

        Integer formId = null;
        Integer formLibVerId = null;

        ArrayList arForms = new ArrayList();
        ArrayList arFormLibVer = new ArrayList();

        ArrayList arStudyForms = new ArrayList();
        ArrayList arStudyFormLibVer = new ArrayList();

        ArrayList arPatForms = new ArrayList();
        ArrayList arPatFormLibVer = new ArrayList();

        ArrayList arAcctExportedFilledForms = new ArrayList();
        ArrayList arStudyExportedFilledForms = new ArrayList();
        ArrayList arPatientExportedFilledForms = new ArrayList();

        ArrayList arSysIds = new ArrayList();
        ArrayList arValues = new ArrayList();
        ArrayList arDataInfo = new ArrayList();

        String blankFormXML = null;
        String updatedXML = null;

        String dataColSQL = "";
        int numberOfColumns = 0;
        int colCounter = 0;
        String colVal = null;
        int retVal = 0;
        int v_datacol_count = 0;

        int frmAccount, frmCompleted, frmValid, frmCreator, frmPat, frmStudy, frmPatProt;
        String frmIpAdd;
        String dataStr = "";
        String colStr = "";
        int formlibver = 0;
        int fk_filledform = -1;

        int pkImpAcctForm = 0;
        int pkImpStudyForm = 0;
        int pkImpPatForm = 0;
        String custom_col = "";

        try {

            Rlog.debug("saveForm",
                    "ExpToFormDao.exportFromLinear--> Export Starting......");

            conn = getLongConnection();

            String queryForms = "Select distinct FK_FORM,FK_FORMLIBVER from er_impacctform where export_flag = 1";
            String queryStudyForms = "Select distinct FK_FORM,FK_FORMLIBVER from er_impstudyform where export_flag = 1";
            String queryPatForms = "Select distinct FK_FORM,FK_FORMLIBVER from er_imppatform where export_flag = 1";

            stmtForms = conn.createStatement();
            ResultSet rsetForms = stmtForms.executeQuery(queryForms);

            while (rsetForms.next()) {
                arForms.add(new Integer(rsetForms.getInt(1)));
                arFormLibVer.add(new Integer(rsetForms.getInt(2)));
            }

            // close statement and resultset

            rsetForms.close();
            stmtForms.close();

            // get study forms

            stmtStudyForms = conn.createStatement();
            ResultSet rsetStudyForms = stmtStudyForms
                    .executeQuery(queryStudyForms);

            while (rsetStudyForms.next()) {
                arStudyForms.add(new Integer(rsetStudyForms.getInt(1)));
                arStudyFormLibVer.add(new Integer(rsetStudyForms.getInt(2)));
            }

            // close statement and resultset

            rsetStudyForms.close();
            stmtStudyForms.close();

            // get patient forms

            stmtPatForms = conn.createStatement();
            ResultSet rsetPatForms = stmtPatForms.executeQuery(queryPatForms);

            while (rsetPatForms.next()) {
                arPatForms.add(new Integer(rsetPatForms.getInt(1)));
                arPatFormLibVer.add(new Integer(rsetPatForms.getInt(2)));
            }

            // close statement and resultset

            rsetPatForms.close();
            stmtPatForms.close();

            // got all forms

            // prepare statements

            stmtAccount = conn
                    .prepareCall("{call PKG_FORMREP.SP_INSERT_ACCTFORM_EXP(?,?,?,?,?,?,?,?,?,?,?,?,?)}");
            stmtStudy = conn
                    .prepareCall("{call PKG_FORMREP.SP_INSERT_STUDYFORM_EXP(?,?,?,?,?,?,?,?,?,?,?,?,?)}");
            stmtPat = conn
                    .prepareCall("{call PKG_FORMREP.SP_INSERT_PATFORM_EXP(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");

            // loop for account forms

            // iterate through each form to export its instances
            formCount = arForms.size();

            for (loopCounter = 0; loopCounter <= formCount - 1; loopCounter++) // (loop1)
            {

                formId = (Integer) arForms.get(loopCounter); // got form
                formLibVerId = (Integer) arFormLibVer.get(loopCounter); // got
                // formlibver

                Rlog.debug("saveForm",
                        "ExpToFormDao.exportFromLinear--> Export for Account form: "
                                + formId);

                if (formId != null) {
                    // get blank xml for this form
                    blankFormXML = getFormXML(formId.intValue(), formLibVerId
                            .intValue());
                    // get field names/systemids for this form
                    arSysIds = getFormSystemIds(formId);
                    // get data information
                    arDataInfo = getFormDataInfo(formId, "A", formLibVerId);

                    // get column list SQL for form data
                    if (arDataInfo.size() == 2) {
                        dataColSQL = (String) arDataInfo.get(0);
                        v_datacol_count = ((Integer) arDataInfo.get(1))
                                .intValue();
                    }

                    Rlog.debug("saveForm",
                            "ExpToFormDao.exportFromLinear--> got info....");
                    // / get all forms instances using the dataColSQL

                    stmtFormData = conn.createStatement();
                    ResultSet rsetFormData = stmtFormData
                            .executeQuery(dataColSQL);
                    ResultSetMetaData metaRsetForm = rsetFormData.getMetaData();

                    numberOfColumns = metaRsetForm.getColumnCount();

                    Rlog
                            .fatal("saveForm",
                                    "ExpToFormDao.exportFromLinear--> getting data....");
                    while (rsetFormData.next()) // loop for every form instance
                    {
                        try {

                            colCounter = 1;

                            if (arValues.size() > 0)
                                arValues.clear();

                            dataStr = "";
                            colStr = "";

                            // append the data in arrayList
                            while (colCounter <= v_datacol_count) {
                                colVal = rsetFormData.getString(colCounter);

                                if (colVal == null)
                                    colVal = "";

                                arValues.add(colVal);

                                // Prepare data String and col string for
                                // er_formslinear

                                colStr = colStr + ",col" + colCounter;

                                if (colVal.indexOf("'") >= 0)
                                    colVal = StringUtil.replace(colVal, "\'",
                                            "[*velquote*]");
                                dataStr = dataStr + ",'" + colVal + "'";

                                colCounter++;

                            } // end of while for colCounter.next()

                            Rlog
                                    .fatal("saveForm",
                                            "ExpToFormDao.exportFromLinear--> got form data columns....");

                            // get other columns for form

                            pkImpAcctForm = rsetFormData
                                    .getInt("pk_impacctform");
                            frmAccount = rsetFormData.getInt("fk_account");
                            frmCompleted = rsetFormData
                                    .getInt("form_completed");
                            frmValid = rsetFormData.getInt("is_valid");
                            frmCreator = rsetFormData.getInt("creator");
                            frmIpAdd = rsetFormData.getString("ip_Add");
                            fk_filledform = rsetFormData
                                    .getInt("fk_filledform");

                            // ready with all column values for this row

                            // update the blank form xml
                            updatedXML = updateBlankXML(blankFormXML, arSysIds,
                                    arValues);

                            Rlog
                                    .fatal("saveForm",
                                            "ExpToFormDao.exportFromLinear--> updated xml....");

                            // insert the xml in respective table

                            stmtAccount.setInt(1, formId.intValue());
                            stmtAccount.setInt(2, frmAccount);
                            stmtAccount.setClob(3, getCLOB(updatedXML,
                                    conn));
                            stmtAccount.setInt(4, frmCompleted);
                            stmtAccount.setInt(5, frmCreator);
                            stmtAccount.setInt(6, frmValid);
                            stmtAccount.setString(7, frmIpAdd);
                            stmtAccount.setString(8, dataStr);

                            stmtAccount.setString(9, colStr);
                            stmtAccount.setInt(10, formLibVerId.intValue());
                            stmtAccount.setInt(11, fk_filledform);

                            stmtAccount.registerOutParameter(12,
                                    java.sql.Types.INTEGER);
                            stmtAccount.setInt(13, pkImpAcctForm);
                            stmtAccount.execute();

                            retVal = stmtAccount.getInt(12);

                            if (fk_filledform <= 0 && retVal > 0) {
                                Rlog.debug("saveForm",
                                        "ExpToFormDao.exportFromLinear--> Inserted form xml....new filledform id:"
                                                + retVal);
                                arAcctExportedFilledForms.add(new Integer(
                                        pkImpAcctForm));
                            } else if (fk_filledform > 0 && retVal == 0) {
                                Rlog.debug("saveForm",
                                        "ExpToFormDao.exportFromLinear--> Updated form xml....for filledform = "
                                                + fk_filledform);
                                arAcctExportedFilledForms.add(new Integer(
                                        pkImpAcctForm));
                            } else {
                                Rlog.debug("saveForm",
                                        "ExpToFormDao.exportFromLinear--> Error in exporting record : "
                                                + pkImpAcctForm
                                                + " from er_impacctform");
                            }
                            // form inserted
                        } // try block
                        catch (SQLException ex) {
                            Rlog
                                    .fatal(
                                            "saveForm",
                                            "ExpToFormDao.exportFromLinear-->EXCEPTION IN EXPORT TO  XML for Account form: "
                                                    + formId
                                                    + " and pk_impaactform: "
                                                    + pkImpAcctForm
                                                    + " with message:"
                                                    + ex.getMessage());

                        }

                    } // end of while for rsetFormData.next()

                    // close statement and resultset
                    rsetFormData.close();
                    stmtFormData.close();

                    // ///

                } // end of if (formId != null)

            } // end of for loop for Account Forms (loop1)

            if (stmtAccount != null)
                stmtAccount.close();

            // start processing study forms

            // iterate through each form to export its instances
            formCount = arStudyForms.size();

            for (loopCounter = 0; loopCounter <= formCount - 1; loopCounter++) // (loop1)
            {

                formId = (Integer) arStudyForms.get(loopCounter); // got form
                formLibVerId = (Integer) arStudyFormLibVer.get(loopCounter); // got
                // formlibver

                Rlog.debug("saveForm",
                        "ExpToFormDao.exportFromLinear--> Export for Study form: "
                                + formId);

                if (formId != null) {
                    // get blank xml for this form
                    blankFormXML = getFormXML(formId.intValue(), formLibVerId
                            .intValue());
                    // get field names/systemids for this form
                    arSysIds = getFormSystemIds(formId);
                    // get data information
                    arDataInfo = getFormDataInfo(formId, "S", formLibVerId);

                    // get column list SQL for form data
                    if (arDataInfo.size() == 2) {
                        dataColSQL = (String) arDataInfo.get(0);
                        v_datacol_count = ((Integer) arDataInfo.get(1))
                                .intValue();
                    }

                    Rlog.debug("saveForm",
                            "ExpToFormDao.exportFromLinear--> got info....");
                    // / get all forms instances using the dataColSQL

                    stmtFormData = conn.createStatement();

                    ResultSet rsetFormData = stmtFormData
                            .executeQuery(dataColSQL);

                    Rlog
                            .fatal("saveForm",
                                    "ExpToFormDao.exportFromLinear--> getting data....");

                    while (rsetFormData.next()) // loop for every form instance
                    {

                        try {
                            colCounter = 1;

                            if (arValues.size() > 0)
                                arValues.clear();

                            dataStr = "";
                            colStr = "";

                            // append the data in arrayList
                            while (colCounter <= v_datacol_count) {
                                colVal = rsetFormData.getString(colCounter);

                                if (colVal == null)
                                    colVal = "";

                                arValues.add(colVal);

                                // Prepare data String and col string for
                                // er_formslinear

                                colStr = colStr + ",col" + colCounter;

                                if (colVal.indexOf("'") >= 0)
                                    colVal = StringUtil.replace(colVal, "\'",
                                            "[*velquote*]");
                                dataStr = dataStr + ",'" + colVal + "'";

                                colCounter++;

                            } // end of while for colCounter.next()

                            Rlog
                                    .fatal("saveForm",
                                            "ExpToFormDao.exportFromLinear--> got form data columns....");

                            // get other columns for form

                            pkImpStudyForm = rsetFormData
                                    .getInt("pk_impstudyform");

                            frmStudy = rsetFormData.getInt("fk_study");
                            frmCompleted = rsetFormData
                                    .getInt("form_completed");
                            frmValid = rsetFormData.getInt("is_valid");
                            frmCreator = rsetFormData.getInt("creator");
                            frmIpAdd = rsetFormData.getString("ip_Add");
                            fk_filledform = rsetFormData
                                    .getInt("fk_filledform");

                            // ready with all column values for this row

                            // update the blank form xml
                            updatedXML = updateBlankXML(blankFormXML, arSysIds,
                                    arValues);

                            Rlog
                                    .fatal("saveForm",
                                            "ExpToFormDao.exportFromLinear--> updated xml....");

                            // insert the xml in respective table

                            stmtStudy.setInt(1, formId.intValue());
                            stmtStudy.setInt(2, frmStudy);
                            stmtStudy.setClob(3, getCLOB(updatedXML,
                                    conn));
                            stmtStudy.setInt(4, frmCompleted);
                            stmtStudy.setInt(5, frmCreator);
                            stmtStudy.setInt(6, frmValid);
                            stmtStudy.setString(7, frmIpAdd);
                            stmtStudy.setString(8, dataStr);

                            stmtStudy.setString(9, colStr);
                            stmtStudy.setInt(10, formLibVerId.intValue());
                            stmtStudy.setInt(11, fk_filledform);
                            stmtStudy.registerOutParameter(12,
                                    java.sql.Types.INTEGER);
                            stmtStudy.setInt(13, pkImpStudyForm);
                            stmtStudy.execute();

                            retVal = stmtStudy.getInt(12);

                            if (fk_filledform <= 0 && retVal > 0) {
                                Rlog.debug("saveForm",
                                        "ExpToFormDao.exportFromLinear--> Inserted form xml...."
                                                + retVal);
                                arStudyExportedFilledForms.add(new Integer(
                                        pkImpStudyForm));
                            } else if (fk_filledform > 0 && retVal == 0) {
                                Rlog.debug("saveForm",
                                        "ExpToFormDao.exportFromLinear--> Updated form xml....for filledform = "
                                                + fk_filledform);
                                arStudyExportedFilledForms.add(new Integer(
                                        pkImpStudyForm));
                            } else {
                                Rlog.debug("saveForm",
                                        "ExpToFormDao.exportFromLinear--> Error in exporting record : "
                                                + pkImpStudyForm
                                                + " from er_impstudyform");
                            }
                            // form inserted
                        } // try block
                        catch (SQLException ex) {
                            Rlog.fatal("saveForm",
                                    "ExpToFormDao.exportFromLinear-->EXCEPTION IN EXPORT TO  XML for Study form: "
                                            + formId + " and pk_impstudyform: "
                                            + pkImpStudyForm + " with message:"
                                            + ex.getMessage());
                        }

                    } // end of while for rsetFormData.next()

                    // close statement and resultset
                    rsetFormData.close();
                    stmtFormData.close();

                    // ///

                } // end of if (formId != null)

            } // end of for loop for STudy Forms (loop1)

            if (stmtStudy != null)
                stmtStudy.close();

            // end of study forms

            // start processing patient forms

            // iterate through each form to export its instances
            formCount = arPatForms.size();

            for (loopCounter = 0; loopCounter <= formCount - 1; loopCounter++) // (loop1)
            {

                formId = (Integer) arPatForms.get(loopCounter); // got form
                formLibVerId = (Integer) arPatFormLibVer.get(loopCounter); // got
                // form

                Rlog.debug("saveForm",
                        "ExpToFormDao.exportFromLinear--> Export for Patient form: "
                                + formId);

                if (formId != null) {
                    // get blank xml for this form
                    blankFormXML = getFormXML(formId.intValue(), formLibVerId
                            .intValue());
                    // get field names/systemids for this form
                    arSysIds = getFormSystemIds(formId);
                    // get data information
                    arDataInfo = getFormDataInfo(formId, "P", formLibVerId);

                    // get column list SQL for form data
                    if (arDataInfo.size() == 2) {
                        dataColSQL = (String) arDataInfo.get(0);
                        v_datacol_count = ((Integer) arDataInfo.get(1))
                                .intValue();
                    }
                    Rlog.debug("saveForm",
                            "ExpToFormDao.exportFromLinear--> got info....");

                    // / get all forms instances using the dataColSQL

                    stmtFormData = conn.createStatement();
                    ResultSet rsetFormData = stmtFormData
                            .executeQuery(dataColSQL);
                    Rlog
                            .fatal("saveForm",
                                    "ExpToFormDao.exportFromLinear--> getting data........");

                    while (rsetFormData.next()) // loop for every form instance
                    {
                        try {
                            colCounter = 1;

                            if (arValues.size() > 0)
                                arValues.clear();

                            dataStr = "";
                            colStr = "";

                            // append the data in arrayList
                            while (colCounter <= v_datacol_count) {
                                colVal = rsetFormData.getString(colCounter);
                                if (colVal == null)
                                    colVal = "";
                                arValues.add(colVal);

                                // Prepare data String and col string for
                                // er_formslinear
                                colStr = colStr + ",col" + colCounter;
                                if (colVal.indexOf("'") >= 0)
                                    colVal = StringUtil.replace(colVal, "\'",
                                            "[*velquote*]");
                                dataStr = dataStr + ",'" + colVal + "'";
                                colCounter++;

                            } // end of while for colCounter.next()

                            Rlog
                                    .fatal("saveForm",
                                            "ExpToFormDao.exportFromLinear--> got form data columns....");
                            // get other columns for form

                            pkImpPatForm = rsetFormData.getInt("pk_imppatform");

                            frmPat = rsetFormData.getInt("fk_per");
                            frmPatProt = rsetFormData.getInt("fk_patprot");

                            frmCompleted = rsetFormData
                                    .getInt("form_completed");
                            frmValid = rsetFormData.getInt("is_valid");
                            frmCreator = rsetFormData.getInt("creator");
                            frmIpAdd = rsetFormData.getString("ip_Add");
                            fk_filledform = rsetFormData
                                    .getInt("fk_filledform");

                            // This column decides if the procedure will run in
                            // custom mode and default mode.
                            custom_col = rsetFormData.getString("custom_col");

                            custom_col = (custom_col == null) ? "" : custom_col;

                            if (custom_col.length() == 0)
                                custom_col = "[VELDEFAULT]";

                            // ready with all column values for this row

                            // update the blank form xml
                            updatedXML = updateBlankXML(blankFormXML, arSysIds,
                                    arValues);

                            Rlog.debug("saveForm",
                                    "ExpToFormDao.exportFromLinear--> updated xml...."
                                            + updatedXML.length());

                            // insert the xml in respective table

                            stmtPat.setInt(1, formId.intValue());
                            stmtPat.setInt(2, frmPat);
                            stmtPat.setInt(3, frmPatProt);
                            stmtPat.setClob(4, getCLOB(updatedXML, conn));
                            stmtPat.setInt(5, frmCompleted);
                            stmtPat.setInt(6, frmCreator);
                            stmtPat.setInt(7, frmValid);
                            stmtPat.setString(8, frmIpAdd);
                            stmtPat.setString(9, dataStr);

                            stmtPat.setString(10, colStr);
                            stmtPat.setInt(11, formLibVerId.intValue());
                            stmtPat.setInt(12, fk_filledform);
                            stmtPat.setString(13, custom_col);
                            stmtPat.registerOutParameter(14,
                                    java.sql.Types.INTEGER);
                            stmtPat.setInt(15, pkImpPatForm);
                            stmtPat.execute();

                            retVal = stmtPat.getInt(14);
                            if (custom_col.equals("[VELDEFAULT]")) {
                                if (fk_filledform <= 0 && retVal > 0) {
                                    Rlog.debug("saveForm",
                                            "ExpToFormDao.exportFromLinear--> Inserted form xml...."
                                                    + retVal);
                                    arPatientExportedFilledForms
                                            .add(new Integer(pkImpPatForm));
                                } else if (fk_filledform > 0 && retVal == 0) {
                                    Rlog.debug("saveForm",
                                            "ExpToFormDao.exportFromLinear--> Updated form xml....for filledform = "
                                                    + fk_filledform);
                                    arPatientExportedFilledForms
                                            .add(new Integer(pkImpPatForm));
                                } else {
                                    Rlog.debug("saveForm",
                                            "ExpToFormDao.exportFromLinear--> Error in exporting record : "
                                                    + pkImpPatForm
                                                    + " from er_imppatform");
                                }
                            } else // do the processing in custom mode
                            {

                                if (retVal >= 0)
                                    arPatientExportedFilledForms
                                            .add(new Integer(pkImpPatForm));
                            }
                            // form inserted
                        } // try block
                        catch (SQLException ex) {
                            Rlog
                                    .fatal(
                                            "saveForm",
                                            "ExpToFormDao.exportFromLinear-->EXCEPTION IN EXPORT TO  XML for Patient form: "
                                                    + formId
                                                    + " and pk_imppatform: "
                                                    + pkImpPatForm
                                                    + "with message:"
                                                    + ex.getMessage());
                        }

                    } // end of while for rsetFormData.next()

                    // close statement and resultset
                    rsetFormData.close();
                    stmtFormData.close();

                    // ///

                } // end of if (formId != null)

            } // end of for loop for STudy Forms (loop1)

            if (stmtPat != null)
                stmtPat.close();

            // end of study forms

            Rlog
                    .fatal("saveForm",
                            "ExpToFormDao.exportFromLinear--> finished exporting data...");
            Rlog.debug("saveForm",
                    "ExpToFormDao.exportFromLinear--> Updating status...");

            if (arAcctExportedFilledForms.size() > 0) {
                // updateExportFlag(arAcctExportedFilledForms, "A");
            }
            if (arStudyExportedFilledForms.size() > 0) {
                // updateExportFlag(arStudyExportedFilledForms, "S");
            }

            if (StringUtil.isEmpty(custom_col)) {
                custom_col = "";
            }
            if (!custom_col.equals("[VELDEFAULT]")) {
                if (arPatientExportedFilledForms.size() > 0) {
                    // updateExportFlag(arPatientExportedFilledForms, "P");
                    Rlog
                            .fatal("saveForm",
                                    "ExpToFormDao.exportFromLinear--> updated export status for pat forms...");
                }
            }
            Rlog.debug("saveForm",
                    "ExpToFormDao.exportFromLinear--> Finished...");

        }

        catch (Exception ex) {
            Rlog.fatal("saveForm",
                    "ExpToFormDao.exportFromLinear-->EXCEPTION IN EXPORT TO  XML"
                            + ex);

        } finally {
            try {
                if (stmtForms != null)
                    stmtForms.close();

                if (stmtFormData != null)
                    stmtFormData.close();
                if (stmtAccount != null)
                    stmtAccount.close();
                if (stmtPat != null)
                    stmtAccount.close();
                if (stmtStudy != null)
                    stmtAccount.close();
                if (stmtStudyForms != null)
                    stmtStudyForms.close();
                if (stmtPatForms != null)
                    stmtPatForms.close();

            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

        // end of method

    }

    /**
     * Gets the blank XML for a form and its version
     *
     * @param formId
     *            Id of the form
     * @param formlibver
     *            Id of the form version
     * @return returns XML string
     */
    public String getFormXML(int formId, int formlibver) {

        Connection conn = null;
        PreparedStatement s = null;

        String xmlStr = "";

        try {
            conn = getLongConnection();

            String query = "select c.FORMLIBVER_XML.getClobVal() from er_formlibver c where fk_formlib = ? and pk_formlibver = ?";

            s = conn.prepareStatement(query);
            s.setInt(1, formId);
            s.setInt(2, formlibver);

            ResultSet rset = s.executeQuery();
            OracleResultSet orset = (OracleResultSet) rset;

            while (orset.next()) {

                // retrieve form xml document from database

                Clob xmlCLob = null;

                xmlCLob = orset.getClob(1);

                if (!(xmlCLob == null)) {
                    xmlStr = xmlCLob.getSubString(1, (int) xmlCLob.length());
                }

            }
            return xmlStr;
        }

        catch (Exception ex) {
            Rlog.fatal("saveForm",
                    "ExpToFormDao.getFormXML EXCEPTION IN getting XML" + ex);
            return "";
        } finally {
            try {
                if (s != null)
                    s.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

        // end of method
    }

    /**
     * Returns an ArrayList of systemIds of all the fields of a form
     *
     * @param formId
     *            Id of the form
     * @return returns an ArrayList of systemIds
     */
    public ArrayList getFormSystemIds(Integer formId) {

        Connection conn = null;
        Statement stmtSysIds = null;
        ArrayList arSysIds = new ArrayList();

        try {
            conn = getLongConnection();
            String querySysId = "Select  MP_SYSTEMID, mp_sequence from er_mapform where fk_form = "
                    + formId + " order by mp_sequence";

            stmtSysIds = conn.createStatement();
            ResultSet rsetSysIds = stmtSysIds.executeQuery(querySysId);

            if (arSysIds.size() > 0)
                arSysIds.clear();

            while (rsetSysIds.next()) {
                arSysIds.add(rsetSysIds.getString(1));
            }

            // close statement and resultset

            rsetSysIds.close();
            stmtSysIds.close();

            // got all field names

            return arSysIds;
        }

        catch (Exception ex) {
            Rlog.fatal("saveForm", "ExpToFormDao.getFormSystemIds " + ex);
            return arSysIds;
        } finally {
            try {
                if (stmtSysIds != null)
                    stmtSysIds.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    // end of method
    /**
     * Gets form data access information. Returns an ArrayList containing
     * following information: <BR>
     * 1. The first element contains a SQL statement that can get filled form
     * records (from respective temporary linear table)<BR>
     * for a form and its version. <BR>
     * 2. The second element contains the number of columns in the select
     * statement
     *
     * @param formId
     *            Id of the form
     * @param formType
     *            Id the form type (A:Account,S:Study,P:Patient)
     * @param formLibVer
     *            Id of the form version
     * @return returns an ArrayList of form data access information
     */

    public ArrayList getFormDataInfo(Integer formId, String formType,
            Integer formLibVer) {

        Connection conn = null;
        CallableStatement stmtDataSQL = null;
        ArrayList arInfo = new ArrayList();
        String dataColSQL = null;
        int v_datacol_count = 0;
        Clob XmlClob = null;
        //KM-#3919
        try {
            conn = getLongConnection();
            stmtDataSQL = conn
                    .prepareCall("{call PKG_FORMREP.SP_GET_DATA_COLUMN_SQL(?,?,?,?,?)}");
            stmtDataSQL.setInt(1, formId.intValue());
            stmtDataSQL.setString(2, formType);
            stmtDataSQL.setInt(3, formLibVer.intValue());
            stmtDataSQL.registerOutParameter(4, java.sql.Types.CLOB);
            stmtDataSQL.registerOutParameter(5, java.sql.Types.INTEGER);

            stmtDataSQL.execute();

            XmlClob = stmtDataSQL.getClob(4);
            dataColSQL = XmlClob.getSubString(1, (int) XmlClob.length());
            v_datacol_count = stmtDataSQL.getInt(5);

            stmtDataSQL.close(); // close statement

            arInfo.add(dataColSQL); // first position add SQL
            arInfo.add(new Integer(v_datacol_count)); // Add column count

            return arInfo;
        }

        catch (Exception ex) {
            Rlog.fatal("saveForm", "ExpToFormDao.getFormDataInfo" + ex);
            return arInfo;
        } finally {
            try {
                if (stmtDataSQL != null)
                    stmtDataSQL.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    // end of method

    /**
     * Updates a blank form XML with the passed values
     *
     * @param xmlTypeStr
     *            form XML to be updated
     * @param params
     *            An ArrayList containing names of XML elements to be updated
     * @param values
     *            An ArrayList containing values to be updated in the respective
     *            XML elements <BR>
     *            (passed as 'params' parameter)
     * @return returns Form XML String updated with the passed values
     */
    /* Modified by Sonia, 10/13/04, added check for check for empty textareas */
    /* Modified by Sonia, 10/09/04, added code to populate attribute - checkboxesdata*/
    static String updateBlankXML(String xmlTypeStr, ArrayList params,
            ArrayList values) {

        int paramCt = 0;
        String paramStr = null;
        String paramValueStr = null;

        String attrColCountVal = "";
        String attrDispVal = "";
        String attrTypeVal = "";
        String multiAtrrType = "";

        paramCt = params.size();
        String linesnumber = "";
        String checkboxData = "";
        int sepIndex = -1;

        String outXML = null;
        try {
            DOMParser parser = new DOMParser();

            parser.setPreserveWhitespace(false);

            parser.parse(new StringReader(xmlTypeStr));

            XMLDocument doc = parser.getDocument();

            for (int i = 0; i < paramCt; i++) // iterate for every parameter
            {
                paramStr = (String) params.get(i);
                paramValueStr = (String) values.get(i);

                NodeList nl = doc.getElementsByTagName(paramStr);
                // System.out.println("nl.getLength(): " + nl.getLength());

                for (int n = 0; n < nl.getLength(); n++) {
                    XMLElement elem = (XMLElement) nl.item(n);
                    XMLNode textNode = (XMLNode) elem.getFirstChild();

                    //Create a new node under the node
                     XMLText text = (XMLText) doc.createTextNode("text");
                    //End
                    attrColCountVal = elem.getAttribute("colcount");
                    attrTypeVal = elem.getAttribute("type");

                    linesnumber = elem.getAttribute("linesno");

                    if (attrTypeVal.equals("MR") || attrTypeVal.equals("MC")) {
                        multiAtrrType = "checked";
                    } else if (attrTypeVal.equals("MD")) {
                        multiAtrrType = "selected";
                    }

                    if (StringUtil.isEmpty(attrColCountVal)) {
                        //if (textNode != null)
                            //textNode.setNodeValue(paramValueStr);
                        text.setNodeValue(paramValueStr);
                        if (elem!=null)
                        {
                            if (textNode!=null)
                            {
                               elem.replaceChild(text,textNode);
                             }
                            else {
                            elem.appendChild(text);
                        }
                        }
                    } else // multichoice field
                    {

                    	// check if the paramValueStr has [VELSEP1], is yes, extract the value before [VELSEP1]. This will extract display value from the string
                    	sepIndex = -1;

                    	sepIndex = paramValueStr.indexOf("[VELSEP1]");
                    	if (sepIndex > -1)
                    	{
                    		paramValueStr = paramValueStr.substring(0,sepIndex);
                    	}

                        // get decendent elements

                        NodeList nlresp = elem.getElementsByTagName("resp");
                        checkboxData = "";


                        // System.out.println("nlresp.getLength(): " +
                        // nlresp.getLength());
                        for (int p = 0; p < nlresp.getLength(); p++) {
                            XMLElement elemresp = (XMLElement) nlresp.item(p);
                            XMLNode textNoderesp = (XMLNode) elemresp
                                    .getFirstChild();

                            attrDispVal = elemresp.getAttribute("dispval");

                            if (attrTypeVal.equals("MC")) {
                                // paramValueStr = addQuote(paramValueStr);
                                System.out.println(" paramValueStr"
                                        + paramValueStr + "*");
                            }

                            if (StringUtil.isEmpty(attrDispVal)) {

                            } else // multichoice field
                            {

                                if (attrTypeVal.equals("MC")) {

                                    if (paramValueStr.indexOf("[" + attrDispVal
                                            + "]") >= 0) // check the option
                                    {
                                        elemresp.setAttribute(multiAtrrType,
                                                "1");



                                       // incase of checkboxes, make the comma
                                        // separated string of selected values
                                        if (checkboxData.equals("")) {
                                            checkboxData = "[" + attrDispVal
                                                    + "]";
                                        } else {
                                            checkboxData = checkboxData + ", ["
                                                    + attrDispVal + "]";
                                        }


                                    } else // uncheck the option
                                    {
                                        elemresp.setAttribute(multiAtrrType,
                                                "0");
                                    }



                                } // for checkbox
                                else if (attrTypeVal.equals("MD") || attrTypeVal.equals("MR")) {
                                    if (paramValueStr.equals(attrDispVal)) // check
                                    // the
                                    // option
                                    {
                                        elemresp.setAttribute(multiAtrrType,
                                                "1");

                                        // store the selected value in checkboxData
                                        checkboxData = attrDispVal;

                                    } else // uncheck the option
                                    {
                                        elemresp.setAttribute(multiAtrrType,
                                                "0");
                                        // store the selected value in checkboxData
                                        //checkboxData = "";
                                    }


                                } // end for radio and dropdown

                            }
                        }// end of multi choice field
                        //	store the selected checkboxes in a field atribute
                        elem.setAttribute("checkboxesdata", checkboxData);
                    }
                }

            }

            StringWriter sw = new StringWriter();
            doc.print(new PrintWriter(sw));

            outXML = sw.toString();

        } // end of try
        catch (Exception e) {
            e.printStackTrace(System.out);
        }
        // System.out.println("outXML" + outXML);
        return outXML;
    }

    /**
     * Updates Export Flag for exported rows in temporary linear form tables
     *
     * @param arPKImp
     *            ArrayList containing Primary Keys
     * @param formType
     *            Form type flag: A:Account, P:Patient, S:Study
     */
    public void updateExportFlag(ArrayList arPKImp, String formType) {
        String updateSQL = "";

        if (formType.equals("A")) {
            updateSQL = "Update er_impacctform set export_flag = ? where pk_impacctform = ? ";
        } else if (formType.equals("P")) {
            updateSQL = "Update er_imppatform set export_flag = ? where pk_imppatform = ? ";
        } else if (formType.equals("S")) {
            updateSQL = "Update er_impstudyform set export_flag = ? where pk_impstudyform = ? ";
        }
        Connection conn = null;
        PreparedStatement prepStmt = null;
        try {
            conn = getLongConnection();
            conn.setAutoCommit(false);
            prepStmt = conn.prepareStatement(updateSQL);

            for (int i = 0; i < arPKImp.size(); i++) {
                Integer pk = null;

                pk = (Integer) arPKImp.get(i);

                if (pk != null) {
                    prepStmt.setInt(1, 0); // 0 for exported
                    prepStmt.setInt(2, pk.intValue());
                    prepStmt.addBatch();
                }
            }

            // execute batch
            int[] numUpdates = prepStmt.executeBatch();

            conn.commit();
        } catch (BatchUpdateException buex) {
            // process BatchUpdateException

            int[] updateCounts = buex.getUpdateCounts();
            for (int i = 0; i < updateCounts.length; i++) {
                Rlog.debug("saveForm", "  Statement " + i + ":"
                        + updateCounts[i]);
            }
            Rlog.debug("saveForm", " Message: " + buex.getMessage());
            Rlog.debug("saveForm", " SQLSTATE: " + buex.getSQLState());
            Rlog.debug("saveForm", " Error code: " + buex.getErrorCode());
            SQLException ex = buex.getNextException();
            while (ex != null) {
                Rlog.debug("saveForm", "SQL exception:");
                Rlog.debug("saveForm", " Message: " + ex.getMessage());
                Rlog.debug("saveForm", " SQLSTATE: " + ex.getSQLState());
                Rlog.debug("saveForm", " Error code: " + ex.getErrorCode());
                ex = ex.getNextException();
            }

        } catch (SQLException ex) {
            Rlog.fatal("saveForm",
                    "ExpToFormDao.updateExportFlag --> SQLException" + ex);
        } finally {
            try {
                if (prepStmt != null)
                    prepStmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }

    /***************************************************************************
     * **************************History Modified by :Sonia Sahni Modified
     * On:06/18/2004 Comment: 1. Commented method since its not in use. We can
     * put in StringUtil later if required
     */

    /*
     * static String addQuote(String mainStr) {
     *
     * String str = "";
     *
     * StringTokenizer st = new StringTokenizer(mainStr,","); while
     * (st.hasMoreTokens()) { str = str + ",'" + st.nextToken() + "'"; }
     *
     * str = str.substring(1); return str; }
     */

    /** method to migrate forms from old version to LATEST version*/

    public void migrateFormToLatestVersion(String formId , String formType, String formLibVerId, String ipAdd, String modifiedBy)
    {
    	//	 get distinct forms for export

        Connection conn = null;
        Statement stmtFormData = null;
        CallableStatement stmtAccount = null;

        CallableStatement stmtPat = null;
        CallableStatement stmtStudy = null;

        ArrayList arSysIds = new ArrayList();
        ArrayList arValues = new ArrayList();
        ArrayList arDataInfo = new ArrayList();

        String blankFormXML = null;
        String updatedXML = null;

        String dataColSQL = "";

        int colCounter = 0;
        String colVal = null;
        int retVal = 0;
        int v_datacol_count = 0;

        int  frmCompleted, frmValid, frmModifiedBy ;
        String frmIpAdd;
        String dataStr = "";
        String colStr = "";
        int formlibver = 0;
        int fk_filledform = -1;

        int pkFormsLinear = 0;
        int idLinkedWithForm = 0; //could be account pk/study pk/patient pk, depending on form type
        int fk_patprot  = 0;



        try {
        	conn = getLongConnection();

            // prepare statements
        	//System.out.println("saveformdao" + "formId" + formId + "formLibVerId" + formLibVerId);
        	stmtAccount = conn
                    .prepareCall("{call PKG_FORMREP.SP_INSERT_ACCTFORM_EXP(?,?,?,?,?,?,?,?,?,?,?,?,?)}");
        	stmtStudy = conn
                    .prepareCall("{call PKG_FORMREP.SP_INSERT_STUDYFORM_EXP(?,?,?,?,?,?,?,?,?,?,?,?,?)}");
            stmtPat = conn
                    .prepareCall("{call PKG_FORMREP.SP_INSERT_PATFORM_EXP(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");

        	frmIpAdd = ipAdd;
        	frmModifiedBy = StringUtil.stringToNum(modifiedBy);


        //      get blank xml for this form
        //	System.out.println("saveformdao" + "1");

        blankFormXML = getFormXML(StringUtil.stringToNum(formId), StringUtil.stringToNum(formLibVerId) );

         //System.out.println("saveformdao" + "2");


        // get field names/systemids for this form
        arSysIds = getFormSystemIds(Integer.valueOf(formId));

        //System.out.println("saveformdao" + "3");
        // get data information, pass form type as 'L' to get rows from forms linear
        arDataInfo = getFormDataInfo(Integer.valueOf(formId), "L", Integer.valueOf(formLibVerId));

        //System.out.println("saveformdao" + "4");

        // get column list SQL for form data
        if (arDataInfo.size() == 2) {
            dataColSQL = (String) arDataInfo.get(0);
            v_datacol_count = ((Integer) arDataInfo.get(1))
                    .intValue();
        }
        //create statement and get formdata

        stmtFormData = conn.createStatement();
        ResultSet rsetFormData = stmtFormData
                .executeQuery(dataColSQL);
        while (rsetFormData.next()) // loop for every form instance
        {
            try {

                colCounter = 1;

                if (arValues.size() > 0)
                    arValues.clear();

                dataStr = "";
                colStr = "";

                // append the data in arrayList
                while (colCounter <= v_datacol_count) {
                    colVal = rsetFormData.getString(colCounter);

                    if (colVal == null)
                        colVal = "";

                    arValues.add(colVal);

                    // Prepare data String and col string for
                    // er_formslinear

                    colStr = colStr + ",col" + colCounter;

                    if (colVal.indexOf("'") >= 0)
                        colVal = StringUtil.replace(colVal, "\'",
                                "[*velquote*]");
                    dataStr = dataStr + ",'" + colVal + "'";

                    colCounter++;

                } // end of while for colCounter.next()

                Rlog
                        .fatal("saveForm",
                                "ExpToFormDao.exportFromLinear--> got form data columns....dataColSQL" + dataColSQL);

                // get other columns for form


                pkFormsLinear = rsetFormData.getInt("pk_formslinear");

                frmCompleted = rsetFormData.getInt("form_completed");
                frmValid = rsetFormData.getInt("is_valid");

                fk_filledform = rsetFormData.getInt("fk_filledform");
                idLinkedWithForm = rsetFormData.getInt("id");
                fk_patprot = rsetFormData.getInt("fk_patprot");

                // ready with all column values for this row

                // update the blank form xml
                updatedXML = updateBlankXML(blankFormXML, arSysIds,
                        arValues);


                Rlog.info("saveForm",
                                "ExpToFormDao.migrateFormToLatestVersion--> updated xml....");

                // update the xml in respective table

                if (formType.equals("A"))
                {
                	stmtAccount.setInt(1, StringUtil.stringToNum(formId));
	                stmtAccount.setInt(2, idLinkedWithForm);
	                stmtAccount.setClob(3, getCLOB(updatedXML,
	                        conn));
	                stmtAccount.setInt(4, frmCompleted);
	                stmtAccount.setInt(5, frmModifiedBy);
	                stmtAccount.setInt(6, frmValid);
	                stmtAccount.setString(7, frmIpAdd);
	                stmtAccount.setString(8, dataStr);

	                stmtAccount.setString(9, colStr);
	                stmtAccount.setInt(10, StringUtil.stringToNum(formLibVerId));
	                stmtAccount.setInt(11, fk_filledform);

	                stmtAccount.registerOutParameter(12,
	                        java.sql.Types.INTEGER);
	                stmtAccount.setInt(13, -1);
	                stmtAccount.execute();

	                retVal = stmtAccount.getInt(12);
                }else if  (formType.equals("S"))
                {
                	 stmtStudy.setInt(1, StringUtil.stringToNum(formId));
                     stmtStudy.setInt(2, idLinkedWithForm);
                     stmtStudy.setClob(3, getCLOB(updatedXML,
                             conn));
                     stmtStudy.setInt(4, frmCompleted);
                     stmtStudy.setInt(5, frmModifiedBy);
                     stmtStudy.setInt(6, frmValid);
                     stmtStudy.setString(7, frmIpAdd);
                     stmtStudy.setString(8, dataStr);

                     stmtStudy.setString(9, colStr);
                     stmtStudy.setInt(10, StringUtil.stringToNum(formLibVerId));
                     stmtStudy.setInt(11, fk_filledform);
                     stmtStudy.registerOutParameter(12,
                             java.sql.Types.INTEGER);
                     stmtStudy.setInt(13, -1);
                     stmtStudy.execute();

                     retVal = stmtStudy.getInt(12);

                }else if  (formType.equals("P"))
                {
                	  stmtPat.setInt(1, StringUtil.stringToNum(formId));
                      stmtPat.setInt(2, idLinkedWithForm);
                      stmtPat.setInt(3, fk_patprot);
                      stmtPat.setClob(4, getCLOB(updatedXML, conn));
                      stmtPat.setInt(5, frmCompleted);
                      stmtPat.setInt(6, frmModifiedBy);
                      stmtPat.setInt(7, frmValid);
                      stmtPat.setString(8, frmIpAdd);
                      stmtPat.setString(9, dataStr);

                      stmtPat.setString(10, colStr);
                      stmtPat.setInt(11, StringUtil.stringToNum(formLibVerId));
                      stmtPat.setInt(12, fk_filledform);
                      stmtPat.setString(13, "[VELDEFAULT]");
                      stmtPat.registerOutParameter(14,
                              java.sql.Types.INTEGER);
                      stmtPat.setInt(15, -1);
                      stmtPat.execute();

                      retVal = stmtPat.getInt(14);

                }

                if (fk_filledform <= 0 && retVal > 0) {
                    Rlog.info("saveForm",
                            "ExpToFormDao.migrateFormToLatestVersion--> Inserted form xml....new filledform id:"
                                    + retVal);

                } else if (fk_filledform > 0 && retVal == 0) {
                    Rlog.info("saveForm",
                            "ExpToFormDao.migrateFormToLatestVersion--> Updated form xml....for filledform = "
                                    + fk_filledform);

                } else {
                    Rlog.info("saveForm",
                            "ExpToFormDao.migrateFormToLatestVersion--> Error in migrating record : "
                                    + pkFormsLinear
                                    + " from er_formslinear");
                }
                // form inserted
            } // try block
            catch (SQLException ex) {
                Rlog
                        .fatal(
                                "saveForm",
                                "ExpToFormDao.migrateFormToLatestVersion-->EXCEPTION : "
                                        + formId
                                        + " and pk_formslinear: "
                                        + pkFormsLinear
                                        + " with message:"
                                        + ex.getMessage());

            }

        } // end of while for rsetFormData.next()

        // update the form versio number, id in er_formslinear
        updateVersionInFormsLinear(formId, formLibVerId);

        // close statement and resultset
        rsetFormData.close();
        stmtFormData.close();



        if (stmtAccount != null)
            stmtAccount.close();

        if (stmtStudy != null)
            stmtStudy.close();

        if (stmtPat != null)
            stmtPat.close();

        }

          catch (Exception ex) {
            Rlog.fatal("saveForm",
                    "ExpToFormDao.migrateFormToLatestVersion -->EXCEPTION IN migrateFormToLatestVersion "
                            + ex);

        } finally {
            try {

                if (stmtFormData != null)
                    stmtFormData.close();

                if (stmtAccount != null)
                    stmtAccount.close();

                if (stmtStudy != null)
                    stmtStudy.close();

                if (stmtPat != null)
                    stmtPat.close();


            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

        //end of method

    }


    void updateVersionInFormsLinear(String formId, String newFormLibVersionPk)
    {
    	Connection conn = null;

        CallableStatement stmt= null;
        String errString = "";

    	try{
    			conn = getLongConnection();
    		 stmt = conn.prepareCall("{call PKG_FORMVER.SP_MIGRATE_FORMLIBVER(?,?,?)}");

    		  stmt.setString(1, formId);
              stmt.setString(2, newFormLibVersionPk);
              stmt.registerOutParameter(3,  java.sql.Types.VARCHAR);

              stmt.execute();
              errString = 	stmt.getString(3);

              if (! StringUtil.isEmpty(errString))
              {
            	  Rlog.fatal("saveForm",
                          "ExpToFormDao.updateVersionInFormsLinear -->EXCEPTION IN PKG_FORMVER.SP_MIGRATE_FORMLIBVER updateVersionInFormsLinear "
                                  + errString);
              }

    	}  catch (Exception ex) {
            Rlog.fatal("saveForm",
                    "ExpToFormDao.updateVersionInFormsLinear -->EXCEPTION IN updateVersionInFormsLinear "
                            + ex);

        } finally {
            try {

                if (stmt!= null)
                    stmt.close();

            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    //end of method
    }

    // end of class
}
