/*
 * Classname			FormLibBean
 * 
 * Version information : 1.0
 *
 * Date					 07/11/2003
 * 
 * Copyright notice :    Velos Inc
 */

package com.velos.eres.business.formLib.impl;

/* IMPORT STATEMENTS */

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* END OF IMPORT STATEMENTS */

/**
 * The form library CMP entity bean.<br>
 * <br>
 * 
 * @author Anu Khanna
 * 
 * @version 1.0, 07/11/2003
 * 
 * @ejbHome FormLibHome
 * 
 * @ejbRemote FormLibRObj
 */

@Entity
@Table(name = "er_formlib")
@NamedQuery(name = "findByUniqueFormName", query = "SELECT OBJECT(form) FROM FormLibBean form where fk_account = :fk_account and trim(lower(form_name)) = trim(lower(:form_name)) and record_type!= 'D' and trim(lower(form_linkto)) = trim(lower(:form_linkto))")
public class FormLibBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3688785881918353968L;

    /**
     * the form Library Id
     */
    public int formLibId;

    /**
     * the category Id
     */
    public Integer catLibId;

    /**
     * the account Id
     */
    public Integer accountId;

    /**
     * the form name
     */
    public String name;

    /**
     * the form description
     */
    public String desc;

    /**
     * the shared with flag
     */
    public String sharedWith;

    /**
     * the form status
     */
    public Integer status;

    /**
     * form link attribute
     */
    public String linkTo;

    /**
     * next form
     */

    public Integer formNext;

    /**
     * flag for the mode
     */
    public String formNextMode;

    /**
     * bold attribute
     */
    public Integer bold;

    /**
     * italics attribute
     */
    public Integer italics;

    /**
     * align attribute
     */
    public String align;

    /**
     * the alignment
     */
    public Integer underLine;

    /**
     * the color
     */
    public String color;

    /**
     * the background color
     */
    public String bgColor;

    /**
     * the Font
     */
    public String font;

    /**
     * the Font Size
     */
    public Integer fontSize;

    /**
     * fill flag
     */
    public Integer fillFlag;

    /**
     * 
     */
    public Integer xslRefresh;

    /**
     * user who last modified
     */
    public Integer lastModifiedBy;

    /**
     * the Creator
     */
    public Integer creator;

    /**
     * the Ip address of the creator
     */
    public String ipAdd;

    /**
     * Record Type
     */
    public String recordType;
    
    /**
     * eSign required
     */
    public String esignRequired;


    private boolean booleanForFrmStat;

    public FormLibBean() {

    }

    // GETTER SETTER METHODS

    public FormLibBean(int formLibId, String catLibId, String accountId,
            String name, String desc, String sharedWith, String status,
            String linkTo, String formNext, String formNextMode, String bold,
            String italics, String align, String underLine, String color,
            String bgColor, String font, String fontSize, String fillFlag,
            String xslRefresh, String lastModifiedBy, String creator,
            String ipAdd, String recordType,String esignReq) {
        super();
        // TODO Auto-generated constructor stub
        setFormLibId(formLibId);
        setCatLibId(catLibId);
        setAccountId(accountId);
        this.setFormLibName(name);
        this.setFormLibDesc(desc);
        this.setFormLibSharedWith(sharedWith);
        this.setFormLibStatus(status);
        this.setFormLibLinkTo(linkTo);
        this.setFormLibNext(formNext);
        setFormLibNextMode(formNextMode);
        this.setFormLibBold(bold);
        this.setFormLibItalics(italics);
        this.setFormLibAlign(align);
        this.setFormLibUnderLine(underLine);
        this.setFormLibColor(color);
        this.setFormLibBgColor(bgColor);
        setFormLibFont(font);
        this.setFormLibFontSize(fontSize);
        this.setFormLibFillFlag(fillFlag);
        this.setFormLibXslRefresh(xslRefresh);
        this.setLastModifiedBy(lastModifiedBy);
        this.setCreator(creator);
        setIpAdd(ipAdd);
        setRecordType(recordType);
        setEsignRequired(esignReq);
        // setBooleanForFrmStat(booleanForFrmStat);
    }

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_FORMLIB", allocationSize=1)
    @Column(name = "PK_FORMLIB")
    public int getFormLibId() {
        return this.formLibId;
    }

    public void setFormLibId(int id) {
        this.formLibId = id;
    }

    @Column(name = "FK_CATLIB")
    public String getCatLibId() {
        return StringUtil.integerToString(this.catLibId);

    }

    public void setCatLibId(String catLibId) {
        if (catLibId != null && catLibId.trim().length() > 0)
            this.catLibId = Integer.valueOf(catLibId);

    }

    @Column(name = "FK_ACCOUNT")
    public String getAccountId() {
        return StringUtil.integerToString(this.accountId);

    }

    public void setAccountId(String accountId) {
        if (accountId != null && accountId.trim().length() > 0)
            this.accountId = Integer.valueOf(accountId);

    }

    @Column(name = "FORM_NAME")
    public String getFormLibName() {
        return this.name;
    }

    public void setFormLibName(String name) {

        this.name = name;

    }

    @Column(name = "FORM_DESC")
    public String getFormLibDesc() {
        return this.desc;
    }

    public void setFormLibDesc(String desc) {

        this.desc = desc;

    }

    @Column(name = "FORM_SHAREDWITH")
    public String getFormLibSharedWith() {
        return this.sharedWith;
    }

    public void setFormLibSharedWith(String sharedWith) {

        this.sharedWith = sharedWith;

    }

    @Column(name = "FORM_STATUS")
    public String getFormLibStatus() {
        return StringUtil.integerToString(this.status);

    }

    public void setFormLibStatus(String status) {
        if (status != null)
            this.status = Integer.valueOf(status);

    }

    @Column(name = "FORM_LINKTO")
    public String getFormLibLinkTo() {
        return this.linkTo;
    }

    public void setFormLibLinkTo(String linkTo) {

        this.linkTo = linkTo;

    }

    @Column(name = "FORM_NEXT")
    public String getFormLibNext() {
        return StringUtil.integerToString(this.formNext);

    }

    public void setFormLibNext(String formNext) {
        if (formNext != null)
            this.formNext = Integer.valueOf(formNext);

    }

    @Column(name = "FORM_NEXTMODE")
    public String getFormLibNextMode() {
        return this.formNextMode;
    }

    public void setFormLibNextMode(String formNextMode) {

        this.formNextMode = formNextMode;

    }

    @Column(name = "FORM_BOLD")
    public String getFormLibBold() {
        return StringUtil.integerToString(this.bold);
    }

    public void setFormLibBold(String bold) {
        if (bold != null)
            this.bold = Integer.valueOf(bold);
    }

    @Column(name = "FORM_ITALICS")
    public String getFormLibItalics() {
        return StringUtil.integerToString(this.italics);
    }

    public void setFormLibItalics(String italics) {
        if (italics != null)
            this.italics = Integer.valueOf(italics);
    }

    @Column(name = "FORM_ALIGN")
    public String getFormLibAlign() {
        return this.align;
    }

    public void setFormLibAlign(String align) {

        this.align = align;

    }

    @Column(name = "FORM_UNDERLINE")
    public String getFormLibUnderLine() {
        return StringUtil.integerToString(this.underLine);
    }

    public void setFormLibUnderLine(String underLine) {

        if (underLine != null)
            this.underLine = Integer.valueOf(underLine);

    }

    @Column(name = "FORM_COLOR")
    public String getFormLibColor() {
        return this.color;
    }

    public void setFormLibColor(String color) {

        this.color = color;

    }

    @Column(name = "FORM_BGCOLOR")
    public String getFormLibBgColor() {
        return this.bgColor;
    }

    public void setFormLibBgColor(String bgColor) {

        this.bgColor = bgColor;

    }

    @Column(name = "FORM_FONT")
    public String getFormLibFont() {
        return this.font;
    }

    public void setFormLibFont(String font) {

        this.font = font;

    }

    @Column(name = "FLD_FONTSIZE")
    public String getFormLibFontSize() {
        return StringUtil.integerToString(this.fontSize);
    }

    public void setFormLibFontSize(String fontSize) {
        if (fontSize != null)
            this.fontSize = Integer.valueOf(fontSize);
    }

    @Column(name = "FORM_FILLFLAG")
    public String getFormLibFillFlag() {
        return StringUtil.integerToString(this.fillFlag);
    }

    public void setFormLibFillFlag(String fillFlag) {
        if (fillFlag != null)
            this.fillFlag = Integer.valueOf(fillFlag);
    }

    @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    public void setCreator(String creator) {
        if (creator != null)
            this.creator = Integer.valueOf(creator);
    }

    @Column(name = "FORM_XSLREFRESH")
    public String getFormLibXslRefresh() {
        return StringUtil.integerToString(this.xslRefresh);
    }

    public void setFormLibXslRefresh(String xslRefresh) {
        if (xslRefresh != null)
            this.xslRefresh = Integer.valueOf(xslRefresh);
        Rlog.debug("formlib", "in setFormLibXslRefresh 2 sonika 2"
                + this.xslRefresh);
    }

    @Column(name = "LAST_MODIFIED_BY")
    public String getLastModifiedBy() {
        return StringUtil.integerToString(this.lastModifiedBy);
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        if (lastModifiedBy != null)
            this.lastModifiedBy = Integer.valueOf(lastModifiedBy);
    }

    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    @Column(name = "RECORD_TYPE")
    public String getRecordType() {
        return this.recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    @Transient
    public boolean getBooleanForFrmStat() {
        Rlog.debug("formlib", "FormLibStateKeeper.getBooleanFormStatus() "
                + this.booleanForFrmStat);
        return this.booleanForFrmStat;
    }

    public void setBooleanForFrmStat(boolean booleanForFrmStat) {
        this.booleanForFrmStat = booleanForFrmStat;
    }

    // END OF GETTER SETTER METHODS

 
 

    /**
     * Saves the form library details in the FormLibStateKeeper to the database
     * and returns 0 if the update is successful and -2 if it fails
     * 
     * @param flsk
     *            form library State Keeper with the updated form library
     *            Information
     * 
     * @return 0 - if update is successful <br>
     *         -2 - if update fails
     * 
     * @see FormLibStateKeeper
     * 
     */
    public int updateFormLib(FormLibBean flsk) {
        try {

            setCatLibId(flsk.getCatLibId());
            setAccountId(flsk.getAccountId());
            setFormLibName(flsk.getFormLibName());
            setFormLibDesc(flsk.getFormLibDesc());
            setFormLibSharedWith(flsk.getFormLibSharedWith());
            setFormLibStatus(flsk.getFormLibStatus());
            setFormLibLinkTo(flsk.getFormLibLinkTo());
            setFormLibNext(flsk.getFormLibNext());
            setFormLibNextMode(flsk.getFormLibNextMode());
            setFormLibBold(flsk.getFormLibBold());
            setFormLibItalics(flsk.getFormLibItalics());
            setFormLibAlign(flsk.getFormLibAlign());
            setFormLibUnderLine(flsk.getFormLibUnderLine());
            setFormLibColor(flsk.getFormLibColor());
            setFormLibBgColor(flsk.getFormLibBgColor());
            setFormLibFont(flsk.getFormLibFont());
            setFormLibFontSize(flsk.getFormLibFontSize());
            setFormLibFillFlag(flsk.getFormLibFillFlag());
            setCreator(flsk.getCreator());
            setFormLibXslRefresh(flsk.getFormLibXslRefresh());
            setLastModifiedBy(flsk.getLastModifiedBy());
            setRecordType(flsk.getRecordType());
            setIpAdd(flsk.getIpAdd());
            setEsignRequired(flsk.getEsignRequired());
            
            Rlog.debug("formlib", "FormLibBean.updateFormLib");
            return 0;
        } catch (Exception e) {
            Rlog.fatal("formlib", " error in FormLibBean.updateFormLib");
            return -2;
        }
    }

    @Column(name = "FORM_ESIGNREQ")
	public String getEsignRequired() {
		return esignRequired;
	}

	public void setEsignRequired(String esignRequired) {
		this.esignRequired = esignRequired;
	}

}// end of class
