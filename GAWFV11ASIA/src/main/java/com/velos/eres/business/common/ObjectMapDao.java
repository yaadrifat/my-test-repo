package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.velos.eres.service.util.Rlog;

public class ObjectMapDao extends CommonDAO implements java.io.Serializable {

	private static final long serialVersionUID = 166051242972940822L;
	private static final String GetObjectSql = "select TABLE_NAME, TABLE_PK from ER_OBJECT_MAP where SYSTEM_ID = ? ";
	private String tableName = null;
	private Integer tablePk = 0;
	public void getObjectMap(String oid) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(GetObjectSql);
            pstmt.setString(1, oid);
            rs = pstmt.executeQuery();
            while (rs.next()) {
            	tableName = rs.getString(1);
            	tablePk = rs.getInt(2);
            }
        } catch(Exception e) {
        	Rlog.fatal("objectMap", "Exception in ObjectMapDao.getObjectMap:"+e);
        } finally {
        	try { if (rs != null) rs.close(); } catch (Exception e) {}
        	try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
        	try { if (conn != null) conn.close(); } catch (Exception e) {}
        }
	}
	public String getTableName() { return tableName; }
	public int getTablePk() { return tablePk; }
}
