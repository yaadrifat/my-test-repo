//sajal dt 03/23/2001

package com.velos.eres.business.common;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * Description of the Class
 *
 * @author vishal
 * @created May 12, 2005
 */
public class GroupDao extends CommonDAO implements java.io.Serializable {
    private ArrayList grpIds;

    private ArrayList accIds;

    private ArrayList grpNames;

    private ArrayList grpDescs;

    private int cRows;

    private ArrayList grpStudyTeamRoles;
    /**
     * Constructor for the GroupDao object
     */
    public GroupDao() {
        grpIds = new ArrayList();
        accIds = new ArrayList();
        grpNames = new ArrayList();
        grpDescs = new ArrayList();
    }

    // Getter and Setter methods

    /**
     * Gets the grpIds attribute of the GroupDao object
     *
     * @return The grpIds value
     */
    public ArrayList getGrpIds() {
        Rlog.debug("group", "GroupDao.getGrpIds size " + this.grpIds.size());
        return this.grpIds;
    }

    /**
     * Sets the grpIds attribute of the GroupDao object
     *
     * @param grpIds
     *            The new grpIds value
     */
    public void setGrpIds(ArrayList grpIds) {
        this.grpIds = grpIds;
    }

    /**
     * Gets the accIds attribute of the GroupDao object
     *
     * @return The accIds value
     */
    public ArrayList getAccIds() {
        return this.accIds;
    }

    /**
     * Sets the accIds attribute of the GroupDao object
     *
     * @param accIds
     *            The new accIds value
     */
    public void setAccIds(ArrayList accIds) {
        this.accIds = accIds;
    }

    /**
     * Gets the grpNames attribute of the GroupDao object
     *
     * @return The grpNames value
     */
    public ArrayList getGrpNames() {
        return this.grpNames;
    }

    /**
     * Sets the grpNames attribute of the GroupDao object
     *
     * @param grpNames
     *            The new grpNames value
     */
    public void setGrpNames(ArrayList grpNames) {
        this.grpNames = grpNames;
    }

    /**
     * Gets the grpDescs attribute of the GroupDao object
     *
     * @return The grpDescs value
     */
    public ArrayList getGrpDescs() {
        return this.grpDescs;
    }

    /**
     * Sets the grpDescs attribute of the GroupDao object
     *
     * @param grpDescs
     *            The new grpDescs value
     */
    public void setGrpDescs(ArrayList grpDescs) {
        this.grpDescs = grpDescs;
    }

    /**
     * Gets the cRows attribute of the GroupDao object
     *
     * @return The cRows value
     */
    public int getCRows() {
        return this.cRows;
    }

    /**
     * Sets the cRows attribute of the GroupDao object
     *
     * @param cRows
     *            The new cRows value
     */
    public void setCRows(int cRows) {
        this.cRows = cRows;
    }

    /**
     * Sets the grpIds attribute of the GroupDao object
     *
     * @param grpId
     *            The new grpIds value
     */
    public void setGrpIds(Integer grpId) {
        grpIds.add(grpId);
    }

    /**
     * Sets the accIds attribute of the GroupDao object
     *
     * @param accId
     *            The new accIds value
     */
    public void setAccIds(String accId) {
        accIds.add(accId);
    }

    /**
     * Sets the grpNames attribute of the GroupDao object
     *
     * @param grpName
     *            The new grpNames value
     */
    public void setGrpNames(String grpName) {
        grpNames.add(grpName);
    }

    /**
     * Sets the grpDescs attribute of the GroupDao object
     *
     * @param grpDesc
     *            The new grpDescs value
     */
    public void setGrpDescs(String grpDesc) {
        grpDescs.add(grpDesc);
    }

    // end of getter and setter methods

    /**
     * Gets the groupValues attribute of the GroupDao object
     *
     * @param accountId
     *            Description of the Parameter
     */

    //Query modified by Manimaran for Enh#U11.
    public void getGroupValues(int accountId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn
                    .prepareStatement("select PK_GRP, FK_ACCOUNT, GRP_NAME, GRP_DESC from er_grps where FK_ACCOUNT =? and GRP_HIDDEN <> 1 order by LOWER(GRP_NAME)");
            pstmt.setInt(1, accountId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setGrpIds(new Integer(rs.getInt("PK_GRP")));
                setAccIds(new Integer(rs.getInt("FK_ACCOUNT")).toString());
                setGrpNames(rs.getString("GRP_NAME"));
                setGrpDescs(rs.getString("GRP_DESC"));
                rows++;
                Rlog.debug("group", "GroupDao.getGroupValues rows " + rows);
                Rlog.debug("group", "GroupDao.getGroupValues grpIds size "
                        + grpIds.size());
                Rlog.debug("group", "GroupDao.getGroupValues accIds size "
                        + accIds.size());
                Rlog.debug("group", "GroupDao.getGroupValues grpNames size "
                        + grpNames.size());
                Rlog.debug("group", "GroupDao.getGroupValues grpDescs size "
                        + grpDescs.size());

            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("group",
                    "GroupDao.getGroupValues EXCEPTION IN FETCHING FROM Group table"
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }


    
    
    public int getGrpPk(int accntid) {
   
        int pk_grp = 0 ;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn
                    .prepareStatement("select PK_GRP, FK_ACCOUNT from er_grps where FK_ACCOUNT =? and GRP_HIDDEN <> 1 and GRP_NAME='Admin' ");
            pstmt.setInt(1, accntid);

            ResultSet rs = pstmt.executeQuery();
            while(rs.next()){
              pk_grp = rs.getInt("PK_GRP");	
            }
            }catch(Exception ex){
            	System.out.println(ex);
            }

           return pk_grp;
            
        
            	
    }
    
    /**
     * Returns the number of groups with the same group name as this one
     *
     * @param groupName
     *            The Group Name that is to be checked in the database
     * @param accId
     *            The account Id of the logged in user.
     * @return int The number of groups with the same name as groupName
     */
    public int getCount(String groupName, String accId) {
        int count = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            int accountId = StringUtil.stringToNum(accId);
            conn = getConnection();

            pstmt = conn.prepareStatement("select count(*) as count "
                    + "from er_grps " + "where upper(grp_name) = upper(?)"
                    + "and fk_account = ?");
            pstmt.setString(1, groupName);
            pstmt.setInt(2, accountId);

            ResultSet rs = pstmt.executeQuery();

            rs.next();
            count = rs.getInt("count");

        } catch (SQLException ex) {
            Rlog.fatal("group",
                    "GroupDao.getCount EXCEPTION IN FETCHING FROM Group table"
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }
        return count;
    }

    /**
     * Calls Stored Procedure to grant super user right to all group members.
     * super user right means access to all studies in an account
     *
     * @param groupId
     *            Description of the Parameter
     * @param accId
     *            Description of the Parameter
     * @param creator
     *            Description of the Parameter
     * @param ipAdd
     *            Description of the Parameter
     * @param rights
     *            Description of the Parameter
     * @return Description of the Return Value
     *
     */

    /* public int grantSupUser(int groupId, String accId, String creator,
            String ipAdd, String rights) {
        CallableStatement cstmt = null;

        Connection conn = null;
        int success = -1;
        try {

            conn = getConnection();
            cstmt = conn
                    .prepareCall("{call PKG_SUPUSER.SP_GRANT_SUPUSER(?,?,?,?,?,?)}");
            cstmt.setInt(1, groupId);
            cstmt.setInt(2, StringUtil.stringToNum(accId));
            cstmt.setInt(3, StringUtil.stringToNum(creator));
            cstmt.setString(4, ipAdd);
            cstmt.setString(5, rights);
            cstmt.registerOutParameter(6, java.sql.Types.INTEGER);

            cstmt.execute();
            success = cstmt.getInt(6);
            return success;

        }
        // end of try
        catch (Exception ex) {
            Rlog.fatal("group", "GroupDao Exception in calling grantSupUser : "
                    + ex);
            return success;
        } finally {
            try {
                if (cstmt != null) {
                    cstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    } */


    /**
     * Calls Stored Procedure to grant super user right to all group members.
     * super user right means access to all studies in an account
     *
     * @param groupId
     *            Description of the Parameter
     * @param accId
     *            Description of the Parameter
     * @param creator
     *            Description of the Parameter
     * @param ipAdd
     *            Description of the Parameter
     * @param rights
     *            Description of the Parameter
     * @return Description of the Return Value
     *
     */
  /*
    public int grantSupUserForUser(String userId, String groupId,
            String creator, String ipAdd) {
        CallableStatement cstmt = null;

        Connection conn = null;
        int success = -1;
        try {

            conn = getConnection();
            cstmt = conn
                    .prepareCall("{call PKG_SUPUSER.SP_GRANT_SUPUSER_FORUSER(?,?,?,?,?)}");
            cstmt.setInt(1, StringUtil.stringToNum(userId));
            cstmt.setInt(2, StringUtil.stringToNum(groupId));
            cstmt.setInt(3, StringUtil.stringToNum(creator));
            cstmt.setString(4, ipAdd);
            cstmt.registerOutParameter(5, java.sql.Types.INTEGER);

            cstmt.execute();
            success = cstmt.getInt(5);
            return success;

        }
        // end of try
        catch (Exception ex) {
            Rlog
                    .fatal("group",
                            "GroupDao Exception in calling grantSupUserFOrUSEr : "
                                    + ex);
            return success;
        } finally {
            try {
                if (cstmt != null) {
                    cstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    } */

    /**
     * Calls Stored Procedure to grant super user right for a Study.
     *
     * @param accId
     *            AccountId
     * @param studyId
     *            Description of the Parameter
     * @param creator
     *            Description of the Parameter
     * @param ipAdd
     *            Description of the Parameter
     * @return Description of the Return Value
     *
     */

   /*  public int grantSupUserForStudy(String accId, String studyId,
            String creator, String ipAdd) {
        CallableStatement cstmt = null;

        Connection conn = null;
        int success = -1;
        try {

            conn = getConnection();
            cstmt = conn
                    .prepareCall("{call PKG_SUPUSER. SP_GRANT_SUPUSER_FORSTUDY(?,?,?,?,?)}");
            cstmt.setInt(1, StringUtil.stringToNum(accId));
            cstmt.setInt(2, StringUtil.stringToNum(studyId));
            cstmt.setInt(3, StringUtil.stringToNum(creator));
            cstmt.setString(4, ipAdd);
            cstmt.registerOutParameter(5, java.sql.Types.INTEGER);

            cstmt.execute();
            success = cstmt.getInt(5);
            return success;

        }
        // end of try
        catch (Exception ex) {
            Rlog.fatal("group",
                    "GroupDao Exception in calling grantSupUserForStudy : "
                            + ex);
            return success;
        } finally {
            try {
                if (cstmt != null) {
                    cstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }*/

    /**
     * calls Stored Procedure to revoke super user rights from all group users.
     * The rights will revoked only if user does not exist in any other 'super
     * user group' of that account.
     *
     * @param groupId
     *            Description of the Parameter
     * @param accId
     *            Description of the Parameter
     * @return Description of the Return Value
     *
     */

    /*public int revokeSupUser(int groupId, String accId) {
        CallableStatement cstmt = null;

        Connection conn = null;
        int success = -1;
        try {

            conn = getConnection();
            cstmt = conn
                    .prepareCall("{call PKG_SUPUSER.SP_REVOKE_SUPUSER(?,?,?)}");
            cstmt.setInt(1, groupId);
            cstmt.setInt(2, StringUtil.stringToNum(accId));
            cstmt.registerOutParameter(3, java.sql.Types.INTEGER);
            cstmt.execute();
            success = cstmt.getInt(3);
            return success;

        }
        // end of try
        catch (Exception ex) {
            Rlog.fatal("group",
                    "GroupDao Exception in calling revokeSupUser : " + ex);
            return success;
        } finally {
            try {
                if (cstmt != null) {
                    cstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }*/

    /**
     * calls Stored Procedure to revoke super user rights from all group users
     * for a study
     *
     * @param studyId
     *            Internal Study Id
     * @return -1 if failure
     *
     */

    /*public int revokeSupUserForStudy(int studyId, String account) {
        CallableStatement cstmt = null;

        Connection conn = null;
        int success = -1;
        try {

            conn = getConnection();
            cstmt = conn
                    .prepareCall("{call PKG_SUPUSER.SP_REVOKE_SUPUSER_FORSTUDY(?,?,?)}");
            cstmt.setInt(1, studyId);
            cstmt.setString(2, account);
            cstmt.registerOutParameter(3, java.sql.Types.INTEGER);
            cstmt.execute();
            success = cstmt.getInt(3);
            return success;

        }
        // end of try
        catch (Exception ex) {
            Rlog.fatal("group",
                    "GroupDao Exception in calling revokeSupUserFORSTUDY : "
                            + ex);
            return success;
        } finally {
            try {
                if (cstmt != null) {
                    cstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }*/

    /**
     * Calls Stored Procedure to grant super user right to all group members.
     * super user right means access to all studies in an account
     *
     * @param groupId
     *            Description of the Parameter
     * @param lastModifiedBy
     *            Description of the Parameter
     * @param ipAdd
     *            Description of the Parameter
     * @param rights
     *            Description of the Parameter
     * @return Description of the Return Value
     *
     */

    /*
    public int updateSupUserRights(int groupId, String lastModifiedBy,
            String ipAdd, String rights) {
        CallableStatement cstmt = null;

        Connection conn = null;
        int success = -1;
        try {

            conn = getConnection();

            cstmt = conn
                    .prepareCall("{call PKG_SUPUSER.SP_UPDATE_SUPUSER_RIGHTS(?,?,?,?,?)}");

            cstmt.setInt(1, groupId);
            cstmt.setInt(2, StringUtil.stringToNum(lastModifiedBy));
            cstmt.setString(3, ipAdd);
            cstmt.setString(4, rights);

            cstmt.registerOutParameter(5, java.sql.Types.INTEGER);

            cstmt.execute();
            success = cstmt.getInt(5);
            return success;
        }
        // end of try
        catch (Exception ex) {
            Rlog
                    .fatal("group",
                            "GroupDao Exception in calling updateSupUserRights : "
                                    + ex);
            return success;
        } finally {
            try {
                if (cstmt != null) {
                    cstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    } */

    // /

    /**
     * Calls tored Procedure to revoke super user rights of a user. super user
     * right means access to all studies in an account The rights will revoked
     * only if user does not exist in any other 'super user group' of that
     * account.
     *
     * @param user
     *            Description of the Parameter
     * @param group
     *            Description of the Parameter
     * @return Description of the Return Value
     *
     */

    /*
    public int revokeSupUserRightsFromUser(String user, String group,
            String creator, String ipAdd) {

        CallableStatement cstmt = null;

        Connection conn = null;
        int success = -1;
        try {

            conn = getConnection();

            cstmt = conn
                    .prepareCall("{call PKG_SUPUSER.SP_REVOKE_SUPUSER_FORUSER(?,?,?,?,?)}");

            cstmt.setInt(1, StringUtil.stringToNum(user));
            cstmt.setInt(2, StringUtil.stringToNum(group));
            cstmt.setString(3, creator);
            cstmt.setString(4, ipAdd);
            cstmt.registerOutParameter(5, java.sql.Types.INTEGER);

            cstmt.execute();
            success = cstmt.getInt(5);
            return success;
        }
        // end of try
        catch (Exception ex) {
            Rlog.fatal("group",
                    "GroupDao Exception in calling revokeSupUserRightsFromUser : "
                            + ex);
            return success;
        } finally {
            try {
                if (cstmt != null) {
                    cstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }
    */

    /**
     * Returns the study super user rights for a user's default group
     *
     * @param p_user
     *            The user PK
      * @return String The String of rights
     */
    public static String getDefaultStudySuperUserRights(String p_user) {
        String rights = "" ;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {

            conn = getConnection();

            pstmt = conn.prepareStatement("select grp_supusr_rights "
                    + " from er_grps,er_user where  pk_user = ? and fk_grp_default = pk_grp");
            pstmt.setString(1, p_user);


            ResultSet rs = pstmt.executeQuery();

            rs.next();
            rights = rs.getString("grp_supusr_rights");

        } catch (SQLException ex) {
            Rlog.fatal("group",
                    "GroupDao.getDefaultStudySuperUserRights(String p_user) EXCEPTION IN FETCHING FROM Group table"
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }
        return rights;
    }


    /**
     * This mehod updates the role's default right from er_ctrl table to the er_grps table's super user rights
     * @param roleId
     * @param grpId
     * @return 0 on successful updates
     */

    public int copyRoleDefRightsToGrpSupUserRights(int roleId, int grpId){
       int success = 0;


       PreparedStatement pstmt = null;
       Connection conn = null;
       try {
           conn = getConnection();

           pstmt = conn.prepareStatement("update er_grps set GRP_SUPUSR_RIGHTS = ( "
        		   + " select ctrl_value from er_ctrltab where ctrl_key = ( "
        		   + " select codelst_subtyp from er_codelst where pk_codelst = ?))"
        		   + " where PK_GRP=? ");


           pstmt.setInt(1, roleId);
           pstmt.setInt(2, grpId);

           ResultSet rs = pstmt.executeQuery();

           rs.next();


       } catch (SQLException ex) {
           Rlog.fatal("group",
                   "GroupDao.copyRoleDefRightsToGrpSupUserRights() EXCEPTION IN updating Group table"
                           + ex);
       } finally {
           try {
               if (pstmt != null) {
                   pstmt.close();
               }
           } catch (Exception e) {
           }
           try {
               if (conn != null) {
                   conn.close();
               }
           } catch (Exception e) {
           }

       }
       return success;



    }

	public ArrayList getGrpStudyTeamRoles() {
		return grpStudyTeamRoles;
	}

	public void setGrpStudyTeamRoles(ArrayList grpStudyTeamRoles) {
		this.grpStudyTeamRoles = grpStudyTeamRoles;
	}

	public void setGrpStudyTeamRoles(String grpStudyTeamRole) {
		this.grpStudyTeamRoles.add(grpStudyTeamRole);
	}

	
    /**
     * This mehod gets the study team role linked with the the user's default group (only may have a value if the group is a super user group)
     * @param userId
     * @return The Role if specified, empty string if there is no role specified or group is not a super user group
     */

    public static int getUserDefaultGroupStudyTeamRole(int userId){
        
       int role = 0;

       PreparedStatement pstmt = null;
       Connection conn = null;
       try {
           conn = getConnection();

           pstmt = conn.prepareStatement(" select nvl(fk_codelst_st_role,'0') role from er_grps,er_user where pk_user = ? and fk_grp_default = pk_grp");

           pstmt.setInt(1, userId);
          
           ResultSet rs = pstmt.executeQuery();

           rs.next();
           
           role = rs.getInt("role");


       } catch (SQLException ex) {
           Rlog.fatal("group",
                   "GroupDao.getUserDefaultGroupStudyTeamRole() EXCEPTION :"
                           + ex);
       } finally {
           try {
               if (pstmt != null) {
                   pstmt.close();
               }
           } catch (Exception e) {
           }
           try {
               if (conn != null) {
                   conn.close();
               }
           } catch (Exception e) {
           }

       }
       return role;

    }
    
	
    // end of class

}