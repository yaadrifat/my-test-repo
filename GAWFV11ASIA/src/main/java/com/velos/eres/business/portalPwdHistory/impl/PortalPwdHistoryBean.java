/*
 * Classname : PortalPwdHistoryBean
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Raviesh
 */

package com.velos.eres.business.portalPwdHistory.impl;

/* Import Statements */
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.Security;
import com.velos.eres.service.util.StringUtil;

/* End of Import Statements */

/**
 * The PortalPwdHistoryBean CMP entity bean.
 * 
 * @author Raviesh
 */
@Entity
@Table(name = "ER_PORTALPWD_HISTORY")
@NamedQueries( {
        @NamedQuery(name = "portalLoginRotation", query = "SELECT user FROM PortalPwdHistoryBean user where portalId = ? and portalLogin = ? order by createdOn Desc"),
   		@NamedQuery(name = "portalRotation", query = "SELECT user FROM PortalPwdHistoryBean user where portalId = ? and fkAccount = ? and portalLogin IS NULL order by createdOn Desc")
   		})
   		
public class PortalPwdHistoryBean implements Serializable {

	private static final long serialVersionUID = 3256725069878539570L;

    @Id 
    @Column(name = "PK_PORTALPWD_HISTORY", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long pwdId;
    
    @Column(name = "FK_PORTAL")
    private Integer portalId;
    
	@Column(name = "FK_PORTAL_LOGIN")
    private Integer portalLogin;
	
    @Column(name = "FK_ACCOUNT")
    private Integer fkAccount;
    
    @Column(name = "USR_PWD")
    private String userPwd;
    
    @Column(name = "RID")
    private Integer rid;
    
    @Column(name = "CREATOR")
    private Integer creator;
    
    @Column(name = "IP_ADD")
    private String ipAddress;
    
    @Column(name = "CREATED_ON")
    private Date createdOn;
    
	public long getPwdId() {
		return pwdId;
	}
	public void setPwdId(long pwdId) {
		this.pwdId = pwdId;
	}
	public Integer getPortalId() {
		return portalId;
	}
	public void setPortalId(Integer portalId) {
		this.portalId = portalId;
	}
	public Integer getPortalLogin() {
		return portalLogin;
	}
	public void setPortalLogin(Integer portalLogin) {
		this.portalLogin = portalLogin;
	}
	public Integer getFkAccount() {
		return fkAccount;
	}
	public void setFkAccount(Integer fkAccount) {
		this.fkAccount = fkAccount;
	}
	public String getUserPwd() {
		return userPwd;
	}
	public void setUserPwd(String userPwd) {
		this.userPwd = userPwd;
	}
	public Integer getRid() {
		return rid;
	}
	public void setRid(Integer rid) {
		this.rid = rid;
	}
	public Integer getCreator() {
		return creator;
	}
	public void setCreator(Integer creator) {
		this.creator = creator;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
    
}
