/*
 * Classname : UserDao 
 * 
 * Version information: 1.0 
 *
 * Date: 03/26/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Sajal, Sonia
 */

package com.velos.eres.business.common;

import java.sql.CallableStatement;//JM
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.util.EnvUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.user.UserJB;

/* End of Import Statements */

/**
 * The User Dao.<br>
 * <br>
 * This class has various methods to get resultsets of user data for different
 * parameters.
 * 
 * @author Sajal, Sonia
 * @vesion 1.0 03/26/2001
 */

public class UserDao extends CommonDAO implements java.io.Serializable {

    private ArrayList usrIds;

    private ArrayList usrAccIds;

    private ArrayList usrSiteIds;

    private ArrayList usrSiteNames;

    private ArrayList usrLastNames;

    private ArrayList usrFirstNames;

    private ArrayList usrMidNames;

    private ArrayList usrWrkExps;

    private ArrayList usrPhaseInvs;

    private ArrayList usrSessTimes;

    private ArrayList usrLogNames;

    private ArrayList usrPwds;

    private ArrayList usrSecQues;

    private ArrayList usrAns;

    private ArrayList usrStats;

    private ArrayList usrTypes; // JM

    private ArrayList usrSpls;

    private ArrayList usrDefGrps;

    private ArrayList usrPerAdds;

    private ArrayList usrJobTypes;

    private ArrayList usrGrpIds;

    private ArrayList usrCodes;

    private ArrayList usrLoggedInFlags;

    private ArrayList usrGrpNames;

    private ArrayList usrStudyNumbers;
    
    private ArrayList usrLoginModuleIdList;
    
    private ArrayList usrLoginModuleMapList;
    
    

    /*
     * Array list to store whether the user to whom a copy of a study is given
     * is is internal or external 1 is stored for internal users and 2 for
     * external
     */
    private ArrayList inexUsers;

    private int cRows;

   

	public UserDao() {
        usrIds = new ArrayList();
        usrAccIds = new ArrayList();
        usrSiteIds = new ArrayList();
        usrSiteNames = new ArrayList();
        usrLastNames = new ArrayList();
        usrFirstNames = new ArrayList();
        usrMidNames = new ArrayList();
        usrWrkExps = new ArrayList();
        usrPhaseInvs = new ArrayList();
        usrSessTimes = new ArrayList();
        usrLogNames = new ArrayList();
        usrPwds = new ArrayList();
        usrSecQues = new ArrayList();
        usrAns = new ArrayList();
        usrTypes = new ArrayList();// JM
        usrStats = new ArrayList();
        usrSpls = new ArrayList();
        usrDefGrps = new ArrayList();
        usrPerAdds = new ArrayList();
        usrJobTypes = new ArrayList();
        usrGrpIds = new ArrayList();
        inexUsers = new ArrayList();
        usrCodes = new ArrayList();
        usrLoggedInFlags = new ArrayList();
        usrGrpNames = new ArrayList();
        usrStudyNumbers = new ArrayList();
        usrLoginModuleIdList=new ArrayList();
        usrLoginModuleMapList=new ArrayList();

    }

    // Getter and Setter methods
 
    public ArrayList getUsrIds() {
        return this.usrIds;
    }

    public void setUsrIds(ArrayList usrIds) {
        this.usrIds = usrIds;
    }

    public ArrayList getUsrAccIds() {
        return this.usrAccIds;
    }

    public void setUsrAccIds(ArrayList usrAccIds) {
        this.usrAccIds = usrAccIds;
    }

    public ArrayList getUsrSiteIds() {
        return this.usrSiteIds;
    }

    public void setUsrSiteIds(ArrayList usrSiteIds) {
        this.usrSiteIds = usrSiteIds;
    }

    public ArrayList getUsrSiteNames() {
        return this.usrSiteNames;
    }

    public void setUsrSiteNames(ArrayList usrSiteNames) {
        this.usrSiteNames = usrSiteNames;
    }

    public ArrayList getUsrLastNames() {
        return this.usrLastNames;
    }

    public void setUsrLastNames(ArrayList usrLastNames) {
        this.usrLastNames = usrLastNames;
    }

    public ArrayList getUsrFirstNames() {
        return this.usrFirstNames;
    }

    public void setUsrFirstNames(ArrayList usrFirstNames) {
        this.usrFirstNames = usrFirstNames;
    }

    public ArrayList getUsrMidNames() {
        return this.usrMidNames;
    }

    public void setUsrMidNames(ArrayList usrMidNames) {
        this.usrMidNames = usrMidNames;
    }

    public ArrayList getUsrLoggedInFlags() {
        return this.usrLoggedInFlags;
    }

    public void setUsrLoggedInFlags(ArrayList loggedInFlags) {
        this.usrLoggedInFlags = loggedInFlags;
    }

    public ArrayList getUsrWrkExps() {
        return this.usrWrkExps;
    }

    public void setUsrWrkExps(ArrayList usrWrkExps) {
        this.usrWrkExps = usrWrkExps;
    }

    public ArrayList getUsrPhaseInvs() {
        return this.usrPhaseInvs;
    }

    public void setUsrPhaseInvs(ArrayList usrPhaseInvs) {
        this.usrPhaseInvs = usrPhaseInvs;
    }

    public ArrayList getUsrSessTimes() {
        return this.usrSessTimes;
    }

    public void setUsrSessTimes(ArrayList usrSessTimes) {
        this.usrSessTimes = usrSessTimes;
    }

    public ArrayList getUsrLogNames() {
        return this.usrLogNames;
    }

    public void setUsrLogNames(ArrayList usrLogNames) {
        this.usrLogNames = usrLogNames;
    }

    public ArrayList getUsrPwds() {
        return this.usrPwds;
    }

    public void setUsrPwds(ArrayList usrPwds) {
        this.usrPwds = usrPwds;
    }

    public ArrayList getUsrSecQues() {
        return this.usrSecQues;
    }

    public void setUsrSecQues(ArrayList usrSecQues) {
        this.usrSecQues = usrSecQues;
    }

    public ArrayList getUsrAns() {
        return this.usrAns;
    }

    public void setUsrAns(ArrayList usrAns) {
        this.usrAns = usrAns;
    }

    public ArrayList getUsrStats() {
        return this.usrStats;
    }

    public void setUsrStats(ArrayList usrStats) {
        this.usrStats = usrStats;
    }

    // JM: 25/08/05
    public ArrayList getUsrTypes() {
        return this.usrTypes;
    }

    public void setUsrTypes(ArrayList usrTypes) {
        this.usrTypes = usrTypes;
    }

    public ArrayList getUsrSpls() {
        return this.usrSpls;
    }

    public void setUsrSpls(ArrayList usrSpls) {
        this.usrSpls = usrSpls;
    }

    public ArrayList getUsrDefGrps() {
        return this.usrDefGrps;
    }

    public void setUsrDefGrps(ArrayList usrDefGrps) {
        this.usrDefGrps = usrDefGrps;
    }

    public ArrayList getUsrPerAdds() {
        return this.usrPerAdds;
    }

    public void setUsrPerAdds(ArrayList usrPerAdds) {
        this.usrPerAdds = usrPerAdds;
    }

    public ArrayList getUsrJobTypes() {
        return this.usrJobTypes;
    }

    public void setUsrJobTypes(ArrayList usrJobTypes) {
        this.usrJobTypes = usrJobTypes;
    }

    public ArrayList getUsrGrpIds() {
        return this.usrGrpIds;
    }

    public void setUsrGrpIds(ArrayList usrGrpIds) {
        this.usrGrpIds = usrGrpIds;
    }

    public ArrayList getUsrCodes() {
        return this.usrCodes;
    }

    public void setUsrCodes(ArrayList usrCodes) {
        this.usrCodes = usrCodes;
    }

    public ArrayList getUsrGrpNames() {
        return this.usrGrpNames;
    }

    public void setUsrGrpNames(ArrayList usrGrpNames) {
        this.usrGrpNames = usrGrpNames;
    }

    public ArrayList getUsrStudyNumbers() {
        return this.usrStudyNumbers;
    }

    public void setUsrStudyNumbers(ArrayList usrGrpNames) {
        this.usrStudyNumbers = usrStudyNumbers;
    }

    /**
     * Returns an Arraylist containing the type of user (internal or external)
     * to whom a copy of the study is given
     * 
     * @return an Arraylist containing the type of user (internal or external)
     *         to whom a copy of the study is given
     */
    public ArrayList getInexUsers() {
        return this.inexUsers;
    }

    /**
     * Sets the Arraylist containing the type of user (internal or external) to
     * whom a copy of the study is given with the ArrayList passed as the
     * parameter
     * 
     * @param teamRights
     *            An ArrayList with String objects having the type of user
     *            (internal or external) to whom a copy of the study is given
     */
    public void setInexUsers(ArrayList inexUsers) {
        this.inexUsers = inexUsers;
    }

    public int getCRows() {
        return this.cRows;
    }

    public void setCRows(int cRows) {
        this.cRows = cRows;
    }

    public void setUsrIds(Integer usrId) {
        usrIds.add(usrId);
    }

    public void setUsrAccIds(String usrAccId) {
        usrAccIds.add(usrAccId);
    }

    public void setUsrSiteIds(String usrSiteId) {
        usrSiteIds.add(usrSiteId);
    }

    public void setUsrSiteNames(String usrSiteName) {
        usrSiteNames.add(usrSiteName);
    }

    public void setUsrLastNames(String usrLastName) {
        usrLastNames.add(usrLastName);
    }

    public void setUsrFirstNames(String usrFirstName) {
        usrFirstNames.add(usrFirstName);
    }

    public void setUsrMidNames(String usrMidName) {
        usrMidNames.add(usrMidName);
    }

    public void setUsrLoggedInFlags(String loggedInFlag) {
        usrLoggedInFlags.add(loggedInFlag);
    }

    public void setUsrWrkExps(String usrWrkExp) {
        usrWrkExps.add(usrWrkExp);
    }

    public void setUsrPhaseInvs(String usrPhaseInv) {
        usrPhaseInvs.add(usrPhaseInv);
    }

    public void setUsrSessTimes(String usrSessTime) {
        usrSessTimes.add(usrSessTime);
    }

    public void setUsrLogNames(String usrLogName) {
        usrLogNames.add(usrLogName);
    }

    public void setUsrPwds(String usrPwd) {
        usrPwds.add(usrPwd);
    }

    public void setUsrSecQues(String usrSecQue) {
        usrSecQues.add(usrSecQue);
    }

    public void setUsrAns(String usrAn) {
        usrAns.add(usrAn);
    }

    public void setUsrStats(String usrStat) {
        usrStats.add(usrStat);
    }

    // JM:
    public void setUsrTypes(String usrType) {
        usrTypes.add(usrType);
    }

    public void setUsrSpls(String usrSpl) {
        usrSpls.add(usrSpl);
    }

    public void setUsrDefGrps(String usrDefGrp) {
        usrDefGrps.add(usrDefGrp);
    }

    public void setUsrPerAdds(String usrPerAdd) {
        usrPerAdds.add(usrPerAdd);
    }

    public void setUsrJobTypes(String usrJobType) {
        usrJobTypes.add(usrJobType);
    }

    public void setUsrGrpIds(String usrGrpId) {
        usrGrpIds.add(usrGrpId);
    }

    public void setUsrCodes(String usrCode) {
        usrCodes.add(usrCode);
    }

    public void setUsrGrpNames(String usrGrpName) {
        usrGrpNames.add(usrGrpName);
    }

    public void setUsrStudyNumbers(String usrStudyNumber) {
        usrStudyNumbers.add(usrStudyNumber);
    }
    /**
	 * @return the usrLoginModuleIdList
	 */
	public ArrayList getUsrLoginModuleIdList() {
		return usrLoginModuleIdList;
	}

	/**
	 * @param usrLoginModuleIdList the usrLoginModuleIdList to set
	 */
	public void setUsrLoginModuleIdList(ArrayList usrLoginModuleIdList) {
		this.usrLoginModuleIdList = usrLoginModuleIdList;
	}
	
	/**
	 * @param usrLoginModuleIdList the usrLoginModuleIdList to set
	 */
	public void setUsrLoginModuleIdList(String usrLoginModuleId) {
		this.usrLoginModuleIdList.add(usrLoginModuleId);
	}

	/**
	 * @return the usrLoginModuleMapList
	 */
	public ArrayList getUsrLoginModuleMapList() {
		return usrLoginModuleMapList;
	}

	/**
	 * @param usrLoginModuleMapList the usrLoginModuleMapList to set
	 */
	public void setUsrLoginModuleMapList(ArrayList usrLoginModuleMapList) {
		this.usrLoginModuleMapList = usrLoginModuleMapList;
	}
	/**
	 * @param usrLoginModuleMapList the usrLoginModuleMapList to set
	 */
	public void setUsrLoginModuleMapList(String usrLoginModuleMap) {
		this.usrLoginModuleMapList.add(usrLoginModuleMap);
	}
    /**
     * Adds the object to the Arraylist containing the type of user (internal or
     * external) to whom a copy of the study has been given
     * 
     * @param inexUser
     *            A String object that needs to be addedto the Arraylist
     *            containing the type of user (internal or external) to whom a
     *            copy of the study has been given
     */
    public void setInexUsers(String inexUser) {
        inexUsers.add(inexUser);
    }

    // end of getter and setter methods

    /**
     * Sets all Users for a Group in the attributes of this class
     * 
     * @param groupId
     *            Id of the Group
     */
    public void getUserValues(int groupId) {
    	int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        
        try {
        	
            conn = getConnection();

             pstmt = conn
                    .prepareStatement("select PK_USER, "
                            + "FK_ACCOUNT, "
                            + "(select SITE_NAME from er_site where PK_SITE  = FK_SITEID) as site_name, "
                            + "USR_LASTNAME, "
                            + "USR_FIRSTNAME, "
                            + "USR_MIDNAME, "
                            + "USR_WRKEXP, "
                            + "USR_PAHSEINV, "
                            + "USR_SESSTIME, "
                            + "USR_LOGNAME, "
                            + "USR_PWD, "
                            + "USR_SECQUES, "
                            + "USR_ANS, "
                            + "USR_STAT, "
                            + "FK_CODELST_SPL, "
                            + "FK_GRP_DEFAULT, "
                            + "FK_PERADD, "
                            + "(select CODELST_DESC from er_codelst where PK_CODELST =  FK_CODELST_JOBTYPE) as job_type "
                            + "from er_user "
                            + "where PK_USER in (select FK_USER from er_usrgrp where  FK_GRP =?) Order by lower(USR_FIRSTNAME || ' ' || USR_LASTNAME) ASC");
            pstmt.setInt(1, groupId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setUsrIds(new Integer(rs.getInt("PK_USER")));
                setUsrAccIds(rs.getString("FK_ACCOUNT"));
                setUsrSiteNames(rs.getString("site_name"));
                setUsrLastNames(rs.getString("USR_LASTNAME"));
                setUsrFirstNames(rs.getString("USR_FIRSTNAME"));
                setUsrMidNames(rs.getString("USR_MIDNAME"));
                setUsrWrkExps(rs.getString("USR_WRKEXP"));
                setUsrPhaseInvs(rs.getString("USR_PAHSEINV"));
                setUsrSessTimes(rs.getString("USR_SESSTIME"));
                setUsrLogNames(rs.getString("USR_LOGNAME"));
                setUsrPwds(rs.getString("USR_PWD"));
                setUsrSecQues(rs.getString("USR_SECQUES"));
                setUsrAns(rs.getString("USR_ANS"));
                setUsrStats(rs.getString("USR_STAT"));
                setUsrSpls(rs.getString("FK_CODELST_SPL"));
                setUsrDefGrps(rs.getString("FK_GRP_DEFAULT"));
                setUsrPerAdds(rs.getString("FK_PERADD"));
                setUsrJobTypes(rs.getString("job_type"));
                rows++;

            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("user",
                    "UserDao.getUserValues EXCEPTION IN FETCHING FROM User table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Sets the user details in the attributes of this class
     * 
     * @param userList
     *            a comma separated list of userIds
     */
     // Missing Commas added by Gopu on 120506
    public void getUsersDetails(String userList) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sqlStr = "";
        try {
            conn = getConnection();
            sqlStr = "select PK_USER, "
                    + "FK_ACCOUNT, "
                    + "FK_SITEID, "
                    + "(select SITE_NAME from er_site where PK_SITE  = FK_SITEID) as site_name, "
                    + "USR_LASTNAME, "
                    + "USR_FIRSTNAME, "
                    + "USR_MIDNAME, "
                    + "USR_WRKEXP, "
                    + "USR_PAHSEINV, "
                    + "USR_SESSTIME, "
                    + "USR_LOGNAME, "
                    + "USR_PWD, "
                    + "USR_SECQUES, "
                    + "USR_ANS, "
                    + "USR_STAT, "
                    + "USR_TYPE, "
                    + "FK_CODELST_SPL, "
                    + "FK_GRP_DEFAULT, "
                    + "FK_PERADD, "
                    + "(select CODELST_DESC from er_codelst where PK_CODELST =  FK_CODELST_JOBTYPE) as job_type, "
                    + "fk_loginmodule, "
                    + "usr_loginmodulemap "
                    + "from er_user "
                    + "where PK_USER in ("
                    + userList
                    + ") Order by lower(USR_FIRSTNAME || ' ' || USR_LASTNAME) ASC";
            pstmt = conn.prepareStatement(sqlStr);
            // pstmt.setString(1,userList);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                setUsrIds(new Integer(rs.getInt("PK_USER")));
                setUsrSiteIds(rs.getString("FK_SITEID"));
                setUsrAccIds(rs.getString("FK_ACCOUNT"));
                setUsrSiteNames(rs.getString("site_name"));
                setUsrLastNames(rs.getString("USR_LASTNAME"));
                setUsrFirstNames(rs.getString("USR_FIRSTNAME"));
                setUsrMidNames(rs.getString("USR_MIDNAME"));
                setUsrWrkExps(rs.getString("USR_WRKEXP"));
                setUsrPhaseInvs(rs.getString("USR_PAHSEINV"));
                setUsrSessTimes(rs.getString("USR_SESSTIME"));
                setUsrLogNames(rs.getString("USR_LOGNAME"));
                setUsrPwds(rs.getString("USR_PWD"));
                setUsrSecQues(rs.getString("USR_SECQUES"));
                setUsrAns(rs.getString("USR_ANS"));
                setUsrStats(rs.getString("USR_STAT"));
                setUsrTypes(rs.getString("USR_TYPE"));
                setUsrSpls(rs.getString("FK_CODELST_SPL"));
                setUsrDefGrps(rs.getString("FK_GRP_DEFAULT"));
                setUsrPerAdds(rs.getString("FK_PERADD"));
                setUsrJobTypes(rs.getString("job_type"));
                this.setUsrLoginModuleIdList(rs.getString("fk_loginmodule"));
                this.setUsrLoginModuleMapList(rs.getString("usr_loginmodulemap"));
                rows++;
                Rlog.debug("user", "UserDao.getUsersDetails rows " + rows);

            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("user",
                    "UserDao.getUsersDetails EXCEPTION IN FETCHING FROM User table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Populates the Data Access Object with all Users for all groups for an
     * Account *
     * 
     * @param accId
     *            Account Id
     */
    public void getAccountUsers(int accId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn
                    .prepareStatement("SELECT ER_GRPS.GRP_NAME,"
                            + "ER_GRPS.PK_GRP,"
                            + "ER_USER.pk_user,"
                            + "ER_USER.USR_LASTNAME, "
                            + "ER_USER.USR_FIRSTNAME, "
                            + "ER_USER.USR_MIDNAME, "
                            + "ER_USER.USR_LOGGEDINFLAG, "
                            + "ER_SITE.SITE_NAME, "
                            + "ER_CODELST.CODELST_DESC  as job_type, "
                            + "ER_USRGRP.pk_usrgrp "
                            + "FROM ER_GRPS, "
                            + "ER_USER, "
                            + "ER_USRGRP, "
                            + "ER_SITE, "
                            + "ER_CODELST "
                            + "Where ER_GRPS.fk_account = ? "
                            + "and ER_USER.USR_TYPE <> 'X' "
                            + "and ER_USRGRP.FK_GRP = ER_GRPS.PK_GRP "
                            + "and ER_USER.pk_user = ER_USRGRP.FK_User "
                            + "and ER_SITE.PK_SITE = ER_USER.FK_SITEID "
                            + "and ER_CODELST.PK_CODELST (+) = ER_USER.FK_CODELST_JOBTYPE "
                            + "order by lower(ER_GRPS.GRP_NAME) ASC , lower(ER_USER.USR_FIRSTNAME || ' ' || ER_USER.usr_lastname) ASC ");

            pstmt.setInt(1, accId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setUsrIds(new Integer(rs.getInt("PK_USER")));
                setUsrSiteNames(rs.getString("site_name"));
                setUsrLastNames(rs.getString("USR_LASTNAME"));
                setUsrFirstNames(rs.getString("USR_FIRSTNAME"));
                setUsrLoggedInFlags(rs.getString("USR_LOGGEDINFLAG"));
                setUsrDefGrps(rs.getString("GRP_NAME"));
                setUsrJobTypes(rs.getString("job_type"));
                setUsrMidNames(rs.getString("USR_MIDNAME"));
                setUsrGrpIds(rs.getString("pk_usrgrp"));

                rows++;
                Rlog.debug("user", "UserDao.getUserValues rows " + rows);

            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("user",
                    "UserDao.getUserValues EXCEPTION IN FETCHING FROM User table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Populates the Data Access Object with all Users for all groups for an
     * Account and a given status
     * 
     * @param accId
     *            Account Id
     * @param status
     *            User status. Possible Values: <BR>
     *            A - Active <br>
     *            B - Blocked <br>
     *            D - Deactivated <br>
     *            P - Pending <br>
     *            X - Deleted <br>
     */
    
    //  Modified by Manimaran to include first name and last name in search criteria.
    public void getAccountUsers(int accId, String status, String firstName, String lastName) {
        int rows = 0;
        PreparedStatement pstmt = null; 
        Connection conn = null;
        String lWhere = null;
        String fWhere = null;
        StringBuffer completeSelect = new StringBuffer();
        try {
            //System.out.println("*************1 **************");
        	
            conn = getConnection();
            
            //System.out.println("*************2 **************");

            String selectSt = 	"SELECT ER_GRPS.GRP_NAME,"
                + "ER_GRPS.PK_GRP,"
                + "ER_USER.pk_user,"
                + "ER_USER.USR_LASTNAME, "
                + "ER_USER.USR_FIRSTNAME, "
                + "ER_USER.USR_MIDNAME, "
                + "ER_SITE.SITE_NAME, "
                + "ER_CODELST.CODELST_DESC  as job_type, "
                + "ER_USER.USR_LOGGEDINFLAG, "
                + "ER_USRGRP.pk_usrgrp "
                + "FROM ER_GRPS, "
                + "ER_USER, "
                + "ER_USRGRP, "
                + "ER_SITE, "
                + "ER_CODELST "
                + "Where ER_GRPS.fk_account = ? "
                + "and ER_USER.USR_TYPE <> 'X' "
                + "and ER_USRGRP.FK_GRP = ER_GRPS.PK_GRP "
                + "and ER_USER.pk_user = ER_USRGRP.FK_User "
                + "and ER_USER.usr_stat = ? "
                + "and ER_SITE.PK_SITE = ER_USER.FK_SITEID "
                + "and ER_CODELST.PK_CODELST (+) = ER_USER.FK_CODELST_JOBTYPE ";
                
                String endSelect = 
                 " order by lower(ER_GRPS.GRP_NAME) ASC , lower(ER_USER.USR_FIRSTNAME || ' ' || ER_USER.usr_lastname) ASC";
            
                
                if ((firstName != null) && (!firstName.trim().equals(""))) {
                    fWhere = " and upper(ER_USER.USR_FIRSTNAME) like upper('%";
                    fWhere += firstName;
                    fWhere += "%')";
                }

                if ((lastName != null) && (!lastName.trim().equals(""))) {
                    lWhere = " and upper(ER_USER.USR_LASTNAME) like upper('%";
                    lWhere += lastName;
                    lWhere += "%')";
                }
                
                completeSelect.append(selectSt);
                
                if (fWhere != null) {
                    completeSelect.append(fWhere);
                }

                if (lWhere != null) {
                    completeSelect.append(lWhere);
                }
                
               completeSelect.append(endSelect);
               pstmt = conn
                    .prepareStatement(completeSelect.toString());
               
               Rlog.debug("user", "UserDao.search Active Account users "
                       + completeSelect.toString());

            //System.out.println("************* 3 **************");
            
            pstmt.setInt(1, accId);
            pstmt.setString(2, status);
           
            //System.out.println("*************4 **************");
            
            ResultSet rs = pstmt.executeQuery();

            //System.out.println("*************5 **************");

            while (rs.next()) {
              
            	//System.out.println("*************6 **************" + rows);

                setUsrIds(new Integer(rs.getInt("PK_USER")));
                setUsrSiteNames(rs.getString("site_name"));
                setUsrLastNames(rs.getString("USR_LASTNAME"));
                setUsrFirstNames(rs.getString("USR_FIRSTNAME"));
                setUsrDefGrps(rs.getString("GRP_NAME"));
                setUsrJobTypes(rs.getString("job_type"));
                setUsrMidNames(rs.getString("USR_MIDNAME"));
                setUsrGrpIds(rs.getString("pk_usrgrp"));
                setUsrLoggedInFlags(rs.getString("USR_LOGGEDINFLAG"));

                rows++;
                Rlog.debug("user", "UserDao.getUserValues rows " + rows);

            }

            //System.out.println("*************6 A **************" + rows);

            setCRows(rows);
            
            //System.out.println("*************6 B **************" + rows);

        } catch (SQLException ex) {
            
        	//System.out.println("*************7 **************" + ex);
            
            Rlog.fatal("user",
                    "UserDao.getUserValues EXCEPTION IN FETCHING FROM User table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /** ********* */

    /**
     * Populates the data Access Object with all Users for an Account
     * 
     * @param accId
     *            Account Id
     */
    public void getAccountUsersDetails(int accId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("SELECT ER_USER.pk_user,"
                    + "ER_USER.USR_LASTNAME, " + "ER_USER.USR_FIRSTNAME, "
                    + "ER_USER.USR_MIDNAME, " + "ER_USER.USR_STAT, "
                    + "ER_USER.USR_CODE, " + "ER_SITE.SITE_NAME "
                    + "FROM ER_USER, " + "ER_SITE "
                    + "WHERE ER_USER.FK_ACCOUNT = ? "
                    + "AND ER_SITE.PK_SITE = ER_USER.FK_SITEID "
                    + "AND ER_USER.USR_TYPE <> 'N' "
                    + "AND ER_USER.USR_TYPE <> 'X' "
                    + "order by lower(USR_FIRSTNAME || ' ' || USR_LASTNAME)");

            pstmt.setInt(1, accId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setUsrIds(new Integer(rs.getInt("PK_USER")));
                setUsrSiteNames(rs.getString("site_name"));
                setUsrLastNames(rs.getString("USR_LASTNAME"));
                setUsrFirstNames(rs.getString("USR_FIRSTNAME"));
                setUsrMidNames(rs.getString("USR_MIDNAME"));
                setUsrStats(rs.getString("USR_STAT"));
                setUsrCodes(rs.getString("USR_CODE"));
                rows++;
                Rlog.debug("user", "UserDao.getAccountUsersDetails rows "
                        + rows);

            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("user",
                    "UserDao.getAccountUsersDetails EXCEPTION IN FETCHING FROM User table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Populates the data Access Object with all Users for an Account
     * including non-system users.
     * 
     * @param accId
     *            Account Id
     */
    public void getAccountUsersDetailsForLookup(int accId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("SELECT ER_USER.pk_user,"
                    + "ER_USER.USR_LASTNAME, " + "ER_USER.USR_FIRSTNAME, "
                    + "ER_USER.USR_MIDNAME, " + "ER_USER.USR_STAT, "
                    + "ER_USER.USR_CODE, " + "ER_SITE.SITE_NAME "
                    + "FROM ER_USER, " + "ER_SITE "
                    + "WHERE ER_USER.FK_ACCOUNT = ? "
                    + "AND ER_SITE.PK_SITE = ER_USER.FK_SITEID "
                    + "AND ER_USER.USR_TYPE <> 'X' "
                    + "order by lower(USR_FIRSTNAME || ' ' || USR_LASTNAME)");

            pstmt.setInt(1, accId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setUsrIds(new Integer(rs.getInt("PK_USER")));
                setUsrSiteNames(rs.getString("site_name"));
                setUsrLastNames(rs.getString("USR_LASTNAME"));
                setUsrFirstNames(rs.getString("USR_FIRSTNAME"));
                setUsrMidNames(rs.getString("USR_MIDNAME"));
                setUsrStats(rs.getString("USR_STAT"));
                setUsrCodes(rs.getString("USR_CODE"));
                rows++;
                Rlog.debug("user", "UserDao.getAccountUsersDetails rows "
                        + rows);

            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("user",
                    "UserDao.getAccountUsersDetails EXCEPTION IN FETCHING FROM User table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /*
     * Changed by Vishal Abrol 07/13/04 corrected the SQL from 'and USR_STAT
     * in('A','B'.'D')' to and USR_STAT in('A','B','D') there was a '.' instead
     * of ',' in the SQL.
     */
    /**
     * Returns the number of users in the account includes active and blocked
     * users
     * 
     * @param accId -
     *            account id as int.
     * @return the number of users in the account
     */

    public int getUsersInAccount(int accId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("SELECT count(*) numberofusers "
                    + "FROM ER_USER " + "Where ER_USER.fk_account = ? "
                    + "and USR_TYPE='S' " + "and USR_STAT in('A','B','D') ");

            pstmt.setInt(1, accId);

            ResultSet rs = pstmt.executeQuery();
            if (rs != null) {
                rs.next();
                rows = rs.getInt("numberofusers");
            }
        } catch (SQLException ex) {
            Rlog.fatal("user",
                    "UserDao.getUsersInAccount EXCEPTION IN FETCHING FROM User table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        return rows;
    }

    // added by JM dt. 27Jan05

    public int getActiveUsersInAccount(int accId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("SELECT count(*) numberofusers "
                    + "FROM ER_USER " + "Where ER_USER.fk_account = ? "
                    + "and USR_TYPE='S' " + "and USR_STAT in ('A','B')");

            pstmt.setInt(1, accId);

            ResultSet rs = pstmt.executeQuery();
            if (rs != null) {
                rs.next();
                rows = rs.getInt("numberofusers");
            }
        } catch (SQLException ex) {
            Rlog.fatal("user",
                    "UserDao.getActiveUsersInAccount EXCEPTION IN FETCHING FROM User table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        return rows;
    }

    /**
     * Sets all Users for an Account in the attributes of this class
     * 
     * @param accId
     *            Account Id
     */
    /*
     * public void getUsersInAccount(int accId) { int rows = 0;
     * PreparedStatement pstmt = null; Connection conn = null; try { conn =
     * getConnection();
     * 
     * pstmt = conn.prepareStatement("SELECT ER_USER.pk_user," +
     * "ER_USER.USR_LASTNAME, " + "ER_USER.USR_FIRSTNAME, " +
     * "ER_USER.USR_MIDNAME, " + "ER_CODELST.CODELST_DESC as job_type " + "FROM
     * ER_USER, " + "ER_CODELST " + "Where ER_USER.fk_account = ? " + "and
     * ER_CODELST.PK_CODELST = ER_USER.FK_CODELST_JOBTYPE " + "order by
     * ER_USER.USR_LASTNAME, " + "ER_USER.USR_FIRSTNAME ");
     * 
     * pstmt.setInt(1,accId);
     * 
     * ResultSet rs = pstmt.executeQuery();
     * 
     * while (rs.next()) { setUsrIds(new Integer(rs.getInt("PK_USER")));
     * setUsrLastNames(rs.getString("USR_LASTNAME"));
     * setUsrFirstNames(rs.getString("USR_FIRSTNAME"));
     * setUsrMidNames(rs.getString("USR_MIDNAME"));
     * setUsrJobTypes(rs.getString("job_type"));
     * 
     * rows ++; Rlog.debug("user","UserDao.getUsersInAccount rows " + rows);
     *  }
     * 
     * setCRows(rows) ; } catch (SQLException ex) {
     * Rlog.fatal("user","UserDao.getUsersInAccount EXCEPTION IN FETCHING FROM
     * User table" + ex); } finally { try { if (pstmt != null) pstmt.close(); }
     * catch (Exception e) { } try { if (conn!=null) conn.close(); } catch
     * (Exception e) {}
     *  }
     *  }
     */

    /** ****SEARCH USER **** */

    /**
     * Sets all those Account Users the attributes of this class which can be
     * assigned to a study team
     * 
     * @param studyId
     *            Id of the Study
     * @param accId
     *            Account Id
     * @param lName
     *            Last Name string (to search for the user)
     * @param fName
     *            First name string(to search for the user)
     */
    // modified for search criteria on org name, group and job type JM:
    // 21April05
    public void getAvailableTeamUsers(int studyId, int accId, String lName,
            String fName, String orgName, String group, String jobType) {

        Rlog.debug("user", "UserDao.getavailableteam users ");
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String lWhere = null;
        String fWhere = null;
        String oWhere = null;
        String gWhere = null;
        String jWhere = null;
        StringBuffer completeSelect = new StringBuffer();

        try {
            conn = getConnection();

            // query modified for fetching Organization, group and jobType JM:
            // 21April05
            // query modified for fetching non system users into the user's
            // list, JM: 11May05
            
            //Query modified for enhancement for query #S12
            
            String selectSt = "SELECT ER_USER.PK_USER,ER_USER.USR_LASTNAME,ER_USER.USR_FIRSTNAME,  ER_USER.USR_MIDNAME , ER_USER.USR_STAT,ER_USER.USR_TYPE,ER_SITE.SITE_NAME , nvl((select GRP_NAME from er_grps where  pK_GRP = FK_GRP_DEFAULT ),'-') as group_name,  nvl((select CODELST_DESC from er_codelst where pk_codelst = fk_codelst_jobtype), '-') as job_type"
                    + " from ER_USER , ER_SITE"
                    + " Where USR_STAT in('A','B','D') and USR_TYPE <> 'X' and USER_HIDDEN <> 1" //KM-031208
                    + " and ER_USER.fk_account = ? "
                    + " and PK_SITE  = FK_SITEID ";

            //Query modififed by Manimaran to fix the Bug2815
            String endSelect = " and not exists (select * from er_studyteam where "
                    + "fk_study = ? and (nvl(STUDY_TEAM_USR_TYPE,'D') = 'D' or nvl(STUDY_TEAM_USR_TYPE,'X') = 'X') and fk_user = er_user.pk_user )";

            if ((fName != null) && (!fName.trim().equals(""))) {
                fWhere = " and upper(ER_USER.USR_FIRSTNAME) like upper('%";
                fWhere += fName.replaceAll("'", "''").replaceAll("%","/%").replaceAll("_","/_"); 
                fWhere += "%') escape '/' ";
            }

            if ((lName != null) && (!lName.trim().equals(""))) {
                lWhere = " and upper(ER_USER.USR_LASTNAME) like upper('%";
                lWhere += lName.replaceAll("'", "''").replaceAll("%","/%").replaceAll("_","/_");  
                lWhere += "%') escape '/' ";
            }
            // JM: 21April05
            if ((orgName != null) && (!orgName.trim().equals(""))) {
                oWhere = " and UPPER(ER_USER.FK_SITEID) = UPPER('";
                oWhere += orgName;
                oWhere += "')";
            }

            //km-All the User's belonging to the selected group should be retreived, default or not doesn't matter. (Bug:2306)
	    if ((group != null) && (!group.trim().equals(""))) {
               // gWhere = "and  upper(ER_USER.FK_GRP_DEFAULT) = upper('";
	       gWhere=" and  er_user.pk_user in (select fk_user from er_usrgrp where upper(fk_grp) = upper('";

                gWhere += group;
                gWhere += "'))";
            }
            // modified by JM: 29April05
            if ((jobType != null) && (!jobType.trim().equals(""))) {
                jWhere = "and upper(ER_USER.FK_CODELST_JOBTYPE)  = UPPER('";
                jWhere += jobType;
                jWhere += "')";
            }
            // ///////

            completeSelect.append(selectSt);

            if (fWhere != null) {
                completeSelect.append(fWhere);
            }

            if (lWhere != null) {
                completeSelect.append(lWhere);
            }

            // JM: 21April05
            if (oWhere != null) {
                completeSelect.append(oWhere);
            }

            if (gWhere != null) {
                completeSelect.append(gWhere);
            }
            if (jWhere != null) {
                completeSelect.append(jWhere);
            }

            // ///////

            completeSelect.append(endSelect);

            // completeSelect.append(" ORDER BY lower(ER_USER.USR_FIRSTNAME || '
            // ' || ER_USER.usr_lastname) ASC");

            // JM: 21April05
            completeSelect.append(" ORDER BY lower(ER_USER.usr_lastname) ASC");

            pstmt = conn.prepareStatement(completeSelect.toString());

            Rlog.debug("user", "UserDao.getavailableteam users "
                    + completeSelect.toString());

            pstmt.setInt(1, accId);
            pstmt.setInt(2, studyId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setUsrIds(new Integer(rs.getInt("PK_USER")));
                setUsrLastNames(rs.getString("USR_LASTNAME"));
                setUsrFirstNames(rs.getString("USR_FIRSTNAME"));
                setUsrMidNames(rs.getString("USR_MIDNAME"));
                
                //code added for #S12 by KN
                setUsrStats(rs.getString("USR_STAT"));	
                setUsrTypes(rs.getString("USR_TYPE"));
                setUsrSiteNames(rs.getString("site_name"));
                rows++;
                Rlog.debug("user", "UserDao.getAvailableTeamUsers rows "
                                + rows);

            }
            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("user",
                    "UserDao.getAvailableTeamUsers  EXCEPTION IN FETCHING FROM User table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /** ******* */

    /**
     * Sets the details of the user for the specified login name in the
     * attributes of this class
     * 
     * @param logName
     *            Login Name
     */
    public void getUserValuesByLoginName(String logName) {
        Rlog.debug("user", "UserDao.getUserValuesByLoginName line number 1 ");
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn
                    .prepareStatement("select PK_USER, "
                            + "FK_ACCOUNT, "
                            + "(select SITE_NAME from er_site where PK_SITE  = FK_SITEID) as site_name, "
                            + "USR_LASTNAME, "
                            + "USR_FIRSTNAME, "
                            + "USR_MIDNAME, "
                            + "USR_WRKEXP, "
                            + "USR_PAHSEINV, "
                            + "USR_SESSTIME, "
                            + "USR_LOGNAME, "
                            + "USR_PWD, "
                            + "USR_SECQUES, "
                            + "USR_ANS, "
                            + "USR_STAT, "
                            + "FK_CODELST_SPL, "
                            + "FK_GRP_DEFAULT, "
                            + "FK_PERADD, "
                            + "(select CODELST_DESC from er_codelst where PK_CODELST =  FK_CODELST_JOBTYPE) as job_type, "
                            + "USR_TYPE "
                            + "from er_user "
                            + "where USR_LOGNAME = ? Order by USR_LASTNAME, USR_FIRSTNAME");

            pstmt.setString(1, logName);

            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("user",
                    "UserDao.getUserValuesByLoginName prepared statement is - "
                            + pstmt.toString());
            while (rs.next()) {

                setUsrIds(new Integer(rs.getInt("PK_USER")));
                setUsrAccIds(rs.getString("FK_ACCOUNT"));
                setUsrSiteNames(rs.getString("site_name"));
                setUsrLastNames(rs.getString("USR_LASTNAME"));
                setUsrFirstNames(rs.getString("USR_FIRSTNAME"));
                setUsrMidNames(rs.getString("USR_MIDNAME"));
                setUsrWrkExps(rs.getString("USR_WRKEXP"));
                setUsrPhaseInvs(rs.getString("USR_PAHSEINV"));
                setUsrSessTimes(rs.getString("USR_SESSTIME"));
                setUsrLogNames(rs.getString("USR_LOGNAME"));
                setUsrPwds(rs.getString("USR_PWD"));
                setUsrSecQues(rs.getString("USR_SECQUES"));
                setUsrAns(rs.getString("USR_ANS"));
                setUsrStats(rs.getString("USR_STAT"));
                setUsrSpls(rs.getString("FK_CODELST_SPL"));
                setUsrDefGrps(rs.getString("FK_GRP_DEFAULT"));
                setUsrPerAdds(rs.getString("FK_PERADD"));
                setUsrJobTypes(rs.getString("job_type"));
                setUsrTypes(rs.getString("USR_TYPE"));
                rows++;
                Rlog.debug("user", "UserDao.getUserValuesByLoginName rows "
                        + rows);

            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("user",
                    "UserDao.getUserValues EXCEPTION IN FETCHING FROM User table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /** ******************* */

    /**
     * Sets all Users for a Group in the attributes of this class
     * 
     * @param groupId
     *            Group Id
     */
    public void getGroupUsers(int groupId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn
                    .prepareStatement("select PK_USER, "
                            + "USR_LASTNAME, "
                            + "USR_FIRSTNAME, "
                            + "USR_MIDNAME, "
                            + "USR_STAT "
                            + "from er_user "
                            + "where "
                            + "ER_USER.USR_TYPE <> 'X' "
                            + "and PK_USER in (select FK_USER from er_usrgrp where  FK_GRP =?) Order by lower(USR_FIRSTNAME || ' ' || usr_lastname) ");
            pstmt.setInt(1, groupId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setUsrIds(new Integer(rs.getInt("PK_USER")));

                setUsrLastNames(rs.getString("USR_LASTNAME"));
                setUsrFirstNames(rs.getString("USR_FIRSTNAME"));
                setUsrMidNames(rs.getString("USR_MIDNAME"));
                setUsrStats(rs.getString("USR_STAT"));

                rows++;
                Rlog.debug("user", "UserDao.getUserValues rows " + rows);

            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("user",
                    "UserDao.getUserValues EXCEPTION IN FETCHING FROM User table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /** ******************** */

    /** ******************* */

    /**
     * Sets all those Users of an Account in the attributes of this class that
     * can be assigned to the specified group
     * 
     * @param accId
     * @param groupId
     */
    public void getAvailableAccountUsers(int accId, int groupId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn
                    .prepareStatement("select PK_USER, "
                            + "USR_LASTNAME, "
                            + "USR_FIRSTNAME, "
                            + "USR_MIDNAME, "
                            + " USR_STAT "
                            + "from er_user "
                            + "where fk_account = ? "
                            + "and USR_TYPE <> 'X' "
                            + "and USR_STAT = 'A' and PK_USER not in (select FK_USER from er_usrgrp where  FK_GRP =?) Order by lower(USR_FIRSTNAME || ' ' || usr_lastname) ASC");
            pstmt.setInt(1, accId);
            pstmt.setInt(2, groupId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setUsrIds(new Integer(rs.getInt("PK_USER")));

                setUsrLastNames(rs.getString("USR_LASTNAME"));
                setUsrFirstNames(rs.getString("USR_FIRSTNAME"));
                setUsrMidNames(rs.getString("USR_MIDNAME"));
                setUsrStats(rs.getString("USR_STAT"));

                rows++;
                Rlog.debug("user", "UserDao.getUserValues rows " + rows);

            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("user",
                    "UserDao.getUserValues EXCEPTION IN FETCHING FROM User table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /** ******************** */

    /**
     * Returns the number of users with the same login ID as this userLogin
     * 
     * @param userLogin
     *            The Login ID which is to be checked in the database
     * @return int The number of users with the same Login Id as userLogin
     */
    public int getCount(String userLogin) {
        int count = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("select count(*) as count "
                    + "from er_user " + "where trim(usr_logname) = trim(?)");
            pstmt.setString(1, userLogin);

            ResultSet rs = pstmt.executeQuery();

            rs.next();
            count = rs.getInt("count");

        } catch (SQLException ex) {
            Rlog.fatal("user",
                    "UserDao.getCount EXCEPTION IN FETCHING FROM User table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        return count;
    }

    /**
     * Returns the User Code corresponding to the userId
     * 
     * @param userId
     *            The User ID for which code is required
     * @return String The User Code for the userId
     */
    public String getUserCode(String userId) {
        String userCode = "";
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("select usr_code " + "from er_user "
                    + "where pk_user = ?");
            pstmt.setString(1, userId);

            ResultSet rs = pstmt.executeQuery();

            rs.next();
            userCode = rs.getString("usr_code");

        } catch (SQLException ex) {
            Rlog.fatal("user",
                    "UserDao.getUserCode EXCEPTION IN FETCHING FROM User table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        return userCode;
    }

    /**
     * Performs a user search with provided criteria and populates the Data
     * Access Object with results
     * 
     * @param accId
     *            Account Id
     * @param lName
     *            Last Name
     * @param role
     *            Code for User Job Type
     * @param organization
     *            Code for organization/site
     * @param fName
     *            First Name
     * 
     */
    /*
     * modified by sonia sahni on 08/06/04 added an order by clause and java doc
     * comments
     */
    public void searchAccountUsers(int accId, String lName, String role,
            String organization, String fName) {
        Rlog.debug("user", "UserDao.Search Account users ");
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String lWhere = null;
        String fWhere = null;
        String oWhere = null;
        String rWhere = null;
        String aWhere = null;
        StringBuffer completeSelect = new StringBuffer();

        try {
            conn = getConnection();

            String selectSt = "SELECT ER_USER.pk_user,ER_USER.USR_LASTNAME, "
                    + "(select SITE_NAME from er_site where PK_SITE  = FK_SITEID) as site_name, "
                    + "ER_USER.USR_FIRSTNAME, ER_USER.USR_MIDNAME, ER_USER.USR_LOGNAME "
                    + " from ER_USER "
                    + " Where USR_STAT in('A','B','D') and USR_TYPE <> 'X' ";

            if ((organization != null) && (!organization.trim().equals(""))) {
                oWhere = " and UPPER(ER_USER.FK_SITEID) = UPPER(";
                oWhere += organization;
                oWhere += ")";
            }

            if ((role != null) && (!role.trim().equals(""))) {
                rWhere = " and UPPER(ER_USER.FK_CODELST_JOBTYPE) = UPPER(";
                rWhere += role;
                rWhere += ")";
            }

            if ((fName != null) && (!fName.trim().equals(""))) {
                fWhere = " and UPPER(ER_USER.USR_FIRSTNAME) like UPPER('";
                fWhere += fName;
                fWhere += "%')";
            }

            if ((lName != null) && (!lName.trim().equals(""))) {
                lWhere = " and UPPER(ER_USER.USR_LASTNAME) like UPPER('";
                lWhere += lName;
                lWhere += "%')";
            }

            completeSelect.append(selectSt);

            if (accId > 0) {
                aWhere = " and ER_USER.fk_account = " + accId;
                completeSelect.append(aWhere);
            }

            if (fWhere != null) {
                completeSelect.append(fWhere);
            }

            if (lWhere != null) {
                completeSelect.append(lWhere);
            }

            if (oWhere != null) {
                completeSelect.append(oWhere);
            }

            if (rWhere != null) {
                completeSelect.append(rWhere);
            }

            // add order by

            completeSelect
                    .append(" ORDER BY lower(ER_USER.USR_FIRSTNAME || ' ' || ER_USER.usr_lastname) ASC ");

            pstmt = conn.prepareStatement(completeSelect.toString());

            Rlog.debug("user", "UserDao.search Account users "
                    + completeSelect.toString());

            // pstmt.setInt(1,accId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setUsrIds(new Integer(rs.getInt("PK_USER")));

                setUsrLastNames(rs.getString("USR_LASTNAME"));

                setUsrFirstNames(rs.getString("USR_FIRSTNAME"));

                setUsrMidNames(rs.getString("USR_MIDNAME"));

                setUsrLogNames(rs.getString("USR_LOGNAME"));

                setUsrSiteNames(rs.getString("site_name"));

                rows++;

            }
            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("user",
                    "UserDao.getAvailableTeamUsers  EXCEPTION IN FETCHING FROM User table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Performs a user search and populates the Data Access Object with results
     * 
     * @param accId
     *            Account Id
     * @param lName
     *            Last Name string (to search for the user)
     * @param fName
     *            First name string(to search for the user)
     * @param ids
     *            Comma separated list of ids that have already been selected
     */

    public void searchAccountUsers(int accId, String lName, String fName,
            String role, String organization, String ids) {
        Rlog.debug("user", "UserDao.Search Account users ");
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String lWhere = null;
        String fWhere = null;
        String oWhere = null;
        String rWhere = null;
        StringBuffer completeSelect = new StringBuffer();

        try {
            conn = getConnection();

            String selectSt = "SELECT ER_USER.pk_user,ER_USER.USR_LASTNAME, "
                    + "(select SITE_NAME from er_site where PK_SITE  = FK_SITEID) as site_name, "
                    + "ER_USER.USR_FIRSTNAME, ER_USER.USR_MIDNAME"
                    + " from ER_USER "
                    + " Where USR_STAT = 'A' and USR_TYPE <> 'X' and ER_USER.fk_account = ? ";

            if ((organization != null) && (!organization.trim().equals(""))) {
                oWhere = " and UPPER(ER_USER.FK_SITEID) = UPPER(";
                oWhere += organization;
                oWhere += ")";
            }

            if ((role != null) && (!role.trim().equals(""))) {
                rWhere = " and UPPER(ER_USER.FK_CODELST_JOBTYPE) = UPPER(";
                rWhere += role;
                rWhere += ")";
            }

            if ((fName != null) && (!fName.trim().equals(""))) {
                fWhere = " and UPPER(ER_USER.USR_FIRSTNAME) like UPPER('";
                fWhere += fName;
                fWhere += "%')";
            }

            if ((lName != null) && (!lName.trim().equals(""))) {
                lWhere = " and UPPER(ER_USER.USR_LASTNAME) like UPPER('";
                lWhere += lName;
                lWhere += "%')";
            }

            completeSelect.append(selectSt);

            if ((ids != null) && (!ids.trim().equals("")))
                completeSelect.append(" and ER_USER.pk_user not in (" + ids
                        + ")");

            if (fWhere != null) {
                completeSelect.append(fWhere);
            }

            if (lWhere != null) {
                completeSelect.append(lWhere);
            }

            if (oWhere != null) {
                completeSelect.append(oWhere);
            }

            if (rWhere != null) {
                completeSelect.append(rWhere);
            }

            // add order by

            completeSelect
                    .append(" ORDER BY lower(ER_USER.USR_FIRSTNAME || ' ' || ER_USER.usr_lastname) ASC ");

            pstmt = conn.prepareStatement(completeSelect.toString());

            Rlog.debug("user", "UserDao.search Account users "
                    + completeSelect.toString());

            pstmt.setInt(1, accId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setUsrIds(new Integer(rs.getInt("PK_USER")));
                
                setUsrLastNames(rs.getString("USR_LASTNAME"));
                setUsrFirstNames(rs.getString("USR_FIRSTNAME"));
                setUsrMidNames(rs.getString("USR_MIDNAME"));
                setUsrSiteNames(rs.getString("site_name"));
       
                rows++;
                Rlog
                        .debug("user", "UserDao.getAvailableTeamUsers rows "
                                + rows);

            }
            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("user",
                    "UserDao.getAvailableTeamUsers  EXCEPTION IN FETCHING FROM User table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    
    /**
     * JM: 11/09/2005: New method added here for conveniently browsing and search user based 
     * on more parameters like group and study team
     * Performs a user search and populates the Data Access Object with results
     * 
     * @param accId
     *            Account Id
     * @param lName
     *            Last Name string (to search for the user)
     * @param fName
     *            Fast Name string (to search for the user)
     * @param role
     *            Job desc string(to search for the user)
     * @param organization
     *            organization name string(to search for the user)
     * @param group
     *            Group name string(to search for the user)
     * @param sTeam
     * 			  Study name string (to search for the user)	
     *@param  ids
     *            Comma separated list of ids that have already been selected 
     **/
    
    public void searchAccountUsers(int accId, String lName, String fName,
            String role, String organization, String group, String sTeam, String ids) {
        Rlog.debug("user", "UserDao.Search Account users with group and study team added.. ");
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String lWhere = null;
        String fWhere = null;
        String oWhere = null;
        String rWhere = null;
        String gWhere = null;
        String sWhere = null;
        
        StringBuffer completeSelect = new StringBuffer();

        try {
            conn = getConnection();

            String selectSt = " SELECT distinct u.pk_user,u.USR_LASTNAME, "
                    + "(select SITE_NAME from er_site where PK_SITE  = FK_SITEID) as site_name, "
                    + " u.USR_FIRSTNAME, u.USR_MIDNAME,u.USR_STAT,u.USR_TYPE "                    
                    + " from  ER_USER u left outer  join  er_studyteam et on u.pk_user=et.fk_user "
                    + " Where u.USR_STAT in ('A','B') and u.USR_TYPE <> 'X' and u.fk_account = ? ";
            
            if ((organization != null) && (!organization.trim().equals(""))) {
                oWhere = " and UPPER(u.FK_SITEID) = UPPER(";
                oWhere += organization;
                oWhere += ")";
            }

            if ((role != null) && (!role.trim().equals(""))) {
                rWhere = " and UPPER(u.FK_CODELST_JOBTYPE) = UPPER(";
                rWhere += role;
                rWhere += ")";
            }

            if ((fName != null) && (!fName.trim().equals(""))) {
                fWhere = " and UPPER(u.USR_FIRSTNAME) like UPPER('";
                fWhere += fName;
                fWhere += "%')";
            }

            if ((lName != null) && (!lName.trim().equals(""))) {
                lWhere = " and UPPER(u.USR_LASTNAME) like UPPER('";
                lWhere += lName;
                lWhere += "%')";
            }
            //JM: 
            if ((group != null) && (!group.trim().equals("") && !group.trim().equals("null")))
			{			
				gWhere =" and UPPER(u.fk_grp_default) = upper(";
				gWhere +=group;
				gWhere +=")";
			}
			
			if ((sTeam != null) && (!sTeam.trim().equals("") && !sTeam.trim().equals("null")))
			{			
				sWhere =" and upper(et.fk_study)=upper(";
				sWhere +=sTeam;
				sWhere +=")";
			}			

			
			
            completeSelect.append(selectSt);

            if ((ids != null) && (!ids.trim().equals("")))
                completeSelect.append(" and u.pk_user not in (" + ids
                        + ")");

            if (fWhere != null) {
                completeSelect.append(fWhere);
            }

            if (lWhere != null) {
                completeSelect.append(lWhere);
            }

            if (oWhere != null) {
                completeSelect.append(oWhere);
            }

            if (rWhere != null) {
                completeSelect.append(rWhere);
            }
            if (gWhere != null) {
                completeSelect.append(gWhere);
            }
            if (sWhere != null) {
                completeSelect.append(sWhere);
            }
            

            // add order by

            completeSelect
                    .append(" ORDER BY lower(u.USR_FIRSTNAME || ' ' || u.usr_lastname) ASC ");

            pstmt = conn.prepareStatement(completeSelect.toString());

            Rlog.debug("user", "UserDao.search Account users "
                    + completeSelect.toString());

            pstmt.setInt(1, accId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setUsrIds(new Integer(rs.getInt("PK_USER")));
                
                setUsrLastNames(rs.getString("USR_LASTNAME"));
                setUsrFirstNames(rs.getString("USR_FIRSTNAME"));
                setUsrMidNames(rs.getString("USR_MIDNAME"));
                setUsrSiteNames(rs.getString("site_name"));
                
                //JM: 
                setUsrStats(rs.getString("USR_STAT"));
                setUsrTypes(rs.getString("USR_TYPE"));
                

                rows++;
                Rlog
                        .debug("user", "UserDao.searchAccountUsers rows "
                                + rows);

            }
            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("user",
                    "UserDao.searchAccountUsers  EXCEPTION IN FETCHING FROM User table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }   
    
    /** ******* */

    public void getUsersWithStudyCopy(String studyId, String accountId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("select usr_lastname, "
                    + "usr_firstname, " + "'1' inExUsers  " + "from er_user "
                    + "where pk_user in " + "(select fk_author "
                    + "from er_study " + "where STUDY_PARENTID = ?) "
                    + "and fk_account = ?" + "Union " + "select usr_lastname, "
                    + "usr_firstname, " + "'2' inExUsers " + "from er_user "
                    + "where pk_user in " + "(select fk_author "
                    + "from er_study " + "where STUDY_PARENTID = ?) "
                    + "and fk_account <> ?"
                    + "order by usr_lastname, usr_firstname");
            pstmt.setInt(1, StringUtil.stringToNum(studyId));
            pstmt.setInt(2, StringUtil.stringToNum(accountId));
            pstmt.setInt(3, StringUtil.stringToNum(studyId));
            pstmt.setInt(4, StringUtil.stringToNum(accountId));

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setUsrLastNames(rs.getString("usr_lastname"));
                setUsrFirstNames(rs.getString("usr_firstname"));
                setInexUsers(rs.getString("inExUsers"));
                rows++;
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("user",
                    "UserDao.getUsersWithStudyCopy EXCEPTION IN FETCHING FROM User table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
    }

    /**
     * Sets all those Account Users
     * 
     * @param accId
     *            Account Id
     * @param userPin
     *            User Pin number(to search for the user)
     */

    public void searchAccountUsersByPin(int accId, String userPin) {
        Rlog.debug("user", "UserDao.searchAccountUsersByPin ");
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String lWhere = null;
        String fWhere = null;

        try {
            conn = getConnection();

            String selectSt = "SELECT ER_USER.pk_user, "
                    + "ER_USER.USR_LASTNAME, " + "ER_USER.USR_FIRSTNAME, "
                    + "ER_USER.USR_MIDNAME " + " from ER_USER "
                    + " Where USR_STAT = 'A' " + "and ER_USER.USR_TYPE <> 'X' "

                    + "and ER_USER.fk_account <> ? "
                    + "and ER_USER.USR_CODE = ?";

            pstmt = conn.prepareStatement(selectSt.toString());

            Rlog.debug("user", "UserDao.searchAccountUsersByPin "
                    + selectSt.toString());

            pstmt.setInt(1, accId);
            pstmt.setString(2, userPin);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setUsrIds(new Integer(rs.getInt("PK_USER")));

                setUsrLastNames(rs.getString("USR_LASTNAME"));
                setUsrFirstNames(rs.getString("USR_FIRSTNAME"));
                setUsrMidNames(rs.getString("USR_MIDNAME"));

                rows++;
                Rlog.debug("user", "UserDao.searchAccountUsersByPin rows "
                        + rows);

            }
            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("user",
                    "UserDao.searchAccountUsersByPin  EXCEPTION IN FETCHING FROM User table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /** ******* */

    /**
     * Gets the users assigned to the event as resource or for mailing
     * 
     * @param eventId
     *            Event Id
     * @param userType
     * 
     */

    public void getUsersForEvent(int eventId, String userType) {
        Rlog.debug("user", "UserDao.getUsersForEvent ");
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();

            String selectSt = "select pk_user, "
                    + "usr_lastname, "
                    + "(select SITE_NAME from er_site where PK_SITE  = FK_SITEID) as site_name, "
                    + "usr_firstname " + "from 	er_user "
                    + "where 	pk_user in (select eventusr "
                    + "from	sch_eventusr " + "where 	fk_event = "
                    + eventId + " " + "and 	eventusr_type = '" + userType
                    + "') ";

            pstmt = conn.prepareStatement(selectSt.toString());

            Rlog.debug("user", "UserDao.getUsersForEvent "
                    + selectSt.toString());

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setUsrIds(new Integer(rs.getInt("PK_USER")));
                setUsrLastNames(rs.getString("USR_LASTNAME"));
                setUsrSiteNames(rs.getString("site_name"));
                setUsrFirstNames(rs.getString("USR_FIRSTNAME"));
                rows++;
            }
            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("user",
                    "UserDao.getUsersForEvent  EXCEPTION IN FETCHING FROM User table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Gets all those Users which can be assigned to an event
     * 
     * @param accId
     *            Account Id
     * @param lName
     *            Last Name string (to search for the user)
     * @param fName
     *            First name string(to search for the user)
     * @param userType
     *            Type of User in the event user table U, R
     * @param eventId
     *            Event Id
     */
    public void getAvailableEventUsers(int accId, String lName, String fName,
            String role, String organization, String userType, int eventId) {
        Rlog.debug("user", "UserDao.getAvailableEventUsers ");
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String lWhere = null;
        String fWhere = null;
        String oWhere = null;
        String rWhere = null;
        StringBuffer completeSelect = new StringBuffer();

        try {
            conn = getConnection();

            String selectSt = "SELECT ER_USER.pk_user,ER_USER.USR_LASTNAME, "
                    + "(select SITE_NAME from er_site where PK_SITE  = FK_SITEID) as site_name, "
                    + "ER_USER.USR_FIRSTNAME " + " from ER_USER "
                    + " Where ER_USER.fk_account = ? "
                    + "and ER_USER.USR_TYPE <> 'X' "
                    + " and ER_USER.usr_stat in('A','B','D')";

            String endSelect = " and pk_user not in (select eventusr from sch_eventusr where "
                    + "eventusr_type = ? " + "and fk_event = ?)";

            if ((organization != null) && (!organization.trim().equals(""))) {
                oWhere = " and UPPER(ER_USER.FK_SITEID) = UPPER(";
                oWhere += organization;
                oWhere += ")";
            }

            if ((role != null) && (!role.trim().equals(""))) {
                rWhere = " and UPPER(ER_USER.FK_CODELST_JOBTYPE) = UPPER(";
                rWhere += role;
                rWhere += ")";
            }

            if ((fName != null) && (!fName.trim().equals(""))) {
                fWhere = " and UPPER(ER_USER.USR_FIRSTNAME) like UPPER('";
                fWhere += fName;
                fWhere += "%')";
            }

            if ((lName != null) && (!lName.trim().equals(""))) {
                lWhere = " and UPPER(ER_USER.USR_LASTNAME) like UPPER('";
                lWhere += lName;
                lWhere += "%')";
            }

            completeSelect.append(selectSt);

            if (fWhere != null) {
                completeSelect.append(fWhere);
            }

            if (lWhere != null) {
                completeSelect.append(lWhere);
            }

            if (oWhere != null) {
                completeSelect.append(oWhere);
            }

            if (rWhere != null) {
                completeSelect.append(rWhere);
            }

            completeSelect.append(endSelect);

            pstmt = conn.prepareStatement(completeSelect.toString());

            Rlog.debug("user", "UserDao.getAvailableEventUsers users "
                    + completeSelect.toString());

            pstmt.setInt(1, accId);
            pstmt.setString(2, userType);
            pstmt.setInt(3, eventId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setUsrIds(new Integer(rs.getInt("PK_USER")));

                setUsrLastNames(rs.getString("USR_LASTNAME"));
                setUsrFirstNames(rs.getString("USR_FIRSTNAME"));
                setUsrSiteNames(rs.getString("site_name"));
                rows++;
                Rlog.debug("user", "UserDao.getAvailableEventUsers rows "
                        + rows);

            }
            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("user",
                    "UserDao.getAvailableEventUsers  EXCEPTION IN FETCHING FROM User table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /** ******* */

    /**
     * Sets all those Group Users
     * 
     * @param accId
     *            Account Id
     * @param lName
     *            Last Name string (to search for the user)
     * @param fName
     *            First name string(to search for the user)
     * @param grpId
     *            Group Id
     */

    public void searchGroupUsers(int accId, String lName, String fName,
            String role, String organization, int grpId) {
        Rlog.debug("user", "UserDao.Search Group users ");
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String lWhere = null;
        String fWhere = null;
        String oWhere = null;
        String rWhere = null;
        StringBuffer completeSelect = new StringBuffer();

        try {
            conn = getConnection();

            String selectSt = "SELECT ER_USER.pk_user,ER_USER.USR_LASTNAME, "
                    + "(select SITE_NAME from er_site where PK_SITE  = FK_SITEID) as site_name, "
                    + "ER_USER.USR_FIRSTNAME, ER_USER.USR_MIDNAME "
                    + " from ER_USER "
                    + " Where USR_STAT in('A','B','D') and USR_TYPE <> 'N' and USR_TYPE <> 'X' and ER_USER.fk_account = ? ";

            if ((organization != null) && (!organization.trim().equals(""))) {
                oWhere = " and UPPER(ER_USER.FK_SITEID) = UPPER(";
                oWhere += organization;
                oWhere += ")";
            }

            if ((role != null) && (!role.trim().equals(""))) {
                rWhere = " and UPPER(ER_USER.FK_CODELST_JOBTYPE) = UPPER(";
                rWhere += role;
                rWhere += ")";
            }

            if ((fName != null) && (!fName.trim().equals(""))) {
                fWhere = " and UPPER(ER_USER.USR_FIRSTNAME) like UPPER('";
                fWhere += fName;
                fWhere += "%')";
            }

            if ((lName != null) && (!lName.trim().equals(""))) {
                lWhere = " and UPPER(ER_USER.USR_LASTNAME) like UPPER('";
                lWhere += lName;
                lWhere += "%')";
            }

            completeSelect.append(selectSt);

            if (fWhere != null) {
                completeSelect.append(fWhere);
            }

            if (lWhere != null) {
                completeSelect.append(lWhere);
            }

            if (oWhere != null) {
                completeSelect.append(oWhere);
            }

            if (rWhere != null) {
                completeSelect.append(rWhere);
            }

            lWhere = " AND ER_USER.PK_USER NOT IN ( SELECT FK_USER FROM ER_USRGRP WHERE FK_GRP = ?)";

            if (lWhere != null) {
                completeSelect.append(lWhere);
            }

            pstmt = conn.prepareStatement(completeSelect.toString());

            pstmt.setInt(1, accId);
            pstmt.setInt(2, grpId);
            Rlog.debug("user", "UserDao.search Group users "
                    + completeSelect.toString());

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setUsrIds(new Integer(rs.getInt("PK_USER")));

                setUsrLastNames(rs.getString("USR_LASTNAME"));
                setUsrFirstNames(rs.getString("USR_FIRSTNAME"));
                setUsrMidNames(rs.getString("USR_MIDNAME"));

                setUsrSiteNames(rs.getString("site_name"));

                rows++;
                Rlog
                        .debug("user", "UserDao.getAvailableTeamUsers rows "
                                + rows);

            }
            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("user",
                    "UserDao.getAvailableTeamUsers  EXCEPTION IN FETCHING FROM User table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /** ******* */

    /**
     * Gets all Users in a Site(Organization)
     * 
     * @param siteId
     *            Site Id
     */
    public void getSiteUsers(int siteId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn
                    .prepareStatement("SELECT ER_USER.pk_user,"
                            + "ER_USER.USR_LASTNAME, "
                            + "ER_USER.USR_FIRSTNAME, "
                            + "ER_USER.USR_MIDNAME "
                            + "FROM ER_USER "
                            + "Where ER_USER.FK_SITEID = ? "
                            + "and ER_USER.USR_TYPE <> 'X' "
                            + "order by lower(USR_FIRSTNAME || ' ' || usr_lastname) ASC");

            pstmt.setInt(1, siteId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setUsrIds(new Integer(rs.getInt("PK_USER")));
                setUsrLastNames(rs.getString("USR_LASTNAME"));
                setUsrFirstNames(rs.getString("USR_FIRSTNAME"));
                setUsrMidNames(rs.getString("USR_MIDNAME"));
                rows++;
                Rlog.debug("user", "UserDao.getSiteUsers rows " + rows);

            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("user",
                    "UserDao.getSiteUsers EXCEPTION IN FETCHING FROM User table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Search for the team users for study studyId that are with last name,
     * first name as lname, fname and the id is not in ids
     * 
     * @param studyId
     *            Study Id
     * @param lName
     *            Last Name string (to search for the user)
     * @param fName
     *            First name string(to search for the user)
     * @param ids
     *            Comma separated list of ids that have already been selected
     * @param accId
     *            account Id
     */

    /*
     * Modified by sonia, 08/06/04 added order by user name Modified by Anu,
     * 09/14/04 . To display all non system, active and blocked users
     */
    /*
     * JM: 11/10/2005 Modified: group and study parameter added 
     */

    public void searchTeamUsers(String studyId, String lName, String fName,
            String role, String organization, String ids, String accId, String group, String sTeam) {
        Rlog.debug("user", "UserDao.searchTeamUsers");
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String lWhere = null;
        String fWhere = null;
        String oWhere = null;
        String rWhere = null;
        String gWhere = null;
        String sWhere = null;
        StringBuffer completeSelect = new StringBuffer();

        try {
            conn = getConnection();

            String selectSt = "SELECT DISTINCT u.pk_user,u.USR_LASTNAME, "
                    + "(select SITE_NAME from er_site where PK_SITE  = FK_SITEID) as site_name, "
                    + " u.USR_FIRSTNAME, u.USR_MIDNAME, u.USR_STAT, u.USR_TYPE   "
                    + " from er_site s, ER_USER u left outer  join  er_studyteam et on u.pk_user=et.fk_user "
                    + " where u.fk_account = ?"
                    + " and u.fk_siteid = s.pk_site "
                    + " and u.USR_TYPE <> 'X' "
                    + " and u.usr_stat in('A','B')";

            if ((organization != null) && (!organization.trim().equals(""))) {
                oWhere = " and UPPER(u.FK_SITEID) = UPPER(";
                oWhere += organization;
                oWhere += ")";
            }

            if ((role != null) && (!role.trim().equals(""))) {
                rWhere = " and UPPER(u.FK_CODELST_JOBTYPE) = UPPER(";
                rWhere += role;
                rWhere += ")";
            }

            if ((fName != null) && (!fName.trim().equals(""))) {
                fWhere = " and UPPER(u.USR_FIRSTNAME) like UPPER('";
                fWhere += fName;
                fWhere += "%')";
            }

            if ((lName != null) && (!lName.trim().equals(""))) {
                lWhere = " and UPPER(u.USR_LASTNAME) like UPPER('";
                lWhere += lName;
                lWhere += "%')";
            }
            
             
//JM: 11/10/2005 
            if ((group != null) && (!group.trim().equals("") && !group.trim().equals("null")))
			{			
				gWhere =" and UPPER(u.fk_grp_default) = upper(";
				gWhere +=group;
				gWhere +=")";
			}
			
			if ((sTeam != null) && (!sTeam.trim().equals("") && !sTeam.trim().equals("null")))
			{			
				sWhere =" and upper(et.fk_study)=upper(";
				sWhere +=sTeam;
				sWhere +=")";
			}			


            completeSelect.append(selectSt);

            if ((ids != null) && (!ids.trim().equals("")))
                completeSelect.append(" and u.pk_user not in (" + ids
                        + ")");

            if (fWhere != null) {
                completeSelect.append(fWhere);
            }

            if (lWhere != null) {
                completeSelect.append(lWhere);
            }

            if (oWhere != null) {
                completeSelect.append(oWhere);
            }

            if (rWhere != null) {
                completeSelect.append(rWhere);
            }
            
            if (gWhere != null) {
                completeSelect.append(gWhere);
            }
            
            if (sWhere != null) {
                completeSelect.append(sWhere);
            }

            // add order by

            completeSelect
                    .append(" ORDER BY lower(u.USR_FIRSTNAME || ' ' || u.usr_lastname) ASC ");

            pstmt = conn.prepareStatement(completeSelect.toString());

            Rlog.debug("user", "UserDao.searchTeamUsers"
                    + completeSelect.toString());

            pstmt.setInt(1, StringUtil.stringToNum(accId));

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setUsrIds(new Integer(rs.getInt("PK_USER")));

                setUsrLastNames(rs.getString("USR_LASTNAME"));

                setUsrFirstNames(rs.getString("USR_FIRSTNAME"));
                setUsrMidNames(rs.getString("USR_MIDNAME"));

                setUsrSiteNames(rs.getString("site_name"));
//              JM: 11/10/2005
                setUsrStats(rs.getString("USR_STAT"));
                setUsrTypes(rs.getString("USR_TYPE"));
                

                rows++;
                Rlog.debug("user", "UserDao.searchTeamUsers rows " + rows);

            }
            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("user",
                    "UserDao.searchTeamUsers  EXCEPTION IN FETCHING FROM User table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Get the names of the Users from the Database when we specify the userids
     * as comma separated ids
     * 
     * @param userIdList
     */

    public String getUserNamesFromIds(String userIdList) {

        Rlog.debug("user", "UserDao.GetUserNamesFromIds");
        PreparedStatement pstmt = null;
        Connection conn = null;
        String userNameList = null;

        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("SELECT USR_LST(?) from dual");

            pstmt.setString(1, userIdList);
            ResultSet rs = pstmt.executeQuery();

            rs.next();
            userNameList = rs.getString(1);

            return userNameList;
        } catch (SQLException ex) {
            Rlog.fatal("user",
                    "UserDao.getUserNamesFromIds  EXCEPTION IN FETCHING FROM USR_LST FUNCTION"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        return userNameList;
    }

    /** ******************* */

    /**
     * Sets all Users for the Groups in the attributes of this class
     * 
     * @param groupIds
     *            Group Ids
     */
    public void getGroupUsers(String groupIds) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = "";

        try {
            conn = getConnection();

            sql = "SELECT  PK_USER,GRP_NAME, USR_LASTNAME,  USR_FIRSTNAME,  USR_MIDNAME "
                    + " FROM ER_USER,ER_GRPS,ER_USRGRP "
                    + " WHERE ER_GRPS.PK_GRP=ER_USRGRP.FK_GRP "
                    + " AND ER_USRGRP.FK_USER=ER_USER.PK_USER "
                    + " AND ER_GRPS.PK_GRP IN("
                    + groupIds
                    + ")   ORDER BY ER_USER.FK_GRP_DEFAULT";

            Rlog.debug("user", "in getGroupUsers in UserDao sql" + sql);

            pstmt = conn.prepareStatement(sql);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setUsrIds(new Integer(rs.getInt("PK_USER")));
                setUsrGrpNames(rs.getString("GRP_NAME"));
                setUsrLastNames(rs.getString("USR_LASTNAME"));
                setUsrFirstNames(rs.getString("USR_FIRSTNAME"));
                setUsrMidNames(rs.getString("USR_MIDNAME"));

                rows++;
                Rlog.debug("user", "UserDao.getGroupUsers rows " + rows);

            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("user",
                    "UserDao.getGroupUsers EXCEPTION IN FETCHING FROM User table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /** ******************* */

    /**
     * Sets all Users for the Study in the attributes of this class
     * 
     * @param studyIds
     *            study Ids
     */
    public void getStudyUsers(String studyIds) {
        Rlog.debug("user", "inside getStudyUsers in UserDao");
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = "";

        try {

            conn = getConnection();

            sql = "SELECT US.PK_USER , US.USR_FIRSTNAME , US.USR_LASTNAME, "
                    + " ST2.STUDY_NUMBER FROM ER_STUDYTEAM ST , ER_USER US , ER_STUDY ST2 "
                    + " WHERE ST.FK_USER = US.PK_USER "
                    + " AND ST.FK_STUDY = ST2.PK_STUDY "
                    + " AND ST.FK_STUDY IN("
                    + studyIds
                    + ") "
                    + " AND NVL(ST.STUDY_TEAM_USR_TYPE,'D')='D' ORDER BY ST.FK_STUDY";

            pstmt = conn.prepareStatement(sql);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setUsrFirstNames(rs.getString("USR_FIRSTNAME"));
                setUsrLastNames(rs.getString("USR_LASTNAME"));
                setUsrStudyNumbers(rs.getString("STUDY_NUMBER"));
                rows++;

            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("user",
                    "UserDao.getStudyUsers EXCEPTION IN FETCHING FROM User table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /** ******************* */

    /**
     * Sets all Users for the Organization
     * 
     * @param orgIds
     *            organization Ids
     */

    public void getOrganizationUsers(String orgIds) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = "";
        try {
            conn = getConnection();
            //KM-10Jun10-#2555
            sql = "select * from (SELECT  US.PK_USER, US.USR_FIRSTNAME , US.USR_LASTNAME , ST.SITE_NAME, us.fk_siteid site " +
            		"FROM ER_USER US, ER_SITE ST WHERE US.FK_SITEID = ST.PK_SITE AND US.FK_SITEID IN ("+ orgIds +") and us.usr_type <> 'X' " +
            		"UNION select pk_user, usr_firstname, usr_lastname, site_name, b.pk_site site  " +
            		"from er_user a, erv_allusersites_distinct b where b.pk_site in ("+ orgIds +") and b.fk_user = a.pk_user and a.usr_type <> 'X')  " +
            		"order by site";

            Rlog.debug("user", "getOrganizationUsers sql" + sql);

            pstmt = conn.prepareStatement(sql);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setUsrFirstNames(rs.getString("USR_FIRSTNAME"));
                setUsrLastNames(rs.getString("USR_LASTNAME"));
                setUsrSiteNames(rs.getString("SITE_NAME"));
                rows++;

            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("user",
                    "UserDao.getOrgUsers EXCEPTION IN FETCHING FROM User table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /*
     * Modified by Sonia - 08/06/04 Changed method Name
     * getActivatedDeactivatedUsers() to getUsersSelectively() added javadoc,
     * added order type ASC on name and used StringBuffer for SQL
     */

    /**
     * Populates the data Access Object with users of an account for a list of
     * status types
     * 
     * @param accId
     *            Account Id
     * @param status
     *            a comma separated list of status sun types. Possible Values:
     *            <BR>
     *            A - Active <br>
     *            B - Blocked <br>
     *            D - Deactivated <br>
     *            P - Pending <br>
     *            X - Deleted <br>
     *            Example a string of "'D','B'" will return Blocked and
     *            Deactivated users
     */
    
    //Modified by Manimaran to include first name and  last name in search criteria.
    public void getUsersSelectively(int accId, String status, String firstName, String lastName) {

        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            StringBuffer sbSQL = new StringBuffer();

            sbSQL
                    .append("SELECT ER_USER.PK_USER,ER_USER.USR_STAT,ER_USER.USR_LASTNAME,ER_USER.USR_FIRSTNAME, ");
            sbSQL
                    .append("ER_USER.USR_MIDNAME, ER_CODELST.CODELST_DESC as job_type, ");
            sbSQL.append(" ER_SITE.SITE_NAME  ");
            sbSQL.append(" FROM ER_USER, ER_SITE,ER_CODELST  ");
            sbSQL.append(" Where ER_USER.fk_account = ?  ");
            sbSQL.append(" and ER_USER.usr_stat in (" + status + ")");
            sbSQL.append(" and ER_USER.USR_TYPE <> 'X' and ER_USER.USR_TYPE <> 'N' "); //KM-3359
            sbSQL.append(" and ER_SITE.PK_SITE = ER_USER.FK_SITEID");
            sbSQL
                    .append(" and ER_CODELST.PK_CODELST (+) = ER_USER.FK_CODELST_JOBTYPE  ");
            
            if ((firstName != null) && (!firstName.trim().equals(""))) {
            	sbSQL.append(" and upper(ER_USER.USR_FIRSTNAME) like upper('%"+firstName+"%') ");
            }
            
            if ((lastName != null) && (!lastName.trim().equals(""))) {
            	sbSQL.append(" and upper(ER_USER.USR_LASTNAME) like upper('%"+lastName+"%') ");
            }
            
            
            sbSQL
                    .append(" order by lower(USR_FIRSTNAME || ' ' || usr_lastname)  ");

            pstmt = conn.prepareStatement(sbSQL.toString());
            pstmt.setInt(1, accId);
            // pstmt.setString(2,status);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setUsrIds(new Integer(rs.getInt("PK_USER")));
                setUsrStats(rs.getString("USR_STAT")); // JM: 18/08/05
                setUsrSiteNames(rs.getString("site_name"));
                setUsrLastNames(rs.getString("USR_LASTNAME"));
                setUsrFirstNames(rs.getString("USR_FIRSTNAME"));
                setUsrJobTypes(rs.getString("job_type"));
                setUsrMidNames(rs.getString("USR_MIDNAME"));
                rows++;
                Rlog.debug("user", "UserDao.getActivatedDeactivatedUsers rows "
                        + rows);

            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("user",
                    "UserDao.getActivatedDeactivatedUsers EXCEPTION IN FETCHING FROM User table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Populates the Data Access Object with users of an account and a specified
     * user type
     * 
     * @param accId
     *            Account Id
     * @param userType
     *            User Type. Possible Values: <BR>
     *            N - Non system user(cannot login but appears in user list)<br>
     *            S - System User <br>
     *            X - Non system user has been logically deleted
     */
    /*
     * Changed on 08/06/04 By Sonia Sahni Added javadoc, sorted by user name
     * Used StringBuffer instead of adding up String objects
     */
    
    //Modified by Manimaran to include first name and  last name in search criteria.
    public void getNonSystemUsers(int accId, String userType, String firstName, String lastName) {

        Rlog.debug("user", "inside getNonSystemUsers");
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            StringBuffer sbSQL = new StringBuffer();

            sbSQL
                    .append("SELECT ER_USER.PK_USER,ER_USER.USR_LASTNAME,ER_USER.USR_FIRSTNAME,");
            sbSQL
                    .append("ER_USER.USR_MIDNAME, ER_USER.USR_STAT, ER_CODELST.CODELST_DESC  as job_type,"); //KM-031108
            sbSQL.append(" ER_SITE.SITE_NAME as site_name ");
            sbSQL.append(" FROM ER_USER,ER_CODELST,ER_SITE");
            sbSQL.append(" Where ER_user.fk_account = ? ");
            sbSQL.append(" and ER_USER.usr_type = ? ");
            sbSQL
                    .append("  and ER_CODELST.PK_CODELST (+) = ER_USER.FK_CODELST_JOBTYPE ");
            sbSQL.append("  and ER_SITE.PK_SITE = ER_USER.FK_SITEID  ");
            if ((firstName != null) && (!firstName.trim().equals(""))) {
            	sbSQL.append(" and upper(ER_USER.USR_FIRSTNAME) like upper('%"+firstName+"%') ");
            }
            
            if ((lastName != null) && (!lastName.trim().equals(""))) {
            	sbSQL.append(" and upper(ER_USER.USR_LASTNAME) like upper('%"+lastName+"%') ");
            }
            
            sbSQL
                    .append("  ORDER BY lower(USR_FIRSTNAME || ' ' || usr_lastname) ASC ");

            pstmt = conn.prepareStatement(sbSQL.toString());

            pstmt.setInt(1, accId);
            pstmt.setString(2, userType);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setUsrIds(new Integer(rs.getInt("PK_USER")));
                setUsrSiteNames(rs.getString("site_name"));
                setUsrLastNames(rs.getString("USR_LASTNAME"));
                setUsrFirstNames(rs.getString("USR_FIRSTNAME"));
                setUsrJobTypes(rs.getString("job_type"));
                setUsrMidNames(rs.getString("USR_MIDNAME"));
                setUsrStats(rs.getString("USR_STAT"));//KM-031108
                rows++;
                Rlog.debug("user", "UserDao.getNonSystemUsers rows " + rows);

            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("user",
                    "UserDao.getNonSystemUsers EXCEPTION IN FETCHING FROM User table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    // //////////////////

    /**
     * Gets the user PK
     * 
     * @param firstName
     *            User's first Name
     * @param firstName
     *            User's last Name
     * @return user Primary Key
     */

    public int getUserPK(String firstName, String lastName) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        int rows = 0;
        int userPk = 0;
        try {
            conn = EnvUtil.getConnection();

            pstmt = conn
                    .prepareStatement("select pk_user from er_user where lower(trim(USR_FIRSTNAME)) = lower(trim(?)) and lower(trim(USR_LASTNAME)) = lower(trim(?)) ");
            pstmt.setString(1, firstName);
            pstmt.setString(2, lastName);

            ResultSet rs = pstmt.executeQuery();

            if (rs != null) {
                while (rs.next()) {
                    userPk = rs.getInt(1);
                    rows++;
                    Rlog.debug("user",
                            "UserDao.getUserPK(String firstName, String lastName) userPk"
                                    + userPk);
                }
              //Raman:Bug#11473
                setCRows(rows);
            }
            return userPk;
        } catch (SQLException ex) {
            Rlog.fatal("user",
                    "UserDao.getUserPK(String firstName, String lastName) EXCEPTION "
                            + ex);
            return userPk;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public static String getUserFullName(int userPk) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sb = new StringBuffer();
        try {
            conn = EnvUtil.getConnection();
            pstmt = conn
                    .prepareStatement("select USR_FIRSTNAME, USR_LASTNAME from er_user where PK_USER = ? ");
            pstmt.setInt(1, userPk);
            ResultSet rs = pstmt.executeQuery();

            if (rs != null) {
                while (rs.next()) {
                    if (rs.getString(1) != null && rs.getString(1).length() > 0) {
                        sb.append(rs.getString(1)).append(" ");
                    }
                    if (rs.getString(2) != null) {
                        sb.append(rs.getString(2));
                    }
                }
            }
        } catch (SQLException ex) {
            Rlog.fatal("user","UserDao.getUserFullName EXCEPTION "+ ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {}
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {}
        }
        return sb.toString();
    }

    /**
     * Gets the user PK
     * 
     * @param firstName
     *            User's first Name
     * @param firstName
     *            User's last Name
     * @param userEmail
     *            User's email address
     * @return user Primary Key
     */

    public static int getUserPK(String firstName, String lastName,
            String userEmail) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        int userPk = 0;
        try {
            conn = EnvUtil.getConnection();

            pstmt = conn
                    .prepareStatement("select pk_user from er_user , er_add where lower(trim(USR_FIRSTNAME)) = lower(trim(?)) and lower(trim(USR_LASTNAME)) = lower(trim(?)) and pk_add = FK_PERADD and trim(lower(ADD_EMAIL)) = trim(lower(?))  ");
            pstmt.setString(1, firstName);
            pstmt.setString(2, lastName);
            pstmt.setString(3, userEmail);

            ResultSet rs = pstmt.executeQuery();

            if (rs != null) {
                while (rs.next()) {
                    userPk = rs.getInt(1);
                    Rlog.debug("user",
                            "UserDao.getUserPK(String firstName, String lastName, String userEmail  )"
                                    + userPk);
                }
            }
            return userPk;
        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "user",
                            "UserDao.getUserPK(String firstName, String lastName, String userEmail  ) EXCEPTION "
                                    + ex);
            return userPk;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    
  
//  JM: 06/09/05 procedure to change the non sytem user to 'Active User'
    public void changeUserType(int userId, int group, String usrLoginName, String usrPass,String usrESign,String usrMail,int loggedInUser){    	
     
    CallableStatement cstmt = null;
    Connection conn = null; 
     
    
     try{
    
   	conn  = EnvUtil.getConnection();
   	cstmt = conn.prepareCall("{call SP_CONVERT_NONSYSTEMUSER(?,?,?,?,?,?,?)}");
   	
   	cstmt.setInt(1,userId);
   	cstmt.setInt(2,group);
   	cstmt.setString(3,usrLoginName);
   	cstmt.setString(4,usrPass);
   	cstmt.setString(5,usrESign);
   	cstmt.setString(6,usrMail);
   	cstmt.setInt(7,loggedInUser);
   	cstmt.execute();	
    Rlog.debug("user", "UserDao.changeUserType()");
    
     }
     catch(Exception e) {
   			Rlog.fatal("User","EXCEPTION in SP_CONVERT_NONSYSTEMUSER, excecuting Stored Procedure " + e);

     
     }
     finally{
   		try
   		{
   			if (cstmt!= null) cstmt.close();
   		}
   		catch (Exception e) 
   		{
   		}
   		try 
   		{
   			if (conn!=null) conn.close();
   		}
   		catch (Exception e) {}
     
     }
     
    }
    
    //added by sonia abrol 10/23/07 to get user's default site
    /**
     * Returns the User's default site 
     * 
     * @param userId
     *            The User ID 
     * @return String The User default site 
     */
    public static String getUserDefaultSite(String userId) {
        String usersite = "";
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("select fk_siteid  from er_user where pk_user = ?");
            pstmt.setString(1, userId);

            ResultSet rs = pstmt.executeQuery();

            rs.next();
            usersite = rs.getString("fk_siteid");

        } catch (SQLException ex) {
            Rlog.fatal("user",
                    "UserDao.getUserDefaultSite EXCEPTION IN FETCHING FROM User table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        return usersite;
    }
    
    
    /**
     * Returns the number of users with the same first name and last name combination in the same account
     * 
     * @param userFanme
     *            user first name which needs to be verified if exists
     * @param userLanme  
     * 			  user last name which needs to be verified if exists      
     * @param accId 
     * 			  user's account id   
     * 
     * @param mode   
     * 			  mode 
     * 
     * @param userId   
     * 				user id, in edit mode
     *            
     * @return int The number of users with the same first name and last name combination or email id provided 
     */
    public int getCount(String userLname, String userFname, String accId, String mode, String userId) {
        int count = 0;
        String userIdReturned = "";
        PreparedStatement pstmt = null;        
        Connection conn = null;
        
        try {
            conn = getConnection();
            //KM-3257	
            pstmt = conn.prepareStatement("select  max(pk_user) as userid, count(*) as count from er_user "
            		+ " where lower(trim(USR_LASTNAME)) = lower(trim(?)) and lower(trim(USR_FIRSTNAME)) = lower(trim(?))" 
            		+ " and fk_account=? and usr_type <> 'X'");
                                  
                       
            pstmt.setString(1, userLname);
            pstmt.setString(2, userFname);            
            pstmt.setString(3, accId);
            

            ResultSet rs = pstmt.executeQuery();

            rs.next();
            
            count = rs.getInt("count");            
            
            
            userIdReturned = rs.getString("userid");
            if (mode.equals("M") && count==1 && userId.equals(userIdReturned)){   	
            	count =0;
            }

        } catch (SQLException ex) {
            Rlog.fatal("user",
                    "UserDao.getCount EXCEPTION IN FETCHING FROM User table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
                	
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
                	
            } catch (Exception e) {
            }

        }
        return count;
    }
    
    public int getCount(String email, String accId, String mode, String userId) {
        int count = 0;
        String userIdReturned = "";
        
        PreparedStatement pstmt = null;
        Connection conn = null;     
        
        
        try {
            conn = getConnection();
            //KM-3257	
            pstmt = conn.prepareStatement("select max(pk_user) as userid, count(*) as count from er_user a, er_add b "
            		+ " where a.fk_peradd = b.pk_add and lower(trim(b.ADD_EMAIL)) = lower(trim(?)) and a.fk_account=? and usr_type <> 'X' ");
           
                       
            
            pstmt.setString(1, email);
            pstmt.setString(2, accId);
            

            ResultSet rs = pstmt.executeQuery();

            rs.next();
            
            count = rs.getInt("count");     
            
            
            
            userIdReturned = rs.getString("userid");
             
            if (mode.equals("M") && count==1 && userId.equals(userIdReturned)){ 
            	 count = 0;            	 
             }
                 
                 


        } catch (SQLException ex) {
            Rlog.fatal("user",
                    "UserDao.getCount EXCEPTION IN FETCHING FROM User table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();               
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();                
            } catch (Exception e) {
            }

        }
        return count;
    }
   
    
 	 
    public int deactivateUser(String userPk, String creator, String ipAdd){    	
     
    CallableStatement cstmt = null;
    Connection conn = null; 
     int ret = 0;
    
     try{
    
   	conn  = getConnection();
   	cstmt = conn.prepareCall("{call pkg_user.SP_deactivate_user(?,?,?,?)}");
   	
   	
   	cstmt.setString(1,userPk);
   	cstmt.setString(2,creator);
   	cstmt.setString(3,ipAdd);
    
   	cstmt.registerOutParameter(4, java.sql.Types.INTEGER);
   	     
    
   	cstmt.execute();
   	
   	ret = cstmt.getInt(4);
    Rlog.debug("user", "UserDao.deactivateUser()");
    
     }
     catch(Exception e) {
   			Rlog.fatal("User","EXCEPTION in deactivateUser, excecuting Stored Procedure " + e);

     
     }
     finally{
   		try
   		{
   			if (cstmt!= null) cstmt.close();
   		}
   		catch (Exception e) 
   		{
   		}
   		try 
   		{
   			if (conn!=null) conn.close();
   		}
   		catch (Exception e) {}
     
     }
     
     return ret;
     
    }
    
    
    public int getResetNew(String login, String lname, String lcode ,String email){    	
        
    	 PreparedStatement pstmt = null;
        Connection conn = null; 
         int ret = 0;
        
         try{
        
       	conn  = getConnection();
       	pstmt = conn.prepareStatement("select usr.pk_user from er_add ad ,er_user usr "
                              + " where usr.fk_peradd = ad.pk_add  and usr.usr_logname = ? and ad.add_email=? and usr.usr_pahseinv = ? and usr.usr_lastname=?");
                             
       	
       	
       	pstmt.setString(1,login);
       	pstmt.setString(2,email);
       	pstmt.setString(3,lcode);
       	pstmt.setString(4,lname);
        
       
        ResultSet rs = pstmt.executeQuery();   
        
        if(rs.next()){        
        ret = rs.getInt("pk_user");
        }
       
        
        
         }
         catch(Exception e) {
       			Rlog.fatal("User","EXCEPTION in Reset New User " + e);

         
         }
         finally{
       		try
       		{
       			if (pstmt!= null) pstmt.close();
       		}
       		catch (Exception e) 
       		{
       		}
       		try 
       		{
       			if (conn!=null) conn.close();
       		}
       		catch (Exception e) {}
         
         }
         
         return ret;
         
        }
    
//APR-04-2011,MSG-TEMPLAGE FOR USER INFO VCTMS-1
    	/**
    	 * set the reset information for a user to be notified ;
    	 * 
    	 * @param pWord: password of the user to be notified.
    	 * @param url: url information.
    	 * @param eSign: e signature of the user.
    	 * @param userId: user id information.
    	 * @param userLogin: login id of the user.               

    	 * @see UserDao
    	 */
    	public int notifyUserResetInfo(String pWord, String url,String eSign,Integer userId,String userLogin,String infoFlag){
    			
    			
    		 int ret = 0;
    		 	CallableStatement cstmt = null;

    	        Rlog.debug("userdao", "notifyUserResetInfo");
    	        Connection conn = null;  
    	        try {
    	            conn = getConnection();
    	            cstmt = conn.prepareCall("{call PKG_USER.SP_NOTIFY_USER_INFO(?,?,?,?,?,?,?)}");

    	            cstmt.setString(1,pWord);
    	            cstmt.setString(2,url);
    	            cstmt.setString(3,eSign);
    	            cstmt.setInt(4,userId);
    	            cstmt.setString(5,userLogin);
    	            cstmt.setString(6,infoFlag);
    	            cstmt.registerOutParameter(7, java.sql.Types.INTEGER);

    	            cstmt.execute();

    	            Rlog.debug("notifyUserResetInfo", " after execution of PKG_USER.SP_NOTIFY_USER_RESETINFO");
    	            ret = cstmt.getInt(7);
    	            
    	            

    	        } catch (Exception ex) {
    	            Rlog.fatal("StudyNotifyDao",
    	                    "In notifyUserResetInfo in StudyNotifyDao: EXCEPTION IN calling the procedure"
    	                            + ex);
    	            
    	            ret = -1;

    	        } finally {
    	            try {
    	                if (cstmt != null)
    	                {
    	                    cstmt.close();
    	                }
    	            } catch (Exception e) {
    	            }
    	            try {
    	                if (conn != null)
    	                {
    	                    conn.close();}
    	            } catch (Exception e) {
    	            }
    	            
    	        }
    	        return ret;
    			
    			
    			
    			
    		}
    	//code developed by vivek
    	public int  updateAuditRow(int userId){
    		String idWithName=null;
    		 int raidId=0;
    		 UserJB userSK = new UserJB();
    		 UserBean usk=null;
    		 PreparedStatement pstmt = null;
    		 int rowImpact=0;
    		 Connection conn = null;
    		 raidId=getMaxRaidIdFromAuditRow();
			 idWithName=getModifyUser(userId);
    		
    		 try{
    			
    			 conn = getConnection();
    			 pstmt=conn.prepareStatement("UPDATE  ERES.AUDIT_ROW SET USER_NAME=? WHERE RAID=?") ;
    			 pstmt.setString(1, idWithName);
    			 pstmt.setInt(2, raidId);
    			 rowImpact=pstmt.executeUpdate();
    		 }catch(Exception ex){
    			 Rlog.fatal("StudyNotifyDao",
 	                    "In notifyUserResetInfo in StudyNotifyDao: EXCEPTION IN calling the procedure"
 	                            + ex); 
    		 }
    		 finally{
    		   		try
    		   		{
    		   			//if (cstmt!= null) cstmt.close();
    		   		}
    		   		catch (Exception e) 
    		   		{
    		   		}
    		   		try 
    		   		{
    		   			if (conn!=null) conn.close();
    		   		}
    		   		catch (Exception e) {}
    		     
    		     }
    		 return rowImpact;
    	}
    	public  int getMaxRaidIdFromAuditRow(){
    		 Connection conn = null;
    		 int raid=0;
    		 Statement stm=null;
    		 PreparedStatement pstmt = null;
    		 ResultSet rs=null;
    		 try{
    			 conn = getConnection();
    			 // stm=conn.createStatement();
    			 pstmt=conn.prepareStatement("select max(RAID) from ERES.audit_row where table_name='ER_USER'");
    			 rs=pstmt.executeQuery();
    			 // rs=stm.executeQuery("select max(RAID) from audit_row");
    			 while(rs.next()){
    				
    				 
    				raid= rs.getInt(1);
    				
    				 
    			 }
    			 
    		 }catch(Exception ex){
    			 Rlog.fatal("StudyNotifyDao",
 	                    "In notifyUserResetInfo in StudyNotifyDao: EXCEPTION IN calling the procedure"
 	                            + ex); 
    		 }
    		 finally{
 		   		try
 		   		{
 		   			if (pstmt!= null) pstmt.close();
 		   		}
 		   		catch (Exception e) 
 		   		{
 		   		}
 		   		try 
 		   		{
 		   			if (conn!=null) conn.close();
 		   		}
 		   		catch (Exception e) {}
 		   	try 
		   		{
		   			if (rs!=null) rs.close();
		   		}
		   		catch (Exception e) {}
 		     
 		     }
    		 
    		return raid;
    	}
    	public String getModifyUser(int userId){
    		String userIdWithName="";
    		 Connection conn = null;
    		 int raid=0;
    		 Statement stm=null;
    		 PreparedStatement pstmt = null;
    		 ResultSet rs=null;
    		 try{
    			 conn = getConnection();
    			 pstmt=conn.prepareStatement("select usr_lastname,usr_firstname from er_user where pk_user=?");
    			 pstmt.setInt(1, userId);
    			 rs=pstmt.executeQuery();
    			 while(rs.next()){
    				 userIdWithName=userIdWithName.concat(new Integer(userId).toString());
    				 userIdWithName=userIdWithName.concat(", ");
    				 userIdWithName=userIdWithName.concat(rs.getString(1));
    				 userIdWithName=userIdWithName.concat(", ");
    				 userIdWithName=userIdWithName.concat(rs.getString(2));
    			 }
    			 
    		 }catch(Exception ex){
    			 Rlog.fatal("StudyNotifyDao",
  	                    "In notifyUserResetInfo in StudyNotifyDao: EXCEPTION IN calling the procedure"
  	                            + ex); 
    		 }
    		 finally{
  		   		try
  		   		{
  		   			if (rs!=null) rs.close();
  		   			if (pstmt!= null) pstmt.close();
  		   			if (conn!=null) conn.close();
  		   		}
  		   		catch (Exception e) {}
  		     
  		     }
     		 
    		return userIdWithName;
    	}// end of code by vivek
     
    	public JSONArray  autocompleteUsers(String searchUser ,int account){
    		 JSONArray   maplist = new  JSONArray();
    		 JSONObject dataMap = null;
    		Connection conn = null;
    	   PreparedStatement pstmt = null;
    	   ResultSet rs = null;
    	   StringBuffer sqlBuffer = new StringBuffer();
    	   sqlBuffer.append("select pk_user,(usr_firstname || ' ' || usr_lastname) as name from er_user where fk_Account=? and (lower(usr_firstname || ' ' || usr_lastname) LIKE lower('%"+searchUser+"%'))");
    	   sqlBuffer.append(" AND USR_TYPE != 'P' order by lower(usr_firstname), lower(usr_lastname)");
    		try {
    			conn = CommonDAO.getConnection();
    			pstmt = conn.prepareStatement(sqlBuffer.toString());
    			pstmt.setInt(1, account);
    			rs = pstmt.executeQuery();

    			while (rs.next()) {
    				dataMap = new JSONObject();
    				dataMap.put("pk_user", rs.getInt(("pk_user")));
    				dataMap.put("name", rs.getString("name"));
    				maplist.put(dataMap);
    			}

    		} catch (Exception e) {
    			e.printStackTrace();
    		} finally {
    			try {
    				rs.close();
    				pstmt.close();
    				conn.close();
    			} catch (SQLException e) {
    				e.printStackTrace();
    			}
    		}
    		return maplist;
    	}
    }