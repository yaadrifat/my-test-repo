/*
 * Classname			CodelstBean.class
 * 
 * Version information
 *
 * Date					03/19/2001
 * 
 * Copyright notice
 */

package com.velos.eres.business.codelst.impl;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * The Codelst CMP entity bean.<br>
 * <br>
 * 
 * @author Dinesh
 */

@Entity
@Table(name = "er_codelst")
//Modified by Manimaran on 21,May06 for Role based Study Rights.
@NamedQueries( {
@NamedQuery(name = "findByUniqueCodelstSubtype", query = "SELECT OBJECT(subtype) FROM CodelstBean subtype where  trim(lower(codelst_subtyp)) = trim(lower(:codelst_subtyp)) and codelst_type='role'"),
@NamedQuery(name = "findByCodelstSubtype", query = "SELECT OBJECT(subtype) FROM CodelstBean subtype where  trim(lower(codelst_subtyp)) = trim(lower(:codelst_subtyp))")
})
public class CodelstBean implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 3546356240990286130L;

    /**
     * the codelst Id
     */
    public int clstId;

    /**
     * the codelst account ID
     */
    public Integer clstAccId;

    /**
     * the codelst account type
     */

    public String clstType;

    /**
     * the codelst Sub type
     */

    public String clstSubType;

    /**
     * the codelst description
     */
    public String clstDesc;

    /**
     * the codelst Hide
     */
    public String clstHide;

    /**
     * the codelst Seq
     */

    public Integer clstSeq;

    /**
     * the codelst Maintenance
     */
    public String clstMaint;

    /*
     * the record creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * the IP Address
     */
    public String ipAdd;

    // GETTER SETTER METHODS
    public CodelstBean() {

    }

    public CodelstBean(int clstId, String clstAccId, String clstType,
            String clstSubType, String clstDesc, String clstHide,
            String clstSeq, String clstMaint, String creator,
            String modifiedBy, String ipAdd) {
        super();
        // TODO Auto-generated constructor stub
        setClstId(clstId);
        setClstAccId(clstAccId);
        setClstType(clstType);
        setClstSubType(clstSubType);
        setClstDesc(clstDesc);
        setClstHide(clstHide);
        setClstSeq(clstSeq);
        setClstMaint(clstMaint);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
    }

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_CODELST", allocationSize=1)
    @Column(name = "PK_CODELST")
    public int getClstId() {
        return this.clstId;
    }

    public void setClstId(int clstId) {
        this.clstId = clstId;
    }

    @Column(name = "FK_ACCOUNT")
    public String getClstAccId() {
        return StringUtil.integerToString(this.clstAccId);
    }

    public void setClstAccId(String clstAccId) {
        if (clstAccId != null) {
            this.clstAccId = Integer.valueOf(clstAccId);
        }
    }

    @Column(name = "CODELST_TYPE")
    public String getClstType() {
        return this.clstType;
    }

    public void setClstType(String clstType) {
        this.clstType = clstType;
    }

    @Column(name = "CODELST_SUBTYP")
    public String getClstSubType() {
        return this.clstSubType;
    }

    public void setClstSubType(String clstSubType) {
        this.clstSubType = clstSubType;
    }

    @Column(name = "CODELST_DESC")
    public String getClstDesc() {
        return this.clstDesc;
    }

    public void setClstDesc(String clstDesc) {
        this.clstDesc = clstDesc;
    }

    @Column(name = "CODELST_HIDE")
    public String getClstHide() {
        return this.clstHide;
    }

    public void setClstHide(String clstHide) {
        this.clstHide = clstHide;
    }

    @Column(name = "CODELST_SEQ")
    public String getClstSeq() {
        return StringUtil.integerToString(this.clstSeq);
    }

    public void setClstSeq(String clstSeq) {
        if (clstSeq != null) {
            this.clstSeq = Integer.valueOf(clstSeq);
        }
    }

    @Column(name = "CODELST_MAINT")
    public String getClstMaint() {
        return this.clstMaint;
    }

    public void setClstMaint(String clstMaint) {
        this.clstMaint = clstMaint;
    }

    /**
     * @return The Id of the creator of the record
     */
    @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    /**
     * @return The Id of the user modifying this record
     */
    @Column(name = "LAST_MODIFIED_BY")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    /**
     * @return IP Address
     */
    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // END OF GETTER SETTER METHODS

    /**
     * returns the state holder object associated with this codelst
     */
    /*
     * public CodelstStateKeeper getCodelstStateKeeper() { Rlog.debug("codelst",
     * "CodelstBean.getCodelstStateKeeper"); return new
     * CodelstStateKeeper(getClstId(), getClstAccId(), getClstType(),
     * getClstSubType(), getClstDesc(), getClstHide(), getClstSeq(),
     * getClstMaint(), getCreator(), getModifiedBy(), getIpAdd()); }
     */

    /**
     * sets up a state keeper containing details of the codelst
     */
    /*
     * public void setCodelstStateKeeper(CodelstStateKeeper clsk) { GenerateId
     * codelstId = null;
     * 
     * try { Connection conn = null; conn = getConnection();
     * Rlog.debug("codelst", "CodelstBean.setCodelstStateKeeper() Connection :" +
     * conn); clstId = codelstId.getId("SEQ_ER_CODELST", conn); conn.close();
     * setClstAccId(clsk.getClstAccId()); setClstType(clsk.getClstType());
     * setClstSubType(clsk.getClstSubType()); setClstDesc(clsk.getClstDesc());
     * setClstHide(clsk.getClstHide()); setClstSeq(clsk.getClstSeq());
     * setClstMaint(clsk.getClstMaint()); setCreator(clsk.getCreator());
     * setModifiedBy(clsk.getModifiedBy()); setIpAdd(clsk.getIpAdd());
     * Rlog.debug("codelst", "CodelstBean.setCodelstStateKeeper() codelstId :" +
     * codelstId); } catch (Exception e) { System.out .println("Error in
     * setCodelstStateKeeper() in CodelstBean " + e); } }
     */

    public int updateCodelst(CodelstBean clsk) {

        try {

            setClstAccId(clsk.getClstAccId());
            setClstType(clsk.getClstType());
            setClstSubType(clsk.getClstSubType());
            setClstDesc(clsk.getClstDesc());
            setClstHide(clsk.getClstHide());
            setClstSeq(clsk.getClstSeq());
            setClstMaint(clsk.getClstMaint());
            setCreator(clsk.getCreator());
            setModifiedBy(clsk.getModifiedBy());
            setIpAdd(clsk.getIpAdd());
            Rlog.debug("codelst", "UserBean.updateUser");
            return 0;
        } catch (Exception e) {
            Rlog.fatal("user", " error in CodelstBean.updateCodelst");
            return -2;
        }
    }

}// end of class
