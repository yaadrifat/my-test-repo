package com.velos.eres.business.submission.impl;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @ejbHomeJNDIname ejb.Submission
 */

@Entity
@Table(name = "er_submission")
@NamedQueries( {
    @NamedQuery(name = "findCurrentByFkStudyAndFlag",
            query = "SELECT OBJECT(s) FROM SubmissionBean s where "+
            " s.fkStudy = :fkStudy and s.submissionFlag = :submissionFlag "),
    @NamedQuery(name = "findCurrentByFkStudy",
            query = "SELECT OBJECT(s) FROM SubmissionBean s where "+
            " s.fkStudy = :fkStudy ORDER BY s.id DESC ")
})
public class SubmissionBean implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private Integer id;
    private Integer fkStudy;
    private Integer submissionFlag;
    private Integer submissionType;
    private Integer submissionStatus;
    private Integer creator;
    private Integer lastModifiedBy;
    private Date    lastModifiedDate;
    private String  ipAdd;
    
    public SubmissionBean() {}
    
    public SubmissionBean(Integer id, Integer fkStudy, Integer submissionFlag, 
            Integer submissionType, Integer submissionStatus, Integer creator,
            Integer lastModifiedBy, String ipAdd) {
        setId(id); setFkStudy(fkStudy); setSubmissionFlag(submissionFlag);
        setSubmissionType(submissionType); setSubmissionStatus(submissionStatus);
        setCreator(creator);setLastModifiedBy(lastModifiedBy); setIpAdd(ipAdd);
    }
    
    public void setDetails(SubmissionBean anotherBean) {
        setFkStudy(anotherBean.getFkStudy());
        setSubmissionFlag(anotherBean.getSubmissionFlag());
        setSubmissionType(anotherBean.getSubmissionType());
        setSubmissionStatus(anotherBean.getSubmissionStatus());
        setCreator(anotherBean.getCreator());
        setLastModifiedBy(anotherBean.getLastModifiedBy());
        setIpAdd(anotherBean.getIpAdd());
    }
  
    
    // GETTER SETTER METHODS
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_SUBMISSION", allocationSize=1)
    @Column(name = "PK_SUBMISSION")
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "FK_STUDY")
    public Integer getFkStudy() {
        return fkStudy;
    }

    public void setFkStudy(Integer fkStudy) {
        this.fkStudy = fkStudy;
    }

    @Column(name = "SUBMISSION_FLAG")
    public Integer getSubmissionFlag() {
        return submissionFlag;
    }

    public void setSubmissionFlag(Integer submissionFlag) {
        this.submissionFlag = submissionFlag;
    }

    @Column(name = "SUBMISSION_TYPE")
    public Integer getSubmissionType() {
        return submissionType;
    }

    public void setSubmissionType(Integer submissionType) {
        this.submissionType = submissionType;
    }

    @Column(name = "SUBMISSION_STATUS")
    public Integer getSubmissionStatus() {
        return submissionStatus;
    }

    public void setSubmissionStatus(Integer submissionStatus) {
        this.submissionStatus = submissionStatus;
    }

    @Column(name = "CREATOR")
    public Integer getCreator() {
        return creator;
    }

    public void setCreator(Integer creator) {
        this.creator = creator;
    }

    @Column(name = "LAST_MODIFIED_DATE")
    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    @Column(name = "LAST_MODIFIED_BY")
    public Integer getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(Integer lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }
    
    
}
