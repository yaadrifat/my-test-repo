/*
 * Classname			EIRBDao.class
 * 
 * Version information 	1.0
 *
 * Date					06/01/2009
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.business.common;

import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import oracle.jdbc.OracleTypes;

import com.velos.eres.business.submission.impl.SubmissionProvisoBean;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.submission.SubmissionStatusJB;

/**
 * EIRBDao for calling EIRB package procedure
 * 
 * @author Isaac Huang
 * @version : 1.0 06/01/2009
 */

public class EIRBDao extends CommonDAO implements java.io.Serializable {

    // ArrayList for all boards
    private ArrayList resultFlags;
    private ArrayList resultTexts;
    private ArrayList testTexts;
    private ArrayList currentStatuses;
    private ArrayList currentStatusCodes;
    private ArrayList currentStatusDates;
    private ArrayList currentStatusEnteredBy;
    private ArrayList currentStatusAssignedTo;
    private ArrayList currentStatusNotes;
    private ArrayList currentOverallStatus1;
    private ArrayList currentOverallStatusDesc1;
    private ArrayList currentOverallStatusDate1;
    private ArrayList currentOverallEnteredById1;
    private ArrayList currentOverallNotes1;
    private ArrayList currentOverallEnteredBy1;
    private ArrayList reviewBoardList;

    // Overall status info
    private int rowCount;
    private String currentOverallStatus;
    private String currentOverallStatusDesc;
    private String currentOverallStatusDate;
    private String currentOverallEnteredBy;
    private String currentOverallEnteredById;
    private String currentOverallNotes;
    
    // ArrayList of summary submission types and counts for submitted and resubmitted statuses combined
    private ArrayList summarySubmissionTypes;
    private ArrayList summarySubmissionTypesCounts;
    
    // Subtotal of submission statuses in codelist subtypes and their counts 
    private ArrayList   subtotalSummarySubmissionStatusSubtypes;
    private ArrayList   subtotalSummarySubmissionStatusCounts;
    // The size of [] below is the size of the subtotal ArrayList above
    private ArrayList[] summarySubmissionStatusSubtypes;
    private ArrayList[] summarySubmissionStatusCounts;
    private ArrayList[] summarySubmissionStatusAssignees;
    
    // ArrayList of summary PIs and counts for the PI Response Required status
    private ArrayList summaryPIs;
    private ArrayList summaryPICounts;
    
    // Subtotal of pending reviews of all types in codelist subtypes and their counts
    private ArrayList   subtotalSummaryReviewSubtypes;
    private ArrayList   subtotalSummaryReviewCounts;
    // The size of [] below is the size of the subtotal ArrayList above
    private ArrayList[] summaryReviewSubtypes;
    private ArrayList[] summaryReviewCounts;
    private ArrayList[] summaryReviewReviewers;
    
    // Subtotal and summary of pending reviews of Ancillary type
    private ArrayList subtotalSummaryAncillaryReviewCounts;
    private ArrayList summaryAncillaryReviewCounts;
    private ArrayList summaryAncillaryReviewBoards;
    private ArrayList summaryAncillaryReviewReviewers;
    
    // Single entry
    private int submissionFlag;
    private String formName;
    private String formSeq;
    private Integer fkformlibsumstat;
    private Integer fkfilledformlibsumstat;
    private Integer formLibVer;
    

    
    // List of review board names for the account
    private ArrayList boardNameList;   
    private ArrayList boardIdList;
    
    // List of status submission history
    private ArrayList historyStatusDescList;
    private ArrayList historyEnteredByList;
    private ArrayList historyDatesList;
    private ArrayList historyNotesList;

    public EIRBDao() {
        resultFlags = new ArrayList();
        resultTexts = new ArrayList();
        testTexts = new ArrayList();
        boardNameList = new ArrayList();
        boardIdList = new ArrayList();
        currentStatuses = new ArrayList();
        currentStatusCodes = new ArrayList();
        currentStatusDates = new ArrayList();
        currentStatusEnteredBy = new ArrayList();
        currentStatusAssignedTo = new ArrayList();
        currentStatusNotes = new ArrayList();
        summarySubmissionTypes = new ArrayList();
        summarySubmissionTypesCounts = new ArrayList();
        subtotalSummarySubmissionStatusSubtypes = new ArrayList();
        subtotalSummarySubmissionStatusCounts = new ArrayList();
        summaryPIs = new ArrayList();
        summaryPICounts = new ArrayList();
        subtotalSummaryReviewSubtypes = new ArrayList();
        subtotalSummaryReviewCounts = new ArrayList();
        subtotalSummaryAncillaryReviewCounts = new ArrayList();
        historyStatusDescList = new ArrayList();
        historyEnteredByList = new ArrayList();
        historyDatesList = new ArrayList();
        historyNotesList = new ArrayList();
        currentOverallStatus1=new ArrayList();
        currentOverallStatusDesc1=new ArrayList();
        currentOverallStatusDate1=new ArrayList();
        currentOverallEnteredById1=new ArrayList();
        currentOverallNotes1=new ArrayList();
        currentOverallEnteredBy1=new ArrayList();
        reviewBoardList=new ArrayList();
    }
    
    private void clearCheckResults() {
        resultFlags.clear();
        resultTexts.clear();
        testTexts.clear();
    }

    private void clearBoardResults() {
        boardNameList.clear();
        boardIdList.clear();
    }

    private void clearStatusResults() {
        currentStatuses.clear();
        currentStatusCodes.clear();
        currentStatusDates.clear();
        currentStatusEnteredBy.clear();
        currentStatusAssignedTo.clear();
        currentStatusNotes.clear();
    }

    private void clearOverallStatusResults() {
        currentOverallStatus = null;
        currentOverallStatusDesc = null;
        currentOverallStatusDate = null;
        currentOverallEnteredBy = null;
        currentOverallEnteredById = null;
        currentOverallNotes = null;
    }
    
    private void clearSummarySubmissionTypes() {
        summarySubmissionTypes.clear();
        summarySubmissionTypesCounts.clear();
    }
    
    private void clearSummaryPIs() {
        summaryPIs.clear();
        summaryPICounts.clear();
    }
    
    private void clearSummarySubmissionStatuses() {
        subtotalSummarySubmissionStatusSubtypes.clear();
        subtotalSummarySubmissionStatusCounts.clear();
        if (summarySubmissionStatusSubtypes != null) {
            if (summarySubmissionStatusSubtypes.length > 0) {
                for(int iX=0; iX<summarySubmissionStatusSubtypes.length; iX++ ) {
                    summarySubmissionStatusSubtypes[iX].clear();
                    summarySubmissionStatusSubtypes[iX] = null;
                }
            }
            summarySubmissionStatusSubtypes = null;
        }
        if (summarySubmissionStatusCounts != null) {
            if (summarySubmissionStatusCounts.length > 0) {
                for(int iX=0; iX<summarySubmissionStatusCounts.length; iX++ ) {
                    summarySubmissionStatusCounts[iX].clear();
                    summarySubmissionStatusCounts[iX] = null;
                }
            }
            summarySubmissionStatusCounts = null;
        }
        if (summarySubmissionStatusAssignees != null) {
            if (summarySubmissionStatusAssignees.length > 0) {
                for(int iX=0; iX<summarySubmissionStatusAssignees.length; iX++ ) {
                    summarySubmissionStatusAssignees[iX].clear();
                    summarySubmissionStatusAssignees[iX] = null;
                }
            }
            summarySubmissionStatusAssignees = null;
        }
    }
    
    private void clearSummaryReviews() {
        subtotalSummaryReviewSubtypes.clear();
        subtotalSummaryReviewCounts.clear();
        if (summaryReviewCounts != null) {
            if (summaryReviewCounts.length > 0) {
                for(int iX=0; iX<summaryReviewCounts.length; iX++ ) {
                    summaryReviewCounts[iX].clear();
                    summaryReviewCounts[iX] = null;
                }
            }
            summaryReviewCounts = null;
        }
        if (summaryReviewReviewers != null) {
            if (summaryReviewReviewers.length > 0) {
                for(int iX=0; iX<summaryReviewReviewers.length; iX++ ) {
                    summaryReviewReviewers[iX].clear();
                    summaryReviewReviewers[iX] = null;
                }
            }
            summaryReviewReviewers = null;
        }
        if (summaryReviewSubtypes != null) {
            if (summaryReviewSubtypes.length > 0) {
                for(int iX=0; iX<summaryReviewSubtypes.length; iX++ ) {
                    summaryReviewSubtypes[iX].clear();
                    summaryReviewSubtypes[iX] = null;
                }
            }
            summaryReviewSubtypes = null;
        }
    }

    private void clearSummaryAncillaryReviews() {
        subtotalSummaryAncillaryReviewCounts.clear();
        if (summaryAncillaryReviewBoards != null) {
            summaryAncillaryReviewBoards.clear();
            summaryAncillaryReviewBoards = null;
        }
        if (summaryAncillaryReviewCounts != null) {
            summaryAncillaryReviewCounts.clear();
            summaryAncillaryReviewCounts = null;
        }
        if (summaryAncillaryReviewReviewers != null) {
            summaryAncillaryReviewReviewers.clear();
            summaryAncillaryReviewReviewers = null;
        }
    }
    
    private void clearHistory() {
        historyStatusDescList.clear();
        historyEnteredByList.clear();
        historyDatesList.clear();
        historyNotesList.clear();
    }
    
    // THE GET AND SEND METHODS

    public String getBoardDropDown(String selBoardId, String prop) {
        StringBuffer sb = new StringBuffer();
        sb.append("<select name='reviewBoard' ").append(prop).append(" >");
        for (int iX=0; iX<boardIdList.size(); iX++ ) {
            sb.append("<option value='").append(boardIdList.get(iX)).append("'");
            if (StringUtil.stringToNum(selBoardId) == Integer.valueOf((String)boardIdList.get(iX))) {
                sb.append(" selected");
            }
            sb.append(">").append(boardNameList.get(iX)).append("</option>");
        }
        sb.append("</select>");
        return sb.toString();
    }
    

    public ArrayList getBoardNameList() {
        return boardNameList;
    }
    
    private void setBoardNameList(String reviewBoardName) {
        this.boardNameList.add(reviewBoardName);
    }

    public ArrayList getBoardIdList() {
        return boardIdList;
    }
    
    private void setBoardIdList(String reviewBoardId) {
        this.boardIdList.add(reviewBoardId);
    }

    private void setResultFlag(Integer resultFlag) {
        this.resultFlags.add(resultFlag);
    }

    private void setResultText(String resultText) {
        this.resultTexts.add(resultText);
    }

    private void setTestText(String testText) {
        this.testTexts.add(testText);
    }

    private void setSubmissionFlag(int flag) {
        this.submissionFlag = flag;
    }

    public ArrayList getResultFlags() {
        return resultFlags;
    }

    public ArrayList getResultTexts() {
        return resultTexts;
    }

    public ArrayList getTestTexts() {
        return testTexts;
    }

    public int getSubmissionFlag() {
        return submissionFlag;
    }

    public ArrayList getCurrentStatuses() {
        return currentStatuses;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatuses.add(currentStatus);
    }

    public ArrayList getCurrentStatusCodes() {
        return currentStatusCodes;
    }

    public void setCurrentStatusCode(String currentStatusCode) {
        this.currentStatusCodes.add(currentStatusCode);
    }
    
    public ArrayList getCurrentStatusDates() {
        return currentStatusDates;
    }

    public void setCurrentStatusDate(String currentStatusDate) {
        this.currentStatusDates.add(currentStatusDate);
    }
    
    public ArrayList getCurrentStatusEnteredBy() {
        return currentStatusEnteredBy;
    }
    
    public void setCurrentStatusEnteredBy(String currentStatusEnteredBy) {
        this.currentStatusEnteredBy.add(currentStatusEnteredBy);
    }
    
    public ArrayList getCurrentStatusAssignedTo() {
        return currentStatusAssignedTo;
    }
    
    public void setCurrentStatusAssignedTo(String currentStatusAssignedTo) {
        this.currentStatusAssignedTo.add(currentStatusAssignedTo);
    }
    
    public ArrayList getCurrentStatusNotes() {
        return currentStatusNotes;
    }
    
    public void setCurrentStatusNotes(String currentStatusNotes) {
        this.currentStatusNotes.add(currentStatusNotes);
    }
    
    public String getCurrentOverallStatus() {
        return currentOverallStatus;
    }

    public String getCurrentOverallStatusDesc() {
        return currentOverallStatusDesc;
    }

    public String getCurrentOverallStatusDate() {
        return currentOverallStatusDate;
    }

    public String getCurrentOverallEnteredBy() {
        return currentOverallEnteredBy;
    }

    public String getCurrentOverallEnteredById() {
        return currentOverallEnteredById;
    }

    public String getCurrentOverallNotes() {
        return currentOverallNotes;
    }
    
    
    public ArrayList getSummarySubmissionTypes() {
        return summarySubmissionTypes;
    }

    private void setSummarySubmissionTypes(String type) {
        this.summarySubmissionTypes.add(type);
    }

    public ArrayList getSummarySubmissionTypesCounts() {
        return summarySubmissionTypesCounts;
    }

    private void setSummarySubmissionTypesCounts(Integer count) {
        this.summarySubmissionTypesCounts.add(count);
    }

    public ArrayList getSummaryPIs() {
        return summaryPIs;
    }

    private void setSummaryPIs(String pi) {
        this.summaryPIs.add(pi);
    }

    public ArrayList getSummaryPICounts() {
        return summaryPICounts;
    }

    private void setSummaryPICounts(Integer count) {
        this.summaryPICounts.add(count);
    }

    public ArrayList getSubtotalSummarySubmissionStatusSubtypes() {
        return subtotalSummarySubmissionStatusSubtypes;
    }

    private void setSubtotalSummarySubmissionStatusSubtypes(String subtype) {
        this.subtotalSummarySubmissionStatusSubtypes.add(subtype);
    }

    public ArrayList getSubtotalSummarySubmissionStatusCounts() {
        return subtotalSummarySubmissionStatusCounts;
    }

    private void setSubtotalSummarySubmissionStatusCounts(Integer count) {
        this.subtotalSummarySubmissionStatusCounts.add(count);
    }

    public ArrayList[] getSummarySubmissionStatusSubtypes() {
        return summarySubmissionStatusSubtypes;
    }

    public ArrayList[] getSummarySubmissionStatusCounts() {
        return summarySubmissionStatusCounts;
    }

    public ArrayList[] getSummarySubmissionStatusAssignees() {
        return summarySubmissionStatusAssignees;
    }

    public ArrayList getSubtotalSummaryReviewSubtypes() {
        return subtotalSummaryReviewSubtypes;
    }

    private void setSubtotalSummaryReviewSubtypes(String subtype) {
        this.subtotalSummaryReviewSubtypes.add(subtype);
    }

    public ArrayList getSubtotalSummaryReviewCounts() {
        return subtotalSummaryReviewCounts;
    }

    private void setSubtotalSummaryReviewCounts(Integer count) {
        this.subtotalSummaryReviewCounts.add(count);
    }
    
    public ArrayList[] getSummaryReviewReviewers() {
        return summaryReviewReviewers;
    }

    public ArrayList[] getSummaryReviewCounts() {
        return summaryReviewCounts;
    }
    
    public ArrayList[] getSummaryReviewSubtypes() {
        return summaryReviewSubtypes;
    }
    
    public ArrayList getSubtotalSummaryAncillaryReviewCounts() {
        return subtotalSummaryAncillaryReviewCounts;
    }

    private void setSubtotalSummaryAncillaryReviewCounts(Integer count) {
        this.subtotalSummaryAncillaryReviewCounts.add(count);
    }
    
    public ArrayList getSummaryAncillaryReviewBoards() {
        return summaryAncillaryReviewBoards;
    }
    
    public ArrayList getSummaryAncillaryReviewCounts() {
        return summaryAncillaryReviewCounts;
    }

    public ArrayList getSummaryAncillaryReviewReviewers() {
        return summaryAncillaryReviewReviewers;
    }
    
    public ArrayList getHistoryStatusDescs() {
        return historyStatusDescList;
    }
    
    private void setHistoryStatusDesc(String status) {
        historyStatusDescList.add(status);
    }
    
    public ArrayList getHistoryDates() {
        return historyDatesList;
    }
    
    private void setHistoryDate(String date) {
        historyDatesList.add(date);
    }
    
    public ArrayList getHistoryEnteredByList() {
        return historyEnteredByList;
    }
    
    private void setHistoryEnteredBy(String enteredBy) {
        historyEnteredByList.add(enteredBy);
    }
    
    public ArrayList getHistoryNotesList() {
        return historyNotesList;
    }
    
    private void setHistoryNotes(String notes) {
        historyNotesList.add(notes);
    }
    public void setCurrentOverallStatus1(String crntOvaerallStatus) {
    	currentOverallStatus1.add(crntOvaerallStatus);
    }
    public ArrayList getCurrentOverallStatus1() {
    	return currentOverallStatus1;
    }
    public void setCurrentOverallStatusDesc1(String crntOvaerallStatusDesc) {
    	currentOverallStatusDesc1.add(crntOvaerallStatusDesc);
    }
    public ArrayList getCurrentOverallStatusDesc1() {
    	return currentOverallStatusDesc1;
    }
    public void setCurrentOverallStatusDate1(String statusDate) {
    	currentOverallStatusDate1.add(statusDate);
    }
    public ArrayList getCurrentOverallStatusDate1() {
    	return currentOverallStatusDate1;
    }
    public void setCurrentOverallEnteredById1(String enteredBy) {
    	currentOverallEnteredById1.add(enteredBy);
    }
    public ArrayList getCurrentOverallEnteredById1() {
    	return currentOverallEnteredById1;
    }
    public void setCurrentOverallNotes1(String submisssinNotes) {
    	currentOverallNotes1.add(submisssinNotes);
    }
    public ArrayList getCurrentOverallNotes1() {
    	return currentOverallNotes1;
    }
    public void setCurrentOverallEnteredBy1(String enteredBy) {
    	currentOverallEnteredBy1.add(enteredBy);
    }
    public ArrayList getCurrentOverallEnteredBy1() {
    	return currentOverallEnteredBy1;
    }
    public void setReviewBoard(String reviewBoard) {
    	reviewBoardList.add(reviewBoard);
    }
    public ArrayList getReviewBoard() {
    	return reviewBoardList;
    }
  
    public int getRowCount() {
    	return rowCount;
    }
    
    public String getIrbFormName() {
        return formName;
    }
    public void setIrbFormName(String formName) {
        this.formName = formName;
    }
    public String getIrbFormSeq() {
        return formSeq;
    }
    public void setIrbFormSeq(String formSeq) {
        this.formSeq = formSeq;
    }
    
    public int getFkformlibsumstat() {
        return fkformlibsumstat;
    }
    public void setFkformlibsumstat(int fkformlibsumstat) {
        this.fkformlibsumstat = fkformlibsumstat;
    }
    
  
    public int getFkfilledformlibsumstat() {
        return fkfilledformlibsumstat;
    }
    public void setFkfilledformlibsumstat(int fkfilledformlibsumstat) {
        this.fkfilledformlibsumstat = fkfilledformlibsumstat;
    }
    
    public Integer getFormLibVer() {
        return formLibVer;
    }
    public void setFormLibVer(Integer formLibVer) {
        this.formLibVer = formLibVer;
    }
    

    
    // END OF SETTERS AND GETTERS

    /**
     * This method returns the list of ...
     * 
     * @param studyId as int
     * @param boardId as int
     * @returns nothing. Use individual getters of the ArrayLists.
     */
    public void checkSubmission(int studyId, int boardId) {
        clearCheckResults();

        CallableStatement cstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            cstmt = conn.prepareCall("{call PKG_EIRB.sp_checkSubmission(?,?,?,?)}");
            		
            cstmt.setInt(1, studyId);
            cstmt.setInt(2, boardId);
            cstmt.getResultSetType();
            cstmt.registerOutParameter(3,OracleTypes.CURSOR);
            cstmt.registerOutParameter(4,java.sql.Types.INTEGER);
            
            cstmt.execute();
            ResultSet rs = (ResultSet)cstmt.getObject(3);
            setSubmissionFlag(cstmt.getInt(4));
            if (rs != null) {
                while(rs.next()) {
                    setResultFlag(rs.getInt("resultFlag"));
                    setResultText(rs.getString("resultText"));
                    setTestText(rs.getString("testText"));
                }
            }

        } catch (SQLException e) {
            Rlog.fatal("EIRBDao", " error in checkSubmission() " + e);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }
    
    public void getReviewBoards(int accountId, int grpId) {
        clearBoardResults();

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            StringBuffer sql = new StringBuffer();
            sql.append("select pk_review_board, review_board_name from er_submission_logic s ");
            sql.append(" inner join er_review_board r on s.fk_review_board = r.pk_review_board ");
            sql.append(" where r.fk_account = ? and ',' || r.board_group_access || ',' LIKE '%,")
            .append(grpId).append(",%' order by s.logic_sequence ");
            pstmt = conn.prepareStatement(sql.toString());
            pstmt.setInt(1, accountId);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                setBoardIdList(rs.getString("pk_review_board"));
                setBoardNameList(rs.getString("review_board_name"));
            }
        } catch(SQLException e) {
            Rlog.fatal("EIRBDao", " error in getReviewBoards() "+e);
        } finally {
            try {
                if (pstmt != null) pstmt.close();
            } catch (Exception e) {}
            try {
                if (conn != null) conn.close();
            } catch (Exception e) {}
        }
    }
    
    public void getCurrentStatuses(int accountId, int studyId, int grpId, int userId, int submPK) {
        clearStatusResults();

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            StringBuffer sql = new StringBuffer();

            sql.append("select rb.pk_review_board, rb.review_board_name, subq.status, subq.submission_status, subq.status_date, ");
            sql.append("       subq.submission_entered_by, subq.submission_assigned_to, subq.submission_notes ");
            sql.append("  from er_submission_logic sl ");
            sql.append("  inner join er_review_board rb on sl.fk_review_board = rb.pk_review_board ");
            sql.append("  left outer join ");
            sql.append("  ( ");
            sql.append("    select s.fk_study, s.pk_submission, ss_rb1.*, F_CODELST_DESC(ss_rb1.submission_status) status from ");
            sql.append("    ( ");
            sql.append("       select ss.pk_submission_status, ss.submission_status, sb.fk_submission,  ");
            sql.append("       sb.fk_review_board, ss.submission_status_date status_date, ");
            sql.append("       GETUSER(ss.submission_entered_by) submission_entered_by, ");
            sql.append("       GETUSER(ss.submission_assigned_to) submission_assigned_to, submission_notes ");
            sql.append("       from er_submission_board sb ");
            sql.append("       inner join (select st.* from er_submission_status st,");
            sql.append(" (SELECT max(ss.pk_submission_status) pk_submission_status, ss.fk_submission_board");
            sql.append("  FROM er_submission_status ss");
            sql.append("  where is_current= 1 and ss.fk_submission_board is not null");
            sql.append(" group by fk_submission_board) ss");
            sql.append(" where ss.pk_submission_status = st.pk_submission_status) ss");
            sql.append("       on ss.fk_submission_board = sb.pk_submission_board ");
            sql.append("       where is_current = 1 ");
            sql.append("    ) ss_rb1 ");
            sql.append("    inner join er_submission s on ss_rb1.fk_submission = s.pk_submission ");
            sql.append("    inner join er_study study on study.pk_study = s.fk_study ");
            sql.append("    where s.fk_study = ? ");
            sql.append("    and study.fk_account = ? ");
            if (submPK > 0) {
                sql.append("  and (s.pk_submission is null or s.pk_submission = ")
                .append(submPK).append(" ) ");
            } else if (submPK == 0) { // Check and Submit page for New Application only
                sql.append("  and (s.pk_submission is null or s.submission_flag = 1) ");
            }
            sql.append("    and (EXISTS (SELECT * FROM   er_studyteam t ");
            sql.append("                 WHERE t.fk_study = study.pk_study AND t.fk_user = ? ");
            sql.append("                 AND NVL (t.study_team_usr_type, 'D') = 'D') ");
            sql.append("                 OR pkg_superuser.F_Is_Superuser (?, pk_study) = 1) ");
            sql.append("  ) subq  ");
            sql.append("  on subq.fk_review_board = rb.pk_review_board ");
            sql.append("  where rb.fk_account = ? ");
            sql.append("  and rb.pk_review_board IN ");
            sql.append("    (SELECT pk_review_board FROM er_review_board erb ");
            sql.append("     WHERE erb.fk_account = ? ");
            sql.append("     and ',' || erb.board_group_access || ',' LIKE '%,").append(grpId).append(",%') ");
            sql.append("  order by sl.logic_sequence ");
            
            pstmt = conn.prepareStatement(sql.toString());
            pstmt.setInt(1, studyId);
            pstmt.setInt(2, accountId);
            pstmt.setInt(3, userId);
            pstmt.setInt(4, userId);
            pstmt.setInt(5, accountId);
            pstmt.setInt(6, accountId);
            
            ResultSet rs = pstmt.executeQuery();
            
            while (rs.next()) {
                setCurrentStatus(rs.getString("status"));
                setCurrentStatusCode(rs.getString("submission_status"));
                setCurrentStatusDate(DateUtil.dateToString(rs.getDate("status_date")));
                setCurrentStatusEnteredBy(rs.getString("submission_entered_by"));
                setCurrentStatusAssignedTo(rs.getString("submission_assigned_to"));
                setCurrentStatusNotes(rs.getString("submission_notes"));
            }
        } catch(SQLException e) {
            Rlog.fatal("EIRBDao", " error in getCurrentStatuses() "+e);
        } finally {
            try {
                if (pstmt != null) pstmt.close();
            } catch (Exception e) {}
            try {
                if (conn != null) conn.close();
            } catch (Exception e) {}
        }
    }

    public void getCurrentOverallStatus(int studyId, int submissionPK) {
        clearOverallStatusResults();

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            
            StringBuffer sql = new StringBuffer();

            sql.append("select F_CODELST_DESC(ss.submission_status) status_desc,(select review_board_name from er_review_board where pk_review_board in (select fk_review_board from er_submission_board where pk_submission_board in (ss.fk_submission_board))) as review_board_name,ss.submission_status,ss.submission_status_date, ");
            sql.append(" ss.submission_entered_by,ss.submission_notes, ");
            sql.append(" u.usr_firstname, u.usr_lastname ");
            sql.append(" from er_submission_status ss ");
            sql.append(" left outer join er_user u on ss.submission_entered_by = u.pk_user ");
            sql.append(" inner join er_codelst c on c.pk_codelst = ss.submission_status ");
            sql.append(" inner join er_submission s on s.pk_submission = ss.fk_submission ");
            sql.append(" where fk_submission_board is not null and ss.is_current = 1 AND ss.submission_status IN (SELECT pk_codelst FROM er_codelst WHERE ',' ||codelst_custom_col1 ||',' LIKE '%,final,%' ) and s.fk_study = ? ");
            sql.append(" and s.pk_submission = ? ");
            
            pstmt = conn.prepareStatement(sql.toString());
            pstmt.setInt(1, studyId);
            pstmt.setInt(2, submissionPK);
            ResultSet rs = pstmt.executeQuery();
            String firstName = null;
            String lastName = null;
            while (rs.next()) {
            	setCurrentOverallStatus1(rs.getString("submission_status"));
                currentOverallStatus = rs.getString("submission_status");
                setCurrentOverallStatusDesc1(rs.getString("status_desc"));
                currentOverallStatusDesc = rs.getString("status_desc");
                setCurrentOverallStatusDate1(DateUtil.dateToString(rs.getDate("submission_status_date")));
                currentOverallStatusDate = DateUtil.dateToString(rs.getDate("submission_status_date"));
                setCurrentOverallEnteredById1(rs.getString("submission_entered_by"));
                currentOverallEnteredById = rs.getString("submission_entered_by");
                setCurrentOverallNotes1(rs.getString("submission_notes"));
                currentOverallNotes = rs.getString("submission_notes");
                setReviewBoard( rs.getString("review_board_name"));
                firstName = rs.getString("usr_firstname");
                lastName = rs.getString("usr_lastname");
                StringBuffer sb = new StringBuffer();
                if (firstName != null) { sb.append(firstName).append(" "); }
                if (lastName != null) { sb.append(lastName); }
                setCurrentOverallEnteredBy1(sb.toString());
                rowCount++;
            }
            // form full name 
            StringBuffer sb = new StringBuffer();
            if (firstName != null) { sb.append(firstName).append(" "); }
            if (lastName != null) { sb.append(lastName); }
            currentOverallEnteredBy = sb.toString();
            // do null checks
            if (currentOverallStatus == null) { currentOverallStatus = ""; }
            if (currentOverallStatusDesc == null) { currentOverallStatusDesc = ""; }
            if (currentOverallStatusDate == null) { currentOverallStatusDate = ""; }
            if (currentOverallEnteredById == null) { currentOverallEnteredById = ""; }
            if (currentOverallNotes == null) { currentOverallNotes = ""; }
        } catch(SQLException e) {
            Rlog.fatal("EIRBDao", " error in getCurrentOverallStatus() "+e);
        } finally {
            try {
                if (pstmt != null) pstmt.close();
            } catch (Exception e) {}
            try {
                if (conn != null) conn.close();
            } catch (Exception e) {}
        }
    }
    
    public void getSummaryPIs(int accountId, int grpId, int userId) {
        clearSummaryPIs();
        
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            StringBuffer sql = new StringBuffer();
            sql.append(" select "); 
            sql.append(" case (s1.study_prinv) ");
            sql.append("   when '0'  then 'Count by PI' ");
            sql.append("   when '-1' then 'No PI Assigned' ");
            sql.append("   else ");
            sql.append("     (select usr_firstname ||' '|| usr_lastname from er_user where pk_user = s1.study_prinv)  ");
            sql.append("   end study_prinv, ");
            sql.append("   s1.pi_count ");
            sql.append(" from ");
            sql.append(" ( ");
            sql.append("   select ");
            sql.append("   case grouping(study_prinv) ");
            sql.append("     when 0 then study_prinv ");
            sql.append("     when -1 then '-1' ");
            sql.append("     else '0' ");
            sql.append("   end study_prinv, ");
            sql.append("   count(subq.study_prinv) pi_count ");
            sql.append("   from ");
            sql.append("   ( ");
            sql.append("     Select study_number, ");
            sql.append("     (select review_board_name from er_review_board where pk_review_board = fk_review_board) submission_board_name, ");
            sql.append("     fk_review_board, pk_study, pk_submission, pk_submission_board, ");
            sql.append("     NVL(s.study_prinv, '-1') study_prinv ");
            sql.append("     from er_submission sub,er_submission_board sb,er_study s,er_submission_status st ");
            sql.append("    where sub.fk_study = s.pk_study and sub.pk_submission = sb.fk_submission and st.fk_submission_board = sb.pk_submission_board and is_current = 1 ");
            sql.append("     and fk_review_board in  (select pk_review_board from er_review_board erb where erb.fk_account = ? and ");
          //  sql.append("     F_IS_IRBFINAL(sb.fk_submission ) = 0 and "); //omit final submissions//comment for aus fix
            sql.append("     ','|| erb.board_group_access || ',' like '%,").append(grpId).append(",%' ) ");
            sql.append("     and s.fk_account = ?  and  ( exists ( select * from er_studyteam t where t.fk_study = s.pk_study and  ");
            sql.append("     t.fk_user = ? and  nvl(t.study_team_usr_type,'D')='D') or pkg_superuser.F_Is_Superuser(?, pk_study) = 1)");
            sql.append("     and ( st.submission_status = F_CODELST_ID('subm_status', 'pi_resp_req') ) ");
            sql.append("   ) subq ");
            sql.append("   group by rollup(study_prinv) ");
            sql.append(" ) s1 ");
            sql.append(" order by s1.pi_count desc, study_prinv ");
            pstmt = conn.prepareStatement(sql.toString());
            pstmt.setInt(1, accountId);
            pstmt.setInt(2, accountId);
            pstmt.setInt(3, userId);
            pstmt.setInt(4, userId);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                setSummaryPIs(rs.getString("study_prinv"));
                setSummaryPICounts(rs.getInt("pi_count"));
            }
        } catch(Exception e) {
            Rlog.fatal("EIRBDao", " error in getSummaryPIs() "+e);
        } finally {
            try {
                if (pstmt != null) pstmt.close();
            } catch (Exception e) {}
            try {
                if (conn != null) conn.close();
            } catch (Exception e) {}
        }
    }
    
    public void getSummarySubmissionTypes(int accountId, int grpId, int userId) {
        clearSummarySubmissionTypes();
        
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            StringBuffer sql = new StringBuffer();

            sql.append(" select "); 
            sql.append(" case (s1.submission_type) ");
            sql.append(" when 0 then 'TOTAL' else c.codelst_desc end submission_type_desc, ");
            sql.append(" s1.count from ");
            sql.append(" ( ");
            sql.append("   select ");
            sql.append("   case grouping(subq.submission_type) ");
            sql.append("   when 0 then subq.submission_type else 0 ");
            sql.append("   end submission_type, ");
            sql.append("   count(subq.submission_type) count ");
            sql.append("   from ");
            sql.append("   ( ");
            sql.append("     Select study_number, ");
            sql.append("     F_CODELST_DESC(submission_type) submission_type_desc,submission_type, ");
            sql.append("     F_CODELST_DESC(st.submission_status) submission_status_desc,st.submission_status, ");
            sql.append("     (select review_board_name from er_review_board where pk_review_board = fk_review_board) submission_board_name, ");
            sql.append("     fk_review_board, pk_study ,pk_submission, pk_submission_board ");
            sql.append("     , (select usr_firstname ||' ' || usr_lastname from er_user where pk_user = submission_assigned_to) assigned_to ");
            sql.append("     , (select usr_firstname ||' ' || usr_lastname from er_user where pk_user = submission_entered_by) entered_by ");
            sql.append("     from er_submission sub,er_submission_board sb,er_study s,er_submission_status st ");
            sql.append("    where sub.fk_study = s.pk_study and sub.pk_submission = sb.fk_submission and st.fk_submission_board = sb.pk_submission_board and is_current = 1 ");
            sql.append("     and fk_review_board in  (select pk_review_board from er_review_board erb where erb.fk_account = ? and ");
            //sql.append("     F_IS_IRBFINAL(sb.fk_submission ) = 0 and ");//comment for aus fix
            sql.append("     ','|| erb.board_group_access || ',' like '%,").append(grpId).append(",%' ) ");
            sql.append("     and s.fk_account = ?  and  ( exists ( select * from er_studyteam t where t.fk_study = s.pk_study and  ");
            sql.append("     t.fk_user = ? and  nvl(t.study_team_usr_type,'D')='D') or pkg_superuser.F_Is_Superuser(?, pk_study) = 1)");
            sql.append("     and ( st.submission_status = F_CODELST_ID('subm_status', 'submitted') or st.submission_status = F_CODELST_ID('subm_status', 'resubmitted') or st.submission_status = F_CODELST_ID('subm_status', 'piresponded')) ");
            sql.append("   ) subq ");
            sql.append("   group by rollup(submission_type) ");
            sql.append(" ) s1 ");
            sql.append(" left outer join er_codelst c "); 
            sql.append(" on s1.submission_type = c.pk_codelst and c.codelst_type = 'submission' ");
            sql.append(" order by c.codelst_seq ");
            
            pstmt = conn.prepareStatement(sql.toString());
            pstmt.setInt(1, accountId);
            pstmt.setInt(2, accountId);
            pstmt.setInt(3, userId);
            pstmt.setInt(4, userId);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                setSummarySubmissionTypes(rs.getString("submission_type_desc"));
                setSummarySubmissionTypesCounts(rs.getInt("count"));
            }
        } catch(SQLException e) {
            Rlog.fatal("EIRBDao", " error in getSummarySubmissionTypes() "+e);
        } finally {
            try {
                if (pstmt != null) pstmt.close();
            } catch (Exception e) {}
            try {
                if (conn != null) conn.close();
            } catch (Exception e) {}
        }
    }
    
    public void getSummarySubmissionStatuses(int accountId, int grpId, int userId) {
        clearSummarySubmissionStatuses();
        
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            StringBuffer sql = new StringBuffer();

            sql.append(" select "); 
            sql.append(" s1.grouping_flag,c.codelst_subtyp, ");
            sql.append(" (select usr_firstname ||' '|| usr_lastname from er_user ");
            sql.append("  where pk_user = submission_assigned_to) assigned_to, ");
            sql.append(" s1.status_count ");
            sql.append(" from ");
            sql.append(" ( ");
            sql.append("   select ");
            sql.append("   grouping(subq.submission_status)||grouping(subq.submission_assigned_to) grouping_flag,");
            sql.append("   submission_status, ");
            sql.append("   submission_assigned_to, ");
            sql.append("   count(subq.submission_status) status_count ");
            sql.append("   from ");
            sql.append("   ( ");
            sql.append("     Select study_number, ");
            sql.append("     F_CODELST_DESC(submission_type) submission_type_desc,submission_type, ");
            sql.append("     F_CODELST_DESC(st.submission_status) submission_status_desc,st.submission_status, ");
            sql.append("     (select review_board_name from er_review_board where pk_review_board = fk_review_board) submission_board_name, ");
            sql.append("     fk_review_board, pk_study ,pk_submission, pk_submission_board ");
            sql.append("     , submission_assigned_to  ");
            sql.append("     , (select usr_firstname ||' ' || usr_lastname from er_user where pk_user = submission_entered_by) entered_by ");
            sql.append("     from er_submission sub,er_submission_board sb,er_study s,er_submission_status st ");
            sql.append("    where sub.fk_study = s.pk_study and sub.pk_submission = sb.fk_submission and st.fk_submission_board = sb.pk_submission_board and is_current = 1 ");
            sql.append("     and fk_review_board in  (select pk_review_board from er_review_board erb where erb.fk_account = ? and ");
        //    sql.append("     F_IS_IRBFINAL(sb.fk_submission ) = 0 and "); //omit final submissions//comment for aus fix
            sql.append("     ','|| erb.board_group_access || ',' like '%,").append(grpId).append(",%' ) ");
            sql.append("     and s.fk_account = ?  and  ( exists ( select * from er_studyteam t where t.fk_study = s.pk_study and  ");
            sql.append("     t.fk_user = ? and  nvl(t.study_team_usr_type,'D')='D') or pkg_superuser.F_Is_Superuser(?, pk_study) = 1)");
            sql.append("     and ( st.submission_status <> F_CODELST_ID('subm_status', 'submitted') and st.submission_status <> F_CODELST_ID('subm_status', 'resubmitted')) ");
            sql.append("   ) subq ");
            sql.append("   group by cube(submission_status,submission_assigned_to) ");
            sql.append("   order by grouping(submission_status),grouping(submission_assigned_to) ");
            sql.append(" ) s1 ");
            sql.append(" left outer join er_codelst c "); 
            sql.append(" on s1.submission_status = c.pk_codelst and c.codelst_type = 'subm_status' ");
            sql.append(" where s1.grouping_flag = '00' or s1.grouping_flag = '01' ");
            sql.append(" order by s1.grouping_flag desc, s1.submission_status, s1.status_count desc, assigned_to ");
            
            pstmt = conn.prepareStatement(sql.toString());
            pstmt.setInt(1, accountId);
            pstmt.setInt(2, accountId);
            pstmt.setInt(3, userId);
            pstmt.setInt(4, userId);
            ResultSet rs = pstmt.executeQuery();
            int arrayIndex = 0;
            String lastSubtype = null;
            while (rs.next()) {
                if ("01".equals(rs.getString("grouping_flag"))) {
                    setSubtotalSummarySubmissionStatusSubtypes(rs.getString("codelst_subtyp"));
                    setSubtotalSummarySubmissionStatusCounts(rs.getInt("status_count"));
                }
                if ("00".equals(rs.getString("grouping_flag"))) {
                    if (summarySubmissionStatusSubtypes == null) {
                        summarySubmissionStatusSubtypes = new ArrayList[subtotalSummarySubmissionStatusCounts.size()];
                        for (int iX=0; iX<summarySubmissionStatusSubtypes.length; iX++) {
                            summarySubmissionStatusSubtypes[iX] = new ArrayList();
                        }
                    }
                    if (summarySubmissionStatusAssignees == null) {
                        summarySubmissionStatusAssignees = new ArrayList[subtotalSummarySubmissionStatusCounts.size()];
                        for (int iX=0; iX<summarySubmissionStatusAssignees.length; iX++) {
                            summarySubmissionStatusAssignees[iX] = new ArrayList();
                        }
                    }
                    if (summarySubmissionStatusCounts == null) {
                        summarySubmissionStatusCounts = new ArrayList[subtotalSummarySubmissionStatusCounts.size()];
                        for (int iX=0; iX<summarySubmissionStatusCounts.length; iX++) {
                            summarySubmissionStatusCounts[iX] = new ArrayList();
                        }
                    }
                    String thisSubtype = rs.getString("codelst_subtyp");
                    if (lastSubtype != null && !lastSubtype.equals(thisSubtype)) {
                        ++arrayIndex;
                    }
                    if (arrayIndex >= summarySubmissionStatusSubtypes.length) { break; }
                    summarySubmissionStatusSubtypes[arrayIndex].add(rs.getString("codelst_subtyp"));
                    summarySubmissionStatusAssignees[arrayIndex].add(rs.getString("assigned_to"));
                    summarySubmissionStatusCounts[arrayIndex].add(rs.getInt("status_count"));
                    lastSubtype = thisSubtype;
                }
            }
        } catch(SQLException e) {
            Rlog.fatal("EIRBDao", " error in getSummarySubmissionStatuses() "+e);
        } finally {
            try {
                if (pstmt != null) pstmt.close();
            } catch (Exception e) {}
            try {
                if (conn != null) conn.close();
            } catch (Exception e) {}
        }
    }
    
    /**
     * Make SQL call to get summary of all pending reviews except for Ancillary, which is 
     * handled differently.
     */
    public void getSummaryPendingReviews(int accountId, int grpId, int userId) {
        clearSummaryReviews();
        
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            StringBuffer sql = new StringBuffer();
            
            sql.append(" select "); 
            sql.append(" s1.grouping_flag,c.codelst_subtyp, ");
            sql.append(" (select usr_firstname ||' '|| usr_lastname from er_user ");
            sql.append("  where pk_user = s1.pk_user) reviewer, ");
            sql.append(" s1.review_count ");
            sql.append(" from ");
            sql.append(" ( ");
            sql.append("  select grouping(submission_review_type)||grouping(pk_user) grouping_flag, ");
            sql.append("  submission_review_type, pk_user, count(rowcount) review_count from ");
            sql.append("  ( ");
            sql.append("   select mainq.*,pk_user from ");
            sql.append("   ( ");
            sql.append("    Select st.rowid rowcount, study_number, study_title, ");
            sql.append("    F_CODELST_DESC(submission_type) submission_type_desc,submission_type, ");
            sql.append("    F_CODELST_DESC(st.submission_status) submission_status_desc,st.submission_status, ");
            sql.append("    to_char((select meeting_date from er_review_meeting where pk_review_meeting = sb.fk_review_meeting),pkg_dateutil.f_get_dateformat) ");
            sql.append("    submission_meeting_date, ");
            sql.append("    F_CODELST_DESC (sb.submission_review_type) review_type_desc, ");
            sql.append("    sb.submission_review_type, ");
            sql.append("    (select review_board_name from er_review_board where pk_review_board = fk_review_board) submission_board_name, ");
            sql.append("    fk_review_board, pk_study ,pk_submission, pk_submission_board, sb.submission_reviewer ");
            sql.append("    from er_submission sub,er_submission_board sb,er_study s,er_submission_status st ");
            sql.append("    where sub.fk_study = s.pk_study and sub.pk_submission = sb.fk_submission and st.fk_submission_board = sb.pk_submission_board and is_current = 1  " );
            sql.append("     and st.submission_status =(select pk_codelst from er_codelst where codelst_type ='subm_status' and codelst_subtyp='reviewer') ");
            sql.append("    and fk_review_board in  (select pk_review_board from er_review_board erb where erb.fk_account = "+accountId+" and ','|| erb.board_group_access || ',' like '%," + grpId+ ",%' ) ");
            sql.append("    and s.fk_account = "+ accountId + "  and  (( exists ( select * from er_studyteam t where t.fk_study = s.pk_study and "); 
            sql.append("    t.fk_user = "+userId+" and  nvl(t.study_team_usr_type,'D')='D') or pkg_superuser.F_Is_Superuser("+userId+", pk_study) = 1 ) or  ','|| sb.submission_reviewer || ',' like '%," + userId+ ",%') ");
            sql.append("   ) mainq  ");
            sql.append("   left outer join er_user u ");
            sql.append("   on ','||mainq.submission_reviewer||',' like '%,'||to_char(u.pk_user)||',%' ");
            sql.append("  ) subq ");
            sql.append("  group by cube(submission_review_type, pk_user) ");
            sql.append("  order by grouping(submission_review_type),grouping(pk_user) ");
            sql.append("  ) s1 ");
            sql.append("  left outer join er_codelst c ");
            sql.append("  on s1.submission_review_type = c.pk_codelst and c.codelst_type = 'revType' ");
            sql.append("  where grouping_flag in ('00', '01') and codelst_subtyp is not null ");
            sql.append("  order by s1.grouping_flag desc, s1.submission_review_type, s1.review_count desc, reviewer ");

            pstmt = conn.prepareStatement(sql.toString());
            ResultSet rs = pstmt.executeQuery();
            
            int arrayIndex = 0;
            String lastSubtype = null;
            while (rs.next()) {
                if ("01".equals(rs.getString("grouping_flag"))) {
                    setSubtotalSummaryReviewSubtypes(rs.getString("codelst_subtyp"));
                    setSubtotalSummaryReviewCounts(rs.getInt("review_count"));
                }
                if ("00".equals(rs.getString("grouping_flag"))) {
                    if (summaryReviewSubtypes == null) {
                        summaryReviewSubtypes = new ArrayList[subtotalSummaryReviewCounts.size()];
                        for (int iX=0; iX<summaryReviewSubtypes.length; iX++) {
                            summaryReviewSubtypes[iX] = new ArrayList();
                        }
                    }
                    if (summaryReviewCounts == null) {
                        summaryReviewCounts = new ArrayList[subtotalSummaryReviewCounts.size()];
                        for (int iX=0; iX<summaryReviewCounts.length; iX++) {
                            summaryReviewCounts[iX] = new ArrayList();
                        }
                    }
                    if (summaryReviewReviewers == null) {
                        summaryReviewReviewers = new ArrayList[subtotalSummaryReviewCounts.size()];
                        for (int iX=0; iX<summaryReviewReviewers.length; iX++) {
                            summaryReviewReviewers[iX] = new ArrayList();
                        }
                    }
                    String thisSubtype = rs.getString("codelst_subtyp");
                    if (lastSubtype != null && !lastSubtype.equals(thisSubtype)) {
                        ++arrayIndex;
                    }
                    if (arrayIndex >= summaryReviewSubtypes.length) { break; }
                    summaryReviewSubtypes[arrayIndex].add(rs.getString("codelst_subtyp"));
                    summaryReviewReviewers[arrayIndex].add(rs.getString("reviewer"));
                    summaryReviewCounts [arrayIndex].add(rs.getInt("review_count"));
                    lastSubtype = thisSubtype;
                }
            }
        } catch(SQLException e) {
            Rlog.fatal("EIRBDao", " error in getSummaryPendingReviews() "+e);
        } finally {
            try {
                if (pstmt != null) pstmt.close();
            } catch (Exception e) {}
            try {
                if (conn != null) conn.close();
            } catch (Exception e) {}
        }
    }
    
    /**
     * Make SQL call to get summary of pending Ancillary Committee reviews.
     */
    public void getSummaryAncillaryReviews(int accountId, int grpId, int userId) {
        clearSummaryAncillaryReviews();
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            StringBuffer sql = new StringBuffer();
            
            sql.append(" select "); 
            sql.append(" s1.grouping_flag, ");
            sql.append(" (select review_board_name FROM er_review_board ");
            sql.append("  where pk_review_board = fk_review_board) review_board, ");
            sql.append(" (select usr_firstname ||' '|| usr_lastname from er_user ");
            sql.append("  where pk_user = s1.pk_user) reviewer, ");
            sql.append(" s1.review_count ");
            sql.append(" from ");
            sql.append(" ( ");
            sql.append("  select grouping(fk_review_board)||grouping(pk_user) grouping_flag, ");
            sql.append("  fk_review_board, pk_user, count(rowcount) review_count from ");
            sql.append("  ( ");
            sql.append("   select mainq.*,pk_user from ");
            sql.append("   ( ");
            sql.append("    Select st.rowid rowcount, study_number, study_title, ");
            sql.append("    F_CODELST_DESC(submission_type) submission_type_desc,submission_type, ");
            sql.append("    F_CODELST_DESC(st.submission_status) submission_status_desc,st.submission_status, ");
            sql.append("    to_char((select meeting_date from er_review_meeting where pk_review_meeting = sb.fk_review_meeting),pkg_dateutil.f_get_dateformat) ");
            sql.append("    submission_meeting_date, ");
            sql.append("    F_CODELST_DESC (sb.submission_review_type) review_type_desc, ");
            sql.append("    sb.submission_review_type, ");
            sql.append("    (select review_board_name from er_review_board where pk_review_board = fk_review_board) submission_board_name, ");
            sql.append("    fk_review_board, pk_study ,pk_submission, pk_submission_board, sb.submission_reviewer ");
            sql.append("    from er_submission sub,er_submission_board sb,er_study s,er_submission_status st ");
            sql.append("    where sub.fk_study = s.pk_study and sub.pk_submission = sb.fk_submission and st.fk_submission_board = sb.pk_submission_board and is_current = 1 " );
            sql.append("     and st.submission_status =(select pk_codelst from er_codelst where codelst_type ='subm_status' and codelst_subtyp='reviewer') ");
            sql.append("    and fk_review_board in  (select pk_review_board from er_review_board erb where erb.fk_account = "+accountId+" and ','|| erb.board_group_access || ',' like '%," + grpId+ ",%' ) ");
            sql.append("    and submission_review_type = F_CODELST_ID('revType','rev_anc') ");
            sql.append("    and s.fk_account = "+ accountId + "  and  ( exists ( select * from er_studyteam t where t.fk_study = s.pk_study and "); 
            sql.append("    t.fk_user = "+userId+" and  nvl(t.study_team_usr_type,'D')='D') or pkg_superuser.F_Is_Superuser("+userId+", pk_study) = 1  or ','|| sb.submission_reviewer || ',' like '%," + userId+ ",%') ");
            sql.append("   ) mainq  ");
            sql.append("   left outer join er_user u ");
            sql.append("   on ','||mainq.submission_reviewer||',' like '%,'||to_char(u.pk_user)||',%' ");
            sql.append("  ) subq ");
            sql.append("  group by cube(fk_review_board, pk_user) ");
            sql.append("  order by grouping(fk_review_board),grouping(pk_user) ");
            sql.append("  ) s1 ");
            sql.append("  where grouping_flag in ('00', '11') ");
            sql.append("  order by s1.grouping_flag desc, s1.fk_review_board, s1.review_count desc, reviewer ");

            pstmt = conn.prepareStatement(sql.toString());
            ResultSet rs = pstmt.executeQuery();
            
            String lastBoard = null;
            while (rs.next()) {
                if ("11".equals(rs.getString("grouping_flag"))) {
                    setSubtotalSummaryAncillaryReviewCounts(rs.getInt("review_count"));
                }
                if ("00".equals(rs.getString("grouping_flag"))) {
                    if (summaryAncillaryReviewBoards == null) {
                        summaryAncillaryReviewBoards = new ArrayList();
                    }
                    if (summaryAncillaryReviewCounts == null) {
                        summaryAncillaryReviewCounts = new ArrayList();
                    }
                    if (summaryAncillaryReviewReviewers == null) {
                        summaryAncillaryReviewReviewers = new ArrayList();
                    }
                    summaryAncillaryReviewBoards.add(rs.getString("review_board"));
                    summaryAncillaryReviewReviewers.add(rs.getString("reviewer"));
                    summaryAncillaryReviewCounts.add(rs.getInt("review_count"));
                }
            }

        } catch(Exception e) {
            Rlog.fatal("EIRBDao", " error in getSummaryPendingReviews() "+e);
        } finally {
            try {
                if (pstmt != null) pstmt.close();
            } catch (Exception e) {}
            try {
                if (conn != null) conn.close();
            } catch (Exception e) {}
        }
    }

 public static String getIRBFormSubmissionTypeSQL(String callingTab,String formCategory, String submissionSubtype) {
        
        PreparedStatement pstmt = null;
        Connection conn = null;
        String formSubissionSubtype = "";
        
        try {
            conn = getConnection();
    	  	StringBuffer sbSQL= new StringBuffer();
    	  	sbSQL.append(" select FORM_SUBMISSION_SUBTYPE from er_irbforms_setup where IRB_TAB_SUBTYPE = ?");
    	  	sbSQL.append(" and  CODELST_SUBMISSION_SUBTYPE = ? and FORM_CATLIB_SUBTYPE = ?");
            String sql = sbSQL.toString();
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, callingTab);
            pstmt.setString(2, submissionSubtype);
            pstmt.setString(3, formCategory);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                formSubissionSubtype = rs.getString("FORM_SUBMISSION_SUBTYPE");
                 
            }
        } catch(SQLException e) {
            Rlog.fatal("EIRBDao", " Exception in getIRBFormSubmissionTypeSQL() "+e);
        } finally {
            try {
                if (pstmt != null) pstmt.close();
            } catch (Exception e) {}
            try {
                if (conn != null) conn.close();
            } catch (Exception e) {}
            
        }
        return formSubissionSubtype;
    }

///////////////////////////
 /** Returns Provisos added for a submission and Submission board */
 public static ArrayList getSubmissionProvisos(int fkSubmission, int fkSubmissionBoard ) {
     ArrayList arResults = new ArrayList();
     
     PreparedStatement pstmt = null;
     Connection conn = null;
     try {
         conn = getConnection();
         StringBuffer sql = new StringBuffer();
          
         sql.append("Select PK_SUBMISSION_PROVISO, FK_USER_ENTEREDBY,usr_lst(FK_USER_ENTEREDBY) User_enteredby_name ,  ");
         sql.append(" PROVISO_DATE, PROVISO , fk_codelst_provisotype,proviso_flag from er_submission_proviso where fk_submission = ? and  ");
         sql.append(" fk_submission_board = ? order by PROVISO_DATE desc");
          
         pstmt = conn.prepareStatement(sql.toString());
         pstmt.setInt(1, fkSubmission);
         pstmt.setInt(2, fkSubmissionBoard);
         
         
         ResultSet rs = pstmt.executeQuery();
         while (rs.next()) {
        	 	SubmissionProvisoBean sb = new SubmissionProvisoBean ();
            	Clob objCLob = null;
            	String provisoStr = "";
            	
        	 	sb.setId(rs.getInt("PK_SUBMISSION_PROVISO"));
        	 //	sb.setfkSubmissionStatus(rs.getInt("FK_SUBMISSION_STATUS"));
        	 	sb.setProvisoDate(rs.getDate("PROVISO_DATE"));
        	 	sb.setProvisoEnteredBy(rs.getInt("FK_USER_ENTEREDBY"));
        	 	sb.setProvisoEnteredByName(rs.getString("User_enteredby_name"));
        	 	sb.setprovisoflag(rs.getString("proviso_flag"));
        	 	
        	 	objCLob = rs.getClob("PROVISO");
        	 	
        	    if (!(objCLob == null)) {
                	provisoStr= objCLob.getSubString(1, (int) objCLob.length());
                }
        	    
        	 	sb.setSubmissionProviso(provisoStr);
        	 	sb.setProvisoType(rs.getString("fk_codelst_provisotype"));
         
        	 	arResults.add(sb);
         }
     } catch(SQLException e) {
         Rlog.fatal("EIRBDao", " error in getSubmissionProvisos() "+e);
     } finally {
         try {
             if (pstmt != null) pstmt.close();
         } catch (Exception e) {}
         try {
             if (conn != null) conn.close();
         } catch (Exception e) {}
     }
     return arResults;
 }

 
 /** Returns Minutes of Meeting for a review board and meeting date*/
 public static String getreviewMeetingMOM(int fkBoard, int fkMeetingDatePK) {
     String mom = "";;
     
     PreparedStatement pstmt = null;
     PreparedStatement pstmt2 = null;
     Connection conn = null;
     try {
         conn = getConnection();
         StringBuffer sql = new StringBuffer();
         
         String momSql = "";
         sql.append("Select board_mom_logic from er_review_board ");
         sql.append(" where pk_review_board = ? ");
          
         pstmt = conn.prepareStatement(sql.toString());
         pstmt.setInt(1, fkBoard);
         
         
         ResultSet rs = pstmt.executeQuery();
         while (rs.next()) {
        	 momSql = rs.getString("board_mom_logic");
         }
         
         rs.close();
         pstmt.close();
         
         if (! StringUtil.isEmpty(momSql ))
         {
	         pstmt2 =  conn.prepareStatement(momSql);
	         pstmt2.setInt(1, fkBoard);
	         pstmt2.setInt(2, fkMeetingDatePK);
	         
	         ResultSet rsClob = pstmt2.executeQuery();
	         
	         while (rsClob.next()) {
	            	Clob objCLob = null;
	            	
	        	 	objCLob = rsClob.getClob(1);
	        	 	
	        	    if (!(objCLob == null)) {
	                	mom= objCLob.getSubString(1, (int) objCLob.length());
	                }
	        	    
	        	 	
	         }
	         rsClob.close();
         }
         
     } catch(SQLException e) {
         Rlog.fatal("EIRBDao", " Exeception in getreviewMeetingMOM() "+e);
     } finally {
         try {
             if (pstmt2 != null) pstmt2.close();
             
         } catch (Exception e) {}
         try {
             if (conn != null) conn.close();
         } catch (Exception e) {}
     }
     return mom;
 }

 /** Updates Minutes of Meeting for a review board and meeting date*/
 public static int updateReviewMeetingMOM( int fkMeetingDatePK,String mom) {
	 int ret = 0;
	 try
	 {
		 CommonDAO cDao = new CommonDAO();            
		              
		 cDao.updateClob(mom,"er_review_meeting","meeting_minutes"," where pk_review_meeting  = " + fkMeetingDatePK );
	 }catch(Exception e)
	 {
		 Rlog.fatal("EIRBDao", " Exeception in updateReviewMeetingMOM() "+e);
		 ret = -1;
	 }
	 
 return ret;
 }
//////////////////////////////////////

/** List of tabs where Action page will be opened after Complete form status; 
 *      the array <b>must be sorted</b>!!!! */
 private static String[] arTabsForActionWindow= {
     "irb_assigned_tab",
     "irb_compl_tab",
     "irb_meeting",
     "irb_new_tab",
     "irb_pend_tab",
     "irb_post_tab",
     "irb_revanc_tab",
     "irb_revdummy_tab",
     "irb_revexem_tab",
     "irb_revexp_tab",
     "irb_revfull_tab",
     "irb_revsumm_tab"
     };

 /** Returns true for a tab where Action page will be opened after Complete form status */
 public static boolean isTabForActionWindow(String selectedTab)
 {
	 boolean val = false;
	 int pos = -1;
	 
	 pos= Arrays.binarySearch(arTabsForActionWindow, selectedTab);
	 
	 if (pos >= 0 )
	 {
		 
		 val = true;
	 }
	 
	 return val;
	 
 }
//////////////////////////////////////
 public boolean getSubmissionAccess(String user, String submissionPK) {
     PreparedStatement pstmt = null;
     Connection conn = null;
     try {
         conn = getConnection();
         StringBuffer sql = new StringBuffer();
         sql.append(" SELECT count(*) FROM er_studyteam t ");
         sql.append(" inner join er_submission s on t.fk_study = s.fk_study ");
         sql.append(" where ((t.fk_user = ").append(user).append(" and  nvl(t.study_team_usr_type,'D')='D') or ");
         sql.append(" pkg_superuser.F_Is_Superuser(").append(user).append(", t.fk_study) = 1 ) ");
         sql.append(" and s.pk_submission = ").append(StringUtil.stringToNum(submissionPK));
         pstmt = conn.prepareStatement(sql.toString());
         ResultSet rs = pstmt.executeQuery();
         int count = 0;
         while(rs.next()) {
             count = rs.getInt(1);
         }
         if (count > 0) { return true; }
     } catch (Exception e) {
         Rlog.fatal("EIRBDao", "Exeception in getSubmissionAccess() "+e);
         return false;
     } finally {
         try {
             if (pstmt != null) pstmt.close();
         } catch (Exception e) {}
         try {
             if (conn != null) conn.close();
         } catch (Exception e) {}
     }
     return false;
 }
 
 public void getSubmissionHistory(int submissionPK, int submissionBoardPK) {
     clearHistory();
     PreparedStatement pstmt = null;
     Connection conn = null;
     try {
         conn = getConnection();
         StringBuffer sql = new StringBuffer();
         sql.append(" select (select codelst_desc from er_codelst where ");
         sql.append(" pk_codelst = SUBMISSION_STATUS) status_desc, ");
         sql.append(" SUBMISSION_STATUS_DATE, (select USR_FIRSTNAME||' '||USR_LASTNAME from er_user ");
         sql.append(" where pk_user = SUBMISSION_ENTERED_BY) entered_by, SUBMISSION_NOTES ");
         sql.append(" from ER_SUBMISSION_STATUS where FK_SUBMISSION = ").append(submissionPK).append(" ");
         sql.append(" AND FK_SUBMISSION_BOARD = ").append(submissionBoardPK).append(" order by ");
         sql.append(" to_date(to_char(SUBMISSION_STATUS_DATE, PKG_DATEUTIL.f_get_dateformat), PKG_DATEUTIL.f_get_dateformat) desc, ");
         sql.append(" PK_SUBMISSION_STATUS desc ");
         pstmt = conn.prepareStatement(sql.toString());
         ResultSet rs = pstmt.executeQuery();
         while(rs.next()) {
             setHistoryStatusDesc(rs.getString("status_desc"));
             setHistoryEnteredBy(rs.getString("entered_by"));
             setHistoryDate(DateUtil.dateToString(rs.getDate("SUBMISSION_STATUS_DATE")));
             setHistoryNotes(rs.getString("SUBMISSION_NOTES"));
         }
     } catch (SQLException e) {
         Rlog.fatal("EIRBDao", "Exeception in getSubmissionHistory() "+e);
     } finally {
         try {
             if (pstmt != null) pstmt.close();
         } catch (Exception e) {}
         try {
             if (conn != null) conn.close();
         } catch (Exception e) {}
     }
 }
 
/** Gets the one status previous from the current status for a submission. Returns the Pk of the codelst
 * */
 public static String getPreviousCurrentSubmissionStatus(int submissionPK, int submissionBoardPK) {
     
     PreparedStatement pstmt = null;
     Connection conn = null;
     String status = "";
     
     try {
         conn = getConnection();
         StringBuffer sql = new StringBuffer();
         
         sql.append(" select submission_status from er_submission_status where pk_submission_status = (select max(i.pk_submission_status ) ");
         sql.append("    from er_submission_status i where i.fk_submission = ? and i.fk_submission_board = ?  and i.is_current = 0)");
         
         
         pstmt = conn.prepareStatement(sql.toString());
         
         pstmt.setInt(1,submissionPK);
         pstmt.setInt(2,submissionBoardPK);
         
         ResultSet rs = pstmt.executeQuery();
         while(rs.next()) {
             status = rs.getString("submission_status");
         }
         
         return status;
     } catch (SQLException e) {
         Rlog.fatal("EIRBDao", "Exeception in getPreviousCurrentSubmissionStatus() "+e);
     } finally {
         try {
             if (pstmt != null) pstmt.close();
         } catch (Exception e) {}
         try {
             if (conn != null) conn.close();
         } catch (Exception e) {}
         
         return status;
         
     }
 }
 
 
 public static String getRecentSubmissionDate(int studyId, int reviewBoard, int statusCode) {
	    
	    PreparedStatement pstmt = null;
	    Connection conn = null;
	    String submissionDate = "";
	    
	    try {
	        conn = getConnection();
	        String sql = " select max(submission_status_date) as submission_date from ER_SUBMISSION_STATUS status left outer join ER_SUBMISSION submission on (status.FK_SUBMISSION=submission.PK_SUBMISSION"+
	        			  " and submission.FK_STUDY=?) left outer join ER_SUBMISSION_BOARD submBoard on (submBoard.FK_SUBMISSION=submission.PK_SUBMISSION and submBoard.FK_REVIEW_BOARD=?)"+
	        			  " where status.SUBMISSION_STATUS=?";
	        
	        
	        pstmt = conn.prepareStatement(sql);
	        
	        pstmt.setInt(1,studyId);
	        pstmt.setInt(2,reviewBoard);
	        pstmt.setInt(3,statusCode);
	        
	        ResultSet rs = pstmt.executeQuery();
	        while(rs.next()) {
	        	submissionDate = rs.getString("submission_date");
	        }
	        
	        return submissionDate;
	    } catch (SQLException e) {
	        Rlog.fatal("EIRBDao", "Exeception in getPreviousCurrentSubmissionStatus() "+e);
	    } finally {
	        try {
	            if (pstmt != null) pstmt.close();
	        } catch (Exception e) {}
	        try {
	            if (conn != null) conn.close();
	        } catch (Exception e) {}
	        
	        return submissionDate;
	        
	    }
	}
 
 public void updateSubmitToBoard(int pksubmission, String boardIds) {
	    PreparedStatement pstmt = null;
	    Connection conn = null;
	    try {
	        conn = getConnection();
	        StringBuffer sql = new StringBuffer(); 
	        sql.append(" update er_submission ");
	        sql.append("set submit_to_board = ? where PK_SUBMISSION = ? ");
	        
	        pstmt = conn.prepareStatement(sql.toString());
	        pstmt.setString(1, boardIds);
	        pstmt.setInt(2, pksubmission);
	        ResultSet rs = pstmt.executeQuery();
	        
	    } catch (SQLException e) {
	        Rlog.fatal("EIRBDao", "Exeception in updateSubmitToBoard() "+e);
	    } finally {
	        try {
	            if (pstmt != null) pstmt.close();
	        } catch (Exception e) {}
	        try {
	            if (conn != null) conn.close();
	        } catch (Exception e) {}
	    }
	}
 

 
 /** returns the PK of the default review board for an account ONLY if the logged in uder's default group (passed as a parameter)
  * has access to the review board
  * 
  * returns 0 if no default group is configured or the group does not have access to it
  * 
  * */
 
 public static int getDefaultReviewBoardWithCheckGroupAccess(int accountId,int grpId) {
      

     PreparedStatement pstmt = null;
     Connection conn = null;
     
     int reviewBoardPK = 0;
     try {
         conn = getConnection();
         StringBuffer sql = new StringBuffer();
         
         sql.append("select pk_review_board ");
         sql.append(" from er_review_board r ");
         sql.append(" where r.fk_account = ? and nvl(review_board_default,0) = 1 and ',' || r.board_group_access || ',' LIKE '%,")
         .append(grpId).append(",%' ");
         
         pstmt = conn.prepareStatement(sql.toString());
         pstmt.setInt(1, accountId);
         
         ResultSet rs = pstmt.executeQuery();
         while (rs.next()) {
        	 
        	 reviewBoardPK = rs.getInt("pk_review_board");
        	 
        	 return reviewBoardPK;
        	 
         }
         
     } catch(SQLException e) {
         Rlog.fatal("EIRBDao", " error in getDefaultReviewBoardAndCheckGroupAccess() "+e);
     } finally {
         try {
             if (pstmt != null) pstmt.close();
         } catch (Exception e) {}
         try {
             if (conn != null) conn.close();
         } catch (Exception e) {}
     
         return reviewBoardPK;
     }
 }
 private static String getDefaultSubmissionBoardIdSql =
	 " select PK_SUBMISSION_BOARD from ER_SUBMISSION_BOARD where FK_SUBMISSION = ? and " 
			 +" FK_REVIEW_BOARD = (select pk_review_board from er_review_board where "
			 +" fk_account = (select FK_ACCOUNT from ER_STUDY, ER_SUBMISSION where "
			 +" PK_STUDY = FK_STUDY and PK_SUBMISSION = ?) and REVIEW_BOARD_DEFAULT = 1 and rownum <= 1) "
			 +" order by PK_SUBMISSION_BOARD desc ";

public int getDefaultSubmissionBoardId(int submissionId) {
 int boardId = 0;
 PreparedStatement pstmt = null;
 Connection conn = null;
 ResultSet rs = null;
 try {
     conn = getConnection();
     pstmt = conn.prepareStatement(getDefaultSubmissionBoardIdSql);
     pstmt.setInt(1, submissionId);
     pstmt.setInt(2, submissionId);
     rs = pstmt.executeQuery();
     while (rs.next()) {
    	 boardId = rs.getInt(1);
    	 break;
     }
 } catch(SQLException e) {
     Rlog.fatal("EIRBDao", " Exception in getDefaultSubmissionBoardId() "+e);
 } finally {
     try {
         if (rs != null) rs.close();
     } catch (Exception e) {}
     try {
         if (pstmt != null) pstmt.close();
     } catch (Exception e) {}
     try {
         if (conn != null) conn.close();
     } catch (Exception e) {}
 }
 return boardId;
}
 
 public static int getDefaultReviewBoardWithCheckGroupAccess(int accountId,int grpId, int submissionBoardPK) {
     

     PreparedStatement pstmt = null;
     Connection conn = null;
     
     int reviewBoardPK = 0;
     try {
         conn = getConnection();
         StringBuffer sql = new StringBuffer();
         
         sql.append("select fk_review_board ");
         sql.append(" from er_submission_board sr ");
         sql.append(" where sr.pk_submission_board = ? ");
         
         pstmt = conn.prepareStatement(sql.toString());
         pstmt.setInt(1, submissionBoardPK);
         
         ResultSet rs = pstmt.executeQuery();
         while (rs.next()) {
        	 
        	 reviewBoardPK = rs.getInt("fk_review_board");
        	 
        	 return reviewBoardPK;
        	 
         }
         
     } catch(SQLException e) {
         Rlog.fatal("EIRBDao", " error in getDefaultReviewBoardAndCheckGroupAccess() "+e);
     } finally {
         try {
             if (pstmt != null) pstmt.close();
         } catch (Exception e) {}
         try {
             if (conn != null) conn.close();
         } catch (Exception e) {}
     
         return reviewBoardPK;
     }
 }


 public static String getIRBFormSubmissionTypes(String submissionSubtype) {
     
     PreparedStatement pstmt = null;
     Connection conn = null;
     String formSubissionSubtype = "";
     
     try {
         conn = getConnection();
	  	 StringBuffer sbSQL= new StringBuffer();
	  	 sbSQL.append(" select FORM_SUBMISSION_SUBTYPE from er_irbforms_setup where CODELST_SUBMISSION_SUBTYPE = '"+submissionSubtype + "'");
	  	 String sql = sbSQL.toString();
         pstmt = conn.prepareStatement("select pkg_util.f_join(cursor("+sql+"),''',''') from dual");
         ResultSet rs = pstmt.executeQuery();
         while (rs.next()) {
             formSubissionSubtype = rs.getString(1);
         }
         formSubissionSubtype = "'" + formSubissionSubtype+ "'"; //append quotes 
         
     } catch(SQLException e) {
         Rlog.fatal("EIRBDao", " Exception in getIRBFormSubmissionTypes() "+e);
     } finally {
         try {
             if (pstmt != null) pstmt.close();
         } catch (Exception e) {}
         try {
             if (conn != null) conn.close();
         } catch (Exception e) {}
         
     }
     return formSubissionSubtype;
 }

 public String getSubmissionStatus() {
	    

	    PreparedStatement pstmt = null;
	    Connection conn = null;
	    
	    String Status = "";
	    try {
	        conn = getConnection();
	        StringBuffer sql = new StringBuffer();
	        
	        sql.append("select codelst_desc ");
	        sql.append(" from er_codelst  ");
	        sql.append(" where codelst_type='subm_status'and codelst_subtyp = 'approved' ");
	        
	        pstmt = conn.prepareStatement(sql.toString());
	        //pstmt.setInt(1, submissionBoardPK);
	        
	        ResultSet rs = pstmt.executeQuery();
	        while (rs.next()) {
	       	 
	        	Status = rs.getString("codelst_desc");
	       	 
	       	 return Status;
	       	 
	        }
	        
	    } catch(SQLException e) {
	        Rlog.fatal("EIRBDao", " error in getSubmissionStatus() "+e);
	    } finally {
	        try {
	            if (pstmt != null) pstmt.close();
	        } catch (Exception e) {}
	        try {
	            if (conn != null) conn.close();
	        } catch (Exception e) {}
	    
	        return Status;
	    }
	}
 
 public String getSubmissionStatusGov() {
	    

	    PreparedStatement pstmt = null;
	    Connection conn = null;
	    
	    String Status = "";
	    try {
	        conn = getConnection();
	        StringBuffer sql = new StringBuffer();
	        
	        sql.append("select codelst_desc ");
	        sql.append(" from er_codelst  ");
	        sql.append(" where codelst_type='subm_status'and codelst_subtyp = 'gov-approved' ");
	        
	        pstmt = conn.prepareStatement(sql.toString());
	        
	        ResultSet rs = pstmt.executeQuery();
	        while (rs.next()) {
	       	 
	        	Status = rs.getString("codelst_desc");
	       	 
	       	 return Status;
	       	 
	        }
	        
	    } catch(SQLException e) {
	        Rlog.fatal("EIRBDao", " error in getSubmissionStatusGov() "+e);
	    } finally {
	        try {
	            if (pstmt != null) pstmt.close();
	        } catch (Exception e) {}
	        try {
	            if (conn != null) conn.close();
	        } catch (Exception e) {}
	    
	        return Status;
	    }
	}

 
 public int getOldSubmStatus(int submissionPK,int submissionBoardPK) {
	    

	    PreparedStatement pstmt = null;
	    Connection conn = null;
	    
	    int Status = 0;
	    
	    try {
	        conn = getConnection();
	        StringBuffer sql = new StringBuffer();
	        
	        sql.append("select max(SUBMISSION_STATUS) as submission_status ");
	        sql.append(" from er_submission_status  ");
	        sql.append(" where fk_submission = ?  and fk_submission_board = ? ");
	        
	        pstmt = conn.prepareStatement(sql.toString());
	        pstmt.setInt(1, submissionPK);
	        pstmt.setInt(2, submissionBoardPK);
	        //pstmt.setInt(1, submissionBoardPK);
	        
	        ResultSet rs = pstmt.executeQuery();
	        while (rs.next()) {
	       	 
	        	Status = rs.getInt("SUBMISSION_STATUS");
	       	 
	       	 return Status;
	       	 
	        }
	        
	    } catch(SQLException e) {
	        Rlog.fatal("EIRBDao", " error in getOldSubmStatus() "+e);
	    } finally {
	        try {
	            if (pstmt != null) pstmt.close();
	        } catch (Exception e) {}
	        try {
	            if (conn != null) conn.close();
	        } catch (Exception e) {}
	    
	        return Status;
	    }
	}
 
 public int getPkCodelstForPiResponded(){
	 
	 
	 PreparedStatement pstmt = null;
	    Connection conn = null;
	    
	    int pkStatus = 0;
	    try {
	        conn = getConnection();
	        StringBuffer sql = new StringBuffer();
	        
	        sql.append(" select pk_codelst from er_codelst where codelst_type = 'subm_status' and codelst_subtyp ='piresponded' ");
	        pstmt = conn.prepareStatement(sql.toString());
	        ResultSet rs = pstmt.executeQuery();
	        while (rs.next()) {
	       	 
	        	pkStatus = rs.getInt("Pk_codelst");
	       	 
	       	 return pkStatus;
	       	 
	        }
	        
	    } catch(SQLException e) {
	        Rlog.fatal("EIRBDao", " error in getPkCodelstForPiResponded() "+e);
	    } finally {
	        try {
	            if (pstmt != null) pstmt.close();
	        } catch (Exception e) {}
	        try {
	            if (conn != null) conn.close();
	        } catch (Exception e) {}
	    
	        return pkStatus;
	    }
	 
	 
	 
 }
 public int getPkSubmissionStatus(int submissionPK,int submissionBoardPK) {
	    

	    PreparedStatement pstmt = null;
	    Connection conn = null;
	    
	    int pkStatus = 0;
	    
	    try {
	        conn = getConnection();
	        StringBuffer sql = new StringBuffer();
	        
	        sql.append("select max(PK_SUBMISSION_STATUS) as submission_status ");
	        sql.append(" from er_submission_status  ");
	        sql.append(" where fk_submission = ?  and fk_submission_board = ? ");
	        
	        pstmt = conn.prepareStatement(sql.toString());
	        pstmt.setInt(1, submissionPK);
	        pstmt.setInt(2, submissionBoardPK);
	        //pstmt.setInt(1, submissionBoardPK);
	        
	        ResultSet rs = pstmt.executeQuery();
	        while (rs.next()) {
	       	 
	        	pkStatus = rs.getInt("SUBMISSION_STATUS");
	       	 
	       	 return pkStatus;
	       	 
	        }
	        
	    } catch(SQLException e) {
	        Rlog.fatal("EIRBDao", " error in getPkSubmissionStatus() "+e);
	    } finally {
	        try {
	            if (pstmt != null) pstmt.close();
	        } catch (Exception e) {}
	        try {
	            if (conn != null) conn.close();
	        } catch (Exception e) {}
	    
	        return pkStatus;
	    }
	}
 
 
 public void updateSubmissionStatusProvisoFlag(int submissionPK, int submissionBoardPK) {
     clearHistory();
     PreparedStatement pstmt = null;
     Connection conn = null;
     try {
         conn = getConnection();
         StringBuffer sql = new StringBuffer(); 
         sql.append(" update er_submission_proviso ");
         sql.append("set proviso_flag = 'Y' where fk_submission = ? and fk_submission_board = ? ");
         
         pstmt = conn.prepareStatement(sql.toString());
         pstmt.setInt(1, submissionPK);
	     pstmt.setInt(2, submissionBoardPK);
         ResultSet rs = pstmt.executeQuery();
         
     } catch (SQLException e) {
         Rlog.fatal("EIRBDao", "Exeception in updateSubmissionStatusProvisoFlag() "+e);
     } finally {
         try {
             if (pstmt != null) pstmt.close();
         } catch (Exception e) {}
         try {
             if (conn != null) conn.close();
         } catch (Exception e) {}
     }
 }
 

 
 public int getdeleteCurrentSubmissionStatus(int submissionPK, int submissionBoardPK) {
	    
	    PreparedStatement pstmt = null;
	    Connection conn = null;
	    int pkstatus = 0;
	    
	    try {
	        conn = getConnection();
	        StringBuffer sql = new StringBuffer();
	        
	        sql.append(" select pk_submission_status from er_submission_status where pk_submission_status = (select max(i.pk_submission_status ) ");
	        sql.append("    from er_submission_status i where i.fk_submission = ? and i.fk_submission_board = ?  and i.is_current = 1)");
	        
	        
	        pstmt = conn.prepareStatement(sql.toString());
	        
	        pstmt.setInt(1,submissionPK);
	        pstmt.setInt(2,submissionBoardPK);
	        
	        ResultSet rs = pstmt.executeQuery();
	        while(rs.next()) {
	        	pkstatus = rs.getInt("pk_submission_status");
	        }
	        
	        return pkstatus;
	    } catch (SQLException e) {
	        Rlog.fatal("EIRBDao", "Exeception in getdeleteCurrentSubmissionStatus() "+e);
	    } finally {
	        try {
	            if (pstmt != null) pstmt.close();
	        } catch (Exception e) {}
	        try {
	            if (conn != null) conn.close();
	        } catch (Exception e) {}
	        
	        return pkstatus;
	        
	    }
	}
 
 public int getPrevioustSubmissionStatus(int submissionPK, int submissionBoardPK) {
	    
	    PreparedStatement pstmt = null;
	    Connection conn = null;
	    int pkstatus = 0;
	    
	    try {
	        conn = getConnection();
	        StringBuffer sql = new StringBuffer();
	        
	        sql.append(" select pk_submission_status from er_submission_status where pk_submission_status = (select max(i.pk_submission_status ) ");
	        sql.append("    from er_submission_status i where i.fk_submission = ? and i.fk_submission_board = ?  and i.is_current = 0)");
	        
	        
	        pstmt = conn.prepareStatement(sql.toString());
	        
	        pstmt.setInt(1,submissionPK);
	        pstmt.setInt(2,submissionBoardPK);
	        
	        ResultSet rs = pstmt.executeQuery();
	        while(rs.next()) {
	        	pkstatus = rs.getInt("pk_submission_status");
	        }
	        
	        return pkstatus;
	    } catch (SQLException e) {
	        Rlog.fatal("EIRBDao", "Exeception in getdeleteCurrentSubmissionStatus() "+e);
	    } finally {
	        try {
	            if (pstmt != null) pstmt.close();
	        } catch (Exception e) {}
	        try {
	            if (conn != null) conn.close();
	        } catch (Exception e) {}
	        
	        return pkstatus;
	        
	    }
	}
 
 
 public void deleteSubmissionStatusFinalTab(int deletepksubmissionstatus) {
     clearHistory();
     PreparedStatement pstmt = null;
     Connection conn = null;
     try {
         conn = getConnection();
         StringBuffer sql = new StringBuffer(); 
         sql.append(" delete from er_submission_status ");
         sql.append("where PK_SUBMISSION_STATUS = ? ");
         
         pstmt = conn.prepareStatement(sql.toString());
         pstmt.setInt(1, deletepksubmissionstatus);
         ResultSet rs = pstmt.executeQuery();
         
     } catch (SQLException e) {
         Rlog.fatal("EIRBDao", "Exeception in deleteSubmissionStatusFinalTab() "+e);
     } finally {
         try {
             if (pstmt != null) pstmt.close();
         } catch (Exception e) {}
         try {
             if (conn != null) conn.close();
         } catch (Exception e) {}
     }
 }
 
 public void updateSubmissionStatusFinalTab(int updatepksubmissionstatus) {
     clearHistory();
     PreparedStatement pstmt = null;
     Connection conn = null;
     try {
         conn = getConnection();
         StringBuffer sql = new StringBuffer(); 
         sql.append(" update er_submission_status ");
         sql.append("set IS_CURRENT = 1 where PK_SUBMISSION_STATUS = ? ");
         
         pstmt = conn.prepareStatement(sql.toString());
         pstmt.setInt(1, updatepksubmissionstatus);
         ResultSet rs = pstmt.executeQuery();
         
     } catch (SQLException e) {
         Rlog.fatal("EIRBDao", "Exeception in updateSubmissionStatusFinalTab() "+e);
     } finally {
         try {
             if (pstmt != null) pstmt.close();
         } catch (Exception e) {}
         try {
             if (conn != null) conn.close();
         } catch (Exception e) {}
     }
 }
 public String getEcompSubmissionStatus(int StudyId) {
	    

	    PreparedStatement pstmt = null;
	    Connection conn = null;
	    
	    String Status = "";
	    try {
	        conn = getConnection();
	        StringBuffer sql = new StringBuffer();
	        
	       /* sql.append("select codelst_desc ");
	        sql.append(" from er_codelst  ");
	        sql.append(" where pk_codelst = (select fk_codelst_stat from er_status_history ");
	        sql.append("where pk_status = ( select max(pk_status) from er_status_history ");
	        sql.append("where status_modpk = ? and status_modtable='er_study') )");
           */
	        /*sql.append(" select cl.codelst_desc  ");
	        sql.append(" from er_codelst cl,er_status_history sh  ");
	        sql.append("  where cl.PK_CODELST = sh.FK_CODELST_STAT");
	        sql.append(" and sh.TIME_STAMP = (select max(time_stamp) from ER_STATUS_HISTORY sh1 where sh1.STATUS_MODPK=? and sh.STATUS_MODTABLE='er_study') ");
	        sql.append(" ORDER by pk_status desc ");*/
	        
	        
	        sql.append("select codelst_desc ");
	        sql.append(" from er_codelst  ");
	        sql.append(" where pk_codelst = (select fk_codelst_stat from er_status_history ");
	        sql.append("where pk_status = ( select max(pk_status) from er_status_history ");
	        sql.append("where time_stamp=(select max(time_stamp) from er_status_history where status_modpk=? and status_modtable='er_study') and status_modpk = ? and status_modtable='er_study') )");
			
	        
	        pstmt = conn.prepareStatement(sql.toString());
	        pstmt.setInt(1, StudyId);
	        pstmt.setInt(2, StudyId);
	        
	        ResultSet rs = pstmt.executeQuery();
	        if (rs.next()) {
	       	 
	        	Status = rs.getString("codelst_desc");
	       	 
	       	 return Status;
	       	 
	        }
	        
	    } catch(SQLException e) {
	        Rlog.fatal("EIRBDao", " error in getEcompSubmissionStatus() "+e);
	    } finally {
	        try {
	            if (pstmt != null) pstmt.close();
	        } catch (Exception e) {}
	        try {
	            if (conn != null) conn.close();
	        } catch (Exception e) {}
	    
	        return Status;
	    }
	}
 
 
 public void updateIscurrentForEcompStatus(int studyId) {
     PreparedStatement pstmt = null;
     Connection conn = null;
     try {
         conn = getConnection();
         StringBuffer sql = new StringBuffer(); 
         sql.append(" update er_status_history ");
         sql.append("set STATUS_ISCURRENT = 0 where STATUS_MODPK = ? and STATUS_MODTABLE = 'er_study' ");
         
         pstmt = conn.prepareStatement(sql.toString());
         pstmt.setInt(1, studyId);
         ResultSet rs = pstmt.executeQuery();
         
     } catch (SQLException e) {
         Rlog.fatal("EIRBDao", "Exeception in updateIscurrentForEcompStatus() "+e);
     } finally {
         try {
             if (pstmt != null) pstmt.close();
         } catch (Exception e) {}
         try {
             if (conn != null) conn.close();
         } catch (Exception e) {}
     }
 }
 public String getUpdateUser(int studyId) {

	     PreparedStatement pstmt = null;
	     Connection conn = null;
	     String updateuser = "";
	     try {
	    	 conn = getConnection();
	         StringBuffer sql = new StringBuffer(); 
	         sql.append("select USER_CHECKED from er_status_history where status_modpk=? and STATUS_MODTABLE='er_study' and STATUS_ISCURRENT=1");
	         pstmt = conn.prepareStatement(sql.toString());
	         pstmt.setInt(1, studyId);
	         ResultSet rs = pstmt.executeQuery();
	         if(rs.next()){
	        	 updateuser = rs.getString("USER_CHECKED");
	         }
	         
	     } catch (SQLException e) {
	         Rlog.fatal("EIRBDao", "Exeception in updateEcompStat() "+e);
	     } finally {
	         try {
	             if (pstmt != null) pstmt.close();
	         } catch (Exception e) {}
	         try {
	             if (conn != null) conn.close();
	         } catch (Exception e) {}
	     }
	     return updateuser;
	 }
 
 	//By Aakriti Maggo
 public String getUserCheckedLatestValue(int studyId, String loginUser, int fkSubmission, int fkSubmBoard){
		 PreparedStatement pstmt = null;
     Connection conn = null;
     String updateuser = "";
     String updateuserstudy = "";
     try {
    	 conn = getConnection();
         StringBuffer sql = new StringBuffer(); 
         StringBuffer sql1 = new StringBuffer(); 
         StringBuffer sql2 = new StringBuffer();
         StringBuffer sql3 = new StringBuffer();
         /*sql.append("select user_checked from er_submission_status sts , (select max(pk_submission_status) pk_submission_status from (select pk_submission,fk_study,max(sub_st.pk_submission_status) pk_submission_status," +
         			"sub_st.user_checked from er_submission sm inner join er_study study on study.pk_study=sm.fk_study inner join er_submission_status sub_st on sub_st.fk_submission=sm.pk_submission " +
         			"where sm.fk_study=? and sub_st.user_checked is not null group by pk_submission,fk_study,user_checked))aa where aa.pk_submission_status=sts.pk_submission_status");
         pstmt = conn.prepareStatement(sql.toString());
         pstmt.setInt(1, studyId);
         ResultSet rs = pstmt.executeQuery();
         if(rs.next()){
        	 updateuser = rs.getString("USER_CHECKED");
         }*/
         
         sql.append("SELECT USER_CHECKED FROM er_submission_status WHERE " +
         		"pk_submission_status=(SELECT MAX(pk_submission_status) FROM er_submission_status where   fk_submission=?" +
         		"AND fk_submission_board =? And pk_submission_status < (SELECT MAX(pk_submission_status) FROM er_submission_status " +
         		"where fk_submission=? AND fk_submission_board =?))");
         pstmt = conn.prepareStatement(sql.toString());
         pstmt.setInt(1, fkSubmission);
         pstmt.setInt(2, fkSubmBoard);
         pstmt.setInt(3, fkSubmission);
         pstmt.setInt(4, fkSubmBoard);
         ResultSet rs = pstmt.executeQuery();
         if(rs.next()){
        	 updateuser = rs.getString("USER_CHECKED");
         }
         
         if(updateuser==null){
        	 updateuser="";
         }
         
         sql2.append("select USER_CHECKED from er_status_history where status_modpk=? and STATUS_MODTABLE='er_study' and STATUS_ISCURRENT=1");
         pstmt = conn.prepareStatement(sql2.toString());
         pstmt.setInt(1, studyId);
         ResultSet rs1 = pstmt.executeQuery();
         if(rs1.next()){
        	 updateuserstudy = rs1.getString("USER_CHECKED");
         }
         	
         if(updateuserstudy==null){
        	 updateuserstudy="";
         }
         
         String rmv = Pattern.quote(loginUser);
         Pattern p = Pattern.compile("^" + rmv + "$" +     // matches only value
                                    "|^" + rmv + "," +     // matches first value + ','
                                    "|," + rmv + "$" +     // matches ',' + last value
                                    "|," + rmv + "(?=,)"); // matches ',' + middle value (+ ',')
         Matcher matcher = p.matcher(updateuserstudy);
         Matcher matcherS = p.matcher(updateuser);
         
        	 if(matcher.find() || matcher.matches()){
        	 sql1.append(" update er_submission_status ");
	         sql1.append("set USER_CHECKED = ? where pk_submission_status = (select max(pk_submission_status) from er_submission_status " +
	         		"where fk_submission in (SELECT pk_submission FROM er_submission WHERE fk_study=? )AND FK_SUBMISSION_BOARD IS NOT NULL )");
	         
	         pstmt = conn.prepareStatement(sql1.toString());
	         pstmt.setString(1, updateuserstudy);
	         pstmt.setInt(2, studyId);
	         ResultSet rs2 = pstmt.executeQuery();
        	 }
        	 if(updateuser!=null){
        		 //if(matcherS.find() || matcherS.matches()){
        			 sql3.append(" update er_submission_status ");
    		         sql3.append("set USER_CHECKED = ? where pk_submission_status = (select max(pk_submission_status) from er_submission_status " +
    		         		"where fk_submission in (SELECT pk_submission FROM er_submission WHERE fk_study=? )AND FK_SUBMISSION_BOARD IS NOT NULL )");
    		         
    		         pstmt = conn.prepareStatement(sql3.toString());
    		         pstmt.setString(1, updateuser);
    		         pstmt.setInt(2, studyId);
    		         ResultSet rs3 = pstmt.executeQuery();
        		 //}
        	 }
         
         
     } catch (SQLException e) {
         Rlog.fatal("EIRBDao", "Exeception in getUserCheckedLatestValue() "+e);
     } finally {
         try {
             if (pstmt != null) pstmt.close();
         } catch (Exception e) {}
         try {
             if (conn != null) conn.close();
         } catch (Exception e) {}
     }
     return updateuserstudy;
	}
	
	public int getMaxSubmissionStatus(int submission,int submission_board) {
	    

	    PreparedStatement pstmt = null;
	    Connection conn = null;
	    
	    int Status=0;
	    try {
	        conn = getConnection();
	        StringBuffer sql = new StringBuffer();
	        
	        sql.append("select max(submission_status) from er_submission_status where fk_submission=? and fk_submission_board=?");
	       
	        
	        pstmt = conn.prepareStatement(sql.toString());
	        pstmt.setInt(1, submission);
	        pstmt.setInt(2, submission_board);
	        
	       
	        
	        ResultSet rs = pstmt.executeQuery();
	        while (rs.next()) {
	       	 
	        	Status = rs.getInt("SUBMISSION_STATUS");
	  
	       	 
	        }
	        
	    } catch(SQLException e) {
	        Rlog.fatal("EIRBDao", " error in getMaxSubmissionStatus() "+e);
	    } finally {
	        try {
	            if (pstmt != null) pstmt.close();
	        } catch (Exception e) {}
	        try {
	            if (conn != null) conn.close();
	        } catch (Exception e) {}
	   	    }
	    return Status;
	}
	
	public void updateIscurrentForFinalOutcomeTab(int fk_submission,int fk_submission_board){
	PreparedStatement pstmt = null;
	Connection conn=null;
	int count = 0;
	
	try{
		conn = getConnection();
		StringBuffer sql = new StringBuffer(); 
        StringBuffer sql1 = new StringBuffer(); 
        sql.append("Select count(*) as pk_submission_status  from er_submission_status where fk_submission=? and fk_submission_board=? and " +
                   "submission_status in(select pk_codelst from er_codelst where codelst_type='subm_status' and codelst_custom_col1='overall,final')");
        pstmt = conn.prepareStatement(sql.toString());
        pstmt.setInt(1,fk_submission);
        pstmt.setInt(2,fk_submission_board);
        ResultSet rs = pstmt.executeQuery();
        if(rs.next()){
        	count = rs.getInt("pk_submission_status");
        }
        if(count>0){
        	sql1.append("update er_submission_status set Is_current=1  where fk_submission=? and fk_submission_board=? and " +
                   "submission_status in(select pk_codelst from er_codelst where codelst_type='subm_status' and codelst_custom_col1='overall,final')");
        	pstmt = conn.prepareStatement(sql1.toString());
        	pstmt.setInt(1,fk_submission);
            pstmt.setInt(2,fk_submission_board);
        	ResultSet rs1 = pstmt.executeQuery();
        }
	}
		
	catch (SQLException e) {
        Rlog.fatal("EIRBDao", "Exeception in updateIscurrentForFinalOutcomeTab() "+e);
    } finally {
        try {
            if (pstmt != null) pstmt.close();
        } catch (Exception e) {}
        try {
            if (conn != null) conn.close();
        } catch (Exception e) {}
    }
	}
	public int getIRBFormCount(int studyId ,String lf_submissiontype){
		 PreparedStatement pstmt = null;
	     Connection conn = null;
	     int getCount=0;
	     try {
	    	 conn = getConnection();
	         StringBuffer sql = new StringBuffer(); 
	         StringBuffer sql1 = new StringBuffer(); 
	         sql.append("select count(*) as FormCount from er_formslinear " +
	         		"where FK_FORM = (select fk_formlib from er_linkedforms " +
	         		"where lf_submission_type=?) and id =? " +
	         		"and form_completed = (select pk_codelst from er_codelst where codelst_type='fillformstat'and codelst_subtyp='complete')");
	         
	         pstmt = conn.prepareStatement(sql.toString());
	         pstmt.setString(1,lf_submissiontype);
	         pstmt.setInt(2, studyId);
	         ResultSet rs = pstmt.executeQuery();
	         if (rs.next()) {
	        	 getCount = rs.getInt("FormCount");
	            }
	         
	         
	     } catch (SQLException e) {
	         Rlog.fatal("EIRBDao", "Exeception in getIRBFormCount() "+e);
	     } finally {
	         try {
	             if (pstmt != null) pstmt.close();
	         } catch (Exception e) {}
	         try {
	             if (conn != null) conn.close();
	         } catch (Exception e) {}
	     }
	     return getCount;	
	}
	
	public void getIRBFormName(int studyid,String formname){
		 PreparedStatement pstmt = null;
	     Connection conn = null;
	     try {
	    	 conn = getConnection();
	         StringBuffer sql = new StringBuffer(); 
	         sql.append("SELECT pk_studyforms,sf.fk_formlib,lbvr.pk_formlibver FROM er_studyforms sf,er_formlib flb,er_formlibver lbvr WHERE sf.fk_study =? AND flb.pk_formlib =sf.fk_formlib and flb.pk_formlib=lbvr.fk_formlib and flb.form_name=?");
	         pstmt = conn.prepareStatement(sql.toString());
	         pstmt.setInt(1, studyid);
	         pstmt.setString(2, formname);
	        
	         ResultSet rs = pstmt.executeQuery();
	         if(rs.next()){
	        	 setFkfilledformlibsumstat(rs.getInt("pk_studyforms"));
	        	 setFkformlibsumstat(rs.getInt("fk_formlib"));
	        	 setFormLibVer(rs.getInt("pk_formlibver"));
	         }    
	         
	     } catch (SQLException e) {
	         Rlog.fatal("EIRBDao", "Exeception in getIRBFormName() "+e);
	     } finally {
	         try {
	             if (pstmt != null) pstmt.close();
	         } catch (Exception e) {}
	         try {
	             if (conn != null) conn.close();
	         } catch (Exception e) {}
	     }
	}
	
	//By Aakriti Maggo(need to check)
 	public void getEnteredFormsLatestValue(int fk_submission,int fk_submissionboard){
 		 PreparedStatement pstmt = null;
	     Connection conn = null;
	     try {
	    	 conn = getConnection();
	         StringBuffer sql = new StringBuffer(); 
	         StringBuffer sql1 = new StringBuffer(); 
	         sql.append("select FORM_NAME,FORM_SEQ,FK_FORMLIB,FK_FILLEDFORM,FK_FORMLIBVER from er_submission_status " +
	         			" where pk_submission_status = (select max(pk_submission_status) from er_submission_status where fk_submission=? and fk_submission_board=?) " );
	         pstmt = conn.prepareStatement(sql.toString());
	         pstmt.setInt(1, fk_submission);
	         pstmt.setInt(2, fk_submissionboard);
	         ResultSet rs = pstmt.executeQuery();
	         while(rs.next()){
	        	 setIrbFormName(rs.getString("FORM_NAME"));
	        	 setIrbFormSeq(rs.getString("FORM_SEQ"));
	        	 setFkformlibsumstat(rs.getInt("FK_FORMLIB"));
	        	 setFkfilledformlibsumstat(rs.getInt("FK_FILLEDFORM"));
	        	 setFormLibVer(rs.getInt("FK_FORMLIBVER"));
	         }
	         
	        /* if(!updateuser.equals("")){
	        	 sql1.append(" update er_submission_status ");
		         sql1.append("set USER_CHECKED = ? where fk_submission in (select pk_submission from er_submission where fk_study=?) and FK_SUBMISSION_BOARD is not null ");
		         
		         pstmt = conn.prepareStatement(sql1.toString());
		         pstmt.setString(1, updateuser);
		         pstmt.setInt(2, studyId);
		         ResultSet rs2 = pstmt.executeQuery();
	         }*/
	         
	     } catch (SQLException e) {
	         Rlog.fatal("EIRBDao", "Exeception in getEnteredFormsLatestValue() "+e);
	     } finally {
	         try {
	             if (pstmt != null) pstmt.close();
	         } catch (Exception e) {}
	         try {
	             if (conn != null) conn.close();
	         } catch (Exception e) {}
	     }
 	}
 	 public String getUserChecked(int fk_study) {
 	    

 	    PreparedStatement pstmt = null;
 	    Connection conn = null;
 	    
 	    String USER_CHECKED = "";
 	    try {
 	        conn = getConnection();
 	        StringBuffer sql = new StringBuffer();
 	       
 	        sql.append("select user_checked from er_status_history ");
 	        sql.append(" where pk_status=(select max(pk_status) from er_status_history where  status_modpk=?  and status_modtable='er_study' )");
 	        
 	        pstmt = conn.prepareStatement(sql.toString());
 	        pstmt.setInt(1, fk_study);
 	        
 	        ResultSet rs = pstmt.executeQuery();
 	        if(rs.next()) {
 	       	 
 	        	USER_CHECKED = rs.getString("USER_CHECKED");
 	       	  	        }
 	        
 	    } catch(SQLException e) {
 	        Rlog.fatal("EIRBDao", " error in getUserChecked() "+e);
 	    } finally {
 	        try {
 	            if (pstmt != null) pstmt.close();
 	        } catch (Exception e) {}
 	        try {
 	            if (conn != null) conn.close();
 	        } catch (Exception e) {}
 	    
 	        return USER_CHECKED;
 	    }
 	}

 // end of class
}