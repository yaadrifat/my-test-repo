/*
 * Classname : ObjectSettingsDao
 *
 * Version information: 1.0
 *
 * Date: 05/04/2009
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Jnanamay Majumdar
 */

package com.velos.eres.business.common;

/* IMPORT STATEMENTS */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */
public class ObjectSettingsDao extends CommonDAO implements
        java.io.Serializable {

    private ArrayList pkObjSetting;
    private ArrayList objType;
    private ArrayList objSubType;
    private ArrayList objName;
    private ArrayList objSequence;
    private ArrayList objVisible;
    private ArrayList objDispTxt;
    private ArrayList objAcc;
    private ArrayList objUrl;

    public ObjectSettingsDao () {
        pkObjSetting =  new ArrayList();
        objType = new ArrayList();
        objSubType = new ArrayList();
        objName = new ArrayList();
        objSequence = new ArrayList();
        objVisible = new ArrayList();
        objDispTxt = new ArrayList();
        objAcc = new ArrayList();
        objUrl=new ArrayList();
    }

    //Then the getter and setter methods goes here
    public ArrayList getPkObjSetting() {
        return this.pkObjSetting;
    }
    public void setPkObjSetting(String pkObjSetting) {
        this.pkObjSetting.add(pkObjSetting);
    }

    public ArrayList getObjType() {
        return this.objType;
    }
    public void setObjType(String objType) {
        this.objType.add(objType);
    }

    public ArrayList getObjSubType() {
        return this.objSubType;
    }
    public void setObjSubType(String objSubType) {
        this.objSubType.add(objSubType);
    }

    public ArrayList getObjName() {
        return this.objName;
    }
    public void setObjName(String objName) {
        this.objName.add(objName);
    }

    public ArrayList getObjSequence() {
        return this.objSequence;
    }
    public void setObjSequence(String objSequence) {
        this.objSequence.add(objSequence);
    }

    public ArrayList getObjVisible() {
        return this.objVisible;
    }
    public void setObjVisible(String objVisible) {
        this.objVisible.add(objVisible);
    }

    public ArrayList getObjDispTxt() {
        return this.objDispTxt;
    }
    public void setObjDispTxt(String objDispTxt) {
        this.objDispTxt.add(objDispTxt);
    }

    public ArrayList getObjAcc() {
        return this.objAcc;
    }
    public void setObjAcc(String objAcc) {
        this.objAcc.add(objAcc);
    }
    public ArrayList getObjUrl() {
        return this.objUrl;
    }
    public void setObjUrl(String objUrl) {
        this.objUrl.add(objUrl);
    }
    //end of getter and setter methods

    public void getObjectSettings(){

        PreparedStatement pstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("select PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, OBJECT_VISIBLE,"
                    + " OBJECT_DISPLAYTEXT, fk_account,object_url from ER_OBJECT_SETTINGS order by FK_ACCOUNT, OBJECT_NAME, OBJECT_SEQUENCE");

            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("Objectsettings", "ObjectSettingsDao.getObjectSettings... ");

            while (rs.next()) {

                setPkObjSetting(rs.getString("PK_OBJECT_SETTINGS"));
                setObjType(rs.getString("OBJECT_TYPE"));
                setObjSubType(rs.getString("OBJECT_SUBTYPE"));
                setObjName(rs.getString("OBJECT_NAME"));
                setObjSequence(rs.getString("OBJECT_SEQUENCE"));
                setObjVisible(rs.getString("OBJECT_VISIBLE"));
                setObjDispTxt(rs.getString("OBJECT_DISPLAYTEXT"));
                setObjAcc(rs.getString("fk_account"));
                setObjUrl(rs.getString("object_url"));

            }
        }catch(SQLException ex){
            Rlog.fatal(
                    "Objectsettings",
                    "ObjectSettingsDao.getObjectSettings EXCEPTION IN FETCHING FROM ER_OBJECT_SETTINGS table.."
                    + ex);

        }finally {
            try{
                if (pstmt != null)
                    pstmt.close();
            }catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }

}