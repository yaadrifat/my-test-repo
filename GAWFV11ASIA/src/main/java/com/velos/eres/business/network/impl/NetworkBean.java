package com.velos.eres.business.network.impl;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

@Entity
@Table(name = "er_nwsites")
public class NetworkBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private Integer networkId;
	private Integer networkSiteId;
	private Integer siteId;
	private Integer networkLevel;
	private Integer creator;
	private Integer modifiedBy;
	private String ipAdd;
	private Integer networkTypeId;
	private Integer networkMainId;
	private Integer networkStatusId;
	
	@Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_NWSITES", allocationSize=1)
    @Column(name = "PK_NWSITES")
	public Integer getNetworkId() {
		return networkId;
	}
	public void setNetworkId(Integer networkId) {
		this.networkId = networkId;
	}
	
	@Column(name = "FK_NWSITES")
	public String getNetworkSiteId() {
		return StringUtil.integerToString(networkSiteId);
	}
	public void setNetworkSiteId(String networkSiteId) {
		this.networkSiteId = StringUtil.stringToInteger(networkSiteId);
	}
	
	@Column(name = "FK_SITE")
	public String getSiteId() {
		return StringUtil.integerToString(siteId);
	}
	public void setSiteId(String siteId) {
		this.siteId = StringUtil.stringToInteger(siteId);
	}
	
	@Column(name = "NW_LEVEL")
	public String getNetworkLevel() {
		return StringUtil.integerToString(networkLevel);
	}
	public void setNetworkLevel(String networkLevel) {
		this.networkLevel = StringUtil.stringToInteger(networkLevel);
	}
	
	@Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null && modifiedBy.trim().length() > 0) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }

    }

    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    public void setCreator(String creator) {
        if (creator != null && creator.trim().length() > 0) {
            this.creator = Integer.valueOf(creator);
        }

    }
	
	@Column(name = "NW_MEMBERTYPE")
	public String getNetworkTypeId() {
		return StringUtil.integerToString(networkTypeId);
	}
	public void setNetworkTypeId(String networkTypeId) {
		this.networkTypeId = StringUtil.stringToInteger(networkTypeId);
	}
	
	@Column(name = "FK_NWSITES_MAIN")
	public String getNetworkMainId() {
		return StringUtil.integerToString(networkMainId);
	}
	public void setNetworkMainId(String networkMainId) {
		this.networkMainId = StringUtil.stringToInteger(networkMainId);
	}
	
	@Column(name = "NW_STATUS")
	public String getNetworkStatusId() {
		return StringUtil.integerToString(networkStatusId);
	}
	public void setNetworkStatusId(String networkStatusId) {
		this.networkStatusId = StringUtil.stringToInteger(networkStatusId);
	}
	
	public int updateNetwork(NetworkBean nwk) {
        try {
        	setNetworkId(nwk.getNetworkId());
        	setNetworkSiteId(nwk.getNetworkSiteId());
        	setSiteId(nwk.getSiteId());
        	setNetworkLevel(nwk.getNetworkLevel());
        	setIpAdd(nwk.getIpAdd());
        	setCreator(nwk.getCreator());
        	setModifiedBy(nwk.getModifiedBy());
        	setNetworkMainId(nwk.getNetworkMainId());
        	setNetworkTypeId(nwk.getNetworkTypeId());
            setNetworkStatusId(nwk.getNetworkStatusId());

            return 0;
        } catch (Exception e) {
            Rlog.fatal("network", " error in NetworkBean.updateNetwork : " + e);
            return -2;
        }
    }
	public NetworkBean() {
		
	}
	public NetworkBean(Integer networkId, String networkSiteId, String siteId, String networkLevel,String creator,String modifiedBy, 
			String ipAdd, String networkTypeId, String networkMainId, String networkStatusId) {
		this.networkId = networkId;
		setNetworkSiteId(networkSiteId);
		setSiteId(siteId);
		setNetworkLevel(networkLevel);
		setIpAdd(ipAdd);
		setModifiedBy(modifiedBy);
		setCreator(creator);
		setNetworkTypeId(networkTypeId);
		setNetworkMainId(networkMainId);
		setNetworkStatusId(networkStatusId);
	}
		
}
