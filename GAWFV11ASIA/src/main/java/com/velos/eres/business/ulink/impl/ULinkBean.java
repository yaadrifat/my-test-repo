/*	
 * Classname			ULinkBean.class
 * 
 * Version information
 *
 * Date					03/15/2001
 * 
 * Copyright notice
 */

package com.velos.eres.business.ulink.impl;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * The User CMP entity bean.<br>
 * <br>
 * 
 * @author Dinesh
 */

@Entity
@Table(name = "ER_USR_LNKS")
public class ULinkBean implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 3905240113373327669L;

    /**
     * the Link Id
     */
    public int lnkId;

    /**
     * the link User id
     */
    public Integer lnkUserId;

    /**
     * the link Account ID
     */
    public Integer lnkAccId;

    /**
     * the Link URI
     */
    public String lnkURI;

    /**
     * the Link Description
     */
    public String lnkDesc;

    /**
     * the Link Group Name, so that links can be put under user defined headings
     */
    public String lnkGrpName;

    /*
     * the record creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * the IP Address
     */
    public String ipAdd;

    public Integer lnkType;

    // GETTER SETTER METHODS

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_USR_LNKS", allocationSize=1)
    @Column(name = "PK_USR_LNKS")
    public int getLnkId() {
        return this.lnkId;
    }

    public void setLnkId(int lnkId) {
        this.lnkId = lnkId;
    }

    @Column(name = "FK_ACCOUNT")
    public String getLnkAccId() {
        return StringUtil.integerToString(this.lnkAccId);
    }

    public void setLnkAccId(String lnkAccId) {
        if (lnkAccId != null) {
            this.lnkAccId = Integer.valueOf(lnkAccId);
        }
    }

    @Column(name = "FK_USER")
    public String getLnkUserId() {
        return StringUtil.integerToString(this.lnkUserId);
    }

    public void setLnkUserId(String lnkUserId) {
        if (lnkUserId != null) {
            this.lnkUserId = Integer.valueOf(lnkUserId);
        }
    }

    @Column(name = "LINK_DESC")
    public String getLnkDesc() {
        return this.lnkDesc;
    }

    public void setLnkDesc(String lnkDesc) {
        this.lnkDesc = lnkDesc;
    }

    @Column(name = "LINK_GRPNAME")
    public String getLnkGrpName() {
        return this.lnkGrpName;
    }

    public void setLnkGrpName(String lnkGrpName) {
        this.lnkGrpName = lnkGrpName;
    }

    @Column(name = "LINK_URI")
    public String getLnkURI() {
        return this.lnkURI;
    }

    public void setLnkURI(String lnkURI) {
        this.lnkURI = lnkURI;
    }

    /**
     * @return The Id of the creator of the record
     */
    @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    /**
     * @return The Id of the user modifying this record
     */
    @Column(name = "LAST_MODIFIED_BY")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    /**
     * @return IP Address
     */
    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    @Column(name = "FK_CODELST_LNKTYPE")
    public String getLnkType() {
        return StringUtil.integerToString(this.lnkType);

    }

    public void setLnkType(String lnkType) {
        if (lnkType != null) {
            this.lnkType = Integer.valueOf(lnkType);
        }
    }

    // END OF GETTER SETTER METHODS

    /**
     * returns the state holder object associated with this user link
     */
    /*
     * public ULinkStateKeeper getULinkStateKeeper() { Rlog.debug("ulink",
     * "ULinkBean.getULjinkStateKeeper"); return new
     * ULinkStateKeeper(getLnkId(), getLnkAccId(), getLnkUserId(), getLnkURI(),
     * getLnkDesc(), getLnkGrpName(), getCreator(), getModifiedBy(), getIpAdd(),
     * getLnkType()); }
     */

    /**
     * sets up a state keeper containing details of the user link
     */
    /*
     * public void setULinkStateKeeper(ULinkStateKeeper lnk) { GenerateId
     * ulinkId = null;
     * 
     * try { Connection conn = null; conn = getConnection(); Rlog.debug("user",
     * "ULinkBean.setULinkStateKeeper() Connection :" + conn); lnkId =
     * ulinkId.getId("SEQ_ER_USR_LNKS", conn); conn.close();
     * 
     * Rlog.debug("ulink", "value of lnkid is"); //
     * Rlog.debug("ulink",Integer.toString(lnkId)); Rlog.debug("ulink", "value
     * of link account id is" + lnk.getLnkAccId()); // setLnkId(lnk.getLnkId());
     * setLnkAccId(lnk.getLnkAccId()); setLnkUserId(lnk.getLnkUserId());
     * setLnkDesc(lnk.getLnkDesc()); setLnkGrpName(lnk.getLnkGrpName());
     * setLnkURI(lnk.getLnkURI()); setCreator(lnk.getCreator());
     * setModifiedBy(lnk.getModifiedBy()); setIpAdd(lnk.getIpAdd());
     * setLnkType(lnk.getLnkType()); Rlog.debug("ulink",
     * "ULinkBean.setULinkStateKeeper() ulinkId :" + lnkId); } catch (Exception
     * e) { System.out.println("Error in setULinkStateKeeper() in ULinkBean " +
     * e); } }
     */

    public int updateULink(ULinkBean lnk) {

        try {
            setLnkAccId(lnk.getLnkAccId());
            setLnkUserId(lnk.getLnkUserId());
            setLnkDesc(lnk.getLnkDesc());
            setLnkGrpName(lnk.getLnkGrpName());
            setLnkURI(lnk.getLnkURI());
            setCreator(lnk.getCreator());
            setModifiedBy(lnk.getModifiedBy());
            setIpAdd(lnk.getIpAdd());
            setLnkType(lnk.getLnkType());
            Rlog.debug("ulink", "ULinkBean.update userlink");
            return 0;
        } catch (Exception e) {
            Rlog.fatal("ulink", " error ULinkBean.update userlink");
            return -2;
        }

    }

    public ULinkBean() {

    }

    public ULinkBean(int lnkId, String lnkUserId, String lnkAccId,
            String lnkURI, String lnkDesc, String lnkGrpName, String creator,
            String modifiedBy, String ipAdd, String lnkType) {
        super();
        // TODO Auto-generated constructor stub
        setLnkId(lnkId);
        setLnkUserId(lnkUserId);
        setLnkAccId(lnkAccId);
        setLnkURI(lnkURI);
        setLnkDesc(lnkDesc);
        setLnkGrpName(lnkGrpName);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setLnkType(lnkType);
    }
}// end of class
