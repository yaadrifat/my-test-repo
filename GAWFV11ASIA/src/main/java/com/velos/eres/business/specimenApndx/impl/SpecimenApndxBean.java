package com.velos.eres.business.specimenApndx.impl;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

@Entity
@Table(name = "ER_SPECIMEN_APPNDX")
public class SpecimenApndxBean implements Serializable {

    /**
     * 
     */
	
    private static final long serialVersionUID = 3617574920440133687L;

    public int specApndxId;

    public Integer fkSpecimen;
    
    public String specApndxDesc;

    public String specApndxUri;

    public String specApndxType;

    public Integer creator;

    public Integer modifiedBy;

    public String ipAdd;
    
    public byte[] specApndxFileObj ;
    
     

    // getter/setter methods
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "eres.SEQ_SPEC_APNDX", allocationSize=1)
    @Column(name = "PK_SPECIMEN_APPNDX")
    public int getSpecApndxId() {
        return this.specApndxId;
    }

    public void setSpecApndxId(int specApndxId) {
        this.specApndxId = specApndxId;
    }

    @Column(name = "FK_SPECIMEN")
    public String getFkSpecimen() {
    	 
        return StringUtil.integerToString(this.fkSpecimen);
    }

    public void setFkSpecimen(String fkSpecimen) {
    	 
        this.fkSpecimen = StringUtil.stringToInteger(fkSpecimen);
         
    }

  
    @Column(name = "SA_DESC")
    public String getSpecApndxDesc() {
        return this.specApndxDesc;
    }

    public void setSpecApndxDesc(String specApndxDesc) {
        this.specApndxDesc = specApndxDesc;
    }

    @Column(name = "SA_TYPE")
    public String getSpecApndxType() {
        return this.specApndxType;
    }

    public void setSpecApndxType(String specApndxType) {
        this.specApndxType = specApndxType;
    }

    @Column(name = "SA_URI")
    public String getSpecApndxUri() {
        return this.specApndxUri;
    }
    
    
    public void setSpecApndxUri(String specApndxUri) {
        this.specApndxUri = specApndxUri;
    }

    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    public void setCreator(String creator) {
    	if (! StringUtil.isEmpty(creator)){
    	  this.creator = Integer.valueOf(creator);
    	}
    	else
    	{
    		this.creator = null;
    		
    	}
    }

    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    public void setModifiedBy(String modifiedBy) {
    	
    	if (! StringUtil.isEmpty(modifiedBy))
    	{
    		this.modifiedBy = Integer.valueOf(modifiedBy);
    	}
    	else
    	{
    		this.modifiedBy = null;
    		
    	}
    }

    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

 
    public int updateSpecimenApndx(SpecimenApndxBean sask) {
        try {
        	         	
        	setFkSpecimen(sask.getFkSpecimen());
        	    
           	setSpecApndxDesc(sask.getSpecApndxDesc());
           	setSpecApndxUri(sask.getSpecApndxUri());
           	 
            setSpecApndxType(sask.getSpecApndxType());
            setCreator(sask.getCreator());
            setModifiedBy(sask.getModifiedBy());
            setIpAdd(sask.getIpAdd());
            
        } catch (Exception e) {
            Rlog.fatal("specapndx", "exception in updateSpecApndx" + e);
            return -1;
        }
        return 0;
    }

    public SpecimenApndxBean() {

    }

    public SpecimenApndxBean(int specApndxId,String fkSpecimen, 
            String specApndxDesc, String specApndxUri, String specApndxType,
            String creator, String modifiedBy, String ipAdd) {	
          	super();
    
        // TODO Auto-generated constructor stub
          	
           	
        this.setSpecApndxId(specApndxId);
        this.setFkSpecimen(fkSpecimen);
         
        this.setSpecApndxDesc(specApndxDesc);
        this.setSpecApndxUri(specApndxUri);
        this.setSpecApndxType(specApndxType);
        this.setCreator(creator);
        this.setModifiedBy(modifiedBy);
        this.setIpAdd(ipAdd);
    }
    
}    

