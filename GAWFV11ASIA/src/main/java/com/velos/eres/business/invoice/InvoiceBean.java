/*
 * Classname			PatFacilityBean
 *
 * Version information : 1.0
 *
 * Date					09/22/2005
 *
 * Copyright notice : Velos Inc
 */

package com.velos.eres.business.invoice;

/* IMPORT STATEMENTS */

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * The Invoice POJO.<br>
 * <br>
 *
 * @author Sonia Abrol
 *
 * @version 1.0, 10/18/2005
 */
@Entity
@Table(name = "er_invoice")

public class InvoiceBean implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -2827130787292895587L;

	/**
	 * the Invoice Primary Key
	 */
	public int id;

	/**
	 * the Invoice Number
	 */
	public String invNumber;

	/**
	 * the Invoice addressed to
	 */
	public String invAddressedto;

	/**
	 * the Invoice sent from
	 */
	public String invSentFrom;

	/**
	 * the invoice header
	 */
	public String invHeader;

	/**
	 * the invoice footer
	 */
	public String invFooter;

	/**
	 * Milestone rule status type
	 */
	public String invMileStatusType;

	/**
	 * the invoice interval type
	 */
	public String invIntervalType;

	/**
	 * Invoice interval from
	 */
	public Date invIntervalFrom;

	/**
	 * Invoice interval To
	 */
	public Date invIntervalTo;

	/**
	 * Invoice Date
	 */
	public Date invDate;

	/**
	 * the invoice Notes
	 */
	public String invNotes ;

	/**
	 * the invoice Other user
	 */
	public String invOtherUser;


	/**
	 * the id of the user who created the record
	 */
	public String creator;

	/**
	 * Id of the user who last modified the record
	 */
	public String lastModifiedBy;

	/**
	 * the IP address of the user who created or modified the record
	 */
	public String iPAdd;

	public String invPayDueBy;
	
	public Date invPayDueDate;
	
	private String covTyp;

	public String invPayUnit;

	public String study;

	public String site;

	public String int_acc_num;//JM:

	public String invStat; //INV_STATUS

	//Commented by Manimaran for FIN12 requirement Change.
	
	//public String includeHeader; //KM

	//public String includeFooter;

	//	 GETTER SETTER METHODS
	@Column(name = "FK_SITE")
	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	// GETTER SETTER METHODS
	@Column(name = "FK_STUDY")
	public String getStudy() {
		return study;
	}

	public void setStudy(String study) {
		this.study = study;
	}


    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
	@SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_INVOICE", allocationSize=1)
	@Column(name = "PK_INVOICE")
	public int getId() {
		return this.id;
	}

	public void setId(int Id) {
		this.id =Id;
	}

	@Column(name = "CREATOR")
	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	@Column(name = "inv_addressedto")
	public String getInvAddressedto() {
		return invAddressedto;
	}

	public void setInvAddressedto(String invAddressedto) {
		this.invAddressedto = invAddressedto;
	}

	@Column(name = "inv_date")
	public Date getInvDate() {
		return invDate;
	}

	public void setInvDate(Date invDate) {
		this.invDate = invDate;
	}
	
	@Column(name = "inv_payment_duedate")
	public Date getInvPayDueDate() {
		return invPayDueDate;
	}

	public void setInvPayDueDate(Date invPayDueDate) {
		this.invPayDueDate = invPayDueDate;
	}

	@Column(name = "inv_footer")
	public String getInvFooter() {
		return invFooter;
	}

	public void setInvFooter(String invFooter) {
		this.invFooter = invFooter;
	}

	@Column(name = "inv_header")
	public String getInvHeader() {
		return invHeader;
	}

	public void setInvHeader(String invHeader) {
		this.invHeader = invHeader;
	}

	@Column(name = "inv_intervalfrom")
	public Date getInvIntervalFrom() {
		return invIntervalFrom;
	}

	public void setInvIntervalFrom(Date invIntervalFrom) {
		this.invIntervalFrom = invIntervalFrom;
	}

	@Column(name = "inv_intervalto")
	public Date getInvIntervalTo() {
		return invIntervalTo;
	}

	public void setInvIntervalTo(Date invIntervalTo) {
		this.invIntervalTo = invIntervalTo;
	}

	@Column(name = "inv_intervaltype")
	public String getInvIntervalType() {
		return invIntervalType;
	}

	public void setInvIntervalType(String invIntervalType) {
		this.invIntervalType = invIntervalType;
	}

	@Column(name = "inv_milestatustype")
	public String getInvMileStatusType() {
		return invMileStatusType;
	}

	public void setInvMileStatusType(String invMileStatusType) {
		this.invMileStatusType = invMileStatusType;
	}

	@Column(name = "inv_notes")
	public String getInvNotes() {
		return invNotes;
	}

	public void setInvNotes(String invNotes) {
		this.invNotes = invNotes;
	}

	@Column(name = "inv_number")
	public String getInvNumber() {
		return invNumber;
	}

	public void setInvNumber(String invNumber) {
		this.invNumber = invNumber;
	}

	@Column(name = "inv_other_user")
	public String getInvOtherUser() {
		return invOtherUser;
	}

	public void setInvOtherUser(String invOtherUser) {
		this.invOtherUser = invOtherUser;
	}

	@Column(name = "inv_sentFrom")
	public String getInvSentFrom() {
		return invSentFrom;
	}

	public void setInvSentFrom(String invSentFrom) {
		this.invSentFrom = invSentFrom;
	}

	@Column(name = "ip_add")
	public String getIPAdd() {
		return iPAdd;
	}

	public void setIPAdd(String add) {
		iPAdd = add;
	}

	@Column(name = "LAST_MODIFIED_BY")

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}


	@Column(name = "INV_PAYMENT_DUEBY")
	public String getInvPayDueBy() {
		return invPayDueBy;
	}

	public void setInvPayDueBy(String invPayDueBy) {
		this.invPayDueBy = invPayDueBy;
	}

	@Column(name = "INV_PAYUNIT")
	public String getInvPayUnit() {
		return invPayUnit;
	}

	public void setInvPayUnit(String invPayUnit) {
		this.invPayUnit = invPayUnit;
	}

//	 GETTER SETTER METHODS
	@Column(name = "INT_ACC_NUM ")
	public String getIntnlAccNum() {
		return int_acc_num;
	}

	public void setIntnlAccNum(String intAccNum) {
		this.int_acc_num = intAccNum;
	}
	
	/**
	 * @param covTyp the covTyp to set
	 */
	public void setCovTyp(String covTyp) {
		this.covTyp = covTyp;
	}

	/**
	 * @return the covTyp
	 */
	@Column(name = "COVERAGE_TYPE_CRITERIA")
	public String getCovTyp() {
		return covTyp;
	}
	

	//Added by Manimaran for Enh#FIN12
	/*@Column(name = "INV_INCLUDE_HEADER")
	public String getIncludeHeader() {
		return includeHeader;
	}

	public void setIncludeHeader(String includeHeader) {
		this.includeHeader = includeHeader;
	}

	@Column(name = "INV_INCLUDE_FOOTER")
	public String getIncludeFooter() {
		return includeFooter;
	}

	public void setIncludeFooter(String includeFooter) {
		this.includeFooter = includeFooter;
	}
	*/


	@Column(name = "INV_STATUS ")
	public String getInvStat() {
		return invStat;
	}

	public void setInvStat(String invStat) {
		this.invStat = invStat;
	}

	// END OF GETTER SETTER METHODS

	/**
	 * Updates the attributes of current insatance with the attributes of the
	 * one passed as argument
	 *
	 * @param iBean
	 *            InvoiceBean with the Invoice Information
	 *
	 * @return 0 - if update is successful <br>
	 *         -2 - if update fails
	 *
	 */
	public int updateInvoice(InvoiceBean iBean) {
		try {

				setInvAddressedto(iBean.getInvAddressedto());
				setInvDate(iBean.getInvDate());
				setInvFooter(iBean.getInvFooter()) ;
				setInvHeader(iBean.getInvHeader()) ;
				setInvIntervalFrom(iBean.getInvIntervalFrom()) ;
				setInvIntervalTo(iBean.getInvIntervalTo());
				setInvIntervalType(iBean.getInvIntervalType()) ;
				setInvMileStatusType(iBean.getInvMileStatusType()) ;
				setInvNotes(iBean.getInvNotes());
				setInvNumber(iBean.getInvNumber()) ;
				setInvOtherUser(iBean.getInvOtherUser()) ;
				setInvSentFrom(iBean.getInvSentFrom()) ;
				setIPAdd(iBean.getIPAdd()) ;
				setLastModifiedBy(iBean.getLastModifiedBy()) ;
				setCreator(iBean.getCreator());
				setInvPayDueBy(iBean.getInvPayDueBy());
				setInvPayUnit(iBean.getInvPayUnit());
				setStudy(iBean.getStudy());
				setSite(iBean.getSite());
				setIntnlAccNum(iBean.getIntnlAccNum());//JM:
				setInvStat(iBean.getInvStat());
				setCovTyp(iBean.getCovTyp());
				setInvPayDueDate(iBean.getInvPayDueDate());
				//setIncludeHeader(iBean.getIncludeHeader());//KM
				//setIncludeFooter(iBean.getIncludeFooter());
				Rlog.debug("invoice", "InvoiceBean.updateInvoice");
				return 0;
		} catch (Exception e) {
			Rlog.fatal("PatFacility",
					"Exception in PatFacilityBean.updatPatFacility " + e);
			return -2;
		}
	}


	/** Default constructor */

	public InvoiceBean()
	{

	}

	/** Full Argument constructor */
	public InvoiceBean(String creator, int id, String addressedto, Date invDate, String footer, String header, Date invIntervalFrom, Date invIntervalTo, String invIntervalType, String invMileStatusType, String notes, String invNumber, String invOtherUser, String invSentFrom, String iPadd, String lastModifiedBy,
		String invPayDueBy,String invPayUnit, String study, String site, String intAccNum, String invStat,Date invPayDueDate ) {

		super();
		// TODO Auto-generated constructor stub
		this.creator = creator;
		this.id = id;
		this.invAddressedto = addressedto;
		this.invDate = invDate;
		this.invFooter = footer;
		this.invHeader = header;
		this.invIntervalFrom = invIntervalFrom;
		this.invIntervalTo = invIntervalTo;
		this.invIntervalType = invIntervalType;
		this.invMileStatusType = invMileStatusType;
		this.invNotes = notes;
		this.invNumber = invNumber;
		this.invOtherUser = invOtherUser;
		this.invSentFrom = invSentFrom;
		this.iPAdd =  iPadd;
		this.lastModifiedBy = lastModifiedBy;
		setInvPayDueBy(invPayDueBy);
		setInvPayUnit(invPayUnit);
		setStudy(study);
		setSite(site);
		setIntnlAccNum(intAccNum);//JM:
		setInvStat(invStat);
		this.invPayDueDate=invPayDueDate;
		
	}
	
	public InvoiceBean(String creator, int id, String addressedto, Date invDate, String footer, String header, Date invIntervalFrom, Date invIntervalTo, String invIntervalType, String invMileStatusType, String notes, String invNumber, String invOtherUser, String invSentFrom, String iPadd, String lastModifiedBy,
			String invPayDueBy,String invPayUnit, String study, String site, String intAccNum, String invStat,String covType ,Date invPayDueDate) {

			super();
			// TODO Auto-generated constructor stub
			this.creator = creator;
			this.id = id;
			this.invAddressedto = addressedto;
			this.invDate = invDate;
			this.invFooter = footer;
			this.invHeader = header;
			this.invIntervalFrom = invIntervalFrom;
			this.invIntervalTo = invIntervalTo;
			this.invIntervalType = invIntervalType;
			this.invMileStatusType = invMileStatusType;
			this.invNotes = notes;
			this.invNumber = invNumber;
			this.invOtherUser = invOtherUser;
			this.invSentFrom = invSentFrom;
			this.iPAdd =  iPadd;
			this.lastModifiedBy = lastModifiedBy;
			setInvPayDueBy(invPayDueBy);
			setInvPayUnit(invPayUnit);
			setStudy(study);
			setSite(site);
			setIntnlAccNum(intAccNum);//JM:
			setInvStat(invStat);
			setCovTyp(covType);
			this.invPayDueDate=invPayDueDate;
			
		}
	
}// end of class
