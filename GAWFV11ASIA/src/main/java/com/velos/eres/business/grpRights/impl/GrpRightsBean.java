/*
 * Classname			GrpRightsBean.class
 * 
 * Version information	1.0
 *
 * Date					03/05/2001
 * 
 * Copyright notice    Velos Inc.
 */

package com.velos.eres.business.grpRights.impl;

import java.io.Serializable;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * The Rights CMP entity bean.<br>
 * <br>
 * 
 * @author Sonia
 * @version 1.0
 * @ejbHome GrpRightsHome
 * @ejbRemote GrpRightsRObj
 */
@Entity
@Table(name = "er_GRPS")
public class GrpRightsBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3545803186591183922L;

    /**
     * the primary key of the Group Rights
     */
    public int id;

    /**
     * the Group id
     */
    public Integer grpId;

    /**
     * Rights of the group
     */
    public String grpRights;

    /*
     * the record creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * the IP Address
     */
    public String ipAdd;

    /**
     * Array List of Feature Sequence
     */
    private ArrayList grSeq;

    /**
     * the Array List of Feature Rights
     */
    private ArrayList ftrRights;

    /**
     * the Array List containing reference name of the feature
     */
    private ArrayList grValue;

    /**
     * the Array List containing user friendly name of the feature
     */
    private ArrayList grDesc;

    // GETTER SETTER METHODS

    /**
     * 
     * 
     * @return id of the group
     */

    @Id
    @Column(name = "PK_grp")
    public int getId() {
        return this.id;
    }

    public void setId(int grpId) {
        this.id = grpId;
    }

    // NO SETTER METHOD FOR PK REQUIRED

    /**
     * 
     * 
     * @return group id of a group
     */
    @Transient
    public String getGrpId() {
        return StringUtil.integerToString(this.grpId);
    }

    /**
     * 
     * 
     * @param grpId
     *            id of the group
     */
    public void setGrpId(String grp) {
        if (grp != null) {
            this.grpId = Integer.valueOf(grp);
        }
    }

    /**
     * 
     * 
     * @return Feature wise Rights for the group
     */
    @Column(name = "GRP_RIGHTS")
    public String getGrpRights() {
        return this.grpRights;
    }

    /**
     * 
     * 
     * @param rights
     *            Feature rights for the group
     */
    public void setGrpRights(String rights) {
        this.grpRights = rights;
    }

    /**
     * @return The Id of the creator of the record
     */
    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    /**
     * @return The Id of the user modifying this record
     */
    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    /**
     * @return IP Address
     */
    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
     * 
     * 
     * @return
     */
    @Transient
    public ArrayList getGrSeq() {
        return this.grSeq;
    }

    /**
     * 
     * 
     * @param seq
     */
    public void setGrSeq(String seq) {
        grSeq.add(seq);
    }

    /**
     * 
     * 
     * @param seq
     */
    public void setGrSeq(ArrayList seq) {
        this.grSeq = seq;
    }

    /**
     * 
     * 
     * @return
     */
    @Transient
    public ArrayList getFtrRights() {
        return this.ftrRights;
    }

    /**
     * Sets the Rights
     * 
     * @param rights
     */
    public void setFtrRights(String rights) {
        Rlog.debug("grprights", "Setting the Rights in State Keeper" + rights);
        try {
            this.ftrRights.add(rights);
        } catch (Exception e) {
            Rlog.fatal("grprights", "EXCEPTION IN String Value Set"
                    + this.ftrRights + "*" + e);
        }
        Rlog.debug("grprights", "String Value Set" + this.ftrRights);
    }

    /**
     * 
     * 
     * @param rights
     */
    public void setFtrRights(ArrayList rights) {
        Rlog.debug("grprights",
                "Setting the Rights in State Keeper (Array List)" + rights);
        this.ftrRights = rights;
        Rlog.debug("grprights", "Array List Value Set" + this.ftrRights);
    }

    /**
     * 
     * 
     * @return
     */
    @Transient
    public ArrayList getGrValue() {
        return this.grValue;
    }

    /**
     * 
     * 
     * @param val
     */
    public void setGrValue(String val) {
        grValue.add(val);
    }

    /**
     * 
     * 
     * @param val
     */

    public void setGrValue(ArrayList val) {
        this.grValue = val;
    }

    /**
     * 
     * 
     * @return
     */
    @Transient
    public ArrayList getGrDesc() {
        return this.grDesc;
    }

    /**
     * 
     * 
     * @param desc
     */
    public void setGrDesc(String desc) {
        grDesc.add(desc);
    }

    /**
     * 
     * 
     * @param desc
     */
    public void setGrDesc(ArrayList desc) {
        this.grDesc = desc;
    }

    // END OF GETTER SETTER METHODS

    /**
     * 
     * 
     * @return state holder object associated with this Group Rights
     */
    @Transient
    /*
     * public GrpRightsBean getGrpRightsStateKeeper() { Rlog.debug("grprights",
     * "GET GROUP RIGHTS KEEPER"); GrpRightsBean gsk = new GrpRightsBean();
     * gsk.setId(getId()); gsk.setCreator(getCreator());
     * gsk.setModifiedBy(getModifiedBy()); gsk.setIpAdd(getIpAdd()); return gsk; }
     */
    /**
     * Inserts the Group Rights in the Database
     * 
     * @param gsk
     *            State Kepper of Group Rights containing the rights of the
     *            group
     * @return 0 when updation is successful and -2 when some exception occurs
     */
    /*
     * public int setGrpRightsStateKeeper(GrpRightsBean gsk) {
     * Rlog.debug("grprights", "IN Group Rights STATE HOLDER"); try { id =
     * gsk.getId(); // Both id and Group id will be similar
     * setGrpId(Integer.toString(gsk.getId())); // All other attributes // are
     * set in Session // Bean setCreator(gsk.getCreator());
     * setModifiedBy(gsk.getModifiedBy()); setIpAdd(gsk.getIpAdd());
     * Rlog.debug("grprights", "GOT Group ID" + id); return 0; } catch
     * (Exception e) { Rlog.fatal("grprights", "EXCEPTION IN ASSOCIATING RIGHTS
     * TO GROUPS" + e); return -2; } }
     */
    /**
     * Updates the Group Rights in the Database
     * 
     * @param gsk
     *            State Kepper of Group Rights containing the rights of the
     *            group
     * @return 0 when updation is successful and -2 when some exception occurs
     */
    public int updateGrpRights(GrpRightsBean gsk, String rights) {
        Rlog.debug("grprights", "IN Group Rights STATE HOLDER");
        try {
            // setGrpId(Integer.toString(gsk.getId())); //All other attributes
            // are set in Session Bean
            setGrpRights(rights);
            setCreator(gsk.getCreator());
            setModifiedBy(gsk.getModifiedBy());
            setIpAdd(gsk.getIpAdd());
            Rlog.debug("grprights", "UPDATED");
            return 0;
        } catch (Exception e) {
            Rlog.fatal("grprights", "EXCEPTION IN ASSOCIATING RIGHTS TO GROUPS"
                    + e);
            return -2;
        }
    }

    public GrpRightsBean() {
        grSeq = new ArrayList();
        ftrRights = new ArrayList();
        grValue = new ArrayList();
        grDesc = new ArrayList();
    }

    public GrpRightsBean(int id, String grpId, ArrayList grSeqList,
            ArrayList ftrRightList, ArrayList grValueList,
            ArrayList grDescList, String creator, String modifiedBy,
            String ipAdd) {
        super();
        // TODO Auto-generated constructor stub
        grSeq = new ArrayList();
        ftrRights = new ArrayList();
        grValue = new ArrayList();
        grDesc = new ArrayList();
        setId(id);
        setGrpId(grpId);
        // setGrpRights(grpRights);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        this.setFtrRights(ftrRightList);
        this.setGrSeq(grSeqList);
        this.setGrValue(grValueList);
        this.setGrDesc(grDescList);
    }

}// end of class

