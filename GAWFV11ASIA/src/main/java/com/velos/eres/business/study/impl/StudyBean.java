/*
 * Classname			StudyBean.class
 *
 * Version information
 *
 * Date					02/17/2001
 *
 * Author 			Sonia Sahni, Sajal
 * Copyright notice
 */

package com.velos.eres.business.study.impl;

/**
 * @ejbHomeJNDIname ejb.Study
 */

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * The Study CMP entity bean.<br>
 * <br>
 *
 * @author
 */
@Entity
@Table(name = "er_study")
@NamedQueries ({
@NamedQuery(name = "findStudyNum", query = "SELECT OBJECT(study) FROM StudyBean study where fk_account = :accountId  AND UPPER(trim(STUDY_NUMBER)) = UPPER(trim(:studyNum))"),
@NamedQuery(name = "findAccountNum", query = "SELECT OBJECT(study) FROM StudyBean study where pk_study = :studyNum))")
})
/*
 * @NamedQueries( { @NamedQuery(name = "findByUser", queryString = "SELECT
 * OBJECT(study) FROM StudyBean study where exists " + " (select 1 from
 * er_studyteam where study.id = er_studyteam.studyId and
 * er_studyteam.teamUser=:userId)"), @NamedQuery(name = "findStudyNum",
 * queryString = "SELECT OBJECT(study) FROM StudyBean study where fk_account =
 * :accountId AND UPPER(trim(STUDY_NUMBER)) = UPPER(trim(:studyNum))") })
 */
public class StudyBean implements Serializable {
	/**
     *
     */
    private static final long serialVersionUID = 4120856546814538035L;

    /**
     * the primary key of the study
     */
    public Integer id;

    /**
     * the Account
     */
    public Integer account;

    /**
     * the Study Sponsor
     */
    public String studySponsor;

    /**
     * the Study Contact
     */
    public String studyContact;

    /**
     * the Study Public Flag
     */

    public String studyPubFlag;

    /**
     * the Study Title
     */

    public String studyTitle = "";

    /**
     * the Study Objective
     */
    public String studyObjective = "";

    /**
     * the Study Summary
     */
    public String studySummary = "";

    /**
     * the Study Product
     */
    public String studyProduct = "";

    /**
     * the Study Size
     */
    public Integer studySize;

    /**
     * the Study Duration
     */
    public Integer studyDuration;

    /**
     * the Study Duration Unit
     */
    public String studyDurationUnit = "";

    /**
     * the Study Estimated Begin date
     */
    public java.util.Date studyEstBeginDate;

    /**
     * the Study Actual Begin Date
     */
    public java.util.Date studyActBeginDate;

    /**
     * the Study End Date
     */
    public java.util.Date studyEndDate;

    /**
     * the Study Principal Investigator
     */
    public String studyPrimInv = "";

    /**
     * the Study Coordinator
     */
    public String studyCoordinator = "";

    /**
     * the Study Principal Investigator
     */
    public String studyPartCenters = "";

    /**
     * the Study keywords
     */
    public String studyKeywords = "";

    /**
     * the Study Public Columns
     */
    public String studyPubCol = "";

    /**
     * the Study Parent Study ID
     */
    public Integer studyParent;

    /**
     * the Study Author
     */
    public Integer studyAuthor;

    /**
     * the Study Therapeutic Area
     */
    public Integer studyTArea;

    /**
     * the Study Research Type
     */
    public Integer studyResType;

    /**
     * the Study Purpose
     */
    public Integer studyPurpose;

    /**
     * the Study TYpe
     */
    public Integer studyType;

    /**
     * the Study Blinding
     */
    public Integer studyBlinding;

    /**
     * the Study Randomization
     */
    public Integer studyRandom;

    // Added by gopu for september2006 enhancement #S7
    /**
     * the Study Sponsor Name
     */
    public Integer studySponsorName;

    /**
     *  the Study Sponsor Id Information
     */
    public String studySponsorIdInfo;

    /**
     * the Study Number
     */
    public String studyNumber = "";

    /**
     * the Study Phase
     */
    public Integer studyPhase;

    /**
     * the Study Version
     */
    public String studyVersion;

    /**
     * the Study Current
     */
    public String studyCurrent;

    /**
     * the Study Info
     */
    public String studyInfo;

    /**
     * the PI major auhtor flag
     */
    public String majAuthor;

    /**
     * the Disease site(s)
     */
    public String disSite;

    /**
     * the scope of study-Codelst
     */
    public Integer studyScope;

    /**
     * the scope of study-linked to another study
     */
    public String studyAssoc;

    /*
     * the record creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * the IP Address
     */
    public String ipAdd;

    /**
     * the Parent Version Study ID
     */
    public Integer studyVersionParent;

    /**
     * the study ICD-9 code1
     */
    public String studyICDCode1;

    /**
     * the study ICD-9 code2
     */
    public String studyICDCode2;

    /**
     * the study ICD-9 code3
     */
    public String studyICDCode3;
    
    /**
     * study creation type
     */
    public String studyCreationType;

    /**
     * the Parent Version Study ID
     */
    public Integer studyCurrency;

    /**
     * the Study Asccociated lookup version
     */
    public Integer studyAdvlkpVer;

    public Integer studyNSamplSize;

    public String studyOtherPrinv;

    /**
     * the Study Division
     */
    public Integer studyDivision;

    /**
     * Flag for PI as major author
     */
    public Integer studyInvIndIdeFlag;

    /**
     * IND/IDE #"
     */
    public String studyIndIdeNum;
    /*YK 29Mar2011 -DFIN20 */
    /**
     * Study Milestone status"
     */
    public Integer milestoneSetStatus;
    /**
     * Study CTRP Reportable
     * CTRP-20527 22-Nov-2011 Ankit
     */
    public Integer studyCtrpReportable;
    /**
     * INF-22247 27-Feb-2012 Ankit
     */
    public Integer fdaRegulatedStudy;
 
    /**
     * Study CCSG Reportable
     * CCSG DT 4 Report
     */
    public Integer studyCcsgReportable;
    
    /**
     * CTRP-22471 Raviesh
     * 
     * the Study NCI Trial Identifier
     */
    public String nciTrialIdentifier;
    
    /**
     * CTRP-22471 Raviesh
     * 
     * the Study NCT Number
     */
    public String nctNumber;
    
    /**
     * CTRP-22471 Raviesh
     * 
     * the Study CTRP Accrual Last Run Date
     */
    public String ctrpAccrualLastRunDate;

	/**
	 * CTRP-22470 Yogendra
	 * Last updated date when study accrual
	 * Zip file is downloaded from 
	 * CTRP ACCRUAL Browser's Download button 
	 */
    public java.util.Date ctrpAccrualLastGenerationDate;
    /**
     * CTRP-22498 Yogesh
     * Date till when the patient data is downloaded.
     * Zip file is downloaded from 
     * CTRP ACCRUAL Browser's Download button 
     */
    public java.util.Date cutOffDate;
    /**
     * CTRP-22498 Yogesh
     * Last Download By the patient data .
     * Zip file is downloaded from 
     * CTRP ACCRUAL Browser's Download button 
     */
    public Integer lastDownloadBy;
    
    private Integer studyIsLocked;
    
    private Integer studyIsBlocked;

	public StudyBean() {
    }

    public StudyBean(Integer id, String account, String studySponsor,
            String studyContact, String studyPubFlag, String studyTitle,
            //String studyObjective, String studySummary,
            String studyProduct,
            String studySize, String studyDuration, String studyDurationUnit,
            String studyEstBeginDate, String studyActBeginDate,
            String studyEndDate, String studyPrimInv, String studyCoordinator,
            String studyPartCenters, String studyKeywords, String studyPubCol,
            String studyParent, String studyAuthor, String studyTArea,
            String studyResType,String studyPurpose, String studyType, String studyBlinding,
            String studyRandom, String studyNumber, String studyPhase,
            String studyVersion, String studyCurrent, String studyInfo,
            String majAuthor, String disSite, String studyScope,
            String studyAssoc, String creator, String modifiedBy, String ipAdd,
            String studyVersionParent, String studyICDCode1,
            String studyICDCode2, String studyICDCode3, String studyCurrency,
            String studyAdvlkpVer, String studyNSamplSize,
            String studyOtherPrinv, String studyDivision,
            String studyInvIndIdeFlag, String studyIndIdeNum, String studySponsorName, 
            String studySponsorIdInfo, String studyCreationType,String milestoneSetStatus,
            String studyCtrpReportable,String fdaRegulatedStudy, String studyCcsgReportable,
            String nciTrialIdentifier,String nctNumber,String ctrpAccrualLastRunDate, 
            String ctrpAccrualLastGenerationDate, String cutOffDate, String lastDownloadBy,
            String studyIsLocked, String studyIsBlocked) {

        setId(id);
        setAccount(account);
        setStudySponsor(studySponsor);
        setStudyContact(studyContact);
        setStudyPubFlag(studyPubFlag);
        setStudyTitle(studyTitle);
        //setStudyObjective(studyObjective);
        //setStudySummary(studySummary);
        setStudyProduct(studyProduct);
        setStudySize(studySize);
        setStudyDuration(studyDuration);
        setStudyDurationUnit(studyDurationUnit);
        setStudyEstBeginDate(studyEstBeginDate);
        setStudyActBeginDate(studyActBeginDate);
        setStudyEndDate(studyEndDate);
        setStudyPrimInv(studyPrimInv);
        setStudyCoordinator(studyCoordinator);
        setStudyPartCenters(studyPartCenters);
        setStudyKeywords(studyKeywords);
        setStudyPubCol(studyPubCol);
        setStudyParent(studyParent);
        setStudyAuthor(studyAuthor);
        setStudyTArea(studyTArea);
        setStudyResType(studyResType);
        setStudyPurpose(studyPurpose);
        setStudyType(studyType);
        setStudyBlinding(studyBlinding);
        setStudyRandom(studyRandom);
        setStudyNumber(studyNumber);
        setStudyPhase(studyPhase);
        setStudyVersion(studyVersion);
        setStudyCurrent(studyCurrent);
        setStudyInfo(studyInfo);
        setMajAuthor(majAuthor);
        setDisSite(disSite);
        setStudyScope(studyScope);
        setStudyAssoc(studyAssoc);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setStudyVersionParent(studyVersionParent);
        setStudyICDCode1(studyICDCode1);
        setStudyICDCode2(studyICDCode2);
        setStudyICDCode3(studyICDCode3);
        setStudyCurrency(studyCurrency);
        setStudyAdvlkpVer(studyAdvlkpVer);
        setStudyNSamplSize(studyNSamplSize);
        setStudyOtherPrinv(studyOtherPrinv);
        setStudyDivision(studyDivision);
        setStudyInvIndIdeFlag(studyInvIndIdeFlag);
        setStudyIndIdeNum(studyIndIdeNum);
        setStudySponsorName(studySponsorName);
        setStudySponsorIdInfo(studySponsorIdInfo);
        setStudyCreationType(studyCreationType);
        setMilestoneSetStatus(milestoneSetStatus);
        setStudyCtrpReportable(studyCtrpReportable);
        setFdaRegulatedStudy(fdaRegulatedStudy);
        setCcsgReportableStudy(studyCcsgReportable);
        setNciTrialIdentifier(nciTrialIdentifier);
        setNctNumber(nctNumber);
        setCtrpAccrualLastRunDate(ctrpAccrualLastRunDate);
        setCtrpAccrualLastGenerationDate(ctrpAccrualLastGenerationDate);
        setCutOffDate(cutOffDate);
        setLastDownloadBy(lastDownloadBy);
        setStudyIsLocked(studyIsLocked);
        setStudyIsBlocked(studyIsBlocked);
    }

    // GETTER SETTER METHODS
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_STUDY", allocationSize=1)
    @Column(name = "PK_STUDY")
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "FK_ACCOUNT")
    public String getAccount() {
        return StringUtil.integerToString(this.account);
    }

    /**
     *
     *
     * @param account
     */
    public void setAccount(String account) {
        this.account = StringUtil.stringToInteger(account);
    }

    @Column(name = "STUDY_ACTUALDT")
    public Date getStudyActBeginDt() {
        return this.studyActBeginDate;

    }

    public void setStudyActBeginDt(Date studyActBeginDate) {
        this.studyActBeginDate = studyActBeginDate;
    }

    @Transient
    public String getStudyActBeginDate() {
        return DateUtil.dateToString(getStudyActBeginDt());

    }

    public void setStudyActBeginDate(String studyActBeginDate) {
        setStudyActBeginDt(DateUtil.stringToDate(studyActBeginDate,
                null));
    }
   
	@Column(name = "CTRP_ACCRUAL_GEN_DATE")
	public Date getCtrpAccrualLastGenerationDt() {
		return this.ctrpAccrualLastGenerationDate;
	}

	/**
	 * @param ctrpLastGenerationDate the ctrpLastGenerationDate to set
	 */
	public void setCtrpAccrualLastGenerationDt(Date ctrpAccrualLastGenerationDate) {
		this.ctrpAccrualLastGenerationDate = ctrpAccrualLastGenerationDate;
	}
	
	@Transient
    public String getCtrpAccrualLastGenerationDate() {
        return DateUtil.dateToString(getCtrpAccrualLastGenerationDt());
    }

    public void setCtrpAccrualLastGenerationDate(String ctrpAccrualLastGenerationDate) {
    	setCtrpAccrualLastGenerationDt(DateUtil.stringToDate(ctrpAccrualLastGenerationDate,
                null));
    }
    
    @Column(name = "CTRP_CUTOFF_DATE")
    public Date getCutOffDt() {
    	return this.cutOffDate;
    }
    
    /**
     * @param cutOffDate the cutOffDate to set
     */
    public void setCutOffDt(Date cutOffDate) {
    	this.cutOffDate = cutOffDate;
    }
    
    @Transient
    public String getCutOffDate() {
    	return DateUtil.dateToString(getCutOffDt());
    }
    
    public void setCutOffDate(String cutOffDate) {
    	setCutOffDt(DateUtil.stringToDate(cutOffDate,
    			null));
    }
    
    
    @Column(name = "CTRP_LAST_DOWNLOAD_BY")
    public String getLastDownloadBy() {
    	return StringUtil.integerToString(this.lastDownloadBy);
    }
    
    /**
     * @param lastDownloadBy the lastDownloadBy to set
     */
    public void setLastDownloadBy(String lastDownloadBy) {
    	this.lastDownloadBy = StringUtil.stringToInteger(lastDownloadBy);
    }
    

    @Column(name = "STUDY_END_DATE")
    public Date getStudyEndDt() {
        return this.studyEndDate;
    }

    public void setStudyEndDt(Date studyEndDate) {

        this.studyEndDate = studyEndDate;
    }

    @Transient
    public String getStudyEndDate() {
        String eDate = "";

        if (this.studyEndDate != null) {
            eDate = DateUtil.dateToString(getStudyEndDt());
        }

        return eDate;
    }

    public void setStudyEndDate(String studyEndDate) {
        if (studyEndDate != null) {
            setStudyEndDt(DateUtil.stringToDate(studyEndDate, null));
        }
    }

    @Column(name = "FK_AUTHOR")
    public String getStudyAuthor() {
        return StringUtil.integerToString(this.studyAuthor);
    }

    public void setStudyAuthor(String studyAuthor) {
        this.studyAuthor = StringUtil.stringToInteger(studyAuthor);
    }

    @Column(name = "FK_CODELST_BLIND")
    public String getStudyBlinding() {
        return StringUtil.integerToString(this.studyBlinding);
    }

    public void setStudyBlinding(String studyBlinding) {

        this.studyBlinding = StringUtil.stringToInteger(studyBlinding);
    }

    @Column(name = "STUDY_CONTACT")
    public String getStudyContact() {
        return this.studyContact;
    }

    public void setStudyContact(String studyContact) {
        this.studyContact = studyContact;
    }

    @Column(name = "STUDY_DUR")
    public String getStudyDuration() {
        return StringUtil.integerToString(this.studyDuration);
    }

    public void setStudyDuration(String studyDuration) {

        this.studyDuration = StringUtil.stringToInteger(studyDuration);
    }

    @Column(name = "STUDY_DURUNIT")
    public String getStudyDurationUnit() {
        return this.studyDurationUnit;
    }

    public void setStudyDurationUnit(String studyDurationUnit) {
        this.studyDurationUnit = studyDurationUnit;
    }

    @Column(name = "STUDY_ESTBEGINDT")
    public Date getStudyEstBeginDt() {

        return this.studyEstBeginDate;

    }

    public void setStudyEstBeginDt(Date studyEstBeginDate) {

        this.studyEstBeginDate = studyEstBeginDate;
    }

    @Transient
    public String getStudyEstBeginDate() {

        return DateUtil.dateToString(getStudyEstBeginDt());
    }

    public void setStudyEstBeginDate(String studyEstBeginDate) {

        setStudyEstBeginDt(DateUtil.stringToDate(studyEstBeginDate,
                null));
    }

    /**
     * Get Study Keywords *
     *
     * @return String
     */
    @Column(name = "STUDY_KEYWRDS")
    public String getStudyKeywords() {
        return this.studyKeywords;
    }

    public void setStudyKeywords(String studyKeywords) {
        this.studyKeywords = studyKeywords;
    }

    @Column(name = "STUDY_NUMBER")
    public String getStudyNumber() {
        return this.studyNumber;
    }

    public void setStudyNumber(String studyNumber) {
        this.studyNumber = studyNumber;
    }

    //@Column(name = "STUDY_OBJ")
    @Transient
    public String getStudyObjective() {
        return this.studyObjective;
    }

    public void setStudyObjective(String studyObjective) {
        this.studyObjective = studyObjective;
    }

    @Column(name = "STUDY_PARENTID")
    public String getStudyParent() {
        return StringUtil.integerToString(this.studyParent);
    }

    public void setStudyParent(String studyParent) {

        this.studyParent = StringUtil.stringToInteger(studyParent);
    }

    @Column(name = "STUDY_PARTCNTR")
    public String getStudyPartCenters() {
        return this.studyPartCenters;
    }

    public void setStudyPartCenters(String studyPartCenters) {
        this.studyPartCenters = studyPartCenters;
    }

    @Column(name = "STUDY_PRINV")
    public String getStudyPrimInv() {
        return this.studyPrimInv;
    }

    public void setStudyPrimInv(String studyPrimInv) {
        this.studyPrimInv = studyPrimInv;
    }

    /**
     * Returns the value of studyCoordinator.
     */
    @Column(name = "STUDY_COORDINATOR")
    public String getStudyCoordinator() {
        return studyCoordinator;
    }

    /**
     * Sets the value of studyCoordinator.
     *
     * @param studyCoordinator
     *            The value to assign studyCoordinator.
     */
    public void setStudyCoordinator(String studyCoordinator) {
        this.studyCoordinator = studyCoordinator;
    }

    @Column(name = "STUDY_PRODNAME")
    public String getStudyProduct() {
        return this.studyProduct;
    }

    public void setStudyProduct(String studyProduct) {
        this.studyProduct = studyProduct;
    }

    @Column(name = "STUDY_PUBCOLLST")
    public String getStudyPubCol() {
        return this.studyPubCol;
    }

    public void setStudyPubCol(String studyPubCol) {
        this.studyPubCol = studyPubCol;
    }

    @Column(name = "STUDY_PUBFLAG")
    public String getStudyPubFlag() {
        return this.studyPubFlag;
    }

    public void setStudyPubFlag(String studyPubFlag) {
        this.studyPubFlag = studyPubFlag;
    }

    @Column(name = "FK_CODELST_RANDOM")
    public String getStudyRandom() {
        return StringUtil.integerToString(this.studyRandom);
    }

    public void setStudyRandom(String studyRandom) {
        this.studyRandom = StringUtil.stringToInteger(studyRandom);
    }

    @Column(name = "FK_CODELST_RESTYPE")
    public String getStudyResType() {
        return StringUtil.integerToString(this.studyResType);
    }

    public void setStudyResType(String studyResType) {

        this.studyResType = StringUtil.stringToInteger(studyResType);
    }

    @Column(name = "FK_CODELST_PURPOSE")
    public String getStudyPurpose() {
    	return StringUtil.integerToString(this.studyPurpose);
    }
    
    public void setStudyPurpose(String studyPurpose) {
    	
    	this.studyPurpose = StringUtil.stringToInteger(studyPurpose);
    }

    @Column(name = "STUDY_SAMPLSIZE")
    public String getStudySize() {
        return StringUtil.integerToString(this.studySize);
    }

    public void setStudySize(String studySize) {

        this.studySize = StringUtil.stringToInteger(studySize);
    }

    @Column(name = "STUDY_SPONSOR")
    public String getStudySponsor() {
        return this.studySponsor;
    }

    public void setStudySponsor(String studySponsor) {
        this.studySponsor = studySponsor;
    }

    // Added by Gopu for September 2006 Enhancement #S7
    @Column(name = "FK_CODELST_SPONSOR")
    public String getStudySponsorName() {
    	return StringUtil.integerToString(this.studySponsorName);
    }

    public void setStudySponsorName(String studySponsorName) {
    	 this.studySponsorName = StringUtil.stringToInteger(studySponsorName);
    }

    @Column(name = "STUDY_SPONSORID")
    public String getStudySponsorIdInfo() {
        return this.studySponsorIdInfo;
    }

    public void setStudySponsorIdInfo(String studySponsorIdInfo) {
        this.studySponsorIdInfo = studySponsorIdInfo;
    }


    //////

    //@Column(name = "STUDY_SUM") //JM: blocked
    @Transient
    public String getStudySummary() {
        return this.studySummary;
    }

    public void setStudySummary(String studySummary) {
        this.studySummary = studySummary;
    }

    @Column(name = "FK_CODELST_TAREA")
    public String getStudyTArea() {
        return StringUtil.integerToString(this.studyTArea);
    }

    public void setStudyTArea(String studyTArea) {

        this.studyTArea = StringUtil.stringToInteger(studyTArea);
    }

    /**
     *
     *
     * @return
     */
    @Column(name = "STUDY_TITLE")
    public String getStudyTitle() {
        return this.studyTitle;
    }

    public void setStudyTitle(String studyTitle) {
        this.studyTitle = studyTitle;
    }

    @Column(name = "FK_CODELST_TYPE")
    public String getStudyType() {
        return StringUtil.integerToString(this.studyType);
    }

    public void setStudyType(String studyType) {

        this.studyType = StringUtil.stringToInteger(studyType);
    }

    @Column(name = "FK_CODELST_PHASE")
    public String getStudyPhase() {
        return StringUtil.integerToString(this.studyPhase);
    }

    public void setStudyPhase(String studyPhase) {

        this.studyPhase = StringUtil.stringToInteger(studyPhase);
    }

    @Column(name = "STUDY_VER_NO")
    public String getStudyVersion() {
        return this.studyVersion;
    }

    public void setStudyVersion(String studyVersion) {
        this.studyVersion = studyVersion;
    }

    @Transient
    public String getStudyCurrent() {
        return this.studyCurrent;
    }

    public void setStudyCurrent(String studyCurrent) {
        this.studyCurrent = studyCurrent;
    }

    @Column(name = "STUDY_INFO")
    public String getStudyInfo() {
        Rlog.debug("study", "IN StudyBean.getStudyInfo studyInfo "
                + this.studyInfo);
        return this.studyInfo;
    }

    public void setStudyInfo(String studyInfo) {
        this.studyInfo = studyInfo;
        Rlog.debug("study", "IN StudyBean.setStudyInfo studyInfo "
                + this.studyInfo);
    }

    @Column(name = "STUDY_MAJ_AUTH")
    public String getMajAuthor() {
        return majAuthor;
    }

    public void setMajAuthor(String majAuthor) {
        this.majAuthor = majAuthor;
    }

    @Column(name = "STUDY_DISEASE_SITE")
    public String getDisSite() {
        return disSite;
    }

    public void setDisSite(String disSite) {
        this.disSite = disSite;
    }

    @Column(name = "FK_CODELST_SCOPE")
    public String getStudyScope() {
        return StringUtil.integerToString(this.studyScope);

    }

    public void setStudyScope(String studyScope) {

        this.studyScope = StringUtil.stringToInteger(studyScope);
    }

    @Column(name = "STUDY_ASSOC")
    public String getStudyAssoc() {
        return studyAssoc;
    }

    public void setStudyAssoc(String studyAssoc) {

        this.studyAssoc = studyAssoc;

    }

    /**
     * @return The Id of the creator of the record
     */
    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {

        this.creator = StringUtil.stringToInteger(creator);
    }

    /**
     * @return The Id of the user modifying this record
     */
    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
    public void setModifiedBy(String modifiedBy) {

        this.modifiedBy = StringUtil.stringToInteger(modifiedBy);
    }

    /**
     * @return IP Address
     */
    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    @Column(name = "STUDY_VERPARENT")
    public String getStudyVersionParent() {
        return StringUtil.integerToString(this.studyVersionParent);
    }

    public void setStudyVersionParent(String studyVersionParent) {

        this.studyVersionParent = StringUtil.stringToInteger(studyVersionParent);
    }

    @Column(name = "FK_CODELST_CURRENCY")
    public String getStudyCurrency() {
        return StringUtil.integerToString(this.studyCurrency);
    }

    public void setStudyCurrency(String studyCurrency) {

        this.studyCurrency = StringUtil.stringToInteger(studyCurrency);
    }

    @Column(name = "STUDY_ADVLKP_VER")
    public String getStudyAdvlkpVer() {
        return StringUtil.integerToString(studyAdvlkpVer);
    }

    public void setStudyAdvlkpVer(String studyAdvlkpVer) {
        if (!StringUtil.isEmpty(studyAdvlkpVer)) {
            this.studyAdvlkpVer = StringUtil.stringToInteger(studyAdvlkpVer);
        } else {
            //this.studyAdvlkpVer = null;
        	//JM: 19Aug2008, #3782
            this.studyAdvlkpVer = 0;
        }
    }

    @Column(name = "STUDY_NSAMPLSIZE")
    public String getStudyNSamplSize() {
        return StringUtil.integerToString(this.studyNSamplSize);
    }

    public void setStudyNSamplSize(String studyNSamplSize) {
        this.studyNSamplSize = StringUtil.stringToInteger(studyNSamplSize);
    }

    @Column(name = "STUDY_OTHERPRINV")
    public String getStudyOtherPrinv() {
        return studyOtherPrinv;
    }

    public void setStudyOtherPrinv(String studyOtherPrinv) {
        this.studyOtherPrinv = studyOtherPrinv;
    }

    /**
     * Get Study Division
     *
     * @return String
     */
    @Column(name = "STUDY_DIVISION")
    public String getStudyDivision() {
        return StringUtil.integerToString(this.studyDivision);
    }

    /**
     * sets study division
     *
     * @param studyDivision
     */
    public void setStudyDivision(String studyDivision) {
        this.studyDivision = StringUtil.stringToInteger(studyDivision);

    }

    /**
     * Get Study IND/IDE flag
     *
     * @return String
     */
    @Column(name = "STUDY_INVIND_FLAG")
    public String getStudyInvIndIdeFlag() {

        return StringUtil.integerToString(this.studyInvIndIdeFlag);
    }

    /**
     * sets Study investigator flag
     *
     * @param studyInvIndIdeFlag
     */
    public void setStudyInvIndIdeFlag(String studyInvIndIdeFlag) {
        this.studyInvIndIdeFlag = StringUtil.stringToInteger(studyInvIndIdeFlag);
    }

    /**
     * Get Study IND/IDE #
     *
     * @return String
     */
    @Column(name = "STUDY_INVIND_NUMBER")
    public String getStudyIndIdeNum() {
        return this.studyIndIdeNum;
    }

    /**
     * sets IND/IDE #
     *
     * @param studyIndIdeNum
     */
    public void setStudyIndIdeNum(String studyIndIdeNum) {
        this.studyIndIdeNum = studyIndIdeNum;
    }

    /**
     * Get Study ICE-9 code1 #
     *
     * @return String
     */
    @Column(name = "STUDY_ICDCODE1")
    public String getStudyICDCode1() {
        return this.studyICDCode1;
    }

    /**
     * sets Study ICD-9 Code1
     *
     * @param studyICDCode1
     */

    public void setStudyICDCode1(String studyICDCode1) {
        this.studyICDCode1 = studyICDCode1;
    }

    /**
     * Get Study ICE-9 code2 #
     *
     * @return String
     */
    @Column(name = "STUDY_ICDCODE2")
    public String getStudyICDCode2() {
        return this.studyICDCode2;
    }

    /**
     * sets Study ICD-9 Code2
     *
     * @param studyICDCode2
     */

    public void setStudyICDCode2(String studyICDCode2) {
        this.studyICDCode2 = studyICDCode2;
    }

    /**
     * Get Study ICE-9 code3 #
     *
     * @return String
     */
    @Column(name = "STUDY_ICDCODE3")
    public String getStudyICDCode3() {
        return this.studyICDCode3;
    }

    /**
     * sets Study ICD-9 Code3
     *
     * @param studyICDCode3
     */

    public void setStudyICDCode3(String studyICDCode3) {
        this.studyICDCode3 = studyICDCode3;
    }

    /**
     * sets Study CreationType
     *
     * @returns studyCreationType
     */
    @Column(name = "STUDY_CREATION_TYPE")
    public String getStudyCreationType() {
        return this.studyCreationType;
    }

    /**
     * sets Study CreationType
     *
     * @param studyCreationType
     */

    public void setStudyCreationType(String studyCreationType) {
        this.studyCreationType = studyCreationType;
    }
    
    /*YK 29Mar2011 -DFIN20 */
    /**
     * Sets Study Milestone Status
     *
     * @returns milestoneSetStatus
     */
    @Column(name = "FK_CODELST_SET_MILESTATUS")
     public String getMilestoneSetStatus() {
    	return StringUtil.integerToString(this.milestoneSetStatus);
    }
    /**
     * Gets Study Milestone Status
     *
     * @returns milestoneSetStatus
     */
    public void setMilestoneSetStatus(String milestoneSetStatus) {
    	 this.milestoneSetStatus = StringUtil.stringToInteger(milestoneSetStatus);
    }

    /**
     * Gets STUDY CTRP REPORTABLE
     * @returns milestoneSetStatus
	 * CTRP-20527 22-Nov-2011 Ankit
     */
    @Column(name = "STUDY_CTRP_REPORTABLE")
     public String getStudyCtrpReportable() {
    	return StringUtil.integerToString(this.studyCtrpReportable);
    }
    /**
     * Set STUDY CTRP REPORTABLE
     */
    public void setStudyCtrpReportable(String studyCtrpReportable) {
    	 this.studyCtrpReportable = StringUtil.stringToInteger(studyCtrpReportable);
    }
    
    /**
     * Get FDA_REGULATED_STUDY
     * @returns FDA_REGULATED_STUDY
	 * INF-22247 27-Feb-2012 Ankit
     */
    @Column(name = "FDA_REGULATED_STUDY")
     public String getFdaRegulatedStudy() {
    	return StringUtil.integerToString(this.fdaRegulatedStudy);
    }

    /**
     * Set FDA_REGULATED_STUDY
     */
    public void setFdaRegulatedStudy(String fdaRegulatedStudy) {
    	 this.fdaRegulatedStudy = StringUtil.stringToInteger(fdaRegulatedStudy);
    }
    
    /**
     * Get CCSG_REPORTABLE_STUDY
     * @returns CCSG_REPORTABLE_STUDY
     */
    @Column(name = "CCSG_REPORTABLE_STUDY")
    public String getCcsgReportableStudy() {
    	return StringUtil.integerToString(this.studyCcsgReportable);
    }
    
    /**
     * Set CCSG_REPORTABLE_STUDY
     */
    public void setCcsgReportableStudy(String studyCcsgReportable) {
    	 this.studyCcsgReportable = StringUtil.stringToInteger(studyCcsgReportable);
    }
    
    /**
     * Get NciTrialIdentifier
     * @returns nciTrialIdentifier
	 * CTRP-22471 : Raviesh
     */
    @Column(name = "NCI_TRIAL_IDENTIFIER")
    public String getNciTrialIdentifier() {
		return nciTrialIdentifier;
	}

    /**
     * Set NciTrialIdentifier
     */
	public void setNciTrialIdentifier(String nciTrialIdentifier) {
		this.nciTrialIdentifier = nciTrialIdentifier;
	}

	/**
     * Get NctNumber
     * @returns NctNumber
	 * CTRP-22471 : Raviesh
     */
	@Column(name = "NCT_NUMBER")
	public String getNctNumber() {
		return nctNumber;
	}
	
	/**
     * Set NctNumber
     */
	public void setNctNumber(String nctNumber) {
		this.nctNumber = nctNumber;
	}
	
	/**
     * Get CtrpAccrualLastRunDate
     * @returns ctrpAccrualLastRunDate
	 * CTRP-22471 : Raviesh
     */
	@Column(name = "CTRP_ACCRREP_LASTRUN_DATE")
	public String getCtrpAccrualLastRunDate() {
		return ctrpAccrualLastRunDate;
	}

	/**
     * Set CtrpAccrualLastRunDate
     */
	public void setCtrpAccrualLastRunDate(String ctrpAccrualLastRunDate) {
		this.ctrpAccrualLastRunDate = ctrpAccrualLastRunDate;
	}
	
	/**
	 * This column stores the flag to lock/unlock protocol when a new version is created. 
	 * @return 0=unlocked, 1=locked	(default is 0)
	 */
	@Column(name = "STUDY_IS_LOCKED")
	public String getStudyIsLocked() {
		return StringUtil.integerToString(studyIsLocked);
	}

	public void setStudyIsLocked(String studyIsLocked) {
		this.studyIsLocked = StringUtil.stringToInteger(studyIsLocked);
	}
	
	/**
	 * This column stores the flag to block/unblock submission. 
	 * @return 0=unblocked, 1=blocked (default is 0)
	 */
	@Column(name = "STUDY_IS_BLOCKED")
	public String getStudyIsBlocked() {
		return StringUtil.integerToString(studyIsBlocked);
	}

	public void setStudyIsBlocked(String studyIsBlocked) {
		this.studyIsBlocked = StringUtil.stringToInteger(studyIsBlocked);
	}
 

    // END OF GETTER SETTER METHODS

    /**
     *
     * /** updates the study details
     *
     * @param ssh
     * @return 0 if successful; -2 for exception
     */

    public int updateStudy(StudyBean ssh) {

        try {

            setAccount(ssh.getAccount());
            setStudySponsor(ssh.getStudySponsor());
            setStudyContact(ssh.getStudyContact());
            setStudyPubFlag(ssh.getStudyPubFlag());

            Rlog.debug("study", "IN update study ssh.getStudyTitle().length"
                    + ssh.getStudyTitle().length());
            setStudyTitle(ssh.getStudyTitle());
            Rlog.debug("study", "IN update study studyTitle.length"
                    + this.studyTitle.length());
//JM: blocked
            //setStudyObjective(ssh.getStudyObjective());
            //setStudySummary(ssh.getStudySummary());
            setStudyProduct(ssh.getStudyProduct());

            setStudySize(ssh.getStudySize());
            setStudyDuration(ssh.getStudyDuration());
            setStudyDurationUnit(ssh.getStudyDurationUnit());
            setStudyEstBeginDate(ssh.getStudyEstBeginDate());
            setStudyActBeginDate(ssh.getStudyActBeginDate());

            setStudyPrimInv(ssh.getStudyPrimInv());
            setStudyCoordinator(ssh.getStudyCoordinator());
            setStudyPartCenters(ssh.getStudyPartCenters());
            setStudyKeywords(ssh.getStudyKeywords());
            setStudyPubCol(ssh.getStudyPubCol());
            setStudyParent(ssh.getStudyParent());
            setStudyAuthor(ssh.getStudyAuthor());
            setStudyTArea(ssh.getStudyTArea());
            setStudyResType(ssh.getStudyResType());
            setStudyPurpose(ssh.getStudyPurpose());

            setStudyType(ssh.getStudyType());
            setStudyBlinding(ssh.getStudyBlinding());
            setStudyRandom(ssh.getStudyRandom());
            setStudyNumber(ssh.getStudyNumber());
            setStudyPhase(ssh.getStudyPhase());

            setStudyVersion(ssh.getStudyVersion());
            setStudyCurrent(ssh.getStudyCurrent());

            setStudyInfo(ssh.getStudyInfo());

            setMajAuthor(ssh.getMajAuthor());
            setDisSite(ssh.getDisSite());
            setStudyScope(ssh.getStudyScope());
            setStudyAssoc(ssh.getStudyAssoc());

            setCreator(ssh.getCreator());
            setModifiedBy(ssh.getModifiedBy());
            setIpAdd(ssh.getIpAdd());
            setStudyVersionParent(ssh.getStudyVersionParent());
            setStudyCurrency(ssh.getStudyCurrency());

            setStudyEndDate(ssh.getStudyEndDate());
            setStudyAdvlkpVer(ssh.getStudyAdvlkpVer());
            setStudyNSamplSize(ssh.getStudyNSamplSize());
            setStudyOtherPrinv(ssh.getStudyOtherPrinv());

            setStudyDivision(ssh.getStudyDivision());
            setStudyInvIndIdeFlag(ssh.getStudyInvIndIdeFlag());
            setStudyIndIdeNum(ssh.getStudyIndIdeNum());
            setStudyICDCode1(ssh.getStudyICDCode1());
            setStudyICDCode2(ssh.getStudyICDCode2());
            setStudyICDCode3(ssh.getStudyICDCode3());
            setStudySponsorName(ssh.getStudySponsorName());
            setStudySponsorIdInfo(ssh.getStudySponsorIdInfo());
            setStudyCreationType(ssh.getStudyCreationType());
            setMilestoneSetStatus(ssh.getMilestoneSetStatus());
            setStudyCtrpReportable(ssh.getStudyCtrpReportable());
            setFdaRegulatedStudy(ssh.getFdaRegulatedStudy());
            setCcsgReportableStudy(ssh.getCcsgReportableStudy());
            
            setNciTrialIdentifier(ssh.getNciTrialIdentifier());
            setNctNumber(ssh.getNctNumber());
            setCtrpAccrualLastRunDate(ssh.getCtrpAccrualLastRunDate());
            setCtrpAccrualLastGenerationDate(ssh.getCtrpAccrualLastGenerationDate());
            setStudyIsLocked(ssh.getStudyIsLocked());
            
            return 0;
        } catch (Exception e) {
            Rlog.fatal("study", " error StduyBean.update" + e);
            return -2;
        }
    }

}// end of class

