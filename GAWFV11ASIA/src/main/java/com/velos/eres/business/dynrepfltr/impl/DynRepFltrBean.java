/*
 *  Classname : DynRepFltrBean
 *
 *  Version information: 1.0
 *
 *  Date: 01/29/2004
 *
 *  Copyright notice: Velos, Inc
 *
 *  Author: Vishal Abrol
 */

package com.velos.eres.business.dynrepfltr.impl;

/*
 * Import Statements
 */
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/*
 * End of Import Statements
 */
/**
 * The DynRepFltrBean CMP entity bean.<br>
 * <br>
 * 
 * 
 * @author vishal abrol
 * @created October 25, 2004
 * @vesion 1.0 12/15/2003
 * @ejbHome DynRepFltrHome
 * @ejbRemote DynRepFltrRObj
 */
/*
 * Modified by Sonia Abrol, 01/13/05, added a new attribute parentFilter- Parent
 * Filter Id associated with the filter. Used for Multiform reports
 */

/*
 * Modified by Sonia Abrol. 01/26/05, added a new attribute formType. This
 * attribute is added to distinguish between application forms and core table
 * lookup forms
 */

/*
 * Modified by Sonia Abrol , 05/02/05, for enhancement default date range
 * filters for each filter, added new
 * attributes:flrDateRangeType,fltrDateRangeFrom,fltrDateRangeTo
 */

@Entity
@Table(name = "er_dynrepfilter")
public class DynRepFltrBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3761971566296185138L;

    /**
     * The primary key:pk_dynrepfilter
     */

    public int id;

    /**
     * The foreign key reference to er_dynrep table.
     */
    public Integer repId;

    /**
     * The creator
     */

    public Integer creator;

    /**
     * The last modification by: LAST_MODIFICATION_BY
     */

    public Integer modifiedBy;

    /**
     * The IP Address : IP_ADD
     */

    public String ipAdd;

    /**
     * Description of the Field
     */
    public String fltrName;

    /**
     * Description of the Field
     */
    public String fltrStr;

    /**
     * FormId associated with the filter
     */
    public String formId;

    /**
     * Parent Filter Id associated with the filter. Used for Multiform reports
     */
    public String parentFilter;

    /**
     * Distinguishes between application forms and core table lookup forms <br>
     * Possible Values: <br>
     * 'F' - Application forms <br>
     * 'C' - COre table lookup forms
     */
    public String formType;

    /**
     * the type of date range applied on the filter. Possible values: A:All,
     * M:Month,Y:Year,DR:given range, PS:patient study status
     */
    public String flrDateRangeType;

    /**
     * the 'from' date for the date range filter. In case of 'patient study
     * status', the attribute stores the code subtype of patient study status
     */
    public String fltrDateRangeFrom;

    /**
     * the 'to' date for the date range filter. In case of 'patient study
     * status', the attribute stores the code subtype of patient study status
     */
    public String fltrDateRangeTo;

    // /////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the id attribute of the DynRepFltrBean object
     * 
     * @param id
     *            The new id value
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets the id attribute of the DynRepFltrBean object
     * 
     * @return The id value
     */
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_DYNREPFILTER", allocationSize=1)
    @Column(name = "PK_DYNREPFILTER")
    public int getId() {
        return id;
    }

    /**
     * Sets the repId attribute of the DynRepFltrBean object
     * 
     * @param repId
     *            The new repId value
     */
    public void setRepId(String repId) {
        if (!StringUtil.isEmpty(repId)) {
            this.repId = Integer.valueOf(repId);
        }
    }

    /**
     * Gets the repId attribute of the DynRepFltrBean object
     * 
     * @return The repId value
     */
    @Column(name = "FK_DYNREP")
    public String getRepId() {
        return StringUtil.integerToString(repId);
    }

    /**
     * @return the Creator
     */

    @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The new creator value
     */

    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }

    }

    /**
     * @return the unique ID of the User
     */
    @Column(name = "LAST_MODIFIED_BY")
    public String getModifiedBy() {

        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The new modifiedBy value
     */

    public void setModifiedBy(String modifiedBy) {

        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }

    }

    /**
     * @return the IP Address of the Form Notification request
     */
    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The new ipAdd value
     */

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
     * Sets the fltrName attribute of the DynRepFltrBean object
     * 
     * @param fltrName
     *            The new fltrName value
     */

    public void setFltrName(String fltrName) {
        this.fltrName = fltrName;
    }

    /**
     * Gets the fltrName attribute of the DynRepFltrBean object
     * 
     * @return The fltrName value
     */
    @Column(name = "DYNREPFILTER_NAME")
    public String getFltrName() {
        return fltrName;
    }

    /**
     * Sets the fltrStr attribute of the DynRepFltrBean object
     * 
     * @param fltrStr
     *            The new fltrStr value
     */
    public void setFltrStr(String fltrStr) {
        this.fltrStr = fltrStr;
    }

    /**
     * Gets the fltrStr attribute of the DynRepFltrBean object
     * 
     * @return The fltrStr value
     */
    @Column(name = "DYNREPFILTER_FILTER")
    public String getFltrStr() {
        return fltrStr;
    }

    /**
     * Returns the value of formId.
     * 
     * @return The formId value
     */
    @Column(name = "FK_FORM")
    public String getFormId() {
        return formId;
    }

    /**
     * Sets the value of formId.
     * 
     * @param formId
     *            The value to assign formId.
     */
    public void setFormId(String formId) {
        this.formId = formId;
    }

    /**
     * Returns the value of parentFilter.
     */
    @Column(name = "FK_PARENTFILTER")
    public String getParentFilter() {
        return parentFilter;
    }

    /**
     * Sets the value of parentFilter.
     * 
     * @param parentFilter
     *            Parent Filter Id, used in multiform reports
     */
    public void setParentFilter(String parentFilter) {
        this.parentFilter = parentFilter;
    }

    /**
     * Returns the value of formType.
     */
    @Column(name = "FORM_TYPE")
    public String getFormType() {
        return formType;
    }

    /**
     * Sets the value of formType.
     * 
     * @param formType
     *            The value to assign formType.
     */
    public void setFormType(String formType) {
        this.formType = formType;
    }

    /**
     * Returns the value of flrDateRangeType.
     */
    @Column(name = "DATERANGE_TYPE")
    public String getFlrDateRangeType() {
        return flrDateRangeType;
    }

    /**
     * Sets the value of flrDateRangeType.
     * 
     * @param flrDateRangeType
     *            The value to assign flrDateRangeType.
     */
    public void setFlrDateRangeType(String flrDateRangeType) {
        this.flrDateRangeType = flrDateRangeType;
    }

    /**
     * Returns the value of fltrDateRangeFrom.
     */
    @Column(name = "DATERANGE_FROM")
    public String getFltrDateRangeFrom() {
        return fltrDateRangeFrom;
    }

    /**
     * Sets the value of fltrDateRangeFrom.
     * 
     * @param fltrDateRangeFrom
     *            The value to assign fltrDateRangeFrom.
     */
    public void setFltrDateRangeFrom(String fltrDateRangeFrom) {
        this.fltrDateRangeFrom = fltrDateRangeFrom;
    }

    /**
     * Returns the value of fltrDateRangeTo.
     */
    @Column(name = "DATERANGE_TO")
    public String getFltrDateRangeTo() {
        return fltrDateRangeTo;
    }

    /**
     * Sets the value of fltrDateRangeTo.
     * 
     * @param fltrDateRangeTo
     *            The value to assign fltrDateRangeTo.
     */
    public void setFltrDateRangeTo(String fltrDateRangeTo) {
        this.fltrDateRangeTo = fltrDateRangeTo;
    }

    // END OF THE GETTER AND SETTER METHODS

    // ////////////////////////////////////////////////////////////////////

    /**
     * Gets the dynRepFltrStateKeeper attribute of the DynRepFltrBean object
     * 
     * @return The dynRepFltrStateKeeper value
     */
    /*
     * public DynRepFltrStateKeeper getDynRepFltrStateKeeper() {
     * Rlog.debug("dynrep", "DynRepFltrBean.getDynRepFltrStateKeeper");
     * 
     * return new DynRepFltrStateKeeper(getId(), getRepId(), getCreator(),
     * getModifiedBy(), getIpAdd(), getFltrName(), getFltrStr(), getFormId(),
     * getParentFilter(), getFormType(), getFlrDateRangeType(),
     * getFltrDateRangeFrom(), getFltrDateRangeTo()); }
     */

    /**
     * sets up a state keeper containing details of the dynrepDT
     * 
     * @param dsk
     *            The new dynRepFltrStateKeeper value
     */

    /*
     * public void setDynRepFltrStateKeeper(DynRepFltrStateKeeper dsk) {
     * GenerateId stId = null; try { Connection conn = null; conn =
     * getConnection(); Rlog.debug("dynrep",
     * "DynRepFltrBean.setDynRepFltrStateKeeper() DynRepFltrSTATEKEEPER :" +
     * dsk); Rlog.debug("dynrep", "DynRepFltrBean.setDynRepFltrStateKeeper()
     * Connection :" + conn);
     * 
     * this.id = stId.getId("SEQ_ER_DYNREPFILTER", conn); Rlog.debug("dynrep",
     * "DynRepFltrBean.Id " + this.id); conn.close();
     * 
     * setRepId(dsk.getRepId()); Rlog.debug("dynrep", "DynRepFltrBean.setRepId() " +
     * dsk.getRepId()); setCreator(dsk.getCreator()); Rlog.debug("dynrep",
     * "DynRepFltrBean.getCreator() " + dsk.getCreator());
     * setIpAdd(dsk.getIpAdd()); Rlog.debug("dynrep", "DynRepFltrBean.getIpAdd() " +
     * dsk.getIpAdd()); setFltrName(dsk.getFltrName()); Rlog.debug("dynrep",
     * "DynRepFltrBean.getFltrName() " + dsk.getFltrName());
     * setFltrStr(dsk.getFltrStr()); Rlog.debug("dynrep",
     * "DynRepFltrBean.getFltrStr() " + dsk.getFltrStr());
     * setFormId(dsk.getFormId()); Rlog.debug("dynrep",
     * "DynRepFltrBean.getFormId " + dsk.getFormId());
     * 
     * setParentFilter(dsk.getParentFilter()); Rlog.debug("dynrep",
     * "DynRepFltrBean.getParentFilter " + dsk.getParentFilter());
     * 
     * setFormType(dsk.getFormType()); Rlog.debug("dynrep",
     * "DynRepFltrBean.getFormType " + dsk.getFormType());
     * 
     * setFlrDateRangeType(dsk.getFlrDateRangeType());
     * setFltrDateRangeFrom(dsk.getFltrDateRangeFrom());
     * setFltrDateRangeTo(dsk.getFltrDateRangeTo()); } catch (Exception e) {
     * Rlog.fatal("dynrep", "Error in setDynRepFltrStateKeeper() in
     * DynRepFltrBean " + e); } }
     */

    /**
     * updates the Record
     * 
     * @param dsk
     * @return int 0: in case of successful updation; -2 in caseof exception
     */
    public int updateDynRepFltr(DynRepFltrBean dsk) {
        try {
            setRepId(dsk.getRepId());
            setCreator(dsk.getCreator());
            setModifiedBy(dsk.getModifiedBy());
            setIpAdd(dsk.getIpAdd());
            setFltrName(dsk.getFltrName());
            setFltrStr(dsk.getFltrStr());
            setFormId(dsk.getFormId());
            setParentFilter(dsk.getParentFilter());
            setFormType(dsk.getFormType());

            setFlrDateRangeType(dsk.getFlrDateRangeType());
            setFltrDateRangeFrom(dsk.getFltrDateRangeFrom());
            setFltrDateRangeTo(dsk.getFltrDateRangeTo());

            Rlog.debug("dynrep", "DynRepFltrBean.getParentFilter "
                    + dsk.getParentFilter());

            return 0;
        } catch (Exception e) {
            Rlog.fatal("dynrep", " error in DynRepFltrBean.updateDynRepFltr"
                    + e);
            return -2;
        }
    }

    public DynRepFltrBean() {

    }

    public DynRepFltrBean(int id, String repId, String creator,
            String modifiedBy, String ipAdd, String fltrName, String fltrStr,
            String formId, String parentFilter, String formType,
            String flrDateRangeType, String fltrDateRangeFrom,
            String fltrDateRangeTo) {
        super();
        // TODO Auto-generated constructor stub
        setId(id);
        setRepId(repId);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setFltrName(fltrName);
        setFltrStr(fltrStr);
        setFormId(formId);
        setParentFilter(parentFilter);
        setFormType(formType);
        setFlrDateRangeType(flrDateRangeType);
        setFltrDateRangeFrom(fltrDateRangeFrom);
        setFltrDateRangeTo(fltrDateRangeTo);
    }

}
