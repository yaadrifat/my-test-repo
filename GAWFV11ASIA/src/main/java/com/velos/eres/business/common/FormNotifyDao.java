/*
 * Classname			FormNotifyDao.class
 * 
 * Version information 	1.0
 *
 * Date					27/06/2003
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.Rlog;

/**
 * FormNotifyDao for getting FormNotification records
 * 
 * @author Sonia Kaura
 * @version : 1.0 27/06/2003
 */

public class FormNotifyDao extends CommonDAO implements java.io.Serializable {

    private int formLibId; // this is the foreign key to the library

    private ArrayList formNotifyId; // this is the primary key of the form

    // notification

    private ArrayList msgType;

    private ArrayList msgText;

    private ArrayList msgSendType;

    private ArrayList userList;

    private int rows; // to get the number of rows of the incoming result set

    public FormNotifyDao() {
        formNotifyId = new ArrayList();
        msgType = new ArrayList();
        msgText = new ArrayList();
        msgSendType = new ArrayList();
        userList = new ArrayList();

    }

    // THE GET AND SEND METHODS

    public int getFormLibId() {
        return this.formLibId;
    }

    public void setFormLibId(int formLibId) {
        this.formLibId = formLibId;
    }

    public ArrayList getFormNotifyId() {
        return this.formNotifyId;
    }

    public void setFormNotifyId(ArrayList formNotifyId) {
        this.formNotifyId = formNotifyId;
    }

    public ArrayList getMsgType() {
        return this.msgType;
    }

    public void setMsgType(ArrayList msgType) {
        this.msgType = msgType;
    }

    public ArrayList getMsgText() {
        return this.msgText;
    }

    public void setMsgText(ArrayList msgText) {
        this.msgText = msgText;
    }

    public ArrayList getMsgSendType() {
        return this.msgSendType;
    }

    public void setMsgSendType(ArrayList msgSendType) {
        this.msgSendType = msgSendType;
    }

    public ArrayList getUserList() {
        return this.userList;
    }

    public void setUserList(ArrayList userList) {
        this.userList = userList;
    }

    public int getRows() {
        return this.rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    // // // // // /
    // ////////////////////////////////////////////////////////////////////

    public void setFormNotifyId(Integer formNotifyId) {
        this.formNotifyId.add(formNotifyId);
    }

    public void setMsgSendType(String msgSendType) {
        this.msgSendType.add(msgSendType);
    }

    public void setMsgText(String msgText) {
        this.msgText.add(msgText);
    }

    public void setMsgType(String msgType) {
        this.msgType.add(msgType);
    }

    public void setUserList(String userList) {
        this.userList.add(userList);
    }

    // END OF SETTERS AND GETTERS

    /**
     * This method gets the records of the form notification for a particular
     * FORM whose ID is as specified by the FORM_LIB_ID foreign key.
     * 
     * @param the
     *            formLibId
     * @returns the arraylist of the result set to retrive all the records from
     *          the table ER_FORMNOTIFY
     * 
     * 
     */

    public void getAllFormNotifications(int formLibId) {

        int rows = 0;

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn
                    .prepareStatement("select PK_FN,FN_MSGTYPE,FN_MSGTEXT,FN_SENDTYPE, "
                            + "FN_USERS from ER_FORMNOTIFY "
                            + "where RECORD_TYPE <> 'D'  AND "
                            + "FK_FORMLIB=? ");

            pstmt.setInt(1, formLibId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setFormNotifyId(new Integer(rs.getInt("PK_FN")));
                setMsgType(rs.getString("FN_MSGTYPE"));
                setMsgText(rs.getString("FN_MSGTEXT"));
                setMsgSendType(rs.getString("FN_SENDTYPE"));
                setUserList(rs.getString("FN_USERS"));
                rows++;
                Rlog.debug("formnotify",
                        "FormNotifyDao.getAllFormNotification rows " + rows);
            }

            setRows(rows);

        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "formnotify",
                            "FormNotifyDao. getAllFormNotification EXCEPTION IN FETCHING FROM ER_FORMNOTIFY TABLE"
                                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

}
