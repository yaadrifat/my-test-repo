package com.velos.eres.business.userSession.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.velos.eres.business.common.CommonDAO;

public class UserSessionTokenDao extends CommonDAO implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private ArrayList<Integer> userSessionIdList = new ArrayList<Integer>();
	private ArrayList<Integer> userSessTimeList = new ArrayList<Integer>();
	
	private static final String getUserSessionByUserIdSql = " select s.PK_USERSESSION, u.USR_SESSTIME from " 
			+ " ER_USERSESSION s, ER_USER u where s.FK_USER = u.PK_USER and "
			+ " s.SUCCESS_FLAG = 1 and s.LOGOUT_TIME is null and s.LOGIN_TIME > (sysdate-1440/1440) and "
			+ " s.FK_USER = ? order by s.PK_USERSESSION desc";
	
	private static final String insertUserSessionTokenSql = " insert into ER_USERSESSIONTOKEN "
			+ " (PK_UST, UST_EXPIRATION_TIME, UST_TOKEN, UST_IP_ADD) values (SEQ_ER_USERSESSIONTOKEN.nextval, (sysdate+1/24), ?, ?) ";
	
	private static final String insertUserSessionTokenByUserSql = " insert into ER_USERSESSIONTOKEN "
			+ " (PK_UST, FK_USERSESSION, FK_USER, UST_EXPIRATION_TIME, UST_TOKEN) values "
			+ " (SEQ_ER_USERSESSIONTOKEN.nextval, ?, ?, (sysdate+?/1440), ?) ";
	
	private static final String invalidateUserSessionTokenByTokenSql = " update ER_USERSESSIONTOKEN set "
			+ " UST_EXPIRATION_TIME = sysdate where UST_TOKEN = ? and UST_IP_ADD = ? and UST_EXPIRATION_TIME > sysdate ";
	
	private static final String invalidateUserSessionTokenByUserSql = " update ER_USERSESSIONTOKEN set "
			+ " UST_EXPIRATION_TIME = sysdate where UST_TOKEN = ? and FK_USER = ? and UST_EXPIRATION_TIME > sysdate ";
	
	private static final String invalidateAllUserSessionTokensByUserSql = " update ER_USERSESSIONTOKEN set "
			+ " UST_EXPIRATION_TIME = sysdate where FK_USER = ? and UST_EXPIRATION_TIME > sysdate ";
	
	private static final String validateUserSessionTokenByUserSql = " select count(*) from ER_USERSESSIONTOKEN "
			+ " where UST_TOKEN = ? and FK_USER = ? and UST_EXPIRATION_TIME > sysdate ";
	
	public ArrayList<Integer> getUserSessionIdList() {
		return userSessionIdList;
	}

	public ArrayList<Integer> getUserSessTimeList() {
		return userSessTimeList;
	}

	public void findUserSessionbyUserId(int userId) {
        PreparedStatement stmt = null;
        Connection conn = null;
        try {
        	conn = getConnection();
        	stmt = conn.prepareStatement(getUserSessionByUserIdSql);
        	stmt.setInt(1, userId);
        	ResultSet rs = stmt.executeQuery();
        	while (rs.next()) {
        		userSessionIdList.add(rs.getInt(1));
        		userSessTimeList.add(rs.getInt(2));
        		break;
        	}
        } catch(Exception e) {
        	e.printStackTrace();
        	return;
        } finally {
        	try { if (stmt != null) { stmt.close(); } } catch(Exception e) {}
        	try { if (conn != null) { conn.close(); } } catch(Exception e) {}
        }
	}
	
	public int insertUserSessionToken(String token, String ipAdd) {
        PreparedStatement stmt = null;
        Connection conn = null;
        int resultIndex = 0;
        try {
        	conn = getConnection();
        	stmt = conn.prepareStatement(insertUserSessionTokenSql);
        	stmt.setString(1, token);
        	stmt.setString(2, ipAdd);
        	resultIndex = stmt.executeUpdate();
        } catch(Exception e) {
        	e.printStackTrace();
        	return -1;
        } finally {
        	try { if (stmt != null) { stmt.close(); } } catch(Exception e) {}
        	try { if (conn != null) { conn.close(); } } catch(Exception e) {}
        }
        if (resultIndex < 1) { return -1; }
		return 1;
	}
	
	public int insertUserSessionTokenByUser(int fkUserSession, int fkUser, int userSessTime, String token) {
        PreparedStatement stmt = null;
        Connection conn = null;
        int resultIndex = 0;
        try {
        	conn = getConnection();
        	stmt = conn.prepareStatement(insertUserSessionTokenByUserSql);
        	stmt.setInt(1, fkUserSession);
        	stmt.setInt(2, fkUser);
        	stmt.setInt(3, userSessTime);
        	stmt.setString(4, token);
        	resultIndex = stmt.executeUpdate();
        } catch(Exception e) {
        	e.printStackTrace();
        	return -1;
        } finally {
        	try { if (stmt != null) { stmt.close(); } } catch(Exception e) {}
        	try { if (conn != null) { conn.close(); } } catch(Exception e) {}
        }
        if (resultIndex < 1) { return -1; }
		return 1;
	}

	public int invalidateUserSessionToken(String token, String ipAdd) {
        PreparedStatement stmt = null;
        Connection conn = null;
        int resultIndex = 0;
        try {
        	conn = getConnection();
        	stmt = conn.prepareStatement(invalidateUserSessionTokenByTokenSql);
        	stmt.setString(1, token);
        	stmt.setString(2, ipAdd);
        	resultIndex = stmt.executeUpdate();
        } catch(Exception e) {
        	e.printStackTrace();
        	return -1;
        } finally {
        	try { if (stmt != null) { stmt.close(); } } catch(Exception e) {}
        	try { if (conn != null) { conn.close(); } } catch(Exception e) {}
        }
		return resultIndex;
	}
	
	public int invalidateUserSessionTokenByUser(String token, int userId) {
        PreparedStatement stmt = null;
        Connection conn = null;
        int resultIndex = 0;
        try {
        	conn = getConnection();
        	stmt = conn.prepareStatement(invalidateUserSessionTokenByUserSql);
        	stmt.setString(1, token);
        	stmt.setInt(2, userId);
        	resultIndex = stmt.executeUpdate();
        } catch(Exception e) {
        	e.printStackTrace();
        	return -1;
        } finally {
        	try { if (stmt != null) { stmt.close(); } } catch(Exception e) {}
        	try { if (conn != null) { conn.close(); } } catch(Exception e) {}
        }
		return resultIndex;
	}
	
	public int invalidateAllUserSessionTokensByUser(int userId) {
        PreparedStatement stmt = null;
        Connection conn = null;
        int resultIndex = 0;
        try {
        	conn = getConnection();
        	stmt = conn.prepareStatement(invalidateAllUserSessionTokensByUserSql);
        	stmt.setInt(1, userId);
        	resultIndex = stmt.executeUpdate();
        } catch(Exception e) {
        	e.printStackTrace();
        	return -1;
        } finally {
        	try { if (stmt != null) { stmt.close(); } } catch(Exception e) {}
        	try { if (conn != null) { conn.close(); } } catch(Exception e) {}
        }
        return resultIndex;
	}
	
	public int validateUserSessionTokenByUser(String token, int userId) {
        PreparedStatement stmt = null;
        Connection conn = null;
        int resultIndex = 0;
        try {
        	conn = getConnection();
        	stmt = conn.prepareStatement(validateUserSessionTokenByUserSql);
        	stmt.setString(1, token);
        	stmt.setInt(2, userId);
        	ResultSet rs = stmt.executeQuery();
        	while(rs.next()) {
        		resultIndex = rs.getInt(1);
        	}
        } catch(Exception e) {
        	e.printStackTrace();
        	return -1;
        } finally {
        	try { if (stmt != null) { stmt.close(); } } catch(Exception e) {}
        	try { if (conn != null) { conn.close(); } } catch(Exception e) {}
        }
		return resultIndex;
	}
}
