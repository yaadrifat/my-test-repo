/*
 * Classname			AddressBean.class
 *
 * Version information
 *
 * Date					02/26/2001
 *
 * Copyright notice
 */

package com.velos.eres.business.address.impl;

/**
 * @ejbHomeJNDIname ejb.Address
 */

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * The Address BMP entity bean.<br>
 * <br>
 *
 * @author Sajal
 * @version 1.0 02/26/2001
 */
@Entity
@Table(name = "er_add")
public class AddressBean implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 3761410811205333553L;

    /**
     * Address Id
     */
    public int addId;

    /**
     * Address type
     */
    public Integer addType;

    /**
     * Primary address
     */
    public String addPri;

    /**
     * City
     */
    public String addCity;

    /**
     * State
     */
    public String addState;

    /**
     * Zip code
     */
    public String addZip;

    /**
     * Country
     */
    public String addCountry;

    /**
     * County
     */
    public String addCounty;

    /**
     * Effective date of address
     */
    public Date addEffDate;

    /**
     * Phone #
     */
    public String addPhone;

    /**
     * Email
     */
    public String addEmail;

    /**
     * Pager #
     */
    public String addPager;

    /**
     * Mobile #
     */
    public String addMobile;

    /**
     * Fax #
     */
    public String addFax;

    /*
     * the TTY
     */
    public String addTty;

    /*
     * the URL
     */
    public String addUrl;
  
    /*
     * the record creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * the IP Address
     */
    public String ipAdd;

    // GETTER SETTER METHODS

    /**
     *
     *
     * @return Address Id
     */
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "seq_er_add", allocationSize=1)
    @Column(name = "PK_ADD")
    public int getAddId() {
        return this.addId;
    }

    public void setAddId(int id) {
        this.addId = id;
    }

    /**
     *
     *
     * @return Address Type
     */
    @Column(name = "FK_CODELST_ADDTYPE")
    public String getAddType() {
        return StringUtil.integerToString(this.addType);
    }

    /**
     *
     *
     * @param addressType
     *            Address Type
     */
    public void setAddType(String addressType) {
        if (addressType != null) {
            this.addType = Integer.valueOf(addressType);
        }
    }

    /**
     *
     *
     * @return Primary Address
     */
    @Column(name = "address")
    public String getAddPri() {
        return this.addPri;
    }

    /**
     *
     *
     * @param addressPri
     *            Primary Address
     */
    public void setAddPri(String addressPri) {
        this.addPri = addressPri;
    }

    /**
     *
     *
     * @return City
     */
    @Column(name = "ADD_CITY")
    public String getAddCity() {
        return this.addCity;
    }

    /**
     *
     *
     * @param addressCity
     *            City
     */
    public void setAddCity(String addressCity) {
        this.addCity = addressCity;
    }

    /**
     *
     *
     * @return State
     */
    @Column(name = "ADD_STATE")
    public String getAddState() {
        return this.addState;
    }

    /**
     *
     *
     * @param addressState
     *            State
     */
    public void setAddState(String addressState) {
        this.addState = addressState;
    }

    /**
     *
     *
     * @return Zip code
     */
    @Column(name = "ADD_ZIPCODE")
    public String getAddZip() {
        return this.addZip;
    }

    /**
     *
     *
     * @param addressZip
     *            Zip code
     */
    public void setAddZip(String addressZip) {
        this.addZip = addressZip;
    }

    /**
     *
     *
     * @return Country
     */
    @Column(name = "ADD_COUNTRY")
    public String getAddCountry() {
        return this.addCountry;
    }

    /**
     *
     *
     * @param addressCountry
     *            Country
     */
    public void setAddCountry(String addressCountry) {
        this.addCountry = addressCountry;
    }

    /**
     *
     *
     * @return County
     */
    @Column(name = "ADD_COUNTY")
    public String getAddCounty() {
        return this.addCounty;
    }

    /**
     *
     *
     * @param addressCounty
     *            County
     */
    public void setAddCounty(String addressCounty) {
        this.addCounty = addressCounty;
    }

    /**
     *
     *
     * @return Effective date of address
     */
    @Transient
    public String getAddEffDate() {
        String addressEffDate;
        addressEffDate = "";
        addressEffDate = DateUtil.dateToString(getAddEffDatePersistent());
        return addressEffDate;
    }

    /*
     * * @param addressEffDate Effective date of address
     */
    public void setAddEffDate(String addressEffDate) {
        DateUtil sDate = null;
        setAddEffDatePersistent(DateUtil.stringToDate(addressEffDate,null));
    }

    @Column(name = "ADD_EFF_DT")
    public Date getAddEffDatePersistent() {
        return addEffDate;
    }

    /**
     *
     *
     * @param addressEffDate
     *            Effective date of address
     */
    public void setAddEffDatePersistent(Date addressEffDate) {
        this.addEffDate = addressEffDate;
    }

    /**
     *
     *
     *
     *
     * /**
     *
     *
     * @return Phone #
     */
    @Column(name = "ADD_PHONE")
    public String getAddPhone() {
        return this.addPhone;
    }

    /**
     *
     *
     * @param addressPhone
     *            Phone #
     */
    public void setAddPhone(String addressPhone) {
        this.addPhone = addressPhone;
    }

    /**
     *
     *
     * @return Email address
     */
    @Column(name = "ADD_EMAIL")
    public String getAddEmail() {
        return this.addEmail;
    }

    /**
     *
     *
     * @param addressEmail
     *            Email address
     */
    public void setAddEmail(String addressEmail) {
        this.addEmail = addressEmail;
    }

    /**
     *
     *
     * @return Pager #
     */
    @Column(name = "ADD_PAGER")
    public String getAddPager() {
        return this.addPager;
    }

    /**
     *
     *
     * @param addressPager
     *            Pager #
     */
    public void setAddPager(String addressPager) {
        this.addPager = addressPager;
    }

    /**
     *
     *
     * @return Mobile #
     */
    @Column(name = "ADD_MOBILE")
    public String getAddMobile() {
        return this.addMobile;
    }

    /**
     *
     *
     * @param addressMobile
     *            Mobile #
     */
    public void setAddMobile(String addressMobile) {
        this.addMobile = addressMobile;
    }

    /**
     *
     *
     * @return Fax #
     */
    @Column(name = "ADD_FAX")
    public String getAddFax() {
        return this.addFax;
    }

    /**
     *
     *
     * @param addressFax
     *            Fax #
     */
    public void setAddFax(String addressFax) {
        this.addFax = addressFax;
    }

    /**
     * @return TTY
     */
    @Column(name = "ADD_TTY")
    public String getAddTty() {
        return this.addTty;
    }
    /**
     * @param addTty
     *     TTY specified as part of address
     */
    public void setAddTty(String addTty) {
        this.addTty = addTty;
    }
    
     /**
     * @return URL
     */
    @Column(name = "ADD_URL")
    public String getAddUrl() {
        return this.addUrl;
    }
    /**
     * @param addUrl
     *     URL specified as part of address
     */
    public void setAddUrl(String addUrl) {
        this.addUrl = addUrl;
    }
    
    /**
     * @return The Id of the creator of the record
     */
    @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    /**
     * @return The Id of the user modifying this record
     */
    @Column(name = "LAST_MODIFIED_BY")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    /**
     * @return IP Address
     */
    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // END OF GETTER SETTER METHODS

    /**
     *
     *
     * @return the state keeper object associated with this address
     */
    /*
     * public AddressStateKeeper getAddressStateKeeper() { Rlog.debug("address",
     * "AddressBean.getAddressStateKeeper"); return (new
     * AddressStateKeeper(getAddId(), getAddType(), getAddPri(), getAddCity(),
     * getAddState(), getAddZip(), getAddCountry(), getAddCounty(),
     * getAddEffDate(), getAddPhone(), getAddEmail(), getAddPager(),
     * getAddMobile(), getAddFax(), getCreator(), getModifiedBy(), getIpAdd())); }
     */

    /**
     * sets up a state keeper containing details of the address
     *
     * @param addsk
     *            AddressStateKeeper, an object which holds the address details
     */
    /*
     * public void setAddressStateKeeper(AddressStateKeeper addsk) { GenerateId
     * addressId = null; try { // id =
     * genId.getId("SEQ_ER_ADD",getConnection()); Connection conn = null; conn =
     * getConnection(); Rlog.debug("address",
     * "AddressBean.setAddressStateKeeper() Connection :" + conn);
     *
     * this.addId = addressId.getId("SEQ_ER_ADD", conn); Rlog.debug("address",
     * "AddressBean.setAddressStateKeeper() addId :" + addId); conn.close();
     * setAddType(addsk.getAddType()); Rlog.debug("address",
     * "AddressBean.setAddressStateKeeper() addType :" + addType);
     * setAddPri(addsk.getAddPri()); Rlog.debug("address",
     * "AddressBean.setAddressStateKeeper() addPri :" + addPri);
     * setAddCity(addsk.getAddCity()); Rlog.debug("address",
     * "AddressBean.setAddressStateKeeper() addCity :" + addCity);
     * setAddState(addsk.getAddState()); Rlog .debug("address",
     * "AddressBean.setAddressStateKeeper() addState :" + addState);
     * setAddZip(addsk.getAddZip()); Rlog.debug("address",
     * "AddressBean.setAddressStateKeeper() addZip :" + addZip);
     * setAddCountry(addsk.getAddCountry()); Rlog.debug("address",
     * "AddressBean.setAddressStateKeeper() addCountry :" + addCountry);
     * setAddCounty(addsk.getAddCounty()); Rlog.debug("address",
     * "AddressBean.setAddressStateKeeper() addCounty :" + addCounty);
     * setAddEffDate(addsk.getAddEffDate()); Rlog.debug("address",
     * "AddressBean.setAddressStateKeeper() addEffDate :" + addEffDate);
     * setAddPhone(addsk.getAddPhone()); Rlog .debug("address",
     * "AddressBean.setAddressStateKeeper() addPhone :" + addPhone);
     * setAddEmail(addsk.getAddEmail()); Rlog .debug("address",
     * "AddressBean.setAddressStateKeeper() addEmail :" + addEmail);
     * setAddPager(addsk.getAddPager()); Rlog .debug("address",
     * "AddressBean.setAddressStateKeeper() addPager :" + addPager);
     * setAddMobile(addsk.getAddMobile()); Rlog.debug("address",
     * "AddressBean.setAddressStateKeeper() addMobile :" + addMobile);
     * setAddFax(addsk.getAddFax()); Rlog.debug("address",
     * "AddressBean.setAddressStateKeeper() addFax :" + addFax);
     * setCreator(addsk.getCreator()); setModifiedBy(addsk.getModifiedBy());
     * setIpAdd(addsk.getIpAdd()); } catch (Exception e) { Rlog.fatal("address",
     * "Error in setAddressStateKeeper() in AddressBean " + e); } }
     */

    /**
     *
     *
     * @param addsk
     *            AddressStateKeeper, an object which holds the address details
     * @return 0 incase of successful update, -2 incase of error
     */
    public int updateAddress(AddressBean addsk) {
        try {
            setAddType(addsk.getAddType());
            setAddPri(addsk.getAddPri());
            setAddCity(addsk.getAddCity());
            setAddState(addsk.getAddState());
            setAddZip(addsk.getAddZip());
            setAddCountry(addsk.getAddCountry());
            setAddCounty(addsk.getAddCounty());
            setAddEffDate(addsk.getAddEffDate());
            setAddPhone(addsk.getAddPhone());
            setAddEmail(addsk.getAddEmail());
            setAddPager(addsk.getAddPager());
            setAddMobile(addsk.getAddMobile());
            setAddFax(addsk.getAddFax());
            setAddTty(addsk.getAddTty());
            setAddUrl(addsk.getAddUrl());
            setCreator(addsk.getCreator());
            setModifiedBy(addsk.getModifiedBy());
            setIpAdd(addsk.getIpAdd());
            Rlog.debug("address", "AddressBean.updateAddress");
            return 0;
        } catch (Exception e) {
            Rlog.fatal("address", " error in AddressBean.updateAddress");
            return -2;
        }
    }

    public AddressBean() {

    }

    public AddressBean(int addId, String addType, String addPri,
            String addCity, String addState, String addZip, String addCountry,
            String addCounty, String addEffDate, String addPhone,
            String addEmail, String addPager, String addMobile, String addFax,
            String addTty, String addUrl,
            String creator, String modifiedBy, String ipAdd) {
        super();
        // TODO Auto-generated constructor stub
        setAddId(addId);
        setAddType(addType);
        setAddPri(addPri);
        setAddCity(addCity);
        setAddState(addState);
        setAddZip(addZip);
        setAddCountry(addCountry);
        setAddCounty(addCounty);
        setAddEffDate(addEffDate);
        setAddPhone(addPhone);
        setAddEmail(addEmail);
        setAddPager(addPager);
        setAddMobile(addMobile);
        setAddFax(addFax);
        setAddTty(addTty);
        setAddUrl(addUrl);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
    }

}// end of class
