/*
 * Classname : FormNotify
 * 
 * Version information: 1.0
 *
 * Date: 24/06/2003
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: SoniaKaura
 */

package com.velos.eres.business.formNotify.impl;

/* Import Statements */

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* End of Import Statements */

/**
 * The FormNotify CMP entity bean.<br>
 * <br>
 * 
 * @author Sonia Kaura
 * @vesion 1.0 24/06/2003
 * @ejbHome FormNotifyHome
 * @ejbRemote FormNotifyRObj
 */
@Entity
@Table(name = "er_formnotify")
public class FormNotifyBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3546637728851832884L;

    /**
     * The form primary key:pk_fn
     */

    public int formNotifyId;

    /**
     * The foreign key reference from the Form Library: FK_FORMLIB
     */
    public Integer formLibId;

    /**
     * The Msg Type, Pop or Notification : FN_MSGTYPE
     */

    public String msgType;

    /**
     * The actual message text to be sent in notification: FN_MSGTEXT
     */
    public String msgText;

    /**
     * The frequency type of the message to be sent; First Time,
     * Everytime:FN_SENDTYPE
     */

    public String msgSendType;

    /**
     * The list of the users to whom the message is sent: FN_USERS
     * 
     */
    public String userList;

    /**
     * The creator making the notifcation : CREATOR
     * 
     */

    public Integer creator;

    /**
     * The last modification by: LAST_MODIFICATION_BY
     */

    public Integer modifiedBy;

    /**
     * The IP Address : IP_ADD
     */

    public String ipAdd;

    /**
     * The Record Type : Record_Type
     */

    public String recordType;

    private String nameList;

    // GETTER AND SETTER METHODS

    /**
     * @return the unique ID of the Form Notification
     */

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_FORMNOTIFY", allocationSize=1)
    @Column(name = "PK_FN")
    public int getFormNotifyId() {
        return this.formNotifyId;
    }

    public void setFormNotifyId(int id) {
        this.formNotifyId = id;
    }

    /**
     * @return the ID of the Form as in Form Library
     */
    @Column(name = "FK_FORMLIB")
    public String getFormLibId() {

        return StringUtil.integerToString(this.formLibId);

    }

    /**
     * @param the
     *            unique ID of the Form Notification
     */

    public void setFormLibId(String formLibId) {

        if (formLibId != null) {
            this.formLibId = Integer.valueOf(formLibId);
        }

    }

    /**
     * @return the Message Type of the Form Notification
     */
    @Column(name = "FN_MSGTYPE")
    public String getMsgType() {
        return String.valueOf(this.msgType);
    }

    /**
     * @param set
     *            the Message Type of the Form Notification
     */

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    /**
     * @return the Message Text of the Form Notification
     */

    @Column(name = "FN_MSGTEXT")
    public String getMsgText() {
        return this.msgText;
    }

    /**
     * @param set
     *            the Message Text of the Form Notification
     */

    public void setMsgText(String msgText) {
        this.msgText = msgText;
    }

    /**
     * @return the Message send type of the Form Notification
     */
    @Column(name = "FN_SENDTYPE")
    public String getMsgSendType() {
        return String.valueOf(this.msgSendType);
    }

    /**
     * @param set
     *            the Message send type ID of the Form Notification
     */

    public void setMsgSendType(String msgSendType) {

        this.msgSendType = msgSendType;

    }

    /**
     * @return the User list of the Form Notification
     */
    @Column(name = "FN_USERS")
    public String getUserList() {
        return this.userList;
    }

    /**
     * @param set
     *            the User list of the Form Notification
     */

    public void setUserList(String userList) {
        this.userList = userList;
    }

    /**
     * @return the unique ID of the User
     */

    @Column(name = "LAST_MODIFIED_BY")
    public String getModifiedBy() {

        return StringUtil.integerToString(this.modifiedBy);

    }

    /**
     * @param the
     *            set unique ID of the User
     */

    public void setModifiedBy(String modifiedBy) {

        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }

    }

    @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);

    }

    /**
     * @param takes
     *            a string
     */

    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }

    }

    /**
     * @return the IP Address of the Form Notification request
     */

    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param set
     *            the IP Address of the Form Notification request
     */

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
     * @return the Record Type : m: modified n:new d: delet
     */
    @Column(name = "RECORD_TYPE")
    public String getRecordType() {
        return this.recordType;
    }

    /**
     * @param set
     *            the Record Type to be modified,new or delete
     */
    public void setRecordType(String recordType) {
        this.recordType = recordType;

    }

    /**
     * @return set the Name List
     */
    @Transient
    public String getNameList() {
        return this.nameList;
    }

    /**
     * @param set
     *            the Record Type to be modified,new or delete
     */

    public void setNameList(String nameList) {
        this.nameList = nameList;
    }

    // END OF THE GETTER AND SETTER METHODS
    public FormNotifyBean() {

    }

    /*
     * public FormNotifyStateKeeper getFormNotifyStateKeeper() {
     * Rlog.debug("formnotify", "FormNotifyBean.getFormNotifyStateKeeper");
     * return (new FormNotifyStateKeeper(getFormNotifyId(), getFormLibId(),
     * getMsgType(), getMsgText(), getMsgSendType(), getUserList(),
     * getCreator(), getModifiedBy(), getIpAdd(), getRecordType())); }
     */

    /**
     * sets up a state keeper containing details of the form notify
     */

    /*
     * public void setFormNotifyStateKeeper(FormNotifyStateKeeper formsk) {
     * GenerateId formNotId = null; try { Connection conn = null; conn =
     * getConnection(); Rlog.debug("formnotify",
     * "FormNotifyBean.setFormNotifyStateKeeper() Connection :" + conn);
     * this.formNotifyId = formNotId.getId("SEQ_ER_FORMNOTIFY", conn);
     * Rlog.debug("formnotify", "FormNotifyBean.setFormNotifyStateKeeper()
     * formNotifyId :" + this.formNotifyId); conn.close(); //
     * Rlog.debug("formnotify","formsk.getFormLibId() :"+ //
     * formsk.getFormLibId());
     * 
     * setFormLibId(formsk.getFormLibId()); //
     * Rlog.debug("formnotify","formsk.getMsgType() :"+ // formsk.getMsgType());
     * setMsgType(formsk.getMsgType()); //
     * Rlog.debug("formnotify","formsk.getMsgTxt() :"+ // formsk.getMsgText());
     * setMsgText(formsk.getMsgText()); //
     * Rlog.debug("formnotify","formsk.getMsgSendType() :"+ //
     * formsk.getMsgSendType()); setMsgSendType(formsk.getMsgSendType()); //
     * Rlog.debug("formnotify","formsk.getUserList() :"+ //
     * formsk.getUserList()); setUserList(formsk.getUserList()); //
     * Rlog.debug("formnotify","formsk.getCreator() :"+ // formsk.getCreator());
     * setCreator(formsk.getCreator()); //
     * Rlog.debug("formnotify","formsk.getModifiedBy() :"+ //
     * formsk.getModifiedBy()); setModifiedBy(formsk.getModifiedBy()); //
     * Rlog.debug("formnotify","formsk.getIpAdd() :"+ // formsk.getIpAdd());
     * setIpAdd(formsk.getIpAdd()); //
     * Rlog.debug("formnotify","formsk.getRecordType() :"+ //
     * formsk.getRecordType()); setRecordType(formsk.getRecordType()); } catch
     * (Exception e) { Rlog.fatal("formnotify", "Error in
     * setFormNotifyStateKeeper() in FormNotifyBean " + e); } }
     */

    public FormNotifyBean(int formNotifyId, String formLibId, String msgType,
            String msgText, String msgSendType, String userList,
            String creator, String modifiedBy, String ipAdd, String recordType) {
        super();
        // TODO Auto-generated constructor stub
        setFormNotifyId(formNotifyId);
        setFormLibId(formLibId);
        setMsgType(msgType);
        setMsgText(msgText);
        setMsgSendType(msgSendType);
        setUserList(userList);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setRecordType(recordType);
    }

    /**
     * updates the Form Notify Record
     * 
     * @param formsk
     * @return int 0: in case of successful updation; -2 in caseof exception
     */
    public int updateFormNotify(FormNotifyBean formsk) {
        try {

            setFormLibId(formsk.getFormLibId());
            setMsgType(formsk.getMsgType());
            setMsgText(formsk.getMsgText());
            setMsgSendType(formsk.getMsgSendType());
            setUserList(formsk.getUserList());
            setCreator(formsk.getCreator());
            setModifiedBy(formsk.getModifiedBy());
            setIpAdd(formsk.getIpAdd());
            setRecordType(formsk.getRecordType());
            return 0;
        } catch (Exception e) {
            Rlog.fatal("formnotify",
                    " error in FormNotifyBean.updateFormNotify");
            return -2;
        }
    }

}
