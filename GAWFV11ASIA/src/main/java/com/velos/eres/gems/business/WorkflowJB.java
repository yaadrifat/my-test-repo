package com.velos.eres.gems.business;

import com.velos.eres.gems.service.WorkflowAgentRObj;
import com.velos.eres.service.util.EJBUtil;

public class WorkflowJB {
	
	public Workflow findWorkflowByType(String workflowType){
		Workflow workflow = new Workflow();
		try {
			WorkflowAgentRObj workflowProcessAgentRObj = EJBUtil.getWorkflowAgentHome();
			workflow = workflowProcessAgentRObj.findWorkflowByType(workflowType);
		} catch (Exception e) {
			e.printStackTrace();
			workflow = null;
		}
		return workflow;
	}

	public Integer getStudyIDbyNumber(String studyNumber) {
		WorkflowDAO workflowDao = new WorkflowDAO(); 
		return workflowDao.getStudyIDbyNumber(studyNumber);
	}	
}
