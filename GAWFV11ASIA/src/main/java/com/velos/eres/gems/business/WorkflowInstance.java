package com.velos.eres.gems.business;

import java.io.Serializable;

import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the WORKFLOW_INSTANCE database table.
 * 
 */
@Entity
@Table(name="WORKFLOW_INSTANCE")
@NamedQueries({
	@NamedQuery(name = "WorkflowInstance.findAll", query = "SELECT w FROM WorkflowInstance w"),
	@NamedQuery(name = "WorkflowInstance.findByFKEntity", query = "SELECT w FROM WorkflowInstance w where w.fkEntityId=:fkEntityId"),
	@NamedQuery(name = "WorkflowInstance.fnByEntityAndTyp", query = "SELECT wfi FROM WorkflowInstance wfi where wfi.fkEntityId=:fkEntityId AND wfi.workflow.pkWorkflow=(SELECT wf.pkWorkflow from Workflow wf where wf.workflowType=:wfType)")})
public class WorkflowInstance implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6811445135436508427L;
	private Integer pkWfInstanceId;
	private String fkEntityId;
	private Integer wfIStatusFlag;
	private Workflow workflow;
	private List<Task> tasks;

	public WorkflowInstance() {
	}


	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_GEN")
	@SequenceGenerator(name = "SEQ_GEN", sequenceName = "SQ_WORKFLOWINSTANCE", allocationSize = 1)
	@Column(name = "PK_WFINSTANCEID")
	public Integer getPkWfInstanceId() {
		return this.pkWfInstanceId;
	}

	public void setPkWfInstanceId(Integer pkWfInstanceId) {
		this.pkWfInstanceId = pkWfInstanceId;
	}


	@Column(name="FK_ENTITYID", length=1000)
	public String getFkEntityId() {
		return this.fkEntityId;
	}

	public void setFkEntityId(String fkEntityId) {
		this.fkEntityId = fkEntityId;
	}


	@Column(name="WI_STATUSFLAG", precision=22)
	public Integer getWfIStatusFlag() {
		return this.wfIStatusFlag;
	}

	public void setWfIStatusFlag(Integer wfIStatusFlag) {
		this.wfIStatusFlag = wfIStatusFlag;
	}

	//uni-directional many-to-one association to Workflow
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="FK_WORKFLOWID")
	public Workflow getWorkflow() {
		return this.workflow;
	}

	public void setWorkflow(Workflow workflow) {
		this.workflow = workflow;
	}


	//bi-directional many-to-one association to Task
	@OneToMany(mappedBy="workflowInstance")
	public List<Task> getTasks() {
		return this.tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}

	public Task addTask(Task task) {
		getTasks().add(task);
		task.setWorkflowInstance(this);

		return task;
	}

	public Task removeTask(Task task) {
		getTasks().remove(task);
		task.setWorkflowInstance(null);

		return task;
	}

}