package com.velos.eres.gems.service;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.gems.business.*;
import com.velos.esch.service.util.StringUtil;

@Stateless
@Remote({ WorkflowInstanceAgentRObj.class })
public class WorkflowInstanceAgentBean implements WorkflowInstanceAgentRObj

{
	@PersistenceContext(unitName = "eres")
	protected EntityManager em;
	@Resource
	private SessionContext context;


	public List<Task> fetchTasks(String entityId, String workflowType) throws Exception {
		
		Query query = null;
		List<WorkflowInstance> resultList = null;
		WorkflowInstance wfi = new WorkflowInstance();
		List<Task> tasks = new ArrayList<Task>();
		
		try {
			
			query = em.createNamedQuery("WorkflowInstance.fnByEntityAndTyp");
			query.setParameter("fkEntityId", entityId);
			query.setParameter("wfType", workflowType);
			
			resultList = query.getResultList();
			
			if (resultList.size() == 0) {
				return null;
			} else {
				wfi = (WorkflowInstance) resultList.get(0);
			}
			
			tasks =  wfi.getTasks();
			em.flush();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}
		
		return tasks;
	}

	public List<Task> fetchPendingTasks(String entityId, String workflowType) throws Exception {
		
		
		List<Task> tasks = this.fetchTasks(entityId, workflowType);
		List<Task> pendingTasks = new ArrayList<Task>();
		
		for(Task task:tasks){
			if(task.getTaskCompleteflag()==0){
				pendingTasks.add(task);
			}
		}
		return pendingTasks;
	}
	
	public WorkflowInstance getWorkflowInstanceStatus(String entityId,String workflowType) throws RemoteException,
			Exception {
		WorkflowInstance wfi = new WorkflowInstance();
		
		try {
			WorkflowDAO wfDao = new WorkflowDAO();
			wfi = wfDao.getWorkflowInstanceStatus(entityId, workflowType);
		} catch (Exception e) {
			e.printStackTrace();
			wfi = null;
		} finally {

		}
		return wfi;
	}

	public ArrayList getWorkflowInstanceTasks(int workflowInstanceId) throws RemoteException, Exception {
		ArrayList workflowTasks = new ArrayList();
		
		try {
			WorkflowDAO wfDao = new WorkflowDAO();
			workflowTasks = wfDao.getWorkflowInstanceTasks(workflowInstanceId);
		
		} catch (Exception e) {
			e.printStackTrace();
			workflowTasks = null;
		} finally {
		
		}
		return workflowTasks;
	}
	
	public Integer startWorkflowInstance( String entityId,String workflowType)
			throws RemoteException, Exception {

		Query query = null;
		List<Workflow> resultList = null;
		
		WorkflowInstance wfi = new WorkflowInstance();
		List<WorkflowActivity> wFActivities =new ArrayList<WorkflowActivity>();
		Workflow wf = new Workflow();
		Task task = new Task();
		try {
			query = em.createNamedQuery("Workflow.findByType");
			query.setParameter("workflowType", workflowType);

			resultList = query.getResultList();

			if (resultList.size() == 0) {
				return null;
			} else {
				wf = resultList.get(0);
			}

			wfi.setFkEntityId(entityId);
			wfi.setWfIStatusFlag(0);
			wfi.setWorkflow(wf);
			em.persist(wfi);
			
			wFActivities =  wf.getWorkflowActivities();

			for (WorkflowActivity wfa : wFActivities) {
				task = new Task();
				task.setDeletedflag(0);
				task.setTaskCompleteflag(0);
				task.setFkEntity(entityId);
				task.setTaskName(wfa.getWaName());
				task.setWorkflowActivity(wfa);
				task.setWorkflowInstance(wfi);
				em.persist(task);
				em.flush();
				
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return wfi.getPkWfInstanceId();
		
	}
	
	// Return Number of pending tasks after updating tasks and the work-flow
	public Integer updateWorkflowInstance(String entityId, String workflowType) throws RemoteException,Exception{
		
		try{
			Integer i = 0;
			Query query = em.createNativeQuery("{call PKG_WORKFLOW.SP_UPDATE_WORKFLOW(?,?,?,?)}");           
			query.setParameter(1, entityId);
            query.setParameter(2, workflowType);
            query.setParameter(3, "");
            query.setParameter(4, i);
            
            List result = query.getResultList();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	public Integer updateWorkflowInstance(String entityId, String workflowType, String[] paramArray) throws RemoteException,Exception{
		Integer workflowFlag = 0;
		try{
			WorkflowDAO wfd = new WorkflowDAO();
			workflowFlag =  wfd.updateWorkflow(entityId, workflowType, paramArray);
			
		} catch (Exception e){
			e.printStackTrace();
			workflowFlag = null;
		}	
		return workflowFlag;
	}
}