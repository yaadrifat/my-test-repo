package com.velos.eres.gems.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.velos.eres.gems.service.WorkflowInstanceAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.esch.service.util.StringUtil;

public class WorkflowInstanceJB {

	public WorkflowInstance getWorkflowInstanceStatus(String entityId,String workflowType) {
		WorkflowInstance workflowInsatnce = new WorkflowInstance();
		try {
			WorkflowInstanceAgentRObj workflowInstanceAgentRObj = EJBUtil.getWorkflowInstanceAgentHome();
			workflowInsatnce = workflowInstanceAgentRObj.getWorkflowInstanceStatus(entityId, workflowType);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return workflowInsatnce;
	}
	
	public ArrayList getWorkflowInstanceTasks(int workflowInstanceId) {
		ArrayList workflowTasks = new ArrayList();
		try {
			WorkflowInstanceAgentRObj workflowInstanceAgentRObj = EJBUtil
					.getWorkflowInstanceAgentHome();
			workflowTasks = workflowInstanceAgentRObj.getWorkflowInstanceTasks(workflowInstanceId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return workflowTasks;
	}
	
	public Integer startWorkflowInstance(String entityId,String workflowType){
		
		 Integer workflowFlag = 0;
		try {
			WorkflowInstanceAgentRObj workflowProcessAgentRObj = EJBUtil
					.getWorkflowInstanceAgentHome();
			workflowFlag = workflowProcessAgentRObj.startWorkflowInstance(entityId,workflowType);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return workflowFlag;

	}
	
	public Integer updateWorkflow(String entityId, String workflowType){
	
		Integer workflowFlag = 0;
		try {
			WorkflowInstanceAgentRObj workflowInstanceAgentRObj = EJBUtil
					.getWorkflowInstanceAgentHome();
			
			//workflowFlag = workflowInstanceAgentRObj.updateWorkflow(entityId,workflowType);
			WorkflowDAO wfd = new WorkflowDAO();
			workflowFlag =  wfd.updateWorkflow(entityId,workflowType);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return workflowFlag;

	}
	
	public Integer updateWorkflowInstance(String entityId, String workflowType, String paramListStr){
		Integer workflowFlag = 0;
		
		if (StringUtil.isEmpty(paramListStr)){
			return updateWorkflow(entityId, workflowType);
		}

		String[] paramArray = paramListStr.split("\\|");
		if (null == paramArray){ return null; }

		try {
			WorkflowInstanceAgentRObj workflowInstanceAgentRObj = EJBUtil.getWorkflowInstanceAgentHome();
			workflowFlag = workflowInstanceAgentRObj.updateWorkflowInstance(entityId,workflowType, paramArray);
		} catch (Exception e) {
			e.printStackTrace();
			workflowFlag = null;
		}
		return workflowFlag;
	}
	
	public Integer fetchNextTaskInQueue(Integer entityId, String workflowType){
		
		Integer taskId = 0;
			// TO-DO route through EJB
			WorkflowDAO wfd = new WorkflowDAO();
			taskId =  wfd.fetchNextTaskInQueue(entityId,workflowType);
		
		return taskId;

	}
	
public Integer fetchFirstPendingTaskInQueue(Integer entityId, String workflowType){
		
		Integer taskId = 0;
			// TO-DO route through EJB
			WorkflowDAO wfd = new WorkflowDAO();
			taskId =  wfd.fetchFirstPendingTaskInQueue(entityId,workflowType);
		
		return taskId;

	}

public Map<String,String> fetchTaskDetails(Integer taskId){
	
	Map<String,String> resultMap = new HashMap<String,String>();
	
		// TO-DO route through EJB
		WorkflowDAO wfd = new WorkflowDAO();
		resultMap =  wfd.fetchTaskDetails(taskId);
	
	return resultMap;

}
	
}
