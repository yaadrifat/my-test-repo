package com.velos.eres.gems.business;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Date;


/**
 * The persistent class for the TASK database table.
 * 
 */
@Entity
@Table(schema="ERES", name="TASK")
@NamedQuery(name="Task.findAll", query="SELECT t FROM Task t")
public class Task implements Serializable {
	
	private Integer pkTaskid;
	private Integer auditid;
	private Date completedon;
	private Integer createdby;
	private Date createdon;
	private Integer deletedflag;
	private String fkEntity;
	private Integer fkUserid;
	private Integer taskCompleteflag;
	private String taskName;
	private Integer taskStartflag;
	private Integer taskUserorroleid;
	private String taskUserorroletype;
	private WorkflowActivity workflowActivity;
	private WorkflowInstance workflowInstance;

	public Task() {
	}


	@Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SQ_TASK", allocationSize=1)
	@Column(name="PK_TASKID")
	public Integer getPkTaskid() {
		return this.pkTaskid;
	}

	public void setPkTaskid(Integer pkTaskid) {
		this.pkTaskid = pkTaskid;
	}


	@Column(precision=22)
	public Integer getAuditid() {
		return this.auditid;
	}

	public void setAuditid(Integer auditid) {
		this.auditid = auditid;
	}


	public Date getCompletedon() {
		return this.completedon;
	}

	public void setCompletedon(Date completedon) {
		this.completedon = completedon;
	}


	@Column(precision=22)
	public Integer getCreatedby() {
		return this.createdby;
	}

	public void setCreatedby(Integer createdby) {
		this.createdby = createdby;
	}


	public Date getCreatedon() {
		return this.createdon;
	}

	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}


	@Column(precision=22)
	public Integer getDeletedflag() {
		return this.deletedflag;
	}

	public void setDeletedflag(Integer deletedflag) {
		this.deletedflag = deletedflag;
	}


	@Column(name="FK_ENTITY", length=1000)
	public String getFkEntity() {
		return this.fkEntity;
	}

	public void setFkEntity(String fkEntity) {
		this.fkEntity = fkEntity;
	}


	@Column(name="FK_USERID", precision=22)
	public Integer getFkUserid() {
		return this.fkUserid;
	}

	public void setFkUserid(Integer fkUserid) {
		this.fkUserid = fkUserid;
	}


	@Column(name="TASK_COMPLETEFLAG", precision=22)
	public Integer getTaskCompleteflag() {
		return this.taskCompleteflag;
	}

	public void setTaskCompleteflag(Integer taskCompleteflag) {
		this.taskCompleteflag = taskCompleteflag;
	}


	@Column(name="TASK_NAME", length=500)
	public String getTaskName() {
		return this.taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}


	@Column(name="TASK_STARTFLAG", precision=22)
	public Integer getTaskStartflag() {
		return this.taskStartflag;
	}

	public void setTaskStartflag(Integer taskStartflag) {
		this.taskStartflag = taskStartflag;
	}


	@Column(name="TASK_USERORROLEID", precision=22)
	public Integer getTaskUserorroleid() {
		return this.taskUserorroleid;
	}

	public void setTaskUserorroleid(Integer taskUserorroleid) {
		this.taskUserorroleid = taskUserorroleid;
	}


	@Column(name="TASK_USERORROLETYPE", length=100)
	public String getTaskUserorroletype() {
		return this.taskUserorroletype;
	}

	public void setTaskUserorroletype(String taskUserorroletype) {
		this.taskUserorroletype = taskUserorroletype;
	}


	//uni-directional many-to-one association to WorkflowActivity
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="FK_WORKFLOWACTIVITY")
	public WorkflowActivity getWorkflowActivity() {
		return this.workflowActivity;
	}

	public void setWorkflowActivity(WorkflowActivity workflowActivity) {
		this.workflowActivity = workflowActivity;
	}


	//bi-directional many-to-one association to WorkflowInstance
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="FK_WFINSTANCEID")
	public WorkflowInstance getWorkflowInstance() {
		return this.workflowInstance;
	}

	public void setWorkflowInstance(WorkflowInstance workflowInstance) {
		this.workflowInstance = workflowInstance;
	}

}