package com.velos.eres.web.study;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.json.JSONArray;
import org.json.JSONObject;

import com.aithent.audittrail.reports.AuditUtils;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.StudySiteDao;
import com.velos.eres.business.common.StudyVerDao;
import com.velos.eres.business.common.TeamDao;
import com.velos.eres.business.common.UserDao;
import com.velos.eres.business.studyId.impl.StudyIdBean;
import com.velos.eres.gems.business.WorkflowDAO;
import com.velos.eres.service.studyVerAgent.StudyVerAgentRObj;
import com.velos.eres.service.util.CFG;
import com.velos.eres.service.util.Configuration;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.MC;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.intercept.InterceptorJB;
import com.velos.eres.web.section.SectionJB;
import com.velos.eres.web.statusHistory.StatusHistoryJB;
import com.velos.eres.web.stdSiteRights.StdSiteRightsJB;
import com.velos.eres.web.studyIDEIND.StudyINDIDEJB;
import com.velos.eres.web.studyId.StudyIdJB;
import com.velos.eres.web.studyNIHGrant.StudyNIHGrantJB;
import com.velos.eres.web.studySite.StudySiteJB;
import com.velos.eres.web.studyVer.StudyVerJB;
import com.velos.eres.web.team.TeamJB;
import com.velos.eres.web.user.UserJB;
import com.velos.eres.widget.business.common.UIFlxPageDao;
import com.velos.esch.business.common.SchCodeDao;

public class StudyAction extends ActionSupport  {
	private static final long serialVersionUID = -3291102981256093400L;
	private HttpServletRequest request = null;
	private HttpServletResponse response = null;
	
	private StudyJB studyJB = new StudyJB();

	private TeamJB teamJB = new TeamJB();
	private StatusHistoryJB statusJB = new StatusHistoryJB();
	
	private StudyINDIDEJB studyIndIdeJB = new StudyINDIDEJB();
	private StudyNIHGrantJB studyNIHGrantJB = new StudyNIHGrantJB();
	
	private StudySiteJB studySiteJB = new StudySiteJB();
	private StdSiteRightsJB stdSiteRightsB =  new StdSiteRightsJB();
	
	public static final String DATA_NOT_SAVED = "dataNotSaved";
	public static final String DATA_NOT_DELETED = "dataNotDeleted";
	public static final String ESIGN_DOES_NOT_MATCH = "esignDoesNotMatch";
	private Map errorMap = null;
	private Collection errorAction = null;
	
	private static final String NULL_STR = "null";
	private static final String A_STR = "A";
	private static final String LIND_STR = "LIND";


	public StudyAction() {
		ActionContext ac = ActionContext.getContext();
		request = (HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
		response = (HttpServletResponse)ac.get(ServletActionContext.HTTP_RESPONSE);
	}
	
	public void setErrorMap(Map errorMap) {
		this.errorMap = errorMap;
	}

	public Map getErrorMap() {
		return errorMap;
	}

	private boolean findRequestParamter(HttpServletRequest request, String paramName, String studyId){
		boolean exists = false;

		if (null == request) return false;
			
		Map paramMap = request.getParameterMap();
		if (paramMap.containsKey(paramName)){
        	exists = true;
        }
		return exists;
	}

	public HashMap<String, Object> loadParameterMap (){
		HashMap<String, Object> args = new HashMap<String, Object>();
		
		HttpSession tSession = request.getSession(true);

		String eSign = request.getParameter("eSign");
		args.put("eSign", eSign);
		
		if (null != eSign){
			String oldESign = (String) tSession.getAttribute("eSign");
			args.put("oldESign", oldESign);
			
			if(!oldESign.equals(eSign)) {
				throw new IllegalArgumentException(ESIGN_DOES_NOT_MATCH);
			}
		}

		String studyId = (String) tSession.getAttribute("studyId");
		args.put("studyId", studyId);
		
		if (null == studyJB) studyJB = new StudyJB();
		studyJB.setId(StringUtil.stringToNum(studyId));
		studyJB.getStudyDetails();
		
		String accId = (String) tSession.getAttribute("accountId");
		args.put("accId", accId);

		String userId = (String) tSession.getAttribute("userId");
		args.put("userId", userId);

		String ipAdd = (String) tSession.getAttribute("ipAdd");
		args.put("ipAdd", ipAdd);	
		
	  	CodeDao cdRole = new CodeDao();
		args.put("piRole", ""+cdRole.getCodeId("role","role_prin"));

		CodeDao codedao = new CodeDao();
		args.put("teamStatusActiveId", ""+codedao.getCodeId("teamstatus","Active"));
		
		Date dt = new java.util.Date();
		args.put("statusDate", DateUtil.dateToString(dt));

		String studyNumber = null;
		if (findRequestParamter(request, "studyNumber", studyId)){
			studyNumber = request.getParameter("studyNumber");
		} else {
			// Handle the auto-generation override here
			studyNumber = studyJB.getStudyNumber();
		}
		// trim the studyNumber
		if (studyNumber == null ) studyNumber = "";//KM
		studyNumber = studyNumber.trim();
		args.put("studyNumber", studyNumber);
		
		String studyTitle = null;
		if (findRequestParamter(request, "studyTitle", studyId)){
			studyTitle = request.getParameter("studyTitle");
		} else {
			studyTitle = studyJB.getStudyTitle();
		}
		if (studyTitle == null) studyTitle =""; //KM
		if (studyTitle.length() > 1000){
			studyTitle = studyTitle.substring(0,1000);
		}
		args.put("studyTitle", studyTitle);
		
		String studyObjective = null;
		if (findRequestParamter(request, "studyObjective", studyId)){
			studyObjective =request.getParameter("studyObjective");
		} else {
			studyObjective = studyJB.getStudyObjective();
		}
		studyObjective = (studyObjective  == null)? "" : (studyObjective);
		args.put("studyObjective", studyObjective);

		String studySummary = null;
		if (findRequestParamter(request, "studySummary", studyId)){
			studySummary = request.getParameter("studySummary");
		} else {
			studySummary = studyJB.getStudySummary();
		}
		studySummary = (studySummary  == null)? "" : (studySummary);
		args.put("studySummary", studySummary);

		String studyProduct = null;
		if (findRequestParamter(request, "studyProduct", studyId)){
			studyProduct = request.getParameter("studyProduct");
		} else {
			studyProduct = studyJB.getStudyProduct();
		}
		studyProduct = (studyProduct  == null)? "" : (studyProduct);
		args.put("studyProduct", studyProduct);

		String studyTArea = null;
		if (findRequestParamter(request, "studyTArea", studyId)){
			studyTArea = request.getParameter("studyTArea");
		} else {
			studyTArea = studyJB.getStudyTArea();
		}
		args.put("studyTArea", studyTArea);

		String studyPurpose  = null;
		if (findRequestParamter(request, "studyPurpose", studyId)){
			studyPurpose = request.getParameter("studyPurpose");
		} else {
			studyPurpose = studyJB.getStudyPurpose();
		}
		args.put("studyPurpose", studyPurpose);
		
		//String studySize = "" ;
		String studyDuration = null;
		if (findRequestParamter(request, "studyDuration", studyId)){
			studyDuration = request.getParameter("studyDuration");
		} else {
			studyDuration = studyJB.getStudyDuration();
		}
		studyDuration=(studyDuration==null)? "" : studyDuration;
		studyDuration=(studyDuration.length()==0)? "0" : studyDuration;
		args.put("studyDuration", studyDuration);

		String studyDurationUnit = null;
		if (findRequestParamter(request, "durationUnit", studyId)){
			studyDurationUnit = request.getParameter("durationUnit");
		} else {
			studyDurationUnit = studyJB.getStudyDurationUnit();
		}
		args.put("studyDurationUnit", studyDurationUnit);
		
		String studyEstBgnDate = null;
		if (findRequestParamter(request, "studyEstBgnDate", studyId)){
			studyEstBgnDate = request.getParameter("studyEstBgnDate");
		} else {
			studyEstBgnDate = studyJB.getStudyEstBeginDate();
		}
		args.put("studyEstBgnDate", studyEstBgnDate);
		
		String studyPhase = null;
		if (findRequestParamter(request, "studyPhase", studyId)){
			studyPhase = request.getParameter("studyPhase");
		} else {
			studyPhase = studyJB.getStudyPhase();
		}
		if(StringUtil.isEmpty(studyPhase)) studyPhase = null;
		args.put("studyPhase", studyPhase);
		
		String studyResType = null;
		if (findRequestParamter(request, "studyResType", studyId)){
			studyResType = request.getParameter("studyResType");
		} else {
			studyResType = studyJB.getStudyResType();
		}
		if(StringUtil.isEmpty(studyResType)) studyResType = null;
		args.put("studyResType", studyResType);
		
		String studyType = null;
		if (findRequestParamter(request, "studyType", studyId)){
			studyType = request.getParameter("studyType");
		} else {
			studyType = studyJB.getStudyType();
		}
		if(StringUtil.isEmpty(studyType))  studyType = null;
		args.put("studyType", studyType);
		
		String studyBlinding = null;
		if (findRequestParamter(request, "studyBlinding", studyId)){
			studyBlinding = request.getParameter("studyBlinding");
		} else {
			studyBlinding = studyJB.getStudyBlinding();
		}
		if(StringUtil.isEmpty(studyBlinding)) studyBlinding = null;
		args.put("studyBlinding", studyBlinding);

		String studyRandom = null;
		if (findRequestParamter(request, "studyRandom", studyId)){
			studyRandom = request.getParameter("studyRandom");
		} else {
			studyRandom = studyJB.getStudyRandom();
		}
		if(StringUtil.isEmpty(studyRandom)) studyRandom = null;
		args.put("studyRandom", studyRandom);

		String studyOtherInfo = null;
		if (findRequestParamter(request, "studyOtherInfo", studyId)){
			studyOtherInfo = request.getParameter("studyOtherInfo");
		} else {
			studyOtherInfo = studyJB.getStudyInfo();
		}
		studyOtherInfo = (studyOtherInfo  == null)?"":(studyOtherInfo ) ;
		if (studyOtherInfo.length() > 2000){
			studyOtherInfo = studyOtherInfo.substring(0,2000);
		}
		args.put("studyOtherInfo", studyOtherInfo);

		String studySponsorName = null;
		if (findRequestParamter(request, "sponsor", studyId)){
			studySponsorName = request.getParameter("sponsor");
		} else {
			studySponsorName = studyJB.getStudySponsorName();
		}
		studySponsorName = (studySponsorName==null)?"":studySponsorName;

		//JM: 26Apr2010: Enh:SW-FIN6
		String studySponsorLkpId = request.getParameter("sponsorpk");
		studySponsorLkpId = (studySponsorLkpId==null)?"":studySponsorLkpId;

		String fromSponsor = null;
		if (findRequestParamter(request, "fromSponsor", studyId)){
			fromSponsor = request.getParameter("fromSponsor");
		} else {
			fromSponsor = studyJB.getStudySponsor();
		}
		fromSponsor = ( fromSponsor==null)?"": fromSponsor;

		if (fromSponsor.equals("DD")){

		}else if (fromSponsor.equals("LKP")){
			studySponsorName = studySponsorLkpId;
		}
		args.put("studySponsorName", studySponsorName);
		args.put("studySponsorLkpId", studySponsorLkpId);

		String studySponsorIdInfo = null;
		if (findRequestParamter(request, "studySponsorIdInfo", studyId)){
			studySponsorIdInfo = request.getParameter("studySponsorIdInfo"); //gopu
		} else {
			studySponsorIdInfo = studyJB.getStudySponsorIdInfo();
		}
		studySponsorIdInfo = (studySponsorIdInfo  == null)?"":(studySponsorIdInfo ) ;
		args.put("studySponsorIdInfo", studySponsorIdInfo);
		
		String studySponsor = null;
		if (findRequestParamter(request, "studySponsor", studyId)){
			studySponsor = request.getParameter("studySponsor");
		} else {
			studySponsor = studyJB.getStudySponsor();
		}
		studySponsor = (studySponsor  == null)?"":(studySponsor) ;
		args.put("studySponsor", studySponsor);
		
		String studyContact = null;
		if (findRequestParamter(request, "studyContact", studyId)){
			studyContact = request.getParameter("studyContact");
		} else {
			studyContact = studyJB.getStudyContact();
		}
		studyContact = (studyContact  == null)?"":(studyContact);
		args.put("studyContact", studyContact);

		String studyKeywrds = null;
		if (findRequestParamter(request, "studyKeywrds", studyId)){
			studyKeywrds = request.getParameter("studyKeywrds");
		} else {
			studyKeywrds = studyJB.getStudyKeywords();
		}
		studyKeywrds = (studyKeywrds  == null)?"":(studyKeywrds);
		if (studyKeywrds.length() > 500)
		{
			studyKeywrds = studyKeywrds.substring(0,500);
		}
		args.put("studyKeywrds", studyKeywrds);

		String dataManager = null;
		if (findRequestParamter(request, "dataManager", studyId)){
			dataManager = request.getParameter("dataManager");
		} else {
			dataManager = studyJB.getStudyAuthor();
		}
		args.put("dataManager", dataManager);
		int iDataManager = StringUtil.stringToNum(dataManager);

		String pubCols ="";
		int totSections = StringUtil.stringToNum(request.getParameter("totPubSections"));

		for(int cnt=1; cnt <= totSections; cnt++){
			String pubcol = "studyPubFlag" +cnt;
			pubCols = pubCols + request.getParameter(pubcol);
		}
		args.put("pubCols", pubCols);

		String studyCurrency = "";
		if (StringUtil.isEmpty(studyCurrency)){
			SchCodeDao scd = new SchCodeDao();
			int curPk = scd.getCodeId("currency","$");
			studyCurrency = String.valueOf(curPk);
		}
		args.put("studyCurrency", studyCurrency);

		String pInvestigator = null;
		if (findRequestParamter(request, "prinInv", studyId)){
			pInvestigator = request.getParameter("prinInv");
		} else {
			pInvestigator = studyJB.getStudyPrimInv();
		}
		args.put("pInvestigator", pInvestigator);
		int iPInvestigator = StringUtil.stringToNum(pInvestigator);

		String studyDivision = null;
		if (findRequestParamter(request, "studyDivision", studyId)){
			studyDivision = request.getParameter("studyDivision");
		} else {
			studyDivision = studyJB.getStudyDivision();
		}
		studyDivision = (StringUtil.isEmpty(studyDivision))? "" : studyDivision;
		args.put("studyDivision", studyDivision);

		String studyCo = null;
		if (findRequestParamter(request, "studyco", studyId)){
			studyCo = request.getParameter("studyco");
		} else {
			studyCo = studyJB.getStudyCoordinator();
		}
		if (StringUtil.isEmpty(studyCo)) studyCo = null;
		args.put("studyCo", studyCo);
		int iStudyCo=StringUtil.stringToNum(studyCo);

		String majAuthor = null;
		if (findRequestParamter(request, "author", studyId)){
			majAuthor = request.getParameter("author");
		} else {
			majAuthor = studyJB.getMajAuthor();
		}
		majAuthor=(majAuthor==null)?"":(majAuthor);
		args.put("majAuthor", majAuthor);

		String studyScope = null;
		if (findRequestParamter(request, "studyScope", studyId)){
			studyScope = request.getParameter("studyScope");
		} else {
			studyScope = studyJB.getStudyScope();
		}
		studyScope=(studyScope==null)?"":(studyScope);
		args.put("studyScope", studyScope);

		String studyAssoc = null;
		if (findRequestParamter(request, "studyAssoc", studyId)){
			studyAssoc = request.getParameter("studyAssoc");
		} else {
			studyAssoc = studyJB.getStudyAssoc();
		}
		studyAssoc=(studyAssoc==null)?"":(studyAssoc);
		args.put("studyAssoc", studyAssoc);

		String disSite = null;
		if (findRequestParamter(request, "disSiteid", studyId)){
			disSite = request.getParameter("disSiteid");
		} else {
			disSite = studyJB.getDisSite();
		}
		disSite=(disSite==null)?"":(disSite);
		args.put("disSite", disSite);
		
		String prinvOther = null;
		if (findRequestParamter(request, "prinvOther", studyId)){
			prinvOther = request.getParameter("prinvOther");
		} else {
			prinvOther = studyJB.getStudyOtherPrinv();
		}
		args.put("prinvOther", prinvOther);
		
		String nStudySize = null;
		if (findRequestParamter(request, "nStudySize", studyId)){
			nStudySize = request.getParameter("nStudySize");
		} else {
			nStudySize = studyJB.getStudyNSamplSize();
		}
		nStudySize=(StringUtil.isEmpty(nStudySize))?"0":nStudySize;
		args.put("nStudySize", nStudySize);
		
		String invFlag = "0";
		String cbInv = request.getParameter("cbInv");
		if (StringUtil.isEmpty(cbInv))
		{
			invFlag = "0";
		}
		else if(cbInv.equals("on"))
		{
			invFlag = "1";
		}
		args.put("invFlag", invFlag);
		
		String indIdeNum = request.getParameter("indIdeNum");
		args.put("indIdeNum", indIdeNum);
		
		String studyICDcode1 = null; 
		if (findRequestParamter(request, "ICDcode1", studyId)){
			studyICDcode1 = request.getParameter("ICDcode1");
		} else {
			studyICDcode1 = studyJB.getStudyICDCode1();
		}
		args.put("studyICDcode1", studyICDcode1);

		String studyICDcode2 = null;
		if (findRequestParamter(request, "ICDcode2", studyId)){
			studyICDcode2 = request.getParameter("ICDcode2");
		} else {
			studyICDcode2 = studyJB.getStudyICDCode2();
		}
		args.put("studyICDcode2", studyICDcode2);
		
		/*String studyICDcode3 = "";*/
		String ctrp="";
		ctrp = request.getParameter("ctrp");
		String ctrpReportFlag=request.getParameter("ctrpReportFlag")==null?"":request.getParameter("ctrpReportFlag");
		if(ctrpReportFlag.equals("ctrpReptFlag")){
		if (StringUtil.isEmpty(ctrp))
		{
			ctrp = "0";
		}
		else if(ctrp.equals("on"))
		{
			ctrp = "1";
		}
		}
		else{
			ctrp = studyJB.getStudyCtrpReportable();
		}
		args.put("ctrp", ctrp);

		String fda = request.getParameter("fda");//INF-22247 27-Feb-2012 @Ankit
		//INF-22247 27-Feb-2012 @Ankit
		String fdaRegFlag=request.getParameter("fdaRegFlag")==null?"":request.getParameter("fdaRegFlag");
		if(fdaRegFlag.equals("fdaFlag")){
		if (StringUtil.isEmpty(fda))
		{
			fda = "0";
		}
		else if(fda.equals("on"))
		{
			fda = "1";
		}
		}
		else{
			fda=studyJB.getFdaRegulatedStudy();
		}
		args.put("fda", fda);

		String ccsgdt = request.getParameter("ccsgdt");//INF-22247 27-Feb-2012 @Ankit
		String CcsgReportFlag=request.getParameter("CcsgReportFlag")==null?"":request.getParameter("CcsgReportFlag");
		if(CcsgReportFlag.equals("CcsgRprtFlag")){
		if (StringUtil.isEmpty(ccsgdt))
		{
			ccsgdt = "0";
		}
		else if(ccsgdt.equals("on"))
		{
			ccsgdt = "1";
		}
		}else{
			ccsgdt=studyJB.getCcsgReportableStudy();	
		}
		args.put("ccsgdt", ccsgdt);
		
			
		/*String nciTrialIdentifier = request.getParameter("NCITrialIdentifier");
		nciTrialIdentifier = ( nciTrialIdentifier==null)?"": nciTrialIdentifier;*/
		String nciTrialIdentifier = "";
		if (findRequestParamter(request, "NCITrialIdentifier", studyId)){
			nciTrialIdentifier = request.getParameter("NCITrialIdentifier");
		} else {
			nciTrialIdentifier = studyJB.getNciTrialIdentifier();
		}
		nciTrialIdentifier = ( nciTrialIdentifier==null)?"": nciTrialIdentifier;
		args.put("nciTrialIdentifier", nciTrialIdentifier);
		
		String nctNumber = "";
		if (findRequestParamter(request, "nctNumber", studyId)){
			nctNumber = request.getParameter("nctNumber");
		} else {
			nctNumber = studyJB.getNctNumber();
		}
		nctNumber = ( nctNumber==null)?"": nctNumber;
		args.put("nctNumber", nctNumber);
		
		String userList = "";

     	if(iDataManager != iPInvestigator && (iPInvestigator != 0))
     		userList = iDataManager +","+ iPInvestigator;
     	else
     		userList = String.valueOf(iDataManager);

		if ((iStudyCo!=0) && (iStudyCo!=iPInvestigator) && (iStudyCo!=iDataManager))
			userList=userList+","+iStudyCo;

		UserJB userB = new UserJB();
		UserDao usrD = new UserDao();
		usrD = userB.getUsersDetails(userList);

		ArrayList aSites = usrD.getUsrSiteIds();
		ArrayList arrSites = new ArrayList();
		for(int i=0;i<aSites.size();i++){
			if(i==0){
				arrSites.add(aSites.get(i).toString());
			}
			if(!arrSites.contains(aSites.get(i).toString()))
				arrSites.add(aSites.get(i).toString());
		}
		args.put("aSites", aSites);
		args.put("arrSites", arrSites);
		String myCreateType = StringUtil.trueValue(request.getParameter("createType"));
		if (NULL_STR.equals(myCreateType)) {
			myCreateType = null;
		}
		if (LIND_STR.equals(CFG.EIRB_MODE)) {
			String notForIRB = StringUtil.trueValue(request.getParameter("notForIRB"));
			if (!StringUtil.isEmpty(notForIRB)) {
				myCreateType = null;
			} else {
				myCreateType = A_STR;
			}
		}
		args.put("createType", myCreateType);
		return args;
	}
	
	public int updateStudy(){
		HashMap<String, Object> args = new HashMap<String, Object>();
		try{
			args = loadParameterMap();
		} catch (IllegalArgumentException e){
			if (ESIGN_DOES_NOT_MATCH.equals(e.getMessage())){
				return -1;
			}
		}
		int success = updateStudyData(args);
		
		if(success != -3) {
			String studyTArea = studyJB.getStudyTArea();
			HttpSession tSession = request.getSession(true);
			tSession.setAttribute("studyTArea",studyTArea);
		}
		
		String indIdeGrid = (String) request.getParameter("cbIndIdeGrid");
		if (StringUtil.isEmpty(indIdeGrid))
		{
			indIdeGrid = "0";
		}
		else if(indIdeGrid.equals("on"))
		{
			indIdeGrid = "1";
		}
		if(success != -3 && indIdeGrid !=null && studyJB.getId()>0){
			updateStudyINDIDEData();
		}
		args.put("indIdeGrid", indIdeGrid); 

		String nihGrantChkValue = (String) request.getParameter("nihGrantChkValue");
		if (StringUtil.isEmpty(nihGrantChkValue)) nihGrantChkValue = "0";
		if(success>=0 && nihGrantChkValue.equals("1") && studyJB.getId()>0){
			createUpdateNIHGrantData();
		}

		if(success >= 0){
			updateStudySectionData();
		}

		return success;
	}

	private int updateStudyData(HashMap<String, Object> args){
		int rows = 0;
		StudySiteDao studySiteDao = new StudySiteDao();
		String addSite = "";
		
		String studyId = (String) args.get("studyId");
		
		String accId = (String) args.get("accId");
		int iAccId = StringUtil.stringToNum(accId);

		String userId = (String) args.get("userId");
		int iUserId = StringUtil.stringToNum(userId);
		
		String ipAdd = (String) args.get("ipAdd");

		ArrayList arrSites = (ArrayList) args.get("arrSites");
		for(int cnt =0 ; cnt < arrSites.size() ; cnt++){
			addSite = (arrSites.get(cnt)).toString();

			studySiteDao = studySiteJB.getCntForSiteInStudy(StringUtil.stringToNum(studyId), StringUtil.stringToNum(addSite));
			rows = studySiteDao.getCRows();

			// add the organization in studysites if this organization is not present for this study in er_studysites
			if(rows == 0 && StringUtil.stringToNum(studyId)>0){
				studySiteJB.setSiteId(addSite);
				studySiteJB.setStudyId(studyId);
				studySiteJB.setCreator(userId);
				studySiteJB.setRepInclude("1");
				studySiteJB.setIpAdd(ipAdd);
				studySiteJB.setStudySiteDetails();
			}
		}

		int usrCnt = 0;
		TeamDao teamDao = new TeamDao();
		String dataManager = (String)args.get("dataManager");
		int iDataManager = StringUtil.stringToNum(dataManager);
		
		String pInvestigator = (String)args.get("pInvestigator");
		int iPInvestigator = StringUtil.stringToNum(pInvestigator);
		
		int piRole = StringUtil.stringToNum((String)args.get("piRole"));

		String teamStatusActiveId = (String)args.get("teamStatusActiveId");

		String statusDate = (String)args.get("statusDate");
		
		// add user study site rights if the user was not in the study team earlier
		if(iDataManager != iPInvestigator && (iPInvestigator != 0) && StringUtil.stringToNum(studyId)>0){
			teamDao  = teamJB.findUserInTeam(StringUtil.stringToNum(studyId), iDataManager);
			usrCnt = teamDao.getCRows();

			if(usrCnt == 0){
				StdSiteRightsJB stdSiteRightsB = new StdSiteRightsJB();
				stdSiteRightsB.createStudySiteRightsData(iDataManager, iAccId, StringUtil.stringToNum(studyId), iUserId, ipAdd );
			}
			teamDao = teamJB.findUserInTeam(StringUtil.stringToNum(studyId), iPInvestigator);
			usrCnt = teamDao.getCRows();

			if(usrCnt == 0){
				//add PI since there is no record in er_studyteam//////////////////
				teamJB = new TeamJB();
				teamJB.setTeamUser(String.valueOf(iPInvestigator));
				teamJB.setStudyId(studyId);
				teamJB.setTeamUserType("D");
				teamJB.setTeamUserRole(String.valueOf(piRole));
				teamJB.setCreator(userId);
				teamJB.setIpAdd(ipAdd);
				
				//Added by Manimaran for the July-August Enhancement S4.
				teamJB.setTeamStatus("Active");
				teamJB.setTeamDetails();
				
				statusJB = new StatusHistoryJB();
				statusJB.setStatusModuleId(String.valueOf(teamJB.getId()));
				statusJB.setStatusModuleTable("er_studyteam");
				statusJB.setStatusCodelstId(teamStatusActiveId);
				statusJB.setStatusStartDate(statusDate);
				statusJB.setCreator(userId);
				statusJB.setIpAdd(ipAdd);
				statusJB.setModifiedBy(userId);
				statusJB.setRecordType("N");
				statusJB.setIsCurrentStat("1");
				statusJB.setStatusHistoryDetailsWithEndDate();

				///////////////////////////////////
				StdSiteRightsJB stdSiteRightsB = new StdSiteRightsJB();
				stdSiteRightsB.createStudySiteRightsData(iPInvestigator, iAccId, StringUtil.stringToNum(studyId), iUserId, ipAdd);
			}

		}
		else if (iDataManager == iPInvestigator || iPInvestigator == 0)
		{
			teamDao = teamJB.findUserInTeam(StringUtil.stringToNum(studyId), iDataManager);
			usrCnt = teamDao.getCRows();
			if(usrCnt == 0 && StringUtil.stringToNum(studyId)>0)
			{
				stdSiteRightsB.createStudySiteRightsData(iDataManager, iAccId, StringUtil.stringToNum(studyId), iUserId, ipAdd);
			}

		}

		studyJB.setId(StringUtil.stringToNum(studyId));
		studyJB.getStudyDetails();
		request.getSession(true).setAttribute("createType", (String)args.get("createType")); // used for redirection
		//Retrieve the previous values for
		//these parameter, Studyteam, is refreshed based
		// on , if they are changed using filters
		//defined in super user rights page.
		String prevTarea=studyJB.getStudyTArea();
		String prevDisSite=studyJB.getDisSite();
		String prevStudyDivision=studyJB.getStudyDivision();

		if (StringUtil.isEmpty(prevTarea))
			prevTarea = "";

		if (StringUtil.isEmpty(prevDisSite))
			prevDisSite = "";

		if (StringUtil.isEmpty(prevStudyDivision))
			prevStudyDivision = "";

		String studyNumber = (String)args.get("studyNumber");
		String studyTitle = (String)args.get("studyTitle");
		String studyObjective = (String) args.get("studyObjective");
		String studySummary = (String) args.get("studySummary");
		String studyProduct = (String) args.get("studyProduct");
		String studyTArea = (String) args.get("studyTArea");
		String studyPurpose = (String) args.get("studyPurpose");
		String studyDuration = (String) args.get("studyDuration");
		String studyDurationUnit = (String) args.get("studyDurationUnit");
		String studyEstBgnDate = (String) args.get("studyEstBgnDate");
		String studyPhase = (String) args.get("studyPhase");
		String studyResType = (String) args.get("studyResType");
		String studyType = (String) args.get("studyType");
		String studyBlinding = (String) args.get("studyBlinding");
		String studyRandom = (String) args.get("studyRandom");
		String studyOtherInfo = (String) args.get("studyOtherInfo");
		String studySponsorName = (String) args.get("studySponsorName");
		String studySponsorIdInfo = (String) args.get("studySponsorIdInfo");
		String studySponsorLkpId = (String) args.get("studySponsorLkpId");
		
		String studyCo = (String) args.get("studyCo");
		int iStudyCo=StringUtil.stringToNum(studyCo);
		
		String studyKeywrds = (String) args.get("studyKeywrds");
		String studyContact = (String) args.get("studyContact");
		String studySponsor = (String) args.get("studySponsor");
		String pubCols = (String) args.get("pubCols");
		String studyPubFlag = "N";//(String) args.get("studyPubFlag");
		String studyCurrency = (String) args.get("studyCurrency");
		String majAuthor = (String) args.get("majAuthor");
		String studyScope = (String) args.get("studyScope");
		String studyAssoc = (String) args.get("studyAssoc");
		
		String disSite = (String) args.get("disSite");
		String prinvOther = (String) args.get("prinvOther");
		String nStudySize = (String) args.get("nStudySize");
		String studyDivision = (String) args.get("studyDivision");
		String invFlag = (String) args.get("invFlag");
		String indIdeNum = (String) args.get("indIdeNum");
		String studyICDcode1 = (String) args.get("studyICDcode1");
		String studyICDcode2 = (String) args.get("studyICDcode2");

		String ctrp = (String) args.get("ctrp");
		String fda = (String) args.get("fda");
		String ccsgdt = (String) args.get("ccsgdt");
		String nciTrialIdentifier = (String) args.get("nciTrialIdentifier");
		String nctNumber = (String) args.get("nctNumber");
		String createType = (String) args.get("createType");

		studyJB.setStudyNumber(studyNumber);
		studyJB.setStudyTitle(StringUtil.stripScript(studyTitle));
		studyJB.setStudyObjective(studyObjective);
		studyJB.setStudySummary(studySummary);
		studyJB.setStudyProduct(studyProduct);
		studyJB.setStudyTArea(studyTArea);
		studyJB.setStudyPurpose(studyPurpose);
		//studyJB.setStudySize(studySize);
		studyJB.setStudyDuration(studyDuration);
		studyJB.setStudyDurationUnit(studyDurationUnit);
		studyJB.setStudyEstBeginDate(studyEstBgnDate);
		studyJB.setStudyPhase(studyPhase);
		studyJB.setStudyResType(studyResType);
		studyJB.setStudyType(studyType);
		studyJB.setStudyBlinding(studyBlinding);
		studyJB.setStudyRandom(studyRandom);
		studyJB.setStudyInfo(studyOtherInfo);
		studyJB.setStudySponsorName(studySponsorName);//gopu
		studyJB.setStudySponsorIdInfo(studySponsorIdInfo); //gopu
		studyJB.setStudySponsor(studySponsorLkpId);

		//studyJB.setStudyPartCenters(studyPartCenters);
		studyJB.setStudyPrimInv(pInvestigator);
		studyJB.setStudyCoordinator(studyCo);
		studyJB.setStudyKeywords(studyKeywrds);
		studyJB.setStudyContact(studyContact);
		studyJB.setStudySponsor(studySponsor);
		studyJB.setStudyAuthor(dataManager);
		studyJB.setStudyPubCol(pubCols);
		studyJB.setStudyPubFlag(studyPubFlag);
		studyJB.setModifiedBy(userId);
		studyJB.setIpAdd(ipAdd);
		studyJB.setStudyCurrency(studyCurrency);
		studyJB.setMajAuthor(majAuthor);
		studyJB.setStudyScope(studyScope);
		studyJB.setStudyAssoc(studyAssoc);
		studyJB.setDisSite(disSite);
		studyJB.setStudyOtherPrinv(prinvOther);
		studyJB.setStudyNSamplSize(nStudySize);
		studyJB.setStudyDivision(studyDivision);
		studyJB.setStudyInvIndIdeFlag(invFlag);
		studyJB.setStudyIndIdeNum(indIdeNum);
		studyJB.setStudyICDCode1(studyICDcode1);//km
		studyJB.setStudyICDCode2(studyICDcode2);
		/*studyJB.setStudyICDCode3(studyICDcode3);*/
		studyJB.setStudyCtrpReportable(ctrp);
		studyJB.setCcsgReportableStudy(ccsgdt);
		studyJB.setFdaRegulatedStudy(fda);//INF-22247 27-Feb-2012 @Ankit
		
		studyJB.setNciTrialIdentifier(nciTrialIdentifier);
		studyJB.setNctNumber(nctNumber);
		studyJB.setStudyCreationType(createType);

		int success = studyJB.updateStudy();
		
		
		// first add its organization if it is not in er_studysite for this study

		// Then add users access rights in er_study_site_rights

		//add in stduy team
		//Coordinator role
		String coRole="";
		CodeDao cdCoRole=new CodeDao();
		coRole=String.valueOf(cdCoRole.getCodeId("role","role_coord"));

		
		if (iStudyCo>0 && StringUtil.stringToNum(studyId)>0){
			usrCnt = 0;
			teamDao = new TeamDao();
			teamDao  = teamJB.findUserInTeam(StringUtil.stringToNum(studyId), iStudyCo);
				usrCnt=teamDao.getCRows();
			if (usrCnt==0){
				teamJB.setTeamUser(studyCo);
				teamJB.setStudyId(studyId);
				teamJB.setTeamUserType("D");
				teamJB.setTeamUserRole(coRole);
				teamJB.setCreator(userId);
				teamJB.setIpAdd(ipAdd);

				//Added by Manimaran for the July-August Enhancement S4.
				teamJB.setTeamStatus("Active");
				teamJB.setTeamDetails();

				int id = teamJB.getId();
				String moduleId = id+"";

				statusJB.setStatusModuleId(moduleId);
				statusJB.setStatusModuleTable("er_studyteam");
				statusJB.setStatusCodelstId(teamStatusActiveId);
				statusJB.setStatusStartDate(statusDate);
				statusJB.setCreator(userId);
				statusJB.setIpAdd(ipAdd);
				statusJB.setModifiedBy(userId);
				statusJB.setRecordType("N");
				statusJB.setIsCurrentStat("1");
				statusJB.setStatusHistoryDetailsWithEndDate();
				stdSiteRightsB.createStudySiteRightsData(iStudyCo, iAccId, StringUtil.stringToNum(studyId), iUserId, ipAdd );
			}
		}

		/* commented by sonia, 10/22/07. changed the implementation of superuser
			if (success>=0)
			{

			if ((!prevTarea.equals(studyTArea)) || (!prevStudyDivision.equals(studyDivision))
			|| (!prevDisSite.equals(disSite)))
			{
			GroupDao gDao=groupB.getGroupDaoInstance();
			success=gDao.revokeSupUserForStudy(StringUtil.stringToNum(studyId),accId);
			if (success>=0) ret=gDao.grantSupUserForStudy(accId , studyId , usr ,ipAdd);

			}
			}
			*/
		return success;
		
	}

	private int updateStudySectionData(){
		try {
			String[] sectionContents = request.getParameterValues("sectionContentsta");
			if (sectionContents.length > 0){
				int studyId = studyJB.getId();
				if (studyId <= 0) { return -1; }

				HttpSession tSession = request.getSession();
				int userId = StringUtil.stringToNum((String) tSession.getAttribute("userId"));
				String ipAdd = (String) tSession.getAttribute("ipAdd");

				String[] sectionNames = request.getParameterValues("sectionName");
				String[] sectionIds = request.getParameterValues("sectionId");

				CodeDao cdVerCat=new CodeDao();
				int studyVerCategoryId = cdVerCat.getCodeId("studyvercat", "protocol");

				int nextVer = 0; nextVer++;//IMP
				int studyVersionPK = 0;
				if(("LIND".equals(CFG.EIRB_MODE))) {					
					if (null != studyJB){
						studyId = studyJB.getId();
						String nextVerStr="0.00";
						StudyVerDao svdao = new StudyVerDao();
						StudyVerJB studyVerB = new StudyVerJB();
						int pkStudyVer = svdao.checkVersionExists(studyId, String.valueOf(nextVer));
						if(pkStudyVer > 0 ) {
							studyVersionPK = pkStudyVer;
						} else {
							//Create a version=0 with category=protocol only for saving studySection data in er_studysec table which is associated with fk_studyver.
							studyVersionPK = studyVerB.newStudyVersion(studyId, ""+nextVerStr, "", DateUtil.getCurrentDate(), ""+studyVerCategoryId, "", userId);
						}	
					}
				} else {
					if (null != studyJB){
						studyId = studyJB.getId();
	
						UIFlxPageDao uiFlxPageDao = new UIFlxPageDao();
						nextVer = uiFlxPageDao.getHighestFlexPageVersion("er_study", studyId);
						nextVer++;//IMP
	
						StudyVerAgentRObj studyVerAgent = EJBUtil.getStudyVerAgentHome();
						studyVersionPK = studyVerAgent.findStudyVersion(studyId, nextVer, studyVerCategoryId);
	
						if (studyVersionPK <= 0){
							//Create a new version with category=protocol
							StudyVerJB studyVerB = new StudyVerJB();
							studyVersionPK = studyVerB.newStudyVersion(studyId, ""+nextVer, "", DateUtil.getCurrentDate(), ""+studyVerCategoryId, "", userId);
						}
					}
				}
				
				for (int secIndex = 0; secIndex < sectionContents.length; secIndex++){
					int id = StringUtil.stringToNum(sectionIds[secIndex]);

					String sectionContent = sectionContents[secIndex];
					String studySectionType = sectionNames[secIndex];
					
					/*if (id == 0 && StringUtil.isEmpty(sectionContent)){
						continue;//Do not create a blank section
					}*/
						
					SectionJB sectionB = new SectionJB();
					if (id > 0){
						sectionB.setId(id);
						sectionB.getSectionDetails();
					}

					sectionB.setSecStudy(""+studyId);
					sectionB.setSecStudyVer(String.valueOf(studyVersionPK));
					sectionB.setContents(sectionContent);
					sectionB.setSecName(studySectionType);

					if (id > 0){
						sectionB.setModifiedBy(""+userId);
						sectionB.setIpAdd(ipAdd);
						sectionB.updateSection();
					} else {
						sectionB.setSecSequence("0");
						sectionB.setCreator(""+userId);
						sectionB.setIpAdd(ipAdd);	
						sectionB.setSectionDetails();
					}
				}
			}
			return 1;
		} catch (Exception e){
			return 0;
		}
	}
	
	private int updateStudyINDIDEData(){
		try {
			HttpSession tSession = request.getSession(true);
			String ipAdd = (String) tSession.getAttribute("ipAdd");
			String userId = (String) tSession.getAttribute("userId");
	
			//Enhancement CTRP-20527 : AGodara
			String [] pKStudyIndIde = null;
			String [] indIdeType = null;
			String [] indIdeNumber = null;
			String [] indIdeGrantors = null;
			String [] indIdeHolders = null;
			String [] indIdeProgramCodes = null;
			String [] indIdeExpandedAccess = null;
			String [] indIdeAccessType = null;
			String [] indIdeExempt = null;

			String deletedIndIdeRows = null;
			String numIndIdeRows ="";
			// Start CTRP-20527(IND/IDE Info)  :AGodara 
	
			// number of data rows
			numIndIdeRows = request.getParameter("numIndIdeRows");
				
			pKStudyIndIde = request.getParameterValues("pKStudyIndIde");
			indIdeType = request.getParameterValues("indIdeType");
			indIdeNumber = request.getParameterValues("indIdeNumber");
			indIdeGrantors = request.getParameterValues("indIdeGrantor");
			indIdeHolders = request.getParameterValues("indIdeHolder");
			indIdeProgramCodes = request.getParameterValues("indIdeProgramCode");
			
			//chkbx
			indIdeExpandedAccess = request.getParameterValues("cbExpandedAccessFlag");
			
			indIdeAccessType = request.getParameterValues("indIdeAccessType");
			
			//chkbx
			indIdeExempt = request.getParameterValues("cbIndIdeExemptFlag");
			
			deletedIndIdeRows = request.getParameter("deletedIndIdeRows");
	
			if(deletedIndIdeRows!=null && !deletedIndIdeRows.isEmpty()){
				for(String s:deletedIndIdeRows.split(",")){
					if(!s.isEmpty() && !s.equals("0"))
						studyIndIdeJB.deleteIndIdeInfo(StringUtil.stringToNum(s),AuditUtils.createArgs(tSession,"",LC.L_Study));
				}
			}
				
			for(int i=0;i< StringUtil.stringToNum(numIndIdeRows);i++){
				studyIndIdeJB.setFkStudy(StringUtil.integerToString(studyJB.getId()));
	
				studyIndIdeJB.setTypeIndIde(indIdeType[i]);
				studyIndIdeJB.setNumberIndIde(indIdeNumber[i]);
				studyIndIdeJB.setFkIndIdeGrantor(indIdeGrantors[i]);
				studyIndIdeJB.setFkIndIdeHolder(indIdeHolders[i]);
				studyIndIdeJB.setFkProgramCode(indIdeProgramCodes[i]);
				studyIndIdeJB.setExpandIndIdeAccess(indIdeExpandedAccess[i]);
				studyIndIdeJB.setFkAccessType(indIdeAccessType[i]);
				studyIndIdeJB.setExemptINDIDE(indIdeExempt[i]);
				studyIndIdeJB.setIpAdd(ipAdd);
				
				if( pKStudyIndIde[i]!= null && StringUtil.stringToNum(pKStudyIndIde[i])!=0 ){
					studyIndIdeJB.setPkStudyINDIDE(StringUtil.stringToNum(pKStudyIndIde[i]));
					studyIndIdeJB.setModifiedBy(userId);
					studyIndIdeJB.updateIndIdeInfo();
				}else{
					studyIndIdeJB.setModifiedBy(null);      //Added to fix Bug#7994 and 7960 : Raviesh
					studyIndIdeJB.setCreator(userId);
					studyIndIdeJB.setIndIdeInfo();
				}
			}
			// End CTRP-20527(IND/IDE Info)  :AGodara 
			return 1;
		} catch (Exception e){
			return 0;
		}
	}

	private int createStudy(){
		System.out.println("Inside createStudy");
		HashMap<String, Object> args = new HashMap<String, Object>();
		try{
			args = loadParameterMap();
		} catch (IllegalArgumentException e){
			if (ESIGN_DOES_NOT_MATCH.equals(e.getMessage())){
				return -1;
			}
		}
		
		int success = createStudyData(args);
		if (success > 0){
			request.getSession(true).setAttribute("studyId", ""+success);
			request.getSession(true).setAttribute("createType", request.getParameter("createType"));
		} else {
			request.getSession(true).removeAttribute("studyId");
			request.getSession(true).removeAttribute("createType");
		}
		
		if(success >= 0){
			updateStudySectionData();
			String userId = (String) args.get("userId");
			WorkflowDAO wdao= new WorkflowDAO();
			wdao.createWorkflow(String.valueOf(success),userId);
		}
		
		return success;
	}

	private int createStudyData(HashMap<String, Object> args){
		String studyId = "";
		
		String accId = (String) args.get("accId");
		int iAccId = StringUtil.stringToNum(accId);

		String userId = (String) args.get("userId");
		int iUserId = StringUtil.stringToNum(userId);
		
		String ipAdd = (String) args.get("ipAdd");

		String studyNumber = (String)args.get("studyNumber");
		String studyTitle = (String)args.get("studyTitle");
		String studyObjective = (String) args.get("studyObjective");
		String studySummary = (String) args.get("studySummary");
		String studyProduct = (String) args.get("studyProduct");
		String studyTArea = (String) args.get("studyTArea");
		String studyDuration = (String) args.get("studyDuration");
		String studyDurationUnit = (String) args.get("studyDurationUnit");
		String studyEstBgnDate = (String) args.get("studyEstBgnDate");
		String studyPhase = (String) args.get("studyPhase");
		String studyPurpose = (String) args.get("studyPurpose");
		String studyResType = (String) args.get("studyResType");
		String studyType = (String) args.get("studyType");
		String studyBlinding = (String) args.get("studyBlinding");
		String studyRandom = (String) args.get("studyRandom");
		String studyOtherInfo = (String) args.get("studyOtherInfo");
		String studySponsorName = (String) args.get("studySponsorName");
		String studySponsorIdInfo = (String) args.get("studySponsorIdInfo");
		
		String studyKeywrds = (String) args.get("studyKeywrds");
		String studyContact = (String) args.get("studyContact");
		String studySponsor = (String) args.get("studySponsor");
		String pubCols = (String) args.get("pubCols");
		String studyPubFlag = "N";//(String) args.get("studyPubFlag");
		String studyCurrency = (String) args.get("studyCurrency");
		String majAuthor = (String) args.get("majAuthor");
		String studyScope = (String) args.get("studyScope");
		String studyAssoc = (String) args.get("studyAssoc");
		
		String dataManager = (String)args.get("dataManager");
		dataManager =  (StringUtil.isEmpty(dataManager))? userId : dataManager;
		int iDataManager = StringUtil.stringToNum(dataManager);
		iDataManager =  (iDataManager <= 0)? iUserId : iDataManager;

		String pInvestigator = (String)args.get("pInvestigator");
		//pInvestigator =  (StringUtil.isEmpty(pInvestigator))? userId : pInvestigator;
		pInvestigator =  (StringUtil.isEmpty(pInvestigator))? "" : pInvestigator;
		int iPInvestigator = StringUtil.stringToNum(pInvestigator);
		//iPInvestigator =  (iDataManager <= 0)? iUserId : iPInvestigator;
		iPInvestigator =  (iDataManager <= 0)? 0 : iPInvestigator;

		String studyCo = (String) args.get("studyCo");
		int iStudyCo=StringUtil.stringToNum(studyCo);

		String disSite = (String) args.get("disSite");
		String prinvOther = (String) args.get("prinvOther");
		String nStudySize = (String) args.get("nStudySize");
		String studyDivision = (String) args.get("studyDivision");
		String invFlag = (String) args.get("invFlag");
		String indIdeNum = (String) args.get("indIdeNum");
		String studyICDcode1 = (String) args.get("studyICDcode1");
		String studyICDcode2 = (String) args.get("studyICDcode2");

		String ctrp = (String) args.get("ctrp");
		String fda = (String) args.get("fda");
		String ccsgdt = (String) args.get("ccsgdt");
		String nciTrialIdentifier = (String) args.get("nciTrialIdentifier");
		String nctNumber = (String) args.get("nctNumber");
		String createType = (String) args.get("createType");

		studyJB.setAccount(accId);
		studyJB.setStudyNumber(studyNumber);
		studyJB.setStudyTitle(studyTitle);
		studyJB.setStudyObjective(studyObjective);
		studyJB.setStudySummary(studySummary);
		studyJB.setStudyProduct(studyProduct);
		studyJB.setStudyTArea(studyTArea);
		//studyJB.setStudySize(studySize);
		studyJB.setStudyDuration(studyDuration);
		studyJB.setStudyDurationUnit(studyDurationUnit);
		studyJB.setStudyEstBeginDate(studyEstBgnDate);
		studyJB.setStudyPhase(studyPhase);
		studyJB.setStudyPurpose(studyPurpose);
		studyJB.setStudyResType(studyResType);
		studyJB.setStudyType(studyType);
		studyJB.setStudyBlinding(studyBlinding);
		studyJB.setStudyRandom(studyRandom);
		studyJB.setStudyInfo(studyOtherInfo);
		studyJB.setStudySponsorName(studySponsorName); //gopu
		studyJB.setStudySponsorIdInfo(studySponsorIdInfo);//gopu

		//studyJB.setStudyPartCenters(studyPartCenters);

		studyJB.setStudyKeywords(studyKeywrds);
		studyJB.setStudyContact(studyContact);
		studyJB.setStudySponsor(studySponsor);
		studyJB.setStudyAuthor(dataManager);
		studyJB.setStudyPubCol(pubCols);
		studyJB.setStudyPubFlag(studyPubFlag);
		studyJB.setCreator(userId);
		studyJB.setIpAdd(ipAdd);
		studyJB.setStudyCurrency(studyCurrency);
		studyJB.setMajAuthor(majAuthor);
		studyJB.setStudyScope(studyScope);
		studyJB.setStudyAssoc(studyAssoc);
		studyJB.setDisSite(disSite);
		studyJB.setStudyPrimInv(pInvestigator);
		studyJB.setStudyCoordinator(studyCo);
		studyJB.setStudyOtherPrinv(prinvOther);
		studyJB.setStudyNSamplSize(nStudySize);
		studyJB.setStudyDivision(studyDivision);
		studyJB.setStudyInvIndIdeFlag(invFlag);
		studyJB.setStudyIndIdeNum(indIdeNum);
		studyJB.setStudyICDCode1(studyICDcode1);//km
		studyJB.setStudyICDCode2(studyICDcode2);
		/*studyJB.setStudyICDCode3(studyICDcode3);*/
		//??studyJB.setStudyCreationType(isIrb?"A":"D");
		studyJB.setStudyCtrpReportable(ctrp);
		studyJB.setCcsgReportableStudy(ccsgdt);
		studyJB.setFdaRegulatedStudy(fda); // Bug#10130 18-June-2012 @Ankit
		
		studyJB.setNciTrialIdentifier(nciTrialIdentifier);
		studyJB.setNctNumber(nctNumber);
		studyJB.setStudyCreationType(createType);
		
		int success = studyJB.setStudyDetails();

		String indIdeGrid = (String) request.getParameter("cbIndIdeGrid");
		if(success != -3 && indIdeGrid !=null && studyJB.getId()>0){
			createStudyINDIDEData();
		}
	
		String nihGrantChkValue = (String) request.getParameter("nihGrantChkValue");
		if (StringUtil.isEmpty(nihGrantChkValue)) nihGrantChkValue = "0";
		if(success != -3 && nihGrantChkValue.equals("1") && studyJB.getId()>0){
			createUpdateNIHGrantData();
		}
		
		if(success != -3 && studyJB.getId()>0) {
			int piRole = StringUtil.stringToNum((String)args.get("piRole"));

			String teamStatusActiveId = (String)args.get("teamStatusActiveId");

			String statusDate = (String)args.get("statusDate");
			
			/*CtrlDao ctrl = new CtrlDao();
			ctrl.getControlValues("author");
			String role = (String) ctrl.getCValue().get(0);*/
			int dmRole = 0;
			CodeDao cdDMRole = new CodeDao();
			dmRole = cdDMRole.getCodeId("role","role_dm");

			String dmRoleStr = String.valueOf(dmRole);
			int uteamId = 0;
			//add logged in user to study team if he selects a different data manager
			//	int iDataManager = StringUtil.stringToNum(dataManager);
			int curUser = StringUtil.stringToNum(userId);
			//	int iPInvestigator = StringUtil.stringToNum(pInvestigator);

			String finalRole = "";
			if (iDataManager == iPInvestigator){
				finalRole = String.valueOf(piRole);
			} else {
				finalRole = dmRoleStr;
			}

			studyId = (new Integer(studyJB.getId())).toString();

			HttpSession tSession = request.getSession(true);
			tSession.setAttribute("studyId",studyId);
			tSession.setAttribute("studyTArea",studyTArea);

			teamJB.setTeamUser(dataManager);
			teamJB.setStudyId(studyId);
			teamJB.setTeamUserType("D");
			teamJB.setTeamUserRole(finalRole);
			teamJB.setTeamStatus("Active");//km
			teamJB.setCreator(userId);
			teamJB.setIpAdd(ipAdd);
			teamJB.setTeamDetails();

			//Added by Manimaran for the July-August Enhancement S4.
			int id = teamJB.getId();
			String moduleId = id+"";

			statusJB.setStatusModuleId(moduleId);
			statusJB.setStatusModuleTable("er_studyteam");
			statusJB.setStatusCodelstId(teamStatusActiveId);
			statusJB.setStatusStartDate(statusDate);
			statusJB.setCreator(userId);
			statusJB.setIpAdd(ipAdd);
			statusJB.setModifiedBy(userId);
			statusJB.setRecordType("N");
			statusJB.setIsCurrentStat("1");
			statusJB.setStatusHistoryDetailsWithEndDate();
			stdSiteRightsB.createStudySiteRightsData(iDataManager, iAccId, StringUtil.stringToNum(studyId), iUserId, ipAdd );

			if ((iDataManager != iPInvestigator) && (iPInvestigator != 0))
			{
				teamJB.setTeamUser(pInvestigator);
				teamJB.setStudyId(studyId);
				teamJB.setTeamUserType("D");
				teamJB.setTeamUserRole(String.valueOf(piRole));
				teamJB.setTeamStatus("Active");//km
				teamJB.setCreator(userId);
				teamJB.setIpAdd(ipAdd);
				teamJB.setTeamDetails();
	
				//Added by Manimaran for the July-August Enhancement S4.
				id = teamJB.getId();
				moduleId=id+"";
				statusJB.setStatusModuleId(moduleId);
				statusJB.setStatusModuleTable("er_studyteam");
				statusJB.setStatusCodelstId(teamStatusActiveId);
				statusJB.setStatusStartDate(statusDate);
				statusJB.setCreator(userId);
				statusJB.setIpAdd(ipAdd);
				statusJB.setModifiedBy(userId);
				statusJB.setRecordType("N");
				statusJB.setIsCurrentStat("1");
				statusJB.setStatusHistoryDetailsWithEndDate();
				stdSiteRightsB.createStudySiteRightsData(iPInvestigator, iAccId, StringUtil.stringToNum(studyId), iUserId, ipAdd );
			}

			//Coordinator role
			String coRole="";
			CodeDao cdCoRole=new CodeDao();
			coRole=String.valueOf(cdCoRole.getCodeId("role","role_coord"));

			if (iStudyCo>0){
				int usrCnt = 0;
				TeamDao teamDao = new TeamDao();
				teamDao  = teamJB.findUserInTeam(StringUtil.stringToNum(studyId), iStudyCo);
				usrCnt=teamDao.getCRows();
				if (usrCnt==0)
				{
					teamJB.setTeamUser(studyCo);
					teamJB.setStudyId(studyId);
					teamJB.setTeamUserType("D");
					teamJB.setTeamUserRole(coRole);
					teamJB.setTeamStatus("Active");//km
					teamJB.setCreator(userId);
					teamJB.setIpAdd(ipAdd);
					teamJB.setTeamDetails();
		
					//Added by Manimaran for the July-August Enhancement S4.
					id = teamJB.getId();
					moduleId=id+"";
					statusJB.setStatusModuleId(moduleId);
					statusJB.setStatusModuleTable("er_studyteam");
					statusJB.setStatusCodelstId(teamStatusActiveId);
					statusJB.setStatusStartDate(statusDate);
					statusJB.setCreator(userId);
					statusJB.setIpAdd(ipAdd);
					statusJB.setModifiedBy(userId);
					statusJB.setRecordType("N");
					statusJB.setIsCurrentStat("1");
					statusJB.setStatusHistoryDetailsWithEndDate();
					stdSiteRightsB.createStudySiteRightsData(iStudyCo, iAccId, StringUtil.stringToNum(studyId), iUserId, ipAdd );
				}
			}

			String siteId = "";
			ArrayList arrSites = (ArrayList) args.get("arrSites");
			for(int i =0 ; i<arrSites.size(); i++){
				siteId = arrSites.get(i).toString();
				if(!siteId.equals("0")){

					studySiteJB.setSiteId(siteId);
					studySiteJB.setStudyId(studyId);
					studySiteJB.setCreator(userId);
					studySiteJB.setIpAdd(ipAdd);
	
					//KM-to fix the Bug 2543 Organiation added by default while creating Study doesn't have 'Include in Reports' checked.
					studySiteJB.setRepInclude("1");
					studySiteJB.setStudySiteDetails();
				}
			}
		// }

			int teamId = teamJB.getId();
	
			/* commented by sonia 10/22/07, changed the super user implementation
				GroupDao gDao=groupB.getGroupDaoInstance();
				gDao.grantSupUserForStudy(accId , studyId , usr ,ipAdd);
			*/
		} //end of else for study number exists check
		return success;
	}
	
	private int createStudyINDIDEData(){
		try {
			HttpSession tSession = request.getSession(true);
			String ipAdd = (String) tSession.getAttribute("ipAdd");
			String userId = (String) tSession.getAttribute("userId");
	
			//Enhancement CTRP-20527 : AGodara
			String [] pKStudyIndIde = null;
			String [] indIdeType = null;
			String [] indIdeNumber = null;
			String [] indIdeGrantors = null;
			String [] indIdeHolders = null;
			String [] indIdeProgramCodes = null;
			String [] indIdeExpandedAccess = null;
			String [] indIdeAccessType = null;
			String [] indIdeExempt = null;
	
			String cbIndIdeGrid = "";
			String deletedIndIdeRows = null;
			String numIndIdeRows ="";
	
			//START Enhancement CTRP-20527 : AGodara

			// number of data rows
			numIndIdeRows = request.getParameter("numIndIdeRows");
			indIdeType = request.getParameterValues("indIdeType");
			indIdeNumber = request.getParameterValues("indIdeNumber");
			indIdeGrantors = request.getParameterValues("indIdeGrantor");
			indIdeHolders = request.getParameterValues("indIdeHolder");
			indIdeProgramCodes = request.getParameterValues("indIdeProgramCode");
			
			//chkbx
			indIdeExpandedAccess = request.getParameterValues("cbExpandedAccessFlag");
			
			indIdeAccessType = request.getParameterValues("indIdeAccessType");
			
			//chkbx
			indIdeExempt = request.getParameterValues("cbIndIdeExemptFlag");
			for(int i=0;i<StringUtil.stringToNum(numIndIdeRows);i++){
				
				studyIndIdeJB.setFkStudy(StringUtil.integerToString(studyJB.getId()));
				studyIndIdeJB.setTypeIndIde(indIdeType[i]);
				studyIndIdeJB.setNumberIndIde(indIdeNumber[i]);
				studyIndIdeJB.setFkIndIdeGrantor(indIdeGrantors[i]);
				studyIndIdeJB.setFkIndIdeHolder(indIdeHolders[i]);
				studyIndIdeJB.setFkProgramCode(indIdeProgramCodes[i]);
				studyIndIdeJB.setExpandIndIdeAccess(indIdeExpandedAccess[i]);
				studyIndIdeJB.setFkAccessType(indIdeAccessType[i]);
				studyIndIdeJB.setExemptINDIDE(indIdeExempt[i]);
				studyIndIdeJB.setCreator(userId);
				studyIndIdeJB.setIpAdd(ipAdd);
				studyIndIdeJB.setIndIdeInfo();
				
			}
			
			return 1;
		} catch (Exception e){
			return 0;
		}	
		// END Enhancement CTRP-20527 : AGodara
	}
	
	private int createUpdateNIHGrantData(){
		try {
			HttpSession tSession = request.getSession(true);
			String studyId = (new Integer(studyJB.getId())).toString();
			String userId = (String) tSession.getAttribute("userId");
			String ipAdd = (String) tSession.getAttribute("ipAdd");

			//#AK:Added for Enhancement CTRP-20527_1
			String studyFuncdMech="";
			String studyInstCode="";
			String programCode="";
			String serialNumber="";
			String fundMechs[] = null;
			String instCodes[]=null;
			String programCodes[]=null;
			String serialNumbers[]=null;
			String idsNIHGrants[]=null;
			fundMechs=request.getParameterValues("fundMech");
			instCodes=request.getParameterValues("instCode");
			programCodes=request.getParameterValues("programCode");
			serialNumbers=request.getParameterValues("serialNum");
			String rows_existing = request.getParameter("totalExtgRows");
			idsNIHGrants=request.getParameterValues("idsNIHgrant");
			String delStrs = request.getParameter("delStrs");  
	
			//Ak:Added For enhancement CTRP-20527_1
	
			String studyNIHgrantId="";
			Integer totalRows= StringUtil.stringToNum(rows_existing);
			totalRows=totalRows>0?totalRows:0;
			//Add/Update functionality of NIH grant records
			for (int j=0 ; j < totalRows ;j ++){
				studyNIHgrantId=(String) idsNIHGrants[j];
				studyNIHgrantId = (studyNIHgrantId==null)?"":studyNIHgrantId;		
				studyFuncdMech=fundMechs[j];
				studyInstCode=instCodes[j];
				programCode=programCodes[j];
				serialNumber=serialNumbers[j].trim();
				studyNIHGrantJB.setStudyNIHGrantId(StringUtil.stringToNum(studyNIHgrantId));
				if(StringUtil.stringToNum(studyNIHgrantId)>0){
				studyNIHGrantJB.getStudyNIHGrantInfo();
				}
				studyNIHGrantJB.setFundMech(studyFuncdMech);
				studyNIHGrantJB.setInstCode(studyInstCode);
				studyNIHGrantJB.setStudyId(studyId);
				studyNIHGrantJB.setProgramCode(programCode);
				studyNIHGrantJB.setNihGrantSerial(serialNumber);
				studyNIHGrantJB.setIpAdd(ipAdd);
				if(StringUtil.stringToNum(studyNIHgrantId)>0){ 
					studyNIHGrantJB.setModifiedBy(userId);
					studyNIHGrantJB.updateStudyNIHGrant();
				}else{
					studyNIHGrantJB.setModifiedBy(null);        //Added to fix Bug#7994 and 7960: Raviesh
					studyNIHGrantJB.setCreator(userId);
					studyNIHGrantJB.setStudyNIHGrantInfo();
				}	
				
			}
			//Delete the NIH Records
			StringTokenizer idStrgs=new StringTokenizer(delStrs,",");
			int Ids=idStrgs.countTokens();
			String NIHgrantId="";
			for(int cnt=0;cnt<Ids;cnt++)
			{
				NIHgrantId = idStrgs.nextToken();
				studyNIHGrantJB.deleteStudyNIHGrantRecord(StringUtil.stringToNum(NIHgrantId), AuditUtils.createArgs(tSession,"",LC.L_Study));
			}//End here deletion functionality
			return 1;
		} catch (Exception e){
			return 0;
		}
	}
	
	private int createUpdateMoreStudyDetails(){
		int success = 0;
		
		try{
			HttpSession tSession = request.getSession(true);
			String userId = (String) tSession.getAttribute("userId");
			String ipAdd = (String)tSession.getAttribute("ipAdd");
			String defUserGroup = (String) tSession.getAttribute("defUserGroup");
			 
			String studyId = request.getParameter("studyId");
			studyId = (String) tSession.getAttribute("studyId");

			ArrayList arRecordType = new ArrayList();
			ArrayList arAlternateId = new ArrayList();
			ArrayList arAlternateValues = new ArrayList();
			ArrayList arId = new ArrayList();
			//String[] arElementId = request.getParameterValues("elementId");

			Enumeration params = request.getParameterNames();
			while (params.hasMoreElements()) {
			    String param = (String)params.nextElement();
			    if (param.startsWith("alternateId") && (!param.endsWith("Checks"))) {
			    	String paramVal = request.getParameter(param);
			    	if (null != paramVal){
			    		String mdId = param.substring(param.indexOf("alternateId")+("alternateId".length()));
			    		arAlternateId.add(mdId);
			    		arAlternateValues.add(paramVal);
			    		paramVal = request.getParameter("recordType"+mdId);
				    	if (null != paramVal){
				    		arRecordType.add(paramVal);
				    	}
				    	paramVal = request.getParameter("id"+mdId);
				    	if (null != paramVal){
				    		arId.add(paramVal);
				    	}
			      	}
			    }
			}


			StudyIdBean sskMain = new StudyIdBean();
			ArrayList alElementData = arAlternateValues;

			int len = alElementData.size();

			for ( int j = 0 ; j< len ; j++){
				StudyIdBean sskTemp = new StudyIdBean();

				if   ( ( !(StringUtil.isEmpty ((String)arAlternateValues.get(j))) &&  "N".equals((String)arRecordType.get(j))) || "M".equals((String)arRecordType.get(j)))
				{
					sskTemp.setId(StringUtil.stringToNum((String)arId.get(j)));
					sskTemp.setStudyId(studyId) ;
					sskTemp.setStudyIdType((String)arAlternateId.get(j));
					sskTemp.setAlternateStudyId((String)arAlternateValues.get(j));
					sskTemp.setRecordType((String)arRecordType.get(j));
					if ("N".equals((String)arRecordType.get(j)))
					{
						sskTemp.setCreator(userId);
					}else
					{
						sskTemp.setModifiedBy(userId);
					}
					sskTemp.setIpAdd(ipAdd);
					sskMain.setStudyIdBeans(sskTemp);
				}
			}

			StudyIdJB studyIdJB = new StudyIdJB();
			success = studyIdJB.createMultipleStudyIds(sskMain,defUserGroup);
		} catch (Exception e){
			success = 0;
		}
		return success;
	}

	private int createUpdateStudyTeam(){
		int success = 0;
		final String TEAMID = "teamId", ROLEID = "roleId", USERID = "userId";
		
		try{
			HttpSession tSession = request.getSession(true);
			String userId = (String) tSession.getAttribute("userId");
			String ipAdd = (String)tSession.getAttribute("ipAdd");
			String defUserGroup = (String) tSession.getAttribute("defUserGroup");
			String acc = (String)tSession.getAttribute("accountId");
			String studyId = request.getParameter("studyId");
			studyId = (String) tSession.getAttribute("studyId");

			ArrayList arRecordType = new ArrayList();
			ArrayList arAlternateId = new ArrayList();
			ArrayList arAlternateValues = new ArrayList();
			ArrayList arId = new ArrayList();
			//String[] arElementId = request.getParameterValues("elementId");

			Enumeration params = request.getParameterNames();
			while (params.hasMoreElements()) {
			    String param = (String)params.nextElement();
			    if (!param.startsWith("sTeamGridData")) {
			    	continue;
			    }
		    	String paramVal = request.getParameter(param);
		    	paramVal = StringUtil.htmlDecode(paramVal);
		    	if (!StringUtil.isEmpty(paramVal)){
		    		String orgId = param.substring(param.indexOf("sTeamGridData")+("sTeamGridData".length()));
		    		//System.out.println(orgId);
		    		
		    		JSONArray teamArray = new JSONArray(paramVal);
		    		//System.out.println(teamArray);
		    		
		    		for (int indx = 0; indx < teamArray.length(); indx++){
		    			JSONObject teamMemberJSON = (JSONObject)teamArray.get(indx);
		    			int teamId = StringUtil.stringToNum(teamMemberJSON.getString(TEAMID));
		    			
		    			String teamUserId = teamMemberJSON.getString(USERID);
		    			if (StringUtil.isEmpty(teamUserId)){
		    				this.errorMap = (null == this.errorMap)? new HashMap<String, Object>() : this.errorMap;
		    				errorMap.put(DATA_NOT_SAVED, "Team Member Not Selected");
		    				return 0;
		    			}

		    			String roleId = teamMemberJSON.getString(ROLEID);
		    			if (StringUtil.isEmpty(roleId)){
		    				this.errorMap = (null == this.errorMap)? new HashMap<String, Object>() : this.errorMap;
		    				errorMap.put(DATA_NOT_SAVED, "Role Not Selected");
		    				return 0;
		    			}

		    			TeamJB teamB = new TeamJB();
		    			if (teamId > 0){
	    					teamB.setId(teamId);
	    					teamB.getTeamDetails();

	    					teamB.setModifiedBy(userId);
			    			teamB.setIpAdd(ipAdd);
	    					teamB.setTeamUserRole(roleId);
	    					teamB.updateTeam();
		    			} else {    					
	    					teamB.setTeamUser(teamUserId);
	    					teamB.setStudyId(studyId);
	    					teamB.setTeamUserRole(roleId);
	    					teamB.setCreator(userId);
	    					teamB.setIpAdd(ipAdd);
	    					teamB.setTeamUserType("D"); // D is for default team type
	    					teamB.setTeamStatus("Active");//km
	    					teamB.setTeamDetails();
	    					CodeDao cd1 = new CodeDao();
	    					int activeStatusId = cd1.getCodeId("teamstatus", "Active");
	    					StatusHistoryJB statusHistoryJB = new StatusHistoryJB();
	    					statusHistoryJB.setStatusModuleTable("er_studyteam");
	    					statusHistoryJB.setStatusModuleId(String.valueOf(teamB.getId()));
	    					statusHistoryJB.setStatusCodelstId(String.valueOf(activeStatusId));
	    					statusHistoryJB.setIsCurrentStat("1");
	    					statusHistoryJB.setStatusHistoryDetails();
	    					stdSiteRightsB.createStudySiteRightsData(EJBUtil.stringToNum(teamUserId), EJBUtil.stringToNum(acc), EJBUtil.stringToNum(studyId), EJBUtil.stringToNum(userId), ipAdd );
		    			}
		    		}
		      	}
			}
			
			String[] deletedMembers = StringUtil.chopChop((String)request.getParameter("sTeamPurged"), ',');
			for (int indx =0; indx <deletedMembers.length; indx++){
				int teamId = StringUtil.stringToNum(deletedMembers[indx]);
				if (teamId > 0){
					TeamJB teamB = new TeamJB();
					teamB.setId(teamId);
					teamB.setModifiedBy(userId);
					teamB.updateTeam();
					int ret = teamB.deleteFromTeam(AuditUtils.createArgs(tSession,"",LC.L_Study));
					if (0 < ret){
						this.errorMap = (null == this.errorMap)? new HashMap<String, Object>() : this.errorMap;
	    				errorMap.put(DATA_NOT_DELETED, "Team Member not deleted " + teamId);
	    				return 0;
					}
				}
			}
		} catch (Exception e){
			success = 0;
		}
		return success;
	}

	public String createStudySite(){
		HttpSession tSession = request.getSession(true);
		int studyId = StringUtil.stringToNum((String)tSession.getAttribute("studyId"));
		int siteId = StringUtil.stringToNum((String)request.getParameter("accSites"));
		int userId = StringUtil.stringToNum((String) tSession.getAttribute("userId"));
		
		if(studyId <= 0 || siteId <= 0 || userId <= 0){
			this.errorMap = (null == this.errorMap)? new HashMap<String, Object>() : this.errorMap;
			errorMap.put(DATA_NOT_SAVED, "Study Site not saved");
			return "error";
		}

		String ipAdd = (String) tSession.getAttribute("ipAdd");

		String siteTypeId=request.getParameter("siteType");
		siteTypeId = (StringUtil.isEmpty(siteTypeId) || "null".equals(siteTypeId)) ? "": siteTypeId;

		StudySiteJB studySiteB = new StudySiteJB();
		StudySiteDao studySiteDao = new StudySiteDao();
		studySiteDao = studySiteB.getCntForSiteInStudy(studyId, siteId);
		int rows = studySiteDao.getCRows();

		if(rows >= 1 ){
			this.errorMap = (null == this.errorMap)? new HashMap<String, Object>() : this.errorMap;
			errorMap.put(DATA_NOT_SAVED, "Study Sites exists");
			return "error";
		}
    	studySiteB.setStudyId(""+studyId);
     	studySiteB.setSiteId(""+siteId);
     	studySiteB.setStudySiteType(siteTypeId);
     	studySiteB.setCreator(""+userId);
     	studySiteB.setIpAdd(ipAdd);

	   	studySiteB.setStudySiteDetails();
     	int ret = studySiteB.getStudySiteId();
	   	return "success";
	}

	public String updateStudyScreen(){
		int success = 0;
		try{
			String mode = request.getParameter("mode");
			if ("M".equals(mode)){
				success = updateStudy();
			} else {
				success = createStudy();
			}
			
			if (success < 0){
				Map<String, String> errorMap = new HashMap<String, String>();
				switch (success){
				case -1:
					if ("userpxd".equals(Configuration.eSignConf)) {
						errorMap.put(ESIGN_DOES_NOT_MATCH, MC.M_EtrWrgPassword_Svg);
						break;
					}
					else
					{
						errorMap.put(ESIGN_DOES_NOT_MATCH,MC.M_EtrWrgEsign_Svg);
					}
					break;
				default:
					errorMap.put(DATA_NOT_SAVED, MC.M_DataNotSvd_Succ);
					break;
				}
				this.errorMap=errorMap;
				return INPUT;
			}
			
			success = createUpdateMoreStudyDetails();
			
			if (success < 0){
				Map<String, String> errorMap = new HashMap<String, String>();
				errorMap.put(DATA_NOT_SAVED, "More Study Details not saved!");
				this.errorMap=errorMap;
				return INPUT;
			}

			
			success = createUpdateStudyTeam();
			
			if (success < 0){
				Map<String, String> errorMap = new HashMap<String, String>();
				errorMap.put(DATA_NOT_SAVED, "Study Team not saved!");
				this.errorMap=errorMap;
				return INPUT;
			}

			// -- Add a hook for customization for after a study update succeeds
			String DEFAULT_DUMMY_CLASS = "[Config_Study_Interceptor_Class]";
			/*try { 
				if (!StringUtil.isEmpty(LC.Config_Study_Interceptor_Class) &&
						!DEFAULT_DUMMY_CLASS.equals(LC.Config_Study_Interceptor_Class)) {
					Class clazz = Class.forName(LC.Config_Study_Interceptor_Class);
					Object obj = clazz.newInstance();
					Object[] args = new Object[2];
					args[0] = (Object)request;
					args[1] = (Integer)success;
					((InterceptorJB) obj).handle(args);
				}
			} catch(Exception e) {
				e.printStackTrace();
			}*/
			// -- End of hook
			
		} catch (Exception e){
			Map<String, String> errorMap = new HashMap<String, String>();
			errorMap.put(DATA_NOT_SAVED, MC.M_DataNotSvd_Succ);
			this.errorMap=errorMap;
			return INPUT;
		}
		this.errorMap = new HashMap<String, Object>();
		return SUCCESS;
	}
}	
