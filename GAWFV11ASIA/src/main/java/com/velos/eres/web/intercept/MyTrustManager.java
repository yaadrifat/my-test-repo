package com.velos.eres.web.intercept;

import java.security.cert.X509Certificate;
import javax.net.ssl.X509TrustManager;

class MyTrustManager
implements X509TrustManager {
    MyTrustManager() {
    }

    @Override
    public X509Certificate[] getAcceptedIssuers() {
        return null;
    }

    public void checkClientTrusted(javax.security.cert.X509Certificate[] certs, String authType) {
    }

    public void checkServerTrusted(javax.security.cert.X509Certificate[] certs, String authType) {
    }

    @Override
    public void checkClientTrusted(X509Certificate[] paramArrayOfX509Certificate, String paramString) {
    }

    @Override
    public void checkServerTrusted(X509Certificate[] paramArrayOfX509Certificate, String paramString) {
    }
}
