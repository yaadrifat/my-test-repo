/*
 * Class Name : UserReportLogJBHelper
 * 
 * Version information: 1.0
 *
 * Date: 18/September/2012
 * 
 */
package com.velos.eres.web.userReportLog;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.aithent.file.uploadDownload.Configuration;
import com.velos.eres.business.common.ReportDaoNew;
import com.velos.eres.business.dynrep.impl.DynRepBean;
import com.velos.eres.business.invoice.InvoiceBean;
import com.velos.eres.business.userReportLog.impl.UserReportLogBean;
import com.velos.eres.service.userReportLogAgent.UserReportLogAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.dynrep.DynRepJB;
import com.velos.eres.web.invoice.InvoiceJB;
import com.velos.eres.web.report.ReportJBNew;
import com.velos.esch.business.budget.impl.BudgetBean;
import com.velos.esch.web.budget.BudgetJB;

/**
 * Client side bean helper class for User report logging
 * 
 * @author Yogendra Pratap Singh
 * 
 * @version 1.0 09/18/2012
 */
public class UserReportLogJBHelper {
	
	private static final String SET="set";
	private static final String GET="get";
	private static final String USR_RPT_LOG="User Report Logging";
	private static final String AD_HOC  = "ad-hoc queries";
	
	/**
	 * @param userReportLogJB
	 * @return 
	 */
	public int setUserReportLogDetails(UserReportLogJB userReportLogJB) {
		 Rlog.info(USR_RPT_LOG, "UserReportLogJBHelper.setUserReportLogDetails() start");
		//get the remote object
			UserReportLogAgentRObj userReportLogAgentRObj= getUserReportRemoteObj();
			//process the UserReportLogJB 
			UserReportLogBean userReportLogBean = processUserReportLogBean(userReportLogJB);
			//Call the Bean method to persist the value
			int pkUserReportLog = userReportLogAgentRObj.setUserReportLogDetails(userReportLogBean);
		 Rlog.info(USR_RPT_LOG, "UserReportLogJBHelper.setUserReportLogDetails() end with pkUserReportLog :"+pkUserReportLog);
		 return pkUserReportLog;
	}
	
	/**
	 * @param userReportLogJB
	 * @return userReportLogBean
	 */
	private UserReportLogBean processUserReportLogBean(UserReportLogJB userReportLogJB){
		Rlog.info(USR_RPT_LOG, "UserReportLogJBHelper.processUserReportLogBean() start");
		UserReportLogBean  userReportLogBean = new UserReportLogBean();
		//transfer the userReportLogJB values into the UserReportLogBean
		userReportLogBean = (UserReportLogBean)transferJBAndBeanValues(userReportLogJB,userReportLogBean);
		Rlog.info(USR_RPT_LOG, "UserReportLogJBHelper.processUserReportLogBean() end");
		return userReportLogBean;
	}
	
	/**
	 * @return UserReportLogAgentRObj
	 */
	private UserReportLogAgentRObj getUserReportRemoteObj(){
		Rlog.info(USR_RPT_LOG, "UserReportLogJBHelper.getUserReportRemoteObj() start");
		UserReportLogAgentRObj userReportLogAgent = EJBUtil.getUserReportLogAgentHome();
		Rlog.info(USR_RPT_LOG, "UserReportLogJBHelper.getUserReportRemoteObj() end");
		return userReportLogAgent;
	}
	
	/**
	 * Transfer the JB Object values to Bean Object values
	 * @return Object
	 * Object transferJBAndBeanValues(Object FromObject, Object ToObject)
	 * This method will transfer the FromObject's data into the ToObject
	 * and returned the ToObject to the user.
	 */
	public Object transferJBAndBeanValues(Object jbObject, Object beanObject){
		Rlog.info(USR_RPT_LOG , "transferJBAndBeanValues() method starts");
		Object transferedObject = new Object();
		HashMap<String ,Object> toFieldTypes = new HashMap<String ,Object>();
		HashMap<String ,String> toMethodsMap = new HashMap<String ,String>(); 

		if(jbObject!= null && beanObject!=null){
			Method[] jbMethods = jbObject.getClass().getMethods();
			Method[] beanMethods = beanObject.getClass().getMethods();
			Field[] declareFields = beanObject.getClass().getDeclaredFields();
			Field[] fields = beanObject.getClass().getFields();
			int fieldSize = declareFields.length + fields.length;

			for(int j =0;j<beanMethods.length;j++){
				if(beanMethods[j].getName().indexOf(SET) == 0){
					toMethodsMap.put(beanMethods[j].getName(), String.valueOf(j));
				}
			}
			// for data type conversion
			Field[] toFields = new Field[fieldSize];
			int i =0;
			for( ;i<declareFields.length;i++){
				toFields[i] = declareFields[i];
			}
			for(int j=0;j<fields.length;j++,i++){
				toFields[i] = fields[j];
			}
			for (int count =0;count<toFields.length;count++){
				toFieldTypes.put(toFields[count].getName().toUpperCase(), toFields[count].getType());
			}

			String fromMethodName="";
			String toMethodName ="";
			Object[] param = null;
			for( i=0;i<jbMethods.length;i++){
				if(jbMethods[i].getName().indexOf(GET) == 0 ){ // only get methods
					fromMethodName = jbMethods[i].getName();
					fromMethodName= fromMethodName.substring(3,fromMethodName.length() ); // remove the string get  
					if(declareFields.length < 2 || toFieldTypes.containsKey(fromMethodName.toUpperCase())){
						// field length for extend class
						try{
							Object obj = jbMethods[i].invoke(jbObject, param);
							//Check For Object Null Check
							if(obj != null ){
								int invokeIndex ;
								if(toMethodsMap.containsKey((SET+fromMethodName))){
									invokeIndex = Integer.parseInt((String)toMethodsMap.get((SET+fromMethodName)));
									toMethodName = beanMethods[invokeIndex].getName();
									toMethodName= toMethodName.substring(3,toMethodName.length() );
									/*************** Object Transformation Start From Here ***************/ 
									if(declareFields.length > 1) { // for Bean value transfer
										if(toFieldTypes.get(toMethodName.toUpperCase()).equals(new Date().getClass()) && (obj instanceof String)  ){
											/* string to Date*/
											if(!((String)obj).trim().equals("")){
												obj = DateUtil.getDate((String)obj);
											}
										}else  if( (obj instanceof java.util.Date) && (toFieldTypes.get(toMethodName.toUpperCase()).equals(new String().getClass())) ){
											// Date to string...
											obj = DateUtil.getDate((String)obj);
										}else if(toFieldTypes.get(toMethodName.toUpperCase()).equals(new Date().getClass()) && (obj instanceof String)  ){
											/* String to Timestamp.....*/
											if(!((String)obj).trim().equals("")){
												obj = DateUtil.getDate((String)obj);
											}

										}else if((obj instanceof java.sql.Timestamp) && (toFieldTypes.get(toMethodName.toUpperCase()).equals(new String().getClass())) ){
											/* Timestamp to String.....*/
											if(!((String)obj).trim().equals("")){
												obj = DateUtil.getDate((String)obj);
											}
										}else if ((obj instanceof String ) && (toFieldTypes.get(toMethodName.toUpperCase()) instanceof java.sql.Timestamp ) ){
											if( obj != null && !((String)obj).trim().equals("")){
												obj = DateUtil.getDate((String)obj);
											}
										}
									}
									beanMethods[invokeIndex].invoke(beanObject, obj);
								} 
								Rlog.info(USR_RPT_LOG , "transferJBAndBeanValues() method complete successfully");
							} /* End of  object  null check.....*/
						}catch(Exception e){
							Rlog.error(USR_RPT_LOG , fromMethodName + " -> Data type mismatch check data types in  (" + jbObject.getClass().getName() + " , " + beanObject.getClass().getName()+")");
							Rlog.error(USR_RPT_LOG , "Exception in transferJBAndBeanValues() method : "+ e.getMessage());
							Rlog.error(USR_RPT_LOG , " Exception in transferJBAndBeanValues() => " + e);
						}
					}/* End of key check.....*/
				}
			}
		}
		transferedObject = beanObject;
		return transferedObject;
	}
	
	/**
	 * @param moduleName
	 * @param reportCat
	 * @param accountId
	 * @param repId
	 * @return
	 * This method returns the report Title
	 */
	public String getReportTitle(String moduleName,String reportCat,int accountId,int repId){
		ReportDaoNew reportDaoNew 	= null;
		DynRepBean getDynRepBean 	= null;
		InvoiceBean invoiceBean		= null;
		String repTitle 			= null;
		if(moduleName.equalsIgnoreCase(LC.L_Adhoc_Queries)
				|| (null != reportCat && reportCat.equalsIgnoreCase(AD_HOC))){
			getDynRepBean = getDynReportInfo(repId);
			if(null!=getDynRepBean)
				repTitle = getDynRepBean.getRepName();
		}else if(moduleName.equalsIgnoreCase(LC.L_Invoice)){
			  invoiceBean = getInvoiceInfo(repId);
			 if(null != invoiceBean){
				repTitle = invoiceBean.getInvNumber();
			 }else{
				reportDaoNew = getReportInfo(accountId,  repId);
				ArrayList<String> repTitles = reportDaoNew.getRepName();
				if(null != repTitles)
					repTitle = repTitles.get(0);
			 }
		}else if(moduleName.equalsIgnoreCase("studyBudget")){
			BudgetBean budgetbean = getBudgetInfo( repId);
			repTitle = budgetbean.getBudgetName();
		}else{
			reportDaoNew = getReportInfo(accountId,  repId);
			ArrayList<String> repTitles = reportDaoNew.getRepName();
			if(null != repTitles)
				repTitle = repTitles.get(0);
		}
		return repTitle;
	}
	
	/**
	 * @param accountId
	 * @param reportId
	 * @return ReportDaoNew
	 */
	public ReportDaoNew getReportInfo(int accountId, int reportId){
		ReportJBNew reportJBNew = new ReportJBNew();
		ReportDaoNew reportDaoNew = reportJBNew.getReport(accountId, reportId);
		return reportDaoNew;
	}
	/**
	 * @param reportId
	 * @return DynRepBean
	 */
	public DynRepBean getDynReportInfo(int reportId){
		DynRepJB dynRepJB = new DynRepJB();
		dynRepJB.setId(reportId);
		DynRepBean dynRepBean = dynRepJB.getDynRepDetails();
		return dynRepBean;
	}
	/**
	 * @param reportId
	 * @return InvoiceBean
	 */
	public InvoiceBean getInvoiceInfo(int reportId){
		InvoiceJB InvB = new InvoiceJB();
		InvB.setId(reportId);
		InvoiceBean InvBean = InvB.getInvoiceDetails();
		return InvBean;
	}
	
	/**
	 * @param reportId
	 * @return BudgetBean
	 */
	public BudgetBean getBudgetInfo(int reportId){
		BudgetJB budgetJB = new BudgetJB();
		budgetJB.setBudgetId(reportId);
		BudgetBean budgetbean =  budgetJB.getBudgetDetails();
		return budgetbean;
	}
	
	/**
	 * @param userId
	 * @param uName
	 * @param repId
	 * @param repTitle
	 * @param ipAdd
	 * @param requestURL
	 * @param accessDateTime
	 * @param queryString
	 * @param moduleName
	 * @param fileName
	 * @param fileDnPath
	 * @param repXml
	 * @param repXsl
	 * @param downloadFlag
	 * @param downloadFormat
	 * @return UserReportLogJB
	 */
	public UserReportLogJB setUserReportLogJBData(Integer userId,
			Integer repId,String repTitle,String ipAdd,String requestURL,
			Date accessDateTime,String queryString, String moduleName,
			String fileName,String fileDnPath, String repXml,
			Integer downloadFlag, String downloadFormat,String tableName){
		/*
		Set the values into the UserReportLogJB bean
		*/
		UserReportLogJB userReportLogJB = new UserReportLogJB();
		userReportLogJB.setFkUser(userId);
		if(repId!=0)
			userReportLogJB.setFkReport(repId);
		else
			userReportLogJB.setFkReport(null);
		userReportLogJB.setUserReportLogReportName(repTitle);
		userReportLogJB.setUserReportLogIPAdd(ipAdd);
		userReportLogJB.setUserReportLogURL(requestURL.toString());
		userReportLogJB.setUserReportLogAccessTime(accessDateTime);
		userReportLogJB.setUserReportLogURLQueryParam(queryString);
		userReportLogJB.setUserReportLogModuleName(moduleName);
		userReportLogJB.setUserReportLogFileName(fileName);
		userReportLogJB.setUserReportLogDownloadPath(fileDnPath);
		userReportLogJB.setUserReportLogFormat(downloadFormat);
		userReportLogJB.setUserReportLogXML(repXml);
		userReportLogJB.setIsUserReportLogDownloadFlag(downloadFlag);
		userReportLogJB.setTableName(tableName);
		return userReportLogJB;
	}
	
	/**
	 * This method is used for logging of file downloaded files.
	 * @param request
	 * @param db
	 * @param tableName
	 * @param columnName
	 * @param pkColumnName
	 * @param filPath
	 * @param pkValueInt
	 * @param module
	 */
	public static void saveLoggingData(HttpServletRequest request){
		HttpSession tSession = request.getSession(false);
		UserReportLogJBHelper userReportLogJBHelper = null;
		UserReportLogJB userReportLogJB = null;
		String queryString	= null ;
		String requestURL 	= null ;
		String repTitle 	= null;
		Date accessDateTime	= null;
		String reportCat	= null;
		String repXml 		= null;
		/*Now fetch the data from session and request*/
		Integer userId 			= StringUtil.stringToInteger((String) (tSession.getValue("userId")));
		String ipAdd 		= (String)tSession.getValue("ipAdd");
		Integer accountId 	= StringUtil.stringToInteger(((String)tSession.getValue("accountId")==null) ? "0" : (String)tSession.getValue("accountId"));
		Integer repId 		= StringUtil.stringToInteger((request.getParameter("repId")==null) ? "0" : request.getParameter("repId"));
		
		String fileName = request.getParameter("file");
		int pkValue = EJBUtil.stringToNum(request.getParameter("pkValue")); // the primary key value of the table
        String tableName = request.getParameter("tableName"); // table name
        
        if(null != tSession.getAttribute("reportCat"))
			reportCat = tSession.getAttribute("reportCat").toString();
        
        /* Current System date and time */
        SimpleDateFormat formatter = new SimpleDateFormat (com.velos.eres.service.util.Configuration.getAppDateFormat() +" H:mm:ss" );
		try {
			accessDateTime  = formatter.parse(DateUtil.getCurrentDateTime());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        String filePath = request.getParameter("filePath");
        String fileDnPath 	= StringUtil.decodeString(filePath);
        String downloadFormat = downloadedFileFormat(fileName);
        int downloadFlag  = 1;
		String moduleName 	= request.getParameter("moduleName");
		moduleName 	= moduleName(moduleName);
		
		if(!(moduleName.equalsIgnoreCase(LC.L_Rpt_Central)) && 
				(reportCat!=null &&  reportCat.equalsIgnoreCase("ad-hoc queries"))){
			tSession.removeAttribute("reportCat");
			reportCat = null;
		}
		if(null==tableName && repId!=0)tableName = getTableName(moduleName,reportCat);
		if(tableName!=null)tableName=tableName.toUpperCase();
		
		requestURL 	= getRequestURL(moduleName, request);
		queryString = getQueryString(moduleName, request);
		requestURL  = ( requestURL == null  ) ? "" : requestURL;
		queryString = ( queryString == null ) ? "" : queryString;
		
		if(null==fileDnPath){
			String module = request.getParameter("module");
			if (Configuration.FILE_UPLOAD_DOWNLOAD==null) Configuration.readSettings("sch");
			Configuration.readUploadDownloadParam(Configuration.FILE_UPLOAD_DOWNLOAD + "fileUploadDownload.xml", module);
			fileDnPath = Configuration.DOWNLOADFOLDER; 
		}
		/*Now call the helper method for further processing the data.*/
		userReportLogJBHelper = new UserReportLogJBHelper();
		if(repId!=0){
			repTitle = userReportLogJBHelper.getReportTitle(moduleName,reportCat,accountId,repId);
		}else{
			repTitle = fileName;
			repId=pkValue;
		}
		userReportLogJB = userReportLogJBHelper.setUserReportLogJBData(userId,repId,repTitle,ipAdd,requestURL,
				accessDateTime,queryString,moduleName,fileName,fileDnPath,repXml,downloadFlag,downloadFormat,tableName);
		userReportLogJBHelper.setUserReportLogDetails(userReportLogJB);
	}
	
	/**
	 * @param fileName
	 * @return downloaded File Format
	 */
	public static String downloadedFileFormat(String fileName){
		String fileFormat=null;
		if( null!=fileName ){
			fileName = fileName.toLowerCase();
			if( fileName.endsWith(".doc") || fileName.endsWith(".docx") )
				fileFormat = LC.L_Word_Format;
			else if( fileName.endsWith(".xls") || fileName.endsWith(".xlsx") )
				fileFormat = LC.L_Excel_Format;
			else if( fileName.endsWith(".html") || fileName.endsWith(".htm") )
				fileFormat = LC.L_Printer_FriendlyFormat;
			else if( fileName.endsWith(".pdf") )
				fileFormat = LC.L_Pdf+" Format";
			else if( fileName.endsWith(".txt") )
				fileFormat = LC.L_Text+" Format";
			else if( fileName.endsWith(".ppt") )
				fileFormat = "Power Point Format";
			else if( fileName.endsWith(".rtf") )
				fileFormat = "RTF Format";
			else if( fileName.endsWith(".jpg") || fileName.endsWith(".gif") 
					|| fileName.endsWith(".jpeg") || fileName.endsWith(".bmp"))
				fileFormat = "Image Format";
			else if( fileName.endsWith(".sas")  )
				fileFormat = LC.L_Sas_Format;
			else if( fileName.endsWith(".xml")  )
				fileFormat = LC.L_Xml_Format;
			else if( fileName.endsWith(".rar") || fileName.endsWith(".zip") )
				fileFormat = LC.L_Zip+" Format";
			else if(fileName.endsWith(".csv"))
				fileFormat = LC.L_CSV_Format;
			else
				fileFormat = "Unknown Format";
		}
		return fileFormat;
	}
	
	/**
	 * @param moduleName
	 * @return moduleName
	 */
	public static String moduleName(String moduleName){
		if(null==moduleName)moduleName="";
		if(moduleName.equalsIgnoreCase(LC.L_Adhoc_Queries)){
			moduleName = LC.L_Adhoc_Queries;
		}else if(moduleName.equalsIgnoreCase(LC.L_Invoice)){
			moduleName = LC.L_Invoice;
		}else if(moduleName.equalsIgnoreCase(LC.L_Budget)){
			moduleName = LC.L_Budget;
		}else if(moduleName.equalsIgnoreCase(LC.L_Study)){
			moduleName = LC.L_Study;
		}else if(moduleName.equalsIgnoreCase(LC.L_Patient)){
			moduleName = LC.L_Patient;
		}else if(moduleName.equalsIgnoreCase(LC.L_Calendar)){
			moduleName = LC.L_Calendar;
		}else if(moduleName.equalsIgnoreCase(LC.L_Milestone)){
			moduleName = LC.L_Milestone;
		}else if(moduleName.equalsIgnoreCase(LC.L_Event)){
			moduleName = LC.L_Event;
		}else{
			moduleName = LC.L_Rpt_Central;
		}
		return moduleName;
	}
	/**
	 * @param moduleName
	 * @param reportCat
	 * @return Report Table Name
	 */
	public static String getTableName(String moduleName , String reportCat){
		String tableName="";
		if(moduleName.equalsIgnoreCase(LC.L_Adhoc_Queries)
				|| (null != reportCat && reportCat.equalsIgnoreCase(AD_HOC))){
			tableName = "ER_DYNREP";
		}else if(moduleName.equalsIgnoreCase(LC.L_Invoice)){
			tableName="ER_INVOICE";
		}else if(moduleName.equalsIgnoreCase("studyBudget")){
			tableName="SCH_BUDGET";
		}else if(moduleName.equalsIgnoreCase(LC.L_Budget) || 
				moduleName.equalsIgnoreCase(LC.L_Patient) ||
				moduleName.equalsIgnoreCase(LC.L_Rpt_Central) || 
				moduleName.equalsIgnoreCase(LC.L_Calendar) ||
				moduleName.equalsIgnoreCase(LC.L_Study)){
			tableName="ER_REPORT";
		}
		return tableName;
	}
	/**
	 * @param moduleName
	 * @param request
	 * @return Request URL through which report is accessed
	 */
	public static String getRequestURL(String moduleName,HttpServletRequest request){
		String requestURL=null;
		if(null!=request.getParameter("requestURL")){
			requestURL = StringUtil.decodeString( request.getParameter("requestURL") );
		}else{
			requestURL 	= StringUtil.decodeString( request.getRequestURL().toString() );
		}
		return requestURL;
	}
	/**
	 * @param moduleName
	 * @param request
	 * @return URL's Query String
	 */
	public static String getQueryString(String moduleName,HttpServletRequest request){
		String queryString=null;
		if(null!=request.getParameter("queryString")){
	        queryString = StringUtil.decodeString( request.getParameter("queryString") );
		}else{
			queryString = StringUtil.decodeString( request.getQueryString() );
		}
		return queryString;
	}
}
