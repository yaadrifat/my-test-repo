package com.velos.eres.web.submission;

import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.audit.web.AuditRowEresJB;
import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.business.submission.impl.SubmissionProvisoBean;
import com.velos.eres.service.submissionAgent.SubmissionProvisoAgent;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

public class SubmissionProvisoJB {
    
    private int id;
    private String fkSubmission;
    private String provisoflag;
    private String fkSubmissionBoard;
    private String fkSubmissionStatus;
    private String    provisoDate;
    private String provisoEnteredBy;
    private String  submissionProviso;
    private String creator;
    private String lastModifiedBy;
    private String  ipAdd;
    private String provisoType;

    public int createSubmissionProviso() {
        int output = 0;
        int rid=0;
        String submissionProviso="";
        try {
            SubmissionProvisoAgent submissionProvisoAgent = EJBUtil.getSubmissionProvisoAgentHome();
            SubmissionProvisoBean submissionProvisoBean = createEntityBean();
              
            this.setId(submissionProvisoAgent.createSubmissionProviso(submissionProvisoBean));
            output = this.getId();
            CommonDAO cDao = new CommonDAO();
            AuditRowEresJB auditRowEresJB = new AuditRowEresJB();
        	AuditUtils audittrails = new AuditUtils();
        	submissionProviso=this.submissionProviso==null?"":this.submissionProviso;
        	if(!submissionProviso.equals("")){
            cDao.updateClob(this.submissionProviso,"er_submission_proviso","proviso"," where pk_submission_proviso = " + output);
            rid = auditRowEresJB.getRidForDeleteOperation(this.getId(), EJBUtil.stringToNum(this.getCreator()), "er_submission_proviso", "pk_submission_proviso");
			audittrails.deleteRaidList("eres", rid, this.getCreator(), "U");
        	}
        } catch(Exception e) {
            Rlog.fatal("submission", "Error in SubmissionProvisoJB.createSubmissionProviso "+e);
            output = -1;
        }
        return output;
    }

    public int removeSubmissionProviso() {

        int output;

        try {

        	SubmissionProvisoAgent pRObj = EJBUtil
                    .getSubmissionProvisoAgentHome();
            output = pRObj.removeSubmissionProviso(this.id);
            return output;

        } catch (Exception e) {
            Rlog.fatal("submisison", "in SubmissionProvisoJB removeSubmissionProviso() method" + e);
            return -1;
        }

    }
 // Overloaded for INF-18183 ::: AGodara
    public int removeSubmissionProviso(Hashtable<String, String> auditInfo) {

        int output;

        try {

        	SubmissionProvisoAgent pRObj = EJBUtil
                    .getSubmissionProvisoAgentHome();
            output = pRObj.removeSubmissionProviso(this.id,auditInfo);
            return output;

        } catch (Exception e) {
            Rlog.fatal("submisison", "removeSubmissionProviso(Hashtable<String, String> auditInfo)" + e);
            return -1;
        }

    }
    
    private SubmissionProvisoBean createEntityBean() {
        return new SubmissionProvisoBean(this.id, EJBUtil.stringToInteger(this.fkSubmission), EJBUtil.stringToInteger(this.fkSubmissionBoard),EJBUtil.stringToInteger(this.fkSubmissionStatus),this.provisoflag,
                DateUtil.stringToDate(this.provisoDate) , EJBUtil.stringToInteger(this.provisoEnteredBy),this.submissionProviso, 
                EJBUtil.stringToInteger(this.creator), EJBUtil.stringToInteger(this.lastModifiedBy), this.ipAdd,this.provisoType);
    }
    
    public int updateSubmissionProviso() {
        int output;
        int rid=0;
        try {

        	SubmissionProvisoAgent pRObj = EJBUtil
            .getSubmissionProvisoAgentHome();
        	
            output = pRObj.updateSubmissionProviso(this.createEntityBean());
            CommonDAO cDao = new CommonDAO();
            AuditRowEresJB auditRowEresJB = new  AuditRowEresJB();
            AuditUtils audittrails =new AuditUtils();
            cDao.updateClob(this.submissionProviso,"er_submission_proviso","proviso"," where pk_submission_proviso = " + this.id);
            rid = auditRowEresJB.getRidForDeleteOperation(this.getId(), EJBUtil.stringToNum(this.getLastModifiedBy()), "er_submission_proviso", "pk_submission_proviso");
			audittrails.deleteRaidList("eres", rid, this.getLastModifiedBy(), "U");      
        } catch (Exception e) {
            Rlog.debug("submisison",
                    "EXCEPTION IN updateSubmissionProviso"
                            + e);
            return -2;
        }
        return output;
    }
    
    
    public SubmissionProvisoBean getSubmissionProvisoDetails() {
    	SubmissionProvisoBean anotherBean = null;
        try {

        	SubmissionProvisoAgent pRObj = EJBUtil
            .getSubmissionProvisoAgentHome();
        	
        	anotherBean = pRObj.getSubmissionProvisoDetails(this.id);
            
             } catch (Exception e) {
            Rlog.fatal("submission", "EXCEPTION IN getSubmissionProvisoDetails" + e);
        }

        if (anotherBean != null) {
        	setFkSubmission(anotherBean.getFkSubmission().toString());
            setFkSubmissionBoard(anotherBean.getFkSubmissionBoard().toString());
            setFkSubmissionStatus(anotherBean.getFkSubmissionStatus().toString());
            setprovisoflag(anotherBean.getprovisoflag().toString());
            setSubmissionProvisoDate(DateUtil.dateToString(anotherBean.getProvisoDate()));
            
            setProvisoEnteredBy(EJBUtil.integerToString(anotherBean.getProvisoEnteredBy()));
            setSubmissionProviso(anotherBean.getSubmissionProviso());
            setCreator(anotherBean.getCreator().toString());
            
            if ( anotherBean.getLastModifiedBy() != null )
            {
            	setLastModifiedBy(anotherBean.getLastModifiedBy().toString());
            }	
            
            setIpAdd(anotherBean.getIpAdd());
            setProvisoType(anotherBean.getProvisoType());
            
        }

        return anotherBean;

    }
    

    /** Returns Provisos added for a submission and Submission board */
    public ArrayList getSubmissionProvisos(int fkSubmission, int fkSubmissionBoard )
    {
    	ArrayList arReturn = new ArrayList();
        try {
        	SubmissionProvisoAgent pRObj = EJBUtil
            .getSubmissionProvisoAgentHome();
        	
        	arReturn = pRObj.getSubmissionProvisos(fkSubmission, fkSubmissionBoard);
    
        		
        } catch (Exception e) {
            Rlog.fatal("submission", "Exception in getSubmissionProvisos" + e);
            

        }

    	return arReturn;
    }
    
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getFkSubmission() {
        return String.valueOf(fkSubmission);
    }
    public void setFkSubmission(String fkSubmission) {
        this.fkSubmission = fkSubmission;
    }
    public String getFkSubmissionBoard() {
        return String.valueOf(fkSubmissionBoard);
    }
    public String getFkSubmissionStatus() {
        return String.valueOf(fkSubmissionStatus);
    }
    public String getprovisoflag() {
        return String.valueOf(provisoflag);
    }
    public void setFkSubmissionBoard(String fkSubmissionBoard) {
        this.fkSubmissionBoard = fkSubmissionBoard;
    }
    public void setFkSubmissionStatus(String fkSubmissionStatus) {
        this.fkSubmissionStatus = fkSubmissionStatus;
    }
    public void setprovisoflag(String provisoflag) {
        this.provisoflag = provisoflag;
    }
    public String getSubmissionProvisoDate() {
        return provisoDate;
    }
    public void setSubmissionProvisoDate(String submissionProvisoDate) {
        if (submissionProvisoDate == null || submissionProvisoDate.length() == 0) {
            this.provisoDate = null;
        } else {
            this.provisoDate = submissionProvisoDate;
        }
    }
 
    public String getCreator() {
        return String.valueOf(creator);
    }
    public void setCreator(String creator) {
        this.creator = creator;
    }
    public String getLastModifiedBy() {
        return String.valueOf(lastModifiedBy);
    }
    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }
    public String getIpAdd() {
        return ipAdd;
    }
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

  public String getProvisoEnteredBy() {
        return provisoEnteredBy;
    }

    public void setProvisoEnteredBy(String provisoEnteredBy) {
        this.provisoEnteredBy = provisoEnteredBy;
    }
 

    public String getSubmissionProviso() {
        return submissionProviso;
    }

    public void setSubmissionProviso(String submissionProviso) {
        this.submissionProviso = submissionProviso;
    }

  
    public String getProvisoType() {
        return provisoType;
    }

    public void setProvisoType(String provisoType) {
        this.provisoType = provisoType;
    }


}