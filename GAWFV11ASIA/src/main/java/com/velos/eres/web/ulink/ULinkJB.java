/*

 * Classname : ULinkJB.java

 * 

 * Version information

 *
 * Date 03/15/2001

 * 

 * Copyright notice: Aithent Technologies 
 */

package com.velos.eres.web.ulink;

import java.util.Hashtable;

import org.json.JSONArray;
import org.json.JSONObject;

import com.velos.eres.business.common.UsrLinkDao;
import com.velos.eres.business.ulink.impl.ULinkBean;
import com.velos.eres.service.ulinkAgent.ULinkAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * 
 * 
 * 
 * 
 * 
 * Client side bean for the User Links
 * 
 * 
 * 
 * 
 * 
 * @author Sonia Sahni
 * 
 * 
 * 
 * 
 * 
 */

public class ULinkJB {

    /***************************************************************************
     * * * * * the Link Id * * * *
     */

    private int lnkId;

    /***************************************************************************
     * * * * * the User id * * * *
     */

    private String lnkUserId;

    /***************************************************************************
     * * * * * the Account ID * * * *
     */

    private String lnkAccId;

    /***************************************************************************
     * * * * * the Link URI * * * *
     */

    private String lnkURI;

    /***************************************************************************
     * * * * * the Link Description * * * *
     */

    private String lnkDesc;

    /***************************************************************************
     * * * * * the Link Group Name, so that links can be put under user defined
     * headings * * * *
     */

    private String lnkGrpName;

    /* * * creator * */

    private String creator;

    /* * * last modified by * */

    private String modifiedBy;

    /* * * IP Address * */

    private String ipAdd;

    private String lnkType;

    // END OF MEMBER DECLARATION

    // GETTER SETTER METHODS

    public int getLnkId()

    {

        return this.lnkId;

    }

    public void setLnkId(int lnkId)

    {

        this.lnkId = lnkId;

    }

    public String getLnkAccId()

    {

        return this.lnkAccId;

    }

    public void setLnkAccId(String lnkAccId)

    {

        this.lnkAccId = lnkAccId;

    }

    public String getLnkUserId()

    {

        return this.lnkUserId;

    }

    public void setLnkUserId(String lnkUserId)

    {

        this.lnkUserId = lnkUserId;

    }

    public String getLnkDesc()

    {

        return this.lnkDesc;

    }

    public void setLnkDesc(String lnkDesc)

    {

        this.lnkDesc = lnkDesc;

    }

    public String getLnkGrpName()

    {

        return this.lnkGrpName;

    }

    public void setLnkGrpName(String lnkGrpName)

    {

        this.lnkGrpName = lnkGrpName;

    }

    public String getLnkURI()

    {

        return this.lnkURI;

    }

    public void setLnkURI(String lnkURI)

    {

        this.lnkURI = lnkURI;

    }

    /**
     * 
     * @return Creator
     * 
     */

    public String getCreator() {

        return this.creator;

    }

    /**
     * 
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     * 
     */

    public void setCreator(String creator) {

        this.creator = creator;

    }

    /**
     * 
     * @return Last Modified By
     * 
     */

    public String getModifiedBy() {

        return this.modifiedBy;

    }

    /**
     * 
     * @param modifiedBy
     *            The value that is required to be registered as record last
     *            modified by
     * 
     */

    public void setModifiedBy(String modifiedBy) {

        this.modifiedBy = modifiedBy;

    }

    /**
     * 
     * @return IP Address
     * 
     */

    public String getIpAdd() {

        return this.ipAdd;

    }

    /**
     * 
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     * 
     */

    public void setIpAdd(String ipAdd) {

        this.ipAdd = ipAdd;

    }

    public String getLnkType() {
        return this.lnkType;
    }

    public void setLnkType(String lnkType) {
        this.lnkType = lnkType;
    }

    // END OF GETTER SETTER METHODS

    public ULinkJB(int lnkId) {

        setLnkId(lnkId);

    }

    // Default Contructor

    public ULinkJB() {
    }

    /**
     * 
     * 
     * 
     * 
     * 
     * Full arguments constructor.
     * 
     * 
     * 
     * 
     * 
     * @param
     * 
     * 
     * 
     * 
     * 
     */

    public ULinkJB(int lnkId, String lnkAccId, String lnkUserId, String lnkURI,
            String lnkDesc, String lnkGrpName, String creator,

            String modifiedBy, String ipAdd, String lnkType) {

        Rlog.debug("ulink", "Begin User links ClientJB Constructor " + lnkId);

        setLnkId(lnkId);

        setLnkAccId(lnkAccId);

        setLnkDesc(lnkDesc);

        setLnkGrpName(lnkGrpName);

        setLnkURI(lnkURI);

        setLnkUserId(lnkUserId);

        setCreator(creator);

        setModifiedBy(modifiedBy);

        setIpAdd(ipAdd);

        setLnkType(lnkType);

        Rlog.debug("ulink", "User links ClientJB Constructor ");

    }

    /**
     * 
     * 
     * 
     * 
     * 
     * calls getULinkDetails() on the UlinkAgent facade and extracts this state
     * holder into the been attribute fields.
     * 
     * 
     * 
     * 
     * 
     */

    public ULinkBean getULinkDetails() {

        ULinkBean lsk = null;

        try {

            ULinkAgentRObj ulinkAgentRObj = EJBUtil.getULinkAgentHome();
            lsk = ulinkAgentRObj.getULinkDetails(this.lnkId);

        }

        catch (Exception e)

        {

            Rlog.fatal("ulink",
                    "EXCEPTION IN GETTING USERLINK DETAILS FROM SESSION BEAN"
                            + e);

        }

        if (lsk != null) {

            setLnkId(lsk.getLnkId());
            setLnkAccId(lsk.getLnkAccId());

            setLnkUserId(lsk.getLnkUserId());
            setLnkDesc(lsk.getLnkDesc());

            setLnkGrpName(lsk.getLnkGrpName());

            setLnkURI(lsk.getLnkURI());

            setCreator(lsk.getCreator());

            setModifiedBy(lsk.getModifiedBy());

            setIpAdd(lsk.getIpAdd());

            setLnkType(lsk.getLnkType());

        }

        return lsk;

    }

    /***************************************************************************
     * * * * * calls setCustomer() on the ReservationAgent facade. * * * *
     */

    public void setULinkDetails() {
        try {
            ULinkAgentRObj ulinkAgentRObj = EJBUtil.getULinkAgentHome();
            ulinkAgentRObj.setULinkDetails(this.createULinkStateKeeper());

        } catch (Exception e) {

            Rlog.fatal("ulink",
                    "EXCEPTION IN SETTING USERLINK DETAILS TO  SESSION BEAN"
                            + e);

        }

    }

    public int updateUlink()

    {
        int output;
        try {

            ULinkAgentRObj ulinkAgentRObj = EJBUtil.getULinkAgentHome();
            output = ulinkAgentRObj.updateULink(this.createULinkStateKeeper());

        }

        catch (Exception e) {

            Rlog.fatal("ulink",
                    "EXCEPTION IN SETTING USERLINK DETAILS TO  SESSION BEAN"
                            + e);
            return -2;

        }

        return output;

    }

    /*
     * 
     * 
     * 
     * 
     * 
     * places bean attributes into a StudyStateHolder.
     * 
     * 
     * 
     * 
     * 
     */

    public ULinkBean createULinkStateKeeper()

    {

        return new ULinkBean(lnkId, lnkUserId, lnkAccId, lnkURI, lnkDesc,
                lnkGrpName, creator, modifiedBy, ipAdd, lnkType);

    }

    public UsrLinkDao getULinkValuesByUserId(int userId) {

        UsrLinkDao usrLinkDao = new UsrLinkDao();

        try {

            Rlog.debug("ulink", "ULinkJB.getULinkValuesByUserId starting");

            ULinkAgentRObj uLinkAgentRObj = EJBUtil.getULinkAgentHome();

            Rlog.debug("ulink", "ULinkJB.getULinkValuesByUserId home");

            usrLinkDao = uLinkAgentRObj.getULinkValuesByUserId(userId);

            Rlog.debug("ulink", "ULinkJB.getULinkValuesByUserId after Dao");

            return usrLinkDao;

        } catch (Exception e) {

            Rlog.fatal("ulink",
                    "Error in getULinkValuesByUserId(int userId) in ULinkJB "
                            + e);

        }

        return usrLinkDao;

    }

    public UsrLinkDao getULinkValuesByAccountId(int accountId, String accType) {

        UsrLinkDao usrLinkDao = new UsrLinkDao();

        try {

            Rlog.debug("ulink", "ULinkJB.getULinkValuesByAccountId starting");

            ULinkAgentRObj uLinkAgentRObj = EJBUtil.getULinkAgentHome();

            Rlog.debug("ulink", "ULinkJB.getULinkValuesByAccountId remote");

            usrLinkDao = uLinkAgentRObj.getULinkValuesByAccountId(accountId,
                    accType);

            Rlog.debug("ulink", "ULinkJB.getULinkValuesByAccountId after Dao");

            return usrLinkDao;

        } catch (Exception e) {

            Rlog.fatal("ulink",
                    "Error in getULinkValuesByAccountId(int accountId) in ULinkJB "
                            + e);

        }

        return usrLinkDao;

    }

    // to delete links

    public int deleteUlink()

    {

        int output;

        try {

            ULinkAgentRObj uLinkAgentRObj = EJBUtil.getULinkAgentHome();

            output = uLinkAgentRObj.deleteULink(this.createULinkStateKeeper());

        }

        catch (Exception e) {

            Rlog.fatal("ulink", "EXCEPTION IN deleting u link " + e);
            return -2;

        }

        return output;

    }
   
    // Overloaded for INF-18183 -- AGodara
    public int deleteUlink(Hashtable<String, String> userInfo){
    	
    	int output;
        try {
            ULinkAgentRObj uLinkAgentRObj = EJBUtil.getULinkAgentHome();
            output = uLinkAgentRObj.deleteULink(this.createULinkStateKeeper(),userInfo);
        }
        catch (Exception e) {
            Rlog.fatal("ulink", "In method - deleteUlink(Hashtable<String, String> userInfo) " + e);
            return -2;
        }
        return output;
    }

    /*
     * 
     * 
     * 
     * 
     * 
     * generates a String of XML for the link details entry form.<br><br>
     * 
     * 
     * 
     * 
     * 
     * Note that it is planned to encapsulate this detail in another object.
     * 
     * 
     * 
     * 
     * 
     */

    public String toXML()

    {

        return new String("<?xml version=\"1.0\"?>");

    }// end of method
    
    private static final String TRUE_STR = "true";
    private static final String FALSE_STR = "false";
    private static final String ADD_MORE_KEY = "addMore";
    private static final String URI_KEY = "uri";
    private static final String DESC_KEY = "desc";
    private static final String LNK_ID_KEY = "lnkid";
    private static final String ARRAY_KEY = "array";
    private static final String HTML_KEY = "html";
    private static final String SECTION_KEY = "section";
    private void jsonPut(JSONObject jObj, String key, Object value) {
    	try {
    		jObj.put(key, value);
    	} catch(Exception e) {
    		Rlog.fatal("ulink", "putJson error for key="+key+" value="+value);
    	}
    }
    public String getMyLinksHTML(String userId) {
    	JSONObject jObj = new JSONObject();
    	JSONArray jArray = new JSONArray();
    	
		StringBuffer sb1 = new StringBuffer();
		if (StringUtil.isEmpty(userId)) { return sb1.toString(); }
		UsrLinkDao usrLinkDao = getULinkValuesByUserId(StringUtil.stringToNum(userId));
		if (usrLinkDao == null) { return sb1.toString(); }
		
		jsonPut(jObj, ADD_MORE_KEY, usrLinkDao.getLnksDescs().size() > 10 ? TRUE_STR : FALSE_STR);
		
		for (int iX=0; iX<usrLinkDao.getLnksDescs().size(); iX++) {
			if (iX > 9) { break; }
			JSONObject aLnk = new JSONObject();
			jsonPut(aLnk, URI_KEY, usrLinkDao.getLnksUris().get(iX));
			jsonPut(aLnk, DESC_KEY, usrLinkDao.getLnksDescs().get(iX));
			jsonPut(aLnk, LNK_ID_KEY, usrLinkDao.getLnksIds().get(iX));
			jsonPut(aLnk, SECTION_KEY, usrLinkDao.getLnksGrpNames().get(iX));
			try {
				jArray.put(aLnk);
			} catch(Exception e) {
				Rlog.fatal("ulink", "jArray.put error: e");
			}
		}
		jsonPut(jObj, ARRAY_KEY, jArray);
		
		sb1.append("<table width='100%' >");
		for (int iX=0; iX<usrLinkDao.getLnksDescs().size(); iX++) {
			if (iX > 9) { break; }
			sb1.append("<tr id='gadgetSample2_myLink-row-").append(iX).append("'><td>");
			try {
				sb1.append("<a target=\"_blank\" id=\"gadgetSample2_myLink").append(iX).append("\"");
				sb1.append(" href=\"").append(usrLinkDao.getLnksUris().get(iX)).append("\"");
				sb1.append(" title=\"").append(usrLinkDao.getLnksUris().get(iX)).append("\">");
				String desc1 = (String)usrLinkDao.getLnksDescs().get(iX);
				if (desc1.length() > 19) {
					sb1.append((StringUtil.htmlEncode(desc1)).substring(0, 19)).append("</a>");
					sb1.append("&nbsp;<img title='").append(StringUtil.htmlEncode(desc1));
					sb1.append("' src='./images/More.png' style='width:20px; height:10px;' border=0 />");
				} else {
					sb1.append(StringUtil.htmlEncode(desc1)).append("</a>");
				}
				sb1.append("</td><td nowrap='nowrap' align='right'>");
				sb1.append("<a style=\"opacity:0; filter:alpha(opacity=0);\" href=\"javascript:gadgetSample2_editMyLink(");
				sb1.append(iX+1);
				sb1.append(");\" id=\"gadgetSample2_editMyLink").append(iX).append("\">");
				sb1.append("<img id='gadgetSample2_myLink-edt-img-").append(iX);
				sb1.append("' class='headerImage' style='border:none; opacity:0; filter:alpha(opacity=0);' title='");
				sb1.append("Edit Link #".replaceAll("\'", "&#39;")).append(iX+1);
				sb1.append("' src='./images/edit.gif' border=0 />");
				sb1.append("</a>&nbsp;");
				sb1.append("<a style=\"opacity:0; filter:alpha(opacity=0);\" href=\"javascript:gadgetSample2_delMyLink(");
				sb1.append(iX+1);
				sb1.append(");\" id=\"gadgetSample2_delMyLink").append(iX).append("\">");
				sb1.append("<img id='gadgetSample2_myLink-del-img-").append(iX);
				sb1.append("' class='headerImage' style='border:none; opacity:0; filter:alpha(opacity=0);' title='");
				sb1.append("Delete Link #".replaceAll("\'", "&#39;")).append(iX+1);
				sb1.append("' src='./images/delete.gif' border=0 />");
				sb1.append("</a>");
			} catch(Exception e) {
				e.printStackTrace();
			}
			sb1.append("</td></tr>");
		}
		if (usrLinkDao.getLnksDescs().size() > 10) {
			sb1.append("<tr><td colspan='2' align='right'><a href='ulinkBrowser.jsp?srcmenu=tdMenuBarItem13&selectedTab=2'");
			sb1.append(" title='").append("More My Links").append("'>");
			sb1.append("More My Links").append("</a>");
			sb1.append("</td></tr>");
		}
		sb1.append("</table>");
		jsonPut(jObj, HTML_KEY, sb1.toString());
		System.out.println("jObj="+jObj.toString());
		return jObj.toString();
    }

}// end of class
