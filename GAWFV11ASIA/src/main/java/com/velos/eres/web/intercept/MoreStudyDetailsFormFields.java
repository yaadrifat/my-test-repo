package com.velos.eres.web.intercept;

public enum MoreStudyDetailsFormFields {
       PROTOCOL_TEMPLATE("spnsrdProt",  "Protocol Template"), 
       STUDY_INCLUDES("stdyIncludes", "study includes"), 
       STUDY_ROLE_MDACC_PI("studyCond1", "Role of MDACC Principal Investigator"),
       //STUDY OBJECTIVES SECTION
       SELECT_OTHER_OBJECTIVES("selOthrObjEntry", "SelectOtherObjectives for entry"), 
       STUDY_SEC_OBJ_TEXT("secObjective", "Secondary Objective"), 
       STUDY_EXP_OBJ_TEXT("explrtObjctv","Exploratory Objective"), 
       STUDY_BIO_OBJ_TEXT("biomrkrObj", "Biomarker Objective"), 
       RANDOMIZATION_NOTES("rndmnTrtGrp", "Randomization Notes"),
       ESTIMATED_ENROLLED_MDACC("accrual1" , "Estimated number enrolled per month at MDACC"),
       TOTAL_ENROLLED_ALL_SITES("accrual12" , "Total number enrolled at all sites"),
       DRUG_AGENT1("drugAgent1" , "Drug Agent1"),
       DRUG_AGENT_DOSE("dose" , "Dose"),
       DRUG_DOSE_RATIONALE("doseRationale" , "Dose Rationale"),
       DRUG_ROUTE("route" , "Drug Route"),
       DRUG_MANUFACTURER("drugMnftr1" , "Drug Manufacturer"),
       STUDY_INVOLVE_RECOMB_DNA("biosafety1", "Does the study involve the use of Recombinant DNA technology"),
       STUDY__INVOLVE_HUMAN_ANIMAL_TISSUE("biosafety3", "Does the study involve human/animal tissue other than blood derived hematopoietic stem cells"),
       STUDY_INVOLVE_RADIO_ISOTOPE("radMat1", "Does the study involve the administration of radioisotopes or a radioisotope labeled agent"),
       AUTHORIZED_USER("authrzdUsr","Authorized User"),
       AUTHORIZATION_NUMBER("authrztnNum" ,"Authorization Number (or state that authorization application is under Radiation Safety Committee Review)"),
       STUDY_ADMINISTER_RADIO_ACTIVE("incldAdminstr" , 
               " Does this protocol include the administration of a radioactive compound(or drug) to a patient intended to obtain basic information regarding metabolism"),
        RADIO_ACTIVE_FDA_APPROVED("radioCompAvail" , "Is the radioactive compound (or drug) FDA approved and/or commercially available?"),     
        FACILITY_INVESTIGATIONAL_AGENT_BOUND("faciltyInvstg" , 
            "Name the facility where the investigational agent is labeled or bound to the  radioisotope"),  
       IBC_PROTOCOL_NUMBER("biosafety4","Provide the IBC protocol number or contact the IBC coordinator for information at 713-563-3879"),
       
       STUDY_INVOLVE_ORGANISMS_INFECTIOUS_TO_HUMANS("biosafety2","Does this study involve the use of organisms that are infectious to humans"),
       STUDY_INVOLVE_ORGANISMS_INFECTIOUS_TO_HUMANS_COMMENT("biosafety2Comm", "Comment"),
       STUDY_REQUIRE_NIH_RAC_REVIEW("nihRacRev","Does the study require NIH/RAC review?"),
       STUDY_REQUIRE_NIH_RAC_REVIEW_COMMENT("nihRacRevComm","Comment"),
       
       TREATMENT_DURATION("trtmntDuration","Treatment duration"),
       USING_TISSEL_OR_EVICEL("biosafety5","Are you using Tissel or Evicel or similar?"),
       STUDY_DEVICE1("deviceAgent1", "Device 1"),
       STUDY_DEVICE_MANUFACTURER1("devMnftr1" , "Manufacturer"),
       
       //Withdrawl and replacement of participants
       //CRITERIA_FOR_REPLACEMENT("rplSubject" ,"Criteria for Replacement of Participants"),
       OFF_STUDY_CRITERIA("offStdyCri" , "Off Study Criteria"),
       CRITERIA_FOR_REMOVAL("stdRemCriteria" , "Criteria for Removal of Participants"),
       SPECIFY_OTHER_CRITERIA("OtrStdRemCrit", "Specify other criteria"),
       
       //Study Evaluation and Procedures
       FRESH_BIOPSY_NEEDED("freshBiopsyNeed", "Are fresh biopsies needed?"),
       FRESH_BIOPSY_NEEDED_YES("freshBiopsyYes", "If yes, please describe"),
       
       SCREENING_BASELINE("section_4_5_1","Screening/Baseline"),
       ON_STUDY_TREATMENT_EVALUATIONS("section_4_5_2" , "On-study/Treatment Evaluations"),
       END_OF_THERAPY("section_4_5_3", "End of Therapy Evaluations"),
       FOLLOW_UP("section_4_5_4", "Follow-up"),
       STUDY_CALENDER("section_4_5", "Study Calendar/Schedule of Events"),
       
       REPORTING_TO_SPONSOR("repSponsr" , "Reporting to Sponsor"),
       DATA_SAFETY_MONITORING_PLAN("section_6_5", "Data Safety and Monitoring Plan"),
       FINANCIAL_SUPPORT_SPONSOR_NAME("sponsorName1","Sponsor Name1"),
       FINANCIAL_SUPPORT_TYPE_OF_FUNDING("fundingType1","Type of funding"),
       FINANCIAL_SUPPORT_CANCER_SUPPORT_GRANT("finSupp4", "Which Cancer Center Support Grant (CCSG) - NCI Core Grant Program does this protocol relate to?"),
       REFERENCES("references", "References"),
       NOMINATED_REVIEWER("nom_rev","Nominated Reviewer"),
       ALTERNATE_NOMINATED_REVIEWER("nom_alt_rev", "Nominated Reviewer (alternate)"),
       DEPARTMENT_CHAIR("dept_chairs", "Department Chair"),
       //STATISTICAL ANALYSIS SECTION
       PRIMARY_END_POINT("statCons1", "Primary Endpoints"),
       DID_MCDACC_BIOSTAT_PARTICIPATE( "proDes2","Did an MDACC Biostatistician participate in the study design?"),
       IS_PLANNED_INTERIM_ANALYSIS("protoMontr1","Does this protocol have a planned interim analysis?"),
       DATA_SAFETY_MONITORING_BOARD("dsmbInt","Data Safety and Monitoring Board"),
       DSMB_COMPOSITION("dsmbCompostn","DSMB Composition"),
       STATISTICAL_ANALYSIS_PLAN("statAnalPlan","Statistical Analysis Plan"),
       BAYSEAN_COMPONENT_INCLUDED("bayesianComp","Bayesian component included in protocol"),
       SUMMARY_OF_PLANNED_INTERIM_ANALYSIS("protoMontr3","Summary of planned interim analyses including stopping rules for futility, efficacy and/or toxicity"),
       RATIONALE_FOR_NO_INTERIM_ANALYSIS("protoMontr4" ,"Rationale for no interim analyses"),
       //BIOMARKER AND CORRELATIVE STUDIES SECTION
       ARE_YOU_MEASURING_BIOMARKERS("bioMark1","Are you measuring Biomarkers?"),
       ARE_THEY_KNOWN_BIOMARKERS("knownBiomrkr","Are they known Biomarkers?"),
       BIOMARKER_TYPE("biomrkrType", "Biomarker type"),
       SPECIFY_OTHER_BIOMARKER("biomrkrSpeOth","Specify other"),
       TEST_NAME("testNameQ", "What are you calling the test? Use commonly accepted test"),
       BIOMARKER_NAME("biomrkrCall","What are you calling the biomarker?"),
       BIOMARKER_LEVEL("levelQues" ,"What level are you trying to look at?"),
       WHAT_ARE_YOU_MEASURING("measureQues", "Where are you measuring?"),
       TIMEPOINTS("timePoints","Time points"),
       METHODOLOGY("methodology","Methodology-How"),
       //STUDY IND/IDE SECTION
       IND_IDE_INFO_AVAILABLE("cbIndIdeGrid" , "IND/IDE Information Available?"),
       IND_IDE_TYPE("indIdeType", "IND/IDE Types"),
       IND_IDE_NUMBER("indIdeNumber" ," IND/IDE #"),
       IND_IDE_GRANTOR("indIdeGrantor" , "IND/IDE Grantor"),
       IND_IDE_HOLDER("indIdeHolder", "IND/IDE Holder Type"),
       NIH_NCI_DIVISION("programCodeDescId0","NIH Institution, NCI Division/Program Code (If applicable)"),
       EXPANDED_ACCESS("cbExpandedAccess" , "Expanded Access"),
       EXPANDED_ACCESS_TYPE("indIdeAccessType", "Expanded Access Type (If applicable)"),
       //  STUDY BACKGROUND
       STUDY_RATIONALE("section_3_2" , "Study Rationale"),
       CLINICAL_DATA_TO_DATE("section_1_3" , "Clinical Data to Date"),
       
       //industry template specific fields
       SECONDARY_DEPARTMENT("studyCond2" ,"Secondary Department"),
       DOES_PROTOCOL_REQUIRE_IND("prot_req_ind" , "Does this protocol require an IND?"),
       REQUEST_EXPEDITED_BIOSTAT_REVIEW("expdBiostat","Are you requesting an expedited biostatistical review for this protocol?"),
       //safety and adverse events
       WHAT_CRITERIA_ADVERSE_EVENTS ("critAdvEvents" ,"What criteria are you using for classifying Adverse Events?"),
       WHAT_CRITERIA_ADVERSE_EVENTS_OTHER("critAdvEvntsOth","Other"),
       PROVIDE_LIST_ANTICIPATED_AE("invAntcpAEs","If investigational, provide list of anticipated AEs"),
       COMMERCIAL_DRUG_INSERT("commDrugInsrt" ,"If commercially available, include drug insert"),
       REPORT_OF_SAE_UNANTICIPATED_PROBLEMS("repSAEs", "Reports of SAEs and Unanticipated Problem"),
       
       STUDY_BLINDING_NOTES("blndTrtGrp", "Blinding notes"),
       TREATMENT_ARMS("trtmntArms","Treatment Arms"),
       SAMPLE_SIZE_JUSTIFICATION("statCons2", "Sample size justification"),
       
       //subject selection section 
       SUBJECT_INCLUSION_CREITERIA("incluCriteria", "Inclusion Criteria"),
       SUBJECT_INCLUSION_CREITERIA_NA("incluCriteriaNa" ,"Inclusion Criteria N/A"),  
       SUBJECT_EXCLUSION_CREITERIA("excluCriteria" ,"Exclusion Criteria"), 
       SUBJECT_EXCLUSION_CREITERIA_NA("excluCriteriaNa","Exclusion Criteria N/A");
       
        
       private String codelst_Subtyp;
       private String field_Label ;

       private MoreStudyDetailsFormFields(String codelst_Subtyp , String field_Label) {
               this.codelst_Subtyp = codelst_Subtyp;
               this.field_Label = field_Label;
       }
       
       public String getCodelst_subtyp(){
        
        return this.codelst_Subtyp;
       }
       
       public String getfield_label(){
        
        return this.field_Label;
       }
};  

