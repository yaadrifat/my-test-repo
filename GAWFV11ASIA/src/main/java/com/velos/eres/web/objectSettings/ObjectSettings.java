/*
 * Classname : ObjectSettings
 *
 * Version information: 1.0
 *
 * Date: 05/04/2009
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Jnanamay Majumdar
 */

package com.velos.eres.web.objectSettings;
import java.math.BigDecimal;

/**
 *  Wrapper class for ER_OBJECT_SETTINGS table records.
 *  Each instance of this class represents a row of that table.
 */
public class ObjectSettings implements Comparable<ObjectSettings> {

    private String pkObjSetting;
    private String objType;
    private String objSubType;
    private String objName;
    private String objSequence;
    private String objVisible;
    private String objDispTxt;
    private String objAcc;
    private String objUrl;

    public String getPkObjSetting() {
        return pkObjSetting;
    }
    public void setPkObjSetting(String pkObjSetting) {
        this.pkObjSetting = pkObjSetting;
    }
    public String getObjType() {
        return objType;
    }
    public void setObjType(String objType) {
        this.objType = objType;
    }
    public String getObjSubType() {
        return objSubType;
    }
    public void setObjSubType(String objSubType) {
        this.objSubType = objSubType;
    }
    public String getObjName() {
        return objName;
    }
    public void setObjName(String objName) {
        this.objName = objName;
    }
    public String getObjSequence() {
        return objSequence;
    }
    public void setObjSequence(String objSequence) {
        this.objSequence = objSequence;
    }
    public String getObjVisible() {
        return objVisible;
    }
    public void setObjVisible(String objVisible) {
        this.objVisible = objVisible;
    }
    public String getObjDispTxt() {
        return objDispTxt;
    }
    public void setObjDispTxt(String objDispTxt) {
        this.objDispTxt = objDispTxt;
    }
    public String getObjAcc() {
        return objAcc;
    }
    public void setObjAcc(String objAcc) {
        this.objAcc = objAcc;
    }
    public String getObjUrl() {
        return objUrl;
    }
    public void setObjUrl(String objUrl) {
        this.objUrl = objUrl;
    }
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("pk=").append(pkObjSetting).append(", objType=").append(objType)
        .append(", objSubType=").append(objSubType).append(", objName=").append(objName)
        .append(", objSequence=").append(objSequence).append(", objVisible=").append(objVisible)
        .append(", objDispTxt=").append(objDispTxt).append(", objAcc=").append(objAcc).append(", objUrl=").append(objUrl);
        return sb.toString();
    }
    public int compareTo(ObjectSettings o) {
        try {
            BigDecimal thisSeq = new BigDecimal(this.getObjSequence());
            return thisSeq.compareTo(new BigDecimal(o.getObjSequence()));
        } catch (Exception e) {}
        return 0;
    }

}