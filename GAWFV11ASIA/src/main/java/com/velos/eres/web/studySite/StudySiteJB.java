/*
 * Classname : StudySiteJB
 *
 * Version information: 1.0
 *
 * Date: 10/27/2004
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Anu Khanna
 */

package com.velos.eres.web.studySite;

/* IMPORT STATEMENTS */

import java.util.Hashtable;

import com.velos.eres.business.common.StudySiteDao;
import com.velos.eres.business.studySite.impl.StudySiteBean;
import com.velos.eres.service.studySiteAgent.StudySiteAgentRObj;
import com.velos.eres.service.studySiteAgent.impl.StudySiteAgentBean;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * Client side bean for StudySite
 *
 * @author Anu Khanna
 * @version 1.0 10/27/2004
 */
/*
 * Modified by Sonia Abrol, 03/22/05, for a new attribute
 * currentSitePatientCount - The number of patients enrolled (with any status)
 * for the study -site combination on 03/23/04, added findBeanByStudySite
 */

public class StudySiteJB {

    /**
     * The primary key:
     */

    private int studySiteId;

    /**
     * The foreign key reference to er_study
     */
    private String studyId;

    /**
     * The foreign key reference to er_site
     */
    private String siteId;

    /**
     * The Site Type
     */

    private String studySiteType;

    /**
     * The study Site Public
     */

    private String studySitePublic;

    /**
     * The study site rep Include
     */

    private String repInclude;

    /**
     * The study site sample size
     */

    private String sampleSize;

    /**
     * The creator
     */

    private String creator;

    /**
     * The last modification by: LAST_MODIFICATION_BY
     */

    private String modifiedBy;

    /**
     * The IP Address : IP_ADD
     */

    private String ipAdd;

    /**
     * The number of patients enrolled (with any status) for the study -site
     * combination
     */

    private String currentSitePatientCount;



    /** If Enrollment is review based for this site*/
    public String isReviewBasedEnrollment;


    /**
     * List of user PKs; these users will receive the notification if a new 'enrolled' status is added
     */
    public String sendEnrNotificationTo;

    /**
     * List of user PKs; these users will receive the notification if a new enrollment status related status is added.
     * example - 'enrollment approved','enrollment denied'
     */

    public String sendEnrStatNotificationTo;

    // /////////////////////////////////////////////////////////////////////////////////

    // GETTER AND SETTER METHODS

    /**
     * Get the Study Site ID
     *
     * @return int study site id
     */
    public int getStudySiteId() {
        return this.studySiteId;
    }

    /**
     * sets Study Site ID
     *
     * @param studySiteId
     */
    public void setStudySiteId(int studySiteId) {
        this.studySiteId = studySiteId;
    }

    /**
     * Get study id
     *
     * @return String studyid
     */

    public String getStudyId() {
        return this.studyId;
    }

    /**
     * sets Study Id
     *
     * @param studyId
     */
    public void setStudyId(String studyId) {
        this.studyId = studyId;
    }

    /**
     * Get site id
     *
     * @return String siteId
     */

    public String getSiteId() {
        return this.siteId;
    }

    /**
     * sets Site Id
     *
     * @param siteId
     */
    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    /**
     * Get study Site Type
     *
     * @return String studySiteType
     */

    public String getStudySiteType() {
        return this.studySiteType;
    }

    /**
     * sets study Site Type
     *
     * @param studySiteType
     */
    public void setStudySiteType(String studySiteType) {
        this.studySiteType = studySiteType;
    }

    /**
     * Get study site sample Size
     *
     * @return String sampleSize
     */

    public String getSampleSize() {
        return this.sampleSize;
    }

    /**
     * sets Study site sample Size
     *
     * @param sampleSize
     */

    public void setSampleSize(String sampleSize) {
        this.sampleSize = sampleSize;

    }

    /**
     * Get study Site Public
     *
     * @return String studySitePublic
     */

    public String getStudySitePublic() {
        return this.studySitePublic;
    }

    /**
     * sets study Site Public
     *
     * @param studySitePublic
     */

    public void setStudySitePublic(String studySitePublic) {
        this.studySitePublic = studySitePublic;
    }

    /**
     * Get report Include flag
     *
     * @return String repInclude
     */

    public String getRepInclude() {
        return this.repInclude;
    }

    /**
     * sets report Include flag
     *
     * @param repInclude
     */

    public void setRepInclude(String repInclude) {
        this.repInclude = repInclude;
    }

    /**
     * @return The Creator of the Form Section
     */

    public String getCreator() {
        return this.creator;
    }

    /**
     *
     * @param to
     *            set the Creator of the Form Section
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param the
     *            set unique ID of the User
     */

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return the IP Address of the Form Section request
     */

    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param set
     *            the IP Address of the Form Section request
     */

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
     * Returns the value of currentSitePatientCount.
     */
    public String getCurrentSitePatientCount() {
        return currentSitePatientCount;
    }

    /**
     * Sets the value of currentSitePatientCount.
     *
     * @param currentSitePatientCount
     *            The value to assign currentSitePatientCount.
     */
    public void setCurrentSitePatientCount(String currentSitePatientCount) {
        this.currentSitePatientCount = currentSitePatientCount;
    }




    public String getIsReviewBasedEnrollment() {
		return isReviewBasedEnrollment;
	}

	public void setIsReviewBasedEnrollment(String isReviewBasedEnrollment) {
		this.isReviewBasedEnrollment = isReviewBasedEnrollment;
	}

	public String getSendEnrNotificationTo() {
		return sendEnrNotificationTo;
	}

	public void setSendEnrNotificationTo(String sendEnrNotificationTo) {
		this.sendEnrNotificationTo = sendEnrNotificationTo;
	}

	public String getSendEnrStatNotificationTo() {
		return sendEnrStatNotificationTo;
	}

	public void setSendEnrStatNotificationTo(String sendEnrStatNotificationTo) {
		this.sendEnrStatNotificationTo = sendEnrStatNotificationTo;
	}

    // //////////////////////////////////////////////////////////////////

    // END OF THE GETTER AND SETTER METHODS

	/**
     * Constructor
     *
     * @param studySiteId:
     *            Unique Id constructor
     *
     */

    public StudySiteJB(int studySiteId) {
        setStudySiteId(studySiteId);
    }

    /**
     * Default Constructor
     */

    public StudySiteJB() {
        Rlog.debug("studySite", "StudySiteJB.StudySiteJB() ");
    }

    public StudySiteJB(int studySiteId, String studyId, String siteId,
            String studySiteType, String sampleSize, String studySitePublic,
            String repInclude, String creator, String modifiedBy, String ipAdd,
            String currentSitePatientCount, String enrType, String notifyEnrTo, String notifyEnrStatTo) {
        setStudySiteId(studySiteId);
        setStudyId(studyId);
        setSiteId(siteId);
        setStudySiteType(studySiteType);
        setSampleSize(sampleSize);
        setStudySitePublic(studySitePublic);
        setRepInclude(repInclude);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setCurrentSitePatientCount(currentSitePatientCount);
        setIsReviewBasedEnrollment(enrType);
    	setSendEnrNotificationTo(notifyEnrTo);
    	setSendEnrStatNotificationTo(notifyEnrStatTo);
    }

    /**
     * Calls getStudySiteDetails of StudyId Session Bean: StudySiteAgentBean
     *
     * @return StudySiteStatKeeper
     * @see StudySiteAgentBean
     */

    public StudySiteBean getStudySiteDetails() {
        StudySiteBean studySitesk = null;
        try {
            StudySiteAgentRObj studySiteAgentRObj = EJBUtil
                    .getStudySiteAgentHome();

            studySitesk = studySiteAgentRObj
                    .getStudySiteDetails(this.studySiteId);
            Rlog.debug("studySite",
                    "StudySiteJB.getStudySiteDetails() StudySiteStateKeeper "
                            + studySitesk);
        } catch (Exception e) {
            Rlog.fatal("studySite", "Error in StudySite getStudySiteDetails"
                    + e);
        }

        if (studySitesk != null) {
            setStudySiteId(studySitesk.getStudySiteId());
            setStudyId(studySitesk.getStudyId());
            setSiteId(studySitesk.getSiteId());
            setStudySiteType(studySitesk.getStudySiteType());
            setSampleSize(studySitesk.getSampleSize());
            setStudySitePublic(studySitesk.getStudySitePublic());
            setRepInclude(studySitesk.getRepInclude());
            setCreator(studySitesk.getCreator());
            setModifiedBy(studySitesk.getModifiedBy());
            setIpAdd(studySitesk.getIpAdd());
            setCurrentSitePatientCount(studySitesk.getCurrentSitePatientCount());

            setIsReviewBasedEnrollment(studySitesk.getIsReviewBasedEnrollment());
        	setSendEnrNotificationTo(studySitesk.getSendEnrNotificationTo());
        	setSendEnrStatNotificationTo(studySitesk.getSendEnrStatNotificationTo());

        }

        return studySitesk;

    }

    /**
     * Calls setStudySiteDetails() of Study Site Session Bean:
     * StudySiteAgentBean
     *
     */

    public int setStudySiteDetails() {
        int toReturn;
        try {

            StudySiteAgentRObj studySiteAgentRObj = EJBUtil
                    .getStudySiteAgentHome();

            StudySiteBean tempStateKeeper = new StudySiteBean();

            tempStateKeeper = this.createStudySiteStateKeeper();

            toReturn = studySiteAgentRObj.setStudySiteDetails(tempStateKeeper);
            this.setStudySiteId(toReturn);

            Rlog.debug("studySite", "StudySIteJB.setStudySiteDetails()");

            return toReturn;

        } catch (Exception e) {
            Rlog.fatal("studySite",
                    "Error in setStudySiteDetails() in StudySiteJB " + e);
            return -2;
        }
    }

    /**
     *
     * @return a statekeeper object for the StudySite Record with the current
     *         values of the bean
     */
    public StudySiteBean createStudySiteStateKeeper() {

        return new StudySiteBean(studySiteId, studyId, siteId, studySiteType,
                sampleSize, studySitePublic, repInclude, creator, modifiedBy,
                ipAdd, currentSitePatientCount,isReviewBasedEnrollment,sendEnrNotificationTo,sendEnrStatNotificationTo);

    }

    /**
     * Calls updateStudySite() of StudySite Session Bean: StudySiteAgentBean
     *
     * @return The status as an integer
     */
    public int updateStudySite() {
        int output;
        try {
            StudySiteAgentRObj studySiteAgentRObj = EJBUtil
                    .getStudySiteAgentHome();

            output = studySiteAgentRObj.updateStudySite(this
                    .createStudySiteStateKeeper());
        } catch (Exception e) {
            Rlog.debug("studySite",
                    "EXCEPTION IN SETTING studySite DETAILS TO  SESSION BEAN"
                            + e);
            return -2;
        }
        return output;
    }

    public int removeStudySite() {

        int output;

        try {

            StudySiteAgentRObj studySiteAgentRObj = EJBUtil
                    .getStudySiteAgentHome();

            output = studySiteAgentRObj.removeStudySite(this.studySiteId);
            return output;

        } catch (Exception e) {
            Rlog.fatal("studySite", "in StudySiteJB removeStudySite() method"
                    + e);
            return -1;
        }

    }

    /**
     * Method to get the count of rows in table for a study, organization and
     * type.. Calls getCntForSiteTypeInStudy of StudySite Session Bean
     *
     * @param studyId
     *            PK of study
     * @param siteId :
     *            organization Id
     * @returns StudySiteDao
     */

    public StudySiteDao getCntForSiteInStudy(int studyId, int siteId) {
        StudySiteDao studySiteDao = new StudySiteDao();
        Rlog.debug("studySite",
                "inside method getCntForSiteInStudy in studySiteBean");

        try {
            StudySiteAgentRObj studySiteAgentRObj = EJBUtil
                    .getStudySiteAgentHome();

            studySiteDao = studySiteAgentRObj.getCntForSiteInStudy(studyId,
                    siteId);

        } catch (Exception e) {
            Rlog.fatal("studySite",
                    "Error in getCntForSiteTypeInStudy in StudySiteJB " + e);

        }
        return studySiteDao;
    }

    /**
     * Method to get the study site records for a specific study. Calls
     * getStudySiteTeamValues of StudySite Session Bean
     *
     * @param studyId
     *            PK of study
     * @param orgId
     *            PK of organization
     * @param userId
     * @param accId
     *            account id
     * @returns StudySiteDao
     */
    public StudySiteDao getStudySiteTeamValues(int studyId, int orgId,
            int userId, int accId) {
        StudySiteDao studySiteDao = new StudySiteDao();
        Rlog.debug("studySite",
                "inside method getStudySiteTeamValues in studySiteBean");

        try {
            StudySiteAgentRObj studySiteAgentRObj = EJBUtil
                    .getStudySiteAgentHome();

            studySiteDao = studySiteAgentRObj.getStudySiteTeamValues(studyId,
                    orgId, userId, accId);

        } catch (Exception e) {
            Rlog.fatal("studySite",
                    "Error in getStudySiteTeamValues in StudySiteJB " + e);

        }
        return studySiteDao;
    }

    /**
     * Method to get the All study team sites a specific study and account id
     * Calls getAllStudyTeamSites of StudySite Session Bean
     *
     * @param studyId
     *            PK of study
     * @param accountId
     * @returns StudySiteDao
     */
    public StudySiteDao getAllStudyTeamSites(int studyId, int accId) {
        StudySiteDao studySiteDao = new StudySiteDao();
        try {
            StudySiteAgentRObj studySiteAgentRObj = EJBUtil
                    .getStudySiteAgentHome();

            studySiteDao = studySiteAgentRObj.getAllStudyTeamSites(studyId,
                    accId);

        } catch (Exception e) {
            Rlog.fatal("studySite",
                    "Error in getAllStudyTeamSites in StudySiteJB " + e);

        }
        return studySiteDao;
    }
    public StudySiteDao getStudyTeamSites(int studyId, int accId) {
        StudySiteDao studySiteDao = new StudySiteDao();
        try {
            StudySiteAgentRObj studySiteAgentRObj = EJBUtil
                    .getStudySiteAgentHome();

            studySiteDao = studySiteAgentRObj.getAllStudyTeamSites(studyId,
                    accId);

        } catch (Exception e) {
            Rlog.fatal("studySite",
                    "Error in getAllStudyTeamSites in StudySiteJB " + e);

        }
        return studySiteDao;
    }

    /**
     *
     * Calls getCountSitesForUserStudy of StudySite Session Bean
     *
     * @param studyId
     *            PK of study
     * @param userId
     *            PK of user
     * @param accountId
     * @returns int
     */
    public int getCountSitesForUserStudy(int studyId, int userId, int accountId) {
        StudySiteDao studySiteDao = new StudySiteDao();
        int rows = 0;
        try {
            StudySiteAgentRObj studySiteAgentRObj = EJBUtil
                    .getStudySiteAgentHome();
            rows = studySiteAgentRObj.getCountSitesForUserStudy(studyId,
                    userId, accountId);

        } catch (Exception e) {
            Rlog.fatal("studySite",
                    "Error in getCountSitesForUserStudy in StudySiteJB " + e);

        }
        return rows;
    }

    /**
     *
     * Calls getSitesForUserStudy of StudySite Session Bean
     *
     * @param studyId
     *            PK of study
     * @param userId
     *            PK of user
     * @param accountId
     * @returns StudySiteDao
     */
    public StudySiteDao getSitesForUserStudy(int studyId, int userId,
            int accountId) {
        StudySiteDao studySiteDao = new StudySiteDao();
        try {
            StudySiteAgentRObj studySiteAgentRObj = EJBUtil
                    .getStudySiteAgentHome();

            studySiteDao = studySiteAgentRObj.getSitesForUserStudy(studyId,
                    userId, accountId);

        } catch (Exception e) {
            Rlog.fatal("studySite",
                    "Error in getSitesForUserStudy in StudySiteJB " + e);

        }
        return studySiteDao;
    }

    /**
     *
     * Calls getStdSitesLSampleSize of StudySite Session Bean
     *
     * @param studyId
     *            PK of study
     * @returns StudySiteDao
     */

    public StudySiteDao getStdSitesLSampleSize(int studyId) {
        StudySiteDao studySiteDao = new StudySiteDao();
        try {
            StudySiteAgentRObj studySiteAgentRObj = EJBUtil
                    .getStudySiteAgentHome();
            studySiteDao = studySiteAgentRObj.getStdSitesLSampleSize(studyId);

        } catch (Exception e) {
            Rlog.fatal("studySite",
                    "Error in getStdSitesLSampleSize in StudySiteJB " + e);

        }
        return studySiteDao;
    }

    public int updateLSampleSizes(String[] studySiteIds, String[] localSamples,
            int modifiedBy, String ipAdd) {
        int ret = 0;
        StudySiteDao studySiteDao = new StudySiteDao();
        try {
            StudySiteAgentRObj studySiteAgentRObj = EJBUtil
                    .getStudySiteAgentHome();
            ret = studySiteAgentRObj.updateLSampleSizes(studySiteIds,
                    localSamples, modifiedBy, ipAdd);

        } catch (Exception e) {
            Rlog.fatal("studySite",
                    "Error in updateLSampleSizes in StudySiteJB " + e);

        }
        return ret;
    }

    public StudySiteDao getAddedAccessSitesForStudy(int studyId, int userId,
            int accountId) {
        StudySiteDao studySiteDao = new StudySiteDao();
        try {
            StudySiteAgentRObj studySiteAgentRObj = EJBUtil
                    .getStudySiteAgentHome();
            studySiteDao = studySiteAgentRObj.getAddedAccessSitesForStudy(
                    studyId, userId, accountId);

        } catch (Exception e) {
            Rlog.fatal("studySite",
                    "Error in getAddedAccessSitesForStudy in StudySiteJB " + e);

        }
        return studySiteDao;
    }

    public int deleteOrgFromStudyTeam(int studyId, int accId, int siteId, int userId) {
        int ret = 0;
        StudySiteDao studySiteDao = new StudySiteDao();
        try {
            StudySiteAgentRObj studySiteAgentRObj = EJBUtil
                    .getStudySiteAgentHome();

            ret = studySiteAgentRObj.deleteOrgFromStudyTeam(studyId, accId, siteId, userId);

        } catch (Exception e) {
            Rlog.fatal("studySite",
                    "Error in getAddedAccessSitesForStudy in StudySiteJB " + e);

        }
        return ret;
    }
    
    //Overloaded for INF-18183 ::: Ankit
    public int deleteOrgFromStudyTeam(int studyId, int accId, int siteId, int userId, Hashtable<String, String> auditInfo) {
        int ret = 0;
        StudySiteDao studySiteDao = new StudySiteDao();
        try {
            StudySiteAgentRObj studySiteAgentRObj = EJBUtil
                    .getStudySiteAgentHome();

            ret = studySiteAgentRObj.deleteOrgFromStudyTeam(studyId, accId, siteId, userId, auditInfo);

        } catch (Exception e) {
            Rlog.fatal("studySite",
                    "Error in getAddedAccessSitesForStudy in StudySiteJB " + e);

        }
        return ret;
    }

    public int checkSuperUser(int studyId, int userId, int accountId) {
        StudySiteDao studySiteDao = new StudySiteDao();
        int rows = 0;
        try {
            StudySiteAgentRObj studySiteAgentRObj = EJBUtil
                    .getStudySiteAgentHome();

            rows = studySiteAgentRObj
                    .checkSuperUser(studyId, userId, accountId);

        } catch (Exception e) {
            Rlog.fatal("studySite", "Error in checkSuperUser in StudySiteJB "
                    + e);

        }
        return rows;
    }

    public StudySiteDao getSitesForStatAndEnrolledPat(int studyId, int userId,
            int accId) {
        StudySiteDao studySiteDao = new StudySiteDao();
        try {
            StudySiteAgentRObj studySiteAgentRObj = EJBUtil
                    .getStudySiteAgentHome();
            studySiteDao = studySiteAgentRObj.getSitesForStatAndEnrolledPat(
                    studyId, userId, accId);

        } catch (Exception e) {
            Rlog.fatal("studySite",
                    "Error in getSitesForStatAndEnrolledPat in StudySiteJB "
                            + e);

        }
        return studySiteDao;
    }

    /**
     * Calls findBeanByStudySite of Session Bean: StudySiteAgentBean finds a
     * studysite bean for a study and site
     *
     * @return StudySiteStatKeeper
     * @see StudySiteAgentBean
     */

    public StudySiteBean findBeanByStudySite() {
        StudySiteBean studySitesk = null;
        try {
            StudySiteAgentRObj studySiteAgentRObj = EJBUtil
                    .getStudySiteAgentHome();
            studySitesk = studySiteAgentRObj.findBeanByStudySite(EJBUtil
                    .stringToNum(this.studyId), EJBUtil
                    .stringToNum(this.siteId));
            Rlog.debug("studySite",
                    "StudySiteJB.findBeanByStudySite() StudySiteStateKeeper "
                            + studySitesk);
        } catch (Exception e) {
            Rlog.fatal("studySite", "Error in StudySite findBeanByStudySite"
                    + e);
        }

        if (studySitesk != null) {
            setStudySiteId(studySitesk.getStudySiteId());
            setStudyId(studySitesk.getStudyId());
            setSiteId(studySitesk.getSiteId());
            setStudySiteType(studySitesk.getStudySiteType());
            setSampleSize(studySitesk.getSampleSize());
            setStudySitePublic(studySitesk.getStudySitePublic());
            setRepInclude(studySitesk.getRepInclude());
            setCreator(studySitesk.getCreator());
            setModifiedBy(studySitesk.getModifiedBy());
            setIpAdd(studySitesk.getIpAdd());
            setCurrentSitePatientCount(studySitesk.getCurrentSitePatientCount());

            setIsReviewBasedEnrollment(studySitesk.getIsReviewBasedEnrollment());
        	setSendEnrNotificationTo(studySitesk.getSendEnrNotificationTo());
        	setSendEnrStatNotificationTo(studySitesk.getSendEnrStatNotificationTo());

        }

        return studySitesk;

    }

    // end of class
}
