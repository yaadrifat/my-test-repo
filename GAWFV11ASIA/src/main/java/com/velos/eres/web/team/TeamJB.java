/**
 * Classname : TeamJB.java
 * 
 * Version information
 *
 * Date 02/26/2001
 * 
 * Copyright notice: Aithent Technologies 
 */
package com.velos.eres.web.team;
import java.util.Hashtable;
import com.velos.eres.business.common.TeamDao;
import com.velos.eres.business.team.impl.TeamBean;
import com.velos.eres.service.teamAgent.TeamAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
/**
 * 
 * 
 * Client side bean for the Study Team
 * 
 * 
 * @author Deepali Sachdeva
 * 
 * 
 */
public class TeamJB {
    /**
     * 
     * 
     * the study team Id
     * 
     * 
     */
    private int id;
    /**
     * 
     * 
     * the Study ID
     * 
     * 
     */
    private String studyId;
    /**
     * 
     * 
     * the Study Team User
     * 
     * 
     */
    private String teamUser;
    /**
     * 
     * 
     * the Study Team User Role
     * 
     * 
     */
    private String teamUserRole;
    /**
     * creator
     */
    private String creator;
    /**
     * last modified by
     */
    private String modifiedBy;
    /**
     * IP Address
     */
    private String ipAdd;
    /**
     * teamUserType
     */
    private String teamUserType;
    /**
     * siteFlag
     */
    private String siteFlag;
    
    /**
     * teamStatus 
     */
    private String teamStatus;
    // END OF MEMBER DECLARATION
    // GETTER SETTER METHODS
    public int getId()
    {
        return this.id;
    }
    public void setId(int id)
    {
        this.id = id;
    }
    public String getTeamUser()
    {
        return this.teamUser;
    }
    public void setTeamUser(String teamUser)
    {
        this.teamUser = teamUser;
    }
    public String getStudyId()
    {
        return this.studyId;
    }
    public void setStudyId(String studyId)
    {
        this.studyId = studyId;
    }
    public String getTeamUserRole()
    {
        return this.teamUserRole;
    }
    public void setTeamUserRole(String teamRole)
    {
        this.teamUserRole = teamRole;
    }
   
    
    /**
     * @return Creator
     */
    public String getCreator() {
        return this.creator;
    }
    /**
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }
    /**
     * @return Last Modified By
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }
    /**
     * @param modifiedBy
     *            The value that is required to be registered as record last
     *            modified by
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
    /**
     * @return IP Address
     */
    public String getIpAdd() {
        return this.ipAdd;
    }
    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }
    /**
     * @return teamUserType
     */
    public String getTeamUserType() {
        return this.teamUserType;
    }
    /**
     * @param teamUserType
     *            Team user type
     */
    public void setTeamUserType(String teamUserType) {
        this.teamUserType = teamUserType;
    }
    /**
     * @return siteFlag
     */
    public String getSiteFlag() {
        return this.siteFlag;
    }
    /**
     * @param siteFlag
     */
    public void setSiteFlag(String siteFlag) {
        this.siteFlag = siteFlag;
    }
    
    
    // Added by Manimaran for the July-August Enhancement S4.
    /**
     * @return teamStatus
     */
    public String getTeamStatus() {
        return this.teamStatus;
    }
    /**
     * @param teamStatus
     */
    public void setTeamStatus(String teamStatus) {
        this.teamStatus = teamStatus;
    }
    // END OF GETTER SETTER METHODS
    public TeamJB(int teamId)
    {
        setId(teamId);
    }
    public TeamJB() {
    }
    /**
     * 
     * 
     * Full arguments constructor.
     * 
     * 
     * @param
     * 
     * 
     */
    public TeamJB(int id, String studyId, String teamUser, String teamUserRole,
            String creator, String modifiedBy, String ipAdd,
            String teamUserType, String siteFlag, String teamStatus)
    {
        setId(id);
        setStudyId(studyId);
        setTeamUser(teamUser);
        setTeamUserRole(teamUserRole);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setTeamUserType(teamUserType);
        setSiteFlag(siteFlag);
        setTeamStatus(teamStatus);
    }
    /**
     * 
     * calls getTeamDetails() on the TeamAgent facade and extracts this state
     * holder into the been attribute fields.
     * 
     */
    public TeamBean getTeamDetails() {
        TeamBean tsk = null;
        try
        {
            TeamAgentRObj teamAgentRObj = EJBUtil.getTeamAgentHome();
            tsk = teamAgentRObj.getTeamDetails(this.id);
        }
        catch (Exception e)
        {
            Rlog.fatal("team", "TeamJB.getTeamDetails exception - " + e);
        }
        if (tsk != null) {
            this.id = tsk.getId();
            this.studyId = tsk.getStudyId();
            this.teamUser = tsk.getTeamUser();
            this.teamUserRole = tsk.getTeamUserRole();
            this.creator = tsk.getCreator();
            this.modifiedBy = tsk.getModifiedBy();
            this.ipAdd = tsk.getIpAdd();
            this.siteFlag = tsk.getSiteFlag();
            this.teamStatus = tsk.getTeamStatus();
            setTeamUserType(tsk.getTeamUserType());
        }
        return tsk;
    }
    /**
     * 
     * 
     * calls setTeamDetails() on the TeamAgent facade.
     * 
     * 
     */
    public int updateTeam() {
        int ret = 0;
        try
        {
            TeamAgentRObj teamAgentRObj = EJBUtil.getTeamAgentHome();
            ret = teamAgentRObj.updateTeam(this.createTeamStateKeeper());
            return ret;
        }
        catch (Exception e)
        {
            Rlog.fatal("team", "TeamJB.updateTeam exception - " + e);
            return -2;
        }
    }
    /**
     * 
     * 
     * calls setCustomer() on the ReservationAgent facade.
     * 
     * 
     */
    public void setTeamDetails() {
        try
        {
            TeamAgentRObj teamAgentRObj = EJBUtil.getTeamAgentHome();
            this.setId(teamAgentRObj.setTeamDetails(this
                    .createTeamStateKeeper()));
        }
        catch (Exception e)
        {
            Rlog.fatal("team", "TeamJB.setTeamDetails exception - " + e);
        }
    }
    // ////////////////////////////////////////////////////
    public int deleteFromTeam() {
        int ret = 0;
        try
        {
            TeamAgentRObj teamAgentRObj = EJBUtil.getTeamAgentHome();
            ret = teamAgentRObj.deleteFromTeam(this.createTeamStateKeeper());
            return ret;
        }
        catch (Exception e)
        {
            Rlog.fatal("team", "TeamJB.deleteFromTeam exception - " + e);
            return -2;
        }
    }
 // Overloaded for INF-18183 ::: AGodara
    public int deleteFromTeam(Hashtable<String, String> auditInfo) {
        int ret = 0;
        try
        {
            TeamAgentRObj teamAgentRObj = EJBUtil.getTeamAgentHome();
            ret = teamAgentRObj.deleteFromTeam(this.createTeamStateKeeper(),auditInfo);
            return ret;
        }catch (Exception e){
            Rlog.fatal("team", "TeamJB.deleteFromTeam(Hashtable<String, String> auditInfo) exception - " + e);
            return -2;
        }
    }
    
    /*
     * 
     * 
     * places bean attributes into a StudyStateHolder.
     * 
     * 
     */
    public TeamBean createTeamStateKeeper()
    {
        return new TeamBean(new Integer(id), studyId, teamUser, teamUserRole,
                creator, modifiedBy, ipAdd, teamUserType, siteFlag, teamStatus);
    }
    /**
     * 
     * 
     * Calls getTeamValues(int, int) of Team Session Bean: TeamAgentBean
     * 
     * 
     * 
     * 
     * 
     * @param studyId
     *            The studyId for which the team details are to be retrieved
     * 
     * 
     * @param studyId
     *            The accId for which the team details are to be retrieved
     * 
     * 
     * @return returns TeamDao for list of all team members
     * 
     * 
     * @See TeamDao
     * 
     * 
     */
    public TeamDao getTeamValues(int studyId, int accId) {
        TeamDao teamDao = new TeamDao();
        try {
            TeamAgentRObj teamAgentRObj = EJBUtil.getTeamAgentHome();
            teamDao = teamAgentRObj.getTeamValues(studyId, accId);
            return teamDao;
        } catch (Exception e) {
            Rlog
                    .fatal("team",
                            "Error in getTeamValues(int studyId, accId) in TeamJB "
                                    + e);
        }
        return teamDao;
    }
    /**
     * 
     * 
     * Calls getTeamRights(int, int) of Team Session Bean: TeamAgentBean
     * 
     * 
     * 
     * 
     * 
     * @param studyId
     *            The studyId for which the team details are to be retrieved
     * 
     * 
     * @param userId
     *            The userId for which the team details are to be retrieved
     * 
     * 
     * @return returns TeamDao for list of all team members
     * 
     * 
     * @See TeamDao
     * 
     * 
     */
    public TeamDao getTeamRights(int studyId, int userId) {
        TeamDao teamDao = new TeamDao();
        try {
            TeamAgentRObj teamAgentRObj = EJBUtil.getTeamAgentHome();
            teamDao = teamAgentRObj.getTeamRights(studyId, userId);
            return teamDao;
        } catch (Exception e) {
            Rlog.fatal("team",
                    "Error in getTeamRights(int studyId, int userId) in TeamJB "
                            + e);
        }
        return teamDao;
    }
    // ////////////
    /**
     * 
     * 
     * Calls getSuperUserTeam(int) of Team Session Bean: TeamAgentBean
     * 
     * 
     * 
     * @param studyId
     *            The studyId for which the team details are to be retrieved
     * 
     * 
     * @See TeamDao
     * 
     * 
     */
    public TeamDao getSuperUserTeam(int studyId) {
        TeamDao teamDao = new TeamDao();
        try {
            TeamAgentRObj teamAgentRObj = EJBUtil.getTeamAgentHome();
            teamDao = teamAgentRObj.getSuperUserTeam(studyId);
            return teamDao;
        } catch (Exception e) {
            Rlog.fatal("team",
                    "Error in getSuperUserTeam(int studyId) in TeamJB " + e);
        }
        return teamDao;
    }
    /**
     * 
     * 
     * Calls getBgtSuperUserTeam(int) of Team Session Bean: TeamAgentBean
     * 
     * 
     * 
     * @param studyId
     *            The studyId for which the team details are to be retrieved
     * 
     * 
     * @See TeamDao
     * 
     * 
     */
    public TeamDao getBgtSuperUserTeam() {
        TeamDao teamDao = new TeamDao();
        try {
            TeamAgentRObj teamAgentRObj = EJBUtil.getTeamAgentHome();
            teamDao = teamAgentRObj.getBgtSuperUserTeam();
            return teamDao;
        } catch (Exception e) {
            Rlog.fatal("team",
                    "Error in getBgtSuperUserTeam(int studyId) in TeamJB " + e);
        }
        return teamDao;
    }
    // /
    /*
     * 
     * 
     * generates a String of XML for the customer details entry form.<br><br>
     * 
     * 
     * Note that it is planned to encapsulate this detail in another object.
     * 
     * 
     */
    public String toXML()
    {
        return new String(
                "<?xml version=\"1.0\"?>"
                        +
                        "<?cocoon-process type=\"xslt\"?>"
                        +
                        "<?xml-stylesheet href=\"/stylesheet/form-html.xsl\" type=\"text/xsl\"?>"
                        +
                        "<?xml-stylesheet href=\"/stylesheet/form-wml.xsl\" type=\"text/xsl\" media=\"wap\"?>"
                        +
                        "<form action=\"UpdateTeam.jsp\">" +
                        "<head>" +
                        "<title>Team Details </title>" +
                        "</head>" +
                        "<input type=\"hidden\" name=\"id\" value=\""
                        + this.getId() + "\" size=\"10\"/>" +
                        "<input type=\"hidden \" name=\"Study\" value=\""
                        + this.getStudyId() + "\" size=\"10\"/>" +
                        "<input type=\"text\" name=\" User\" value=\""
                        + this.getTeamUser() + "\" size=\"12\"/>" +
                        "<input type=\"text\" name=\" Role\" value=\""
                        + this.getTeamUserRole() + "\" size=\"30\"/>" +
                        "</form>");
    }// end of method
    /**
     * Calls getTeamValuesBySite(int, int, int) of Team Session Bean:
     * TeamAgentBean
     * 
     * @param studyId
     *            The studyId for which the team details are to be retrieved
     * @param accId
     *            The accId for which the team details are to be retrieved
     * @param orgId
     *            The orgId for which the team details are to be retrieved
     * @return returns TeamDao for list of all team members
     * @See TeamDao
     */
    public TeamDao getTeamValuesBySite(int studyId, int accId, int orgId) {
        TeamDao teamDao = new TeamDao();
        try {
            TeamAgentRObj teamAgentRObj = EJBUtil.getTeamAgentHome();
            teamDao = teamAgentRObj.getTeamValuesBySite(studyId, accId, orgId);
            return teamDao;
        } catch (Exception e) {
            Rlog.fatal("team",
                    "Error in getTeamValuesBySite(int studyId, accId, orgId) in TeamJB "
                            + e);
        }
        return teamDao;
    }
    /**
     * Find whether user is present in the study team for a particular study
     * 
     * @param int
     *            studyId - study Id of the study
     * @param int
     *            userId - user Id of the user
     * @param int
     *            orId - organization Id
     */
    public TeamDao findUserInTeam(int studyId, int userId) {
        TeamDao teamDao = new TeamDao();
        try {
            TeamAgentRObj teamAgentRObj = EJBUtil.getTeamAgentHome();
            teamDao = teamAgentRObj.findUserInTeam(studyId, userId);
            return teamDao;
        } catch (Exception e) {
            Rlog.fatal("team",
                    "Error in findUserInTeam(int studyId, accId, orgId) in TeamJB "
                            + e);
        }
        return teamDao;
    }
    
    /** Add users to multiple studies    
     * 
     * @param String[]
     * 					userIds - String array of user ids
     * @param String[]
     * 					studyIds - String array of study ids
     * @param String[]
     * 					roles - String array of roles 
     * @param int
     * 				user - logged in user id
     * @param ipAdd
     * 				ipAdd - ipAdd of the logged in user 
     * 
     * @return int
     */
    
    
    public int addUsersToMultipleStudies(String[] userIds, String[] studyIds, String[] roles, int user, String ipAdd){
    	
    	int ret = 0;
    	
        try
        {
            TeamAgentRObj teamAgentRObj = EJBUtil.getTeamAgentHome();
            ret = teamAgentRObj.addUsersToMultipleStudies(userIds, studyIds, roles, user, ipAdd);
            return ret;
        }
        catch (Exception e)
        {
            Rlog.fatal("team", "TeamJB.addUsersToMultipleStudies exception - " + e);
            return -2;
        }
    	
    	
    }
    
}// end of class
