/*
 * Classname : 				ReportsJB
 * 
 * Version information : 	1.0
 *
 * Date 					05/12/2001
 * 
 * Copyright notice: 		Velos Inc
 * 
 * Author 					Sajal
 */

package com.velos.eres.web.reports;

/* IMPORT STATEMENTS */

import com.velos.eres.service.reportsAgent.ReportsAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * Client side bean for Reports module. This class is used for fetching data for
 * any report
 * 
 * @author Sajal
 * @version 1.0, 05/12/2001
 */

public class ReportsJB {

    /**
     * Defines an ReportsJB object
     */
    public ReportsJB() {
        Rlog.debug("reports", "ReportsJB.ReportsJB() ");
    };

    public ReportsDao fetchReportsData(int reportNumber, String filterType,
            String year, String month, String dateFrom, String dateTo,
            String id, String accId) {
        ReportsDao reportsDao = new ReportsDao();
        try {
            Rlog.debug("reports", "ReportsJB.fetchReportsData starting");

            ReportsAgentRObj reportsAgentRObj = EJBUtil.getReportsAgentHome();
            Rlog.debug("reports", "ReportsJB.fetchReportsData after remote");
            reportsDao = reportsAgentRObj.fetchReportsData(reportNumber,
                    filterType, year, month, dateFrom, dateTo, id, accId);
            Rlog.debug("reports", "ReportsJB.fetchReportsData after Dao");
            return reportsDao;
        } catch (Exception e) {
            Rlog
                    .fatal("reports", "Error in fetchReportsData in ReportsJB "
                            + e);
        }
        return reportsDao;
    }

}// end of class

