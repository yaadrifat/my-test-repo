/*
 * Classname : SiteJB
 * 
 * Version information: 1.0
 *
 * Date: 02/25/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh Khurana
 */

package com.velos.eres.web.site;

/* Import Statements */
import com.velos.eres.business.common.SiteDao;
import com.velos.eres.business.site.impl.SiteBean;
import com.velos.eres.service.siteAgent.SiteAgentRObj;
import com.velos.eres.service.siteAgent.impl.SiteAgentBean;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* End of Import Statements */
/* Modified by Sonia Abrol, added an attribute siteIdentifier , 03/17/05 */

/**
 * Client side bean for Site
 * 
 * @author Dinesh Khurana
 * @version 1.0 02/25/2001
 */

/* Modified by Amarnadh ,Added an attribute siteNotes for June Enhancement '07  #U7 */
public class SiteJB {

    /**
     * the site Id
     */
    private int siteId;

    /**
     * the site Codelst
     */
    private String siteCodelstType;

    /**
     * the user accountId
     */

    private String siteAccountId;

    /**
     * the site address
     */

    private String sitePerAdd;

	/**
	 * the site notes
	 */

	 private String siteNotes;

    /**
     * the site name
     */
    private String siteName;
    private String ctepid;

    /**
     * the site Info
     */

    private String siteInfo;

    /**
     * the site status
     */

    private String siteStatus;

    /**
     * the site parent
     */

    private String siteParent;

    /**
     * creator
     */
    private String creator;

    /**
     * last modified by
     */
    private String modifiedBy;

    /**
     * IP Address
     */
    private String ipAdd;

    /**
     * the site AbstractIdentifier - Id to identify a site
     */

    private String siteIdentifier;

    /**
     * A number to identify the site's sequence within an account- for internal
     * use
     */
    private String siteSequence;
    
    private String siteHidden; //KM
    
    private String poIdentifier;

    // GETTER SETTER METHODS

    /**
     * Returns Site id
     * 
     * @return Site id
     */
    public int getSiteId() {
        return this.siteId;
    }

    /**
     * Sets Site id
     * 
     * @param siteId
     *            Site id
     */
    public void setSiteId(int siteId) {
        this.siteId = siteId;
    }

    /**
     * Returns Code List Id of Site Type
     * 
     * @return Code List Id of Site Type
     */
    public String getSiteCodelstType() {
        return this.siteCodelstType;
    }

    /**
     * Sets Code List Id of Site Type
     * 
     * @param siteCodelstType
     *            Code List Id of Site Type
     */
    public void setSiteCodelstType(String siteCodelstType) {
        this.siteCodelstType = siteCodelstType;
    }

    /**
     * Returns Account Id ofthe Site
     * 
     * @return Account Id ofthe Site
     */
    public String getSiteAccountId() {
        return this.siteAccountId;
    }

    /**
     * Sets Account Id ofthe Site
     * 
     * @param siteAccountId
     *            Account Id ofthe Site
     */
    public void setSiteAccountId(String siteAccountId) {
        this.siteAccountId = siteAccountId;
    }

    /**
     * Returns Address Id ofthe Site
     * 
     * @return Address Id ofthe Site
     */
    public String getSitePerAdd() {
        return this.sitePerAdd;
    }

    /**
     * Sets Address Id ofthe Site
     * 
     * @param sitePerAdd
     *            Address Id ofthe Site
     */
    public void setSitePerAdd(String sitePerAdd) {
        this.sitePerAdd = sitePerAdd;
    }

	/**
	 * Returns Site Notes
	 * @returns Site Notes
	 */

	 public String getSiteNotes() {
		 return this.siteNotes;
	 }

	 /**
	  * Sets the SiteNotes
	  * @param siteNotes 
	  *                  siteNotes
	  */

	  public void setSiteNotes(String siteNotes) {
		  this.siteNotes = siteNotes;
	  }


    /**
     * Returns Site Name
     * 
     * @return Site Name
     */
    public String getSiteName() {
        return this.siteName;
    }

    /**
     * Sets Site Name
     * 
     * @param siteName
     *            Site Name
     */
    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }


    public String getCtepId() {
    	return this.ctepid;
    }

    public void setCtepId(String ctepid){
    	this.ctepid = ctepid;
    }
    /**
     * Returns Site Information
     * 
     * @return Site Information
     */
    public String getSiteInfo() {
        return this.siteInfo;
    }

    /**
     * Sets Site Information
     * 
     * @param siteInfo
     *            Site Information
     */
    public void setSiteInfo(String siteInfo) {
        this.siteInfo = siteInfo;
    }

    /**
     * Returns Site Status
     * 
     * @return Site Status
     */
    public String getSiteStatus() {
        return this.siteStatus;
    }

    /**
     * Sets Site Status
     * 
     * @param siteStatus
     *            Site Status
     */
    public void setSiteStatus(String siteStatus) {
        this.siteStatus = siteStatus;
    }

    /**
     * Returns Id of the parent Site
     * 
     * @return Id of the parent Site
     */
    public String getSiteParent() {
        return this.siteParent;
    }

    /**
     * Sets Id of the parent Site
     * 
     * @param siteParent
     *            Id of the parent Site
     */
    public void setSiteParent(String siteParent) {
        this.siteParent = siteParent;
    }

    /**
     * @return Creator
     */
    public String getCreator() {
        return this.creator;
    }

    /**
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return Last Modified By
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param modifiedBy
     *            The value that is required to be registered as record last
     *            modified by
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return IP Address
     */
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
     * Returns the value of siteIdentifier.
     */
    public String getSiteIdentifier() {
        return siteIdentifier;
    }

    /**
     * Sets the value of siteIdentifier.
     * 
     * @param siteIdentifier
     *            The value to assign siteIdentifier.
     */
    public void setSiteIdentifier(String siteIdentifier) {
        this.siteIdentifier = siteIdentifier;
    }

    /**
     * Returns the value of siteSequence.
     */
    public String getSiteSequence() {
        return siteSequence;
    }

    /**
     * Sets the value of siteSequence.
     * 
     * @param siteSequence
     *            The value to assign siteSequence.
     */
    public void setSiteSequence(String siteSequence) {
        this.siteSequence = siteSequence;
    }
    
    //KM-#U11
    /**
     * Returns Site Hidden
     * 
     * @return Site Hidden
     */
    public String getSiteHidden() {
        return this.siteHidden;
    }

    /**
     * Sets Site Hidden
     * 
     * @param siteHidden
     *            Site Hidden
     */
    public void setSiteHidden(String siteHidden) {
    	
    	String siteH = "";
    	
    	if (StringUtil.isEmpty(siteHidden))
    	{
    		siteH = "0";
    	}
    	else
    	{
    		siteH = siteHidden;
    	}
        this.siteHidden = siteH;
    }
    
    public String getPoIdentifier() {
		return poIdentifier;
	}

	public void setPoIdentifier(String poIdentifier) {
		this.poIdentifier = poIdentifier;
	}
    

    // end of getter and setter methods

    /**
     * Full arguments constructor.
     */

    // COMMENTED FOR TIME BEING
    /**
     * Constructor
     * 
     * @param siteId
     */
    public SiteJB(int siteId) {
        setSiteId(siteId);
    }

    /**
     * Default Constructor
     * 
     */
    public SiteJB() {
        Rlog.debug("site", "SiteJB.SiteJB() ");
    }

    /**
     * Full Argument Constructor
     * 
     * @param siteId
     * @param siteCodelstType
     * @param siteAccountId
     * @param sitePerAdd
     * @param siteName
     * @param siteNotes
     * @param siteDept
     * @param siteInfo
     * @param siteStatus
     * @param siteParent
     * @param siteIdentifier
     * @param siteSequence
     * @param siteHidden
     */
    public SiteJB(Integer siteId, String siteCodelstType, String siteAccountId,
            String sitePerAdd, String siteName, String ctepid, String siteNotes, String siteDept,
            String siteInfo, String siteStatus, String siteParent,
            String creator, String modifiedBy, String ipAdd,
            String siteIdentifier, String siteSequence, String siteHidden, String poIdentifier) {
        setSiteId(siteId);
        setSiteCodelstType(siteCodelstType);
        setSiteAccountId(siteAccountId);
        setSitePerAdd(sitePerAdd);
		setSiteNotes(siteNotes);
        setSiteName(siteName);
        setCtepId(ctepid);
        setSiteInfo(siteInfo);
        setSiteStatus(siteStatus);
        setSiteParent(siteParent);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setSiteIdentifier(siteIdentifier);
        setSiteSequence(siteSequence);
        setSiteHidden(siteHidden);//KM
        setPoIdentifier(poIdentifier);
        Rlog.debug("site", "SiteJB.SiteJB(all parameters)");
    }

    /**
     * Calls getSiteDetails of Site Session Bean: SiteAgentBean
     * 
     * @returns SiteStateKeeper object
     * @see SiteAgentBean
     */
    public SiteBean getSiteDetails() {
        SiteBean ssk = null;

        try {

            // SiteAgentHome siteAgentHome = EJBUtil.getSiteAgentHome();
            SiteAgentRObj siteAgent = EJBUtil.getSiteAgentHome();
            Rlog.debug("site", "Error in getSiteDetails() in siteJB "
                    + siteAgent.getSiteDetails(this.siteId));
            ssk = siteAgent.getSiteDetails(this.siteId);

        } catch (Exception e) {
            Rlog.debug("site", "Error in getSiteDetails() in UserJB " + e);
            e.printStackTrace();
        }
        if (ssk != null) {

            this.siteId = ssk.getSiteId();
            this.siteCodelstType = ssk.getSiteCodelstType();
            this.siteAccountId = ssk.getSiteAccountId();
            this.sitePerAdd = ssk.getSitePerAdd();
			this.siteNotes = ssk.getSiteNotes();
            this.siteName = ssk.getSiteName();
            this.ctepid = ssk.getCtepId();
            this.siteInfo = ssk.getSiteInfo();
            this.siteStatus = ssk.getSiteStatus();
            this.siteParent = ssk.getSiteParent();
            this.creator = ssk.getCreator();
            this.modifiedBy = ssk.getModifiedBy();
            this.ipAdd = ssk.getIpAdd();
            setSiteIdentifier(ssk.getSiteIdentifier());
            setSiteSequence(ssk.getSiteSequence());
            this.siteHidden = ssk.getSiteHidden();//KM
            this.poIdentifier = ssk.getPoIdentifier();

        }
        return ssk;
    }

    /**
     * Calls setSiteDetails of Site Session Bean: SiteAgentBean
     * 
     * @see SiteAgentBean
     */
    public int setSiteDetails() {
        int output = 0;
        try {
            SiteAgentRObj siteAgent = EJBUtil.getSiteAgentHome();
           
            SiteBean stkeep = this.createSiteStateKeeper();
            
            // Object[] methods=SiteAgentRObj.class.getMethods();
            // String tempStr="";
            /*
             * int len=SiteAgentRObj.class.getMethods().length; for (int i=0;i<len;i++) {
             * tempStr=(methods[i]).toString(); System.out.println(tempStr); }
             */
            siteAgent.setSiteDetails(stkeep);

            // output = siteAgent.setSiteDetails(stkeep);
            
            this.setSiteId(output);
            Rlog.debug("site", "SiteJB.setSiteDetails()");
            return output;
        } catch (Exception e) {
            Rlog.fatal("site", "Error in setSiteDetails() in SiteJB " + e);
            e.printStackTrace();
            return -2;
        }
    }

    /**
     * Calls updateDetails of Site Session Bean: SiteAgentBean
     * 
     * @return int 0 if successful, -2 for exception
     * @see SiteAgentBean
     */
    public int updateSite() {
        int output;
        try {
            // SiteAgentHome siteAgentHome = EJBUtil.getSiteAgentHome();
            SiteAgentRObj siteAgentRObj = EJBUtil.getSiteAgentHome();
            output = siteAgentRObj.updateSite(this.createSiteStateKeeper());
        } catch (Exception e) {
            Rlog.fatal("site",
                    "EXCEPTION IN SETTING ACCOUNT DETAILS TO  SESSION BEAN "
                            + e);

            return -2;
        }
        return output;
    }

    /**
     * Creates a SiteStateKeeper onject with the attribute vales of the bean
     * 
     * @return SiteStateKeeper
     */

    public SiteBean createSiteStateKeeper() {
        Rlog.debug("site", "SiteJB.createSiteStateKeeper ");
        try {
        	
        	
            return new SiteBean(new Integer(siteId), siteCodelstType,
                    siteAccountId, sitePerAdd, siteName,ctepid, siteInfo,siteNotes,siteStatus,
                    siteParent, creator, modifiedBy, ipAdd, siteIdentifier,
                    siteSequence, siteHidden, poIdentifier);
        } catch (RuntimeException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Calls getByAccountId of Site Session Bean: SiteAgentBean
     * 
     * @param accId
     * @return SiteDao
     * @see SiteDao
     * @see SiteDao
     */

    public SiteDao getByAccountId(int accId) {
        SiteDao siteDao = new SiteDao();
        try {
            Rlog.debug("site", "SiteJB.getByAccountId starting");
            Rlog.debug("site", "SiteJB.getByAccountId after home");
            SiteAgentRObj siteAgentRObj = EJBUtil.getSiteAgentHome();
            Rlog.debug("site", "SiteJB.getByAccountId after remote");
            siteDao = siteAgentRObj.getByAccountId(accId);
            Rlog.debug("site", "SiteJB.getByAccountId after Dao");
            return siteDao;
        } catch (Exception e) {
            Rlog.fatal("site", "Error in getByAccountId(int accId) in SiteJB "
                    + e);
        }
        return siteDao;
    }

    /**
     * Calls getSiteValuesForStudy of Site Session Bean: SiteAgentBean
     * 
     * @param studyId
     * @return SiteDao
     * @see SiteDao
     * @see SiteDao
     */
    public SiteDao getSiteValuesForStudy(int studyId) {
        SiteDao siteDao = new SiteDao();
        try {
            Rlog.debug("site", "SiteJB.getSiteValuesForStudy starting");
            // SiteAgentHome siteAgentHome = EJBUtil.getSiteAgentHome();
            Rlog.debug("site", "SiteJB.getSiteValuesForStudy after home");
            SiteAgentRObj siteAgentRObj = EJBUtil.getSiteAgentHome();
            Rlog.debug("site", "SiteJB.getSiteValuesForStudy after remote");
            siteDao = siteAgentRObj.getSiteValuesForStudy(studyId);
            Rlog.debug("site", "SiteJB.getSiteValuesForStudy after Dao");
            return siteDao;
        } catch (Exception e) {
            Rlog.fatal("site", "Error in getSiteValuesForStudy in SiteJB " + e);
        }
        return siteDao;
    }

    /**
     * Calls getByAccountId of Site Session Bean: SiteAgentBean
     * 
     * @param accId
     * @param siteType
     * @return SiteDao
     * @see SiteDao
     * 
     */
    public SiteDao getByAccountId(int accId, String siteType) {
        SiteDao siteDao = new SiteDao();
        try {

            // SiteAgentHome siteAgentHome = EJBUtil.getSiteAgentHome();
            SiteAgentRObj siteAgentRObj = EJBUtil.getSiteAgentHome();

            siteDao = siteAgentRObj.getByAccountId(accId, siteType);

            return siteDao;
        } catch (Exception e) {
            Rlog.fatal("site", "Error in getByAccountId(int accId) in SiteJB "
                    + e);
        }
        return siteDao;
    }

    // modified on 29th march for organization sorting, bug 2072
    public SiteDao getByAccountId(int accId, String orderBy, String orderType) {
        SiteDao siteDao = new SiteDao();
        try {
            Rlog.debug("site", "SiteJB.getByAccountId starting");
            // SiteAgentHome siteAgentHome = EJBUtil.getSiteAgentHome();
            Rlog.debug("site", "SiteJB.getByAccountId after home");
            SiteAgentRObj siteAgentRObj = EJBUtil.getSiteAgentHome();
            Rlog.debug("site", "SiteJB.getByAccountId after remote");
            siteDao = siteAgentRObj.getByAccountId(accId, orderBy, orderType);
            Rlog.debug("site", "SiteJB.getByAccountId after Dao");
            return siteDao;
        } catch (Exception e) {
            Rlog.fatal("site", "Error in getByAccountId(int accId) in SiteJB "
                    + e);
        }
        return siteDao;
    }
    public SiteDao getByAccountId(int accId, String orderBy, String orderType,String flag) {
        SiteDao siteDao = new SiteDao();
        try {
            Rlog.debug("site", "SiteJB.getByAccountId starting");
            // SiteAgentHome siteAgentHome = EJBUtil.getSiteAgentHome();
            Rlog.debug("site", "SiteJB.getByAccountId after home");
            SiteAgentRObj siteAgentRObj = EJBUtil.getSiteAgentHome();
            Rlog.debug("site", "SiteJB.getByAccountId after remote");
            siteDao = siteAgentRObj.getByAccountId(accId, orderBy, orderType,"");
            Rlog.debug("site", "SiteJB.getByAccountId after Dao");
            return siteDao;
        } catch (Exception e) {
            Rlog.fatal("site", "Error in getByAccountId(int accId) in SiteJB "
                    + e);
        }
        return siteDao;
    }
    /**
     * Calls getBySiteTypeCodeId of Site Session Bean: SiteAgentBean
     * 
     * @param accId
     * @param siteType (comma delimited string of code Ids)
     * @return SiteDao
     * @see SiteDao
     * 
     */
    public SiteDao getBySiteTypeCodeId(int accId, String siteType) {
        SiteDao siteDao = new SiteDao();
        try {

            // SiteAgentHome siteAgentHome = EJBUtil.getSiteAgentHome();
            SiteAgentRObj siteAgentRObj = EJBUtil.getSiteAgentHome();

            siteDao = siteAgentRObj.getBySiteTypeCodeId(accId, siteType);

            return siteDao;
        } catch (Exception e) {
            Rlog.fatal("site", "Error in getByAccountId(int accId) in SiteJB "
                    + e);
        }
        return siteDao;
    }
    
    public SiteDao getSitesByUser(int userId) {
    	SiteDao siteDao = null;
        try {
        	SiteAgentRObj siteAgentRObj = EJBUtil.getSiteAgentHome();
        	siteDao = siteAgentRObj.getSitesByUser(userId);
        } catch (Exception e) {
            Rlog.fatal("site", "Error in getSitesByUser() in SiteJB: "+ e);
        }
        if (siteDao == null) { siteDao = new SiteDao(); }
        return siteDao;
    }
    
    /*
     * generates a String of XML for the user details entry form.<br><br> Not
     * in use
     */

    /**
     * Not in Use
     * 
     * @return
     */
    public String toXML() {
        Rlog.debug("site", "UserJB.toXML()");
        return new String(
                "<?xml version=\"1.0\"?>"
                        + "<?cocoon-process type=\"xslt\"?>"
                        + "<?xml-stylesheet href=\"/stylesheet/form-html.xsl\" type=\"text/xsl\"?>"
                        + "<?xml-stylesheet href=\"/stylesheet/form-wml.xsl\" type=\"text/xsl\" media=\"wap\"?>"
                        +
                        // to be done "<form action=\"usersummary.jsp\">" +
                        "<head>"
                        +
                        // to be done "<title>User Summary </title>" +
                        "</head>"
                        + "<input type=\"text\" name=\"siteId\" value=\""
                        + this.getSiteId()
                        + "\" size=\"10\"/>"
                        + "<input type=\"text\" name=\"siteCodelstType\" value=\""
                        + this.getSiteCodelstType()
                        + "\" size=\"10\"/>"
                        + "<input type=\"text\" name=\"siteAccountId\" value=\""
                        + this.getSiteAccountId()
                        + "\" size=\"10\"/>"
                        + "<input type=\"text\" name=\"sitePerAdd\" value=\""
                        + this.getSitePerAdd()
                        + "\" size=\"38\"/>"
						+ "<input type=\"text\" notes=\"siteNotes\" value=\""
						+ this.getSiteNotes() 
						+ "\" size=\"400\"/>"
                        + "<input type=\"text\" name=\"this.siteName\" value=\""
                        + this.getSiteName() + "\" size=\"50\"/>"
                        +"<input type=\"text\" name=\"this.ctepid\" value=\""
                        + this.getCtepId() + "\" size=\"50\"/>"
                        + "<input type=\"text\" name=\"siteInfo\" value=\""
                        + this.getSiteInfo() + "\" size=\"2000\"/>"
                        + "<input type=\"text\" name=\"siteStatus\" value=\""
                        + this.getSiteStatus() + "\" size=\"10\"/>"
                        + "<input type=\"text\" name=\"siteParent\" value=\""
                        + this.getSiteParent() + "\" size=\"1\"/>" + "</form>");

    }// end of method

}