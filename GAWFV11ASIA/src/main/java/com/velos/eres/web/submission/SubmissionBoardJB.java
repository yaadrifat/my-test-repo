package com.velos.eres.web.submission;

import java.util.Date;
import java.util.Hashtable;

import com.velos.eres.business.common.EIRBDao;
import com.velos.eres.business.common.UserDao;
import com.velos.eres.business.submission.impl.SubmissionBean;
import com.velos.eres.business.submission.impl.SubmissionBoardBean;
import com.velos.eres.service.submissionAgent.SubmissionBoardAgent;
import com.velos.eres.service.submissionAgent.SubmissionBoardAgentBean;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

public class SubmissionBoardJB {
    private int id;
    private Integer fkSubmission;
    private Integer fkReviewBoard;
    private String submissionReviewer;
    private Integer submissionReviewType;
    private Integer creator;
    private Integer lastModifiedBy;
    private String ipAdd;
    private Integer fkReviewMeeting;
    private String reviewerFirstNameList;
    private String reviewerLastNameList;
    private String reviewerFullNameList;

    public Hashtable createSubmissionBoard() {
        Hashtable output = new Hashtable();
        try {
            SubmissionBoardAgent submissionBoardAgent = EJBUtil.getSubmissionBoardAgentHome();
            SubmissionBoardBean newBean = createEntityBean();
            Integer id = submissionBoardAgent.getExistingSubmissionBoard(newBean);
            if (id > 0) {
                output.put("previouslySubmitted", "Y");
                output.put("id", id);
                return output;
            }
            id = submissionBoardAgent.createSubmissionBoard(newBean);
            output.put("id", id);
        } catch(Exception e) {
            Rlog.fatal("submission", "Error in SubmissionBoardJB.createSubmissionBoard "+e);
            output.put("id", -1);
        }
        return output;
    }
    
    public int findByFkSubmissionAndBoard(int fkSubmission, int fkReviewBoard) {
        try {
            SubmissionBoardAgent submissionBoardAgent = EJBUtil.getSubmissionBoardAgentHome();
            SubmissionBoardBean submissionBoardBean = 
                submissionBoardAgent.findByFkSubmissionAndBoard(fkSubmission, fkReviewBoard);
            if (submissionBoardBean == null) {
                return 0;
            }
            this.fkReviewBoard = submissionBoardBean.getFkReviewBoard();
            this.fkSubmission = submissionBoardBean.getFkSubmission();
            this.id = submissionBoardBean.getId();
            this.ipAdd = submissionBoardBean.getIpAdd();
            this.lastModifiedBy = submissionBoardBean.getLastModifiedBy();
            this.submissionReviewer = submissionBoardBean.getSubmissionReviewer();
            this.submissionReviewType = submissionBoardBean.getSubmissionReviewType();
            this.fkReviewMeeting = submissionBoardBean.getFkReviewMeeting();
            this.reviewerFirstNameList = null;
            this.reviewerLastNameList = null;
            this.reviewerFullNameList = null;
            if (submissionReviewer != null) {
                StringBuffer sbFN = new StringBuffer();
                StringBuffer sbLN = new StringBuffer();
                StringBuffer sbFull = new StringBuffer();
                String reviewers[] =  submissionReviewer.split(",");
                for (int iX=0; iX<reviewers.length; iX++) {
                    UserDao userDao = new UserDao();
                    String fullname = userDao.getUserFullName(EJBUtil.stringToNum(reviewers[iX]));
                    String subname[] = fullname.split(" ");
                    if (subname != null && subname.length > 0) {
                        sbFN.append(subname[0]).append(",");
                    }
                    if (subname != null && subname.length > 1) {
                        sbLN.append(subname[1]).append(",");
                        sbFull.append(subname[0]).append(" ").append(subname[1]).append(",");
                    }
                }
                sbFN.setLength(sbFN.length()-1);
                reviewerFirstNameList = sbFN.toString();
                sbLN.setLength(sbLN.length()-1);
                reviewerLastNameList = sbLN.toString();
                sbFull.setLength(sbFull.length()-1);
                reviewerFullNameList = sbFull.toString();
            }
        } catch(Exception e) {
            Rlog.fatal("submission", "Error in SubmissionBoardJB.findByPkSubmissionBoard "+e);
        }
        return this.id;
    }
    
    public void findByPkSubmissionBoard(int pkSubmissionBoard) {
        try {
            SubmissionBoardAgent submissionBoardAgent = EJBUtil.getSubmissionBoardAgentHome();
            SubmissionBoardBean submissionBoardBean = 
                submissionBoardAgent.findByPkSubmissionBoard(new Integer(pkSubmissionBoard));
            this.fkReviewBoard = submissionBoardBean.getFkReviewBoard();
            this.fkSubmission = submissionBoardBean.getFkSubmission();
            this.id = submissionBoardBean.getId();
            this.ipAdd = submissionBoardBean.getIpAdd();
            this.lastModifiedBy = submissionBoardBean.getLastModifiedBy();
            this.submissionReviewer = submissionBoardBean.getSubmissionReviewer();
            this.submissionReviewType = submissionBoardBean.getSubmissionReviewType();
            this.fkReviewMeeting = submissionBoardBean.getFkReviewMeeting();
        } catch(Exception e) {
            Rlog.fatal("submission", "Error in SubmissionBoardJB.findByPkSubmissionBoard "+e);
        }
    }
    
    public int updateSubmissionBoard() {
        int output = 0;
        try {
            SubmissionBoardAgent submissionBoardAgent = EJBUtil.getSubmissionBoardAgentHome();
            output = submissionBoardAgent.updateSubmissionBoard(createEntityBean(), true); // applyNull = true
        } catch(Exception e) {
            Rlog.fatal("submission", "Error in SubmissionJB.updateSubmissionBoard "+e);
            output = -1;
        }
        return output;
    }
    
    private SubmissionBoardBean createEntityBean() {
        return new SubmissionBoardBean(this.id, this.fkSubmission, this.fkReviewBoard, 
                this.submissionReviewer, this.submissionReviewType,
                this.creator, this.lastModifiedBy, this.ipAdd,
                this.fkReviewMeeting );
    }
    
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getFkSubmission() {
        return String.valueOf(fkSubmission);
    }
    public void setFkSubmission(String fkSubmission) {
        this.fkSubmission = EJBUtil.stringToNum(fkSubmission);
    }
    public String getFkReviewBoard() {
        return String.valueOf(fkReviewBoard);
    }
    public void setFkReviewBoard(String fkReviewBoard) {
        this.fkReviewBoard = EJBUtil.stringToNum(fkReviewBoard);
    }
    public String getSubmissionReviewer() {
        return submissionReviewer;
    }
    public void setSubmissionReviewer(String submissionReviewer) {
        this.submissionReviewer = submissionReviewer;
    }
    public String getSubmissionReviewType() {
        return String.valueOf(submissionReviewType);
    }
    public void setSubmissionReviewType(String submissionReviewType) {
        this.submissionReviewType = submissionReviewType == null ?
                null : EJBUtil.stringToInteger(submissionReviewType);
    }
    public String getCreator() {
        return String.valueOf(creator);
    }
    public void setCreator(String createdBy) {
        this.creator = EJBUtil.stringToInteger(createdBy);
    }
    public String getLastModifiedBy() {
        return String.valueOf(lastModifiedBy);
    }
    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = EJBUtil.stringToInteger(lastModifiedBy);
    }
    public String getIpAdd() {
        return ipAdd;
    }
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }
    public String getFkReviewMeeting () {
        return String.valueOf(fkReviewMeeting);
    }
    public void setFkReviewMeeting(String fkReviewMeeting) {
        if (fkReviewMeeting == null || fkReviewMeeting.length() == 0) {
            this.fkReviewMeeting = null;
        } else {
            this.fkReviewMeeting = EJBUtil.stringToInteger(fkReviewMeeting);
        }
    }
    public String getReviewerFirstNameList() {
        return this.reviewerFirstNameList;
    }
    public String getReviewerLastNameList() {
        return this.reviewerLastNameList;
    }
    public String getReviewerFullNameList() {
        return this.reviewerFullNameList;
    }


    /** Returns Minutes of Meeting for a review board and meeting date*/
    public  String getreviewMeetingMOM(int fkBoard, int fkMeetingDatePK)
    {
    	String mom = "";
    	
    	try{
            SubmissionBoardAgent submissionBoardAgent = EJBUtil.getSubmissionBoardAgentHome();
            mom = submissionBoardAgent.getreviewMeetingMOM(fkBoard, fkMeetingDatePK);
 
     	}
    	 catch (Exception e) {
             Rlog.fatal("submission", "Exception in SubmissionBoardJB.getreviewMeetingMOM "+e);
              
         }
    	return mom;
    }


    /** Updates Minutes of Meeting for a review board and meeting date*/
    public int updateReviewMeetingMOM( int fkMeetingDatePK,String mom) 
    {
    	int ret= 0;
    	
    	try{
            SubmissionBoardAgent submissionBoardAgent = EJBUtil.getSubmissionBoardAgentHome();
            ret = submissionBoardAgent.updateReviewMeetingMOM(fkMeetingDatePK,mom);
 
     	}
    	 catch (Exception e) {
             Rlog.fatal("submission", "Exception in SubmissionBoardJB.updateReviewMeetingMOM "+e);
              ret = -1;
         }
    	return ret;
 
    }


    /** returns the PK of the default review board for an account ONLY if the logged in uder's default group (passed as a parameter)
     * has access to the review board
     * 
     * returns 0 if no default group is configured or the group does not have access to it
     * 
     * */
    
    public static int getDefaultReviewBoardWithCheckGroupAccess(int accountId,int grpId) {
 
    	int boardPK= 0;
    	
    	try{
            SubmissionBoardAgent submissionBoardAgent = EJBUtil.getSubmissionBoardAgentHome();
            boardPK = submissionBoardAgent.getDefaultReviewBoardWithCheckGroupAccess( accountId, grpId) ;
 
     	}
    	 catch (Exception e) {
             Rlog.fatal("submission", "Exception in getDefaultReviewBoardWithCheckGroupAccess(int accountId,int grpId) "+e);
             boardPK = -1;
         }
    	return boardPK;
 
    }
    public static int getDefaultReviewBoardWithCheckGroupAccess(int accountId,int grpId,int submissionBoardPK) {
   	 
    	int boardPK= 0;
    	
    	try{
            SubmissionBoardAgent submissionBoardAgent = EJBUtil.getSubmissionBoardAgentHome();
            boardPK = submissionBoardAgent.getDefaultReviewBoardWithCheckGroupAccess( accountId, grpId,submissionBoardPK) ;
 
     	}
    	 catch (Exception e) {
             Rlog.fatal("submission", "Exception in getDefaultReviewBoardWithCheckGroupAccess(int accountId,int grpId,int submissionBoardPK) "+e);
             boardPK = -1;
         }
    	return boardPK;
 
    }
    
    
}