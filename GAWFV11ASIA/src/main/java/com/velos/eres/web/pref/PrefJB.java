/*

 * Classname : PrefJB

 * 

 * Version information: 1.0

 *

 * Date: 06/02/2001

 * 

 * Copyright notice: Velos, Inc

 *

 * Author: Sonia Sahni

 */

package com.velos.eres.web.pref;

/* IMPORT STATEMENTS */

import com.velos.eres.business.pref.impl.PrefBean;
import com.velos.eres.service.prefAgent.PrefAgentRObj;
import com.velos.eres.service.prefAgent.impl.PrefAgentBean;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * 
 * Client side bean for Pref (Pat Ref)
 * 
 * @author Sonia Sahni
 * 
 * @version 1.0 06/02/2001
 * 
 */

public class PrefJB {

    /**
     * 
     * 
     * 
     * 
     * 
     * the Pref PK Id
     * 
     * 
     * 
     * 
     * 
     */

    private int prefPKId;

    /**
     * 
     * 
     * 
     * 
     * 
     * the Pref ID
     * 
     * 
     * 
     * 
     * 
     */

    private String prefPId;

    /**
     * 
     * 
     * 
     * 
     * 
     * the Pref Account
     * 
     * 
     * 
     * 
     * 
     */

    private String prefAccount;

    /**
     * 
     * 
     * 
     * 
     * 
     * the Pref Account
     * 
     * 
     * 
     * 
     * 
     */

    private String prefLocation;

    private String timeZoneId;

    /*
     * 
     * creator
     * 
     */

    private String creator;

    /*
     * 
     * last modified by
     * 
     */

    private String modifiedBy;

    /*
     * 
     * IP Address
     * 
     */

    private String ipAdd;
    
    /**
     * the Patient Facility Id
     */
    private String patientFacilityId;


    // GETTER SETTER METHODS

    public String getPrefAccount()

    {

        return this.prefAccount;

    }

    public void setPrefAccount(String prefAccount)

    {

        this.prefAccount = prefAccount;

    }

    public String getPrefLocation()

    {

        return this.prefLocation;

    }

    public void setPrefLocation(String prefLocation)

    {

        this.prefLocation = prefLocation;

    }

    public String getPrefPId()

    {

        return this.prefPId;

    }

    public void setPrefPId(String prefPId)

    {

        this.prefPId = prefPId;

    }

    public int getPrefPKId()

    {

        return this.prefPKId;

    }

    public void setPrefPKId(int prefPKId)

    {

        this.prefPKId = prefPKId;

    }

    public String getTimeZoneId()

    {

        return this.timeZoneId;

    }

    public void setTimeZoneId(String timeZoneId)

    {

        this.timeZoneId = timeZoneId;

    }

    /**
     * 
     * @return Creator
     * 
     */

    public String getCreator() {

        return this.creator;

    }

    /**
     * 
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     * 
     */

    public void setCreator(String creator) {

        this.creator = creator;

    }

    /**
     * 
     * @return Last Modified By
     * 
     */

    public String getModifiedBy() {

        return this.modifiedBy;

    }

    /**
     * 
     * @param modifiedBy
     *            The value that is required to be registered as record last
     *            modified by
     * 
     */

    public void setModifiedBy(String modifiedBy) {

        this.modifiedBy = modifiedBy;

    }

    /**
     * 
     * @return IP Address
     * 
     */

    public String getIpAdd() {

        return this.ipAdd;

    }

    /**
     * 
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     * 
     */

    public void setIpAdd(String ipAdd) {

        this.ipAdd = ipAdd;

    }

    /**
	 * Returns the value of patientFacilityId.
	 */
	public String getPatientFacilityId()
	{
		return patientFacilityId;
	}

	/**
	 * Sets the value of patientFacilityId.
	 * @param patientFacilityId The value to assign patientFacilityId.
	 */
	public void setPatientFacilityId(String patientFacilityId)
	{
		this.patientFacilityId = patientFacilityId;
	}

	
    // end of getter setter methods here

    /**
     * 
     * Constructor
     * 
     * 
     * 
     * @param prefId
     *            PK
     * 
     */

    public PrefJB(int prefPKId)

    {

        setPrefPKId(prefPKId);

    }

    /**
     * 
     * Default Constructor
     * 
     * 
     * 
     */

    public PrefJB()

    {

        Rlog.debug("pref", "PrefJB.PrefJB() ");

    };

    /**
     * 
     * Full arguments constructor.
     * 
     * 
     * 
     * @param prefPkId
     * 
     * @param prefPId
     * 
     * @param prefAccount
     * 
     * @param prefLocation
     * 
     */

    public PrefJB(int prefPkId, String prefPId, String prefAccount,
            String prefLocation, String timeZoneId,

            String creator, String modifiedBy, String ipAdd ,String patientFacilityId) {

        setPrefPKId(prefPKId);

        setPrefPId(prefPId);

        setPrefAccount(prefAccount);

        setPrefLocation(prefLocation);

        setTimeZoneId(timeZoneId);

        setCreator(creator);

        setModifiedBy(modifiedBy);

        setIpAdd(ipAdd);
	setPatientFacilityId(patientFacilityId); 
    }

    /**
     * 
     * Calls getPrefDetails on Pref Session Bean: PrefAgentBean
     * 
     * 
     * 
     * @return PrefStateKeeper
     * 
     * @see PrefAgentBean
     * 
     */

    public PrefBean getPrefDetails()

    {

        PrefBean psk = null;

        try {

            PrefAgentRObj prefAgentRObj = EJBUtil.getPrefAgentHome();

            psk = prefAgentRObj.getPrefDetails(this.prefPKId);

            Rlog
                    .debug("pref", "PrefJB.getPrefDetails() PrefStateKeeper "
                            + psk);

        } catch (Exception e) {

            Rlog.fatal("pref", "Error in getPrefDetails() in PrefJB " + e);

        }

        if (psk != null) {

            prefPKId = psk.getPrefPKId();

            prefPId = psk.getPrefPId();

            prefAccount = psk.getPrefAccount();

            prefLocation = psk.getPrefLocation();

            timeZoneId = psk.getTimeZoneId();

            creator = psk.getCreator();

            modifiedBy = psk.getModifiedBy();

            ipAdd = psk.getIpAdd();
	    setPatientFacilityId(psk.getPatientFacilityId());

        }

        return psk;

    }

    /**
     * 
     * Calls setPrefDetails() on Pref Session Bean: PrefAgentBean
     * 
     * 
     * 
     */

    public PrefBean setPrefDetails() {

        PrefBean psk = null;

        try {

            PrefAgentRObj prefAgentRObj = EJBUtil.getPrefAgentHome();

            psk = prefAgentRObj.setPrefDetails(this.createPrefStateKeeper());

            this.setPrefPKId(psk.getPrefPKId());

            Rlog
                    .debug("pref", "PrefJB.setPrefDetails() PK"
                            + psk.getPrefPKId());

        } catch (Exception e) {

            Rlog.fatal("pref", "Error in setPrefDetails() in PrefJB " + e);

        }

        return psk;

    }

    /**
     * 
     * Calls updatePref() on Pref Session Bean: PrefAgentBean
     * 
     * 
     * 
     * @return
     * 
     */

    public int updatePref()

    {

        int output;

        try {

            PrefAgentRObj prefAgentRObj = EJBUtil.getPrefAgentHome();

            output = prefAgentRObj.updatePref(this.createPrefStateKeeper());

        }

        catch (Exception e) {
            Rlog.fatal("pref",
                    "EXCEPTION IN updating Pref DETAILS TO  SESSION BEAN " + e);
            return -2;

        }

        return output;

    }

    public int removePref() {

        int output;

        try

        {

            PrefAgentRObj prefAgentRObj = EJBUtil.getPrefAgentHome();

            output = prefAgentRObj.removePref(this.prefPKId);

            return output;

        }

        catch (Exception e)

        {

            Rlog.debug("pref", "in JB - remove PREF");

            return -1;

        }

    }

    /**
     * 
     * 
     * 
     * 
     * 
     * @return a statekeeper object for the User Group Record with the current
     *         values of the bean
     * 
     */

    public PrefBean createPrefStateKeeper() {

        Rlog.debug("pref", "PrefJB.createPrefStateKeeper ");

        return new PrefBean(prefPKId, prefPId, prefAccount, prefLocation,
                timeZoneId, creator, modifiedBy, ipAdd,patientFacilityId);

    }

    /*
     * 
     * The following function finds the number of patients with same patient
     * code as 'patientCode' in site 'siteId'
     * 
     */

    public boolean patientCodeExists(String patientCode, String siteId) {

        boolean codeExists = false;

        try {

            PrefAgentRObj prefAgentRObj = EJBUtil.getPrefAgentHome();

            Rlog.debug("pref", "PrefJB.patientCodeExists after remote");

            codeExists = prefAgentRObj.patientCodeExists(patientCode, siteId);

            Rlog.debug("pref", "PrefJB.patientCodeExists after Dao");

        } catch (Exception e) {

            Rlog.fatal("pref", "Error in patientCodeExists in PrefJB " + e);

        }

        return codeExists;

    }
    
    public boolean patientCodeExistsOneOrg(String patientCode, String siteId) {

        boolean codeExists = false;

        try {

            PrefAgentRObj prefAgentRObj = EJBUtil.getPrefAgentHome();

            Rlog.debug("pref", "PrefJB.patientCodeExistsOneOrg after remote");

            codeExists = prefAgentRObj.patientCodeExistsOneOrg(patientCode, siteId);

            Rlog.debug("pref", "PrefJB.patientCodeExistsOneOrg after Dao");

        } catch (Exception e) {

            Rlog.fatal("pref", "Error in patientCodeExistsOneOrg in PrefJB " + e);

        }

        return codeExists;

    }
    
    public boolean patientFacilityCodeExists(String patfacilityid,String patientStudyID) {

        boolean codeExists = false;

        try {

            PrefAgentRObj prefAgentRObj = EJBUtil.getPrefAgentHome();

            Rlog.debug("pref", "PrefJB.patientFacilityCodeExists after remote");

            codeExists = prefAgentRObj.patientFacilityCodeExists(patfacilityid,patientStudyID);

            Rlog.debug("pref", "PrefJB.patientFacilityCodeExists after Dao");

        } catch (Exception e) {

            Rlog.fatal("pref", "Error in patientFacilityCodeExists in PrefJB " + e);

        }

        return codeExists;

    }
    
    public boolean patientCheck(String fname, String lname,String date,String siteId,String patId) {

        boolean patExists = false;

        try {

            PrefAgentRObj prefAgentRObj = EJBUtil.getPrefAgentHome();

            patExists = prefAgentRObj.patientCheck(fname,lname,date, siteId,patId);

            

        } catch (Exception e) {
        	e.printStackTrace();
            Rlog.fatal("pref", "Error in patientCheck in PrefJB " + e);

        }

        return patExists;

    }

    /**
     * 
     * NOT IN USE
     * 
     * 
     * 
     * @return
     * 
     */

    public String toXML()

    {

        Rlog.debug("pref", "PrefJB.toXML()");

        String str = "";

        return str;

    }// end of method

}// end of class

