/*
 * Classname : UserReportLogJB
 * 
 * Version information: 1.0
 *
 * Date: 18/September/2012
 * 
 * Copyright notice: Aithent
 */

package com.velos.eres.web.userReportLog;

import java.io.Serializable;
import java.util.Date;

/**
 * Client side bean for User report logging
 * 
 * @author Yogendra Pratap Singh
 * 
 * @version 1.0 09/18/2012
 */
public class UserReportLogJB implements Serializable{
	
	/*
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 5917809949665147286L;
	
	/**
	 * Primary key of the table
	 * Mapped Table Field : PK_USR_RPT_LOG
     */
	private 	Integer 	pkUserReportLog;// PK_USR_RPT_LOG
	
	/**
	 * This field is used to store user id. Stores PK to ER_USER
	 * Mapped Table Field : FK_USER
	 */
	private 	Integer 	fkUser; //FK_USER
	
	/**
	 * This field is used to store user name 
	 * Mapped Table Field : USR_RPT_LOG_USER_NAME
	 */
	
	/**
	 * This field is used to store report id. Stores PK to ER_REPORT
	 * Mapped Table Field : FK_REPORT
	 */
	private 	Integer 	fkReport; //FK_REPORT
	
	/**
	 * This field stores the REPORT NAME of the page accessed
	 * Mapped Table Field : USR_RPT_LOG_REP_NAME
	 */
	private 	String 	userReportLogReportName; //USR_RPT_LOG_REP_NAME	
	
	/**
	 * Stores the IP ADDRESS of the client machine.
	 * Mapped Table Field : IP_ADD
	 */
	private 	String 		userReportLogIPAdd; // IP_ADD;
	
	/**
	 * Stores the URL of the client machine.
	 * Mapped Table Field : USR_RPT_LOG_URL
	 */
	private 	String 		userReportLogURL; //USR_RPT_LOG_URL
	
	/**
	 * This field stores the date and time on which the page was accessed
	 * Mapped Table Field : USR_RPT_LOG_ACCESSTIME
	 */
	private 	Date		userReportLogAccessTime; // USR_RPT_LOG_ACCESSTIME
	
	/**
	 * This field Stores the list of URL Query parameters passed to the page
	 * Mapped Table Field : USR_RPT_LOG_URLPARAM
	 */
	private 	String 		userReportLogURLQueryParam; //USR_RPT_LOG_URLPARAM	

	/**
	 * This field stores the module name given to the page.
     * the module name (From where user is viewing/downloading  
     * the reports like: Invoice, calendars, Budgets, Enrollment, 
     * Study Reports, Patient Reports, Ad-Hoc )
     * 
     * Mapped Table Field : USR_RPT_LOG_MODNAME
     */
	private 	String 		userReportLogModuleName; //USR_RPT_LOG_MODNAME	

	/**
	 * This field stores the download location of the report on the server.
	 * Mapped Table Field : USR_RPT_LOG_FILENAME
	 */
	private  	String 		userReportLogFileName; //USR_RPT_LOG_FILENAME
	
	/**
	 * This field stores the download location of the report on the server.
	 * Mapped Table Field : USR_RPT_LOG_LOCATION
	 */
	private 	String 		userReportLogDownloadPath; //USR_RPT_LOG_LOCATION
	
	/**
	 * This field is used to store report format(Microsoft Word,Excel,PDF 
	 * or Printer Friendly Format).
	 * 
	 * Mapped Table Field : USR_RPT_LOG_FORMAT
	 */
	private 	String 		userReportLogFormat; //USR_RPT_LOG_FORMAT
	
	/**
	 * This field stores the generated XML of the accessed report
	 * Mapped Table Field : USR_RPT_LOG_XML
	 */
	private 	String 		userReportLogXML; //USR_RPT_LOG_XML
	
	
	/**
	 * This field stores the success flag for downloaded report. 
	 * Stores : 1 if report download was successful, 
	 *          0 if report download was unsuccessful or not done
	 * 
	 * Mapped Table Field : USR_RPT_LOG_DOWNLOAD_FLAG
	 */
	private 	Integer 	isUserReportLogDownloadFlag;//USR_RPT_LOG_DOWNLOAD_FLAG;
	
	/**
	 * This field stores the Table Name related to the FK_FILE id
	 * Mapped Table Field : USR_RPT_LOG_TABLE_NAME
	 */
	private String tableName;

	
	
	/**
	 * @return the pkUserReportLog
	 */
	public Integer getPkUserReportLog() {
		return pkUserReportLog;
	}

	/**
	 * @param pkUserReportLog the pkUserReportLog to set
	 */
	public void setPkUserReportLog(Integer pkUserReportLog) {
		this.pkUserReportLog = pkUserReportLog;
	}

	/**
	 * @return the fkUser
	 */
	public Integer getFkUser() {
		return fkUser;
	}

	/**
	 * @param fkUser the fkUser to set
	 */
	public void setFkUser(Integer fkUser) {
		this.fkUser = fkUser;
	}


	/**
	 * @return the fkReport
	 */
	public Integer getFkReport() {
		return fkReport;
	}

	/**
	 * @param fkReport the fkReport to set
	 */
	public void setFkReport(Integer fkReport) {
		this.fkReport = fkReport;
	}

	/**
	 * @return the userReportLogReportName
	 */
	public String getUserReportLogReportName() {
		return userReportLogReportName;
	}

	/**
	 * @param userReportLogReportName the userReportLogReportName to set
	 */
	public void setUserReportLogReportName(String userReportLogReportName) {
		this.userReportLogReportName = userReportLogReportName;
	}

	/**
	 * @return the userReportLogIPAdd
	 */
	public String getUserReportLogIPAdd() {
		return userReportLogIPAdd;
	}

	/**
	 * @param userReportLogIPAdd the userReportLogIPAdd to set
	 */
	public void setUserReportLogIPAdd(String userReportLogIPAdd) {
		this.userReportLogIPAdd = userReportLogIPAdd;
	}

	/**
	 * @return the userReportLogURL
	 */
	public String getUserReportLogURL() {
		return userReportLogURL;
	}

	/**
	 * @param userReportLogURL the userReportLogURL to set
	 */
	public void setUserReportLogURL(String userReportLogURL) {
		this.userReportLogURL = userReportLogURL;
	}

	/**
	 * @return the userReportLogAccessTime
	 */
	public Date getUserReportLogAccessTime() {
		return userReportLogAccessTime;
	}

	/**
	 * @param userReportLogAccessTime the userReportLogAccessTime to set
	 */
	public void setUserReportLogAccessTime(Date userReportLogAccessTime) {
		this.userReportLogAccessTime = userReportLogAccessTime;
	}

	/**
	 * @return the userReportLogURLQueryParam
	 */
	public String getUserReportLogURLQueryParam() {
		return userReportLogURLQueryParam;
	}

	/**
	 * @param userReportLogURLQueryParam the userReportLogURLQueryParam to set
	 */
	public void setUserReportLogURLQueryParam(String userReportLogURLQueryParam) {
		this.userReportLogURLQueryParam = userReportLogURLQueryParam;
	}

	/**
	 * @return the userReportLogModuleName
	 */
	public String getUserReportLogModuleName() {
		return userReportLogModuleName;
	}

	/**
	 * @param userReportLogModuleName the userReportLogModuleName to set
	 */
	public void setUserReportLogModuleName(String userReportLogModuleName) {
		this.userReportLogModuleName = userReportLogModuleName;
	}

	/**
	 * @return the userReportLogDownloadPath
	 */
	public String getUserReportLogDownloadPath() {
		return userReportLogDownloadPath;
	}

	/**
	 * @param userReportLogDownloadPath the userReportLogDownloadPath to set
	 */
	public void setUserReportLogDownloadPath(String userReportLogDownloadPath) {
		this.userReportLogDownloadPath = userReportLogDownloadPath;
	}

	/**
	 * @return the userReportLogFormat
	 */
	public String getUserReportLogFormat() {
		return userReportLogFormat;
	}

	/**
	 * @param userReportLogFormat the userReportLogFormat to set
	 */
	public void setUserReportLogFormat(String userReportLogFormat) {
		this.userReportLogFormat = userReportLogFormat;
	}

	/**
	 * @return the userReportLogXML
	 */
	public String getUserReportLogXML() {
		return userReportLogXML;
	}

	/**
	 * @param userReportLogXML the userReportLogXML to set
	 */
	public void setUserReportLogXML(String userReportLogXML) {
		this.userReportLogXML = userReportLogXML;
	}


	/**
	 * @return the isUserReportLogDownloadFlag
	 */
	public Integer getIsUserReportLogDownloadFlag() {
		return isUserReportLogDownloadFlag;
	}

	/**
	 * @param isUserReportLogDownloadFlag the isUserReportLogDownloadFlag to set
	 */
	public void setIsUserReportLogDownloadFlag(Integer isUserReportLogDownloadFlag) {
		this.isUserReportLogDownloadFlag = isUserReportLogDownloadFlag;
	}

	/**
	 * @return the userReportLogFileName
	 */
	public String getUserReportLogFileName() {
		return userReportLogFileName;
	}

	/**
	 * @param userReportLogFileName the userReportLogFileName to set
	 */
	public void setUserReportLogFileName(String userReportLogFileName) {
		this.userReportLogFileName = userReportLogFileName;
	}

	/**
	 * @return the tableName
	 */
	public String getTableName() {
		return tableName;
	}

	/**
	 * @param tableName the tableName to set
	 */
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
}
