/*
 * Classname : 				PatProtJB
 * 
 * Version information : 	1.0
 *
 * Date 					06/16/2001
 * 
 * Copyright notice: 		Velos Inc
 * 
 * Author 					Sajal
 */

package com.velos.eres.web.patProt;

/* IMPORT STATEMENTS */

import com.velos.eres.business.common.PatFacilityDao;
import com.velos.eres.business.patProt.impl.PatProtBean;
import com.velos.eres.service.patFacilityAgent.PatFacilityAgentRObj;
import com.velos.eres.service.patProtAgent.PatProtAgentRObj;

import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.esch.business.common.ScheduleDao;
import java.util.Hashtable;

/* END OF IMPORT STATEMENTS */

/**
 * PatProtJB is a client side Java Bean for Patient Enrollment business
 * components. The class implements the Business Delegate pattern to reduce
 * coupling between presentation-tier clients and business services.
 * 
 * <p>
 * In eResearch Architecture, the class resides at the Application layer.
 * </p>
 * 
 * @author Sajal
 * @version %I%, %G%
 */
/*
 * *************************History********************************* Modified by
 * :Sonia Modified On:08/18/2004 Comment: 1. Added new attribute
 * patProtRandomNumber,patProtPhysician and respective getter/setters 2.
 * Modified methods for the new attribute 3. Modified JavaDoc comments
 * *************************END****************************************
 */

public class PatProtJB {

    /*
     * CLASS METHODS
     * 
     * public int getPatProtId() public void setPatProtId(int patProtId) public
     * String getPatProtProtocolId() public void setPatProtProtocolId(String
     * patProtProtocolId) public String getPatProtStudyId() public void
     * setPatProtStudyId(String patProtStudyId) public String getPatProtUserId()
     * public void setPatProtUserId(String patProtUserId) public String
     * getPatProtEnrolDt() public void setPatProtEnrolDt(String patProtEnrolDt)
     * public String getPatProtPersonId() public void setPatProtPersonId(String
     * patProtPersonId) public String getPatProtStartDt() public void
     * setPatProtStartDt(String patProtStartDt) public String getPatProtNotes()
     * public void setPatProtNotes(String patProtNotes) public String
     * getPatProtStat() public void setPatProtStat(String patProtStat) public
     * PatProtJB(int patProtId) { public PatProtJB() { public PatProtJB(int
     * patProtId, String patProtProtocolId, String patProtStudyId, String
     * patProtUserId, String patProtEnrolDt, String patProtPersonId, String
     * patProtStartDt, String patProtNotes, String patProtStat) public
     * PatProtStateKeeper getPatProtDetails() { public void setPatProtDetails()
     * public int updatePatProt() public PatProtStateKeeper
     * createPatProtStateKeeper()
     * 
     * 
     * END OF CLASS METHODS
     */

    /**
     * PatProt Id
     */
    private int patProtId;

    /**
     * PatProt Protocol Id
     */
    private String patProtProtocolId;

    /**
     * PatProt Study Id
     */
    private String patProtStudyId;

    /**
     * PatProt User Id
     */
    private String patProtUserId;

    /**
     * PatProt Enrollment date
     */
    private String patProtEnrolDt;

    /**
     * PatProt Person Id
     */
    private String patProtPersonId;

    /**
     * PatProt Start Date
     */
    private String patProtStartDt;

    /**
     * PatProt Notes
     */
    private String patProtNotes;

    /**
     * PatProt Status
     */
    private String patProtStat;

    /**
     * Time Zone
     */
    private String timeZoneId;

    /**
     * creator
     */
    private String creator;

    /**
     * last modified by
     */
    private String modifiedBy;

    /**
     * IP Address
     */
    private String ipAdd;

    /**
     * Pat Prot Schedule Start Day
     */
    private String patProtStartDay;

    /**
     * PatProt reason for protocol change
     */
    private String patProtReason;

    /**
     * PatProt Discontinuation date
     */
    private String patProtDiscDt;

    /**
     * Patient's study id
     */
    private String patStudyId;

    /**
     * Informed Consent signed on date
     */
    private String consignDt;

    /**
     * Informed Consent resigned on date 1
     */
    private String reSignDt1;

    /**
     * Informed Consent resigned on date 2
     */
    private String reSignDt2;

    /**
     * Patient signed the informed consent: 0-signed , 1-not signed
     */
    private String consign;

    /**
     * Patient Randomization Number
     */
    private String patProtRandomNumber;

    /**
     * Patient Physician
     */
    private String patProtPhysician;

    // /

    /**
     * Patient evaluation flag
     */
    private String ptstEvalFlag;

    /**
     * Patient evaluation status
     */
    private String ptstEval;

    /**
     * Patient inevaluation status
     */
    private String ptstInEval;

    /**
     * Patient survival status
     */
    private String ptstSurvival;

    /**
     * Patient death related to study
     */
    private String ptstDthStdRel;

    /**
     * Patient reason for deatj related to study
     */
    private String ptstDthStdRelOther;

    /**
     * Patient consent verison
     */
    private String ptstConsentVer;

    /**
     * Patient next followup date
     */
    private String ptstNextFlwup;

    /**
     * Patient's death date
     */
    private String ptstDeathDate;
    
    /**
     * Patient's Treating Organization
     */
    private String patOrg;  
    
    /**
     * Patient Demographics Reportable
     */
    private String dGrphReportable;
    
    /**
     * Patient Study Data 
     */
    private String patDiseaseCode;

    /**
     * the Disease site
     */
    private String patOtherDisCode;

    /**
     * Patient Specific Sites1
     */
    private String patMoreDisCode1;

    /**
     * Patient Specific Sites2
     */
    private String patMoreDisCode2;
    

	//  JM: 07Aug2006
    public String getpatOrg() {
        return this.patOrg;
    }

    public void setpatOrg(String patOrg) {
        this.patOrg = patOrg;
    }
    
  
	public String getPatDiseaseCode() {
		return patDiseaseCode;
	}

	public void setPatDiseaseCode(String patDiseaseCode) {
		this.patDiseaseCode = patDiseaseCode;
	}

	public String getdGrphReportable() {
		return dGrphReportable;
	}

	public void setdGrphReportable(String dGrphReportable) {
		this.dGrphReportable = dGrphReportable;
	}
	
    public String getPatMoreDisCode1() {
        return this.patMoreDisCode1;
    }

    public void setPatMoreDisCode1(String patMoreDisCode1) {
        this.patMoreDisCode1 = patMoreDisCode1;
    }

    public String getPatMoreDisCode2() {
        return this.patMoreDisCode2;
    }

    public void setPatMoreDisCode2(String patMoreDisCode2) {
        this.patMoreDisCode2 = patMoreDisCode2;
    }

    public String getPatOtherDisCode() {
        return patOtherDisCode;
    }

    public void setPatOtherDisCode(String patOtherDisCode) {
        this.patOtherDisCode = patOtherDisCode;
    }

	public String getPatProtReason() {
        return this.patProtReason;
    }

    public void setPatProtReason(String patProtReason) {
        this.patProtReason = patProtReason;
    }

    public String getPatProtDiscDt() {
        return this.patProtDiscDt;
    }

    public void setPatProtDiscDt(String patProtDiscDt) {
        this.patProtDiscDt = patProtDiscDt;
    }

    public String getPatProtStartDay() {
        return this.patProtStartDay;
    }

    public void setPatProtStartDay(String patProtStartDay) {
        this.patProtStartDay = patProtStartDay;
    }

    /*
     * Patient assigned to
     */
    private String assignTo;

    /*
     * Patient's treatment location
     */
    private String treatmentLoc;
    
    /**
     * Patient's enrollling organization
     */
    private String enrollingSite = "";

    // GETTER SETTER METHODS

    /**
     * @return PatProt Id
     */
    public int getPatProtId() {
        return this.patProtId;
    }

    /**
     * @param patProtId
     *            The value that is required to be registered as PatProt Id
     */
    public void setPatProtId(int patProtId) {
        this.patProtId = patProtId;
    }

    /**
     * @return PatProt Protocol Id
     */
    public String getPatProtProtocolId() {
        return this.patProtProtocolId;
    }

    /**
     * @param patProtProtocolId
     *            The value that is required to be registered as PatProt
     *            Protocol Id
     */
    public void setPatProtProtocolId(String patProtProtocolId) {
        this.patProtProtocolId = patProtProtocolId;
    }

    /**
     * @return PatProt Study Id
     */
    public String getPatProtStudyId() {
        return this.patProtStudyId;
    }

    /**
     * @param patProtStudyId
     *            The value that is required to be registered as PatProt Study
     *            Id
     */
    public void setPatProtStudyId(String patProtStudyId) {
        this.patProtStudyId = patProtStudyId;
    }

    /**
     * @return PatProt User Id
     */
    public String getPatProtUserId() {
        return this.patProtUserId;
    }

    /**
     * @param patProtUserId
     *            The value that is required to be registered as PatProt User Id
     */
    public void setPatProtUserId(String patProtUserId) {
        this.patProtUserId = patProtUserId;
    }

    /**
     * @return PatProt Enrollment date
     */
    public String getPatProtEnrolDt() {
        return this.patProtEnrolDt;
    }

    /**
     * @param patProtEnrolDt
     *            The value that is required to be registered as PatProt
     *            Enrollment date
     */
    public void setPatProtEnrolDt(String patProtEnrolDt) {
        this.patProtEnrolDt = patProtEnrolDt;
    }

    /**
     * @return PatProt Person Id
     */
    public String getPatProtPersonId() {
        return this.patProtPersonId;
    }

    /**
     * @param patProtPersonId
     *            The value that is required to be registered as PatProt Person
     *            Id
     */
    public void setPatProtPersonId(String patProtPersonId) {
        this.patProtPersonId = patProtPersonId;
    }

    /**
     * @return PatProt Start Date
     */
    public String getPatProtStartDt() {
        return this.patProtStartDt;
    }

    /**
     * @param patProtStartDt
     *            The value that is required to be registered as PatProt Start
     *            Date
     */
    public void setPatProtStartDt(String patProtStartDt) {
        Rlog.debug("patProt", "patProtStartDt@@@@@@ " + patProtStartDt);
        this.patProtStartDt = patProtStartDt;
    }

    /**
     * @return PatProt Notes
     */
    public String getPatProtNotes() {
        return this.patProtNotes;
    }

    /**
     * @param patProtNotes
     *            The value that is required to be registered as PatProt Notes
     */
    public void setPatProtNotes(String patProtNotes) {
        this.patProtNotes = patProtNotes;
    }

    /**
     * @return PatProt Status
     */
    public String getPatProtStat() {
        return this.patProtStat;
    }

    /**
     * @param patProtStat
     *            The value that is required to be registered as PatProt Status
     */
    public void setPatProtStat(String patProtStat) {
        this.patProtStat = patProtStat;
    }

    /**
     * @return Time Zone
     */
    public String getTimeZoneId() {
        return this.timeZoneId;
    }

    public void setTimeZoneId(String timeZoneId) {
        this.timeZoneId = timeZoneId;
    }

    /**
     * @return Creator
     */
    public String getCreator() {
        return this.creator;
    }

    /**
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return Last Modified By
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param modifiedBy
     *            The value that is required to be registered as record last
     *            modified by
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return IP Address
     */
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
     * @param patStudyId
     *            The value that is required to be registered as Patient Study
     *            Id
     */
    public void setPatStudyId(String patStudyId) {
        Rlog.debug("patProt", "patStudyId@@@@@@ " + patStudyId);
        this.patStudyId = patStudyId;

    }

    /**
     * @return Patient Study Id
     */
    public String getPatStudyId() {
        return this.patStudyId;
    }

    /**
     * @param consignDt
     *            The value that is required to be registered as Informed
     *            Consent signed date
     */
    public void setConsignDt(String consignDt) {
        Rlog.debug("patProt", "consignDt@@@@@@ " + consignDt);
        this.consignDt = consignDt;

    }

    /**
     * @return Informed Consent signed date
     */
    public String getConsignDt() {

        return this.consignDt;
    }

    /**
     * @param reSignDt1
     *            The value that is required to be registered as Informed
     *            Consent resigned on date 1
     */
    public void setReSignDt1(String reSignDt1) {
        Rlog.debug("patProt", "reSignDt1@@@@@@ " + reSignDt1);
        this.reSignDt1 = reSignDt1;

    }

    /**
     * @return Informed Consent resigned on date 1
     */
    public String getReSignDt1() {

        return this.reSignDt1;
    }

    /**
     * @param reSignDt2
     *            The value that is required to be registered as Informed
     *            Consent resigned on date 2
     */
    public void setReSignDt2(String reSignDt2) {
        Rlog.debug("patProt", "reSignDt2@@@@@@ " + reSignDt2);
        this.reSignDt2 = reSignDt2;

    }

    /**
     * @return Informed Consent resigned on date 2
     */
    public String getReSignDt2() {
        return this.reSignDt2;
    }

    /**
     * @param reSignDt2
     *            The value that is required to be registered as Whether Patient
     *            signed the informed consent 0-signed , 1-not signed
     */
    public void setConsign(String consign) {
        Rlog.debug("patProt", "consign@@@@@@ " + consign);
        this.consign = consign;

    }

    public String getConsign() {
        return this.consign;
    }

    /**
     * @return assignTo
     */
    public String getAssignTo() {
        return this.assignTo;
    }

    /**
     * @param assignTo
     */
    public void setAssignTo(String assignTo) {
        Rlog.debug("patProt", "assignTo@@@@@@ " + assignTo);
        this.assignTo = assignTo;

    }

    /**
     * @return treatmentLoc
     */
    public String getTreatmentLoc() {
        return this.treatmentLoc;
    }

    /**
     * @param treatmentLoc
     */
    public void setTreatmentLoc(String treatmentLoc) {
        Rlog.debug("patProt", "treatmentLoc@@@@@@ " + treatmentLoc);
        this.treatmentLoc = treatmentLoc;

    }

    /**
     * Returns patient Randomization Number
     * 
     * @return Patient Randomization Number
     */
    public String getPatProtRandomNumber() {
        return patProtRandomNumber;
    }

    /**
     * Sets patient Randomization Number
     * 
     * @param patProtRandomNumber
     *            Patient Randomization Number
     */
    public void setPatProtRandomNumber(String patProtRandomNumber) {
        this.patProtRandomNumber = patProtRandomNumber;
    }

    /**
     * Returns Patient Enrollment Physician id
     * 
     * @return Patient Enrollment Physician id
     */
    public String getPatProtPhysician() {
        return patProtPhysician;
    }

    /**
     * Sets Patient Enrollment Physician id
     * 
     * @param patProtPhysician
     *            The Patient Enrollment Physician id.
     */
    public void setPatProtPhysician(String patProtPhysician) {
        this.patProtPhysician = patProtPhysician;
    }

    // /
    public String getPtstEvalFlag() {
        return ptstEvalFlag;
    }

    public void setPtstEvalFlag(String ptstEvalFlag) {
        this.ptstEvalFlag = ptstEvalFlag;
    }

    public String getPtstEval() {
        return ptstEval;
    }

    public void setPtstEval(String ptstEval) {
        this.ptstEval = ptstEval;
    }

    public String getPtstInEval() {
        return ptstInEval;
    }

    public void setPtstInEval(String ptstInEval) {
        this.ptstInEval = ptstInEval;
    }

    public String getPtstSurvival() {
        return ptstSurvival;
    }

    public void setPtstSurvival(String ptstSurvival) {
        this.ptstSurvival = ptstSurvival;
    }

    public String getPtstDthStdRel() {
        return ptstDthStdRel;
    }

    public void setPtstDthStdRel(String ptstDthStdRel) {
        this.ptstDthStdRel = ptstDthStdRel;
    }

    public String getPtstDthStdRelOther() {
        return ptstDthStdRelOther;
    }

    public void setPtstDthStdRelOther(String ptstDthStdRelOther) {
        this.ptstDthStdRelOther = ptstDthStdRelOther;
    }

    public String getPtstConsentVer() {
        return ptstConsentVer;
    }

    public void setPtstConsentVer(String ptstConsentVer) {
        this.ptstConsentVer = ptstConsentVer;
    }

    public String getPtstNextFlwup() {
        return ptstNextFlwup;
    }

    public void setPtstNextFlwup(String ptstNextFlwup) {
        this.ptstNextFlwup = ptstNextFlwup;
    }

    public String getPtstDeathDate() {
        return ptstDeathDate;
    }

    public void setPtstDeathDate(String ptstDeathDate) {
        this.ptstDeathDate = ptstDeathDate;
    }

    // END OF GETTER SETTER METHODS

    /**
     * Defines a PatProtJB object with the specified patProtId
     * 
     * @param patProtId
     *            Patient Enrollment id
     */
    public PatProtJB(int patProtId) {
        setPatProtId(patProtId);
    }

    /**
     * Default Constructor
     */
    public PatProtJB() {
        Rlog.debug("patProt", "PatProtJB.PatProtJB() ");
    };

	public String getEnrollingSite() {
		return enrollingSite;
	}

	public void setEnrollingSite(String enrollingSite) {
		this.enrollingSite = enrollingSite;
	}

	
    /**
     * Full argument Constructor
     * 
     * @param patProtId
     *            Patient Enrollment Id
     * @param patProtProtocolId
     *            Protocol Id if patient is put on any protocol/calendar
     * @param patProtStudyId
     *            Study Id - Study id on which patient is enrolled
     * @param patProtUserId
     *            The user who has enrolled the patient (person) to the study
     * @param patProtEnrolDt
     *            Enrollment Date
     * @param patProtPersonId
     *            The patient Id
     * @param patProtStartDt
     *            The Start date of the patient protocol enrollment. <BR>
     *            this is the date used as start date for the schedule
     *            generation of the patient
     * @param patProtNotes
     *            Notes or comments for the Patient protocol enrollment
     * @param patProtStat
     *            This column stores the status of the patient/protocol
     *            enrollment.<br>
     *            1 indicates Active enrollment and 0 indicates inactive
     *            enrollment.
     * @param timeZoneId
     *            Time zone setting for an enrollment record
     * @param creator
     *            User who created the record
     * @param modifiedBy
     *            User who last modified the record
     * @param ipAddString
     *            IP Address of User who created/last modified the record
     * @param patProtStartDay
     *            The day from which the schedule is to be calculated
     * @param patProtReason
     *            The reason of protocol discontinuation
     * @param patProtDiscDt
     *            The protocol discontinuation date
     * @param patStudyId
     *            Patient's study Identification Number
     * @param consignDt
     *            The informed consent signed on date.(Not in use after patient
     *            status enhacements, Aug 04)
     * @param reSignDt1
     *            The Informed Consent Re-signed On - Date 1. (Not in use after
     *            patient status enhacements, Aug 04)
     * @param reSignDt2
     *            The Informed Consent Re-signed On - Date 2. (Not in use after
     *            patient status enhacements, Aug 04)
     * @param consign
     *            Flag whether patient signed the Informed Consent.(Not in use
     *            after patient status enhacements, Aug 04)
     * @param assignTo
     *            The user patient is assigned to
     * @param treatmentLoc
     *            The Code for treatment location
     * @param patProtRandomNumber
     *            The Randomization Number for patient enrollment
     * @param patProtPhysician
     *            Physician for patient enrollment
     */
    public PatProtJB(int patProtId, String patProtProtocolId,
            String patProtStudyId, String patProtUserId, String patProtEnrolDt,
            String patProtPersonId, String patProtStartDt, String patProtNotes,
            String patProtStat, String timeZoneId, String creator,
            String modifiedBy, String ipAddString, String patProtStartDay,
            String patProtReason, String patProtDiscDt, String patStudyId,
            String consignDt, String reSignDt1, String reSignDt2,
            String consign, String assignTo, String treatmentLoc,
            String patProtRandomNumber, String patProtPhysician,
            String ptstEvalFlag, String ptstEval, String ptstInEval,
            String ptstSurvival, String ptstDthStdRel,
            String ptstDthStdRelOther, String ptstConsentVer,
            String ptstNextFlwup, String ptstDeathDate, String enrollingSite, String patOrg) {

        setPatProtId(patProtId);
        setPatProtProtocolId(patProtProtocolId);
        setPatProtStudyId(patProtStudyId);
        setPatProtUserId(patProtUserId);
        setPatProtEnrolDt(patProtEnrolDt);
        setPatProtPersonId(patProtPersonId);
        setPatProtStartDt(patProtStartDt);
        setPatProtNotes(patProtNotes);
        setPatProtStat(patProtStat);
        setTimeZoneId(timeZoneId);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setPatProtReason(patProtReason);
        setPatProtDiscDt(patProtDiscDt);
        setPatProtStartDay(patProtStartDay);
        setPatStudyId(patStudyId);
        setConsignDt(consignDt);
        setReSignDt1(reSignDt1);
        setReSignDt2(reSignDt2);
        setConsign(consign);
        setAssignTo(assignTo);
        setTreatmentLoc(treatmentLoc);
        setPatProtRandomNumber(patProtRandomNumber);
        setPatProtPhysician(patProtPhysician);
        setPtstEvalFlag(ptstEvalFlag);
        setPtstEval(ptstEval);
        setPtstInEval(ptstInEval);
        setPtstSurvival(ptstSurvival);
        setPtstDthStdRel(ptstDthStdRel);
        setPtstDthStdRelOther(ptstDthStdRelOther);
        setPtstConsentVer(ptstConsentVer);
        setPtstNextFlwup(ptstNextFlwup);
        setPtstDeathDate(ptstDeathDate);
        setEnrollingSite(enrollingSite);
        setpatOrg(patOrg);//JM
        Rlog.debug("patProt", "PatProtJB.PatProtJB(all parameters)");
    }

    /**
     * Populates itself with the patient study enrollment details using its
     * attribute : id Calls getPatProtDetails() on the Patient Study Enrollment
     * Session Bean: PatProtAgentBean. <br>
     * The session bean method returns a PatProtStateKeeper object that is used
     * by this method <br>
     * to populate the object's attributes.
     * 
     * @return A PatProtBean object
     * @see com.velos.eres.busniess.patProt.impl.PatProtBean
     */
    /* modified by sonia, 08/25/04, use the common setter for the javabean */
    public PatProtBean getPatProtDetails() {
        PatProtBean ppsk = null;
        try {
            PatProtAgentRObj patProtAgentRObj = EJBUtil.getPatProtAgentHome();

            ppsk = patProtAgentRObj.getPatProtDetails(this.patProtId);
            Rlog.debug("patProt",
                    "PatProtJB.getPatProtDetails() PatProtStateKeeper " + ppsk);
        } catch (Exception e) {
            Rlog.fatal("PatProt", "Error in getPatProtDetails() in PatProtJB "
                    + e);
        }
        if (ppsk != null) {
            setJBFromStateKeeper(ppsk);
        }
        return ppsk;
    }

    /**
     * Creates a new patient study enrollment using the object's attributes.
     * Calls setPatProtDetails() on the <br>
     * Session Bean PatProtAgentBean.
     * 
     * @return success flag
     *         <ul>
     *         <li> id of new patient study enrollment</li>
     *         <li> -1 in case of a failure</li>
     *         </ul>
     */
    public int setPatProtDetails() {
        int output = 0;
        try {
            PatProtAgentRObj patProtAgentRObj = EJBUtil.getPatProtAgentHome();

            this.setPatProtId(patProtAgentRObj.setPatProtDetails(this
                    .createPatProtStateKeeper()));
            output = this.getPatProtId();
            Rlog.debug("patProt", "PatProtJB.setPatProtDetails()");
            return output;
        } catch (Exception e) {
            Rlog.fatal("patProt", "Error in setPatProtDetails() in PatProtJB "
                    + e);
            return -1;
        }
    }

    /**
     * Updates a Patient Study Enrollment using the object's attributes. Calls
     * updatePatProt() on the <br>
     * PatProtAgent Session Bean
     * 
     * @return success flag
     *         <ul>
     *         <li> 0 if successful</li>
     *         <li> -2 in case of a failure</li>
     *         </ul>
     */
    public int updatePatProt() {
        int output;
        try {
            PatProtAgentRObj patProtAgentRObj = EJBUtil.getPatProtAgentHome();

            Rlog.debug("patProt", "PatProtJB.patProtAgentRObj"
                    + patProtAgentRObj);
            output = patProtAgentRObj.updatePatProt(this
                    .createPatProtStateKeeper());
            Rlog.debug("patProt", "PatProtJB.output" + output);

        } catch (Exception e) {
            Rlog
                    .fatal("patProt",
                            "EXCEPTION IN SETTING PatProt DETAILS TO  SESSION BEAN"
                                    + e);
            return -2;
        }
        return output;
    }

    /**
     * Returns an PatProtStateKeeper object with the current values of the Bean
     * 
     * @return PatProtStateKeeper object
     * @see com.velos.eres.business.patProt.PatProtStateKeeper
     */
    public PatProtBean createPatProtStateKeeper() {
        Rlog.debug("patProt", "PatProtJB.createPatProtStateKeeper ");

        return new PatProtBean(this.patProtId, this.patProtProtocolId,
                this.patProtStudyId, this.patProtUserId, this.patProtEnrolDt,
                this.patProtPersonId, this.patProtStartDt, this.patProtNotes,
                this.patProtStat, this.timeZoneId, this.creator,
                this.modifiedBy, this.ipAdd, this.patProtStartDay,
                this.patProtReason, this.patProtDiscDt, this.patStudyId,
                this.consignDt, this.reSignDt1, this.reSignDt2, this.consign,
                this.assignTo, this.treatmentLoc, this.patProtRandomNumber,
                this.patProtPhysician, this.ptstEvalFlag, this.ptstEval,
                this.ptstInEval, this.ptstSurvival, this.ptstDthStdRel,
                this.ptstDthStdRelOther, this.ptstConsentVer,
                this.ptstNextFlwup, this.ptstDeathDate,this.enrollingSite ,
                this.patOrg, this.patDiseaseCode, this.dGrphReportable, 
                this.patMoreDisCode1,this.patMoreDisCode2, this.patOtherDisCode);

    }

    /* added by sonia, 08/25/04 */
    /**
     * Complete setter method for the JavaBean.<br>
     * Sets the attibutes of the JavaBean using the PatProtStateKeeper object's
     * attributes
     * 
     * @param ppsk
     *            A PatProtStateKeeper object
     */
    public void setJBFromStateKeeper(PatProtBean ppsk) {
        if (ppsk != null) {
            this.patProtId = ppsk.getPatProtId();
            this.patProtProtocolId = ppsk.getPatProtProtocolId();
            this.patProtStudyId = ppsk.getPatProtStudyId();
            this.patProtUserId = ppsk.getPatProtUserId();
            this.patProtEnrolDt = ppsk.returnPatProtEnrolDt();
            this.patProtPersonId = ppsk.getPatProtPersonId();
            this.patProtStartDt = ppsk.getPatProtStartDt();
            this.patProtNotes = ppsk.getPatProtNotes();
            this.patProtStat = ppsk.getPatProtStat();
            this.timeZoneId = ppsk.getTimeZoneId();
            this.creator = ppsk.getCreator();
            this.modifiedBy = ppsk.getModifiedBy();
            this.ipAdd = ppsk.getIpAdd();
            this.patStudyId = ppsk.getPatStudyId();
            this.consignDt = ppsk.getConsignDt();
            this.reSignDt1 = ppsk.getReSignDt1();
            this.reSignDt2 = ppsk.getReSignDt2();
            this.consign = ppsk.getConsign();
            this.assignTo = ppsk.getAssignTo();
            this.treatmentLoc = ppsk.getTreatmentLoc();
            
            this.patOrg = ppsk.getpatOrg();//JM
            this.dGrphReportable = ppsk.getdGrphReportable();//JM
            
            this.patDiseaseCode = ppsk.getPatDiseaseCode();
            this.patMoreDisCode1 = ppsk.getPatMoreDisCode1();
            this.patMoreDisCode2 = ppsk.getPatMoreDisCode2();
            this.patOtherDisCode = ppsk.getPatOtherDisCode();
            setPatProtReason(ppsk.getPatProtReason());
            setPatProtDiscDt(ppsk.getPatProtDiscDt());
            setPatProtStartDay(ppsk.getPatProtStartDay());
            setPatProtRandomNumber(ppsk.getPatProtRandomNumber());
            setPatProtPhysician(ppsk.getPatProtPhysician());

            setPtstEvalFlag(ppsk.getPtstEvalFlag());
            setPtstEval(ppsk.getPtstEval());
            setPtstInEval(ppsk.getPtstInEval());
            setPtstSurvival(ppsk.getPtstSurvival());
            setPtstDthStdRel(ppsk.getPtstDthStdRel());
            setPtstDthStdRelOther(ppsk.getPtstDthStdRelOther());
            setPtstConsentVer(ppsk.getPtstConsentVer());
            setPtstNextFlwup(ppsk.getPtstNextFlwup());
            setPtstDeathDate(ppsk.getPtstDeathDate());
            setEnrollingSite(ppsk.getEnrollingSite());           
                       
            

        }
    }

    /**
     * Populates itself with the current patient study enrollment details Calls
     * findCurrentPatProtDetails() on the Patient Study Enrollment Session Bean:
     * PatProtAgentBean. <br>
     * The session bean method returns a PatProtStateKeeper object that is used
     * by this method <br>
     * to populate the object's attributes.
     * 
     * @param studyId
     *            Study Id
     * @param patientId
     *            patient Id
     * @return A PatProtStateKeeper object
     * @see com.velos.eres.busniess.patProt.PatProtStateKeeper
     */
    public PatProtBean findOldestPatProtDetails(int studyId, int patientId) {
        PatProtBean ppsk = null;
        try {
            PatProtAgentRObj patProtAgentRObj = EJBUtil.getPatProtAgentHome();

            ppsk = patProtAgentRObj.findOldestPatProtDetails(studyId, patientId);
            Rlog.debug("patProt",
                    "PatProtJB.findOldestPatProtDetails() PatProtStateKeeper "
                            + ppsk);
        } catch (Exception e) {
            Rlog.fatal("PatProt",
                    "Exception in findOldestPatProtDetails() in PatProtJB " + e);
        }
        if (ppsk != null) {
            setJBFromStateKeeper(ppsk);
        }
        return ppsk;
    }

    /**
     * Populates itself with the current patient study enrollment details Calls
     * findCurrentPatProtDetails() on the Patient Study Enrollment Session Bean:
     * PatProtAgentBean. <br>
     * The session bean method returns a PatProtStateKeeper object that is used
     * by this method <br>
     * to populate the object's attributes.
     * 
     * @param studyId
     *            Study Id
     * @param patientId
     *            patient Id
     * @return A PatProtStateKeeper object
     * @see com.velos.eres.busniess.patProt.PatProtStateKeeper
     */
    public PatProtBean findCurrentPatProtDetails(int studyId, int patientId) {
        PatProtBean ppsk = null;
        try {
            PatProtAgentRObj patProtAgentRObj = EJBUtil.getPatProtAgentHome();

            ppsk = patProtAgentRObj.findCurrentPatProtDetails(studyId,
                    patientId);
            Rlog.debug("patProt",
                    "PatProtJB.findCurrentProtDetails() PatProtStateKeeper "
                            + ppsk);
        } catch (Exception e) {
            Rlog.fatal("PatProt",
                    "Exception in findCurrentProtDetails() in PatProtJB " + e);
        }
        if (ppsk != null) {
            setJBFromStateKeeper(ppsk);
        }
        return ppsk;
    }

    
    /** Returns main Schedule information for a patient on a study
	   */
    public ScheduleDao getAvailableSchedules(String patientId, String studyId)
    {
    	ScheduleDao  sd = new ScheduleDao();
    	
    	try
    	{
    		PatProtAgentRObj patProtAgentRObj = EJBUtil.getPatProtAgentHome();
    		
    		sd = patProtAgentRObj.getAvailableSchedules( patientId, studyId);
    		
    		return sd;
    	}
    	catch (Exception e) {
            Rlog.fatal("patProt",  "EXCEPTION IN PatProtJB:getAvailableSchedules(String patientId, String studyId)"      + e.getMessage());
            e.printStackTrace();

            return sd;
        }
    	
    }
  //KV:BugNo:5111

    
    public int deleteSchedules(String[] deleteId, int userId) {
        int ret = 0;
        try {
            Rlog.debug("patProt", "PatProtJB:deleteSchedules starting");

            PatProtAgentRObj patProtAgentRObj = EJBUtil.getPatProtAgentHome();
            ret = patProtAgentRObj.deleteSchedules(deleteId,userId);

        } catch (Exception e) {
            Rlog.fatal("patProt", "PatProtJB:deleteSchedules error " + e);
            return -1;
        }
        return ret;

    }
    
    
    // deleteSchedules() Overloaded for INF-18183 ::: Raviesh
    public int deleteSchedules(String[] deleteId, int userId,Hashtable<String, String> args ) {
        int ret = 0;
        try {
            Rlog.debug("patProt", "PatProtJB:deleteSchedules starting");

            PatProtAgentRObj patProtAgentRObj = EJBUtil.getPatProtAgentHome();
            ret = patProtAgentRObj.deleteSchedules(deleteId,userId,args);

        } catch (Exception e) {
            Rlog.fatal("patProt", "PatProtJB:deleteSchedules error " + e);
            return -1;
        }
        return ret;

    }
    
}// end of class

