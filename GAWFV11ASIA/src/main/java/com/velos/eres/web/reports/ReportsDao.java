/*
 * Classname			ReportsDao
 * 
 * Version information 	1.0
 *
 * Date					05/12/2001
 * 
 * Copyright notice		Velos, Inc.
 *
 * Author 				Sajal
 */

package com.velos.eres.web.reports;

/* IMPORT STATEMENTS */

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import oracle.sql.BLOB;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * ReportsDao for fetching Reports data from the database for various reports
 * 
 * @author Sajal
 * @version : 1.0 05/12/2001
 */

public class ReportsDao extends CommonDAO implements java.io.Serializable {

    /*
     * CLASS METHODS
     * 
     * 
     * END OF CLASS METHODS
     */

    /*
     * Arraylist to save User's Last Names
     */
    private ArrayList userLastNames;

    /*
     * Arraylist to save User's First Names
     */
    private ArrayList userFirstNames;

    /*
     * Arraylist to save Organization Names
     */
    private ArrayList organizationNames;

    /*
     * Arraylist to save User Job Types
     */
    private ArrayList userJobTypes;

    /*
     * Arraylist to save Study Numbers
     */
    private ArrayList studyNumbers;

    /*
     * Arraylist to save Study Titles
     */
    private ArrayList studyTitles;

    /*
     * Arraylist to save Therapeautic Areas
     */
    private ArrayList tareas;

    /*
     * Arraylist to save User Team Roles
     */
    private ArrayList userTeamRoles;

    /*
     * Arraylist to save User's Work Experiences
     */
    private ArrayList userWrkExps;

    /*
     * Arraylist to save User's Phases Involved
     */
    private ArrayList userPhaseInvs;

    /*
     * Arraylist to save User's Speciality
     */
    private ArrayList userSpls;

    /*
     * Arraylist to save User's Address
     */
    private ArrayList userAdds;

    /*
     * Arraylist to save User's City
     */
    private ArrayList userCitys;

    /*
     * Arraylist to save User's Address State
     */
    private ArrayList userStates;

    /*
     * Arraylist to save User's Address Zip code
     */
    private ArrayList userZips;

    /*
     * Arraylist to save User's Country
     */
    private ArrayList userCountrys;

    /*
     * Arraylist to save User's Phone number
     */
    private ArrayList userPhones;

    /*
     * Arraylist to save User's Email
     */
    private ArrayList userEmails;

    /*
     * Arraylist to save Site's Address
     */
    private ArrayList siteAdds;

    /*
     * Arraylist to save Site's City
     */
    private ArrayList siteCitys;

    /*
     * Arraylist to save Site's Address State
     */
    private ArrayList siteStates;

    /*
     * Arraylist to save Site's Address Zip code
     */
    private ArrayList siteZips;

    /*
     * Arraylist to save Site's Country
     */
    private ArrayList siteCountrys;

    /*
     * Arraylist to save Site's Phone number
     */
    private ArrayList sitePhones;

    /*
     * Arraylist to save Site's Email
     */
    private ArrayList siteEmails;

    /*
     * Arraylist to save Study Authors
     */
    private ArrayList studyAuthors;

    /*
     * Arraylist to save Study Participating Centers
     */
    private ArrayList partCenters;

    /*
     * Arraylist to save Study Data Managers
     */
    private ArrayList studyDataMgrs;

    /*
     * Arraylist to save Study Status
     */
    private ArrayList studyStats;

    /*
     * Arraylist to save Study Status Dates
     */
    private ArrayList studyStatDates;

    /*
     * Arraylist to save Section Names
     */
    private ArrayList sectionNames;

    /*
     * Arraylist to save Right of External User ('V' - View or 'M' - Mofify)
     */
    private ArrayList viewMods;

    /*
     * Arraylist to save Section Contents
     */
    private ArrayList sectionContents;

    /*
     * Arraylist to save StudyIds
     */
    private ArrayList studyIds;

    private int cRows;

    /**
     * Defines a ReportsDao object with empty array lists
     */
    public ReportsDao() {
        userLastNames = new ArrayList();
        userFirstNames = new ArrayList();
        organizationNames = new ArrayList();
        userJobTypes = new ArrayList();
        studyNumbers = new ArrayList();
        studyTitles = new ArrayList();
        tareas = new ArrayList();
        userTeamRoles = new ArrayList();
        userWrkExps = new ArrayList();
        userPhaseInvs = new ArrayList();
        userSpls = new ArrayList();
        userAdds = new ArrayList();
        userCitys = new ArrayList();
        userStates = new ArrayList();
        userZips = new ArrayList();
        userCountrys = new ArrayList();
        userPhones = new ArrayList();
        userEmails = new ArrayList();
        siteAdds = new ArrayList();
        siteCitys = new ArrayList();
        siteStates = new ArrayList();
        siteZips = new ArrayList();
        siteCountrys = new ArrayList();
        sitePhones = new ArrayList();
        siteEmails = new ArrayList();
        studyAuthors = new ArrayList();
        partCenters = new ArrayList();
        studyDataMgrs = new ArrayList();
        studyStats = new ArrayList();
        studyStatDates = new ArrayList();
        sectionNames = new ArrayList();
        sectionContents = new ArrayList();
        viewMods = new ArrayList();
        studyIds = new ArrayList();
    }

    // Getter and Setter methods

    /**
     * Returns an Arraylist containing String objects with User Last Names
     * 
     * @return an Arraylist containing the String objects with User Last Names
     */
    public ArrayList getUserLastNames() {
        return this.userLastNames;
    }

    /**
     * Sets the Arraylist containing the User Last Names with the ArrayList
     * passed as the parameter
     * 
     * @param userLastNames
     *            An ArrayList with String objects having User Last Names
     */
    public void setUserLastNames(ArrayList userLastNames) {
        this.userLastNames = userLastNames;
    }

    /**
     * Returns an Arraylist containing String objects with User First Names
     * 
     * @return an Arraylist containing the String objects with User First Names
     */
    public ArrayList getUserFirstNames() {
        return this.userFirstNames;
    }

    /**
     * Sets the Arraylist containing the User First Names with the ArrayList
     * passed as the parameter
     * 
     * @param userFirstNames
     *            An ArrayList with String objects having User First Names
     */
    public void setUserFirstNames(ArrayList userFirstNames) {
        this.userFirstNames = userFirstNames;
    }

    /**
     * Returns an Arraylist containing String objects with Organization Names
     * 
     * @return an Arraylist containing the String objects with Organization
     *         Names
     */
    public ArrayList getOrganizationNames() {
        return this.organizationNames;
    }

    /**
     * Sets the Arraylist containing the Organization Names with the ArrayList
     * passed as the parameter
     * 
     * @param organizationNames
     *            An ArrayList with String objects having Organization Names
     */
    public void setOrganizationNames(ArrayList organizationNames) {
        this.organizationNames = organizationNames;
    }

    /**
     * Returns an Arraylist containing String objects with User Job Types
     * 
     * @return an Arraylist containing the String objects with User Job Types
     */
    public ArrayList getUserJobTypes() {
        return this.userJobTypes;
    }

    /**
     * Sets the Arraylist containing the User Job Types with the ArrayList
     * passed as the parameter
     * 
     * @param userJobTypes
     *            An ArrayList with String objects having User Job Types
     */
    public void setUserJobTypes(ArrayList userJobTypes) {
        this.userJobTypes = userJobTypes;
    }

    /**
     * Returns an Arraylist containing String objects with Study Numbers
     * 
     * @return an Arraylist containing the String objects with Study Numbers
     */
    public ArrayList getStudyNumbers() {
        return this.studyNumbers;
    }

    /**
     * Sets the Arraylist containing Study Numbers with the ArrayList passed as
     * the parameter
     * 
     * @param studyNumbers
     *            An ArrayList with String objects having Study Numbers
     */
    public void setStudyNumbers(ArrayList studyNumbers) {
        this.studyNumbers = studyNumbers;
    }

    /**
     * Returns an Arraylist containing String objects with Study Titles
     * 
     * @return an Arraylist containing the String objects with Study Titles
     */
    public ArrayList getStudyTitles() {
        return this.studyTitles;
    }

    /**
     * Sets the Arraylist containing Study Titles with the ArrayList passed as
     * the parameter
     * 
     * @param studyTitles
     *            An ArrayList with String objects having Study Titles
     */
    public void setStudyTitles(ArrayList studyTitles) {
        this.studyTitles = studyTitles;
    }

    /**
     * Returns an Arraylist containing String objects with Therapeutic Areas
     * 
     * @return an Arraylist containing the String objects with Therapeutic Areas
     */
    public ArrayList getTareas() {
        return this.tareas;
    }

    /**
     * Sets the Arraylist containing Therapeutic Areas with the ArrayList passed
     * as the parameter
     * 
     * @param tareas
     *            An ArrayList with String objects having Therapeutic Areas
     */
    public void setTareas(ArrayList tareas) {
        this.tareas = tareas;
    }

    /**
     * Returns an Arraylist containing String objects with User's Team Roles
     * 
     * @return an Arraylist containing the String objects with User's Team Roles
     */
    public ArrayList getUserTeamRoles() {
        return this.userTeamRoles;
    }

    /**
     * Sets the Arraylist containing User's Team Roles with the ArrayList passed
     * as the parameter
     * 
     * @param userTeamRoles
     *            An ArrayList with String objects having User's Team Roles
     */
    public void setUserTeamRoles(ArrayList userTeamRoles) {
        this.userTeamRoles = userTeamRoles;
    }

    /**
     * Returns an Arraylist containing String objects with User's Work
     * Experience
     * 
     * @return an Arraylist containing the String objects with User's Work
     *         Experience
     */
    public ArrayList getUserWrkExps() {
        return this.userWrkExps;
    }

    /**
     * Sets the Arraylist containing User's Work Experience with the ArrayList
     * passed as the parameter
     * 
     * @param userWrkExps
     *            An ArrayList with String objects having User's Team Roles
     */
    public void setUserWrkExps(ArrayList userWrkExps) {
        this.userWrkExps = userWrkExps;
    }

    /**
     * Returns an Arraylist containing String objects with User's Phase Involved
     * 
     * @return an Arraylist containing the String objects with User's Phase
     *         Involved
     */
    public ArrayList getUserPhaseInvs() {
        return this.userPhaseInvs;
    }

    /**
     * Sets the Arraylist containing User's Phase Involved with the ArrayList
     * passed as the parameter
     * 
     * @param userPhaseInvs
     *            An ArrayList with String objects having User's Phase Involved
     */
    public void setUserPhaseInvs(ArrayList userPhaseInvs) {
        this.userPhaseInvs = userPhaseInvs;
    }

    /**
     * Returns an Arraylist containing String objects with User's Specialities
     * 
     * @return an Arraylist containing the String objects with User's
     *         Specialities
     */
    public ArrayList getUserSpls() {
        return this.userSpls;
    }

    /**
     * Sets the Arraylist containing User's Specialities with the ArrayList
     * passed as the parameter
     * 
     * @param userSpls
     *            An ArrayList with String objects having User's Specialities
     */
    public void setUserSpls(ArrayList userSpls) {
        this.userSpls = userSpls;
    }

    /**
     * Returns an Arraylist containing String objects with User's Primary
     * Addresses
     * 
     * @return an Arraylist containing the String objects with User's Primary
     *         Addresses
     */
    public ArrayList getUserAdds() {
        return this.userAdds;
    }

    /**
     * Sets the Arraylist containing User's Primary Addresses with the ArrayList
     * passed as the parameter
     * 
     * @param userAdds
     *            An ArrayList with String objects having User's Primary
     *            Addresses
     */
    public void setUserAdds(ArrayList userAdds) {
        this.userAdds = userAdds;
    }

    /**
     * Returns an Arraylist containing String objects with User's Address Cities
     * 
     * @return an Arraylist containing the String objects with User's Address
     *         Cities
     */
    public ArrayList getUserCitys() {
        return this.userCitys;
    }

    /**
     * Sets the Arraylist containing User's Address Cities with the ArrayList
     * passed as the parameter
     * 
     * @param userCitys
     *            An ArrayList with String objects having User's Address Cities
     */
    public void setUserCitys(ArrayList userCitys) {
        this.userCitys = userCitys;
    }

    /**
     * Returns an Arraylist containing String objects with User's Address States
     * 
     * @return an Arraylist containing the String objects with User's Address
     *         States
     */
    public ArrayList getUserStates() {
        return this.userStates;
    }

    /**
     * Sets the Arraylist containing User's Address States with the ArrayList
     * passed as the parameter
     * 
     * @param userStates
     *            An ArrayList with String objects having User's Address States
     */
    public void setUserStates(ArrayList userStates) {
        this.userStates = userStates;
    }

    /**
     * Returns an Arraylist containing String objects with User's Address Zips
     * 
     * @return an Arraylist containing the String objects with User's Address
     *         Zips
     */
    public ArrayList getUserZips() {
        return this.userZips;
    }

    /**
     * Sets the Arraylist containing User's Address Zips with the ArrayList
     * passed as the parameter
     * 
     * @param userZips
     *            An ArrayList with String objects having User's Address Zips
     */
    public void setUserZips(ArrayList userZips) {
        this.userZips = userZips;
    }

    /**
     * Returns an Arraylist containing String objects with User's Address
     * Countries
     * 
     * @return an Arraylist containing the String objects with User's Address
     *         Countries
     */
    public ArrayList getUserCountrys() {
        return this.userCountrys;
    }

    /**
     * Sets the Arraylist containing User's Address Countries with the ArrayList
     * passed as the parameter
     * 
     * @param userCountrys
     *            An ArrayList with String objects having User's Address
     *            Countries
     */
    public void setUserCountrys(ArrayList userCountrys) {
        this.userCountrys = userCountrys;
    }

    /**
     * Returns an Arraylist containing String objects with User's Phone Numbers
     * 
     * @return an Arraylist containing the String objects with User's Phone
     *         Numbers
     */
    public ArrayList getUserPhones() {
        return this.userPhones;
    }

    /**
     * Sets the Arraylist containing User's Phone Numbers with the ArrayList
     * passed as the parameter
     * 
     * @param userPhones
     *            An ArrayList with String objects having User's Phone Numbers
     */
    public void setUserPhones(ArrayList userPhones) {
        this.userPhones = userPhones;
    }

    /**
     * Returns an Arraylist containing String objects with User's Emails
     * 
     * @return an Arraylist containing the String objects with User's Emails
     */
    public ArrayList getUserEmails() {
        return this.userEmails;
    }

    /**
     * Sets the Arraylist containing User's Emails with the ArrayList passed as
     * the parameter
     * 
     * @param userEmails
     *            An ArrayList with String objects having User's Emails
     */
    public void setUserEmails(ArrayList userEmails) {
        this.userEmails = userEmails;
    }

    /**
     * Returns an Arraylist containing String objects with Site's Primary
     * Address
     * 
     * @return an Arraylist containing the String objects with Site's Primary
     *         Address
     */
    public ArrayList getSiteAdds() {
        return this.siteAdds;
    }

    /**
     * Sets the Arraylist containing Site's Primary Address with the ArrayList
     * passed as the parameter
     * 
     * @param siteAdds
     *            An ArrayList with String objects having Site's Primary Address
     */
    public void setSiteAdds(ArrayList siteAdds) {
        this.siteAdds = siteAdds;
    }

    /**
     * Returns an Arraylist containing String objects with Site's Cities
     * 
     * @return an Arraylist containing the String objects with Site's Cities
     */
    public ArrayList getSiteCitys() {
        return this.siteCitys;
    }

    /**
     * Sets the Arraylist containing Site's Cities with the ArrayList passed as
     * the parameter
     * 
     * @param siteCitys
     *            An ArrayList with String objects having Site's Cities
     */
    public void setSiteCitys(ArrayList siteCitys) {
        this.siteCitys = siteCitys;
    }

    /**
     * Returns an Arraylist containing String objects with Site's States
     * 
     * @return an Arraylist containing the String objects with Site's States
     */
    public ArrayList getSiteStates() {
        return this.siteStates;
    }

    /**
     * Sets the Arraylist containing Site's States with the ArrayList passed as
     * the parameter
     * 
     * @param siteStates
     *            An ArrayList with String objects having Site's States
     */
    public void setSiteStates(ArrayList siteStates) {
        this.siteStates = siteStates;
    }

    /**
     * Returns an Arraylist containing String objects with Site's Zips
     * 
     * @return an Arraylist containing the String objects with Site's Zips
     */
    public ArrayList getSiteZips() {
        return this.siteZips;
    }

    /**
     * Sets the Arraylist containing Site's Zips with the ArrayList passed as
     * the parameter
     * 
     * @param siteZips
     *            An ArrayList with String objects having Site's Zips
     */
    public void setSiteZips(ArrayList siteZips) {
        this.siteZips = siteZips;
    }

    /**
     * Returns an Arraylist containing String objects with Site's Countries
     * 
     * @return an Arraylist containing the String objects with Site's Countries
     */
    public ArrayList getSiteCountrys() {
        return this.siteCountrys;
    }

    /**
     * Sets the Arraylist containing Site's Countries with the ArrayList passed
     * as the parameter
     * 
     * @param siteCountrys
     *            An ArrayList with String objects having Site's Countries
     */
    public void setSiteCountrys(ArrayList siteCountrys) {
        this.siteCountrys = siteCountrys;
    }

    /**
     * Returns an Arraylist containing String objects with Site's Phones
     * 
     * @return an Arraylist containing the String objects with Site's Phones
     */
    public ArrayList getSitePhones() {
        return this.sitePhones;
    }

    /**
     * Sets the Arraylist containing Site's Phones with the ArrayList passed as
     * the parameter
     * 
     * @param sitePhones
     *            An ArrayList with String objects having Site's Phones
     */
    public void setSitePhones(ArrayList sitePhones) {
        this.sitePhones = sitePhones;
    }

    /**
     * Returns an Arraylist containing String objects with Site's Emails
     * 
     * @return an Arraylist containing the String objects with Site's Emails
     */
    public ArrayList getSiteEmails() {
        return this.siteEmails;
    }

    /**
     * Sets the Arraylist containing Site's Emails with the ArrayList passed as
     * the parameter
     * 
     * @param siteEmails
     *            An ArrayList with String objects having Site's Emails
     */
    public void setSiteEmails(ArrayList siteEmails) {
        this.siteEmails = siteEmails;
    }

    /**
     * Returns an Arraylist containing String objects with Study Authors
     * 
     * @return an Arraylist containing the String objects with Study Authors
     */
    public ArrayList getStudyAuthors() {
        return this.studyAuthors;
    }

    /**
     * Sets the Arraylist containing Study Authors with the ArrayList passed as
     * the parameter
     * 
     * @param studyAuthors
     *            An ArrayList with String objects having Study Authors
     */
    public void setStudyAuthors(ArrayList studyAuthors) {
        this.studyAuthors = studyAuthors;
    }

    /**
     * Returns an Arraylist containing String objects with Study Participating
     * Centers
     * 
     * @return an Arraylist containing the String objects with Study
     *         Participating Centers
     */
    public ArrayList getPartCenters() {
        return this.partCenters;
    }

    /**
     * Sets the Arraylist containing Study Participating Centers with the
     * ArrayList passed as the parameter
     * 
     * @param partCenters
     *            An ArrayList with String objects having Study Participating
     *            Centers
     */
    public void setPartCenters(ArrayList partCenters) {
        this.partCenters = partCenters;
    }

    /**
     * Returns an Arraylist containing String objects with Study Data Managers
     * 
     * @return an Arraylist containing the String objects with Study Data
     *         Managers
     */
    public ArrayList getStudyDataMgrs() {
        return this.studyDataMgrs;
    }

    /**
     * Sets the Arraylist containing Study Data Managers with the ArrayList
     * passed as the parameter
     * 
     * @param studyDataMgrs
     *            An ArrayList with String objects having Study Data Managers
     */
    public void setStudyDataMgrs(ArrayList studyDataMgrs) {
        this.studyDataMgrs = studyDataMgrs;
    }

    /**
     * Returns an Arraylist containing String objects with Study Status
     * 
     * @return an Arraylist containing the String objects with Study Status
     */
    public ArrayList getStudyStats() {
        return this.studyStats;
    }

    /**
     * Sets the Arraylist containing Study Status with the ArrayList passed as
     * the parameter
     * 
     * @param studyStats
     *            An ArrayList with String objects having Study Status
     */
    public void setStudyStats(ArrayList studyStats) {
        this.studyStats = studyStats;
    }

    /**
     * Returns an Arraylist containing String objects with Study Status Dates
     * 
     * @return an Arraylist containing the String objects with Study Status
     *         Dates
     */
    public ArrayList getStudyStatDates() {
        return this.studyStatDates;
    }

    /**
     * Sets the Arraylist containing Study Status Dates with the ArrayList
     * passed as the parameter
     * 
     * @param studyStatDates
     *            An ArrayList with String objects having Study Status Dates
     */
    public void setStudyStatDates(ArrayList studyStatDates) {
        this.studyStatDates = studyStatDates;
    }

    /**
     * Returns an Arraylist containing String objects with Section Names
     * 
     * @return an Arraylist containing the String objects with Section Names
     */
    public ArrayList getSectionNames() {
        return this.sectionNames;
    }

    /**
     * Sets the Arraylist containing Section Names with the ArrayList passed as
     * the parameter
     * 
     * @param sectionNames
     *            An ArrayList with String objects having Section Names
     */
    public void setSectionNames(ArrayList sectionNames) {
        this.sectionNames = sectionNames;
    }

    /**
     * Returns an Arraylist containing String objects with Section Contents
     * 
     * @return an Arraylist containing the String objects with Section Contents
     */
    public ArrayList getSectionContents() {
        return this.sectionContents;
    }

    /**
     * Sets the Arraylist containing Section Contents with the ArrayList passed
     * as the parameter
     * 
     * @param sectionContents
     *            An ArrayList with String objects having Section Contents
     */
    public void setSectionContents(ArrayList sectionContents) {
        this.sectionContents = sectionContents;
    }

    /**
     * Returns an Arraylist containing String objects with Rights for External
     * Users
     * 
     * @return an Arraylist containing the String objects with Rights for
     *         External Users
     */
    public ArrayList getViewMods() {
        return this.viewMods;
    }

    /**
     * Sets the Arraylist containing Rights for External Users with the
     * ArrayList passed as the parameter
     * 
     * @param viewMods
     *            An ArrayList with String objects having Rights for External
     *            Users
     */
    public void setViewMods(ArrayList viewMods) {
        this.viewMods = viewMods;
    }

    /**
     * Returns an Arraylist containing String objects with Study Ids
     * 
     * @return an Arraylist containing the String objects with Study Ids
     */
    public ArrayList getStudyIds() {
        return this.studyIds;
    }

    /**
     * Sets the Arraylist containing Study Ids with the ArrayList passed as the
     * parameter
     * 
     * @param studyIds
     *            An ArrayList with String objects having Study Ids
     */
    public void setStudyIds(ArrayList studyIds) {
        this.studyIds = studyIds;
    }

    /**
     * Returns the number of elements in the ArrayLists
     * 
     * @return the number of elements in the ArrayLists
     */
    public int getCRows() {
        return this.cRows;
    }

    /**
     * Sets the number of elements in the ArrayLists
     * 
     * @param cRows
     *            the number of elements in the ArrayLists
     */
    public void setCRows(int cRows) {
        this.cRows = cRows;
    }

    /**
     * Adds the object to the Arraylist containing User Last Names
     * 
     * @param userLastName
     *            A String object that needs to be added to the Arraylist
     *            containing User Last Names
     */
    public void setUserLastNames(String userLastName) {
        userLastNames.add(userLastName);
    }

    /**
     * Adds the object to the Arraylist containing User First Names
     * 
     * @param userFirstName
     *            A String object that needs to be added to the Arraylist
     *            containing User First Names
     */
    public void setUserFirstNames(String userFirstName) {
        userFirstNames.add(userFirstName);
    }

    /**
     * Adds the object to the Arraylist containing Organization Names
     * 
     * @param organizationName
     *            A String object that needs to be added to the Arraylist
     *            containing Organization Names
     */
    public void setOrganizationNames(String organizationName) {
        organizationNames.add(organizationName);
    }

    /**
     * Adds the object to the Arraylist containing User Job Types
     * 
     * @param userJobType
     *            A String object that needs to be added to the Arraylist
     *            containing User Job Types
     */
    public void setUserJobTypes(String userJobType) {
        userJobTypes.add(userJobType);
    }

    /**
     * Adds the object to the Arraylist containing Study Numbers
     * 
     * @param studyNumber
     *            A String object that needs to be added to the Arraylist
     *            containing Study Numbers
     */
    public void setStudyNumbers(String studyNumber) {
        studyNumbers.add(studyNumber);
    }

    /**
     * Adds the object to the Arraylist containing Study Titles
     * 
     * @param studyTitle
     *            A String object that needs to be added to the Arraylist
     *            containing Study Titles
     */
    public void setStudyTitles(String studyTitle) {
        studyTitles.add(studyTitle);
    }

    /**
     * Adds the object to the Arraylist containing Therapeutic Areas
     * 
     * @param tarea
     *            A String object that needs to be added to the Arraylist
     *            containing Therapeutic Areas
     */
    public void setTareas(String tarea) {
        tareas.add(tarea);
    }

    /**
     * Adds the object to the Arraylist containing User Team Roles
     * 
     * @param userTeamRole
     *            A String object that needs to be added to the Arraylist
     *            containing User Team Roles
     */
    public void setUserTeamRoles(String userTeamRole) {
        userTeamRoles.add(userTeamRole);
    }

    /**
     * Adds the object to the Arraylist containing User's Work Experience
     * 
     * @param userWrkExp
     *            A String object that needs to be added to the Arraylist
     *            containing User's Work Experience
     */
    public void setUserWrkExps(String userWrkExp) {
        userWrkExps.add(userWrkExp);
    }

    /**
     * Adds the object to the Arraylist containing User's Phase Involved
     * 
     * @param userPhaseInv
     *            A String object that needs to be added to the Arraylist
     *            containing User's Phase Involved
     */
    public void setUserPhaseInvs(String userPhaseInv) {
        userPhaseInvs.add(userPhaseInv);
    }

    /**
     * Adds the object to the Arraylist containing User's Specialities
     * 
     * @param userSpl
     *            A String object that needs to be added to the Arraylist
     *            containing User's Specialities
     */
    public void setUserSpls(String userSpl) {
        userSpls.add(userSpl);
    }

    /**
     * Adds the object to the Arraylist containing User's Primary Addresses
     * 
     * @param userAdd
     *            A String object that needs to be added to the Arraylist
     *            containing User's Primary Addresses
     */
    public void setUserAdds(String userAdd) {
        userAdds.add(userAdd);
    }

    /**
     * Adds the object to the Arraylist containing User's Address Cities
     * 
     * @param userCity
     *            A String object that needs to be added to the Arraylist
     *            containing User's Address Cities
     */
    public void setUserCitys(String userCity) {
        userCitys.add(userCity);
    }

    /**
     * Adds the object to the Arraylist containing User's Address States
     * 
     * @param userStates
     *            A String object that needs to be added to the Arraylist
     *            containing User's Address States
     */
    public void setUserStates(String userState) {
        userStates.add(userState);
    }

    /**
     * Adds the object to the Arraylist containing User's Address Zips
     * 
     * @param userZip
     *            A String object that needs to be added to the Arraylist
     *            containing User's Address Zips
     */
    public void setUserZips(String userZip) {
        userZips.add(userZip);
    }

    /**
     * Adds the object to the Arraylist containing User's Address Countries
     * 
     * @param userCountry
     *            A String object that needs to be added to the Arraylist
     *            containing User's Address Countries
     */
    public void setUserCountrys(String userCountry) {
        userCountrys.add(userCountry);
    }

    /**
     * Adds the object to the Arraylist containing User's Phone Numbers
     * 
     * @param userPhone
     *            A String object that needs to be added to the Arraylist
     *            containing User's Phone Numbers
     */
    public void setUserPhones(String userPhone) {
        userPhones.add(userPhone);
    }

    /**
     * Adds the object to the Arraylist containing User's Emails
     * 
     * @param userEmail
     *            A String object that needs to be added to the Arraylist
     *            containing User's Emails
     */
    public void setUserEmails(String userEmail) {
        userEmails.add(userEmail);
    }

    /**
     * Adds the object to the Arraylist containing Site's Primary Address
     * 
     * @param siteAdd
     *            A String object that needs to be added to the Arraylist
     *            containing Site's Primary Address
     */
    public void setSiteAdds(String siteAdd) {
        siteAdds.add(siteAdd);
    }

    /**
     * Adds the object to the Arraylist containing Site's Cities
     * 
     * @param siteCity
     *            A String object that needs to be added to the Arraylist
     *            containing Site's Cities
     */
    public void setSiteCitys(String siteCity) {
        siteCitys.add(siteCity);
    }

    /**
     * Adds the object to the Arraylist containing Site's States
     * 
     * @param siteState
     *            A String object that needs to be added to the Arraylist
     *            containing Site's States
     */
    public void setSiteStates(String siteState) {
        siteStates.add(siteState);
    }

    /**
     * Adds the object to the Arraylist containing Site's Zips
     * 
     * @param siteZip
     *            A String object that needs to be added to the Arraylist
     *            containing Site's Zips
     */
    public void setSiteZips(String siteZip) {
        siteZips.add(siteZip);
    }

    /**
     * Adds the object to the Arraylist containing Site's Countries
     * 
     * @param siteCountry
     *            A String object that needs to be added to the Arraylist
     *            containing Site's Countries
     */
    public void setSiteCountrys(String siteCountry) {
        siteCountrys.add(siteCountry);
    }

    /**
     * Adds the object to the Arraylist containing Site's Phones
     * 
     * @param sitePhone
     *            A String object that needs to be added to the Arraylist
     *            containing Site's Phones
     */
    public void setSitePhones(String sitePhone) {
        sitePhones.add(sitePhone);
    }

    /**
     * Adds the object to the Arraylist containing Site's Emails
     * 
     * @param siteEmail
     *            A String object that needs to be added to the Arraylist
     *            containing Site's Emails
     */
    public void setSiteEmails(String siteEmail) {
        siteEmails.add(siteEmail);
    }

    /**
     * Adds the object to the Arraylist containing Study Authors
     * 
     * @param studyAuthor
     *            A String object that needs to be added to the Arraylist
     *            containing Study Authors
     */
    public void setStudyAuthors(String studyAuthor) {
        studyAuthors.add(studyAuthor);
    }

    /**
     * Adds the object to the Arraylist containing Study Participating Centers
     * 
     * @param partCenter
     *            A String object that needs to be added to the Arraylist
     *            containing Study Participating Centers
     */
    public void setPartCenters(String partCenter) {
        partCenters.add(partCenter);
    }

    /**
     * Adds the object to the Arraylist containing Study Data Managers
     * 
     * @param studyDataMgr
     *            A String object that needs to be added to the Arraylist
     *            containing Study Data Managers
     */
    public void setStudyDataMgrs(String studyDataMgr) {
        studyDataMgrs.add(studyDataMgr);
    }

    /**
     * Adds the object to the Arraylist containing Study Status
     * 
     * @param studyStat
     *            A String object that needs to be added to the Arraylist
     *            containing Study Status
     */
    public void setStudyStats(String studyStat) {
        studyStats.add(studyStat);
    }

    /**
     * Adds the object to the Arraylist containing Study Status Dates
     * 
     * @param studyStatDate
     *            A String object that needs to be added to the Arraylist
     *            containing Study Status Dates
     */
    public void setStudyStatDates(String studyStatDate) {
        studyStatDates.add(studyStatDate);
    }

    /**
     * Adds the object to the Arraylist containing Section Names
     * 
     * @param sectionName
     *            A String object that needs to be added to the Arraylist
     *            containing Section Names
     */
    public void setSectionNames(String sectionName) {
        sectionNames.add(sectionName);
    }

    /**
     * Adds the object to the Arraylist containing Study Ids
     * 
     * @param studyId
     *            A String object that needs to be added to the Arraylist
     *            containing Study Ids
     */
    public void setStudyIds(Integer studyId) {
        studyIds.add(studyId);
    }

    /**
     * Adds the object to the Arraylist containing Section Contents
     * 
     * @param sectionContent
     *            A String object that needs to be added to the Arraylist
     *            containing Section Contents
     */
    public void setSectionContents(BLOB contBlob) {
        BufferedReader bfRdr = null;
        String sectionContent = null;
        try {
            InputStream inbyteStream = contBlob.getBinaryStream();
            bfRdr = new BufferedReader(new InputStreamReader(inbyteStream));
            sectionContent = bfRdr.readLine();
        } catch (Exception e) {
            Rlog.debug("reports", "in DAO: Reports" + e);
        }
        sectionContents.add(sectionContent);
    }

    /**
     * Adds the object to the Arraylist containing Rights for External Users
     * 
     * @param viewMod
     *            A String object that needs to be added to the Arraylist
     *            containing Rights for External Users
     */
    public void setViewMods(String viewMod) {
        viewMods.add(viewMod);
    }

    // end of getter and setter methods

    /**
     * Fetches the data for the reports
     * 
     * @param
     * 
     */
    public void fetchReportsData(int reportNumber, String filterType,
            String year, String month, String dateFrom, String dateTo,
            String id, String accId) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        ResultSet rs = null;
        String sql = null;
        try {
            int rows = 0;
            conn = getConnection();
            switch (reportNumber) {
            case 1:
                pstmt = conn
                        .prepareStatement("select 	USR_LASTNAME, "
                                + "USR_FIRSTNAME, "
                                + "add_phone, "
                                + "add_email, "
                                + "(	select 	site_name "
                                + "		from 	er_site "
                                + "		where 	pk_site = FK_SITEID) as organization, "
                                + "(	select	codelst_desc "
                                + "		from 	er_codelst "
                                + "		where	pk_codelst = FK_CODELST_JOBTYPE) as jobType "
                                + "from 	er_user, " + "er_add "
                                + "where 	fk_account = ? "
                                + "and 	usr_stat = 'A' "
                                + "and 	fk_peradd = pk_add "
                                + "order by USR_LASTNAME, USR_FIRSTNAME ");
                pstmt.setInt(1, EJBUtil.stringToNum(accId));

                rs = pstmt.executeQuery();
                Rlog.debug("reports", "ReportsDao.fetchReportsData  ");
                while (rs.next()) {
                    setUserLastNames(rs.getString("USR_LASTNAME"));
                    setUserFirstNames(rs.getString("USR_FIRSTNAME"));
                    setUserPhones(rs.getString("add_phone"));
                    setUserEmails(rs.getString("add_email"));
                    setOrganizationNames(rs.getString("organization"));
                    setUserJobTypes(rs.getString("jobType"));
                    rows++;
                }
                break;
            case 2:
                pstmt = conn.prepareStatement("select 	USR_LASTNAME, "
                        + "USR_FIRSTNAME, " + "USR_WRKEXP, " + "USR_PAHSEINV, "
                        + "SPL, " + "JOB_TYPE, " + "SITE_NAME, "
                        + "USER_ADDRESS, " + "USER_CITY, " + "USER_STATE, "
                        + "USER_ZIP, " + "USER_COUNTRY, " + "USER_PHONE, "
                        + "USER_EMAIL, " + "ORGANIZATION_ADDRESS, "
                        + "ORGANIZATION_CITY, " + "ORGANIZATION_STATE, "
                        + "ORGANIZATION_ZIP, " + "ORGANIZATION_COUNTRY, "
                        + "ORGANIZATION_PHONE, " + "ORGANIZATION_EMAIL "
                        + "from ERV_USRPROFILE " + "where PK_USER = ?");
                pstmt.setInt(1, EJBUtil.stringToNum(id));

                rs = pstmt.executeQuery();
                Rlog.debug("reports", "ReportsDao.fetchReportsData  ");
                rs.next();
                setUserLastNames(rs.getString("USR_LASTNAME"));
                setUserFirstNames(rs.getString("USR_FIRSTNAME"));
                setUserWrkExps(rs.getString("USR_WRKEXP"));
                setUserPhaseInvs(rs.getString("USR_PAHSEINV"));
                setUserSpls(rs.getString("SPL"));
                setUserJobTypes(rs.getString("JOB_TYPE"));
                setOrganizationNames(rs.getString("SITE_NAME"));
                setUserAdds(rs.getString("USER_ADDRESS"));
                setUserCitys(rs.getString("USER_CITY"));
                setUserStates(rs.getString("USER_STATE"));
                setUserZips(rs.getString("USER_ZIP"));
                setUserCountrys(rs.getString("USER_COUNTRY"));
                setUserPhones(rs.getString("USER_PHONE"));
                setUserEmails(rs.getString("USER_EMAIL"));
                setSiteAdds(rs.getString("ORGANIZATION_ADDRESS"));
                setSiteCitys(rs.getString("ORGANIZATION_CITY"));
                setSiteStates(rs.getString("ORGANIZATION_STATE"));
                setSiteZips(rs.getString("ORGANIZATION_ZIP"));
                setSiteCountrys(rs.getString("ORGANIZATION_COUNTRY"));
                setSitePhones(rs.getString("ORGANIZATION_PHONE"));
                setSiteEmails(rs.getString("ORGANIZATION_EMAIL"));
                rows++;
                break;
            case 3:
                sql = "select STUDY_NUMBER, " + "STUDY_TITLE, " + "TA, "
                        + "DATA_MGR " + "from erv_studyact "
                        + "where fk_account = ? ";
                switch (EJBUtil.stringToNum(filterType)) {
                case 1:
                    sql += "order by TA";
                    break;
                case 2:
                    sql += " and to_Char(STUDYSTAT_DATE,'yyyy') = " + year
                            + " order by TA";
                    break;
                case 3:
                    sql += " and to_Char(STUDYSTAT_DATE,'yyyy') = " + year
                            + " and to_Char(STUDYSTAT_DATE,'mm') = " + month
                            + " order by TA";
                    break;
                case 4:
                    sql += " and STUDYSTAT_DATE > to_date('" + dateFrom
                            + "','mm/dd/yyyy')"
                            + " and STUDYSTAT_DATE < to_date('" + dateTo
                            + "','mm/dd/yyyy')" + " order by TA";
                    break;
                default:
                }
                Rlog.debug("reports",
                        "ReportsDao.fetchReportsData prepared sql " + sql);
                pstmt = conn.prepareStatement(sql);

                pstmt.setInt(1, EJBUtil.stringToNum(accId));

                rs = pstmt.executeQuery();
                Rlog.debug("reports", "ReportsDao.fetchReportsData  ");
                while (rs.next()) {
                    setStudyNumbers(rs.getString("STUDY_NUMBER"));
                    setStudyTitles(rs.getString("STUDY_TITLE"));
                    setTareas(rs.getString("TA"));
                    setStudyDataMgrs(rs.getString("DATA_MGR"));
                    rows++;
                }
                break;
            case 4:
                pstmt = conn.prepareStatement("select STUDY_NUMBER, "
                        + "STUDY_TITLE, " + "TA, " + "STUDY_AUTHOR "
                        + "from erv_study " + "where fk_account = ? "
                        + "and STUDY_PUBFLAG = 'Y' " + "order by TA ");
                pstmt.setInt(1, EJBUtil.stringToNum(accId));

                rs = pstmt.executeQuery();
                Rlog.debug("reports", "ReportsDao.fetchReportsData  ");
                while (rs.next()) {
                    setStudyNumbers(rs.getString("STUDY_NUMBER"));
                    setStudyTitles(rs.getString("STUDY_TITLE"));
                    setTareas(rs.getString("TA"));
                    setStudyAuthors(rs.getString("STUDY_AUTHOR"));
                    rows++;
                }
                break;
            case 5:
                sql = "select	usr_lastname, "
                        + "usr_firstname, "
                        + "study_number, "
                        + "study_title, "
                        + "(select codelst_desc "
                        + "from 	er_codelst "
                        + "where 	pk_codelst = er_study.fk_codelst_tarea) as tarea, "
                        + "(select codelst_desc "
                        + "from 	er_codelst "
                        + "where 	pk_codelst = er_studyteam.FK_CODELST_TMROLE) as teamRole "
                        + "from	er_study, " + "er_studyteam, " + "	er_user "
                        + "where 	er_studyteam.fk_user = ? "
                        + "and 	er_study.pk_study = er_studyteam.fk_study "
                        + "and 	er_user.pk_user = ?";
                switch (EJBUtil.stringToNum(filterType)) {
                case 2:
                    sql += " and to_Char(er_studyteam.created_on,'yyyy') = "
                            + year;
                    break;
                case 3:
                    sql += " and to_Char(er_studyteam.created_on,'yyyy') = "
                            + year
                            + " and to_Char(er_studyteam.created_on,'mm') = "
                            + month;
                    break;
                case 4:
                    sql += " and er_studyteam.created_on > to_date('"
                            + dateFrom + "','mm/dd/yyyy')"
                            + " and er_studyteam.created_on < to_date('"
                            + dateTo + "','mm/dd/yyyy')";
                    break;
                default:
                }
                Rlog.debug("reports",
                        "ReportsDao.fetchReportsData prepared sql " + sql);
                pstmt = conn.prepareStatement(sql);

                pstmt.setInt(1, EJBUtil.stringToNum(id));
                pstmt.setInt(2, EJBUtil.stringToNum(id));

                rs = pstmt.executeQuery();
                Rlog.debug("reports", "ReportsDao.fetchReportsData  ");
                while (rs.next()) {
                    setUserLastNames(rs.getString("usr_lastname"));
                    Rlog.debug("reports",
                            "ReportsDao.fetchReportsData after usr_lastname "
                                    + getUserLastNames());
                    setUserFirstNames(rs.getString("usr_firstname"));
                    Rlog.debug("reports",
                            "ReportsDao.fetchReportsData  after usr_firstname "
                                    + getUserFirstNames());
                    setStudyNumbers(rs.getString("study_number"));
                    Rlog.debug("reports",
                            "ReportsDao.fetchReportsData after study_number "
                                    + getStudyNumbers());
                    setStudyTitles(rs.getString("study_title"));
                    Rlog.debug("reports",
                            "ReportsDao.fetchReportsData after study_title "
                                    + getStudyTitles());
                    setTareas(rs.getString("tarea"));
                    Rlog.debug("reports",
                            "ReportsDao.fetchReportsData after tarea "
                                    + getTareas());
                    setUserTeamRoles(rs.getString("teamRole"));
                    Rlog.debug("reports",
                            "ReportsDao.fetchReportsData after teamrole "
                                    + getUserTeamRoles());
                    rows++;
                }
                break;
            case 6:
                sql = "select STUDY_NUMBER, " + "STUDY_TITLE, " + "TA, "
                        + "AUTHOR_SITE, " + "STUDY_AUTHOR " + "from erv_study "
                        + "where fk_account = ? " + "and FK_CODELST_TAREA = ? ";
                switch (EJBUtil.stringToNum(filterType)) {
                case 1:
                    sql += "order by STUDY_TITLE";
                    break;
                case 2:
                    sql += " and to_Char(CREATED_ON,'yyyy') = " + year
                            + " order by STUDY_TITLE";
                    break;
                case 3:
                    sql += " and to_Char(CREATED_ON,'yyyy') = " + year
                            + " and to_Char(CREATED_ON,'mm') = " + month
                            + " order by STUDY_TITLE";
                    break;
                case 4:
                    sql += " and CREATED_ON > to_date('" + dateFrom
                            + "','mm/dd/yyyy')" + " and CREATED_ON < to_date('"
                            + dateTo + "','mm/dd/yyyy')"
                            + " order by STUDY_TITLE";
                    break;
                default:
                }
                Rlog.debug("reports",
                        "ReportsDao.fetchReportsData prepared sql " + sql);
                pstmt = conn.prepareStatement(sql);

                pstmt.setInt(1, EJBUtil.stringToNum(accId));
                pstmt.setInt(2, EJBUtil.stringToNum(id));

                rs = pstmt.executeQuery();
                Rlog.debug("reports", "ReportsDao.fetchReportsData  ");
                while (rs.next()) {
                    setStudyNumbers(rs.getString("STUDY_NUMBER"));
                    setStudyTitles(rs.getString("STUDY_TITLE"));
                    setTareas(rs.getString("TA"));
                    setPartCenters(rs.getString("AUTHOR_SITE"));
                    setStudyAuthors(rs.getString("STUDY_AUTHOR"));
                    rows++;
                }
                break;
            case 7:
                pstmt = conn
                        .prepareStatement("select c.USR_LASTNAME, "
                                + "c.USR_FIRSTNAME, "
                                + "er_study.pk_study, "
                                + "er_study.STUDY_TITLE, "
                                + "er_study.STUDY_NUMBER, "
                                + "a.CODELST_DESC tarea, "
                                + "b.CODELST_DESC team_role, "
                                + "e.codelst_desc study_stat "
                                + "from ER_STUDYTEAM, "
                                + "er_user c, "
                                + "er_study, "
                                + "er_codelst a, "
                                + "er_codelst b, "
                                + "er_codelst e, "
                                + "er_studystat "
                                + "where c.fk_siteid = ? "
                                + "and 	ER_STUDYTEAM.FK_USER = c.pk_user "
                                + "and 	er_study.pk_study = ER_STUDYTEAM.FK_STUDY "
                                + "and 	er_study.FK_CODELST_TAREA = a.pk_codelst "
                                + "and	ER_STUDYTEAM.FK_CODELST_TMROLE = b.pk_codelst "
                                + "and 	er_studystat.created_on = (select max(created_on) from er_studystat where fk_study = er_study.pk_study) "
                                + "and 	er_studystat.fk_study = er_study.pk_study "
                                + "and 	er_studystat.FK_CODELST_STUDYSTAT = e.pk_codelst "
                                + "order by tarea, " + "pk_study ");
                pstmt.setInt(1, EJBUtil.stringToNum(id));

                rs = pstmt.executeQuery();
                Rlog.debug("reports", "ReportsDao.fetchReportsData  ");
                while (rs.next()) {
                    setUserLastNames(rs.getString("USR_LASTNAME"));
                    setUserFirstNames(rs.getString("USR_FIRSTNAME"));
                    setStudyIds(new Integer(rs.getInt("pk_study")));
                    setStudyTitles(rs.getString("STUDY_TITLE"));
                    setStudyNumbers(rs.getString("STUDY_NUMBER"));
                    setTareas(rs.getString("tarea"));
                    setUserTeamRoles(rs.getString("team_role"));
                    setStudyStats(rs.getString("study_stat"));
                    rows++;
                }
                break;
            case 9:
                pstmt = conn.prepareStatement("select STUDYSEC_NAME, "
                        + "STUDYSEC_CONTENTS " + "from ER_STUDYSEC "
                        + "where FK_STUDY = ? " + "and STUDYSEC_PUBFLAG = 'Y'");
                pstmt.setInt(1, EJBUtil.stringToNum(id));

                rs = pstmt.executeQuery();
                Rlog.debug("reports", "ReportsDao.fetchReportsData  ");
                while (rs.next()) {
                    setSectionNames(rs.getString("STUDYSEC_NAME"));
                    setSectionContents((BLOB) rs.getObject("STUDYSEC_CONTENTS"));
                    rows++;
                }
                break;
            case 10:
                pstmt = conn
                        .prepareStatement("select 	er_user.USR_LASTNAME, "
                                + "er_user.USR_FIRSTNAME, "
                                + "er_codelst.codelst_desc team_role, "
                                + "er_add.ADD_PHONE, "
                                + "er_add.ADD_EMAIL, "
                                + "er_site.site_name "
                                + "from er_user, "
                                + "ER_STUDYTEAM, "
                                + "er_codelst, "
                                + "er_add, "
                                + "er_site "
                                + "where ER_STUDYTEAM.fk_study = ? "
                                + "and er_user.pk_user = ER_STUDYTEAM.fk_user "
                                + "and er_codelst.pk_codelst = ER_STUDYTEAM.FK_CODELST_TMROLE "
                                + "and 	er_add.pk_add = er_user.FK_PERADD "
                                + "and 	er_user.FK_SITEID = er_site.pk_site "
                                + "order by USR_LASTNAME, USR_FIRSTNAME");
                pstmt.setInt(1, EJBUtil.stringToNum(id));

                rs = pstmt.executeQuery();
                Rlog.debug("reports", "ReportsDao.fetchReportsData  ");
                while (rs.next()) {
                    setUserLastNames(rs.getString("USR_LASTNAME"));
                    setUserFirstNames(rs.getString("USR_FIRSTNAME"));
                    setUserTeamRoles(rs.getString("team_role"));
                    setUserPhones(rs.getString("ADD_PHONE"));
                    setUserEmails(rs.getString("ADD_EMAIL"));
                    setOrganizationNames(rs.getString("site_name"));
                    rows++;
                }
                break;
            case 11:
                pstmt = conn
                        .prepareStatement("select 	er_user.usr_lastname, "
                                + "er_user.usr_firstname, "
                                + "er_codelst.codelst_desc team_role, "
                                + "er_site.site_name, "
                                + "er_add.add_phone, "
                                + "er_add.add_email, "
                                + "'V' view_modify "
                                + "from er_studyteam, "
                                + "er_user, "
                                + "er_study, "
                                + "er_codelst, "
                                + "er_site, "
                                + "er_add "
                                + "where er_studyteam.fk_study = ? "
                                + "and	er_user.pk_user = er_studyteam.fk_user "
                                + "and er_study.pk_study = ? "
                                + "and er_study.fk_account <> er_user.fk_account "
                                + "and er_codelst.pk_codelst = er_studyteam.fk_codelst_tmrole "
                                + "and er_user.fk_siteid = er_site.pk_site "
                                + "and er_add.pk_add = er_user.fk_peradd "
                                + "and 	er_studyteam.STUDY_TEAM_RIGHTS like '4%' "
                                + "union all "
                                + "select er_user.usr_lastname, "
                                + "er_user.usr_firstname, "
                                + "'-' team_role, "
                                + "er_site.site_name, "
                                + "er_add.add_phone, "
                                + "er_add.add_email, "
                                + "'M' view_modify "
                                + "from er_user, "
                                + "er_study, "
                                + "er_site, "
                                + "er_add "
                                + "where er_study.study_parentid = ? "
                                + "and er_study.fk_author = er_user.pk_user "
                                + "and er_user.fk_siteid = er_site.pk_site "
                                + "and er_add.pk_add = er_user.fk_peradd "
                                + "order by view_modify, usr_lastname, usr_firstname");
                pstmt.setInt(1, EJBUtil.stringToNum(id));
                pstmt.setInt(2, EJBUtil.stringToNum(id));
                pstmt.setInt(3, EJBUtil.stringToNum(id));

                rs = pstmt.executeQuery();
                Rlog.debug("reports", "ReportsDao.fetchReportsData  ");
                while (rs.next()) {
                    setUserLastNames(rs.getString("usr_lastname"));
                    setUserFirstNames(rs.getString("usr_firstname"));
                    setUserTeamRoles(rs.getString("team_role"));
                    setOrganizationNames(rs.getString("site_name"));
                    setUserPhones(rs.getString("add_phone"));
                    setUserEmails(rs.getString("add_email"));
                    setViewMods(rs.getString("view_modify"));

                    rows++;
                }
                break;
            case 12:
                pstmt = conn
                        .prepareStatement("select  codelst_desc study_stat, "
                                + "to_char(STUDYSTAT_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) stat_date "
                                + "from er_codelst, " + "er_studystat "
                                + "where FK_STUDY = ? "
                                + "and pk_codelst = FK_CODELST_STUDYSTAT "
                                + "and CODELST_TYPE='studystat' "
                                + "order by STUDYSTAT_DATE desc");
                pstmt.setInt(1, EJBUtil.stringToNum(id));

                rs = pstmt.executeQuery();
                Rlog.debug("reports", "ReportsDao.fetchReportsData  ");
                while (rs.next()) {
                    setStudyStats(rs.getString("study_stat"));
                    setStudyStatDates(rs.getString("stat_date"));
                    rows++;
                }
                break;
            default:
            }
            Rlog.debug("reports", "ReportsDao.fetchReportsData number of rows:"
                    + rows);
            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("reports",
                    "ReportsDao.fetchReportsData EXCEPTION IN FETCHING " + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }

}