/*
 * Classname : SectionJB.java
 * 
 * Version information
 *
 * Date 02/22/2001
 * 
 * Copyright notice: Aithent
 */

package com.velos.eres.web.section;

import java.util.ArrayList;
import java.util.Hashtable;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.audit.web.AuditRowEresJB;
import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.business.common.SectionDao;
import com.velos.eres.business.section.impl.SectionBean;
import com.velos.eres.service.sectionAgent.SectionAgent;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/**
 * Client side bean for the Study Sections
 * 
 * @author Sonia Sahni
 */

public class SectionJB {

    /**
     * the Section Id
     */
    private int id;

    /**
     * the Study to which the secton belongs
     */
    private String secStudy;

    /**
     * the name of the section
     */
    private String secName;

    /**
     * the number of the section
     */
    private String secNum;

    /**
     * the Section contents
     */

    private String contents;

    /**
     * the Section Public Flag
     */

    private String secPubFlag;

    /**
     * the Section Sequence
     */
    private String secSequence = "";

    /**
     * the Study Version to which the section belongs
     */
    private String secStudyVer;

    /*
     * creator
     */
    private String creator;

    /*
     * last modified by
     */
    private String modifiedBy;

    /*
     * IP Address
     */
    private String ipAdd;

    // GETTER SETTER METHODS

    public String getContents() {
        return this.contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSecName() {
        return this.secName;
    }

    public void setSecName(String secName) {
        this.secName = secName;
    }

    public String getSecNum() {
        return this.secNum;
    }

    public void setSecNum(String secNum) {
        this.secNum = secNum;
    }

    public String getSecPubFlag() {
        return this.secPubFlag;
    }

    public void setSecPubFlag(String secPubFlag) {
        this.secPubFlag = secPubFlag;
    }

    public String getSecSequence() {
        return this.secSequence;
    }

    public void setSecSequence(String secSequence) {
        this.secSequence = secSequence;
    }

    public String getSecStudy() {
        return this.secStudy;
    }

    public void setSecStudy(String secStudy) {
        this.secStudy = secStudy;
    }

    public String getSecStudyVer() {
        return this.secStudyVer;
    }

    public void setSecStudyVer(String secStudyVer) {
        this.secStudyVer = secStudyVer;
    }

    /**
     * @return Creator
     */
    public String getCreator() {
        return this.creator;
    }

    /**
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return Last Modified By
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param modifiedBy
     *            The value that is required to be registered as record last
     *            modified by
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return IP Address
     */
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // END OF GETTER SETTER METHODS

    // CONSTRUCTOR TO CREATE SECTION OBJ WITH SEC ID
    public SectionJB(String id) {

        int secId = 0;
        secId = EJBUtil.stringToNum(id);
        setId(secId);
    }

    // DEFAULT CONSTRUCTOR
    public SectionJB() {

    }

    /**
     * 
     */
    public SectionBean getSectionDetails() {

        SectionBean ssk = null;

        try {

            SectionAgent sectionAgent = EJBUtil.getSectionAgentHome();
            ssk = sectionAgent.getSectionDetails(this.id);
        } catch (Exception e) {
            Rlog.fatal("section", "EXception in getSectionDetails" + e);

        }
        if (ssk != null) {

            this.id = ssk.getId();
            this.secStudy = ssk.getSecStudy();
            this.secName = ssk.getSecName();
            this.secNum = ssk.getSecNum();
            this.contents = ssk.getContents();
            this.secPubFlag = ssk.getSecPubFlag();
            this.secSequence = ssk.getSecSequence();
            this.secStudyVer = ssk.getSecStudyVer();
            this.creator = ssk.getCreator();
            this.modifiedBy = ssk.getModifiedBy();
            this.ipAdd = ssk.getIpAdd();
        }
        return ssk;
    }

    /**
     * calls setCustomer() on the ReservationAgent facade.
     */
    public int setSectionDetails() {

        int ret = 0;
        String studysec_text="";
        int rid=0;
        try {

            SectionAgent sectionAgent = EJBUtil.getSectionAgentHome();
            ret = sectionAgent.setSectionDetails(this
                    .createSectionStateKeeper());
            
            CommonDAO cdSec = new CommonDAO();
            
            Rlog.debug("section", "section saved:" + ret);
            Rlog.debug("section", "section this.getContents():" + this.getContents());
            studysec_text= this.getContents();
            if(!studysec_text.equals("")){
            cdSec.updateClob(this.getContents(),"er_studysec","studysec_text"," where pk_studysec = " + ret);
            AuditRowEresJB auditRowEresJB = new AuditRowEresJB();
        	AuditUtils audittrails = new AuditUtils();
        	
        	rid = auditRowEresJB.getRidForDeleteOperation(this.getId(), EJBUtil.stringToNum(this.getCreator()), "ER_STUDYSEC", "PK_STUDYSEC");
			audittrails.deleteRaidList("eres", rid, this.getCreator(), "U");
             }
            
            return ret;
        } catch (Exception e) {
            Rlog.fatal("section", "EXception in setSectionDetails" + e);
            e.printStackTrace();
            return -2;
        }
    }

    /**
     * calls setSectionDetails() on the SectionAgent facade.
     */
    public int updateSection() {

        int ret = 0;
        int rid=0;
         try {

            SectionAgent sectionAgent = EJBUtil.getSectionAgentHome();
            Rlog.debug("section", "In updateSection() in SectionJB line 3");
            ret = sectionAgent.updateSection(this.createSectionStateKeeper());
            
            CommonDAO cdSec = new CommonDAO();
            cdSec.updateClob(this.getContents(),"er_studysec","studysec_text"," where pk_studysec = " + this.getId());
            AuditRowEresJB auditRowEresJB = new AuditRowEresJB();
        	AuditUtils audittrails = new AuditUtils();
        	
        	rid = auditRowEresJB.getRidForDeleteOperation(this.getId(), EJBUtil.stringToNum(this.getModifiedBy()), "ER_STUDYSEC", "PK_STUDYSEC");
			audittrails.deleteRaidList("eres", rid, this.getModifiedBy(), "U");
           
            return ret;

        } catch (Exception e) {
            Rlog.fatal("section", "Error in updateSection() in SectionJB " + e);
            return -2;
        }
    }

    /*
     * added dinesh 04/07/01 this methods calls SectionDao to fetch details from
     * er_studysec flags values can be y,Y, n,N and a,A for flags yes no and all
     * respectively
     */

    public SectionDao getStudySection(int studyVer, String flag, String sectionName) {
        try {

            SectionAgent sectionAgent = EJBUtil.getSectionAgentHome();
            return sectionAgent.getStudySection(studyVer, flag, sectionName);
        } catch (Exception e) {
            Rlog.fatal("section", "Error in getSection() in SectionJB " + e);
        }
        return null;
    }

    /*
     * places bean attributes into a CustomerStateHolder.
     */
    public SectionBean createSectionStateKeeper() {

        return new SectionBean(id, secStudy, secName, secNum, contents,
                secPubFlag, secSequence, secStudyVer, creator, modifiedBy,
                ipAdd);
    }

    /**
     * calls updateSectionSequence() on the SectionAgent facade.
     */
    public int updateSectionSequence(ArrayList sectionIds,
            ArrayList sectionSequences) {

        int ret = 0;
        try {

            SectionAgent sectionAgent = EJBUtil.getSectionAgentHome();
            ret = sectionAgent.updateSectionSequence(sectionIds,
                    sectionSequences);
            return ret;

        } catch (Exception e) {
            Rlog.fatal("section",
                    "Error in updateSectionSequence in SectionJB " + e);
            return -2;
        }
    }

    /*
     * delete study section
     */
    public int studySectionDelete(int sectionId) {
        int output = 0;

        try {

            SectionAgent sectionAgent = EJBUtil.getSectionAgentHome();
            output = sectionAgent.studySectionDelete(sectionId);
        } catch (Exception e) {
            Rlog
                    .fatal("section",
                            "Exception in removing study section in studySectionDelete in SectionJB");
            return -1;
        }
        return output;
    }
    
 // Overloaded for INF-18183 ::: AGodara
    public int studySectionDelete(int sectionId,Hashtable<String, String> auditInfo) {
        
    	int output = 0;
        try {
            SectionAgent sectionAgent = EJBUtil.getSectionAgentHome();
            output = sectionAgent.studySectionDelete(sectionId,auditInfo);
        } catch (Exception e) {
            Rlog.fatal("section","Exception in removing study section in studySectionDelete in SectionJB studySectionDelete(int sectionId,Hashtable<String, String> auditInfo)");
            return -2;
        }
        return output;
    }

    /*
     * generates a String of XML for the customer details entry form.<br><br>
     * Note that it is planned to encapsulate this detail in another object.
     */
    public String toXML() {
        return null;
    }// end of method

}// end of class
