/*

 * Classname : GrpRightsJB

 * 

 * Version information 1.0

 *

 * Date 03/03/2001

 * 

 * Copyright notice: Velos Inc.

 */

package com.velos.eres.web.grpRights;

import java.util.ArrayList;

import com.velos.eres.business.common.UserSiteGroupDao;
import com.velos.eres.business.grpRights.impl.GrpRightsBean;
import com.velos.eres.service.grpRightsAgent.GrpRightsAgentRObj;
import com.velos.eres.service.grpRightsAgent.impl.GrpRightsAgentBean;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/**
 * 
 * Client side bean for the Group Rights
 * 
 * 
 * 
 * @author Deepali Sachdeva
 * 
 * @version 1.0 03/03/2001
 * 
 */

public class GrpRightsJB {

    /**
     * 
     * the Group Id
     * 
     */

    private int id;

    /**
     * 
     * Array List of Feature Sequence
     * 
     */

    private ArrayList grSeq;

    /**
     * 
     * the Array List of Feature Rights
     * 
     */

    private ArrayList ftrRights;

    /**
     * 
     * the Array List containing reference name of the feature
     * 
     */

    private ArrayList grValue;

    /**
     * 
     * the Array List containing user friendly name of the feature
     * 
     */

    private ArrayList grDesc;

    /*
     * 
     * creator
     * 
     */

    private String creator;

    /*
     * 
     * last modified by
     * 
     */

    private String modifiedBy;

    /*
     * 
     * IP Address
     * 
     */

    private String ipAdd;

    // GETTER SETTER METHODS

    /**
     * 
     * Returns Id of the Group
     * 
     * 
     * 
     * @return Id of Group
     * 
     */

    public int getId() {

        return this.id;

    }

    /**
     * 
     * 
     * 
     * 
     * 
     * @param id
     *            id of the group
     * 
     */

    public void setId(int id) {

        this.id = id;

    }

    /**
     * 
     * Returns the seq no of all the features in an ArrayList
     * 
     * 
     * 
     * @return an arraylist of the feature sequence nos.
     * 
     */

    public ArrayList getGrSeq() {

        return this.grSeq;

    }

    /**
     * 
     * Add/Append the sequence of the features in an arrayList
     * 
     * 
     * 
     * @param seq
     *            Sequence of the feature
     * 
     */

    public void setGrSeq(String seq) {

        grSeq.add(seq);

    }

    /**
     * 
     * Sets the sequence no of feature in an ArrayList
     * 
     * 
     * 
     * @param seq
     * 
     */

    public void setGrSeq(ArrayList seq) {

        this.grSeq = seq;

    }

    /**
     * 
     * returns the list of feature wise Rights of a group
     * 
     * 
     * 
     * @return List of rights of a group
     * 
     */

    public ArrayList getFtrRights() {

        return this.ftrRights;

    }

    /**
     * 
     * Returns the rights of the group for a particular feature sequence
     * 
     * 
     * 
     * @param seq
     *            sequence (feature) for which group rights are required
     * 
     * @return Rights assigned to the group on the feature
     * 
     */

    public String getFtrRightsBySeq(String seq) {

        // int count = Integer.parseInt(seq) - 1;

       // System.out.println("Getting Rights By Sequence"      + this.ftrRights.get(Integer.parseInt(seq) - 1));

        return this.ftrRights.get(Integer.parseInt(seq) - 1).toString();

    }

    /**
     * 
     * Each feature has an associated value. Returns the Group rights of the
     * feature
     * 
     * whose value is passed as an argument.
     * 
     * 
     * 
     * @param val
     *            Value associated with the feature
     * 
     * @return Rights assigned on the feature for the group
     * 
     */

    public String getFtrRightsByValue(String val) {

        int seq = grValue.indexOf(val);
        if(seq==-1)
        {
        System.out.println("USER DOES'NT HAVE ANY ACCESS RIGHTS");
        	return null;
        }
        else
        return this.ftrRights.get(seq).toString();

    }

    /**
     * 
     * Sets the Rights in a list according to feature sequence
     * 
     * 
     * 
     * @param rights
     *            Group rights for the feature
     * 
     */

    public void setFtrRights(String rights) {

        ftrRights.add(rights);

    }

    /**
     * 
     * Sets the list of rights in class attribute
     * 
     * 
     * 
     * @param rights
     *            List of group rights
     * 
     */

    public void setFtrRights(ArrayList rights) {

        this.ftrRights = rights;

    }

    /**
     * 
     * Returns the Associated value for each feature
     * 
     * 
     * 
     * @return the value associated with each feature
     * 
     */

    public ArrayList getGrValue() {

        return this.grValue;

    }

    /**
     * 
     * Returns the value of the required feature by sequence no
     * 
     * 
     * 
     * @param seq
     *            Sequence of the feature for which value is required
     * 
     * @return value of the feature
     * 
     */

    public String getGrValueBySeq(String seq) {

        int count = Integer.parseInt(seq) - 1;

        return this.grValue.get(count).toString();

    }

    /**
     * 
     * Sets the value of each feature in the arraylist
     * 
     * 
     * 
     * @param val
     *            value associated with each feature
     * 
     */

    public void setGrValue(String val) {

        grValue.add(val);

    }

    /**
     * 
     * Sets the List of Feature Values in an class attribute for Value
     * 
     * 
     * 
     * @param val
     *            arraylist of values to be set
     * 
     */

    public void setGrValue(ArrayList val) {

        this.grValue = val;

    }

    /**
     * 
     * Returns the list of description for each feature
     * 
     * 
     * 
     * @return Feature Description
     * 
     */

    public ArrayList getGrDesc() {

        return this.grDesc;

    }

    /**
     * 
     * Returns the Description of feature
     * 
     * 
     * 
     * @param seq
     *            Sequence of feature for which Descrpition is required
     * 
     * @return Feature description
     * 
     */

    public String getGrDescBySeq(String seq) {

        int count = Integer.parseInt(seq) - 1;

        return this.grDesc.get(count).toString();

    }

    /**
     * 
     * Returns the Description of the feature by value associated with each
     * feature
     * 
     * 
     * 
     * @param val
     *            Value asscciated with the feature
     * 
     * @return Feature Description
     * 
     */

    public String getGrDescByValue(String val) {

        int seq = grValue.indexOf(val);

        return this.grDesc.get(seq).toString();

    }

    /**
     * 
     * Sets the Descrption of the feature in the ArrayList
     * 
     * 
     * 
     * @param desc
     * 
     */

    public void setGrDesc(String desc) {

        grDesc.add(desc);

    }

    /**
     * 
     * Sets the list of Description for each feature
     * 
     * 
     * 
     * @param desc
     *            Descrption of all the features
     * 
     */

    public void setGrDesc(ArrayList desc) {

        this.grDesc = desc;

    }

    /**
     * 
     * @return Creator
     * 
     */

    public String getCreator() {

        return this.creator;

    }

    /**
     * 
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     * 
     */

    public void setCreator(String creator) {

        this.creator = creator;

    }

    /**
     * 
     * @return Last Modified By
     * 
     */

    public String getModifiedBy() {

        return this.modifiedBy;

    }

    /**
     * 
     * @param modifiedBy
     *            The value that is required to be registered as record last
     *            modified by
     * 
     */

    public void setModifiedBy(String modifiedBy) {

        this.modifiedBy = modifiedBy;

    }

    /**
     * 
     * @return IP Address
     * 
     */

    public String getIpAdd() {

        return this.ipAdd;

    }

    /**
     * 
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     * 
     */

    public void setIpAdd(String ipAdd) {

        this.ipAdd = ipAdd;

    }

    // END OF GETTER SETTER METHODS

    /**
     * 
     * Constructor which sets the Id of the group
     * 
     * 
     * 
     * @param grpId
     *            Id of the group
     * 
     */

    public GrpRightsJB(int grpId)

    {

        setId(grpId);

    }

    /**
     * 
     * Default Constructor
     * 
     * 
     * 
     */

    public GrpRightsJB() {

        grSeq = new ArrayList();

        ftrRights = new ArrayList();

        grValue = new ArrayList();

        grDesc = new ArrayList();

    }

    /**
     * 
     * Full arguments constructor.
     * 
     * 
     * 
     * @param id
     * 
     * @param grSeq
     * 
     * @param ftrRights
     * 
     * @param grValue
     * 
     * @param grDesc
     * 
     */

    public GrpRightsJB(int id, ArrayList grSeq, ArrayList ftrRights,
            ArrayList grValue, ArrayList grDesc, String creator,
            String modifiedBy, String ipAdd)

    {

        setId(id);
        setGrSeq(grSeq);
        setFtrRights(ftrRights);
        setGrValue(grValue);
        setGrDesc(grDesc);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);

    }

    /**
     * 
     * calls getGrpRightsDetails() on the GrpRightsAgent facade and extracts
     * this state holder into the been attribute fields.
     * 
     * 
     * 
     * @return State Keeper of group rights which holds the group rights
     * 
     * for all the features
     * 
     */

    public GrpRightsBean getGrpRightsDetails() {

        GrpRightsBean gsk = null;

        try

        {

            GrpRightsAgentRObj grpRightsAgentRObj = EJBUtil
                    .getGrpRightsAgentHome();

            gsk = grpRightsAgentRObj.getGrpRightsDetails(this.id);

        }

        catch (Exception e)

        {

            System.out.print("Test Error");

        }

        if (gsk != null)

        {

            this.id = gsk.getId();

            this.grSeq = gsk.getGrSeq();

            this.ftrRights = gsk.getFtrRights();

            this.grValue = gsk.getGrValue();

            this.grDesc = gsk.getGrDesc();

            this.creator = gsk.getCreator();

            this.modifiedBy = gsk.getModifiedBy();

            this.ipAdd = gsk.getIpAdd();

        }

        return gsk;

    }

    /**
     * 
     * calls setGrpRightsDetails() on the GrpRightsAgent facade.
     * 
     * 
     * 
     * @see GrpRightsAgentBean
     * 
     */

    public void setGrpRightsDetails() {

        try

        {

            GrpRightsAgentRObj grpRightsAgentRObj = EJBUtil
                    .getGrpRightsAgentHome();
            grpRightsAgentRObj.setGrpRightsDetails(this
                    .createGrpRightsStateKeeper());

        }

        catch (Exception e)

        {

            System.out.print("Test Error");

            e.printStackTrace();

        }

    }

    /**
     * 
     * calls updateGrpRights() on the GrpRightsAgent facade.
     * 
     * 
     * 
     * @see GrpRightsAgentBean
     * 
     */

    public void updateGrpRights() {

        try

        {

            Rlog.debug("grprights", "In try block of Set GRp Rights of JB ");

            GrpRightsAgentRObj grpRightsAgentRObj = EJBUtil
                    .getGrpRightsAgentHome();

            grpRightsAgentRObj.updateGrpRights(this
                    .createGrpRightsStateKeeper());

        }

        catch (Exception e)

        {

            Rlog.fatal("grprights",
                    "EXCEPTION IN In try block of Set GRp Rights of JB " + e);

        }

    }

    /**
     * 
     * places bean attributes into a Group Rights StateHolder.
     * 
     * 
     * 
     * @param groupId
     * 
     */

    public GrpRightsBean createGrpRightsStateKeeper()

    {
        return new GrpRightsBean(id, (new Integer(id)).toString(), this.grSeq,
                this.ftrRights, this.grValue, this.grDesc, creator, modifiedBy,
                ipAdd);

    }

    /**
     * Created by : Ankit Kumar
     * Created On : 18-Apr-2012 
     * Enhancement: Garuda Security Imp
     * Purpose    : Get maximum rights of Groups within Organization.
     * @return    : GrpRightsBean;
     */
    public GrpRightsBean getMaxGrpRights(long userId, long siteId) {

        GrpRightsBean gsk = null;
        try{
        	UserSiteGroupDao usrSiteGrp= new UserSiteGroupDao();
            gsk = usrSiteGrp.getMaxGrpRights(userId, siteId);
        }
        catch (Exception e)
        {
            System.out.print("Test Error");
        }
        if (gsk != null){
        
            this.id = gsk.getId();
            this.grSeq = gsk.getGrSeq();
            this.ftrRights = gsk.getFtrRights();
            this.grValue = gsk.getGrValue();
            this.grDesc = gsk.getGrDesc();
            this.creator = gsk.getCreator();
            this.modifiedBy = gsk.getModifiedBy();
            this.ipAdd = gsk.getIpAdd();
        }

        return gsk;
    }
  
    
}// end of class

