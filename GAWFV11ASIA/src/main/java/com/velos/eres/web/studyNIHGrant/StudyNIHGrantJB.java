/*
 * Classname: 				StudyNIHGrantJB
 *
 * Version information: 	1.0
 *
 * Date: 					11/22/2011
 *
 * Copyright notice: 		Velos Inc
 *
 * Author: 					Ashu Khatkar
 */


package com.velos.eres.web.studyNIHGrant;

import java.util.Hashtable;

import com.velos.eres.business.common.StudyNIHGrantDao;
import com.velos.eres.business.studyNIHGrant.impl.StudyNIHGrantBean;
import com.velos.eres.service.studyNIHGrantAgent.StudyNIHGrantAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

public class StudyNIHGrantJB {
 
 /**
  * The primary key:
  */
 private int studyNIHGrantId;
 
 /**
  * The foreign key reference to er_study
  */
 private String studyId;
 
 /**
  * The the Funding Mechanism Type
  */

 private String fundMech;
 
 /**
  * The the Institude Code
  */

 private String instCode;
 
 /**
  * the NCI PRogram Code
  */
	
 private String programCode;
 
 /**
  * the NIH grant Serial Number
  */
	
 private String   nihGrantSerial;
 
 /**
  * The creator
  */

 private String creator;

 /**
  * The last modification by: LAST_MODIFICATION_BY
  */

 private String modifiedBy;

 /**
  * The IP Address : IP_ADD
  */

 private String ipAdd;
 
 
 // /////////////////////////////////////////////////////////////////////////////////

 // GETTER AND SETTER METHODS
 /**
  * Get the study NIH grant ID
  *
  * @return int study NIH grant id
  */
 public int getStudyNIHGrantId() {
     return this.studyNIHGrantId;
 }

 /**
  * sets Study NIH Grant  ID
  *
  * @param studyNIHGrantId
  */
 public void setStudyNIHGrantId(int studyNIHGrantId) {
     this.studyNIHGrantId = studyNIHGrantId;
 }
 
 
 /**
  * Get study id
  *
  * @return String studyid
  */

 public String getStudyId() {
     return this.studyId;
 }

 /**
  * sets Study Id
  *
  * @param studyId
  */
 public void setStudyId(String studyId) {
     this.studyId = studyId;
 }
 
 /**
  * Get study Funding mechanism 
  *
  * @return String fundMech
  */

 public String getFundMech() {
     return this.fundMech;
 }

 /**
  * sets Funding mechanism
  *
  * @param fundMech
  */
 public void setFundMech(String fundMech) {
     this.fundMech = fundMech;
 }

 /**
  * Get Institude Code
  *
  * @return String instCode
  */

 public String getInstCode() {
     return this.instCode;
 }

 /**
  * sets Institude Code
  *
  * @param instCode
  */
 public void setInstCode(String instCode) {
     this.instCode = instCode;
 }

 /**
  * Get Program Code
  *
  * @return String programCode
  */

 public String getProgramCode() {
     return this.programCode;
 }

 /**
  * sets Program Code
  *
  * @param programCode
  */
 public void setProgramCode(String programCode) {
     this.programCode = programCode;
 }

 /**
  * Get NIH Grant Serial Number
  *
  * @return String nihGrantSerial
  */

 public String getNihGrantSerial() {
     return this.nihGrantSerial;
 }

 /**
  * sets NIH Grant Serial Number
  *
  * @param nihGrantSerial
  */
 public void setNihGrantSerial(String nihGrantSerial) {
     this.nihGrantSerial = nihGrantSerial;
 }
 
 /**
  * @return The Creator of the  Study NIH Grant
  */

 public String getCreator() {
     return this.creator;
 }

 /**
  *
  * @param to
  *            set the Creator of the Study NIH Grant
  */
 public void setCreator(String creator) {
     this.creator = creator;
 }

 public String getModifiedBy() {
     return this.modifiedBy;
 }

 /**
  * @param the
  *            set unique ID of the User
  */

 public void setModifiedBy(String modifiedBy) {
     this.modifiedBy = modifiedBy;
 }

 /**
  * @return the IP Address of the Study NIH Grant request
  */

 public String getIpAdd() {
     return this.ipAdd;
 }

 /**
  * @param set
  *            the IP Address of the Study NIH Grant request
  */

 public void setIpAdd(String ipAdd) {
     this.ipAdd = ipAdd;
 }
 
 /**
  * 
  * @param studyNIHGrantId
  */
 public StudyNIHGrantJB(int studyNIHGrantId) {
     setStudyNIHGrantId(studyNIHGrantId);
 }

 /**
  * Default Constructor
  */

 public StudyNIHGrantJB() {
     Rlog.debug("StudyNIhGrant", "StudyNIHGrantJB.StudyNIHGrantJB() ");
 }
 /**
  * 
  * @param studyNIHGrantId
  * @param studyId
  * @param fundMech
  * @param instCode
  * @param programCode
  * @param nihGrantSerial
  * @param creator
  * @param modifiedBy
  * @param ipAdd
  */

 public StudyNIHGrantJB(int studyNIHGrantId, String studyId, String fundMech,
         String instCode, String programCode, String nihGrantSerial,
         String creator, String modifiedBy, String ipAdd) {
     setStudyNIHGrantId(studyNIHGrantId);
     setStudyId(studyId);
     setFundMech(fundMech);
     setInstCode(instCode);
     setProgramCode(programCode);
     setNihGrantSerial(nihGrantSerial);
     setCreator(creator);
     setModifiedBy(modifiedBy);
     setIpAdd(ipAdd);
  }
 /**
  * 
  * @return StudyNIHGrantBean
  */
 
 public StudyNIHGrantBean getStudyNIHGrantInfo() {
	 StudyNIHGrantBean studyNIHGrantsk = null;
     try {
    	 
    	 StudyNIHGrantAgentRObj studyNIHGrantAgentRObj =EJBUtil.getStudyNIHGrantAgentHome();
    	 studyNIHGrantsk=studyNIHGrantAgentRObj.getStudyNIHGrantInfo(EJBUtil.stringToNum(this.studyId));
         Rlog.debug("studyNIHGrant",
                 "StudyNIHGrantJB.getStudyNIHGrantInfo() StudyNIHGrantStateKeeper "
                         + studyNIHGrantsk);
     } catch (Exception e) {
         Rlog.fatal("studyNIHGrant", "Error in Study  getStudyNIHGrantInfo"
                 + e);
     }

     if (studyNIHGrantsk != null) {

     	copyStudyNIHGrantBeanAttributes(studyNIHGrantsk);

     }
     return studyNIHGrantsk;

 }
 
 /**
  * 
  * @param studyNIHGrantsk
  */
 private void copyStudyNIHGrantBeanAttributes(StudyNIHGrantBean studyNIHGrantsk) {
	 if (studyNIHGrantsk != null) {
    	 setStudyNIHGrantId(studyNIHGrantsk.getStudyNIHGrantId());
         setStudyId(studyNIHGrantsk.getStudyId());
         setFundMech(studyNIHGrantsk.getFundMech());
         setInstCode(studyNIHGrantsk.getInstCode());
         setProgramCode(studyNIHGrantsk.getProgramCode());
         setNihGrantSerial(studyNIHGrantsk.getNihGrantSerial());
         setCreator(studyNIHGrantsk.getCreator());
         setModifiedBy(studyNIHGrantsk.getModifiedBy());
         setIpAdd(studyNIHGrantsk.getIpAdd());
     }
	
}
/**
 * 
 * @return StudyNIHGrantBean
 */
public StudyNIHGrantBean createStudyNIHGrantStateKeeper() {
     Rlog.debug("studyNIhGrant", "StudyNIHGrantJB.createStudyNIHGrantStateKeeper ");
     return new StudyNIHGrantBean(
     		this.studyNIHGrantId,
     		this.studyId,
     		this.fundMech,
     		this.instCode,
     		this.programCode,
     		this.nihGrantSerial,
     	    this.creator,
     	    this.modifiedBy,
     	    this.ipAdd);
 }
 
/**
 * 
 * @return int studyNIHGrantId
 */
 
 public int setStudyNIHGrantInfo() {
     int output;
     try {

    	 StudyNIHGrantAgentRObj studyNIHGrantAgentRObj =EJBUtil.getStudyNIHGrantAgentHome();
    	 StudyNIHGrantBean tempStateKeeper= new StudyNIHGrantBean();
    	 output=studyNIHGrantAgentRObj.setStudyNIHGrantInfo(this.createStudyNIHGrantStateKeeper());
         Rlog.debug("studyNihGrant", "StudyNIHGrantJB.setStudyNIHGrantInfo()");

         return output;

     } catch (Exception e) {
         Rlog.fatal("studyNihGrant",
                 "Error in setStudyNIHGrantInfo() in StudyNIHGrantJB " + e);
         return -2;
     }
 }
 /**
  * 
  * @param fkStudyId
  * @return
  */
 
 public StudyNIHGrantDao getStudyNIHGrantRecords(int fkStudyId) {
	 StudyNIHGrantDao studyNIHGrantDao = new StudyNIHGrantDao();
 	try {
 		 StudyNIHGrantAgentRObj studyNIHGrantAgentRObj =EJBUtil.getStudyNIHGrantAgentHome();
 		 studyNIHGrantDao= studyNIHGrantAgentRObj.getStudyNIHGrantRecords(fkStudyId);
         return studyNIHGrantDao;

     } catch (Exception e) {
         Rlog.fatal("studyNIhGrant", "Error in getStudyNIHGrantRecords(fkStudyId) in StudyNIHGrantJB " + e);
     }
     return studyNIHGrantDao;
 }
 
 /**
  * 
  * @return int
  */
	 public int updateStudyNIHGrant() {
	     int output;
	     try {
	    	 StudyNIHGrantAgentRObj studyNIHGrantAgentRObj = EJBUtil.getStudyNIHGrantAgentHome();
	         output = studyNIHGrantAgentRObj.updateStudyNIHGrant(this.createStudyNIHGrantStateKeeper());
	
	     } catch (Exception e) {
	         Rlog.debug("studyNIhGrant",
	                 "EXCEPTION IN SETTING updateStudyNIHGrant TO  SESSION BEAN"
	                         + e.getMessage());
	
	         return -2;
	     }
	     return output;
	 }

/**
 * 
 * @param studyNIHGrantId
 * @param argsvalues
 * @return int
 */
	 public int deleteStudyNIHGrantRecord(int studyNIHGrantId, Hashtable<String, String> argsvalues)
	 {
	 try {
		 
		 StudyNIHGrantAgentRObj studyNIHGrantAgentRObj = EJBUtil.getStudyNIHGrantAgentHome();
	  	 return studyNIHGrantAgentRObj.deleteStudyNIHGrantRecord(studyNIHGrantId, argsvalues);
	  	 
	  } catch (Exception e) {
	      Rlog.debug("studyNIhGrant",
	              "Exception in deleteStudyNIHGrantRecord "
	                      + e.getMessage());
	      return -2;
	  }
	}


}
