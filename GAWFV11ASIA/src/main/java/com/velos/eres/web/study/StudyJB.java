/*
 * Classname : StudyJB.java
 * 
 * Version information
 *
 * Date 02/16/2001
 * 
 * Copyright notice: Aithent
 */

package com.velos.eres.web.study;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.audit.web.AuditRowEresJB;
import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.business.common.LookupDao;
import com.velos.eres.business.common.PatStudyStatDao;
import com.velos.eres.business.common.StudyDao;
import com.velos.eres.business.common.TeamDao;
import com.velos.eres.business.study.impl.StudyBean;
import com.velos.eres.gems.business.WorkflowDAO;
import com.velos.eres.service.studyAgent.StudyAgent;
import com.velos.eres.service.util.CFG;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.account.AccountJB;
import com.velos.eres.web.grpRights.GrpRightsJB;
import com.velos.eres.web.patProt.PatProtJB;
import com.velos.eres.web.studyRights.StudyRightsJB;

/**
 * Client side bean for the Study
 * 
 * @author Sonia Sahni
 */

public class StudyJB implements Serializable{

	/**
     * the study Id
     */
    private int id;

    /**
     * the Account
     */
    private String account;

    /**
     * the Study Sponsor
     */
    private String studySponsor;

    /**
     * the Study Contact
     */
    private String studyContact;

    /**
     * the Study Public Flag
     */

    private String studyPubFlag;

    /**
     * the Study Title
     */

    private String studyTitle = "";

    /**
     * the Study Objective
     */
    private String studyObjective = "";

    /**
     * the Study Summary
     */
    private String studySummary = "";

    /**
     * the Study Product
     */
    private String studyProduct = "";

    /**
     * the Study Size
     */
    private String studySize;

    /**
     * the Study Duration
     */
    private String studyDuration;

    /**
     * the Study Duration Unit
     */
    private String studyDurationUnit = "";

    /**
     * the Study Estimated Begin date
     */
    private String studyEstBeginDate;

    /**
     * the Study Actual Begin Date
     */
    private String studyActBeginDate;

    /**
     * the Study End Date
     */
    private String studyEndDate;

    /**
     * the Study Principal Investigator
     */
    private String studyPrimInv = "";

    /**
     * the Study Coordinator
     */
    private String studyCoordinator = "";

    /**
     * the Study Principal Investigator
     */
    private String studyPartCenters = "";

    /**
     * the Study keywords
     */
    private String studyKeywords = "";

    /**
     * the Study Public Columns
     */
    private String studyPubCol = "";

    /**
     * the Study Parent Study ID
     */
    private String studyParent;

    /**
     * the Study Author
     */
    private String studyAuthor;

    /**
     * the Study Therapeutic Area
     */
    private String studyTArea;

    /**
     * the Study Research Type
     */
    private String studyResType;

    /**
     * the Study Purpose
     */
    private String studyPurpose;

    /**
     * the Study TYpe
     */
    private String studyType;

    /**
     * the Study Blinding
     */
    private String studyBlinding;

    /**
     * the Study Randomization
     */
    private String studyRandom;

    
    // Added by gopu for September2006 Enhancement #S7
    /**
     * the Study Sponsor Name
     */
    private String studySponsorName;       
    
    /**
     * the Study Sponsor Id Information
     */
    private String studySponsorIdInfo;
    
    
    /**
     * the Study Number
     */
    private String studyNumber = "";

    /**
     * the Study Phase
     */
    private String studyPhase;

    /**
     * the Study Version
     */
    private String studyVersion;

    /**
     * the Study Current
     */
    private String studyCurrent;

    /**
     * the Study Info
     */
    private String studyInfo;

    /**
     * the PI major auhtor flag
     */
    private String majAuthor;

    /**
     * the Disease site(s)
     */
    private String disSite;

    /**
     * the scope of study-Codelst
     */
    private String studyScope;

    /**
     * the scope of study-linked to another study
     */
    private String studyAssoc;

    /**
     * creator
     */
    private String creator;

    /**
     * last modified by
     */
    private String modifiedBy;

    /**
     * IP Address
     */
    private String ipAdd;

    /**
     * the Study Version Parent Study ID
     */
    private String studyVersionParent;

    /**
     * the Study Currency
     */
    private String studyCurrency;

    /**
     * the Study Asccociated lookup version
     */
    private String studyAdvlkpVer;

    /**
     * the Study ICD-9 Code1
     */
    private String studyICDCode1;

    /**
     * the Study ICD-9 Code2
     */
    private String studyICDCode2;

    /**
     * the Study ICD-9 Code3
     */
    private String studyICDCode3;

    private String studyNSamplSize;

    private String studyOtherPrinv;

    /**
     * the Study Division
     */
    private String studyDivision;

    /**
     * Flag for PI as major author
     */
    private String studyInvIndIdeFlag;

    /**
     * IND/IDE #"
     */
    private String studyIndIdeNum;
    
    /**
     * Denotes how it was created as
     * D=Default; A=Application such as for IRB
     */
    private String studyCreationType;
    
    /*YK 29Mar2011 -DFIN20 */
    /**
     * Study Level Milestone status from Codelist
     */
    private String milestoneSetStatus;
    
    /**
     * CTRP-20527 22-Nov-2011 Ankit
     * 
     * the Study CTRP Reportable
     */
    public String studyCtrpReportable;
    
    /**
     * INF-22247 27-Feb-2012 Ankit
     * 
     * the Study FDA_REGULATED_STUDY
     */
    public String fdaRegulatedStudy;

    /**
     * CCSG DT4 Report
 	 *
     * the Study CCSG_REPORTABLE_STUDY
     */
    public String ccsgReportableStudy;
    
    
    /**
     * CTRP-22471 Raviesh
     * 
     * the Study NCI Trial Identifier
     */
    public String nciTrialIdentifier;
    
    /**
     * CTRP-22471 Raviesh
     * 
     * the Study NCT Number
     */
    public String nctNumber;
    /**
     * CTRP-22471 Raviesh
     * 
     * the Study CTRP Accrual Last Run Date
     */
    public String ctrpAccrualLastRunDate;
    
    /**
	 * CTRP-22470 Yogendra
	 * Last updated date when study accrual
	 * Zip file is downloaded from 
	 * CTRP ACCRUAL Browser's Download button 
	 */
    public String ctrpAccrualLastGenerationDate;
    /**
     * CTRP-22498 Yogesh
     * Date till when the patient data is downloaded.
     * Zip file is downloaded from 
     * CTRP ACCRUAL Browser's Download button 
     */
    public String cutOffDate;
    
    /**
     * CTRP-22498 Yogesh
     * Last Download By the patient data .
     * Zip file is downloaded from 
     * CTRP ACCRUAL Browser's Download button 
     */
    public String lastDownloadBy;    
    
    private String studyIsLocked;
    
    private String studyIsBlocked;
    
    // END OF MEMBER DECLARATION

    // GETTER SETTER METHODS
    /*YK 29Mar2011 -DFIN20 */
    public String getMilestoneSetStatus() {
		return milestoneSetStatus;
	}

	public void setMilestoneSetStatus(String milestoneSetStatus) {
		this.milestoneSetStatus = milestoneSetStatus;
	}

	public String getAccount() {
        return this.account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStudyActBeginDate() {
        return this.studyActBeginDate;
    }

    public void setStudyActBeginDate(String studyActBeginDate) {
        this.studyActBeginDate = studyActBeginDate;
    }

    public String getStudyEndDate() {
        return this.studyEndDate;
    }

    public void setStudyEndDate(String studyEndDate) {
        this.studyEndDate = studyEndDate;
    }

    public String getStudyAuthor() {
        return this.studyAuthor;
    }

    public void setStudyAuthor(String studyAuthor) {
        this.studyAuthor = studyAuthor;
    }

    public String getStudyBlinding() {
        return this.studyBlinding;
    }

    public void setStudyBlinding(String studyBlinding) {
        this.studyBlinding = studyBlinding;
    }

    public String getStudyContact() {
        return this.studyContact;
    }

    public void setStudyContact(String studyContact) {
        this.studyContact = studyContact;
    }

    public String getStudyDuration() {
        return this.studyDuration;
    }

    public void setStudyDuration(String studyDuration) {
        this.studyDuration = studyDuration;
    }

    public String getStudyDurationUnit() {
        return this.studyDurationUnit;
    }

    public void setStudyDurationUnit(String studyDurationUnit) {
        this.studyDurationUnit = studyDurationUnit;
    }

    public String getStudyEstBeginDate() {
        return this.studyEstBeginDate;
    }

    public void setStudyEstBeginDate(String studyEstBeginDate) {
        this.studyEstBeginDate = studyEstBeginDate;
    }

    /**
     * Get Study Keywords *
     * 
     * @return String
     */
    public String getStudyKeywords() {
        return this.studyKeywords;
    }

    public void setStudyKeywords(String studyKeywords) {
        this.studyKeywords = studyKeywords;
    }

    public String getStudyNumber() {
        return this.studyNumber;
    }

    public void setStudyNumber(String studyNumber) {
        this.studyNumber = studyNumber;
    }

    public String getStudyObjective() {
        return this.studyObjective;
    }

    public void setStudyObjective(String studyObjective) {
        this.studyObjective = studyObjective;
    }

    public String getStudyParent() {
        return this.studyParent;
    }

    public void setStudyParent(String studyParent) {
        this.studyParent = studyParent;
    }

    public String getStudyPartCenters() {
        return this.studyPartCenters;
    }

    public void setStudyPartCenters(String studyPartCenters) {
        this.studyPartCenters = studyPartCenters;
    }

    public String getStudyPrimInv() {
        return this.studyPrimInv;
    }

    public void setStudyPrimInv(String studyPrimInv) {
        this.studyPrimInv = studyPrimInv;
    }

    /**
     * Returns the value of studyCoordinator.
     */
    public String getStudyCoordinator() {
        return studyCoordinator;
    }

    /**
     * Sets the value of studyCoordinator.
     * 
     * @param studyCoordinator
     *            The value to assign studyCoordinator.
     */
    public void setStudyCoordinator(String studyCoordinator) {
        this.studyCoordinator = studyCoordinator;
    }

    public String getStudyProduct() {
        return this.studyProduct;
    }

    public void setStudyProduct(String studyProduct) {
        this.studyProduct = studyProduct;
    }

    public String getStudyPubCol() {
        return this.studyPubCol;
    }

    public void setStudyPubCol(String studyPubCol) {
        this.studyPubCol = studyPubCol;
    }

    public String getStudyPubFlag() {
        return this.studyPubFlag;
    }

    public void setStudyPubFlag(String studyPubFlag) {
        this.studyPubFlag = studyPubFlag;
    }

    public String getStudyRandom() {
        return this.studyRandom;
    }

    public void setStudyRandom(String studyRandom) {
        this.studyRandom = studyRandom;
    }

    // Added by Gopu for September Enhancement #S7
    
       
    public String getStudySponsorName() {
        return this.studySponsorName;
    }

    public void setStudySponsorName(String studySponsorName) {
        this.studySponsorName = studySponsorName;
    }
    
    public String getStudySponsorIdInfo() {
        return this.studySponsorIdInfo;
    }

    public void setStudySponsorIdInfo(String studySponsorIdInfo) {
        this.studySponsorIdInfo = studySponsorIdInfo;
    }
    
  /////////////
    
    
    
    public String getStudyResType() {
        return this.studyResType;
    }

    public void setStudyResType(String studyResType) {
        this.studyResType = studyResType;
    }

    public String getStudyPurpose() {
    	return this.studyPurpose;
    }
    
    public void setStudyPurpose(String studyPurpose) {
    	this.studyPurpose = studyPurpose;
    }

    public String getStudySize() {
        return this.studySize;
    }

    public void setStudySize(String studySize) {
        this.studySize = studySize;
    }

    public String getStudySponsor() {
        return this.studySponsor;
    }

    public void setStudySponsor(String studySponsor) {
        this.studySponsor = studySponsor;
    }

    public String getStudySummary() {
        return this.studySummary;
    }

    public void setStudySummary(String studySummary) {
        this.studySummary = studySummary;
    }

    public String getStudyTArea() {
        return this.studyTArea;
    }

    public void setStudyTArea(String studyTArea) {
        this.studyTArea = studyTArea;
    }

    public String getStudyTitle() {
        return this.studyTitle;
    }

    public void setStudyTitle(String studyTitle) {
        this.studyTitle = studyTitle;
    }
    
    public String getStudyScope() {
        return this.studyScope;
    }

    public void setStudyScope(String studyScope) {
        this.studyScope = studyScope;
    }

    public String getStudyType() {
        return this.studyType;
    }

    public void setStudyType(String studyType) {
        this.studyType = studyType;
    }

    public String getStudyPhase() {
        return this.studyPhase;
    }

    public void setStudyPhase(String studyPhase) {
        this.studyPhase = studyPhase;
    }

    public String getStudyVersion() {
        return this.studyVersion;
    }

    public void setStudyVersion(String studyVersion) {
        this.studyVersion = studyVersion;
    }

    public String getStudyCurrent() {
        return this.studyCurrent;
    }

    public void setStudyCurrent(String studyCurrent) {
        this.studyCurrent = studyCurrent;
    }

    public String getStudyInfo() {
        Rlog.debug("study", "IN StudyJB.getStudyInfo studyInfo "
                + this.studyInfo);
        return this.studyInfo;
    }

    public void setStudyInfo(String studyInfo) {
        this.studyInfo = studyInfo;
        Rlog.debug("study", "IN StudyJB.setStudyInfo studyInfo "
                + this.studyInfo);
    }

    public void setMajAuthor(String majAuthor) {
        this.majAuthor = majAuthor;
    }

    public void setDisSite(String disSite) {
        this.disSite = disSite;
    }

    public void setStudyAssoc(String studyAssoc) {
        this.studyAssoc = studyAssoc;
    }

    public String getMajAuthor() {
        return majAuthor;
    }

    public String getDisSite() {
        return disSite;
    }

    public String getStudyAssoc() {
        return studyAssoc;
    }

    /**
     * @return Creator
     */
    public String getCreator() {
        return this.creator;
    }

    /**
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return Last Modified By
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param modifiedBy
     *            The value that is required to be registered as record last
     *            modified by
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return IP Address
     */
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    public String getStudyVersionParent() {
        return this.studyVersionParent;
    }

    public void setStudyVersionParent(String studyVersionParent) {
        this.studyVersionParent = studyVersionParent;
    }

    public String getStudyCurrency() {
        return this.studyCurrency;
    }

    public void setStudyCurrency(String studyCurrency) {
        this.studyCurrency = studyCurrency;
    }

    public String getStudyAdvlkpVer() {
        return studyAdvlkpVer;
    }

    public void setStudyAdvlkpVer(String studyAdvlkpVer) {
        this.studyAdvlkpVer = studyAdvlkpVer;
    }

    public void setStudyNSamplSize(String studyNSamplSize) {
        this.studyNSamplSize = studyNSamplSize;
    }

    public void setStudyOtherPrinv(String studyOtherPrinv) {
        this.studyOtherPrinv = studyOtherPrinv;
    }

    public String getStudyNSamplSize() {
        return studyNSamplSize;
    }

    public String getStudyOtherPrinv() {
        return studyOtherPrinv;
    }

    /**
     * Get Study Division
     * 
     * @return String
     */
    public String getStudyDivision() {
        return this.studyDivision;
    }

    /**
     * sets study division
     * 
     * @param studyDivision
     */
    public void setStudyDivision(String studyDivision) {
        this.studyDivision = studyDivision;
    }

    /**
     * Get Study IND/IDE flag
     * 
     * @return String
     */
    public String getStudyInvIndIdeFlag() {
        return this.studyInvIndIdeFlag;
    }

    /**
     * sets Study investigator flag
     * 
     * @param studyInvIndIdeFlag
     */
    public void setStudyInvIndIdeFlag(String studyInvIndIdeFlag) {
        this.studyInvIndIdeFlag = studyInvIndIdeFlag;
    }

    /**
     * Get Study IND/IDE #
     * 
     * @return String
     */

    public String getStudyIndIdeNum() {
        return this.studyIndIdeNum;
    }

    /**
     * sets IND/IDE #
     * 
     * @param studyIndIdeNum
     */
    public void setStudyIndIdeNum(String studyIndIdeNum) {
        this.studyIndIdeNum = studyIndIdeNum;
    }

    /**
     * Get Study ICD-9 code1
     * 
     * @return String
     */

    public String getStudyICDCode1() {
        return this.studyICDCode1;
    }

    /**
     * sets studyICDCode1
     * 
     * @param studyICDCode1
     */
    public void setStudyICDCode1(String studyICDCode1) {
        this.studyICDCode1 = studyICDCode1;
    }

    /**
     * Get Study ICD-9 code2
     * 
     * @return String
     */

    public String getStudyICDCode2() {
        return this.studyICDCode2;
    }

    /**
     * sets studyICDCode2
     * 
     * @param studyICDCode2
     */
    public void setStudyICDCode2(String studyICDCode2) {
        this.studyICDCode2 = studyICDCode2;
    }

    /**
     * Get Study ICD-9 code3
     * 
     * @return String
     */

    public String getStudyICDCode3() {
        return this.studyICDCode3;
    }

    /**
     * sets studyICDCode3
     * 
     * @param studyICDCode3
     */
    public void setStudyICDCode3(String studyICDCode3) {
        this.studyICDCode3 = studyICDCode3;
    }
    
    public String getStudyCreationType() {
        return studyCreationType;
    }

    public void setStudyCreationType(String studyCreationType) {
        this.studyCreationType = studyCreationType;
    }
    
    //CTRP-20527 22-Nov-2011 Ankit
    public String getStudyCtrpReportable() {
    	return studyCtrpReportable;
    }

    public void setStudyCtrpReportable(String studyCtrpReportable) {
    	 this.studyCtrpReportable = studyCtrpReportable;
    }

    //INF-22247 27-Feb-2012 Ankit
    public String getFdaRegulatedStudy() {
    	return fdaRegulatedStudy;
    }

    public void setFdaRegulatedStudy(String fdaRegulatedStudy) {
    	 this.fdaRegulatedStudy = fdaRegulatedStudy;
    }
    
    public String getCcsgReportableStudy() {
    	return ccsgReportableStudy;
    }
    
    public void setCcsgReportableStudy(String ccsgReportableStudy) {
    	this.ccsgReportableStudy = ccsgReportableStudy;
    }
    
    
    //CTRP-22471 : Raviesh
    public String getNciTrialIdentifier() {
		return nciTrialIdentifier;
	}

	public void setNciTrialIdentifier(String nciTrialIdentifier) {
		this.nciTrialIdentifier = nciTrialIdentifier;
	}
	
	//CTRP-22471 : Raviesh
	public String getNctNumber() {
		return nctNumber;
	}

	public void setNctNumber(String nctNumber) {
		this.nctNumber = nctNumber;
	}
	
	//CTRP-22471 : Raviesh
	public String getCtrpAccrualLastRunDate() {
		return ctrpAccrualLastRunDate;
	}

	public void setCtrpAccrualLastRunDate(String ctrpAccrualLastRunDate) {
		this.ctrpAccrualLastRunDate = ctrpAccrualLastRunDate;
	}
	
    
    // END OF GETTER SETTER METHODS

    public StudyJB(String studyNumber) {
        setStudyNumber(studyNumber);
    }

    public StudyJB() {
    }

    /**
     * Full arguments constructor.
     * 
     * @param
     */
    public StudyJB(int id, String account, String studySponsor, 
            String studyContact, String studyPubFlag, String studyTitle,
            String studyObjective, String studySummary, String studyProduct,
            String studySize, String studyDuration, String studyDurationUnit,
            String studyEstBeginDate, String studyActBeginDate,
            String studyPrimInv, String studyCoordinator,
            String studyPartCenters, String studyKeywords, String studyPubCol,
            String studyParent, String studyAuthor, String studyTArea,
            String studyResType, String studyPurpose, String studyType, String studyBlinding,
            String studyRandom, String studyNumber, String studyPhase,
            String studyVersion, String studyCurrent, String studyInfo,
            String majAuthor, String disSite, String studyScope,
            String studyAssoc, String creator, String modifiedBy, String ipAdd,
            String studyVersionParent, String studyCurrency,
            String studyEndDate, String studyAdvlkpVer, String studyNSamplSize,
            String studyOtherPrinv, String studyDivision,
            String studyInvIndIdeFlag, String studyIndIdeNum,
            String studyICDCode1, String studyICDCode2, String studyICDCode3,
            String studySponsorName, String studySponsorIdInfo,String milestoneSetStatus,
            String studyCtrpReportable,String fdaRegulatedStudy,String ccsgReportableStudy, 
            String nciTrialIdentifier,String nctNumber,String ctrpAccrualLastRunDate,
            String ctrpAccrualLastGenerationDate,String cutOffDate, String lastDownloadBy,
            String studyIsLocked, String studyIsBlocked) {
        setAccount(account);
        setId(id);
        setStudyActBeginDate(studyActBeginDate);
        setStudyAuthor(studyAuthor);
        setStudyBlinding(studyBlinding);
        setStudyContact(studyContact);
        setStudyDuration(studyDuration);
        setStudyDurationUnit(studyDurationUnit);
        setStudyEstBeginDate(studyEstBeginDate);
        setStudyKeywords(studyKeywords);
        setStudyNumber(studyNumber);
        setStudyObjective(studyObjective);
        setStudyParent(studyParent);
        setStudyPartCenters(studyPartCenters);
        setStudyPrimInv(studyPrimInv);
        setStudyCoordinator(studyCoordinator);
        setStudyProduct(studyProduct);
        setStudyPubCol(studyPubCol);
        setStudyPubFlag(studyPubFlag);
        setStudyRandom(studyRandom);
        setStudyResType(studyResType);
        setStudyPurpose(studyPurpose);
        setStudySize(studySize);
        setStudySponsor(studySponsor);
        //gopu
        setStudySponsorName(studySponsorName);
        setStudySponsorIdInfo(studySponsorIdInfo);
        setStudySummary(studySummary);
        setStudyTArea(studyTArea);
        setStudyTitle(studyTitle);
        setStudyScope(studyScope);
        setStudyType(studyType);
        setStudyPhase(studyPhase);
        setStudyVersion(studyVersion);
        setStudyCurrent(studyCurrent);
        setStudyInfo(studyInfo);
        setMajAuthor(majAuthor);
        setDisSite(disSite);
        setStudyAssoc(studyAssoc);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setStudyVersionParent(studyVersionParent);
        setStudyCurrency(studyCurrency);
        setStudyEndDate(studyEndDate);
        setStudyAdvlkpVer(studyAdvlkpVer);
        setStudyNSamplSize(studyNSamplSize);
        setStudyOtherPrinv(studyOtherPrinv);
        setStudyDivision(studyDivision);
        setStudyInvIndIdeFlag(studyInvIndIdeFlag);
        setStudyIndIdeNum(studyIndIdeNum);
        setStudyICDCode1(studyICDCode1);
        setStudyICDCode2(studyICDCode2);
        setStudyICDCode3(studyICDCode3);
        setMilestoneSetStatus(milestoneSetStatus);
        setStudyCtrpReportable(studyCtrpReportable);
        setFdaRegulatedStudy(fdaRegulatedStudy);
        setCcsgReportableStudy(ccsgReportableStudy);
        setNciTrialIdentifier(nciTrialIdentifier);
        setNctNumber(nctNumber);
        setCtrpAccrualLastRunDate(ctrpAccrualLastRunDate);
        setCtrpAccrualLastGenerationDate(ctrpAccrualLastGenerationDate);
        setCutOffDate(cutOffDate);
        setLastDownloadBy(lastDownloadBy);
        setStudyIsLocked(studyIsLocked);
        setStudyIsBlocked(studyIsBlocked);
    }
    
    /**
     * Calls getEventdefDetails() of StudyAgent facade and extracts this state
     * holder into the been attribute fields.
     * 
     * 
     * @return The Details of the StudyAgent in the StudyAgent StateKeeper
     *         Object
     * @See StudyAgentBean
     */

    public StudyBean getStudyDetails() {
        StudyBean ssh = null;
        try {
            StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
            ssh = studyAgent.getStudyDetails(this.id);
        } catch (Exception e) {
            Rlog.fatal("study", "Exception in StudyJB.getStudyDetails()" + e);

        }
        if (ssh != null) {

            this.account = ssh.getAccount();
            this.id = ssh.getId();
            this.studyActBeginDate = ssh.getStudyActBeginDate();
            this.studyAuthor = ssh.getStudyAuthor();
            this.studyBlinding = ssh.getStudyBlinding();
            this.studyContact = ssh.getStudyContact();
            this.studyDuration = ssh.getStudyDuration();
            this.studyDurationUnit = ssh.getStudyDurationUnit();
            this.studyEstBeginDate = ssh.getStudyEstBeginDate();
            this.studyKeywords = ssh.getStudyKeywords();
            this.studyNumber = ssh.getStudyNumber();
            this.studyObjective = ssh.getStudyObjective();
            this.studyParent = ssh.getStudyParent();
            this.studyPartCenters = ssh.getStudyPartCenters();
            this.studyPrimInv = ssh.getStudyPrimInv();
            this.studyCoordinator = ssh.getStudyCoordinator();
            this.studyProduct = ssh.getStudyProduct();
            this.studyPubCol = ssh.getStudyPubCol();
            this.studyPubFlag = ssh.getStudyPubFlag();
            this.studyRandom = ssh.getStudyRandom();
            this.studyResType = ssh.getStudyResType();
            this.studyPurpose = ssh.getStudyPurpose();
            this.studySize = ssh.getStudySize();
            this.studySponsor = ssh.getStudySponsor();
            //gopu
            this.studySponsorName = ssh.getStudySponsorName();
            this.studySponsorIdInfo = ssh.getStudySponsorIdInfo();
            this.studySummary = ssh.getStudySummary();
            this.studyTArea = ssh.getStudyTArea();
            this.studyTitle = ssh.getStudyTitle();
            this.studyScope = ssh.getStudyScope();
            this.studyType = ssh.getStudyType();
            this.studyPhase = ssh.getStudyPhase();
            this.studyVersion = ssh.getStudyVersion();
            this.studyCurrent = ssh.getStudyCurrent();
            this.studyInfo = ssh.getStudyInfo();
            this.majAuthor = ssh.getMajAuthor();
            this.disSite = ssh.getDisSite();
            this.studyAssoc = ssh.getStudyAssoc();
            this.creator = ssh.getCreator();
            this.modifiedBy = ssh.getModifiedBy();
            this.ipAdd = ssh.getIpAdd();
            this.studyVersionParent = ssh.getStudyVersionParent();
            this.studyCurrency = ssh.getStudyCurrency();
            this.studyEndDate = ssh.getStudyEndDate();
            this.studyDivision = ssh.getStudyDivision();
            this.studyInvIndIdeFlag = ssh.getStudyInvIndIdeFlag();
            this.studyIndIdeNum = ssh.getStudyIndIdeNum();

            this.studyICDCode1 = ssh.getStudyICDCode1();
            this.studyICDCode2 = ssh.getStudyICDCode2();
            this.studyICDCode3 = ssh.getStudyICDCode3();

            this.studyCreationType = ssh.getStudyCreationType();
            this.milestoneSetStatus=ssh.getMilestoneSetStatus();
            this.studyCtrpReportable=ssh.getStudyCtrpReportable();
            this.fdaRegulatedStudy=ssh.getFdaRegulatedStudy();
            this.ccsgReportableStudy=ssh.getCcsgReportableStudy();
            
            this.nciTrialIdentifier=ssh.getNciTrialIdentifier();
            this.nctNumber=ssh.getNctNumber();
            this.ctrpAccrualLastRunDate=ssh.getCtrpAccrualLastRunDate();
            this.ctrpAccrualLastGenerationDate=ssh.getCtrpAccrualLastGenerationDate();
            this.cutOffDate=ssh.getCutOffDate();
            this.lastDownloadBy=ssh.getLastDownloadBy();
            setStudyAdvlkpVer(ssh.getStudyAdvlkpVer());
            setStudyNSamplSize(ssh.getStudyNSamplSize());
            setStudyOtherPrinv(ssh.getStudyOtherPrinv());
            setMilestoneSetStatus(ssh.getMilestoneSetStatus());
            setStudyCtrpReportable(ssh.getStudyCtrpReportable());
            setFdaRegulatedStudy(ssh.getFdaRegulatedStudy());
            setCcsgReportableStudy(ssh.getCcsgReportableStudy());
            setStudyIsLocked(ssh.getStudyIsLocked());
            setStudyIsBlocked(ssh.getStudyIsBlocked());
        }
        return ssh;
    }

    /**
     * Calls setStudyDetails() of Study Session Bean: StudyAgentBean returns 1
     * if successful
     */

    public int setStudyDetails() {
        try {
            int output = 0;
            CallableStatement cstmt = null;
            Connection conn = null;
            StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
            this.setId(studyAgent.setStudyDetails(this.createStudyStateKeeper()));
            output = this.getId();
            String study_Obj="";
            String study_Summary="";
            int rid=0;
            
            //JM 28Sep2006            
            CommonDAO cDao = new CommonDAO();
            
            Rlog.debug("study:---Objective", "study data saved:" + output);
            Rlog.debug("study", "study this.getContents():" + this.getStudyObjective()); 
            Rlog.debug("study:--Summary", "study data saved:" + output);
            Rlog.debug("study", "study this.getContents():" + this.getStudySummary());
            study_Obj=this.getStudyObjective()==null?"":this.getStudyObjective();
            study_Summary=this.getStudySummary()==null?"":this.getStudySummary();
            if(!study_Obj.equals("") || !study_Summary.equals("")){
            cDao.updateClob(this.getStudyObjective(),"er_study","study_obj_clob"," where pk_study = " + output);
            cDao.updateClob(this.getStudySummary(),"er_study","study_sum_clob"," where pk_study = " + output);
            AuditRowEresJB auditRowEresJB = new AuditRowEresJB();
        	AuditUtils audittrails = new AuditUtils();
            rid = auditRowEresJB.getRidForDeleteOperation(this.getId(), EJBUtil.stringToNum(this.getCreator()), "ER_STUDY", "PK_STUDY");
			audittrails.deleteRaidList("eres", rid, this.getCreator(), "U");
                        
             }
            ///
           
            
            if (output > 0) {
                try {
                	String procSql = "";
                	if(CFG.EIRB_MODE.equals("LIND")){
                		procSql = "{call PKG_STUDYSTAT.SP_CREATE_DEF_VERSION_LIND(?,?,?)}";
                	}else{
                		procSql = "{call PKG_STUDYSTAT.SP_CREATE_DEFAULT_VERSION(?,?,?)}";
                	}
                	System.out.println("procSql------->"+procSql);
                    conn = EJBUtil.getConnection();
                    cstmt = conn
                            .prepareCall(procSql);
                    cstmt.setInt(1, output);
                    cstmt.setInt(2, Integer.parseInt(this.creator));
                    cstmt.setString(3, this.ipAdd);
                    cstmt.execute();
                } catch (SQLException ex) {
                    Rlog
                            .fatal(
                                    "study",
                                    "In setStudyDetails call to sp_create_default_Version EXCEPTION IN calling the procedure"
                                            + ex);
                } finally {
                    try {
                        if (cstmt != null)
                            cstmt.close();
                    } catch (Exception e) {
                    }
                    try {
                        if (conn != null)
                            conn.close();
                    } catch (Exception e) {
                    }
                }
            }

            return output;
        } catch (Exception e) {
            Rlog.fatal("study", "EXCEPTION IN setStudyDetails" + e);
            return -1;
        }
    }

    public int updateStudy() {
        int output;
        int rid=0;
        try {
            StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
            output = studyAgent.updateStudy(this.createStudyStateKeeper());
            
            //JM: 28Sept2006
            CommonDAO cDao = new CommonDAO();
            cDao.updateClob(this.getStudyObjective(),"er_study","study_obj_clob"," where pk_study = " + this.getId());
            cDao.updateClob(this.getStudySummary(),"er_study","study_sum_clob"," where pk_study = " + this.getId());
            
            AuditRowEresJB auditRowEresJB = new AuditRowEresJB();
        	AuditUtils audittrails = new AuditUtils();
        	rid = auditRowEresJB.getRidForDeleteOperation(this.getId(), EJBUtil.stringToNum(this.getModifiedBy()), "ER_STUDY", "PK_STUDY");
			audittrails.deleteRaidList("eres", rid, this.getModifiedBy(), "U");
           
           
            
        } catch (Exception e) {
            Rlog.fatal("study", "EXCEPTION IN updateStudy" + e);
            return -2;
        }
        return output;
    }

    /*
     * places bean attributes into a StudyStateHolder.
     */
    public StudyBean createStudyStateKeeper() {
    	
        return new StudyBean(new Integer(id), account, studySponsor, 
                studyContact, studyPubFlag, studyTitle, 
                //studyObjective,studySummary, 
                studyProduct, studySize, studyDuration,
                studyDurationUnit, studyEstBeginDate, studyActBeginDate,
                studyEndDate, studyPrimInv, studyCoordinator, studyPartCenters,
                studyKeywords, studyPubCol, studyParent, studyAuthor,
                studyTArea, studyResType, studyPurpose, studyType, studyBlinding,
                studyRandom, studyNumber, studyPhase, studyVersion,
                studyCurrent, studyInfo, majAuthor, disSite, studyScope,
                studyAssoc, creator, modifiedBy, ipAdd, studyVersionParent,
                studyICDCode1, studyICDCode2, studyICDCode3, studyCurrency,
                studyAdvlkpVer, studyNSamplSize, studyOtherPrinv,
                studyDivision, studyInvIndIdeFlag, studyIndIdeNum, studySponsorName, studySponsorIdInfo,
                studyCreationType,milestoneSetStatus,studyCtrpReportable,fdaRegulatedStudy,ccsgReportableStudy,
                nciTrialIdentifier,nctNumber,ctrpAccrualLastRunDate,ctrpAccrualLastGenerationDate,cutOffDate, lastDownloadBy,
                studyIsLocked, studyIsBlocked);
    }

    public StudyDao getStudyValuesKeyword(String search) {
        StudyDao studyDao = new StudyDao();
        try {
            Rlog.debug("study", "StudyJB.getStudyValuesKeyword starting");
            StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
            Rlog.debug("study", "StudyJB.getStudyValuesKeyword after remote");
            studyDao = studyAgent.getStudyValuesKeyword(search);
            Rlog.debug("study", "StudyJB.getStudyValuesKeyword after Dao");
            return studyDao;
        } catch (Exception e) {
            Rlog.fatal("study",
                    "Error in getStudyValuesKeyword(String search) in StudyJB "
                            + e);
        }
        return studyDao;
    }
 
    public StudyDao getStudyValuesKeywordAll(ArrayList search) {
        StudyDao studyDao = new StudyDao();
        try {
            Rlog.debug("study", "StudyJB.getStudyValuesKeywordAll starting");
            StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
            Rlog
                    .debug("study",
                            "StudyJB.getStudyValuesKeywordAll after remote");
            studyDao = studyAgent.getStudyValuesKeywordAll(search);
            Rlog.debug("study", "StudyJB.getStudyValuesKeywordAll after Dao");
            return studyDao;
        } catch (Exception e) {
            Rlog.fatal("study",
                    "Error in getStudyValuesKeywordAll(String search) in StudyJB "
                            + e);
        }
        return studyDao;
    }

    public StudyDao getStudyValuesAuthor(String search) {
        StudyDao studyDao = new StudyDao();
        try {
            Rlog.debug("study", "StudyJB.getStudyValuesAuthor starting");
            StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
            Rlog.debug("study", "StudyJB.getStudyValuesAuthor after remote");
            studyDao = studyAgent.getStudyValuesAuthor(search);
            Rlog.debug("study", "StudyJB.getStudyValuesAuthor after Dao");
            return studyDao;
        } catch (Exception e) {
            Rlog.fatal("study",
                    "Error in getStudyValuesAuthor(String search) in StudyJB "
                            + e);
        }
        return studyDao;
    }

    public StudyDao getStudyValuesTitle(String search) {
        StudyDao studyDao = new StudyDao();
        try {
            Rlog.debug("study", "StudyJB.getStudyValuesTitle starting");
            StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
            Rlog.debug("study", "StudyJB.getStudyValuesTitle after remote");
            studyDao = studyAgent.getStudyValuesTitle(search);
            Rlog.debug("study", "StudyJB.getStudyValuesTitle after Dao");
            return studyDao;
        } catch (Exception e) {
            Rlog.fatal("study",
                    "Error in getStudyValuesTitle(String search) in StudyJB "
                            + e);
        }
        return studyDao;
    }

    public StudyDao getStudyValuesTarea(String search) {
        StudyDao studyDao = new StudyDao();
        try {
            Rlog.debug("study", "StudyJB.getStudyValuesTarea starting");
            StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
            Rlog.debug("study", "StudyJB.getStudyValuesTarea after remote");
            studyDao = studyAgent.getStudyValuesTarea(search);
            Rlog.debug("study", "StudyJB.getStudyValuesTarea after Dao");
            return studyDao;
        } catch (Exception e) {
            Rlog.fatal("study",
                    "Error in getStudyValuesTarea(String search) in StudyJB "
                            + e);
        }
        return studyDao;
    }

    public StudyDao getStudyValuesForUsers(String userId) {
        StudyDao studyDao = new StudyDao();
        try {
            Rlog.debug("study", "StudyJB.getStudyValuesForUsers starting");
            StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
            Rlog.debug("study", "StudyJB.getStudyValuesForUsers after remote");
            studyDao = studyAgent.getStudyValuesForUsers(userId);
            Rlog.debug("study", "StudyJB.getStudyValuesForUsers after Dao");
            return studyDao;
        } catch (Exception e) {
            Rlog.fatal("study",
                    "Error in getStudyValuesForUsers(String userId) in StudyJB "
                            + e);
        }
        return studyDao;
    }

    // over loaded function

    public StudyDao getStudyValuesForUsers(String userId, String studycount) {
        StudyDao studyDao = new StudyDao();
        try {
            Rlog.debug("study", "StudyJB.getStudyValuesForUsers starting");
            StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
            Rlog.debug("study", "StudyJB.getStudyValuesForUsers after remote");
            studyDao = studyAgent.getStudyValuesForUsers(userId, studycount);
            Rlog.debug("study", "StudyJB.getStudyValuesForUsers after Dao");
            return studyDao;
        } catch (Exception e) {
            Rlog.fatal("study",
                    "Error in getStudyValuesForUsers(String userId) in StudyJB "
                            + e);
        }
        return studyDao;
    }

    public StudyDao verifyStudyRequest(String studyId, String userId) {
        StudyDao studyDao = new StudyDao();
        try {
            Rlog.debug("study", "StudyJB.verifyStudyRequest starting");
            StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
            Rlog.debug("study", "StudyJB.verifyStudyRequest after remote");
            studyDao = studyAgent.verifyStudyRequest(studyId, userId);
            Rlog.debug("study", "StudyJB.verifyStudyRequest after Dao");
            return studyDao;
        } catch (Exception e) {
            Rlog.fatal("study", "Error in verifyStudyRequest in StudyJB " + e);
        }
        return studyDao;
    }

    public int getStudyCount(String accountId) {
        int count = 0;
        try {
            Rlog.debug("study", "StudyJB.getStudyCount starting");
            StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
            Rlog.debug("study", "StudyJB.getStudyCount after remote");
            count = studyAgent.getStudyCount(accountId);
            Rlog.debug("study", "StudyJB.getStudyCount after Dao");
        } catch (Exception e) {
            Rlog.fatal("study",
                    "Error in getStudyCount(String accountId) in StudyJB " + e);
        }
        return count;
    }

    public void copyStudy(int studyId, int xUser, int author,
            String permission, int msgId) {
        try {
            Rlog.debug("study", "StudyJB.copyStudy starting");
            StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
            Rlog.debug("study", "StudyJB.copyStudy after remote");
            studyAgent.copyStudy(studyId, xUser, author, permission, msgId);
            Rlog.debug("study", "StudyJB.copyStudy after Dao");
        } catch (Exception e) {
            Rlog.fatal("study", "Error in copyStudy in StudyJB " + e);
        }
    }

    // to create study version
    public int createVersion(int studyId, int xUser) {
        try {
            Rlog.debug("study", "StudyJB.createVersion starting");
            StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
            return studyAgent.createVersion(studyId, xUser);

        } catch (Exception e) {
            Rlog.fatal("study", "Error in creteVersion in StudyJB " + e);
            return -1;
        }
    }

    // to get studies dropdown

    public StudyDao getUserStudies(String userId, String dName, int selVal,
            String flag) {
        StudyDao studyDao = new StudyDao();
        try {
            Rlog.debug("study", "StudyJB.getuserstudies starting");
            StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
            studyDao = studyAgent.getUserStudies(userId, dName, selVal, flag);
            return studyDao;

        } catch (Exception e) {
            Rlog.fatal("study", "Error in getuserstudies in StudyJB " + e);
            return studyDao;
        }
    }

    // get search results for patients enrolled to study

    public PatStudyStatDao getStudyPatientResults(int studyId,
            String patientID, int patStatus, int user, int userSite) {
        PatStudyStatDao pDao = new PatStudyStatDao();
        try {
            Rlog.debug("study", "StudyJB.getStudyPatientResults starting");
            StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
            Rlog.debug("study", "StudyJB.getStudyPatientResults after remote");
            pDao = studyAgent.getStudyPatientResults(studyId, patientID,
                    patStatus, user, userSite);
            Rlog.debug("study", "StudyJB.getStudyPatientResults after Dao");
            return pDao;
        } catch (Exception e) {
            Rlog.fatal("study", "Error in getStudyPatientResults in StudyJB "
                    + e);
        }
        return pDao;
    }

    /**
     * Gets Patients associated with a study. Filters data according to the
     * parameters passed. If no study is <br>
     * passed, gets data for all the studies
     * 
     * @param studyId
     *            Study Id
     * @param patientID
     *            Patient Code
     * @param patStatus
     *            Patient current status in Study
     * @param user
     *            Logged in user id
     * @param userSite
     *            Organization id
     * @param enrolDt
     *            Enrollment date
     * @param lastDoneDt
     *            Last visit done on
     * @param nextVisitDate
     *            Next visit date
     * @param lastVisit
     *            last Visit Number
     * @param htMoreFilterParams
     *            A Hashtable object with more filter parameters. Possible keys:
     *            <br>
     *            1. excludeNotEnrolled - "0" Do not exclude patients that have
     *            no enrollment record <br> - "1" exclude patients that have no
     *            enrollment record <br>
     *            2. sortByColumn - code for column name to sort on. These are
     *            not actual column names but a <br>
     *            friendly name for each sortable column. Codes currently
     *            supported:<br>
     *            <ul>
     *            <li>patId : for Patient Id</li>
     *            <li>patStudyId : for patient study id</li>
     *            <li>patName : for Patient Name</li>
     *            <li>enrolledOn : for Date enrolled on</li>
     *            <li>lastVisit : for Patient's Last Visit</li>
     *            <li>nextVisit : for Patient's Next Visit</li>
     *            <li>doneOn : for last event done on</li>
     *            <li>patientStudyStatus : for patient's current study status</li>
     *            </ul>
     *            3. sortByOrder - "asc" for ascending order <br>
     *            "desc" for descending order <br>
     *            If more filters are required, add to this hashtable instead of
     *            adding more parameters to this method
     * @return A PatStudyStatDao object with patient enrollment data
     */
    /*
     * Modified by Sonia, 09/09/04, modified javadoc for the method for
     * parameter htMoreFilterParams. The hashtable may process two more keys
     * now: sortByColumn and sortByOrder
     */
    /*
     * Modified by Sonia, 09/14/04, modified javadoc for the method for
     * parameter htMoreFilterParams. A new sort code patStudyId for patient
     * study id is added
     */

    // patStudyId is added by Manimaran on 21Apr2005 for Enrolled PatientStudyId
    // search
    // public PatStudyStatDao getStudyPatientResultsWithVisits(int studyId,
    // String patientID, int patStatus, int user, String userSite, String
    // enrolDt, String lastDoneDt, String nextVisitDate, String lastVisit,
    // Hashtable htMoreFilterParams)
    public PatStudyStatDao getStudyPatientResultsWithVisits(int studyId,
            String patientID, String patStudyId, int patStatus, int user,
            String userSite, String enrolDt, String lastDoneDt,
            String nextVisitDate, String lastVisit, Hashtable htMoreFilterParams) {

        PatStudyStatDao pDao = new PatStudyStatDao();
        try {

            StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
            pDao = studyAgent.getStudyPatientResultsWithVisits(studyId,
                    patientID, patStudyId, patStatus, user, userSite, enrolDt,
                    lastDoneDt, nextVisitDate, lastVisit, htMoreFilterParams);
            return pDao;

        } catch (Exception e) {
            Rlog
                    .fatal("study",
                            "Error in getStudyPatientResultsWithVisits in StudyJB "
                                    + e);
        }
        return pDao;
    }

    public PatStudyStatDao getPatientStudiesWithVisits(int studyId, int pkey,
            int patStatus, int user, int userSite, String enrolDt,
            String lastDoneDt, String nextVisitDate, String lastVisit) {
        PatStudyStatDao pDao = new PatStudyStatDao();
        try {

            StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
            pDao = studyAgent.getPatientStudiesWithVisits(studyId, pkey,
                    patStatus, user, userSite, enrolDt, lastDoneDt,
                    nextVisitDate, lastVisit);
            return pDao;
        } catch (Exception e) {
            Rlog.fatal("study", "Error in getStudyPatientResults in StudyJB "
                    + e);
        }
        return pDao;
    }

    public PatStudyStatDao getPatientStudies(int pkey, int user) {
        PatStudyStatDao pDao = new PatStudyStatDao();
        try {

            StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
            pDao = studyAgent.getPatientStudies(pkey, user);
            return pDao;
        } catch (Exception e) {
            Rlog.fatal("study", "Error in getPatientStudies in StudyJB " + e);
        }
        return pDao;
    }

    public int validateStudyNumber(String accId, String studyNum) {
        try {
            int output = 0;

            StudyAgent studyAgent = EJBUtil.getStudyAgentHome();

            output = studyAgent.validateStudyNumber(accId, studyNum);

            return output;
        } catch (Exception e) {
            Rlog.fatal("study", "Exception in validateStudyNumber in StudyJB "
                    + e);
            e.printStackTrace();
            return -1;
        }
    }

  /*  public int copyStudySelective(int origstudy, int statflag, int teamflag,
            String[] verarr, String[] calarr, String studynum, int studydm,
            String title, int userId, String ipAdd) {
        try {
            int output = 0;

            StudyAgent studyAgent = EJBUtil.getStudyAgentHome();

            output = studyAgent.copyStudySelective(origstudy, statflag,
                    teamflag, verarr, calarr, studynum, studydm, title, userId,
                    ipAdd);
            return output;
        } catch (Exception e) {
            Rlog.fatal("study", "Exception in copyStudySelective in StudyJB "
                    + e);
            e.printStackTrace();
            return -1;
        }
    }*/

    //JM: modified 31July2006
    
    public int copyStudySelective(int origstudy, int statflag, int teamflag, int dictflag, 
            String[] verarr, String[] calarr, String[] txarr, String[] formsarr, String studynum, int studydm,
            String title, int userId, String ipAdd) {
        try {
            int output = 0;

            StudyAgent studyAgent = EJBUtil.getStudyAgentHome();

            output = studyAgent.copyStudySelective(origstudy, statflag,
                    teamflag, dictflag, verarr, calarr, txarr, formsarr, studynum, studydm, title, userId,
                    ipAdd);
            if(output>0){
            	
        			WorkflowDAO wdao= new WorkflowDAO();
        			wdao.createWorkflow(String.valueOf(output),String.valueOf(userId));
        		
            }
            return output;
        } catch (Exception e) {
            Rlog.fatal("study", "Exception in copyStudySelective in StudyJB "
                    + e);
            e.printStackTrace();
            return -1;
        }
    }

    public String getLkpTypeVersionForView(int viewpk) {
        try {
            String output = "";

            StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
            output = studyAgent.getLkpTypeVersionForView(viewpk);
            return output;
        } catch (Exception e) {
            Rlog.fatal("study", "getLkpTypeVersionForView" + e);
            e.printStackTrace();
            return null;
        }
    }

    public LookupDao getAllViewsForLkpType(int account, String lkptype) {
        LookupDao ld = new LookupDao();
        try {
            String output = "";

            StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
            ld = studyAgent.getAllViewsForLkpType(account, lkptype);
            return ld;
        } catch (Exception e) {
            Rlog.fatal("study", "getAllViewsForLkpType" + e);
            return ld;
        }
    }

    /*
     * generates a String of XML for the customer details entry form.<br><br>
     * Note that it is planned to encapsulate this detail in another object.
     */
    public String toXML() {
    		/* Modified by Amarnadh for issue #3208 28th Nov '07 */ 
        return new String(
                "<?xml version=\"1.0\"?>"
                        + "<?cocoon-process type=\"xslt\"?>"
                        + "<?xml-stylesheet href=\"/stylesheet/form-html.xsl\" type=\"text/xsl\"?>"
                        + "<?xml-stylesheet href=\"/stylesheet/form-wml.xsl\" type=\"text/xsl\" media=\"wap\"?>"
                        + "<form action=\"studysummary.jsp\">"
                        + "<head>"
                        + "<title>Study Summary </title>"
                        + "</head>"
                        + "<input type=\"hidden\" name=\"id\" value=\""
                        + this.getId()
                        + "\" size=\"10\"/>"
                        + "<input type=\"hidden \" name=\"account\" value=\""
                        + this.getAccount()
                        + "\" size=\"10\"/>"
                        + "<input type=\"text\" name=\" studyActBeginDate\" value=\""
                        + this.getStudyActBeginDate()
                        + "\" size=\"12\"/>"
                        + "<input type=\"text\" name=\" studyAuthor\" value=\""
                        + this.getStudyAuthor()
                        + "\" size=\"30\"/>"
                        + "<input type=\"text\" name=\" studyBlinding\" value=\""
                        + this.getStudyBlinding()
                        + "\" size=\"20\"/>"
                        + "<input type=\"text\" name=\" studyStudyContact\" value=\""
                        + this.getStudyContact()
                        + "\" size=\"20\"/>"
                        + "<input type=\"text\" name=\" studyDuration\" value=\""
                        + this.getStudyDuration()
                        + "\" size=\"10\"/>"
                        + "<input type=\"text\" name=\" studyDurationUnit\" value=\""
                        + this.getStudyDurationUnit()
                        + "\" size=\"10\"/>"
                        + "<input type=\"text\" name=\" studyEstBeginDate\" value=\""
                        + this.getStudyEstBeginDate()
                        + "\" size=\"12\"/>"
                        + "<input type=\"text\" name=\" studyKeywords \" value=\""
                        + this.getStudyKeywords()
                        + "\" size=\"50\"/>"
                        + "<input type=\"text\" name=\" studyNumber \" value=\""
                        + this.getStudyNumber()
                        + "\" size=\"15\"/>"
                        + "<input type=\"text\" name=\" studyParent \" value=\""
                        + this.getStudyParent()
                        + "\" size=\"10\"/>"
                        + "<input type=\"text\" name=\" studyPartCenters \" value=\""
                        + this.getStudyPartCenters()
                        + "\" size=\"30\"/>"
                        + "<input type=\"text\" name=\" studyPrimInv \" value=\""
                        + this.getStudyPrimInv()
                        + "\" size=\"30\"/>"
                        + "<input type=\"text\" name=\" studyPrimInv \" value=\""
                        + this.getStudyCoordinator()
                        + "\" size=\"30\"/>"
                        + "<input type=\"text\" name=\" studyProduct\" value=\""
                        + this.getStudyProduct()
                        + "\" size=\"30\"/>"
                        + "<input type=\"text\" name=\" studyPubCol \" value=\""
                        + this.getStudyPubCol()
                        + "\" size=\"40\"/>"
                        + "<input type=\"text\" name=\" studyPubFlag\" value=\""
                        + this.getStudyPubFlag()
                        + "\" size=\"10\"/>"
                        + "<input type=\"text\" name=\" studyRandom\" value=\""
                        + this.getStudyRandom()
                        + "\" size=\"20\"/>"
                        + "<input type=\"text\" name=\" studyResType\" value=\""
                        + this.getStudyResType()
                        + "<input type=\"text\" name=\" studyPurpose\" value=\""
                        + this.getStudyPurpose()
                        + "\" size=\"20\"/>"
                        + "<input type=\"text\" name=\" studySize\" value=\""
                        + this.getStudySize()
                        + "\" size=\"10\"/>"
                        + "<input type=\"text\" name=\" studySponsor\" value=\""
                        + this.getStudySponsor()
                        + "\" size=\"15\"/>"
                        + "<input type=\"text\" name=\" studyScope\" value=\""
                        + this.getStudyScope()
                        + "\" size=\"20\"/>"
                        + "<input type=\"text\" name=\" studySummary\" value=\""                     
                        + this.getStudySummary() + "\" size=\"100\"/>"
                        + "<input type=\"text\" name=\" studyTArea\" value=\""
                        + this.getStudyTArea() + "\" size=\"20\"/>"
                        + "<input type=\"text\" name=\" studyTitle\" value=\""
                        + this.getStudyTitle() + "\" size=\"30\"/>"
                        + "<input type=\"text\" name=\" studyType\" value=\""
                        + this.getStudyType() + "\" size=\"20\"/>" + "</form>");
    }// end of method

    public StudyDao getUserStudiesWithRights(String userId, String dName,
            int selVal, String siteId) {
        StudyDao studyDao = new StudyDao();
        try {
            Rlog.debug("study", "StudyJB.getuserstudies starting");

            StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
            studyDao = studyAgent.getUserStudiesWithRights(userId, dName,
                    selVal, siteId);
            return studyDao;

        } catch (Exception e) {
            Rlog.fatal("study", "Error in getUserStudiesWithRights in StudyJB "
                    + e);
            return studyDao;
        }
    }

    public int getOrgAccessForStudy(int userId, int studyId, int orgId) {
        int count = 0;
        try {

            StudyAgent studyAgent = EJBUtil.getStudyAgentHome();

            count = studyAgent.getOrgAccessForStudy(userId, studyId, orgId);

        } catch (Exception e) {
            Rlog.fatal("study",
                    "Error in getOrgAccessForStudy(String accountId) in StudyJB "
                            + e);
        }
        return count;
    }

    public int getStudyCompleteDetailsAccessRight(int userId, int groupId,
            int studyId) {
        int returnRight = 0;
        try {

            StudyAgent studyAgent = EJBUtil.getStudyAgentHome();

            returnRight = studyAgent.getStudyCompleteDetailsAccessRight(userId,
                    groupId, studyId);

        } catch (Exception e) {

            Rlog.fatal("study",
                    "Error in getStudyCompleteDetailsAccessRight in PersonJB "
                            + e);
        }
        return returnRight;
    }

    // to delete a study
    // km
    public void deleteStudy(int studyId) {
        try {

            StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
            Rlog.debug("study", "StudyJB.deleteStudy after remote");
            studyAgent.deleteStudy(studyId);
            Rlog.debug("study", "StudyJB.deleteStudy after Dao");
        } catch (Exception e) {
            Rlog.fatal("study", "Error in deleteStudy in StudyJB " + e);
        }
    }
    
    // deleteStudy() Overloaded for INF-18183 ::: Raviesh
    public void deleteStudy(int studyId,Hashtable<String, String> args) {
        try {

            StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
            Rlog.debug("study", "StudyJB.deleteStudy after remote");
            studyAgent.deleteStudy(studyId,args);
            Rlog.debug("study", "StudyJB.deleteStudy after Dao");
        } catch (Exception e) {
            Rlog.fatal("study", "Error in deleteStudy in StudyJB " + e);
        }
    }

    // km

    // to delete a Patient from a particular Study
    // km
    public void deletePatStudy(int studyId, int patId) {
        try {

            StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
            Rlog.debug("study", "StudyJB.deletePatStudy after remote");
            studyAgent.deletePatStudy(studyId, patId);
            Rlog.debug("study", "StudyJB.deletePatStudy after Dao");
        } catch (Exception e) {
            Rlog.fatal("study", "Error in deletePatStudy in StudyJB" + e);
        }
    }

    // km   
 
    // Sonia Abrol, added the method to track the user who deleted the study
    public void deleteStudy(int studyId, String deletedBy) {
        try {
        	
        	StudyJB sb = new StudyJB();
        	
        	sb.setId(studyId);
        	sb.getStudyDetails();
        	sb.setModifiedBy(deletedBy);
        	sb.updateStudy();

            StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
            Rlog.debug("study", "StudyJB.deleteStudy after remote");
            studyAgent.deleteStudy(studyId);
            Rlog.debug("study", "StudyJB.deleteStudy after Dao");
        } catch (Exception e) {
            Rlog.fatal("study", "Error in deleteStudy in StudyJB " + e);
        }
    }
    
    // Overloaded for INF-18183 ::: AGodara
    public void deleteStudy(int studyId, String deletedBy,Hashtable<String, String> auditInfo) {
        try {
        	
        	StudyJB sb = new StudyJB();
        	
        	sb.setId(studyId);
        	sb.getStudyDetails();
        	sb.setModifiedBy(deletedBy);
        	sb.updateStudy();

            StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
            Rlog.debug("study", "StudyJB.deleteStudy after remote");
            studyAgent.deleteStudy(studyId,auditInfo);
            Rlog.debug("study", "StudyJB.deleteStudy after Dao");
        } catch (Exception e) {
            Rlog.fatal("study", "Error in deleteStudy in StudyJB " + e);
        }
    }
    
    /* deletes patient from a study and tracks the deleted by user in er_patprot*/
    
    public void deletePatStudy(int studyId, int patId, String deletedBy, String patProtId) {
        try {
        	
        	PatProtJB sb = new PatProtJB ();
        	sb.setPatProtId(EJBUtil.stringToNum(patProtId));
        	
        	sb.getPatProtDetails();
        	sb.setModifiedBy(deletedBy);
        	sb.updatePatProt();


            StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
            Rlog.debug("study", "StudyJB.deletePatStudy after remote");
            studyAgent.deletePatStudy(studyId, patId);
            Rlog.debug("study", "StudyJB.deletePatStudy after Dao");
        } catch (Exception e) {
            Rlog.fatal("study", "Error in deletePatStudy in StudyJB" + e);
        }
    }
 // Overloaded for INF-18183 ::: AGodara
    public void deletePatStudy(int studyId, int patId, String deletedBy, String patProtId,Hashtable<String, String> auditInfo) {
        try {
        	
        	PatProtJB sb = new PatProtJB ();
        	sb.setPatProtId(EJBUtil.stringToNum(patProtId));
        	
        	sb.getPatProtDetails();
        	sb.setModifiedBy(deletedBy);
        	sb.updatePatProt();


            StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
            Rlog.debug("study", "StudyJB.deletePatStudy after remote");
            studyAgent.deletePatStudy(studyId, patId,auditInfo);
            Rlog.debug("study", "StudyJB.deletePatStudy after Dao");
        } catch (Exception e) {
            Rlog.fatal("study", "Error in deletePatStudy in StudyJB" + e);
        }
    }
    
    public StudyDao getStudyAutoRows(String userId ,String actId, String studyIds, int criterion) {
    	try {
    		StudyDao studyDao = new StudyDao();
            StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
            studyDao = studyAgent.getStudyAutoRows(userId, actId, studyIds, criterion);
            return studyDao;
        } catch (Exception e) {
            Rlog.fatal("study", "Error in getStudyAutoRows in StudyJB " + e);
            return null;
        }
    }

	/**
	 * @return the ctrpAccrualLastGenerationDate
	 */
	public String getCtrpAccrualLastGenerationDate() {
		return ctrpAccrualLastGenerationDate;
	}

	/**
	 * @param ctrpAccrualLastGenerationDate the ctrpAccrualLastGenerationDate to set
	 */
	public void setCtrpAccrualLastGenerationDate(
			String ctrpAccrualLastGenerationDate) {
		this.ctrpAccrualLastGenerationDate = ctrpAccrualLastGenerationDate;
	}
	/**
	 * @return the cutOffDate
	 */
	public String getCutOffDate() {
		return cutOffDate;
	}
	
	/**
	 * @param cutOffDate the cutOffDate to set
	 */
	public void setCutOffDate(
			String cutOffDate) {
		this.cutOffDate = cutOffDate;
	}
	/**
	 * @return the lastDownloadBy
	 */
	public String getLastDownloadBy() {
		return lastDownloadBy;
	}
	
	/**
	 * @param cutOffDate the cutOffDate to set
	 */
	public void setLastDownloadBy(
			String lastDownloadBy) {
		this.lastDownloadBy = lastDownloadBy;
	}
	
	public String getStudyIsLocked() {
		return studyIsLocked;
	}

	public void setStudyIsLocked(String studyIsLocked) {
		this.studyIsLocked = studyIsLocked;
	}

	public String getStudyIsBlocked() {
		return studyIsBlocked;
	}

	public void setStudyIsBlocked(String studyIsBlocked) {
		this.studyIsBlocked = studyIsBlocked;
	}

    public HashMap<String, Object> getStudySysData(HttpServletRequest request) {
    	HashMap<String, Object> retHash = new HashMap<String, Object>();
		int pageRight = 0;

		try {
    		HttpSession tSession = request.getSession(true);
    		String strAccId = (String) tSession.getAttribute("accountId");
    		if (StringUtil.isEmpty(strAccId)) return null;

    		AccountJB accJB = new AccountJB();
    		//Added by Manimaran for the Enh.#GL7.
			accJB.setAccId(StringUtil.stringToNum(strAccId));
			accJB.getAccountDetails();
			String autoGenStudy = accJB.getAutoGenStudy();
			if (autoGenStudy == null)
				autoGenStudy ="0";
			retHash.put("autoGenStudy", autoGenStudy);

			int grpRight = 0;
			//copy a new study is controlled by Manage Protocols new group right
			GrpRightsJB grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
	   		grpRight = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));
	   		retHash.put("grpRight", ""+grpRight);

			String strUserId = (String) tSession.getAttribute("userId");

			String mode = request.getParameter("mode");
			if (StringUtil.isEmpty(mode)) {
				mode = StringUtil.stringToNum(request.getParameter("studyId")) > 0 ? "M" : "N";
			}
			retHash.put("mode", mode);
			
			int studyId = 0;
			if ("M".equals(mode)) {
				studyId = StringUtil.stringToNum(request.getParameter("studyId"));
			}
			
			if ("M".equals(mode)){
				String roleCodePk ="";

				TeamDao teamDao = new TeamDao();
				teamDao.getTeamRights(studyId,StringUtil.stringToNum(strUserId));

				ArrayList tId = new ArrayList();
				tId = teamDao.getTeamIds();

				StudyRightsJB studyRightsJB = new StudyRightsJB();
				if (tId.size() <=0){
					pageRight  = 0;
					retHash.put("studyRights", studyRightsJB);
					retHash.put("studyRoleCodePk", roleCodePk);
				} else {
					studyId = StringUtil.stringToNum(request.getParameter("studyId"));
					retHash.put("studyId", ""+studyId);

					studyRightsJB.setId(StringUtil.stringToNum(tId.get(0).toString()));
	
					ArrayList teamRights = new ArrayList();
					teamRights = teamDao.getTeamRights();
	
					ArrayList arRoles = new ArrayList();
					arRoles = teamDao.getTeamRoleIds();
	
					if (arRoles != null && arRoles.size() >0 ){
						roleCodePk = (String) arRoles.get(0);
						if (StringUtil.isEmpty(roleCodePk)){
							roleCodePk="";
						}
					} else {
						roleCodePk ="";
					}
	
					studyRightsJB.setSuperRightsStringForStudy((String)teamRights.get(0));
					studyRightsJB.loadStudyRights();

					retHash.put("studyRights", studyRightsJB);
					retHash.put("studyRoleCodePk", roleCodePk);
					
					int stdSummRight =0;
					if ((studyRightsJB.getFtrRights().size()) == 0){
						stdSummRight= 0;
					}else{
						stdSummRight = Integer.parseInt(studyRightsJB.getFtrRightsByValue("STUDYSUM"));
					}
					retHash.put("stdSummRight", stdSummRight);	
				}
				//studyRightsJB = (StudyRightsJB) tSession.getAttribute("studyRights");
	
				if (tId.size() > 0){
					pageRight = Integer.parseInt(studyRightsJB.getFtrRightsByValue("STUDYSUM"));
				}

				StudyJB studyJB = new StudyJB();
				studyJB.setId(studyId);
				studyJB.getStudyDetails();
				String studyNumber = "";
				studyNumber = studyJB.getStudyNumber();
				if (studyNumber == null ) studyNumber ="";
				retHash.put("studyNo", studyNumber);
	
				String studyVerParent = studyJB.getStudyVersionParent();
				if (studyVerParent == null)	studyVerParent = "";
				retHash.put("studyVerParent", studyVerParent);

				String studyDiv = studyJB.getStudyDivision();
				if (StringUtil.isEmpty(studyDiv)){
					studyDiv = "";
				}
				retHash.put("studyDiv", studyDiv);
				
				String studyPurpse = studyJB.getStudyPurpose();
				if (StringUtil.isEmpty(studyPurpse)){
					studyPurpse = "";
				}
				retHash.put("studyPurpse", studyPurpse);
				
				String studyInvFlag = studyJB.getStudyInvIndIdeFlag();
				if (studyInvFlag == null) studyInvFlag = "";
				retHash.put("studyInvFlag", studyInvFlag);

				String studyInvNum = studyJB.getStudyIndIdeNum();
				if (studyInvNum == null) studyInvNum = "";
				retHash.put("studyInvNum", studyInvNum);

				String ctrpRepFlag = studyJB.getStudyCtrpReportable();
				if (ctrpRepFlag == null) ctrpRepFlag = "";
				retHash.put("ctrpRepFlag", ctrpRepFlag);

				String fdaRegStdFlag = studyJB.getFdaRegulatedStudy();			
				if (fdaRegStdFlag == null) fdaRegStdFlag = "";
				retHash.put("fdaRegStdFlag", fdaRegStdFlag);

				String ccsgdtFlag = studyJB.getCcsgReportableStudy();			
				if (ccsgdtFlag == null) ccsgdtFlag = "";
				retHash.put("ccsgdtFlag", ccsgdtFlag);

				String studyT = "";
				studyT = studyJB.getStudyTArea();
				if (studyT == null)
					studyT = "";
				retHash.put("studyTArea", studyT);
			} else {
				retHash.put("studyId", "");
				retHash.put("studyNo", "");
				retHash.put("StudyRights", null);
		   		pageRight = grpRight;
			}
			
			retHash.put("pageRight", pageRight);
		} catch (Exception e) {
    		Rlog.fatal("studyJB", 
            		"Exception in getStudyData in StudyJB "+e);
    		retHash = null;
    	}
    	return retHash;
    }

}// end of class

