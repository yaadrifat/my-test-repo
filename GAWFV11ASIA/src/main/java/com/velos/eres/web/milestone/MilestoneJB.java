/*
 * Classname : MilestoneJB
 *
 * Version information: 1.0
 *
 * Date: 05/28/2002
 *
 * Copyright notice: Velos, Inc
 */

package com.velos.eres.web.milestone;

/* Import Statements */
import java.sql.Date;
import java.util.HashMap;
import java.util.Hashtable;

import com.velos.eres.business.common.MileAchievedDao;
import com.velos.eres.business.common.MilestoneDao;
import com.velos.eres.business.milestone.impl.MilestoneBean;
import com.velos.eres.service.milestoneAgent.MilestoneAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/* End of Import Statements */

/**
 * Client side bean for milestone
 *
 * @author Sonika Talwar
 * @version 1.0 05/28/2002
 */

public class MilestoneJB {
    /**
     * the Milestone Id
     */
    private int id;

    /**
     * the Study Id to which Milestone is related
     */
    private String milestoneStudyId;

    /**
     * the Milestone Type
     */
    private String milestoneType;

    /**
     * the Milestone Calendar
     */
    private String milestoneCalId;

    /**
     * the Milestone Rule
     */
    private String milestoneRuleId;

    /**
     * the Milestone Amount
     */
    private String milestoneAmount;

    /**
     * the Milestone Visit
     */
    private String milestoneVisit;

    /**
     * the Milestone Event
     */
    private String milestoneEvent;

    /**
     * the Milestone Count
     */
    private String milestoneCount;

    /**
     * the Milestone Delflag
     */
    private String milestoneDelFlag;

    /**
     * the Milestone UserTo
     */
    private String milestoneUsersTo;

    /*
     * creator
     */
    private String creator;

    /*
     * last modified by
     */
    private String modifiedBy;

    /*
     * IP Address
     */
    private String ipAdd;

    /**
     * the Milestone Visit Foreign Key
     */
    private String milestoneVisitFK;

    /**
     * Milestone Limit
     */
    private String milestoneLimit;


    /**
     * Milestone Rule Status type
     */
    private String milestoneStatus ;


    /**
     * Milestone Rule Payment type
     */
    private String milestonePayType ;

    /**
     * Milestone Rule Payment Due by number
     */
    private String milestonePaydDueBy ;

    /**
     * Milestone Rule Payment Due by Unit
     */
    private String milestonePayByUnit ;

    /**
     * Milestone Rule Payment For
     */
    private String  milestonePayFor;

    /**
     * Milestone Rule Event Status
     */
    private String  milestoneEventStatus;

    /**
     * Milestone Rule Achieved Count
     */
    private String  milestoneAchievedCount;


    //Added by Manimaran for Additional Milestone.

    /**
     * Description for Additional Milestone
     */
    private String milestoneDescription;

    /**
     * The Milestone Status Foreign Key
     */

    private String milestoneStatFK;
    
    /**
     * The Milestone Date From
     */

    private String dateFrom;
    
    /**
     * The Milestone Date To
     */

    private String dateTo;
	/**
     * The Milestone holdBack
     */

    private Float holdBack;

    // GETTER SETTER METHODS

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMilestoneAmount() {
        return this.milestoneAmount;
    }

    public void setMilestoneAmount(String milestoneAmount) {
        this.milestoneAmount = milestoneAmount;
    }

    public String getMilestoneCalId() {
        return this.milestoneCalId;
    }

    public void setMilestoneCalId(String milestoneCalId) {
        this.milestoneCalId = milestoneCalId;
    }

    public String getMilestoneCount() {
        return this.milestoneCount;
    }

    public void setMilestoneCount(String milestoneCount) {
        this.milestoneCount = milestoneCount;
    }

    public String getMilestoneDelFlag() {
        return this.milestoneDelFlag;
    }

    public void setMilestoneDelFlag(String milestoneDelFlag) {
        this.milestoneDelFlag = milestoneDelFlag;
    }

    public String getMilestoneEvent() {
        return this.milestoneEvent;
    }

    public void setMilestoneEvent(String milestoneEvent) {
        this.milestoneEvent = milestoneEvent;
    }

    public String getMilestoneRuleId() {
        return this.milestoneRuleId;
    }

    public void setMilestoneRuleId(String milestoneRuleId) {
        this.milestoneRuleId = milestoneRuleId;
    }

    public String getMilestoneStudyId() {
        return this.milestoneStudyId;
    }

    public void setMilestoneStudyId(String milestoneStudyId) {
        this.milestoneStudyId = milestoneStudyId;
    }

    public String getMilestoneType() {
        return this.milestoneType;
    }

    public void setMilestoneType(String milestoneType) {
        this.milestoneType = milestoneType;
    }

    public String getMilestoneUsersTo() {
        return this.milestoneUsersTo;
    }

    public void setMilestoneUsersTo(String milestoneUsersTo) {
        this.milestoneUsersTo = milestoneUsersTo;
    }

    public String getMilestoneVisit() {
        return this.milestoneVisit;
    }

    public void setMilestoneVisit(String milestoneVisit) {
        this.milestoneVisit = milestoneVisit;
    }

    public String getModifiedBy() {
        return this.modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getCreator() {
        return this.creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    public String getMilestoneVisitFK() {
        return this.milestoneVisitFK;
    }

    public void setMilestoneVisitFK(String milestoneVisitFK) {
        this.milestoneVisitFK = milestoneVisitFK;
    }



    public String getMilestoneAchievedCount() {
		return milestoneAchievedCount;
	}

	public void setMilestoneAchievedCount(String milestoneAchievedCount) {
		this.milestoneAchievedCount = milestoneAchievedCount;
	}

	public String getMilestoneEventStatus() {
		return milestoneEventStatus;
	}

	public void setMilestoneEventStatus(String milestoneEventStatus) {
		this.milestoneEventStatus = milestoneEventStatus;
	}

	public String getMilestoneLimit() {
		return milestoneLimit;
	}

	public void setMilestoneLimit(String milestoneLimit) {
		this.milestoneLimit = milestoneLimit;
	}

	public String getMilestonePayByUnit() {
		return milestonePayByUnit;
	}

	public void setMilestonePayByUnit(String milestonePayByUnit) {
		this.milestonePayByUnit = milestonePayByUnit;
	}

	public String getMilestonePaydDueBy() {
		return milestonePaydDueBy;
	}

	public void setMilestonePaydDueBy(String milestonePaydDueBy) {
		this.milestonePaydDueBy = milestonePaydDueBy;
	}

	public String getMilestonePayFor() {
		return milestonePayFor;
	}

	public void setMilestonePayFor(String milestonePayFor) {
		this.milestonePayFor = milestonePayFor;
	}

	public String getMilestonePayType() {
		return milestonePayType;
	}

	public void setMilestonePayType(String milestonePayType) {
		this.milestonePayType = milestonePayType;
	}

	public String getMilestoneStatus() {
		return milestoneStatus;
	}

	public void setMilestoneStatus(String milestoneStatus) {
		this.milestoneStatus = milestoneStatus;
	}

	//KM
	public String getMilestoneDescription() {
		return milestoneDescription;
	}

	public void setMilestoneDescription(String milestoneDescription) {
		this.milestoneDescription = milestoneDescription;
	}

	//KM-D-FIN7
	public String getMilestoneStatFK() {
        return this.milestoneStatFK;
    }

    public void setMilestoneStatFK(String milestoneStatFK) {
        this.milestoneStatFK = milestoneStatFK;
    }
    
    //Fin-22373 28-Sep-2012  Ankit
	public String getDateFrom() {
        return this.dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }
    
	public String getDateTo() {
        return this.dateTo;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }
	
	public Float getHoldBack() {
		return holdBack;
	}

	public void setHoldBack(Float holdBack) {
		this.holdBack = holdBack;
	}

    // end of getter and setter methods

	/**
     * Constructor
     *
     * @param milestoneId
     */
    public MilestoneJB(int milestoneId) {
        setId(milestoneId);
    }

    /**
     * Default Constructor
     *
     */
    public MilestoneJB() {
        Rlog.debug("milestone", "milestoneJB.milestoneJB() ");
    }

    /**
     * Full Argument Constructor
     */
    public MilestoneJB(int id, String milestoneStudyId, String milestoneType,
            String milestoneCalId, String milestoneRuleId,
            String milestoneAmount, String milestoneVisit,
            String milestoneEvent, String milestoneCount,
            String milestoneDelFlag, String milestoneUsersTo, String creator,
            String modifiedBy, String ipAdd, String milestoneVisitFK,
            String milestoneAchievedCount,String milestoneEventStatus,
            String milestoneLimit,String milestonePayByUnit,
            String milestonePaydDueBy,  String milestonePayFor, String milestonePayType,String milestoneStatus,
            String milestoneDescription, String milestoneStatFK, Float holdBack) {

        setId(id);
        setMilestoneStudyId(milestoneStudyId);
        setMilestoneType(milestoneType);
        setMilestoneCalId(milestoneCalId);
        setMilestoneRuleId(milestoneRuleId);
        setMilestoneAmount(milestoneAmount);
        setMilestoneVisit(milestoneVisit);
        setMilestoneEvent(milestoneEvent);
        setMilestoneCount(milestoneCount);
        setMilestoneDelFlag(milestoneDelFlag);
        setMilestoneUsersTo(milestoneUsersTo);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setMilestoneVisitFK(milestoneVisitFK); // SV, 11/1
        setMilestoneAchievedCount(milestoneAchievedCount) ;
        setMilestoneEventStatus( milestoneEventStatus);
        setMilestoneLimit( milestoneLimit) ;
        setMilestonePayByUnit( milestonePayByUnit) ;
        setMilestonePaydDueBy( milestonePaydDueBy) ;
        setMilestonePayFor( milestonePayFor) ;
        setMilestonePayType( milestonePayType) ;
        setMilestoneStatus( milestoneStatus);
        setMilestoneDescription( milestoneDescription);
        setMilestoneStatFK(milestoneStatFK);
		setHoldBack(holdBack);
        Rlog.debug("milestone", "milestoneJB.milestoneJB(all parameters)");
    }

    /**
     * Calls getMilestoneDetails of milestone Session Bean: milestoneAgentBean
     *
     * @returns milestoneStateKeeper object
     * @see milestoneAgentBean
     */
    public MilestoneBean getMilestoneDetails() {
        MilestoneBean msk = null;

        try {

            MilestoneAgentRObj milestoneAgent = EJBUtil.getMilestoneAgentHome();
            msk = milestoneAgent.getMilestoneDetails(this.id);
            Rlog.debug("milestone",
                    "milestoneJB.getMilestoneDetails() milestoneStateKeeper "
                            + msk);
        } catch (Exception e) {
            Rlog.debug("milestone", "Error in getMilestoneDetails() in UserJB "
                    + e);
        }
        if (msk != null) {
            this.id = msk.getId();
            this.milestoneAmount = msk.getMilestoneAmount();
            this.milestoneCalId = msk.getMilestoneCalId();
            this.milestoneCount = msk.getMilestoneCount();
            this.milestoneDelFlag = msk.getMilestoneDelFlag();
            this.milestoneEvent = msk.getMilestoneEvent();
            this.milestoneRuleId = msk.getMilestoneRuleId();
            this.milestoneStudyId = msk.getMilestoneStudyId();
            this.milestoneType = msk.getMilestoneType();
            this.milestoneUsersTo = msk.getMilestoneUsersTo();
            this.milestoneVisit = msk.getMilestoneVisit();
            this.creator = msk.getCreator();
            this.modifiedBy = msk.getModifiedBy();
            this.ipAdd = msk.getIpAdd();
            this.milestoneVisitFK = msk.getMilestoneVisitFK();
            
            this.dateFrom= msk.getMilestoneDateFrom();
            this.dateTo = msk.getMilestoneDateTo();
			this.holdBack=msk.getHoldBack();
            setMilestoneAchievedCount(msk.getMilestoneAchievedCount()) ;
            setMilestoneEventStatus( msk.getMilestoneEventStatus());
            setMilestoneLimit( msk.getMilestoneLimit()) ;
            setMilestonePayByUnit( msk.getMilestonePayByUnit()) ;
            setMilestonePaydDueBy( msk.getMilestonePaydDueBy()) ;
            setMilestonePayFor( msk.getMilestonePayFor()) ;
            setMilestonePayType( msk.getMilestonePayType()) ;
            setMilestoneStatus( msk.getMilestoneStatus());
            setMilestoneDescription(msk.getMilestoneDescription());
            setMilestoneStatFK(msk.getMilestoneStatFK());
            setDateFrom(dateFrom);
            setDateTo(dateTo);
			setHoldBack(holdBack);

        }
        return msk;
    }

    /**
     * Calls setmilestoneDetails of milestone Session Bean: milestoneAgentBean
     *
     * @see milestoneAgentBean
     */
    public int setMilestoneDetails() {
        int ret = 0;
        try {

            MilestoneAgentRObj milestoneAgent = EJBUtil.getMilestoneAgentHome();
            Rlog.debug("milestone", "milestoneJB.setMilestoneDetails() line 3");
            this.setId(milestoneAgent.setMilestoneDetails(this
                    .createMilestoneStateKeeper()));
            Rlog.debug("milestone", "milestoneJB.setMilestoneDetails() line 4");
            return ret;
        } catch (Exception e) {
            Rlog.debug("milestone",
                    "Error in setMilestoneDetails() in milestoneJB " + e);
            return -2;
        }
    }

    /**
     * Calls updateDetails of milestone Session Bean: milestoneAgentBean
     *
     * @return int 0 if successful, -2 for exception
     * @see milestoneAgentBean
     */
    public int updateMilestone() {
        int output = 0;
        try {
            Rlog.debug("milestone", "milestoneJB.updateMilestone line 1");

            MilestoneAgentRObj milestoneAgentRObj = EJBUtil
                    .getMilestoneAgentHome();
            Rlog.debug("milestone", "milestoneJB.updateMilestone line 3");
            output = milestoneAgentRObj.updateMilestone(this
                    .createMilestoneStateKeeper());
        } catch (Exception e) {
            Rlog.fatal("milestone", "milestoneJB.updateMilestone eEXCEPTION"
                    + e);
            return -2;
        }
        return output;
    }

    // updateMilestone() Overloaded for INF-18183 ::: Raviesh
    public int updateMilestone(Hashtable<String, String> args) {
        int output = 0;
        try {
            Rlog.debug("milestone", "milestoneJB.updateMilestone line 1");

            MilestoneAgentRObj milestoneAgentRObj = EJBUtil
                    .getMilestoneAgentHome();
            Rlog.debug("milestone", "milestoneJB.updateMilestone line 3");
            output = milestoneAgentRObj.updateMilestone(this
                    .createMilestoneStateKeeper(),args);
        } catch (Exception e) {
            Rlog.fatal("milestone", "milestoneJB.updateMilestone eEXCEPTION"
                    + e);
            return -2;
        }
        return output;
    }
    
    /**
     * Creates a milestoneStateKeeper object with the attribute vales of the
     * bean
     *
     * @return milestoneStateKeeper
     */
    public MilestoneBean createMilestoneStateKeeper() {


    	 Rlog.debug("milestone", "MilestoneJB.createMilestoneStateKeeper starting");
    	 MilestoneBean mb = new MilestoneBean(id, milestoneStudyId, milestoneType, milestoneCalId, milestoneRuleId, milestoneAmount,milestoneVisit, milestoneEvent, milestoneCount,
                milestoneDelFlag, milestoneUsersTo, creator, modifiedBy, ipAdd,
                milestoneVisitFK , milestoneAchievedCount, milestoneEventStatus,
                milestoneLimit,milestonePayByUnit,
                milestonePaydDueBy,  milestonePayFor, milestonePayType, milestoneStatus ,
                milestoneDescription, milestoneStatFK,dateFrom,dateTo,holdBack);

        Rlog.debug("milestone", "MilestoneJB.createMilestoneStateKeeper created");
        return mb ;
    }

    public MilestoneDao getMilestoneRows(int studyId) {
        MilestoneDao milestoneDao = new MilestoneDao();
        try {
            Rlog.debug("milestone", "MilestoneJB.getMilestoneRows starting");

            MilestoneAgentRObj milestoneAgentRObj = EJBUtil
                    .getMilestoneAgentHome();
            milestoneDao = milestoneAgentRObj.getMilestoneRows(studyId);
            Rlog.debug("milestone", "MilestoneJB.getMilestoneRows after Dao");
            return milestoneDao;
        } catch (Exception e) {
            Rlog.fatal("milestone",
                    "Error in getMilestoneRows(int studyId) in MilestoneJB "
                            + e);
            return null;
        }
    }

    public MilestoneDao getMilestoneRows(int studyId, String mileStoneType, String payType,String milestoneReceivableStatus){
        MilestoneDao milestoneDao = new MilestoneDao();
        try {
            Rlog.debug("milestone", "MilestoneJB.getMilestoneRows starting");

            MilestoneAgentRObj milestoneAgentRObj = EJBUtil
                    .getMilestoneAgentHome();
            milestoneDao = milestoneAgentRObj.getMilestoneRows(studyId,  mileStoneType,  payType, milestoneReceivableStatus);
            Rlog.debug("milestone", "MilestoneJB.getMilestoneRows after Dao");
            return milestoneDao;
        } catch (Exception e) {
            Rlog.fatal("milestone",
                    "Error in getMilestoneRows(int studyId, String mileStoneType, String payType,String milestoneReceivableStatus) in MilestoneJB "
                            + e);
            return null;
        }
    }

    //JM: 20MAR2008: #FIN13
    public MilestoneDao getMilestoneRows(int studyId, String mileStoneType, String payType,String milestoneReceivableStatus, String selStatus, String selMilePayType, String srchStrCalName, String srchStrVisitName, String srchStrEvtName, String inActiveFlag, String orderBy, String orderType){
        MilestoneDao milestoneDao = new MilestoneDao();
        try {
            Rlog.debug("milestone", "MilestoneJB.getMilestoneRows starting");

            MilestoneAgentRObj milestoneAgentRObj = EJBUtil
                    .getMilestoneAgentHome();
            milestoneDao = milestoneAgentRObj.getMilestoneRows(studyId,  mileStoneType,  payType, milestoneReceivableStatus, selStatus, selMilePayType, srchStrCalName, srchStrVisitName, srchStrEvtName, inActiveFlag,  orderBy, orderType);
            Rlog.debug("milestone", "MilestoneJB.getMilestoneRows after Dao");
            return milestoneDao;
        } catch (Exception e) {
            Rlog.fatal("milestone",
                    "Error in getMilestoneRows(int studyId, String mileStoneType, String payType,String milestoneReceivableStatus) in MilestoneJB "
                            + e);
            return null;
        }
    }

    public MilestoneDao getMilestoneRowsForStudy(int studyId){
    	MilestoneDao milestoneDao = new MilestoneDao();
        try {
            Rlog.debug("milestone", "MilestoneJB.getMilestoneRowsForStudy starting");

            MilestoneAgentRObj milestoneAgentRObj = EJBUtil
                    .getMilestoneAgentHome();
            milestoneDao = milestoneAgentRObj.getMilestoneRowsForStudy(studyId);
            Rlog.debug("milestone", "MilestoneJB.getMilestoneRowsForStudy after Dao");
            return milestoneDao;
        } catch (Exception e) {
            Rlog.fatal("milestone",
                    "Error in getMilestoneRowsForStudy(int studyId) in MilestoneJB "
                            + e);
            return null;
        }
    }
    /*Added for INF-22500 by Yogendra Pratap Singh 24/01/13*/
    public MilestoneDao getMilestoneRowsForSearch(int studyId,HashMap<String,String> hMap){
    	MilestoneDao milestoneDao = new MilestoneDao();
        try {
            Rlog.debug("milestone", "MilestoneJB.getMilestoneRowsForSearch starting");

            MilestoneAgentRObj milestoneAgentRObj = EJBUtil
                    .getMilestoneAgentHome();
            milestoneDao = milestoneAgentRObj.getMilestoneRowsForSearch( studyId, hMap);
            Rlog.debug("milestone", "MilestoneJB.getMilestoneRowsForSearch after Dao");
            return milestoneDao;
        } catch (Exception e) {
            Rlog.fatal("milestone",
                    "Error in getMilestoneRowsForSearch in MilestoneJB "
                            + e);
            return null;
        }
    }
    
    /*YK 29Mar2011 -DFIN20 */
    public MilestoneDao getStudyMileStoneForSetStatus(int studyId){
    	MilestoneDao milestoneDao = new MilestoneDao();
    	try {
    		Rlog.debug("milestone", "getStudyMileStoneForSetStatus starting");
    		
    		MilestoneAgentRObj milestoneAgentRObj = EJBUtil
    		.getMilestoneAgentHome();
    		milestoneDao = milestoneAgentRObj.getStudyMileStoneForSetStatus(studyId);
    		Rlog.debug("milestone", "MilestoneJB.getStudyMileStoneForSetStatus after Dao");
    		return milestoneDao;
    	} catch (Exception e) {
    		Rlog.fatal("milestone",
    				"Error in getStudyMileStoneForSetStatus(int studyId) in MilestoneJB "
    				+ e);
    		return null;
    	}
    }

  //JM: 20MAR2008:



    public MilestoneDao getEventsForNew(int studyId, int calId) {
        MilestoneDao milestoneDao = new MilestoneDao();
        try {
            Rlog.debug("milestone", "MilestoneJB.getEventsForNew starting");

            MilestoneAgentRObj milestoneAgentRObj = EJBUtil
                    .getMilestoneAgentHome();
            milestoneDao = milestoneAgentRObj.getEventsForNew(studyId, calId);
            Rlog.debug("milestone", "MilestoneJB.getEventsForNew after Dao");
            return milestoneDao;
        } catch (Exception e) {
            Rlog.fatal("milestone",
                    "Error in getEventsForNew(int studyId,int calId) in MilestoneJB "
                            + e);
            return null;
        }
    }

    public MilestoneDao getEventsForNewVisitMilestone(int studyId, int calId) {
        MilestoneDao milestoneDao = new MilestoneDao();
        try {
            Rlog.debug("milestone",
                    "MilestoneJB.getEventsForNewVisitMilestone starting");

            MilestoneAgentRObj milestoneAgentRObj = EJBUtil
                    .getMilestoneAgentHome();
            milestoneDao = milestoneAgentRObj.getEventsForNewVisitMilestone(
                    studyId, calId);
            Rlog.debug("milestone",
                    "MilestoneJB.getEventsForNewVisitMilestone after Dao");
            return milestoneDao;
        } catch (Exception e) {
            Rlog.fatal("milestone",
                    "Error in getEventsForNewVisitMilestone(int studyId,int calId) in MilestoneJB "
                            + e);
            return null;
        }
    }

    /**
     * Calls setmilestoneDetails of milestone Session Bean: milestoneAgentBean
     *
     * @see milestoneAgentBean
     */
    public int setMilestoneDetails(MilestoneDao mdo, String creator,
            String ipAdd) {
        int ret = 0;
        try {

            MilestoneAgentRObj milestoneAgent = EJBUtil.getMilestoneAgentHome();

            milestoneAgent.setMilestoneDetails(mdo, creator, ipAdd);
            Rlog.debug("milestone",
                    "milestoneJB.setMilestoneDetails() after set");
            return ret;
        } catch (Exception e) {
            Rlog.debug("milestone",
                    "Error in setMilestoneDetails(dao,usr,ip) in milestoneJB "
                            + e);
            return -2;
        }
    }


    /**
     * Gets Achieved milestones
     *
     * @param mileStoneId
     *            mileStoneId
     */
    public MileAchievedDao getAchievedMilestones(int mileStoneId , String user) {
    	MileAchievedDao mile = new MileAchievedDao();
        try {
            Rlog.debug("milestone",
                    "MilestoneJB.getAchievedMilestones starting");

            MilestoneAgentRObj milestoneAgentRObj = EJBUtil
                    .getMilestoneAgentHome();
            mile = milestoneAgentRObj.getAchievedMilestones(
            		mileStoneId,user);
            Rlog.debug("milestone",
                    "MilestoneJB.getAchievedMilestones after Dao");
            return mile;
        } catch (Exception e) {
            Rlog.fatal("milestone",
                    "Error in getAchievedMilestones(int milestoneId) in MilestoneJB "
                            + e);
            return null;
        }
    }


    /* Sonia Abrol, 12/07/06
	 * Apply user notification settings of a milestone to all milestones in a study*/
    public int applyNotificationSettingsToAll(String milestoneID, String userIds, String studyId, String modifiedBy, String ipAdd )
    {
    	int retValue = -1;

    	try {
            Rlog.debug("milestone",
                    "MilestoneJB.applyNotificationSettingsToAll starting");

            MilestoneAgentRObj milestoneAgentRObj = EJBUtil
                    .getMilestoneAgentHome();
            retValue = milestoneAgentRObj.applyNotificationSettingsToAll( milestoneID, userIds,studyId, modifiedBy, ipAdd );
            Rlog.debug("milestone",
                    "MilestoneJB.applyNotificationSettingsToAll after Dao");

            return retValue;
        } catch (Exception e) {
            Rlog.fatal("milestone",
                    "Error in applyNotificationSettingsToAll(int milestoneId) in MilestoneJB "
                            + e);
            return retValue;
        }

    }

    public int createMultiMilestones(int userId, String ipAdd,MilestoneDao md, int pkbudget, int study,int bgtcalId)
    {
    	int retValue = -1;

    	try {

            MilestoneAgentRObj milestoneAgentRObj = EJBUtil
                    .getMilestoneAgentHome();
            retValue = milestoneAgentRObj.createMultiMilestones(userId, ipAdd,md, pkbudget, study,bgtcalId);


            return retValue;
        } catch (Exception e) {
            Rlog.fatal("milestone",
                    "Exception in createMultiMilestones(int userId, String ipAdd,MilestoneDao md, int pkbudget, int study) in MilestoneJB "
                            + e);
            return retValue;
        }

    }


    /**
     *
     * @author Sonia Abrol
     * date - 08/26/08
     *
     * Gets milestone Achievemnt details
     *
     * @param studyId
     *            study id
     * @param   milestoneType milestone type code
     * @param htParams ashtable of additional parameters (for later use)
     */
    public MileAchievedDao getALLAchievedMilestones(int studyId, String milestoneType , Hashtable htParams)
    {
    	MileAchievedDao mile = new MileAchievedDao();
        try {

            MilestoneAgentRObj milestoneAgentRObj = EJBUtil
                    .getMilestoneAgentHome();
            mile = milestoneAgentRObj.getALLAchievedMilestones(studyId, milestoneType , htParams);

            return mile;
        } catch (Exception e) {
            Rlog.fatal("milestone",
                    "Error in getALLAchievedMilestones  in MilestoneJB "
                            + e);
            return null;
        }
    }


    /** Sonia Abrol, 08/27/08
	 * delete milestione achievement records from er_mileachieved
	 * @param arPkmileAch Array of PKs to be deleted */

    public int deleteMSAch(String[] arPkmileAch )
    {
    	int ret = 0;
        try {

            MilestoneAgentRObj milestoneAgentRObj = EJBUtil
                    .getMilestoneAgentHome();
            ret = milestoneAgentRObj.deleteMSAch(arPkmileAch);

            return ret;
        } catch (Exception e) {
            Rlog.fatal("milestone",
                    "Exception in deleteMSAch  in MilestoneJB "
                            + e);
            e.printStackTrace();
            return -1;
        }
    }
    
    //Overloaded for INF-18183 ::: Ankit
    public int deleteMSAch(String[] arPkmileAch, Hashtable<String, String> args)
    {
    	int ret = 0;
        try {

            MilestoneAgentRObj milestoneAgentRObj = EJBUtil
                    .getMilestoneAgentHome();
            ret = milestoneAgentRObj.deleteMSAch(arPkmileAch,args);

            return ret;
        } catch (Exception e) {
            Rlog.fatal("milestone",
                    "Exception in deleteMSAch  in MilestoneJB "
                            + e);
            e.printStackTrace();
            return -1;
        }
    }
    
	//DFIN-13 BK MAR-16-2011
    
    /**
     * Create milestone records having  parameters in form of JSON array string.
     * @param dataGridString String
     * @return integer     
     */

    public int createMilestonesUsingJSON(String dataGridString)
    {
    	int ret = 0;
        try {

            MilestoneAgentRObj milestoneAgentRObj = EJBUtil
                    .getMilestoneAgentHome();
            this.setId(milestoneAgentRObj.createMilestonesUsingJSON(dataGridString, this
                    .createMilestoneStateKeeper()));      

            return ret;
        } catch (Exception e) {
            Rlog.fatal("milestone",
                    "Exception in deleteMilestonesUsingJSON  in MilestoneJB "
                            + e);
            e.printStackTrace();
            return -1;
        }
    } 

    
    /**
     * update milestone records having  parameters in form of JSON array.
     * @param dataGridString String
     * @param updateString   String
     * @return
     */

    public int updateMilestonesUsingJSON(String dataGridString,String updateString)
    {
    	int ret = 0;
        try {

            MilestoneAgentRObj milestoneAgentRObj = EJBUtil
                    .getMilestoneAgentHome();
            ret = milestoneAgentRObj.updateMilestonesUsingJSON(dataGridString,updateString);

            return ret;
        } catch (Exception e) {
            Rlog.fatal("milestone",
                    "Exception in updateMilestonesUsingJSON  in MilestoneJB "
                            + e);
            e.printStackTrace();
            return -1;
        }
    }

    /**
     * Overloaded Method for setting the Modified By and IP Address
     * update milestone records having  parameters in form of JSON array.
     * @param dataGridString String
     * @param updateString   String
     * @return
     */

    public int updateMilestonesDataUsingJSON(String dataGridString,String updateString)
    {
    	int ret = 0;
        try {

            MilestoneAgentRObj milestoneAgentRObj = EJBUtil
                    .getMilestoneAgentHome();
            ret = milestoneAgentRObj.updateMilestonesUsingJSON(dataGridString,updateString, this
                    .createMilestoneStateKeeper());

            return ret;
        } catch (Exception e) {
            Rlog.fatal("milestone",
                    "Exception in updateMilestonesUsingJSON  in MilestoneJB "
                            + e);
            e.printStackTrace();
            return -1;
        }
    }
    
    /**
     * delete milestone records having  parameters in form of JSON array.
     * @param dataGridString String
     * @param updateString   String
     * @return
     */

    public int deleteMilestonesUsingJSON(String dataGridString)
    {
    	int ret = 0;
        try {

            MilestoneAgentRObj milestoneAgentRObj = EJBUtil
                    .getMilestoneAgentHome();
            ret = milestoneAgentRObj.deleteMilestonesUsingJSON(dataGridString);

            return ret;
        } catch (Exception e) {
            Rlog.fatal("milestone",
                    "Exception in deleteMilestonesUsingJSON  in MilestoneJB "
                            + e);
            e.printStackTrace();
            return -1;
        }
    } 
  // Overloaded for INF-18183 ::: Akshi
    public int deleteMilestonesUsingJSON(String dataGridString,Hashtable<String, String> args)
    {
    	int ret = 0;
        try {

            MilestoneAgentRObj milestoneAgentRObj = EJBUtil
                    .getMilestoneAgentHome();
            ret = milestoneAgentRObj.deleteMilestonesUsingJSON(dataGridString,args);

            return ret;
        } catch (Exception e) {
            Rlog.fatal("milestone",
                    "Exception in deleteMilestonesUsingJSON  in MilestoneJB "
                            + e);
            e.printStackTrace();
            return -1;
        }
    } 
    /* FIN-22373 09-Oct-2012 -Sudhir*/
    public MilestoneDao getMilestoneRows(int studyId, String mileStoneType, String payType,String milestoneReceivableStatus, String selStatus, String selMilePayType, String srchStrCalName, String srchStrVisitName, String srchStrEvtName, String inActiveFlag, String orderBy, String orderType , String datefrom, String dateto){
        MilestoneDao milestoneDao = new MilestoneDao();
        try {
            Rlog.debug("milestone", "MilestoneJB.getMilestoneRows starting");

            MilestoneAgentRObj milestoneAgentRObj = EJBUtil
                    .getMilestoneAgentHome();
            milestoneDao = milestoneAgentRObj.getMilestoneRows(studyId,  mileStoneType,  payType, milestoneReceivableStatus, selStatus, selMilePayType, srchStrCalName, srchStrVisitName, srchStrEvtName, inActiveFlag,  orderBy, orderType , datefrom, dateto);
            Rlog.debug("milestone", "MilestoneJB.getMilestoneRows after Dao");
            return milestoneDao;
        } catch (Exception e) {
            Rlog.fatal("milestone",
                    "Error in getMilestoneRows(int studyId, String mileStoneType, String payType,String milestoneReceivableStatus) in MilestoneJB "
                            + e);
            return null;
        }
    }    
} // end of class
