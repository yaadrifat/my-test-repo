/*
 * Classname : 				AccountWrapperJB
 * 
 * Version information		1.0
 *
 * Date 					03/14/2001	
 * 
 * Copyright notice: 		Velos Inc
 *
 * Author 					Sajal
 */

package com.velos.eres.web.accountWrapper;

/* IMPORT STATEMENTS */

import java.util.Enumeration;

import com.velos.eres.business.account.impl.AccountBean;
import com.velos.eres.business.accountWrapper.AccountWrapperStateKeeper;
import com.velos.eres.business.address.impl.AddressBean;
import com.velos.eres.business.common.CtrlDao;
import com.velos.eres.business.common.UserDao;
import com.velos.eres.business.group.impl.GroupBean;
import com.velos.eres.business.grpRights.impl.GrpRightsBean;
import com.velos.eres.business.msgcntr.impl.MsgcntrBean;
import com.velos.eres.business.site.impl.SiteBean;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.business.usrGrp.impl.UsrGrpBean;
import com.velos.eres.service.accountAgent.AccountAgentRObj;
import com.velos.eres.service.accountWrapperAgent.AccountWrapperAgentRObj;
import com.velos.eres.service.addressAgent.AddressAgentRObj;
import com.velos.eres.service.groupAgent.GroupAgentRObj;
import com.velos.eres.service.grpRightsAgent.GrpRightsAgentRObj;
import com.velos.eres.service.msgcntrAgent.MsgcntrAgentRObj;
import com.velos.eres.service.siteAgent.SiteAgentRObj;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.usrGrpAgent.UsrGrpAgentRObj;
import com.velos.eres.service.util.CFG;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.address.AddressJB;
import com.velos.eres.web.formLib.FormLibJB;
import com.velos.eres.web.site.SiteJB;
import com.velos.eres.web.user.UserJB;
import com.velos.eres.web.userSite.UserSiteJB;

/* END OF IMPORT STATEMENTS */

/**
 * Client side bean for AccountWrapper. This class is a wrapper for account
 * creation and some other important operations (Please refer to the method list
 * for details). The functions in this class call other functions of different
 * classes thus helping in transaction commits and rollbacks
 * 
 * @author Sajal
 * 
 * @version 1.0, 03/14/2001
 */

public class AccountWrapperJB {

    /*
     * CLASS METHODS
     * 
     * public String getAccType() public void setAccType(String accType) public
     * String getAccRoleType() public void setAccRoleType(String accRoleType)
     * public String getAccName() public void setAccName(String accName) public
     * String getAccMailFlag() public void setAccMailFlag(String accMailFlag)
     * public String getAccPubFlag() public void setAccPubFlag(String
     * accPubFlag) public String getAccLogo() public void setAccLogo(String
     * accLogo) public String getGroupName() public void setGroupName(String
     * groupName) public String getGroupDesc() public void setGroupDesc(String
     * groupDesc) public String getAddTypeUser() public void
     * setAddTypeUser(String addTypeUser) public String getAddPriUser() public
     * void setAddPriUser(String addPriUser) public String getAddCityUser()
     * public void setAddCityUser(String addCityUser) public String
     * getAddStateUser() public void setAddStateUser(String addStateUser) public
     * String getAddZipUser() public void setAddZipUser(String addZipUser)
     * public String getAddCountryUser() public void setAddCountryUser(String
     * addCountryUser) public String getAddCountyUser() public void
     * setAddCountyUser(String addCountyUser) public String getAddEffDateUser()
     * public void setAddEffDateUser(String addEffDateUser) public String
     * getAddPhoneUser() public void setAddPhoneUser(String addPhoneUser) public
     * String getAddEmailUser() public void setAddEmailUser(String addEmailUser)
     * public String getAddPagerUser() public void setAddPagerUser(String
     * addPagerUser) public String getAddMobileUser() public void
     * setAddMobileUser(String addMobileUser) public String getAddFaxUser()
     * public void setAddFaxUser(String addFaxUser) public String
     * getAddTypeSite() public void setAddTypeSite(String addTypeSite) public
     * String getAddPriSite() public void setAddPriSite(String addPriSite)
     * public String getAddCitySite() public void setAddCitySite(String
     * addCitySite) public String getAddStateSite() public void
     * setAddStateSite(String addStateSite) public String getAddZipSite() public
     * void setAddZipSite(String addZipSite) public String getAddCountrySite()
     * public void setAddCountrySite(String addCountrySite) public String
     * getAddCountySite() public void setAddCountySite(String addCountySite)
     * public String getAddEffDateSite() public void setAddEffDateSite(String
     * addEffDateSite) public String getAddPhoneSite() public void
     * setAddPhoneSite(String addPhoneSite) public String getAddEmailSite()
     * public void setAddEmailSite(String addEmailSite) public String
     * getAddPagerSite() public void setAddPagerSite(String addPagerSite) public
     * String getAddMobileSite() public void setAddMobileSite(String
     * addMobileSite) public String getAddFaxSite() public void
     * setAddFaxSite(String addFaxSite) public String getSiteCodelstType()
     * public void setSiteCodelstType(String siteCodelstType) public String
     * getSiteName() public void setSiteName(String siteName) public String
     * getSiteInfo() public void setSiteInfo(String siteInfo) public String
     * getSiteParent() public void setSiteParent(String siteParent) public
     * String getUserCodelstJobtype() public void setUserCodelstJobtype(String
     * userCodelstJobtype) public String getUserLastName() public void
     * setUserLastName(String userLastName) public String getUserFirstName()
     * public void setUserFirstName(String userFirstName) public String
     * getUserMidName() public void setUserMidName(String userMidName) public
     * String getUserWrkExp() public void setUserWrkExp(String userWrkExp)
     * public String getUserPhaseInv() public void setUserPhaseInv(String
     * userPhaseInv) public String getUserSessionTime() public void
     * setUserSessionTime(String userSessionTime) public String
     * getUserLoginName() public void setUserLoginName(String userLoginName)
     * public String getUserPwd() public void setUserPwd(String userPwd) public
     * String getUserSecQues() public void setUserSecQues(String userSecQues)
     * public String getUserAnswer() public void setUserAnswer(String
     * userAnswer) public String getUserCodelstSpl() public void
     * setUserCodelstSpl(String userCodelstSpl) public String
     * getMsgcntrToUserId() public void setMsgcntrToUserId(String
     * msgcntrToUserId) public String getUserStatus() public void
     * setUserStatus(String userStatus) public String getUserGrpDefault() public
     * void setUserGrpDefault(String userGrpDefault) public String
     * getUserAccountId() public void setUserAccountId(String userAccountId)
     * public String getUserPerAddressId() public void
     * setUserPerAddressId(String userPerAddressId) public String
     * getUserSiteId() public void setUserSiteId(String userSiteId) public
     * String getSiteStatus() public void setSiteStatus(String siteStatus)
     * public String getSitePerAdd() public void setSitePerAdd(String
     * sitePerAdd) public String getSiteId() public void setSiteId(String
     * siteId) public String getUserId() public void setUserId(String userId)
     * public String getAccStatus() public void setAccStatus(String accStatus)
     * public String getAccStartDate() public void setAccStartDate(String
     * accStartDate) public void createAccount() public int createUser() public
     * int updateUser() public int createSite() public int updateSite() public
     * AccountWrapperStateKeeper createAccountWrapperStateKeeper() public
     * SiteStateKeeper createSiteStateKeeper() public AddressStateKeeper
     * createAddStateKeeper() public AccountWrapperStateKeeper
     * createAccountUserWrapperStateKeeper()
     * 
     * END OF CLASS METHODS
     */

	
    /**
     * the account type
     */
    private String accType;

    /**
     * the account role type
     */
    private String accRoleType;

    /**
     * the account name
     */
    private String accName;

    /**
     * the account mail flag
     */
    private String accMailFlag;

    /**
     * the account public flag
     */
    private String accPubFlag;

    /**
     * the account logo
     */
    private byte[] accLogo;

    /**
     * the account Module Right
     */
    private String accModRight;

    /**
     * the group name
     */
    private String groupName;

    /**
     * the group description
     */
    private String groupDesc;

    /**
     * the address type for User
     */
    private String addTypeUser;

    /**
     * the address primary for User
     */
    private String addPriUser;

    /**
     * the address city for User
     */
    private String addCityUser;

    /**
     * the address state for User
     */
    private String addStateUser;

    /**
     * the address zip code for User
     */
    private String addZipUser;

    /**
     * the address country for User
     */
    private String addCountryUser;

    /**
     * the address county for User
     */
    private String addCountyUser;

    /**
     * the address effective date for User
     */
    private String addEffDateUser;

    /**
     * the address phone for User
     */
    private String addPhoneUser;

    /**
     * the address email for User
     */
    private String addEmailUser;

    /**
     * the address pager for User
     */
    private String addPagerUser;

    /**
     * the address pager for User
     */
    private String addMobileUser;

    /**
     * the address fax for User
     */
    private String addFaxUser;

    /**
     * the address type for Site
     */
    private String addTypeSite;

    /**
     * the address primary for Site
     */
    private String addPriSite;

    /**
     * the address city for Site
     */
    private String addCitySite;

    /**
     * the address state for Site
     */
    private String addStateSite;

    /**
     * the address zip code for Site
     */
    private String addZipSite;

    /**
     * the address country for Site
     */
    private String addCountrySite;

    /**
     * the address county for Site
     */
    private String addCountySite;

    /**
     * the address effective date for Site
     */
    private String addEffDateSite;

    /**
     * the address phone for Site
     */
    private String addPhoneSite;

    /**
     * the address email for Site
     */
    private String addEmailSite;

    /**
     * the address pager for Site
     */
    private String addPagerSite;

    /**
     * the address pager for Site
     */
    private String addMobileSite;

    /**
     * the address fax for Site
     */
    private String addFaxSite;

    /**
     * the site Type
     */
    private String siteCodelstType;

    /**
     * the site name
     */
    private String siteName;
    private String ctepid;

	/* Added an attribute siteNotes for June Enhancement '07  #U7 */
    /**
	 * the site notes
	 */

	 private String siteNotes;

    /**
     * the site Info
     */
    private String siteInfo;

    /**
     * the site Info
     */
    private String siteStatus;

    /**
     * the site Id
     */
    private String siteId;

    /**
     * the site Address Id
     */
    private String sitePerAdd;

    /**
     * the site AbstractIdentifier - Id to identify a site
     */

    private String siteIdentifier;

    /**
     * the site parent
     */
    private String siteParent;

    /**
     * the user codelst jobtype
     */
    private String userCodelstJobtype;

    /**
     * the user lastName
     */
    private String userLastName;

    /**
     * the user firstName
     */

    private String userFirstName;

    /**
     * the user mid Name
     */

    private String userMidName;

    /**
     * the user work experience
     */

    private String userWrkExp;

    /**
     * the user phase ivaluation
     */

    private String userPhaseInv;

    /**
     * the user session Time out
     */

    private String userSessionTime;

    /**
     * the user login Name
     */

    private String userLoginName;

    /**
     * the user password
     */

    private String userPwd;

    /**
     * the user Secret Question
     */

    private String userSecQues;

    /**
     * the user Answer
     */

    private String userAnswer;

    /**
     * the user status
     */

    private String userStatus;

    /**
     * the user default group
     */
    private String userGrpDefault;

    /**
     * the user Account
     */
    private String userAccountId;

    /**
     * the user Site ID
     */
    private String userSiteId;

    /**
     * the user Address ID
     */
    private String userPerAddressId;

    /**
     * the user codelist primary Speciality
     */

    private String userCodelstSpl;

    private String userTimeZoneId;

    /**
     * the msgcntr to userId
     */
    private String msgcntrToUserId;

    /**
     * the user Id
     */

    private String userId;

    /**
     * the account status
     */

    private String accStatus;

    /**
     * the account start date
     */

    private String accStartDate;

    /**
     * the msgcntr status
     */

    private String msgcntrStatus;

    /**
     * creator
     */
    private String creator;

    /**
     * last modified by
     */
    private String modifiedBy;

    /**
     * IP Address
     */
    private String ipAdd;

    /**
     * Pwd Expiry Date
     */
    private String userPwdExpiryDate;

    /**
     * Pwd Expiry Days
     */
    private String userPwdExpiryDays;

    /**
     * Pwd Reminder to be sent on
     */
    private String userPwdReminderDate;

    /**
     * E-signature
     */
    private String userESign;

    /**
     * E-signature Expiry Date
     */
    private String userESignExpiryDate;

    /**
     * Pwd Expiry Days
     */
    private String userESignExpiryDays;

    /**
     * Pwd Reminder to be sent on
     */
    private String userESignReminderDate;

    /**
     * User Site Flag
     */
    private String userSiteFlag;

    /**
     * User Type
     */
    private String userType;
    
    private String userLoginModuleId;
    private String userLoginModuleMap;
    
    private String siteHidden; //KM
    private String poIdentifier;
    
    private String userHidden; //KM

    // GETTER SETTER METHODS

    /**
	 * @return the userLoginModuleId
	 */
	public String getUserLoginModuleId() {
		return userLoginModuleId;
	}

	/**
	 * @param userLoginModuleId the userLoginModuleId to set
	 */
	public void setUserLoginModuleId(String userLoginModuleId) {
		this.userLoginModuleId = userLoginModuleId;
	}

	/**
	 * @return the userLoginModuleMap
	 */
	public String getUserLoginModuleMap() {
		return userLoginModuleMap;
	}

	/**
	 * @param userLoginModuleMap the userLoginModuleMap to set
	 */
	public void setUserLoginModuleMap(String userLoginModuleMap) {
		this.userLoginModuleMap = userLoginModuleMap;
	}

	/**
     * Returns the Account Type
     * 
     * @return the Account Type
     */
    public String getAccType() {
        return this.accType;
    }

    /**
     * Registers the Account Type
     * 
     * @param accType
     *            The value that is required to be registered as Account Type
     */
    public void setAccType(String accType) {
        this.accType = accType;
    }

    /**
     * Returns the Account Role Type
     * 
     * @return the Account Role Type
     */
    public String getAccRoleType() {
        return this.accRoleType;
    }

    /**
     * Registers the Account Role Type
     * 
     * @param accRoleType
     *            The value that is required to be registered as Account Role
     *            Type
     */
    public void setAccRoleType(String accRoleType) {
        this.accRoleType = accRoleType;
    }

    /**
     * Returns the Account Name
     * 
     * @return the Account Name
     */
    public String getAccName() {
        return this.accName;
    }

    /**
     * Registers the Account Name
     * 
     * @param accName
     *            The Account Name that is to be registered
     */
    public void setAccName(String accName) {
        this.accName = accName;
    }

    /**
     * Returns the value of Account Mail Flag
     * 
     * @return Y - if the flag is turned on <br>
     *         N - if the flag is turned off
     */
    public String getAccMailFlag() {
        return this.accMailFlag;
    }

    /**
     * Registers the value of Account Mail Flag
     * 
     * @param accMailFlag
     *            The value that is required to be registered as Account Mail
     *            Flag <br>
     *            Y - if the flag is to be turned on <br>
     *            N - if the flag is to be turned off
     */
    public void setAccMailFlag(String accMailFlag) {
        this.accMailFlag = accMailFlag;
    }

    /**
     * Returns the value of Account Public Flag
     * 
     * @return Y - if the flag is turned on <br>
     *         N - if the flag is turned off
     */
    public String getAccPubFlag() {
        return this.accPubFlag;
    }

    /**
     * Registers the value of Account Public Flag
     * 
     * @param accPubFlag
     *            The value that is required to be registered as Account Public
     *            Flag <br>
     *            Y - if the flag is to be turned on <br>
     *            N - if the flag is to be turned off
     */
    public void setAccPubFlag(String accPubFlag) {
        this.accPubFlag = accPubFlag;
    }

    /**
     * Returns the Account Logo
     * 
     * @return the Account Logo
     */
    public byte[] getAccLogo() {
        return this.accLogo;
    }

    /**
     * Registers the Account Logo
     * 
     * @param accLogo
     *            The value that is required to be registered as Account Logo
     */
    public void setAccLogo(byte[] accLogo) {
        this.accLogo = accLogo;
    }

    /**
     * Returns the Account Module Right
     * 
     * @return the Account Module Right
     */
    public String getAccModRight() {
        return this.accModRight;
    }

    /**
     * Registers the Account Module Right
     * 
     * @param accLogo
     *            The value that is required to be registered as Account Module
     *            Right
     */
    public void setAccModRight(String accModRight) {
        this.accModRight = accModRight;
    }

    /**
     * Returns the Group Name
     * 
     * @return the Group Name
     */
    public String getGroupName() {
        return this.groupName;
    }

    /**
     * Registers the Group Name
     * 
     * @param groupName
     *            The Group Name that is to be registered
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /**
     * Returns the Group Description
     * 
     * @return the Group Description
     */
    public String getGroupDesc() {
        return this.groupDesc;
    }

    /**
     * Registers the Group Description
     * 
     * @param groupDesc
     *            The String that is required to be registered as Group
     *            Description
     */
    public void setGroupDesc(String groupDesc) {
        this.groupDesc = groupDesc;
    }

    /**
     * Returns the Type of User Address
     * 
     * @return the Type of User Address
     */
    public String getAddTypeUser() {
        return this.addTypeUser;
    }

    /**
     * Registers the Type of User Address
     * 
     * @param addTypeUser
     *            The String that is required to be registered as the Type of
     *            User Address
     */
    public void setAddTypeUser(String addTypeUser) {
        this.addTypeUser = addTypeUser;
    }

    /**
     * Returns the Primary Address of the user
     * 
     * @return the Primary Address of the user
     */
    public String getAddPriUser() {
        return this.addPriUser;
    }

    /**
     * Registers the Primary Address of the user
     * 
     * @param addPriUser
     *            The String that is required to be registered as Primary
     *            Address of the user
     */
    public void setAddPriUser(String addPriUser) {
        this.addPriUser = addPriUser;
    }

    /**
     * Returns the City of the user's address
     * 
     * @return the City of the user's address
     */
    public String getAddCityUser() {
        return this.addCityUser;
    }

    /**
     * Registers the City of the user's address
     * 
     * @param addCityUser
     *            The String that is required to be registered as the City of
     *            the user's address
     */
    public void setAddCityUser(String addCityUser) {
        this.addCityUser = addCityUser;
    }

    /**
     * Returns the State of the user's address
     * 
     * @return the State of the user's address
     */
    public String getAddStateUser() {
        return this.addStateUser;
    }

    /**
     * Registers the State of the user's address
     * 
     * @param addStateUser
     *            The String that is required to be registered as the State of
     *            the user's address
     */
    public void setAddStateUser(String addStateUser) {
        this.addStateUser = addStateUser;
    }

    /**
     * Returns the Zip code of the user's address
     * 
     * @return the Zip code of the user's address
     */
    public String getAddZipUser() {
        return this.addZipUser;
    }

    /**
     * Registers the Zip code of the user's address
     * 
     * @param addZipUser
     *            The String that is required to be registered as the Zip code
     *            of the user's address
     */
    public void setAddZipUser(String addZipUser) {
        this.addZipUser = addZipUser;
    }

    /**
     * Returns the Country of the user's address
     * 
     * @return the Country of the user's address
     */
    public String getAddCountryUser() {
        return this.addCountryUser;
    }

    /**
     * Registers the Country of the user's address
     * 
     * @param addCountryUser
     *            The String that is required to be registered as the Country of
     *            the user's address
     */
    public void setAddCountryUser(String addCountryUser) {
        this.addCountryUser = addCountryUser;
    }

    /**
     * Returns the County of the user's address
     * 
     * @return the County of the user's address
     */
    public String getAddCountyUser() {
        return this.addCountyUser;
    }

    /**
     * Registers the County of the user's address
     * 
     * @param addCountyUser
     *            The String that is required to be registered as the County of
     *            the user's address
     */
    public void setAddCountyUser(String addCountyUser) {
        this.addCountyUser = addCountyUser;
    }

    /**
     * Returns the Effective date for the user's address
     * 
     * @return the Effective date for the user's address
     */
    public String getAddEffDateUser() {
        return this.addEffDateUser;
    }

    /**
     * Registers the Effective date for the user's address
     * 
     * @param addEffDateUser
     *            The String that is required to be registered as the Effective
     *            date for the user's address
     */
    public void setAddEffDateUser(String addEffDateUser) {
        this.addEffDateUser = addEffDateUser;
    }

    /**
     * Returns the Phone number for the user's address
     * 
     * @return the Phone number for the user's address
     */
    public String getAddPhoneUser() {
        return this.addPhoneUser;
    }

    /**
     * Registers the Phone number for the user's address
     * 
     * @param addPhoneUser
     *            The String that is required to be registered as the Phone
     *            number for the user's address
     */
    public void setAddPhoneUser(String addPhoneUser) {
        this.addPhoneUser = addPhoneUser;
    }

    /**
     * Returns the User's email address
     * 
     * @return the User's email address
     */
    public String getAddEmailUser() {
        return this.addEmailUser;
    }

    /**
     * Registers the User's email address
     * 
     * @param addEmailUser
     *            The String that is required to be registered as the User's
     *            email address
     */
    public void setAddEmailUser(String addEmailUser) {
        this.addEmailUser = addEmailUser;
    }

    /**
     * Returns the User's Pager number
     * 
     * @return the User's Pager number
     */
    public String getAddPagerUser() {
        return this.addPagerUser;
    }

    /**
     * Registers the User's Pager number
     * 
     * @param addPagerUser
     *            The String that is required to be registered as the User's
     *            Pager number
     */
    public void setAddPagerUser(String addPagerUser) {
        this.addPagerUser = addPagerUser;
    }

    /**
     * Returns the User's Mobile number
     * 
     * @return the User's Mobile number
     */
    public String getAddMobileUser() {
        return this.addMobileUser;
    }

    /**
     * Registers the User's Mobile number
     * 
     * @param addMobileUser
     *            The String that is required to be registered as the User's
     *            Mobile number
     */
    public void setAddMobileUser(String addMobileUser) {
        this.addMobileUser = addMobileUser;
    }

    /**
     * Returns the User's Fax number
     * 
     * @return the User's Fax number
     */
    public String getAddFaxUser() {
        return this.addFaxUser;
    }

    /**
     * Registers the User's Fax number
     * 
     * @param addFaxUser
     *            The String that is required to be registered as the User's Fax
     *            number
     */
    public void setAddFaxUser(String addFaxUser) {
        this.addFaxUser = addFaxUser;
    }

    /**
     * Returns the Type of Site Address
     * 
     * @return the Type of Site Address
     */
    public String getAddTypeSite() {
        return this.addTypeSite;
    }

    /**
     * Registers the Type of Site Address
     * 
     * @param addTypeSite
     *            The String that is required to be registered as the Type of
     *            Site Address
     */
    public void setAddTypeSite(String addTypeSite) {
        this.addTypeSite = addTypeSite;
    }

    /**
     * Returns the Primary Address of the site
     * 
     * @return the Primary Address of the site
     */
    public String getAddPriSite() {
        return this.addPriSite;
    }

    /**
     * Registers the Primary Address of the site
     * 
     * @param addPriSite
     *            The String that is required to be registered as the Primary
     *            Address of the site
     */
    public void setAddPriSite(String addPriSite) {
        this.addPriSite = addPriSite;
    }

    /**
     * Returns the City of the site address
     * 
     * @return the City of the site address
     */
    public String getAddCitySite() {
        return this.addCitySite;
    }

    /**
     * Registers the City of the site address
     * 
     * @param addCitySite
     *            The String that is required to be registered as the City of
     *            the site address
     */
    public void setAddCitySite(String addCitySite) {
        this.addCitySite = addCitySite;
    }

    /**
     * Returns the State of the site address
     * 
     * @return the State of the site address
     */
    public String getAddStateSite() {
        return this.addStateSite;
    }

    /**
     * Registers the State of the site address
     * 
     * @param addStateSite
     *            The String that is required to be registered as the State of
     *            the site address
     */
    public void setAddStateSite(String addStateSite) {
        this.addStateSite = addStateSite;
    }

    /**
     * Returns the Zip Code of the site address
     * 
     * @return the Zip Code of the site address
     */
    public String getAddZipSite() {
        return this.addZipSite;
    }

    /**
     * Registers the Zip Code of the site address
     * 
     * @param addZipSite
     *            The String that is required to be registered as the Zip Code
     *            of the site address
     */
    public void setAddZipSite(String addZipSite) {
        this.addZipSite = addZipSite;
    }

    /**
     * Returns the Country of the site address
     * 
     * @return the Country of the site address
     */
    public String getAddCountrySite() {
        return this.addCountrySite;
    }

    /**
     * Registers the Country of the site address
     * 
     * @param addCountrySite
     *            The String that is required to be registered as the Country of
     *            the site address
     */
    public void setAddCountrySite(String addCountrySite) {
        this.addCountrySite = addCountrySite;
    }

    /**
     * Returns the County of the site address
     * 
     * @return the County of the site address
     */
    public String getAddCountySite() {
        return this.addCountySite;
    }

    /**
     * Registers the County of the site address
     * 
     * @param addCountySite
     *            The String that is required to be registered as the County of
     *            the site address
     */
    public void setAddCountySite(String addCountySite) {
        this.addCountySite = addCountySite;
    }

    /**
     * Returns the Effective date for the site address
     * 
     * @return the Effective date for the site address
     */
    public String getAddEffDateSite() {
        return this.addEffDateSite;
    }

    /**
     * Registers the Effective date for the site address
     * 
     * @param addEffDateSite
     *            The String that is required to be registered as the Effective
     *            date for the site address
     */
    public void setAddEffDateSite(String addEffDateSite) {
        this.addEffDateSite = addEffDateSite;
    }

    /**
     * Returns the Phone number for the site
     * 
     * @return the Phone number for the site
     */
    public String getAddPhoneSite() {
        return this.addPhoneSite;
    }

    /**
     * Registers the Phone number for the site
     * 
     * @param addPhoneSite
     *            The String that is required to be registered as the Phone
     *            number for the site
     */
    public void setAddPhoneSite(String addPhoneSite) {
        this.addPhoneSite = addPhoneSite;
    }

    /**
     * Returns the email address for the site
     * 
     * @return the email address for the site
     */
    public String getAddEmailSite() {
        return this.addEmailSite;
    }

    /**
     * Registers the email address for the site
     * 
     * @param addEmailSite
     *            The String that is required to be registered as the email
     *            address for the site
     */
    public void setAddEmailSite(String addEmailSite) {
        this.addEmailSite = addEmailSite;
    }

    /**
     * Returns the Pager number for the site
     * 
     * @return the Pager number for the site
     */
    public String getAddPagerSite() {
        return this.addPagerSite;
    }

    /**
     * Registers the Pager number for the site
     * 
     * @param addPagerSite
     *            The String that is required to be registered as the Pager
     *            number for the site
     */
    public void setAddPagerSite(String addPagerSite) {
        this.addPagerSite = addPagerSite;
    }

    /**
     * Returns the Mobile number for the site
     * 
     * @return the Mobile number for the site
     */
    public String getAddMobileSite() {
        return this.addMobileSite;
    }

    /**
     * Registers the Mobile number for the site
     * 
     * @param addMobileSite
     *            The String that is required to be registered as the Mobile
     *            number for the site
     */
    public void setAddMobileSite(String addMobileSite) {
        this.addMobileSite = addMobileSite;
    }

    /**
     * Returns the fax number for the site
     * 
     * @return the fax number for the site
     */
    public String getAddFaxSite() {
        return this.addFaxSite;
    }

    /**
     * Registers the fax number for the site
     * 
     * @param addFaxSite
     *            The String that is required to be registered as the fax number
     *            for the site
     */
    public void setAddFaxSite(String addFaxSite) {
        this.addFaxSite = addFaxSite;
    }

    /**
     * Returns the Site Type
     * 
     * @return the Site Type
     */
    public String getSiteCodelstType() {
        return this.siteCodelstType;
    }

    /**
     * Registers the Site Type
     * 
     * @param siteCodelstType
     *            The String that is required to be registered as the Site Type
     */
    public void setSiteCodelstType(String siteCodelstType) {
        this.siteCodelstType = siteCodelstType;
    }

	/**
	 * Register the Site notes
	 * 
	 */

	 public void setSiteNotes(String siteNotes) {
		 
		this.siteNotes = siteNotes;
	 }

	 /**
	  * Returns the Site notes
	  * @returns the Site Notes
	  */

	  public String getSiteNotes() {
		  return this.siteNotes;
	  }
			
    /**
     * Returns the Site Name
     * 
     * @return the Site Name
     */
    public String getSiteName() {
        return this.siteName;
    }

    /**
     * Registers the Site Name
     * 
     * @param siteName
     *            The String that is required to be registered as the Site Name
     */
    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getCtepId() {
    	return this.ctepid;
    }

    public void setCtepId(String ctepid){
    	this.ctepid = ctepid;
    }


	    /**
     * Returns the Information about the site
     * 
     * @return the Information about the site
     */
    public String getSiteInfo() {
        return this.siteInfo;
    }

    /**
     * Registers the Information about the site
     * 
     * @param siteInfo
     *            The String that is required to be registered as the
     *            Information about the site
     */
    public void setSiteInfo(String siteInfo) {
        this.siteInfo = siteInfo;
    }

    /**
     * Returns the Parent Site for this site
     * 
     * @return the Parent Site for this site
     */
    public String getSiteParent() {
        return this.siteParent;
    }

    /**
     * Registers the Parent Site for this site
     * 
     * @param siteParent
     *            The String that is required to be registered as the Parent
     *            Site for this site
     */
    public void setSiteParent(String siteParent) {
        this.siteParent = siteParent;
    }

    /**
     * Returns the User's jobtype
     * 
     * @return the User's jobtype
     */
    public String getUserCodelstJobtype() {
        return this.userCodelstJobtype;
    }

    /**
     * Registers the User's jobtype
     * 
     * @param userCodelstJobtype
     *            The String that is required to be registered as the User's
     *            jobtype
     */
    public void setUserCodelstJobtype(String userCodelstJobtype) {
        this.userCodelstJobtype = userCodelstJobtype;
    }

    /**
     * Returns the User's Last Name
     * 
     * @return the User's Last Name
     */
    public String getUserLastName() {
        return this.userLastName;
    }

    /**
     * Registers the User's Last Name
     * 
     * @param userLastName
     *            The String that is required to be registered as the User's
     *            Last Name
     */
    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    /**
     * Returns the User's First Name
     * 
     * @return the User's First Name
     */
    public String getUserFirstName() {
        Rlog.debug("accountWrapper", "in accountWrapperJB getUserFirstName"
                + this.userFirstName);
        return this.userFirstName;
    }

    /**
     * Registers the User's First Name
     * 
     * @param userFirstName
     *            The String that is required to be registered as the User's
     *            First Name
     */
    public void setUserFirstName(String userFirstName) {
        Rlog.debug("accountWrapper", "in accountWrapperJB getUserFirstName"
                + userFirstName);
        this.userFirstName = userFirstName;
    }

    /**
     * Returns the User's Middle Name
     * 
     * @return the User's Middle Name
     */
    public String getUserMidName() {
        return this.userMidName;
    }

    /**
     * Registers the User's Middle Name
     * 
     * @param userMidName
     *            The value that is required to be registered as the User's
     *            Middle Name
     */
    public void setUserMidName(String userMidName) {
        this.userMidName = userMidName;
    }

    /**
     * Returns the User's Work Experience
     * 
     * @return the User's Work Experience
     */
    public String getUserWrkExp() {
        return this.userWrkExp;
    }

    /**
     * Registers the User's Work Experience
     * 
     * @param userWrkExp
     *            The String that is required to be registered as the User's
     *            Work Experience
     */
    public void setUserWrkExp(String userWrkExp) {
        this.userWrkExp = userWrkExp;
    }

    /**
     * Returns the Phase in which the user is involved
     * 
     * @return the Phase in which the user is involved
     */
    public String getUserPhaseInv() {
        return this.userPhaseInv;
    }

    /**
     * Registers the Phase in which the user is involved
     * 
     * @param userPhaseInv
     *            The String that is required to be registered as the Phase in
     *            which the user is involved
     */
    public void setUserPhaseInv(String userPhaseInv) {
        this.userPhaseInv = userPhaseInv;
    }

    /**
     * Returns the Time after which the user's session would be timed out
     * 
     * @return the Time after which the user's session would be timed out
     */
    public String getUserSessionTime() {
        return this.userSessionTime;
    }

    /**
     * Registers the Time after which the user's session would be timed out
     * 
     * @param userSessionTime
     *            The value that is required to be registered as the Time after
     *            which the user's session would be timed out
     */
    public void setUserSessionTime(String userSessionTime) {
        this.userSessionTime = userSessionTime;
    }

    /**
     * Returns the Login Name for the user
     * 
     * @return the Login Name for the user
     */
    public String getUserLoginName() {
        return this.userLoginName;
    }

    /**
     * Registers the Login Name for the user
     * 
     * @param userLoginName
     *            The String that is required to be registered as the Login Name
     *            for the user
     */
    public void setUserLoginName(String userLoginName) {
        this.userLoginName = userLoginName;
    }

    /**
     * Returns the Password for the user
     * 
     * @return the Password for the user
     */
    public String getUserPwd() {
        return this.userPwd;
    }

    /**
     * Registers the Password for the user
     * 
     * @param userPwd
     *            The String that is required to be registered as the Password
     *            for the user
     */
    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd;
    }

    /**
     * Returns the Secret Question for the user
     * 
     * @return the Secret Question for the user
     */
    public String getUserSecQues() {
        return this.userSecQues;
    }

    /**
     * Registers the Secret Question for the user
     * 
     * @param userSecQues
     *            The String that is required to be registered as the Secret
     *            Question for the user
     */
    public void setUserSecQues(String userSecQues) {
        this.userSecQues = userSecQues;
    }

    /**
     * Returns the Answer to User's Secret Question
     * 
     * @return the Answer to User's Secret Question
     */
    public String getUserAnswer() {
        return this.userAnswer;
    }

    /**
     * Registers the Answer to User's Secret Question
     * 
     * @param userAnswer
     *            The String that is required to be registered as the Answer to
     *            User's Secret Question
     */
    public void setUserAnswer(String userAnswer) {
        this.userAnswer = userAnswer;
    }

    /**
     * Returns the User's speciality area
     * 
     * @return the User's speciality area
     */
    public String getUserCodelstSpl() {
        return this.userCodelstSpl;
    }

    /**
     * Registers the User's speciality area
     * 
     * @param userCodelstSpl
     *            The String that is required to be registered as the User's
     *            speciality area
     */
    public void setUserCodelstSpl(String userCodelstSpl) {
        this.userCodelstSpl = userCodelstSpl;
    }

    public String getUserTimeZoneId() {
        return this.userTimeZoneId;
    }

    public void setUserTimeZoneId(String userTimeZoneId) {
        this.userTimeZoneId = userTimeZoneId;
    }

    /**
     * Returns the User Id to whom the message is to be sent for Account
     * creation
     * 
     * @return the User Id to whom the message is to be sent for Account
     *         creation
     */
    public String getMsgcntrToUserId() {
        return this.msgcntrToUserId;
    }

    /**
     * Registers the User Id to whom the message is to be sent for Account
     * creation
     * 
     * @param msgcntrToUserId
     *            The String that is required to be registered as the User Id to
     *            whom the message is to be sent for Account creation
     */
    public void setMsgcntrToUserId(String msgcntrToUserId) {
        this.msgcntrToUserId = msgcntrToUserId;
    }

    /**
     * Returns the Status of the user
     * 
     * @return the Status of the user
     */
    public String getUserStatus() {
        return this.userStatus;
    }

    /**
     * Registers the Status of the user
     * 
     * @param userStatus
     *            The String that is required to be registered as the Status of
     *            the user
     */
    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    /**
     * Returns the Default Group for the user
     * 
     * @return the Default Group for the user
     */
    public String getUserGrpDefault() {
        return this.userGrpDefault;
    }

    /**
     * Registers the Default Group for the user
     * 
     * @param userGrpDefault
     *            The String that is required to be registered as the Default
     *            Group for the user
     */
    public void setUserGrpDefault(String userGrpDefault) {
        this.userGrpDefault = userGrpDefault;
    }

    /**
     * Returns the Account Id to which the user is associated
     * 
     * @return the Account Id to which the user is associated
     */
    public String getUserAccountId() {
        return this.userAccountId;
    }

    /**
     * Registers the Account Id to which the user is associated
     * 
     * @param userAccountId
     *            The String that is required to be registered as the Account Id
     *            to which the user is associated
     */
    public void setUserAccountId(String userAccountId) {
        this.userAccountId = userAccountId;
    }

    /**
     * Returns the User's Permanent Address Id
     * 
     * @return the User's Permanent Address Id
     */
    public String getUserPerAddressId() {
        return this.userPerAddressId;
    }

    /**
     * Registers the User's Permanent Address Id
     * 
     * @param userPerAddressId
     *            The String that is required to be registered as the User's
     *            Permanent Address Id
     */
    public void setUserPerAddressId(String userPerAddressId) {
        this.userPerAddressId = userPerAddressId;
    }

    /**
     * Returns the Site Id associated to the user
     * 
     * @return the Site Id associated to the user
     */
    public String getUserSiteId() {
        return this.userSiteId;
    }

    /**
     * Registers the Site Id associated to the user
     * 
     * @param userSiteId
     *            The String that is required to be registered as the Site Id
     *            associated to the user
     */
    public void setUserSiteId(String userSiteId) {
        this.userSiteId = userSiteId;
    }

    /**
     * Returns the Site Flag of the user
     * 
     * @return the Site Flag associated to the user
     */

    public String getUserSiteFlag() {
        return this.userSiteFlag;
    }

    /**
     * Set Site Flag of the user
     * 
     * @param userSiteFlag
     *            A/S
     */

    public void setUserSiteFlag(String userSiteFlag) {
        this.userSiteFlag = userSiteFlag;
    }

    /**
     * Returns the user Type (N - Non system user,S - System User)
     * 
     * @return user type N/S
     */

    public String getUserType() {
        Rlog.debug("accountWrapper", "in accountWrapperJB getUserType"
                + this.userType);
        return this.userType;
    }

    /**
     * Set user Type
     * 
     * @param userType
     *            N/S
     */

    public void setUserType(String userType) {
        Rlog.debug("accountWrapper", "in accountWrapperJB setUserType"
                + userType);
        this.userType = userType;
    }

    /**
     * Returns the Site Status
     * 
     * @return the Site Status
     */
    public String getSiteStatus() {
        return this.siteStatus;
    }

    /**
     * Registers the Site Status
     * 
     * @param siteStatus
     *            The String that is required to be registered as the Site
     *            Status
     */
    public void setSiteStatus(String siteStatus) {
        this.siteStatus = siteStatus;
    }

    /**
     * Returns the Site's Address Id
     * 
     * @return the Site's Address Id
     */
    public String getSitePerAdd() {
        return this.sitePerAdd;
    }

    /**
     * Registers the Site's Address Id
     * 
     * @param sitePerAdd
     *            The String that is required to be registered as the Site's
     *            Address Id
     */
    public void setSitePerAdd(String sitePerAdd) {
        this.sitePerAdd = sitePerAdd;
    }

    /**
     * Returns the Site Id
     * 
     * @return the Site Id
     */
    public String getSiteId() {
        return this.siteId;
    }

    /**
     * Registers the Site Id
     * 
     * @param siteId
     *            The String that is required to be registered as the Site Id
     */
    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    /**
     * Returns the User Id
     * 
     * @return the User Id
     */
    public String getUserId() {
        return this.userId;
    }

    /**
     * Registers the User Id
     * 
     * @param userId
     *            The String that is required to be registered as the User Id
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * Returns the Account Status
     * 
     * @return the Account Status
     */
    public String getAccStatus() {
        return this.accStatus;
    }

    /**
     * Registers the Account Status
     * 
     * @param accStatus
     *            The String that is required to be registered as the Account
     *            Status
     */
    public void setAccStatus(String accStatus) {
        this.accStatus = accStatus;
    }

    /**
     * Returns the Start Date for the Account
     * 
     * @return the Start Date for the Account
     */
    public String getAccStartDate() {
        return this.accStartDate;
    }

    /**
     * Registers the Start Date for the Account
     * 
     * @param accStartDate
     *            The String that is required to be registered as the Start Date
     *            for the Account
     */
    public void setAccStartDate(String accStartDate) {
        this.accStartDate = accStartDate;
    }

    /**
     * Returns msgcntrStatus
     * 
     * @return the msgcntrStatus
     */
    public String getMsgcntrStatus() {
        return this.msgcntrStatus;
    }

    /**
     * Registers the msgcntrStatus
     * 
     * @param msgcntrStatus
     *            The String that is required to be registered as the
     *            msgcntrStatus
     */
    public void setMsgcntrStatus(String msgcntrStatus) {
        this.msgcntrStatus = msgcntrStatus;
    }

    /**
     * @return Creator
     */
    public String getCreator() {
        return this.creator;
    }

    /**
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return Last Modified By
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param modifiedBy
     *            The value that is required to be registered as record last
     *            modified by
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return IP Address
     */
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
     * @return Pwd Expiry Date
     */
    public String getUserPwdExpiryDate() {
        return this.userPwdExpiryDate;
    }

    /**
     * @param userPwdExpiryDate
     *            The value that is required to be registered as the Pwd Expiry
     *            Date
     */
    public void setUserPwdExpiryDate(String userPwdExpiryDate) {
        this.userPwdExpiryDate = userPwdExpiryDate;
    }

    /**
     * @return Pwd Expiry Days
     */
    public String getUserPwdExpiryDays() {
        return this.userPwdExpiryDays;
    }

    /**
     * @param userPwdExpiryDays
     *            The value that is required to be registered as the Pwd Expiry
     *            Days
     */
    public void setUserPwdExpiryDays(String userPwdExpiryDays) {
        this.userPwdExpiryDays = userPwdExpiryDays;
    }

    /**
     * @return Pwd Reminder to be sent
     */
    public String getUserPwdReminderDate() {
        return this.userPwdReminderDate;
    }

    /**
     * @param userPwdReminderDate
     *            The value that is required to be registered as the Pwd
     *            Reminder to be sent on
     */
    public void setUserPwdReminderDate(String userPwdReminderDate) {
        this.userPwdReminderDate = userPwdReminderDate;
    }

    /**
     * @return E-signature
     */
    public String getUserESign() {
        return this.userESign;
    }

    /**
     * @param userESign
     *            The value that is required to be registered as the E-signature
     */
    public void setUserESign(String userESign) {
        this.userESign = userESign;
    }

    /**
     * @return E-signature Expiry Date
     */
    public String getUserESignExpiryDate() {
        return this.userESignExpiryDate;
    }

    /**
     * @param userESignExpiryDate
     *            The value that is required to be registered as the E-signature
     *            Expiry Date
     */
    public void setUserESignExpiryDate(String userESignExpiryDate) {
        this.userESignExpiryDate = userESignExpiryDate;
    }

    /**
     * @return Pwd Expiry Days
     */
    public String getUserESignExpiryDays() {
        return this.userESignExpiryDays;
    }

    /**
     * @param userESignExpiryDays
     *            The value that is required to be registered as the Pwd Expiry
     *            Days
     */
    public void setUserESignExpiryDays(String userESignExpiryDays) {
        this.userESignExpiryDays = userESignExpiryDays;
    }

    /**
     * @return Pwd Reminder to be sent on
     */
    public String getUserESignReminderDate() {
        return this.userESignReminderDate;
    }

    /**
     * @param userESignReminderDate
     *            The value that is required to be registered as the Pwd
     *            Reminder to be sent on
     */
    public void setUserESignReminderDate(String userESignReminderDate) {
        this.userESignReminderDate = userESignReminderDate;
    }

    /**
     * Returns the value of siteIdentifier.
     */
    public String getSiteIdentifier() {
        return siteIdentifier;
    }

    /**
     * Sets the value of siteIdentifier.
     * 
     * @param siteIdentifier
     *            The value to assign siteIdentifier.
     */
    public void setSiteIdentifier(String siteIdentifier) {
        this.siteIdentifier = siteIdentifier;
    }
    
    
    //KM
    /**
     * Returns the value of siteHidden.
     */
    public String getSiteHidden() {
        return siteHidden;
    }

    /**
     * Sets the value of siteHidden.
     * 
     * @param siteHidden
     *            The value to assign siteHidden.
     */
    public void setSiteHidden(String siteHidden) {
        this.siteHidden = siteHidden;
    }
    

    /**
     * Returns the value of poIdentifier.
     */
    public String getPoIdentifier() {
		return poIdentifier;
	}

    /**
     * Sets the value of poIdentifier.
     * 
     * @param poIdentifier
     *            The value to assign poIdentifier.
     */
	public void setPoIdentifier(String poIdentifier) {
		this.poIdentifier = poIdentifier;
	}
    

    
    
    /**
     * Returns the value of userHidden.
     */
    public String getUserHidden() {
        return userHidden;
    }

    /**
     * Sets the value of userHidden.
     * 
     * @param userHidden
     *            The value to assign userHidden.
     */
    public void setUserHidden(String userHidden) {
        this.userHidden = userHidden;
    }

    // END OF GETTER SETTER METHODS

    /**
     * Creates an account using the values in this object
     */
    //Modified by Manimaran to fix the Bug:2661
    public int createAccount() {
        try {
        	int retAccId =0;
            AccountWrapperAgentRObj accountWrapperAgentRObj = EJBUtil
                    .getAccountWrapperAgentHome();
            retAccId = createAccount(this.createAccountUserWrapperStateKeeper());
            Rlog.debug("accountWrapper",
                    "AccountWrapperJB.setAccountWrapperDetails()");
            return retAccId;
        } catch (Exception e) {
            Rlog.fatal("accountWrapper",
                    "Error in setAccountWrapperDetails() in AccountWrapperJB "
                            + e);
            return -2;
        }
    }

    /**
     * Creates a user using the values in this object
     * 
     * @return 0 - if the create is successful <br>
     *         -2 - if the create fails
     */
    public int createUser() {
        int ret = 0;
        int retUserId = 0;
        String userType = "";

        UserSiteJB usd = new UserSiteJB();
        FormLibJB formJB = new FormLibJB();
        try {

            AccountWrapperAgentRObj accountWrapperAgentRObj = EJBUtil
                    .getAccountWrapperAgentHome();
            retUserId = createUser(this.createAccountUserWrapperStateKeeper());
            if ("LIND".equals(CFG.EIRB_MODE)) {
            	if (retUserId > 0 && "N".equals(this.userType) && StringUtil.isEmpty(this.userLoginName)) {
            		this.userLoginName = "nonsys-"+retUserId;
            		this.modifiedBy = this.creator;
            		updateUser(this.createAccountUserWrapperStateKeeper());
            	}
            }

            Rlog.debug("accountWrapper", "*****retUserId " + retUserId + "***");
            Rlog.debug("accountWrapper", "this.getUserType()::"
                    + this.getUserType() + ":");
            userType = this.getUserType();
            
            this.setUserId(StringUtil.integerToString(retUserId));
            
            if (retUserId > 0 && !userType.equals("N")) {
                String mode = "N";
                // in new mode, set oldsite as 0
                ret = usd.createUserSiteData(retUserId, EJBUtil
                        .stringToNum(this.getUserAccountId()), EJBUtil
                        .stringToNum(this.getUserGrpDefault()), 0, EJBUtil
                        .stringToNum(this.getUserSiteId()), EJBUtil
                        .stringToNum(this.getCreator()), this.getIpAdd(), mode);

            }

            return ret;
        } catch (Exception e) {
            Rlog.fatal("accountWrapper",
                    "Error in create new user  AccountWrapperJB " + e);
            return -2;
        }
    }

    /**
     * Updates a user using the values in this object
     * 
     * @return 0 - if the update is successful <br>
     *         -2 - if the update fails
     */
    public int updateUser() {
        int ret = 0;
        int retVal = 0;
        String preUserSiteId = "";
        String updUserSiteId = "";
        String userType = "";
        UserJB usr = new UserJB();

        // To find the previous userSiteId before updateUser method is called

        usr.setUserId(EJBUtil.stringToNum(this.userId));
        usr.getUserDetails();
        preUserSiteId = usr.getUserSiteId();

        UserSiteJB usd = new UserSiteJB();

        try {

            AccountWrapperAgentRObj accountWrapperAgentRObj = EJBUtil
                    .getAccountWrapperAgentHome();

            retVal = updateUser(this.createAccountUserWrapperStateKeeper());
            // userSiteId after updateUser() method is called
            updUserSiteId = this.getUserSiteId();
            userType = this.getUserType();

            /*
             * If the userSiteId is changed then createUserSiteData() method is
             * called which in turn calls the SP SP_CREATE_USERSITE_DATA to
             * reset all the organization rights ie all the primary organization
             * rights are given and rest are set to 0
             */

            /*
             * This method internally calls sp which has the logic for updating
             * the form share with data also added a new argument old site id,
             * pass old site id as well as new site id
             */
            Rlog.debug("accountWrapper", "this.getUserType()::" + userType
                    + ":");

            if (retVal >= 0 && !updUserSiteId.equals(preUserSiteId)
                    && !userType.equals("N")) {
                String mode = "M";
                ret = usd.createUserSiteData(EJBUtil.stringToNum(this
                        .getUserId()), EJBUtil.stringToNum(this
                        .getUserAccountId()), EJBUtil.stringToNum(this
                        .getUserGrpDefault()), EJBUtil
                        .stringToNum(preUserSiteId), EJBUtil
                        .stringToNum(updUserSiteId), EJBUtil.stringToNum(this
                        .getModifiedBy()), this.getIpAdd(), mode);
            }

            return ret;

        } catch (Exception e) {
            Rlog.fatal("accountWrapper",
                    "Error in update user  AccountWrapperJB " + e);
            return -2;
        }
    }

    /**
     * Creates a site using the values in this object
     * 
     * @return 0 - if the create is successful <br>
     *         -2 - if the create fails
     */
    public int createSite() {
        int output = 0;
        
        try {

        	AccountWrapperAgentRObj accountWrapperAgentRObj = EJBUtil
                    .getAccountWrapperAgentHome();

            // output =
            // accountWrapperAgentRObj.createSite(this.createSiteStateKeeper(),
            // this.createAddStateKeeper());
            output = this.createSite(this.createSiteStateKeeper(), this
                    .createAddStateKeeper());

            Rlog.debug("accountWrapper", "after create new site");
            return output;
        } catch (Exception e) {
            Rlog.fatal("accountWrapper",
                    "Error in create new site  AccountWrapperJB " + e);
            return -2;
        }
    }

    /**
     * Updates a site using the values in this object
     * 
     * @return 0 - if the update is successful <br>
     *         -2 - if the update fails
     */
    public int updateSite() {
        int output = 0;
        try {

            AccountWrapperAgentRObj accountWrapperAgentRObj = EJBUtil
                    .getAccountWrapperAgentHome();
            // output =
            // accountWrapperAgentRObj.updateSite(this.createSiteStateKeeper(),
            // this.createAddStateKeeper());
            output = this.updateSite(this.createSiteStateKeeper(), this
                    .createAddStateKeeper());
            Rlog.debug("accountWrapper", "after update site");
            return output;
        } catch (Exception e) {
            Rlog.fatal("accountWrapper",
                    "Error in update site  AccountWrapperJB " + e);
            e.printStackTrace();
            return -2;
        }
    }

    /**
     * Sets the various state keepers with the user inputs for site creation and
     * calls different sessions bean methods to store these inputs in the
     * database
     * 
     * @param ssk
     *            SiteStateKeeper containing site details ask AddressStateKeeper
     *            containing address details
     * @return void
     * @see SiteStateKeeper
     * @see AddressStateKeeper
     */

    public int createSite(SiteBean ssk, AddressBean ask) {
    	
        int output = 0;
        int removeOutput = 0;
        int cnt = 0;
        String addId = null;
        String siteId = null;
        AddressAgentRObj addressUserAgentRObj = null;
        SiteAgentRObj siteAgent = null;

        Enumeration enum_velos;
        String siteIdentifier = "";
        String siteName = null;//KM
        String ctepid = null;
		String siteNotes = null;
        try {

            // SiteHome siteHome = EJBUtil.getSiteHome();

            siteIdentifier = ssk.getSiteIdentifier();
			siteNotes = ssk.getSiteNotes();
			
            siteName = ssk.getSiteName();
            ctepid = ssk.getCtepId();
            siteAgent = EJBUtil.getSiteAgentHome();
            if (!StringUtil.isEmpty(siteIdentifier)) {
                // check if the site identifier value passed is unique
                cnt = siteAgent.findBySiteIdentifier((ssk
                        .getSiteAccountId()), siteIdentifier);

                if (cnt < 0)
                    return cnt;
                if (cnt > 0) {
                    // site identifier is not unique
                    Rlog
                            .debug("accountWrapper",
                                    "SiteAgentBean.setSiteDetails : the site identifier is not unique");
                    return -3;
                }

            }
            
            //Added by Manimaran to fix the bug2729
            if (!StringUtil.isEmpty(siteName)) {
                // check if the site name value passed is unique
                cnt = siteAgent.findBySiteName((ssk
                        .getSiteAccountId()), siteName);

                if (cnt < 0)
                    return cnt;
                if (cnt > 0) {
                    // site name is not unique
                    Rlog
                            .debug("accountWrapper",
                                    "SiteAgentBean.setSiteDetails : the site name is not unique");
                    return -4;
                }

            }

            addressUserAgentRObj = EJBUtil.getAddressAgentHome();

            addId = String.valueOf(addressUserAgentRObj.setAddressDetails(ask));
            Rlog
                    .debug("accountWrapper",
                            "In AccountWrapperAgentBean.createSite: create  site address  for site");
        } catch (Exception e) {
            Rlog.fatal("accountWrapper",
                    "Error in AccountWrapperAgentBean.createSite  for site  address "
                            + e);
            e.printStackTrace();
            return -2;
        }

        ssk.setSitePerAdd(addId);

        try {
            // siteAgentHome = EJBUtil.getSiteAgentHome();
            SiteAgentRObj siteAgentRObj = EJBUtil.getSiteAgentHome();
            output = siteAgentRObj.setSiteDetails(ssk);
            siteId = String.valueOf(output);
            Rlog.debug("accountWrapper",
                    "In AccountWrapperAgentBean.createSite : add new site");

            /*
             * if (output < 0 ) // the site was not created {
             * Rlog.debug("accountWrapper","In
             * AccountWrapperAgentBean.createSite : deleting the address record
             * as site was not created"); if (addressUserAgentRObj != null) {
             * removeOutput =
             * addressUserAgentRObj.removeAddress(EJBUtil.stringToNum(addId));
             * if (removeOutput == 0) { Rlog.debug("accountWrapper","In
             * AccountWrapperAgentBean.createSite : deleted the address record
             * as site was not created"); } else {
             * Rlog.fatal("accountWrapper","In
             * AccountWrapperAgentBean.createSite : the address record is not
             * deleted (and site is not created)"); } } }
             */

            return output;
        } catch (Exception e) {
            Rlog.fatal("accountWrapper",
                    "Error in AccountWrapperAgentBean.createSite for : add new site  "
                            + e);
            return -2;
        }

    }

    public int updateSite(SiteBean ssk, AddressBean ask) {

        int output = 0;

        try {
            // SiteAgentHome siteAgentHome = EJBUtil.getSiteAgentHome();
            // SiteAgentRObj siteAgentRObj = EJBUtil.getSiteAgentHome();

            // output = siteAgentRObj.updateSite(ssk);

            SiteJB siteJb = new SiteJB();

            siteJb.setSiteId(ssk.getSiteId());
            siteJb.getSiteDetails();

            siteJb.setSitePerAdd(ssk.getSitePerAdd());
            siteJb.setSiteStatus(ssk.getSiteStatus());
            siteJb.setSiteCodelstType(ssk.getSiteCodelstType());
            siteJb.setSiteInfo(ssk.getSiteInfo());
            siteJb.setSiteName(ssk.getSiteName());
            siteJb.setCtepId(ssk.getCtepId());
			siteJb.setSiteNotes(ssk.getSiteNotes());
            siteJb.setSiteIdentifier(ssk.getSiteIdentifier());
            siteJb.setSiteParent(ssk.getSiteParent());
            siteJb.setModifiedBy(ssk.getModifiedBy());
            siteJb.setIpAdd(ssk.getIpAdd());
            siteJb.setSiteHidden(ssk.getSiteHidden());//KM
            siteJb.setPoIdentifier(ssk.getPoIdentifier());
            output = siteJb.updateSite();

            Rlog.debug("accountWrapper",
                    "In AccountWrapperAgentBean.createAccount : add new site");
        } catch (Exception e) {
            Rlog.fatal("accountWrapper",
                    "Error in AccountWrapperAgentBean.createAccount for : add new site  "
                            + e);
            return -2;
        }

        if (output >= 0) // if site was updated
        {
            try {

                AddressJB ad = new AddressJB();

                ad.setAddId(ask.getAddId());
                ad.getAddressDetails();

                ad.setAddPri(ask.getAddPri());
                ad.setAddCity(ask.getAddCity());
                ad.setAddState(ask.getAddState());
                ad.setAddZip(ask.getAddZip());
                ad.setAddCountry(ask.getAddCountry());

                ad.setAddPhone(ask.getAddPhone());
                ad.setAddEmail(ask.getAddEmail());
                ad.setModifiedBy(ask.getModifiedBy());
                ad.setIpAdd(ask.getIpAdd());
                ad.updateAddress();

                Rlog
                        .debug("accountWrapper",
                                "In AccountWrapperAgentBean.updateSite: create  site address  for site");
            } catch (Exception e) {
                Rlog.fatal("accountWrapper",
                        "Error in AccountWrapperAgentBean.createSite  for site  address "
                                + e);
                return -2;
            }
        }
        return output;

    }

    /** ********end of method ********* */

    /**
     * Creates a AccountWrapperStateKeeper using the values in this object
     * 
     * @return an AccountWrapperStateKeeper created using the values in this
     *         object
     * 
     * @see AccountWrapperStateKeeper
     */
    public AccountWrapperStateKeeper createAccountWrapperStateKeeper() {
        Rlog.debug("accountWrapper",
                "AccountWrapperJB.createAccountWrapperStateKeeper ");
        return new AccountWrapperStateKeeper(this.accType, this.accRoleType,
                this.accName, this.accMailFlag, this.accPubFlag, this.accLogo,
                this.accModRight, this.groupName, this.groupDesc,
                this.addTypeUser, this.addPriUser, this.addCityUser,
                this.addStateUser, this.addZipUser, this.addCountryUser,
                this.addCountyUser, this.addEffDateUser, this.addPhoneUser,
                this.addEmailUser, this.addPagerUser, this.addMobileUser,
                this.addFaxUser, this.addTypeSite, this.addPriSite,
                this.addCitySite, this.addStateSite, this.addZipSite,
                this.addCountrySite, this.addCountySite, this.addEffDateSite,
                this.addPhoneSite, this.addEmailSite, this.addPagerSite,
                this.addMobileSite, this.addFaxSite, this.siteCodelstType,
                this.siteName, this.ctepid, this.siteNotes, this.siteInfo, this.siteParent,
                this.userCodelstJobtype, this.userLastName, this.userFirstName,
                this.userMidName, this.userWrkExp, this.userPhaseInv,
                this.userSessionTime, this.userLoginName, this.userPwd,
                this.userSecQues, this.userAnswer, this.userCodelstSpl,
                this.userTimeZoneId, this.msgcntrToUserId, this.userId,
                this.accStatus, this.accStartDate, this.msgcntrStatus,
                this.creator, this.modifiedBy, this.ipAdd,
                this.userPwdExpiryDate, this.userPwdExpiryDays,
                this.userPwdReminderDate, this.userESign,
                this.userESignExpiryDate, this.userESignExpiryDays,
                this.userESignReminderDate, this.userType,this.userLoginModuleId,
                this.userLoginModuleMap, this.siteHidden, this.userHidden, this.poIdentifier);

    }

    /**
     * Creates a SiteStateKeeper using the values in this object
     * 
     * @return an SiteStateKeeper created using the values in this object
     * 
     * @see SiteStateKeeper
     */
    public SiteBean createSiteStateKeeper() {
        Rlog.debug("accountWrapper", "AccountWrapperJB.createSiteStateKeeper ");

        SiteBean ssk = new SiteBean();

        ssk.setSiteId(EJBUtil.stringToNum(this.siteId));
        ssk.setSiteCodelstType(this.siteCodelstType);
        ssk.setSiteAccountId(this.userAccountId);
        ssk.setSiteName(this.siteName);
        ssk.setCtepId(this.ctepid);
		ssk.setSiteNotes(this.siteNotes);
        ssk.setSiteInfo(this.siteInfo);
        ssk.setSiteStatus(this.siteStatus);
        ssk.setSiteParent(this.siteParent);
        ssk.setCreator(this.creator);
        ssk.setModifiedBy(this.modifiedBy);
        ssk.setIpAdd(this.ipAdd);
        ssk.setSiteIdentifier(this.siteIdentifier);
        ssk.setSiteHidden(this.siteHidden); //KM
        ssk.setPoIdentifier(this.poIdentifier);
        return ssk;

    }

    /**
     * Creates a AddressStateKeeper using the values in this object
     * 
     * @return an AddressStateKeeper created using the values in this object
     * 
     * @see AddressStateKeeper
     */
    public AddressBean createAddStateKeeper() {
        Rlog.debug("accountWrapper",
                "AccountWrapperJB.createAddressStateKeeper ");

        AddressBean addressSKSite = new AddressBean();

        /*
         * Rlog.debug("accountWrapper","AccountWrapperJB.createAddressStateKeeper
         * sitePerAdd " + EJBUtil.stringToNum(this.sitePerAdd));
         * Rlog.debug("accountWrapper","AccountWrapperJB.createAddressStateKeeper
         * addTypeSite " + this.addTypeSite);
         * Rlog.debug("accountWrapper","AccountWrapperJB.createAddressStateKeeper
         * addPriSite " + this.addPriSite);
         * Rlog.debug("accountWrapper","AccountWrapperJB.createAddressStateKeeper
         * addCitySite " + this.addCitySite);
         * Rlog.debug("accountWrapper","AccountWrapperJB.createAddressStateKeeper
         * addStateSite " + this.addStateSite);
         * Rlog.debug("accountWrapper","AccountWrapperJB.createAddressStateKeeper
         * addZipSite " + this.addZipSite);
         * Rlog.debug("accountWrapper","AccountWrapperJB.createAddressStateKeeper
         * addCountrySite " + this.addCountrySite);
         * Rlog.debug("accountWrapper","AccountWrapperJB.createAddressStateKeeper
         * addCountySite " + this.addCountySite);
         * Rlog.debug("accountWrapper","AccountWrapperJB.createAddressStateKeeper
         * addEffDateSite " + this.addEffDateSite);
         * Rlog.debug("accountWrapper","AccountWrapperJB.createAddressStateKeeper
         * addPhoneSite " + this.addPhoneSite);
         * Rlog.debug("accountWrapper","AccountWrapperJB.createAddressStateKeeper
         * addEmailSite " + this.addEmailSite);
         * Rlog.debug("accountWrapper","AccountWrapperJB.createAddressStateKeeper
         * addPagerSite " + this.addPagerSite);
         * Rlog.debug("accountWrapper","AccountWrapperJB.createAddressStateKeeper
         * addMobileSite " + this.addMobileSite);
         * Rlog.debug("accountWrapper","AccountWrapperJB.createAddressStateKeeper
         * addFaxSite " + this.addFaxSite);
         */

        addressSKSite.setAddId(EJBUtil.stringToNum(this.sitePerAdd));
        addressSKSite.setAddType(this.addTypeSite);
        addressSKSite.setAddPri(this.addPriSite);
        addressSKSite.setAddCity(this.addCitySite);
        addressSKSite.setAddState(this.addStateSite);
        addressSKSite.setAddZip(this.addZipSite);
        addressSKSite.setAddCountry(this.addCountrySite);
        addressSKSite.setAddCounty(this.addCountySite);
        addressSKSite.setAddEffDate(this.addEffDateSite);
        addressSKSite.setAddPhone(this.addPhoneSite);
        addressSKSite.setAddEmail(this.addEmailSite);
        addressSKSite.setAddPager(this.addPagerSite);
        addressSKSite.setAddMobile(this.addMobileSite);
        addressSKSite.setAddFax(this.addFaxSite);
        addressSKSite.setCreator(this.creator);
        addressSKSite.setModifiedBy(this.modifiedBy);
        addressSKSite.setIpAdd(this.ipAdd);

        return addressSKSite;
    }

    /**
     * Creates a AccountWrapperStateKeeper using the values in this object. This
     * also sets the user details in the AccountWrapperStateKeeper
     * 
     * @return an AccountWrapperStateKeeper created using the values in this
     *         object
     * 
     * @see AccountWrapperStateKeeper
     */
    public AccountWrapperStateKeeper createAccountUserWrapperStateKeeper() {
        AccountWrapperStateKeeper ask;

        Rlog.debug("accountWrapper",
                "AccountWrapperJB.createAccountWrapperStateKeeper ");
        ask = new AccountWrapperStateKeeper(this.accType, this.accRoleType,
                this.accName, this.accMailFlag, this.accPubFlag, this.accLogo,
                this.accModRight, this.groupName, this.groupDesc,
                this.addTypeUser, this.addPriUser, this.addCityUser,
                this.addStateUser, this.addZipUser, this.addCountryUser,
                this.addCountyUser, this.addEffDateUser, this.addPhoneUser,
                this.addEmailUser, this.addPagerUser, this.addMobileUser,
                this.addFaxUser, this.addTypeSite, this.addPriSite,
                this.addCitySite, this.addStateSite, this.addZipSite,
                this.addCountrySite, this.addCountySite, this.addEffDateSite,
                this.addPhoneSite, this.addEmailSite, this.addPagerSite,
                this.addMobileSite, this.addFaxSite, this.siteCodelstType,
                this.siteName, this.ctepid, this.siteNotes, this.siteInfo, this.siteParent,
                this.userCodelstJobtype, this.userLastName, this.userFirstName,
                this.userMidName, this.userWrkExp, this.userPhaseInv,
                this.userSessionTime, this.userLoginName, this.userPwd,
                this.userSecQues, this.userAnswer, this.userCodelstSpl,
                this.userTimeZoneId, this.msgcntrToUserId, this.userId,
                this.accStatus, this.accStartDate, this.msgcntrStatus,
                this.creator, this.modifiedBy, this.ipAdd,
                this.userPwdExpiryDate, this.userPwdExpiryDays,
                this.userPwdReminderDate, this.userESign,
                this.userESignExpiryDate, this.userESignExpiryDays,
                this.userESignReminderDate, this.userType,this.userLoginModuleId,
                this.userLoginModuleMap,this.siteHidden, this.userHidden, this.poIdentifier);

        ask.setUserGrpDefault(getUserGrpDefault());
        ask.setUserStatus(getUserStatus());
        ask.setUserAccountId(getUserAccountId());
        ask.setUserSiteId(getUserSiteId());
        ask.setUserPerAddressId(getUserPerAddressId());
        ask.setUserSiteFlag(getUserSiteFlag());
        // ask.setUserType(getUserType());
        return ask;

    }

    /**
     * Populates JB with user details
     * 
     * @return 0 - if the create is successful <br>
     *         -2 - if the create fails
     */
    public void populateUser() {
        UserBean usk = null;
        AddressBean addsk = null;

        try {

            UserAgentRObj userAgent = EJBUtil.getUserAgentHome();
            usk = userAgent.getUserDetails(EJBUtil.stringToNum(this.userId));
            Rlog.debug("accountWrapper",
                    "accountWrapperJB.getuser UserStateKeeper " + usk);
        } catch (Exception e) {
            Rlog.fatal("accountWrapper",
                    "Exception in getUser in accountWrapper " + e);
        }
        if (usk != null) {
            setUserId(String.valueOf(usk.getUserId()));
            setUserCodelstJobtype(usk.getUserCodelstJobtype());
            setUserLastName(usk.getUserLastName());
            setUserFirstName(usk.getUserFirstName());
            setUserMidName(usk.getUserMidName());
            setUserWrkExp(usk.getUserWrkExp());
            setUserPhaseInv(usk.getUserPhaseInv());
            setUserSessionTime(usk.getUserSessionTime());
            setUserLoginName(usk.getUserLoginName());
            setUserPwd(usk.getUserPwd());
            setUserSecQues(usk.getUserSecQues());
            setUserAnswer(usk.getUserAnswer());
            setUserCodelstSpl(usk.getUserCodelstSpl());
            setUserGrpDefault(usk.getUserGrpDefault());
            setUserTimeZoneId(usk.getTimeZoneId());
            setUserStatus(usk.getUserStatus());
            setUserAccountId(usk.getUserAccountId());
            setUserSiteId(usk.getUserSiteId());
            setUserPerAddressId(usk.getUserPerAddressId());
            setIpAdd(usk.getIpAdd());
            setModifiedBy(usk.getModifiedBy());
            setUserPwdExpiryDate(usk.getUserPwdExpiryDate());
            setUserPwdExpiryDays(usk.getUserPwdExpiryDays());
            setUserPwdReminderDate(usk.getUserPwdReminderDate());
            setUserESign(usk.getUserESign());
            setUserESignExpiryDate(usk.getUserESignExpiryDate());
            setUserESignExpiryDays(usk.getUserESignExpiryDays());
            setUserESignReminderDate(usk.getUserESignReminderDate());
            setUserSiteFlag(usk.getUserSiteFlag());
            setUserType(usk.getUserType());
            this.setUserLoginModuleId(usk.getUserLoginModule());
            this.setUserLoginModuleMap(usk.getUserLoginModuleMap());
            setUserHidden(usk.getUserHidden());

        }

        // populate address information

        try {
            AddressAgentRObj addressAgentRObj = EJBUtil.getAddressAgentHome();
            addsk = addressAgentRObj.getAddressDetails(EJBUtil
                    .stringToNum(getUserPerAddressId()));
            Rlog.debug("accountWrapper",
                    "accountWrapper.getAddressDetails() AddressStateKeeper "
                            + addsk);
        } catch (Exception e) {
            Rlog.fatal("accountWrapper",
                    "Fatal in getAddressDetails() in accountWrapper " + e);
        }
        if (addsk != null) {

            setAddTypeUser(addsk.getAddType());
            setAddPriUser(addsk.getAddPri());
            setAddCityUser(addsk.getAddCity());
            setAddStateUser(addsk.getAddState());
            setAddZipUser(addsk.getAddZip());
            setAddCountryUser(addsk.getAddCountry());
            setAddCountyUser(addsk.getAddCounty());
            setAddEffDateUser(addsk.getAddEffDate());
            setAddPhoneUser(addsk.getAddPhone());
            setAddEmailUser(addsk.getAddEmail());
            setAddPagerUser(addsk.getAddPager());
            setAddMobileUser(addsk.getAddMobile());
            setAddFaxUser(addsk.getAddFax());

        }

    }

    /**
     * Sets the various state keepers with the user inputs for account creation
     * and calls different sessions bean methods to store these inputs in the
     * database
     * 
     * @param awsk
     *            AccountWrapperStateKeeper containing accountWrapper attributes
     *            to be set.
     * @return void
     * @see AccountWrapperStateKeeper
     */
    //Modified by Manimaran to fix the Bug:2661
    public int createAccount(AccountWrapperStateKeeper awsk) {
        String accId = null;
        String groupId = null;
        String siteAddId = null;
        String siteId = null;
        String userAddId = null;
        String userId = null;
        String usrGrpId = null;
        String msgcntrId = null;
        String rights = "";

        String ip = null;

        ip = awsk.getIpAdd();

        // AccountStateKeeper accountSK = new AccountStateKeeper();
        AccountBean accountBean = new AccountBean();
        GroupBean groupSK = new GroupBean();
        AddressBean addressSKUser = new AddressBean();
        AddressBean addressSKSite = new AddressBean();
        SiteBean siteSK = new SiteBean();
        // UserStateKeeper userSK = new UserStateKeeper();
        UserBean userSK = new UserBean();
        UsrGrpBean usrGrpSK = new UsrGrpBean();
        MsgcntrBean msgcntrSK = new MsgcntrBean();
        GrpRightsBean grpRightsSK = new GrpRightsBean();
        CtrlDao ctrl = new CtrlDao();

        accountBean.setAccType(awsk.getAccType());
        accountBean.setAccRoleType(awsk.getAccRoleType());
        accountBean.setAccName(awsk.getAccName());
        accountBean.setAccMailFlag(awsk.getAccMailFlag());
        accountBean.setAccPubFlag(awsk.getAccPubFlag());
        accountBean.setAccLogo(awsk.getAccLogo());
        accountBean.setAccModRight(awsk.getAccModRight());
        accountBean.setAccStatus(awsk.getAccStatus());
        accountBean.setAccStartDate(awsk.getAccStartDate());
        accountBean.setIpAdd(awsk.getIpAdd());

        groupSK.setGroupName(awsk.getGroupName());
        groupSK.setGroupDesc(awsk.getGroupDesc());
        groupSK.setIpAdd(awsk.getIpAdd());

        addressSKUser.setAddType(awsk.getAddTypeUser());
        addressSKUser.setAddPri(awsk.getAddPriUser());
        addressSKUser.setAddCity(awsk.getAddCityUser());
        addressSKUser.setAddState(awsk.getAddStateUser());
        addressSKUser.setAddZip(awsk.getAddZipUser());
        addressSKUser.setAddCountry(awsk.getAddCountryUser());
        addressSKUser.setAddCounty(awsk.getAddCountyUser());
        addressSKUser.setAddEffDate(awsk.getAddEffDateUser());
        addressSKUser.setAddPhone(awsk.getAddPhoneUser());
        addressSKUser.setAddEmail(awsk.getAddEmailUser());
        addressSKUser.setAddPager(awsk.getAddPagerUser());
        addressSKUser.setAddMobile(awsk.getAddMobileUser());
        addressSKUser.setAddFax(awsk.getAddFaxUser());
        addressSKUser.setIpAdd(awsk.getIpAdd());

        addressSKSite.setAddType(awsk.getAddTypeSite());
        addressSKSite.setAddPri(awsk.getAddPriSite());
        addressSKSite.setAddCity(awsk.getAddCitySite());
        addressSKSite.setAddState(awsk.getAddStateSite());
        addressSKSite.setAddZip(awsk.getAddZipSite());
        addressSKSite.setAddCountry(awsk.getAddCountrySite());
        addressSKSite.setAddCounty(awsk.getAddCountySite());
        addressSKSite.setAddEffDate(awsk.getAddEffDateSite());
        addressSKSite.setAddPhone(awsk.getAddPhoneSite());
        addressSKSite.setAddEmail(awsk.getAddEmailSite());
        addressSKSite.setAddPager(awsk.getAddPagerSite());
        addressSKSite.setAddMobile(awsk.getAddMobileSite());
        addressSKSite.setAddFax(awsk.getAddFaxSite());
        addressSKSite.setIpAdd(awsk.getIpAdd());

        siteSK.setSiteCodelstType(awsk.getSiteCodelstType());
        siteSK.setSiteName(awsk.getSiteName());
        siteSK.setCtepId(awsk.getCtepId());
		siteSK.setSiteNotes(awsk.getSiteNotes());  
        siteSK.setSiteInfo(awsk.getSiteInfo());
        siteSK.setSiteParent(awsk.getSiteParent());
        siteSK.setIpAdd(awsk.getIpAdd());
        siteSK.setSiteHidden(awsk.getSiteHidden());
        siteSK.setPoIdentifier(awsk.getPoIdentifier());

        userSK.setUserCodelstJobtype(awsk.getUserCodelstJobtype());
        userSK.setUserLastName(awsk.getUserLastName());
        userSK.setUserFirstName(awsk.getUserFirstName());
        userSK.setUserMidName(awsk.getUserMidName());
        userSK.setUserWrkExp(awsk.getUserWrkExp());
        userSK.setUserPhaseInv(awsk.getUserPhaseInv());
        userSK.setUserSessionTime(awsk.getUserSessionTime());
        userSK.setUserLoginName(awsk.getUserLoginName());
        userSK.setUserPwd(awsk.getUserPwd());
        userSK.setUserSecQues(awsk.getUserSecQues());
        userSK.setUserAnswer(awsk.getUserAnswer());
        userSK.setUserCodelstSpl(awsk.getUserCodelstSpl());
        userSK.setUserType(awsk.getUserType());
        userSK.setTimeZoneId(awsk.getUserTimeZoneId());

        userSK.setUserStatus(awsk.getUserStatus());
        userSK.setUserPwdExpiryDate(awsk.getUserPwdExpiryDate());
        userSK.setUserPwdExpiryDays(awsk.getUserPwdExpiryDays());
        userSK.setUserPwdReminderDate(awsk.getUserPwdReminderDate());
        userSK.setUserESign(awsk.getUserESign());
        userSK.setUserESignExpiryDate(awsk.getUserESignExpiryDate());
        userSK.setUserESignExpiryDays(awsk.getUserESignExpiryDays());
        userSK.setUserESignReminderDate(awsk.getUserESignReminderDate());
        userSK.setIpAdd(awsk.getIpAdd());
        userSK.setUserHidden(awsk.getUserHidden());

        msgcntrSK.setMsgcntrToUserId(awsk.getMsgcntrToUserId());
        msgcntrSK.setMsgcntrStatus(awsk.getMsgcntrStatus());
        msgcntrSK.setIpAdd(awsk.getIpAdd());

        // Rlog.debug("accountWrapper","In AccountWrapperAgentBean
        // accountSK.getAccType() " + accountSK.getAccType());

        try {
            AccountAgentRObj accountAgentRObj = EJBUtil.getAccountAgentHome();
            accId = String.valueOf(accountAgentRObj
                    .setAccountDetails(accountBean));
            Rlog.debug("accountWrapper",
                    "In AccountWrapperAgentBean.createAccount for account");
        } catch (Exception e) {
            Rlog.fatal("accountWrapper",
                    "Error in AccountWrapperAgentBean.createAccount for account "
                            + e);
        }

        groupSK.setGroupAccId(accId);

        try {
            GroupAgentRObj groupAgentRObj = EJBUtil.getGroupAgentHome();
            ;
            groupId = String.valueOf(groupAgentRObj.setGroupDetails(groupSK));
            Rlog.debug("accountWrapper",
                    "In AccountWrapperAgentBean.createAccount for group");
        } catch (Exception e) {
            Rlog.fatal("accountWrapper",
                    "Error in AccountWrapperAgentBean.createAccount for group "
                            + e);
        }

        try {
            AddressAgentRObj addressSiteAgentRObj = EJBUtil
                    .getAddressAgentHome();
            siteAddId = String.valueOf(addressSiteAgentRObj
                    .setAddressDetails(addressSKSite));
            Rlog
                    .debug("accountWrapper",
                            "In AccountWrapperAgentBean.createAccount for site address");
        } catch (Exception e) {
            Rlog.fatal("accountWrapper",
                    "Error in AccountWrapperAgentBean.createAccount for site address "
                            + e);
        }

        siteSK.setSitePerAdd(siteAddId);
        siteSK.setSiteAccountId(accId);

        try {
            // SiteAgentHome siteAgentHome = EJBUtil.getSiteAgentHome();
            // SiteAgentRObj siteAgentRObj = siteAgentHome.create();
            SiteAgentRObj siteAgentRObj = EJBUtil.getSiteAgentHome();
            siteId = String.valueOf(siteAgentRObj.setSiteDetails(siteSK));
            Rlog.debug("accountWrapper",
                    "In AccountWrapperAgentBean.createAccount for site");
        } catch (Exception e) {
            Rlog.fatal("accountWrapper",
                    "Error in AccountWrapperAgentBean.createAccount for site "
                            + e);
        }

        try {

            AddressAgentRObj addressUserAgentRObj = EJBUtil
                    .getAddressAgentHome();
            userAddId = String.valueOf(addressUserAgentRObj
                    .setAddressDetails(addressSKUser));
            Rlog
                    .debug("accountWrapper",
                            "In AccountWrapperAgentBean.createAccount for user address");
        } catch (Exception e) {
            Rlog.fatal("accountWrapper",
                    "Error in AccountWrapperAgentBean.createAccount for user address "
                            + e);
        }

        userSK.setUserAccountId(accId);
        userSK.setUserSiteId(siteId);
        userSK.setUserPerAddressId(userAddId);
        userSK.setUserGrpDefault(groupId);

        try {
            UserAgentRObj userAgentRObj = EJBUtil.getUserAgentHome();
            userId = String.valueOf(userAgentRObj.setUserDetails(userSK));
            Rlog.debug("accountWrapper",
                    "In AccountWrapperAgentBean.createAccount for user");
        } catch (Exception e) {
            Rlog.fatal("accountWrapper",
                    "Error in AccountWrapperAgentBean.createAccount for user "
                            + e);
        }

        usrGrpSK.setUsrGrpUserId(userId);
        usrGrpSK.setUsrGrpGroupId(groupId);
        usrGrpSK.setIpAdd(awsk.getIpAdd());

        try {
            UsrGrpAgentRObj usrGrpAgentRObj = EJBUtil.getUsrGrpAgentHome();
            usrGrpId = String.valueOf(usrGrpAgentRObj
                    .setUsrGrpDetails(usrGrpSK));
            Rlog.debug("accountWrapper",
                    "In AccountWrapperAgentBean.createAccount for usrGrp");
        } catch (Exception e) {
            Rlog.fatal("accountWrapper",
                    "Error in AccountWrapperAgentBean.createAccount for usrGrp "
                            + e);
        }

        msgcntrSK.setMsgcntrFromUserId(userId);

        try {
            MsgcntrAgentRObj msgcntrAgentRObj = EJBUtil.getMsgcntrAgentHome();
            msgcntrId = String.valueOf(msgcntrAgentRObj
                    .setMsgcntrDetails(msgcntrSK));
            Rlog.debug("accountWrapper",
                    "In AccountWrapperAgentBean.createAccount for msgcntr");
        } catch (Exception e) {
            Rlog.fatal("accountWrapper",
                    "Error in AccountWrapperAgentBean.createAccount for msgcntr "
                            + e);
        }

        ctrl.getControlValues("app_rights");
        int rows = ctrl.getCRows();
        for (int count = 0; count < rows; count++) {
            rights = rights + "7";
        }

        Rlog.debug("Default Group ID of User", groupId + " " + rights);
        grpRightsSK.setId(EJBUtil.stringToNum(groupId));
        grpRightsSK.setFtrRights(rights);
        grpRightsSK.setIpAdd(awsk.getIpAdd());

        try {
            GrpRightsAgentRObj grpRightsAgentRObj = EJBUtil
                    .getGrpRightsAgentHome();
            grpRightsAgentRObj.updateGrpRights(grpRightsSK);
            Rlog.debug("accountWrapper",
                    "In AccountWrapperAgentBean.createGroupRights for group");
        } catch (Exception e) {
            Rlog.fatal("accountWrapper",
                    "Error in AccountWrapperAgentBean.createGroupRights for group "
                            + e);
        }

        try {
            AccountAgentRObj accountAgentRObj = EJBUtil.getAccountAgentHome();
            accountBean = accountAgentRObj
                    .getAccountDetails(new Integer(accId));
            accountBean.setAccCreator(userId);
            int ret = accountAgentRObj.updateAccount(accountBean);
            Rlog.debug("accountWrapper",
                    "In AccountWrapperAgentBean.createAccount for account update ret "
                            + ret);
        } catch (Exception e) {
            Rlog.fatal("accountWrapper",
                    "Error in AccountWrapperAgentBean.createAccount for account update"
                            + e);
        }

        UserSiteJB usb = new UserSiteJB();

        usb.setUserSiteUserId(userId);
        usb.setUserSiteSiteId(siteId);
        usb.setUserSiteRight("7");
        usb.setIpAdd(ip);
        usb.setUserSiteDetails();
        return EJBUtil.stringToNum(accId);//KM

    }

    /**
     * Sets the various state keepers with the user inputs for user creation and
     * calls different sessions bean methods to store these inputs in the
     * database
     * 
     * @param awsk
     *            AccountWrapperStateKeeper containing accountWrapper attributes
     *            to be set.
     * @return void
     * @see AccountWrapperStateKeeper
     */

    public int createUser(AccountWrapperStateKeeper awsk) {

        String userAddId = null;
        String userId = null;
        String usrGrpId = null;
        int ret = 0;

        AddressBean addressSKUser = new AddressBean();
        // UserStateKeeper userSK = new UserStateKeeper();
        UserBean userSK = new UserBean();
        UsrGrpBean usrGrpSK = new UsrGrpBean();

        addressSKUser.setAddType(awsk.getAddTypeUser());
        addressSKUser.setAddPri(awsk.getAddPriUser());
        addressSKUser.setAddCity(awsk.getAddCityUser());
        addressSKUser.setAddState(awsk.getAddStateUser());
        addressSKUser.setAddZip(awsk.getAddZipUser());
        addressSKUser.setAddCountry(awsk.getAddCountryUser());
        addressSKUser.setAddCounty(awsk.getAddCountyUser());
        addressSKUser.setAddEffDate(awsk.getAddEffDateUser());
        addressSKUser.setAddPhone(awsk.getAddPhoneUser());
        addressSKUser.setAddEmail(awsk.getAddEmailUser());
        addressSKUser.setAddPager(awsk.getAddPagerUser());
        addressSKUser.setAddMobile(awsk.getAddMobileUser());
        addressSKUser.setAddFax(awsk.getAddFaxUser());
        addressSKUser.setIpAdd(awsk.getIpAdd());
        addressSKUser.setCreator(awsk.getCreator());

        userSK.setUserCodelstJobtype(awsk.getUserCodelstJobtype());
        userSK.setUserLastName(awsk.getUserLastName());
        userSK.setUserFirstName(awsk.getUserFirstName());
        userSK.setUserMidName(awsk.getUserMidName());
        userSK.setUserWrkExp(awsk.getUserWrkExp());
        userSK.setUserPhaseInv(awsk.getUserPhaseInv());
        userSK.setUserSessionTime(awsk.getUserSessionTime());
        userSK.setUserLoginName(awsk.getUserLoginName());
        userSK.setUserPwd(awsk.getUserPwd());
        userSK.setUserSecQues(awsk.getUserSecQues());
        userSK.setUserAnswer(awsk.getUserAnswer());
        userSK.setUserCodelstSpl(awsk.getUserCodelstSpl());
        userSK.setTimeZoneId(awsk.getUserTimeZoneId());
        userSK.setUserType(awsk.getUserType());

        userSK.setUserGrpDefault(awsk.getUserGrpDefault());
        userSK.setUserStatus(awsk.getUserStatus());
        userSK.setUserAccountId(awsk.getUserAccountId());
        userSK.setUserSiteId(awsk.getUserSiteId());
        userSK.setIpAdd(awsk.getIpAdd());
        userSK.setCreator(awsk.getCreator());
        userSK.setUserPwdExpiryDate(awsk.getUserPwdExpiryDate());
        userSK.setUserPwdExpiryDays(awsk.getUserPwdExpiryDays());
        userSK.setUserPwdReminderDate(awsk.getUserPwdReminderDate());
        userSK.setUserESign(awsk.getUserESign());
        userSK.setUserESignExpiryDate(awsk.getUserESignExpiryDate());
        userSK.setUserESignExpiryDays(awsk.getUserESignExpiryDays());
        userSK.setUserESignReminderDate(awsk.getUserESignReminderDate());
        userSK.setUserLoginModule(awsk.getUserLoginModuleId());
        userSK.setUserLoginModuleMap(awsk.getUserLoginModuleMap());
        userSK.setUserHidden(awsk.getUserHidden());

        try {
            AddressAgentRObj addressUserAgentRObj = EJBUtil
                    .getAddressAgentHome();
            userAddId = String.valueOf(addressUserAgentRObj
                    .setAddressDetails(addressSKUser));
            Rlog.debug("accountWrapper",
                    "In AccountWrapperAgentBean.createUSer for user address");
        } catch (Exception e) {
            Rlog.fatal("accountWrapper",
                    "Error in AccountWrapperAgentBean.createUser for user address "
                            + e);
            return -2;
        }
        userSK.setUserPerAddressId(userAddId);

        try {
            UserAgentRObj userAgentRObj = EJBUtil.getUserAgentHome();
            userId = String.valueOf(userAgentRObj.setUserDetails(userSK));
            Rlog.debug("accountWrapper",
                    "In AccountWrapperAgentBean.createUser for user");
        } catch (Exception e) {
            Rlog
                    .fatal("accountWrapper",
                            "Error in AccountWrapperAgentBean.createUser for user "
                                    + e);
            return -2;
        }
        usrGrpSK.setUsrGrpUserId(userId);
        usrGrpSK.setUsrGrpGroupId(awsk.getUserGrpDefault());
        usrGrpSK.setIpAdd(awsk.getIpAdd());
        usrGrpSK.setCreator(awsk.getCreator());

        try {
            UsrGrpAgentRObj usrGrpAgentRObj = EJBUtil.getUsrGrpAgentHome();
            usrGrpId = String.valueOf(usrGrpAgentRObj
                    .setUsrGrpDetails(usrGrpSK));
            Rlog.debug("accountWrapper",
                    "In AccountWrapperAgentBean.createUserGroup for user"
                            + usrGrpId);
        } catch (Exception e) {
            Rlog.fatal("accountWrapper",
                    "Error in AccountWrapperAgentBean.createUserGroup for user "
                            + e);
            return -2;
        }
//APR-04-2011,MSG-TEMPLAGE FOR USER INFO
        this.setUserId(userId);
        return EJBUtil.stringToNum(userId);
    }

    /**
     * Sets the various state keepers with the user inputs for user updation and
     * calls different sessions bean methods to store these inputs in the
     * database
     * 
     * @param awsk
     *            AccountWrapperStateKeeper containing accountWrapper attributes
     *            to be set.
     * @return void
     * @see AccountWrapperStateKeeper
     */

    public int updateUser(AccountWrapperStateKeeper awsk) {

        // UserStateKeeper userSK = new UserStateKeeper();
        // AddressStateKeeper addressSKUser = new AddressStateKeeper();

        UserJB userSK = new UserJB();
        AddressJB addressSKUser = new AddressJB();
        int output;

        /*
         * Rlog.debug("accountWrapper","In AccountWrapperAgentBean.createUSer
         * for user address EJBUtil.stringToNum(awsk.getUserId()) " +
         * EJBUtil.stringToNum(awsk.getUserId()));
         * Rlog.debug("accountWrapper","In AccountWrapperAgentBean.createUSer
         * for user address awsk.getUserCodelstJobtype() " +
         * awsk.getUserCodelstJobtype()); Rlog.debug("accountWrapper","In
         * AccountWrapperAgentBean.createUSer for user address
         * awsk.getUserLastName() " + awsk.getUserLastName());
         * Rlog.debug("accountWrapper","In AccountWrapperAgentBean.createUSer
         * for user address awsk.getUserFirstName() " +
         * awsk.getUserFirstName()); Rlog.debug("accountWrapper","In
         * AccountWrapperAgentBean.createUSer for user address
         * awsk.getUserMidName() " + awsk.getUserMidName());
         * Rlog.debug("accountWrapper","In AccountWrapperAgentBean.createUSer
         * for user address awsk.getUserWrkExp() " + awsk.getUserWrkExp());
         * Rlog.debug("accountWrapper","In AccountWrapperAgentBean.createUSer
         * for user address awsk.getUserPhaseInv() " + awsk.getUserPhaseInv());
         * Rlog.debug("accountWrapper","In AccountWrapperAgentBean.createUSer
         * for user address awsk.getUserSessionTime() " +
         * awsk.getUserSessionTime()); Rlog.debug("accountWrapper","In
         * AccountWrapperAgentBean.createUSer for user address
         * awsk.getUserLoginName() " + awsk.getUserLoginName());
         * Rlog.debug("accountWrapper","In AccountWrapperAgentBean.createUSer
         * for user address awsk.getUserPwd() " + awsk.getUserPwd());
         * Rlog.debug("accountWrapper","In AccountWrapperAgentBean.createUSer
         * for user address awsk.getUserSecQues() " + awsk.getUserSecQues());
         * Rlog.debug("accountWrapper","In AccountWrapperAgentBean.createUSer
         * for user address awsk.getUserAnswer() " + awsk.getUserAnswer());
         * Rlog.debug("accountWrapper","In AccountWrapperAgentBean.createUSer
         * for user address awsk.getUserCodelstSpl() " +
         * awsk.getUserCodelstSpl()); Rlog.debug("accountWrapper","In
         * AccountWrapperAgentBean.createUSer for user address
         * awsk.getUserGrpDefault() " + awsk.getUserGrpDefault());
         * Rlog.debug("accountWrapper","In AccountWrapperAgentBean.createUSer
         * for user address awsk.getUserStatus() " + awsk.getUserStatus());
         * Rlog.debug("accountWrapper","In AccountWrapperAgentBean.createUSer
         * for user address awsk.getUserAccountId() " +
         * awsk.getUserAccountId()); Rlog.debug("accountWrapper","In
         * AccountWrapperAgentBean.createUSer for user address
         * awsk.getUserSiteId() " + awsk.getUserSiteId());
         */

        try {

            userSK.setUserId(EJBUtil.stringToNum(awsk.getUserId()));

            userSK.getUserDetails();

            userSK.setUserCodelstJobtype(awsk.getUserCodelstJobtype());
            userSK.setUserLastName(awsk.getUserLastName());
            userSK.setUserFirstName(awsk.getUserFirstName());
            userSK.setUserMidName(awsk.getUserMidName());
            userSK.setUserWrkExp(awsk.getUserWrkExp());
            userSK.setUserPhaseInv(awsk.getUserPhaseInv());
            userSK.setUserSessionTime(awsk.getUserSessionTime());
            userSK.setUserLoginName(awsk.getUserLoginName());
            userSK.setUserPwd(awsk.getUserPwd());
            userSK.setUserSecQues(awsk.getUserSecQues());
            userSK.setUserAnswer(awsk.getUserAnswer());
            userSK.setUserCodelstSpl(awsk.getUserCodelstSpl());
            userSK.setUserGrpDefault(awsk.getUserGrpDefault());
            userSK.setTimeZoneId(awsk.getUserTimeZoneId());
            userSK.setUserStatus(awsk.getUserStatus());
            userSK.setUserAccountId(awsk.getUserAccountId());
            userSK.setUserSiteId(awsk.getUserSiteId());
            userSK.setUserPerAddressId(userSK.getUserPerAddressId());
            userSK.setUserType(awsk.getUserType());
            userSK.setIpAdd(awsk.getIpAdd());
            userSK.setModifiedBy(awsk.getModifiedBy());
            userSK.setUserPwdExpiryDate(awsk.getUserPwdExpiryDate());
            userSK.setUserPwdExpiryDays(awsk.getUserPwdExpiryDays());
            userSK.setUserPwdReminderDate(awsk.getUserPwdReminderDate());
            userSK.setUserESign(awsk.getUserESign());
            userSK.setUserESignExpiryDate(awsk.getUserESignExpiryDate());
            userSK.setUserESignExpiryDays(awsk.getUserESignExpiryDays());
            userSK.setUserESignReminderDate(awsk.getUserESignReminderDate());
            userSK.setUserSiteFlag(awsk.getUserSiteFlag());
            userSK.setUserType(awsk.getUserType());
            userSK.setUserLoginModule(awsk.getUserLoginModuleId());
            userSK.setUserLoginModuleMap(awsk.getUserLoginModuleMap());
            userSK.setUserHidden(awsk.getUserHidden()); //KM

            output = userSK.updateUser();
            
            
           
              if(output==0){
            	  UserDao userDao=new UserDao();
            	  int rowImpact=0;
            	 
            	  int userId=Integer.parseInt(userSK.getModifiedBy());
            	
            	  rowImpact= userDao.updateAuditRow(userId);
          	    
              }
            addressSKUser.setAddId(EJBUtil.stringToNum(awsk
                    .getUserPerAddressId()));
            addressSKUser.getAddressDetails();

            addressSKUser.setAddType(awsk.getAddTypeUser());
            addressSKUser.setAddPri(awsk.getAddPriUser());
            addressSKUser.setAddCity(awsk.getAddCityUser());
            addressSKUser.setAddState(awsk.getAddStateUser());
            addressSKUser.setAddZip(awsk.getAddZipUser());
            addressSKUser.setAddCountry(awsk.getAddCountryUser());
            addressSKUser.setAddCounty(awsk.getAddCountyUser());
            addressSKUser.setAddEffDate(awsk.getAddEffDateUser());
            addressSKUser.setAddPhone(awsk.getAddPhoneUser());
            addressSKUser.setAddEmail(awsk.getAddEmailUser());
            addressSKUser.setAddPager(awsk.getAddPagerUser());
            addressSKUser.setAddMobile(awsk.getAddMobileUser());
            addressSKUser.setAddFax(awsk.getAddFaxUser());
            addressSKUser.setIpAdd(awsk.getIpAdd());
            addressSKUser.setModifiedBy(awsk.getModifiedBy());

            output = addressSKUser.updateAddress();
           
            /*
             * try{ AddressAgentHome addressUserAgentHome =
             * EJBUtil.getAddressAgentHome(); AddressAgentRObj
             * addressUserAgentRObj = addressUserAgentHome.create(); output =
             * addressUserAgentRObj.updateAddress(addressSKUser);
             * Rlog.debug("accountWrapper","In
             * AccountWrapperAgentBean.createUSer for user address"); }
             * catch(Exception e) { Rlog.fatal("accountWrapper","Error in
             * AccountWrapperAgentBean.createUser for user address " + e);
             * return -2; }
             * 
             * 
             * try{ UserAgentHome userAgentHome = EJBUtil.getUserAgentHome();
             * UserAgentRObj userAgentRObj = userAgentHome.create(); output =
             * userAgentRObj.updateUser(userSK); Rlog.debug("accountWrapper","In
             * AccountWrapperAgentBean.createUser for user"); } catch(Exception
             * e) { Rlog.fatal("accountWrapper","Error in
             * AccountWrapperAgentBean.createUser for user " + e); return -2; }
             */

            return output;
        }

        catch (Exception e) {
            Rlog.fatal("accountWrapper",
                    "Error in AccountWrapperAgentBean.updateuser " + e);
            return -2;
        }

    }

}// end of class

