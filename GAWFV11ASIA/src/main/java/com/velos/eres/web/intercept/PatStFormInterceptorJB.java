package com.velos.eres.web.intercept;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import oracle.jdbc.OracleResultSet;

import com.velos.eres.business.common.CtrlDao;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.service.util.VMailMessage;
import com.velos.eres.service.util.VMailer;
import com.velos.eres.service.util.VelosAuthenticator;


public class PatStFormInterceptorJB extends InterceptorJB {
	String jdbcDriver;

    String jdbcUrl;

    String emailInTable;

    String loginID;

    String loginPassword;
    String supportEmail; 
    
    Connection dbCon;
    CallableStatement callablestatement = null;
    Clob templateCLob = null;
 // Message Object

    private VMailMessage vMailMessage;

    private Properties propObj;

    private VelosAuthenticator velAuthenticator;
    
	@Override
	public void handle(Object... args) {
		if (args == null || args.length < 1) {
			return;
		}
		ServletRequest request = (ServletRequest)args[0];
		String reportContent = null;
		System.out.println("Hemant123333");
		String address = "";
        String subject = "";
        String dispDate = "";
        String patientId = request.getParameter("formPatient");
		String patProtId = request.getParameter("formPatprot");
		System.out.println("Dharam===>>>>patProtId"+patProtId);
		String statid = request.getParameter("er_def_formstat");
		String formId = request.getParameter("formId");
		String formFilledId = (request.getParameter("formFilledFormId")==null)?"0":request.getParameter("formFilledFormId");
		readIni();
		System.out.println("readIni==>>>>>");	
        InitDB();
        System.out.println("line77");
        System.out.println("Dharam===>>>>patProtId"+StringUtil.stringToInteger(patProtId));
        if(StringUtil.stringToInteger(patProtId)>0){
        	System.out.println("Start==>>>>>");	
        try{
        	System.out.println("try==>>>>>");	
        	callablestatement = dbCon
            .prepareCall("{call eres.SP_MSGDISPATCHNCC(?,?,?,?,?,?,?,?)}");
    callablestatement.setInt(1, StringUtil.stringToNum(patientId));
    callablestatement.setInt(2, StringUtil.stringToNum(patProtId));
    callablestatement.setString(3, statid);
    callablestatement.setInt(4, StringUtil.stringToNum(formId));
    callablestatement.setInt(5, StringUtil.stringToNum(formFilledId));
    callablestatement.registerOutParameter(6, java.sql.Types.CLOB);
    callablestatement.registerOutParameter(7, java.sql.Types.VARCHAR);
    callablestatement.registerOutParameter(8, java.sql.Types.VARCHAR);
    callablestatement.execute();
    System.out.println("execute==>>>>>");	
    templateCLob = callablestatement.getClob(6);
    System.out.println("templateCLob==>>>>>"+  templateCLob);
    address = callablestatement.getString(7);
    System.out.println("address==>>>>>"+address);
    subject = callablestatement.getString(8);
    System.out.println("subject==>>>>>"+subject);
    
    if (!(templateCLob == null)) {
    	System.out.println("templateCLob==>>>>>"+templateCLob);
    	reportContent = templateCLob.getSubString(1,
                (int) templateCLob.length());
    } 
    reportContent=(reportContent==null)?"":reportContent;
    address=(address==null)?"":address;
    subject=(subject==null)?"":subject;
        	
        	if(reportContent.length()>0 && address.length()>0){
        	
                
                dispDate = (new Date()).toString();
                
                if (StringUtil.isEmpty(subject))
                {
                	subject = "Your Notifications For Today, As Of " + dispDate;
                }	
                	
                System.out.println("address" + address);
                System.out.println("supportEmail" + supportEmail);
                System.out.println("reportContent new:" + reportContent);
                
                if (SendMail(address, "", "", subject, reportContent,
                        supportEmail, "NC")){
                	 System.out.println("To supportEmail For Testecomp in send   "+address);
            }

            }

        }catch(Exception e){
        	e.printStackTrace();
        }
        finally{
        	CloseDB();
        }
        }
	}
	
	public void readIni() {
        try {
            String eHome = EJBUtil.getEnvVariable("ERES_HOME");
            eHome = (eHome == null) ? "" : eHome;
             if (eHome.trim().equals("%ERES_HOME%"))
                eHome = System.getProperty("ERES_HOME");
            eHome = eHome.trim();
            String  fileName = eHome + "eresMailer.ini";
            String iniString = getFileText(fileName);
            jdbcDriver = getValue(iniString, "<jdbcDriver>", "</jdbcDriver>");
            jdbcUrl = getValue(iniString, "<jdbcUrl>", "</jdbcUrl>");
            loginID = getValue(iniString, "<loginID>", "</loginID>");
            loginPassword = getValue(iniString, "<loginPassword>",
                    "</loginPassword>");
            supportEmail = getValue(iniString, "<supportEmail>",
            "</supportEmail>");
            System.out.println("To supportEmail EeHome   "+eHome);
        } catch (Exception e) {
            System.out.println("getValue" + e);
        }
    }
	
	public void InitDB() {
        try {

            Class.forName(jdbcDriver);

            dbCon = DriverManager
                    .getConnection(jdbcUrl, loginID, loginPassword);

        } catch (Exception e) {
            System.out.println("InitDB" + e); 
        }
    }
	
	public String getFileText(String fileName) throws IOException {
        String htmlBuffer = "";
        try {
            BufferedReader in = new BufferedReader(new FileReader(fileName));
            String inputLine;

            while ((inputLine = in.readLine()) != null)
                htmlBuffer = htmlBuffer + inputLine;
            in.close();
        } catch (Exception e) {
            System.out.println("I/O error\n" + e);
        }
        return htmlBuffer;
    }
	
	 public String getValue(String inString, String beginDelimiter,
	            String endDelimiter) {
	        int begin = inString.indexOf(beginDelimiter);
	        if (begin<0) return "";
	        int end = inString.indexOf(endDelimiter, begin);
	        String valueString = inString.substring(
	                begin + beginDelimiter.length(), end).trim();
	        System.out.println("vcMailer: " + valueString);
	        return valueString;
	    }
	 
	 public boolean SendMail(String to_addresses, String cc_addresses,
	            String bcc_addresses, String subject, String message,
	            String from_address, String msg_type) {
	        boolean status = true;
	        String mailTitle = "";
	        String mailStatusString = "";

	        try {
	            //VMailer vm = new VMailer();
	            VMailMessage msgObj = new VMailMessage();

	            if (msg_type.equals("NC")) {
	                mailTitle = "Velos eResearch Notification Center";
	            }else {
	                mailTitle = "Velos eResearch Notification Center";  
	            }
	            
	            msgObj.setMessageFrom(from_address);
	            msgObj.setMessageFromDescription(mailTitle);
	            msgObj.setMessageTo(to_addresses);
	            msgObj.setMessageSubject(subject);
	            msgObj.setMessageSentDate(new java.util.Date());
	            msgObj.setMessageText(message);
	            System.out.println("from_address    : " + from_address);
	            setDefaults();
	            this.setVMailMessage(msgObj);
	            mailStatusString = sendMail();
	            if (!StringUtil.isEmpty(mailStatusString)) {
	                return false;
	            } else {
	                return true;
	            }

	            
	        } catch (Exception e) {
	            status = false;
	            System.out.println("SendMail Mailing error:" + e);
	        }
	        
	        return status;
	    }
	 
	    
	    /**
	     * Sends the mail to specificed recipients
	     * 
	     * @return Returns the email server message returned.
	     */
	    public String sendMail() {
	        try {
	            // prepare mail session

	            Session sendMailSession;
	            Transport transport;

	            Rlog.debug("vmailer", "VMailer.sendMail() ..getPropObj()"
	                    + getPropObj());
	            Rlog.debug("vmailer", "VMailer.sendMail() ..getVelAuthenticator()"
	                    + getVelAuthenticator());

	            sendMailSession = Session.getInstance(getPropObj(),
	                    getVelAuthenticator());

	            Rlog.debug("vmailer", "VMailer.setMail() ..sendMailSession"
	                    + sendMailSession);

	            Message newMessage = new MimeMessage(sendMailSession);

	            if (vMailMessage == null) {
	                return "Can not send mail, VMailer.vMailMessage is null";
	            }

	            newMessage
	                    .setFrom(new InternetAddress(vMailMessage.getMessageFrom(),
	                            vMailMessage.getMessageFromDescription()));

	            if (!EJBUtil.isEmpty(vMailMessage.getMessageTo())) {
	                // VA- To suppot send email to multiple addresses. Before it was
	                // restricted to only one user.
	                newMessage.setRecipients(Message.RecipientType.TO,
	                        (new InternetAddress()).parse(vMailMessage
	                                .getMessageTo()));
	            }
	            // newMessage.setRecipient(Message.RecipientType.TO, new
	            // InternetAddress(vMailMessage.getMessageTo()));

	            if (!EJBUtil.isEmpty(vMailMessage.getMessageCC())) {
	                // VA- To suppot send email to multiple addresses. Before it was
	                // restricted to only one user.
	                // newMessage.setRecipient(Message.RecipientType.CC, new
	                // InternetAddress(vMailMessage.getMessageCC()));
	                newMessage.setRecipients(Message.RecipientType.CC,
	                        (new InternetAddress()).parse(vMailMessage
	                                .getMessageCC()));
	            }

	            if (!EJBUtil.isEmpty(vMailMessage.getMessageBCC())) {
	                // VA- To suppot send email to multiple addresses. Before it was
	                // restricted to only one user.
	                // newMessage.setRecipient(Message.RecipientType.BCC, new
	                // InternetAddress(vMailMessage.getMessageBCC()));
	                newMessage.setRecipients(Message.RecipientType.BCC,
	                        (new InternetAddress()).parse(vMailMessage
	                                .getMessageBCC()));
	            }
	            
	            newMessage.setSubject(vMailMessage.getMessageSubject());
	            newMessage.setSentDate(vMailMessage.getMessageSentDate());
	            newMessage.setContent(vMailMessage.getMessageText(),"text/html; charset=utf-8");

	            if (StringUtil.isEmpty((String)this.propObj.get("mail.smtp.port")) 
	                    || "25".equals((String)this.propObj.get("mail.smtp.port"))) {
	                transport = sendMailSession.getTransport("smtp");
	            } else {
	                transport = sendMailSession.getTransport("smtps");
	            }
	            transport.send(newMessage);
	            System.out.println("New newMessage in send mail  "+newMessage);
	            return "";

	        } catch (Exception ex) {
	            Rlog.fatal("vmailer",
	                    "VMailer.sendMail() : Exception in sending mail " + ex);
	            ex.printStackTrace();
	            return ex.toString();
	        }

	    }

	    /**
	     * Sets the defaults attribute of the VMailer object
	     */
	    public void setDefaults() {

	        Rlog.debug("vmailer", "VMailer.setDefaults() ..started");

	        String smtpHost = "";
	        String defMailUser = "";
	        String defMailPass = "";

	        CtrlDao ctrl = new CtrlDao();
	        int rows = 0;

	        try {
	            // get SMTPHost

	            ctrl.getControlValues("mailsettings");
	            rows = ctrl.getCRows();

	            if (rows > 0) {
	                smtpHost = (String) ctrl.getCValue().get(0);
	            }

	            // get Default Mail Server User

	            ctrl = new CtrlDao();
	            ctrl.getControlValues("vmailuser");
	            rows = ctrl.getCRows();

	            if (rows > 0) {
	                defMailUser = (String) ctrl.getCValue().get(0);
	            }

	            // get Default Mail Server Password

	            ctrl = new CtrlDao();
	            ctrl.getControlValues("vmailpass");
	            rows = ctrl.getCRows();

	            if (rows > 0) {
	                defMailPass = (String) ctrl.getCValue().get(0);
	            }
	            defMailUser = (defMailUser == null) ? "" : defMailUser;
	            defMailPass = (defMailPass == null) ? "" : defMailPass;

	            // create default properties object

	            Properties props = new Properties();
	            if (defMailUser.length() > 0) {
	                props.put("mail.smtp.auth", "true");
	            } else {
	                props.put("mail.smtp.auth", "false");
	            }
	            props.put("mail.smtp.host", smtpHost);
	            props.put("mail.debug", "true");
	            System.out.println("New newMessage in send mail in setdefault  "+smtpHost);
	            ctrl = new CtrlDao();
	            ctrl.getControlValues("mailport");
	            rows = ctrl.getCRows();
	            if (rows > 0) {
	                String mailPort = StringUtil.trueValue((String) ctrl.getCValue().get(0)).trim();
	                if (!StringUtil.isEmpty(mailPort)) {
	                    props.put("mail.smtp.port", mailPort);
	                    if (!"25".equals(mailPort)) {
	                        props.put("mail.smtp.class", "com.sun.mail.smtp.SMTPSSLTransport");
	                    }
	                }
	            }

	            props.put("mail.smtp.connectiontimeout","30000");
	            props.put("mail.smtp.timeout","30000");
	            
	            Rlog.debug("vmailer", "VMailer.setDefaults() value of smtpHost "
	                    + smtpHost);

	            // create default VelosAuthenticator

	            VelosAuthenticator auth = new VelosAuthenticator();

	            auth.setUser(defMailUser);
	            auth.setPassword(defMailPass);

	            // set class instance variables

	            setVelAuthenticator(auth);
	            setPropObj(props);

	            Rlog.debug("vmailer", "VMailer.setDefaults() value of defMailUser "
	                    + defMailUser);

	            Rlog.debug("vmailer", "VMailer.setDefaults() ..defaults set");
	 
	        } catch (Exception ex) {
	            Rlog.fatal("vmailer",
	                    "EXCEPTION IN DEFAULT CONSTRUCTOR of VMailer " + ex);
	            ex.printStackTrace();
	        }

	    }

	    /**
	     * Gets the propObj attribute of the VMailer object
	     * 
	     * @return The propObj value
	     */
	    public Properties getPropObj() {
	        return this.propObj;
	    }

	    /**
	     * Sets the propObj attribute of the VMailer object
	     * 
	     * @param propObj
	     *            The new propObj value
	     */
	    public void setPropObj(Properties propObj) {
	        this.propObj = propObj;
	    }

	    /**
	     * Gets the velAuthenticator attribute of the VMailer object
	     * 
	     * @return The velAuthenticator value
	     */
	    public VelosAuthenticator getVelAuthenticator() {
	        return this.velAuthenticator;
	    }

	    /**
	     * Sets the velAuthenticator attribute of the VMailer object
	     * 
	     * @param velAuthenticator
	     *            The new velAuthenticator value
	     */
	    public void setVelAuthenticator(VelosAuthenticator velAuthenticator) {
	        this.velAuthenticator = velAuthenticator;
	    }

	    /**
	     * Gets the vMailMessage attribute of the VMailer object
	     * 
	     * @return The vMailMessage value
	     */
	    public VMailMessage getVMailMessage() {
	        return this.vMailMessage;
	    }

	    /**
	     * Sets the vMailMessage attribute of the VMailer object
	     * 
	     * @param vMailMessage
	     *            The new vMailMessage value
	     */
	    public void setVMailMessage(VMailMessage vMailMessage) {
	        this.vMailMessage = vMailMessage;
	    }


	 
	 public void CloseDB() {
	        try {
	            dbCon.close();
	        } catch (SQLException e) {
	            System.out.println("CloseDB:" + e);
	        }
	    }

}
