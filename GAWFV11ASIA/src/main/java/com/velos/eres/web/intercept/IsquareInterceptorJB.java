package com.velos.eres.web.intercept;


import com.velos.eres.service.util.CFG;
import com.velos.eres.web.intercept.InterceptorJB;
import com.velos.eres.web.intercept.IsquareInterceptorJB;
import com.velos.eres.web.intercept.MyTrustManager;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.swing.Timer;
import org.json.JSONObject;
import sun.misc.BASE64Encoder;

public class IsquareInterceptorJB extends InterceptorJB implements Runnable {
	public static final String ISQ_URL_NOT_CONFIGURED_STR = "[ISQ_URL_NOT_CONFIGURED]";
	public static final String ISQ_CRED_NOT_CONFIGURED_STR = "[ISQ_CRED_NOT_CONFIGURED]";
	public static final String UTF8_STR = "UTF-8";
	public static final String GET_STR = "GET";
	
	public final static int SIX_HUNDERED_SECONDS = 360;// 1 mins

	//Timer timer = new Timer(SIX_HUNDERED_SECONDS, null);
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			loginToIsquare();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
		
	public void handle(Object...args) {
		if(ISQ_URL_NOT_CONFIGURED_STR.equals(CFG.ISQ_URL) || ISQ_CRED_NOT_CONFIGURED_STR.equals(CFG.ISQ_AUTH)){
			return;
		}
		ScheduledExecutorService scheduledThreadPool = Executors.newScheduledThreadPool(2);
		scheduledThreadPool.scheduleAtFixedRate(new IsquareInterceptorJB(),0, SIX_HUNDERED_SECONDS, TimeUnit.SECONDS);
	}

	private void loginToIsquare() throws Throwable{
		try {
			HttpURLConnection conn;
			StringBuffer fullUrl = new StringBuffer(CFG.ISQ_URL);
			String respStr = null;
			fullUrl.append("/login");
			URL url = new URL(fullUrl.toString());
            System.out.println("URL===>" + url);
            System.out.println("Disable SSL ");
            if ("Y".equals(CFG.ISQ_SSL)) {
                IsquareInterceptorJB.disableSSL();
                conn = (HttpsURLConnection)url.openConnection();
                respStr = this.readResponseContent((HttpsURLConnection)conn);
            } else {
                conn = (HttpURLConnection)url.openConnection();
                respStr = this.readResponseContent(conn);
            }
            System.out.println("respStr=" + respStr);
			try{
				JSONObject respObj = new JSONObject(respStr);
				JSONObject locationObj = respObj.getJSONObject("bhLocations");
				synchronized(CFG.class) {
					CFG.ISQ_TOKEN = respObj.getString("access_token");
					CFG.ISQ_BEACHHEADS = (null != locationObj)? locationObj.getString("ui") : null;
				}
			} catch (Exception e1){
				synchronized(CFG.class) {
					CFG.ISQ_TOKEN = null;
					CFG.ISQ_BEACHHEADS = null;
				}
			}
			//timer.start();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
    private String readResponseContent(HttpURLConnection conn) throws Throwable {
        StringBuffer sb;
        block13 : {
            sb = new StringBuffer();
            char[] buffer = new char[1];
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setConnectTimeout(15000);
            conn.setReadTimeout(15000);
            conn.setRequestMethod("GET");
            System.out.println("Before set verifier");
            String userPassword = CFG.ISQ_AUTH;
            String encoding = new BASE64Encoder().encode(userPassword.getBytes());
            conn.setRequestProperty("Authorization", "Basic " + encoding);
            conn.setRequestProperty("Product", "VelosER");
            System.out.println("Conn URL ===>" + conn.getURL());
            try {
                try {
                    if (conn.getInputStream() != null) {
                        InputStreamReader reader = new InputStreamReader(conn.getInputStream(), "UTF-8");
                        int iRead = 0;
                        while ((iRead = reader.read(buffer)) >= 0) {
                            sb.append(buffer);
                        }
                    }
                }
                catch (IOException reader) {
                    try {
                        conn.getInputStream().close();
                    }
                    catch (IOException var9_9) {}
                    break block13;
                }
            }
            catch (Throwable var8_12) {
                try {
                    conn.getInputStream().close();
                }
                catch (IOException var9_10) {
                    // empty catch block
                }
                throw var8_12;
            }
            try {
                conn.getInputStream().close();
            }
            catch (IOException var9_11) {
                // empty catch block
            }
        }
        return sb.toString();
    }

    private String readResponseContent(HttpsURLConnection conn) throws Throwable {
        StringBuffer sb;
        block13 : {
            sb = new StringBuffer();
            char[] buffer = new char[1];
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setConnectTimeout(15000);
            conn.setReadTimeout(15000);
            conn.setRequestMethod("GET");
            System.out.println("Before set verifier");
            String userPassword = CFG.ISQ_AUTH;
            String encoding = new BASE64Encoder().encode(userPassword.getBytes());
            conn.setRequestProperty("Authorization", "Basic " + encoding);
            conn.setRequestProperty("Product", "VelosER");
            System.out.println("Conn URL ===>" + conn.getURL());
            try {
                try {
                    if (conn.getInputStream() != null) {
                        InputStreamReader reader = new InputStreamReader(conn.getInputStream(), "UTF-8");
                        int iRead = 0;
                        while ((iRead = reader.read(buffer)) >= 0) {
                            sb.append(buffer);
                        }
                    }
                }
                catch (IOException reader) {
                    try {
                        conn.getInputStream().close();
                    }
                    catch (IOException var9_9) {}
                    break block13;
                }
            }
            catch (Throwable var8_12) {
                try {
                    conn.getInputStream().close();
                }
                catch (IOException var9_10) {
                    // empty catch block
                }
                throw var8_12;
            }
            try {
                conn.getInputStream().close();
            }
            catch (IOException var9_11) {
                // empty catch block
            }
        }
        return sb.toString();
    }

    private static void disableSSL() {
        try {
            TrustManager[] trustAllCerts = new TrustManager[]{new MyTrustManager()};
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new SecureRandom());
             HostnameVerifier allHostsValid = new HostnameVerifier(){
			@Override
			public boolean verify(String arg0, SSLSession arg1) {
				// TODO Auto-generated method stub
				return true;
			}};
            HttpsURLConnection.setDefaultHostnameVerifier((HostnameVerifier)allHostsValid);
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
	
}
