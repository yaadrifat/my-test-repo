package com.velos.eres.compliance.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.StudyVerDao;
import com.velos.eres.service.util.CFG;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.widget.business.common.ResubmDraftDao;
import com.velos.eres.widget.business.common.UIFlxPageDao;
import com.velos.eres.widget.service.util.FlxPageFields;

public class ProtocolChangeLog {

	private static final String STUDY_ADM_DIV = "studyAdmDiv", STUDY_ADM_DEPT = "studyCond2";
	private static final String STUDY_CCSG = "STUDY_CCSG";
	private static final String Str_dropdown = "dropdown", Str_input = "input";
	private static final String EMPTY_STRING = "";
	public static final String VERSIONDIFF_STR = "versionDiff", ATTACHDIFF_STR = "attachDiff", FORMRESPDIFF_STR = "formRespDiff", COMMENTS_STR = "comments";
	
	private JSONObject getThisVersionNumber(JSONObject versionDetailsJSON){
		String pageVer = "", pageMinorVer = "";
		try {
			pageVer = versionDetailsJSON.getString("majorVersion");
			pageMinorVer = versionDetailsJSON.getString("minorVersion");
		} catch (JSONException e) {
			e.printStackTrace();
			//return null;
		}

		String fullVer = pageVer + ".";
		fullVer += (StringUtil.stringToNum(pageMinorVer) > 9) ? pageMinorVer : "0" + pageMinorVer;
		
		try {
			versionDetailsJSON.put("fullVersion", fullVer);
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		return versionDetailsJSON;
	}

	private JSONObject getLastResubmDraftVersionNumber(JSONObject versionDetailsJSON){
		JSONObject lastVersionDetailsJSON = new JSONObject();
		
		int studyId;
		try {
			studyId = versionDetailsJSON.getInt("studyId");
		} catch (JSONException e1) {
			e1.printStackTrace();
			studyId = 0;
		}
		ResubmDraftDao resubmDraftDao = new ResubmDraftDao();
		int versionNum = resubmDraftDao.getHighestResubmDraftVersion("er_study", studyId);
		if (versionNum <= 0){
			try {
				lastVersionDetailsJSON.put("majorVersion", versionNum);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return lastVersionDetailsJSON;
		}
		int minorVerNum = resubmDraftDao.getHighestMinorResubmDraftVersion(versionNum, "er_study", studyId);
		String fullVerNum = versionNum + ".";
		fullVerNum += (minorVerNum > 9) ? minorVerNum : "0" + minorVerNum;
		try {
			lastVersionDetailsJSON.put("majorVersion", versionNum);
			lastVersionDetailsJSON.put("minorVersion", minorVerNum);
			lastVersionDetailsJSON.put("fullVersion", fullVerNum);
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}

		int versionNumOld = -1;
		int minorVerNumOld = -1;
		if (minorVerNum >= 1) {
			minorVerNumOld = minorVerNum - 1;
			versionNumOld = versionNum;
		} else {
			versionNumOld = versionNum - 1;
			minorVerNumOld = resubmDraftDao.getHighestMinorResubmDraftVersion(versionNumOld, "er_study", studyId);
		}

		String fullVerNumOld = versionNumOld + ".";
		fullVerNumOld += (minorVerNumOld > 9) ? minorVerNumOld : "0" + minorVerNumOld;
		
		try {
			lastVersionDetailsJSON.put("lastMajorVersion", versionNumOld);
			lastVersionDetailsJSON.put("lastMinorVersion", minorVerNumOld);
			lastVersionDetailsJSON.put("lastFullVersion", fullVerNumOld);
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		return lastVersionDetailsJSON;
	}

	private JSONObject getLastProtocolVersionNumber(JSONObject versionDetailsJSON){
		JSONObject lastVersionDetailsJSON = new JSONObject();
		
		int studyId;
		try {
			studyId = versionDetailsJSON.getInt("studyId");
		} catch (JSONException e1) {
			e1.printStackTrace();
			studyId = 0;
		}
		
		UIFlxPageDao uiFlxPageDao = new UIFlxPageDao();
		int versionNum = uiFlxPageDao.getHighestFlexPageVersion("er_study", studyId);
		int minorVerNum = uiFlxPageDao.getHighestMinorFlexPageVersion(versionNum, "er_study", studyId);
		String fullVerNum = versionNum + ".";
		fullVerNum += (minorVerNum > 9) ? minorVerNum : "0" + minorVerNum;
		try {
			lastVersionDetailsJSON.put("majorVersion", versionNum);
			lastVersionDetailsJSON.put("minorVersion", minorVerNum);
			lastVersionDetailsJSON.put("fullVersion", fullVerNum);
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		
		int versionNumOld = -1;
		int minorVerNumOld = -1;

		if (minorVerNum >= 1) {
			minorVerNumOld = minorVerNum - 1;
			versionNumOld = versionNum;
		} else {
			versionNumOld = versionNum - 1;
			minorVerNumOld = uiFlxPageDao.getHighestMinorFlexPageVersion(versionNumOld, "er_study", studyId);
		}

		String fullVerNumOld = minorVerNumOld + ".";
		fullVerNumOld += (versionNumOld > 9) ? versionNumOld : "0" + versionNumOld;
		
		try {
			lastVersionDetailsJSON.put("lastMajorVersion", versionNumOld);
			lastVersionDetailsJSON.put("lastMinorVersion", minorVerNumOld);
			lastVersionDetailsJSON.put("lastFullVersion", fullVerNumOld);
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		return lastVersionDetailsJSON;
	}

	public String getAllProtocolVersions(int studyId, String grpId) {
		StringBuilder allVersions = new StringBuilder();
		UIFlxPageDao uiFlxPageDao = new UIFlxPageDao();
		ArrayList<HashMap<String, String>> resList = uiFlxPageDao
				.getAllVersionsForProtocol("er_study", studyId);

		allVersions
				.append("<table class='TFtable' width='100%' cellspacing='4' cellpadding='4' border='1'>");
		allVersions.append("<tr><th>" + LC.Version_Header + "</th><th>"
				+ LC.Change_Log_Justification + "</th>" + " <th>"
				+ LC.Submission_Type_Description + "</th>" + " </tr>");

		int totalVersions = resList.size();
		for (HashMap<String, String> resMap : resList) {
			String pageVer = resMap.get("page_ver");
			String pageMinorVer = resMap.get("page_minor_ver");
			String submissionTypeDesc = resMap.get("submissionTypeDesc");
			String fullVer = pageVer;

			JSONObject versionDetailsJSON = new JSONObject();
			try {
				versionDetailsJSON.put("majorVersion", pageVer);
				versionDetailsJSON.put("minorVersion", pageMinorVer);
			} catch (JSONException e) {
				e.printStackTrace();
			}

			JSONObject newVersionDetailsJSON  = getThisVersionNumber(versionDetailsJSON);
			try {
				fullVer = newVersionDetailsJSON.getString("fullVersion");
			} catch (JSONException e) {
				e.printStackTrace();
			}

			String versionSummary = "studyVersionDetails.jsp?studyId="
					+ studyId + "&pageVer=" + pageVer + "&pageMinorVer="
					+ pageMinorVer;
			String versionChangeLog = "studyVersionChangeLog.jsp?studyId="
					+ studyId + "&pageVer=" + pageVer + "&pageMinorVer="
					+ pageMinorVer;
			allVersions.append("<tr>");
			/* 
			 * allVersions.append("<td>" +
			 * "<A href='studyVersionDetails.jsp?studyId="
			 * +studyId+"&pageVer="+pageVer
			 * +"&pageMinorVer="+pageMinorVer+"'>"+"Version " + fullVer +"</A>"
			 * + "</td>");
			 */
			allVersions.append("<td>"
					+ "<A href='javascript:f_open_versionDetails(&quot;"
					+ versionSummary + "&quot;);'>" + "Version " + fullVer
					+ "</A>" + "</td>");

			if (totalVersions == 1) {
				allVersions.append("<td>" + "NA" + "</td>");
			} else {
				allVersions.append("<td>"
						+ "<A href='javascript:f_open_versionChangeLog(&quot;"
						+ versionChangeLog + "&quot;);'>" + "Change Log"
						+ "</A>" + "</td>");
			}

			allVersions.append("<td>" + submissionTypeDesc + "</td>");
			allVersions.append("</tr>");
			totalVersions--;
		}
		allVersions.append("</table>");
		return allVersions.toString();
	}

	public String fetchAdditionalComments(int studyId) {
		ResubmDraftDao resubmDraftDao = new ResubmDraftDao();
		resubmDraftDao.getHighestResubmDraftFullVersion("er_study", studyId);

		//Most recent Additional Comments
		return resubmDraftDao.getComments();
	}

	public JSONArray fetchProtocolVersionDiff(int studyId, String grpId) {
		JSONObject versionDetailsJSON = new JSONObject();
		try {
			versionDetailsJSON.put("studyId", studyId);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		JSONObject lastVersionDetailsJSON  = getLastResubmDraftVersionNumber(versionDetailsJSON);
		int versionNum = 0;
		try {
			versionNum = lastVersionDetailsJSON.getInt("majorVersion");
		} catch (JSONException e) {
			e.printStackTrace();
			versionNum = 0;
		}
		if (versionNum <= 0){
			return new JSONArray();
		}

		int minorVerNum = 0;
		try {
			minorVerNum = lastVersionDetailsJSON.getInt("minorVersion");
		} catch (JSONException e) {
			e.printStackTrace();
			minorVerNum = 0;
		}
		
		String fullVerNum = EMPTY_STRING;
		try {
			fullVerNum = lastVersionDetailsJSON.getString("fullVersion");
		} catch (JSONException e) {
			e.printStackTrace();
		}

		int versionNumOld = -1;
		try {
			versionNumOld = lastVersionDetailsJSON.getInt("lastMajorVersion");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		int minorVerNumOld = -1;
		try {
			minorVerNumOld = lastVersionDetailsJSON.getInt("lastMinorVersion");
		} catch (JSONException e) {
			e.printStackTrace();
		}

		String fullVerNumOld = EMPTY_STRING;
		try {
			fullVerNumOld = lastVersionDetailsJSON.getString("lastFullVersion");
		} catch (JSONException e) {
			e.printStackTrace();
		}

		//Diff versions first
		JSONArray jsVersionDiffArray = fetchProtocolVersionDiffDetails(fullVerNumOld, fullVerNum, studyId, grpId);

		//Diff re-submission draft second
		return fetchProtocolVersionResubmDraftDiffDetails(versionNum, minorVerNum, studyId, grpId, jsVersionDiffArray);
	}

	public JSONObject fetchStudyVersionChangeLog(int versionNum, int minorVerNum, int studyId, String grpId) {
		JSONObject protocolVerDiffJSON = new JSONObject();

		UIFlxPageDao uiFlxPageDao = new UIFlxPageDao();
		if (versionNum == 0) {
			versionNum = uiFlxPageDao.getHighestFlexPageVersion("er_study",
					studyId);
			minorVerNum = uiFlxPageDao.getHighestMinorFlexPageVersion(
					versionNum, "er_study", studyId);
		}

		JSONObject verDiffJSON = uiFlxPageDao.getFlexPageVersionDiffDetails("er_study", studyId, versionNum, minorVerNum);
		if (null == verDiffJSON) { return null; }

		try {
			protocolVerDiffJSON.put(VERSIONDIFF_STR, this.fetchProtocolVersionDiffForVersion(verDiffJSON.getString(UIFlxPageDao.PAGE_VERDIFF)));
			protocolVerDiffJSON.put(ATTACHDIFF_STR, this.fetchAttachProtocolVersionDiffForVersion(verDiffJSON.getString(UIFlxPageDao.PAGE_ATTACHDIFF)));
			protocolVerDiffJSON.put(FORMRESPDIFF_STR, this.fetchProtocolVersionDiffForVersion(verDiffJSON.getString(UIFlxPageDao.PAGE_FORMRESPDIFF)));
			protocolVerDiffJSON.put(COMMENTS_STR, verDiffJSON.getString(UIFlxPageDao.PAGE_COMMENTS));
		} catch (JSONException e) {
			e.printStackTrace();
			protocolVerDiffJSON = null;
		}

		return protocolVerDiffJSON;
	}

	private String fetchProtocolVersionDiffForVersion(String verDiff) {
		StringBuilder changeVersion = new StringBuilder();
		CodeDao codeDao = new CodeDao();

		if(EJBUtil.isEmpty(verDiff))
		{
			return LC.Field_Version_No_Diff_Message;
		}
		try {
			JSONArray versionDiffList = new JSONArray(verDiff);
			int noOfFields = versionDiffList.length();
			if(noOfFields == 0){
				return LC.Field_Version_No_Diff_Message;
			}
			for (int i = 0; i < noOfFields; i++) {
				changeVersion
				.append("<tr>");
				changeVersion
				.append("<td>" + versionDiffList.getJSONObject(i).get("title") + "</td>");
				changeVersion
				.append("<td>" + versionDiffList.getJSONObject(i).get("fieldName") + "</td>");
				changeVersion
				.append("<td>" + versionDiffList.getJSONObject(i).get("oldValue") + "</td>");
				changeVersion
				.append("<td>" + versionDiffList.getJSONObject(i).get("newValue") + "</td>");
				changeVersion
				.append("<td>" + versionDiffList.getJSONObject(i).get("notes") + "</td>");
				changeVersion
				.append("<td>" + codeDao.getCodeDescription(Integer.parseInt((String) versionDiffList.getJSONObject(i).get("revBoard"))) + "</td>");
				changeVersion
				.append("</tr>");
			}
		} catch (JSONException e) {
			return LC.Field_Version_No_Diff_Message;
		}

		return changeVersion.toString();
	}

	public String returnProtocolVersionDiff(String versionOld,
			String versionNew, int studyId, String grpId) {
		
		JSONObject versionDetailsJSON = new JSONObject();
		try {
			versionDetailsJSON.put("studyId", studyId);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		JSONObject lastVersionDetailsJSON = getLastProtocolVersionNumber(versionDetailsJSON);
		int versionNum = 0;
		try {
			versionNum = lastVersionDetailsJSON.getInt("majorVersion");
		} catch (JSONException e) {
			e.printStackTrace();
			versionNum = 0;
		}

		int minorVerNum = 0;
		try {
			minorVerNum = lastVersionDetailsJSON.getInt("minorVersion");
		} catch (JSONException e) {
			e.printStackTrace();
			minorVerNum = 0;
		}
		
		String fullVerNum = EMPTY_STRING;
		try {
			fullVerNum = lastVersionDetailsJSON.getString("fullVersion");
		} catch (JSONException e) {
			e.printStackTrace();
		}

		int versionNumOld = -1;
		try {
			versionNumOld = lastVersionDetailsJSON.getInt("lastMajorVersion");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		int minorVerNumOld = -1;
		try {
			minorVerNumOld = lastVersionDetailsJSON.getInt("lastMinorVersion");
		} catch (JSONException e) {
			e.printStackTrace();
		}

		String fullVerNumOld = EMPTY_STRING;
		try {
			fullVerNumOld = lastVersionDetailsJSON.getString("lastFullVersion");
		} catch (JSONException e) {
			e.printStackTrace();
		}

		JSONArray versionDiffList = fetchProtocolVersionDiffDetails(fullVerNumOld, fullVerNum, studyId, grpId);
		StringBuilder changeVersion = new StringBuilder();
		int noOfRows = versionDiffList.length();
		changeVersion
				.append("<table class='TFtable' width='90%' cellspacing='4' cellpadding='4' border='1'>");
		changeVersion
				.append("<tr><th>"
						+ LC.Change_Version_Title
						+ "</th><th>"
						+ LC.Change_Version_Field_Name
						+ "</th>"
						+ " <th>"
						+ LC.Change_Version_Old_Value
						+ "</th>"
						+ " <th>"
						+ LC.Change_Version_New_Value
						+ "</th>"
						+ " <th>"
						+ LC.Change_Version_Reason_for_Change
						+ "<FONT class='Mandatory' id='pgcustompatid'>*  </FONT>  </th>"
						+ " <th>"
						+ LC.Change_Version_Review_Board
						+ "<FONT class='Mandatory' id='pgcustompatid'>*  </FONT>  </th>"
						+ " </tr>");

		for (int i = 0; i < noOfRows; i++) {
			try {
				JSONObject fieldDetails = versionDiffList.getJSONObject(i);
				changeVersion.append(fieldDetails.get("fieldRow"));
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
		changeVersion.append("</table>");
		return changeVersion.toString();
	}

	private JSONObject createProtocolVersionDiffJSON(Object...args){
		String grpId = args[0].toString();
		String fldKey = args[1].toString(), fldName = args[2].toString(), oldValue = args[3].toString(),
		newValue = args[4].toString(), title = args[5].toString(), fldLabel = args[6].toString(),
		notes = (args.length < 8 || null == args[7])? "" : args[7].toString(), 
		board = (args.length < 9 || null == args[8])? "" : args[8].toString();
		notes = (StringUtil.isEmpty(notes))? "" : notes;

		fldLabel = (StringUtil.isEmpty(fldLabel)) ? fldKey : fldLabel ;
		String notesTAreaName = fldLabel.replaceAll(" ", "_") + "_" + "notes";

		CodeDao cdReviewBoard = new CodeDao();
		//cdReviewBoard.getCodeValues("rev_board");
		cdReviewBoard.getCodeValuesWithoutHide("rev_board");
		cdReviewBoard.setCType("rev_board");
		cdReviewBoard.setForGroup(grpId);
		
		String ddName = fldLabel.replaceAll(" ", "_") + "_" + "revBoard";
		
		if ("NEWATTACHMENT".equals(fldKey)){
			notesTAreaName += "1";
			ddName += "1";
		}
		if ("DELETEDATTACHMENT".equals(fldKey)){
			notesTAreaName += "2";
			ddName += "2";
		}
		
		String ddRevBoard = EJBUtil.createPullDown(ddName, StringUtil.stringToNum(board) , cdReviewBoard.getCId(), cdReviewBoard.getCDesc()) ;

		JSONObject versionDiffMap = new JSONObject();
		try {
			versionDiffMap.put("field", fldKey);
			versionDiffMap.put("fieldName",fldName);
			versionDiffMap.put("oldValue", oldValue);
			versionDiffMap.put("newValue", newValue);
			versionDiffMap.put("title",title);
			versionDiffMap.put("fieldLabel", fldLabel);
			versionDiffMap.put("fieldRow",
				"<tr>" + "<td width='10%'>"
						+ title
						+ "</td>"
						+ "<td width='10%'>"
						+ fldName
						+ "</td>"
						+ "<td width='15%'>"
						+ oldValue
						+ "</td>"
						+ "<td width='15%'>"
						+ newValue
						+ "</td>"
						+ "<td width='25%'>"
						+ "<textarea type='text' id='"
						+ notesTAreaName
						+ "' "
						+ "name='"
						+ notesTAreaName
						+ "' "
						+ "rows='2' cols='30' maxlength='4000'>"+ notes +"</textarea>"
						+ "</td>"
						+ "<td>"
						+ ddRevBoard
						+ "</td>" + "</tr>");
		} catch (JSONException e) {
			versionDiffMap = null;
			e.printStackTrace();
		}
		
		return versionDiffMap;
	}

	private JSONArray compareMergeDiffs(String grpId, JSONArray jsVersionDiffArray, JSONArray jsResubmDraftDiffArray){
		// Comparing the difference of the two versions to Re-submission Draft version difference.
		JSONArray list = new JSONArray();
		try {
			for (int verDiffIndex = 0; verDiffIndex < jsVersionDiffArray.length(); verDiffIndex++) {
				JSONObject jsVersionDiffObj = jsVersionDiffArray.getJSONObject(verDiffIndex);
				String newVersionKey = jsVersionDiffObj.getString("field");
				if (StringUtil.isEmpty(newVersionKey)) continue;

				jsVersionDiffObj.put("notes", EMPTY_STRING);
				jsVersionDiffObj.put("board", EMPTY_STRING);

				boolean keyFound = false;
				for (int rdDiffIndex = 0; rdDiffIndex < jsResubmDraftDiffArray.length(); rdDiffIndex++) {
					JSONObject jsResubmDraftDiffObj = jsResubmDraftDiffArray.getJSONObject(rdDiffIndex);
					String oldVersionKey = jsResubmDraftDiffObj.getString("field");
					if (StringUtil.isEmpty(oldVersionKey)) continue;

					if (newVersionKey.equals(oldVersionKey)){
						String versionOldValue = jsVersionDiffObj.getString("oldValue");
						String rdOldValue = jsResubmDraftDiffObj.getString("oldValue");
						
						String versionNewValue = jsVersionDiffObj.getString("newValue");
						String rdNewValue = jsResubmDraftDiffObj.getString("newValue");
						
						if (rdOldValue.equals(versionOldValue) && rdNewValue.equals(versionNewValue)){
							//No change, just copy comments and re-submission board
							String fieldNotes = "";
							try{
								fieldNotes = jsResubmDraftDiffObj.getString("fieldNotes");
			                	fieldNotes = (StringUtil.isEmpty(fieldNotes)) ? "" : fieldNotes;
			                	
							} catch (JSONException jsE){ }
							jsVersionDiffObj.put("notes", fieldNotes);

							String fieldBoard = "";
							try{
			                	fieldBoard = jsResubmDraftDiffObj.getString("fieldBoard");
			                	fieldBoard = (StringUtil.isEmpty(fieldBoard)) ? "" : fieldBoard;
			                	
							} catch (JSONException jsE){ }
							jsVersionDiffObj.put("board", fieldBoard);
						}
						if (rdNewValue.equals(versionOldValue) && !rdOldValue.equals(versionNewValue)){
							//Change A => B, B => C, just discard comments and re-submission board
							//jsVersionDiffObj.put("oldValue", rdOldValue);
							
						}
						keyFound = true;
						//jsVersionDiffArray.put(verDiffIndex, jsVersionDiffObj);
						JSONObject versionDiffMap = new JSONObject();
						versionDiffMap = createProtocolVersionDiffJSON(
							grpId, jsVersionDiffObj.getString("field"),
							jsVersionDiffObj.getString("fieldName"),
							jsVersionDiffObj.getString("oldValue"),
							jsVersionDiffObj.getString("newValue"),
							jsVersionDiffObj.getString("title"),
							jsVersionDiffObj.getString("fieldLabel"),
							jsVersionDiffObj.getString("notes"),
							jsVersionDiffObj.getString("board")
						);

						//versionDiffList.add(versionDiffMap);
						list.put(versionDiffMap);
					}
				}
				
				if (!keyFound){
					JSONObject versionDiffMap = new JSONObject();
					versionDiffMap = createProtocolVersionDiffJSON(
						grpId, jsVersionDiffObj.getString("field"),
						jsVersionDiffObj.getString("fieldName"),
						jsVersionDiffObj.getString("oldValue"),
						jsVersionDiffObj.getString("newValue"),
						jsVersionDiffObj.getString("title"),
						jsVersionDiffObj.getString("fieldLabel"),
						jsVersionDiffObj.getString("notes"),
						jsVersionDiffObj.getString("board")
					);

					//versionDiffList.add(versionDiffMap);
					list.put(versionDiffMap);
				}
			}
							
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Finish");

		// return versionDiffList;
		return list;
	}

	private JSONArray fetchProtocolVersionResubmDraftDiffDetails(int versionNewMajor, int versionNewMinor, int studyId, 
			String grpId, JSONArray jsVersionDiffArray) {
		// Resubmission Draft diff
		ResubmDraftDao resubmDraftDao = new ResubmDraftDao();
		String resubmDraftDiffStr = resubmDraftDao.getResubmDraftVersionDiffDetails
				("er_study", studyId, versionNewMajor, versionNewMinor);
		if (StringUtil.isEmpty(resubmDraftDiffStr)){
			return jsVersionDiffArray;
		}
		JSONArray jsResubmDraftDiffArray;
		try {
			jsResubmDraftDiffArray = new JSONArray(resubmDraftDiffStr);
		} catch (JSONException e1) {
			e1.printStackTrace();
			return jsVersionDiffArray;
		}
		
		// Comparing the difference of the two versions to Re-submission Draft version difference.
		JSONArray list = this.compareMergeDiffs(grpId, jsVersionDiffArray, jsResubmDraftDiffArray);
		System.out.println("Finish");

		return list;
	}

	private JSONArray fetchAttachProtocolVersionResubmDraftDiffDetails(int versionNewMajor, int versionNewMinor, int studyId, 
			String grpId, JSONArray jsAttachDiffArray) {
		// Resubmission Draft diff
		ResubmDraftDao resubmDraftDao = new ResubmDraftDao();
		String resubmDraftDiffStr = resubmDraftDao.getResubmDraftAttachVersionDiffDetails
				("er_study", studyId, versionNewMajor, versionNewMinor);
		if (StringUtil.isEmpty(resubmDraftDiffStr)){
			return jsAttachDiffArray;
		}
		JSONArray jsResubmDraftDiffArray;
		try {
			jsResubmDraftDiffArray = new JSONArray(resubmDraftDiffStr);
		} catch (JSONException e1) {
			e1.printStackTrace();
			return jsAttachDiffArray;
		}
		
		// Comparing the difference of the two versions to Re-submission Draft version difference.
		JSONArray list = this.compareMergeDiffs(grpId, jsAttachDiffArray, jsResubmDraftDiffArray);
		System.out.println("Finish");

		return list;
	}

	private JSONArray fetchFormRespProtocolVersionResubmDraftDiffDetails(int versionNewMajor, int versionNewMinor, int studyId, 
			String grpId, JSONArray jsFormRespDiffArray) {
		// Resubmission Draft diff
		ResubmDraftDao resubmDraftDao = new ResubmDraftDao();
		String resubmDraftDiffStr = resubmDraftDao.getResubmDraftFormRespVersionDiffDetails
				("er_study", studyId, versionNewMajor, versionNewMinor);
		if (StringUtil.isEmpty(resubmDraftDiffStr)){
			return jsFormRespDiffArray;
		}
		JSONArray jsResubmDraftDiffArray;
		try {
			jsResubmDraftDiffArray = new JSONArray(resubmDraftDiffStr);
		} catch (JSONException e1) {
			e1.printStackTrace();
			return jsFormRespDiffArray;
		}
		
		// Comparing the difference of the two versions to Re-submission Draft version difference.
		JSONArray list = this.compareMergeDiffs(grpId, jsFormRespDiffArray, jsResubmDraftDiffArray);
		System.out.println("Finish");

		return list;
	}


	public JSONArray fetchProtocolVersionDiffDetails(String versionOld,
			String versionNew, int studyId, String grpId) {

		// Old Version Details
		HashMap<String, HashMap<String, String>> oldVersionFields = new HashMap<String, HashMap<String, String>>();

		oldVersionFields = getProtocolVersion(studyId, versionOld);

		// New Version Details
		HashMap<String, HashMap<String, String>> newVersionFields = new HashMap<String, HashMap<String, String>>();

		newVersionFields = getResubmDraftProtocolVersion(studyId, versionNew);

		// Comparing the versions and extracting the difference between the two
		// versions.
		// List<HashMap<String, String>> versionDiffList = new
		// ArrayList<HashMap<String, String>>();
		//List<JSONObject> versionDiffList = new ArrayList<JSONObject>();
		JSONArray list = new JSONArray();

		HashSet<String> fields = new HashSet<String>();

		Iterator i = oldVersionFields.keySet().iterator();

		String oldVersionKey = "";
		HashSet<String> versionDiff = new HashSet<String>();
		try {
			while (i.hasNext()) {
				oldVersionKey = (String) i.next();
				// HashMap<String, String> versionDiffMap = new HashMap<String,
				// String>();
				JSONObject versionDiffMap = new JSONObject();
				if (!isEmpty(oldVersionFields.get(oldVersionKey))) {
					String fieldLabel = oldVersionFields.get(oldVersionKey)
							.get("label").replaceAll(" ", "__");
					versionDiff.add(oldVersionKey);

					if (newVersionFields.containsKey(oldVersionKey)) {
						if (!isEqual(oldVersionFields.get(oldVersionKey),
								newVersionFields.get(oldVersionKey))) {
							versionDiffMap = createProtocolVersionDiffJSON(
								grpId, oldVersionKey,
								oldVersionFields.get(oldVersionKey).get("label"),
								oldVersionFields.get(oldVersionKey).get("data"),
								newVersionFields.get(oldVersionKey).get("data"),
								oldVersionFields.get(oldVersionKey).get("title"),
								fieldLabel
							);

							//versionDiffList.add(versionDiffMap);
							list.put(versionDiffMap);
							/*
							 * fldHTML.append("<tr>" + "<td>" +
							 * oldVersionFields.get(oldVersionKey).get("title")
							 * + "</td>" + "<td>" +
							 * oldVersionFields.get(oldVersionKey).get("label")
							 * + "</td>" + "<td>" +
							 * oldVersionFields.get(oldVersionKey).get("data") +
							 * "</td>" + "<td>" +
							 * newVersionFields.get(oldVersionKey).get("data") +
							 * "</td>" + "<td>" + "<textarea type='text' id='"+
							 * fieldLabel +"_"+ "notes" +"' "+ "name='"+
							 * fieldLabel +"_"+ "notes" +"' " +
							 * "rows='2' maxlength='50'></textarea>" + "</td>" +
							 * "<td>" + cdReviewBoard.toPullDown(fieldLabel +"_"
							 * +"revBoard") + "</td>" + "</tr>");
							 */

						}
					} else {
						if (null != oldVersionFields.get(oldVersionKey)){
							versionDiffMap = createProtocolVersionDiffJSON(
								grpId, oldVersionKey,
								oldVersionFields.get(oldVersionKey).get("label"),
								"",
								newVersionFields.get(oldVersionKey).get("data"),
								oldVersionFields.get(oldVersionKey).get("title"),
								fieldLabel
							);

							//versionDiffList.add(versionDiffMap);
							list.put(versionDiffMap);

						/*
						 * fldHTML.append("<tr>" + "<td>" +
						 * oldVersionFields.get(oldVersionKey).get("title") +
						 * "</td>" + "<td>" +
						 * oldVersionFields.get(oldVersionKey).get("label") +
						 * "</td>" + "<td>" +
						 * oldVersionFields.get(oldVersionKey).get("data") +
						 * "</td>" + "<td>" + "" + "</td>" + "<td>" +
						 * "<textarea type='text' id='"+ fieldLabel +"_"+
						 * "notes" +"' "+ "name='"+ fieldLabel +"_"+ "notes"
						 * +"' " + "rows='2' maxlength='50'></textarea>" +
						 * "</td>" + "<td>" +
						 * cdReviewBoard.toPullDown(fieldLabel +"_" +"revBoard")
						 * + "</td>" + "</tr>");
						 */
						}
					}
				}
			}

			i = newVersionFields.keySet().iterator();

			String newVersionKey = "";

			while (i.hasNext()) {
				newVersionKey = (String) i.next();
				if (!isEmpty(newVersionFields.get(newVersionKey))) {
					if (!versionDiff.contains(newVersionKey)) {
						// HashMap<String, String> versionDiffMap = new
						// HashMap<String, String>();
						JSONObject versionDiffMap = new JSONObject();
						versionDiff.add(newVersionKey);
						String fieldLabel = newVersionFields.get(newVersionKey)
								.get("label").replaceAll(" ", "__");
						if (oldVersionFields.containsKey(newVersionKey)) {
							if (!isEqual(oldVersionFields.get(newVersionKey),
									newVersionFields.get(newVersionKey))) {
								versionDiffMap = createProtocolVersionDiffJSON(
									grpId, newVersionKey,
									newVersionFields.get(newVersionKey).get("label"),
									oldVersionFields.get(newVersionKey).get("data"),
									newVersionFields.get(newVersionKey).get("data"),
									newVersionFields.get(newVersionKey).get("title"),
									fieldLabel
								);

								//versionDiffList.add(versionDiffMap);
								list.put(versionDiffMap);
								/*
								 * fldHTML.append("<tr>" + "<td>" +
								 * newVersionFields
								 * .get(newVersionKey).get("title") + "</td>" +
								 * "<td>" +
								 * newVersionFields.get(newVersionKey).get
								 * ("label") + "</td>" + "<td>" +
								 * oldVersionFields
								 * .get(newVersionKey).get("data") + "</td>" +
								 * "<td>" +
								 * newVersionFields.get(newVersionKey).get
								 * ("data") + "</td>" + "<td>" +
								 * "<textarea type='text' id='"+ fieldLabel
								 * +"_"+ "notes" +"' "+ "name='"+ fieldLabel
								 * +"_"+ "notes" +"' " +
								 * "rows='2' maxlength='50'></textarea>" +
								 * "</td>" + "<td>" +
								 * cdReviewBoard.toPullDown(fieldLabel +"_"
								 * +"revBoard") + "</td>" + "</tr>");
								 */
							}
						} else {
							versionDiffMap = createProtocolVersionDiffJSON(
								grpId, newVersionKey,
								newVersionFields.get(newVersionKey).get("label"),
								"",
								newVersionFields.get(newVersionKey).get("data"),
								newVersionFields.get(newVersionKey).get("title"),
								fieldLabel
							);

							//versionDiffList.add(versionDiffMap);
							list.put(versionDiffMap);
							/*
							 * fldHTML.append("<tr>" + "<td>" +
							 * newVersionFields.get(newVersionKey).get("title")
							 * + "</td>" + "<td>" +
							 * newVersionFields.get(newVersionKey).get("label")
							 * + "</td>" + "<td>" + "" + "</td>" + "<td>" +
							 * newVersionFields.get(newVersionKey).get("data") +
							 * "</td>" + "<td>" + "<textarea type='text' id='"+
							 * fieldLabel +"_"+ "notes" +"' "+ "name='"+
							 * fieldLabel +"_"+ "notes" +"' " +
							 * "rows='2' maxlength='50'></textarea>" + "</td>" +
							 * "<td>" + cdReviewBoard.toPullDown(fieldLabel +"_"
							 * +"revBoard") + "</td>" + "</tr>");
							 */
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Finish");

		// return versionDiffList;
		return list;
	}

	private HashMap<String, HashMap<String, String>> getVersionHashMap(JSONObject result){
		HashMap<String, HashMap<String, String>> versionFields = new HashMap<String, HashMap<String, String>>();
		try{
			JSONArray sections = result.getJSONArray("sections");
			List<JSONArray> listFields = new ArrayList<JSONArray>();

			CodeDao codeDao = new CodeDao();
			int codeId = 0;
			for (int t = 0; t < sections.length(); t++) {
	
				listFields
						.add(sections.getJSONObject(t).getJSONArray("fields"));
				JSONArray listFieldDetails = sections.getJSONObject(t)
						.getJSONArray("fields");
				for (int i = 0; i < listFieldDetails.length(); i++) {
					HashMap<String, String> fieldDetails = new HashMap<String, String>();
					JSONObject fields = listFieldDetails.getJSONObject(i);
					String type = fields.getString("type");
					if ("STD".equals(type)) {
						Iterator stdField = fields.keys();
						while (stdField.hasNext()) {
							String field = stdField.next().toString();
							if (!"type".equals(field)) {
								String data = fields.getString(field);
								fieldDetails.put("data", data);
								if (STUDY_CCSG.equals(field)){
									data = (StringUtil.isEmpty(data))? EMPTY_STRING : data;
									data = (StringUtil.stringToNum(data) == 0) ? "Unchecked" : "Checked";
									fieldDetails.put("data", data);
								}
								fieldDetails.put("label", FlxPageFields
										.getFlxPageFieldByKey(field)
										.getFieldLabel());
								fieldDetails.put("title", sections
										.getJSONObject(t).getString("title"));
								versionFields.put(FlxPageFields
										.getFlxPageFieldByKey(field)
										.getFieldKeyword(), fieldDetails);
							}
						}
					} else if ("MSD".equals(type)) {
						String data = fields.getString("data");
						fieldDetails.put("data", data);
						if (!StringUtil.isEmpty(data)){
							try{
								String codeSubtype = fields.getString("codeSubtype");
								if (STUDY_ADM_DIV.equals(codeSubtype) || STUDY_ADM_DEPT.equals(codeSubtype)){									
									CodeDao cdMSD = new CodeDao();
									cdMSD.getCodeValuesById(StringUtil.stringToNum(data));
									data = (null == cdMSD.getCDesc() || cdMSD.getCDesc().size() < 1)?
											"" : (String) cdMSD.getCDesc().get(0);
								} else {
									CodeDao cdMSD = new CodeDao();
									cdMSD.getCodeValuesBySubTypes("studyidtype", new String[] {codeSubtype});
									
									String fldType = (null == (cdMSD.getCodeCustom()).get(0))? EMPTY_STRING : (cdMSD.getCodeCustom()).get(0).toString();
									fldType = (StringUtil.isEmpty(fldType ))? Str_input : fldType;
									
									if (Str_dropdown.equals(fldType)){
										try{
											JSONObject fldConfigJSON = new JSONObject((cdMSD.getCodeCustom1()).get(0).toString());
											String fldRespCodelstType = fldConfigJSON.getString("codelst_type");
											
											if (! StringUtil.isEmpty(fldRespCodelstType)){
												CodeDao cdMSDResp = new CodeDao();
												cdMSDResp.getCodeValuesBySubTypes(fldRespCodelstType, new String[] {data});
												
												@SuppressWarnings("unchecked")
												ArrayList<String> cdDescriptions = cdMSDResp.getCDesc();
												if (null != cdDescriptions){
													data = (String) cdDescriptions.get(0);	
												}
											}
										} catch (JSONException jsE){}										
									}
								}
								fieldDetails.put("data", data);
							}catch (Exception e){}
						}
						// codeDao.getCodeId("studyidtype",fields.getString("codeSubtype"));
						// fieldDetails.put("label",
						// codeDao.getCodeDescription());
						fieldDetails.put("label", fields.getString("label"));
						fieldDetails.put("title", sections.getJSONObject(t)
								.getString("title"));
						versionFields.put(fields.getString("codeSubtype"),
								fieldDetails);
					} else if ("studySection".equals(type)) {						
						Iterator ssField = fields.keys();						
						while (ssField.hasNext()) {							
							String field = ssField.next().toString();
							if (!"type".equals(field)) {
								codeId = codeDao.getCodeId("section", field);
								String description = codeDao.getCodeDesc(codeId);
								System.out.println("description : " + description);
								if (!"type".equals(field)) {
									String value = (String)fields.get(field);								
									fieldDetails.put("data", value);
									fieldDetails.put("label", description);
									fieldDetails.put("title", sections
											.getJSONObject(t).getString("title"));
									versionFields.put(description, fieldDetails);
								}
							}
						}
					} else if ("indide".equals(type)) {						
						Iterator ssField = fields.keys();						
						while (ssField.hasNext()) {							
							String field = ssField.next().toString();
							if (!"type".equals(field)) {
								if (!"type".equals(field)) {
									String value = (String)fields.get(field);
									value = value.replaceAll(",\"", ", \"");
									fieldDetails.put("data", value);
									fieldDetails.put("label", field);
									fieldDetails.put("title", sections
											.getJSONObject(t).getString("title"));
									versionFields.put(field, fieldDetails);
								}								
							}
						}
					} else if ("steam".equals(type)) {
						System.out.println("in steam type");
						Iterator ssField = fields.keys();						
						while (ssField.hasNext()) {							
							String field = ssField.next().toString();
							if (!"type".equals(field)) {								
								if (!"type".equals(field)) {
									String value = (String)fields.get(field);
									value = value.replaceAll(",\"", ", \"");
									fieldDetails.put("data", value);
									fieldDetails.put("label", field);
									fieldDetails.put("title", sections
											.getJSONObject(t).getString("title"));
									versionFields.put(field, fieldDetails);
								}
							}
						}
					}
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return versionFields;
	}

	public HashMap<String, HashMap<String, String>> getResubmDraftProtocolVersion(
			int studyId, String studyVersion) {
		HashMap<String, HashMap<String, String>> versionFields = new HashMap<String, HashMap<String, String>>();

		try {
			ResubmDraftDao resubmDraftDao = new ResubmDraftDao();
			String json = resubmDraftDao.getPageVersionDetails("er_study",
					studyId, studyVersion);
			JSONObject result = new JSONObject(json);
			versionFields =  getVersionHashMap(result);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return versionFields;
	}

	public JSONObject getResubmDraftFormResponses(
			int studyId, String studyVersion) {
		JSONObject result = null;
		try {
			ResubmDraftDao resubmDraftDao = new ResubmDraftDao();
			String json = resubmDraftDao.getResubmDraftFormResponses("er_study",
					studyId, studyVersion);
			result = new JSONObject(json);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return result;
	}

	public HashMap<String, HashMap<String, String>> getProtocolVersion(
			int studyId, String studyVersion) {
		HashMap<String, HashMap<String, String>> versionFields = new HashMap<String, HashMap<String, String>>();

		try {
			UIFlxPageDao uIFlxPageDao = new UIFlxPageDao();
			String json = uIFlxPageDao.getPageVersionDetails("er_study",
					studyId, studyVersion);
			JSONObject result = new JSONObject(json);
			versionFields = getVersionHashMap(result);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return versionFields;
	}
	
	public JSONObject getProtocolFormResponses(
			int studyId, String studyVersion) {
		JSONObject result = null;

		try {
			UIFlxPageDao uIFlxPageDao = new UIFlxPageDao();
			String json = uIFlxPageDao.getPageVersionFormResponses("er_study",
					studyId, studyVersion);
			if (!StringUtil.isEmpty(json)){
				result = new JSONObject(json);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean isEmpty(HashMap<String, String> fieldsMap) {
		String data = fieldsMap.get("data");
		if ((!(data == null)) && (!(data.trim()).equals(""))
				&& ((data.trim()).length() > 0))
			return false;
		else
			return true;
	}
	
	public boolean isEqual(HashMap<String, String> oldVersion,
			HashMap<String, String> newVersion) {
		if (oldVersion.get("data").equals(newVersion.get("data"))) {
			return true;
		}
		return false;
	}

	public boolean isEqual(JSONObject oldVersion,
			JSONObject newVersion) {
		String oldKey = null, newKey = null;
		String oldData = null, newData = null;
		/*try {
			oldKey = oldVersion.getString("keyword");
		} catch (JSONException e) {}
		oldKey = StringUtil.isEmpty(oldKey)? EMPTY_STRING : oldKey;

		try {
			newKey = newVersion.getString("keyword");
		} catch (JSONException e) {}
		newKey = StringUtil.isEmpty(newKey)? EMPTY_STRING : newKey;*/
		
		try {
			oldData = oldVersion.getString("data");
		} catch (JSONException e) {}
		oldData = StringUtil.isEmpty(oldData)? EMPTY_STRING : oldData;

		try {
			newData = newVersion.getString("data");
		} catch (JSONException e) {}
		newData = StringUtil.isEmpty(newData)? EMPTY_STRING : newData;
		
		if (oldData.equals(newData)) {
			return true;
		}
		return false;
	}

	public boolean contains(List<HashMap<String, String>> versionDiffList,
			String key) {
		Iterator i = versionDiffList.iterator();
		while (i.hasNext()) {
			HashMap<String, String> field = (HashMap<String, String>) i.next();
			if (field.containsValue(key))
				return true;
		}
		return false;
	}
	
	public JSONArray fetchAttachProtocolVersionDiff(int studyId, String grpId) {	
		JSONObject versionDetailsJSON = new JSONObject();
		try {
			versionDetailsJSON.put("studyId", studyId);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		JSONObject lastVersionDetailsJSON = getLastResubmDraftVersionNumber(versionDetailsJSON);
		int versionNum = 0;
		try {
			versionNum = lastVersionDetailsJSON.getInt("majorVersion");
		} catch (JSONException e) {
			e.printStackTrace();
			versionNum = 0;
		}
		if (versionNum <= 0){
			return new JSONArray();
		}

		int minorVerNum = 0;
		try {
			minorVerNum = lastVersionDetailsJSON.getInt("minorVersion");
		} catch (JSONException e) {
			e.printStackTrace();
			minorVerNum = 0;
		}
		
		String fullVerNum = EMPTY_STRING;
		try {
			fullVerNum = lastVersionDetailsJSON.getString("fullVersion");
		} catch (JSONException e) {
			e.printStackTrace();
		}

		int versionNumOld = -1;
		try {
			versionNumOld = lastVersionDetailsJSON.getInt("lastMajorVersion");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		int minorVerNumOld = -1;
		try {
			minorVerNumOld = lastVersionDetailsJSON.getInt("lastMinorVersion");
		} catch (JSONException e) {
			e.printStackTrace();
		}

		String fullVerNumOld = EMPTY_STRING;
		try {
			fullVerNumOld = lastVersionDetailsJSON.getString("lastFullVersion");
		} catch (JSONException e) {
			e.printStackTrace();
		}

		System.out.println("fullVerNumOld:::"+fullVerNumOld+":::fullVerNumNew:::"+fullVerNum);
		//Diff versions first
		JSONArray jsAttachDiffArray = fetchAttachProtocolVersionDetails(fullVerNumOld, fullVerNum, studyId, grpId);
		
		//Diff resubmission draft second
		return fetchAttachProtocolVersionResubmDraftDiffDetails(versionNum, minorVerNum, studyId, grpId, jsAttachDiffArray);
	}
	
	public JSONArray fetchAttachProtocolVersionDetails(String versionOld,
			String versionNew, int studyId, String grpId) {	
		
		StudyVerDao sVerDao = new StudyVerDao();
		String prevVer="";
		String curVer="";
		ArrayList<String> newAttachList= new ArrayList<String>();
		ArrayList<String> deletedAttachList= new ArrayList<String>();
		newAttachList.clear();
		deletedAttachList.clear();
		int found =0;	
		ArrayList<String> verList = new ArrayList<String>();
		if (!"LIND".equals(CFG.EIRB_MODE)){
			verList = sVerDao.getStudyVersions(studyId);
		}else{
			verList = sVerDao.getStudyLindVersions(studyId);
		}
		curVer = (String)verList.get(0);
		prevVer = (String)verList.get(1);
		ArrayList<String> prevVerList = sVerDao.getStudyVerAndApndxDetails(studyId, prevVer, false);
		ArrayList<String> curVerList = sVerDao.getStudyVerAndApndxDetails(studyId, curVer, true);
		ArrayList<Integer> studyVerIgnoreIds = new ArrayList<Integer>();
		studyVerIgnoreIds.addAll(getStudyVerIgnoreIds(studyId,prevVer));	
        for (String temp : prevVerList) {
        	if(!(curVerList.contains(temp))) {
        		boolean ignoreFlag = false;
        		StringTokenizer tokenizer=new StringTokenizer(temp," ");
        		String tokenOne=tokenizer.nextToken();
        		System.out.println("Final Docs--->"+studyVerIgnoreIds.size());
        		if(sVerDao.getPrevApndxUris().contains(tokenOne)){
        			int fileIndx=sVerDao.getPrevApndxUris().indexOf(tokenOne);
        			int studyVerPk = (Integer) sVerDao.getPrevStudyVerIds().get(fileIndx);
        			System.out.println("Ignored????===="+studyVerIgnoreIds.contains(studyVerPk));
        			if(studyVerIgnoreIds.contains(studyVerPk)){
        				continue;
        			}
        		}
        		if(studyVerIgnoreIds.size()>0 && sVerDao.getPrevStudyVerIds().containsAll(studyVerIgnoreIds)){
        			ignoreFlag=true;
        		}
        		System.out.println("Inside deletedAttachList tokenOne--->"+tokenOne);
        		if (tokenOne.endsWith(".doc") || tokenOne.endsWith(".docx")){
        			String tmpTokenOne = tokenOne;
        			tmpTokenOne = tmpTokenOne.substring(0, tmpTokenOne.indexOf("."));
        			tmpTokenOne = tmpTokenOne + ".pdf";
        			System.out.println("tmpTokenOne--->"+tmpTokenOne);
        			if (sVerDao.getCurrApndxUris().contains(tmpTokenOne)){
        				int indx = sVerDao.getCurrApndxUris().indexOf(tmpTokenOne);
        				int stampFlag = (Integer)sVerDao.getCurrStampFlags().get(indx);
        				System.out.println("indx-->"+sVerDao.getCurrApndxUris().indexOf(tmpTokenOne));
        				System.out.println("stampFlag-->"+stampFlag);
        				if (stampFlag==1){
        					ignoreFlag=true;
        				}
        			}
        			
        		}
        		
        		System.out.println("Ignore Flag--->"+ignoreFlag);
        		if(!ignoreFlag){
        			deletedAttachList.add(tokenOne);  
        		}
        	}
		}        
        for (String temp1 : curVerList) {
        	if(!(prevVerList.contains(temp1))) {
        		boolean ignoreFlag = false;
        		StringTokenizer tokenizer=new StringTokenizer(temp1," ");
        		String tokenOne=tokenizer.nextToken();
        		System.out.println("Inside newAttachList tokenOne--->"+tokenOne);
        		if (tokenOne.endsWith(".pdf")){
        			String tmpTokenOne = tokenOne;
        			tmpTokenOne = tmpTokenOne.substring(0, tmpTokenOne.indexOf("."));
        			System.out.println("tmpTokenOne--->"+tmpTokenOne);
        			System.out.println("doc?????"+sVerDao.getPrevApndxUris().contains((tmpTokenOne+".doc")));
        			System.out.println("docx?????"+sVerDao.getPrevApndxUris().contains((tmpTokenOne+".docx")));
        			if (sVerDao.getPrevApndxUris().contains((tmpTokenOne+".doc")) || sVerDao.getPrevApndxUris().contains((tmpTokenOne+".docx"))){
        				int indx = sVerDao.getCurrApndxUris().indexOf(tokenOne);
        				
        				int stampFlag = (Integer)sVerDao.getCurrStampFlags().get(indx);
        				System.out.println("indx-->"+sVerDao.getPrevApndxUris().indexOf(tmpTokenOne));
        				System.out.println("stampFlag-->"+stampFlag);
        				if (stampFlag==1){
        					ignoreFlag=true;
        				}
        			}
        		}
        		if(!ignoreFlag){
        			newAttachList.add(tokenOne);
        		}
        	}
		}     
		
		//List<JSONObject> versionDiffList = new ArrayList<JSONObject>();
		JSONArray list = new JSONArray();	
		CodeDao cdReviewBoard = new CodeDao();
		cdReviewBoard.getCodeValues("rev_board");
		cdReviewBoard.setCType("rev_board");
		cdReviewBoard.setForGroup(grpId);		
		JSONObject attachNewDiffMap = new JSONObject();
		String fieldLabelNew="Attachment_New";		
		JSONObject attachDeleteDiffMap = new JSONObject();
		String fieldLabelDelete="Attachment_Deleted";

		try {
			attachNewDiffMap = createProtocolVersionDiffJSON(
				grpId, "NEWATTACHMENT",
				"Attachment",
				"-",
				newAttachList.toString(),
				"New Attachments",
				fieldLabelNew,
				EMPTY_STRING,
				EMPTY_STRING
			);
			if(newAttachList.size() != 0 ) {
				//versionDiffList.add(attachDiffMap);
				list.put(attachNewDiffMap);
			}
			
			attachDeleteDiffMap = createProtocolVersionDiffJSON(
				grpId, "DELETEDATTACHMENT",
				"Attachment",
				deletedAttachList.toString(),
				"-",
				"Deleted Attachments",
				fieldLabelDelete,
				EMPTY_STRING,
				EMPTY_STRING
			);
			
			if(deletedAttachList.size() != 0 )  {
				//versionDiffList.add(attachDiffMap1);
				list.put(attachDeleteDiffMap);
			}
		} catch (Exception e) {			
			e.printStackTrace();
		}				
		return list;
	}
	
	private List getStudyVerIgnoreIds(int studyId, String prevVer) {
		int countPrevDocs =0;
		ArrayList<Integer> studyVerIgnoreIds = new ArrayList<Integer>();
		int majorVer = Integer.parseInt(prevVer.substring(0,(prevVer.indexOf("."))));
		int minorVer = Integer.parseInt(prevVer.substring((prevVer.indexOf(".")+1), prevVer.length()));
		try{
			UIFlxPageDao uiFlxDao = new UIFlxPageDao();
			studyVerIgnoreIds.addAll(uiFlxDao.getStudyVerIgnoreIds(studyId, majorVer, minorVer));
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return studyVerIgnoreIds;
	}
	
	private String fetchAttachProtocolVersionDiffForVersion(String attachDiff) {
		CodeDao codeDao = new CodeDao();
		StringBuilder attachVersion = new StringBuilder();

		if(EJBUtil.isEmpty(attachDiff))
		{
			return LC.Attach_Version_No_Diff_Message;
		}
		try {
			JSONArray attachDiffList = new JSONArray(attachDiff);
			int noOfFields = attachDiffList.length();
			if(noOfFields == 0){
				return LC.Attach_Version_No_Diff_Message;
			}
			for (int i = 0; i < noOfFields; i++) {
				attachVersion
				.append("<tr>");
				attachVersion
				.append("<td>" + attachDiffList.getJSONObject(i).get("title") + "</td>");
				attachVersion
				.append("<td>" + attachDiffList.getJSONObject(i).get("fieldName") + "</td>");
				attachVersion
				.append("<td>" + attachDiffList.getJSONObject(i).get("oldValue") + "</td>");
				attachVersion
				.append("<td>" + attachDiffList.getJSONObject(i).get("newValue") + "</td>");
				if( (attachDiffList.getJSONObject(i).get("title")).equals("New Attachments") ) {
					attachVersion
					.append("<td>" + attachDiffList.getJSONObject(i).get("notes1") + "</td>");
				} 
				if( (attachDiffList.getJSONObject(i).get("title")).equals("Deleted Attachments") ) {
					attachVersion
					.append("<td>" + attachDiffList.getJSONObject(i).get("notes2") + "</td>");
				}
				if( (attachDiffList.getJSONObject(i).get("title")).equals("New Attachments") ) {
					attachVersion
					.append("<td>" + codeDao.getCodeDescription(Integer.parseInt((String) attachDiffList.getJSONObject(i).get("revBoard1"))) + "</td>");
				} 
				if( (attachDiffList.getJSONObject(i).get("title")).equals("Deleted Attachments") ) {
					attachVersion
					.append("<td>" + codeDao.getCodeDescription(Integer.parseInt((String) attachDiffList.getJSONObject(i).get("revBoard2"))) + "</td>");
				}
				attachVersion
				.append("</tr>");
			}
		} catch (JSONException e) {
			return LC.Attach_Version_No_Diff_Message;
		}

		return attachVersion.toString();
	}

	public JSONArray fetchFormRespProtocolVersionDetails(String versionOld,
		String versionNew, int studyId, String grpId) {

		// Old Version Form Responses
		JSONObject oldFormResponses = getProtocolFormResponses(studyId, versionOld);

		// New Version Form Responses
		JSONObject newFormResponses = getResubmDraftFormResponses(studyId, versionNew);

		// Comparing the form responses and extracting the difference between the two sets of form responses.
		JSONArray list = new JSONArray();

		HashSet<String> fields = new HashSet<String>();

		Iterator formIterator= null;

		if (null != oldFormResponses){
			formIterator = oldFormResponses.keys();

			String oldVersionFormName = EMPTY_STRING;
			HashSet<String> versionDiff = new HashSet<String>();
			try {
				while (formIterator.hasNext()) {
					oldVersionFormName = (String) formIterator.next();
					JSONObject versionDiffMap = new JSONObject();

					if (newFormResponses.has(oldVersionFormName)) {
						String oldFormResponseStr = (oldFormResponses.get(oldVersionFormName)).toString();
						oldFormResponseStr = StringUtil.isEmpty(oldFormResponseStr)? EMPTY_STRING : oldFormResponseStr;
						String newFormResponseStr = (newFormResponses.get(oldVersionFormName)).toString();
						newFormResponseStr = StringUtil.isEmpty(newFormResponseStr)? EMPTY_STRING : newFormResponseStr;

						// If strings are same, the responses match, so no difference
						if (!oldFormResponseStr.equals(newFormResponseStr)) {
							JSONObject oldFormResponseJSON = new JSONObject(oldFormResponseStr);
							JSONObject newFormResponseJSON = new JSONObject(newFormResponseStr);

							String oldFormName = EMPTY_STRING, newFormName = EMPTY_STRING;
							try {
								oldFormName = oldFormResponseJSON.getString("formName");
							} catch (JSONException e){}
							try {
								newFormName = newFormResponseJSON.getString("formName");
							} catch (JSONException e){}
							
							if (StringUtil.isEmpty(oldFormName) || StringUtil.isEmpty(newFormName)){ continue; }

							//Form PK check
							int oldFormId = 0, newFormId = 0;
							try {
								oldFormId = oldFormResponseJSON.getInt("formId");
							} catch (JSONException e){}
							try {
								newFormId = newFormResponseJSON.getInt("formId");
							} catch (JSONException e){}

							if (oldFormId != newFormId){
								String fldKey = oldVersionFormName + "_formId";
								versionDiffMap = createProtocolVersionDiffJSON(
									grpId, fldKey,
									oldFormName,
									"[Deleted Form]", //oldFormId,
									"[New Form]", //newFormId,
									oldFormName,
									fldKey //"Form Identifier"
								);
								list.put(versionDiffMap);

								// If form is different, form response is different
								fldKey = oldVersionFormName + "_formResponseId";
								versionDiffMap = createProtocolVersionDiffJSON(
									grpId, fldKey,
									oldFormName,
									"[Deleted Form Response]", //oldFormRespId,
									"[New Form Response]", //newFormRespId,
									oldFormName,
									fldKey //"Form Response Identifier"
								);
								list.put(versionDiffMap);
								continue;
							}

							//Form Response PK check (If the form response is deleted and recreated, it will be caught here)
							int oldFormRespId = 0, newFormRespId = 0;
							try {
								oldFormRespId = oldFormResponseJSON.getInt("formRespId");
							} catch (JSONException e){}
							try {
								newFormRespId = newFormResponseJSON.getInt("formRespId");
							} catch (JSONException e){}

							if (oldFormRespId != newFormRespId){
								String fldKey = oldVersionFormName + "_formResponseId";
								versionDiffMap = createProtocolVersionDiffJSON(
									grpId, fldKey,
									oldFormName,
									"[Deleted Form Response]", //oldFormRespId,
									"[New Form Response]", //newFormRespId,
									oldFormName,
									fldKey //"Form Response Identifier"
								);
								list.put(versionDiffMap);
							}

							//Form Response Status check
							String oldFormRespStatus = null, newFormRespStatus = null;
							try {
								oldFormRespStatus = oldFormResponseJSON.getString("formRespStatus");
							} catch (JSONException e){}
							oldFormRespStatus = StringUtil.isEmpty(oldFormRespStatus)? EMPTY_STRING : oldFormRespStatus;
							try {
								newFormRespStatus = newFormResponseJSON.getString("formRespStatus");
							} catch (JSONException e){}
							newFormRespStatus = StringUtil.isEmpty(newFormRespStatus)? EMPTY_STRING : newFormRespStatus;
							
							if (!oldFormRespStatus.equals(newFormRespStatus)){
								String fldKey = oldVersionFormName + "_formResponseStat";
								versionDiffMap = createProtocolVersionDiffJSON(
									grpId, fldKey,
									oldFormName,
									oldFormRespStatus,
									newFormRespStatus,
									oldFormName,
									fldKey //"Form Response Status"
								);
								list.put(versionDiffMap);
							}

							String oldFormRespDataStr = oldFormResponseJSON.getString("formRespData");
							oldFormRespDataStr = StringUtil.isEmpty(oldFormRespDataStr)? EMPTY_STRING : oldFormRespDataStr;
							String newFormRespDataStr = newFormResponseJSON.getString("formRespData");
							newFormRespDataStr = StringUtil.isEmpty(newFormRespDataStr)? EMPTY_STRING : newFormRespDataStr;

							// If strings are same, the form field data matches, so no difference
							if (!oldFormRespDataStr.equals(newFormRespDataStr)){
								JSONObject oldFormFieldsJSON = null;
								try {
									oldFormFieldsJSON = new JSONObject(oldFormRespDataStr);
								} catch (JSONException e) {}

								JSONObject newFormFieldsJSON = null;
								try {
									newFormFieldsJSON = new JSONObject(newFormRespDataStr);
								} catch (JSONException e) {}

								if ((oldFormFieldsJSON == null && newFormFieldsJSON != null) || (oldFormFieldsJSON != null && newFormFieldsJSON == null)){
									String fldKey = oldVersionFormName + "_formRespJSON";
									versionDiffMap = createProtocolVersionDiffJSON(
										grpId, fldKey,
										oldFormName,
										oldFormFieldsJSON,
										newFormFieldsJSON,
										oldFormName,
										fldKey //"Form Response JSON"
									);
									list.put(versionDiffMap);
								}
								if (oldFormFieldsJSON != null || newFormFieldsJSON != null){
									Iterator fieldIterator = oldFormFieldsJSON.keys();
									String oldVersionKey = EMPTY_STRING; 
											
									while (fieldIterator.hasNext()) {
										// This is field system id
										oldVersionKey = (String) fieldIterator.next();
										
										JSONObject oldFieldJSON = null;
										try{
											oldFieldJSON = oldFormFieldsJSON.getJSONObject(oldVersionKey);
										} catch (JSONException e){
											oldFieldJSON = null;
										}

										JSONObject newFieldJSON = null;
										try{
											newFieldJSON = newFormFieldsJSON.getJSONObject(oldVersionKey);
										} catch (JSONException e){
											newFieldJSON = null;
										}

										if (oldFieldJSON != null && newFieldJSON == null){
											versionDiffMap = createProtocolVersionDiffJSON(
												grpId, oldVersionKey,
												oldFieldJSON.getString("label"),
												oldFieldJSON.getString("data"),
												"-",
												oldFormName,
												oldVersionKey
											);
											list.put(versionDiffMap);
											continue;
										}

										if (newFormFieldsJSON.has(oldVersionKey)) {
											if (!isEqual(oldFormFieldsJSON.getJSONObject(oldVersionKey),
													newFormFieldsJSON.getJSONObject(oldVersionKey))) {
												versionDiffMap = createProtocolVersionDiffJSON(
													grpId, oldVersionKey,
													oldFieldJSON.getString("label"),
													oldFieldJSON.getString("data"),
													newFieldJSON.getString("data"),
													oldFormName,
													oldVersionKey
												);
												list.put(versionDiffMap);
											}
										} else {
											versionDiffMap = createProtocolVersionDiffJSON(
												grpId, oldVersionKey,
												oldFieldJSON.getString("label"),
												EMPTY_STRING,
												newFieldJSON.getString("data"),
												oldFormName,
												oldVersionKey
											);
											list.put(versionDiffMap);
										}
									}
									
									fieldIterator = newFormFieldsJSON.keys();
									String newVersionKey = EMPTY_STRING;

									while (fieldIterator.hasNext()) {
										newVersionKey = (String) fieldIterator.next();
										
										JSONObject oldFieldJSON = null;
										try{
											oldFieldJSON = oldFormFieldsJSON.getJSONObject(newVersionKey);
										} catch (JSONException e){
											oldFieldJSON = null;
										}

										JSONObject newFieldJSON = null;
										try{
											newFieldJSON = newFormFieldsJSON.getJSONObject(newVersionKey);
										} catch (JSONException e){
											newFieldJSON = null;
										}
										
										if (oldFieldJSON == null && newFieldJSON != null){
											versionDiffMap = createProtocolVersionDiffJSON(
												grpId, oldVersionKey,
												newFieldJSON.getString("label"),
												"-",
												newFieldJSON.getString("data"),
												newFormName,
												oldVersionKey
											);
											list.put(versionDiffMap);
											continue;
										}
										
										//if (!isEmpty(newFormFieldsJSON.get(newVersionKey))) {
											if (!versionDiff.contains(newVersionKey)) {
												versionDiff.add(newVersionKey);
												if (oldFieldJSON.has(newVersionKey)) {
													if (!isEqual(oldFieldJSON.getJSONObject(newVersionKey),
															newFieldJSON.getJSONObject(newVersionKey))) {
														versionDiffMap = createProtocolVersionDiffJSON(
															grpId, newVersionKey,
															newFieldJSON.getString("label"),
															oldFieldJSON.getString("data"),
															newFieldJSON.getString("data"),
															newFormName,
															newVersionKey
														);
														list.put(versionDiffMap);
													}
												} /*else {
													versionDiffMap = createProtocolVersionDiffJSON(
														grpId, newVersionKey,
														newFieldJSON.getString("label"),
														"",
														newFieldJSON.getString("data"),
														newFieldJSON.getString("title"),
														newVersionKey
													);
	
													list.put(versionDiffMap);
												}*/
											}
										//}
									}
								}
							}
						}
					} else {
						//form response deleted
						String oldFormResponseStr = (oldFormResponses.get(oldVersionFormName)).toString();
						oldFormResponseStr = StringUtil.isEmpty(oldFormResponseStr)? EMPTY_STRING : oldFormResponseStr;
	
						JSONObject oldFormResponseJSON = new JSONObject(oldFormResponseStr);
						
						String oldFormName = EMPTY_STRING, newFormName = EMPTY_STRING;
						try {
							oldFormName = oldFormResponseJSON.getString("formName");
						} catch (JSONException e){}
	
						if (StringUtil.isEmpty(oldFormName)){ continue; }
	
						//Form PK check
						int oldFormId = 0;
						try {
							oldFormId = oldFormResponseJSON.getInt("formId");
						} catch (JSONException e){}
	
						//Form Response PK check (If the form response is deleted and recreated, it will be caught here)
						int oldFormRespId = 0;
						try {
							oldFormRespId = oldFormResponseJSON.getInt("formRespId");
						} catch (JSONException e){}
	
						String fldKey = oldVersionFormName + "_formRespId";
						versionDiffMap = createProtocolVersionDiffJSON(
							grpId, fldKey,
							oldFormName,
							"-", // oldFormRespId,
							"[Deleted Form Response]",
							oldFormName,
							fldKey
						);
						list.put(versionDiffMap);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		formIterator = newFormResponses.keys();

		String newVersionFormName = EMPTY_STRING;

		try {
			while (formIterator.hasNext()) {
				newVersionFormName = (String) formIterator.next();
				JSONObject versionDiffMap = new JSONObject();
	
				if (null == oldFormResponses || !oldFormResponses.has(newVersionFormName)) {
					String newFormResponseStr = (newFormResponses.get(newVersionFormName)).toString();
					newFormResponseStr = StringUtil.isEmpty(newFormResponseStr)? EMPTY_STRING : newFormResponseStr;
	
					JSONObject newFormResponseJSON = new JSONObject(newFormResponseStr);
						
					String newFormName = EMPTY_STRING;
					try {
						newFormName = newFormResponseJSON.getString("formName");
					} catch (JSONException e){}
						
					if (StringUtil.isEmpty(newFormName)){ continue; }
					//Form PK check
					int newFormId = 0;
					try {
						newFormId = newFormResponseJSON.getInt("formId");
					} catch (JSONException e){}
	
					//Form Response PK check (If the form response is deleted and recreated, it will be caught here)
					int newFormRespId = 0;
					try {
						newFormRespId = newFormResponseJSON.getInt("formRespId");
					} catch (JSONException e){}
						
					//Form Response Status check
					String newFormRespStatus = null;
					try {
						newFormRespStatus = newFormResponseJSON.getString("formRespStatus");
					} catch (JSONException e){}
					newFormRespStatus = StringUtil.isEmpty(newFormRespStatus)? EMPTY_STRING : newFormRespStatus;
				
					String newFormRespDataStr = newFormResponseJSON.getString("formRespData");
					newFormRespDataStr = StringUtil.isEmpty(newFormRespDataStr)? EMPTY_STRING : newFormRespDataStr;

					JSONObject newFormFieldsJSON = null;
					try {
						newFormFieldsJSON = new JSONObject(newFormRespDataStr);
					} catch (JSONException e) {}
	
					if (newFormFieldsJSON != null){
						versionDiffMap = createProtocolVersionDiffJSON(
							grpId, newVersionFormName,
							newFormName,
							"-",
							"[New Form Response]",
							newFormName,
							newFormId + "_" + newFormRespId + "_New_Form_Response"
						);
						list.put(versionDiffMap);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Finish");

		// return versionDiffList;
		return list;
	}

	public JSONArray fetchFormResponseDiff(int studyId, String grpId) {
		JSONObject versionDetailsJSON = new JSONObject();
		try {
			versionDetailsJSON.put("studyId", studyId);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		JSONObject lastVersionDetailsJSON = getLastResubmDraftVersionNumber(versionDetailsJSON);
		int versionNum = 0;
		try {
			versionNum = lastVersionDetailsJSON.getInt("majorVersion");
		} catch (JSONException e) {
			e.printStackTrace();
			versionNum = 0;
		}
		if (versionNum <= 0){
			return new JSONArray();
		}

		int minorVerNum = 0;
		try {
			minorVerNum = lastVersionDetailsJSON.getInt("minorVersion");
		} catch (JSONException e) {
			e.printStackTrace();
			minorVerNum = 0;
		}
		
		String fullVerNum = EMPTY_STRING;
		try {
			fullVerNum = lastVersionDetailsJSON.getString("fullVersion");
		} catch (JSONException e) {
			e.printStackTrace();
		}

		int versionNumOld = -1;
		try {
			versionNumOld = lastVersionDetailsJSON.getInt("lastMajorVersion");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		int minorVerNumOld = -1;
		try {
			minorVerNumOld = lastVersionDetailsJSON.getInt("lastMinorVersion");
		} catch (JSONException e) {
			e.printStackTrace();
		}

		String fullVerNumOld = EMPTY_STRING;
		try {
			fullVerNumOld = lastVersionDetailsJSON.getString("lastFullVersion");
		} catch (JSONException e) {
			e.printStackTrace();
		}

		//Diff versions first
		JSONArray jsFormRespDiffArray = fetchFormRespProtocolVersionDetails(fullVerNumOld, fullVerNum, studyId, grpId);

		//Diff resubmission draft second
		return fetchFormRespProtocolVersionResubmDraftDiffDetails(versionNum, minorVerNum, studyId, grpId, jsFormRespDiffArray);
	}
}