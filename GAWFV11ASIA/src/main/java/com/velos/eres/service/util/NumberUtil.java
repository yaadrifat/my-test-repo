package com.velos.eres.service.util;

import java.sql.Connection;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Formatter;
import java.util.HashSet;
import java.util.Locale;
import java.util.StringTokenizer;

import com.velos.eres.business.common.CommonDAO;

public final class NumberUtil {

	private static String xslNumFormat;
	private static String xslFloatFormat;

	private static String numDecSymbol;
	private static String numGrpSymbol;

	private static String NUMFORMATCONST = "#,###.##";
	private static String FLOATFORMATCONST = "#,##0.00";

	private static String DECSYMBOLCONST = ".";
	private static String GRPSYMBOLCONST = ",";

	private static NumberFormat nf;
	private static DecimalFormat customDF;
	private static DecimalFormat customNumDF;
	
	 // Contains list of all columns which are to be treated as numeric for Ad-Hoc queries,
    // in Ad-Hoc queries the column's data type is not clear indication as primary keys and FK  are also numeric
	// Hence values are hard-coded here and needs to be maintained. 
	public static final HashSet<String> adhocQNumCols = new HashSet<String>(Arrays.asList(new String[]{"SPEC_QTY","STATUS_QUANTITY","exp_spec_qty","coll_spec_qty"
    		,"BGTCAL_INDIRECTCOST","BGTCAL_FRGBENEFIT","BGTCAL_DISCOUNT","GRAND_RES_COST","GRAND_RES_TOTALCOST"
    		,"GRAND_SOC_COST","GRAND_SOC_TOTALCOST","GRAND_SOC_SPONSOR","GRAND_SOC_VARIANCE","GRAND_TOTAL_COST"
    		,"GRAND_TOTAL_TOTCOST","GRAND_TOTAL_SPONSOR","GRAND_TOTAL_VARIANCE","GRAND_SALARY_COST"
    		,"GRAND_SALARY_TOTCOST","GRAND_FRINGE_COST","GRAND_FRINGE_TOTCOST","GRAND_DISC_COST"
    		,"GRAND_DISC_TOTALCOST","GRAND_IND_COST","GRAND_IND_TOTALCOST","BGTLI_UNIT_COST","BGTLI_NUM_UNITS"
    		,"LINEITEM_RESCOST","LINEITM_TOTAL_COST","INV_TOTAL","MSPAY_AMT_RECVD","MSRUL_AMOUNT","MSRUL_PT_COUNT"
    		,"MSRUL_LIMIT","STAPX_FILE_SIZE","SUBCOST_ITEM_COST","STSTP_CNT_FORMS","STSTP_CNT_CALNDRS"
    		,"STSIT_SAMP_SIZE","STVER_SECTION_CNT","STVER_ATTACHMENT_CNT","STUDY_NSAMPLSIZE","LBCAL_DURATION"
    		,"EVCRF_NUMBER","EVCST_COST","LBEVE_COST_CNT","LBEVE_APPNX_CNT","EXP_SPEC_QTY","COLL_SPEC_QTY"
    		,"STORAGE_CAPACITY","AVAIL_UNITS"}));
	

	// Function to round off the number
	public static Formatter roundOffNo(double val) {
		Formatter fmt = new Formatter();
		fmt.format("%.2f", val);
		return fmt;
	}

	public static boolean isInteger(String input) {
		try {
			Integer.parseInt(input);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
/**
 * Used to load number format setting in er_codelst
 * 
 */
	public static void setCodeLstNumFormat() {

		Statement stm = null;
		Connection conn = null;
		try {
			StringBuilder sql1 = new StringBuilder("Update ER_CODELST SET CODELST_DESC='");
			sql1.append(getNumDecSymbol());
			sql1.append("' WHERE codelst_type = 'num_format' AND codelst_subtyp = 'decimal_symbol'");

			StringBuilder sql2 = new StringBuilder("Update ER_CODELST SET CODELST_DESC='");
			sql2.append(getNumGrpSymbol());
			sql2.append("' WHERE codelst_type = 'num_format' AND codelst_subtyp = 'group_symbol'");

			conn = CommonDAO.getConnection();
			conn.setAutoCommit(false);
			stm = conn.createStatement();
			stm.addBatch(sql1.toString());
			stm.addBatch(sql2.toString());
			stm.executeBatch();
			conn.commit();
			
		} catch (Exception e) {
			Rlog.fatal("NumFormatUtils", "exception in setCodeLstNumFormat "
					+ e);

		}finally {
            try {
                if (stm != null)
                	stm.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
		}
	}

	// Use the "numFormatLocale" defined in eresearch.xml to set Locale info
	public static void setLocaleInfo() {

		try {

			DecimalFormat df;
			String localeStr = Configuration.getNumFormatLocale();
			Locale loc;
			if (xslNumFormat == null || xslNumFormat.isEmpty()
					|| numDecSymbol == null || numDecSymbol.isEmpty()
					|| numGrpSymbol == null || numGrpSymbol.isEmpty()
					|| xslFloatFormat == null || xslFloatFormat.isEmpty()||customDF==null || customNumDF==null) {

				if (localeStr != null && !localeStr.isEmpty()) {

					String langCode = "";
					String countryCode = "";
					StringTokenizer st = new StringTokenizer(localeStr, "-");

					if (st.hasMoreTokens()) {
						langCode = st.nextToken();
					}					
					if (st.hasMoreTokens()) {
						countryCode = st.nextToken();
					}					
					if(countryCode.isEmpty()){
						loc = new Locale(langCode);
					}else{
						loc = new Locale(langCode, countryCode);
					}
					
					nf = NumberFormat.getNumberInstance(loc);
					df = (DecimalFormat) nf;
					DecimalFormatSymbols dfs = df.getDecimalFormatSymbols();

					numDecSymbol = Character
							.toString(dfs.getDecimalSeparator());
					numGrpSymbol = Character.toString(dfs
							.getGroupingSeparator());

					String numTemp = "#~###^##";
					String floatTemp = "#~##0^00";

					xslNumFormat = numTemp.replace("~", numGrpSymbol);
					xslNumFormat = xslNumFormat.replace("^", numDecSymbol);

					xslFloatFormat = floatTemp.replace("~", numGrpSymbol);
					xslFloatFormat = xslFloatFormat.replace("^", numDecSymbol);
					customDF = new DecimalFormat(FLOATFORMATCONST);
					customDF.setDecimalFormatSymbols(dfs);
					if(!countryCode.isEmpty() && "US".equals(countryCode)){
						customDF.setNegativePrefix("(");
						customDF.setNegativeSuffix(")");
					}

					customNumDF = new DecimalFormat(NUMFORMATCONST);
					customNumDF.setDecimalFormatSymbols(dfs);
					
				} else {
					xslNumFormat = NUMFORMATCONST;
					numDecSymbol = DECSYMBOLCONST;
					numGrpSymbol = GRPSYMBOLCONST;
					xslFloatFormat = FLOATFORMATCONST;
					nf = NumberFormat.getNumberInstance(new Locale("en", "US"));
					customDF = new DecimalFormat(FLOATFORMATCONST);
					customDF.setDecimalFormatSymbols(new DecimalFormatSymbols(new Locale("en", "US")));
					customNumDF = new DecimalFormat(NUMFORMATCONST);
					customNumDF.setDecimalFormatSymbols(new DecimalFormatSymbols(new Locale("en", "US")));
				}
			}
		} catch (Exception e) {
			xslNumFormat = NUMFORMATCONST;
			numDecSymbol = DECSYMBOLCONST;
			numGrpSymbol = GRPSYMBOLCONST;
			xslFloatFormat = FLOATFORMATCONST;
			nf = NumberFormat.getNumberInstance(new Locale("en", "US"));
			customDF = new DecimalFormat(FLOATFORMATCONST);
			customDF.setDecimalFormatSymbols(new DecimalFormatSymbols(new Locale("en", "US")));
			customNumDF = new DecimalFormat(NUMFORMATCONST);
			customNumDF.setDecimalFormatSymbols(new DecimalFormatSymbols(new Locale("en", "US")));
			Rlog.fatal("NumFormatUtils", "exception in setLocaleInfo " + e);
		}
	}

	public static String getXSLNumFormatDef() {
		StringBuilder sb = new StringBuilder("<xsl:decimal-format name='numFormatDef' decimal-separator='");
		sb.append(getNumDecSymbol()).append("' grouping-separator='");
		sb.append(getNumGrpSymbol()).append("'  NaN=''/>");
		return sb.toString();
	}

	public static String getXSLFloatFormatDef() {
		StringBuilder sb = new StringBuilder("<xsl:decimal-format name='floatFormatDef' decimal-separator='");
		sb.append(getNumDecSymbol()).append("' grouping-separator='");
		sb.append(getNumGrpSymbol()).append("' NaN=''/>");
		return sb.toString();
	}

	public static String getNumDecSymbol() {
		if (numDecSymbol == null || numDecSymbol.isEmpty()) {
			setLocaleInfo();
		}
		return numDecSymbol;
	}

	public static String getNumGrpSymbol() {
		if (numGrpSymbol == null || numGrpSymbol.isEmpty()) {
			setLocaleInfo();
		}
		return numGrpSymbol;
	}

	public static String getXSLNumFormat() {
		return xslNumFormat;
	}

	public static String getXSLFloatFormat() {
		return xslFloatFormat;
	}
// format Number to the app locale and retrun a string
	public static String formatToNumber(Number n) {
		if (n != null) {
			return getCustomNumDF().format(n);
		}
		return getCustomNumDF().format(0);
	}
	
// format Number to the app locale and retrun a string
	public static String formatToFloat(Number n) {
		if (n != null) {
			return getCustomDF().format(n);
		}
		return getCustomDF().format(0);
	}	
	
	
	public static String formatObject(Object obj) {
		if (obj != null) {
			try{
				Double d = Double.parseDouble(obj.toString());
				return customDF.format(d);
			}catch(Exception e){
				Rlog.fatal("NumFormatUtils", "exception in formatString " + e);
				return obj.toString();
			}
		}
		return "";
	}
	
	public static DecimalFormat getCustomDF(){
		if(customDF==null){
			setLocaleInfo();
		}
		return customDF;
	}
	
	public static DecimalFormat getCustomNumDF(){
		if(customNumDF==null){
			setLocaleInfo();
		}
		return customNumDF;
	}
}
