/*
 * Classname			AccountAgentBean
 * 
 * Version information : 1.0
 *
 * Date					02/20/2001
 * 
 * Copyright notice : Velos Inc
 */

package com.velos.eres.service.accountAgent.impl;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.eres.business.account.impl.AccountBean;
import com.velos.eres.business.common.AccountDao;
import com.velos.eres.service.accountAgent.AccountAgentRObj;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.business.common.PortalDao;
/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the entity CMP.
 * 
 * @author sajal
 * @version 1.0, 02/20/2001
 * @ejbHome AccountAgentHome
 * @ejbRemote AccountAgentRObj
 */
@Stateless
public class AccountAgentBean implements AccountAgentRObj {

    /*
     * CLASS METHODS
     * 
     * public void setSessionContext(SessionContext context) public void
     * ejbActivate() public void ejbPassivate() public void ejbRemove() public
     * void ejbCreate() public AccountStateKeeper getAccountDetails(int accId)
     * public int setAccountDetails(AccountStateKeeper account) public int
     * updateAccount(AccountStateKeeper ask)
     * 
     * END OF CLASS METHODS
     */

    /**
     * 
     */
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    private static final long serialVersionUID = 3258135756097925680L;
    @Resource
    private SessionContext ctx;

    /**
     * Looks up on the Account id parameter passed in and constructs and returns
     * a AccountStateKeeper containing all the summary details of the account
     * 
     * @param accId
     *            The Account id
     * @see AccountStateKeeper
     */
    public AccountBean getAccountDetails(Integer accId) {

        AccountBean accBean = null;

        try {
            accBean = (AccountBean)em.find(AccountBean.class, accId);
            em.clear();
            return accBean;
        } catch (Exception e) {
            Rlog.fatal("account",
                    "Error in getAccountDetails() in AccountAgentBean" + e);
            return accBean;
        }

    }

    /**
     * Add the Account details in the AccountStateKeeper Object to the database
     * 
     * @param account
     *            An AccountStateKeeper containing account attributes to be set.
     * @return The Account Id for the account just created <br>
     *         0 - if the method fails
     */

    public int setAccountDetails(AccountBean account) {
        try {
            AccountBean ac = new AccountBean();

            ac.updateAccount(account);
            em.persist(ac);

            return (ac.getAccId());
        } catch (Exception e) {
            Rlog.fatal("account",
                    "Error in setAccountDetails() in AccountAgentBean " + e);
            return 0;
        }

    }

    /**
     * Update the Account details contained in the AccountStateKeeper Object to
     * the database
     * 
     * @param ask
     *            An AccountStateKeeper containing account attributes to be
     *            updated.
     * @return 0 - if the update is successful<br>
     *         -2 - if the update fails
     */
    public int updateAccount(AccountBean ask) {

        int output;
        AccountBean accBean = null;

        try {

            accBean = em.find(AccountBean.class, ask.getAccId());
            output = accBean.updateAccount(ask);
            em.merge(accBean);
        } catch (Exception e) {
            Rlog.debug("account", "Error in updateAccount in AccountAgentBean"
                    + e);
            return -2;
        }
        return output;
    }

    public String getCreationDate(int accId) {
        String creation_date = "";
        try {
            Rlog.debug("account",
                    "In getCreationDate in AccountAgentBean line number 0");
            AccountDao accountDao = new AccountDao();
            Rlog.debug("account",
                    "In getCreationDate in AccountAgentBean line number 1");
            creation_date = accountDao.getCreationDate(accId);
            Rlog.debug("account",
                    "In getCreationDate in AccountAgentBean line number 2");
        } catch (Exception e) {
            Rlog.fatal("account",
                    "Error in getCreationDate(int accId) in AccountAgentBean "
                            + e);
        }
        return creation_date;
    }

    public int copyAccount(int accId) {
        int ret = 0;
        try {
            Rlog.debug("account",
                    "In copyAccount in AccountAgentBean line number 0");
            AccountDao accountDao = new AccountDao();
            Rlog.debug("account",
                    "In copyAccount in AccountAgentBean line number 1 ret "
                            + ret);
            ret = accountDao.copyAccount(accId);
            Rlog.debug("account",
                    "In copyAccount in AccountAgentBean line number 2 ret "
                            + ret);
        } catch (Exception e) {
            Rlog.fatal("account",
                    "Error in copyAccount(int accId) in AccountAgentBean " + e);
        }
        return ret;
    }

    public AccountDao getAccounts() {
        Rlog.debug("account", "In getAccounts in AccountAgentBean line AAA");
        AccountDao accountDao = new AccountDao();
        try {
            Rlog
                    .debug("account",
                            "In getAccounts in AccountAgentBean line BBB");

            Rlog.debug("account",
                    "In getAccounts in AccountAgentBean line number 1 ret ");
            accountDao.getAccounts();
            Rlog
                    .debug("account",
                            "In getAccounts in AccountAgentBean line CCC");
            Rlog.debug("account",
                    "In getAccounts in AccountAgentBean line number 2 ret ");
            Rlog
                    .debug("account",
                            "In getAccounts in AccountAgentBean line DDD");
        } catch (Exception e) {
            Rlog.fatal("account", "Error in getAccounts() in AccountAgentBean "
                    + e);
        }
        return accountDao;
    }
    public AccountDao getLoginModuleDetails(int accId) {
        
        AccountDao accountDao = new AccountDao();
        try {
        
            accountDao.getLoginModuleDetails(accId);
            
        } catch (Exception e) {
            Rlog.fatal("account", "Error in getgetLoginModuleDetails() in AccountAgentBean "
                    + e);
            e.printStackTrace();
        }
        return accountDao;
    }
public AccountDao getLoginModule(int loginModuleId) {
        
        AccountDao accountDao = new AccountDao();
        try {
        
            accountDao.getLoginModule(loginModuleId);
            
        } catch (Exception e) {
            Rlog.fatal("account", "Error in getLoginModule() in AccountAgentBean "
                    + e);
            e.printStackTrace();
        }
        return accountDao;
    }


//JM:
public int getPortalsCount(int accId) {
    int ret = 0;
    try {
        Rlog.debug("account",
                "In getPortalsCount in AccountAgentBean line number 0");        
        PortalDao portalDao = new PortalDao();
        Rlog.debug("account",
                "In getPortalsCount in AccountAgentBean line number 1 ret "
                        + ret);
        ret = portalDao.getPortalsCount(accId);
        Rlog.debug("account",
                "In getPortalsCount in AccountAgentBean line number 2 ret "
                        + ret);
    } catch (Exception e) {
        Rlog.fatal("account",
                "Error in getPortalsCount(int accId) in AccountAgentBean " + e);
    }
    return ret;
}



}// end of class

