package com.velos.eres.service.submissionAgent;

import java.util.ArrayList;
import java.util.Calendar;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.velos.eres.business.common.EIRBDao;
import com.velos.eres.business.submission.impl.SubmissionBoardBean;
import com.velos.eres.service.util.Rlog;
@Stateless
@Remote( { SubmissionBoardAgent.class } )
public class SubmissionBoardAgentBean implements SubmissionBoardAgent {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    public Integer createSubmissionBoard(SubmissionBoardBean submissionBoardBean) {
        Integer output = 0;
        SubmissionBoardBean newBean = new SubmissionBoardBean();
        try {
            newBean.setDetails(submissionBoardBean);
            em.persist(newBean);
            output = newBean.getId();
        } catch(Exception e) {
            Rlog.fatal("submission", "Exception in SubmissionAgentBean.createSubmissionBoard "+e);
            output = -1;
        }
        return output;
    }
    
    public SubmissionBoardBean findByPkSubmissionBoard(Integer pkSubmissionBoard) {
        Query query = em.createNamedQuery("findByPkSubmissionBoard");
        query.setParameter("pkSubmissionBoard", pkSubmissionBoard.intValue());
        ArrayList list = (ArrayList) query.getResultList();
        if (list == null || list.size() == 0) { return null; }
        return (SubmissionBoardBean) list.get(0);
    }

    public SubmissionBoardBean findByFkSubmissionAndBoard(Integer pkSubmission, Integer fkReviewBoard) {
        Query query = em.createNamedQuery("findByFkSubmissionAndBoard");
        query.setParameter("fkSubmission", pkSubmission);
        query.setParameter("fkReviewBoard", fkReviewBoard);
        ArrayList list = (ArrayList) query.getResultList();
        if (list == null || list.size() == 0) { return null; }        
        return (SubmissionBoardBean)list.get(0);
    }
    
    public Integer getExistingSubmissionBoard(SubmissionBoardBean submissionBoardBean) {
        Query query = em.createNamedQuery("findByFkSubmissionAndBoard");
        query.setParameter("fkSubmission", submissionBoardBean.getFkSubmission());
        query.setParameter("fkReviewBoard", submissionBoardBean.getFkReviewBoard());
        ArrayList list = (ArrayList) query.getResultList();
        if (list == null || list.size() == 0) { return 0; }
        return ((SubmissionBoardBean)list.get(0)).getId();
    }
    
    public Integer updateSubmissionBoard(SubmissionBoardBean submissionBoardBean, boolean applyNull) {
        Integer output = 0;
        Query query = em.createNamedQuery("findByFkSubmissionAndBoard");
        query.setParameter("fkSubmission", submissionBoardBean.getFkSubmission());
        query.setParameter("fkReviewBoard", submissionBoardBean.getFkReviewBoard());
        ArrayList list = (ArrayList) query.getResultList();
        if (list == null || list.size() == 0) { return output; }
        try {
            SubmissionBoardBean bean = (SubmissionBoardBean) list.get(0);
            if (applyNull) {
                bean.setFkReviewMeeting(submissionBoardBean.getFkReviewMeeting());
                bean.setSubmissionReviewType(submissionBoardBean.getSubmissionReviewType());
                bean.setSubmissionReviewer(submissionBoardBean.getSubmissionReviewer());
            } else {
                if (submissionBoardBean.getFkReviewMeeting() != null) {
                    bean.setFkReviewMeeting(submissionBoardBean.getFkReviewMeeting());
                }
                if (submissionBoardBean.getSubmissionReviewer() != null &&
                        submissionBoardBean.getSubmissionReviewType() > 0) {
                    bean.setSubmissionReviewType(submissionBoardBean.getSubmissionReviewType());
                }
                if (submissionBoardBean.getSubmissionReviewer() != null) {
                    bean.setSubmissionReviewer(submissionBoardBean.getSubmissionReviewer());
                }
            }
            bean.setLastModifiedBy(submissionBoardBean.getLastModifiedBy());
            bean.setLastModifiedDate(Calendar.getInstance().getTime());
            em.merge(bean);
            output = bean.getId();
        } catch (Exception e) {
            Rlog.fatal("submission", "Exception in SubmissionAgentBean.updateSubmissionByFkStudy "+e);
            output = -1;
        }
        return output;
    }

    /** Returns Minutes of Meeting for a review board and meeting date*/
    public  String getreviewMeetingMOM(int fkBoard, int fkMeetingDatePK)
    {
    	String mom = "";
    	
    	try{
    		mom = EIRBDao.getreviewMeetingMOM(fkBoard, fkMeetingDatePK);
    	}
    	 catch (Exception e) {
             Rlog.fatal("submission", "Exception in SubmissionAgentBean.getreviewMeetingMOM "+e);
              
         }
    	return mom;
    }
    
    /** Updates Minutes of Meeting for a review board and meeting date*/
    public int updateReviewMeetingMOM( int fkMeetingDatePK,String mom) 
    {
    	int ret = 0;
    	
    	try{
    		ret= EIRBDao.updateReviewMeetingMOM(fkMeetingDatePK,mom);
    	}
    	 catch (Exception e) {
             Rlog.fatal("submission", "Exception in SubmissionAgentBean.updateReviewMeetingMOM "+e);
             ret = -1;
              
         }
    	return ret;
    }

    /** returns the PK of the default review board for an account ONLY if the logged in uder's default group (passed as a parameter)
     * has access to the review board
     * 
     * returns 0 if no default group is configured or the group does not have access to it
     * 
     * */
    
    public int getDefaultReviewBoardWithCheckGroupAccess(int accountId,int grpId) 
    {
    	int boardPK = 0;
    	
    	try{
    		boardPK= EIRBDao.getDefaultReviewBoardWithCheckGroupAccess( accountId, grpId);
    	}
    	 catch (Exception e) {
             Rlog.fatal("submission", "Exception in SubmissionAgentBean.getDefaultReviewBoardWithCheckGroupAccess(int accountId,int grpId)"+e);
             boardPK = -1;
              
         }
    	return boardPK;
    }
    public int getDefaultReviewBoardWithCheckGroupAccess(int accountId,int grpId,int submissionBoardPK) 
    {
    	int boardPK = 0;
    	
    	try{
    		boardPK= EIRBDao.getDefaultReviewBoardWithCheckGroupAccess( accountId, grpId, submissionBoardPK);
    	}
    	 catch (Exception e) {
             Rlog.fatal("submission", "Exception in SubmissionAgentBean.getDefaultReviewBoardWithCheckGroupAccess(int accountId,int grpId)"+e);
             boardPK = -1;
              
         }
    	return boardPK;
    }
    public int getReviewBoardIfExistsForStudy(int submissionId , int reviewBoard) 
    {    
    	int boardPK = 0;
    	 try {
    		Query query = em.createNamedQuery("findPkSubmissionBoardBySubmissionAndRevBoard");
    		query.setParameter("fkRevBoard", reviewBoard);
    		query.setParameter("submissionId", submissionId);
            ArrayList list = (ArrayList) query.getResultList();
            if (list != null && list.size() != 0) { 
            	boardPK = ((SubmissionBoardBean)list.get(0)).getId();
            }
    	 } catch (Exception e) {
             Rlog.fatal("submission", "Exception in SubmissionBoardAgentBean.getReviewBoardIfExistsForStudy"+e);
             boardPK = -1;
              
         }
    	return boardPK;
    }
     
}