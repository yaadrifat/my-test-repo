package com.velos.eres.service.networkAgent;

import javax.ejb.Remote;

import com.velos.eres.business.network.impl.NetworkBean;


@Remote
public interface NetworkAgentRObj {
	
	public int setNetworkDetails(NetworkBean nwb);
	public int updateNetwork(NetworkBean nwb);
	public NetworkBean getNetworkDetails(int networkId);
}
