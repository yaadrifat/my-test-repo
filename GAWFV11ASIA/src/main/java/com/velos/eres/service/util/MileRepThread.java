/*
 * Classname : MileRepThread
 * 
 * Version information: 1.0
 *
 * Date 09/05/2002
 * 
 * Copyright notice: Velos Inc.
 */

package com.velos.eres.service.util;

import com.velos.eres.business.common.MileRepGen;

public class MileRepThread extends Thread {
    int userId;

    String ipAdd;

    int acc;

    int repId;

    String repName;

    String genRepName;

    String dtFrom;

    String dtTo;

    String studyPk;

    String patientId;

    String uName;

    String repArgsDisplay;

    String orgId;

    public MileRepThread(int userId, String ipAdd, int acc, int repId,
            String repName, String genRepName, String dtFrom, String dtTo,
            String studyPk, String patientId, String uName,
            String repArgsDisplay, String orgId) {
        Rlog.debug("common", "MileRepThread constructor");
        this.userId = userId;
        this.ipAdd = ipAdd;
        this.acc = acc;
        this.repId = repId;
        this.repName = repName;
        this.genRepName = genRepName;
        this.dtFrom = dtFrom;
        this.dtTo = dtTo;
        this.studyPk = studyPk;
        this.patientId = patientId;
        this.uName = uName;
        this.repArgsDisplay = repArgsDisplay;
        this.orgId = orgId;
    }

    public void run() {
        MileRepGen rg = new MileRepGen();
        rg.generateReport(userId, ipAdd, acc, repId, repName, genRepName,
                dtFrom, dtTo, studyPk, patientId, uName, repArgsDisplay, orgId);
        Rlog.debug("common", "MileRepThread.run line 3");
    }

}