package com.velos.eres.service.util;

import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;

import com.velos.eres.business.common.LinkedFormsDao;
import com.velos.eres.web.study.StudyJB;

public final class WorkflowUtil {
	public static int getEntityId(String wfPageId, HttpSession tSession){
		int entityId = 0;

		if (wfPageId.equals(FlxPageUtil.getFlexStudyPageName())) {
			StudyJB studyJB = null;
			try {
				studyJB = (StudyJB)(tSession.getAttribute("studyJB"));
			} catch (Exception e){
				return 0;
			}
			if (null != studyJB){
				entityId = studyJB.getId();
			} else return 0;
		}
		return entityId;
	}

	public static String getWorkflowTypes(String wfPageId){
		String workflowTypes = null;

		if (wfPageId.equals(FlxPageUtil.getFlexStudyPageName())) {
			final String DUMMY_StudyWorkflowTypes ="[Workflow_FlexStudy_WfTypes]";
			if (!DUMMY_StudyWorkflowTypes.equals(CFG.Workflow_FlexStudy_WfTypes)){
				workflowTypes = CFG.Workflow_FlexStudy_WfTypes;
			}
		}
		return workflowTypes;
	}

	public static String getWorkflowFormType(String wfPageId){
		String workflowFormType = null;

		if (wfPageId.equals(FlxPageUtil.getFlexStudyPageName())) {
			workflowFormType = "SA";
		}
		return workflowFormType;
	}

	public static String getWorkflowFormName(String wfPageId){
		String workflowFormName = null;

		if (wfPageId.equals(FlxPageUtil.getFlexStudyPageName())) {
			workflowFormName = "document.studyScreenForm";
		}
		return workflowFormName;
	}
	
    public static String getOnclickJS(Object...args) {
    	String onclickJS ="";
    	if (null == args || null == args[0]) { return onclickJS; }
    	HttpSession session = (HttpSession) args[0];
    	if (null == session) return onclickJS;

    	String wfPageId = (String) args[1];
    	int taskId = StringUtil.stringToNum(""+args[2]);
    	String onclickJSONStr = (String)args[3];

    	if (StringUtil.isEmpty(wfPageId) || taskId <= 0 || null == session) return onclickJS;
    	
    	int accountId = StringUtil.stringToNum((String)session.getAttribute("accountId"));
    	if (accountId <= 0) return onclickJS;

    	if (StringUtil.isEmpty(onclickJSONStr)){
    		return onclickJS;
    	}

    	try {
			JSONObject onclickJSON = new JSONObject(onclickJSONStr);
			String moduleId = onclickJSON.getString("moduleId");
	    	if (StringUtil.isEmpty(moduleId)){
	    		return onclickJS;
	    	}

	    	if ("ERES_FORM".equals(moduleId)){
	    		/*
	    		 * Config JSON example- {"moduleId":"ERES_FORM", "moduleDet":{"formId":123, }}
	    		 * */
	    		JSONObject moduleDet = onclickJSON.getJSONObject("moduleDet");
	    		if (null == moduleDet) return onclickJS;

	    		int formId = moduleDet.getInt("formId");
	    		if (formId <= 0) return onclickJS;

	    		LinkedFormsDao lnkFormDao = new LinkedFormsDao();
	    		lnkFormDao.getLinkedFormByFormId(accountId, formId, "SA");
	    		String entryChar = (null == lnkFormDao.getFormEntryChar() || lnkFormDao.getFormEntryChar().isEmpty() )? "M" : (String)(lnkFormDao.getFormEntryChar()).get(0);
	    		entryChar = (StringUtil.isEmpty(entryChar))? "M" : entryChar;

	    		String formType = getWorkflowFormType(wfPageId);
	    		String htmlFormName = getWorkflowFormName(wfPageId);
	    		if (StringUtil.isEmpty(formType) || StringUtil.isEmpty(htmlFormName)) { return onclickJS; }

		    	onclickJS = "$j('#taskImg"+taskId+"').click(function(){"
		    		+ "	return openlinkedForm('"+formId+"','"+ entryChar +"','"+ formType +"',"+ htmlFormName +",'workflow','');"
		    		+ "});";
		    	
		    	onclickJS += "$j('#taskImg"+taskId+"').hover(function(){"
		    		+ "$j(this).css('cursor','hand');"
		    		+ "});";
	    	}
		} catch (JSONException e) {
			e.printStackTrace();
		}

    	return onclickJS;
    }
}
