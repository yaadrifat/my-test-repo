package com.velos.eres.service.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;

public class Bundles {

	public static void main(String[] args) {
	}

	public static String contextPath;

	public static void generateLableBundleJS(String contextPathArg) {

		contextPath = contextPathArg;

		// writeFile(contextPath + "/jsp/js/bundles/labelBundle.js");
		// writeFile(contextPath + "/jsp/js/bundles/messageBundle.js");

		// Java Should normalize path // otherwise use int envType =
		int osType = EnvUtil.getSystemType();
		File lcFile = null;
		File mcFile = null;
		if (osType == 0) {
			lcFile = new File(contextPathArg + "\\jsp\\js\\bundles\\",
					"labelBundle.js");

			mcFile = new File(contextPathArg + "\\jsp\\js\\bundles\\",
					"messageBundle.js");
		} else {
			lcFile = new File(contextPathArg + "/jsp/js/bundles/",
					"labelBundle.js");

			mcFile = new File(contextPathArg + "/jsp/js/bundles/",
					"messageBundle.js");
		}
		
		try {
			if (lcFile.createNewFile()) {
				writeFile(lcFile.getAbsolutePath());
			} else {
				PrintWriter pw = new PrintWriter(lcFile);
				pw.close();

				lcFile.createNewFile();
				writeFile(lcFile.getAbsolutePath());
			}

			if (mcFile.createNewFile()) {
				writeFile(mcFile.getAbsolutePath());
			} else {
				PrintWriter pw = new PrintWriter(mcFile);
				pw.close();

				mcFile.createNewFile();
				writeFile(mcFile.getAbsolutePath());
			}

		} catch (IOException e) { // TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void writeFile(String filename) {

		String varName;
		String bundleName;
		if (filename.contains("messageBundle.js")) {
			varName = "MC";
			bundleName = VelosResourceBundle.MESSAGE_BUNDLE;
		} else {
			bundleName = VelosResourceBundle.LABEL_BUNDLE;
			varName = "LC";
		}

		BufferedWriter out = null;
		try {
			ResourceBundle rbLables = VelosResourceBundle.getBundle(bundleName);
			Set<String> keySet = rbLables.keySet();
			out = new java.io.BufferedWriter(new java.io.FileWriter(filename,
					false));

			out.write(varName + " = {");
			out.newLine();

			String value = "";
			for (String s : keySet) {
				value = (rbLables.getString(s).replaceAll("\"", "'"));
				value += "\",";
				out.write(s + " : \"" + value);
				out.newLine();
			}
			out.write("}");

		} catch (Exception e) // catch and report any errors
		{
			e.printStackTrace();
		} finally {
			try {
				out.close();
				out = null;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	public static void reload() {

	}
}
