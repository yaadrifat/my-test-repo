/*
 * Classname			PortalDesignAgentRObj
 * 
 * Version information : 1.0  
 *
 * Date					06/25/2003
 * 
 * Copyright notice : Velos Inc
 */

package com.velos.eres.service.portalDesignAgent;

import java.util.Hashtable;
import javax.ejb.Remote;

import com.velos.eres.business.common.PortalDesignDao;
import com.velos.eres.business.portalDesign.impl.PortalDesignBean;


/**
 * Remote interface for PortalDesignAgentBean session EJB
 * 
 * @author Manimaran
 * @version 1.0, 04/04/2007
 */
@Remote
public interface PortalDesignAgentRObj {
    public PortalDesignBean getPortalDesignDetails(int portalModId);

    public int setPortalDesignDetails(PortalDesignBean portalDesign);    
    
    public int updatePortalDesign(PortalDesignBean portalDesign);   
    
    public PortalDesignDao getPortalModValues(int portalId);
    
    public int deletePortalModules(int modId); 
    
    // Overloaded for INF-18183 ::: AGodara
    public int deletePortalModules(int modId,Hashtable<String, String> auditInfo); 
    
    public int deletePortalDesignData(int portalID);
    
 // Overloaded for INF-18183 ::: Akshi
    public int deletePortalDesignData(int portalID,Hashtable<String, String> args);
    
    
}
