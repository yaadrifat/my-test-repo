package com.velos.eres.service.userSessionAgent.impl;

import java.util.UUID;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;

import com.velos.eres.business.userSession.impl.UserSessionTokenDao;
import com.velos.eres.service.userSessionAgent.UserSessionTokenAgentRObj;

@Stateless
@Remote( { UserSessionTokenAgentRObj.class } )
public class UserSessionTokenAgentBean implements UserSessionTokenAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
    private static Logger logger = Logger.getLogger(UserSessionTokenAgentBean.class);
	
	private String generateTokenInternal() {
		try {
			return UUID.randomUUID().toString();
		} catch(Exception e) {
			logger.fatal("Error in UserSessionTokenAgentBean.generateTokenInternal:"+e);
			return null;
		}
	}

	public String createTokenBeforeLogin(String ipAdd) {
		String token = generateTokenInternal();
		if (token == null) { return null; }
		UserSessionTokenDao userSessionTokenDao = new UserSessionTokenDao();
		if (userSessionTokenDao.insertUserSessionToken(token, ipAdd) < 1) { return null; }
		return token;
	}

	public String createTokenForUser(Integer fkUser) {
		if (fkUser == null) { return null; }
		UserSessionTokenDao userSessionTokenDao = new UserSessionTokenDao();
		userSessionTokenDao.findUserSessionbyUserId(fkUser);
		int fkUserSession = 0;
		if (userSessionTokenDao.getUserSessionIdList() != null 
				&& userSessionTokenDao.getUserSessionIdList().size() > 0) {
			fkUserSession = userSessionTokenDao.getUserSessionIdList().get(0);
		}
		int userSessTime = 0;
		if (userSessionTokenDao.getUserSessTimeList() != null
				&& userSessionTokenDao.getUserSessTimeList().size() > 0) {
			userSessTime = userSessionTokenDao.getUserSessTimeList().get(0);
		}
		if (fkUserSession < 1 || userSessTime < 1) {
			logger.fatal("Error in UserSessionTokenAgentBean.createTokenForUser: could not find user session");
			return null;
		}
		String token = generateTokenInternal();
		if (token == null) { return null; }
		int resultIndex = userSessionTokenDao.insertUserSessionTokenByUser(
				fkUserSession, 
				fkUser,
				userSessTime, 
				token);
		if (resultIndex < 1) { return null; }
		return token;
	}
	
	public String challengeTokenBeforeLogin(String token, String ipAdd) {
		if (token == null) { return null; }
		
		// Try to invalidate the token; if no row updated => token is invalid
		UserSessionTokenDao userSessionTokenDao = new UserSessionTokenDao();
		if (userSessionTokenDao.invalidateUserSessionToken(token, ipAdd) < 1) {
			logger.debug("challengeTokenBeforeLogin: Token is invalid");
			return null;
		}
		
		// Issue a new token for before login
		return createTokenBeforeLogin(ipAdd);
	}

	public String challengeTokenForUser(String token, Integer fkUser) {
		if (token == null || fkUser == null) { return null; }
		
		// Try to validate the token for this user; if no row selected => token is invalid
		UserSessionTokenDao userSessionTokenDao = new UserSessionTokenDao();
		if (userSessionTokenDao.validateUserSessionTokenByUser(token, fkUser) < 1) {
			logger.debug("challengeTokenForUser: Token is invalid");
			return null;
		}
		
		// Issue a new token for this user
		return createTokenForUser(fkUser);
	}
	
	public Integer invalidateAllTokensForUser(Integer fkUser) {
		if (fkUser == null) { return 0; }
		UserSessionTokenDao userSessionTokenDao = new UserSessionTokenDao();
		return userSessionTokenDao.invalidateAllUserSessionTokensByUser(fkUser);
	}
}
