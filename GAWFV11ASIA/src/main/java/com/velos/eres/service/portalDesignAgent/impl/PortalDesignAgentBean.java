/*
 * Classname			PortalDesignAgentBean
 * 
 * Version information : 
 *
 * Date					04/20/2007
 * 
 * Copyright notice : Velos Inc
 */

package com.velos.eres.service.portalDesignAgent.impl;

/* IMPORT STATEMENTS */

import java.util.ArrayList;
import java.util.Hashtable;
import javax.annotation.Resource;
import javax.ejb.SessionContext;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;



import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.common.PortalDesignDao;
import com.velos.eres.business.portalDesign.impl.PortalDesignBean;
import com.velos.eres.service.portalDesignAgent.PortalDesignAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.business.audit.impl.AuditBean;

/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the entity CMP.
 * 
 * @author Manimaran
 * @version 1.0, 04/20/2007
 * @ejbHome PortalDesignAgentHome
 * @ejbRemote PortalDesignAgentRObj
 */

@Stateless
public class PortalDesignAgentBean implements PortalDesignAgentRObj {

    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
    @Resource private SessionContext context;

    public PortalDesignBean getPortalDesignDetails(int portalModId) {
        PortalDesignBean ap = null;
        try {

            ap = (PortalDesignBean) em.find(PortalDesignBean.class, new Integer(portalModId));

        } catch (Exception e) {
            Rlog.fatal("portalDesign",
                    "Error in getPortalDesignDetails() in PortalDesignAgentBean" + e);
        }

        return ap;
    }
    
    public int setPortalDesignDetails(PortalDesignBean admin) {
        try {
        	PortalDesignBean ad = new PortalDesignBean();
        	ad.updatePortalDesign(admin);
            em.persist(ad);
            return ad.getPortalModId();
        } catch (Exception e) {
            Rlog.fatal("portalDesign",
                    "Error in setPortalDesignDetails() in PortalDesignAgentBean " + e);
            e.printStackTrace(); 
            
        }
        return 0; 
    }
    
    public int updatePortalDesign(PortalDesignBean ap) {
    	PortalDesignBean adm = null;
        int output;
        try {
            adm = (PortalDesignBean) em.find(PortalDesignBean.class, new Integer(
                    ap.getPortalModId()));
            if (adm == null) {
                return -2;
            }
            output = adm.updatePortalDesign(ap);
            em.merge(adm);
        } catch (Exception e) {
            Rlog.debug("portalDesign", "Error in updatePortalDesign in PortalDesignAgentBean"
                    + e);
            return -2;
        }
        return output;
    }   
    
    public PortalDesignDao getPortalModValues(int portalId) {

        try {
        
            Rlog.debug("portalDesign",
                    "PortaDesignlAgentBean.getPortalModValue starting");
            PortalDesignDao pdDao = new PortalDesignDao();        
            pdDao.getPortalModValues(portalId);            
            return pdDao;
        } catch (Exception e) {
            Rlog.fatal("portalDesign",
                    "Exception In getPortalModValues in PortalDesignAgentBean " + e);
        }
        return null;
    }
    
//  JM: 17Aug2007:  issue #3061
    public int deletePortalModules(int modId){
    	
    	PortalDesignDao pdDao = new PortalDesignDao();
        int success = 0;
        try {
            Rlog.debug("PortalDesign", "PortalDesignAgentBean:deletePortalModules ...");
            success = pdDao.deletePortalModules(modId);            

        } catch (Exception e) {
            Rlog.fatal("PortalDesign", "PortalDesignAgentBean:deletePortalModules-Exception In deletePortalModules "
                    + e);
            return -1;
        }        
        return success;

    }
    
    // Overloaded for INF-18183 ::: AGodara   
    public int deletePortalModules(int modId,Hashtable<String, String> auditInfo){
    	
    	PortalDesignDao pdDao = new PortalDesignDao();
        int success = 0;
        try {
        	AuditBean audit=null;
            String currdate =DateUtil.getCurrentDate();
        	String userID=(String)auditInfo.get(AuditUtils.USER_ID_KEY); //Fetches the User ID from the Hashtable
            String ipAdd=(String)auditInfo.get(AuditUtils.IP_ADD_KEY); //Fetches the IP Address from the Hashtable
            String reason=(String)auditInfo.get(AuditUtils.REASON_FOR_DEL); //Fetches the IP Address from the Hashtable
            String app_module=(String)auditInfo.get(AuditUtils.APP_MODULE); //Fetches the IP Address from the Hashtable
            String rid= AuditUtils.getRidValue("ER_PORTAL_MODULES","eres","PK_PORTAL_MODULES="+modId); //Fetches the RID
            //POPULATE THE AUDIT BEAN 
            audit = new AuditBean("ER_PORTAL_MODULES",String.valueOf(modId),rid,userID,currdate,app_module,ipAdd,reason);
			em.persist(audit); // PERSIST THE DATA IN AUDIT TABLE
            
            success = pdDao.deletePortalModules(modId);
           
            if (success == -1) {
        	   context.setRollbackOnly();
			}
            
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("PortalDesign", "PortalDesignAgentBean:deletePortalModules-Exception In deletePortalModules "
                    + e);
            return -1;
        }        
        return success;

    }
       
    //JM: 20Aug2007: issue #3072
    public int deletePortalDesignData(int portalID){
    
    	int success = 0;
        try {
        
        	PortalDesignDao pdDao = new PortalDesignDao();
            success=pdDao.deletePortalDesignData(portalID);            
           
        } catch (Exception e) {
            Rlog.fatal("portalDesign",
                    "Exception In deletePortalDesignData in PortalAgentBean " + e);
            return -1;
        }
        return success;
    }
    
 // Overloaded for INF-18183 ::: Akshi
    public int deletePortalDesignData(int portalID,Hashtable<String, String> args){
        
    	int success = 0;
        try {
        	AuditBean audit=null;
            String currdate =DateUtil.getCurrentDate();
            String userID=(String)args.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
            String ipAdd=(String)args.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
            String reason=(String)args.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
            String appmodule=(String)args.get(AuditUtils.APP_MODULE); /*Fetches the application module name from the Hashtable*/
            String getRidValue= AuditUtils.getRidValue("ER_PORTAL_MODULES","eres","PK_PORTAL_MODULES="+portalID);/*Fetches the RID/PK_VALUE*/ 
            audit = new AuditBean("ER_PORTAL_MODULES",String.valueOf(portalID),getRidValue,
        			userID,currdate,appmodule,ipAdd,reason);/*POPULATE THE AUDIT BEAN*/ 
            em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
        	PortalDesignDao pdDao = new PortalDesignDao();
            success=pdDao.deletePortalDesignData(portalID);   
            if(success != 0){/*if Dao returns result without exception*/
            	context.setRollbackOnly();
            }
           
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("portalDesign",
                    "Exception In deletePortalDesignData in PortalAgentBean " + e);
            return -1;
        }
        return success;
    }        
     
}
   