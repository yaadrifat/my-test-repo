package com.velos.eres.service.util;

public class LkpViewObject {
    private Integer pkLkpView = null;
    private String lkpViewKey = null;
    private String lkpViewName = null;
    private String lkpType = null;
    private String lkpViewFilterSQL = null;
    private String lkpViewIgnoreFilter = null;

    
    public LkpViewObject(Integer pkLkpView, String lkpViewKey, String lkpViewName,  String lkpType,
    		String lkpViewFilterSQL, String lkpViewIgnoreFilter) {
    	this.pkLkpView = pkLkpView;
        this.lkpViewKey = lkpViewKey;
        this.lkpViewName = lkpViewName;
        this.lkpType = lkpType;
        this.lkpViewFilterSQL = lkpViewFilterSQL;
        this.lkpViewIgnoreFilter = lkpViewIgnoreFilter;
    }
    
    public void setLkpViewObject(LkpViewObject lkpViewObject) {
    	this.pkLkpView = lkpViewObject.pkLkpView;
        this.lkpViewKey = lkpViewObject.lkpViewKey;
        this.lkpViewName = lkpViewObject.lkpViewName;
        this.lkpType = lkpViewObject.lkpType;
        this.lkpViewFilterSQL = lkpViewObject.lkpViewFilterSQL;
        this.lkpViewIgnoreFilter = lkpViewObject.lkpViewIgnoreFilter;
    }
    
    public Integer getLkpViewPk() {
        return pkLkpView;
    }
    
    public String getLkpViewKey() {
        return lkpViewKey;
    }

    public String getLkpViewName() {
        return lkpViewName;
    }
    
    public String getLkpViewType() {
        return lkpType;
    }

    public String getLkpViewFilterSQL() {
        return lkpViewFilterSQL;
    }

    public String getLkpViewIgnoreFilter() {
        return lkpViewIgnoreFilter;
    }

}
