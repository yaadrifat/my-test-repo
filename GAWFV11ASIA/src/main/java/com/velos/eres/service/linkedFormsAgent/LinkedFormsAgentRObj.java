/*
 * Classname			LinkedFormAgentRObj
 * 
 * Version information : 1.0  
 *
 * Date					08/08/2003
 * 
 * Copyright notice : Velos Inc
 */
 
package com.velos.eres.service.linkedFormsAgent;

import java.util.Hashtable;

import javax.ejb.Remote;

import com.velos.eres.business.common.LinkedFormsDao;
import com.velos.eres.business.common.SaveFormDao;
import com.velos.eres.business.linkedForms.impl.LinkedFormsBean;

/**
 * Remote interface for LinkedFormAgentBean session EJB
 * 
 * @author Anu Khanna
 * @version 1.0, 07/11/2003
 */

@Remote
public interface LinkedFormsAgentRObj {
    public LinkedFormsBean getLinkedFormDetails(int linkedFormId);

    public int setLinkedFormDetails(LinkedFormsBean linkedFormlib);

    public int updateLinkedForm(LinkedFormsBean lfsk);
//siteIdAcc Parameter Added by Gopu to fix the bugzilla Issue #2406
    public LinkedFormsDao getFormListForLinkToStudy(int accountId, int userId,
            String formName, int formType, int studyId, String siteIdAcc);

    public LinkedFormsDao getStudyLinkedForms(int studyId);

    public int linkFormsToStudyAccount(LinkedFormsDao linkedFormsDao);

    public LinkedFormsDao getPatientForms(int accountId, int patient,
            int userId, int siteId);

    public LinkedFormsDao getPatientStudyForms(int accountId, int patId,
            int studyId, int userId, int siteId);

    public LinkedFormsDao getPatientStudyForms(int accountId, int patId,
            int studyId, int userId, int siteId, boolean includeHidden);

    public LinkedFormsDao getAccountForms(int accountId, int userId, int siteId);
    
    public LinkedFormsDao getAccountGenericForms(int accountId, int userId, int siteId,String displayType);
    
    public LinkedFormsDao getAccountForms(String formname,int account);
    public LinkedFormsDao getOrganisationForms(int accountId, int userId, int siteId);
    
    public LinkedFormsDao getNetworkForms(int accountId, int userId, int siteId);
    
    public LinkedFormsDao getUserForms(int accountId, int userId, int siteId);
    
    public LinkedFormsDao getAccountFormsForSpecimen(int accountId, int userId, int siteId);

    public LinkedFormsDao getStudyForms(int accountId, int studyId, int userId,
            int siteId);

    public LinkedFormsDao getStudyForms(int accountId, int studyId, int userId,
            int siteId, int grpRight, int stdRight, 
            boolean isIrb, String submissionType, String formSubtype);
    
    public LinkedFormsDao getStudyForms(int accountId, int studyId, int userId,
            int siteId, int grpRight, int stdRight, 
            boolean isIrb, String submissionType, String formSubtype,String formflag);
    

    public LinkedFormsDao getFormsLinkedToAccount(int accountId,
            String formName, int studyId, String linkedTo, int siteId,
            int groupId);

    public int copyFormsForStudy(String[] idArray, String formType,
            String studyId, String formName, String creator, String ipAdd);

    public int copyFormsForAccount(String[] idArray, String formType,
            String formName, String creator, String ipAdd);

    public LinkedFormsDao getOrgNamesIds(int pkLf, int fkFormlib);

    public int updateOrgIds(int pkLf, String fkForm, String orgIds, String oldOrgIds, String userId, String ipAdd);

    public String getFormHtmlforPreview(int formId);
    public String getFormHtmlforPreviewNoRole(int formId);
    
    public String getFormHtmlforPreview(int formId,Hashtable moreParams);

    public String getFilledFormHtml(int pkfilledform, String formdisplaytype);
    
    public SaveFormDao getFilledFormHtml(int pkfilledform, String formdisplaytype,String flag);
    public SaveFormDao getFilledFormHtmlNoRole(int pkfilledform, String formdisplaytype,String flag);
    
    public SaveFormDao getFilledFormHtml(int pkfilledform, String formdisplaytype,String flag,Hashtable moreParams);

    public int insertCrfForms(String[] formIds, String accId,
            String[] crfNames, String[] crfNumbers, String calId,
            String eventId, String user, String ipAdd);

    public LinkedFormsDao getGrpNamesIds(int pkLf, int fkFormlib);

    public int updateGrpIds(int pkLf, String fkForm, String grpIds, String oldGrpIds, String userId, String ipAdd);

    public String getPrintFormHtml(int formId, int filledFormId, String linkFrom);

    public LinkedFormsBean findByCrfEventId(int crfId, int eventId);

    public LinkedFormsBean findByFormId(int formId);

    public int updateHideSeqOfLF(String[] pkLF, String[] hide, String[] seq,
            String[] patProfile, String ipAdd, int user , String[] dispInSpec);

    public LinkedFormsDao getActiveFormsLinkedToAccount(int accountId,
            String linkedTo);

    public LinkedFormsDao getStudyLinkedActiveForms(int studyId);

    public LinkedFormsDao getStudyLinkedActiveForms(int studyId, int accountId,
            int userId);

    public int deleteFilledFormResp(String filledFormId,
            String formDispLocation, String lastModifiedBy, String ipAdd);
    // Overloaded for INF-18183 ::: AGodara
    public int deleteFilledFormResp(String filledFormId,
            String formDispLocation, Hashtable<String, String> auditInfo);
    //added by sonia Abrol, to check if user has right a form
    
    /** checks if user has access to the form (evenif the form is hidden)
     * 
     * @param accountId
     *            The form Id
     * @param userId
     *            The user for which access rights are checked
     * 
     */

    public boolean hasFormAccess(int formId, int userId );
    
    //Added by Gopu for May-June Enhancement(#F1)  for Calendar to Form Linking
    
    public LinkedFormsDao getStudyActiveLockdownForms(int studyId, int accountId);
    
    public LinkedFormsDao getStudyFormsForSpecimen(int accountId, int studyId, int userId,
            int siteId, int grpRight, int stdRight);

    public LinkedFormsDao getPatientFormsForSpecimen(int accountId, int patient,
            int userId, int siteId);
    
    public LinkedFormsDao getPatientStudyFormsForSpecimen(int accountId, int patId,
            int studyId, int userId, int siteId);
    
    public LinkedFormsDao getLinkedFormByFormId(int accountId, int formId, String type);
    
    public LinkedFormsDao getFormResponseFkPer(int filledFormId);
    
    public LinkedFormsDao getFormResponseCreator(int filledFormId, int formId);
    
    public int getUserAccess(int formId, int userId, int creator);


    /** get latest response for a study form 
     *  @author Sonia Abrol
     *  @return Hashtable of data with response attributes
     *  	possible keys:
     *  
     *  	formlibver - form version for the response
     *     pkstudyforms - response PK
     *     
     *  07/02/09 */
       
    
   	public Hashtable getLatestStudyFormResponeData(String formId, String studyId) ;

   	/**
     * Overloaded method Gets the list of Filled study specific forms and
     * respective number of times they have been filled - Study Level/Specific
     * Study Level For displaying the Study forms for data entry, the primary
     * organization of the logged in user is matched with the organizations
     * selected in the filter and all the groups that the user belongs to are
     * matched with the selected filter groups. The hidden forms are not
     * displayed and forms are displayed as per the sequence entered by user The
     * study forms are displayed before All Study Forms Retrieves Active and
     * Lockdown forms. The method also gets the response data and sets it within the LinkedFormsDao
     *
     * @param accountId
     *            The accountId for which the Forms are to be fetched
     * @param studyId
     *            The studyId for which the Forms are to be fetched
     * @param userId
     *            The user for which the Forms are to be fetched
     * @param siteId
     *            The primary organization of loggedin user
     * @param grpRight
     *            The group right for All Study Forms access
     * @param stdRight
     *            The study team right for Study Forms access
     * @param isIrb
     *            true=only get IRB forms; false=get all forms
     * @param submissionType
     *            Submission type for IRB application (study) 
     * @param formSubtype
     *            form subtype for IRB application (study) 
     */

    public LinkedFormsDao getStudyFormsWithResponseData(int accountId, int studyId, int userId,
            int siteId, int grpRight, int stdRight, 
            boolean isIrb, String submissionType, String formSubtype);
    
    /*Added For Bug# 9255 :Date 14 May 2012 :By YPS*/
    public LinkedFormsDao getPatStdFormsForPatRestrAndSingleEntry(int accountId, int patId,
            int studyId, int userId, int siteId,Hashtable<String , String> htParams);

    public LinkedFormsDao getFormResponseAuditInfo(int formId, int filledFormId);

}