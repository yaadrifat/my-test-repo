/*
 * Classname			StorageAllowedItemsAgentRObj
 * 
 * Version information : 
 *
 * Date					08/13/2007
 * 
 * Copyright notice : Velos Inc
 */

package com.velos.eres.service.storageAllowedItemsAgent.impl;

/* IMPORT STATEMENTS */

import java.util.Hashtable;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.common.StorageAllowedItemsDao;
import com.velos.eres.business.storageAllowedItems.impl.StorageAllowedItemsBean;
import com.velos.eres.service.storageAllowedItemsAgent.StorageAllowedItemsAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.business.audit.impl.AuditBean;

/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the entity CMP.
 * 
 * @author Khader
 * @version 1.0, 08/13/2007
 * @ejbHome StorageStatusAgentHome
 * @ejbRemote StorageAllowedItemsAgentRObj
 */

@Stateless
public class StorageAllowedItemsAgentBean implements StorageAllowedItemsAgentRObj {

    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
    @Resource private SessionContext context;

   

    public int setStorageAllowedItemsDetails(StorageAllowedItemsBean storageAllowedItems) {
    	StorageAllowedItemsBean ad = new StorageAllowedItemsBean();
    	int rowfound = 0;

    	try {
    		ad.updateStorageAllowedItems(storageAllowedItems);
    		em.persist(ad);
    		return ad.getPkAllowedItem();
    	} catch (Exception e) {
    		Rlog.fatal("storageAllowedItems",
    				"Error in setStorageAllowedItemsDetails() in StorageAllowedItemsAgentRObj " + e);
    	}
    	return 0;

    	
    }
    // Overloaded for INF-18183 ::: Akshi
    public void deleteStorageAllowedItems(int fkStorage,Hashtable<String, String> args) {    	
    	try {
    		
    		AuditBean audit=null;
            String currdate =DateUtil.getCurrentDate();
            String userID=(String)args.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
            String ipAdd=(String)args.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
            String reason=(String)args.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
            String appmodule=(String)args.get(AuditUtils.APP_MODULE); /*Fetches the application module name from the Hashtable*/
            String getRidValue= AuditUtils.getRidValue("ER_ALLOWED_ITEMS","eres","FK_STORAGE="+fkStorage);/*Fetches the RID/PK_VALUE*/ 
            audit = new AuditBean("ER_ALLOWED_ITEMS",String.valueOf(fkStorage),getRidValue,
        			userID,currdate,appmodule,ipAdd,reason);/*POPULATE THE AUDIT BEAN*/ 
            em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/       	    	
            StorageAllowedItemsDao storageDao = new StorageAllowedItemsDao();
            storageDao.deleteStorageAllowedItems(fkStorage);
           
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("crf", "Exception In deleteEventCrf in EventCrfAgentBean " + e);
        }        
    }
    
    public int updateStorageAllowedItems(StorageAllowedItemsBean storageAllowedItems){
    	
    	StorageAllowedItemsBean adm = null;
    	int rowfound = 0;
        int output;
        try {
            adm = (StorageAllowedItemsBean) em.find(StorageAllowedItemsBean.class, new Integer(
            		storageAllowedItems.getPkAllowedItem()));
            if (adm == null) {
                return -2;
            }
            output = adm.updateStorageAllowedItems(storageAllowedItems);
            em.merge(adm);
        } catch (Exception e) {
            Rlog.debug("storageAllowedItem", "Error in updateStorageAllowedItems in StorageAllowedItemsAgentBean"
                    + e);
            return -2;
        }
        return output;
    }
    
    public StorageAllowedItemsDao getStorageAllowedItemValue(int pkStorage) {
        
        try {
        	
        	StorageAllowedItemsDao storageAllowedItemsDao = new StorageAllowedItemsDao();
        	storageAllowedItemsDao.getStorageAllowedItemValue(pkStorage);
            return storageAllowedItemsDao;
            
        } catch (Exception e) {
            Rlog.fatal("storage", "Exception In  getStorageAllowedItemValue in StorageAllowedItemsAgentBean " + e);
        }
        return null;
    }

    	
    	
   
   
    
       
}
   
