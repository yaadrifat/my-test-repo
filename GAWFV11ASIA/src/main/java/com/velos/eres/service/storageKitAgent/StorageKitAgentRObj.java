/*
 * Classname:			StorageKitAgentRObj
 *
 * Version information: 1.0
 *
 * Date:				05Aug2009
 *
 * Copyright notice: 	Velos Inc
 *
 * Author:				Jnanamay
 */

package com.velos.eres.service.storageKitAgent;

import javax.ejb.Remote;

import com.velos.eres.business.storageKit.impl.StorageKitBean;

import com.velos.eres.business.common.StorageKitDao;



/**
 * Remote interface for StorageKitAgentBean session EJB
 *
 * @author Jnanamay
 * @version 1.0, 08/05/2009
 */
@Remote
public interface StorageKitAgentRObj {

    public StorageKitBean getStorageKitDetails(int pkStorageKit);

    public int setStorageKitDetails(StorageKitBean storagekit);

    public int updateStorageKit(StorageKitBean storagekit);

    public StorageKitDao getStorageKitAttributes(int pkStorageKit);


}
