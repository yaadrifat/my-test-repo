/*
 * Classname			SectionAgentBean.class
 * 
 * Version information
 *
 * Date					02/22/2001
 * 
 * Copyright notice
 */

package com.velos.eres.service.sectionAgent.impl;

import java.util.ArrayList;
import java.util.Hashtable;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.audit.impl.AuditBean;
import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.business.common.SectionDao;
import com.velos.eres.business.invoice.InvoiceBean;
import com.velos.eres.business.section.impl.SectionBean;
import com.velos.eres.service.sectionAgent.SectionAgent;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * The stateless session EJB acting as a facade for the entity CMP.<br>
 * <br>
 * 
 * @author
 */

@Stateless
public class SectionAgentBean implements SectionAgent {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
	
	@Resource
	private SessionContext context;

    /**
     * 
     */
    private static final long serialVersionUID = 3905241208606568505L;

    public SectionBean getSectionDetails(int id) {
    	SectionBean sb = new SectionBean();
        try {

            sb = (SectionBean) em.find(SectionBean.class, new Integer(id));
            
            CommonDAO cdSec = new CommonDAO();
            cdSec.populateClob( "er_studysec","studysec_text"," where pk_studysec = " + id);
            sb.setContents(cdSec.getClobData());
            
            return sb;

        } catch (Exception e) {
            e.printStackTrace();
            Rlog.fatal("section", "EXCEPTION IN GETTING SECTION DETAILS" + e);
            return null;
        }

    }

    /**
     * Sets a Study.
     * 
     * @param study
     *            a state holder containing study attributes to be set.
     * @return String
     */

    public int setSectionDetails(SectionBean ssk) {
        int sectionID = 0;

        try {

        	SectionBean sb = new SectionBean();
             
             sb.updateSection(ssk);
             em.persist(sb);
             
             //em.merge(ssk);
               
            return sb.getId();
        } catch (Exception e) {
            e.printStackTrace();
            Rlog.fatal("section", "EXCEPTION IN SETTING SECTION" + e);
        }

        return -2;
    }

    // calls update method

    public int updateSection(SectionBean ssk) {
        SectionBean sec = null; // Study section Entity Bean Remote Object

        int ret = 0;

        try {

            sec = (SectionBean) em.find(SectionBean.class, new Integer(ssk
                    .getId()));
            ret = sec.updateSection(ssk);
            em.merge(sec);
            
            
            return ret;
        } catch (Exception e) {
            e.printStackTrace();
            Rlog.fatal("section", "EXCEPTION IN GETTING SECTION DETAILS" + e);
            return -2;
        }
    }

    public SectionDao getStudySection(int studyVer, String flag, String sectionName) {
        Rlog.debug("section", "session bean: outside try block");
        try {
            Rlog.debug("section",
                    "In getStudy in SectionAgentBean line number 0");
            SectionDao sectionDao = new SectionDao();
            sectionDao.getStudySection(studyVer, flag, sectionName);
            return sectionDao;
        } catch (Exception e) {
            Rlog.fatal("section", "Exception In getStudy in SectionAgentBean "
                    + e);
        }
        return null;

    }

    /**
     * Updates the sequence number of the sections
     */
    public int updateSectionSequence(ArrayList sectionIds,
            ArrayList sectionSequences) {

        int ret = 0;
        int length = sectionIds.size();
        Rlog
                .debug("section",
                        "In updateSectionSequence in SectionAgentBean length="
                                + length);
        try {
            for (int i = 0; i < length; i++) {
                Rlog.debug("section",
                        "In updateSectionSequence in SectionAgentBean sectionId="
                                + StringUtil.stringToNum((String) (sectionIds
                                        .get(i))));
                SectionBean ssk = this.getSectionDetails(StringUtil
                        .stringToNum((String) (sectionIds.get(i))));
                Rlog.debug("section",
                        "In updateSectionSequence in SectionAgentBean sequence="
                                + (String) sectionSequences.get(i));
                ssk.setSecSequence((String) sectionSequences.get(i));
                Rlog
                        .debug("section",
                                "In updateSectionSequence after setting the section sequence");
                // this.setSectionDetails(ssk);
                ret = ret + this.updateSection(ssk);

                Rlog
                        .debug("section",
                                "In updateSectionSequence after updation");
            }
        } catch (Exception e) {
            Rlog.fatal("section",
                    "Exception In updateSectionSequence in SectionAgentBean "
                            + e);
            ret = -2;
        }
        return ret;
    }

    public int studySectionDelete(int sectionId) {
        SectionBean sBean = null;
        try {
            /*
             * SectionPK sectionPk = new SectionPK(sectionId); SectionHome
             * sectionHome = EJBUtil.getSectionHome();
             */
            sBean = (SectionBean) em.find(SectionBean.class, new Integer(
                    sectionId));
            if (sBean != null)
                em.remove(sBean);
            return 0;
        } catch (Exception e) {
            Rlog.fatal("section",
                    "Exception in studySectionDelete in SectionAgenBean" + e);
            return -1;
        }
    }
    
    // Overloaded for INF-18183 ::: AGodara
    public int studySectionDelete(int sectionId,Hashtable<String, String> auditInfo) {
        
    	SectionBean sBean = null;
    	AuditBean audit=null;
        try {
        	            
        	String currdate =DateUtil.getCurrentDate();
        	String userID=(String)auditInfo.get(AuditUtils.USER_ID_KEY); //Fetches the User ID from the Hashtable
            String ipAdd=(String)auditInfo.get(AuditUtils.IP_ADD_KEY); //Fetches the IP Address from the Hashtable
            String reason=(String)auditInfo.get(AuditUtils.REASON_FOR_DEL); //Fetches the reason for deletion from the Hashtable
            String app_module=(String)auditInfo.get(AuditUtils.APP_MODULE);// Fetches the APP_Module from the Hashtable
            String rid= AuditUtils.getRidValue("ER_STUDYSEC","eres","PK_STUDYSEC="+sectionId); //Fetches the RID
            
            //POPULATE THE AUDIT BEAN 
            audit = new AuditBean("ER_STUDYSEC",String.valueOf(sectionId),rid,userID,currdate,app_module,ipAdd,reason);
        	
            sBean = (SectionBean) em.find(SectionBean.class, new Integer(
                    sectionId));
                       
            em.persist(audit); //PERSIST THE DATA IN AUDIT TABLE
            em.remove(sBean);
           
            return 0;
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("section",
                    "Exception in studySectionDelete(int sectionId,Hashtable<String, String> auditInfo)" + e);
            return -2;
        }
    }

}// end of class

