/*
 * Classname			UserSessionLogAgentBean
 * 
 * Version information 	1.0
 *
 * Date					03/10/2003
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.service.userSessionLogAgent.impl;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.eres.business.userSessionLog.impl.UserSessionLogBean;
import com.velos.eres.service.userSessionLogAgent.UserSessionLogAgentRObj;
import com.velos.eres.service.util.Rlog;

/**
 * The stateless session EJB acting as a facade for the entity CMP.<br>
 * <br>
 * 
 * @author Sonia Sahni
 * @version 1.0 03/10/2003
 */
@Stateless
public class UserSessionLogAgentBean implements UserSessionLogAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    /**
     * 
     */
    private static final long serialVersionUID = 3762813800943990070L;

    /**
     * getUserSessionLogDetails Calls getUserSessionLogDetails() on entity bean -
     * UserSessionLogBean Looks up on the UserSessionLog id parameter passed in
     * and constructs and returns a UserSessionLogStateKeeper.
     * 
     * @param userSessionLogId
     * @see UserSessionLogBean
     */
    public UserSessionLogBean getUserSessionLogDetails(int userSessionLogId) {

        UserSessionLogBean usk = null;

        try {

            return (UserSessionLogBean) em.find(UserSessionLogBean.class,
                    new Integer(userSessionLogId));
        } catch (Exception e) {
            Rlog.fatal("usl",
                    "Error in getUserSessionLogDetails() in UserSessionLogAgentBean"
                            + e);
        }
        return usk;
    }

    /**
     * Creates a new a UserSessionLog. Calls setUserSessionLogDetails() on
     * entity bean UserSessionLog
     * 
     * @param a
     *            UserSessionLogStateKeeper containing UserSessionLog attributes
     *            to be set.
     * @return generated UserSessionLog id
     * @see UserSessionBeanLog
     */

    public int setUserSessionLogDetails(UserSessionLogBean usk) {

        try {

            em.merge(usk);
            return usk.getId();
        } catch (Exception e) {
            Rlog.fatal("usl",
                    "Error in setUserSessionLogDetails() in UserSessionLogAgentBean"
                            + e);
        }
        return -1;
    }

    /**
     * Updates UserSessionLog Details. Calls updateUserSessionLog() on entity
     * bean UserSessionBeanLog
     * 
     * @param a
     *            UserSessionLogStateKeeper containing UserSessionLog attributes
     *            to be set.
     * @return int 0 for successful ; -2 for exception
     * @see UserSessionBeanLog
     */

    public int updateUserSessionLog(UserSessionLogBean usk) {

        UserSessionLogBean userSessionLogBean = null;
        int output;

        try {
            userSessionLogBean = (UserSessionLogBean) em.find(
                    UserSessionLogBean.class, new Integer(usk.getId()));

            if (userSessionLogBean == null) {
                return -2;
            }
            output = userSessionLogBean.updateUserSessionLog(usk);
        } catch (Exception e) {
            Rlog.debug("usl",
                    "Error in updateUserSessionLog in UserSessionLogAgentBean"
                            + e);
            return -2;
        }
        return output;
    }

}// end of class
