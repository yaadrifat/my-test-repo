/*
 * Classname			FormLibAgentBean
 * 
 * Version information : 
 *
 * Date					07/11/2003
 * 
 * Copyright notice : Velos Inc
 */

package com.velos.eres.service.formLibAgent.impl;

/* IMPORT STATEMENTS */

import java.util.ArrayList;
import java.util.Hashtable;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.audit.impl.AuditBean;
import com.velos.eres.business.common.FormLibDao;
import com.velos.eres.business.common.SaveFormDao;
import com.velos.eres.business.formLib.impl.FormLibBean;
import com.velos.eres.business.saveForm.SaveFormStateKeeper;
import com.velos.eres.service.formLibAgent.FormLibAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the entity CMP.
 * 
 * @author Anu Khanna
 * @version 1.0, 07/11/2003
 * @ejbHome FormLibAgentHome
 * @ejbRemote FormLibAgentRObj
 */

@Stateless
public class FormLibAgentBean implements FormLibAgentRObj {

    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
    
	@Resource
	private SessionContext context;

    /**
     * Looks up on the form id parameter passed in and constructs and returns a
     * FormLibStateKeeper containing all the summary details of the form
     * 
     * @param formLibId
     *            The Form id
     * @see FormLibStateKeeper
     */

@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public FormLibBean getFormLibDetails(int formLibId) {
        FormLibBean retrieved = null;

        try {

            retrieved = (FormLibBean) em.find(FormLibBean.class, new Integer(
                    formLibId));
        } catch (Exception e) {
            Rlog.fatal("formlib",
                    "Error in getFormLibDetails() in FormLibAgentBean" + e);
        }

        return retrieved;
    }

    /**
     * Add the form details in the FormLibStateKeeper Object to the database
     * 
     * @param formlib
     *            An FormLibStateKeeper containing form attributes to be set.
     * @return The form Id for the form just created <br>
     *         0 - if the method fails
     */

    public int setFormLibDetails(FormLibBean formLibsk) {
        try {

            int rowfound = 0;
            FormLibBean fb = new FormLibBean();
            try {
                Query query = em.createNamedQuery("findByUniqueFormName");
                query.setParameter("fk_account", StringUtil.stringToNum(formLibsk
                        .getAccountId()));
                query.setParameter("form_name", formLibsk.getFormLibName());
                query.setParameter("form_linkto", formLibsk.getFormLibLinkTo());

                ArrayList list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();
                if (list.size() > 0) {
                    // form name is not unique
                    Rlog
                            .debug("formlib",
                                    "setFormLibAgentBean.setFormDetails : the form name is not unique");
                    rowfound = 1;
                    return -3;
                }

            } catch (Exception ex) {
                Rlog.fatal("formlib",
                        "In setFormLibAgentBean Details Session FIND UNIQUE FORM NAME EXCEPTION"
                                + ex);
                rowfound = 0; // unique name does not exist
                // Added by Manimaran on 1,Jun2005 for preventing the duplicate
                // entry of form name.

                // Commented by SOnia Abrol, 08/12/05, will not be relevant in
                // ejb 3
                /*
                 * String s1 = ex.toString(); if
                 * (s1.equals("javax.ejb.FinderException: More than one entity
                 * matches the finder criteria.")) { return -3;// if multiple
                 * record exists with same form // name. }
                 */

            }

            fb.updateFormLib(formLibsk);
            em.persist(fb);

            int formLibId = fb.getFormLibId();
            return formLibId;

        } catch (Exception e) {
            Rlog.fatal("formlib",
                    "Error in setFormLibDetails() in FormLibAgentBean " + e);
            return -2;
        }

    }
    
    
    public int updateFormLib(FormLibBean flsk, boolean skipNameCheck) {
    	FormLibBean retrieved = null;
    	int output;
    	String oldStatus = "";
    	String newStatus = "";
    	boolean booleanForFormStatus;

    	try {

    		retrieved = (FormLibBean) em.find(FormLibBean.class, new Integer(
    				flsk.getFormLibId()));

    		if (!skipNameCheck && !((retrieved.getFormLibName()).toUpperCase()).equals((flsk
    				.getFormLibName()).toUpperCase())) {

    			//Apr17,06-Added by Manimaran to check forms which are not linked to Study(Bug 2563)
    			if  (!flsk.getFormLibLinkTo().equals("S")) {
    				try {
    					Query query = em.createNamedQuery("findByUniqueFormName");
    					query.setParameter("fk_account", StringUtil.stringToNum(flsk
    							.getAccountId()));
    					query.setParameter("form_name", flsk.getFormLibName());
    					query.setParameter("form_linkto", flsk.getFormLibLinkTo());

    					ArrayList list = (ArrayList) query.getResultList();
    					if (list == null)
    						list = new ArrayList();
    					if (list.size() > 0) {
    						// form name is not unique
    						Rlog
    						.debug("formlib",
    								"setFormLibAgentBean.updateFormDetails : the form name is not unique");
    						return -3;
    					}
    				} catch (Exception ex) {
    					Rlog.fatal("formLib", "In updateFormLib call to  FIND FORM NAME EXCEPTION" + ex);
    				}
    			}

    		} // end check if form name is changed

    		/* Get the old and new status */
    		oldStatus = retrieved.getFormLibStatus();
    		newStatus = flsk.getFormLibStatus();
    		booleanForFormStatus = flsk.getBooleanForFrmStat();

    		/*
    		 * If the old and new status are different ie the user has changed
    		 * the status of the form then call SP which updates the
    		 * er_formstatus and inserts new record in it.
    		 */
    		Rlog.debug("formlib", "oldStatus equals" + oldStatus);
    		Rlog.debug("formlib", "newStatus equals" + newStatus);
    		Rlog.debug("formlib", "booleanFormStatus equals:"
    				+ booleanForFormStatus);
    		if (booleanForFormStatus) {
    			if (!oldStatus.equals(newStatus)) {
    				Rlog.debug("formlib", "inside not equals");
    				FormLibDao formLibDao = new FormLibDao();


    				formLibDao.updateInsertForStatusChange(flsk
    						.getFormLibId(), StringUtil.stringToNum(newStatus),
    						StringUtil.stringToNum(flsk.getCreator()), flsk
    						.getIpAdd());
    			}
    		}
    		if (retrieved == null) {
    			return -2;
    		}
    		output = retrieved.updateFormLib(flsk);
    		em.merge(retrieved);

    		Rlog.debug("formlib",
    				"updateFormLib in SONIA* FormLibAgentBean retrieved"
    						+ retrieved.getFormLibXslRefresh());

    	} catch (Exception e) {
    		Rlog.fatal("formlib", "Error in updateFormLib in FormLibAgentBean"
    				+ e);
    		return -2;
    	}
    	return output;
    }

    /**
     * Update the form details contained in the FormLibStateKeeper Object to the
     * database
     * 
     * @param flsk
     *            An FormLibStateKeeper containing form attributes to be
     *            updated.
     * @return 0 - if the update is successful<br>
     *         -2 - if the update fails
     */
    public int updateFormLib(FormLibBean flsk) {

        FormLibBean retrieved = null;

        int output;
        String oldStatus = "";
        String newStatus = "";
        boolean booleanForFormStatus;

        int ret = 0;

        try {

            retrieved = (FormLibBean) em.find(FormLibBean.class, new Integer(
                    flsk.getFormLibId()));

            if (!((retrieved.getFormLibName()).toUpperCase()).equals((flsk
                    .getFormLibName()).toUpperCase())) {
            
				//Apr17,06-Added by Manimaran to check forms which are not linked to Study(Bug 2563)
				if  (!flsk.getFormLibLinkTo().equals("S")) {
            	try {
                    Query query = em.createNamedQuery("findByUniqueFormName");
                    query.setParameter("fk_account", StringUtil.stringToNum(flsk
                            .getAccountId()));
                    query.setParameter("form_name", flsk.getFormLibName());
                    query.setParameter("form_linkto", flsk.getFormLibLinkTo());

                    ArrayList list = (ArrayList) query.getResultList();
                    if (list == null)
                        list = new ArrayList();
                    if (list.size() > 0) {
                        // form name is not unique
                        Rlog
                                .debug("formlib",
                                        "setFormLibAgentBean.updateFormDetails : the form name is not unique");
                    return -3;
                    }

                } catch (Exception ex) {
                    Rlog.fatal("formLib",
                            "In updateFormLib call to  FIND FORM NAME EXCEPTION"
                                    + ex);

                }

            }

        } // end check if form name is changed

            /* Get the old and new status */
            oldStatus = retrieved.getFormLibStatus();
            newStatus = flsk.getFormLibStatus();
            booleanForFormStatus = flsk.getBooleanForFrmStat();

            /*
             * If the old and new status are different ie the user has changed
             * the status of the form then call SP which updates the
             * er_formstatus and inserts new record in it.
             */
            Rlog.debug("formlib", "oldStatus equals" + oldStatus);
            Rlog.debug("formlib", "newStatus equals" + newStatus);
            Rlog.debug("formlib", "booleanFormStatus equals:"
                    + booleanForFormStatus);
            if (booleanForFormStatus) {
                if (!oldStatus.equals(newStatus)) {
                    Rlog.debug("formlib", "inside not equals");
                    FormLibDao formLibDao = new FormLibDao();


					ret = formLibDao.updateInsertForStatusChange(flsk
                            .getFormLibId(), StringUtil.stringToNum(newStatus),
                            StringUtil.stringToNum(flsk.getCreator()), flsk
                                    .getIpAdd());
				}
            }
            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updateFormLib(flsk);
            em.merge(retrieved);

            Rlog.debug("formlib",
                    "updateFormLib in SONIA* FormLibAgentBean retrieved"
                            + retrieved.getFormLibXslRefresh());

        } catch (Exception e) {
            Rlog.fatal("formlib", "Error in updateFormLib in FormLibAgentBean"
                    + e);
            return -2;
        }
        return output;
    }
    
    
    // Overloaded for INF-18183 ::: AGodara
    public int updateFormLib(FormLibBean flsk,Hashtable<String, String> auditInfo) {

        FormLibBean retrieved = null;
        int output;
        String oldStatus = "";
        String newStatus = "";
        boolean booleanForFormStatus;
        int ret;
        try {
            retrieved = (FormLibBean) em.find(FormLibBean.class, new Integer(
                    flsk.getFormLibId()));
            if (!((retrieved.getFormLibName()).toUpperCase()).equals((flsk
                    .getFormLibName()).toUpperCase())) {
        
				//Apr17,06-Added by Manimaran to check forms which are not linked to Study(Bug 2563)
				if  (!flsk.getFormLibLinkTo().equals("S")) {
            	try {
                    Query query = em.createNamedQuery("findByUniqueFormName");
                    query.setParameter("fk_account", StringUtil.stringToNum(flsk
                            .getAccountId()));
                    query.setParameter("form_name", flsk.getFormLibName());
                    query.setParameter("form_linkto", flsk.getFormLibLinkTo());

                    ArrayList list = (ArrayList) query.getResultList();
                    if (list == null)
                        list = new ArrayList();
                    if (list.size() > 0) {
                        // form name is not unique
                        Rlog
                                .debug("formlib",
                                        "setFormLibAgentBean.updateFormDetails : the form name is not unique");
                    return -3;
                    }

                } catch (Exception ex) {
                    Rlog.fatal("formLib",
                            "In updateFormLib call to  FIND FORM NAME EXCEPTION"
                                    + ex);
                }
            }
        } // end check if form name is changed

            /* Get the old and new status */
            oldStatus = retrieved.getFormLibStatus();
            newStatus = flsk.getFormLibStatus();
            booleanForFormStatus = flsk.getBooleanForFrmStat();

            /*
             * If the old and new status are different ie the user has changed
             * the status of the form then call SP which updates the
             * er_formstatus and inserts new record in it.
             */
            Rlog.debug("formlib", "oldStatus equals" + oldStatus);
            Rlog.debug("formlib", "newStatus equals" + newStatus);
            Rlog.debug("formlib", "booleanFormStatus equals:"
                    + booleanForFormStatus);
            if (booleanForFormStatus) {
                if (!oldStatus.equals(newStatus)) {
                    Rlog.debug("formlib", "inside not equals");
                    FormLibDao formLibDao = new FormLibDao();


                    ret = formLibDao.updateInsertForStatusChange(flsk
                            .getFormLibId(), StringUtil.stringToNum(newStatus),
                            StringUtil.stringToNum(flsk.getCreator()), flsk
                                    .getIpAdd());
				}
            }
            if (retrieved == null) {
                return -2;
            }

			if(flsk.getRecordType().equalsIgnoreCase("D")){
				AuditBean audit=null;
	            
	        	String currdate =DateUtil.getCurrentDate();
	        	String userID=(String)auditInfo.get(AuditUtils.USER_ID_KEY); //Fetches the User ID from the Hashtable
	            String ipAdd=(String)auditInfo.get(AuditUtils.IP_ADD_KEY); //Fetches the IP Address from the Hashtable
	            String reason=(String)auditInfo.get(AuditUtils.REASON_FOR_DEL); //Fetches the reason for deletion from the Hashtable
	            String app_module=(String)auditInfo.get(AuditUtils.APP_MODULE);// Fetches the APP_Module from the Hashtable
	            String rid= AuditUtils.getRidValue("ER_FORMLIB","eres","PK_FORMLIB="+flsk.getFormLibId()); //Fetches the RID
				
	            //POPULATE THE AUDIT BEAN
				audit = new AuditBean("ER_FORMLIB",String.valueOf(flsk.getFormLibId()),rid,userID,currdate,app_module,ipAdd,reason);
                em.persist(audit); //PERSIST THE DATA IN AUDIT TABLE
            }
           output = retrieved.updateFormLib(flsk);
           if(output!=0){
              	context.setRollbackOnly();
           }
           em.merge(retrieved);
           
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("formlib", "Error in updateFormLib in FormLibAgentBean"
                    + e);
            return -2;
        }
        return output;
    }

    public FormLibDao getFormDetails(int accountId, int userId,
            String formName, int formType) {
        try {
            Rlog.debug("formlib", "In getFormDetails in FormLibAgentBean - 0");
            FormLibDao formLibDao = new FormLibDao();
            formLibDao.getFormDetails(accountId, userId, formName, formType);
            return formLibDao;

        } catch (Exception e) {
            Rlog.fatal("formlib",
                    "Exception In getFormDetails in FormLibAgentBean " + e);
        }

        return null;
    }
    
    //added by IA bug 2614 multipleorganizations
    public FormLibDao getFormDetails(int accountId, int userId,
            String formName, int formType, String orgId) {
        try {
            Rlog.debug("formlib", "In getFormDetails in FormLibAgentBean - 0");
            FormLibDao formLibDao = new FormLibDao();
            formLibDao.getFormDetails(accountId, userId, formName, formType, orgId);
            return formLibDao;

        } catch (Exception e) {
            Rlog.fatal("formlib",
                    "Exception In getFormDetails in FormLibAgentBean " + e);
        }

        return null;
    }    
    
    //end added

    public int[] insertCopyFieldToForm(int pkForm, int pkSection, int pkField,
            String user, String ipAdd) {

        int ret[] = new int[2];
        try {
            Rlog.debug("formlib",
                    "In insertCopyFieldToForm in FormLibAgentBean - 0");
            FormLibDao formLibDao = new FormLibDao();
            ret = formLibDao.insertCopyFieldToForm(pkForm, pkSection, pkField,
                    user, ipAdd);

        } catch (Exception e) {
        	
            Rlog.fatal("formlib",
                    "Exception In getFormDetails in FormLibAgentBean " + e);
            ret[0] = -2;
            return ret;
        }

        return ret;
    }

    public FormLibDao getSectionFieldInfo(int formId) {
        try {
            Rlog.debug("formlib",
                    "In getSectionFieldInfo in FormLibAgentBean - 0");
            FormLibDao formLibDao = new FormLibDao();
            formLibDao.getSectionFieldInfo(formId);
            return formLibDao;

        } catch (Exception e) {
            Rlog
                    .fatal("formlib",
                            "Exception In getSectionFieldInfo in FormLibAgentBean "
                                    + e);
        }

        return null;
    }

    public int copyFormsFromLib(String[] idArray, String formType,
            String formName, String creator, String ipAdd, String formDesc) {
        int newFormPk = 0;
        try {
            Rlog
                    .debug("formlib",
                            "In copyFormsFromLib in FormLibAgentBean - 0");
            FormLibDao formLibDao = new FormLibDao();
            newFormPk = formLibDao.copyFormsFromLib(idArray, formType,
                    formName, creator, ipAdd, formDesc);
            return newFormPk;

        } catch (Exception e) {
            Rlog.fatal("formlib",
                    "Exception In copyFormsFromLib in FormLibAgentBean " + e);
        }

        return -1;
    }

    /**
     * This method creates er_objectshare data for all the forms who have the
     * user primary organization in sharewith also to create record for the new
     * user account called when a new user is created
     * 
     * @param userId
     * @param accountId
     * @param siteId
     * @param creator
     * @param ipAdd
     * @return 0 if successful, -1 if error
     */
    public int createFormShareWithData(int userId, int accountId, int siteId,
            int creator, String ipAdd) {
        int ret = 0;
        try {
            Rlog.debug("formlib",
                    "In createFormShareWithData in FormLibAgentBean");
            FormLibDao formLibDao = new FormLibDao();
            ret = formLibDao.createFormShareWithData(userId, accountId, siteId,
                    creator, ipAdd);
            return ret;

        } catch (Exception e) {
            Rlog.fatal("formlib",
                    "Exception In createFormShareWithData in FormLibAgentBean "
                            + e);
            return -1;
        }

    }

    public int updateInsertForStatusChange(int formLibId, int newStatus,
            int creator, String ipAdd) {

        int ret = 0;
        try {
            Rlog.debug("formlib",
                    "In updateInsertForStatusChange in FormLibAgentBean - 0");
            FormLibDao formLibDao = new FormLibDao();
            ret = formLibDao.updateInsertForStatusChange(formLibId, newStatus,
                    creator, ipAdd);

        } catch (Exception e) {
            Rlog.fatal("formlib",
                    "Exception In updateInsertForStatusChange in FormLibAgentBean "
                            + e);
            return -2;
        }

        return ret;
    }

    public int insertFilledFormBySP(SaveFormStateKeeper sk)
    {
        int ret=-1;
        try {
            Rlog.debug("formlib",
                    "In insertFilledFormBySP in FormLibAgentBean ");
            SaveFormDao saveFormDao = new SaveFormDao();
            ret = saveFormDao.insertFilledFormBySP(sk);

        } catch (Exception e) {
            Rlog.fatal("formlib",
                    "Exception In FormLibAgentBean:insertFilledFormBySP(SaveFormStateKeeper sk) "
                            + e);
            return -2;
        }
        
        return ret;
    }
    public int  updateFilledFormBySP(SaveFormStateKeeper sk)
    {
        int ret=-1;
        try {
            Rlog.debug("formlib",
                    "In updateFilledFormBySP in FormLibAgentBean ");
            SaveFormDao saveFormDao = new SaveFormDao();
            ret = saveFormDao.updateFilledFormBySP(sk);

        } catch (Exception e) {
            Rlog.fatal("formlib",
                    "Exception In FormLibAgentBean:updateFilledFormBySP(SaveFormStateKeeper sk) "
                            + e);
            return -2;
        }
        
        return ret;
    }
  //Added by Vivek Jha for Monitor Team Role in jupiter
    public int  updateFilledFormStatus(SaveFormStateKeeper sk)
    {
        int ret=-1;
        try {
            Rlog.debug("formlib",
                    "In updateFilledFormStatus in FormLibAgentBean ");
            SaveFormDao saveFormDao = new SaveFormDao();
            ret = saveFormDao.updateFilledFormStatus(sk);

        } catch (Exception e) {
            Rlog.fatal("formlib",
                    "Exception In FormLibAgentBean:updateFilledFormStatus(SaveFormStateKeeper sk) "
                            + e);
            return -2;
        }
        
        return ret;
    }
    
    
    //Added by Manimaran for the Enhancement F6
    public String getFormLibVersionNumber(int formlibverPK) {
    	String formLibVer = "";
        try {
        	Rlog.debug("formlib",
                    "In getFormLibVersionNumber in FormLibAgentBean");
        	FormLibDao formLibDao = new FormLibDao();
        	formLibVer = formLibDao.getFormLibVersionNumber(formlibverPK);
                       
        } catch (Exception e) {
            Rlog
                    .fatal("formlib", "Exception in getFormLibVersionNumber in FormLibAgentBean "
                            + e);
        }
        return formLibVer;
        
    }
    
//  added by Sonia, 06/15/07, to get the first PK of a filled form answered for a patient by a creator
    public int getPatFilledFormPKForCreator(int form,int patientPK,int creator)
    {	
    	int  filledformpk = 0;
    try {
    	
    	FormLibDao formLibDao = new FormLibDao();
    	filledformpk  = formLibDao.getPatFilledFormPKForCreator(form, patientPK,creator);
                   
    } catch (Exception e) {
        Rlog
                .fatal("formlib", "Exception in getPatFilledFormPKForCreator in FormLibAgentBean "
                        + e);
    }
    return filledformpk;
    
    }
    
    public int validateFormField(int formID,int responseID,String systemID,int studyID,int patientID,int accountID,String datavalue,String operator)
    {
    	int ret=0;
    	try{
    		FormLibDao formLibDao = new FormLibDao();
    		ret = formLibDao.validateFormField(formID,responseID,systemID,studyID,patientID,accountID,datavalue,operator);
    		
    	}
    	catch(Exception e){
    		e.printStackTrace();
    		Rlog.fatal("formlib", "Exception in validateFormField in FormLibAgentBean "+ e);
    		
    	}
    	return ret;
    }
    
    public String validateFormField(int formID,String systemID,int studyID,int patientID,int accountID,String datavalue)
    {
    	String ret="";
    	try{
    		FormLibDao formLibDao = new FormLibDao();
    		ret = formLibDao.validateFormField(formID,systemID,studyID,patientID,accountID,datavalue);
    		
    	}
    	catch(Exception e){
    		e.printStackTrace();
    		Rlog.fatal("formlib", "Exception in validateFormField in FormLibAgentBean "+ e);
    		
    	}
    	return ret;
    }
     
    public int getPatFilledFormPKForCreatorForParam(int form,int patientPK,int creator,Hashtable htParam)
    {	
    	int  filledformpk = 0;
    try {
    	
    	FormLibDao formLibDao = new FormLibDao();
    	filledformpk  = formLibDao.getPatFilledFormPKForCreatorForParam(form, patientPK,creator,htParam);
                   
    } catch (Exception e) {
        Rlog
                .fatal("formlib", "Exception in getPatFilledFormPKForCreatorForParam in FormLibAgentBean "
                        + e);
    }
    return filledformpk;
    
    }
    
}// end of class

