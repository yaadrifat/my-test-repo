/*
 * Classname			SectionAgent.class
 * 
 * Version information   
 *
 * Date					02/22/2001
 * 
 * Copyright notice
 */

package com.velos.eres.service.sectionAgent;

import java.util.ArrayList;
import java.util.Hashtable;
import javax.ejb.Remote;

import com.velos.eres.business.common.SectionDao;
import com.velos.eres.business.section.impl.SectionBean;

/**
 * Remote interface for SectionAgent session EJB
 * 
 * @author
 */
@Remote
public interface SectionAgent {

    /**
     * gets the section details
     */
    SectionBean getSectionDetails(int id);

    /**
     * sets the section details
     */
    public int setSectionDetails(SectionBean study);

    public int updateSection(SectionBean ssk);

    public SectionDao getStudySection(int studyVer, String flag, String sectionName);

    public int updateSectionSequence(ArrayList sectionIds,
            ArrayList sectionSequences);

    public int studySectionDelete(int sectionId);
    // Overloaded for INF-18183 ::: AGodara
    public int studySectionDelete(int sectionId,Hashtable<String, String> auditInfo);

}
