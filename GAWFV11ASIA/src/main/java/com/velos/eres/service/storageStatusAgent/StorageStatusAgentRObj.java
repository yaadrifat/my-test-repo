/*
 * Classname			StorageStatusAgentRObj
 *
 * Version information : 1.0
 *
 * Date					08/13/2007
 *
 * Copyright notice : Velos Inc
 */

package com.velos.eres.service.storageStatusAgent;

import java.util.Hashtable;
import javax.ejb.Remote;
import com.velos.eres.business.common.StorageStatusDao;

import com.velos.eres.business.storageStatus.impl.StorageStatusBean;

/**
 * Remote interface for StorageStatusAgentBean session EJB
 *
 * @author Manimaran
 * @version 1.0, 08/13/2007
 */
@Remote
public interface StorageStatusAgentRObj {
    public StorageStatusBean getStorageStatusDetails(int pkStorageStat);

    public int setStorageStatusDetails(StorageStatusBean storageStat);

    public int updateStorageStatus(StorageStatusBean storageStat);

    public StorageStatusDao getStorageStausValues(int storageId) ;

    public int delStorageStatus(int pkStorStat);

    // Overloaded for INF-18183 ::: AGodara
    public int delStorageStatus(int pkStorStat,Hashtable<String, String> auditInfo);
    
    public boolean isStorageAllocated(int storageId, int accId);

    public int getDefStatStudyId(int storageId);
}
