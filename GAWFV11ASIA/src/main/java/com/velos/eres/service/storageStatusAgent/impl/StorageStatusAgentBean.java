/*
 * Classname			StorageStatusAgentBean
 *
 * Version information :
 *
 * Date					08/13/2007
 *
 * Copyright notice : Velos Inc
 */

package com.velos.eres.service.storageStatusAgent.impl;

/* IMPORT STATEMENTS */

import java.util.Hashtable;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.storageStatus.impl.StorageStatusBean;
import com.velos.eres.service.storageStatusAgent.StorageStatusAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.business.audit.impl.AuditBean;
import com.velos.eres.business.common.StorageStatusDao;
import com.velos.eres.business.common.CommonDAO;


/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the entity CMP.
 *
 * @author Manimaran
 * @version 1.0, 08/13/2007
 * @ejbHome StorageStatusAgentHome
 * @ejbRemote StorageStatusAgentRObj
 */

@Stateless
public class StorageStatusAgentBean implements StorageStatusAgentRObj {

    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
    
    @Resource
    private SessionContext context;

    public StorageStatusBean getStorageStatusDetails(int pkStorageStat) {
        StorageStatusBean ap = null;
        try {

            ap = (StorageStatusBean) em.find(StorageStatusBean.class, new Integer(pkStorageStat));
            CommonDAO cdao = new CommonDAO();//KM-3171
            cdao.populateClob("er_storage_status","ss_notes"," where pk_storage_status = " + pkStorageStat );
            ap.setSsNotes(cdao.getClobData());

        } catch (Exception e) {
            Rlog.fatal("storageStatus",
                    "Error in getStorageStatusDetails() in StorageStatusAgentBean" + e);
        }

        return ap;
    }

    public int setStorageStatusDetails(StorageStatusBean admin) {

    	StorageStatusBean ad = new StorageStatusBean();
    	int rowfound = 0;

    	try {
    		Rlog.debug("storageStatus", "in try bloc of storageStatusagent");
    		ad.updateStorageStatus(admin);
    		em.persist(ad);
    		return ad.getPkStorageStat();
    	} catch (Exception e) {
    		Rlog.fatal("storageStatus",
    				"Error in setStorageStatusDetails() in StorageStatusAgentBean " + e);
    	}
    	return 0;
    }

    public int updateStorageStatus(StorageStatusBean ap) {
    	StorageStatusBean adm = null;
    	int rowfound = 0;
        int output;
        try {
            adm = (StorageStatusBean) em.find(StorageStatusBean.class, new Integer(
                    ap.getPkStorageStat()));
            if (adm == null) {
                return -2;
            }
            output = adm.updateStorageStatus(ap);
            em.merge(adm);
        } catch (Exception e) {
            Rlog.debug("storageStatus", "Error in updateStorageStatus in StorageStatusAgentBean"
                    + e);
            return -2;
        }
        return output;
    }

    //JM: 06Sep2007
    public StorageStatusDao getStorageStausValues(int storageId) {


    	try {
    		StorageStatusDao storStatDao = new StorageStatusDao();

    		storStatDao.getStorageStausValues(storageId);
            return storStatDao;

        } catch (Exception e) {
            Rlog.fatal("storageStatus", "Exception In getStorageStausValues in StorageStatusAgentBean " + e);
        }
        return null;


    }

    //Added by Manimaran for deletion of storage status.
    public int delStorageStatus(int pkStorStat) {
    	int ret = 0;
    	try {
    		StorageStatusDao storStatDao = new StorageStatusDao();
    		ret = storStatDao.delStorageStatus(pkStorStat);

    	} catch (Exception e) {
    		Rlog.fatal("storageStatus", "Exception In delStorageStatus in StorageStatusAgentBean " + e);
    		return -1;
    	}
    	return ret;


    }
    // Overloaded for INF-18183 ::: AGodara
    public int delStorageStatus(int pkStorStat,Hashtable<String, String> auditInfo) {
    	int ret = 0;
    	AuditBean audit=null;
    	try {
    		
    		String currdate =DateUtil.getCurrentDate();
        	String userID=(String)auditInfo.get(AuditUtils.USER_ID_KEY); //Fetches the User ID from the Hashtable
            String ipAdd=(String)auditInfo.get(AuditUtils.IP_ADD_KEY); //Fetches the IP Address from the Hashtable
            String reason=(String)auditInfo.get(AuditUtils.REASON_FOR_DEL); //Fetches the reason for deletion from the Hashtable
            String app_module=(String)auditInfo.get(AuditUtils.APP_MODULE);// Fetches the APP_Module from the Hashtable
            String rid= AuditUtils.getRidValue("ER_STORAGE_STATUS","eres","pk_storage_status="+pkStorStat); //Fetches the RID
            
            audit = new AuditBean("ER_STORAGE_STATUS",String.valueOf(pkStorStat),rid,userID,currdate,app_module,ipAdd,reason);
        	em.persist(audit); //PERSIST THE DATA IN AUDIT TABLE
        	
            StorageStatusDao storStatDao = new StorageStatusDao();
    		ret = storStatDao.delStorageStatus(pkStorStat);
    		if(ret==-1){
    			context.setRollbackOnly();
    		}
    	} catch (Exception e) {
    		context.setRollbackOnly();
    		Rlog.fatal("storageStatus", "Exception In delStorageStatus in StorageStatusAgentBean " + e);
    		return -1;
    	}
    	return ret;

    }
    
    
  //JM: 23Jul2009: #4147
    public boolean isStorageAllocated(int storageId, int accId){
    	boolean ret = false;
    	try {
    		StorageStatusDao storStatDao = new StorageStatusDao();
    		ret = storStatDao.isStorageAllocated(storageId,accId);

    	} catch (Exception e) {
    		Rlog.fatal("storageStatus", "Exception In isStorageAllocated in StorageStatusAgentBean " + e);
    		return false;
    	}
    	if (ret){
    		return true;
    	}else{
    		return false;
    	}
    }

    public int getDefStatStudyId(int storageId) {

    	int retStudy=0;

    	try{
    		StorageStatusDao storStatDao = new StorageStatusDao();
    		retStudy = storStatDao.getDefStatStudyId(storageId);
    	}catch(Exception e){
    		Rlog.fatal("storageStatus", "Exception In getDefStatStudyId() in StorageStatusAgentBean " + e);
    		return -1;

    	}
    	return retStudy;
    }


}
