/*
 * Classname			InvoiceAgentBean
 * 
 * Version information : 
 *
 * Date					10/18/2005
 * 
 * Copyright notice : Velos Inc
 */

package com.velos.eres.service.invoiceAgent.impl;

/* IMPORT STATEMENTS */

import java.util.Hashtable;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.audit.impl.AuditBean;
import com.velos.eres.business.common.InvoiceDao;
import com.velos.eres.business.common.MileAchievedDao;
import com.velos.eres.business.invoice.InvoiceBean;
import com.velos.eres.service.invoiceAgent.InvoiceAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the PatientFacility POJO.
 * 
 * @author Sonia Abrol
 * @version 1.0, 09/22/2005
  */

@Stateless
public class InvoiceAgentBean implements InvoiceAgentRObj {

    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
	@Resource
	private SessionContext context;

    /**
     * Looks up on the id parameter passed in and constructs and
     * returns a InvoiceBean containing all the Invoice Details 
     * @param id   The Invoice Id
     */
     
    public InvoiceBean getInvoiceDetails(int id) {
        InvoiceBean pb = null; // Category Entity Bean Remote Object
        try {

           pb = (InvoiceBean) em.find(InvoiceBean.class, new Integer(id));

        } catch (Exception e) {
            Rlog.fatal("Invoice",
                    "Error in getInvoiceDetails() in InvoiceAgentBean" + e);
        }

        return pb;
    }

    /**
     * Creates a new Invoice record
     * 
     * @param invoice
     *            A InvoiceBean containing patient invoice details.
     * @return The Invoice Id for the new record <br>
     *         0 - if the method fails
     */

    public int setInvoiceDetails(InvoiceBean invoice) {

        try {

                       
            InvoiceBean pf = new InvoiceBean();
               
            pf.updateInvoice(invoice);
            em.persist(pf);
            return (pf.getId());
            
        } catch (Exception e) {
            Rlog.fatal("Invoice",  "Error in setInvoiceDetails() in InvoiceAgentBean " + e);
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * Updates the Invoice record with data passed in InvoiceBean object 
     * 
     * @param clsk
     *            An InvoiceBean containing category attributes to be
     *            updated.
     * @return 0 - if the update is successful<br>
     *         -2 - if the update fails
     */

    public int updateInvoice(InvoiceBean pb) {

        InvoiceBean retrieved = null; 
        int output;
        
        try {

            retrieved = (InvoiceBean) em.find(InvoiceBean.class, new Integer(pb.getId()));

            if (retrieved == null) {
                return -2;
            }
            
            output = retrieved.updateInvoice(pb);
            em.merge(retrieved);
        } catch (Exception e) {
            Rlog.debug("Invoice", "Error in updateInvoice in InvoiceAgentBean"
                            + e);
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    /**
     * Deletes an Invoice record 
     * @param id  The Invoice Id
     */
     
    public int  deleteInvoice(int id) {
        InvoiceBean pb = null;
        InvoiceDao invD = new InvoiceDao();
        
        try {

           pb = (InvoiceBean) em.find(InvoiceBean.class, new Integer(id));
           
           // check if the invoice has any dependencies
           if (! invD.hasDependencies(id))
           {
	           // delete invoice details
	           if ( invD.deleteInvoiceDetails(id) >= 0)
	           {
	        	   em.remove(pb);
	           }  
	           return 0;
           }
           else
           {
        	   
        	   return -3 ; // invoice has dependencies
           }
        } catch (Exception e) {
            Rlog.fatal("Invoice",
                    "Error in getInvoiceDetails() in InvoiceAgentBean" + e);
	 return -2;
        }
     
    }
    
    // Overloaded for INF-18183 ::: AGodara
    public int  deleteInvoice(int id,Hashtable<String, String> auditInfo) {
        
    	InvoiceBean pb = null;
        InvoiceDao invD = new InvoiceDao();
        AuditBean audit=null;
        
        try {

        	String currdate =DateUtil.getCurrentDate();
        	String userID=(String)auditInfo.get(AuditUtils.USER_ID_KEY); //Fetches the User ID from the Hashtable
            String ipAdd=(String)auditInfo.get(AuditUtils.IP_ADD_KEY); //Fetches the IP Address from the Hashtable
            String reason=(String)auditInfo.get(AuditUtils.REASON_FOR_DEL); //Fetches the reason for deletion from the Hashtable
            String app_module=(String)auditInfo.get(AuditUtils.APP_MODULE);// Fetches the APP_Module from the Hashtable
            String rid= AuditUtils.getRidValue("ER_INVOICE","eres","PK_INVOICE="+id); //Fetches the RID
            
            //POPULATE THE AUDIT BEAN 
            audit = new AuditBean("ER_INVOICE",String.valueOf(id),rid,userID,currdate,app_module,ipAdd,reason);
        	pb = (InvoiceBean) em.find(InvoiceBean.class, new Integer(id));
            // check if the invoice has any dependencies
        	if (! invD.hasDependencies(id))
        	{
        		// delete invoice details
        		if ( invD.deleteInvoiceDetails(id)== 0)
        		{
        			em.persist(audit); //PERSIST THE DATA IN AUDIT TABLE
        			em.remove(pb);
	           }  
	           return 0;
           }else{
        	   	return -3 ; // invoice has dependencies
           }
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("Invoice",
                    "Error in deleteInvoice(int id,Hashtable<String, String> auditInfo) in InvoiceAgentBean" + e);
            return -2;
        }
     
    }
    
    /**
     * gets saved invoices for a study
     * @param studyId  studyId
     */
     
    public InvoiceDao getSavedInvoices(int studyId) {
    	InvoiceDao  inv = new InvoiceDao (); 
        try {

        		inv.getSavedInvoices(studyId);
        		return inv ;
        		
        } catch (Exception e) {
            Rlog.fatal("Invoice",
                    "Exception in getSavedInvoices() in InvoiceAgentBean" + e);
	 return inv ;
        }
     
    }
    
    /**
     * Gets Saved Invoices and parameters to be used to apply pagination in invoice browser
     * Added By : Raviesh
     */
    public InvoiceDao getSavedInvoicesWithPagination(int studyId,String orderBy,String orderType,Map getRowsCount,int curPage) {
    	InvoiceDao  inv = new InvoiceDao (); 
        try {
        		inv.getSavedInvoicesWithPagination(studyId,orderBy,orderType, getRowsCount, curPage);
        		return inv ;
        		
        } catch (Exception e) {
            Rlog.fatal("Invoice",
                    "Exception in getSavedInvoicesWithPagination() in InvoiceAgentBean" + e);
            return inv ;
        }
     
    }
        
    public MileAchievedDao getAchievedMilestones(int studyId, String rangeFrom,String rangeTo, String milestoneReceivableStatus,String milestoneType, String site) 
    {
    	MileAchievedDao mile = new MileAchievedDao();
    	
    	try {

    		mile.getAchievedMilestones(studyId, rangeFrom,rangeTo, milestoneReceivableStatus,milestoneType,site);
    	
    		return mile;
    	} catch (Exception e) {
           Rlog.fatal("Invoice",
                    "Exception in getAchievedMilestones() in InvoiceAgentBean" + e);
           return mile ;
        }
    	
    }

    public MileAchievedDao  getAchievedMilestonesForUser(int studyId, String rangeFrom,String rangeTo, String milestoneReceivableStatus,String milestoneType, String user)
    {
    	MileAchievedDao mile = new MileAchievedDao();
    	
    	try {

    		mile.getAchievedMilestonesForUser(studyId, rangeFrom,rangeTo, milestoneReceivableStatus,milestoneType,user);
    	
    		return mile;
    	} catch (Exception e) {
           Rlog.fatal("Invoice",
                    "Exception in getAchievedMilestonesForUser() in InvoiceAgentBean" + e);
           return mile ;
        }
    	
    	
    }

    /**
     * Gets the total amount invoiced for an invoice
     * 
     * @param inv Invoice PK
     *            Description of the Parameter
     */
    public double getTotalAmountInvoiced(int inv) 
    {
    	InvoiceDao invDao = new InvoiceDao();
    	double ret = 0;
    	
    	try {

    		ret = invDao.getTotalAmountInvoiced(inv);
    	
    		return ret;
    	} catch (Exception e) {
           Rlog.fatal("Invoice",
                    "Exception in getTotalAmountInvoiced(int inv) in InvoiceAgentBean" + e);
           return ret ;
        }
    	
    	
    }
    
    /**
     * gets invoice autogenerated sequence number
     * @param 
     */
     
    public String getInvoiceNumber() {
    	InvoiceDao  inv = new InvoiceDao ();
    	String invNumber=null;
        try {

        	invNumber = inv.getInvoiceNumber();
        		return invNumber ;
        		
        } catch (Exception e) {
            Rlog.fatal("Invoice",
                    "Exception in getInvoiceNumber() in InvoiceAgentBean" + e);
	 return invNumber ;
        }
     
    }
    
    // Added for FIN-22378
    public MileAchievedDao getAchievedMilestonesNew(int studyId, String rangeFrom,String rangeTo, String milestoneReceivableStatus,String milestoneType, String site, String yearFilter, String monthFilter, String monthFilterYear,String dateRangeType) 
    {
    	MileAchievedDao mile = new MileAchievedDao();
    	
    	try {

    		mile.getAchievedMilestonesNew(studyId, rangeFrom,rangeTo, milestoneReceivableStatus,milestoneType,site,yearFilter,monthFilter,monthFilterYear,dateRangeType);
    	
    		return mile;
    	} catch (Exception e) {
           Rlog.fatal("Invoice",
                    "Exception in getAchievedMilestones() in InvoiceAgentBean" + e);
           return mile ;
        }
    	
    }
    
    // 	Added for FIN-22378
    public MileAchievedDao getAchievedMilestonesEM(int studyId, String rangeFrom, String rangeTo, String milestoneReceivableStatus, String site , String CovType, String yearFilter, String monthFilter, String monthFilterYear,String dateRangeType) 
    {
    	MileAchievedDao mile = new MileAchievedDao();
    	
    	try {
    		mile.getAchievedMilestonesEM(studyId, rangeFrom,rangeTo, milestoneReceivableStatus, site, CovType,yearFilter,monthFilter,monthFilterYear,dateRangeType);
    		return mile;
    	} catch (Exception e) {
           Rlog.fatal("Invoice",
                    "Exception in getAchievedMilestonesEM() in InvoiceAgentBean" + e);
           return mile ;
        }
    	
    }
    
    public MileAchievedDao getAchievedMilestonesAM(int studyId, String rangeFrom,String rangeTo, String milestoneReceivableStatus, String site, String CovType, String yearFilter, String monthFilter, String monthFilterYear,String dateRangeType)
    {
    	MileAchievedDao mile = new MileAchievedDao();
    	
    	try {
    		mile.getAchievedMilestonesAM(studyId, rangeFrom, rangeTo, milestoneReceivableStatus, site, CovType,yearFilter,monthFilter,monthFilterYear,dateRangeType);
    		return mile;
    	} catch (Exception e) {
           Rlog.fatal("Invoice",
                    "Exception in getAchievedMilestonesAM() in InvoiceAgentBean" + e);
           return mile ;
        }
    	
    }
	/**
	 * check invoice ID is existed or not 
	 * Added by Sudhir Shukla for FIN-22382_0926012 Enhancement
	 */
    public boolean getInvoiceNumber(String invNumber) {
    	InvoiceDao  inv = new InvoiceDao ();
    	boolean flag = false;
        try {

        	flag = inv.getInvoiceNumber(invNumber);
        		return flag ;
        		
        } catch (Exception e) {
            Rlog.fatal("Invoice",
                    "Exception in getInvoiceNumber(invNumber) in InvoiceAgentBean" + e);
	 return flag ;
        }
     
    }
    
    public int getInvoiceId(String invNumber) {
    	InvoiceDao  inv = new InvoiceDao ();
    	int invId = 0;
        try {

        	invId = inv.getInvoiceId(invNumber);
        		return invId ;
        		
        } catch (Exception e) {
            Rlog.fatal("Invoice",
                    "Exception in getInvoiceNumber(invNumber) in InvoiceAgentBean" + e);
	 return invId ;
        }
     
    }
 // Added for FIN-21239_09262012 ::: Sudhir Shukla
    public int  selectInvoiceReconciledOrNot(int id,Hashtable<String, String> auditInfo) {
        
    	InvoiceBean pb = null;
        InvoiceDao invD = new InvoiceDao();
        AuditBean audit=null;
        int returnVal =0;
        
        try {

        	String currdate =DateUtil.getCurrentDate();
        	String userID=(String)auditInfo.get(AuditUtils.USER_ID_KEY); //Fetches the User ID from the Hashtable
            String ipAdd=(String)auditInfo.get(AuditUtils.IP_ADD_KEY); //Fetches the IP Address from the Hashtable
            String reason=(String)auditInfo.get(AuditUtils.REASON_FOR_DEL); //Fetches the reason for deletion from the Hashtable
            String app_module=(String)auditInfo.get(AuditUtils.APP_MODULE);// Fetches the APP_Module from the Hashtable
            String rid= AuditUtils.getRidValue("ER_INVOICE","eres","PK_INVOICE="+id); //Fetches the RID
            
            //POPULATE THE AUDIT BEAN 
            audit = new AuditBean("ER_INVOICE",String.valueOf(id),rid,userID,currdate,app_module,ipAdd,reason);
        	pb = (InvoiceBean) em.find(InvoiceBean.class, new Integer(id));
            // check if the invoice has any dependencies
        	if ( invD.hasDependencies(id))
        //	{        		
        		returnVal= -3 ; // invoice has dependencies
        	//}
        	
//        	if (! invD.hasDependencies(id))
//        	{
//        		// delete invoice details
//        		if ( invD.deleteInvoiceDetails(id)== 0)
//        		{
//        			em.persist(audit); //PERSIST THE DATA IN AUDIT TABLE
//        			em.remove(pb);
//	           }  
//	           return 0;
//           }else{
//        	   	return -3 ; // invoice has dependencies
//           }
        	return returnVal;
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("Invoice",
                    "Error in selectInvoiceReconciledOrNot(int id,Hashtable<String, String> auditInfo) in InvoiceAgentBean" + e);
            return -2;
        }
     
    }
    /**
     * gets invoice autogenerated sequence number
     * @param 
     */
     
    public int getMaxInvoiceNumber(int studyID) {
    	InvoiceDao  inv = new InvoiceDao ();
    	int invNumber=0;
        try {

        	invNumber = inv.getMaxInvoiceNumber(studyID);
        		return invNumber ;
        		
        } catch (Exception e) {
            Rlog.fatal("Invoice",
                    "Exception in getMaxInvoiceNumber() in InvoiceAgentBean" + e);
	 return invNumber ;
        }
     
    }
}// end of class

