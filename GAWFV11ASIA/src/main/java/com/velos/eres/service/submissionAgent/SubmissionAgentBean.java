package com.velos.eres.service.submissionAgent;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.business.common.EIRBDao;
import com.velos.eres.business.submission.impl.SubmissionBean;
import com.velos.eres.service.util.Rlog;

@Stateless
@Remote( { SubmissionAgent.class } )
public class SubmissionAgentBean implements SubmissionAgent {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    public SubmissionBean getSubmissionDetails(int id) {
        if (id == 0) {
            return null;
        }
        SubmissionBean submissionBean = null;
        try {
            submissionBean = (SubmissionBean) em.find(SubmissionBean.class, new Integer(id));
        } catch (Exception e) {
            Rlog.fatal("submission", "Exception in SubmissionAgentBean.getSubmssionDetails "+e);
            return submissionBean;
        }

        return submissionBean;
    }

    public Integer updateSubmissionByFkStudy(SubmissionBean submissionBean) {
        Integer output = 0;
        Query query = em.createNamedQuery("findCurrentByFkStudy");
        query.setParameter("fkStudy", submissionBean.getFkStudy());
        ArrayList list = (ArrayList) query.getResultList();
        if (list == null || list.size() == 0) { return 0; }
        SubmissionBean bean = (SubmissionBean)list.get(0);
        try {
            if (submissionBean.getSubmissionFlag() != null) {
                bean.setSubmissionFlag(submissionBean.getSubmissionFlag());
            }
            if (submissionBean.getSubmissionStatus() != null &&
                    submissionBean.getSubmissionStatus() > 0) {
                bean.setSubmissionStatus(submissionBean.getSubmissionStatus());
            }
            if (submissionBean.getSubmissionType() != null &&
                    submissionBean.getSubmissionType() > 0) {
                bean.setSubmissionType(submissionBean.getSubmissionType());
            }
            bean.setLastModifiedBy(submissionBean.getLastModifiedBy());
            bean.setLastModifiedDate(Calendar.getInstance().getTime());
            em.merge(bean);
            output = bean.getId();
        } catch (Exception e) {
            Rlog.fatal("submission", "Exception in SubmissionAgentBean.updateSubmissionByFkStudy "+e);
            output = -1;
        }
        return output;
    }
    
    public Integer getExistingSubmissionByFkStudy(Integer fkStudy) {
        Integer output = 0;
        Query query = em.createNamedQuery("findCurrentByFkStudy");
        query.setParameter("fkStudy", fkStudy);
        ArrayList list = (ArrayList) query.getResultList();
        if (list == null || list.size() == 0) { return 0; }
        SubmissionBean bean = (SubmissionBean)list.get(0);
        output = bean.getId();
        return output;
    }
    
    public Integer getNewAmendSubmissionByFkStudy(Integer fkStudy) {
        Integer output = 0;
        Query query = em.createNamedQuery("findCurrentNewAmendByFkStudy");
        query.setParameter("fkStudy", fkStudy);
        CodeDao cDao = new CodeDao();
        query.setParameter("newAppId", cDao.getCodeId("submission", "new_app"));
        query.setParameter("studyAmendId", cDao.getCodeId("submission", "study_amend"));
        ArrayList list = (ArrayList) query.getResultList();
        if (list == null || list.size() == 0) { return 0; }
        SubmissionBean bean = (SubmissionBean)list.get(0);
        output = bean.getId();
        return output;
    }
    
    public Integer getExistingSubmissionByFkStudyAndFlag(Integer fkStudy, Integer submissionFlag) {
        Integer output = 0;
        Query query = em.createNamedQuery("findCurrentByFkStudyAndFlag");
        query.setParameter("fkStudy", fkStudy);
        query.setParameter("submissionFlag", submissionFlag);
        ArrayList list = (ArrayList) query.getResultList();
        if (list == null || list.size() == 0) { return 0; }
        SubmissionBean bean = (SubmissionBean)list.get(0);
        output = bean.getId();
        return output;
    }
    
    public Integer createSubmission(SubmissionBean submissionBean) {
        Integer output = 0;
        try {
            SubmissionBean newBean = new SubmissionBean();
            newBean.setDetails(submissionBean);
            newBean.setSubmissionStatus(getSubmissionStatus("submitted"));
            em.persist(newBean);
            output = newBean.getId();
        } catch(Exception e) {
            Rlog.fatal("submission", "Exception in SubmissionAgentBean.createSubmission "+e);
            output = -1;
        }
        return output;
    }
    
    public boolean getSubmissionAccess(String usrId, String submissionPK) {
        EIRBDao eIrbDao = new EIRBDao();
        return eIrbDao.getSubmissionAccess(usrId, submissionPK);
    }

    private Integer getSubmissionStatus(String subtype) {
        if (subtype == null) { return new Integer(0); }
        CodeDao codeDao = new CodeDao();
        codeDao.getCodeValues("subm_status");
        ArrayList codeSubtypes = codeDao.getCSubType();
        ArrayList codePks = codeDao.getCId();
        for (int iX=0; iX<codeSubtypes.size(); iX++) {
            if (subtype.equals(codeSubtypes.get(iX))) {
                return (Integer)codePks.get(iX);
            }
        }
        return new Integer(0);
    }
    
    public Integer updateAmendFlagSubmission(Integer pkSubmission, Integer amendType) {
        Integer output = 0;
        Query query = em.createNamedQuery("findSubmissionByPk");
        query.setParameter("pkSubmission", pkSubmission);
        ArrayList list = (ArrayList) query.getResultList();
        if (list == null || list.size() == 0) { return 0; }
        SubmissionBean bean = (SubmissionBean)list.get(0);
        try {
        	System.out.println("After change getSubmissionType:::"+bean.getSubmissionType());
        	System.out.println("After change amendType:::"+amendType);
            if (bean.getSubmissionType() != null &&
            		bean.getSubmissionType().equals(amendType)) {
            	System.out.println("Inside IF");
                //bean.setAmendFlag(0);
                bean.setLastModifiedDate(Calendar.getInstance().getTime());
               // System.out.println("Amend Flag:::::::"+bean.getAmendFlag());
            em.merge(bean);
           // System.out.println("After Setting Amend Flag:::::::"+bean.getAmendFlag());
            }
            output = bean.getId();
        } catch (Exception e) {
            Rlog.fatal("submission", "Exception in SubmissionAgentBean.updateSubmissionByFkStudy "+e);
            output = -1;
        }
        return output;
    }
    
    public String getSubmissionCreatedDate(int submissionId){
    	PreparedStatement pstmt = null;
        Connection conn = null;
        ResultSet rs = null;
        String createdOn = "";
        try {
            CommonDAO cmDao = new CommonDAO();
            conn = CommonDAO.getConnection();
            pstmt = conn
                    .prepareCall("select to_char(created_on,PKG_DATEUTIL.F_GET_DATETIMEFORMAT) ||','|| to_char(created_on,PKG_DATEUTIL.F_GET_DATEFORMAT) as CREATED_ON from er_submission where pk_submission=?");
            pstmt.setInt(1, submissionId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
            	createdOn = rs.getString("CREATED_ON");	
            }
        } catch(Exception e) {
        	Rlog.fatal("objectMap", "Exception in SubmissionAgentBean.getSubmissionCreatedDate:"+e);
        } finally {
        	try { if (rs != null) rs.close(); } catch (Exception e) {}
        	try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
        	try { if (conn != null) conn.close(); } catch (Exception e) {}
        }
    	return createdOn;
    }
    
    public String getSubmissionApprovedDate(int submissionId, int statusCode){
    	PreparedStatement pstmt = null;
        Connection conn = null;
        ResultSet rs = null;
        String createdOn = "";
        try {
            CommonDAO cmDao = new CommonDAO();
            conn = CommonDAO.getConnection();
            pstmt = conn
                    .prepareCall("select to_char(max(created_on),PKG_DATEUTIL.F_GET_DATETIMEFORMAT) as CREATED_ON from er_submission_status where fk_submission=?"+
                    			" and submission_status in (select pk_codelst from er_codelst where codelst_custom_col1='overall,final' and codelst_type='subm_status' and codelst_hide='N') and is_current=1");
            pstmt.setInt(1, submissionId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
            	if(rs.getString("CREATED_ON")!=null){
            		createdOn = rs.getString("CREATED_ON");
            	}
            }
            System.out.println("createdOn==="+createdOn);
        } catch(Exception e) {
        	Rlog.fatal("objectMap", "Exception in SubmissionAgentBean.getSubmissionApprovedDate:"+e);
        } finally {
        	try { if (rs != null) rs.close(); } catch (Exception e) {}
        	try { if (pstmt != null) pstmt.close(); } catch (Exception e) {}
        	try { if (conn != null) conn.close(); } catch (Exception e) {}
        }
    	return createdOn;
    }
}
