/**
 *@author Sonika Talwar 
 * date Aug 28, 2002
 *@version 1.0
 */

package com.velos.eres.service.perApndxAgent;

import java.util.Hashtable;

import javax.ejb.Remote;

import com.velos.eres.business.common.PerApndxDao;
import com.velos.eres.business.perApndx.impl.PerApndxBean;

@Remote
public interface PerApndxAgentRObj {
    public int setPerApndxDetails(PerApndxBean pask);

    public PerApndxBean getPerApndxDetails(int id);

    public int updatePerApndx(PerApndxBean pask);

    public int removePerApndx(int id);
 
    // Overloaded for INF-18183 AGodara
    public int removePerApndx(int id,Hashtable<String, String> auditInfo);
  
    public PerApndxDao getPerApndxFiles(int patId);

    public PerApndxDao getPerApndxUris(int patId);

}