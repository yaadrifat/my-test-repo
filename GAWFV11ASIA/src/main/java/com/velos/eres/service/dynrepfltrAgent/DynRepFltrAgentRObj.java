/*
 * Classname : DynRepAgentDtRObj
 * 
 * Version information: 1.0
 *
 * Date: 09/28/2003
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Vishal Abrol		
 */

package com.velos.eres.service.dynrepfltrAgent;

/* Import Statements */
import java.util.ArrayList;
import java.util.Hashtable;

import javax.ejb.Remote;

import com.velos.eres.business.dynrepfltr.impl.DynRepFltrBean;

/* End of Import Statements */

/**
 * Remote interface for dynrepdtAgent session EJB
 * 
 * @author Vishal Abrol
 * @version : 1.0 09/28/2003
 */

@Remote
public interface DynRepFltrAgentRObj {
    public DynRepFltrBean getDynRepFltrDetails(int id);

    public int setDynRepFltrDetails(DynRepFltrBean dsk);

    public int updateDynRepFltr(DynRepFltrBean dsk);
    
    public int removeDynRepFltr(int id);
    
    // removeDynRepFltr() Overloaded for INF-18183 ::: Raviesh
    public int removeDynRepFltr(int id,Hashtable<String, String> args);
    
    public int removeDynFilters(ArrayList Ids);
    
    //Overloaded for INF-18183 ::: Ankit
    public int removeDynFilters(ArrayList Ids, Hashtable<String, String> auditInfo);
}
