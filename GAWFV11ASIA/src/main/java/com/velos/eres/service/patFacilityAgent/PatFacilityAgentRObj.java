/*
 * Classname			PatFacilityAgentRObj
 * 
 * Version information : 1.0  
 *
 * Date					09/22/2005
 * 
 * Copyright notice : Velos Inc
 */

package com.velos.eres.service.patFacilityAgent;

import javax.ejb.Remote;

import com.velos.eres.business.common.PatFacilityDao;
import com.velos.eres.business.patFacility.impl.PatFacilityBean;


/**
 * Remote interface for PatFacilityAgentBean session EJB
 * 
 * @author Sonia Abrol
 * @version 1.0, 09/22/2005
 */
@Remote
public interface PatFacilityAgentRObj {
    public PatFacilityBean getPatFacilityDetails(int pb);

    public int setPatFacilityDetails(PatFacilityBean pb);

    public int updatePatFacility(PatFacilityBean pb);

    public int  deletePatFacility(int patFacilityId);
    
    public PatFacilityDao getPatientFacilities(int perPk);
    
    public PatFacilityDao getPatientFacilitiesSiteIds(int perPk);
    
    public int getUserFacilitySpecialtyAccessRight(int splId, int perPk) ;
    
}
