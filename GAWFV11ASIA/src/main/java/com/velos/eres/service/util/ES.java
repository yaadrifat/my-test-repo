package com.velos.eres.service.util;

import java.lang.reflect.Field;

/**
 * ES stands for "eSample". Class to hold all the key strings in esampleBundle.properties file.
 * Make all key strings public static.
 * Start each key with a category prefix, and arrange them alphabetically.
*/
public class ES {
	public static String getValueByKey(String key) {
		String messageText ="" ;
		synchronized(ES.class) {
			Field field;
			try {
				field = ES.class.getField(key);
				ES objectMC = new ES();

				try {
					Object value = field.get(objectMC);
					messageText = value.toString(); 
				} catch(Exception e) {
					System.out.println(e.getMessage());
				}
			} catch (SecurityException e1) {
				e1.printStackTrace();
			} catch (NoSuchFieldException e1) {
				e1.printStackTrace();
			}
		}
		return messageText;
	}

	public static void reload() {
		synchronized(ES.class) {
			Field[] fields = ES.class.getFields();
			for (Field field : fields) {
				try {
					if (field.getName() != null && field.getName().endsWith("_Upper")) {
						field.set(null, VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE,
								field.getName().replaceAll("_Upper", "")).toUpperCase());
					} else if (field.getName() != null && field.getName().endsWith("_Lower")) {  
						field.set(null, VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE,
								field.getName().replaceAll("_Lower", "")).toLowerCase());
					} else {
						field.set(null, VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE,field.getName()));
					}
				} catch(Exception e) {
					System.out.println(e.getMessage());
				}
			}
		}
	}

    public static String ES_StrtNew_Upload = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_StrtNew_Upload");
    public static String ES_Selc_File = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_Selc_File");
    public static String ES_PlsSelc_DotCSVFile = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_PlsSelc_DotCSVFile");
    public static String ES_Selc_Mapp = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_Selc_Mapp");
    public static String ES_Create_NewMAppt = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_Create_NewMAppt");
    public static String ES_ToRow_InFile = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_ToRow_InFile");
    public static String ES_UpldInPrgess = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_UpldInPrgess");
    public static String ES_ViewBlk_FldMapp = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_ViewBlk_FldMapp");
    public static String ES_InstBulk_UpldData = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_InstBulk_UpldData");
    public static String ES_BulkUpld_Det = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_BulkUpld_Det");
    public static String ES_View_Mapp = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_View_Mapp");
    public static String ES_VelosEsam_FldName = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_VelosEsam_FldName");
    public static String ES_File_FldNAme = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_File_FldNAme");
    public static String ES_Uplod = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_Uplod");
    public static String ES_Uplod_Data = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_Uplod_Data");
    public static String ES_Manage_MApp = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_Manage_MApp");
    public static String ES_PrevSaved_Mapp = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_PrevSaved_Mapp");
    public static String ES_MApp_NAme = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_MApp_NAme");
    public static String ES_Crted_By = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_Crted_By");
    public static String ES_BulkUpld_Area = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_BulkUpld_Area");
    public static String ES_StatNew_Uplod = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_StatNew_Uplod");
    public static String ES_Mang_MApp = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_Mang_MApp");
    public static String ES_Upld_Histy = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_Upld_Histy");
    public static String ES_DateOf_Upld = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_DateOf_Upld");
    public static String ES_Upld_By = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_Upld_By");
    public static String ES_Upld_Log = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_Upld_Log");
    public static String ES_Upld_Num = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_Upld_Num");
    public static String ES_TotFile_Rec = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_TotFile_Rec");
    public static String ES_RecUpld_Succ = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_RecUpld_Succ");
    public static String ES_RecNot_UpldSucc = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_RecNot_UpldSucc");
    public static String ES_Row_Num = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_Row_Num");
    public static String ES_Faiure_Reason = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_Faiure_Reason");
    public static String ES_BulkUpld_Mapp = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_BulkUpld_Mapp");
    public static String ES_MappName_AllExst = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_MappName_AllExst");
    public static String ES_Pls_Sel = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_Pls_Sel");
    public static String ES_Vald_Pass = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_Vald_Pass");
    public static String ES_EitherSelc_AutoSpec_OrMap = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_EitherSelc_AutoSpec_OrMap");
    public static String ES_FldName_Doesnt_Match = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_FldName_Doesnt_Match");
    public static String ES_PlsSelc_FileHeader = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_PlsSelc_FileHeader");
    public static String ES_FileHeaderUnq_ForSuppFld = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_FileHeaderUnq_ForSuppFld");
    public static String ES_PlsSelc_SuppFld = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_PlsSelc_SuppFld");
    public static String ES_PlsEntr_MappName = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_PlsEntr_MappName");
    public static String ES_WantToSave_Mapp = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_WantToSave_Mapp");
    public static String ES_Strt_NewUpld = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_Strt_NewUpld");
    public static String ES_Saved_Mapping = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_Saved_Mapping");
    public static String ES_Validate_Map = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_Validate_Map");
    public static String ES_PrevAndSave = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_PrevAndSave");
    public static String ES_FldMapp = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_FldMapp");
    public static String ES_FileFld_Name = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_FileFld_Name");
    public static String ES_SaveMApp_As = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_SaveMApp_As");
    public static String ES_NoMapping_Found = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_NoMapping_Found");
    public static String ES_AutoSpecId_NotProv_InFileOrMapp = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_AutoSpecId_NotProv_InFileOrMapp");
    public static String ES_SelcFile_IsEmpt = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_SelcFile_IsEmpt");
    public static String ES_PlsEntVald_CsvFile = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_PlsEntVald_CsvFile");
    public static String ES_PlsEnter_DotCsvFile = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_PlsEnter_DotCsvFile");
    public static String ES_Rows_Imported = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_Rows_Imported");
    public static String ES_Issue_Desc = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_Issue_Desc");
    public static String ES_BlkUpld_Prev = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_BlkUpld_Prev");
    public static String ES_ErrProc_TryAgn = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_ErrProc_TryAgn");
    public static String ES_Select_Data_File = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_Select_Data_File");
    public static String ES_Submit = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_Submit");
    public static String ES_DelSel = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_DelSel");
    public static String ES_Select_AnOption = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_Select_AnOption");
    public static String ES_Preview = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_Preview");
    public static String ES_Get_Def_Template = VelosResourceBundle.getString(VelosResourceBundle.ESAMPLE_BUNDLE, "ES_Get_Def_Template");
}
