package com.velos.eres.service.web;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import org.json.JSONObject;


import com.opensymphony.xwork2.ActionSupport;
import com.velos.eres.business.common.CommonDAO;

import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;


public class GetStudyStatusData extends ActionSupport{
	/**
	 * 
	 */

		private static final long serialVersionUID = 1L;
		private HttpServletRequest request = null;
		private ArrayList<HashMap<String,Object>> jsonData=null;
	    JSONObject   jsObj=new JSONObject();
	    
	    private ArrayList statStartDates;

	    private ArrayList statStartDatesPRC;
	    
	    private ArrayList statStartDatesCTRC;

	    
	    public ArrayList getStatStartDates() {
	        return this.statStartDates;
	    }

	    public void setStatStartDates(ArrayList statStartDates) {
	        this.statStartDates = statStartDates;
	    }

	    public ArrayList getStatStartDatesPRC() {
	    	return this.statStartDatesPRC;
	    }
	    
	    public void setStatStartDatesPRC(ArrayList statStartDatesPRC) {
	    	this.statStartDatesPRC = statStartDatesPRC;
	    }
	    
	    public ArrayList getStatStartDatesCTRC() {
	    	return this.statStartDatesCTRC;
	    }
	    
	    public void setStatStartDatesCTRC(ArrayList statStartDatesCTRC) {
	    	this.statStartDatesCTRC = statStartDatesCTRC;
	    }

	    public void setStatStartDates(String statStartDate) {
	        statStartDates.add(statStartDate);
	    }
	    
	    public String getStatStartDates(int idx) {
	    	if (statStartDates != null && idx < statStartDates.size())
	    	{
	    		return (String) statStartDates.get(idx);
	    	}
	    	else
	    	{
	    		return null;
	    	}
	    	
	    }

	    public void setStatStartDatesPRC(String statStartDate) {
	    	statStartDatesPRC.add(statStartDate);
	    }
	    
	    public String getStatStartDatesPRC(int idx) {
	    	if (statStartDatesPRC != null && idx < statStartDatesPRC.size())
	    	{
	    		return (String) statStartDatesPRC.get(idx);
	    	}
	    	else
	    	{
	    		return null;
	    	}
	    	
	    }

	    
	    public void setStatStartDatesCTRC(String statStartDate) {
	    	statStartDatesCTRC.add(statStartDate);
	    }
	    
	    public String getStatStartDatesCTRC(int idx) {
	    	if (statStartDatesCTRC != null && idx < statStartDatesCTRC.size())
	    	{
	    		return (String) statStartDatesCTRC.get(idx);
	    	}
	    	else
	    	{
	    		return null;
	    	}
	    	
	    }


  ///  public JSONObject jsObj = null;
	
	/**
	 * @return the jsonData
	 */
	public ArrayList<HashMap<String, Object>> getJsonData() {
		return jsonData;
	}

	/**
	 * @param jsonData the jsonData to set
	 */
	public void setJsonData(ArrayList<HashMap<String, Object>> jsonData) {
		this.jsonData = jsonData;
	}
	
	public GetStudyStatusData() {
		request = ServletActionContext.getRequest();
        statStartDates = new ArrayList();
        statStartDatesPRC = new ArrayList();
        statStartDatesCTRC = new ArrayList();
	}
	
	public String getStudyStatusData(){

		String studyID=request.getParameter("studyID");
		int StudyID1 = Integer.parseInt(studyID);
		ArrayList<HashMap<String, Object>> argMap=new  ArrayList<HashMap<String, Object>>();
		HashMap<String, Object> argHMap = new HashMap<String, Object>();
		String userId = null;
		
		try {
			userId = (String)request.getSession(false).getAttribute("userId");
			}
		finally {
			userId = StringUtil.trueValue(userId);
			}
		int userID = Integer.parseInt(userId);
		getStudy(StudyID1);
		ArrayList<String>  studystatusdatePRC=new ArrayList<String>();			  
		ArrayList<String>  studystatusdateCTRC=new ArrayList<String>();			  
		studystatusdatePRC=getStatStartDatesPRC();
		studystatusdateCTRC=getStatStartDatesCTRC();
				 
		if(null != studystatusdatePRC.get(0)){
		String ctrc_kac_PRCdate =studystatusdatePRC.get(0).substring(0,10);
		String prcDate;
		prcDate = DateUtil.format2DateFormat(ctrc_kac_PRCdate);
		ctrc_kac_PRCdate = prcDate;
		argHMap.put("ctrc_kac_PRCdate", ctrc_kac_PRCdate);
		}else{
		argHMap.put("ctrc_kac_PRCdate", "d1null");
		}
		
		if(null != studystatusdateCTRC.get(0)){
			String ctrc_kac_oandldate = studystatusdateCTRC.get(0).substring(0,10);
			String oandlDate;
			oandlDate = DateUtil.format2DateFormat(ctrc_kac_oandldate);
			ctrc_kac_oandldate = oandlDate;
			argHMap.put("ctrc_kac_oandldate", ctrc_kac_oandldate);   
		}else{
		argHMap.put("ctrc_kac_oandldate", "d2null");
		}
		argMap.add(argHMap);
		setJsonData(argMap);
		
	return SUCCESS;
	}
	
	   public void getStudy(int study) {

	        PreparedStatement pstmt = null;
	        PreparedStatement pstmtPRC = null;
	        PreparedStatement pstmtCTRC = null;
	        Connection conn = null;
	        try {
	        	CommonDAO cd = new CommonDAO();
	            conn = cd.getConnection();
	            Rlog.debug("studystatus", "got connection" + conn);
	            String mysql = "SELECT PK_STUDYSTAT,"
	                    + "FK_STUDY," + "STUDYSTAT_DATE"
	                    + " FROM ER_STUDYSTAT WHERE FK_STUDY =?";

	            String mysqlCustom = " Select max(STUDYSTAT_DATE) as StudyStat_Date "
	         	   + " from ER_CODELST ERC," + " ER_STUDY ERS, " + " ER_STUDYSTAT ERST "
	               + " WHERE ERS.PK_STUDY=ERST.FK_STUDY "
	               + " AND ERST.FK_CODELST_STUDYSTAT=ERC.PK_CODELST "
	               + " AND ers.pk_study = ?" ;
	            
	            String mysqlPRC = mysqlCustom + " and  erc.codelst_subtyp = 'app_PRC' ";
	            String mysqlCTRC = mysqlCustom + " and  erc.codelst_subtyp = 'CTRC_Operations' ";
	            Rlog.debug("studystatus", mysql);
	            pstmt = conn.prepareStatement(mysql);
	            pstmt.setInt(1, study);
	            ResultSet rs = pstmt.executeQuery();
	            Rlog.debug("studystatus", "got resultset" + rs);

	            while (rs.next()) {
	                setStatStartDates(rs.getString("STUDYSTAT_DATE"));
	            }
	//START
	// This code is only intended for fetching the custom Study Statuses.
	            pstmtPRC = conn.prepareStatement(mysqlPRC);
	            pstmtPRC.setInt(1, study);
	            ResultSet rsPRC = pstmtPRC.executeQuery();
	            while (rsPRC.next()) {
	           		setStatStartDatesPRC(rsPRC.getString("STUDYSTAT_DATE"));
	            }

	            pstmtCTRC = conn.prepareStatement(mysqlCTRC);
	            pstmtCTRC.setInt(1, study);
	            ResultSet rsCTRC = pstmtCTRC.executeQuery();
	            while (rsCTRC.next()) {
	            	setStatStartDatesCTRC(rsCTRC.getString("STUDYSTAT_DATE"));
	            }
	            Rlog.debug("studystatus", "StudyStatusDao.rows ");
	//END
	        } catch (SQLException ex) {
	            Rlog.fatal("studystatus",
	                    "studystatusDao.getMsgcntr EXCEPTION IN FETCHING FROM er_studystat"
	                            + ex);
	        } finally {
	            try {
	                if ((pstmt != null)||(pstmtPRC != null)||(pstmtCTRC != null)){
	                    pstmt.close();
	                    pstmtPRC.close();
	                    pstmtCTRC.close();
	                    }
	            } catch (Exception e) {
	            }
	            try {
	                if (conn != null){
	                    conn.close();
	                    }
	            } catch (Exception e) {
	            }
	        }
	    }
	   public String updateEcompStat() {
		   String study=request.getParameter("study");
		   String loginuser=request.getParameter("loginUser");
		   String checkedFlag =request.getParameter("checkflag");
		   String pksubmissionstatus =request.getParameter("pksubmissionstatus");
		   int loginUser = Integer.parseInt(loginuser);
		   int studyId = Integer.parseInt(study);
		   int pkstatus=0;
		   if (checkedFlag==null){
	        	 checkedFlag ="";
	        	 }
		   if(!checkedFlag.equalsIgnoreCase("")){
		    pkstatus = Integer.parseInt(pksubmissionstatus);
		   }
		     PreparedStatement pstmt = null;
		     Connection conn = null;
		     String updateuser = "";
		     try {
		    	 CommonDAO cd = new CommonDAO();
		    	 conn = cd.getConnection();
		         StringBuffer sql = new StringBuffer(); 
		         StringBuffer sql1 = new StringBuffer();
		         StringBuffer sql2 = new StringBuffer();
		         StringBuffer sql3 = new StringBuffer();
		         StringBuffer sql4 = new StringBuffer();
		         StringBuffer sql5 = new StringBuffer();
		         if (checkedFlag==null){
		        	 checkedFlag ="";
		        	 }
		         if(checkedFlag.equalsIgnoreCase("")){
		         sql.append("select USER_CHECKED from er_status_history where status_modpk=? and STATUS_MODTABLE='er_study' and STATUS_ISCURRENT=1");
		         pstmt = conn.prepareStatement(sql.toString());
		         pstmt.setInt(1, studyId);
		         ResultSet rs = pstmt.executeQuery();
		         if(rs.next()){
		        	 updateuser = rs.getString("USER_CHECKED");
		         }
		         if(updateuser==null){
		        	 updateuser= "";
		         }else{
		        	 updateuser+=",";
		         }
		         updateuser+= loginuser; 
		         
		         sql1.append(" update er_status_history ");
		         sql1.append("set USER_CHECKED = ? where STATUS_MODPK = ? and STATUS_MODTABLE = 'er_study' and STATUS_ISCURRENT=1  ");
		         
		         pstmt = conn.prepareStatement(sql1.toString());
		         pstmt.setString(1, updateuser);
		         pstmt.setInt(2, studyId);
		         ResultSet rs1 = pstmt.executeQuery();
		         
		         //By Aakriti Maggo
		         sql2.append(" update er_submission_status ");
		         sql2.append("set USER_CHECKED = USER_CHECKED||',"+loginuser+"' where fk_submission in (select pk_submission from er_submission where fk_study=?) and FK_SUBMISSION_BOARD is not null and USER_CHECKED is not null AND USER_CHECKED NOT LIKE '%"+loginuser+"%' ");
		         pstmt = conn.prepareStatement(sql2.toString());
		         //pstmt.setString(1, USER_CHECKED||updateuser);
		         pstmt.setInt(1, studyId);
		         //pstmt.setString(2, loginuser);
		         ResultSet rs2 = pstmt.executeQuery();
		         
		         sql5.append(" update er_submission_status ");
		         sql5.append("set USER_CHECKED = '"+loginuser+"' where fk_submission in (select pk_submission from er_submission where fk_study=?) and FK_SUBMISSION_BOARD is not null and USER_CHECKED is null");
		         pstmt = conn.prepareStatement(sql5.toString());
		         pstmt.setInt(1, studyId);
		         ResultSet rs3 = pstmt.executeQuery();
		         }
		         else{
		        	 sql3.append("select user_checked from er_submission_status where pk_submission_status = ?");
		        	 pstmt = conn.prepareStatement(sql3.toString());
		        	 pstmt.setInt(1, pkstatus);
		        	 ResultSet rs3 = pstmt.executeQuery();
		        	 if(rs3.next()){
			        	 updateuser = rs3.getString("USER_CHECKED");
			         }
		        	 if(updateuser==null){
			        	 updateuser= "";
			         }else{
			        	 updateuser+=",";
			         }
			         updateuser+= loginuser; 
		        	 sql4.append(" update er_submission_status ");
			         sql4.append("set USER_CHECKED = ? where pk_submission_status = ?");
			         
			         pstmt = conn.prepareStatement(sql4.toString());
			         pstmt.setString(1, updateuser);
			         pstmt.setInt(2, pkstatus);
			         ResultSet rs4 = pstmt.executeQuery();
			         	 
		        	 
		         }} catch (SQLException e) {
		         Rlog.fatal("EIRBDao", "Exeception in updateEcompStat() "+e);
		     } finally {
		         try {
		             if (pstmt != null) pstmt.close();
		         } catch (Exception e) {}
		         try {
		             if (conn != null) conn.close();
		         } catch (Exception e) {}
		     }
		     return "success";
		 }
	   public String afterDelUpdateEcompStat() {
		   String study=request.getParameter("study");
		   String loginuser=request.getParameter("loginUser");
		   String checkedFlag =request.getParameter("checkflag");
		   int pkstatus = 0;
		   if (checkedFlag==null){
	        	 checkedFlag ="";
	        	 }
		   String pksubmissionstatus =request.getParameter("pksubmissionstatus");
		   if(!checkedFlag.equalsIgnoreCase("")){
		   pkstatus = Integer.parseInt(pksubmissionstatus);
		   }
		   int loginUser = Integer.parseInt(loginuser);
		   int studyId = Integer.parseInt(study);
		   
		     PreparedStatement pstmt = null;
		     Connection conn = null;
		     String updateuser = "";
		     int count=0;
		     try {
		    	 CommonDAO cd = new CommonDAO();
		    	 conn = cd.getConnection();
		         StringBuffer sql = new StringBuffer(); 
		         StringBuffer sql1 = new StringBuffer();
		         StringBuffer sql2 = new StringBuffer();
		         StringBuffer sql3 = new StringBuffer();
		         StringBuffer sql4 = new StringBuffer();
		         if(checkedFlag.equalsIgnoreCase("")){
		         sql.append("select USER_CHECKED from er_status_history where status_modpk=? and STATUS_MODTABLE='er_study' and STATUS_ISCURRENT=1");
		         pstmt = conn.prepareStatement(sql.toString());
		         pstmt.setInt(1, studyId);
		         ResultSet rs = pstmt.executeQuery();
		         if(rs.next()){
		        	 updateuser = rs.getString("USER_CHECKED");
		         }
		         String rmv = Pattern.quote(loginuser);
		         Pattern p = Pattern.compile("^" + rmv + "$" +     // matches only value
		                                    "|^" + rmv + "," +     // matches first value + ','
		                                    "|," + rmv + "$" +     // matches ',' + last value
		                                    "|," + rmv + "(?=,)"); // matches ',' + middle value (+ ',')
		         String updateuserchecked = p.matcher(updateuser).replaceAll("");
		        System.out.println("updateuserchecked = " +updateuserchecked);
		         //updateuser+= loginuser; 
		         
		         sql1.append(" update er_status_history ");
		         sql1.append("set USER_CHECKED = ? where STATUS_MODPK = ? and STATUS_MODTABLE = 'er_study' and STATUS_ISCURRENT=1  ");
		         
		         pstmt = conn.prepareStatement(sql1.toString());
		         pstmt.setString(1, updateuserchecked);
		         pstmt.setInt(2, studyId);
		         ResultSet rs1 = pstmt.executeQuery();
		         
		         sql4.append("select pk_submission_status, USER_CHECKED from er_submission_status where fk_submission in (select pk_submission from er_submission where fk_study=?) and FK_SUBMISSION_BOARD is not null ");
		         pstmt = conn.prepareStatement(sql4.toString());
		         pstmt.setInt(1, studyId);
		         ResultSet rs4 = pstmt.executeQuery();
		         while(rs4.next()){
		        	 updateuser = rs4.getString("USER_CHECKED");
		        	 int pk_status=rs4.getInt("pk_submission_status");
		         String rmv1 = Pattern.quote(loginuser);
		         Pattern p1 = Pattern.compile("^" + rmv1 + "$" +     // matches only value
		                                    "|^" + rmv1 + "," +     // matches first value + ','
		                                    "|," + rmv1 + "$" +     // matches ',' + last value
		                                    "|," + rmv1 + "(?=,)"); // matches ',' + middle value (+ ',')
		         String updateuserchecked1 = p1.matcher(updateuser).replaceAll("");
		        System.out.println("updateuserchecked = " +updateuserchecked1);
		         //updateuser+= loginuser; 
		        
		         //By Aakriti Maggo
		       
		         sql2.append(" update er_submission_status ");
		         sql2.append("set USER_CHECKED = ? where pk_submission_status=? ");
		         
		         pstmt = conn.prepareStatement(sql2.toString());
		         pstmt.setString(1, updateuserchecked1);
		         pstmt.setInt(2, pk_status);
		         ResultSet rs2 = pstmt.executeQuery();
		         sql2.setLength(0);
		         }}
		         
		         
		         else{
		        	 
		        	 sql3.append("select user_checked from er_submission_status where pk_submission_status = ?");
		        	 pstmt = conn.prepareStatement(sql3.toString());
		        	 pstmt.setInt(1, pkstatus);
		        	 ResultSet rs3 = pstmt.executeQuery();
		        	 if(rs3.next()){
			        	 updateuser = rs3.getString("USER_CHECKED");
			        	 
			         }
		        	 String rmv = Pattern.quote(loginuser);
			         Pattern p = Pattern.compile("^" + rmv + "$" +     // matches only value
			                                    "|^" + rmv + "," +     // matches first value + ','
			                                    "|," + rmv + "$" +     // matches ',' + last value
			                                    "|," + rmv + "(?=,)"); // matches ',' + middle value (+ ',')
			         String updateuserchecked = p.matcher(updateuser).replaceAll("");
		        	 sql4.append(" update er_submission_status ");
			         sql4.append("set USER_CHECKED = ? where pk_submission_status = ?");
			         
			         pstmt = conn.prepareStatement(sql4.toString());
			         pstmt.setString(1, updateuserchecked);
			         pstmt.setInt(2, pkstatus);
			         ResultSet rs4 = pstmt.executeQuery();	        
	 
		         }
		         
		     } catch (SQLException e) {
		         Rlog.fatal("EIRBDao", "Exeception in afterDelUpdateEcompStat() "+e);
		     } finally {
		         try {
		             if (pstmt != null) pstmt.close();
		         } catch (Exception e) {}
		         try {
		             if (conn != null) conn.close();
		         } catch (Exception e) {}
		     }
		     return "success";
		 }
	
}