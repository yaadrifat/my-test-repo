package com.velos.eres.service.submissionAgent;

import java.util.ArrayList;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.velos.eres.business.submission.impl.SubmissionStatusBean;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.business.common.*;

@Stateless
@Remote( { SubmissionStatusAgent.class } )
public class SubmissionStatusAgentBean implements SubmissionStatusAgent {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    public SubmissionStatusBean getSubmissionStatusDetails(int id) {
        try {
            return (SubmissionStatusBean)em.find(SubmissionStatusBean.class, id);
        } catch(Exception e) {
            Rlog.fatal("submission",
                    "Exception in getSubmissionStatusDetails() in SubmissionStatusAgentBean"
                            + e);
            return null;
        }
    }

    public Integer createSubmissionStatus(SubmissionStatusBean submissionStatusBean) {
        Integer output = 0;
        try {
            SubmissionStatusBean newBean = new SubmissionStatusBean();
            newBean.setDetails(submissionStatusBean);
            em.persist(newBean);
            output = newBean.getId();
        } catch(Exception e) {
            Rlog.fatal("submission", "Exception in SubmissionStatusAgentBean.createSubmissionStatus "+e);
            output = -1;
        }
        return output;
    }
    
    public Integer setCurrentToOld(SubmissionStatusBean submissionStatusBean) {
        Integer output = 0;
        Query query = null;
        if (submissionStatusBean.getFkSubmissionBoard() == null) {
            query = em.createNamedQuery("findCurrentByFkSubmission");
            query.setParameter("fkSubmission", submissionStatusBean.getFkSubmission());
        } else {
            query = em.createNamedQuery("findCurrentByFkSubmissionAndBoard");
            query.setParameter("fkSubmission", submissionStatusBean.getFkSubmission());
            query.setParameter("fkSubmissionBoard", submissionStatusBean.getFkSubmissionBoard());
        }
        ArrayList list = (ArrayList) query.getResultList();
        if (list == null || list.size() == 0) { return 0; }
        for (int iX=0; iX<list.size(); iX++) {
            try {
                SubmissionStatusBean bean = (SubmissionStatusBean)list.get(iX);
                // System.out.println("Found bean "+bean.getId());
                bean.setIsCurrent(0);
                em.merge(bean);
            } catch(Exception e) {
                Rlog.fatal("submission", "Exception in SubmissionStatusAgentBean.setCurrentToOld "+e);
                output = -1;
                break;
            }
        }
        return output;
    }


    /** Gets the one status previous from the current status for a submission board and submission. Returns the Pk of the codelst
     * */
     public String getPreviousCurrentSubmissionStatus(int submissionPK, int submissionBoardPK)
     {
    	 String retStatus = "";
     	
     	try{
     		retStatus= EIRBDao.getPreviousCurrentSubmissionStatus(submissionPK,submissionBoardPK);
     	}
     	 catch (Exception e) {
              Rlog.fatal("submission", "Exception in SubmissionAgentBean.getPreviousCurrentSubmissionStatus "+e);
              retStatus = "";
               
          }
     	return retStatus;
    	 
     }
     
}