package com.velos.eres.service.util;



/**
 *
 *
 * This class is to be used as a central repository to store the
 *
 *
 * Internal JNDI names of various components. Any sort of change
 *
 *
 * at this location should also reflect in the Deployment Descriptor
 *
 *
 */

public class JNDINames {

    /* JNDI names of EJB home objects */

	public final static String prefix = "velos/";
	public final static String suffix = "/remote";
	
	public final static String WorkflowAgentHome = "ejb:velos/velos-ejb-eres/WorkflowAgentBean!com.velos.eres.gems.service.WorkflowAgentRObj";
	public final static String WorkflowInstanceAgentHome = "ejb:velos/velos-ejb-eres/WorkflowInstanceAgentBean!com.velos.eres.gems.service.WorkflowInstanceAgentRObj";
	public final static String AccountAgentHome = "ejb:velos/velos-ejb-eres/AccountAgentBean!com.velos.eres.service.accountAgent.AccountAgentRObj";
	public final static String AccountWrapperAgentHome = "ejb:velos/velos-ejb-eres/AccountWrapperAgentBean!com.velos.eres.service.accountWrapperAgent.AccountWrapperAgentRObj";
	public final static String AddressAgentHome = "ejb:velos/velos-ejb-eres/AddressAgentBean!com.velos.eres.service.addressAgent.AddressAgentRObj";
	public final static String AppendixAgentHome = "ejb:velos/velos-ejb-eres/AppendixAgentBean!com.velos.eres.service.appendixAgent.AppendixAgentRObj";
	public final static String BrowserRowsAgentHome = "ejb:velos/velos-ejb-eres/BrowserRowsAgentBean!com.velos.eres.service.browserRowsAgent.BrowserRowsAgentRObj";
	public final static String CatLibAgentHome = "ejb:velos/velos-ejb-eres/CatLibAgentBean!com.velos.eres.service.catLibAgent.CatLibAgentRObj";
	public final static String CodelstAgentHome = "ejb:velos/velos-ejb-eres/CodelstAgentBean!com.velos.eres.service.codelstAgent.CodelstAgentRObj";
	public final static String CommonAgentHome = "ejb:velos/velos-ejb-eres/CommonAgentBean!com.velos.eres.service.commonAgent.CommonAgentRObj";
	public final static String CtrltabAgentHome = "ejb:velos/velos-ejb-eres/CtrltabAgentBean!com.velos.eres.service.ctrltabAgent.CtrltabAgentRObj";
	public final static String DynRepAgentHome = "ejb:velos/velos-ejb-eres/DynRepAgentBean!com.velos.eres.service.dynrepAgent.DynRepAgentRObj";
	public final static String DynRepDtAgentHome = "ejb:velos/velos-ejb-eres/DynRepDtAgentBean!com.velos.eres.service.dynrepdtAgent.DynRepDtAgentRObj";
	public final static String DynRepFltrAgentHome = "ejb:velos/velos-ejb-eres/DynRepFltrAgentBean!com.velos.eres.service.dynrepfltrAgent.DynRepFltrAgentRObj";
	public final static String EschReportsAgentHome = prefix+"EschReportsAgentBean"+suffix;
	public final static String ExtURightsAgentHome = "ejb:velos/velos-ejb-eres/ExtURightsAgentBean!com.velos.eres.service.extURightsAgent.ExtURightsAgentRObj";
	public final static String FieldLibAgentHome = "ejb:velos/velos-ejb-eres/FieldLibAgentBean!com.velos.eres.service.fieldLibAgent.FieldLibAgentRObj";
	public final static String FldRespAgentHome = "ejb:velos/velos-ejb-eres/FldRespAgentBean!com.velos.eres.service.fldRespAgent.FldRespAgentRObj";
	public final static String FldValidateAgentHome = "ejb:velos/velos-ejb-eres/FldValidateAgentBean!com.velos.eres.service.fldValidateAgent.FldValidateAgentRObj";
	public final static String FormActionAgentHome = "ejb:velos/velos-ejb-eres/FormActionAgentBean!com.velos.eres.service.formActionAgent.FormActionAgentRObj";
	public final static String FormFieldAgentHome = "ejb:velos/velos-ejb-eres/FormFieldAgentBean!com.velos.eres.service.formFieldAgent.FormFieldAgentRObj";
	public final static String FormLibAgentHome = "ejb:velos/velos-ejb-eres/FormLibAgentBean!com.velos.eres.service.formLibAgent.FormLibAgentRObj";
	public final static String FormNotifyAgentHome = "ejb:velos/velos-ejb-eres/FormNotifyAgentBean!com.velos.eres.service.formNotifyAgent.FormNotifyAgentRObj";
	public final static String FormQueryAgentHome = "ejb:velos/velos-ejb-eres/FormQueryAgentBean!com.velos.eres.service.formQueryAgent.FormQueryAgentRObj";
	public final static String FormQueryStatusAgentHome = "ejb:velos/velos-ejb-eres/FormQueryStatusAgentBean!com.velos.eres.service.formQueryStatusAgent.FormQueryStatusAgentRObj";
	public final static String FormSecAgentHome = "ejb:velos/velos-ejb-eres/FormSecAgentBean!com.velos.eres.service.formSecAgent.FormSecAgentRObj";
	public final static String FormStatAgentHome = "ejb:velos/velos-ejb-eres/FormStatAgentBean!com.velos.eres.service.formStatAgent.FormStatAgentRObj";
	public final static String GroupAgentHome = "ejb:velos/velos-ejb-eres/GroupAgentBean!com.velos.eres.service.groupAgent.GroupAgentRObj";
	public final static String GrpRightsAgentHome = "ejb:velos/velos-ejb-eres/GrpRightsAgentBean!com.velos.eres.service.grpRightsAgent.GrpRightsAgentRObj";
	public final static String GrpSupUserRigthtsAgentHome = prefix+"GrpSupUserRightsAgentBean"+suffix;
	public final static String InvoiceAgentHome = "ejb:velos/velos-ejb-eres/InvoiceAgentBean!com.velos.eres.service.invoiceAgent.InvoiceAgentRObj";
	public final static String InvoiceDetailAgentHome = "ejb:velos/velos-ejb-eres/InvoiceDetailAgentBean!com.velos.eres.service.invoiceAgent.InvoiceDetailAgentRObj";
	public final static String LabAgentHome = "ejb:velos/velos-ejb-eres/LabAgentBean!com.velos.eres.service.labAgent.LabAgentRObj";
	public final static String LinkedFormsAgentHome = "ejb:velos/velos-ejb-eres/LinkedFormsAgentBean!com.velos.eres.service.linkedFormsAgent.LinkedFormsAgentRObj";
	public final static String MileApndxAgentHome = "ejb:velos/velos-ejb-eres/MileApndxAgentBean!com.velos.eres.service.mileApndxAgent.MileApndxAgentRObj";
	public final static String MilepaymentAgentHome = "ejb:velos/velos-ejb-eres/MilepaymentAgentBean!com.velos.eres.service.milepaymentAgent.MilepaymentAgentRObj";
	public final static String MilestoneAgentHome = "ejb:velos/velos-ejb-eres/MilestoneAgentBean!com.velos.eres.service.milestoneAgent.MilestoneAgentRObj";
	public final static String MoreDetailsAgentHome = "ejb:velos/velos-ejb-eres/MoreDetailsAgentBean!com.velos.eres.service.moreDetailsAgent.MoreDetailsAgentRObj";
	public final static String MsgcntrAgentHome = "ejb:velos/velos-ejb-eres/MsgcntrAgentBean!com.velos.eres.service.msgcntrAgent.MsgcntrAgentRObj";
	public final static String PatFacilityAgentHome = "ejb:velos/velos-ejb-eres/PatFacilityAgentBean!com.velos.eres.service.patFacilityAgent.PatFacilityAgentRObj";
	public final static String PatLoginAgentHome = "ejb:velos/velos-ejb-eres/PatLoginAgentBean!com.velos.eres.service.patLoginAgent.PatLoginAgentRObj";
	public final static String PatProtAgentHome = "ejb:velos/velos-ejb-eres/PatProtAgentBean!com.velos.eres.service.patProtAgent.PatProtAgentRObj";
	public final static String PatStudyStatAgentHome = "ejb:velos/velos-ejb-eres/PatStudyStatAgentBean!com.velos.eres.service.patStudyStatAgent.PatStudyStatAgentRObj";
	public final static String PatTXArmAgentHome = "ejb:velos/velos-ejb-eres/PatTXArmAgentBean!com.velos.eres.service.patTXArmAgent.PatTXArmAgentRObj";
	public final static String PaymentDetailAgentHome = "ejb:velos/velos-ejb-eres/PaymentDetailAgentBean!com.velos.eres.service.milepaymentAgent.PaymentDetailAgentRObj";
	public final static String PerApndxAgentHome = "ejb:velos/velos-ejb-epat/PerApndxAgentBean!com.velos.eres.service.perApndxAgent.PerApndxAgentRObj";
	public final static String PerIdAgentHome = "ejb:velos/velos-ejb-epat/PerIdAgentBean!com.velos.eres.service.perIdAgent.PerIdAgentRObj";
	public final static String PersonAgentHome = "ejb:velos/velos-ejb-epat/PersonAgentBean!com.velos.eres.service.personAgent.PersonAgentRObj";
	public final static String PortalAgentHome = "ejb:velos/velos-ejb-eres/PortalAgentBean!com.velos.eres.service.portalAgent.PortalAgentRObj";
	public final static String PortalDesignAgentHome = "ejb:velos/velos-ejb-eres/PortalDesignAgentBean!com.velos.eres.service.portalDesignAgent.PortalDesignAgentRObj";
	public final static String PrefAgentHome = "ejb:velos/velos-ejb-eres/PrefAgentBean!com.velos.eres.service.prefAgent.PrefAgentRObj";
	public final static String ReportAgentHome = "ejb:velos/velos-ejb-eres/ReportAgentBean!com.velos.eres.service.reportAgent.ReportAgentRObj";
	public final static String ReportsAgentHome = "ejb:velos/velos-ejb-eres/ReportsAgentBean!com.velos.eres.service.reportsAgent.ReportsAgentRObj";
	public final static String ReviewMeetingTopicAgentHome = "ejb:velos/velos-ejb-eres/ReviewMeetingTopicAgentBean!com.velos.eres.service.submissionAgent.ReviewMeetingTopicAgent";
	public final static String SavedRepAgentHome = "ejb:velos/velos-ejb-eres/SavedRepAgentBean!com.velos.eres.service.savedRepAgent.SavedRepAgentRObj";
	public final static String SectionAgentHome = "ejb:velos/velos-ejb-eres/SectionAgentBean!com.velos.eres.service.sectionAgent.SectionAgent";
	public final static String SiteAgentHome = "ejb:velos/velos-ejb-eres/SiteAgentBean!com.velos.eres.service.siteAgent.SiteAgentRObj";
	public final static String SpecimenAgentHome = "ejb:velos/velos-ejb-eres/SpecimenAgentBean!com.velos.eres.service.specimenAgent.SpecimenAgentRObj";
	// Enhancement INV-22518: Pritam Singh
	public final static String BulkUploadAgentHome = "ejb:velos/velos-ejb-eres/BulkUploadAgentBean!com.velos.eres.bulkupload.service.BulkUploadAgent";
	public final static String BulkUploadErrAgentHome = "ejb:velos/velos-ejb-eres/BulkErrAgentBean!com.velos.eres.bulkupload.service.BulkErrorAgent";
	public final static String BulkUploadErrLogAgentHome = "ejb:velos/velos-ejb-eres/BulkErrLogAgentBean!com.velos.eres.bulkupload.service.BulkErrLogAgent";
	public final static String BulkTemplateAgentHome = "ejb:velos/velos-ejb-eres/BulkTemplateAgentBean!com.velos.eres.bulkupload.service.BulkTemplateAgent";
	public final static String BulkTemplateDetAgentHome = "ejb:velos/velos-ejb-eres/BulkTempateDetailAgentBean!com.velos.eres.bulkupload.service.BulkTempateDetailAgent";
	public final static String SpecimenApndxAgentHome = "ejb:velos/velos-ejb-eres/SpecimenApndxAgentBean!com.velos.eres.service.specimenApndxAgent.SpecimenApndxAgentRObj";
	public final static String SpecimenStatusAgentHome = "ejb:velos/velos-ejb-eres/SpecimenStatusAgentBean!com.velos.eres.service.specimenStatusAgent.SpecimenStatusAgentRObj";
	public final static String StatusHistoryAgentHome = "ejb:velos/velos-ejb-eres/StatusHistoryAgentBean!com.velos.eres.service.statusHistoryAgent.StatusHistoryAgentRObj";
	public final static String StdSiteAddInfoAgentHome = "ejb:velos/velos-ejb-eres/StdSiteAddInfoAgentBean!com.velos.eres.service.stdSiteAddInfoAgent.StdSiteAddInfoAgentRObj";
	public final static String StdSiteRightsAgentHome = "ejb:velos/velos-ejb-eres/StdSiteRightsAgentBean!com.velos.eres.service.stdSiteRightsAgent.StdSiteRightsAgentRObj";
	public final static String StorageAgentHome = "ejb:velos/velos-ejb-eres/StorageAgentBean!com.velos.eres.service.storageAgent.StorageAgentRObj";
	public final static String StorageAllowedItemsAgentHome = "ejb:velos/velos-ejb-eres/StorageAllowedItemsAgentBean!com.velos.eres.service.storageAllowedItemsAgent.StorageAllowedItemsAgentRObj";
	public final static String StorageStatusAgentHome = "ejb:velos/velos-ejb-eres/StorageStatusAgentBean!com.velos.eres.service.storageStatusAgent.StorageStatusAgentRObj";
	public final static String StorageKitAgentHome = "ejb:velos/velos-ejb-eres/StorageKitAgentBean!com.velos.eres.service.storageKitAgent.StorageKitAgentRObj";
	public final static String StudyAgentHome = "ejb:velos/velos-ejb-eres/StudyAgentBean!com.velos.eres.service.studyAgent.StudyAgent";
	public final static String StudyIdAgentHome = "ejb:velos/velos-ejb-eres/StudyIdAgentBean!com.velos.eres.service.studyIdAgent.StudyIdAgentRObj";
	// Enhancement CTRP-20527 : AGodara
	public final static String StudyINDIDEAgentHome = "ejb:velos/velos-ejb-eres/StudyINDIDEAgentBean!com.velos.eres.service.studyINDIDEAgent.StudyINDIDEAgentRObj";
	public final static String StudyNotifyAgentHome = "ejb:velos/velos-ejb-eres/StudyNotifyAgentBean!com.velos.eres.service.studyNotifyAgent.StudyNotifyAgentRObj";
	public final static String StudyRightsAgentHome = "ejb:velos/velos-ejb-eres/StudyRightsAgentBean!com.velos.eres.service.studyRightsAgent.StudyRightsAgentRObj";
	public final static String StudySiteAgentHome = "ejb:velos/velos-ejb-eres/StudySiteAgentBean!com.velos.eres.service.studySiteAgent.StudySiteAgentRObj";
	public final static String StudySiteApndxAgentHome = "ejb:velos/velos-ejb-eres/StudySiteApndxAgentBean!com.velos.eres.service.studySiteApndxAgent.StudySiteApndxAgentRObj";
	public final static String StudyStatusAgentHome = "ejb:velos/velos-ejb-eres/StudyStatusAgentBean!com.velos.eres.service.studyStatusAgent.StudyStatusAgentRObj";
	public final static String StudyTXArmAgentHome = "ejb:velos/velos-ejb-eres/StudyTXArmAgentBean!com.velos.eres.service.studyTXArmAgent.StudyTXArmAgentRObj";
	public final static String StudyVerAgentHome = "ejb:velos/velos-ejb-eres/StudyVerAgentBean!com.velos.eres.service.studyVerAgent.StudyVerAgentRObj";
	public final static String StudyViewAgentHome = "ejb:velos/velos-ejb-eres/StudyViewAgentBean!com.velos.eres.service.studyViewAgent.StudyViewAgentRObj";
	//Ak:Added for enhancement CTRP-20527_1
	public final static String StudyNIHGrantAgentHome = "ejb:velos/velos-ejb-eres/StudyNIHGrantAgentBean!com.velos.eres.service.studyNIHGrantAgent.StudyNIHGrantAgentRObj";
	public final static String SubmissionAgentHome = "ejb:velos/velos-ejb-eres/SubmissionAgentBean!com.velos.eres.service.submissionAgent.SubmissionAgent";
	public final static String SubmissionBoardAgentHome = "ejb:velos/velos-ejb-eres/SubmissionBoardAgentBean!com.velos.eres.service.submissionAgent.SubmissionBoardAgent";
	public final static String SubmissionProvisoAgentHome = "ejb:velos/velos-ejb-eres/SubmissionProvisoAgentBean!com.velos.eres.service.submissionAgent.SubmissionProvisoAgent";
	public final static String SubmissionStatusAgentHome = "ejb:velos/velos-ejb-eres/SubmissionStatusAgentBean!com.velos.eres.service.submissionAgent.SubmissionStatusAgent";
	public final static String TeamAgentHome = "ejb:velos/velos-ejb-eres/TeamAgentBean!com.velos.eres.service.teamAgent.TeamAgentRObj";
	public final static String ULinkAgentHome = "ejb:velos/velos-ejb-eres/ULinkAgentBean!com.velos.eres.service.ulinkAgent.ULinkAgentRObj";
	public final static String UpdateSchedulesAgentHome = "ejb:velos/velos-ejb-eres/UpdateSchedulesAgentBean!com.velos.eres.service.updateSchedulesAgent.UpdateSchedulesAgentRObj";
	public final static String UserAgentHome = "ejb:velos/velos-ejb-eres/UserAgentBean!com.velos.eres.service.userAgent.UserAgentRObj";
	public final static String UserSessionAgentHome = "ejb:velos/velos-ejb-eres/UserSessionAgentBean!com.velos.eres.service.userSessionAgent.UserSessionAgentRObj";
	public final static String UserSessionTokenAgentHome = "ejb:velos/velos-ejb-eres/UserSessionTokenAgentBean!com.velos.eres.service.userSessionAgent.UserSessionTokenAgentRObj";
	public final static String UserSessionLogAgentHome = "ejb:velos/velos-ejb-eres/UserSessionLogAgentBean!com.velos.eres.service.userSessionLogAgent.UserSessionLogAgentRObj";
	public final static String UserSiteAgentHome = "ejb:velos/velos-ejb-eres/UserSiteAgentBean!com.velos.eres.service.userSiteAgent.UserSiteAgentRObj";
	public final static String UsrGrpAgentHome = "ejb:velos/velos-ejb-eres/UsrGrpAgentBean!com.velos.eres.service.usrGrpAgent.UsrGrpAgentRObj";
	public final static String CtrpDraftAgentHome = "ejb:velos/velos-ejb-eres/CtrpDraftAgentBean!com.velos.eres.ctrp.service.CtrpDraftAgent";
	public final static String CtrpDraftDocAgentHome = "ejb:velos/velos-ejb-eres/CTRPDraftDocAgentBean!com.velos.eres.ctrp.service.CTRPDraftDocAgent";
	public final static String AuditRowEresAgentHome = "ejb:velos/velos-ejb-eres/AuditRowEresAgentBean!com.velos.eres.audit.service.AuditRowEresAgent";
	public final static String AuditRowEpatAgentHome = "ejb:velos/velos-ejb-epat/AuditRowEpatAgentBean!com.velos.epat.audit.service.AuditRowEpatAgent";
	public final static String CtrpAccrualAgentHome = "ejb:velos/velos-ejb-eres/CtrpAccrualAgentBean!com.velos.eres.ctrp.service.CtrpAccrualAgent";
	
	public final static String UsrSiteGrpAgentBean = "ejb:velos/velos-ejb-eres/UsrSiteGrpAgentBean!com.velos.eres.service.usrSiteGrpAgent.UsrSiteGrpAgentRObj";
	
	
	public final static String UserReportLogAgentHome = "ejb:velos/velos-ejb-eres/UserReportLogAgentBean!com.velos.eres.service.userReportLogAgent.UserReportLogAgentRObj";
	public final static String FinancialsAgentHome = "ejb:velos/velos-ejb-eres/FinancialsAgentBean!com.velos.eres.financials.service.FinancialsAgent";
	public final static String ComplianceAgentHome = "ejb:velos/velos-ejb-eres/ComplianceAgentBean!com.velos.eres.compliance.service.ComplianceAgent";
	public final static String NetworkAgentHome = "ejb:velos/velos-ejb-eres/NetworkAgentBean!com.velos.eres.service.networkAgent.NetworkAgentRObj";
	public static final String SPECIMEN_EJBHOME  = "SpecimenAgentBean";

	public static final String SPECIMENSTATUS_EJBHOME = "SpecimenStatusAgentBean";//JM

    public static final String STUDY_EJBHOME = "StudyBean";

    public static final String STUDYAGENT_EJBHOME = "StudyAgentBean";

    public static final String STUDYSECTION_EJBHOME = "SectionBean";

    public static final String STUDYSECTIONAGENT_EJBHOME = "SectionAgentBean";

    public static final String STUDYSTATUS_EJBHOME = "StudyStatusBean";

    public static final String STUDYSTATUSAGENT_EJBHOME = "StudyStatusAgentBean";

    public static final String TEAM_EJBHOME = "TeamBean";

    public static final String TEAMAGENT_EJBHOME = "TeamAgentBean";

    public static final String ACCOUNT_EJBHOME = "AccountBean";

    public static final String ACCOUNTAGENT_EJBHOME = "AccountAgentBean";

    public static final String GROUP_EJBHOME = "GroupBean";

    public static final String GROUPAGENT_EJBHOME = "GroupAgentBean";

    public static final String ADDRESS_EJBHOME = "AddressBean";

    public static final String ADDRESSAGENT_EJBHOME = "AddressAgentBean";

    public static final String MSGCNTR_EJBHOME = "MsgcntrBean";

    public static final String MSGCNTRAGENT_EJBHOME = "MsgcntrAgentBean";

    public static final String USER_EJBHOME = "UserEntityBean";

    public static final String USERAGENT_EJBHOME = "UserAgentBean";

    public static final String SITE_EJBHOME = "SiteBean";

    public static final String SITEAGENT_EJBHOME = "SiteAgentBean";

    public static final String ULINK_EJBHOME = "ULinkBean";

    public static final String ULINKAGENT_EJBHOME = "ULinkAgentBean";

    public static final String GRPRIGHTSAGENT_EJBHOME = "GrpRightsAgentBean";

    public static final String GRPRIGHTS_EJBHOME = "GrpRightsBean";

    public static final String REPORTSAGENT_EJBHOME = "ReportsAgentBean";

    public static final String USRGRPAGENT_EJBHOME = "UsrGrpAgentBean";

    public static final String USRGRP_EJBHOME = "UsrGrpBean";

    public static final String APPENDIXAGENT_EJBHOME = "AppendixAgentBean";

    public static final String APPENDIX_EJBHOME = "AppendixBean";

    public static final String REPORTAGENT_EJBHOME = "ReportAgentBean";

    public static final String ACCOUNTWRAPPERAGENT_EJBHOME = "AccountWrapperAgentBean";

    public static final String STUDYRIGHTSAGENT_EJBHOME = "StudyRightsAgentBean";

    public static final String STUDYRIGHTS_EJBHOME = "StudyRightsBean";

    public static final String STUDYNOTIFYAGENT_EJBHOME = "StudyNotifyAgentBean";

    public static final String STUDYNOTIFY_EJBHOME = "StudyNotifyBean";

    public static final String EXTURIGHTSAGENT_EJBHOME = "ExtURightsAgentBean";

    public static final String EXTURIGHTS_EJBHOME = "ExtURightsBean";

    public static final String CODELSTAGENT_EJBHOME = "CodelstAgentBean";

    public static final String CODELST_EJBHOME = "CodelstBean";


    //KM:20,May2006-Newly added for adding data in the ctrl tab table for tech. support work requirement
    public static final String CTRLTABAGENT_EJBHOME = "CtrltabAgentBean";

    public static final String CTRLTAB_EJBHOME = "CtrltabBean";

    public static final String PREFAGENT_EJBHOME = "PrefAgentBean";

    public static final String PREF_EJBHOME = "PrefBean";

    public static final String PERSONAGENT_EJBHOME = "PersonAgentBean";

    public static final String PERSON_EJBHOME = "PersonBean";

    public static final String ERESEARCH_DATASOURCE = "java:comp/env/eres";

    public static final String ERESEARCH_PAT_DATASOURCE = "java:comp/env/perjunk";

    public static final String PATPROT_EJBHOME = "PatProtBean";

    public static final String PATPROTAGENT_EJBHOME = "PatProtAgentBean";

    public static final String PATSTUDYSTAT_EJBHOME = "PatStudyStatBean";

    public static final String PATSTUDYSTATAGENT_EJBHOME = "PatStudyStatAgentBean";

    public static final String ESCHREPORTSAGENT_EJBHOME = "EschReportsAgentBean";

    public static final String STUDYVIEW_EJBHOME = "StudyViewBean";

    public static final String STUDYVIEWAGENT_EJBHOME = "StudyViewAgentBean";

    public static final String MILESTONE_EJBHOME = "MilestoneBean";

    public static final String MILESTONEAGENT_EJBHOME = "MilestoneAgentBean";

    public static final String MILEPAYMENT_EJBHOME = "MilepaymentBean";

    public static final String MILEPAYMENTAGENT_EJBHOME = "MilepaymentAgentBean";

    public static final String SAVEDREP_EJBHOME = "SavedRepBean";

    public static final String SAVEDREPAGENT_EJBHOME = "SavedRepAgentBean";

    public static final String STUDYVER_EJBHOME = "StudyVerBean";

    public static final String STUDYVERAGENT_EJBHOME = "StudyVerAgentBean";

    public static final String MILEAPNDXAGENTBEAN_EJBHOME = "MileApndxAgentBean";

    public static final String MILEAPNDXBEAN_EJBHOME = "MileApndxBean";

    public static final String PERAPNDXAGENTBEAN_EJBHOME = "PerApndxAgentBean";

    public static final String PERAPNDXBEAN_EJBHOME = "PerApndxBean";

    public static final String USERSESSIONLOGAGENTBEAN_EJBHOME = "UserSessionLogAgentBean";

    public static final String USERSESSIONLOGBEAN_EJBHOME = "UserSessionLogBean";

    public static final String USERSESSIONAGENTBEAN_EJBHOME = "UserSessionAgentBean";

    public static final String USERSESSIONBEAN_EJBHOME = "UserSessionBean";

    public static final String USERSITE_EJBHOME = "UserSiteBean";

    public static final String USERSITEAGENT_EJBHOME = "UserSiteAgentBean";

    public static final String FORMNOTIFY_EJBHOME = "FormNotifyBean";

    public static final String FORMNOTIFYAGENT_EJBHOME = "FormNotifyAgentBean";

    public static final String CATLIB_EJBHOME = "CatLibBean";

    public static final String CATLIBAGENT_EJBHOME = "CatLibAgentBean";

    public static final String FLDRESP_EJBHOME = "FldRespBean";

    public static final String FLDRESPAGENT_EJBHOME = "FldRespAgentBean";

    public static final String FIELDLIB_EJBHOME = "FieldLibBean";

    public static final String FIELDLIBAGENT_EJBHOME = "FieldLibAgentBean";

    public static final String FORMLIB_EJBHOME = "FormLibBean";

    public static final String FORMLIBAGENT_EJBHOME = "FormLibAgentBean";

    public static final String FORMFIELD_EJBHOME = "FormFieldBean";

    public static final String FORMFIELDAGENT_EJBHOME = "FormFieldAgentBean";

    public static final String LINKEDFORMS_EJBHOME = "LinkedFormsBean";

    public static final String LINKEDFORMSAGENT_EJBHOME = "LinkedFormsAgentBean";

    public static final String FORMSEC_EJBHOME = "FormSecBean";

    public static final String FORMSECAGENT_EJBHOME = "FormSecAgentBean";

    public static final String FORMSTAT_EJBHOME = "FormStatBean";

    public static final String FORMSTATAGENT_EJBHOME = "FormStatAgentBean";

    public static final String GRPSUPUSER_RIGHTSAGENT_EJBHOME = "GrpSupUserRightsAgentBean";

    public static final String GRPSUPUSER_RIGHTS_EJBHOME = "GrpSupUserRightsBean";

    public static final String STUDYIDAGENT_EJBHOME = "StudyIdAgentBean";

    public static final String STUDYID_EJBHOME = "StudyIdBean";

    public static final String STUDYSITEAGENT_EJBHOME = "StudySiteAgentBean";

    public static final String STUDYSITE_EJBHOME = "StudySiteBean";

    public static final String PERIDAGENT_EJBHOME = "PerIdAgentBean";

    public static final String PERID_EJBHOME = "PerIdBean";

    public static final String DYNREPAGENT_EJBHOME = "DynRepAgentBean";

    public static final String DYNREP_EJBHOME = "DynRepBean";

    public static final String DYNREPDTAGENT_EJBHOME = "DynRepDtAgentBean";

    public static final String DYNREPDT_EJBHOME = "DynRepDtBean";

    public static final String DYNREPFLTRAGENT_EJBHOME = "DynRepFltrAgentBean";

    public static final String DYNREPFLTR_EJBHOME = "DynRepFltrBean";

    public static final String FLDVALIDATE_EJBHOME = "FldValidateBean";

    public static final String FLDVALIDATEAGENT_EJBHOME = "FldValidateAgentBean";

    public static final String BROWSERROWSAGENT_EJBHOME = "BrowserRowsAgentBean";

    public static final String LABAGENT_EJBHOME = "LabAgentBean";

    public static final String LAB_EJBHOME = "LabBean";

    public static final String STATUSHISTORY_EJBHOME = "StatusHistoryBean";

    public static final String STATUSHISTORYAGENT_EJBHOME = "StatusHistoryAgentBean";

    public static final String STUDYSITEAPNDX_EJBHOME = "StudySiteApndxBean";

    public static final String STUDYSITEAPNDXAGENT_EJBHOME = "StudySiteApndxAgentBean";

    public static final String STDSITEADDINFO_EJBHOME = "StdSiteAddInfoBean";

    public static final String STDSITEADDINFOAGENT_EJBHOME = "StdSiteAddInfoAgentBean";

    public static final String STDSITERIGHTS_EJBHOME = "StdSiteRightsBean";

    public static final String STDSITERIGHTSAGENT_EJBHOME = "StdSiteRightsAgentBean";

    public static final String FORMQUERY_EJBHOME = "FormQueryBean";

    public static final String FORMQUERYAGENT_EJBHOME = "FormQueryAgentBean";

    public static final String FORMQUERYSTATUS_EJBHOME = "FormQueryStatusBean";

    public static final String FORMQUERYSTATUSAGENT_EJBHOME = "FormQueryStatusAgentBean";

    public static final String STUDYTXARM_EJBHOME = "StudyTXArmBean";

    public static final String STUDYTXARMAGENT_EJBHOME = "StudyTXArmAgentBean";

    public static final String PATTXARM_EJBHOME = "PatTXArmBean";

    public static final String PATTXARMAGENT_EJBHOME = "PatTXArmAgentBean";

    public static final String COMMONAGENT_EJBHOME = "CommonAgentBean";

    public static final String PORTAL_EJBHOME ="PortalAgentBean";//KM

    //JM: 040407
    public static final String PATLOGIN_EJBHOME = "PatLoginBean";

    public static final String PATLOGINAGENT_EJBHOME = "PatLoginAgentBean";

    public static final String PORTALDESIGN_EJBHOME = "PortalDesignAgentBean";

    public static final String STORAGE_EJBHOME = "StorageAgentBean";//KM

    public static final String STORAGESTATUS_EJBHOME = "StorageStatusAgentBean";//KM

    public static final String STORAGEALLOWEDITEMS_EJBHOME = "StorageAllowedItemsAgentBean";//KM

    public static final String SPECIMENAPPENDX_EJBHOME = "SpecimenApndxAgentBean";//KN

    public static final String UPDATESCHEDULESAGENT_EJBHOME = "UpdateSchedulesAgentBean";//KM

    public static final String FORMACTION_EJBHOME = "FormActionAgentBean";

    public static final String STORAGEKIT_EJBHOME = "StorageKitBean";

    public static final String STORAGEKITAGENT_EJBHOME = "StorageKitAgentBean";


}
