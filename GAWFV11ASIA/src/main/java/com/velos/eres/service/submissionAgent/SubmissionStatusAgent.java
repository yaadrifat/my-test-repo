package com.velos.eres.service.submissionAgent;

import javax.ejb.Remote;

import com.velos.eres.business.submission.impl.SubmissionStatusBean;

@Remote
public interface SubmissionStatusAgent {
    public SubmissionStatusBean getSubmissionStatusDetails(int id);
    public Integer createSubmissionStatus(SubmissionStatusBean submissionStatusBean);
    public Integer setCurrentToOld(SubmissionStatusBean submissionStatusBean);
    
    /** Gets the one status previous from the current status for a for a submission board and submission. Returns the Pk of the codelst
     * */
     public String getPreviousCurrentSubmissionStatus(int submissionPK, int submissionBoardPK);

     
}