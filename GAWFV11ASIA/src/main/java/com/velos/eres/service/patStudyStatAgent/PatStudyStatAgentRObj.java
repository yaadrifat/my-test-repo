/*
 * Classname			PatStudyStatAgentRObj.class
 * 
 * Version information   
 *
 * Date					06/16/2001
 * 
 * Copyright notice
 */

package com.velos.eres.service.patStudyStatAgent;

import java.util.Hashtable;

import javax.ejb.Remote;

import com.velos.eres.business.common.PatStudyStatDao;
import com.velos.eres.business.patStudyStat.impl.PatStudyStatBean;

/**
 * Remote interface for Patient Study Status session EJB
 * 
 * @author Sonia
 */

/*
 * modified by sonia, 08/25/04, added methods
 * getPatStudyCurrentStatus(),reomovePatStudyStat()
 */
@Remote
public interface PatStudyStatAgentRObj {
    /**
     * gets the Patient Study Status details
     */
    PatStudyStatBean getPatStudyStatDetails(int id);

    /**
     * sets the Patient study status details
     */
    public int setPatStudyStatDetails(PatStudyStatBean psk);

    public int updatePatStudyStat(PatStudyStatBean psk);

    public PatStudyStatDao getStudyPatients(String studyId);

    public PatStudyStatBean getPatStudyCurrentStatus(String studyId,
            String patientId);

    public int getPkForStatusCodelstSubType(int studyId, int patId,
            String codeSubType);

    /**
     * Removes Patient Study Status
     * 
     * @param id
     *            Patient Study Status Id to be deleted
     * @return int :0 if successful else it returns -1
     */
    public int reomovePatStudyStat(int statId);
    
    public int reomovePatStudyStat(int statId,Hashtable<String, String> args);
    
    public int getCountPatStudyId(int studyId, String patStudyId);//KM
    
    public PatStudyStatBean getLatestPatStudyStat(int studyId, int patientId); 

}
