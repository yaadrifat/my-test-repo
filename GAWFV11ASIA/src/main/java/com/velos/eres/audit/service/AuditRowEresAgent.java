package com.velos.eres.audit.service;
import javax.ejb.Remote;

import com.velos.eres.audit.business.AuditRowEresBean;
@Remote
public interface AuditRowEresAgent {
	public AuditRowEresBean getAuditRowEresBean(Integer id);
	public int getRID(int pkey, String tableName, String pkColumnName);
	public int findLatestRaid(int rid, int userId);
	public int setReasonForChange(int raid, String remarks);
	public int setAuditRowDetails(AuditRowEresBean arsk);
}
