package com.velos.eres.audit.service;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import java.util.ArrayList;

import com.velos.eres.audit.business.AuditRowEresBean;
import com.velos.eres.business.common.AuditRowEresDao;
import com.velos.eres.service.util.Rlog;

@Stateless
@Remote( { AuditRowEresAgent.class } )
public class AuditRowEresAgentBean implements AuditRowEresAgent {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

	public AuditRowEresBean getAuditRowEresBean(Integer id) {
		if (id == null || id == 0) { return null; }
		AuditRowEresBean AuditRowBean = null;
		try {
			AuditRowBean = (AuditRowEresBean) em.find(AuditRowEresBean.class, new Integer(id));
		} catch (Exception e) {
			Rlog.fatal("audit", "Exception in AuditRowEresAgentBean.getAuditRowEresBean: "+e);
		}
		return AuditRowBean;
	}
	 @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public int getRID(int pkey, String tableName, String pkColumnName) {
		int output = 0;
		AuditRowEresDao auditDao = new AuditRowEresDao();
		output = auditDao.getRID(pkey, tableName, pkColumnName);
		return output;
	}
	 @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public int findLatestRaid(int rid, int userId) {
		int output = 0;
		try{
			Query query = em.createNamedQuery("findLatestAuditRowEresByUserIdRID");
			query.setParameter("rid", rid);
			query.setParameter("userId", userId+"");
			query.setParameter("userIdLike", userId+",%");
			
			ArrayList list = (ArrayList) query.getResultList();
			if (list == null || list.size() == 0) { return -1; }

			AuditRowEresBean auditRow = (AuditRowEresBean) list.get(0);
			output = ((Integer)auditRow.getId()).intValue();
		} catch (Exception e){
			Rlog.fatal("audit", "Exception in AuditRowEresAgentBean.findLatestRaid: "+e);
		}
		return output;
	}
	 @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public int setReasonForChange(int raid, String remarks) {
		int output = 0;
		AuditRowEresDao auditDao = new AuditRowEresDao();
		output = auditDao.setReasonForChange(raid, remarks);
		return output;
	}
	public int setAuditRowDetails(AuditRowEresBean arsk) {
        try {
        	AuditRowEresBean arBean = new AuditRowEresBean();
        	arBean.updateAuditRow(arsk);
            em.persist(arBean);
            return (arBean.getId());
        } catch (Exception e) {
            Rlog.fatal("auditRow", "Error in setAuditRowDetails() in AuditRowEresAgentBean "
                    + e);
        }
        return 0;
    }
}
