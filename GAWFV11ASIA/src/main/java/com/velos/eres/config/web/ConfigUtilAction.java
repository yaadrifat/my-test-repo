package com.velos.eres.config.web;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.ActionSupport;
import com.velos.eres.compliance.web.ComplianceJB;
import com.velos.eres.service.util.CFG;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.study.StudyJB;

public class ConfigUtilAction extends ActionSupport  {
	private static final long serialVersionUID = 1L;
	private HttpServletRequest request = null;
	private HttpServletResponse response = null;
	private static final String CONFIG_STUDY_SCREEN_TILE = "configStudyScreenTile";
	private static final String FLEX_STUDY_SCREEN_TILE = "flexStudyScreenTile";	
	private static final String FLEX_STUDY_SCREEN_TILE_ECOMP = "flexStudyScreenEcompTile";	
	private static final String FLEX_STUDY_SCREEN_LIND_FT_TILE = "flexStudyScreenLindFtTile";	
	private static final String STUDY_CHECK_N_SUBMIT_TILE = "studyCheckNSubmitTile";
	private static final String FLEX_PROTOCOL_CHANGE_LOG = "flexProtocolChangeLogTile";
	private static final String FLEX_RESUBMISSION_DRAFT = "flexResubmissionDraftTile";
	private static final String STUDY_ID = "studyId";
	private StudyJB studyJB = null;
	private ComplianceJB complianceJB = null;
	private Integer userId = null;
	private Integer accountId = null;
	private String isNewAmendment="false";

	public String getIsNewAmendment() {
		return isNewAmendment;
	}

	public void setIsNewAmendment(String isNewAmendment) {
		this.isNewAmendment = isNewAmendment;
	}

	public ComplianceJB getComplianceJB() {
		return complianceJB;
	}

	public void setComplianceJB(ComplianceJB complianceJB) {
		this.complianceJB = complianceJB;
	}

	public ConfigUtilAction() {
		ActionContext ac = ActionContext.getContext();
		request = (HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
		response = (HttpServletResponse)ac.get(ServletActionContext.HTTP_RESPONSE);
		if (!StringUtil.isEmpty(request.getParameter(STUDY_ID))) {
			int iStudyId = StringUtil.stringToNum(request.getParameter(STUDY_ID).trim());
			if (iStudyId > 0) {
				studyJB = new StudyJB();
				studyJB.setId(iStudyId);
				studyJB.getStudyDetails();
				try {
					request.getSession(false).setAttribute("studyJB", studyJB);
				} catch(Exception e) {}
			}
		}
		complianceJB = new ComplianceJB();
		if (request.getSession(false) != null) {
			userId = StringUtil.stringToInteger((String) request.getSession(false).getAttribute("userId"));
			accountId = StringUtil.stringToInteger((String) request.getSession(false).getAttribute("accountId"));
		}
		isNewAmendment =(String)request.getParameter("isAmendment");	
		System.out.println("isNewAmendment:::"+isNewAmendment);
		if(isNewAmendment == null ) {
			isNewAmendment="false";
		}
		setIsNewAmendment(isNewAmendment);
		
	}
	
	public String getConfigStudyScreen() {
		return CONFIG_STUDY_SCREEN_TILE;
	}

	private String intercept(ActionInvocation invocation) throws Exception {
	    ActionContext context = invocation.getInvocationContext();
	    Map<String,Object> parameters = (Map<String,Object>)context.get(ActionContext.PARAMETERS);

	    Map<String, Object> parametersCopy = new HashMap<String, Object>();
	    parametersCopy.putAll(parameters);
	    parametersCopy.put("isAmendment", new java.lang.String("false"));

	    context.put(ActionContext.PARAMETERS, parametersCopy);
	    return invocation.invoke();
	}

	public String getFlexStudyScreen() {
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("userId", userId);
		paramMap.put("accountId", accountId);
		int pkStudy = StringUtil.stringToNum((String)request.getParameter("studyId"));
		paramMap.put("studyId", pkStudy);
		System.out.println("userId::::: "+userId);
		System.out.println("accountId::: "+accountId);
		System.out.println("pkStudy::: "+pkStudy);
		if("LIND".equals(CFG.EIRB_MODE)){
		if("true".equals(this.getIsNewAmendment()) ){
			int newStudyStatusId=0;
			newStudyStatusId = complianceJB.addStudyStatus(paramMap);
			System.out.println(" Inside true:::"+newStudyStatusId);
			if (newStudyStatusId > 0 ){
				this.setIsNewAmendment("false");
				ActionContext ac = ActionContext.getContext();
				try {
					this.intercept(ac.getActionInvocation());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		complianceJB.retrieveLockStudyFlag(paramMap);
		}
		if(pkStudy==0){
			try {
				request.getSession(false).setAttribute("studyJB", null);
			} catch(Exception e) {}
		}
		String resultTile="";
		if("LIND".equals(CFG.EIRB_MODE)){
			resultTile=FLEX_STUDY_SCREEN_TILE;
		}/*else{
			resultTile=FLEX_STUDY_SCREEN_TILE_ECOMP;
			System.out.println("Inside non-Lind FT mode");
		}*/
		return resultTile;
	}
	//adding this to support the mapping for a new view of the protocol study management page for lind FT 
	public String getFlexStudyScreenLindFt() {
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("userId", userId);
		paramMap.put("accountId", accountId);
		int pkStudy = StringUtil.stringToNum((String)request.getParameter("studyId"));
		paramMap.put("studyId", pkStudy);
		if("LIND".equals(CFG.EIRB_MODE)){
			if("true".equals(this.getIsNewAmendment()) ){
				int newStudyStatusId=0;			
				newStudyStatusId = complianceJB.addStudyStatus(paramMap);
				if (newStudyStatusId > 0 ){
					this.setIsNewAmendment("false");
				}
			}
			complianceJB.retrieveLockStudyFlag(paramMap);
		}
		if(pkStudy==0){
			try {
				request.getSession(false).setAttribute("studyJB", null);
			} catch(Exception e) {}
		}
		return FLEX_STUDY_SCREEN_LIND_FT_TILE;
	}
	public String getFlexResubmissionDraft() {
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("userId", userId);
		paramMap.put("accountId", accountId);
		int pkStudy = StringUtil.stringToNum((String)request.getParameter("studyId"));
		paramMap.put("studyId", pkStudy);
		complianceJB.retrieveLockStudyFlag(paramMap);
		if("true".equals(isNewAmendment) ){
			
		}
		return FLEX_RESUBMISSION_DRAFT;
	}
	public String getFlexProtocolChangeLog() {
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("userId", userId);
		paramMap.put("accountId", accountId);
		int pkStudy = StringUtil.stringToNum((String)request.getParameter("studyId"));
		paramMap.put("studyId", pkStudy);
		complianceJB.retrieveLockStudyFlag(paramMap);
		if("true".equals(isNewAmendment) ){
			int newStudyStatusId=0;			
			newStudyStatusId = complianceJB.addStudyStatus(paramMap);
			if (newStudyStatusId > 0 ){
				this.setIsNewAmendment("false");
			}
		}		
		return FLEX_PROTOCOL_CHANGE_LOG;
	}

	public StudyJB getStudyJB() {
		return studyJB;
	}	
}
