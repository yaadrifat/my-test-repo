package com.velos.eres.bulkupload.service;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.eres.bulkupload.business.BulkErrBean;
import com.velos.eres.service.util.Rlog;

@Stateless
@Remote( { BulkErrorAgent.class } )
public class BulkErrAgentBean implements BulkErrorAgent{
	  @PersistenceContext(unitName = "eres")
	    protected EntityManager em;
	  public BulkErrBean getBulkErrDetails(int pkSpec) {
	        if (pkSpec == 0) {
	            return null;
	        } 
	        BulkErrBean spec = null;
	        try {
	        	spec = (BulkErrBean) em.find(BulkErrBean .class, new Integer(pkSpec));	            
	        } catch (Exception e) {
	            Rlog.fatal("bulkupload",
	                    "Error in getBulkErrDetails() in BulkErrAgentBean" + e);
	        }
	        return spec;
	    }
	
	  public int setBulkErrDetails(BulkErrBean spsk) {    
	        try {
	        	BulkErrBean  spec = new BulkErrBean(); 
	        	spec.updateBulkErr(spsk);        	 
	            em.persist(spec);
	            return spec.getPkErr();
	        } catch (Exception e) {
	        	  Rlog.fatal("bulkupload",  "Error in setBulkErrDetails() in BulkErrAgentBean" + e);
	        }
	        return 0;
	    }
	    public int updateBulkErr(BulkErrBean spsk) {
	    	BulkErrBean appSpec= null;
	        int output;
	        try {	            
	            output = appSpec.updateBulkErr(spsk);            
	            em.merge(appSpec);
	        } catch (Exception e) {
	        	Rlog.fatal("bulkupload",  "Error in updateBulkUpload() in BulkErrAgentBean" + e);
	            return -2;
	        }
	        return output;
	    }

		

}
