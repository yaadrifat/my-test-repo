package com.velos.eres.bulkupload.service;

import javax.ejb.Remote;

import com.velos.eres.bulkupload.business.BulkUploadBean;
@Remote
public interface BulkUploadAgent {
	 	public BulkUploadBean getBulkUploadDetails(int pkSpec);    
	    public int setBulkUplodDetails(BulkUploadBean specimen);    
	    public int updateBulkUpload(BulkUploadBean specimen);
}
