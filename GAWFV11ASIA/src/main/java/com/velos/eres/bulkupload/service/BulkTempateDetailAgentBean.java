package com.velos.eres.bulkupload.service;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.velos.eres.bulkupload.business.BulkTemplateDetailBean;
import com.velos.eres.service.util.Rlog;
@Stateless
@Remote( { BulkTempateDetailAgent.class } )
public class BulkTempateDetailAgentBean implements BulkTempateDetailAgent{
	  @PersistenceContext(unitName = "eres")
	  protected EntityManager em;
	  public BulkTemplateDetailBean getBulkTemplateDetDetails(int pkSpec) {
	        if (pkSpec == 0) {
	            return null;
	        } 
	        BulkTemplateDetailBean spec = null;
	        try {
	        	spec = (BulkTemplateDetailBean) em.find(BulkTemplateDetailBean .class, new Integer(pkSpec));	            
	        } catch (Exception e) {
	            Rlog.fatal("bulktemplateDet",
	                    "Error in getBulkTemplateDetDetails() in BulkTempateDetailAgentBean" + e);
	        }
	        return spec;
	    }
	
	  public int setBulkTemplateDetDetails(BulkTemplateDetailBean spsk) {    
	        try {
	        	BulkTemplateDetailBean  spec = new BulkTemplateDetailBean(); 
	        	spec.updateBulktemplateDet(spsk);        	 
	            em.persist(spec);
	            return spec.getPkTemplateDet();
	        } catch (Exception e) {
	        	  Rlog.fatal("bulktemplateDet",  "Error in setBulkTemplateDetDetails() in BulkTempateDetailAgentBean" + e);
	        }
	        return 0;
	    }
	    public int updateBulktemplateDet(BulkTemplateDetailBean spsk) {
	    	BulkTemplateDetailBean appSpec= null;
	        int output;
	        try {	            
	            output = appSpec.updateBulktemplateDet(spsk);            
	            em.merge(appSpec);
	        } catch (Exception e) {
	        	Rlog.fatal("bulktemplateDet",  "Error in updateBulktemplateDet() in BulkTempateDetailAgentBean" + e);
	            return -2;
	        }
	        return output;
	    }

}
