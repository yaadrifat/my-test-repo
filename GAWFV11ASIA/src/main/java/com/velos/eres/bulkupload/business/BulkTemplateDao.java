package com.velos.eres.bulkupload.business;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.service.util.Rlog;

public class BulkTemplateDao extends CommonDAO implements java.io.Serializable{
	
	
	private ArrayList pkTemplate;
    private ArrayList fkBulkEntity;
    private ArrayList templateName;
    private ArrayList createdOn;
	private ArrayList fkUser;
	private ArrayList<Integer> fkEntityDetail;
	private ArrayList fileFldName;
	private ArrayList<Integer> fileFldColnum;
    private ArrayList usrFirstName;
    private ArrayList usrMidName;
    private ArrayList usrLastName;
    private ArrayList velosFieldName;
	
    public ArrayList getVelosFieldName() {
		return velosFieldName;
	}
	public void setVelosFieldName(ArrayList velosFieldName) {
		this.velosFieldName = velosFieldName;
	}
	public void setVelosFieldName(String velosFieldName) {
		this.velosFieldName.add(velosFieldName);
	}
	public ArrayList getFkEntityDetail() {
		return fkEntityDetail;
	}
	public void setFkEntityDetail(ArrayList fkEntityDetail) {
		this.fkEntityDetail = fkEntityDetail;
	}
	public void setFkEntityDetail(int fkEntityDetail) {
		this.fkEntityDetail.add(fkEntityDetail);
	}
	public ArrayList getFileFldName() {
		return fileFldName;
	}
	public void setFileFldName(ArrayList fileFldName) {
		this.fileFldName = fileFldName;
	}
	public void setFileFldName(String fileFldName) {
		this.fileFldName.add(fileFldName);
	}
	public ArrayList getFileFldColnum() {
		return fileFldColnum;
	}
	public void setFileFldColnum(ArrayList fileFldColnum) {
		this.fileFldColnum = fileFldColnum;
	}
	public void setFileFldColnum(int fileFldColnum) {
		this.fileFldColnum.add(fileFldColnum);
	}
    public ArrayList getUsrFirstName() {
		return usrFirstName;
	}


	public void setUsrFirstName(ArrayList usrFirstName) {
		this.usrFirstName = usrFirstName;
	}

	public void setUsrFirstName(String usrFirstName) {
		this.usrFirstName.add(usrFirstName);
	}

	public ArrayList getUsrMidName() {
		return usrMidName;
	}


	public void setUsrMidName(ArrayList usrMidName) {
		this.usrMidName = usrMidName;
	}
	public void setUsrMidName(String usrMidName) {
		this.usrMidName.add(usrMidName);
	}


	public ArrayList getUsrLastName() {
		return usrLastName;
	}


	public void setUsrLastName(ArrayList usrLastName) {
		this.usrLastName = usrLastName;
	}
	public void setUsrLastName(String usrLastName) {
		this.usrLastName.add(usrLastName);
	}
	public BulkTemplateDao(){
    	pkTemplate=new ArrayList();
    	fkBulkEntity=new ArrayList();
    	templateName=new ArrayList();
    	fkUser=new ArrayList();
    	createdOn=new ArrayList();
    	fkEntityDetail=new ArrayList();
    	fileFldName=new ArrayList();
    	fileFldColnum=new ArrayList();
    	usrFirstName=new ArrayList();
    	usrMidName=new ArrayList();
    	usrLastName=new ArrayList();
    	velosFieldName=new ArrayList();
    }
    public ArrayList getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(ArrayList createdOn) {
		this.createdOn = createdOn;
	}
	public void setCreatedOn(String createdOn) {
		this.createdOn.add(createdOn);
	}
	public ArrayList getPkTemplate() {
		return pkTemplate;
	}

	public void setPkTemplate(ArrayList pkTemplate) {
		this.pkTemplate = pkTemplate;
	}

	public void setPkTemplate(String pkTemplate) {
		this.pkTemplate.add(pkTemplate);
	}

	public ArrayList getFkBulkEntity() {
		return fkBulkEntity;
	}

	public void setFkBulkEntity(ArrayList fkBulkEntity) {
		this.fkBulkEntity = fkBulkEntity;
	}
	public void setFkBulkEntity(String fkBulkEntity) {
		this.fkBulkEntity.add(fkBulkEntity);
	}

	public ArrayList getTemplateName() {
		return templateName;
	}

	public void setTemplateName(ArrayList templateName) {
		this.templateName = templateName;
	}
	public void setTemplateName(String templateName) {
		this.templateName.add(templateName);
	}

	public ArrayList getFkUser() {
		return fkUser;
	}

	public void setFkUser(ArrayList fkUser) {
		this.fkUser = fkUser;
	}
	public void setFkUser(String fkUser) {
		this.fkUser.add(fkUser);
	}
	public void getUserTemplates(String fkAccount){
		PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement("SELECT t.pk_bulk_template_master,t.fk_bulk_entity_master,t.template_name,t.created_on,u.usr_firstname,u.usr_midname,u.usr_lastname from er_bulk_template_master t,er_user u where t.fk_user=u.pk_user and fk_account=? order by t.pk_bulk_template_master desc");
            pstmt.setString(1, fkAccount);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
            	setPkTemplate(rs.getString("pk_bulk_template_master"));
            	setFkBulkEntity(rs.getString("fk_bulk_entity_master"));
            	setTemplateName(rs.getString("template_name"));
            	setCreatedOn(rs.getString("created_on"));
            	setUsrFirstName(rs.getString("usr_firstname"));
            	setUsrMidName(rs.getString("usr_midname"));
            	setUsrLastName(rs.getString("usr_lastname"));
            }

       } catch (SQLException ex) {
            Rlog.fatal("BulkTemplateDao",
                    "BulkTemplateDao.getUserTemplates EXCEPTION IN FETCHING FROM ER_BULK_UPLOAD table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
	}
    public int deleteMappings(String[] deleteIds, String userId, String ipAdd) {
        int ret = 1;
        CallableStatement cstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            ArrayDescriptor inIds = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY deleteIdsArray = new ARRAY(inIds, conn, deleteIds);
            cstmt = conn
                    .prepareCall("{call SP_DELETE_BULKMAPPING(?,?,?,?)}");
            Rlog.debug("bulkUploadMapping",
                    "BulkTemplateDao:deleteMappings-after prepare call of SP_DELETE_BULKMAPPING");
            cstmt.setArray(1, deleteIdsArray);
            cstmt.setString(2, userId);
            cstmt.setString(3, ipAdd);
            Rlog.debug("bulkUploadMapping",
                    "BulkTemplateDao:deleteMappings-after prepare call of SP_DELETE_BULKMAPPING"
                            + deleteIdsArray);

            cstmt.registerOutParameter(4, java.sql.Types.INTEGER);
            cstmt.execute();
            Rlog.debug("bulkUploadMapping",
                    "BulkTemplateDao:deleteMappings-after execute of sp_delete_specimens");
            ret = cstmt.getInt(4);
            Rlog.debug("bulkUploadMapping", "BulkTemplateDao:deleteMappings-after execute of RETURN VALUE"
                    + ret);

            return ret;

        } catch (Exception e) {
            Rlog.fatal("storage",
                    "EXCEPTION in BulkTemplateDao:deleteSpecimens, excecuting Stored Procedure "
                            + e);
            return -1;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }
	public void getMappingDetails(String fkBulkMaster,String fkAccount){
		PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement("SELECT d.fk_bulk_entity_detail,d.file_field_name,d.file_field_colnum from er_bulk_template_master m, er_bulk_template_detail d, er_user u where m.pk_bulk_template_master=d.fk_bulk_template_master and u.pk_user =  m.fk_user and m.pk_bulk_template_master=? and u.fk_account = ?");
            pstmt.setString(1, fkBulkMaster);
            pstmt.setString(2, fkAccount);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
            	setFkEntityDetail(rs.getInt("fk_bulk_entity_detail"));
            	setFileFldName(rs.getString("file_field_name"));
            	setFileFldColnum(rs.getInt("file_field_colnum"));
            }

       } catch (SQLException ex) {
            Rlog.fatal("BulkTemplateDao",
                    "BulkTemplateDao.getMappingDetails EXCEPTION IN FETCHING FROM ER_BULK_UPLOAD table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
	}
	public void getMappingNames(int pktem){
		PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement("select e.VELOS_FIELD_NAME ,d.FILE_FIELD_NAME from ER_BULK_ENTITY_DETAIL e,ER_BULK_TEMPLATE_DETAIL d,ER_BULK_TEMPLATE_MASTER m where M.PK_BULK_TEMPLATE_MASTER=D.FK_BULK_TEMPLATE_MASTER and D.FK_BULK_ENTITY_DETAIL=E.PK_BULK_ENTITY_DETAIL and M.PK_BULK_TEMPLATE_MASTER=?");
            pstmt.setInt(1, pktem);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
            	setVelosFieldName(rs.getString("velos_field_name"));
            	setFileFldName(rs.getString("file_field_name"));
            }

       } catch (SQLException ex) {
            Rlog.fatal("BulkTemplateDao",
                    "BulkTemplateDao.getMappingDetails EXCEPTION IN FETCHING FROM ER_BULK_UPLOAD table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
	}
	public void getTemplateNames(String temNsme,String accountId){
		PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement("select pk_bulk_template_master from er_bulk_template_master m, er_user u where u.pk_user = m.fk_user and upper(m.template_name) = upper(?) and fk_account=?");
            pstmt.setString(1,temNsme);
            pstmt.setString(2,accountId);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
            	setPkTemplate(rs.getString("pk_bulk_template_master"));
            }

       } catch (SQLException ex) {
            Rlog.fatal("BulkTemplateDao",
                    "BulkTemplateDao.getTemplateNames EXCEPTION IN FETCHING FROM ER_BULK_UPLOAD table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
	}



}
