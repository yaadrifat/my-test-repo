package com.velos.eres.bulkupload.business;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.velos.eres.service.util.Rlog;

@Entity
@Table(name = "ER_BULK_UPLOAD_ERRORLOG")
public class BulkErrLogBean implements Serializable{
	private  int pkErrLog;
    private  String fkBulkErrRow;
    private  String fkBulkTemDet;
    private  String errMsg;
    private  String creator;
    
    private  String ipAdd;
	@Column(name = "IP_ADD")
	public String getIpAdd() {
		return ipAdd;
	}
	public void setIpAdd(String ipAdd) {
		this.ipAdd = ipAdd;
	}
    @Column(name = "creator")
    public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
    
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "seq_er_bulk_upload", allocationSize=1)
    @Column(name = "PK_BULK_UPLOAD_ERRORLOG")
	public int getPkErrLog() {
		return this.pkErrLog;
	}
	public void setPkErrLog(int pkErrLog) {
		this.pkErrLog = pkErrLog;
	}
	@Column(name = "FK_BULK_UPLOAD_ERRORROW")
	public String getFkBulkErrRow() {
		return this.fkBulkErrRow;	
	}
	public void setFkBulkErrRow(String fkBulkErrRow) {
		this.fkBulkErrRow = fkBulkErrRow;
	}
	@Column(name = "FK_BULK_TEMPLATE_DETAIL")
	public String getFkBulkTemDet() {
		return this.fkBulkTemDet;
	}
	public void setFkBulkTemDet(String fkBulkTemDet) {
		this.fkBulkTemDet = fkBulkTemDet;
	}
	@Column(name = "ERROR_MESSAGE")
	public String getErrMsg() {
		return this.errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	public BulkErrLogBean() {

    }
	 public BulkErrLogBean( int pkErrLog,String fkBulkErrRow,String fkBulkTemDet,String errMsg,String creator,String ipAdd) {
		 	setPkErrLog(pkErrLog);
		 	setFkBulkErrRow(fkBulkErrRow);
		 	setFkBulkTemDet(fkBulkTemDet);
		 	setErrMsg(errMsg);
		 	setCreator(creator);
		 	setIpAdd(ipAdd);
}
	    public int updateBulkErrLog(BulkErrLogBean spsk) {
	        try {
	        	setPkErrLog(spsk.getPkErrLog());
	        	setFkBulkErrRow(spsk.getFkBulkErrRow());
	        	setFkBulkTemDet(spsk.getFkBulkTemDet());
	        	setErrMsg(spsk.getErrMsg());
	        	setCreator(spsk.getCreator());
	        	setIpAdd(spsk.getIpAdd());
	            return spsk.getPkErrLog();
	        } catch (Exception e) {
	            Rlog.fatal("bulkupload", " error in updateBulkErrLog.updateBulkErrLog" + e);
	            e.printStackTrace();
	            return -2;
	        }
	    }
}
