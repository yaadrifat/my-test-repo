package com.velos.eres.bulkupload.business;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

@Entity
@Table(name = "ER_BULK_UPLOAD")
public class BulkUploadBean implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private  int pkBulkUpload;
    private  Integer fkUser;
    private  Integer fkBulkTamplate;
    private  String fileName;
    private  Integer totRecord;
    private  Integer sucRecord;
    private  Integer unsucRecord;
    private  Integer  lastModBy;
    private  String ipAdd;
    private  String creator;
    
    @Column(name = "creator")
    public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "seq_er_bulk_upload", allocationSize=1)
    @Column(name = "PK_BULK_UPLOAD")
	public int getPkBulkUpload() {
		return this.pkBulkUpload;
	}
	public void setPkBulkUpload(int pkBulkUpload) {
		this.pkBulkUpload = pkBulkUpload;
	}
	@Column(name = "FK_USER")
	public String getFkUser() {
		return StringUtil.integerToString(this.fkUser);
	}
	public void setFkUser(String fkUser) {
		this.fkUser = StringUtil.stringToInteger(fkUser);
	}
	@Column(name = "FK_BULK_TEMPLATE_MASTER")
	public String getFkBulkTamplate() {
		return StringUtil.integerToString(this.fkBulkTamplate);
	}
	public void setFkBulkTamplate(String fkBulkTamplate) {
		this.fkBulkTamplate = StringUtil.stringToInteger(fkBulkTamplate);
	}
	@Column(name = "FILE_NAME")
	public String getFileName() {
		return this.fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	@Column(name = "TOTAL_RECORDS")
	public String getTotRecord() {
		return StringUtil.integerToString(this.totRecord);
	}
	public void setTotRecord(String totRecord) {
		this.totRecord = StringUtil.stringToInteger(totRecord);
	}
	@Column(name = "SUCCESSFUL_RECORDS")
	public String getSucRecord() {
		return StringUtil.integerToString(this.sucRecord);
	}
	public void setSucRecord(String sucRecord) {
		this.sucRecord = StringUtil.stringToInteger(sucRecord);
	}
	@Column(name = "UNSUCCESSFUL_RECORDS")
	public String getUnSucRecord() {
		return StringUtil.integerToString(this.unsucRecord);
	}
	public void setUnSucRecord(String unsucRecord) {
		this.unsucRecord = StringUtil.stringToInteger(unsucRecord);
	}
	@Column(name = "LAST_MODIFIED_BY")
	public String getLastModBy() {
		return StringUtil.integerToString(this.lastModBy);
	}
	public void setLastModBy(String lastModBy) {
		this.lastModBy = StringUtil.stringToInteger(lastModBy);
	}
	@Column(name = "IP_ADD")
	public String getIpAdd() {
		return this.ipAdd;
	}
	public void setIpAdd(String ipAdd) {
		this.ipAdd = ipAdd;
	}

    public BulkUploadBean() {

    }
	 public BulkUploadBean( int  pkBulkUpload,String fkUser,String fkBulkTem,String fileName,String totRec,String sucRecord,
			String  lastModBy,String unSucc,String creator,String ipAdd) {
		 	setPkBulkUpload(pkBulkUpload);
			setFkUser(fkUser);
			setFkBulkTamplate(fkBulkTem);
			setFileName(fileName);
			setTotRecord(totRec);
			setSucRecord(sucRecord);
			setLastModBy(lastModBy);
			setIpAdd(ipAdd);
			setCreator(creator);
			setUnSucRecord(unSucc);
}
	    public int updateBulkUpload(BulkUploadBean spsk) {
	        try {
			 	setPkBulkUpload(spsk.getPkBulkUpload());
				setFkUser(spsk.getFkUser());
				setFkBulkTamplate(spsk.getFkBulkTamplate());
				setFileName(spsk.getFileName());
				setTotRecord(spsk.getTotRecord());
				setSucRecord(spsk.getSucRecord());
				setLastModBy(spsk.getLastModBy());
				setUnSucRecord(spsk.getUnSucRecord());
				setIpAdd(spsk.getIpAdd());
				setCreator(spsk.getCreator());
	            return spsk.getPkBulkUpload();
	        } catch (Exception e) {
	            Rlog.fatal("bulkupload", " error in BulkUploadBean.updateBulkUpload" + e);
	            e.printStackTrace();
	            return -2;
	        }
	    }
}
