package com.velos.eres.bulkupload.business;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.bulkupload.business.BULKSQLS;
import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.Rlog;

public class BulkDao extends CommonDAO implements java.io.Serializable{
	
	 private int pkBulk;
	 private String bulkType;
	 private ArrayList<Integer> patprotid=null;
	 private ArrayList<String> patprotstd=null;
	 private ArrayList<String> patprotpat=null;
	 private ArrayList<String> patprotorg=null;
	 private ArrayList<Integer> userAcc=null;
	 private ArrayList<String> userAccFirstName=null;
	 private ArrayList<String> userAccMidName=null;;
	 private ArrayList<String> userAccLastName=null;
	 private int patprotoprot;
	 private ArrayList<Integer> parentSpec=null;
	 private ArrayList<String> pkPer=null;
	 private ArrayList<String> fkSite=null;
	 public ArrayList<String> getPkPer() {
		return this.pkPer;
	}
	public void setPkPer(ArrayList<String> pkPer) {
		this.pkPer = pkPer;
	}
	public void setPkPer(String pkPer) {
		this.pkPer.add(pkPer);
	}
	public ArrayList<String> getFkSite() {
		return this.fkSite;
	}
	public void setFkSite(ArrayList<String> fkSite) {
		this.fkSite = fkSite;
	}
	public void setFkSite(String fkSite) {
		this.fkSite.add(fkSite);
	}
	 public ArrayList<Integer> getParentSpec() {
		return parentSpec;
	}
	public void setParentSpec(ArrayList<Integer> parentSpec) {
		this.parentSpec = parentSpec;
	}
	public void setParentSpec(int parentSpec) {
		this.parentSpec.add(parentSpec);
	}
	private String patprotocolname;
	 public String getPatprotocolname() {
		return this.patprotocolname;
	}
	public void setPatprotocolname(String patprotocolname) {
		this.patprotocolname = patprotocolname;
	}
	 public int getPatprotprot() {
			return this.patprotoprot;
		}
	public void setPatprotprot(int patprotoprot) {
			this.patprotoprot=patprotoprot;
		}
	 public ArrayList getPatprotOrg() {
			return patprotorg;
		}
	public void setPatprotOrg(String patprotstd) {
			this.patprotorg.add(patprotstd);
		}
	public void setPatprotOrg(ArrayList patprotorg) {
			this.patprotorg=patprotorg;
		}
	public ArrayList getPatprotstd() {
		return patprotstd;
	}
	public void setPatprotstd(String patprotstd) {
		this.patprotstd.add(patprotstd);
	}
	public void setPatprotstd(ArrayList patprotstd) {
		this.patprotstd=patprotstd;
	}
	public ArrayList getPatprotpat() {
		return patprotpat;
	}
	public void setPatprotpat(String patprotpat) {
		this.patprotpat.add(patprotpat);
	}
	public void setPatprotpat(ArrayList patprotpat) {
		this.patprotpat=patprotpat;
	}
	public ArrayList getPatprotid() {
		return patprotid;
	}
	public void setPatprotid(int patprotid) {
		this.patprotid.add(patprotid);
	}
	public void setPatprotid(ArrayList patprotid) {
		this.patprotid=patprotid;
	}
	public int getPkBulk() {
	     return this.pkBulk;
	 }
	 public void setPkBulk(int pkBulk) {
		this.pkBulk = pkBulk;
	 }
	 public String getBulkType() {
		return bulkType;
	 }

     public void setBulkType(String bulkType) {
		this.bulkType = bulkType;
	} 
    public BulkDao() {
    	patprotid=new ArrayList();
    	patprotpat=new ArrayList();
    	patprotstd=new ArrayList();
    	patprotorg=new ArrayList();
    	parentSpec=new ArrayList<Integer>();
    	userAcc=new ArrayList<Integer>();
    	userAccFirstName=new ArrayList<String>();
    	userAccLastName=new ArrayList<String>();
    	userAccMidName=new ArrayList<String>();
    	pkPer=new ArrayList<String>();
    	fkSite=new ArrayList<String>();
    	
    }
    public ArrayList<String> getUserAccFirstName() {
		return userAccFirstName;
	}
	public void setUserAccFirstName(ArrayList<String> userAccFirstName) {
		this.userAccFirstName = userAccFirstName;
	}
	public void setUserAccFirstName(String userAccFirstName) {
		this.userAccFirstName.add(userAccFirstName);
	}
	public ArrayList<String> getUserAccMidName() {
		return userAccMidName;
	}
	public void setUserAccMidName(ArrayList<String> userAccMidName) {
		this.userAccMidName = userAccMidName;
	}
	public void setUserAccMidName(String userAccMidName) {
		this.userAccMidName.add(userAccMidName);
	}
	public ArrayList<String> getUserAccLastName() {
		return userAccLastName;
	}
	public void setUserAccLastName(ArrayList<String> userAccLastName) {
		this.userAccLastName = userAccLastName;
	}
	public void setUserAccLastName(String userAccLastName) {
		this.userAccLastName.add(userAccLastName);
	}
	public ArrayList<Integer> getUserAcc() {
		return userAcc;
	}
	public void setUserAcc(ArrayList<Integer> userAcc) {
		this.userAcc = userAcc;
	}
	public void setUserAcc(int userAcc) {
		this.userAcc.add(userAcc);
	}
	public boolean getBulkPkValue(String c) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        setBulkType(c);
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(BULKSQLS.erbulkupld);
            pstmt.setString(1, getBulkType());
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
            	setPkBulk(rs.getInt("PK_BULK_ENTITY_MASTER"));
            }
            return true;
        } catch (SQLException ex) {
            Rlog.fatal("bulkupload", "EXCEPTION IN FETCHING FROMBULKUPLOADMASTER TABLE "
                    + ex);
            return false;
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
    }
    public boolean getPatProtDet(String patprotid) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(BULKSQLS.erbulkpatprotdet);
            pstmt.setString(1,patprotid);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
            	setPatprotid(rs.getInt("pk_patprot")); 
            	setPatprotstd(rs.getString("fk_study"));
            	setPatprotpat(rs.getString("fk_per"));
            	setPatprotOrg(rs.getString("fk_site_enrolling"));
            }
            return true;
        } catch (SQLException ex) {
            Rlog.fatal("bulkupload", "EXCEPTION IN FETCHING FROM PatProt TABLE in BULKUPLOADDAO"
                    + ex);
            return false;
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
    }
    public boolean getPatProtFkProtocol(String patprotid) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(BULKSQLS.erbulkpatprotfk);
            pstmt.setString(1,patprotid);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) { 
            	setPatprotprot(rs.getInt("fk_protocol"));
            }
            return true;
        } catch (SQLException ex) {
            Rlog.fatal("bulkupload", "EXCEPTION IN FETCHING FROM PatProt TABLE in BULKUPLOADDAO"
                    + ex);
            return false;
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
    }
    public boolean getPatProtProtocolName(String patprotid) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(BULKSQLS.erbulkpatprotName);
            pstmt.setString(1,patprotid);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) { 
            	setPatprotocolname(rs.getString("name"));
            }
            return true;
        } catch (SQLException ex) {
            Rlog.fatal("bulkupload", "EXCEPTION IN FETCHING FROM PatProt TABLE in BULKUPLOADDAO"
                    + ex);
            return false;
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
    }
    public boolean getParentSpecimen(String parentSpec,String fkAccount) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        int count=0;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(BULKSQLS.erbulkgetparentSpec);
            pstmt.setString(1,fkAccount);
            pstmt.setString(2,parentSpec);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) { 
            	setParentSpec(rs.getInt(1));
            }
            return true;
        } catch (SQLException ex) {
            Rlog.fatal("bulkupload", "EXCEPTION IN FETCHING FROM PatProt TABLE in BULKUPLOADDAO"
                    + ex);
            return false;
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
    }
    public boolean getUserAccount(String fkAccount) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        int count=0;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(BULKSQLS.erbulkgetuseracc);
            pstmt.setString(1,fkAccount);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) { 
            	setUserAcc(rs.getInt("pk_user"));
            	setUserAccFirstName(rs.getString("USR_FIRSTNAME"));
            	setUserAccMidName(rs.getString("USR_MIDNAME"));
            	setUserAccLastName(rs.getString("USR_LASTNAME"));
            }
            return true;
        } catch (SQLException ex) {
            Rlog.fatal("bulkupload", "EXCEPTION IN FETCHING FROM PatProt TABLE in BULKUPLOADDAO"
                    + ex);
            return false;
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
    }   
    public boolean getPatientData(String percode) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        int count=0;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(BULKSQLS.erbulkgetpatient);
            pstmt.setString(1,percode);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) { 
            	setPkPer(rs.getString("PK_PER"));
            	setFkSite(rs.getString("FK_SITE"));
            }
            return true;
        } catch (SQLException ex) {
            Rlog.fatal("bulkupload", "EXCEPTION IN FETCHING FROM PatProt TABLE in BULKUPLOADDAO"
                    + ex);
            return false;
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
    }   
}
