package com.velos.eres.dashboard.web;

import java.util.ArrayList;
import java.util.HashMap;

import com.velos.eres.dashboard.business.DashboardDAO;

public class DashboardJB {

	public ArrayList<HashMap<String, Object>> dbFetchCodeLstStaues(
			String codelstType) {
		// TODO route through EJB Service
		DashboardDAO dbDAO = new DashboardDAO();
		return dbDAO.dbFetchCodeLstStaues(codelstType);
	}
	
	 /*       Newly Added for delete functionality in DashBoardAction  */
	   public int dbDeleteSavedSearches(
			String saveSearch,String modulename) {
		// TODO route through EJB Service
		DashboardDAO dbDAO = new DashboardDAO();
		return dbDAO.dbDeleteSavedSearches(saveSearch,modulename);
	}
       
	
	
	
	public ArrayList<HashMap<String, Object>> dbFetchFormAdvStaues(
			Integer userId,Integer accountId,String codelstType, Integer tabType,String studyNum) {
		// TODO route through EJB Service
		DashboardDAO dbDAO = new DashboardDAO();
		return dbDAO.dbFetchFormAdvStaues(userId,accountId,codelstType, tabType,studyNum);
	}
	
	public ArrayList<HashMap<String, Object>> dbFetchSavedSearches(
			String searchModule, Integer userId) {
		// TODO route through EJB Service
		DashboardDAO dbDAO = new DashboardDAO();
		return dbDAO.dbFetchSavedSearches(searchModule, userId);
	}

	public ArrayList<HashMap<String, Object>> dbFetchStudies(Integer userId,
			Integer accountId, String studyNum) {
		// TODO route through EJB Service
		DashboardDAO dbDAO = new DashboardDAO();
		return dbDAO.dbFetchStudies(userId, accountId, studyNum);
	}
	
	public ArrayList<HashMap<String, Object>> dbFetchOrganizations(Integer userId,
			Integer accountId, String studyNum, String orgName, Integer tabType, String selectedOrgId) {
		// TODO route through EJB Service
		DashboardDAO dbDAO = new DashboardDAO();
		return dbDAO.dbFetchOrganizations(userId, accountId, studyNum, orgName, tabType, selectedOrgId);
	}
	
	public ArrayList<HashMap<String, Object>> dbFetchForms(Integer userId,
			Integer accountId, String studyNum, String orgId, String formName, Integer tabType) {
		// TODO route through EJB Service
		DashboardDAO dbDAO = new DashboardDAO();
		return dbDAO.dbFetchForms(userId, accountId, studyNum, orgId, formName, tabType);
	}
	
	public ArrayList<HashMap<String, Object>> dbFetchFormStatues(Integer userId,
			Integer accountId,String studyNum, String statusDesc,Integer tabType) {
		// TODO route through EJB Service
		DashboardDAO dbDAO = new DashboardDAO();
		return dbDAO.dbFetchFormStatues(userId, accountId,studyNum, statusDesc,tabType);
	}
	
	public ArrayList<HashMap<String, Object>> dbFetchAEStatues(Integer userId,
			Integer accountId,String studyNum, String aeStatusDesc) {
		// TODO route through EJB Service
		DashboardDAO dbDAO = new DashboardDAO();
		return dbDAO.dbFetchAEStatues(userId, accountId,studyNum, aeStatusDesc);
	}
	
	public ArrayList<HashMap<String, Object>> dbFetchPatientIDs(Integer userId,
			Integer accountId, String studyNum, String orgId, String patientId) {
		// TODO route through EJB Service
		DashboardDAO dbDAO = new DashboardDAO();
		return dbDAO.dbFetchPatientIDs(userId, accountId, studyNum, orgId, patientId);
	}
	
	public ArrayList<HashMap<String, Object>> dbFetchPatientStudyIDs(Integer userId,
			Integer accountId, String studyNum, String orgId, String studyPatientId) {
		// TODO route through EJB Service
		DashboardDAO dbDAO = new DashboardDAO();
		return dbDAO.dbFetchPatientStudyIDs(userId, accountId, studyNum, orgId, studyPatientId);
	}
	
	public ArrayList<HashMap<String, Object>> dbFetchCalendars(Integer userId,
			Integer accountId, String studyNum, String orgId, String calenderName) {
		// TODO route through EJB Service
		DashboardDAO dbDAO = new DashboardDAO();
		return dbDAO.dbFetchCalendars(userId, accountId, studyNum, orgId, calenderName);
	}
	
	public ArrayList<HashMap<String, Object>> dbFetchVisits(Integer userId,
			Integer accountId, String calendarIds, String visitName) {
		// TODO route through EJB Service
		DashboardDAO dbDAO = new DashboardDAO();
		return dbDAO.dbFetchVisits(userId, accountId, calendarIds, visitName);
	}
	
	public ArrayList<HashMap<String, Object>> dbFetchEvents(Integer userId,
			Integer accountId, String visitIds, String eventName) {
		// TODO route through EJB Service
		DashboardDAO dbDAO = new DashboardDAO();
		return dbDAO.dbFetchEvents(userId, accountId, visitIds, eventName);
	}
	
	public ArrayList<HashMap<String, Object>> dbFetchQueryCreators(Integer userId,
			Integer accountId, String queryCreator) {
		// TODO route through EJB Service
		DashboardDAO dbDAO = new DashboardDAO();
		return dbDAO.dbFetchQueryCreators(userId, accountId, queryCreator);
	}
	
	public ArrayList<HashMap<String, Object>> dbFetchQueryStatuses(Integer userId,
			Integer accountId, String queryStatus) {
		// TODO route through EJB Service
		DashboardDAO dbDAO = new DashboardDAO();
		return dbDAO.dbFetchQueryStatuses(userId, accountId, queryStatus);
	}
	public int saveSearchFilter(String browserName,String searchUrl,String browsersearch_isdefault,String moduleName,int userId){
		DashboardDAO dbDAO = new DashboardDAO();
		return dbDAO.browserSaveFilter( browserName, searchUrl, browsersearch_isdefault,moduleName,userId);
	}
	public int saveViewFilter(String searchUrl,String browsersearch_isdefault,String moduleName,int userId,String viewCriteria){
		DashboardDAO dbDAO = new DashboardDAO();
		return dbDAO.browserSaveView( searchUrl, browsersearch_isdefault,moduleName,userId,viewCriteria);
	}
	public ArrayList<HashMap<String, Object>>  fetchSavedView(int userId){
		DashboardDAO dbDAO = new DashboardDAO();
		return(dbDAO.fetchSavedView(userId));
	}
	public ArrayList<HashMap<String, Object>>  getPatStudyFormRes(String studyNumber,String patStudyId){
		System.out.println("getPatStudyFormRes method in DashboardJB start");
		DashboardDAO dbDAO = new DashboardDAO();
		return(dbDAO.getPatStudyFormRes( studyNumber, patStudyId));
	}
	public ArrayList<HashMap<String, Object>>  getPatStudyFormQueryFetch(int filledForm){
		System.out.println("getPatStudyFormQueryFetch method in DashboardJB start and filledForm:-"+filledForm);
		DashboardDAO dbDAO = new DashboardDAO();
		return(dbDAO.getPatStudyFormQueryFetch(filledForm));
	}
	public ArrayList<HashMap<String, Object>> studyTeamRoleQueryFetch(int userId, int accountId,String studyIds,String tabSelected){
		System.out.println("studyTeamRoleQueryFetch method in DashboardJB start and studyIds:-"+studyIds);
		DashboardDAO dbDAO = new DashboardDAO();
		return(dbDAO.studyTeamRoleQueryFetch(userId,accountId,studyIds,tabSelected));
	}
	/*public ArrayList<HashMap<String, Object>> viewRightOrgBaseToUse(int userId, String siteId){
		System.out.println("find the viewRightOrgBaseToUse in DashboardJb class");
		DashboardDAO dbDAO = new DashboardDAO();
		return null; //(dbDAO.viewRightOrgBaseToUse(userId,siteId));fetchDefaultStRoleForSuper
	}*/
	public ArrayList<HashMap<String, Object>> fetchDefaultStRoleForSuper(int userId){
		System.out.println("find the fetchDefaultStRoleForSuper in DashboardJb class");
		DashboardDAO dbDAO = new DashboardDAO();
		return(dbDAO.fetchDefaultStRoleForSuper(userId)); //(dbDAO.viewRightOrgBaseToUse(userId,siteId));fetchDefaultStRoleForSuper
	}
}