package com.velos.eres.financials.web;

import com.velos.eres.financials.business.common.FinancialsDao;
import com.velos.eres.financials.service.FinancialsAgent;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

public class FinancialsJB {
	
	public FinancialsJB() {}
	
	private static final String GADGET_SAMPLE2JB_DOT = "FinancialsJB.";

	
	private String accountId = null;
	private String ipAdd = null;
	private String userId = null;
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public void setIpAdd(String ipAdd) {
		this.ipAdd = ipAdd;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}

    public FinancialsDao getMyInvoiceables(String studyIds) {
    	FinancialsDao finDao = null;
        try {
            FinancialsAgent finAgent = EJBUtil.getFinancialsAgentHome();
            finDao = finAgent.getMyInvoiceables(studyIds);
        } catch (Exception e) {
            Rlog.fatal("gadget",
                    "Error in FinancialsJB.getMyInvoiceables: "+e);
        }
        return finDao;
    }
}
