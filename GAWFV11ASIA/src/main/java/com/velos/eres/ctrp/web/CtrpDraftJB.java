package com.velos.eres.ctrp.web;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletResponse;

import com.velos.eres.business.address.impl.AddressBean;
import com.velos.eres.business.appendix.impl.AppendixBean;
import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.CtrpDraftDao;
import com.velos.eres.business.common.StatusHistoryDao;
import com.velos.eres.ctrp.business.CtrpDraftBean;
import com.velos.eres.ctrp.business.CtrpDraftNotificationBean;
import com.velos.eres.ctrp.service.CtrpDraftAgent;
import com.velos.eres.service.addressAgent.AddressAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.JNDINames;
import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.statusHistory.StatusHistoryJB;
import com.velos.eres.web.study.StudyJB;
import com.velos.eres.web.user.UserJB;

public class CtrpDraftJB {
	private CtrpDraftSectSponsRespJB ctrpDraftSectSponsorRespJB = null;
    private Integer loggedUser=0;
	private Integer id = 0;
	private String eSign = null;
	private String fkStudy = null;
	private String studyNumber = null;
	private String studyTitle = null;
	private Integer hideeSign = null;
	private Integer draftType = null;
	private String trialSubmTypeMenu = null;
	private Integer trialSubmTypeInt = null;
	private CodeDao trialSubmDao = null;
	private String trialSubmTypeA = null;
	private String trialSubmTypeO = null;
	private String trialSubmTypeU = null;
	private String trialSubmTypeAorO = null;
	private String trialSubmTypeAorU = null;
	private Integer draftXmlRequired = null;
	private List<RadioOption> draftXmlReqList = null;
	private String amendNumber = null;
	private String amendDate = null;
	private String nciTrialId = null;
	private String leadOrgTrialId = null;
	private String nctNumber = null;
	private String otherTrialId = null;
	private String studyPhase = null;
	private String draftStudyPhaseMenu = null;
	private Integer draftStudyPhase = null;
	private CodeDao draftStudyPhaseDao = null;
	private String draftStudyPhaseNA = null;
	private Integer draftPilot = null;
	private List<RadioOption> draftPilotList = null;
	private List<RadioOption> draftStudyTypeList = null;
	private Integer draftStudyType = null;
	private String studyType = null;
	private String studyPurposeMenu = null;
	private Integer studyPurpose = null;
	private CodeDao studyPurposeDao = null;
	private String studyPurposeOtherPk = null;
	private String studyPurposeOther = null;
	private CodeDao draftStatusDao = null;
	private Integer draftStatus = null;
	private String draftStatusMenu = null;
	private String leadOrgCtrpId = null;
	private String leadOrgFkSite = null;
	private String leadOrgTypeMenu = null;
	private Integer leadOrgTypeInt = null;
	private CodeDao leadOrgTypeDao = null;
	private String leadOrgName = null;
	private String leadOrgStreet = null;
	private String leadOrgCity = null;
	private String leadOrgState = null;
	private String leadOrgZip = null;
	private String leadOrgCountry = null;
	private String leadOrgEmail = null;
	private String leadOrgPhone = null;
	private String leadOrgFkAdd = null;
	private String leadOrgTty = null;
	private String leadOrgFax = null;
	private String leadOrgUrl = null;
	private String piId = null;
	private String piFkUser = null;
	private String piFirstName = null;
	private String piMiddleName = null;
	private String piLastName = null;
	private String piStreetAddress = null;
	private String piCity = null;
	private String piState = null;
	private String piZip = null;
	private String piCountry = null;
	private String piEmail = null;
	private String piPhone = null;
	private String piFkAdd = null;
	private String piTty = null;
	private String piFax = null;
	private String piUrl = null;
	
	private String studyDraftsMenu = null;
	private Integer studyDraftsInt = null;
	private CodeDao studyDraftsDao = null;
	//------------------------------CTRP-20552-7------------------
	private String indusTrialTypeMenu = null;
	private Integer indusTrialTypeInt = null;
	private String ctrpIntvnTypeMenu = null;
	private String interventionName = null;
	private Integer ctrpIntvnTypeInt = null;
	private CodeDao ctrpIntvnTypeDao = null;
	private String subOrgTrialId = null;
	private String subOrgTypeMenu = null;
	private Integer subOrgTypeInt = null;
	private CodeDao subOrgTypeDao = null;
	private Integer isNCIDesignated = null;
	private List<RadioOption> isNCIDesignatedList = null;
	private String trialUsrId=null;
	private String trialFirstName=null;
	private String trialLastName=null;
	private String trialEmailId=null;
	private String solIdentifierId = null;

	private String siteRecruitTypeMenu = null;
	private Integer siteRecruitTypeInt = null;
	private CodeDao siteRecruitDao = null;
	private String recStatusDate = null;
	private String openAccrualDate = null;
	private String closedAccrualDate = null;
	private String siteTrgtAccrual = null;
	private String activeStatusValidFrom = null;

	private String studyEnrollClosedDt=null;
	private String studyDiseaseSite=null;
	private String studyDiseaseName=null;
	private String summ4CtrpId=null;
	private Integer summ4TypeInt=null;
	private String summ4Type=null;
	private String summ4TypeFk=null;
	private String summ4FkSite=null;
	private String summ4FkAdd=null;
	private String summ4Name=null;
	private String summ4Email=null;
	private String summ4Street=null;
	private String summ4City=null;
	private String summ4Phone=null;
	private String summ4State=null;
	private String summ4Tty=null;
	private String summ4Zip=null;
	private String summ4Fax=null;
	private String summ4Country=null;
	private String summ4Url=null;
	
	private String subOrgCtrpId = null;
	private String subOrgName = null;
	private String subOrgStreet = null;
	private String subOrgCity = null;
	private String subOrgState = null;
	private String subOrgZip = null;
	private String subOrgCountry = null;
	private String subOrgEmail = null;
	private String subOrgPhone = null;
	private String subOrgFkSite = null;
	private String subOrgFkAdd = null;
	private String subOrgTty = null;
	private String subOrgFax = null;
	private String subOrgUrl = null;
	private String subOrgProgCode = null;
	
	private Integer abbrTrialTempPK=0;
	private String abbrTrialTempDesc=null;	
	private Integer trialAbbrTrialTempPK=0;
	
	private CodeDao currntStudyStatusDao = null;
	private CtrpDraftDao userStatusDao=null;
	private String currntStudyStatus = null;
	private String studyStatusChckLst = null;
	private String studyStatusUserLst = null;
	private String statusCountVal=null;
	private String EmailUserId=null;	
	private Integer fkUser;
	private String protocolStatus = null;
	
	
	
	public static final String FLD_CTRP_ABBTRIAL_DOC_PK = "ctrpDraftJB.abbrTrialTempPK";
	public static final String FLD_CTRP_ABBTRIAL_DOC_DESC = "ctrpDraftJB.abbrTrialTempDesc";
	public static final String FLD_CTRP_OTHER_DOC_PK = "ctrpDraftJB.otherPK";

	private static final String CTRP_INTERVENTION_TYPE = "ctrpIntvnType";
	private static final String SITE_RECRUIT_TYPE = "ctrpRecruitStat";
	public static final String FLD_SUBORG_TRIAL_ID = "ctrpDraftJB.subOrgTrialId";
	public static final String FLD_CTRP_INTERVENTION_TYPE = "ctrpDraftJB.ctrpIntvnType";
	public static final String FLD_SITE_RECRUIT_STAT = "ctrpDraftJB.siteRecruitType";
	public static final String FLD_CTRP_INTERVENTION_NAME = "ctrpDraftJB.interventionName";
	public static final String FLD_STUDY_DISEASE_SITE = "ctrpDraftJB.studyDiseaseSite";

	public static final String FLD_SITE_RECRUIT_DATE = "ctrpDraftJB.recStatusDate";
	public static final String FLD_OPEN_FOR_ACCRUAL = "ctrpDraftJB.openAccrualDate";
	public static final String FLD_CLOSED_FOR_ACCRUAL = "ctrpDraftJB.closedAccrualDate";
	public static final String FLD_SITE_TRGT_ACRUAL = "ctrpDraftJB.siteTrgtAccrual";
	public static final String FLD_LEAD_ORG_ID = "ctrpDraftJB.leadOrgCtrpId";
	public static final String FLD_LEAD_ORG_FK_SITE = "ctrpDraftJB.leadOrgFkSite";
	public static final String FLD_LEAD_ORG_FK_ADD = "ctrpDraftJB.leadOrgFkAdd";
	public static final String FLD_LEAD_ORG_NAME = "ctrpDraftJB.leadOrgName";
	public static final String FLD_LEAD_ORG_STREET = "ctrpDraftJB.leadOrgStreet";
	public static final String FLD_LEAD_ORG_CITY = "ctrpDraftJB.leadOrgCity";
	public static final String FLD_LEAD_ORG_STATE = "ctrpDraftJB.leadOrgState";
	public static final String FLD_LEAD_ORG_ZIP = "ctrpDraftJB.leadOrgZip";
	public static final String FLD_LEAD_ORG_COUNTRY = "ctrpDraftJB.leadOrgCountry";
	public static final String FLD_LEAD_ORG_EMAIL = "ctrpDraftJB.leadOrgEmail";

	public static final String FLD_PI_ID = "ctrpDraftJB.piId";
	public static final String FLD_PI_FK_USER = "ctrpDraftJB.piFkUser";
	public static final String FLD_PI_FK_ADD = "ctrpDraftJB.piFkAdd";
	public static final String FLD_PI_STREET = "ctrpDraftJB.piStreetAddress";
	public static final String FLD_PI_CITY = "ctrpDraftJB.piCity";
	public static final String FLD_PI_STATE = "ctrpDraftJB.piState";
	public static final String FLD_PI_ZIP = "ctrpDraftJB.piZip";
	public static final String FLD_PI_COUNTRY = "ctrpDraftJB.piCountry";
	public static final String FLD_PI_EMAIL = "ctrpDraftJB.piEmail";
	public static final String FLD_PI_PHONE = "ctrpDraftJB.piPhone";
	
	public static final String FLD_SUB_ORG_ID = "ctrpDraftJB.subOrgCtrpId";
	public static final String FLD_SUB_ORG_FK_SITE = "ctrpDraftJB.subOrgFkSite";
	public static final String FLD_SUB_ORG_FK_ADD = "ctrpDraftJB.subOrgFkAdd";
	public static final String FLD_SUB_ORG_NAME = "ctrpDraftJB.subOrgName";
	public static final String FLD_SUB_ORG_STREET = "ctrpDraftJB.subOrgStreet";
	public static final String FLD_SUB_ORG_CITY = "ctrpDraftJB.subOrgCity";
	public static final String FLD_SUB_ORG_STATE = "ctrpDraftJB.subOrgState";
	public static final String FLD_SUB_ORG_ZIP = "ctrpDraftJB.subOrgZip";
	public static final String FLD_SUB_ORG_COUNTRY = "ctrpDraftJB.subOrgCountry";
	public static final String FLD_SUB_ORG_EMAIL = "ctrpDraftJB.subOrgEmail";
	public static final String FLD_SUB_ORG_PROG_CODE = "ctrpDraftJB.subOrgProgCode";
	
	public static final String FLD_SUMM4_ID = "ctrpDraftJB.summ4CtrpId";
	public static final String FLD_SUMM4_TYPE = "ctrpDraftJB.summ4Type";
	public static final String FLD_SUMM4_TYPE_INT = "ctrpDraftJB.summ4TypeInt";
	public static final String FLD_SUMM4_FK_SITE = "ctrpDraftJB.summ4FkSite";
	public static final String FLD_SUMM4_FK_ADD = "ctrpDraftJB.summ4FkAdd";
	public static final String FLD_SUMM4_NAME = "ctrpDraftJB.summ4Name";
	public static final String FLD_SUMM4_STREET = "ctrpDraftJB.summ4Street";
	public static final String FLD_SUMM4_CITY = "ctrpDraftJB.summ4City";
	public static final String FLD_SUMM4_STATE = "ctrpDraftJB.summ4State";
	public static final String FLD_SUMM4_ZIP = "ctrpDraftJB.summ4Zip";
	public static final String FLD_SUMM4_COUNTRY = "ctrpDraftJB.summ4Country";
	public static final String FLD_SUMM4_EMAIL = "ctrpDraftJB.summ4Email";
	
	public static final String FLD_NIH_GRANTS = "ctrpDraftJB.trialNIHGrants";
	public static final String FLD_INDIDE = "ctrpDraftJB.trialIndIde";
	
	//------Added to solve Bug#8844 : Raviesh
	public static final String FLD_PROT_DOC_PK = "ctrpDraftJB.protocolDocPK";
	public static final String FLD_IRB_APPR_PK = "ctrpDraftJB.IRBApprovalPK";
	public static final String FLD_PART_SITES_LIST_PK = "ctrpDraftJB.partSitesListPK";
	public static final String FLD_INFORM_CONSENT_PK = "ctrpDraftJB.informedConsentPK";
	public static final String FLD_CHANGE_MEMO_DOC_PK = "ctrpDraftJB.changeMemoDocPK";
	public static final String FLD_PROT_HIGHLIGHT_PK = "ctrpDraftJB.protHighlightPK";

	//------------------------------CTRP-20552-7------------------
	
	private String accountId = null;
	private String studyIds = null;
	private String ctrpUnmark = null;
	private String userIdS = null;
	private String grpId = null;
	private String groupName = null;
	private String rightSeq = null;
	private String superuserRights = null;
	private String pmtclsId = null;
	
	//Start Regulatory Info section: Start AGodara
	private Integer fdaIntvenIndicator = null;
	private List<RadioOption> fdaIntvenIndicatorList = null;
	private Integer section801Indicator = null;
	private List<RadioOption> section801IndicatorList = null;
	private Integer delayPostIndicator = null;
	private List<RadioOption> delayPostIndicatorList = null;
	private Integer dmAppointIndicator = null;
	private List<RadioOption> dmAppointIndicatorList = null;
	  
	

	private CodeDao oversightCountryDao = null;
	private CodeDao oversightOrgDao = null;
	private Integer oversightCountry = null;
	private String oversightCountryMenu = null;
	private Integer oversightOrganization = null;
	
	public Integer getOversightCountry() {
		return oversightCountry;
	}

	public void setOversightCountry(Integer oversightCountry) {
		this.oversightCountry = oversightCountry;
		if(oversightCountry != null)
		oversightCountryMenu = oversightCountryDao.toPullDown(FLD_OVERSIGHTCOUNTRY, oversightCountry,ON_CHANGE_OVERSIGHT_COUNTRY);
	}
	
	
	private String oversightOrgMenu = null;
	
	public Integer getOversightOrganization() {
		return oversightOrganization;
	}

	public void setOversightOrganization(Integer oversightOrganization) {
		this.oversightOrganization = oversightOrganization;
		if(oversightOrganization != null && oversightCountry!=null){
			oversightOrgDao = new CodeDao();
			String codesubtype = oversightOrgDao.getCodeSubtype(oversightCountry);
			oversightOrgDao.getCodeValuesFilterCustom1(OVERSIGHT_ORGANIZATION, codesubtype);
			oversightOrgMenu = oversightOrgDao.toPullDown(FLD_OVERSIGHTORGANIZATION, oversightOrganization,ON_CHANGE_OVERSIGHT_ORGANIZATION);
		}
	}
	
	public void setOversightCountryMenu(String oversightCountryMenu) {
		this.oversightCountryMenu = oversightCountryMenu;
	}
	public String getOversightCountryMenu() {
		return oversightCountryMenu;
	}
	
	
	public void setOversightOrgMenu(String oversightOrgMenu) {
		this.oversightOrgMenu = oversightOrgMenu;
	}
	public String getOversightOrgMenu() {
		return oversightOrgMenu;
	}
	
	public Integer getFdaIntvenIndicator() {
		return fdaIntvenIndicator;
	}
	public void setFdaIntvenIndicator(Integer fdaIntvenIndicator) {
		this.fdaIntvenIndicator = fdaIntvenIndicator;
	}
	
	public List<RadioOption> getFdaIntvenIndicatorList() {
		return fdaIntvenIndicatorList;
	}
	public void setFdaIntvenIndicatorList(List<RadioOption> fdaIntvenIndicatorList) {
		this.fdaIntvenIndicatorList = fdaIntvenIndicatorList;
	}
	
	public Integer getSection801Indicator() {
		return section801Indicator;
	}
	public void setSection801Indicator(Integer section801Indicator) {
		this.section801Indicator = section801Indicator;
	}
	
	public List<RadioOption> getSection801IndicatorList() {
		return section801IndicatorList;
	}
	public void setSection801IndicatorList(List<RadioOption> section801IndicatorList) {
		this.section801IndicatorList = section801IndicatorList;
	}
	
	public Integer getDelayPostIndicator() {
		return delayPostIndicator;
	}
	public void setDelayPostIndicator(Integer delayPostIndicator) {
		this.delayPostIndicator = delayPostIndicator;
	}
	
	public List<RadioOption> getDelayPostIndicatorList() {
		return delayPostIndicatorList;
	}
	public void setDelayPostIndicatorList(List<RadioOption> delayPostIndicatorList) {
		this.delayPostIndicatorList = delayPostIndicatorList;
	}
	
	public Integer getDmAppointIndicator() {
		return dmAppointIndicator;
	}
	public void setDmAppointIndicator(Integer dmAppointIndicator) {
		this.dmAppointIndicator = dmAppointIndicator;
	}
		
	public List<RadioOption> getDmAppointIndicatorList() {
		return dmAppointIndicatorList;
	}
	public void setDmAppointIndicatorList(List<RadioOption> dmAppointIndicatorList) {
		this.dmAppointIndicatorList = dmAppointIndicatorList;
	}
	
	public static final String FLD_OVERSIGHTCOUNTRY = "ctrpDraftJB.oversightCountry";
	public static final String FLD_OVERSIGHTORGANIZATION = "ctrpDraftJB.oversightOrganization";
	
	private static final String OVERSIGHT_COUNTRIES = "ctrpCountry";
	private static final String OVERSIGHT_ORGANIZATION = "ctrpOverAuth";
	private static final String ON_CHANGE_OVERSIGHT_COUNTRY = "id='"+FLD_OVERSIGHTCOUNTRY+"' onChange='callAjaxGetOversightOrgDD();'";
	private static final String ON_CHANGE_OVERSIGHT_ORGANIZATION= "id='"+FLD_OVERSIGHTORGANIZATION+"'";
	
	// End Regulatory Info section: AGodara
	
	public static final String FLD_E_SIGN = "ctrpDraftJB.eSign";
	public static final String FLD_AMEND_NUM = "ctrpDraftJB.amendNumber";
	public static final String FLD_AMEND_DATE = "ctrpDraftJB.amendDate";
	public static final String FLD_NCI_TRIAL_ID = "ctrpDraftJB.nciTrialId";
	public static final String FLD_ORG_TRIAL_ID = "ctrpDraftJB.leadOrgTrialId";
	public static final String FLD_NCT_NUM = "ctrpDraftJB.nctNumber";
	public static final String FLD_OTHER_TRIAL_ID = "ctrpDraftJB.otherTrialId";
	
	public static final String FLD_STUDY_TYPE = "ctrpDraftJB.draftStudyType";
	public static final String FLD_SUBM_TYPE = "ctrpDraftJB.ctrpSubmType";
	public static final String FLD_XML_REQD = "ctrpDraftJB.draftXmlRequired";
	public static final String FLD_STUDY_PHASE = "ctrpDraftJB.ctrpStudyPhase";
	public static final String FLD_STUDY_PURPOSE = "ctrpDraftJB.studyPurpose";
	public static final String FLD_STUDY_PURPOSE_OTHER = "ctrpDraftJB.studyPurposeOther";
	public static final String FLD_DRAFT_STATUS = "ctrpDraftJB.ctrpDraftStatus";
	public static final String FLD_LEAD_ORG_TYPE = "ctrpDraftJB.leadOrgType";
	public static final String FLD_SUB_ORG_TYPE = "ctrpDraftJB.subOrgType";

	public static final String TRIAL_OWNER_ID="ctrpDraftJB.trialUsrId";
	public static final String TRIAL_OWNER_FNAME="ctrpDraftJB.trialFirstName";
	public static final String TRIAL_OWNER_LNAME="ctrpDraftJB.trialLastName";
	public static final String TRIAL_OWNER_EMAIL="ctrpDraftJB.trialEmailId";

	public static final String ER_CTRP_DRAFT = "er_ctrp_draft";
	public static final String STATUS_CODE_ID = "statusCodeId";

	private static final String USER_ID = "userId";
	private static final String IP_ADD = "ipAdd";
	private static final String LETTER_A = "A";
	private static final String LETTER_M = "M";
	private static final String LETTER_N = "N";
	private static final String LETTER_O = "O";
	private static final String LETTER_U = "U";
	private static final String LETTER_NA= "NA";
	private static final String STRING_OTHER = "other";
	private static final String STRING_SUBMITTED = "submitted";
	private static final String STRING_READY = "readySubmission";
	private static final String EMPTY_STRING = "";
	private static final String CTRP_SUBM_TYPE = "ctrpSubmType";
	private static final String CTRP_STUDY_PHASE = "ctrpStudyPhase";
	private static final String CTRP_STUDY_PURPOSE = "studyPurpose";
	public static final String CTRP_DRAFT_STATUS = "ctrpDraftStatus";
	private static final String CTRP_ORG_TYPE = "ctrpOrgType";
	private static final String studyDraftCodeType = "ctrpDraftSearch";
	private static final String ON_CHANGE_SUBM_TYPE = "id='"+FLD_SUBM_TYPE+"' onChange='submTypeAction();'";
	private static final String ON_CHANGE_STUDY_PHASE = "id='"+FLD_STUDY_PHASE+"' onChange='studyPhaseAction();'";
	private static final String ON_CHANGE_STUDY_PURPOSE = "id='"+FLD_STUDY_PURPOSE+"' onChange='studyPurposeAction();'";
	private static final String ON_CHANGE_DRAFT_STATUS = "id='"+FLD_DRAFT_STATUS+"'";
	
	// Added for CTRP-20579, Ankit (Start)
	
	public static final String ADMIN_COMP = "adminComplete ";
	public static final String WITHDRAWN = "withdrawn";
	public static final String TEMPCLOSEDACC = "tempClosedAcc";
	
	private String statusTypeAdminComplete = null;
	private String statusTypeWithdrawn = null;
	private String statusTypeTempClosedAcc = null;
	
	private String draftTrialStatusMenu = null;
	private CodeDao trialStatusDao = null;
	
	private Integer trialStatus = 0;
	private String trialStatusDate = null;
	private String studyStopReason = null;
	private String trialStartDate = null;
	private Integer trialStartType = null;
	private String primaryCompletionDate = null;
	private Integer primaryCompletionType = null;
	
	private String studyEndDate = null;
	private String studyActiveDate = null;
	private String statusValidFrm = null;
	private String currStudyStatus = null;
	
	public static final String FLD_TRIAL_STATUS = "ctrpDraftJB.trialStatus";
	public static final String FLD_TRIAL_STATUS_DATE = "ctrpDraftJB.trialStatusDate";
	public static final String FLD_STUDY_STOP_REASON = "ctrpDraftJB.studyStopReason";
	public static final String FLD_TRIAL_START_DATE = "ctrpDraftJB.trialStartDate";
	public static final String FLD_TRIAL_START_TYPE = "ctrpDraftJB.trialStartType";
	public static final String FLD_PRIMARY_COMPLETION_DATE = "ctrpDraftJB.primaryCompletionDate";
	public static final String FLD_PRIMARY_COMPLETION_TYPE = "ctrpDraftJB.primaryCompletionType";
	
	private static final String CTRP_STUDY_STATUS = "ctrpStudyStatus";
	private static final String ON_CHANGE_TRIAL_STATUS = "id='"+FLD_TRIAL_STATUS+"'";
	private List<RadioOption> trialStartTypeList = null;
	private List<RadioOption> primaryCompletionTypeList = null;
	//Added for CTRP-20579, Ankit  (End) 
	//------------------------------CTRP-20579-6------------------
	//AK : Added for the requirementCTRP-20579-6	
	private Integer protocolDocPK=0;
	private Integer IRBApprovalPK=0;
	private Integer partSitesListPK =0;
	private Integer informedConsentPK=0;
	private Integer changeMemoDocPK=0;
	private Integer protHighlightPK=0;
	private Integer otherPK=0;
	
	///Description fields
	private String protocolDocDesc=null;
	private String IRBApprovalDesc=null;		
	private String partSitesListDesc=null;	
	private String informedConsentDesc=null;
	private String changeMemoDocDesc=null;	
	private String protHighlightDesc=null;
	private String otherDesc=null;

	// Trail Doc PK value
	private Integer trialProtocolDocPK=0;
	private Integer trialIRBApprovalPK=0;
	private Integer trialPartSitesListPK =0;
	private Integer trialInformedConsentPK=0;
	private Integer trialChangeMemoDocPK=0;
	private Integer trialProtHighlightPK=0;
	private Integer trialOtherPK=0;

	public static final String CDLST_TYPE="ctrpDocType";                      
    public static final String CDLST_OTHER="other";                      
    public static final String CDLST_ABBRTRIALTEMPL="abbrTrialTempl";     
    public static final String CDLST_PROTHIGHLIGHT="protHighlight";
    public static final String CDLST_CHANGEMEMODOC="changeMemoDoc";
    public static final String CDLST_INFORMEDCONSENT="informedConsent"; 
    public static final String CDLST_PARTSITESLIST="PartSitesList";   
    public static final String CDLST_IRBAPPROVAL="IRBApproval";    
    public static final String CDLST_PROTOCOLDOC="protocolDoc";         

	public static final String FLD_CTRP_OTHER_DOC_DESC = "ctrpDraftJB.otherDesc";
	public static final String FLD_CTRP_PROTHIGHLIGHT_DOC_DESC = "ctrpDraftJB.protHighlightDesc";
	public static final String FLD_CTRP_CHANGEMEMODOC_DESC = "ctrpDraftJB.changeMemoDocDesc";
	public static final String FLD_CTRP_INFORMEDCONSENT_DOC_DESC = "ctrpDraftJB.informedConsentDesc";
	public static final String FLD_CTRP_PARTSITESLIST_DESC = "ctrpDraftJB.partSitesListDesc";
	public static final String FLD_CTRP_IRBAPPROVAL_DESC = "ctrpDraftJB.IRBApprovalDesc";
	public static final String FLD_CTRPDOC_PROTOCOLDOC_DESC = "ctrpDraftJB.protocolDocDesc";
	public static final String CTRPDOC_DUPLICATE = "ctrpDraftJB.DuplicateDocDesc";
	public static final String STUDY_STATUS = "studystat";
	public static final String PROTOCOL_STATUS = "protocolStatus";
	public static final String STUDY_STAUS_CHECKBOXlIST = "ctrpDraftJB.statusCountVal";
	public static final String STUDY_STATUSES = "ctrpDraftJB.statusCountVal1";
	public static final String  STUDY_STATUSES_USERS= "ctrpDraftJB.userPK";

	//Ak:End here  CTRP Trial documents declaration
	
	
	//22472 Search filters
	private String draftStatusesMenu = null;
	private CodeDao draftStatusesDao = null;
	public static final String DRAFT_STATUSES = "ctrpDraftStatuses";
	
	private CodeDao draftActionsDao = null;
	private String draftActionsMenu = null;
	public static final String DRAFT_ACTIONS = "ctrpDraftAction";
	
	private String nciProcStatMenu = null;
	private CodeDao nciDraftStatusDao = null;
	public static final String NCI_TRAIL_STATUS = "nciTrialStatus";
	
	public static final String CTRP_DESG_CODELST_SUBTYP = "CTRPDESIG";
	

	//22472 draft fields	
	private Integer draftNum=null;
	
	private Integer ctrpNciProcStatId = null;
	private CodeDao ctrpNciProcStatDao = null;
	private String ctrpNciProcStatMenu=null;
	public static final String NCI_PROC_STAT = "nciTrialStatus";
	public static final String FLD_CTRP_NCI_PROC_STATUS = "ctrpDraftJB.ctrpNciProcStatId";
	private static final String ON_CHANGE_CTRP_NCI_STAT = "id='"+FLD_CTRP_NCI_PROC_STATUS+"'";

	private String ctrpNciProcStatDt= null;
	public static final String FLD_NCI_TRIAL_STATUS_DATE = "ctrpDraftJB.ctrpNciProcStatDt";
	public static final String ER_CTRP_DRAFT_NCI_STAT = "er_ctrp_nci_stat"; //for NCI Stats in er_status_history table
	// To mark a status as System generated
	public static final String SYS_GEN_STAT = "SysGenerated";
	
	// The auto complete preloaded Data
	private Map<String, String> studyAutoComplete;
	
	public void setStudyAutoComplete(Map<String, String> studyAutoComplete){
		this.studyAutoComplete=studyAutoComplete;
	}
	
	public Map<String, String> getStudyAutoComplete(){
		return this.studyAutoComplete;
	}
	
	
	
	public CtrpDraftJB() {
		trialSubmDao = new CodeDao();
		trialSubmDao.getCodeValues(CTRP_SUBM_TYPE);
		//Parminder Singh: CTRP-22473
		currntStudyStatusDao = new CodeDao();
		currntStudyStatusDao.getCodeValues(STUDY_STATUS);
		setProtocolStatus(currntStudyStatusDao.toPullDown(PROTOCOL_STATUS));
		studyStatusChckLst= currntStudyStatusDao.toCheckboxList(STUDY_STAUS_CHECKBOXlIST);
		
		fillAorOorU(trialSubmDao);
		indusTrialTypeMenu = createDraftSubType(trialSubmDao,StringUtil.stringToNum(this.trialSubmTypeO));
		trialSubmTypeMenu = trialSubmDao.toPullDown(FLD_SUBM_TYPE, 
				StringUtil.stringToNum(this.trialSubmTypeO), ON_CHANGE_SUBM_TYPE);
		
		studyDraftsDao = new CodeDao();
		studyDraftsDao.getCodeValues(studyDraftCodeType);
		studyDraftsMenu = studyDraftsDao.toPullDownSubTypSel(studyDraftCodeType,CTRP_DESG_CODELST_SUBTYP);
		
		ctrpIntvnTypeDao=new CodeDao();
		ctrpIntvnTypeDao.getCodeValues(CTRP_INTERVENTION_TYPE);
		ctrpIntvnTypeMenu =ctrpIntvnTypeDao.toPullDown(FLD_CTRP_INTERVENTION_TYPE, 0);
		siteRecruitDao = new CodeDao();
		siteRecruitDao.getCodeValues(SITE_RECRUIT_TYPE);
		siteRecruitTypeMenu=siteRecruitDao.toPullDown(FLD_SITE_RECRUIT_STAT, 0);
		
		draftXmlReqList = new ArrayList<RadioOption>();
		draftXmlReqList.add(new RadioOption(LC.L_Yes, 1));
		draftXmlReqList.add(new RadioOption(LC.L_No, 0));
		isNCIDesignatedList = new ArrayList<RadioOption>();
		isNCIDesignatedList.add(new RadioOption(LC.L_Yes, 1));
		isNCIDesignatedList.add(new RadioOption(LC.L_No, 0));
		draftStudyPhaseDao = new CodeDao();
		draftStudyPhaseDao.getCodeValues(CTRP_STUDY_PHASE);
		fillStudyPhaseNA(draftStudyPhaseDao);
		draftStudyPhaseMenu = draftStudyPhaseDao.toPullDown(FLD_STUDY_PHASE,
				0, ON_CHANGE_STUDY_PHASE);
		draftPilotList = new ArrayList<RadioOption>();
		draftPilotList.add(new RadioOption(LC.L_No, 0));
		draftPilotList.add(new RadioOption(LC.L_Yes, 1));
		draftStudyType = 1; // Interventional by default
		draftStudyTypeList = new ArrayList<RadioOption>();
		draftStudyTypeList.add(new RadioOption(LC.CTRP_DraftInterventional, 1));
		draftStudyTypeList.add(new RadioOption(LC.CTRP_DraftObservational, 0));
		studyPurposeDao = new CodeDao();
		studyPurposeDao.getCodeValues(CTRP_STUDY_PURPOSE);
		fillStudyPurposeOther(studyPurposeDao);
		studyPurposeMenu = studyPurposeDao.toPullDown(FLD_STUDY_PURPOSE,
				0, ON_CHANGE_STUDY_PURPOSE);
		draftStatusDao = new CodeDao();
		draftStatusDao.getCodeValues(CTRP_DRAFT_STATUS);
		draftStatusMenu = filterDraftStatusesForDraftDetails(draftStatusDao, 0);

		
		// Start Regulatory Info section: AGodara
		oversightCountryDao = new CodeDao();
		oversightCountryDao.getCodeValues(OVERSIGHT_COUNTRIES);
		oversightCountryMenu = oversightCountryDao.toPullDown(FLD_OVERSIGHTCOUNTRY, 0, ON_CHANGE_OVERSIGHT_COUNTRY);
		
		oversightOrgDao = new CodeDao();
		oversightOrgDao.getCodeValues(OVERSIGHT_ORGANIZATION);
		oversightOrgMenu = oversightOrgDao.toPullDown(FLD_OVERSIGHTORGANIZATION, 0, ON_CHANGE_OVERSIGHT_ORGANIZATION);
		
		
		fdaIntvenIndicatorList = new ArrayList<RadioOption>();
		fdaIntvenIndicatorList.add(new RadioOption(LC.L_No, 0));
		fdaIntvenIndicatorList.add(new RadioOption(LC.L_Yes, 1));
		
		section801IndicatorList = new ArrayList<RadioOption>();
		section801IndicatorList.add(new RadioOption(LC.L_No, 0));
		section801IndicatorList.add(new RadioOption(LC.L_Yes, 1));
		
		delayPostIndicatorList = new ArrayList<RadioOption>();
		delayPostIndicatorList.add(new RadioOption(LC.L_No, 0));
		delayPostIndicatorList.add(new RadioOption(LC.L_Yes, 1));
		
		dmAppointIndicatorList = new ArrayList<RadioOption>();
		dmAppointIndicatorList.add(new RadioOption(LC.L_No, 0));
		dmAppointIndicatorList.add(new RadioOption(LC.L_Yes, 1));
		
		
		//End Regulatory Info section: AGodara
		
		// Added for CTRP-20579, Ankit (Start)
		trialStatusDao = new CodeDao();
		trialStatusDao.getCodeValues(CTRP_STUDY_STATUS);
		fillRequiredTrialStatus(trialStatusDao);
		draftTrialStatusMenu = trialStatusDao.toPullDown(FLD_TRIAL_STATUS,
				this.trialStatus, ON_CHANGE_TRIAL_STATUS);
		
		trialStartTypeList = new ArrayList<RadioOption>();
		trialStartTypeList.add(new RadioOption(LC.L_Actual, 1));
		trialStartTypeList.add(new RadioOption(LC.L_Anticipated, 0));
		
		primaryCompletionTypeList = new ArrayList<RadioOption>();
		primaryCompletionTypeList.add(new RadioOption(LC.L_Actual, 1));
		primaryCompletionTypeList.add(new RadioOption(LC.L_Anticipated, 0));
	
		//Added for CTRP-20579, Ankit  (End)
		
		leadOrgTypeDao = new CodeDao();
		leadOrgTypeDao.getCodeValues(CTRP_ORG_TYPE);
		leadOrgTypeMenu = leadOrgTypeDao.toPullDown(FLD_LEAD_ORG_TYPE);
		
		subOrgTypeDao = new CodeDao();
		subOrgTypeDao.getCodeValues(CTRP_ORG_TYPE);
		subOrgTypeMenu = subOrgTypeDao.toPullDown(FLD_SUB_ORG_TYPE);
	
		//22472 Search filters
		draftStatusesDao = new CodeDao();
		draftStatusesDao.getCodeValues(CTRP_DRAFT_STATUS);
		setDraftStatusesMenu(draftStatusesDao.toPullDown(DRAFT_STATUSES));
		
		draftActionsDao = new CodeDao();
		draftActionsDao.getCodeValues(DRAFT_ACTIONS);
		setDraftActionsMenu(draftActionsDao.toPullDown(DRAFT_ACTIONS));
		
		nciDraftStatusDao = new CodeDao();
		nciDraftStatusDao.getCodeValues(NCI_TRAIL_STATUS);
		setNciProcStatMenu(nciDraftStatusDao.toPullDown(NCI_TRAIL_STATUS));
		
		//22472 draft fields
		ctrpNciProcStatDao = new CodeDao();
		ctrpNciProcStatDao.getCodeValues(NCI_PROC_STAT);
		setCtrpNciProcStatMenu(ctrpNciProcStatDao.toPullDown("NCI_PROC_STAT",0,true));
		ctrpNciProcStatMenu = filterNCITrailStatusesForDraftDetails(nciDraftStatusDao,0);
		
	}
	
	private String filterDraftStatusesForDraftDetails(CodeDao codeDao, Integer selectedOptionValue) {
		ArrayList pkList = codeDao.getCId();
		ArrayList descList = codeDao.getCDesc();
		ArrayList subList = codeDao.getCSubType();
		StringBuffer sb1 =  new StringBuffer();
		sb1.append("<select name='").append(FLD_DRAFT_STATUS).append("' ").append(ON_CHANGE_DRAFT_STATUS);
		sb1.append(">");
		try {
			for (int iX=0; iX<pkList.size(); iX++) {
			//	if (STRING_SUBMITTED.equals(subList.get(iX))) { continue; } // Skip "submitted" status
			//	if (STRING_READY.equals(subList.get(iX))) { continue; } // Skip "readySubmission" status
				sb1.append("<option value='").append(pkList.get(iX)).append("' ");
				if (selectedOptionValue!=null && selectedOptionValue.intValue() == ((Integer)pkList.get(iX)).intValue()) { sb1.append("SELECTED"); }
				sb1.append(">").append(descList.get(iX)).append("</option>");
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		sb1.append("</select>");
		return sb1.toString();
	}
	// Added for CTRP-20579, Ankit (Start)
	private void fillRequiredTrialStatus(CodeDao trialStatusDao) {
		ArrayList trialStatusSub = trialStatusDao.getCSubType();
		ArrayList trialStatusPk = trialStatusDao.getCId();
		for (int iX=0; iX<trialStatusSub.size(); iX++) {
			if (ADMIN_COMP.equals(trialStatusSub.get(iX))) {
				this.setStatusTypeAdminComplete(String.valueOf(trialStatusPk.get(iX)));
			} else if (WITHDRAWN.equals(trialStatusSub.get(iX))) {
				this.setStatusTypeWithdrawn(String.valueOf(trialStatusPk.get(iX)));
			} else if (TEMPCLOSEDACC.equals(trialStatusSub.get(iX))) {
				this.setStatusTypeTempClosedAcc(String.valueOf(trialStatusPk.get(iX)));
			}
		}
	}
	//Added for CTRP-20579, Ankit  (End)
	
	private void fillAorOorU(CodeDao trialSubmDao) {
		ArrayList trialSubmDaoSub = trialSubmDao.getCSubType();
		ArrayList trialSubmDaoPk = trialSubmDao.getCId();
		StringBuffer sb1 = new StringBuffer("[");
		StringBuffer sb2 = new StringBuffer("[");
		for (int iX=0; iX<trialSubmDaoSub.size(); iX++) {
			if (LETTER_A.equals(trialSubmDaoSub.get(iX))) {
				sb1.append(trialSubmDaoPk.get(iX)).append(",");
				sb2.append(trialSubmDaoPk.get(iX)).append(",");
				this.setTrialSubmTypeA(String.valueOf(trialSubmDaoPk.get(iX)));
			} else if (LETTER_O.equals(trialSubmDaoSub.get(iX))) {
				sb2.append(trialSubmDaoPk.get(iX)).append(",");
				this.setTrialSubmTypeO(String.valueOf(trialSubmDaoPk.get(iX)));
			} else if (LETTER_U.equals(trialSubmDaoSub.get(iX))) {
				sb1.append(trialSubmDaoPk.get(iX)).append(",");
				this.setTrialSubmTypeU(String.valueOf(trialSubmDaoPk.get(iX)));
			}
		}
		if (sb1.length() > 1) { sb1.deleteCharAt(sb1.length()-1); }
		sb1.append("]");
		setTrialSubmTypeAorU(sb1.toString());
		if (sb2.length() > 1) { sb2.deleteCharAt(sb2.length()-1); }
		sb2.append("]");
		setTrialSubmTypeAorO(sb2.toString());
	}
	
	private void fillStudyPhaseNA(CodeDao draftStudyPhaseDao) {
		ArrayList draftStudyPhaseDaoSub = draftStudyPhaseDao.getCSubType();
		ArrayList draftStudyPhaseDaoPk = draftStudyPhaseDao.getCId();
		for (int iX=0; iX<draftStudyPhaseDaoSub.size(); iX++) {
			if (LETTER_NA.equals(draftStudyPhaseDaoSub.get(iX))) {
				this.setDraftStudyPhaseNA(String.valueOf(draftStudyPhaseDaoPk.get(iX)));
				return;
			}
		}
	}
	
	private void fillStudyPurposeOther(CodeDao studyPurposeDao) {
		ArrayList studyPurposeDaoSub = studyPurposeDao.getCSubType();
		ArrayList studyPurposeDaoPk = studyPurposeDao.getCId();
		for (int iX=0; iX<studyPurposeDaoSub.size(); iX++) {
			if (STRING_OTHER.equals(studyPurposeDaoSub.get(iX))) {
				this.setStudyPurposeOtherPk(String.valueOf(studyPurposeDaoPk.get(iX)));
				return;
			}
		}
	}
	
	private CtrpDraftAgent getCtrpDraftAgent() throws NamingException {
		InitialContext ic = new InitialContext();
		return (CtrpDraftAgent)ic.lookup(JNDINames.CtrpDraftAgentHome);
	}

	private AddressAgentRObj getAddressAgent() throws NamingException {
		InitialContext ic = new InitialContext();
		return (AddressAgentRObj)ic.lookup(JNDINames.AddressAgentHome);
	}

	public void retrieveStudy(int studyId) {
		CtrpDraftJBHelper helper = new CtrpDraftJBHelper(this);
		helper.retrieveStudy(studyId);
	}
	
	public void retrieveCtrpDraft(int draftId) {
		this.setId(draftId);
		CtrpDraftJBHelper helper = new CtrpDraftJBHelper(this);
		helper.retrieveCtrpDraft(draftId);
	}
	
	public Integer updateCtrpDraftNonIndustrial(HashMap<String, String> argMap) {
		Integer output = 0;
		CtrpDraftBean ctrpDraftBean = new CtrpDraftBean();
		fillCtrpDraftBeanNonIndustrial(ctrpDraftBean);
		if (argMap.get(IP_ADD) != null) {
			ctrpDraftBean.setIpAdd(argMap.get(IP_ADD));
		}
		try {
			CtrpDraftAgent ctrpDraftAgent = this.getCtrpDraftAgent();
			if (id > 0) {
				fillCtrpDraftBeanLastModifiedBy(ctrpDraftBean, argMap);
				output = ctrpDraftAgent.updateCtrpDraft(ctrpDraftBean);
				updateStatusHistoryDetails(argMap,CTRP_DRAFT_STATUS);
				updateStatusHistoryDetails(argMap,ER_CTRP_DRAFT_NCI_STAT);
			} else {
				fetchNextDraftNum(ctrpDraftBean);
				fillCtrpDraftBeanCreator(ctrpDraftBean, argMap);
				output = ctrpDraftAgent.createCtrpDraft(ctrpDraftBean);
				id = output; // newly created ID
				createStatusHistoryDetails(argMap,CTRP_DRAFT_STATUS);
				createStatusHistoryDetails(argMap,ER_CTRP_DRAFT_NCI_STAT);
			}
			updateDraftAddressDetails();
		} catch(Exception e) {
			Rlog.fatal("ctrp", "Exception in CtrpDraftJB.updateCtrpDraftNonIndustrial: "+e);
			output = -1;
		}
		return output;
	}
	
	/**
	 * Added by Parminder Singh for CTRP-22473
	 * This method is used to persist or update data in table
	 * 
	 */
	public Integer updateCtrpDraftNontification(HashMap<String, String> argMap) {
		Integer output = 0;
		Integer count = 0;
		CtrpDraftNotificationBean ctrpDraftNotificationBean=new CtrpDraftNotificationBean();
		String statusCountVal[] = StringUtil.strSplit(getStatusCountVal(),",",false);
		String userPk[]=StringUtil.strSplit(getEmailUserId(),",",false);
		for(int i=0;i<statusCountVal.length;i++)
		{
			for(int j=0;j<userPk.length;j++)
			{   
				fillCtrpDraftNotificationBean(ctrpDraftNotificationBean,statusCountVal[i],userPk[j]);
				if (argMap.get(IP_ADD) != null) {
						ctrpDraftNotificationBean.setIpAdd(argMap.get(IP_ADD));
					}
				fillCtrpDraftNotificationBeanCreator(ctrpDraftNotificationBean, argMap);
					try {
						CtrpDraftAgent ctrpDraftAgent = this.getCtrpDraftAgent();
						output = ctrpDraftAgent.createCtrpDraftNotification(ctrpDraftNotificationBean);
						if(output>0)
							count++;
						} 
					catch(Exception e) 
							{
							Rlog.fatal("ctrp", "Exception in CtrpDraftJB.updateCtrpDraftNontification: "+e);
							output = -1;
							}
			}
		}
		return count;
	}
	/**
	 * Added by Parminder Singh for CTRP-22473
	 * This method is used to delete data from table
	 * 
	 */
	public Integer deleteCtrpNontificationUsers() {
		Integer output = 0;
		String statusCountVal[] = StringUtil.strSplit(getStatusCountVal(),",",false);
		String userPk[]=StringUtil.strSplit(getEmailUserId(),";",false);
		for(int i=0;i<statusCountVal.length;i++)
		{
					try {
						CtrpDraftAgent ctrpDraftAgent = this.getCtrpDraftAgent();
						output = ctrpDraftAgent.deleteDraftNotiUser(StringUtil.stringToInteger(statusCountVal[i]),userPk[i]);
						} 
					catch(Exception e) 
							{
							Rlog.fatal("ctrp", "Exception in CtrpDraftJB.deleteCtrpNontificationUsers: "+e);
							output = -1;
							}
		}
		return output;
	}
	
	/**
	 * Added by Parminder Singh for CTRP-22473
	 * This method is used to retrieve data from table
	 * 
	 */
	 public void  getStatusUserList(){
	  	userStatusDao=new CtrpDraftDao();
		userStatusDao.getUserStatusValues();
		studyStatusUserLst= userStatusDao.toStatusUserList(STUDY_STATUSES,STUDY_STATUSES_USERS,this.loggedUser);
		}
	
	
	private void fillCtrpDraftBeanNonIndustrial(CtrpDraftBean ctrpDraftBean) {
		if (id > 0) { ctrpDraftBean.setId(this.id); }
		ctrpDraftBean.setSponsorCtrpId(this.ctrpDraftSectSponsorRespJB.getSponsorId());
		ctrpDraftBean.setFkSiteSponsor(this.ctrpDraftSectSponsorRespJB.getSponsorFkSite());
		ctrpDraftBean.setFkAddSponsor(this.ctrpDraftSectSponsorRespJB.getSponsorFkAdd());
		ctrpDraftBean.setResponsibleParty(this.ctrpDraftSectSponsorRespJB.getResponsibleParty());
		ctrpDraftBean.setRpEmail(this.ctrpDraftSectSponsorRespJB.getResponsiblePartyEmail());
		ctrpDraftBean.setRpPhone(this.ctrpDraftSectSponsorRespJB.getResponsiblePartyPhone());
		ctrpDraftBean.setRpExtn(StringUtil.stringToInteger(
				this.ctrpDraftSectSponsorRespJB.getResponsiblePartyPhoneExt()));
		if (this.ctrpDraftSectSponsorRespJB.getResponsibleParty() != null){
			if (this.ctrpDraftSectSponsorRespJB.getResponsibleParty() == 1) { // responsible party = PI
				ctrpDraftBean.setRpSponsorContactType(null);
			} else { // responsible party = Sponsor
				ctrpDraftBean.setRpSponsorContactType(this.ctrpDraftSectSponsorRespJB.getSponsorContactType());
			}
		}
		if (this.ctrpDraftSectSponsorRespJB.getSponsorContactType() != null){
			if (this.ctrpDraftSectSponsorRespJB.getSponsorContactType() == 1) { // sponsor contact type = personal
				ctrpDraftBean.setRpSponsorContactId(this.ctrpDraftSectSponsorRespJB.getPersonalContactId());
				ctrpDraftBean.setFkUserSponsorPersonal(
						this.ctrpDraftSectSponsorRespJB.getPersonalContactFkUser());
				ctrpDraftBean.setFkAddSponsorPersonal(
						this.ctrpDraftSectSponsorRespJB.getPersonalContactFkAdd());
			} else { // sponsor contact type = generic
				ctrpDraftBean.setRpSponsorContactId(this.ctrpDraftSectSponsorRespJB.getGenericContactId());
				ctrpDraftBean.setSponsorGenericTitle(this.ctrpDraftSectSponsorRespJB.getGenericContactTitle());
			}
		}
		ctrpDraftBean.setFkStudy(StringUtil.stringToInteger(this.fkStudy));
		ctrpDraftBean.setCtrpDraftType(0); // 0 = non-industrial
		ctrpDraftBean.setFkCodelstSubmissionType(this.trialSubmTypeInt);
		ctrpDraftBean.setDraftXMLReqd(this.draftXmlRequired);
		if (this.trialSubmTypeInt == Integer.parseInt(this.trialSubmTypeA)) {
			ctrpDraftBean.setAmendNumber(this.amendNumber);
			ctrpDraftBean.setAmendDate(DateUtil.stringToDate(this.amendDate));
		} else {
			ctrpDraftBean.setAmendNumber(null);
			ctrpDraftBean.setAmendDate(null);
		}
		ctrpDraftBean.setNciStudyId(this.nciTrialId);
		ctrpDraftBean.setLeadOrgStudyId(this.leadOrgTrialId);
		ctrpDraftBean.setNctNumber(this.nctNumber);
		ctrpDraftBean.setOtherNumber(this.otherTrialId);
		ctrpDraftBean.setFkCodelstPhase(this.draftStudyPhase);
		if (this.draftStudyPhase == Integer.parseInt((this.draftStudyPhaseNA))) {
			ctrpDraftBean.setDraftPilot(this.draftPilot);
		}
		ctrpDraftBean.setStudyType(this.draftStudyType);
		ctrpDraftBean.setStudyPurpose(this.studyPurpose);
		if (this.studyPurpose == Integer.parseInt(studyPurposeOtherPk)) {
			ctrpDraftBean.setStudyPurposeOther(this.studyPurposeOther);
		}
		
		ctrpDraftBean.setLeadOrgCtrpId(this.leadOrgCtrpId);
		ctrpDraftBean.setFkSiteLeadOrg(StringUtil.stringToInteger(this.leadOrgFkSite));
		ctrpDraftBean.setLeadOrgType(this.leadOrgTypeInt);
		ctrpDraftBean.setFkAddLeadOrg(StringUtil.stringToInteger(this.leadOrgFkAdd));
		ctrpDraftBean.setPiCtrpId(this.piId);
		ctrpDraftBean.setFkUserPI(StringUtil.stringToInteger(this.piFkUser));
		ctrpDraftBean.setFkAddPi(StringUtil.stringToInteger(this.piFkAdd));
		
		//START Summury4 Sponsor Info section: SM
		ctrpDraftBean.setSumm4SponsorId(this.summ4CtrpId);
		ctrpDraftBean.setFkSiteSumm4Sponsor(StringUtil.stringToInteger(this.summ4FkSite));
		ctrpDraftBean.setSumm4SponsorType(this.summ4TypeInt);
		ctrpDraftBean.setFkAddSumm4Sponsor(StringUtil.stringToInteger(this.summ4FkAdd));
		
		//START Regulatory Info section: AGodara
		ctrpDraftBean.setOversightCountry(this.oversightCountry);
		ctrpDraftBean.setOversightOrganization(this.oversightOrganization);
		ctrpDraftBean.setFdaIntvenIndicator(this.fdaIntvenIndicator);
		ctrpDraftBean.setSection801Indicator(this.section801Indicator);
		ctrpDraftBean.setDelayPostIndicator(this.delayPostIndicator);
		ctrpDraftBean.setDmAppointIndicator(this.dmAppointIndicator);
		
		//END Regulatory Info section: AGodara	
		
		// Added for CTRP-20579, Ankit (Start)
		ctrpDraftBean.setFkCodelstCtrpStudyStatus(this.trialStatus);
		ctrpDraftBean.setCtrpStudyStatusDate(DateUtil.stringToDate(this.trialStatusDate));
		ctrpDraftBean.setCtrpStudyStopReason(this.studyStopReason);
		ctrpDraftBean.setCtrpStudyStartDate(DateUtil.stringToDate(this.trialStartDate));
		ctrpDraftBean.setIsStartActual(this.trialStartType);
		ctrpDraftBean.setCtrpStudyCompDate(DateUtil.stringToDate(this.primaryCompletionDate));
		ctrpDraftBean.setIsCompActual(this.primaryCompletionType);
		//Added for CTRP-20579, Ankit  (End)
		ctrpDraftBean.setDraftNum(this.draftNum);
	}
	
	/**
	 * Added by Parminder Singh for CTRP-22473
	 * This method is used to set data in Bean
	 * 
	 */
	private void fillCtrpDraftNotificationBean(CtrpDraftNotificationBean ctrpDraftNotificationBean,String statusCountVal,String userPk) {
		UserJB user=new UserJB();
		user.setUserId(StringUtil.stringToInteger(userPk));
		user.getUserDetails();
		ctrpDraftNotificationBean.setFkAccount(StringUtil.stringToInteger(user.getUserAccountId()));
		ctrpDraftNotificationBean.setFkSiteId(StringUtil.stringToInteger(user.getUserSiteId()));
		ctrpDraftNotificationBean.setFkCodelstStudyStatus(StringUtil.stringToInteger(statusCountVal));
		ctrpDraftNotificationBean.setFkUser(StringUtil.stringToInteger(userPk));
		
	}
	/**
	 * Added by Parminder Singh for CTRP-22473
	 * This method is used to set data in Bean
	 * 
	 */
	private void fillCtrpDraftNotificationBeanCreator(CtrpDraftNotificationBean ctrpDraftNotificationBean, HashMap<String, String> argMap) {
		if (argMap.get(USER_ID) == null) { return; }
		ctrpDraftNotificationBean.setCreator(StringUtil.stringToInteger(argMap.get(USER_ID)));
		ctrpDraftNotificationBean.setLastModifiedBy(StringUtil.stringToInteger(argMap.get(USER_ID)));
	}	
	
	private void fillCtrpDraftBeanCreator(CtrpDraftBean ctrpDraftBean, HashMap<String, String> argMap) {
		if (argMap.get(USER_ID) == null) { return; }
		ctrpDraftBean.setCreator(StringUtil.stringToInteger(argMap.get(USER_ID)));
	}
	
	private void fillCtrpDraftBeanLastModifiedBy(CtrpDraftBean ctrpDraftBean, HashMap<String, String> argMap) {
		if (argMap.get(USER_ID) == null) { return; }
		ctrpDraftBean.setLastModifiedBy(StringUtil.stringToInteger(argMap.get(USER_ID)));
	}
	
	private void createStatusHistoryDetails(HashMap<String, String> argMap,String status) {
		Date dt1 = new java.util.Date();
		String stDate = DateUtil.dateToString(dt1);
		if(status!=null && status.trim().equals(CTRP_DRAFT_STATUS)){
			
			StatusHistoryJB statusHistoryJB = new StatusHistoryJB();
			statusHistoryJB.setStatusModuleId(String.valueOf(this.id));
			statusHistoryJB.setStatusCodelstId(argMap.get(STATUS_CODE_ID));
			statusHistoryJB.setStatusStartDate(stDate);

			statusHistoryJB.setCreator(argMap.get(USER_ID));
			statusHistoryJB.setIpAdd(argMap.get(IP_ADD));
			statusHistoryJB.setRecordType(LETTER_N);
			// First time the status should be WIP and should be marked as System generated
			statusHistoryJB.setStatusNotes(CtrpDraftJB.SYS_GEN_STAT);
			statusHistoryJB.setIsCurrentStat(String.valueOf(1));
			statusHistoryJB.setStatusModuleTable(ER_CTRP_DRAFT);
			statusHistoryJB.setStatusHistoryDetailsWithEndDate();
		}else if((status!=null && status.trim().equals(ER_CTRP_DRAFT_NCI_STAT)) && !argMap.get(FLD_CTRP_NCI_PROC_STATUS).isEmpty()){
			StatusHistoryJB statusHistoryJB = new StatusHistoryJB();
			statusHistoryJB.setStatusModuleId(String.valueOf(this.id));
			statusHistoryJB.setStatusModuleTable(ER_CTRP_DRAFT_NCI_STAT);
			statusHistoryJB.setStatusCodelstId(argMap.get(FLD_CTRP_NCI_PROC_STATUS));
			if(argMap.get(FLD_NCI_TRIAL_STATUS_DATE)==null || argMap.get(FLD_NCI_TRIAL_STATUS_DATE)==""){
				statusHistoryJB.setStatusStartDate(stDate);
			}else{
				statusHistoryJB.setStatusStartDate(argMap.get(FLD_NCI_TRIAL_STATUS_DATE));
			}
			
			statusHistoryJB.setCreator(argMap.get(USER_ID));
			statusHistoryJB.setIpAdd(argMap.get(IP_ADD));
			statusHistoryJB.setRecordType(LETTER_N);
			statusHistoryJB.setIsCurrentStat(String.valueOf(1));
			statusHistoryJB.setStatusHistoryDetailsWithEndDate();
		}
	}
	
	public void updateStatusHistoryDetails(HashMap<String, String> argMap, String status) {
		Date dt1 = new java.util.Date();
		String stDate = DateUtil.dateToString(dt1);
		
		if(status!=null && status.trim().equals(CTRP_DRAFT_STATUS)){
			
			StatusHistoryJB statusHistoryJB = new StatusHistoryJB();
			int statusId = statusHistoryJB.getLatestStatusId(this.id, ER_CTRP_DRAFT);
			statusHistoryJB.setStatusId(statusId);
			statusHistoryJB.getStatusHistoryDetails();
			int statusCodeId = StringUtil.stringToNum(statusHistoryJB.getStatusCodelstId());
			int reqStatusCodeId = StringUtil.stringToNum(argMap.get(STATUS_CODE_ID));
			statusHistoryJB.setStatusModuleId(String.valueOf(this.id));
			statusHistoryJB.setStatusModuleTable(ER_CTRP_DRAFT);
			statusHistoryJB.setStatusCodelstId(argMap.get(STATUS_CODE_ID));
			statusHistoryJB.setIpAdd(argMap.get(IP_ADD));
			if(argMap.containsKey("NOTES")){
				statusHistoryJB.setStatusNotes(argMap.get("NOTES"));
			}else{
				statusHistoryJB.setStatusNotes(null);
			}
			if (statusCodeId == reqStatusCodeId) {
				statusHistoryJB.setRecordType(LETTER_M);
				statusHistoryJB.setModifiedBy(argMap.get(USER_ID));
				statusHistoryJB.setIsCurrentStat(String.valueOf(1));
				statusHistoryJB.updateStatusHistory();
			} else {
				statusHistoryJB.setRecordType(LETTER_N);
				statusHistoryJB.setCreator(argMap.get(USER_ID));
				statusHistoryJB.setStatusStartDate(stDate);
				statusHistoryJB.setIsCurrentStat(String.valueOf(1));
				statusHistoryJB.setStatusHistoryDetails();
			}
		}else if((status!=null && status.trim().equals(ER_CTRP_DRAFT_NCI_STAT)) && !argMap.get(FLD_CTRP_NCI_PROC_STATUS).isEmpty()){
			StatusHistoryJB statusHistoryJB = new StatusHistoryJB();
			int nciStatusId = statusHistoryJB.getLatestStatusId(this.id, ER_CTRP_DRAFT_NCI_STAT);
			
			statusHistoryJB.setStatusId(nciStatusId);
			statusHistoryJB.getStatusHistoryDetails();
			
			int statusCodeId = StringUtil.stringToNum(statusHistoryJB.getStatusCodelstId());
			int reqStatusCodeId = StringUtil.stringToNum(argMap.get(FLD_CTRP_NCI_PROC_STATUS));
			statusHistoryJB.setStatusModuleId(String.valueOf(this.id));
			
			statusHistoryJB.setStatusModuleTable(ER_CTRP_DRAFT_NCI_STAT);
			statusHistoryJB.setStatusCodelstId(argMap.get(FLD_CTRP_NCI_PROC_STATUS));
			if(argMap.get(FLD_NCI_TRIAL_STATUS_DATE)==null || argMap.get(FLD_NCI_TRIAL_STATUS_DATE)==""){
				statusHistoryJB.setStatusStartDate(stDate);
			}else{
				statusHistoryJB.setStatusStartDate(argMap.get(FLD_NCI_TRIAL_STATUS_DATE));
			}
			statusHistoryJB.setIpAdd(argMap.get(IP_ADD));
			if (statusCodeId == reqStatusCodeId) {
				statusHistoryJB.setRecordType(LETTER_M);
				statusHistoryJB.setModifiedBy(argMap.get(USER_ID));
				statusHistoryJB.setIsCurrentStat(String.valueOf(1));
				statusHistoryJB.updateStatusHistory();
			} else {
				statusHistoryJB.setRecordType(LETTER_N);
				statusHistoryJB.setCreator(argMap.get(USER_ID));
				//statusHistoryJB.setStatusStartDate(stDate);
				statusHistoryJB.setIsCurrentStat(String.valueOf(1));
				statusHistoryJB.setStatusHistoryDetails();
			}			
		}
	}
	
	
	
	public void studyUpdateCtrpLastDwn(HashMap<String, String> argMap,String studyId){
		int i=0;
		StudyJB studyJB=new StudyJB();
		studyJB.setId(StringUtil.stringToNum(studyId));
		studyJB.getStudyDetails();
		studyJB.setLastDownloadBy(argMap.get(USER_ID));
		studyJB.setModifiedBy(argMap.get(USER_ID));
		studyJB.updateStudy();
	}
	
	private void updateDraftAddressDetails() {
		if (!StringUtil.isEmpty(this.leadOrgFkSite)) {
			try {
				AddressAgentRObj addressAgent = this.getAddressAgent();
				AddressBean addressBean = addressAgent.getAddressDetails(StringUtil.stringToNum(this.leadOrgFkAdd));
				addressBean.setAddTty(this.leadOrgTty);
				addressBean.setAddFax(this.leadOrgFax);
				addressBean.setAddUrl(this.leadOrgUrl);
				addressAgent.updateAddress(addressBean);
			} catch(Exception e) {
				Rlog.fatal("ctrp", "Exception in CtrpDraftJB.updateDraftAddressDetails - leadOrg: "+e);
			}
		}
		if (!StringUtil.isEmpty(this.piFkUser)) {
			try {
				AddressAgentRObj addressAgent = this.getAddressAgent();
				AddressBean addressBean = addressAgent.getAddressDetails(StringUtil.stringToNum(this.piFkAdd));
				addressBean.setAddTty(this.piTty);
				addressBean.setAddFax(this.piFax);
				addressBean.setAddUrl(this.piUrl);
				addressAgent.updateAddress(addressBean);
			} catch(Exception e) {
				Rlog.fatal("ctrp", "Exception in CtrpDraftJB.updateDraftAddressDetails - pi: "+e);
			}
		}
		if (!StringUtil.isEmpty(this.subOrgFkAdd)) {
			try {
				AddressAgentRObj addressAgent = this.getAddressAgent();
				AddressBean addressBean = addressAgent.getAddressDetails(StringUtil.stringToNum(this.subOrgFkAdd));
				addressBean.setAddTty(this.subOrgTty);
				addressBean.setAddFax(this.subOrgFax);
				addressBean.setAddUrl(this.subOrgUrl);
				addressAgent.updateAddress(addressBean);
			} catch(Exception e) {
				Rlog.fatal("ctrp", "Exception in CtrpDraftJB.updateDraftAddressDetails - suborg: "+e);
			}
		}
		if (this.ctrpDraftSectSponsorRespJB != null &&
				this.ctrpDraftSectSponsorRespJB.getSponsorFkAdd() != null) {
			try {
				AddressAgentRObj addressAgent = this.getAddressAgent();
				AddressBean addressBean = addressAgent.getAddressDetails(
						this.ctrpDraftSectSponsorRespJB.getSponsorFkAdd());
				addressBean.setAddTty(this.ctrpDraftSectSponsorRespJB.getSponsorTty());
				addressBean.setAddFax(this.ctrpDraftSectSponsorRespJB.getSponsorFax());
				addressBean.setAddUrl(this.ctrpDraftSectSponsorRespJB.getSponsorUrl());
				addressAgent.updateAddress(addressBean);
			} catch (Exception e) {
				Rlog.fatal("ctrp", "Exception in CtrpDraftJB.updateDraftAddressDetails - sponsor: "+e);
			}
		}
		if (this.ctrpDraftSectSponsorRespJB != null &&
				this.ctrpDraftSectSponsorRespJB.getPersonalContactFkAdd() != null) {
			try {
				AddressAgentRObj addressAgent = this.getAddressAgent();
				AddressBean addressBean = addressAgent.getAddressDetails(
						this.ctrpDraftSectSponsorRespJB.getPersonalContactFkAdd());
				addressBean.setAddTty(this.ctrpDraftSectSponsorRespJB.getPersonalContactTty());
				addressBean.setAddFax(this.ctrpDraftSectSponsorRespJB.getPersonalContactFax());
				addressBean.setAddUrl(this.ctrpDraftSectSponsorRespJB.getPersonalContactUrl());
				addressAgent.updateAddress(addressBean);
			} catch (Exception e) {
				Rlog.fatal("ctrp", "Exception in CtrpDraftJB.updateDraftAddressDetails - sponsor: "+e);
			}
		}
		if (!StringUtil.isEmpty(this.summ4FkAdd)) {
			try {
				AddressAgentRObj addressAgent = this.getAddressAgent();
				AddressBean addressBean = addressAgent.getAddressDetails(StringUtil.stringToNum(this.summ4FkAdd));
				addressBean.setAddTty(this.summ4Tty);
				addressBean.setAddFax(this.summ4Fax);
				addressBean.setAddUrl(this.summ4Url);
				addressAgent.updateAddress(addressBean);
			} catch(Exception e) {
				Rlog.fatal("ctrp", "Exception in CtrpDraftJB.updateDraftAddressDetails - Summ4: "+e);
			}
		}
	}
	
	private String getCodeSubtypeFromCodeId(int codeId) {
		if (draftStatusDao == null) { return EMPTY_STRING; }
		ArrayList pkList = draftStatusDao.getCId();
		if (pkList.size() < 1) { return EMPTY_STRING; }
		ArrayList subList = draftStatusDao.getCSubType();
		for (int iX=0; iX<pkList.size(); iX++) {
			if (codeId == (Integer)pkList.get(iX)) {
				if (subList.size() <= iX) { return EMPTY_STRING; }
				return (String)subList.get(iX);
			}
		}
		return EMPTY_STRING;
	}
	
	// Getters and setters
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getESign() {
		return eSign;
	}

	public void setESign(String eSign) {
		this.eSign = eSign;
	}

	public String getFkStudy() {
		return fkStudy;
	}

	public void setFkStudy(String fkStudy) {
		this.fkStudy = fkStudy;
	}

	public String getStudyNumber() {
		return studyNumber;
	}

	public void setStudyNumber(String studyNumber) {
		this.studyNumber = studyNumber;
	}

	public String getStudyTitle() {
		return studyTitle;
	}

	public void setStudyTitle(String studyTitle) {
		this.studyTitle = studyTitle;
	}

	public Integer getHideeSign() {
		return hideeSign;
	}

	public void setHideeSign(Integer hideeSign) {
		this.hideeSign = hideeSign;
	}
	
	public Integer getDraftType() {
		return draftType;
	}

	public void setDraftType(Integer draftType) {
		this.draftType = draftType;
	}
	
	public String getTrialSubmTypeMenu() {
		return trialSubmTypeMenu;
	}
	
	public Integer getTrialSubmTypeInt() {
		return trialSubmTypeInt;
	}

	public void setTrialSubmTypeInt(Integer trialSubmTypeInt) {
		this.trialSubmTypeInt = trialSubmTypeInt;
		trialSubmTypeMenu = trialSubmDao.toPullDown(FLD_SUBM_TYPE, 
				trialSubmTypeInt, ON_CHANGE_SUBM_TYPE);
	}
	public String getIndusTrialTypeMenu() {
		return indusTrialTypeMenu;
	}
	
	public Integer getIndusTrialTypeInt() {
		return indusTrialTypeInt;
	}
	
	public void setIndusTrialTypeInt(Integer indusTrialTypeInt) {
		this.indusTrialTypeInt = indusTrialTypeInt;
		indusTrialTypeMenu = createDraftSubType(trialSubmDao,indusTrialTypeInt);
	}

	public String getTrialSubmTypeA() {
		if (trialSubmTypeA == null) { return EMPTY_STRING; }
		return trialSubmTypeA;
	}

	public void setTrialSubmTypeA(String trialSubmTypeA) {
		this.trialSubmTypeA = trialSubmTypeA;
	}

	public String getTrialSubmTypeO() {
		if (trialSubmTypeO == null) { return EMPTY_STRING; }
		return trialSubmTypeO;
	}

	public void setTrialSubmTypeO(String trialSubmTypeO) {
		this.trialSubmTypeO = trialSubmTypeO;
	}

	public void setTrialSubmTypeU(String trialSubmTypeU) {
		this.trialSubmTypeU = trialSubmTypeU;
	}

	public String getTrialSubmTypeU() {
		if (trialSubmTypeU == null) { return EMPTY_STRING; }
		return trialSubmTypeU;
	}

	public String getTrialSubmTypeAorO() {
		return trialSubmTypeAorO;
	}

	public void setTrialSubmTypeAorO(String trialSubmTypeAorO) {
		this.trialSubmTypeAorO = trialSubmTypeAorO;
	}

	public String getTrialSubmTypeAorU() {
		return trialSubmTypeAorU;
	}

	public void setTrialSubmTypeAorU(String trialSubmTypeAorU) {
		this.trialSubmTypeAorU = trialSubmTypeAorU;
	}

	public void setDraftXmlRequired(Integer draftXmlRequired) {
		this.draftXmlRequired = draftXmlRequired;
	}

	public Integer getDraftXmlRequired() {
		return draftXmlRequired;
	}

	public List<RadioOption> getDraftXmlReqList() {
		return draftXmlReqList;
	}

	public void setDraftXmlReqList(List<RadioOption> draftXmlReqList) {
		this.draftXmlReqList = draftXmlReqList;
	}

	public String getAmendNumber() {
		return amendNumber;
	}

	public void setAmendNumber(String amendNumber) {
		this.amendNumber = amendNumber;
	}

	public String getAmendDate() {
		return amendDate;
	}

	public void setAmendDate(String amendDate) {
		this.amendDate = amendDate;
	}

	public String getNciTrialId() {
		return nciTrialId;
	}

	public void setNciTrialId(String nciTrialId) {
		this.nciTrialId = nciTrialId;
	}

	public String getLeadOrgTrialId() {
		return this.leadOrgTrialId;
	}

	public void setLeadOrgTrialId(String leadOrgTrialId) {
		this.leadOrgTrialId = leadOrgTrialId;
	}

	public String getNctNumber() {
		return nctNumber;
	}

	public void setNctNumber(String nctNumber) {
		this.nctNumber = nctNumber;
	}

	public String getOtherTrialId() {
		return otherTrialId;
	}

	public void setOtherTrialId(String otherTrialId) {
		this.otherTrialId = otherTrialId;
	}
	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getStudyIds() {
		return studyIds;
	}

	public void setStudyIds(String studyIds) {
		this.studyIds = studyIds;
	}

	public String getCtrpUnmark() {
		return ctrpUnmark;
	}

	public void setCtrpUnmark(String ctrpUnmark) {
		this.ctrpUnmark = ctrpUnmark;
	}

	public String getGrpId() {
		return grpId;
	}

	public void setGrpId(String grpId) {
		this.grpId = grpId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getRightSeq() {
		return rightSeq;
	}

	public void setRightSeq(String rightSeq) {
		this.rightSeq = rightSeq;
	}

	public String getSuperuserRights() {
		return superuserRights;
	}

	public void setSuperuserRights(String superuserRights) {
		this.superuserRights = superuserRights;
	}

	public String getPmtclsId() {
		return pmtclsId;
	}

	public void setPmtclsId(String pmtclsId) {
		this.pmtclsId = pmtclsId;
	}

	public String getUserIdS() {
		return userIdS;
	}

	public void setUserIdS(String userIdS) {
		this.userIdS = userIdS;
	}

	public String getStudyDraftsMenu() {
		return studyDraftsMenu;
	}

	public Integer getStudyDraftsInt() {
		return studyDraftsInt;
	}

	public void setStudyDraftsInt(Integer studyDraftsInt) {
		this.studyDraftsInt = studyDraftsInt;
		// to select CTRP designation by default
		//studyDraftsMenu = studyDraftsDao.toPullDown(studyDraftCodeType, studyDraftsInt);
	}

	public int unmarkStudyCtrpReportable(String studyIds) {
		int ret=0;
		try{
			CtrpDraftAgent ctrpDraftAgent = this.getCtrpDraftAgent();
			Rlog.debug("studyDraft", "CtrpDraftJB.unmarkStudyCtrpReportable after remote");
			ret=ctrpDraftAgent.unmarkStudyCtrpReportable(studyIds);
			Rlog.debug("studyDraft", "CtrpDraftJB.unmarkStudyCtrpReportable after Dao");
			return ret;
		} catch (Exception e) {
			Rlog.fatal("studyDraft", "Error in unmarkStudyCtrpReportable in CtrpDraftJB " + e);
			return ret;
		}

	}
	public int deleteStudyDrafts(String studyIds,String draftIds) {
		int ret=0;
		try{
			CtrpDraftAgent ctrpDraftAgent = this.getCtrpDraftAgent();
			Rlog.debug("studyDraft", "CtrpDraftJB.deleteStudyDrafts remote");
			ret=ctrpDraftAgent.deleteStudyDrafts(studyIds,draftIds);
			Rlog.debug("studyDraft", "CtrpDraftJB.deleteStudyDrafts after Dao");
			return ret;
		} catch (Exception e) {
			Rlog.fatal("studyDraft", "Error in deleteStudyDrafts in CtrpDraftJB " + e);
			return ret;
		}

	}
	//Modified for Bug#8029:Akshi
	public int deleteStudyDrafts(String studyIds,String draftIds,Hashtable<String, String> args) {
		int ret=0;
		try{
			CtrpDraftAgent ctrpDraftAgent = this.getCtrpDraftAgent();
			Rlog.debug("studyDraft", "CtrpDraftJB.deleteStudyDrafts remote");
			ret=ctrpDraftAgent.deleteStudyDrafts(studyIds,draftIds,args);
			Rlog.debug("studyDraft", "CtrpDraftJB.deleteStudyDrafts after Dao");
			return ret;
		} catch (Exception e) {
			Rlog.fatal("studyDraft", "Error in deleteStudyDrafts in CtrpDraftJB " + e);
			return ret;
		}
	}
	public String getStudyPhase() {
		return studyPhase;
	}

	public void setStudyPhase(String studyPhase) {
		this.studyPhase = studyPhase;
	}

	public Integer getDraftPilot() {
		return draftPilot;
	}

	public void setDraftPilot(Integer draftPilot) {
		if (draftPilot == null) { draftPilot = 0; } // 0 = No by default
		this.draftPilot = draftPilot;
	}

	public List<RadioOption> getDraftPilotList() {
		return draftPilotList;
	}

	public void setDraftPilotList(List<RadioOption> draftPilotList) {
		this.draftPilotList = draftPilotList;
	}

	public List<RadioOption> getDraftStudyTypeList() {
		return draftStudyTypeList;
	}

	public void setDraftStudyTypeList(List<RadioOption> draftStudyTypeList) {
		this.draftStudyTypeList = draftStudyTypeList;
	}

	public Integer getDraftStudyType() {
		return draftStudyType;
	}

	public void setDraftStudyType(Integer draftStudyType) {
		this.draftStudyType = draftStudyType;
	}

	public String getStudyType() {
		return studyType;
	}

	public void setStudyType(String studyType) {
		this.studyType = studyType;
	}

	public void setDraftStudyPhaseMenu(String draftStudyPhaseMenu) {
		this.draftStudyPhaseMenu = draftStudyPhaseMenu;
	}

	public String getDraftStudyPhaseMenu() {
		return draftStudyPhaseMenu;
	}

	public Integer getDraftStudyPhase() {
		return draftStudyPhase;
	}

	public void setDraftStudyPhase(Integer draftStudyPhase) {
		this.draftStudyPhase = draftStudyPhase;
		this.draftStudyPhaseMenu = draftStudyPhaseDao.toPullDown(FLD_STUDY_PHASE, 
				draftStudyPhase, ON_CHANGE_STUDY_PHASE);
	}

	public void setDraftStudyPhaseNA(String draftStudyPhaseNA) {
		this.draftStudyPhaseNA = draftStudyPhaseNA;
	}

	public String getDraftStudyPhaseNA() {
		return draftStudyPhaseNA;
	}

	public String getStudyPurposeMenu() {
		return studyPurposeMenu;
	}

	public void setStudyPurposeMenu(String studyPurposeMenu) {
		this.studyPurposeMenu = studyPurposeMenu;
	}

	public Integer getStudyPurpose() {
		return studyPurpose;
	}

	public void setStudyPurpose(Integer studyPurpose) {
		this.studyPurpose = studyPurpose;
		this.studyPurposeMenu = studyPurposeDao.toPullDown(FLD_STUDY_PURPOSE, 
				studyPurpose, ON_CHANGE_STUDY_PURPOSE);
	}

	public String getStudyPurposeOtherPk() {
		return studyPurposeOtherPk;
	}

	public void setStudyPurposeOtherPk(String studyPurposeOtherPk) {
		this.studyPurposeOtherPk = studyPurposeOtherPk;
	}

	public String getStudyPurposeOther() {
		return studyPurposeOther;
	}

	public void setStudyPurposeOther(String studyPurposeOther) {
		this.studyPurposeOther = studyPurposeOther;
	}

	public String getDraftStatusMenu() {
		return draftStatusMenu;
	}

	public void setDraftStatusMenu(String draftStatusMenu) {
		this.draftStatusMenu = draftStatusMenu;
	}

	public Integer getDraftStatus() {
		return draftStatus;
	}

	public void setDraftStatus(Integer draftStatus) {
		this.draftStatus = draftStatus;
		this.draftStatusMenu = filterDraftStatusesForDraftDetails(draftStatusDao, draftStatus);
	}

	public void setLeadOrgCtrpId(String leadOrgCtrpId) {
		this.leadOrgCtrpId = leadOrgCtrpId;
	}

	public String getLeadOrgCtrpId() {
		return leadOrgCtrpId;
	}
	
	// Added for CTRP-20579, Ankit (Start)
	public void setDraftTrialStatusMenu(String draftTrialStatusMenu) {
		this.draftTrialStatusMenu = draftTrialStatusMenu;
	}

	public String getDraftTrialStatusMenu() {
		return draftTrialStatusMenu;
	}
	
	public List<RadioOption> getTrialStartTypeList() {
		return trialStartTypeList;
	}

	public void setTrialStartTypeList(List<RadioOption> trialStartTypeList) {
		this.trialStartTypeList = trialStartTypeList;
	}

	public List<RadioOption> getPrimaryCompletionTypeList() {
		return primaryCompletionTypeList;
	}

	public void setPrimaryCompletionTypeList(List<RadioOption> primaryCompletionTypeList) {
		this.primaryCompletionTypeList = primaryCompletionTypeList;
	}
	//Added for CTRP-20579, Ankit  (End)

	private String createDraftSubType(CodeDao codeDao, int selectedOptionValue)
	{
		ArrayList pkList = codeDao.getCId();
		ArrayList descList = codeDao.getCDesc();
		ArrayList subList = codeDao.getCSubType();
		StringBuffer sb1 =  new StringBuffer();
		sb1.append("<select name='").append(FLD_SUBM_TYPE).append("'  id='").append(FLD_SUBM_TYPE).append("' ");
		sb1.append(">");
		try {
			for (int iX=0; iX<pkList.size(); iX++) {
				if (LETTER_A.equals(subList.get(iX))) { continue; } // Skip "A" status
				sb1.append("<option value='").append(pkList.get(iX)).append("' ");
				if (selectedOptionValue == (Integer)pkList.get(iX)) { sb1.append("SELECTED"); }else if (LETTER_O.equals(subList.get(iX))){sb1.append("SELECTED"); }
				sb1.append(">").append(descList.get(iX)).append("</option>");
			}
			if (selectedOptionValue == 0){
				sb1.append("<OPTION value='' SELECTED>").append(LC.L_Select_AnOption).append("</OPTION>");
            } else {
            	sb1.append("<OPTION value='' >").append(LC.L_Select_AnOption).append("</OPTION>");
            }
		} catch(Exception e) {
			e.printStackTrace();
		}
		sb1.append("</select>");
		return sb1.toString();
	}

	public String getTrialUsrId() {
		return trialUsrId;
	}

	public void setTrialUsrId(String trialUsrId) {
		this.trialUsrId = trialUsrId;
	}

	public String getTrialFirstName() {
		return trialFirstName;
	}

	public void setTrialFirstName(String trialFirstName) {
		this.trialFirstName = trialFirstName;
	}

	public String getTrialLastName() {
		return trialLastName;
	}

	public void setTrialLastName(String trialLastName) {
		this.trialLastName = trialLastName;
	}

	public String getTrialEmailId() {
		return trialEmailId;
	}

	public void setTrialEmailId(String trialEmailId) {
		this.trialEmailId = trialEmailId;
	}

	public String getSolIdentifierId() {
		return solIdentifierId;
	}

	public void setSolIdentifierId(String solIdentifierId) {
		this.solIdentifierId = solIdentifierId;
	}

	public String getStudyDiseaseSite() {
		return studyDiseaseSite;
	}

	public void setStudyDiseaseSite(String studyDiseaseSite) {
		this.studyDiseaseSite = studyDiseaseSite;
	}
	public void setCtrpIntvnTypeMenu(String ctrpIntvnTypeMenu) {
		this.ctrpIntvnTypeMenu = ctrpIntvnTypeMenu;
	}
	public String getCtrpIntvnTypeMenu() {
		return ctrpIntvnTypeMenu;
	}

	public Integer getCtrpIntvnTypeInt() {
		return ctrpIntvnTypeInt;
	}

	public void setCtrpIntvnTypeInt(Integer ctrpIntvnTypeInt) {
		this.ctrpIntvnTypeInt = ctrpIntvnTypeInt;
		ctrpIntvnTypeMenu =ctrpIntvnTypeDao.toPullDown(FLD_CTRP_INTERVENTION_TYPE, ctrpIntvnTypeInt);
	}

	public String getInterventionName() {
		return interventionName;
	}

	public void setInterventionName(String interventionName) {
		this.interventionName = interventionName;
	}

	public String getStudyDiseaseName() {
		return studyDiseaseName;
	}

	public void setStudyDiseaseName(String studyDiseaseName) {
		this.studyDiseaseName = studyDiseaseName;
	}
	
	// Added for CTRP-20579, Ankit (Start)
	public Integer getTrialStatus() {
		return trialStatus;
	}

	public void setTrialStatusInt(Integer trialStatus) {
		this.trialStatus = trialStatus;
		this.draftTrialStatusMenu = trialStatusDao.toPullDown(FLD_TRIAL_STATUS, 
				trialStatus, ON_CHANGE_TRIAL_STATUS);
	}

	public String getTrialStatusDate() {
		return trialStatusDate;
	}

	public void setTrialStatusDate(String trialStatusDate) {
		this.trialStatusDate = trialStatusDate;
	}

	public String getStudyStopReason() {
		return studyStopReason;
	}

	public void setStudyStopReason(String studyStopReason) {
		this.studyStopReason = studyStopReason;
	}

	public String getTrialStartDate() {
		return trialStartDate;
	}

	public void setTrialStartDate(String trialStartDate) {
		this.trialStartDate = trialStartDate;
	}

	public Integer getTrialStartType() {
		return trialStartType;
	}

	public void setTrialStartType(Integer trialStartType) {
		this.trialStartType = trialStartType;
	}

	public String getPrimaryCompletionDate() {
		return primaryCompletionDate;
	}

	public void setPrimaryCompletionDate(String primaryCompletionDate) {
		this.primaryCompletionDate = primaryCompletionDate;
	}

	public Integer getPrimaryCompletionType() {
		return primaryCompletionType;
	}

	public void setPrimaryCompletionType(Integer primaryCompletionType) {
		this.primaryCompletionType = primaryCompletionType;
	}

	public String getStudyEndDate() {
		return studyEndDate;
	}

	public void setStudyEndDate(String studyEndDate) {
		this.studyEndDate = studyEndDate;
	}

	public String getStudyActiveDate() {
		return studyActiveDate;
	}

	public void setStudyActiveDate(String studyActiveDate) {
		this.studyActiveDate = studyActiveDate;
	}

	public String getStatusValidFrm() {
		return statusValidFrm;
	}

	public void setStatusValidFrm(String statusValidFrm) {
		this.statusValidFrm = statusValidFrm;
	}

	public String getCurrStudyStatus() {
		return currStudyStatus;
	}

	public void setCurrStudyStatus(String currStudyStatus) {
		this.currStudyStatus = currStudyStatus;
	}

	public String getStatusTypeAdminComplete() {
		if (statusTypeAdminComplete == null) { return EMPTY_STRING; }
		return statusTypeAdminComplete;
	}

	public void setStatusTypeAdminComplete(String statusTypeAdminComplete) {
		this.statusTypeAdminComplete = statusTypeAdminComplete;
	}

	public String getStatusTypeWithdrawn() {
		if (statusTypeWithdrawn == null) { return EMPTY_STRING; }
		return statusTypeWithdrawn;
	}

	public void setStatusTypeWithdrawn(String statusTypeWithdrawn) {
		this.statusTypeWithdrawn = statusTypeWithdrawn;
	}

	public String getStatusTypeTempClosedAcc() {
		if (statusTypeTempClosedAcc == null) { return EMPTY_STRING; }
		return statusTypeTempClosedAcc;
	}

	public void setStatusTypeTempClosedAcc(String statusTypeTempClosedAcc) {
		this.statusTypeTempClosedAcc = statusTypeTempClosedAcc;
	}
	//Added for CTRP-20579, Ankit  (End)
	public void setLeadOrgTypeMenu(String leadOrgTypeMenu) {
		this.leadOrgTypeMenu = leadOrgTypeMenu;
	}
	public String getLeadOrgTypeMenu() {
		return leadOrgTypeMenu;
	}

	public void setLeadOrgTypeInt(Integer leadOrgTypeInt) {
		this.leadOrgTypeInt = leadOrgTypeInt;
		if (leadOrgTypeInt == null) { this.leadOrgTypeInt = 0; }
		this.leadOrgTypeMenu = leadOrgTypeDao.toPullDown(FLD_LEAD_ORG_TYPE, this.leadOrgTypeInt);
	}

	public Integer getLeadOrgTypeInt() {
		return leadOrgTypeInt;
	}

	public void setLeadOrgFkSite(String leadOrgFkSite) {
		this.leadOrgFkSite = leadOrgFkSite;
	}

	public String getLeadOrgFkSite() {
		return leadOrgFkSite;
	}

	public void setSumm4FkSite(String summ4FkSite) {
		this.summ4FkSite = summ4FkSite;
	}

	public String getSumm4FkSite() {
		return summ4FkSite;
	}
	
	//Ak: Added for CTRP Trial documents 
	
	public String getProtocolDocDesc() {
		return protocolDocDesc;
	}
	public void setProtocolDocDesc(String protocolDocDesc) {
		this.protocolDocDesc = protocolDocDesc;
	}
	public String getIRBApprovalDesc() {
		return IRBApprovalDesc;
	}
	public void setIRBApprovalDesc(String iRBApprovalDesc) {
		IRBApprovalDesc = iRBApprovalDesc;
	}
	public String getPartSitesListDesc() {
		return partSitesListDesc;
	}
	public void setPartSitesListDesc(String partSitesListDesc) {
		this.partSitesListDesc = partSitesListDesc;
	}
	public String getInformedConsentDesc() {
		return informedConsentDesc;
	}
	public void setInformedConsentDesc(String informedConsentDesc) {
		this.informedConsentDesc = informedConsentDesc;
	}
	public String getChangeMemoDocDesc() {
		return changeMemoDocDesc;
	}
	public void setChangeMemoDocDesc(String changeMemoDocDesc) {
		this.changeMemoDocDesc = changeMemoDocDesc;
	}
	public String getProtHighlightDesc() {
		return protHighlightDesc;
	}
	public void setProtHighlightDesc(String protHighlightDesc) {
		this.protHighlightDesc = protHighlightDesc;
	}
	public String getOtherDesc() {
		return otherDesc;
	}
	public void setOtherDesc(String otherDesc) {
		this.otherDesc = otherDesc;
	}
	public Integer getProtocolDocPK() {
		return protocolDocPK;
	}
	public void setProtocolDocPK(Integer protocolDocPK) {
		this.protocolDocPK = protocolDocPK;
	}
	public Integer getIRBApprovalPK() {
		return IRBApprovalPK;
	}
	public void setIRBApprovalPK(Integer iRBApprovalPK) {
		IRBApprovalPK = iRBApprovalPK;
	}
	public Integer getPartSitesListPK() {
		return partSitesListPK;
	}
	public void setPartSitesListPK(Integer partSitesListPK) {
		this.partSitesListPK = partSitesListPK;
	}
	public Integer getInformedConsentPK() {
		return informedConsentPK;
	}
	public void setInformedConsentPK(Integer informedConsentPK) {
		this.informedConsentPK = informedConsentPK;
	}
	public Integer getChangeMemoDocPK() {
		return changeMemoDocPK;
	}
	public void setChangeMemoDocPK(Integer changeMemoDocPK) {
		this.changeMemoDocPK = changeMemoDocPK;
	}
	public Integer getProtHighlightPK() {
		return protHighlightPK;
	}
	public void setProtHighlightPK(Integer protHighlightPK) {
		this.protHighlightPK = protHighlightPK;
	}
	public Integer getOtherPK() {
		return otherPK;
	}
	public void setOtherPK(Integer otherPK) {
		this.otherPK = otherPK;
	}
	public Integer getTrialProtocolDocPK() {
		return trialProtocolDocPK;
	}

	public void setTrialProtocolDocPK(Integer trialProtocolDocPK) {
		this.trialProtocolDocPK = trialProtocolDocPK;
	}

	public Integer getTrialIRBApprovalPK() {
		return trialIRBApprovalPK;
	}

	public void setTrialIRBApprovalPK(Integer trialIRBApprovalPK) {
		this.trialIRBApprovalPK = trialIRBApprovalPK;
	}

	public Integer getTrialPartSitesListPK() {
		return trialPartSitesListPK;
	}

	public void setTrialPartSitesListPK(Integer trialPartSitesListPK) {
		this.trialPartSitesListPK = trialPartSitesListPK;
	}

	public Integer getTrialInformedConsentPK() {
		return trialInformedConsentPK;
	}

	public void setTrialInformedConsentPK(Integer trialInformedConsentPK) {
		this.trialInformedConsentPK = trialInformedConsentPK;
	}

	public Integer getTrialChangeMemoDocPK() {
		return trialChangeMemoDocPK;
	}

	public void setTrialChangeMemoDocPK(Integer trialChangeMemoDocPK) {
		this.trialChangeMemoDocPK = trialChangeMemoDocPK;
	}

	public Integer getTrialProtHighlightPK() {
		return trialProtHighlightPK;
	}

	public void setTrialProtHighlightPK(Integer trialProtHighlightPK) {
		this.trialProtHighlightPK = trialProtHighlightPK;
	}

	public Integer getTrialOtherPK() {
		return trialOtherPK;
	}

	public void setTrialOtherPK(Integer trialOtherPK) {
		this.trialOtherPK = trialOtherPK;
	}
	
	public Integer setCtrpDraftDocinfo(HashMap<String, String> argMap,Integer draftId) {
		CTRPDraftDocJB ctrpDraftDocJB = new CTRPDraftDocJB();
		CtrpDraftJB ctrpDraftJB = new CtrpDraftJB();
		Integer outputTrail=0;
		CodeDao ctrpDoctType= new CodeDao();        
		ctrpDraftDocJB.setFkCtrpDraft(draftId);
		if((this.protocolDocPK!=null && this.protocolDocPK>0) || this.trialProtocolDocPK>0){	
			if(this.protocolDocPK!=null && this.protocolDocPK>0){
				ctrpDraftDocJB.setFkStudyApndx(this.protocolDocPK);
				ctrpDraftDocJB.setId(this.trialProtocolDocPK);
				ctrpDraftDocJB.setFkCtrpDocType(ctrpDoctType.getCodeId(ctrpDraftJB.CDLST_TYPE,CDLST_PROTOCOLDOC)); 
			    outputTrail = ctrpDraftDocJB.setCtrpDraftDocInfo(argMap);
				
			}else{
			  outputTrail = ctrpDraftDocJB.deleteCTRPDraftDocRecord(this.trialProtocolDocPK);
			}
			if (outputTrail < 0) return outputTrail;
			
		}if((this.IRBApprovalPK!=null && this.IRBApprovalPK>0)|| this.trialIRBApprovalPK >0){	
			if(this.IRBApprovalPK!=null && this.IRBApprovalPK>0){
				ctrpDraftDocJB.setId(this.trialIRBApprovalPK);
				ctrpDraftDocJB.setFkStudyApndx(this.IRBApprovalPK);
				ctrpDraftDocJB.setFkCtrpDocType(ctrpDoctType.getCodeId(ctrpDraftJB.CDLST_TYPE,CDLST_IRBAPPROVAL)); 
				outputTrail = ctrpDraftDocJB.setCtrpDraftDocInfo(argMap);
				
			}else{
				 outputTrail = ctrpDraftDocJB.deleteCTRPDraftDocRecord(this.trialIRBApprovalPK);
			}	
			if (outputTrail < 0) return outputTrail;
		}if((this.partSitesListPK!=null && this.partSitesListPK>0 )|| this.trialPartSitesListPK >0){	
			if(this.partSitesListPK!=null && this.partSitesListPK>0){
				ctrpDraftDocJB.setId(this.trialPartSitesListPK);
				ctrpDraftDocJB.setFkStudyApndx(this.partSitesListPK);
				ctrpDraftDocJB.setFkCtrpDocType(ctrpDoctType.getCodeId(ctrpDraftJB.CDLST_TYPE,CDLST_PARTSITESLIST)); 
				outputTrail = ctrpDraftDocJB.setCtrpDraftDocInfo(argMap);
				
			}else{
				outputTrail = ctrpDraftDocJB.deleteCTRPDraftDocRecord(this.trialPartSitesListPK);
			}
			if (outputTrail < 0) return outputTrail;
		}if((this.informedConsentPK!=null && this.informedConsentPK >0) || this.trialInformedConsentPK>0){
			if(this.informedConsentPK!=null && this.informedConsentPK >0){
				ctrpDraftDocJB.setId(this.trialInformedConsentPK);
				ctrpDraftDocJB.setFkStudyApndx(this.informedConsentPK);
				ctrpDraftDocJB.setFkCtrpDocType(ctrpDoctType.getCodeId(ctrpDraftJB.CDLST_TYPE,CDLST_INFORMEDCONSENT)); 
				outputTrail = ctrpDraftDocJB.setCtrpDraftDocInfo(argMap);
				
			}else{
				outputTrail = ctrpDraftDocJB.deleteCTRPDraftDocRecord(this.trialInformedConsentPK);
			}
			if (outputTrail < 0) return outputTrail;
		}if((this.changeMemoDocPK!=null &&this.changeMemoDocPK >0) || trialChangeMemoDocPK>0 ){
			if(this.changeMemoDocPK!=null &&this.changeMemoDocPK >0){
				ctrpDraftDocJB.setId(this.trialChangeMemoDocPK);
				ctrpDraftDocJB.setFkStudyApndx(this.changeMemoDocPK);
				ctrpDraftDocJB.setFkCtrpDocType(ctrpDoctType.getCodeId(ctrpDraftJB.CDLST_TYPE,CDLST_CHANGEMEMODOC)); 
				outputTrail = ctrpDraftDocJB.setCtrpDraftDocInfo(argMap);

			}else{
				outputTrail = ctrpDraftDocJB.deleteCTRPDraftDocRecord(this.trialChangeMemoDocPK);
			}
			if (outputTrail < 0) return outputTrail;
		}if((this.protHighlightPK !=null &&this.protHighlightPK >0) || trialProtHighlightPK>0){
			if(this.protHighlightPK !=null &&this.protHighlightPK >0){
				ctrpDraftDocJB.setId(this.trialProtHighlightPK);
				ctrpDraftDocJB.setFkStudyApndx(this.protHighlightPK);
				ctrpDraftDocJB.setFkCtrpDocType(ctrpDoctType.getCodeId(ctrpDraftJB.CDLST_TYPE,CDLST_PROTHIGHLIGHT)); 
				outputTrail = ctrpDraftDocJB.setCtrpDraftDocInfo(argMap);
				
			}else{
				outputTrail = ctrpDraftDocJB.deleteCTRPDraftDocRecord(this.trialProtHighlightPK);
			}
			if (outputTrail < 0) return outputTrail;
		}if((this.otherPK!=null &&this.otherPK >0) || trialOtherPK>0){
			if(this.otherPK!=null &&this.otherPK >0){
				ctrpDraftDocJB.setId(this.trialOtherPK);
				ctrpDraftDocJB.setFkStudyApndx(this.otherPK);
				ctrpDraftDocJB.setFkCtrpDocType(ctrpDoctType.getCodeId(ctrpDraftJB.CDLST_TYPE,CDLST_OTHER)); 
				outputTrail = ctrpDraftDocJB.setCtrpDraftDocInfo(argMap);
				
			}else{
				outputTrail = ctrpDraftDocJB.deleteCTRPDraftDocRecord(this.trialOtherPK);
			}
			if (outputTrail < 0) return outputTrail;
		}if((this.abbrTrialTempPK!=null &&this.abbrTrialTempPK >0) || this.trialAbbrTrialTempPK>0){/*YK:Added for CTRP-20552-7*/
			if(this.abbrTrialTempPK!=null &&this.abbrTrialTempPK >0){
				ctrpDraftDocJB.setId(this.trialAbbrTrialTempPK);
				ctrpDraftDocJB.setFkStudyApndx(this.abbrTrialTempPK);
				ctrpDraftDocJB.setFkCtrpDocType(ctrpDoctType.getCodeId(CDLST_TYPE,CDLST_ABBRTRIALTEMPL)); 
				outputTrail = ctrpDraftDocJB.setCtrpDraftDocInfo(argMap);
				
			}else{
				outputTrail = ctrpDraftDocJB.deleteCTRPDraftDocRecord(this.trialAbbrTrialTempPK);
			}
			if (outputTrail < 0) return outputTrail;
		}
		
		return outputTrail;
        
	}
	
	public void retreiveCtrpDocumentInfo(int draftId) {
		CtrpDraftJBHelper helper = new CtrpDraftJBHelper(this);
		helper.retreiveCtrpDocumentInfo(draftId);
	}
	
	public Integer updateCtrpDraftIndustrial(HashMap<String, String> argMap) {
		Integer output = 0;
		CtrpDraftBean ctrpDraftBean = new CtrpDraftBean();
		fillCtrpDraftBeanIndustrial(ctrpDraftBean);
		if (argMap.get(IP_ADD) != null) {
			ctrpDraftBean.setIpAdd(argMap.get(IP_ADD));
		}
		try {
			CtrpDraftAgent ctrpDraftAgent = this.getCtrpDraftAgent();
			if (id > 0) {
				fillCtrpDraftBeanLastModifiedBy(ctrpDraftBean, argMap);
				output = ctrpDraftAgent.updateCtrpDraft(ctrpDraftBean);
				updateStatusHistoryDetails(argMap,CTRP_DRAFT_STATUS);
				updateStatusHistoryDetails(argMap,ER_CTRP_DRAFT_NCI_STAT);
			} else {
				fetchNextDraftNum(ctrpDraftBean);
				fillCtrpDraftBeanCreator(ctrpDraftBean, argMap);
				output = ctrpDraftAgent.createCtrpDraft(ctrpDraftBean);
				id = output; // newly created ID
				createStatusHistoryDetails(argMap,CTRP_DRAFT_STATUS);
				createStatusHistoryDetails(argMap,ER_CTRP_DRAFT_NCI_STAT);
			}
			updateDraftAddressDetails();
		} catch(Exception e) {
			Rlog.fatal("ctrp", "Exception in CtrpDraftJB.updateCtrpDraftIndustrial: "+e);
			output = -1;
		}
		return output;
	}
	private void fillCtrpDraftBeanIndustrial(CtrpDraftBean ctrpDraftBean ) {
		if (this.id> 0) { ctrpDraftBean.setId(this.id); }
		
		//------------- Trial Submission ------------//
		ctrpDraftBean.setFkStudy(StringUtil.stringToInteger(this.fkStudy));
		ctrpDraftBean.setCtrpDraftType(1); // 1 = industrial
		ctrpDraftBean.setFkCodelstSubmissionType(this.indusTrialTypeInt);
		ctrpDraftBean.setFkTrialOwner(StringUtil.stringToInteger(this.trialUsrId));
		
		//------------- Trial Identification  ------------//
		
		ctrpDraftBean.setNciStudyId(this.nciTrialId);
		ctrpDraftBean.setLeadOrgStudyId(this.leadOrgTrialId);
		ctrpDraftBean.setSubmitOrgStudyId(this.subOrgTrialId);
		ctrpDraftBean.setNctNumber(this.nctNumber);
		
		//------------- Trial Details  ------------//
		ctrpDraftBean.setFkCodelstPhase(this.draftStudyPhase);
		if (this.draftStudyPhase == Integer.parseInt(this.draftStudyPhaseNA)) {
			ctrpDraftBean.setDraftPilot(this.draftPilot);
		}
		ctrpDraftBean.setStudyType(this.draftStudyType);
		ctrpDraftBean.setFkCodelstInterventionType(this.ctrpIntvnTypeInt);
		ctrpDraftBean.setInterventionName(this.interventionName);
		if(!StringUtil.isEmpty(this.studyDiseaseSite))
		{ctrpDraftBean.setFkCodelstDiseaseSite(this.studyDiseaseSite);}
		ctrpDraftBean.setStudyPurpose(this.studyPurpose);
		if (this.studyPurpose == Integer.parseInt(studyPurposeOtherPk)) {
			ctrpDraftBean.setStudyPurposeOther(this.studyPurposeOther);
		}
		//------------- Lead or Submitting Organizations / Principal Investigator  ------------//
		 //---- Lead Organization ----//
		
			ctrpDraftBean.setLeadOrgCtrpId(this.leadOrgCtrpId);
			ctrpDraftBean.setLeadOrgType(this.leadOrgTypeInt);
			if(!StringUtil.isEmpty(this.leadOrgFkAdd))
			{
			ctrpDraftBean.setFkSiteLeadOrg(StringUtil.stringToInteger(this.leadOrgFkSite));
			ctrpDraftBean.setFkAddLeadOrg(StringUtil.stringToInteger(this.leadOrgFkAdd));
			}
		 //---- Submitting Organization ----//
			ctrpDraftBean.setSubmitOrgCtrpId(this.subOrgCtrpId);
			ctrpDraftBean.setSubmitOrgType(this.subOrgTypeInt);
			ctrpDraftBean.setSubmitNCIDesignated(this.isNCIDesignated);
			if(!StringUtil.isEmpty(this.subOrgFkAdd))
			{
				ctrpDraftBean.setFkSiteSubmitOrg(StringUtil.stringToInteger(this.subOrgFkSite));
				ctrpDraftBean.setFkAddSubmitOrg(StringUtil.stringToInteger(this.subOrgFkAdd));
			}
			ctrpDraftBean.setSponsorProgramCode(this.subOrgProgCode);
		 //---- Principal Investigator ----//
			ctrpDraftBean.setPiCtrpId(this.piId); 
			ctrpDraftBean.setFkUserPI(StringUtil.stringToInteger(this.piFkUser));
			ctrpDraftBean.setPiFirstName(this.piFirstName);
			ctrpDraftBean.setPiLastName(this.piLastName);
			ctrpDraftBean.setFkAddPi(StringUtil.stringToInteger(this.piFkAdd));
			
		//------------- Summary4 Information (for trials at NCI-designated cancer centers)   ------------//
		ctrpDraftBean.setSumm4SponsorId(this.summ4CtrpId);
		ctrpDraftBean.setSumm4SponsorType(this.summ4TypeInt);
		if(!StringUtil.isEmpty(this.summ4FkAdd))
		{
		ctrpDraftBean.setFkSiteSumm4Sponsor(StringUtil.stringToInteger(this.summ4FkSite));
		ctrpDraftBean.setFkAddSumm4Sponsor(StringUtil.stringToInteger(this.summ4FkAdd));
		}

		//------------- Status/Dates (site specific)  ------------//
		ctrpDraftBean.setFkCodelstCtrpStudyStatus(this.siteRecruitTypeInt);
		ctrpDraftBean.setCtrpStudyStatusDate(DateUtil.stringToDate(this.recStatusDate));
		ctrpDraftBean.setCtrpStudyAccOpenDate(DateUtil.stringToDate(this.openAccrualDate));
		ctrpDraftBean.setCtrpStudyAccClosedDate(DateUtil.stringToDate(this.closedAccrualDate));
		if(this.isNCIDesignated==1)
		{
			ctrpDraftBean.setSiteTargetAccrual(StringUtil.stringToInteger(this.siteTrgtAccrual));
		}
		//------------- Trial Related Documents  ------------//
		
		//22472
		ctrpDraftBean.setDraftNum(this.draftNum);
	}
	/*Added for download CVS Section : Yogendra Pratap Singh*/
	public ArrayList<CtrpDraftCSVExportJB>  ctrpDraftCSVExport(String draftIds,int nonIndustryFlag){
		String[] draftIdsArr = null;
		ArrayList<CtrpDraftBean> aList = new ArrayList<CtrpDraftBean>();
		ArrayList<CtrpDraftCSVExportJB> exportList = new ArrayList<CtrpDraftCSVExportJB>();
		CtrpDraftJBHelper ctrpDraftJBHelper = new CtrpDraftJBHelper(this);
		CtrpDraftCSVExportJB ctrpDraftCSVExportJB = null;
		try {
			CtrpDraftAgent ctrpDraftAgent = this.getCtrpDraftAgent();
			if(nonIndustryFlag==1){
				ctrpDraftCSVExportJB  = ctrpDraftJBHelper.addNonIndustryCSVExportHeader();
			}else{
				ctrpDraftCSVExportJB  = ctrpDraftJBHelper.addIndustryCSVExportHeader();
			}
			exportList.add(ctrpDraftCSVExportJB);//Add Header To the List
			String[] drafIdsArr1 = {draftIds}; 
			if(draftIds.indexOf(",") != -1)
				draftIdsArr = draftIds.split(",");
			else
				draftIdsArr = drafIdsArr1;
			
			for( String draftId:draftIdsArr){
				CtrpDraftBean ctrpDraftBean = new CtrpDraftBean();
				ctrpDraftBean=ctrpDraftAgent.getCtrpDraftBean(StringUtil.stringToInteger(draftId));
				aList.add(ctrpDraftBean);
			}
			if(nonIndustryFlag==1){
				exportList = ctrpDraftJBHelper.retriveNonIndustryCSVExportData(exportList , aList);
			}else{
				exportList = ctrpDraftJBHelper.retriveIndudtryCSVExportData(exportList , aList);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return exportList;
	}
	/*Added for download CVS Section : Yogendra Pratap Singh*/
	public boolean generateCSVDocument(ArrayList<CtrpDraftCSVExportJB> exportList,int nonIndustrialFlag)throws IOException{
		boolean result = false;
		FileWriter fw = null;
		CtrpDraftJBHelper ctrpDraftJBHelper = null;
		try{
			ctrpDraftJBHelper = new CtrpDraftJBHelper(this);
			fw = ctrpDraftJBHelper.getFileWriterForCSVExport(nonIndustrialFlag);
			if(nonIndustrialFlag==1){
				fw = ctrpDraftJBHelper.generateNonIndustryCSVDcoument(exportList,fw);
				result = true;
			}else{
				fw = ctrpDraftJBHelper.generateIndustryCSVDcoument(exportList,fw);
				result = true;
			}
		}catch(IOException io){
			result = false;
			io.printStackTrace();
		}finally{
			fw.close();
		}
		return result;
	}

	public String getSubOrgTypeMenu() {
		return subOrgTypeMenu;
	}

	public void setSubOrgTypeMenu(String subOrgTypeMenu) {
		this.subOrgTypeMenu = subOrgTypeMenu;
	}

	public Integer getSubOrgTypeInt() {
		return subOrgTypeInt;
	}

	public void setSubOrgTypeInt(Integer subOrgTypeInt) {
		this.subOrgTypeInt = subOrgTypeInt;
		subOrgTypeMenu = subOrgTypeDao.toPullDown(FLD_SUB_ORG_TYPE,subOrgTypeInt);
	}

	public List<RadioOption> getIsNCIDesignatedList() {
		return isNCIDesignatedList;
	}

	public void setIsNCIDesignatedList(List<RadioOption> isNCIDesignatedList) {
		this.isNCIDesignatedList = isNCIDesignatedList;
	}

	public Integer getIsNCIDesignated() {
		return isNCIDesignated;
	}

	public void setIsNCIDesignated(Integer isNCIDesignated) {
		this.isNCIDesignated = isNCIDesignated;
	}

	public String getSubOrgTrialId() {
		return subOrgTrialId;
	}

	public void setSubOrgTrialId(String subOrgTrialId) {
		this.subOrgTrialId = subOrgTrialId;
	}

	public String getStudyEnrollClosedDt() {
		return studyEnrollClosedDt;
	}

	public void setStudyEnrollClosedDt(String studyEnrollClosedDt) {
		this.studyEnrollClosedDt = studyEnrollClosedDt;
	}

	public String getSiteRecruitTypeMenu() {
		return siteRecruitTypeMenu;
	}

	public void setSiteRecruitTypeMenu(String siteRecruitTypeMenu) {
		this.siteRecruitTypeMenu = siteRecruitTypeMenu;
	}

	public Integer getSiteRecruitTypeInt() {
		return siteRecruitTypeInt;
	}

	public void setSiteRecruitTypeInt(Integer siteRecruitTypeInt) {
		this.siteRecruitTypeInt = siteRecruitTypeInt;
		siteRecruitTypeMenu=siteRecruitDao.toPullDown(FLD_SITE_RECRUIT_STAT, siteRecruitTypeInt);
	}

	public String getRecStatusDate() {
		return recStatusDate;
	}

	public void setRecStatusDate(String recStatusDate) {
		this.recStatusDate = recStatusDate;
	}

	public String getOpenAccrualDate() {
		return openAccrualDate;
	}

	public void setOpenAccrualDate(String openAccrualDate) {
		this.openAccrualDate = openAccrualDate;
	}

	public String getClosedAccrualDate() {
		return closedAccrualDate;
	}

	public void setClosedAccrualDate(String closedAccrualDate) {
		this.closedAccrualDate = closedAccrualDate;
	}

	public String getSiteTrgtAccrual() {
		return siteTrgtAccrual;
	}

	public void setSiteTrgtAccrual(String siteTrgtAccrual) {
		this.siteTrgtAccrual = siteTrgtAccrual;
	}

	public void setLeadOrgFax(String leadOrgFax) {
		this.leadOrgFax = leadOrgFax;
	}

	public String getLeadOrgFax() {
		return leadOrgFax;
	}

	public void setLeadOrgTty(String leadOrgTty) {
		this.leadOrgTty = leadOrgTty;
	}

	public String getLeadOrgTty() {
		return leadOrgTty;
	}

	public void setLeadOrgUrl(String leadOrgUrl) {
		this.leadOrgUrl = leadOrgUrl;
	}

	public String getLeadOrgUrl() {
		return leadOrgUrl;
	}

	public void setPiId(String piId) {
		this.piId = piId;
	}

	public String getPiId() {
		return piId;
	}

	public void setPiFkUser(String piFkUser) {
		this.piFkUser = piFkUser;
	}

	public String getPiFkUser() {
		return piFkUser;
	}

	public void setLeadOrgFkAdd(String leadOrgFkAdd) {
		this.leadOrgFkAdd = leadOrgFkAdd;
	}

	public String getLeadOrgFkAdd() {
		return leadOrgFkAdd;
	}

	public void setPiFkAdd(String piFkAdd) {
		this.piFkAdd = piFkAdd;
	}

	public String getPiFkAdd() {
		return piFkAdd;
	}

	public void setPiTty(String piTty) {
		this.piTty = piTty;
	}

	public String getPiTty() {
		return piTty;
	}

	public void setPiFax(String piFax) {
		this.piFax = piFax;
	}

	public String getPiFax() {
		return piFax;
	}

	public void setPiUrl(String piUrl) {
		this.piUrl = piUrl;
	}

	public String getPiUrl() {
		return piUrl;
	}

	public void setLeadOrgName(String leadOrgName) {
		this.leadOrgName = leadOrgName;
	}

	public String getLeadOrgName() {
		return leadOrgName;
	}

	public void setLeadOrgStreet(String leadOrgStreet) {
		this.leadOrgStreet = leadOrgStreet;
	}

	public String getLeadOrgStreet() {
		return leadOrgStreet;
	}

	public void setLeadOrgCity(String leadOrgCity) {
		this.leadOrgCity = leadOrgCity;
	}

	public String getLeadOrgCity() {
		return leadOrgCity;
	}

	public void setLeadOrgState(String leadOrgState) {
		this.leadOrgState = leadOrgState;
	}

	public String getLeadOrgState() {
		return leadOrgState;
	}

	public void setLeadOrgZip(String leadOrgZip) {
		this.leadOrgZip = leadOrgZip;
	}

	public String getLeadOrgZip() {
		return leadOrgZip;
	}

	public void setLeadOrgCountry(String leadOrgCountry) {
		this.leadOrgCountry = leadOrgCountry;
	}

	public String getLeadOrgCountry() {
		return leadOrgCountry;
	}

	public void setLeadOrgEmail(String leadOrgEmail) {
		this.leadOrgEmail = leadOrgEmail;
	}

	public String getLeadOrgEmail() {
		return leadOrgEmail;
	}

	public void setLeadOrgPhone(String leadOrgPhone) {
		this.leadOrgPhone = leadOrgPhone;
	}

	public String getLeadOrgPhone() {
		return leadOrgPhone;
	}

	public void setPiFirstName(String piFirstName) {
		this.piFirstName = piFirstName;
	}

	public String getPiFirstName() {
		return piFirstName;
	}

	public void setPiMiddleName(String piMiddleName) {
		this.piMiddleName = piMiddleName;
	}

	public String getPiMiddleName() {
		return piMiddleName;
	}

	public void setPiLastName(String piLastName) {
		this.piLastName = piLastName;
	}

	public String getPiLastName() {
		return piLastName;
	}

	public void setPiStreetAddress(String piStreetAddress) {
		this.piStreetAddress = piStreetAddress;
	}

	public String getPiStreetAddress() {
		return piStreetAddress;
	}

	public void setPiCity(String piCity) {
		this.piCity = piCity;
	}

	public String getPiCity() {
		return piCity;
	}

	public void setPiState(String piState) {
		this.piState = piState;
	}

	public String getPiState() {
		return piState;
	}

	public void setPiZip(String piZip) {
		this.piZip = piZip;
	}

	public String getPiZip() {
		return piZip;
	}

	public void setPiCountry(String piCountry) {
		this.piCountry = piCountry;
	}

	public String getPiCountry() {
		return piCountry;
	}

	public void setPiEmail(String piEmail) {
		this.piEmail = piEmail;
	}

	public String getPiEmail() {
		return piEmail;
	}

	public void setPiPhone(String piPhone) {
		this.piPhone = piPhone;
	}

	public String getPiPhone() {
		return piPhone;
	}
	public String getSumm4CtrpId() {
		return summ4CtrpId;
	}

	public void setSumm4CtrpId(String summ4CtrpId) {
		this.summ4CtrpId = summ4CtrpId;
	}

	public String getSumm4Type() {
		return summ4Type;
	}

	public void setSumm4Type(String summ4Type) {
		this.summ4Type = summ4Type;
	}
	
	public Integer getSumm4TypeInt() {
		return summ4TypeInt;
	}

	public void setSumm4TypeInt(Integer summ4TypeInt) {
		this.summ4TypeInt = summ4TypeInt;
	}

	public String getSumm4Name() {
		return summ4Name;
	}

	public void setSumm4Name(String summ4Name) {
		this.summ4Name = summ4Name;
	}

	public String getSumm4FkAdd() {
		return summ4FkAdd;
	}

	public void setSumm4FkAdd(String summ4FkAdd) {
		this.summ4FkAdd = summ4FkAdd;
	}

	/*public String getSumm4TypeFk() {
		return summ4TypeFk;
	}

	public void setSumm4TypeFk(String summ4TypeFk) {
		this.summ4TypeFk = summ4TypeFk;
	}*/

	public String getSumm4Email() {
		return summ4Email;
	}

	public void setSumm4Email(String summ4Email) {
		this.summ4Email = summ4Email;
	}

	public String getSumm4Street() {
		return summ4Street;
	}

	public void setSumm4Street(String summ4Street) {
		this.summ4Street = summ4Street;
	}

	public String getSumm4City() {
		return summ4City;
	}

	public void setSumm4City(String summ4City) {
		this.summ4City = summ4City;
	}

	public String getSumm4Phone() {
		return summ4Phone;
	}

	public void setSumm4Phone(String summ4Phone) {
		this.summ4Phone = summ4Phone;
	}

	public String getSumm4State() {
		return summ4State;
	}

	public void setSumm4State(String summ4State) {
		this.summ4State = summ4State;
	}

	public String getSumm4Tty() {
		return summ4Tty;
	}

	public void setSumm4Tty(String summ4Tty) {
		this.summ4Tty = summ4Tty;
	}

	public String getSumm4Zip() {
		return summ4Zip;
	}

	public void setSumm4Zip(String summ4Zip) {
		this.summ4Zip = summ4Zip;
	}

	public String getSumm4Fax() {
		return summ4Fax;
	}

	public void setSumm4Fax(String summ4Fax) {
		this.summ4Fax = summ4Fax;
	}

	public String getSumm4Country() {
		return summ4Country;
	}

	public void setSumm4Country(String summ4Country) {
		this.summ4Country = summ4Country;
	}

	public String getSumm4Url() {
		return summ4Url;
	}

	public void setSumm4Url(String summ4Url) {
		this.summ4Url = summ4Url;
	}

	public Integer getTrialAbbrTrialTempPK() {
		return trialAbbrTrialTempPK;
	}

	public void setTrialAbbrTrialTempPK(Integer trialAbbrTrialTempPK) {
		this.trialAbbrTrialTempPK = trialAbbrTrialTempPK;
	}

	public Integer getAbbrTrialTempPK() {
		return abbrTrialTempPK;
	}

	public void setAbbrTrialTempPK(Integer abbrTrialTempPK) {
		this.abbrTrialTempPK = abbrTrialTempPK;
	}

	public String getAbbrTrialTempDesc() {
		return abbrTrialTempDesc;
	}

	public void setAbbrTrialTempDesc(String abbrTrialTempDesc) {
		this.abbrTrialTempDesc = abbrTrialTempDesc;
	}

	public String getSubOrgCtrpId() {
		return subOrgCtrpId;
	}

	public void setSubOrgCtrpId(String subOrgCtrpId) {
		this.subOrgCtrpId = subOrgCtrpId;
	}

	public String getSubOrgFkSite() {
		return subOrgFkSite;
	}

	public void setSubOrgFkSite(String subOrgFkSite) {
		this.subOrgFkSite = subOrgFkSite;
	}
	
	public String getSubOrgName() {
		return subOrgName;
	}

	public void setSubOrgName(String subOrgName) {
		this.subOrgName = subOrgName;
	}

	public String getSubOrgStreet() {
		return subOrgStreet;
	}

	public void setSubOrgStreet(String subOrgStreet) {
		this.subOrgStreet = subOrgStreet;
	}

	public String getSubOrgCity() {
		return subOrgCity;
	}

	public void setSubOrgCity(String subOrgCity) {
		this.subOrgCity = subOrgCity;
	}

	public String getSubOrgState() {
		return subOrgState;
	}

	public void setSubOrgState(String subOrgState) {
		this.subOrgState = subOrgState;
	}

	public String getSubOrgZip() {
		return subOrgZip;
	}

	public void setSubOrgZip(String subOrgZip) {
		this.subOrgZip = subOrgZip;
	}

	public String getSubOrgCountry() {
		return subOrgCountry;
	}

	public void setSubOrgCountry(String subOrgCountry) {
		this.subOrgCountry = subOrgCountry;
	}

	public String getSubOrgEmail() {
		return subOrgEmail;
	}

	public void setSubOrgEmail(String subOrgEmail) {
		this.subOrgEmail = subOrgEmail;
	}

	public String getSubOrgPhone() {
		return subOrgPhone;
	}

	public void setSubOrgPhone(String subOrgPhone) {
		this.subOrgPhone = subOrgPhone;
	}

	public String getSubOrgFkAdd() {
		return subOrgFkAdd;
	}

	public void setSubOrgFkAdd(String subOrgFkAdd) {
		this.subOrgFkAdd = subOrgFkAdd;
	}

	public String getSubOrgTty() {
		return subOrgTty;
	}

	public void setSubOrgTty(String subOrgTty) {
		this.subOrgTty = subOrgTty;
	}

	public String getSubOrgFax() {
		return subOrgFax;
	}

	public void setSubOrgFax(String subOrgFax) {
		this.subOrgFax = subOrgFax;
	}

	public String getSubOrgUrl() {
		return subOrgUrl;
	}

	public void setSubOrgUrl(String subOrgUrl) {
		this.subOrgUrl = subOrgUrl;
	}

	public String getSubOrgProgCode() {
		return subOrgProgCode;
	}

	public void setSubOrgProgCode(String subOrgProgCode) {
		this.subOrgProgCode = subOrgProgCode;
	}
	public String getActiveStatusValidFrom() {
		return activeStatusValidFrom;
	}

	public void setActiveStatusValidFrom(String activeStatusValidFrom) {
		this.activeStatusValidFrom = activeStatusValidFrom;
	}

	/*Added for download CVS Section : Yogendra Pratap Singh*/
	public void openCSVDoc(HttpServletResponse response , String zipFileName) {
		try{
			CtrpDraftJBHelper ctrpDraftJBHelper = new CtrpDraftJBHelper(this);
			ctrpDraftJBHelper.openCSVDoc(response , zipFileName);
		}catch(Exception e){
			Rlog.fatal("ctrp", "Exception in CtrpDraftJB.openCSVDoc: "+e);
		}
		
	}

	public void setCtrpDraftSectSponsorRespJB(CtrpDraftSectSponsRespJB ctrpDraftSectSponsorRespJB) {
		this.ctrpDraftSectSponsorRespJB = ctrpDraftSectSponsorRespJB;
	}

	public CtrpDraftSectSponsRespJB getCtrpDraftSectSponsorRespJB() {
		return ctrpDraftSectSponsorRespJB;
	}
	
	public Integer markCtrpDraftReadyForSubmission(HashMap<String, String> argMap) {
		Integer output = 0;
		CtrpDraftBean ctrpDraftBean = new CtrpDraftBean();
		Integer draftType = this.getDraftType();
		
		if (draftType != null){
			switch(draftType){
				case 0:
					fillCtrpDraftBeanNonIndustrial(ctrpDraftBean);
					break;
				case 1:
					fillCtrpDraftBeanIndustrial(ctrpDraftBean);
					break;
			}
		
			if (argMap.get(IP_ADD) != null) {
				ctrpDraftBean.setIpAdd(argMap.get(IP_ADD));
			}
			try {
				CtrpDraftAgent ctrpDraftAgent = this.getCtrpDraftAgent();
				if (id > 0) {
					fillCtrpDraftBeanLastModifiedBy(ctrpDraftBean, argMap);
					updateStatusHistoryDetails(argMap,NCI_TRAIL_STATUS);
					updateStatusHistoryDetails(argMap,CTRP_DRAFT_STATUS);
					output = 1;
				} else {
					fillCtrpDraftBeanCreator(ctrpDraftBean, argMap);
					output = ctrpDraftAgent.createCtrpDraft(ctrpDraftBean);
					id = output; // newly created ID
					createStatusHistoryDetails(argMap,NCI_TRAIL_STATUS);
					createStatusHistoryDetails(argMap,CTRP_DRAFT_STATUS);
					output = 1;
				}
			} catch(Exception e) {
				Rlog.fatal("ctrp", "Exception in CtrpDraftJB.markCtrpDraftReadyForSubmission: "+e);
				output = -1;
			}
		}
		return output;
	}
	public Integer checkCtrpDraftStatus() {
		Integer output = 1;
		try {
			StatusHistoryJB statusHistoryJB = new StatusHistoryJB();
			String statusSubType = statusHistoryJB.getLatestStatus(this.id, ER_CTRP_DRAFT);
			if (statusSubType != null){
				if (statusSubType.equals(STRING_READY) || statusSubType.equals(STRING_SUBMITTED)){
					output = 0;
				}
			}
		} catch(Exception e) {
			Rlog.fatal("ctrp", "Exception in CtrpDraftJB.markCtrpDraftReadyForSubmission: "+e);
			output = -1;
		}
		return output;
	}
	//Added for Bug#8464 | Date: 20 FEB 2012 |By :YPS
	public String findSelectedDtaftIds(String draftIds) {
		CtrpDraftJBHelper ctrpDraftJBHelper = new CtrpDraftJBHelper(this);
		return ctrpDraftJBHelper.findSelectedDtaftIds(draftIds);
	}

	public Integer getLoggedUser() {
		return loggedUser;
	}

	public void setLoggedUser(Integer loggedUser) {
		this.loggedUser = loggedUser;
	}
	
	// --- Dummy methods needed by OGNL
	public void setCtrpSubmType(String s) {}
	public void setCtrpDraftStatus(String s) {}
	public void setCtrpStudyPhase(String s) {}
	public void setLeadOrgType(String s) {}
	public void setTrialStatus(String s) {}
	public void setCtrpIntvnType(String s) {}
	public void setSiteRecruitType(String s) {}
	public void setSubOrgType(String s) {}
	// --- End of Dummy methods needed by OGNL

	public String getStatusCountVal() {
		return statusCountVal;
	}

	public void setStatusCountVal(String statusCountVal) {
		this.statusCountVal = statusCountVal;
	}


	public void setCurrntStudyStatus(String currntStudyStatus) {
		this.currntStudyStatus = currntStudyStatus;
	}

	public String getCurrntStudyStatus() {
		return currntStudyStatus;
	}

	public String getStudyStatusChckLst() {
		return studyStatusChckLst;
	}

	public void setStudyStatusChckLst(String studyStatusChckLst) {
		this.studyStatusChckLst = studyStatusChckLst;
	}

	public String getEmailUserId() {
		return EmailUserId;
	}

	public void setEmailUserId(String emailUserId) {
		EmailUserId = emailUserId;
	}


	public Integer getFkUser() {
		return fkUser;
	}

	public void setFkUser(Integer fkUser) {
		this.fkUser = fkUser;
	}
	/**
	 * @param protocolStatus the protocolStatus to set
	 */
	public void setProtocolStatus(String protocolStatus) {
		this.protocolStatus = protocolStatus;
	}

	/**
	 * @return the protocolStatus
	 */
	public String getProtocolStatus() {
		return protocolStatus;
	}

	public String getStudyStatusUserLst() {
		return studyStatusUserLst;
	}

	public void setStudyStatusUserLst(String studyStatusUserLst) {
		this.studyStatusUserLst = studyStatusUserLst;
	}

	public void setTrialStatus(Integer trialStatus) {
		this.trialStatus = trialStatus;
	}
	
	public List<String> processForZipFile(ArrayList<CtrpDraftCSVExportJB> exportList) {
		CTRPDraftDocJB ctrpDraftDocJB = new CTRPDraftDocJB();
		List<String> draftList = new ArrayList<String>();
		try{
			int i=1;
			for(CtrpDraftCSVExportJB ctrpDraftCSVExportJB:exportList ){
				List<AppendixBean> appenxBeanList = ctrpDraftCSVExportJB.getDocumentInfoList();
				if(i!=1){
					int draftId = StringUtil.stringToInteger(ctrpDraftCSVExportJB.getId());
					List<String> draftNameList = ctrpDraftDocJB.generateDraftTempDoc(appenxBeanList,draftId);
					for(String dfList:draftNameList){
						draftList.add(dfList);
					}
				}
				i=i+1;
			}
		}catch(Exception ex){ex.printStackTrace();}
		return draftList;
	}

	public String prepareZipFileFromTempDoc(List<String> draftNameList) {
		CTRPDraftDocJB ctrpDraftDocJB = new CTRPDraftDocJB();
		String zipFileName = ctrpDraftDocJB.prepareZipFileFromTempDoc(draftNameList);
		return zipFileName;
	}
	
	
	//22472
	public String getNciProcStatMenu() {
		return nciProcStatMenu;
	}
	public String getDraftActionsMenu() {
		return draftActionsMenu;
	}
	public String getDraftStatusesMenu() {
		return draftStatusesMenu;
	}

	public String getCtrpNciProcStatMenu() {
		return ctrpNciProcStatMenu;
	}

	public void setCtrpNciProcStatMenu(String ctrpNciProcStatMenu) {
		this.ctrpNciProcStatMenu = ctrpNciProcStatMenu;
	}
		
	public String getCtrpNciProcStatDt() {
		return ctrpNciProcStatDt;
	}

	public void setCtrpNciProcStatDt(String ctrpNciProcStatDt) {
		this.ctrpNciProcStatDt = ctrpNciProcStatDt;
	}
	public void setDraftNum(Integer draftNum){
		this.draftNum=draftNum;
	}
	
	public Integer getDraftNum(){
		return draftNum;
	}
	
		/**
	 * @param protocolStatus the protocolStatus to set
	 */
	public void setNciProcStatMenu(String nciProcStatMenu) {
		this.nciProcStatMenu = nciProcStatMenu;
	}
	
	/**
	 * @param protocolStatus the protocolStatus to set
	 */
	public void setDraftActionsMenu(String draftActionsMenu) {
		this.draftActionsMenu = draftActionsMenu;
	}
	
	/**
	 * @param protocolStatus the protocolStatus to set
	 */
	public void setDraftStatusesMenu(String draftStatusesMenu) {
		this.draftStatusesMenu = draftStatusesMenu;
	}
	
	public void copyCtrpDraft(Integer draftId) {
		CtrpDraftJBHelper helper = new CtrpDraftJBHelper(this);
		helper.retrieveCtrpDraft(draftId);
		helper.retreiveCtrpDocumentInfo(draftId);
		helper.retrieveStudy(StringUtil.stringToNum(this.getFkStudy()));
		this.setId(0);
		this.setTrialProtocolDocPK(0);
		this.setTrialIRBApprovalPK(0);
		this.setTrialPartSitesListPK(0);
		this.setTrialInformedConsentPK(0);
		this.setTrialChangeMemoDocPK(0);
		this.setTrialProtHighlightPK(0);
		this.setTrialOtherPK(0);
		this.setTrialAbbrTrialTempPK(0);
		this.setDraftNum(null);
	}
	
	public Integer getCtrpNciProcStatId() {
		return this.ctrpNciProcStatId;
	}

	public void setCtrpNciProcStatId(Integer ctrpNciProcStatId) {
		this.ctrpNciProcStatId = ctrpNciProcStatId;
		this.ctrpNciProcStatMenu = filterNCITrailStatusesForDraftDetails(nciDraftStatusDao, ctrpNciProcStatId);
	}

	
	private String filterNCITrailStatusesForDraftDetails(CodeDao codeDao, int selectedOptionValue) {
		ArrayList pkList = codeDao.getCId();
		ArrayList descList = codeDao.getCDesc();

		StringBuffer sb1 =  new StringBuffer();
		sb1.append("<select name='").append(FLD_CTRP_NCI_PROC_STATUS).append("' ").append(ON_CHANGE_CTRP_NCI_STAT);
		sb1.append(">");
		try {
			
			if (selectedOptionValue == 0){
				sb1.append("<OPTION value='' SELECTED>").append(LC.L_Select_AnOption).append("</OPTION>");
            } else {
            	sb1.append("<OPTION value='' >").append(LC.L_Select_AnOption).append("</OPTION>");
            }
			
			for (int iX=0; iX<pkList.size(); iX++) {
				sb1.append("<option value='").append(pkList.get(iX)).append("' ");
				if (selectedOptionValue == (Integer)pkList.get(iX)) { sb1.append("SELECTED"); }
				sb1.append(">").append(descList.get(iX)).append("</option>");
			}
			
		} catch(Exception e) {
			Rlog.fatal("ctrp", "Exception in CtrpDraftJB.filterNCITrailStatusesForDraftDetails: "+e);			
			e.printStackTrace();
		}
		sb1.append("</select>");
		return sb1.toString();
	}
	
	public Integer deleteCtrpDrftStats(String statIds, String draftId) {
		
		Integer output = 0;
		try {
				CtrpDraftAgent ctrpDraftAgent = this.getCtrpDraftAgent();
				output = ctrpDraftAgent.deleteCtrpDrftStats(statIds,draftId);
		}catch(Exception e){
			Rlog.fatal("ctrp", "Exception in CtrpDraftJB.deleteCtrpNontificationUsers: "+e);
			output = -1;
		}
		return output;
	}
	public HashMap<String, String> fetchCtrpDraftStatuses() {
		
		HashMap<String, String> statMap=new HashMap<String, String>();

		try {
			
			StatusHistoryJB statusHistoryJB = new StatusHistoryJB();
			StatusHistoryDao statusHistoryDao = statusHistoryJB.getStatusHistoryCTRP(this.getId(),ER_CTRP_DRAFT);
			Map<String, String> statHistoryMap  = statusHistoryDao.getStatHisotryMap();
			
			statMap.put("statIds",statHistoryMap.get("statIds"));
			statMap.put("statDate",statHistoryMap.get("statDate"));
			statMap.put("statDesc",statHistoryMap.get("statDesc"));
			statMap.put("statEnteredBy",statHistoryMap.get("statEnteredBy"));
			statMap.put("statNotes",statHistoryMap.get("statNotes"));
			statMap.put("isCurr",statHistoryMap.get("isCurr"));
			
			StatusHistoryDao nciStatusHistoryDao = statusHistoryJB.getStatusHistoryCTRP(this.getId(),ER_CTRP_DRAFT_NCI_STAT);
			Map<String, String> nciStatHistoryMap  = nciStatusHistoryDao.getStatHisotryMap();
			
			statMap.put("nciStatIds",nciStatHistoryMap.get("statIds"));
			statMap.put("nciStatDate",nciStatHistoryMap.get("statDate"));
			statMap.put("nciStatDesc",nciStatHistoryMap.get("statDesc"));
			statMap.put("nciStatEnteredBy",nciStatHistoryMap.get("statEnteredBy"));
			statMap.put("nciStatNotes",nciStatHistoryMap.get("statNotes"));
			statMap.put("nciIsCurr",nciStatHistoryMap.get("isCurr"));
			
		}catch(Exception e){
				Rlog.fatal("ctrp", "Exception in CtrpDraftJB.fetchCtrpDraftStatuses: "+e);
				statMap = null;
		}
		return statMap;
	}
	
	public Map<String, String> fetchStudyAutoComplete(String accId, String userId){
		
		try {
			CtrpDraftAgent ctrpDraftAgent = this.getCtrpDraftAgent();
			return ctrpDraftAgent.fetchStudyAutoComplete(accId,userId);
		}catch(Exception e){
			Rlog.fatal("ctrp", "Exception in CtrpDraftJB.fetchStudyAutoComplete: "+e);
			return null;
		}		
	}
	
	public void fetchNextDraftNum(CtrpDraftBean ctrpDraftBean){
		
		try {
			if(this.draftNum==null||this.draftNum==0){
				CtrpDraftAgent ctrpDraftAgent = this.getCtrpDraftAgent();
				ctrpDraftBean.setDraftNum(ctrpDraftAgent.fetchNextDraftNum(StringUtil.stringToInteger(this.fkStudy)));
			}
			
		}catch(Exception e){
			Rlog.fatal("ctrp", "Exception in CtrpDraftJB.fetchNextDraftNum: "+e);
		}		
	}
	
}