/**
 * Added by Parminder Singh for CTRP-22473
 * This class is used to persist and delete data for configured Notification data from table.
 * 
*/
package com.velos.eres.ctrp.business;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
@Entity
@Table(name="ER_STUDYSTAT_NOTIF")
@NamedQueries({
    @NamedQuery(name = "findCtrpDraftNotificationByUserId", 
    		query = " SELECT OBJECT(draft) FROM CtrpDraftNotificationBean draft "+
    		" where fk_codelst_studystat = :fk_codelst_studystat and fk_user = :fk_user"),
    @NamedQuery(name = "findCtrpDraftNotificationUsersByStatusId", 
    	    query = " SELECT OBJECT(draft) FROM CtrpDraftNotificationBean draft "+
    	    " where fk_codelst_studystat = :fk_codelst_studystat")
})
public class CtrpDraftNotificationBean implements Serializable {

	private static final long serialVersionUID = 1304975335133980132L;
	private Integer id;
	private Integer fkCodelstStudyStatus;
	private Integer fkUser;
	private Integer fkAccount;
	private Integer fkSiteId;
	private String ipAdd;
	private Integer creator;
	private Integer lastModifiedBy;
		
	public CtrpDraftNotificationBean() {
		// TODO Auto-generated constructor stub
	}
	
	@Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_STUDYSTAT_NOTIF", allocationSize=1)
    @Column(name = "PK_STUDYSTAT_NOTIF")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="FK_CODELST_STUDYSTAT")
	public Integer getFkCodelstStudyStatus() {
		return fkCodelstStudyStatus;
	}

	public void setFkCodelstStudyStatus(Integer fkCodelstStudyStatus) {
		this.fkCodelstStudyStatus = fkCodelstStudyStatus;
	}
	@Column(name="FK_USER")
	public Integer getFkUser() {
		return fkUser;
	}

	public void setFkUser(Integer fkUser) {
		this.fkUser = fkUser;
	}
	@Column(name="IP_ADD")
	public String getIpAdd() {
		return ipAdd;
	}

	public void setIpAdd(String ipAdd) {
		this.ipAdd = ipAdd;
	}
	@Column(name="CREATOR")
	public Integer getCreator() {
		return creator;
	}

	public void setCreator(Integer creator) {
		this.creator = creator;
	}
	@Column(name="LAST_MODIFIED_BY")
	public Integer getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(Integer lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	@Column(name="FK_ACCOUNT")
	public Integer getFkAccount() {
		return fkAccount;
	}

	public void setFkAccount(Integer fkAccount) {
		this.fkAccount = fkAccount;
	}
	@Column(name="FK_SITEID")
	public Integer getFkSiteId() {
		return fkSiteId;
	}

	public void setFkSiteId(Integer fkSiteId) {
		this.fkSiteId = fkSiteId;
	}

}
