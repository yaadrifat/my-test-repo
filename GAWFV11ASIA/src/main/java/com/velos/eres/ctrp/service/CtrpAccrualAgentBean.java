package com.velos.eres.ctrp.service;

import java.util.ArrayList;
import java.util.Map;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.eres.business.common.CtrpPatientAccrualDao;
import com.velos.eres.business.studySite.impl.StudySiteBean;
import com.velos.eres.ctrp.business.CtrpAccrualDao;
import com.velos.eres.service.util.Rlog;

@Stateless
@Remote( { CtrpAccrualAgent.class } )
public class CtrpAccrualAgentBean implements CtrpAccrualAgent{

	@PersistenceContext(unitName = "eres")
    protected EntityManager em;

	public Map<String, String> validateStudyAccru(Integer pkStudy,Integer studyType){
		try{
			CtrpAccrualDao ctrpAccrualDao = new CtrpAccrualDao();
			return ctrpAccrualDao.validateStudyAccru(pkStudy,studyType);
		}catch(Exception e){
			Rlog.fatal("CtrpAccrualAgentBean",
					"Exception In validateStudyAccru in CtrpAccrualAgentBean " + e);
			return null;
		}
	}
/**
 * @author Yogendra
 * @param Map<String, String> dataMapForPatient
 */
	public ArrayList<CtrpPatientAccrualDao> getPatientAccrualData(Map<String, String> dataMapForPatient) {
		try{
			CtrpAccrualDao ctrpAccrualDao = new CtrpAccrualDao();
			return ctrpAccrualDao.getPatientAccrualData(dataMapForPatient);
		}catch (Exception e) {
			Rlog.fatal("CtrpAccrualAgentBean",
					"Exception In getPatientAccrualData in CtrpAccrualAgentBean " + e);
		}
		return null;
	}
	
	public ArrayList<StudySiteBean> getSiteAccrualCount( Map<String, String> dataMapForPatient){
		try{
			CtrpAccrualDao ctrpAccrualDao = new CtrpAccrualDao();
			return ctrpAccrualDao.getSiteAccrualCount(dataMapForPatient);
		}catch (Exception e) {
			Rlog.fatal("CtrpAccrualAgentBean",
					"Exception In getPatientAccrualData in CtrpAccrualAgentBean " + e);
		}
		return null;
	}
}

