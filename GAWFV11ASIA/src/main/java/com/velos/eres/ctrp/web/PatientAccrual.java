package com.velos.eres.ctrp.web;

public class PatientAccrual {
	
	private String patientAccrual;
	private String studyIdentifier;
	private String studySubjectIdentifier;
	private String zipCode;
	private String countryCode;
	private String birthDate;
	private String gender;
	private String ethnicity;
	private String paymentMethod;
	private String subjectRegistrationDate;
	private String studySiteIdentifier;
	private String subjectDiseaseCode;
	private Integer dmgrphReportable;
	private Integer patEnrSiteOnStdTeam;
	
	private String blankField1;
	private String blankField2;
	private String blankField3;
	private String blankField4;
	private String blankField5;
	private String blankField6;
	private String blankField7;
	private String blankField8;
	private String blankField9;
	private String blankField10;
	private String blankField11;
	private String blankField12;
	/**
	 * @return the patientAccrual
	 */
	public String getPatientAccrual() {
		return patientAccrual;
	}
	/**
	 * @param patientAccrual the patientAccrual to set
	 */
	public void setPatientAccrual(String patientAccrual) {
		this.patientAccrual = patientAccrual;
	}
	/**
	 * @return the studyIdentifier
	 */
	public String getStudyIdentifier() {
		return studyIdentifier;
	}
	/**
	 * @param studyIdentifier the studyIdentifier to set
	 */
	public void setStudyIdentifier(String studyIdentifier) {
		this.studyIdentifier = studyIdentifier;
	}
	/**
	 * @return the studySubjectIdentifier
	 */
	public String getStudySubjectIdentifier() {
		return studySubjectIdentifier;
	}
	/**
	 * @param studySubjectIdentifier the studySubjectIdentifier to set
	 */
	public void setStudySubjectIdentifier(String studySubjectIdentifier) {
		this.studySubjectIdentifier = studySubjectIdentifier;
	}
	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}
	/**
	 * @param zipCode the zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	/**
	 * @return the countryCode
	 */
	public String getCountryCode() {
		return countryCode;
	}
	/**
	 * @param countryCode the countryCode to set
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	/**
	 * @return the birthDate
	 */
	public String getBirthDate() {
		return birthDate;
	}
	/**
	 * @param birthDate the birthDate to set
	 */
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}
	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}
	/**
	 * @return the ethnicity
	 */
	public String getEthnicity() {
		return ethnicity;
	}
	/**
	 * @param ethnicity the ethnicity to set
	 */
	public void setEthnicity(String ethnicity) {
		this.ethnicity = ethnicity;
	}
	/**
	 * @return the paymentMethod
	 */
	public String getPaymentMethod() {
		return paymentMethod;
	}
	/**
	 * @param paymentMethod the paymentMethod to set
	 */
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	/**
	 * @return the subjectRegistrationDate
	 */
	public String getSubjectRegistrationDate() {
		return subjectRegistrationDate;
	}
	/**
	 * @param subjectRegistrationDate the subjectRegistrationDate to set
	 */
	public void setSubjectRegistrationDate(String subjectRegistrationDate) {
		this.subjectRegistrationDate = subjectRegistrationDate;
	}
	/**
	 * @return the studySiteIdentifier
	 */
	public String getStudySiteIdentifier() {
		return studySiteIdentifier;
	}
	/**
	 * @param studySiteIdentifier the studySiteIdentifier to set
	 */
	public void setStudySiteIdentifier(String studySiteIdentifier) {
		this.studySiteIdentifier = studySiteIdentifier;
	}
	/**
	 * @return the subjectDiseaseCode
	 */
	public String getSubjectDiseaseCode() {
		return subjectDiseaseCode;
	}
	/**
	 * @param subjectDiseaseCode the subjectDiseaseCode to set
	 */
	public void setSubjectDiseaseCode(String subjectDiseaseCode) {
		this.subjectDiseaseCode = subjectDiseaseCode;
	}
	/**
	 * @return the blankField1
	 */
	public String getBlankField1() {
		return blankField1;
	}
	/**
	 * @param blankField1 the blankField1 to set
	 */
	public void setBlankField1(String blankField1) {
		this.blankField1 = blankField1;
	}
	/**
	 * @return the blankField2
	 */
	public String getBlankField2() {
		return blankField2;
	}
	/**
	 * @param blankField2 the blankField2 to set
	 */
	public void setBlankField2(String blankField2) {
		this.blankField2 = blankField2;
	}
	/**
	 * @return the blankField3
	 */
	public String getBlankField3() {
		return blankField3;
	}
	/**
	 * @param blankField3 the blankField3 to set
	 */
	public void setBlankField3(String blankField3) {
		this.blankField3 = blankField3;
	}
	/**
	 * @return the blankField4
	 */
	public String getBlankField4() {
		return blankField4;
	}
	/**
	 * @param blankField4 the blankField4 to set
	 */
	public void setBlankField4(String blankField4) {
		this.blankField4 = blankField4;
	}
	/**
	 * @return the blankField5
	 */
	public String getBlankField5() {
		return blankField5;
	}
	/**
	 * @param blankField5 the blankField5 to set
	 */
	public void setBlankField5(String blankField5) {
		this.blankField5 = blankField5;
	}
	/**
	 * @return the blankField6
	 */
	public String getBlankField6() {
		return blankField6;
	}
	/**
	 * @param blankField6 the blankField6 to set
	 */
	public void setBlankField6(String blankField6) {
		this.blankField6 = blankField6;
	}
	/**
	 * @return the blankField7
	 */
	public String getBlankField7() {
		return blankField7;
	}
	/**
	 * @param blankField7 the blankField7 to set
	 */
	public void setBlankField7(String blankField7) {
		this.blankField7 = blankField7;
	}
	/**
	 * @return the blankField8
	 */
	public String getBlankField8() {
		return blankField8;
	}
	/**
	 * @param blankField8 the blankField8 to set
	 */
	public void setBlankField8(String blankField8) {
		this.blankField8 = blankField8;
	}
	/**
	 * @return the blankField9
	 */
	public String getBlankField9() {
		return blankField9;
	}
	/**
	 * @param blankField9 the blankField9 to set
	 */
	public void setBlankField9(String blankField9) {
		this.blankField9 = blankField9;
	}
	/**
	 * @return the blankField10
	 */
	public String getBlankField10() {
		return blankField10;
	}
	/**
	 * @param blankField10 the blankField10 to set
	 */
	public void setBlankField10(String blankField10) {
		this.blankField10 = blankField10;
	}
	/**
	 * @return the blankField11
	 */
	public String getBlankField11() {
		return blankField11;
	}
	/**
	 * @param blankField11 the blankField11 to set
	 */
	public void setBlankField11(String blankField11) {
		this.blankField11 = blankField11;
	}
	/**
	 * @return the dmgrphReportable
	 */
	public Integer getDmgrphReportable() {
		return dmgrphReportable;
	}
	/**
	 * @param dmgrphReportable the dmgrphReportable to set
	 */
	public void setDmgrphReportable(Integer dmgrphReportable) {
		this.dmgrphReportable = dmgrphReportable;
	}
	/**
	 * @return the blankField12
	 */
	public String getBlankField12() {
		return blankField12;
	}
	/**
	 * @param blankField12 the blankField12 to set
	 */
	public void setBlankField12(String blankField12) {
		this.blankField12 = blankField12;
	}
	public void setPatEnrSiteOnStdTeam(Integer patEnrSiteOnStdTeam) {
		this.patEnrSiteOnStdTeam = patEnrSiteOnStdTeam;
	}
	public Integer getPatEnrSiteOnStdTeam() {
		return patEnrSiteOnStdTeam;
	}
	
	
	

}
