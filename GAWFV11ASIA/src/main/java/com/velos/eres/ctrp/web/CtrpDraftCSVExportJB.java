package com.velos.eres.ctrp.web;

import java.util.List;

import com.velos.eres.business.appendix.impl.AppendixBean;
import com.velos.eres.web.address.AddressJB;
import com.velos.eres.web.site.SiteJB;
import com.velos.eres.web.study.StudyJB;
import com.velos.eres.web.studyIDEIND.StudyINDIDEJB;
import com.velos.eres.web.studyNIHGrant.StudyNIHGrantJB;
import com.velos.eres.web.user.UserJB;

public class CtrpDraftCSVExportJB {
	
	private String id;
    private String fkStudy;
	private String ctrpDraftType;
    private String fkCodelstSubmissionType;
    private String amendNumber;
    private String amendDate;
    private String draftXMLReqd;
    private String fkTrialOwner;
    private String nciStudyId;
    private String leadOrgStudyId;
    private String submitOrgStudyId;
    private String nctNumber;
    private String otherNumber;
    private String fkCodelstPhase;
    private String draftPilot;
    private String studyType;
    private String fkCodelstInterventionType;
    private String interventionName;
    private String fkCodelstDiseaseSite;
    private String studyPurpose;
    private String studyPurposeOther;
    private String leadOrgCtrpId;
    private String fkSiteLeadOrg;
    private String fkAddLeadOrg;
    private String leadOrgType;
    private String submitOrgCtrpId;
    private String submitOrgName;
    private String fkAddSubmitOrg;
    private String submitOrgType;
    private String submitNCIDesignated;
    private String piCtrpId;
    private String fkUserPi;
    private String piFirstName;
    private String piMiddleName;
    private String piLastName;
    private String fkAddPi;
    private String sponsorCtrpId;
    private String sponsorName;
    private String fkAddSponsor;
    private String responsibleParty;
    private String rpSponsorContactType;
    private String rpSponsorContactId;
    private String sponsorPersonalFirstName;
    private String sponsorPersonalMiddleName;
    private String sponsorPersonalLastName;
    private String fkAddSponsorPersonal;
    private String sponsorGenericTitle;
    private String rpEmail;
    private String rpPhone;
    private String rpExtn;
    private String summ4SponsorType;
    private String summ4SponsorId;
    private String summ4SponsorName;
    private String fkAddSumm4Sponsor;
    private String sponsorProgramCode;
    private String fkCodelstCtrpStudyStatus;
    private String ctrpStudyStatusDate;
    private String ctrpStudyStopReason;
    private String ctrpStudyStartDate;
    private String isStartActual;
    private String ctrpStudyCompDate;
    private String isCompActual;
    private String ctrpStudyAccOpenDate;
    private String ctrpStudyAccClosedDate;
    private String siteTargetAccrual;
    private String oversightCountry;
    private String oversightOrganization;
    private String fdaIntvenIndicator;
    private String section801Indicator;
    private String delayPostIndicator;
    private String dmAppointIndicator;
    private String deletedFlag;
    private String ipAdd;
    private String creator;
    private String lastModifiedBy;
    
    private String leadOrgName;//LEAD_ORG_NAME
    
    //Extra Added For Data
    private String triailDetailsTitle;
    private String nctTrailIdentifier;//NCT Trial Identifier
    private String studyPurposeAdditionalQualifier;

    //For Address Table
    private AddressJB sponsorAddressJB = new AddressJB();
    private AddressJB personalSponsorAddressJB = new AddressJB();
    private AddressJB leadOrganizationAddressJB = new AddressJB();
    private AddressJB principalInvestgatorAddressJB = new AddressJB();
    private AddressJB summ4SponsorAddressJB = new AddressJB();
    //for Industrial
    private AddressJB submittingOrganizationAddressJB = new AddressJB();
    private AddressJB sitePrincipalInvestgatorAddressJB = new AddressJB();
    private AddressJB trailAddressJB = new AddressJB();
    
    private StudyJB studyJB  = new StudyJB();
    //For Organization Name
    private SiteJB orgazationSiteName = new SiteJB();
    
    private UserJB principalInvestgatorUserJB = new UserJB();
    
    //For Industrail
    private UserJB trailUserJB = new UserJB();
    
    
    private StudyNIHGrantJB studyNIHGrantJB = new StudyNIHGrantJB();
    
    private StudyINDIDEJB studyINDIDEJB = new StudyINDIDEJB();
    
    
    //Fields for CTRP DOC
   private String protocolDocument;
   private String irbApprovalDocument;
   private String participatingSitesDocument;
   private String informedConsentDocument;
   private String otherTrialRelatedDocument;
   private String ChangeMemoDocument;
   private String ProtocolHighlightDocument;
   
   private String submitOrgType1;
	
	 
   //Modified for Bug# 8478 
   private String responsiblePartyContact; 
   private String responsiblePartyEmailAddress;
   private String responsiblePartyPhone;
   private String responsiblePartyExt;
   //Fields for CTRP Industrail Doc
   private String abbreviatedTrialTemplate;
   
   private List<AppendixBean> documentInfoList;
   private List<Integer> documentInfoIdList;
	
	
	//Ctrp Draft Primary Key
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFkStudy() {
		return fkStudy;
	}
	public void setFkStudy(String fkStudy) {
		this.fkStudy = fkStudy;
	}
	public String getCtrpDraftType() {
		return ctrpDraftType;
	}
	public void setCtrpDraftType(String ctrpDraftType) {
		this.ctrpDraftType = ctrpDraftType;
	}
	public String getFkCodelstSubmissionType() {
		return fkCodelstSubmissionType;
	}
	public void setFkCodelstSubmissionType(String fkCodelstSubmissionType) {
		this.fkCodelstSubmissionType = fkCodelstSubmissionType;
	}
	public String getAmendNumber() {
		return amendNumber;
	}
	public void setAmendNumber(String amendNumber) {
		this.amendNumber = amendNumber;
	}
	public String getAmendDate() {
		return amendDate;
	}
	public void setAmendDate(String amendDate) {
		this.amendDate = amendDate;
	}
	public String getDraftXMLReqd() {
		return draftXMLReqd;
	}
	public void setDraftXMLReqd(String draftXMLReqd) {
		this.draftXMLReqd = draftXMLReqd;
	}
	public String getFkTrialOwner() {
		return fkTrialOwner;
	}
	public void setFkTrialOwner(String fkTrialOwner) {
		this.fkTrialOwner = fkTrialOwner;
	}
	public String getNciStudyId() {
		return nciStudyId;
	}
	public void setNciStudyId(String nciStudyId) {
		this.nciStudyId = nciStudyId;
	}
	public String getLeadOrgStudyId() {
		return leadOrgStudyId;
	}
	public void setLeadOrgStudyId(String leadOrgStudyId) {
		this.leadOrgStudyId = leadOrgStudyId;
	}
	public String getSubmitOrgStudyId() {
		return submitOrgStudyId;
	}
	public void setSubmitOrgStudyId(String submitOrgStudyId) {
		this.submitOrgStudyId = submitOrgStudyId;
	}
	public String getNctNumber() {
		return nctNumber;
	}
	public void setNctNumber(String nctNumber) {
		this.nctNumber = nctNumber;
	}
	public String getOtherNumber() {
		return otherNumber;
	}
	public void setOtherNumber(String otherNumber) {
		this.otherNumber = otherNumber;
	}
	public String getFkCodelstPhase() {
		return fkCodelstPhase;
	}
	public void setFkCodelstPhase(String fkCodelstPhase) {
		this.fkCodelstPhase = fkCodelstPhase;
	}
	public String getDraftPilot() {
		return draftPilot;
	}
	public void setDraftPilot(String draftPilot) {
		this.draftPilot = draftPilot;
	}
	public String getStudyType() {
		return studyType;
	}
	public void setStudyType(String studyType) {
		this.studyType = studyType;
	}
	public String getFkCodelstInterventionType() {
		return fkCodelstInterventionType;
	}
	public void setFkCodelstInterventionType(String fkCodelstInterventionType) {
		this.fkCodelstInterventionType = fkCodelstInterventionType;
	}
	public String getInterventionName() {
		return interventionName;
	}
	public void setInterventionName(String interventionName) {
		this.interventionName = interventionName;
	}
	public String getFkCodelstDiseaseSite() {
		return fkCodelstDiseaseSite;
	}
	public void setFkCodelstDiseaseSite(String fkCodelstDiseaseSite) {
		this.fkCodelstDiseaseSite = fkCodelstDiseaseSite;
	}
	public String getStudyPurpose() {
		return studyPurpose;
	}
	public void setStudyPurpose(String studyPurpose) {
		this.studyPurpose = studyPurpose;
	}
	public String getStudyPurposeOther() {
		return studyPurposeOther;
	}
	public void setStudyPurposeOther(String studyPurposeOther) {
		this.studyPurposeOther = studyPurposeOther;
	}
	public String getLeadOrgCtrpId() {
		return leadOrgCtrpId;
	}
	public void setLeadOrgCtrpId(String leadOrgCtrpId) {
		this.leadOrgCtrpId = leadOrgCtrpId;
	}
	public String getFkSiteLeadOrg() {
		return fkSiteLeadOrg;
	}
	public void setFkSiteLeadOrg(String fkSiteLeadOrg) {
		this.fkSiteLeadOrg = fkSiteLeadOrg;
	}
	public String getFkAddLeadOrg() {
		return fkAddLeadOrg;
	}
	public void setFkAddLeadOrg(String fkAddLeadOrg) {
		this.fkAddLeadOrg = fkAddLeadOrg;
	}
	public String getLeadOrgType() {
		return leadOrgType;
	}
	public void setLeadOrgType(String leadOrgType) {
		this.leadOrgType = leadOrgType;
	}
	public String getSubmitOrgCtrpId() {
		return submitOrgCtrpId;
	}
	public void setSubmitOrgCtrpId(String submitOrgCtrpId) {
		this.submitOrgCtrpId = submitOrgCtrpId;
	}
	public String getSubmitOrgName() {
		return submitOrgName;
	}
	public void setSubmitOrgName(String submitOrgName) {
		this.submitOrgName = submitOrgName;
	}
	public String getFkAddSubmitOrg() {
		return fkAddSubmitOrg;
	}
	public void setFkAddSubmitOrg(String fkAddSubmitOrg) {
		this.fkAddSubmitOrg = fkAddSubmitOrg;
	}
	public String getSubmitOrgType() {
		return submitOrgType;
	}
	public void setSubmitOrgType(String submitOrgType) {
		this.submitOrgType = submitOrgType;
	}
	public String getSubmitNCIDesignated() {
		return submitNCIDesignated;
	}
	public void setSubmitNCIDesignated(String submitNCIDesignated) {
		this.submitNCIDesignated = submitNCIDesignated;
	}
	public String getPiCtrpId() {
		return piCtrpId;
	}
	public void setPiCtrpId(String piCtrpId) {
		this.piCtrpId = piCtrpId;
	}
	public String getFkUserPi() {
		return fkUserPi;
	}
	public void setFkUserPi(String fkUserPi) {
		this.fkUserPi = fkUserPi;
	}
	public String getPiFirstName() {
		return piFirstName;
	}
	public void setPiFirstName(String piFirstName) {
		this.piFirstName = piFirstName;
	}
	public String getPiLastName() {
		return piLastName;
	}
	public void setPiLastName(String piLastName) {
		this.piLastName = piLastName;
	}
	public String getFkAddPi() {
		return fkAddPi;
	}
	public void setFkAddPi(String fkAddPi) {
		this.fkAddPi = fkAddPi;
	}
	public String getSponsorCtrpId() {
		return sponsorCtrpId;
	}
	public void setSponsorCtrpId(String sponsorCtrpId) {
		this.sponsorCtrpId = sponsorCtrpId;
	}
	public String getSponsorName() {
		return sponsorName;
	}
	public void setSponsorName(String sponsorName) {
		this.sponsorName = sponsorName;
	}
	public String getFkAddSponsor() {
		return fkAddSponsor;
	}
	public void setFkAddSponsor(String fkAddSponsor) {
		this.fkAddSponsor = fkAddSponsor;
	}
	public String getResponsibleParty() {
		return responsibleParty;
	}
	public void setResponsibleParty(String responsibleParty) {
		this.responsibleParty = responsibleParty;
	}
	public String getRpSponsorContactType() {
		return rpSponsorContactType;
	}
	public void setRpSponsorContactType(String rpSponsorContactType) {
		this.rpSponsorContactType = rpSponsorContactType;
	}
	public String getRpSponsorContactId() {
		return rpSponsorContactId;
	}
	public void setRpSponsorContactId(String rpSponsorContactId) {
		this.rpSponsorContactId = rpSponsorContactId;
	}
	public String getSponsorPersonalFirstName() {
		return sponsorPersonalFirstName;
	}
	public void setSponsorPersonalFirstName(String sponsorPersonalFirstName) {
		this.sponsorPersonalFirstName = sponsorPersonalFirstName;
	}
	public String getSponsorPersonalMiddleName() {
		return sponsorPersonalMiddleName;
	}
	public void setSponsorPersonalMiddleName(String sponsorPersonalMiddleName) {
		this.sponsorPersonalMiddleName = sponsorPersonalMiddleName;
	}
	public String getSponsorPersonalLastName() {
		return sponsorPersonalLastName;
	}
	public void setSponsorPersonalLastName(String sponsorPersonalLastName) {
		this.sponsorPersonalLastName = sponsorPersonalLastName;
	}
	public String getFkAddSponsorPersonal() {
		return fkAddSponsorPersonal;
	}
	public void setFkAddSponsorPersonal(String fkAddSponsorPersonal) {
		this.fkAddSponsorPersonal = fkAddSponsorPersonal;
	}
	public String getSponsorGenericTitle() {
		return sponsorGenericTitle;
	}
	public void setSponsorGenericTitle(String sponsorGenericTitle) {
		this.sponsorGenericTitle = sponsorGenericTitle;
	}
	public String getRpEmail() {
		return rpEmail;
	}
	public void setRpEmail(String rpEmail) {
		this.rpEmail = rpEmail;
	}
	public String getRpPhone() {
		return rpPhone;
	}
	public void setRpPhone(String rpPhone) {
		this.rpPhone = rpPhone;
	}
	public String getRpExtn() {
		return rpExtn;
	}
	public void setRpExtn(String rpExtn) {
		this.rpExtn = rpExtn;
	}
	public String getSumm4SponsorType() {
		return summ4SponsorType;
	}
	public void setSumm4SponsorType(String summ4SponsorType) {
		this.summ4SponsorType = summ4SponsorType;
	}
	public String getSumm4SponsorId() {
		return summ4SponsorId;
	}
	public void setSumm4SponsorId(String summ4SponsorId) {
		this.summ4SponsorId = summ4SponsorId;
	}
	public String getSumm4SponsorName() {
		return summ4SponsorName;
	}
	public void setSumm4SponsorName(String summ4SponsorName) {
		this.summ4SponsorName = summ4SponsorName;
	}
	public String getFkAddSumm4Sponsor() {
		return fkAddSumm4Sponsor;
	}
	public void setFkAddSumm4Sponsor(String fkAddSumm4Sponsor) {
		this.fkAddSumm4Sponsor = fkAddSumm4Sponsor;
	}
	public String getSponsorProgramCode() {
		return sponsorProgramCode;
	}
	public void setSponsorProgramCode(String sponsorProgramCode) {
		this.sponsorProgramCode = sponsorProgramCode;
	}
	public String getFkCodelstCtrpStudyStatus() {
		return fkCodelstCtrpStudyStatus;
	}
	public void setFkCodelstCtrpStudyStatus(String fkCodelstCtrpStudyStatus) {
		this.fkCodelstCtrpStudyStatus = fkCodelstCtrpStudyStatus;
	}
	public String getCtrpStudyStatusDate() {
		return ctrpStudyStatusDate;
	}
	public void setCtrpStudyStatusDate(String ctrpStudyStatusDate) {
		this.ctrpStudyStatusDate = ctrpStudyStatusDate;
	}
	public String getCtrpStudyStopReason() {
		return ctrpStudyStopReason;
	}
	public void setCtrpStudyStopReason(String ctrpStudyStopReason) {
		this.ctrpStudyStopReason = ctrpStudyStopReason;
	}
	public String getCtrpStudyStartDate() {
		return ctrpStudyStartDate;
	}
	public void setCtrpStudyStartDate(String ctrpStudyStartDate) {
		this.ctrpStudyStartDate = ctrpStudyStartDate;
	}
	public String getIsStartActual() {
		return isStartActual;
	}
	public void setIsStartActual(String isStartActual) {
		this.isStartActual = isStartActual;
	}
	public String getCtrpStudyCompDate() {
		return ctrpStudyCompDate;
	}
	public void setCtrpStudyCompDate(String ctrpStudyCompDate) {
		this.ctrpStudyCompDate = ctrpStudyCompDate;
	}
	public String getIsCompActual() {
		return isCompActual;
	}
	public void setIsCompActual(String isCompActual) {
		this.isCompActual = isCompActual;
	}
	public String getCtrpStudyAccOpenDate() {
		return ctrpStudyAccOpenDate;
	}
	public void setCtrpStudyAccOpenDate(String ctrpStudyAccOpenDate) {
		this.ctrpStudyAccOpenDate = ctrpStudyAccOpenDate;
	}
	public String getCtrpStudyAccClosedDate() {
		return ctrpStudyAccClosedDate;
	}
	public void setCtrpStudyAccClosedDate(String ctrpStudyAccClosedDate) {
		this.ctrpStudyAccClosedDate = ctrpStudyAccClosedDate;
	}
	public String getSiteTargetAccrual() {
		return siteTargetAccrual;
	}
	public void setSiteTargetAccrual(String siteTargetAccrual) {
		this.siteTargetAccrual = siteTargetAccrual;
	}
	public String getOversightCountry() {
		return oversightCountry;
	}
	public void setOversightCountry(String oversightCountry) {
		this.oversightCountry = oversightCountry;
	}
	public String getOversightOrganization() {
		return oversightOrganization;
	}
	public void setOversightOrganization(String oversightOrganization) {
		this.oversightOrganization = oversightOrganization;
	}
	public String getFdaIntvenIndicator() {
		return fdaIntvenIndicator;
	}
	public void setFdaIntvenIndicator(String fdaIntvenIndicator) {
		this.fdaIntvenIndicator = fdaIntvenIndicator;
	}
	public String getSection801Indicator() {
		return section801Indicator;
	}
	public void setSection801Indicator(String section801Indicator) {
		this.section801Indicator = section801Indicator;
	}
	public String getDelayPostIndicator() {
		return delayPostIndicator;
	}
	public void setDelayPostIndicator(String delayPostIndicator) {
		this.delayPostIndicator = delayPostIndicator;
	}
	public String getDmAppointIndicator() {
		return dmAppointIndicator;
	}
	public void setDmAppointIndicator(String dmAppointIndicator) {
		this.dmAppointIndicator = dmAppointIndicator;
	}
	public String getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(String deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	public String getIpAdd() {
		return ipAdd;
	}
	public void setIpAdd(String ipAdd) {
		this.ipAdd = ipAdd;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	public String getTriailDetailsTitle() {
		return triailDetailsTitle;
	}
	public void setTriailDetailsTitle(String triailDetailsTitle) {
		this.triailDetailsTitle = triailDetailsTitle;
	}
	public AddressJB getSponsorAddressJB() {
		return sponsorAddressJB;
	}
	public void setSponsorAddressJB(AddressJB sponsorAddressJB) {
		this.sponsorAddressJB = sponsorAddressJB;
	}
	public AddressJB getPersonalSponsorAddressJB() {
		return personalSponsorAddressJB;
	}
	public void setPersonalSponsorAddressJB(AddressJB personalSponsorAddressJB) {
		this.personalSponsorAddressJB = personalSponsorAddressJB;
	}
	public SiteJB getOrgazationSiteName() {
		return orgazationSiteName;
	}
	public void setOrgazationSiteName(SiteJB orgazationSiteName) {
		this.orgazationSiteName = orgazationSiteName;
	}
	public AddressJB getLeadOrganizationAddressJB() {
		return leadOrganizationAddressJB;
	}
	public void setLeadOrganizationAddressJB(AddressJB leadOrganizationAddressJB) {
		this.leadOrganizationAddressJB = leadOrganizationAddressJB;
	}
	public String getPiMiddleName() {
		return piMiddleName;
	}
	public void setPiMiddleName(String piMiddleName) {
		this.piMiddleName = piMiddleName;
	}
	public UserJB getPrincipalInvestgatorUserJB() {
		return principalInvestgatorUserJB;
	}
	public void setPrincipalInvestgatorUserJB(UserJB principalInvestgatorUserJB) {
		this.principalInvestgatorUserJB = principalInvestgatorUserJB;
	}
	public AddressJB getPrincipalInvestgatorAddressJB() {
		return principalInvestgatorAddressJB;
	}
	public void setPrincipalInvestgatorAddressJB(
			AddressJB principalInvestgatorAddressJB) {
		this.principalInvestgatorAddressJB = principalInvestgatorAddressJB;
	}
	public AddressJB getSumm4SponsorAddressJB() {
		return summ4SponsorAddressJB;
	}
	public void setSumm4SponsorAddressJB(AddressJB summ4SponsorAddressJB) {
		this.summ4SponsorAddressJB = summ4SponsorAddressJB;
	}
	public void setStudyNIHGrantJB(StudyNIHGrantJB studyNIHGrantJB) {
		this.studyNIHGrantJB = studyNIHGrantJB;
	}
	public StudyNIHGrantJB getStudyNIHGrantJB() {
		return studyNIHGrantJB;
	}
	public StudyINDIDEJB getStudyINDIDEJB() {
		return studyINDIDEJB;
	}
	public void setStudyINDIDEJB(StudyINDIDEJB studyINDIDEJB) {
		this.studyINDIDEJB = studyINDIDEJB;
	}
	public String getProtocolDocument() {
		return protocolDocument;
	}
	public void setProtocolDocument(String protocolDocument) {
		this.protocolDocument = protocolDocument;
	}
	public String getIrbApprovalDocument() {
		return irbApprovalDocument;
	}
	public void setIrbApprovalDocument(String irbApprovalDocument) {
		this.irbApprovalDocument = irbApprovalDocument;
	}
	public String getParticipatingSitesDocument() {
		return participatingSitesDocument;
	}
	public void setParticipatingSitesDocument(String participatingSitesDocument) {
		this.participatingSitesDocument = participatingSitesDocument;
	}
	public String getInformedConsentDocument() {
		return informedConsentDocument;
	}
	public void setInformedConsentDocument(String informedConsentDocument) {
		this.informedConsentDocument = informedConsentDocument;
	}
	public String getOtherTrialRelatedDocument() {
		return otherTrialRelatedDocument;
	}
	public void setOtherTrialRelatedDocument(String otherTrialRelatedDocument) {
		this.otherTrialRelatedDocument = otherTrialRelatedDocument;
	}
	public String getChangeMemoDocument() {
		return ChangeMemoDocument;
	}
	public void setChangeMemoDocument(String changeMemoDocument) {
		ChangeMemoDocument = changeMemoDocument;
	}
	public String getProtocolHighlightDocument() {
		return ProtocolHighlightDocument;
	}
	public void setProtocolHighlightDocument(String protocolHighlightDocument) {
		ProtocolHighlightDocument = protocolHighlightDocument;
	}
	public AddressJB getSubmittingOrganizationAddressJB() {
		return submittingOrganizationAddressJB;
	}
	public void setSubmittingOrganizationAddressJB(
			AddressJB submittingOrganizationAddressJB) {
		this.submittingOrganizationAddressJB = submittingOrganizationAddressJB;
	}
	public AddressJB getSitePrincipalInvestgatorAddressJB() {
		return sitePrincipalInvestgatorAddressJB;
	}
	public void setSitePrincipalInvestgatorAddressJB(
			AddressJB sitePrincipalInvestgatorAddressJB) {
		this.sitePrincipalInvestgatorAddressJB = sitePrincipalInvestgatorAddressJB;
	}
	public UserJB getTrailUserJB() {
		return trailUserJB;
	}
	public void setTrailUserJB(UserJB trailUserJB) {
		this.trailUserJB = trailUserJB;
	}
	public String getNctTrailIdentifier() {
		return nctTrailIdentifier;
	}
	public void setNctTrailIdentifier(String nctTrailIdentifier) {
		this.nctTrailIdentifier = nctTrailIdentifier;
	}
	public String getLeadOrgName() {
		return leadOrgName;
	}
	public void setLeadOrgName(String leadOrgName) {
		this.leadOrgName = leadOrgName;
	}
	public StudyJB getStudyJB() {
		return studyJB;
	}
	public void setStudyJB(StudyJB studyJB) {
		this.studyJB = studyJB;
	}
	public String getStudyPurposeAdditionalQualifier() {
		return studyPurposeAdditionalQualifier;
	}
	public void setStudyPurposeAdditionalQualifier(
			String studyPurposeAdditionalQualifier) {
		this.studyPurposeAdditionalQualifier = studyPurposeAdditionalQualifier;
	}
	public String getSubmitOrgType1() {
		return submitOrgType1;
	}
	public void setSubmitOrgType1(String submitOrgType1) {
		this.submitOrgType1 = submitOrgType1;
	}
	public AddressJB getTrailAddressJB() {
		return trailAddressJB;
	}
	public void setTrailAddressJB(AddressJB trailAddressJB) {
		this.trailAddressJB = trailAddressJB;
	}
	public String getAbbreviatedTrialTemplate() {
		return abbreviatedTrialTemplate;
	}
	public void setAbbreviatedTrialTemplate(String abbreviatedTrialTemplate) {
		this.abbreviatedTrialTemplate = abbreviatedTrialTemplate;
	}
	public String getResponsiblePartyContact() {
		return responsiblePartyContact;
	}
	public void setResponsiblePartyContact(String responsiblePartyContact) {
		this.responsiblePartyContact = responsiblePartyContact;
	}
	public String getResponsiblePartyEmailAddress() {
		return responsiblePartyEmailAddress;
	}
	public void setResponsiblePartyEmailAddress(String responsiblePartyEmailAddress) {
		this.responsiblePartyEmailAddress = responsiblePartyEmailAddress;
	}
	public String getResponsiblePartyPhone() {
		return responsiblePartyPhone;
	}
	public void setResponsiblePartyPhone(String responsiblePartyPhone) {
		this.responsiblePartyPhone = responsiblePartyPhone;
	}
	public String getResponsiblePartyExt() {
		return responsiblePartyExt;
	}
	public void setResponsiblePartyExt(String responsiblePartyExt) {
		this.responsiblePartyExt = responsiblePartyExt;
	}
	/**
	 * @return the documentInfoList
	 */
	public List<AppendixBean> getDocumentInfoList() {
		return documentInfoList;
	}
	/**
	 * @param documentInfoList the documentInfoList to set
	 */
	public void setDocumentInfoList(List<AppendixBean> documentInfoList) {
		this.documentInfoList = documentInfoList;
	}
	
	/**
	 * @return the documentInfoIdList
	 */
	public List<Integer> getDocumentInfoIdList() {
		return documentInfoIdList;
	}
	/**
	 * @param documentInfoIdList the documentInfoIdList to set
	 */
	public void setDocumentInfoIdList(List<Integer> documentInfoIdList) {
		this.documentInfoIdList = documentInfoIdList;
	}

}
