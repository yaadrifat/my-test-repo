package com.velos.eres.ctrp.business;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.business.common.CtrpPatientAccrualDao;
import com.velos.eres.business.common.StudySiteDao;
import com.velos.eres.business.studySite.impl.StudySiteBean;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

public class CtrpAccrualDao extends CommonDAO implements Serializable {

/**
	 * 
	 */
	private static final long serialVersionUID = 887353656345106677L;
	
	public Map<String, String> validateStudyAccru(Integer pk_study,
			Integer studyType) {
		Map<String, String> data = new HashMap<String, String>();
		data.put("the_key", "the_value");
		return data;
	}
	
	/**
	 * @param dataMapForPatient
	 * @return
	 */
	public ArrayList<CtrpPatientAccrualDao> getPatientAccrualData(Map<String, String> dataMapForPatient) {
		
		int userId = StringUtil.stringToNum( dataMapForPatient.get("userId") );  
		String userGroupId = dataMapForPatient.get("userGroupId");
		String siteId  = dataMapForPatient.get("siteId");
		int accountId = StringUtil.stringToNum(dataMapForPatient.get("accountId"));
		String cutOffDate  = dataMapForPatient.get("cutOffDate");
		int studyId = StringUtil.stringToNum(dataMapForPatient.get("studyId"));
		//Start
		StudySiteDao ssDao2 = new StudySiteDao();
		com.velos.eres.web.studySite.StudySiteJB studySiteB= new com.velos.eres.web.studySite.StudySiteJB(); 
		ssDao2	= studySiteB.getSitesForStatAndEnrolledPat(studyId, userId,accountId);
		ArrayList arrStudySites  = ssDao2.getSiteIds();
			
		String siteIds = arrStudySites.toString();
		siteIds = siteIds.substring(1, siteIds.length()-1);
		siteIds = siteIds.replaceAll(" ", "");
		
		//End
		ArrayList<CtrpPatientAccrualDao> patAccList = new ArrayList<CtrpPatientAccrualDao>();
		
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT DISTINCT p.rowid AS rowcount, erv.fk_per, p.person_lname AS mask_person_lname," );
		sb.append(" p.person_fname AS mask_person_fname, (select NVL(NCI_POID,' ') from er_site where PK_SITE = per_site) siteId, ");
		sb.append(" estd.NCI_TRIAL_IDENTIFIER NCI_TRIAL_IDEN , ");
		sb.append(" ( SELECT ptprot.patprot_disease_code FROM er_patprot ptprot WHERE ptprot.FK_STUDY=erv.fk_study AND ptprot.fk_per=erv.fk_per AND ptprot.PATPROT_STAT=1 ) AS DISEASE_CODE, ");
		sb.append(" ( SELECT ptprot.DMGRPH_REPORTABLE FROM er_patprot ptprot WHERE ptprot.FK_STUDY=erv.fk_study AND ptprot.fk_per=erv.fk_per AND ptprot.PATPROT_STAT=1 ) AS DMGRPH_REPORTABLE , ");
		sb.append(" p.person_zip, p.person_country,  p.PERSON_DOB, (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst=p.FK_CODELST_GENDER ) GENDER , " );
		sb.append("(SELECT PATSTUDYSTAT_DATE FROM ER_PATSTUDYSTAT WHERE PK_PATSTUDYSTAT =");
		sb.append("(NVL((SELECT MAX( PK_PATSTUDYSTAT ) FROM ER_PATSTUDYSTAT WHERE FK_PER=erv.fk_per AND FK_STUDY=erv.fk_study");
		sb.append(" AND FK_CODELST_STAT=(SELECT PK_CODELST  FROM ER_CODELST  WHERE CODELST_TYPE='patStatus' AND CODELST_SUBTYP='enrolled'))");
		sb.append(",(SELECT MAX( PK_PATSTUDYSTAT ) FROM ER_PATSTUDYSTAT WHERE FK_PER =erv.fk_per AND FK_STUDY=erv.fk_study AND FK_CODELST_STAT=");
		sb.append("(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='patStatus' AND CODELST_SUBTYP='enrolled')) ) ) ) PERSON_REGDATE");
		sb.append( ", (select count(*) from er_studysites ess,er_patprot epp where ess.fk_site = epp.fk_site_enrolling AND" +
				" epp.fk_per=erv.fk_per AND ess.fk_study=erv.fk_study AND ess.fk_study=epp.fk_study) PatEnrSiteOnStdTeam");
		sb.append( ", (SELECT site_name  FROM ER_SITE " );
		sb.append(" WHERE pk_site = per_site  ) site_name, pkg_studystat.f_get_patientright( '"+userId+"' , '"+userGroupId+"' , erv.fk_per ) right_mask, ");
		sb.append(" erv.study_number, erv.PATPROT_PATSTDID, erv.fk_study,  lower(per_code) lowerpercode, erv.per_code, " );
		sb.append(" DECODE(status_browser_flag,1,patstudystat_desc,'-') patstudystat_desc, erv.PATSTUDYSTAT_SUBTYPE, ");
		sb.append(" erv.PK_PATSTUDYSTAT,  NVL( (SELECT codelst_desc  FROM er_codelst  WHERE pk_codelst=(SELECT fk_codelst_stat ");
		sb.append(" FROM er_patstudystat WHERE fk_per    = erv.fk_per AND fk_study    =erv.fk_study  AND current_stat=1 ) " );
		sb.append(" AND codelst_type      ='patStatus'  AND codelst_custom_col='browser'  ), '-') patstudycur_stat, " );
		sb.append(" DECODE(status_browser_flag,1,patstudystat_date,NULL) patstudystat_date_datesort, " );
		sb.append(" DECODE(status_browser_flag,1,patstudystat_note,'-') patstudystat_note, " );
		sb.append(" DECODE(status_browser_flag,1,patstudystat_reason_desc,'-') patstudystat_reason, " );
		sb.append(" erv.patprot_enroldt AS patprot_enroldt_datesort, lower(erv.last_visit_name) lowerlastvisit, " );
		sb.append(" erv.last_visit_name,  erv.cur_visit,  erv.cur_visit_date,  erv.next_visit AS next_visit_datesort, " );
		sb.append(" lower(p.person_lname || p.person_fname) mask_patnamelower,  lower(erv.PATPROT_PATSTDID) lowerpatstdid, " );
		sb.append(" erv.PATPROT_PATSTDID,  ('[VELSEP]'  ||p.PERSON_HPHONE ||'[VELSEP]'  ||p.PERSON_BPHONE ||'[VELSEP]') mask_PERSON_PHONE , ");
		sb.append(" ('[VELSEP]' ||p.PERSON_ADDRESS1 ||'[VELSEP]' || p.PERSON_ADDRESS2 ||'[VELSEP]' ||p.PERSON_CITY ||'[VELSEP]' || p.PERSON_STATE ||'[VELSEP]' ||p.PERSON_ZIP ||'[VELSEP]' ||p.PERSON_COUNTRY ||'[VELSEP]') AS mask_pataddress , " );
		sb.append(" (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst=p.FK_CODELST_RACE  ) patrace , (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst=p.FK_CODELST_ETHNICITY  ) patethnicity , " );
		sb.append(" dbms_lob.substr(p.PERSON_NOTES_CLOB,1000,1) personnotes,  lower(p.person_lname) mask_lowerplname,  lower(p.person_fname) mask_lowerpfname, " );
		sb.append(" assignedto_name, physician_name, enrolledby_name, treatingorg_name,  (SELECT tx_name  FROM er_studytxarm " );
		sb.append(" WHERE pk_studytxarm=   (SELECT Z.fk_studytxarm    FROM er_pattxarm Z    WHERE Z.pk_pattxarm = (SELECT MAX(X.pk_pattxarm) FROM er_pattxarm X " );
		sb.append(" WHERE X.FK_PATPROT =erv.PK_PATPROT  AND X.tx_start_date= (SELECT MAX(Y.tx_start_date) FROM er_pattxarm Y ");
		sb.append(" WHERE Y.FK_PATPROT=erv.PK_PATPROT  )   )  ) ) current_tx_arm, (SELECT COUNT(*)  FROM esch.sch_adverseve eve " );
		sb.append(" WHERE eve.fk_study =erv.fk_study AND eve.fk_per =erv.fk_per  AND eve.fk_codlst_aetype " );
		sb.append(" IN (SELECT pk_codelst FROM sch_codelst cl WHERE cl.codelst_type='adve_type'   )  ) AE_COUNT_NUMSORT, " );
		sb.append(" (SELECT COUNT(*)  FROM esch.sch_adverseve eve  WHERE eve.fk_study =erv.fk_study  AND eve.fk_per=erv.fk_per " );
		sb.append(" AND eve.fk_codlst_aetype=  (SELECT pk_codelst    FROM sch_codelst cl   WHERE cl.codelst_type='adve_type' " );
		sb.append(" AND cl.codelst_subtyp='al_sadve'  )  ) SAE_COUNT_NUMSORT,  (SELECT COUNT(*)  FROM er_patforms pf  WHERE pf.fk_per =erv.fk_per " );
		sb.append(" AND pf.fk_patprot IN (SELECT pk_patprot  FROM er_patprot pt   WHERE PT.FK_STUDY=erv.fk_study  AND pt.fk_per    =erv.fk_per " );
		sb.append(" ) AND record_type<>'D' ) form_count_numsort FROM erv_studypat_by_visit erv ,  erv_person p , er_study estd	WHERE P.pk_person = erv.fk_per " );
		sb.append(" AND erv.fk_study  = ?	AND estd.pk_study = erv.fk_study" );
		
		// Added Condition For  Study Team Rights Bug#15565 
		sb.append(" AND pkg_util.f_getStudyRight(pkg_studystat.F_GET_USERRIGHTS_FOR_STUDY('"+userId+"', erv.fk_study ), " );
		sb.append(" (SELECT CTRL_SEQ  FROM er_ctrltab  WHERE CTRL_KEY = 'study_rights' " );
		sb.append("	AND upper(ctrl_value) = 'STUDYMPAT' )) >0  " );
				  
		// Added Condition for CutOff Dates. // YK: Modified the condition for Bug#15620
		sb.append(" AND ( exists ( Select * from ER_PATSTUDYSTAT where  FK_CODELST_STAT in (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE ='patStatus' AND CODELST_SUBTYP='enrolled' ) ");
		sb.append(" and fk_study  = erv.fk_study and fk_per = erv.fk_per and PATSTUDYSTAT_DATE <= F_TO_DATE('").append(cutOffDate).append("') )" );
	    
		sb.append(" AND NOT EXISTS (SELECT * FROM ER_PATSTUDYSTAT WHERE FK_CODELST_STAT IN (SELECT PK_CODELST FROM ER_CODELST");
		sb.append(" WHERE CODELST_TYPE ='patStatus' AND CODELST_SUBTYP='infConsent' ) AND fk_study = erv.fk_study AND fk_per  = erv.fk_per))");

		sb.append(" AND EXISTS  (SELECT * FROM ER_PATFACILITY fac  WHERE fac.fk_per= erv.fk_per " );
		sb.append(" AND fac.fk_site IN( "+siteIds+" )  AND fac.patfacility_accessright > 0  )	" );
		Rlog.debug("CTRP","In getPatientAccrualData Query is : "+sb.toString());
		
		Connection conn = getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		try {
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setInt(1, studyId);
			rs = pstmt.executeQuery();
	        int count=0;
	        while(rs.next()){
	        	CtrpPatientAccrualDao ctrpPatientAccrualPojo = new CtrpPatientAccrualDao();
	        	ctrpPatientAccrualPojo.setFkPer( rs.getInt("FK_PER") );	
	        	ctrpPatientAccrualPojo.setMaskPersonLName( rs.getString("MASK_PERSON_LNAME") );	
	        	ctrpPatientAccrualPojo.setSiteId( rs.getString("SITEID") );
	        	ctrpPatientAccrualPojo.setNciTrailIden( rs.getString("NCI_TRIAL_IDEN") );
	        	ctrpPatientAccrualPojo.setPersonZip( rs.getString("PERSON_ZIP") );
	        	ctrpPatientAccrualPojo.setPersonCountry( rs.getString("person_country") );
	        	ctrpPatientAccrualPojo.setPersonDOB( rs.getString("PERSON_DOB") );
	        	ctrpPatientAccrualPojo.setPersonGender( rs.getString("GENDER") );
	        	ctrpPatientAccrualPojo.setPersonRegDate( rs.getString("PERSON_REGDATE") );
	        	ctrpPatientAccrualPojo.setSiteName( rs.getString("SITE_NAME") );
	        	ctrpPatientAccrualPojo.setRightMask( rs.getString("RIGHT_MASK") );
	        	ctrpPatientAccrualPojo.setStudyNumber( rs.getString("STUDY_NUMBER") );
	        	ctrpPatientAccrualPojo.setPkPatProt( rs.getString("PATPROT_PATSTDID") );
	        	ctrpPatientAccrualPojo.setFkStudy( rs.getString("FK_STUDY") );
	        	ctrpPatientAccrualPojo.setLowerPerCode( rs.getString("LOWERPERCODE") );
	        	ctrpPatientAccrualPojo.setPerCode( rs.getString("PER_CODE") );
	        	ctrpPatientAccrualPojo.setPatStudyStatDesc( rs.getString("PATSTUDYSTAT_DESC") );
	        	ctrpPatientAccrualPojo.setPatStudyStatSubtype( rs.getString("PATSTUDYSTAT_SUBTYPE") );
	        	ctrpPatientAccrualPojo.setPkPatStudyStat( rs.getString("PK_PATSTUDYSTAT") );
	        	ctrpPatientAccrualPojo.setPatStudyCurStat( rs.getString("PATSTUDYCUR_STAT") ); 
	        	ctrpPatientAccrualPojo.setPatStudyStatDateDateSort( rs.getString("PATSTUDYSTAT_DATE_DATESORT") );
	        	ctrpPatientAccrualPojo.setPatStudyStatNote( rs.getString("PATSTUDYSTAT_NOTE") );
	        	ctrpPatientAccrualPojo.setPatStudyStatReason(rs.getString("PATSTUDYSTAT_REASON") );
	        	ctrpPatientAccrualPojo.setPatProtEnroldtDatesort( rs.getString("PATPROT_ENROLDT_DATESORT") );
	        	ctrpPatientAccrualPojo.setLowerLastVisit( rs.getString("LOWERLASTVISIT") );
	        	ctrpPatientAccrualPojo.setLastVisitName( rs.getString("LAST_VISIT_NAME") );
	        	ctrpPatientAccrualPojo.setCurVisit( rs.getString("CUR_VISIT") );
	        	ctrpPatientAccrualPojo.setCurVisitDate( rs.getString("CUR_VISIT_DATE") );
	        	ctrpPatientAccrualPojo.setNextVisitDateSort( rs.getString("NEXT_VISIT_DATESORT") );
	        	ctrpPatientAccrualPojo.setMaskPatnameLower( rs.getString("MASK_PATNAMELOWER") );
	        	ctrpPatientAccrualPojo.setLowerPatStdId( rs.getString("LOWERPATSTDID") );
	        	ctrpPatientAccrualPojo.setPatProtPatstdid( rs.getString("PATPROT_PATSTDID") );
	        	ctrpPatientAccrualPojo.setMaskPersonPhone( rs.getString("MASK_PERSON_PHONE") );
	        	ctrpPatientAccrualPojo.setMaskPatAddress( rs.getString("MASK_PATADDRESS") );
	        	ctrpPatientAccrualPojo.setPatRace( rs.getString("PATRACE") );
	        	ctrpPatientAccrualPojo.setPatEthnicity( rs.getString("PATETHNICITY") );
	        	ctrpPatientAccrualPojo.setPersonNotes( rs.getString("PERSONNOTES") );
	        	ctrpPatientAccrualPojo.setMaskLowerplName( rs.getString("MASK_LOWERPLNAME") );
	        	ctrpPatientAccrualPojo.setMaskLowerpfName( rs.getString("MASK_LOWERPFNAME") );
	        	ctrpPatientAccrualPojo.setAssignedtoName( rs.getString("ASSIGNEDTO_NAME") );
	        	ctrpPatientAccrualPojo.setPhysicianName( rs.getString("PHYSICIAN_NAME") );
	        	ctrpPatientAccrualPojo.setEnrolledbyName( rs.getString("ENROLLEDBY_NAME") );
	        	ctrpPatientAccrualPojo.setTreatIngorgName( rs.getString("TREATINGORG_NAME") );
	        	ctrpPatientAccrualPojo.setCurrentTxArm( rs.getString("CURRENT_TX_ARM") );
	        	ctrpPatientAccrualPojo.setAeCountNumsort( rs.getString("AE_COUNT_NUMSORT") );
	        	ctrpPatientAccrualPojo.setSaeCountNumsort( rs.getString("SAE_COUNT_NUMSORT") );
	        	ctrpPatientAccrualPojo.setFormCountNumsort( rs.getString("FORM_COUNT_NUMSORT") );
	        	ctrpPatientAccrualPojo.setSubjectDiseaseCode(rs.getString("DISEASE_CODE"));
	        	ctrpPatientAccrualPojo.setDmgrphReportable(rs.getInt("DMGRPH_REPORTABLE"));
	        	ctrpPatientAccrualPojo.setPatEnrSiteOnStdTeam(rs.getInt("PatEnrSiteOnStdTeam"));
	        	
	        	patAccList.add(ctrpPatientAccrualPojo);
	        	count++;
	        }
	        Rlog.debug("CTRP","Total record count is : "+count);
		} catch (SQLException e) {
			Rlog.fatal("CTRP","In getPatientAccrualData Query is : "+sb.toString());
			e.printStackTrace();
		}finally{
			try {
                if (conn != null)conn.close();
            } catch (SQLException e) {
            	Rlog.fatal("CTRP","Error while closing the Connection in CtrpAccrualDao.getPatientAccrualData method");
    			e.printStackTrace();
            }
		}
		return patAccList;
	}

	public ArrayList<StudySiteBean> getSiteAccrualCount(Map<String, String> dataMapForPatient) {
		ArrayList<StudySiteBean> finalList =  new ArrayList<StudySiteBean>();;
		int studyId = StringUtil.stringToNum(dataMapForPatient.get("studyId"));
		String cutOffDate  = dataMapForPatient.get("cutOffDate");
		// Added Condition For  Study Team Rights Bug#15565 
		int userId = StringUtil.stringToNum( dataMapForPatient.get("userId") ); 
		StringBuilder sb = new StringBuilder();
		/*sb.append( " SELECT  " );
		sb.append( " PK_STUDYSITES, " );
		sb.append( " FK_STUDY, " );
		sb.append( " FK_CODELST_STUDYSITETYPE, " );
		sb.append( " STUDYSITE_ENRCOUNT " );
		sb.append( " FROM ER_STUDYSITES " );
		sb.append( " WHERE FK_STUDY = ? " );*/
		//YK_Modified the query to Fetch Patient Data based on CuttOff Date For Bug#15546 and #15568 
		sb.append( " SELECT FK_SITE_ENROLLING,(select NVL(NCI_POID,' ') from er_site where PK_SITE = FK_SITE_ENROLLING) siteSiteId, " );
		//YPS Modified the query for apply the study team rights for Manage Patient (STUDYMPAT) For Bug#15565
		sb.append( "  DECODE( ( pkg_util.f_getStudyRight(pkg_studystat.F_GET_USERRIGHTS_FOR_STUDY('" + userId + "', '" + studyId + "' ), " );
		sb.append( " ( SELECT CTRL_SEQ  FROM er_ctrltab  WHERE CTRL_KEY = 'study_rights' " );
		sb.append( " AND upper(ctrl_value) = 'STUDYMPAT' ))), 0  , '' , count(*) ) Patient_count " );
		sb.append( " FROM ER_PATSTUDYSTAT b, " );
		sb.append( " er_patprot c " );
		sb.append( " WHERE b.FK_STUDY = c.FK_STUDY " );
		sb.append( " AND b.FK_STUDY = ? " );
		sb.append( " AND b.FK_PER =c.FK_PER " );
		sb.append( " AND c.PATPROT_STAT =1 " );
		sb.append( " AND PK_PATSTUDYSTAT = " );
		
		sb.append( "(SELECT MAX(PK_PATSTUDYSTAT) FROM ER_PATSTUDYSTAT d where d.FK_CODELST_STAT = (SELECT PK_CODELST FROM ER_CODELST");
		sb.append(" WHERE CODELST_TYPE ='patStatus' AND CODELST_SUBTYP='enrolled')");
		sb.append("AND d.fk_study=b.FK_STUDY AND b.FK_PER =d.FK_PER AND d.PATSTUDYSTAT_DATE <= F_TO_DATE('"+cutOffDate+"'))" );

		
		sb.append( " GROUP BY FK_SITE_ENROLLING " );
		
		Connection conn = getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		try {
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setInt(1, studyId);
			rs = pstmt.executeQuery();
	        while(rs.next()){
	        	StudySiteBean ssBean = new StudySiteBean();
	        	ssBean.setStudySiteId(EJBUtil.stringToNum(rs.getString("FK_SITE_ENROLLING")));
				ssBean.setSiteSiteId(rs.getString("siteSiteId"));
	        	ssBean.setCurrentSitePatientCount( rs.getString("Patient_count") );
	        	finalList.add(ssBean);
	        }
		}catch (Exception e) {
		}
		finally{
			try {
                if (conn != null)conn.close();
                if(pstmt!=null) pstmt.close();
                if(rs!=null) rs.close();
            } catch (SQLException e) {
            	Rlog.fatal("CTRP","Error while closing the Connection in CtrpAccrualDao.getSiteAccrualCount method");
    			e.printStackTrace();
            }
		}
		return finalList;
	}
	
}
