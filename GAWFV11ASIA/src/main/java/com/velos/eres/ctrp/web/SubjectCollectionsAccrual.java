package com.velos.eres.ctrp.web;

import java.io.Serializable;

public class SubjectCollectionsAccrual implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String studyId;
	private String SubjectCollectionsAccrual;
	private String studyIdentifier;
	private String blankField1;
	private String blankField2;
	private String blankField3;
	private String blankField4;
	private String blankField5;
	private String blankField6;
	private String blankField7;
	private String blankField8;
	private String blankField9;
	/**
	 * @return the studyId
	 */
	public String getStudyId() {
		return studyId;
	}
	/**
	 * @param studyId the studyId to set
	 */
	public void setStudyId(String studyId) {
		this.studyId = studyId;
	}
	/**
	 * @return the subjectCollectionsAccrual
	 */
	public String getSubjectCollectionsAccrual() {
		return SubjectCollectionsAccrual;
	}
	/**
	 * @param subjectCollectionsAccrual the subjectCollectionsAccrual to set
	 */
	public void setSubjectCollectionsAccrual(String subjectCollectionsAccrual) {
		SubjectCollectionsAccrual = subjectCollectionsAccrual;
	}
	/**
	 * @return the studyIdentifier
	 */
	public String getStudyIdentifier() {
		return studyIdentifier;
	}
	/**
	 * @param studyIdentifier the studyIdentifier to set
	 */
	public void setStudyIdentifier(String studyIdentifier) {
		this.studyIdentifier = studyIdentifier;
	}
	/**
	 * @return the blankField1
	 */
	public String getBlankField1() {
		return blankField1;
	}
	/**
	 * @param blankField1 the blankField1 to set
	 */
	public void setBlankField1(String blankField1) {
		this.blankField1 = blankField1;
	}
	/**
	 * @return the blankField2
	 */
	public String getBlankField2() {
		return blankField2;
	}
	/**
	 * @param blankField2 the blankField2 to set
	 */
	public void setBlankField2(String blankField2) {
		this.blankField2 = blankField2;
	}
	/**
	 * @return the blankField3
	 */
	public String getBlankField3() {
		return blankField3;
	}
	/**
	 * @param blankField3 the blankField3 to set
	 */
	public void setBlankField3(String blankField3) {
		this.blankField3 = blankField3;
	}
	/**
	 * @return the blankField4
	 */
	public String getBlankField4() {
		return blankField4;
	}
	/**
	 * @param blankField4 the blankField4 to set
	 */
	public void setBlankField4(String blankField4) {
		this.blankField4 = blankField4;
	}
	/**
	 * @return the blankField5
	 */
	public String getBlankField5() {
		return blankField5;
	}
	/**
	 * @param blankField5 the blankField5 to set
	 */
	public void setBlankField5(String blankField5) {
		this.blankField5 = blankField5;
	}
	/**
	 * @return the blankField6
	 */
	public String getBlankField6() {
		return blankField6;
	}
	/**
	 * @param blankField6 the blankField6 to set
	 */
	public void setBlankField6(String blankField6) {
		this.blankField6 = blankField6;
	}
	/**
	 * @return the blankField7
	 */
	public String getBlankField7() {
		return blankField7;
	}
	/**
	 * @param blankField7 the blankField7 to set
	 */
	public void setBlankField7(String blankField7) {
		this.blankField7 = blankField7;
	}
	/**
	 * @return the blankField8
	 */
	public String getBlankField8() {
		return blankField8;
	}
	/**
	 * @param blankField8 the blankField8 to set
	 */
	public void setBlankField8(String blankField8) {
		this.blankField8 = blankField8;
	}
	/**
	 * @return the blankField9
	 */
	public String getBlankField9() {
		return blankField9;
	}
	/**
	 * @param blankField9 the blankField9 to set
	 */
	public void setBlankField9(String blankField9) {
		this.blankField9 = blankField9;
	}
}
