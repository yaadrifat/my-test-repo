package com.velos.eres.ctrp.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.velos.eres.business.common.CtrlDao;
import com.velos.eres.business.common.GroupDao;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.SessionMaint;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.group.GroupJB;
import com.velos.eres.web.grpRights.GrpRightsJB;
import com.velos.eres.web.user.UserJB;

public class CtrpAccrualAction extends ActionSupport  {
	private static final long serialVersionUID = -4944853697792820322L;
	private HttpServletRequest request = null;
	private HttpServletResponse response = null;
	private CtrpAccrualJB ctrpAccrualJB = null;

	private static final String CTRP_ACCRUAL_BROWSER_TILE = "ctrpAccrualBrowserTile";
	private static final String CTRP_STUDY_IDS ="selectedStudyIds";
	private static final String CTRP_RESEARCH_TYPES ="researchType";
	private static final String PK_STUDY = "pkStudy";
	private static final String STUDY_TYPE = "studyType";
	private static final String CUT_OFFDATE = "cutOffDate";
	
	private Map userDataMap=null;
	public Map getUserDataMap() {
		return userDataMap;
	}
	public void setUserDataMap(Map userDataMap) {
		this.userDataMap = userDataMap;
	}
	
	public CtrpAccrualAction() {
		ActionContext ac = ActionContext.getContext();
		request = (HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
		response = (HttpServletResponse)ac.get(ServletActionContext.HTTP_RESPONSE);
		ctrpAccrualJB = new CtrpAccrualJB();
	}
	public CtrpAccrualJB getCtrpAccrualJB() {
		return ctrpAccrualJB;
	}
	
	public void setCtrpAccrualJB(CtrpAccrualJB ctrpAccrualJB) {
		this.ctrpAccrualJB = ctrpAccrualJB;
	}
	
	public String getctrpAccrualBrowser() {
		this.populateJB();
		return CTRP_ACCRUAL_BROWSER_TILE;
	}
	private void populateJB()
	{
		String userId ="" ;
		char protocolManagementRight = '0';
		HttpSession tSession = request.getSession(false);
		SessionMaint sessionmaint = new SessionMaint();
		if (sessionmaint.isValidSession(tSession)) {

			GrpRightsJB grpRights = (GrpRightsJB) request.getSession(false).getAttribute("GRights");
			if (grpRights == null) { return; }
			String modRight = (String) tSession.getValue("modRight");
			
			
			userId = (String) tSession.getAttribute("userId");
			ctrpAccrualJB.setUserIdS(userId);
	    	String accountId = (String) tSession.getValue("accountId");
	    	ctrpAccrualJB.setAccountId(accountId);
	    	
	    	int usrId = EJBUtil.stringToNum(userId);
	    	UserJB userB = new UserJB();
	    	userB.setUserId(usrId);
	    	userB.getUserDetails();
	    	
	    	
	    	String defGroup = userB.getUserGrpDefault();
	    	int grpId=EJBUtil.stringToNum(defGroup);
	    	ctrpAccrualJB.setGrpId(String.valueOf(grpId));
	    	GroupJB groupB = new GroupJB();
	    	groupB.setGroupId(EJBUtil.stringToNum(defGroup));
	    	groupB.getGroupDetails();
	    	String groupName = groupB.getGroupName();
	    	ctrpAccrualJB.setGroupName(groupName);
	    	CtrlDao acmod = new CtrlDao();
	    	 acmod.getControlValues("study_rights","STUDYSUM");  
	         ArrayList aRightSeq = acmod.getCSeq();  
	         String rightSeq = aRightSeq.get(0).toString();  
	         ctrpAccrualJB.setRightSeq(rightSeq);
	         int iRightSeq = EJBUtil.stringToNum(rightSeq);
	        
	        String superuserRights = "";
	    	superuserRights = GroupDao.getDefaultStudySuperUserRights(userId);
	    	ctrpAccrualJB.setSuperuserRights(superuserRights);
	    	

		}
	}
	
	public String fetchPatientAccural(){
		String zipFileName=null;
		ArrayList<String> generatedDoclist=null;
		ArrayList<String> researchTypes=null;
		ArrayList<String> studyIds=null;
		Map<String, ArrayList<String>> stdNResTypeMap =null;
		String stdNResType = null;
		ArrayList<String> userDeatils = new ArrayList<String>();
		Boolean indusFlag = false;
		
		this.populateJB();
		
		stdNResType = StringUtil.trueValue( request.getParameter(CTRP_STUDY_IDS));
		//get Data From session for user details 
		HttpSession tSession = request.getSession(true);
		
		String userId 	= (String) tSession.getAttribute("userId");
	    UserJB userB 	= (UserJB) tSession.getAttribute("currentUser");
	    String accountId = (String) tSession.getAttribute("accountId");
	    String userGroupId  = 	userB.getUserGrpDefault();
		String siteId 		=	userB.getUserSiteId();
		
		userDeatils.add(userId);
		userDeatils.add(userGroupId);
		userDeatils.add(siteId);
		userDeatils.add(accountId);
		
		String cutOffDate = StringUtil.trueValue(request.getParameter(CUT_OFFDATE));
		cutOffDate=cutOffDate==null?"":cutOffDate;
		ArrayList<String> cutOffDateList = new ArrayList<String>();
		cutOffDateList.add(cutOffDate);		
		stdNResTypeMap = ctrpAccrualJB.findStudyAndResearchTypeData(stdNResType);
		
		stdNResTypeMap.put("userDeatils", userDeatils);
		stdNResTypeMap.put("cutOffDate", cutOffDateList);
		
		generatedDoclist  = ctrpAccrualJB.generateStudySubjectAccrual(stdNResTypeMap);
		
		researchTypes =stdNResTypeMap.get( CTRP_RESEARCH_TYPES );
		studyIds =stdNResTypeMap.get(CtrpAccrualExportJB.STUDY_IDS);
		
		if(researchTypes.indexOf("Industrial")!=-1) indusFlag	=	true;
		
		zipFileName = ctrpAccrualJB.prepareZipFileFromDoc( generatedDoclist , indusFlag );
		StringBuilder sb = new StringBuilder();
		int i=1;
		for(String studyId : studyIds){
			if(studyIds.size()!=i){
				sb.append(studyId).append(",");
			}else{
				sb.append(studyId);
			}
			i++;
		}
		Map<String , String > dataMap = new HashMap<String, String>();
		// validating data
		CtrpAccrualExportJBHelper ctrpAccHelper = new CtrpAccrualExportJBHelper();
		ctrpAccHelper.validateStudiesData(ctrpAccrualJB,dataMap,indusFlag);
				
		dataMap.put("zipFileName", zipFileName);
		dataMap.put("studyIdList", sb.toString());
		this.userDataMap=dataMap;
		return SUCCESS;
	}
	
	public String downloadPatStdAccrual(){
		String zipFileName = null;
		String downloadedStudyIds = null;
		String delFlag = null;
		HttpSession tSession = request.getSession(true);
		String userId 	= (String) tSession.getAttribute("userId");
		zipFileName = request.getParameter("zipFileName")== null ? "" : request.getParameter("zipFileName");
		delFlag = request.getParameter("delFlag");
		String cutOffDate= request.getParameter("cutOffDateForPat");
		cutOffDate=cutOffDate==null?"":cutOffDate;
		
		if(delFlag.equals("0")){
			ctrpAccrualJB.downloadPatStdAccrual(response, zipFileName);
			downloadedStudyIds=request.getParameter("downloadedStudyIds")==null ? "":request.getParameter("downloadedStudyIds");
			ctrpAccrualJB.setStudyLastAccrualGenerationDate(downloadedStudyIds);
			ctrpAccrualJB.setCutOffDtAndLastDwnLoadBy(downloadedStudyIds,cutOffDate,userId);
		}else{
			ctrpAccrualJB.deleteCTRPAccrualDoc(zipFileName);
		}
		
		return null;
	}
	
	public String downloadErrorList(){
		
		String errorListFileName = request.getParameter("errorListFileName")== null ? "" : request.getParameter("errorListFileName");
		String delFlag = request.getParameter("delFlag")==null?"":request.getParameter("delFlag");
	
		if(delFlag.equals("0")){
			ctrpAccrualJB.downloadErrorList(response, errorListFileName);
		}else{
			ctrpAccrualJB.deleteErrorList(errorListFileName);
		}
		return null;
	}
	
	public String validateStudyAccru(){
		ctrpAccrualJB.setStudyId(StringUtil.trueValue(request.getParameter(PK_STUDY)));
		ctrpAccrualJB.setStudyType(StringUtil.trueValue(request.getParameter(STUDY_TYPE)));
		//this.userDataMap = ctrpAccrualJB.validateStudyAccru();

		
		ArrayList<String> researchTypes=null;
		Map<String, ArrayList<String>> stdNResTypeMap =null;
		String stdNResType = null;
		ArrayList<String> userDeatils = new ArrayList<String>();
		ArrayList<String> cutOffDateList = new ArrayList<String>();
		Boolean indusFlag = false;
		
		this.populateJB();
		String cutOffDate = StringUtil.trueValue(request.getParameter(CUT_OFFDATE));
		stdNResType = StringUtil.trueValue(request.getParameter(PK_STUDY));
		//get Data From session for user details) 
		HttpSession tSession = request.getSession(true);
		
		String userId 	= (String) tSession.getAttribute("userId");
	    UserJB userB 	= (UserJB) tSession.getAttribute("currentUser");
	    String accountId = (String) tSession.getAttribute("accountId");
	    String userGroupId  = 	userB.getUserGrpDefault();
		String siteId 		=	userB.getUserSiteId();
		
		userDeatils.add(userId);
		userDeatils.add(userGroupId);
		userDeatils.add(siteId);
		userDeatils.add(accountId);
		
		cutOffDate=cutOffDate==null?"":cutOffDate;
		cutOffDateList.add(cutOffDate);
		stdNResTypeMap = ctrpAccrualJB.findStudyAndResearchTypeData(stdNResType);
		stdNResTypeMap.put("userDeatils", userDeatils);
		stdNResTypeMap.put("cutOffDate", cutOffDateList);
		
		ctrpAccrualJB.populateStudySubjectAccrual(stdNResTypeMap);
		
		researchTypes =stdNResTypeMap.get( CTRP_RESEARCH_TYPES );
		
		if(researchTypes.indexOf("Industrial")!=-1) indusFlag	=	true;

		Map<String , String > dataMap = new HashMap<String, String>();
		
		// validating data
		CtrpAccrualExportJBHelper ctrpAccHelper = new CtrpAccrualExportJBHelper();
		ctrpAccHelper.validateStudyData(ctrpAccrualJB,dataMap,indusFlag);
		
		this.userDataMap=dataMap;
		return SUCCESS;
		
	}
}
