/**
 * __astcwz_cmpnt_det: Author: Sonia Sahni Date and Time Created: Wed Jul 09
 * 14:41:07 IST 2003 Note: DO NOT DELETE OR MODIFY KONESA CODEWIZARD COMMENTS
 * STARTING WITH ///__astcwz_ and /**__astcwz_
 */
// /__astcwz_model_idt#(0)
// /__astcwz_maxim_idt#(1587)
// /__astcwz_packg_idt#(1580,1582,1583,1584~,,,)
package com.velos.eres.tags;

import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.StringUtil;

// /__astcwz_class_idt#(1428!n)
public abstract class VTag {
    // /__astcwz_attrb_idt#(1433)
    public String id;

    // /__astcwz_attrb_idt#(1434)
    public String tagClass;

    // /__astcwz_attrb_idt#(1435)
    public String title;

    // /__astcwz_attrb_idt#(1436)
    public String style;

    // /__astcwz_attrb_idt#(1517)
    public boolean sameLine;

    // /__astcwz_opern_idt#(1514)
    public String drawXSL() {
        return "";
    }

    public String getStyle() {

        // get style
        if (!EJBUtil.isEmpty(this.style)) {
            return " style = \"" + this.style + "\"";
        } else {
            return "";

        }

    }

    public String getTagClass() {

        // get class

        if (!EJBUtil.isEmpty(this.tagClass)) {
            return " class = \"" + this.tagClass + "\"";
        } else {
            return "";

        }

    }

    public String getTitle() {
        // get title
        String escapeSpecialString = "";

        if (!EJBUtil.isEmpty(this.title)) {
            escapeSpecialString = VTag.escapeSpecial(this.title);
            return " title = \"" + escapeSpecialString + "\"";
        } else {
            return "";

        }

    }

    public String getId() {
        // get title

        if (!EJBUtil.isEmpty(this.id)) {
            return " id = \"" + (this.id) + "\"";
        } else {
            return "";

        }

    }

    public String getHiddenId(String str) {
        // get title
        String fldId = "hdn_" + str + "_" + this.id;

        if (!EJBUtil.isEmpty(this.id)) {
            return " id = \"" + fldId + "\"";
        } else {
            return "";

        }

    }

    public String getHiddenReasonId(String str) {
        // get title
        String fldId = "rsn_" + str + "_" + this.id;

        if (!EJBUtil.isEmpty(this.id)) {
            return " id = \"" + fldId + "\"";
        } else {
            return "";

        }

    }

    // get help icon
    public String getHelpIcon() {

        if (!EJBUtil.isEmpty(this.title)) {
            return "<img src=\"../images/jpg/help.jpg\" " + getToolTip()
                    + " ></img> ";
        } else {
            return "";

        }

    }

    // make the tooltip using code instead of title as title tooltip stays only
    // for 10 sec
    public String getToolTip() {
        String escapeSpecialString = "";

        if (!EJBUtil.isEmpty(this.title)) {

            escapeSpecialString = StringUtil.htmlDecode(this.title);
            escapeSpecialString = VTag.escapeSpecial(escapeSpecialString);
            return " onmouseover=\"return overlib('" + escapeSpecialString
                    + "',CAPTION,'Help');\" onmouseout=\"return nd();\" ";
        } else {
            return "";

        }

    }

    /**
     * In a String, escapes special characters restricted in XSL
     * 
     * @param oldString
     *            tge string in which special characters are to be replaced
     * @return Returns a string with special characters escaped
     */
    public static String escapeSpecial(String oldString) {
        String escapedString;

        if (EJBUtil.isEmpty(oldString))
            return oldString;

        escapedString = oldString;

        escapedString = StringUtil.replace(escapedString, "&", "&amp;");
        escapedString = StringUtil.replace(escapedString, "\"", "&quot;");
        // escapedString = StringUtil.replace(escapedString,"'","&apos;");
        escapedString = StringUtil.replace(escapedString, "'", "\\'");
        escapedString = StringUtil.replace(escapedString, "<", "&lt;");
        escapedString = StringUtil.replace(escapedString, ">", "&gt;");

        // escapedString = StringUtil.replace(escapedString,"\\", "\\\\");

        return escapedString;

    }
}
