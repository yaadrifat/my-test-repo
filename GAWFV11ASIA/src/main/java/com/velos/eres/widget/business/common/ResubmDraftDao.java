package com.velos.eres.widget.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import oracle.jdbc.OraclePreparedStatement;

import org.apache.commons.lang.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.service.util.CFG;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.SessionMaint;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.widget.service.util.FlxPageArchive;

public class ResubmDraftDao extends CommonDAO implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private static final String getFlexPageCookie = "select page_def, page_js from UI_FLX_PAGE where PAGE_ID=?";
	//private static final String getFlexPageJS = "select page_js from UI_FLX_PAGE where PAGE_ID=?";
	private static final String insertFlexPageSql = "insert into UI_FLX_PAGE(PAGE_ID, PAGE_DEF, PAGE_JS) values (?,?,?) ";
	private static final String upateFlexPageSql = "update UI_FLX_PAGE set PAGE_DEF = ? where PAGE_ID = ?";
	private static final String selectResubmDraftSql = "SELECT "+
			" PAGE_MOD_TABLE, PAGE_MOD_PK, PAGE_VER, PAGE_ID, PAGE_DEF, PAGE_JS, PAGE_JSON, PAGE_HTML, PAGE_MINOR_VER, PAGE_VERDIFF, FK_SUBMISSION, PAGE_ATTACHDIFF, PAGE_FORMRESPONSES, PAGE_FORMRESPDIFF, PAGE_COMMENTS FROM "+
			" ER_RESUBM_DRAFT WHERE PAGE_ID=? AND PAGE_MOD_TABLE=? AND PAGE_MOD_PK=? AND PAGE_VER=? AND PAGE_MINOR_VER=?";
	private static final String insertResubmDraftSql = "insert into ER_RESUBM_DRAFT "+
			" (PAGE_MOD_TABLE, PAGE_MOD_PK, PAGE_VER, PAGE_ID, PAGE_DEF, PAGE_JS, PAGE_JSON, PAGE_HTML, PAGE_MINOR_VER, PAGE_VERDIFF, FK_SUBMISSION, PAGE_ATTACHDIFF, PAGE_FORMRESPONSES, PAGE_FORMRESPDIFF, PAGE_COMMENTS) values "+
			" (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
	private static final String updateResubmDraftSql = "update ER_RESUBM_DRAFT set "+
			" PAGE_DEF=?, PAGE_JS=?, PAGE_JSON=?, PAGE_HTML=?, PAGE_VERDIFF=?, FK_SUBMISSION=?, PAGE_ATTACHDIFF=?, PAGE_FORMRESPONSES=?, PAGE_FORMRESPDIFF=?, PAGE_COMMENTS=? "+
			" WHERE PAGE_ID=? AND PAGE_MOD_TABLE=? AND PAGE_MOD_PK=? AND PAGE_VER=? AND PAGE_MINOR_VER=?";
	
	private static final String updateResubmDraftRetainLastDiffSql = "update ER_RESUBM_DRAFT set "+
			" PAGE_DEF=?, PAGE_JS=?, PAGE_JSON=?, PAGE_HTML=?, FK_SUBMISSION=?, PAGE_FORMRESPONSES=? "+
			" WHERE PAGE_ID=? AND PAGE_MOD_TABLE=? AND PAGE_MOD_PK=? AND PAGE_VER=? AND PAGE_MINOR_VER=?";

	private static final String getHighestResubmDraftVersionSql = "select nvl(max(PAGE_VER), 0) from ER_RESUBM_DRAFT where "+
			"PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ? ";
	private static final String getHighestMinorResubmDraftVersionSql = "select nvl(max(PAGE_MINOR_VER), 0) from ER_RESUBM_DRAFT where "+
			"PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ? and page_ver= ?";
	private static final String getResubmDraftPageHtmlOfHighestVersionSql = 
			"select PAGE_HTML, PAGE_VER, PAGE_MINOR_VER from ER_RESUBM_DRAFT where "+
			" PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ? "+
			" and PAGE_VER = (select nvl(max(PAGE_VER), 0) from ER_RESUBM_DRAFT where "+
			" PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ?) "+
			" and PAGE_MINOR_VER = (select nvl(max(PAGE_MINOR_VER), 0) from ER_RESUBM_DRAFT where "+
			" PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ? and PAGE_VER = ("+
			" select max(PAGE_VER) from ER_RESUBM_DRAFT where PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ? "+
			" )) ";
	private static final String getHighestResubmDraftFullVersionSql = 
			"select PAGE_VER, PAGE_MINOR_VER, FK_SUBMISSION, PAGE_COMMENTS from ER_RESUBM_DRAFT where PAGE_MOD_TABLE = ? "+ 
			" and PAGE_MOD_PK = ? order by PAGE_VER desc, PAGE_MINOR_VER desc nulls last  ";
	
	private static final String getFlexPageLatestSubmissionStatusSql = 
			"SELECT SUBMISSION_STATUS, PK_SUBMISSION_STATUS FROM ER_SUBMISSION_STATUS WHERE PK_SUBMISSION_STATUS = "+
            " (SELECT MAX (PK_SUBMISSION_STATUS) FROM ER_SUBMISSION_STATUS "+
            " WHERE SUBMISSION_STATUS IS NOT NULL AND FK_SUBMISSION = "+
            " (SELECT PK_SUBMISSION from (SELECT PK_SUBMISSION FROM ER_SUBMISSION WHERE FK_STUDY = ? ORDER BY PK_SUBMISSION DESC) where ROWNUM <= 1)) ";
	private static final String getResubmDraftVersionJSONSql = "SELECT PAGE_JSON FROM ER_RESUBM_DRAFT WHERE PAGE_MOD_TABLE = ? and page_mod_pk =? and page_ver = ?";
	
	private static final String getResubmDraftFormResponsesSql = "SELECT PAGE_FORMRESPONSES FROM ER_RESUBM_DRAFT WHERE PAGE_MOD_TABLE = ? and page_mod_pk =? and page_ver = ?";
	
	private static final String chkPageProtocolFrozen = "select PAGE_MOD_STAT_SUBTYP, PAGE_MOD_TABLE from ER_RESUBM_DRAFT where "
			+ " PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ? and PAGE_VER = ? and PAGE_MINOR_VER = ? ";
	
	private static final String updateCurrentResubmDraftChangesSql = "update ER_RESUBM_DRAFT set page_verdiff = ?, page_attachdiff=?, PAGE_FORMRESPDIFF=?, PAGE_COMMENTS = ? where "
			+ " PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ? and PAGE_VER = ? and PAGE_MINOR_VER = ? ";

	private static final String getResubmDraftVersionDiffDetailsSql = 
			"select page_verdiff from ER_RESUBM_DRAFT where "+
			" PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ? "+
			" and PAGE_VER = ? and PAGE_MINOR_VER = ?";
	
	private static final String updateStatOfLatestVersionSql =
			"update ER_RESUBM_DRAFT set PAGE_MOD_STAT_SUBTYP = ? where "+
			" PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ? "+
			" and PAGE_VER = ? and PAGE_MINOR_VER = ?";
	
	private static final String checkIrbApprovedExistsSql =
			"select rownum from ui_flx_pagever where page_mod_pk=? and page_ver=? and PAGE_MOD_STAT_SUBTYP=?";
	
	public static final String MOD_TABLE_STR = "modTable", MOD_PK_STR = "modPK", PAGE_VER_STR = "pageVer", PAGE_ID_STR = "pageId";
	public static final String PAGE_DEF_STR = "pageDef", PAGE_JS_STR = "pageJs", PAGE_JSON_STR = "pageJson", PAGE_HTML_STR = "pageHtml";
	public static final String FLX_PAGE_COOKIE = "FLX_PAGE_COOKIE";
	public static final String PAGE_DEF = "page_def";
	public static final String PAGE_JS = "page_js";
	public static final String EMPTY_STRING = "";
	public static final String PAGE_MINOR_VER = "page_minor_ver";
	public static final String FK_SUBMISSION = "fk_submission";
	public static final String PAGE_ATTACHDIFF = "page_attachdiff";
	public static final String PAGE_VERDIFF = "page_verdiff";
	public static final String PAGE_FORMRESPONSES = "page_formresponses";
	public static final String PAGE_FORMRESPDIFF = "page_formrespdiff";
	public static final String PAGE_COMMENTS = "page_comments";

	public ResubmDraftDao() {
	}
	
	/**
	 *  Use this for unit-testing
	 */
	public static void main(String[] args) {
		ResubmDraftDao flxDao = new ResubmDraftDao();
		System.setProperty("ERES_HOME", "C:/Velos/eResearch_jboss510/conf/");
		HashMap<String, Object> argMap = new HashMap<String, Object>();
		try {
			String pageId = "protocol";
			JSONObject protocolJson = flxDao.flexPageData(pageId);
			argMap.put(PAGE_ID_STR, pageId);
			argMap.put(PAGE_DEF_STR, protocolJson.get(PAGE_DEF));
			argMap.put(PAGE_JS_STR, protocolJson.get(PAGE_JS));
		} catch (Exception e) {
			e.printStackTrace();
		}
		argMap.put(MOD_TABLE_STR, "er_study");
		argMap.put(MOD_PK_STR, 876);
		argMap.put(PAGE_JSON_STR, "{test:123}");
		argMap.put(PAGE_HTML_STR, "<html></html>");
		int retNum = 0;
		// retNum = flxDao.insertFlexPageVer(argMap);
		retNum = flxDao.updateStatOfLatestVersion("er_study", 1157, "app_CHR");
		System.out.println("retNum="+retNum);
	}
	
	private Integer submissionStatusPk = 0;
	
	public Integer getSubmissionStatusPk() {
		return submissionStatusPk;
	}

	public void insertFlexPage(String pageId) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(insertFlexPageSql);
			pstmt.setString(1, pageId);
			((OraclePreparedStatement) pstmt).setStringForClob(2,
					"{name:\"Study Screen\",sections:[{fields:[]}]}");
			((OraclePreparedStatement) pstmt).setStringForClob(3,
					"var studyScreenJS = {};");

			pstmt.executeUpdate();
		} catch (Exception ex) {
			Rlog.fatal("ResubmDraftDao", "ResubmDraftDao.testInsert EXCEPTION: "
					+ ex);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
			} catch (Exception e) {
			}
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
			}
		}
	}

	public void updateFlexPageCookie(String pageId, HashMap hash) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		String pageDef = (String) hash.get(FLX_PAGE_COOKIE);
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(upateFlexPageSql);
			((OraclePreparedStatement) pstmt).setStringForClob(1, pageDef);
			pstmt.setString(2, pageId);
			pstmt.executeUpdate();
		} catch (Exception ex) {
			Rlog.fatal("ResubmDraftDao", "ResubmDraftDao.testInsert EXCEPTION: "
					+ ex);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
			} catch (Exception e) {
			}
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
			}
		}
	}
	
	public int updateResubmissionDraft(HashMap<String, Object> argMap) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		int retNum = 0;
		int paramSeq=1;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(updateResubmDraftSql);

			((OraclePreparedStatement) pstmt).setStringForClob(paramSeq++, (String)argMap.get(PAGE_DEF_STR));
			((OraclePreparedStatement) pstmt).setStringForClob(paramSeq++, (String)argMap.get(PAGE_JS_STR));
			((OraclePreparedStatement) pstmt).setStringForClob(paramSeq++, (String)argMap.get(PAGE_JSON_STR));
			((OraclePreparedStatement) pstmt).setStringForClob(paramSeq++, (String)argMap.get(PAGE_HTML_STR));
			pstmt.setString(paramSeq++, (String)argMap.get(PAGE_VERDIFF));
			pstmt.setInt(paramSeq++, (Integer)argMap.get(FK_SUBMISSION));
			pstmt.setString(paramSeq++, (String)argMap.get(PAGE_ATTACHDIFF));
			((OraclePreparedStatement) pstmt).setStringForClob(paramSeq++, (String)argMap.get(PAGE_FORMRESPONSES));
			((OraclePreparedStatement) pstmt).setStringForClob(paramSeq++, (String)argMap.get(PAGE_FORMRESPDIFF));
			((OraclePreparedStatement) pstmt).setStringForClob(paramSeq++, (String)argMap.get(PAGE_COMMENTS));
			pstmt.setString(paramSeq++, (String)argMap.get(PAGE_ID_STR));
			pstmt.setString(paramSeq++, (String)argMap.get(MOD_TABLE_STR));
			pstmt.setInt(paramSeq++, (Integer)argMap.get(MOD_PK_STR));
			Integer pageVer = (Integer)argMap.get(PAGE_VER_STR);
			if (pageVer == null) { pageVer = 1; }
			pstmt.setInt(paramSeq++, pageVer);
			Integer pageMinorVer = (Integer)argMap.get(PAGE_MINOR_VER);
			pstmt.setInt(paramSeq++, pageMinorVer);

			retNum = pstmt.executeUpdate();
		} catch (Exception ex) {
			Rlog.fatal("ResubmDraftDao", "ResubmDraftDao.updateResubmissionDraft EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return retNum;
	}

	public int updateResubmissionDraftRetainLastDiff(HashMap<String, Object> argMap) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		int retNum = 0;
		int paramSeq=1;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(updateResubmDraftRetainLastDiffSql);

			((OraclePreparedStatement) pstmt).setStringForClob(paramSeq++, (String)argMap.get(PAGE_DEF_STR));
			((OraclePreparedStatement) pstmt).setStringForClob(paramSeq++, (String)argMap.get(PAGE_JS_STR));
			((OraclePreparedStatement) pstmt).setStringForClob(paramSeq++, (String)argMap.get(PAGE_JSON_STR));
			((OraclePreparedStatement) pstmt).setStringForClob(paramSeq++, (String)argMap.get(PAGE_HTML_STR));;
			pstmt.setInt(paramSeq++, (Integer)argMap.get(FK_SUBMISSION));
			((OraclePreparedStatement) pstmt).setStringForClob(paramSeq++, (String)argMap.get(PAGE_FORMRESPONSES));
			pstmt.setString(paramSeq++, (String)argMap.get(PAGE_ID_STR));
			pstmt.setString(paramSeq++, (String)argMap.get(MOD_TABLE_STR));
			pstmt.setInt(paramSeq++, (Integer)argMap.get(MOD_PK_STR));
			Integer pageVer = (Integer)argMap.get(PAGE_VER_STR);
			if (pageVer == null) { pageVer = 1; }
			pstmt.setInt(paramSeq++, pageVer);
			Integer pageMinorVer = (Integer)argMap.get(PAGE_MINOR_VER);
			pstmt.setInt(paramSeq++, pageMinorVer);

			retNum = pstmt.executeUpdate();
		} catch (Exception ex) {
			Rlog.fatal("ResubmDraftDao", "ResubmDraftDao.updateResubmissionDraft EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return retNum;
	}
	
	public JSONObject getResubmissionDraft(HashMap<String, Object> argMap) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		JSONObject jsResubmDraftObj = new JSONObject();
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(selectResubmDraftSql);
			pstmt.setString(1, (String)argMap.get(PAGE_ID_STR));
			pstmt.setString(2, (String)argMap.get(MOD_TABLE_STR));
			pstmt.setInt(3, (Integer)argMap.get(MOD_PK_STR));
			Integer pageVer = (Integer)argMap.get(PAGE_VER_STR);
			Integer pageMinorVer = (Integer)argMap.get(PAGE_MINOR_VER);
			if (pageVer == null) { pageVer = 1; }
			pstmt.setInt(4, pageVer);
			pstmt.setInt(5, pageMinorVer);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				//PAGE_MOD_TABLE, PAGE_MOD_PK, PAGE_VER, PAGE_ID, PAGE_DEF, PAGE_JS, PAGE_JSON, PAGE_HTML, PAGE_MINOR_VER, 
				//PAGE_VERDIFF, FK_SUBMISSION, PAGE_ATTACHDIFF, PAGE_FORMRESPONSES, PAGE_FORMRESPDIFF, PAGE_COMMENTS
				jsResubmDraftObj.put("PAGE_VER", rs.getString("PAGE_VER"));
				jsResubmDraftObj.put("PAGE_MINOR_VER", rs.getString("PAGE_MINOR_VER"));
				
				jsResubmDraftObj.put("PAGE_JSON", rs.getString("PAGE_JSON"));
				jsResubmDraftObj.put(PAGE_VERDIFF, rs.getString(PAGE_VERDIFF));
				jsResubmDraftObj.put(PAGE_ATTACHDIFF, rs.getString(PAGE_ATTACHDIFF));
				jsResubmDraftObj.put(PAGE_FORMRESPONSES, rs.getString(PAGE_FORMRESPONSES));
				jsResubmDraftObj.put(PAGE_FORMRESPDIFF, rs.getString(PAGE_FORMRESPDIFF));
				jsResubmDraftObj.put(PAGE_COMMENTS, rs.getString(PAGE_COMMENTS));
				return jsResubmDraftObj;
			}
		} catch (Exception ex) {
			Rlog.fatal("ResubmDraftDao", "ResubmDraftDao.getResubmissionDraft EXCEPTION:"+ ex);
			jsResubmDraftObj = null;
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return jsResubmDraftObj;
	}

	public int insertResubmissionDraft(HashMap<String, Object> argMap) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		int retNum = 0;		
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(insertResubmDraftSql);
			pstmt.setString(1, (String)argMap.get(MOD_TABLE_STR));
			pstmt.setInt(2, (Integer)argMap.get(MOD_PK_STR));
			Integer pageVer = (Integer)argMap.get(PAGE_VER_STR);
			Integer pageMinorVer = (Integer)argMap.get(PAGE_MINOR_VER);
			if (pageVer == null) { pageVer = 1; }
			pstmt.setInt(3, pageVer);
			pstmt.setString(4, (String)argMap.get(PAGE_ID_STR));
			((OraclePreparedStatement) pstmt).setStringForClob(5, (String)argMap.get(PAGE_DEF_STR));
			((OraclePreparedStatement) pstmt).setStringForClob(6, (String)argMap.get(PAGE_JS_STR));
			((OraclePreparedStatement) pstmt).setStringForClob(7, (String)argMap.get(PAGE_JSON_STR));
			((OraclePreparedStatement) pstmt).setStringForClob(8, (String)argMap.get(PAGE_HTML_STR));
			pstmt.setInt(9, pageMinorVer);
			pstmt.setString(10, (String)argMap.get(PAGE_VERDIFF));
			pstmt.setInt(11, (Integer)argMap.get(FK_SUBMISSION));
			pstmt.setString(12, (String)argMap.get(PAGE_ATTACHDIFF));
			((OraclePreparedStatement) pstmt).setStringForClob(13, (String)argMap.get(PAGE_FORMRESPONSES));
			((OraclePreparedStatement) pstmt).setStringForClob(14, (String)argMap.get(PAGE_FORMRESPDIFF));
			((OraclePreparedStatement) pstmt).setStringForClob(15, (String)argMap.get(PAGE_COMMENTS));
			retNum = pstmt.executeUpdate();
		} catch (Exception ex) {
			Rlog.fatal("ResubmDraftDao", "ResubmDraftDao.insertResubmissionDraft EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return retNum;
	}
	
	public int getHighestResubmDraftVersion(String tablename, int tablePk) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		int retNum = 0;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(getHighestResubmDraftVersionSql);
			pstmt.setString(1, (String)tablename);
			pstmt.setInt(2, (Integer)tablePk);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				retNum = rs.getInt(1);
			}
		} catch (Exception ex) {
			Rlog.fatal("ResubmDraftDao", "ResubmDraftDao.getHighestFlexPageVersion EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return retNum;
	}

	public String getResubmDraftPageHtmlOfHighestVersion(String tablename, int tablePk) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		String html = EMPTY_STRING;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(getResubmDraftPageHtmlOfHighestVersionSql);
			pstmt.setString(1, (String)tablename);
			pstmt.setInt(2, (Integer)tablePk);
			pstmt.setString(3, (String)tablename);
			pstmt.setInt(4, (Integer)tablePk);
			pstmt.setString(5, (String)tablename);
			pstmt.setInt(6, (Integer)tablePk);
			pstmt.setString(7, (String)tablename);
			pstmt.setInt(8, (Integer)tablePk);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				html = rs.getString(1);
			}
		} catch (Exception ex) {
			Rlog.fatal("ResubmDraftDao", "ResubmDraftDao.getResubmDraftPageHtmlOfHighestVersion EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return html;
	}
	
	public int getFlexLatestSubmissionStatus(int fkStudy) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		int submPk = 0;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(getFlexPageLatestSubmissionStatusSql);
			pstmt.setInt(1, fkStudy);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				submPk = rs.getInt(1);
				submissionStatusPk = rs.getInt(2);
			}
		} catch (Exception ex) {
			Rlog.fatal("ResubmDraftDao", "ResubmDraftDao.getFlexLatestSubmissionStatus EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return submPk;
	}
	
	public JSONObject getFlxPageCookie(JSONObject jsObjPageRec) {
		JSONObject newJSONObj = null;
		try {
			newJSONObj = flexPageConfigJson(jsObjPageRec);
		} catch (Exception ex) {
			Rlog.fatal("ResubmDraftDao",
					"ResubmDraftDao.getFlxPageCookie EXCEPTION: " + ex);
		}
		if (newJSONObj == null) {
			newJSONObj = new JSONObject();
		}
		return newJSONObj;
	}

	public String getFlxPageJS(JSONObject jsObjPageRec) {
		String newFlexPageJS = null;
		try {
			newFlexPageJS = flexPageJS(jsObjPageRec);
		} catch (Exception ex) {
			Rlog.fatal("ResubmDraftDao",
					"ResubmDraftDao.getFlxPageJS EXCEPTION: " + ex);
		}
		return newFlexPageJS;
	}
	
	public JSONObject flexPageData(String pageId) throws Exception {
		JSONObject jsObjPageRec = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(getFlexPageCookie);
			pstmt.setString(1, pageId);

			ResultSet rs = pstmt.executeQuery();
			if (rs == null) {
				return jsObjPageRec;
			}
			while (rs.next()) {
				jsObjPageRec = new JSONObject();

				String myPageDef = rs.getString(PAGE_DEF);
				jsObjPageRec.put(PAGE_DEF, myPageDef);
				
				String myPageJS = rs.getString(PAGE_JS);
				jsObjPageRec.put(PAGE_JS, myPageJS);
				return jsObjPageRec;
			}
		} catch (Exception ex) {
			Rlog.fatal("ResubmDraftDao",
					"ResubmDraftDao.flexPageData EXCEPTION: " + ex);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
			} catch (Exception e) {
			}
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
			}
		}
		return jsObjPageRec;
	}

	private JSONObject flexPageConfigJson(JSONObject jsObj) throws Exception {
		JSONObject newJSONObj = null;
		if (jsObj == null) {
			return newJSONObj;
		}
		String myPageDef = jsObj.getString(PAGE_DEF);
		newJSONObj = new JSONObject(myPageDef);
		return newJSONObj;
	}

	private String flexPageJS(JSONObject jsObj) throws Exception {
		String newFlexPageJS = null;
		if(jsObj == null) {
			return newFlexPageJS;
		}

		newFlexPageJS = jsObj.getString(PAGE_JS);
		return newFlexPageJS;
	}

	public int getHighestMinorResubmDraftVersion(int version, String tablename, int tablePk) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		int retNum = 0;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(getHighestMinorResubmDraftVersionSql);
			pstmt.setString(1, (String)tablename);
			pstmt.setInt(2, (Integer)tablePk);
			pstmt.setInt(3, version);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				retNum = rs.getInt(1);
			}
		} catch (Exception ex) {
			Rlog.fatal("ResubmDraftDao", "ResubmDraftDao.getHighestMinorFlexPageVersion EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return retNum;
	}
	
	private Integer majorVer = 0;
	private Integer minorVer = 0;
	private String comments = "";
	private Integer submissionPk = 0;
	public Integer getMajorVer() { return majorVer; }
	public Integer getMinorVer() { return minorVer; }
	public Integer getSubmissionPk() { return submissionPk; }
	public String getComments() { return comments; }

	public void getHighestResubmDraftFullVersion(String tablename, int tablePk) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		majorVer = 0;
		minorVer = 0;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(getHighestResubmDraftFullVersionSql);
			pstmt.setString(1, (String)tablename);
			pstmt.setInt(2, (Integer)tablePk);			
			rs = pstmt.executeQuery();
			while(rs.next()) {
				majorVer = rs.getInt(1);
				minorVer = rs.getInt(2);
				submissionPk = rs.getInt(3);
				if (submissionPk == null) {
					submissionPk = 0;
				}
				comments = rs.getString(4);
				break;
			}
		} catch (Exception ex) {
			Rlog.fatal("ResubmDraftDao", "ResubmDraftDao.getHighestResubmDraftFullVersion EXCEPTION:"+ ex);
			majorVer = 0;
			minorVer = 0;
			submissionPk = 0;
			comments = EMPTY_STRING;
		} finally {
			try { if (rs != null) { rs.close(); } } catch (Exception e) {}
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
	}

	public String getPageVersionDetails(String tablename, int studyId, String studyVersion) {
		String json = "";
		String[] studyVersionString;
		int page_ver = -1;
		int page_minor_ver = -1;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try {

			if (studyVersion.contains(".")) {
				studyVersionString = studyVersion.split("\\.");
				if (studyVersionString.length == 2) {
					page_ver = Integer.parseInt(studyVersionString[0]);
					page_minor_ver = Integer.parseInt(studyVersionString[1]);
				}
			} else {
				page_ver = Integer.parseInt(studyVersion);
			}

			conn = getConnection();

			String mysql = getResubmDraftVersionJSONSql;
			if (page_minor_ver != -1) {
				mysql += " and page_minor_ver = ?";
			}

			pstmt = conn.prepareStatement(mysql);
			pstmt.setString(1, (String)tablename);
			pstmt.setInt(2, studyId);
			pstmt.setInt(3, page_ver);
			if (page_minor_ver != -1) {
				pstmt.setInt(4, page_minor_ver);
			}
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				json = rs.getString("PAGE_JSON");
			}

		} catch (SQLException e) {
			Rlog.fatal("ResubmDraftDao", "ResubmDraftDao.getPageVersionDetails EXCEPTION:"+ e);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}
		return json;
	}
	
	public String getResubmDraftFormResponses(String tablename, int studyId, String studyVersion) {
		String jsonArray = "";
		String[] studyVersionString;
		int page_ver = -1;
		int page_minor_ver = -1;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try {

			if (studyVersion.contains(".")) {
				studyVersionString = studyVersion.split("\\.");
				if (studyVersionString.length == 2) {
					page_ver = Integer.parseInt(studyVersionString[0]);
					page_minor_ver = Integer.parseInt(studyVersionString[1]);
				}
			} else {
				page_ver = Integer.parseInt(studyVersion);
			}

			conn = getConnection();

			String mysql = getResubmDraftFormResponsesSql;
			if (page_minor_ver != -1) {
				mysql += " and page_minor_ver = ?";
			}

			pstmt = conn.prepareStatement(mysql);
			pstmt.setString(1, (String)tablename);
			pstmt.setInt(2, studyId);
			pstmt.setInt(3, page_ver);
			if (page_minor_ver != -1) {
				pstmt.setInt(4, page_minor_ver);
			}
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				jsonArray = rs.getString(PAGE_FORMRESPONSES);
			}

		} catch (SQLException e) {
			Rlog.fatal("ResubmDraftDao", "ResubmDraftDao.getPageVersionDetails EXCEPTION:"+ e);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}
		return jsonArray;
	}

	public int chkPageProtocolFrozen(String tableName, int studyId, int major, int minor) {
		int cntProtocolFrozen = -1;
		this.getHighestResubmDraftFullVersion(tableName, studyId);
		int highestVersion = majorVer;
		if(highestVersion == 0)
		{
			cntProtocolFrozen = -1;
		}
		else
		{
			int count = 0;
			PreparedStatement pstmt = null;
			Connection conn = null;
			String statSubtyp = null;
			try {

				conn = getConnection();
				pstmt = conn.prepareStatement(chkPageProtocolFrozen);
				pstmt.setString(1, tableName);
				pstmt.setInt(2, studyId);
				pstmt.setInt(3, major);
				pstmt.setInt(4, minor);
				ResultSet rs = pstmt.executeQuery();
				while (rs.next()) {
					count++;
					statSubtyp = rs.getString(1);
				}

			}  catch (Exception e) {
				Rlog.fatal("ResubmDraftDao", "ResubmDraftDao.chkPageProtocolFrozen EXCEPTION:"+ e);
			} finally {
				try {
					if (pstmt != null)
						pstmt.close();
				} catch (Exception e) {
				}
				try {
					if (conn != null)
						conn.close();
				} catch (Exception e) {
				}
			}
			/*if (!StringUtil.isEmpty(statSubtyp)) {
				cntProtocolFrozen = 2; // = Submitted / IRB Approved / IRB Disapproved
			} else if (count > 0) {
				cntProtocolFrozen = 1; // = Archive exists
			} else {
				cntProtocolFrozen = 0; // = No archive
			}*/
			cntProtocolFrozen = count;
		}
		return cntProtocolFrozen;
	}

	
	public int updateCurrentResubmDraftChanges(String tableName, int studyId, String changelog, String attachLog,
			String formRespLog,	String comments) {
		this.getHighestResubmDraftFullVersion(tableName, studyId);
		PreparedStatement pstmt = null;
		Connection conn = null;
		int retNum = 0;
		int paramSeq = 1;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(updateCurrentResubmDraftChangesSql);
			pstmt.setString(paramSeq++, changelog);
			pstmt.setString(paramSeq++, attachLog);
			pstmt.setString(paramSeq++, formRespLog);
			pstmt.setString(paramSeq++, comments);
			pstmt.setString(paramSeq++, tableName);
			pstmt.setInt(paramSeq++, studyId);
			pstmt.setInt(paramSeq++, majorVer);
			pstmt.setInt(paramSeq++, minorVer);
			retNum = pstmt.executeUpdate();
			
		} catch (Exception ex) {
			Rlog.fatal("ResubmDraftDao", "ResubmDraftDao.updateCurrentResubmDraftChanges EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return retNum;
	}
	
	public int updateStatOfLatestVersion(String tableName, int studyId, String statSubtyp) {
		this.getHighestResubmDraftFullVersion(tableName, studyId);
		if (majorVer < 1 || submissionPk < 1) {
			return -1;
		}
		if (StringUtil.isEmpty(statSubtyp) || statSubtyp.length() > 15) {
			return -2;
		}
		int retNum = 0;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(updateStatOfLatestVersionSql);
			pstmt.setString(1, statSubtyp);
			pstmt.setString(2, tableName);
			pstmt.setInt(3, studyId);
			pstmt.setInt(4, majorVer);
			pstmt.setInt(5, minorVer);
			retNum = pstmt.executeUpdate();
		} catch (Exception ex) {
			Rlog.fatal("ResubmDraftDao", "ResubmDraftDao.updateStatOfLatestVersion EXCEPTION:"+ ex);
			retNum = -3;
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return retNum;
	}
	
	public int checkLockedVersionExists(int studyId, int verNum) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		int retNum = 0;
		try {
			conn = getConnection();
			String Sql = "select rownum from ui_flx_pagever where page_mod_pk=? and page_ver=? and page_minor_ver=0";
			pstmt = conn.prepareStatement(Sql);
			pstmt.setInt(1, studyId);
			pstmt.setInt(2, verNum);			
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				retNum = rs.getInt("rownum");
			}
			
		} catch (Exception ex) {
			Rlog.fatal("ResubmDraftDao", "ResubmDraftDao.checkLockedVersionExists EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return retNum;
	}
	
	public int checkIrbApprovedExists(int studyId, int verNum) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		int retNum = 0;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(checkIrbApprovedExistsSql);
			pstmt.setInt(1, studyId);
			pstmt.setInt(2, verNum);
			pstmt.setString(3, CFG.EIRB_APPROVED_SUBTYP);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				retNum = rs.getInt("rownum");
			}
			
		} catch (Exception ex) {
			Rlog.fatal("ResubmDraftDao", "ResubmDraftDao.checkIrbApprovedExists EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return retNum;
	}
	
	public int getApprovedVersionForStudy(int studyId) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		int retNum = 0;
		try {
			conn = getConnection();
			String Sql = "select page_ver from ui_flx_pagever where page_mod_pk=? and page_minor_ver=0 order by page_ver ASC";
			pstmt = conn.prepareStatement(Sql);
			pstmt.setInt(1, studyId);
					
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				retNum = rs.getInt("page_ver");
			}
			
		} catch (Exception ex) {
			Rlog.fatal("ResubmDraftDao", "ResubmDraftDao.getApprovedVersionForStudy EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return retNum;
	}
	
	public String getResubmDraftVersionDiffDetails(String tablename, int tablePk, int pageVer, int pageMinorVer) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		String verDiff = EMPTY_STRING;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(getResubmDraftVersionDiffDetailsSql);
			pstmt.setString(1, (String)tablename);
			pstmt.setInt(2, (Integer)tablePk);
			pstmt.setInt(3, pageVer);
			pstmt.setInt(4, pageMinorVer);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				verDiff = rs.getString(1);
			}
		} catch (Exception ex) {
			Rlog.fatal("ResubmDraftDao", "ResubmDraftDao.getFlexPageVersionDiffDetails EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return verDiff;
	}
	
	public String getResubmDraftAttachVersionDiffDetails(String tablename, int tablePk, int pageVer, int pageMinorVer) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		String verDiff = EMPTY_STRING;
		try {
			conn = getConnection();
			String sql = "select page_attachdiff from er_resubm_draft where " +
			" PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ? "+
			" and PAGE_VER = ? and PAGE_MINOR_VER = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, (String)tablename);
			pstmt.setInt(2, (Integer)tablePk);
			pstmt.setInt(3, pageVer);
			pstmt.setInt(4, pageMinorVer);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				verDiff = rs.getString(1);
			}
		} catch (Exception ex) {
			Rlog.fatal("ResubmDraftDao", "ResubmDraftDao.getResubmDraftAttachVersionDiffDetails EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return verDiff;
	}
	
	public int saveResubmissionDraft(HashMap<String, Object> paramMap){
		int retInt = -1;
		HttpServletRequest request = (HttpServletRequest) paramMap.get("request"); if (null == request) return retInt;
		SessionMaint sessionMaint = new SessionMaint();
		HttpSession tSession =  request.getSession(true); if (null == tSession || !sessionMaint.isValidSession(tSession)) return retInt;
		
		String accIdStr = (String) tSession.getAttribute("accountId");
		if (StringUtil.stringToNum(accIdStr) <= 0) return retInt;
		String studyIdStr = (String) tSession.getAttribute("studyId");
		int studyId = StringUtil.stringToNum(studyIdStr);
		if (studyId <= 0) return retInt;
		String usr = (String) tSession.getAttribute("userId");
		int iusr = StringUtil.stringToNum(usr);

		String comments = StringUtil.htmlEncodeXss(request.getParameter("comments"));
		String fieldDetails = StringEscapeUtils.unescapeHtml(StringUtil.htmlEncodeXss(request.getParameter("fieldDetails")));
		String attachFieldDetails = StringEscapeUtils.unescapeHtml(StringUtil.htmlEncodeXss(request.getParameter("attachFieldDetails")));
		if(attachFieldDetails == null) {
			attachFieldDetails="[]";
		}
		String formRespDetails = StringEscapeUtils.unescapeHtml(StringUtil.htmlEncodeXss(request.getParameter("formRespDetails")));
		if(formRespDetails == null) {
			formRespDetails="[]";
		}
		fieldDetails= StringUtil.decodeString(fieldDetails);
		attachFieldDetails=StringUtil.decodeString(attachFieldDetails);
		formRespDetails= StringUtil.decodeString(formRespDetails);
	    String resultMsg = null;
	    String fieldDetailsString = "[]"; 
        if (fieldDetails != null) {
            JSONArray fieldDetailsArray = null;
			try {
				fieldDetailsArray = new JSONArray(fieldDetails);
				 int noOfFields = fieldDetailsArray.length();
		            for (int i = 0; i < noOfFields; i++) {
		            	JSONObject jsDiffObj = fieldDetailsArray.getJSONObject(i);
		            	String fieldLabel = jsDiffObj.getString("fieldLabel");
		            	String fieldNotes = StringEscapeUtils.unescapeHtml(StringUtil.htmlEncodeXss(request.getParameter(fieldLabel + "_" + "notes")));
		            	fieldNotes = (StringUtil.isEmpty(fieldNotes)) ? "" : fieldNotes;
		            	jsDiffObj.put("fieldNotes", fieldNotes);
		            	
		            	String fieldBoard = StringEscapeUtils.unescapeHtml(StringUtil.htmlEncodeXss(request.getParameter(fieldLabel + "_" + "revBoard")));
		            	fieldBoard = (StringUtil.isEmpty(fieldBoard)) ? "" : fieldBoard;
		            	jsDiffObj.put("fieldBoard", fieldBoard);

		            	jsDiffObj.remove("fieldLabel");
		            	jsDiffObj.remove("fieldRow");
		            }
			} catch (JSONException e) {
				e.printStackTrace();
			}
           
            fieldDetailsString = fieldDetailsArray.toString();
        }

        if (attachFieldDetails != null && !"[]".equals(attachFieldDetails)) {
            JSONArray attachDetailsArray = null;
			try {
				attachDetailsArray = new JSONArray(attachFieldDetails);
				int noOfFields = attachDetailsArray.length();
	            for (int i = 0; i < noOfFields; i++) {
		            try {
		            	JSONObject jsDiffObj = attachDetailsArray.getJSONObject(i);
		            	String fieldKey = jsDiffObj.getString("field");
		            	String fieldLabel = jsDiffObj.getString("fieldLabel");
		            	if (StringUtil.isEmpty(fieldKey) || StringUtil.isEmpty(fieldLabel)) continue;
		            	String fieldNotes = "", fieldBoard = "";
		            	
		            	if (FlxPageArchive.NEWATTACHMENT_STR.equals(fieldKey)){
			            	fieldNotes = StringEscapeUtils.unescapeHtml(StringUtil.htmlEncodeXss(request.getParameter(fieldLabel + "_" + "notes1")));
			            	fieldBoard = StringEscapeUtils.unescapeHtml(StringUtil.htmlEncodeXss(request.getParameter(fieldLabel + "_" + "revBoard1")));
		            	} 
		            	if (FlxPageArchive.DELETEDATTACHMENT_STR.equals(fieldKey)){
		            		fieldNotes = StringEscapeUtils.unescapeHtml(StringUtil.htmlEncodeXss(request.getParameter(fieldLabel + "_" + "notes2")));
			            	fieldBoard = StringEscapeUtils.unescapeHtml(StringUtil.htmlEncodeXss(request.getParameter(fieldLabel + "_" + "revBoard2")));
		            	}
		            	fieldNotes = (StringUtil.isEmpty(fieldNotes)) ? "" : fieldNotes;
		            	fieldBoard = (StringUtil.isEmpty(fieldBoard)) ? "" : fieldBoard;
		            	try {
							jsDiffObj.put("fieldNotes", fieldNotes);
						} catch (JSONException e) {
							e.printStackTrace();
						}
	
		            	try {
							jsDiffObj.put("fieldBoard", fieldBoard);
						} catch (JSONException e) {
							e.printStackTrace();
						}
	
		            	jsDiffObj.remove("fieldLabel");
		            	jsDiffObj.remove("fieldRow");
		            } catch (JSONException e2) {
		            	
		            }
	            }
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
            
            attachFieldDetails = attachDetailsArray.toString();
        }
        
        String respDetailsString = "[]"; 
        if (formRespDetails != null && !"[]".equals(formRespDetails)) {
            JSONArray respDetailsArray = null;
			try {
				respDetailsArray = new JSONArray(formRespDetails);
				 int noOfFields = respDetailsArray.length();
		            for (int i = 0; i < noOfFields; i++) {
		            	JSONObject jsDiffObj = respDetailsArray.getJSONObject(i);
		            	String fieldLabel = jsDiffObj.getString("fieldLabel");
		            	String fieldNotes = StringEscapeUtils.unescapeHtml(StringUtil.htmlEncodeXss(request.getParameter(fieldLabel + "_" + "notes")));
		            	fieldNotes = (StringUtil.isEmpty(fieldNotes)) ? "" : fieldNotes;
		            	jsDiffObj.put("fieldNotes", fieldNotes);
		            	
		            	String fieldBoard = StringEscapeUtils.unescapeHtml(StringUtil.htmlEncodeXss(request.getParameter(fieldLabel + "_" + "revBoard")));
		            	fieldBoard = (StringUtil.isEmpty(fieldBoard)) ? "" : fieldBoard;
		            	jsDiffObj.put("fieldBoard", fieldBoard);

		            	jsDiffObj.remove("fieldLabel");
		            	jsDiffObj.remove("fieldRow");
		            }
			} catch (JSONException e) {
				e.printStackTrace();
			}
           
			respDetailsString = respDetailsArray.toString();
        }

	    ResubmDraftDao resubmDraftDao = new ResubmDraftDao();
        resubmDraftDao.updateCurrentResubmDraftChanges("er_study", studyId, fieldDetailsString, attachFieldDetails, respDetailsString, comments);
        
        retInt = resubmDraftDao.updateStatOfLatestVersion("er_study", studyId, "submitting"); // marker to show this version was submitted
        return retInt;
	}
	
	public String getResubmDraftFormRespVersionDiffDetails(String tablename, int tablePk, int pageVer, int pageMinorVer) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		String verDiff = EMPTY_STRING;
		try {
			conn = getConnection();
			String sql = "select PAGE_FORMRESPDIFF from er_resubm_draft where " +
			" PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ? "+
			" and PAGE_VER = ? and PAGE_MINOR_VER = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, (String)tablename);
			pstmt.setInt(2, (Integer)tablePk);
			pstmt.setInt(3, pageVer);
			pstmt.setInt(4, pageMinorVer);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				verDiff = rs.getString(1);
			}
		} catch (Exception ex) {
			Rlog.fatal("ResubmDraftDao", "ResubmDraftDao.getResubmDraftFormRespVersionDiffDetails EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return verDiff;
	}
}
