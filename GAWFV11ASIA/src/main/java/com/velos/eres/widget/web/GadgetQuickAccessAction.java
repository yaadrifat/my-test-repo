package com.velos.eres.widget.web;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.aithent.audittrail.reports.AuditUtils;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.MC;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.grpRights.GrpRightsJB;

public class GadgetQuickAccessAction extends ActionSupport {
	private static final long serialVersionUID = -3017353659165410425L;
	private HttpServletRequest request = null;
	private HttpSession tSession = null;
	private GadgetQuickAccessJB gadgetQuickAccessJB = null;
	private Map<String, List<String>> inputErrors = null;
	private Collection<String> operationErrors = null;
	private LinkedHashMap<String, Object> jsonData = null;
	
	private static final String ESIGN_STR = "eSign";
	private static final String USER_ID = "userId";
	private static final String ACCOUNT_ID = "accountId";
	private static final String IP_ADD = "ipAdd";
	private static final String EMPTY_STR = "";
	private static final String GRIGHTS_STR = "GRights";
	private static final String MACCLINKS = "MACCLINKS";
	private static final String ACTFRMSACC = "ACTFRMSACC";
	private static final String LAST_HOME_ACCESS = "LAST_HOME_ACCESS";
	private static final String LANDING_PAGE = "landingPage";
	
	public GadgetQuickAccessAction() {
		ActionContext ac = ActionContext.getContext();
		request = (HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
		tSession = request.getSession(false);
		tSession.setAttribute(LAST_HOME_ACCESS, LANDING_PAGE);
		gadgetQuickAccessJB = new GadgetQuickAccessJB();
		GrpRightsJB grpRights = (GrpRightsJB) tSession.getAttribute(GRIGHTS_STR);
		gadgetQuickAccessJB.setAcctLinksAccessRights(
				Integer.parseInt(grpRights.getFtrRightsByValue(MACCLINKS)));
		gadgetQuickAccessJB.setAcctFormsAccessRights(
				Integer.parseInt(grpRights.getFtrRightsByValue(ACTFRMSACC)));
	}
	
	public Map<String, List<String>> getInputErrors() { return this.inputErrors; }
	
	public Collection<String> getOperationErrors() { return this.operationErrors; }
	
	public LinkedHashMap<String, Object> getJsonData() { return jsonData; }

	public String getGadgetQuickAccess() {
		gadgetQuickAccessJB.loadStudyDD(this.getUserIdInSession());
		return SUCCESS;
	}
	
	public String getUserIdInSession() {
		String userId = null;
		try {
			userId = (String) request.getSession(false).getAttribute(USER_ID);
		} finally {
			userId = StringUtil.trueValue(userId);
		}
		return userId;
	}
	
	private void validateESign(String eSign, String target) {
		if (StringUtil.isEmpty(eSign) ||
				!eSign.equals(request.getSession(false).getAttribute(ESIGN_STR))) {
			addFieldError(target, MC.M_IncorrEsign_EtrAgain);
		}
	}
	
	public String getMyLinksData() {
		jsonData = gadgetQuickAccessJB.getMyLinksData(this.getUserIdInSession());
		return SUCCESS;
	}
	
	public String getAcctLinksData() {
		jsonData = gadgetQuickAccessJB.getAcctLinksData(
				(String)tSession.getAttribute(ACCOUNT_ID), GadgetQuickAccessJB.LNK_GEN);
		return SUCCESS;
	}
	
	public GadgetQuickAccessJB getGadgetQuickAccessJB() { return gadgetQuickAccessJB; }
	
	public void setGadgetQuickAccessJB(GadgetQuickAccessJB gadgetQuickAccessJB) {
		this.gadgetQuickAccessJB = gadgetQuickAccessJB;
	}
	
	public String deleteMyLink() {
		String userId = this.getUserIdInSession();
		if (StringUtil.isEmpty(userId)) { return null; }
		
		// If eSign is wrong, don't process the rest of the request
		validateESign(gadgetQuickAccessJB.getMyLinksDeleteESign(), GadgetQuickAccessJB.FLD_MY_LINKS_DELETE_ESIGN);
		if (hasFieldErrors()) {
			this.inputErrors = getFieldErrors();
			return INPUT;
		}
		
		// Do field-by-field validation in the JB
		gadgetQuickAccessJB.validateMyLinksForDelete(this);
		if (hasFieldErrors()) {
			this.inputErrors = getFieldErrors();
			return INPUT;
		}
		
		// Do the actual delete
		int resultFromEJB = gadgetQuickAccessJB.deleteUlinkForMyLinks(
				AuditUtils.createArgs(request.getSession(false), EMPTY_STR, LC.L_Manage_Acc));
		if (resultFromEJB < 0 || hasActionErrors()) {
			addActionError(MC.M_Err_SavingData);
			this.operationErrors = getActionErrors();
			return ERROR;
		}
		
		return SUCCESS;
	}
	
	public String saveMyLink() {
		String userId = this.getUserIdInSession();
		if (StringUtil.isEmpty(userId)) { return null; }
		
		// If eSign is wrong, don't process the rest of the request
		validateESign(gadgetQuickAccessJB.getMyLinksESign(), GadgetQuickAccessJB.FLD_MY_LINKS_ESIGN);
		if (hasFieldErrors()) {
			this.inputErrors = getFieldErrors();
			return INPUT;
		}
		
		// Do field-by-field validation in the JB
		gadgetQuickAccessJB.validateMyLinksForSave(this);
		if (hasFieldErrors()) {
			this.inputErrors = getFieldErrors();
			return INPUT;
		}
		
		// Do the actual save
		gadgetQuickAccessJB.setAccountId((String)tSession.getAttribute(ACCOUNT_ID));
		gadgetQuickAccessJB.setIpAdd((String)tSession.getAttribute(IP_ADD));
		gadgetQuickAccessJB.setUserId(getUserIdInSession());
		int resultFromEJB = gadgetQuickAccessJB.saveUlinkForMyLinks();
		if (resultFromEJB < 0 || hasActionErrors()) {
			addActionError(MC.M_Err_SavingData);
			this.operationErrors = getActionErrors();
			return ERROR;
		}

		return SUCCESS;
	}
	
	public String deleteAcctLink() {
		String userId = this.getUserIdInSession();
		if (StringUtil.isEmpty(userId)) { return null; }
		
		// If eSign is wrong, don't process the rest of the request
		validateESign(gadgetQuickAccessJB.getAcctLinksDeleteESign(), GadgetQuickAccessJB.FLD_ACCT_LINKS_DELETE_ESIGN);
		if (hasFieldErrors()) {
			this.inputErrors = getFieldErrors();
			return INPUT;
		}
		
		// Do field-by-field validation in the JB
		gadgetQuickAccessJB.validateAcctLinksForDelete(this);
		if (hasFieldErrors()) {
			this.inputErrors = getFieldErrors();
			return INPUT;
		}
		
		// Do the actual delete
		int resultFromEJB = gadgetQuickAccessJB.deleteUlinkForAcctLinks(this,
				AuditUtils.createArgs(request.getSession(false), EMPTY_STR, LC.L_Manage_Acc));
		if (resultFromEJB < 0 || hasActionErrors()) {
			addActionError(MC.M_Err_SavingData);
			this.operationErrors = getActionErrors();
			return ERROR;
		}
		
		return SUCCESS;
	}
	
	public String saveAcctLink() {
		String userId = this.getUserIdInSession();
		if (StringUtil.isEmpty(userId)) { return null; }
		
		// If eSign is wrong, don't process the rest of the request
		validateESign(gadgetQuickAccessJB.getAcctLinksESign(), GadgetQuickAccessJB.FLD_ACCT_LINKS_ESIGN);
		if (hasFieldErrors()) {
			this.inputErrors = getFieldErrors();
			return INPUT;
		}
		
		// Do field-by-field validation in the JB
		gadgetQuickAccessJB.validateAcctLinksForSave(this);
		if (hasFieldErrors()) {
			this.inputErrors = getFieldErrors();
			return INPUT;
		}
		
		// Do the actual save
		gadgetQuickAccessJB.setAccountId((String)tSession.getAttribute(ACCOUNT_ID));
		gadgetQuickAccessJB.setIpAdd((String)tSession.getAttribute(IP_ADD));
		gadgetQuickAccessJB.setUserId(getUserIdInSession());
		int resultFromEJB = gadgetQuickAccessJB.saveUlinkForAcctLinks(this);
		if (resultFromEJB < 0 || hasActionErrors()) {
			addActionError(MC.M_Err_SavingData);
			this.operationErrors = getActionErrors();
			return ERROR;
		}

		return SUCCESS;
	}

}
