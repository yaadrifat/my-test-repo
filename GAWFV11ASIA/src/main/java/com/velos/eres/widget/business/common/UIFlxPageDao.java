package com.velos.eres.widget.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import oracle.jdbc.OraclePreparedStatement;

import org.apache.commons.lang.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.service.util.CFG;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

public class UIFlxPageDao extends CommonDAO implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private static final String getFlexPageCookie = "select page_def, page_js from UI_FLX_PAGE where PAGE_ID=?";
	//private static final String getFlexPageJS = "select page_js from UI_FLX_PAGE where PAGE_ID=?";
	private static final String insertFlexPageSql = "insert into UI_FLX_PAGE(PAGE_ID, PAGE_DEF, PAGE_JS) values (?,?,?) ";
	private static final String upateFlexPageSql = "update UI_FLX_PAGE set PAGE_DEF = ? where PAGE_ID = ?";
	private static final String insertFlexPageVerSql = "insert into UI_FLX_PAGEVER "+
			" (PAGE_MOD_TABLE, PAGE_MOD_PK, PAGE_VER, PAGE_ID, PAGE_DEF, PAGE_JS, PAGE_JSON, PAGE_HTML, PAGE_MINOR_VER, PAGE_VERDIFF, FK_SUBMISSION, PAGE_ATTACHDIFF, PAGE_FORMRESPONSES, PAGE_FORMRESPDIFF) values "+
			" (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
	private static final String getHighestFlexPageVersionSql = "select nvl(max(PAGE_VER), 0) from UI_FLX_PAGEVER where "+
			"PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ? ";
	private static final String getHighestMinorFlexPageVersionSql = "select nvl(max(PAGE_MINOR_VER), 0) from UI_FLX_PAGEVER where "+
			"PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ? and page_ver= ?";
	private static final String getFlexPageHtmlOfHighestVersionSql = 
			"select PAGE_HTML, PAGE_VER, PAGE_MINOR_VER from UI_FLX_PAGEVER where "+
			" PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ? "+
			" and PAGE_VER = (select nvl(max(PAGE_VER), 0) from UI_FLX_PAGEVER where "+
			" PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ?) "+
			" and PAGE_MINOR_VER = (select nvl(max(PAGE_MINOR_VER), 0) from UI_FLX_PAGEVER where "+
			" PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ? and PAGE_VER = ("+
			" select max(PAGE_VER) from UI_FLX_PAGEVER where PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ? "+
			" )) ";
	private static final String getFlexPageHtmlOfHighestApprovedVersionSql = 
			"select PAGE_HTML, html.PAGE_VER, html.PAGE_MINOR_VER from UI_FLX_PAGEVER html, "
			+ "(select minor.PAGE_VER major_ver, max(PAGE_MINOR_VER) minor_ver from UI_FLX_PAGEVER minor "
			+ "where minor.PAGE_MOD_TABLE = ? and minor.PAGE_MOD_PK = ? "
			+ "and minor.PAGE_MOD_STAT_SUBTYP = '"+ CFG.EIRB_APPROVED_SUBTYP +"' and minor.PAGE_VER in ( "
			+ "select max(major.PAGE_VER) from UI_FLX_PAGEVER major where "
			+ "major.PAGE_MOD_TABLE = ? and major.PAGE_MOD_PK = ? and major.PAGE_MOD_STAT_SUBTYP = '"+ CFG.EIRB_APPROVED_SUBTYP +"') "
			+ "group by minor.PAGE_VER) t "
			+ "where html.PAGE_VER = t.major_ver and html.PAGE_MINOR_VER = t.minor_ver "
			+ "and html.PAGE_MOD_TABLE = ? and html.PAGE_MOD_PK = ? and html.PAGE_MOD_STAT_SUBTYP = '"+ CFG.EIRB_APPROVED_SUBTYP +"'";
	private static final String getFlexPageVerOfHighestApprovedVersionSql = 
			"select max(PAGE_VER) from UI_FLX_PAGEVER where "+
			" PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ? "+
			" and PAGE_MINOR_VER = 0";
	private static final String getHighestFlexPageFullVersionSql = 
			"select PAGE_VER, PAGE_MINOR_VER, FK_SUBMISSION from UI_FLX_PAGEVER where PAGE_MOD_TABLE = ? "+ 
			" and PAGE_MOD_PK = ? order by PAGE_VER desc, PAGE_MINOR_VER desc nulls last  ";
	
	private static final String getFlexPageLatestSubmissionStatusSql = 
			"SELECT SUBMISSION_STATUS, PK_SUBMISSION_STATUS FROM ER_SUBMISSION_STATUS WHERE PK_SUBMISSION_STATUS = "+
            " (SELECT MAX (PK_SUBMISSION_STATUS) FROM ER_SUBMISSION_STATUS "+
            " WHERE SUBMISSION_STATUS IS NOT NULL AND FK_SUBMISSION = "+
            " (SELECT PK_SUBMISSION from (SELECT PK_SUBMISSION FROM ER_SUBMISSION WHERE FK_STUDY = ? ORDER BY PK_SUBMISSION DESC) where ROWNUM <= 1)) ";
	private static final String getDetailedFlexPageSql =
			" select * from ui_flx_pagever where page_mod_pk=? and page_ver= ? and page_minor_ver=? ";	
	
	private static final String getFlexPageVersionJsonSql = "SELECT PAGE_JSON FROM ui_flx_pagever WHERE PAGE_MOD_TABLE = ? and page_mod_pk =? and page_ver = ?";

	private static final String getFlexPageVersionFormResponsesSql = "SELECT PAGE_FORMRESPONSES FROM ui_flx_pagever WHERE PAGE_MOD_TABLE = ? and page_mod_pk =? and page_ver = ?";

	private static final String chkPageVersionSubmited = "select PAGE_MOD_STAT_SUBTYP, PAGE_MOD_TABLE from  UI_FLX_PAGEVER where "
			+ " PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ? and PAGE_VER = ? and PAGE_MINOR_VER = ? ";
	
	private static final String chkPageSubmited = "select count(*) as version_exists from  UI_FLX_PAGEVER where "
		+ " PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ? ";
	/*
	private static final String chkPageVersionSubmited = "select count (*) from  UI_FLX_PAGEVER where"
			+ " PAGE_VER = (select nvl( max(PAGE_VER), 0) PAGE_VER from UI_FLX_PAGEVER where PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ?) "
			+ " and (page_minor_ver = (select nvl( max(page_minor_ver), 0) page_minor_ver from UI_FLX_PAGEVER where PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ?) OR page_minor_ver IS NULL)"
			+ " and PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ? and page_verdiff is not null";
	private static final String updateCurrentPageVersionChangesSql = "update UI_FLX_PAGEVER set page_verdiff = ?, fk_submission = ? where"
			+ " PAGE_VER = (select nvl( max(PAGE_VER), 0) PAGE_VER from UI_FLX_PAGEVER where PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ?) "
			+ " and (page_minor_ver = (select nvl( max(page_minor_ver), 0) page_minor_ver from UI_FLX_PAGEVER where PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ?) OR page_minor_ver IS NULL)"
			+ " and PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ?";
	*/
	
	private static final String updateCurrentPageVersionChangesSql = "update UI_FLX_PAGEVER set page_verdiff = ?, fk_submission = ?, PAGE_COMMENTS = ? where "
			+ " PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ? and PAGE_VER = ? and PAGE_MINOR_VER = ? ";
	
	public static final String getAllVersionsForProtocolSql = "select page_ver, page_minor_ver, F_CODELST_DESC(er.submission_type) submission_type_desc from ui_flx_pagever ui, er_submission er"
			+ "  where PAGE_MOD_TABLE = ? and page_mod_pk = ? and er.pk_submission = ui.fk_submission order by page_ver desc, page_minor_ver desc";
	
	private static final String getFlexPageHtmlOfVersionSql = 
			"select PAGE_HTML, PAGE_VER from UI_FLX_PAGEVER where "+
			" PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ? "+
			" and PAGE_VER = ? and PAGE_MINOR_VER = ?";
	
	private static final String getFlexPageVersionDiffDetailsSql = 
			"select page_verdiff, page_attachdiff, page_formrespdiff, page_comments from UI_FLX_PAGEVER where "+
			" PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ? "+
			" and PAGE_VER = ? and PAGE_MINOR_VER = ?";
	
	private static final String updateStatOfLatestVersionSql =
			"update UI_FLX_PAGEVER set PAGE_MOD_STAT_SUBTYP = ? where "+
			" PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ? "+
			" and PAGE_VER = ? and PAGE_MINOR_VER = ?";
	
	private static final String checkIrbApprovedExistsSql =
			"select rownum from ui_flx_pagever where page_mod_pk=? and page_ver=? and PAGE_MOD_STAT_SUBTYP=?";
	
	private static final String getPrevFormFillStatus =
			"SELECT CASE when FA_OLDVALUE is not null then (SELECT pk_codelst FROM er_codelst WHERE codelst_type='fillformstat' AND codelst_desc  =FA_OLDVALUE AND codelst_hide  ='N') END AS old_status "+
			"FROM ER_FORMAUDITCOL WHERE PK_FORMAUDITCOL=(SELECT MAX(PK_FORMAUDITCOL) FROM ER_FORMAUDITCOL WHERE FK_FORM  =? AND "+
			"FK_FILLEDFORM=(select distinct(pk_studyforms) from er_studyforms where fk_study=? and FK_FORMLIB=? and RECORD_TYPE <>'D') AND FA_SYSTEMID='Form Status' AND FA_NEWVALUE=?)";
	
	private static final String getAllLockedStudyVersions = 
			"SELECT CASE WHEN page_ver IS NOT NULL "+
			"AND page_minor_ver IS NOT NULL "+
			"AND page_minor_ver>9 THEN "+
			"page_ver||'.'||page_minor_ver "+
			"WHEN page_ver      IS NOT NULL "+
			"AND page_minor_ver IS NOT NULL "+
			"AND page_minor_ver<=9 THEN "+
			"page_ver||'.0'||page_minor_ver "+
			"END AS LOCKED_VERSIONS "+
			"FROM ui_flx_pagever "+
			"WHERE PAGE_MOD_PK =? "+
			"AND page_MOD_TABLE='er_study' "+
			"AND PAGE_MOD_STAT_SUBTYP in ('submitting','app_CHR')";
	
	private static final String getHighestFlexPageVersionSubmissionSql = "select nvl(max(PAGE_VER), 0) from UI_FLX_PAGEVER where "+
	"PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ? and FK_SUBMISSION = ?";
	
	private static final String getHighestMinorFlexPageVersionSubmissionSql = "select nvl(max(PAGE_MINOR_VER), 0) from UI_FLX_PAGEVER where "+
	"PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ? and page_ver= ? and FK_SUBMISSION = ?";
	
	private static final String getStudyVerIgnoreIds = "select pk_studyver from er_studyver where fk_study=? and STUDYVER_MAJORVER=? and STUDYVER_MINORVER=? and STUDYVER_STATUS='NP'";
	
	public static final String MOD_TABLE_STR = "modTable", MOD_PK_STR = "modPK", PAGE_VER_STR = "pageVer", PAGE_ID_STR = "pageId";
	public static final String PAGE_DEF_STR = "pageDef", PAGE_JS_STR = "pageJs", PAGE_JSON_STR = "pageJson", PAGE_HTML_STR = "pageHtml";
	public static final String FLX_PAGE_COOKIE = "FLX_PAGE_COOKIE";
	public static final String PAGE_DEF = "page_def";
	public static final String PAGE_JS = "page_js";
	public static final String EMPTY_STRING = "";
	public static final String PAGE_MINOR_VER = "page_minor_ver";
	public static final String FK_SUBMISSION = "fk_submission";
	public static final String PAGE_ATTACHDIFF = "page_attachdiff";
	public static final String PAGE_VERDIFF = "page_verdiff";
	public static final String PAGE_FORMRESPONSES = "page_formresponses";
	public static final String PAGE_FORMRESPDIFF = "page_formrespdiff";
	public static final String PAGE_COMMENTS = "page_comments";
	//public static final String VERSIONDIFF_STR = "versionDiff", ATTACHDIFF_STR = "attachDiff", FORMRESPDIFF_STR = "page_formrespdiff", COMMENTS_STR = "comments";

	public UIFlxPageDao() {
	}
	
	/**
	 *  Use this for unit-testing
	 */
	public static void main(String[] args) {
		UIFlxPageDao flxDao = new UIFlxPageDao();
		System.setProperty("ERES_HOME", "C:/Velos/eResearch_jboss510/conf/");
		HashMap<String, Object> argMap = new HashMap<String, Object>();
		try {
			String pageId = "protocol";
			JSONObject protocolJson = flxDao.flexPageData(pageId);
			argMap.put(PAGE_ID_STR, pageId);
			argMap.put(PAGE_DEF_STR, protocolJson.get(PAGE_DEF));
			argMap.put(PAGE_JS_STR, protocolJson.get(PAGE_JS));
		} catch (Exception e) {
			e.printStackTrace();
		}
		argMap.put(MOD_TABLE_STR, "er_study");
		argMap.put(MOD_PK_STR, 876);
		argMap.put(PAGE_JSON_STR, "{test:123}");
		argMap.put(PAGE_HTML_STR, "<html></html>");
		int retNum = 0;
		// retNum = flxDao.insertFlexPageVer(argMap);
		retNum = flxDao.updateStatOfLatestVersion("er_study", 1157, "app_CHR");
		System.out.println("retNum="+retNum);
	}
	
	private Integer submissionStatusPk = 0;
	
	public Integer getSubmissionStatusPk() {
		return submissionStatusPk;
	}

	public void insertFlexPage(String pageId) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(insertFlexPageSql);
			pstmt.setString(1, pageId);
			((OraclePreparedStatement) pstmt).setStringForClob(2,
					"{name:\"Study Screen\",sections:[{fields:[]}]}");
			((OraclePreparedStatement) pstmt).setStringForClob(3,
					"var studyScreenJS = {};");

			pstmt.executeUpdate();
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.testInsert EXCEPTION: "
					+ ex);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
			} catch (Exception e) {
			}
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
			}
		}
	}

	public void updateFlexPageCookie(String pageId, HashMap hash) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		String pageDef = (String) hash.get(FLX_PAGE_COOKIE);
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(upateFlexPageSql);
			((OraclePreparedStatement) pstmt).setStringForClob(1, pageDef);
			pstmt.setString(2, pageId);
			pstmt.executeUpdate();
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.testInsert EXCEPTION: "
					+ ex);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
			} catch (Exception e) {
			}
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
			}
		}
	}
	
	public int insertFlexPageVer(HashMap<String, Object> argMap) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		int retNum = 0;		
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(insertFlexPageVerSql);
			pstmt.setString(1, (String)argMap.get(MOD_TABLE_STR));
			pstmt.setInt(2, (Integer)argMap.get(MOD_PK_STR));
			Integer pageVer = (Integer)argMap.get(PAGE_VER_STR);
			Integer pageMinorVer = (Integer)argMap.get(PAGE_MINOR_VER);
			if (pageVer == null) { pageVer = 1; }
			pstmt.setInt(3, pageVer);
			pstmt.setString(4, (String)argMap.get(PAGE_ID_STR));
			((OraclePreparedStatement) pstmt).setStringForClob(5, (String)argMap.get(PAGE_DEF_STR));
			((OraclePreparedStatement) pstmt).setStringForClob(6, (String)argMap.get(PAGE_JS_STR));
			((OraclePreparedStatement) pstmt).setStringForClob(7, (String)argMap.get(PAGE_JSON_STR));
			((OraclePreparedStatement) pstmt).setStringForClob(8, (String)argMap.get(PAGE_HTML_STR));
			pstmt.setInt(9, pageMinorVer);
			pstmt.setString(10, (String)argMap.get(PAGE_VERDIFF));
			pstmt.setInt(11, (Integer)argMap.get(FK_SUBMISSION));
			pstmt.setString(12, (String)argMap.get(PAGE_ATTACHDIFF));
			((OraclePreparedStatement) pstmt).setStringForClob(13, (String)argMap.get(PAGE_FORMRESPONSES));
			((OraclePreparedStatement) pstmt).setStringForClob(14, (String)argMap.get(PAGE_FORMRESPDIFF));
			retNum = pstmt.executeUpdate();
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.insertFlexPageVer EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return retNum;
	}
	
	public int getHighestFlexPageVersion(String tablename, int tablePk) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		int retNum = 0;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(getHighestFlexPageVersionSql);
			pstmt.setString(1, (String)tablename);
			pstmt.setInt(2, (Integer)tablePk);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				retNum = rs.getInt(1);
			}
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.getHighestFlexPageVersion EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return retNum;
	}
	
	public String getHighestFlexPageByDate(String tablename, int tablePk, String submissionDate) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		String retNum = "";
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement("select max(PAGE_VER||','|| PAGE_MINOR_VER) as full_version from UI_FLX_PAGEVER where PAGE_MOD_TABLE = '"+tablename+"'"+
					" and PAGE_MOD_PK ="+tablePk+" and page_date < TO_DATE('"+submissionDate+"', PKG_DATEUTIL.F_GET_DATETIMEFORMAT)");
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				if(rs.getString(1)!=null){
					retNum = rs.getString(1);
				}	
			}
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.getHighestFlexPageVersion EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return retNum;
	}

	public String getFlexPageHtmlOfHighestVersion(String tablename, int tablePk) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		String html = EMPTY_STRING;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(getFlexPageHtmlOfHighestVersionSql);
			pstmt.setString(1, (String)tablename);
			pstmt.setInt(2, (Integer)tablePk);
			pstmt.setString(3, (String)tablename);
			pstmt.setInt(4, (Integer)tablePk);
			pstmt.setString(5, (String)tablename);
			pstmt.setInt(6, (Integer)tablePk);
			pstmt.setString(7, (String)tablename);
			pstmt.setInt(8, (Integer)tablePk);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				html = rs.getString(1);
			}
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.getFlexPageHtmlOfHighestVersion EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return html;
	}
	
	public int getFlexLatestSubmissionStatus(int fkStudy) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		int submPk = 0;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(getFlexPageLatestSubmissionStatusSql);
			pstmt.setInt(1, fkStudy);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				submPk = rs.getInt(1);
				submissionStatusPk = rs.getInt(2);
			}
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.getFlexLatestSubmissionStatus EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return submPk;
	}
	
	public List getStudyVerIgnoreIds(int studyId, int majorVer, int minorVer) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		ArrayList<Integer> prevVerIgnoreIds = new ArrayList<Integer>();
		int docsCount = 0;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(getStudyVerIgnoreIds);
			pstmt.setInt(1, studyId);
			pstmt.setInt(2, majorVer);
			pstmt.setInt(3, minorVer);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				prevVerIgnoreIds.add(rs.getInt("pk_studyver"));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.getStudyVerIgnoreIds EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return prevVerIgnoreIds;
	}
	
	public JSONObject getFlxPageCookie(JSONObject jsObjPageRec) {
		JSONObject newJSONObj = null;
		try {
			newJSONObj = flexPageConfigJson(jsObjPageRec);
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao",
					"UIFlxPageDao.getFlxPageCookie EXCEPTION: " + ex);
		}
		if (newJSONObj == null) {
			newJSONObj = new JSONObject();
		}
		return newJSONObj;
	}

	public String getFlxPageJS(JSONObject jsObjPageRec) {
		String newFlexPageJS = null;
		try {
			newFlexPageJS = flexPageJS(jsObjPageRec);
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao",
					"UIFlxPageDao.getFlxPageJS EXCEPTION: " + ex);
		}
		return newFlexPageJS;
	}
	
	public JSONObject flexPageData(String pageId) throws Exception {
		JSONObject jsObjPageRec = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(getFlexPageCookie);
			pstmt.setString(1, pageId);

			ResultSet rs = pstmt.executeQuery();
			if (rs == null) {
				return jsObjPageRec;
			}
			while (rs.next()) {
				jsObjPageRec = new JSONObject();

				String myPageDef = rs.getString(PAGE_DEF);
				jsObjPageRec.put(PAGE_DEF, myPageDef);
				
				String myPageJS = rs.getString(PAGE_JS);
				jsObjPageRec.put(PAGE_JS, myPageJS);
				return jsObjPageRec;
			}
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao",
					"UIFlxPageDao.flexPageData EXCEPTION: " + ex);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
			} catch (Exception e) {
			}
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
			}
		}
		return jsObjPageRec;
	}

	private JSONObject flexPageConfigJson(JSONObject jsObj) throws Exception {
		JSONObject newJSONObj = null;
		if (jsObj == null) {
			return newJSONObj;
		}
		String myPageDef = jsObj.getString(PAGE_DEF);
		newJSONObj = new JSONObject(myPageDef);
		return newJSONObj;
	}

	private String flexPageJS(JSONObject jsObj) throws Exception {
		String newFlexPageJS = null;
		if(jsObj == null) {
			return newFlexPageJS;
		}

		newFlexPageJS = jsObj.getString(PAGE_JS);
		return newFlexPageJS;
	}

	public int getHighestMinorFlexPageVersion(int version, String tablename, int tablePk) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		int retNum = 0;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(getHighestMinorFlexPageVersionSql);
			pstmt.setString(1, (String)tablename);
			pstmt.setInt(2, (Integer)tablePk);
			pstmt.setInt(3, version);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				retNum = rs.getInt(1);
			}
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.getHighestMinorFlexPageVersion EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return retNum;
	}
		
	public String getFlexPageHtmlOfHighestApprovedVersion(String tablename, int tablePk) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		String html = EMPTY_STRING;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(getFlexPageHtmlOfHighestApprovedVersionSql);
			pstmt.setString(1, (String)tablename);
			pstmt.setInt(2, (Integer)tablePk);
			pstmt.setString(3, (String)tablename);
			pstmt.setInt(4, (Integer)tablePk);
			pstmt.setString(5, (String)tablename);
			pstmt.setInt(6, (Integer)tablePk);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				html = rs.getString(1);
			}
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.getFlexPageHtmlOfHighestApprovedVersionSql EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return html;
	}
	
	private Integer majorVer = 0;
	private Integer minorVer = 0;
	private String comments = "";
	private Integer submissionPk = 0;
	public Integer getMajorVer() { return majorVer; }
	public Integer getMinorVer() { return minorVer; }
	public Integer getSubmissionPk() { return submissionPk; }
	public String getComments() { return comments; }

	public void setComments(String comments) {
		this.comments = comments;
	}

	public void getHighestFlexPageFullVersion(String tablename, int tablePk) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		majorVer = 0;
		minorVer = 0;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(getHighestFlexPageFullVersionSql);
			pstmt.setString(1, (String)tablename);
			pstmt.setInt(2, (Integer)tablePk);			
			rs = pstmt.executeQuery();
			while(rs.next()) {
				majorVer = rs.getInt(1);
				minorVer = rs.getInt(2);
				submissionPk = rs.getInt(3);
				if (submissionPk == null) {
					submissionPk = 0;
				}
				break;
			}
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.getHighestFlexPageFullVersion EXCEPTION:"+ ex);
			majorVer = 0;
			minorVer = 0;
			submissionPk = 0;
		} finally {
			try { if (rs != null) { rs.close(); } } catch (Exception e) {}
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
	}
	
	public String getFlexPageVerOfHighestApprovedVersion(String tablename, int tablePk) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		String pageVer = EMPTY_STRING;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(getFlexPageVerOfHighestApprovedVersionSql);
			pstmt.setString(1, (String)tablename);
			pstmt.setInt(2, (Integer)tablePk);			
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				pageVer = rs.getString(1);
			}
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.getFlexPageVerOfHighestApprovedVersionSql EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return pageVer;
	}
	
	public HashMap<String, Object> getDetailedFlexPage(int studyId, int pageVer, int pageMinorVer) {
		HashMap<String, Object> resMap = new HashMap<String, Object>();
		PreparedStatement pstmt = null;
		Connection conn = null;
		String pageId = EMPTY_STRING;
		String pageDef = EMPTY_STRING;
		String pageJs = EMPTY_STRING;
		String pageJson = EMPTY_STRING;
		String pageHtml = EMPTY_STRING;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(getDetailedFlexPageSql);
			pstmt.setInt(1, studyId);
			pstmt.setInt(2, pageVer);	
			pstmt.setInt(3, pageMinorVer);			
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				resMap.put("pageModTable", rs.getString("PAGE_MOD_TABLE"));
				resMap.put("pageModPk", rs.getString("PAGE_MOD_PK"));
				resMap.put("pageVer", rs.getString("PAGE_VER"));
				resMap.put("pageMinorVer", rs.getString("PAGE_MINOR_VER"));
				resMap.put("pageId", rs.getString("PAGE_ID"));
				resMap.put("pageDef", rs.getString("PAGE_DEF"));
				resMap.put("pageJs", rs.getString("PAGE_JS"));
				resMap.put("pageJson", rs.getString("PAGE_JSON"));
				resMap.put("pageHtml", rs.getString("PAGE_HTML"));	
				resMap.put("pageVerDiff", rs.getString("PAGE_VERDIFF"));
				resMap.put("fkSubmission", rs.getInt("FK_SUBMISSION"));	
				resMap.put("pageAttachDiff", rs.getString("PAGE_ATTACHDIFF"));				
			}
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.getDetailedFlexPage EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return resMap;
	}

	public String getPageVersionDetails(String tablename, int studyId, String studyVersion) {
		String json = "";
		String[] studyVersionString;
		int page_ver = -1;
		int page_minor_ver = -1;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try {

			if (studyVersion.contains(".")) {
				studyVersionString = studyVersion.split("\\.");
				if (studyVersionString.length == 2) {
					page_ver = Integer.parseInt(studyVersionString[0]);
					page_minor_ver = Integer.parseInt(studyVersionString[1]);
				}
			} else {
				page_ver = Integer.parseInt(studyVersion);
			}

			conn = getConnection();

			String mysql = getFlexPageVersionJsonSql;
			if (page_minor_ver != -1) {
				mysql += " and page_minor_ver = ?";
			}

			pstmt = conn.prepareStatement(mysql);
			pstmt.setString(1, (String)tablename);
			pstmt.setInt(2, studyId);
			pstmt.setInt(3, page_ver);
			if (page_minor_ver != -1) {
				pstmt.setInt(4, page_minor_ver);
			}
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				json = rs.getString("PAGE_JSON");
			}

		} catch (SQLException e) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.getPageVersionDetails EXCEPTION:"+ e);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}
		return json;
	}

	public String getPageVersionFormResponses(String tablename, int studyId, String studyVersion) {
		String jsonArray = "";
		String[] studyVersionString;
		int page_ver = -1;
		int page_minor_ver = -1;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try {

			if (studyVersion.contains(".")) {
				studyVersionString = studyVersion.split("\\.");
				if (studyVersionString.length == 2) {
					page_ver = Integer.parseInt(studyVersionString[0]);
					page_minor_ver = Integer.parseInt(studyVersionString[1]);
				}
			} else {
				page_ver = Integer.parseInt(studyVersion);
			}

			conn = getConnection();

			String mysql = getFlexPageVersionFormResponsesSql;
			if (page_minor_ver != -1) {
				mysql += " and page_minor_ver = ?";
			}

			pstmt = conn.prepareStatement(mysql);
			pstmt.setString(1, (String)tablename);
			pstmt.setInt(2, studyId);
			pstmt.setInt(3, page_ver);
			if (page_minor_ver != -1) {
				pstmt.setInt(4, page_minor_ver);
			}
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				jsonArray = rs.getString(PAGE_FORMRESPONSES);
			}

		} catch (SQLException e) {
			Rlog.fatal("ResubmDraftDao", "ResubmDraftDao.getPageVersionDetails EXCEPTION:"+ e);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}
		return jsonArray;
	}

	public int chkPageVersionSubmited(String tableName, int studyId, int major, int minor) {
		System.out.println("studyId:::::>>>>>>>>>>"+studyId);
		int versionSubmitted = -1;
		this.getHighestFlexPageFullVersion(tableName, studyId);
		int highestVersion = majorVer;
		if(highestVersion == 0)
		{
			versionSubmitted = -1;
		}
		else
		{
			int count = 0;
			PreparedStatement pstmt = null;
			Connection conn = null;
			String statSubtyp = null;
			try {

				conn = getConnection();
				pstmt = conn.prepareStatement(chkPageVersionSubmited);
				pstmt.setString(1, tableName);
				pstmt.setInt(2, studyId);
				pstmt.setInt(3, major);
				pstmt.setInt(4, minor);
				ResultSet rs = pstmt.executeQuery();
				while (rs.next()) {
					count++;
					statSubtyp = rs.getString(1);
				}

			}  catch (Exception e) {
				Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.chkPageVersionSubmited EXCEPTION:"+ e);
			} finally {
				try {
					if (pstmt != null)
						pstmt.close();
				} catch (Exception e) {
				}
				try {
					if (conn != null)
						conn.close();
				} catch (Exception e) {
				}
			}
			System.out.println("statSubtyp:::::>>>>>>>>>>"+statSubtyp);
			if (!StringUtil.isEmpty(statSubtyp)) {
				versionSubmitted = 2; // = Submitted / IRB Approved / IRB Disapproved
			} else if (count > 0) {
				versionSubmitted = 1; // = Archive exists
			} else {
				versionSubmitted = 0; // = No archive
			}
		}
		System.out.println("versionSubmitted:::::>>>>>>>>>>"+versionSubmitted);
		return versionSubmitted;
	}

	
	public int updateCurrentPageVersionChanges(String tableName, int studyId, String changelog, int fk_submission) {
		this.getHighestFlexPageFullVersion(tableName, studyId);
		PreparedStatement pstmt = null;
		Connection conn = null;
		int retNum = 0;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(updateCurrentPageVersionChangesSql);
			pstmt.setString(1, changelog);
			pstmt.setInt(2, fk_submission);
			pstmt.setString(3, comments);
			pstmt.setString(4, tableName);
			pstmt.setInt(5, studyId);
			pstmt.setInt(6, majorVer);
			pstmt.setInt(7, minorVer);
			retNum = pstmt.executeUpdate();
			
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.updateCurrentPageVersionChanges EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return retNum;
	}
	
	public int updateStatOfLatestVersion(String tableName, int studyId, String statSubtyp) {
		this.getHighestFlexPageFullVersion(tableName, studyId);
		if (majorVer < 1 || submissionPk < 1) {
			return -1;
		}
		if (StringUtil.isEmpty(statSubtyp) || statSubtyp.length() > 15) {
			return -2;
		}
		int retNum = 0;
		PreparedStatement pstmt = null;
		Connection conn = null;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(updateStatOfLatestVersionSql);
			pstmt.setString(1, statSubtyp);
			pstmt.setString(2, tableName);
			pstmt.setInt(3, studyId);
			pstmt.setInt(4, majorVer);
			pstmt.setInt(5, minorVer);
			retNum = pstmt.executeUpdate();
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.updateStatOfLatestVersion EXCEPTION:"+ ex);
			retNum = -3;
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return retNum;
	}
	
	public int checkLockedVersionExists(int studyId, int verNum) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		int retNum = 0;
		try {
			conn = getConnection();
			String Sql = "select rownum from ui_flx_pagever where page_mod_pk=? and page_ver=? and page_minor_ver=0";
			pstmt = conn.prepareStatement(Sql);
			pstmt.setInt(1, studyId);
			pstmt.setInt(2, verNum);			
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				retNum = rs.getInt("rownum");
			}
			
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.checkLockedVersionExists EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		System.out.println("checkLockedVersionExists:::::"+retNum);
		return retNum;
	}
	
	public int checkIrbApprovedExists(int studyId, int verNum) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		int retNum = 0;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(checkIrbApprovedExistsSql);
			pstmt.setInt(1, studyId);
			pstmt.setInt(2, verNum);
			pstmt.setString(3, CFG.EIRB_APPROVED_SUBTYP);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				retNum = rs.getInt("rownum");
			}
			
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.checkIrbApprovedExists EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return retNum;
	}
	
	public int getApprovedVersionForStudy(int studyId) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		int retNum = 0;
		try {
			conn = getConnection();
			String Sql = "select page_ver from ui_flx_pagever where page_mod_pk=? and page_minor_ver=0 order by page_ver ASC";
			pstmt = conn.prepareStatement(Sql);
			pstmt.setInt(1, studyId);
					
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				retNum = rs.getInt("page_ver");
			}
			
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.getApprovedVersionForStudy EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return retNum;
	}	
	
	public ArrayList<HashMap<String, String>> getAllVersionsForProtocol(String tablename, int tablePk) {
		ArrayList<HashMap<String, String>> resList = new ArrayList<HashMap<String, String>>();

		PreparedStatement pstmt = null;
		Connection conn = null;
		try {
			conn = getConnection();

			String mysql = getAllVersionsForProtocolSql;

			pstmt = conn.prepareStatement(mysql);
			pstmt.setString(1, (String)tablename);
			pstmt.setInt(2, tablePk);
		
			ResultSet rs = pstmt.executeQuery();

			while(rs.next()) {
				HashMap<String, String> resMap = new HashMap<String, String>();
				resMap.put("page_ver", rs.getString("PAGE_VER"));
				resMap.put("page_minor_ver", rs.getString("PAGE_MINOR_VER"));
				resMap.put("submissionTypeDesc", rs.getString("SUBMISSION_TYPE_DESC"));
				resList.add(resMap);
			}

		} catch (SQLException e) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.getAllVersionsForProtocol EXCEPTION:"+ e);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}
		return resList;
	}
	
	public String getFlexPageHtmlOfVersion(String tablename, int tablePk, int pageVer, int pageMinorVer) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		String html = EMPTY_STRING;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(getFlexPageHtmlOfVersionSql);
			pstmt.setString(1, (String)tablename);
			pstmt.setInt(2, (Integer)tablePk);
			pstmt.setInt(3, pageVer);
			pstmt.setInt(4, pageMinorVer);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				html = rs.getString(1);
			}
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.getFlexPageHtmlOfVersion EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return html;
	}
	
	public JSONObject getFlexPageVersionDiffDetails(String tablename, int tablePk, int pageVer, int pageMinorVer) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		JSONObject verDiffJSON = new JSONObject();
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(getFlexPageVersionDiffDetailsSql);
			pstmt.setString(1, (String)tablename);
			pstmt.setInt(2, (Integer)tablePk);
			pstmt.setInt(3, pageVer);
			pstmt.setInt(4, pageMinorVer);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				String data = rs.getString(1);
				data = StringUtil.isEmpty(data)? "[]" : data;
				verDiffJSON.put(PAGE_VERDIFF, data);
				data = rs.getString(2);
				data = StringUtil.isEmpty(data)? "[]" : data;
				verDiffJSON.put(PAGE_ATTACHDIFF, data);
				data = rs.getString(3);
				data = StringUtil.isEmpty(data)? "[]" : data;
				verDiffJSON.put(PAGE_FORMRESPDIFF, data);
				data = rs.getString(4);
				data = StringUtil.isEmpty(data)? EMPTY_STRING : data;
				verDiffJSON.put(PAGE_COMMENTS, data);
			}
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.getFlexPageVersionDiffDetails EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return verDiffJSON;
	}
	
	public String updateAttachPageDiffFlexPageVersion(int studyId, String attachJSON) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		String verDiff = EMPTY_STRING;
		int pageVer = getHighestFlexPageVersion("er_study",studyId);
		int pageMinorVer = getHighestMinorFlexPageVersion(pageVer, "er_study", studyId);
		try {
			conn = getConnection();
			String sql = "update ui_flx_pagever set page_attachdiff=? where page_mod_pk=? and page_ver = ? and page_minor_ver=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, attachJSON);
			pstmt.setInt(2, studyId);
			pstmt.setInt(3, pageVer);
			pstmt.setInt(4, pageMinorVer);
			int result = pstmt.executeUpdate();
			
		} catch (Exception ex) { 
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.getFlexPageVersionDiffDetails EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return verDiff;
	}
	
	public String updateFormPageDiffFlexPageVersion(int studyId, String formJSON) {
		PreparedStatement pstmt = null; 
		Connection conn = null;
		String verDiff = EMPTY_STRING;
		int pageVer = getHighestFlexPageVersion("er_study",studyId);
		int pageMinorVer = getHighestMinorFlexPageVersion(pageVer, "er_study", studyId);
		try {
			conn = getConnection();
			String sql = "update ui_flx_pagever set page_formrespdiff=? where page_mod_pk=? and page_ver = ? and page_minor_ver=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, formJSON);
			pstmt.setInt(2, studyId);
			pstmt.setInt(3, pageVer);
			pstmt.setInt(4, pageMinorVer);
			int result = pstmt.executeUpdate();
			
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.getFlexPageVersionDiffDetails EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return verDiff;
	}
	
	
	public String getFlexPageAttachVersionDiffDetails(String tablename, int tablePk, int pageVer, int pageMinorVer) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		String verDiff = EMPTY_STRING;
		try {
			conn = getConnection();
			String sql = "select page_attachdiff from ui_flx_pagever where " + 	" PAGE_MOD_TABLE = ? and PAGE_MOD_PK = ? "+
			" and PAGE_VER = ? and PAGE_MINOR_VER = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, (String)tablename);
			pstmt.setInt(2, (Integer)tablePk);
			pstmt.setInt(3, pageVer);
			pstmt.setInt(4, pageMinorVer);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				verDiff = rs.getString(1);
			}
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.getFlexPageAttachVersionDiffDetails EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return verDiff;
	}
	
	public int getPrevFormFillStatus(int studyId, int formId, String codePkLckDown) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		int prevStatus=0;
		try {
			conn = getConnection();
			String sql = getPrevFormFillStatus;
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, formId);
			pstmt.setInt(2, studyId);
			pstmt.setInt(3, formId);
			pstmt.setString(4, codePkLckDown);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				prevStatus = rs.getInt(1);
			}
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.getFlexPageAttachVersionDiffDetails EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return prevStatus;
	}
	public ArrayList<String> getAllLockedStudyVersions(int studyId) {
		ArrayList<String> lockedVersions = new ArrayList<String>();
		PreparedStatement pstmt = null; 
		Connection conn = null;
		try {
			conn = getConnection();
			String Sql = getAllLockedStudyVersions;
			pstmt = conn.prepareStatement(Sql);
			pstmt.setInt(1, studyId);
					
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				lockedVersions.add(rs.getString("LOCKED_VERSIONS"));
			}
			
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.getAllLockedStudyVersions EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return lockedVersions;
	}
	//chkPageSubmited
	public boolean chkPageSubmited(int studyId) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		int retNum = 0;
		boolean existsFlag=false;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(chkPageSubmited);
			pstmt.setString(1, "er_study");
			pstmt.setInt(2, studyId);
					
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				retNum = rs.getInt("version_exists");
				if(retNum > 0){
					existsFlag = true;
				}
			}
			
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.getApprovedVersionForStudy EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return existsFlag;
	}
	
	public int getHighestFlexPageVersionSubmission(String tablename, int tablePk, int submissionId) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		int retNum = 0;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(getHighestFlexPageVersionSubmissionSql);
			pstmt.setString(1, (String)tablename);
			pstmt.setInt(2, (Integer)tablePk);
			pstmt.setInt(3, (Integer)submissionId);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				retNum = rs.getInt(1);
			}
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.getHighestFlexPageVersionSubmission EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return retNum;
	}
	
	public int getHighestMinorFlexPageVersionSubmission(int version, String tablename, int tablePk, int submissionId) {
		PreparedStatement pstmt = null;
		Connection conn = null;
		int retNum = 0;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(getHighestMinorFlexPageVersionSubmissionSql);
			pstmt.setString(1, (String)tablename);
			pstmt.setInt(2, (Integer)tablePk);
			pstmt.setInt(3, version);
			pstmt.setInt(4, (Integer)submissionId);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				retNum = rs.getInt(1);
			}
		} catch (Exception ex) {
			Rlog.fatal("UIFlxPageDao", "UIFlxPageDao.getHighestMinorFlexPageVersionSubmission EXCEPTION:"+ ex);
		} finally {
			try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
			try { if (conn != null) { conn.close(); } } catch (Exception e) {}
		}
		return retNum;
	}
}
