package com.velos.eres.widget.service.util;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.MC;
import com.velos.eres.service.util.CFG;
import com.velos.eres.service.util.StringUtil;
/*
 * Supported Values-
 * FIELD_TYPE: input, textarea, lookup, dropdown, checkbox, fieldGroup
 */
public enum  FlxPageFields {
	//Study Number - Page Keyword, Field Keyword, Field Label
	STUDY_STUDYNUMBER("STUDY", "STUDYNUMBER", LC.L_Study_Number) {
        @Override
        public JSONObject getFieldHTMLSpecJSON() {
        	JSONObject jsFldSpecs = new JSONObject();
        	try {
        		jsFldSpecs.put(FIELD_TYPE, "input");
				jsFldSpecs.put(ID, "studyNumber");
	        	jsFldSpecs.put(NAME, "studyNumber");
        		jsFldSpecs.put("size", "40");
        		jsFldSpecs.put("maxlength", "100");
			} catch (JSONException e) {
				e.printStackTrace();
				jsFldSpecs = null;
			}
            return jsFldSpecs;
        }
    },
	//Title
	STUDY_STUDYTITLE("STUDY", "STUDYTITLE", LC.L_Study_Title) {
    	@Override
        public JSONObject getFieldHTMLSpecJSON() {
        	JSONObject jsFldSpecs = new JSONObject();
        	try {
        		jsFldSpecs.put(FIELD_TYPE, "textarea");
				jsFldSpecs.put(ID, "studyTitle");
	        	jsFldSpecs.put(NAME, "studyTitle");
        		jsFldSpecs.put("rows", "2");
        		jsFldSpecs.put("cols", "93");
        		jsFldSpecs.put("maxlength", "1000");
			} catch (JSONException e) {
				e.printStackTrace();
				jsFldSpecs = null;
			}
            return jsFldSpecs;
        }
    },
    //Study entered by
    STUDY_DATAMANAGER("STUDY", "DATAMANAGER", "Study " + LC.L_Entered_By ) {
    	@Override
    	public JSONObject getFieldHTMLSpecJSON() {
    		JSONObject jsFldSpecs = new JSONObject();
    		try {
    			jsFldSpecs.put(FIELD_TYPE, "lookup");
    			jsFldSpecs.put(ID, "study_selectLink");
    			jsFldSpecs.put(NAME, "dataManagerName");
    			jsFldSpecs.put("readonly", "READONLY");
        		jsFldSpecs.put("linkText", "Select User");
        		jsFldSpecs.put("linkHref", "#");
        		jsFldSpecs.put("onClick", "return studyFunctions.openwin1(\"study\");");
    		} catch (JSONException e) {
    			//e.printStackTrace();
    			jsFldSpecs = null;
    		}
    		return jsFldSpecs;
    	}
    },
    
	//Objective
	STUDY_OBJECTIVE("STUDY", "OBJECTIVE", LC.L_Objective) {
    	@Override
        public JSONObject getFieldHTMLSpecJSON() {
        	JSONObject jsFldSpecs = new JSONObject();
        	try {
        		jsFldSpecs.put(FIELD_TYPE, "textarea");
				jsFldSpecs.put(ID, "studyObjective");
	        	jsFldSpecs.put(NAME, "studyObjective");
        		jsFldSpecs.put("rows", "3");
        		jsFldSpecs.put("cols", "93");
        		jsFldSpecs.put("maxlength", "2000");
			} catch (JSONException e) {
				e.printStackTrace();
				jsFldSpecs = null;
			}
            return jsFldSpecs;
        }
    },
	//Summary
	STUDY_STUDYSUMM("STUDY", "STUDYSUMMARY", LC.L_Summary) {
    	@Override
        public JSONObject getFieldHTMLSpecJSON() {
        	JSONObject jsFldSpecs = new JSONObject();
        	try {
        		jsFldSpecs.put(FIELD_TYPE, "textarea");
				jsFldSpecs.put(ID, "studySummary");
	        	jsFldSpecs.put(NAME, "studySummary");
        		jsFldSpecs.put("rows", "3");
        		jsFldSpecs.put("cols", "93");
        		jsFldSpecs.put("maxlength", "2000");
			} catch (JSONException e) {
				e.printStackTrace();
				jsFldSpecs = null;
			}
            return jsFldSpecs;
        }
    },
	//Principal Investigator
	STUDY_PIAUTHOR("STUDY", "PIAUTHOR", LC.L_Principal_Investigator) {
    	@Override
        public JSONObject getFieldHTMLSpecJSON() {
        	JSONObject jsFldSpecs = new JSONObject();
        	try {
        		jsFldSpecs.put(FIELD_TYPE, "lookup");
        		jsFldSpecs.put(ID, "studyinv_selectLink");
        		jsFldSpecs.put(NAME, "prinInvName");
        		jsFldSpecs.put("readonly", "READONLY");
        		jsFldSpecs.put("linkText", "Select User");
        		jsFldSpecs.put("linkHref", "#");
        		jsFldSpecs.put("onClick", "return studyFunctions.openwin1(\"studyinv\");");
        	} catch (JSONException e) {
        		e.printStackTrace();
        		jsFldSpecs = null;
        	}
            return jsFldSpecs;
        }
    },
    //Principal Investigator if other
    STUDY_PIOTHER("STUDY", "PIOTHER", LC.L_If_Other) {
    	@Override
    	public JSONObject getFieldHTMLSpecJSON() {
    		JSONObject jsFldSpecs = new JSONObject();
    		try {
    			jsFldSpecs.put(FIELD_TYPE, "input");
    			jsFldSpecs.put(ID, "prinvIfOther");
    			jsFldSpecs.put(NAME, "prinvOther");
    			jsFldSpecs.put("type", "text");
    		} catch (JSONException e) {
    			//e.printStackTrace();
    			jsFldSpecs = null;
    		}
    		return jsFldSpecs;
    	}
    },
    //Study Contact
    STUDY_STUDYCONTACT("STUDY", "STUDYCONTACT", LC.L_Study_Contact) {
    	@Override
    	public JSONObject getFieldHTMLSpecJSON() {
    		JSONObject jsFldSpecs = new JSONObject();
    		try {
    			jsFldSpecs.put(FIELD_TYPE, "lookup");
    			jsFldSpecs.put(ID, "studyco_selectLink");
    			jsFldSpecs.put(NAME, "studycoName");
    			jsFldSpecs.put("readonly", "READONLY");
        		jsFldSpecs.put("linkText", "Select User");
        		jsFldSpecs.put("linkHref", "#");
        		jsFldSpecs.put("onClick", "return studyFunctions.openwin1(\"studyco\");");
    		} catch (JSONException e) {
    			//e.printStackTrace();
    			jsFldSpecs = null;
    		}
    		return jsFldSpecs;
    	}
    },
    //Principal Investigator was a major author/initiator of this study?
	STUDY_PINVCHECK("STUDY", "PINVCHECK", MC.M_PInvestigator_MajorAuth) {
    	@Override
    	public JSONObject getFieldHTMLSpecJSON() {
    		JSONObject jsFldSpecs = new JSONObject();
    		try {
    			jsFldSpecs.put(FIELD_TYPE, "checkbox");
    			jsFldSpecs.put(ID, "cbauth");
    			jsFldSpecs.put(NAME, "cbauth");
    			jsFldSpecs.put("type", "checkbox");
    		} catch (JSONException e) {
    			//e.printStackTrace();
    			jsFldSpecs = null;
    		}
    		return jsFldSpecs;
    	}
    },
    //CTRP Reportable?
    STUDY_CRTP_REPORTABLE("STUDY", "CTRP_REPORTABLE", LC.L_Ctrp_Reportable) {
    	@Override
    	public JSONObject getFieldHTMLSpecJSON() {
    		JSONObject jsFldSpecs = new JSONObject();
    		try {
    			jsFldSpecs.put(FIELD_TYPE, "checkbox");
    			jsFldSpecs.put(ID, "ctrp");
    			jsFldSpecs.put(NAME, "ctrp");
    			jsFldSpecs.put("type", "checkbox");
    		} catch (JSONException e) {
    			//e.printStackTrace();
    			jsFldSpecs = null;
    		}
    		return jsFldSpecs;
    	}
    },
    //FDA Regulated Study
    STUDY_FDA_REGULATED("STUDY", "FDA_REGULATED", LC.L_FdaRegulatedStudy) {
    	@Override
    	public JSONObject getFieldHTMLSpecJSON() {
    		JSONObject jsFldSpecs = new JSONObject();
    		try {
    			jsFldSpecs.put(FIELD_TYPE, "checkbox");
    			jsFldSpecs.put(ID, "fda");
    			jsFldSpecs.put(NAME, "fda");
    			jsFldSpecs.put("type", "checkbox");
    		} catch (JSONException e) {
    			//e.printStackTrace();
    			jsFldSpecs = null;
    		}
    		return jsFldSpecs;
    	}
    },
    //NCT Number
    STUDY_NCTNUMBER("STUDY", "NCTNUMBER", "NCT Number") {
    	@Override
    	public JSONObject getFieldHTMLSpecJSON() {
    		JSONObject jsFldSpecs = new JSONObject();
    		try {
    			jsFldSpecs.put(FIELD_TYPE, "input");
    			jsFldSpecs.put(ID, "nctNumber");
    			jsFldSpecs.put(NAME, "nctNumber");
    			jsFldSpecs.put("type", "text");
    			jsFldSpecs.put("maxlength", "100");
    			jsFldSpecs.put("size", "40");
    		} catch (JSONException e) {
    			//e.printStackTrace();
    			jsFldSpecs = null;
    		}
    		return jsFldSpecs;
    	}
    },
    //Primary Purpose
    STUDY_PPURPOSE("STUDY", "PPURPOSE", LC.L_Primary_Purpose) {
    	@Override
    	public JSONObject getFieldHTMLSpecJSON() {
    		JSONObject jsFldSpecs = new JSONObject();
    		try{
    			jsFldSpecs.put(FIELD_TYPE, "dropdown");
    			jsFldSpecs.put(ID, "studyPurpose");
    			jsFldSpecs.put(NAME, "studyPurpose");
    			jsFldSpecs.put("codelst_type", "studyPurpose");
    		} catch (JSONException e) {
    			//e.printStackTrace();
    			jsFldSpecs = null;
    		}
    		return jsFldSpecs;
    	}
    },
    //Therapeutic Area
	STUDY_TAREA("STUDY", "TAREA", LC.L_Therapeutic_Area) {
    	@Override
        public JSONObject getFieldHTMLSpecJSON() {
        	JSONObject jsFldSpecs = new JSONObject();
        	try {
        		jsFldSpecs.put(FIELD_TYPE, "dropdown");
				jsFldSpecs.put(ID, "studyTArea");
	        	jsFldSpecs.put(NAME, "studyTArea");
	        	jsFldSpecs.put("codelst_type", "tarea");
			} catch (JSONException e) {
				e.printStackTrace();
				jsFldSpecs = null;
			}
            return jsFldSpecs;
        }
    },
    //Agent/Device
    STUDY_AGENTDEVICE("STUDY", "AGENTDEVICE", LC.L_AgentOrDevice) {
    	@Override
    	public JSONObject getFieldHTMLSpecJSON() {
    		JSONObject jsFldSpecs = new JSONObject();
    		try {
    			jsFldSpecs.put(FIELD_TYPE, "input");
    			jsFldSpecs.put(ID, "studyProduct");
    			jsFldSpecs.put(NAME, "studyProduct");
    			jsFldSpecs.put("type", "text");
    			jsFldSpecs.put("maxlength", "100");
    			jsFldSpecs.put("size", "40");
    		} catch (JSONException e) {
    			//e.printStackTrace();
    			jsFldSpecs = null;
    		}
    		return jsFldSpecs;
    	}
    },
	//Disease Sites
	STUDY_DISEASESITES("STUDY", "DISEASESITES", LC.L_Disease_Site) {
    	@Override
        public JSONObject getFieldHTMLSpecJSON() {
        	JSONObject jsFldSpecs = new JSONObject();
        	try {
        		jsFldSpecs.put(FIELD_TYPE, "lookup");
        		jsFldSpecs.put(ID, "disSitename");
	        	jsFldSpecs.put(NAME, "disSitename");
	        	jsFldSpecs.put("readonly", "READONLY");
        		jsFldSpecs.put("linkText", "Select Disease Site(s)");
        		jsFldSpecs.put("linkHref", "#");
        		jsFldSpecs.put("onClick", "return studyFunctions.selectDisease(studyFunctions.formObj);");
        		jsFldSpecs.put("size", "20");
			} catch (JSONException e) {
				e.printStackTrace();
				jsFldSpecs = null;
			}
            return jsFldSpecs;
        }
    },
    // Sites1
    STUDY_SPECIFICSITES1("STUDY", "SPECIFICSITES1", "Specific Site") {
    	@Override
    	public JSONObject getFieldHTMLSpecJSON() {
    		JSONObject jsFldSpecs = new JSONObject();
    		try {
    			jsFldSpecs.put(FIELD_TYPE, "lookup");
    			jsFldSpecs.put(ID, "ICDcode1");
    			jsFldSpecs.put(NAME, "ICDcode1");
    			jsFldSpecs.put("readonly", "READONLY");
        		jsFldSpecs.put("linkText", "Select Site 1");
        		jsFldSpecs.put("linkHref", "#");
        		jsFldSpecs.put("linkOnClick", "return studyFunctions.openWinICD(\"ICDcode1\",\""+CFG.ICD_FK_LKPLIB+"\",\"icd_code\");");
        		jsFldSpecs.put("size", "20");
    		} catch (JSONException e) {
    			//e.printStackTrace();
    			jsFldSpecs = null;
    		}
    		return jsFldSpecs;
    	}
    },
    //Specific Sites2
    STUDY_SPECIFICSITES2("STUDY", "SPECIFICSITES2", "Specific Site 2") {
    	@Override
    	public JSONObject getFieldHTMLSpecJSON() {
    		JSONObject jsFldSpecs = new JSONObject();
    		try {
    			jsFldSpecs.put(FIELD_TYPE, "lookup");
    			jsFldSpecs.put(ID, "ICDcode2");
    			jsFldSpecs.put(NAME, "ICDcode2");
    			jsFldSpecs.put("readonly", "READONLY");
        		jsFldSpecs.put("linkText", "Select Site 2");
        		jsFldSpecs.put("linkHref", "#");
        		jsFldSpecs.put("linkOnClick", "return studyFunctions.openWinICD(\"ICDcode2\",\""+CFG.ICD_FK_LKPLIB+"\",\"icd_code\");");
        		jsFldSpecs.put("size", "20");
    		} catch (JSONException e) {
    			//e.printStackTrace();
    			jsFldSpecs = null;
    		}
    		return jsFldSpecs;
    	}
    },
	//National Sample Size
	STUDY_SAMPLESIZE("STUDY", "SAMPLESIZE", LC.L_Sample_Size) {
    	@Override
        public JSONObject getFieldHTMLSpecJSON() {
        	JSONObject jsFldSpecs = new JSONObject();
        	try {
        		jsFldSpecs.put(FIELD_TYPE, "lookup");
        		jsFldSpecs.put(ID, "nssLink");
        		jsFldSpecs.put(NAME, "nStudySize");
        		jsFldSpecs.put("readonly", "READONLY");
        		jsFldSpecs.put("linkText", "Local Sample Size");
        		jsFldSpecs.put("linkHref", "#");
        		jsFldSpecs.put("linkOnClick", "return studyFunctions.openLSampleSize(\"0\",\"N\");");
        		jsFldSpecs.put("maxlength", "10");
        		jsFldSpecs.put("size", "20");
        	} catch (JSONException e) {
        		//e.printStackTrace();
        		jsFldSpecs = null;
        	}
            return jsFldSpecs;
        }
    },
	//Study Duration
	STUDY_STUDYDURN("STUDY", "STUDYDURN", LC.L_Study_Duration) {
    	@Override
        public JSONObject getFieldHTMLSpecJSON() {
        	JSONObject jsFldSpecs = new JSONObject();
        	try {
        		jsFldSpecs.put(FIELD_TYPE, "input");
        		jsFldSpecs.put(ID, "stdDuration");
        		jsFldSpecs.put(NAME, "studyDuration");
        		jsFldSpecs.put("type", "text");
        		jsFldSpecs.put("maxlength", "10");
        		jsFldSpecs.put("size", "20");
        	} catch (JSONException e) {
        		//e.printStackTrace();
        		jsFldSpecs = null;
        	}
            return jsFldSpecs;
        }
    },
    //Study Duration Dropdown
    STUDY_STUDYDURN_DROP("STUDY", "STUDYDURN", LC.L_Duration){  //added this for the dropdown of study duration
    	@Override
    	public JSONObject getFieldHTMLSpecJSON() {
    		JSONObject jsFldSpecs = new JSONObject();
    		try {
        		jsFldSpecs.put(FIELD_TYPE, "dropdown");
				jsFldSpecs.put(ID, "durunit");
	        	jsFldSpecs.put(NAME, "durationUnit");	      
			} catch (JSONException e) {
				//e.printStackTrace();
				jsFldSpecs = null;
			}
    		return jsFldSpecs;
    	}
    },
	//Division
	STUDY_DIVISION("STUDY", "DIVISION", LC.L_Division) {
    	@Override
        public JSONObject getFieldHTMLSpecJSON() {
        	JSONObject jsFldSpecs = new JSONObject();
        	try {
        		jsFldSpecs.put(FIELD_TYPE, "dropdown");
        		jsFldSpecs.put(ID, "studyDivision");
        		jsFldSpecs.put(NAME, "studyDivision");
        		jsFldSpecs.put("codelst_type", "study_division");
        	} catch (JSONException e) {
        		//e.printStackTrace();
        		jsFldSpecs = null;
        	}
            return jsFldSpecs;
        }
    },
    //CCSG Data Table 4 Reportable
    STUDY_CCSG("STUDY", "CCSG", LC.L_CCSG_DT_Reportable ) {
    	@Override
    	public JSONObject getFieldHTMLSpecJSON() {
    		JSONObject jsFldSpecs = new JSONObject();
    		try {
    			jsFldSpecs.put(FIELD_TYPE, "checkbox");
    			jsFldSpecs.put(ID, "ccsgdt");
    			jsFldSpecs.put(NAME, "ccsgdt");
    			jsFldSpecs.put("type", "checkbox");
    		} catch (JSONException e) {
    			//e.printStackTrace();
    			jsFldSpecs = null;
    		}
    		return jsFldSpecs;
    	}
    },
    //Estimated Begin Date
    STUDY_ESTIMATEDBEGINDATE("STUDY", "ESTIMATEDBEGINDATE", LC.L_Estimated_BeginDate) {
    	@Override
    	public JSONObject getFieldHTMLSpecJSON() {
    		JSONObject jsFldSpecs = new JSONObject();
    		try {
    			jsFldSpecs.put(FIELD_TYPE, "input");
    			jsFldSpecs.put(ID, "studyEstBgnDate");
    			jsFldSpecs.put(NAME, "studyEstBgnDate");
    			jsFldSpecs.put("class", "datefield hasDatepicker");
    			jsFldSpecs.put("type", "text");
    			jsFldSpecs.put("readonly", "READONLY");
    			jsFldSpecs.put("maxlength", "20");
    			jsFldSpecs.put("size", "20");
    		} catch (JSONException e) {
    			//e.printStackTrace();
    			jsFldSpecs = null;
    		}
    		return jsFldSpecs;
    	}
    },
	//Phase
	STUDY_PHASE("STUDY", "STUDYPHASE", LC.L_Phase) {
    	@Override
        public JSONObject getFieldHTMLSpecJSON() {
        	JSONObject jsFldSpecs = new JSONObject();
        	try {
        		jsFldSpecs.put(FIELD_TYPE, "dropdown");
        		jsFldSpecs.put(ID, "studyPhase");
        		jsFldSpecs.put(NAME, "studyPhase");
        		jsFldSpecs.put("codelst_type", "phase");
        	} catch (JSONException e) {
        		e.printStackTrace();
        		jsFldSpecs = null;
        	}
            return jsFldSpecs;
        }
    },
	//Research Type
	STUDY_RESEARCHTYPE("STUDY", "RESEARCHTYPE", LC.L_Research_Type) {
    	@Override
        public JSONObject getFieldHTMLSpecJSON() {
        	JSONObject jsFldSpecs = new JSONObject();
        	try {
        		jsFldSpecs.put(FIELD_TYPE, "dropdown");
        		jsFldSpecs.put(ID, "studyResType");
        		jsFldSpecs.put(NAME, "studyResType");
        		jsFldSpecs.put("codelst_type", "research_type");
        	} catch (JSONException e) {
        		//e.printStackTrace();
        		jsFldSpecs = null;
        	}
            return jsFldSpecs;
        }
    },
	//Study Scope
	STUDY_STUDYSCOPE("STUDY", "STUDYSCOPE", LC.L_Study_Scope) {
    	@Override
        public JSONObject getFieldHTMLSpecJSON() {
        	JSONObject jsFldSpecs = new JSONObject();
        	try {
        		jsFldSpecs.put(FIELD_TYPE, "dropdown");
        		jsFldSpecs.put(ID, "studyScope");
        		jsFldSpecs.put(NAME, "studyScope");
        		jsFldSpecs.put("codelst_type", "studyscope");
        	} catch (JSONException e) {
        		//e.printStackTrace();
        		jsFldSpecs = null;
        	}
            return jsFldSpecs;
        }
    },
	//Study Type
	STUDY_STUDYTYPE("STUDY", "STUDYTYPE", LC.L_Study_Type) {
    	@Override
        public JSONObject getFieldHTMLSpecJSON() {
        	JSONObject jsFldSpecs = new JSONObject();
        	try {
        		jsFldSpecs.put(FIELD_TYPE, "dropdown");
        		jsFldSpecs.put(ID, "studyType");
        		jsFldSpecs.put(NAME, "studyType");
        		jsFldSpecs.put("codelst_type", "study_type");
        	} catch (JSONException e) {
        		//e.printStackTrace();
        		jsFldSpecs = null;
        	}
            return jsFldSpecs;
        }
    },
    //Study Linked To
    STUDY_LINKEDTO("STUDY", "LINKEDTO", LC.L_Linked_To) {
    	@Override
    	public JSONObject getFieldHTMLSpecJSON() {
    		JSONObject jsFldSpecs = new JSONObject();
    		try {
    			jsFldSpecs.put(FIELD_TYPE, "input");
    			jsFldSpecs.put(ID, "studyAssocNum");
    			jsFldSpecs.put(NAME, "studyAssocNum");
    			jsFldSpecs.put("maxlength", 20);
    			jsFldSpecs.put("readonly", "READONLY");
        		jsFldSpecs.put("linkText", "Select");
        		jsFldSpecs.put("linkHref", "#");
        		jsFldSpecs.put("onClick", "return studyFunctions.openLookup(53);");
    		} catch (JSONException e) {
    			//e.printStackTrace();
    			jsFldSpecs = null;
    		}
    		return jsFldSpecs;
    	}
    },
    //Blinding
    STUDY_BLINDING("STUDY", "BLINDING", LC.L_Blinding) {
    	@Override
    	public JSONObject getFieldHTMLSpecJSON() {
    		JSONObject jsFldSpecs = new JSONObject();
    		try {
    			jsFldSpecs.put(FIELD_TYPE, "dropdown");
    			jsFldSpecs.put(ID, "studyBlinding");
    			jsFldSpecs.put(NAME, "studyBlinding");
    			jsFldSpecs.put("codelst_type", "blinding");
    		} catch (JSONException e) {
    			//e.printStackTrace();
    			jsFldSpecs = null;
    		}
    		return jsFldSpecs;
    	}
    },
    //Randomization
    STUDY_RANDOMIZATION("STUDY", "RANDOMIZATION", LC.L_Randomization) {
    	@Override
    	public JSONObject getFieldHTMLSpecJSON() {
    		JSONObject jsFldSpecs = new JSONObject();
    		try {
    			jsFldSpecs.put(FIELD_TYPE, "dropdown");
    			jsFldSpecs.put(ID, "studyRandom");
    			jsFldSpecs.put(NAME, "studyRandom");
    			jsFldSpecs.put("codelst_type", "randomization");
    		} catch (JSONException e) {
    			//e.printStackTrace();
    			jsFldSpecs = null;
    		}
    		return jsFldSpecs;
    	}
    },
    //Sponsor
    STUDY_SPONSOR("STUDY", "SPONSOR", LC.L_Sponsor_Name) {
    	@Override
    	public JSONObject getFieldHTMLSpecJSON() {
    		JSONObject jsFldSpecs = new JSONObject();
    		try {
    			jsFldSpecs.put(FIELD_TYPE, "lookup");
    			jsFldSpecs.put(ID, "sponsor1");
    			jsFldSpecs.put(NAME, "sponsor1");
    			jsFldSpecs.put("readonly", "READONLY");
        		jsFldSpecs.put("codelst_type", "sponsor");
        		jsFldSpecs.put("linkText", "Select Sponsor");
        		jsFldSpecs.put("linkHref", "#");
        		jsFldSpecs.put("onClick", "return studyFunctions.openLookupSpnsr(studyFunctions.formObj);");
    		} catch (JSONException e) {
    			//e.printStackTrace();
    			jsFldSpecs = null;
    		}
    		return jsFldSpecs;
    	}
    },
	//Sponsor Name, If Other  
	STUDY_SPONSOR_IFOTHER("STUDY", "SPONSOR_IFOTHER", LC.L_If_Other) {
    	@Override
        public JSONObject getFieldHTMLSpecJSON() {
        	JSONObject jsFldSpecs = new JSONObject();
        	try {
        		jsFldSpecs.put(FIELD_TYPE, "input");
        		jsFldSpecs.put(ID, "ifothersponsor");
        		jsFldSpecs.put(NAME, "studySponsor");
        		jsFldSpecs.put("size", 20);
        		jsFldSpecs.put("maxlength", 200);
        		jsFldSpecs.put("type", "text");
        		jsFldSpecs.put("codelst_type", "sponsor");
        	} catch (JSONException e) {
        		//e.printStackTrace();
        		jsFldSpecs = null;
        	}
            return jsFldSpecs;
        }
    },
    //Sponsor ID
    STUDY_SPONSORID("STUDY", "SPONSORID", LC.L_Sponsor_Id) {
    	@Override
    	public JSONObject getFieldHTMLSpecJSON() {
    		JSONObject jsFldSpecs = new JSONObject();
    		try {
    			jsFldSpecs.put(FIELD_TYPE, "input");
    			jsFldSpecs.put(ID, "studySponsorIdInfo");
    			jsFldSpecs.put(NAME, "studySponsorIdInfo");
    			jsFldSpecs.put("type", "text");
        		jsFldSpecs.put("size", 20);
    			jsFldSpecs.put("maxlength", "50");
    		} catch (JSONException e) {
    			//e.printStackTrace();
    			jsFldSpecs = null;
    		}
    		return jsFldSpecs;
    	}
    },    
    //Contact
    STUDY_CONTACT("STUDY", "CONTACT", LC.L_Contact) {
    	@Override
    	public JSONObject getFieldHTMLSpecJSON() {
    		JSONObject jsFldSpecs = new JSONObject();
    		try {
    			jsFldSpecs.put(FIELD_TYPE, "input");
    			jsFldSpecs.put(ID, "studyContact");
    			jsFldSpecs.put(NAME, "studyContact");
    			jsFldSpecs.put("type", "text");
    			jsFldSpecs.put("size", 20);
    			jsFldSpecs.put("maxlength", 50);
    		} catch (JSONException e) {
    			//e.printStackTrace();
    			jsFldSpecs = null;
    		}
    		return jsFldSpecs;
    	}
    },
    //Keywords
    STUDY_KEYWORDS("STUDY", "KEYWORDS", LC.L_Keywords) {
    	@Override
    	public JSONObject getFieldHTMLSpecJSON() {
    		JSONObject jsFldSpecs = new JSONObject();
    		try {
    			jsFldSpecs.put(FIELD_TYPE, "textarea");
    			jsFldSpecs.put(ID, "studyKeywrds");
    			jsFldSpecs.put(NAME, "studyKeywrds");
    			jsFldSpecs.put("maxlength", "500");
    			jsFldSpecs.put("cols", "50");
    			jsFldSpecs.put("rows", "2");
    		} catch (JSONException e) {
    			//e.printStackTrace();
    			jsFldSpecs = null;
    		}
    		return jsFldSpecs;
    	}
    },
    // If this is checked, we would like the CREATION_TYPE of ER_STUDY to be set to null; otherwise set to 'A'.
    STUDY_NOT_FOR_IRB("STUDY", "NOT_FOR_IRB", LC.L_NotForIRB) {
    	@Override
    	public JSONObject getFieldHTMLSpecJSON() {
    		JSONObject jsFldSpecs = new JSONObject();
    		try {
    			jsFldSpecs.put(FIELD_TYPE, "checkbox");
    			jsFldSpecs.put(ID, "notForIRB");
    			jsFldSpecs.put(NAME, "notForIRB");
    			jsFldSpecs.put("type", "checkbox");
    		} catch (JSONException e) {
    			//e.printStackTrace();
    			jsFldSpecs = null;
    		}
    		return jsFldSpecs;
    	}
    },
	;
	
	private String pageKeyword;
	private String fieldKeyword;
	private String fieldLabel;
	public abstract JSONObject getFieldHTMLSpecJSON();

	private final static String FIELD_TYPE = "fieldType";
	private final static String ID = "id";
	private final static String NAME = "name";

	public String getFlxPageFieldKey() { return this.toString(); }
	public String getPageKeyword() { return pageKeyword; }
	public String getFieldKeyword() { return fieldKeyword;}
	public String getFieldLabel() { return fieldLabel;}

	FlxPageFields(String page, String fieldKeyword, String fieldLabel) {
		this.pageKeyword = page;
		this.fieldKeyword = fieldKeyword;
		this.fieldLabel = fieldLabel;
	}

	public static FlxPageFields getFlxPageFieldByKey(String fldEnumKey) {
		if (fldEnumKey == null) { return null; }
		try {
			return FlxPageFields.valueOf(fldEnumKey);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String getFieldSpec_STD(String fldEnumKey) { 
		JSONObject fieldJSON = new JSONObject();
		FlxPageFields pageField = getFlxPageFieldByKey(fldEnumKey);

		if (null == pageField) {
			return null;
		}
			
        if (pageField.name().equals(fldEnumKey)) {
			try {
				fieldJSON.put("type", "STD");
				fieldJSON.put("flxFieldId",  pageField.name());
		    	fieldJSON.put("keyword", pageField.name());
		    	fieldJSON.put("label", pageField.fieldLabel);
				return fieldJSON.toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}
        }
		return fieldJSON.toString();
	}

	public static String getFieldSpec_StudyMD(String fldCodePKey, int defUserGrp) { 
		JSONObject fieldJSON = new JSONObject();
		//CodeDao.get
			
		try {
			fieldJSON.put("type", "MSD");
			return fieldJSON.toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return fieldJSON.toString();
	}

	public static String getFieldSpec(Object...args) { 
		String fldPage = (args[0]).toString();
		String fldType = (args[1]).toString();
		String fldEnumKey = (args[2]).toString();
		
		if ("STD".equals(fldType)) {
			return getFieldSpec_STD(fldEnumKey);
		}
		if ("MSD".equals(fldType)) {
			if ("STUDY".equals(fldPage)) {
				int defUserGrp = StringUtil.stringToNum((args[3]).toString());
				return getFieldSpec_StudyMD(fldEnumKey, defUserGrp);
			}
		}
		return null;
	}
	
	public static JSONArray getFieldsForPage(String page) {
		JSONArray pagefieldJSArray = new JSONArray();
		if (StringUtil.isEmpty(page)) return null;
		int indx = 0;
		JSONObject fieldJSON = null;
		
		for (FlxPageFields pageField : FlxPageFields.values()) {
			fieldJSON = new JSONObject();
			
	        if (pageField.getPageKeyword().equals(page)) {
	        	try {
	        		fieldJSON.put("seq", indx);
	        		fieldJSON.put("type", "STD");
	        		fieldJSON.put("flxFieldId",  pageField.name());
	        		fieldJSON.put("keyword", pageField.name());
		        	fieldJSON.put("label", pageField.fieldLabel);
		        	pagefieldJSArray.put(fieldJSON);
					indx++;
				} catch (JSONException e) {
					e.printStackTrace();
				}
	        }
	    }
		return pagefieldJSArray;
	}
	
	public static JSONArray getUnusedFieldsForPage_STD(String page, ArrayList<String> pageSectionFieldsList) {
		if (StringUtil.isEmpty(page)) return null;
		
		JSONArray pagefieldJSArray = new JSONArray();
		int seq = 0;
		JSONObject fieldJSON = null;
		
		for (FlxPageFields pageField : FlxPageFields.values()) {
			fieldJSON = new JSONObject();
			
			if (pageField.getPageKeyword().equals(page) 
					&& pageSectionFieldsList.indexOf(pageField.getFlxPageFieldKey()) >= 0) {
				continue;
			}
			
	        if (pageField.getPageKeyword().equals(page)) {
	        	try {
	        		fieldJSON.put("seq", seq);
	        		fieldJSON.put("type", "STD");
	        		fieldJSON.put("flxFieldId",  pageField.name());
		        	fieldJSON.put("keyword", pageField.name());
		        	fieldJSON.put("label", pageField.fieldLabel);
					pagefieldJSArray.put(fieldJSON);
					seq++;
				} catch (JSONException e) {
					e.printStackTrace();
				}
	        }
	    }
		return pagefieldJSArray;
	}
	

}
