<%--

	Project Name:		Velos eResearch
	Author:				Jnanamay Majumdar	
	Created on Date:	15Nov2007
	Purpose:			Gui for Storage Update Status
	File Name:			editmultiplestoragestatus.jsp

--%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title> <%=MC.M_Updt_MultiStorStat%><%--Update Multiple Storage Status*****--%></title>


<%@ page import ="java.util.*, java.io.*, org.w3c.dom.*,com.velos.eres.business.common.*"%>
<meta http-equiv="Content-type" content="text/html; charset=UTF-8">
<jsp:include page="include.jsp" flush="true"/>

</head>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="whichcss.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>

<script Language="javascript"> 

function openLookup(formobj, name, cntr) {

				
	formobj.target="Lookup";
	formobj.method="post";	
	
	if (name=='study'){
	
	formobj.action="multilookup.jsp?viewId=6013&form=storagestat&seperator=,"+
                  "&keyword=selStudy"+cntr+"|STUDY_NUMBER~selStudyIds"+cntr+"|LKP_PK|[VELHIDE]&maxselect=1";
	
	
	}else if ( name=='user'){

	formobj.action="multilookup.jsp?viewId=6000&form=storagestat&seperator=,"+
		"&keyword=userFName"+cntr+"|USRFNAME|[VELHIDE]~userLName"+cntr+"|USRLNAME|[VELHIDE]~creatorId"+cntr+"|USRPK|[VELHIDE]~createdBy"+cntr+"|[VELEXPR]*[VELKEYWORD=USRFNAME]*[VELSTR=]*[VELSPACE]*[VELKEYWORD=USRLNAME]&maxselect=1";
	
	}
	formWin =open('donotdelete.html','Lookup','resizable=1,status=0, width=850,height=550 top=100,left=100,menubar=no,scrollbars=1');
	if (formWin && !formWin.closed) formWin.focus();	
	formobj.submit();
	formobj.target="_self";
	formobj.action="editmultiplestoragestatussave.jsp";
	void(0);
}


//KM-#INVP2.3.1
 function updateAll(formobj)
 {

	totrows = formobj.cntNumrows.value;
	var idx = formobj.storStatTop.options.selectedIndex ;
	var status = formobj.storStatTop.options[idx].value;

	dtVal = formobj.statDtTop.value;

	selStudyIdsVal = formobj.selStudyIdsTop.value;
	selStudyVal = formobj.selStudyTop.value;
	creatorIdVal = formobj.creatorIdTop.value;
	createdByVal = formobj.createdByTop.value;
	userLNameVal = formobj.userLNameTop.value;
	userFNameVal = formobj.userFNameTop.value;
	statusNotes = formobj.statusNotesTop.value;


	for(i=1;i<=totrows;i++) {
		storStat = "formobj.storStat" + i;	
		eval(storStat).value = status;

		statDt = "formobj.statDt"+i;
		eval(statDt).value = dtVal;

		selStudyIds = "formobj.selStudyIds"+i;
		eval(selStudyIds).value = selStudyIdsVal;

		selStudy = "formobj.selStudy"+i;
		eval(selStudy).value = selStudyVal;

		creatorId = "formobj.creatorId"+i;
		eval(creatorId).value = creatorIdVal;

		createdBy = "formobj.createdBy"+i;
		eval(createdBy).value = createdByVal;

		userLName = "formobj.userLName"+i;
		eval(userLName).value = userLNameVal;

		userFName = "formobj.userFName"+i;
		eval(userFName).value = userFNameVal;

		eval("formobj.statusNotes"+i).value  = statusNotes;

	}
 }
 
 function validateme(formObj){
 	counter = 0; 
 	totRows = formObj.cntNumrows.value;	 	 	
 	 	
 	 	
 	 	
 	for(cnt = 1; cnt<=totRows; cnt++){ 
 	
 		 
 	storStat = "formObj.storStat" + cnt;	
	storStats=eval(storStat).value;
	
	
	statDt = "formObj.statDt" + cnt;	
	statDts=eval(statDt).value;
	
	if (storStats == '' && statDts ==''){
        counter++;
            
	    if (counter == totRows){
	        alert("<%=MC.M_AtleastOne_StorStat%>");/*alert("Please enter atleast one Storage Status");*****/
	    	return false;
	    }
	}
	
	
	if ( (storStats == '' && statDts !='') || (storStats != '' && statDts =='') ){
    alert("<%=MC.M_Etr_MandantoryFlds%>");/*alert("Please enter data in all mandatory fields");*****/
    return false;     
    }	
    
    

}
	if(!(validate_col('e-Signature',formObj.eSign))) return false
// 	if (isNaN(formObj.eSign.value) == true){
<%-- 		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/ --%>
// 		formObj.eSign.focus();
// 		return false;		
// 	}

 }




</script>

<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.StringUtil"%>
<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*, com.velos.eres.web.user.UserJB"%>
<jsp:useBean id="StorageStatJB" scope="request" class="com.velos.eres.web.storageStatus.StorageStatusJB"/>

<jsp:useBean id="sess" scope="request" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="specJB" scope="request" class="com.velos.eres.web.specimen.SpecimenJB"/>
<jsp:useBean id="stdJB" scope="request" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id="userJB" scope="request" class="com.velos.eres.web.user.UserJB"/>




<BODY style="overflow:scroll"> <!-KM-15Oct08-->
<%
HttpSession tSession = request.getSession(true);
if (sess.isValidSession(tSession)){	
	String usrId = "",usrName ="";
	String userIdFromSession = (String) tSession.getValue("userId");
	String accId = (String) tSession.getValue("accountId");
	
	String selStrs = request.getParameter("selStrs");	
 	if (selStrs==null) selStrs="";	
	
	

	String mode= "";
	mode = request.getParameter("mode");
	if (EJBUtil.isEmpty(mode))
		mode="N";
	
	  
	//create the status drop down but inside the loop below
	CodeDao cdSpecStat = new CodeDao();
	cdSpecStat.getCodeValues("storage_stat");	
	String storStat = "";
	

	String mainsql = "";
	String countsql = "";  
	
	StringBuffer sbSelect = new StringBuffer();
	StringBuffer sbFrom = new StringBuffer();
	StringBuffer sbWhere = new StringBuffer();
	
	

	sbSelect.append(" SELECT b.pk_storage_status,b.fk_storage,a.storage_name,STORAGE_CAP_NUMBER,");
	sbSelect.append("(SELECT COUNT(*) FROM er_storage c WHERE c.fk_storage=a.pk_storage) child_allocation_count,(SELECT COUNT(*) FROM er_specimen WHERE fk_storage=a.pk_storage) specimen_allocation_count ");
	sbFrom.append(" FROM er_storage a, er_storage_status b ") ; 
	
	sbWhere.append(" WHERE a.fk_account = " + accId +" AND a.pk_storage = b.fk_storage AND a.STORAGE_NAME IS NOT NULL ");	
	
	sbWhere.append(" AND a.pk_storage IN (" + selStrs + ")");	
	
	sbWhere.append(" AND b.pk_storage_status IN (SELECT pk_storage_status FROM er_storage_status WHERE pk_storage_status = (SELECT pk_storage_status FROM er_storage_status WHERE fk_storage = pk_storage ");
	sbWhere.append(" AND pk_storage_status =(SELECT MAX(pk_storage_status)FROM er_storage_status WHERE fk_storage = pk_storage AND SS_START_DATE =(SELECT MAX(SS_START_DATE)    FROM er_storage_status WHERE fk_storage = pk_storage))))");
	
	
     


 			mainsql = sbSelect.toString() + sbFrom.toString() + sbWhere.toString(); 	
			//out.println("mainsql=====>"+mainsql);
		    countsql = " Select count(*) from ( " + mainsql + " )";	 
	 
			String pagenum = "";
			int curPage = 0;
			long startPage = 1;
			long cntr = 0;
			

			pagenum = request.getParameter("page");
			if (pagenum == null)
			{
			pagenum = "1";
			}
			curPage = EJBUtil.stringToNum(pagenum);
			
			String orderBy = "";
			orderBy = request.getParameter("orderBy");
			
			if (EJBUtil.isEmpty(orderBy)){			
			//orderBy = "lower(STORAGE_ID)	";   
			}

			String orderType = "";
			
			orderType = request.getParameter("orderType");

			if (EJBUtil.isEmpty(orderType))
			{
			//orderType = "asc";		
			 
			}
			

			long rowsPerPage=0;
			long totalPages=0;	
			long rowsReturned = 0;			
			long showPages = 0;
			long totalRows = 0;	   
			long firstRec = 0;
			long lastRec = 0;	   
			boolean hasMore = false;
			boolean hasPrevious = false;

		
			rowsPerPage =  Configuration.ROWSPERBROWSERPAGE ;
			totalPages =Configuration.PAGEPERBROWSER ;
		
			BrowserRows br = new BrowserRows();					
			
			br.getPageRows(curPage,rowsPerPage,mainsql,totalPages,countsql,orderBy,orderType);
			
	    	rowsReturned = br.getRowReturned();	    	
			showPages = br.getShowPages();
			startPage = br.getStartPage();
			hasMore = br.getHasMore();
			hasPrevious = br.getHasPrevious();
			totalRows = br.getTotalRows();	   	   
			firstRec = br.getFirstRec();
			lastRec = br.getLastRec();
			
			
			
			
			

%>

<p class = "sectionHeadings" ><%=MC.M_Updt_MultiStorStat%><%--Update Multiple Storage Status*****--%></P>
<br>


<form name="storagestat" id="multstoragestat" method=post action="editmultiplestoragestatussave.jsp" onsubmit="if (validateme(document.storagestat)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">


<DIV class="popDefault" id="div1">
<table width="100%">
	
	
	<tr>	
	<th width="15%"> <%=LC.L_Storage_UnitName%><%--Storage Unit Name*****--%> </th>
	<th width="15%"> <%=LC.L_Status%><%--Status*****--%> <FONT class="Mandatory">* </FONT></th>
	<th width="16%"> <%=LC.L_Status_Date%><%--Status Date*****--%><FONT class="Mandatory">* </FONT></th>			
	<th width="20%"> <%=LC.L_For_Study%><%--For Study*****--%></th>					
	<th width="21%"> <%=LC.L_For_User%><%--For User*****--%></th>		
	<th width="15%"> <%=LC.L_Notes%><%--Notes*****--%> </th>
	</tr>

<br>

<%
	String dStorStatTop =cdSpecStat.toPullDown("storStatTop");
%>

<tr>
<td></td>
<td> <%=dStorStatTop%> </td>
<%-- INF-20084 Datepicker-- AGodara --%>
<td ><Input type = "text" name="statDtTop" size=10 align = right readonly class="datefield"></td>
	
	<input type="hidden" name="selStudyIdsTop" > 
	<td >
	<Input type = "text" name="selStudyTop"  size=10 readonly >
					
	<A href="#" onClick="openLookup(document.storagestat, 'study', 'Top')" ><%=LC.L_Select%><%--SELECT*****--%></A>
	</td>
	
	<td >
	<input type="hidden" name="creatorIdTop" size = 10 >
	<input type="text" name="createdByTop" size = 10 readonly>
	
	<input type="hidden" name="userLNameTop" size = 10 >
	<input type="hidden" name="userFNameTop" size = 10 >

	<A href=# onClick="return openLookup(document.storagestat,'user', 'Top');"><%=LC.L_Select%><%--SELECT*****--%> </A>
		 
	</td>	
	
	<td >
	<textarea name="statusNotesTop" rows=1 cols=15 MAXLENGTH = 20000>
	</textarea>
	</td>
	<td nowrap><a href=# onclick="updateAll(document.storagestat)" ><%=LC.L_Set_ToAll%><%--Set To All*****--%> </a></td>

</tr>
<tr>
	<td></td>
	<td colspan="5"><hr class="thinLine"></td>
</tr>


<%	
			String storageStatusPk = "";
			String fkStorage = "";
			String storageId = "";
			String storageName = "";
			String studyNumber = "";
			String studyId="";
			String stNotes = "";
			String stUserFK = "";
			String userFName="";
			String userLName="";
			String stStudyFk="";
			String stStatus = "";
			String  statusStartDt = "";
			String codeSubType = "";
			String specAllocation = "";
			String childAllocation = "";
			String childCapCount = "";
			StringBuffer stStatbuffer = new StringBuffer();
			
			CodeDao cdStatus = new CodeDao();
			//BK,MAR-28
		    int  def_studyId ;
		for(int i = 1 ; i <=rowsReturned ; i++){
			def_studyId = 0;	
			 storageStatusPk = br.getBValues(i,"pk_storage_status");
			 storageStatusPk=(storageStatusPk==null)?"":storageStatusPk;	
			 
			 fkStorage = br.getBValues(i,"fk_storage");	
			 fkStorage=(fkStorage==null)?"":fkStorage;			
			 
			 storageName = br.getBValues(i,"storage_name");	
			 storageName=(storageName==null)?"":storageName;
			 
			 
			 StorageStatJB.setPkStorageStat(StringUtil.stringToNum(storageStatusPk));
			 StorageStatJB.getStorageStatusDetails();
			 stNotes = StorageStatJB.getSsNotes();	
			 stNotes=(stNotes==null)?"":stNotes;
			 stUserFK = StorageStatJB.getFkUser();
			 stUserFK=(stUserFK==null)?"":stUserFK;
			 stStudyFk = StorageStatJB.getFkStudy();
			 stStudyFk=(stStudyFk==null)?"":stStudyFk;
			 stStatus = StorageStatJB.getFkCodelstStorageStat();
			 stStatus=(stStatus==null)?"":stStatus;
			 statusStartDt = StorageStatJB.getSsStartDate();
			 statusStartDt=(statusStartDt==null)?"":statusStartDt;
			 specAllocation = br.getBValues(i,"specimen_allocation_count");	
			 childAllocation = br.getBValues(i,"child_allocation_count");	
			 childCapCount = br.getBValues(i,"STORAGE_CAP_NUMBER");
			 codeSubType = cdStatus.getCodeSubtype(StringUtil.stringToNum(stStatus));
			 //BK,MAR-28-11,4638
			 //BK,MAY-23-11,4638
			 def_studyId= StorageStatJB.getDefStatStudyId(EJBUtil.stringToNum(fkStorage));		 
			 				
					stdJB.setId(def_studyId);
					stdJB.getStudyDetails();
					studyNumber = stdJB.getStudyNumber();
					studyNumber = (def_studyId==0)?"":studyNumber;					
			 
			 int specIdLen = storageId.length();
			 if (specIdLen > 50)
			 storageId = storageId.substring(1,50)+"...";		 


	
	
	%>

<%if(!("PartOccupied".equals(codeSubType) || "Occupied".equals(codeSubType)) || ("PartOccupied".equals(codeSubType) && StringUtil.stringToNum(specAllocation) == 0) || ("Occupied".equals(codeSubType) && StringUtil.stringToNum(specAllocation) == 0))
{%>
	<input type="hidden" name="storageStatPk<%=i%>" value="<%=storageStatusPk%>">

	<input type="hidden" name="fkStorag<%=i%>" value="<%=fkStorage%>">
	<tr >
	
	
	
	<td ><%=storageName%>		</td>
	
	
	<td >
	<%
	
	storStat = "storStat" + i ;	
	String dStorStat =cdSpecStat.toPullDown(""+storStat);
	 		
	%>
	
	<%=dStorStat%>
	</td>		
<%-- INF-20084 Datepicker-- AGodara --%>
	<td ><Input type = "text" name="statDt<%=i%>" size=10 align = right readonly class="datefield"></td>	
	<%	
	studyId = def_studyId != 0 ? ""+def_studyId : "";
	%>	
	<Input type="hidden" name="selStudyIds<%=i%>"  > 
	<td >
	<Input type = "text" name="selStudy<%=i%>" size=10 readonly >
					
	<A href="#" onClick="openLookup(document.storagestat, 'study', <%=i%>)" ><%=LC.L_Select%><%--SELECT*****--%></A>
	</td>
	
	<td >
	<input type="hidden" name="creatorId<%=i%>" size = 10 >
	<input type="text" name="createdBy<%=i%>" size = 10 readonly>
	
	<input type="hidden" name="userLName<%=i%>" size = 10 >
	<input type="hidden" name="userFName<%=i%>" size = 10 >

	<A href=# onClick="return openLookup(document.storagestat,'user', <%=i%>);"><%=LC.L_Select%><%--SELECT*****--%> </A>
		 
	</td>	
	
	
	
	<td >
	<textarea name="statusNotes<%=i%>" rows=1 cols=15 MAXLENGTH = 20000>
	</textarea>
	</td>
	
	
	
	
     </tr>
		<%}else{%>
	<input type="hidden" name="storageStatPk<%=i%>" value="<%=storageStatusPk%>">

	<input type="hidden" name="fkStorag<%=i%>" value="<%=fkStorage%>">
		
    <tr >
	
	
	
	<td ><%=storageName%>		</td>
	
	
	<td >
	<%
	stStatbuffer = new StringBuffer();
	storStat = "storStat" + i ;	
	String dStorStat =cdSpecStat.toPullDown(""+storStat,StringUtil.stringToNum(stStatus));
	int len = dStorStat.length();
	int strIndex = dStorStat.indexOf(">");

	String strSel =  dStorStat.substring(0, strIndex);
	strSel = strSel + " "+ "disabled>";
	stStatbuffer.append(strSel);

	String strApp = dStorStat.substring(strIndex+1, len);

	stStatbuffer.append(strApp);
	dStorStat = stStatbuffer.toString(); 		
	%>
	
	<%=dStorStat%>
	<Input type="hidden" name="storStat<%=i%>"  value =""> 
	</td>		
	<td ><Input type = "text" name="statDt<%=i%>" value="<%=statusStartDt%>" size="10" align = "right" disabled>
	<Input type = "hidden" name="statDt<%=i%>" value="<%=statusStartDt%>">
	</td>	
	<%
	stdJB.setId(StringUtil.stringToNum(stStudyFk));
	stdJB.getStudyDetails();
	studyNumber = stdJB.getStudyNumber();
	studyNumber = (StringUtil.stringToNum(stStudyFk)==0)?"":studyNumber;
	studyId = StringUtil.stringToNum(stStudyFk) != 0 ? ""+stStudyFk : "";
	%>	
	<Input type="hidden" name="selStudyIds<%=i%>"  value ="<%=studyId%>"> 
	<td >
	<Input type = "text" name="selStudys<%=i%>" value="<%=studyNumber%>" size=10 disabled >
	<Input type="hidden" name="selStudy<%=i%>"  value ="<%=studyId%>">				
	</td>
	<%userJB.setUserId(StringUtil.stringToNum(stUserFK));
	  userJB.getUserDetails();
	  userFName = (StringUtil.stringToNum(stUserFK)==0)?"":userJB.getUserFirstName();
	  userLName = (StringUtil.stringToNum(stUserFK)==0)?"":userJB.getUserLastName();
	  %>
	<td >
	<input type="hidden" name="creatorId<%=i%>" size = 10 value="<%=stUserFK%>">
	<input type="text" name="createdBys<%=i%>" size = 10 disabled value="<%=userFName+" "+userLName%>">
	<input type="hidden" name="createdBy<%=i%>" size = 10 disabled value="<%=userFName+" "+userLName%>">
	<input type="hidden" name="userLName<%=i%>" size = 10 value="<%=userLName%>">
	<input type="hidden" name="userFName<%=i%>" size = 10 value="<%=userFName%>">
	</td>	
	
	
	
	<td >
	<textarea name="statusNotes<%=i%>" rows=1 cols=15 MAXLENGTH = 20000 disabled><%=stNotes%>
	</textarea>
	<input type="hidden" name="statusNotes<%=i%>" value="<%=stNotes%>">
	</td>
	
	
	
	
     </tr>
		
		
		<%}
	       }//end of for loop 

		%>
</table>
<input type="hidden" name="cntNumrows" value="<%=rowsReturned%>">
<BR>
<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="multstoragestat"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>

</form>

<%

}
//end of session
else{
%>

<jsp:include page="timeout.html" flush='true'></jsp:include>
<%}%>
   </div>


<div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
</div>

  
</BODY>

</html> 