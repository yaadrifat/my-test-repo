<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></HEAD>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.aithent.audittrail.reports.AuditUtils" %>
</head>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventuser" scope="request" class="com.velos.esch.web.eventuser.EventuserJB"/>
<%@ page language = "java" import = "com.velos.esch.service.util.EJBUtil,com.velos.esch.web.eventuser.EventuserJB,com.velos.eres.service.util.*"%>

<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>



<%
   String fromPage = request.getParameter("fromPage");
if ((fromPage.equals("selectEvent")) || (fromPage.equals("fetchProt")))
{ %>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>
 
<% } //end for if ((request.getParameter("fromPage")).equals("selectEvent"))
%>

<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

<%-- 	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   } --%>
}
</SCRIPT>
<jsp:include page="skinChoser.jsp" flush="true"/>

<% String src;
src= request.getParameter("srcmenu");
if ((fromPage.equals("selectEvent")) || (fromPage.equals("fetchProt"))){
%>
<jsp:include page="include.jsp" flush="true"/>
<%

}
 else{
%>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<%}%>

<body>

<br>

<% if ((fromPage.equals("selectEvent")) || (fromPage.equals("fetchProt"))){%>

		<DIV class="formDefaultpopup" id="div1">

	<%	}

else { %>

<DIV class="formDefault" id="div1">

<%}%>

<%

String duration = request.getParameter("duration");
String protocolId = request.getParameter("protocolId");
String calledFrom = request.getParameter("calledFrom");
String eventId = request.getParameter("eventId");
String mode = request.getParameter("mode");
String calStatus = request.getParameter("calStatus");
String eventmode = request.getParameter("eventmode");
String displayDur=request.getParameter("displayDur");
String displayType=request.getParameter("displayType");
String eventuserId = request.getParameter("eventuserId");
//String eventName = request.getParameter("eventName");
String eventName = "";//KM
String calAssoc=request.getParameter("calassoc");

String propagateInVisitFlag = request.getParameter("propagateInVisitFlag"); //SV, 9/16/04, cal-enh-05
String propagateInEventFlag = request.getParameter("propagateInEventFlag");
String userType = request.getParameter("userType");
String propagationType = "";

calAssoc=(calAssoc==null)?"":calAssoc;


if (calledFrom.equals("P")||calledFrom.equals("L"))
{
	  eventdefB.setEvent_id(EJBUtil.stringToNum(eventId));
	  eventdefB.getEventdefDetails();
	  eventName = eventdefB.getName();
//JM: 12Nov2009: #4399
  	  eventName = (eventName==null)?"":eventName;
  	  if (eventName.length()>1000){//for the old existing event names > 1000 or larger
  		eventName=eventName.substring(0,1000);
  	  }
}else{
	  eventassocB.setEvent_id(EJBUtil.stringToNum(eventId));
	  eventassocB.getEventAssocDetails();
	  eventName = eventassocB.getName();
//JM: 12Nov2009: #4399
  	  eventName = (eventName==null)?"":eventName;
  	  if (eventName.length()>1000){//for the old existing event names > 1000 or larger
  		eventName=eventName.substring(0,1000);
  	  }
 }



HttpSession tSession = request.getSession(true);
   if (sessionmaint.isValidSession(tSession)) {
    String delMode=request.getParameter("delMode");
	if (delMode==null) {
		delMode="final";
%>

	<FORM name="deleteUsr" id="delevtuser" method="post" action="eventuserdelete.jsp" onSubmit="if (validate(document.deleteUsr)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<br><br>
	  <input type="hidden" name="delMode" value="<%=delMode%>">
  	 <input type="hidden" name="srcmenu" value="<%=src%>">

  	 <input type="hidden" name="eventId" value="<%=eventId%>">
  	 <input type="hidden" name="duration" value="<%=duration%>">
  	 <input type="hidden" name="protocolId" value="<%=protocolId%>">
  	 <input type="hidden" name="calledFrom" value="<%=calledFrom%>">
  	 <input type="hidden" name="mode" value="<%=mode%>">
  	 <input type="hidden" name="calStatus" value="<%=calStatus%>">
  	 <input type="hidden" name="fromPage" value="<%=fromPage%>">
   	 <input type="hidden" name="eventmode" value="<%=eventmode%>">
  	 <input type="hidden" name="displayDur" value="<%=displayDur%>">
  	 <input type="hidden" name="displayType" value="<%=displayType%>">
   	 <input type="hidden" name="eventuserId" value="<%=eventuserId%>">
   	 <input type="hidden" name="eventName" value="<%=eventName%>">
   	 <input type="hidden" name="calassoc" value="<%=calAssoc%>">


   	 <input type="hidden" name="userType" value="<%=userType%>">


	<jsp:include page="propagateEventUpdate.jsp" flush="true">

		<jsp:param name="fromPage" value="<%=fromPage%>"/>

		<jsp:param name="formName" value="deleteUsr"/>

		<jsp:param name="eventName" value="<%=eventName%>"/>

		</jsp:include>

	<P class="defComments"><%=MC.M_EsignToProc_ResrcDelFromEvt%><%-- Please enter e-Signature to proceed with Resource Delete from Event.*****--%></P>
	<jsp:include page="submitBar.jsp" flush="true">
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="delevtuser"/>
			<jsp:param name="showDiscard" value="N"/>
	</jsp:include>





	</FORM>
<%
	} else {
			String eSign = request.getParameter("eSign");
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>
	 		  <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
			} else {


				if ((propagateInVisitFlag == null) || (propagateInVisitFlag.equals("")))

					propagateInVisitFlag = "N";

				if ((propagateInEventFlag == null) || (propagateInEventFlag.equals("")))

				propagateInEventFlag = "N";

				 if (userType.equals("R"))
				 {
				 	propagationType  = "EVENT_USER"; //handle it like normal user
				 }
				 else if (userType.equals("U"))
				 {
				 	propagationType = "EVENT_USER_USER";
				 }

				if ( (propagateInVisitFlag.equals("Y")) || (propagateInEventFlag.equals("Y")))
				{
					//sonia - this will delete the main event too
					eventuser.setEventuserId(EJBUtil.stringToNum(eventuserId));
					eventuser.setUserType(userType);
					eventuser.setEventId(eventId);
					System.out.println("eventId" + eventId);
					System.out.println("eventuserId" + eventuserId);
			   		eventuser.propagateEventuser(EJBUtil.stringToNum(protocolId), propagateInVisitFlag, propagateInEventFlag, "D", calledFrom, propagationType);

				} else
				{
					//Modified for INF-18183 ::: Ankit
					String moduleName = null;
					if (calledFrom.equals("L")) {
				    	moduleName = LC.L_Evt_Lib; /*Event Library*/
				    }else if (calledFrom.equals("P")) {
				    	moduleName = LC.L_Library_Calendar; /*Library Calendar*/
				    }else {
				    	moduleName = LC.L_Study_Calendar; /*Study Calendar*/
				    }
				   eventuser.removeEventuser(EJBUtil.stringToNum(eventuserId), AuditUtils.createArgs(session,"",moduleName));
				}
%>


<br><br><br><br><br><p class = "successfulmsg" align = center><%=MC.M_Data_DelSucc%><%-- Data deleted successfully.*****--%></p>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=eventresource.jsp?eventmode=<%=eventmode%>&mode=<%=mode%>&duration=<%=duration%>&srcmenu=tdmenubaritem3&selectedTab=5&eventId=<%=eventId%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&calassoc=<%=calAssoc%>">
<%
		} //end esign
	} //end of delMode
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}

if ((fromPage.equals("selectEvent")) || (fromPage.equals("fetchProt"))){

%>
<div class = "myHomebottomPanel">
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
<%


}
 else{
%>
  <div class = "myHomebottomPanel">
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</DIV>

<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/>
</div>
<%}%>
</BODY>
</HTML>


