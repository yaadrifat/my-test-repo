<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true" />
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true" />
<BODY>
	<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint" />
	<jsp:include page="include.jsp" flush="true" />

	<%@ page language="java"
		import="com.velos.eres.business.study.*,com.velos.eres.business.common.*,java.util.*,java.text.*,com.velos.eres.service.util.*,com.velos.eres.web.account.AccountJB,com.velos.esch.business.common.*,com.velos.eres.compliance.web.*"%>
	<%@ page
		import="com.velos.eres.widget.business.common.ResubmDraftDao,com.velos.eres.widget.service.util.FlxPageArchive"%>
	<%
		HttpSession tSession = request.getSession(true);
		String accId = (String) tSession.getValue("accountId");
		int iaccId = EJBUtil.stringToNum(accId);
		String studyId = (String) tSession.getValue("studyId");
		String grpId = (String) tSession.getValue("defUserGroup");
		String ipAdd = (String) tSession.getValue("ipAdd");
		String usr = (String) tSession.getValue("userId");
		int iusr = EJBUtil.stringToNum(usr);

		String src = StringUtil.htmlEncodeXss(request
				.getParameter("srcmenu"));
		String selectedTab = StringUtil.htmlEncodeXss(request
				.getParameter("selectedTab"));
		String eSign = request.getParameter("eSign");
		if (sessionmaint.isValidSession(tSession)) {
	%>
	<jsp:include page="sessionlogging.jsp" flush="true" />
	<%
		String oldESign = (String) tSession.getValue("eSign");
			if (!oldESign.equals(eSign)) {
	%>
	<jsp:include page="incorrectesign.jsp" flush="true" />
	<%
		} else {
			String resultMsg = null;
			// Save re-submission draft first
		    HashMap<String, Object> paramMap = new HashMap<String, Object>();
		    paramMap.put("request", request);
		    ResubmDraftDao resubmDraftDao = new ResubmDraftDao();
		    int ret = resubmDraftDao.saveResubmissionDraft(paramMap);

		    if (ret > 0){
		    	// Unfreeze the protocol next
		    	paramMap.put("userId", (usr.equals(null) ? null : Integer.valueOf(usr)));
				paramMap.put("accountId", (accId.equals(null) ? null : Integer.valueOf(accId)));
				paramMap.put("studyId", (studyId.equals(null) ? null : Integer.valueOf(studyId)));

				FlxPageArchive archive = new FlxPageArchive();
				float retFloat = archive.unfreezeProtocol(paramMap);
				ret = (retFloat > 0)? 1 : -1;
	    	}
			if (ret > 0) {
				resultMsg = MC.M_Data_SvdSucc/*"Data was saved successfully"*****/;
			} else {
				resultMsg = MC.M_Err_SavingData/*"There was an error while saving data"*****/;
				%>
				<br>
				<br>
				<br>
				<br>
				<p class="successfulmsg" align=center><%=resultMsg%></p>
				<META HTTP-EQUIV=Refresh CONTENT="3; URL=studyCheckNSubmit?mode=M&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>">
	 			<%
				return; // An error occurred; let's get out of here
			}

			String nextScreen = "flexStudyScreen";
			%>
			 <br><br><br><br>
			 <p class = "successfulmsg" align = center><%=resultMsg%></p>
			 <META HTTP-EQUIV=Refresh CONTENT="1; URL=<%=nextScreen%>?mode=M&selectedTab=irb_check_tab&studyId=<%=studyId%>">
			<%
		}// end of if body for e-sign
	}//end of if body for valid session
	else {
%>
<jsp:include page="timeout.html" flush="true" />
<%
	}
%>

</BODY>
</HTML>
