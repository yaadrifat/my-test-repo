<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page import="com.velos.eres.web.objectSettings.ObjectSettingsCache, com.velos.eres.web.objectSettings.ObjectSettings"%>
<%@ page language = "java" import="com.velos.eres.service.util.EJBUtil,java.util.*"%>


<%!
  private String getHrefBySubtype(String subtype) {
      if (subtype == null) { return "#"; }
	
	  if ("2".equals(subtype)) {
          return "ulinkBrowser.jsp?srcmenu=tdMenuBarItem13&selectedTab=2";
      }

	  if ("3".equals(subtype)) {
          return "mySubscriptions.jsp?srcmenu=tdMenuBarItem13&selectedTab=3";
      }

	  if ("4".equals(subtype)) {
          return "systemsettings.jsp?srcmenu=tdMenuBarItem13&selectedTab=4";
      }

	  if ("5".equals(subtype)) {
          return "changepwd.jsp?srcmenu=tdmenubaritem13&selectedTab=5";
      }
	 
      return "#";
  }
%>


<%
String mode="M";
String selclass;
String userId="";	
String acc="";	
String tab= request.getParameter("selectedTab");
HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession))
{
	acc = (String) tSession.getValue("accountId");
	userId = (String) tSession.getValue("userId");
}

ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
ArrayList tabList = objCache.getAccountObjects(EJBUtil.stringToNum(acc),"persacc_tab");
%>

<!--  <table  class= "selectedTab" cellspacing="0" cellpadding="0" border="0"> -->
<table  cellspacing="0" cellpadding="0" border="0">
	<tr>

		
		<%
			for (int iX=0; iX<tabList.size(); iX++) {
            ObjectSettings settings = (ObjectSettings)tabList.get(iX);

    		if ("0".equals(settings.getObjVisible())) {
    		    continue;
    		}

            boolean showThisTab = false;
    		if ("1".equals(settings.getObjSubType())) {
    		     showThisTab = true; 
    		} 
			else if ("2".equals(settings.getObjSubType())) {
					showThisTab = true; 
			
			}
			else if ("3".equals(settings.getObjSubType())) {
					showThisTab = true; 
			}

			else if ("4".equals(settings.getObjSubType())) {
					showThisTab = true; 
			}
			else if ("5".equals(settings.getObjSubType())) {
					showThisTab = true; 
			}
	
			else {
    		    showThisTab = true;
    		}

            if (!showThisTab) { continue; } 

		
			if (tab == null) { 
                selclass = "unselectedTab";
    		} else if (tab.equals(settings.getObjSubType())) {
                selclass = "selectedTab";
            } else {
                selclass = "unselectedTab";
            }
	 %>

		<td  valign="TOP">
			
			<table class="<%=selclass%>"  cellspacing="0" cellpadding="0" border="0" width="99%" >
				<tr>
			<!-- 	   	<td rowspan="3" valign="top" wclassth="7">
						<img src="../images/leftgreytab.gif" width=8 height=20 border=0 alt="">
					</td>  -->
				<td>

			 <%
			 if ("1".equals(settings.getObjSubType())) {
			  %>
			 <a href="userdetails.jsp?mode=<%=mode%>&srcmenu=tdmenubaritem13&selectedTab=1&pname=ulinkBrowser"><%=settings.getObjDispTxt()%>	</a>
			 <%} else {%>	
				<a href="<%=getHrefBySubtype(settings.getObjSubType())%>"><%=settings.getObjDispTxt()%>	</a>
			<%}%>
					</td> 
			       <!--   <td rowspan="3" valign="top" wclassth="7">
						<img src="../images/rightgreytab.gif" wclassth=7 height=20 border=0 alt="">
			        </td> -->
			   	</tr> 
		   	</table> 		   	
        </td> 

	 <%
        } 
      %>


	</tr>
<!--     <tr>
	     <td height="10"></td>
	</tr>  --> 
</table>
<!-- <table style="background:#afafaf; margin-left:3px;" width="99%" height="5"><tr><td></td></tr></table> -->
<table  class="tabBorder" width="99%" height="5"><tr><td></td></tr></table>
