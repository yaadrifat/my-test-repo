<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=MC.M_EvtLib_EvtCat%><%--Event Library>> Event Category*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<%--Link Rel=STYLESHEET HREF="common.css" type=text/css--%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>

<SCRIPT Language="javascript">

 function  validate(formobj){

	 if (!(validate_col('Event Library Type',formobj.cmbLibType))) return false; 
	 if (!(validate_col('Category Name',formobj.categoryName))) return false
     if (!(validate_col('e-Signature',formobj.eSign))) return false


	/*if(isNaN(formobj.eSign.value) == true) {
	alert*/<%--("<%=MC.M_IncorrEsign_EtrAgain%>");--%>
	/*alert("Incorrect e-Signature. Please enter again");
	formobj.eSign.focus();
	return false;
   	}*/

   }

</SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,com.velos.esch.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB"%>


<% String src;

src= request.getParameter("srcmenu");

%>


<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>   


<body>
<DIV class="formDefault" id="div1"> 

  <%

	String categoryId  ="";
	String categoryName ="";

	String desc ="";

	String mode="";
	String catMode="";

	String fromPage = request.getParameter("fromPage");
	String duration = request.getParameter("duration");
	String protocolId = request.getParameter("protocolId");
	String calledFrom = request.getParameter("calledFrom");

	HttpSession tSession = request.getSession(true); 

	if (sessionmaint.isValidSession(tSession))
	{
		String uName = (String) tSession.getValue("userName");

		mode = request.getParameter("mode");
		catMode = request.getParameter("catMode");
		int pageRight = 7;
		String libTypePullDn = "";
		String bgtCatPullDn = "";
		String evtLibType ="";
		String evtLineCat ="";

		SchCodeDao schDao = new SchCodeDao();
		schDao.getCodeValues("lib_type");
		libTypePullDn = schDao.toPullDown("cmbLibType");
		SchCodeDao schCatDao = new SchCodeDao();
		schCatDao.getCodeValues("category");
		bgtCatPullDn = schCatDao.toPullDown("cmbBgtCat");

	//	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
//        	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CGRP"));

		if (catMode.equals("M")) {
			categoryId = request.getParameter("categoryId");
			eventdefB.setEvent_id(EJBUtil.stringToNum(categoryId));
        
			eventdefB.getEventdefDetails();
			categoryName = eventdefB.getName(); 

			desc = eventdefB.getDescription();
			desc = (desc==null)?"":desc;
			evtLibType = eventdefB.getEventLibType();
			evtLineCat = eventdefB.getEventLineCategory();

   		}
%>


  <%

	if ((catMode.equals("M") && pageRight >=6) || (catMode.equals("N") && (pageRight == 5 || pageRight == 7 )) )


	{

%>
  
  <!--P class="sectionHeadings"> Event Library >> Event Category </P-->

  <P class="defComments"> <%=MC.M_Etr_EvtCatDet%><%--Please enter Event Category details*****--%></P>

  <Form name="category" id="categoryfrm" method="post" action="updatecategory.jsp?srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>" onsubmit="if (validate(document.category)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
    <input type="hidden" name="srcmenu" value="<%=src%>">
    <input type="hidden" name="mode" size = 20  value ="<%=mode%>" >
    <input type="hidden" name="catMode" size = 20  value ="<%=catMode%>" >
    <input type="hidden" name="categoryId" size = 20  value ="<%=categoryId%>" >
    <input type="hidden" name="fromPage" size = 20  value ="<%=fromPage%>" >

    <table width="100%" cellspacing="0" cellpadding="0" border="0" class="basetbl">

	 <tr> 
        <td width="30%"><%=LC.L_Evt_Lib%><%--Event Library Type*****--%> <FONT class="Mandatory">* </FONT></td>
        <td> 
		 <% if(mode.equals("N")){ %>
		 <%=libTypePullDn%>
		 <%} else {
		 libTypePullDn = schDao.toPullDown("cmbLibType",EJBUtil.stringToNum(evtLibType)); %>
		 <%=libTypePullDn%>
		 <%}%>
        </td>
      </tr>

	<tr> 
        <td width="30%"><%=LC.L_Evt_CatName%><%--Event Category Name*****--%> <FONT class="Mandatory">* </FONT> </td>
        <td width="60%"> 
          <input type="text" name="categoryName" size = 20 MAXLENGTH = 50 	value="<%=categoryName%>">
        </td>
      </tr>

	  <tr> 
        <td width="30%"><%=LC.L_Evt_CatDesc%><%--Event Category Description*****--%></td>
        <td>
			<input type="text" name="desc" size = 40 MAXLENGTH = 50 value="<%=desc%>">
        </td>
      </tr>

      <tr> 
        <td width="30%"><%=LC.L_Bgt_Cat%><%--Budget Category*****--%> </td>
        <td> 
		<% if(mode.equals("N")){ %>
		<%=bgtCatPullDn%>
		<%} else {
		bgtCatPullDn = schCatDao.toPullDown("cmbBgtCat",EJBUtil.stringToNum(evtLineCat)); %>
		<%=bgtCatPullDn%>
		<%}%>
        </td>
      </tr>

  </table>

<table width="100%" cellspacing="0" cellpadding="0" border="0" class="searchbg"><tr>
<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="categoryfrm"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>
<td></td></tr></table>
    <br>

  <table width="100%" cellspacing="0" cellpadding="0">
      <td align=right> 
	<!--	<input type="image" src="../images/jpg/Submit.gif" align="absmiddle" border="0" >-->
      </td> 
      </tr>
  </table>
  </Form>
  <%

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div><div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/></div>
</body>

</html>


