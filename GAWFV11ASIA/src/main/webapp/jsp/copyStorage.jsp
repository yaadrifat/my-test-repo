<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
 
<head>
<title> <%=LC.L_Copy_Storages%><%--Copy Storages*****--%></title>

<%@ page import ="java.util.*, java.io.*, org.w3c.dom.*,com.velos.eres.business.common.*" %>
<meta http-equiv="Content-type" content="text/html; charset=UTF-8">
</head>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="whichcss.js"></SCRIPT>

<script Language="javascript"> 
function validateme(formObj){	

	var len = formObj.len.value;
	if (len ==1) {
		name =formObj.strgName.value;
		if (name == ''){
	      alert("<%=MC.M_Etr_StorName%>");/*alert("Please enter Storage Name");*****/
	      formObj.strgName.focus();	
	      return false;
       }
	}
    else
	{
	  for (var i=0;i<len;i++) {
	     name =formObj.strgName[i].value;
	     if (name=='') {
	    	alert("<%=MC.M_Etr_StorName%>");/*alert("Please enter Storage Name");*****/
		   formObj.strgName[i].focus();	
		   return false;
	   }
	  }
	}


	if(!(validate_col('e-Signature',formObj.eSign))) return false
	<%-- if (isNaN(formObj.eSign.value) == true){
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formObj.eSign.focus();
		return false;		
	} --%>

}
</script>
<%@ page import="com.velos.eres.service.util.*" %>
<jsp:useBean id="sess" scope="request" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="storageB" scope="request" class="com.velos.eres.web.storage.StorageJB"/>
<jsp:include page="include.jsp" flush="true"/>
<BODY>
<%
HttpSession tSession = request.getSession(true);
if (sess.isValidSession(tSession)){
 String selStrs = request.getParameter("selStrs");
 String[] selStrpk = selStrs.split(",");
 String strgPk ="";
 String strgName ="";
 int len = selStrpk.length;
	
%>

<p class = "sectionHeadings" ><%=MC.M_CopyStor_UntOrTpl%><%--Copy Storage Unit/Template*****--%></p>

<form name="storage" method=post id="cpyStorage" action="updateCopyStorage.jsp" onsubmit="if (validateme(document.storage)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

<input type="hidden" name="len" value =<%=len%> >
<DIV class="popDefault" id="div1">
<table width="100%" >
	<tr>
	<th> <%=LC.L_Storage_UnitName%><%--Storage Unit Name*****--%> </th>
	<th> # <%=LC.L_Of_Copies%><%--of Copies*****--%> </th>	
	<!--<th> Storage ID	</FONT> </th>-->
	<th> <%=LC.L_Storage_Name%><%--Storage Name*****--%> <FONT class="Mandatory">* </FONT> </th>			
	<th> <%=LC.L_Template_Q%><%--Template?*****--%> </th>
	</tr>

<br>
<%

	
	for (int i=0; i <  len; i++){ 
	strgPk = selStrpk[i];
	storageB.setPkStorage(EJBUtil.stringToNum(strgPk));
	storageB.getStorageDetails();
	strgName = storageB.getStorageName();
		%>

	<tr width = "100%">
	
	<td width="15%">
	<input type="hidden" name="strgPk" value=<%=strgPk%> >
	<%=strgName%>
	</td>

	<td width="15%">
	<input type="text" name="copyCount" maxlength=250 size=20>
	</td>

	 
	<input type="hidden" name="strgId" maxlength=250 size=20>	
	 

	<td width="15%">
	<input type="text" name="strgName" maxlength=250 size=20>
	</td>
	<td width="15%" align=center><input type="checkbox" name="template<%=i%>"></td>	
	
    
<%} %>
</table>

<br>

<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="cpyStorage"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>

</div>

</form>

<%

}
//end of session
else{
%>

<jsp:include page="timeout.html" flush='true'></jsp:include>
<%}%>

	<div class = "myHomebottomPanel"> 
	<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</BODY>

</html> 