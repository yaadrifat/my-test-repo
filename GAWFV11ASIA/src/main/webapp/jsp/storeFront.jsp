<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<%@page import="com.velos.eres.service.util.*,java.util.*,com.velos.esch.business.common.SchCodeDao"%>
<html>
<head>
<title>Storefront</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="addressUserB" scope="page" class="com.velos.eres.web.address.AddressJB" />
<script LANGUAGE="JavaScript" src="js/jquery/jquery-1.4.4.js"></script>
</head>
<body>
<%
   HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession)){
String userAgent = request.getHeader("User-Agent");
String usr = null;
usr = (String) tSession.getValue("userId");
userB.setUserId(StringUtil.stringToNum(usr));
userB.getUserDetails();
String uFName = userB.getUserFirstName();
String uLName = userB.getUserLastName();
if (userB.getUserPerAddressId() != null) {
	addressUserB.setAddId(EJBUtil.stringToNum(userB.getUserPerAddressId()));
	addressUserB.getAddressDetails();
}
String  userAddPri="";
String  userAddPhone="";
String  userEmailAdd="";
userAddPri= addressUserB.getAddPri();
userAddPri = (   userAddPri  == null      )?"":(  userAddPri ) ;
userAddPhone= addressUserB.getAddPhone();
userAddPhone = (   userAddPhone  == null      )?"":(  userAddPhone ) ;
userEmailAdd= addressUserB.getAddEmail();
userEmailAdd = (   userEmailAdd  == null      )?"":(  userEmailAdd ) ;
%>
<script type="text/javascript">
refreshPageAlert =1;
function showWarningChange(){
	userTimeoutInMSec = userTimeoutInMSec;
	warningPeriodInMSec = 60 * 1000;
	sessTimeoutWarning = '<%=MC.M_Usr_SessTimeout%>';
	if (userTimeoutInMSec - warningPeriodInMSec < 10000) {
		warningPeriodInMSec = 30000;
		sessTimeoutWarning = '<%=MC.M_Usr_SessTimeoutShort%>';
	}

	resetSessionWarningOnTimer();
}
userInfo = {};
jwttoken = "<%=tSession.getAttribute("jwttoken")%>";
userInfo = {userFirstName:"<%=uFName%>",
		    userLastName:"<%=uLName%>",
		    userPhoneNo:"<%=userAddPhone%>",
		    userAddPri:"<%=userAddPri%>",
			userEmailAdd:"<%=userEmailAdd%>",
		    };
userAgent = "<%=userAgent%>";
URL_HOST="";
mainDivHeight="";
subDivHeight="";
var screenWidth = screen.width;
var screenHeight = screen.height;
if(userAgent.indexOf("Firefox")>-1){
	URL_HOST = "velos/jsp/dist/";
} else if(userAgent.indexOf("Chrome")>-1){
	  URL_HOST = "./velos/jsp/dist/";
} 
else{
	  URL_HOST = "../dist/";
}
if(screenWidth>1280 || screenHeight>1024){
		mainDivHeight="530px;";
		subDivHeight="340px;";
	}else{
		mainDivHeight="675px;";
		subDivHeight="475px;";
	}
function onPageChange(URL){
var origin   = window.location.origin;
window.location.href = window.location.origin+URL;
}
$.noConflict();
$j = jQuery;
</script>
<%if(userAgent.indexOf("Firefox")>-1){ %>
<div style="Float:right;">
                        <%= StringUtil.htmlEncodeXss((String) request.getSession(true).getValue("userName")) %>
						&nbsp;|&nbsp;
                        <a id="homePage" href="../velos/jsp/myHome.jsp">
									<img style="border:none;" 
									src="./velos/images/home.png" title="My Homepage"/></a>
						&nbsp;|&nbsp;
						<a id="logOut" href="../velos/jsp/logout.jsp">
						<img src="./velos/images/SignOff.png" title="Logout"/></a>
						</div>
<%} else if(userAgent.indexOf("Chrome")>-1){%>
<div style="Float:right;">
                        <%= StringUtil.htmlEncodeXss((String) request.getSession(true).getValue("userName")) %>
						&nbsp;|&nbsp;
                        <a id="homePage" href="../velos/jsp/myHome.jsp">
									<img style="border:none;" 
									src="../images/home.png" title="My Homepage"/></a>
						&nbsp;|&nbsp;
						<a id="logOut" href="../velos/jsp/logout.jsp">
						<img src="../images/SignOff.png" title="Logout"/></a>
						</div>
<%} else{%>
<div style="Float:right;">
                        <%= StringUtil.htmlEncodeXss((String) request.getSession(true).getValue("userName")) %>
						&nbsp;|&nbsp;
                        <a id="homePage" href="#" onclick="onPageChange('/velos/jsp/myHome.jsp')">
									<img style="border:none;" 
									src="../images/home.png" title="My Homepage"/></a>
						&nbsp;|&nbsp;
						<a id="logOut" href="#" onclick="onPageChange('/velos/jsp/logout.jsp')">
						<img src="../images/SignOff.png" title="Logout"/></a>
						</div>
<%} %>

						<br/>
<jsp:include page="./dist/index.html" flush="true" /> 
<%
}//end of if body for session
	else{%> 
	 <jsp:include page="timeout.html" flush="true" /> 
<%}%>
</body>
</html>