<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="jqueryUtils.jsp"></jsp:include>
<jsp:include page="velos_includes.jsp"></jsp:include>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Comment_Type%><%--Comment Type*****--%> </title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<script type="text/javascript" src="./htmlarea/htmlarea.js"></script>
<SCRIPT LANGUAGE="JavaScript" SRC="whichcss.js"></SCRIPT>

 <script type="text/javascript" src="../js/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
	checkQuote="N";
	var fck;
	var oFCKeditor;
     function init()
      {
       
     /* oFCKeditor = new FCKeditor("fldinst") ;
      oFCKeditor.BasePath = './FCKeditor/';
      oFCKeditor.Width = editorWidth ;
      oFCKeditor.Height = editorHeight ; // 400 pixels
      oFCKeditor.ToolbarSet="Velos";
      //oFCKeditor.Config['SkinPath'] = '/FCKeditor/editor/skins/office2003/' ;
      oFCKeditor.ReplaceTextarea() ;*/
      if (document.commentType.section.type!="hidden")
      document.commentType.section.focus();
      else 
      document.commentType.sequence.focus();
      }
       function processFormat(formobj,mode)
      {
       
      	if (mode=="D")
	{
	 if (formobj.fldinst.value.length>0)
	 formobj.fldinst.value="[VELDISABLE]"+formobj.fldinst.value;
	 //showHide("eformat","S");
	 //showHide("dformat","H");
	 document.poplayer=document.getElementById("dformat");
	 document.poplayer.innerHTML="<A href='javascript:void(0)' onClick='processFormat(document.commentType,\"E\")'><img src=\"./images/enableformat.gif\" alt=\"<%=LC.L_Enable_Formatting%><%--Enable Formatting*****--%>\" border = 0 align=absbotton></A>";
	 
	} else if (mode=="R")
	{
	   if (confirm("<%=MC.M_RemFmt_CommentsText%>"))/*if (confirm("Remove formatting for Comments Text?"))*****/
	   {
	    formobj.fldinst.value="";
	    
	   }
	} else if(mode=="E")
	{
	 formobj.fldinst.value=replaceSubstring(formobj.fldinst.value,"[VELDISABLE]","");
	 //showHide("eformat","H");
	 //showHide("dformat","S");
	 document.poplayer=document.getElementById("dformat");
	 document.poplayer.innerHTML="<A href='javascript:void(0)' onClick='processFormat(document.commentType,\"D\")'><img src=\"./images/disableformat.gif\" alt=\"<%=LC.L_Disable_Formatting%><%--Disable Formatting*****--%>\" border = 0 align=absbotton></A>";
	}
	
      }
    </script>
<script type="text/javascript">
     overRideChar("#");
   overRideChar("&");
   overRideFld("fldinst");
   var editor;
  function FCKeditor_OnComplete(oFCKeditor)
{
	 fck= FCKeditorAPI.GetInstance('fldinst');
	 
}
window.onload= function()
{
init();
}
</script>
<SCRIPT Language="javascript">

 function  validate(formobj)
 {
 
 	if (   !(validate_col ('  Section', formobj.section)  )  )  return false
	if (!(validate_col('Sequence',formobj.sequence))) return false		
	
    
	
		
 	//if (   !(validate_col ('  Instructions', formobj.fldinst)  )  )  return false
	if (formobj.instructions.value.length==0)
	{
	 alert("<%=MC.M_EtrData_InComment%>");/*alert("Please enter data in comment text.");*****/
	  formobj.instructions.focus();
	  return false;
	}
	
 		
 	/*if (checkChar(formobj.instructions.value,"'"))
	 {
	   alert("Special Character(') not allowed for this Field");
	   formobj.instructions.focus();
	    return false;
	 }*/
	
	 //if (!(validateDataSize ( formobj.instructions,2000,'Text'))) return false
	<%--
	 if ((formobj.instructions.value.length)>2000)
	{
	  alert("<%=MC.M_TextLengthExcs_MaxAlw2000%>");/*alert("'Comment Text' length exceeds the maximum allowed limit of 2000 characters.\n Please correct to continue.");*****/
	  formobj.instructions.focus();
	  return false;
	}
	-->
	/*if (formobj.fldinst.value.length>0)
	formobj.fldinst.value=htmlEncode(formobj.fldinst.value);*/
	<%--
     if (  !(validate_col('e-Signature',formobj.eSign)     )   ) return false
	 
	  

	if(isNaN(formobj.eSign.value) == true) 
	{
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		 return false;
   	}  --%>
	
	if(isNaN(formobj.sequence.value) == true) 
	{
		alert("<%=MC.M_SecSeq_EtrValidNum%>");/*alert("Section Sequence has to be a number. Please enter a valid number");*****/
		formobj.sequence.focus();
		 return false;
   	}
   	if(isNaN(formobj.labelDisplayWidth.value) == true) 
      {
        alert("<%=MC.M_LabelDispWidth_HasToNum%>");/*alert("Label Display Width has to be a number. Please enter a valid number.");*****/
        formobj.labelDisplayWidth.focus();
        return false;
       }    

	
	return true;
  }
  
 /***** @Gyanendra update for Bug#20544 *****/

var textEventLimitFunction = {		
		fixTextAreas:{}
	};

     textEventLimitFunction.fixTextAreas = function (frm) {
	
	limitChars('instructions', 4000, 'countinstructions');
	
	$j("#instructions").keyup(function(){
		var divId = 'count'+this.id;
		limitChars(this.id, 4000, divId);
	});	
};

$j(document).ready(function(){	
	
	textEventLimitFunction.fixTextAreas();
		
});

/***** End update @Gyanendra *****/
 


</SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="fieldLibJB" scope="request"  class="com.velos.eres.web.fieldLib.FieldLibJB"/>
<jsp:useBean id="formSecJB" scope="request"  class="com.velos.eres.web.formSec.FormSecJB"/>
<jsp:useBean id="formFieldJB" scope="request"  class="com.velos.eres.web.formField.FormFieldJB"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB"%>

<body>
<DIV class="popDefault" id="div1"> 
 <%

	HttpSession tSession = request.getSession(true);
	int pageRight = 0;	

   if (sessionmaint.isValidSession(tSession))
   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
	<jsp:include page="include.jsp" flush="true"/>
<%
	 GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
	 String calledFrom=request.getParameter("calledFrom");
	 String lnkFrom=request.getParameter("lnkFrom");
	 if(lnkFrom==null) 
		{lnkFrom="-";}
	 String codeStatus = request.getParameter("codeStatus");
	 String stdId=request.getParameter("studyId");
	 int studyId= EJBUtil.stringToNum(stdId);			

		String userIdFromSession = (String) tSession.getValue("userId");
		
		if( calledFrom.equals("A")){ //from account form management
	 	    if (lnkFrom.equals("S")) 
	 	      {   
		 		 ArrayList tId ;
				 tId = new ArrayList();
				 if(studyId >0)
				 {
				 	TeamDao teamDao = new TeamDao();
					teamDao.getTeamRights(studyId,EJBUtil.stringToNum(userIdFromSession));
					tId = teamDao.getTeamIds();
					if (tId.size() <=0)
					 {
						pageRight  = 0;
					  } else 
					  {
					  	StudyRightsJB stdRights = new StudyRightsJB();
						stdRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));
						 
							ArrayList teamRights ;
							teamRights  = new ArrayList();
							teamRights = teamDao.getTeamRights();
								 
							stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
							stdRights.loadStudyRights();
							 
						 
						 		
						if ((stdRights.getFtrRights().size()) == 0)
						{					
						  pageRight= 0;		
						} else 
						{								
							pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMMANAG"));
						}
					  }
			    }
			 }	
    	 	else
    	    {		 
    	  		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MACCFRMS"));
    		 }
	  	} 
		
	 	if( calledFrom.equals("L")) //form library
	 	{
			pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MFRMLIB"));
	 	}
		
		if( calledFrom.equals("St")) //study
	 	{
			StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");	
	  		pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMMANAG"));
		}
		 

	if (pageRight >= 4)
		
	{
	
		String mode=request.getParameter("mode");
		
		String fieldLibId="";
		
		String fldInstructions="";
		String fldInstFormat="";
		String formatDisable="N";
		
		// THE STYLE OBJECT WHICH TAKES VALUES OF ATTRIBUTES OF THE FIELD  
		// LIKE BOLD, SAMELINE, ALIGN , ITALICS
		Style aStyle= new Style();
		String formId="";
		String formSecId="";
		String fldSeq="";
		String fldAlign="";
		String samLinePrevFld="";
		String fldBold="";
		String fldItalics="";
		String formFldId="";
		String fldType="";
		String fldIsVisible="";
		String offlineFlag = "0";
		String labelDisplayWidth = "";	
		
		formId=request.getParameter("formId");		
		String accountId=(String)tSession.getAttribute("accountId");
		
		int tempAccountId=EJBUtil.stringToNum(accountId);
		

		
		ArrayList idSec= new ArrayList();
		ArrayList descSec= new ArrayList();
				
		String pullDownSection;
		FormSecDao formSecDao= new FormSecDao();
		formSecDao = formSecJB.getAllSectionsOfAForm( EJBUtil.stringToNum(formId));
		idSec= formSecDao.getFormSecId();
		descSec=formSecDao.getFormSecName();
		
		pullDownSection=EJBUtil.createPullDown("section", 0, idSec, descSec);
						
		if (mode.equals("M"))
		{
			
				formId=request.getParameter("formId");		
				formFldId=request.getParameter("formFldId");
				
				formFieldJB.setFormFieldId(Integer.parseInt(formFldId) );
				formFieldJB.getFormFieldDetails();
				fieldLibId=formFieldJB.getFieldId();
				offlineFlag = formFieldJB.getOfflineFlag();
				
				
				fieldLibJB.setFieldLibId(Integer.parseInt(fieldLibId));
				fieldLibJB.getFieldLibDetails();
			
				formSecId=formFieldJB.getFormSecId();  
				fldIsVisible = fieldLibJB.getFldIsVisible();
				
				
				//TO GIVE A SINGLE SECTION IN EDIT MODE
				ArrayList idSecNew = new ArrayList();
				ArrayList descSecNew = new ArrayList();
				int secId =EJBUtil.stringToNum(formSecId) ;
				 for(int j = 0 ; j < idSec.size() ; j++)
				 {
				 	int  secIdNew =(   (Integer) idSec.get(j)  ).intValue();
					if ( secIdNew  ==   secId )
					{
				 		idSecNew.add(   (  Integer )idSec.get(j) );
						descSecNew.add( (String )descSec.get(j) ) ;
						break ;	
					}
				 }
				
				// TO MAKE A READONLY BOX FOR NOT LETTING THE EDITING OF THE SECTION FOR FIELD IN EDIT MODE 
				String sectionN = (String)descSecNew.get(0) ;
				pullDownSection = "<label>"+sectionN+"</label>" 
				 							+ "<Input type=\"hidden\" name=\"section\" value="+(Integer)idSecNew.get(0)+" >  " ;
								  
			//	pullDownSection=EJBUtil.createPullDown("section", secId,idSecNew, descSecNew   );

				fldSeq=((formFieldJB.getFormFldSeq()) == null)?"":(formFieldJB.getFormFldSeq()) ;
				
						
				//  WHILE GETTING THE STYLE WE CHECK ITS VALUES TO DISPLAY APPROPRIATELY
				aStyle=fieldLibJB.getAStyle( ); 
				if ( aStyle != null )  
				{
					
					fldAlign = (  (   aStyle.getAlign()   ) == null)?"-":(   aStyle.getAlign()   ) ;	
					fldBold = (  (   aStyle.getBold()   ) == null)?"-1":(   aStyle.getBold()   ) ;	
					fldItalics = (  (   aStyle.getItalics()   ) == null)?"-1":(   aStyle.getItalics()   ) ;	
					samLinePrevFld = (  (   aStyle.getSameLine()   ) == null)?"-1":(   aStyle.getSameLine()   ) ;	
					}
			
				
			
				
				fldType=fieldLibJB.getFldType();
				
				fldInstructions = ((fieldLibJB.getFldInstructions()) == null)?"":(fieldLibJB.getFldInstructions()) ;
				
				fldInstFormat=StringUtil.unEscapeSpecialCharHTML(fieldLibJB.getFldNameFormat()) ;
				fldInstFormat=(fldInstFormat == null)?"":StringUtil.encodeString(fldInstFormat.replace("&amp;#39;","\'")) ;
				
				if (fldInstFormat.length()>0)
				{
				 if (fldInstFormat.indexOf("[VELDISABLE]")>=0)
				 formatDisable="Y";
				}
				
				
				
				labelDisplayWidth = ((fieldLibJB.getFldDisplayWidth())== null)?"":(fieldLibJB.getFldDisplayWidth()) ; 
				
				
				
				
							
				
				
		}
		
		// #5437,5438 02/02/2011 @Ankit
		if(((pageRight == 6 && mode.equals("M") ) || (pageRight == 5 && mode.equals("N")) || pageRight == 7) && (mode.equals("M") && (!codeStatus.equals("Lockdown") && !codeStatus.equals("Deactivated") && !codeStatus.equals("Freeze") && !codeStatus.equals("Active")) )|| mode.equals("N")){%>
		<Form name="commentType" id="commentTypeFrm" method="post" action="commentTypeSubmit.jsp" onsubmit="if (validate(document.commentType)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
			<%}else{%>
		<Form name="commentType" method="post" onsubmit="return false">
			<%}%>

	<P class="sectionHeadings"> <%=LC.L_Fld_Comments%><%--Field Type: Comments*****--%> </P> </td>
 
	
	<Input type="hidden" name="mode" value=<%=mode%> >
	<Input type="hidden" name="codeStatus" value=<%=codeStatus%> >
	<Input type="hidden" name="formId" value=<%=formId%> >  
	<Input type="hidden" name="formFldId" value=<%=formFldId%> >  
	<table width="80%" cellspacing="0" cellpadding="0" border="0">
		<tr> 
      <td><%=LC.L_Section%><%--Section*****--%><FONT class="Mandatory">* </FONT></td>
        <td width="60%"><%=pullDownSection%> </td>
	  </tr>
	<tr class="browserEvenRow"> 
      <td width="20%"><%=LC.L_Sequence%><%--Sequence*****--%><FONT class="Mandatory">* </FONT></td>
        <td width="60%"><input type="text" name="sequence" size = 10 MAXLENGTH = 10 value="<%=fldSeq%>"> </td>
		</tr>
		<tr><td><%=LC.L_Fld_Chars%><%--Field Characteristics*****--%></td><td>
		<input type="radio" name="isvisible" value="0" <%if(fldIsVisible.equals("0")||fldIsVisible.equals("")){%> checked <%}%>><%=LC.L_Visible%><%--Visible*****--%>
		
		<input type="radio" name="isvisible" align="center" value="1" <%if(fldIsVisible.equals("1")){%> checked <%}%>><%=LC.L_Hide%><%--Hide*****--%>
				
		</td></tr>
		
                      <!-- ************ @Gyanendra Update for Bug #4421 ************/ --> 
                      
		 <tr class="browserEvenRow"><td><%=LC.L_Text%><%--Text*****--%><FONT class="Mandatory">* </FONT></td>
		<td> <textarea id="instructions" name="instructions" rows="" 
		cols="200" style="width:400;height:100;" MAXLENGTH=4000><%=fldInstructions%></textarea>
		<font class="Mandatory"><div id="countinstructions"><%=MC.M_Limit4000_CharLeft %></div>
		
		            <!-- ************ Update for Bug #4421 end************/ -->
		            
		 <input name="fldinst" type="hidden" value="<%=fldInstFormat%>"> </td>
	<td width="45" bgcolor="#FFFFFF"><A href="javascript:void(0);" onClick="openPopupEditor('commentType','instructions','fldinst','Velos',2000)">
	<%=LC.L_Format%></A>
	<% if (fldInstFormat.length()>0){%>
	<%if (formatDisable.equals("N")){%>
	<DIV id="dformat"><A href="javascript:void(0);" onClick="processFormat(document.commentType,'D')"><img src="./images/disableformat.gif" alt="<%=LC.L_Disable_Formatting %><%-- Disable Formatting*****--%>" border = 0 align=absbotton></A></DIV>
	<%} else if (formatDisable.equals("Y")){%>
	<DIV id="dformat"><A href="javascript:void(0);" onClick="processFormat(document.commentType,'E')"><img src="./images/enableformat.gif" alt="<%=LC.L_Enable_Formatting %><%-- Enable Formatting*****--%>" border = 0 ></A></DIV>
	<%}%>
	<A href="javascript:void(0);" onClick="processFormat(document.commentType,'R')"><img src="./images/removeformat.gif" alt="<%=LC.L_Rem_Format %><%-- Remove Formatting*****--%>" border = 0 ></A>
      <%}%>
	</td>

	</tr>
	 <tr>
		  <td><%=LC.L_Label_DispWidth%><%--Label Display Width*****--%></td>
		  <td><input type="text" name="labelDisplayWidth" size = 3 MAXLENGTH = 3 value="<%=labelDisplayWidth%>">&nbsp;%

	<%if (  ( fldBold.equals("-1") )  || ( fldBold.equals("0" ) ) ||  (fldBold.equals("") )    )
		   {%>
			<input type="checkbox" name="bold" value="1"><%=LC.L_Bold%><%--Bold*****--%>
		 <%}%>
		 <% if (  fldBold.equals("1")  ) 
		   {%>
			<input type="checkbox" name="bold" value="1" CHECKED><%=LC.L_Bold%><%--Bold*****--%>
		 <%}%>
		<%if (  ( fldItalics.equals("-1") ) || (fldItalics.equals("0" )  ) ||  ( fldItalics.equals("") )     )
		   {%>
			<input type="checkbox" name="italics" value="1"><%=LC.L_Italics%><%--Italics*****--%>
		 <%}%>
		 <% if (   fldItalics.equals("1")  ) 
		   {%>
			<input type="checkbox" name="italics" value="1" CHECKED><%=LC.L_Italics%><%--Italics*****--%>
		 <%}%>

		</td>
	</tr>
		<%//Commented as per UCSF requirements*/%>
	<%//Now uncommented as per 19th April enhancements of %>
	<tr class="browserEvenRow"><td width="20%" > <%=LC.L_Align %><%-- Align*****--%> </td><td>
	<% if  (  (fldAlign.equals("left")  )  || (  fldAlign.equals("") )   )
		{ %>
		 <input type="radio" name="align" value="left"  onclick=  " " CHECKED> <%=LC.L_Left%><%--Left*****--%>
		<input type="radio" name="align" value="right"  onclick=  " "> <%=LC.L_Right%><%--Right*****--%> 
		 <input type="radio" name="align" value="center"  onclick=  " "> <%=LC.L_Center%><%--Center*****--%> 
		<%}   if (   fldAlign.equals("right")   ) 
		{%>
		<input type="radio" name="align" value="left"  onclick=  " " > <%=LC.L_Left%><%--Left*****--%>
		<input type="radio" name="align" value="right"  onclick=  " " CHECKED> <%=LC.L_Right%><%--Right*****--%>
		  <input type="radio" name="align" value="center"  onclick=  " "> <%=LC.L_Center%><%--Center*****--%>
		<%} if (   fldAlign.equals("center")   ) 
		{%>
		<input type="radio" name="align" value="left"  onclick=  " " > <%=LC.L_Left%><%--Left*****--%>
		<input type="radio" name="align" value="right"  onclick=  " " > <%=LC.L_Right%><%--Right*****--%>
		  <input type="radio" name="align" value="center"  onclick=  " " CHECKED> <%=LC.L_Center%><%--Center*****--%>
		<%}  if (  fldAlign.equals("-")  )
		 {%>
		<input type="radio" name="align" value="left"  onclick=  " " > <%=LC.L_Left%><%--Left*****--%>
		<input type="radio" name="align" value="right"  onclick=  " "> <%=LC.L_Right%><%--Right*****--%> 
		  <input type="radio" name="align" value="center"  onclick=  " "> <%=LC.L_Center%><%--Center*****--%>
		<%}%> 


		<%if (  ( samLinePrevFld.equals("-1")  ) || ( samLinePrevFld.equals("0" ) ) ||  (samLinePrevFld.equals("") ) )
		   {%>
			<input type="checkbox" name="sameLine" value="1" ><%=MC.M_SameLine_AsPrevFld%><%--Same line as previous field*****--%>
		 <%}%>
		 <% if (   samLinePrevFld.equals("1")  ) 
		   {%>
			<input type="checkbox" name="sameLine" value="1" CHECKED><%=MC.M_SameLine_AsPrevFld%><%--Same line as previous field*****--%>
		 <%}%>
		</td></tr>  


	<%
	// #5437,5438 02/02/2011 @Ankit
		if(((pageRight == 6 && mode.equals("M") ) || (pageRight == 5 && mode.equals("N")) || pageRight == 7) && (mode.equals("M") && (!codeStatus.equals("Lockdown") && !codeStatus.equals("Deactivated") && !codeStatus.equals("Freeze") && !codeStatus.equals("Active")) )|| mode.equals("N")){
		if(codeStatus.equals("WIP")){
						%>
						<table align="center"><tr>
	  <td align="center"><br />
				<button type="submit" id="submit_btn" ondblclick="return false"><%=LC.L_Submit%></button>
			</td> 
	  </tr></table>
     
<%}else{ 
%>
<br />
	<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="editBoxFieldFrm"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include> 

<br>

 	 <table width="100%" cellspacing="0" cellpadding="0">
      <td align=right> 

      </td> 
      </tr>
  </table>	<% }
	}%>
     <br>

      <td align=right> 

      </td> 
      </tr>
  </table>
  </Form>
  
<%

	} //end of if body for page right
	else
	{
%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
<%

	} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
<%

}

%>
<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>

</body>

</html>


		

