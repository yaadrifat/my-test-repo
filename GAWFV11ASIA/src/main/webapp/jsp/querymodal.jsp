<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%!
private String extractFieldId(String fldSysId) {
    if (fldSysId == null) { return null; }
    String[] part = fldSysId.split("_");
    if (part == null || part.length < 1) { return null; }
    return part[part.length-1];
}
%>
<html>
<head>
<title><%=LC.L_Query_Form%><%--Query Form*****--%></title>

<%@ page import="com.velos.eres.service.util.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*, oracle.xml.parser.v2.*,org.xml.sax.*,org.w3c.dom.*, oracle.xdb.*,com.velos.eres.business.saveForm.* " %>
<jsp:include page="include.jsp" flush="true"/>  
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<Link Rel=STYLESHEET HREF='./styles/yuilookandfeel/yuilookandfeel.css' type=text/css>
<!-- <SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>  -->
<jsp:include page="skinChoser.jsp" flush="true"/>
<script Language="javascript">


//KV Bug No:4606
if (window.event != 'undefined')
	document.onkeydown = function()
	{
		return (event.keyCode); 
	}
else
	document.onkeypress = function(e)
	{
		return (e.keyCode != 8);
	}


function killTime(formobj)
{
 lform="er_fillform1";
}
function submitParent(formObj)
{
  var runValidation = true;
  var fldmodestr = "";
  count=formObj.totCount.value;
  lform="er_fillform1"; 
  var parentForm = window.opener.document.forms[lform];

  if(count > 1)
  {
  for (j=0;j<count;j++)
  {
    
     fieldSysId=formObj.fieldSysId[j].value;	
     fldmodei = formObj.fldmodei[j].value;	
	 if(j == 0)
	 fldmodestr = fldmodei;
	 else
	 fldmodestr = fldmodestr + '*' + fldmodei;
		 
  	}//for
  }//count>1
  else{
  fieldSysId=formObj.fieldSysId.value;	
     fldmodei = formObj.fldmodei.value;	 //fldmodestr = fieldSysId+'[VELSEPIDMODE]'+ fldmode;
	 fldmodestr =  fldmodei;
  }
 
  if (count>1){	  
	  for (var i=0;i<count;i++)
	  {
	  	  //EDC_AT5a
		  var qryValidationType = "";
		  if (formObj.qryValidationType[i]){
			  qryValidationType=formObj.qryValidationType[i].value;
		  }
		  
		  var fieldQSelectId='qry_'+qryValidationType+'_'+formObj.fieldSysId[i].value;
		  var fieldQSelectValue = 1;
		  if (formObj.fldQuerySelect[i] != undefined){ 
			  fieldQSelectValue = (formObj.fldQuerySelect[i].checked)? 1:0;
		  }

		  var qrySelectFld = parentForm.elements[fieldQSelectId];
		  if (qrySelectFld == undefined){
		  	  qrySelectFld = window.opener.document.createElement('input');
			  qrySelectFld.setAttribute('id', fieldQSelectId);
			  qrySelectFld.setAttribute('name', fieldQSelectId);
			  qrySelectFld.setAttribute('type', "hidden");
			  qrySelectFld.setAttribute('value', fieldQSelectValue);
			  
			  window.opener.document.getElementById("fillform").appendChild (qrySelectFld);
		  } else{
			  window.opener.document.forms[lform].elements[fieldQSelectId].value = fieldQSelectValue;
		  }
		  
		 runValidation = true;
		 if (formObj.fldQuerySelect != undefined){
			runValidation = formObj.fldQuerySelect[i].checked;
		 }
		 if (runValidation){			
			rsnField=formObj.rsnField[i].value;
			var fieldQModeId='mode_'+qryValidationType+'_'+formObj.fieldSysId[i].value;
			var fieldStatusId='sts_'+qryValidationType+'_'+formObj.fieldSysId[i].value;
			var fieldCommentId='cmt_'+qryValidationType+'_'+formObj.fieldSysId[i].value;
			
			var qryModeFld = parentForm.elements[fieldQModeId];
			if (qryModeFld == undefined){
				qryModeFld = window.opener.document.createElement('input');
				qryModeFld.setAttribute('id', fieldQModeId);
				qryModeFld.setAttribute('name', fieldQModeId);
				qryModeFld.setAttribute('type', "hidden");
				qryModeFld.setAttribute('value', formObj.fldQueryMode[i].value);
				 
				window.opener.document.getElementById("fillform").appendChild (qryModeFld);
			} else{
				window.opener.document.forms[lform].elements[fieldQModeId].value = formObj.fldQueryMode[i].value;
			}

			var statusfld = parentForm.elements[fieldStatusId];
			if (statusfld == undefined){
				statusfld = window.opener.document.createElement('input');
				statusfld.setAttribute('id', fieldStatusId);
				statusfld.setAttribute('name', fieldStatusId);
				statusfld.setAttribute('type', "hidden");
				statusfld.setAttribute('value', formObj.cmbStatus[i].value);
				window.opener.document.getElementById("fillform").appendChild (statusfld);
			} else{
				window.opener.document.forms[lform].elements[fieldStatusId].value = formObj.cmbStatus[i].value;
			}
			
			var commentfld = parentForm.elements[fieldCommentId];
			if (commentfld == undefined){
				commentfld = window.opener.document.createElement('input');
				commentfld.setAttribute('id', fieldCommentId);
				commentfld.setAttribute('name', fieldCommentId);
				commentfld.setAttribute('type', "hidden");
				commentfld.setAttribute('value', formObj.comments[i].value);
				window.opener.document.getElementById("fillform").appendChild (commentfld);
			} else{
				window.opener.document.forms[lform].elements[fieldCommentId].value = formObj.comments[i].value;
			}
			
		   	if (typeof(window.opener.document.forms[lform])!="undefined")
		  	{
			  	if (formObj.reasons){//EDC_AT4 extn
		   			if (!(validate_col('Reason',formObj.reasons[i]))) return false;
		   			window.opener.document.forms[lform].elements[rsnField].value=formObj.reasons[i].value;
			  	} else
			  		window.opener.document.forms[lform].elements[rsnField].value='';				

		   		if (!(validate_col('Query Status',formObj.cmbStatus[i]))) return false;

		   	}
		 }
	  
	  }
  }
  else
  {
	//EDC_AT5a
	var qryValidationType = "";
	if (formObj.qryValidationType){  
		qryValidationType=formObj.qryValidationType.value;
	}

	var fieldQSelectId='qry_'+qryValidationType+'_'+formObj.fieldSysId.value;
	var fieldQSelectValue = 1;
	if (formObj.fldQuerySelect != undefined){  
		fieldQSelectValue = (formObj.fldQuerySelect.checked)? 1:0;
	}
	var qrySelectFld = parentForm.elements[fieldQSelectId];
	if (qrySelectFld == undefined){
		qrySelectFld = window.opener.document.createElement('input');
		qrySelectFld.setAttribute('id', fieldQSelectId);
		qrySelectFld.setAttribute('name', fieldQSelectId);
		qrySelectFld.setAttribute('type', "hidden");
		qrySelectFld.setAttribute('value', fieldQSelectValue);
		window.opener.document.getElementById("fillform").appendChild (qrySelectFld);
	} else{
		window.opener.document.forms[lform].elements[fieldQSelectId].value = (formObj.fldQuerySelect.checked)? 1:0;
	}

	  runValidation = true;
	  if (formObj.fldQuerySelect != undefined){
	  		runValidation = formObj.fldQuerySelect.checked;
	  }
	  if (runValidation){
   	  	rsnField=formObj.rsnField.value;
   	 	var fieldQModeId='mode_'+qryValidationType+'_'+formObj.fieldSysId.value;
   	  	var fieldStatusId='sts_'+qryValidationType+'_'+formObj.fieldSysId.value;
		var fieldCommentId='cmt_'+qryValidationType+'_'+formObj.fieldSysId.value;

		var qryModeFld = parentForm.elements[fieldQModeId];
		if (qryModeFld == undefined){
			qryModeFld = window.opener.document.createElement('input');
			qryModeFld.setAttribute('id', fieldQModeId);
			qryModeFld.setAttribute('name', fieldQModeId);
			qryModeFld.setAttribute('type', "hidden");
			qryModeFld.setAttribute('value', formObj.fldQueryMode.value);
			 
			window.opener.document.getElementById("fillform").appendChild (qryModeFld);
		} else{
			window.opener.document.forms[lform].elements[fieldQModeId].value = formObj.fldQueryMode.value;
		}
		
		var statusfld = parentForm.elements[fieldStatusId];
		if (statusfld == undefined){
			statusfld = window.opener.document.createElement('input');
			statusfld.setAttribute('id', fieldStatusId);
			statusfld.setAttribute('name', fieldStatusId);
			statusfld.setAttribute('type', "hidden");
			statusfld.setAttribute('value', formObj.cmbStatus.value);
			window.opener.document.getElementById("fillform").appendChild (statusfld);
		} else{
			window.opener.document.forms[lform].elements[fieldStatusId].value = formObj.cmbStatus.value;
		}
		
		var commentfld = parentForm.elements[fieldCommentId];
		if (commentfld == undefined){
			commentfld = window.opener.document.createElement('input');
			commentfld.setAttribute('id', fieldCommentId);
			commentfld.setAttribute('name', fieldCommentId);
			commentfld.setAttribute('type', "hidden");
			commentfld.setAttribute('value', formObj.comments.value);
			window.opener.document.getElementById("fillform").appendChild (commentfld);
		} else{
			window.opener.document.forms[lform].elements[fieldCommentId].value = formObj.comments.value;
		}
		
   	  	  if (typeof(window.opener.document.forms[lform])!="undefined")
      	  {
   	  		if (formObj.reasons){//EDC_AT4 extn
	   			if (!(validate_col('Reason',formObj.reasons))) return false;
	   			window.opener.document.forms[lform].elements[rsnField].value=formObj.reasons.value;
   	  		} else 
   	  			window.opener.document.forms[lform].elements[rsnField].value='';

	   		if (!(validate_col('Query Status',formObj.cmbStatus))) return false;
	   	  }	
	  }	  
  	}
	if (typeof(window.opener.document.forms[lform])!="undefined")
		window.opener.document.forms[lform].fldMode.value = fldmodestr;

  
 // formObj.fldmode.value = fldmodestr;
// window.opener.document.er_fillform1.elements[fldmode].value = formObj.fldmode.value;
   if (typeof(window.opener.document.er_fillform1)!="undefined") {
 window.opener.document.er_fillform1.action="updateFormData.jsp";
  //window.opener.document.er_fillform1.action="updateFormData.jsp";
  window.opener.document.er_fillform1.target="";
  window.opener.document.er_fillform1.submit();
  }
  window.close();
  
  
}

function enableFldQuery(cnt){
	var fldQuerySelect = document.getElementsByName('fldQuerySelect')[cnt];
	
	if (fldQuerySelect.checked){
		document.getElementsByName('rsnField')[cnt].disabled = false;
		if (document.getElementsByName('reasons')[cnt])//EDC_AT4 extn
			document.getElementsByName('reasons')[cnt].disabled = false;
		document.getElementsByName('cmbStatus')[cnt].disabled = false;
		document.getElementsByName('comments')[cnt].disabled = false;
	}else{
		document.getElementsByName('rsnField')[cnt].disabled = true;
		if (document.getElementsByName('reasons')[cnt])//EDC_AT4 extn
			document.getElementsByName('reasons')[cnt].disabled = true;
		document.getElementsByName('cmbStatus')[cnt].disabled = true;
		document.getElementsByName('comments')[cnt].disabled = true;
	}
	
}

function countCharacters(obj,evt,limit,counter)
{
	var notes = obj.value;
	var charCount = obj.value.length;
	var allowedChars = parseInt(limit)- parseInt(charCount);
	if(parseInt(allowedChars)<=0)
	{
		if (parseInt(allowedChars) < 0)
			obj.value=notes.substring(0,limit);
		allowedChars="0  <font color='red'><%=LC.L_More_Than%><%--More than*****--%>"+" "+limit+" "+"<%=LC.L_Char_NotAllowed%><%--Characters not allowed*****--%></font>";
	}
	document.getElementsByName("charCount")[counter].innerHTML="<%=LC.L_Chars_Allowed%><%--Characters allowed*****--%>"+": "+allowedChars;
}

function openQueryHistory(studyId,formQueryId,fldName,formFilledFormId,formId){

	var windowName=window.open('formQueryHistory.jsp?studyId='+studyId
			+'&formQueryId='+formQueryId
			+'&fieldName='+encodeString(fldName)
			+'&filledFormId='+formFilledFormId+'&from=3&formId='+formId+'&showLink=0',
			'Information1','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200');
	windowName.focus();

}

function enableFldQueryAll(fldQuerySelectAll){
	var fldQueryArray = document.getElementsByName('fldQuerySelect');
	var fldQueryModeArray = document.getElementsByName('fldQueryMode');

	for (var i=0; i< fldQueryArray.length; i++){
		if (fldQueryModeArray[i].value != "N"){
			fldQueryArray[i].checked = fldQuerySelectAll.checked;
			enableFldQuery(i);
		}
	}
	
}
 function  validate(formobj){ 
 
   /* if (!(validate_col('e-Signature',formobj.eSign))) return false

	
	if(isNaN(formobj.eSign.value) == true) {
	alert("Incorrect e-Signature. Please enter again");
	formobj.eSign.focus();
	return false;
   	}*/

   }
  
var skipcycle = false

function focOnMe(){
if(!skipcycle){
	window.focus();
}
popopen =  setTimeout('focOnMe()', 2);
}


   </script>



<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="fldlibB" scope="request" class="com.velos.eres.web.fieldLib.FieldLibJB"/>
<jsp:useBean id="frmQueryB" scope="request" class="com.velos.eres.web.formQuery.FormQueryJB"/>
<jsp:useBean id="groupB" scope="request" class="com.velos.eres.web.group.GroupJB"/>
<%@ page language = "java" import = "java.util.*,com.velos.eres.business.common.*"%>
<%@ page language = "java" import = "com.velos.eres.web.formQuery.FormQueryJB"%>


<body onload = "popopen =  setTimeout('focOnMe()', 2);">
<DIV  id="div1" style="overflow:auto; height:100%;"> 	<!-- Shoaib & Rashi fixed for bug #21374-->
<%
HttpSession tSession = request.getSession(true); 
int cnt = 0;
if (sessionmaint.isValidSession(tSession))
{

%>	
		<jsp:include page="sessionlogging.jsp" flush="true"/>
 <%   
	String eSign= request.getParameter("eSign");
	String formStatus = request.getParameter("er_def_formstat");
	
   	String oldESign = (String) tSession.getValue("eSign");
	String param = "";
	String paramVal = "";
	String paramName = "";
	
	String eSignRequired = "";		
   	eSignRequired = request.getParameter("eSignRequired");
	
	if(StringUtil.isEmpty(eSignRequired))
	{
		eSignRequired = "0";
	}   

	if (eSignRequired.equals("0"))
	{
			eSign = "";
			oldESign = "";
	}	

   if(oldESign.equals(eSign)) 
   {
   		String formId = "";
   		formId = (String) tSession.getAttribute("formId");
		if (formId == null || formId.equals("null"))
			formId = (String) request.getParameter("formId");
		tSession.setAttribute("formId", formId);
		
		int errorCode=0;
		String src = "";
		String selectedTab="";
		String formDispLocation = (String)tSession.getAttribute("formDispLocation");
		String formFillMode = (String)tSession.getAttribute("formFillMode");
		
		formFillMode = (formFillMode == null || formFillMode.equals("null"))? request.getParameter("formFillMode"):"";

		String formPullDown= (String)tSession.getAttribute("formPullDown");
		String formFillDt= (String)tSession.getAttribute("formFillDt");	
		
		String formFilledFormId =  request.getParameter("formFilledFormId");

		if(formFilledFormId == null )
		formFilledFormId = "";

		Enumeration paramNames = request.getParameterNames();
			
		ArrayList alParam = new ArrayList ();
		ArrayList alParamVal = new ArrayList ();
	
		while (paramNames.hasMoreElements())
		{
   			param = (String) paramNames.nextElement();
			
			if ((! param.equals("y")) && (! param.equals("x")) && (! param.equals("eSign")))
			{
				if ( ! alParam.contains(param))
				{
					alParam.add(param);
				}
			}	
		
		}
		for (int k = 0; k < alParam.size() ; k++ )
		{
   			paramName = (String) alParam.get(k);

			if ( request.getParameterValues(paramName) != null ) 
			{				
				paramVal = "";

   				for (int i = 0; i < request.getParameterValues(paramName).length; i++ ) 
   				{						
						if ( i == 0)
						{
							paramVal = request.getParameterValues(paramName)[i] ;
						}
						else
						{	
						//changed by sonika on April 28, 04
						//[VELCOMMA] is used as data separator incase of multiple choice 
   							paramVal =  paramVal + "[VELCOMMA]" + request.getParameterValues(paramName)[i]  ;											
						}
							
   				}

   				alParamVal.add(paramVal);

			}
				
		}
		

		//out.println("*****alParam:"+alParam);
		//out.println("*****alParamVal:"+alParamVal);
		
				
////////////////////////////anu
	
	
		
		
		ArrayList fldName = new ArrayList();
		ArrayList fldValue = new ArrayList();
		ArrayList fldValidation = new ArrayList();
		ArrayList qryValidationType = new ArrayList();
		ArrayList fldSysId = new ArrayList();
		
		int indx = 0;
		int velSepIndx = 0;
		String str ="";
		String strVal = "";
		String rsnStr="";
		String val = "";
		ArrayList rsnFieldList=new ArrayList();
		
     /*for(int j=0;j<alParam.size();j++)
			{
				out.println("alParam::"+alParam.get(j)+":alParamVal	::"+alParamVal.get(j)+"::");
			}
			out.println("////////////////////////////////////////////////////////////////////");*/
	for(int i=0;i<alParam.size();i++)
			{
				str = (String)alParam.get(i);
			
				strVal = (String)alParamVal.get(i);
				if(str.startsWith("hdn_") && !((strVal).equals(""))){
					String validationType = "";
				   	indx = str.indexOf("fld");	
				   	
					if (indx==-1){
						indx = str.indexOf("er_def_date_01");
						validationType = str.substring(str.indexOf("_")+1, str.indexOf("_er_def_date_01"));
					}else{
						validationType = str.substring(str.indexOf("_")+1, str.indexOf("_fld"));
					}
				   qryValidationType.add(""+validationType);
				   fldSysId.add(str.substring(indx));
				
				   velSepIndx =	strVal.indexOf("[VELSEPLABEL]");
				   fldValidation.add(strVal.substring(0,velSepIndx));
				   fldName.add(strVal.substring(velSepIndx+13));
				//StringTokenizer strTokenizer = new StringTokenizer(strVal,"[VELSEPLABEL]");
				
				//fldValidation.add(strTokenizer.nextToken());
				//fldName.add(strTokenizer.nextToken());

				rsnStr="rsn"+str.substring(3);
				if (rsnStr!=null) rsnFieldList.add(rsnStr);

			    val = request.getParameter(str.substring(indx));
				if(val == null || val.equals("null"))
				val = "";
				fldValue.add(val);	
				cnt++;
				}
				
			
			}
			System.out.println("fldName:"+fldName+"\nfldSysId:"+fldSysId+"\nfldValidation:"+fldValidation+"\nRsnList:"+rsnFieldList);
			
	if(cnt>0){
		
				
		StringBuffer ddReasons = new StringBuffer();
		StringBuffer ddReasonsN = new StringBuffer();
		CodeDao  cdRes = new CodeDao();
		cdRes.getCodeValues("form_resp");
		ArrayList cDesc=cdRes.getCDesc();
		ArrayList cId=cdRes.getCId();
		//ddReasons = cdRes.toPullDown("reasons");
	       
		ddReasonsN.append("<SELECT NAME=reasons onfocus='skipcycle=true' onblur='skipcycle=false'>") ;
		for (int counter = 0; counter <= cDesc.size() -1 ; counter++){
			
			ddReasonsN.append("<OPTION value = "+ cId.get(counter)+">" + cDesc.get(counter)+ "</OPTION>");
		}
		ddReasonsN.append("<OPTION value='' SELECTED> "+LC.L_Select_AnOption/*Select an Option*****/+"</OPTION>");
		ddReasonsN.append("</SELECT>");
		
		String roleCodePk="";
		String roleSubType="";
		
		String userIdFromSession = (String) tSession.getValue("userId");
		UserDao uDao = new UserDao();
		uDao.setUsrIds(EJBUtil.stringToInteger(userIdFromSession));
		uDao.getUsersDetails(userIdFromSession);
		String groupId = "";
		groupId = (String) uDao.getUsrDefGrps().get(0);
		
		String studyId ="";
		//EDC_At4 extn
    	studyId = (String) tSession.getValue("studyIdForFormQueries");	
	 	if (StringUtil.isEmpty(studyId)) 
	 		studyId="";     
	 	//tSession.removeAttribute("studyIdForFormQueries");	
	 	//
	 	
	 	if (! StringUtil.isEmpty(studyId) && EJBUtil.stringToNum(studyId) > 0)
		{
			ArrayList tId = new ArrayList();
			
			TeamDao teamDao = new TeamDao();
			teamDao.getTeamRights(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(userIdFromSession));
			tId = teamDao.getTeamIds();
							
			if (tId != null && tId.size() > 0)
			{
				ArrayList arRoleIds = new ArrayList();
				arRoleIds = teamDao.getTeamRoleIds();
				
				if (arRoleIds != null && arRoleIds.size() >0 )	
				{
					roleCodePk = (String) arRoleIds.get(0);
					//Bug #6003
					roleSubType = cdRes.getCodeSubtype(EJBUtil.stringToNum(roleCodePk));
					
					if (StringUtil.isEmpty(roleCodePk))
					{
						roleCodePk="";
					}
				}	
				else
				{
					roleCodePk ="";
				}
				
			}	
			else
			{
				roleCodePk ="";
			}
			//Bug #5992
			if (roleCodePk.equals("")){
				int grpId = Integer.parseInt(groupId);
				groupB.setGroupId(grpId);
				groupB.getGroupDetails();
				if (groupB.getGroupSuperUserFlag().equals("1")){
					String rolePK = groupB.getRoleId();
					if (rolePK == null || rolePK.equals("0")){
						roleCodePk = "";
						roleSubType = "";
					}else{
						roleCodePk = rolePK;
						roleSubType = cdRes.getCodeSubtype(EJBUtil.stringToNum(roleCodePk));
					}
				}
			}
		} else {			
			roleCodePk ="";
		}		

	 	CodeDao cdQStatus = new CodeDao();
	 	cdQStatus.getCodeValuesForStudyRole("query_status",roleCodePk);
		
		String statusPullDn = "";
		String statusId = "";
		 
	     FormQueryDao fqDao = new FormQueryDao();
		
		 String strFldSysIds = "";
		 
		 
		 for(int i=0;i<fldSysId.size();i++)
		 {
		   if(i==0)
		   strFldSysIds = "'"+(String)fldSysId.get(0)+"'";
		   else
		   strFldSysIds = strFldSysIds + "," +"'"+(String)fldSysId.get(i)+"'";
		 }
		
		 //KM-#4016
		 fqDao = frmQueryB.getSystemFormQueryStat(EJBUtil.stringToNum(formFilledFormId), fldSysId, fldValidation);
		 ArrayList pkQueryStats = fqDao.getFormQueryStatusIds();
		 ArrayList queryTypeIds =  fqDao.getFormQueryType();
		 ArrayList oldFldSysIds = fqDao.getFldSysIds();
		 ArrayList querySystemNotes =  fqDao.getQueryNotes();
		 
		 ArrayList formQueryIds = fqDao.getFormQueryIds();
		 ArrayList formQueryNotes = new ArrayList();
		 ArrayList formFieldIds = new ArrayList();
		 formFieldIds = fqDao.getFieldIds();
		 
		 ArrayList formFieldSysIds = new ArrayList();
		 formFieldSysIds = fqDao.getFldSysIds();
		 
		 ArrayList queryResponseStats =  fqDao.getQueryStatus();
		 
		 
		 // Bug 5333 - caused by the fact that the two ArrayLists - pkQueryStats and fldValidation - were
		 // not in the same order. To sort pkQueryStats, use that fact that 
		 // * fldSysId and fldValidation are in the same order
		 // * oldFldSysIds and pkQueryStats are in the same order
		 // and use fldSysId and oldFldSysIds to connect the two pairs.
		 // fldValidation is used along with fldSysId as there can be multiple soft checks overridden for a field.
		 // Similarly querySystemNotes is used along with oldFldSysIds.
		 // I use a HashMap for sorting efficiency.
		 ArrayList sortedPkQueryStats = new ArrayList(pkQueryStats);
		 HashMap pkQueryStatHash = new HashMap();
		 for (int iX=0; iX<fldSysId.size(); iX++) {
		     if (fldSysId.get(iX) == null) { continue; }
		     pkQueryStatHash.put(fldSysId.get(iX)+"_"+fldValidation.get(iX), new Integer(iX));
		 }
		 for (int iY=0; iY<oldFldSysIds.size(); iY++) {
		     if (pkQueryStatHash.get(oldFldSysIds.get(iY)+"_"+querySystemNotes.get(iY)) == null) 
		     	{ continue; }
		     int sortedIndex = ((Integer)pkQueryStatHash.get(
		    		 oldFldSysIds.get(iY)+"_"+querySystemNotes.get(iY))).intValue();
		     if (sortedIndex < 0 || sortedIndex >= sortedPkQueryStats.size()) { continue; }
		     sortedPkQueryStats.set(sortedIndex, pkQueryStats.get(iY));
		 }

		 tSession.setAttribute("pkQueryStats",sortedPkQueryStats);
		
		%>
		<P class="defComments"><%=MC.M_FldsHaving_SoftCheck%><%--System-generated Queries for Form Field Validations*****--%></P>		

		<Form name="formdata" id="queryfrm" method="post" onSubmit="if (submitParent(document.formdata) == false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
		
			<table width="100%" class="tablebdr">
			<tr>
			<th width="1%" class="normalcol"
			
			<%
			//KM-#5939
			if (formFillMode.equals("N") || formQueryIds.size() == 0) {
			%> 
				style ="display:none"
			<%} %> >
			<%=LC.L_Select%><%--Select*****--%> 	<input type="checkbox" id="fldQuerySelectAll" name="fldQuerySelectAll" onclick="return enableFldQueryAll(this);" 
			
			<%if (formFillMode.equals("N")){%>
				value="1" checked disabled
			<%}else{ %>
				value="0"
			<%} %>
			/></th>
			<th width="20%" class="normalcol"><%=LC.L_Fld_Name%><%--Field Name*****--%></th>
			<th width="20%" class="normalcol"><%=LC.L_Value_Entered%><%--Value Entered*****--%></th>
			<th width="20%" class="normalcol"><%=LC.L_Validation_ForFld%><%--Validation for Field*****--%></th>
			<%if (!studyId.equals("")){ %>
				<%if (roleSubType.equals("role_coord")){ %>
			<th width="15%" class="normalcol"><%=LC.L_Response%><%--Response*****--%><FONT class="Mandatory" >* </FONT></th>
				<%} %>
			<%}else{ %>
			<th width="15%" class="normalcol"><%=LC.L_Response%><%--Response*****--%><FONT class="Mandatory" >* </FONT></th>
			<%} %>
			<th width="20%" class="normalcol"><%=LC.L_Query_Status%><%--Query Status*****--%><FONT class="Mandatory" >* </FONT></th>
			<th width="15%" class="normalcol"><%=LC.L_Comments%><%--Comments*****--%></th>
			</tr>
			<%
			String fldQueryMode = "";
			int idx = 0 ;
			int validationIdx = 0 ;
			Integer iReason ;
			String pullDown = "";
			
			for(int counter=0;counter<fldName.size();counter++)
				{ 
				ddReasons = new StringBuffer();
				String fieldSysId = (String)fldSysId.get(counter);
				String fieldValid = (String)fldValidation.get(counter);
				
				if(oldFldSysIds.contains(fieldSysId)){
					int ifld = 0;
					for (ifld = 0; ifld <oldFldSysIds.size(); ifld++){
						if (oldFldSysIds.get(ifld).equals(fieldSysId) 
							&& querySystemNotes.get(ifld).equals(fieldValid)){
							idx = ifld;
							fldQueryMode = "M";
							break;
						}
					}
					if (ifld == oldFldSysIds.size()){
						fldQueryMode = "N";
					}
				}
				else fldQueryMode = "N";	
				
				String normalrowCss="";
		        String selectedColCss="";
				if ((counter%2)==0) 
				{
					normalrowCss="normalinfoeven";
					selectedColCss="selectedinfoeven";
				}
				else{
					normalrowCss="normalinfoodd";
			       	selectedColCss="selectedinfoodd";
				}%>				
				
				<tr class="normalinfo" height="25">
					<%
					int formQueryId = 0;
					if (!fldQueryMode.equals("N"))
						if (formQueryIds.size() > 0)
							formQueryId = EJBUtil.stringToNum(""+formQueryIds.get(idx));
					%>
					<input type=hidden name="fldQueryMode" value="<%=fldQueryMode%>"/>
					<td class="<%= normalrowCss%>"
						<%if (formFillMode.equals("N") || formQueryIds.size() == 0 ) {%> 
							style ="display:none"
						<%} %>>
						<%if (formFillMode.equals("N") || formQueryId == 0 || fldQueryMode.equals("N")){%>
							<input type="checkbox" id="fldQuerySelect" name="fldQuerySelect" onclick="return enableFldQuery(<%=counter%>);" value="1" checked disabled
							<%if (fldQueryMode.equals("N")){%> 
								style ="display:none"
							<%} %>
							/>
						<%}else{ %>
							<input type="checkbox" id="fldQuerySelect" name="fldQuerySelect" onclick="return enableFldQuery(<%=counter%>);" value="0"/>
						<%} %>
					</td>
					<td class="<%= normalrowCss%>"><%=fldName.get(counter)%>
					<input type=hidden name="qryValidationType" value="<%=qryValidationType.get(counter)%>"/>
					<input type=hidden name="fieldSysId" value=<%=fldSysId.get(counter)%>>
					<input type=hidden name="fldmodei" value="<%=fldQueryMode%>"/>
					
					<%if (!formFillMode.equals("N") && formQueryId > 0){%>
						<BR/> <BR/>
						<a href="#" onClick="return openQueryHistory('<%=studyId%>','<%=formQueryId%>','<%=((String)fldName.get(counter))%>','<%=formFilledFormId%>','<%=formId%>');"><%=LC.L_Query_History%><%--Query History*****--%></a>
					<%} %>
					</td>
					<td class="<%= normalrowCss%>"><%=fldValue.get(counter)%>
					&nbsp;<input type=hidden name="rsnField" value="<%=rsnFieldList.get(counter)%>">
					</td>
					<td class="<%= normalrowCss%>"><%=fldValidation.get(counter)%>
					<input type=hidden name="fldValidation" value="<%=fldValidation.get(counter)%>">
					</td>
					<%
					String sID = "";
					 //KM-#4016
					 if(fldQueryMode.equals("M")){				
						if(queryTypeIds.get(idx)==null)
							sID = "0";
						else
						    sID = (String)queryTypeIds.get(idx);
						pullDown = cdRes.toPullDown("reasons",Integer.parseInt(sID),true);
						ddReasons.append("<SELECT NAME=reasons onfocus='skipcycle=true' onblur='skipcycle=false'>") ;
						ddReasons.append("<OPTION value=''>"+LC.L_Select_AnOption/*Select an Option*****/+"</OPTION>") ;	//KM-3758 			
						if (cId.size() > 0)
						{
							for (int count = 0; count < cId.size()  ; count++)
							{
             				    String fullFieldId = (String)fldSysId.get(counter);
             				    if (fullFieldId == null) { continue; }
             				    String extractedFieldId = extractFieldId((String)fldSysId.get(counter));
             				    if (extractedFieldId == null) { continue; }
             				    int eFeildId = -1;
             				    try {
             				        eFeildId = Integer.parseInt(extractedFieldId);
             				    } catch(Exception e) {}
             				    
                 				iReason = (Integer)cId.get(count);
                 				
                 				if(iReason.intValue() == EJBUtil.stringToNum((String)(queryTypeIds.get(idx)))) {
                 				    ddReasons.append("<OPTION value = "+ iReason+" selected>" + cDesc.get(count)+ "</OPTION>");
                 				} else {
                 				    ddReasons.append("<OPTION value = "+ iReason+">" + cDesc.get(count)+ "</OPTION>");
                 				}
                 			}
                 		}	

            		ddReasons.append("</SELECT>");
	
				}
				if(fldQueryMode.equals("N")){
					ddReasons = ddReasonsN;
				}%>
				
				<%//EDC_AT4 extn
				if (!studyId.equals("")){ %>
					<%if (roleSubType.equals("role_coord")){ %>
					<td class="<%= normalrowCss%>">
						<%=ddReasons%>
					</td>
					<%}%>
				<%}else{ %>
					<td class="<%= normalrowCss%>">
						<%=ddReasons%>
					</td>
				<%} %>
					<td class="<%= normalrowCss%>">
					<%
					if(!fldQueryMode.equals("N")){
						if (queryResponseStats != null && queryResponseStats.size() > 0){
							//referring to idx for old queries
							statusId = (queryResponseStats.get(idx)).toString();
						%>
						<b><%=LC.L_Prev_Status%><%--Previous Status*****--%>: <%=cdQStatus.getCodeDescription(EJBUtil.stringToNum(statusId))%></b><BR/>
						<%
						}else statusId ="";
					}
					//statusPullDn = cdQStatus.toPullDown("cmbStatus", EJBUtil.stringToNum(statusId), false);
					statusPullDn = cdQStatus.toPullDown("cmbStatus", 0);
					%>
					<%=statusPullDn%>
					</td>
					<td class="<%= normalrowCss%>">
						<textarea id="comments" name="comments" cols="30" rows="3"  MAXLENGTH=4000 onkeypress="return countCharacters(this,event,4000,<%=counter%>);"
						onkeyup="return countCharacters(this,event,4000,<%=counter%>);" onClick="return countCharacters(this,event,4000,<%=counter%>);"
						onmouseOut="return countCharacters(this,event,4000,<%=counter%>);"></textarea> 
						<BR/><div id="charCount" name="charCount"><%=LC.L_CharAllwd_4000%><%--Characters allowed: 4000*****--%></div>  <%-- YK 17DEC-- EDC_AT4--%> <%-- YK 06Feb-2011 Bug #5694 --%>
					</td>
				</tr>
				<script>
					enableFldQuery(<%=counter%>);
				</script>
				<%
						
			}// for loop
		%>
				
		<tr>
			<td>
				<!--<A  href="#" onclick="history.go(-1);"><img src="../images/jpg/Back.gif" align="absmiddle"   border=0> </A>-->
			</td>
		</tr>
		<tr>
			<input type="hidden" name="totCount" value="<%=cnt%>"> 
		   	<!--td width="41%">
				<e-Signature <FONT class="Mandatory">* </FONT>
		   	</td-->
		   	<!--td width="10%">
				<input type="password" name="eSign" maxlength="8" onfocus='skipcycle=true' onblur='skipcycle=false'>
			</td-->
		</tr>
		<tr align="center" bgcolor="#cccccc">
			<td colspan=7>
				<table cellpadding=0 cellspacing=0>
					<tr>
						<td align="center">
                        <table width="98%" border="0" cellspacing="0" cellpadding="0" bgcolor="#cccccc" >
	                        <tr valign="baseline" bgcolor="#cccccc">
                            <td>
                            <button onclick="self.close()" type="button"><%=LC.L_Back%></button>
                            </td>
                            </tr>
                        </table>
						</td>
						<td width="65%"></td>
						<td align="center">
							<jsp:include page="submitBar.jsp" flush="true"> 
								<jsp:param name="displayESign" value="N"/>
								<jsp:param name="formID" value="queryfrm"/>
								<jsp:param name="showDiscard" value="N"/>
								<jsp:param name="noBR" value="Y"/>
							</jsp:include>		
						</td>
					</tr>
				</table>
			</td>
	 	</tr>
	</table>
	 <input type=hidden name="calledFrom" value="<%=formDispLocation%>"/>
	 <input type=hidden name="formFillMode" value="<%=formFillMode%>"/>
	 <input type=hidden name="formDispLocation" value="<%=formDispLocation%>"/>
	
	</Form>
<%	} else {   %>
  <br><br><br><br><br><br><br>
  <table width="100%"  >
	<tr><td width="10%">&nbsp;</td>
		<td width="60%">
			<p class = "sectionHeadings"><%=MC.M_VerifyFrmValidation_Wait%><%--Verifying form validations,please wait*****--%>....</p>
		</td>
	</tr>
  </table>
  <script>
	if (window.opener.document.er_fillform1 != null &&
		typeof(window.opener.document.er_fillform1)!="undefined") {
		window.opener.document.er_fillform1.action="updateFormData.jsp";
		window.opener.document.er_fillform1.target="";
		if (window.opener.document.er_fillform1.override_count != null) {
			 if (parseInt(window.opener.document.er_fillform1.override_count.value, 10) > 0) {
				 window.opener.document.er_fillform1.override_count.value=0;
		 		window.opener.document.er_fillform1.submit();
			 }
		}
	}
	self.setTimeout('window.close()',500);
  </script>
<%	} %>
				
<%} //esign
	else {%>
	<br><br><br><br><br><br><br>
	<table width="100%"  >
		<tr><td width="10%">&nbsp;</td>
		<td width="60%">
		
			<p class = "sectionHeadings">
			
			<%=MC.M_EtrWrongEsign_CloseWin%><%--You have entered a wrong e-Signature. Please close this window to change.*****--%>
			
			</p>
	
		</td>
		</tr>
		<tr></tr>
		<tr height=20>
			<td></td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<button onClick = "self.close()" type="button"><%=LC.L_Back%></button></td>
		</tr>
		<tr>				
			<td>			
			</td>		
		</tr>
	</table>
	<%}%>
<%} //Valid Session%>
	</DIV>
</body>
<script>
	window.moveTo(0,0);
	window.resizeTo(screen.width, 550);
</script>  
<div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</html>