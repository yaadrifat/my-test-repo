<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:useBean id="repB" scope="request" class="com.velos.eres.web.report.ReportJBNew"/>
<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,javax.xml.transform.TransformerFactory,javax.xml.transform.Transformer,javax.xml.transform.Source,javax.xml.transform.stream.StreamSource,javax.xml.transform.stream.StreamResult,javax.xml.transform.TransformerException,javax.xml.transform.TransformerConfigurationException,java.io.*,java.text.*,java.net.URL,com.velos.eres.web.user.UserJB,com.velos.eres.web.grpRights.*"%>
<%
HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession))
{
	int repId = EJBUtil.stringToNum(request.getParameter("repId"));
	String uName =(String) tSession.getValue("userName");	 
	int acc = EJBUtil.stringToNum((String) (tSession.getValue("accountId")));
	String accountId= (String) (tSession.getValue("accountId"));
	int userId = EJBUtil.stringToNum((String) (tSession.getValue("userId")));
	userB = (UserJB)tSession.getValue("currentUser");
	Enumeration ep = request.getParameterNames();
	String paramString = "";
	String currName = "";
	ArrayList filterKey=new ArrayList();
	ArrayList filterValue=new ArrayList();

	filterKey.add("sessAccId");
	filterValue.add(tSession.getValue("accountId"));

	filterKey.add("sessUserId");
	filterValue.add(tSession.getValue("userId"));

	while(ep.hasMoreElements()){
	   currName = (String)ep.nextElement();
	   paramString = request.getParameter(currName);
		filterKey.add(currName);
		filterValue.add(paramString);
	}
	ReportDaoNew rD =new ReportDaoNew();
	rD.setFilterKey(filterKey.toArray());
	rD.setFilterValue(filterValue.toArray());
	int success=rD.createReport(repId,acc);

	ArrayList repXml = rD.getRepXml();
	Object xmlObj;
	String xml=null;
	String xsl=null;
	xmlObj=repXml.get(0);				
	if (!(xmlObj == null)) 
	{
		xml=xmlObj.toString();
		xml = StringUtil.replace(xml,"&amp;#","&#");
	}
	else
	{
		out.println(MC.M_Err_GettingXml);/*out.println("Error in getting XML");*****/
		Rlog.fatal("Report",MC.M_Err_GettingXml);/*Rlog.fatal("Report","ERROR in getting XML");*****/			
		return;
	}
//	xsl = "<?xml version='1.0'?> <xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'><xsl:output method='text'/><xsl:template match='ROWSET'><xsl:apply-templates select='ROW'/></xsl:template><xsl:template match='ROWSET'>{rows: { trows: &quot;<xsl:value-of select = 'count(ROW)' />&quot;,row : [<xsl:apply-templates select='ROW'/>]}}</xsl:template><xsl:template match='ROW'><xsl:apply-templates select='*'/><xsl:if test='position() != last()'>,</xsl:if></xsl:template><xsl:template match='*'><xsl:if test='position() = 1'>{</xsl:if><xsl:value-of select = \"concat(name(),\':\',\'&quot;\',text(),\'&quot;\')\" /><xsl:choose><xsl:when test='position() = last()'>}</xsl:when><xsl:otherwise>,</xsl:otherwise></xsl:choose></xsl:template></xsl:stylesheet>";
	xsl = "<?xml version='1.0'?> <xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'><xsl:output method='text'/><xsl:template match='ROWSET'><xsl:apply-templates select='ROW'/></xsl:template><xsl:template match='ROWSET'>{rows: { trows: &#39;<xsl:value-of select = 'count(ROW)' />&#39;,row : [<xsl:apply-templates select='ROW'/>]}}</xsl:template><xsl:template match='ROW'><xsl:apply-templates select='*'/><xsl:if test='position() != last()'>,</xsl:if></xsl:template><xsl:template match='*'><xsl:if test='position() = 1'>{</xsl:if><xsl:value-of select = \"name()\" />: \'<xsl:value-of select = \"text()\"/>\'<xsl:choose><xsl:when test='position() = last()'>}</xsl:when><xsl:otherwise>,</xsl:otherwise></xsl:choose></xsl:template></xsl:stylesheet>";

	try  {	
		TransformerFactory tFactory1 = TransformerFactory.newInstance();
		Reader mR1=new StringReader(xml); 
		Reader sR1=new StringReader(xsl); 
		Source xmlSource1=new StreamSource(mR1);
		Source xslSource1 = new StreamSource(sR1);
 		Transformer transformer1 = tFactory1.newTransformer(xslSource1);
		transformer1.setOutputProperty("encoding", "UTF-8");		
	  	transformer1.transform(xmlSource1, new StreamResult(out));
    }
	catch (Exception e)
	{
  		out.write(e.getMessage());
 	}
%>
<%
} //end of if session times out
else
{
%>
<%=LC.L_Access_Forbidden %><%-- Access Forbidden*****--%>
<%
} 
%>