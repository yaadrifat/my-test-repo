<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.velos.eres.service.util.LC" %>

<%--Include JSPs --%>
<%@page import="com.velos.eres.service.util.StringUtil"%>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<jsp:include page="velos_includes.jsp"></jsp:include>
<%int patCompDetRight = StringUtil.stringToNum(request.getParameter("patCompDetRight")); %>
<jsp:include page="patientDetailsInclude.jsp" flush="true"/>
<%
if(patCompDetRight>=4){
%>
<jsp:include page="morePerDetailsInclude.jsp" flush="true"/>
<%} %>
<%----%>
<%!
static final String patientScreen_AdminGrps = "[Config_PatientScreen_AdminGrps]";
%>

<div id="submitFailedDialog" title="<%=LC.L_Error%>" class="comments" style="display:none; overflow:auto">
	<table align="center">
		<tr><td height="20px">&nbsp;</td></tr>
		<tr align="center">
			<td id="errorMessageTD"></td>
		</tr>
		<tr><td height="20px">&nbsp;</td></tr>
		<tr align="center">
			<td>
				<button onclick='$j("#submitFailedDialog").dialog("close");'><%=LC.L_Close%></button>
			</td>
		</tr>
	</table>
</div>
<script>
var patientDemogScreenFunctions = {
	patientScrnIsAdmin: false,
	patCompleteDetailsRight: <%=patCompDetRight%>,
	patientScrnAdminGrps:"<%=((patientScreen_AdminGrps.equals(LC.Config_PatientScreen_AdminGrps)) ? 
			"" : LC.Config_PatientScreen_AdminGrps)%>",
	validatePatientDemogScreen: {}
};

{
	var grp = ''+patientDetailsFunctions.defUserGroup;
	var patientScrnAdminGrps = (patientDemogScreenFunctions.patientScrnAdminGrps).split(",");
	patientDemogScreenFunctions.patientScrnIsAdmin = false;
	
	for (var indx=0; indx < patientScrnAdminGrps.length; indx++){
		if (grp == jQuery.trim(patientScrnAdminGrps[indx])){
			patientDemogScreenFunctions.patientScrnIsAdmin = true;
			break;
		}
	}
}
</script>

<script type="text/javascript">
var isValidatedForm = false;
patientDemogScreenFunctions.validatePatientDemogScreen = function() {
	//Call all the validate functions one-by-one
	if(!patientDetailsFunctions.validate()){
		return false;
	}
	if(patientDemogScreenFunctions.patCompleteDetailsRight>=4){
	if (!morePatientDetFunctions.validate()){ 
		return false;
	}}

	if (patientDemogScreenJS && patientDemogScreenJS.validate){
		if (!patientDemogScreenJS.validate()){
			return false;
		}
	}
	
	if (isValidatedForm) { return true; }
	isValidatedForm = true;

	$j.post('updatePatientDemogScreen',
		$j('#patientDemogForm').serialize(),
		function(data) {
			var errorMap = data.errorMap;
			var hasErrors = false;
			for (var key in errorMap) {
				hasErrors = true;
				isValidatedForm = false;
				$j('#errorMessageTD').html(errorMap[key]);
				break;
			}
			if (hasErrors) {
				$j('#submitFailedDialog').dialog({
					modal:true,
					closeText: '',
					close: function() {
						$j("#submitFailedDialog" ).dialog("destroy");
					}
				});
			} else {
				$("patientDemogForm").onSubmit=null;
				$("patientDemogForm").action ="genericMessage.jsp?messageKey=M_Data_SvdSucc&id=7";
				$("patientDemogForm").submit();
				$("patientDemogForm").action = '#';
				$("patientDemogForm").onSubmit = "return patientDemogScreenFunctions.validatePatientDemogScreen();";
			}
		}
	);

	return false;
};
</script>


<%--Custom JS inclusion --%>
<script type="text/javascript" src="./js/velosCustom/patientDemogScreen.js"></script>
<%----%>