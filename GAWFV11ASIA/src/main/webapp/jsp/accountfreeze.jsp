<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<title><%=LC.L_Acc_Blocked%><%-- Account Blocked*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="styles/login.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" type="image/x-icon" href="./favicon.ico" />
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<%@ page import="com.velos.eres.service.util.*"%>
<%@page import = "com.velos.eres.service.util.Configuration"%>
<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB" />
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<style>
.input1{
    border: 1px solid #acaa9e;
    border-radius: 0px;
    width: 180px;
}
button:hover{
background-color:yellow;
}
</style>

<%
String supportEmail = Configuration.SUPPORTEMAIL;
int failedAccessCount = Configuration.FAILEDACCESSCOUNT;
String hiddenflag ="";
hiddenflag = request.getParameter("hiddenflag")==null?"":request.getParameter("hiddenflag");
String flag = request.getParameter("flag")==null?"":request.getParameter("flag");
%>

<table width = "100%" border = 0>
<tr><td>
<p>
<%--<font font-family =  "arial" color="red"; font-size=14><b> Account Blocked</b>*****--%>
<font font-family =  "arial" color="red"; font-size=14><b> <%=LC.L_Acc_Blocked%></b>
</p>
</td></tr>
<tr><td><br><p class = "defComments">
<%--There have been <%=failedAccessCount%> failed login attempts to this account and the account is now Blocked. 
To reactivate your account, please contact your account administrator. 
If you are an account administrator, contact Customer Support at*****--%> 
<%if(!hiddenflag.equalsIgnoreCase("0")){ %>
<%=LC.L_There_HaveBeen%> <%=failedAccessCount%> <%=MC.M_FailLogin_AttemptAccBlock%> <u><a href="mailto:<%=supportEmail%>" style="color: #0000FF"><%=supportEmail%></a></u> </p>
<%} else { %>
<%=LC.L_There_HaveBeen%><%=MC.M_FailLogin_AttemptAccForm %>

<%} %>
</p>
</td></tr>
<%if(hiddenflag.equalsIgnoreCase("0")){ %>

		<Form name="Account Reset" method="post" action="updateresetpass.jsp" onsubmit="">
		
<div class="tmpHeight"></div>

<table  width="99%" border="0" cellspacing="0" cellpadding="3" class="basetbl outline midAlign">
	
	<tr >
	<td style="font-family: Arial,Helvetica,sans-serif;font-size: 1.5em;">Last Name</td>
  <td><input type="text"  class="input1" id="lname" name="lname" value="" style="width:20%; height:40px" ></td>
  </tr>
  <tr>
  
  <td style="font-family: Arial,Helvetica,sans-serif;font-size: 1.5em;">Login Id</td>
  <td><input type="text" class="input1" id="lid" name="lid" value="" style="width:20%; height:40px"></td>
  
  </tr>
  <tr>
  
  <td style="font-family: Arial,Helvetica,sans-serif;font-size: 1.5em;">Letter Code</td>
  <td><input type="text"  class="input1" id="lcode" name="lcode" value="" style="width:20%; height:40px"></td>
  
  </tr>
  <tr>
  
  <td style="font-family: Arial,Helvetica,sans-serif; font-size: 1.5em;" >E-mail Address</td>
  <td><input type="text" class="input1" id="email" name="email" value="" style="width:20%; height:40px"></td>
  
  </tr>
  <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>
   <tr height="20">
	<td width="12%" valign="bottom">
					
     </td>
    
	                <td>
	      			<button type="submit" style="width:10%; position:relative; margin:auto; text-align: center; background-color: #5c5c5c; font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:12px; color:#fff; border-radius:3px 3px 3px 3px;" onclick ="return validate();"><font color="white"><%=LC.L_Reset%></font></button>&nbsp;&nbsp;&nbsp;&nbsp;
	      			<button type="button" style="width:10%; position:relative; margin:auto; text-align: center; background-color: #5c5c5c; font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:12px; color:#fff; border-radius:3px 3px 3px 3px;" onClick = "goBack();"><font color="white"><%=LC.L_Close %></font></button>
	      			</td>
	</tr>
	

			
				</table>
			
				<input type="hidden" name="fromPage" value="User"/>
				<input type="hidden" name="password" value="on"/>
				<input type="hidden" name="esignBox" value="on"/>
				<input type="hidden" name="flag" value="1" >

				</form>
				
				<%} %>
<%if(!hiddenflag.equalsIgnoreCase("0")){ %>
<tr><td align="center"><br><br><%--Modified By Tarun Kumar : Bug#10302 --%>
<!--<button onClick="window.self.close();"  style="text-align: center;" ><font color="white"><%=LC.L_Close %></font></button>-->

<button type="button" style="width:10%; position:relative; margin:auto; text-align: center; background-color: #5c5c5c; font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:12px; color:#fff; border-radius:3px 3px 3px 3px;" onClick = "goBack();"><font color="white"><%=LC.L_Close %></font></button>&nbsp;&nbsp;
<%if(!("N".equalsIgnoreCase(LC.USER_DISABLE_RESETPWD_LINK))){ %>
<br></br>
<button type="button" style="width:10%; position:relative; margin:auto; text-align: center; background-color: #5c5c5c; font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:12px; color:#fff; border-radius:3px 3px 3px 3px;" onClick = "resetForm();"><font color="white"><%=LC.L_Res_Pwd %></font></button>&nbsp;&nbsp;
<%} %>
</td></tr>
<%} %>
</table>
<SCRIPT>
function goBack(){
	 document.location.href = "ereslogin.jsp";
}	


function resetForm() {
    location.href = "accountfreeze.jsp?mode=N&hiddenflag=0";
};

</SCRIPT>