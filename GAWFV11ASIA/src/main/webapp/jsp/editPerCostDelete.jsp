<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">



<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>


<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

// 	if(isNaN(formobj.eSign.value) == true) {
<%-- 		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/ --%>
// 	formobj.eSign.focus();
// 	return false;
//    }
}
</SCRIPT>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="lineitemB" scope="session" class="com.velos.esch.web.lineitem.LineitemJB"/>
<%@ page language = "java" import = "com.velos.esch.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.aithent.audittrail.reports.AuditUtils"%>
<%@ page import = "com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>
<jsp:include page="include.jsp" flush="true"/>
<BODY> 
<br>

<DIV class="popDefault" id="div1">
<% 
	String mode= "";
	String lineitemId="";
	
	String creator="";
	String ipAdd="";
	String bgtcalId = "";
	String from  = "";
	String applyInFuture = "";
	String fromRt ="";
	int ret=0;
	HttpSession tSession = request.getSession(true); 
 	if (sessionmaint.isValidSession(tSession))
	{ 	
	  
				
		mode= request.getParameter("mode");
		if (mode==null) mode="initial";		
		lineitemId=request.getParameter("lineitemId");		
		
		bgtcalId = request.getParameter("bgtcalId");
		from  = request.getParameter("from");
		

		applyInFuture = request.getParameter("applyInFuture");

		fromRt = request.getParameter("fromRt");
		if (fromRt==null || fromRt.equals("null")) fromRt="";
		
if(from.equals("perCost")){%>	
	<title><%=LC.L_Personnel_CostDel%><%--Personnel Cost Delete*****--%> </title>
	<%}else if(from.equals("repLineitem")){%>
	<title><%=MC.M_Repeat_LineItemDel%><%--Repeat Line Item Delete*****--%> </title>
	<%}%>
		<FORM name="deletePerCost" method="post" id="percostdelfrm" action="editPerCostDelete.jsp" onSubmit="if (validate(document.deletePerCost)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
		<br><br>
		<input type="hidden" name="fromRt" value="<%=fromRt%>"/>
		<%  if ( mode.equals("initial") )
			{ %>     
				<P class="defComments"><%=MC.M_PlsEtrEsign_Del%><%--Please enter e-Signature to proceed with deletion.*****--%></P>
				<jsp:include page="submitBar.jsp" flush="true"> 
						<jsp:param name="displayESign" value="Y"/>
						<jsp:param name="formID" value="percostdelfrm"/>
						<jsp:param name="showDiscard" value="N"/>
				</jsp:include>
		
				<Input type="hidden" name="mode" value="final" >
				<Input type="hidden" name="lineitemId" value="<%=lineitemId%>" >
					<input type="hidden" name="bgtcalId" value="<%=bgtcalId%>">
					 	<Input type="hidden" name="from" value="<%=from%>" >
					<input type="hidden" name="applyInFuture" value="<%=applyInFuture%>"> 

		<%  
			}
		else
		{
			
				String eSign = request.getParameter("eSign");	
				String oldESign = (String) tSession.getValue("eSign");
				if(!oldESign.equals(eSign)) 
				{
		%>
	 			  <jsp:include page="incorrectesign.jsp" flush="true"/>	
		<%
				} 
				else 
				{
			
					creator=(String) tSession.getAttribute("userId");
			
					ipAdd=(String)tSession.getAttribute("ipAdd");					
			
					lineitemId=  request.getParameter("lineitemId");
			
					applyInFuture = request.getParameter("applyInFuture");
			
					//Modified for INF-18183 ::: Ankit	
					lineitemB.deleteLineitemPerCost(EJBUtil.stringToNum(lineitemId),EJBUtil.stringToNum(applyInFuture),EJBUtil.stringToNum(creator),ipAdd, AuditUtils.createArgs(session,"",LC.L_Budget));
		
					if (ret==-2) 
					{%>	

						<br><br><br><br><br><br>
	
						<table width=100%>
						<tr>
						<td align=center><p class = "successfulmsg"><%=MC.M_Data_NotDelSucc%><%--Data not deleted successfully.*****--%> </p>
					<%} else 
					{			tSession.setAttribute("lineItemDeleted", "Y");  
					 %>
						<br><br><br><br><br><br>
						<P class="successfulmsg" align="center"><%=MC.M_Data_DelSucc%><%--Data deleted successfully.*****--%> </P>
												
						<%if(from.equals("perCost")){%>				
							<META HTTP-EQUIV=Refresh CONTENT="1;
							URL=editPersonnelCost.jsp?mode=M&bgtcalId=<%=bgtcalId%>&refresh=true&pageMode=delete&fromRt=<%=fromRt%>">	<%}else if(from.equals("repLineitem")){%>
							<META HTTP-EQUIV=Refresh CONTENT="1;
							URL=editRepeatLineItems.jsp?mode=M&bgtcalId=<%=bgtcalId%>&refresh=true&pageMode=delete&fromRt=<%=fromRt%>">
								<%}%>
						<script LANGUAGE="JavaScript">
							window.opener.parent.location.reload();
						</script>
						
					<%}%>
		
					</p>
			 			</td>
						</tr>
						<tr height=20></tr>
						<tr>
						<td align=center>
						</td>		
						</tr>		
						</table>				
						<br><br><br><br><br> <p class = "successfulmsg" align = center>  </p>
				<% 



					
				} //esign 


	
		} //mode 	
 		%>
		</FORM>
		<%
  } //session
 else
	{ %> 
	 	<jsp:include page="timeout.html" flush="true"/> 
 <% } //end session%>	
	 
</DIV>
<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</body>
</HTML>


