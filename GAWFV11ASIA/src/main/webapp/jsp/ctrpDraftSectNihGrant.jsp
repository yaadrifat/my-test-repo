<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.velos.eres.web.user.ConfigFacade,com.velos.eres.web.user.ConfigObject, com.velos.eres.web.user.ConfigDetailsObject,com.velos.eres.business.common.CommonDAO"%>
<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC,com.velos.eres.business.studyNIHGrant.impl.StudyNIHGrantBean" %>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.StringUtil,com.velos.eres.web.grpRights.GrpRightsJB" %>
<jsp:useBean id="studyNIHGrantB"  scope="request" class="com.velos.eres.web.studyNIHGrant.StudyNIHGrantJB"/>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<%
int studyId = StringUtil.stringToNum(request.getParameter("studyId"));
String pageMode = (request.getParameter("pageMode")==null)? "":(request.getParameter("pageMode"));
//AK:Added for enhancement CTRP-20527-1
String dFundMech = "";
Integer noOfNIhRecords=0;
String dInstCode = "";
String dProgramCode = "";
CodeDao cdNIHFundMech = new CodeDao();
CodeDao cdNIHInstCode = new CodeDao();
CodeDao cdProgramCode = new CodeDao();
//AK:Added for enhancement CTRP-20527-1

cdNIHFundMech.getCodeValues("NIHFundMech"); 
dFundMech=cdNIHFundMech.toPullDown("fundMech",0,true);		

cdNIHInstCode.getCodeValues("NIHInstCode"); 
dInstCode=cdNIHInstCode.toPullDown("instCode",0,true);
				
//Fixed Bug#7916 : Raviesh
cdProgramCode.getCodeValues("ProgramCode","NCI");
dProgramCode=cdProgramCode.toPullDown("programCode",0,true);

%>
<script>
$j(document).ready(function (){ //Added for enhancement CTRP-20579-6
	if('<%=pageMode%>'=='R'){
	$j("td","#nihGrantTable").children("select,input").bind("focus", function(){
	       $(this).blur();  
	          return;
	     
	      });
	}
});

delStr= new Array();
var delCount=0;

//AK:Added for enhancement CTRP-20527-1
//Modified for bug#7969:Akshi
function showNIHGrantRecords(){
	var formobj = document.studyScreenForm;
	if (!formobj) return;
	var checkedFlag=document.getElementById("nihGrantChk").checked;
	if(checkedFlag==true){
		document.getElementById("nihGrantTable").style.display="block";
		$j("#NIH").show();
		formobj.nihGrantChkValue.value="1";	
		makeMeFixedHeader('nihGrantScrollTableBody');
	}else{		
		document.getElementById("nihGrantTable").style.display="none";
		$j("#NIH").hide();
		formobj.nihGrantChkValue.value="0";	
		}
}

//AK:Added for enhancement CTRP-20527-1
function validateNumber(serialNum){ //Ak:Fixed BUG#8142
	var seialFormat = /^\d{1,10}\s*$/;
	if(!(seialFormat.test(serialNum))){
		 alert("<%=MC.M_EtrNum_SerialNum%>");
		 return false;
	}
	return true;
}//Ends here number validation

//Delete function for deleteing the NIH records

function fnDeleteNIHGrantRows(currentRow){
	var formobj = document.studyScreenForm;
	if (!formobj) return;

	if(confirm("<%=MC.M_WantDel_NIHGrant%>")){
	    var nihGrantId=0;
	  	var numCompRowExists = 0;
	  	
		numCompRowExists = formobj.totalExtgRows.value;		
		var i=currentRow.parentNode.parentNode.rowIndex;
		
		nihFieldID = currentRow.parentNode.parentNode.cells[4].getElementsByTagName("input")[0];	  
		nihGrantId=nihFieldID.value;
		document.getElementById('nihGrantTable').deleteRow(i);
		 numCompRowExists--;
	     if(nihGrantId>0){
	        delStr[delCount]=nihGrantId;  
	        delCount++;     
	     }    
		    formobj.totalExtgRows.value = numCompRowExists;
		    formobj.delStrs.value = delStr;  
		if (i==1)
			makeMeFixedHeader('nihGrantScrollTableBody');

	}
}//Ends here deletion function
   
//AK:Added the add functionality   
function fnAddNIHGrantRows(){
	var formobj = document.studyScreenForm;
	if (!formobj) return;
	
	var numCompRowExists = 0;
	numCompRowExists = formobj.totalExtgRows.value;
	var myRow = document.createElement('tr');
    myRow.className="browserOddRow";

    var myCell = document.createElement('td');
    myCell.innerHTML = '<tr id="myiRow'+numCompRowExists+'" ><td class="tdDefault">'+"<%=dFundMech%>"+'</td>';
    myRow.appendChild(myCell);

    myCell = document.createElement('td');
	myCell.innerHTML = '<td class="tdDefault">'
	+"<%=dInstCode%>"
	+'</td>';
	myRow.appendChild(myCell);

    myCell = document.createElement('td');
	myCell.innerHTML = '<td class="tdDefault">'
		+'<Input type=text name="serialNum" id ="serialNum" value="" maxlength="10">'
		+'</td>';
	myRow.appendChild(myCell);

    myCell = document.createElement('td');
	myCell.innerHTML = "<td class='tdDefault'><%=dProgramCode%></td>";
	myRow.appendChild(myCell);

    myCell = document.createElement('td');
    myCell.innerHTML = '<A id="del" name="del" onClick="fnDeleteNIHGrantRows(this);">'
		+'<img src="../images/delete.png" alt="'+Delete_Row+'" title="<%=LC.L_Delete%>" border="0"/></A>'
		+'<input type="hidden" id="idsNIHgrant" name="idsNIHgrant" value="0">';
	myRow.appendChild(myCell);

	document.getElementById('nihGrantScrollTableBody').appendChild(myRow);
	if (numCompRowExists == 0){
		makeMeFixedHeader('nihGrantScrollTableBody');
	}
    numCompRowExists++;
	formobj.totalExtgRows.value = numCompRowExists;
}//Ends here add functionality

function validateNIHGrantGrid(formobj){
	 var totalNIHGrants=0;
	 totalNIHGrants=formobj.totalExtgRows.value;
	      if (totalNIHGrants==1){	      	 
			  fund_mech = formobj.fundMech.value;
		      inst_code = formobj.instCode.value;
		      program_code = formobj.programCode.value;
		      serial_num = formobj.serialNum.value;				  
			  if(fund_mech==''){
				  	alert("<%=MC.M_Selc_FundMechsm%>");/*alert("Please select Funding Mechanism value");****/
				  	formobj.fundMech.focus();
				 	 return false;
			    }if(inst_code==''){
				   	alert("<%=MC.M_Selc_InstCode%>"); /*alert("Please select Institute Code value");****/
				  	formobj.instCode.focus();
				    return false;
			    }if(serial_num==''){
				   	alert("<%=MC.M_Etr_SerialNum%>");/*alert("Please enter Serial Number value");****/
				   	formobj.serialNum.focus();
				  	 return false;
				}if(serial_num!=''){ //Ak:Fixed BUG#8142
				   if(!(validateNumber(serial_num))){ 
					   return false;
					}
				}if(program_code==''){
					alert("<%=MC.M_Selc_NCIProgCode%>");/***alert("Please NCI Division/Program Code value");***/ 
					formobj.programCode.focus();                  //Modified for Bug#8038 : Raviesh
					return false;
				}

	      }else if (totalNIHGrants>1){
		      
		      for(i=0;i<totalNIHGrants;i++) {
			      fund_mech = formobj.fundMech[i].value;
			      inst_code = formobj.instCode[i].value;
			      program_code = formobj.programCode[i].value;
			      serial_num = formobj.serialNum[i].value;			     		      
			      if(fund_mech==''){
			    	 	 alert("<%=MC.M_Selc_FundMechsm%>");
				     	 formobj.fundMech[i].focus(); 
				     	 return false;
				   }if(inst_code==''){
					 	 alert("<%=MC.M_Selc_InstCode%>");
					  	 formobj.instCode[i].focus();
					  	 return false;
				   }if(serial_num==''){				 
					  	 alert("<%=MC.M_Etr_SerialNum%>");
					  	 formobj.serialNum[i].focus();
					 	 return false;
				   }if(serial_num!=''){ //Ak:Fixed BUG#8142
					   if(!(validateNumber(serial_num))){
						   return false;
						}
				   }if(program_code==''){ 
					   	 alert("<%=MC.M_Selc_NCIProgCode%>");
					  	 formobj.programCode[i].focus();
					     return false;
				   }	
				  
	
		   	  }
	    }
  return true;
}


</script>

<%
							
 							//AK:Added for enhancement CTRP-20527-1
 							int noOfRows=0;
							String nihChkValue="0";
							StudyNIHGrantDao studyNIHGrantDao = studyNIHGrantB.getStudyNIHGrantRecords(studyId);
							ArrayList pk_NIhGrants = studyNIHGrantDao.getPkStudyNIhGrants();
							ArrayList fund_mechsms = studyNIHGrantDao.getFundMechsms();
							ArrayList inst_codes = studyNIHGrantDao.getInstitudeCodes();
							ArrayList program_codes = studyNIHGrantDao.getProgramCodes();
							ArrayList serial_numbers = studyNIHGrantDao.getNihGrantSerials();
							Integer tempFundMechsm=0;
							Integer tempInstCode=0;
							Integer tempProgramCode=0;
							String tempSerialNumber="";
							noOfRows=pk_NIhGrants.size();
							noOfRows=(pk_NIhGrants.size()>0)?noOfRows:1;
							nihChkValue=(pk_NIhGrants.size()>0)?"1":"0";
							%><input type = "hidden" name="totalExtgRows" value=<%=noOfRows%>>
							<input type = "hidden" name="oldExtgRows" value=<%=noOfRows%>>
							<input type = "hidden" name="nihGrantChkValue" value=<%=nihChkValue%>>
							<!-- Ak:Added for enhancement CCTRP-20579-6 -->
	<%if(!(pk_NIhGrants.size()>0) && (pageMode.equals("R"))){%>
		<table>
			<tr>
			<td align="left">
				<p class = "defComments"><%=MC.M_NoRecordsFound%></p>
			</td>
			<td align="left">
				<span id="ctrpDraftJB.trialNIHGrants_error" class="errorMessage" style="display:none;"></span>
			</td>
			<td width="10%">&nbsp;</td>
			</tr>
		</table>
	<%}else{ %>	
			<%if(pageMode.equals("E")) {%>
				<input type="checkbox" id="nihGrantChk" name="nihGrantChk" <%if(pk_NIhGrants.size()>0){ %>checked style="display: none" <%}else{%>onClick="showNIHGrantRecords()" <%}%>>     
				    <%=LC.L_NIH_Grant_Information%><%}%> 
		 		<br/>
		 		<%if(pk_NIhGrants.size()==0){ %>
		 			<div id="NIH" class="TableContainer" style="width: 635px;height: 120px; border-collapse:separate;display:none" border="1">
		 		<%}else{%>
		 			<div id="NIH" class="TableContainer" style="width: 635px;height: 120px; border-collapse:separate;" border="1">
		 		<%}%>
		 			   <TABLE class="scrollTable" id="nihGrantTable" CELLSPACING="1" border="1" <%if(!(pk_NIhGrants.size()>0)){%>Style ="display:none" <%} %>>
		 			   		<thead class="fixedHeader headerFormat">
        					<TR id="myiRow0">							
							 	 <th align="center" height="35px" nowrap="nowrap"><%=LC.L_Funding_Mechsm%><FONT class="Mandatory">* </FONT></th>
							     <th align="center" nowrap="nowrap"><%=LC.L_Institute_Code%><FONT class="Mandatory">*</FONT></th>
							     <th align="center" nowrap="nowrap"><%=LC.L_Serial_Number%><FONT class="Mandatory">*</FONT></th>
							     <th align="center" nowrap="nowrap"><%=LC.L_NCI_Div_Prom_Code%><FONT class="Mandatory">*</FONT></th>
							     <%if(pageMode.equals("E")) {%>
							     <th>
					 				<A id="addMore" name="addMore" onclick="fnAddNIHGrantRows();">
										<img class="headerImage" src="../images/add.png" alt="<%=LC.L_Add%>" title="<%=LC.L_Add%>" border="0">
									</A>
								 </th>
							     <%}%>
							</tr>
							</thead>
							<tbody id="nihGrantScrollTableBody" name="nihGrantScrollTableBody" class="scrollContent bodyFormat" style="height:80px;">
							<% 
							if(pk_NIhGrants.size()>0){
							for(int k=0;k<pk_NIhGrants.size();k++){
								tempFundMechsm=(Integer)fund_mechsms.get(k);
								tempInstCode=(Integer)inst_codes.get(k);
								tempProgramCode=(Integer)program_codes.get(k);
								tempSerialNumber=(String)serial_numbers.get(k);
								tempSerialNumber=(tempSerialNumber==null)?"":tempSerialNumber;
								dFundMech=cdNIHFundMech.toPullDown("fundMech",tempFundMechsm,true);
								dInstCode=cdNIHInstCode.toPullDown("instCode",tempInstCode,true);
								dProgramCode=cdProgramCode.toPullDown("programCode",tempProgramCode,true);
								%>
							<tr id="myiRow<%=k+1%>" class="browserOddRow">
							<!-- Modified for Bug#7969 : Akshi -->
	                           <td class=tdDefault><%=dFundMech%></td>
	                           <td class=tdDefault><%=dInstCode%></td>
	                           <td class=tdDefault><Input type=text name="serialNum" id ="serialNum" value="<%=tempSerialNumber%>" maxlength="10"></td>
	                           <td class=tdDefault><%=dProgramCode%></td>
	                           <%if(pageMode.equals("E")) {%>
	                           <td class=tdDefault>
									<A id="del" name="del" onClick="fnDeleteNIHGrantRows(this);">
										<img src="../images/delete.png" alt="<%=LC.L_Delete%>" title="<%=LC.L_Delete%>"  border="0">
									</A>
		                           <input type="hidden" name="idsNIHgrant" id="idsNIHgrant" value="<%=pk_NIhGrants.get(k).toString()%>" >
		                       </td>
		                        <%}%>
                           </tr>
	                       <%} %>	                           
						
						   <%}else if(pageMode.equals("E")){ %>	   
		                    <tr id="myiRow1" class="browserOddRow">		              
		                           <td class=tdDefault><%=dFundMech%></td>
		                           <td class=tdDefault><%=dInstCode%></td>
		                           <td class=tdDefault><Input type=text name="serialNum" value="" maxlength="10"></td>
		                           <td class=tdDefault><%=dProgramCode%></td>
		                           <td class=tdDefault>
										<A id="del" name="del" onClick="fnDeleteNIHGrantRows(this);">
											<img src="../images/delete.png" alt="<%=LC.L_Delete%>" title="<%=LC.L_Delete%>" border="0">
										</A>
			                       		<input type="hidden" name="idsNIHgrant" id="idsNIHgrant" value="0" >
			                       	</td>
	                        </tr>
	                        <%}%>
	                       </tbody>
						</TABLE>
					</div>
					<br/>
	 <%}%>
<script>
if (document.getElementById('nihGrantScrollTableBody')){
	if (navigator.userAgent.indexOf("MSIE") == -1){ 
		$E.onContentReady(makeMeFixedHeader('nihGrantScrollTableBody'));
	}
}
</script>