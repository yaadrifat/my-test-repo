<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=MC.M_BgtUsr_AccRgts%><%-- Budget >> User Access Rights*****--%></title>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.user.UserJB,com.velos.eres.service.util.*" %>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id ="budgetUsersB" scope="request" class="com.velos.esch.web.budgetUsers.BudgetUsersJB"/>
<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/><b></b>
</head>


<SCRIPT Language="JavaScript">

function changeRights(obj,row, frmname)	{
	  selrow = row ;
	  totrows = frmname.totalrows.value;
	  if (totrows > 1)
		rights =frmname.rights[selrow].value;
	  else
		rights =frmname.rights.value;

	  objName = obj.name;
	  if (obj.checked)  {
       	if (objName == "newr")	{
			rights = parseInt(rights) + 1;
			if (!frmname.view[selrow].checked)	{ 
				frmname.view[selrow].checked = true;
				rights = parseInt(rights) + 4;
			}
		}
       	if (objName == "edit")	{
			rights = parseInt(rights) + 2;
			if (!frmname.view[selrow].checked){ 
				frmname.view[selrow].checked = true;
				rights = parseInt(rights) + 4;
			}
		}
       	if (objName == "view") rights = parseInt(rights) + 4;

		if (totrows > 1 )
			frmname.rights[selrow].value = rights;
		else
			frmname.rights.value = rights;
	  }else	{
	       	if (objName == "newr") rights = parseInt(rights) - 1;
    	   	if (objName == "edit") rights = parseInt(rights) - 2;
	       	if (objName == "view"){	
			if (frmname.newr[selrow].checked) 
			{
				alert("<%=MC.M_UsrRgt_CntRevoke%>");/*alert("The user has right to New or Edit. You can not revoke right to View");*****/
				frmname.view[selrow].checked = true;		
			}
		    else if (frmname.edit[selrow].checked) 
			{
		    	alert("<%=MC.M_UsrRgt_CntRevoke%>");/*alert("The user has right to New or Edit. You can not revoke right to View");*****/
				frmname.view[selrow].checked = true;		
			}
			else
			{		
			  rights = parseInt(rights) - 4;
			}
		}
	
		if (totrows > 1 )
			frmname.rights[selrow].value = rights;
		else
			frmname.rights.value = rights;
    	 }
}
	
function validate(formobj){
	 if (!(validate_col('e-Signature',formobj.eSign))) return false

	<%--  if(isNaN(formobj.eSign.value) == true) {
		 alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
   } --%>
}	

</SCRIPT>


<% String src="";
src= request.getParameter("srcmenu");
String budgetTemplate=request.getParameter("budgetTemplate");
String 	bottomdivClass="tabDefBotN";
if ("S".equals(request.getParameter("budgetTemplate"))) { %>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   
<% } else { 
bottomdivClass="popDefault";
%>
<jsp:include page="include.jsp" flush="true"/> 
<% }  %>

<body>
<br>
<DIV class="tabDefTopN" id="div1">
</DIV>
	<Form name="budgetUserRights" id="bdgtuserrights" method="post" action="updatebudgetuserrights.jsp" onSubmit="if (validate(document.budgetUserRights)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}"> 
<%
HttpSession tSession = request.getSession(true); 
if(sessionmaint.isValidSession(tSession))  {

	String budgetId=request.getParameter("budgetId");
	String bgtUsrId=request.getParameter("bgtUsrId");
	String userId=request.getParameter("userId");
	String selectedTab = request.getParameter("selectedTab");
	String budgetType =  request.getParameter("budgetType");
	String mode =  request.getParameter("mode");	
	int counter = 0;
    int pageRight = 0;	

	GrpRightsJB bgtRights = (GrpRightsJB) tSession.getValue("BRights");		
    pageRight = Integer.parseInt(bgtRights.getFtrRightsByValue("BGTACCESS"));

	if (pageRight > 0 )
	{
	budgetUsersB.setBgtUsersId(EJBUtil.stringToNum(bgtUsrId));
	budgetUsersB.getBudgetUsersDetails();
	String bgtUserRights = budgetUsersB.getBgtUsersRights();	
%>
   <Input type=hidden name="budgetId" value="<%=budgetId%>">
   <Input type=hidden name="srcmenu" value="<%=src%>">
   <Input type=hidden name="bgtUsrId" value="<%=bgtUsrId%>">
   <Input type=hidden name="selectedTab" value="<%=selectedTab%>">
   <Input type=hidden name="budgetType" value="<%=budgetType%>">
   <Input type=hidden name="mode" value="<%=mode%>">
   <Input type=hidden name="budgetTemplate" value="<%=budgetTemplate%>">
   
<%
	UserJB userB = new UserJB();
	userB.setUserId(EJBUtil.stringToNum(userId));
	userB.getUserDetails();
%>   
               
<DIV class="<%=bottomdivClass%>" id="div2">
 	 <BR>
 	 <%Object[] arguments = {userB.getUserFirstName(),userB.getUserLastName()};	 
	 %>
   <P class="defComments" align="center"><b><%=VelosResourceBundle.getMessageString("M_DfnAcesRgt_For",arguments)%><%--Define Access Rights for <%=userB.getUserFirstName()%> <%=userB.getUserLastName()%>*****--%></b></P>   
<%
	ctrl.getControlValues("bgt_rights");
	int rows = ctrl.getCRows();

	ArrayList feature =  ctrl.getCValue();
	ArrayList ftrDesc = ctrl.getCDesc();
	ArrayList ftrSeq = ctrl.getCSeq();
%>
    <Input type="hidden" name="totalrows" value=<%=rows%> >
    <TABLE width="80%" >
      <TH>&nbsp;</TH>
      <TH><%=LC.L_New%><%-- New*****--%></TH>
      <TH><%=LC.L_Edit%><%-- Edit*****--%></TH>
      <TH><%=LC.L_View%><%-- View*****--%></TH>
<%
	
	for(int count=0;count<rows;count++){ 
	    String ftr = (String) feature.get(count);
    	String desc = (String) ftrDesc.get(count);
    	int seq = ((Integer) ftrSeq.get(count)).intValue();
		//get rights for the specific budget module		
		String rights = String.valueOf(bgtUserRights.charAt(seq-1));
	 %>
	   <TR>
       <Input type="hidden" name=rights value=<%=rights%> >
        <TD>
		<%= desc%>
		</TD>
		<%  
		 if (rights.compareTo("7") == 0){//all rights 
		 %> 
        <TD align="center"> 
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>, document.budgetUserRights)" CHECKED>
        </TD>
        <TD align="center"> 
          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>, document.budgetUserRights)" CHECKED>
        </TD>
        <TD align="center"> 
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>, document.budgetUserRights)" CHECKED>
        </TD>
        <%}else if (rights.compareTo("1") == 0){//new right %> 
        <TD align="center"> 
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>, document.budgetUserRights)" CHECKED>
        </TD>
        <TD align="center"> 
          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>, document.budgetUserRights)" >
        </TD>
        <TD align="center"> 
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>, document.budgetUserRights)">
        </TD>
        <%}else if(rights.compareTo("3") == 0){//new & edit right %> 
        <TD align="center"> 
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>, document.budgetUserRights)" CHECKED>
        </TD>
        <TD align="center"> 
          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>, document.budgetUserRights)" CHECKED>
        </TD>
        <TD align="center"> 
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>, document.budgetUserRights)">
        </TD>
        <%}else if (rights.compareTo("2") == 0){//edit right %> 
        <TD align="center"> 
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>, document.budgetUserRights)">
        </TD>
        <TD align="center"> 
          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>, document.budgetUserRights)" CHECKED>
        </TD>
        <TD align="center"> 
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>, document.budgetUserRights)">
        </TD>
        <%}else if (rights.compareTo("4") == 0){//view right %> 
        <TD align="center"> 
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>, document.budgetUserRights)">
        </TD>
        <TD align="center"> 
          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>, document.budgetUserRights)">
        </TD>
        <TD align="center"> 
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>, document.budgetUserRights)" CHECKED>
        </TD>
        <%}else if (rights.compareTo("5") == 0){//new & view right%> 
        <TD align="center"> 
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>, document.budgetUserRights)" CHECKED>
        </TD>
        <TD align="center"> 
          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>, document.budgetUserRights)">
        </TD>
        <TD align="center"> 
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>, document.budgetUserRights)" CHECKED>
        </TD>
        <%}else if (rights.compareTo("6") == 0){//edit & view right%> 
        <TD align="center"> 
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>, document.budgetUserRights)" >
        </TD>
        <TD align="center"> 
          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>, document.budgetUserRights)" CHECKED>
        </TD>
        <TD align="center"> 
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>, document.budgetUserRights)" CHECKED>
        </TD>
        <%}else{ %>
        <TD align="center"> 
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>, document.budgetUserRights)">
        </TD>
        <TD align="center"> 
          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>, document.budgetUserRights)">
        </TD>
        <TD align="center"> 
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>, document.budgetUserRights)">
        </TD>
        <%}%>
      </TR>
      	  
	  	  	   
	<%
	}		 
%>
  	 <tr height=15><td clospan=4></td></tr>
	<%	if (pageRight!=4) { %>	 
	 <jsp:include page="submitBar.jsp" flush="true"> 
				<jsp:param name="displayESign" value="Y"/>
				<jsp:param name="formID" value="bdgtuserrights"/>
				<jsp:param name="showDiscard" value="N"/>
		   </jsp:include>
	<% }%>	  
	</table>

   <%
	} //end of if body for page right

	else
	{
	%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%
	} //end of else body for page right   
   	
   }
   else {            //end of if body for session
   
   %>
   <jsp:include page="timeout.html" flush="true"/>
   <%
   }
%>

</Form>
<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</div>

<div class="mainMenu" id="emenu"> 
  <jsp:include page="getmenu.jsp" flush="true"/>
</div>

</body>
</html>


