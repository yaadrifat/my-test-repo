<%@taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC" %>
<%@page import="com.velos.eres.service.util.VelosResourceBundle"%>

<script >
var isNCIdesignated = '<s:property escape="false" value="ctrpDraftJB.isNCIDesignated" />';
$j(document).ready(function(){

	 checkisNCIDesignated();

	$j('#ctrpDraftIndustrialForm_ctrpDraftJB_isNCIDesignated1').change(function() {
		
	    if($j('#ctrpDraftIndustrialForm_ctrpDraftJB_isNCIDesignated1').is(':checked')){
		$j('#siteProdCode0').show();			
		$j('#siteProdCode1').show();	
		$j('#siteProdCode2').show();	
		$j('#siteTrgtAccrual0').show();			
		$j('#siteTrgtAccrual1').show();	
		$j('#siteTrgtAccrual2').show();	
	    }		
	});
	$j('#ctrpDraftIndustrialForm_ctrpDraftJB_isNCIDesignated0').change(function() {
	    if($j('#ctrpDraftIndustrialForm_ctrpDraftJB_isNCIDesignated0').is(':checked')){
		$j('#siteProdCode0').hide();			
		$j('#siteProdCode1').hide();	
		$j('#siteProdCode2').hide();	
		$j('#siteTrgtAccrual0').hide();			
		$j('#siteTrgtAccrual1').hide();	
		$j('#siteTrgtAccrual2').hide();
		$j('#ctrpDraftJB\\.subOrgProgCode').val('');	
	    }
	});
	

});

function checkisNCIDesignated()
{ 
	if(isNCIdesignated=='0'){
		$j('#ctrpDraftIndustrialForm_ctrpDraftJB_isNCIDesignated0').attr('checked',true);
		$j('#siteProdCode0').hide();			
		$j('#siteProdCode1').hide();	
		$j('#siteProdCode2').hide();	
		$j('#siteTrgtAccrual0').hide();			
		$j('#siteTrgtAccrual1').hide();	
		$j('#siteTrgtAccrual2').hide();
		$j('#ctrpDraftJB\\.subOrgProgCode').val('');
	}else{
		$j('#ctrpDraftIndustrialForm_ctrpDraftJB_isNCIDesignated1').attr('checked',true);
		$j('#siteProdCode0').show();			
		$j('#siteProdCode1').show();	
		$j('#siteProdCode2').show();	
		$j('#siteTrgtAccrual0').show();			
		$j('#siteTrgtAccrual1').show();	
		$j('#siteTrgtAccrual2').show();
	}
}
//YPS 21 Dec 2012 for PO Person and PO Organization lookup:CTRP-22496
function lookupSitePI(Pkval,DisVal){	    
	windowName=window.open("getlookup.jsp?viewId=&viewName=Principal Investigator&form=ctrpDraftIndustrialForm&dfilter=PoPerson&keyword="+Pkval+"|PERSON_ID~"+DisVal+"|PERSON_ID","Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=550 top=100,left=100");
	windowName.focus();
}
function lookupPOOrganization(Pkval,DisVal){	    
	windowName=window.open("getlookup.jsp?viewId=&viewName=PO Organization&form=ctrpDraftIndustrialForm&dfilter=PoOrganization&keyword="+Pkval+"|ORGANIZATION_PO_ID~"+DisVal+"|ORGANIZATION_PO_ID","Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=550 top=100,left=100");
	windowName.focus();
}
</script>
<table>
	<tr>
		<td width="100%" valign="top">
			<table>
				 <tr>
				 	<td width="50%" valign="top">
						<div id="leadOrgDiv">
						<table width="50%" cellpadding="0" cellspacing="0" border="0">
						<tr>
						<td align="right"><a href="javascript:lookupPOOrganization('ctrpDraftJB.leadOrgCtrpId','ctrpDraftJB.leadOrgCtrpId');" ><%=LC.L_Select %></a></td></tr>
						<tr>
						<td align="right" width="30%" height="22px">
							<%=VelosResourceBundle.getLabelString("CTRP_DraftLeadOrgSpecifics",LC.L_Id_Upper) %>&nbsp;
						</td>
						<td width="1%">&nbsp;</td>
						<td align="left">
						<s:textfield name="ctrpDraftJB.leadOrgCtrpId" id="ctrpDraftJB.leadOrgCtrpId" size="50"></s:textfield></td>
						<td align="left" >&nbsp;</td>
						<td >&nbsp;</td>
						<td align="left" >&nbsp;</td>
						</tr>
						<tr>
						<td align="right"  height="30px"><span id="ctrpDraftJB.leadOrgCtrpId_error" class="errorMessage" style="display:none;"></span>&nbsp;</td>
						<td width="1%"><FONT style="visibility:visible" class="Mandatory">* </FONT></td>
						<td align="left" ><p class="defComments"><FONT id="lead_org_ctrp_id_mandatory"><%=MC.CTRP_LeadOrgEnterOrSelect%></FONT></p></td>
						</tr>
						<tr>
						<td align="right">
						<a href="javascript:openSelectOrg(constleadOrg);" ><%=LC.L_Select %></a>
						</td>
						</tr>
						<tr>
						<td align="right"  height="22px">
							<%=LC.L_Name%>&nbsp;
							<span id="ctrpDraftJB.leadOrgName_error" class="errorMessage" style="display:none;"></span>
						</td>
						<td >&nbsp;</td>
						<td align="left"  >
						<s:hidden name="ctrpDraftJB.leadOrgFkSite" id="ctrpDraftJB.leadOrgFkSite"/>
						<s:textfield name="ctrpDraftJB.leadOrgName" id="ctrpDraftJB.leadOrgName" readonly="true" size="50" cssClass="readonly-input"></s:textfield>&nbsp;</td>
						</tr>
						<tr>
						<s:hidden name="ctrpDraftJB.leadOrgFkAdd" id="ctrpDraftJB.leadOrgFkAdd"/>
						<td align="right"  height="22px"><%=LC.L_Street_Address%>&nbsp;
						<span id="ctrpDraftJB.leadOrgStreet_error" class="errorMessage" style="display:none;"></span>
						</td>
						<td >&nbsp;</td>
						<td align="left" >
						<s:textfield name="ctrpDraftJB.leadOrgStreet" id="ctrpDraftJB.leadOrgStreet" readonly="true" size="50" cssClass="readonly-input"></s:textfield></td>
						</tr>
						<tr>
						<td align="right"  height="22px">
							<%=LC.L_City%>&nbsp;
							<span id="ctrpDraftJB.leadOrgCity_error" class="errorMessage" style="display:none;"></span>
						</td>
						<td >&nbsp;</td>
						<td align="left" >
						<s:textfield name="ctrpDraftJB.leadOrgCity" id="ctrpDraftJB.leadOrgCity" readonly="true" size="50" cssClass="readonly-input"></s:textfield></td>
						</tr>
						<tr>
						<td align="right"  height="22px">
							<%=LC.CTRP_DraftStateProvince%>&nbsp;
							<span id="ctrpDraftJB.leadOrgState_error" class="errorMessage" style="display:none;"></span>
						</td>
						<td >&nbsp;</td>
						<td align="left" >
						<s:textfield name="ctrpDraftJB.leadOrgState" id="ctrpDraftJB.leadOrgState" readonly="true" size="50" cssClass="readonly-input"></s:textfield></td>
						</tr>
						<tr>
						<td align="right"  height="22px"><%=LC.L_ZipOrPostal_Code%>&nbsp;
						<span id="ctrpDraftJB.leadOrgZip_error" class="errorMessage" style="display:none;"></span>
						</td>
						<td >&nbsp;</td>
						<td align="left" >
						<s:textfield name="ctrpDraftJB.leadOrgZip" id="ctrpDraftJB.leadOrgZip" readonly="true" size="50" cssClass="readonly-input"></s:textfield></td>
						</tr>
						<tr>
						<td align="right"  height="22px"><%=LC.L_Country%>&nbsp;
						<span id="ctrpDraftJB.leadOrgCountry_error" class="errorMessage" style="display:none;"></span>
						</td>
						<td >&nbsp;</td>
						<td align="left" >
						<s:textfield name="ctrpDraftJB.leadOrgCountry" id="ctrpDraftJB.leadOrgCountry" readonly="true" size="50" cssClass="readonly-input"></s:textfield></td>
						</tr>
						<tr>
						<td align="right" ><%=LC.L_Email_Addr%>&nbsp;
						<span id="ctrpDraftJB.leadOrgEmail_error" class="errorMessage" style="display:none;"></span>
						</td>
						<td >&nbsp;</td>
						<td align="left" >
						<s:textfield name="ctrpDraftJB.leadOrgEmail" id="ctrpDraftJB.leadOrgEmail" readonly="true" size="50" cssClass="readonly-input"></s:textfield></td>
						</tr>
						<tr>
						<td align="right" ><%=LC.L_Phone%>&nbsp;</td>
						<td >&nbsp;</td>
						<td align="left" ><s:textfield name="ctrpDraftJB.leadOrgPhone" id="ctrpDraftJB.leadOrgPhone" readonly="true" size="50" cssClass="readonly-input"></s:textfield></td>
						</tr>
						<tr>
						<td align="right" ><%=LC.L_TTY %>&nbsp;</td>
						<td >&nbsp;</td>
						<td align="left" ><s:textfield name="ctrpDraftJB.leadOrgTty" id="ctrpDraftJB.leadOrgTty" size="50"></s:textfield></td>
						</tr>
						<tr>
						<td align="right" ><%=LC.L_Fax%>&nbsp;</td>
						<td >&nbsp;</td>
						<td align="left" ><s:textfield name="ctrpDraftJB.leadOrgFax" id="ctrpDraftJB.leadOrgFax" size="50"></s:textfield></td>
						</tr>
						<tr>
						<td align="right" ><%=LC.L_Url.toUpperCase()%>&nbsp;</td>
						<td >&nbsp;</td>
						<td align="left" ><s:textfield name="ctrpDraftJB.leadOrgUrl" id="ctrpDraftJB.leadOrgUrl" size="50"></s:textfield></td>
						</tr>
						<tr>
						<td align="right" width="15%"><%=VelosResourceBundle.getLabelString("CTRP_DraftLeadOrgSpecifics",LC.L_Type) %>&nbsp;</td>
						<td width="1%">&nbsp;</td>
						<td align="left" ><s:property escape="false" value="ctrpDraftJB.leadOrgTypeMenu" /></td>
						</tr>
						<tr>
						<td align="right"  height="22px">&nbsp;</td>
						<td >&nbsp;</td>
						<td align="right" >&nbsp;</td>
						</tr>
						<tr>
						<td align="right"  height="22px">&nbsp;</td>
						<td >&nbsp;</td>
						<td align="right" >&nbsp;</td>
						</tr>
						</table>
						</div>
					</td>
					<td width="50%" valign="top">
						<div id="submOrgDiv">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
						<tr>
						<td align="right"><a href="javascript:lookupPOOrganization('ctrpDraftJB.subOrgCtrpId','ctrpDraftJB.subOrgCtrpId');" ><%=LC.L_Select %></a></td></tr>
						<tr>
						<td align="right" width="30%" height="22px">
							<%=VelosResourceBundle.getLabelString("CTRP_DraftSubmitOrgSpecifics",LC.L_Id_Upper) %>&nbsp;
						</td>
						<td width="1%">&nbsp;</td>
						<td align="left" >
						<s:textfield name="ctrpDraftJB.subOrgCtrpId" id="ctrpDraftJB.subOrgCtrpId"  size="50"></s:textfield></td>
						<td width="1%" align="left" >&nbsp;</td>
						</tr>
						<tr>
						<td align="right"  height="30px"><span id="ctrpDraftJB.subOrgCtrpId_error" class="errorMessage" style="display:none;"></span>&nbsp;</td>
						<td width="1%"><FONT style="visibility:visible" class="Mandatory">* </FONT></td>
						<td align="left"  >
						<p class="defComments"><FONT><%=MC.CTRP_SubmitOrgEnterOrSelect%></FONT></p></td>
						</tr>
						<tr>
						<td align="right">
						<a href="javascript:openSelectOrg(constSubOrg);" ><%=LC.L_Select %></a>
						</td>
						</tr>
						<tr>
						<td align="right"  height="22px">
							<%=LC.L_Name%>&nbsp;
							<span id="ctrpDraftJB.subOrgName_error" class="errorMessage" style="display:none;"></span>
						</td>
						<td >&nbsp;</td>
						<td align="left" >
						<s:hidden name="ctrpDraftJB.subOrgFkSite" id="ctrpDraftJB.subOrgFkSite"/>
						<s:textfield name="ctrpDraftJB.subOrgName" id="ctrpDraftJB.subOrgName" readonly="true" size="50" cssClass="readonly-input"></s:textfield>&nbsp;</td>
						</tr>
						<tr>
						<s:hidden name="ctrpDraftJB.subOrgFkAdd" id="ctrpDraftJB.subOrgFkAdd"/>
						<td align="right"  height="22px"><%=LC.L_Street_Address%>&nbsp;
						<span id="ctrpDraftJB.subOrgStreet_error" class="errorMessage" style="display:none;"></span>
						</td>
						<td >&nbsp;</td>
						<td align="left" >
						<s:textfield name="ctrpDraftJB.subOrgStreet" id="ctrpDraftJB.subOrgStreet" readonly="true" size="50" cssClass="readonly-input"></s:textfield></td>
						</tr>
						<tr>
						<td align="right"  height="22px"><%=LC.L_City%>&nbsp;
						<span id="ctrpDraftJB.subOrgCity_error" class="errorMessage" style="display:none;"></span>
						</td>
						<td >&nbsp;</td>
						<td align="left" >
						<s:textfield name="ctrpDraftJB.subOrgCity" id="ctrpDraftJB.subOrgCity" readonly="true" size="50" cssClass="readonly-input"></s:textfield></td>
						</tr>
						<tr>
						<td align="right"  height="22px"><%=LC.CTRP_DraftStateProvince%>&nbsp;
						<span id="ctrpDraftJB.subOrgState_error" class="errorMessage" style="display:none;"></span>
						</td>
						<td >&nbsp;</td>
						<td align="left" >
						<s:textfield name="ctrpDraftJB.subOrgState" id="ctrpDraftJB.subOrgState" readonly="true" size="50" cssClass="readonly-input"></s:textfield></td>
						</tr>
						<tr>
						<td align="right"  height="22px"><%=LC.L_ZipOrPostal_Code%>&nbsp;
						<span id="ctrpDraftJB.subOrgZip_error" class="errorMessage" style="display:none;"></span>
						</td>
						<td >&nbsp;</td>
						<td align="left" >
						<s:textfield name="ctrpDraftJB.subOrgZip" id="ctrpDraftJB.subOrgZip" readonly="true" size="50" cssClass="readonly-input"></s:textfield></td>
						</tr>
						<tr>
						<td align="right"  height="22px"><%=LC.L_Country%>&nbsp;
						<span id="ctrpDraftJB.subOrgCountry_error" class="errorMessage" style="display:none;"></span>
						</td>
						<td >&nbsp;</td>
						<td align="left" >
						<s:textfield name="ctrpDraftJB.subOrgCountry" id="ctrpDraftJB.subOrgCountry" readonly="true" size="50" cssClass="readonly-input"></s:textfield></td>
						</tr>
						<tr>
						<td align="right" ><%=LC.L_Email_Addr%>&nbsp;
						<span id="ctrpDraftJB.subOrgEmail_error" class="errorMessage" style="display:none;"></span>
						</td>
						<td >&nbsp;</td>
						<td align="left" >
						<s:textfield name="ctrpDraftJB.subOrgEmail" id="ctrpDraftJB.subOrgEmail" readonly="true" size="50" cssClass="readonly-input"></s:textfield></td>
						</tr>
						<tr>
						<td align="right" ><%=LC.L_Phone%>&nbsp;
						<span id="ctrpDraftJB.leadOrgCountry_error" class="errorMessage" style="display:none;"></span>
						</td>
						<td >&nbsp;</td>
						<td align="left" ><s:textfield name="ctrpDraftJB.subOrgPhone" id="ctrpDraftJB.subOrgPhone" readonly="true" size="50" cssClass="readonly-input"></s:textfield></td>
						</tr>
						<tr>
						<td align="right" ><%=LC.L_TTY %>&nbsp; </td>
						<td >&nbsp;</td>
						<td align="left" ><s:textfield name="ctrpDraftJB.subOrgTty" id="ctrpDraftJB.subOrgTty" size="50"></s:textfield></td>
						</tr>
						<tr>
						<td align="right" ><%=LC.L_Fax%>&nbsp;</td>
						<td >&nbsp;</td>
						<td align="left" ><s:textfield name="ctrpDraftJB.subOrgFax" id="ctrpDraftJB.subOrgFax" size="50"></s:textfield></td>
						</tr>
						<tr>
						<td align="right" ><%=LC.L_Url.toUpperCase()%>&nbsp; </td>
						<td >&nbsp;</td>
						<td align="left" ><s:textfield name="ctrpDraftJB.subOrgUrl" id="ctrpDraftJB.subOrgUrl" size="50"></s:textfield></td>
						</tr>
						<tr>
						<td align="right" width="15%"><%=VelosResourceBundle.getLabelString("CTRP_DraftSubmitOrgSpecifics",LC.L_Type) %>&nbsp;</td>
						<td width="1%">&nbsp;</td>
						<td align="left" ><s:property escape="false" value="ctrpDraftJB.subOrgTypeMenu" /></td>
						</tr>
						<tr>
						<td align="right"  height="22px"><%=LC.CTRP_DraftIsSubmOrgNCIDesignated %>&nbsp;</td>
						<td >&nbsp;</td>
						<td align="left" >
						<s:radio name="ctrpDraftJB.isNCIDesignated" list="ctrpDraftJB.isNCIDesignatedList" listKey="radioCode" listValue="radioDisplay" value="ctrpDraftJB.isNCIDesignated" /></td>
						</tr>
						<tr>
						<td align="right" >
							<span id="siteProdCode0" ><%=LC.CTRP_DraftSiteProgramCode %>&nbsp;</span>
							<span id="ctrpDraftJB.subOrgProgCode_error" class="errorMessage" style="display:none;"></span>
						</td>
						<td ><span id="siteProdCode1" ><FONT style="visibility:visible" class="Mandatory">* </FONT></span></td>
						<td align="left" >
						<span id="siteProdCode2" >
						<s:textfield name="ctrpDraftJB.subOrgProgCode" id="ctrpDraftJB.subOrgProgCode" size="50"></s:textfield></span></td>
						</tr>
						</table>
						</div>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="100%" valign="top">
			<table>
				<tr>
					<td width="50%" valign="top">
						<div id="sitePIDiv">
						<table width="50%" cellpadding="0" cellspacing="0" border="0">
						<tr>
						<td align="right"><a href="javascript:lookupSitePI('ctrpDraftJB.piId','ctrpDraftJB.piId');" ><%=LC.L_Select %></a></td></tr>
						<tr>
						<td align="right" width="30%" height="22px">
							<%=VelosResourceBundle.getLabelString("CTRP_DraftSitePiSpecifics",LC.L_Id_Upper) %>&nbsp;
						</td>
						<td width="1%">&nbsp;</td>
						<td align="left" >
						<s:textfield name="ctrpDraftJB.piId" id="ctrpDraftJB.piId"  size="50"></s:textfield></td>
						<td align="left" >&nbsp;</td>
						<td >&nbsp;</td>
						<td align="left" >&nbsp;</td>
						</tr>
						<tr>
						<td align="right"  height="30px"><span id="ctrpDraftJB.piId_error" class="errorMessage" style="display:none;"></span>&nbsp;</td>
						<td width="1%"><FONT style="visibility:visible" class="Mandatory">* </FONT></td>
						<td align="left"  >
						<p class="defComments"><FONT><%=MC.CTRP_PiEnterOrSelect%></FONT></p></td>
						</tr>
						<tr>
						<td align="right"><a href="javascript:openSelectUser(constPrinv);" ><%=LC.L_Select %></a></td>
						</tr>
						<tr>
						<td align="right"  height="22px"><%=LC.L_First_Name %>&nbsp; </td>
						<td >&nbsp;</td>
						<td align="left"  >
						<s:hidden name="ctrpDraftJB.piFkUser" id="ctrpDraftJB.piFkUser"/>
						<s:textfield name="ctrpDraftJB.piFirstName" id="ctrpDraftJB.piFirstName" readonly="true" size="50" cssClass="readonly-input"></s:textfield>&nbsp;</td>
						</tr>
						<tr>
						<td align="right"  height="22px"><%=LC.L_Middle_Name %>&nbsp;</td>
						<td >&nbsp;</td>
						<td align="left" ><s:textfield name="ctrpDraftJB.piMiddleName" id="ctrpDraftJB.piMiddleName" readonly="true" size="50" cssClass="readonly-input"></s:textfield></td>
						</tr>
						<tr>
						<td align="right"  height="22px"><%=LC.L_Last_Name %>&nbsp; </td>
						<td >&nbsp;</td>
						<td align="left" >
						<s:textfield name="ctrpDraftJB.piLastName" id="ctrpDraftJB.piLastName" readonly="true" size="50" cssClass="readonly-input"></s:textfield></td>
						</tr>
						<tr>
						<td align="right"  height="22px"><%=LC.L_Street_Address%>&nbsp;
						<span id="ctrpDraftJB.piStreetAddress_error" class="errorMessage" style="display:none;"></span>
						</td>
						<td >&nbsp;</td>
						<td align="left" >
						<s:hidden name="ctrpDraftJB.piFkAdd" id="ctrpDraftJB.piFkAdd"/>
						<s:textfield name="ctrpDraftJB.piStreetAddress" id="ctrpDraftJB.piStreetAddress" readonly="true" size="50" cssClass="readonly-input"></s:textfield></td>
						</tr>
						<tr>
						<td align="right"  height="22px"><%=LC.L_City%>&nbsp;
						<span id="ctrpDraftJB.piCity_error" class="errorMessage" style="display:none;"></span>
						</td>
						<td >&nbsp;</td>
						<td align="left" >
						<s:textfield name="ctrpDraftJB.piCity" id="ctrpDraftJB.piCity" readonly="true" size="50" cssClass="readonly-input"></s:textfield></td>
						</tr>
						<tr>
						<td align="right"  height="22px"><%=LC.CTRP_DraftStateProvince%>&nbsp;
						<span id="ctrpDraftJB.piState_error" class="errorMessage" style="display:none;"></span>
						</td>
						<td >&nbsp;</td>
						<td align="left" >
						<s:textfield name="ctrpDraftJB.piState" id="ctrpDraftJB.piState" readonly="true" size="50" cssClass="readonly-input"></s:textfield></td>
						</tr>
						<tr>
						<td align="right" ><%=LC.L_ZipOrPostal_Code%>&nbsp;
						<span id="ctrpDraftJB.piZip_error" class="errorMessage" style="display:none;"></span>
						</td>
						<td >&nbsp;</td>
						<td align="left" ><s:textfield name="ctrpDraftJB.piZip" id="ctrpDraftJB.piZip" readonly="true" size="50" cssClass="readonly-input"></s:textfield></td>
						</tr>
						<tr>
						<td align="right"  height="22px"><%=LC.L_Country%>&nbsp;
						<span id="ctrpDraftJB.piCountry_error" class="errorMessage" style="display:none;"></span>
						</td>
						<td >&nbsp;</td>
						<td align="left" >
						<s:textfield name="ctrpDraftJB.piCountry" id="ctrpDraftJB.piCountry" readonly="true" size="50" cssClass="readonly-input"></s:textfield></td>
						</tr>
						<tr>
						<td align="right" ><%=LC.L_Email_Addr%>&nbsp;
						<span id="ctrpDraftJB.piEmail_error" class="errorMessage" style="display:none;"></span>
						</td>
						<td >&nbsp;</td>
						<td align="left" >
						<s:textfield name="ctrpDraftJB.piEmail" id="ctrpDraftJB.piEmail" readonly="true" size="50" cssClass="readonly-input"></s:textfield></td>
						</tr>
						<tr>
						<td align="right" ><%=LC.L_Phone%>
						<span id="ctrpDraftJB.piPhone_error" class="errorMessage" style="display:none;"></span>
						</td>
						<td >&nbsp;</td>
						<td align="left" >
						<s:textfield name="ctrpDraftJB.piPhone" id="ctrpDraftJB.piPhone" readonly="true" size="50" cssClass="readonly-input"></s:textfield></td>
						</tr>
						<tr>
						<td align="right" ><%=LC.L_TTY %>&nbsp;</td>
						<td >&nbsp;</td>
						<td align="left" ><s:textfield name="ctrpDraftJB.piTty" id="ctrpDraftJB.piTty" size="50"></s:textfield></td>
						</tr>
						<tr>
						<td align="right" ><%=LC.L_Fax%>&nbsp;</td>
						<td >&nbsp;</td>
						<td align="left" ><s:textfield name="ctrpDraftJB.piFax" id="ctrpDraftJB.piFax" size="50"></s:textfield></td>
						</tr>
						<tr>
						<td align="right" ><%=LC.L_Url.toUpperCase()%>&nbsp;</td>
						<td >&nbsp;</td>
						<td align="left" ><s:textfield name="ctrpDraftJB.piUrl" id="ctrpDraftJB.piUrl" size="50"></s:textfield></td>
						</tr>					
						</table>
						</div>
					</td>
					<td width="50%" valign="top"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
