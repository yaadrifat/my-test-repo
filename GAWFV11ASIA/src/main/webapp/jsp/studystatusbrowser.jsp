<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>

<%@page import="java.sql.Timestamp"%>
<html>
<head>
<title><%=LC.L_Std_Stat%><%--<%=LC.L_Study%> >> Status*****--%></title>

<%@ page import="java.util.*,java.io.*" %>
<%@page import="com.velos.eres.service.util.LC"%>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">



<script>

var screenWidth = screen.width;
var screenHeight = screen.height;
function ecomp_delete(studyStatusId)
{	
	if (confirm("<%=MC.M_WantToDel_StdStat%>"))/*if (confirm("Do you want to delete this <%=LC.Std_Study_Lower%> status?"))*****/
{

} else
{
	return false
}
}

function f_delete(islatest,nonactiveId,statId,pageRight,enrPatCount,subType,activeCount,len,active,currentStatId) {


	//Added by Manimaran for September Enhancement S8.
	if(statId == currentStatId) {
	  alert("<%=MC.M_CntDel_CurStdStat%>");/*alert("You can not delete the current <%=LC.Std_Study_Lower%> status");*****/
	  return false;
	}
	if (f_check_perm(pageRight,'E'))
	{
			if (len<2)
			{
				alert("<%=MC.M_CntDel_StatMustExist%>");/*alert("Can not delete status, atleast one status must exist.");*****/
				return false;
			}

//			if ((enrPatCount > 0) && (subType=='active'))
//			{
//				alert("A <%=LC.Pat_Patient_Lower%> is currently enrolled to the <%=LC.Std_Study_Lower%> and Active/Enrolling status can not be deleted.");
//				return false;
//			}


// RK - 09/20/04
			if ((activeCount==1) && (enrPatCount > 0) && (subType=='active'))
			{
				var paramArray = [active];
				alert(getLocalizedMessageString("M_PatCurEnrl_StatDel",paramArray));/*alert("A <%=LC.Pat_Patient_Lower%> is currently enrolled to the <%=LC.Std_Study_Lower%> and '"+active+"' status can not be deleted.");*****/
				return false;
			}
// RK - 09/20/04

		if (confirm("<%=MC.M_WantToDel_StdStat%>"))/*if (confirm("Do you want to delete this <%=LC.Std_Study_Lower%> status?"))*****/
		{

		} else
		{
			return false
		}
	} else
	{
	return false;
	}//end of edit right
} //end of function

function f_open(frm,pgRight,studyId, active, closure){

	if (f_check_perm(pgRight,'E') == true) {

		if ((frm.studyStartDate.value=="") && (frm.studyEndDate.value=="")) {
			var paramArray = [active,closure];
			alert(getLocalizedMessageString("M_EtrStdStrt_EdtStatEtr",paramArray));/*alert("To enter/edit the <%=LC.Std_Study%> Start Date, a status of '"+active+"' should have been entered. To enter/edit the <%=LC.Std_Study%> End Date, a status of '"+closure+"' should have been entered.");*****/
			return false;
		}
		windowName= window.open("changestudydates.jsp?studyId="+studyId,"information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=650,height=250");
		windowName.focus();
	}
}

 // modified by Gopu on 05th May 2005 for fixing the bug # 2132

 function openWin(pageRight,src,selectedTab,oldStudyStatusId,formobj,selOrgId,sitesCnt,studyStartDt,studyEndDt,currentStatId,stid,hiddenflag)
{
     if ( f_check_perm(pageRight,'N') )
	{
	 	if(sitesCnt == 0)
		{
		 alert("<%=MC.M_NotAces_ToAddStdStat%>");/*alert("You do not have access to add <%=LC.L_Study%> Status for any Organization");*****/
		 return false;
		}
		else{
		 // modified by Gopu on 05th May 2005 for fixing the bug # 2132
		param = "studystatus.jsp?lastStatId="+oldStudyStatusId+"&mode=N&srcmenu="+src+"&selectedTab="+selectedTab+"&selOrgId="+selOrgId+"&studyStartDt="+studyStartDt+"&studyEndDt="+studyEndDt+"&currentStatId="+currentStatId+"&studyId="+stid+"&hiddenflag="+hiddenflag;
		windowName= window.open(param,"_self","toolbar=no,scrollbars=yes,resizable=yes,menubar=yes,status=yes,dependant=false,width=600,height=300");
		windowName.focus();
		}
	}
}



</script>


</head>

<% String src;
src= request.getParameter("srcmenu");
String from  = "status";


String selectedTab;
selectedTab = request.getParameter("selectedTab");
%>


<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<jsp:useBean id="studyStatB" scope="request" class="com.velos.eres.web.studyStatus.StudyStatusJB"/>

<jsp:useBean id="grpk" scope="page" class="com.velos.eres.business.common.GroupDao" />
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="stdSiteRightsB" scope="page" class="com.velos.eres.web.stdSiteRights.StdSiteRightsJB" />
<jsp:useBean id="studySiteB" scope="page" class="com.velos.eres.web.studySite.StudySiteJB" />
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />

<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0">

<%@ page language = "java" import = "com.velos.eres.business.site.*,com.velos.eres.web.user.*,java.text.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.DateUtil"%>
<%@page import="com.velos.eres.service.util.StringUtil"%>
<%
HttpSession tSession = request.getSession(true);

   if (sessionmaint.isValidSession(tSession))

	{
	   String hiddenflag=request.getParameter("hiddenflag")==null?"":request.getParameter("hiddenflag"); //hidden flag for epworth researcher enhancement
	
		%>

<!--
//JM: 12/05/2005
-->
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
<!--
//JM: 12/05/2005
-->
<%if(hiddenflag.equalsIgnoreCase("0")){ %>
<DIV class="BrowserTopn" id="divTab" style="top:10px;">
<%}else{ %>
<DIV class="BrowserTopn" id="divTab"><%} %>
		<jsp:include page="studytabs.jsp" flush="true">
		<jsp:param name="from" value="<%=from%>"/>
		<jsp:param name="selectedTab" value="<%=selectedTab%>"/>
		<jsp:param name="studyId" value="<%=StringUtil.htmlEncodeXss(request.getParameter(\"studyId\")) %>"/>
	</jsp:include>
</DIV>
<%
    userB= (com.velos.eres.web.user.UserJB)session.getAttribute("currentUser");
    GroupDao grpid = new GroupDao();
	String sessStudyId = request.getParameter("studyId");
	String oldStudyStatusId = request.getParameter("oldStudyStatusId");
	if(sessStudyId == null || sessStudyId.equals(""))
	sessStudyId = (String) tSession.getValue("studyId");

	String acc = (String) tSession.getValue("accountId");
	int accntid = Integer.parseInt(acc);
	//km-Bug 2400 Displays incorrect Study Number of the Study last accessed.
	tSession.setAttribute("studyId",sessStudyId);
	studyB.setId(EJBUtil.stringToNum(sessStudyId));
	studyB.getStudyDetails();
	String sno= studyB.getStudyNumber();
	tSession.setAttribute("studyNo",sno);

//	UserJB user = (UserJB) tSession.getValue("currentUser");
	String uName = (String) tSession.getValue("userName");

	String userIdFromSession = (String) tSession.getValue("userId");
	UserJB user = (UserJB) tSession.getValue("currentUser");

	 int grpid_no = 0;	
	grpid_no = grpk.getGrpPk(accntid);
	String siteId = user.getUserSiteId();

	String dOrg = "";
	Integer org ;

	if(sessStudyId == null || sessStudyId.equals("")) {

	sessStudyId = (String) tSession.getValue("studyId");
	  } else {

	  	String roleCodePk ="";

	  	tSession.setAttribute("studyId",sessStudyId);
		studyB.setId(EJBUtil.stringToNum(sessStudyId));
		studyB.getStudyDetails();
		String studyNumber = studyB.getStudyNumber();
		tSession.setAttribute("studyNo",studyNumber);


		TeamDao teamDao = new TeamDao();
		teamDao.getTeamRights(EJBUtil.stringToNum(sessStudyId),EJBUtil.stringToNum(userIdFromSession));
		ArrayList tId ;
		tId = new ArrayList();
		tId = teamDao.getTeamIds();
		if (tId.size() <=0)
		{
			StudyRightsJB stdRightstemp = new StudyRightsJB();
			tSession.putValue("studyRights",stdRightstemp);
			tSession.putValue("studyRoleCodePk",roleCodePk);

		} else {

				ArrayList arRoles = new ArrayList();
					arRoles = teamDao.getTeamRoleIds();

					if (arRoles != null && arRoles.size() >0 )
					{
						roleCodePk = (String) arRoles.get(0);

						if (StringUtil.isEmpty(roleCodePk))
						{
							roleCodePk="";
						}
					}
					else
					{
						roleCodePk ="";
					}


				stdRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));

				ArrayList teamRights ;
				teamRights  = new ArrayList();
				teamRights = teamDao.getTeamRights();

				stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
				stdRights.loadStudyRights();


			tSession.putValue("studyRoleCodePk",roleCodePk);

			tSession.putValue("studyRights",stdRights);
		}
	  }

	String studyNo = (String) tSession.getValue("studyNo");


	int accountId = EJBUtil.stringToNum((String) tSession.getValue("accountId"));
 	String dAccSites = "";
 	StringBuffer sb = new StringBuffer();
 	int pageVisit = 0;
 	String selsite = request.getParameter("selsite") ;

	String selOrg = request.getParameter("accsites") ;

	if(selOrg == null)
		selOrg = selsite;



	studyB.setId(EJBUtil.stringToNum(sessStudyId));
	studyB.getStudyDetails();
	String studyNumber = studyB.getStudyNumber();
	String studyStartDt = studyB.getStudyActBeginDate();
	String studyEndDt = studyB.getStudyEndDate();

	/*if(selOrg == null)
		selOrg = siteId;
	*/
	if(selOrg == null || selOrg.equals(""))
		selOrg = "0";
	 int selOrgId = EJBUtil.stringToNum(selOrg);

     StudySiteDao ssDao = new StudySiteDao();

     //ssDao	= studySiteB.getAddedAccessSitesForStudy(EJBUtil.stringToNum(sessStudyId), EJBUtil.stringToNum(userIdFromSession),accountId);
     //ssDao	= studySiteB.getAllStudyTeamSites(EJBUtil.stringToNum(sessStudyId), accountId);

	 // changed req

	 /* Changed by Sonia Abrol, 06/28/05, Now we need same organizations as we get on the study team page*/

	 //ssDao	= studySiteB.getSitesForStatAndEnrolledPat(EJBUtil.stringToNum(sessStudyId), EJBUtil.stringToNum(userIdFromSession), accountId);
	  ssDao	= studySiteB.getAllStudyTeamSites(EJBUtil.stringToNum(sessStudyId), accountId);

     ArrayList arrSiteIds = ssDao.getSiteIds();
	 ArrayList arrSiteNames = ssDao.getSiteNames();

	 //commented by VA, seems like Anu missed files in development release.StudySiteJB did not have this method
	 //right  now this method is getting referred from DAO which needs to be changed by Anu and re-release the patch
	 //int cnt =  studySiteB.checkSuperUser(EJBUtil.stringToNum(sessStudyId), EJBUtil.stringToNum(userIdFromSession),accountId);
	 // Integrated the related beans and now the method is called using client bean and not diectly calling the DAO method .--Anu

	 int cnt = 0 ;

	 int totalSites = arrSiteIds.size();
	 int sitesCnt = totalSites;

	 if(totalSites == 0)
	 {
	    cnt = studySiteB.checkSuperUser(EJBUtil.stringToNum(sessStudyId), EJBUtil.stringToNum(userIdFromSession),accountId);
	 	totalSites = cnt;
	 }

	  sb.append("<SELECT NAME='accsites'>") ;
	  sb.append("<option value = ''>"+LC.L_All+"</option>") ;/*sb.append("<option>All</option>") ;*****/

		if (arrSiteIds.size() > 0)
		{
			for (int counter = 0; counter <= arrSiteNames.size() -1 ; counter++)
			{
				org = (Integer) arrSiteIds.get(counter);
				if(	org.intValue() == selOrgId)	{
				sb.append("<OPTION value = "+ org+" SELECTED>" + arrSiteNames.get(counter)+ "</OPTION>");
				}
				else
				{
				sb.append("<OPTION value = "+ org+">" + arrSiteNames.get(counter)+ "</OPTION>");
				}
			}
		}
		sb.append("</SELECT>");
	dOrg = sb.toString();
%>


<%
String stid= request.getParameter("studyId");
if(stid==null || stid==""){%>
<SCRIPT LANGUAGE="JavaScript">
	if(screenWidth>1280 || screenHeight>1024)
		document.write('<DIV class="BrowserBotN BrowserBotN_S_1" id="div1"  style="height:75%;">')
	else
		document.write('<DIV class="BrowserBotN BrowserBotN_S_1" id="div1">')
</SCRIPT>

	<%} 
	else if(hiddenflag.equalsIgnoreCase("0")){%>
<SCRIPT LANGUAGE="JavaScript">
document.write('<DIV class="BrowserBotN BrowserBotN_S_3" id="div1" style="top:30;height:90%;">')
</SCRIPT>
<%}
	
	else {%>
<SCRIPT LANGUAGE="JavaScript">
/* Condition to check configuration for workflow enabled */
<% if("Y".equals(CFG.Workflows_Enabled) && (stid!=null && !stid.equals("") && !stid.equals("0")) && !"LIND".equals(CFG.EIRB_MODE)){ %>
		if(screenWidth>1280 || screenHeight>1024)
			document.write('<DIV class="BrowserBotN BrowserBotN_S_3 workflowDivBig" id="div1" style="height:75%;" >')
		else
			document.write('<DIV class="BrowserBotN BrowserBotN_S_3 workflowDivSmall" id="div1">')
<% } else{ %>
		if(screenWidth>1280 || screenHeight>1024)
			document.write('<DIV class="BrowserBotN BrowserBotN_S_3" id="div1" style="height:75%;" >')
		else
			document.write('<DIV class="BrowserBotN BrowserBotN_S_3" id="div1">')
<% } %>
</SCRIPT>
<%}

	

      int pageRight= 0;

      if( (sessStudyId== null) || (sessStudyId.equals(""))) {

 		//out.println("<jsp:include page=\"studyDoesNotExist.jsp\" flush=\"true\"/>");
 		%>
 			<%@ include file="studyDoesNotExist.jsp" %>

 			<%


	   } else {

		   int studyId = EJBUtil.stringToNum(sessStudyId);

	   	   stdRights =(StudyRightsJB) tSession.getValue("studyRights");

	   	   if ((stdRights.getFtrRights().size()) == 0){

		 	pageRight= 0;

		   }else{

			pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYPTRACK"));

	   	   }

	 	if (pageRight > 0 )

		{
		   //Added by Manimaran for the September Enhancement S8.

            StudyStatusDao studyStatDao = new StudyStatusDao();
	 		StudyStatusDao currStudyStatDao = new StudyStatusDao();
    if(hiddenflag.equalsIgnoreCase("")){
		   int allOrg=0;
		    currStudyStatDao = studyStatB.getStudyStatusDesc(studyId, allOrg, EJBUtil.stringToNum(userIdFromSession), accountId);
       }
		   ArrayList currentStats =  currStudyStatDao.getCurrentStats();
		   ArrayList currStatusIdLst = currStudyStatDao.getIds();
		   ArrayList currStatusLst = currStudyStatDao.getDescStudyStats();
		   int len = currStatusIdLst.size();
		   String currentStatId="";
		   String currentVal="";
		   String currentStatus ="";
           if(hiddenflag.equalsIgnoreCase("")){
		   for (int i=0;i<len;i++) {
		        currentVal = (currentStats.get(i) == null)?"":(currentStats.get(i)).toString();
			if ( currentVal.equals("1")) {
				 currentStatId = (String)currStatusIdLst.get(i).toString();
			     currentStatus=((currStatusLst.get(i)) == null)?"-":(currStatusLst.get(i)).toString();
		        }
    		   }



		    studyStatDao = studyStatB.getStudyStatusDesc(studyId, EJBUtil.stringToNum(selOrg), EJBUtil.stringToNum(userIdFromSession), accountId);
         }
      if(hiddenflag.equalsIgnoreCase("0")){
		   studyStatDao= studyStatB.getecompStatusdesc(studyId);
		   }


//JM: 12/05/2005
	  	   ArrayList maxStudyStatDates = studyStatDao.getMaxStudyStatDates();

		   ArrayList irbExpDateLst = studyStatDao.getIrbExpirationDates();
		   ArrayList dateDiffs = studyStatDao.getExpDateDiffs();

		   ArrayList studyStatusIdLst = studyStatDao.getIds();

		   ArrayList startDateLst = studyStatDao.getStatStartDates();
		   ArrayList statNotesLst = studyStatDao.getStatNotes();
		   ArrayList externalLinkLst = studyStatDao.getExternalLinks();
		   ArrayList protocolStatusLst = studyStatDao.getDescStudyStats();
		   ArrayList subTypeStatusLst = studyStatDao.getSubTypeStudyStats();
		   ArrayList studyStartDatesLst = studyStatDao.getStudyStartDates();
   		   ArrayList studyEndDatesLst = studyStatDao.getStudyEndDates();
		   ArrayList statValidDatesLst = studyStatDao.getStatValidDates();
           ArrayList statMeetingDatesLst = studyStatDao.getStatMeetingDates(); //Amar
		   ArrayList studyStatEndDates = studyStatDao.getStatEndDates();
		   ArrayList siteIds = studyStatDao.getSiteIds();
		   ArrayList siteNames = studyStatDao.getSiteNames();

		   int firstNonActiveStatus = studyStatDao.getFirstNonActiveStatusId();
		   int enrPatCount = studyStatDao.getEnrPatCnt();

		   String maxStudyStatDate = null;//
		   String   irbExpDateStr = null;
		   String 	dateDiffStr = null;
		   String studyStatusId = null;
		   String startDate =  null;
		   String protocolStatus =null;
   		   String statNotes =null;
   		   String externalLink = null;
		   String subType = null;
		   String statEnd = null;
		   String statValidDate = null;
		   String statMeetingDate = null;	//Amar
		   String stdSiteId = null;
		   String siteName = "";
		   int subtyperows = 0;
		   String apprStatusCtrlSubType = "";

		  CodeDao  cd = new CodeDao();
		  cd.getCodeId("studystat","active");
		  String active = cd.getCodeDescription();

		  cd.getCodeId("studystat","prmnt_cls");

   		  String closure = cd.getCodeDescription();

   		  	CtrlDao statusCtrl = new CtrlDao();
	    	statusCtrl.getControlValues("irb_app_stat");
			subtyperows = statusCtrl.getCRows();
			if (subtyperows > 0)
			   {
			   	apprStatusCtrlSubType = (String) statusCtrl.getCValue().get(0);
			   }

   		      if(studyStartDt == null)
		      	  studyStartDt = "";

   		      if(studyEndDt == null)
   		      	  studyEndDt = "";

		  /* if (studyStartDatesLst.size() >0) {
		   	 studyStartDt = ((studyStartDatesLst.get(0)) == null)?"-":(studyStartDatesLst.get(0)).toString();
		   }

		   if (studyEndDatesLst.size() >0) {
		   	 studyEndDt =((studyEndDatesLst.get(0)) == null)?"-":(studyEndDatesLst.get(0)).toString();
		   }*/
		   len = studyStatusIdLst.size();
		   // Total count of the status entered for an organization
		   int statusCnt = studyStatB.getCountStudyStat(studyId);

		   int counter = 0;
		   int activeCount = 0;
		   String stDate="";
		   int isLatest = 0;
		   String lastStudyStatusId = null;

%>

<Form name="studystatusbrowser" method="post" action="" onSubmit="">
<div class="tmpHieght"></div>
<%if(hiddenflag.equalsIgnoreCase("")){ %>
<table width="99%" border="0" cellspacing="0" cellpadding="0" class="basetbl outline midAlign">
<tr height="3"><td></td></tr>
<tr>
<td width="50%">
    	<table width="100%" >
    			
    			<tr><td width ="35%"><%=LC.L_Search_ByOrg%><%--Search by Organization*****--%></td>
    	<%if(totalSites>0){%>
    			<td width = "15%"><%=dOrg%></td>
    			<td><P><button type="submit"><%=LC.L_Search%></button></P></td>
    				<%}else{%>
    				<td><P class="defComments"><B><%=MC.M_NoRgt_ToAddStat%><%--You do not have rights to add any status*****--%></B> </P></td>
    				<%}%>

    			</tr>
    		</table>
</td>
<td width="50%">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="outline">	
	<!-- Added by Manimaran for September Enhancement S8. -->
	<th width="20%"><%=LC.L_Current_Status%><%--Current Status*****--%> </th>
	<th width="20%"><%=LC.L_Std_StartDate%><%--<%=LC.L_Study%> Start Date*****--%></th>
	<th width="20%"><%=LC.L_Std_EndDate%><%--<%=LC.L_Study%> End Date*****--%></th>
	<th width="20%">&nbsp;</th>
	<tr class="browserEvenRow">
		<td><%=currentStatus%></td>
		<td><%=studyStartDt%></td>
		<td><%=studyEndDt%></td>
		<td><A href="#" onclick="f_open(studystatusbrowser,<%=pageRight%>,<%=studyId%>,'<%=active%>','<%=closure%>')"><%=LC.L_Change_Dates%><%--Change Dates*****--%></A></td>
	</tr>
	</table>
</td>
</tr>
<tr height="3"><td></td></tr>
</table>
<%} %>
    <Input type="hidden" name="selectedTab" value="<%=selectedTab%>">
    <Input type="hidden" name="studyStartDate" value="<%=studyStartDt%>">
    <Input type="hidden" name="studyEndDate" value="<%=studyEndDt%>">
    <Input type="hidden" name="oldStudyStatusId" value="<%=oldStudyStatusId%>">

		<table width="99%" border="0" cellspacing="0" cellpadding="0" class="midAlign">
    			<tr height="8"><td></td></tr>
    			<tr>
    		<td width = "80%">
          <P class = "sectionheadings" ><%=LC.L_Std_StatusHistory%><%--<%=LC.L_Study%> Status History*****--%>:
          </P>
        </td>

	 <!-- modified by Gopu on 05th May 2005 for fixing the bug # 2132  -->

	
	 <%
	if(totalSites>0)

		{ if(hiddenflag.equalsIgnoreCase("")){
		%>
<td align="right">
		<A href="#" onclick="openWin(<%=pageRight%>,'<%=src%>',<%=3%>,<%=oldStudyStatusId%>,document.studystatusbrowser,<%=selOrgId%>,<%=sitesCnt%>,'<%=studyStartDt%>','<%=studyEndDt%>','<%=currentStatId%>','<%=stid%>','<%=hiddenflag%>')"><%=LC.L_Add_NewStatus_Upper%><%--ADD NEW STATUS*****--%> </A>

<%} else{
	if(userB.getUserGrpDefault().equalsIgnoreCase(grpid_no+"")){%>
	
	<td align="left">
       <A href="#" onclick="openWin(<%=pageRight%>,'<%=src%>',<%=3%>,<%=oldStudyStatusId%>,document.studystatusbrowser,<%=selOrgId%>,<%=sitesCnt%>,'<%=studyStartDt%>','<%=studyEndDt%>','<%=currentStatId%>','<%=stid%>','<%=hiddenflag%>')"><%=LC.L_Add_NewStatus_Upper%><%--ADD NEW STATUS*****--%> </A>
	<% }}}%>
		</td></tr>

    				</table>
    <table width="99%" border="0" cellspacing="0" cellpadding="0" class="outline midAlign">
    	 <%if(hiddenflag.equalsIgnoreCase("0")){ %> 
      
      <tr>
        <th width="5%"> <%=LC.L_Study_Comp_Status%><%--Study Compliance Status --%> </th>
        <th width="5%"> <%=LC.L_Status_ValidFrom%><%--Status Valid From*****--%></th>
        <%	if(userB.getUserGrpDefault().equalsIgnoreCase(grpid_no+"")){%>
         <th width="3%"><%=LC.L_Delete%><%--Delete*****--%></th>
         <%} %>
         </tr>
        
         <%}

    
         
   		else{ %>

 			
    
      <tr>
      	<th width="15%"> <%=LC.L_Organization%><%--Organization*****--%></th>
        <th width="15%"> <%=LC.L_Study_Status%><%--<%=LC.L_Study%> Status*****--%> </th>
        <th width="15%"> <%=LC.L_Status_ValidFrom%><%--Status Valid From*****--%></th>
		<th width="15%"> <%=LC.L_Status_ValidUntil%><%--Status Valid Until*****--%></th>
		<th width="15%"> <%=LC.L_Meeting_Date%><%--Meeting Date*****--%></th> <!--Amar -->
        <th width="30%"> <%=LC.L_Notes%><%--Notes*****--%> </th>
        <% if("LIND".equals(CFG.EIRB_MODE)) { %>
        <th width="10%"> <%=LC.L_Link_Uri%><%--Link*****--%> </th>
        <% } %>
        <th width="10%"><%=LC.L_Delete%><%--Delete*****--%></th>
      </tr>

<%}

// RK - 09/20/04
    for(counter = 0;counter<len;counter++)
	{
		subType =(((subTypeStatusLst.get(counter)) == null)?"-":(subTypeStatusLst.get(counter)).toString()).trim();
		if (subType.equals("active")){
			activeCount++;
		}
	}

// RK - 09/20/04
	String oldSiteId = "";
	//String CurrentDate = DateUtil.getCurrentDate();

	Date dCurrent = DateUtil.stringToDate(DateUtil.dateToString(new Date()),null);

	for(counter = 0;counter<len;counter++)
	{

//JM: 24/10/05

//irbStatusStr = ((irbStatusLst.get(counter)) == null)?"-":(irbStatusLst.get(counter)).toString();
//JM: 12/05/2005
      Date dExpire = null;
 	 if(hiddenflag.equalsIgnoreCase("")){ 
	maxStudyStatDate=((maxStudyStatDates.get(counter)) == null)?"-":(maxStudyStatDates.get(counter)).toString();
	irbExpDateStr = ((irbExpDateLst.get(counter))==null)?"":DateUtil.dateToString(java.sql.Date.valueOf(irbExpDateLst.get(counter).toString().substring(0,10)));
	dateDiffStr=((dateDiffs.get(counter))==null)?"-":(dateDiffs.get(counter)).toString();


	
	if (!irbExpDateStr.equals("")){
	 dExpire = DateUtil.stringToDate(irbExpDateStr,null);
	}
	else
	dExpire = null;
}

		studyStatusId= ((Integer)studyStatusIdLst.get(counter)).toString();
		startDate=((startDateLst.get(counter)) == null)?"-":(startDateLst.get(counter)).toString();

if(hiddenflag.equalsIgnoreCase("")){ 

		stdSiteId=((siteIds.get(counter)) == null)?"-":(siteIds.get(counter)).toString();
		siteName=((siteNames.get(counter)) == null)?"-":(siteNames.get(counter)).toString();
		statValidDate=((statValidDatesLst.get(counter)) == null)?"-":(statValidDatesLst.get(counter)).toString();
		statMeetingDate=((statMeetingDatesLst.get(counter)) == null)?"-":(statMeetingDatesLst.get(counter)).toString(); //Amar
		int validDateLen = statValidDate.length();
		int meetingDateLen = statMeetingDate.length(); //Amar
		int startDateLen=startDate.length();

		if (startDateLen > 1) {
			stDate = DateUtil.dateToString(java.sql.Date.valueOf(startDateLst.get(counter).toString().substring(0,10)));
			//startDate = startDate.substring(0,10);
		}
		if(validDateLen > 1){
			statValidDate = DateUtil.dateToString(java.sql.Date.valueOf(statValidDatesLst.get(counter).toString().substring(0,10)));

		}
        // Added By Amar
		validDateLen = statMeetingDate.length();
		if(validDateLen > 1){
			statMeetingDate = DateUtil.dateToString(java.sql.Date.valueOf(statMeetingDatesLst.get(counter).toString().substring(0,10)));

		}

		
		statNotes=((statNotesLst.get(counter)) == null)?"-":(statNotesLst.get(counter)).toString();
                statEnd = ((studyStatEndDates.get(counter)) == null)?"-":(studyStatEndDates.get(counter)).toString();
                externalLink=((externalLinkLst.get(counter)) == null)?"-":(externalLinkLst.get(counter)).toString();              
}
		
	   protocolStatus=((protocolStatusLst.get(counter)) == null)?"-":(protocolStatusLst.get(counter)).toString();
		
		subType =(((subTypeStatusLst.get(counter)) == null)?"-":(subTypeStatusLst.get(counter)).toString()).trim();


		if(hiddenflag.equalsIgnoreCase("")){ 


		if  (   studyStatEndDates.get(counter)   == null )
		{
				lastStudyStatusId = ((Integer)studyStatusIdLst.get(counter)).toString() ;
		}
		if(counter == 0 )
			oldSiteId = stdSiteId;
		else
			oldSiteId = (siteIds.get(counter-1)).toString();



		if (statEnd.length() > 1)
		{
		 isLatest = 0;
		}
		else
		{
  	     isLatest = 1;
		}

			if(counter == 0 || !oldSiteId.equals(stdSiteId)){
  			%>	<tr><td colspan="6"><%=siteName%></td></tr>
  	    	 <%}
             }

		if ((counter%2)==0) {

  %>
      <tr class="browserEvenRow">
        <%

		}

		else{

  %>
      <tr class="browserOddRow">
        <%

		}
		%>
		<!-- JM: show on tooltip IRB approval status expiry date-->
		<% if(hiddenflag.equalsIgnoreCase("")){%> 
		<td>
		<%
//JM: 01.06.2006:
//	if (startDate.equals(maxStudyStatDate) && dExpire !=null){
//JM: 01.23.2006: Study status having irb approval status is required here
 // by sonia abrol, to get approval status subtype from control table


	if (startDate.equals(maxStudyStatDate) && subType.equals(apprStatusCtrlSubType) ){
		if (dExpire !=null){

			if ( dCurrent.compareTo(dExpire)>0 ){
				Object[] arguments = {protocolStatus,dateDiffStr,irbExpDateStr};%>
			<img src="../images/jpg/alert.jpg" onmouseover="return overlib('<%="<tr><td><font size=2><b> "+VelosResourceBundle.getMessageString("M_StatStd_ExpDays",arguments)/*{0} status of this study expired since {1} day(s) from {2}*****/+"</b></font></td></tr>"%>',CAPTION,'<%=LC.L_IrbExp_Note_Upper%><%-- IRB EXPIRATION NOTE*****--%>',RIGHT,ABOVE);" onmouseout="return nd();"/>
			<%}else{Object[] arguments1 = {protocolStatus,(EJBUtil.stringToNum(dateDiffStr)+1),irbExpDateStr};%>
			<img src="../images/jpg/info.jpg" onmouseover="return overlib('<%="<tr><td><font size=2><b> "+VelosResourceBundle.getMessageString("M_StatStd_WillExpDays",arguments1)/*{0} status of this study will expire in {1} day(s) on {2}*****/+"</b></font></td></tr>"%>',CAPTION,'<%=LC.L_IrbExp_Note_Upper%><%-- IRB EXPIRATION NOTE*****--%>',RIGHT,ABOVE);" onmouseout="return nd();"/>
			<%}
		}else{Object[] arguments2 = {protocolStatus};%>
		<img src="../images/jpg/info.jpg" onmouseover="return overlib('<%="<tr><td><font size=2><b> "+VelosResourceBundle.getMessageString("M_StatStd_Exp",arguments2)/*{0} status of this study will expire in ...*****/+"</b></font></td></tr>"%>',CAPTION,'<%=LC.L_IrbExp_Note_Upper%><%-- IRB EXPIRATION NOTE*****--%>',RIGHT,ABOVE);" onmouseout="return nd();"/>
	   <%}

	}%>
		</td>
<% }%>
		<!-- Added by Gopu on 06th May 2005 for fixing the bug # 2132  -->
 <% if(hiddenflag.equalsIgnoreCase("0")) {%>
      <td> <%=protocolStatus%></td>
      <td><%=startDate%> </td>
      <%if(userB.getUserGrpDefault().equalsIgnoreCase(grpid_no+"")){%>
       <td>
	 <!--Modified by Manimaran on 7Aug08 -->
	<A href="studystatusdelete.jsp?studyStatusId=<%=studyStatusId%>&srcmenu=<%=src%>&hiddenflag=<%=hiddenflag%>&studyId=<%=studyId%>" onClick="return ecomp_delete(<%=studyStatusId%>)"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" align="left"/></A></td>
    
      <%} }
      
		else {%>
        <td> <A href="studystatus.jsp?selOrgId=<%=stdSiteId%>&studyStatusId=<%=studyStatusId%>&mode=M&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&studyStartDt=<%=studyStartDt%>&studyEndDt=<%=studyEndDt%>&currentStatId=<%=currentStatId%>&studyId=<%=stid%>" onClick="return f_check_perm(<%=pageRight%>,'E')"><%=protocolStatus%> </A></td>
        <td><%=stDate%> </td>
		<td><%=statValidDate%></td>
		<td><%=statMeetingDate%></td>
        <td><%=statNotes%></td>
        <% if("LIND".equals(CFG.EIRB_MODE)) { %>
        <td><% if (!"-".equals(externalLink)) { %><a href='<%=externalLink%>' title='<%=externalLink%>'>Link</a> <% } else { out.print(externalLink); }%></td>
        <% } %>
        <td>
	 <!--Modified by Manimaran on 7Aug08 -->
	<A href="studystatusdelete.jsp?studyStatusId=<%=studyStatusId%>&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>&lastStudyStatusId=<%=lastStudyStatusId%>&selOrgId=<%=selOrgId%>&&delMode=null&from=<%=from%>&subType=<%=subType%>" onClick="return f_delete(<%=isLatest%>,<%=firstNonActiveStatus%>,<%=studyStatusId%>,<%=pageRight%>,<%=enrPatCount%>,'<%=subType%>',<%=activeCount%>,<%=statusCnt%>,'<%=active%>','<%=currentStatId%>')"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" align="left"/></A></td>
     <%






}%>
      </tr>
      <%

		}

%>
    </table>

 </Form>
  <%

	} //end of if body for page right
else
{
 // out.println("<jsp:include page=\"accessdenied.jsp\" flush=\"true\"/>");
  %>

  	  <%@ include file="accessdenied.jsp" %>
  	 <%

} //end of else body for page right

}//end of study id check

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</DIV>
<div class ="mainMenu" id="emenu">
  	<jsp:include page="getmenu.jsp" flush="true"/>
</div>
</body>
</html>