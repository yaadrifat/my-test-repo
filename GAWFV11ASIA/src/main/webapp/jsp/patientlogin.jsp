<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" type="image/x-icon" href="./favicon.ico" />
<title><%=MC.M_VelosEres_PatLogin%><%--Velos eResearch : <%=LC.Pat_Patient%> login*****--%></title>
<!-- Gopu : import the objects for customized filed -->
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*,com.velos.eres.web.user.ConfigFacade" %>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>


<%
	Configuration.readSettings();

	//modifed by Gopu for MAHI Enahncement on 3rd Mar 2005
//	ConfigFacade.getConfigFacade();

%>
<meta name="Keywords" content="" />
<link href="styles/login.css" rel="stylesheet" type="text/css" />
<!--[if IE]><link rel="stylesheet" href="styles/login_ie.css" type="text/css" /><![endif]-->
<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

</head>
<script>
function openwin1() {
      window.open("loggingin.htm","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=600,height=500")
;}
</script>
<%
String scHeight = "";
String scWidth = "";
String appver = acmod.getControlValue("app_version");
String siteAdmin = acmod.getControlValue("eresuser");


scHeight = request.getParameter("scHeight");
scWidth = request.getParameter("scWidth");

if (scHeight == null)
	{
		scHeight = "600";
		scWidth = "800";
		}

%>
<body>
<SCRIPT LANGUAGE="JavaScript">
<!--
function OpenCertDetails()
	{
	thewindow = window.open("https://www.thawte.com/cgi/server/certdetails.exe?code=USVELO5-2", "anew", config="height=400,width=450,toolbar=no,menubar=no,scrollbars=yes,resizable=no,location=no,directories=no,status=yes");
	}
// -->
</SCRIPT>
<%String mode=request.getParameter("mode");
mode=(mode==null)?"":mode;
String logName="";
String logUrl="";


if (mode.equals("popup"))
{
 logName=request.getParameter("logname");
 logName=(logName==null)?"":logName;
 logUrl=request.getParameter("logurl");
 logUrl=(logUrl==null)?"":logUrl;

 logUrl="user="+logName+",url="+logUrl;
}

%>

<div class="wrapper">

<% if (!mode.equals("popup")) { %><div class="helix"></div><% } %>
   
<div style="border-bottom: 2px solid #2F7E9E; box-shadow: 10px 10px 100px gray;" class="header">
    <div class="logo">
     <a href= "http://www.velos.com"><img src="images/logo.jpg" alt="<%=LC.L_Velos%><%--Velos*****--%>" title="<%=LC.L_Velos%><%--Velos*****--%>"></a>
	</div>
 </div> 
   
   
    
<form name="login" method="post" action="loginpatient.jsp?&scWidth=<%=StringUtil.htmlEncodeXss(scWidth)%>&scHeight=<%=StringUtil.htmlEncodeXss(scHeight)%>">

<div class="clr"></div>
<%	 if (mode.length()==0){%>
    <div class="clr"></div>
	
<div class="clr"></div>
<div class="border">
<div class="welcome">
    	<%=MC.M_WelcomeVelos_Participant%>
</div>
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td  class="wrapper"><div class="bottom-panel"> 
	<div class="container">
  
    	<div class="lt-btm-panel">
        	<table width="331" border="0" cellspacing="0" cellpadding="0" class="tbbg">
  
  <tr>
    <td colspan="3" valign="top" class="bg-white">
    <table width="90%" border="0" align="center" cellpadding="2" cellspacing="2">
      <tr>
      
        <td width="75" height="40"><input autocomplete="off"  type="text" name="username" id="textfield" class="input1" placeholder= "Username" /></td>
      </tr>
      <tr>
       
        <td height="40"><input autocomplete="off"  type="password" name="password" id="textfield2" class="input2" placeholder="Password" /></td>
      </tr>
      <tr>
      	<!--Bug#9648 23-May-2012 -Sudhir-->
        <td height="30" colspan="2" valign="top"><!--<input type="submit" class="ui-button" value="<%=LC.L_Login%>" role="button" aria-disabled="false">--><button type="submit" style="height: 40px; text-align: center;"><%=LC.L_Login%></button></td>
        </tr>
      </table>
    </td>
    </tr>
          </table>
<!--<div class="copyright">Copyright &copy; 2012 Velos Inc. All Rights Reserved.</div>-->

</div>
       
    </div>
</div></td>
  </tr>
</table>
<%}else if (mode.equals("popup")){%>
<table width="60%" align="center">
<tr>
<td width="5%"></td>
<td width="90%"><p><%=MC.M_AppLogPwd_BlkToolIePop %><%-- Application was unable to open a new window. This <FONT color=red><B>possibly</b></font>,can happen because 

of a <FONT color=red><B>Popup Blocker</B></FONT>.
A good way to verify if this is the issue is to do the following:<br>
<br>
1. Enter your login and password.<br>
2. Hold the *Ctrl* key down while clicking with the mouse on the &quot;Login&quot; <br>
button.<br>
<br>
This will allow the pop-up window to open. Please keep in mind that our program
utilizes pop-up windows frequently, so you will need to locate the pop-up
blocker being used on your computer and either disable it or allow pop-ups from
the program.<br>
<br>
If you have a third party tool bar, such as Yahoo!, Google, etc., these do have
the capability to block pop-ups. You should be able to see the pop-up manager
icon within the tool bar. You'll need to allow the URL to have pop-up windows.<br>
<br>
The latest version of Netscape (7.2) has a pop-up blocker, which can be managed
either in the Netscape Tool bar, or in the &quot;Tools&quot; menu option.<br>
<br>
Here is some information regarding pop-up windows in Internet Explorer. <br>
In the Windows XP Service Pack 2, there is a pop-up manager. If your computer
has had this service pack, your Internet Explorer will have &quot;Pop-up Manager&quot;
under the &quot;Tools&quot; menu in the top bar. Select this option and it should walk you
through the steps for allowing pop-up windows for specific sites. Previous
versions of Internet Explorer do not have the capability to block pop-up
windows.*****--%></p>
<P> <%=MC.M_OnceVerifyInfo_Click %><%-- Once you verify this information, please click here to*****--%> <A href="patientlogin.jsp"><B><%=LC.L_Login %><%-- Login*****--%></b></A>.<br>
<%=MC.M_IfStillLogin_PlsCont %><%-- If you are still unable to login, please contact*****--%> <A href="mailto:customersupport@veloseresearch.com?subject=[Login Issue,<%=StringUtil.htmlEncodeXss(logUrl)%>]">
<B><%=MC.M_VelosEres_CustSupp%><%--Velos eResearch Customer Support*****--%></B></A>.
</P>
</td><td width="40%"></td>
</tr>
</table>
 <!-- LIBRARY OBJECT START-->
	  	      <td width="1" bgcolor="9999ff"><img src="./images/sspacer.gif" width="1">
		      </td>
		   	   <!-- LIBRARY OBJECT END-->
<%}%>
<div class="clr"></div>
<div class="ques"><a href="mailto:customersupport@veloseresearch.com" title="">Need help?</a></div>
<div class="version"><%=appver%><%-- <%="Ver "+appver%>*****--%></div>
</div>
<div class="clr"></div>

<div style="background: gray;" class="footer">
	<div style="color: white;" class="copyright"><%=MC.M_CpyrgtVelos_RgtRerv%><%-- Copyright &copy; 2015 Velos Inc. All Rights Reserved.*****--%></div>
	<div style="color: white;" class="suggestion"><%=MC.M_FfoxIe_ScrnReso%><%--Firefox 4.0, Firefox 7.0, Internet Explorer 8.0. | Screen Resolution 1024x768 and 1280x1024*****--%></div>

</div>


</form>

<script>
	var txtBox=document.getElementById("textfield" );
	if (txtBox!=null ) txtBox.focus();
</script>

</body>
</html>
