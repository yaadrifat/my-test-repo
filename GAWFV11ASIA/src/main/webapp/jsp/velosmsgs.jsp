<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<html>

<head>


<title><%=MC.M_Pending_MsgsInEres%><%--Pending messages in eResearch*****--%></title>



<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>

<SCRIPT language="javascript">
function confirmBox(fileName,type,pgRight) {
		msg= MC.M_WillDelAcc_Cont;/*msg="This will delete the Account. Continue?";*****/
 
	if (confirm(msg)) 
	{
    	return true;
	}
	else
	{
		return false;
	}
}

function fopen(link)
{
// alert(link);
	window.open(link,'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=400,height=280')
	return true;
}


</SCRIPT> 




<body>

<jsp:useBean id="msgcntrB" scope="session" class="com.velos.eres.web.msgcntr.MsgcntrJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="ctrlDao" scope="request" class="com.velos.eres.business.common.CtrlDao"/>

<%@ page language = "java" import = "com.velos.eres.business.msgcntr.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.Configuration,com.velos.eres.service.util.*,java.text.*"%>



<%

String lname = request.getParameter("lname");
String fname = request.getParameter("fname");
String uStat = request.getParameter("userStatus");

if(lname == null) lname = "";
if(fname == null) fname = "";
if(uStat == null) uStat = "P";

lname = lname.trim();
fname = fname.trim();
uStat = uStat.trim();

//   MsgsPendingValidationStateKeeper msk = null;

   MsgCntrDao msgcntrDao = new MsgCntrDao();

   ArrayList msgIds;

   ArrayList msgStats;

   ArrayList userIds;

   ArrayList userNames;

   ArrayList userStats;

   ArrayList createdOns;
   
   ArrayList accTypes;
   
   ArrayList userCodes;
   
   ArrayList siteNames;
   
   ArrayList accIds;

   msgIds = new ArrayList();

   msgStats = new ArrayList();

   userIds = new ArrayList();

   userNames = new ArrayList();

   userStats = new ArrayList();

   createdOns = new ArrayList();
   
   accTypes = new ArrayList();
   
   accIds = new ArrayList();

   userCodes = new ArrayList();
   
   siteNames = new ArrayList();

//   msk = msgcntrB.findByMsgsPendingValidation();

   

   Configuration conf = new Configuration();

   String velosUserId = conf.getVelosAdminId(conf.ERES_HOME +"eresearch.xml");
     
   
   msgcntrDao = msgcntrB.getUserMsgsWithSearch(EJBUtil.stringToNum(velosUserId), lname, fname, uStat);



   msgIds = msgcntrDao.getMsgcntrIds();

   msgStats = msgcntrDao.getMsgcntrStatuses();

   userIds = msgcntrDao.getMsgcntrFromUserIds();

   userNames = msgcntrDao.getUserFroms();

   userStats = msgcntrDao.getUserStatus();

   createdOns = msgcntrDao.getCreateOns();

   accTypes = msgcntrDao.getAccType();
   
   userCodes = msgcntrDao.getUserCode();
   
   siteNames  = msgcntrDao.getUserSite();

  accIds = msgcntrDao.getUserAccIds();

   int len = userIds.size();

   int counter = 0;



   int msgId = 0;

   String msgStatus = "";

   String userName = "";

   String userStatus = "";

   String userId = "";

   String createdOn = "";
   
   String accType = "";
   
   String accId = "";
   
   String userCode = "";
   
   String siteName = "";

   String link="";	

   String pagename = "accountcreation.jsp";

%>



<br>

<DIV class="browserDefault_veloshome" id="div1">


<% 
HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))

	{
	String version = "";		
	
   	//get the application version      
   	ctrlDao.getControlValues("app_version");
   	int rows = ctrlDao.getCRows();
   	if (rows > 0)
    {
   	   	version = (String) ctrlDao.getCValue().get(0);
    }	
%>
<P class="blackComments" align="right"><B><%Object[] arguments1 = {version}; %><%=VelosResourceBundle.getLabelString("L_Ver_Hyp",arguments1)%><%--Version - <%=version%>*****--%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</B></P>

<P class = "defComments">

<%=MC.M_Usr_WhoSentMsgs%><%--Following are the users who have sent messages*****--%>

</P>
<form method="post" action="velosmsgs.jsp">
<table width="100%">
<tr>
	<td colspan="4"> 
		<P class = "defComments"> 
			<%=LC.L_Filter%><%--Filter*****--%> 
		</P>
	</td>
</tr>
<tr>
	<td width="25%"> <%=LC.L_User_Last_Name%><%--User Last Name*****--%> </td>
	<td width="25%"> <%=LC.L_User_FirstName%><%--User First Name*****--%> </td>
	<td width="25%"> <%=LC.L_User_Status%><%--User Status*****--%> </td>
	<td width="25%"> </td>
</tr>
<tr>	<!-- km - to fix Bug#2308 -->
	<td> <input name=lname value="<%=lname%>"> </td>
	<td> <input name=fname value="<%=fname%>"> </td>
	<td> 
		<select name=userStatus>
			<option value="All"  
				<%	
					if(uStat.equals("All")) out.print(LC.L_Selected_Lower);/*out.print("selected");*****/
				%> 
			> <%=LC.L_All%><%--All*****--%> </option>
			<option value="P" 
				<%	
					if(uStat.equals("P")) out.print(LC.L_Selected_Lower);/*out.print("selected");*****/
				%> 
			> <%=LC.L_Pending%><%--Pending*****--%> </option>
			<option value="A" 
				<%	
					if(uStat.equals("A")) out.print(LC.L_Selected_Lower);/*out.print("selected");*****/
				%> 
			> <%=LC.L_Activated%><%--Activated*****--%> </option>
			<option value="D"  
				<%	
					if(uStat.equals("D")) out.print(LC.L_Selected_Lower);/*out.print("selected");*****/
				%> 
			> <%=LC.L_Deactivated%><%--Deactivated*****--%> </option>
			<option value="J"  
				<%	
					if(uStat.equals("J")) out.print(LC.L_Selected_Lower);/*out.print("selected");*****/
				%> 
			> <%=LC.L_Rejected%><%--Rejected*****--%> </option>
		</select> 
	</td>
	<td><button type="submit"><%=LC.L_Search%></button></td>
</tr>
<tr>
	<td colspan="4"> 
			<%=LC.L_Filtered_Count%><%--Filtered Count*****--%>: <%=len%>   
	</td>
</tr>
</table>
</form>

<Form name="groupbrowser" method="post" action="" onsubmit="">
<table width="100%" >

 <tr>

   <th width="15%"> 
	<%=LC.L_User_Name%><%--User Name*****--%>

   </th>
   
   <th width="15%">

	<%=LC.L_User_Id%><%--User ID*****--%>

   </th>
   

   <th width="10%">

	<%=LC.L_Req_Date%><%--Requested Date*****--%>

   </th>

   <th  width="10%">

	<%=LC.L_User_Status%><%--User Status*****--%>

   </th>
   
   <th  width="10%">

	<%=LC.L_Account_Type%><%--Account Type*****--%>

   </th>
   
   <th  width="20%">

	<%=LC.L_Organization%><%--Organization*****--%>

   </th>
   <th  width="10%">
   </th>

   </th>
   <th  width="10%">
   <%=LC.L_Reset_Pwd%><%--Reset Password*****--%>	
   </th>
  </tr>


 <%

	for(counter = 0;counter<len;counter++)

	{	

	msgId = ((Integer) msgIds.get(counter)).intValue();

	

	createdOn = DateUtil.dateToString(java.sql.Date.valueOf(createdOns.get(counter).toString().substring(0,10)));



	msgStatus=((msgStats.get(counter)) == null)?"-":(msgStats.get(counter)).toString();

	if (msgStatus.equals("R")) {

		msgStatus=LC.L_Read/*Read*****/;

	}

	else if (msgStatus.equals("U"))  {

		msgStatus=LC.L_Unread/*UnRead*****/;

	}



	userId=((userIds.get(counter)) == null)?"-":(userIds.get(counter)).toString();



 userName=((userNames.get(counter))==null)?"-":(userNames.get(counter)).toString();

   userStatus=((userStats.get(counter)) == null)?"-":(userStats.get(counter)).toString();


	if (userStatus.equals("A")) {

		userStatus=LC.L_Activated/*Activated*****/;

	}

	else if (userStatus.equals("D"))  {

		userStatus=LC.L_Deactivated/*Deactivated*****/;

	}

	else if (userStatus.equals("R"))  {

		userStatus=LC.L_Reactivated/*Reactivated*****/;

	}

	else if (userStatus.equals("P"))  {

		userStatus=LC.L_Pending/*Pending*****/;

	}
	else if (userStatus.equals("J"))  {

		userStatus=LC.L_Rejected/*Rejected*****/;

	}
	
	accType = ((accTypes.get(counter)) == null)?"-":(accTypes.get(counter)).toString();
	
	userCode = ((userCodes.get(counter)) == null)?"-":(userCodes.get(counter)).toString();
	
	siteName = ((siteNames.get(counter)) == null)?"-":(siteNames.get(counter)).toString();
	
	accId = ((accIds.get(counter)) == null)?"-":(accIds.get(counter)).toString();
	if(accType.equals("I")) {
		accType = LC.L_Individual/*Individual*****/;
	} else if(accType.equals("E")) {
		accType = LC.L_Evaluation/*Evaluation*****/;
	} else if(accType.equals("G")) {
		accType = LC.L_Group/*Group*****/;
	}
	
%>

<%

	if ((counter%2)==0) {

%>

	

		<tr class="browserEvenRow">	

  <%

	}

	else{

  %>

		

			<tr class="browserOddRow">	

  <%

	}

  %>



		<td>

	<A HREF="<%=pagename%>?userId=<%=userId%>&mode=M&msgId=<%=msgId%>"><%=userName%></A> 

		</td>

		<td>
		<%=userCode%>
		</td>
		<td>

		  <%=createdOn%>

		</td>

		<td>

		 <%= userStatus%>
		 
		 <%		
	if (userStatus.equals("Deactivated"))  {
		%>
		<A href = "activateuservel.jsp?userId=<%=userId%>">
		<%=LC.L_Reactivate%><%--Reactivate*****--%>
		</A>
		<%
		}
	%>	


		</td>

		<td>
<%
	if(accType.equals("Group")) {
%>				
			<A HREF="accountUsers.jsp?accId=<%=accId%>"><%=accType%></A>
<%			
	} else {
%>
			<%=accType%>
<%
	}	
%>	
							
		</td>
		<td>
			<%=siteName%>
		</td>
		<td>
<%		
	if (userStatus.equals("Deactivated"))  {
%>
		<A href="accountdelete.jsp?userId=<%=userId%>&lname=<%=lname%>&fname=<%=fname%>&uStat=<%=uStat%>" onClick="return confirmBox();" ><%=LC.L_Delete%><%--Delete*****--%></A>
<%
	}
%>	
		</td>
<td>

<%		 link ="resetpass.jsp?userId=" + userId + "&fromPage=Velos";  %>
		<A href=# title="<%=LC.L_Change_Pwd%><%--Change Password*****--%>"  onClick='return fopen("<%=link%>")'><%=LC.L_Reset%><%--Reset*****--%></A>
		</td>

		</tr>



<%

		}

%>





</table>

</Form>
<%
}//end of if body for session

else

{
%>
  <jsp:include page="timeout_admin.html" flush="true"/>
  <%
}

%>

<div>
<jsp:include page="bottompanel.jsp" flush="true"/> 

</div>

</div>


	<jsp:include page="velosmenus.htm" flush="true"/> 


</body>

</html>
