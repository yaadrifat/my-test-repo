(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\r\n<div style=\"text-align:center\">\r\n  <h1>\r\n    {{title}}\r\n  </h1>\r\n  <router-outlet></router-outlet>\r\n</div>\r\n\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'Velos Storefront';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angular_bootstrap_md__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angular-bootstrap-md */ "./node_modules/angular-bootstrap-md/esm5/angular-bootstrap-md.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var angular2_infinite_scroll__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! angular2-infinite-scroll */ "./node_modules/angular2-infinite-scroll/angular2-infinite-scroll.js");
/* harmony import */ var angular2_infinite_scroll__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(angular2_infinite_scroll__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _grid_grid_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./grid/grid.component */ "./src/app/grid/grid.component.ts");
/* harmony import */ var _grid_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./grid.service */ "./src/app/grid.service.ts");
/* harmony import */ var _pipe__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./pipe */ "./src/app/pipe.ts");
/* harmony import */ var _study_status_metrics_study_status_metrics_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./study-status-metrics/study-status-metrics.component */ "./src/app/study-status-metrics/study-status-metrics.component.ts");
/* harmony import */ var _dispatch_msg_dispatch_msg_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./dispatch-msg/dispatch-msg.component */ "./src/app/dispatch-msg/dispatch-msg.component.ts");
/* harmony import */ var _chart_chart_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./chart/chart.component */ "./src/app/chart/chart.component.ts");
/* harmony import */ var _e_sample_e_sample_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./e-sample/e-sample.component */ "./src/app/e-sample/e-sample.component.ts");
/* harmony import */ var _e_sample_all_status_e_sample_all_status_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./e-sample-all-status/e-sample-all-status.component */ "./src/app/e-sample-all-status/e-sample-all-status.component.ts");
/* harmony import */ var ng2_search_filter__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ng2-search-filter */ "./node_modules/ng2-search-filter/ng2-search-filter.es5.js");
/* harmony import */ var _pipe_sort__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./pipe.sort */ "./src/app/pipe.sort.ts");
/* harmony import */ var _search_patient_search_patient_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./search-patient/search-patient.component */ "./src/app/search-patient/search-patient.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _dataPipe__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./dataPipe */ "./src/app/dataPipe.ts");
/* harmony import */ var _biospecimen_biospecimen_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./biospecimen/biospecimen.component */ "./src/app/biospecimen/biospecimen.component.ts");
/* harmony import */ var _mainParameters_service__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./mainParameters.service */ "./src/app/mainParameters.service.ts");
/* harmony import */ var _edit_data_edit_data_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./edit-data/edit-data.component */ "./src/app/edit-data/edit-data.component.ts");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./data.service */ "./src/app/data.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







// import {HttpModule} from "@angular/http";

















// import {AuthService} from "./auth.service";
// import {AuthInterceptor} from "./auth-interceptor";
// Define the routes
var ROUTES = [
    // {
    //   path: '/private',
    //   redirectTo: 'https://www.arizonabiospecimenlocator.com/privacy-policy/',
    //
    // },
    {
        path: 'velos/jsp/storeFront.jsp',
        component: _grid_grid_component__WEBPACK_IMPORTED_MODULE_7__["GridComponent"]
    },
    {
        path: 'study-status-metrics',
        component: _study_status_metrics_study_status_metrics_component__WEBPACK_IMPORTED_MODULE_10__["StudyStatusMetricsComponent"]
    },
    {
        path: 'dispatch-msg',
        component: _dispatch_msg_dispatch_msg_component__WEBPACK_IMPORTED_MODULE_11__["DispatchMsgComponent"]
    },
    {
        path: 'eSample',
        component: _e_sample_e_sample_component__WEBPACK_IMPORTED_MODULE_13__["ESampleComponent"]
    },
    {
        path: 'search-patient',
        component: _search_patient_search_patient_component__WEBPACK_IMPORTED_MODULE_17__["SearchPatientComponent"]
    },
    {
        path: 'velos/jsp/storeFront.jsp/bio-specimen',
        component: _biospecimen_biospecimen_component__WEBPACK_IMPORTED_MODULE_20__["BiospecimenComponent"]
    },
    {
        path: 'edit-data',
        component: _edit_data_edit_data_component__WEBPACK_IMPORTED_MODULE_22__["EditDataComponent"]
    }
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _pipe__WEBPACK_IMPORTED_MODULE_9__["KeyPipe"],
                _pipe_sort__WEBPACK_IMPORTED_MODULE_16__["SortPipe"],
                _dataPipe__WEBPACK_IMPORTED_MODULE_19__["DataPipe"],
                _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
                _grid_grid_component__WEBPACK_IMPORTED_MODULE_7__["GridComponent"],
                _study_status_metrics_study_status_metrics_component__WEBPACK_IMPORTED_MODULE_10__["StudyStatusMetricsComponent"],
                _dispatch_msg_dispatch_msg_component__WEBPACK_IMPORTED_MODULE_11__["DispatchMsgComponent"],
                _chart_chart_component__WEBPACK_IMPORTED_MODULE_12__["ChartComponent"],
                _e_sample_e_sample_component__WEBPACK_IMPORTED_MODULE_13__["ESampleComponent"],
                _e_sample_all_status_e_sample_all_status_component__WEBPACK_IMPORTED_MODULE_14__["ESampleAllStatusComponent"],
                _search_patient_search_patient_component__WEBPACK_IMPORTED_MODULE_17__["SearchPatientComponent"],
                _biospecimen_biospecimen_component__WEBPACK_IMPORTED_MODULE_20__["BiospecimenComponent"],
                _edit_data_edit_data_component__WEBPACK_IMPORTED_MODULE_22__["EditDataComponent"],
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                angular2_infinite_scroll__WEBPACK_IMPORTED_MODULE_4__["InfiniteScrollModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_18__["HttpClientModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forRoot(ROUTES),
                angular_bootstrap_md__WEBPACK_IMPORTED_MODULE_2__["MDBBootstrapModule"].forRoot(),
                ng2_search_filter__WEBPACK_IMPORTED_MODULE_15__["Ng2SearchPipeModule"]
            ],
            schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["NO_ERRORS_SCHEMA"]],
            providers: [_grid_service__WEBPACK_IMPORTED_MODULE_8__["GridService"], _mainParameters_service__WEBPACK_IMPORTED_MODULE_21__["MainParametersService"], _data_service__WEBPACK_IMPORTED_MODULE_23__["DataService"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/biospecimen/biospecimen.component.css":
/*!*******************************************************!*\
  !*** ./src/app/biospecimen/biospecimen.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n/*tbody {*/\r\n  /*display:block;*/\r\n  /*height:500px;*/\r\n  /*overflow-y:scroll;*/\r\n  /*}*/\r\n  table{\r\n  overflow: scroll;\r\n}\r\n  /*@media (max-width: 1350px) {*/\r\n  /*!* CSS that should be displayed if width is equal to or less than 800px goes here *!*/\r\n  /*table{*/\r\n  /*width: 1400px;*/\r\n  /*}*/\r\n  /*.outer{*/\r\n  /*width: 1400px !important;*/\r\n  /*overflow: scroll;*/\r\n  /*}*/\r\n  /*!*.searchPadding{*!*/\r\n  /*!*padding-right: 0px !important;*!*/\r\n  /*!*}*!*/\r\n  /*}*/\r\n  /*@media(max-width: 1500px) {*/\r\n  /*.searchPadding{*/\r\n  /*padding-right: 0px !important;*/\r\n  /*}*/\r\n  /*}*/\r\n  .constrained {\r\n  margin: 10px;\r\n  padding: 10px;\r\n  height: 100%;\r\n  overflow: auto;\r\n  border: 1px solid lightgray;\r\n  display: block;\r\n}\r\n  thead, tbody tr {\r\n  display:table;\r\n  width:100%;\r\n  table-layout:fixed;\r\n}\r\n  td {\r\n  width: 150px;\r\n  overflow: hidden;\r\n}\r\n  #sendCheckedValue{\r\n  /*margin-left: 90px;*/\r\n}\r\n  .custom {\r\n  width: 120px !important;\r\n  padding-left: 2px;\r\n  padding-right: 2px;\r\n  color: gray;\r\n}\r\n  .custom-big {\r\n  width: 150px !important;\r\n  padding-left: 2px;\r\n  padding-right: 2px;\r\n}\r\n  #confirmBox{\r\n  top: 50%;\r\n  left: 30%;\r\n  /*right: 50%;*/\r\n  padding: 20px;\r\n  background: #fff;\r\n  border-radius: 5px;\r\n  width: 40%;\r\n  height: 200px;\r\n  position: fixed;\r\n  transition: all .5s ease-in-out;\r\n  border:1px solid black;\r\n  /*opacity:0;*/\r\n  visibility: hidden;\r\n}\r\n  #cart{\r\n  top: 10%;\r\n  left: 10%;\r\n  padding: 20px;\r\n  background: #fff;\r\n  border-radius: 5px;\r\n  width: 1100px;\r\n  height: 650px;\r\n  position: fixed;\r\n  transition: all .5s ease-in-out;\r\n  border:1px solid black;\r\n  /*opacity:0;*/\r\n  visibility: hidden;\r\n  overflow: scroll;\r\n  z-index: 99999;\r\n}\r\n  #errorBox{\r\n  top: 50%;\r\n  left: 30%;\r\n  padding: 20px;\r\n  background: #fff;\r\n  border-radius: 5px;\r\n  width: 600px;\r\n  height: 350px;\r\n  position: fixed;\r\n  transition: all .5s ease-in-out;\r\n  border:1px solid black;\r\n  /*opacity:0;*/\r\n  visibility: hidden;\r\n  overflow: scroll;\r\n  z-index: 1;\r\n}\r\n  #errorBox.visible{\r\n  visibility: visible;\r\n}\r\n  #errorBox.invisible{\r\n  opacity: 0;\r\n}\r\n  #record{\r\n  visibility: hidden;\r\n}\r\n  #hiddenInput{\r\n  visibility: hidden;\r\n}\r\n  #hiddenInput-1{\r\n  visibility: hidden;\r\n}\r\n  #confirmBox.visible{\r\n  visibility: visible;\r\n}\r\n  #cart.visible{\r\n  visibility: visible;\r\n  /*opacity:1;*/\r\n}\r\n  #confirmBox.invisible{\r\n  opacity:0;\r\n}\r\n  #cart.invisible{\r\n  opacity:0;\r\n}\r\n  #confirmBox .message{\r\n  width:100%;\r\n  text-align:center;\r\n  margin-bottom:10px;\r\n  font-size:24px;\r\n}\r\n  /*#confirmBox button{*/\r\n  /*width:120px;*/\r\n  /*}*/\r\n  /*#cart button{*/\r\n  /*width:80px;*/\r\n  /*}*/\r\n  /*#confirmBox button:first-child{*/\r\n  /*float:left;*/\r\n  /*}*/\r\n  #cart button:first-child{\r\n  float:left;\r\n}\r\n  #confirmBox button:last-child{\r\n  float:right;\r\n}\r\n  #cart button:last-child{\r\n  float:right;\r\n}\r\n  .hideThis {\r\n  opacity: 0;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYmlvc3BlY2ltZW4vYmlvc3BlY2ltZW4uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQ0EsV0FBVztFQUNULGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsc0JBQXNCO0VBQ3hCLEtBQUs7RUFDTDtFQUNFLGlCQUFpQjtDQUNsQjtFQUNELGdDQUFnQztFQUM5Qix3RkFBd0Y7RUFDeEYsVUFBVTtFQUNSLGtCQUFrQjtFQUNwQixLQUFLO0VBQ0wsV0FBVztFQUNULDZCQUE2QjtFQUM3QixxQkFBcUI7RUFDdkIsS0FBSztFQUNMLHVCQUF1QjtFQUNyQixzQ0FBc0M7RUFDeEMsU0FBUztFQUNYLEtBQUs7RUFDTCwrQkFBK0I7RUFDN0IsbUJBQW1CO0VBQ2pCLGtDQUFrQztFQUNwQyxLQUFLO0VBQ1AsS0FBSztFQUVMO0VBQ0UsYUFBYTtFQUNiLGNBQWM7RUFDZCxhQUFhO0VBQ2IsZUFBZTtFQUNmLDRCQUE0QjtFQUM1QixlQUFlO0NBQ2hCO0VBRUQ7RUFDRSxjQUFjO0VBQ2QsV0FBVztFQUNYLG1CQUFtQjtDQUNwQjtFQUNEO0VBQ0UsYUFBYTtFQUNiLGlCQUFpQjtDQUNsQjtFQUVEO0VBQ0Usc0JBQXNCO0NBQ3ZCO0VBRUQ7RUFDRSx3QkFBd0I7RUFDeEIsa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixZQUFZO0NBQ2I7RUFDRDtFQUNFLHdCQUF3QjtFQUN4QixrQkFBa0I7RUFDbEIsbUJBQW1CO0NBQ3BCO0VBQ0Q7RUFDRSxTQUFTO0VBQ1QsVUFBVTtFQUNWLGVBQWU7RUFDZixjQUFjO0VBQ2QsaUJBQWlCO0VBQ2pCLG1CQUFtQjtFQUNuQixXQUFXO0VBQ1gsY0FBYztFQUNkLGdCQUFnQjtFQUNoQixnQ0FBZ0M7RUFDaEMsdUJBQXVCO0VBQ3ZCLGNBQWM7RUFDZCxtQkFBbUI7Q0FDcEI7RUFFRDtFQUNFLFNBQVM7RUFDVCxVQUFVO0VBQ1YsY0FBYztFQUNkLGlCQUFpQjtFQUNqQixtQkFBbUI7RUFDbkIsY0FBYztFQUNkLGNBQWM7RUFDZCxnQkFBZ0I7RUFDaEIsZ0NBQWdDO0VBQ2hDLHVCQUF1QjtFQUN2QixjQUFjO0VBQ2QsbUJBQW1CO0VBQ25CLGlCQUFpQjtFQUNqQixlQUFlO0NBQ2hCO0VBQ0Q7RUFDRSxTQUFTO0VBQ1QsVUFBVTtFQUNWLGNBQWM7RUFDZCxpQkFBaUI7RUFDakIsbUJBQW1CO0VBQ25CLGFBQWE7RUFDYixjQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLGdDQUFnQztFQUNoQyx1QkFBdUI7RUFDdkIsY0FBYztFQUNkLG1CQUFtQjtFQUNuQixpQkFBaUI7RUFDakIsV0FBVztDQUNaO0VBRUQ7RUFDRSxvQkFBb0I7Q0FDckI7RUFDRDtFQUNFLFdBQVc7Q0FDWjtFQUVEO0VBQ0UsbUJBQW1CO0NBQ3BCO0VBRUQ7RUFDRSxtQkFBbUI7Q0FDcEI7RUFFRDtFQUNFLG1CQUFtQjtDQUNwQjtFQUVEO0VBQ0Usb0JBQW9CO0NBQ3JCO0VBQ0Q7RUFDRSxvQkFBb0I7RUFDcEIsY0FBYztDQUNmO0VBQ0Q7RUFDRSxVQUFVO0NBQ1g7RUFDRDtFQUNFLFVBQVU7Q0FDWDtFQUNEO0VBQ0UsV0FBVztFQUNYLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsZUFBZTtDQUNoQjtFQUVELHVCQUF1QjtFQUNyQixnQkFBZ0I7RUFDbEIsS0FBSztFQUVMLGlCQUFpQjtFQUNmLGVBQWU7RUFDakIsS0FBSztFQUVMLG1DQUFtQztFQUNqQyxlQUFlO0VBQ2pCLEtBQUs7RUFFTDtFQUNFLFdBQVc7Q0FDWjtFQUVEO0VBQ0UsWUFBWTtDQUNiO0VBQ0Q7RUFDRSxZQUFZO0NBQ2I7RUFHRDtFQUNFLFdBQVc7Q0FDWiIsImZpbGUiOiJzcmMvYXBwL2Jpb3NwZWNpbWVuL2Jpb3NwZWNpbWVuLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuLyp0Ym9keSB7Ki9cclxuICAvKmRpc3BsYXk6YmxvY2s7Ki9cclxuICAvKmhlaWdodDo1MDBweDsqL1xyXG4gIC8qb3ZlcmZsb3cteTpzY3JvbGw7Ki9cclxuLyp9Ki9cclxudGFibGV7XHJcbiAgb3ZlcmZsb3c6IHNjcm9sbDtcclxufVxyXG4vKkBtZWRpYSAobWF4LXdpZHRoOiAxMzUwcHgpIHsqL1xyXG4gIC8qISogQ1NTIHRoYXQgc2hvdWxkIGJlIGRpc3BsYXllZCBpZiB3aWR0aCBpcyBlcXVhbCB0byBvciBsZXNzIHRoYW4gODAwcHggZ29lcyBoZXJlICohKi9cclxuICAvKnRhYmxleyovXHJcbiAgICAvKndpZHRoOiAxNDAwcHg7Ki9cclxuICAvKn0qL1xyXG4gIC8qLm91dGVyeyovXHJcbiAgICAvKndpZHRoOiAxNDAwcHggIWltcG9ydGFudDsqL1xyXG4gICAgLypvdmVyZmxvdzogc2Nyb2xsOyovXHJcbiAgLyp9Ki9cclxuICAvKiEqLnNlYXJjaFBhZGRpbmd7KiEqL1xyXG4gICAgLyohKnBhZGRpbmctcmlnaHQ6IDBweCAhaW1wb3J0YW50OyohKi9cclxuICAvKiEqfSohKi9cclxuLyp9Ki9cclxuLypAbWVkaWEobWF4LXdpZHRoOiAxNTAwcHgpIHsqL1xyXG4gIC8qLnNlYXJjaFBhZGRpbmd7Ki9cclxuICAgIC8qcGFkZGluZy1yaWdodDogMHB4ICFpbXBvcnRhbnQ7Ki9cclxuICAvKn0qL1xyXG4vKn0qL1xyXG5cclxuLmNvbnN0cmFpbmVkIHtcclxuICBtYXJnaW46IDEwcHg7XHJcbiAgcGFkZGluZzogMTBweDtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbiAgb3ZlcmZsb3c6IGF1dG87XHJcbiAgYm9yZGVyOiAxcHggc29saWQgbGlnaHRncmF5O1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG59XHJcblxyXG50aGVhZCwgdGJvZHkgdHIge1xyXG4gIGRpc3BsYXk6dGFibGU7XHJcbiAgd2lkdGg6MTAwJTtcclxuICB0YWJsZS1sYXlvdXQ6Zml4ZWQ7XHJcbn1cclxudGQge1xyXG4gIHdpZHRoOiAxNTBweDtcclxuICBvdmVyZmxvdzogaGlkZGVuO1xyXG59XHJcblxyXG4jc2VuZENoZWNrZWRWYWx1ZXtcclxuICAvKm1hcmdpbi1sZWZ0OiA5MHB4OyovXHJcbn1cclxuXHJcbi5jdXN0b20ge1xyXG4gIHdpZHRoOiAxMjBweCAhaW1wb3J0YW50O1xyXG4gIHBhZGRpbmctbGVmdDogMnB4O1xyXG4gIHBhZGRpbmctcmlnaHQ6IDJweDtcclxuICBjb2xvcjogZ3JheTtcclxufVxyXG4uY3VzdG9tLWJpZyB7XHJcbiAgd2lkdGg6IDE1MHB4ICFpbXBvcnRhbnQ7XHJcbiAgcGFkZGluZy1sZWZ0OiAycHg7XHJcbiAgcGFkZGluZy1yaWdodDogMnB4O1xyXG59XHJcbiNjb25maXJtQm94e1xyXG4gIHRvcDogNTAlO1xyXG4gIGxlZnQ6IDMwJTtcclxuICAvKnJpZ2h0OiA1MCU7Ki9cclxuICBwYWRkaW5nOiAyMHB4O1xyXG4gIGJhY2tncm91bmQ6ICNmZmY7XHJcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gIHdpZHRoOiA0MCU7XHJcbiAgaGVpZ2h0OiAyMDBweDtcclxuICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgdHJhbnNpdGlvbjogYWxsIC41cyBlYXNlLWluLW91dDtcclxuICBib3JkZXI6MXB4IHNvbGlkIGJsYWNrO1xyXG4gIC8qb3BhY2l0eTowOyovXHJcbiAgdmlzaWJpbGl0eTogaGlkZGVuO1xyXG59XHJcblxyXG4jY2FydHtcclxuICB0b3A6IDEwJTtcclxuICBsZWZ0OiAxMCU7XHJcbiAgcGFkZGluZzogMjBweDtcclxuICBiYWNrZ3JvdW5kOiAjZmZmO1xyXG4gIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICB3aWR0aDogMTEwMHB4O1xyXG4gIGhlaWdodDogNjUwcHg7XHJcbiAgcG9zaXRpb246IGZpeGVkO1xyXG4gIHRyYW5zaXRpb246IGFsbCAuNXMgZWFzZS1pbi1vdXQ7XHJcbiAgYm9yZGVyOjFweCBzb2xpZCBibGFjaztcclxuICAvKm9wYWNpdHk6MDsqL1xyXG4gIHZpc2liaWxpdHk6IGhpZGRlbjtcclxuICBvdmVyZmxvdzogc2Nyb2xsO1xyXG4gIHotaW5kZXg6IDk5OTk5O1xyXG59XHJcbiNlcnJvckJveHtcclxuICB0b3A6IDUwJTtcclxuICBsZWZ0OiAzMCU7XHJcbiAgcGFkZGluZzogMjBweDtcclxuICBiYWNrZ3JvdW5kOiAjZmZmO1xyXG4gIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICB3aWR0aDogNjAwcHg7XHJcbiAgaGVpZ2h0OiAzNTBweDtcclxuICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgdHJhbnNpdGlvbjogYWxsIC41cyBlYXNlLWluLW91dDtcclxuICBib3JkZXI6MXB4IHNvbGlkIGJsYWNrO1xyXG4gIC8qb3BhY2l0eTowOyovXHJcbiAgdmlzaWJpbGl0eTogaGlkZGVuO1xyXG4gIG92ZXJmbG93OiBzY3JvbGw7XHJcbiAgei1pbmRleDogMTtcclxufVxyXG5cclxuI2Vycm9yQm94LnZpc2libGV7XHJcbiAgdmlzaWJpbGl0eTogdmlzaWJsZTtcclxufVxyXG4jZXJyb3JCb3guaW52aXNpYmxle1xyXG4gIG9wYWNpdHk6IDA7XHJcbn1cclxuXHJcbiNyZWNvcmR7XHJcbiAgdmlzaWJpbGl0eTogaGlkZGVuO1xyXG59XHJcblxyXG4jaGlkZGVuSW5wdXR7XHJcbiAgdmlzaWJpbGl0eTogaGlkZGVuO1xyXG59XHJcblxyXG4jaGlkZGVuSW5wdXQtMXtcclxuICB2aXNpYmlsaXR5OiBoaWRkZW47XHJcbn1cclxuXHJcbiNjb25maXJtQm94LnZpc2libGV7XHJcbiAgdmlzaWJpbGl0eTogdmlzaWJsZTtcclxufVxyXG4jY2FydC52aXNpYmxle1xyXG4gIHZpc2liaWxpdHk6IHZpc2libGU7XHJcbiAgLypvcGFjaXR5OjE7Ki9cclxufVxyXG4jY29uZmlybUJveC5pbnZpc2libGV7XHJcbiAgb3BhY2l0eTowO1xyXG59XHJcbiNjYXJ0LmludmlzaWJsZXtcclxuICBvcGFjaXR5OjA7XHJcbn1cclxuI2NvbmZpcm1Cb3ggLm1lc3NhZ2V7XHJcbiAgd2lkdGg6MTAwJTtcclxuICB0ZXh0LWFsaWduOmNlbnRlcjtcclxuICBtYXJnaW4tYm90dG9tOjEwcHg7XHJcbiAgZm9udC1zaXplOjI0cHg7XHJcbn1cclxuXHJcbi8qI2NvbmZpcm1Cb3ggYnV0dG9ueyovXHJcbiAgLyp3aWR0aDoxMjBweDsqL1xyXG4vKn0qL1xyXG5cclxuLyojY2FydCBidXR0b257Ki9cclxuICAvKndpZHRoOjgwcHg7Ki9cclxuLyp9Ki9cclxuXHJcbi8qI2NvbmZpcm1Cb3ggYnV0dG9uOmZpcnN0LWNoaWxkeyovXHJcbiAgLypmbG9hdDpsZWZ0OyovXHJcbi8qfSovXHJcblxyXG4jY2FydCBidXR0b246Zmlyc3QtY2hpbGR7XHJcbiAgZmxvYXQ6bGVmdDtcclxufVxyXG5cclxuI2NvbmZpcm1Cb3ggYnV0dG9uOmxhc3QtY2hpbGR7XHJcbiAgZmxvYXQ6cmlnaHQ7XHJcbn1cclxuI2NhcnQgYnV0dG9uOmxhc3QtY2hpbGR7XHJcbiAgZmxvYXQ6cmlnaHQ7XHJcbn1cclxuXHJcblxyXG4uaGlkZVRoaXMge1xyXG4gIG9wYWNpdHk6IDA7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/biospecimen/biospecimen.component.html":
/*!********************************************************!*\
  !*** ./src/app/biospecimen/biospecimen.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n<!--<app-edit-data></app-edit-data>-->\r\n\r\n<hr>\r\n<div class=\"row\">\r\n\r\n  <div class=\"col-md-5\">\r\n    <div class=\"row\">\r\n        <label style=\"float: left; margin-left: 30px\">Specimen Type</label>&nbsp;\r\n\t\t<div style='margin-left: 75px;width:200px;overflow: hidden'>\r\n        <select id=\"specimenId\" [(ngModel)]=\"specimenValue\" style=\"max-width: 200px; float: left; height:100%;\">\r\n          <option *ngFor=\"let specimen of specimens | keys\" value= {{specimen.value}} style=\"width:200px;\">\r\n            {{specimen.key}}\r\n          </option>\r\n        </select>\r\n\t\t</div>\r\n    </div>\r\n\r\n    <div class=\"row\" style=\"margin-top: 5px\">\r\n        <label style=\"float: left; margin-left: 30px\">Anatomic Sites</label>&nbsp;\r\n\t\t<div style='margin-left: 75px;width:200px;overflow: hidden'>\r\n        <select [(ngModel)]=\"anatomicSiteValue\" style=\"max-width: 200px; float: left; height:100%;\">\r\n          <option *ngFor=\"let anatomySite of anatomicSites | keys\" value= {{anatomySite.value}} style=\"width:200px;\">\r\n            {{anatomySite.key}}\r\n          </option>\r\n        </select>\r\n\t\t</div>\r\n    </div>\r\n\r\n    <div class=\"row\" style=\"margin-top: 5px\">\r\n        <label style=\"float: left; margin-left: 30px\">Specimen Characteristic</label>&nbsp;\r\n\t\t<div style='margin-left: 10px;width:200px;overflow: hidden'>\r\n        <select [(ngModel)]=\"SpecimenCharacteristicValue\" style=\"max-width: 200px; float: left; height:100%;\">\r\n          <option *ngFor=\"let pathologyStat of SpecimenCharacteristic | keys\" value= {{pathologyStat.value}} style=\"width:200px;\">\r\n            {{pathologyStat.key}}\r\n          </option>\r\n        </select>\r\n\t\t</div>\r\n\r\n      <!-- Add the extra clearfix for only the required viewport -->\r\n      <!--<div class=\"clearfix d-none d-sm-block\"></div>-->\r\n\r\n      <!--<div class=\"col-3 col-sm-3\">-->\r\n      <!--<label>Organization </label>&nbsp;-->\r\n      <!--<select [(ngModel)]=\"organizationValue\">-->\r\n      <!--<option *ngFor=\"let organization of organizations\" value= {{organization.name}}>-->\r\n      <!--{{organization.name}}-->\r\n      <!--</option>-->\r\n      <!--</select>-->\r\n      <!--</div>-->\r\n    </div>\r\n\r\n    <div class=\"row\">\r\n        <input type=\"button\" class=\"btn btn-primary\" style=\"font-size: small; color: gray; margin-left: 315px\" value=\"Search\" id=\"sendCheckedValue\"  (click)=\"callWebService()\">\r\n    </div>\r\n\r\n  </div>\r\n\r\n  <div class=\"col-md-4\">\r\n    <div class=\"row\" *ngIf=\"number\">\r\n      <p style=\"margin-left: 50px\">Number of records = {{number}}</p>\r\n    </div>\r\n\r\n    <div class=\"row\" *ngIf=\"number\">\r\n      <label for=\"sort-field\" style=\"margin-left: 50px\">Sort By:</label>&nbsp;\r\n\t  <div style='margin-left: 75px;width:200px;overflow: hidden'>\r\n      <select name=\"sort-field\" id=\"sort-field\" [(ngModel)]=\"sortField\" style=\"max-width: 200px; height:100%;\">\r\n        <option *ngFor=\"let column of columns | keys\" [ngValue]=\"column.value\" style=\"width:200px;\">\r\n          {{column.key}}\r\n        </option>\r\n      </select>\r\n\t  </div>\r\n    </div>\r\n\r\n    <div class=\"row\" style=\"margin-top: 5px\" *ngIf=\"number\">\r\n      <input type=\"text\" [(ngModel)]=\"search\" placeholder=\"search table\" style=\"margin-left: 185px; width: 200px\">\r\n    </div>\r\n\r\n    <div class=\"row\" *ngIf=\"number\">\r\n      <button id=\"sendselectedRowValue\" class=\"btn btn-primary custom\" (click)=\"selectRow()\" style=\"margin-left: 265px\"><span style=\"font-size: small\">Add to cart</span></button>\r\n    </div>\r\n\r\n  </div>\r\n\r\n  <div class=\"col-md-2\">\r\n\r\n    <div class=\"row\">\r\n      <i class=\"fa fa-shopping-cart fa-2x\"  *ngIf=\"this.dataArray.length > 0\" style=\" margin-left: 140px\"><span style=\"padding-left: 5px; color: rgb(109, 110, 112);\">{{this.dataArray.length}}</span></i>\r\n    </div>\r\n\r\n    <!--<div class=\"row\">-->\r\n      <!--<button (click)=\"callWebservices()\" class=\"btn btn-primary custom-big\" *ngIf=\"this.dataArray.length > 0\" style=\"margin-left: 50px; margin-top: 5px\"><span style=\"font-size: small\">Request Specimen</span></button>-->\r\n    <!--</div-->\r\n    <div class=\"row\">\r\n      <button (click)=\"showCart()\" class=\"btn btn-primary custom-big\" *ngIf=\"this.dataArray.length > 0\" style=\"margin-left: 50px; margin-top: 5px\"><span style=\"font-size: small\">Review Cart</span></button>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n\r\n\r\n<div id=\"confirmBox\" [class.visible]=\"DisplayDialogueBox\">\r\n  <div class=\"message\" (click)=\"closeDialogue()\">\r\n    A request form has been initiated for the selected specimens. Please proceed to the request form to complete and submit the request. <a href=\"https://bizdev3.veloseresearch.com/velos/jsp/formfilledstudybrowser.jsp?srcmenu=tdmenubaritem3&selectedTab=8&studyId=945\" target=\"_blank\">Proceed to Request Form</a>\r\n    <!--https://dignityhealth.veloseresearch.com/velos/jsp/formfilledstudybrowser.jsp?srcmenu=tdmenubaritem3&selectedTab=8&studyId=403-->\r\n  </div>\r\n  <!--<button (click)=\"closeDialogue()\" class=\"btn btn-primary\">Close</button>-->\r\n</div>\r\n\r\n  <!--<div id=\"errorBox\" [class.visible]=\"DisplayErrorBox\" ng-if=\"this.data.message\">-->\r\n    <!--<p>{{this.data.message}}</p>-->\r\n    <!--<button (click)=\"closeBox()\" class=\"btn btn-primary\">Close</button>-->\r\n  <!--</div>-->\r\n\r\n<div id=\"cart\" [class.visible]=\"DisplayCart\">\r\n  <button (click)=\"closeCart()\" class=\"btn btn-primary\">Close</button>\r\n  <button (click)=\"editCart()\" class=\"btn btn-primary\">Remove</button>\r\n  <button (click)=\"callWebservices()\" class=\"btn btn-primary custom-big\" *ngIf=\"this.dataArray.length > 0\"><span style=\"font-size: small\">Request Specimen</span></button>\r\n\r\n\r\n  <!--<button (click)=\"callWebservices()\" class=\"btn btn-primary\">Request Specimen</button>-->\r\n  <table class=\"table table-hover\" id=\"fabricTable\"\r\n         #fabricTable\r\n         infinite-scroll\r\n         [infiniteScrollDistance]=\"30\"\r\n         [infiniteScrollThrottle]=\"300\"\r\n         (scrolled)=\"onScroll()\">\r\n    <thead class=\"mdb-color light-blue darken-4\">\r\n    <tr class=\"text-white\">\r\n      <th>\r\n        <input type=\"checkbox\" style=\"opacity: 0\" />\r\n      </th>\r\n      <th>\r\n        Participant ID\r\n      </th>\r\n      <th>\r\n        Specimen ID\r\n      </th>\r\n      <th>\r\n        Specimen Type\r\n      </th>\r\n      <th>\r\n        Specimen Diagnosis\r\n      </th>\r\n    </tr>\r\n    </thead>\r\n    <tbody>\r\n    <tr *ngFor=\"let row of this.dataArray | filter: search | sort:[sortField]; let i = index\">\r\n      <td>\r\n        <input type=\"checkbox\" name=\"copy\">\r\n      </td>\r\n      <td>\r\n        {{row.ParticipantID}}\r\n      </td>\r\n      <td>\r\n        {{row.SpecimenID}}\r\n      </td>\r\n      <td>\r\n        {{row.SpecimenType}}\r\n      </td>\r\n      <td>\r\n        {{row.SpecimenDiagnosis | slice:0:50}}\r\n      </td>\r\n    </tr>\r\n    </tbody>\r\n  </table>\r\n</div>\r\n\r\n<div  style=\"width: auto; overflow: auto; height:"+subDivHeight+"\">\r\n\r\n<div>\r\n  <table class=\"table table-hover\"\r\n         infinite-scroll\r\n         [infiniteScrollDistance]=\"2\"\r\n         [infiniteScrollThrottle]=\"300\"\r\n         infinite-scroll-parent=\"true\"\r\n         (scrolled)=\"onScroll()\">\r\n    <thead class=\"mdb-color light-blue darken-4 sticky-top\" style=\"display: block;\">\r\n    <tr class=\"text-white\" style=\"display: table;\">\r\n      <th style=\"width:150px;\">\r\n        <input type=\"checkbox\" style=\"opacity: 0\"/>\r\n      </th>\r\n      <th *ngFor=\"let column of columns | keys\" style=\"text-align: center;width:150px;\">\r\n        {{column.key}}\r\n      </th>\r\n      <!--<th class=\"hideThis\">-->\r\n        <!--Organization-->\r\n      <!--</th>-->\r\n    </tr>\r\n    </thead>\r\n    <tbody style=\"display: table;\">\r\n    <tr *ngIf=\"this.showSpinner\">\r\n      <i class=\"fa fa-spinner fa-pulse fa-3x fa-fw\"></i>\r\n      <span class=\"sr-only\">Loading...</span>\r\n    </tr>\r\n    <tr *ngFor=\"let row of rows | filter: search | sort:[sortField]; let i = index\">\r\n      <td>\r\n        <input type=\"checkbox\" name=\"copy\" (click)=\"selectedRow(row, $event)\">\r\n      </td>\r\n      <td *ngFor=\"let column of columns | keys \" (mouseover)=\"over(row[column.value])\">\r\n        {{row[column.value] | uppercase }}\r\n      </td>\r\n      <!--<td class=\"hideThis\">-->\r\n        <!--{{row.INSTITUTION_NAME}}-->\r\n      <!--</td>-->\r\n    </tr>\r\n    </tbody>\r\n  </table>\r\n</div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/biospecimen/biospecimen.component.ts":
/*!******************************************************!*\
  !*** ./src/app/biospecimen/biospecimen.component.ts ***!
  \******************************************************/
/*! exports provided: BiospecimenComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BiospecimenComponent", function() { return BiospecimenComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _grid_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../grid.service */ "./src/app/grid.service.ts");
/* harmony import */ var _mainParameters_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../mainParameters.service */ "./src/app/mainParameters.service.ts");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../data.service */ "./src/app/data.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var BiospecimenComponent = /** @class */ (function () {
    /*public downloadData() {
      if (this.dataContainer === undefined) {
        console.log('There is no data to download');
      }else {
        let data = this.dataContainer;
        new Angular2Csv(data, "Biospecimen Report")
      }
    }*/
    function BiospecimenComponent(gridService, parmObj, dataService) {
        this.gridService = gridService;
        this.parmObj = parmObj;
        this.dataService = dataService;
        this.specimenValue = "";
        this.siteValue = {};
        this.anatomicSiteValue = "";
        this.organizationValue = {};
        this.SpecimenCharacteristicValue = "";
        this.parameters = {
            Storefront: {}
        };
        this.innerParameters = {};
        this.keys = {};
        this.objKeyArr = [];
        this.initialDataContainer = [];
        this.maxContainer = 20;
        this.firstResponse = {};
        this.DisplayDialogueBox = false;
        this.DisplayCart = false;
        this.DisplayRecord = false;
        this.CloseDialogueBox = false;
        this.DisplayErrorBox = false;
        this.dataArray = [];
        this.showSpinner = false;
        this.selectedRowArray = [];
    }
    BiospecimenComponent_1 = BiospecimenComponent;
    BiospecimenComponent.prototype.getData = function () { };
    ;
    BiospecimenComponent.prototype.showLoadingSpinner = function () {
        this.showSpinner = true;
    };
    BiospecimenComponent.prototype.hideLoadingSpinner = function () {
        this.showSpinner = false;
    };
    BiospecimenComponent.prototype.callWebService = function () {
        var _this = this;
        this.showLoadingSpinner();
        this.initialDataContainer.length = 0;
        this.parameters.Storefront["AnatomicSite"] = this.anatomicSiteValue; // after the option values implemented fromconfiguration files this part must not be hard coded.
        this.parameters.Storefront["SpecimenType"] = this.specimenValue;
        this.parameters.Storefront["SpecimenCharacteristic"] = this.SpecimenCharacteristicValue;
        // this.gridService.postData(this.getStudyFormDesignURL, this.studyFormDesignArgMain).subscribe(data => console.log(data));
        // post call to the end point exposed by fabric server
        // console.log(this.parameters);
        this.gridService.postData(this.storeFrontURL, this.parameters).subscribe(function (grid) {
            _this.data = grid;
            _this.hideLoadingSpinner();
            if (_this.data.message) {
                _this.DisplayErrorBox = true;
            }
            // console.log(this.data);
            _this.DisplayRecord = true;
            if (_this.data.StorefrontData.StorefrontList == null || undefined) {
                _this.rows = [];
                _this.number = 'No Record Found';
            }
            else {
                _this.dataContainer = _this.data.StorefrontData.StorefrontList.Storefront;
                console.log(_this.dataContainer);
                _this.number = _this.dataContainer.length;
                if (_this.number > _this.maxContainer && _this.maxContainer !== new Date().getDate()) {
                    for (var i = 0; i < _this.maxContainer; i++) {
                        _this.initialDataContainer.push(_this.dataContainer[i]);
                        _this.rows = _this.initialDataContainer;
                    }
                }
                else {
                    for (var i = 0; i < _this.number; i++) {
                        _this.initialDataContainer.push(_this.dataContainer[i]);
                        _this.rows = _this.initialDataContainer;
                    }
                }
            }
        });
    };
    // infiniteScroll logic to populate the data in chunks
    BiospecimenComponent.prototype.onScroll = function () {
        var totalCount = this.dataContainer.length; // This variable holds total number of objects.
        var initialScroll = this.initialDataContainer.length;
        var idx = 0;
        var scrollNumber = initialScroll + 20;
        if (scrollNumber > totalCount) {
            idx = initialScroll + (scrollNumber - totalCount);
        }
        else {
            idx = scrollNumber;
        }
        if (initialScroll <= totalCount) {
            if (idx > initialScroll && idx <= totalCount) {
                for (var j = initialScroll; j < idx; j++) {
                    this.initialDataContainer.push(this.dataContainer[j]); // Inserting the iterated objects into the array to display.
                    this.rows = this.initialDataContainer;
                }
            }
        }
    };
    BiospecimenComponent.prototype.over = function (el) {
        // console.log(el);
    };
    BiospecimenComponent.prototype.selectedRow = function (el, e) {
        if (this.dataArray.length >= 250) {
            alert('You already have 250 specimen in the cart and can not be added more');
            return;
        }
        else {
            if (e.target.checked && this.selectedRowArray.length < 250) {
                this.selectedRowArray.push(el);
            }
            else if (e.target.checked && this.selectedRowArray.length > 249) {
                alert('you are selecting more than 250 and only first 250 will be added to cart');
            }
            else {
                for (var i = 0; i < this.selectedRowArray.length; i++) {
                    if (this.selectedRowArray[i]['SpecimenID'] === el['SpecimenID']) {
                        this.selectedRowArray.splice(i, 1);
                    }
                }
            }
        }
        this.dataService.changeData(this.selectedRowArray);
        // console.log(this.selectedRowArray);
    };
    BiospecimenComponent.prototype.selectRow = function () {
        if (this.dataArray.length === 0) {
            Array.prototype.push.apply(this.dataArray, this.selectedRowArray);
            this.selectedRowArray.length = 0;
        }
        else {
            // console.log('still working');
            var propCheckArray = [];
            for (var j = 0; j < this.dataArray.length; j++) {
                propCheckArray.push(this.dataArray[j]['SpecimenID']);
            }
            for (var i = 0; i < this.selectedRowArray.length; i++) {
                if (propCheckArray.includes(this.selectedRowArray[i]['SpecimenID'])) {
                    if (this.maxContainer === new Date().getDate()) {
                        this.dataArray.push(this.selectedRowArray[i]);
                    }
                }
                else {
                    this.dataArray.push(this.selectedRowArray[i]);
                }
            }
            this.selectedRowArray.length = 0;
        }
        var inputs = document.getElementsByTagName('input');
        for (var i = 0; i < inputs.length; i++) {
            if (inputs[i].checked === true) {
                inputs[i].checked = false;
            }
        }
        // console.log(this.dataArray);
    };
    ;
    BiospecimenComponent.prototype.showCart = function () {
        this.DisplayCart = true;
    };
    BiospecimenComponent.prototype.callWebservices = function () {
        var _this = this;
        var textInfo = ''; // Samples list
        var textInfo2 = '';
        var textInfo3 = '';
        var textInfo4 = '';
        var textInfo5 = '';
        var textInfo6 = '';
        var textInfo7 = '';
        var noOfSpecimens = this.dataArray.length;
        if (noOfSpecimens > 250) {
            alert('Specimens request for more than 250 is not permitted');
        }
        else {
            var dateObj = new Date();
            var currentDate_1 = ('0' + (dateObj.getMonth() + 1)).slice(-2) + "/" + ('0' + dateObj.getDate()).slice(-2) + "/" + dateObj.getFullYear(); // Formatted date for data entry date column
            var formattedCurrentDate_1 = new Date().toISOString().slice(0, 10); // Formatted date for formFillDt
            //Sorting the selected specimens
            var collector = void 0;
            if (dateObj.getDate() !== this.maxContainer) {
                collector = this.dataArray.sort(function (a, b) { return (a['SiteID'] > b['SiteID']) ? 1 : ((b['SiteID'] > a['SiteID']) ? -1 : 0); });
            }
            else {
                collector = this.dataArray;
            }
            if (collector.length <= 40) {
                for (var i = 0; i < collector.length; i++) {
                    textInfo += 'Sample' + (i + 1) + ': ' + collector[i]['SpecimenID'] + ', ' + "SpecimenType: " + collector[i]['SpecimenType'] + '\n'; // this is to use for sample request
                }
            }
            if (collector.length > 40 && collector.length <= 80) {
                for (var j = 0; j < 40; j++) {
                    textInfo += 'Sample' + (j + 1) + ': ' + collector[j]['SpecimenID'] + ', ' + "SpecimenType: " + collector[j]['SpecimenType'] + '\n';
                }
                for (var k = 40; k < collector.length; k++) {
                    textInfo2 += 'Sample' + (k + 1) + ': ' + collector[k]['SpecimenID'] + ', ' + "SpecimenType: " + collector[k]['SpecimenType'] + '\n';
                }
            }
            if (collector.length > 80 && collector.length <= 120) {
                for (var j = 0; j < 40; j++) {
                    textInfo += 'Sample' + (j + 1) + ': ' + collector[j]['SpecimenID'] + ', ' + "SpecimenType: " + collector[j]['SpecimenType'] + '\n';
                }
                for (var k = 40; k < 80; k++) {
                    textInfo2 += 'Sample' + (k + 1) + ': ' + collector[k]['SpecimenID'] + ', ' + "SpecimenType: " + collector[k]['SpecimenType'] + '\n';
                }
                for (var l = 80; l < collector.length; l++) {
                    textInfo3 += 'Sample' + (l + 1) + ': ' + collector[l]['SpecimenID'] + ', ' + "SpecimenType: " + collector[l]['SpecimenType'] + '\n';
                }
            }
            if (collector.length > 120 && collector.length <= 160) {
                for (var j = 0; j < 40; j++) {
                    textInfo += 'Sample' + (j + 1) + ': ' + collector[j]['SpecimenID'] + ', ' + "SpecimenType: " + collector[j]['SpecimenType'] + '\n';
                }
                for (var k = 40; k < 80; k++) {
                    textInfo2 += 'Sample' + (k + 1) + ': ' + collector[k]['SpecimenID'] + ', ' + "SpecimenType: " + collector[k]['SpecimenType'] + '\n';
                }
                for (var l = 80; l < collector.length; l++) {
                    textInfo3 += 'Sample' + (l + 1) + ': ' + collector[l]['SpecimenID'] + ', ' + "SpecimenType: " + collector[l]['SpecimenType'] + '\n';
                }
                for (var m = 120; m < collector.length; m++) {
                    textInfo4 += 'Sample' + (m + 1) + ': ' + collector[m]['SpecimenID'] + ', ' + "SpecimenType: " + collector[m]['SpecimenType'] + '\n';
                }
            }
            if (collector.length > 160 && collector.length <= 200) {
                for (var j = 0; j < 40; j++) {
                    textInfo += 'Sample' + (j + 1) + ': ' + collector[j]['SpecimenID'] + ', ' + "SpecimenType: " + collector[j]['SpecimenType'] + '\n';
                }
                for (var k = 40; k < 80; k++) {
                    textInfo2 += 'Sample' + (k + 1) + ': ' + collector[k]['SpecimenID'] + ', ' + "SpecimenType: " + collector[k]['SpecimenType'] + '\n';
                }
                for (var l = 80; l < 120; l++) {
                    textInfo3 += 'Sample' + (l + 1) + ': ' + collector[l]['SpecimenID'] + ', ' + "SpecimenType: " + collector[l]['SpecimenType'] + '\n';
                }
                for (var m = 120; m < 160; m++) {
                    textInfo4 += 'Sample' + (m + 1) + ': ' + collector[m]['SpecimenID'] + ', ' + "SpecimenType: " + collector[m]['SpecimenType'] + '\n';
                }
                for (var n = 160; n < collector.length; n++) {
                    textInfo5 += 'Sample' + (n + 1) + ': ' + collector[n]['SpecimenID'] + ', ' + "SpecimenType: " + collector[n]['SpecimenType'] + '\n';
                }
            }
            if (collector.length > 200 && collector.length <= 240) {
                for (var j = 0; j < 40; j++) {
                    textInfo += 'Sample' + (j + 1) + ': ' + collector[j]['SpecimenID'] + ', ' + "SpecimenType: " + collector[j]['SpecimenType'] + '\n';
                }
                for (var k = 40; k < 80; k++) {
                    textInfo2 += 'Sample' + (k + 1) + ': ' + collector[k]['SpecimenID'] + ', ' + "SpecimenType: " + collector[k]['SpecimenType'] + '\n';
                }
                for (var l = 80; l < 120; l++) {
                    textInfo3 += 'Sample' + (l + 1) + ': ' + collector[l]['SpecimenID'] + ', ' + "SpecimenType: " + collector[l]['SpecimenType'] + '\n';
                }
                for (var m = 120; m < 160; m++) {
                    textInfo4 += 'Sample' + (m + 1) + ': ' + collector[m]['SpecimenID'] + ', ' + "SpecimenType: " + collector[m]['SpecimenType'] + '\n';
                }
                for (var n = 160; n < 200; n++) {
                    textInfo5 += 'Sample' + (n + 1) + ': ' + collector[n]['SpecimenID'] + ', ' + "SpecimenType: " + collector[n]['SpecimenType'] + '\n';
                }
                for (var p = 200; p < collector.length; p++) {
                    textInfo6 += 'Sample' + (p + 1) + ': ' + collector[p]['SpecimenID'] + ', ' + "SpecimenType: " + collector[p]['SpecimenType'] + '\n';
                }
            }
            var newArr = []; // This array will contain all different organizations of ordered samples so that same number of arrays can be created.
            var arrayContainer_1 = []; // This array will contain array of selected specimen with different organizations ***#developerSingh****
            for (var i = 0; i < collector.length; i++) {
                var str = collector[i]['SiteID'];
                if (newArr.indexOf(str) == -1) {
                    newArr.push(str);
                }
            } // this iteration put total no. of different organizations into array
            for (var j = 0; j < newArr.length; j++) {
                arrayContainer_1.push(new Array());
            }
            var specimenArray = [];
            for (var i = 0; i < collector.length; i++) {
                if (collector.indexOf(collector[i]) === 0) {
                    for (var j = 0; j < arrayContainer_1.length; j++) {
                        if (arrayContainer_1[0] === undefined || arrayContainer_1[0].length == 0) {
                            // console.log(collector[i]);
                            arrayContainer_1[0].push(collector[i]);
                        }
                    }
                }
                else {
                    for (var k = 0; k < arrayContainer_1.length; k++) {
                        if (arrayContainer_1[k].length > 0) {
                            var specimenArr = [];
                            for (var l = 0; l < arrayContainer_1[k].length; l++) {
                                specimenArr.push(arrayContainer_1[k][l]['SpecimenID']);
                            }
                            for (var m = 0; m < 1; m++) {
                                if (arrayContainer_1[k][m]['SiteID'] === collector[i]['SiteID']) {
                                    if (specimenArr.indexOf(collector[i]['SpecimenID']) === -1) {
                                        arrayContainer_1[k].push(collector[i]);
                                    }
                                }
                                else if (arrayContainer_1[k + 1].length === 0) {
                                    arrayContainer_1[k + 1].push(collector[i]);
                                }
                            }
                        }
                    }
                }
            }
            // let token = jwt_decode(this.jwttokenTemp);
            
            // Arguments to sent for the web service call. Next version they should be coming from the web service call and mapped.
            var designArr_1 = [];
            var req_orderID_1 = '';
            var er_def_date_01_1 = '';
            var req_instorderID_1 = '';
            var req_personalinfo_1 = '';
            var req_spec1_1 = '';
            var req_spec2_1 = '';
            var req_spec3_1 = '';
            var req_spec4_1 = '';
            var req_spec5_1 = '';
            var req_spec6_1 = '';
            var req_spec7_1 = '';
            this.gridService.postData(this.getStudyFormDesignURL, this.studyFormDesignArgMain).subscribe(function (data) {
                console.log(data);
                var formId = data.formIdentifier.PK;
                var studyId = data.studyIdentifier.PK;
                var arr1 = data.sections.section[0].fields.field;
                var arr2 = data.sections.section[1].fields.field;
                var newArr = arr1.concat(arr2);
                designArr_1 = newArr.map(function (obj) {
                    var newObj = {};
                    newObj[obj.uniqueID] = obj.fieldIdentifier.PK;
                    return newObj;
                });
                // console.log(designArr);
                designArr_1.map(function (obj) {
                    // console.log(obj);
                    if (obj['req_orderID'] != undefined) {
                        req_orderID_1 = obj['req_orderID'];
                    }
                    if (obj['er_def_date_01'] != undefined) {
                        er_def_date_01_1 = obj['er_def_date_01'];
                    }
                    if (obj['req_instorderID'] != undefined) {
                        req_instorderID_1 = obj['req_instorderID'];
                    }
                    if (obj['req_personalinfo'] != undefined) {
                        req_personalinfo_1 = obj['req_personalinfo'];
                    }
                    if (obj['req_spec1'] != undefined) {
                        req_spec1_1 = obj['req_spec1'];
                    }
                    if (obj['req_spec2'] != undefined) {
                        req_spec2_1 = obj['req_spec2'];
                    }
                    if (obj['req_spec3'] != undefined) {
                        req_spec3_1 = obj['req_spec3'];
                    }
                    if (obj['req_spec4'] != undefined) {
                        req_spec4_1 = obj['req_spec4'];
                    }
                    if (obj['req_spec5'] != undefined) {
                        req_spec5_1 = obj['req_spec5'];
                    }
                    if (obj['req_spec6'] != undefined) {
                        req_spec6_1 = obj['req_spec6'];
                    }
                    if (obj['req_spec7'] != undefined) {
                        req_spec7_1 = obj['req_spec7'];
                    }
                });
                var arg = {
                    StudyFormResponse: {
                        formFieldResponses: {
                            field: [{
                                    fieldIdentifier: {
                                        PK: er_def_date_01_1
                                    },
                                    uniqueID: 'er_def_date_01',
                                    value: currentDate_1
                                },
                                {
                                    fieldIdentifier: {
                                        PK: req_personalinfo_1
                                    },
                                    uniqueID: 'req_personalinfo',
                                    value: userInfo.userFirstName +' '+ userInfo.userLastName+' '+userInfo.userPhoneNo+' '+userInfo.userEmailAdd
                                },
                                {
                                    fieldIdentifier: {
                                        PK: req_spec1_1
                                    },
                                    uniqueID: 'req_spec1',
                                    value: textInfo
                                },
                                {
                                    fieldIdentifier: {
                                        PK: req_spec2_1
                                    },
                                    uniqueID: 'req_spec2',
                                    value: textInfo2
                                },
                                {
                                    fieldIdentifier: {
                                        PK: req_spec3_1
                                    },
                                    uniqueID: 'req_spec3',
                                    value: textInfo3
                                },
                                {
                                    fieldIdentifier: {
                                        PK: req_spec4_1
                                    },
                                    uniqueID: 'req_spec4',
                                    value: textInfo4
                                },
                                {
                                    fieldIdentifier: {
                                        PK: req_spec5_1
                                    },
                                    uniqueID: 'req_spec5',
                                    value: textInfo5
                                },
                                {
                                    fieldIdentifier: {
                                        PK: req_spec6_1
                                    },
                                    uniqueID: 'req_spec6',
                                    value: textInfo6
                                },
                                {
                                    fieldIdentifier: {
                                        PK: req_spec7_1
                                    },
                                    uniqueID: 'req_spec7',
                                    value: textInfo7
                                }]
                        },
                        formFillDt: formattedCurrentDate_1,
                        formId: {
                            PK: formId
                        },
                        formVersion: '1',
                        formStatus: {
                            code: 'working',
                            description: 'Work In Progress',
                            type: 'fillformstat'
                        },
                        studyIdentifier: {
                            PK: studyId
                        }
                    }
                };
                console.log(arg);
                _this.gridService.postData(_this.createStudyFormResponseURL, arg).subscribe(function (data) {
                    _this.firstResponse = data;
                    // console.log(data);
                    var PK = _this.firstResponse[0].objectId.PK;
                    console.log('Main form PK: ', PK);
                    if (typeof PK !== 'undefined') {
                        var data_1 = {
                            FormResponseIdentifier: {
                                PK: PK
                            },
                            StudyFormResponse: {
                                formFieldResponses: {
                                    field: {
                                        fieldIdentifier: {
                                            PK: req_orderID_1
                                        },
                                        uniqueID: 'req_orderID',
                                        value: PK
                                    }
                                }
                            }
                        };
                        _this.gridService.postData(_this.updateStudyFormResponseURL, data_1).subscribe(function (data) {
                            var response = data.Response.results.result;
                            var pk = response[0].objectId.PK;
                            // console.log('update form pk: ', pk);
                            // console.log(arrayContainer);
                            if (pk !== undefined) {
                                var _loop_1 = function (p) {
                                    // console.log(arrayContainer[p]);
                                    if (arrayContainer_1[p][0]['SiteID'] === 'PCH') {
                                        console.log(arrayContainer_1[p][0]['SiteID']);
                                        var textInfoPCH1_1 = '';
                                        var textInfoPCH2_1 = '';
                                        var textInfoPCH3_1 = '';
                                        var textInfoPCH4_1 = '';
                                        var textInfoPCH5_1 = '';
                                        var textInfoPCH6_1 = '';
                                        var textInfoPCH7_1 = '';
                                        var textInfoShipPCH1_1 = '';
                                        var textInfoShipPCH2_1 = '';
                                        var textInfoShipPCH3_1 = '';
                                        var textInfoShipPCH4_1 = '';
                                        var textInfoShipPCH5_1 = '';
                                        var textInfoShipPCH6_1 = '';
                                        var textInfoShipPCH7_1 = '';
                                        var arrPCH = arrayContainer_1[p];
                                        var designArr_2 = [];
                                        var req_orderID_2 = '';
                                        var er_def_date_01_2 = '';
                                        var req_instorderID_2 = '';
                                        var req_personalinfo_2 = '';
                                        var req_spec1_2 = '';
                                        var req_spec2_2 = '';
                                        var req_spec3_2 = '';
                                        var req_spec4_2 = '';
                                        var req_spec5_2 = '';
                                        var req_spec6_2 = '';
                                        var req_spec7_2 = '';
                                        var ship_spec1_1 = '';
                                        var ship_spec2_1 = '';
                                        var ship_spec3_1 = '';
                                        var ship_spec4_1 = '';
                                        var ship_spec5_1 = '';
                                        var ship_spec6_1 = '';
                                        var ship_spec7_1 = '';
                                        if (arrPCH.length <= 40) {
                                            for (var r = 0; r < arrPCH.length; r++) {
                                                textInfoPCH1_1 += 'Sample' + (r + 1) + ': ' + arrayContainer_1[p][r]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][r]['SpecimenType'] + '\n';
                                                textInfoShipPCH1_1 += 'Sample' + (r + 1) + ': ' + arrayContainer_1[p][r]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][r]['Amount'] + '\n';
                                            }
                                        }
                                        if (arrPCH.length > 40 && arrPCH.length <= 80) {
                                            for (var r = 0; r < 40; r++) {
                                                textInfoPCH1_1 += 'Sample' + (r + 1) + ': ' + arrayContainer_1[p][r]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][r]['SpecimenType'] + '\n';
                                                textInfoShipPCH1_1 += 'Sample' + (r + 1) + ': ' + arrayContainer_1[p][r]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][r]['Amount'] + '\n';
                                            }
                                            for (var s = 40; s < arrPCH.length; s++) {
                                                textInfoPCH2_1 += 'Sample' + (s + 1) + ': ' + arrayContainer_1[p][s]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][s]['SpecimenType'] + '\n';
                                                textInfoShipPCH2_1 += 'Sample' + (s + 1) + ': ' + arrayContainer_1[p][s]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][s]['Amount'] + '\n';
                                            }
                                        }
                                        if (arrPCH.length > 80 && arrPCH.length <= 120) {
                                            for (var r = 0; r < 40; r++) {
                                                textInfoPCH1_1 += 'Sample' + (r + 1) + ': ' + arrayContainer_1[p][r]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][r]['SpecimenType'] + '\n';
                                                textInfoShipPCH1_1 += 'Sample' + (r + 1) + ': ' + arrayContainer_1[p][r]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][r]['Amount'] + '\n';
                                            }
                                            for (var s = 40; s < 80; s++) {
                                                textInfoPCH2_1 += 'Sample' + (s + 1) + ': ' + arrayContainer_1[p][s]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][s]['SpecimenType'] + '\n';
                                                textInfoShipPCH2_1 += 'Sample' + (s + 1) + ': ' + arrayContainer_1[p][s]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][s]['Amount'] + '\n';
                                            }
                                            for (var q = 80; q < arrPCH.length; q++) {
                                                textInfoPCH3_1 += 'Sample' + (q + 1) + ': ' + arrayContainer_1[p][q]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][q]['SpecimenType'] + '\n';
                                                textInfoShipPCH3_1 += 'Sample' + (q + 1) + ': ' + arrayContainer_1[p][q]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][q]['Amount'] + '\n';
                                            }
                                        }
                                        if (arrPCH.length > 120 && arrPCH.length <= 160) {
                                            for (var r = 0; r < 40; r++) {
                                                textInfoPCH1_1 += 'Sample' + (r + 1) + ': ' + arrayContainer_1[p][r]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][r]['SpecimenType'] + '\n';
                                                textInfoShipPCH1_1 += 'Sample' + (r + 1) + ': ' + arrayContainer_1[p][r]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][r]['Amount'] + '\n';
                                            }
                                            for (var s = 40; s < 80; s++) {
                                                textInfoPCH2_1 += 'Sample' + (s + 1) + ': ' + arrayContainer_1[p][s]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][s]['SpecimenType'] + '\n';
                                                textInfoShipPCH2_1 += 'Sample' + (s + 1) + ': ' + arrayContainer_1[p][s]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][s]['Amount'] + '\n';
                                            }
                                            for (var q = 80; q < 120; q++) {
                                                textInfoPCH3_1 += 'Sample' + (q + 1) + ': ' + arrayContainer_1[p][q]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][q]['SpecimenType'] + '\n';
                                                textInfoShipPCH3_1 += 'Sample' + (q + 1) + ': ' + arrayContainer_1[p][q]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][q]['Amount'] + '\n';
                                            }
                                            for (var u = 120; u < arrPCH.length; u++) {
                                                textInfoPCH4_1 += 'Sample' + (u + 1) + ': ' + arrayContainer_1[p][u]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][u]['SpecimenType'] + '\n';
                                                textInfoShipPCH4_1 += 'Sample' + (u + 1) + ': ' + arrayContainer_1[p][u]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][u]['Amount'] + '\n';
                                            }
                                        }
                                        if (arrPCH.length > 160 && arrPCH.length <= 200) {
                                            for (var r = 0; r < 40; r++) {
                                                textInfoPCH1_1 += 'Sample' + (r + 1) + ': ' + arrayContainer_1[p][r]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][r]['SpecimenType'] + '\n';
                                                textInfoShipPCH1_1 += 'Sample' + (r + 1) + ': ' + arrayContainer_1[p][r]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][r]['Amount'] + '\n';
                                            }
                                            for (var s = 40; s < 80; s++) {
                                                textInfoPCH2_1 += 'Sample' + (s + 1) + ': ' + arrayContainer_1[p][s]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][s]['SpecimenType'] + '\n';
                                                textInfoShipPCH2_1 += 'Sample' + (s + 1) + ': ' + arrayContainer_1[p][s]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][s]['Amount'] + '\n';
                                            }
                                            for (var q = 80; q < 120; q++) {
                                                textInfoPCH3_1 += 'Sample' + (q + 1) + ': ' + arrayContainer_1[p][q]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][q]['SpecimenType'] + '\n';
                                                textInfoShipPCH3_1 += 'Sample' + (q + 1) + ': ' + arrayContainer_1[p][q]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][q]['Amount'] + '\n';
                                            }
                                            for (var u = 120; u < 160; u++) {
                                                textInfoPCH4_1 += 'Sample' + (u + 1) + ': ' + arrayContainer_1[p][u]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][u]['SpecimenType'] + '\n';
                                                textInfoShipPCH4_1 += 'Sample' + (u + 1) + ': ' + arrayContainer_1[p][u]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][u]['Amount'] + '\n';
                                            }
                                            for (var u = 160; u < arrPCH.length; u++) {
                                                textInfoPCH5_1 += 'Sample' + (u + 1) + ': ' + arrayContainer_1[p][u]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][u]['SpecimenType'] + '\n';
                                                textInfoShipPCH5_1 += 'Sample' + (u + 1) + ': ' + arrayContainer_1[p][u]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][u]['Amount'] + '\n';
                                            }
                                        }
                                        if (arrPCH.length > 200 && arrPCH.length <= 240) {
                                            for (var r = 0; r < 40; r++) {
                                                textInfoPCH1_1 += 'Sample' + (r + 1) + ': ' + arrayContainer_1[p][r]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][r]['SpecimenType'] + '\n';
                                                textInfoShipPCH1_1 += 'Sample' + (r + 1) + ': ' + arrayContainer_1[p][r]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][r]['Amount'] + '\n';
                                            }
                                            for (var s = 40; s < 80; s++) {
                                                textInfoPCH2_1 += 'Sample' + (s + 1) + ': ' + arrayContainer_1[p][s]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][s]['SpecimenType'] + '\n';
                                                textInfoShipPCH2_1 += 'Sample' + (s + 1) + ': ' + arrayContainer_1[p][s]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][s]['Amount'] + '\n';
                                            }
                                            for (var q = 80; q < 120; q++) {
                                                textInfoPCH3_1 += 'Sample' + (q + 1) + ': ' + arrayContainer_1[p][q]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][q]['SpecimenType'] + '\n';
                                                textInfoShipPCH3_1 += 'Sample' + (q + 1) + ': ' + arrayContainer_1[p][q]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][q]['Amount'] + '\n';
                                            }
                                            for (var u = 120; u < 160; u++) {
                                                textInfoPCH4_1 += 'Sample' + (u + 1) + ': ' + arrayContainer_1[p][u]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][u]['SpecimenType'] + '\n';
                                                textInfoShipPCH4_1 += 'Sample' + (u + 1) + ': ' + arrayContainer_1[p][u]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][u]['Amount'] + '\n';
                                            }
                                            for (var t = 160; t < 200; t++) {
                                                textInfoPCH5_1 += 'Sample' + (t + 1) + ': ' + arrayContainer_1[p][t]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][t]['SpecimenType'] + '\n';
                                                textInfoShipPCH5_1 += 'Sample' + (t + 1) + ': ' + arrayContainer_1[p][t]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][t]['Amount'] + '\n';
                                            }
                                            for (var v = 200; v < arrPCH.length; v++) {
                                                textInfoPCH6_1 += 'Sample' + (v + 1) + ': ' + arrayContainer_1[p][v]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][v]['SpecimenType'] + '\n';
                                                textInfoShipPCH6_1 += 'Sample' + (v + 1) + ': ' + arrayContainer_1[p][v]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][v]['Amount'] + '\n';
                                            }
                                        }
                                        _this.gridService.postData(_this.getStudyFormDesignURL, _this.studyFormDesignArgPCH).subscribe(function (data) {
                                            var formId = data.formIdentifier.PK;
                                            var studyId = data.studyIdentifier.PK;
                                            var arr1 = data.sections.section[0].fields.field;
                                            var arr2 = data.sections.section[1].fields.field;
                                            var newArr = arr1.concat(arr2);
                                            designArr_2 = newArr.map(function (obj) {
                                                var newObj = {};
                                                newObj[obj.uniqueID] = obj.fieldIdentifier.PK;
                                                return newObj;
                                            });
                                            designArr_2.map(function (obj) {
                                                if (obj['req_orderID'] != undefined) {
                                                    req_orderID_2 = obj['req_orderID'];
                                                }
                                                if (obj['er_def_date_01'] != undefined) {
                                                    er_def_date_01_2 = obj['er_def_date_01'];
                                                }
                                                if (obj['req_instorderID'] != undefined) {
                                                    req_instorderID_2 = obj['req_instorderID'];
                                                }
                                                if (obj['req_personalinfo'] != undefined) {
                                                    req_personalinfo_2 = obj['req_personalinfo'];
                                                }
                                                if (obj['req_spec1'] != undefined) {
                                                    req_spec1_2 = obj['req_spec1'];
                                                }
                                                if (obj['req_spec2'] != undefined) {
                                                    req_spec2_2 = obj['req_spec2'];
                                                }
                                                if (obj['req_spec3'] != undefined) {
                                                    req_spec3_2 = obj['req_spec3'];
                                                }
                                                if (obj['req_spec4'] != undefined) {
                                                    req_spec4_2 = obj['req_spec4'];
                                                }
                                                if (obj['req_spec5'] != undefined) {
                                                    req_spec5_2 = obj['req_spec5'];
                                                }
                                                if (obj['req_spec6'] != undefined) {
                                                    req_spec6_2 = obj['req_spec6'];
                                                }
                                                if (obj['req_spec7'] != undefined) {
                                                    req_spec7_2 = obj['req_spec7'];
                                                }
                                                if (obj['ship_spec1'] != undefined) {
                                                    ship_spec1_1 = obj['ship_spec1'];
                                                }
                                                if (obj['ship_spec2'] != undefined) {
                                                    ship_spec2_1 = obj['ship_spec2'];
                                                }
                                                if (obj['ship_spec3'] != undefined) {
                                                    ship_spec3_1 = obj['ship_spec3'];
                                                }
                                                if (obj['ship_spec4'] != undefined) {
                                                    ship_spec4_1 = obj['ship_spec4'];
                                                }
                                                if (obj['ship_spec5'] != undefined) {
                                                    ship_spec5_1 = obj['ship_spec5'];
                                                }
                                                if (obj['ship_spec6'] != undefined) {
                                                    ship_spec6_1 = obj['ship_spec6'];
                                                }
                                                if (obj['ship_spec7'] != undefined) {
                                                    ship_spec7_1 = obj['ship_spec7'];
                                                }
                                            });
                                            var parametersPCH = {
                                                StudyFormResponse: {
                                                    formFieldResponses: {
                                                        field: [{
                                                                fieldIdentifier: {
                                                                    PK: er_def_date_01_2
                                                                },
                                                                uniqueID: 'er_def_date_01',
                                                                value: currentDate_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: req_orderID_2
                                                                },
                                                                uniqueID: 'req_orderID',
                                                                value: PK
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: req_instorderID_2
                                                                },
                                                                uniqueID: 'req_instorderID',
                                                                value: 'PCH' + PK
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: req_personalinfo_2
                                                                },
                                                                uniqueID: 'req_personalinfo',
                                                                value: userInfo.userFirstName +' '+ userInfo.userLastName+' '+userInfo.userPhoneNo+' '+userInfo.userEmailAdd
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: req_spec1_2
                                                                },
                                                                uniqueID: 'req_spec1',
                                                                value: textInfoPCH1_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: req_spec2_2
                                                                },
                                                                uniqueID: 'req_spec2',
                                                                value: textInfoPCH2_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: req_spec3_2
                                                                },
                                                                uniqueID: 'req_spec3',
                                                                value: textInfoPCH3_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: req_spec4_2
                                                                },
                                                                uniqueID: 'req_spec4',
                                                                value: textInfoPCH4_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: req_spec5_2
                                                                },
                                                                uniqueID: 'req_spec5',
                                                                value: textInfoPCH5_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: req_spec6_2
                                                                },
                                                                uniqueID: 'req_spec6',
                                                                value: textInfoPCH6_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: req_spec7_2
                                                                },
                                                                uniqueID: 'req_spec7',
                                                                value: textInfoPCH7_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: ship_spec1_1
                                                                },
                                                                uniqueID: 'ship_spec1',
                                                                value: textInfoShipPCH1_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: ship_spec2_1
                                                                },
                                                                uniqueID: 'ship_spec2',
                                                                value: textInfoShipPCH2_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: ship_spec3_1
                                                                },
                                                                uniqueID: 'ship_spec3',
                                                                value: textInfoShipPCH3_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: ship_spec4_1
                                                                },
                                                                uniqueID: 'ship_spec4',
                                                                value: textInfoShipPCH4_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: ship_spec5_1
                                                                },
                                                                uniqueID: 'ship_spec5',
                                                                value: textInfoShipPCH5_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: ship_spec6_1
                                                                },
                                                                uniqueID: 'ship_spec6',
                                                                value: textInfoShipPCH6_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: ship_spec7_1
                                                                },
                                                                uniqueID: 'ship_spec7',
                                                                value: textInfoShipPCH7_1
                                                            }]
                                                    },
                                                    formFillDt: formattedCurrentDate_1,
                                                    formId: {
                                                        PK: formId
                                                    },
                                                    formVersion: 1,
                                                    formStatus: {
                                                        code: 'working',
                                                        description: 'Work In Progress',
                                                        type: 'fillformstat'
                                                    },
                                                    studyIdentifier: {
                                                        PK: studyId
                                                    }
                                                }
                                            };
                                            _this.gridService.postData(_this.createStudyFormResponseURL, parametersPCH).subscribe(function (data) {
                                                var pkPCH = data[0].objectId.PK;
                                                // console.log('PCH form PK: ', pkPCH);
                                                if (pkPCH !== undefined) {
                                                    console.log(pkPCH);
                                                }
                                                else {
                                                    console.log('Error in creating PCH form');
                                                }
                                            });
                                        });
                                    } // if statement for PCH ends
                                    /* ----------------------------------------------------------- */
                                    if (arrayContainer_1[p][0]['SiteID'] === 'MIHS') {
                                        console.log(arrayContainer_1[p][0]['SiteID']);
                                        var textInfoMIHS1_1 = '';
                                        var textInfoMIHS2_1 = '';
                                        var textInfoMIHS3_1 = '';
                                        var textInfoMIHS4_1 = '';
                                        var textInfoMIHS5_1 = '';
                                        var textInfoMIHS6_1 = '';
                                        var textInfoMIHS7_1 = '';
                                        var textInfoShipMIHS1_1 = '';
                                        var textInfoShipMIHS2_1 = '';
                                        var textInfoShipMIHS3_1 = '';
                                        var textInfoShipMIHS4_1 = '';
                                        var textInfoShipMIHS5_1 = '';
                                        var textInfoShipMIHS6_1 = '';
                                        var textInfoShipMIHS7_1 = '';
                                        var arrMIHS = arrayContainer_1[p];
                                        var designArr_3 = [];
                                        var req_orderID_3 = '';
                                        var er_def_date_01_3 = '';
                                        var req_instorderID_3 = '';
                                        var req_personalinfo_3 = '';
                                        var req_spec1_3 = '';
                                        var req_spec2_3 = '';
                                        var req_spec3_3 = '';
                                        var req_spec4_3 = '';
                                        var req_spec5_3 = '';
                                        var req_spec6_3 = '';
                                        var req_spec7_3 = '';
                                        var ship_spec1_2 = '';
                                        var ship_spec2_2 = '';
                                        var ship_spec3_2 = '';
                                        var ship_spec4_2 = '';
                                        var ship_spec5_2 = '';
                                        var ship_spec6_2 = '';
                                        var ship_spec7_2 = '';
                                        if (arrMIHS.length <= 40) {
                                            for (var r = 0; r < arrMIHS.length; r++) {
                                                console.log(arrayContainer_1[p][r]);
                                                textInfoMIHS1_1 += 'Sample' + (r + 1) + ': ' + arrayContainer_1[p][r]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][r]['SpecimenType'] + '\n';
                                                textInfoShipMIHS1_1 += 'Sample' + (r + 1) + ': ' + arrayContainer_1[p][r]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][r]['Amount'] + '\n';
                                            }
                                        }
                                        if (arrMIHS.length > 40 && arrMIHS.length <= 80) {
                                            for (var r = 0; r < 40; r++) {
                                                textInfoMIHS1_1 += 'Sample' + (r + 1) + ': ' + arrayContainer_1[p][r]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][r]['SpecimenType'] + '\n';
                                                textInfoShipMIHS1_1 += 'Sample' + (r + 1) + ': ' + arrayContainer_1[p][r]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][r]['Amount'] + '\n';
                                            }
                                            for (var s = 40; s < arrMIHS.length; s++) {
                                                textInfoMIHS2_1 += 'Sample' + (s + 1) + ': ' + arrayContainer_1[p][s]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][s]['SpecimenType'] + '\n';
                                                textInfoShipMIHS2_1 += 'Sample' + (s + 1) + ': ' + arrayContainer_1[p][s]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][s]['Amount'] + '\n';
                                            }
                                        }
                                        if (arrMIHS.length > 80 && arrMIHS.length <= 120) {
                                            for (var r = 0; r < 40; r++) {
                                                textInfoMIHS1_1 += 'Sample' + (r + 1) + ': ' + arrayContainer_1[p][r]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][r]['SpecimenType'] + '\n';
                                                textInfoShipMIHS1_1 += 'Sample' + (r + 1) + ': ' + arrayContainer_1[p][r]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][r]['Amount'] + '\n';
                                            }
                                            for (var s = 40; s < 80; s++) {
                                                textInfoMIHS2_1 += 'Sample' + (s + 1) + ': ' + arrayContainer_1[p][s]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][s]['SpecimenType'] + '\n';
                                                textInfoShipMIHS2_1 += 'Sample' + (s + 1) + ': ' + arrayContainer_1[p][s]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][s]['Amount'] + '\n';
                                            }
                                            for (var t = 80; t < arrMIHS.length; t++) {
                                                textInfoMIHS3_1 += 'Sample' + (t + 1) + ': ' + arrayContainer_1[p][t]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][t]['SpecimenType'] + '\n';
                                                textInfoShipMIHS3_1 += 'Sample' + (t + 1) + ': ' + arrayContainer_1[p][t]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][t]['Amount'] + '\n';
                                            }
                                        }
                                        if (arrMIHS.length > 120 && arrMIHS.length <= 160) {
                                            for (var r = 0; r < 40; r++) {
                                                textInfoMIHS1_1 += 'Sample' + (r + 1) + ': ' + arrayContainer_1[p][r]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][r]['SpecimenType'] + '\n';
                                                textInfoShipMIHS1_1 += 'Sample' + (r + 1) + ': ' + arrayContainer_1[p][r]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][r]['Amount'] + '\n';
                                            }
                                            for (var s = 40; s < 80; s++) {
                                                textInfoMIHS2_1 += 'Sample' + (s + 1) + ': ' + arrayContainer_1[p][s]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][s]['SpecimenType'] + '\n';
                                                textInfoShipMIHS2_1 += 'Sample' + (s + 1) + ': ' + arrayContainer_1[p][s]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][s]['Amount'] + '\n';
                                            }
                                            for (var t = 80; t < 120; t++) {
                                                textInfoMIHS3_1 += 'Sample' + (t + 1) + ': ' + arrayContainer_1[p][t]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][t]['SpecimenType'] + '\n';
                                                textInfoShipMIHS3_1 += 'Sample' + (t + 1) + ': ' + arrayContainer_1[p][t]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][t]['Amount'] + '\n';
                                            }
                                            for (var q = 120; q < arrMIHS.length; q++) {
                                                textInfoMIHS4_1 += 'Sample' + (q + 1) + ': ' + arrayContainer_1[p][q]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][q]['SpecimenType'] + '\n';
                                                textInfoShipMIHS4_1 += 'Sample' + (q + 1) + ': ' + arrayContainer_1[p][q]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][q]['Amount'] + '\n';
                                            }
                                        }
                                        if (arrMIHS.length > 160 && arrMIHS.length <= 200) {
                                            for (var r = 0; r < 40; r++) {
                                                textInfoMIHS1_1 += 'Sample' + (r + 1) + ': ' + arrayContainer_1[p][r]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][r]['SpecimenType'] + '\n';
                                                textInfoShipMIHS1_1 += 'Sample' + (r + 1) + ': ' + arrayContainer_1[p][r]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][r]['Amount'] + '\n';
                                            }
                                            for (var s = 40; s < 80; s++) {
                                                textInfoMIHS2_1 += 'Sample' + (s + 1) + ': ' + arrayContainer_1[p][s]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][s]['SpecimenType'] + '\n';
                                                textInfoShipMIHS2_1 += 'Sample' + (s + 1) + ': ' + arrayContainer_1[p][s]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][s]['Amount'] + '\n';
                                            }
                                            for (var t = 80; t < 120; t++) {
                                                textInfoMIHS3_1 += 'Sample' + (t + 1) + ': ' + arrayContainer_1[p][t]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][t]['SpecimenType'] + '\n';
                                                textInfoShipMIHS3_1 += 'Sample' + (t + 1) + ': ' + arrayContainer_1[p][t]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][t]['Amount'] + '\n';
                                            }
                                            for (var q = 120; q < 160; q++) {
                                                textInfoMIHS4_1 += 'Sample' + (q + 1) + ': ' + arrayContainer_1[p][q]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][q]['SpecimenType'] + '\n';
                                                textInfoShipMIHS4_1 += 'Sample' + (q + 1) + ': ' + arrayContainer_1[p][q]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][q]['Amount'] + '\n';
                                            }
                                            for (var v = 160; v < arrMIHS.length; v++) {
                                                textInfoMIHS5_1 += 'Sample' + (v + 1) + ': ' + arrayContainer_1[p][v]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][v]['SpecimenType'] + '\n';
                                                textInfoShipMIHS5_1 += 'Sample' + (v + 1) + ': ' + arrayContainer_1[p][v]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][v]['Amount'] + '\n';
                                            }
                                        }
                                        if (arrMIHS.length > 200 && arrMIHS.length <= 240) {
                                            for (var r = 0; r < 40; r++) {
                                                textInfoMIHS1_1 += 'Sample' + (r + 1) + ': ' + arrayContainer_1[p][r]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][r]['SpecimenType'] + '\n';
                                                textInfoShipMIHS1_1 += 'Sample' + (r + 1) + ': ' + arrayContainer_1[p][r]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][r]['Amount'] + '\n';
                                            }
                                            for (var s = 40; s < 80; s++) {
                                                textInfoMIHS2_1 += 'Sample' + (s + 1) + ': ' + arrayContainer_1[p][s]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][s]['SpecimenType'] + '\n';
                                                textInfoShipMIHS2_1 += 'Sample' + (s + 1) + ': ' + arrayContainer_1[p][s]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][s]['Amount'] + '\n';
                                            }
                                            for (var t = 80; t < 120; t++) {
                                                textInfoMIHS3_1 += 'Sample' + (t + 1) + ': ' + arrayContainer_1[p][t]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][t]['SpecimenType'] + '\n';
                                                textInfoShipMIHS3_1 += 'Sample' + (t + 1) + ': ' + arrayContainer_1[p][t]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][t]['Amount'] + '\n';
                                            }
                                            for (var q = 120; q < 160; q++) {
                                                textInfoMIHS4_1 += 'Sample' + (q + 1) + ': ' + arrayContainer_1[p][q]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][q]['SpecimenType'] + '\n';
                                                textInfoShipMIHS4_1 += 'Sample' + (q + 1) + ': ' + arrayContainer_1[p][q]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][q]['Amount'] + '\n';
                                            }
                                            for (var v = 160; v < 200; v++) {
                                                textInfoMIHS5_1 += 'Sample' + (v + 1) + ': ' + arrayContainer_1[p][v]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][v]['SpecimenType'] + '\n';
                                                textInfoShipMIHS5_1 += 'Sample' + (v + 1) + ': ' + arrayContainer_1[p][v]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][v]['Amount'] + '\n';
                                            }
                                            for (var w = 200; w < arrMIHS.length; w++) {
                                                textInfoMIHS6_1 += 'Sample' + (w + 1) + ': ' + arrayContainer_1[p][w]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][w]['SpecimenType'] + '\n';
                                                textInfoShipMIHS6_1 += 'Sample' + (w + 1) + ': ' + arrayContainer_1[p][w]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][w]['Amount'] + '\n';
                                            }
                                        }
                                        _this.gridService.postData(_this.getStudyFormDesignURL, _this.studyFormDesignArgMIHS).subscribe(function (data) {
                                            var formId = data.formIdentifier.PK;
                                            var studyId = data.studyIdentifier.PK;
                                            var arr1 = data.sections.section[0].fields.field;
                                            var arr2 = data.sections.section[1].fields.field;
                                            var newArr = arr1.concat(arr2);
                                            designArr_3 = newArr.map(function (obj) {
                                                var newObj = {};
                                                newObj[obj.uniqueID] = obj.fieldIdentifier.PK;
                                                return newObj;
                                            });
                                            designArr_3.map(function (obj) {
                                                if (obj['req_orderID'] != undefined) {
                                                    req_orderID_3 = obj['req_orderID'];
                                                }
                                                if (obj['er_def_date_01'] != undefined) {
                                                    er_def_date_01_3 = obj['er_def_date_01'];
                                                }
                                                if (obj['req_instorderID'] != undefined) {
                                                    req_instorderID_3 = obj['req_instorderID'];
                                                }
                                                if (obj['req_personalinfo'] != undefined) {
                                                    req_personalinfo_3 = obj['req_personalinfo'];
                                                }
                                                if (obj['req_spec1'] != undefined) {
                                                    req_spec1_3 = obj['req_spec1'];
                                                }
                                                if (obj['req_spec2'] != undefined) {
                                                    req_spec2_3 = obj['req_spec2'];
                                                }
                                                if (obj['req_spec3'] != undefined) {
                                                    req_spec3_3 = obj['req_spec3'];
                                                }
                                                if (obj['req_spec4'] != undefined) {
                                                    req_spec4_3 = obj['req_spec4'];
                                                }
                                                if (obj['req_spec5'] != undefined) {
                                                    req_spec5_3 = obj['req_spec5'];
                                                }
                                                if (obj['req_spec6'] != undefined) {
                                                    req_spec6_3 = obj['req_spec6'];
                                                }
                                                if (obj['req_spec7'] != undefined) {
                                                    req_spec7_3 = obj['req_spec7'];
                                                }
                                                if (obj['ship_spec1'] != undefined) {
                                                    ship_spec1_2 = obj['ship_spec1'];
                                                }
                                                if (obj['ship_spec2'] != undefined) {
                                                    ship_spec2_2 = obj['ship_spec2'];
                                                }
                                                if (obj['ship_spec3'] != undefined) {
                                                    ship_spec3_2 = obj['ship_spec3'];
                                                }
                                                if (obj['ship_spec4'] != undefined) {
                                                    ship_spec4_2 = obj['ship_spec4'];
                                                }
                                                if (obj['ship_spec5'] != undefined) {
                                                    ship_spec5_2 = obj['ship_spec5'];
                                                }
                                                if (obj['ship_spec6'] != undefined) {
                                                    ship_spec6_2 = obj['ship_spec6'];
                                                }
                                                if (obj['ship_spec7'] != undefined) {
                                                    ship_spec7_2 = obj['ship_spec7'];
                                                }
                                            });
                                            var parametersMIHS = {
                                                StudyFormResponse: {
                                                    formFieldResponses: {
                                                        field: [{
                                                                fieldIdentifier: {
                                                                    PK: er_def_date_01_3
                                                                },
                                                                uniqueID: 'er_def_date_01',
                                                                value: currentDate_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: req_orderID_3
                                                                },
                                                                uniqueID: 'req_orderID',
                                                                value: PK
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: req_instorderID_3
                                                                },
                                                                uniqueID: 'req_instorderID',
                                                                value: 'MIHS' + PK
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: req_personalinfo_3
                                                                },
                                                                uniqueID: 'req_personalinfo',
                                                                value: userInfo.userFirstName +' '+ userInfo.userLastName+' '+userInfo.userPhoneNo+' '+userInfo.userEmailAdd
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: req_spec1_3
                                                                },
                                                                uniqueID: 'req_spec1',
                                                                value: textInfoMIHS1_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: req_spec2_3
                                                                },
                                                                uniqueID: 'req_spec2',
                                                                value: textInfoMIHS2_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: req_spec3_3
                                                                },
                                                                uniqueID: 'req_spec3',
                                                                value: textInfoMIHS3_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: req_spec4_3
                                                                },
                                                                uniqueID: 'req_spec4',
                                                                value: textInfoMIHS4_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: req_spec5_3
                                                                },
                                                                uniqueID: 'req_spec5',
                                                                value: textInfoMIHS5_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: req_spec6_3
                                                                },
                                                                uniqueID: 'req_spec6',
                                                                value: textInfoMIHS6_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: req_spec7_3
                                                                },
                                                                uniqueID: 'req_spec7',
                                                                value: textInfoMIHS7_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: ship_spec1_2
                                                                },
                                                                uniqueID: 'ship_spec1',
                                                                value: textInfoShipMIHS1_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: ship_spec2_2
                                                                },
                                                                uniqueID: 'ship_spec2',
                                                                value: textInfoShipMIHS2_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: ship_spec3_2
                                                                },
                                                                uniqueID: 'ship_spec3',
                                                                value: textInfoShipMIHS3_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: ship_spec4_2
                                                                },
                                                                uniqueID: 'ship_spec4',
                                                                value: textInfoShipMIHS4_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: ship_spec5_2
                                                                },
                                                                uniqueID: 'ship_spec5',
                                                                value: textInfoShipMIHS5_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: ship_spec6_2
                                                                },
                                                                uniqueID: 'ship_spec6',
                                                                value: textInfoShipMIHS6_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: ship_spec7_2
                                                                },
                                                                uniqueID: 'ship_spec7',
                                                                value: textInfoShipMIHS7_1
                                                            }]
                                                    },
                                                    formFillDt: formattedCurrentDate_1,
                                                    formId: {
                                                        PK: formId
                                                    },
                                                    formVersion: 1,
                                                    formStatus: {
                                                        code: 'working',
                                                        description: 'Work In Progress',
                                                        type: 'fillformstat'
                                                    },
                                                    studyIdentifier: {
                                                        PK: studyId
                                                    }
                                                }
                                            };
                                            console.log('Parameters for MIHS', parametersMIHS);
                                            _this.gridService.postData(_this.createStudyFormResponseURL, parametersMIHS).subscribe(function (data) {
                                                var pkMIH = data[0].objectId.PK;
                                                if (pkMIH !== undefined) {
                                                    console.log(pkMIH);
                                                }
                                                else {
                                                    console.log('Error in creating MIH form');
                                                }
                                            });
                                        });
                                    } // if statement for PCH ends
                                    /* ----------------------------------------------------------- */
                                    if (arrayContainer_1[p][0]['SiteID'] === "500") {
                                        console.log(arrayContainer_1[p][0]['SiteID']);
                                        var textInfoSJHMC1_1 = '';
                                        var textInfoSJHMC2_1 = '';
                                        var textInfoSJHMC3_1 = '';
                                        var textInfoSJHMC4_1 = '';
                                        var textInfoSJHMC5_1 = '';
                                        var textInfoSJHMC6_1 = '';
                                        var textInfoSJHMC7_1 = '';
                                        var textInfoShipSJHMC1_1 = '';
                                        var textInfoShipSJHMC2_1 = '';
                                        var textInfoShipSJHMC3_1 = '';
                                        var textInfoShipSJHMC4_1 = '';
                                        var textInfoShipSJHMC5_1 = '';
                                        var textInfoShipSJHMC6_1 = '';
                                        var textInfoShipSJHMC7_1 = '';
                                        var arrSJHMC = arrayContainer_1[p];
                                        var designArr_4 = [];
                                        var req_orderID_4 = '';
                                        var er_def_date_01_4 = '';
                                        var req_instorderID_4 = '';
                                        var req_personalinfo_4 = '';
                                        var req_spec1_4 = '';
                                        var req_spec2_4 = '';
                                        var req_spec3_4 = '';
                                        var req_spec4_4 = '';
                                        var req_spec5_4 = '';
                                        var req_spec6_4 = '';
                                        var req_spec7_4 = '';
                                        var ship_spec1_3 = '';
                                        var ship_spec2_3 = '';
                                        var ship_spec3_3 = '';
                                        var ship_spec4_3 = '';
                                        var ship_spec5_3 = '';
                                        var ship_spec6_3 = '';
                                        var ship_spec7_3 = '';
                                        if (arrSJHMC.length <= 40) {
                                            for (var r = 0; r < arrSJHMC.length; r++) {
                                                textInfoSJHMC1_1 += 'Sample' + (r + 1) + ': ' + arrayContainer_1[p][r]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][r]['SpecimenType'] + '\n';
                                                textInfoShipSJHMC1_1 += 'Sample' + (r + 1) + ': ' + arrayContainer_1[p][r]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][r]['Amount'] + '\n';
                                            }
                                        }
                                        if (arrSJHMC.length > 40 && arrSJHMC.length <= 80) {
                                            for (var r = 0; r < 40; r++) {
                                                textInfoSJHMC1_1 += 'Sample' + (r + 1) + ': ' + arrayContainer_1[p][r]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][r]['SpecimenType'] + '\n';
                                                textInfoShipSJHMC1_1 += 'Sample' + (r + 1) + ': ' + arrayContainer_1[p][r]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][r]['Amount'] + '\n';
                                            }
                                            for (var s = 40; s < arrSJHMC.length; s++) {
                                                textInfoSJHMC2_1 += 'Sample' + (s + 1) + ': ' + arrayContainer_1[p][s]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][s]['SpecimenType'] + '\n';
                                                textInfoShipSJHMC2_1 += 'Sample' + (s + 1) + ': ' + arrayContainer_1[p][s]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][s]['Amount'] + '\n';
                                            }
                                        }
                                        if (arrSJHMC.length > 80 && arrSJHMC.length <= 120) {
                                            for (var r = 0; r < 40; r++) {
                                                textInfoSJHMC1_1 += 'Sample' + (r + 1) + ': ' + arrayContainer_1[p][r]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][r]['SpecimenType'] + '\n';
                                                textInfoShipSJHMC1_1 += 'Sample' + (r + 1) + ': ' + arrayContainer_1[p][r]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][r]['Amount'] + '\n';
                                            }
                                            for (var s = 40; s < 80; s++) {
                                                textInfoSJHMC2_1 += 'Sample' + (s + 1) + ': ' + arrayContainer_1[p][s]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][s]['SpecimenType'] + '\n';
                                                textInfoShipSJHMC2_1 += 'Sample' + (s + 1) + ': ' + arrayContainer_1[p][s]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][s]['Amount'] + '\n';
                                            }
                                            for (var t = 80; t < arrSJHMC.length; t++) {
                                                textInfoSJHMC3_1 += 'Sample' + (t + 1) + ': ' + arrayContainer_1[p][t]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][t]['SpecimenType'] + '\n';
                                                textInfoShipSJHMC3_1 += 'Sample' + (t + 1) + ': ' + arrayContainer_1[p][t]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][t]['Amount'] + '\n';
                                            }
                                        }
                                        if (arrSJHMC.length > 120 && arrSJHMC.length <= 160) {
                                            for (var r = 0; r < 40; r++) {
                                                textInfoSJHMC1_1 += 'Sample' + (r + 1) + ': ' + arrayContainer_1[p][r]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][r]['SpecimenType'] + '\n';
                                                textInfoShipSJHMC1_1 += 'Sample' + (r + 1) + ': ' + arrayContainer_1[p][r]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][r]['Amount'] + '\n';
                                            }
                                            for (var s = 40; s < 80; s++) {
                                                textInfoSJHMC2_1 += 'Sample' + (s + 1) + ': ' + arrayContainer_1[p][s]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][s]['SpecimenType'] + '\n';
                                                textInfoShipSJHMC2_1 += 'Sample' + (s + 1) + ': ' + arrayContainer_1[p][s]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][s]['Amount'] + '\n';
                                            }
                                            for (var t = 80; t < 120; t++) {
                                                textInfoSJHMC3_1 += 'Sample' + (t + 1) + ': ' + arrayContainer_1[p][t]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][t]['SpecimenType'] + '\n';
                                                textInfoShipSJHMC3_1 += 'Sample' + (t + 1) + ': ' + arrayContainer_1[p][t]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][t]['Amount'] + '\n';
                                            }
                                            for (var q = 120; q < arrSJHMC.length; q++) {
                                                textInfoSJHMC4_1 += 'Sample' + (q + 1) + ': ' + arrayContainer_1[p][q]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][q]['SpecimenType'] + '\n';
                                                textInfoShipSJHMC4_1 += 'Sample' + (q + 1) + ': ' + arrayContainer_1[p][q]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][q]['Amount'] + '\n';
                                            }
                                        }
                                        if (arrSJHMC.length > 160 && arrSJHMC.length <= 200) {
                                            for (var r = 0; r < 40; r++) {
                                                textInfoSJHMC1_1 += 'Sample' + (r + 1) + ': ' + arrayContainer_1[p][r]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][r]['SpecimenType'] + '\n';
                                                textInfoShipSJHMC1_1 += 'Sample' + (r + 1) + ': ' + arrayContainer_1[p][r]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][r]['Amount'] + '\n';
                                            }
                                            for (var s = 40; s < 80; s++) {
                                                textInfoSJHMC2_1 += 'Sample' + (s + 1) + ': ' + arrayContainer_1[p][s]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][s]['SpecimenType'] + '\n';
                                                textInfoShipSJHMC2_1 += 'Sample' + (s + 1) + ': ' + arrayContainer_1[p][s]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][s]['Amount'] + '\n';
                                            }
                                            for (var t = 80; t < 120; t++) {
                                                textInfoSJHMC3_1 += 'Sample' + (t + 1) + ': ' + arrayContainer_1[p][t]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][t]['SpecimenType'] + '\n';
                                                textInfoShipSJHMC3_1 += 'Sample' + (t + 1) + ': ' + arrayContainer_1[p][t]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][t]['Amount'] + '\n';
                                            }
                                            for (var q = 120; q < 160; q++) {
                                                textInfoSJHMC4_1 += 'Sample' + (q + 1) + ': ' + arrayContainer_1[p][q]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][q]['SpecimenType'] + '\n';
                                                textInfoShipSJHMC4_1 += 'Sample' + (q + 1) + ': ' + arrayContainer_1[p][q]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][q]['Amount'] + '\n';
                                            }
                                            for (var u = 160; u < arrSJHMC.length; u++) {
                                                textInfoSJHMC5_1 += 'Sample' + (u + 1) + ': ' + arrayContainer_1[p][u]['SpecimenID'] + ', ' + "SpecimenType: " + arrayContainer_1[p][u]['SpecimenType'] + '\n';
                                                textInfoShipSJHMC5_1 += 'Sample' + (u + 1) + ': ' + arrayContainer_1[p][u]['SpecimenID'] + ', ' + "Quantity: " + arrayContainer_1[p][u]['Amount'] + '\n';
                                            }
                                        }
                                        _this.gridService.postData(_this.getStudyFormDesignURL, _this.studyFormDesignArgSJHMC).subscribe(function (data) {
                                            console.log(data);
                                            var formId = data.formIdentifier.PK;
                                            var studyId = data.studyIdentifier.PK;
                                            var arr1 = data.sections.section[0].fields.field;
                                            var arr2 = data.sections.section[1].fields.field;
                                            var newArr = arr1.concat(arr2);
                                            designArr_4 = newArr.map(function (obj) {
                                                var newObj = {};
                                                newObj[obj.uniqueID] = obj.fieldIdentifier.PK;
                                                return newObj;
                                            });
                                            // console.log(designArr);
                                            designArr_4.map(function (obj) {
                                                if (obj['req_orderID'] != undefined) {
                                                    req_orderID_4 = obj['req_orderID'];
                                                }
                                                if (obj['er_def_date_01'] != undefined) {
                                                    er_def_date_01_4 = obj['er_def_date_01'];
                                                }
                                                if (obj['req_instorderID'] != undefined) {
                                                    req_instorderID_4 = obj['req_instorderID'];
                                                }
                                                if (obj['req_personalinfo'] != undefined) {
                                                    req_personalinfo_4 = obj['req_personalinfo'];
                                                }
                                                if (obj['req_spec1'] != undefined) {
                                                    req_spec1_4 = obj['req_spec1'];
                                                }
                                                if (obj['req_spec2'] != undefined) {
                                                    req_spec2_4 = obj['req_spec2'];
                                                }
                                                if (obj['req_spec3'] != undefined) {
                                                    req_spec3_4 = obj['req_spec3'];
                                                }
                                                if (obj['req_spec4'] != undefined) {
                                                    req_spec4_4 = obj['req_spec4'];
                                                }
                                                if (obj['req_spec5'] != undefined) {
                                                    req_spec5_4 = obj['req_spec5'];
                                                }
                                                if (obj['req_spec6'] != undefined) {
                                                    req_spec6_4 = obj['req_spec6'];
                                                }
                                                if (obj['req_spec7'] != undefined) {
                                                    req_spec7_4 = obj['req_spec7'];
                                                }
                                                if (obj['ship_spec1'] != undefined) {
                                                    ship_spec1_3 = obj['ship_spec1'];
                                                }
                                                if (obj['ship_spec2'] != undefined) {
                                                    ship_spec2_3 = obj['ship_spec2'];
                                                }
                                                if (obj['ship_spec3'] != undefined) {
                                                    ship_spec3_3 = obj['ship_spec3'];
                                                }
                                                if (obj['ship_spec4'] != undefined) {
                                                    ship_spec4_3 = obj['ship_spec4'];
                                                }
                                                if (obj['ship_spec5'] != undefined) {
                                                    ship_spec5_3 = obj['ship_spec5'];
                                                }
                                                if (obj['ship_spec6'] != undefined) {
                                                    ship_spec6_3 = obj['ship_spec6'];
                                                }
                                                if (obj['ship_spec7'] != undefined) {
                                                    ship_spec7_3 = obj['ship_spec7'];
                                                }
                                            });
                                            var parametersSJHMC = {
                                                StudyFormResponse: {
                                                    formFieldResponses: {
                                                        field: [{
                                                                fieldIdentifier: {
                                                                    PK: er_def_date_01_4
                                                                },
                                                                uniqueID: 'er_def_date_01',
                                                                value: currentDate_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: req_orderID_4
                                                                },
                                                                uniqueID: 'req_orderID',
                                                                value: PK
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: req_instorderID_4
                                                                },
                                                                uniqueID: 'req_instorderID',
                                                                value: 500 + PK
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: req_personalinfo_4
                                                                },
                                                                uniqueID: 'req_personalinfo',
                                                                value: userInfo.userFirstName +' '+ userInfo.userLastName+' '+userInfo.userPhoneNo+' '+userInfo.userEmailAdd
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: req_spec1_4
                                                                },
                                                                uniqueID: 'req_spec1',
                                                                value: textInfoSJHMC1_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: req_spec2_4
                                                                },
                                                                uniqueID: 'req_spec2',
                                                                value: textInfoSJHMC2_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: req_spec3_4
                                                                },
                                                                uniqueID: 'req_spec3',
                                                                value: textInfoSJHMC3_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: req_spec4_4
                                                                },
                                                                uniqueID: 'req_spec4',
                                                                value: textInfoSJHMC4_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: req_spec5_4
                                                                },
                                                                uniqueID: 'req_spec5',
                                                                value: textInfoSJHMC5_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: req_spec6_4
                                                                },
                                                                uniqueID: 'req_spec6',
                                                                value: textInfoSJHMC6_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: req_spec7_4
                                                                },
                                                                uniqueID: 'req_spec7',
                                                                value: textInfoSJHMC7_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: ship_spec1_3
                                                                },
                                                                uniqueID: 'ship_spec1',
                                                                value: textInfoShipSJHMC1_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: ship_spec2_3
                                                                },
                                                                uniqueID: 'ship_spec2',
                                                                value: textInfoShipSJHMC2_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: ship_spec3_3
                                                                },
                                                                uniqueID: 'ship_spec3',
                                                                value: textInfoShipSJHMC3_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: ship_spec4_3
                                                                },
                                                                uniqueID: 'ship_spec4',
                                                                value: textInfoShipSJHMC4_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: ship_spec5_3
                                                                },
                                                                uniqueID: 'ship_spec5',
                                                                value: textInfoShipSJHMC5_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: ship_spec6_3
                                                                },
                                                                uniqueID: 'ship_spec6',
                                                                value: textInfoShipSJHMC6_1
                                                            },
                                                            {
                                                                fieldIdentifier: {
                                                                    PK: ship_spec7_3
                                                                },
                                                                uniqueID: 'ship_spec7',
                                                                value: textInfoShipSJHMC7_1
                                                            }]
                                                    },
                                                    formFillDt: '2018-10-04',
                                                    formId: {
                                                        PK: formId
                                                    },
                                                    formVersion: 1,
                                                    formStatus: {
                                                        code: 'working',
                                                        description: 'Work In Progress',
                                                        type: 'fillformstat'
                                                    },
                                                    studyIdentifier: {
                                                        PK: studyId
                                                    }
                                                }
                                            };
                                            console.log('Parameters for SJHMC', parametersSJHMC);
                                            _this.gridService.postData(_this.createStudyFormResponseURL, parametersSJHMC).subscribe(function (data) {
                                                var pkSJHMC = data[0].objectId.PK;
                                                if (pkSJHMC !== undefined) {
                                                    console.log(pkSJHMC);
                                                }
                                                else {
                                                    console.log('Error in creating SJHMC form');
                                                }
                                            });
                                        });
                                    } // if statement ends
                                    /* ------------------------------------------ */
                                };
                                for (var p = 0; p < arrayContainer_1.length; p++) {
                                    _loop_1(p);
                                }
                            }
                        });
                    }
                    _this.dataArray.length = 0;
                });
            });
			setTimeout(()=>{
				this.DisplayDialogueBox = true;
			}, 3000);
            
        }
        this.DisplayCart = false;
        console.log('cart displayed now');
    }; /// call webservices ends
    // This function close the dialogue box appers after the specimen request
    BiospecimenComponent.prototype.closeDialogue = function () {
        this.DisplayDialogueBox = false;
        console.log('clicked this');
    };
    BiospecimenComponent.prototype.closeCart = function () {
        this.DisplayCart = false;
        console.log('cart displayed now');
    };
    BiospecimenComponent.prototype.closeBox = function () {
        this.DisplayErrorBox = false;
        console.log('close clicked');
    };
    BiospecimenComponent.prototype.editCart = function () {
        var noOfSpecimens = this.dataArray.length;
		var orginalArray = this.dataArray;
		var cnt = 0;
        var inputs = document.getElementsByTagName('input');
        for (var i = 0; i < inputs.length; i++) {
            if (inputs[i].checked === true) {
				//console.log(i-3);
                //console.log(orginalArray[i]);
				//console.log(this.dataArray.indexOf(orginalArray[i])-3);
                this.dataArray.splice(i-3-cnt, 1);
				cnt++;
            }
        }
        // inputs[i].checked = false; // Deselecting after call is made.
    };
    BiospecimenComponent.prototype.ngOnInit = function () {
        var _this = this;
        var URL_KEY = URL_HOST+'assets/data/bioSpecimenDataKeys.json';
        var SPECIMEN_TYPE = URL_HOST+'assets/data/specimenType.json';
        var SPECIMEN_CHARACTERISTICS = URL_HOST+'assets/data/specimenCharacteristic.json';
        var ANATOMIC_SITE = URL_HOST+'assets/data/anatomicSite.json';
		console.log(URL_HOST);
        // this.auth.login("http://localhost:3000/api/v2/testToken").subscribe(res => {
        //   let token = res;
        //   console.log(token);
        // });
        // this.auth.login();
        function sortedObject(data) {
            var sortable = [];
            for (var key in data) {
                sortable.push([key, data[key]]);
            }
            sortable.sort(function (a, b) {
                return (a[0] < b[0] ? -1 : (a[0] > b[0] ? 1 : 0));
            });
            var orderedList = {};
            for (var i = 0; i < sortable.length; i++) {
                orderedList[sortable[i][0]] = sortable[i][1];
            }
            //console.log(orderedList);
            return orderedList;
        }
        this.gridService.getData(URL_HOST+'assets/data/config.json').subscribe(function (data) {
            _this.envSystem = data.server.system;
            _this.storeFrontURL = data.server[_this.envSystem].storeFrontURL;
            _this.createStudyFormResponseURL = data.server[_this.envSystem].createStudyFormResponseURL;
            _this.updateStudyFormResponseURL = data.server[_this.envSystem].updateStudyFormResponseURL;
            _this.getStudyFormDesignURL = data.server[_this.envSystem].getStudyFormDesignURL;
            _this.studyFormDesignArgMain = data.server[_this.envSystem].studyFormDesignArg.mainReqForm;
            _this.studyFormDesignArgPCH = data.server[_this.envSystem].studyFormDesignArg.PCH;
            _this.studyFormDesignArgMIHS = data.server[_this.envSystem].studyFormDesignArg.MIHS;
            _this.studyFormDesignArgSJHMC = data.server[_this.envSystem].studyFormDesignArg.SJHMC;
            // this.storeFrontURL = data.server.localhost.storeFrontURL;
        });
        // get call to retrieve the key from json files stored on data directory
        this.gridService.getKeyData(URL_KEY).subscribe(function (keys) {
            _this.columns = keys;
        });
        this.gridService.getKeyData(SPECIMEN_TYPE).subscribe(function (data) {
            _this.specimens = sortedObject(data); //data;
        });
        this.gridService.getKeyData(SPECIMEN_CHARACTERISTICS).subscribe(function (data) {
            _this.SpecimenCharacteristic = sortedObject(data);
        });
        this.gridService.getKeyData(ANATOMIC_SITE).subscribe(function (data) {
            _this.anatomicSites = sortedObject(data);
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('fabricTable'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], BiospecimenComponent.prototype, "elContent", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChildren"])(BiospecimenComponent_1),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["QueryList"])
    ], BiospecimenComponent.prototype, "tabs", void 0);
    BiospecimenComponent = BiospecimenComponent_1 = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-biospecimen',
            template: __webpack_require__(/*! ./biospecimen.component.html */ "./src/app/biospecimen/biospecimen.component.html"),
            styles: [__webpack_require__(/*! ./biospecimen.component.css */ "./src/app/biospecimen/biospecimen.component.css")]
        }),
        __metadata("design:paramtypes", [_grid_service__WEBPACK_IMPORTED_MODULE_1__["GridService"], _mainParameters_service__WEBPACK_IMPORTED_MODULE_2__["MainParametersService"], _data_service__WEBPACK_IMPORTED_MODULE_3__["DataService"]])
    ], BiospecimenComponent);
    return BiospecimenComponent;
    var BiospecimenComponent_1;
}());



/***/ }),

/***/ "./src/app/chart/chart.component.css":
/*!*******************************************!*\
  !*** ./src/app/chart/chart.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NoYXJ0L2NoYXJ0LmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/chart/chart.component.html":
/*!********************************************!*\
  !*** ./src/app/chart/chart.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div style=\"display: block\">\r\n  <canvas mdbChart\r\n          [chartType]=\"chartType\"\r\n          [data]=\"chartData\"\r\n          [labels]=\"chartLabels\"\r\n          [colors]=\"chartColors\"\r\n          [options]=\"chartOptions\"\r\n          [legend]=\"true\"\r\n          (chartHover)=\"chartHovered($event)\"\r\n          (chartClick)=\"chartClicked($event)\">\r\n  </canvas>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/chart/chart.component.ts":
/*!******************************************!*\
  !*** ./src/app/chart/chart.component.ts ***!
  \******************************************/
/*! exports provided: ChartComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChartComponent", function() { return ChartComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _grid_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../grid.service */ "./src/app/grid.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ChartComponent = /** @class */ (function () {
    function ChartComponent(gridService) {
        this.gridService = gridService;
    }
    ChartComponent.prototype.ngOnInit = function () {
        //   this.gridService.getStudyStatusMetricsData().subscribe(grid => {
        //     // console.log(grid);
        //     let data = [];
        //     let cancerCenterData  = [];
        //     let cardiologyData    = [];
        //     let neurologyData     = [];
        //     let medicineData      = [];
        //     let surgeryData       = [];
        //     grid.forEach(function (keys, value) {
        //       let val = grid[value];
        //       for (let key in val) {
        //         if (key === "STUDY_DIVISION" && "Cardiology" && "Neurology" && "Medicine" && "Surgery") {
        //           data.push(val[key]);
        //           // console.log(data);
        //         }
        //       }
        //     });
        //     for (let i = 0; i < data.length; i++) {
        //       if (data[i] === "Cancer Center") {
        //         cancerCenterData.push(data[i]);
        //       } else if (data[i] === "Cardiology") {
        //         cardiologyData.push(data[i])
        //       }else if (data[i] === "Neurology") {
        //         neurologyData.push(data[i])
        //       }else if (data[i] === "Medicine") {
        //         medicineData.push(data[i])
        //       }else if (data[i] === "Surgery") {
        //         surgeryData.push(data[i])
        //       }
        //     }
        //     let cancerDataNumber      = cancerCenterData.length;
        //     let cardiologyDataNumber  = cardiologyData.length;
        //     let neurologyDataNumber   = neurologyData.length;
        //     let medicineDataNumber    = medicineData.length;
        //     let surgeryDataNumber     = surgeryData.length;
        //     this.chartData            = [cancerDataNumber, neurologyDataNumber, cardiologyDataNumber, medicineDataNumber, surgeryDataNumber]
        //   });
        // }
        //
        //
        // public chartType:string = 'pie';
        //
        // public chartData:Array<any> = [];
        //
        // public chartLabels:Array<any> = ['Cancer Center', 'Neurology', 'Cardiology', 'Medicine', 'Surgery'];
        //
        // public chartColors:Array<any> = [{
        //   hoverBorderColor: ['rgba(0, 0, 0, 0.1)', 'rgba(0, 0, 0, 0.1)', 'rgba(0, 0, 0, 0.1)', 'rgba(0, 0, 0, 0.1)', 'rgba(0, 0, 0, 0.1)'],
        //   hoverBorderWidth: 0,
        //   backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"],
        //   hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5","#616774"]
        // }];
        //
        // public chartOptions:any = {
        //   responsive: true
        // };
        //
        // public chartClicked(e: any): void {
        //
        // }
        //
        // public chartHovered(e: any): void {
        //
    };
    ChartComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-chart',
            template: __webpack_require__(/*! ./chart.component.html */ "./src/app/chart/chart.component.html"),
            styles: [__webpack_require__(/*! ./chart.component.css */ "./src/app/chart/chart.component.css")]
        }),
        __metadata("design:paramtypes", [_grid_service__WEBPACK_IMPORTED_MODULE_1__["GridService"]])
    ], ChartComponent);
    return ChartComponent;
}());



/***/ }),

/***/ "./src/app/data.service.ts":
/*!*********************************!*\
  !*** ./src/app/data.service.ts ***!
  \*********************************/
/*! exports provided: DataService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataService", function() { return DataService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DataService = /** @class */ (function () {
    function DataService() {
        this.dataSource = new rxjs__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"]([]);
        this.currentData = this.dataSource.asObservable();
    }
    DataService.prototype.changeData = function (data) {
        this.dataSource.next(data);
    };
    DataService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], DataService);
    return DataService;
}());



/***/ }),

/***/ "./src/app/dataPipe.ts":
/*!*****************************!*\
  !*** ./src/app/dataPipe.ts ***!
  \*****************************/
/*! exports provided: DataPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataPipe", function() { return DataPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var DataPipe = /** @class */ (function () {
    function DataPipe() {
    }
    DataPipe.prototype.transform = function (value, args) {
        var arr = [];
        for (var key in value) {
            if (typeof value[key] === 'object') {
                for (var prop in value[key]) {
                    if (typeof value[key][prop] === 'object') {
                        for (var innerProp in value[key][prop]) {
                            var newKey = prop + innerProp;
                            value[key][newKey] = value[key][prop][innerProp];
                        }
                    }
                }
            }
            console.log(value);
            arr.push(value[key]);
        }
        return arr;
    };
    DataPipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
            name: "data"
        })
    ], DataPipe);
    return DataPipe;
}());



/***/ }),

/***/ "./src/app/dispatch-msg/dispatch-msg.component.css":
/*!*********************************************************!*\
  !*** ./src/app/dispatch-msg/dispatch-msg.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "tbody {\r\n  display:block;\r\n  height:800px;\r\n  overflow:auto;\r\n}\r\nthead, tbody tr {\r\n  display:table;\r\n  width:100%;\r\n  table-layout:fixed;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGlzcGF0Y2gtbXNnL2Rpc3BhdGNoLW1zZy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsY0FBYztFQUNkLGFBQWE7RUFDYixjQUFjO0NBQ2Y7QUFDRDtFQUNFLGNBQWM7RUFDZCxXQUFXO0VBQ1gsbUJBQW1CO0NBQ3BCIiwiZmlsZSI6InNyYy9hcHAvZGlzcGF0Y2gtbXNnL2Rpc3BhdGNoLW1zZy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsidGJvZHkge1xyXG4gIGRpc3BsYXk6YmxvY2s7XHJcbiAgaGVpZ2h0OjgwMHB4O1xyXG4gIG92ZXJmbG93OmF1dG87XHJcbn1cclxudGhlYWQsIHRib2R5IHRyIHtcclxuICBkaXNwbGF5OnRhYmxlO1xyXG4gIHdpZHRoOjEwMCU7XHJcbiAgdGFibGUtbGF5b3V0OmZpeGVkO1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/dispatch-msg/dispatch-msg.component.html":
/*!**********************************************************!*\
  !*** ./src/app/dispatch-msg/dispatch-msg.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<table class=\"table table-hover\">\r\n  <thead class=\"mdb-color deep-orange lighten-1\">\r\n  <tr class=\"text-white\">\r\n    <th *ngFor=\"let column of columns | keys\" style=\"text-align: center\">\r\n      {{column.key}}\r\n    </th>\r\n  </tr>\r\n  </thead>\r\n  <tbody>\r\n  <tr *ngFor=\"let row of rows\">\r\n    <td *ngFor=\"let column of columns | keys\">\r\n      {{row[column.value]}}\r\n    </td>\r\n  </tr>\r\n  </tbody>\r\n</table>\r\n"

/***/ }),

/***/ "./src/app/dispatch-msg/dispatch-msg.component.ts":
/*!********************************************************!*\
  !*** ./src/app/dispatch-msg/dispatch-msg.component.ts ***!
  \********************************************************/
/*! exports provided: DispatchMsgComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DispatchMsgComponent", function() { return DispatchMsgComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _grid_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../grid.service */ "./src/app/grid.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DispatchMsgComponent = /** @class */ (function () {
    function DispatchMsgComponent(gridService) {
        this.gridService = gridService;
    }
    DispatchMsgComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Retrieve data from api
        var URL = "/api/v2/getDispatchMsg_U";
        var K_URL = "velos/jsp/dist/assets/data/dispatchMsgDataKeys.json";
        this.gridService.getData(URL).subscribe(function (grid) {
            _this.rows = grid;
        });
        this.gridService.getData(K_URL).subscribe(function (keys) {
            _this.columns = keys;
        });
    };
    DispatchMsgComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-dispatch-msg',
            template: __webpack_require__(/*! ./dispatch-msg.component.html */ "./src/app/dispatch-msg/dispatch-msg.component.html"),
            styles: [__webpack_require__(/*! ./dispatch-msg.component.css */ "./src/app/dispatch-msg/dispatch-msg.component.css")]
        }),
        __metadata("design:paramtypes", [_grid_service__WEBPACK_IMPORTED_MODULE_1__["GridService"]])
    ], DispatchMsgComponent);
    return DispatchMsgComponent;
}());



/***/ }),

/***/ "./src/app/e-sample-all-status/e-sample-all-status.component.css":
/*!***********************************************************************!*\
  !*** ./src/app/e-sample-all-status/e-sample-all-status.component.css ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\ntbody {\r\n  display:block;\r\n  height:800px;\r\n  overflow:auto;\r\n}\r\nthead, tbody tr {\r\n  display:table;\r\n  width:100%;\r\n  table-layout:fixed;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZS1zYW1wbGUtYWxsLXN0YXR1cy9lLXNhbXBsZS1hbGwtc3RhdHVzLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUNBO0VBQ0UsY0FBYztFQUNkLGFBQWE7RUFDYixjQUFjO0NBQ2Y7QUFDRDtFQUNFLGNBQWM7RUFDZCxXQUFXO0VBQ1gsbUJBQW1CO0NBQ3BCIiwiZmlsZSI6InNyYy9hcHAvZS1zYW1wbGUtYWxsLXN0YXR1cy9lLXNhbXBsZS1hbGwtc3RhdHVzLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxudGJvZHkge1xyXG4gIGRpc3BsYXk6YmxvY2s7XHJcbiAgaGVpZ2h0OjgwMHB4O1xyXG4gIG92ZXJmbG93OmF1dG87XHJcbn1cclxudGhlYWQsIHRib2R5IHRyIHtcclxuICBkaXNwbGF5OnRhYmxlO1xyXG4gIHdpZHRoOjEwMCU7XHJcbiAgdGFibGUtbGF5b3V0OmZpeGVkO1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/e-sample-all-status/e-sample-all-status.component.html":
/*!************************************************************************!*\
  !*** ./src/app/e-sample-all-status/e-sample-all-status.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<table class=\"table table-hover\">\r\n  <thead class=\"mdb-color light-blue darken-4\">\r\n  <tr class=\"text-white\">\r\n    <th *ngFor=\"let column of columns | keys\" style=\"text-align: center\">\r\n      {{column.key}}\r\n    </th>\r\n  </tr>\r\n  </thead>\r\n  <tbody>\r\n  <tr *ngFor=\"let row of rows\">\r\n    <td *ngFor=\"let column of columns | keys\">\r\n      {{row[column.value]}}\r\n    </td>\r\n  </tr>\r\n  </tbody>\r\n</table>\r\n\r\n"

/***/ }),

/***/ "./src/app/e-sample-all-status/e-sample-all-status.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/e-sample-all-status/e-sample-all-status.component.ts ***!
  \**********************************************************************/
/*! exports provided: ESampleAllStatusComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ESampleAllStatusComponent", function() { return ESampleAllStatusComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _grid_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../grid.service */ "./src/app/grid.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ESampleAllStatusComponent = /** @class */ (function () {
    function ESampleAllStatusComponent(gridService) {
        this.gridService = gridService;
    }
    ESampleAllStatusComponent.prototype.ngOnInit = function () {
        // Retrieve data from api
        var _this = this;
        var URL = '/api/v2/getAllStatus';
        var K_URL = URL_HOST+'assets/data/eSampleAllStatusDataKeys.json';
        this.gridService.getData(URL).subscribe(function (grid) {
            _this.rows = grid;
        });
        this.gridService.getData(K_URL).subscribe(function (keys) {
            _this.columns = keys;
        });
    };
    ESampleAllStatusComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-e-sample-all-status',
            template: __webpack_require__(/*! ./e-sample-all-status.component.html */ "./src/app/e-sample-all-status/e-sample-all-status.component.html"),
            styles: [__webpack_require__(/*! ./e-sample-all-status.component.css */ "./src/app/e-sample-all-status/e-sample-all-status.component.css")]
        }),
        __metadata("design:paramtypes", [_grid_service__WEBPACK_IMPORTED_MODULE_1__["GridService"]])
    ], ESampleAllStatusComponent);
    return ESampleAllStatusComponent;
}());



/***/ }),

/***/ "./src/app/e-sample/e-sample.component.css":
/*!*************************************************!*\
  !*** ./src/app/e-sample/e-sample.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\ntbody {\r\n  display:block;\r\n  height:800px;\r\n  overflow:auto;\r\n}\r\nthead, tbody tr {\r\n  display:table;\r\n  width:100%;\r\n  table-layout:fixed;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZS1zYW1wbGUvZS1zYW1wbGUuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQ0E7RUFDRSxjQUFjO0VBQ2QsYUFBYTtFQUNiLGNBQWM7Q0FDZjtBQUNEO0VBQ0UsY0FBYztFQUNkLFdBQVc7RUFDWCxtQkFBbUI7Q0FDcEIiLCJmaWxlIjoic3JjL2FwcC9lLXNhbXBsZS9lLXNhbXBsZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbnRib2R5IHtcclxuICBkaXNwbGF5OmJsb2NrO1xyXG4gIGhlaWdodDo4MDBweDtcclxuICBvdmVyZmxvdzphdXRvO1xyXG59XHJcbnRoZWFkLCB0Ym9keSB0ciB7XHJcbiAgZGlzcGxheTp0YWJsZTtcclxuICB3aWR0aDoxMDAlO1xyXG4gIHRhYmxlLWxheW91dDpmaXhlZDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/e-sample/e-sample.component.html":
/*!**************************************************!*\
  !*** ./src/app/e-sample/e-sample.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<table class=\"table table-hover\">\r\n  <thead class=\"mdb-color deep-orange lighten-1\">\r\n  <tr class=\"text-white\">\r\n    <th *ngFor=\"let column of columns | keys\" style=\"text-align: center\">\r\n      {{column.key}}\r\n    </th>\r\n  </tr>\r\n  </thead>\r\n  <tbody>\r\n  <tr *ngFor=\"let row of rows\">\r\n    <td *ngFor=\"let column of columns | keys\">\r\n      {{row[column.value]}}\r\n    </td>\r\n  </tr>\r\n  </tbody>\r\n</table>\r\n"

/***/ }),

/***/ "./src/app/e-sample/e-sample.component.ts":
/*!************************************************!*\
  !*** ./src/app/e-sample/e-sample.component.ts ***!
  \************************************************/
/*! exports provided: ESampleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ESampleComponent", function() { return ESampleComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _grid_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../grid.service */ "./src/app/grid.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ESampleComponent = /** @class */ (function () {
    function ESampleComponent(gridService) {
        this.gridService = gridService;
    }
    ESampleComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Retrieve data from api
        var URL = '/api/v2/getDetails';
        var K_URL = URL_HOST+'assets/data/eSampleDetailsDataKeys.json';
        this.gridService.getData(URL).subscribe(function (grid) {
            _this.rows = grid;
        });
        this.gridService.getData(K_URL).subscribe(function (keys) {
            _this.columns = keys;
        });
    };
    ESampleComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-e-sample',
            template: __webpack_require__(/*! ./e-sample.component.html */ "./src/app/e-sample/e-sample.component.html"),
            styles: [__webpack_require__(/*! ./e-sample.component.css */ "./src/app/e-sample/e-sample.component.css")]
        }),
        __metadata("design:paramtypes", [_grid_service__WEBPACK_IMPORTED_MODULE_1__["GridService"]])
    ], ESampleComponent);
    return ESampleComponent;
}());



/***/ }),

/***/ "./src/app/edit-data/edit-data.component.css":
/*!***************************************************!*\
  !*** ./src/app/edit-data/edit-data.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2VkaXQtZGF0YS9lZGl0LWRhdGEuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/edit-data/edit-data.component.html":
/*!****************************************************!*\
  !*** ./src/app/edit-data/edit-data.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  edit-data works!\r\n  {{this.dataFromService}}\r\n</p>\r\n\r\n<table>\r\n  <tr *ngFor=\"let newData of this.dataFromService\">\r\n    {{newData.Age}}\r\n  </tr>\r\n</table>\r\n"

/***/ }),

/***/ "./src/app/edit-data/edit-data.component.ts":
/*!**************************************************!*\
  !*** ./src/app/edit-data/edit-data.component.ts ***!
  \**************************************************/
/*! exports provided: EditDataComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditDataComponent", function() { return EditDataComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../data.service */ "./src/app/data.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var EditDataComponent = /** @class */ (function () {
    function EditDataComponent(data) {
        this.data = data;
        this.dataFromService = [];
    }
    EditDataComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.data.currentData.subscribe(function (data) {
            _this.dataFromService = data;
        });
        console.log(this.dataFromService);
    };
    EditDataComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-edit-data',
            template: __webpack_require__(/*! ./edit-data.component.html */ "./src/app/edit-data/edit-data.component.html"),
            styles: [__webpack_require__(/*! ./edit-data.component.css */ "./src/app/edit-data/edit-data.component.css")]
        }),
        __metadata("design:paramtypes", [_data_service__WEBPACK_IMPORTED_MODULE_1__["DataService"]])
    ], EditDataComponent);
    return EditDataComponent;
}());



/***/ }),

/***/ "./src/app/grid.service.ts":
/*!*********************************!*\
  !*** ./src/app/grid.service.ts ***!
  \*********************************/
/*! exports provided: GridService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GridService", function() { return GridService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
/* harmony import */ var rxjs_add_operator_do__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/add/operator/do */ "./node_modules/rxjs-compat/_esm5/add/operator/do.js");
/* harmony import */ var rxjs_add_operator_catch__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/add/operator/catch */ "./node_modules/rxjs-compat/_esm5/add/operator/catch.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var GridService = /** @class */ (function () {
    function GridService(http) {
        this.http = http;
    }
    //get data from api
    GridService.prototype.getData = function (url) {
        return this.http.get(url)
            .map(function (res) { return res; });
    };
    GridService.prototype.postData = function (url, data) {
        // console.log(url);
        // console.log(data);
        var dataPost = {
            data: data
        };
        // console.log(dataPost);
        return this.http.post(url, dataPost)
            .map(function (res) { return res; });
    };
    GridService.prototype.getKeyData = function (url) {
        return this.http.get(url)
            .map(function (res) { return res; });
    };
    GridService.prototype.getTestData = function () {
        return this.http.get('http://localhost:3000/api/v2/test');
    };
    // getToken() {
    //   return localStorage.getItem('token')
    // }
    GridService.prototype.newMethod = function () {
        return this.http.get('http://localhost:3000/api/v2/test');
    };
    GridService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], GridService);
    return GridService;
}());



/***/ }),

/***/ "./src/app/grid/grid.component.css":
/*!*****************************************!*\
  !*** ./src/app/grid/grid.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#main{\r\nheight: "+mainDivHeight+" overflow-y: scroll;overflow-x: hidden;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZ3JpZC9ncmlkLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQSxjQUFjLENBQUMsbUJBQW1CLG1CQUFtQjtDQUNwRCIsImZpbGUiOiJzcmMvYXBwL2dyaWQvZ3JpZC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI21haW57XHJcbmhlaWdodDogNzAwcHg7IG92ZXJmbG93LXk6IHNjcm9sbDtvdmVyZmxvdy14OiBoaWRkZW47XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/grid/grid.component.html":
/*!******************************************!*\
  !*** ./src/app/grid/grid.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--<app-biospecimen></app-biospecimen>-->\r\n\r\n<div id=\"main\">\r\n<div class=\"terms container\" id=\"terms\">\r\n  <h2>Privacy Policy</h2>\r\n  <p style=\"text-align: left\">\r\n    Thank you for visiting the Arizona Biospecimen Locator (ABL) Web Portal. The ABL is controlled by the Arizona Biospecimen Consortium (ABC or we). As part of its commitment to maintain the privacy of users of the ABL, the ABC has developed this Privacy Policy, which tells you about the following:\r\n\r\n    The information we collect from you\r\n    The ways we use your information\r\n    With whom we share your information\r\n    The choices and obligations you have about your information\r\n    How we protect your information\r\n    How you can contact us\r\n    This Privacy Policy only applies to information that we collect from you through the ABL. You must be at least 18 years old to use the ABL. We do not knowingly collect information from anyone under the age of 18, and if you are not 18, you may not use the ABL.\r\n\r\n    If you do not want your information collected or used in the ways we describe in this Privacy Policy, you should not use the ABL. Your use of the ABL means you agree with this Privacy Policy and the Terms of Use on the ABL. You may also be linked to eSample through the use of the ABL. Please note that eSample has its own Privacy Policy.\r\n  </p>\r\n  <h4 style=\"text-align: left\">What Information Do We Collect About You and How Do We Collect It?</h4>\r\n  <p style=\"text-align: left\">When you use the ABL you submit certain information that identifies you (Personal Information). The Personal Information includes your name, contact information, and any other identifiable information that you submit. We do not collect Personal Information automatically without your knowledge. You will be required to register to use the ABL, and the information you submit in your registration that identifies you is also Personal Information. You may be able to use some of the features of the ABL without submitting Personal Information, but you will not be able to make full use of the ABL if you do not submit Personal Information.\r\n\r\n    We may collect Non-Personal Information from you. For example, we may collect Non-Personal Information through “cookies” or through other analytical services. A cookie is a small amount of data, such as an anonymous unique identifier that is sent to your browser from a website’s computers and stored on your computer’s hard drive.\r\n\r\n    We may use cookies to collect general, non-personal, statistical information about the use of the ABL, such as how many visitors visit a specific page of the ABL, how long they stay on that page, the sequence of pages accessed, and which hyperlinks, if any, they click on. We may also use cookies to identify your computer when you re-visit the ABL or to make some of the features of the ABL work more quickly or automatically.\r\n\r\n    How Do We Use Information?\r\n    We use your Personal Information to make the ABL available to you. For example, we may use your Personal Information for purposes of authentication and security. We may also use Personal Information collected when using the ABL (through Velos eSample) for future outreach about research topics. Your Personal Information may also be made available to ABC members and ABC administrators for them to notify you about research opportunities. ABC members mean the third-party ABC member biobanks and members that participate in ABC. The list of biobanks and members may change from time to time. The ABL never uses Personal Information for commercial marketing or any purpose unrelated to the ABC mission and goals.\r\n\r\n    The ABC requires ABC members to treat research protocols submitted to the ABL as confidential. The protocols submitted through the ABL will only be used for the purpose of reviewing and approving research requests for biospecimens through the ABL. This confidentiality provision does not cover summary research results submitted by a researcher to comply with the terms of the Material Transfer Agreement. We may also summarize, de-identify or aggregate information about the protocols and use this information in a non-identifiable manner. Requests for samples will be submitted through eSample, and you should review the eSample Privacy Policy for additional information on how information collected in eSample may be used or disclosed.\r\n\r\n    When users send email messages containing Personal Information to the ABL contact email or through ABL website forms, only ABC members and their designated staff members requiring access to the emails may view or answer them. This includes information sent regarding feedback related to the ABL website and requested biospecimens. If appropriate, the email messages may also be disclosed to an administrator of eSample.\r\n\r\n    We may also use your Personal Information as needed to operate the ABC. For instance, we may use Personal Information to administer your account, communicate with you about the ABL, or respond to inquiries that you send to us.\r\n\r\n    Uses of the ABL website and eSample storefront are subject to all applicable state and federal laws.\r\n\r\n    We may use Non-Personal Information from you and other users of the ABL for research and analysis, such as trend identification and performance enhancements. We may analyze user activity on the ABL. This information is blended with information from other users of the ABL and is not linked to any particular individual.\r\n\r\n    What Information Do We Share With Third Parties?\r\n    Except as described in this Privacy Policy, we do not sell, exchange, or share Personal Information with third parties. We share your Personal and Non-Personal Information as necessary to provide our services. For instance, we may share your Personal and Non-Personal Information with companies we have hired to provide services for us, such as hosting our servers and collecting and processing information on our behalf. We may also share your Personal and Non-Personal Information with our ABC members as we’ve described elsewhere in this Privacy Policy.\r\n\r\n    In some special circumstances, we may be required or we may choose to share your information with third parties. For example, we may share Personal Information to investigate, prevent, or take action regarding illegal activities, suspected fraud, situations involving potential threats to the physical safety of any person, violations of this Privacy Policy or the Terms of Use, or as otherwise required or permitted by law.\r\n\r\n    We may also transfer Personal and Non-Personal Information if ABC is acquired by or merged with another entity or undergoes a similar business restructuring or transaction, or if ABC files for bankruptcy.\r\n\r\n    What Choices and Obligations Do You Have About Your Information?\r\n    To protect your security and privacy, we require that you personally enter the Personal Information that you provide to us and that the information be current, complete, and accurate. We cannot verify the accuracy of the information you provide to us, and we disclaim any duty to do so.\r\n\r\n    You are free to request the Personal Information that we have about you by contacting us using one of the methods on the Contact Us page.  In addition, at any time, you can request that we modify or delete your Personal Information by contacting us by one of the methods on the Contact Us page.\r\n\r\n    In some cases, you may be able to opt out of certain services or uses of your Personal Information. If you do opt out, we will respect your decision.\r\n\r\n    If you request that we delete your Personal Information, our system may maintain copies of your Personal Information for audit purposes. Personal Information that is updated, modified, or deleted may be retained by us for compliance and legal reasons. We will retain Personal Information in accordance with our record retention policies.\r\n\r\n    How Do We Protect Your Information?\r\n    ABC’s staff, third party vendors, and hosting partners provide the hardware, software, networking, storage, security, backup, and related technology required to run the ABL Web Portal. We utilize reasonable safeguards to protect your Personal Information.\r\n\r\n    We utilize the competencies of third party vendors and hosting partners to provide physical security and backup of data. Despite the security measures employed by ABC and its vendors, you should be aware that it is impossible to guarantee absolute security with respect to information sent over the Internet.\r\n\r\n    Changes to This Policy\r\n    If we decide to change our privacy policies and practices, we will post those changes to this Privacy Policy, the homepage, and other places we deem appropriate so that you are aware of what information we collect, how we use it, and under what circumstances, if any, we disclose it. We reserve the right to modify this Privacy Policy at any time, so please review it frequently. Unless otherwise stated, changes will be effective immediately so you are advised to review this Privacy Policy regularly. If you do not agree to the modified Privacy Policy, you should discontinue your use of the ABL. Your continued use of the ABL will constitute your acceptance of the terms of the modified Privacy Policy.\r\n\r\n    Hyperlinks\r\n    The ABL website may provide links to third party websites for your convenience and information. If you access those links, you will leave the ABL website. With the exception of the eSample storefront, the ABL and its governing consortium do not control those sites or their privacy practices, which may differ from the ABL’s. We do not endorse or make any representations about third party websites. This Privacy Policy does not cover the personal data you choose to give to third parties. We encourage you to review the privacy policy of any company before submitting your personal information. Some third party companies may choose to share their personal data with the ABL; that sharing is governed by that third party company’s privacy policy.\r\n  </p>\r\n  <button class=\"btn btn-primary\" [routerLink]=\"['bio-specimen']\">Agree</button>\r\n</div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/grid/grid.component.ts":
/*!****************************************!*\
  !*** ./src/app/grid/grid.component.ts ***!
  \****************************************/
/*! exports provided: GridComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GridComponent", function() { return GridComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import {headersCTX, keysCTX} from "../../assets/data/headerCTX";
var GridComponent = /** @class */ (function () {
    // rows: {};
    // columns: {};
    // headersCTX = headersCTX;
    // keysCTX = keysCTX;
    function GridComponent(router) {
        this.router = router;
    }
    GridComponent.prototype.ngOnInit = function () {
        // Retrieve data from api
        // this.gridService.getTestData().subscribe(grid => {
        //   this.rows = grid;
        // console.log(grid);
        // });
        // console.log(this.headersCTX);
        // console.log(this.keysCTX);
        // this.gridService.studyStatusMetricsDataKeys().subscribe( data => {
        //   this.columns = data;
        // console.log(data);
        // });
    };
    GridComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-grid',
            template: __webpack_require__(/*! ./grid.component.html */ "./src/app/grid/grid.component.html"),
            styles: [__webpack_require__(/*! ./grid.component.css */ "./src/app/grid/grid.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], GridComponent);
    return GridComponent;
}());



/***/ }),

/***/ "./src/app/mainParameters.service.ts":
/*!*******************************************!*\
  !*** ./src/app/mainParameters.service.ts ***!
  \*******************************************/
/*! exports provided: MainParametersService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainParametersService", function() { return MainParametersService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _grid_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./grid.service */ "./src/app/grid.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MainParametersService = /** @class */ (function () {
    function MainParametersService(http, gridService) {
        this.http = http;
        this.parameters = {};
    }
    MainParametersService.prototype.getMainParameters = function () {
        return this.parameters = {
            StudyFormResponse: {
                formFieldResponses: {
                    field: [{
                            fieldIdentifier: {
                                PK: 22039
                            },
                            uniqueID: 'er_def_date_01',
                            value: ''
                        },
                        {
                            fieldIdentifier: {
                                PK: 22041
                            },
                            uniqueID: 'req_personalinfo',
                            value: 'Velos Admin'
                        },
                        {
                            fieldIdentifier: {
                                PK: 22076
                            },
                            uniqueID: 'req_spec1',
                            value: ''
                        },
                        {
                            fieldIdentifier: {
                                PK: 22077
                            },
                            uniqueID: 'req_spec2',
                            value: ''
                        },
                        {
                            fieldIdentifier: {
                                PK: 22078
                            },
                            uniqueID: 'req_spec3',
                            value: ''
                        },
                        {
                            fieldIdentifier: {
                                PK: 22079
                            },
                            uniqueID: 'req_spec4',
                            value: ''
                        },
                        {
                            fieldIdentifier: {
                                PK: 22080
                            },
                            uniqueID: 'req_spec5',
                            value: ''
                        }]
                },
                formFillDt: '',
                formId: {
                    PK: 773
                },
                formVersion: '1',
                formStatus: {
                    code: 'working',
                    description: 'Work In Progress',
                    type: 'fillformstat'
                },
                studyIdentifier: {
                    PK: 945
                }
            }
        };
    };
    MainParametersService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _grid_service__WEBPACK_IMPORTED_MODULE_2__["GridService"]])
    ], MainParametersService);
    return MainParametersService;
}());



/***/ }),

/***/ "./src/app/pipe.sort.ts":
/*!******************************!*\
  !*** ./src/app/pipe.sort.ts ***!
  \******************************/
/*! exports provided: SortPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SortPipe", function() { return SortPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var SortPipe = /** @class */ (function () {
    function SortPipe() {
    }
    SortPipe.prototype.transform = function (arr, args) {
        if (!arr) {
            return null;
        }
        arr.sort(function (a, b) {
            if (!a.hasOwnProperty(args) || !b.hasOwnProperty(args)) {
                return 0;
            }
            var A = (typeof a[args] === 'string') ? a[args].toUpperCase() : a[args];
            var B = (typeof b[args] === 'string') ? b[args].toUpperCase() : b[args];
            if (!isNaN(A && B)) {
                return A - B;
            }
            else {
                return A < B ? -1 : A > B ? 1 : 0;
            }
        });
        return arr;
    };
    SortPipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
            name: 'sort'
        })
    ], SortPipe);
    return SortPipe;
}());



/***/ }),

/***/ "./src/app/pipe.ts":
/*!*************************!*\
  !*** ./src/app/pipe.ts ***!
  \*************************/
/*! exports provided: KeyPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "KeyPipe", function() { return KeyPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var KeyPipe = /** @class */ (function () {
    function KeyPipe() {
    }
    KeyPipe.prototype.transform = function (value, args) {
        var arr = [];
        for (var prop in value) {
            arr.push({ key: prop, value: value[prop] });
        }
        return arr;
    };
    KeyPipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
            name: "keys"
        })
    ], KeyPipe);
    return KeyPipe;
}());



/***/ }),

/***/ "./src/app/search-patient/search-patient.component.css":
/*!*************************************************************!*\
  !*** ./src/app/search-patient/search-patient.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NlYXJjaC1wYXRpZW50L3NlYXJjaC1wYXRpZW50LmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/search-patient/search-patient.component.html":
/*!**************************************************************!*\
  !*** ./src/app/search-patient/search-patient.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div style=\"margin: 5px; float: left\">\r\n  <input [(ngModel)] = \"searchText\" placeholder=\"search\">\r\n</div>\r\n<div>\r\n  <label for=\"sort-field\">Sort By</label>\r\n  <select name=\"sort-field\" id=\"sort-field\" [(ngModel)]=\"sortField\">\r\n    <option *ngFor=\"let column of columns | keys\" [ngValue]=\"column.value\">\r\n      {{column.key}}\r\n    </option>\r\n  </select>\r\n</div>\r\n\r\n<table class=\"table table-hover\">\r\n  <thead class=\"mdb-color light-blue darken-4\">\r\n  <tr class=\"text-white\">\r\n    <th *ngFor=\"let column of columns | keys\" style=\"text-align: center\">\r\n      {{column.key}}\r\n    </th>\r\n  </tr>\r\n  </thead>\r\n  <tbody>\r\n  <tr *ngFor=\"let row of rows | data | filter: searchText | sort:[sortField]\">\r\n    <td *ngFor=\"let column of columns | keys \">\r\n      {{row[column.value]}}\r\n    </td>\r\n  </tr>\r\n  </tbody>\r\n</table>\r\n\r\n"

/***/ }),

/***/ "./src/app/search-patient/search-patient.component.ts":
/*!************************************************************!*\
  !*** ./src/app/search-patient/search-patient.component.ts ***!
  \************************************************************/
/*! exports provided: SearchPatientComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchPatientComponent", function() { return SearchPatientComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _grid_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../grid.service */ "./src/app/grid.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SearchPatientComponent = /** @class */ (function () {
    function SearchPatientComponent(gridService) {
        this.gridService = gridService;
    }
    SearchPatientComponent.prototype.ngOnInit = function () {
        var _this = this;
        var URL = "api/v2/searchPatient";
        var URL_KEY = "velos/jsp/dist/assets/data/searchPatientDataKey.json";
        this.gridService.getData(URL).subscribe(function (grid) {
            _this.rows = grid;
            console.log(_this.rows);
        });
        this.gridService.getData(URL_KEY).subscribe(function (keys) {
            _this.columns = keys;
            // console.log(this.columns);
        });
    };
    SearchPatientComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-search-patient',
            template: __webpack_require__(/*! ./search-patient.component.html */ "./src/app/search-patient/search-patient.component.html"),
            styles: [__webpack_require__(/*! ./search-patient.component.css */ "./src/app/search-patient/search-patient.component.css")]
        }),
        __metadata("design:paramtypes", [_grid_service__WEBPACK_IMPORTED_MODULE_1__["GridService"]])
    ], SearchPatientComponent);
    return SearchPatientComponent;
}());



/***/ }),

/***/ "./src/app/study-status-metrics/study-status-metrics.component.css":
/*!*************************************************************************!*\
  !*** ./src/app/study-status-metrics/study-status-metrics.component.css ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\ntbody {\r\n  display:block;\r\n  height:800px;\r\n  overflow:auto;\r\n}\r\nthead, tbody tr {\r\n  display:table;\r\n  width:100%;\r\n  table-layout:fixed;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3R1ZHktc3RhdHVzLW1ldHJpY3Mvc3R1ZHktc3RhdHVzLW1ldHJpY3MuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQ0E7RUFDRSxjQUFjO0VBQ2QsYUFBYTtFQUNiLGNBQWM7Q0FDZjtBQUNEO0VBQ0UsY0FBYztFQUNkLFdBQVc7RUFDWCxtQkFBbUI7Q0FDcEIiLCJmaWxlIjoic3JjL2FwcC9zdHVkeS1zdGF0dXMtbWV0cmljcy9zdHVkeS1zdGF0dXMtbWV0cmljcy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbnRib2R5IHtcclxuICBkaXNwbGF5OmJsb2NrO1xyXG4gIGhlaWdodDo4MDBweDtcclxuICBvdmVyZmxvdzphdXRvO1xyXG59XHJcbnRoZWFkLCB0Ym9keSB0ciB7XHJcbiAgZGlzcGxheTp0YWJsZTtcclxuICB3aWR0aDoxMDAlO1xyXG4gIHRhYmxlLWxheW91dDpmaXhlZDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/study-status-metrics/study-status-metrics.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/study-status-metrics/study-status-metrics.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div style=\"margin: 5px; float: left\">\r\n  <input [(ngModel)] = \"searchText\" placeholder=\"search\">\r\n</div>\r\n<div>\r\n  <input type=\"button\" value=\"send\" id=\"sendCheckedValue\"  (click)=\"saveInArray()\">\r\n</div>\r\n<div>\r\n  <label for=\"sort-field\">Sort By</label>\r\n  <select name=\"sort-field\" id=\"sort-field\" [(ngModel)]=\"sortField\">\r\n    <option *ngFor=\"let column of columns | keys\" [ngValue]=\"column.value\">\r\n      {{column.key}}\r\n    </option>\r\n  </select>\r\n</div>\r\n\r\n  <table class=\"table table-hover\" id=\"angTable\">\r\n    <thead class=\"mdb-color light-blue darken-4\">\r\n    <tr class=\"text-white\">\r\n      <th>\r\n        Select\r\n      </th>\r\n      <th *ngFor=\"let column of columns | keys\" style=\"text-align: center\">\r\n        {{column.key}}\r\n      </th>\r\n    </tr>\r\n    </thead>\r\n    <tbody>\r\n    <tr *ngFor=\"let row of rows | filter: searchText | sort:[sortField]\">\r\n      <td>\r\n        <input type=\"checkbox\" name=\"copy\">{{i}}\r\n      </td>\r\n      <td *ngFor=\"let column of columns | keys \">\r\n        {{row[column.value] | uppercase}}\r\n      </td>\r\n    </tr>\r\n    </tbody>\r\n  </table>\r\n\r\n"

/***/ }),

/***/ "./src/app/study-status-metrics/study-status-metrics.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/study-status-metrics/study-status-metrics.component.ts ***!
  \************************************************************************/
/*! exports provided: StudyStatusMetricsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudyStatusMetricsComponent", function() { return StudyStatusMetricsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _grid_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../grid.service */ "./src/app/grid.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var StudyStatusMetricsComponent = /** @class */ (function () {
    function StudyStatusMetricsComponent(gridService) {
        this.gridService = gridService;
    }
    StudyStatusMetricsComponent.prototype.saveInArray = function () {
        // console.log(this.rows.length);
        var index;
        var dataArray = [];
        var inputs = document.getElementsByTagName('input');
        for (var i = 0; i < inputs.length; i++) {
            if (inputs[i].checked == true) {
                index = i - 1;
                var selectedRow = (document.getElementsByTagName('tr'))[index].cells;
                for (var j = 0; j < selectedRow.length; j++) {
                    // if (selectedRow[j].innerText != "") {
                    //   console.log(selectedRow[j]);
                    // }
                    var selectedRowData = selectedRow[j].innerText;
                    dataArray.push((_a = {}, _a[j] = selectedRowData, _a));
                }
            }
        }
        console.log(dataArray);
        var _a;
    };
    StudyStatusMetricsComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Retrieve data from api
        var URL = "/api/v2/getStudyStatusMetrics";
        var URL_KEY = "velos/jsp/dist/assets/data/data.json";
        this.gridService.getData(URL).subscribe(function (grid) {
            _this.rows = grid;
        });
        this.gridService.getKeyData(URL_KEY).subscribe(function (keys) {
            _this.columns = keys;
        });
    };
    StudyStatusMetricsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-study-status-metrics',
            template: __webpack_require__(/*! ./study-status-metrics.component.html */ "./src/app/study-status-metrics/study-status-metrics.component.html"),
            styles: [__webpack_require__(/*! ./study-status-metrics.component.css */ "./src/app/study-status-metrics/study-status-metrics.component.css")]
        }),
        __metadata("design:paramtypes", [_grid_service__WEBPACK_IMPORTED_MODULE_1__["GridService"]])
    ], StudyStatusMetricsComponent);
    return StudyStatusMetricsComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Parminder_Singh\dignity_storefrontnew\fabric\fabric\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map