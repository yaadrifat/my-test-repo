<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<SCRIPT language="JavaScript1.1">
window.name="formWin";
function openuserwin(userId) {
      windowName = window.open("viewuser.jsp?userId=" + userId,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=500,height=400")
	windowName.focus();
;}
</script>
<%@page language="java" import="com.velos.eres.web.objectSettings.ObjectSettingsCache,com.velos.eres.web.objectSettings.ObjectSettings"%>
<%@page import="com.velos.esch.business.common.SchMsgTxtDao" %>
<%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.web.user.UserJB,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.ulink.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.common.MsgCntrDao, com.velos.eres.business.common.*,com.velos.eres.web.grpRights.GrpRightsJB"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%

ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
ArrayList tabList = null;
String titleStr = "";
if (sessionmaint.isValidSession(request.getSession(true)))
{
    String accId = (String) request.getSession(true).getValue("accountId");
    
    ArrayList tempList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "top_menu");
    for (int iX=0; iX<tempList.size(); iX++) {
        ObjectSettings settings = (ObjectSettings) tempList.get(iX);
        if ("irb_menu".equals(settings.getObjSubType())) {
            if (settings.getObjDispTxt() != null) {
                titleStr += settings.getObjDispTxt();
            }
            break;
        } 
    }
    tempList = null;

    tempList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "irb_menu");
    for (int iX=0; iX<tempList.size(); iX++) {
        ObjectSettings settings = (ObjectSettings) tempList.get(iX);
        if ("landing_menu".equals(settings.getObjSubType())) {
            if (settings.getObjDispTxt() != null) {
            	Object[] arguments = {titleStr,settings.getObjDispTxt()};
            	titleStr = VelosResourceBundle.getLabelString("L_Gtgt",arguments);
                /*titleStr += " >> " + settings.getObjDispTxt();*****/
            }
            break;
        } 
    }
    tempList = null;
}
%>
<title><%=titleStr %></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<%@ page language = "java" import = "com.velos.eres.business.ulink.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.EJBUtil"%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*" %>
<% 
String src;
src= request.getParameter("srcmenu");
String selectedTab = request.getParameter("selectedTab");
HttpSession tSession = request.getSession(true); 
String accountId = "";
String studyId = "";
String grpId = "";
String usrId = "";
if (sessionmaint.isValidSession(tSession))
{
    accountId = (String) tSession.getValue("accountId");
    studyId = (String) tSession.getValue("studyId");
    grpId = (String) tSession.getValue("defUserGroup");
    usrId = (String) tSession.getValue("userId");
    
    if(accountId == null || accountId == "") {
    %>
		<jsp:include page="timeout.html" flush="true"/>
	<%
        return;
    }
    
    if ("N".equals(request.getParameter("mode"))) {
        studyId = "";
        tSession.removeAttribute("studyId");
    }
}
%>

<jsp:include page="include.jsp" flush="true"/>
<jsp:include page="ui-include.jsp" flush="true"></jsp:include>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<body>
<div id="overDiv" style="position:absolute;visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<%
	String from = "version";
	String tab = request.getParameter("selectedTab");
	  
	String studyIdForTabs = request.getParameter("studyId");
 	
    String includeTabsJsp = "irbnewtabs.jsp";
%>
<DIV class="BrowserTopn" id = "divtab">
</DIV>

<DIV class="tabDefBotN" id = "div1" style="border:0;">
<%
  if (sessionmaint.isValidSession(tSession))
  {
    int pageRight = 0;
    String initParam = null;
    if ("N".equals(request.getParameter("mode")) || studyId == null || studyId.length() == 0) {
        initParam = "&mode=N";
  }  
  else 
  {
    initParam = "&mode=M&studyId="+studyId;
  }
  UserJB user = (UserJB) tSession.getValue("currentUser");
 %>
  	<div id="newirbPage">
  		</br></br>
  		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  		<a href="irbnewsubmission.jsp?mode=N"><%=LC.L_subm %></a> &nbsp;&nbsp;
  		<a href="irbstudy.jsp?mode=N"><%=LC.Std_Studies %></a> &nbsp;&nbsp;
  		<a href="irbnewinit.jsp?srcmenu=tdMenuBarItem3&selectedTab=irb_init_tab&mode=N"><%=LC.L_initiate_new_app%></a> &nbsp;&nbsp;
  		<a href="irbongoing.jsp"><%=LC.L_ong_Stud%></a> &nbsp;&nbsp;
  		<a href="formfilledaccountbrowser.jsp?entryflag=Y"><%=LC.L_Res_REg %></a></td>
  	</div>  
  	<jsp:useBean id="ulinkB" scope="request" class="com.velos.eres.web.ulink.ULinkJB"/>
  	<%
  		int usrIdNew=0;
		int counter = 0;

		usrIdNew = user.getUserId();
   		UsrLinkDao usrLinkDao = ulinkB.getULinkValuesByUserId(usrIdNew);
   		ArrayList lnksIds = usrLinkDao.getLnksIds();
   		ArrayList lnksUris = usrLinkDao.getLnksUris();
   		ArrayList lnksDescs = usrLinkDao.getLnksDescs();
   		ArrayList lnksGrps = usrLinkDao.getLnksGrpNames();
   		String lnkUri = null;
		String lnkDesc = null;
   		String oldGrp = null;
   		String lnkGrp = null;
   		int len = lnksIds.size();
   		
	%>
  	<table width="100%" border="0">
	<tr height="10"><td colspan="2"></td></tr>
	<tr valign="top">

	<td width="50%" align="left" >
    <Form name="usrlnkbrowser" method="post" action="" onSubmit="">
      	<table class="outline" width="100%" cellspacing="0" cellpadding="0"  height="130">
			<tr><th class="labelFont" width="30%" align="left" height="20"><%=LC.L_My_Links%><%--My Links*****--%></th>
			<th width="70%" class="rhsFont" nowrap="nowrap" align="right"><a href="ulinkBrowser.jsp?user=<%=usrIdNew%>&srcmenu=tdmenubaritem2&selectedTab=2"><img class="headerImage" title="<%=LC.L_Edit%>" src="./images/edit.gif" border ="0"/><%//=LC.L_Edit%><%--Edit*****--%></a></th>
			</tr>
			<%if(len==0){%>
				 <tr class="browserEvenRow"><td></td><td ></td></tr>
			<%} %>
        			<%
				for(counter = 0;counter<len;counter++)
				{
					lnkUri=((lnksUris.get(counter))==null)?"-":(lnksUris.get(counter)).toString();
					lnkDesc=((lnksDescs.get(counter))==null)?"-":(lnksDescs.get(counter)).toString();
					lnkGrp = ((lnksGrps.get(counter))==null)?"-":(lnksGrps.get(counter)).toString();
				
				 if (counter%2==0){%>
					        <tr class="browserEvenRow">
				        <%}else{
				        	%>
					        <tr class="browserOddRow">
				     
			        <%} if ( ! lnkGrp.equals(oldGrp)){ %>
						    <td class="lhsFont" width="35%"> <%= lnkGrp %> <br>
				     <%}else{%>
						    <td class="lhsFont" width="65%">&nbsp;<br>
				     <%}%>

				         </td><td class="lhsFont"><A href="<%= lnkUri%>" target="_new"><%= lnkDesc%></A> </td>
			        </tr>
				    <%
					oldGrp = lnkGrp;
		 		}
				%>
				
		</table>
	</Form>
		<%
			String accId = (String) request.getSession(true).getValue("accountId");
			usrLinkDao = ulinkB.getULinkValuesByAccountId(EJBUtil.stringToNum(accId),"lnk_gen");
		   	lnksIds = usrLinkDao.getLnksIds();
			lnksUris = usrLinkDao.getLnksUris();
			lnksDescs = usrLinkDao.getLnksDescs();
			lnksGrps = usrLinkDao.getLnksGrpNames();
			len = lnksIds.size();
			counter = 0;
		%>
	</td>

	<td width="50%" align="right"  class="plainbg">

	<Form name="acclnkbrowser" method="post" action="" onSubmit="">
		 <table class="outline"  width="99%" cellspacing="0" cellpadding="0" height="130">
			<tr>
			<th class="labelFont" width="30%" align="left" height="20"><%=LC.L_Quick_Links%><%--Quick Links*****--%></th>
			<th width="70%" class="rhsFont" nowrap="nowrap" align="right"><a href="accountlinkbrowser.jsp?srcmenu=tdmenubaritem2&selectedTab=6"><img class="headerImage" title="<%=LC.L_Edit%>" src="./images/edit.gif" border ="0"/><%//=LC.L_Edit%><%--Edit*****--%></a></th>
			</tr>
			<%if(len==0){%>
				 <tr class="browserEvenRow"><td></td><td ></td></tr>
			<%} %>
			      <%
			        oldGrp = null;
				    for(counter = 0;counter<len;counter++)
					{
						lnkUri=((lnksUris.get(counter))==null)?"-":(lnksUris.get(counter)).toString();
						lnkDesc=((lnksDescs.get(counter))==null)?"-":(lnksDescs.get(counter)).toString();
						lnkGrp = ((lnksGrps.get(counter))==null)?"-":(lnksGrps.get(counter)).toString();
				        if (counter%2==0){%>
					        <tr class="browserEvenRow">
				        <%}else{
				        	%>
					        <tr class="browserOddRow">
				        <%
				        } if ( ! lnkGrp.equals(oldGrp)){ %>
						        <td class="lhsFont quickLinks_border" width="35%"> <%= lnkGrp %> <br>
						<%}else{%>
						        <td class="lhsFont quickLinks_border" width="70%">&nbsp;<br>
						<%}%>
					         </td><td class="lhsFont quickLinks_border"><A href="<%= lnkUri%>" target="_new"><%= lnkDesc%></A></td>
				        </tr>
				        <%
						oldGrp = lnkGrp;
			 		}
				
				%>
		</table>
	</Form></td></tr>
	<tr><td >&nbsp;</td></tr>
    </td></tr></table>
    
    
<% }//end of if body for isValidSession
  else
  {
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
  }
  %>
  </DIV>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

<div class ="mainMenu" id = "emenu" > 
  <jsp:include page="getmenu.jsp" flush="true"/> 
</div>
</body>
</html>