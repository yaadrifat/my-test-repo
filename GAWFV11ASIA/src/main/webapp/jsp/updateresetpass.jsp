<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="include.jsp" flush="true"/>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@page import="com.velos.eres.service.util.DateUtil"%>

<link href="styles/login.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" type="image/x-icon" href="./favicon.ico" />
<jsp:include page="jqueryUtils.jsp" flush="true"/>	
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="addressB" scope="page" class="com.velos.eres.web.address.AddressJB" />

 
<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>

<%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.business.common.*,java.util.*,java.text.*,com.velos.eres.service.util.*"%>
<%
 HttpSession tSession = request.getSession(true); 
 
String Flag = (String)request.getParameter("flag")==null?"":request.getParameter("flag");

if (sessionmaint.isValidSession(tSession)|| Flag.equalsIgnoreCase("1"))
   {  
	int userpk=0;
	String password="";
	String esign="";
	if(Flag.equalsIgnoreCase("1")){
     
	  password=request.getParameter("password");
	  esign=request.getParameter("esignBox");
	 String login=request.getParameter("lid");
	 String lname=request.getParameter("lname");
	 String lcode=request.getParameter("lcode");
	 String email=request.getParameter("email");
	 userpk = userB.getResetNew(login, lname, lcode, email);
	 }	
 String fromPage = (String)request.getParameter("fromPage");
 boolean incorrectesign=false;
 if (fromPage.equals("User") && StringUtil.stringToNum(Flag)==0) {
 %>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<% 
 String eSign = request.getParameter("eSign");
 String oldESign = (String) tSession.getValue("eSign");
 if(!oldESign.equals(eSign)) {
 	incorrectesign=true;
 }else {
	incorrectesign=false;
 }
 }

 if (incorrectesign==true) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {
    
	String supportEmail = Configuration.SUPPORTEMAIL;
	   
 String ipAdd = (String) tSession.getValue("ipAdd");
// String user = null;
 String userPass="";
 String usereSign="";
 String messageSubtext="";
 String addressId="";
 

 String messageFrom = "";
 String messageText = "";
 String messageSubject = "";
 String messageHeader = "";
 String messageFooter = "";		
 String completeMessage = "";

 String messageTo = "";
 int rows = 0;
 String	usrEmail="";
 String userLoginName ="";
 String siteUrl = "";



 int ret =0;
 double randomNumber=Math.random();
 String randomNum=String.valueOf(randomNumber);
 String userESignExpiryDate="";
 String userPwdExpiryDate="";
  password=request.getParameter("password");
  esign=request.getParameter("esignBox");
 String userId="";

 if(userpk==0){
	 userId  = String.valueOf(request.getParameter("userId"));
	 }
 else{
	 userId = String.valueOf(userpk);
 }



 int user = EJBUtil.stringToNum(userId);

 userB.setUserId(user);
 userB.getUserDetails();
 addressId=userB.getUserPerAddressId();
 addressB.setAddId(EJBUtil.stringToNum(addressId));
 addressB.getAddressDetails();
 usrEmail=addressB.getAddEmail();
 userLoginName =userB.getUserLoginName();

 String userMailStatus = "";
 int metaTime = 1;

// Added by Rajeev K - 02/18/04
// Begin
// get site URL
	rows = 0;
	CtrlDao urlCtrl = new CtrlDao();	
	urlCtrl.getControlValues("site_url");
	rows = urlCtrl.getCRows();
   	if (rows > 0)
	   {
	   	siteUrl = (String) urlCtrl.getCValue().get(0);
	   }
// End

if(user!=0){
if((password!=null)&&(esign!=null)){
 //VCTMS-1 :April-15th-2011
	userPass=randomNum.substring(2,10);
	usereSign=randomNum.substring(11,15);
	//messageSubtext=" \nNew Password    : " + userPass +  "\nNew e-Signature : " + usereSign + "\nApplication URL : " + siteUrl;
	userB.setUserPwd(Security.encryptSHA(userPass));
	userB.setUserESign(usereSign);
	userB.setUserPwdExpiryDate(DateUtil.dateToString(DateUtil.addDays((new Date()),-1)));
	userB.setUserESignExpiryDate(DateUtil.dateToString(DateUtil.addDays((new Date()),-1)));
	userB.notifyUserResetInfo(userPass,siteUrl,usereSign,userId,userLoginName,"R"); 
	}
	else
	if(password!=null){
	userPass=randomNum.substring(2,10);
	userB.setUserPwd(Security.encryptSHA(userPass));
	userB.setUserPwdExpiryDate(DateUtil.dateToString(DateUtil.addDays((new Date()),-1)));
	//messageSubtext="\nNew Password    : " + userPass + "\nApplication URL : " + siteUrl; 
	userB.notifyUserResetInfo(userPass,siteUrl,"0000",userId,userLoginName,"RP"); 
	}
	else{
	usereSign=randomNum.substring(11,15);
	userB.setUserESign(usereSign);
	userB.setUserESignExpiryDate(DateUtil.dateToString(DateUtil.addDays((new Date()),-1)));
	//messageSubtext="\nNew e-Signature :   " + usereSign + "\nApplication URL : " + siteUrl; 
	userB.notifyUserResetInfo("NoPass",siteUrl,usereSign,userId,userLoginName,"RE"); 
	}
	userB.setIpAdd(ipAdd);
	//Added for Bug#15318 : Raviesh
	if(StringUtil.stringToNum(Flag)==1){
		userB.setUsrLoggedIn( (userId));
		userB.setIpAdd(request.getRemoteAddr());
		userB.setUserAttemptCount("0");
		userB.setUserStatus("A");
	}
	else{
		userB.setIpAdd(ipAdd);
	userB.setUsrLoggedIn( (tSession.getAttribute("userId").toString()));
	}
	ret = userB.updateUser();	



	messageTo = usrEmail;
}
/*

	rows = 0;
	CtrlDao userCtrl = new CtrlDao();	
	userCtrl.getControlValues("eresuser");
	rows = userCtrl.getCRows();
	if (rows > 0)
	   {
	   	messageFrom = (String) userCtrl.getCDesc().get(0);
	   }
	
	messageSubject = "Request for change of password/esign";	
	messageHeader ="\nDear Velos eResearch Member," ; 

	 messageText = "\n\nAs per your request your Password/e-Signature has been reset. Your new Password/e-Signature is:\n\nLogin ID        : " + userLoginName + messageSubtext + "\n\nYou may of course change your Password/e-Signature at any time should you have any concerns regarding the privacy of your information. To change your Password/e-Signature, you will need to login with the above ID , go to the Personalize Account section and then change your Password/e-Signature.\n\nTo begin using the Velos eResearch system, login to your account.\n **** Important ****\nPlease remember that the above information needs to be kept in a secure place. Any one with access to the above information can access your account information.\n\nVelos eResearch" ;

	messageFooter = "\n\nThis email transmission and any documents, files or previous email messages attached to it may contain information that is confidential or legally privileged. If you are not the intended recipient or a person responsible for delivering this transmission to the intended recipient you are hereby notified that you must not read this transmission and that any disclosure, copying, printing, distribution or use of this transmission is strictly prohibited. If you have received this transmission in error, please immediately notify the sender by telephone or return email and delete the original transmission and its attachments without reading or saving in any manner." ;
	completeMessage = messageHeader  + messageText + messageFooter ;


	try{
		VMailer vm = new VMailer();
    	VMailMessage msgObj = new VMailMessage();
    				
		msgObj.setMessageFrom(messageFrom);
		msgObj.setMessageFromDescription("Velos eResearch Customer Services");
		msgObj.setMessageTo(messageTo);
		msgObj.setMessageSubject(messageSubject);
		msgObj.setMessageSentDate(new Date());
		msgObj.setMessageText(completeMessage);

		vm.setVMailMessage(msgObj);
		userMailStatus = vm.sendMail(); 						
	  }
	  catch(Exception ex)
      {
     	 userMailStatus = ex.toString();
      }
	  */
%>
<br><br><br><br><br>

<%
if (fromPage.equals("User") && StringUtil.stringToNum(Flag)==1){
	System.out.println("PSingh"+fromPage+Flag);
if(userpk>0){
	
	if ( EJBUtil.isEmpty(userMailStatus) )
	  {
	  	metaTime = 1;
		
	  %>
		<p class = "successfulmsg" align = center> <%=MC.M_DataSvdSucc_NotficSent%><%--Data was saved successfully and Notification sent.*****--%> </p>
		<p class = "successfulmsg" align = center> <%=MC.M_Chk_Email%><%--Please Check Your Email.*****--%> </p>
	
	 <%
		 } else
		{
		 	metaTime = 10;
	     %>
			<p class = "redMessage" align = center> <%=MC.M_DataSuccErr_CustSup%><%--Data was saved successfully but there was an error in sending notification to the user.<br>
			Please contact Customer Support with the following message:<BR><%=userMailStatus%>*****--%></p>

		 <%
	  		}%>
	
	<table align="center" >
	<tr>
	<td>

	<button type="button"  style="text-align: center;" onclick ="goBack();"><font color="white"><%=LC.L_Close%></font></button>

	</td>
	</tr>
	</table>
<% }
else{
	%>
	<p class = "redMessage" align = center><%=MC.M_Try_Again %></p>
	<p class = "redMessage" align = center> <%=MC.M_Err_Msg%><%--We are unable to validate this account with information provided.Please double check to make sure all the information is correct and matches the information in your account.<br>
	If you continue to have difficulties,Please contact Customer Support at velos@ncc.re.kr*****--%></p>
	
<table align="center" >
	<tr>
	<td>

	<button type="button"  style="text-align: center;" onclick ="history.go(-1);"><font color="white"><%=LC.L_Close%></font></button>

	</td>
	</tr>
	</table>
<%}


}
%>
<%if(StringUtil.stringToNum(Flag)!=1){ %>
  
  <% if ( EJBUtil.isEmpty(userMailStatus) )
  {
  	metaTime = 1;
	
  %>
	<p class = "successfulmsg" style="margin-top:-230" align = center> <%=MC.M_DataSvdSucc_NotficSent%><%--Data was saved successfully and Notification sent.*****--%> </p>
 <%
	 } else
	{
	 	metaTime = 10;
     %>
		<p class = "redMessage" align = center> <%=MC.M_DataSuccErr_CustSup%><%--Data was saved successfully but there was an error in sending notification to the user.<br>
		Please contact Customer Support with the following message:<BR><%=userMailStatus%>*****--%></p>

	 <%
  		}}
	%>	 	


<%	if(StringUtil.stringToNum(Flag)!=1){%>
  <table align="center" >
<tr>
<td>
<button onClick="window.self.close();"><%=LC.L_Close%></button>
</td>
</tr>
</table>

  <%} 

}//e-sign 
 
	}//end of if body for session
	else
	{%>
	
	<jsp:include page="timeout_adminchild.jsp" flush="true"/>

	<%}%>

	<div class = "myHomebottomPanel">
		<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>  
</BODY>
<SCRIPT>
function goBack(){
	 document.location.href = "ereslogin.jsp";
}	
</SCRIPT>
</HTML>