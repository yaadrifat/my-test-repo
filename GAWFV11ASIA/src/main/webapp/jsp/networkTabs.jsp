<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=11" /> 
    <!--  <meta http-equiv="X-UA-Compatible" content="IE=edge">  -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE" />
	<META HTTP-EQUIV="EXPIRES" CONTENT="-1"> 
    <%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    <jsp:include page="localization.jsp" flush="true"/>
    <jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB"/>
    <jsp:useBean id="groupB" scope="page" class="com.velos.eres.web.group.GroupJB"/>
    <%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
    <title>Networks Tab<%--Manage Account >> Organizations*****--%></title>
	<!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->
    <style type="text/css">
 .ui-autocomplete {
    cursor: default;
    position: absolute;
    background: #FAFAFA;
    max-height: 400px;
    overflow-y: auto;
    overflow-x: hidden;
    }
  .ui-widget-content a text{
   color: :#FAFAFA
	}
 </style>
</head>

<% 
	String src;
	String srcmenu;
	String selectedTab = request.getParameter("selectedTab");
	srcmenu= request.getParameter("srcmenu");
	src= request.getParameter("src");
	String osName = System.getProperty("os.name");
%>
<jsp:include page="panel.jsp" flush="true" > 
	<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

   <style>
   
   tr.browserOddRow TD ,tr.browserEvenRow TD{
   
   word-break:break-all;
   }
   </style>
<script language="javascript">
var mainRowId;
var rowspan=1;
var mnNetworkId;
var chNetworkId;
var siteTypeCount=0;
var expandNtw=[];
var rowId;

/*$j(function() {
	  $j(".research tr:not(.accordion)").hide();
	  $j(".research tr:first-child").show();
	    
	  $j(".research tr.accordion").click(function(){
	      $j(this).nextAll("tr").fadeToggle(500);
	  }).eq(0).trigger('click');
	});*/	
	function noHighlightRow(){
		if(rowId!=undefined || rowId!=null)
		 	$j("#"+rowId+" td").css("background-color", "transparent");
		}
	
function getPadding(padding){
		
		var padding1;
    	var valueOfPadding=padding;
    	if(valueOfPadding!=undefined)
		{
    		valueOfPadding=valueOfPadding.trim();
    		var indexPadding=valueOfPadding.indexOf("padding-left:");
    		var firstIndOrgn=valueOfPadding.indexOf("%;",indexPadding);
    		if(indexPadding>=0)
    		{
	    		padding1=valueOfPadding.substring(indexPadding+13,firstIndOrgn);
	    		padding1=parseInt(padding1);
    		}
		}
	
	  return padding1;
	}

	
	function showHide(event){
		var testing=$j(event).parent().attr("style");
		var rowCount = $j('#table2 tr').length;
		var index1=$j(event).closest('tr').index()+1;
		var parentPadding=getPadding(testing);
		var testpad;
		var childPadding;
		
		if($j(event).attr('src')=='./images/formdown.gif'){
			expandNtw.pop($j(event).closest('tr').attr('id'));
			$j(event).attr('src','./images/formright.gif');
					while(index1<rowCount){
						testpad=$j('#table2 tr:eq(' + (index1)+ ')').children('td:first').attr('style');
						childPadding=getPadding(testpad);
						if(childPadding>parentPadding){
							$j('#table2 tr:eq(' + (index1)+ ')').hide();
							}
						else{
								return;
							}
						index1++;
						}
			}
		else{
			expandNtw.push($j(event).closest('tr').attr('id'));
			$j(event).attr('src','./images/formdown.gif');
			var img='';
			var comparePadding;
			 while(index1<rowCount){
				testpad=$j('#table2 tr:eq(' + (index1)+ ')').children('td:first').attr('style');
				childPadding=getPadding(testpad);
				if(img=='./images/formright.gif'){
					while(comparePadding<childPadding){
						index1++;
						testpad=$j('#table2 tr:eq(' + (index1)+ ')').children('td:first').attr('style');
						childPadding=getPadding(testpad);
						}			
					}
				if(childPadding>parentPadding){
						$j('#table2 tr:eq(' + (index1)+ ')').show();	
					}
				else{
						return;
					}
				comparePadding=childPadding;
				img=$j('#table2 tr:eq(' + (index1)+ ')').children('td:first').find('img').attr('src');
				index1++;
				}
		}
	}

	function expandCollapseAll(){
		
		var index=$j('#row_'+selectedNtwId+'_0').index();
		var buttonVal=$j("#expandCollapseAll").children().text();
		var img;
		
		if(buttonVal==L_Expand_All){
			$j("#expandCollapseAll").children().text(L_Collps_All);
			$j("#table2 tr td:nth-child(1)").each(function(){
				img=$j(this).children(':first').attr('src');
			if(img=='./images/formright.gif')
				$j(this).children(':first').attr('src','./images/formdown.gif');
			});
		$j("#table2 tr:gt("+index+")").show();
		}
		else{
			$j("#expandCollapseAll").children().text(L_Expand_All);
			$j("#table2 tr td:nth-child(1)").each(function(){
				img=$j(this).children(':first').attr('src');
			if(img=='./images/formdown.gif')
				$j(this).children(':first').attr('src','./images/formright.gif');
			});
		$j("#table2 tr:gt("+index+")").hide();
		}
	}

  function functionCall(pageRight){
	 //fetchNetworkRows();
	  hideTableRowSortable();
	  /* if($j('#table2 tr').length>1)
	  siteTypeCount = $j('#table2 tr').length-1;
	  var netPk = 0;
	  $j(".sortable" ).sortable({
		  placeholder: "marker",
		  connectWith: ".sortable",
          axis: "y",
	      revert: true,
	      items: 'tr:not(.not)',
	      start : function(event,ui){
	    	  hideTableRowSortable();
		    if(rowId!=undefined || rowId!=null)
	    	   $j("#"+rowId+" td").css("background-color", "transparent");
	    	$j("#row").css("background-color","#2ECCFA");
	    	rowId=$j(event.target).attr('id');
          	if(rowId=='row')
    		  	 $j("#"+rowId+" td").css("background-color", "#4a2da5");
    	    else
          	    $j("#"+rowId+" td").css("background-color", "#faface ");
      		},
	      update:function(event,ui){
	    	if (f_check_perm(pageRight,'N') == true){
	    	var id=$j(event.target).attr('id');
	    	var orgn = $j(event.target).html();
	    	var orgnInd=0;
	    	var firstIndOrgn=0;
	    	var lastIndOrgn=0;
	    	var sliceOrgn='';
	    		    	
	    	if(id=="row"){
	    		$j("#row").html("<td colspan=\"7\" id=\"dragHere\" style=\"background-color: #2ECCFA; color: white;\">Drag and drop Organization here from the left panel to begin creating a new Network</td>");
	    		rowspan++;
	    		firstIndOrgn=0;
	    		lastIndOrgn=0;
	    		firstIndOrgn = orgn.indexOf("</td>");
	    		if(firstIndOrgn>0){
	    			//lastIndOrgn = orgn.indexOf("</td>",firstIndOrgn);
	    			//if(lastIndOrgn>0){
	    				orgn=orgn.substring(firstIndOrgn+5,orgn.length);
	    			//}	
	    		}
	    		
	    		orgn = orgn.replace("class=\"browserEvenRow draggable ui-draggable\"", "class=\"sortable not\"");
		    	orgn = orgn.replace("class=\"browserOddRow draggable ui-draggable\"", "class=\"sortable not\"");
		    	orgn = orgn.replace("onmousedown=\"functionCall("+pageRight+");\"", "");

		    	//orgn = orgn.replace("<span", "<span id=\"span_"+mainRowId+"_"+siteTypeCount+"\" onclick=\"openDDList(this.id,0);\"");
		    	//orgn = orgn.replace("siteType_"+mainRowId, "siteTypeId_"+mainRowId+"_"+siteTypeCount);
		    	//orgn = orgn.replace("<select", "<select onblur=\"openDDList(this.id,1,\"siteTypeId\");\" ");
		    	orgn = orgn.replace("<span id=\"span_ntType_", "<span class=\"ddSpan\" id=\"span_ntType_"+mainRowId+"_"+siteTypeCount+"\" onclick=\"openDDList(this.id,0,"+pageRight+");\"");
		    	orgn = orgn.replace("<select id=\"siteType_"+mainRowId, "<select class=\"ddSelector\" onblur=\"openDDList(this.id,1,7);\" id=\"siteTypeId_"+mainRowId+"_"+siteTypeCount);
		    	orgn = orgn.replace("<td>","<td style=\"padding-left:0%;\">");
		    	saveNetworkRow(mainRowId,0,0,0,$j("#siteType_"+mainRowId).val());
	    		orgn = orgn.replace(""+mainRowId,"row_"+mnNetworkId+"_0");
	    		siteTypeCount++;
	    		firstIndOrgn=0;
	    		lastIndOrgn=0;
	    		firstIndOrgn = orgn.indexOf("fOpenNetwork(");
	    		if(firstIndOrgn>0){
	    			lastIndOrgn = orgn.indexOf(",0,'')",firstIndOrgn);
	    		  if(lastIndOrgn>0){
	    			  sliceOrgn =  orgn.substring(firstIndOrgn,lastIndOrgn+6);
	    			  orgn = orgn.replace(sliceOrgn,"fOpenNetwork("+mainRowId+","+mnNetworkId+",'0')");
	    		  }
	    		}
	    		//$j('#table2').empty();
	    		//$j("#table2").find("tr:gt(1)").remove();
	    		//$j('#table2').append("<tr  class=\"sortable not\" id=\"row\"><td colspan=\"5\" id=\"dragHere\" style=\"background-color: #2ECCFA; color: white;\">Drag and drop Organization here from the left panel to begin creating a new Network</td>");
	    		//$j("#"+id).closest( "tr" ).after(orgn);
	    		orgn = orgn.replace("</tr>", "<td>Active</td><td><A href=\"#\" onclick=\"fAddMultiUserToSite('"+mnNetworkId+"','0','"+pageRight+"');\"><img title=\"Network Users\" src=\"./images/User.gif\" border=\"0\"></A></td><td><img title=\"Network Appendix\" src=\"./images/Appendix.gif\" border=\"0\"></td><td><a href=\"#\" onclick=\"deleteNetwork("+mnNetworkId+");\"><img src=\"./images/delete.gif\" title=\"Delete\" border=\"0\"></a></td></tr>");
	    		//$j('#table2 tr:last').after(orgn);
	    		$j("#table2").find("tr:gt(1)").remove();
		    	fetchNetworkRows();
		    	hideTableRowSortable();
		    	//$j("#row_"+mnNetworkId+"_0 td").css("background-color", "#D9E9D9");
	        	}
	    	else
		    	{*/	
		    	
         /*if(orgn.indexOf("src=")>=0){
		    	if(orgn.indexOf("src=\"./images/Add.gif\"")>=0 ){
			    	alert("Please expand the network before add any Organisation.");
			    	fetchNetworkRows();
			    	return false;
		    	}
		    	}*/


		    	
	    		/* orgn = orgn.replace("class=\"browserEvenRow draggable ui-draggable\"", "class=\"sortable not\"");
		    	orgn = orgn.replace("class=\"browserOddRow draggable ui-draggable\"", "class=\"sortable not\"");
		    	orgn = orgn.replace("onmousedown=\"functionCall("+pageRight+");\"", "");
		    	firstIndOrgn=0;
	    		lastIndOrgn=0;
	    		firstIndOrgn = orgn.indexOf("title=\"Delete\"");
	    		if(firstIndOrgn>0){
	    			lastIndOrgn = orgn.indexOf("</td>",firstIndOrgn);
	    			if(lastIndOrgn>0){
	    				orgn=orgn.substring(lastIndOrgn+5,orgn.length);
	    			}	
	    		}
		        //orgn = orgn.replace("<span", "<span id=\"span_"+mainRowId+"_"+siteTypeCount+"\" onclick=\"openDDList(this.id,0);\"");
	    		//orgn = orgn.replace("siteType_"+mainRowId, "siteTypeId_"+mainRowId+"_"+siteTypeCount);
		    	//orgn = orgn.replace("<select", "<select onblur=\"openDDList(this.id,1,'siteTypeId');\" ");
		    	
	    		orgn = orgn.replace("<span id=\"span_ntType_", "<span class=\"ddSpan\" id=\"span_ntType_"+mainRowId+"_"+siteTypeCount+"\" onclick=\"openDDList(this.id,0,"+pageRight+");\"");
		    	orgn = orgn.replace("<select id=\"siteType_"+mainRowId, "<select class=\"ddSelector\" onblur=\"openDDList(this.id,1,7);\" id=\"siteTypeId_"+mainRowId+"_"+siteTypeCount);

	    		var splitId= id.split("_");

	    		var levelch=parseInt(splitId[2])+1;
	    		var sliceRow=$j("#"+id).html();
	    		firstIndOrgn=0;
	    		lastIndOrgn=0;
	    		var padding1;
	    		firstIndOrgn = sliceRow.indexOf("<tr");
	    		if(firstIndOrgn>0){
	    			sliceRow=sliceRow.substring(0,firstIndOrgn);
	    			firstIndOrgn=0;
	    			firstIndOrgn = sliceRow.indexOf("padding-left:");
	    			var indexPadding=sliceRow.indexOf("%;",firstIndOrgn);
	    			padding1=sliceRow.substring(firstIndOrgn+13,indexPadding)
	    			padding1 = parseInt(padding1)+parseInt(padding1);
	    			lastIndOrgn=sliceRow.indexOf("<td");
			    	if(lastIndOrgn>=0 && levelch<=6)
			    	{  
				    	if(sliceRow.indexOf('src="./js/dojo/src/widget/templates/images/Tree/minus.gif"')<=0)
				    		sliceRow=sliceRow.substring(0,indexPadding+4)+'<img style="height:10px;width:10px;" src="./js/dojo/src/widget/templates/images/Tree/minus.gif" border="0" onclick="showHide(this)">'+sliceRow.substring(indexPadding+4,sliceRow.length);
			    	}
	    		}
	    		$j("#"+id).html(sliceRow);
	    		if(levelch>6){
	    			alert(M_NtLevel_RchMaxLevel);
	    			return false;
	    		}
	    		sliceOrgn='';
	    		if(splitId[3]>0){
	    			saveNetworkRow(mainRowId,parseInt(splitId[2])+1,splitId[1],splitId[3],$j("#siteType_"+mainRowId).val());
	    			orgn = orgn.replace(""+mainRowId,"row_"+splitId[1]+"_"+levelch+"_"+mnNetworkId);
	    		}else{
	    			saveNetworkRow(mainRowId,splitId[2]+1,splitId[1],splitId[1],$j("#siteType_"+mainRowId).val());
	    			orgn = orgn.replace(""+mainRowId,"row_"+splitId[1]+"_"+levelch+"_"+mnNetworkId);
	    		}
	    		sliceOrgn = '';
	    		firstIndOrgn=0;
	    		lastIndOrgn=0;
	    		firstIndOrgn = orgn.indexOf("fOpenNetwork(");
	    		if(firstIndOrgn>0){
	    			lastIndOrgn = orgn.indexOf(",0,'')",firstIndOrgn);
	    		  if(lastIndOrgn>0){
	    			  sliceOrgn =  orgn.substring(firstIndOrgn,lastIndOrgn+6);
	    			  orgn = orgn.replace(sliceOrgn,"fOpenNetwork("+mainRowId+","+mnNetworkId+",'"+levelch+"')");
	    		  }
	    		}
	    		orgn = orgn.replace("</tr>", "<td>Active</td><td><A href=\"#\" onclick=\"fAddMultiUserToSite('"+mnNetworkId+"','"+levelch+"','"+pageRight+"');\"><img title=\"Network Users\" src=\"./images/User.gif\" border=\"0\"></A></td><td><img title=\"Network Appendix\" src=\"./images/Appendix.gif\" border=\"0\"></td><td><a href=\"#\" onclick=\"deleteNetwork("+mnNetworkId+");\"><img src=\"./images/delete.gif\" title=\"Delete\" border=\"0\"></a></td></tr>");
	    		orgn = orgn.replace("<td>","<td style=\"padding-left:"+padding1+"%;\">");
	    		siteTypeCount++;
		    	//$j("#"+id).closest( "tr" ).after(orgn);
		    	$j("#table2").find("tr:gt(1)").remove();
		    	fetchNetworkRows();
		    	hideTableRowSortable();
		    	//$j("#row_"+splitId[1]+"_"+levelch+"_"+mnNetworkId+" td").css("background-color","#D9E9D9");
	    	}
	    	}
	    	else{
	    			$j("#table2").find("tr:gt(1)").remove();
			    	fetchNetworkRows();
			    	hideTableRowSortable();
		    	}
	    }
	    });
	  
	    $j( ".draggable" ).draggable({
	      cursor: "move",
	      revertDuration: 500,
	      appendTo: 'body',
	      containment: 'window',
	      scroll: true,
	      connectToSortable: ".sortable",
	      helper: "clone",
	      revert: "invalid",
	      zIndex:1000,
	      refreshPositions: true,
	      start:function(){
	    	 //$j(this).css({ width: 700 });
	    	 mainRowId=$j(this).attr('id');
	    }
	      
	    });
	    $j('body').css('cursor', 'auto'); 
	    $j('#table2').css('cursor', 'auto'); 
	   
	  //  if(dragable==undefined){
		   // alert("dragable-"+dragable);
		  hideTableRowSortable();
		 // } */
  	}


	var screenWidth = screen.width;
	var screenHeight = screen.height;

	function setOrderNetPage(formObj,orderBy) //orderBy column number 
	{
		jQuery("#mydiv").html('<div id="mydiv"> <table width="100%" class="outline midAlign"  id="table1" style="margin: 0px 0px 0px 0px;"><tr><th width="40%">'+'<%=LC.L_Organization_Name%>'+'</th><th width="33%">'+'<%=LC.L_Organization_Type%>'+'</th><th width="30%">'+'<%=LC.L_CTEP_ID%>'+'</th></tr></table><%=LC.L_LoadingPlsWait%></div>');
		if (formObj.orderType.value=="asc") 
		{
			formObj.orderType.value= "desc";
		} 
		else if (formObj.orderType.value=="desc") 
		{
			formObj.orderType.value= "asc";
		}

		orderType=formObj.orderType.value;
		formObj.orderBy.value = orderBy;	
		var accountId=document.getElementById("accountId").value;
		var bg=document.getElementById("search").value;
		var pageRight=document.getElementById("pageRight").value;
		var urlCall="sitebrowser.jsp?orderType="+orderType+"&orderBy="+orderBy+"&accountId="+accountId+"&searchby="+bg+"&calledFrom=networkTabs";

		jQuery.ajax({
			url : urlCall,
			cache: false,
			success : function (data) 
			{  
				jQuery("#mydiv").html(data);
				//functionCall(pageRight);
				
			}
		});		
	}

	function hideTableRowSortable(){
		  /*Performance issue*/
         // alert("find the hideTableRowSortable button");
	    var table = document.getElementById("table2");
	    
	    
	   $j("#table2 tr").each(function(i,row){
	   
	      var rowId=row.id;
	      if(rowId !='')
	    	if(rowId.indexOf('hidden')>0)
	    		$j("#"+rowId).hide();
	});
	   /*Performance issue*/
		}

	function openWinHistory(networkId,pageRight,historyId,parentNtwId)
	{
		otherParam = "&moduleTable=er_nwsites&selectedTab=7&from=ntwhistory&fromjsp=showinvoicehistory.jsp";
		windowName= window.open("showinvoicehistory.jsp?modulePk="+networkId+"&pageRight="+pageRight+"&currentStatId="+historyId+"&parentNtwId="+parentNtwId + otherParam,"statusdetails","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=800,height=600,top=100, left=150");
		windowName.focus();
	}
	
	function openWinStatus(mode,Id,statusId,pageRight){

		var checkType;
		if (mode=='N')
			checkType = 'N';
		else
			checkType = 'E';
		if (f_check_perm(pageRight,checkType) == true){
			var otherParam;
			var networkId;
			var sliceId = jQuery("#"+Id).closest('tr').attr('id').split("_");
			if(sliceId[3]>0){
				networkId=sliceId[3];
			}
			else{
				networkId=sliceId[1];
			}
			otherParam = "&moduleTable=er_nwsites&statusCodeType=networkstat&sectionHeading=<%=MC.M_Ntwk_StatDets%>&fromPage=networkTab&statusId=" + statusId;
			windowName= window.open("editstatus.jsp?mode=" + mode +"&parentNtwId="+selectedNtwId+"&modulePk=" +  networkId + otherParam,"statusdetails","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=800,height=600");
			windowName.focus();
		}
	}
	function mySearch()
	{
		var accountId=document.getElementById("accountId").value;
		var bg=document.getElementById("search").value;
		var pageRight=document.getElementById("pageRight").value;
		var urlCall="sitebrowser.jsp?orgname=SITE_NAME&ordertype=asc&accountId="+accountId+"&searchby="+encodeURIComponent(bg)+"&calledFrom=networkTabs";
		jQuery.ajax({
			url : urlCall,
			cache: false,
			success : function (data) 
			{
				jQuery("#mydiv").html(data);
				//functionCall(pageRight);
			}
		});	
	}

	function networkSiteByName(event){
		if (event.keyCode != 37 && event.keyCode != 39 && event.keyCode != 36 && event.keyCode != 38 && event.keyCode != 100 && event.keyCode != 102 && event.keyCode != 104) {
		var searchNetWorkByName=document.getElementById("searchNetWorkByName").value;
		var moreParam ='{"searchByName":'+'"'+searchNetWorkByName+'"'+',"calledFrom":"networkTabs"'+'}';
		var pageRight=document.getElementById("pageRight").value;
		//alert("param-"+param);
		var urlCall="networksearch.jsp?&calledFrom=networkTabs&src=networkTabs&searchByName="+searchNetWorkByName+"&pageRight="+pageRight+"&expandNtw="+expandNtw+'&moreParam='+moreParam+'&parentNtwId='+selectedNtwId;
		
		jQuery.ajax({
			url: urlCall,
			cache: false,
			type: "POST",
			async:false,
			success : function (data) 
			{
				//alert(data);
				$j("#table2").find("tr:gt(1)").remove();
				$j("#row").closest( "tr" ).after(data);
			}
		});	
		}
		}
	
	function networkSiteByName1(event){
		if (event.keyCode != 37 && event.keyCode != 39 && event.keyCode != 36 && event.keyCode != 38 && event.keyCode != 100 && event.keyCode != 102 && event.keyCode != 104) {
		var searchNetWorkByName1=document.getElementById("searchNetWorkByName1").value;
		var moreParam='';
		if(searchNetWorkByName1!=""){
			moreParam ='{"searchByName":'+'"'+searchNetWorkByName1+'"'+',"calledFrom":"networkTabs"'+',"filter":"name"'+',"networkId":"'+selectedNtwId+'"}';
		}else{
			moreParam ='{"calledFrom":"networkTabs"'+',"filter":"name"'+',"networkId":"'+selectedNtwId+'"}';
		}
		var pageRight=document.getElementById("pageRight").value;
		//alert("param-"+param);
		var urlCall="networksearch.jsp?&calledFrom=networkTabs&src=networkTabs&searchByName="+searchNetWorkByName1+"&pageRight="+pageRight+"&expandNtw="+expandNtw+'&moreParam='+moreParam+'&parentNtwId='+selectedNtwId;
		jQuery.ajax({
			url: urlCall,
			cache: false,
			type: "POST",
			async:false,
			success : function (data) 
			{
				//alert(data);
				$j("#table2").find("tr:gt(1)").remove();
				$j("#row").closest( "tr" ).after(data);
				var new1=($j("#searchNetWorkByName1").val(searchNetWorkByName1));
				var setselection=new1.val().length * 2;
				new1.focus();
				new1[0].setSelectionRange(setselection,setselection);
				//$j("#searchNetWorkByName1").focus();
				hideTableRowSortable();
			}
		});	
		}
	}
	function participationType(event){
		if (event.keyCode != 37 && event.keyCode != 39 && event.keyCode != 36 && event.keyCode != 38 && event.keyCode != 100 && event.keyCode != 102 && event.keyCode != 104) {
		var searchNetWorkByparticipationType=document.getElementById("searchNetWorkByparticipationType").value;
		var moreParam='';
		if(searchNetWorkByparticipationType!=""){
			moreParam ='{"searchByName":'+'"'+searchNetWorkByparticipationType+'"'+',"calledFrom":"networkTabs"'+',"filter":"type"'+',"networkId":"'+selectedNtwId+'"}';	
		}else{
			moreParam ='{"calledFrom":"networkTabs"'+',"filter":"type"'+',"networkId":"'+selectedNtwId+'"}';
		}
		var pageRight=document.getElementById("pageRight").value;
		//alert("param-"+param);
		var urlCall="networksearch.jsp?&calledFrom=networkTabs&src=networkTabs&searchByName="+searchNetWorkByparticipationType+"&pageRight="+pageRight+"&expandNtw="+expandNtw+'&moreParam='+moreParam+'&parentNtwId='+selectedNtwId;
		jQuery.ajax({
			url: urlCall,
			cache: false,
			type: "POST",
			async:false,
			success : function (data) 
			{
				//alert(data);
				$j("#table2").find("tr:gt(1)").remove();
				$j("#row").closest( "tr" ).after(data);
				var new1=($j("#searchNetWorkByparticipationType").val(searchNetWorkByparticipationType));
				var setselection=new1.val().length * 2;
				new1.focus();
				new1[0].setSelectionRange(setselection,setselection);
				//$j("#searchNetWorkByparticipationType").focus();
				hideTableRowSortable();
			}
		});
		}	
		}
function NetWorkByStatus(event){
	if (event.keyCode != 37 && event.keyCode != 39 && event.keyCode != 36 && event.keyCode != 38 && event.keyCode != 100 && event.keyCode != 102 && event.keyCode != 104) {
		var searchNetWorkByStatus=document.getElementById("searchNetWorkByStatus").value;
		var moreParam='';
		if(searchNetWorkByStatus!=""){
			moreParam ='{"searchByName":'+'"'+searchNetWorkByStatus+'"'+',"calledFrom":"networkTabs"'+',"filter":"status"'+',"networkId":"'+selectedNtwId+'"}';
		}else{
			moreParam ='{"calledFrom":"networkTabs"'+',"filter":"status"'+',"networkId":"'+selectedNtwId+'"}';
		}
		var pageRight=document.getElementById("pageRight").value;
		//alert("param-"+param);
		var urlCall="networksearch.jsp?&calledFrom=networkTabs&src=networkTabs&searchByName="+searchNetWorkByStatus+"&pageRight="+pageRight+"&expandNtw="+expandNtw+'&moreParam='+moreParam+'&parentNtwId='+selectedNtwId;
		jQuery.ajax({
			url: urlCall,
			cache: false,
			type: "POST",
			async:false,
			success : function (data) 
			{
				//alert(data);
				$j("#table2").find("tr:gt(1)").remove();
				$j("#row").closest( "tr" ).after(data);
				var new1=($j("#searchNetWorkByStatus").val(searchNetWorkByStatus));
				var setselection=new1.val().length * 2;
				new1.focus();
				new1[0].setSelectionRange(setselection,setselection);
				//$j("#searchNetWorkByStatus").focus();
				hideTableRowSortable();
			}
		});	
	}
		}
function searchByCtepId(event){
	if (event.keyCode != 37 && event.keyCode != 39 && event.keyCode != 36 && event.keyCode != 38 && event.keyCode != 100 && event.keyCode != 102 && event.keyCode != 104) {
	var searchNetWorkByCTEPID=document.getElementById("searchNetWorkByCTEPID").value;
	var moreParam=''
 	 if (searchNetWorkByCTEPID!=""){
 		moreParam ='{"searchByName":'+'"'+searchNetWorkByCTEPID+'"'+',"calledFrom":"networkTabs"'+',"filter":"ctepid"'+',"networkId":"'+selectedNtwId+'"}';
 	 }else{
 		moreParam ='{"calledFrom":"networkTabs"'+',"filter":"ctepid"'+',"networkId":"'+selectedNtwId+'"}';
 	 }
	var pageRight=document.getElementById("pageRight").value;
	//alert("param-"+param);
	var urlCall="networksearch.jsp?calledFrom=networkTabs&src=networkTabs&searchByName="+searchNetWorkByCTEPID+"&pageRight="+pageRight+"&expandNtw="+expandNtw+'&moreParam='+moreParam+'&parentNtwId='+selectedNtwId;
	jQuery.ajax({
		url: urlCall,
		cache: false,
		type: "POST",
		async:false,
		success : function (data) 
		{
			//alert(data);
			$j("#table2").find("tr:gt(1)").remove();
			$j("#row").closest( "tr" ).after(data);
			var new1=($j("#searchNetWorkByCTEPID").val(searchNetWorkByCTEPID));
			var setselection=new1.val().length * 2;
			new1.focus();
			new1[0].setSelectionRange(setselection,setselection);
			//$j("#searchNetWorkByStatus").focus();
			hideTableRowSortable();
		}
	});	
	}
	}
	
	function saveNetworkRow(siteId,level,mainNtwId,networkId,sitestype)
	{
		var urlCall="";
		var chld_ids="";
		if(level<=0)
			urlCall="saveNetwork.jsp?siteId="+siteId+"&level="+level+"&sitestype="+sitestype+"&calledFrom=networkTabs";
		else
			urlCall="saveNetwork.jsp?siteId="+siteId+"&level="+level+"&sitestype="+sitestype+"&mainNtwId="+mainNtwId+"&networkId="+networkId+"&calledFrom=networkTabs";
		
		jQuery.ajax({
			url: urlCall,
			cache: false,
			type: "POST",
			async:false,
			success : function (data) 
			{
				var response=jQuery(data);
				var message = response.filter('#resultMsg').val();
				mnNetworkId = response.filter('#netId').val();
				chld_ids=response.filter('#chld_ids').val();
			}
		});
		document.getElementById("chld_ids").value=chld_ids;
		return mnNetworkId;
	}


	function updateNetworkRowStatus(siteId,level,mainNtwId,networkId,statusId)
	{
		var urlCall="";
		if(level<=0)
			urlCall="saveNetwork.jsp?siteId="+siteId+"&level="+level+"&statusId="+statusId+"&calledFrom=networkTabs";
		else{
			expandNtw.push('row_'+mainNtwId+'_0');
			urlCall="saveNetwork.jsp?siteId="+siteId+"&level="+level+"&statusId="+statusId+"&mainNtwId="+mainNtwId+"&networkId="+networkId+"&calledFrom=networkTabs";
		}
		
		jQuery.ajax({
			url: urlCall,
			cache: false,
			type: "POST",
			async:false,
			success : function (data) 
			{
				//fetchNetworkRows();	
				hideTableRowSortable();
			}
		});	
	}

	
	function fetchNetworkRows(networkId)
	{
		var moreParam ='{"calledFrom":"networkTabs"'+',"networkId":"'+networkId+'"}';
		var rowCount = $j('#table2 tr').length;
		var pageRight=document.getElementById("pageRight").value;
		network_flag=document.getElementById("network_flag").value;
		if(networkId!='null'){
			selectedNtwId=networkId;
		}
		var urlCall="networksearch.jsp?&calledFrom=networkTabs&src=networkTabs&pageRight="+pageRight+"&expandNtw="+expandNtw+'&moreParam='+moreParam+"&rowCount="+rowCount+'&parentNtwId='+selectedNtwId+'&network_flag='+network_flag;

		jQuery.ajax({
			url: urlCall,
			cache: false,
			type: "POST",
			async:false,
			success : function (data) 
			{
				$j("#table2").find("tr:gt(1)").remove();
				//$j("#row td").css("background-color","#2ECCFA");
				$j("#row").closest( "tr" ).after(data);
				//hideTableRowSortable();
				defaultNetworkDisplay();
			}
		});
		var len=$j("#table2 tr").length;
		if(len>3)
			document.getElementById('expandCollapseAll').style.display="block";	
	}
	function fAddMultiUserToSite(networkId,networkLevel,pageRight){
		if(f_check_perm(pageRight,'V')  == true){
		windowName =window.open('usersnetworksites.jsp?networkId='+networkId+'&pageRight='+pageRight+'&from=networkTabs&nLevel='+networkLevel, 'Information2', 'toolbar=no,scrollbar=yes,resizable=yes,menubar=no,status=yes,width=900,height=500, top=100, left=80');
		  windowName.focus();
		}}
	
	function fOpenNetwork(siteId,networkId,networkLevel){
		if(networkId===0 && networkLevel===""){
			windowOrg =window.open('sitedetails.jsp?siteId='+siteId+'&mode=M&srcmenu=tdMenuBarItem2&src=networkTabs', 'Information1', 'toolbar=no,scrollbar=yes,resizable=yes,menubar=no,status=yes,width=900,height=500, top=100, left=80');
			windowOrg.focus();}
		else{
			windowOrg =window.open('sitedetails.jsp?siteId='+siteId+'&mode=M&srcmenu=tdMenuBarItem2&src=networkTabs&networkId='+networkId+'&parentNtwId='+selectedNtwId+'&nLevel='+networkLevel, 'Information1', 'toolbar=no,scrollbar=yes,resizable=yes,menubar=no,status=yes,width=900,height=500, top=100, left=80');
			windowOrg.focus();
		}
		}
	function deleteNetwork(netId){
		if (confirm(M_NtDelMesg)) {
			var moreParam ='{"calledFrom":"networkTabs"'+',"networkId":"'+selectedNtwId+'"}';
			var pageRight=document.getElementById("pageRight").value;
			var urlCall="networksearch.jsp?&calledFrom=networkTabs&src=networkTabs&flag=D&networkId="+netId+"&pageRight="+pageRight+"&expandNtw="+expandNtw+'&moreParam='+moreParam+"&network_flag=network_flag";
			
			jQuery.ajax({
				url: urlCall,
				cache: false,
				type: "POST",
				async:false,
				success : function (data) 
				{
					//alert(data);
					var response=$j(data);
					var message = response.filter('#resultMsg').val();
					//alert(message);
					if(message=="true"){
						  alert("This Network is Associated to study.");
						}else{
						$j("#table2").find("tr:gt(1)").remove();
						$j("#row").closest( "tr" ).after(data);
						//hideTableRowSortable();
						defaultNetworkDisplay();
						}
				}
			});
			var len=$j("#table2 tr").length;
			if(len<=3)
				document.getElementById('expandCollapseAll').style.display="none";
	    }else{
	    	return false;
	    }
	}
	var showOrHide = true;
	var selectedNtwId="";
	$j(function() {	
		var historyReq='<%=request.getParameter("parentNtwId")%>';
		if(historyReq=='null'){
			selectedNtwId=0;
		}
		else{
			selectedNtwId=historyReq;
			}
		fetchNetworkRows('<%=request.getParameter("parentNtwId")%>'); 
			
		$j("#displayNetwork").autocomplete({
		      
		      source : function(request, response) {
		          $j.ajax({
		               url : "userNetworkAutoCompleteJson.jsp?searchNetwork=true&networkId="+selectedNtwId,
		               type : "POST",
		               autofocus:true,

		               data : {
		              	 term : request.term
		               },		               
		               dataType : "json",
		               success: function(data){
		                   response( $j.map( data, function( item ) {
		                       return {
		                           label: item.network_name,
		                           networkId: item.pk_nwsites     // EDIT
		                       }
		                   }));
		                },
		                
		        });
		     },
		     select : function(event, ui) {
		    	 document.getElementById("network_flag").value='';
		         selectedNtwId=ui.item.networkId;
		  	   	 fetchNetworkRows(selectedNtwId); 
		  	 //  defaultNetworkDisplay();
		         $j("#displayNetwork").val('');
		         return false;
		     }
		  });

		
		var pageRight=$j('#pageRight').val();
		$j( "#dragOrgTable" ).hide();
		  $j( "#dropNetworkTable" ).attr("style","width:100%;");
		  $j("#toggleGadgetList").attr('src',"../images/jpg/ListExpand.jpg");
		  $j("#clr").attr("style","display:inline;");  
		//fetchNetworkRows();
		//functionCall(pageRight);
		$j("#btnSideMenuToggleDIV").click(function () {
		      //$("#searchPanelDIV").toggle(showOrHide);
		       if ( showOrHide === true ) {
				  $j( "#dragOrgTable" ).show();
				  $j( "#dropNetworkTable" ).attr("style","width:70%;");
				  $j("#toggleGadgetList").attr('src',"../images/jpg/ListCollapse.jpg");
				  $j("#clr").attr("style","display:none;");
				  showOrHide = false;
				} else if ( showOrHide === false ) {
				  $j( "#dragOrgTable" ).hide();
				  $j( "#dropNetworkTable" ).attr("style","width:100%;");
				  $j("#toggleGadgetList").attr('src',"../images/jpg/ListExpand.jpg");
				  $j("#clr").attr("style","display:inline;");
				  showOrHide = true;
				}
		    });
    });
	function defaultNetworkDisplay(){
        var limitLevel=1;
        var currentLevel=0;
    	 var table = document.getElementById("table2");
    	  $j("#table2 tr").each(function(i,row){
    		   
    	      var rowId=row.id;
    	      var splitId='';
    	      if(rowId !=''){
    	    		splitId=rowId.split('_');
	    		if(splitId[2]<=limitLevel){
	    			currentLevel=splitId[2];
	    		var _img=$j("#"+rowId).find('img:first');
	    		var tdhtml=$j("#"+rowId).children('td:first').html();
	    		if(_img.attr('src')=='./images/formright.gif' && currentLevel<limitLevel){
	    			_img.attr('src','./images/formdown.gif');
	    		}
	    		$j("#"+rowId).show();
		    		}
    	      }
    	});
      }
    function networkStateManitain(){
        }
	function openDDList(Id,flag,pageRight){
		if (f_check_perm(pageRight,'E') == true){
			if(flag==0){
				$j("#"+Id).hide();
				$j("#"+Id.replace("span_ntType","siteTypeId")).show();
				$j("#"+Id.replace("span_ntType","siteTypeId")).focus();}
			else{
				$j("#"+Id).hide();
				$j("#"+Id.replace("siteTypeId","span_ntType")).text($j("#"+Id+" option:selected").text());
				//alert($j("#"+Id).closest('tr').attr('id'));
				var sliceId = $j("#"+Id).closest('tr').attr('id').split("_");
				//alert(sliceId[3]);
				if(sliceId[3]>0){
					saveNetworkRow(sliceId[3],-1,0,0,$j("#"+Id+" option:selected").val());
				}
				else{
					saveNetworkRow(sliceId[1],-1,0,0,$j("#"+Id+" option:selected").val());
				}
				$j("#"+Id.replace("siteTypeId","span_ntType")).show();
				//$j("#"+Id.replace("siteTypeId","span_ntType")).focus();
			}
			
			var firefox=navigator.userAgent.toLowerCase().indexOf("firefox");
			if(firefox>0 && $j.browser.version<50){
				    $j(".ddSelector").focusout(function(){
				    	openDDList(this.id,1,7);
					});
				}
		}
	}

	function openDDstatusList(Id,flag){
		if(flag==0){
			$j("#"+Id).hide();
			$j("#"+Id.replace("span_status","siteStatusId")).show();}
		else{
			$j("#"+Id).hide();
			$j("#"+Id.replace("siteStatusId","span_status")).text($j("#"+Id+" option:selected").text());
			var sliceId = $j("#"+Id).closest('tr').attr('id').split("_");
			if(sliceId[3]>0){
				updateNetworkRowStatus(sliceId[3],-2,0,0,$j("#"+Id+" option:selected").val());
			}
			else{
				updateNetworkRowStatus(sliceId[1],-2,0,0,$j("#"+Id+" option:selected").val());
			}
			$j("#"+Id.replace("siteStatusId","span_status")).show();
		}
	}
	
	function networkSitesAppendix(networkId,siteId,pageRight){
		var siteName=document.getElementById("siteName"+siteId).value;
		var networkType=document.getElementById("networkType"+networkId).value;
		windowName =window.open('eventappendix.jsp?networkId='+networkId+'&siteId='+siteId+'&siteName='+siteName+'&networkType='+networkType+'&pageRight='+pageRight+'&calledFrom=NetWork&fromPage=selectNetwork&networkFlag=Org_doc&selectedTab=1', 'Information', 'toolbar=no,scrollbar=yes,resizable=yes,menubar=no,status=yes,width=1100,height=600, top=100, left=100');
		  windowName.focus();
		}
//Function use for change Background -color for particular time//	
	function changeColor(row_id){
			row_id.css("background-color", "#faface").animate({ 'opacity': '0.5' }, 3000, function () {
	    	 row_id.css({'backgroundColor': '#fff','opacity': '1'});
	    		});
		        }
		        
	function createNetworkSites(lReleation,rDetails){
		var pageRight=$j('#pageRight').val();
		document.getElementById('network_flag').value = 'network_flag';
		var mainRowIds='';
       var count=0;
       var mainRowId='';
		$j('input:checkbox[name=selectNetwork]').each(function(){
			 if($j(this).is(':checked')) {
				 mainRowIds= mainRowIds+$j(this).val()+',';
				 count=count+1;
			 }
			});
		
		if(count==0){
			alert(M_Sel_SiteFrst);
			return false;
			}
		if (f_check_perm(pageRight,'N') == true){
		mainRowIds=mainRowIds.substr(0, mainRowIds.length-1);
		if(lReleation=='parent'){
			if(count>1){
				alert(M_Sel_Only_OneSite);
				return false;
				}
			selectedNtwId=saveNetworkRow(mainRowIds,0,0,0,$j("#siteType_"+mainRowId).val());
		}else{
			var splitId=rDetails.split("_");
			var levelch=parseInt(splitId[2])+1;
			if(levelch>6){
    			alert(M_NtLevel_RchMaxLevel);
    			$j('input:checkbox[name=selectNetwork]').attr('checked', false);
    			return false;
    		}
			if(splitId[3]>0){
				saveNetworkRow(mainRowIds,parseInt(splitId[2])+1,splitId[1],splitId[3],$j("#siteType_"+mainRowId).val());
				selectedNtwId=splitId[1];
    		}else{
    			saveNetworkRow(mainRowIds,splitId[2]+1,splitId[1],splitId[1],$j("#siteType_"+mainRowId).val());
    			selectedNtwId=splitId[1];
    		}
			}
		fetchNetworkRows(selectedNtwId);
	var row_id="";	
	if(lReleation=='parent'){
		row_id=$j("#row_"+selectedNtwId+"_0 td");
		changeColor(row_id);//call when parent  org is added
		    	}
	else{	
		 var child_pk=$j("#chld_ids").val();
		 var child_ids=child_pk.split(",");
		 for(var i = 0; i < child_ids.length; i++) {
			 row_id=$j("#row_"+selectedNtwId+"_"+levelch+"_"+child_ids[i]+" td");
				changeColor(row_id);//call when single/multiple child are added.
			}}
	
	    }
		$j('input:checkbox[name=selectNetwork]').attr('checked', false);
	}

function showAllNetwork(){
	document.getElementById("network_flag").value='';
	windowName=window.open("showAllNetworks.jsp?networkId="+selectedNtwId,"showAllNetworks","toolbar=no,resizable=yes,menubar=no,status=yes,dependant=true,width=800,height=600,top=100,left=250");
	windowName.focus();
		}
		
function openformwin(NetworkId){
	windowform = window.open('formfilledaccountbrowser.jsp?modpk='+NetworkId+'&srcmenu=tdMenuBarItem1&commonformflag=NTW', '_blank','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=900,height=700,left=10,top=100');
	windowform.focus();	
}
	
</script>



<body>
	<jsp:useBean id="siteB" scope="request" class="com.velos.eres.web.site.SiteJB"/>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<%@ page language = "java" import = "com.velos.eres.business.site.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%><%@page import="com.velos.eres.service.util.*"%>

	<DIV class="tabDefTopN" id="divTab"> 
		<jsp:include page="accounttabs.jsp" flush="true"> 
			<jsp:param name="selectedTab" value="<%=selectedTab %>"/>
		</jsp:include>
	</DIV>
	<%if(osName.contains("Linux"))
			{%>
		<SCRIPT LANGUAGE="JavaScript">
			document.write('<DIV class="tabDefBotN" id="div1" style="overflow:auto;">')
	</SCRIPT>
	<%} else{%>
	<SCRIPT LANGUAGE="JavaScript">
		if(screenWidth>1280 || screenHeight>1024)
			document.write('<DIV class="tabDefBotN" id="div1" style="height:80%; overflow:hidden;">')
			
		else
			document.write('<DIV class="tabDefBotN" id="div1" style="overflow:auto;">')
	</SCRIPT>
	<%} %>
<a id = "btnSideMenuToggleDIV" style="padding: 0px 0px 0px 5px;" href="#"><img id="toggleGadgetList" title="Show/Hide Organizations List" src="../images/jpg/ListCollapse.jpg" style="border:none;"></a><span id="clr" style="display:none" >&nbsp;Click here to view the list of organizations</span>
<TABLE width="100%">  
<TR>

<TD id="dragOrgTable" WIDTH="30%" valign="top">	
	<Form name="networkTabs" method="post" action="networkTabs.jsp" onsubmit="" flush="true">

  	<%
   		HttpSession tSession = request.getSession(true); 
   		if (sessionmaint.isValidSession(tSession))
		{
			String uName = (String) tSession.getValue("userName");
	 		int pageRight = 0;
	   		GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
   			pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MSITES"));
 			
   			if ((pageRight == 5 || pageRight == 7 ) || pageRight >=4)
			{
		   		int accountId=0;   
				String orderBy="";
				orderBy=request.getParameter("orderBy");
		
				if (orderBy==null) 
					orderBy="SITE_NAME";
				String orderType = "";
				orderType = request.getParameter("orderType");
				if (orderType == null)
				{
					orderType = "asc";
				} 

				String acc = (String) tSession.getValue("accountId");
				accountId = EJBUtil.stringToNum(acc);
				String userId = (String) tSession.getValue("userId");
				int pkuser = EJBUtil.stringToNum(userId);
				userB.setUserId(pkuser);
				userB.getUserDetails();
				String defGroup = userB.getUserGrpDefault();
				groupB.setGroupId(EJBUtil.stringToNum(defGroup));
				groupB.getGroupDetails();
				String groupName = groupB.getGroupName();
				SiteDao siteDao = siteB.getByAccountId(accountId,orderBy,orderType,"");
			
				ArrayList siteIds = siteDao.getSiteIds(); 			
				ArrayList siteTypes = siteDao.getSiteTypes();
				ArrayList siteTypeIds = siteDao.getSiteTypeIds();
				ArrayList siteNames = siteDao.getSiteNames();			
				ArrayList ctepIds = siteDao.getCtepIds();
				ArrayList siteInfos = siteDao.getSiteInfos();			
				ArrayList siteParents = siteDao.getSiteParents();			
				ArrayList siteStats = siteDao.getSiteStats();

			   String siteType = null;
			   String siteName = null;	
			   String ctepid = null;
			   String siteInfo = null;		
			   String siteParent = null;		
			   String siteStat = null;

			   int siteId=0;
			   int siteTypeId=0;
			   int len = siteIds.size();
			   int counter = 0;
			   CodeDao cd1 = new CodeDao();
			   cd1.getCodeValues("relnshipTyp");
			   String dSiteType="";

	%>

	<input type="hidden" name="orderType" value="<%=orderType%>"> 
	<input type="hidden" name="srcmenu" value="<%=src%>">
	<input type="hidden" name="src" value="<%=src%>">
	<input type="hidden" name="orderBy" value="<%=orderBy%>">
	<input type="hidden" name="accountId" value="<%=accountId%>" id="accountId">
	<input type="hidden" id="pageRight" name="pageRight" value="<%=pageRight%>"> 

   <%--  <%=MC.M_DragOrgnToCrtNxtLevel%> --%>
    <br></br>
    <input type="input" size="30" name="text" placeholder="Type to search Organization" id="search" class="search-table midAlign" onkeyup="mySearch();" autocomplete="off"/>
    <button type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" onclick="createNetworkSites('parent');" name="create_network" style="left:18%;position:absolute"><%=LC.L_Create_Ntwk%></button>
    <br><br>
<SCRIPT LANGUAGE="JavaScript">
		if(screenWidth>1280 || screenHeight>1024)
			document.write(' <div id="mydiv" style="width:100%;  overflow:auto; height:400px;">')
			
		else
			document.write(' <div id="mydiv" style="width:100%;  overflow:auto; height:600px;">')
	</SCRIPT>
    
    	<table width="100%" class="outline midAlign"  id="table1" style="margin: 0px 0px 0px 0px;">
      		<tr>
      			<th width="5%" onClick=""></th>
        		<th width="40%" onClick="setOrderNetPage(document.networkTabs,'lower(SITE_NAME)')"> <%=LC.L_Organization_Name%><%--Organization Name*****--%> &loz;</th>
				<th width="33%" onClick="setOrderNetPage(document.networkTabs,'lower(SITE_TYPE)')"> <%=LC.L_Organization_Type%><%--Type*****--%> &loz;</th>  
      			<th width="30%" onClick="setOrderNetPage(document.networkTabs,'lower(CTEP_ID)')"> <%=LC.L_CTEP_ID%><%--Type*****--%> &loz;</th>
      		</tr>
      <%
			for(counter = 0;counter<len;counter++)
			{	
				siteId=EJBUtil.stringToNum(((siteIds.get(counter)) == null)?"-":(siteIds.get(counter)).toString());
				siteType=((siteTypes.get(counter)) == null)?"-":(siteTypes.get(counter)).toString();
				siteTypeId=EJBUtil.stringToNum(((siteTypeIds.get(counter)) == null)?"-":(siteTypeIds.get(counter)).toString());
				siteName=((siteNames.get(counter)) == null)?"-":(siteNames.get(counter)).toString();
				ctepid=((ctepIds.get(counter)) == null)?"-":(ctepIds.get(counter)).toString();
				siteParent=((siteParents.get(counter)) ==null)?"-":(siteParents.get(counter)).toString();
				siteStat=((siteStats.get(counter)) == null)?"-":(siteStats.get(counter)).toString();
				dSiteType = cd1.toPullDown("siteType_"+siteId, siteTypeId,"style='display:none;'");
				if (siteStat.equals("A"))
				{
				   siteStat = "Active";	
				}
		
				else if (siteStat.equals("I"))
				{		
				   siteStat = "InActive";	
				}

				if ((counter%2)==0) 
				{  %>
      				<tr class="browserEvenRow" id="<%=siteId%>"  onmouseup="noHighlightRow()"> 
        		<%}	
				else{ %>
      				<tr class="browserOddRow" id="<%=siteId%>"  onmouseup="noHighlightRow()"> 
        		<%}
				//String checkedValue="siteId:"+siteId+"[VELCOMMA]siteType:"+siteType+"[VELCOMMA]ctep:"+ctepid;//+"siteType:"+siteType+"ctep:"+ctepid;
  		%>
  						<td><input value="<%=siteId%>" type="checkbox"  name="selectNetwork"></td>
        				<td> 
        					<%if(ctepid.equals("-")) {%>
        						<A href="../jsp/sitedetails.jsp?siteId=<%=siteId%>&mode=M&srcmenu=<%=src%>">
        					<%}else{%>
        						<A href="../jsp/sitedetails.jsp?siteId=<%=siteId%>&mode=M&srcmenu=<%=src%>" title="<%= ctepid%>">
        					<%}%>
          						<%= siteName%> 
          					</A>
          				</td>
        				<td> <span><%= siteType%></span><%=dSiteType%> </td>     
        				<td> <span><%= ctepid%></span> </td>
      				</tr>	
	   				<%
			}
			%>
    	</table>
    </div>
    <div class="tmpHeight"></div>
  </Form>
  </TD>
  <TD id="dropNetworkTable">
  </br>
  <table style="width:98%">
  <tr>
  <td><div><b><%=LC.L_Sel_NtwDisp%></b> &nbsp;&nbsp;
<!--   <input size="30" name="searchNetWorkByName" placeholder="Search Network" id="searchNetWorkByName" class="search-table midAlign" onkeyup="networkSiteByName();" autocomplete="off" type="input"> -->
  <input size="50" placeholder="Display Network" id="displayNetwork" class="search-table midAlign" type="text">
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" name="browse" onclick="showAllNetwork()"><%=LC.L_Browse_Ntwk%></button>
  </div></td>
  <td align="right">
  <div ><button type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" id="expandCollapseAll" name="expandCollapseAll" onclick="expandCollapseAll()" style="display:none"><%=LC.L_Expand_All%></button>
  </div></td>
  </tr>
  </table>
  </br>
  </br>
  <SCRIPT LANGUAGE="JavaScript">
		if(screenWidth>1280 || screenHeight>1024)
			document.write('<div id="divRight" style="right:1%; top:10%; width:99%; height:372px; overflow:auto;">')
			
		else
			document.write('<div id="divRight"  style="right:1%; top:10%; width:99%; height:600px; overflow:auto;">')
	</SCRIPT>
		<table border="2" width="100%" id="table2" cellpadding="0" cellspacing="0">
			<tr style="color: #D3D3D3">
				<th width="48%" onClick="setOrder(document.networkTabs,'lower(SITE_NAME)')"> <%=LC.L_Organization_Name%><%--Organization Name*****--%></th>
        		<th width="22%" onClick="setOrder(document.networkTabs,'lower(SITE_TYPE)')"> <%=LC.L_Relation_Type%></th>
        		<th width="20%"> <%=LC.L_Status%></th>
        		<th width="10%"><%=LC.L_CTEP_ID%></th>
        		<th width="10%"> <%=LC.L_Users%><%--Organization Name*****--%> </th>
        		<th width="10%"> <%=LC.L_Documents%><%--Type*****--%> </th>
        		<th width="10%">  <%=LC.L_Forms%><%--Forms*****--%> </th>  
        		<% if(groupName.equalsIgnoreCase("Admin")){%> 
        		<th width="10%"> <%=LC.L_Delete%><%--DELETE*****--%> </th>
        		<%} %>
			</tr>
			<tr  class="sortable not" id="row">
				<td colspan="8" id="dragHere" >
					<%-- <%=MC.M_DragOrgnFrmLToRPanel%> --%>
				</td>
			</tr>			
		</table>
	</div>
  </TD>
  </TR></TABLE>
  <input type="hidden" name="chld_ids" id="chld_ids" value="">
  <input type="hidden" name="network_flag" id="network_flag" value=""></input>
  <%

		} //end of if body for page right

		else
		{%>
  		<jsp:include page="accessdenied.jsp" flush="true"/>
  		<%} //end of else body for page right

	}//end of if body for session

	else
	{%>
  		<jsp:include page="timeout.html" flush="true"/>
  		<%}
	%>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

	<div class ="mainMenu" id="emenu"> 
  		<jsp:include page="getmenu.jsp" flush="true"/>
	</div>
</body>
</html>