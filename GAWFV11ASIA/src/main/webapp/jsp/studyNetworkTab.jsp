<html>
<head>
 	<meta http-equiv="X-UA-Compatible" content="IE=11" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE" />
	<META HTTP-EQUIV="EXPIRES" CONTENT="-1"> 
    <%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    <%@page import="com.velos.eres.web.studyRights.StudyRightsJB"%>
    <%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
    <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
    <jsp:include page="localization.jsp" flush="true"/>
	<title>Study Network Tab<%--Manage Account >> Organizations*****--%></title>
	<!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->
   <style type="text/css">
 .ui-autocomplete {
    cursor: default;
    position: absolute;
    background: #FAFAFA;
    max-height: 100px;
    overflow-y: auto;
    overflow-x: hidden;
    }
    .ui-widget-content a text{
   color: :#FAFAFA
}
 </style>
    
</head>

<% 
	String src;
	String srcmenu;
	String selectedTab = request.getParameter("selectedTab");
	srcmenu= request.getParameter("srcmenu");
	src= request.getParameter("src");
	String studyId=(request.getParameter("studyId")==null)?"":request.getParameter("studyId");
	String from=(request.getParameter("fromPage")==null)?"":request.getParameter("fromPage");
	NetworkDao nwdao = new NetworkDao();
	HttpSession tSession = request.getSession(true); 
		if (sessionmaint.isValidSession(tSession))
	{
	int pageRight = 0;
		GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");	
		StudyRightsJB stdRights = new StudyRightsJB();
		stdRights =(StudyRightsJB) tSession.getValue("studyRights");
		if(stdRights==null){
			pageRight= 0;
		}else{
			if ((stdRights.getFtrRights().size()) == 0){
				pageRight= 0;
			}else{
				pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYTEAM"));
			}
		}
%>
<input type="hidden" id="pageRight" name="pageRight" value="<%=pageRight%>"/>
<jsp:include page="panel.jsp" flush="true" > 
	<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

   
<script language="javascript">
var mainRowId;
var rowspan=1;
var mnNetworkId;
var chNetworkId;
var siteTypeCount=0;
var expandNtw=[];
var rowId;

	function noHighlightRow(){
		if(rowId!=undefined || rowId!=null)
		 	$j("#"+rowId+" td").css("background-color", "transparent");
		}
	
function getPadding(padding){
		
		var padding1;
    	var valueOfPadding=padding
    	valueOfPadding=valueOfPadding.trim();
    	var indexPadding=valueOfPadding.indexOf("padding-left:");
    	var firstIndOrgn=valueOfPadding.indexOf("%;",indexPadding);
    	if(indexPadding>=0)
    	{
	    	padding1=valueOfPadding.substring(indexPadding+13,firstIndOrgn);
	    	padding1=parseInt(padding1);
    	}
	
	  return padding1;
	}

	
	function showHide(event){
		var testing=$j(event).parent().attr("style");
		var rowCount = $j('#table2 tr').length;
		var index1=$j(event).closest('tr').index()+1;
		var parentPadding=getPadding(testing);
		var testpad;
		var childPadding;
		
		if($j(event).attr('src')=='./images/formdown.gif'){
			expandNtw.pop($j(event).closest('tr').attr('id'));
			$j(event).attr('src','./images/formright.gif');
					while(index1<rowCount){
						testpad=$j('#table2 tr:eq(' + (index1)+ ')').children('td:first').attr('style');
						childPadding=getPadding(testpad);
						if(childPadding>parentPadding){
							$j('#table2 tr:eq(' + (index1)+ ')').hide();
							}
						else{
								return;
							}
						index1++;
						}
			}
		else{
			expandNtw.push($j(event).closest('tr').attr('id'));
			$j(event).attr('src','./images/formdown.gif');
			var img='';
			var comparePadding;
			 while(index1<rowCount){
				testpad=$j('#table2 tr:eq(' + (index1)+ ')').children('td:first').attr('style');
				childPadding=getPadding(testpad);
				if(img=='./images/formright.gif'){
					while(comparePadding<childPadding){
						index1++;
						testpad=$j('#table2 tr:eq(' + (index1)+ ')').children('td:first').attr('style');
						childPadding=getPadding(testpad);
						}			
					}
				if(childPadding>parentPadding){
						$j('#table2 tr:eq(' + (index1)+ ')').show();	
					}
				else{
						return;
					}
				comparePadding=childPadding;
				img=$j('#table2 tr:eq(' + (index1)+ ')').children('td:first').find('img').attr('src');
				index1++;
				}
		}
	}


	var screenWidth = screen.width;
	var screenHeight = screen.height;

	function networkSiteByName(event){
		if (event.keyCode != 37 && event.keyCode != 39 && event.keyCode != 36 && event.keyCode != 38 && event.keyCode != 100 && event.keyCode != 102 && event.keyCode != 104) {
		var searchNetWorkByName=document.getElementById("searchNetWorkByName").value;
		var studyId=jQuery("#studyId").val();
		var from=jQuery("#from").val();
		var selectedTab=jQuery("#selectedTab").val();
		var pgRight= jQuery("#pageRight").val();
		var moreParam='';
		if(searchNetWorkByName!=""){
			moreParam='{"searchByName":"'+searchNetWorkByName+'"'+',"calledFrom":"studynetworkTabs"'+','+'"studyId":"'+studyId+'"'+',"filter":"name"'+'}';
		}else{
			moreParam='{"calledFrom":"studynetworkTabs"'+','+'"studyId":"'+studyId+'"'+',"filter":"name"'+'}';
		}
		var urlCall="networksearch.jsp?&calledFrom=studynetworkTabs&src=networkTabs&pageRight="+pgRight+"&searchByName="+searchNetWorkByName+"&expandNtw="+expandNtw+"&studyId="+studyId+"&from="+from+"&selectedTab"+selectedTab+'&moreParam='+moreParam;
		
		jQuery.ajax({
			url: urlCall,
			cache: false,
			type: "POST",
			async:false,
			success : function (data) 
			{
				//alert(data);
				$j("#table2").find("tr:gt(0)").remove();
				$j("#row").closest( "tr" ).after(data);
				var new1=($j("#searchNetWorkByName").val(searchNetWorkByName));
				var setselection=new1.val().length * 2;
				new1.focus();
				new1[0].setSelectionRange(setselection,setselection);
				//$j("#searchNetWorkByName").focus();
				hideTableRowSortable();
			}
		});	
		}
	}  
	
	function networkSiteBystatus(event){
		if (event.keyCode != 37 && event.keyCode != 39 && event.keyCode != 36 && event.keyCode != 38 && event.keyCode != 100 && event.keyCode != 102 && event.keyCode != 104) {
		var searchNetWorkByStatus=document.getElementById("searchNetWorkByStatus").value;
		var studyId=jQuery("#studyId").val();
		var from=jQuery("#from").val();
		var selectedTab=jQuery("#selectedTab").val();
		var pgRight= jQuery("#pageRight").val();
		var moreParam='';
		if(searchNetWorkByStatus!=""){
			moreParam='{"searchByName":"'+searchNetWorkByStatus+'"'+',"calledFrom":"studynetworkTabs"'+','+'"studyId":"'+studyId+'"'+',"filter":"status"'+'}';
		}else{
			moreParam='{"calledFrom":"studynetworkTabs"'+','+'"studyId":"'+studyId+'"'+',"filter":"status"'+'}';
			}
		var urlCall="networksearch.jsp?&calledFrom=studynetworkTabs&src=networkTabs&pageRight="+pgRight+"&searchByName="+searchNetWorkByStatus+"&expandNtw="+expandNtw+"&studyId="+studyId+"&from="+from+"&selectedTab"+selectedTab+'&moreParam='+moreParam;
		
		jQuery.ajax({
			url: urlCall,
			cache: false,
			type: "POST",
			async:false,
			success : function (data) 
			{
				//alert(data);
				$j("#table2").find("tr:gt(0)").remove();
				$j("#row").closest( "tr" ).after(data);
				var new1=($j("#searchNetWorkByStatus").val(searchNetWorkByStatus));
				var setselection=new1.val().length * 2;
				new1.focus();
				new1[0].setSelectionRange(setselection,setselection);
				//$j("#searchNetWorkByStatus").focus();
				hideTableRowSortable();
			}
		});	
		}		
		}
	
	function networkSiteByNetworkstatus(event){
		if (event.keyCode != 37 && event.keyCode != 39 && event.keyCode != 36 && event.keyCode != 38 && event.keyCode != 100 && event.keyCode != 102 && event.keyCode != 104) {
		var searchNetWorkByNetworkStatus=document.getElementById("searchNetWorkByNetworkStatus").value;
		var studyId=jQuery("#studyId").val();
		var from=jQuery("#from").val();
		var selectedTab=jQuery("#selectedTab").val();
		var pgRight= jQuery("#pageRight").val();
		var moreParam='';
		if(searchNetWorkByNetworkStatus!=""){
			moreParam='{"searchByName":"'+searchNetWorkByNetworkStatus+'"'+',"calledFrom":"studynetworkTabs"'+','+'"studyId":"'+studyId+'"'+',"filter":"Networkstatus"'+'}';
		}else{
			moreParam='{"calledFrom":"studynetworkTabs"'+','+'"studyId":"'+studyId+'"'+',"filter":"Networkstatus"'+'}';
		}
		
		
		var urlCall="networksearch.jsp?&calledFrom=studynetworkTabs&src=networkTabs&pageRight="+pgRight+"&searchByName="+searchNetWorkByNetworkStatus+"&expandNtw="+expandNtw+"&studyId="+studyId+"&from="+from+"&selectedTab"+selectedTab+'&moreParam='+moreParam;
		
		jQuery.ajax({
			url: urlCall,
			cache: false,
			type: "POST",
			async:false,
			success : function (data) 
			{
				//alert(data);
				$j("#table2").find("tr:gt(0)").remove();
				$j("#row").closest( "tr" ).after(data);
				var new1=($j("#searchNetWorkByNetworkStatus").val(searchNetWorkByNetworkStatus));
				var setselection=new1.val().length * 2;
				new1.focus();
				new1[0].setSelectionRange(setselection,setselection);
				//$j("#searchNetWorkByNetworkStatus").focus();
				hideTableRowSortable();
			}
		});	
		}			
		}
		
		function networkSiteByType(event){
			if (event.keyCode != 37 && event.keyCode != 39 && event.keyCode != 36 && event.keyCode != 38 && event.keyCode != 100 && event.keyCode != 102 && event.keyCode != 104) {
			var searchNetWorkByName=document.getElementById("searchNetWorkByType").value;
			var studyId=jQuery("#studyId").val();
			var from=jQuery("#from").val();
			var selectedTab=jQuery("#selectedTab").val();
			var pgRight= jQuery("#pageRight").val();
			var moreParam='';
			if(searchNetWorkByName!=""){
				moreParam='{"searchByName":"'+searchNetWorkByName+'"'+',"calledFrom":"studynetworkTabs"'+','+'"studyId":"'+studyId+'"'+',"filter":"type"'+'}';
				
			}else{
				moreParam='{"calledFrom":"studynetworkTabs"'+','+'"studyId":"'+studyId+'"'+',"filter":"type"'+'}';
			}
			var urlCall="networksearch.jsp?&calledFrom=studynetworkTabs&src=networkTabs&pageRight="+pgRight+"&searchByName="+searchNetWorkByName+"&expandNtw="+expandNtw+"&studyId="+studyId+"&from="+from+"&selectedTab"+selectedTab+'&moreParam='+moreParam;
			
			jQuery.ajax({
				url: urlCall,
				cache: false,
				type: "POST",
				async:false,
				success : function (data) 
				{
					//alert(data);
					$j("#table2").find("tr:gt(0)").remove();
					$j("#row").closest( "tr" ).after(data);
					var new1=($j("#searchNetWorkByType").val(searchNetWorkByName));
					var setselection=new1.val().length * 2;
					new1.focus();
					new1[0].setSelectionRange(setselection,setselection);
					//$j("#searchNetWorkByType").focus();
					hideTableRowSortable();
				}
			});	
			}
			}
	
	function saveNetworkRow(siteId,level,mainNtwId,networkId,sitestype)
	{
		var urlCall="";
		if(level<=0)
			urlCall="saveNetwork.jsp?siteId="+siteId+"&level="+level+"&sitestype="+sitestype+"&calledFrom=networkTabs";
		else
			urlCall="saveNetwork.jsp?siteId="+siteId+"&level="+level+"&sitestype="+sitestype+"&mainNtwId="+mainNtwId+"&networkId="+networkId+"&calledFrom=networkTabs";
		
		jQuery.ajax({
			url: urlCall,
			cache: false,
			type: "POST",
			async:false,
			success : function (data) 
			{
				var response=jQuery(data);
				var message = response.filter('#resultMsg').val();
				mnNetworkId = response.filter('#netId').val();
			}
		});	
	}
	
	function fetchNetworkRows()
	{
		var studyId=jQuery("#studyId").val();
		var from=jQuery("#from").val();
		var selectedTab=jQuery("#selectedTab").val();
		var pgRight= jQuery("#pageRight").val();
		var moreParam='{"calledFrom":"studynetworkTabs"'+','+'"studyId":"'+studyId+'"'+'}';
		var urlCall="networksearch.jsp?&calledFrom=studynetworkTabs&src=studynetworkTabs&expandNtw="+expandNtw+"&pageRight="+pgRight+"&studyId="+studyId+"&from="+from+"&selectedTab"+selectedTab+'&moreParam='+moreParam;
		
		jQuery.ajax({
			url: urlCall,
			cache: false,
			type: "POST",
			async:false,
			success : function (data) 
			{
				$j("#table2").find("tr:gt(0)").remove();
				$j("#row").closest("tr").after(data);
				hideTableRowSortable();
			}
		});	
	}
	function fAddMultiUserToSite(networkId,networkLevel,from,pageRight){
		if(f_check_perm(pageRight,'V')  == true){
		windowName =window.open('usersnetworksites.jsp?networkId='+networkId+'&nLevel='+networkLevel+'&from='+from+'&pageRight='+pageRight, 'Information2', 'toolbar=no,scrollbar=yes,resizable=yes,menubar=no,status=yes,width=900,height=500, top=100, left=80');
		  windowName.focus();
		}}
	
	function fOpenNetwork(siteId,networkId,networkLevel){
		if(networkId===0 && networkLevel===""){
			windowOrg =window.open('sitedetails.jsp?siteId='+siteId+'&mode=M&srcmenu=tdMenuBarItem2&src=networkStudyTab', 'Information1', 'toolbar=no,scrollbar=yes,resizable=yes,menubar=no,status=yes,width=900,height=500, top=100, left=80');
			windowOrg.focus();}
		else{
			windowOrg =window.open('sitedetails.jsp?siteId='+siteId+'&mode=M&srcmenu=tdMenuBarItem2&src=networkStudyTab&networkId='+networkId+'&nLevel='+networkLevel, 'Information1', 'toolbar=no,scrollbar=yes,resizable=yes,menubar=no,status=yes,width=900,height=500, top=100, left=80');
			windowOrg.focus();
		}
		}
	function deleteNetwork(netId,pgRight,mode){

		var checkType;

		if (mode=='N')
			checkType = 'N';
		else
			checkType = 'E';
		if (f_check_perm(pgRight,checkType) == true) {
		if (confirm(M_NtDelMesg)) {
			var studyId=jQuery("#studyId").val();
			var from=jQuery("#from").val();
			var selectedTab=jQuery("#selectedTab").val();
			var pgRight= jQuery("#pageRight").val();
			var moreParam ='{"calledFrom":"studynetworkTabs"'+','+'"studyId":"'+studyId+'"'+'}';
			var urlCall="networksearch.jsp?&calledFrom=studynetworkTabs&src=studynetworkTabs&pageRight="+pgRight+"&flag=D&networkId="+netId+"&expandNtw="+expandNtw+"&studyId="+studyId+"&from="+from+"&selectedTab"+selectedTab+'&moreParam='+moreParam;
			
			jQuery.ajax({
				url: urlCall,
				cache: false,
				type: "POST",
				async:false,
				success : function (data) 
				{
					//alert(data);
					$j("#table2").find("tr:gt(0)").remove();
					$j("#row").closest( "tr" ).after(data);
					hideTableRowSortable();
				}
			});	
	    }else{
	    	return false;
	    }}
	}
	function hideTableRowSortable(){
		  /*Performance issue*/
     // alert("find the hideTableRowSortable button");
	    var table = document.getElementById("table2");
	    
	    
	   $j("#table2 tr").each(function(i,row){
	   
	      var rowId=row.id;
	      if(rowId !='')
	    	if(rowId.indexOf('hidden')>0)
	    		$j("#"+rowId).hide();
	});
	   /*Performance issue*/
		}
	function openWinStatus(pgRight,mode, studyId, networkId,statusId){
		 
		var checkType;

		if (mode=='N')
			checkType = 'N';
		else
			checkType = 'E';

				var otherParam;

					otherParam = "&moduleTable=er_nwsites&statusCodeType=sntwStat&sectionHeading=<%=StringUtil.encodeString(MC.M_StdNtwk_StatDets)%>&fromPage=StudyNetworkTab&statusId=" + statusId;

					if (f_check_perm(pgRight,checkType) == true) {

						windowName= window.open("editstatus.jsp?mode=" + mode + "&studyId="+ studyId+"&modulePk=" +  networkId + otherParam,"statusdetails","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=800,height=600");
						windowName.focus();
					}
			}

	function openDDList(Id,flag){
		
		if(flag==0){
			$j("#"+Id).hide();
			$j("#"+Id.replace("span","siteTypeId")).show();}
		else{
			$j("#"+Id).hide();
			$j("#"+Id.replace("siteTypeId","span")).text($j("#"+Id+" option:selected").text());
			//alert($j("#"+Id).closest('tr').attr('id'));
			var sliceId = $j("#"+Id).closest('tr').attr('id').split("_");
			//alert(sliceId[3]);
			if(sliceId[3]>0)
				saveNetworkRow(sliceId[3],-1,0,0,$j("#"+Id+" option:selected").val());
			else
				saveNetworkRow(sliceId[1],-1,0,0,$j("#"+Id+" option:selected").val());
			$j("#"+Id.replace("siteTypeId","span")).show();
		}
	}
	function networkSitesAppendix(networkId,siteId,from){
		var siteName=document.getElementById("siteName"+siteId).value;
		var networkType=document.getElementById("networkType"+networkId).value;
		windowName =window.open('eventappendix.jsp?networkId='+networkId+'&siteId='+siteId+'&siteName='+siteName+'&networkType='+networkType+'&calledFrom='+from+'&fromPage=selectStudyNetwork&networkFlag=Org_doc&selectedTab=1', 'Information', 'toolbar=no,scrollbar=yes,resizable=yes,menubar=no,status=yes,width=1100,height=600, top=100, left=100');
		  windowName.focus();
		}

	$j(function(){
		fetchNetworkRows();
	    $j( "#searchStudyNetwork").autocomplete({
	        
	        source : function(request, response) {
	            $j.ajax({
	                 url : "userNetworkAutoCompleteJson.jsp",
	                 type : "POST",
	                 autofocus:true,

	                 data : {
	                	 term : request.term,
	                	 studyId:$j("#studyId").val()  
	                 },
	                 dataType : "json",
	                 success: function(data){
	                     response( $j.map( data, function( item ) {
	                         return {
	                             label: item.value,
	                             networkId: item.key     // EDIT
	                         }
	                     }));
	                  },
	                  
	          });
	       },
	       select : function(event, ui) {
	           var networkId=ui.item.networkId
	           addNetworkToStudy(networkId);
		   $j("#searchStudyNetwork").val('');
	           return false;
	       }
	    
	    });
	});


	function addNetworkToStudy(networkId)
	{
		var studyId=document.getElementById("studyId").value;
		var urlCall="";
			urlCall="saveNetwork.jsp?studyId="+studyId+"&networkId="+networkId+"&calledFrom=studynetworkTabs";
		jQuery.ajax({
			url: urlCall,
			cache: false,
			type: "POST",
			async:false,
			success : function (data) 
			{
				fetchNetworkRows();
			}
		});	
	}
	
</script>



<body>
	<jsp:useBean id="siteB" scope="request" class="com.velos.eres.web.site.SiteJB"/>
	
	<%@ page language = "java" import = "com.velos.eres.business.site.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%><%@page import="com.velos.eres.service.util.*"%>

	<DIV class="BrowserTopn" id="divTab"> 
		<jsp:include page="studytabs.jsp" flush="true">
		<jsp:param name="from" value="<%=from %>"/>
		<jsp:param name="selectedTab" value="<%=selectedTab %>"/>
		<jsp:param name="studyId" value="<%=studyId %>"/>
		</jsp:include>
	</DIV>
	
	<SCRIPT LANGUAGE="JavaScript">
	/* Condition to check configuration for workflow enabled */
	<% if("Y".equals(CFG.Workflows_Enabled)&& (studyId!=null && !studyId.equals("") && !studyId.equals("0"))){ %>
			if(screenWidth>1280 || screenHeight>1024)
				document.write('<DIV class="tabDefBotN BrowserBotN  BrowserBotN_S_3 workflowDivBig" id = "div1" style = "height:76%; overflow:hidden;">')
			else
				document.write('<DIV class="tabDefBotN BrowserBotN  BrowserBotN_S_3 workflowDivSmall" id = "div1" style="overflow:hidden;">')
	<% } else{ %>
			
				if(screenWidth>1280 || screenHeight>1024)
			document.write('<DIV class="tabDefBotN BrowserBotN BrowserBotN_S_3" id="div1" style="height:80%; overflow:hidden;">')
			
		else
			document.write('<DIV class="tabDefBotN BrowserBotN BrowserBotN_S_3" id="div1" style="overflow:hidden;">')
	<% } %>
		
	</SCRIPT>
	
<TABLE width="100%">  
<TR>
  	<%
   		
			String uName = (String) tSession.getValue("userName");
			String study = (String) tSession.getValue("studyId");
		     if(study == "" || study == null || "0".equals(study)||study.equalsIgnoreCase("")) {
		    	 %>
		    	   <jsp:include page="studyDoesNotExist.jsp" flush="true"/>
		    	   <% } else {
	 		
   			if (pageRight > 0 )
			{
		   		int accountId=0;   
				//String orderBy="";
				//orderBy=request.getParameter("orderBy");
		
				//if (orderBy==null) 
				//	orderBy="SITE_NAME";
				//String orderType = "";
				//orderType = request.getParameter("orderType");
				//if (orderType == null)
				//{
				//	orderType = "asc";
			//	} 

				String acc = (String) tSession.getValue("accountId");
				accountId = EJBUtil.stringToNum(acc);

				/*SiteDao siteDao = siteB.getByAccountId(accountId,orderBy,orderType);
			
				ArrayList siteIds = siteDao.getSiteIds(); 			
				ArrayList siteTypes = siteDao.getSiteTypes();
				ArrayList siteTypeIds = siteDao.getSiteTypeIds();
				ArrayList siteNames = siteDao.getSiteNames();			
				ArrayList siteInfos = siteDao.getSiteInfos();			
				ArrayList siteParents = siteDao.getSiteParents();			
				ArrayList siteStats = siteDao.getSiteStats();

			   String siteType = null;
			   String siteName = null;	
			   String siteInfo = null;		
			   String siteParent = null;		
			   String siteStat = null;

			   int siteId=0;
			   int siteTypeId=0;
			   int len = siteIds.size();
			   int counter = 0;
			   CodeDao cd1 = new CodeDao();
			   cd1.getCodeValues("site_type");
			   String dSiteType="";*/

	%>
  <TD id="dropNetworkTable">
  </br>
  <div align="left" style="right:1%; top:10%; width:97%;"><input size="80" name="searchStudyNetwork" <%if(pageRight<5 || pageRight==6) {%> disabled <%} %> placeholder="Lookup Networks to Add to this study" id="searchStudyNetwork" autocomplete="off" type="input" style="border-radius:5px;border: 2px solid #aaa;padding:3px 3px 3px 0px;background-position:right;background-image: url('../images/jpg/search_pg.png');background-repeat: no-repeat;"></div>
  </br>
  <SCRIPT LANGUAGE="JavaScript">
		if(screenWidth>1280 || screenHeight>1024){
			document.write('<div id="divRight" style="right:1%; top:10%; width:99%; height:372px; overflow:auto;">')
		}
		else{
			
			document.write('<div id="divRight"  style="right:1%; top:10%; width:99%; height:600px; overflow:auto;">')
			}
		if(screenWidth<1280 && screenHeight<1024){
			
			document.write('<div id="divRight"  style="right:1%; top:10%; width:64%; height:355px; overflow:auto;">')
		}else{
			
			document.write('<div id="divRight"  style="right:1%; top:10%; width:100%; height:355px; overflow:auto;">')
		}
	</SCRIPT>
		<table border="2" width="100%" id="table2" cellpadding="0" cellspacing="0">
			<tr style="color: #D3D3D3" id="row">
				<th width="40%" onClick="setOrder(document.networkTabs,'lower(SITE_NAME)')"> <%=LC.L_Organization_Name%><%--Organization Name*****--%></th>
        		<th width="15%" onClick="setOrder(document.networkTabs,'lower(SITE_TYPE)')"> <%=LC.L_Participate_Type%></th>
        		<th width="10%"> Network Status</th>
        		<th width="5%"> <%=LC.L_Users%><%--Organization Name*****--%> </th>
        		<th width="5%"> <%=LC.L_Documents%><%--Type*****--%> </th>
        		<th width="20%">Study Network Status</th>  
        		<th width="5%"> <%=LC.L_Delete%><%--Type*****--%> </th>
			</tr>
		</table>
	</div>
  </TD>
  </TR></TABLE>
  <input type="hidden" id="studyId" name="studyId" value="<%=studyId%>"/>
  <input type="hidden" id="from" name="from" value="<%=from%>"/>
  <input type="hidden" id="selectedTab" name="selectedTab" value="<%=selectedTab%>"/>
  <%

		} //end of if body for page right

		else
		{%>
  		<jsp:include page="accessdenied.jsp" flush="true"/>
  		<%} //end of else body for page right

	}
		}//end of if body for session

	else
	{%>
  		<jsp:include page="timeout.html" flush="true"/>
  		<%}
	%>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

	<div class ="mainMenu" id="emenu"> 
  		<jsp:include page="getmenu.jsp" flush="true"/>
	</div>
</body>
</html>