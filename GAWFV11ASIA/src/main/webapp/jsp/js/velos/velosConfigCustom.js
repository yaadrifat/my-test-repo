/* 
 * Customization file for all configurable Javascript data. 
 * This file is released once as a blank file and will not be overridden in the subsequence releases.
 * Therefore, the customizations made in this file will not be lost. To customize a value, copy
 * the exact line or block from velosConfig.js. Then change the value here. 
 * Do NOT change anything in velosConfig.js.
 * 
 * Note that this is a site-wide (not an account-wide) configuration. 
 * All these data will be applied to all accounts in a shared/ASP environment.
 * 
 * The data elements that can be accepted in this file are those listed in velosConfig.js only.
 * If a data element is listed in this file, it will override the one in velosConfig.js.
 * If a data element is not listed in this file, the one in velosConfig.js is used.
 *
 * For example, to restrict future date from being entered in Patient Study Status Date,
 * copy the next two lines to the uncommented area:
// Restrict Patient Study Status Date from being a future date
var VAL_PATSTUDYSTAT_RESTRICT_FUTURE_STATDATE = 1;
 *
 */

