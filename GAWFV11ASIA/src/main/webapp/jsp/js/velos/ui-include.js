
				var $C = YAHOO.util.Connect,
				//$E = YAHOO.util.Event,
				//$D = YAHOO.util.Dom,
				$J = YAHOO.lang.JSON,
				$S=YAHOO.util.Selector;
			//	$ = $D.get;
				
    	// keeping track of what to show
			var totalRecords, 
			

				recordsReturned , 
				startIndex = 0 , 
				endIndex , 
				sortDir, 
				sortKey,
				module,
				defaultParam,
				addlParam,
				globalSpecimenDataTable,
				dataTable='serverpagination'
				,curPage
				,totalPages
				,filterColumns
				,filterColumnArray
				,rowsPerPage=0;
			function includeJS(file)
			{
			  var script  = document.createElement('script');
			  script.src  = file;
			  script.type = 'text/javascript';
			  script.defer = true;
			  document.getElementsByTagName('head').item(0).appendChild(script);
			}
	    includeJS('js/velos/velosUtil.js');		    
        var CommErr_CnctVelos=M_CommErr_CnctVelos;
        var Save_View=L_Save_View;
        var Saved_Search=L_Saved_Search;
        var Export_ToExcel_Upper=L_Export_ToExcel;
        var Search_Widget=L_Search_Widget;
        var Save_As=L_Save_As;
        var Save_Search=L_Save_Search;
        var Saved_Searches=L_Saved_Searches;
        var Delete_Searh=L_Delete_Searh;
        var Reset_Search=L_Reset_Search;
        var PleaseWait_Dots=L_PleaseWait_Dots;        
        var Curr_Page=L_Curr_Page;
        var RowsPer_Page=L_RowsPer_Page;
        var varShowing=L_Showing;
        var Of_Lower=L_Of_Lower;
        var Previous=L_Previous;
        var Next=L_Next;
        var Last=L_Last;
        var First=L_First;
        var SavedSearch=L_SavedSearch;
        
VELOS.ajaxObject=function (url,args)
{
	   
	this.url=url;
	this.reqType="POST";
	
	if (args.outputElement){
		this.handleSuccess = function(o){ 
			var defaultSuccess = function(o){ 
			    hideMsg();
			    if((o.responseText) && (o.argument[0].outputElement)){
			    	o.argument[0].outputElement.innerHTML=o.responseText;
			    }
			}(o);

			var userSuccess = eval(args.success);
			return (userSuccess);
		};
	}else{
		this.handleSuccess = eval(args.success);
	}
this.handleFailure = function(o){
		hideMsg();
		alert(CommErr_CnctVelos);/*alert("Communication Error. Please contact Velos Support");*****/
	
};
this.initConfigs(args);
}
    
VELOS.ajaxObject.prototype.initConfigs=function(args) {
if(args) {
        if(args.constructor != Object) {
            args = null;
            return false;
            
        }else
 {
  
    if (args.urlData)
     this.urlData= args.urlData;
    if (args.reqType) {
     this.reqType=args.reqType;   
     }
    if (args.context) {
     if (typeof this.context=='object') {	
     this.context=(args.context);   
     } else 
     {
     this.context=$(args.context);
     }
     }
    if (args.outputElement) {
    if (typeof this.outputElement=='object') {
    this.outputElement=args.outputElement;
    } else {
    this.outputElement=$(args.outputElement);
    }
    }

    if ((args.success) && (typeof args.success=='function'))  
    this.handleSuccess=args.success;
    if ((args.failure) && (typeof args.failure=='function'))  
    this.handleSuccess=args.success;
    
    if ((args.scope) && (typeof args.scope=='object'))  
    this.scope=args.scope;
 
    }
  
    
  }
}
VELOS.ajaxObject.prototype.startRequest=function() {
 beInteractive(this.context,{text:"<img src='../images/indicator.gif' align='absmiddle' />",
                             type:'overlay' });
 var callback =
{
  success:this.handleSuccess,
  failure:this.handleFailure,
  scope: window ,
  argument:[this,this.scope]
};  
if (this.reqType=="GET" )
{
 var url=this.url+"?"+this.urlData;
 
 $C.asyncRequest('GET', url, callback);
}
else {
 $C.asyncRequest('POST', this.url, callback,this.urlData);
 }
 }

VELOS.Paginator=function(module,args)
{
	var myDataTable,recordsReturned,endIndex,totalRecords,defaultQuery,filterArray;
    this.module=module;
    this.startIndex==0;
    this.initConfigs(args);
	if (args.noIndexGeneration)
	{
		this.uid=1;
	}
	else
	{
		this.uid=VELOS.generateIndex();
	}
	// Main function to draw a pagefull of data depending on the above variables
	this.getPage = function()
	{
		showPanel();
		newRequest=this.getQuery();
		$C.asyncRequest
		(
				'POST',
				'../paginate',
				{
					success: function (o)
					{
						var r = $J.parse(o.responseText);
						$('CSRFToken').value=r.CSRFToken;
						// if the data table already exists, repopulate it, otherwise, create it
						if (myDataTable)
						{
							myDataTable.set("sortedBy",
							{
								"key": this.sortKey,
								"dir": this.sortDir
							});

							myDataTable.onDataReturnInitializeTable
							(
									newRequest,
									{
										error:false,
										results:r.resultSet
									}
									,
									{
										"sortedBy": { "key": this.sortKey, "dir": "yui-dt-"+this.sortDir }
									}
							);
						}
						else
						{
							// actually, the meta information coming from the server might need some converting, 
							// but in this case it can be taken verbatim
							var colArray = r.colArray;
							var myColumnDefs=r.meta;
								
							//Process CustomFormatter , if present
							var f = [];
							for(var i = 0; i < myColumnDefs.length; i++)
							{
								var c = myColumnDefs[i];
								var formatter=c.format;
								if (formatter)
								{
									c.formatter=formatterDispatcher;  
									   
								}
          						var editor=c.edit;
          						if (editor)
								{
          							c.editor=editorDispatcher;  
									   
								}
          							 
							}
								
								
							// DataSource instance
							var myDataSource = new YAHOO.util.DataSource(r.resultSet);
							myDataSource.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
							myDataSource.responseSchema =
							{
									fields: colArray
							};
							// DataTable instance
							myDataTable = new YAHOO.widget.DataView
							(
									this.dataTable, 
									myColumnDefs, 
									myDataSource,
									{
										"sortedBy" :
										{
											"key": this.sortKey,
											"dir": this.sortDir
										}
										
									}
									
							);
							    
							// If myDataTable is empty, all the rest will fail; so return.
							if (!myDataTable || !myDataTable.getTableEl()) { hidePanel(); return; }
								
							myDataTable.subscribe("cellClickEvent", myDataTable.onEventShowCellEditor);
							myDataTable.getTableEl().style.tableLayout = 'auto';
							// sort handler, it doesn't actually make any sort, the server will do that, 
							// it just sets the variables to ask the server for the sorted data
						
							myDataTable.sortColumn = function(oColumn)
							{
								//Modified for Bug#8464 | Date: 20 FEB 2012 |By :YPS
								checkedDraftIds="";
								if(oColumn.key === this.parentObject.sortKey)
								{
									this.parentObject.sortDir = (this.parentObject.sortDir === 'asc') ?
												'desc' : 'asc';
								}
								else
								{
									this.parentObject.sortDir = 'asc';
								}
								this.parentObject.sortKey = oColumn.key;
								this.parentObject.render();
							};
								
						}
							
						
						myDataTable.parentObject=this;	
						myDataTable.on('refreshEvent',function()
						{
							var cells = YAHOO.util.Dom.getElementsByClassName('hide', 'th', myDataTable.getTableEl());
							for (var i = 0; i < cells.length; i++)
							{
								var trCells = YAHOO.util.Dom.getElementsByClassName('yui-dt-col-'+cells[i].id.substring(11), 
										'td', myDataTable.getTableEl());
								YAHOO.util.Dom.addClass(trCells,'hide');
							}
							var unhideCells = YAHOO.util.Selector.query('th:not([class*=hide])', myDataTable.getTableEl());
							for (var i = 0; i < unhideCells.length; i++)
							{
								var trCells = YAHOO.util.Dom.getElementsByClassName('yui-dt-col-'+unhideCells[i].id.substring(11), 
										'td', myDataTable.getTableEl());
								YAHOO.util.Dom.removeClass(trCells,'yui-dt-hidden');
							}
						});
						// reading the variables from the server that tells us what it has delivered
						recordsReturned = parseInt(r.rowsReturned,10); // How many records this page
						this.startIndex = parseInt(r.firstRecord,10); // Start record index this page
						endIndex = this.startIndex + recordsReturned -1; // End record index this page
						totalRecords = parseInt(r.totalRecords,10); // Total records all pages
						if ((this.rowsPerPage==0) && (recordsReturned>0)) this.rowsPerPage=recordsReturned;
							
							
						//Calculate pages for the records
						var tot_Page;
							
						if (totalRecords==0)
						{ 
							totalPages=0;
						}
						else
						{
							tot_Page=totalRecords/this.rowsPerPage;
							totalPages=parseInt(Math.round(totalRecords/this.rowsPerPage),0);
							
							if (totalPages<tot_Page) totalPages++;
							
						} 
							
						var cur_page=endIndex/this.rowsPerPage;
						curPage=parseInt(Math.round(endIndex/this.rowsPerPage));
							
						if (curPage<cur_page) curPage++;
						if (curPage<1 || isNaN(curPage)) curPage=1;
							
							/*
							myDataTable.set('sortedBy', {
								key: this.sortKey,
								dir: this.sortDir
							}
							);
							*/
							
						this.globalSpecimenDataTable = myDataTable;
						var dt=$(this.dataTable);
						if (this.navigation)
						{
							//Enable the navigation header
							var dtNav=$(this.uid+'_dt-page-nav');
							dt.className="d-tbl yui-dt";
							var pl=$(this.uid+'_prevLink'),nl=$(this.uid+'_nextLink'),fl=$(this.uid+'_firstLink'),
							ll=$(this.uid+'_lastLink'),pn=$(this.uid+'_pgNum')
							,rp=$(this.uid+'_rowsPerPage'),
							sr=$(this.uid+"_search"),sv=$(this.uid+"_save");
							var vc =$(this.uid+'_view'),ac=$(this.uid+'_add'),mc=$(this.uid+'_manage'),ps=$(this.uid+'_prepare');
							if (!dtNav)
							{
								dtNav=	this.navigationLinks();
								$D.insertBefore(dtNav,dt);
								var pl=$(this.uid+'_prevLink'),nl=$(this.uid+'_nextLink'),fl=$(this.uid+'_firstLink'),
								ll=$(this.uid+'_lastLink'),pn=$(this.uid+'_pgNum')
								,rp=$(this.uid+'_rowsPerPage'),
								sr=$(this.uid+"_search"),sv=$(this.uid+"_save"),ev=$(this.uid+"_excel");
								//added by Bikash for INVP 2.8
								var vc =$(this.uid+'_view'),ac=$(this.uid+'_add'),mc=$(this.uid+'_manage'),ps=$(this.uid+'_prepare');
							
								$E.addListener(pl, 'click', getPreviousPage, this, true);
								$E.addListener(nl, 'click', getNextPage, this, true);
								$E.addListener(fl,'click',getFirstPage,this,true);
								$E.addListener(ll,'click',getLastPage,this,true);
								$E.addListener(sr,'click',toggleSearchSetting,this,true);
								$E.addListener(sv,'click',saveSettings,this,true);
								$E.addListener(ev,'click',exportData,this,true);
								$E.addListener(pn,'keydown',goToPage,this,true);
								$E.addListener(rp,'change',getRowsPerPage,this,true);
								//added by Bikash for INVP 2.8
								$E.addListener(vc,'click',viewCart,this,true);
								$E.addListener(ac,'click',addToCart,this,true);
								$E.addListener(mc,'click',manageCart,this,true);
								$E.addListener(ps,'click',prepareSpecimen,this,true);
								//$E.addListener(myDataTable,'headerCellClickEvent',sortTable,this,true);
						
							}
					
						
							pl.className = (this.startIndex === 1) ? 'd-btn nav-prev-disable' :'d-btn nav-prev' ;
							nl.className =(endIndex  >= totalRecords) ? 'd-btn nav-next-disable' :
									'd-btn nav-next';		
							fl.className=(curPage==1) ? 'd-btn nav-first-disable':'d-btn nav-first';
							ll.className=(curPage==totalPages || totalPages==0 )?'d-btn nav-last-disable':'d-btn nav-last';		
							$(this.uid+'_startIndex').innerHTML = this.startIndex;
							$(this.uid+'_endIndex').innerHTML = endIndex;
							$(this.uid+'_ofTotal').innerHTML =  totalRecords;
							$(this.uid+'_totalPages').innerHTML=totalPages;
							pn.value=curPage;
							if(this.rowsPerPage<5)
								rp.value=5;
							else
								rp.value=this.rowsPerPage;
						}
						// Subscribe to events for row selection
						//myDataTable.subscribe("rowMouseoverEvent", myDataTable.onEventHighlightRow);
						//myDataTable.subscribe("rowMouseoutEvent", myDataTable.onEventUnhighlightRow);
						//myDataTable.subscribe("rowClickEvent", myDataTable.onEventSelectRow);
						
						// Programmatically select the first row immediately
						//myDataTable.selectRow(myDataTable.getTrEl(0));
						hidePanel();
					       
					},
					failure: function (o)
					{
						hidePanel();
						var paramArray = [o.statusText];
						alert(getLocalizedMessageString("L_Fail",paramArray));/*alert('Failure: ' + o.statusText);*****/
						myDataTable.onDataReturnInitializeTable(newRequest,null,true);
					},
					scope:this
				},newRequest
			);
		}
		this.getQuery=function()
		{
			var l_filter=	this.buildCriteria();
			
			this.setExtParam(l_filter);
			var newRequest = 'module='+this.module+'&curPage=' + this.startIndex + '&rowsPerPage=' + this.rowsPerPage;
			if (this.defaultParam)
			{
				if (!defaultQuery) defaultQuery=createQuery(this.getDefaultArray());
				newRequest += '&' + defaultQuery;
			}
			if (this.addlParam)
			{
				newRequest += '&' + this.addlParam;
				//addlParam="";
			}
			if (this.sortKey)
			{
				if (this.sortDir) 
				{
					newRequest += '&orderType=' + this.sortDir;
				}
				else
				{
					newRequest += '&orderType=asc';
				}
				newRequest += '&orderBy=' + this.sortKey
			}
			if (myDataTable === undefined) newRequest += '&meta=true';
				
			return newRequest;
		}
		var getPreviousPage = function(e)
		{
			$E.stopEvent(e);
			// Already at first page
			if(this.startIndex == 1)
			{
				return;
			}
			// it can be negative, the server will set it to zero anyway
			this.startIndex -= this.rowsPerPage;
				
			this.render();
		};
			
		var getNextPage = function(e)
		{
			$E.stopEvent(e);
			// Already at last page
				
			if((this.startIndex + this.rowsPerPage) - 1  >=  totalRecords)
			{
				return;
			}
			this.startIndex += this.rowsPerPage;
			this.render();
		};
			
		var getLastPage=function(e)
		{
			$E.stopEvent(e);
			if (curPage==totalPages || totalPages==0) return;
			this.startIndex=((totalPages-1)*this.rowsPerPage)+1;
			this.render();
		}
		var getFirstPage=function(e)
		{
			$E.stopEvent(e);
			if (curPage==1) return;
			this.startIndex=1;
			this.render();
		}
		var goToPage=function(e)
		{
			if (e.keyCode==13) 
			{
				$E.stopEvent(e);
				var page=$(this.uid+'_pgNum').value;
				if (totalPages==0) return;
				if (page==curPage) return;
				if (page>totalPages) page=totalPages;
				this.startIndex=( (page*this.rowsPerPage) - this.rowsPerPage ) + 1;			
				this.render();
			}
		}
		var getRowsPerPage = function (e)
		{
			//Modified for Bug#8464 | Date: 20 FEB 2012 |By :YPS
			checkedDraftIds="";
			var tempRowPerPage = $E.getTarget(e).value;
			if(tempRowPerPage == "")
			{				
				this.rowsPerPage = 0;
			}
			else
			{
				this.rowsPerPage = parseInt($E.getTarget(e).value,10);
				//this.rowsPerPage = $(this.id+"_rowsPerPage").value;
				this.render();
			}
		};
		var exportData=function(e)
		{
			//if (totalPages==0) return;
			var moduleName = "";
			if(document.getElementById("moduleName"))
			moduleName = document.getElementById("moduleName").value;
			var o_rp=this.rowsPerPage;
			this.rowsPerPage=totalRecords;
			var o_st=this.startIndex;
			this.startIndex=0;
			var fltrD='',fltr='';
			fltrD=this.getQuery();
			fltr=this.buildCriteria();
			//reset RowsPerpage,startIndex back to Original
			this.rowsPerPage=o_rp;
			this.startIndex=o_st;
			
			fltr=fltrD+'&'+fltr+"&dmode=xls"; 
			var refObj=$E.getTarget(e);
			new VELOS.ajaxObject('../paginate', 
			{
				urlData:fltr,
				reqType:"POST",
				context:refObj,
				success:function(o)
				{
					url=o.responseText+"&moduleName="+moduleName;
					  
					var dummyform = document.createElement("form");
					dummyform.setAttribute("method", "post");
					dummyform.setAttribute("action", "exportData.jsp");

					// setting form target to a window named 'formresult'
					dummyform.setAttribute("target", "expwin");

					var hiddenField = document.createElement("input");
					hiddenField.setAttribute("name", "url");
					hiddenField.setAttribute("value", url);
					hiddenField.setAttribute("type", "hidden"); // YK 14Sep2011: Added to hide the unexpected text (Bug #6917) 
					dummyform.appendChild(hiddenField);
					document.body.appendChild(dummyform);

					// creating the 'formresult' window with custom features prior to submitting the form
					expwin = window.open('donotdelete.html', 'expwin', 'width=200,height=150,resizable=1');

					dummyform.submit();
							
					// expwin=window.open("exportData.jsp?url="+url,'expwin','width=200,height=150,resizable=1');
					 
					//setTimeout( "newwindow.location.href = url", 0 ); 
					//expwin=window.open('donotdelete.html','expwin');
					//setTimeout( "expwin.location.href = url", 0 );
      				        				  
				}
			}).startRequest();
		}

		//this.render();
		this.getFilterArray=function()
		{
			//filterArray=new Array();
			if ((this.filterParam) && (!filterArray))
			{
				filterArray=this.filterParam.split(",");
			}
			return filterArray;
		}
		this.getDefaultArray=function()
		{
			var defaultArray=new Array();
			if ((this.defaultParam) && (!this.defaultArray))
			{
				this.defaultArray=this.defaultParam.split(",");
			}
			return this.defaultArray;
		}
		this.setExtParam=function(param)
		{
			this.addlParam=param;
		}
   
		var saveSettings=function(e)
		{
			var cells = YAHOO.util.Selector.query('th:not([class*=hide])', myDataTable.getTableEl());
			var hideCells = YAHOO.util.Dom.getElementsByClassName('hide', 'th', myDataTable.getTableEl());
			var tCol='';
			var hCol='';
			var fStr='';
			for (var i=0; i<cells.length;i++)
			{
				tCol=cells[i].id.replace(/^yui-dt0-th-/,'');
				var isHidden = false;
				for (var j=0; j<hideCells.length; j++)
				{
					hCol=hideCells[j].id.replace(/^yui-dt0-th-/,'');
					if (hCol == tCol) isHidden = true;
				}
		      	if (!tCol || isHidden) continue;
		      	if (fStr.length > 0)
		      		fStr=fStr+','+tCol;
		      	else
		      		fStr=tCol;
			}
	     	var rp=$(this.uid+'_rowsPerPage').value;
	     	var refObj=$E.getTarget(e);
			//alert("Sent To Database"+fStr+ 'with Rows='+rp );
			new VELOS.ajaxObject('../config',
			{
				urlData:'module='+this.module+'&reqKey=bsetting&row='+rp+"&reqType=save&"+this.module+"_meta="+fStr+"&"+this.module+"_rows="+rp+"&"+defaultQuery,
				reqType:"POST",
				context:refObj
			}).startRequest();
		}
		var resetFilter=function(refObj)
		{
	
		}
		this.saveSearch=function(e)
		{
			var criteria=this.buildCriteria();
			var refObj=e.currentTarget;
			if (!refObj) refObj=$E.getTarget(e).parentNode;
     
			var list=$E.getListeners(refObj);
			var snm=$(this.uid+'_searchName');
			var searchName=snm.value;
			var searchDD=$(this.uid+'_s-search-dd').firstChild;
			if (searchDD)
			{
				var opts=searchDD.options;
				for (var i=0;i<opts.length;i++)
				{
					var text=opts[i].text;
					if (text==searchName)
					{
						beInteractive(refObj,{text:"Duplicate"});
						return false;
						break;
					}
				}
			}
			snm.value="";
			if ((criteria) && (searchName))
			{
				postData="name="+escape(searchName)+"&criteria="+escape(criteria)+"&module="+module+"&"+defaultQuery+"&reqKey=search&reqType=save";
     
				new VELOS.ajaxObject('../config',
				{
					urlData:postData,
					reqType:"POST",
					context:refObj,
					scope:this,
					success:function(o)
					{
						hideMsg();
						o.argument[1].retrieveSearch(refObj);
        
					}
				}).startRequest();
			}
		}
		this.deleteSearch=function(e)
		{
			var searchDD=$(module.toLowerCase()+'_search');
			if (searchDD)
			{
				var selectedIndex=searchDD.selectedIndex;
		        var searchName=searchDD.options[selectedIndex].text;
		        var searchVal=searchDD.options[selectedIndex].value;
		        var refObj=$E.getTarget(e);
		      	if ((searchName) && (searchVal))
		      	{
		      		new VELOS.ajaxObject('../config',
		      		{
		      			urlData:"name="+escape(searchName)+"&module="+module+"&"+defaultQuery+"&reqKey=search&reqType=delete",
		      			context:refObj,
		      			scope:this,
		      			success:function(o)
		      			{
		      				hideMsg();
		      				o.argument[1].retrieveSearch(refObj);
		      			}
		      		}).startRequest();
		      	}
      	 
			}
   
		}
		var toggleSearchSetting=function(e)
		{
			var search= $(this.uid+'_searchsave');
			if (!search) search=this.searchWidget();
			search.className="";
			hangDIV(search,this.uid+"_search");
			var refObj=$E.getTarget(e);
			this.retrieveSearch(refObj);
   
		}
   
		this.retrieveSearch=function(refObj)
		{
			var postData="module="+module+"&reqKey=search&reqType=get&"+defaultQuery;
			new VELOS.ajaxObject("../config",
			{
				urlData:postData,
				reqType:"POST",
				context:refObj,
				scope:this,
				success:function(o)
				{
					hideMsg();
					var eln=o.argument[1].uid;
					var el=$(eln+"_s-search-dd");
					var ssearch=$(eln+"_ssearch");
					var dsearch=$(eln+"_dsearch");
					var rsearch=$(eln+"_resetSearch");
      	 
					el.innerHTML=o.responseText;
			      	
					$E.addListener(el.firstChild, 'change', o.argument[1].applyCriteria, o.argument[1], true);
					if (!$E.getListeners(ssearch)) $E.addListener($(eln+"_ssearch"), 'click', o.argument[1].saveSearch, o.argument[1], true);
					if (!$E.getListeners(dsearch)) $E.addListener($(eln+"_dsearch"), 'click', o.argument[1].deleteSearch, o.argument[1], true);
					if (!$E.getListeners(rsearch)) $E.addListener($(eln+"_resetSearch"), 'click', o.argument[1].applyCriteria, o.argument[1], true);
				}
   
			}).startRequest();
   
   
		}
 
		this.applyCriteria=function(e,obj)
		{
			var nvpairs=new Object();
			var f_criteria="";
			f_criteria=$E.getTarget(e).value;
			if (!f_criteria) f_criteria="";
			if (e.type=='click')
			{
				//KLUDGE:flag to indicate that search needs to be reset
				f_criteria='reset';
   
			}
			if (f_criteria)
				nvpairs=parseQuery(f_criteria);
			var fArray=this.getFilterArray();
			if (fArray)
			{
				var fltrCol="";
				var fltrVal="";
				for (var i=0;i<fArray.length;i++)
				{
					fltrCol=fArray[i];
					var fltrColElem=$(fltrCol);
	
					fltrVal=nvpairs[fltrCol];
					if(fltrCol!='CSRFToken'){
					if (fltrVal)
					{
						if (fltrColElem.type=="checkbox")
						{
							if (fltrVal==1)
							{
								fltrColElem.checked=true;
							}
						} //end for .type
						else
						{ //else for .type
							fltrColElem.value=fltrVal;
						}
						//if (fltrColElem.onchange) fltrColElem.onchange();
					}
					else
					{
						fltrColElem.value="";
						if (fltrColElem.type=="checkbox")
						{
							fltrColElem.checked=false;
						} //end for .type
					} //End else
					}
				} //End for
			} //end if
    
			if (f_criteria)
			{
				if (this.refreshMethod)
				{
					var func=eval(this.refreshMethod) ;
					//func.call(f_criteria);
				}
				else
				{
					this.runFilter(f_criteria);
				}
			}
		}
 
		var parseQuery=function(query)
		{
			var objURL = new Object();
 
			// Use the String::replace method to iterate over each
			// name-value pair in the query string. Location.search
			// gives us the query string (if it exists).
			query.replace
			(
					new RegExp( "([^?=&]+)(=([^&]*))?", "g" ),
					// For each matched query string pair, add that
					// pair to the URL struct using the pre-equals
					// value as the key.
					function( $0, $1, $2, $3 )
					{
						objURL[ $1 ] = $3;
					}
			);
			return objURL;
		}
		var createQuery=function(objArray)
		{
			var filter='';
			var fltrElem,fltrValue;
			for (var i=0;i<objArray.length;i++)
			{
				fltrElem=$(objArray[i]);
				if (fltrElem)
				{
					fltrValue=fltrElem.value;
					if (fltrValue)
					{
						if (filter.length==0)
						{
							filter=objArray[i]+"="+fltrValue;
						}
						else
						{     //filter.length
							filter=filter+"&"+objArray[i]+"="+fltrValue;
						}
					}//fltrValue
   
				}//fltrElem
			}
			return filter;
		}
  
    
		this.buildCriteria=function()
		{
			var filter="";
			var fArray=this.getFilterArray();
			var t_fltrValue,t_fltrElem;
			if (fArray)
			{
				for (var i=0;i<fArray.length;i++)
				{
					t_fltrElem=$(fArray[i]);
					if (t_fltrElem)
					{
  
						if (t_fltrElem.type=="checkbox")
						{
							if (t_fltrElem.checked)
							{
								t_fltrValue=1;
							}
							else
							{
								t_fltrValue=0;
							}
						} //end for .checkbox 
						else
						{ //Else for .type
							t_fltrValue=$(fArray[i]).value;
						}
						//KLudge: Handle string "0" since we use "0" in lot of DD.
						//best would be have consistency in all DD
						if ((t_fltrValue)=="0") t_fltrValue="";
  
						if (t_fltrValue)
						{
							t_fltrValue = t_fltrValue+"";
							t_fltrValue = t_fltrValue.replace(/^\s+/, '').replace(/\s+$/, '');
							if (filter.length>0)
								filter=filter+"&"+fArray[i]+"="+encodeURIComponent(t_fltrValue);
							else
								filter=fArray[i]+"="+encodeURIComponent(t_fltrValue);
						}
					}
				}
			}


			return filter;
		}    
      
		this.runFilter=function(filter)
		{
			var l_filter;
			if (filter && filter!='reset')
			{
				l_filter=filter
			}
			else
			{
				l_filter=this.buildCriteria();
			}
			this.setExtParam(l_filter);
			this.startIndex=0;
			this.rowsPerPage=0;
			this.render();
			return false;
		}

		this.navigationLinks = function()
		{
			var enav=document.createElement('div');
			enav.id=this.uid+"_dt-page-nav";
			enav.className="dt-page-nav"
				var exportLinks="";
			var paramArray3 = ["<span id='"+this.uid+"_totalPages'>"];
			var navLinks="<table border=0 width='100%'><tbody><tr><td><button title='"+First+"' id='"+this.uid+"_firstLink' class='d-btn nav-first-disable' type='button'></button></td>" +
			"<td><button title='"+Previous+"' id='" +this.uid+"_prevLink' class='d-btn nav-prev-disable' type='button'></button></td><td>"+
			"<td><span class='split'></span></td><td>"+Curr_Page/*Current Page :******/+"<input type='text' id='"+this.uid+"_pgNum' size='2'>"+getLocalizedMessageString("L_Total_Pages",paramArray3)/*Total Pages <span id='"+this.uid+"_totalPages'>*****/+"</span></td>" +
			"<td><span class='split'></span></td><td><button title='"+Next+"' id='"+this.uid+"_nextLink'  class='d-btn nav-next-disable' type='button' ></button></td><td>"+
			"<button title='"+Last+"' id='"+this.uid+"_lastLink'  class='d-btn nav-last-disable' type='button'></button></td><td><span class='split'></span></td>"+
			"<td> &nbsp;&nbsp; <span> "+RowsPer_Page/*Rows per page*****/+" </span><select id='"+this.uid+"_rowsPerPage'>";
			var opts='';
			var paramArray2 = ["<span id='"+this.uid+"_startIndex'>0</span> - <span id='"+this.uid+"_endIndex'>0</span>","<span id='"+this.uid+"_ofTotal'></span>"];
			for(var i=0;i<this.rowSelection.length;i++)
			{
				opts=opts+"<option value='"+this.rowSelection[i]+"'>"+this.rowSelection[i]+"</option>";  
			}
			navLinks=navLinks+opts;
			navLinks=navLinks+"</select></td><td>&nbsp; "+getLocalizedMessageString("L_Showing_Of",paramArray2)/*Showing <span id='"+this.uid+"_startIndex'>0</span> - <span id='"+this.uid+"_endIndex'>0</span>"+
    		" of   <span id='"+this.uid+"_ofTotal'></span>*****/+"</td>";
			if (this.saveEnable)
			{
				navLinks=navLinks+
				"<td ><A href='javascript:void(0);' id='"+this.uid+"_save' title='"+Save_View/*Save View*****/+"'><img src='./images/save.gif' border='0'/></A></td>";
			}
			if (this.searchEnable)
			{
				navLinks=navLinks+
				"<td >&nbsp;&nbsp;<A href='javascript:void(0);' id='"+this.uid+"_search' title='"+SavedSearch/*Save(d) Search*****/+"'><img type='image' src='../images/jpg/search_pg.png' border='0'/></A></td>";
			}
			navLinks = formSpecimenNavigation(navLinks,this.viewCartEnable,this.addCartEnable,this.manageCartEnable,this.prepareEnable,this.uid);	
			if (this.exportEnable)
			{
				exportLinks="<td > <A href='javascript:void(0);' id='"+this.uid+"_excel' title='"+Export_ToExcel_Upper+"'><img type='image' src='../images/jpg/excel.GIF' border='0'/></A></td>"
			}
			navLinks=navLinks+exportLinks+"</tr></tbody></table>";
			enav.innerHTML=navLinks;
			return enav
		}
		this.searchWidget=function()
		{
			var srchStr="";
			var srchW=document.createElement('div');
			srchW.id=this.uid+"_searchsave";
			srchW.className="hide";
			srchStr="<div class=\"hd\">"+Search_Widget/*Search Widget*****/+"</div><div class=\"bd\"><table><tr><td><span>"+Save_As/*Save as*****/+" "+
			"<input id=\""+this.uid+"_searchName\" type=\"text\" size=\"10\" maxlength=\"100\"/></span>"+
			"<A href=\"javascript:void(0);\" title=\""+Save_Search/*Save Search*****/+"\" id=\""+this.uid+"_ssearch\">"+
			"<img src=\"./images/save.gif\" border=\"0\"></img></A></td></tr><tr><td>"+
			"<span id=\""+this.uid+"_s-search-span\">"+Saved_Searches/*Saved Searches*****/+"<span id=\""+this.uid+"_s-search-dd\"></span></span></td>"+
			"<td><A href=\"javascript:void(0);\" title=\""+Delete_Searh/*Delete Search*****/+"\" id=\""+this.uid+"_dsearch\" >"+
			"<img src=\"../images/jpg/delete_pg.png\" border=\"0\"></A></td></tr></table></DIV><div class=\"ft\"><A href=\"javascript:void(0)\" id=\""+this.uid+"_resetSearch\">"+Reset_Search/*Reset Search*****/+"</A></div>";
			srchW.innerHTML=srchStr;
			return srchW;
		}			

		var formatterDispatcher = function (elCell, oRecord, oColumn,oData) 
		{
			var meta = oColumn.format;
			var type=typeof meta;
			if (meta)
			{
				switch (meta)
				{
					case 'Number':
						YAHOO.widget.DataTable.formatNumber.call(this,elCell, oRecord, oColumn,oData);
						break;
					case 'date':
						YAHOO.widget.DataTable.formatDate.call(this,elCell, oRecord, oColumn,oData);
						break;
					case 'text':
						YAHOO.widget.DataTable.formatText.call(this,elCell, oRecord, oColumn,oData);
						break;
					case 'Radio':
						elCell.innerHTML = oData;
						break;
					default :
					    var func=eval(meta) ;
					func.call(this,elCell,oRecord, oColumn,oData);	
				}
			}
		};
			
		var editorDispatcher = function(oEditor, oSelf)
		{
			var meta = oColumn.edit;
			var type=typeof meta;
			switch (meta)
			{
				case 'Number':
					YAHOO.widget.DataTable.editTextbox.call(oSelf,oEditor, oSelf);
					break;
				case 'Date':
					YAHOO.widget.DataTable.editDate.call(oSelf,oEditor, oSelf);
					break;
				case 'Text':
					YAHOO.widget.DataTable.editTextbox.call(oSelf,oEditor, oSelf);
					break;
				case 'radio':
					YAHOO.widget.DataTable.editRadio.call(oSelf,oEditor, oSelf);
					break;
			}
		}
	};
		VELOS.Paginator.prototype.initConfigs=function(args)
		{
		if(args) {
        if(args.constructor != Object) {
            args = null;
            
        }else
 		{
 		 this.rowsPerPage=args.rowsPerPage || 0;
 		 this.sortDir=args.sortDir||asc;
 		 this.sortKey=args.sortKey;
 		 this.defaultParam=args.defaultParam;
 		 this.dataTable=args.dataTable ||'serverpagination' ;
 		 this.filterParam=args.filterParam;
		 this.addlParam=args.addlParam;
		 this.rowSelection=args.rowSelection||[5,10,15,25,50,75,100];
		 this.saveEnable=(typeof args.saveEnable!="undefined")?	args.saveEnable:true;
		 this.searchEnable=(typeof args.searchEnable!="undefined")?	args.searchEnable:true;
		 this.exportEnable=(typeof args.exportEnable!="undefined")?	args.exportEnable:true;
		 this.viewCartEnable=(typeof args.viewCartEnable!="true")? args.viewCartEnable:true;
		 this.addCartEnable=(typeof args.addCartEnable!="true")? args.addCartEnable:true;		 		 
		 this.prepareEnable=(typeof args.prepareEnable!="true")?	args.prepareEnable:true;		 
		 this.manageCartEnable=(typeof args.manageCartEnable!="true")?	args.manageCartEnable:true;
		 this.navigation=(typeof args.navigation!="undefined")?	args.navigation:true;
		 this.refreshMethod=args.refreshMethod||'';
         		 
		}
		}
		}
	VELOS.Paginator.prototype.render=function()
	{
	this.getPage();
	}	
   
   function hangDIV(obj,cntxt)
   {
    //var cntxt=this.uid+"_search";
    var panel= new YAHOO.widget.Panel(obj, { width:"320px", visible:false,constraintoviewport:true,underlay: "none",context:[cntxt, "bl", "tr"]   } );
   var success = panel.render(document.body);
   panel.show();
   
   
   }
   function beInteractive(obj,args)
   {
    var l_class="",l_text="",l_type="";
    if (args)
     {
      l_class=(args.formatClass)?args.formatClass:"";
      l_text=(args.text)?args.text:"";
      l_type=(args.type)?args.type:"attach";
     }
     
    var msgDiv=getMsgContext();
    msgDiv.className=l_class;
    msgDiv.innerHTML=l_text;
    
    if (l_type=="attach"){
    $D.insertAfter(msgDiv,obj);
    //obj.appendChild(msgDiv);
    }  else if (l_type=="overlay") {
    
    var pos=$D.getXY(obj);
    $D.setXY(msgDiv,pos);
    }
     
   }
   
         
function getMsgContext()
{   
 var msgDiv=$('msgDiv');
  if (!msgDiv) { 
    msgDiv=document.createElement("div");
    msgDiv.id="msgDiv";
   msgDiv.className="hide";
   //document.body.appendChild(msgDiv);
  } 
return msgDiv;
}
function hideMsg()
{   
 getMsgContext().innerHTML="";
 getMsgContext().className="hide";
}
function showMsg()
{   
 getMsgContext.className="";
}

function loadingPanel() {
// Initialize the temporary Panel to display while waiting for external content to load
if (!(VELOS.wait)) {
VELOS.wait = 
		new YAHOO.widget.Panel("wait",  
			{ width:"240px", 
			  fixedcenter:true, 
			  close:false, 
			  draggable:false, 
			  zindex:4,
			  modal:true,
			  visible:false
			} 
		);

VELOS.wait.setHeader(PleaseWait_Dots);/*VELOS.wait.setHeader("Loading, please wait...");*****/
VELOS.wait.setBody('<img class="asIsImage_progbar" src="../images/jpg/loading_pg.gif" />');
VELOS.wait.render(document.body);
}
}
function showPanel()
{
if (!(VELOS.wait)) {
loadingPanel(); 
VELOS.wait.show();
}else {
VELOS.wait.show();
} 
}
function hidePanel()
{

if ((VELOS.wait)) {
VELOS.wait.hide();
}
}