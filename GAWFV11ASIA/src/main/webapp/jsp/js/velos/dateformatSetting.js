
/* Date format and separator
  default is MM/dd/yyyy
*/

//var calDateFormat = "dd/MM/yyyy";

var calDateFormat = "MM/dd/yyyy";
var calDateFormatSeparator = "/"; 


/* Specify indivisual positions for month, day and year components*/
var calMonthPos = 2;
var calDayPos = 3;
var calYearPos = 1;


