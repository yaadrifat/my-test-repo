var APPMODE_SAVE ="SAVE";
var APPMODE_ADD="ADD";
var APPMODE_DELETE="DELETE";
var APPMODE_READ="READ";
var errMsgNoRights ='Error: User does not have rights to ';



function checkAppRights(rights,mode){
    if(mode == APPMODE_READ && rights.indexOf("R") == -1 ){
			alert(errMsgNoRights + mode);
			return false;      		
    }else if(mode == APPMODE_ADD && rights.indexOf("A") == -1 ){
			alert(errMsgNoRights + mode);
			return false;      		
    }else if(mode == APPMODE_SAVE && rights.indexOf("M") == -1 ){
			alert(errMsgNoRights + mode);
			return false;      		
    }else if(mode == APPMODE_DELETE && rights.indexOf("D") == -1 ){
			alert(errMsgNoRights + mode);
			return false;      		
    }
   
    return true;
}


//JavaScript Document
//code inside this will happen after page is loaded  
$j(document).ready(function(){     
	
	$j('#header').load('html/inc_header.html');
	$j('#breadcrumb').load('html/inc_breadcrumb.html');
	
	$j('#footer').load('html/inc_footer.html');
	
});

function showAddWidgetModal(url)
{
	var position = $j("#widgetId").position();
	 $j.ajax({
	        type: "POST",
	        url: url,
	        async:false,
	        success: function (result, status, error){
	        	
	        	$j("#modelPopup1").html(result);
	        	$j("#modelPopup1").dialog(
	     			   {autoOpen: true,
	     				title: 'Add Widgets',
	     				position: [position.left,(position.top+25)],
	    				modal: true, width:500, height:240,
	     				close: function() {
	     					//$j(".ui-dialog-content").html("");
	     					//jQuery("#subeditpop").attr("id","subeditpop_old");
	     	        		jQuery("#modelPopup1").dialog("destroy");
	     			    }
	     			   }
	     			  );        	
	        },
	        error: function (request, status, error) {
	        	alert("Error " + error);
	            alert(request.responseText);
	        }

		});
}
function closeModal(){
	 $j("#modelPopup1").dialog("close");
	 $j("#modelPopup").dialog("close");
	 $j("#modelPopup1").dialog("destroy");
	 $j("#modelPopup").dialog("destroy");
}
function exportData(tableId){
	// alert("export -> " + tableId);
	 $j('#'+tableId).table2CSV({
		separator : '\t'
		});
}

function showData(data){
	//alert("showData")
	//alert("data " + data);
	//window.location='data:application/vnd.ms-excel;charset=utf8,' + encodeURIComponent(data) + ', #Content-Disposition:filename=export.xls,';	
	
	window.location='data:application/vnd.ms-excel;charset=utf8;Content-Type=application/vnd.ms-excel;Content-Disposition:attachment;filename=export.xls,' + encodeURIComponent(data) ;
 return true;
}

function trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}

function clearFields(){
	$j('#username').val("");
	$j('#password').val("");
	$j("#errormessagetd").html("");
}
function updateBreadcrumb(dispname,link ){
	//alert("update bread");
	if( $j('#breadcrumblink').text().indexOf( dispname ) == -1 ){

		$j('#breadcrumblink').append("&nbsp;&gt;&gt; <a  href='#' onclick='"+link+"';>" +dispname +"</a>");
	}
}
 
function changeBreadcrumb(pagename){
	var breadcrumbVal = '&nbsp;&nbsp;<a href="#" onclick="javascript:showHighlight(\'mnuHome\');checkLogin();" >Home </a>';
	if(pagename=="volunteer"){
		showHighlight('mnuVol');
		breadcrumbVal += '&nbsp;&nbsp;&gt;&gt;&nbsp;<a href="#" >New Volunteer</a>&nbsp;';
		$j('#breadcrumblink').html(breadcrumbVal);
	}else if(pagename=="respondent"){
		showHighlight('mnuResp');
		breadcrumbVal += '&nbsp;&nbsp;&gt;&gt;&nbsp;<a href="#" onclick="loadPage(\'getRespondents\');">Respondents</a>&nbsp;';
		breadcrumbVal += '&nbsp;&nbsp;&gt;&gt;&nbsp;<a href="#" >Edit Respondent</a>&nbsp;';
		$j('#breadcrumblink').html(breadcrumbVal);
	}else if(pagename=="editVolunteer"){
		showHighlight('mnuVol');
		breadcrumbVal += '&nbsp;&nbsp;&gt;&gt;&nbsp;<a href="#" onclick="loadPage(\'getVolunteerlst\');">Volunteers</a>&nbsp;';
		breadcrumbVal += '&nbsp;&nbsp;&gt;&gt;&nbsp;<a href="#" >Edit Volunteer</a>&nbsp;';
		$j('#breadcrumblink').html(breadcrumbVal);
	}else if(pagename=="Volunteers"){
		showHighlight('mnuVol');
		breadcrumbVal += '&nbsp;&nbsp;&gt;&gt;&nbsp;<a href="#" onclick="loadPage(\'getVolunteerlst\');">Volunteers</a>&nbsp;';		
		$j('#breadcrumblink').html(breadcrumbVal);
	}
	
	
}



function updateDisplayName(type, name){
	//alert("update user sel");
	$j("#pageType").val(type);
	$j("#pageName").val(name);
	var userSelection = type + " : " + name;
	$j('#userSelection').html(userSelection);
}


function getHome(){
	$j('#header').load('jsp/cb_inc_blankheader.jsp');
	
	$j('#header').removeClass('headerStyle').addClass('blankheader');
	$j('#nav').css('height','32px');
	$j('#nav').load('jsp/cb_inc_nav.jsp');
	$j('#breadcrumb').load('html/inc_breadcrumb.html');
	$j('#menu').removeClass('hidden').addClass('visible');
	
	$j('#breadcrumb').removeClass('hidden').addClass('visible');
	loadPage('home');
	$j('.jclock').jclock();
}
function forgotpassword(){
	$j("#errormesssagetd").html("");
	var data = "forgotuserName="+$j("#forgotuserName").val();+"&email="+$j("#email").val();
	//alert("context path " + contextpath);
	$j.ajax({
		type: "POST",
		url : "getforgotpassword",
		data : $j("#forgotform").serialize(),
		success : function(data) {
			$j("#errormesssagetd").html(data.errorMessage);
		},
		error:function() { 
			alert("Error ");
		}
		
	});
	return false;
}
function checkLogin(){
	//alert("check login");
	if($j('#username').val()!= "" && $j('#password').val()!=""){
		//alert(" inside if");
		var errorMessage = "";
		var errorMessage = validateUserDetails("getLoginDetails?userName="+$j('#username').val()+"&password="+$j('#password').val());
		if(errorMessage != "" && errorMessage!=null)
		{
		alert(errorMessage);
		}
		if(errorMessage == "" || errorMessage==null){  
			$j('#header').load('jsp/cb_inc_blankheader.jsp');
	
			$j('#header').removeClass('headerStyle').addClass('blankheader');
			$j('#nav').css('height','32px');
			$j('#nav').load('jsp/cb_inc_nav.jsp');
			$j('#breadcrumb').load('html/inc_breadcrumb.html');
			$j('#menu').removeClass('hidden').addClass('visible');
			
			$j('#breadcrumb').removeClass('hidden').addClass('visible');
			loadPage('home');
			
			//$('#main').html(" ");
			
			//$('#footer').load('html/inc_footer.html');
			//alert("wait");
			 $j('.jclock').jclock();
		}else{
			$j("#errormessagetd").html(errorMessage);
			return false;
		}	
	}
	 $j('#fimg').removeClass('hidden').addClass('visible');
	return true;
	//alert(" end of check ");
}
 
function validateUserDetails(url){
	//alert("validateUserDetails ");
	var errorMessage = "";
	 $j.ajax({
	        type: "POST",
	        url: url,
	        async:false,
	        success: function (result){
	        	//alert(result);
	        	//alert(result.errorMessage);
	        	errorMessage = result.errorMessage;
	        },
	        error: function (request, status, error) {
	        	alert("Error " + error);
	            alert(request.responseText);
	        }

		});
	 return errorMessage;
}
function showPage(frmName){
	var pageName = "jsp/" + frmName;
	$j('.ui-dialog').html("");
	$j('.ui-datepicker').html("");
	$j('.tabledisplay').html("");
	$j('#main').html("");
	$j('.tabledisplay').html("");
	$j('#main').load(pageName);
}

function showModal(title,url)
{

	$j.ajax({
        type: "POST",
        url: url,
        async:false,
        success: function (result, status, error){
        	
        	$j("#modelPopup1").html(result);
        	$j("#modelPopup1").dialog(
     			   {autoOpen: true,
     				title: title  ,
     				modal: true, width:800, height:700,
     				close: function() {
     					//$(".ui-dialog-content").html("");
     					//jQuery("#subeditpop").attr("id","subeditpop_old");
     	        		jQuery("#modelPopup1").dialog("destroy");
     			    }
     			   }
     			  );        	
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }

	});
}

function loadPage(url){  
	   // alert(" url " + url); 
	 $j.ajax({
	        type: "POST",
	        url: url,
	        async:false,
	        success: function (result){
	        	//alert("test" + result);
	        	//showPage(pageName);
	        	//$(".ui-dialog-content").html("");
	        	$j('.ui-dialog').html("");
	        	//$('.ui-datepicker').html("");
	        	$j('.tabledisplay').remove();
	        	$j('#main').html(result);
	        	valid8();
	        	
	        },
	        error: function (request, status, error) {
	        	alert("Error " + error);
	            alert(request.responseText);
	        }

		});
}
// save Modal form

function modalFormSubmit(url,formId)
{
	$j.ajax({
        type: "POST",
        url: url,
        async:false,
        data : $j("#"+formId).serialize(),
        success: function (result){
        	$j('.ui-datepicker').html("");
        	//$('.tabledisplay').html("");
            var $response=$j(result);
            //query the jq object for the values
            var errorpage = $response.find('#errorForm').html();
           // alert(oneval);
           // var subval = $response.find('#sub').text();
            if(errorpage != null &&  errorpage != "" ){
            	$j('#main').html(result);
            }else{
            	$j("#modelPopup1").html(result);
            }
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }

	});
}


//Load GET Request by Mohiuddin

function loadPageByGetRequset(url,divname){  
	   // alert(" url " + url); 
	var data = '';
	var index = url.indexOf('?');
    if(index!=-1)
    {
         data = url.substring(index+1);
         url = url.substring(0,index);
    }
     $j.ajax({
	        type: "GET",
	        url: url,
	        data:data,
	        async:false,
	        success: function (result){
	        	//alert("test" + result);
	        	//showPage(pageName);
	        	//$(".ui-dialog-content").html("");
		    	 $j('.ui-datepicker').html("");
		     	//$('.tabledisplay').html("");
		         var $response=$j(result);
		         //query the jq object for the values
		         var errorpage = $response.find('#errorForm').html();
		        // alert(oneval);
		        // var subval = $response.find('#sub').text();
		         if(errorpage != null &&  errorpage != "" ){
		         	$j('#main').html(result);
		         }else{
		         	$j("#"+divname).html(result);
		       }
	        	
	        },
	        error: function (request, status, error) {
	        	alert("Error " + error);
	            alert(request.responseText);
	        }

		});
}

//Load Post Form by Mohiuddin
function loadDivWithFormSubmit(url,divname,formId){
	$j.ajax({
        type: "POST",
        url: url,
        async:false,
        data : $j("#"+formId).serialize(),
        success: function (result){
        	$j('.ui-datepicker').html("");
        	//$j('.tabledisplay').html("");
            var $response=$j(result);
            //query the jq object for the values
            var errorpage = $response.find('#errorForm').html();
           // alert(oneval);
           // var subval = $response.find('#sub').text();
            if(errorpage != null &&  errorpage != "" ){
            	$j('#main').html(result);
            }else{
            	$j("#"+divname).html(result);
            }
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }

	});
	

}

//Load Particular div
function loaddiv(url,divname){  
	 $j.ajax({
	        type: "POST",
	        url: url,
	        async:false,
	        success: function (result){
	        	//$('.ui-dialog').html("");
	        	//$('.ui-datepicker').html("");
	        	var $response=$j(result);
	        	var errorpage = $response.find('#errorForm').html();
	        	if(errorpage != null &&  errorpage != "" ){
	            	$j('#main').html(result);
	            }else{
	            	$j("#"+divname).replaceWith($j('#'+divname, $j(result)));
	            }
	        },
	        error: function (request, status, error) {
	        	alert("Error " + error);
	            alert(request.responseText);
	        }

		});
}
//load more div
function loadmultiplediv(url,divname){  
	var divnamearr = divname.split(",");
	var newdivname = "";
	 $j.ajax({
	        type: "POST",
	        url: url,
	        async:false,
	        success: function (result){
	        	//$j('.ui-dialog').html("");
	        	//$('.ui-datepicker').html("");
	        	var $response=$j(result);
	        	var errorpage = $response.find('#errorForm').html();
	        	if(errorpage != null &&  errorpage != "" ){
	            	$j('#main').html(result);
	            }else{ 
		        	for(i=0;i<divnamearr.length;i++) {
		        		newdivname = divnamearr[i];
		        		$j("#"+newdivname).replaceWith($j('#'+newdivname, $j(result)));
		        	}
	            }
	        },
	        error: function (request, status, error) {
	        	alert("Error " + error);
	            alert(request.responseText);
	        }

		});
}

function refreshDiv(url,divname,formId){
	$j.ajax({
        type: "POST",
        url: url,
        async:false,
        data : $j("#"+formId).serialize(),
        success: function (result){
        	$j('.ui-datepicker').html("");
        	//$('.tabledisplay').html("");
            var $response=$j(result);
            //query the jq object for the values
            var errorpage = $response.find('#errorForm').html();
           // alert(oneval);
           // var subval = $response.find('#sub').text();
            if(errorpage != null &&  errorpage != "" ){
            	$j('#main').html(result);
            }else{
            	$j("#"+divname).replaceWith($j('#'+divname, $j(result)));
            	constructTable();
            }
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }

	});
	

}


/*window.onload=function(){
var formref=document.getElementById("switchform")
indicateSelected(formref.choice)
}*/


function showCallQueue(){
	//alert("showCallQueue");
	$j('#tdshowid').html($j('#callQueue').html())
}
function showResQueue(){
	$j('#tdshowid').html($j('#tblresp').html())
	
}

function showusecase(){
	if(typeof(document.getElementById("formname"))!=undefined){
		var frmName = document.getElementById("formname").value;
		if(frmName != "" ){
			url = "images/" + frmName +"_usecase.jpg";
			
			window.open(url);	
		}
	}else{
		alert("To Be Developed");	
	}
}

function showwireframe(){
	if(typeof(document.getElementById("formname"))!=undefined){
		var frmName = document.getElementById("formname").value;
		if(frmName != "" ){
			url = "images/" + frmName +"_wireframe.jpg";
			window.open(url);	
		}
	}else{
		alert("To Be Developed");	
	}
}

function showassdoc(){
	if(typeof(document.getElementById("formname"))!=undefined){
		var frmName = document.getElementById("formname").value;
		if(frmName != "" ){
			url = "images/" + frmName +"_assdoc.jpg";
			window.open(url);	
		}
	}else{
		alert("To Be Developed");	
	}
}

function showhelp(){
	alert("To Be Developed");	
}

function showdetails(){
	document.getElementById("editrespondent").style.visibility="visible";
	}
	
function showcontact() {
	document.getElementById("contactactivity").style.visibility="visible";
	document.getElementById("recruitactivity").style.visibility="hidden";
	}
	
	
function showrecruit() {
	document.getElementById("contactactivity").style.visibility="hidden";
	document.getElementById("recruitactivity").style.visibility="visible";
	
	}

function showsearchValues(){
	if(document.getElementById("searchVal").innerHTML == ""){
		document.getElementById("searchVal").innerHTML = 	'<a href="#" onclick="removeSearch()" >Remove All</a>' ;
	}
	document.getElementById("searchVal").innerHTML = document.getElementById("searchVal").innerHTML  + '<a href="#">' + document.getElementById("searchType").value +":" + 	document.getElementById("name").value + '<br /></a>';
	
}

function removeSearch(){
	document.getElementById("searchVal").innerHTML = "";
}

function showMedications(){
	document.getElementById("listMedications").style.display="block";
}

function addMoreFiles(id){

	$j("#"+id).find("tr .add").each(function(){
         $j(this).after("<tr ><td> <s:text name='garuda.unitreport.label.selectyourfile' /></td>"
 				+"<td><s:file name='fileUpload' /><span id='span1' style='color: red;display: none;'><strong>Uploaded file - <s:property value='#request.path' />"
				+"</strong></span></td></tr>");
		});		
}
