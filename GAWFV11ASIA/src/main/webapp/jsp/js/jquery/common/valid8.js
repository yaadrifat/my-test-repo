function valid8(){   
	//alert(" valid 8 1ready");
	var formName = $j('#frmName').val();
	//alert(" fromName " + formName);
	if( formName != undefined){
	var xmlData = readXml();
	//alert(" xml " + $j(xmlData).text());
	validate(xmlData,formName);
	}
	
}   

function validate(xmlData,formName){
	//alert("validate ");
	//alert("form " + formName);
	$j(xmlData).find("form").each(function (){
		if(formName == $j(this).find("formname").text()){
			if($j(this).find("formvalidate").text()=="true" ){
				var fields = $j(this).find("field");
				validateRules(fields);
			}
			
		}
	});
	
}







function validateRules(fields){
	//alert(" validate rules" )
	//alert($j(fields).text());
	$j(fields).each(function (){
	//	alert($(this).text());
		var fieldName = $j(this).find("fieldname").text();
		//alert("fieldName " + fieldName);
		$j(this).find("rule").each(function (){
				bindrule(fieldName,$j(this));
		});
	});
	
}

function bindrule(fieldName,rule){
	//alert(" bind rule")
	//alert($(rule).find("validate").text() );
	if($j(rule).find("validate").text() == "true"){
	//	alert("inside if trace1");
	//	alert(fieldName +  " method " + $j(rule).find("event").text());
		//$('#'+fieldName).bind(''+$(rule).find("event").text(),[fieldName,$(rule).find("message").text()],false);
	
		//$('#'+fieldName).bind($(rule).find("event").text(),['test field','test msg'],$(rule).find("methodname").text());
			
		
		$j('#'+fieldName).bind($j(rule).find("event").text() , function (){
		
			
		if($j(rule).find("ruletype").text() =="script"){
					$j(document).trigger($j(rule).find("methodname").text(),[fieldName,$j(rule).find("message").text()],false);
		}else{
		//	 var jsonObj = {};

			var params = "" ;
			$j(rule).find("param").each(function(){
				params +=  $j(this).find("paramname").text() + ":" + $j(this).find("paramvalue").text() + ",";
		//		jsonObj.push({$j(this).find("paramname").text(),$(this).find("paramvalue").text()});
			});
			params+="value:'"+ $j('#'+fieldName).val()+"'";
		//	alert("params" + params);
		//	alert( " json " + jsonObj); 
			$j(document).trigger($j(rule).find("methodname").text(),[fieldName,params,$j(rule).find("message").text()],false);
		//	$(document).trigger($(rule).find("methodname").text(),[jsonObj],false);
		}
			
			//if($(rule).find("ruletype").text() !="ajax"){
			//	alert("method " + $(rule).find("methodname").text());
			//	$('#'+fieldName).trigger($(rule).find("methodname").text(),[fieldName,$(rule).find("message").text()]);
			//}
			
		});
		
	}
}


function readXml(fileName){
	// alert(fileName);
	var resXml = "";
	
	var defFileName = "valid8.xml";
	if(fileName != null && fileName != ""){
		defFileName = fileName;
	}
	 var fileurl = "../xml/"+defFileName;
	
	// alert("fileurl " + fileurl);
	 $.ajaxSetup({
         url: fileurl
      });
	 $.ajax({
	        type: "GET",
	      
	        dataType: "xml",
	        async:false,
	        cache:false,
	        success: function(xml){
	        	resXml = xml;
	        },
			complete: function( xhr, status ){
				//alert("status "  +status);
				if( status == 'parsererror' ){
					xmlDoc = null;
					if (document.implementation.createDocument){
						// code for Firefox, Mozilla, Opera, etc.
						var xmlhttp = new window.XMLHttpRequest();
						xmlhttp.open("GET", fileurl, false);
						xmlhttp.send(null);
						xmlDoc = xmlhttp.responseXML.documentElement;
					}else  if (window.ActiveXObject){
						
					  xmlDoc=new ActiveXObject( "Microsoft.XMLDOM" ) ;
					  xmlDoc.async = "false" ;
					  xmlDoc.loadXML( xhr.responseText ) ;
					}  else if( window.DOMParser ) {
						// others
					  parser=new DOMParser();
					  xmlDoc=parser.parseFromString( xhr.responseText,"text/xml" ) ;
					}else{
						// 
						alert('Your browser cannot handle this script');
					} 
						resXml= xmlDoc ;				
				  }
				
			},
			error: function(xmlhttp, status, error){
	 			alert("Err" + error); 
			}
	 });
	// alert("resXml " + resXml);
	 return resXml;
}
