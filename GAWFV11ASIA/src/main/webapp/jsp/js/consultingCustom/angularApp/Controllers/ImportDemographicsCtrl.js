/**
 * @ngdoc controller
 * @name myApp:ImportDemographicsCtrl
 *
 * @description
 *
 *
 * @requires $scope
 * */
angular.module('myApp')
    .controller('ImportDemographicsCtrl', ['$scope', '$http',
        '$location',
        '$interval', '$q',
        '$templateCache',
        function ($scope, $http, $location, $interval, $q, $templateCache) {

            //Get list of gender items from code list
            var GetGenderCodes = function(){
                var req = {
                    method: 'GET',
                    url: 'https://bizdev3.veloseresearch.com:55555/systemAdminSEI/getGenderList',
                    headers: {
                        'Content-Type': 'application/json;charset=UTF-8'
                    }
                };

                $http(req).then(function (response) { //success
                    $scope.soapStatus1 = response.data.codes;
                    $scope.gridOptions.columnDefs[6].editDropdownOptionsArray = response.data.codes;
                    $scope.genderList = response.data.codes;
                }, function (response) { //error
                    $scope.soapStatus = response.data;
                });
            };

            //Call function to populate dropdown values
            GetGenderCodes();


            $scope.data = [];
            $scope.gridOptions = {
                enableGridMenu: true,
                enableFiltering: true,
                enableSorting: true,
                enableCellEditOnFocus: true,
                importerDataAddCallback: function (grid, newObjects) {
                    $scope.data = $scope.data.concat(newObjects);
                },
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    gridApi.rowEdit.on.saveRow($scope, $scope.saveRow);
                },
                columnDefs: [
                    {field: 'PatientId', name:'Patient Id'},
                    {field:'Organization', name: 'Org Name'},
                    {field: 'DOB', name: 'Date Of Birth', type: 'date', cellFilter: 'date:"yyyy-MM-dd"' },
                    {field:'FirstName', name: 'First Name', cellTooltip:
                        function( row, col ) {
                            return 'Name: ' + row.entity.FirstName + ' Company: ' + row.entity.Organization;
                        } },
                    {field:'LastName',name:'Last Name'},
                    {field:'MiddleName',name:'Middle Name'},
                    {name:'Gender',
                    //cellTemplate: '<h6><div class="label label-info">{{ COL_FIELD }}</div></h6>',
                        editableCellTemplate: 'ui-grid/dropdownEditor',
                        //Setting the options list from the getGenderList function
                        editDropdownIdLabel: 'code',
                        editDropdownValueLabel: 'description'
                    },
                    {field:'EMail',name:'E-Mail',
                        cellTemplate: '<div class="ui-grid-cell-contents"><a href="mailto:{{ COL_FIELD }}">{{COL_FIELD}}</a></div>'},
                    {field:'Address1',name:'Address Line 1'},
                    {field:'Address2',name:'Address Line 2'},
                    {field:'City',name:'City'},
                    {field:'Zip',name:'Zip Code'},
                    {field:'HomePhone',name:'Home Phone'},
                    {field:'WorkPhone',name:'Work Phone'}
                ],
                data: 'data'
            };

            //Clear the Ui- Gird of all the data
            $scope.ClearData = function () {
                $scope.soapStatus =$scope.gridOptions.data;
                $scope.gridOptions.rowEditWaitInterval = $scope.gridOptions.rowEditWaitInterval * -1 ;
                $scope.soapStatus =$scope.gridOptions.data;

                //$scope.gridOptions.columnDefs = [];
            };

            $scope.TiggerSaveData = function () {
                $scope.gridApi.rowEdit.flushDirtyRows($scope.gridApi.grid);
            };

            /*
             *
             * Function to convert the gender value from the spreadsheet to
             * the complex type object in the code list.
             *
             * */
            var getGender = function (gender) {
                for (var i = 0, len = $scope.genderList.length; i < len; i++) {
                    if ($scope.genderList[i].code === gender) {
                        return $scope.genderList[i];
                    }
                }
            };

            /*
            * Update patient demographics data
            *  Can be expanded to handle update or create.
            *  API has a create patient option that we can leverage
            *  -(create not implemented as of 10/21/16)
            * */
            $scope.saveRow = function (rowEntity) {
                $scope.soapStatus = "New row being saved....wait!";
                // create a fake promise - normally you'd use the promise returned by $http or $resource
                var promise = $scope.updatePatient(rowEntity);
                $scope.gridApi.rowEdit.setSavePromise(rowEntity, promise.promise);

                /*// fake a delay of 3 seconds whilst the save occurs, return error if gender is "male"
                 $interval(function () {
                 if (rowEntity.Gender === 'male') {
                 promise.reject();
                 } else {
                 promise.resolve();
                 }
                 }, 3000, 1);*/
            };

            /* Code to update the Patient Demographics using SOAP */
            $scope.updatePatient = function (rowEntity) {

                //Convert Gender to gender code type
                var gender = getGender(rowEntity.Gender);

                // <%--Angular REST request--%>
                // <%--Create post request--%>
                var req = {
                    method: 'POST',
                    url: 'https://bizdev3.veloseresearch.com:55555/PatientDemographics/updatePatient',
                    headers: {
                        'Content-Type': 'application/json;charset=UTF-8'
                    },
                    data: {
                        PatientIdentifier: {
                            patientId: rowEntity.PatientId,
                            organizationId: {
                                siteName: rowEntity.Organization
                            }
                        },
                        PatientDemographics: {
                            address1: rowEntity.Address1,
                            address2: rowEntity.Address2,
                            city: rowEntity.City,
                            zipCode: rowEntity.Zip,
                            workPhone: rowEntity.WorkPhone,
                            homePhone: rowEntity.HomePhone,
                            dateOfBirth: rowEntity.DOB,
                            firstName: rowEntity.FirstName,
                            middleName: rowEntity.MiddleName,
                            lastName: rowEntity.LastName,
                            eMail: rowEntity.EMail,
                            gender: gender,
                            // {
                            //     code: rowEntity.Gender.code,
                            //     description: rowEntity.Gender.description,
                            //     type: rowEntity.Gender.type
                            // },
                            reasonForChange: "Some reason to keep FDA Happy."
                        }
                    }
                };

                var soapReply = $q.defer();

                $http(req).then(function (response) { //success
                    var soapData = response.data;
                    try
                    {
                        if (soapData.ResponseHolder.issues === null) {
                            $scope.soapStatus = soapData;
                            return soapReply.resolve();
                        }
                        else
                        {
                            $scope.soapStatus = soapData;
                            return soapReply.reject()
                        }
                    }
                    catch($scope){
                        $scope.soapStatus = soapData;
                        return soapReply.reject()
                    }
                }, function (response) { //error
                    $scope.soapStatus = response.data;
                    soapReply.reject();
                });

                return soapReply;
            };


            /*Load CSV flies into the GRID*/
            var handleFileSelect = function (event) {
                var target = event.srcElement || event.target;

                if (target && target.files && target.files.length === 1) {
                    var fileObject = target.files[0];
                    $scope.gridApi.importer.importFile(fileObject);
                    target.form.reset();
                }
            };

            var fileChooser = document.querySelectorAll('.file-chooser');

            if (fileChooser.length !== 1) {
                console.log('Found > 1 or < 1 file choosers within the menu item, error, cannot continue');
            } else {
                fileChooser[0].addEventListener('change', handleFileSelect, false);  // TODO: why the false on the end?  Google
            }
        }]);

