/**
 * @ngdoc controller
 * @name myApp:StudyFinancialCtrl
 *
 * @description
 *
 *
 * @requires $scope
 * */
angular.module('myApp')
    .controller('StudyFinancialCtrl', ['$scope', '$http',
        '$location',
        '$interval', '$q',
        '$templateCache',
        'uiGridGroupingConstants',
        'uiGridConstants',
        function ($scope, $http, $location, $interval, $q, $templateCache, uiGridGroupingConstants,uiGridConstants) {
            $scope.viewVisitLineItems = false;

            $scope.gridOptions = {
                enableFiltering: true,
                enableGridMenu: true,
                enableSorting: true,
                enableColumnResizing: true,
                //treeRowHeaderAlwaysVisible: false,
                columnDefs: [
                    {
                        name: 'Sections', field: 'Section',
                        //width: '20%',
                        minWidth: 100,
                        resizable: true,
                        grouping: {groupPriority: 0},
                        sort: {priority: 1, direction: 'asc'}
                    },
                    {
                        name: 'CostType', field: 'PK_CODELST_COST_TYPE.description',
                        //width: '20%',
                        minWidth: 100,
                        resizable: true,
                        grouping: {groupPriority: 1}, sort: {priority: 1, direction: 'desc'}
                    },
                    {
                        name: 'Event Name', field: 'lineItemIdent.lineItemName',
                        //width: '20%',
                        minWidth: 100,
                        resizable: true
                    },
                    {
                        name: 'Category', field: 'PK_CODELST_CATEGORY.description',
                        //width: '20%',
                        minWidth: 100,
                        resizable: true
                    },
                    {
                        name: 'Unit Cost', field: 'LINEITEM_OTHERCOST',
                        //width: '20%',
                        minWidth: 100,
                        resizable: true
                    },
                    {
                        name: 'Total Cost/Patient',
                        //width: '20%',
                        minWidth: 100,
                        resizable: true
                    }, //???Not sure where the data is for this
                    {
                        name: 'Cost/All Patients', field: 'LINEITEM_OTHERCOST',
                        //width: '20%',
                        minWidth: 100,
                        resizable: true,
                        treeAggregationType: uiGridGroupingConstants.aggregation.SUM
                    },
                    {
                        name: 'Sponsor Amount', field: 'LINEITEM_SPONSORUNIT',
                        //width: '20%',
                        minWidth: 100,
                        resizable: true,
                        treeAggregationType: uiGridGroupingConstants.aggregation.SUM
                    },
                    {
                        name: 'Variance', field: 'Variance',
                        //width: '20%',
                        minWidth: 100,
                        resizable: true,
                        treeAggregationType: uiGridGroupingConstants.aggregation.SUM
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                }
            };

            //Define variable for dynamic col list
            $scope.calViewColumns = [];

            //Calendar View Grid View
            $scope.calendarViewGridOptions = {
                //Turn on filtering for all columns
                enableFiltering: true,
                //Burger menu on top right of the page
                enableGridMenu: true,
                //Column sorting
                enableSorting: true,
                //well...pretty self explanatory
                enableColumnResizing: true,
                //Set row height if template is not default
                rowHeight: 60,

                //Pagination options
                paginationPageSizes: [2, 5, 10],
                paginationPageSize: 10,

                //Exporter options
                enableSelectAll: true,
                exporterCsvFilename: 'myFile.csv',
                exporterPdfDefaultStyle: {fontSize: 9},
                exporterPdfTableStyle: {margin: [30, 30, 30, 30]},
                exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: true, color: 'red'},
                exporterPdfHeader: {text: "My Header", style: 'headerStyle'},
                exporterPdfFooter: function (currentPage, pageCount) {
                    return {text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle'};
                },
                exporterPdfCustomFormatter: function (docDefinition) {
                    docDefinition.styles.headerStyle = {fontSize: 22, bold: true};
                    docDefinition.styles.footerStyle = {fontSize: 10, bold: true};
                    return docDefinition;
                },
                exporterPdfOrientation: 'portrait',
                exporterPdfPageSize: 'LETTER',
                exporterPdfMaxGridWidth: 500,
                exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
                /*End of exported options*/

                //treeRowHeaderAlwaysVisible: false,

                /*Column Definitions - This is just a place holder to show something
                 * on page load. I am dynamically creating the columns using code once the
                 * data is obtained from the web service on the fly.*/
                columnDefs: [
                    {
                        name: 'Sections', field: 'Section',
                        //width: '20%',
                        minWidth: 100,
                        resizable: true,
                        grouping: {groupPriority: 0},
                        sort: {priority: 1, direction: 'asc'}
                    }],
                onRegisterApi: function (gridApi) {
                    $scope.calendarViewGridApi = gridApi;
                }
            };

            //Summary total variable
            $scope.researchInternal = 1.1;
            $scope.researchSponsor = 21.10;
            $scope.researchVariance = 0.0;
            $scope.socInternal = 0.0;
            $scope.socSponsor = 0.0;
            $scope.socVariance = 0.0;
            $scope.otherInternal = 0.0;
            $scope.otherSponsor = 0.0;
            $scope.otherVariance = 0.0;
            $scope.indirectsPercentage = 12;


            //Chart object
            //Draw chart
            $scope.patientCosts = {
                "type": "PieChart",
                "data": [
                    ["CostType", "Cost"]
                ],
                "options": {
                    "displayExactValues": true,
                    "width": 400,
                    "height": 200,
                    "is3D": true,
                    "title": "Total Cost/Patient",
                    "chartArea": {
                        "left": 10,
                        "top": 10,
                        "bottom": 0,
                        "height": "100%"
                    }
                },
                "formatters": {
                    "number": [
                        {
                            "columnNum": 1,
                            "pattern": "$ #,##0.00"
                        }
                    ]
                }
            };


            var drawDiffChart = function () {
                $scope.costTotalDiff_dt =
                {cols: [
                    {id: "t", label: "Cost Type", type: "string"},
                    {id: "s", label: "Cost Sponsor", type: "number", p: {role: "old-data"}},
                    {id: "s", lable: "Cost Internal", type: "number"}
                ],
                    rows: [
                        {
                            c:[{v:"Research"},{v:$scope.researchSponsor},{v:$scope.researchInternal}]
                        },
                        {
                            c:[{v:"SOC"},{v:$scope.socSponsor},{v:$scope.socInternal}]
                        },
                        {
                            c:[{v:"Other"},{v:$scope.otherSponsor},{v:$scope.otherInternal}]
                        }
                    ]};

                $scope.costTotalDiff = {
                    "type": "PieChart",
                    "data": $scope.costTotalDiff_dt,
                    "options": {
                        "displayExactValues": true,
                        "width": 400,
                        "height": 200,
                        "is3D": true,
                        "title": "Total Cost Interval vs. Sponsor",
                        "chartArea": {
                            "left": 10,
                            "top": 10,
                            "bottom": 0,
                            "height": "100%"
                        }
                    },
                    "formatters": {
                        "number": [
                            {
                                "columnNum": 1,
                                "pattern": "$ #,##0.00"
                            }
                        ]
                    }
                };
            };




            // Get BudgetData
            var GetStudyBudget = function () {

                // var calOid = $location.search().hasOwnProperty( 'CAL_OID' )? $location.search()['CAL_OID'] : "6cda02b9-4e7c-4f2b-ae7d-0bc4df08235f";
                var req = {
                    method: 'POST',
                    url: 'https://bizdev3.veloseresearch.com:55555/StudyCalendarBudget',
                    headers: {
                        'Content-Type': 'application/json;charset=UTF-8'
                    },
                    data: {
                        StudyIdentifier: {OID: $location.search()['S_OID']},
                        CalendarNameIdentifier: {
                            OID: $location.search()['CAL_OID']
                        }
                    }
                }

                $http(req).then(function (response) { //success
                    $scope.response = response;
                    //Get the grid setup for budget view
                    GetBudgetViewGrid(response);
                    //Get the grid setup for Calendar view
                    GetCalViewGrid(response);
                    //Get Budget Totals
                    GetBudgetTotals(response);

                }, function (response) { //error
                    $scope.soapStatus = response.data;
                });
            };

            //Get Study Details
            var GetStudyDetails = function (studyId) {
                var req = {
                    method: 'POST',
                    url: 'https://bizdev3.veloseresearch.com:55555/study',
                    headers: {
                        'Content-Type': 'application/json;charset=UTF-8'
                    },
                    data: {
                        StudyIdentifier: {OID: $location.search()['S_OID']},
                        CalendarNameIdentifier: {
                            OID: $location.search()['CAL_OID']
                        }
                    }
                }

                $http(req).then(function (response) { //success
                    $scope.response = response;
                    //Get the grid setup for budget view
                    GetBudgetViewGrid(response);
                    //Get the grid setup for Calendar view
                    GetCalViewGrid(response);
                    //Get Budget Totals
                    GetBudgetTotals(response);

                }, function (response) { //error
                    $scope.soapStatus = response.data;
                });

            };


            //Update Grid
            $scope.UpdateGrid = function () {
                GetBudgetViewGrid($scope.response);
                $scope.gridApi.core.refresh();
            };

            var GetBudgetTotals = function (response) {
                var budgetSections = response.data.Budget.budgetInfo.budgetCalendars.budgetCalendar;
                angular.forEach(budgetSections, function (calendar, calKey) {
                    /*Check if the budget section has a calendar name
                     looks like there is another section that seems to have the same non calendar*/
                    if (calendar.hasOwnProperty('calNameIdent')) {
                        angular.forEach(calendar.budgetSections.budgetSectionInfo, function (x, key) {
                            /*Try catch because some sections dont have lineitems and if that is the case there
                             is a missing xml tag so looking for it will cause an error*/
                            try {
                                /*We only want to display the non calendar items from the
                                 budget, so we only want sections that dont have visitIdent object.*/
                                if (x.hasOwnProperty('lineItems')) {
                                    var lineItems = x.lineItems.budgetLineItemInfo;

                                    angular.forEach(lineItems, function (y, key1) {
                                        var lineCost = parseFloat(y.LINEITEM_OTHERCOST);
                                        var lineSponsor = parseFloat(y.LINEITEM_SPONSORUNIT);

                                        //Calculate the Total Costs
                                        if (y.PK_CODELST_COST_TYPE.code == "res_cost") {
                                            $scope.researchInternal = $scope.researchInternal + lineCost;
                                            $scope.researchSponsor = $scope.researchSponsor + lineSponsor;
                                            $scope.researchVariance = $scope.researchVariance + (lineCost - lineSponsor);
                                        }
                                        else if (y.PK_CODELST_COST_TYPE.code == "stdcare_cost") {
                                            $scope.socInternal = $scope.socInternal + lineCost;
                                            $scope.socSponsor = $scope.socSponsor + lineSponsor;
                                            $scope.socVariance = $scope.socVariance + (lineCost - lineSponsor);
                                        }
                                        else {
                                            $scope.otherInternal = $scope.otherInternal + lineCost;
                                            $scope.otherSponsor = $scope.otherSponsor + lineSponsor;
                                            $scope.otherVariance = $scope.otherVariance + (lineCost - lineSponsor);
                                        }
                                    });
                                }
                            }
                            catch (e) {
                                $scope.soapStatus = "Error!!!";
                            }
                        });
                    }
                });

                //Populate the chart with the calculated values
                populateChart();
            };

            var populateChart = function(){
                //Data for Total cost/patient chart
                $scope.patientCosts.data.push(["Research", $scope.researchInternal+$scope.researchSponsor]);
                $scope.patientCosts.data.push(["SOC", $scope.socSponsor+$scope.socInternal]);
                $scope.patientCosts.data.push(["Other", $scope.otherSponsor+$scope.otherInternal]);

                //Draw diff chart
                drawDiffChart();

            };

            /*var drawChart = function () {
                // Define the chart to be drawn.
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Element');
                data.addColumn('number', 'Percentage');
                data.addRows([
                    ['Research', $scope.researchSponsor+$scope.researchInternal],
                    ['SOC', $scope.socInternal+$scope.socSponsor],
                    ['Other', $scope.otherInternal+$scope.otherSponsor],

                ]);

                // Instantiate and draw the chart.
                var chart = new google.visualization.PieChart(document.getElementById('myPieChart'));
                chart.draw(data, null);
            };*/


            var GetBudgetViewGrid = function (response) {
                $scope.AllLineItems = [];
                //$scope.calsoapStatus = response.data.Budget.budgetInfo;
                var budgetSections = response.data.Budget.budgetInfo.budgetCalendars.budgetCalendar;
                angular.forEach(budgetSections, function (calendar, calKey) {
                    /*Check if the budget section has a calendar name
                     looks like there is another section that seems to have the same non calendar*/
                    if (calendar.hasOwnProperty('calNameIdent')) {
                        angular.forEach(calendar.budgetSections.budgetSectionInfo, function (x, key) {
                            /*Try catch because some sections dont have lineitems and if that is the case there
                             is a missing xml tag so looking for it will cause an error*/
                            try {
                                /*We only want to display the non calendar items from the
                                 budget, so we only want sections that dont have visitIdent object.
                                 Comment out the condition if you want to display both visits and non
                                 visit items.*/
                                if (!x.hasOwnProperty('visitIdent') || $scope.viewVisitLineItems) {
                                    if (x.hasOwnProperty('lineItems')) {
                                        var lineItems = x.lineItems.budgetLineItemInfo;

                                        angular.forEach(lineItems, function (y, key1) {
                                            //Section Name
                                            y.Section = x.bgtSectionName.bgtSectionName;
                                            //Calculate Variance
                                            y.Variance = y.LINEITEM_OTHERCOST - y.LINEITEM_SPONSORUNIT;
                                            $scope.AllLineItems.push(y);
                                        });
                                    }
                                }

                            }
                            catch (e) {
                                $scope.soapStatus = "Error!!!";
                            }
                        });
                    }
                });

                //set the grid data source
                $scope.gridOptions.data = $scope.AllLineItems;

                //Get Budget name and version
                $scope.budgetName = response.data.Budget.budgetIdentifier.budgetName;
                $scope.budgetVersion = response.data.Budget.budgetIdentifier.budgetVersion;

                $scope.soapStatus = "Row count: " + $scope.gridOptions.data.length;
                //+ "data: " + $scope.gridOptions.data[30];
            };

            $scope.CalendarViewLineItems = [];
            var GetCalViewGrid = function (response) {
                //$scope.calsoapStatus = response.data;

                //Get calendar visit and assign columns
                //Create first column for event Names
                var column = {
                    name: 'Event',
                    field: 'Event',
                    //width: '20%',
                    minWidth: 100,
                    resizable: true,
                    headerCellTemplate: 'ui-grid/uiGridHeaderCell',
                    cellTemplate: '<div class="ui-grid-cell-contents">{{ COL_FIELD }}</div>'
                };
                $scope.calViewColumns.push(column);

                var budgetCalendar = response.data.Budget.budgetInfo.budgetCalendars.budgetCalendar;

                angular.forEach(budgetCalendar, function (budget, budgetKey) {
                    /*Check if the budget section has a calendar name
                     looks like there is another section that seems to have the same non calendar*/
                    if (budget.hasOwnProperty('calNameIdent')) {
                        //Loop through each section and construct a column per budget section
                        angular.forEach(budget.budgetSections.budgetSectionInfo, function (budgetSection, sectionKey) {

                            /*We only want to display the calendar items from the
                             budget, so we only want sections that HAVE 'visitIdent' object.*/
                            if (budgetSection.hasOwnProperty('visitIdent')) {
                                var column = {
                                    name: budgetSection.visitIdent.visitName,
                                    field: budgetSection.visitIdent.visitName,
                                    //width: '20%',
                                    minWidth: 100,
                                    resizable: true,
                                    /*
                                     if this is customised other header functions will have to be tested..
                                     There is dependecy to a lot of functionality in the default
                                     header that I dont think it is wise to change the template until
                                     absolutely neccessary
                                     //headerCellTemplate: 'ui-grid/uiGridHeaderCell',
                                     */
                                    headerTooltip: 'Number of Patients for this Section: ' + budgetSection.BGTSECTION_PATNO,
                                    filters: [
                                        /*
                                         //Custom filter for Categoties
                                         {
                                         placeholder: 'Category',
                                         condition:function (searchTerm, cellValue) {
                                         if(cellValue != undefined && cellValue.hasOwnProperty('PK_CODELST_CATEGORY'))
                                         {
                                         if(cellValue.PK_CODELST_CATEGORY.description.toLowerCase().search(searchTerm) >= 0)
                                         {
                                         return cellValue;
                                         }
                                         }
                                         }
                                         },
                                         */

                                        //Dropdown filter for Cost Type
                                        {
                                            placeholder: 'Cost Type',
                                            type: uiGridConstants.filter.SELECT,
                                            selectOptions: [
                                                {value: 'Research Cost', label: 'Research Cost'},
                                                {value: 'Standard of Care', label: 'Standard of Care'},
                                                {value: 'unknown', label: 'unknown'}
                                            ],

                                            condition: function (searchTerm, cellValue) {
                                                if (cellValue != undefined && cellValue.hasOwnProperty('PK_CODELST_COST_TYPE')) {
                                                    /* alert("search:" + searchTerm + "value = "
                                                     + cellValue.PK_CODELST_COST_TYPE.description
                                                     + cellValue.PK_CODELST_COST_TYPE.description.search(searchTerm) );*/
                                                    if (cellValue.PK_CODELST_COST_TYPE.description.search(searchTerm) >= 0) {
                                                        return cellValue;
                                                    }
                                                }
                                            }

                                        },
                                        {
                                            placeholder: 'Cost > x',
                                            condition: function (searchTerm, cellValue) {
                                                if (cellValue != undefined && cellValue.hasOwnProperty('LINEITEM_OTHERCOST')) {
                                                    /* alert("value=" +cellValue.PK_CODELST_CATEGORY.description + " Search: " + searchTerm
                                                     );*/
                                                    if (parseInt(cellValue.LINEITEM_OTHERCOST) > searchTerm) {
                                                        return cellValue;
                                                    }
                                                }
                                            }
                                        }
                                        /*
                                         //Custom filter for Sponsor cost
                                         ,
                                         {
                                         placeholder: 'Sponsor Cost < x',
                                         condition:function (searchTerm, cellValue) {
                                         if(cellValue != undefined && cellValue.hasOwnProperty('LINEITEM_OTHERCOST'))
                                         {
                                         /!*alert("value=" +cellValue.LINEITEM_SPONSORUNIT + " Search: " + searchTerm
                                         + "lowercase: " + cellValue.LINEITEM_SPONSORUNIT.toLowerCase()
                                         + " Match -- " + searchTerm.toLowerCase().search(cellValue.LINEITEM_SPONSORUNIT.toString())
                                         );*!/
                                         if(parseInt(cellValue.LINEITEM_SPONSORUNIT) < searchTerm)
                                         {
                                         return cellValue;
                                         }
                                         }
                                         }
                                         }*/
                                    ],
                                    cellTemplate: '<div class="ui-grid-cell-contents">' +
                                    '<div class="col-lg-6">' +
                                    '<div class="row"> ' +
                                    '{{COL_FIELD.PK_CODELST_CATEGORY.description}}' +
                                    '</div>' +
                                    '<div class="row"> ' +
                                    '{{COL_FIELD.PK_CODELST_COST_TYPE.description}}' +
                                    '</div>' +
                                    '</div>' +
                                    '<div class="col-lg-6">' +
                                    '<div class="row"><div ng-hide="!COL_FIELD.LINEITEM_OTHERCOST">C:${{COL_FIELD.LINEITEM_OTHERCOST}}</div></div>' +
                                    '<div class="row"><div ng-hide="!COL_FIELD.LINEITEM_SPONSORUNIT">S:${{COL_FIELD.LINEITEM_SPONSORUNIT}}</div></div>' +
                                    '</div>' +
                                    '</div>'
                                };
                                $scope.calViewColumns.push(column);

                                //$scope.calsoapStatus = budgetSection;
                                //Get the line Items and create grid rows.
                                if (budgetSection.hasOwnProperty('lineItems')) {
                                    angular.forEach(budgetSection.lineItems.budgetLineItemInfo, function (lineItem, lineItemKey) {
                                        var newEvent = true;
                                        angular.forEach($scope.CalendarViewLineItems, function (eventItem, eventKey) {
                                            if (eventItem.Event == lineItem.eventNameIdent.eventName) {
                                                eventItem[budgetSection.visitIdent.visitName] = lineItem;
                                                newEvent = false;
                                            }
                                        });
                                        if (newEvent) {
                                            var eventDetails = {Event: lineItem.eventNameIdent.eventName};
                                            eventDetails[budgetSection.visitIdent.visitName] = lineItem;
                                            $scope.CalendarViewLineItems.push(eventDetails);
                                        }
                                    });
                                }
                            }
                        });
                    }
                });

                //Get Events Calendar and non calendar events for data rows.
                // Obj.merge = true for all non calendar items
                $scope.calendarViewGridOptions.columnDefs = $scope.calViewColumns;
                //set the grid data source
                $scope.calendarViewGridOptions.data = $scope.CalendarViewLineItems;

                //Debug messages
                /*$scope.calsoapStatus = "Row count: " + $scope.calendarViewGridOptions.data.length
                 + "data: " + $scope.calendarViewGridOptions.data[30];
                 $scope.calsoapStatus =$scope.CalendarViewLineItems[3];*/
                $scope.calsoapStatus = "Row count: " + $scope.calendarViewGridOptions.data.length;

            };


            //Call function to populate dropdown values
            GetStudyBudget();

            /*$http.get('/data/500_complex.json')
             .success(function(data) {
             for ( var i = 0; i < data.length; i++ ){
             var registeredDate = new Date( data[i].registered );
             data[i].state = data[i].address.state;
             data[i].gender = data[i].gender === 'male' ? 1: 2;
             data[i].balance = Number( data[i].balance.slice(1).replace(/,/,'') );
             data[i].registered = new Date( registeredDate.getFullYear(), registeredDate.getMonth(), 1 )
             }
             delete data[2].age;
             $scope.gridOptions.data = data;
             });*/

        }]);
