/**
 * @ngdoc controller
 * @name myApp:UCSD_StudyCoordinatorCtrl
 *
 * @description
 *
 *
 * @requires $scope
 * */
angular.module('myApp')
    .controller('UCSD_StudyCoordinatorCtrl', ['$scope', '$http','$httpParamSerializerJQLike',
		'$window',
        '$location',
        '$interval', '$q', '$filter',
        'uiGridGroupingConstants',
        'uiGridConstants','$timeout','$templateCache',
         function ($scope, $http,$httpParamSerializerJQLike,$window,$location, $interval,
                                    $q, $filter, uiGridGroupingConstants, uiGridConstants,$timeout,$templateCache) {
    	
    	$scope.selectEpicButton=0;
		$scope.consResultSet=[];
    	$scope.allPatStatCodeList=[];
		$scope.allEthnicityCodeList=[];
		$scope.allGenderList=[];
		$scope.allSurvivalStatList=[];
		$scope.allRaceList=[];
		$scope.allEventStatusList=[];
		$scope.expandedRowEntity=-1;
		$scope.selectedRowEntity=-1;
		$scope.patientStudyID='';
		$scope.searchByCols='';
		$scope.searchByColsVal='';
		$scope.getPatientsCall=0;
		$scope.patDataList=[];
		$scope.buttonFlag=0;
		$scope.buttonFLNFlag=0;
		$scope.hasFocus=0;
            //Get code list Data
            $scope.GetCodeListData = function (codeType) {
             var req = {
             method: 'POST',
             url: nodeURL+'/systemAdminSEI/getCodeList',
             headers: {
             'Content-Type': 'application/json;charset=UTF-8'
            	 },
                    data: { args:{
                        "type": codeType},
                        "jwtToken": jwttoken
                    }
             };

             $http(req).then(function (response) { //success
             //$scope.soapStatus1 = response.data.Codes;
             //$scope.gridOptions.columnDefs[6].editDropdownOptionsArray = response.data.codes;
             //$scope.genderList = response.data.codes;
	     if(response.data.Codes !=null && response.data.Codes.codes!=null){
                    		angular.forEach(response.data.Codes.codes,function(codeValue,codeKey){
				      if(codeType==='patient_status'){
                    			if(codeValue.hidden!==true)
                    				$scope.allSurvivalStatList.push(codeValue);
					}else if(codeType==='gender'){
                    			if(codeValue.hidden!==true)
                    				$scope.allGenderList.push(codeValue);
					}else if(codeType==='patStatus'){
            			if(codeValue.hidden!==true)
            				$scope.allPatStatCodeList.push(codeValue);
					}else if(codeType==='ethnicity'){
            			if(codeValue.hidden!==true)
            				$scope.allEthnicityCodeList.push(codeValue);
					}else if(codeType==='race'){
            			if(codeValue.hidden!==true)
            				$scope.allRaceList.push(codeValue);
					}else if(codeType==='eventstatus'){
            			if(codeValue.hidden!==true)
            				$scope.allEventStatusList.push(codeValue);
					}
                            })
                       }
	     if(codeType==='patient_status')
        	 $scope.GetCodeListData('gender');
	     else if(codeType==='gender')
        	$scope.GetCodeListData('patStatus');
	     else if(codeType==='patStatus')
	        	$scope.GetCodeListData('ethnicity');
	     else if(codeType==='ethnicity')
	        	$scope.GetCodeListData('race');
             }, function (response) { //error
             $scope.soapStatus = response.data;
             });
             };
             $scope.GetCodeListData('patient_status');
             $scope.GetCodeListData('eventstatus');
             $scope.showCodeSType = function(subType, selectedSType){
            	 if(subType==='patient_status')
            		 $scope.selectedSurStatus=selectedSType;
            	 else if(subType==='gender')
            		 $scope.selectedGender=selectedSType;
            	 else if(subType==='patStatus')
            		 $scope.selectedPatStatus=selectedSType;
            	 else if(subType==='ethnicity')
            		 $scope.selectedEthnicity=selectedSType;
            	 else if(subType==='race')
            		 $scope.selectedRace=selectedSType;
            	 else if(subType==='eventstatus')
            		 $scope.selectedEventStatus=selectedSType;
             }
    	    
    	    $scope.organizationList=[];
            //Get list of Studies from search study WS
            //This is for the typeahead study search input.
		$scope.patstudyId="";
    	$scope.patientId="";
    	$scope.enrollDOB="";
            $scope.studyLookup = function (studySearch) {
                var req = {
                    method: 'POST',


                    url: nodeURL+'/study/searchStudy',
                    headers: {
                        'Content-Type': 'application/json;charset=UTF-8'
                    },
                    data: { args:{
                        "StudySearch": {"studyNumber": studySearch}},
                        "jwtToken": jwttoken
                    }
                };


                //Request search study ws for list of studies
                return $http(req).then(function (response) { //success
                	showWarningChange();
                    try {
                        if (response.data.StudySearchResults != null) {
                            //$scope.soapStatus1 = response.data.StudySearchResults.studySearch.count;
                            return response.data.StudySearchResults.studySearch;
                        }
                        else {
                            $scope.studySearchError = "No search results found.";
                            return $q.reject();
                        }
                    }
                    catch ($scope) {
                        return "REST Call error."
                    }
                }, function (response) { //error
                	showWarningChange();
                	alert('Unable to connect to node server.');
                    $scope.soapStatus = response.data;
                });
            };
            $scope.openPatSchedule=function(){
            	showWarningChange();
            	patId=escape(encodeString($scope.selectedPatient.patientIdentifier.patientId));
            	patPK=$scope.selectedPatient.patientIdentifier.PK;
            	studyPK=$scope.selectedPatient.currentStudyPK;
            	patprotPK=$scope.selectedPatient.patprotID;
            	statPK=$scope.selectedPatient.currentStatusPK;
            	if(studyPK===undefined || statPK===undefined){
            		alert('Please wait Until Page Loads...');
            		return false;
            	}
            	if (f_check_perm_org($scope.studyPatRights,$scope.orgRights,'E'))
           			{
            	$scope.hasFocus=1;
            	url = '/velos/jsp/patientschedule.jsp?srcmenu=tdmenubaritem5&calledFrom=patRoster&selectedTab=3&mode=M&patientCode='+patId+'&pkey='+patPK+'&studyId='+studyPK+'&patProtId='+patprotPK+'&statid='+statPK+'&page=patientEnroll&generate=N';
            	//$window.location.href=url;
            	//$location.path(url);
            	$window.open(url,  "PatientSchedule","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=1050,height=600,top=50,left=200");
           			}
            	else
    				{
            		return false;
    				} 
           };
		$scope.addScheduleEvent=function(rowEntity){
			   showWarningChange();
			 if (f_check_perm_org($scope.studyPatRights,$scope.orgRights,'E'))
          		{
				$scope.hasFocus=2;
				var url='selecteventus.jsp?mode=M&showsubmitbutt=N&patId='+$scope.selectedPatient.patientIdentifier.PK+'&patProtId='+$scope.selectedPatient.patprotID+'&calledFromPage=patientEnroll&visitId='+rowEntity.visitId+'&displacement=0&duration=365&protocolId=&calledFrom=S&calStatus=A&cost=-1&calltime=New&form=patsch&calassoc=P&catId=All&catName=All0&ucsdStudyRoster=called&studyId='+$scope.selectedPatient.currentStudyPK;
				//$('#preview').attr('src', url);
				//$('#addUnscheduleEvent').modal('show');
				/*$uibModal.open({
				      templateUrl: url,
				      //controller: 'ModalInstanceCtrl',
				      //size: 'lg'
				    });*/
            	$window.open(url,  "Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=1050,height=600,top=50,left=200");
          		}
			 else
				{
				return false;
				}
          	};
            $scope.openWinProtocol= function(studyPK,patientPK)
            {     showWarningChange();
            if (f_check_perm_org($scope.studyPatRights,$scope.orgRights,'E'))
      			{
            		$scope.hasFocus=1;
    				var url="patientprotocol.jsp?studyId="+studyPK+"&pkey=" + patientPK+"&calldFrom=ucsdStdRoster";
                	$window.open(url,  "patprotocol","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=1000,height=600,top=50,left=200");
      			}
			else
				{
					return false;
				}   
            	
            };
            $scope.openWinMulEventEdit = function()
            {   showWarningChange();
            	patPK=$scope.selectedPatient.patientIdentifier.PK;
            	studyPK=$scope.selectedPatient.currentStudyPK;
            	patprotPK=$scope.selectedPatient.patprotID;	
            if (f_check_perm_org($scope.studyPatRights,$scope.orgRights,'E'))
      			{
            	$scope.hasFocus=2;
    			var url="editMulEventDetails.jsp?patProtId="+patprotPK+"&studyId="+studyPK+"&pkey="+patPK+"&calldFrom=ucsdStdRoster";
               	$window.open(url,  "editMultipleEvent","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=1050,height=600,top=50,left=200");	
      			}
			else
				{
					return false;
				}
      		};
            /*$scope.changeScheduleDate=function(rowEntity){
            	alert(JSON.stringify(rowEntity));
            	$scope.hasFocus=1;
            	var url='changedate.jsp?prevDate='+rowEntity.eventSchDate+'&eventId='+rowEntity.scheduleEventIdentifier.PK+'&patProtId='+$scope.selectedPatient.patprotID+'&pkey='+$scope.selectedPatient.patientIdentifier.PK +'&statDesc=null&statid=&studyVer=&patientCode='+$scope.selectedPatient.patientIdentifier.patientId+'&org_id=&calledFrom=S&acDateFlag=1&protocol_id=&ucsdStudyRoster=called&studyId='+$scope.selectedPatient.currentStudyPK;
            	$window.open(url,  "Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=700,height=600,top=50,left=200");
            };*/
            $window.onfocus = function() {            	
                if($scope.hasFocus===1){
                	showWarningChange();
                	$scope.pageLoaderShow();
            		 $scope.patientVisitGridOptions.data=[];
            		 
            		 var myPromise = $scope.visitWindowLoad($scope.selectedPatient);

            		    // wait until the promise return resolve or eject
            		    //"then" has 2 functions (resolveFunction, rejectFunction)
            		    myPromise.then(function(resolve){
            		    	if($scope.expandedRowEntity !== -1){
            		    	$timeout(function(){$scope.gridApi1.expandable.toggleRowExpansion($scope.gridApi1.grid.rows[$scope.expandedRowEntity].entity);}, 100);
            		    		}
            		        }, function(reject){
            		        console.log(reject);    
            		    });
            		    $scope.gridApi1.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
               		    $scope.gridApi1.grid.refresh();
                        $scope.hasFocus=0;
            		 
                }
                if($scope.hasFocus===2){
                	showWarningChange();
                	$scope.pageLoaderShow();
                	if($scope.expandedRowEntity !== -1){
    		    		$scope.gridApi1.expandable.toggleRowExpansion($scope.gridApi1.grid.rows[$scope.expandedRowEntity].entity);
    		    	    $timeout(function(){$scope.gridApi1.expandable.toggleRowExpansion($scope.gridApi1.grid.rows[$scope.expandedRowEntity].entity);}, 100);
    		    		}else{
    		    			$scope.pageLoaderHide();
    		    		}
                	$scope.gridApi1.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
           		    $scope.gridApi1.grid.refresh();
                    $scope.hasFocus=0; 
                }
                if($scope.hasFocus===3){
                	showWarningChange();
                	$scope.pageLoaderShow();
                	$scope.gridOptions.data = [];
           		 
           		 	getPatients($scope.selectedStudy.studyNumber);

           		    // wait until the promise return resolve or eject
           		    //"then" has 2 functions (resolveFunction, rejectFunction)
           		    $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
              		    $scope.gridApi.grid.refresh();
                       $scope.hasFocus=0;
           		 
               }
                //$scope.pageLoaderHide();
              };
            
            $scope.openSelectPatient = function(firstName,lastName,dob){
            	firstName=$('#firstname').val();
            	lastName=$('#lastname').val();
            	dob=$('#patDateofBirth').val();
            	$scope.isPatientLoading="";
            	$scope.buttonFlag=0;
            	$scope.buttonFLNFlag=0;
            	
            	if(((firstName=='' || !angular.isDefined(firstName)) && ((lastName=='' || !angular.isDefined(lastName)) || (dob=='' || !angular.isDefined(dob)))) || ((lastName=='' || !angular.isDefined(lastName)) && ((firstName=='' || !angular.isDefined(firstName)) || (dob=='' || !angular.isDefined(dob)))) ||
            			((dob=='' || !angular.isDefined(dob)) && ((firstName=='' || !angular.isDefined(firstName)) || (lastName=='' || !angular.isDefined(lastName))))){
            		//alert("At least MRN or Any two of the following fields are required to search in EPIC: \n First Name, Last Name");
            		return false;
            	}
            	var req = {};
				var reqData={};
				var findData="";
            	if((firstName!='' && angular.isDefined(firstName)) && (lastName!='' && angular.isDefined(lastName)) && (dob!='' && angular.isDefined(dob))){
            		req ={
                        method: 'POST',
                        url: nodeURL+'/PatientDemographics/PatientSearch/Name',
                        headers: {
                            'Content-Type': 'application/json;charset=UTF-8'
                        },
                        data: { args:{
                            "PatientSearch": {"patFirstName": firstName,"patLastName":lastName,"patDateofBirth":$filter('date')(new Date(dob), "yyyy-MM-dd"),searchFLNameStartWith:true}},
                            "jwtToken": jwttoken
                        }
                    };
					reqData={'module':'allPatient','curPage':'0','rowsPerPage':'5000','orderType':'asc',
										'orderBy':'PERSON_CODE','meta':'false','userId':userId,'accountId':accountId,
										'grpId':grpId,'fName':firstName,'lName':lastName,"dob":$filter('date')(new Date(dob), "MM/dd/yyyy"),'hbtnValue':'false'
										};
					findData="No patient with name '" + firstName +"' and DOB '"+$filter('date')(new Date(dob), "MM/dd/yyyy")+"' found.";
            	}else if((firstName!='' && angular.isDefined(firstName)) && (lastName!='' && angular.isDefined(lastName))){
            		req ={
                            method: 'POST',
                            url: nodeURL+'/PatientDemographics/PatientSearch/Name',
                            headers: {
                                'Content-Type': 'application/json;charset=UTF-8'
                            },
                            data: { args:{
                                "PatientSearch": {"patFirstName": firstName,"patLastName":lastName,searchFLNameStartWith:true}},
                                "jwtToken": jwttoken
                            }
                        };
						reqData={'module':'allPatient','curPage':'0','rowsPerPage':'5000','orderType':'asc',
										'orderBy':'PERSON_CODE','meta':'false','userId':userId,'accountId':accountId,
										'grpId':grpId,'fName':firstName,'lName':lastName,'hbtnValue':'false'
										};
						findData="No patient with name '" + firstName +" "+lastName +"' found.";
                	}else if((lastName!='' && angular.isDefined(lastName)) && (dob!='' && angular.isDefined(dob))){
                		req ={
                                method: 'POST',
                                url: nodeURL+'/PatientDemographics/PatientSearch/Name',
                                headers: {
                                    'Content-Type': 'application/json;charset=UTF-8'
                                },
                                data: { args:{
                                    "PatientSearch": {"patLastName":lastName,"patDateofBirth":$filter('date')(new Date(dob), "yyyy-MM-dd"),searchFLNameStartWith:true}},
                                    "jwtToken": jwttoken
                                }
                            };
							reqData={'module':'allPatient','curPage':'0','rowsPerPage':'5000','orderType':'asc',
										'orderBy':'PERSON_CODE','meta':'false','userId':userId,'accountId':accountId,
										'grpId':grpId,"dob":$filter('date')(new Date(dob), "MM/dd/yyyy"),'lName':lastName,'hbtnValue':'false'
										};
							findData="No patient with name '" +lastName +"' and DOB '"+$filter('date')(new Date(dob), "MM/dd/yyyy")+"' found.";
                    	}else if((firstName!='' && angular.isDefined(firstName)) && (dob!='' && angular.isDefined(dob))){
                    		req ={
                                    method: 'POST',
                                    url: nodeURL+'/PatientDemographics/PatientSearch/Name',
                                    headers: {
                                        'Content-Type': 'application/json;charset=UTF-8'
                                    },
                                    data: { args:{
                                        "PatientSearch": {"patFirstName": firstName,"patDateofBirth":$filter('date')(new Date(dob), "yyyy-MM-dd"),searchFLNameStartWith:true}},
                                        "jwtToken": jwttoken
                                    }
                                };
							reqData={'module':'allPatient','curPage':'0','rowsPerPage':'5000','orderType':'asc',
										'orderBy':'PERSON_CODE','meta':'false','userId':userId,'accountId':accountId,
										'grpId':grpId,'fName':firstName,"dob":$filter('date')(new Date(dob), "MM/dd/yyyy"),'hbtnValue':'false'
										};
							findData="No patient with name '"+ firstName +"' and DOB '"+$filter('date')(new Date(dob), "MM/dd/yyyy")+"' found.";
                        	}
                    //alert(alert(JSON.stringify(req)));
					//alert(alert(JSON.stringify(reqData)));
                    //Request search study ws for list of studies
                    return $http(req).then(function (response) { //success
                    	     showWarningChange();
                            try {
                                if (response.data.PatientSearchResponse != null
                                    && response.data.PatientSearchResponse.patDataBean != null) {
                                	$scope.selectEpicButton=1;
                                    $scope.isPatientLoading=null;
                                    $scope.patDataList = response.data.PatientSearchResponse.patDataBean;
                                    for(var i=0;i<$scope.patDataList.length;i++){
                                    	$scope.patDataList[i].patDOB=$filter('date')(new Date($scope.patDataList[i].patDateofBirth), "MM/dd/yyyy");
                                    }
                                    $('#getPatientData').modal('show');
                                }
                                else {
                                $scope.buttonFLNFlag=2;
                				$scope.buttonFlag=1;
								$scope.consResultSet=[];
								$.ajax({
									type        : 'POST',
									url     : '../custompaginate',
									data        : reqData,
									async: false,
									cache: false,
									//dataType: 	"json",
									//contentType : "multipart/form-data",                        
									success     : function(r) {
														showWarningChange();
														var response=JSON.parse(r);
														$scope.consResultSet=$scope.changePatbeanJsonElement(response.resultSet);
													},
									error       : function(r) {
														showWarningChange();
														console.log('jQuery Error'); }

								});
								if($scope.consResultSet.length>0){
										$scope.patDataList=$scope.consResultSet;
														for(var i=0;i<$scope.patDataList.length;i++){
															$scope.patDataList[i].patDOB=$filter('date')(new Date($scope.patDataList[i].patDateofBirth), "MM/dd/yyyy")
														}
										//$scope.soapStatus1 = $scope.patDataList.length;
                                        $('#getPatientData').modal('show');
								}else {
				                        $scope.noPatientFLNResults="";
										//$scope.searchedPatient = null;
										$scope.patientSearchError = findData;
										//$scope.buttonFLNFlag=2;
										//$scope.buttonFlag=1;
				                            }
                                }
                            }
                            catch
                                ($scope) {
                                return "REST Call error."
                            }
                        }, function (response) { //error
                        	showWarningChange();
                            $scope.soapStatus = response.data;
                        }
                    );
            }
            
            $scope.selectPatData = function(patBeanData){
            	showWarningChange();
            	$scope.noPatientResults=null;
            	if(patBeanData.MASK_MRN){
            		$scope.buttonFLNFlag=2;
					$scope.buttonFlag=1;}
            	else{
            		$scope.buttonFLNFlag=1;
					$scope.buttonFlag=0;
				}
            	if(patBeanData){
                	$scope.searchedPatient=patBeanData;
            		//$("#patid").val(patBeanData.patientIdentifier.patientId);
            		//$scope.searchedPatient.patientIdentifier.organizationId.PK=patBeanData.patientIdentifier.organizationId.PK;
            		
            	}
            	/*if(patBeanData.patFirstName){
            		$scope.searchedPatient.patFirstName = patBeanData.patFirstName;
           	    }
            	if(patBeanData.patLastName){
            		$scope.searchedPatient.patLastName = patBeanData.patLastName;
           	    }
            	if(patBeanData.patDateofBirth){
            		$scope.searchedPatient.patDateofBirth = new Date(patBeanData.patDateofBirth);
           	    }
            	if(patBeanData.patSurvivalStat)
            	$scope.selectedSurStatus=patBeanData.patSurvivalStat.code;
            	if(patBeanData.gender)
            	$scope.selectedGender=patBeanData.gender.code;
            	if(patBeanData.race)
            	$scope.selectedRace=patBeanData.race.code;
            	if(patBeanData.patientIdentifier.organizationId)
            	$scope.selectedOrganization = {"Id": patBeanData.patientIdentifier.organizationId.PK,
        		 		"Name": patBeanData.patientIdentifier.organizationId.siteName};*/
            	$('#getPatientData').modal('hide');
            }
            
            //Search Patient From Epic
            $scope.selectPatDataEpic = function(){
            	$scope.selectEpicButton=0;
            	showWarningChange();
            	$('#getPatientData').modal('hide');
            	var firstName=$('#firstname').val();
            	var lastName=$('#lastname').val();
            	var dob=$('#patDateofBirth').val();
            	$scope.isPatientLoading="";
            	$scope.buttonFlag=0;
            	$scope.buttonFLNFlag=0;
            	
            	if(((firstName=='' || !angular.isDefined(firstName)) && ((lastName=='' || !angular.isDefined(lastName)) || (dob=='' || !angular.isDefined(dob)))) || ((lastName=='' || !angular.isDefined(lastName)) && ((firstName=='' || !angular.isDefined(firstName)) || (dob=='' || !angular.isDefined(dob)))) ||
            			((dob=='' || !angular.isDefined(dob)) && ((firstName=='' || !angular.isDefined(firstName)) || (lastName=='' || !angular.isDefined(lastName))))){
            		//alert("At least MRN or Any two of the following fields are required to search in EPIC: \n First Name, Last Name");
            		return false;
            	}
				var reqData={};
				var findData="";
            	if((firstName!='' && angular.isDefined(firstName)) && (lastName!='' && angular.isDefined(lastName)) && (dob!='' && angular.isDefined(dob))){
					reqData={'module':'allPatient','curPage':'0','rowsPerPage':'5000','orderType':'asc',
										'orderBy':'PERSON_CODE','meta':'false','userId':userId,'accountId':accountId,
										'grpId':grpId,'fName':firstName,'lName':lastName,"dob":$filter('date')(new Date(dob), "MM/dd/yyyy"),'hbtnValue':'false'
										};
					findData="No patient with name '" + firstName +"' and DOB '"+$filter('date')(new Date(dob), "MM/dd/yyyy")+"' found.";
            	}else if((firstName!='' && angular.isDefined(firstName)) && (lastName!='' && angular.isDefined(lastName))){
            		reqData={'module':'allPatient','curPage':'0','rowsPerPage':'5000','orderType':'asc',
										'orderBy':'PERSON_CODE','meta':'false','userId':userId,'accountId':accountId,
										'grpId':grpId,'fName':firstName,'lName':lastName,'hbtnValue':'false'
										};
						findData="No patient with name '" + firstName +" "+lastName +"' found.";
                	}else if((lastName!='' && angular.isDefined(lastName)) && (dob!='' && angular.isDefined(dob))){
                		reqData={'module':'allPatient','curPage':'0','rowsPerPage':'5000','orderType':'asc',
										'orderBy':'PERSON_CODE','meta':'false','userId':userId,'accountId':accountId,
										'grpId':grpId,"dob":$filter('date')(new Date(dob), "MM/dd/yyyy"),'lName':lastName,'hbtnValue':'false'
										};
							findData="No patient with name '" +lastName +"' and DOB '"+$filter('date')(new Date(dob), "MM/dd/yyyy")+"' found.";
                    	}else if((firstName!='' && angular.isDefined(firstName)) && (dob!='' && angular.isDefined(dob))){
                    		reqData={'module':'allPatient','curPage':'0','rowsPerPage':'5000','orderType':'asc',
										'orderBy':'PERSON_CODE','meta':'false','userId':userId,'accountId':accountId,
										'grpId':grpId,'fName':firstName,"dob":$filter('date')(new Date(dob), "MM/dd/yyyy"),'hbtnValue':'false'
										};
							findData="No patient with name '"+ firstName +"' and DOB '"+$filter('date')(new Date(dob), "MM/dd/yyyy")+"' found.";
                        	}
            	
            	$scope.buttonFLNFlag=2;
				$scope.buttonFlag=1;
				$scope.consResultSet=[];
				$.ajax({
					type        : 'POST',
					url     : '../custompaginate',
					data        : reqData,
					async: false,
					cache: false,
					//dataType: 	"json",
					//contentType : "multipart/form-data",                        
					success     : function(r) {
										showWarningChange();
										var response=JSON.parse(r);
										$scope.consResultSet=$scope.changePatbeanJsonElement(response.resultSet);
									},
					error       : function(r) {
										showWarningChange();console.log('jQuery Error'); }

				});
				if($scope.consResultSet.length>0){
						$scope.patDataList=$scope.consResultSet;
										for(var i=0;i<$scope.patDataList.length;i++){
											$scope.patDataList[i].patDOB=$filter('date')(new Date($scope.patDataList[i].patDateofBirth), "MM/dd/yyyy")
										}
						//$scope.soapStatus1 = $scope.patDataList.length;
                        $('#getPatientData').modal('show');
				}else {
                        $scope.noPatientFLNResults="";
						$scope.patientSearchError = findData;
                            }
            }
                       
                
           //Search by patient id 
            $scope.patientIDLookup = function (patientId) {
            	//$scope.patientId=null;
                var req = {
                    method: 'POST',
                    url: nodeURL+'/PatientDemographics/PatientSearch/Name',
                    headers: {
                        'Content-Type': 'application/json;charset=UTF-8'
                    },
                    data: { 
                    	args:{"PatientSearch": {"patientID":patientId,searchFLNameStartWith:true}},
                        "jwtToken": jwttoken
                    }
                };
                
                //Request search study ws for list of studies
                return $http(req).then(function (response) { //success
                		showWarningChange();
                		$scope.buttonFlag=0;
                    	$scope.buttonFLNFlag=0;
                        try {
                            if (response.data.PatientSearchResponse != null
                                && response.data.PatientSearchResponse.patDataBean != null) {
                            	$scope.buttonFLNFlag=1;
                                $scope.buttonFlag=0;
                                console.log('1');
                                //$scope.soapStatus1 = response.data.PatientSearchResponse.patDataBean.count;
                                return response.data.PatientSearchResponse.patDataBean;
                            }
                            else {
                            	//alert(JSON.stringify($scope.consRejultSet.resultSet));
						        //var jsonData=$scope.changePatbeanJsonElement($scope.consRejultSet.resultSet);
                            	console.log('2');
                            	$scope.buttonFLNFlag=2;
            					$scope.buttonFlag=1;
						        //$scope.patientSearchError = "No patient with patient id '" + patientId + "' found.";
                            	//$scope.searchedPatient = null;
                            	//return $q.reject();
								//var jsonData=$scope.changePatbeanJsonElement($scope.consRejultSet.resultSet);
				                //$scope.soapStatus1 = jsonData.count;
				                //return jsonData;
                            	/*var req = {
										method: 'POST',
										url: '../custompaginate',
										headers: {
												'Content-Type': 'application/x-www-form-urlencoded'
												},
										data: $httpParamSerializerJQLike({'module':'allPatient','curPage':'0','rowsPerPage':'5000','orderType':'asc',
										'orderBy':'PERSON_CODE','meta':'false','userId':userId,'accountId':accountId,
										'grpId':grpId,'patCode':patientId,'patMrn':patientId,'hbtnValue':'false'
										})
								};
								
							$http(req).then(function (response) {
								var jsonData=$scope.changePatbeanJsonElement($scope.consRejultSet.resultSet);
												$scope.soapStatus1 = jsonData.count;
												return jsonData;
										if (response.data != null
				                                && response.data.resultSet != null) {
				                            	//alert(JSON.stringify(response.data));
												//var jsonData=$scope.changePatbeanJsonElement($scope.consRejultSet.resultSet);
				                                //$scope.soapStatus1 = jsonData.count;
				                                //return jsonData;
												
				                            }
				                            else {
				                            	$scope.patientSearchError = "No patient with patient id '" + patientId + "' found.";
				                            	$scope.searchedPatient = null;
				                            	return $q.reject();
				                            }
									}, function (response) { //error
									$scope.soapStatus = response.data;
								});*/
								
								$scope.consResultSet=[];
								$.ajax({
									type        : 'POST',
									url     : '../custompaginate',
									data        : {'module':'allPatient','curPage':'0','rowsPerPage':'5000','orderType':'asc',
										'orderBy':'PERSON_CODE','meta':'false','userId':userId,'accountId':accountId,
										'grpId':grpId,'patCode':patientId,'patMrn':patientId,'hbtnValue':'false'
										},
									async: false,
									cache: false,
									//dataType: 	"json",
									//contentType : "multipart/form-data",                        
									success     : function(r) {
														showWarningChange();
														var response=JSON.parse(r);
														$scope.consResultSet=$scope.changePatbeanJsonElement(response.resultSet);
													},
									error       : function(r) {showWarningChange();console.log('jQuery Error'); }

								});
							}
							if($scope.consResultSet.length>0){
								//$scope.soapStatus1 = $scope.consResultSet.length;
								return $scope.consResultSet;
							}else {
				                            	$scope.patientSearchError = "No patient with patient id '" + patientId + "' found.";
				                            	$scope.searchedPatient = null;
				                            	$scope.patientId=null;
				                            	return $q.reject();
				                            }
                        }
                        catch
                            ($scope) {
                            return "REST Call error."
                        }
                    }, function (response) { //error
                    	showWarningChange();
                        $scope.soapStatus = response.data;
                    }
                );
            };
            
            $scope.$watch("selectedStudy",function(){
            	showWarningChange();
            	$scope.gridOptions.data = [];
				/* subGridOptions Grid clear if we  search any  study */
                if($scope.patientVisitGridOptions !=null){
                $scope.patientVisitGridOptions.data = [];
                $scope.expandedRowEntity=-1;
                $scope.selectedPatient = null;
                $scope.patientVisitGridOptions = null;
                $scope.gridOptions.columnDefs[3].visible = true;
                $scope.gridOptions.columnDefs[4].visible = true;
                $scope.gridOptions.columnDefs[5].visible = true;
                $scope.gridOptions.columnDefs[6].visible = true;
                $scope.gridOptions.columnDefs[7].visible = true;
                $scope.gridOptions.columnDefs[8].visible = true;

                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
                
                }
                /* subGridOptions Grid clear if we  search any  study */
                $scope.currentPage = 1;
                $scope.gridOptions.totalItems = 0;
                $scope.soapStatus = "Row count: " + 0;
       		 	$scope.gridApi.core.refresh();
                $scope.gridApi.grid.refresh();
            	if($scope.selectedStudy.studyNumber){
            		//alert($scope.selectedStudy.studyNumber);
               	 /*var req = {
                            method: 'POST',
                            url: nodeURL+'/study/getStudy',
                            headers: {
                                'Content-Type': 'application/json;charset=UTF-8'
                            },
                            data: {args:{
                                StudyIdentifier: {
                                    studyNumber: $scope.selectedStudy.studyNumber
                                }},
                                "jwtToken": jwttoken
                            }
                        };

                        $http(req).then(function (response) { //success
                            var studyData = response.data.Study;
                            var jsonObj = {};
                             if (response.data.Study.studyOrganizations != null) {
                                	//$scope.organizationList = response.data.Study.studyOrganizations.studyOrganizaton;
                            	 $scope.organizationList =[];
                            	 for (var i = 0, len = response.data.Study.studyOrganizations.studyOrganizaton.length; i < len; i++) {
                                     if (response.data.Study.studyOrganizations.studyOrganizaton[i].organizationIdentifier) {
                                         //alert(response.data.Study.studyOrganizations.studyOrganizaton[i].organizationIdentifier.PK);
                                        jsonObj = {}
                                        jsonObj = {"Id": response.data.Study.studyOrganizations.studyOrganizaton[i].organizationIdentifier.PK,
                                        		 		"Name": response.data.Study.studyOrganizations.studyOrganizaton[i].organizationIdentifier.siteName};
                                        $scope.organizationList.push(jsonObj);
                                     }
                                     }	                                     
                                }
                                

                        }, function (response) { //error
                            $scope.soapStatus = response.data;
                        });*/
                        
                        $scope.populateCalData($scope.selectedStudy.studyNumber);
                        $scope.currentPage = paginationOptions.pageNumber;
              		  	$scope.pageSize = paginationOptions.pageSize;
              		    $scope.sortBy = paginationOptions.sortColumn;
              		    $scope.sortOrder = paginationOptions.sort;
              		  	$scope.selectedRowEntity=-1;
              		  	$scope.studyPatRights = 0;
                        getPatients($scope.selectedStudy.studyNumber);
               }
            });
            
            $scope.$watch("searchedPatient",function(){
            	showWarningChange();
            	if($scope.searchedPatient.patientIdentifier.PK || $scope.searchedPatient.MASK_MRN){
            	$scope.selectedSurStatus='';
            	$scope.selectedEthnicity='';
            	$('#selectedEthnicity').val('');
        		$scope.selectedRace='';
        		$scope.selectedGender='';
        		$scope.selectedPatStatus='';
        		$scope.selectedCalendar='';
        		$scope.studyStatusDate='';
        		$scope.calStartDate='';
        		$('#firstname').val($scope.searchedPatient.patFirstName);
        		$('#lastname').val($scope.searchedPatient.patLastName);
        		$('#patDateofBirth').val($scope.searchedPatient.patDateofBirth);
                if($scope.searchedPatient=='[object Object]'){
                	var iEl = angular.element(document.querySelector('#patid'));
                	$timeout(function(){iEl.val('');},200);
                }
            	if($scope.searchedPatient.patDateofBirth){
            		$scope.searchedPatient.patDateofBirth = new Date($scope.searchedPatient.patDateofBirth);
           	    }
            	if($scope.searchedPatient.deathDate){
            		$scope.searchedPatient.deathDate = new Date($scope.searchedPatient.deathDate);
           	    }
            	if($scope.searchedPatient.patSurvivalStat)
            	$scope.selectedSurStatus=$scope.searchedPatient.patSurvivalStat.code;
            	if($scope.searchedPatient.gender)
            	$scope.selectedGender=$scope.searchedPatient.gender.code;
            	if($scope.searchedPatient.race)
            	$scope.selectedRace=$scope.searchedPatient.race.code;
            	if($scope.searchedPatient.patientIdentifier.organizationId){
            		angular.forEach($scope.organizationList, function (organization, organizationKey) {
            			if(organization.Id===$scope.searchedPatient.patientIdentifier.organizationId.PK){
            				$scope.selectedOrganization = {"Id": $scope.searchedPatient.patientIdentifier.organizationId.PK,
                    		 		"Name": $scope.searchedPatient.patientIdentifier.organizationId.siteName};
            			}
						});
            	}
				if(angular.isDefined($scope.patientId) && $scope.patientId==''){
					$scope.patientId = $scope.searchedPatient.patientIdentifier.patientId;
				}
            	}
            	/*if($scope.searchedPatient.patFirstName){
            		$scope.searchedPatient=$scope.searchedPatient.patFirstName;
            	}*/
            });
            
            /*$scope.$watch("searchedPatient.patDateofBirth",function(){
            	var iEl = angular.element(document.querySelector('#patid'));
            	$timeout(function(){iEl.val('');},200);
            });*/
          
            /**
             * Reset add patient modal on modal close.
             * This is done so that the modal starts fresh each time it is
             * opened.


             */
            $('#myModal').on('hidden.bs.modal', function () {
                $(this).find("input,textarea,select").val('').end();
                $scope.searchedPatient = null;
            });
            $scope.openPatDialog = function(){
            	showWarningChange();
            	$(".btn").removeClass('disabled');
            	$scope.patientId='';
            	//$scope.searchedPatient.patDateofBirth='';
             	$scope.patientDOB='';
             	$scope.selectedPatStatus='';
             	$scope.selectedOrganization=$scope.organizationList[0];
             	$scope.selectedSurStatus='';
             	$scope.selectedEthnicity='';
             	$scope.selectedRace='';
             	$scope.selectedGender='';
            	$scope.searchedPatient = null;
            	$scope.noPatientResults=null;
            	$scope.buttonFlag=0;
            	$scope.buttonFLNFlag=0;
            	$('#myModal').modal('show');
                $('#myModal').modal({
                    keyboard: false,
                    backdrop: 'static'
                });


            $('#myModal').on('hidden.bs.modal', function (event) {
                    $(this).find("input,textarea,select").val('').end();
                    $scope.searchedPatient = null;
                });
            }
            
            $('#eventPowerBarModal').on('hidden.bs.modal', function () {
                $(this).find("input,textarea,select").val('').end();
            });
            $scope.openSchEventUpdate = function(){
            	showWarningChange();
            if (f_check_perm_org($scope.studyPatRights,$scope.orgRights,'E'))
                {
            	$(".btn").removeClass('disabled');
             	$scope.selectedEventStatus='';
             	$scope.statusDate='';
             	$scope.eventNotes='';
            	$('#eventPowerBarModal').modal('show');
                $('#eventPowerBarModal').modal({
                    keyboard: false,
                    backdrop: 'static'
                });


            $('#eventPowerBarModal').on('hidden.bs.modal', function (event) {
                    $(this).find("input,textarea,select").val('').end();
                });
                }
            else
                {
                return false;
                }
            }
            /*$scope.openTestModal=function(){
                $('#myModal').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            $('#myModal').on('hidden.bs.modal', function (event) {
                    $(this).find("input,textarea,select").val('').end();
                    $scope.searchedPatient = null;
                });
            };*/
            $scope.openPatStatModal=function(StudyId,patId,patientId,patStudyId,currPatStatId,type,pageRight,orgRight){
            	showWarningChange();
            	if(StudyId===undefined || patId===undefined || currPatStatId===undefined){
            		alert('Please wait Until Page Loads...');
            		return false;
            	}
            	$scope.patientStudyID = patStudyId;
            	$scope.patientID = patientId;
            	$scope.patPK   = patId;
            	$scope.studyPK = StudyId;
            	/*$('#patStatUpdate').modal('show');
                $('#patStatUpdate').modal({
                    keyboard: false,
                    backdrop: 'static'
                });


            $('#patStatUpdate').on('hidden.bs.modal', function (event) {
                    $(this).find("input,textarea,select").val('').end();
                    $scope.patientID ='';
                });*/
            	if (f_check_perm_org(pageRight,orgRight,'E'))
            		{
            	$scope.hasFocus=3;
            	if(type==='new')
            		url = '/velos/jsp/patstudystatus.jsp?patStudiesEnrollingOrg=&studyId='+StudyId+'&calldFrom=ucsdStdRoster&changeStatusMode=no&statid='+currPatStatId+'&pkey='+patId+'&currentStatId='+currPatStatId;
            	else
            		url = '/velos/jsp/patstudystatus.jsp?patStudiesEnrollingOrg=&studyId='+StudyId+'&calldFrom=ucsdStdRoster&changeStatusMode=yes&statid='+currPatStatId+'&pkey='+patId+'&currentStatId='+currPatStatId;
            		
            	$window.open(url,  "PatientStatus","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=1000,height=600,top=50,left=200");
            		}
            	else
        			{
        			return false;
        			}
            };
            
            $('#patStatUpdate').on('hidden.bs.modal', function (e) {
                $(this).find("input,textarea,select").val('').end();
                $scope.patientID ='';
            });
            $('#eventPowerBarModal').on('hidden.bs.modal', function (e) {
                $(this).find("input,textarea,select").val('').end();
            });

            /**
             *  Method to enroll an existing patient
             *  in the current study.
             *
             * @returns {*}
             * @param selectedPatient
             * @param selectedStudy
             */
            $scope.enrollPatientToStudy = function (selectedPatient, selectedStudy) {
            	$scope.pageLoaderShow();
            	showWarningChange();
            	$(".btn").addClass('disabled');
                //Get Study PK
                //Get study Details
                // <%--Angular REST request--%>
                // <%--Create post request--%>
                //need to replace static code with angular service
                var req = {
                    method: 'POST',
                    url: nodeURL+'/study/getStudy',
                    headers: {
                        'Content-Type': 'application/json;charset=UTF-8'
                    },
                    data: {args:{
                        StudyIdentifier: {
                            studyNumber: selectedStudy.studyNumber
                        }},
                        "jwtToken": jwttoken
                    }
                };

                $http(req).then(function (response) { //success
                    var studyData = response.data.Study;
                    if(($scope.selectedPatStatus==null || $scope.selectedPatStatus=='') || ($scope.selectedOrganization == null || $scope.selectedOrganization == '') || ($scope.studyStatusDate=='' || angular.isUndefined($scope.studyStatusDate))){
  		    			 alert('Please Enter Data in All Mandatory Fields.');
  		    			 $(".btn").removeClass('disabled');
  		    			$scope.pageLoaderHide();
  		    			 return false;
  		    		 }
                    if((($scope.selectedCalendar!='' && !angular.isUndefined($scope.selectedCalendar)) && ($scope.calStartDate=='' || angular.isUndefined($scope.calStartDate))) || (($scope.selectedCalendar==null || $scope.selectedCalendar=='' || angular.isUndefined($scope.selectedCalendar)) && ($scope.calStartDate!='' && !angular.isUndefined($scope.calStartDate)))){
                		 alert('Please Select Calendar and Calendar Start Date.');
                		 $(".btn").removeClass('disabled');
                		 $scope.pageLoaderHide();
                		 return false;
                	}
                    if($scope.selectedPatStatus==='active' || $scope.selectedPatStatus==='followup' || $scope.selectedPatStatus==='offtreat' || $scope.selectedPatStatus==='offstudy' || $scope.selectedPatStatus==='lockdown'){
                    	 alert("The patient is not currently enrolled. To enter this status, please enter an 'Enrolled' status first.");
 		    			 $(".btn").removeClass('disabled');
 		    			 $scope.pageLoaderHide();
 		    			 return false;
                    }
                    var currentDate = new Date();
                    var req = {
                        method: 'POST',
                        url: nodeURL+'/StudyPatient/enrollPatientToStudy',
                        headers: {
                            'Content-Type': 'application/json;charset=UTF-8'
                        },
                        data: {args:{
                            "PatientIdentifier": {"PK": selectedPatient.patientIdentifier.PK},
                            "StudyIdentifier": {"PK": studyData.studyIdentifier.PK},
                            "PatientEnrollmentDetails": {
                                "currentStatus": "1",
                                "enrolledBy": {"PK": userId}, 
                                "enrollingSite": {"PK": $scope.selectedOrganization.Id}, 
                                "patientStudyId": $scope.patientStudyId,
                                "status": {"code": $scope.selectedPatStatus  , "type": "patStatus"},
                                "statusDate": $filter('date')(new Date($scope.studyStatusDate), "yyyy-MM-dd")
                            }},
                            "jwtToken": jwttoken,
                            "remoteaddr": ipAdd
                        }
                    };

                    //Enroll Patient
                    $http(req).then(function (response) { //success
                            try {
                                if (response.data.Response != null
                                    && response.data.Response.results != null) {
                                	$scope.pageLoaderShow();
                                	$('#myModal').modal('hide');
                                    $scope.soapStatus =
                                        "Enrolled new Patient:" + selectedPatient.patientIdentifier.patientId;
                                    
                                    if(($scope.selectedCalendar==null || $scope.selectedCalendar=='') || ($scope.calStartDate=='' || angular.isUndefined($scope.calStartDate))){
                                    	$scope.pageLoaderHide();
                                    	$scope.pageSize = paginationOptions.pageSize;
                                    	getPatients(selectedStudy.studyNumber);
              	                       	$scope.PatDialogClose();
              	                        $(".btn").removeClass('disabled');
               		    			    return false;
               		    		    }
                                  //generate Schedule 
                     		    	var req = {
                     		    			 method: 'POST',
                                              url: nodeURL+'/PatientSchedule/addMPatientSchedules',
                                              headers: {
                                                  'Content-Type': 'application/json;charset=UTF-8'
                                              },
                                              data: {args: {
                                            	  "PatientSchedules":{
                                              	                        "mPatientSchedule":[{
                                              	                        "patientIdentifier":{"patientId":selectedPatient.patientIdentifier.patientId,"organizationId":{"PK":$scope.selectedOrganization.Id,"siteName":$scope.selectedOrganization.Name}},
                                              	                        "studyIdentifier":{"PK": studyData.studyIdentifier.PK,"studyNumber":selectedStudy.studyNumber},
                                              	                        "calendarIdentifier":{"PK":$scope.selectedCalendar},
                                              	                        "startDate": $filter('date')(new Date($scope.calStartDate), "yyyy-MM-dd")
                                              	                        }]
                                              	                    }},
                                                                    "jwtToken": jwttoken,
                                                                    "remoteaddr": ipAdd
                                                  
                                              }
                     		    	};
         	                        $http(req).then(function (response) {
         	                        	if(response.data.addmpatientschedules.mPatientSchedule){
         	                        		$scope.soapStatus = "Schedule generated successfully for:"+selectedPatient.patientIdentifier.patientId;
         	                        		$scope.pageLoaderHide();
         	                        		$scope.pageSize = paginationOptions.pageSize;
                  	                       	getPatients(selectedStudy.studyNumber);
                  	                       	$scope.PatDialogClose();
                  	                       $(".btn").removeClass('disabled');
         	                        	}else{
         	                        		alert('Unable to Genrate Schedule');
         	                        		$scope.pageLoaderHide();
         	                        		$scope.pageSize = paginationOptions.pageSize;
                  	                       	getPatients(selectedStudy.studyNumber);
                  	                       	$scope.PatDialogClose();
         	                        		$(".btn").removeClass('disabled');
         	                        	}
                     		    	},function (response) { //error
                     		    		$scope.soapStatus = "Unable to Genrate Schedule";
                     		    		$scope.pageLoaderHide();
     	                        		$scope.pageSize = paginationOptions.pageSize;
              	                       	getPatients(selectedStudy.studyNumber);
              	                       	$scope.PatDialogClose();
              	                        $(".btn").removeClass('disabled');
                                        $scope.status = response.status;
                                     });
                     		    	// End Schedule 
                                    //If sucessful refresh the study patient grid
         	                       $scope.PatDialogClose();
         	                       $scope.pageLoaderHide();
         	                       $(".btn").removeClass('disabled');
         	                       //getPatients(selectedStudy.studyNumber);

                                }
                                else {
                                	if(response.data.body){
                              			var errorType = JSON.stringify(response.data.body);
                              			var firstIndex = errorType.indexOf('<message>');
                              			var lastIndex = errorType.indexOf('</message>');
                              			if(firstIndex>0)
                              				errorType = errorType.slice(firstIndex+9,lastIndex)
                              			if('Failed to addPatientStudyStatus'===errorType)
                              				alert("The patient is not currently enrolled. To enter this status, please enter an 'Enrolled' status first.");
                              			else if('java.lang.NullPointerException'===errorType)
                              				alert('Enrolling Site is not in list of Study Organizations where user can enroll patient');
                              			else if('EnrollingSite is not in list of Study Organizations where user can enroll patient'===errorType)
                              				alert('Enrolling Site is not in list of Study Organizations where user can enroll patient');
                              			else
                              				alert(errorType);
                              			$scope.pageLoaderHide();
                              			$(".btn").removeClass('disabled');
                             		 }else{
                                    $scope.soapStatus = "Unable to enroll patient...Already Enrolled?";
                                    $scope.pageLoaderHide();
                             		 }
                                	$(".btn").removeClass('disabled');
                                }
                            }
                            catch
                                ($scope) {
                                $scope.soapStatus = "REST Call error."
                                $scope.pageLoaderHide();
                            }
                        }, function (response) { //error
                            $scope.soapStatus = response.data;
                            $scope.pageLoaderHide();
                        }
                    );
                }, function (response) { //error
                    $scope.status = response.status;
                    $scope.pageLoaderHide();
                });
            };


//Call function to populate dropdown values
//GetGenderCodes();

			var filterChanged = false;
			var correctTotalPaginationTemplate = 
	            "<div role=\"contentinfo\" class=\"ui-grid-pager-panel\" ui-grid-pager ng-show=\"grid.options.enablePaginationControls\"><div role=\"navigation\" class=\"ui-grid-pager-container\"><div role=\"menubar\" class=\"ui-grid-pager-control\"><button type=\"button\" role=\"menuitem\" class=\"ui-grid-pager-first\" ui-grid-one-bind-title=\"aria.pageToFirst\" ui-grid-one-bind-aria-label=\"aria.pageToFirst\" ng-click=\"pageFirstPageClick()\" ng-disabled=\"cantPageBackward()\"><div class=\"first-triangle\"><div class=\"first-bar\"></div></div></button> <button type=\"button\" role=\"menuitem\" class=\"ui-grid-pager-previous\" ui-grid-one-bind-title=\"aria.pageBack\" ui-grid-one-bind-aria-label=\"aria.pageBack\" ng-click=\"pagePreviousPageClick()\" ng-disabled=\"cantPageBackward()\"><div class=\"first-triangle prev-triangle\"></div></button> <input type=\"number\" ui-grid-one-bind-title=\"aria.pageSelected\" ui-grid-one-bind-aria-label=\"aria.pageSelected\" class=\"ui-grid-pager-control-input\" ng-model=\"grid.options.paginationCurrentPage\" min=\"1\" max=\"{{ paginationApi.getTotalPages() }}\" required> <span class=\"ui-grid-pager-max-pages-number\" ng-show=\"paginationApi.getTotalPages() > 0\"><abbr ui-grid-one-bind-title=\"paginationOf\">/</abbr> {{ paginationApi.getTotalPages() }}</span> <button type=\"button\" role=\"menuitem\" class=\"ui-grid-pager-next\" ui-grid-one-bind-title=\"aria.pageForward\" ui-grid-one-bind-aria-label=\"aria.pageForward\" ng-click=\"pageNextPageClick()\" ng-disabled=\"cantPageForward()\"><div class=\"last-triangle next-triangle\"></div></button> <button type=\"button\" role=\"menuitem\" class=\"ui-grid-pager-last\" ui-grid-one-bind-title=\"aria.pageToLast\" ui-grid-one-bind-aria-label=\"aria.pageToLast\" ng-click=\"pageLastPageClick()\" ng-disabled=\"cantPageToLast()\"><div class=\"last-triangle\"><div class=\"last-bar\"></div></div></button></div><div class=\"ui-grid-pager-row-count-picker\" ng-if=\"grid.options.paginationPageSizes.length > 1\"><select ui-grid-one-bind-aria-labelledby-grid=\"'items-per-page-label'\" ng-model=\"grid.options.paginationPageSize\" ng-options=\"o as o for o in grid.options.paginationPageSizes\"></select><span ui-grid-one-bind-id-grid=\"'items-per-page-label'\" class=\"ui-grid-pager-row-count-label\">&nbsp;{{sizesLabel}}</span></div><span ng-if=\"grid.options.paginationPageSizes.length <= 1\" class=\"ui-grid-pager-row-count-label\">{{grid.options.paginationPageSize}}&nbsp;{{sizesLabel}}</span></div><div class=\"ui-grid-pager-count-container\"><div class=\"ui-grid-pager-count\"><span ng-show=\"grid.options.totalItems > 0\">{{(((grid.options.paginationCurrentPage-1)*grid.options.paginationPageSize)+1)}} <abbr ui-grid-one-bind-title=\"paginationThrough\">-</abbr> {{(grid.options.paginationCurrentPage*grid.options.paginationPageSize>grid.options.totalItems?grid.options.totalItems:grid.options.paginationCurrentPage*grid.options.paginationPageSize)}} {{paginationOf}} {{grid.options.totalItems}} {{totalItemsLabel}}</span></div></div></div>";
            var paginationOptions = {
        		    pageNumber: 1,
        		    pageSize: 25,
        		    sort: null,
        		    sortColumn: null
        		  };
        		  $scope.currentPage = paginationOptions.pageNumber;
        		  $scope.pageSize = paginationOptions.pageSize;
        		  $scope.sortBy = paginationOptions.sortColumn;
        		  $scope.sortOrder = paginationOptions.sort;
             var expandableScope =
             {
                name: "SubGridScope",

                /*
                 * Update patient demographics data
                 *  Can be expanded to handle update or create.
                 *  API has a create patient option that we can leverage
                 *  -(create not implemented as of 10/21/16)
                 * */
                saveRow: function (rowEntity) {
                    // create a fake promise - normally you'd use the promise returned by $http or $resource
                    var promise = expandableScope.updateEvent(rowEntity);
                    expandableScope.subGridApi.rowEdit.setSavePromise(rowEntity, promise.promise);
                },

                updateEvent: function (rowEntity) {
                    //Code goes in here for the event update
                }
            };


//Functions needed for sub grid.
            /*$scope.subGridScope = {
             clickMeSub: function(){
             alert('hi2');
             }
             };*/
             /*$scope.gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
            	    paginationOptions.pageNumber = newPage;
            	    paginationOptions.pageSize = pageSize;
            	    alert(newPage);
            	    //getPage();
            	});*/


            $scope.data = [];
            $scope.selectedRowData=[];
            $scope.gridOptions = {
            	virtualizationThreshold: 1000,
                enableGridMenu: true,
                enableFiltering: true,
                //enableSorting: true,
                resizable: true,
                enableCellEditOnFocus: false,
                enableSelectAll: false,
                enableColumnResizing: true,
                multiSelect: false,
                useExternalPagination: true,
                useExternalSorting: true,
				paginationPageSizes: [$scope.pageSize, $scope.pageSize * 2, $scope.pageSize * 3, $scope.pageSize * 4],
                paginationPageSize: paginationOptions.pageSize,
                paginationTemplate: correctTotalPaginationTemplate,
                //This is the template that will be used to render subgrid.
                // expandableRowTemplate: 'htmlTemplates/expandableRowTemplate.html',
                //This will be the height of the subgrid
                // expandableRowHeight: 150,
                //Variables of object expandableScope will be available in the scope of the expanded subgrid
                // expandableRowScope: expandableScope,
                /*importerDataAddCallback: function (grid, newObjects) {
                 $scope.data = $scope.data.concat(newObjects);
                 },*/
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    gridApi.rowEdit.on.saveRow($scope, $scope.saveRow);
                    gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
                    	showWarningChange();
                    	paginationOptions.pageNumber = newPage;
                        paginationOptions.pageSize = pageSize;
                        $scope.pageSize = pageSize;
                        $scope.currentPage = newPage;
                        $scope.totalPage = Math.ceil($scope.gridOptions.totalItems / $scope.pageSize);
                        getPatients($scope.selectedStudy.studyNumber);
                          
                      });
                    $scope.gridApi.core.on.sortChanged($scope, function(grid, sortColumns) {
                    	showWarningChange();
                        if (sortColumns.length === 0) {
                          //paginationOptions.sort = "asc";
                          //paginationOptions.sortColumn = "studyPatEnrollingSite.siteName";
                        } else {
                          paginationOptions.sort = sortColumns[0].sort.direction;
                          paginationOptions.sortColumn = sortColumns[0].field;
                        }
                        //alert(sortColumns[0].sort.direction);
                        //alert(sortColumns[0].field);
                        $scope.sortBy = paginationOptions.sortColumn;
              		    $scope.sortOrder = paginationOptions.sort;
              		  if (sortColumns.length === 1){
              			$scope.gridOptions.data=[];
                        $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
                        if($scope.selectedStudy.studyNumber)
                        	getPatients($scope.selectedStudy.studyNumber);
              		  }
                      });
                    /*gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
                        paginationOptions.pageNumber = newPage;
                        paginationOptions.pageSize = pageSize;
                        getPatients($scope.selectedStudy.studyNumber)
                      });*/
                    /************************* Row count after filter implementation *************************************/
                    gridApi.core.on.filterChanged($scope, function() {
                    	showWarningChange();
                        filterChanged = true;
                        var grid = this.grid;
                        var cont=0;
                        angular.forEach(grid.columns, function(value, key) {
                            if(value.filters[0].term) {
                            	if(cont===0){
                            		$scope.searchByCols = '';
                            		$scope.searchByColsVal = '';
                            		$scope.searchByCols = value.colDef.field;
                            		$scope.searchByColsVal = value.filters[0].term;
                            		cont++;
                            	}else{
                            		$scope.searchByCols = $scope.searchByCols+','+value.colDef.field;
                            		$scope.searchByColsVal = $scope.searchByColsVal+','+value.filters[0].term;
                            		cont++;
                            	}
                            }
                            $scope.getPatientsCall=1;
                        });
                        if(cont===0){
                        	$scope.searchByCols = '';
                    		$scope.searchByColsVal = '';
                        }
                        $scope.gridOptions.data=[];
                        $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
                        var currPage=$scope.currentPage;
                        $scope.currentPage = 1;
                        //$scope.gridOptions.totalItems = 0;
                        $scope.pageSize = paginationOptions.pageSize;
              		    $scope.sortBy = paginationOptions.sortColumn;
              		    $scope.sortOrder = paginationOptions.sort;
              		    if($scope.selectedStudy.studyNumber)
              		    	$timeout( function(){if($scope.getPatientsCall===1){$scope.getPatientsCall=0;if(currPage>1){$scope.gridOptions.totalItems = 0}else{getPatients($scope.selectedStudy.studyNumber);}}}, 3000 );
                    });

                    gridApi.core.on.rowsVisibleChanged($scope, function() {
                    	showWarningChange();
                    	$scope.soapStatus = "Row count: " + gridApi.grid.getVisibleRows().length;
                        if(!filterChanged){
                        	$scope.soapStatus = "Row count: " + gridApi.grid.getVisibleRows().length;
                            return;
                        }
                        filterChanged = false;
                        // The following line extracts the filtered rows
                        var filtered = _.filter($scope.gridApi.grid.rows, function(o) { return o.visible; });
                        var entities = _.map(filtered, 'entity'); // Entities extracted from the GridRow array
                        //alert("filter end");
                       
                    });
                    
                    /*************************** Row count after filter implementation *************************************/
                    gridApi.selection.on.rowSelectionChanged($scope,
                        function (row) {
                    	showWarningChange();
                            if (row.isSelected) {
                            	$scope.patientStudyID = row.entity.studyPatId;
                            	//function for selected row  
                            	//alert(row.entity.patientIdentifier.PK);
                            	//alert(row.entity.patprotID);
                            	//alert('hi');
                            	//Get Patient Schedules
                            	// override the normal expandable grid row header to remove the expansion icon/button
                                if ($templateCache.get('ui-grid/expandableTopRowHeader') !== "<div class=\"ui-grid-row-header-cell\"></div>") {
                                  var savedTemplate = $templateCache.get('ui-grid/expandableTopRowHeader');
                                  $templateCache.put('ui-grid/expandableTopRowHeader', "<div class=\"ui-grid-row-header-cell\"></div>");
                                  // restore the default template
                                  $scope.$on('$destroy', function() {
                                    $templateCache.put('ui-grid/expandableTopRowHeader', savedTemplate);
                                  });
                                }
                            	$scope.patientVisitGridOptions = {
                            		virtualizationThreshold: 10000,
                                    //appScopeProvider: $scope.subGridScope,
                                    //enableGridMenu: true,
                                    enableFiltering: true,
                                    enableGroupHeaderSelection: true,
                                    //enableColumnResizing: true,
                                    //enableExpandable: true,
                                    enableExpandable: true,
                                    enableExpandableRowHeader: true,
                                    enableRowHeaderSelection: true,
                                    enableRowSelection: false,
                                    enableSelectAll: false,
                                    enableSorting: false,
                                    enableColumnMenus: false,
                                    //multiSelect: false,
                                    expandableRowTemplate: '<div ui-grid="row.entity.subGridOptions" style="height:150px;" ui-grid-auto-resize ui-grid-selection ui-grid-resize-columns></div>',
                                    expandableRowHeight: 150,
                                    //showExpandAllInHeader : false,
                                    //expandableRowHeight: 70,
                                  //subGridVariable will be available in subGrid scope
                                    expandableRowScope: {
                                      subGridVariable: 'subGridScopeVariable',
                                    changeScheduleDate: function(rowEntity){
                                	//alert(JSON.stringify(rowEntity));
                            		if (f_check_perm_org($scope.studyPatRights,$scope.orgRights,'E'))
                            			{
                                	$scope.hasFocus=2;
                                	var url='changedate.jsp?prevDate='+rowEntity.eventSchDate+'&eventId='+rowEntity.scheduleEventIdentifier.PK+'&patProtId='+$scope.selectedPatient.patprotID+'&pkey='+$scope.selectedPatient.patientIdentifier.PK +'&statDesc=null&statid=&studyVer=&patientCode='+$scope.selectedPatient.patientIdentifier.patientId+'&org_id=&calledFrom=S&acDateFlag=1&protocol_id=&ucsdStudyRoster=called&studyId='+$scope.selectedPatient.currentStudyPK;
                                	$window.open(url,  "Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=700,height=600,top=50,left=200");
                            			}
                                	else
                            			{
                            			return false;
                            			}
                            			}
                                    },
                                    resizable: true,
                                    onRegisterApi: function (gridApi) {
                                    	$scope.gridApi1 = gridApi;
                                        gridApi.rowEdit.on.saveRow($scope, $scope.saveRow);                                        
                                        //function for selected row
                                        gridApi.selection.on.rowSelectionChanged($scope, function(row){
                                            var selectedState = row.isSelected;
                                            
                                            // if row is expanded, toggle its children as selected
                                            if (row.isExpanded){
                                              // choose the right callback according to row status
                                              var selectCallBack = selectedState?"selectAllRows":"clearSelectedRows";
                                              // do the selection/unselection of children
                                              row.subGridApi.selection[selectCallBack]();
                                            }else{
                                            	row.isSelected=false;
                                            }
                                            
                                          //mark children as selected if needed
                                            angular.forEach(row.entity.subGridOptions.data, function(value){
                                              // create the "isSelected" property if not exists
                                              /*if (angular.isUndefined(row.isSelected)){
                                                row.isSelected = {}; 
                                              }*/
                                            	//alert(JSON.stringify(value));
                                            	if (row.isSelected) {
                                            		var checkedEvents=JSON.stringify($scope.selectedRowData);
                                             	   if(value.visitId){
                                             		  if(checkedEvents.search(value.$$hashKey)<0){
                                             			 $scope.selectedRowData.push(value);
                                                  	  }
                                             		$scope.patientID = value.patientID;
                                             	   }
                                             	}else{
                                             		var cnt = 0;
                                             		for(var k=0;k<$scope.selectedRowData.length;k++){
                                             			if(value.$$hashKey===$scope.selectedRowData[k].$$hashKey){
                                             				cnt=k;
                                             				break;
                                             			}
                                             		}
                                             		$scope.selectedRowData.splice(cnt,1);
                                             	}
                                              // keep the selected rows values in the parent row - idealy would be a unique ID coming from the server
                                              //row.isSelected[value.name] = selectedState;
                                            });
                                            
                                          });
                                        /*gridApi.selection.on.rowSelectionChanged($scope,
                                                function (row) {                                                            	
                                        	if (row.isSelected) {
                                        	   if(row.entity.visitId){
                                        		$scope.selectedRowData.push(row.entity);
                                        		$scope.patientID = row.entity.patientID;
                                        	   }
                                        	}else{
                                        		var cnt = 0;
                                        		for(var k=0;k<$scope.selectedRowData.length;k++){
                                        			if(row.entity.$$hashKey===$scope.selectedRowData[k].$$hashKey){
                                        				cnt=k;
                                        				break;
                                        			}
                                        		}
                                        		$scope.selectedRowData.splice(cnt,1);
                                        	}
                                        	if(row.internalRow==true && row.isSelected==true){
                                                var childRows   = row.treeNode.children;
                                                for (var j = 0, length = childRows.length; j < length; j++) {
                                                    var rowEntity   = childRows[j].row.entity;
                                                    gridApi.selection.selectRow(rowEntity);
                                                    
                                                }
                                            }

                                            if(row.internalRow==true && row.isSelected==false){
                                                var childRows   = row.treeNode.children;
                                                for (var j = 0, length = childRows.length; j < length; j++) {
                                                    var rowEntity   = childRows[j].row.entity;
                                                    gridApi.selection.unSelectRow(rowEntity);
                                                   
                                                }
                                            }
                                        });
                                        gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
                                            rows.forEach(function (row) {                                                            	
                                            	if (row.isSelected) {
                                            		$scope.selectedRowData.push(row.entity);
                                            		$scope.patientID = row.entity.patientID;
                                            	}else{
                                            		var cnt = 0;
                                            		for(var k=0;k<$scope.selectedRowData.length;k++){
                                            			if(row.entity.$$hashKey===$scope.selectedRowData[k].$$hashKey){
                                            				cnt=k;
                                            				break;
                                            			}
                                            		}
                                            		$scope.selectedRowData.splice(cnt,1);
                                            	}
                                            });
                                            gridApi.grid.treeBase.tree.forEach(function(branch){
                                                branch.row.isSelected = false;
                                            });
                                        });*/
                                        // End function for selected row
                                        gridApi.expandable.on.rowExpandedStateChanged($scope, function (row) {
                                        	//row.expandableRowHeight = row.entity.subGridOptions.length * 30;
                                        	//row.expandableRowHeight = row.entity.expandedHeight;
                                            if (row.isExpanded) {
                                            	//alert(JSON.stringify(row.entity));
                                            	 // high-light the expanded row
                                            	//$scope.gridApi1.selection.selectRow(row.entity);
                                                // dynamically size the expansion pane
                                                //$scope.patientVisitGridOptions.expandableRowHeight = row.entity.expandedHeight;
                                                // collapse all other expanded rows
                                                for (var i = 0, r; r = this.grid.rows[i]; i++) {
                                                  if (r.isExpanded && r.uid !== row.uid) {
                                                	  $scope.gridApi1.expandable.toggleRowExpansion(r.entity);
                                                	  $scope.gridApi1.selection.unSelectRow(r.entity);
                                                  }
                                                }
                                              row.entity.subGridOptions = {
                                                showHeader: false,
                                                columnDefs: [
                                                             
                                                             {

                                                             	field: 'visitName', name: 'Visit',
                                                             	cellTemplate: '<div class="ui-grid-cell-contents ui-grid-header-cell-primary-focus"><span class="ui-grid-header-cell-label ng-binding" title="{{COL_FIELD}}">{{COL_FIELD}}</span></div>',
                                                             	//grouping: {groupPriority: 0},
                                                             	width: 129,
                                                             	pinnedLeft:true,
                                                             	enableCellEdit: false

                                                             },

                                                             {
                                                                 field: 'eventScheduledDate', name: 'Scheduled Date',
                                                                 type: 'date', cellFilter: 'date:"MM/dd/yyyy"',
                                                                 width: 160,
                                                                 enableCellEdit: false,
                                                                 cellTemplate:'<div ng-if="!row.groupHeader" class="ui-grid-cell-contents">' +
                                                                 '<span> {{COL_FIELD}} ' +
                                                                 '<span style="padding-left:15px;">' +
                                                                 '<a ng-href="#"   ng-click="grid.appScope.changeScheduleDate(row.entity)">' +
                                                                 '<span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>' +
                                                                 '</span></span></div>'

                                                             },													/*{
                                                                 field: 'addUnschEvent', name: '',
                                                                 cellTemplate: '<div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents"> <button ng-show="row.treeLevel==0" type="button" class="btn btn-success" style="padding: 4px 10px;" ng-click="grid.appScope.addScheduleEvent(row.treeNode.children[0],patient)">' +'+Unsch' +' </button> </div>'

                                                             },*/
                                                             {
                                                                 field: 'eventName',
                                                                 name: 'Event Name',
                                                                 cellTemplate: '<div class="ui-grid-cell-contents ui-grid-header-cell-primary-focus"><span class="ui-grid-header-cell-label ng-binding" title="{{COL_FIELD}}">{{COL_FIELD}}</span></div>',
                                                                 width: 160,
                                                                 enableCellEdit: false
                                                             },
                                                             {
                                                                 field: 'eventStatus.eventStatusCode.description',
                                                                 name: 'Event Status',
                                                                 width: 160,
                                                                 enableCellEdit: false,
                                                                 cellClass: function (grid, row, col, rowIndex, colIndex) {
                                                                     var val = grid.getCellValue(row, col);
                                                                     //alert(val);
                                                                     if (val === 'Done') {
                                                                         //Take note that I created a .grid .ui-grid-row .green css entry for this to work....
                                                                         return 'green';
                                                                     }
                                                                     else if (val === 'Not done') {
                                                                         return 'pink';
                                                                     }

                                                                 }
                                                             },
                                                             {field: 'eventStatus.statusValidFrom',
                                                                 name: 'Status Date',
                                                                 width: 160,
                                                                 type: 'date', cellFilter: 'date:"MM/dd/yyyy"',
                                                                 enableCellEdit: false
                                                             },

                                                             {
                                                                 field: 'coverageType.code',
                                                                 name: 'Coverage',
                                                                 width: 160,
                                                                 enableCellEdit: false


                                                             },
                                                             {field: 'eventStatus.notes',width: 185, name: 'Notes', enableCellEdit: false,}



                                                         ]};
                                            
                                              /*$http.get('https://cdn.rawgit.com/angular-ui/ui-grid.info/gh-pages/data/500_complex.json')
                                                .success(function(data) {
                                                  row.entity.subGridOptions.data = data;
                                                });*/
                                              //alert(row.uid);
                                              $scope.eventWindowLoad(row.entity);
                                              row.entity.subGridOptions.onRegisterApi = subGridApiRegister;
                                            }else {
                                            	$scope.gridApi1.selection.unSelectRow(row.entity);
                                            }
                                            /*$timeout(function () {
                                            	if(row.entity.subGridOptions.data.length>5)
                                            		row.expandedRowHeight = 150;
                                            	else
                                            	    row.expandedRowHeight = (row.entity.subGridOptions.data.length+1)*30;
                                        	    	$j(".ui-grid-viewport").height((row.entity.subGridOptions.data.length+1)*30);
                                            }, 3000);*/
                                        });
                                    },
                                    columnDefs: [
                                                 
                                        {

                                        	field: 'visitName', name: 'Visit',
                                        	cellTemplate: '<div class="ui-grid-cell-contents ui-grid-header-cell-primary-focus"><span class="ui-grid-header-cell-label ng-binding" title="{{COL_FIELD}}">{{COL_FIELD}}</span></div>',
                                        	//grouping: {groupPriority: 0},
                                        	width: 160,
                                        	pinnedLeft:true,
                                        	enableCellEdit: false

                                        },

                                        {
                                            field: 'eventScheduledDate', name: 'Scheduled Date',
                                            width: 160,
                                            enableFiltering:false,
                                            type: 'date', cellFilter: 'date:"MM/dd/yyyy"',
                                            enableCellEdit: false,
                                            cellTemplate:'<div ng-if="!row.groupHeader" class="ui-grid-cell-contents"> <button type="button" class="btn btn-success" style="padding: 4px 10px;" ng-click="grid.appScope.addScheduleEvent(row.entity)">' +'+Unsch' +' </button> </div>'

                                        },													/*{
                                            field: 'addUnschEvent', name: '',
                                            cellTemplate: '<div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents"> <button ng-show="row.treeLevel==0" type="button" class="btn btn-success" style="padding: 4px 10px;" ng-click="grid.appScope.addScheduleEvent(row.treeNode.children[0],patient)">' +'+Unsch' +' </button> </div>'

                                        },*/
                                        {
                                            field: 'eventName',
                                            name: 'Event Name',
                                            enableFiltering:false,
                                            cellTemplate: '<div class="ui-grid-cell-contents ui-grid-header-cell-primary-focus"><span class="ui-grid-header-cell-label ng-binding" title="{{COL_FIELD}}">{{COL_FIELD}}</span></div>',
                                            width: 160,
                                            enableCellEdit: false
                                        },
                                        {
                                            field: 'eventStatus.eventStatusCode.description',
                                            name: 'Event Status',
                                            width: 160,
                                            enableFiltering:false,
                                            enableCellEdit: false,
                                            cellClass: function (grid, row, col, rowIndex, colIndex) {
                                                var val = grid.getCellValue(row, col);
                                                if (val === 'Done') {
                                                    //Take note that I created a .grid .ui-grid-row .green css entry for this to work....
                                                    return 'green';
                                                }
                                                else if (val === 'Not done') {
                                                    return 'pink';
                                                }

                                            }
                                        },
                                        {field: 'eventStatus.statusValidFrom',
                                            name: 'Status Date',
                                            width: 160,
                                            enableFiltering:false,
                                            type: 'date', cellFilter: 'date:"MM/dd/yyyy"',
                                            enableCellEdit: false
                                        },

                                        {
                                            field: 'coverageType.code',
                                            name: 'Coverage',
                                            enableFiltering:false,
                                            width: 160,
                                            enableCellEdit: false


                                        },
                                        {field: 'eventStatus.notes',width: 185,enableFiltering:false, name: 'Notes', enableCellEdit: false,}



                                    ],
                                    data: []
                                };
                            	 $scope.$watch('data', function(value) {
                            	        if (value) {
                            	          for (var i = 0, val; val = value[i]; i++) {
                            	            var expandedHeight = 36;
                            	            if (val.subData.length) {
                            	              expandedHeight += val.subData.length * 30;
                            	            }
                            	            val.expandedHeight = expandedHeight;
                            	            val.subGridOptions = {
                            	              columnDefs: [{}], // we don't want the table, just the data
                            	              data: val.subData
                            	            };
                            	          }
                            	          $scope.patientVisitGridOptions.data = value;
                            	        }
                            	      });
                            	//$scope.patientVisitGridOptions.data=[];
                                $scope.selectedPatient = null;
                                //$scope.patientVisitGridOptions = null;
                               $scope.selectedRowData=[];
                               //If patient selected set patient visit grid
                               var hash = row.entity.$$hashKey;
                               var data = $scope.gridOptions.data;     // original rows of data
                               for (var ndx = 0; ndx < data.length; ndx++) {
                                   if (data[ndx].$$hashKey == hash) {
                                	   $scope.selectedRowEntity=ndx;
                                       break;
                                   }
                               }
                                $scope.selectedPatient = row.entity;
                                $scope.visitWindowLoad(row.entity);
                                $scope.studyPatRights = row.entity.studyPatRights;
                                $scope.orgRights	  = row.entity.orgRights;
                                //$scope.patientVisitGridOptions = row.entity.subGridOptions;
                                //Show only patId first/last name
                                $scope.gridOptions.columnDefs[3].visible = false;
                                $scope.gridOptions.columnDefs[4].visible = false;
                                $scope.gridOptions.columnDefs[5].visible = false;
                                $scope.gridOptions.columnDefs[6].visible = false;
                                $scope.gridOptions.columnDefs[7].visible = false;
                                $scope.gridOptions.columnDefs[8].visible = false;
                                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);

                            }
                            else {
                                $scope.selectedPatient = null;
                                $scope.selectedRowData=[];
                                $scope.patientID='';
                                $scope.patientVisitGridOptions = null;
                                $scope.expandedRowEntity=-1;
                                $scope.selectedRowEntity=-1;
                                $scope.orgRights	    = 0;
                                $scope.gridOptions.columnDefs[3].visible = true;
                                $scope.gridOptions.columnDefs[4].visible = true;
                                $scope.gridOptions.columnDefs[5].visible = true;
                                $scope.gridOptions.columnDefs[6].visible = true;
                                //TODO: Making the following two false until the date source is identified.
                                $scope.gridOptions.columnDefs[7].visible = true;
                                $scope.gridOptions.columnDefs[8].visible = true;
                                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
                            }
                        });
                    /*//Sub grid options - Will only be run on row expand.
                     gridApi.expandable.on.rowExpandedStateChanged($scope, function (row) {
                     if (row.isExpanded) {
                     var rowData = {
                     eventScheduledDate:"2010-12-12",
                     eventName:"TestEvent",
                     visitName:"Visit1"
                     }
                     row.entity.subGridOptions.data = rowData;
                     }
                     });*/
                },
                columnDefs: [
                    {
                        field: 'studyPatId', name: 'Patient Study Id',
                        enableCellEdit: false,
                        //sort: {priority: 2, direction: 'asc'}
                    },
                    {
                        field: 'studyPatFirstName', name: 'First Name',
                        cellTooltip: function (row, col) {
                            return 'Name: ' + row.entity.studyPatFirstName + " " + row.entity.studyPatLastName;
                        },enableCellEdit: false
                    },
                    {field: 'studyPatLastName', name: 'Last Name',enableCellEdit: false},
                    {
                        field: 'studyPatEnrollDate', name: 'Enrolled Date',
                        enableFiltering:false,
                        type: 'date', cellFilter: 'date:"MM/dd/yyyy"',
                        enableCellEdit: false
                    },
                    {
                        field: 'studyPatEnrollingSite.siteName', name: 'Enrolling Site',
                        sort: {priority: 1, direction: 'asc'},
                        enableCellEdit: false
                    },
                    {
                        field: 'studyPatStatus.description', name: 'Current Status',
                        cellTemplate: '<div class="ui-grid-cell-contents">' +
                        '<div style="float:left; width: 170px;"> <a href="#" title="{{COL_FIELD}}"  ng-click="grid.appScope.openPatStatModal(row.entity.currentStudyPK,row.entity.patientIdentifier.PK,row.entity.patientIdentifier.patientId,row.entity.studyPatId,row.entity.studyPatStatId.PK,\'new\',row.entity.studyPatRights,row.entity.orgRights)"> {{COL_FIELD.length>20 ? COL_FIELD.substring(0,20)+"..." : COL_FIELD}} </a> ' +
                        '</div>'+
                        '<div><a href="#"  ng-click="grid.appScope.openPatStatModal(row.entity.currentStudyPK,row.entity.patientIdentifier.PK,row.entity.patientIdentifier.patientId,row.entity.studyPatId,row.entity.studyPatStatId.PK,\'edit\',row.entity.studyPatRights,row.entity.orgRights)">' +
                        '<span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></div>' +
                        '</div>',
                        width: 200,
                        enableCellEdit: false
                    },
                    {field: 'studyPatAssignedTo.userLoginName', name: 'Assigned To',enableCellEdit: false},
                    {field: 'studyPatLastVisit', name: 'Last Visit',enableCellEdit: false},
                    {field: 'studyPatNextDue', name: 'Next Due',enableFiltering:false,type: 'date', cellFilter: 'date:"MM/dd/yyyy"',enableCellEdit: false},
                    
                ]
            };


//Get Study Patients
            $scope.getStudyPatients = function (studyName) {
                //Get Study Patients
                getPatients(studyName);

            };

//REST call to get study Patients
            var getPatients = function (studyName) {
            	showWarningChange();
            	$scope.pageLoaderShow();
            	if($scope.allSurvivalStatList.length<=0)
            		$scope.GetCodeListData('patient_status');
            	if($scope.allEventStatusList.length<=0)
            		$scope.GetCodeListData('eventstatus');
                //Get study Details
                // <%--Angular REST request--%>
                // <%--Create post request--%>
                //need to replace static code with angular service
                var req = {
                    method: 'POST',
                    url: nodeURL+'/study/getStudy',
                    headers: {
                        'Content-Type': 'application/json;charset=UTF-8'
                    },
                    data: {args:{
                        StudyIdentifier: {
                            studyNumber: studyName
                        }},
                        "jwtToken": jwttoken
                    }
                };

                $http(req).then(function (response) { //success
                    var ucsdStudy = response.data.Study;
                    
                    var jsonObj = {};
                    if (response.data.Study.studyOrganizations != null) {
                       	//$scope.organizationList = response.data.Study.studyOrganizations.studyOrganizaton;
                   	 $scope.organizationList =[];
                   	 for (var i = 0, len = response.data.Study.studyOrganizations.studyOrganizaton.length; i < len; i++) {
                            if (response.data.Study.studyOrganizations.studyOrganizaton[i].organizationIdentifier) {
                                //alert(response.data.Study.studyOrganizations.studyOrganizaton[i].organizationIdentifier.PK);
                               jsonObj = {}
                               jsonObj = {"Id": response.data.Study.studyOrganizations.studyOrganizaton[i].organizationIdentifier.PK,
                               		 		"Name": response.data.Study.studyOrganizations.studyOrganizaton[i].organizationIdentifier.siteName};
                               $scope.organizationList.push(jsonObj);
                            }
                            }	                                     
                       }
                    // <%--Angular REST request--%>
                    // <%--Create post request--%>
                    //need to replace static code with angular service
                    var req = {
                        method: 'POST',
                        url: nodeURL+'/StudyPatient/GetStudyPatients',
                        headers: {
                            'Content-Type': 'application/json;charset=UTF-8'
                        },
                        data: {args:{
                        	StudyPatientSearch:{
                        	pageNumber:$scope.currentPage,
                        	pageSize:$scope.pageSize,
                        	sortBy:$scope.sortBy,
                        	sortOrder:$scope.sortOrder,
                        	searchBy:{
                        	searchByColumn:$scope.searchByCols,
                        	value:$scope.searchByColsVal
                        	},
                        	studyIdentifier: {
                            studyNumber: studyName
                        			}
                        		}
                            },
                            "jwtToken": jwttoken
                        }
                    };

                    $http(req).then(function (response) { 
                    	//success
                        var patientList = "";
                        if (response.data.StudyPatients != null) {
                        	patientList = response.data.StudyPatients.studyPatient;
                        	}
                        if(patientList==null || patientList==""){
                         	if(response.data.body){
                      			var errorType = JSON.stringify(response.data.body);
                      			var firstIndex = errorType.indexOf('<message>');
                      			var lastIndex = errorType.indexOf('</message>');
                      			if(firstIndex>0)
                      				errorType = errorType.slice(firstIndex+9,lastIndex)
                      			else{
                          				firstIndex = errorType.indexOf('<faultstring>');
                              			lastIndex = errorType.indexOf('</faultstring>');
                              			if(firstIndex>0)
                              				errorType = errorType.slice(firstIndex+13,lastIndex)
                              			
                          			}
                          			if('Patient not found'===errorType && $scope.searchByCols===''){
                                       	alert("No patients enrolled to the study.");
                          			}
                          			else if('Patient not found'===errorType){
                          				alert('Patient not found.');
                          			}
                          			else{
                          				$scope.currentPage = 1;
                          				$scope.pageSize = paginationOptions.pageSize;
                          				$scope.gridOptions.totalItems = 0;
                          				alert(errorType);
                          			}
                     		 }
                         	$scope.pageLoaderHide();
                  			$(".btn").removeClass('disabled');
                  			return false;
                        	}else{
                        //Get additional data for the patient,
                        //The grid options.data will be used there.
                        //Get the patient status history and enroll site details
                        //loop through each patient row in the grid
                        //alert(JSON.stringify(paginationOptions.sortColumn));
                        $scope.pageSize = response.data.StudyPatients.pageSize;
                        $scope.currentPage = response.data.StudyPatients.pageNumber;
                        $scope.gridOptions.totalItems = response.data.StudyPatients.totalCount;
                        //var firstRow = (paginationOptions.pageNumber - 1) * paginationOptions.pageSize;
                        //$scope.gridOptions.data = data.slice(firstRow, firstRow + paginationOptions.pageSize);
                        /*uniquePatList=[];
						uniquePatStdIdList=[];
						angular.forEach(patientList, function (patient, patientKey) {
						if($.inArray(patient.patientIdentifier.patientId, uniquePatStdIdList) == -1){
                        		uniquePatList.push(patient);
								uniquePatStdIdList.push(patient.patientIdentifier.patientId);
                        	  }
						})
						patientList = uniquePatList;*/
                        angular.forEach(patientList, function (patient, patientKey) {
                            //REST call to get ALL patient status details
                            // <%--Angular REST request--%>
                            // <%--Create post request--%>
                            //need to replace static code with angular service
                        	patient.currentStudyPK = ucsdStudy.studyIdentifier.PK;
                                                    //Check if current status data is available
                                                        //Populate patient with additional status details
                                                        patient.currentStatusPK = patient.studyPatStatId.PK;
                                                        //Assigned to
                                                        /*if (patient.hasOwnProperty('studyPatAssignedTo')) {

                                                            patient.AssignedTo =
                                                                patient.studyPatAssignedTo.userLoginName;

                                                        }

                                                        //Enrolling Site
                                                        if (patient.hasOwnProperty('studyPatEnrollingSite')) {
                                                            if (patient.studyPatEnrollingSite.hasOwnProperty('siteName')) {
                                                                patient.EnrollingSite = patient.studyPatEnrollingSite.siteName;
                                                            }
                                                        }

                                                        //Status Date
                                                        if (patient.hasOwnProperty('studyPatEnrollDate')) {

                                                            patient.EnrolledDate =
                                                                new Date(patient.studyPatEnrollDate);
                                                        }

                                                        //Current Status
                                                        if (patient.hasOwnProperty('studyPatStatus')) {

                                                            patient.CurrentStatus =
                                                            	patient.studyPatStatus.description;
                                                        }*/                                             
                                                    patient.studyPK=ucsdStudy.studyIdentifier.PK;
                                              
                                 });

                        //Populate table as soon as the initial data is obtained
                        $scope.pageLoaderHide();
                        $scope.gridOptions.data = patientList;
                        $scope.soapStatus = "Row count: " + $scope.gridOptions.data.length;
                        $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
               		 	$scope.gridApi.core.refresh();
               		    $scope.gridOptions.grid.refresh();
                        $timeout( function(){$scope.gridApi.grid.refresh();}, 2000 );
                        if($scope.selectedRowEntity !== -1){
                        $scope.gridApi.grid.registerDataChangeCallback(function(data)
                        		 {
                        			$scope.gridApi.selection.selectRow($scope.gridOptions.data[$scope.selectedRowEntity]);
                        		 }, [uiGridConstants.dataChange.ROW]);}
                    	}
                    }, function (response) { //error

                        $scope.status = response.status;
                    });
                }, function (response) { //error
                    $scope.status = response.status;
                });
            };

    //Save grid display options
                $scope.saveState = function () {
                    $scope.gridState = $scope.gridApi.saveState.save();
                };

    //Restore grid display options from save
                $scope.restoreState = function () {
                    $scope.gridApi.saveState.restore($scope, $scope.gridState);
                };

//Clear the Ui- Gird of all the data
            $scope.ClearData = function () {
                $scope.soapStatus = $scope.gridOptions.data;
                $scope.gridOptions.rowEditWaitInterval = $scope.gridOptions.rowEditWaitInterval * -1;
                $scope.soapStatus = $scope.gridOptions.data;

                //$scope.gridOptions.columnDefs = [];
            };

            $scope.TiggerSaveData = function () {
                $scope.gridApi.rowEdit.flushDirtyRows($scope.gridApi.grid);
            };

            /*
             *
             * Function to convert the gender value from the spreadsheet to
             * the complex type object in the code list.
             *
             * */
            var getGender = function (gender) {
                for (var i = 0, len = $scope.genderList.length; i < len; i++) {
                    if ($scope.genderList[i].code === gender) {
                        return $scope.genderList[i];
                    }
                }
            };

            /*
             * Update patient demographics data
             *  Can be expanded to handle update or create.
             *  API has a create patient option that we can leverage
             *  -(create not implemented as of 10/21/16)
             * */
            $scope.saveRow = function (rowEntity) {
                $scope.soapStatus = "New row being saved....wait!";
                // create a fake promise - normally you'd use the promise returned by $http or $resource
                var promise = $scope.updatePatient(rowEntity);
                $scope.gridApi.rowEdit.setSavePromise(rowEntity, promise.promise);
            };

            /* Code to update the Patient Demographics using SOAP */
            $scope.updatePatient = function (rowEntity) {
            	showWarningChange();
                //Convert Gender to gender code type
                /*var gender = getGender(rowEntity.Gender);*/
                // <%--Angular REST request--%>
                // <%--Create post request--%>
                var req = {
                    method: 'POST',
                    url: nodeURL+'/PatientDemographics/updatePatient',
                    headers: {
                        'Content-Type': 'application/json;charset=UTF-8'
                    },
                    data: { args:{
                        PatientIdentifier: {
                            PK: rowEntity.patientIdentifier.PK,
                            organizationId: {
                                siteName: rowEntity.EnrollingSite
                            }
                        },
                        PatientDemographics: {
                            firstName: rowEntity.studyPatFirstName,
                            lastName: rowEntity.studyPatLastName,
                            reasonForChange: "Study Coordinator page edit"
                        }},
                        "jwtToken": jwttoken
                    }
                };

                var soapReply = $q.defer();

                $http(req).then(function (response) { //success
                    var soapData = response.data;
                    try {
                        if (soapData.ResponseHolder.issues === null) {
                            $scope.soapStatus = soapData;
                            return soapReply.resolve();
                        }
                        else {
                            $scope.soapStatus = soapData;
                            return soapReply.reject()
                        }
                    }
                    catch ($scope) {
                        $scope.soapStatus = soapData;
                        return soapReply.reject()
                    }
                }, function (response) { //error
                    $scope.soapStatus = response.data;
                    soapReply.reject();
                });

                return soapReply;
            };


            $scope.showSelectValue = function(selectedValue) {
            	$scope.selectedCalendar=selectedValue;
            }
			         
            $scope.createAndEnrollPatientToStudy=function (selectedStudy,selectedPatient){
            	$scope.pageLoaderShow();
            	showWarningChange();            	
            	 var patId = angular.element(document.getElementById("patientId")); 
             	 $scope.patientId = patId.val();
             	 var dob = angular.element(document.getElementById("patDateofBirth"));
             	 $scope.patientDOB = dob.val();
             	firstName=$('#firstname').val();
            	lastName=$('#lastname').val();
             	 $(".btn").addClass('disabled');
             	 //alert($scope.selectedPatStatus+' '+selectedPatient.studyStatusDate+' '+$scope.selectedOrganization+' '+selectedPatient.patDateofBirth+' '+$scope.selectedSurStatus+' '+selectedPatient.patientId+' '+$scope.selectedEthnicity+' '+$scope.selectedRace+' '+$scope.selectedGender);
             	if((!angular.isDefined($scope.selectedPatStatus) || $scope.selectedPatStatus=='') || ($scope.studyStatusDate=='' || angular.isUndefined($scope.studyStatusDate)) || $scope.selectedOrganization == '' || $scope.patientDOB=='' || $scope.selectedSurStatus=='' || $scope.patientId==''){
		    			 alert('Please Enter Data in All Mandatory Fields.');
		    			 $(".btn").removeClass('disabled');
		    			 $scope.pageLoaderHide();
		    			 return false;
		    		 }
             	if((($scope.selectedCalendar!='' && !angular.isUndefined($scope.selectedCalendar)) && ($scope.calStartDate=='' || angular.isUndefined($scope.calStartDate))) || (($scope.selectedCalendar==null || $scope.selectedCalendar=='' || angular.isUndefined($scope.selectedCalendar)) && ($scope.calStartDate!='' && !angular.isUndefined($scope.calStartDate)))){
             		 alert('Please Select Calendar and Calendar Start Date.');
	    			 $(".btn").removeClass('disabled');
	    			 $scope.pageLoaderHide();
	    			 return false;
             	}
             	if ($scope.compareDates($scope.patientDOB,$scope.studyStatusDate)){
    				alert ("Status Date should not be less than Birth Date");/*alert ("Status Date should not be less than Birth Date");*****/
    				$(".btn").removeClass('disabled');
	    			$scope.pageLoaderHide();
    				return false;
    			}
             	if($scope.selectedSurStatus!=='' && $scope.selectedSurStatus==='dead' && (selectedPatient.deathDate=='' || !angular.isDefined(selectedPatient.deathDate))){
             		 alert("Death Date is required when Patient's survival status is given as 'Dead'.");
	    			 $(".btn").removeClass('disabled');
	    			 $scope.pageLoaderHide();
	    			 return false;
             	}
             	if($scope.selectedPatStatus==='active' || $scope.selectedPatStatus==='followup' || $scope.selectedPatStatus==='offtreat' || $scope.selectedPatStatus==='offstudy' || $scope.selectedPatStatus==='lockdown'){
             		 alert("The patient is not currently enrolled. To enter this status, please enter an 'Enrolled' status first.");
	    			 $(".btn").removeClass('disabled');
	    			 $scope.pageLoaderHide();
	    			 return false;
             		}
            	 var req = {
                         method: 'POST',
                         url: nodeURL+'/study/getStudy',
                         headers: {
                             'Content-Type': 'application/json;charset=UTF-8'
                         },
                         data: {args:{
                             StudyIdentifier: {
                                 studyNumber: selectedStudy.studyNumber
                             }},
                             "jwtToken": jwttoken
                         }
                     };
            	 
            	 $http(req).then(function (response) {
            		 var studyData=response.data.Study;
            		 var req = {};
            		 if($scope.selectedSurStatus==='dead'){
            		  req = {
                              method: 'POST',
                              url: nodeURL+'/StudyPatient/createAndEnrollPatientToStudy',
                              headers: {
                                  'Content-Type': 'application/json;charset=UTF-8'
                              },
                              data: { args: {
                                  "Patient": {"patientDemographics":{"firstName":firstName,"lastName":lastName,"dateOfBirth":$filter('date')(new Date($scope.patientDOB), "yyyy-MM-dd"),"organization":{"PK": $scope.selectedOrganization.Id,"siteName": $scope.selectedOrganization.Name},
                            	  "survivalStatus":{"code":$scope.selectedSurStatus,"type":"patient_status"},"deathDate":$filter('date')(new Date(selectedPatient.deathDate), "yyyy-MM-dd"),
                            	  "patientCode": $scope.patientId,"middleName":selectedPatient.patmname,"address1":selectedPatient.patadd1,"city":selectedPatient.patcity,"state":selectedPatient.patstate,"county":selectedPatient.patcounty,"zipCode":selectedPatient.patzip,"country":selectedPatient.patcountry,"SSN":selectedPatient.patssn,"homePhone":selectedPatient.pathphone},
                            	  "patientIdentifier":{"patientId": $scope.patientId}},
                                  "StudyIdentifier": {"PK": studyData.studyIdentifier.PK},
                                  "PatientEnrollmentDetails": {
                                      "currentStatus": "1",
                                      "enrolledBy": {"PK": userId}, //Currently set to psahai for demo TODO:Replace with current user
                                      "enrollingSite": {"PK": $scope.selectedOrganization.Id}, //TODO:replace with current org
                                      "patientStudyId": $scope.patientStudyId,
                                      "status": {"code": $scope.selectedPatStatus  , "type": "patStatus"},
                                      "statusDate": $filter('date')(new Date($scope.studyStatusDate), "yyyy-MM-dd")
                                  }},
                                  "jwtToken": jwttoken,
                                  "remoteaddr": ipAdd
                              }
                          };
            		 }else{
            			 req = {
                                 method: 'POST',
                                 url: nodeURL+'/StudyPatient/createAndEnrollPatientToStudy',
                                 headers: {
                                     'Content-Type': 'application/json;charset=UTF-8'
                                 },
                                 data: { args: {
                                     "Patient": {"patientDemographics":{"firstName":firstName,"lastName":lastName,"dateOfBirth":$filter('date')(new Date($scope.patientDOB), "yyyy-MM-dd"),"organization":{"PK": $scope.selectedOrganization.Id,"siteName": $scope.selectedOrganization.Name},
                                	 "survivalStatus":{"code":$scope.selectedSurStatus,"type":"patient_status"},
                                	 "patientCode": $scope.patientId,"middleName":selectedPatient.patmname,"address1":selectedPatient.patadd1,"city":selectedPatient.patcity,"state":selectedPatient.patstate,"county":selectedPatient.patcounty,"zipCode":selectedPatient.patzip,"country":selectedPatient.patcountry,"SSN":selectedPatient.patssn,"homePhone":selectedPatient.pathphone},
                                	 "patientIdentifier":{"patientId": $scope.patientId}},
                                     "StudyIdentifier": {"PK": studyData.studyIdentifier.PK},
                                     "PatientEnrollmentDetails": {
                                         "currentStatus": "1",
                                         "enrolledBy": {"PK": userId}, //Currently set to psahai for demo TODO:Replace with current user
                                         "enrollingSite": {"PK": $scope.selectedOrganization.Id}, //TODO:replace with current org
                                         "patientStudyId": $scope.patientStudyId,
                                         "status": {"code": $scope.selectedPatStatus  , "type": "patStatus"},
                                         "statusDate": $filter('date')(new Date($scope.studyStatusDate), "yyyy-MM-dd")
                                     }},
                                     "jwtToken": jwttoken,
                                     "remoteaddr": ipAdd
                                 }
                             };
            		 }
            		 if($scope.selectedEthnicity!='')
            			 req.data.args.Patient.patientDemographics.ethnicity={"code":$scope.selectedEthnicity,"type":"ethnicity"};
            		 if($scope.selectedRace!='')
            			 req.data.args.Patient.patientDemographics.race={"code":$scope.selectedRace,"type":"race"};
            		 if($scope.selectedGender!='')
            			 req.data.args.Patient.patientDemographics.gender={"code":$scope.selectedGender,"type":"gender"};
            		     $http(req).then(function (response) {
            		    	 if (response.data.Response != null
                                     && response.data.Response.results != null) {
            		    		 $scope.pageLoaderShow();
            		    		 $('#myModal').modal('hide');
            		    		 $scope.soapStatus =
                                     "Patient is created and enrolled:" + $scope.patientId;
            		    		 if(($scope.selectedCalendar==null || $scope.selectedCalendar=='') || ($scope.calStartDate=='' || angular.isUndefined($scope.calStartDate))){
            		    			 $scope.pageLoaderHide();
            		    			 $(".btn").removeClass('disabled');
            		    			 $scope.pageSize = paginationOptions.pageSize;
            		    			 getPatients(selectedStudy.studyNumber);
            		    			 return false;
            		    		 }
             		    	//generate Schedule 
             		    	var req = {
             		    			 method: 'POST',
                                      url: nodeURL+'/PatientSchedule/addMPatientSchedules',
                                      headers: {
                                          'Content-Type': 'application/json;charset=UTF-8'
                                      },
                                      data: {args: {
                                    	  "PatientSchedules":{
                                      	                        "mPatientSchedule":[{
                                      	                        "patientIdentifier":{"patientId":$scope.patientId,"organizationId":{"PK":$scope.selectedOrganization.Id,"siteName":$scope.selectedOrganization.Name}},
                                      	                        "studyIdentifier":{"PK": studyData.studyIdentifier.PK,"studyNumber":selectedStudy.studyNumber},
                                      	                        "calendarIdentifier":{"PK":$scope.selectedCalendar},
                                      	                        "startDate": $filter('date')(new Date($scope.calStartDate), "yyyy-MM-dd")
                                      	                        }]
                                      	                    }},
                                                            "jwtToken": jwttoken,
                                                            "remoteaddr": ipAdd
                                          
                                      }
             		    	};
             		    	$http(req).then(function (response) {
 	                        	if(response.data.addmpatientschedules.mPatientSchedule){
 	                        		$scope.soapStatus = "Schedule generated successfully for:"+$scope.patientId;
 	                		    	$scope.pageLoaderHide();
 	                		    	$scope.pageSize = paginationOptions.pageSize;
 	                		    	getPatients(selectedStudy.studyNumber);
 	                		    	$scope.PatDialogClose();
 	                		    	$(".btn").removeClass('disabled');
 	                        	}else{
 	                        		alert('Unable to Genrate Schedule');
 	                        		$scope.pageLoaderHide();
 	                        		$scope.pageSize = paginationOptions.pageSize;
 	                		    	getPatients(selectedStudy.studyNumber);
 	                		    	$scope.PatDialogClose();
 	                        		$(".btn").removeClass('disabled');
 	                        	}
 	                        	
             		    	},function (response) { //error
             		    			$scope.soapStatus = "Unable to Genrate Schedule";
             		    			$scope.pageLoaderHide();
	                        		$scope.pageSize = paginationOptions.pageSize;
	                		    	getPatients(selectedStudy.studyNumber);
	                		    	$scope.PatDialogClose();
	                        		$(".btn").removeClass('disabled');
	                        		$scope.status = response.status;
                                 //getPatients(selectedStudy.studyNumber);
                             });
             		    	// End Schedule
             		    	$scope.PatDialogClose();
             		    	$scope.pageLoaderHide();
             		    	$(".btn").removeClass('disabled');
	                        //getPatients(selectedStudy.studyNumber);
            		     } else {
                         	if(response.data.body){
                      			var errorType = JSON.stringify(response.data.body);
                      			var firstIndex = errorType.indexOf('<message>');
                      			var lastIndex = errorType.indexOf('</message>');
                      			if(firstIndex>0)
                      				errorType = errorType.slice(firstIndex+9,lastIndex)
                      			else{
                          				firstIndex = errorType.indexOf('<faultstring>');
                              			lastIndex = errorType.indexOf('</faultstring>');
                              			if(firstIndex>0)
                              				errorType = errorType.slice(firstIndex+13,lastIndex)
                              			
                          			}
                          			if('Failed to addPatientStudyStatus'===errorType)
                                       	 alert("The patient is not currently enrolled. To enter this status, please enter an 'Enrolled' status first.");
                    		    	else if('java.lang.NullPointerException'===errorType)
                          				alert('Enrolling Site is not in list of Study Organizations where user can enroll patient');
                          			else if('EnrollingSite is not in list of Study Organizations where user can enroll patient'===errorType)
                          				alert('Enrolling Site is not in list of Study Organizations where user can enroll patient');
                          			else
                          				alert(errorType);
                          			$scope.pageLoaderHide();
                          			$(".btn").removeClass('disabled');
                          			//getPatients(selectedStudy.studyNumber);
                     		 }else{
                            $scope.soapStatus = "Unable to enroll patient...Already Enrolled?";
                            $scope.pageLoaderHide();
                            $(".btn").removeClass('disabled');
                            //getPatients(selectedStudy.studyNumber);
                     		 }
                        }
                         }, function (response) { //error
                             $scope.status = response.status;
                             $scope.soapStatus = "Unable to enroll patient...Already Enrolled?";
                             getPatients(selectedStudy.studyNumber);
                         });
            		 
            		 
            	 }, function (response) { //error
                     $scope.status = response.status;
                     $scope.pageLoaderHide();
                     $(".btn").removeClass('disabled');
                 });
            	
            };
            
            
            $scope.addScheduleEventStatus=function (){
            	showWarningChange();
            	if($scope.selectedRowData.length<=0){
            		alert('Please select an event to edit.');
            		return false;
            	}
            	$(".btn").addClass('disabled');
            	if($scope.selectedEventStatus == '' || !angular.isDefined($scope.selectedEventStatus) || $scope.statusDate == '' || !angular.isDefined($scope.statusDate)){
         		   alert('Please enter data in Mandatory fields.');
         		   $(".btn").removeClass('disabled');
         		   return false;
         	   }
            	$scope.updatePatScheduleStatuses();
           };
           
           $scope.updatePatScheduleStatuses=function(){
               	$('#eventPowerBarModal').modal('hide');
             	$(".btn").removeClass('disabled');
             	$scope.pageLoaderShow();
             	var urlArgs=""
             		urlArgs = "enrolledId="+$scope.selectedPatient.patprotID+"&remarks=Patient+Roster+Changed&eSign="+eSign;
             	var notes = '';
             	if(angular.isDefined($scope.eventNotes))
             			notes = $scope.eventNotes;
               for(var i=0;i<$scope.selectedRowData.length;i++){
            	   /*$.ajax({
						type        : 'POST',
						url     : nodeURL+'/PatientSchedule/addScheduleEventStatus',
						data: {args:{
                            EventIdentifier: $scope.selectedRowData[i].scheduleEventIdentifier,
                            "EventStatus":
            							{
                							"eventStatusCode":
                								{
                    								"code":$scope.selectedEventStatus,
                    								"type":"eventstatus"
                								},
                							"notes":$scope.eventNotes,
                							"reasonForChange":"Patient Roster Changed",
                							"statusValidFrom":$filter('date')($scope.statusDate, "yyyy-MM-dd")
             							}},
                            "jwtToken": jwttoken},
						async: false,
						cache: false,
						//dataType: 	"json",
						//contentType : "application/json;charset=UTF-8",                        
						success     : function(response) {
                            	if(response.body){
                         			var errorType = JSON.stringify(response.body);
                         			var firstIndex = errorType.indexOf('<message>');
                         			var lastIndex = errorType.indexOf('</message>');
                         			if(firstIndex>0)
                         				errorType = errorType.slice(firstIndex+9,lastIndex)
                         			alert(errorType);
                         			$(".btn").removeClass('disabled');
                         			return false;
                        		 }else{
                      		$scope.soapStatus = "Bulk Status Updated Successfully.";
                      	 	}
                      		//deffered.resolve('request successful');
                      	 
										},
						error       : function(r) {
											$scope.status = "Bulk Status Updated Failure."; 
											}

					});*/
            	   if(!angular.isDefined($scope.selectedRowData[i].coverageType.code))
            		   urlArgs = urlArgs +"&selEvent"+i+"=on"+"&eventStatus"+i+"="+eventStatusJson[$scope.selectedEventStatus]+"&eventId"+i+"="+$scope.selectedRowData[i].scheduleEventIdentifier.PK+"&oldStatus"+i+"="+eventStatusJson[$scope.selectedRowData[i].eventStatus.eventStatusCode.code]+"&notes"+i+"="+notes+"&serviceSite"+i+"="+"&statusValid"+i+"="+$filter('date')($scope.statusDate, appDateFormat)+"&covType"+i+"=&covTypeFor"+i+"=";
            	   else
            		   urlArgs = urlArgs +"&selEvent"+i+"=on"+"&eventStatus"+i+"="+eventStatusJson[$scope.selectedEventStatus]+"&eventId"+i+"="+$scope.selectedRowData[i].scheduleEventIdentifier.PK+"&oldStatus"+i+"="+eventStatusJson[$scope.selectedRowData[i].eventStatus.eventStatusCode.code]+"&notes"+i+"="+notes+"&serviceSite"+i+"="+"&statusValid"+i+"="+$filter('date')($scope.statusDate, appDateFormat)+"&covType"+i+"="+eventCovTypeJson[$scope.selectedRowData[i].coverageType.code]+"&covTypeFor"+i+"="+eventCovTypeJson[$scope.selectedRowData[i].coverageType.code];

          		}
               $j.ajax({
           		url:"updateEvtVisitPatSched.jsp",
           		type:"POST",
           		async:false,
           		data:urlArgs,
           		dataType:"json",
           		success: function(response) {
            	   $scope.selectedEventStatus='';
             		 $scope.eventNotes='';
             		 $scope.statusDate='';
             		 $scope.selectedRowData=[];
             		 //$scope.patientVisitGridOptions.data=[];
             		//var myPromise = $scope.visitWindowLoad($scope.selectedPatient);

        		    // wait until the promise return resolve or eject
        		    //"then" has 2 functions (resolveFunction, rejectFunction)
        		   // myPromise.then(function(resolve){
        		    	if($scope.expandedRowEntity){
        		    		$scope.gridApi1.expandable.toggleRowExpansion($scope.gridApi1.grid.rows[$scope.expandedRowEntity].entity);
        		    	$timeout(function(){$scope.gridApi1.expandable.toggleRowExpansion($scope.gridApi1.grid.rows[$scope.expandedRowEntity].entity);}, 100);
        		    		}else if($scope.expandedRowEntity !== -1){
        		    			$scope.gridApi1.expandable.toggleRowExpansion($scope.gridApi1.grid.rows[$scope.expandedRowEntity].entity);
        		    			$timeout(function(){$scope.gridApi1.expandable.toggleRowExpansion($scope.gridApi1.grid.rows[$scope.expandedRowEntity].entity);}, 100);
        		    		}
        		        /*}, function(reject){
        		        //alert(reject)      
        		    });*/
             		 $(".ui-grid-all-selected").removeClass("ui-grid-all-selected");
             		 $scope.gridApi1.grid.treeBase.tree.forEach(function(branch){
                          branch.row.isSelected = false;});
             		 $scope.gridApi1.grid.notifyDataChange(uiGridConstants.dataChange.COLUMN);
             		 $scope.gridApi1.grid.refresh();
               },
           		error:function(data) { alert(data.statusText); return false; }
           			});
           };
          
           $scope.dialogClose = function(){
        	   showWarningChange();
        	   $(".btn").removeClass('disabled');
        	    $scope.PatStatDialogClose();
        	    $scope.eventStatDialogClose();
        	    $('#getPatientData').modal('hide');
        	    $('#myModal').modal('hide');
        	    $scope.buttonFlag=0;
            	$scope.buttonFLNFlag=0;
        		$scope.patientId='';
        		$scope.searchedPatient.patFirstName='';
        		$scope.searchedPatient.patLastName='';
        		$scope.patientStudyId='';
        		$scope.searchedPatient.patDateofBirth='';
        		$scope.studyStatusDate='';
        		$scope.selectedOrganization = '';
        		$scope.calStartDate='';
        		$scope.selectedEthnicity='';
        		$scope.selectedRace='';
        		$scope.selectedGender='';
           }
           $scope.addPatStatus = function(){
        	   showWarningChange();
        	   $(".btn").addClass('disabled');
        	   if($scope.selectedPatStatus == '' || $scope.patstatusDate == '' || $scope.selectedOrganization == ''){
        		   alert('Please enter data in Mandatory fields.');
        		   $(".btn").removeClass('disabled');
        		   return false;
        	   }
        	   var req = {
                       method: 'POST',
                       url: nodeURL+'/StudyPatient/addStudyPatientStatus',
                       headers: {
                           'Content-Type': 'application/json;charset=UTF-8'

                       },
                       data: {args:{PatientStudyStatus:{currentStatus:"true",enrollingSite:{PK:$scope.selectedOrganization.Id},
                    	   "enrolledBy": {"PK": userId}, notes:$scope.patNotes,reasonForChange:'Study Patient Roster',
                    	   status:{code:$scope.selectedPatStatus,type:'patStatus'
                    	   },statusDate:$filter('date')(new Date($scope.patstatusDate), "yyyy-MM-dd")},
                    	   PatientIdentifier:{PK:$scope.patPK},
                    	   StudyIdentifier: {PK:$scope.studyPK}},
                           "jwtToken": jwttoken,
                           "remoteaddr": ipAdd
                       }
                   };
          	 
          	 $http(req).then(function (response) {
          		if(response.data.body){
          			var errorType = JSON.stringify(response.data.body);
          			var firstIndex = errorType.indexOf('<message>');
          			var lastIndex = errorType.indexOf('</message>');
          			if(firstIndex>0)
          				errorType = errorType.slice(firstIndex+9,lastIndex)
          			else{
          				firstIndex = errorType.indexOf('<faultstring>');
              			lastIndex = errorType.indexOf('</faultstring>');
              			if(firstIndex>0)
              				errorType = errorType.slice(firstIndex+13,lastIndex)
              			
          			}
          			if('Failed to addPatientStudyStatus'===errorType)
          				alert('\'Enrolled\' status for this patient already exists. Please Enter a different status.');
          			else if('java.lang.NullPointerException'===errorType)
          				alert('Enrolling Site is not in list of Study Organizations where user can enroll patient');
          			else if('EnrollingSite is not in list of Study Organizations where user can enroll patient'===errorType)
          				alert('Enrolling Site is not in list of Study Organizations where user can enroll patient');
          			else
          				alert(errorType);
          			$(".btn").removeClass('disabled');
         		 }else{
          		$scope.soapStatus = "Status Created Successfully.";
          		$('#patStatUpdate').modal('hide');
          		$(".btn").removeClass('disabled');
          		$scope.patNotes = '';
          		$scope.selectedPatStatus='';
          		$scope.patstatusDate = '';
          		$scope.selectedOrganization = '';
          		$scope.gridOptions.data = [];
                  if($scope.patientVisitGridOptions !=null){
                  $scope.patientVisitGridOptions.data = [];
                  $scope.selectedPatient = null;
                  $scope.patientVisitGridOptions = null;
                  $scope.gridOptions.columnDefs[3].visible = true;
                  $scope.gridOptions.columnDefs[4].visible = true;
                  $scope.gridOptions.columnDefs[5].visible = true;
                  $scope.gridOptions.columnDefs[6].visible = true;
                  $scope.gridOptions.columnDefs[7].visible = true;
                  $scope.gridOptions.columnDefs[8].visible = true;
                  $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
                  
                  }
                  $scope.soapStatus = "Row count: " + 0;
                  $scope.gridApi.core.refresh();
          		getPatients($scope.selectedStudy.studyNumber);
          	 	}
          	 }, function (response) { //error
                   $scope.status = response.status;

               });
           	};
            $scope.populateCalData=function(studyNumber){
            	showWarningChange();
            	 //$scope.openTestModal();
            	//var responseHolder={"Response":{"results":{"result":[{"action":"CREATE","objectId":{"attributes":{"xsi:type":"ns2:patientStudyStatusIdentifier"},"OID":"9037716b-8ff3-4b49-84a5-3029f64d86e1","PK":"3394"},"objectName":"PatientStudyStatusIdentifier"},{"action":"CREATE","objectId":{"attributes":{"xsi:type":"ns2:patientIdentifier"},"OID":"76545341-f071-445d-bb72-696c306020d3","PK":"2817","patientId":"NCI-010"},"objectName":"PatientIdentifier"}]},"issues":null}};
            	//var resultlist= responseHolder.Response.results.result;
            	var req = {
                        method: 'POST',
                        url: nodeURL+'/StudyCalendar/getStudyCalendarList',
                        headers: {
                            'Content-Type': 'application/json;charset=UTF-8'
                        },
                        data: {
                        	args:{"StudyIdentifier": {
                                "studyNumber": studyNumber
                            }},
                            "jwtToken": jwttoken
                        }
                    };
            	
            	  $http(req).then(function (response) { //success
                    try{
                    	$scope.allStudyCalendarList=[];
                    	if(response.data.StudyCalendarList !=null && response.data.StudyCalendarList.studyCalendars!=null){
                    		angular.forEach(response.data.StudyCalendarList.studyCalendars,function(value,index){
                    			if(value.calendarStatus.code=='A')
                    				$scope.allStudyCalendarList.push(value);
                            })
                        } }catch($scope) {
                        return "REST Call error."
                    }
                    	 
                    }, function (response) { 
                        $scope.soapStatus = response.data;
                    }
                );
            	
            };
            
            $scope.visitWindowLoad = function(row){
            	$scope.pageLoaderShow();
            	showWarningChange();
            	var deferred = $q.defer();
            	//Get Patient Schedules
                             //Get schedule Details
                                var req = {
                                        method: 'POST',
                                        url: nodeURL+'/PatientSchedule/getPatientScheduleVisits',
                                        headers: {
                                            'Content-Type': 'application/json;charset=UTF-8'
                                        },
                                        data: {args:{
                                            ScheduleIdentifier: {
                                                PK: row.patprotId.PK,
                                                patientIdentifier: {
													PK: row.patientIdentifier.PK
                                                },
                                                studyIdentifier: {
                                                    PK: row.studyPK
                                                }}
                                            },
                                            "jwtToken": jwttoken
                                        }
                                    };

                                    $http(req).then(function (response) {
                                    	//success schedule details
                                    	$scope.patientVisitGridOptions.data=[];
									    
                                        var scheduleDetails = response.data.PatientSchedule;
                                        //alert(JSON.stringify(scheduleDetails));
                                        if(scheduleDetails.visits==null){
											$scope.pageLoaderHide();
										}
                                        row.patprotID=scheduleDetails.patientScheduleSummary.scheduleIdentifier.PK;
                                        row.patprotId.PK=scheduleDetails.patientScheduleSummary.scheduleIdentifier.PK;
                                        //patient.studyPK=scheduleDetails.studyIdentifier.PK;
                                        angular.forEach(scheduleDetails.visits.visit,
                                            function (visit, visitKey) {
                                                /*if (visit.hasOwnProperty('events')) {
                                                    angular.forEach(visit.events.event,
                                                        function (event, eventKey) {*/
                                                    	
                                                    	if(visit.visitScheduledDate!=='Not Defined'){
															 var schDate= visit.visitScheduledDate;
															  var indx = schDate.indexOf(' ');
																	if(indx>0)
																			schDate = schDate.slice(0, indx)
																visit.eventSchDate=$filter('date')(schDate, "MM/dd/yyyy");
																var todayDate = new Date();
																var pastDate=false;
																	pastDate=$scope.compareDate(todayDate,schDate);
																//if(pastDate && (eptRight=='1') && (event.eventStatus.eventStatusCode.description==='Not done'))
																//	event.eventStatus.eventStatusCode.description='Past Scheduled Date';
																visit.visitScheduledDate = $filter('date')(schDate, "MM/dd/yyyy");
																}else{
																	visit.eventSchDate="Null";
																}
                                                    	/*if(event.eventStatus.statusValidFrom){
                                                    		event.eventStatus.statusValidFrom = new Date(event.eventStatus.statusValidFrom);
                                                    	}*/
                                                            visit["visitName"]=visit.visitName;
                                                            visit["visitId"]=visit.visitIdentifier.PK;
                                                            visit["patientID"]=scheduleDetails.patientIdentifier.patientId;
                                                            $scope.patientVisitGridOptions.data.push(visit);
                                                        //});
                                                   // patient.subGridOptions.data.push(visit);
                                                    //patient.rowCount = patient.subGridOptions.data.length;
                                                //}
                                            });
                                        if($scope.hasFocus===0){
                                        $scope.gridApi1.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
                               		 	$scope.gridApi1.grid.refresh();
                               		 	$scope.pageLoaderHide();}
                               		 deferred.resolve('request successful');
                                    }, function (response) { //error Schedule Details
                                    	deferred.reject('ERROR');
                                        return response.status;
                                    });
                                    return deferred.promise;
                                                            
            }
            
            $scope.eventWindowLoad = function(row){
            	$scope.pageLoaderShow();
            	showWarningChange();
            	//Get Patient Schedules
            	for (var i = 0, r; r = $scope.gridApi1.grid.rows[i]; i++) {
                    if(r.isExpanded){
                  	  $scope.expandedRowEntity = i;
                    }
                  }
                //alert(JSON.stringify(row));
                             //Get schedule Details
                                var req = {
                                        method: 'POST',
                                        url: nodeURL+'/PatientSchedule/getPatientSchedule',
                                        headers: {
                                            'Content-Type': 'application/json;charset=UTF-8'
                                        },
                                        data: {args:{
                                            ScheduleIdentifier: {
                                                PK: $scope.selectedPatient.patprotID,
                                                patientIdentifier: {
													PK: $scope.selectedPatient.patientIdentifier.PK
                                                },
                                                studyIdentifier: {
                                                    PK: $scope.selectedPatient.currentStudyPK
                                                }},
                                                VisitIdentifier:   {
                                                	PK: row.visitIdentifier.PK
                                                }
                                            },
                                            "jwtToken": jwttoken
                                        }
                                    };

                                    $http(req).then(function (response) {
                                    	//success schedule details
									    //row.subGridOptions.data=[];
									    //alert(response.data.PatientSchedule);
                                    	var eventData = [];
                                        var scheduleDetails = response.data.PatientSchedule;
                                        //alert(JSON.stringify(scheduleDetails));
                                        if(scheduleDetails.visits==null){
											$scope.pageLoaderHide();
											//$scope.gridApi1.expandable.toggleRowExpansion(row);
										}
                                        //alert(JSON.stringify(visit))
                                        row.patprotID=scheduleDetails.patientScheduleSummary.scheduleIdentifier.PK;
                                        //alert(row.patprotID)
                                        //row.patprotId.PK=scheduleDetails.patientScheduleSummary.scheduleIdentifier.PK;
                                        //alert(row.patprotId.PK)
                                        //patient.studyPK=scheduleDetails.studyIdentifier.PK;
                                        angular.forEach(scheduleDetails.visits.visit,
                                            function (visit, visitKey) {
                                        	//alert(JSON.stringify(visit))
                                                if (visit.hasOwnProperty('events')) {
                                                    angular.forEach(visit.events.event,
                                                        function (event, eventKey) {
                                                    	
                                                    	if(event.eventScheduledDate!=='Not Defined'){
															 var schDate= event.eventScheduledDate;
															  var indx = schDate.indexOf(' ');
																	if(indx>0)
																			schDate = schDate.slice(0, indx)
																event.eventSchDate=$filter('date')(schDate, "MM/dd/yyyy");
																var todayDate = new Date();
																var pastDate=false;
																	pastDate=$scope.compareDate(todayDate,schDate);
																if(pastDate && (eptRight=='1') && (event.eventStatus.eventStatusCode.description==='Not done'))
																	event.eventStatus.eventStatusCode.description='Past Scheduled Date';
																event.eventScheduledDate = $filter('date')(schDate, "MM/dd/yyyy");
																}else{
																	event.eventSchDate="Null";
																}
                                                    	if(event.eventStatus.statusValidFrom){
                                                    		event.eventStatus.statusValidFrom = new Date(event.eventStatus.statusValidFrom);
                                                    	}
                                                            event["visitName"]=visit.visitName;
                                                            event["visitId"]=visit.visitIdentifier.PK;
                                                            event["patientID"]=scheduleDetails.patientIdentifier.patientId;
                                                            eventData.push(event);
                                                        });
                                                   // patient.subGridOptions.data.push(visit);
                                                    //patient.rowCount = patient.subGridOptions.data.length;
                                                }
                                            });
                                        row.subGridOptions.data = eventData;
                                        $scope.gridApi1.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
                               		 	$scope.gridApi1.grid.refresh();
                               		 	$scope.pageLoaderHide();
                                    }, function (response) { //error Schedule Details
                                        return response.status;
                                    });                                
            }
            
            $scope.PatDialogClose= function(){
            	showWarningChange();
            	$scope.buttonFlag=0;
            	$scope.buttonFLNFlag=0;
            	if($scope.patientVisitGridOptions !=null){
                    $scope.patientVisitGridOptions.data = [];
                    $scope.selectedPatient = null;
                    $scope.patientVisitGridOptions = null;
                    $scope.gridOptions.columnDefs[3].visible = true;
                    $scope.gridOptions.columnDefs[4].visible = true;
                    $scope.gridOptions.columnDefs[5].visible = true;
                    $scope.gridOptions.columnDefs[6].visible = true;
                    $scope.gridOptions.columnDefs[7].visible = true;
                    $scope.gridOptions.columnDefs[8].visible = true;

                    $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
                    
                    }
        		$scope.patientId='';
        		$scope.searchedPatient.patFirstName='';
        		$scope.searchedPatient.patLastName='';
        		$scope.patientStudyId='';
        		$scope.searchedPatient.patDateofBirth='';
        		$scope.studyStatusDate='';
        		$scope.selectedOrganization = '';
        		$scope.calStartDate='';
        		$scope.selectedEthnicity='';
        		$scope.selectedRace='';
        		$scope.selectedGender='';
            }
            $scope.PatStatDialogClose= function(){
            	showWarningChange();
            	$('#patStatUpdate').modal('hide');
         		$scope.patNotes = '';
         		$scope.selectedPatStatus='';
         		$scope.patstatusDate = '';
         		$scope.selectedOrganization = '';
            }
            $scope.eventStatDialogClose= function(){
            	showWarningChange();
            	$('#eventPowerBarModal').modal('hide');
            	$scope.selectedEventStatus='';
            	$scope.eventNotes='';
            	$scope.statusDate='';
            }
            
            function subGridApiRegister(gridApi){
            	//showWarningChange();
                // register the child API in the parent - can't tell why it's not in the core...
                var parentRow = gridApi.grid.appScope.row;
                parentRow.subGridApi = gridApi;
                
               // TODO::run over the subGrid's rows and match them to the parentRow.isSelected property by name to toggle the row's selection
                
                // subGrid selection method
                gridApi.selection.on.rowSelectionChanged(gridApi.grid.appScope, function(row){
                	showWarningChange();
                	if (row.isSelected) {
                  	   if(row.entity.visitId){
                  		$scope.selectedRowData.push(row.entity);
                  		$scope.patientID = row.entity.patientID;
                  	   }
                  	}else{
                  		parentRow.isSelected = false;
                  		var cnt = 0;
                  		for(var k=0;k<$scope.selectedRowData.length;k++){
                  			if(row.entity.$$hashKey===$scope.selectedRowData[k].$$hashKey){
                  				cnt=k;
                  				break;
                  			}
                  		}
                  		$scope.selectedRowData.splice(cnt,1);
                  	}
                  /*if (angular.isUndefined(parentRow.isSelected)){
                    parentRow.isSelected = {}; 
                  }*/
                	//alert(parentRow.entity.subGridOptions.data.length);
                	//alert(parentRow.subGridApi.selection.getSelectedRows().length);
                 if(parentRow.entity.subGridOptions.data.length===parentRow.subGridApi.selection.getSelectedRows().length){
                	 parentRow.isSelected = true;
                 }
                });
              }
            
            $scope.compareDate = function(todayDate,schDate){
            	showWarningChange();
            	var scheDate = schDate.split('-'),
            	scheDateYear = parseInt(scheDate[0], 10), // cast Strings as Numbers
            	scheDateMo = parseInt(scheDate[1], 10),
            	scheDateDay = parseInt(scheDate[2], 10);

            var nowYear = todayDate.getFullYear(),
                nowMo = todayDate.getMonth() + 1, // for getMonth(), January is 0
                nowDay = todayDate.getDate();
            
            if(nowYear > scheDateYear ||
                    nowYear == scheDateYear && nowMo > scheDateMo ||
                    nowYear == scheDateYear && nowMo == scheDateMo && nowDay > scheDateDay){
            	return true;
            }else{
            	return false;
            }

            }
            
            $scope.compareDates = function(dob,statd){
            	showWarningChange();
            	if(new Date(dob)>new Date(statd))
            		return true;
            	else
            		return false;
            }

            
            $scope.pageLoaderShow = function () {
            	showWarningChange();
            	$('#sideMenuTogleDIV').html('');
                $('#pageContent').css("backgroundColor", "#E6E6E6");
                $('#pageContent').css("opacity", 5);
                document.getElementById('spinner').style.display = 'block';
            };
            
            $scope.pageLoaderHide = function () {
            	showWarningChange();
            	document.getElementById('spinner').style.display = 'none';
            	//queryManagement.renderToggleButton();
            }
            
            
            $scope.today =  new Date();
                            
              
              $scope.clear = null;

              $scope.inlineOptions = {
                customClass: getDayClass,
                minDate: new Date(),
                showWeeks: true
              };
              $scope.dateOptions = {
                      formatYear: 'yy',
                      maxDate: new Date(2035, 5, 22),
                      minDate: new Date(),
                      startingDay: 1
                    };

              $scope.dateOptions1 = {
            	dateDisabled: disabled,
                formatYear: 'yy',
                maxDate: new Date(2035, 5, 22),
                startingDay: 1
              };
              function disabled(data) {
            	    var date = data.date,
            	      mode = data.mode;
            	    return mode === 'day' && (date>new Date());
            	  }
              

              $scope.toggleMin = function() {
                $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
                $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
              };

              $scope.toggleMin();

              $scope.open1 = function() {
                $scope.popup1.opened = true;
              };

              $scope.open2 = function() {
                $scope.popup2.opened = true;
              };
              
              $scope.open3 = function() {
                  $scope.popup3.opened = true;
                };
              $scope.open4 = function() {
                  $scope.popup4.opened = true;
                  };
              $scope.open5 = function() {
                  $scope.popup5.opened = true;
                    };
              $scope.open6 = function() {
                  $scope.popup6.opened = true;
                      };

              $scope.setDate = function(year, month, day) {
                $scope.patDateofBirth = new Date(year, month, day);
              };

              $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
              $scope.format = $scope.formats[0];
              $scope.altInputFormats = ['M!/d!/yyyy'];

              $scope.popup1 = {
                opened: false
              };

              $scope.popup2 = {
                opened: false
              };
              
              $scope.popup3 = {
                      opened: false
                    };
              $scope.popup4 = {
                      opened: false
                    };
              $scope.popup5 = {
                      opened: false
                    };
              $scope.popup6 = {
                      opened: false
                    };

              var tomorrow = new Date();
              tomorrow.setDate(tomorrow.getDate() + 1);
              var afterTomorrow = new Date();
              afterTomorrow.setDate(tomorrow.getDate() + 1);
              $scope.events = [
                {
                  date: tomorrow,
                  status: 'full'
                },
                {
                  date: afterTomorrow,
                  status: 'partially'
                }
              ];

              function getDayClass(data) {
                var date = data.date,
                  mode = data.mode;
                if (mode === 'day') {
                  var dayToCheck = new Date(date).setHours(0,0,0,0);

                  for (var i = 0; i < $scope.events.length; i++) {
                    var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

                    if (dayToCheck === currentDay) {
                      return $scope.events[i].status;
                    }
                  }
                }

                return '';
              }      
            
//***************************************************************************************************************//  
             
              
              //start changePatbeanJsonElement function
              $scope.changePatbeanJsonElement=function(patDataBeanJson){
            	  showWarningChange();
            	  for(var i=0;i<patDataBeanJson.length;i++){
            	  var jsonStr=JSON.stringify(patDataBeanJson[i]);
            	  jsonStr=jsonStr.slice(0,jsonStr.length-1)
            	  if(patDataBeanJson[i].MASK_PFNAME!=undefined)
            	       jsonStr=jsonStr+',"patFirstName":"'+patDataBeanJson[i].MASK_PFNAME+'"'; //for firstName
            	  if(patDataBeanJson[i].MASK_PLNAME!=undefined)
            		   jsonStr=jsonStr+',"patLastName":"'+patDataBeanJson[i].MASK_PLNAME+'"'; //for lastName
            	  if(patDataBeanJson[i].MASK_PERSON_DOB_DATESORT!=undefined)
           		   jsonStr=jsonStr+',"patDateofBirth":"'+patDataBeanJson[i].MASK_PERSON_DOB_DATESORT+'"'; //for dob
            	  if(patDataBeanJson[i].MASK_MRN!=undefined)
           	       jsonStr=jsonStr+',"patientIdentifier":{"patientId":"'+patDataBeanJson[i].MASK_MRN+'"},"patCode":"'+patDataBeanJson[i].MASK_MRN+'"'; //for PatinetId
            	  if(patDataBeanJson[i].PERSON_DEATHDT!=undefined)
              		   jsonStr=jsonStr+',"deathDate":"'+patDataBeanJson[i].PERSON_DEATHDT+'"'; //for dod
            	  
            	  if(patDataBeanJson[i].MASK_SSN!=undefined)
             		   jsonStr=jsonStr+',"patssn":"'+patDataBeanJson[i].MASK_SSN+'"'; //for patssn
            	  if(patDataBeanJson[i].MASK_PZIP!=undefined)
             		   jsonStr=jsonStr+',"patzip":"'+patDataBeanJson[i].MASK_PZIP+'"'; //for patzip
            	  if(patDataBeanJson[i].MASK_PSTATE!=undefined)
             		   jsonStr=jsonStr+',"patstate":"'+patDataBeanJson[i].MASK_PSTATE+'"'; //for patstate
            	  if(patDataBeanJson[i].MASK_PCOUNTRY!=undefined)
             		   jsonStr=jsonStr+',"patcountry":"'+patDataBeanJson[i].MASK_PCOUNTRY+'"'; //for patcountry
            	  if(patDataBeanJson[i].MASK_PADD1!=undefined)
             		   jsonStr=jsonStr+',"patadd1":"'+patDataBeanJson[i].MASK_PADD1+'"'; //for patadd1
            	  if(patDataBeanJson[i].MASK_PCITY!=undefined)
             		   jsonStr=jsonStr+',"patcity":"'+patDataBeanJson[i].MASK_PCITY+'"'; //for patcity
            	  if(patDataBeanJson[i].MASK_PHPHONE!=undefined)
             		   jsonStr=jsonStr+',"pathphone":"'+patDataBeanJson[i].MASK_PHPHONE+'"'; //for pathphone
            	  if(patDataBeanJson[i].MASK_COUNTY!=undefined)
             		   jsonStr=jsonStr+',"patcounty":"'+patDataBeanJson[i].MASK_COUNTY+'"'; //for patcounty
            	  if(patDataBeanJson[i].MASK_PMNAME!=undefined)
            		   jsonStr=jsonStr+',"patmname":"'+patDataBeanJson[i].MASK_PMNAME+'"'; //for patmname
            	  
            	  
            	  if(patDataBeanJson[i].PERSON_STATUS!=undefined){
            		  if(patDataBeanJson[i].PERSON_STATUS.toLowerCase()==='alive')
            			  jsonStr=jsonStr+',"patSurvivalStat":{"code":"A","type": "patient_status"}';
            		  else if(patDataBeanJson[i].PERSON_STATUS.toLowerCase()==='dead')
            			  jsonStr=jsonStr+',"patSurvivalStat":{"code":"dead","type": "patient_status"}'; 
            		  else
            			  jsonStr=jsonStr+',"patSurvivalStat":{"code":"A","type": "patient_status"}';  
            	  }
            	  if(patDataBeanJson[i].PERSON_GENDER!=undefined){
            		  if(patDataBeanJson[i].PERSON_GENDER.toLowerCase()==='male')
            			  	jsonStr=jsonStr+',"gender":{"code":"male","type":"gender"}';
            		  else if(patDataBeanJson[i].PERSON_GENDER.toLowerCase()==='female')
               		   		jsonStr=jsonStr+',"gender":{"code":"female","type":"gender"}';
            		  else if(patDataBeanJson[i].PERSON_GENDER.toLowerCase()==='other')
               		   		jsonStr=jsonStr+',"gender":{"code":"other","type":"gender"}';
            		  else
               		   		jsonStr=jsonStr+',"gender":{"code":"unknown","type":"gender"}';
            	  }
            	  if(patDataBeanJson[i].MASK_RACE!=undefined){
            		  if(patDataBeanJson[i].MASK_RACE.toLowerCase()==='black or african american')
            			  	jsonStr=jsonStr+',"race":{"code":"race_blkafr","type":"race"}';
            		  else if(patDataBeanJson[i].MASK_RACE.toLowerCase()==='white')
               		   		jsonStr=jsonStr+',"race":{"code":"race_white","type":"race"}';
            		  else if(patDataBeanJson[i].MASK_RACE.toLowerCase()==='white caribbean')
               		   		jsonStr=jsonStr+',"race":{"code":"race_WCARIB","type":"race"}';
            		  else if(patDataBeanJson[i].MASK_RACE.toLowerCase()==='white so. or central american')
             		   		jsonStr=jsonStr+',"race":{"code":"race_WSCA","type":"race"}';
            		  else if(patDataBeanJson[i].MASK_RACE.toLowerCase()==='asian')
           		   			jsonStr=jsonStr+',"race":{"code":"race_asian","type":"race"}';
            		  else if(patDataBeanJson[i].MASK_RACE.toLowerCase()==='not reported')
           		   			jsonStr=jsonStr+',"race":{"code":"race_notrep","type":"race"}';
            		  else if(patDataBeanJson[i].MASK_RACE.toLowerCase()==='american indian or alaska native')
           		   			jsonStr=jsonStr+',"race":{"code":"race_indala","type":"race"}';
            		  else if(patDataBeanJson[i].MASK_RACE.toLowerCase()==='native hawaiian or other pacific islander')
           		   			jsonStr=jsonStr+',"race":{"code":"race_hwnisl","type":"race"}';
            		  else if(patDataBeanJson[i].MASK_RACE.toLowerCase()==='south cntrl/cntrl amer. hisp.')
           		   			jsonStr=jsonStr+',"race":{"code":"race_SCAHIS","type":"race"}';
            		  else if(patDataBeanJson[i].MASK_RACE.toLowerCase()==='south or central american')
           		   			jsonStr=jsonStr+',"race":{"code":"race_SCAMB","type":"race"}';
            		  else if(patDataBeanJson[i].MASK_RACE.toLowerCase()==='unknown/question not asked')
           		   			jsonStr=jsonStr+',"race":{"code":"race_UNK","type":"race"}';
            		  else if(patDataBeanJson[i].MASK_RACE.toLowerCase()==='southeast asian')
           		   			jsonStr=jsonStr+',"race":{"code":"race_SCSEAI","type":"race"}';
            		  else if(patDataBeanJson[i].MASK_RACE.toLowerCase()==='north american')
           		   			jsonStr=jsonStr+',"race":{"code":"race_NAMER","type":"race"}';
            		  else if(patDataBeanJson[i].MASK_RACE.toLowerCase()==='eastern european')
           		   			jsonStr=jsonStr+',"race":{"code":"race_EEURO","type":"race"}';
            		  else if(patDataBeanJson[i].MASK_RACE.toLowerCase()==='western european')
           		   			jsonStr=jsonStr+',"race":{"code":"race_WEURO","type":"race"}';
            		  else if(patDataBeanJson[i].MASK_RACE.toLowerCase()==='northern european')
           		   			jsonStr=jsonStr+',"race":{"code":"race_NEURO","type":"race"}';
            		  else if(patDataBeanJson[i].MASK_RACE.toLowerCase()==='mediterranean')
           		   			jsonStr=jsonStr+',"race":{"code":"race_MEDIT","type":"race"}';
            		  else if(patDataBeanJson[i].MASK_RACE.toLowerCase()==='middle eastern')
           		   			jsonStr=jsonStr+',"race":{"code":"race_MIDEAS","type":"race"}';
            		  else if(patDataBeanJson[i].MASK_RACE.toLowerCase()==='north coast of africa')
           		   			jsonStr=jsonStr+',"race":{"code":"race_NCAFRI","type":"race"}';
            		  else if(patDataBeanJson[i].MASK_RACE.toLowerCase()==='american indian so. or central')
           		   			jsonStr=jsonStr+',"race":{"code":"race_AISC","type":"race"}';
            		  else if(patDataBeanJson[i].MASK_RACE.toLowerCase()==='caribbean indian')
           		   			jsonStr=jsonStr+',"race":{"code":"race_CARIBI","type":"race"}';
            		  else if(patDataBeanJson[i].MASK_RACE.toLowerCase()==='vietnamese')
           		   			jsonStr=jsonStr+',"race":{"code":"race_VIET","type":"race"}';
            		  else if(patDataBeanJson[i].MASK_RACE.toLowerCase()==='hawaiian')
           		   			jsonStr=jsonStr+',"race":{"code":"race_HAWAII","type":"race"}';
            		  else if(patDataBeanJson[i].MASK_RACE.toLowerCase()==='guamanian')
           		   			jsonStr=jsonStr+',"race":{"code":"race_GUAMAN","type":"race"}';
            		  else if(patDataBeanJson[i].MASK_RACE.toLowerCase()==='samoan')
           		   			jsonStr=jsonStr+',"race":{"code":"race_SAMOAN","type":"race"}';
            		  else if(patDataBeanJson[i].MASK_RACE.toLowerCase()==='other pacific islander')
           		   			jsonStr=jsonStr+',"race":{"code":"race_OPI","type":"race"}';
            		  else
               		   		jsonStr=jsonStr+',"race":{"code":"race_unknown","type":"race"}';
            	  }
            	  //jsonStr=jsonStr+',"mask_race":{"code":"'+$scope.selectedRace +'","type":"race"}';   //single for  race 
            	
            	  jsonStr=jsonStr+'}';
            	  patDataBeanJson[i]=JSON.parse(jsonStr);
            	  }
            	   return patDataBeanJson;
              }// End changePatbeanJsonElement function
              
        }
    ]);