describe('Controller: Controllers.bioSampleStoreFrontCtrl', function () {

    // load the controller's module
    beforeEach(module('Controllers'));

    var ctrl,
        scope;

    // Initialize the controller and a mock scope
    beforeEach(inject(function ($controller, $rootScope) {
        scope = $rootScope.$new();
        ctrl = $controller('bioSampleStoreFrontCtrl', {
            $scope: scope
        });
    }));

    it('should be defined', function () {
        expect(ctrl).toBeDefined();
    });
});
