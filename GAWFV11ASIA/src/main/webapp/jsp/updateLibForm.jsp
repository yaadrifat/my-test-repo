<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<!-- Bug#9941 29-May-2012 -Sudhir-->
<jsp:include page="include.jsp" flush="true"/>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<BODY>
<jsp:useBean id="formlibB" scope="request" class="com.velos.eres.web.formLib.FormLibJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="objShareB" scope="request" class="com.velos.eres.web.objectShare.ObjectShareJB"/>
<jsp:useBean id="linkedFormsJB" scope="request"  class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
<%@ page language = "java" import ="com.velos.eres.business.common.*,java.util.*, com.velos.eres.service.util.*" %>
<%
 int iret = -1;
 int iupret = 0;
 int iformLibId=0;
 int lfPk = 0;
 String mode =request.getParameter("mode");
 
 String esignReq = request.getParameter("esignReq");
 
 if (StringUtil.isEmpty(esignReq))
    	{
    		esignReq="0";
    	}

 String eSign = request.getParameter("eSign");
 String formLibId =request.getParameter("formLibId");
 String src =request.getParameter("srcmenu");

 String calledFrom= request.getParameter("calledFrom");
  /// by salil 17 sep 2003 
 String lnkFrom= request.getParameter("lnkFrom");
 String studyId= request.getParameter("studyId");
 // end by salil
 String codeStatus ="";
 codeStatus=request.getParameter("codeStatus");

 HttpSession tSession = request.getSession(true); 
 String accId = (String) tSession.getValue("accountId");

 String ipAdd = (String) tSession.getValue("ipAdd");

 String usr = (String) tSession.getValue("userId");
 //String sharedWith="";
 //String sharedWithIds="";

int FLDCounts=StringUtil.stringToNum(request.getParameter("FLDcount"));
 if (sessionmaint.isValidSession(tSession)) {
//out.println("codeStatus"+codeStatus);
%>
	
<jsp:include page="sessionlogging.jsp" flush="true"/> 

<%	
String oldESign = (String) tSession.getValue("eSign");

	if(!oldESign.equals(eSign)) {

%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
	} 
else
	{

	String name=StringUtil.stripScript(request.getParameter("txtName"));
	name = name.trim();

	String desc=request.getParameter("txtAreaDesc");

	String type=request.getParameter("formType");

	String status=request.getParameter("cmbStatus");
	//out.println("status"+status);

	int ret = 0;
	
	
	String recordType="";

	CodeDao cd = new CodeDao();
		int istatus = EJBUtil.stringToNum(status);
		int codeId=0;
		String  CodeDesc=cd.getCodeDescription(istatus);
		if(calledFrom.equals("L"))
		{
			codeId=cd.getCodeId("frmlibstat","W");
		}
		else if(calledFrom.equals("St") || calledFrom.equals("A") )
		{
			
			codeId=cd.getCodeId("frmstat","W");
		}
		//out.println("codeId"+codeId);
		if(codeId==istatus)
			{
			 //out.println("inside if");
			 codeStatus = "WIP";
			}
		
	if(mode.equals("M"))
    	{
    	
    	String migrate = "";
    	boolean bMigrate = false;
    	
    	migrate = request.getParameter("migrateForm");
    	
    	if (! StringUtil.isEmpty(migrate))
    	{
    		if (migrate.equals("1"))
    		{
    			bMigrate = true;
    		}
    	}
    	
    	
		iformLibId=Integer.parseInt(formLibId);
		formlibB.setFormLibId(iformLibId);
		formlibB.getFormLibDetails();
		boolean nameChanged = !StringUtil.trueValue(formlibB.getFormLibName()).equals(name);
		formlibB.setFormLibName(name);
		formlibB.setCatLibId(type);
		formlibB.setFormLibDesc(desc);
		formlibB.setFormLibStatus(status);
		formlibB.setIpAdd(ipAdd);
		formlibB.setCreator(usr);
		formlibB.setRecordType(mode);
		formlibB.setLastModifiedBy(usr);
		formlibB.setMigrateVersion(bMigrate);
		formlibB.setEsignRequired(esignReq);
		
		lfPk = StringUtil.stringToNum(request.getParameter("lfPk"));
		int formNameCount = 0;
		if (nameChanged && lfPk > 0) {
			linkedFormsJB.setLinkedFormId(lfPk) ;
			linkedFormsJB.getLinkedFormDetails();
			LinkedFormsDao linkedFormsDao = new LinkedFormsDao(); 
			formNameCount = linkedFormsDao.getFormNameCountByName(
					name, linkedFormsJB.getLFDisplayType(), 
					StringUtil.stringToNum((String)tSession.getAttribute("accountId")),
					StringUtil.stringToNum(studyId));
			System.out.println("formNameCount="+formNameCount);
		} else if (nameChanged && lfPk == 0) {
			FormLibDao formLibDao = new FormLibDao();
			formNameCount = formLibDao.uniqueLibraryFormName(StringUtil.stringToNum(accId), name);
			System.out.println("For library, formNameCount="+formNameCount);
		}
        	
		//set the refresh flag to 1 indicating that the xsl needs to be refreshed
		
		//commented by Sonia Abrol, 08/31/05, so that xsl is not regenerated if form details are saved and xsl refresh flag is not changed
		//formlibB.setFormLibXslRefresh("0"); 
		
		//Modified by Manimaran to fix the Bug 2563. <== commented out by IH for Bug 10583
		//if (!lnkFrom.equals("S"))
		//iupret=formlibB.updateFormLib();
		//else if(lnkFrom.equals("S")){
        //FormLibDao formLibDao =new FormLibDao();
		//ret = formLibDao.uniqueLinkedFormName(EJBUtil.stringToNum(accId),iformLibId,name,lnkFrom,EJBUtil.stringToNum(studyId));
		
		if (formNameCount > 0) {  ret = -3; }
		
		if (ret==0) {
			if((FLDCounts>950) && (CodeDesc.equalsIgnoreCase("Active"))){
				FLDCounts=FLDCounts;
			}else{
				formlibB.setSkipNameCheck(true); // Name check already performed
				iupret=formlibB.updateFormLib();
			}
		} else {
			formlibB.setSkipNameCheck(false);
			iupret=ret;
		}
		
		//}  // End of Manimaran fix for Bug 2563
						
  }
  else
		{
		/*CodeDao cd = new CodeDao();
		int istatus = EJBUtil.stringToNum(status);
		int codeId=0;
		if(calledFrom.equals("L"))
		{
			codeId=cd.getCodeId("frmlibstat","W");
		}
		else if(calledFrom.equals("SA"))
		{
			
			codeId=cd.getCodeId("frmstat","W");
		}

		if(codeId==istatus)
			{
			 codeStatus = "WIP";
			}
		else
			{
			codeStatus = "";
			}*/

	formlibB.setIpAdd(ipAdd);
	formlibB.setCreator(usr);
	formlibB.setFormLibName(name);
	formlibB.setFormLibDesc(desc);
	formlibB.setCatLibId(type);
	formlibB.setRecordType(mode);
	//formlibB.setLastModifiedBy(usr);
	formlibB.setFormLibStatus(status);
	formlibB.setAccountId(accId);
	formlibB.setFormLibLinkTo("L");
	formlibB.setFormLibSharedWith("A");

	//out.println("status"+status);
	
	/*else{
	formlibB.setSharedWithIds("");
	formlibB.setFormLibSharedWith("");
	}*/
	//set the refresh flag to 1 indicating that the xsl needs to be refreshed
	formlibB.setFormLibXslRefresh("1");
	formlibB.setFormLibSharedWith("A");
	formlibB.setEsignRequired(esignReq);
	if((FLDCounts>950) && (CodeDesc.equalsIgnoreCase("Active"))){
		FLDCounts=FLDCounts;
	}else{
	iformLibId = formlibB.setFormLibDetails();
	}
	//iret=formlibB.getFormLibId();
	/*iformLibId = iret;

	
		    objShareB.setIpAdd(ipAdd);
         	objShareB.setCreator(usr);
         	objShareB.setObjNumber("1");//number of module
         	objShareB.setFkObj(formLibId);// formId, CalId, reportId
         	objShareB.setObjSharedId(accId);// shared ids
         	objShareB.setObjSharedType("A");// U-user id,A -account id,S - study id,G - group id,O - organization id 
         	objShareB.setRecordType(mode);         		
         	ret = objShareB.setObjectShareDetails();*/
	}
	
	
		 if (iformLibId == -3 || iupret == -3)
			{%>
			<br><br><br><br><br>
			<p class = "successfulmsg" align = center > <%=MC.M_UnqFrmName_EtrUnqFrmName%><%-- The unique form name already exists. Please enter a different unique form name*****--%></p>
			<center><!-- Bug#9941 29-May-2012 -Sudhir-->
			<button tabindex=2 onclick="window.history.back();return false;"><%=LC.L_Back%></button>
		<%
		} else if(iupret != 0 ||  iformLibId < 0 ) {
			%>
					<br><br><br><br><br>
					<p class = "successfulmsg" align = center> <%=MC.M_Data_NotSvdSucc%><%-- Data not saved successfully*****--%> </p>			
					<!-- Bug#9941 29-May-2012 -Sudhir-->
					<button name="back" onclick="window.history.back();return false;"><%=LC.L_Back%></button></p>
				<% return;
		}
			if((FLDCounts>950) && (CodeDesc.equalsIgnoreCase("Active"))){
				%> 
				<p class = "successfulmsg" align = center>
						System can not Active this form with more than 950 data capturing fields.
						Please reduce the fields count by deleting the data capturing fields OR
						Reducing Repratable sections fields count.
					</p>
				<br><br><br><br><br>
					<p class = "successfulmsg" align = center> <%=MC.M_Data_NotSvdSucc%><%-- Data not saved successfully*****--%> </p>
					<DIV style="position:relative;margin: auto;margin-left: 48%">
					<button name="back" onclick="window.history.back();return false;"><%=LC.L_Back%></button>
					</DIV>
					</p>
				<% return;}
				
	
	else {%>
		<br><br><br><br><br><p class = "successfulmsg" align = center>
		<%=MC.M_Data_SavedSucc%><%-- Data saved successfully*****--%></p>	
		<META HTTP-EQUIV=Refresh CONTENT="1;URL=formcreation.jsp?mode=M&calledFrom=<%=calledFrom%>&srcmenu=<%=src%>&selectedTab=1&formLibId=<%=iformLibId%>&codeStatus=<%=codeStatus%>&lnkFrom=<%=lnkFrom%>&studyId=<%=studyId%>&lfPk=<%=lfPk%>">
		
	<%}
	}
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
 <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</BODY>

</HTML>

