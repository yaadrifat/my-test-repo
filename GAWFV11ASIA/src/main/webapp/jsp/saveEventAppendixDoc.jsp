<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<jsp:include page="localization.jsp" flush="true" />

<%@page import="com.velos.eres.service.util.MC"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><%=MC.M_EvtLib_ApdxEvtDets%><%--Event Library >> Event Appendix >> Event File Details*****--%></title>
</head>

<body>

<%
	String srcmenu = request.getParameter("srcmenu");
	String duration = request.getParameter("duration");
	String eventId = request.getParameter("eventId");
	String networkId = request.getParameter("networkId")==null?"":request.getParameter("networkId");
	String networkFlag = request.getParameter("networkFlag")==null?"":request.getParameter("networkFlag");
	String protocolId = request.getParameter("protocolId");
	String calledFrom = request.getParameter("calledFrom");
	String mode = request.getParameter("mode");
	String fromPage = request.getParameter("fromPage");
	String siteId = request.getParameter("siteId")==null?"":request.getParameter("siteId");
	String userPk = (request.getParameter("userPk")==null)?"":request.getParameter("userPk");
	String selectedTab = request.getParameter("selectedTab");
	String calStatus = request.getParameter("calStatus");
	String eventmode = request.getParameter("eventmode");
	String displayType = request.getParameter("displayType");

	String displayDur = request.getParameter("displayDur");
	String calassoc = request.getParameter("calassoc");
%>
<jsp:include page="include.jsp" flush="true"/>
<br>
<br>
<br>
<br>
<br>
<p class="sectionHeadings" align=center><%=MC.M_Data_SavedSucc%><%-- Data saved successfully.*****--%>
</p>
<%if(networkFlag.equalsIgnoreCase("")){ %>
<META HTTP-EQUIV=Refresh
	CONTENT="1; URL=eventappendix.jsp?srcmenu=<%=srcmenu%>&duration=<%=duration%>&eventId=<%=eventId%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&selectedTab=4&calStatus=<%=calStatus%>&eventmode=<%=eventmode%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&calassoc=<%=calassoc%>">
<%}else{ %>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=eventappendix.jsp?srcmenu=<%=srcmenu%>&duration=<%=duration%>&eventId=<%=eventId%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&selectedTab=<%=selectedTab%>&calStatus=<%=calStatus%>&eventmode=<%=eventmode%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&calassoc=<%=calassoc%>&networkFlag=<%=networkFlag%>&networkId=<%=networkId%>&siteId=<%=siteId%>&userPk=<%=userPk%>">
<%} %>
</body>
</html>
