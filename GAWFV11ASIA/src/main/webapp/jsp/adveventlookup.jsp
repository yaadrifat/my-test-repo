<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=LC.L_Adverse_Event%><%--Advent Event*****--%></title>
<SCRIPT Language="Javascript1.2">
//new method added
function setOrder(formobj,orderBy)
{

	if (formobj.orderType.value=="asc") {
		formobj.orderType.value= "desc";
	} else 	if (formobj.orderType.value=="desc") {
		formobj.orderType.value= "asc";
	}
	formobj.orderBy.value= orderBy;
	formobj.submit();
}
function setValue(id,name,flag){
	var formObj = window.opener.document.adverseEventScreenForm;
	if (flag=="remove"){
		formObj.linkedToId.value = "";
		formObj.linkedToName.value = "";
	}
	else {
		formObj.linkedToId.value = id;
		formObj.linkedToName.value = id;
	}
	self.close();
}

</SCRIPT>

<jsp:useBean id="siteB" scope="request" class="com.velos.eres.web.site.SiteJB"/>
<jsp:useBean id="userB" scope="session" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,com.velos.esch.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.*"%>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<% response.setHeader("Cache-Control", "max-age=1, must-revalidate");%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
	<jsp:include page="skinChoser.jsp" flush="true"/>
	<style>
	html, body { overflow: visible; }
	</style>
<body>
<%
        String studyId  = (String)request.getParameter("studyId");
		studyId = StringUtil.stripScript(studyId);

		String personPK = (String)request.getParameter("personPK");
		personPK = StringUtil.stripScript(personPK);

		String pagenum = "";
		int curPage = 0;
		long startPage = 1;
		String stPage;
		long cntr = 0;
		pagenum = request.getParameter("currentpage");


		if (pagenum == null)
		{
	   	   pagenum = "1";
		}
		curPage = EJBUtil.stringToNum(pagenum);

	    String orderBy = "";
	    orderBy = request.getParameter("orderBy");
	    if (EJBUtil.isEmpty(orderBy))
		    orderBy = "ae_stdate";
		else {
			orderBy = FilterUtil.sanitizeTextForSQL(orderBy);
		    orderBy = StringUtil.stripScript(orderBy);
		}

	    String src="";
        src= request.getParameter("srcmenu");
        src = StringUtil.stripScript(src);

	    String  orderType = "";
	    orderType = request.getParameter("orderType");

		if (EJBUtil.isEmpty(orderType)){
			orderType = "desc";
		} else {
			orderType = FilterUtil.sanitizeTextForSQL(orderType);
	    	orderType = StringUtil.stripScript(orderType);
		}
%>
<Form name="adeventpopup" method="post" action="adveventlookup.jsp">
<form>

		    <%
		    String mysql = "  select a.pk_adveve as advId,"
                    + " b.CODELST_DESC as advType,"
                    + " a.FK_CODLST_AETYPE as advTypeId,"
                    + "	a.AE_DESC as advDesc,"
                    + " a.AE_GRADE as advGrade,"
                    + " a.AE_Name  as advName,"
					+ " a.AE_RELATIONSHIP ,"
					+ " (select distinct codelst_desc from sch_codelst where sch_codelst.codelst_type='adve_relation' and "
					+ " sch_codelst.PK_CODELST=a.AE_RELATIONSHIP) as attribution,"
                    + "	to_char(a.ae_stdate,'yyyy-mm-dd') as advSdate,"
                    + "	to_char(a.ae_enddate,'yyyy-mm-dd') as advEdate,"
                    + "	c.usr_firstname ||' '|| c.usr_lastname  as advUser, FORM_STATUS  "
                    + "	from sch_adverseve a,"
                    + "	sch_codelst b,"
                    + "	ERES.er_user c"
                    + "	where a.FK_CODLST_AETYPE=b.pk_codelst"
                    + " and a.AE_ENTERBY= c.pk_user and fk_study= " + FilterUtil.sanitizeNumberForSQL(studyId) +  " and fk_per= " + FilterUtil.sanitizeNumberForSQL(personPK)  ; //+ "  order by a.ae_stdate desc,a.pk_adveve desc";

          String countSql = " select count(*) from ( " + mysql + " ) ";


          long rowsPerPage=0;
	  long totalPages=0;
	  long rowsReturned = 0;
	  long showPages = 0;

	  boolean hasMore = false;
	  boolean hasPrevious = false;
	  long firstRec = 0;
	  long lastRec = 0;
	  long totalRows = 0;

	  rowsPerPage =   Configuration.MOREBROWSERROWS ;
	  totalPages  =    Configuration.PAGEPERBROWSER ;




          BrowserRows br = new BrowserRows();





	  br.getPageRows(curPage,rowsPerPage,mysql ,totalPages,countSql,orderBy,orderType);

   	  rowsReturned = br.getRowReturned();
	  showPages = br.getShowPages();
	  startPage = br.getStartPage();

	  hasMore = br.getHasMore();
	  hasPrevious = br.getHasPrevious();
	  totalRows = br.getTotalRows();
	  firstRec = br.getFirstRec();
	  lastRec = br.getLastRec();

    %>
    <input type="hidden" name=srcmenu value="<%=src%>">
   	<Input type="hidden" name="selectedTab" value="6">
	<Input type="hidden" name="pkey" value="<%=personPK%>">
	<Input type="hidden" name="studyId" value="<%=studyId%>">
	<Input type="hidden" name="currentpage" value="<%=curPage%>">

   <p class="defcomments"><A href="#" onClick="setValue('','','remove')"><%=MC.M_RemAlready_SelcAdv%><%--Remove Already Selected Adverse Event*****--%></A></p>
    <TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" class="basetbl">
<%-- Nicholas : Start --%>    
  <TR>  
<%-- Nicholas : End --%>
        <TH></TH>
        <TH onClick="setOrder(document.adeventpopup,'advId')" class="th_fontStyle" ><%=LC.L_AE_RES_ID%><%--Response ID*****--%></TH>
	 <TH onClick="setOrder(document.adeventpopup,'advType')" class="th_fontStyle" ><%=LC.L_AdverseEvent_Type%><%--Type*****--%></TH>
        <TH onClick="setOrder(document.adeventpopup,'advGrade')" class="th_fontStyle" ><%=LC.L_Adverse_EventOrGrading%><%--Adverse Event/Grading*****--%></TH>
	<TH onClick="setOrder(document.adeventpopup,'attribution')" class="th_fontStyle" ><%=LC.L_Attribution%><%--Attribution*****--%></TH>
        <TH onClick="setOrder(document.adeventpopup,'advSdate')"  class="th_fontStyle"><%=LC.L_Start_Date%><%--Start Date*****--%> </TH>
        <TH onClick="setOrder(document.adeventpopup,'advEdate')" class="th_fontStyle" ><%=LC.L_Stop_Date%><%--Stop Date*****--%> </TH>
        <TH onClick="setOrder(document.adeventpopup,'advUser')" class="th_fontStyle" ><%=LC.L_Entered_By%><%--Entered By*****--%> </TH>
	<TH onClick="setOrder(document.adeventpopup,'FORM_STATUS')"  class="th_fontStyle"><%=LC.L_AE_STATUS%><%--Form Status*****--%> </TH>


      </TR>
    <%




      //populate form status
      SchCodeDao cdFormStatus = new SchCodeDao();
      cdFormStatus.getCodeValues("fillformstat");

    	String advId =null;
    	String advType =null;
    	String advDesc =null;
    	String advGrade="";
    	String advName="";
	String attribution= null;
	String advStartDate =null;
    	String advEndDate =null;

    	String advUser =null;
	String advFormStatus = null;
	String statusDesc = "";
  	int count = 0;


	for(count = 1 ; count <= rowsReturned ; count++)
    	{



			advId= br.getBValues(count,"advId");

			advType= br.getBValues(count,"advType");

			advDesc= ((br.getBValues(count,"advDesc") == null)?"-":br.getBValues(count,"advDesc"));

			advGrade= ((br.getBValues(count,"advGrade") == null)?"-":br.getBValues(count,"advGrade"));

			/* advName =  ((br.getBValues(count,"advName") == null)?"-":br.getBValues(count,"advName"));			*/
			advName = br.getBValues(count,"advName");

			if(advName == null){
			  advName="";

			}




			attribution= ((br.getBValues(count,"attribution") == null)?"-":br.getBValues(count,"attribution"));

			advStartDate=  ((br.getBValues(count,"advSdate") == null)?"-":br.getBValues(count,"advSdate"));

			advEndDate=  ((br.getBValues(count,"advEdate") == null)?"-":br.getBValues(count,"advEdate"));

			advUser= br.getBValues(count,"advUser");

			advFormStatus = br.getBValues(count,"FORM_STATUS");


		if (! EJBUtil.isEmpty(advFormStatus))
		{
			statusDesc = cdFormStatus.getDesc(advFormStatus);
		}
		else
		{
			statusDesc = "-";
		}

    		if(!advStartDate.equals("-"))
    		{
    		//advStartDate = advStartDate .toString().substring(5,7) + "/" + advStartDate .toString().substring(8,10) + "/" + advStartDate .toString().substring(0,4);
    		java.sql.Date advStartDateDt = java.sql.Date.valueOf(advStartDate);
    		advStartDate = DateUtil.dateToString(advStartDateDt);

    		}
    		if(!advEndDate.equals("-")){
    		//advEndDate = advEndDate .toString().substring(5,7) + "/" + advEndDate .toString().substring(8,10) + "/" + advEndDate.toString().substring(0,4);
    		java.sql.Date advEndDateDt = java.sql.Date.valueOf(advEndDate);
    		advEndDate = DateUtil.dateToString(advEndDateDt);
    		}
    	%>

    	<%
    	if(count%2==0){
    	%>
    	<TR class="browserEvenRow">
    <%
    	}else{
    %>
    	<TR class="browserOddRow">
    <%
    	}
    %>
       <td>
      		<A href="#" onclick="setValue('<%=advId%>','<%=advName%>','')"><%=LC.L_Select%><%--Select*****--%></A>

      </td>
				<td class=tdDefault align="center" )"><%=advId%></td>
      		    <td class=tdDefault align="center" )"><%=advType%></td>
       		     <%if ((advGrade.length()==0) && (advName.length()==0)) {%>
    			 <td class=tdDefault align="center">-</td>
    			 <%}else{%>
    			 <td class=tdDefault align="center"><%=LC.L_Grade %><%--Grade*****--%>: <%=advGrade%> <%=advName%></td>
    			 <%}%>

    			 <td class=tdDefault align="center"><%=attribution%></td>
    			 <td class=tdDefault align="center"><%=advStartDate %></td>
    			 <td class=tdDefault align="center"><%=advEndDate %></td>
    			 <td class=tdDefault align="center"><%=advUser%></td>
			 <td class=tdDefault align="center"><%=statusDesc%></td>
       			 <td class=tdDefault align="center"></td>



    			 </tr>

    	<%
    %>

    <%
    	}
      %>

      </TR-->
    </TABLE>
     <table>
	<tr>
	<td>
	<% if (totalRows > 0) { Object[] arguments = {firstRec,lastRec,totalRows}; %>
		<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%--<font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
	<%} %>
	</td>
	</tr>
	</table>
	<table align=center>

	<%
	if (curPage==1) startPage=1;

    for (count = 1; count <= showPages;count++)
	{
        cntr = (startPage - 1) + count;
     	if ((count == 1) && (hasPrevious))
	{
    %>
	<td colspan = 2>
  	<A HREF="adveventlookup.jsp?srcmenu=<%=src%>&selectedTab=6&mode=M&personPK=<%=personPK%>&currentpage=<%=cntr-1%>&adveventId=<%=advId%>&studyId=<%=studyId%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>">< <%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%> ></A>
	</td>
	<%
  	}
	%>
	<td>
	<%

	 if (curPage  == cntr)
	 {
     %>
	<FONT class = "pageNumber"><%=cntr %></Font>
       <%
       }
      else
        {
       %>


	   <A HREF="adveventlookup.jsp?srcmenu=<%=src%>&selectedTab=6&mode=M&personPK=<%=personPK%>&studyId=<%=studyId%>&currentpage=<%=cntr%>&adveventId=<%=advId%>&orderBy=<%=orderBy%>&&orderType=<%=orderType%>"><%= cntr%></A>
       <%
    	}
	 %>
	</td>
	<%
	  }

	if (hasMore)
	{
   %>
   <td colspan = 3 align = center>
  	&nbsp;&nbsp;&nbsp;&nbsp;
  	  <A HREF="adveventlookup.jsp?srcmenu=<%=src%>&selectedTab=6&mode=M&personPK=<%=personPK%>&studyId=<%=studyId%>&orderBy=<%=orderBy%>&&orderType=<%=orderType%>&currentpage=<%=cntr+1%>">< <%=LC.L_Next%><%--Next*****--%> <%=totalPages%>></A>
	</td>
	<%
  	}
	%>
   </tr>
  </table>
  <input type="hidden" name="orderBy" value="<%=orderBy%>">
  <input type="hidden" name="orderType" value="<%=orderType%>">
  <input type="hidden" name="personPK" value="<%=personPK%>">
  <input type="hidden" name="studyId" value="<%=studyId%>">
</form>

<!--<body style="overflow:scroll;">-->


</body>
</html>
