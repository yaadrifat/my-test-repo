<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<%
boolean isIrb = "irb_personnel_tab".equals(request.getParameter("selectedTab")) ? true : false;
String nextSelectedTab = "";
String parentNtwId=request.getParameter("parentNtwId");
if (request.getParameter("selectedTab") != null &&
        request.getParameter("selectedTab").startsWith("irb")) {
    isIrb = true;
    nextSelectedTab = request.getParameter("selectedTab");
}
String isPopupWin = "0";
if("LIND".equals(CFG.EIRB_MODE) && request.getParameter("isPopupWin")!=null){
	isPopupWin = request.getParameter("isPopupWin");
}
if (isIrb) {
%>
    <title><%=MC.M_ResComp_NewStdPers%><%--Research Compliance >> New Application >> <%=LC.Std_Study%> Personnel*****--%></title>
<% } else {  %>
	<title><%=LC.L_Status_History%><%--Status History*****--%></title>
<% }  %>
	
	<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
	<%@ page import="com.velos.eres.business.common.*, com.velos.eres.service.util.*,com.velos.eres.web.user.UserJB,com.velos.eres.web.grpRights.GrpRightsJB"%>
	<%@page import="com.velos.eres.widget.business.common.UIFlxPageDao"%>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<SCRIPT Language="javascript">
	function confirmBox() {
		msg="<%=MC.M_Del_StatusFromHist%>";/*msg="Delete Status from history?";*****/
		if (confirm(msg)) {
			return true;
		}else{
			return false;
		}
	}


	function confirmBoxPortal(isCurrentStat, pgRight) {

		if(isCurrentStat==1) {
		   alert("<%=MC.M_YouCntDel_CurStatus%>");/*alert("You cannot delete current status");*****/
		   return false;
		}else{

		  if ( pgRight>=6 ) {
			if (f_check_perm(pgRight,'E') == true) {
				msg="<%=MC.M_Del_StatusFromHist%>";/*msg="Delete Status from history?";*****/
				if (confirm(msg)) {
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		 }else {
		 alert("<%=LC.L_Edit_PermDenied%>");/*alert("Edit Permission denied");*****/
		 return false;
		 }
		}
	}

//Added by Manimaran to fix the Bug 3041
	function confirmBoxTeam(isCurrentStat) {
		if(isCurrentStat==1) {
		   alert("<%=MC.M_YouCntDel_CurStatus%>");/*alert("You cannot delete current status");*****/
		   return false;
		}else{
			msg="<%=MC.M_Del_StatusFromHist%>";/*msg="Delete Status from history?";*****/
			if (confirm(msg)) {
				return true;
			}else{
				return false;
			}
		}
	}


function openWinStatusInvoice(pgRight,mode,invId,invNumber,statusId, studyNumber) {
		var checkType;
		if (mode=='N')
			checkType = 'N';
		else
			checkType = 'E';
		var otherParam;
		otherParam = "&moduleTable=er_invoice&statusCodeType=invStatus&sectionHeading=<%=MC.M_MstoneInv_StatDets%>&statusId=" + statusId;/*otherParam = "&moduleTable=er_invoice&statusCodeType=invStatus&sectionHeading=Milestones >> Invoicing >> Status Details&statusId=" + statusId;*****/
		if (f_check_perm(pgRight,checkType) == true) {
			windowName= window.open("editstatus.jsp?mode=" + mode + "&modulePk=" +  invId + "&invNumber=" + invNumber + "&studyNumber=" + studyNumber + otherParam,"statusdetails","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=500,height=300");
			windowName.focus();
		}
	}


//JM: 11/18/2005: Modified: added 'verNumber' parameter
//function openWinStatus(pgRight,mode,studyVerId,statusId)
	function openWinStatus(pgRight,mode,studyVerId,verNumber,statusId) {
		var checkType;
		if (mode=='N')
			checkType = 'N';
		else
			checkType = 'E';
		var otherParam;
		otherParam = "&moduleTable=er_studyver&statusCodeType=versionStatus&sectionHeading=<%=isIrb?LC.L_Status_Dets:MC.M_MngPcolVer_StatDets%>&statusId=" + statusId;/*otherParam = "&moduleTable=er_studyver&statusCodeType=versionStatus&sectionHeading=<%=isIrb?"":"Manage Protocols >> Versions >> "%>Status Details&statusId=" + statusId;*****/
		if (f_check_perm(pgRight,checkType) == true) {
			windowName= window.open("editstatus.jsp?mode=" + mode + "&modulePk=" +  studyVerId + "&verNumber=" + verNumber + otherParam,"statusdetails","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=500,height=300");
			windowName.focus();
		}
	}
	//Added by Manimaran for the July-August Enhancement S4.
	function openeditstatus(pgRight,teamId,statusId,teamStat,mode,userName,isCurrentStat){			//KM-Modified to fix the Bug2716
		if(f_check_perm(pgRight,'E') == true) {
		windowname=window.open("editStudyTeamStatus.jsp?moduleTable=er_studyteam&statusCodeType=teamstatus&teamId="+teamId+"&statusId="+statusId+"&teamStat="+teamStat+"&mode="+mode+"&userName='"+userName+"'&isCurrentStat="+isCurrentStat ,"Information","toolbar=no,resizable=yes,scrollbars=no,menubar=no,status=yes,width=650,height=320 top=120,left=200, ");
		windowname.focus();
		}
	}

	function openeditstdntstatus(pgRight,mode, studyId, networkId,statusId){
		 
		var checkType;

		if (mode=='N')
			checkType = 'N';
		else
			checkType = 'E';

				var otherParam;

				    otherParam = "&moduleTable=er_nwsites&statusCodeType=sntwStat&sectionHeading=<%=StringUtil.encodeString(MC.M_StdNtwk_StatDets)%>&statusId=" + statusId;

					if (f_check_perm(pgRight,checkType) == true) {

						windowName= window.open("editstatus.jsp?mode=" + mode + "&studyId="+ studyId+"&modulePk=" +  networkId + otherParam,"statusdetails","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=800,height=600");
						windowName.focus();
					}
			}


      function openeditstatusportal(pgRight,portalId,statusId,portalStat,mode,portalName,isCurrentStat){
		windowname=window.open("editPortalStatus.jsp?moduleTable=er_portal&statusCodeType=portalstatus&portalId="+portalId+"&statusId="+statusId+"&portalStat="+portalStat+"&mode="+mode+"&portalName="+portalName+"&isCurrentStat="+isCurrentStat ,"Information","toolbar=no,resizable=yes,scrollbars=no,menubar=no,status=yes,width=450,height=275 top=120,left=200, ");
		windowname.focus();

	}
    
     function openWinUserStatus(pgRight,mode,moduleTable, networkuserId,statusId){
    		 
    		var checkType;

    		if (mode=='N')
    			checkType = 'N';
    		else
    			checkType = 'E';

    				var otherParam;

    					otherParam = "&statusCodeType=networkuserstat&sectionHeading=<%=MC.M_NtwUserStatus%>&statusId=" + statusId;

    					if (f_check_perm(pgRight,checkType) == true) {

    						windowName= window.open("editstatus.jsp?mode=" + mode + "&modulePk=" +  networkuserId + "&moduleTable=" +  moduleTable + otherParam,"statusdetails","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=800,height=600");
    						windowName.focus();
    					}
    }

     function openeditntwstatus(mode,networkId,statusId,pageRight){
     	var checkType;
		if (mode=='N')
			checkType = 'N';
		else
			checkType = 'E';
		if (f_check_perm(pageRight,checkType) == true){
    	var otherParam;
 		var networkId;
 		otherParam = "&moduleTable=er_nwsites&statusCodeType=networkstat&sectionHeading=<%=MC.M_Ntwk_StatDets%>&fromPage=networkTab&statusId=" + statusId;
 		windowName= window.open("editstatus.jsp?mode=" + mode + "&pageRight="+pageRight+"&modulePk=" +  networkId + otherParam,"statusdetails","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=800,height=600");
 		windowName.focus();
		}
    }  
      
      
</SCRIPT>
	<jsp:useBean id="statHistoryB" scope="request" class="com.velos.eres.web.statusHistory.StatusHistoryJB"/>
	<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<body>
	<%
		String studyId = "";
		String patId = "";
		String note = "";
		String status = "";
		String sDate = "";
		String eDate = "";
		//String from = "" ;
		String selectedTab ="" ;
		String statusId= "";
		String isCurrentStat="";
		Integer approvedCount = 0;
		int len = 0;
		int startDateLen = 0;
		int endDateLen = 0;
		Integer netId=0;
		String nLevel = "";		
		String moduleTable = request.getParameter("moduleTable");
		if(moduleTable.equalsIgnoreCase("er_nwusers") || moduleTable.equalsIgnoreCase("er_nwsites") || moduleTable.equalsIgnoreCase("er_nwusers_addnlroles")){
			isPopupWin = "1";
			netId = EJBUtil.stringToNum(request.getParameter("netWorkId"));
			nLevel = (request.getParameter("nLevel")==null)?"":request.getParameter("nLevel");
		}
		HttpSession tSession = request.getSession(true);
		String eSign ="";
	      if (sessionmaint.isValidSession(tSession))	{
	       eSign = (String) tSession.getValue("eSign");
	      }
		String srcmenu = request.getParameter("srcmenu");
		String tab = request.getParameter("selectedTab");
	   	int modulePk = EJBUtil.stringToNum(request.getParameter("modulePk"));
		String fromjsp = request.getParameter("fromjsp");
		String pageRight = request.getParameter("pageRight");
		String verNumber = request.getParameter("verNumber");
		String userName = request.getParameter("userName");
		String	portalName =request.getParameter("portalName");
		portalName = StringUtil.decodeString(portalName);//KM
		String statusDesc=request.getParameter("statusDesc")==null?"":request.getParameter("statusDesc");
		String invNumber = request.getParameter("invNumber");

		StatusHistoryDao shDao = new StatusHistoryDao();
		studyId = request.getParameter("studyId");
		boolean freezeFlag = false;
		if("LIND".equals(CFG.EIRB_MODE) && moduleTable.equals("er_studyver")){
			CodeDao freezeCode = new CodeDao();
			int pkFreezecode=freezeCode.getCodeId("versionStatus","F");
			String freezeDesc = freezeCode.getCodeDesc(pkFreezecode);
		    StatusHistoryDao shDao1 = new StatusHistoryDao();
		    shDao1 = statHistoryB.getStatusHistoryInfo(modulePk, moduleTable);
		    
		    UIFlxPageDao uiFlxPage = new UIFlxPageDao();
		  	ArrayList<String> lockedVersions = uiFlxPage.getAllLockedStudyVersions(EJBUtil.stringToNum(studyId));
		  	for (String version: lockedVersions){
		  		System.out.println("Locked Version::::"+version);
		  	}
		  	System.out.println("verNumber::::"+verNumber);
		  	if (lockedVersions.contains(verNumber) || statusDesc.equals(freezeDesc)){
			    freezeFlag = shDao1.getCodelstIds().contains(String.valueOf(pkFreezecode));
		  	}
		  	System.out.println("freezeFlag::::"+freezeFlag);
		}

	studyB.setId(EJBUtil.stringToNum(studyId));
	studyB.getStudyDetails();
	String studyNumber = studyB.getStudyNumber();

		ArrayList startDates = new ArrayList();
		ArrayList endDates = new ArrayList();
		ArrayList statusDescs = new ArrayList();
		ArrayList notes = new ArrayList();
		ArrayList statusIds = new ArrayList(); 
		ArrayList isCurrentStats= new ArrayList();//KM
		ArrayList codelstSubtypes = new ArrayList();
		ArrayList approvedCounter = new ArrayList();
		
		if(moduleTable.equals("er_nwsites"))
			shDao.getStatusHistoryInfo(modulePk, moduleTable,EJBUtil.stringToNum(studyId));
		else
			shDao = statHistoryB.getStatusHistoryInfo(modulePk, moduleTable);
		startDates = shDao.getStatusDates();
		endDates = shDao.getStatusEndDates();
		statusDescs = shDao.getCodelstDescs();
		codelstSubtypes = shDao.getCodelstSubTypes();
		notes = shDao.getStatusNotes();
		statusIds = shDao.getStatusIds();
		isCurrentStats = shDao.getIsCurrentStats();
		approvedCounter = shDao.getApprovedCounter();
   	  	len= shDao.getCRows();

		String from = request.getParameter("from");


   	  	//sonia 08/17/07 - to hide delete/edit links depending on parameters

   	  	String	showDeleteLink = request.getParameter("showDeleteLink");

   	  	if (StringUtil.isEmpty(showDeleteLink))
   	  	{
   	  		showDeleteLink  = "true";
   	  	}

   	  	String	showEditLink = request.getParameter("showEditLink");

   	  	if (StringUtil.isEmpty(showEditLink))
   	  	{
   	  		showEditLink  = "true";
   	  	}


%>
  <br>
		<P class="sectionHeadings"></P>
		<table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl midalign">
			<tr>
				<td width = "70%">
				<p class="sectionheadings"><%=LC.L_Status_History%><%--Status History*****--%></p>
				</td>
			</tr>
	   </table>
	   <table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl outline midalign" >
	       <tr>
				<th width="15%"> <%=LC.L_Start_Date%><%--Start Date*****--%></th>
		        <th width="15%"> <%=LC.L_End_Date%><%--End Date*****--%> </th>
		        <th width="20%"> <%=LC.L_Status%><%--Status*****--%> </th>
		        <th width="43%"> <%=LC.L_Notes%><%--Notes*****--%> </th>
				<th width="5%"><%=LC.L_Delete%><%--Delete*****--%></th>
	      </tr>
<%
	for(int counter = 0;counter<len;counter++){
		sDate=((startDates.get(counter)) == null)?"-":(startDates.get(counter)).toString();
		startDateLen=sDate.length();
		if (startDateLen > 1) {
			sDate = DateUtil.dateToString(java.sql.Date.valueOf(sDate.substring(0,10)));
		}
		eDate=((endDates.get(counter)) == null)?"-":(endDates.get(counter)).toString();
		endDateLen=eDate.length();
		if (endDateLen > 1) {
			eDate = DateUtil.dateToString(java.sql.Date.valueOf(eDate.substring(0,10)));
		}
			status=((statusDescs.get(counter)) == null)?"-":(statusDescs.get(counter)).toString();
			note=((notes.get(counter)) == null)?"-":(notes.get(counter)).toString();
			statusId = (String)statusIds.get(counter).toString();
			isCurrentStat = (isCurrentStats.get(counter) == null)?"":(isCurrentStats.get(counter)).toString();
			approvedCount = (approvedCounter!=null && approvedCounter.size()>0)?(Integer) approvedCounter.get(counter):0;
		if ((counter%2)==0) {
	  %>
      <tr class="browserEvenRow">
        <%
	}else{
  %>
	  <tr class="browserOddRow">
<%
	}
%>
		<td align=center><%=sDate%> </td>
		<td align=center><%=eDate%></td>


<%
	if(moduleTable.equals("er_invoice")){
%>
		<td align=center >
		<!--KM-Modified -->
		<A href="#" onclick="openWinStatusInvoice(<%=pageRight%>,'M','<%=modulePk%>','<%=invNumber%>','<%=statusId%>','<%=StringUtil.encodeString(studyNumber)%>')"> <%=status%></A>
		</td>
<% }
	else if(moduleTable.equals("er_studyver")){
		if("LIND".equals(CFG.EIRB_MODE)){
			if (!freezeFlag){%>
				<td align=center >
				<A href="#" onclick="openWinStatus(<%=pageRight%>,'M','<%=modulePk%>','<%=verNumber%>','<%=statusId%>')"> <%=status%></A>
				</td>
			<%}else{%>
			<td align=center >
				<%=status%>
			</td>
			<%}
		}else{
%>
		<td align=center >
			<A href="#" onclick="openWinStatus(<%=pageRight%>,'M','<%=modulePk%>','<%=verNumber%>','<%=statusId%>')"> <%=status%></A>
		</td>
<% }}

else if(moduleTable.equals("er_portal")){

%>
         <td align=center >
			<% if ( showEditLink.equals("true") ) {  %>
				<A href="#" onclick="openeditstatusportal(<%=pageRight%>,<%=modulePk%>,<%=statusId%>,'<%=status%>','M','<%=StringUtil.encodeString(portalName)%>',<%=isCurrentStat%>)"> <%=status%></A>
			  <% } else {
			  %>
			    	<%=status%>
			  <%

			  }
			%>
		</td>

<% }
else if(moduleTable.equals("er_nwsites"))  //Added by Manimaran for the July-August Enhancement S4.
{
	if(EJBUtil.stringToNum(studyId)==0){
%>
	<td><!--KM-->
		<A href="#" onClick="openeditntwstatus('M',<%=modulePk%>,<%=statusId%>,<%=pageRight%>)"><%=status%></A> <!--KM: to fix 3014 -->
	</td>
<%}else{%>
	<td><!--KM-->
		<A href="#" onClick="openeditstdntstatus(<%=pageRight%>,'M',<%=studyId%>,<%=modulePk%>,<%=statusId%>)"><%=status%></A> <!--KM: to fix 3014 -->
	</td>
	<%}
}
else if(moduleTable.equals("er_nwusers") || moduleTable.equals("er_nwusers_addnlroles"))  //Added by Manimaran for the July-August Enhancement S4.
{
%>
	<td><!--KM-->
		<A href="#" onClick="openWinUserStatus(<%=pageRight%>,'M','<%=moduleTable %>','<%=modulePk%>','<%=statusId%>')"><%=status%></A> <!--KM: to fix 3014 -->
	</td>
<%}
else  //Added by Manimaran for the July-August Enhancement S4.
	{
%>
		<td><!--KM-->
			<A href="#" onClick="openeditstatus(<%=pageRight%>,<%=modulePk%>,<%=statusId%>,'<%=status%>','M','<%=userName%>',<%=isCurrentStat%>)"><%=status%></A> <!--KM: to fix 3014 -->
		</td>
<%}%>


		<td align=left><%=note%></td>

		<!--  sonia 8/17 generic code totally messed up. why do we have if - else for moduleTable all over? -->

		<%
		 if(moduleTable.equals("er_studyver")){
			if("LIND".equals(CFG.EIRB_MODE)){
				if (!freezeFlag){
				if( showDeleteLink.equals("true") ) {%>
					<td align=center >
					<a href="deleteStatusHistory.jsp?statusId=<%=statusId%>&studyId=<%=studyId %>&moduleTable=<%=moduleTable%>&modulePk=<%=modulePk%>&srcmenu=<%=srcmenu%>&selectedTab=<%=tab%>&fromjsp=<%=fromjsp%>&from=<%=from%>&verNumber=<%=StringUtil.encodeString(verNumber)%>&isPopupWin=<%=isPopupWin %>" onclick="return confirmBox()"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0"/>
					</td>
				<%}}else if ((codelstSubtypes!=null && codelstSubtypes.size()>0 && codelstSubtypes.get(counter).toString().equals("A")) && approvedCount.intValue()>0){
					if( showDeleteLink.equals("true") ) {%>
					<td align=center >
					<a href="deleteStatusHistory.jsp?statusId=<%=statusId%>&studyId=<%=studyId %>&moduleTable=<%=moduleTable%>&modulePk=<%=modulePk%>&srcmenu=<%=srcmenu%>&selectedTab=<%=tab%>&fromjsp=<%=fromjsp%>&from=<%=from%>&verNumber=<%=StringUtil.encodeString(verNumber)%>&isPopupWin=<%=isPopupWin %>" onclick="return confirmBox()"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0"/>
					</td>
					<%}}else{%>
				<td align=center >
					-
				</td>
				<%}
			}else{
				if( showDeleteLink.equals("true") ) {%>
			<td align=center >
				<a href="deleteStatusHistory.jsp?statusId=<%=statusId%>&moduleTable=<%=moduleTable%>&modulePk=<%=modulePk%>&srcmenu=<%=srcmenu%>&selectedTab=<%=tab%>&fromjsp=<%=fromjsp%>&from=<%=from%>&verNumber=<%=StringUtil.encodeString(verNumber)%>" onclick="return confirmBox()"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0"/>
			</td>
	<% }}}else if (moduleTable.equals("er_portal")){
		%>              <!--KM-to fix the Bug 2973
							JM: 14Jun2007: changed  -->
			<!--Modified by Manimaran to fix the issue 3040 -->

			<td align="center">
			<% if ( showDeleteLink.equals("true") ) {  %>

					<a 	href="deleteStatusHistory.jsp?statusId=<%=statusId%>&moduleTable=<%=moduleTable%>&modulePk=<%=modulePk%>&srcmenu=<%=srcmenu%>&selectedTab=<%=tab%>&fromjsp=showPortalHistory.jsp&from=portalHistory&userName=<%=userName%>&portalName=<%=StringUtil.encodeString(portalName)%>&pageRight=<%=pageRight%>" onclick="return confirmBoxPortal(<%=isCurrentStat%>, <%=pageRight%>)"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0"/>
					</a>
			<% } %>
			</td>

<%			}else if(moduleTable.equals("er_nwusers") || moduleTable.equalsIgnoreCase("ER_NWUSERS_ADDNLROLES")){
%>
			<td align="center">
			<% if ( showDeleteLink.equals("true") ) {  %>

					<a 	href="deleteStatusHistory.jsp?statusId=<%=statusId%>&moduleTable=<%=moduleTable%>&modulePk=<%=modulePk%>&srcmenu=<%=srcmenu%>&selectedTab=<%=tab%>&fromjsp=showinvoicehistory.jsp&from=ntwusrhistory&userName=<%=userName%>&pageRight=<%=pageRight%>&netWorkId=<%=netId %>&nLevel=<%=nLevel %>&delMode=er_newsites&eSign=<%=eSign%>" onclick="return confirmBox()"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0"/>
					</a>
			<% } %>
			</td>
	
<%			}else if(moduleTable.equals("er_nwsites") && EJBUtil.stringToNum(studyId)==0){
%>
			<td align="center">
			<% if (EJBUtil.stringToNum(pageRight)>=6 ) {  %>
					<a href="deleteStatusHistory.jsp?statusId=<%=statusId%>&moduleTable=<%=moduleTable%>&modulePk=<%=modulePk%>&srcmenu=<%=srcmenu%>&selectedTab=<%=tab%>&fromjsp=showinvoicehistory.jsp&from=ntwhistory&pageRight=<%=pageRight%>&eSign=<%=eSign%>&delMode=er_newsites&parentNtwId=<%=parentNtwId %>" onclick="return confirmBox()"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0"/>
					</a>
			<% } %>
			</td>
			<%}else{
%>
			<td align="center">&nbsp;
			<% if ( showDeleteLink.equals("true") ) {  %>


							<a 	href="deleteStatusHistory.jsp?statusId=<%=statusId%>&moduleTable=<%=moduleTable%>&modulePk=<%=modulePk%>&srcmenu=<%=srcmenu%>&selectedTab=<%=tab%>&fromjsp=<%=fromjsp%>&from=<%=from%>&studyId=<%=studyId%>&verNumber=<%=verNumber%>&userName=<%=userName%>&from=studyTeamHistory&pageRight=<%=pageRight%>&invNumber=<%=invNumber%>&eSign=<%=eSign%>&delMode=er_newsites" onclick="return confirmBox()"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0"/>
							</a>

					<% } %>
			<%}%>
			</td>
		</tr>
<% } %>

		<tr><td>&nbsp;</td></tr>
		<tr>

			<td colspan="5" align=center>
<%
				if(moduleTable.equals("er_studyver"))	{
				    nextSelectedTab = isIrb ? nextSelectedTab : "2";
				 if("LIND".equals(CFG.EIRB_MODE)){ 
					 if(isPopupWin.equals("1")){%>
						  <a href="studyVerBrowser.jsp?srcmenu=tdmenubaritem3&selectedTab=<%=nextSelectedTab%>&mode=N&studyId=<%=studyId %>&isPopupWin=<%=isPopupWin %>" type="submit">
						<%=LC.L_Back%></a>
					 <%}else if(isIrb!=true){
%>				<!--Modified by Manimaran to fix the issue 3005-->
				<a href="studyAttachments?srcmenu=tdmenubaritem3&selectedTab=<%=nextSelectedTab%>&studyId=<%=studyId %>&isPopupWin=<%=isPopupWin %>" type="submit">
					<%=LC.L_Back%></a>
<%				
				 }else{
					 %>
					 <a href="studyAttachmentsLindFt?srcmenu=tdmenubaritem3&selectedTab=<%=nextSelectedTab%>&studyId=<%=studyId %>&isPopupWin=<%=isPopupWin %>" type="submit">
						<%=LC.L_Back%></a>
					 <%
				 }
					 
				 }else{%>
					 <a href="studyVerBrowser.jsp?srcmenu=tdmenubaritem3&selectedTab=<%=nextSelectedTab%>&mode=N&studyId=<%=studyId %>&isPopupWin=<%=isPopupWin %>" type="submit">
						<%=LC.L_Back%></a>
				<% }
				 }else if (moduleTable.equals("er_invoice")){
%>
					<a href="invoicebrowser.jsp?srcmenu=tdmenubaritem7&selectedTab=7&studyId=<%=studyId%>" type="submit">
					<%=LC.L_Back%></a>
<%				}else if (moduleTable.equals("er_portal")){
%>
					<a href="portal.jsp?srcmenu=tdmenubaritem3&selectedTab=5" type="submit">
					<%=LC.L_Back%></a>
<%				}else if (moduleTable.equals("er_nwsites") && EJBUtil.stringToNum(studyId)!=0){
%>
				<a href="studyNetworkTab.jsp?srcmenu=tdmenubaritem3&selectedTab=13&studyId=<%=studyId%>&fromPage=studyNetwork" type="submit">
				<%=LC.L_Back%></a>
<%				}else if (moduleTable.equals("er_nwsites") && EJBUtil.stringToNum(studyId)==0){
%>
				<%-- <a href="networkTabs.jsp?srcmenu=tdmenubaritem3&selectedTab=7&parentNtwId=<%=parentNtwId %>" type="submit">
				<%=LC.L_Back%></a> --%>
				<button onclick= "javascript:self.close()"><%=LC.L_Close%></button>
				
<%				}else if (moduleTable.equalsIgnoreCase("er_nwusers") || moduleTable.equalsIgnoreCase("ER_NWUSERS_ADDNLROLES")){
%>
				<a href="usersnetworksites.jsp?networkId=<%=netId %>&pageRight=<%=pageRight %>&nLevel=<%=nLevel %>" type="submit">
				<%=LC.L_Back%></a>
<%				}
				else {
                    nextSelectedTab = isIrb ? nextSelectedTab : "5";
%>
					<a href="teamBrowser.jsp?srcmenu=tdmenubaritem3&selectedTab=<%=nextSelectedTab%>&studyId=<%=studyId%>" type="submit">
					<%=LC.L_Back%></a>
<%				}
%>
			</td>
		</tr>
	</table>
</body>

</html>



