<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<title><%=LC.L_Bgt_UpldFile%><%-- Budget >> Upload File*****--%></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</HEAD>


<%
String src= request.getParameter("srcmenu");
String 	bottomdivClass="tabDefBotN";
%>

<% if ("S".equals(request.getParameter("budgetTemplate"))) { %>
	<jsp:include page="panel.jsp" flush="true">
	<jsp:param name="src" value="<%=src%>"/>
	</jsp:include>   
<% } else { 
    bottomdivClass="popDefault";
	%>
    <jsp:include page="include.jsp" flush="true"/> 
<% } %>



<SCRIPT Language="javascript">

 function  validate(formobj){;
	 mode = formobj.mode.value;
	 if (mode == 'N')
	 {
	     if (!(validate_col('File',formobj.name))) return false
	 }
     if (!(validate_col('Description',formobj.desc))) return false
     if (!(validate_col('e-Sign',formobj.eSign))) return false
	 <%-- if(isNaN(formobj.eSign.value) == true) {
		 alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
		} --%>
		 if(formobj.desc.value.length>500){  
	    	 alert("<%=MC.M_ShortDesc_MaxCharsAllwd%>");/*alert(" 'Short Description' exceeded maximum number of characters allowed.");*****/
		     formobj.desc.focus();
		     return false;
	     } 
	     if (!(checkquote(formobj.desc.value))) return false 
 }


function openwin() {

      window.open("","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=600,height=450")

;}

</SCRIPT>

<BODY>
<jsp:useBean id ="sessionmaint" scope="request" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id ="bgtapndx" scope="request" class="com.velos.esch.web.bgtApndx.BgtApndxJB"/>

<%@ page  language = "java" import="com.velos.esch.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*"%>
<br>
<DIV class="tabDefTopN" id="div1"> 

  <%

HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession))

{
	String accId = (String) tSession.getAttribute("accountId"); 
	String userId = (String) tSession.getAttribute("userId");
	String uName = (String) tSession.getAttribute("userName");
    String bgtId = request.getParameter("bgtId");
	String ipAdd = (String) tSession.getAttribute("ipAdd");
	String mode = (String) request.getParameter("mode");
	String selectedTab = request.getParameter("selectedTab");
	String budgetTemplate=request.getParameter("budgetTemplate");
	String budgetMode =request.getParameter("budgetMode");	
	String accMaxStorage = (String) tSession.getAttribute("accMaxStorage");
	String fromRt = request.getParameter("fromRt");
	if (fromRt==null || fromRt.equals("null")) fromRt="";
	
	String bgtApndxDesc = "";
	String apndxId = "";
		

	CtrlDao ctrlDao = new CtrlDao();
	long freeSpace = ctrlDao.getFreeSpace(EJBUtil.stringToNum(accId));

%>
  
  <%
  if (mode.equals("N"))
  {
	 com.aithent.file.uploadDownload.Configuration.readSettings("sch");
	com.aithent.file.uploadDownload.Configuration.readUploadDownloadParam(com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD + "fileUploadDownload.xml", "budget");	   
	String upld=com.aithent.file.uploadDownload.Configuration.UPLOADSERVLET;
   	%>

  <form name=upload id="bdgtfile" action=<%=upld%> METHOD=POST ENCTYPE=multipart/form-data onSubmit=" if (validate(document.upload)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
  <%
  }
  else
  {
	apndxId = (String) request.getParameter("apndxId");
	bgtapndx.setBgtApndxId(com.velos.eres.service.util.EJBUtil.stringToNum(apndxId));  
	bgtapndx.getBgtApndxDetails();
	bgtApndxDesc = bgtapndx.getBgtApndxDesc(); 
  
  %>
  <form name="upload" id="bdgtfile" action="budgetfileupdate.jsp" METHOD=POST onSubmit="if (validate(document.upload)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
  <%
  }
   %>

   </DIV>
	<DIV class="<%=bottomdivClass%>" id="div2">
    <table width="98%" >
      <tr> 
        <td colspan="2" align="center"> 
		<%
		if (mode.equals("N"))
		  {
	   %> 
          <P class = "sectionHeadingsFrm">  <%=MC.M_UploadDocs_ToBgtsAppx%><%-- Upload Documents to your Budget's Appendix*****--%> 
		  </P>
		 <%
			}
		 else
		  {
		%>
	      <P class = "defComments"> <b> <%=LC.L_Edit_ShortDesc%><%-- Edit Short Description*****--%> </b>
		  </P>
	
		<%
		  }
		%>   
		    
        </td>
      </tr>
    <tr><td>&nbsp;</td></tr>
	  <%
	  if (mode.equals("N"))
	  {
	   %>
      <tr> 
        <td width="20%" align="right"> <b> <%=LC.L_File%><%-- File*****--%> </b> <FONT class="Mandatory">* </FONT> &nbsp; &nbsp;</td>
        <td width="65%"> 
          <input type=file name=name size=40 >
		  <input type=hidden name=db value='sch'>
		  <input type=hidden name=module value='budget'>		  
		  <input type=hidden name=tableName value='SCH_BGTAPNDX'>
		  <input type=hidden name=columnName value='BGTAPNDX_FILEOBJ'>
		  <input type=hidden name=pkValue value='0'>
		  <input type=hidden name=pkColumnName value='PK_BGTAPNDX'>
		  		  
       
		  <input type=hidden name=nextPage value='../../velos/jsp/budgetapndx.jsp?mode=<%=budgetMode%>&budgetTemplate=<%=budgetTemplate%>&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&budgetId=<%=bgtId%>&fromRt=<%=fromRt%>'>	   
	   
	    </td>
      </tr>
      <tr> 
        <td > </td>
        <td > 
          <P class="defComments"> <%=MC.M_Specify_FullFilePath%><%-- Specify full path of the file.*****--%> </P>
        </td>
      </tr>
	  <%
	  }
	  %>
      <tr><td>&nbsp;</td></tr>
      	  <tr> 
        <td align="right"> <b><%=LC.L_Short_Desc%><%-- Short Description*****--%> </b> <FONT class="Mandatory" >* </FONT> &nbsp; &nbsp;
        </td>
        <td >
		<%
		if (mode.equals("N"))
		  {
	   %>
		  <!--  <input type=text name=desc MAXLENGTH=250 size=40> -->
		    <TextArea type=text name=desc row=3 cols=50 ></TextArea>
		<%
			}
		 else
		  {
		%>
		    <%-- <input type=text name=desc MAXLENGTH=250 size=40 value='<%=bgtApndxDesc%>'> --%>
		    <TextArea type=text name=desc row=3 cols=50 ><%=bgtApndxDesc%></TextArea>
		<%
		  }
		%>   
		   
        </td>
      </tr>
      <tr> 
        <td > </td>
        <td > 
          <P class="defComments"> <%=MC.M_ShortDescFile_500CharMax%><%-- Give a short description of your file (500 char  max.)*****--%> </P>
        </td>
      </tr>
      		<tr><td>&nbsp;</td></tr>
      		<tr><td>&nbsp;</td></tr>
    </table>
   
  	<input type=hidden name=accId value=<%=accId%>>
  	<input type=hidden name=userId value=<%=userId%>>
	<input type=hidden name=bgtId value=<%=bgtId%>>
	<input type=hidden name=ipAdd value=<%=ipAdd%>>
	<input type=hidden name=accountId value=<%=accId%>>
	<input type=hidden name=maxFileSize value=<%=freeSpace%>>
	<input type=hidden name=accMaxStorage value=<%=accMaxStorage%>> 
	
	<Input type="hidden" name="fromRt" value="<%=fromRt%>" />
    <input type="hidden" name="type" value='F'>
    <input type="hidden" name="mode" value=<%=mode%>>
	<input type="hidden" name="srcmenu" value=<%=src%>>
	<input type="hidden" name="budgetTemplate" value=<%=budgetTemplate%>>
	<input type="hidden" name="budgetMode" value=<%=budgetMode%>>
	
  	<input type="hidden" name="selectedTab" value="<%=selectedTab%>">	
  
    <%
	  if (mode.equals("M"))
	  {
    %>
	   <input type="hidden" name="apndxId" value=<%=apndxId%>>
 	<%
	   }
	%>
	  
	  
	<BR>
    <jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="bdgtfile"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>

	 
  </form>
  <%

}//end of if body for session
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
  <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<DIV class="mainMenu" id = "emenu"> 
  <jsp:include page="getmenu.jsp" flush="true"/>
</DIV>
</body>

</html>
