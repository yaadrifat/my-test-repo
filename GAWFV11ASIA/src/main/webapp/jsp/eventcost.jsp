<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title><%=MC.M_EvtLib_EvtCost %><%-- Event Library >> Event Cost*****--%></title>



<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<% if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt"))

{ %>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>

<% } //end for if ((request.getParameter("fromPage")).equals("selectEvent"))

	

	%>



<SCRIPT language="javascript">



function confirmBox(fileName,pgRight) {



	if (f_check_perm(pgRight,'E') == true) {



		/*if (confirm("Delete " + fileName + " from Events?")) {*****/
		var paramArray = [fileName];
		if (confirm( getLocalizedMessageString("L_Del_FromEvt",paramArray))) {

		    return true;}

		else

		{

			return false;

		}

	} else {

		return false;

	}

}
/* Added For Bug# 11300 :Yogendra(INF-21649 Unable to add cost for 
 * the event for calendar status 'Deactivated') */	
function specifyCostRights(pgSCRight,accessSCRight){
	var specifyCostEditRights = false;
	var specifyCostNewRights = false;
	var specifyCostRights = false;
	if(f_check_perm(pgSCRight,'E',false)){
		specifyCostEditRights = true;
	}
	if(f_check_perm(pgSCRight,'N',false)){
		specifyCostNewRights = true;
	}
	if(specifyCostEditRights==true || 
			specifyCostNewRights==true ){
		specifyCostRights = true;
	}else{
		specifyCostRights = false;
	}
	return 	specifyCostRights;
}
</SCRIPT> 


<jsp:include page="skinChoser.jsp" flush="true"/>
</head>



<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.esch.business.common.EventInfoDao,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB"%>

<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="codelstB" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>


<% String src;



src= request.getParameter("srcmenu");
//SV, commented on 10/28/04, changes: eventname, propagation flags
String eventName= request.getParameter("eventName");
eventName = StringUtil.encodeString(eventName);



%>



<% if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt")){
%>
<jsp:include page="include.jsp" flush="true"/>
<%
}else{
%>

<jsp:include page="panel.jsp" flush="true"> 



<jsp:param name="src" value="<%=src%>"/>



</jsp:include>   

<%}%>





<body id="forms">



<%if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt")){%>
		<DIV class="popDefault" id="div1"> 
		<br>
	<%	} 

else { %>

<DIV class="browserDefault" id="div1"> 
<%}%>	


<%

	int pageRight = 0;
	int finDetRight = 0;
	String duration = request.getParameter("duration");   

	String protocolId = request.getParameter("protocolId");   

	String calledFrom = request.getParameter("calledFrom");   
	String eventId = request.getParameter("eventId");
	String mode = request.getParameter("mode");
	String fromPage = request.getParameter("fromPage");
	String calStatus = request.getParameter("calStatus");

	String eventmode = request.getParameter("eventmode");

	String displayDur=request.getParameter("displayDur");

	String displayType=request.getParameter("displayType");	  	

	HttpSession tSession = request.getSession(true); 



	if (sessionmaint.isValidSession(tSession))

	{

	   int count = 0;

	   String calAssoc = request.getParameter("calassoc");
       calAssoc = (calAssoc == null)?"":calAssoc;

	   if (calledFrom.equals("S")) {
		   if ("N".equals(eventmode)){
				GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
				pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));
				finDetRight=pageRight;
			} else {
				if ("selectEvent".equals(fromPage)){
					//Editing the just declared Library event
					GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
					pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));
					finDetRight=pageRight;
				} else {
	   	   	   		StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");	 
					if ((stdRights.getFtrRights().size()) == 0){
				 		pageRight= 0;
				 		finDetRight=0;
					}else{
						pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));
						finDetRight=Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFIN"));
	    	   		}
				}
			}
		} else if (calledFrom.equals("P")) {

				GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		

				pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));
				finDetRight=pageRight;
		}

		else if(calledFrom.equals("L")){

				GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		

				pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("EVLIB"));
				finDetRight=pageRight;
		
		}

	   
	   EventInfoDao eventinfoDao = eventdefB.getEventDefInfo(EJBUtil.stringToNum(eventId));

	   ArrayList costIds = eventinfoDao.getCostIds() ;

	   ArrayList costDescs = eventinfoDao.getCostDescriptions() ;

	   ArrayList costValues = eventinfoDao.getCostValues();

	   

	   ArrayList costCurs = eventinfoDao.getCurrencyIds();

	   

	   

	   String costId = "";

	   String costDesc = "";

	   String costValue = "";

	   String eventUserId = "";

	   String userType = "";

	   String userId ="";	


	   String cur = "";


	String calName = "";

	

	if (fromPage.equals("eventbrowser") || fromPage.equals("eventlibrary") || fromPage.equals("patientschedule") )
	{
	%>
		<!--P class="sectionHeadings"> Event Library >> Event Cost  </P-->
	<%
	}else{

		calName = (String) tSession.getValue("protocolname");
	%>
		<P class="sectionHeadings"><%Object[] arguments = {calName}; %>
	    <%=VelosResourceBundle.getMessageString("M_PcolCal_EvtCost",arguments)%><%--Protocol Calendar [ {0} ] >> Event Cost*****--%> </P>
	<%
	}

	//KM
	if (calledFrom.equals("P")||calledFrom.equals("L"))
    {
       	  eventdefB.setEvent_id(EJBUtil.stringToNum(eventId));
       	  eventdefB.getEventdefDetails();
          eventName = eventdefB.getName();
//JM: 12Nov2009: #4399
		  eventName = (eventName==null)?"":eventName;
		  if (eventName.length()>1000){//for the old existing event names > 1000 or larger
 	      	eventName=eventName.substring(0,1000);
          }

    }else{
       	  eventassocB.setEvent_id(EJBUtil.stringToNum(eventId));
          eventassocB.getEventAssocDetails();
       	  eventName = eventassocB.getName();		 
//JM: 12Nov2009: #4399
		  eventName = (eventName==null)?"":eventName;
		  if (eventName.length()>1000){//for the old existing event names > 1000 or larger
 	      	eventName=eventName.substring(0,1000);
          }

    }


%>

<jsp:include page="eventtabs.jsp" flush="true">

<jsp:param name="duration" value="<%=duration%>"/>

<jsp:param name="protocolId" value="<%=protocolId%>"/>

<jsp:param name="calledFrom" value="<%=calledFrom%>"/>

<jsp:param name="fromPage" value="<%=fromPage%>"/>

<jsp:param name="mode" value="<%=mode%>"/>

<jsp:param name="calStatus" value="<%=calStatus%>"/>

<jsp:param name="displayDur" value="<%=displayDur%>"/>

<jsp:param name="displayType" value="<%=displayType%>"/>

<jsp:param name="eventId" value="<%=eventId%>"/>

<jsp:param name="src" value="<%=src%>"/>

<jsp:param name="eventName" value="<%=eventName%>"/>

<jsp:param name="calassoc" value="<%=calAssoc%>"/>


</jsp:include>

<%

if(eventId == "" || eventId == null || eventId.equals("null") || eventId.equals("")) {

	%>

	  <jsp:include page="eventDoesNotExist.jsp" flush="true"/>

  <%

	}else{

  %>




<form METHOD=POST action="fetchProt.jsp?protocolId=<%=protocolId%>&srcmenu=<%=src%>&selectedTab=4&mode=<%=mode%>&calledFrom=<%=calledFrom%>&calStatus=<%=calStatus%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&eventName=<%=eventName%>">

 

<%         

//if ((calStatus.equals("A")) || (calStatus.equals("F"))){%>

         <!--  <P class = "defComments"><FONT class="Mandatory">Changes cannot be made to Event Interval and Event Details for an Active/Frozen Protocol</Font></P> -->

<%//}



if (finDetRight == 4) {

%>

   <P class = "defComments"><FONT class="Mandatory"><%=MC.M_OnlyViewPerm_ForEvtCost%><%-- You have only View permission for the Event Costs.*****--%></Font></P>

<%}%>



<%if (StringUtil.isAccessibleFor(finDetRight, 'V')) { %>

<TABLE width="90%">

   <tr>

	<!-- <td width="70%" colspan=2> Cost associated with this event </td> -->

	<td colspan=2>

	<%         

	/*if ((calStatus.equals("A")) || (calStatus.equals("F")))

	{

	}else

	{*/
	
	String m;
	if (fromPage.equals("eventlibrary")){
	  m="N"; 
	}
	else
	{
	if (mode.equals("N")){m="N";}else{m="E";}
	}
	%>	 

	<%if (StringUtil.isAccessibleFor(finDetRight, 'N')) { %>
	<A href="neweventcost.jsp?selectedTab=3&costmode=N&eventId=<%=eventId%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&eventmode=<%=eventmode%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&eventName=<%=eventName%>&calassoc=<%=calAssoc%>" onClick="return specifyCostRights(<%=finDetRight%>,'<%=m%>')"> <%=LC.L_Specify_Cost%><%-- Specify Cost*****--%></A>
	<%} %>
	<%

	//}

	%>

	</td>

	

   </tr>

   <tr>

	<td> <P class = "defComments"><%=LC.L_Assoc_Costs%><%-- Associated Costs are*****--%> </P> </td>

   </tr>

</TABLE>

<TABLE class="tableDefault" width="90%">



<tr> 

          <th width="50%"> <%=LC.L_Cost_Type%><%-- Cost Type*****--%> </th>

          <th width="45%"> <%=LC.L_Cost%><%-- Cost*****--%> </th>

		<%if (StringUtil.isAccessibleFor(finDetRight, 'E')) { %>
         <th width="45%"><%=LC.L_Delete%><%--Delete*****--%></th>
		<%} %>
</tr>





<% 

   for(int i=0;i<costIds.size(); i++) {

	costValue= (String)costValues.get(i);

	costDesc= (String)costDescs.get(i); 

	costId = (String)costIds.get(i); 

	cur = (String)costCurs.get(i);

	

		if ((i%2)==0) {



  %>

      <tr class="browserEvenRow"> 

        <%

		}

		else{

  %>

      <tr class="browserOddRow"> 

        <%

		}

  %>

	<td>

	<%if (StringUtil.isAccessibleFor(finDetRight, 'E')) { %>
	<A HREF="addeventcost.jsp?selectedTab=3&costmode=M&costId=<%=costId%>&eventId=<%=eventId%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&eventmode=<%=eventmode%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&eventName=<%=eventName%>&calassoc=<%=calAssoc%>" onClick="return f_check_perm(<%=finDetRight%>,'E')"> <%=costDesc %></A></td>
	<%} else { %>
	<%=costDesc %>
	<%} %>

	<td> <%=cur%>&nbsp;<span class="numberfield" data-unitsymbol="" data-formatas="currency"><%=costValue%></span></td>

	<%if (StringUtil.isAccessibleFor(finDetRight, 'E')) { %>
	<td width=15%> 
		<A HREF="costsave.jsp?costId=<%=costId%>&costmode=D&eventId=<%=eventId%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&eventmode=<%=eventmode%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&eventName=<%=eventName%>&calassoc=<%=calAssoc%>" onClick="return confirmBox('<%=costDesc%>',<%=finDetRight%>)"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" align="left"/></A>
	</td>
	<%} %>

   </tr>

<%

   }

%>

</TABLE>

<br>

<%} else {%>
	<table>
		<tr>
			<td>
				<p class = "sectionHeadings">
					<%=MC.M_NoRgtTo_ViewPage%><%--You do not have appropriate rights to view this page.*****--%>
				</p>
				<p class = "defComments"> 
					<%=MC.M_ObtainRgt_RptErr%><%--To obtain appropriate rights or to report an error, please contact your account administrator.*****--%>
				</p>
			</td>
		</tr>
	</table>
<%} %>
<table>

<tr>

  <td> 

   <%

  	if (fromPage.equals("selectEvent")) {

         %>   

		<A type="submit" href="selecteventus.jsp?fromPage=NewEvent" ><%=LC.L_Back%></A>

 <%

 	}

 %> 

  <%

  	if (fromPage.equals("eventbrowser")) {

  %>

		<A type="submit" href="eventbrowser.jsp?srcmenu=<%=src%>&selectedTab=2&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&eventName=<%=eventName%>&calassoc=<%=calAssoc%>" ><%=LC.L_Back%></A>

 <%

 	}

  	if (fromPage.equals("eventlibrary")) {

 %>

		<A type="submit" href="eventlibrary.jsp?srcmenu=<%=src%>&selectedTab=2&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&calassoc=<%=calAssoc%>"><%=LC.L_Back%></A>

 <%

 	}

  	//if (fromPage.equals("fetchProt")) {

  %>

		<!--<A href="fetchProt.jsp?srcmenu=<%=src%>&selectedTab=4&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&pageNo=1&displayType=<%=displayType%>&displayDur=<%=displayDur%>"  type="submit"><%=LC.L_Back%></A>-->

		<!-- <A href="fetchProt.jsp?srcmenu=<%=src%>&selectedTab=4&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&pageNo=1&displayType=<%=displayType%>&displayDur=<%=displayDur%>" >Back to Protocol Calendar : Customize Event Details</A>																				 -->

 <%

 	//}

 %>



 </td>  

</tr>

</table>



</form>



<%

	}//event

} else {  //else of if body for session



%>

  <jsp:include page="timeout.html" flush="true"/>

  <%



}



%>



  <%if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt")){}

else {

%>



</DIV>

<div class ="mainMenu" id = "emenu">

<jsp:include page="getmenu.jsp" flush="true"/>

</div>

	<% }%>

 <div class = "myHomebottomPanel">
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</body>



</html>





