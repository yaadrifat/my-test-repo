<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language = "java" import = "com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.esch.business.common.*,java.util.*"%>

<jsp:useBean id ="sessionmaint2" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="ctrldao1" scope="request" class="com.velos.esch.business.common.EventdefDao"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>

<%

   HttpSession tSession = request.getSession(true); 
   if (sessionmaint2.isValidSession(tSession))
	{
	String codeType = request.getParameter("codeType");
	String ddName = request.getParameter("ddName");
	
	String studyId = request.getParameter("studyId");
	
	String ddType = request.getParameter("ddType");
	String prop = request.getParameter("prop");

	String userIdFromSession = (String) tSession.getValue("userId");

	CodeDao cd1 = new CodeDao();
	String roleCodePk="";
	
	if (! StringUtil.isEmpty(studyId) && EJBUtil.stringToNum(studyId) > 0)
	{
		ArrayList tId = new ArrayList();
		
		TeamDao teamDao = new TeamDao();
		teamDao.getTeamRights(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(userIdFromSession));
		tId = teamDao.getTeamIds();
						
		if (tId != null && tId.size() > 0)
		{
			ArrayList arRoles = new ArrayList();
			arRoles = teamDao.getTeamRoleIds();		
			
			if (arRoles.size() >0 )	
			{
				roleCodePk = (String) arRoles.get(0);
				
				if (StringUtil.isEmpty(roleCodePk))
				{
					roleCodePk="";
				}
			}	
			else
			{
				roleCodePk ="";
			}
			
		}	
		else
		{
			roleCodePk ="";
		}
	} else
		{			
		  roleCodePk ="";
		} 
	
	cd1.getCodeValuesForStudyRole(codeType,roleCodePk);
	String ddString = cd1.toPullDown(ddName, prop);
%>
<%= ddString %>

<% } %>