<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Meeting_Topic%><%--Meeting Topic*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 
</head>

<SCRIPT>
if (document.getElementById('eSign')) { document.getElementById('eSign').disabled=false; } 
if (document.getElementById('submit_btn')) { document.getElementById('submit_btn').disabled=false; }
function  blockSubmit() {
	 setTimeout('document.getElementById(\'eSign\').disabled=true', 10);
	 setTimeout('document.getElementById(\'submit_btn\').disabled=true', 10);
}
function  validate(formobj){
	
	if (!(validate_col('e-Signature',formobj.eSign))) return false;
		
	if (document.getElementById('topicNumber')) {
		 if (!(validate_col('topicNumber',formobj.topicNumber))) return false	
    }
  	if (!(validate_col('',formobj.topic))) {
  	  	return false
  	}
	if (formobj.topic != null 
			&& formobj.topic.value.length > 4000 ) {
		var paramArray = [formobj.topic.value.length];
		alert(getLocalizedMessageString("M_TopicExcd_4kChar",paramArray));/*alert('Topic may not exceed 4000 characters. Currently:'+formobj.topic.value.length);*****/
  	  	formobj.topic.focus();
		return false;
	}
  	if (formobj.eSign.value == null) {
  	  	alert("<%=MC.M_PlsEnterEsign%>");/*alert("Please enter e-Signature.");*****/
  	  	return false
  	}
  	<%-- if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
  		return false;
   	} --%>
}
</SCRIPT>



<% String srcmenu;
srcmenu= request.getParameter("srcmenu");
%>

<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0">

<br>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="topicJB" scope="request" class="com.velos.eres.web.submission.ReviewMeetingTopicJB"/>
	
<%@ page language = "java" import = "com.velos.eres.business.common.*,com.velos.eres.business.common.TeamDao,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.DateUtil,com.velos.esch.business.common.*"%>
<DIV class="popDefault" id="div1">
<%
   HttpSession tSession = request.getSession(true);
   if (sessionmaint.isValidSession(tSession))
   {	
%>
<jsp:include page="sessionlogging.jsp" flush="true"/> 
<jsp:include page="include.jsp" flush="true"/>

<%	
    String userIdFromSession = (String) tSession.getValue("userId");
    String topicNumber = null;
    String topic = null;
    if (request.getParameter("pkTopic") != null) {
        topicJB.setId(EJBUtil.stringToNum(request.getParameter("pkTopic")));
        topicJB.loadMeetingTopic();
        topicNumber = topicJB.getTopicNumber();
        topic = StringUtil.trueValue(topicJB.getMeetingTopic()).replaceAll("&lt;br/&gt;","\n");
    }
    topicNumber = StringUtil.trueValue(topicNumber);
    topic = StringUtil.trueValue(topic);
%>

<Form name="topicForm" id="topicForm" method="post" action="updateMeetingTopic.jsp" onsubmit="if (validate(document.topicForm)== false) { setValidateFlag('false'); return false; } else { blockSubmit(); setValidateFlag('true'); return true; }">
 <P class="sectionHeadings">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Meeting_Topic%><%--Meeting Topic*****--%></P>
 <input type="hidden" name="meetingId" value="<%=StringUtil.htmlEncodeXss(request.getParameter("meetingId"))%>" />
 <input type="hidden" name="reviewBoard" value="<%=StringUtil.htmlEncodeXss(request.getParameter("reviewBoard"))%>" />
 <input type="hidden" name="pkTopic" value="<%=StringUtil.htmlEncodeXss(request.getParameter("pkTopic"))%>" />
 <table width="99%" align="center">
  <tr align="left">
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
    <td style="font-family:Verdana,Arial,Helvetica,sans-serif; color:black; font-size:8pt;">
      <%=LC.L_Topic_Number%><%--Topic Number*****--%>&nbsp;<FONT class="Mandatory">* </FONT><input name="topicNumber" id="topicNumber" value="<%=topicNumber%>">
    </td>
  </tr>
  <tr align="left">
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td style="font-family:Verdana,Arial,Helvetica,sans-serif; color:black; font-size:8pt;">
      <%=LC.L_Topic%><%--Topic*****--%>&nbsp;<FONT class="Mandatory">*</FONT>
	</td>
  </tr>
  <tr align="left">
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td>
	  <TextArea name="topic" id="topic" rows=12 cols=90 MAXLENGTH=4000 ><%=topic%></TextArea>
	</td>
  </tr>
  
 </table>
	<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="versionForm"/>
		<jsp:param name="showDiscard" value="Y"/>
	</jsp:include>
</Form>
  <%

//} //end of else body for page right



}//end of if body for session

else

{

%>
  <jsp:include page="timeout_childwindow.jsp" flush="true"/>
  <%

}

%>

<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</div>
</body>

</html>

