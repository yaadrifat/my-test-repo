<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_Insert_TitileHere %><%-- Insert title here*****--%></title>
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<body>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/> 
  <%@page language = "java" import = "com.velos.eres.service.util.*,com.velos.eres.web.specimen.*,java.util.*"%>

  <%

	String eSign = request.getParameter("eSign");
	HttpSession tSession = request.getSession(true);

	if (sessionmaint.isValidSession(tSession))

   {
     %>
     <jsp:include page="sessionlogging.jsp" flush="true"/>

     <%

   	String oldESign = (String) tSession.getValue("eSign");

	if(!oldESign.equals(eSign))
	{
%>
  	   <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
	}
	else
  	{
		String ipAdd = (String) tSession.getValue("ipAdd");
    	String usr = null;
    	int ret= -2;
	    usr = (String) tSession.getValue("userId");
		String accId = (String) tSession.getValue("accountId");
		String delinx = request.getParameter("rem");
		System.out.println(delinx);
		PreparationCartDto aPreparationCartDto = (PreparationCartDto)tSession.getAttribute("aPreparationCartDto");
		if(aPreparationCartDto !=null){
		if(!StringUtil.isEmpty(delinx)){
			 String indices[] = StringUtil.strSplit(delinx,"[INDEXSEP]");
			 int j=0;
			 for(int i=0;i<indices.length;i++){				 
				 aPreparationCartDto= ManageCart.remove(aPreparationCartDto,Integer.parseInt(indices[i])-j);
				j++;
			 }
		 }
		ret=1;		
		}
		%>
  	<br>
      <br>
      <br>
      <br>
      <br>
	  <% if (ret > 0) { %>
      <p class = "successfulmsg" align = center> <%=LC.L_ItemRem_Succ %><%-- Items removed successfully*****--%> </p>
      

	  <% } else if(ret == -2) { %>
	  <p class = "successfulmsg" align = center> <%=MC.M_ItemNt_SvdSucc %><%-- Items was not saved successfully*****--%> </p>
      <SCRIPT>
			//window.opener.location.reload();
           	//setTimeout("self.close()",1000);
      </SCRIPT>

<%
	  }
%>
<META HTTP-EQUIV="Refresh" CONTENT="1; URL=managePreparationCart.jsp">
<%	  
  }
   }	
else{
%>	
<jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
</BODY>
</HTML>