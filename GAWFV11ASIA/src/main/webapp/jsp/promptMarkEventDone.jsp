<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="include.jsp" flush="true"/>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="whichcss.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<BODY>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<%@ page language = "java" import = "com.velos.esch.service.util.EJBUtil,com.velos.eres.service.util.DateUtil,java.util.*,com.velos.eres.service.util.LC,com.velos.eres.service.util.MC,com.velos.eres.service.util.StringUtil,com.velos.esch.business.common.SchCodeDao,com.velos.esch.business.common.*"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventStatB" scope="request" class="com.velos.esch.web.eventstat.EventStatJB"/>

<%
 HttpSession tSession = request.getSession(true);
 if (sessionmaint.isValidSession(tSession))
 {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>

<form name="markdone" action="promptMarkEventDone.jsp" method="post" onSubmit="">
<%


	String mode =  request.getParameter("mode");
	String fk_sch_eventid  =  request.getParameter("fk_sch_eventid");
	String pkey = "";
	pkey =  request.getParameter("pkey");
	int ret = 0;

	if (StringUtil.isEmpty(mode))
	{
		mode = "N";
	}

	if (StringUtil.isEmpty(fk_sch_eventid))
	{
		fk_sch_eventid = "";
	}


	SchCodeDao scdaoDesc = new SchCodeDao();
	String statDesc  = "";
	int statid= 0;

	statid = scdaoDesc.getCodeId("eventstatus", "ev_done");
	statDesc = scdaoDesc.getCodeDescription(statid);

	if (mode.equals("N"))
	{
	%>

	<input type="hidden" name="mode" value="S" />
	<input type="hidden" name="fk_sch_eventid" value=<%=fk_sch_eventid%> />
	<input type="hidden" name="pkey" value=<%=pkey%> />



	<p class="defComments"><%=MC.M_CrfFrmAssocEvt_MoreResp%><%--All CRF forms associated with this event have one or more responses added. Would you like to mark this event as*****--%> '<%=statDesc%>'? </p>
		<!--KM-#3867 -->
		<TABLE cellspacing="0" cellpadding="0" >
		<tr>
		<td>
		<button type = "Submit" name="submit" ><%=LC.L_Yes%></button>
	   	</td>
	   	<td >
		<button type="button" name="return"  onClick="window.self.close();" ><%=LC.L_No%></button>
		 </td>
		</tr>
		</TABLE>

	<%
	}

	%>
		</form>
	<%

	if (mode.equals("S"))
	{

	String ipAdd = (String) tSession.getValue("ipAdd");


	String startdt= com.velos.eres.service.util.DateUtil.getCurrentDate();
	String prepflag = request.getParameter("prepflag")==null?"":request.getParameter("prepflag");
	if(prepflag.equals("Y")){
		startdt = request.getParameter("statusdate")==null?"":request.getParameter("statusdate");
	}

	Date st_date1 = DateUtil.stringToDate(startdt,"");
	java.sql.Date protEndDate = DateUtil.dateToSqlDate(st_date1);

   String usr = (String) tSession.getValue("userId");
	String eventId = request.getParameter("fk_sch_eventid");
	pkey = request.getParameter("pkey");





	int oldStatus= 0;

	SchCodeDao scdao = new SchCodeDao();
	oldStatus = scdao.getCodeId("eventstatus", "ev_notdone");

	//JM: 22JUN2010: #D-FIN23
	String reasonForCoverageChange = request.getParameter("reasonForCoverageChange");
	reasonForCoverageChange = (reasonForCoverageChange==null)?"":reasonForCoverageChange;

	EventAssocDao assocDao = new EventAssocDao();
	assocDao.getPatSchEvent_SOS_CoverType(EJBUtil.stringToNum(eventId));
	
	ArrayList tpArray = new ArrayList();
	tpArray = assocDao.getEventSOSIds();
    int eventSOSId = (tpArray == null || tpArray.size()==0)?0:EJBUtil.stringToNum(""+tpArray.get(0));
    tpArray = new ArrayList(); 
    tpArray = assocDao.getEventCoverageTypes();
	int eventCoverageType = (tpArray == null || tpArray.size()==0)?0:EJBUtil.stringToNum(""+tpArray.get(0));

  //passing the mode "N", since from here new event status will be added
	ret = eventdefB.MarkDone(EJBUtil.stringToNum(eventId),"",protEndDate,EJBUtil.stringToNum(usr),statid,oldStatus,ipAdd,"N", 
			eventSOSId, eventCoverageType, reasonForCoverageChange);



	//////////

	int len_evt_id = eventId.length();
	int len_pad = 10 - len_evt_id ;
	String len_pad_str = "";

	for (int k=0; k < len_pad; k++ ){
		len_pad_str = len_pad_str + "0";
	}


	// save new status record

	    eventStatB.setEventStatDate(startdt);

	    eventStatB.setFkEvtStat(len_pad_str + eventId);
	    eventStatB.setEvtStatus( String.valueOf(statid) );
	    eventStatB.setCreator(usr);
	    eventStatB.setIpAdd(ipAdd) ;
	    eventStatB.setEventStatDetails();

	//}KM -to fix the Bug2678

%>

<br><br><br><br>
<p class="sectionHeadings" align=center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%> </p>

<script>
	window.opener.location.reload();
	try {
		window.opener.opener.location.reload();
	} catch (e) {}
	setTimeout("self.close()",1000);


</script>


<%

}//end of if for eSign
%>

<%
} //end of if for session
else{%>
<jsp:include page="timeout.html" flush="true"/>
<%
}%>

</BODY>
</HTML>
