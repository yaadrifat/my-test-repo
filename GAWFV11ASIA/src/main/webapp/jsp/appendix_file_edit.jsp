<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>
<%
boolean isIrb = "irb_upload_tab".equals(request.getParameter("selectedTab")) ? true : false; 
String includeTabsJsp = isIrb ? ("LIND".equals(CFG.EIRB_MODE) ? "ecompNewTabs.jsp" : "irbnewtabs.jsp") : "studytabs.jsp";
if (isIrb) { 
%>
<title><%=MC.M_ResComp_NewUpldDocu%><%--Research Compliance >> New Application >> Upload Documents*****--%></title>
<% } else { %>
<title><%=MC.M_StdVer_FileDets%><%--<%=LC.Std_Study%> >> Version >> File Details--%></title>
<% } %>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">



</HEAD>



<% String src;
String from = "appendixver";
src= request.getParameter("srcmenu");
String statusPk=request.getParameter("statusPk")==null?"":request.getParameter("statusPk");
String statusDesc=request.getParameter("statusDesc")==null?"":request.getParameter("statusDesc");

%>



<jsp:include page="panel.jsp" flush="true">

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>   



<SCRIPT Language="javascript">

 function  validate(formobj){
	 
	 if (jQuery('#file').val() == ''){
		 	jQuery('#fileuploaded').val('no file');
		}

//     formobj=document.update

     if (!(validate_col('Description',formobj.desc))) return false
      if(formobj.desc.value.length>500){  
	     alert(" <%=MC.M_SrtDesc_MaxChar%>");/*alert(" 'Short Description' exceeded maximum number of characters allowed.");*****/
	     formobj.desc.focus();
	     return false;
     } 
	 
	 if (!(validate_col('Esign',formobj.eSign))) return false
	 
 	 <%-- if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
   } --%>	 

 }


</SCRIPT>

<BODY>
<jsp:useBean id ="sessionmaint" scope="request" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id ="appendixB" scope="request" class="com.velos.eres.web.appendix.AppendixJB"/>
<jsp:useBean id="studyJB" scope="session" class="com.velos.eres.web.study.StudyJB" />
<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*"%>
<%String studyIdForTabs="";
if(request.getParameter("studyId")!=null && !request.getParameter("studyId").equals("")){
	studyIdForTabs = request.getParameter("studyId");
}
String hiddenflag = request.getParameter("hiddenflag")==null?"":request.getParameter("hiddenflag"); 
String docshow = request.getParameter("docshow") == null?"":request.getParameter("docshow");%>
<%if(hiddenflag.equalsIgnoreCase("")) {%>
<DIV class="BrowserTopn" id="div1"> 
  <jsp:include page="<%=includeTabsJsp%>" flush="true">
  <jsp:param name="from" value="<%=from%>"/>  
  <jsp:param name="studyId" value="<%=studyIdForTabs %>"/>
  </jsp:include>
  </DIV>
  <% } 
if(includeTabsJsp.equalsIgnoreCase("studytabs.jsp") && !hiddenflag.equalsIgnoreCase("0")){
if("Y".equals(CFG.Workflows_Enabled)&& (studyIdForTabs!=null && !studyIdForTabs.equals("") && !studyIdForTabs.equals("0")) && !"LIND".equals(CFG.EIRB_MODE)){ %>
  <DIV class="BrowserBotN BrowserBotN_S_3 workflowDivBig" id = "div1">
  <%} else{%>
<DIV class="BrowserBotN BrowserBotN_S_3" id = "div1">
<%}}else{ %>
   <DIV class="BrowserBotN BrowserBotN_S_3" id = "div1">
  <%}

HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession))
{
	String uName = (String) tSession.getValue("userName");
	String sessStudyId="";
	if(hiddenflag.equalsIgnoreCase("")){
	 sessStudyId = (String) tSession.getValue("studyId");
	} else{
		
	 sessStudyId = (String) request.getParameter("studyId");
	}
	String studyVerId = request.getParameter("studyVerId");

	String studyNo = (String) tSession.getValue("studyNo");

	String tab = request.getParameter("selectedTab");
	
	String appndxId = request.getParameter("appndxId");
	String shortDesc="";
	
	int stId=EJBUtil.stringToNum(sessStudyId);
	
	appendixB.setId(EJBUtil.stringToNum(appndxId));
	appendixB.getAppendixDetails();
	shortDesc=appendixB.getAppendixDescription();
	
	String accId = (String) tSession.getValue("accountId"); 
	String userId = (String) tSession.getValue("userId");
	String accMaxStorage = (String) tSession.getAttribute("accMaxStorage");
	CtrlDao ctrlDao = new CtrlDao();	
	long freeSpace = ctrlDao.getFreeSpace(EJBUtil.stringToNum(accId));

	com.aithent.file.uploadDownload.Configuration.readSettings("eres");
	com.aithent.file.uploadDownload.Configuration.readUploadDownloadParam(com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD + "fileUploadDownload.xml", "study");
	String upld=com.aithent.file.uploadDownload.Configuration.UPLOADSERVLET;
	
%>

<Form name=update action=<%=upld%> id="appFileEditId"  onsubmit="if (validate(document.update)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); document.update.submit(); setTimeout('document.upload.eSign.disabled=true', 10); document.getElementById('submit_btn').disabled=true;}"  METHOD="POST" ENCTYPE="multipart/form-data">
 	<input type="hidden" name="studyVerId" value=<%=studyVerId%>>
	<input type=hidden name=accId value=<%=accId%>>
  	<input type=hidden name=userId value=<%=userId%>>
  	<input type=hidden name=srcmenu value=<%=src%>>
	<input type=hidden name=hiddenflag value=<%=hiddenflag%>>
	<input type=hidden name=docshow value=<%=docshow%>>
	<input type="hidden" name="studyVerId" value=<%=studyVerId%>>
    <table width="100%" >
      <tr> 
        <td colspan="2" align="center"> 
          <P class = "defComments"> <b><%=LC.L_Edit_Dets%><%--Edit details*****--%> </b></P>
        </td>
      </tr>
    <tr><td>&nbsp;</td></tr>
    <tr>    
    	<td width="20%" align="right"><%=LC.L_File%><%--File*****--%></td>
        <td width="65%"> 
          <input type=file name=file id=file size=40 >
        </td>
      </tr>  
      <tr> 
        <td width="20%" align="right"> <%=LC.L_Short_Desc%><%--Short Description*****--%> <FONT class="Mandatory" >* </FONT> 
        </td>
        <td width="65%"> 
          <TextArea type=text name=desc row=3 cols=50 value="<%=shortDesc%>"><%=shortDesc%></TextArea>
        </td>
      </tr>
      <tr> 
        <td width="35%"> </td>
        <td width="65%"> 
          <P class="defComments"> <%=MC.M_ShortDescFile_500CharMax%><%--Give a short description of your file (500 char 
            max.)*****--%> </P>
        </td>
      </tr>
    </table>
    <table width="100%" >
      
      <tr> 
        <td  colspan=2> <%=MC.M_DoYouWant_InfoToPublic%><%--Do you want Information in this section to be available 
          to the public?*****--%> </td>
      </tr>
      <tr> 
        <td colspan=2> 
		<%
			if(appendixB.getAppendixPubFlag().equals("Y")){
		%>
          <input type="Radio" name="pubflag" value=Y checked>
          <%=LC.L_Yes%><%--Yes*****--%> 
          <input type="Radio" name="pubflag" value=N >
          <%=LC.L_No%><%--No*****--%> 
		<%
			} else {
		%>
		  <input type="Radio" name="pubflag" value=Y >
          <%=LC.L_Yes%><%--Yes*****--%> 
          <input type="Radio" name="pubflag" value=N checked>
          <%=LC.L_No%><%--No*****--%> 
		<%
			} 
		%>
		  
		  &nbsp; <A href="pubvsnonpub.htm" target="Information" onClick="openwin()"><%=MC.M_PublicVsNotPublic_Info%><%--What 
          is Public vs Not Public Information?*****--%></A> </td>
      </tr>
    </table>

	 <BR>
	 <input type="hidden" name="type" value='file'>
	 <input type="hidden" name="study" value=<%=stId%>>
     <input type="hidden" name="studyId" value=<%=stId%>>
     <input type="hidden" name="studyVer" value=<%=studyVerId%>>	
     <input type="hidden" name="appndxId" value=<%=appndxId%>>	
     <input type="hidden" name="fileuploaded" id="fileuploaded" value="">
     <input type=hidden name=db value='eres'>
	 <input type=hidden name=module value='study'>		  
	 <input type=hidden name=tableName value='ER_STUDYAPNDX'>
	 <input type=hidden name=columnName value='STUDYAPNDX_FILEOBJ'>
	 <input type=hidden name=pkValue value='0'>
	 <input type=hidden name=pkColumnName value='PK_STUDYAPNDX'>	
	 <input type=hidden name=maxFileSize value=<%=freeSpace%>>
	 <input type=hidden name=accMaxStorage value=<%=accMaxStorage%>> 
	 <input type=hidden name=nextPage value='../../velos/jsp/appendixbrowser.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&studyVerId=<%=studyVerId%>&studyId=<%=studyIdForTabs%>&statusPk=<%=statusPk %>&statusDesc=<%=statusDesc %>'>
	 <input type=hidden name=successPage value='../../velos/jsp/studyVersuccessPage.jsp?'>

	<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="appFileEditId"/>
			<jsp:param name="showDiscard" value="N"/>
	</jsp:include>


  </form>
	
    <BR>
    <table width="100%" cellspacing="0" cellpadding="0">
	  <tr>
      <td align=right> 
 <!--       <input type="Submit" name="submit" value="Submit"> -->
<!-- <input type="image" src="../images/jpg/Submit.gif" onClick = "return validate()" align="absmiddle">	-->
       </td>
      </tr>
    </table>
  </form>
  <%

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<DIV class="mainMenu" id = "emenu"> 
  <jsp:include page="getmenu.jsp" flush="true"/>
</DIV>
</body>

</html>