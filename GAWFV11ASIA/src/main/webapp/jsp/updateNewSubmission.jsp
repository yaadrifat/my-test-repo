<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML> 

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:include page="include.jsp" flush="true"/>	
<jsp:useBean id="submB" scope="request" class="com.velos.eres.web.submission.SubmissionJB"/>
<jsp:useBean id="submBoardB" scope="request" class="com.velos.eres.web.submission.SubmissionBoardJB"/>
<jsp:useBean id="submStatusB" scope="request" class="com.velos.eres.web.submission.SubmissionStatusJB"/>
<jsp:useBean id="statusB" scope="request" class="com.velos.eres.web.statusHistory.StatusHistoryJB"/>

	<%@ page language="java" import="com.velos.eres.business.study.*,com.velos.eres.business.common.*,java.util.*,java.text.*,com.velos.eres.service.util.*,com.velos.eres.web.account.AccountJB,com.velos.esch.business.common.*,com.velos.eres.compliance.web.*,org.json.*,org.apache.commons.lang.StringEscapeUtils"%>
	<%@ page import="com.velos.eres.web.study.StudyJB,com.velos.eres.web.studyId.StudyIdJB" %>
	<%@ page import="com.velos.eres.widget.business.common.UIFlxPageDao,com.velos.eres.widget.business.common.ResubmDraftDao,com.velos.eres.widget.service.util.FlxPageArchive" %>
	<%@page import="com.velos.eres.web.intercept.InterceptorJB"%>
	<%@page import="com.velos.eres.web.studyVer.StudyVerJB,com.velos.eres.business.common.StudyVerDao" %>
	<%@page import="com.velos.eres.web.statusHistory.StatusHistoryJB" %>
	<%@page import="com.velos.eres.web.studyStatus.StudyStatusJB" %>
	<%@page import="com.velos.eres.compliance.web.ComplianceJB" %>

	<%!
	private static final String DEFAULT_DUMMY_CLASS = "[Config_Submission_Interceptor_Class]";
	private static final String VERSION_STATUS = "versionStatus";
	private static final String LETTER_F = "F";
	private static final String ER_STUDYVER_TABLE = "er_studyver";
	%>

	<%
  	HttpSession tSession = request.getSession(true);
  	String accId = (String) tSession.getValue("accountId");
  	int iaccId = EJBUtil.stringToNum(accId);
  	String studyId = (String) tSession.getValue("studyId");
  	String grpId = (String) tSession.getValue("defUserGroup");
  	String ipAdd = (String) tSession.getValue("ipAdd");
  	String usr = (String) tSession.getValue("userId");
  	int iusr = EJBUtil.stringToNum(usr);
        CodeDao codedao = new CodeDao();
       EIRBDao eirbDao = new EIRBDao();
        int codeId1=codedao.getCodeId("irbstatus","Ethics");
	int codeId=codedao.getCodeId("irbstatus","Governance");
	int irbcodeId=codedao.getCodeId("irbstatus","All");
	String statusCodeId=codeId+"";
	String statusCodeIdforEth=codeId1+"";
	String irbCodeId=irbcodeId+"";
	String boardFlag="";
	String formforEthics=LC.L_Form_For_Ethics;
	String formforGovernance=LC.L_Form_For_Governance;
	String resultMsg = null;
    int fk_filledform=0;
    int fk_formlib=0;
    int fk_formlibver=0;
    StudyJB studyJB = new StudyJB();
	String userChecked="";
	int newSubmBoardId =0;
	userChecked = eirbDao.getUserChecked(EJBUtil.stringToNum(studyId));
  	int newSubmStatusId = 0;
  	String fullVersion="";
	 Date dt = new java.util.Date();
  String stDate = DateUtil.dateToString(dt);
  Calendar cal = Calendar.getInstance(TimeZone.getDefault());
  String DATE_FORMAT = "hh:mm:ss a";
  java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(DATE_FORMAT);       
  String time = sdf.format(cal.getTime());
  String strDate = stDate +" "+ time;
  	String fieldDetails = StringEscapeUtils.unescapeHtml(StringUtil.htmlEncodeXss(request.getParameter("fieldDetails")));

	String attachFieldDetails = StringEscapeUtils.unescapeHtml(StringUtil.htmlEncodeXss(request.getParameter("attachFieldDetails")));
	if(attachFieldDetails == null) {
	 attachFieldDetails="[]";
	}

	String comments = StringEscapeUtils.unescapeHtml(StringUtil.htmlEncodeXss(request.getParameter("comments")));

	String src = StringUtil.htmlEncodeXss(request.getParameter("srcmenu"));
	String selectedTab = StringUtil.htmlEncodeXss(request.getParameter("selectedTab"));
	String eSign = request.getParameter("eSign");
	if (sessionmaint.isValidSession(tSession)) {
	%>
	<jsp:include page="sessionlogging.jsp" flush="true"/>
	<%
	    String oldESign = (String) tSession.getValue("eSign");
		if (!oldESign.equals(eSign)) {
	%>
	 		<jsp:include page="incorrectesign.jsp" flush="true"/>
	<%
	} else {
	    UIFlxPageDao uiFlxPageDao = new UIFlxPageDao();
	    int version = uiFlxPageDao.getHighestFlexPageVersion("er_study", Integer.valueOf(studyId));

	    HashMap<String, Object> paramMap = new HashMap<String, Object>();
	    int ret = 0;
	    if ("LIND".equals(CFG.EIRB_MODE)){
	    if (version > 0) {
			//Save re-submission draft first
		    paramMap.put("request", request);
		    ResubmDraftDao resubmDraftDao = new ResubmDraftDao();
		    ret = resubmDraftDao.saveResubmissionDraft(paramMap);
	    } else { ret = 1; }
	    }
	    if( ret < 0 ) {
%>
  <br><br><br><br>
  <p class = "successfulmsg" align = center><%= MC.M_Err_SavingData/*"There was an error while saving data"*****/%></p>
  <META HTTP-EQUIV=Refresh CONTENT="3; URL=studyCheckNSubmit?mode=M&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>">
</BODY>
</HTML>
<%          
            return; // An error occurred; let's get out of here
        }

		// Create a new submission status bean for submission
		paramMap.put("userId", (usr.equals(null) ? null : Integer.valueOf(usr)));
		paramMap.put("accountId", (accId.equals(null) ? null : Integer.valueOf(accId)));
		paramMap.put("studyId", (studyId.equals(null) ? null : Integer.valueOf(studyId)));

		// Implement Flex Study part here
		if ("LIND".equals(CFG.EIRB_MODE)){
	
		studyJB.setId(StringUtil.stringToNum(studyId));
		studyJB.getStudyDetails();
		String defUserGroup = (String) tSession.getAttribute("defUserGroup");
		StudyIdJB studyIdJB = new StudyIdJB();
		StudyIdDao sidDao = new StudyIdDao();
		sidDao = studyIdJB.getStudyIds(
				StringUtil.stringToNum(studyId), defUserGroup);

		FlxPageArchive archive = new FlxPageArchive();
		 fullVersion = archive.createArchive(studyJB, sidDao, paramMap);
	    double versionNum = Double.valueOf(fullVersion).doubleValue();
	    // Freeze all categories of this version of this study
	    StudyVerJB studyVerJB = new StudyVerJB();
	    StudyVerDao studyVerDao = studyVerJB.getAllVers(StringUtil.stringToNum(studyId));
	    ArrayList studyVerIds = studyVerDao.getStudyVerIds();
	    ArrayList studyVerNumbers = studyVerDao.getStudyVerNumbers();
	    // Get a codelst item for Freeze status
	    CodeDao statDao = new CodeDao();
	    int freezeStat = statDao.getCodeId(VERSION_STATUS, LETTER_F);
		//Calendar cal = Calendar.getInstance();
		//SimpleDateFormat sdf = new SimpleDateFormat(Configuration.getAppDateFormat());
	    for (int iX = 0; iX < studyVerNumbers.size(); iX++) {
	    	String svNumStr = (String)studyVerNumbers.get(iX);
	    	double svNum = Double.valueOf(svNumStr).doubleValue();
	    	double vNum = Double.valueOf(versionNum).doubleValue();
	    	if( versionNum > 0 ) {
    			if( svNum == vNum) {
		    		// add a Freeze row to ER_STATUS_HISTORY
		    		StatusHistoryJB statusHistoryJB = new StatusHistoryJB();
		    		statusHistoryJB.setStatusModuleTable(ER_STUDYVER_TABLE);
		    		int modulePk = (Integer)studyVerIds.get(iX);
		    		statusHistoryJB.setStatusModuleId(String.valueOf(modulePk));
		    		statusHistoryJB.setStatusCodelstId(String.valueOf(freezeStat));
		    		statusHistoryJB.setStatusStartDate(sdf.format(cal.getTime()));
			    	statusHistoryJB.setStatusEnteredBy(usr);
			    	statusHistoryJB.setCreator(usr);
			   		statusHistoryJB.setStatusHistoryDetails();
			   		// add version status "F" in er_studyver table
			   		studyVerDao.updateVersionStatus(svNumStr, Integer.parseInt(studyId), modulePk, "F",Integer.parseInt(usr));			   		
	    		}
		   	}
		}

    	/* Add form locking code here. */
    	
    	
	    //Locking the Study as Version is frozen
	    StudyDao studyDao = new StudyDao();
	    studyDao.lockUnlockStudy(EJBUtil.stringToNum(studyId), 1);
		
		if (!"0".equals(fullVersion)) {
			resultMsg = MC.M_Data_SvdSucc/*"Data was saved successfully"*****/;
		} else {
			resultMsg = MC.M_Err_SavingData/*"There was an error while saving data"*****/;
		}}
	    // Create a new submission bean
        submB.setStudy(studyId);
	    submB.setIpAdd(ipAdd);
	    submB.setCreator(usr);
	    submB.setLastModifiedBy(usr);
	    int newSubmId = submB.createSubmission();

        if( newSubmId > 0 ) {
            resultMsg = MC.M_Data_SvdSucc/*"Data was saved successfully"*****/;
        } else {
            resultMsg = MC.M_Err_SavingData/*"There was an error while saving data"*****/;
%>
  <br><br><br><br>
  <p class = "successfulmsg" align = center><%=resultMsg%></p>
  <META HTTP-EQUIV=Refresh CONTENT="3; URL=studyCheckNSubmit?mode=M&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>">
</BODY>
</HTML>
<%          
            return; // An error occurred; let's get out of here
        }

        // Create a new submission status bean for submission
        EIRBDao eIrbDao = new EIRBDao();
        eIrbDao.getCurrentOverallStatus(EJBUtil.stringToNum(studyId), newSubmId);
        String currentOverallStatus = eIrbDao.getCurrentOverallStatus();
        // If the overall status does not already exist for this study, create a new one
        if (currentOverallStatus == null || currentOverallStatus.length() == 0) {
            submStatusB.setFkSubmission(String.valueOf(newSubmId));
            submStatusB.setFkSubmissionBoard(null); // For overall status, this is null
            //submStatusB.setSubmissionEnteredBy(usr);
            submStatusB.setCreator(usr);
            submStatusB.setLastModifiedBy(usr);
            submStatusB.setIpAdd(ipAdd);
            submStatusB.setSubmissionStatusDate(null);
            submStatusB.setSubmissionAssignedTo(null);
            submStatusB.setSubmissionCompletedBy(null);
            submStatusB.setSubmissionNotes(null);
         //   submStatusB.setSubmissionStatusBySubtype("submitted");

            newSubmStatusId = submStatusB.createSubmissionStatus();
            if( newSubmStatusId < 1 ) {
                resultMsg = MC.M_Err_SavingData/*"There was an error while saving data"*****/;
%>
  <br><br><br><br>
  <p class = "successfulmsg" align = center><%=resultMsg%></p>
  <META HTTP-EQUIV=Refresh CONTENT="3; URL=studyCheckNSubmit?mode=M&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>">
</BODY>
</HTML>
<%          
                return; // An error occurred; let's get out of here
            }
        }
        
	    // Figure out which board was selected
        eIrbDao.getReviewBoards(EJBUtil.stringToNum(accId),EJBUtil.stringToNum(grpId));
        ArrayList boardNameList = eIrbDao.getBoardNameList();
        ArrayList boardIdList = eIrbDao.getBoardIdList();
        boolean noBoardIsSelected = true;
	String currboard="";
        String boardselected="";
        int counter=0;
        //EIRBDao codelstsubm_stat = new EIRBDao();
    	String Updateuser = eirbDao.getUpdateUser(EJBUtil.stringToNum(studyId));
    	eirbDao.updateIscurrentForEcompStatus(EJBUtil.stringToNum(studyId));
	    for (int iBoard=0; iBoard<boardIdList.size(); iBoard++) {
	    	if (!"on".equals(request.getParameter("submitTo"+iBoard))) { continue; }
	    	noBoardIsSelected = false;
	    }
	    for (int iBoard=0; iBoard<boardIdList.size(); iBoard++) {
	    	if ("LIND".equals(CFG.EIRB_MODE) && noBoardIsSelected) {
	    		if (iBoard > 0) {
	    			break;
	    		}
	    	} else if (!"on".equals(request.getParameter("submitTo"+iBoard))) {
	        	continue;
	        }
		if("on".equals(request.getParameter("submitTo"+iBoard))){
	        	currboard+=boardNameList.get(iBoard)+",";
	        	boardselected=boardNameList.get(iBoard)+"";
	        	counter++;
	        }

	        // Create a new submission_board bean for each board selected
		    submBoardB.setFkSubmission(String.valueOf(newSubmId));
		    submBoardB.setFkReviewBoard((String) boardIdList.get(iBoard));
		    submBoardB.setCreator(usr);
		    submBoardB.setLastModifiedBy(usr);
		    submBoardB.setIpAdd(ipAdd);
		    submBoardB.setFkReviewMeeting(null);
		    submBoardB.setSubmissionReviewer(null);
		    submBoardB.setSubmissionReviewType(null);
		    
		    Hashtable ht = submBoardB.createSubmissionBoard();
		    
		     newSubmBoardId = ((Integer)ht.get("id")).intValue(); 
		    if (newSubmBoardId < 1) {
		        resultMsg = MC.M_Err_SavingData/*"There was an error while saving data"*****/;
		        break;
		    }
		    if(currboard.startsWith("Ethics")){
		    	boardFlag="Ethics";
		    }else{
		    	boardFlag="Governance";
		    }
		    
		    // Create a new submission status bean for this submission_board
            submStatusB.setFkSubmission(String.valueOf(newSubmId));
            submStatusB.setFkSubmissionBoard(String.valueOf(newSubmBoardId));
            submStatusB.setSubmissionEnteredBy(usr);
            submStatusB.setCreator(usr);
            submStatusB.setLastModifiedBy(usr);
            submStatusB.setIpAdd(ipAdd);
            submStatusB.setSubmissionStatusDate(null);
            submStatusB.setSubmissionAssignedTo(null);
            submStatusB.setSubmissionCompletedBy(null);
            if(boardselected.startsWith("Ethics")){
            	eirbDao.getIRBFormName(EJBUtil.stringToNum(studyId),formforEthics);
            	fk_filledform = eirbDao.getFkfilledformlibsumstat();
            	fk_formlib = eirbDao.getFkformlibsumstat();
            	fk_formlibver=eirbDao.getFormLibVer();
            	submStatusB.setFkfilledformlibsumstat(fk_filledform);
            	submStatusB.setFkformlibsumstat(fk_formlib);
            	submStatusB.setIrbFormName(formforEthics);
            	submStatusB.setFormLibVer(fk_formlibver);
            }
            else if(boardselected.startsWith("Governance")){
            	eirbDao.getIRBFormName(EJBUtil.stringToNum(studyId),formforGovernance);
            	fk_filledform = eirbDao.getFkfilledformlibsumstat();
            	fk_formlib = eirbDao.getFkformlibsumstat();
            	fk_formlibver=eirbDao.getFormLibVer();
            	submStatusB.setFkfilledformlibsumstat(fk_filledform);
            	submStatusB.setFkformlibsumstat(fk_formlib);
            	submStatusB.setIrbFormName(formforGovernance);
            	submStatusB.setFormLibVer(fk_formlibver);
            }
            submStatusB.setUserChecked(userChecked);
            submStatusB.setIrbFormSeq("1");
            submStatusB.setSubmissionNotes(null);
		    if (ht.containsKey("previouslySubmitted")) {
		        submStatusB.setSubmissionStatusBySubtype("resubmitted");
		    } else {
		        submStatusB.setSubmissionStatusBySubtype("submitted");
		    }
            
            int newSubmStatusIdForBoard = submStatusB.createSubmissionStatus();
            
            if(newSubmStatusIdForBoard>1){
            	eirbDao.updateIscurrentForFinalOutcomeTab(newSubmId,newSubmBoardId);
                }
            
            if( newSubmStatusIdForBoard < 1 ) {
                resultMsg = MC.M_Err_SavingData/*"There was an error while saving data"*****/;
                break;
            }
	    } // End of board loop
	     String updateuserchecked=eirbDao.getUserCheckedLatestValue(EJBUtil.stringToNum(studyId),usr,newSubmId,newSubmBoardId);
	    /*eirbDao.getEnteredFormsLatestValue(EJBUtil.stringToNum(studyId));*/
	    
	    statusB.setStatusModuleId(studyId);
		statusB.setStatusModuleTable("er_study");
		 if(counter==boardIdList.size()){
			 statusB.setStatusCodelstId(irbCodeId);
		    }
		 else{
			 if(currboard.startsWith("Ethics")){
			 statusB.setStatusCodelstId(statusCodeIdforEth);
		    }
			 else{
				 statusB.setStatusCodelstId(statusCodeId); 
			 } 
			 }
		statusB.setStatusStartDate(stDate);
		statusB.setCreator(usr);
		statusB.setIpAdd(ipAdd);
		statusB.setModifiedBy(usr);
		statusB.setRecordType("N");
		statusB.setIsCurrentStat("1");
		statusB.setIsUserChecked(Updateuser);
		statusB.setTimeStamp(strDate);
		statusB.setStatusHistoryDetailsWithEndDate();

		paramMap.put("attachFieldDetails", attachFieldDetails);
		
		
	    // Implement Flex Study part here

	    CodeDao studyStatCodeDao = new CodeDao();
	    int submittingCodeId = studyStatCodeDao.getCodeId("studystat", "submitting");
	    StudyStatusJB studyStatusJB = new StudyStatusJB();
	    if (submittingCodeId > 0) {
	        studyStatusJB.addStudyStatus(studyId, usr, submittingCodeId);
	    }
	    
	    String fieldDetailsString = "[]"; 
        if (fieldDetails != null) {
                JSONArray fieldDetailsArray = new JSONArray(
                                fieldDetails);
                int noOfFields = fieldDetailsArray.length();
                for (int i = 0; i < noOfFields; i++) {
                        fieldDetailsArray.getJSONObject(i).remove(
                                        "fieldLabel");
                        fieldDetailsArray.getJSONObject(i).remove(
                                        "fieldRow");
                }
                fieldDetailsString = fieldDetailsArray.toString();
        }

        uiFlxPageDao.setComments(comments);
        uiFlxPageDao.updateCurrentPageVersionChanges(
                                "er_study", studyJB.getId(),
                                fieldDetailsString, newSubmId);
        
        uiFlxPageDao.updateAttachPageDiffFlexPageVersion(studyJB.getId(),attachFieldDetails);
        
        uiFlxPageDao.updateStatOfLatestVersion("er_study", studyJB.getId(), "submitting"); // marker to show this version was submitted
	    
		// -- Add a hook for customization for after a submission update succeeds
		try {
			if (!StringUtil.isEmpty(CFG.Config_Submission_Interceptor_Class) &&
					!DEFAULT_DUMMY_CLASS.equals(CFG.Config_Submission_Interceptor_Class)) {
				Class clazz = Class.forName(CFG.Config_Submission_Interceptor_Class);
				Object obj = clazz.newInstance();
				Object[] args = new Object[3];
				args[0] = (Object)request;
				args[1] = studyJB;
				args[2] = fullVersion;
				((InterceptorJB) obj).handle(args);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		// -- End of hook
        String nextScreen = "LIND".equals(CFG.EIRB_MODE) ? "flexStudyScreen" : "irbnewcheck.jsp";
%>
  <br><br><br><br>
  <p class = "successfulmsg" align = center><%=resultMsg%></p>
  <META HTTP-EQUIV=Refresh CONTENT="1; URL=<%=nextScreen%>?mode=M&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>&autoPopulate=Y">
<%        
    }// end of if body for e-sign
  } //end of if body for valid session
  else
  {
  %>
  <jsp:include page="timeout.html" flush="true"/>
  <%
  }
%>

</BODY>
</HTML>