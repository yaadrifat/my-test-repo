<!--All the values from messageBundle.properties are included here-->
<%@ page import="com.velos.eres.service.util.MC"%>
<%@ page import="com.velos.eres.service.util.Configuration"%>

<script type="text/javascript" >

<%-- Testing unused Keys 
var Pat_RestrictFutureDate ="<%=MC.Pat_RestrictFutureDate %>";
var Usr_SessTimeout ="<%=MC.Usr_SessTimeout %>";
var Usr_SessTimeoutShort ="<%=MC.Usr_SessTimeoutShort %>";
var Svr_CommunicationError ="<%=MC.Svr_CommunicationError %>"; --%>

/* New Message Bundle Testing (External JS) */

var M_RestrictFutureDate ="<%=MC.M_RestrictFutureDate %>";
var M_Usr_SessTimeout ="<%=MC.M_Usr_SessTimeout %>";
var M_Usr_SessTimeoutShort ="<%=MC.M_Usr_SessTimeoutShort %>";
var M_BgtSavingProg_PlsWait ="<%=MC.M_BgtSavingProg_PlsWait %>"
var M_SaveBgtFst_BefCalAttch ="<%=MC.M_SaveBgtFst_BefCalAttch %>";
var M_Selc_IndvCal ="<%=MC.M_Selc_IndvCal %>";
var M_Specify_AfterDecimal ="<%=MC.M_Specify_AfterDecimal %>";
var M_FormatWith_Decimal ="<%=MC.M_FormatWith_Decimal %>";
var M_Sbmt_CurrFrmFirst ="<%=MC.M_Sbmt_CurrFrmFirst %>";
var M_Status_CntModified ="<%=MC.M_Status_CntModified %>";
var M_TemplStat_CntModify ="<%=MC.M_TemplStat_CntModify %>";
var M_FldDisab_CntSelDt ="<%=MC.M_FldDisab_CntSelDt %>";
var M_FrmStatNotAllow_WantProc ="<%=MC.M_FrmStatNotAllow_WantProc %>";
var M_Func_NotAvalForSelc ="<%=MC.M_Func_NotAvalForSelc %>";
var M_View_PermissionDenied ="<%=MC.M_View_PermissionDenied %>";
var M_Width_OfTheCol ="<%=MC.M_Width_OfTheCol %>";
var M_CntMakeChg_ToBgt ="<%=MC.M_CntMakeChg_ToBgt %>";
var M_Max_SizeAllowed ="<%=MC.M_Max_SizeAllowed%>";
var M_EtrVld_FringeBenftVal ="<%=MC.M_EtrVld_FringeBenftVal%>";
var M_Vldt_CostDiscount ="<%=MC.M_Vldt_CostDiscount%>";
var M_Etr_ValidCost ="<%=MC.M_Etr_ValidCost%>";
var M_EtrValid_IndirectsValue ="<%=MC.M_EtrValid_IndirectsValue%>";
var M_Etr_ValidNum ="<%=MC.M_Etr_ValidNum%>";
var M_Vldt_NumOfPatValue ="<%=MC.M_Vldt_NumOfPatValue%>";
var M_EtrValid_UnitCost ="<%=MC.M_EtrValid_UnitCost%>";
var M_Vldt_SponsorAmt ="<%=MC.M_Vldt_SponsorAmt%>";
var M_Etr_DataIn ="<%=MC.M_Etr_DataIn%>";
var M_Etr_MandantoryFlds ="<%=MC.M_Etr_MandantoryFlds%>";
var M_Etr_OnlyTheseValues ="<%=MC.M_Etr_OnlyTheseValues%>";
var M_EtrMthIn_CorrectCases ="<%=MC.M_EtrMthIn_CorrectCases%>";
var M_Etr_ValidData ="<%=MC.M_Etr_ValidData%>";
var M_Etr_ValidDate ="<%=MC.M_Etr_ValidDate%>";
var M_Save_BgtFirst ="<%=MC.M_Save_BgtFirst%>";
var M_PlsEtrCorrect_MthCase ="<%=MC.M_PlsEtrCorrect_MthCase%>";
var M_RowsNum_OnPage ="<%=MC.M_RowsNum_OnPage%>";
var M_OverlibFor_HideFormPlugin ="<%=MC.M_OverlibFor_HideFormPlugin%>";
var M_OverlibFor_ShadowPlugin ="<%=MC.M_OverlibFor_ShadowPlugin%>";
var M_AttachStd_BeforeSelPcol ="<%=MC.M_AttachStd_BeforeSelPcol%>";

/* New Message Bundle Testing (External JS) 19 july 2011 */

var M_BgtSvgProg_PlsWait ="<%=MC.M_BgtSvgProg_PlsWait %>";
var M_FuncNtAval_PlsSelIndCal ="<%=MC.M_FuncNtAval_PlsSelIndCal %>";
var M_InvDt_EtrValidDt ="<%=MC.M_InvDt_EtrValidDt %>";
var M_Lib410_ReqHideForm ="<%=MC.M_Lib410_ReqHideForm %>";
var M_Lib410_ReqShadow ="<%=MC.M_Lib410_ReqShadow %>";
var M_PlsEtrCost_PatVal ="<%=MC.M_PlsEtrCost_PatVal %>";
var M_PlsSpfy_Format ="<%=MC.M_PlsSpfy_Format %>";
var M_SplCharNotAllowed ="<%=MC.M_SplCharNotAllowed%>";

/* New Message Bundle Testing (External JS) 20 july 2011 */

var M_PlsEtr_MonthCase_Jan ="<%=MC.M_PlsEtr_MonthCase_Jan %>";
var M_PlsEtr_ValidData ="<%=MC.M_PlsEtr_ValidData %>";
var M_PlsEtrVal_GtrThan ="<%=MC.M_PlsEtrVal_GtrThan %>";
var M_PlsSpfyHs_DeciPt ="<%=MC.M_PlsSpfyHs_DeciPt %>";
var M_NoSplCharAllowed ="<%=MC.M_NoSplCharAllowed%>";

/* New Message Bundle Testing (External JS) 20 july 2011 */

var M_Data_SizeExc ="<%=MC.M_Data_SizeExc %>";
var M_At_AGlance ="<%=MC.M_At_AGlance %>";
var M_LessThan_Or ="<%=MC.M_LessThan_Or %>";
var M_Please_Select ="<%=MC.M_Please_Select %>";

/* New Message Bundle Testing (External JS) 20 july 2011 */

var M_IntegerPartCnt_Exceed ="<%=MC.M_IntegerPartCnt_Exceed%>";
var M_EtrValidUnits ="<%=MC.M_EtrValidUnits%>";
var M_Freeze_CntModified ="<%=MC.M_Freeze_CntModified%>";
var M_Disab_InPrvMode="<%=MC.M_Disab_InPrvMode%>";
var M_NoRgtTo_SetStatus="<%=MC.M_NoRgtTo_SetStatus%>";
var M_IncorrEsign_EtrAgain="<%=MC.M_IncorrEsign_EtrAgain%>";
var M_IncorrUsrName_EtrAgain="<%=MC.M_IncorrUsrName_EtrAgain%>";

/* PCAL-20461 New Message Bundle 03-Oct-2011 */
var M_DontEditRgt_AccsMod="<%=MC.M_DontEditRgt_AccsMod%>";
var M_PlsPrewSave_MoveFwd="<%=MC.M_PlsPrewSave_MoveFwd%>";
var M_SrySeqDisp_ModActDact="<%=MC.M_SrySeqDisp_ModActDact%>";
var M_PlsEnterEsign="<%=MC.M_PlsEnterEsign%>";
var M_ThereNo_ChgToSave="<%=MC.M_ThereNo_ChgToSave%>";
var M_1000OrLessChanges="<%=MC.M_1000OrLessChanges%>";
var M_NoEvt_InThisVisit="<%=MC.M_NoEvt_InThisVisit%>";

/* New Message Bundle Testing (External Velos Dir JS) 15 October 2011 */

var M_404Nt_Fnd="<%=MC.M_404Nt_Fnd%>";
var M_AddPrep_Cart="<%=MC.M_AddPrep_Cart%>";
var M_BrwsNt_SuppAjax="<%=MC.M_BrwsNt_SuppAjax%>";
var M_CldNtCont_InvldIndx="<%=MC.M_CldNtCont_InvldIndx%>";
var M_CnfrmMstone_Change="<%=MC.M_CnfrmMstone_Change%>";
var M_CnfrmStdLvl_Change="<%=MC.M_CnfrmStdLvl_Change%>";
var M_CnfrmVst_EvtChg="<%=MC.M_CnfrmVst_EvtChg%>";
var M_CnfrmVst_ItemChg="<%=MC.M_CnfrmVst_ItemChg%>";
var M_CntDel_FormOffEdt="<%=MC.M_CntDel_FormOffEdt%>";
var M_CntMore_WhenMonth="<%=MC.M_CntMore_WhenMonth%>";
var M_CntMore_WhenWeek="<%=MC.M_CntMore_WhenWeek%>";
var M_CommErr_CnctVelos="<%=MC.M_CommErr_CnctVelos%>";
var M_CreateVstsOrEvts_Mstone="<%=MC.M_CreateVstsOrEvts_Mstone%>";
var M_DayEtr_Row="<%=MC.M_DayEtr_Row%>";
var M_DaysEtr_Row="<%=MC.M_DaysEtr_Row%>";
var M_DelFrm_Form="<%=MC.M_DelFrm_Form%>";
var M_EdtCvg_AnlysEvt="<%=MC.M_EdtCvg_AnlysEvt%>";
var M_EntrEvtFmt_NoOptRow="<%=MC.M_EntrEvtFmt_NoOptRow%>";
var M_EntrMthDays_NoFmtRow="<%=MC.M_EntrMthDays_NoFmtRow%>";
var M_EntrMthDays_NoOptRow="<%=MC.M_EntrMthDays_NoOptRow%>";
var M_ErrGetResp_CnctVelos="<%=MC.M_ErrGetResp_CnctVelos%>";
var M_EtrCostName_Row="<%=MC.M_EtrCostName_Row%>";
var M_EtrIntWeek_AftNoOpt="<%=MC.M_EtrIntWeek_AftNoOpt%>";
var M_ItrvlEtrAft_ValidRow="<%=MC.M_ItrvlEtrAft_ValidRow%>";
var M_MnthDayWeekEtr_Row="<%=MC.M_MnthDayWeekEtr_Row%>";
var M_MnthEtr_Row="<%=MC.M_MnthEtr_Row%>";
var M_NoRec_ChldGrid="<%=MC.M_NoRec_ChldGrid%>";
var M_PlsSel_OneItem="<%=MC.M_PlsSel_OneItem%>";
var M_PlsSv_VstDepend="<%=MC.M_PlsSv_VstDepend%>";
var M_ProcYour_Req="<%=MC.M_ProcYour_Req%>";
var M_SelDaysMths_AftrRow="<%=MC.M_SelDaysMths_AftrRow%>";
var M_SelItems_AddSucc="<%=MC.M_SelItems_AddSucc%>";
var M_SelVstName_AftrRow="<%=MC.M_SelVstName_AftrRow%>";
var M_ShdBe11Dgt_PlsEtrVal="<%=MC.M_ShdBe11Dgt_PlsEtrVal%>";
var M_ThereNoChg_Save="<%=MC.M_ThereNoChg_Save%>";
var M_ThereNoChg_Validate="<%=MC.M_ThereNoChg_Validate%>";
var M_ThereNoEvt_SelMstone="<%=MC.M_ThereNoEvt_SelMstone%>";
var M_ToDelPerm_PlsPrvSv="<%=MC.M_ToDelPerm_PlsPrvSv%>";
var M_ToDelVst_FstChgRef="<%=MC.M_ToDelVst_FstChgRef%>";
var M_TotAdd_Rows="<%=MC.M_TotAdd_Rows%>";
var M_TotAdd_UpdtRows="<%=MC.M_TotAdd_UpdtRows%>";
var M_ValEtrCrit_250CharPlsEtr="<%=MC.M_ValEtrCrit_250CharPlsEtr%>";
var M_ValEtrCrit_4kCharPlsEtr="<%=MC.M_ValEtrCrit_4kCharPlsEtr%>";
var M_ValEtrCrit_MstoneDescValid="<%=MC.M_ValEtrCrit_MstoneDescValid%>";
var M_ValEtrCrt_DaysIntValid="<%=MC.M_ValEtrCrt_DaysIntValid%>";
var M_ValEtrFwg_11DgtLessValid="<%=MC.M_ValEtrFwg_11DgtLessValid%>";
var M_ValEtrFwg_2Dgtholdback="<%=MC.M_ValEtrFwg_2Dgtholdback%>";
var M_ValEtrFwg_11DgtPatCnt="<%=MC.M_ValEtrFwg_11DgtPatCnt%>";
var M_ValEtrFwg_12DgtValid="<%=MC.M_ValEtrFwg_12DgtValid%>";
var M_ValEtrFwg_AmtFmt11Dgt="<%=MC.M_ValEtrFwg_AmtFmt11Dgt%>";
var M_ValEtrFwg_InsertlessNum="<%=MC.M_ValEtrFwg_InsertlessNum%>";
var M_ValEtrFwg_IntWinPlsValid="<%=MC.M_ValEtrFwg_IntWinPlsValid%>";
var M_ValEtrFwg_IntWinValid="<%=MC.M_ValEtrFwg_IntWinValid%>";
var M_ValEtrFwg_Qnty11DgtVal="<%=MC.M_ValEtrFwg_Qnty11DgtVal%>";
var M_ValEtrFwg_ShldbeInt="<%=MC.M_ValEtrFwg_ShldbeInt%>";
var M_ValEtrFwg_UnitCst11DgtMax="<%=MC.M_ValEtrFwg_UnitCst11DgtMax%>";
var M_ValEtrInt_11DgtLessValid="<%=MC.M_ValEtrInt_11DgtLessValid%>";
var M_ValFwg_PipeQuoteValid="<%=MC.M_ValFwg_PipeQuoteValid%>";
var M_WeekEtr_Row="<%=MC.M_WeekEtr_Row%>";
var M_WeeksEtr_Row="<%=MC.M_WeeksEtr_Row%>";
var M_YouSure_WantDel="<%=MC.M_YouSure_WantDel%>";
var M_Data_SavedSucc="<%=MC.M_Data_SavedSucc%>";
var M_StdStatMstone_AddlMstone="<%=MC.M_StdStatMstone_AddlMstone%>";
var M_RowOptDefn_NoItrvlDef="<%=MC.M_RowOptDefn_NoItrvlDef%>";
var M_AddUpdtDel_Row="<%=MC.M_AddUpdtDel_Row%>";
var M_CfmVst_Chg="<%=MC.M_CfmVst_Chg%>";
var M_CntDel_CalStat="<%=MC.M_CntDel_CalStat%>";
var M_CommErr_CnctVelosSupp="<%=MC.M_CommErr_CnctVelosSupp%>";
var M_DayEtr_RowNtVld="<%=MC.M_DayEtr_RowNtVld%>";
var M_DayEtrRow_CntMonEtr="<%=MC.M_DayEtrRow_CntMonEtr%>";
var M_DayEtrRow_CntWkEtr="<%=MC.M_DayEtrRow_CntWkEtr%>";
var M_EtrCstName_Row="<%=MC.M_EtrCstName_Row%>";
var M_EtrFor_Row="<%=MC.M_EtrFor_Row%>";
var M_EtrSetAll_PowerBar="<%=MC.M_EtrSetAll_PowerBar%>";
var M_MthEtr_RowNtVld="<%=MC.M_MthEtr_RowNtVld%>";
var M_MthOrWkEtr_RowVld="<%=MC.M_MthOrWkEtr_RowVld%>";
var M_NoteMax_4kChar="<%=MC.M_NoteMax_4kChar%>";
var M_PlsSvSpec_DepdIntr="<%=MC.M_PlsSvSpec_DepdIntr%>";
var M_RowVstDupl_ChgName="<%=MC.M_RowVstDupl_ChgName%>";
var M_SetStdMstone_ForStd="<%=MC.M_SetStdMstone_ForStd%>";
var M_TotAddOrDel_Rows="<%=MC.M_TotAddOrDel_Rows%>";
var M_ValEtrFlwAft_WinLessValid="<%=MC.M_ValEtrFlwAft_WinLessValid%>";
var M_ValEtrFlwBef_WinLessValid="<%=MC.M_ValEtrFlwBef_WinLessValid%>";
var M_ValEtrFwg_AftIntrValid="<%=MC.M_ValEtrFwg_AftIntrValid%>";
var M_ValEtrFwg_BefIntrValid="<%=MC.M_ValEtrFwg_BefIntrValid%>";
var M_ValEtrFwg_IntPosVal="<%=MC.M_ValEtrFwg_IntPosVal%>";
var M_ValEtrFwg_IntrDgtPos="<%=MC.M_ValEtrFwg_IntrDgtPos%>";
var M_ValEtrFwg_LenGtVal="<%=MC.M_ValEtrFwg_LenGtVal%>";
var M_ValEtrFwg_SplCharVal="<%=MC.M_ValEtrFwg_SplCharVal%>";
var M_ValEtrFwg_VstGt2Val="<%=MC.M_ValEtrFwg_VstGt2Val%>";
var M_ValEtrFwg_VstSglVal="<%=MC.M_ValEtrFwg_VstSglVal%>";
var M_WantDel_PermSv="<%=MC.M_WantDel_PermSv%>";
var M_WkEtr_RowNtVld="<%=MC.M_WkEtr_RowNtVld%>";
var M_WkEtrRow_CntMonEtr="<%=MC.M_WkEtrRow_CntMonEtr%>";
var M_YouWtDel_PermSv="<%=MC.M_YouWtDel_PermSv%>";
var M_ValEtrFlw_DaysIntValid="<%=MC.M_ValEtrFlw_DaysIntValid%>";
var M_SureWant_DelVst="<%=MC.M_SureWant_DelVst%>";
var M_SvChldVst_BfrPrtVst="<%=MC.M_SvChldVst_BfrPrtVst%>";
var M_AddSetAft_OfflineZeroVst="<%=MC.M_AddSetAft_OfflineZeroVst%>";
var M_ParentChild_OfflineZeroVst="<%=MC.M_ParentChild_OfflineZeroVst%>";
var M_DayIntr_CntOfflineCal="<%=MC.M_DayIntr_CntOfflineCal%>";
var M_DayValid_FirstWeek="<%=MC.M_DayValid_FirstWeek%>";
var M_CboxEvtVst_MouseOut="<%=MC.M_CboxEvtVst_MouseOut%>";
var M_CvgAnly_Cal="<%=MC.M_CvgAnly_Cal%>";

/* New Message Bundle for FIN-20609 20 Oct 2011 */

var M_InvAmtNt_NegVal="<%=MC.M_InvAmtNt_NegVal%>";
var M_TotInvAmtNt_NegVal="<%=MC.M_TotInvAmtNt_NegVal%>";
var M_InvAmtGt_MstoneAmt="<%=MC.M_InvAmtGt_MstoneAmt%>";
var M_TotInvAmtGt_MstoneAmt="<%=MC.M_TotInvAmtGt_MstoneAmt%>";

//New Message for FIN-20607 : 20 oct 2011 :: Raviesh
var M_CrtRedClrVal_AmtLessInvAmt="<%=MC.M_CrtRedClrVal_AmtLessInvAmt%>";
var M_DupliSpmenIdEtr_PrvdUnqId="<%=MC.M_DupliSpmenIdEtr_PrvdUnqId%>";
//Ak:Added for enhancement PCAL-20801

var M_CpyngVstAlso_EvtChgPrmt="<%=MC.M_CpyngVstAlso_EvtChgPrmt%>";
var M_WantCpyVst_EvtChgPrmt="<%=MC.M_WantCpyVst_EvtChgPrmt%>";

var M_CrtRedClrVal_AmtLessMileAmt="<%=MC.M_CrtRedClrVal_AmtLessMileAmt%>";
//New Message added for PCAL 20071:BK
var M_VstNoIntvlDefine="<%=MC.M_VstNoIntvlDefine%>";

/* New Message Bundle Testing (External JS) 02 November 2011 */
var M_UsrDataMgr_CntRemFrmTeam="<%=MC.M_UsrDataMgr_CntRemFrmTeam%>";
var M_Del_UserFromStudyTeam="<%=MC.M_Del_UserFromStudyTeam%>";
var M_InOrgIsManager_OrgCntDel="<%=MC.M_InOrgIsManager_OrgCntDel%>";
var M_PatCurEnrl_CanNotDelOrg="<%=MC.M_PatCurEnrl_CanNotDelOrg%>";

/* New runtime additions Message Bundle Testing (concatenation issues) 04 November 2011 */

var M_BgtStat_CntChgBgt="<%=MC.M_BgtStat_CntChgBgt%>";
var M_CldrCnt_Del="<%=MC.M_CldrCnt_Del%>";
var M_CntAddEvt_CldrStat="<%=MC.M_CntAddEvt_CldrStat%>";
var M_CntDelEvt_CldrStat="<%=MC.M_CntDelEvt_CldrStat%>";
var M_CntChngEvtSeq_CldrStat="<%=MC.M_CntChngEvtSeq_CldrStat%>";
var M_CntChngStat="<%=MC.M_CntChngStat%>";
var M_CntDelFrm_OffEdtStat="<%=MC.M_CntDelFrm_OffEdtStat%>";
var M_CntDelResp_OffEdtStat="<%=MC.M_CntDelResp_OffEdtStat%>";
var M_DataExcd_MaxAlwd="<%=MC.M_DataExcd_MaxAlwd%>";
var M_DataMgrSumm_RmvdTeam="<%=MC.M_DataMgrSumm_RmvdTeam%>";
var M_Del_FrmAccLnk="<%=MC.M_Del_FrmAccLnk%>";
var M_Del_FrmAtchmt="<%=MC.M_Del_FrmAtchmt%>";
var M_Del_FrmCrfList="<%=MC.M_Del_FrmCrfList%>";
var M_Del_FrmLnks="<%=MC.M_Del_FrmLnks%>";
var M_Del_FrmPatApdx="<%=MC.M_Del_FrmPatApdx%>";
var M_Del_FrmPrslSec="<%=MC.M_Del_FrmPrslSec%>";
var M_Del_FrmRptItm="<%=MC.M_Del_FrmRptItm%>";
var M_Del_FrmSpmenApdx="<%=MC.M_Del_FrmSpmenApdx%>";
var M_Del_FrmSvdRpt="<%=MC.M_Del_FrmSvdRpt%>";
var M_DelCat_Lib="<%=MC.M_DelCat_Lib%>";
var M_DelCatLib_EvtCat="<%=MC.M_DelCatLib_EvtCat%>";
var M_DelEvtCat_DelEvtCat="<%=MC.M_DelEvtCat_DelEvtCat%>";
var M_DelFrm_Std="<%=MC.M_DelFrm_Std%>";
var M_DoYouWant_Del="<%=MC.M_DoYouWant_Del%>";
var M_EtrStdDt_ShldBeenEtr="<%=MC.M_EtrStdDt_ShldBeenEtr%>";
var M_EtrStdStrt_EdtStatEtr="<%=MC.M_EtrStdStrt_EdtStatEtr%>";
var M_EtrStdStrtDt_StatEtr="<%=MC.M_EtrStdStrtDt_StatEtr%>";
var M_EtrVldDt_ResRow="<%=MC.M_EtrVldDt_ResRow%>";
var M_FmtDispAled_PlsCont="<%=MC.M_FmtDispAled_PlsCont%>";
var M_InvdRes_PlsLongData="<%=MC.M_InvdRes_PlsLongData%>";
var M_IvdDt_PlsVldFmt="<%=MC.M_IvdDt_PlsVldFmt%>";
var M_MaxSelAlwd_DeselSel="<%=MC.M_MaxSelAlwd_DeselSel%>";
var M_NewStat_SpmenStatDt="<%=MC.M_NewStat_SpmenStatDt%>";
var M_NoFldSel_25ModSel="<%=MC.M_NoFldSel_25ModSel%>";
var M_NoMstoneSec_SelMstone="<%=MC.M_NoMstoneSec_SelMstone%>";
var M_NoSuch_PlsVldVal="<%=MC.M_NoSuch_PlsVldVal%>";
var M_NotesExcd_4kCur="<%=MC.M_NotesExcd_4kCur%>";
var M_OnlyOrgStat_LkdDelOrg="<%=MC.M_OnlyOrgStat_LkdDelOrg%>";
var M_PatCurEnrl_StatDel="<%=MC.M_PatCurEnrl_StatDel%>";
var M_PatCurrStd_OrgDelOrg="<%=MC.M_PatCurrStd_OrgDelOrg%>";
var M_PlsEtr_DataFmt="<%=MC.M_PlsEtr_DataFmt%>";
var M_PlsEtr_DataIn="<%=MC.M_PlsEtr_DataIn%>";
var M_PlsEtr_ValsFor="<%=MC.M_PlsEtr_ValsFor%>";
var M_PlsEtr_ValsForSel="<%=MC.M_PlsEtr_ValsForSel%>";
var M_PlsEtrVld_DtIn="<%=MC.M_PlsEtrVld_DtIn%>";
var M_PlsSel_VldFileTry="<%=MC.M_PlsSel_VldFileTry%>";
var M_RsonNtExcd_4kCharCur="<%=MC.M_RsonNtExcd_4kCharCur%>";
var M_SelDuplLoc_UnqSpmen="<%=MC.M_SelDuplLoc_UnqSpmen%>";
var M_SeqNumAldy_ChgSeq="<%=MC.M_SeqNumAldy_ChgSeq%>";
var M_SpmenStat_ChldPrmt="<%=MC.M_SpmenStat_ChldPrmt%>";
var M_StatCnt_BeModf="<%=MC.M_StatCnt_BeModf%>";
var M_SureWantDel_Rpt="<%=MC.M_SureWantDel_Rpt%>";
var M_SureWantDel_Tplt="<%=MC.M_SureWantDel_Tplt%>";
var M_TestRsltLong_LabRsltRow="<%=MC.M_TestRsltLong_LabRsltRow%>";
var M_ThsOrgMgr_OrgCntDel="<%=MC.M_ThsOrgMgr_OrgCntDel%>";
var M_TmentArmAldy_CntDel="<%=MC.M_TmentArmAldy_CntDel%>";
var M_TopicExcd_4kChar="<%=MC.M_TopicExcd_4kChar%>";
var M_Want_DelEvtStat="<%=MC.M_Want_DelEvtStat%>";
var M_Want_DelInv="<%=MC.M_Want_DelInv%>";
var M_Want_DelRule="<%=MC.M_Want_DelRule%>";
var M_Want_DelStd="<%=MC.M_Want_DelStd%>";
var M_Want_DelTmtArm="<%=MC.M_Want_DelTmtArm%>";
var M_WantDel_MtgTopic="<%=MC.M_WantDel_MtgTopic%>";
var M_WantDel_Pat="<%=MC.M_WantDel_Pat%>";
var M_WantDel_PatStd="<%=MC.M_WantDel_PatStd%>";
var M_WantDel_PvisoEtr="<%=MC.M_WantDel_PvisoEtr%>";
var M_TotQtyChildComp_LtEqComp="<%=MC.M_TotQtyChildComp_LtEqComp%>";
var M_Row_PlsEtrVal="<%=MC.M_Row_PlsEtrVal%>";
var M_Row_PlsSelQualifier="<%=MC.M_Row_PlsSelQualifier%>";
var M_Row_PlsSelFld="<%=MC.M_Row_PlsSelFld%>";
var M_TmentArmAldy_PatCntDel="<%=MC.M_TmentArmAldy_PatCntDel%>";
var M_PlsEtr_ValsInFld="<%=MC.M_PlsEtr_ValsInFld%>";
var M_PlsEtrVal_GtOrLtEqtVal="<%=MC.M_PlsEtrVal_GtOrLtEqtVal%>";
var M_MinLen_Is="<%=MC.M_MinLen_Is%>";
var M_ChgInclLoss_DataEtrProc="<%=MC.M_ChgInclLoss_DataEtrProc%>";
var M_DelPcol_Cal="<%=MC.M_DelPcol_Cal%>";
var M_ActDelSpmen_SelWantCont="<%=MC.M_ActDelSpmen_SelWantCont%>";
var M_WantToUnSch_EvtsVisit="<%=MC.M_WantToUnSch_EvtsVisit%>";
var M_FunErr_IncrtMth="<%=MC.M_FunErr_IncrtMth%>";

/* New Message Bundle entries (External JS) 30 November 2011 */
var M_InsuffAccRgt_CnctSysAdmin="<%=MC.M_InsuffAccRgt_CnctSysAdmin%>";
var M_PlsSel_OneStd="<%=MC.M_PlsSel_OneStd%>";
var M_TrialCatNat_ExtrIndlStdPg="<%=MC.M_TrialCatNat_ExtrIndlStdPg%>";
var M_SureWantDel_SelDrfts="<%=MC.M_SureWantDel_SelDrfts%>";

var CTRP_ReadyDraftsDownload = "<%=MC.CTRP_ReadyDraftsDownload%>";
var CTRP_IndNonIndDraftsDownload ="<%=MC.CTRP_IndNonIndDraftsDownload%>";
//Yogendra Pratap Added For CTRP CSV Download
var CTRP_ConfirmDraftDownload = "<%=MC.CTRP_ConfirmDraftDownload%>";

//Section for CTRP module messages -START 
var CTRP_ReadyDraftsDownload="<%=MC.CTRP_ReadyDraftsDownload%>";
var CTRP_IndNonIndDraftsDownload = "<%=MC.CTRP_IndNonIndDraftsDownload%>";
var M_NoDataDboard_QryActv = "<%=MC.M_NoDataDboard_QryActv%>";
var M_FetchDboard_QryAct = "<%=MC.M_FetchDboard_QryAct%>";
var M_NoDataDboard_FrmExptcy = "<%=MC.M_NoDataDboard_FrmExptcy%>";
var M_FetchDboard_FrmExptcy = "<%=MC.M_FetchDboard_FrmExptcy%>";
var M_NoDataDboard_OverFrms = "<%=MC.M_NoDataDboard_OverFrms%>";
var M_FetchDboard_OverFrm = "<%=MC.M_FetchDboard_OverFrm%>";
var M_NoDataDboard_AdvEvts = "<%=MC.M_NoDataDboard_AdvEvts%>";
var M_FetchDboard_AdvEvt = "<%=MC.M_FetchDboard_AdvEvt%>";
var M_NoDataDboard_VstReltd = "<%=MC.M_NoDataDboard_VstReltd%>";
var M_FetchDboard_VstRelated = "<%=MC.M_FetchDboard_VstRelated%>";
var M_ChgExcd4000_Char = "<%=MC.M_ChgExcd4000_Char%>";
var M_PlsVldFile_TryAgain = "<%=MC.M_PlsVldFile_TryAgain%>";
var M_NoDataDboard_Bgt = "<%=MC.M_NoDataDboard_Bgt%>";
var M_FetchData_DboardBgt = "<%=MC.M_FetchData_DboardBgt%>";
var M_FetchDboard = "<%=MC.M_FetchDboard%>";
var M_NoDataDboard_Inv = "<%=MC.M_NoDataDboard_Inv%>";
var M_FetchDboard_Inv = "<%=MC.M_FetchDboard_Inv%>";
var M_NoDataDboard_Payment = "<%=MC.M_NoDataDboard_Payment%>";
var M_FetchDboard_Payment = "<%=MC.M_FetchDboard_Payment%>";
//Section for CTRP module messages -END

// PCAL-22322
var M_CntEvt_VisitCal = "<%=MC.M_CntEvt_VisitCal%>";
var M_StdNumExst = "<%=MC.M_StdNumExst%>";
var M_EthrVNSelOrCorrect = "<%=MC.M_EthrVNSelOrCorrect%>";
var M_RefVisitDelSelOrCorrect = "<%=MC.M_RefVisitDelSelOrCorrect%>";
var M_CircularRelationFound = "<%=MC.M_CircularRelationFound%>";
var M_EntrIntrvlInMWD = "<%=MC.M_EntrIntrvlInMWD%>";
var M_EntrIntrvlInInsertAfter = "<%=MC.M_EntrIntrvlInInsertAfter%>";
var M_FixedTimePoint_Columns="<%=MC.M_FixedTimePoint_Columns%>";
var M_DepTimePoint_Columns="<%=MC.M_DepTimePoint_Columns%>";

// Snapshot
var M_Study_Snapshot="<%=MC.M_Study_Snapshot%>";
var M_HowToTakeSnapshot="<%=MC.M_HowToTakeSnapshot%>";
var M_HowToTakeSnapshotCases="<%=MC.M_HowToTakeSnapshotCases%>";
var M_HowToTakeSnapshotFirstCase="<%=MC.M_HowToTakeSnapshotFirstCase%>";
var M_HowToTakeSnapshotSecondCase="<%=MC.M_HowToTakeSnapshotSecondCase%>";
var M_HowToTakeSnapshotThirdCase="<%=MC.M_HowToTakeSnapshotThirdCase%>";
var M_UpdateSnapshotManulally="<%=MC.M_UpdateSnapshotManulally%>";
var M_HowToUpdateSnapshotManulally="<%=MC.M_HowToUpdateSnapshotManulally%>";
var M_UpdateSnapshot="<%=MC.M_UpdateSnapshot%>";
var M_LinkOnStudyBrowsers="<%=MC.M_LinkOnStudyBrowsers%>";

var M_Click_SortAscend = "<%=MC.M_Click_SortAscend%>";
var M_Click_SortDescend = "<%=MC.M_Click_SortDescend%>";
/* For INF-22330 Enhancement 01-Aug-2012 -Sudhir*/
var M_LoggedInDiffrentSessionAreYouSureToReLogin = "<%=MC.M_LoggedInDiffrentSessionAreYouSureToReLogin%>";
var M_NewSessionwasInitiated = "<%=MC.M_NewSessionwasInitiated%>";
var M_SessionTerminated = "<%=MC.M_SessionTerminated%>";
var M_InternetConnectionDisabled = "<%=MC.M_InternetConnectionDisabled%>";
var M_Login_Error = "<%=MC.M_Login_Error%>";
var M_NoRecordsFound ="<%=MC.M_NoRecordsFound%>"
var M_StrgSpec_CntChg="<%=MC.M_StrgSpec_CntChg%>";
var M_PrevStrg_CapGtr = "<%=MC.M_PrevStrg_CapGtr%>";
var M_ParentStrg_ChldStrgDims = "<%=MC.M_ParentStrg_ChldStrgDims%>";
var M_ChldSpec_CntBlank = "<%=MC.M_ChldSpec_CntBlank%>";
var M_ChldSpec_Duplicate = "<%=MC.M_ChldSpec_Duplicate%>";
var M_LocBeyond_Capacity = "<%=MC.M_LocBeyond_Capacity%>";
var M_VstNoIntvlDef_CantSet = '<%=MC.M_VstNoIntvlDef_CantSet%>';
var M_DtToNotLessThan_DtFrom ="<%=MC.M_DtToNotLessThan_DtFrom%>"
var M_AtLeast_OneCheckbox ="<%=MC.M_AtLeast_OneCheckbox%>"
var M_DelTheFollowingInvcs ="<%=MC.M_DelTheFollowingInvcs%>"
var	M_InvalidNum_EtrAgain="<%=MC.M_InvalidNum_EtrAgain%>"
var	M_AmtBeforeDecimalValueLimitExceeds="<%=MC.M_AmtBeforeDecimalValueLimitExceeds%>"
var	M_Invc_PaymentsReconciledAgainstInvcsCannotDeleted="<%=MC.M_Invc_PaymentsReconciledAgainstInvcsCannotDeleted%>";
var	M_Invc_PaymentsReconciledAgainstInvcCannotDeleted="<%=MC.M_Invc_PaymentsReconciledAgainstInvcCannotDeleted%>";
var	M_And="<%=MC.M_And%>";
var M_InvCnt_DelSucc="<%=MC.M_InvCnt_DelSucc%>";
var M_Failed="<%=MC.M_Failed%>";
var M_NotDelete="<%=MC.M_NotDelete%>";
var M_ExstngInvcNumberPlsModifyOrLeaveBlank="<%=MC.M_ExstngInvcNumberPlsModifyOrLeaveBlank%>";
var M_ExstngInvcNumberPlsModify="<%=MC.M_ExstngInvcNumberPlsModify%>";
var M_CheckToSelectDisplayedInvc="<%=MC.M_CheckToSelectDisplayedInvc%>";
var M_PymntDueDtMustNum_ReEtr="<%=MC.M_PymntDueDtMustNum_ReEtr%>";
var M_PymntDueInMustNum_ReEtr="<%=MC.M_PymntDueInMustNum_ReEtr%>";
var M_PymntDueDtMust_GtZero="<%=MC.M_PymntDueDtMust_GtZero%>";
var M_PymntDueInMust_GtZero="<%=MC.M_PymntDueInMust_GtZero%>";
var M_DataNotDel_Succ = "<%=MC.M_DataNotDel_Succ%>";
var M_DataNotSvd_Succ= "<%=MC.M_DataNotSvd_Succ%>";
var M_Data_SvdSucc= "<%=MC.M_Data_SvdSucc%>";
var M_SelAtLeast_OneOpt= "<%=MC.M_SelAtLeast_OneOpt%>"
var	M_PlsEtr_ValuesFor= "<%=MC.M_PlsEtr_ValuesFor%>"
var	M_Holdback_Amt= "<%=MC.M_Holdback_Amt%>"
var	M_Invoice_Amt= "<%=MC.M_Invoice_Amt%>"
var	M_PleaseSelectMile= "<%=MC.M_PleaseSelectMile%>"
var	M_CTRP_Create_Drft= "<%=MC.M_CTRP_Create_Drft%>";
var M_CTRP_SysGen_Stat= "<%=MC.M_CTRP_SysGen_Stat%>";
var	M_CTRP_WIP_DRFT_Stat= "<%=MC.M_CTRP_WIP_DRFT_Stat%>";
var	M_CTRP_NCI_Stat_WIP= "<%=MC.M_CTRP_NCI_Stat_WIP%>";
var	M_CTRP_Cpy_Drft= "<%=MC.M_CTRP_Cpy_Drft%>";
var	M_CTRP_ONLY_DRFT_DWN= "<%=MC.M_CTRP_ONLY_DRFT_DWN%>";
var	M_CTRP_ONLY_STD_UNMRK= "<%=MC.M_CTRP_ONLY_STD_UNMRK%>";
var	M_CTRP_ONLY_DRFT_DEL= "<%=MC.M_CTRP_ONLY_DRFT_DEL%>";
var	M_CTRP_DRFT_EDIT= "<%=MC.M_CTRP_DRFT_EDIT%>";
var	M_PlzEtr_validPass= "<%=MC.M_PlzEtr_validPass%>";
var	M_CTRP_WIP_NCI_STAT= "<%=MC.M_CTRP_WIP_NCI_STAT%>";
var M_CTRP_ACCR_Val_Fail="<%=MC.M_CTRP_ACCR_Val_Fail %>";
var M_CTRP_ACCR_Val_Fail_Rea="<%=MC.M_CTRP_ACCR_Val_Fail_Rea %>";
var M_PatEnrSite_NotOnStdTeam="<%=MC.M_PatEnrSite_NotOnStdTeam %>";
var M_DtCnt_FutDt="<%=MC.M_DtCnt_FutDt%>";
var M_LoginIdExst_EtrNew ="<%=MC.M_LoginIdExst_EtrNew %>";
var M_UsrName_AldyExst ="<%=MC.M_UsrName_AldyExst %>";
var M_EmailIdExst_Cont ="<%=MC.M_EmailIdExst_Cont %>";
var M_CantDelete_LockDownFrmResp ="<%=MC.M_CantDelete_LockDownFrmResp %>";
var M_Version_Freeze ="<%=MC.M_Version_Freeze %>";
var M_Uncheck_Indirects ="<%=MC.M_Uncheck_Indirects %>";
var M_DelAch_UsingBtn ="<%=MC.M_DelAch_UsingBtn %>";
var M_First_Visit="<%=MC.M_First_Visit%>";
var M_First_Visit_NotDepnd="<%=MC.M_First_Visit_NotDepnd%>";
var M_Previous_visit="<%=MC.M_Previous_visit%>";
var M_Lock_Down="<%=MC.M_Lock_Down%>";
var M_NtLevel_RchMaxLevel="<%=MC.M_NtLevel_RchMaxLevel%>";
var M_NtDelMesg="<%=MC.M_NtDelMesg%>";
var M_NtUsrDelMesg="<%=MC.M_NtUsrDelMesg%>";
var M_DelDoc="<%=MC.M_DelDoc%>";
var M_Select_Val_First="<%=MC.M_Select_Val_First%>";
var M_assign_user="<%=MC.M_assign_user%>";
var M_eSignConfig="<%=Configuration.eSignConf%>";
var M_Sel_Only_OneSite="<%=MC.M_Sel_Only_OneSite%>";
var M_Sel_SiteFrst="<%=MC.M_Sel_SiteFrst%>";
var M_Try_Again ="<%=MC.M_Try_Again %>";
var M_Err_Msg ="<%=MC.M_Err_Msg %>";
var M_Chk_Email ="<%=MC.M_Chk_Email %>";
var M_FailLogin_AttemptAccForm ="<%=MC.M_FailLogin_AttemptAccForm %>";
</script>