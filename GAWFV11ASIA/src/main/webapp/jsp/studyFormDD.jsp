<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@ page language = "java" import = "com.velos.eres.web.user.UserJB,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*,com.velos.eres.business.common.*,java.util.*"%>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<script>
var historyWin;
function openHistoryWin(tabsubtype,study,pkSubmission,submissionBoardPK,appSubmissionType) {
	var w = screen.availWidth-100;
	historyWin = window.open("irbhistory.jsp?appSubmissionType="+appSubmissionType+"&tabsubtype="+tabsubtype+"&studyId="+study+"&submissionPK="+pkSubmission+"&submissionBoardPK="+ submissionBoardPK,"historyWin","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=100");
	historyWin.focus();
}
function openActionWin(studyId, submissionPK, submissionBoardPK, IRBSelectedTab,provisoflag) {
    var actionWin = window.open("irbactionwin.jsp?&studyId="+studyId+"&submissionPK="+submissionPK+"&submissionBoardPK="+submissionBoardPK+"&selectedTab="+IRBSelectedTab+"&provisoflag="+provisoflag,"actionWin","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=900,height=700,left=10,top=100");
	if (!actionWin) { actionWin.focus(); }
}
function reloadIrbForms() {
	if (top != null && top.reloadOpener != undefined) {
		top.reloadOpener();
	}
}

</script>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="lnkformB" scope="request" class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
		 
		 <% 
		 
		 HttpSession tSession = request.getSession(true); 
 
		if (sessionmaint.isValidSession(tSession)) {
	
	  	 ArrayList arrFrmNames = null;
		 ArrayList arrNumEntries = null;
 		 ArrayList arrFrmIds = null;
		 ArrayList arrEntryChar = null;
		 ArrayList arrFrmInfo = new ArrayList();
		 String frmName = "";
		 String entryChar="";
		 String numEntries = "";
		 String frmInfo= "";
		 int formId = 0;
		 String firstFormInfo = "";
	
	 	 boolean isIrb = false;
		 
		 String strFormId = request.getParameter("formPullDown");	 
		 String formCategory = request.getParameter("formCategory")==null?"":request.getParameter("formCategory");	 
		 String submissionType = request.getParameter("submissionType");	 
		 String calledFromForm = request.getParameter("calledFromForm");	 
		 String targateframe =  request.getParameter("target");	 
		 String studyId = StringUtil.htmlEncodeXss(request.getParameter("studyId"));
		 String tabsubtype = StringUtil.htmlEncodeXss(request.getParameter("tabsubtype"));
		 String appSubmissionType = StringUtil.htmlEncodeXss(request.getParameter("appSubmissionType"));
		 String provisoflag = request.getParameter("provisoflag")==null?"":request.getParameter("provisoflag");
		 String submissionPK = StringUtil.htmlEncodeXss(request.getParameter("submissionPK"));
		 String submissionBoardPK = StringUtil.htmlEncodeXss(request.getParameter("submissionBoardPK"));
		 String hiddenflag = request.getParameter("hiddenflag")==null?"":request.getParameter("hiddenflag");
		 String hide = request.getParameter("hide")==null?"":request.getParameter("hide");
		 String newflag = request.getParameter("newflag")==null?"":request.getParameter("newflag");
		 String ongoingflag = request.getParameter("ongoingflag")==null?"":request.getParameter("ongoingflag");
		 System.out.println("ongoingflag"+ongoingflag);
		 String pksubmissionStatus = request.getParameter("pksubmissionStatus")==null?"":request.getParameter("pksubmissionStatus");
		 int study_acc_form_right = EJBUtil.stringToNum(request.getParameter("study_acc_form_right"));	 
		 int study_team_form_access_right = EJBUtil.stringToNum(request.getParameter("study_team_form_access_right"));	 

		 String accId=(String)tSession.getAttribute("accountId");
		 int iaccId=EJBUtil.stringToNum(accId);
		 
 	     String userId = (String) tSession.getValue("userId");
	     UserJB userB = (UserJB) tSession.getValue("currentUser");
   	     String siteId = userB.getUserSiteId();
	 	 
	 	 int istudyId = EJBUtil.stringToNum(studyId);
		
		  
			 
		 if (! StringUtil.isEmpty(formCategory) && ! StringUtil.isEmpty(submissionType))
		 {
		 	 isIrb=true;
		 }
		 
		 if ("undefined".equals(tabsubtype)) {
		     isIrb=false;
		 }
		 
		 LinkedFormsDao lnkFrmDao = new LinkedFormsDao();
		 if (isIrb) {
		 lnkFrmDao = lnkformB.getStudyForms(iaccId,istudyId, EJBUtil.stringToNum(userId), EJBUtil.stringToNum(siteId), 
		         study_acc_form_right,  study_team_form_access_right, 
		         isIrb, submissionType, formCategory);
		 }
	 
		 arrFrmIds = lnkFrmDao.getFormId();		 
		 arrFrmNames = lnkFrmDao.getFormName();
		 arrEntryChar = lnkFrmDao.getFormEntryChar();
		 arrNumEntries = lnkFrmDao.getFormSavedEntriesNum();
		
		 
		
		 if (arrFrmIds.size() > 0) { 	
    		 if (strFormId==null) {
    		 	formId = EJBUtil.stringToNum(arrFrmIds.get(0).toString());
    			entryChar = arrEntryChar.get(0).toString();
    			numEntries = arrNumEntries.get(0).toString();		   
    			firstFormInfo = arrFrmIds.get(0).toString() + "*"+ entryChar + "*" + arrNumEntries.get(0).toString();
    		 }
    		 else {
    		 	 StringTokenizer strTokenizer = new StringTokenizer(strFormId,"*");
    		 	 if (calledFromForm.equals(""))
    	 		  
    			 	formId = EJBUtil.stringToNum(strTokenizer.nextToken());
    			 	entryChar = strTokenizer.nextToken();
    			 	
    			 	lnkformB.findByFormId(formId );
		   		 	numEntries = lnkformB.getLfDataCnt();
    			 	//get the number of times the form was answered
    			 	//prepare strFormId again
    			 	strFormId = formId + "*" + entryChar + "*" + numEntries;
    			 	firstFormInfo = strFormId;	   
    			 
    			 
    		 }
    		 
    		 
    		 for (int i=0;i<arrFrmIds.size();i++)
    		 {  //store the formId, entryChar and num Entries separated with a *
    		  	 frmInfo = arrFrmIds.get(i).toString() + "*"+ arrEntryChar.get(i).toString() + "*" + arrNumEntries.get(i).toString();
    		 	 arrFrmInfo.add(frmInfo);		 
    		 }
    		 		
    	}		 
    	 	 String dformPullDown = EJBUtil.createPullDownWithStrNoSelect("formPullDown",firstFormInfo, arrFrmInfo , arrFrmNames);
    	 	
    	 	StringBuffer formBuffer = new StringBuffer(dformPullDown);

			 //submit the form when the user selects the form name
			 
			 formBuffer.replace(0,7,"<SELECT onChange=\"document.studyform.submit();\"");
			 
			  
			 dformPullDown = formBuffer.toString();
			 
			 
    	 	%>
    	 	<%-- For File Upload Functionality--%>
    	 	<script>
    	 	function openNewMultiVersionWin(pgRight,pageRightApndx,mode,studyId)
    	 	{	if (f_check_perm(pgRight,'N') == true && f_check_perm(pageRightApndx,'N') == true) {
    	 		//windowName= window.open("appendix_file_multi.jsp?docflag=N&mode=" + mode + "&studyId=" +  studyId,"information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=2150,height=400");
	 			windowName= window.open("appendix_file_multi.jsp?docflag=N&mode=" + mode + "&studyId=" +  studyId,"information","toolbar=no, scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=1150,height=400");
    	 			windowName.focus();
    	 		}
    	 	}

    	 	
    	 	</script>
    	 	<%
    	 	int pageRight=7;
    	 	int pageRightApndx=7;
    	 	
    	 	
			
	   		
			 %>
    	 <%-- end of For File Upload Functionality--%>	
    	 	
    	 		<form action="formfilledstudybrowser.jsp" method="POST" name="studyform" id="studyform" target="<%=targateframe%>">
                <div id="ddOpt" style="font-family:Verdana,Arial,Helvetica,sans-serif;font-size:11">
    	 		<table><tr><td><p class="sectionHeadings"><%=LC.L_Select_AForm%><%--Select a Form*****--%>: <%=dformPullDown%></p></td>
    	 		<td>&nbsp;<button type="submit"  name="goButton" value="<%=LC.L_Go%><%--Go*****--%>"><%=LC.L_Go%></button></td>
    	 		
    	 		<td><A href=# onclick="openNewMultiVersionWin(<%=pageRight%>,<%=pageRightApndx%>,'N','<%=studyId%>')">Attach Documents<%--ADD NEW VERSION/DOCUMENT*****--%></A></td>
    	 		<td><A href="studyVerBrowser.jsp?mode=N&appSubmissionType=<%=appSubmissionType%>&tabsubtype=<%=tabsubtype%>&submissionPK=<%=submissionPK%>&submissionBoardPK=<%=submissionBoardPK%>&pksubmissionStatus=<%=pksubmissionStatus%>&hide=<%=hide%>&newflag=<%=newflag%>&studyId=<%=studyId%>&ongoingflag=<%=ongoingflag%>&hiddenflag=0">View Documents<%--ADD NEW VERSION/DOCUMENT*****--%></A></td>
                
    	 		
    	 		</td><td>&nbsp;</td>
            <%  
			    Hashtable htIRBParams = (Hashtable)tSession.getAttribute("IRBParams");
                if (htIRBParams != null) {
                    if (submissionPK == null) {
                        submissionPK = (String) htIRBParams.get("submissionPK");
                    }
                    if (submissionBoardPK == null) {
    					submissionBoardPK = (String) htIRBParams.get("submissionBoardPK");
                    }
                    if (tabsubtype == null) {
                        tabsubtype = (String) htIRBParams.get("selectedTab");
                    }
                }
                if (tabsubtype != null && EIRBDao.isTabForActionWindow(tabsubtype)) { %>
                <td align="center" style="font-family:Verdana,Arial,Helvetica,sans-serif;font-size:10">
                <a title="<%=LC.L_ShowSub_History%><%--Show Submission History*****--%>" href="javascript:void(0);"
                  onclick="openHistoryWin('<%=tabsubtype%>',<%=studyId%>,<%=submissionPK%>,
                           <%=submissionBoardPK%>,'<%=appSubmissionType%>')"><%=LC.L_Sub_Hist%><%--Submission<br/>History*****--%></a>
                </td>
                <td>&nbsp;</td>
                <td align="center" style="font-family:Verdana,Arial,Helvetica,sans-serif;font-size:10">
                <a href="javascript:void(0);" onclick="openActionWin(<%=studyId%>,<%=submissionPK%>,
                           <%=submissionBoardPK%>,'<%=tabsubtype%>','<%=provisoflag%>')"><%=LC.L_Act_Page%><%--Action<br/>Page*****--%></a>
                </td>
            <%  }  %>
    	 		</tr></table>
                </div>
    	 		
    	 		 
    	 			
		    	 	<input type="hidden" name="studyId" value=<%=studyId%>>
		    	 	<input type="hidden" name="flag" value="Y">				
		    		<input type="hidden" name="calledFromForm" value=<%=calledFromForm%>>
		    		
		    		<input type="hidden" name="showPanel" value=false>
		    			
		    		<input type="hidden" name="formCategory" value=<%=formCategory%>>
		    		<input type="hidden" name="hiddenflag" value=<%=hiddenflag%>>
		    		<input type="hidden" name="submissionType" value=<%=submissionType%>>
		    		<input type="hidden" name="irbReviewForm" value="true">
		    	
    	
    	 		</form>
    	 		
    	 		<% 
    		} //end of session check 
    		
    		
    		 %>