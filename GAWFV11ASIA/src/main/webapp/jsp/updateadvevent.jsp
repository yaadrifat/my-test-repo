<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>  
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></HEAD>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
<jsp:useBean id="adveventB" scope="request" class="com.velos.esch.web.advEve.AdvEveJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import = "com.velos.esch.business.common.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.MC"%>
<%

   int ret=2;
   String src =request.getParameter("srcmenu");   
   String mode =request.getParameter("mode");
   String eventId= request.getParameter("eventId");
   String adve_type = request.getParameter("adve_type");
   String descripton = request.getParameter("descripton");
   String startDt = request.getParameter("startDt");
   String stopDt = request.getParameter("stopDt");
   String enteredBy= request.getParameter("enteredBy"); 
   String outcomeString= request.getParameter("outcomeString");
   String outcomeDt = request.getParameter("outcomeDt");
   String addInfoString= request.getParameter("addInfoString");
   String notes = request.getParameter("notes"); 
   String adveventId=request.getParameter("adveventId");   
   String patProtId= request.getParameter("patProtId");
    int advId=0;
   String studyNum = request.getParameter("studyNum");
   

   
   String death = request.getParameter("death");

   String studyId =request.getParameter("studyId");	

   String statDesc=request.getParameter("statDesc");
   String statid=request.getParameter("statid");
	String studyVer=request.getParameter("studyVer");	
	String patientCode =request.getParameter("patientCode");	

	
   
  // out.println("adveventId" +adveventId);
   if (mode.equals("M"))

 {
    advId=EJBUtil.stringToNum(request.getParameter("adveventId"));
  }

//out.println("adveventId" +adveventId);

   String eSign = request.getParameter("eSign");
   String outcomeNotes=request.getParameter("outcomeNotes");		   

   String msg="";	
   String pkey = request.getParameter("pkey");
   String eventName=request.getParameter("eventName");
   String visit=request.getParameter("visit");	

/* out.println("pkey" +pkey);
out.println("eventName" +eventName);
out.println("visit" +visit);
out.println("eventId" +eventId); */


HttpSession tSession = request.getSession(true); 


 if (sessionmaint.isValidSession(tSession))
   {	
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%   
 
   
   	String oldESign = (String) tSession.getValue("eSign");


	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {

 String ipAdd = (String) tSession.getValue("ipAdd");
 String usr = null;
 usr = (String) tSession.getValue("userId");



 if (mode.equals("M"))

 {

   adveventB.setAdvEveId(advId); 

 }

  // eventId = "32397";
   adveventB.setAdvEveEvents1Id(eventId);
   adveventB.setAdvEveCodelstAeTypeId(adve_type);
   adveventB.setAdvEveDesc(descripton);
   adveventB.setAdvEveStDate(startDt);
   adveventB.setAdvEveEndDate(stopDt);
   
   adveventB.setFkStudy(studyId);
   adveventB.setFkPer(pkey);
   adveventB.setAdvEveEnterBy(enteredBy);
   adveventB.setAdvEveOutType(outcomeString);
   adveventB.setAdvEveOutDate(outcomeDt);
   adveventB.setAdvEveAddInfo(addInfoString);
   adveventB.setAdvEveNotes(notes); 
   adveventB.setAdvEveOutNotes(outcomeNotes);

//out.println("mode is "+ mode);


   if (mode.equals("M")) {

	 adveventB.setModifiedBy(usr);
	 adveventB.setIpAdd(ipAdd);
	 ret =adveventB.updateAdvEve();

   }  else {     

	adveventB.setCreator(usr);
	adveventB.setIpAdd(ipAdd);   
	adveventB.setAdvEveDetails();

   }

   if (ret == 0) {

	msg = MC.M_AdvEvtDet_SvdSucc;/*msg = "Adverse Event details saved successfully";******/
   }
   else {
	msg = MC.M_AdvEvtDet_NotSvd;/*msg = "Adverse Event details not saved";******/
   }  

/*
   out.println("eventId"+eventId);
   out.println("adve_type"+adve_type);
   out.println("descripton" + descripton );
   out.println("startDt"+startDt);
   out.println("stopDt "+stopDt );
   out.println("enteredBy"+enteredBy);
   out.println("outcomeString"+outcomeString);
   out.println("outcomeDt "+outcomeDt );
   out.println("addInfoString"+addInfoString);
   out.println("notes"+notes);  
   out.println("studyId"+studyId);  
   out.println("pkey"+pkey);  
*/
%>
<br>
<br>
<br>
<br>
<br>
<%//out.println(patProtId);%>
<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%> </p>


<%	if(death.equals("Yes"))	{	%>

<META HTTP-EQUIV=Refresh CONTENT="1; URL=patstudystatus.jsp?srcmenu=<%=src%>&studyId=<%=studyId%>&statid=<%=statid%>&studyNum=<%=studyNum%>&pcode=<%=patientCode%>">

	<%	}else{	%>

	<META HTTP-EQUIV=Refresh CONTENT="1; URL=adveventbrowser.jsp?srcmenu=<%=src%>&page=patientEnroll&selectedTab=6&pkey=<%=pkey%>&visit=<%=visit%>&eventname=<%=eventName%>&eventId=<%=eventId%>&generate=N&studyId=<%=studyId%>&statDesc=<%=statDesc%>&statid=<%=statid%>&patProtId=<%=patProtId%>&studyVer=<%=studyVer%>&patientCode=<%=patientCode%>">

	<% }  }


} //end of if for eSign check


else {   

%>
  <jsp:include page="timeout.html" flush="true"/>
<%
}
%> 


</BODY>
</HTML>
