<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<html>
<head>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.user.UserJB"%>
<jsp:useBean id="commonB" scope="request" class="com.velos.eres.web.common.CommonJB"/>

<title><%--Personalize Account >> Change Password*****--%><%=MC.M_PrsnlzAcc_ChgPwd %></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT Language="javascript">
function  validate(formobj){
	//	formobj=document.pwd;
    //Added by Manimaran to fix the Bug #2532.
	formobj.oldPass.value=fnTrimSpaces(formobj.oldPass.value);
	formobj.newPass.value=fnTrimSpaces(formobj.newPass.value);
	formobj.confPass.value=fnTrimSpaces(formobj.confPass.value);
	formobj.newESign.value=fnTrimSpaces(formobj.newESign.value);
	formobj.oldESign.value=fnTrimSpaces(formobj.oldESign.value);
	formobj.confESign.value=fnTrimSpaces(formobj.confESign.value);

	if ((formobj.checkPass.checked == false) && (formobj.checkESign.checked == false)) {
		alert("<%=MC.M_SelChgPwdOr_Esign%>");/*alert("Please select either Change Password or Change e-Signature.");*****/
		return false;
	}

	if (!(validate_col('e-Signature',formobj.eSign))) return false;

<%-- 	if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again.");*****/
		formobj.eSign.focus();
		return false;
   }
 --%>
    //loginName = formobj.loginName.value;
    // newPass = formobj.newPass.value;
    // if (loginName==newPass) {
	//   alert("<%=MC.M_PwdCnt_PlsEtr%>");/*alert("Password cannot be same as User Name. Please enter again.");*****/
	//	formobj.newPass.focus();
	//	return false;
	//}


	// Added by Gopu to fix the issues #1917 trim leading  and trailing spaces for the eSign Expiry

	formobj.daysExpESign.value = fnTrimSpaces(formobj.daysExpESign.value);

	if( !(isInteger(formobj.daysExpESign.value))){
		alert("<%=MC.M_InvalidExp_PlsEtrAgn%>");/*alert("Invalid Expiration days.Please enter again.");*****/
		formobj.daysExpESign.focus();
		return false;
	}

	// Added by Gopu to fix the issues #1917 trim leading  and trailing spaces for the password Expiry
   formobj.daysExp.value = fnTrimSpaces(formobj.daysExp.value);
	if(!(isInteger(formobj.daysExp.value))){
		alert("<%=MC.M_InvalidExp_PlsEtrAgn%>");/*alert("Invalid expiration days. Please enter again.");*****/
		formobj.daysExp.focus();
		return false;
	}

    if(formobj.checkPass.checked) {

    	//User Password Validation Check for INF-18669 : Raviesh
    	var isSubmitFlag=$j("#isSubmitFlag").val();
    	if(isSubmitFlag=="false"){
    		alert("<%=MC.M_PlzEtr_validPass%>");
    		formobj.newPass.focus();
    		return false;
    	}
    	
		if (!(validate_col('session',formobj.newPass))) return false
		if (!(validate_col('session',formobj.confPass))) return false
		// modified by gopu to fix the issues #1917 for trim leading and trailing spaces
		//if( fnTrimSpaces(formobj.newPass.value).length < 8){
		//	alert("<%=MC.M_NewPwd_Atleast8CharsLong%>");/*alert("The New Password should be atleast 8 characters long. Please enter again.");*****/
		//		formobj.newPass.focus();
		//		return false;
		//	}
		// modified by gopu to fix the issues #1917 for trim leading and trailing spaces
		if( (fnTrimSpaces(formobj.newPass.value)) != (fnTrimSpaces(formobj.confPass.value))){
			alert("<%=MC.M_NewConfirm_PwdSame%>");/*alert("Values in 'New Password' and 'Confirm Password' are not same. Please enter again.");*****/
				formobj.confPass.focus();
				return false;
		}

		//JM:
		//if (formobj.daysExp.value==0) formobj.daysExp.value=365;


	//JM: 17OCT2006: added
		formobj.daysExp.value = fnTrimSpaces(formobj.daysExp.value);
		/*Added For Bug#10355 Date:3 July 2012 By : Yogendra Pratap*/
		if(isNaN(parseInt(formobj.daysExp.value))){
			alert("<%=MC.M_InvalidExp_PlsEtrAgn%>");/*alert("Invalid expiration days. Please enter again.");*****/
			formobj.daysExp.focus();
			return false;
		}
		
		if (parseInt(formobj.daysExp.value) < 1 ){
			alert("<%=MC.M_PwdExp_Less1Day%>");/*alert("Password Expires can not be less than 1 day");*****/
				formobj.daysExp.focus();
				return false;
		}

	}
	   if(formobj.checkESign.checked) {
			if (!(validate_col('session',formobj.newESign))) return false
			if (!(validate_col('session',formobj.confESign))) return false

			if( isNaN(formobj.newESign.value) == true){
				alert("<%=MC.M_Vldt_EsignForNume%>");/*alert("e-Signature must be numeric. Please enter again.");*****/
				formobj.newESign.focus();
				return false;
			}
			// modified by gopu to fix the issues #1917 for trim leading and trailing spaces
			if( fnTrimSpaces(formobj.newESign.value).length < 4){
				alert("<%=MC.M_NewEsign_Atleast4Chars%>");/*alert("The New e-Signature should be atleast 4 characters long. Please enter again.");*****/
				formobj.newESign.focus();
				return false;
			}
			// modified by gopu to fix the issues #1917 for trim leading and trailing spaces
			if( (fnTrimSpaces(formobj.newESign.value)) != (fnTrimSpaces(formobj.confESign.value))){
				alert("<%=MC.M_NewConfirm_EsignSame%>");/*alert("Values in 'New e-Signature' and 'Confirm e-Signature' are not same. Please enter again.");*****/
				formobj.newESign.focus();
				return false;
			}

			//JM: 17OCT2006: added
			formobj.daysExpESign.value = fnTrimSpaces(formobj.daysExpESign.value);
			/*Added For Bug#10355 Date:3 July 2012 By : Yogendra Pratap*/
			if(isNaN(parseInt(formobj.daysExpESign.value))){
				alert("<%=MC.M_InvalidExp_PlsEtrAgn%>");/*alert("Invalid expiration days. Please enter again.");*****/
				formobj.daysExpESign.focus();
				return false;
			}

			if (parseInt(formobj.daysExpESign.value) < 1 ){
				alert("<%=MC.M_EsignExp_CntLess1Day%>");/*alert("e-Signature Expires can not be less than 1 day");*****/
				formobj.daysExpESign.focus();
				return false;
			}

		}

		if ((formobj.checkESign.checked) && (formobj.checkPass.checked)){
			if( formobj.newESign.value==formobj.newPass.value){
				alert("<%=MC.M_EsignCnt_AsPwd%>");/*alert("e-signature cannot be same as password. Please enter again.");*****/
				formobj.newESign.focus();
				return false;
			}
		}

		
}

//Modified by : Yogendra Pratap : Bug 8199 - Http Status 404 error occurs in changepwd.jsp page 
function openTips(){
	window.open("pwdtips.htm","","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=870,height=600, top=40,left=100");
}
</SCRIPT>

<script>
$j(document).ready(function(){
	$j(".passwordCheck").passStrength({
		shortPass: 		"validation-fail",
		valid    :      "validation-pass",
		Invalid  :      "validation-fail",
		okPass:         "",
		messageloc:     1,
		comeFrom:		1
		
	});
})
</script>

</head>

<body>
		<% String src;
		src= request.getParameter("srcmenu");
		%>
		<%if (request.getParameter("selectedTab").equals("0")){%>
		<%}else{%>
			<jsp:include page="panel.jsp" flush="true">
			<jsp:param name="src" value="<%=src%>"/>
			</jsp:include>
		<%}%>

	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<%
	int pageRight = 5; //all users can change their password
	HttpSession tSession = request.getSession(true);
	String strUserOldPwd = null;
	String strUserOldESign = null;
	if (sessionmaint.isValidSession(tSession)){
		String uName = (String) tSession.getValue("userName");
		UserJB curUser = (UserJB) tSession.getValue("currentUser");
		strUserOldPwd = curUser.getUserPwd();
		strUserOldESign = curUser.getUserESign();
		String daysExpPass = curUser.getUserPwdExpiryDays();
		String daysExpESign = curUser.getUserESignExpiryDays();
		String loginName = curUser.getUserLoginName();
		pageRight = 5;
	    String tab = request.getParameter("selectedTab");
	    String accId=(String)tSession.getValue("accountId");
	    if (pageRight > 0 ){
		    String uSession  = (String) tSession.getValue("userSession");
	%>
</div>

<DIV class="tabDefTopN" id="div2">
	<%if (!(request.getParameter("selectedTab").equals("0"))){%>
			<jsp:include page="personalizetabs.jsp" flush="true"/>
		<%}%>


</DIV>

<%
// implemented this to fix the bug 8533 on 13/02/2012
	if(request.getParameter("srcmenu").equals("tdMenuBarItem2")){ %>
		<DIV class="tabDefBotN tabDefBotN_vhome" id="div3">
	<%}else{%>
		<DIV class="tabDefBotN" id="div3">
	<%}

%>

	<Form name = "pwd" id="pwdForm" method="post" action="updatepwd.jsp?from=changepwd" onSubmit="if (validate(document.pwd)==false){setValidateFlag('false');return false;} else {setValidateFlag('true'); return true;}">
    	<Input type="hidden" name="selectedTab" value="<%=tab%>">
        <input type="hidden" name="src" size = 15 value = "<%=src%>">
        <input type="hidden" name="loginName" id="loginName" size = 15 value = "<%=loginName%>">

	<table width="100%" cellspacing="0" cellpadding="0" border="0">
    	<tr>
	<td>
    	<P class = "defComments">
    	<% String Hyper_Link = "<A HREF=\"pwdtips.htm\" target=\"_new\">"+MC.M_SelcPwdCrt+"</A>"; Object[] arguments = {Hyper_Link}; %><%=VelosResourceBundle.getMessageString("M_CanChgPwd_TipsPwd",arguments) %>
          <%-- You can change your current Password or e-Signature. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<A HREF="pwdtips.htm" target="_new">Criteria for selecting a Password</A>*****--%></P>
	</td>
	</tr>
    </table>
	<table width="100%" cellspacing="0" cellpadding="0" border="0">
		<tr>
    		<td width="100%">
    			<input type="checkbox" name="checkPass"><%=MC.M_ChgPwdCntSame_OldPwd%><%-- Check if you want to change your password.*****--%>
    		</td>
    		<td>&nbsp;</td>
   		</tr>
   		<tr><td></td></tr>
	</table>
	<table width="100%">
        <tr>
    	    <td width="20%" align="right"> <%=LC.L_Cur_Pwd%><%-- Current Password*****--%> <FONT class="Mandatory">* </FONT> &nbsp;&nbsp;</td>
    		<td width="80%"><input type="password" name="oldPass" id="oldPass" size = 15 MAXLENGTH = 15></td>
    	</tr>
    	<tr>
    	    <td align="right"  width="20%"> <%=LC.L_New_Pwd%><%-- New Password*****--%> <FONT class="Mandatory">* </FONT> &nbsp;&nbsp;</td>
    	    <td width="80%"><input type="password" name="newPass" id="newPass" class="passwordCheck" size = 15 MAXLENGTH = 15></td>
				
		</tr>
		<tr>
	        <td align="right"  width="20%"> <%=LC.L_Confirm_NewPwd%><%-- Confirm New Password*****--%> <FONT class="Mandatory">* </FONT> &nbsp;&nbsp; </td>
		    <td  width="80%"><input type="password" name="confPass" id="confPass" size = 15 MAXLENGTH = 15></td>
    	</tr>

	</table>
	<table width="100%" cellspacing="0" cellpadding="0" border="0">
   		<tr>
    		<td width="20%" align="right"> <%=MC.M_PwdExpires_AfterDays%><%-- Number of days after which password expires*****--%> &nbsp;&nbsp;</td>
		<!-- Added for July-August'06 Enhancement (U2) - Default account level settings for Number of days password will expires .  If 'can be overridden' checkbox was selected, the user can change the setting, otherwise show 'Default Number of days after which password expires'  in ReadOnly mode. -->
		<%
			SettingsDao settingsDao=commonB.getSettingsInstance();
			String setvalueover="1";
			int modname=1;
			String keyword="ACC_DAYS_PWD_EXP_OVERRIDE";
			settingsDao.retrieveSettings(EJBUtil.stringToNum(accId),modname,keyword);
			ArrayList setvalueovers=settingsDao.getSettingValue();
			if(setvalueovers.size()>0)
				setvalueover=(setvalueovers.get(0)==null)?"":(setvalueovers.get(0)).toString();
			if(EJBUtil.stringToNum(setvalueover)==0){
		%>
			<td  width="80%">
    			<input type="input" name="daysExp" size=15 MAXLENGTH=3 readonly value=<%=daysExpPass%>>
    		</td>
		<%}	else {
		%>
			<td  width="80%">
				<input type="input" name="daysExp" size=15 MAXLENGTH=3  value=<%=daysExpPass%>>
    		</td>
		<%}%>
		</tr>
	<tr><td height="10"></td><td height="10"></td></tr>		
	</table>
    <table width="100%" cellspacing="0" cellpadding="0"  border="0">
        <tr>
    		<td width="40%">
				<input type="checkbox" name="checkESign"> <%=MC.M_ChkWntChg_Esign4Dgt%><%-- Check if you want to change your e-Signature.&nbsp;&nbsp;&nbsp;<font class="mandatory">(minimum 4 digits)</font>*****--%>
    		</td>    		
    	</tr>
 		<tr><td></td></tr>
	</table>
	<table width="100%">
    	<tr>
    	    <td width="20%" align="right"> <%=LC.L_Cur_Esign%><%-- Current e-Signature*****--%>  <FONT class="Mandatory">* </FONT> &nbsp;&nbsp;</td>
    	    <td width="80%">
    	       <input type="password" name="oldESign" size = 15 MAXLENGTH = 8>
		    </td>
    	</tr>
    	<tr>
    	    <td align="right"  width="20%"> <%=LC.L_New_Esign%><%-- New e-Signature*****--%> <FONT class="Mandatory">* </FONT>&nbsp;&nbsp; </td>
    		<td width="80%">
    			<input type="password" name="newESign" size = 15 MAXLENGTH = 8>
    		</td>
    	</tr>
    	<tr>
    	   <td align="right"  width="20%"> <%=LC.L_Confirm_NewEsign%><%-- Confirm New e-Signature*****--%> <FONT class="Mandatory">* </FONT> &nbsp;&nbsp;</td>
		   <td width="80%"><input type="password" name="confESign" size = 15 MAXLENGTH = 8></td>
	    </tr>
 
	</table>
	<table width="100%" cellspacing="0" cellpadding="0"  border="0">
    	<tr>
			<td width="20%" align="right"> <%=MC.M_DaysAfter_EsignExp%><%-- Number of days after which e-Signature expires*****--%> &nbsp;&nbsp; <!--For e-Signature never expires, enter 0.-->
    	    </td>
    	<%
		/* Added for July-August'06 Enhancement (U2) - Default account level settings for Number of days e-Sign will expire. .  If 'can be overridden' checkbox was selected, the user can change the setting, otherwise show 'Default Number of days after which e-Signature expires' in ReadOnly mode.*/
			setvalueover="0";
			modname=1;
			settingsDao.reset();//JM:
			keyword="ACC_DAYS_ESIGN_EXP_OVERRIDE";
			settingsDao.retrieveSettings(EJBUtil.stringToNum(accId),modname,keyword);
			setvalueovers=settingsDao.getSettingValue();
			if(setvalueovers.size()>0)
			  setvalueover=(setvalueovers.get(0)==null)?"":(setvalueovers.get(0)).toString();
			  if(EJBUtil.stringToNum(setvalueover)==0){
		%>
		<td>
			<input type="input" name="daysExpESign" size=15 MAXLENGTH=3 readonly value=<%=daysExpESign%>>
    	</td>
		<%}	else {
		%>
		<td>
    		<input type="input" name="daysExpESign" size=15 MAXLENGTH=3  value=<%=daysExpESign%>>
    	</td>
		<%}%>
		</tr>

	</table>
<br>
<br>
<% if (pageRight > 4 ) {%>

<table width="100%" cellspacing="0" cellpadding="0" class="esignStyle"><tr><td>
<!--JM: 19June2009: implementing common Submit Bar in all pages -->
	<jsp:include page="submitBar.jsp" flush="true">
	<jsp:param name="displayESign" value="Y"/>
	<jsp:param name="formID" value="pwdForm"/>
	<jsp:param name="showDiscard" value="N"/>
	</jsp:include>
</td></tr></table>
	<%}%>
		<table  >
        <tr>
		    <td align="right">
    	    <% if (pageRight > 4 ){
			%>
			<!--<input type="image" src="../images/jpg/Submit.gif" onClick = "return validate()" align="absmiddle" border="0">	-->
		    <% } %>
			</td>
		</tr>
	</table>
</DIV>
<input type="hidden" id="isSubmitFlag" value="" />
</Form>
    		<%
    		} //end of if body for page right
    		else{
			%>
    			<jsp:include page="accessdenied.jsp" flush="true"/>
    		<%
    		} //end of else body for page right
		}//end of if body for session
		else{
			if (request.getParameter("selectedTab").equals("0")){
			%>
				<jsp:include page="timeout_admin.html" flush="true"/>
			<%} else { //else of selectedTab check
			%>
				<jsp:include page="timeout.html" flush="true"/>
			<%	} // end of else for selectedTab check
			}%>
	<div class = "myHomebottomPanel">
    	<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</div>
		<%if (request.getParameter("selectedTab").equals("0")){%>
				<jsp:include page="velosmenus.htm" flush="true"/>
		<%}else{%>
				<jsp:include page="getmenu.jsp" flush="true"/>
		 <%}%>
		
</body>

</html>

