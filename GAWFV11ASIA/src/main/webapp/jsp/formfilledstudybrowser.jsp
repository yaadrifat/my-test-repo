<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%
boolean isIrb = "irb_form_tab".equals(request.getParameter("selectedTab")) ? true : false;
String IRBTRUE = request.getParameter("IRBTRUE")==null?"":request.getParameter("IRBTRUE");
if (isIrb) {
%>
<title><%=MC.M_ResCompApp_CompFrm%><%-- Research Compliance >> New Application >> Complete Forms*****--%></title>
<%
} else {
%>
<title><%=MC.M_StdFrm_RespBrow%><%-- <%=LC.Std_Study%> >> Form Response Browser*****--%></title>
<%
}
%>
<SCRIPT>
var screenWidth = screen.width;
var screenHeight = screen.height;

function reloadOpener() {
	if (top != null && top.reloadOpener != undefined) {
		top.reloadOpener();
	}
}

</SCRIPT>

<script>

//function openWin(formId,studyId) 
//{  
//	windowName= window.open("studyformdetails.jsp?formId="+formId+"&mode=N&studyId="+studyId+"&formDispLocation=S","information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=600,height=500");
	//windowName.focus();
//}
	
function checkPerm(pageRightForm) 
{	
	if (f_check_perm(pageRightForm,'N') == true) 
	{
		return true ;	
 	
	} 
	else 
	{
		return false;
	}
}	
	
	
	
</script>

<%
String irbReviewForm=request.getParameter("irbReviewForm")==null?"":request.getParameter("irbReviewForm");
String src= request.getParameter("srcmenu");
String selectedTab = request.getParameter("selectedTab");
String studyId =request.getParameter("studyId");
String clickedBy=request.getParameter("clickedBy");
	String studyIdForTabs = ""; 
 	studyIdForTabs = request.getParameter("studyId");
 	

String from="form";

String calledFromForm = "";
       calledFromForm = request.getParameter("calledFromForm");	 
     
      if (StringUtil.isEmpty(calledFromForm))
      {
     	calledFromForm = "";
      }
      
String showPanel= "";
       showPanel= request.getParameter("showPanel");	 
     
      if (StringUtil.isEmpty(showPanel))
      {
     	showPanel= "true";
      }

      
      
      String outputTarget = ""; //if a target name is passed, open the new and edit page in this target
	   outputTarget = request.getParameter("outputTarget");
	   
	 if (StringUtil.isEmpty(outputTarget))
	 {
	 	outputTarget = "";
	 }  
	 
	 String specimenPk = "";
    specimenPk = request.getParameter("specimenPk");
    
     if (StringUtil.isEmpty(specimenPk))
	 {
	 	specimenPk = "";
	 }  
      
   
   if (calledFromForm.equals("") && showPanel.equals("true") )   
   { 
   	   
%>
	<jsp:include page="panel.jsp" flush="true"> 
	<jsp:param name="src" value="<%=src%>"/>
	</jsp:include>   
	
 <% }
 else
 { %>
 	<jsp:include page="popupJS.js" flush="true"/>
	<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
	<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
	<SCRIPT LANGUAGE="JavaScript" SRC="formjs.js"></SCRIPT>	
 	<%	
 }
 %>

<body>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="formLibB" scope="request" class="com.velos.eres.web.formLib.FormLibJB"/>
<jsp:useBean id="lnkformB" scope="request" class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
<%@ page language = "java" import = "com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.*,com.velos.eres.business.common.CtrlDao,java.util.*,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration,com.velos.eres.business.common.*,com.velos.eres.web.user.UserJB"%>


<%
if (calledFromForm.equals("") && showPanel.equals("true"))   
   {
%>
<div class="BrowserTopn" id="divtab">
<% String includeTabsJsp = isIrb ? ("LIND".equals(CFG.EIRB_MODE) ? "ecompNewTabs.jsp" : "irbnewtabs.jsp") : "studytabs.jsp"; %>
<jsp:include page="<%=includeTabsJsp%>" flush="true"> 
<jsp:param name="from" value="<%=from%>"/> 
<jsp:param name="selectedTab" value="<%=selectedTab%>"/>
<jsp:param name="studyId" value="<%=studyIdForTabs%>"/>
</jsp:include>   
</div>
<%
String stid= request.getParameter("studyId");
if(stid==null || stid=="" || "0".equals(stid)){%>
<SCRIPT LANGUAGE="JavaScript">
	if(screenWidth>1280 || screenHeight>1024)
		document.write('<DIV class="BrowserBotN BrowserBotN_RC_1" id="div1"  style="height:77%;">')
	else
		document.write('<DIV class="BrowserBotN BrowserBotN_RC_1" id="div1">')
</SCRIPT>
	<%} else {%>
<SCRIPT LANGUAGE="JavaScript">
/* Condition to check configuration for workflow enabled */

<% if("Y".equals(CFG.Workflows_Enabled) && (stid!=null && !stid.equals("") && !stid.equals("0"))){ %>
		if(screenWidth>1280 || screenHeight>1024)
			document.write('<DIV class="BrowserBotN BrowserBotN_S_3 workflowDivBig" id="div1" style="height:77%;" >')
		else
			document.write('<DIV class="BrowserBotN BrowserBotN_S_3 workflowDivSmall" id="div1">')



<% } else{ %>


		if(screenWidth>1280 || screenHeight>1024)
			document.write('<DIV class="BrowserBotN BrowserBotN_S_3" id="div1" style="height:75%;" >')
		else
			document.write('<DIV class="BrowserBotN BrowserBotN_S_3" id="div1">')
<% } %>
</SCRIPT>
<%}

}	
%>	


<%
HttpSession tSession = request.getSession(true); 
boolean removeIRBParam= false;
if (request.getParameter("irbReviewForm") == null 
        || "".equals(request.getParameter("irbReviewForm"))) {
    if ( tSession.getAttribute("IRBParams")!=null	 ) {
   		tSession.removeAttribute("IRBParams");
   	}
}
if (sessionmaint.isValidSession(tSession)) {
		 GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
		 String formName="";
		 String study = null;		
		 
		 if (calledFromForm.equals("") && showPanel.equals("true"))   
   			{
		 
		 		study = (String) tSession.getValue("studyId");
		 	}
		 	else
		 	{
		 		study = studyId; 
		 	
		 	}	

          String hiddenflag = request.getParameter("hiddenflag")==null?"":request.getParameter("hiddenflag");
		  String hide = request.getParameter("hide")==null?"":request.getParameter("hide");
		 	
		 int study_acc_form_right = 0;
		 int study_team_form_access_right =0;
		 int pageRight = 0;

  if(study == "" || study == null|| "0".equals(study)||study.equalsIgnoreCase("")) {
%>
  <jsp:include page="studyDoesNotExist.jsp" flush="true"/>
  <%
   } else {
   			
   		if (StringUtil.isEmpty(outputTarget) )
		{   
   			// change by salil pasted here 	
   			StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");	  
			
			if (stdRights != null )
			{
				if ((stdRights.getFtrRights().size()) == 0 ){
					if(hiddenflag.equalsIgnoreCase("N")){
						study_acc_form_right= 7;
						study_team_form_access_right=7;
				 	 }
					else{
						study_team_form_access_right= 0;
					}
				}else{
					 study_acc_form_right = Integer.parseInt(grpRights.getFtrRightsByValue("STUDYFRMSACC"));
	    			 study_team_form_access_right=Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMACC"));		
				}
			}	
		}
		else if(outputTarget.equals("specFormIds")) //by pass rights
		{
			study_acc_form_right = Integer.parseInt(grpRights.getFtrRightsByValue("STUDYFRMSACC"));
			study_team_form_access_right = 7;
		}
	
	  	 ArrayList arrFrmNames = null;
		 ArrayList arrNumEntries = null;
 		 ArrayList arrFrmIds = null;
		 ArrayList arrEntryChar = null;
		 ArrayList arrFrmInfo = new ArrayList();
		 String frmName = "";
		 String entryChar="";
		 String numEntries = "";
		 String frmInfo= "";
		 int formId = 0;
		 String firstFormInfo = "";
 String selTypeperm = request.getParameter("selTypeperm")==null?"":request.getParameter("selTypeperm");
		 String link = request.getParameter("link")==null?"":request.getParameter("link");
		 String strFormId = null;
		 String strFormId1 = null;
		 if(selTypeperm.equalsIgnoreCase("Y")){
		 strFormId1 = (String)tSession.getAttribute("strFormId");
		 }
		   
		if(selTypeperm.equalsIgnoreCase("Y")){
		 strFormId =request.getParameter("selType");
		 if(strFormId==null){
			 strFormId = strFormId1;
			
		}
			 
		}else{
			
			strFormId = request.getParameter("formPullDown");
			
		}
		
		String delimiter="\\*";
		String[] temp ;
		int strformidlength = 0;
		
		if(strFormId!=null){
			temp = strFormId.split(delimiter);
		 strformidlength = temp.length;
		System.out.println("length is "+strformidlength);}
		
		 String accId=(String)tSession.getAttribute("accountId");
		 int iaccId=EJBUtil.stringToNum(accId);
 	     String userId = (String) tSession.getValue("userId");
	     UserJB userB = (UserJB) tSession.getValue("currentUser");
   	     String siteId = userB.getUserSiteId();
	 	 int istudyId = EJBUtil.stringToNum(studyId);
		
		 String formCategory = request.getParameter("formCategory");	 
		 String submissionType = request.getParameter("submissionType");	 
		 if (submissionType != null && isIrb) {
		     
		     tSession.setAttribute("submissionType", submissionType);
		     	 
		 } else {
		 	 if (isIrb) { // get the session value only for IRB module
		     	submissionType = (String)tSession.getAttribute("submissionType");
		     }
		 }
		 
		 
		 
		 if (isIrb)
		 {
		 	if (submissionType == null) {
		 	    submissionType = "new_app";
		 	}
		 	formCategory = "irb_sub";
		 }
		 else
		 {
			if (StringUtil.isEmpty(formCategory))
			 {
			 	 formCategory="";
			 }
			 
			 if (StringUtil.isEmpty(submissionType))
			 {
			 	 submissionType="";
			 }
			 
			 if (! StringUtil.isEmpty(formCategory) && ! StringUtil.isEmpty(submissionType))
			 {
			 	 isIrb=true;
			 }
			 
		 }
		  
		 
		 
		 LinkedFormsDao lnkFrmDao = new LinkedFormsDao();
		 lnkFrmDao = lnkformB.getStudyForms(iaccId,istudyId, EJBUtil.stringToNum(userId), EJBUtil.stringToNum(siteId), 
		         study_acc_form_right,  study_team_form_access_right, 
		         isIrb, submissionType, formCategory);
	 
		 arrFrmIds = lnkFrmDao.getFormId();		 
		 arrFrmNames = lnkFrmDao.getFormName();
		 arrEntryChar = lnkFrmDao.getFormEntryChar();
		 arrNumEntries = lnkFrmDao.getFormSavedEntriesNum();
	
		 if (arrFrmIds.size() > 0) { 	
    		 if (strFormId==null) {
    		 	formId = EJBUtil.stringToNum(arrFrmIds.get(0).toString());
    			entryChar = arrEntryChar.get(0).toString();
    			numEntries = arrNumEntries.get(0).toString();		   
    			firstFormInfo = arrFrmIds.get(0).toString() + "*"+ entryChar + "*" + arrNumEntries.get(0).toString();
    		 }
    		 else {
    		 	 StringTokenizer strTokenizer = new StringTokenizer(strFormId,"*");
    		 	 if (calledFromForm.equals(""))
    	 		 {
	    	 		 
	    	 	     formId = EJBUtil.stringToNum(strTokenizer.nextToken());
	    			 entryChar = strTokenizer.nextToken();

if(selTypeperm.equalsIgnoreCase("") && strformidlength > 2 ){
	    				 
	    		     numEntries = strTokenizer.nextToken();
	    				 
	    			 }
	    			 firstFormInfo = strFormId;	   
	    		}
	    		else
    			 {
    			 	formId = EJBUtil.stringToNum(strTokenizer.nextToken());
    			 	entryChar = strTokenizer.nextToken();
    			 	
    			 	lnkformB.findByFormId(formId );
		   		 	numEntries = lnkformB.getLfDataCnt();
    			 	//get the number of times the form was answered
    			 	//prepare strFormId again
    			 	strFormId = formId + "*" + entryChar + "*" + numEntries;
    			 	firstFormInfo = strFormId;	   
    			 
    			 }
    		 }
    	
    		//check for access rights if form is called from a 'link form' link	
      if (!calledFromForm.equals(""))
    	{
    		
    		if (!arrFrmIds.contains(new Integer(formId)))
    		{
	    		// check if the form is hidden and user has access to it
				if (! lnkformB.hasFormAccess(formId, EJBUtil.stringToNum(userId)))    			
				{
	    			//user does not have access rights to this form
	    			study_acc_form_right = 0;
	    			study_team_form_access_right = 0;
    			}
    		}
    		
    	}		
		 
	 if (study_acc_form_right>=4 || study_team_form_access_right>0) 
		{
	     	 
    		 for (int i=0;i<arrFrmIds.size();i++)
    		 {  //store the formId, entryChar and num Entries separated with a *
    		  	 frmInfo = arrFrmIds.get(i).toString() + "*"+ arrEntryChar.get(i).toString() + "*" + arrNumEntries.get(i).toString();
    		 	 arrFrmInfo.add(frmInfo);		 
    		 }
    		 		
    				 
    	 	 String dformPullDown = EJBUtil.createPullDownWithStrNoSelect("formPullDown",firstFormInfo, arrFrmInfo , arrFrmNames);
    		 StringBuffer formBuffer = new StringBuffer(dformPullDown);

			 //submit the form when the user selects the form name
			 if (calledFromForm.equals(""))	 
			 {
			 	formBuffer.replace(0,7,"<SELECT onChange=\"document.studyform.submit();\"");
			 }
			 else
			 {
			 //	formBuffer.replace(0,7,"<SELECT DISABLED onChange=\"document.studyform.submit();\"");
			 }	
			 
			 dformPullDown = formBuffer.toString();
			 
    		 //date filter
    	  	 String formFillDt = "";
    	 	 formFillDt = request.getParameter("formFillDt") ;
     	 	 if (formFillDt == null) formFillDt = "ALL";	 	 
    		 String dDateFilter = EJBUtil.getRelativeTimeDD("formFillDt",formFillDt);
    				
    	   String linkFrom = "S";
    	   String hrefName = "formfilledstudybrowser.jsp?srcmenu="+src+"&hiddenflag="+hiddenflag;
    	   String modifyPageName = "studyformdetails.jsp?srcmenu="+src+"&hiddenflag="+hiddenflag+"&selTypeperm="+selTypeperm;
		   String formStatus = "";
	       String statSubType = "";
		   		   
   	       if (formId >0) {
	     	   formLibB.setFormLibId(formId);
   	   		   formLibB.getFormLibDetails();
			   formName=formLibB.getFormLibName();
		   
		   	   formStatus = formLibB.getFormLibStatus();  
			   CodeDao cdao = new CodeDao();
			   cdao.getCodeValuesById(EJBUtil.stringToNum(formStatus));
			   ArrayList arrSybType = cdao.getCSubType();
			   statSubType = arrSybType.get(0).toString();
		    }		   
		   		   
		   lnkformB.findByFormId(formId);
		   String formLinkedFrom = lnkformB.getLnkFrom(); 
		   String formDispType = lnkformB.getLFDisplayType();
		   
		   if (formLinkedFrom==null) formLinkedFrom="-";
		   if (formDispType==null) formDispType="-";	
		   formDispType = formDispType.trim();
		   	   
		   if (formLinkedFrom.equals("S")) 
		   {
		   	  pageRight = study_team_form_access_right;
		   } else if (formLinkedFrom.equals("A")) 
		   {
		   	 if (formDispType.equals("S")) { //specific study form 
			 	pageRight = study_team_form_access_right;
			 } else if (formDispType.equals("SA")) { //all study forms
			    pageRight = study_acc_form_right;
			 }
		   }
// Added by Gopu to fix the bugzilla issue # 2808    
			formLibB.setFormLibId(formId);
			String formLibVer = formLibB.getFormLibVersionNumber(formId);

    	%>
    	<Form method="post" name="studyform" action="formfilledstudybrowser.jsp" onsubmit="">
    		<input type="hidden" name="srcmenu" value=<%=src%>>
             <input type="hidden" name="hide" value=<%=hide%>>
    		<input type="hidden" name="selectedTab" value=<%=selectedTab%>>
    		<input type="hidden" name="studyId" value=<%=studyId%>>	
    		<input type="hidden" name="clickedBy" value=<%=clickedBy%>>				
    		<input type="hidden" name="calledFromForm" value=<%=calledFromForm%>>
            <input type="hidden" name="link" value=<%=link%>>
    		<input type="hidden" name="link1" value="N">
		<input type="hidden" name="selTypeperm" value="<%=selTypeperm%>">
          <input type="hidden" name="hiddenflag" value=<%=hiddenflag%>>
    		<input type="hidden" name="hide" value=<%=hide%>>

    		
    		<input type="hidden" name="showPanel" value=<%=showPanel%>>
    			
    		<input type="hidden" name="formCategory" value=<%=formCategory%>>
    		<input type="hidden" name="submissionType" value=<%=submissionType%>>


    		
    		<input type="hidden" name="previousPage" value="study"/>
    		
    	<%  if ("true".equals(irbReviewForm)) { %>
    		<input type="hidden" name="irbReviewForm" value="<%=irbReviewForm%>">
    	<%  }  %>
    		
    		 
    		<table width="98%" border=0>
    		
    		<tr >
    		<td width="30%" align="right">
    		<%
    		if (calledFromForm.equals("") && showPanel.equals("true"))	 
			 {
			 	 %>
    			<%=LC.L_Frm_Name%><%-- Form Name*****--%>:
			<% }   %>    				 
    		
    		</td>
    		<% if (calledFromForm.equals("") && showPanel.equals("true"))	 
			 {
			%>
    		<td width="50%" >  
    			  
    		 <%=dformPullDown%>
    		
    		<% } 
    			//System.out.println("statSubType" +statSubType);
    			//System.out.println("entryChar" +entryChar);
    			//System.out.println("numEntries" +numEntries);
    			//Added For Bug# 9255 :Date 14 May 2012 :By YPS
    		    if(!specimenPk.equals("")){
    		    	if(formDispType.equalsIgnoreCase("SA") && entryChar.equals("E")) {
    		    		int indexOfFormId =  arrFrmIds.indexOf(formId);
    		    		if(indexOfFormId!=-1){
    		    			numEntries = arrNumEntries.get(indexOfFormId).toString();
    		    		}
    		    	 }
    		     }
    			%>
    		
    		<%//Only Active forms can be answered 
			  if (statSubType.equals("A") && (entryChar.equals("M")) || ((entryChar.equals("E")) && (numEntries.equals("0")))   ) 
			  {
			  	String jsCheck="";
 			  	
 			  	String hrefurl ="";
 			  	
 			  	hrefurl= "studyformdetails.jsp?srcmenu="+src+"&selectedTab="+ selectedTab+"&formId="+formId+"&mode=N&studyId="+studyId+
 			  	"&formDispLocation=S&entryChar="+entryChar+"&formFillDt="+formFillDt+"&formPullDown="+firstFormInfo+"&calledFromForm="+
 			  	calledFromForm + "&specimenPk=" + specimenPk+"&showPanel="+showPanel+"&submissionType="+submissionType+"&formCategory="+formCategory+"&clickedBy="+clickedBy+"&hiddenflag="+hiddenflag+"&selTypeperm="+selTypeperm; ;
 			  	

 			 	 if ("true".equals(irbReviewForm)) { 
 			 		hrefurl= hrefurl +  "&irbReviewForm=" + irbReviewForm;
 	    		
 	    	  }  
 			  	if (!StringUtil.isEmpty(outputTarget))
			  	{
			  	 
			  		hrefurl= hrefurl +  "&outputTarget=" + outputTarget;
			  		
			  		jsCheck = " onClick='openWindowCommon(\""+hrefurl+"\",\""+ outputTarget +"\");' ";
			  		hrefurl = "#";
			  	}
			  	else
			  	{
			  		jsCheck = " onClick =\" return checkPerm(" +pageRight+ " )\"";
			  	}
 			  	
			  

						if(hide.equalsIgnoreCase("")){   %>

			  	<A type="submit" <%=jsCheck%> HREF="<%=hrefurl%>"><%=LC.L_New%></A>
    		<%}}%>

    		</td>
    					
    		</tr>
    		<tr><td>&nbsp;</td></tr>
    		</table>
    			
    		<table align="center">
    		<tr>		
    		<td>
    		<P class="blackComments"><%=MC.M_Prev_EntriesForFrm%><%-- Previous entries for Form*****--%>: <B>"<%=formName%>"</B></P>
    		</td>
    		<% if (calledFromForm.equals(""))	 
			{ %>
			
	    		<td colspan="2">		
	    		</td>	
	    		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Filter_ByDate%><%-- Filter By Date*****--%> &nbsp;  <%=dDateFilter%> </td>
	    		<td><button type="submit"><%=LC.L_Search%></button></td>		
    		<%
    		 }
   			%>	
    		</tr>
    		</table>
    			 
    	    </Form>	
<% 
  if ("irb_ongoing_menu".equals(request.getParameter("tabsubtype"))) {
      String formValue = StringUtil.htmlEncodeXss(request.getParameter("formValue"));
%>
<script>
var menus = document.getElementsByName("formPullDown");
var opts = menus[0].options;
for(var i=0; i<opts.length; i++) {
	if (opts[i].value == '<%=formValue%>') {
		opts[i].selected = true;
		document.studyform.submit();
	}
}
</script>
<% }  %>

    	<!--include the jsp for showing the records-->
    	 <jsp:include page="formdatarecords.jsp" flush="true">
     	 <jsp:param name="formId" value="<%=formId%>"/>	 
          <jsp:param name="hide" value="<%=hide%>"/>
    	 <jsp:param name="linkFrom" value="<%=linkFrom%>"/>
         <jsp:param name="link" value="<%=link%>"/>
    	 <jsp:param name="hiddenflag" value="<%=hiddenflag%>"/>

    	 <jsp:param name="hrefName" value="<%=hrefName%>"/>
    	 <jsp:param name="modifyPageName" value="<%=modifyPageName%>"/>
          <jsp:param name="selTypeperm" value="<%=selTypeperm%>"/>
    	 <jsp:param name="selectedTab" value="<%=selectedTab%>"/>	
    	 <jsp:param name="entryChar" value="<%=entryChar%>"/> 	
    	 <jsp:param name="studyId" value="<%=studyId%>"/> 	  
    	 <jsp:param name="formFillDt" value="<%=formFillDt%>"/>
    	 <jsp:param name="formPullDown" value="<%=firstFormInfo%>"/>	 
    	 <jsp:param name="srcmenu" value="<%=src%>"/>
    	 <jsp:param name="pageRight" value="<%=pageRight%>"/>
    	 <jsp:param name="statSubType" value="<%=statSubType%>"/>	
    	 <jsp:param name="calledFromForm" value="<%=calledFromForm%>"/>	
	 	<jsp:param name="formLibVer" value="<%=formLibVer%>"/>
	 	<jsp:param name="outputTarget" value="<%=outputTarget%>"/>
	  	<jsp:param name="specimenPk" value="<%=specimenPk%>"/>
	  	<jsp:param name="showPanel" value="<%=showPanel%>"/>
	  	<jsp:param name="formCategory" value="<%=formCategory%>"/>
	  	<jsp:param name="submissionType" value="<%=submissionType%>"/>
	  	<jsp:param name="previousPage" value="study"/>
	  	<jsp:param name="formDispType" value="<%=formDispType%>"/>


	   	<jsp:param name="irbReviewForm" value="<%=irbReviewForm %>"/>
	  	
	  	 	 				 		 	  		 	 			 		 		 	 	 	 	 
    	 </jsp:include>
     <%
	   } //ids.size
	   else{
	   	   removeIRBParam= true;
		%>
  			<jsp:include page="accessdenied.jsp" flush="true"/>
		<%
		} //end of else body for page right		

 if(selTypeperm.equalsIgnoreCase("Y")){
	 tSession.setAttribute("strFormId",strFormId);
		 }

	 } //end of if body for page right
	else 
	   {
	     removeIRBParam= true;
	%>
	  <P class="defComments"><%=MC.M_NoAssocForms%><%-- No associated Forms.*****--%></P>	   
    <%  }	
	}//end of if body for check on studyId
	

	
		//remove session variable for IRBParam
	   	if (removeIRBParam)
	   	{
	   		if 	 ( tSession.getAttribute("IRBParams")!=null	 )
			   	{
			   		// tSession.removeAttribute("IRBParams"); // <- For Bug 4562
			   	}
	   	}
		//////////////////////////
}//end of if body for session
else {
%>
	<jsp:include page="timeout.html" flush="true"/>
<%}
%>
<br>

<%if (calledFromForm.equals("") && showPanel.equals("true"))   
   {
%>	

<div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</div>

<DIV class="mainMenu" id = "emenu"> 
  <jsp:include page="getmenu.jsp" flush="true"/>
</DIV>
	<%
}
	%>
</body>
</html>