dashboard = {
    renderPage: {},
    renderDBTypeDD: {},
}

dashboard.renderPage = function () {
    dashboard.renderDBTypeDD();
   
}

dashboard.renderDBTypeDD = function () {
    dashboardUtils
        .fetchCodeLstItems(
            'newdashboard',
            function (jsonData) {
            	 var source   = $("#dashBoardTypesDIV").html();
            	 var template = Handlebars.compile(source);
            	 var html    = template(LC);
            	    $("#dashBoardTypesDIV").html(html);
                var html = '<select id="dashboardTypeDD"><options><option>Select an option</option>';
                $.each(jsonData, function () {
                	if(this.CODELST_SUBTYP=="dbformquery"){
                		html += '<option data-codelstSubType="' + this.CODELST_SUBTYP + '" value="' + this.PK_CODELST + '" selected>' + this.CODELST_DESC + '</option>'
                	}
                	else{
                    html += '<option data-codelstSubType="' + this.CODELST_SUBTYP + '" value="' + this.PK_CODELST + '">' + this.CODELST_DESC + '</option>'
                	}
                });
                html += '</options><select>';
                $('#dashBoardTypesDIV').append(html);
            });

    $('#dashboardTypeDD')
        .change(
            function () {
                var codelstSubType = $(this).find('option:selected')
                    .attr("data-codelstSubType");
                if (codelstSubType == 'dbformquery') {
                    queryManagement.renderPage();
                } else if (codelstSubType == 'dbstudyreview') {
                	compliance.renderPage();
                } else if (codelstSubType == 'dbothers') {

                } else {

                }

            });
}
/*
 * $j(document).bind("ajaxSend", function () { $j("#loading").show();
 * }).bind("ajaxComplete", function () { $j("#loading").hide(); });
 */