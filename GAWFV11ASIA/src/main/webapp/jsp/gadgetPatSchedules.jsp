<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@page import="java.util.ArrayList,java.util.HashMap,org.json.*"%>
<%@page import="com.velos.eres.widget.business.common.UIGadgetDao, com.velos.eres.service.util.StringUtil,
	com.velos.eres.service.util.LC" %>
<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%
HttpSession tSession = request.getSession(false); 
if (!sessionmaint.isValidSession(tSession)) {
	return;
}
String studyIdsStr = null, visitFilter = null;
int numStudies = 0;

%>
<div onmouseout='return nd();'>
	<table width="100%">
	<tr>
		<td>
		<div id="gadgetPatSchedules_settings" style="border:#aaaaaa 1px solid; padding: 2px; display:none">
		    <form id="gadgetPatSchedules_formSettings" name="gadgetPatSchedules_formSettings" onsubmit="return false;" >
		    <p><b>Settings for Gadget Patient Schedule</b></p>
		    <table style="width:99%;">
		        <!-- <tr style="display:none">
		            <td>
		            	<label for="gadgetPatSchedules_studies">Studies</label><FONT class="Mandatory"> *</FONT><BR/>
		                <div id="gadgetPatSchedules_errMsg_studyIds"></div>
                    </td>
		            <td>
		                <s:if test="%{gadgetPatSchedulesJB.settingsStudyCount < 5}">
		                <input id="gadgetPatSchedules_study" name="gadgetPatSchedules_study" type="hidden" size="20" />
		                <input id="gadgetPatSchedules_studyId" name="gadgetPatSchedules_studyId" type="hidden" size="20" />
		                <button id="gadgetPatSchedules_addStudyButton" name="gadgetPatSchedules_addStudyButton" type="button">Add</button>
						</s:if>
		            </td>
		        </tr>
		        <tr>
		            <td colspan="2">
		            	<div id="gadgetPatSchedules_myStudiesHTML" style="display:none"></div>
		            	<s:hidden id="gadgetPatSchedulesJB.settingsStudyIds" name="gadgetPatSchedulesJB.settingsStudyIds"/>
		            </td>
		        </tr>-->
		        <tr>
		            <td>
		            	<label for="gadgetPatSchedules_visitFilter"><%=LC.L_Default_filter%></label><FONT class="Mandatory"> *</FONT><BR/>
		            	<div id="gadgetPatSchedules_errMsg_visitFilter"></div>
		            </td>
		            <td>
		            	<s:property escape="false" value="gadgetPatSchedulesJB.settingsVisitFilterMenu" />
		            </td>
		            <td align="left">&nbsp;</td>
		        </tr>
		        <tr>
		            <td>
		            	<label for="gadgetPatSchedules_settingsSortBy"><%=LC.L_Sort_By%></label><FONT class="Mandatory"> *</FONT><BR/>
		            	<div id="gadgetPatSchedules_errMsg_settingsSortBy"></div>
		            </td>
		            <td>
		            	<s:radio name="gadgetPatSchedulesJB.settingsSortBy" id="gadgetPatSchedulesJB.settingsSortBy" 
		            		list="gadgetPatSchedulesJB.settingsSortByList" listKey="radioCode" listValue="radioDisplay" 
		            		value="gadgetPatSchedulesJB.settingsSortBy" />
		            </td>
		            <td>
           				<s:property escape="false" value="gadgetPatSchedulesJB.settingsSortDirMenu" />
           			</td>
		            <td>
		            	<button type="submit" id="gadgetPatSchedules_saveButton"
		                    class="gadgetPatSchedules_saveButton">Save</button>&nbsp;&nbsp;&nbsp;&nbsp;
		            	<button type="button" id="gadgetPatSchedules_cancelButton"
		                    class="gadgetPatSchedules_cancelButton">Cancel</button>
		            </td>
		        </tr>
		    </table>
		    </form>
		</div>
		</td>
	</tr>
	<tr>
		<td>
			<form id="gadgetPatSchedules_visitFilter" name="gadgetPatSchedules_visitFilter" method="post" >
		    	<table class="basetbl midAlign" width="100%">
		    		<tr style="background:none">
						<td align="center">
			    			<s:property escape="false" value="gadgetPatSchedulesJB.visitFilterMenu" />
			    		</td>
			    	</tr>
		    		<tr style="background:none"></tr>
			    	<tr style="background:none">
						<td colspan="2" style="padding-left:2px;">
		        			<div id="gadgetPatSchedules_myPatSchedulesGrid" name="gadgetPatSchedules_myPatSchedulesGrid"></div>
			        	</td>
			    	</tr>
			    	<tr id="gadgetPatSchedules_moreLinkTr" style="display:none; background:none">
		     			<td align="right">
		    				<div id="gadgetPatSchedules_morePatSchHTML"></div>&nbsp;&nbsp;
		     			</td>
		     			<td align="left">&nbsp;&nbsp;</td>
 					</tr>
				</table>
			</form>
		</td>
	</tr>
	<tr></tr>
	</table>
</div>

