<html>
<script language="JavaScript" src="budget.js"></script>
<head>

<style>
.yui-skin-sam .yui-dt th.yui-dt-hidden,
.yui-skin-sam .yui-dt tr.yui-dt-odd .yui-dt-hidden,
.yui-skin-sam .yui-dt tr.yui-dt-even .yui-dt-hidden {
border:0px;
}
</style>

<%@page import="com.velos.eres.web.objectSettings.ObjectSettingsCache,com.velos.eres.web.objectSettings.ObjectSettings"%>
<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%
String mileFrom = request.getParameter("mileFrom");
if(StringUtil.isEmpty(mileFrom)){mileFrom="";}
String src="";
if(mileFrom.equalsIgnoreCase("studyMilestone")){
ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
ArrayList tabList = null;
String titleStr = LC.L_Study/*LC.Std_Study*****/;
if (sessionmaint.isValidSession(request.getSession(true)))
{
    String accId = (String) request.getSession(true).getValue("accountId");
    
    ArrayList tempList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "study_tab");
    for (int iX=0; iX<tempList.size(); iX++) {
        ObjectSettings settings = (ObjectSettings) tempList.get(iX);
        if ("12".equals(settings.getObjSubType())) {
            if (settings.getObjDispTxt() != null) {
            	Object[] arguments = {titleStr,settings.getObjDispTxt()};
            	titleStr = VelosResourceBundle.getLabelString("L_Gtgt",arguments);/*titleStr +=  " >> " + settings.getObjDispTxt();*****/
            }
            break;
        } 
    }
    tempList = null;
}
src= StringUtil.htmlEncodeXss(request.getParameter("srcmenu"));
%>
<title><%=titleStr %></title>
<%} %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%if(mileFrom.equalsIgnoreCase("studyMilestone")){ %>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<%} %>
<jsp:include page="ui-include.jsp" flush="true"/>
<%
//DFIN-13 BK MAR-16-2011
String mode =   request.getParameter("mode");

boolean isIrb = "irb_personnel_tab".equals(request.getParameter("selectedTab")) ? true : false; 

%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.studyRights.StudyRightsJB" %>
<jsp:useBean id="budgetB" scope="request"	class="com.velos.esch.web.budget.BudgetJB" />
<jsp:useBean id="userB" scope="session" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id="budgetUsersB" scope="request"	class="com.velos.esch.web.budgetUsers.BudgetUsersJB" />
<jsp:useBean id="studyB" scope="session" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id="stdRightsB" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="grpB" scope="request"	class="com.velos.eres.web.group.GroupJB" />
<jsp:useBean id="ctrl" scope="request"	class="com.velos.eres.business.common.CtrlDao" />
<jsp:useBean id="grpRights" scope="request"	class="com.velos.eres.web.grpRights.GrpRightsJB" />

<%@ page language = "java" import = "com.velos.eres.business.common.*,com.velos.esch.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil, com.velos.eres.service.util.StringUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.*"%>
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%
	String from = "combBudget";

	String pageMode = request.getParameter("pageMode");
	if(pageMode == null) pageMode = "initial";

	String studyIdForTabs = "";
 	studyIdForTabs = request.getParameter("studyId");
 	
%>

<body class="yui-skin-sam yui-dt yui-dt-liner" style="overflow-y:hidden">
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
<%
   HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession)){
	String studyId = request.getParameter("studyId");
	tSession.putValue("studyId", studyId);
	if(studyId== null) studyId = "";	
	String styleTop="";
	String includeTabsJsp = isIrb ? "irbnewtabs.jsp" : "studytabs.jsp";
	//Start Code committed for the bug id 23673
	String workFlag = (request.getParameter("workFlag")==null) ? "Y" : (request.getParameter("workFlag"));
	//End Code
	if(mileFrom.equalsIgnoreCase("manageMilestone")){
		styleTop="style=top:10px;";
	}
	if(mileFrom.equalsIgnoreCase("studyMilestone")){
	%>
	
	<DIV class="BrowserTopn" id="divTop">    
	  <jsp:include page="<%=includeTabsJsp%>" flush="true">
	  <jsp:param name="from" value="<%=from%>"/>
	  	<jsp:param name="studyId" value="<%=studyIdForTabs%>"/>
	   </jsp:include>
	</DIV>
	<%
	}
	if(studyId == "" || studyId == null||"0".equals(studyId)||studyId.equalsIgnoreCase("")) {
		%> <DIV class="BrowserBotn BrowserBotN_S_1" id="divBot">   
				<jsp:include page="studyDoesNotExist.jsp" flush="true" /> 
			</DIV>
		<%
	} else {
		boolean withSelect = true;
	StringBuffer urlParamSB = new StringBuffer();
	urlParamSB.append("studyId=").append(StringUtil.htmlEncodeXss(studyIdForTabs));
		
	SchCodeDao schD = new SchCodeDao();
	int perCodeId = schD.getCodeId("category","ctgry_per");

	String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = null;
	usr = (String) tSession.getValue("userId");
	
	String userIdFromSession = (String) tSession.getValue("userId");
	String acc = (String) tSession.getValue("accountId");
	
	int accId = EJBUtil.stringToNum(acc);
	int userId = EJBUtil.stringToNum(userIdFromSession);
	
	userB.setUserId(userId);
	userB.getUserDetails();

	String includedIn ="S";
	
	String selectedTab = request.getParameter("selectedTab");
	String selectedBgtcalId = "";
	
	int pageRight= 0;
	
	ArrayList teamId = new ArrayList();
	TeamDao teamDao = new TeamDao();
	teamDao.getTeamRights(EJBUtil.stringToNum(studyIdForTabs),EJBUtil.stringToNum(userIdFromSession));
	teamId = teamDao.getTeamIds();

	if (teamId.size() <=0)
	{
		pageRight  = 0;
		StudyRightsJB stdRightstemp = new StudyRightsJB();
	}
	if (teamId.size() == 0) {
		pageRight=0 ;
	}else {
		stdRightsB.setId(EJBUtil.stringToNum(teamId.get(0).toString()));
	 	ArrayList teamRights ;
		teamRights = new ArrayList();
		teamRights = teamDao.getTeamRights();

		stdRightsB.setSuperRightsStringForStudy((String)teamRights.get(0));
		stdRightsB.loadStudyRights();

		if ((stdRightsB.getFtrRights().size()) == 0){
			pageRight= 0;
		}else{
			//checking study setup access unless a new access right for combined budget
			pageRight = Integer.parseInt(stdRightsB.getFtrRightsByValue("MILESTONES"));
		}
	}
	 /*YK 29Mar2011 -DFIN20 */
	int studyIDs=Integer.parseInt(studyId);
	if (teamId.size()> 0) {
	studyB.setId(studyIDs);
	studyB.getStudyDetails();
	}
	CodeDao cdMsSetDao = new CodeDao();
	String roleCodePk="";

	if (! StringUtil.isEmpty(studyId) && EJBUtil.stringToNum(studyId) > 0)
	{
		ArrayList tId = new ArrayList();

		teamDao.getTeamRights(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(userIdFromSession));
		tId = teamDao.getTeamIds();

		if (tId != null && tId.size() > 0)
		{
			ArrayList arRoles = new ArrayList();
			arRoles = teamDao.getTeamRoleIds();

			if (arRoles != null && arRoles.size() >0 )
			{
				roleCodePk = (String) arRoles.get(0);

				if (StringUtil.isEmpty(roleCodePk))
				{
					roleCodePk="";
				}
			}
			else
			{
				roleCodePk ="";
			}

		}
		else
		{
			roleCodePk ="";
		}
	} else
		{
		  roleCodePk ="";
		}

	ArrayList setting = new ArrayList();
	SettingsDao setDao = new SettingsDao();
	setDao.retrieveSettings(accId, 1, "MILESTONE_SET_STATUS");
	setting = setDao.getSettingValue();
	
	cdMsSetDao.getCodeValuesForStudyRole("mile_setstat",roleCodePk);
	String mileSetStat = "";

	String selMilePayType = "";
	selMilePayType = request.getParameter("mileStat");
	if (selMilePayType==null) selMilePayType="";
	if ("N".equalsIgnoreCase(mode))
		mileSetStat = cdMsSetDao.toPullDown("mile_setstat id='mile_setstat' onchange='setMileStatus(this);'");
	else
		mileSetStat = cdMsSetDao.toPullDown("mile_setstat id='mile_setstat' onchange='setMileStatus(this);'",EJBUtil.stringToNum(studyB.getMilestoneSetStatus()),withSelect);
	
	String studyNo = (String) tSession.getValue("studyNo");
	/*YK 29Mar2011 -DFIN20 */
	
	/*
	Start By Yogendra for 22500
	*/
	
	
	CodeDao cdMs = new CodeDao();

	cdMs.getCodeValuesForStudyRole("milestone_stat",roleCodePk);

	ArrayList statSubTypes = cdMs.getCSubType();
	
	int wipPk = cdMs.getCodeId("milestone_stat","WIP");
	String wipPkStr = wipPk +"";
	//KM-08Jun10-#4966
	String wipDesc = cdMs.getCodeDescription(wipPk);

	int actPk = cdMs.getCodeId("milestone_stat","A");
	String actPkStr = actPk + "";

	int inactPk = cdMs.getCodeId("milestone_stat","IA");
	String inactPkStr = inactPk + "";
	
	
	CodeDao cdMsTypDao = new CodeDao();
	cdMsTypDao.getCodeValues("milestone_type");
	
	String msType ="";
		String selMileStone = "";
		selMileStone = request.getParameter("milestoneType");
		if (selMileStone==null) selMileStone="";

		if (EJBUtil.isEmpty(selMileStone))

			msType	= cdMsTypDao.toPullDown("milestoneType");
		else
			msType = cdMsTypDao.toPullDown("milestoneType",EJBUtil.stringToNum(selMileStone),withSelect);
	
		//Status
		String msStat = "";
		CodeDao cdMsStatDao = new CodeDao();
		cdMsStatDao.getCodeValues("milestone_stat");

		String selStatus = "";
		selStatus = request.getParameter("milestoneStat");
		if (selStatus==null) selStatus="";



		if (EJBUtil.isEmpty(selStatus))
			msStat = cdMsStatDao.toPullDown("milestoneStat");
		else
			msStat = cdMsStatDao.toPullDown("milestoneStat",EJBUtil.stringToNum(selStatus),withSelect);
	
		//Payment type
		CodeDao cdMsPayDao = new CodeDao();
		cdMsPayDao.getCodeValues("milepaytype");
		String msPayType = "";

		String selMilePayType1 = "";
		selMilePayType1 = request.getParameter("milestPayType");
		if (selMilePayType1==null) selMilePayType1="";



		if (EJBUtil.isEmpty(selMilePayType1))
			msPayType = cdMsPayDao.toPullDown("milestPayType");
		else
			msPayType = cdMsPayDao.toPullDown("milestPayType",EJBUtil.stringToNum(selMilePayType1),withSelect);
		
		
		String srchStrCalName = StringUtil.decodeString(request.getParameter("calName"));
		if (srchStrCalName ==null) srchStrCalName="";
		
		String srchStrProtocolId = StringUtil.decodeString(request.getParameter("protocolId"));
		if (srchStrProtocolId ==null) srchStrProtocolId="";
		
		String srchStrVisitName = request.getParameter("visitName");
		if (srchStrVisitName ==null) srchStrVisitName="";

		String srchStrEvtName = request.getParameter("evtName");
		if (srchStrEvtName ==null) srchStrEvtName="";
		
		String showFlag = "false";

		if ( (!srchStrCalName.equals("")) || (!srchStrVisitName.equals("")) || (!srchStrEvtName.equals(""))){
			showFlag = "true";
		}
		
		
		
		CodeDao cdao  = new CodeDao();
		cdao.getCodeValues("milestone_type");
		ArrayList<String> subType = cdao.getCSubType();
		ArrayList<Integer> codeIds = cdao.getCId();
		
	
	//String mileStatDesc= cdMsPayDao.getCodeDesc();
%>
<%
//Start Code committed for the bug id 23673
if("Y".equals(CFG.Workflows_Enabled)&& (studyIdForTabs!=null && !studyIdForTabs.equals("") && !studyIdForTabs.equals("0")) && !mileFrom.equalsIgnoreCase("milestoneSearch") && workFlag.equalsIgnoreCase("Y")){
//End Code
%>
<DIV class="BrowserBotN BrowserBotN_S_3 workflowDivBig" id="divBottom" style="top:150px">
<%}else{ %>
<script language=javascript>
var isIE = jQuery.browser.msie;
if(isIE==true)
{
	document.write('<DIV class="BrowserBotN BrowserBotN_S_3" style="width:101%" id="divBottom" <%=styleTop %>>')
}
else
{
	document.write('<DIV class="BrowserBotN BrowserBotN_S_3" id="divBottom" <%=styleTop %>>')
}
</script>
<%} %>
<%
if (pageRight > 0){		
	%>
	<input type="hidden" id ="pageRight" name="pageRight" value="<%=pageRight%>"/>
	<%
	if(studyId == "" || studyId == null) {%>
	<jsp:include page="studyDoesNotExist.jsp" flush="true"/>
	<% }else{
	%>
	<script>
		function reloadMilestoneGrid(pMileTypeId) {
			
			reloadMilestoneGridBySearch(pMileTypeId,'','','','','','','','','','');
		}

		function reloadMilestoneGridBySearch(mileType,milestoneStatusVal,milestPayTypeVal,calNameVal,visitNameVal,evtNameVal,isSearch,inactiveFlag,datefrom,dateto,clickedOnMile){
			var srchStrProtocolId	=	$j("#srchStrProtocolId").val();
			var strCalName = $j("#strCalName").val();
			calNameVal=encodeURIComponent(calNameVal);
			var param=
				YAHOO.example.milestoneGrid = function() {
					var args = {
						urlParams: "<%=urlParamSB.toString()%>&msType="+mileType+"&msStatus="+milestoneStatusVal+"&msPayType="+milestPayTypeVal+"&calName="+calNameVal+"&visitName="+visitNameVal+"&evtName="+evtNameVal+"&inactiveFlag="+inactiveFlag+"&datefrom="+datefrom+"&dateto="+dateto+"&srchStrProtocolId="+srchStrProtocolId+"&strCalName="+strCalName,
						dataTable: "milestonegrid",
						pMileTypeId: mileType ,
						isSearch:isSearch,
						clickedOnMile:clickedOnMile
					};
					myMilestoneGrid = new VELOS.milestoneGrid('fetchMilestoneJSON.jsp', args);
					myMilestoneGrid.startRequest();
			    }();
		    
		}

		function reloadMilestoneGridForHoldBack(mileType,holdback){
			
			var param=
			YAHOO.example.milestoneGrid = function() {
				var args = {
					urlParams: "<%=urlParamSB.toString()%>&msType="+mileType,
					dataTable: "milestonegrid",
					pMileTypeId: mileType,
					holdbackToAll:holdback.value
				};
				
				myMilestoneGrid = new VELOS.milestoneGrid('fetchMilestoneJSON.jsp', args);
				myMilestoneGrid.startRequest();
		    }();
		}
		
		/*YK 01Aug11:Commented for Enhancement FIN-20073, Now using onload of Panel for individual Milestones*/
		/*YAHOO.util.Event.addListener(window, "load", function() {
			reloadMilestoneGrid('ALL');
		});
		*/
		function validateNumber(evt){
			if(evt.shiftKey) return false;
			var charCode = (evt.which) ? evt.which : evt.keyCode;
			//delete, backspace & arrow keys
			if (charCode == 8 || charCode == 46 || charCode == 37 || charCode == 39)
				return true;

			if (charCode > 31 && (!((47 < charCode && charCode  < 58) || (95 < charCode &&  charCode < 106))))
				return false;
			return true;
		}

		// Added for Holdback value validation - FIN-22301 : Raviesh
		function validateHoldback(evt){
			if(evt.shiftKey) return false;
			var charCode = (evt.which) ? evt.which : evt.keyCode;
			//delete, backspace & arrow keys
			if (charCode == 8 || charCode == 46 || charCode == 37 || charCode == 39 || charCode == 110 || charCode == 190 )
				return true;

			if (charCode > 31 && (!((47 < charCode && charCode  < 58) || (95 < charCode &&  charCode < 106))))
				return false;
			return true;
		}
		
        function validateHoldbackNumber(holdbackVal){
			if(holdbackVal > 99.99 || holdbackVal < 0){
					document.getElementById("holdback").value = "0.00";
					alert("<%=MC.M_ValEtrFwg_2Dgtholdback %>");
					setTimeout(function() { $j('#holdback').focus(); }, 0);
			return false;
		}
	}

/*YK 29Mar2011 -DFIN20 */
function setMileStatus(oBj){

	var element=oBj.value;
	var elementDesc=oBj.options[oBj.selectedIndex].text
	if(element.length==0 || element=="")
		{
		 alert("<%=MC.M_PlsSel_StatChg%>");/*alert("Please select status to change.");*****/
			 document.getElementById("mileStat").value="";
			 document.getElementById("mileStatDesc").value="";
			 document.getElementById("mile_setstat").focus();
			 return false;
		}
		document.getElementById("mileStat").value=element;
		document.getElementById("mileStatDesc").value=elementDesc;
		return true;
	
}	

function createMileStone()
{
 if(!(setMileStatus(document.getElementById("mile_setstat")))){ return false;} 	
 var studyID=document.getElementById("studyId").value;
 var studyName=document.getElementById("studyName").value;
 var mileStatus=document.getElementById("mileStat").value;
 var oldmileStatus=document.getElementById("oldmileStat").value;
 var statusDesc=document.getElementById("mileStatDesc").value;
 if(oldmileStatus==mileStatus)
	{ 
		alert("<%=MC.M_TheStatAlrdy_StdSel%>");/*alert("This Status is already set in <%=LC.Std_Study %>. Please select other status to change.");*****/
		document.getElementById("mile_setstat").focus();
	    return false;
	}
 VELOS.milestoneGrid.setStudyMileStat(studyID,studyName,mileStatus,statusDesc);
 return false;
 
}
/*YK 29Mar2011 -DFIN20 */
	</script>
<!-- YK 01Aug11: Milestone Accordion Functions Starts -->
<script>
jQuery.noConflict(); /*stops conflicting code with jQuery syntax*/
jQuery(function() {

	/*Open/Closes Patient Milestone Panel*/
	jQuery( "#PatientMileStone" ).accordion({
		autoHeight: false, collapsible: true, active: false, fillSpace: false, animated: false
	});
	/*Open/Closes Visit Milestone Panel*/
	jQuery( "#VisitMilestone" ).accordion({
		autoHeight: false, collapsible: true, active: false, fillSpace: false, animated: false
	});
	/*Open/Closes Event Milestone Panel*/
	jQuery( "#EventMilestone" ).accordion({
		autoHeight: false, collapsible: true, active: false, fillSpace: false, animated: false
	});
	/*Open/Closes Study Milestone Panel*/
	jQuery( "#StudyMilestone" ).accordion({
		autoHeight: false, collapsible: true, active: false, fillSpace: false, animated: false
	});
	/*Open/Closes Additional Milestone Panel*/
	jQuery( "#AdditionalMilestone" ).accordion({
		autoHeight: false, collapsible: true, active: false, fillSpace: false, animated: false
	});

	jQuery("#save_changes").click(function(){
		if (f_check_perm(<%=pageRight%>,'E')) {
		VELOS.milestoneGrid.saveDialogForAll();
		}
	});
});

$j(document).ready(function(){
	
	$j("#inactiveMiles").attr("checked", "checked");
	document.getElementById('inactiveFlag').value="1";
	var mileStoneType = GetURLParameter('mileStoneType');
	if(mileStoneType=='PM'){
		$j( "#PatientMileStone" ).accordion({active:0});
		loadMilestoneData(mileStoneType);
	}else if(mileStoneType=='VM'){
		$j( "#VisitMilestone" ).accordion({active:0});
		loadMilestoneData(mileStoneType);
	}else if(mileStoneType=='EM'){
		$j( "#EventMilestone" ).accordion({active:0});
		loadMilestoneData(mileStoneType);
	}else if(mileStoneType=='SM'){
		$j( "#StudyMilestone" ).accordion({active:0});
		loadMilestoneData(mileStoneType);
	}else if(mileStoneType=='AM'){
		$j( "#AdditionalMilestone" ).accordion({active:0});
		loadMilestoneData(mileStoneType);
	}
	<%if( mileFrom.equalsIgnoreCase("manageMilestone") ){%>
		searchMilestones('');
		<% } %>

	//Submitting form On enter key press Bug# 14779 
	$j(".serachField").keyup(function(event){
		searchform(event);
	});
	
});

function searchform(event){
	 if (event.which == 13 || event.keyCode == 13) {
		 searchMilestones('');
         return false;
     }
     return true;
}

// Function to apply Holdback in all the milestones 
function holdback_applyAll(){	
	var holdBackVal=$j("#holdback").val();
	var accorClass='ui-accordion-header ui-helper-reset ui-state-active ui-corner-top';
	var mileArr = [{mileType:'PM',id:'#PatientMileStone'},{mileType:'VM',id:'#VisitMilestone'},
					{mileType:'EM',id:'#EventMilestone'},{mileType:'SM',id:'#StudyMilestone'},
					{mileType:'AM',id:'#AdditionalMilestone'}];
	
	for(var mile=0; mile< mileArr.length;mile++){
		var milestone=mileArr[mile];
		var isOpen=jQuery("#mile"+milestone.mileType).hasClass(accorClass);
		var isHidden=jQuery(milestone.id).attr('style')!=undefined && jQuery(milestone.id).attr('style').indexOf('none')!=-1?true:false;
		if(!isHidden){
		if(isOpen){
			VELOS.applyHoldBackToActiveAccordian(milestone.mileType,holdBackVal);
		}else{
			$j(milestone.id).accordion({active:0});
			loadMilestoneDataToSetHoldBack(milestone.mileType,holdBackVal);
		}
		}	
	}
}

//Search the Milestones
function searchMilestones(clickedMileType){
	var milestoneTypeVal 	= 	$j.trim($j("#milestoneType").val());
	var milestoneStatusVal 	= 	$j.trim($j("#milestoneStat").val());
	var milestPayTypeVal 	= 	$j.trim($j("#milestPayType").val());
	var calNameVal 			=	$j.trim($j("#calName").val());
	var visitNameVal 		=	$j.trim($j("#visitName").val());
	var evtNameVal 			=	$j.trim($j("#evtName").val());
	var datefrom			= 	$j.trim($j("#datefrom").val());
	var dateto				= 	$j.trim($j("#dateto").val());
	var holdBackVal 		= 	$j.trim($j("#holdback").val());
	var inactiveFlag 		= 	chkExcludeMilestone();
	
	var selectedMileType = "";
	<% for(int i=0; i<codeIds.size();i++){
		int codeId = codeIds.get(i); 
		String subType1 = subType.get(i);
	%>
		var codeIdVal = "<%=codeId%>";
		var subTypeVal = "<%=subType1%>";
			if(codeIdVal==milestoneTypeVal){
				selectedMileType = subTypeVal;
			}	
	<%}%>

	var selMileType="";
	var isSearch='Y';
	var clickedOnMile='';
	if(clickedMileType==''){
		if((datefrom!=null || datefrom!="undefined") && (dateto !=null || dateto!="undefined")){
			if(CompareDates(datefrom,dateto)){
				alert(M_DtToNotLessThan_DtFrom);/*"Date To" should not be less than "Date From".*/
				return false;
			}
		}
		selectedMileType = selectedMileType;
		collapseAllAccordion();
		showSelectedMilestones(selectedMileType);
	}else{
		isSearch='N';
		clickedOnMile='Y'
		selectedMileType = clickedMileType;
	}
	
	if(selectedMileType=='PM' || selectedMileType=="" ){
		if(clickedMileType=='')
			$j( "#PatientMileStone" ).accordion({active:0});
		loadMilestoneDataBySearch('PM',milestoneStatusVal,milestPayTypeVal,calNameVal,visitNameVal,evtNameVal,inactiveFlag,datefrom,dateto,clickedMileType,isSearch,clickedOnMile);
	}
	if(selectedMileType=='VM' || selectedMileType=="" ){
		if(clickedMileType=='')
			$j( "#VisitMilestone" ).accordion({active:0});
		loadMilestoneDataBySearch('VM',milestoneStatusVal,milestPayTypeVal,calNameVal,visitNameVal,evtNameVal,inactiveFlag,datefrom,dateto,clickedMileType,isSearch,clickedOnMile);
	}
	if(selectedMileType=='EM' || selectedMileType=="" ){
		if(clickedMileType=='')
			$j( "#EventMilestone" ).accordion({active:0});
		loadMilestoneDataBySearch('EM',milestoneStatusVal,milestPayTypeVal,calNameVal,visitNameVal,evtNameVal,inactiveFlag,datefrom,dateto,clickedMileType,isSearch,clickedOnMile);
	}
	if(selectedMileType=='SM' || selectedMileType=="" ){
		if(clickedMileType=='')
			$j( "#StudyMilestone" ).accordion({active:0});
		loadMilestoneDataBySearch('SM',milestoneStatusVal,milestPayTypeVal,calNameVal,visitNameVal,evtNameVal,inactiveFlag,datefrom,dateto,clickedMileType,isSearch,clickedOnMile);
	}
	if(selectedMileType=='AM' || selectedMileType==""){
		if(clickedMileType=='')
			$j( "#AdditionalMilestone" ).accordion({active:0});
		loadMilestoneDataBySearch('AM',milestoneStatusVal,milestPayTypeVal,calNameVal,visitNameVal,evtNameVal,inactiveFlag,datefrom,dateto,clickedMileType,isSearch,clickedOnMile);
	}
}
/**
 *
 * Loads Milestone Panel On-Demand 
 * param milestonetype as PM, VM, EM, SM, AM.
 */
function loadMilestoneData(mileType)
{
	var checkData = mileType+"_milestonegrid";
	if(document.getElementById(checkData).innerHTML.length<=0) /*Checks if grid is empty*/
	{
			reloadMilestoneGrid(mileType); /*Loads the grid*/
	}else
	{
		document.getElementById(checkData).innerHTML=""; /*Empty the grid when panel closes.*/
	}
	
}

//Used to Load Mliestone Data by Search
function loadMilestoneDataBySearch(mileType,milestoneStatusVal,milestPayTypeVal,calNameVal,visitNameVal,evtNameVal,inactiveFlag,datefrom,dateto,clickedMileType,isSearch,clickedOnMile){
	var checkData = mileType+"_milestonegrid";
		if(clickedMileType==''){
		document.getElementById(checkData).innerHTML=""; /*Empty the grid when panel closes.*/
		}
		if(document.getElementById(checkData).innerHTML.length<=0) /*Checks if grid is empty*/
		{
				reloadMilestoneGridBySearch(mileType,milestoneStatusVal,milestPayTypeVal,calNameVal,visitNameVal,evtNameVal,isSearch,inactiveFlag,datefrom,dateto,clickedOnMile); /*Loads the grid*/
		}else
		{
			document.getElementById(checkData).innerHTML=""; /*Empty the grid when panel closes.*/
			VELOS.milestoneGrid.flushListAndResetRemainder(mileType);
		}
}

//Used to Load Milestone Data to set Holdback - FIN-22301 : Raviesh
function loadMilestoneDataToSetHoldBack(mileType,holdBack){
	reloadMilestoneGridForHoldBack(mileType,holdback); /*Loads the grid*/
}

//Collapse All Milestone Grids (or) Accordian
function collapseAllAccordion(){
	$j( "#PatientMileStone" ).accordion({active: false, collapsible: true});
	$j( "#VisitMilestone" ).accordion({active: false, collapsible: true});
	$j( "#EventMilestone" ).accordion({active: false, collapsible: true});
	$j( "#StudyMilestone" ).accordion({active: false, collapsible: true});
	$j( "#AdditionalMilestone" ).accordion({active: false, collapsible: true});
}

function showSelectedMilestones(selectedMileType){
	if(selectedMileType!=""){
		if(selectedMileType=='PM'){
			showAndHideMilestones('0');
		}else if(selectedMileType=='VM'){
			showAndHideMilestones('1');
		}else if(selectedMileType=='EM'){
			showAndHideMilestones('2');
		}else if(selectedMileType=='SM'){
			showAndHideMilestones('3');
		}else if(selectedMileType=='AM'){
			showAndHideMilestones('4');
		}
	}else{
		showAndHideMilestones("");
	}
}
function showAndHideMilestones(indx){
	var mileDivIds=["PatientMileStone","VisitMilestone","EventMilestone","StudyMilestone","AdditionalMilestone"];
	for(var i=0 ; i<5 ; i++){
		if(indx!=''){
			if(i==parseInt(indx)){
				$j( "#"+mileDivIds[i] ).show();
			}else{
				$j( "#"+mileDivIds[i] ).hide();
			}
		}else{
			$j( "#"+mileDivIds[i] ).show();
		}
	}
}
// View/Delete Achievements

function viewAch( rt){

	var studyId  =document.getElementById('studyId').value;

   if(!f_check_perm(rt,'E')) {
	          return false;
	  }


	  windowName =window.open("deleteAchMS.jsp?studyId="+studyId, "Del","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=850,height=400,top=100,left=100");
	  windowName.focus();


}

//to excude inactive milestones set the flag 
function chkExcludeMilestone(){
	if (document.getElementById('inactiveMiles').checked){
		return document.getElementById('inactiveFlag').value="1";
	}else{
		return document.getElementById('inactiveFlag').value="0";
	}
}
</script>

<!-- Milestone Accordion Functions Ends -->
<%--Milestone search  criteria Starts --%>

	
	
	<table width="99%" border=0 cellspacing="0" cellpadding="0" class="basetbl">
	<tr >
	<td colspan="6" > <b> <%=LC.L_Search_By%><%--Search By*****--%></b></td>
	</tr>
	<tr>
	<td width="15%" > <%=LC.L_Milestone_Type%><%--Milestone Type*****--%>:</td>
		 <td width="18%" ><%=msType%></td>
    <td width="15%"  align="right"> <%=LC.L_Status%><%--Status*****--%>:&nbsp;</td>
    	 <td width="18%" ><%=msStat%></td>
    <td width="15%" align="right"> <%=LC.L_Payment_Type%><%--Payment Type*****--%>:&nbsp;</td>
    	 <td width="18%" ><%=msPayType%></td>
    </tr>
    <tr>
    <td > <%=LC.L_Calendar%><%--Calendar*****--%>:</td>
    	 <td ><input type="text" name="calName" id="calName" class="serachField" value="<%=srchStrCalName%>" size="25" ></td>
    <td align="right"> <%=LC.L_Visit%><%--Visit*****--%>:&nbsp;</td>
    	 <td ><input type="text" name="visitName" id="visitName" class="serachField" value="<%=srchStrVisitName%>" size="18" ></td>
    <td align="right"> <%=LC.L_Event%><%--Event*****--%>:&nbsp;</td>
    	 <td ><input type="text" name="evtName" id="evtName" class="serachField" value="<%=srchStrEvtName%>" size="17" ></td>
	</tr>
	
	<tr>
    <td > <%=LC.L_Date_From%><%--Date From*****--%>:</td>
    	 <td ><input type="text" name="datefrom"  id="datefrom" class="datefield" size ="15" MAXLENGTH ="20" value='' ></td>
    <td align="right"> <%=LC.L_Date_To%><%--Date To*****--%>:&nbsp;</td>
    	 <td ><input type="text" name="dateto" id="dateto" class="datefield" size ="15" MAXLENGTH ="20" value='' ></td>
    <td align="right"><input type="checkbox" name="inactiveMiles" id="inactiveMiles"/>
		<%=LC.L_Exclude_InactiveItems%><%--Exclude Inactive Items*****--%></td>
    	<td align="center"><button type="button" onclick="searchMilestones('');" ><%=LC.L_Search%></button>
		</td>
	</tr>
</table>
<hr class="thickline"></hr>

<%--Milestone search  criteria End --%>

	<table width="98%" cellspacing="0" cellpadding="0" >
	  <!-- Holdback Implementation for All milestones - FIN-22301 : Raviesh -->
	  <tr>
	  <td width="7%" align="center"><%=LC.L_Holdback_Amt+" "+"%" %></td>
	  <td width="5%" align="left"><input id="holdback" onkeydown="return validateHoldback(event);" onblur="return validateHoldbackNumber(this.value);" class="index numberfield" type="text"  maxlength="5" size="5" value="0.0"></td>
	  <%if (isAccessibleFor(pageRight, 'N')||isAccessibleFor(pageRight, 'E')){%> 
 	  <td width="8%" align="left"><button id="apply_all"  name="apply_all" type="button" onclick="ripLocaleFromAll(); if (f_check_perm(<%=pageRight%>,'E')) {holdback_applyAll(); applyLocaleToAll();}"><%=LC.L_Apply_All%></button></td>
 	  <td width="15%" align="left"><button id="save_changes"  name="save_changes" type="button"><%=LC.L_Preview_AndSave%></button></td>
 	  <%} %>
	  <td><button id="view_delete" name="view_delete" type="button" onClick ="viewAch('<%=pageRight%>');" ><%=LC.L_ViewOrDel_Achv%><%--View/Delete Achievements*****--%></button></td>
 	  </tr>
	  <input type="hidden" name="isSearch" id="isSearch" value=""/>
	  <input type="hidden" name="clickedOnMile" id="clickedOnMile" value=""/>
 	  <input type="hidden" name="studyName" id="studyName" value="<%=studyNo%>"/>
 	  <input type="hidden" name="studyId" id="studyId" value="<%=studyId%>"/>
 	  <input type="hidden" name="mileStat" id="mileStat" />
 	  <input type="hidden" name="oldmileStat" id="oldmileStat" value="<%=studyB.getMilestoneSetStatus()%>"/>
 	  <input type="hidden" name="mileStatDesc" id="mileStatDesc"/>
 	  <input type="hidden" name="inactiveFlag" id="inactiveFlag"/>
	  <input type="hidden" name="mileFrom" id="mileFrom" value="<%=mileFrom %>" />
 	  <input type="hidden" name="strCalName" id="strCalName" value="<%=srchStrCalName %>" />
 	  <input type="hidden" name="srchStrProtocolId" id="srchStrProtocolId" value="<%=srchStrProtocolId%>" />
 	   
 	  <!-- SM 8Apr2011 -DFIN20 -->
 	  <%if (setting.size() > 0 && "1".equals(setting.get(0))){%>
	  <!-- YK 29Mar2011 -DFIN20 -->
	  <tr> <td align="left" colspan="6">
	  <!--YK: 05APR2011 Bug#6007 & #6005  -->
	  <%=LC.L_StdMstone_Stat%><%--<%=LC.Std_Study %> Milestone Status*****--%>:&nbsp;<br/><%=mileSetStat %>&nbsp;
	  <button name="set_mile_setStat" onclick="if (f_check_perm(<%=pageRight%>,'E')) { createMileStone(); }" ondblclick="return false;"><%=LC.L_Set_Status%></button>
	  </td>
 	  </tr>
 	  <%} %>
 	  
 	  <!-- YK 29Mar2011 -DFIN20 -->
	  <!-- YK 01Aug11: Milestone Accordion Start Here -->
	  <tr><td align="center" colspan="6"><p class="defComments_txt"><%=MC.M_ExpdMstone_ExstNewMstone%><%--Please expand a Milestone Type panel below to manage existing  or add New Milestones*****--%></p></td></tr>
	  
	  </table>
	  
	<!-- Start Patient Status MileStone --> 
	<div id="PatientMileStone">
	<h3 onclick="searchMilestones('PM');" id="milePM"><a href="#" ><%=LC.L_Pat_StatusMstones%><%--<%=LC.Pat_Patient %> Status Milestones*****--%></a></h3> <%-- YK(05Aug11):Bug#6759  --%>
	<div >
		<table width="98%" cellspacing="0" cellpadding="0" >
		<tr>
		  <td>
			<%if (isAccessibleFor(pageRight, 'N')||isAccessibleFor(pageRight, 'E')){%> 
		 			<FONT class='Mandatory'>(<%=MC.M_ClkRow_ToEdt%><%--Click row to edit*****--%>)</FONT>&nbsp;&nbsp;&nbsp;&nbsp;
		 		<button type="submit" id="save_changes" name="save_changes" 
		    	onclick="if (f_check_perm_noAlert(<%=pageRight%>,'N') || f_check_perm_noAlert(<%=pageRight%>,'E')) { VELOS.milestoneGrid.saveDialog('PM'); }" ><%=LC.L_Preview_AndSave%></button>
		    <%} %>
		  </td>
		
		<%if (isAccessibleFor(pageRight, 'N')){%> 
	
			<td align="right">
				<%=LC.L_Add%><%--Add*****--%> &nbsp;<input id="PM_rowCount" type="text" value="0" size="2" maxlength="2" class="index"
				onkeydown = "return validateNumber(event);"/> <%=LC.L_Add_Mstone_S%><%--Add Milestone(s)*****--%>
				<img src="../images/add.png"/ title="<%=LC.L_Add%>" onclick="VELOS.milestoneGrid.addRows('PM');">
			<span width="50%">&nbsp;</span>
			</td>
		<%} %>
		</tr>
		</table>
		<!-- Patient Status Milestone Power Bar Grid - INF-22500 : Raviesh -->
		<%if (isAccessibleFor(pageRight, 'N')||isAccessibleFor(pageRight, 'E')){%> 
		<table width="98%" cellspacing="0" cellpadding="0">
			<tr>
			 <td>
				<div width="90%" id="PM_Parent_milestonegrid"></div>
			 </td>
			 <td>
			 	<button id="setToSel" type="button" name="setToSel" onclick="if (f_check_perm_noAlert(<%=pageRight%>,'N') || f_check_perm_noAlert(<%=pageRight%>,'E')) { VELOS.milestoneGrid.setToSelected('PM'); }"><%=LC.L_Set_To_Sel%><%--Set to Selected*****--%></button>
        	 </td>				
			</tr>
			<tr style="height:10px"><td></td></tr>
		</table>
		<%} %>
		<table width="98%" cellspacing="0" cellpadding="0">
		  <tr>
			<td>
				<div width="100%" id="PM_milestonegrid"></div>
			</td>
		  </tr>
		</table>
	</div>
	</div>
	<!-- End Patient Status MileStone -->
	<!-- Start Visit Milestones --> 
	<div id="VisitMilestone">
	<h3 onclick="searchMilestones('VM');" id="mileVM"><a href="#" ><%=LC.L_Visit_Mstones%><%--Visit Milestones*****--%></a></h3> <%-- YK(05Aug11):Bug#6759  --%>
	<div >
		<table width="98%" cellspacing="0" cellpadding="0" >
		<tr>
		  <td>
			<%if (isAccessibleFor(pageRight, 'N')||isAccessibleFor(pageRight, 'E')){%> 
  			<FONT class='Mandatory'>(<%=MC.M_ClkRow_ToEdt%><%--Click row to edit*****--%>)</FONT>&nbsp;&nbsp;&nbsp;&nbsp;
  			<button type="submit" id="save_changes" name="save_changes" 
	    	onclick="if (f_check_perm_noAlert(<%=pageRight%>,'N') || f_check_perm_noAlert(<%=pageRight%>,'E')) { VELOS.milestoneGrid.saveDialog('VM'); }" ><%=LC.L_Preview_AndSave%></button>
	    <%} %>
		  </td>
		
		<%if (isAccessibleFor(pageRight, 'N')){%>
	 	<td align="right">
	  		<%=LC.L_Add%><%--Add*****--%> &nbsp;<input id="VM_rowCount" type="text" value="0" size="2" maxlength="2" class="index"
	  		onkeydown = "return validateNumber(event);"/> <%=LC.L_Add_Mstone_S%><%--Add Milestone(s)*****--%>
			<img src="../images/add.png"/ title="<%=LC.L_Add%>" onclick="VELOS.milestoneGrid.addRows('VM');"> 			<span width="50%">&nbsp;</span>
	  	</td>
	  	<%} %>
		</tr>
		</table>
		<!-- Visit Milestone Power Bar Grid - INF-22500 : Raviesh -->
		<%if (isAccessibleFor(pageRight, 'N')||isAccessibleFor(pageRight, 'E')){%> 
		<table width="98%" cellspacing="0" cellpadding="0">
			<tr>
			 <td>
				<div width="90%" id="VM_Parent_milestonegrid"></div>
			 </td>
			 <td>
  				<button id="setToSel" type="button" name="setToSel" onclick="VELOS.milestoneGrid.setToSelected('VM');"><%=LC.L_Set_To_Sel%><%--Set to Selected*****--%></button>
        	 </td>				
			</tr>
			<tr style="height:10px"><td></td></tr>
		</table>
		<%} %>
		<table width="98%" cellspacing="0" cellpadding="0" >
		  <tr>
			<td>
				<div id="VM_milestonegrid"></div>
			</td>
		  </tr>
		</table>
	</div>
	</div>
	<!-- End Visit Milestones -->
	<!-- Start Event Milestones --> 
	<div id="EventMilestone">
	<h3 onclick="searchMilestones('EM');" id="mileEM"><a href="#" ><%=LC.L_Evt_Mstone%><%--Event Milestones*****--%></a></h3> <%-- YK(05Aug11):Bug#6759  --%>
	<div >
		<table width="98%" cellspacing="0" cellpadding="0" >
		<tr>
		  <td>
			<%if (isAccessibleFor(pageRight, 'N')||isAccessibleFor(pageRight, 'E')){%> 
  			<FONT class='Mandatory'>(<%=MC.M_ClkRow_ToEdt%><%--Click row to edit*****--%>)</FONT>&nbsp;&nbsp;&nbsp;&nbsp;
  			<button type="submit" id="save_changes" name="save_changes" 
	    	onclick="if (f_check_perm_noAlert(<%=pageRight%>,'N') || f_check_perm_noAlert(<%=pageRight%>,'E')) { VELOS.milestoneGrid.saveDialog('EM'); }" ><%=LC.L_Preview_AndSave%></button>
	   <%} %>
		  </td>
		
		<%if (isAccessibleFor(pageRight, 'N')){%>
	 	<td align="right">
	  		<%=LC.L_Add%><%--Add*****--%> &nbsp;<input id="EM_rowCount" type="text" value="0" size="2" maxlength="2" class="index"
	  		onkeydown = "return validateNumber(event);"/>  <%=LC.L_Add_Mstone_S%><%--Add Milestone(s)*****--%>			
			<img src="../images/add.png"/ title="<%=LC.L_Add%>" onclick="VELOS.milestoneGrid.addRows('EM');">  
			<span width="50%">&nbsp;</span>
	  	</td>
	  	<%} %>
		</tr>
		</table>
		<!-- Event Milestone Power Bar Grid - INF-22500 : Raviesh -->
		<table width="98%" cellspacing="0" cellpadding="0">
		<%if (isAccessibleFor(pageRight, 'N')||isAccessibleFor(pageRight, 'E')){%> 
			<tr>
			 <td>
				<div width="90%" id="EM_Parent_milestonegrid"></div>		
			 </td>
			 <td>
  				<button id="setToSel" type="button" name="setToSel" onclick="VELOS.milestoneGrid.setToSelected('EM');"><%=LC.L_Set_To_Sel%><%--Set to Selected*****--%></button>
        	 </td>		
        	 <tr style="height:10px"><td></td></tr>
			</tr>
			<%} %>
		</table>
		<table width="98%" cellspacing="0" cellpadding="0" >
		  <tr>
			<td>
				<div id="EM_milestonegrid"></div>
			</td>
		  </tr>
		</table>
	</div>
	</div>
	<!-- End Event Milestones -->
	<!-- Start Study Status Milestones --> 
	<div id="StudyMilestone">
	<h3 onclick="searchMilestones('SM');" id="mileSM"><a href="#" ><%=LC.L_StdPat_Mstone%><%--<%=LC.Std_Study %> Status Milestones*****--%></a></h3> <%-- YK(05Aug11):Bug#6759  --%>
	<div >
		<table width="98%" cellspacing="0" cellpadding="0" >
		<tr>
		  <td>
			<%if (isAccessibleFor(pageRight, 'N')||isAccessibleFor(pageRight, 'E')){%> 
  			<FONT class='Mandatory'>(<%=MC.M_ClkRow_ToEdt%><%--Click row to edit*****--%>)</FONT>&nbsp;&nbsp;&nbsp;&nbsp;
	    	<button type="submit" id="save_changes" name="save_changes" 
	    	onclick="if (f_check_perm_noAlert(<%=pageRight%>,'N') || f_check_perm_noAlert(<%=pageRight%>,'E')) { VELOS.milestoneGrid.saveDialog('SM'); }" ><%=LC.L_Preview_AndSave%></button>
	    <%} %>
		  </td>
		
		<%if (isAccessibleFor(pageRight, 'N')){%>
	 	<td align="right">
	  		<%=LC.L_Add%><%--Add*****--%> &nbsp;<input id="SM_rowCount" type="text" value="0" size="2" maxlength="2" class="index"
	  		onkeydown = "return validateNumber(event);"/>  <%=LC.L_Add_Mstone_S%><%--Add Milestone(s)*****--%>
			<img src="../images/add.png"/ title="<%=LC.L_Add%>" onclick="VELOS.milestoneGrid.addRows('SM');">   
			<span width="50%">&nbsp;</span>
	  	</td>
	  	<%} %>
		</tr>
		</table>
		<!-- Study Status Milestone Power Bar Grid - INF-22500 : Raviesh -->
		<%if (isAccessibleFor(pageRight, 'N')||isAccessibleFor(pageRight, 'E')){%> 
		<table width="98%" cellspacing="0" cellpadding="0">
			<tr>
			 <td>
				<div width="90%" id="SM_Parent_milestonegrid"></div>	
			 </td>
			 <td>
  				<button id="setToSel" type="button" name="setToSel" onclick="VELOS.milestoneGrid.setToSelected('SM');"><%=LC.L_Set_To_Sel%><%--Set to Selected*****--%></button>
        	 </td>			
			</tr>
			<tr style="height:10px"><td></td></tr>
		</table>
		<%} %>
		<table width="98%" cellspacing="0" cellpadding="0" >
		  <tr>
			<td>
			 <td>
				<div id="SM_milestonegrid"></div>
			 </td>
			</td>
		  </tr>
		</table>
	</div>
	</div>
	<!-- End Study Status Milestones --> 	 
	<!-- Start Additional Milestones --> 
	<div id="AdditionalMilestone">
	<h3 onclick="searchMilestones('AM');" id="mileAM"><a href="#" ><%=LC.L_Addl_Mstones%><%--Additional Milestones*****--%></a></h3> <%-- YK(05Aug11):Bug#6759  --%>
	<div >
		<table width="98%" cellspacing="0" cellpadding="0" >
		<tr>
		  <td>
			<%if (isAccessibleFor(pageRight, 'N')||isAccessibleFor(pageRight, 'E')){%> 
  			<FONT class='Mandatory'>(<%=MC.M_ClkRow_ToEdt%><%--Click row to edit*****--%>)</FONT>&nbsp;&nbsp;&nbsp;&nbsp;
			<button type="submit" id="save_changes" name="save_changes" 
	    	onclick="if (f_check_perm_noAlert(<%=pageRight%>,'N') || f_check_perm_noAlert(<%=pageRight%>,'E')) { VELOS.milestoneGrid.saveDialog('AM'); }" ><%=LC.L_Preview_AndSave%></button>
		<%} %>
		  </td>
		
		<%if (isAccessibleFor(pageRight, 'N')){%>
	  <td align="right">
	  		<%=LC.L_Add%><%--Add*****--%> &nbsp;<input id="AM_rowCount" type="text" value="0" size="2" maxlength="2" class="index"
	  		onkeydown = "return validateNumber(event);"/>
	  		<%=LC.L_Add_Mstone_S%><%--Add Milestone(s)*****--%>
			<img src="../images/add.png"/ title="<%=LC.L_Add%>" onclick="VELOS.milestoneGrid.addRows('AM');">    
			<span width="50%">&nbsp;</span>
	  </td>
	  <%} %>
	 </tr>
	</table>
	<!-- Additional Milestone Power Bar Grid - INF-22500 : Raviesh -->
	<%if (isAccessibleFor(pageRight, 'N')||isAccessibleFor(pageRight, 'E')){%> 
		<table width="98%" cellspacing="0" cellpadding="0">
			<tr>
			 <td>
				<div width="90%" id="AM_Parent_milestonegrid"></div>
			 </td>
			 <td>
  				<button id="setToSel" type="button" name="setToSel" onclick="VELOS.milestoneGrid.setToSelected('AM');"><%=LC.L_Set_To_Sel%><%--Set to Selected*****--%></button>
        	 </td>				
			</tr>
			<tr style="height:10px"><td></td></tr>
		</table>
		<%} %>
		<table width="98%" cellspacing="0" cellpadding="0" >
		  <tr>
			<td>
				<div id="AM_milestonegrid"></div>
			</td>
		  </tr>
		</table>
	</div>
	</div>
	<!-- End Additional Milestones --> 
	<!-- Milestone Accordion Ends Here -->
	<%} %>
	<%}else{//page right%>
		<div class=formdefault>
			<BR/>
			<table>
				<tr> 
					<td>
						<p class = "sectionHeadings"><font color="red"> <%=MC.M_UnfortStd_LvlMstone%><%--Unfortunately you do not have <%=LC.Std_Study%> level Milestones access.*****--%> </font></p>
						<p class = "defComments"> <%=MC.M_ObtainRgt_RptErr%><%--To obtain appropriate rights or to report an error, please contact your account administrator.*****--%> <BR></p>
					</td>
				</tr>
			</table>
			<BR>
		</div>
	<%}%>
	</DIV>
</body>
<%}%>
<%}//end of if body for session
	else{%> 
	 <jsp:include page="timeout.html" flush="true" /> 
<%}%>
<div><jsp:include page="bottompanel.jsp" flush="true" /></div>
<div class="mainMenu" id="emenu"><jsp:include page="getmenu.jsp" flush="true" /></div>



</html>
<script>
var screenWidth = screen.width;
var screenHeight = screen.height;
if(screenWidth>1280 || screenHeight>1024){
	$j(".BrowserBotN_S_3").css('height','76%');
	$j(".BrowserBotN").css('height','76%');
}
</script>
<%!
private boolean isAccessibleFor(int pageRight, char type) {
    switch(type) {
    case 'V': return ((pageRight & 0x0004) >> 2) == 1;
    case 'E': return ((pageRight & 0x0002) >> 1) == 1;
    case 'N': return  (pageRight & 0x0001) == 1;
    }
    return false;
}
%>