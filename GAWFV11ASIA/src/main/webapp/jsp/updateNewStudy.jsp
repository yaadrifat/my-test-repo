<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<Link Rel=STYLESHEET HREF="common.css" type=text/css>

</HEAD>
<%!
private static final String DEFAULT_DUMMY_CLASS = "[Config_Study_Interceptor_Class]";
%>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
<jsp:useBean id="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id="siteB" scope="request" class="com.velos.eres.web.site.SiteJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studyRightsB" scope="request" class="com.velos.eres.web.studyRights.StudyRightsJB"/>
<jsp:useBean id="teamB" scope="request" class="com.velos.eres.web.team.TeamJB"/>
<jsp:useBean id="groupB" scope="request" class="com.velos.eres.web.group.GroupJB"/>
<jsp:useBean id="userTeamB" scope="request" class="com.velos.eres.web.team.TeamJB"/>
<jsp:useBean id="userStudyRightsB" scope="request" class="com.velos.eres.web.studyRights.StudyRightsJB"/>
<jsp:useBean id="stdSiteRightsB" scope="request" class="com.velos.eres.web.stdSiteRights.StdSiteRightsJB"/>
<jsp:useBean id="studySiteB" scope="request" class="com.velos.eres.web.studySite.StudySiteJB"/>
<jsp:useBean id="statusB" scope="request" class="com.velos.eres.web.statusHistory.StatusHistoryJB"/>
<jsp:useBean id="studyNIHGrantB"  scope="request" class="com.velos.eres.web.studyNIHGrant.StudyNIHGrantJB"/>
<jsp:useBean id="studyIndIdeB"  scope="request" class="com.velos.eres.web.studyIDEIND.StudyINDIDEJB"/>
<jsp:include page="include.jsp" flush="true"/>
<jsp:include page="sessionwarning.jsp" flush="true"/>

<%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.business.common.*,java.util.*,java.text.*,com.velos.eres.service.util.*,com.velos.eres.web.account.AccountJB,com.velos.esch.business.common.*,com.aithent.audittrail.reports.AuditUtils"%>
<%@page import="com.velos.eres.web.intercept.InterceptorJB" %>
<%

 HttpSession tSession = request.getSession(true);
 String accId = (String) tSession.getValue("accountId");
 int iaccId = EJBUtil.stringToNum(accId);
 String studyId = (String) tSession.getValue("studyId");
 String ipAdd = (String) tSession.getValue("ipAdd");
 String usr = null;
 usr = (String) tSession.getValue("userId");
 int iusr = EJBUtil.stringToNum(usr);

 String src= request.getParameter("srcmenu");

 String selectedTab = request.getParameter("selectedTab");
 boolean isIrb = "irb_init_tab".equals(selectedTab) ? true : false;

String eSign = request.getParameter("eSign");
int success = 0;

 if (sessionmaint.isValidSession(tSession))
   {
	int prinvRole = 0;
  	CodeDao cdRole = new CodeDao();
  	prinvRole = cdRole.getCodeId("role","role_prin");

%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
String oldESign = (String) tSession.getValue("eSign");

	//Added by Manimaran for the July-August Enhancement S4.
	int id=0;
	String moduleId="";
	CodeDao codedao = new CodeDao();
	int codeId=codedao.getCodeId("teamstatus","Active");
	int irbcodeId=codedao.getCodeId("irbstatus","hrec_strt");
	String statusCodeId=codeId+"";
	String irbCodeId=irbcodeId+"";

    String  ecompStatus = request.getParameter("ecompStatus")==null?"":request.getParameter("ecompStatus"); 
    System.out.print("ecompStatus"+ecompStatus);
	Date dt = new java.util.Date();
	String stDate = DateUtil.dateToString(dt);
	String strDate = "";
    Calendar cal = Calendar.getInstance(TimeZone.getDefault());
    
    String DATE_FORMAT = "hh:mm:ss a";
	java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(DATE_FORMAT);       
    String time = sdf.format(cal.getTime());
    if(ecompStatus.equalsIgnoreCase("")){
    	strDate = stDate +" "+ time;
    }
	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
	} else {


   int ret = 0;

   String studyNumber = "" ;

   String studyTitle = "" ;

   String studyObjective = "" ;

   String studySummary = "" ;

   String studyProduct = "" ;

   String studyTArea = "" ;

   //String studySize = "" ;

   String studyDuration = "" ;

   String studyDurationUnit = "";

   String studyEstBgnDate = "" ;

   String studyPhase = "" ;

   String studyResType = "" ;

   String studyType = "" ;

   String studyBlinding = "" ;

   String studyRandom = "" ;

   String studySponsorName = ""; //gopu

   String studySponsorIdInfo="";//gopu

   String studySponsorLkpId = "";

   String studySponsor = "" ;

   String studyContact = "" ;

   String studyOtherInfo = "" ;

   String studyPartCenters = "" ;

   String studyKeywrds = "" ;

   String dataManager = "";

   String mode = "" ;

   String pubCols ="";

   String studyPubFlag ="N";

   String studyCurrency = "";
   String prinInv = "";
   String studyCo="";
   String prinvOther="",nStudySize="";



   String majAuthor="";
   String studyScope="";
   String studyAssoc="";
   String disSite="";
   String cbInv = "";
   String invFlag = "0";
   String indIdeNum = "";
   String studyDivision = "";
   String studyICDcode1 = "";//km
   String studyICDcode2 = "";   
   //#AK:Added for Enhyancment CTRP-20527_1
   String studyFuncdMech="";
   String studyInstCode="";
   String programCode="";
   String serialNumber="";
   String fundMechs[] = null;
   String instCodes[]=null;
   String programCodes[]=null;
   String serialNumbers[]=null;
   String idsNIHGrants[]=null;
   fundMechs=request.getParameterValues("fundMech");
   instCodes=request.getParameterValues("instCode");
   programCodes=request.getParameterValues("programCode");
   serialNumbers=request.getParameterValues("serialNum");
   String rows_existing = request.getParameter("totalExtgRows");
   idsNIHGrants=request.getParameterValues("idsNIHgrant");
   String delStrs = request.getParameter("delStrs");  
   String nihGrantChkValue = request.getParameter("nihGrantChkValue");
   if (StringUtil.isEmpty(nihGrantChkValue))
	   nihGrantChkValue = "0";
    
   /*String studyICDcode3 = "";*/
   String ctrp = ""; //CTRP-20527 22-Nov-2011 @Ankit
   String fda = ""; //INF-22247 27-Feb-2012 @Ankit
   
   String nciTrialIdentifier = "";
   String nctNumber = "";



//Enhancement CTRP-20527 : AGodara

	String [] pKStudyIndIde = null;
	String [] indIdeType = null;
	String [] indIdeNumber = null;
	String [] indIdeGrantors = null;
	String [] indIdeHolders = null;
	String [] indIdeProgramCodes = null;
	String [] indIdeExpandedAccess = null;
	String [] indIdeAccessType = null;
	String [] indIdeExempt = null;

	String cbIndIdeGrid = "";
	String deletedIndIdeRows = null;
	String numIndIdeRows ="";
   
   
   studyNumber = request.getParameter("studyNumber");

   // trim the studyNumber
   if (studyNumber == null ) studyNumber = "";//KM
   studyNumber = studyNumber.trim();



   majAuthor=request.getParameter("author");
   majAuthor=(majAuthor==null)?"":(majAuthor);
   studyScope=request.getParameter("studyScope");
   studyScope=(studyScope==null)?"":(studyScope);

   studyAssoc=request.getParameter("studyAssoc");
   studyAssoc=(studyAssoc==null)?"":(studyAssoc);
   disSite=request.getParameter("disSiteid");
   disSite=(disSite==null)?"":(disSite);
   cbInv = request.getParameter("cbInv");
   studyICDcode1 = request.getParameter("ICDcode1");//km
   studyICDcode2 = request.getParameter("ICDcode2");
   /*studyICDcode3 = request.getParameter("ICDcode3");*/
   ctrp = request.getParameter("ctrp");
   fda = request.getParameter("fda");//INF-22247 27-Feb-2012 @Ankit
   if (EJBUtil.isEmpty(cbInv))
   {
      invFlag = "0";
	  }
   else if(cbInv.equals("on"))
   {
	   invFlag = "1";
   }
   
   if (EJBUtil.isEmpty(ctrp))
   {
	   ctrp = "0";
   }
   else if(ctrp.equals("on"))
   {
	   ctrp = "1";
   }
   //INF-22247 27-Feb-2012 @Ankit
   if (EJBUtil.isEmpty(fda))
   {
	   fda = "0";
   }
   else if(fda.equals("on"))
   {
	   fda = "1";
   }

   indIdeNum = request.getParameter("indIdeNum");

   studyDivision = request.getParameter("studyDivision");

   studyTitle = request.getParameter("studyTitle");

   if (studyTitle == null) studyTitle =""; //KM

   if (studyTitle.length() > 1000)
   {
    	studyTitle = studyTitle.substring(0,1000);
	}



   nciTrialIdentifier = request.getParameter("NCITrialIdentifier");
   nciTrialIdentifier = ( nciTrialIdentifier==null)?"": nciTrialIdentifier;
   
   nctNumber = request.getParameter("nctNumber");
   nctNumber = ( nctNumber==null)?"": nctNumber;
   
   

   studyObjective = request.getParameter("studyObjective");
   studyObjective = (   studyObjective  == null      )?"":(  studyObjective ) ;

   //Modified by Manimaran to fix the Bug2741
   /*if (studyObjective.length() > 2000)
   {
    	studyObjective = studyObjective.substring(0,2000);
	}
   */


   studySummary = request.getParameter("studySummary");
   studySummary = (   studySummary  == null      )?"":(  studySummary ) ;
   /*if (studySummary.length() > 2000)
   {
    	studySummary = studySummary.substring(0,2000);
	}
    */

   studyProduct = request.getParameter("studyProduct");
   studyProduct = (   studyProduct  == null      )?"":(  studyProduct ) ;

   studyTArea = request.getParameter("studyTArea");

   //studySize = request.getParameter("studySize");
  // studySize=(studySize==null)?"0":studySize;
  // studySize=(studySize.length()==0)?"0":studySize;
   studyDuration = request.getParameter("studyDuration");
   studyDuration=(studyDuration==null)?"":studyDuration;
  studyDuration=(studyDuration.length()==0)?"0":studyDuration;

   studyDurationUnit = request.getParameter("durationUnit");

   studyEstBgnDate = request.getParameter("studyEstBgnDate");

   studyPhase = request.getParameter("studyPhase");

   studyResType = request.getParameter("studyResType");

   studyType = request.getParameter("studyType");

   studySponsorName = request.getParameter("sponsor");
   studySponsorName = (studySponsorName==null)?"":studySponsorName;

   //JM: 26Apr2010: Enh:SW-FIN6
   studySponsorLkpId = request.getParameter("sponsorpk");
   studySponsorLkpId = (studySponsorLkpId==null)?"":studySponsorLkpId;

   String fromSponsor = request.getParameter("fromSponsor");
   fromSponsor = ( fromSponsor==null)?"": fromSponsor;


   if (fromSponsor.equals("DD")){

   }else if (fromSponsor.equals("LKP")){
   	studySponsorName = studySponsorLkpId;
   }


   studySponsorIdInfo = request.getParameter("studySponsorIdInfo"); //gopu
   studySponsorIdInfo = (   studySponsorIdInfo  == null      )?"":(  studySponsorIdInfo ) ;





   studyBlinding = request.getParameter("studyBlinding");

   studyRandom = request.getParameter("studyRandom");

   studySponsor = request.getParameter("studySponsor");
    studySponsor = (   studySponsor  == null      )?"":(  studySponsor ) ;


   studyContact = request.getParameter("studyContact");
   studyContact = (   studyContact  == null      )?"":(  studyContact ) ;

   studyOtherInfo = request.getParameter("studyOtherInfo");
    studyOtherInfo = (   studyOtherInfo  == null      )?"":(  studyOtherInfo ) ;
   if (studyOtherInfo.length() > 2000)
   {
    	studyOtherInfo = studyOtherInfo.substring(0,2000);
	}

   /* studyPartCenters = request.getParameter("studyPartCenters");
    studyPartCenters = (   studyPartCenters  == null      )?"":(  studyPartCenters ) ;
   if (studyPartCenters.length() > 1000)
   {
    	studyPartCenters = studyPartCenters.substring(0,1000);
	}

	*/

	studyKeywrds = request.getParameter("studyKeywrds");
   studyKeywrds = (   studyKeywrds  == null      )?"":(  studyKeywrds ) ;
   if (studyKeywrds.length() > 500)
   {
    	studyKeywrds = studyKeywrds.substring(0,500);
	}

   dataManager = request.getParameter("dataManager");
   prinInv = request.getParameter("prinInv");



   studyCo=request.getParameter("studyco");
   if (studyCo.length()==0) studyCo=null;
   int studyCoNum=EJBUtil.stringToNum(studyCo);
   prinvOther=request.getParameter("prinvOther");
   nStudySize=request.getParameter("nStudySize");
   nStudySize=(nStudySize==null)?"0":nStudySize;
   nStudySize=(nStudySize.length()==0)?"0":nStudySize;

   mode = request.getParameter("mode");

   int totSections = EJBUtil.stringToNum(request.getParameter("totPubSections"));

   for(int cnt=1; cnt <= totSections; cnt++){

	String pubcol = "studyPubFlag" +cnt;

	pubCols = pubCols + request.getParameter(pubcol);

   }

%>

<%

if(StringUtil.isEmpty(studyPhase)) studyPhase = null;

if(StringUtil.isEmpty(studyResType)) studyResType = null;

if(StringUtil.isEmpty(studyType))  studyType = null;

if(StringUtil.isEmpty(studyBlinding)) studyBlinding = null;

if(StringUtil.isEmpty(studyRandom)) studyRandom = null;


SchCodeDao scd = new SchCodeDao();

int curPk = 0;

int dtManager = EJBUtil.stringToNum(dataManager);

int prinUser = EJBUtil.stringToNum(prinInv);



String userList = "";

     	if(dtManager != prinUser && (prinUser != 0))
     		userList = dtManager +","+ prinUser;

     	else
     		userList = String.valueOf(dtManager);
	if ((studyCoNum!=0) && (studyCoNum!=prinUser) && (studyCoNum!=dtManager))
	 userList=userList+","+studyCoNum;

     UserDao usrD = new UserDao();
     usrD = userB.getUsersDetails(userList);
     //ArrayList arrSites = usrD.getUsrSiteIds();
	  ArrayList aSites = usrD.getUsrSiteIds();
     ArrayList arrSites = new ArrayList();
  	 for(int i=0;i<aSites.size();i++)
	 {
	  if(i==0){
	  	  arrSites.add(aSites.get(i).toString());
	  }
	  if(!arrSites.contains(aSites.get(i).toString()))
	    arrSites.add(aSites.get(i).toString());

	 }

if (studyCurrency.equals(""))
{
 curPk = scd.getCodeId("currency","$");
 studyCurrency = String.valueOf(curPk);

}

%>

<BR>


<BR>

<%

if (mode.equals("M")||mode.equals("S"))
{
	int rows = 0;
	 StudySiteDao studySiteDao = new StudySiteDao();
	 String addSite = "";
	 for(int cnt =0 ; cnt < arrSites.size() ; cnt++){

	 	  addSite = (arrSites.get(cnt)).toString();

	 studySiteDao = studySiteB.getCntForSiteInStudy(EJBUtil.stringToNum(studyId), EJBUtil.stringToNum(addSite));
	 rows = studySiteDao.getCRows();

	 // add the organization in studysites if this organization is not present for this study in er_studysites
	 if(rows == 0 ){
	 	        studySiteB.setSiteId(addSite);
	     		studySiteB.setStudyId(studyId);
	     		studySiteB.setCreator(usr);
			studySiteB.setRepInclude("1");
    	 		studySiteB.setIpAdd(ipAdd);
    	 		studySiteB.setStudySiteDetails();
	  }
	 }
	 int usrCnt = 0;
	 TeamDao teamDao = new TeamDao();
	 // add user study site rights if the user was not in the study team earlier
	 	 	if(dtManager != prinUser && (prinUser != 0)){
	 	    teamDao  = teamB.findUserInTeam(EJBUtil.stringToNum(studyId), dtManager);
	 	    usrCnt = teamDao.getCRows();

	 	  	if(usrCnt == 0)
	 		{
	 	 		stdSiteRightsB.createStudySiteRightsData(dtManager, iaccId, EJBUtil.stringToNum(studyId), iusr, ipAdd );
	 		}
	 		teamDao = teamB.findUserInTeam(EJBUtil.stringToNum(studyId), prinUser);
	 		usrCnt = teamDao.getCRows();

	 		if(usrCnt == 0)
	 		{
	 			//add PI since there is no record in er_studyteam//////////////////

				 	teamB.setTeamUser(String.valueOf(prinUser));
			     	teamB.setStudyId(studyId);
					teamB.setTeamUserType("D");
			     	teamB.setTeamUserRole(String.valueOf(prinvRole));
			     	teamB.setCreator(usr);
			     	teamB.setIpAdd(ipAdd);

				//Added by Manimaran for the July-August Enhancement S4.
				teamB.setTeamStatus("Active");
				teamB.setTeamDetails();

				statusB.setStatusModuleId(String.valueOf(teamB.getId()));
				statusB.setStatusModuleTable("er_studyteam");
				statusB.setStatusCodelstId(statusCodeId);
				statusB.setStatusStartDate(stDate);
				statusB.setCreator(usr);
				statusB.setIpAdd(ipAdd);
				statusB.setModifiedBy(usr);
				statusB.setRecordType("N");
				statusB.setIsCurrentStat("1");
				statusB.setStatusHistoryDetailsWithEndDate();

	 			///////////////////////////////////
	 	 		stdSiteRightsB.createStudySiteRightsData(prinUser, iaccId, EJBUtil.stringToNum(studyId), iusr, ipAdd );
	 		}

	   }
	   else if (dtManager == prinUser || prinUser == 0)
	   {
	   	  teamDao = teamB.findUserInTeam(EJBUtil.stringToNum(studyId), dtManager);
	   	  usrCnt = teamDao.getCRows();
	 	  	if(usrCnt == 0)
	 		{
	 	 		stdSiteRightsB.createStudySiteRightsData(dtManager, iaccId, EJBUtil.stringToNum(studyId), iusr, ipAdd );
	 		}

	   }


	studyB.setId(EJBUtil.stringToNum(studyId));
	studyB.getStudyDetails();
	//Retrieve the previous values for
	//these parameter, Studytea, is refreshed based
	// on , if they are changed using filters
	//defined in super user rights page.
	String prevTarea=studyB.getStudyTArea();
	String prevDisSite=studyB.getDisSite();
	String prevStudyDivision=studyB.getStudyDivision();

	if (StringUtil.isEmpty(prevTarea))
		prevTarea = "";

	if (StringUtil.isEmpty(prevDisSite))
		prevDisSite = "";

	if (StringUtil.isEmpty(prevStudyDivision))
		prevStudyDivision = "";

	if (StringUtil.isEmpty(studyDivision))
		studyDivision = "";

	if (StringUtil.isEmpty(disSite))
		disSite = "";

	if (StringUtil.isEmpty(studyTArea))
		studyTArea = "";

	//
	studyB.setStudyNumber(studyNumber);
	studyB.setStudyTitle(StringUtil.stripScript(studyTitle));
	studyB.setStudyObjective(studyObjective);
	studyB.setStudySummary(studySummary);
	studyB.setStudyProduct(studyProduct);
	studyB.setStudyTArea(studyTArea);
	//studyB.setStudySize(studySize);
	studyB.setStudyDuration(studyDuration);
	studyB.setStudyDurationUnit(studyDurationUnit);
	studyB.setStudyEstBeginDate(studyEstBgnDate);
	studyB.setStudyPhase(studyPhase);
	studyB.setStudyResType(studyResType);
	studyB.setStudyType(studyType);
	studyB.setStudyBlinding(studyBlinding);
	studyB.setStudyRandom(studyRandom);
	studyB.setStudyInfo(studyOtherInfo);
	studyB.setStudySponsorName(studySponsorName);//gopu
	studyB.setStudySponsorIdInfo(studySponsorIdInfo); //gopu

	//studyB.setStudyPartCenters(studyPartCenters);
	studyB.setStudyPrimInv(prinInv);
	studyB.setStudyCoordinator(studyCo);
	studyB.setStudyKeywords(studyKeywrds);
	studyB.setStudyContact(studyContact);
	studyB.setStudySponsor(studySponsor);
	studyB.setStudyAuthor(dataManager);
	studyB.setStudyPubCol(pubCols);
	studyB.setStudyPubFlag(studyPubFlag);
	studyB.setModifiedBy(usr);
	studyB.setIpAdd(ipAdd);
	studyB.setStudyCurrency(studyCurrency);
	studyB.setMajAuthor(majAuthor);
	studyB.setStudyScope(studyScope);
	studyB.setStudyAssoc(studyAssoc);
	studyB.setDisSite(disSite);
	studyB.setStudyOtherPrinv(prinvOther);
	studyB.setStudyNSamplSize(nStudySize);
	studyB.setStudyDivision(studyDivision);
	studyB.setStudyInvIndIdeFlag(invFlag);
	studyB.setStudyIndIdeNum(indIdeNum);
	studyB.setStudyICDCode1(studyICDcode1);//km
	studyB.setStudyICDCode2(studyICDcode2);
	/*studyB.setStudyICDCode3(studyICDcode3);*/
	studyB.setStudyCtrpReportable(ctrp);
	studyB.setFdaRegulatedStudy(fda);//INF-22247 27-Feb-2012 @Ankit
	
	studyB.setNciTrialIdentifier(nciTrialIdentifier);
	studyB.setNctNumber(nctNumber);



	success = studyB.updateStudy();
	
	// Start CTRP-20527(IND/IDE Info)  :AGodara 

	if(success != -3 && request.getParameter("cbIndIdeGrid")!=null){
		// number of data rows
			numIndIdeRows = request.getParameter("numIndIdeRows");
			
			pKStudyIndIde = request.getParameterValues("pKStudyIndIde");
			indIdeType = request.getParameterValues("indIdeType");
			indIdeNumber = request.getParameterValues("indIdeNumber");
			indIdeGrantors = request.getParameterValues("indIdeGrantor");
			indIdeHolders = request.getParameterValues("indIdeHolder");
			indIdeProgramCodes = request.getParameterValues("indIdeProgramCode");
			
			//chkbx
			indIdeExpandedAccess = request.getParameterValues("cbExpandedAccessFlag");
			
			indIdeAccessType = request.getParameterValues("indIdeAccessType");
			
			//chkbx
			indIdeExempt = request.getParameterValues("cbIndIdeExemptFlag");
			
			deletedIndIdeRows = request.getParameter("deletedIndIdeRows");
				
			if(deletedIndIdeRows!=null && !deletedIndIdeRows.isEmpty()){
				for(String s:deletedIndIdeRows.split(",")){
					if(!s.isEmpty() && !s.equals("0"))
						studyIndIdeB.deleteIndIdeInfo(StringUtil.stringToNum(s),AuditUtils.createArgs(session,"",LC.L_Study));
				}
			}
						
			for(int i=0;i< StringUtil.stringToNum(numIndIdeRows);i++){
				
				studyIndIdeB.setFkStudy(StringUtil.integerToString(studyB.getId()));
				
				studyIndIdeB.setTypeIndIde(indIdeType[i]);
				studyIndIdeB.setNumberIndIde(indIdeNumber[i]);
				studyIndIdeB.setFkIndIdeGrantor(indIdeGrantors[i]);
				studyIndIdeB.setFkIndIdeHolder(indIdeHolders[i]);
				studyIndIdeB.setFkProgramCode(indIdeProgramCodes[i]);
				studyIndIdeB.setExpandIndIdeAccess(indIdeExpandedAccess[i]);
				studyIndIdeB.setFkAccessType(indIdeAccessType[i]);
				studyIndIdeB.setExemptINDIDE(indIdeExempt[i]);
				studyIndIdeB.setIpAdd(ipAdd);
				
				if( pKStudyIndIde[i]!= null && StringUtil.stringToNum(pKStudyIndIde[i])!=0 ){
					studyIndIdeB.setPkStudyINDIDE(StringUtil.stringToNum(pKStudyIndIde[i]));
					studyIndIdeB.setModifiedBy(usr);
					studyIndIdeB.updateIndIdeInfo();
				}else{
					studyIndIdeB.setModifiedBy(null);      //Added to fix Bug#7994 and 7960 : Raviesh
					studyIndIdeB.setCreator(usr);
					studyIndIdeB.setIndIdeInfo();
					
				}
			}
	}
	// End CTRP-20527(IND/IDE Info)  :AGodara 

	// first add its organization if it is not in er_studysite for this study

	// Then add users access rights in er_study_site_rights

	//add in stduy team
	//Coordinator role
	String coRole="";
	CodeDao cdCoRole=new CodeDao();
	coRole=String.valueOf(cdCoRole.getCodeId("role","role_coord"));

	if (studyCoNum>0){
	usrCnt = 0;
	 teamDao = new TeamDao();
	teamDao  = teamB.findUserInTeam(EJBUtil.stringToNum(studyId), studyCoNum);
     	usrCnt=teamDao.getCRows();
	if (usrCnt==0)
	{
	teamB.setTeamUser(studyCo);
     	teamB.setStudyId(studyId);
	teamB.setTeamUserType("D");
     	teamB.setTeamUserRole(coRole);
     	teamB.setCreator(usr);
     	teamB.setIpAdd(ipAdd);

	//Added by Manimaran for the July-August Enhancement S4.
	teamB.setTeamStatus("Active");
	teamB.setTeamDetails();
	id = teamB.getId();
	moduleId=id+"";
	statusB.setStatusModuleId(moduleId);
	statusB.setStatusModuleTable("er_studyteam");
	statusB.setStatusCodelstId(statusCodeId);
	statusB.setStatusStartDate(stDate);
	statusB.setCreator(usr);
	statusB.setIpAdd(ipAdd);
	statusB.setModifiedBy(usr);
	statusB.setRecordType("N");
	statusB.setIsCurrentStat("1");
	statusB.setStatusHistoryDetailsWithEndDate();
	stdSiteRightsB.createStudySiteRightsData(studyCoNum, iaccId, EJBUtil.stringToNum(studyId), iusr, ipAdd );


	}
	}

	/* commented by sonia, 10/22/07. changed the implementation of superuser
	 	if (success>=0)
		{

		if ((!prevTarea.equals(studyTArea)) || (!prevStudyDivision.equals(studyDivision))
		|| (!prevDisSite.equals(disSite)))
		{
		  GroupDao gDao=groupB.getGroupDaoInstance();
		 success=gDao.revokeSupUserForStudy(EJBUtil.stringToNum(studyId),accId);
		  if (success>=0) ret=gDao.grantSupUserForStudy(accId , studyId , usr ,ipAdd);

		 }
		}
		*/




	if(success == -3) {

%>
  <jsp:include page="studynumexists.jsp" flush="true"/>
<%
	} else {
			tSession.putValue("studyTArea",studyTArea);
	}

}

else

{
	studyB.setAccount(accId);
	studyB.setStudyNumber(studyNumber);
	studyB.setStudyTitle(studyTitle);
	studyB.setStudyObjective(studyObjective);
	studyB.setStudySummary(studySummary);
	studyB.setStudyProduct(studyProduct);
	studyB.setStudyTArea(studyTArea);
	//studyB.setStudySize(studySize);
	studyB.setStudyDuration(studyDuration);
	studyB.setStudyDurationUnit(studyDurationUnit);
	studyB.setStudyEstBeginDate(studyEstBgnDate);
	studyB.setStudyPhase(studyPhase);
	studyB.setStudyResType(studyResType);
	studyB.setStudyType(studyType);
	studyB.setStudyBlinding(studyBlinding);
	studyB.setStudyRandom(studyRandom);
	studyB.setStudyInfo(studyOtherInfo);
	studyB.setStudySponsorName(studySponsorName); //gopu
	studyB.setStudySponsorIdInfo(studySponsorIdInfo);//gopu

	//studyB.setStudyPartCenters(studyPartCenters);

	studyB.setStudyKeywords(studyKeywrds);
	studyB.setStudyContact(studyContact);
	studyB.setStudySponsor(studySponsor);
	studyB.setStudyAuthor(dataManager);
	studyB.setStudyPubCol(pubCols);
	studyB.setStudyPubFlag(studyPubFlag);
	studyB.setCreator(usr);
	studyB.setIpAdd(ipAdd);
    studyB.setStudyCurrency(studyCurrency);
    studyB.setMajAuthor(majAuthor);
    studyB.setStudyScope(studyScope);
    studyB.setStudyAssoc(studyAssoc);
    studyB.setDisSite(disSite);
    studyB.setStudyPrimInv(prinInv);
    studyB.setStudyCoordinator(studyCo);
	studyB.setStudyOtherPrinv(prinvOther);
	studyB.setStudyNSamplSize(nStudySize);
    studyB.setStudyDivision(studyDivision);
	studyB.setStudyInvIndIdeFlag(invFlag);
	studyB.setStudyIndIdeNum(indIdeNum);
	studyB.setStudyICDCode1(studyICDcode1);//km
	studyB.setStudyICDCode2(studyICDcode2);
	/*studyB.setStudyICDCode3(studyICDcode3);*/
	studyB.setStudyCreationType(isIrb?"A":"D");
	studyB.setStudyCtrpReportable(ctrp);
	studyB.setFdaRegulatedStudy(fda); // Bug#10130 18-June-2012 @Ankit
	
	studyB.setNciTrialIdentifier(nciTrialIdentifier);
	studyB.setNctNumber(nctNumber);
	
	success = studyB.setStudyDetails();


	// START Enhancement CTRP-20527 : AGodara
	if(success != -3 && request.getParameter("cbIndIdeGrid")!=null){
	
			// number of data rows
			numIndIdeRows = request.getParameter("numIndIdeRows");
			indIdeType = request.getParameterValues("indIdeType");
			indIdeNumber = request.getParameterValues("indIdeNumber");
			indIdeGrantors = request.getParameterValues("indIdeGrantor");
			indIdeHolders = request.getParameterValues("indIdeHolder");
			indIdeProgramCodes = request.getParameterValues("indIdeProgramCode");
			
			//chkbx
			indIdeExpandedAccess = request.getParameterValues("cbExpandedAccessFlag");
			
			indIdeAccessType = request.getParameterValues("indIdeAccessType");
			
			//chkbx
			indIdeExempt = request.getParameterValues("cbIndIdeExemptFlag");
			for(int i=0;i<StringUtil.stringToNum(numIndIdeRows);i++){
				
				studyIndIdeB.setFkStudy(StringUtil.integerToString(studyB.getId()));
				studyIndIdeB.setTypeIndIde(indIdeType[i]);
				studyIndIdeB.setNumberIndIde(indIdeNumber[i]);
				studyIndIdeB.setFkIndIdeGrantor(indIdeGrantors[i]);
				studyIndIdeB.setFkIndIdeHolder(indIdeHolders[i]);
				studyIndIdeB.setFkProgramCode(indIdeProgramCodes[i]);
				studyIndIdeB.setExpandIndIdeAccess(indIdeExpandedAccess[i]);
				studyIndIdeB.setFkAccessType(indIdeAccessType[i]);
				studyIndIdeB.setExemptINDIDE(indIdeExempt[i]);
				studyIndIdeB.setCreator(usr);
				studyIndIdeB.setIpAdd(ipAdd);
				studyIndIdeB.setIndIdeInfo();
				
			}
		
	}
	
	
	
	// END Enhancement CTRP-20527 : AGodara
	
	
	
	if(success == -3) {

	%>

	<jsp:include page="studynumexists.jsp" flush="true"/>
	<%
	} else
	{
     	/*CtrlDao ctrl = new CtrlDao();
     	ctrl.getControlValues("author");
     	String role = (String) ctrl.getCValue().get(0);*/
     	int dmRole = 0;
     	CodeDao cdDMRole = new CodeDao();
     	dmRole = cdDMRole.getCodeId("role","role_dm");

     	String dmRoleStr = String.valueOf(dmRole);




     	int uteamId = 0;
     	//add logged in user to study team if he selects a different data manager
    //	int dtManager = EJBUtil.stringToNum(dataManager);
     	int curUser = EJBUtil.stringToNum(usr);
     //	int prinUser = EJBUtil.stringToNum(prinInv);

     	String finalRole = "";

     	if (dtManager == prinUser)
     	{
     		finalRole = String.valueOf(prinvRole);
     	}
     	else
     	{
     		finalRole = dmRoleStr;
     	}


     	studyId = (new Integer(studyB.getId())).toString();

     	tSession.putValue("studyId",studyId);
     	tSession.putValue("studyTArea",studyTArea);

     	teamB.setTeamUser(dataManager);
     	teamB.setStudyId(studyId);
		teamB.setTeamUserType("D");
     	teamB.setTeamUserRole(finalRole);
     	teamB.setTeamStatus("Active");//km
	teamB.setCreator(usr);
     	teamB.setIpAdd(ipAdd);
	teamB.setTeamDetails();

	//Added by Manimaran for the July-August Enhancement S4.
	id = teamB.getId();
	moduleId=id+"";
	statusB.setStatusModuleId(moduleId);
	statusB.setStatusModuleTable("er_studyteam");
	statusB.setStatusCodelstId(statusCodeId);
	statusB.setStatusStartDate(stDate);
	statusB.setCreator(usr);
	statusB.setIpAdd(ipAdd);
	statusB.setModifiedBy(usr);
	statusB.setRecordType("N");
	statusB.setIsCurrentStat("1");
	statusB.setStatusHistoryDetailsWithEndDate();
	stdSiteRightsB.createStudySiteRightsData(dtManager, iaccId, EJBUtil.stringToNum(studyId), iusr, ipAdd );

     	if ((dtManager != prinUser) && (prinUser != 0))
     	{
	     	teamB.setTeamUser(prinInv);
	     	teamB.setStudyId(studyId);
		teamB.setTeamUserType("D");
     		teamB.setTeamUserRole(String.valueOf(prinvRole));
		teamB.setTeamStatus("Active");//km
	     	teamB.setCreator(usr);
    	 	teamB.setIpAdd(ipAdd);
     		teamB.setTeamDetails();

	//Added by Manimaran for the July-August Enhancement S4.
	id = teamB.getId();
	moduleId=id+"";
	statusB.setStatusModuleId(moduleId);
	statusB.setStatusModuleTable("er_studyteam");
	statusB.setStatusCodelstId(statusCodeId);
	statusB.setStatusStartDate(stDate);
	statusB.setCreator(usr);
	statusB.setIpAdd(ipAdd);
	statusB.setModifiedBy(usr);
	statusB.setRecordType("N");
	statusB.setIsCurrentStat("1");
	statusB.setStatusHistoryDetailsWithEndDate();
	stdSiteRightsB.createStudySiteRightsData(prinUser, iaccId, EJBUtil.stringToNum(studyId), iusr, ipAdd );
     	}

	//Coordinator role
	String coRole="";
	CodeDao cdCoRole=new CodeDao();
	coRole=String.valueOf(cdCoRole.getCodeId("role","role_coord"));


	if (studyCoNum>0){
	int usrCnt = 0;
	 TeamDao teamDao = new TeamDao();
	teamDao  = teamB.findUserInTeam(EJBUtil.stringToNum(studyId), studyCoNum);
     	usrCnt=teamDao.getCRows();
	if (usrCnt==0)
	{
	teamB.setTeamUser(studyCo);
     	teamB.setStudyId(studyId);
	teamB.setTeamUserType("D");
     	teamB.setTeamUserRole(coRole);
	teamB.setTeamStatus("Active");//km
     	teamB.setCreator(usr);
     	teamB.setIpAdd(ipAdd);
     	teamB.setTeamDetails();

	//Added by Manimaran for the July-August Enhancement S4.
	id = teamB.getId();
	moduleId=id+"";
	statusB.setStatusModuleId(moduleId);
	statusB.setStatusModuleTable("er_studyteam");
	statusB.setStatusCodelstId(statusCodeId);
	statusB.setStatusStartDate(stDate);
	statusB.setCreator(usr);
	statusB.setIpAdd(ipAdd);
	statusB.setModifiedBy(usr);
	statusB.setRecordType("N");
	statusB.setIsCurrentStat("1");
	statusB.setStatusHistoryDetailsWithEndDate();
	stdSiteRightsB.createStudySiteRightsData(studyCoNum, iaccId, EJBUtil.stringToNum(studyId), iusr, ipAdd );
	}
	}
     /*	String userList = "";


     	if(dtManager != prinUser && (prinUser != 0))
     		userList = dtManager +","+ prinUser;

     	else
     		userList = String.valueOf(dtManager); */


     	/*UserDao usrD = new UserDao();
     	usrD = userB.getUsersDetails(userList);
     	ArrayList arrSites = usrD.getUsrSiteIds();*/
      	String siteId1 = "";
     	String siteId = "";
     /*	if(arrSites.size() == 2 && ((arrSites.get(0).toString()).equals(arrSites.get(1).toString())))
     	{

	     		siteId1 = arrSites.get(0).toString();
	     		studySiteB.setSiteId(siteId1);
	     		studySiteB.setStudyId(studyId);
	     		studySiteB.setCreator(usr);
    	 		studySiteB.setIpAdd(ipAdd);
    	 		studySiteB.setStudySiteDetails();
	    }
	    else{*/

	    for(int i =0 ; i<arrSites.size(); i++)
	    	{

	    		siteId = arrSites.get(i).toString();
	    		if(!siteId.equals("0")){

	     		studySiteB.setSiteId(siteId);
	     		studySiteB.setStudyId(studyId);
	     		studySiteB.setCreator(usr);
    	 		studySiteB.setIpAdd(ipAdd);

			//KM-to fix the Bug 2543 Organiation added by default while creating Study doesn't have 'Include in Reports' checked.
			studySiteB.setRepInclude("1");
    	 		studySiteB.setStudySiteDetails();
    	 		}
	    	}
	   // }

     	int teamId = teamB.getId();

	/* commented by sonia 10/22/07, changed the super user implementation
		GroupDao gDao=groupB.getGroupDaoInstance();
		 gDao.grantSupUserForStudy(accId , studyId , usr ,ipAdd);
	*/


  } //end of else for study number exists check
} //end of if for NEW mode

//Ak:Added For enhancement CTRP-20527_1
if( success>=0 && nihGrantChkValue.equals("1")){
	String studyNIHgrantId="";
	Integer totalRows= EJBUtil.stringToNum(rows_existing);
	totalRows=totalRows>0?totalRows:0;
	//Add/Update functionality of NIH grant records
	 for (int j=0 ; j < totalRows ;j ++){
		studyNIHgrantId=(String) idsNIHGrants[j];
		studyNIHgrantId = (studyNIHgrantId==null)?"":studyNIHgrantId;		
		studyFuncdMech=fundMechs[j];
		studyInstCode=instCodes[j];
		programCode=programCodes[j];
		serialNumber=serialNumbers[j].trim();
		studyNIHGrantB.setStudyNIHGrantId(EJBUtil.stringToNum(studyNIHgrantId));
		if(EJBUtil.stringToNum(studyNIHgrantId)>0){
		studyNIHGrantB.getStudyNIHGrantInfo();
		}
		studyNIHGrantB.setFundMech(studyFuncdMech);
		studyNIHGrantB.setInstCode(studyInstCode);
		studyNIHGrantB.setStudyId(studyId);
		studyNIHGrantB.setProgramCode(programCode);
		studyNIHGrantB.setNihGrantSerial(serialNumber);
		studyNIHGrantB.setIpAdd(ipAdd);
		if(EJBUtil.stringToNum(studyNIHgrantId)>0){ 
			studyNIHGrantB.setModifiedBy(usr);
			studyNIHGrantB.updateStudyNIHGrant();
		}else{
			studyNIHGrantB.setModifiedBy(null);        //Added to fix Bug#7994 and 7960: Raviesh
			studyNIHGrantB.setCreator(usr);
			studyNIHGrantB.setStudyNIHGrantInfo();
		
		}	
		
	}
	 //Delete the NIH Records
	StringTokenizer idStrgs=new StringTokenizer(delStrs,",");
	int Ids=idStrgs.countTokens();
	String NIHgrantId="";
	for(int cnt=0;cnt<Ids;cnt++)
	{
		NIHgrantId = idStrgs.nextToken();
		studyNIHGrantB.deleteStudyNIHGrantRecord(EJBUtil.stringToNum(NIHgrantId), AuditUtils.createArgs(session,"",LC.L_Study));
	}//End here deletion functionality	 

}
//added for Epworth Eresearch Researcher Customizations

if(success>=0){
	if(isIrb){
		if(ecompStatus.equalsIgnoreCase("")){
        statusB.setStatusModuleId(studyId);
		statusB.setStatusModuleTable("er_study");
		statusB.setStatusCodelstId(irbCodeId);
		statusB.setStatusStartDate(stDate);
		statusB.setCreator(usr);
		statusB.setIpAdd(ipAdd);
		statusB.setModifiedBy(usr);
		statusB.setRecordType("N");
		statusB.setIsCurrentStat("1");
		statusB.setTimeStamp(strDate);
		statusB.setStatusHistoryDetailsWithEndDate();
		}
		
	}
	
}

if(success >=0 )
 {
	// -- Add a hook for customization for after a study update succeeds
	try {
		if (!StringUtil.isEmpty(LC.Config_Study_Interceptor_Class) &&
				!DEFAULT_DUMMY_CLASS.equals(LC.Config_Study_Interceptor_Class)) {
			Class clazz = Class.forName(LC.Config_Study_Interceptor_Class);
			Object obj = clazz.newInstance();
			Object[] args = new Object[2];
			args[0] = (Object)request;
			args[1] = (Integer)success;
			((InterceptorJB) obj).handle(args);
		}
	} catch(Exception e) {
		e.printStackTrace();
	}
	// -- End of hook
%>
  <br>
  <Br>
  <br>
  <Br>
  <p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%-- Data was saved successfully*****--%> </p>
  <%
  String nextUrl = isIrb ? "irbnewinit.jsp?irbFlag=Y" : "study.jsp?irbFlag=N";
  if(mode.equals("S")){%>
  
  <META HTTP-EQUIV=Refresh CONTENT="1; URL=<%=nextUrl%>&mode=S&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&tabFlag=Y&studyId=<%=studyId%>">
  <% }else{%>

  <META HTTP-EQUIV=Refresh CONTENT="1; URL=<%=nextUrl%>&mode=M&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>">
<%}
}// end of if body for success
}// end of if body for e-sign
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
<div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</BODY>
</HTML>