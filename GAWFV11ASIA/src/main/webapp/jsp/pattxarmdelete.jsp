<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@page import="com.aithent.audittrail.reports.AuditUtils,com.velos.eres.service.util.LC"%>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=MC.M_Del_PatsTreatArm%><%--Delete <%=LC.Pat_Patient%>'s Treatment Arm*****--%></title>
<jsp:include page="popupJS.js" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT>
function  validate(formobj){
	var fdaRegulated = document.getElementById("FDARegulated").value;
	var reasonDel = document.getElementById("reason_del").value;
	reasonDel=reasonDel.replace(/^\s+|\s+$/g,"");
	if(fdaRegulated=="1" && reasonDel.length<=0)
	{
		alert("<%=MC.M_Etr_MandantoryFlds%>");
		document.getElementById("reason_del").focus();
		return false;
			
	}
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	<%-- if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   } --%>
}
</SCRIPT>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>
<jsp:useBean id="pattxarmJB" scope="request" class="com.velos.eres.web.patTXArm.PatTXArmJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/> 
<jsp:include page="include.jsp" flush="true"/>
<BODY> 
<br>

<DIV class="popDefault" id="div1"> 
<% 
	String pattxArmId= "";
	

HttpSession tSession = request.getSession(true); 
 if (sessionmaint.isValidSession(tSession))	{ 	
		pattxArmId= request.getParameter("pattxArmId");
		
		String studyId=request.getParameter("studyId");
		studyId = studyId==null?"0":studyId;
		String fdaRegulated="";
		studyB.setId(EJBUtil.stringToNum(studyId));
	    	studyB.getStudyDetails();
	    	fdaRegulated=studyB.getFdaRegulatedStudy();
	    	fdaRegulated = fdaRegulated==null?"0":fdaRegulated;
	    	
		int ret=0;
		String delMode=request.getParameter("delMode");
	
		if (EJBUtil.isEmpty(delMode)) {
			delMode="final";
%>
  
	<FORM name="pattxarmdelete"  id="pattrtarmdel" method="post" action="pattxarmdelete.jsp" onSubmit="if (validate(document.pattxarmdelete)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	
	<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
	<tr>
	<td align="right" valign="top" width="50%"><%=LC.L_Reason_ForDelete %>
	<%if(fdaRegulated.equals("1")) {%>
	<font class="Mandatory">*</font>
	<%} %>&nbsp;
	</td>
	<Input type="hidden" name="FDARegulated" id="FDARegulated" value="<%=fdaRegulated %>">
	<td><textarea maxlength="4000" name="reason_del" id="reason_del"></textarea>
	<br><font class="Mandatory"><%=MC.M_Limit4000_Char %></font>
	</td>
	</tr>
	</table>

	<P class="defComments"><%=MC.M_Etr_Esign_ToProc%><%--Please enter e-Signature to proceed*****--%></P>
		
	<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="pattrtarmdel"/>
			<jsp:param name="showDiscard" value="N"/>
	</jsp:include>

 	 <input type="hidden" name="delMode" value="<%=delMode%>">
  	 <input type="hidden" name="pattxArmId" value="<%=pattxArmId%>">
   	 
	</FORM>
<%
	} else {
			String eSign = request.getParameter("eSign");	
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>
 		  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
			} else {
			pattxarmJB.setPatTXArmId(Integer.parseInt(pattxArmId));
			pattxarmJB.getPatTXArmDetails();
			
			String reason_del = request.getParameter("reason_del");
			reason_del = reason_del.trim();
			reason_del = reason_del==null?"":reason_del;
			//Modified for INF-18183 ::: Akshi
			ret = pattxarmJB.removePatTXArm(AuditUtils.createArgs(session,reason_del,LC.L_Mng_Pats));  
			%>
				<br><br><br><br><br><br><br>
			<TABLE width="550" border = "0">
			 <tr>
				<td align="center">
				<%
			
			if (ret == -1) {%>
			 <p class = "successfulmsg"> <%=MC.M_DataCnt_DelSucc%><%--Data not could not be deleted successfully*****--%> </p>			
			<%}
			else { %>
			 <p class = "successfulmsg"> <%=MC.M_Data_DelSucc%><%--Data deleted successfully*****--%> </p>
			<%}
			%>
			
				</td>
			   </tr>
			 </table>
				<% if (ret >= 0)
					{%>
				  <script>
						window.opener.location.reload();
						setTimeout("self.close()",1000);
				  </script>	  				
					<%
					} // end of if status got deleted 
				  else
					 {
					 	%>
					 	<TABLE width="550" border = "0">
			 			<tr>
							<td align="center">
								<button onClick="window.self.close();"><%=LC.L_Close%></button>	 
							</td>
			   			</tr>
			 			</table>	
						<%
					 
					 }  
						
			
			} //end esign
	} //end of delMode	
  }//end of if body for session 
else { %>
 <jsp:include page="timeout_childwindow.jsp" flush="true"/> 
 <% } %>
 
 
</DIV>
<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</body>
</HTML>


