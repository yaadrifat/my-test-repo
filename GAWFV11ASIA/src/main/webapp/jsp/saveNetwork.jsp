<%@page import="com.velos.eres.business.common.CodeDao"%>
<%@page import="com.velos.eres.web.network.NetworkJB"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.*"%>
<%@page import="com.velos.eres.business.common.*"%>
<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%
	request.setCharacterEncoding("UTF-8");
	response.setContentType("text/html");
	HttpSession tSession = request.getSession(false);
	//JSONObject jsObj = new JSONObject();
	if (!sessionmaint.isValidSession(tSession)) {
		// Session is invalid; print an error and exit
		response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");
		//jsObj.put("result", -1);
		//jsObj.put("resultMsg", MC.M_UsrNot_LoggedIn);/*jsObj.put("resultMsg", "User is not logged in.");*****/
   	%>
		<input type="hidden" name="resultMsg" id="resultMsg" value="<%=MC.M_UsrNot_LoggedIn %>"/>    	
	<%
		//return;
	}
	
	int accountId=0;
	String acc = (String) tSession.getValue("accountId");
	accountId = EJBUtil.stringToNum(acc);
	String userId = (String) tSession.getValue("userId");
	int pkuser = EJBUtil.stringToNum(userId);
	String ipAdd = (String) tSession.getValue("ipAdd");
    int dFlag=0;
	boolean flag=true;	
	int netId = 0;
    try { 
    	String siteId = "";
    	String level = "";
    	String sitestype = "";
    	String mainNtwId = "";
    	String networkId = "";
    	String studyId="";
    	String calledFrom=(request.getParameter("calledFrom")==null)?"":request.getParameter("calledFrom");
    	siteId = (request.getParameter("siteId")==null)?"0":request.getParameter("siteId");
    	level = (request.getParameter("level")==null)?"0":request.getParameter("level");
    	sitestype = (request.getParameter("sitestype")==null)?"0":request.getParameter("sitestype");
    	mainNtwId = (request.getParameter("mainNtwId")==null)?"":request.getParameter("mainNtwId");
    	networkId = (request.getParameter("networkId")==null)?"":request.getParameter("networkId");
    String siteIds []=siteId.split(",");
    String sitesTypes []=sitestype.split(",");
    String chld_ids="";
    	CodeDao cd1 = new CodeDao();
    	
    	String pkCode = StringUtil.integerToString(cd1.getCodeId("networkstat", "pending"));
    	//String relNType=StringUtil.integerToString(cd1.getCodeId("relnshipTyp", "lab"));
    	String relNType="0";
    	
    	NetworkJB njb =new NetworkJB();
    	int retNetId=0;
    	if(calledFrom.equals("studynetworkTabs")){
    		NetworkDao dao=new NetworkDao();
    		studyId = (request.getParameter("studyId")==null)?"":request.getParameter("studyId");
    		dao.saveNetworkToStudy(StringUtil.stringToNum(studyId),networkId,userId,ipAdd);
    		
    	}else{
    	if(level.equals("-1")){
    		njb.setNetworkId(StringUtil.stringToInteger(siteId));
    		njb.getNetworkDetails();
    		njb.setNetworkTypeId(sitestype);
    		njb.setModifiedBy(userId);
    		njb.updateNetwork();
    		retNetId=njb.getNetworkId();
    	}else if(level.equals("-2")){
    		/*int ret=0;*/
    		//njb.setNetworkId(StringUtil.stringToInteger(siteId));
    	//	njb.getNetworkDetails();
    		//njb.setNetworkStatusId((request.getParameter("statusId")==null)?"0":request.getParameter("statusId"));
    		//njb.setModifiedBy(userId);
    		//njb.updateNetwork();
    		/*NetworkDao ntDao=new NetworkDao();
    		ret=ntDao.saveNetworkStatus(StringUtil.stringToInteger(siteId),StringUtil.stringToInteger((request.getParameter("statusId")==null)?"0":request.getParameter("statusId")));
    		njb.setNetworkId(ret);
    		njb.getNetworkDetails();*/
    		
    		
    	}else{
    		NetworkDao dao=new NetworkDao();
    		retNetId=dao.saveMultipleNetwork(siteIds,level,userId,ipAdd,relNType,mainNtwId,networkId,pkCode);
    		chld_ids=dao.getChld_ntw_ids();
    		/*
    		njb.setSiteId(siteId);
    		njb.setNetworkLevel(level);
    		njb.setCreator(userId);
    		njb.setIpAdd(ipAdd);
    		//njb.setNetworkTypeId(sitestype);
    		njb.setNetworkTypeId(relNType);
    		njb.setNetworkMainId(mainNtwId);
    		njb.setNetworkSiteId(networkId);
    		njb.setNetworkStatusId(pkCode);
    		//njb.setNetworkDetails();
    		//retNetId=njb.getNetworkId();*/
    	}
    	if(retNetId<=0)
    		flag=false;
    	
		%>
				<input type="hidden" name="resultMsg" id="resultMsg" value="<%=flag %>"/>    	
				<input type="hidden" name="netId" id="netId" value="<%=retNetId%>"/>
				<input type="hidden" name="chld_ids" id="chld_ids" value="<%=chld_ids%>"/>
		<%			
    	}		
	} catch(Exception e) {
    	%>
    		<input type="hidden" name="resultMsg" id="resultMsg" value="<%=flag %>"/>    	
    	<%
		//return ;
	}
%>
	