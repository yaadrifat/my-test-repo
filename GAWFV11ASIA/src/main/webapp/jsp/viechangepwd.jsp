<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<head>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.*"%>

<title><%=LC.L_Change_Pwd %><%-- Change Password*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="resolution.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
	
<SCRIPT Language="javascript">
function  validate(formobj)
{
   newPass = formobj.newpass.value;
   /*if (loginName==newPass) {
		alert("Password cannot be same as User Name. Please enter again.");
		formobj.newPass.focus();
		return false;
	}*/
   		if( formobj.newpass.value.length < 8)
			{
   				alert("<%=MC.M_NewPwd_Atleast8CharsLong%>");/*alert("The New Password should be atleast 8 characters long. Please enter again.");*****/
				formobj.newpass.focus();
				return false;
			} 
		if( formobj.newpass.value != formobj.confpass.value)
			{
				alert("<%=MC.M_NewConfirm_PwdSame%>");/*alert("Values in 'New Password' and 'Confirm Password' are not same. Please enter again.");*****/
				formobj.newpass.focus();
				return false;
		} 
			
     }
</SCRIPT>
</head>

<body>

	<%  HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))
	{


String src;
src= request.getParameter("srcmenu");
%>

<BR>
<DIV class="popupDefault" > 
	<%

	String strUserOldPwd = null;
	String strUserOldESign = null;
    VieDao curUser=new VieDao();
		strUserOldPwd = curUser.getUserPwd();
		
//		String loginName = curUser.getUserLoginName();	

	    String tab = request.getParameter("selectedTab");%>
	    
    	
			<P class="sectionHeadings"><%=MC.M_VieSet_ChgPwd %><%-- Vie Settings >> Change Password*****--%> </P>
			<jsp:include page="settingtabs.jsp" flush="true"/>

<!--    		<Form name = "pwd" method="post" action="updateviesettings.jsp?scr=pwd" onsubmit= "return validate(document.pwd)">-->
    		<Form name = "pwd" method="post" action="updateviesettings.jsp?scr=pwd">
    			<Input type="hidden" name="selectedTab" value="<%=tab%>">
    		    <input type="hidden" name="src" size = 15 value = "<%=src%>">
    		   
    		   
    		    <table width="400" cellspacing="0" cellpadding="0" border="0">
    			    <tr> 
    			        <td> 
    					    <P class = "defComments"><%=MC.M_Can_ChgYourPwd %><%-- You can change your existing Password*****--%></P>
    			        </td>
    			    </tr>
    		    </table>
    			
    		    <table width="500" cellspacing="0" cellpadding="0" border="0">
    			    
    			    <tr> 
    				    <td width="200" class="tdDefault"> <%=LC.L_Old_Pwd %><%-- Old Password*****--%>  </td>
    					<td> 
    				        <input type="password" name="oldpass" size = 15 MAXLENGTH = 15>
    			        </td>
    			    </tr>
    				<tr> 
    			        <td width="200" class="tdDefault"> <%=LC.L_New_Pwd %><%-- New Password*****--%> <FONT class="Mandatory">* </FONT> </td>
    			        <td> 
    				        <input type="password" name="newpass" size = 15 MAXLENGTH = 15>
    			        </td>
    				</tr>
    				<tr> 
    			        <td width="200" class="tdDefault"> <%=LC.L_Confirm_NewPwd %><%-- Confirm New Password*****--%> <FONT class="Mandatory">* </FONT> 
    			        </td>
    			        <td> 
    				        <input type="password" name="confpass" size = 15 MAXLENGTH = 15>
    			        </td>
    			    </tr>
    		<tr><td></td>	<td>
   <input type="image" src="../images/jpg/Submit.gif" onClick="return validate(document.pwd)"  align="absmiddle" border="0">	

	     <button onclick="javascript:self.close()"><%=LC.L_Close%></button></td>  </tr>	
</table>
<br>

  




    		</Form>

</div>
 <%}else{%>		 	
<br>
<br>
<br>
<br>
<br>

<p class = "sectionHeadings" align = center> <%=MC.M_YourSessExpired_Relogin %><%-- Your session has Expired.Please Relogin.*****--%> </p>
<script>window.self.close();</script>
<!--	<META HTTP-EQUIV=Refresh CONTENT="3; URL=viehome.jsp">-->

<%}%>
	
</body>

</html>

