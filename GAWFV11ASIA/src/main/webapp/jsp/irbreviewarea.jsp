<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@ page language="java" import="com.velos.eres.web.objectSettings.ObjectSettingsCache, com.velos.eres.web.objectSettings.ObjectSettings"%>
<%@ page language="java" import="com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.business.ulink.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.EJBUtil"%>
<%@ page language="java" import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*" %>
<%!
String buildBarChart(int index, ArrayList countList, int total) {
    if (countList.size() < 1) { return "&nbsp;"; }
    if (total < 1) { return "&nbsp;"; }
    double percent = Double.valueOf(((Integer)countList.get(index)).intValue()).doubleValue() * 100d;
   // percent /= Double.valueOf(total).doubleValue();
    StringBuffer sb = new StringBuffer();
	  percent = percent/100;
	  if(percent>=100){
	 	   percent=100;
	  	  sb.append("<table height=\"100%\" width=\"").append(Math.round(percent))
	  	    .append("%\"><tr><td width=\"100%\" style=\"background-color:#880000\">&nbsp;</td></tr></table>");
	  	    return sb.toString();
	     }
	     else{
	     	sb.append("<table height=\"100%\" width=\"").append(Math.round(percent))
	  	    .append("%\"><tr><td width=\"100%\" style=\"background-color:#880000\">&nbsp;</td></tr></table>");
	  	    return sb.toString();
	     }
}
String buildSummaryTable(ArrayList tabList, String accountId, String grpId, String userId) {
    EIRBDao eIrbDao = new EIRBDao();
    eIrbDao.getSummaryPendingReviews(EJBUtil.stringToNum(accountId), EJBUtil.stringToNum(grpId),
            EJBUtil.stringToNum(userId));
    eIrbDao.getSummaryAncillaryReviews(EJBUtil.stringToNum(accountId), EJBUtil.stringToNum(grpId),
            EJBUtil.stringToNum(userId));
    StringBuffer sb = new StringBuffer();
    for (int iX=0; iX<tabList.size(); iX++) {
        ObjectSettings settings = (ObjectSettings) tabList.get(iX);
        String dispTxtLink = "<a href=\"irbreviewarea.jsp?selectedTab="+settings.getObjSubType()+
                "&mode=N\">" + settings.getObjDispTxt() + "</a>";
        if ("irb_revsumm_tab".equals(settings.getObjSubType())) { continue; }
        if ("irb_revanc_tab".equals(settings.getObjSubType())) {
            sb.append( buildAncillaryReviewType("rev_anc", dispTxtLink, eIrbDao) );
        } else if ("irb_revfull_tab".equals(settings.getObjSubType())) {
            sb.append( buildEachReviewType("rev_full", dispTxtLink, eIrbDao) );
        } else if ("irb_revexp_tab".equals(settings.getObjSubType())) {
            sb.append( buildEachReviewType("rev_exp", dispTxtLink, eIrbDao) );
        } else if ("irb_revexem_tab".equals(settings.getObjSubType())) {
            sb.append( buildEachReviewType("rev_exe", dispTxtLink, eIrbDao) );
        }
    }

    return sb.toString();    
}
String buildEachReviewType(String codelstSubtype, String dispText, EIRBDao eIrbDao) {
    ArrayList subtotalSummaryReviewSubtypes = eIrbDao.getSubtotalSummaryReviewSubtypes();
    ArrayList subtotalSummaryReviewCounts = eIrbDao.getSubtotalSummaryReviewCounts();
    int arrayIndex = -1;
    StringBuffer sb = new StringBuffer();
    if (subtotalSummaryReviewSubtypes != null) {
        for (int iY=0; iY<subtotalSummaryReviewSubtypes.size(); iY++) {
            if (codelstSubtype.equals(subtotalSummaryReviewSubtypes.get(iY))) {
                arrayIndex = iY;
                break;
            }
        }
    }
    if (arrayIndex < 0) {
        sb.append("<tr class=\"browserEvenRow\" align=\"left\"><td width=\"30%\"><b>")
        .append(dispText).append("</b></td>")
        .append("<td align=\"center\" width=\"20%\">")
       // .append("0") // for australia enhancement
        .append("</td><td width=\"50%\"></td></tr>");
        return sb.toString();
    }
    String summaryReviewCount=subtotalSummaryReviewCounts.get(arrayIndex).toString();
    sb.append("<tr class=\"browserEvenRow\" align=\"left\"><td width=\"30%\"><b>")
    .append(dispText).append("</b></td><td align=\"center\" width=\"20%\">")
    // .append(subtotalSummaryReviewCounts.get(arrayIndex))//for australia enhancement
    .append("</td><td width=\"50%\">")
    .append("<table height=\"100%\" width=\"");
    if((Integer)subtotalSummaryReviewCounts.get(arrayIndex)>=100){
    	summaryReviewCount="100";
    	
    }
    sb.append(summaryReviewCount)
    .append("%\"><tr><td width=\"100%\" style=\"background-color:#880000\">&nbsp;</td></tr></table>")
    .append("</td></tr>");
    ArrayList[] revCounts = eIrbDao.getSummaryReviewCounts();
    ArrayList[] revNames  = eIrbDao.getSummaryReviewReviewers();
    for (int iY=0; iY<revCounts[arrayIndex].size(); iY++) {
        sb.append("<tr class=\"browserOddRow\" ><td align=\"right\" width=\"30%\"><i>")
        .append(revNames[arrayIndex].get(iY) == null ? LC.L_Not_Assigned/*Not Assigned*****/ : revNames[arrayIndex].get(iY))
        .append("</i></td><td align=\"center\" width=\"20%\">")
        .append(revCounts[arrayIndex].get(iY))
        .append("</td><td>").append(buildBarChart(iY, revCounts[arrayIndex], 
                ((Integer)subtotalSummaryReviewCounts.get(arrayIndex)).intValue()))
        .append("</td></tr>");
    }
	return sb.toString();
}
String buildAncillaryReviewType(String codelstSubtype, String dispText, EIRBDao eIrbDao) {
    StringBuffer sb = new StringBuffer();
    ArrayList subtotalSummaryAncillaryReviewCounts = eIrbDao.getSubtotalSummaryAncillaryReviewCounts();
    int arrayIndex = -1;
    if (subtotalSummaryAncillaryReviewCounts != null) {
        arrayIndex = subtotalSummaryAncillaryReviewCounts.size()-1;
    }
    if (arrayIndex < 0) {
        sb.append("<tr class=\"browserEvenRow\" align=\"left\"><td width=\"30%\"><b>")
        .append(dispText).append("</b></td><td align=\"center\" width=\"20%\">")
        .append("0</td><td width=\"50%\"></td></tr>");
        return sb.toString();
    }
    String ancCmtReviewCount=subtotalSummaryAncillaryReviewCounts.get(arrayIndex).toString();
    sb.append("<tr class=\"browserEvenRow\" align=\"left\"><td width=\"30%\"><b>")
    .append(dispText).append("</b></td><td align=\"center\" width=\"20%\">")
    //.append(subtotalSummaryAncillaryReviewCounts.get(arrayIndex)) // for australia enhancement
    .append("</td><td width=\"50%\">")
    .append("<table height=\"100%\" width=\"");
    if((Integer)subtotalSummaryAncillaryReviewCounts.get(arrayIndex)>=100){
    	
    	ancCmtReviewCount="100";
    }
   sb .append(ancCmtReviewCount)
    .append("%\"><tr><td width=\"100%\" style=\"background-color:#880000\">&nbsp;</td></tr></table>")
    .append("</td></tr>");
    ArrayList revBoards = eIrbDao.getSummaryAncillaryReviewBoards();
    ArrayList revCounts = eIrbDao.getSummaryAncillaryReviewCounts();
    ArrayList revNames  = eIrbDao.getSummaryAncillaryReviewReviewers();
    for (int iY=0; iY<revCounts.size(); iY++) {
        String nameDisp = revNames.get(iY) == null ? LC.L_Not_Assigned/*Not Assigned*****/ : (String)revNames.get(iY);
        sb.append("<tr class=\"browserOddRow\" ><td align=\"right\" width=\"30%\"><i>")
        .append(revBoards.get(iY) == null ? "" : revBoards.get(iY)).append(" (")
        .append(revNames.get(iY) == null ? LC.L_Not_Assigned/*Not Assigned*****/ : revNames.get(iY)).append(")")
        .append("</i></td><td align=\"center\" width=\"20%\">")
        .append(revCounts.get(iY))
        .append("</td><td>").append(buildBarChart(iY, revCounts, 
                ((Integer)subtotalSummaryAncillaryReviewCounts.get(arrayIndex)).intValue()))
        .append("</td></tr>");
    }
	return sb.toString();
}
%>
<html>
<head>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="codeB" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<%
ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
ArrayList tabList = null;
String titleStr = "";
if (sessionmaint.isValidSession(request.getSession(true)))
{
    String accId = (String) request.getSession(true).getValue("accountId");
    
    ArrayList tempList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "top_menu");
    for (int iX=0; iX<tempList.size(); iX++) {
        ObjectSettings settings = (ObjectSettings) tempList.get(iX);
        if ("irb_menu".equals(settings.getObjSubType())) {
            if (settings.getObjDispTxt() != null) {
                titleStr += settings.getObjDispTxt();
            }
            break;
        } 
    }
    tempList = null;

    tempList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "irb_menu");
    for (int iX=0; iX<tempList.size(); iX++) {
        ObjectSettings settings = (ObjectSettings) tempList.get(iX);
        if ("review_menu".equals(settings.getObjSubType())) {
            if (settings.getObjDispTxt() != null) {
                titleStr += " >> " + settings.getObjDispTxt();
            }
            break;
        } 
    }
    tempList = null;
    
    tabList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "irb_rev_tab");
    for (int iX=0; iX<tabList.size(); iX++) {
        ObjectSettings settings = (ObjectSettings) tabList.get(iX);
        if (settings.getObjSubType().equals(request.getParameter("selectedTab"))) {
        	Object[] arguments = {titleStr,settings.getObjDispTxt()};
            titleStr = VelosResourceBundle.getLabelString("L_Gtgt",arguments);/*titleStr += " >> "+ settings.getObjDispTxt();*****/
        }
    }
}
%>
<title><%=titleStr %></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<% 
String src= request.getParameter("srcmenu");
HttpSession tSession = request.getSession(true); 
String accountId = "";
String studyId = "";
if (sessionmaint.isValidSession(tSession))
{
  accountId = (String) tSession.getValue("accountId");
  studyId = (String) tSession.getValue("studyId");
  if(accountId == null || accountId == "") {
  %>
    <jsp:include page="timeout.html" flush="true"/>
  <%
    return;
  }
}
%>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<SCRIPT language="javascript">
function submitForm(formobj) {
	formobj.action="updateSubmission.jsp?&srcmenu=<%=request.getParameter("srcmenu")%>&selectedTab=irb_init_tab"; 
	return true;
}
</SCRIPT> 
<body>

<div id="overDiv" style="position:absolute;visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<%
   
  String tab = request.getParameter("selectedTab");
   
   	int pendRevRight= 0;
	int fullRevRight=  0;
	int expRevRight=  0;
	int exeRevRight  = 0;
	int ancRevRight  = 0;
	
	boolean showThisTab = false;
	int submissionBrowserRight = 0;

	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
	pendRevRight = Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_RPR"));
	fullRevRight =  Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_RFR"));
	expRevRight =  Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_RXPR"));
    exeRevRight = Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_RXER"));
    ancRevRight = Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_RACR"));
 
 
	  if ("irb_revsumm_tab".equals(tab) )
			{
				if (pendRevRight >= 4)
				{
					showThisTab = true;
					submissionBrowserRight = pendRevRight;
				}
				else 	{
					tab="irb_revfull_tab";
				}	
			}
	 
	  if ("irb_revfull_tab".equals(tab) )
			{
				if (fullRevRight >=6 || fullRevRight == 4)
				{
					showThisTab = true;
					submissionBrowserRight = fullRevRight;
				}
				else 	{
					tab="irb_revexp_tab";
				}	
			}
		 
	  if ("irb_revexp_tab".equals(tab) )
			{
				if (expRevRight >=6 || expRevRight == 4)
				{
					showThisTab = true;
					submissionBrowserRight = expRevRight;
				}
				else 	{
					tab="irb_revexem_tab";
				}	
			}
		 
		
	  if ("irb_revexem_tab".equals(tab) )
		{
			if (exeRevRight >=6 || exeRevRight == 4)
			{
				showThisTab = true;
				submissionBrowserRight = exeRevRight;
			}
			else 	{
				tab="irb_revanc_tab";
			}	
		}  
		  
  
	  if ("irb_revanc_tab".equals(tab) )
		{
			if (ancRevRight >=6 || ancRevRight == 4)
			{
				showThisTab = true;
				submissionBrowserRight = ancRevRight;
			}
		 
		} 
 
 
  String includeTabsJsp = "irbreviewtabs.jsp";
%>
<DIV class="BrowserTopn" id = "divtab">
<jsp:include page="<%=includeTabsJsp%>" flush="true">
   
  <jsp:param name="selectedTab" value="<%=tab%>"/>
   
</jsp:include>
</DIV>
<DIV class="BrowserBotN BrowserBotN_RC_2" id = "div1">
<%
  if (sessionmaint.isValidSession(tSession))
  {
    String usrId = (String) tSession.getValue("userId");
    String uName = (String) tSession.getValue("userName");
    String defUserGroup = (String) tSession.getAttribute("defUserGroup");
    String tableHeader = null;   
    String tableContent = null;
    if ("irb_revsumm_tab".equals(tab)) {
        tableHeader = "<td align=\"center\"><P class=\"defComments\">"+LC.L_PendingRev_Smry/*Pending Review Summary*****/+"</P></td>";
        
        tableContent = buildSummaryTable(tabList, accountId, defUserGroup, usrId);
    } else {
        tableHeader = "";
        tableContent = "";
    }
    
    int revType = 0;
	String revSubType = "";
	
	HashMap hmReviewSubType= new HashMap();
 
 	hmReviewSubType.put("irb_revfull_tab","rev_full");
    hmReviewSubType.put("irb_revexp_tab","rev_exp");
    hmReviewSubType.put("irb_revexem_tab","rev_exe");
    hmReviewSubType.put("irb_revanc_tab","rev_anc");
    
    revSubType  = (String) hmReviewSubType.get(tab);
     
    revType = codeB.getCodeId("revType",revSubType ); 
    
 if (showThisTab )
{
 	if ("irb_revsumm_tab".equals(tab)) {
    	%>
    	<table width="100%" cellspacing="0" cellpadding="0" border="0">
    	  	<tr height="5"><td></td></tr>
      <tr><%=tableHeader%></tr>
      <tr ><td align="center" width="98%" ><br /></td><td></td></tr>
    </table>
      
      
    <table width="99%" cellspacing="0" cellpadding="0" border="0" class="outline midAlign basetbl">
      <%=tableContent%>
    </table>
    	<% } else {%>

    <jsp:include page="submissionBrowser.jsp" flush="true">
      <jsp:param name="tabsubtype" value="<%=tab%>"/>
      <jsp:param name="tabtype" value="irbReview"/>
      <jsp:param name="revType" value="<%=revType%>"/> 
      <jsp:param name="hideRevTypeFilter" value="Y"/> 
       <jsp:param name="submissionBrowserRight" value="<%=submissionBrowserRight%>"/> 
       
    </jsp:include>

  <%
        }
  	  } //show this tab
	else
	{
		%>
			<jsp:include page="accessdenied.jsp" flush="true"/>
			<%

	}
  	
  }//end of if body for session
  else
  {
  %>
    <jsp:include page="timeout.html" flush="true"/>
  <%
  }
  %>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
</body>

</html>