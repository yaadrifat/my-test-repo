<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@page import="com.velos.eres.service.util.LC"%>
<%@page import="com.velos.eres.service.util.MC"%>
<html>
<head>
<title><%--E-mail User*****--%><%=LC.L_Email_User%> </title>


<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB, com.velos.eres.service.util.EJBUtil" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>

<SCRIPT language="javascript">

function setVals(formobj){
		checkQuote = "N";
		/*if(formobj.eSign.value== "")
		{
			alert("Please enter e-Signature");
			formobj.eSign.focus();
			return false;
		}*/
		
		if (!(validate_col('\'To\'',formobj.To)))
		{
		formobj.ToName.focus();
		return false;
		}
		if (!(validate_col('e-Signature',formobj.eSign))) return false;
		<%-- if(isNaN(formobj.eSign.value) == true) {
			/*alert("Incorrect e-Signature. Please enter again.");*****/
			alert("<%=MC.M_IncorrEsign_EtrAgain%>");
			formobj.eSign.focus();
			return false;
		   } --%>
		 
		
		  
		
		

		 formobj.action = "emailusersubmit.jsp"; 
		 return true;
	
	}

function openwindow() {
windowName = window.open("multilookup.jsp?viewId=6000&form=emailuser&dfilter=&keyword=To|USREMAIL","Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=550 top=100,left=100 0,")
	windowName.focus();

}
function openLookup(formobj,act) {
//frameMode=formobj.drop.value;
formobj.target="Lookup";
formobj.method="post";
accId=formobj.accId.value;
formobj.action="multilookup.jsp?viewId=6000&form=emailuser&dfilter=emailuser|"+accId+"&seperator=,&maxselect="+
           "&keyword=To|USREMAIL|[VELHIDE]~dummy|USRFNAME|[VELHIDE]~dummy|USRLNAME|[VELHIDE]~ToName|[VELEXPR]*[VELKEYWORD=USRLNAME]*[VELSTR=;]*[VELSPACE]*[VELKEYWORD=USRFNAME]";
formWin =open('donotdelete.html','Lookup','resizable=1,status=0, width=850,height=550 top=100,left=100,menubar=no,scrollbars=1');
if (formWin && !formWin.closed) formWin.focus();
formobj.submit();
formobj.target="";
formobj.action="";
void(0);
}





</SCRIPT>
 
</head>
<% response.setHeader("Cache-Control", "max-age=1, must-revalidate");%>
<%
int ienet = 2;

String agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1) 
     ienet = 0; //IE
    else
	ienet = 1;
	if(ienet == 0) {	
%>
<body style="overflow:scroll;">
<%
	} else {
%>
<body>
<%
	}
%>


<% 
String src;

src= request.getParameter("srcmenu");

%>



<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   

<body>
<jsp:useBean id="groupB" scope="page" class="com.velos.eres.web.group.GroupJB"/>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.StringUtil"%>
<BR>
<DIV  id = "div1"> 

  <%

HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession))

{

	String uName = (String) tSession.getValue("userName");



	int pageRight = 0;

	String accId = (String) tSession.getValue("accountId");

	//get the default group of the login user. If the default group is Admin then show reset password and reset user session for users
	String logUsr = (String) tSession.getValue("userId");
	int loginUser = EJBUtil.stringToNum(logUsr);
	userB.setUserId(loginUser);
	userB.getUserDetails();
	String defGroup = userB.getUserGrpDefault();
	groupB.setGroupId(EJBUtil.stringToNum(defGroup));
	groupB.getGroupDetails();
	String groupName = ((groupB.getGroupName())==null)?"-":(groupB.getGroupName());
	
	
	
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		

	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MUSERS"));	

   if (pageRight > 0 )

	 {
       ArrayList usrLastNames = null;

       ArrayList usrFirstNames = null;

%>


  <Form name="emailuser" id="emailuserfrm" method="post" onsubmit="if (setVals(document.emailuser)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;};">
	  <input type="hidden" name=srcmenu value=<%=src%>>
	  <input type="hidden" name=accId value=<%=accId%>>
	  <td ><P class="sectionHeadings"> <%-- Manage Account >> Users >> Send e-mail *****--%><%=MC.M_MngAcc_UsrEmail%></P></td>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="basetbl midAlign redmondTxtcolor">
	<tr height="3"><td colspan="5"></td></tr>
	<tr>
	<td colspan="5"><font size="2"><%-- Send e-Mail message to your account users. Select user(s) from the link below. ***** --%><%=MC.M_SendEmailAcc_Usr%></font>
	</td>
	</tr>
	<tr>
			<td width="5%"><font size="2"><%-- To***** --%><%=LC.L_To%>:</font><FONT class="Mandatory">* </FONT></td>
			<%--Width="70" 24766 FIXED BY DHARAM decrease and Now width="5%"  --%>
			<td width="5%">
			<input type="hidden" name="To" size="80" value="" readonly>
			<input type="text" name="ToName" size="80" value="" readonly>
			<input type="hidden" name="dummy" size="80" value="" readonly>
			
			</td> 
			<td width="auto" colspan="3"><a href="#" onClick="openLookup(document.emailuser)"><%-- Select User(s)***** --%><%=LC.L_Select_User_S%></a></td>
		</tr>
		<tr>
		<td width="5%"><font size="2"><%-- Subject***** --%><%=LC.L_Subject%> :</FONT></td>
			<td width="70%" colspan="5"><input type="text" name="subject" size="80" value=""></td> 
</tr>
<tr>
			<td  width="9%" ><font size="2"><%-- Message Text***** --%><%=LC.L_Msg_Text%> :</font></td>	
			<td colspan="4"   class="textareaheight">
				<%--24766 FIXED BY DHARAM here cols="80" --%>
				<TEXTAREA name="txtAreamessage" rows="10" cols="65" MAXLENGTH="255" value=""></TEXTAREA>
			</td>
			
		</tr>
	<tr height="3"><td colspan="5"></td></tr>

	</table>
	<div class="tmpHeight"></div>
	<div class="basetbl outline">
	<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="emailuserfrm"/>
			<jsp:param name="showDiscard" value="N"/>
	</jsp:include>
</div>
</Form>
  <%

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

}//end of if body for session

else  

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
<BR>
 <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id = "emenu"> 
  <jsp:include page="getmenu.jsp" flush="true"/>
</div>
</body>

</html>
