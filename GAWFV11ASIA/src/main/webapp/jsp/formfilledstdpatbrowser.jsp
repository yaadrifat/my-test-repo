<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<!-- Changes By Sudhir for Bug 8850 on 13-Mar-2012 -->
<jsp:include page="include.jsp" flush="true"/>

<%@page import="com.velos.esch.web.eventassoc.EventAssocJB"%>
<jsp:useBean id="portalJB" scope="request" class="com.velos.eres.web.portal.PortalJB"/>
<html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>
<title><%=MC.M_StdPat_FrmRespBrow%><%-- <%=LC.Std_Study%> <%=LC.Pat_Patient%> >> Form Response Browser*****--%></title>

<script>
window.onclose = function(){
		var schevent;

		schevent = window.document.stdpatform.fk_sch_events1.value;
		//alert("schevent" + schevent);

		if (schevent.length > 0 && (!isWhitespace( schevent ) )  && schevent != "null"  )
		{
			if (window.opener != undefined)
			{
				window.opener.location.reload();
			}
		}

	}
</script>


<script>

//function openWin(formId,studyId)
//{
//	windowName= window.open("studyformdetails.jsp?formId="+formId+"&mode=N&studyId="+studyId+"&formDispLocation=S","information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=600,height=500");
	//windowName.focus();
//}
function newDenied()
{
	alert("<%=LC.L_New_PermissionDenied%>");/*alert("New Permission Denied");*****/
	return false;
}

function checkPerm(pageRight)
{

	if (f_check_perm(pageRight,'N') == true)
	{
		return true ;

	}
	else
	{
		return false;
	}
}

 function openWinStatus(patId,patStatPk,pageRight,study)
{

	changeStatusMode = "yes";

	if (f_check_perm(pageRight,'N') == true)
	{
		windowName= window.open("patstudystatus.jsp?studyId="+study+"&changeStatusMode=yes&statid=" + patStatPk + "&pkey=" + patId,"patstatusdetails","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=550,height=560");
		windowName.focus();
	}
}

</script>

<%
String src= request.getParameter("srcmenu");
String selectedTab = request.getParameter("selectedTab");
String studyId=request.getParameter("studyId");
studyId=(studyId==null)?"":studyId;
String statDesc=request.getParameter("statDesc");
String statid=request.getParameter("statid");
String patProtId=request.getParameter("patProtId");
String patientCode=request.getParameter("patientCode");
String mode = request.getParameter("mode");
String fromLab = request.getParameter("fromLab");
String formDispType = request.getParameter("formDispType");
formDispType = (formDispType==null) ? "" :formDispType;
String calledFromPatSch=request.getParameter("calledFromPatSch");
calledFromPatSch=(calledFromPatSch==null)?"":calledFromPatSch;

int patId=EJBUtil.stringToNum(request.getParameter("pkey"));

//Added for Bug related to 	INF - 21828

String eventId=request.getParameter("eventId");
eventId=eventId==null?"0":eventId;
String portalID=request.getParameter("portalID");
portalID=portalID==null?"0":portalID;
int portalId =EJBUtil.stringToNum(portalID);
String patProtProtlId=request.getParameter("patProtProtocolId");
patProtProtlId=patProtProtlId==null?"0":patProtProtlId;
int patProtProtocolId =EJBUtil.stringToNum(patProtProtlId);
String patientForm=request.getParameter("patientForm");
patientForm=patientForm==null?"0":patientForm;
String filledFormId =  request.getParameter("formPullDown");
filledFormId=filledFormId==null?"0*M":filledFormId;
String[] crfFromId=StringUtil.chopChop(filledFormId,'*');
int showFormDetails=0;
if(patientForm.equalsIgnoreCase("patientPortalSch")){
	EventAssocJB evJB = new EventAssocJB();
	showFormDetails=evJB.getEventFormAccees(EJBUtil.stringToNum(crfFromId[0]), EJBUtil.stringToNum(eventId), portalId );
if(showFormDetails==0){
	%><br><br><br><table align=center><tr><td><P class="defComments" >
  	<%=MC.M_NoFrmBcos_DelOrUrlOrNoRgts%> 
  </P></td></tr></table>
   <jsp:include page="bottompanel.jsp" flush="true"/><% return;
}
}
if(patientForm.equalsIgnoreCase("patientPortal")){
	PortalDao pdao = new PortalDao();
	
	portalJB.setPortalId(portalId);
	portalJB.getPortalDetails();
	pdao = portalJB.getDesignPreview(portalId, patProtProtocolId);
	int count=0;
	ArrayList linkedModuleIds = new ArrayList();
	linkedModuleIds= pdao.getLinkedModuleIds();
	int arraySize = linkedModuleIds.size();
	if(arraySize>=1){
		String formIDes[] = StringUtil.chopChop(linkedModuleIds.get(0).toString(),',');
		//For Bug# 15476
		StringBuilder sb = new StringBuilder();
		try{
		if(arraySize>1){
			for(int z=0 ; z<arraySize ; z++){
				int k=0;
				k = Integer.parseInt((String)linkedModuleIds.get(z));
				if(z!=arraySize-1){
					sb.append(k).append(",");
				}else{
					sb.append(k);
				}
			}
			formIDes = sb.toString().split(",");
		}
		}catch(Exception e){
			String csv = linkedModuleIds.toString().replace("[", "").replace("]", "").replace(", ", ",");
			formIDes = csv.split(",");
		}
		//END FOR Bug 15476 
		for(int i =0; i < formIDes.length; i++)
		    {
		        String formIDss= crfFromId[0];
		        if(formIDss.equalsIgnoreCase(formIDes[i]))
		        {
		        	  count++;
		        	  break;
		        }
		    }
	}
	if(count==0){
		%><br><br><br><table align=center><tr><td><P class="defComments" >
	  	<%=MC.M_NoFrmBcos_DelOrUrlOrNoRgts%> 
	  </P></td></tr></table>
	   <jsp:include page="bottompanel.jsp" flush="true"/><% return;
	}
}

//Added on 14th March Mahi-II

String strform = request.getParameter("strform");

String fk_sch_events1 = "";

fk_sch_events1 = request.getParameter("fk_sch_events1");

if (StringUtil.isEmpty(fk_sch_events1 ))
{
	fk_sch_events1 = "";
}
String eventSchIds = "";

eventSchIds = request.getParameter("checkeventIds");

if (StringUtil.isEmpty(eventSchIds ))
{
	eventSchIds = "";
}
if(patientForm.equalsIgnoreCase("patientPortalSch")){
if(!fk_sch_events1.equalsIgnoreCase(eventSchIds)){
	%><br><br><br><table align=center><tr><td><P class="defComments" >
  	<%=MC.M_NoFrmBcos_DelOrUrlOrNoRgts%> 
  </P></td></tr></table>
   <jsp:include page="bottompanel.jsp" flush="true"/><% return;
}
}
String fkStorageForSpecimen = "";

fkStorageForSpecimen = request.getParameter("fkStorageForSpecimen");

if (StringUtil.isEmpty(fkStorageForSpecimen ))
{
	fkStorageForSpecimen = "";
}

//out.println("fk_sch_events1 " + fk_sch_events1 );


//String formPullDown111	=	request.getParameter("formPullDown111");


String calledFromForm = "";
       calledFromForm = request.getParameter("calledFromForm");

      if (StringUtil.isEmpty(calledFromForm))
      {
     	calledFromForm = "";
      }

     String outputTarget = ""; //if a target name is passed, open the new and edit page in this target
   outputTarget = request.getParameter("outputTarget");

 if (StringUtil.isEmpty(outputTarget))
 {
 	outputTarget = "";
 }


    String specimenPk = "";
    specimenPk = request.getParameter("specimenPk");

     if (StringUtil.isEmpty(specimenPk))
	 {
	 	specimenPk = "";
	 }




   if (calledFromForm.equals(""))
   {
%>
	<jsp:include page="panel.jsp" flush="true">
	<jsp:param name="src" value="<%=src%>"/>
	</jsp:include>

 <% }
 else
 { %>
 	<jsp:include page="popupJS.js" flush="true"/>
	<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
	<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
	<SCRIPT LANGUAGE="JavaScript" SRC="formjs.js"></SCRIPT>
 	<%
 }
 %>
<body>

<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="patEnrollB" scope="request" class="com.velos.eres.web.patProt.PatProtJB"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="eventAssocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="formLibB" scope="request" class="com.velos.eres.web.formLib.FormLibJB"/>
<jsp:useBean id="lnkformB" scope="request" class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
<jsp:useBean id="personB" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="userSiteB" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" />
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="patB" scope="request" class="com.velos.eres.web.patStudyStat.PatStudyStatJB"/>
<jsp:useBean id="commonB" scope="request" class="com.velos.eres.web.common.CommonJB"/>

<%@ page language = "java" import = "com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.*,com.velos.eres.business.common.CtrlDao,com.velos.eres.business.common.SettingsDao,java.util.*,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration,com.velos.eres.business.common.*,com.velos.eres.web.user.UserJB,com.velos.eres.web.studyRights.StudyRightsJB"%>
<%@ page language = "java" import = "com.velos.eres.web.patLogin.PatLoginJB" %>

<%
if (calledFromForm.equals(""))
   {
%>

<%
	}
%>

<%
HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession)) {


	if (StringUtil.isEmpty(patientCode))
	{
		 personB.setPersonPKId(patId);
		personB.getPersonDetails();
		patientCode = personB.getPersonPId();
	}

   int pageRight = 0;
   int pageRightPat = 0; //KM
   int siteid=0;
   int personPK = 0;
   String protocolId = "";
//   String enrollId = "";
   String protName = "";
   String studyTitle = "";
   String studyNumber = "";
   char formLibAppRight = '0';
   char patProfileAppRight = '0';
   int patProtPK = 0;



   String parentId=request.getParameter("parentId");
   parentId=(parentId==null)?"":parentId;

   String selectedStudy = request.getParameter("selectedStudy");
   String strFormId = request.getParameter("formPullDown");

   String usr = (String) tSession.getValue("userId");

   String responseId=request.getParameter("responseId");
   responseId=(responseId==null)?"":responseId;

	//GET STUDY TEAM RIGHTS
    TeamDao teamDao = new TeamDao();
    teamDao.getTeamRights(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(usr));
    ArrayList tId = teamDao.getTeamIds();
    if (tId.size() == 0)
	{
    	pageRight=0 ;
    }
	else
	{
    	stdRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));

    		ArrayList teamRights ;
					teamRights  = new ArrayList();
					teamRights = teamDao.getTeamRights();

					stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
					stdRights.loadStudyRights();


    	if ((stdRights.getFtrRights().size()) == 0)
		{
    	 	pageRight= 0;
		pageRightPat = 0;
    	}
		else
		{
    		pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMACC"));
		//KM-to fix the Bug2388
		pageRightPat = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYMPAT"));
    	}
    }


	String modRight = (String) tSession.getValue("modRight");
	int patProfileSeq = 0, formLibSeq = 0;


	 	// To check for the account level rights
		 acmod.getControlValues("module");
		 ArrayList acmodfeature =  acmod.getCValue();
		 ArrayList acmodftrSeq = acmod.getCSeq();
		 patProfileSeq = acmodfeature.indexOf("MODPATPROF");
		 formLibSeq = acmodfeature.indexOf("MODFORMS");
		 formLibSeq = ((Integer) acmodftrSeq.get(formLibSeq)).intValue();
		 patProfileSeq = ((Integer) acmodftrSeq.get(patProfileSeq)).intValue();
		 formLibAppRight = modRight.charAt(formLibSeq - 1);
		 patProfileAppRight = modRight.charAt(patProfileSeq - 1);

		///////////////////////////


		if (EJBUtil.isEmpty(patProtId)  || (patProtId.equals("0"))) // if there is no patProtId passed/is empty
    		{
    			//check if there is any current enrollment
    			patEnrollB.findCurrentPatProtDetails(EJBUtil.stringToNum(studyId),patId );
    			patProtPK =  patEnrollB.getPatProtId(); //get patProtId from current enrollment

    			if (patProtPK  > 0)
    			{
    				patProtId = String.valueOf(patProtPK);
    			}

    		}
    		else
    		{
				//SV, 8/13/04, as a fix for re-opened 1599, decided to use patProtId itself, as enrollid is always equal to patProtId and is not used for any other purpose.
    			patEnrollB.setPatProtId(EJBUtil.stringToNum(patProtId)); //SV, 8/13/04, changed to use patProtId
    			patEnrollB.getPatProtDetails();
    		}


		/////////////////////////

		protocolId =patEnrollB.getPatProtProtocolId();

		if(protocolId != null) {
			eventAssocB.setEvent_id(EJBUtil.stringToNum(protocolId));
			eventAssocB.getEventAssocDetails();
			protName = 	eventAssocB.getName();
		}
		studyB.setId(EJBUtil.stringToNum(studyId));

		studyB.getStudyDetails();
		studyTitle = studyB.getStudyTitle();
		studyNumber = studyB.getStudyNumber();

	%>
   <%
   			String entryChar="",lfType="";;
	     ArrayList arrFrmNames = null;
		 ArrayList arrNumEntries = null;
 		 ArrayList arrFrmIds = null;
		 ArrayList arrEntryChar = null;
		 ArrayList arrFrmInfo = new ArrayList();
		 ArrayList arrLFTypes=null;
		 //String entryChar="";
		 String numEntries = "";
		 String frmInfo= "";
		 int formId = 0;
		 String firstFormInfo = "";
  	     strFormId = request.getParameter("formPullDown");
	 	 String accId=(String)tSession.getAttribute("accountId");
		 int iaccId=EJBUtil.stringToNum(accId);
 	     String userId = (String) tSession.getValue("userId");
	     UserJB userB = (UserJB) tSession.getValue("currentUser");
   	     String siteId = userB.getUserSiteId();
	 	 int istudyId = EJBUtil.stringToNum(studyId);
	     int ipatProtId = EJBUtil.stringToNum(patProtId);

		 personPK = EJBUtil.stringToNum(request.getParameter("pkey"));

    	 personB.setPersonPKId(personPK);
     	 personB.getPersonDetails();
    	 String patientId = personB.getPersonPId();

		 LinkedFormsDao lnkFrmDao = new LinkedFormsDao();
		 lnkFrmDao = lnkformB.getPatientStudyForms(iaccId,personPK,istudyId, EJBUtil.stringToNum(userId), EJBUtil.stringToNum(siteId));

		 arrFrmIds = lnkFrmDao.getFormId();
		 	 arrFrmNames = lnkFrmDao.getFormName();
		 arrEntryChar = lnkFrmDao.getFormEntryChar();
		 arrNumEntries = lnkFrmDao.getFormSavedEntriesNum();
		 arrLFTypes=lnkFrmDao.getFormDisplayType();

		  //VA-05/05/05-check if lab is hidden for this study,if yes,don't add in DD
		 SettingsDao settingDao=commonB.retrieveSettings(istudyId,3,"FORM_HIDE");
		 ArrayList hideList=settingDao.getSettingValue();
		 boolean hideLab=false;
		  //-1 means lab is blocked
		 if (hideList.indexOf("-1")>=0) hideLab=true;

	 	 if (strFormId==null) {
		    if (arrFrmIds.size() > 0) {
          		 	formId = EJBUtil.stringToNum(arrFrmIds.get(0).toString());
          			entryChar = arrEntryChar.get(0).toString();
          			numEntries = arrNumEntries.get(0).toString();
          			firstFormInfo = arrFrmIds.get(0).toString() + "*"+ entryChar + "*" + arrNumEntries.get(0).toString();
    		} else {
				if (!hideLab)  firstFormInfo="lab";
			}
		 }
		 else if (strFormId.equals("lab")) {
		 	if (!hideLab) firstFormInfo="lab";
	     }
     	else {
     		  //StringTokenizer strTokenizer = new StringTokenizer(strFormId,"*");
		  String[] st=StringUtil.chopChop(strFormId,'*');
    		  if (calledFromForm.equals(""))
    	 		 {
		 	  	     formId = EJBUtil.stringToNum(st[0]);
	     			 entryChar = st[1];
				 entryChar=(entryChar==null)?"":entryChar;
	     		     numEntries = st[2];
			     numEntries=(numEntries==null)?"":numEntries;
			     if ((numEntries.length()==0) && (entryChar.length()==0))
			     	firstFormInfo=st[0];
				else
	     			 firstFormInfo = strFormId;
	     		}
	     		else
    			 {
    			 	 formId = EJBUtil.stringToNum(st[0]);
    			 	 entryChar = st[1];

    			 	//to get numentries, use the data already retrieved

    			 		int idxForm = 0;

	    			 	if (arrFrmIds != null)
	    			 	{
	    			 		idxForm = arrFrmIds.indexOf(new Integer(formId));

	    			 		 if (idxForm >=0)
	    			 		{
	    			 			if (arrNumEntries != null && arrNumEntries.size() >idxForm )
	    			 			{
	    			 					numEntries = ((Integer) arrNumEntries.get(idxForm)).toString();
	    			 					if (StringUtil.isEmpty(numEntries))
	    			 					{
	    			 						numEntries = "0";
	    			 					}
	    			 			}
	    			 		}
	    			 		else
	    			 		{
	    			 						numEntries = "0";
	    			 		}


	    			 	}
	    			 	else
	    			 	{
	    			 						numEntries = "0";
	    			 	}

    			 	 //prepare strFormId again
    			 	 strFormId = formId + "*" + entryChar + "*" + numEntries;
    			 	 firstFormInfo = strFormId;
    			 	 lfType = 	lnkformB.getLFDisplayType();
    			 }
     	   }

	    		int index=-1;
			if (firstFormInfo.indexOf("*")<0 && !(firstFormInfo.trim()).toLowerCase().equals("lab") && (firstFormInfo.length()>0))
	    		index=arrFrmIds.indexOf(new Integer(firstFormInfo));

			 if (index >=0)
			 {
			    firstFormInfo=firstFormInfo + "*"+ arrEntryChar.get(index).toString() + "*" + arrNumEntries.get(index).toString();
			    entryChar=arrEntryChar.get(index).toString();
			    numEntries=arrNumEntries.get(index).toString();
			 }

    	//check for access rights if form is called from a 'link form' link
      if (!calledFromForm.equals(""))
    	{

    		if (!arrFrmIds.contains(new Integer(formId)))
    		{
    		// check if the form is hidden and user has access to it

    			//out.println("Access" + lnkformB.hasFormAccess(formId, EJBUtil.stringToNum(userId)));
				if (! lnkformB.hasFormAccess(formId, EJBUtil.stringToNum(userId)))
				{
    			//user does not have access rights to this form
    			pageRight  = 0;
    			}
    		}

    	}

		//if the form link is called from patient portal, grant all rights
		String ignoreRights = "";
		ignoreRights = (String) tSession.getValue("pp_ignoreAllRights") ;

		String pp_linkedModuleFormAfter = "";

		if (StringUtil.isEmpty(ignoreRights))
		{
			ignoreRights = "false";
		}
		else
		{

			pp_linkedModuleFormAfter = (String) tSession.getAttribute("pp_linkedModuleFormAfter");
			pp_linkedModuleFormAfter=(pp_linkedModuleFormAfter==null)?"":pp_linkedModuleFormAfter;
			if (pp_linkedModuleFormAfter.length()==0) pp_linkedModuleFormAfter="N";
 		}

		int orgRight = 0;
		orgRight = userSiteB.getMaxRightForStudyPatient(EJBUtil.stringToNum(userId), personPK ,istudyId );

		if (orgRight > 0)
		{
			orgRight = 7;
		}

		if (orgRight < 4)
		{
			pageRight = 0;
		}

		if (ignoreRights.equals("true"))
		{
			pageRight = 7;
			pageRightPat = 7;
			formLibAppRight = '1';
			patProfileAppRight = '1';
			
			PatLoginJB patLoginJB = (PatLoginJB)tSession.getAttribute("pp_currentLogin");
			try {
				int portalUserId = StringUtil.stringToNum(patLoginJB.getPlId());
				if (patId != portalUserId) {
					pageRight = 0;
					pageRightPat = 0;
				}
				if (!StringUtil.isEmpty(patProtId)) {
					if (portalUserId != StringUtil.stringToNum(patEnrollB.getPatProtPersonId())
							|| StringUtil.stringToNum(patEnrollB.getPatProtPersonId()) < 1) {
						pageRight = 0;
						pageRightPat = 0;
					}
				}
			} catch(Exception e) {
				pageRight = 0;
				pageRightPat = 0;
			}
		}

		/////////////////////////////////

   		//JM: 112006
	if (pageRight >=4) {

	//if (((String.valueOf(formLibAppRight).compareTo("1") == 0) ||(String.valueOf(patProfileAppRight).compareTo("1") == 0)) &&(pageRight >=4))

	if (((String.valueOf(formLibAppRight).compareTo("1") == 0) || (String.valueOf(patProfileAppRight).compareTo("1") == 0)) )
	{
		if (calledFromForm.equals(""))
		{
		%>
<DIV class="BrowserTopn" id="div1">
		   <jsp:include page="patienttabs.jsp" flush="true">
		   <jsp:param name="pkey" value="<%=patId%>"/>
		   <jsp:param name="statDesc" value="<%=statDesc%>"/>
		   <jsp:param name="statid" value="<%=statid%>"/>
		   <jsp:param name="patProtId" value="<%=patProtId%>"/>
		   <jsp:param name="patientCode" value="<%=StringUtil.encodeString(patientCode)%>"/>
		   <jsp:param name="fromPage" value=""/>
		   <jsp:param name="page" value="patientEnroll"/>
		   </jsp:include>
</div>
<DIV class="tabFormBotN tabFormBotN_FRB_1 tabFormBotN_PSc_1" id="div2">
	<%
		} //end of if for calldFrom


  if (!(patProtId.equals("")) && (!(patProtId.equals("0")))) {
  	   		//get current patient status and its subtype



			String patStatSubType = "";
			String patStudyStat =  "";
			int patStudyStatpk = 0;

			patStatSubType = patB.getPatStudyCurrentStatusWithCodeSubType(studyId, String.valueOf(patId));
			patStudyStat = patB.getPatStudyStat();
			patStudyStatpk = patB.getId();
			statid = patStudyStat ;
			if (EJBUtil.isEmpty(patStatSubType))
				patStatSubType= "";

			///////////////////////////////////


	   if (calledFromForm.equals(""))
  		{
		%>

    <table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr>

    <%
	    studyTitle = StringUtil.escapeSpecialCharJS(studyTitle);
    %>
    <script language=javascript>
		var varViewTitle = htmlEncode('<%=studyNumber%>');
		var study_Title = htmlEncode('<%=studyTitle%>');
	</script>

	<td><font size="1">
	<%=LC.L_Study_Number%><%-- <%=LC.Std_Study%> Number*****--%>: <%=studyNumber%>&nbsp;<a href="study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId=<%=studyId%>"><img id = "activeImage" class = "unSelectedImage imageDim" src="../images/jpg/study.gif" onmouseover="return overlib(varViewTitle,CAPTION,'<%=LC.L_Study_Number%>' ,RIGHT , BELOW );" onmouseout="return nd();" ></a> &nbsp;&nbsp;&nbsp;&nbsp;
	<a href="#" onmouseover="return overlib(study_Title,CAPTION,'<%=LC.L_Study_Title%><%-- <%=LC.Std_Study%> Title*****--%>');" onmouseout="return nd();"><img border="0" src="./images/View.gif"/></a>  &nbsp;&nbsp;&nbsp;&nbsp;
    	<%if(protocolId != null){%>
	<!--Protocol Calendar: <%=protName%>-->
    	<%}%>
     </td>
     	 </tr>
     </table>




     	 <%
		}
			//Modified by Manimaran for November Enhancement PS4.
			if((patStatSubType.trim().equalsIgnoreCase("lockdown")))
			{ %>
				<P class = "defComments"><FONT class="Mandatory"><%=MC.M_PatStatLkdwn_CntEdit%><%-- <%=LC.Pat_Patient%>'s <%=LC.Std_Study%> status is 'Lockdown'.You cannot Add or Edit Form Responses.*****--%> </FONT>

				<%	if (calledFromForm.equals(""))
  					{
				%>
 	   				<A onclick="openWinStatus('<%=patId%>','<%=patStudyStatpk%>','<%=pageRight%>','<%=studyId%>')"  href="#"  ><%=LC.L_Click_Here%><%-- Click Here*****--%></A>
					<FONT class="Mandatory"> <%=MC.M_ToChg_TheStat%><%-- to change the status.*****--%></FONT>
				<%
				}
				%>
				</P>
		<%	}
		else
		{
			if (calledFromForm.equals(""))
			 {
				if ( entryChar.equals("M") ){//for multiple entry forms only

		 %>

		 <%
		 	}
			 }
		 	else
		 	{
				if ( entryChar.equals("M")){//for multiple entry forms only
		 	%>

		 <%
				}
		 	}
		}
		 %>




	<%


     		 for (int i=0;i<arrFrmIds.size();i++)
     		 {  //store the formId, entryChar and num Entries separated with a *
     		  	 frmInfo = arrFrmIds.get(i).toString() + "*"+ arrEntryChar.get(i).toString() + "*" + arrNumEntries.get(i).toString();
     		 	 arrFrmInfo.add(frmInfo);
     		 }

		 if (!hideLab)
		 {
		  //to add lab entry in the DD

		 arrFrmInfo.add("lab");
    		 arrFrmNames.add("Lab");
		 }

 /*    	 	 String dformPullDown = EJBUtil.createPullDownWithStrNoSelect("formPullDown",firstFormInfo, arrFrmInfo , arrFrmNames);
	 		 StringBuffer formBuffer = new StringBuffer(dformPullDown);

			 //submit the form when the user selects the form name
		 	 if (calledFromForm.equals(""))
			 {
				 formBuffer.replace(0,7,"<SELECT onChange=\"document.stdpatform.submit();\"");
			 } else
			  {
			 	 //formBuffer.replace(0,7,"<SELECT DISABLED onChange=\"document.stdpatform.submit();\"");
			  }

			 dformPullDown = formBuffer.toString();*/

     		 //date filter
     	  	 String formFillDt = "";
     	 	 formFillDt = request.getParameter("formFillDt") ;
      	 	 if (formFillDt == null) formFillDt = "ALL";
     		 String dDateFilter = EJBUtil.getRelativeTimeDD("formFillDt",formFillDt);

     	   String linkFrom = "SP";
     	   String hrefName = "formfilledstdpatbrowser.jsp?srcmenu="+src;
     	   String modifyPageName = "patstudyformdetails.jsp?srcmenu="+src;
     	   String formName= "";
		   String formStatus = "";
	       String statSubType = "";

   		   if (formId >0) {
	     	   formLibB.setFormLibId(formId);
   	   		   formLibB.getFormLibDetails();
			   formName=formLibB.getFormLibName();

		   	   formStatus = formLibB.getFormLibStatus();
			   CodeDao cdao = new CodeDao();
			   cdao.getCodeValuesById(EJBUtil.stringToNum(formStatus));
			   ArrayList arrSybType = cdao.getCSubType();
			   statSubType = arrSybType.get(0).toString();
		   }
// Added by Gopu to fix the bugzilla issue # 2808
		formLibB.setFormLibId(formId);
		String formLibVer = formLibB.getFormLibVersionNumber(formId);
     	%>

     		<Form method="post" name="stdpatform" action="formfilledstdpatbrowser.jsp" onsubmit="">
     		<input type="hidden" name="srcmenu" value=<%=src%>>
     		<input type="hidden" name="selectedTab" value=<%=selectedTab%>>
     		<input type="hidden" name="studyId" value=<%=studyId%>>
     		<input type="hidden" name="statDesc" value=<%=statDesc%>>
     		<input type="hidden" name="statid" value=<%=statid%>>
     		<input type="hidden" name="patProtId" value=<%=patProtId%>>
     		<input type="hidden" name="patientCode" value=<%=StringUtil.encodeString(patientCode)%>>
     		<input type="hidden" name="mode" value=<%=mode%>>
     		<input type="hidden" name="pkey" value=<%=patId%>>
   			<input type="hidden" name="calledFromForm" value=<%=calledFromForm%>>
   			<input type="hidden" name="fk_sch_events1" value=<%=fk_sch_events1%>>
   			<input type="hidden" name="fkStorageForSpecimen" value=<%=fkStorageForSpecimen%>>

			<input type="hidden" name="previousPage" value="studyPat"/>





     		<table width="98%" align="center">

     		<tr>
				<% if (calledFromForm.equals(""))
				 {

				if (entryChar.equals("M") && ignoreRights.equals("false")){ %>

				<td width="45%" align="right">
				<P class="blackComments"><%=MC.M_Prev_EntriesForFrm%><%-- Previous entries for Form*****--%>: <B>"<%=formName%>"</B></P>
				</td>
					<% }
				} %>
				<%
    			//Added For Bug# 9255 :Date 14 May 2012 :By YPS
    		     if(!specimenPk.equals("")){
    		    	if(formDispType.equalsIgnoreCase("PR") && entryChar.equals("E")) {
    		    		Hashtable<String,String> htParams = new Hashtable<String,String>();
    		    		htParams.put("formDispType",formDispType);
    		    		htParams.put("entryChar",entryChar);
    		    		htParams.put("patProtId" ,patProtId );
    		    		LinkedFormsDao lnkFrmDao1 =new LinkedFormsDao();
    		    		lnkFrmDao1 = lnkformB.getPatStdFormsForPatRestrAndSingleEntry(iaccId,personPK,istudyId, EJBUtil.stringToNum(userId), EJBUtil.stringToNum(siteId),htParams);
    		    		arrFrmIds = lnkFrmDao1.getFormId();
    		    		arrNumEntries = lnkFrmDao1.getFormSavedEntriesNum();
    		    		int indexOfFormId =  arrFrmIds.indexOf(formId);
    		    		if(indexOfFormId!=-1){
    		    			numEntries = arrNumEntries.get(indexOfFormId).toString();
    		    		}
    		     }
    		    	 
    		     }
				%>
				<td width="10%">
					<% //Only Active forms can be answered
					  //Modified by Manimaran for November Enhancement PS4.
					  if ( (! patStatSubType.trim().equalsIgnoreCase("lockdown")) && statSubType.equals("A") )
					  {

					  	if (   (entryChar.equals("M")) || (entryChar.equals("E") && numEntries.equals("0") ) )
					  	{

							String jsCheck="";
						  	String hrefurl = "";

						  	hrefurl= "patstudyformdetails.jsp?srcmenu="+src+"&selectedTab="+selectedTab+"&formId="+formId+"&mode=N&formDispLocation=SP&entryChar="+entryChar+"&pkey="
						  	+	patId+"&patProtId="+ patProtId +"&studyId="+studyId+"&calledFrom=S&formFillDt="+formFillDt+"&formPullDown="+firstFormInfo+"&calledFromForm="+calledFromForm +
						  	"&statDesc="+statDesc+"&statid="+ statid + "&patientCode="+StringUtil.encodeString(patientCode) +
						  	"&schevent="+fk_sch_events1+"&responseId="+responseId + "&parentId=" + parentId+ "&specimenPk=" + specimenPk + "&fkStorageForSpecimen=" +fkStorageForSpecimen  ;

						  	if (!StringUtil.isEmpty(outputTarget))
						  	{
						  	 	hrefurl= hrefurl +  "&outputTarget=" + outputTarget;

						  		jsCheck = " onClick='openWindowCommon(\""+hrefurl+"\",\""+ outputTarget +"\");' ";
						  		hrefurl = "#";
						  	}
						  	else
						  	{
						  		jsCheck = " onClick =\" return checkPerm(" +pageRightPat+ " )\"";
						  	}

					  %>
					  	<A id= "newlink" type="submit" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="border:1px solid;padding: 5px 7px;border-radius: 0px; text-decoration: none;color:white;" <%=jsCheck%> HREF="<%=hrefurl%>" ><%=LC.L_New%></A>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<%
						}
					}
						%>
					</td>



			<%if (firstFormInfo.equals("lab")){} else if (entryChar.equals("M")){ %>

     		<!--<td>
     		<P class="blackComments">Previous entries for Form: <B>"<--%=formName--%>"</B>  </P>
     		</td>-->

     		<% if (calledFromForm.equals(""))
			{ %>
     		<td width="20%"><P class="blackComments"><%=LC.L_Filter_ByDate%><%-- Filter By Date*****--%> &nbsp;&nbsp; <%=dDateFilter%> </P></td>
     		<td width="23%"><button type="submit" onClick="javascript:fnfiltering(document.stdpatform.formFillDt.value)"><%=LC.L_Search%></button>
			</td>
     		<% } %>

     		</tr>
     		</table>
			<%


			HttpSession tS2 = request.getSession(true);
			String sess_val;
			sess_val=(String)tS2.getValue("Sess");


				%>
				<input type="hidden" name="sess_val" value=<%=sess_val%>>

			<script>

			var form_names;

			function fnfiltering(form_names)
			{
				document.stdpatform.action="formfilledstdpatbrowser.jsp?formPullDown=<%=sess_val%>";
			}
			</script>
		<%}%>



     	    </Form>

     <!--include the jsp for showing the records-->
     <%
	if(selectedStudy == null) {
	    selectedStudy = "default" ;
	}

	if (firstFormInfo.equals("lab")){
     %>
	<!--Added by Manimaran to fix the Bug2388 -->
	 <jsp:include page="labbrowser.jsp" flush="true">
         <jsp:param name="selectedTab" value="<%=selectedTab%>"/>
	 <jsp:param name="mode" value="<%=mode%>"/>
	 <jsp:param name="page" value="patient"/>
	 <jsp:param name="pkey" value="<%=patId%>"/>
	 <jsp:param name="patientCode" value="<%=StringUtil.encodeString(patientCode)%>"/>
	 <jsp:param name="fromlab" value="patstd"/>
	 <jsp:param name="patProtId" value="<%=patProtId%>"/>
	 <jsp:param name="studyId" value="<%=studyId%>"/>
	 <jsp:param name="patStatSubType" value="<%=patStatSubType%>"/>
	 <jsp:param name="pageRightPat" value="<%=pageRightPat%>"/>
	 <jsp:param name="studyNumber" value="<%=studyNumber%>"/>
	 <jsp:param name="frmLab" value="<%=fromLab%>"/>
	 <jsp:param name="selectedStudy" value="<%=selectedStudy%>"/>
	 </jsp:include>

	<%}else{

			 if (arrFrmIds.size() > 0 || (ignoreRights.equals("true") && ( pp_linkedModuleFormAfter.equals("P") || entryChar.equals("E") )  ) || (calledFromPatSch.equals("true")) )
			 {


			 	if (formId>0 && ignoreRights.equals("false"))
				{
				  index=arrFrmIds.indexOf(new Integer(formId));
				  if (index>=0)
				  lfType=(String)arrLFTypes.get(index);
				}


				 //Modified by Manimaran for November Enhancement PS4.
				 if (patStatSubType.trim().equalsIgnoreCase("lockdown"))
			 	 {
			 	 	 pageRight = 4;
			 	 }
	if(specimenPk==null || specimenPk.isEmpty()){%>
	<DIV class="browserDefault" id="div1">
	<%}else{ %>
	<DIV id="div1">
	<% }%>
	<!-- Bug#7039 16-May-2012 -Sudhir-->
	
     	 <jsp:include page="formdatarecords.jsp" flush="true">
      	 <jsp:param name="formId" value="<%=formId%>"/>
     	 <jsp:param name="linkFrom" value="<%=linkFrom%>"/>
     	 <jsp:param name="linkFrom" value="<%=linkFrom%>"/>
		 <jsp:param name="lftype" value="<%=lfType%>"/>
		 <jsp:param name="patprotid" value="<%=patProtId%>"/>
		 <jsp:param name="hrefName" value="<%=hrefName%>"/>
		 <jsp:param name="modifyPageName" value="<%=modifyPageName%>"/>
     	 <jsp:param name="selectedTab" value="<%=selectedTab%>"/>
     	 <jsp:param name="entryChar" value="<%=entryChar%>"/>
     	 <jsp:param name="formFillDt" value="<%=formFillDt%>"/>
     	 <jsp:param name="formPullDown" value="<%=firstFormInfo%>"/>
    	 <jsp:param name="srcmenu" value="<%=src%>"/>
    	 <jsp:param name="pageRight" value="<%=pageRight%>"/>
    	 <jsp:param name="statSubType" value="<%=statSubType%>"/>
		 <jsp:param name="protocolId" value="<%=protocolId%>"/>
		 <jsp:param name="calledFromForm" value="<%=calledFromForm%>"/>
		 <jsp:param name="mode" value="<%=mode%>"/>
		 <jsp:param name="fk_sch_events1" value="<%=fk_sch_events1%>"/>
		 <jsp:param name="studyNumber" value="<%=studyNumber%>"/>
		 <jsp:param name="formLibVer" value="<%=formLibVer%>"/>
		 <jsp:param name="outputTarget" value="<%=outputTarget%>"/>
		 <jsp:param name="specimenPk" value="<%=specimenPk%>"/>
		 <jsp:param name="fkStorageForSpecimen" value="<%=fkStorageForSpecimen%>"/>
		 <jsp:param name="previousPage" value="studyPat"/>
		 <jsp:param name="formDispType" value="<%=formDispType%>"/>
     	 </jsp:include>
		 </div>
	 	 <%
			 }
	   else if (ignoreRights.equals("false"))
	   {
	%>
	  <P class="defComments"><%=MC.M_NoAssocForms%><%-- No associated Forms.*****--%></P>
    <%  }

    if ( ignoreRights.equals("true") && pp_linkedModuleFormAfter.equals("N") )
    {
	    %>
	    	<script>
				var newl = document.getElementById("newlink");
				//alert("newl " + newl );
				//newl.click();
				window.location =newl ;
	    	</script>
	    <%
     }
   }
  } // if patProtId
  else {%>
  <P class="defComments">
  	<!--  This patient has not been enrolled yet. Please enroll patient before answering Forms. -->
  	  <%=MC.M_NoStat_AddedPat%><%-- There is no <%=LC.Std_Study_Lower%> status added for this <%=LC.Pat_Patient_Lower%>. Please add a <%=LC.Std_Study_Lower%> status before answering Forms.*****--%>
  </P>
  <%}


	} //end of if body for page right
	else{ %>

	<jsp:include page="accessdenied.jsp" flush="true"/>
	<%


	}

	}//JM: page right
	else{
%>
  <!--jsp:include page="accessdenied.jsp" flush="true"/-->
  <br><br><br><table align=center><tr><td><P class="defComments" >
  	<%=MC.M_NoFrmBcos_DelOrNoRgts%> <%-- The Form is not available because of following reasons: <BR>
  	1.This form has been deleted<BR>
  	2. OR You do not have access rights to enter or view responses for this form*****--%>
  </P></td></tr></table>
<%
	} //end of else body for page right
}//end of if body for session
else {
%>
	<jsp:include page="timeout.html" flush="true"/>
<%}
%>
</div>
<br>
<div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
</div>
<%if (calledFromForm.equals(""))
  {
%>
<DIV class="mainMenu" id = "emenu">
  <jsp:include page="getmenu.jsp" flush="true"/>
</DIV>

<%
}
		%>
	<script>
		window.focus();
	</script>
</body>
</html>
