<%@page import="com.velos.eres.web.network.NetworkJB"%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language = "java" import = "com.velos.eres.business.site.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%><%@page import="com.velos.eres.service.util.*"%>
<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id="groupB" scope="page" class="com.velos.eres.web.group.GroupJB"/>
<%
 
	request.setCharacterEncoding("UTF-8");
	response.setContentType("text/html");
	HttpSession tSession = request.getSession(false);
	//JSONObject jsObj = new JSONObject();
	if (!sessionmaint.isValidSession(tSession)) {
		// Session is invalid; print an error and exit
		response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");
		//jsObj.put("result", -1);
		//jsObj.put("resultMsg", MC.M_UsrNot_LoggedIn);/*jsObj.put("resultMsg", "User is not logged in.");*****/
   	%>
		<input type="hidden" name="resultMsg" id="resultMsg" value="<%=MC.M_UsrNot_LoggedIn %>"/>    	
	<%
		//return;
	}
	String uName = (String) tSession.getValue("userName");
	int pageRight = 0;
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MSITES"));
	
	int accountId=0;
	String calledFrom;
	calledFrom= request.getParameter("calledFrom");
	String src = request.getParameter("src");
	String from=(request.getParameter("from")==null)?"":request.getParameter("from");
	String acc = (String) tSession.getValue("accountId");
	accountId = EJBUtil.stringToNum(acc);
	String userId = (String) tSession.getValue("userId");
	int pkuser = EJBUtil.stringToNum(userId);
	userB.setUserId(pkuser);
	userB.getUserDetails();
	String defGroup = userB.getUserGrpDefault();
	groupB.setGroupId(EJBUtil.stringToNum(defGroup));
	groupB.getGroupDetails();
	String groupName = groupB.getGroupName();
	String ipAdd = (String) tSession.getValue("ipAdd");
	String flag = request.getParameter("flag");
	String searchNWUser=request.getParameter("searchNWUser");
	Integer netId=0;
	netId = EJBUtil.stringToNum(request.getParameter("netWorkId"));
	String nLevel = (request.getParameter("nLevel")==null)?"":request.getParameter("nLevel");
	NetworkDao nwdao = new NetworkDao();
	nwdao.getNetworkUsers(netId,accountId,searchNWUser);
	ArrayList networkUsrPkList = nwdao.getNetworkUsrPkList();
	ArrayList networkUsersList = nwdao.getNetworkUsersList();
	ArrayList networkMemeberTRoleList = nwdao.getNetworkMemeberTRoleList();
	ArrayList networkMemeberADTRoleList = nwdao.getNetworkMemeberADTRoleList();;
	ArrayList networkTRoleNameList = nwdao.getNetworkTRoleNameList();
	ArrayList networkADTRoleNameList = nwdao.getNetworkADTRoleNameList();
	ArrayList networkUsrNameList = nwdao.getNetworkUsrNameList();
	ArrayList networkUsrstatusList = nwdao.getNetworkUsrStatusList();
	ArrayList networkUsrTypeList = nwdao.getNetworkUsrTypeList();
	ArrayList networkUserStatusList = nwdao.getNetworkUserStatusList();
	ArrayList stathistoryId = nwdao.getHistoryIdsList();
	int len=0;
	len=nwdao.getcRows();
	String ntUsrAddlpk="";
	String ntUsrAddlroledesc="";
	String ntUsrAddlstatdesc="";
	String ntUsrAddlstathistory="";
	String ntUsrAddlrole="";
	String ntUsrAddlstatus="";
	String ntUsrPk = "";
    String ntUsr = "";
    String ntUsrName = "";
    String ntUsrStatus = "";
    String ntTRole = "";
    String ntADTRole= "";
    String ntTRoleName = "";
    String ntTRoleNameMulti="";
    String ntADTRoleName="";
    String ntUsrType = "";
    String ntUserStatus = "";
    String historyId = "";
    
	%>
	<table border="2"  width="100%">
<tr style="color: #D3D3D3">
<th width="25%"><%=LC.L_User_Name%></th>
<!-- <th width="20%"><%=LC.L_User_Status%></th> -->
<th width="45%"><%=LC.L_NetRole%></th>
<!-- <th width="20%"><%=LC.L_Net_Add_Role%></th>  -->
<th width="20%"><%=LC.L_Documents%></th>
<%if(from.equals("studyNetwork")){
	if(groupName.equalsIgnoreCase("Admin")){%>
	<th width="10%" style="display:none"><%=LC.L_Delete%></th>
<%}}else{
	if(groupName.equalsIgnoreCase("Admin")){%>
	<th width="10%"><%=LC.L_Delete%></th>
<%}}%>
</tr>
<%
for(int counter = 0;counter<len;counter++)
{ 
	//ntADTRoleName="";
	 ntUsrPk=networkUsrPkList.get(counter).toString();
	 ntUsr=networkUsersList.get(counter).toString();
	 ntUsrName=((networkUsrNameList.get(counter))==null)?"":networkUsrNameList.get(counter).toString();
	 ntUsrStatus=((networkUsrstatusList.get(counter))==null)?"":networkUsrstatusList.get(counter).toString(); 
	 ntTRole=((networkMemeberTRoleList.get(counter))==null)?"":networkMemeberTRoleList.get(counter).toString();
	// ntADTRole = ((networkMemeberADTRoleList.get(counter))==null)?"":networkMemeberADTRoleList.get(counter).toString();
	// System.out.println("ntTRole-"+ntTRole);
	 ntTRoleName=((networkTRoleNameList.get(counter))==null)?"":networkTRoleNameList.get(counter).toString();
	 //if(!ntADTRole.equalsIgnoreCase("")){
	// String[] ntADTRoleMulti = ntADTRole.split(",");
	// for(int i=0;i<ntADTRoleMulti.length;i++){
		// String id = ntADTRoleMulti[i];
		// ntADTRoleName+= nwdao.codelst_desc(id)+","; 
		
	 //}
	// ntADTRoleName = ntADTRoleName.substring(0,ntADTRoleName.length()-1);
	// }
	 //ntADTRoleName=((networkADTRoleNameList.get(counter))==null)?"":networkADTRoleNameList.get(counter).toString();
	 ntUsrType = ((networkUsrTypeList.get(counter))==null)?"":networkUsrTypeList.get(counter).toString();
	 ntUserStatus = ((networkUserStatusList.get(counter))==null)?"":networkUserStatusList.get(counter).toString();
	 historyId = ((stathistoryId.get(counter))==null)?"":stathistoryId.get(counter).toString();
	 CodeDao cd1 = new CodeDao();
     cd1.getCodeValues("nwusersrole");
     String dNetTRole="";
     String dNetADTRole="";
     String dNetAdditonalTRole = "";     
     String dNetUStatus="";
     dNetTRole=cd1.toPullDown("ntusrroleId_"+ntUsrPk+"_"+ntTRole,StringUtil.stringToInteger(networkMemeberTRoleList.get(counter).toString()),"style='display:none;' onblur='openDDList(this.id,1,7);'");
     dNetAdditonalTRole=cd1.toPullDown("ntusradroleId_pkaddnl_0",0,"style='display:none;' onblur='openDDListMul(this.id,1,7);'");
     dNetAdditonalTRole= dNetAdditonalTRole.replaceAll("'", "@");
     if("0".equals(ntTRole)){
    	 ntTRoleName=LC.L_Select_AnOption;
    	 
     }
     //if(ntADTRole.equalsIgnoreCase("")|| ntADTRole==null){
     //ntADTRoleName=LC.L_Select_AnOption;
    	 
   // }
%>

<tr id="row_<%=ntUsrPk%>">
<td ><A href="#" onclick="fOpenNetworkUser(<%=ntUsr%>,<%=ntUsrPk%>,'<%=nLevel%>','<%=ntUsrType%>','<%=from%>');"><%=ntUsrName %></A></td>

<%if(from.equals("studyNetwork")){ %>
<td><span id="span_<%=ntUsrPk%>_<%=ntTRole%>"><%=ntTRoleName %></span><%=dNetTRole %>&nbsp;&nbsp;
<span id="span_status_<%=ntUsrPk%>_<%=ntUserStatus%>"><%=ntUsrStatus%></span>&nbsp;&nbsp;
<%}else{ %>
<td id ="additon_role_<%=ntUsrPk%>"><br>
<div style="float:left;width:60%"><span style="margin-left: 5px;" id="span_<%=ntUsrPk%>_<%=ntTRole%>" onclick="openDDList(this.id,0,'<%=pageRight%>');"><%=ntTRoleName %></span><%=dNetTRole %></div>
<div style="float:left;width:15%;margin-left:1px"><A href="#" id="span_status_<%=ntUsrPk%>_<%=ntUserStatus%>" onclick="openWinStatus(<%=pageRight%>,'M','er_nwusers','<%=ntUsrPk%>','<%=historyId%>')"><%=ntUsrStatus%></A></div>
<div style="float:left;width:18%"><A href="#" onclick="openWinStatus(<%=pageRight%>,'N','er_nwusers', '<%=ntUsrPk%>','0')"><img style="height:19px;width:19px;" src="./images/edit.gif" title="<%=LC.L_Edit%>" border="0"/></A>
<A href="showinvoicehistory.jsp?modulePk=<%=ntUsrPk%>&pageRight=<%=pageRight%>&srcmenu=<%=src%>&from=ntwusrhistory&fromjsp=usersnetworksites.jsp&currentStatId=<%=historyId%>&moduleTable=er_nwusers&netWorkId=<%=netId%>&nLevel=<%=nLevel%>"><img border="0" title="<%=LC.L_History%>" alt="<%=LC.L_History%>" style="height:19px;width:19px;" src="./images/History.gif" ><%//=LC.L_H%><%--H*****--%></A>
<img border="0" style="height:19px;width:19px;cursor:pointer;" src="./images/Add.gif" onclick="addrow(<%=ntUsrPk%>,<%=pageRight%>,'<%=historyId%>',<%=src%>,'<%=netId%>','<%=nLevel%>','<%=dNetAdditonalTRole%>')" >
</div>
<%}
NetworkDao nwdao1 = new NetworkDao();
nwdao1.getNetworkAdditonalRoles(StringUtil.stringToInteger(ntUsrPk));
ArrayList networkUsersAddnlList = nwdao1.getNetworkAdlPkList();
ArrayList networkMemeberAddnlstatus = nwdao1.getNetworkAdtstatus();
ArrayList networkMemeberAddnlTRoleList = nwdao1.getNetworkAdtrole();
ArrayList networkMemeberAddnlTRoledesc = nwdao1.getNetworkADDTLRoleNameList();
ArrayList networkMemeberAddnlTRolestatdesc = nwdao1.getNetworkADDTLRoleStatus();
ArrayList networkMemeberAddnlTRolestathistory = nwdao1.getNetworkAdlPkHistory();
//

int len1=0;
len1=nwdao1.getcRowsaddl();
for(int count = 0; count<len1;count++){

ntUsrAddlpk = ((networkUsersAddnlList.get(count))==null?"":networkUsersAddnlList.get(count).toString());
System.out.println(ntUsrAddlpk+ntUsrAddlrole+ntUsrAddlstatus);
ntUsrAddlrole= ((networkMemeberAddnlstatus.get(count))==null?"":networkMemeberAddnlstatus.get(count).toString());
ntUsrAddlstatus = ((networkMemeberAddnlTRoleList.get(count))==null?"":networkMemeberAddnlTRoleList.get(count).toString());
ntUsrAddlroledesc= ((networkMemeberAddnlTRoledesc.get(count))==null?"":networkMemeberAddnlTRoledesc.get(count).toString());

ntUsrAddlstatdesc = ((networkMemeberAddnlTRolestatdesc.get(count))==null?"":networkMemeberAddnlTRolestatdesc.get(count).toString());
ntUsrAddlstathistory = ((networkMemeberAddnlTRolestathistory.get(count))==null?"":networkMemeberAddnlTRolestathistory.get(count).toString());
dNetADTRole=cd1.toPullDown("ntusradroleId_"+ntUsrAddlpk+"_"+ntUsrAddlrole,StringUtil.stringToInteger(networkMemeberAddnlstatus.get(count).toString()),"style='display:none;' onblur='openDDListMul(this.id,1,7);'");

if("0".equals(ntUsrAddlrole)){
	ntUsrAddlroledesc=LC.L_Select_AnOption;
	 
}
%>
<table style="width:100%">
<tr id="ntUsrAddlpk_<%=ntUsrAddlpk%>">
<td id ="additon_Role_<%=ntUsrAddlpk%>">
<%if(from.equals("studyNetwork")){ %>
<span id="span_<%=ntUsrAddlpk%>_<%=ntUsrAddlrole%>"><%=ntUsrAddlroledesc %></span><%=dNetADTRole %>&nbsp;&nbsp;
<span id="span_addstatus_<%=ntUsrAddlpk%>_<%=ntUsrAddlstatus%>"><%=ntUsrAddlstatdesc%></span>&nbsp;&nbsp;
</td></tr></table>
<%}else{ %>
<div style="float:left;width:60%"><span style="margin-left:0px;" id="span_<%=ntUsrAddlpk%>_<%=ntUsrAddlrole%>" onclick="openDDListMul(this.id,0,'<%=pageRight%>');"><%=ntUsrAddlroledesc %></span><%=dNetADTRole %></div>
<div style="float:left;width:15%"><A href="#" id="span_addstatus_<%=ntUsrAddlpk%>_<%=ntUsrAddlstatus%>" onclick="openWinStatus(<%=pageRight%>,'M','er_nwusers_addnlroles','<%=ntUsrAddlpk%>','<%=ntUsrAddlstathistory%>')"><%=ntUsrAddlstatdesc%></A></div>
<div style="float:left;width:18%"><A href="#" onclick="openWinStatus(<%=pageRight%>,'N', 'er_nwusers_addnlroles','<%=ntUsrAddlpk%>','0')"><img style="height:19px;width:19px;" src="./images/edit.gif" title="<%=LC.L_Edit%>" border="0"/></A>
<A href="showinvoicehistory.jsp?modulePk=<%=ntUsrAddlpk%>&pageRight=<%=pageRight%>&srcmenu=<%=src%>&from=ntwusrhistory&fromjsp=usersnetworksites.jsp&currentStatId=<%=ntUsrAddlstathistory%>&moduleTable=er_nwusers_addnlroles&netWorkId=<%=netId%>&nLevel=<%=nLevel%>"><img border="0" title="<%=LC.L_History%>" alt="<%=LC.L_History%>" style="height:19px;width:19px;" src="./images/History.gif" ><%//=LC.L_H%><%--H*****--%></A>
<a href="#" ><img style="height:19px;width:19px;" src="./images/delete.gif" onclick="deleteUserNetworkAddRole(<%=ntUsrAddlpk%>);" title="Delete" border="0"></a>
</div>
</td></tr></table>
<%}
%>
 <!--  <td><span id="span1_<%=ntUsrPk%>_1" onclick="openDDListMul(this.id,0,'<%=pageRight%>');"><%=ntADTRoleName %></span><%=dNetADTRole %></td> --> 
<%} %>
</td>
<td align="center"><img title="Network Appendix" style="height:19px;width:19px;cursor:pointer;" onclick="networkUserSitesAppendix('<%=ntUsrPk %>','<%=from %>','<%=ntUsr%>','<%=pageRight %>');" src="./images/Appendix.gif" border="0"></td>
<%if(from.equals("studyNetwork")){%>
	<td style="display: none;align:center"><a href="#" ><img style="height:19px;width:19px;" src="./images/delete.gif" onclick="deleteUserNetwork(<%=ntUsrPk%>);" title="Delete" border="0"></a></td>
<%}else{
		if(groupName.equalsIgnoreCase("Admin")){%>
	<td align="center"><a href="#" ><img style="height:19px;width:19px;" src="./images/delete.gif" onclick="deleteUserNetwork(<%=ntUsrPk%>);" title="Delete" border="0"></a></td>
<%}}%>
</tr>
<%} %>
</table>