<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<html>
<head>
<title><%=MC.M_Fld_SQuryHist%><%--Field(s)Query History*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<jsp:useBean id="groupB" scope="request" class="com.velos.eres.web.group.GroupJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="formQueryDao" scope="request" class="com.velos.eres.business.common.FormQueryDao"/>
<jsp:useBean id="formQueryB" scope="request" class="com.velos.eres.web.formQuery.FormQueryJB"/>

<%@ page language = "java" import = "com.velos.eres.business.section.*,java.util.*,com.velos.eres.service.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB"%>



<DIV id="div1"> 
<script type="text/javascript">

</script>

<%-- <b><p class="defcomments"><%=LC.L_Frms_QryHist%></p></b>--%>
 <%
   HttpSession tSession = request.getSession(true);   
	
      int pageRight = 0;
      pageRight = EJBUtil.stringToNum((String)session.getAttribute("formQueryRight"));
	  String userId = (String) tSession.getValue("userId");
      String accountId = (String) tSession.getValue("accountId");
	  String loginUserName = 	(String) tSession.getValue("userName"); 
	  int formId = EJBUtil.stringToNum(request.getParameter("formId"));  
	  String filledFormId =  request.getParameter("filledFormId");
	  String from = request.getParameter("from");
	  String windowType=request.getParameter("windowType");
	  String filedIdStr = request.getParameter("filedId");
	  String responseIdStr = 	request.getParameter("responseId");
	  String formName=request.getParameter("formName");
	  int filedId = 0;
	  int responseId = 0;	
	if(filedIdStr != null && responseIdStr != null){
		try{			
			filedIdStr = filedIdStr.replace("_span","");			
			responseId = Integer.parseInt(responseIdStr);			
		}catch(Exception e){
			System.out.print("error:  " + e.getMessage());
		}
		
	} 
	String showLink = request.getParameter("showLink");	
	if (showLink == null || EJBUtil.isEmpty(showLink))
		showLink = "1";	 
		GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
		String studyId ="";
    	studyId = request.getParameter("studyId");
	 	if (StringUtil.isEmpty(studyId))
	 	{
	 		studyId="";
	 	}
     String formQueryId = "";
     String fieldName = "";
     int count = 0;
     int iaccId=EJBUtil.stringToNum(accountId);    
	 	 formQueryId = request.getParameter("formQueryId");
	 	 fieldName = request.getParameter("fieldName");		 
	 	 fieldName=(fieldName==null)?"":fieldName;
		 fieldName =StringUtil.decodeString(fieldName);	
		 int queryStatusId = 0;
		 int formQuery=0;
		 String entrdOn = "";
		 int fieldId = 0; 
		 String queryStat = "";
		 String entrdBy = "";
		 String queryType = "";
		 String formQueryType = "";
		 String note = "";
		 String type = "";		
		String userIdFromSession = (String) tSession.getValue("userId");
     	UserDao uDao = new UserDao();
		uDao.setUsrIds(EJBUtil.stringToInteger(userIdFromSession));
		uDao.getUsersDetails(userIdFromSession);
		String groupId = "";
		groupId = (String) uDao.getUsrDefGrps().get(0);
		CodeDao cd1 = new CodeDao();
		String roleCodePk="";
		String roleSubType="";
		if (! StringUtil.isEmpty(studyId) && EJBUtil.stringToNum(studyId) > 0)
		{
			ArrayList tId = new ArrayList();
			TeamDao teamDao = new TeamDao();
			teamDao.getTeamRights(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(userIdFromSession));
			tId = teamDao.getTeamIds();
			if (tId != null && tId.size() > 0)
			{
				ArrayList arRoleIds = new ArrayList();
				arRoleIds = teamDao.getTeamRoleIds();
				if (arRoleIds != null && arRoleIds.size() >0 )	
				{
					roleCodePk = (String) arRoleIds.get(0);
					roleSubType = cd1.getCodeSubtype(EJBUtil.stringToNum(roleCodePk));
					
					if (StringUtil.isEmpty(roleCodePk))
					{
						roleCodePk="";
					}
				}	
				else
				{
					roleCodePk ="";
				}
				
			}	
			else
			{
				roleCodePk ="";
			}
			
			//Bug #5993
			if (roleCodePk.equals("")){
				int grpId = Integer.parseInt(groupId);
				groupB.setGroupId(grpId);
				groupB.getGroupDetails();
				if (groupB.getGroupSuperUserFlag().equals("1")){
					String rolePK = groupB.getRoleId();
					if (rolePK == null || rolePK.equals("0")){
						roleCodePk = "";
						roleSubType = "";
					}else{
						roleCodePk = rolePK;
						roleSubType = cd1.getCodeSubtype(EJBUtil.stringToNum(roleCodePk));
					}
				}
			}
		} else
			{			
			  roleCodePk ="";
			}  
		
	 	cd1.getCodeValuesForStudyRole("query_status",roleCodePk);
		
		String statusPullDn = "";
		statusPullDn = cd1.toPullDown("cmbStatus");
		CodeDao cd2 = new CodeDao();
		cd2.getCodeValues("form_query");
		String queryRespPulln = "";
		 queryRespPulln = cd2.toPullDown("cmbQueryResp");
		 ArrayList fieldNames=new ArrayList();
		 ArrayList formQueryIds=new ArrayList();
		//formQueryDao = formQueryB.getQueriesForForm(EJBUtil.stringToNum(formId), EJBUtil.stringToNum(filledFormId), EJBUtil.stringToNum(from));
		if("adverseForm".equals(formName)|| "patStaForm".equals(formName)){
		String	filedIdStr1 []=filedIdStr.split("\\[Velsep]");
		System.out.println("first String :"+filedIdStr1[0]+"  Second String :"+filedIdStr1[1]);
		String tempFieldId=filedIdStr1[0].substring(4);
		System.out.println("tempField-"+tempFieldId);
		formQueryDao = formQueryB.getOtherQueryHistoryForField(StringUtil.stringToNum(tempFieldId),responseId);
		fieldNames=	formQueryDao.getFieldNames();
		
		if(fieldNames.size()!=0){
			
		}else{
		fieldNames.add(filedIdStr1[1]);
		}
		}else{
		  formQueryDao = formQueryB.getCurrentQueryHistoryForField(filedIdStr,responseId);
		  fieldNames=	formQueryDao.getFieldNames();
		}
		  formQueryIds=formQueryDao.getFormQueryIds(); 
		  ArrayList queryStatusIds = formQueryDao.getFormQueryStatusIds();
		  ArrayList enteredOn = formQueryDao.getEnteredOn();
		  ArrayList queryStatus = formQueryDao.getQueryStatus();
		  ArrayList enteredBy = formQueryDao.getEnteredBy();
		  ArrayList queryTypes = formQueryDao.getQueryType();
		  ArrayList formQueryTypes = formQueryDao.getFormQueryType();
		  ArrayList notes = formQueryDao.getQueryNotes();
		  int rowsReturned = queryStatusIds.size();
		
	  %>
	  
<%
StringBuffer buttonStr=new StringBuffer();
if(windowType==null ||"undefined".equals(windowType) ){
	if("adverseForm".equals(formName)|| "patStaForm".equals(formName)){
		buttonStr.append("<input type='button' value='Save' onclick='saveQueryAndResponse(\""+filedIdStr+"\","+"\""+formQueryIds.get(0)+"\""+");'");
	}else{
buttonStr.append("<input type='button' value='Save' onclick='saveQueryAndResponse(\""+filedIdStr+"\");'");
	}
}else{
if("adverseForm".equals(formName)|| "patStaForm".equals(formName)){
	buttonStr.append("<input type='button' value='Save'  onclick='saveQuery(\""+filedIdStr+"\");' ");
	}else{
		if(rowsReturned==0){
	buttonStr.append("<input type='button' value='Save'  onclick='saveQuery(\""+filedIdStr+"\");' ");
		}
		else{
			buttonStr.append("<input type='button' value='Save'  onclick='saveQueryAndResponse(\""+filedIdStr+"\");' ");
		}
	}
}

%>	 
<table width="100%">
<tr>
<td align="left" id="windowTd" width="35%">
<%if("queryWin".equals(windowType) && rowsReturned==0){ %>
<b>Query:</b>
<%}else{ %>
<b>Responses:</b>
<%} %>
</td >
<td align="center" id="fieldTd" width="35%">
<b><u><%=fieldNames.get(0) %></u></b>
</td>
<td align="right" id="saveTd" width="30%">
<%=buttonStr.toString() %>
</td>
</tr>
</table>
	<%if("queryWin".equals(windowType) && rowsReturned==0){  %>
	<table width="100%" cellspacing="0" cellpadding="0" border="0"  class="basetbl">
	<tr>
	<th width=10%>Status <%--<%=LC.L_Qry_StatID--%><%--Query Status ID*****--%></th> <%-- YK 03DEC-- EDC_AT3/4 REQ--%>
	<th width=75%><%=LC.L_Comments%> <%--<%=LC.L_Date%><%--Date*****--%></th>
	<th width=15%><%=LC.L_Query_Type%><%--Query Type*****--%></th>
	</tr>
	<tr class="browserEvenRow">
	<td><%=statusPullDn %></td>
	<td><input style="width:100%" type="text" id="instructions" name="instructions" /></td>
	<td><%=queryRespPulln %></td>
	</tr>
	 </table>	 
<%}else{ %>
    <table width="100%" cellspacing="0" cellpadding="0" border="0"  class="basetbl">
       
       <tr> 
       <%-- YK 14DEC-- BUG #5650--%>
       <th width=3%></th>
       <th width=10%>Status <%--<%=LC.L_Qry_StatID--%><%--Query Status ID*****--%></th> <%-- YK 03DEC-- EDC_AT3/4 REQ--%>
       <th width=25%><%=LC.L_Comments%> <%--<%=LC.L_Date%><%--Date*****--%></th>
       <!--<th width=15%>Author--> <%--<%=LC.L_Status--%><%--Status*****--%></th> <%-- YK 03DEC-- EDC_AT3/4 REQ--%>
	   <th width=10%><%=LC.L_Date%> <%--<%=LC.L_QryOrResp%><%--Query/Response*****--%> </th>
	   <th width=3%><%=LC.L_Type%> <%-- <%=LC.L_Type%><%--Type*****--%></th>
	   <th width=15%><%=LC.L_QryOrResp%><%-- <%=LC.L_Comments%><%--Comments*****--%> </th>	   
	   <th width=20%><%=LC.L_Entered_By%><%--Entered By*****--%></th>	  
	   </tr>
	   
	     <tr class="browserEvenRow"> 
	     <td><%=statusPullDn %></td>
	     <td colspan="6"> <input style="width:100%" type="text" id="instructions" name="instructions" /></td>
	    
	     </tr>
	   <%
	    for(count = 0;count < rowsReturned;count++)
					{
						
	    	             formQuery=(   (Integer)formQueryIds.get(count)  ).intValue();
						 queryStatusId = (   (Integer)queryStatusIds.get(count)  ).intValue();					 
					 	 
						 entrdOn = ((enteredOn.get(count)) == null)?"-":(enteredOn.get(count)).toString();
						 
						 queryStat = ((queryStatus.get(count)) == null)?"-":(queryStatus.get(count)).toString();
						 
						 entrdBy = ((enteredBy.get(count)) == null)?"-":(enteredBy.get(count)).toString();
						 queryType = ((queryTypes.get(count)) == null)?"-":(queryTypes.get(count)).toString();
						 formQueryType = ((formQueryTypes.get(count)) == null)?"-":(formQueryTypes.get(count)).toString();
						 if(formQueryType.equals("1") || formQueryType.equals("2"))
						  type = "Q";
						 else
						  type = "R";
						 
						 note = ((notes.get(count)) == null)?"-":(notes.get(count)).toString();						 
						 
						if ((count%2)==0) 
						{
						%>
					      	<tr class="browserEvenRow">
						<%}else
						 { %>
				      		<tr class="browserOddRow">
						<%} %>
						
						<td align="center">
						<%if("Q".equals(type)){%>
						<input type="radio"  name="queryRadio" id="queryRadio" value="<%=formQuery%>">
						<%} %>
						</td>
						<td><%=queryStat%></td>  <%-- YK 03DEC-- EDC_AT3/4 REQ--%>
						<td>	
						<%=note%>
						</td>
					<%--<td>Author</td>  YK 03DEC-- EDC_AT3/4 REQ--%>
						<td>
						<% if(formQueryType.equals("1")  && entrdBy.equals("system-generated")){%>
						<%=entrdOn%>
						<%}else{%>	
						<%-- YK 03DEC-- EDC_AT3/4 REQ--%>									
						<%=entrdOn%>
						<%--<A href="addNewQuery.jsp?studyId=<%=studyId%>&queryStatusId=<%=queryStatusId%>&thread=old&mode=M&fieldName=<%=StringUtil.encodeString(fieldName)%>&formQueryId=<%=formQueryId%>&filledFormId=<%=filledFormId%>&from=<%=from%>&formId=<%=formId%>&entrdBy=<%=entrdBy%>&page=H"><%=entrdOn%></A> --%>
						<%-- YK 03DEC-- EDC_AT3/4 REQ--%>
						<%}%>
						</td>
						
						<td><%=type%></td>
						<td><%=queryType%></td>
						<td>
						<%if(type.equals("R") && entrdBy.equals("system-generated")){%>
						<%="Unknown User"%>
						<%}else{
						if(entrdBy.equals("system-generated")){%>
						&lt;<%=entrdBy%>&gt;
						<%}else{%>
						<%=entrdBy%>						
						<%}
						}%>
						</td>						
			    	 	</tr>
						
				<%	}//counter			
	%>
	  </table>
	 <%} %> 
</body>

</html>
<%!
private boolean isAccessibleFor(int pageRight, char type) {
    switch(type) {
    case 'V': return ((pageRight & 0x0004) >> 2) == 1;
    case 'E': return ((pageRight & 0x0002) >> 1) == 1;
    case 'N': return  (pageRight & 0x0001) == 1;
    }
    return false;
}
%>