<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Del_Schs%><%--DeleteSchedules*****--%></title>
	<%-- Nicholas : Start --%>
	<%--<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>--%>
	<%-- Nicholas : End --%>
</head>
	<%-- Nicholas : Start --%>
	<jsp:include page="skinChoser.jsp" flush="true"/>
	<%-- Nicholas : End --%>
<SCRIPT>

function checkAll(formobj){
           act="check";
     if(formobj.totcount){
    	
           totcount=formobj.totcount.value;
        
    if (formobj.deleteAll.value=="checked") 
	   act="uncheck";
    if (totcount==1){
       if (act=="uncheck") 
	   formobj.selectedSchedule.checked =false ;
       else 
	   formobj.selectedSchedule.checked =true ;
	}
	
    else {
         for (i=0;i<totcount;i++){
             if (act=="uncheck") 
		 formobj.selectedSchedule[i].checked=false;
             else 
		 formobj.selectedSchedule[i].checked=true;
         }
    }
    
    
    if (act=="uncheck") 
		formobj.deleteAll.value="unchecked"; 
    else 
		formobj.deleteAll.value="checked"; 
}//end of if statement- this will only execute if formobj totcount is valid
}//end of function


 function  validate(formobj) {

	 	var fdaRegulated = document.getElementById("FDARegulated").value;
		var reasonDel = document.getElementById("reason_del").value;
		reasonDel=reasonDel.replace(/^\s+|\s+$/g,"");
		if(fdaRegulated=="1" && reasonDel.length<=0)
		{
			alert("<%=MC.M_Etr_MandantoryFlds%>");
			document.getElementById("reason_del").focus();
			return false;
				
		}
     selDelIds = new Array(); 
	 totcount = formobj.totcount.value;
	
    if (!(validate_col('e-Signature',formobj.eSign))) return false
	 
	if(formobj.eSign.value == ""){
	 alert("<%=MC.M_Etr_MandantoryFlds%>");/*alert("Please enter data in all mandatory fields");*****/
	return false;
	}
	

	<%-- if(isNaN(formobj.eSign.value) == true) {
	  alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	  formobj.eSign.focus();
	  return false;
	} --%>
 
	if(formobj.totcount.value == 1) {
	
	     if(formobj.selectedSchedule.checked == false) {
	     alert("<%=MC.M_SelSch_ToDel%>");/*alert("Please select Schedules to be Deleted");*****/
	     return false;
	 }
	
	 
	}
	else {
	var cnt = 0;
	for(i = 0;i<formobj.totcount.value;i++) {
	
	    
	     if(formobj.selectedSchedule[i].checked == true) {
	        cnt ++
	     }
	 
	   }
	 }
	 if(cnt==0) {
	 
		 alert("<%=MC.M_SelSch_ToDel%>");/*alert("Please select Schedules to be Deleted");*****/
	    return false;
	 }
	 
	 if(totcount == 1) {
	     selDelIds = formobj.selectedSchedule.value
	 }
	 else {
	 
	     var j=0;
	     for(i = 0;i<formobj.totcount.value;i++) {
	     
	      if(formobj.selectedSchedule[i].checked == true) {
	         selDelIds[j] = formobj.selectedSchedule[i].value;
	         j++;
	    } 
	 	     
	   }  
	  
      }	
	    
	  formobj.selDelIds.value=selDelIds ;
	  return true; 
	   
   
}

 

</SCRIPT>

<%@ page language = "java" import = "java.util.*,com.velos.esch.business.common.*,com.aithent.audittrail.reports.AuditUtils,com.velos.eres.web.patProt.PatProtJB,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.*"%>
<%@ page language = "java" import = "com.velos.eres.audit.web.AuditRowEresJB"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />

<jsp:include page="include.jsp" flush="true"/>

<body>

   <%
                  ScheduleDao schDao = new ScheduleDao(); 
		  String src = null;
		  int schedulesCount = 0;
		  HttpSession tSession = request.getSession(true); 
	          if (sessionmaint.isValidSession(tSession)) {
	        	  //Virendra:BugNo:5110
	        	  %>
	        	  <jsp:include page="sessionlogging.jsp" flush="true"/>
	        	  <%  
               String patientId   = request.getParameter("patientId");
		       boolean formDisplay = true;
		       String studyId     = request.getParameter("studyId");
		              src         = request.getParameter("srcmenu");
		       String eSign      = request.getParameter("eSign");	
		       String oldESign   = (String) tSession.getValue("eSign");
		       String delMode    = request.getParameter("delete");
		       String selDelids  = request.getParameter("selDelIds");
		       String userId = (String) tSession.getValue("userId");//KV: BugNo:5111
		   	   //System.out.println("userIdFromSession " + userId); 
		       String fdaRegulated="";
			studyId = studyId==null?"0":studyId;
			studyB.setId(EJBUtil.stringToNum(studyId));
		    	studyB.getStudyDetails();
		    	fdaRegulated=studyB.getFdaRegulatedStudy();
			fdaRegulated = fdaRegulated==null?"0":fdaRegulated;
			if(eSign == null) {
			      eSign = "";
			}
			
			if(!oldESign.equals(eSign)  && delMode != null) {
%>
	 		  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
			} else {
			
			
				
			 if(selDelids != null) {
		           
		          StringTokenizer idStrgs=new StringTokenizer(selDelids,",");
	                  int Ids=idStrgs.countTokens();
	                  String[] strArrStrgs = new String[Ids] ;
		
	                   for(int cnt=0;cnt<Ids;cnt++) {
	               		   strArrStrgs[cnt] = idStrgs.nextToken();	
			
	                   }
	                   	       String reason_del = request.getParameter("reason_del");	
	    		       reason_del = reason_del.trim();
	    		       reason_del = reason_del==null?"":reason_del;
			            PatProtJB patProtJB = new PatProtJB();
			          //Modified for INF-18183 ::: Raviesh
			            int ret = patProtJB.deleteSchedules(strArrStrgs,EJBUtil.stringToNum(userId),AuditUtils.createArgs(session,reason_del,LC.L_Manage_Pats));//KV: BugNo:5111
			            //System.out.println("ReturnValue" + ret);
			if(ret == 0) {
				String remarks = request.getParameter("reason_del");
				if (!StringUtil.isEmpty(remarks)){
					 for(int cnt=0;cnt<strArrStrgs.length;cnt++) {
							AuditRowEresJB schAuditJB = new AuditRowEresJB();
							schAuditJB.setReasonForChangeOfPatProt(StringUtil.stringToNum(strArrStrgs[cnt]), 
									StringUtil.stringToNum(userId), remarks);
					 }
				 }
			  formDisplay = false;
              %>
               <BR>
               <BR>
               <BR>
               <BR>

<p class = "successfulmsg" align = center> <%=MC.M_Del_Succ%><%--Deleted successfully*****--%></p>
          
           <%
		} else {
		formDisplay = false;
             %>

<BR>
<BR>
<BR>
<BR>
<p class = "successfulmsg" align = center> <%=MC.M_DataCnt_BeDel%><%--Data could not be deleted.*****--%> </p>
<%
		}%>
		
		
		   <script>
		 	   window.opener.location.reload();
			  setTimeout("self.close()",2000);
	        </script>	
		            
		 <%	      
		  }  
		%>
			
	<%
	 if(formDisplay) {
	 %>

	<FORM name="deleteschedules" id="deletesch" method="post" action="deleteschedules.jsp?srcmenu=<%=src%>" onSubmit="if (validate(document.deleteschedules)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
         <TR align = left>
           <td><p class="sectionheadings"><%=MC.M_DelPat_Sch_S%><%--Delete <%=LC.Pat_Patient%> Schedule(s)*****--%></p><td>
            </TR> 
     </TABLE> 
     
    <BR>
     <table width="100%" cellspacing="0" cellpadding="0" border="0" class="basetbl">

      
<TR >
     <th width=7%  align =center ><input type="checkbox" name="deleteAll" value="" onClick="checkAll(document.deleteschedules)"></th>     
     <th width=2%><%=LC.L_Select%><%--Select*****--%></th>
     <th width=50%><%=LC.L_Patient_Schedule_S%><%--<%=LC.Pat_Patient%> Schedule(s)*****--%></th>
       
</TR>
<%
		   schDao = new ScheduleDao();
		   PatProtJB patProtJB = new PatProtJB();
		   schDao = patProtJB.getAvailableSchedules(patientId,studyId);
		   schedulesCount = (schDao.getPatProtIds()).size() ;
		   ArrayList   patProtidList  = schDao.getPatProtIds();
		          
		  if (schDao != null)
		  {
		 	 for (int ctr=0; ctr<schedulesCount; ctr++){
			           
			          String protocolName = schDao.getProtocolNames(ctr);
				  if(protocolName == null){
				        protocolName = "";
				  }
				  String protocolDates = schDao.getProtocolStartDates(ctr);
				  if(protocolDates == null){
				        protocolDates = "";
				  }
				          
			          String protocolNameAndDate =   protocolName +","+ protocolDates ;
                  %>                                  

                  <TR>				
                        <td></td>
			<td>
			 <input type="checkbox" name="selectedSchedule" value=<%=schDao.getPatProtIds(ctr)%>></td>
			<td>
			  <%=protocolNameAndDate%>
			</td>
                  </TR>				
	         <%			
		 }
		
		}
	         %>	

     </table>
    <input type="hidden" name="patientId" value ="<%=patientId%>">
    <input type="hidden" name="studyId"   value="<%=studyId%>">
    <%
      if(schedulesCount > 0) {
    %>
    <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
	<tr>
	<td align="right" valign="top" width="50%"><%=LC.L_Reason_ForDelete %> 
	<%if(fdaRegulated.equals("1")) {%>
	<font class="Mandatory">*</font>
	<%} %>&nbsp;
	</td>
	<Input type="hidden" name="FDARegulated" id="FDARegulated" value="<%=fdaRegulated %>">
	<td><textarea maxlength="4000" name="reason_del" id="reason_del"></textarea>
	<br><font class="Mandatory"><%=MC.M_Limit4000_Char %></font>
	</td>
	</tr>
	</table>
     <P class="defComments"><%=MC.M_EsignToProc_WithDelSch%><%--Please enter e-Signature to proceed with deletion of Schedules*****--%></P>	
       <jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="deletesch"/>
			<jsp:param name="showDiscard" value="N"/>
	</jsp:include>
      
    <input type="hidden" name="totcount" value="<%=schedulesCount%>">
    <input type="hidden" name="delete"   value="<%=delMode%>">
    <input type="hidden" name="selDelIds">
        
    <%
    }
    else {
    %>
    <BR>
    <BR>
    <P class="successfulmsg">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp    
    <%=MC.M_NoDataFound_ForSch%><%--No Data found for the Schedules*****--%></P>	
    <%
    }
    %>
</FORM>
<%
  }
 }
}
else{
%>
<jsp:include page="timeout.html" flush="true"/> 
<%
}
%>

<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</BODY>
</HTML>
