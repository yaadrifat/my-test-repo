<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<Link Rel=STYLESHEET HREF="common.css" type=text/css>

</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>




<BODY>
  <jsp:useBean id="crfNotifyB" scope="request" class="com.velos.esch.web.crfNotify.CrfNotifyJB"/>
  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
  <%@ page language = "java" import = "com.velos.eres.service.util.MC,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%>
  <%

	String src = null;
	src = request.getParameter("srcmenu");
	String eSign = request.getParameter("eSign");
	
	String crfNotifyId = request.getParameter("crfNotifyId");
	
	String mode = request.getParameter("mode");
	String crfNotifyToId = request.getParameter("crfNotifyToId");
	String fromPage = request.getParameter("fromPage");
	String protocolId = request.getParameter("protocolId");
	String studyId = request.getParameter("studyId");	
	String crfStat = request.getParameter("crfStat");	
	String globalFlag= request.getParameter("globalFlag");		
	String lockSetting=request.getParameter("lockSetting");	
	String addFlag=request.getParameter("addFlag");	




	HttpSession tSession = request.getSession(true); 

	if (sessionmaint.isValidSession(tSession))
	   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%	   
   		String oldESign = (String) tSession.getValue("eSign");
		String eventId = (String) tSession.getValue("eventId");
			if(!oldESign.equals(eSign)) {
		%>
	  	<jsp:include page="incorrectesign.jsp" flush="true"/>	
		<%
		} else {
	

		String ipAdd = (String) tSession.getValue("ipAdd");
		String usr = (String) tSession.getValue("userId");
		%>
	
		<%
		if(mode.equals("N")) {

		crfNotifyB.setCrfNotifyCodelstNotStatId(crfStat);
		crfNotifyB.setCrfNotifyUsersTo(crfNotifyToId);
		crfNotifyB.setCrfNotStudyId(studyId);
		crfNotifyB.setCrfNotProtocolId(protocolId);		
		crfNotifyB.setCreator(usr);
		crfNotifyB.setIpAdd(ipAdd);	
		crfNotifyB.setCrfNotGlobalFlag(globalFlag);
	
		crfNotifyB.setCrfNotifyDetails();		
		}
	
		if(mode.equals("M")) {
		crfNotifyB.setCrfNotifyId(EJBUtil.stringToNum(crfNotifyId));
		crfNotifyB.getCrfNotifyDetails();		
		crfNotifyB.setCrfNotifyCodelstNotStatId(crfStat);
		crfNotifyB.setCrfNotifyUsersTo(crfNotifyToId);
		crfNotifyB.setModifiedBy(usr);
		crfNotifyB.setIpAdd(ipAdd);
		crfNotifyB.updateCrfNotify();
		}		
		%>
<br>
<br>
<br>
<br>
<br>
<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%> </p>
<%
if(addFlag != null && addFlag.equals("Add"))
{
%>

<META HTTP-EQUIV=Refresh CONTENT="0; URL=crfnotifyglobal.jsp?srcmenu=tdMenuBarItem5&selectedTab=5&mode=N&studyId=<%=studyId %>&globalFlag=<%=globalFlag%>&fromPage=patientEnroll&protocolId=<%=protocolId%>&lockSetting=<%=lockSetting%>">

<%}%>


<META HTTP-EQUIV=Refresh CONTENT="0; URL=crfngbrowser.jsp?srcmenu=tdMenuBarItem5&mode=<%=mode%>&globalFlag=<%=globalFlag%>&page=patientEnroll&generate=N&studyId=<%=studyId%>&protocolId=<%=protocolId%>&lockSetting=<%=lockSetting%>">
		
<%
		}//end of if for eSign check
		}//end of if body for session

		else
	{
		%>
	  <jsp:include page="timeout.html" flush="true"/>
	  <%
	}
	%>

</BODY>

</HTML>
