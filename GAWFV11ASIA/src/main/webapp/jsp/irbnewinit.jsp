	<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html> 
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=MC.M_ResCompApp_InitDets%><%--Research Compliance >> New Application >> Initial Details*****--%></title>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.StringUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*" %>

<!-- KM : import the objects for customized field -->
<%@ page import="com.velos.eres.web.user.ConfigFacade,com.velos.eres.web.user.ConfigObject, com.velos.eres.web.user.ConfigDetailsObject,com.velos.eres.business.common.CommonDAO"%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="accountB" scope="page" class="com.velos.eres.web.account.AccountJB" /> 
<jsp:useBean id="usr" scope="request" class="com.velos.eres.web.accountWrapper.AccountWrapperJB"/>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<SCRIPT language="Javascript1.2">
	function openLSampleSize(studyId, mode)	{
		if (mode == 'N') {
			alert("<%=MC.M_SbmtStdFirst_ForSampleSize%>");/*alert("Please Submit the <%=LC.Std_Study%> first for entering Local Sample Size");*****/
			return false;
		}else{
			windowName = window.open("LSampleSizePopUp.jsp?studyId="+studyId,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=600,height=500 top=50,left=100 0, ");
			windowName.focus();
		}
	}	

	function setValue(formobj){
	value=formobj.author.value;
	if (value=="Y"){
		formobj.cbauth.checked=false;
		formobj.author.value="";
	}
	if ((value.length==0) || (value=="N")){
		formobj.cbauth.checked=true;
		formobj.author.value="Y";
	}
	   
}
	
	   
	function  validate(formobj,autogen) {	   

		 if (document.getElementById('mandstudyent')) { //KM-3655
			if (!(validate_col('Study Entered By',formobj.dataManagerName))) return false   
		 }
		 
		 var mode = formobj.mode.value;
		
		 //KM: Modified for Edit mode.
		 if (autogen != 1 || mode == 'M' || mode=='S'){  //KM-0702
		   if (document.getElementById('mandsnumber')) {
		  	 if (!(validate_col('Study Number',formobj.studyNumber))) return false     
		 }
         }  
		 
		 if (document.getElementById('mandtarea')) {
			if (!(validate_col('Therapeutic Area',formobj.studyTArea))) return false
		 }

		 if (document.getElementById('mandtitle')) {
		     if (!(validate_col('Title',formobj.studyTitle))) return false 	   	    
		 }
		
	    
		if (!(validate_col('e-Sign',formobj.eSign))) return false
		
		
		 if (document.getElementById('pgcustomstudyentby')) {
				 if (!(validate_col('Study Entered By',formobj.dataManagerName))) return false
		 } 	
				 
		 
		 //Prinicipal Investigator mandatory/non-mand implementation
		 if (document.getElementById('pgcustompi')) {
				 if (!(validate_col('pinvestigator',formobj.prinInvName))) return false
		 } 	 
		 

		 if (document.getElementById('pgcustomstudycont')) {
				 if (!(validate_col('Study Contact',formobj.studycoName))) return false
		 } 	 

	    
		
		/*if (autogen != 1){
	    	if (!(validate_col('Study Number',formobj.studyNumber))) return false
	    }*/

		
		 if (autogen != 1 || mode == 'M' ){ //KM:31July08
  		   if (document.getElementById('pgcustomstudynum')) {
				  if (!(validate_col('Study Number',formobj.studyNumber))) return false  
		   } 
	     }


		
		 if (document.getElementById('pgcustomstudytitle')) {
				  if (!(validate_col('Title',formobj.studyTitle))) return false 	
		 } 	
       				  

		
		 if (document.getElementById('pgcustomdivision')) {
				  if (!(validate_col('Division',formobj.studyDivision))) return false 	
		 }

	
		if (document.getElementById('pgcustomtarea')) {
				 if (!(validate_col('Therapeutic Area',formobj.studyTArea))) return false	
		}


		if (document.getElementById('pgcustomresearchtype')) {
				 if (!(validate_col('Research Type',formobj.studyResType))) return false	
		 }

		if (document.getElementById('pgcustomtype')) {
				 if (!(validate_col('Study Type',formobj.studyType))) return false	
		 }

		if (document.getElementById('pgcustomstdlinkedto')) {
				 if (!(validate_col('Study LinkedTo',formobj.studyAssocNum))) return false	
		 }

		if (document.getElementById('pgcustomspname')) {
				 if (!(validate_col('Sponsor Name',formobj.sponsor))) return false	
		 }

		if (document.getElementById('pgcustomothsponsor')) {
				 if (!(validate_col('If other',formobj.studySponsor))) return false	
		 }

		if (document.getElementById('pgcustomsponsorid')) {
				 if (!(validate_col('Sponsor ID',formobj.studySponsorIdInfo))) return false	
		 }

		if (document.getElementById('pgcustomcontact')) {
				 if (!(validate_col('Contact',formobj.studyContact))) return false	
		 }

		////KM
		
	
		//Added by Manimaran to fix the Bug.2658
	    
		if (formobj.studyNumber) {
  		   if (checkChar(formobj.studyNumber.value,'<') ) {
		      alert("<%=MC.M_SplChar_LtNtAlwdStd%>");/*alert("Special character '<' is not allowed in <%=LC.Std_Study%> Number");*****/
	          formobj.studyNumber.focus();
		   return false;
	    }
		}


			<%-- if(isNaN(formobj.eSign.value) == true) {
				alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
				formobj.eSign.focus();
				return false;
			} --%>

	    }	
		

	function openwin() {
		window.open("","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=600,height=450")
		;}
		
	function openwinstudyid(studyid, mode, studyRights)
	{
		if (mode == 'M' || mode == 'S'){
			windowname=window.open("newStudyIds.jsp?studyId=" + studyid+"&studyRights="+studyRights ,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=650,height=550 top=120,left=200 0, "); 
			windowname.focus();
		} else if (mode == 'N')	{
			alert("<%=MC.M_SbmtStdFirst_ForMoreStdDet%>");/*alert("Please Submit the <%=LC.Std_Study%> first for entering More <%=LC.Std_Study%> Details");*****/
			return false;
		}			
	}
	
	function callAjaxGetTareaDD(formobj){
	   
	   selval = formobj.studyDivision.value;
	   
 	   new VELOS.ajaxObject("getCodeDropDown.jsp", {
   		urlData:"selectedVal="+selval + "&ddName=studyTArea&codeType=tarea&ddType=child" ,
		   reqType:"POST",
		   outputElement: "span_tarea" } 
   
   ).startRequest();

}
	
</SCRIPT>

<SCRIPT langauge="Javascript1.2">
	function openwin1(frm) {
		windowName = window.open("usersearchdetails.jsp?fname=&lname=&from="+ frm,"TEST","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
		windowName.focus();
	;}


	function openwin2(src,selectedTab,studyId){

		
		windowEcomp= window.open("studystatusbrowser.jsp?srcmenu="+src+"&selectedTab="+selectedTab+"&studyId="+studyId+"&hiddenflag=0","information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=750,height=350");
		windowEcomp.focus();
			
		}
	var timer = setInterval(function () {
        if (windowEcomp.closed) {
            clearInterval(timer);
            window.location.reload(); // Refresh the parent page
        }
    }, 100);
</SCRIPT>

<% String src;
src= request.getParameter("srcmenu");
String selectedTab = request.getParameter("selectedTab");
String tabFlag = request.getParameter("tabFlag")==null?"":request.getParameter("tabFlag");
String mode ="";
mode = request.getParameter("mode");
//Mode is used as "S" for Resaercher Enhancements
String from = "default";

int studyId = 0;
if (mode.equals("M")|| mode.equals("S")) {
    	studyId = EJBUtil.stringToNum(request.getParameter("studyId"));
	}
%>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   

<jsp:include page="ui-include.jsp" flush="true"></jsp:include>

<body>
<DIV class="BrowserTopn" id="divTab"> 
	<jsp:include page="irbnewtabs.jsp" flush="true"> 
	<jsp:param name="from" value="<%=from%>"/> 
	<jsp:param name="mode" value="<%=mode%>"/> 
	
	<jsp:param name="tabFlag" value="<%=tabFlag%>"/> 
	<jsp:param name="selectedTab" value="<%=selectedTab%>"/>
	<jsp:param name="studyId" value="<%=studyId%>"/>
	</jsp:include>
</DIV>	
<DIV class="BrowserBotN BrowserBotN_RC_2" id="div1" style="top:97px"> 
		
  	<%
  	boolean hasAccess = false;
	HttpSession tSession = request.getSession(true); 
	String uName = (String) tSession.getValue("userName");
	String userIdFromSession = (String) tSession.getValue("userId");
	String acc = (String) tSession.getValue("accountId");
	if (sessionmaint.isValidSession(tSession))
	{
	    String submissionType = request.getParameter("submissionType");	 
	    if (submissionType != null) {
		    tSession.setAttribute("submissionType", submissionType);
		} else {
		    submissionType = (String)tSession.getAttribute("submissionType");
		}
	    String defUserGroup = (String) tSession.getAttribute("defUserGroup");
		
		int accId = EJBUtil.stringToNum(acc);	
		int pageRight = 0;
		int stdRight =0;
		int grpRight = 0;
		
		//copy a new study is controlled by Manage Protocols new group right
		GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
   		grpRight = Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_APP"));
		
		ArrayList tId ;
		tId = new ArrayList();
		
		
		
		StudyDao stDao=new StudyDao();
		CtrlDao ctrlValue=new CtrlDao();//km
		CodeDao cdTArea = new CodeDao();   
		CodeDao cdResType = new CodeDao();
		CodeDao cdType = new CodeDao();
		CodeDao cdDivision = new CodeDao();
		CodeDao cdSponsorName = new CodeDao();//gopu
		String studyNumber = "" ;
		String studyTitle = "" ;
		String studyDivision = "";
		String studyTArea = "" ;
		String studySize = "0" ;
		String studyResType = "" ;
		String studyType = "" ;
		String studySponsorId = "" ;
		String studySponsorName = ""; //gopu
		String studySponsorIdInfo=""; //gopu
		String studyContactId = "" ;
		String studyPartCenters = "" ;
		String studyAuthor = "" ;
		String studyAuthorName = "" ;
		String studyPrinv = "" ;
		String studyCo="";
		String studyCoName="";
		String studyPrinvName = "" ;
		String studyPrinvEmailPhone = "";
		String studyPrinvAddress = "";
		String studyCoEmailPhone = "";
		String studyCoAddress = "";
		String majAuthor="";
		String studyAssoc="";
		String studyAssocNum="";
		String studyPubCol = "00000";
		String studyVerParent = "";
		String studyInvFlag = ""; 
		String studyInvNum = ""; 
		String prinvOther="";
		String lastName  ="";
		String firstName = "";
		String studyDiv = "";

		//KM
		String disableStr ="";
		String readOnlyStr ="";
		
		/*String ICDCode3 = "";*/
		
		StringBuffer studyContact = new StringBuffer();
		StringBuffer studySponsor = new StringBuffer();
		StringBuffer dataManager = new StringBuffer();
		StringBuffer durationUnit = new StringBuffer();
		int counter = 0;
		int userId = 0;
		int siteId = 0;
		SiteDao siteDao = new SiteDao();
		String ctrlVal=ctrlValue.getControlValue("icd");//km
		
		//Added for Ecomp Researcher Customization
		String ecompStatus = "";
		String irbFlag = "";
		irbFlag= request.getParameter("irbFlag")==null?"":request.getParameter("irbFlag");
		//Added by Manimaran for the Enh.#GL9

		ConfigFacade cFacade=ConfigFacade.getConfigFacade();
		HashMap hashPgCustFld = new HashMap();
		
		ConfigDetailsObject cdoPgField = cFacade.populateObject(Integer.parseInt(acc), "studysummary");
		
		if (cdoPgField.getPcfField()!= null && cdoPgField.getPcfField().size() > 0) {
			for (int i=0;i<cdoPgField.getPcfField().size();i++){
				hashPgCustFld.put((String)cdoPgField.getPcfField().get(i), String.valueOf(i));

	       }
	    }
		

		//Added by Manimaran for the Enh.#GL7.
		accountB.setAccId(EJBUtil.stringToNum(acc));
		accountB.getAccountDetails();
		String autoGenStudy = accountB.getAutoGenStudy();
		if (autoGenStudy == null)
			autoGenStudy ="0";
		
								
		if (mode.equals("M") || mode.equals("S")) 
		{	
					
			TeamDao teamDao = new TeamDao();
			teamDao.getTeamRights(studyId,EJBUtil.stringToNum(userIdFromSession));
			tId = teamDao.getTeamIds();
					
			
			if (tId.size() <=0)
			{	
				pageRight  = 0;
				StudyRightsJB stdRightstemp = new StudyRightsJB();
				tSession.putValue("studyRights",stdRightstemp);
			} else {
					studyId = EJBUtil.stringToNum(request.getParameter("studyId"));
					
					stdRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));
					   
					ArrayList teamRights ;
					teamRights  = new ArrayList();
					teamRights = teamDao.getTeamRights();
						 
					stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
					stdRights.loadStudyRights();
					
					
					
					tSession.putValue("studyRights",stdRights);
					if ((stdRights.getFtrRights().size()) == 0){
					
					 	stdRight= 0;
					}else{
						stdRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYSUM"));
					}
				}

				
				stdRights =(StudyRightsJB) tSession.getValue("studyRights");
					
			if (tId.size() >0)				
				pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYSUM"));
						
		
				String studyT = "";
				
				studyB.setId(studyId);
				studyB.getStudyDetails();
				studyNumber = studyB.getStudyNumber();
				if (studyNumber == null ) studyNumber ="";
							
				studyVerParent = studyB.getStudyVersionParent();
					
				studyDiv = studyB.getStudyDivision();
					
				studyInvFlag = studyB.getStudyInvIndIdeFlag(); 
					
				
				studyInvNum = studyB.getStudyIndIdeNum(); 
					
				
				if (studyVerParent == null)
					studyVerParent = "";
					
				if (StringUtil.isEmpty(studyDiv))
				{
					studyDiv = "";
				}	
					
				if (studyInvFlag == null)
					studyInvFlag = "";
				if (studyInvNum == null)
					studyInvNum = "";

					
				tSession.putValue("studyId",request.getParameter("studyId"));
							
				tSession.putValue("studyNo",studyNumber);
					
				
				tSession.putValue("studyVerParent",studyVerParent);
					
				
				studyT = studyB.getStudyTArea();
					
				 
					
				if (studyT == null)
					studyT = "";
				 
				tSession.putValue("studyTArea",studyT);
		} else {							
			tSession.putValue("studyId","");
			tSession.putValue("studyNo","");
			tSession.putValue("StudyRights",null);
	   		pageRight = grpRight;
		}		
		
		if ((mode.equals("M") && pageRight >=4) ||(mode.equals("S") && pageRight >=4) || (mode.equals("N") && (pageRight == 5 || pageRight == 7 )) )
		{

			hasAccess = true;
			
			//cdTArea.getCodeValues("tarea");
			
			cdTArea.getCodeValuesForCustom1("tarea",studyDiv);
			cdTArea.setCType("tarea");
	        cdTArea.setForGroup(defUserGroup);
			 
			cdDivision.getCodeValues("study_division");
			cdDivision.setCType("study_division");
	        cdDivision.setForGroup(defUserGroup);
			
			
			cdResType.getCodeValues("research_type");
			cdResType.setCType("research_type");
	        cdResType.setForGroup(defUserGroup);
			
			
			cdType.getCodeValues("study_type");
			cdType.setCType("study_type");
	        cdType.setForGroup(defUserGroup); 
			
			
			cdSponsorName.getCodeValues("sponsor"); //gopu
			cdSponsorName.setCType("sponsor");
	        cdSponsorName.setForGroup(defUserGroup); 
			
			
			////

		    String divAtt ="";
			String tareaAtt ="";
			String researchAtt ="";
			String typeAtt ="";
			String spnameAtt = "";




		    if (hashPgCustFld.containsKey("division")) {
				int fldNumDivision = Integer.parseInt((String)hashPgCustFld.get("division"));
				divAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumDivision));
				if(divAtt == null) divAtt ="";
		    }


			if (hashPgCustFld.containsKey("therapeuticarea")) {
				int fldNumTarea = Integer.parseInt((String)hashPgCustFld.get("therapeuticarea"));
				tareaAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumTarea));
				if(tareaAtt == null) tareaAtt ="";
		    }


			if (hashPgCustFld.containsKey("researchtype")) {
				int fldNumResearch = Integer.parseInt((String)hashPgCustFld.get("researchtype"));
				researchAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumResearch));
				if (researchAtt == null) researchAtt ="";
		    }

			if (hashPgCustFld.containsKey("studytype")) {
				int fldNumType = Integer.parseInt((String)hashPgCustFld.get("studytype"));
				typeAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumType));
				if (typeAtt == null) typeAtt ="";
		    }


			if (hashPgCustFld.containsKey("sponsorname")) {
				int fldNumSpname = Integer.parseInt((String)hashPgCustFld.get("sponsorname"));
				spnameAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumSpname));
				if (spnameAtt == null) spnameAtt ="";
		    }


			////

			if (mode.equals("M") || mode.equals("S")) {
				//stdRights.setId(studyId);
				studyTitle = studyB.getStudyTitle();
				
				if(irbFlag.equalsIgnoreCase("Y")){
					EIRBDao codelstsubm_stat = new EIRBDao();
					ecompStatus = codelstsubm_stat.getEcompSubmissionStatus(studyId);
					
				}
				if(studyTitle == null)
					studyTitle = ""; //KM
				
				//studyPartCenters = studyB.getStudyPartCenters();
				//studyPartCenters = (   studyPartCenters  == null      )?"":(  studyPartCenters ) ;
				
				studySponsorId = studyB.getStudySponsor();
				studySponsorId = (   studySponsorId  == null      )?"":(  studySponsorId ) ;
					
				
				//gopu
				studySponsorName = studyB.getStudySponsorName();
				studySponsorName = (   studySponsorName  == null      )?"":(  studySponsorName ) ;
								
				
			
				studySponsorIdInfo = studyB.getStudySponsorIdInfo();
				studySponsorIdInfo = (   studySponsorIdInfo  == null      )?"":(  studySponsorIdInfo ) ;
				
				////

				studyContactId = studyB.getStudyContact();
				studyContactId = (   studyContactId  == null      )?"":(  studyContactId ) ;
				
				
				studyAuthor = studyB.getStudyAuthor();
				if(studyAuthor == null)
					studyAuthor = "";
					
				studyPrinv = studyB.getStudyPrimInv();
				if(studyPrinv==null) studyPrinv="";
				 
				//Study Coordinator
				studyCo=studyB.getStudyCoordinator();
				studyCo=(studyCo==null)?"":studyCo;
				
				prinvOther=studyB.getStudyOtherPrinv();
				if (prinvOther==null) prinvOther="";
				majAuthor=studyB.getMajAuthor();
				majAuthor=(majAuthor==null)?"":(majAuthor);
				
				studyAssoc=studyB.getStudyAssoc();
				studyAssoc=(studyAssoc==null)?"":(studyAssoc);
				
				tSession.putValue("studyDataMgr",studyAuthor);
								
				userB.setUserId(EJBUtil.stringToNum(studyAuthor));
				userB.getUserDetails();
				firstName  = userB.getUserFirstName();
				lastName = userB.getUserLastName();
				if(firstName == null)
					firstName  = "";
				if(lastName == null)
					lastName  = "";
				
			
						
				studyAuthorName = firstName+" " +lastName ;
			
				//if (! EJBUtil.isEmpty(studyPrinv) )
				if(studyPrinv.length()==0) 
					studyPrinvName="";
				
				else 					{
					userB.setUserId(EJBUtil.stringToNum(studyPrinv));
					userB.getUserDetails();
					studyPrinvName =  userB.getUserFirstName() + " " +userB.getUserLastName();
				}
				
				//if (! EJBUtil.isEmpty(studyPrinv) )
				if(studyCo.length()==0) 
					studyCoName="";
				
				else 					{
					userB.setUserId(EJBUtil.stringToNum(studyCo));
					userB.getUserDetails();
					studyCoName =  userB.getUserFirstName() + " " +userB.getUserLastName();
				}
				
				if (studyPrinv.length() > 0 ) {
					usr.setUserId(studyPrinv);
					usr.populateUser();
					StringBuffer emailBuf = new StringBuffer();
					if (usr.getAddEmailUser() != null) { emailBuf.append(usr.getAddEmailUser()); }
					if (usr.getAddPhoneUser() != null) {
					    if (emailBuf.length() > 0) { emailBuf.append(" "); }
					    emailBuf.append(usr.getAddPhoneUser());
					}
					studyPrinvEmailPhone = emailBuf.toString();
					StringBuffer addrBuf = new StringBuffer();
					if (usr.getAddPriUser() != null) { addrBuf.append(usr.getAddPriUser()); }
					if (usr.getAddCityUser() != null) {
					    if (addrBuf.length() > 0) { addrBuf.append(" "); }
					    addrBuf.append(usr.getAddCityUser());
					}
					if (usr.getAddStateUser() != null) {
					    if (addrBuf.length() > 0) { addrBuf.append(" "); }
					    addrBuf.append(usr.getAddStateUser());
					}
					if (usr.getAddZipUser() != null) {
					    if (addrBuf.length() > 0) { addrBuf.append(" "); }
					    addrBuf.append(usr.getAddZipUser());
					}
					studyPrinvAddress = addrBuf.toString();
				}
				
				if (studyCo.length() > 0 ) {
					usr.setUserId(studyCo);
					usr.populateUser();
					StringBuffer emailBuf = new StringBuffer();
					if (usr.getAddEmailUser() != null) { emailBuf.append(usr.getAddEmailUser()); }
					if (usr.getAddPhoneUser() != null) {
					    if (emailBuf.length() > 0) { emailBuf.append(" "); }
					    emailBuf.append(usr.getAddPhoneUser());
					}
					studyCoEmailPhone = emailBuf.toString();
					StringBuffer addrBuf = new StringBuffer();
					if (usr.getAddPriUser() != null) { addrBuf.append(usr.getAddPriUser()); }
					if (usr.getAddCityUser() != null) {
					    if (addrBuf.length() > 0) { addrBuf.append(" "); }
					    addrBuf.append(usr.getAddCityUser());
					}
					if (usr.getAddStateUser() != null) {
					    if (addrBuf.length() > 0) { addrBuf.append(" "); }
					    addrBuf.append(usr.getAddStateUser());
					}
					if (usr.getAddZipUser() != null) {
					    if (addrBuf.length() > 0) { addrBuf.append(" "); }
					    addrBuf.append(usr.getAddZipUser());
					}
					studyCoAddress = addrBuf.toString();
				}
				
				
				
				studyPubCol = studyB.getStudyPubCol();				
				
		
				if (tareaAtt.equals("1"))
					studyTArea = cdTArea.toPullDown("studyTArea",EJBUtil.stringToNum(studyB.getStudyTArea()) ,"disabled");
				else 
					studyTArea = cdTArea.toPullDown("studyTArea",EJBUtil.stringToNum(studyB.getStudyTArea()));
		

				if (divAtt.equals("1"))
					studyDivision = cdDivision.toPullDown("studyDivision",EJBUtil.stringToNum(studyDiv), " onChange=callAjaxGetTareaDD(document.study); disabled");
				else
				    studyDivision = cdDivision.toPullDown("studyDivision",EJBUtil.stringToNum(studyDiv), " onChange=callAjaxGetTareaDD(document.study);");
				
				
				if (researchAtt.equals("1"))
					studyResType = cdResType.toPullDown("studyResType",EJBUtil.stringToNum(studyB.getStudyResType()),"disabled");
				else 
					studyResType = cdResType.toPullDown("studyResType",EJBUtil.stringToNum(studyB.getStudyResType()));

				if (spnameAtt.equals("1"))
					studySponsorName = cdSponsorName.toPullDown("sponsor",EJBUtil.stringToNum(studyB.getStudySponsorName()),"disabled"); 
				else
					studySponsorName = cdSponsorName.toPullDown("sponsor",EJBUtil.stringToNum(studyB.getStudySponsorName()),true);


				if (typeAtt.equals("1"))
					studyType = cdType.toPullDown("studyType",EJBUtil.stringToNum(studyB.getStudyType()),"disabled");
				else
					studyType = cdType.toPullDown("studyType",EJBUtil.stringToNum(studyB.getStudyType()));


				if (studyAssoc.length()>0){
				studyB.setId(EJBUtil.stringToNum(studyAssoc));
				studyB.getStudyDetails();
				studyAssocNum = studyB.getStudyNumber();
				}
		   	} else {
				

				if (tareaAtt.equals("1"))
					studyTArea = cdTArea.toPullDown("studyTArea","disabled");
				else 
					studyTArea = cdTArea.toPullDown("studyTArea");

				if (divAtt.equals("1"))
					studyDivision = cdDivision.toPullDown("studyDivision",0, " onChange=callAjaxGetTareaDD(document.study); disabled");
				else
				    studyDivision = cdDivision.toPullDown("studyDivision",0, " onChange=callAjaxGetTareaDD(document.study);");

				if (researchAtt.equals("1"))
					studyResType = cdResType.toPullDown("studyResType","disabled");
				else
					studyResType = cdResType.toPullDown("studyResType");


				if (spnameAtt.equals("1"))
					studySponsorName = cdSponsorName.toPullDown("sponsor","disabled");
				else
					studySponsorName = cdSponsorName.toPullDown("sponsor");

				
				if (typeAtt.equals("1"))
					studyType = cdType.toPullDown("studyType","disabled");
				else
					studyType = cdType.toPullDown("studyType");

				
				studyAuthor = userIdFromSession;
				userB.setUserId(EJBUtil.stringToNum(studyAuthor));
				userB.getUserDetails();
				firstName  = userB.getUserFirstName();
				lastName = userB.getUserLastName();
				if(firstName == null)
					firstName  = "";
				if(lastName == null)
					lastName  = "";
				studyAuthorName = firstName+" " + lastName;
				
				//studyPrinv = userIdFromSession;
				//studyPrinvName = userB.getUserLastName() + ", " + userB.getUserFirstName();
				
			}
			
			char[] stdSecRights= null;
			if (studyPubCol == null) studyPubCol="1111";
			stdSecRights= studyPubCol.toCharArray();
			int totSec = studyPubCol.length();
			int secCount=0;
			
			from = "default";
			
	
			%>

                         
			<Form name="study" id="studyForm" method="post" action="updateNewStudy.jsp"  onSubmit="if (validate(document.study,<%=autoGenStudy%>)==false) {setValidateFlag('false');return false;} else {setValidateFlag('true'); return true;}">
			    <input type="hidden" name="srcmenu" size = 20  value = <%=src%> >
			    <input type="hidden" name="selectedTab" size = 20  value = <%=selectedTab%> >
			    <input type="hidden" name="ecompStatus" value = <%=ecompStatus%> >
                <input type="hidden" name="totPubSections" size = 20  value=4>
                <input type="hidden" name="studyPhase" value="">
                <input type="hidden" name="studyBlinding" value="">
                <input type="hidden" name="studyRandom" value="">
                <input type="hidden" name="studyPubFlag1" value ="0">
                <input type="hidden" name="studyPubFlag2" value ="0">
                <input type="hidden" name="studyPubFlag3" value ="0">
                <input type="hidden" name="studyPubFlag4" value ="0">
                <input type="hidden" name="studyPubFlag5" value ="0">
		    <%

				if (mode.equals("M") || mode.equals("S"))
				{
					%>
				    <input type="hidden" name="studyId" size = 20  value = <%=studyId%> >
					<input type="hidden" name="studyVerParent" size = 20  value = <%=studyVerParent%>>
			<%  }   %>
				
			    <input type="hidden" name="mode" size = 20  value = <%=mode%> >
		    <table width="99%" cellspacing="0" cellpadding="2" Border="0" class="basetbl midAlign">
		    <tr height="7">
			 <td>
                  
                    <P class="defComments">
					
					  <%          if( tSession.getAttribute("studyId")!=""){%><b><%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%>: </b><%=tSession.getAttribute("studyNo") %> <%}%>
					 
                  </P>
                </td>
			
			</tr>








            <tr align="right">




                <td colspan=2 align="right">
                <table align="right">
                    <tr align="right">
                        <td align="right"><%=LC.L_Std_EnteredBy%><%--<%=LC.Std_Study%> Entered By*****--%>&nbsp;</td><td>
                        <input type=hidden name="dataManager" value='<%=studyAuthor%>' <%=disableStr%> >
                        <input type=text name="dataManagerName" value="<%=studyAuthorName%>" readonly <%=disableStr%>> 
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
            <td colspan="3"><P class="sectionHeadings"><%=LC.L_Std_Info_Upper%><%--<%=LC.Std_Study_Upper%> INFORMATION*****--%></P></td>
            </tr>
            </table>
		    <table width="99%" cellspacing="0" cellpadding="1" Border="0" class="midAlign">
			<tr>
				<%
				if (hashPgCustFld.containsKey("pinvestigator")) {
				
				int fldNumPi = Integer.parseInt((String)hashPgCustFld.get("pinvestigator"));
				String pIMand = ((String)cdoPgField.getPcfMandatory().get(fldNumPi));
				String pILable = ((String)cdoPgField.getPcfLabel().get(fldNumPi));
				String pIAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumPi));
			
				disableStr ="";
				readOnlyStr ="";
				
				if(pIAtt == null) pIAtt =""; 
  				if(pIMand == null) pIMand =""; 

				if(!pIAtt.equals("0")) {

				if(pILable !=null){
				%>
				<td width="20%">
				 <%=pILable%> 
				<%} else {%> <td width="20%">
				<%=LC.L_Principal_Investigator%><%--Principal Investigator*****--%>
				<% }
			
				
			   if (pIMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompi">* </FONT>
		 	   <% }
			   %>
				
			   <%if(pIAtt.equals("1")) {
			       disableStr = "disabled"; }
		        
  		       %>
 			  </td>
			  <td> 
			  <input type=hidden name="prinInv" value='<%=studyPrinv%>'> 
			  <input type=text name="prinInvName" value="<%=studyPrinvName%>" readonly <%=disableStr%>> 

			   <%if(!pIAtt.equals("1") && !pIAtt.equals("2")) {%>
			  <A HREF=# onClick=openwin1('studyinv') ><%=LC.L_Select_User%><%--SELECT USER*****--%></A>
			   <%}%>
			  </td>
              <td id="td-prinv-contact" align="left" width="55%" ><%=LC.L_EmailOrPhone%><%--Email/Phone*****--%>: <%=studyPrinvEmailPhone %><br><%=LC.L_Address%><%--Address*****--%>: <%=studyPrinvAddress %></td>
				
				<% } else { %>
				 <input type=hidden name="prinInv" value='<%=studyPrinv%>'> 
			
			 	<% } }  else {%>
				
				<td width="20%"> <%=LC.L_Principal_Investigator%><%--Principal Investigator*****--%>
				 </td>
				 <td> 
							<input type=hidden name="prinInv" value='<%=studyPrinv%>'> 
							<input type=text name="prinInvName" value="<%=studyPrinvName%>" readonly> 
							<A HREF=# onClick=openwin1('studyinv') ><%=LC.L_Select_User%><%--SELECT USER*****--%></A>
                 </td>
                 <td id="td-prinv-contact" align="left" width="55%" ><%=LC.L_EmailOrPhone%><%--Email/Phone*****--%>: <%=studyPrinvEmailPhone %><br><%=LC.L_Address%><%--Address*****--%>: <%=studyPrinvAddress %></td>
			 <%}%>
			 </tr>

			<tr>
				<%
				
				if (hashPgCustFld.containsKey("studycontact")) {
				
				int fldNumStudyCont = Integer.parseInt((String)hashPgCustFld.get("studycontact"));
				String StudyContMand = ((String)cdoPgField.getPcfMandatory().get(fldNumStudyCont));
				String StudyContLable = ((String)cdoPgField.getPcfLabel().get(fldNumStudyCont));
				String StudyContAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumStudyCont));
			
				disableStr ="";
				readOnlyStr ="";
				
				if(StudyContAtt == null) StudyContAtt =""; 
  				if(StudyContMand == null) StudyContMand =""; 

				if(!StudyContAtt.equals("0")) {

				if(StudyContLable !=null){
				%>
				<td width="20%">
				 <%=StudyContLable%> 
				<%} else {%> <td width="20%">
			    <%=LC.L_Study_Contact%><%--Study Contact*****--%> 
				<% }
	
			   if (StudyContMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomstudycont">* </FONT>
		 	   <% }
			   %>
				
			   <%if(StudyContAtt.equals("1")) {
			       disableStr = "disabled"; }
  		       %>
 			  </td>
			  <td > 
					<input type=hidden name="studyco" value='<%=studyCo%>'> 
					<input type=text name="studycoName" value="<%=studyCoName%>" readonly  <%=disableStr%> > 

					 <%if(!StudyContAtt.equals("1") && !StudyContAtt.equals("2")) {%>
					<A HREF=# onClick=openwin1('studyco') ><%=LC.L_Select_User%><%--SELECT USER*****--%></A>
					<%}%>

						
			  </td>			
              <td id="td-studyco-contact" align="left" width="55%" ><%=LC.L_EmailOrPhone%><%--Email/Phone*****--%>: <%=studyCoEmailPhone %><br><%=LC.L_Address%><%--Address*****--%>: <%=studyCoAddress %></td>
				
				<% } else {%>
				<input type=hidden name="studyco" value='<%=studyCo%>'> 
				<% }}  else {%>
				
				<td width="20%"> <%=LC.L_Study_Contact%><%--<%=LC.Std_Study%> Contact*****--%> </td>
		        <td > 
					<input type=hidden name="studyco" value='<%=studyCo%>'> 
					<input type=text name="studycoName" value="<%=studyCoName%>" readonly> 
					<A HREF=# onClick=openwin1('studyco') ><%=LC.L_Select_User%><%--SELECT USER*****--%></A>
				</td>	
              <td id="td-studyco-contact" align="left" width="55%" ><%=LC.L_EmailOrPhone%><%--Email/Phone*****--%>: <%=studyCoEmailPhone %><br><%=LC.L_Address%><%--Address*****--%>: <%=studyCoAddress %></td>

			 <%}%>
			</tr>


			<tr>
				<%
				
				if (hashPgCustFld.containsKey("studynumber")) {
				
				int fldNumStudyNum = Integer.parseInt((String)hashPgCustFld.get("studynumber"));
				String StudyNumMand = ((String)cdoPgField.getPcfMandatory().get(fldNumStudyNum));
				String StudyNumLable = ((String)cdoPgField.getPcfLabel().get(fldNumStudyNum));
				String StudyNumAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumStudyNum));
			
				disableStr ="";
				readOnlyStr ="";
				
				if(StudyNumAtt == null) StudyNumAtt =""; 
  				if(StudyNumMand == null) StudyNumMand =""; 

				if(!StudyNumAtt.equals("0")) {

				if(StudyNumLable !=null){
				%>
				<td width="20%">  <%=StudyNumLable%> 
				<%} else {%> <td width="20%">
				  <%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%>
				<% }
			   
			   if (StudyNumMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomstudynum">* </FONT>
		 	   <% }
				//KM:31July08
				if (autoGenStudy.equals("1") && mode.equals("N")) {
			   %>
			   (<%=LC.L_SysHypenGen%><%--system-generated*****--%>)
			   <% }
			     if(StudyNumAtt.equals("1")) {
			       disableStr = "disabled"; }
		         else if (StudyNumAtt.equals("2") ) {
			       readOnlyStr = "readonly";
			     }
  		       %>
 			  </td>
			  <td> 
   				        <input type="text" name="studyNumber" size = 40 MAXLENGTH = 100   <%=disableStr%> <%=readOnlyStr%>  value="<%=studyNumber%>" dojoType="ValidationTextBox" required="true" >
				        </td>					
				<% } else { %>
				
				 <input type="hidden" name="studyNumber" size = 40 MAXLENGTH = 100   value="<%=studyNumber%>" >
				<% }}  else {%>
					
				<% 
				//Modified by Manimaran on July31,08
				if (autoGenStudy.equals("1")){ %>
						<td width="20%"> <%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%>  <FONT class="Mandatory" id="mandsnumber">* </FONT> <% if (mode.equals("N")) { %> (<%=LC.L_SysHypenGen%><%--system-generated*****--%>) <%}%> </td>
				<%}else { %>
				        <td width="20%"> <%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%> <FONT class="Mandatory" id="mandsnumber">* </FONT> </td>
				<%} %>
				        <td><input type="text" name="studyNumber" size = 40 MAXLENGTH = 100 value="<%=studyNumber%>" dojoType="ValidationTextBox" required="true" ></td>
			 <%}%>
			</tr>

			
			<tr>
				<%
				if (hashPgCustFld.containsKey("studytitle")) {
				
				int fldNumStudyTitle = Integer.parseInt((String)hashPgCustFld.get("studytitle"));
				String stdTitleMand = ((String)cdoPgField.getPcfMandatory().get(fldNumStudyTitle));
				String stdTitleLable = ((String)cdoPgField.getPcfLabel().get(fldNumStudyTitle));
				String stdTitleAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumStudyTitle));
			
				disableStr ="";
				readOnlyStr ="";
				
				if(stdTitleAtt == null) stdTitleAtt =""; 
  				if(stdTitleMand == null) stdTitleMand =""; 

				if(!stdTitleAtt.equals("0")) {

				if(stdTitleLable !=null){
				%>
				<td width="20%">
				 <%=stdTitleLable%> 
				<%} else {%> <td width="20%">
				<%=LC.L_Title%><%--Title*****--%>
				<% }
			
				
			   if (stdTitleMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomstudytitle">* </FONT>
		 	   <% }
			   %>
				
			   <%if(stdTitleAtt.equals("1")) {
			       disableStr = "disabled"; }
		         else if (stdTitleAtt.equals("2") ) {
			       readOnlyStr = "readonly"; }
  		       %>
 			  </td>
				
              <td colspan="2"> <TextArea name="studyTitle" rows=3 cols=70 MAXLENGTH = 1000  <%=disableStr%>  <%=readOnlyStr%> > <%=studyTitle%> </TextArea> </td>
			
			  <% } else { %>
			   <TextArea name="studyTitle"  Style = "visibility:hidden"  rows=2 cols=70 MAXLENGTH = 1000> <%=studyTitle%> </TextArea>
			  
			 <% }}  else {%>
				
				<td width="20%"><%=LC.L_Title%><%--Title*****--%><FONT class="Mandatory" id="mandtitle">* </FONT> </td>
			        <td colspan="2"> 
				        <TextArea name="studyTitle" rows=2 cols=70 MAXLENGTH = 1000  ><%=studyTitle%></TextArea>
			        </td>
			 <%}%>
			 </tr>		
		
			
		<tr> 
       
       <%if (hashPgCustFld.containsKey("division")) {
			int fldNumDiv = Integer.parseInt((String)hashPgCustFld.get("division"));
			String divMand = ((String)cdoPgField.getPcfMandatory().get(fldNumDiv));
			String divLable = ((String)cdoPgField.getPcfLabel().get(fldNumDiv));
			divAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumDiv));

			if(divAtt == null) divAtt ="";
			if(divMand == null) divMand ="";
			
			if(!divAtt.equals("0")) {
			if(divLable !=null){
			%><td width="20%">
			<%=divLable%>
			<%} else {%> <td width="20%">
			<%=LC.L_Division%><%--Division*****--%>
			<%}
			
			if (divMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomdivision">* </FONT>
			<% }
		 %>
      </td>
      <td>
		<%=studyDivision%> <%if(divAtt.equals("1")) {%><input type="hidden" name="studyDivision" value=""> <%}%><!--KM-->
     </td>

	 <%} else { %>
	
	 <input type="hidden" name="studyDivision" value=<%=studyDiv%> > 
	
	 <%}} else {%>
	  <td width="20%"> <%=LC.L_Division%><%--Division*****--%> </td>
	  <td> <%=studyDivision%> </td>

	 <% }%>

     </tr>


				  
     <tr>
			
	 <%     if (hashPgCustFld.containsKey("therapeuticarea")) {
			int fldNumTarea = Integer.parseInt((String)hashPgCustFld.get("therapeuticarea"));
			String tareaMand = ((String)cdoPgField.getPcfMandatory().get(fldNumTarea));
			String tareaLable = ((String)cdoPgField.getPcfLabel().get(fldNumTarea));
			tareaAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumTarea));

			if(tareaAtt == null) tareaAtt ="";
			if(tareaMand == null) tareaMand ="";
			
			if(!tareaAtt.equals("0")) {
			if(tareaLable !=null){
			%><td width="20%">
			<%=tareaLable%>
			<%} else {%> <td width="20%">
			<%=LC.L_Therapeutic_Area%><%--Therapeutic Area*****--%>
			<%}
			
			if (tareaMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomtarea">* </FONT>
			<% }
	  %>
      </td>
       <td> <span id = "span_tarea"> <%=studyTArea%> </span>
		 <%if(tareaAtt.equals("1")) {%><input type="hidden" name="studyTArea" value=""> <%}%><!--KM-->
      </td>

	  <%} else if(tareaAtt.equals("0")) {%>
	
	  <input type="hidden" name="studyTArea" value= <%=studyB.getStudyTArea()%>  > <!--KM-->
	
	 <%}} else {%>
	   <td width="20%"> <%=LC.L_Therapeutic_Area%><%--Therapeutic Area*****--%> <FONT class="Mandatory" id="mandtarea">* </FONT> </td>
	   <td><span id = "span_tarea"> <%=studyTArea%> </span></td>
	 <% }%>	  
			  
	 </tr>	  
      <%if(irbFlag.equalsIgnoreCase("Y")){ %>
			<tr>
			<td>
			
			</td>
			</tr>
			<tr>
			<td>
			
			</td>
			</tr>
			<tr>
			<td>
			
			</td>
			</tr>
			<tr>
			 <td width="20%">Study Compliance Status</td>
	   <td><span id = "span_tarea"> <%=ecompStatus%></span><A HREF=# onClick=openwin2("<%=src%>","<%=selectedTab%>","<%=studyId%>") >&nbsp;&nbsp;<img src="./images/History.gif" style="height: 23px; width: 20px";></img></A></td>
			</tr>	

       <% } %>
				</table>
			    
    			<br>
				<table width="98%" cellspacing="0" cellpadding="0">
					<tr>
						<td align=right> 
					        <%if (mode.equals("N"))
								{%>
									<jsp:include page="submitBar.jsp" flush="true"> 
											<jsp:param name="displayESign" value="Y"/>
											<jsp:param name="formID" value="studyForm"/>
											<jsp:param name="showDiscard" value="N"/>
									</jsp:include>
						        <%}
							else
							{
									if (stdRight>= 6)
									{
									%>
										<jsp:include page="submitBar.jsp" flush="true"> 
												<jsp:param name="displayESign" value="Y"/>
												<jsp:param name="formID" value="studyForm"/>
												<jsp:param name="showDiscard" value="N"/>
										</jsp:include>				
							        <%
								}
							}
								%>
						</td>
					</tr> 
				</table>
			</Form>
		<%
		}else {
		%>
			<jsp:include page="accessdenied.jsp" flush="true"/>
			<%
		}
	}//end of if body for session
   	else
	{
	%>

	<jsp:include page="irbnewtabs.jsp" flush="true"> 
	<jsp:param name="from" value='<%=from%>'/> 
	<jsp:param name="selectedTab" value="<%=selectedTab%>"/>
	<jsp:param name="studyId" value="<%=studyId%>"/>
	</jsp:include>	
	
		<jsp:include page="timeout.html" flush="true"/>
	<%
	}
	%>

	<div class = "myHomebottomPanel"> 
	    <jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</DIV>
	
<div class ="mainMenu" id="emenu"> 
  	<jsp:include page="getmenu.jsp" flush="true"/>
</div>

</body>

</html>