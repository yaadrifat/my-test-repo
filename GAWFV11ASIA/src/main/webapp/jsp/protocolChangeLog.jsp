<%@page import="org.json.*"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true" />
<html>
<style>
table.memoContentHeight > tbody
{
  height: 250px !important;
}
</style>
<head>
<%
String isNewAmendment="false";
session.setAttribute("isNewAmendment", isNewAmendment);
boolean isIrb = "irb_check_tab".equals(request.getParameter("selectedTab")) ? true : false; 
if (isIrb) {
%>
<title><%=MC.M_ResCompApp_ChkSub%><%--Research Compliance >> New Application >> Check and Submit*****--%></title>
<% } else { %>
<title><%=MC.M_Std_CkhSubmit%><%--<%=LC.Std_Study%> >> Check and Submit*****--%></title>
<% } %>
<%@ page
	import="java.util.*,java.io.*,org.w3c.dom.*,org.json.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*"%>
<%@ page import="com.velos.esch.business.common.SchMsgTxtDao"%>
<%@ page import="com.velos.eres.compliance.web.ComplianceJB"%>
<%@ page import="com.velos.eres.web.grpRights.GrpRightsJB" %>
<%@ page
	import="com.velos.eres.widget.business.common.UIFlxPageDao,com.velos.eres.widget.service.util.FlxPageArchive"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ page import="com.velos.eres.compliance.web.ProtocolChangeLog"%>

</head>
<jsp:useBean id="sessionmaint" scope="session"
	class="com.velos.eres.service.util.SessionMaint" />
<jsp:useBean id="studyB" scope="page"
	class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="complianceJB" scope="request" class="com.velos.eres.compliance.web.ComplianceJB" />

<% 
String src= request.getParameter("srcmenu");
HttpSession tSession = request.getSession(true); 
String accountId = "";
String studyId = "";
String grpId = "";
String usrId = "";
ArrayList boardNameList = null;
ArrayList boardIdList = null;
ArrayList statusList = null;
ArrayList statusDateList = null;
EIRBDao eIrbDao = new EIRBDao();
int count = 0;


if (!sessionmaint.isValidSession(tSession))
{
%>	
<jsp:include page="index.html" flush="true" />
<%
	return;
} else {
    accountId = (String) tSession.getValue("accountId");
    studyId = (String) tSession.getValue("studyId");
    grpId = (String) tSession.getValue("defUserGroup");
    usrId = (String) tSession.getValue("userId");
}

int iStudyId = StringUtil.stringToNum(studyId);
ProtocolChangeLog protocolChangeLog = new ProtocolChangeLog();

/*int cntVersionSubmitted = -1;
int cntProtocolFrozen = -1;

FlxPageArchive archive = new FlxPageArchive();
cntVersionSubmitted = archive.chkPageVersionSubmited(StringUtil.stringToNum(studyId));
if (cntVersionSubmitted == 0){
	//Check if protocol is frozen
	cntProtocolFrozen = archive.chkPageProtocolFrozen(StringUtil.stringToNum(studyId));
} else {
    // Check based on study status history
    if (cntVersionSubmitted == 2) {
    	//isBlockedForSubmission = true;
    } else {
    	//isBlockedForSubmission = complianceJB.getIsBlockedForSubmission();
    }
}
boolean isProtocolLocked= complianceJB.getIsLockedStudy();
if (isProtocolLocked && cntProtocolFrozen == 1){
	
} else {
	
}*/

String comments = protocolChangeLog.fetchAdditionalComments(iStudyId);
comments = StringUtil.isEmpty(comments)? "" : comments;
JSONArray versionDiffList = protocolChangeLog.fetchProtocolVersionDiff(iStudyId, grpId);
JSONArray attachDiffList = protocolChangeLog.fetchAttachProtocolVersionDiff(iStudyId, grpId);
JSONArray formRespDiffList = protocolChangeLog.fetchFormResponseDiff(iStudyId,grpId);
%>
<jsp:include page="include.jsp" flush="true" />
<jsp:include page="ui-include.jsp" flush="true"></jsp:include>
<%if (!"LIND".equals(CFG.EIRB_MODE)){ %>
<jsp:include page="panel.jsp" flush="true">
	<jsp:param name="src" value="<%=src%>" />
</jsp:include>
<%} %>
<link href="styles/studyVersion.css" rel="stylesheet" type="text/css" />
<SCRIPT LANGUAGE="JavaScript">
var screenWidth = screen.width;
var screenHeight = screen.height;
</SCRIPT>
<SCRIPT language="javascript">
$j(document).ready(function() {
	$j(".title").children().css("border", "0");
	$j("html").css("overflow", "hidden");
	$j("body").css("overflow", "hidden");

	var divHeight = (screenHeight > 768)? 400 : 280;
	$j("div.ScrollStyle").css({"overflow-y":"hidden", "overflow-x":"auto", "width":"91%", "height":""+divHeight});
	var tbodyHeight = (screenHeight > 768)? 351 : 231;
	$j("tbody.scrollContent").css({"height":""+tbodyHeight});
	
	if (navigator.appName == 'Microsoft Internet Explorer'){
		var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
		var rv;
		if (re.exec(navigator.userAgent) != null)
			rv = parseFloat( RegExp.$1 );
		if (rv < 11){
			$j("div.ScrollStyle").css({"overflow-y":"auto"});
			$j("thead.fixedHeader").css({"display": "table-header-group"});
		}
	}
});

function saveResubmissionDraft() {
	var elements =   <%=versionDiffList%> ;
	var size = <%=versionDiffList.length()%> ;

	for (var iX=0; iX < size; iX++) {
		var str = elements[iX].fieldLabel;
		var notes = document.getElementById(str+"_notes");
		var reviewBoard = document.getElementById(str+"_revBoard");
		elements[iX].notes = notes.value;
		elements[iX].revBoard = reviewBoard.value;
	}
	
	var objs =   <%=attachDiffList%> ;
	var length = <%=attachDiffList.length()%> ;
	
	for (var iY=0; iY < length; iY++) {
		
		var str = objs[iY].fieldLabel;		
		if(str == "Attachment_New") {
			var notes = document.getElementById(str+"_notes1");
			var reviewBoard = document.getElementById(str+"_revBoard1");
			objs[iY].notes1 = notes.value;
			objs[iY].revBoard1 = reviewBoard.value;
		} else if(str == "Attachment_Deleted") {
			var notes = document.getElementById(str+"_notes2");
			var reviewBoard = document.getElementById(str+"_revBoard2");
			objs[iY].notes2 = notes.value;
			objs[iY].revBoard2 = reviewBoard.value;
		
		}		
	}

	var forms =   <%=formRespDiffList%> ;
	var formslen = <%=formRespDiffList.length()%> ;
	for (var iX=0; iX < formslen; iX++) {
		
		var str = forms[iX].fieldLabel;
		var notes = document.getElementById(str+"_notes");
		var reviewBoard = document.getElementById(str+"_revBoard");
		forms[iX].notes = notes.value;
		forms[iX].revBoard = reviewBoard.value; 
		
	} 
	var text = JSON.stringify(elements);
	var text1 = JSON.stringify(objs);
	var text2 = JSON.stringify(forms);
	 if (text.indexOf("'")>=0) text=replaceSubstring(text,"'","[SQuote]");
	 if (text.indexOf("\"")>=0) text=replaceSubstring(text,"\"","[DQuote]");
	 if (text.indexOf("\r")>=0) text=replaceSubstring(text,"\r","[CRet]");
	 if (text.indexOf("\n")>=0) text=replaceSubstring(text,"\n","[\n]");
	 if (text.indexOf("\t")>=0) text=replaceSubstring(text,"\t","[HTab]");
	 if (text.indexOf("\\")>=0) text=replaceSubstring(text,"\\","[BSlash]");
	 if (text.indexOf("#")>=0) text=replaceSubstring(text,"#","[VelHash]");
	 if (text.indexOf(",")>=0) text=replaceSubstring(text,",","[VELCOMMA]");	 
	 if (text.indexOf("%")>=0) text=replaceSubstring(text,"%","[VELPERCENT]");
	 if (text.indexOf("<")>=0) text=replaceSubstring(text,"<","[VELLTSIGN]");
	 if (text.indexOf(">")>=0) text=replaceSubstring(text,">","[VELGTSIGN]");
	 if (text.indexOf("&")>=0) text=replaceSubstring(text,"&","[VELAMP]");
	 if (text.indexOf(";")>=0) text=replaceSubstring(text,";","[VELSEMICOLON]");
	 if (text.indexOf("+")>=0) text=replaceSubstring(text,"+","[VELPLUS]");
	 if (text.indexOf(" ")>=0) text=replaceSubstring(text," ","[VELSP]");

	 if (text1.indexOf("'")>=0) text1=replaceSubstring(text1,"'","[SQuote]");
	 if (text1.indexOf("\"")>=0) text1=replaceSubstring(text1,"\"","[DQuote]");
	 if (text1.indexOf("\r")>=0) text1=replaceSubstring(text1,"\r","[CRet]");
	 if (text1.indexOf("\n")>=0) text1=replaceSubstring(text1,"\n","[\n]");
	 if (text1.indexOf("\t")>=0) text1=replaceSubstring(text1,"\t","[HTab]");
	 if (text1.indexOf("\\")>=0) text1=replaceSubstring(text1,"\\","[BSlash]");
	 if (text1.indexOf("#")>=0) text1=replaceSubstring(text1,"#","[VelHash]");
	 if (text1.indexOf(",")>=0) text1=replaceSubstring(text1,",","[VELCOMMA]");	 
	 if (text1.indexOf("%")>=0) text1=replaceSubstring(text1,"%","[VELPERCENT]");
	 if (text1.indexOf("<")>=0) text1=replaceSubstring(text1,"<","[VELLTSIGN]");
	 if (text1.indexOf(">")>=0) text1=replaceSubstring(text1,">","[VELGTSIGN]");
	 if (text1.indexOf("&")>=0) text1=replaceSubstring(text1,"&","[VELAMP]");
	 if (text1.indexOf(";")>=0) text1=replaceSubstring(text1,";","[VELSEMICOLON]");
	 if (text1.indexOf("+")>=0) text1=replaceSubstring(text1,"+","[VELPLUS]");
	 if (text1.indexOf(" ")>=0) text1=replaceSubstring(text1," ","[VELSP]");

	 if (text2.indexOf("'")>=0) text2=replaceSubstring(text2,"'","[SQuote]");
	 if (text2.indexOf("\"")>=0) text2=replaceSubstring(text2,"\"","[DQuote]");
	 if (text2.indexOf("\r")>=0) text2=replaceSubstring(text2,"\r","[CRet]");
	 if (text2.indexOf("\n")>=0) text2=replaceSubstring(text2,"\n","[\n]");
	 if (text2.indexOf("\t")>=0) text2=replaceSubstring(text2,"\t","[HTab]");
	 if (text2.indexOf("\\")>=0) text2=replaceSubstring(text2,"\\","[BSlash]");
	 if (text2.indexOf("#")>=0) text2=replaceSubstring(text2,"#","[VelHash]");
	 if (text2.indexOf(",")>=0) text2=replaceSubstring(text2,",","[VELCOMMA]");	 
	 if (text2.indexOf("%")>=0) text2=replaceSubstring(text2,"%","[VELPERCENT]");
	 if (text2.indexOf("<")>=0) text2=replaceSubstring(text2,"<","[VELLTSIGN]");
	 if (text2.indexOf(">")>=0) text2=replaceSubstring(text2,">","[VELGTSIGN]");
	 if (text2.indexOf("&")>=0) text2=replaceSubstring(text2,"&","[VELAMP]");
	 if (text2.indexOf(";")>=0) text2=replaceSubstring(text2,";","[VELSEMICOLON]");
	 if (text2.indexOf("+")>=0) text2=replaceSubstring(text2,"+","[VELPLUS]");
	 if (text2.indexOf(" ")>=0) text2=replaceSubstring(text2," ","[VELSP]");
	if (!(validate_col('e-Signature',document.mainForm.eSign))) return false;
	document.getElementById("fieldDetails").value = text;
	document.getElementById("attachFieldDetails").value = text1;
	document.getElementById("formRespDetails").value = text2;

	document.mainForm.action="saveResubmissionDraft.jsp?&srcmenu=<%=request.getParameter("srcmenu")%>&selectedTab=irb_check_tab"; 
	document.mainForm.submit();
	return true;
}

function unfreezeProtocol() {
	var elements =   <%=versionDiffList%> ;
	var size = <%=versionDiffList.length()%> ;

	for (var iX=0; iX < size; iX++) {
		
		var str = elements[iX].fieldLabel;
			
		var notes = document.getElementById(str+"_notes");
		var reviewBoard = document.getElementById(str+"_revBoard");
		elements[iX].notes = notes.value;
		elements[iX].revBoard = reviewBoard.value;
	}
	
	var objs =   <%=attachDiffList%> ;
	var length = <%=attachDiffList.length()%> ;
	
	for (var iY=0; iY < length; iY++) {
		
		var str = objs[iY].fieldLabel;		
		if(str == "Attachment_New") {
			var notes = document.getElementById(str+"_notes1");
			var reviewBoard = document.getElementById(str+"_revBoard1");
			objs[iY].notes1 = notes.value;
			objs[iY].revBoard1 = reviewBoard.value;
		} else if(str == "Attachment_Deleted") {
			var notes = document.getElementById(str+"_notes2");
			var reviewBoard = document.getElementById(str+"_revBoard2");
			objs[iY].notes2 = notes.value;
			objs[iY].revBoard2 = reviewBoard.value;
		
		}		
	}

	var forms =   <%=formRespDiffList%> ;
	var formslen = <%=formRespDiffList.length()%> ;
	for (var iX=0; iX < formslen; iX++) {
		
		var str = forms[iX].fieldLabel;
		var notes = document.getElementById(str+"_notes");
		var reviewBoard = document.getElementById(str+"_revBoard");
		forms[iX].notes = notes.value;
		forms[iX].revBoard = reviewBoard.value; 
		
	} 
	var text = JSON.stringify(elements);
	var text1 = JSON.stringify(objs);
	var text2 = JSON.stringify(forms);
	 if (text.indexOf("'")>=0) text=replaceSubstring(text,"'","[SQuote]");
	 if (text.indexOf("\"")>=0) text=replaceSubstring(text,"\"","[DQuote]");
	 if (text.indexOf("\r")>=0) text=replaceSubstring(text,"\r","[CRet]");
	 if (text.indexOf("\n")>=0) text=replaceSubstring(text,"\n","[\n]");
	 if (text.indexOf("\t")>=0) text=replaceSubstring(text,"\t","[HTab]");
	 if (text.indexOf("\\")>=0) text=replaceSubstring(text,"\\","[BSlash]");
	 if (text.indexOf("#")>=0) text=replaceSubstring(text,"#","[VelHash]");
	 if (text.indexOf(",")>=0) text=replaceSubstring(text,",","[VELCOMMA]");	 
	 if (text.indexOf("%")>=0) text=replaceSubstring(text,"%","[VELPERCENT]");
	 if (text.indexOf("<")>=0) text=replaceSubstring(text,"<","[VELLTSIGN]");
	 if (text.indexOf(">")>=0) text=replaceSubstring(text,">","[VELGTSIGN]");
	 if (text.indexOf("&")>=0) text=replaceSubstring(text,"&","[VELAMP]");
	 if (text.indexOf(";")>=0) text=replaceSubstring(text,";","[VELSEMICOLON]");
	 if (text.indexOf("+")>=0) text=replaceSubstring(text,"+","[VELPLUS]");
	 if (text.indexOf(" ")>=0) text=replaceSubstring(text," ","[VELSP]");

	 if (text1.indexOf("'")>=0) text1=replaceSubstring(text1,"'","[SQuote]");
	 if (text1.indexOf("\"")>=0) text1=replaceSubstring(text1,"\"","[DQuote]");
	 if (text1.indexOf("\r")>=0) text1=replaceSubstring(text1,"\r","[CRet]");
	 if (text1.indexOf("\n")>=0) text1=replaceSubstring(text1,"\n","[\n]");
	 if (text1.indexOf("\t")>=0) text1=replaceSubstring(text1,"\t","[HTab]");
	 if (text1.indexOf("\\")>=0) text1=replaceSubstring(text1,"\\","[BSlash]");
	 if (text1.indexOf("#")>=0) text1=replaceSubstring(text1,"#","[VelHash]");
	 if (text1.indexOf(",")>=0) text1=replaceSubstring(text1,",","[VELCOMMA]");	 
	 if (text1.indexOf("%")>=0) text1=replaceSubstring(text1,"%","[VELPERCENT]");
	 if (text1.indexOf("<")>=0) text1=replaceSubstring(text1,"<","[VELLTSIGN]");
	 if (text1.indexOf(">")>=0) text1=replaceSubstring(text1,">","[VELGTSIGN]");
	 if (text1.indexOf("&")>=0) text1=replaceSubstring(text1,"&","[VELAMP]");
	 if (text1.indexOf(";")>=0) text1=replaceSubstring(text1,";","[VELSEMICOLON]");
	 if (text1.indexOf("+")>=0) text1=replaceSubstring(text1,"+","[VELPLUS]");
	 if (text1.indexOf(" ")>=0) text1=replaceSubstring(text1," ","[VELSP]");


	 if (text2.indexOf("'")>=0) text2=replaceSubstring(text2,"'","[SQuote]");
	 if (text2.indexOf("\"")>=0) text2=replaceSubstring(text2,"\"","[DQuote]");
	 if (text2.indexOf("\r")>=0) text2=replaceSubstring(text2,"\r","[CRet]");
	 if (text2.indexOf("\n")>=0) text2=replaceSubstring(text2,"\n","[\n]");
	 if (text2.indexOf("\t")>=0) text2=replaceSubstring(text2,"\t","[HTab]");
	 if (text2.indexOf("\\")>=0) text2=replaceSubstring(text2,"\\","[BSlash]");
	 if (text2.indexOf("#")>=0) text2=replaceSubstring(text2,"#","[VelHash]");
	 if (text2.indexOf(",")>=0) text2=replaceSubstring(text2,",","[VELCOMMA]");	 
	 if (text2.indexOf("%")>=0) text2=replaceSubstring(text2,"%","[VELPERCENT]");
	 if (text2.indexOf("<")>=0) text2=replaceSubstring(text2,"<","[VELLTSIGN]");
	 if (text2.indexOf(">")>=0) text2=replaceSubstring(text2,">","[VELGTSIGN]");
	 if (text2.indexOf("&")>=0) text2=replaceSubstring(text2,"&","[VELAMP]");
	 if (text2.indexOf(";")>=0) text2=replaceSubstring(text2,";","[VELSEMICOLON]");
	 if (text2.indexOf("+")>=0) text2=replaceSubstring(text2,"+","[VELPLUS]");
	 if (text2.indexOf(" ")>=0) text2=replaceSubstring(text2," ","[VELSP]");
	if (!(validate_col('e-Signature',document.mainForm.eSign))) return false;
	document.getElementById("fieldDetails").value = text;
	document.getElementById("attachFieldDetails").value = text1;
	document.getElementById("formRespDetails").value = text2;

	if (!(validate_col('e-Signature',document.mainForm.eSign))) return false;
	document.mainForm.action="unfreezeProtocol.jsp?&srcmenu=<%=request.getParameter("srcmenu")%>&selectedTab=irb_check_tab"; 
	document.mainForm.submit();
	return true;
}

$j(document).ready(function(){
	var saveDraftBtn = document.getElementById('saveDraftBtn');
	var unfreezeProtocolBtn = document.getElementById('unfreezeProtocolBtn');

	if (!window.addEventListener) {
		saveDraftBtn.attachEvent('onclick', function(e) {
			saveResubmissionDraft();
			return false;
		});
		unfreezeProtocolBtn.attachEvent('onclick', function(e) {
			unfreezeProtocol();
			return false;
		});
	} else {
		saveDraftBtn.addEventListener('click', function(e) {
			saveResubmissionDraft();
			return false;
		});
		unfreezeProtocolBtn.addEventListener('click', function(e) {
			unfreezeProtocol();
			return false;
		});
	}
});

function submitForm(formobj) {
	var elements =   <%=versionDiffList%> ;
	
	var size = <%=versionDiffList.length()%> ;

	for (var iX=0; iX < size; iX++) {
		
		var str = elements[iX].fieldLabel;
			
		var notes = document.getElementById(str+"_notes");
		if(notes && notes.value == "")
		{
			alert("<%=LC.Change_Version_Reason_for_Change_Mandatory%>");
			notes.focus();
			return false;
		}
		
		var reviewBoard = document.getElementById(str+"_revBoard");
		if(reviewBoard && reviewBoard.value == "")
		{
			alert("<%=LC.Change_Version_Review_Board_Mandatory%>");
			reviewBoard.focus();
			return false;
		}
		elements[iX].notes = notes.value;
		elements[iX].revBoard = reviewBoard.value;
	}
	
	var objs =   <%=attachDiffList%> ;
	var length = <%=attachDiffList.length()%> ;
	
	for (var iY=0; iY < length; iY++) {
		
		var str = objs[iY].fieldLabel;		
		if(str == "Attachment_New") {
			var notes = document.getElementById(str+"_notes1");
			if(notes && notes.value == "")
			{
				alert("<%=LC.Change_Version_Reason_for_Change_Mandatory%>"); 
				notes.focus();
				return false;
			}
			var reviewBoard = document.getElementById(str+"_revBoard1");
			if(reviewBoard && reviewBoard.value == "")
			{
				alert("<%=LC.Change_Version_Review_Board_Mandatory%>");
				reviewBoard.focus();
				return false;
			}
			objs[iY].notes1 = notes.value;
			objs[iY].revBoard1 = reviewBoard.value;
		} else if(str == "Attachment_Deleted") {
			var notes = document.getElementById(str+"_notes2");
			if(notes && notes.value == "")
			{
				alert("<%=LC.Reason_For_Change_Mandatory%>"); 
				notes.focus();
				return false;
			}
			var reviewBoard = document.getElementById(str+"_revBoard2");
			if(reviewBoard && reviewBoard.value == "")
			{
				alert("<%=LC.Review_Board_Mandatory%>");
				reviewBoard.focus();
				return false;
			}
			objs[iY].notes2 = notes.value;
			objs[iY].revBoard2 = reviewBoard.value;
		
		}		
	}

	var forms =   <%=formRespDiffList%> ;
	var formslen = <%=formRespDiffList.length()%> ;
	for (var iX=0; iX < formslen; iX++) {
		
		var str = forms[iX].fieldLabel;
		var notes = document.getElementById(str+"_notes");
		if(notes && notes.value == "")
		{
			alert("<%=LC.Change_Version_Reason_for_Change_Mandatory%>");
			notes.focus();
			return false;
		}

		var reviewBoard = document.getElementById(str+"_revBoard");

		if(reviewBoard && reviewBoard.value == "")
		{
			alert("<%=LC.Change_Version_Review_Board_Mandatory%>");
			reviewBoard.focus();
			return false;
		}
		forms[iX].notes = notes.value;
		forms[iX].revBoard = reviewBoard.value; 
		
	} 
	
	var comments = $j('#comments').val();
	if (!comments || comments.length < 1){
		alert("<%=MC.M_Etr_MandantoryFlds%>");
		$j('#comments').focus();
		return false;
	}

	if (!(validate_col('e-Signature',document.mainForm.eSign))) return false;
	var text = JSON.stringify(elements);
	var text1 = JSON.stringify(objs);
	var text2 = JSON.stringify(forms);
	 if (text.indexOf("'")>=0) text=replaceSubstring(text,"'","[SQuote]");
	 if (text.indexOf("\"")>=0) text=replaceSubstring(text,"\"","[DQuote]");
	 if (text.indexOf("\r")>=0) text=replaceSubstring(text,"\r","[CRet]");
	 if (text.indexOf("\n")>=0) text=replaceSubstring(text,"\n","[\n]");
	 if (text.indexOf("\t")>=0) text=replaceSubstring(text,"\t","[HTab]");
	 if (text.indexOf("\\")>=0) text=replaceSubstring(text,"\\","[BSlash]");
	 if (text.indexOf("#")>=0) text=replaceSubstring(text,"#","[VelHash]");
	 if (text.indexOf(",")>=0) text=replaceSubstring(text,",","[VELCOMMA]");	 
	 if (text.indexOf("%")>=0) text=replaceSubstring(text,"%","[VELPERCENT]");
	 if (text.indexOf("<")>=0) text=replaceSubstring(text,"<","[VELLTSIGN]");
	 if (text.indexOf(">")>=0) text=replaceSubstring(text,">","[VELGTSIGN]");
	 if (text.indexOf("&")>=0) text=replaceSubstring(text,"&","[VELAMP]");
	 if (text.indexOf(";")>=0) text=replaceSubstring(text,";","[VELSEMICOLON]");
	 if (text.indexOf("+")>=0) text=replaceSubstring(text,"+","[VELPLUS]");
	 if (text.indexOf(" ")>=0) text=replaceSubstring(text," ","[VELSP]");
	 if (text1.indexOf("'")>=0) text1=replaceSubstring(text1,"'","[SQuote]");
	 if (text1.indexOf("\"")>=0) text1=replaceSubstring(text1,"\"","[DQuote]");
	 if (text1.indexOf("\r")>=0) text1=replaceSubstring(text1,"\r","[CRet]");
	 if (text1.indexOf("\n")>=0) text1=replaceSubstring(text1,"\n","[\n]");
	 if (text1.indexOf("\t")>=0) text1=replaceSubstring(text1,"\t","[HTab]");
	 if (text1.indexOf("\\")>=0) text1=replaceSubstring(text1,"\\","[BSlash]");
	 if (text1.indexOf("#")>=0) text1=replaceSubstring(text1,"#","[VelHash]");
	 if (text1.indexOf(",")>=0) text1=replaceSubstring(text1,",","[VELCOMMA]");	 
	 if (text1.indexOf("%")>=0) text1=replaceSubstring(text1,"%","[VELPERCENT]");
	 if (text1.indexOf("<")>=0) text1=replaceSubstring(text1,"<","[VELLTSIGN]");
	 if (text1.indexOf(">")>=0) text1=replaceSubstring(text1,">","[VELGTSIGN]");
	 if (text1.indexOf("&")>=0) text1=replaceSubstring(text1,"&","[VELAMP]");
	 if (text1.indexOf(";")>=0) text1=replaceSubstring(text1,";","[VELSEMICOLON]");
	 if (text1.indexOf("+")>=0) text1=replaceSubstring(text1,"+","[VELPLUS]");
	 if (text1.indexOf(" ")>=0) text1=replaceSubstring(text1," ","[VELSP]");
	 if (text2.indexOf("'")>=0) text2=replaceSubstring(text2,"'","[SQuote]");
	 if (text2.indexOf("\"")>=0) text2=replaceSubstring(text2,"\"","[DQuote]");
	 if (text2.indexOf("\r")>=0) text2=replaceSubstring(text2,"\r","[CRet]");
	 if (text2.indexOf("\n")>=0) text2=replaceSubstring(text2,"\n","[\n]");
	 if (text2.indexOf("\t")>=0) text2=replaceSubstring(text2,"\t","[HTab]");
	 if (text2.indexOf("\\")>=0) text2=replaceSubstring(text2,"\\","[BSlash]");
	 if (text2.indexOf("#")>=0) text2=replaceSubstring(text2,"#","[VelHash]");
	 if (text2.indexOf(",")>=0) text2=replaceSubstring(text2,",","[VELCOMMA]");	 
	 if (text2.indexOf("%")>=0) text2=replaceSubstring(text2,"%","[VELPERCENT]");
	 if (text2.indexOf("<")>=0) text2=replaceSubstring(text2,"<","[VELLTSIGN]");
	 if (text2.indexOf(">")>=0) text2=replaceSubstring(text2,">","[VELGTSIGN]");
	 if (text2.indexOf("&")>=0) text2=replaceSubstring(text2,"&","[VELAMP]");
	 if (text2.indexOf(";")>=0) text2=replaceSubstring(text2,";","[VELSEMICOLON]");
	 if (text2.indexOf("+")>=0) text2=replaceSubstring(text2,"+","[VELPLUS]");
	 if (text2.indexOf(" ")>=0) text2=replaceSubstring(text2," ","[VELSP]");
	 document.getElementById("fieldDetails").value = text;
	 document.getElementById("attachFieldDetails").value = text1;
	 document.getElementById("formRespDetails").value = text2;
	//alert("forms::::"+JSON.stringify(forms));
	document.mainForm.action="updateNewSubmission.jsp?&srcmenu=<%=request.getParameter("srcmenu")%>&selectedTab=irb_check_tab&from=resub_draft"; 
	return true;
}


</SCRIPT>

<body>

	<div id="overDiv"
		style="position: absolute; visibility: hidden; z-index: 1000;"></div>
	<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
	<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
	<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

	<jsp:useBean id="studyVerB" scope="request"
		class="com.velos.eres.web.studyVer.StudyVerJB" />
	<jsp:useBean id="appendixB" scope="request"
		class="com.velos.eres.web.appendix.AppendixJB" />
	<%@ page language="java"
		import="com.velos.eres.business.ulink.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.EJBUtil"%>
	<%
	
	if (sessionmaint.isValidSession(tSession))
	{
		GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
	    int pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));
        if(studyId == "" || studyId == null || "0".equals(studyId)) {
	    %>
	<jsp:include page="studyDoesNotExist.jsp" flush="true" />
	<%
	    } else {
        %>
<div class="divBrowserBotN">
	<form name="mainForm" id="mainForm" method="post"
		onsubmit="if (submitForm(this) == false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<input type="hidden" id="fieldDetails" name="fieldDetails" Value="">
	<input type="hidden" id="attachFieldDetails" name="attachFieldDetails" Value="">
	<input type="hidden" id="formRespDetails" name="formRespDetails" Value="">
		<% if (!"LIND".equals(CFG.EIRB_MODE)) {} else {
        	  
        %>
		<table width="90%" cellspacing="4" cellpadding="4" border="0"
			class="midAlign">
			<tr>
				<th><%=LC.Change_Version_Page_Header%></th>
			</tr>
		</table>
		<%
		int totalRows = 0;
		int versionRows = versionDiffList.length();
		totalRows += versionRows;
		int attachRows = attachDiffList.length();
		totalRows += attachRows;
		int formRows = formRespDiffList.length();
		totalRows += formRows;
		%>
		<%if (totalRows > 0){%>
		<div class="ScrollStyle">
		<table class="TFtable" border="1">
			<thead class="fixedHeader headerFormat">
			<tr class="title">
				<th width="10%"><%=LC.Change_Version_Title%></th>
				<th width="10%"><%=LC.Change_Version_Field_Name%></th>
				<th width="15%"><%=LC.Change_Version_Old_Value%></th>
				<th width="15%"><%=LC.Change_Version_New_Value%></th>
				<th width="25%"><%=LC.Change_Version_Reason_for_Change%><FONT class="Mandatory" id="pgcustompatid">*  </FONT>  </th>
				<th width="25%"><%=LC.Change_Version_Review_Board%><FONT class="Mandatory" id="pgcustompatid">*  </FONT>  </th>
			</tr>
			</thead>
			<tbody class="scrollContent bodyFormat" style="height:250px">
			<%for(int i= 0; i < versionRows; i++)
			{
				JSONObject fieldDetails = versionDiffList.getJSONObject(i);
				
				%>
			<%=fieldDetails.get("fieldRow")%>
			<%
			} %>

			<%for(int j= 0; j < attachRows; j++)
			{
				JSONObject attachFieldDetails = attachDiffList.getJSONObject(j);
				
				%>
			<%=attachFieldDetails.get("fieldRow")%>
			<%
			} %>
			
			<%for(int i= 0; i < formRows; i++)
			{
				JSONObject fieldDetails = formRespDiffList.getJSONObject(i);
				
				%>
			<%=fieldDetails.get("fieldRow")%> 
			<%
			} %>
			</tbody>
		</table>
		</div>
		<%} else {%>
		<table width="90%" cellspacing="4" cellpadding="4" border="0"
			class="midAlign">
			<tr>
				<td align="center"><%=LC.Change_Version_No_Diff_Message%></td>
			</tr>
		</table>
		<%} %>
		<table width="95%">
			<tr>
				<td><%=LC.L_Comments%><FONT class='Mandatory' id='pgcustompatid'>*</FONT> </td>
				<td>
					<textarea id="comments" name="comments" rows="4" cols="40"><%=comments%></textarea>
				</td>
				<td>
				<% if(pageRight==6 || pageRight==7 || pageRight==2){ %>
					<jsp:include page="protocolChangeLogBar.jsp" flush="true">
						<jsp:param name="displayESign" value="Y" />
						<jsp:param name="formID" value="irbcheckid" />
						<jsp:param name="showDiscard" value="N" />
					</jsp:include>
					<%} %>
				</td>
			</tr>
		</table>
		<%
		} 
        %>
		
	</form>
	<div id="ajaxSubmitDiv"></div>
</div>
	<%
		}// end of else of study null check
	}//end of if body for session
	else
	{
	%>
	<jsp:include page="timeout.html" flush="true" />
	<%
	}
	%>
	<div>
		<jsp:include page="bottompanel.jsp" flush="true" />
	</div>
	<%if (!"LIND".equals(CFG.EIRB_MODE)){ %>
	</div>
	<%} %>
	<div class="mainMenu" id="emenu">
		<jsp:include page="getmenu.jsp" flush="true" />
	</div>
</body>

</html>
