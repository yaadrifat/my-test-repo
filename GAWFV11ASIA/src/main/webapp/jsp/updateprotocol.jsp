<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<Link Rel=STYLESHEET HREF="common.css" type=text/css>

</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<jsp:include page="include.jsp" flush="true"/> 
<BODY>

<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="alnot" scope="request" class="com.velos.esch.web.alertNotify.AlertNotifyJB"/>
<jsp:useBean id="objShareB" scope="request" class="com.velos.eres.web.objectShare.ObjectShareJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="bgtcaldao" scope="request" class="com.velos.esch.business.common.BudgetcalDao"/>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

  <%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,java.text.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*,com.velos.esch.business.common.*"%>
<div class = "myHomebottomPanel"> 
		<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
<%



HttpSession tSession = request.getSession(true);
	int ret = 0;
	int ret1=0;
	int eventId=0;
	String name ="";
	String description ="";
	String eventType="";
	String duration = "";
	String durationUnit="";
	String durationUnitCode=""; //SV, 9/14/04
	String status="";
	String mode = "";
	String msg="";
	String userId="";
	String accId="";
	String src = null;
	String calledFrom = "";
	String actualSts="";

	String currDate = DateUtil.dateToString(new Date());

	String eSign = request.getParameter("eSign");
	boolean incorrectEsign = false;
	String sharedWith="";
	String sharedWithIds="";
	String objNumber ="";
	String fkObj = "";

    if (sessionmaint.isValidSession(tSession)) {

%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%

//JM: 22May2008, added for, issue #3492

	String oldStatus= request.getParameter("actualSts");
	oldStatus = (oldStatus == null)?"":oldStatus ;


	String selStat ="";
	if (oldStatus.equals("O")){

		selStat = request.getParameter("patstat");
		selStat = (selStat==null)?"":selStat;


	}

	String checkedVal = request.getParameter("patSchChk");
	checkedVal = (checkedVal == null)?"":checkedVal ;
	
	// For PCAL-22446 Enhancement By Parminder Singh
	String patSchUpdateStatus="";
	if(request.getParameterValues("statusflag")!=null)
	{
	String statusflag[]=request.getParameterValues("statusflag");
	for(int i=0;i<statusflag.length;i++){
	if(patSchUpdateStatus=="")
		patSchUpdateStatus+=statusflag[i];
	else
		patSchUpdateStatus=patSchUpdateStatus+","+statusflag[i];
	
	}
	}else{
		patSchUpdateStatus="X";
	}
	String calendarId = request.getParameter("protocolId");


	String dateVal = request.getParameter("dateTxt");
	dateVal = (dateVal==null)?"":dateVal;

	Date dt = null;
	dt = DateUtil.stringToDate(dateVal,"");
	java.sql.Date dtVal = DateUtil.dateToSqlDate(dt);

//JM: 22May2008, added for, issue #3492 ---above

	userId = (String) (tSession.getValue("userId"));
	accId = (String) (tSession.getValue("accountId"));

	String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = null;
	usr = (String) tSession.getValue("userId");

	src = request.getParameter("srcmenu");

	eventType = "P";

	mode = request.getParameter("mode");
	calledFrom = request.getParameter("calledFrom");

	name = StringUtil.stripScript(request.getParameter("protocolName"));
	name = name.trim();

	description = request.getParameter("desc");
	description = (   description  == null      )?"":(  description ) ;

	status = request.getParameter("calStatus");
	status=(status==null)?"":status;

	
	//JM: 08FEB2011, #D-FIN9
	SchCodeDao scho = new SchCodeDao();
	
	int status1 = 0;
	int cal_stat=0;
	if (calledFrom.equals("S")) {	   	  
		 status1 = scho.getCodeId("calStatStd", status);
	} else if ((calledFrom.equals("P")) || (calledFrom.equals("L"))) {
		 status1 = scho.getCodeId("calStatLib", status);
	}



	actualSts = request.getParameter("actualSts");

	String calType=request.getParameter("caltype");
	calType=(calType==null)?"":calType;

	String calAssoc=request.getParameter("calassoc");
	calAssoc=(calAssoc==null)?"":calAssoc;

	String bgtTemplate="0";
	bgtTemplate=request.getParameter("budgetTemplate");
	
	int tempId = 0;
	if ((!EJBUtil.isEmpty(bgtTemplate)) && bgtTemplate!= null){
		tempId =EJBUtil.stringToNum(bgtTemplate);
	}else bgtTemplate =null;
	
	if (actualSts==null) actualSts = "W";
	if (actualSts.trim().equals("")) actualSts = "W";

	int pageRight=0;

	String oldESign = (String) tSession.getValue("eSign");

	String durReduced = request.getParameter("durReduced"); //SV, 8/19/04,


	if (calledFrom.equals("S")) {
   	   StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");
  	   if ((stdRights.getFtrRights().size()) == 0){
		 	pageRight= 0;
	   }else{
			pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));
   	   }
	} else if ((calledFrom.equals("P")) || (calledFrom.equals("L"))) {
			GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
			pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));
	}


//if ( (actualSts.trim().equals("W") || actualSts.trim().equals("D")) &&  ( (mode.equals("M") && pageRight > 5) || (mode.equals("N") && pageRight > 4) ) ) {
// SV, 8/13/04, when it's not 'A' or 'F' (meaning 'D' or 'W') check always. if it's A or F (second part of if below), then check only if status changed.
	if ( (mode.equals("M") && pageRight > 5) || (mode.equals("N") && pageRight > 4) ) {
		if (!(oldESign.equals(eSign))) {

%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>

<%
  return;
  }
}
%>

	<%

	Integer durationNum = new Integer(request.getParameter("durNum"));
	durationUnit = request.getParameter("durUnit");
	//JM: 30May2008,
	durationUnit = (durationUnit == null)?"":durationUnit;

	durationUnitCode = "D";
	String oldStat  = "";
	String chainId = "";

	String offlineFalg ="";//JM: 23Apr2008, added for, Enh. #C9

	if(durationUnit.equals("week")) {
		int temp = durationNum.intValue();
		temp = temp*7;
		durationNum = new Integer(temp);
		durationUnitCode = "W";

	}
	else if(durationUnit.equals("month")) {
		int temp = durationNum.intValue();
		temp = temp*30;
		durationNum = new Integer(temp);
		durationUnitCode = "M";

	}
	else if(durationUnit.equals("year")) {
		int temp = durationNum.intValue();
		temp = temp*365;
		durationNum = new Integer(temp);
		durationUnitCode = "Y";

	}

	duration = durationNum.toString();
//REDTAG - what kind of impact it will have, adding "N" mode in?
	if (mode.equals("M") || (mode.equals("N"))){
	   eventId = EJBUtil.stringToNum(request.getParameter("protocolId"));

	   if (mode.equals("M") && calledFrom.equals("S"))
	   {
	   		eventassocB.setEvent_id(eventId);
         	eventassocB.getEventAssocDetails();
            cal_stat=EJBUtil.stringToNum(eventassocB.getStatCode());
         	if(status1!=cal_stat){
         	eventassocB.setStatusUser(userId);
         	}
         	//JM: 10FEB2011: #D-FIN9
         	//oldStat = 	eventassocB.getStatus();         	
         	oldStat = scho.getCodeSubtype(EJBUtil.stringToNum(eventassocB.getStatCode()));
         	
         	chainId = eventassocB.getChain_id(); //studyId

	   }
//SV<<



	sharedWith="";
	sharedWithIds="";
	objNumber =request.getParameter("objNumber");
	fkObj = request.getParameter("fkObj");
	sharedWith=request.getParameter("rbSharedWith");
	sharedWith = (sharedWith == null)?"":sharedWith;
	sharedWithIds="";

	if(sharedWith.equals("P"))
	{
		sharedWithIds=request.getParameter("selUsrId");
	}
	else if(sharedWith.equals("A"))
	{
		sharedWithIds=request.getParameter("selAccId");
	}
	else if(sharedWith.equals("G"))
	{
		sharedWithIds=request.getParameter("selGrpIds");
	}
	else if(sharedWith.equals("S"))
	{
		sharedWithIds=request.getParameter("selStudyIds");
	}
	else if(sharedWith.equals("O"))
	{
	 sharedWithIds=request.getParameter("selOrgIds");
	}
//SV>>


	}
//if pageRight is view then donot save
if (((!(actualSts.trim().equals("F")))) && ( (mode.equals("M") && pageRight > 5) || (mode.equals("N") && pageRight > 4) ) )
{
	if (calledFrom.equals("P")  || calledFrom.equals("L"))  {

		eventdefB.setDuration(duration);
		eventdefB.setName(name);
		eventdefB.setDescription(description);
		eventdefB.setEventCalType(calType);
		eventdefB.setEvent_type(eventType);
		eventdefB.setUser_id(accId);		
		eventdefB.setStatCode(""+status1);
		eventdefB.setCost("0");
		eventdefB.setDisplacement("0");
		eventdefB.setCalSharedWith(sharedWith);
		eventdefB.setDurationUnit(durationUnitCode);

	} else if (calledFrom.equals("S")) { //called from Study Protocol
		eventassocB.setDuration(duration);
		eventassocB.setName(name);
		eventassocB.setDescription(description);
		eventassocB.setEvent_type(eventType);
		eventassocB.setEventCalType(calType);
		//eventassocB.setUser_id(userId);
		eventassocB.setStatCode(""+status1);
		
		eventassocB.setCost("0");
		eventassocB.setDisplacement("0");
		eventassocB.setStatusDate(currDate);
		eventassocB.setDurationUnit(durationUnitCode);
		eventassocB.setCalAssocTo(calAssoc);
		eventassocB.setBudgetTemplate(bgtTemplate);
		
		eventassocB.setPatSchUpdate(patSchUpdateStatus);//For PCAL-22446 Enhancement By Parminder Singh
	}


	if (mode.equals("M")){

	   eventId = EJBUtil.stringToNum(request.getParameter("protocolId"));

	   if (calledFrom.equals("P") || calledFrom.equals("L"))  {
		   eventdefB.setModifiedBy(usr);
		   eventdefB.setIpAdd(ipAdd);
		   eventdefB.setEvent_id(eventId);

		   eventdefB.setCalSharedWith(sharedWith);
		   eventdefB.setDurationUnit(durationUnitCode); //SV, 9/14/04

		   ret1 = eventdefB.updateEventdef();
		   if (ret1 == 0) {
			   if (durReduced.equals("1")) {
//SV, REDTAG, do we need to pass durUnit into the following method as well?
			   	ret = eventdefB.DeleteProtocolEventsPastDuration(String.valueOf(eventId), durationNum.intValue());
			   }
			fkObj=request.getParameter("protocolId");
			objShareB.setIpAdd(ipAdd);
	         	objShareB.setCreator(usr);
	         	objShareB.setObjNumber("3");//number of module
	         	objShareB.setFkObj(fkObj);// formId, CalId, reportId
	         	objShareB.setObjSharedId(sharedWithIds);// shared ids
	         	objShareB.setObjSharedType(sharedWith);// U-user id,A -account id,S - study id,G - group id,O - organization id
				System.out.println("sharedWith="+sharedWith+" sharedWithIds="+sharedWithIds);
	         	objShareB.setRecordType(mode);
	         	objShareB.setModifiedBy(usr);
	         	ret = objShareB.setObjectShareDetails();
//			    System.out.println("sonika ret " + ret);

		}

   	   } else if (calledFrom.equals("S")) { //called from Study Protocol
		   eventassocB.setEvent_id(eventId);

		   eventassocB.setModifiedBy(usr);
		   eventassocB.setCreator(userId);/*Bug# 10139 : Date 25 June 2012 : Yogendra Pratap */
		   eventassocB.setIpAdd(ipAdd);
		   ret1 = eventassocB.updateEventAssoc();
		   
		//JM: 22May2008, added for, issue #3492
			if(oldStatus.equals("O") && status.equals("A") && ( !checkedVal.equals("0") && !checkedVal.equals("") )){
         		eventassocB.updatePatientSchedulesNow(EJBUtil.stringToNum(calendarId),EJBUtil.stringToNum(checkedVal), EJBUtil.stringToNum(selStat), dtVal, EJBUtil.stringToNum(userId), ipAdd);
        	}

		   //JM: 24Apr2008, added for, Enh. #C9
			//if Calendar status is made Offline for editing then offlineFalg="1" else "0"
         	if(oldStat.equals("A") && status.equals("O")){
         	 offlineFalg="1";
         	 eventassocB.updateOfflnFlgForEventsVisits(eventId, offlineFalg);
         	}else if(oldStat.equals("O") && status.equals("A")){
         	 offlineFalg="0";
         	 eventassocB.updateOfflnFlgForEventsVisits(eventId, offlineFalg);
         	}else if(oldStat.equals("O") && status.equals("D")){
         	 offlineFalg="0";
         	 eventassocB.updateOfflnFlgForEventsVisits(eventId, offlineFalg);
         	}

		   if ((ret1 == 0) && durReduced.equals("1")) {
//SV, REDTAG, do we need to pass durUnit into the following method as well?
		   	ret = eventassocB.DeleteProtocolEventsPastDuration(String.valueOf(eventId), durationNum.intValue());
		   }

		    //JM: 22May2008 issue #3496
		    //if ( (! oldStat.equals(status)) && status.equals("A"))
	       	if ( (! oldStat.equals(status)) && status.equals("A") && !oldStatus.equals("O")){
	      	   		System.out.println("###### oldStat" +  oldStat + " ##status" + status);
    	   	   		ret1 = alnot.setDefStudyAlnot(chainId,request.getParameter("protocolId"),usr,ipAdd);
    	  	}

	   }

	   if (calAssoc.equals("P"))  {
		   if (ret1 == 0 && tempId>0) {
			   int studyId = EJBUtil.stringToNum(chainId);
			   String bgtName = name + " " +DateUtil.getCurrentDateTime();
			
			   int budgetPK =0;
			   budgetPK = eventassocB.createProtocolBudget(eventId,tempId,bgtName,EJBUtil.stringToNum(userId),ipAdd,studyId);
			   
			   if (budgetPK > 0) {
				   ArrayList bgtCalPKs = new ArrayList();
				   ArrayList bgtProtPKs = new ArrayList();
				   
				   bgtcaldao.getAllBgtCals(budgetPK, 0);
				   bgtCalPKs = bgtcaldao.getBgtcalIds();
				   bgtProtPKs = bgtcaldao.getBgtcalProtIds();
				   
				   int indx = bgtProtPKs.indexOf(EJBUtil.integerToString(new Integer(eventId)));
				   
				   int bgtCalPK =0;
				   bgtCalPK = EJBUtil.stringToNum((bgtCalPKs.get(indx).toString()));
				   ret1= bgtcaldao.addBudgetSection(eventId,bgtCalPK,"S",EJBUtil.stringToNum(userId),ipAdd,"C");
				   
				   if (ret1==1){
						ret1 = 0; 
					}else{
						ret1 = 0; 
					}
			   }
				
		   }
	   }
	   
	   if (ret1 == 0) {

		   msg = MC.M_Pcol_CalSvdSucc;/*msg = "Protocol Calendar saved successfully";*****/

	   } else {

		   msg = MC.M_Pcol_CalNotSvd;/*msg = "Protocol Calendar not saved";*****/

	   }

	} else { //mode=New
		if (calledFrom.equals("P") || calledFrom.equals("L"))  {
			eventdefB.setCreator(usr);
			eventdefB.setIpAdd(ipAdd);
			eventId = eventdefB.setEventdefDetails();
			ret1=eventId;
System.out.println("ret code in seteventdefDetails="+ret1);
			if (eventId > 0) {
				eventId = eventdefB.getEvent_id();

				eventdefB.setCalSharedWith(sharedWith);
				eventdefB.setDurationUnit(durationUnitCode);

				fkObj= Integer.toString(eventId);
				objShareB.setIpAdd(ipAdd);
				objShareB.setCreator(usr);
				objShareB.setObjNumber("3");//number of module
				objShareB.setFkObj(fkObj);// formId, CalId, reportId
				objShareB.setObjSharedId(sharedWithIds);// shared ids
				objShareB.setObjSharedType(sharedWith);// U-user id,A -account id,S - study id,G - group id,O - organization id
				objShareB.setRecordType(mode);
				objShareB.setModifiedBy(usr);
				ret = objShareB.setObjectShareDetails();
 System.out.println("sonika ret " + ret1);
			}
		} else if (calledFrom.equals("S")) { //called from Study Protocol
			eventassocB.setCreator(usr);
			eventassocB.setIpAdd(ipAdd);

			ret1 = eventassocB.setEventAssocDetails();
			eventassocB.setEventAssocDetails();
			eventId = eventassocB.getEvent_id();
		}
    }


}
	//changed by sonia to put protocol name in session
	tSession.putValue("protocolname", name);
	Object[] arguments = {StringUtil.htmlEncode(name)}; //fixed for bug#26958 by jp
if ((calledFrom.equals("P")|| calledFrom.equals("L") )&& (ret1 == -1) )  { %>
		<br><br><br><br><br><p class = "successfulmsg" align = center>
		<%=VelosResourceBundle.getMessageString("M_CalNameDupli_BackBtnToChgNew",arguments)%> <%-- Calender Name is Duplicate. Click on "Back" Button to change the name --%>
		<!-- <!%=MC.M_PcolLibName_Dupli%><%-- Protocol Library Name is duplicate. Click on "Back" Button to change the name*****--%> --> <Br><Br> 
		<button name="back" onclick="window.history.back();"><%=LC.L_Back%></button></p>
 		<%return;  }

/*YK - 15NOV2010 Changes for BUG # 4538  */
if ((calledFrom.equals("S"))&& (ret1 == -1) )  { 
%>
		<br><br><br><br><br><p class = "successfulmsg" align = center>
		<%=VelosResourceBundle.getMessageString("M_CalNameDupli_BackBtnToChgNew",arguments)%> <%-- Calender Name is Duplicate. Click on "Back" Button to change the name --%>
		<!-- <!%=MC.M_CalNameDupli_BackBtnToChg%><%-- Calender Name is duplicate. Click on "Back" Button to change the name*****--%> --> <Br><Br>
		<button name="back" onclick="window.history.back();"><%=LC.L_Back%></button></p>
 		<%return;  }

	if (mode.equals("N")) {
%>
 <META HTTP-EQUIV=Refresh CONTENT="0; URL=eventbrowser.jsp?mode=<%=mode%>&srcmenu=<%=src%>&selectedTab=2&duration=<%=duration%>&protocolId=<%=eventId%>&calledFrom=<%=calledFrom%>&calStatus=<%=status%>&calassoc=<%=calAssoc%>">
 
<br>

<br>

<br>

<br>

<br>

<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%> </p>
<%
	} else if (mode.equals("M")) { //Modify mode
%>
<br><br><br><br><br>
<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%> </p>
<!--
//JM: 23Nov2006
<META HTTP-EQUIV=Refresh CONTENT="0; URL=displayDOW.jsp?selectedTab=4&mode=<%=mode%>&duration=<%=duration%>&calledFrom=<%=calledFrom%>&protocolId=<%=eventId%>&srcmenu=<%=src%>&calStatus=<%=status%>&pageNo=1&displayType=D&headingNo=1&displayDur=3&pageRight=<%=pageRight%>&calassoc=<%=calAssoc%>">  -->
  <META HTTP-EQUIV=Refresh CONTENT="0; URL=protocolcalendar.jsp?selectedTab=1&mode=<%=mode%>&duration=<%=duration%>&calledFrom=<%=calledFrom%>&protocolId=<%=eventId%>&srcmenu=<%=src%>&calStatus=<%=status%>&pageNo=1&displayType=D&headingNo=1&displayDur=3&pageRight=<%=pageRight%>&calassoc=<%=calAssoc%>">

<%		}

} //end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>



</BODY>

</HTML>





