<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_Pat_Home%><%--<%=LC.Pat_Patient%> Home*****--%></title>
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true">
	<jsp:param name="isPPortal" value="Y"/>
</jsp:include>

<BODY leftmargin="15%"  scroll="auto">


<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>




<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>


<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.web.patLogin.PatLoginJB"%>

<%
String  portalId = "";
	String patientId = "";

	PatLoginJB patLoginB = new PatLoginJB();

	HttpSession tSession = request.getSession(true);
	String uName ="";
if (tSession != null)
	{
	uName = (String) tSession.getValue("pp_patientName") + " (" + (String) tSession.getValue("pp_patient_code") + ")";


	}

	if (sessionmaint.isValidSession(tSession))
	{

	 //uName = (String) tSession.getValue("pp_patientName");
	 patLoginB = (PatLoginJB) tSession.getAttribute("pp_currentLogin");

	int ienet = 2;
	if (patLoginB != null)
	{
		portalId = patLoginB.getFkPortal();
		patientId = patLoginB.getPlId();
	}
	String agent1 = request.getHeader("USER-AGENT");

   	if (agent1 != null && agent1.indexOf("MSIE") != -1)
    	ienet = 0; //IE
    else
		ienet = 1;


	String accId = (String) tSession.getValue("pp_accountId");
	%>
	<table width="100%" border=0>
	<tr style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; font-style: normal; line-height: normal; font-weight: normal; font-variant: normal; text-transform: none;">
	<td>
		<b><%=LC.L_Welcome%><%--Welcome*****--%> <%=uName%> !</b>
		</td>
		<td align="right" >
			<A href="patientlogout.jsp" ><%=LC.L_Logout%><%--Logout*****--%></A>

		</td>
	<tr>
	</table>



	<% if (patLoginB != null)
	{%>
	<jsp:include page="previewPortal.jsp" flush="true">
		<jsp:param name="portalId" value="<%=portalId%>"/>
		<jsp:param name="patientId" value="<%=patientId%>"/>
		<jsp:param name="isPPortal" value="Y"/>
	</jsp:include>

	<%
	}
	} //end of body for session
	else
	{
	%>
		<jsp:include page="timeout.html" flush="true"/>
	<%
	}
	%>





</BODY>

</HTML>