<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%--Assign Users to a Group >> User Search*****--%> <%=MC.M_AssignUsr_UsrSrch%></title>

<%@page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.business.common.*" %>
<%@page import="com.velos.eres.service.util.*"%>


<meta http-equiv="Content-Type" content="text/html; charset=">

</head>
<%!
private boolean isAccessibleFor(int pageRight, char type) {
    switch(type) {
    case 'V': return ((pageRight & 0x0004) >> 2) == 1;
    case 'E': return ((pageRight & 0x0002) >> 1) == 1;
    case 'N': return  (pageRight & 0x0001) == 1;
    }
    return false;
}
%>
<SCRIPT Language="javascript">

var screenWidth = screen.width;
var screenHeight = screen.height;

function checkUser(formobj)
{

 if (!(validate_col('e-Signature',formobj.eSign))) return false

   <%--  if(isNaN(formobj.eSign.value) == true) {

	/* alert("Incorrect e-Signature. Please enter again");*****/
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");

	formobj.eSign.focus();

	return false;

   }	--%>



totrows = formobj.totalrows.value;

	if (totrows < 1)

	{

<%-- alert("Please select at least one user");*****--%>
	alert("<%=MC.M_SelAtLeast_OneUsr%>");
		return false;

	}

	return true;

}

	

	function changeCount(formobj,row)

	{

	  selrow = row ;
	  totrows =formobj.totalrows.value;
	  totusers = formobj.lusers.value;
	  rows = parseInt(totrows);	
	  usernum = parseInt(totusers);		
	if (usernum > 1)

	{

	  if (formobj.assign[selrow].checked)

		{ 

			rows = rows + 1;

		}

	  else

		{

		   rows = rows - 1;

		}

	}else{

	  if (formobj.assign.checked)

		{ 

			rows = rows + 1;

		}

	  else

		{

		  rows = rows - 1;

		}

	}	

	formobj.totalrows.value = rows;

	}


</SCRIPT>



<% String src;

src= request.getParameter("srcmenu");

%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   

<body>

<jsp:useBean id="groupB" scope="session" class="com.velos.eres.web.group.GroupJB"/>
<jsp:useBean id="userB" scope="session" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id="teamB" scope="page" class="com.velos.eres.web.team.TeamJB"/>
<jsp:useBean id ="sessionmaint" scope="page" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import = "com.velos.eres.business.team.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%>

<!-- Bug#16116 Fix for 1360*768 resolution- Tarun Kumar -->
<SCRIPT LANGUAGE="JavaScript">
	if(screenWidth>1280 || screenHeight>1024)
		document.write('<DIV class="browserDefault" id="div1" style="height: 90%;"> ')
	else
		document.write('<DIV class="browserDefault" id="div1" style="height: 93%;"> ')
</SCRIPT>


<%HttpSession tSession = request.getSession(true);

  if (sessionmaint.isValidSession(tSession))

{
	  int pageRights=0;
	  GrpRightsJB grpRights = (GrpRightsJB)tSession.getAttribute("GRights");
	  if (grpRights == null) { return; }
	  pageRights=Integer.parseInt(grpRights.getFtrRightsByValue("ASSIGNUSERS"));

	String mode = request.getParameter("mode");	
	String accId = (String) tSession.getValue("accountId");
	int grpId = EJBUtil.stringToNum(request.getParameter("grpId"));	
	String grpName = null;
	groupB.setGroupId(grpId);
	groupB.getGroupDetails();
	grpName  = groupB.getGroupName();	 
	
	
		String userJobType = "";
		String dJobType = "";
		String dAccSites ="";
		String jobTyp="";
		String orgName="";
		
		String orgNam = "";
		UserDao userDao = new UserDao();
		
		CodeDao cd = new CodeDao();
		CodeDao cd2 = new CodeDao();
		cd2.getAccountSites(EJBUtil.stringToNum(accId)); 
    	cd.getCodeValues("job_type");
		userJobType =userB.getUserCodelstJobtype();
		//dJobType = cd.toPullDown("jobType", EJBUtil.stringToNum(userJobType));
		jobTyp=request.getParameter("jobType") ;
		orgName=request.getParameter("accsites") ;
		if(jobTyp==null)
			jobTyp = "";
		
		if(orgName == null)
			orgName = "";
		if (jobTyp.equals(""))			
		  dJobType=cd.toPullDown("jobType");
		 
		else
		  dJobType=cd.toPullDown("jobType",EJBUtil.stringToNum(jobTyp),true);

		if (orgName.equals(""))
		   dAccSites=cd2.toPullDown("accsites");
		  

		else
		  dAccSites=cd2.toPullDown("accsites",EJBUtil.stringToNum(orgName),true);

	%>



	<P class="comments"> <b><%-- Assign Users to Group*****--%><%=MC.M_Assign_UsersToGrp%>: <%=grpName %></b></P>

<%

	String usrLastName = "";
	String usrFirstName = "";
	String ufname="";
	String ulname="";
	
	
	if(!mode.equals("N")){
	
	ufname= request.getParameter("fname") ;
	ulname=request.getParameter("lname") ;
	
	
	}
	
	%>

	<Form  name="searchMod1" method="post"  action="assignuserstogroupusrsearch.jsp?srcmenu=<%=src%>&mode=M">
    <table width="98%" cellspacing="0" cellpadding="0" class="basetbl" >
		<tr height="5"><td></td></tr>
      <tr height="20" bgcolor="#dcdcdc">
		<td colspan="4"><b><%-- Search By*****--%>            <%=LC.L_Search_By%></b></td>
	</tr>
	<tr bgcolor="#dcdcdc">
	
 	 	<td class=tdDefault width=18%> <%--First Name*****--%>  <%=LC.L_First_Name%> : <Input type=text name="fname" size="10" Value="<%=ufname%>"></td>
        <td class=tdDefault width=18%><%-- Last Name*****--%>  <%=LC.L_Last_Name%> : <Input type=text name="lname" size="10" Value="<%=ulname%>"></td>
		<td class=tdDefault width=25%> <%--Job Type*****--%> <%=LC.L_Job_Type%>  : <%=dJobType%></td>
		<td class=tdDefault width=40%> <%--Organization*****--%>  <%=LC.L_Organization%> : <%=dAccSites%> </td>
		</tr>
	<tr bgcolor="#dcdcdc">
        <td class=tdDefault colspan="4" align="right"><button type="submit"><%=LC.L_Search%></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		</td>

     </tr>         

	</table>	
		<input type="hidden" name="grpId" Value="<%=grpId%>">	
   		<Input type="hidden" name= "grpName" value=" <%=grpName%>" >	
	</Form>

		
	<Form  name="searchMod" id="srchModFrm" method="post" action="updategrpusers.jsp" onSubmit = "if (checkUser(document.searchMod)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

	<%

		 accId = (String) tSession.getValue("accountId");
		//UserDao userDao = new UserDao();
		userDao.searchGroupUsers(EJBUtil.stringToNum(accId),ulname.trim(),ufname.trim(),jobTyp,orgName,grpId);	
		String usrMidName = "";
		String usrId = "";
		ArrayList usrIds = null;
		ArrayList usrLastNames=null;
		ArrayList usrFirstNames=null;
		ArrayList usrMidNames=null;
		ArrayList userSiteNames = null;
		usrLastNames = userDao.getUsrLastNames();
		usrFirstNames = userDao.getUsrFirstNames();
		userSiteNames = userDao.getUsrSiteNames();
		usrIds = userDao.getUsrIds();
		
	%>	

	<table width="450" border="0" cellpadding="0" cellspacing="0" class="basetbl">

      <tr> </tr>

      <%

    int i;

	int lenUsers = usrLastNames.size();

	%>

      <tr id ="browserBoldRow"> <%Object[] arguments1 = {lenUsers}; %>

        <td width = 400><%-- Total Number of Users*****--%> <%=MC.M_Tot_NumOfUsers%>: <%=VelosResourceBundle.getLabelString("L_Usr_S",arguments1)%><%-- <%= lenUsers%> User(s)*****--%>  </td>

      </tr>

      <tr> 

        <th> <%--Available Users *****--%><%=LC.L_Available_Users%></th>
		<th> <%--Organization *****--%><%=LC.L_Organization%></th>
        <th> <%--Select *****--%><%=LC.L_Select%></th>

      </tr>

      <%

      for(i = 0 ; i < lenUsers ; i++)

      {
	usrLastName=((usrLastNames.get(i)) == null)?"-":(usrLastNames.get(i)).toString();
	usrFirstName=((usrFirstNames.get(i))==null)?"-":(usrFirstNames.get(i)).toString();
	usrId = ((Integer)usrIds.get(i)).toString();
	
	orgNam = (String)userSiteNames.get(i);
	
	if (orgNam == null){
	orgNam ="-";
	}
			//String usr_id = usrIds.get(i).toString();
			//userDao = userB.getUsersDetails(usr_id);
			//ArrayList organizationNames = userDao.getUsrSiteNames();
			//orgNam = ((organizationNames.get(0)) ==null)?"-":(organizationNames.get(0)).toString();
	

	if ((i%2)==0) {

  	%>

      <tr class="browserEvenRow"> 

        <%

        }

        else{

  	%>

      <tr class="browserOddRow"> 

        <%

        }

  %>
   <td width =200> <%= usrFirstName%>&nbsp;<%= usrLastName%> </td>
   <td width =200> <%=orgNam%> </td>
  <td width =50> 
  <Input type="checkbox" name="assign" value = "<%=usrId %>" onclick="changeCount(document.searchMod,<%=i%>)"  >
  </td>
  </tr>
  <%

  }

%>

<Input type="hidden" name = "lusers" value= <%=lenUsers%> >
</table>
<%
if (lenUsers == 0 )
{
%>
	<P class="defComments"><%--No Results Found*****--%> <%=LC.L_NoRes_Found%></P>

	<%

}

else{
if (isAccessibleFor(pageRights, 'N')){
%>

<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="srchModFrm"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>


  <%
}
//}
//	}

}

%>

	<input type="hidden" name="mode" Value="<%=mode%>">
	<input type="hidden" name="grpId" Value="<%=grpId%>">	
	<Input type="hidden" name="totalrows" value=0  >
    <Input type="hidden" name= "grpName" value= <%=grpName%> >
	 <Input type="hidden" name= "src" value= <%=src%> >

<%-- The bracket below is for the session.--%>	



<%

}

%>


<table><tr><td height="7">&nbsp;</td></tr></table>
</Form>

		

 <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>



</div>





<DIV class="mainMenu" id = "emenu"> 

  <jsp:include page="getmenu.jsp" flush="true"/>

</DIV>



</body>



</html>

