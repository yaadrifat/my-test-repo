<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.json.*"%>
<%@ page language = "java" import = "com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.esch.business.common.*,java.util.*"%>
<jsp:useBean id ="sessionmaint2" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studySiteB" scope="session" class="com.velos.eres.web.studySite.StudySiteJB"/>
<%
	HttpSession tSession2 = request.getSession(true);
	request.setCharacterEncoding("UTF-8");
	response.setContentType("application/json");
	JSONObject jsObj = new JSONObject();
	if (!sessionmaint2.isValidSession(tSession2)) {
		// Session is invalid; print an error and exit
		response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");
		jsObj.put("result", -1);
		jsObj.put("resultMsg", MC.M_UsrNot_LoggedIn);/*jsObj.put("resultMsg", "User is not logged in.");*****/
		out.println(jsObj.toString());
		return;
	}

   	if (sessionmaint2.isValidSession(tSession2)){
   		int formId = StringUtil.stringToNum((String) request.getParameter("formId"));
	
		FormCompareUtilityDao formCUDao = new FormCompareUtilityDao();
		formCUDao.setFormId(formId);
		formCUDao.getFormFieldMap();

		ArrayList arrColNames = formCUDao.getColNames();
		ArrayList arrColTypes = formCUDao.getColTypes();
		ArrayList arrColSysIds = formCUDao.getColSysIds();
		ArrayList arrColSecIds = formCUDao.getColSectionIds();
		ArrayList arrColUIds = formCUDao.getColUIds();
		
		JSONArray jsObjArray = new JSONArray();
		JSONObject jsObjColumn = new JSONObject();
		for (int iCol=0; iCol < arrColUIds.size(); iCol++){
			JSONObject jsObjColDetail = new JSONObject();

			String colUID = (String)arrColUIds.get(iCol);
			jsObjColDetail.put("colUID", colUID);
			jsObjColDetail.put("colName", arrColNames.get(iCol));
			jsObjColDetail.put("colType", arrColTypes.get(iCol));
			jsObjColDetail.put("colSysId", arrColSysIds.get(iCol));
			jsObjColDetail.put("colSecId", arrColSecIds.get(iCol));
			
			//jsObjColumn = new JSONObject();
			//jsObjColumn.put(colUID,jsObjColDetail);
			jsObjArray.put(jsObjColDetail);
		}
		/*jsObj.put("arrSiteIds", arrSiteIds);
		jsObj.put("arrSiteNames", arrSiteNames);
		jsObj.put("arrSiteSampleSizes", arrSiteSampleSizes);*/
		
		jsObj.put("arrColumns", jsObjArray);
		
		out.println(jsObj.toString());

	} %>