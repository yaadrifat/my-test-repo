<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<title><%=LC.L_Organization%><%-- Organization*****--%></title>
<head>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<%-- Nicholas : Start --%>
	<%--<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>--%>
	<%-- Nicholas : End --%>
	
	<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.business.common.UserSiteDao,com.velos.eres.service.util.*"%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB"/>
	<%-- Nicholas : Start --%>
	<jsp:include page="skinChoser.jsp" flush="true"/>
	<Link Rel=STYLESHEET HREF='./styles/velos_popup.css' type=text/css>
	<%-- Nicholas : End --%>
<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1) 
   	ienet = 0; //IE
else
	ienet = 1;

if (ienet == 0) {
%>
<body style="overflow:scroll">
<%} else {%>
<body>
<%}%>


<DIV class="popDefault" > 

<P class="sectionHeadings"> <%=LC.L_FmrLib_Org%><%-- Form Library >> Organization*****--%> </P>
<%
	HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession))	
{
 %>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<% 	
	
	ArrayList arrIds=null;
	ArrayList arrFirstNames=null;
	ArrayList arrLastNames=null;
	ArrayList arrSiteNames=null;
	String orgIds=request.getParameter("orgIds");
	
	String usrLastName="";
	String usrFirstName="";
	String siteName="";
	 
	UserDao usrDao=new UserDao();
	usrDao.getOrganizationUsers(orgIds);
	arrIds=usrDao.getUsrIds();
	arrLastNames=usrDao.getUsrLastNames();
	arrFirstNames=usrDao.getUsrFirstNames();
	arrSiteNames = usrDao.getUsrSiteNames();
	int rows = usrDao.getCRows();


	if(rows==0)
	{%>
		<P class="defComments"><%=MC.M_NoRecordsFound%><%-- No records found*****--%></P>
	
	<%}
else{%>
	<table width="95%" cellspacing="0" cellpadding="0" border="0" >
    <tr class="browserOddRow"> 
    <th  width="25%"> <%=LC.L_Organization%><%-- Organization*****--%> </th>
    <th   width="25%"> <%=LC.L_User_Name%><%-- User Name*****--%> </th>

    </tr>
<%
	
	for(int i = 0 ; i < rows ; i++)
	{
		
		siteName= arrSiteNames.get(i).toString();
		
		if ( i > 0)
		{
			if ( siteName.equals(arrSiteNames.get(i-1).toString() ) )
			{
				siteName="";
			}
		}

		usrLastName=((arrLastNames.get(i)) == null)?"-":(arrLastNames.get(i)).toString();
			
		usrFirstName=((arrFirstNames.get(i))==null)?"-":(arrFirstNames.get(i)).toString();
		
		if ((i%2)==0) 
		{

%>
      <tr class="browserEvenRow"> 
<%
		}

	   else
	   {
%>
      <tr class="browserOddRow"> 
<%

	   }
%>
	<td ><b><%= siteName%><b></td>

	<td >	<%=usrFirstName%>&nbsp;<%=usrLastName%></td>
	  </tr>
<%
		}//end of for statement
}//end of else
%>	</table>
<%
}//end of if body for session
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
</div>
</body>

</html>

