<!-- 
	File Name: adminsettings.jsp

	Author : Gopu
	
	Created Date	: 07/28/2006

	Last Modified Date : 

	Purpose	: Default account level settings for User / Patient Timezone, Seesion Timeout time, Number of days password will expire, Number of days e-Sign will expire
	 
-->
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%-- Admin Settings*****--%><%=LC.L_Admin_Settings%></title>

<%@page import="com.velos.eres.service.util.MC"%>
<%@page import="com.velos.eres.service.util.LC"%>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT>
	function validate(formobj){
	
		if(formobj.uzone.checked)
		   formobj.uzonetxt.value=1;
		else
		   formobj.uzonetxt.value=0;

		if(formobj.pzone.checked)
		   formobj.pzonetxt.value=1;
		else
		   formobj.pzonetxt.value=0;

		if(formobj.ltime.checked)
		  formobj.ltimetxt.value=1;
		else
		  formobj.ltimetxt.value=0;

		if(formobj.pwdcheck.checked)
		   formobj.pwdchecktxt.value=1;
		else
		   formobj.pwdchecktxt.value=0;

		if(formobj.echeck.checked)
		  formobj.echecktxt.value=1;
		else
		  formobj.echecktxt.value=0;

		// Session Timeout Validation
		formobj.logouttime.value = fnTrimSpaces(formobj.logouttime.value);
		if(isNaN(formobj.logouttime.value) == true)	{
			/* alert("Please enter a valid number");*****/
			alert("<%=MC.M_Etr_ValidNum%>");
			formobj.logouttime.focus();
			return false;
		}
		//Modified by Manimaran to fix the Bug2717  
		if ((parseInt(formobj.logouttime.value) < 10 ) || (parseInt(formobj.logouttime.value) > 300 )) {
		 	/* alert("NOT less than 10 minutes and NOT more than 300 minutes");*****/
			     alert("<%=MC.M_NotLess10_NotMore300%>");
		    formobj.logouttime.focus();
		    return false; 
		}
		
		// Password Expires validation
		formobj.pwdexpires.value = fnTrimSpaces(formobj.pwdexpires.value);
		if (parseInt(formobj.pwdexpires.value) < 1 ){
			/* alert("Password Expires can not be less than 1 day");*****/
					alert("<%=MC.M_PwdExp_Less1Day%>");
			formobj.pwdexpires.focus();
			return false;
		}
		if(isNaN(formobj.pwdexpires.value)==true){
			/* alert("Password expires has to be a number. Please enter valid number");*****/ 
			alert("<%=MC.M_PwdExpires_EtrValidNum%>");
			formobj.pwdexpires.focus();
			return false;
		}
		

		//eSignature expires
		formobj.esignexpires.value = fnTrimSpaces(formobj.esignexpires.value);
		if (parseInt(formobj.esignexpires.value) < 1 ){
//			alert("e-Signature Expires can not be less than Zero day");
			/*  alert("e-Signature Expires can not be less than 1 day");*****/ 
			alert("<%=MC.M_EsignExp_CntLess1Day%>");
			formobj.esignexpires.focus();
			return false;
		}
		if(isNaN(formobj.esignexpires.value)==true){
	/* alert("e-Signature expires has to be a number. Please enter valid number");*****/
			alert("<%=MC.M_EsignExpires_ValidNum%>");
			formobj.esignexpires.focus();
			return false;
		}
		if (!(validate_col('e-Sign',formobj.eSign))) return false;
	<%--	if(isNaN(formobj.eSign.value) == true) {
				/* alert("Incorrect e-Signature. Please enter again");*****/
			alert("<%=MC.M_IncorrEsign_EtrAgain%>");
			formobj.eSign.focus();
			return false;
		} --%>
	}
	</SCRIPT>
	<%@ page import="java.util.*,java.io.*" %>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<jsp:useBean id="commonB" scope="request" class="com.velos.eres.web.common.CommonJB"/>
	<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
	<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.DOMUtil,com.velos.eres.service.util.LC"%>
</head>
<body>

<%
HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession))
{
%>
	<jsp:include page="sessionlogging.jsp" flush="true"/>
	<jsp:include page="include.jsp" flush="true"/>
	<%
	String accId=(String)tSession.getValue("accountId");
	String uTimeZone ="";
	String pTimeZone ="";
	String logouttime="";
	String pwdexpires="";
	String esignexpires="";
	String utimezone="";
	String ptimezone="";
	int uzone=0;
	int pzone=0;
	int ltime=0;
	int pwdcheck=0;
	int echeck=0;
	String pageRight = request.getParameter("pageRight");
	if (EJBUtil.stringToNum(pageRight) >= 4){//KM
		CodeDao codeDao = new CodeDao();
		codeDao.getTimeZones();
		SettingsDao settingsDao=commonB.getSettingsInstance();
		int modname=1;
		settingsDao.retrieveSettings(EJBUtil.stringToNum(accId),modname);
		ArrayList keywords=settingsDao.getSettingKeyword();
		ArrayList setvalues=settingsDao.getSettingValue();
		HashMap keywordHash = new HashMap();
		for(int i=0;i<keywords.size();i++){	keywordHash.put(keywords.get(i).toString(),(setvalues.get(i)==null)?"":setvalues.get(i).toString()) ;
		}
		String tempStr=""; 
		if (!(keywordHash.isEmpty())) {
			utimezone = String.valueOf(keywordHash.get("ACC_USER_TZ"));
			tempStr = String.valueOf(keywordHash.get("ACC_USER_TZ_OVERRIDE"));
			uzone = (tempStr.equals(""))?0: Integer.parseInt(tempStr);
			ptimezone = String.valueOf(keywordHash.get("ACC_PAT_TZ"));
			tempStr = String.valueOf(keywordHash.get("ACC_PAT_TZ_OVERRIDE"));
			pzone = (tempStr.equals(""))?0: Integer.parseInt(tempStr);
			logouttime = String.valueOf(keywordHash.get("ACC_SESSION_TIMEOUT"));
			tempStr = String.valueOf(keywordHash.get("ACC_SESSION_TIMEOUT_OVERRIDE"));
			ltime = (tempStr.equals(""))?0: Integer.parseInt(tempStr);
		    pwdexpires = String.valueOf(keywordHash.get("ACC_DAYS_PWD_EXP"));
			tempStr = String.valueOf(keywordHash.get("ACC_DAYS_PWD_EXP_OVERRIDE"));
			pwdcheck = (tempStr.equals(""))?0: Integer.parseInt(tempStr);
			esignexpires = String.valueOf(keywordHash.get("ACC_DAYS_ESIGN_EXP"));
			tempStr = String.valueOf(keywordHash.get("ACC_DAYS_ESIGN_EXP_OVERRIDE"));
			echeck = (tempStr.equals(""))?0: Integer.parseInt(tempStr);
		}
		if(utimezone.equals(""))
		    uTimeZone = codeDao.toPullDown("utimeZone");
		else
			// by default timezone settings   
			uTimeZone = codeDao.toPullDown("utimeZone", EJBUtil.stringToNum(utimezone));
		if(ptimezone.equals(""))
		   pTimeZone = codeDao.toPullDown("ptimeZone");
		else
			// By defualt timezone settings			   
			pTimeZone = codeDao.toPullDown("ptimeZone", EJBUtil.stringToNum(ptimezone));
		String checkstr="";
	%>
	<Form name = "adminsettings" id="adminsettingsfrm" method="post" action="updateadminsettings.jsp" onsubmit = "if (validate(document.adminsettings)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

	<div class="popDefault">
		<table width="99%" cellspacing="0" cellpadding="0" border="0"  >
			<tr align="left">
				<th><P class = "sectionHeadings"> <%-- Admin Settings*****--%>
               <%=LC.L_Admin_Settings%></P></th>
			</tr>
			<tr align="left">
				<td><p class="defComments"> <%--You can define the following default settings for your account users. &nbsp; Check the 'Can be overridden' checkbox if you want to allow your users to change these settings in the respective modules.*****--%><%=MC.M_DefineAcc_CboxModule%></p> </td>
			</tr>
			<tr align="left">
				<td><p class="defComments"><%--  If you do not wish to define any setting, application defaults will be used*****--%><%=MC.M_IfNoSettDefine_DefUsed%>
				</p></td>
			</tr>
		</table>
			<div class="tmpHeight"></div>
		<table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl midalign">
			<tr>
				<th width="37%" align="left"><%-- Admin Settings*****--%>
               <%=LC.L_Admin_Settings%> </th>
				<th width="10%">&nbsp;</th>
				<th width="20%">&nbsp;</th>
				<th width="33%" align="center"><%-- Can be overridden?*****--%>
               <%=LC.L_Can_BeOverridden%> </th>
			</tr>
			<tr><td colspan="4">&nbsp;</td></tr>
			<tr>
				<td> <%-- Default User Time Zone*****--%> <%=MC.M_Def_UsrTZone%></td>
				<td colspan="2"> <%=uTimeZone%> </td>
				<td align="center"><input type="checkbox" name="uzone" <%=((uzone==1)?"checked":"") %>>
				</td>
			</tr>
			<tr><td colspan="4">&nbsp;</td></tr>
			<tr>
				<td><%-- Default <%=LC.Pat_Patient%> Time Zone*****--%>  
				 <%=MC.M_DefPat_TimeZone%> </td>
				<td colspan="2"> <%=pTimeZone%> </td>
				<td align="center"><input type="checkbox" name="pzone" <%=((pzone==1)?"checked":"") %>>
				</td>
			</tr>
			<tr><td colspan="4">&nbsp;</td></tr>
			<tr>
				<td colspan="2"> <%--Default Automatic Logout Time*****--%><%=MC.M_Def_AutoLoutTime%></td>
				<td><input type="text" name="logouttime" value="<%=logouttime%>" maxlength="3">(<%--Less than 300 minutes*****--%><%=MC.M_LessThan300_Min%>)
				</td>
				<td align="center">
					<input type="checkbox" name="ltime" <%=((ltime==1)?"checked":"") %>>
				</td>
			</tr>
			<tr><td colspan="4">&nbsp;</td></tr>
			<tr>
				<td colspan="2"> <%--Default Number of days after which the password expires. *****--%> <%=MC.M_DaysAfter_PwdExpires%> </td>
				<td><input type="text" name="pwdexpires" value="<%=pwdexpires%>" maxlength="3"></td>
				<td align="center">
					<input type="checkbox" name="pwdcheck" <%=((pwdcheck==1)?"checked":"") %>>
				</td>
			</tr>
			<tr><td colspan="4">&nbsp;</td></tr>
			<tr>
				<td colspan="2"> <%--Default Number of days after which the e-Signature expires.*****--%> <%=MC.M_DaysAfter_EsignExpires%> </td>
				<td><input type="text" name="esignexpires" value="<%=esignexpires%>" maxlength="3"></td>
				<td align="center">
					<input type="checkbox" name="echeck" <%=((echeck==1)?"checked":"") %>>
				</td>
			</tr>
			<tr><td colspan="4">&nbsp;</td></tr>
		</table>



		 <% 
		   String showSubmit = ""; 	 
		   if (!(EJBUtil.stringToNum(pageRight) > 5)) {
			   	showSubmit = "N";
			}
		 %>

		 <jsp:include page="submitBar.jsp" flush="true"> 
				<jsp:param name="displayESign" value="Y"/>
				<jsp:param name="formID" value="adminsettingsfrm"/>
				<jsp:param name="showDiscard" value="N"/>
				<jsp:param name="showSubmit" value="<%=showSubmit%>"/>
		</jsp:include>

			
		
		<input type="hidden" name="uzonetxt">
		<input type="hidden" name="pzonetxt">
		<input type="hidden" name="ltimetxt">
		<input type="hidden" name="pwdchecktxt">
		<input type="hidden" name="echecktxt">
	</div>
	</form>
	<%
	} //end of if body for page right
	else {
	%>
		<jsp:include page="accessdenied.jsp" flush="true"/>
	<%
	} //end of else body for page right

}//end of if body for session
else{
%>
  <jsp:include page="timeout.html" flush="true"/>
<%
}
%>

<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>

</body>
</html>
