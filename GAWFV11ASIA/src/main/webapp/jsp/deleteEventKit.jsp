<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<HEAD>
<title><%=MC.M_Del_EvtStorKit%><%--Delete Event Storage kit*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<%
String fromPage = request.getParameter("fromPage");

 %>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>


<SCRIPT>
function  validate(formobj){

	if (!(validate_col('e-Signature',formobj.eSign))) return false

<%-- 	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   } --%>
}
</SCRIPT>
<jsp:include page="skinChoser.jsp" flush="true"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="eventkitB" scope="request" class="com.velos.esch.web.eventKit.EventKitJB"/>

<%@ page language = "java" import = "com.velos.esch.service.util.EJBUtil,com.velos.esch.business.common.*"%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.aithent.audittrail.reports.AuditUtils,com.velos.eres.service.util.*" %>

 <jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>

<jsp:include page="include.jsp" flush="true"/>

<body>
<br>


 <DIV class="formDefaultpopup" id="div1">


<%

String protocolId = request.getParameter("protocolId");
String calledFrom = request.getParameter("calledFrom");
String eventId = request.getParameter("eventId");
String delMode=request.getParameter("delMode");
String eventkitpk =request.getParameter("eventkitpk");
//String eventName = request.getParameter("eventName");
String eventName ="";//KM

String propagateInVisitFlag = request.getParameter("propagateInVisitFlag");
String propagateInEventFlag = request.getParameter("propagateInEventFlag");
String eSign = request.getParameter("eSign");


if (calledFrom.equals("P")||calledFrom.equals("L"))
{
	  eventdefB.setEvent_id(EJBUtil.stringToNum(eventId));
	  eventdefB.getEventdefDetails();
	  eventName = eventdefB.getName();
//JM: 12Nov2009: #4399
  	  eventName = (eventName==null)?"":eventName;
  	  if (eventName.length()>1000){//for the old existing event names > 1000 or larger
  		eventName=eventName.substring(0,1000);
  	  }
}else{
	  eventassocB.setEvent_id(EJBUtil.stringToNum(eventId));
	  eventassocB.getEventAssocDetails();
	  eventName = eventassocB.getName();
//JM: 12Nov2009: #4399
  	  eventName = (eventName==null)?"":eventName;
  	  if (eventName.length()>1000){//for the old existing event names > 1000 or larger
  		eventName=eventName.substring(0,1000);
  	  }
 }




HttpSession tSession = request.getSession(true); %>

<%
if (sessionmaint.isValidSession(tSession)) {
	String oldESign = (String) tSession.getValue("eSign");
/////////////////

	if(!oldESign.equals(eSign)&& delMode.equals("final")) {
	%>
	<jsp:include page="incorrectesign.jsp" flush="true"/>
	<%
	}else {



		if (delMode.equals("initial" )) {

			delMode="final";
		%>


		<FORM name="deleteFile" id="delEvtKit" method="post" action="deleteEventKit.jsp" onSubmit="if (validate(document.deleteFile)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
		<br><br>


		<jsp:include page="propagateEventUpdate.jsp" flush="true">

		<jsp:param name="fromPage" value="<%=fromPage%>"/>

		<jsp:param name="formName" value="deleteFile"/>

		<jsp:param name="eventName" value="<%=eventName%>"/>

		</jsp:include>

		<P class="defComments"><%=MC.M_Etr_Esign_ToProc%><%--Please enter e-Signature to proceed .*****--%></P>
		<jsp:include page="submitBar.jsp" flush="true">
				<jsp:param name="displayESign" value="Y"/>
				<jsp:param name="formID" value="delEvtKit"/>
				<jsp:param name="showDiscard" value="N"/>
		</jsp:include>

		 <input type="hidden" name="delMode" value="<%=delMode%>">
		 <input type="hidden" name="protocolId" value="<%=protocolId%>">
		 <input type="hidden" name="calledFrom" value="<%=calledFrom%>">
		 <input type="hidden" name="eventId" value="<%=eventId%>">

		<input type="hidden" name="fromPage" value="<%=fromPage%>">
		 <input type="hidden" name="eventkitpk" value="<%=eventkitpk%>">
		 <input type="hidden" name="eventName" value="<%=eventName%>">

		</FORM>
	<%
		} else {


		if ((propagateInVisitFlag == null) || (propagateInVisitFlag.equals("")))

				propagateInVisitFlag = "N";

			if ((propagateInEventFlag == null) || (propagateInEventFlag.equals("")))

			propagateInEventFlag = "N";

			 if ( (propagateInVisitFlag.equals("Y")) || (propagateInEventFlag.equals("Y")))
				 {

				EventdefDao eventdefdao = new EventdefDao();



				eventdefdao.propagateEventUpdates(EJBUtil.stringToNum(protocolId), EJBUtil.stringToNum(eventId), "SCH_EVENT_KIT", EJBUtil.stringToNum(eventkitpk), propagateInVisitFlag, propagateInEventFlag, "D", calledFrom);


				 }else
				 {
					//Modified for INF-18183 ::: Ankit
					String moduleName = null;
					if (calledFrom.equals("L")) {
				    	moduleName = LC.L_Evt_Lib; /*Event Library*/
				    }else if (calledFrom.equals("P")) {
				    	moduleName = LC.L_Library_Calendar; /*Library Calendar*/
				    }else {
				    	moduleName = LC.L_Study_Calendar; /*Study Calendar*/
				    }
					eventkitB.deleteEventKitRecord(EJBUtil.stringToNum(eventkitpk), AuditUtils.createArgs(session,"",moduleName));
				}

		%>

		<br><br><br><br><br>
		<p class = "successfulmsg" align = center><%=MC.M_Data_DelSucc%><%--Data deleted successfully.*****--%></p>
			<script>

				  window.opener.location.reload();

				  setTimeout("self.close()",1000);

				 </script>


		<%


		}

			}


		}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}

 %>
  <div class = "myHomebottomPanel">
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>



</BODY>
</HTML>



