<%--PCAL-20461 For Updating Changes in Database for Events Sequence & Hide/Unhide Changes--%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONObject"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
 <%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.EJBUtil"%>
  <%

	HttpSession tSession = request.getSession(true);

    if (sessionmaint.isValidSession(tSession)) {
	String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = (String) tSession.getValue("userId");
	String src = request.getParameter("srcmenu");

	String sequenceShuffleData = request.getParameter("sequenceShuffleData");
	String hideEventData = request.getParameter("hideEventData");
	
	String calledFrom = request.getParameter("calledFrom");
	String calStatus = request.getParameter("calStatus");
	String protId = request.getParameter("protocolId");
	String calAssoc = request.getParameter("calAssoc");
	
	calledFrom = calledFrom.trim();
	int setId = 0;
	JSONArray shuffleEvent;
	JSONObject shuffleEventUpdate = null;
	JSONArray hideEvent;
	JSONObject hideEventUpdate = null;
	if (sequenceShuffleData.length() > 0) {
		try{
			shuffleEvent = new JSONArray(sequenceShuffleData);
		  	if (calledFrom.equals("P")  || calledFrom.equals("L")){
		  	//PCAL-20461 For Upating Events Sequence under the Visit
	    		for(int iS=0;iS<shuffleEvent.length();iS++)
				{
					shuffleEventUpdate = shuffleEvent.getJSONObject(iS);
					String eventID=shuffleEventUpdate.getString("eventID");
					String eventSeqNew=shuffleEventUpdate.getString("eventSeqNew");
						
					eventdefB.setEvent_id(Integer.parseInt(eventID));
					eventdefB.getEventdefDetails();
					eventdefB.setEventSequence(eventSeqNew);
					eventdefB.setModifiedBy(usr);
					eventdefB.setIpAdd(ipAdd);
					setId=eventdefB.updateEventdef();
				}
	    	}else if (calledFrom.equals("S")){
	    		//PCAL-20461 For Upating Events Hide/Unhide under the Visit
	    		for(int iS=0;iS<shuffleEvent.length();iS++)
				{
					shuffleEventUpdate = shuffleEvent.getJSONObject(iS);
					String eventID=shuffleEventUpdate.getString("eventID");
					String eventSeqNew=shuffleEventUpdate.getString("eventSeqNew");
					
					eventassocB.setEvent_id(Integer.parseInt(eventID));
					eventassocB.getEventAssocDetails();
					eventassocB.setEventSequence(eventSeqNew);
					eventassocB.setModifiedBy(usr);
					eventassocB.setIpAdd(ipAdd);
					setId=eventassocB.updateEventAssoc();
				}
			}
    	out.print(LC.L_Successful);/*out.print("Successful");*****/
	}catch(Exception exp)
	{
		out.print(LC.L_Error);/*out.print("Error");*****/
    	System.out.print("Exception Caught in Updating Events Sequence : "+exp);
	}
	}
	
	if (hideEventData.length() > 0)
	{
		try{
			hideEvent = new JSONArray(hideEventData);
			for(int iS=0;iS<hideEvent.length();iS++)
			{
				hideEventUpdate = hideEvent.getJSONObject(iS);
				String eventID= hideEventUpdate.getString("eventID");
				String eventIds [] =new String[1];
				eventIds[0]=eventID;
				String hideFlag  =hideEventUpdate.getString("eventHiddenNew");
				String strArrFlags [] =new String[1];
				strArrFlags[0]="0";
				int i=eventassocB.hideUnhideEventsVisits(eventIds,hideFlag, strArrFlags);
			}
			out.print(LC.L_Successful);/*out.print("Successful");*****/
		}catch(Exception exp)
		{
			out.print(LC.L_Error);/*out.print("Error");*****/
	    	System.out.print("Exception Caught in Hiding/Unhide Events : "+exp);
		}
	  }
    
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>





