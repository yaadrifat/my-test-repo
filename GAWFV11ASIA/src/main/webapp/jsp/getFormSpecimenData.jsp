<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="SpecStatB" scope="page" class="com.velos.eres.web.specimenStatus.SpecimenStatusJB"/>
<jsp:useBean id="SpecimenB" scope="page" class="com.velos.eres.web.specimen.SpecimenJB"/>
<jsp:useBean id="StorageB" scope="page" class="com.velos.eres.web.storage.StorageJB"/>

<%
	HttpSession tSession = request.getSession(true);
 if (sessionmaint.isValidSession(tSession))
 {
	String formFillMode = "";
	String disabledStr = "";
	int specimenPk = 0;
	String locationName = "";
	String specimenId = "";
	String specimenType = "";
	String storedIn = "";
	Integer status=new Integer(0);
	String statusDate = "";
	int locationPk = 0;
	int parentLocation = 0;
	SpecimenStatusDao  specStatDao = new SpecimenStatusDao();
	ArrayList arStatus= new ArrayList();
	ArrayList arStatusDate= new ArrayList();
	String fkStorageForSpecimen = "";
	String locationLabel = "";
	String gridFor="";

	formFillMode  = request.getParameter("formFillMode");
	specimenPk  = EJBUtil.stringToNum(request.getParameter("specimenPk"));
	fkStorageForSpecimen = request.getParameter("fkStorageForSpecimen");


	if (StringUtil.isEmpty(formFillMode ))
	{
		formFillMode  = "N";
	}

 	if  (StringUtil.isEmpty(fkStorageForSpecimen ))
	{
		fkStorageForSpecimen  = "";
	}

	if (fkStorageForSpecimen=="" && formFillMode.equals("N"))
	{
		locationLabel="Stored In";
		gridFor = "SP";
	} else
	{
		locationLabel="Location";
		gridFor="ST";
	}

	if ( specimenPk > 0)
	{
		//get specimen details
		SpecimenB.setPkSpecimen(specimenPk);
		SpecimenB.getSpecimenDetails();

		specimenType = SpecimenB.getSpecType();
		specimenId = SpecimenB.getSpecimenId();
		locationPk= EJBUtil.stringToNum(SpecimenB.getFkStorage());

		StorageB.setPkStorage(locationPk);
		StorageB.getStorageDetails();

		parentLocation = EJBUtil.stringToNum(StorageB.getFkStorage());
		storedIn = StorageB.getStorageName();

		if (StringUtil.isEmpty(storedIn))
		{
			storedIn = "";
		}

		//get spec status details
		specStatDao = SpecStatB.getLatestSpecimenStaus(specimenPk);
		arStatus = specStatDao.getFkCodelstSpecimenStats();
		arStatusDate = specStatDao.getSsDates();


		if (arStatus.size() >0)
		{
			status = (Integer) arStatus.get(0);
			statusDate = (String) arStatusDate.get(0);
		}

		//get parent location details
		StorageB.setPkStorage(parentLocation);
		StorageB.getStorageDetails();

		locationName = StorageB.getStorageName();

		if (StringUtil.isEmpty(locationName))
		{
			locationName = "";
		}


	}


	if ( formFillMode.equals("M"))
	{
	 disabledStr = " DISABLED ";
	}

	CodeDao cdSpecTyp = new CodeDao();
	cdSpecTyp.getCodeValues("specimen_type");
	String dSpecType = "";
	String dSpecimenStatus = "";


	String specTyp =	request.getParameter("specType");

	dSpecType=cdSpecTyp.toPullDown("specType",EJBUtil.stringToNum(specimenType),disabledStr);

	CodeDao cdStat = new CodeDao();
    cdStat.getCodeValues("specimen_stat");

	dSpecimenStatus=cdStat.toPullDown("specimenStatus",status.intValue(),disabledStr);


%>
<div style="border:double;border-width: thin;">
<table width="98%" cellspacing="0" cellpadding="2" >

<tr>
		<td colspan="4">Leave 'Specimen ID' field blank for system auto-generated ID</td>
</tr>

<tr>

	<td class=tdDefault width="15%"><b>Specimen ID</b></td>
	<td class=tdDefault width="15%"><Input type = "text" name="specimenId"   size=15 MAXLENGTH = 100 <%= disabledStr%> value=<%=specimenId%>></td>

    <td  width="15%"><b><%=locationLabel%></b></td>
		<td width="23%"><Input type = "text" name="storageLoc" value = "<%=locationName%>" size=15 align = right readonly <%= disabledStr%>>
		<Input type = "hidden" name="mainFKStorage" value = "" size=20 align = right>

		<Input type = "hidden" name="strCoordinateY" value = "" size=20 align = right>
		<Input type = "hidden" name="strCoordinateX" value = "" size=20 align = right>

	<% if ( formFillMode.equals("N"))
	{ %>
		<A  href="javascript:void(0);" onClick="return openLocationLookup(document.er_fillform1,'<%=gridFor%>')" >Select</A></td>

		<td><A  href="javascript:void(0);" onClick="return removeLocation(document.er_fillform1,1)" >Remove</A></td>

	<% } 
	 if (formFillMode.equals("M")) {
	%>
	<td class=tdDefault width="15%"><b>Stored In</b></td><td><%=storedIn%></td>
	<% } %>

</tr>

<tr>
	<td class=tdDefault width="15%" ><b>Specimen Type</b></td>

	<td width="15%"><%=dSpecType%></td>

	<td width="15%"><b>Specimen Status</b></td><td width="20%"><%=dSpecimenStatus%></td>
	<td width="15%"><b>Status Date</b></td>
		<!--KM-#4073 -To avoid Dynamic html validation -->
	<td class=tdDefault width="15%">
<%-- INF-20084 Datepicker-- AGodara --%>
	<% if ( formFillMode.equals("N")){ %>
		<Input type = "text" name="statDate" id="statDate" class="datefield" <%= disabledStr%> readonly  size=10 MAXLENGTH =20 value=<%=statusDate%>>
	<% }else{ %>
		<Input type = "text" name="statDate" id="statDate" <%= disabledStr%> readonly  size=10 MAXLENGTH =20 value=<%=statusDate%>>
	<%}%>
	</td>

</tr>
</table>
</div>
<br>
<% } //end of session check%>