<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>

<SCRIPT Language="javascript">

function openGroupWindow(formobj,formname) {

	 selGrpNames1 =formobj.selGrpIds.value;

	/* Added By Amarnadh for bugzilla issue #3231 */

	if (selGrpNames1.indexOf("+") >= 0) {
       selGrpNames1 = replaceSubstring(selGrpNames1, "+", "[VELPLUS]");
       }

   windowName=window.open("selectGroup.jsp?openerform="+formname+"&selectGroups="+selGrpNames1,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=350,height=300");//km
	windowName.focus();

}

function openStudyWindow(formobj,formname) {
    selectStudy=formobj.selStudyIds.value;

	/* Added By Amarnadh for bugzilla issue #3231 */
	selectStudy=encodeString(selectStudy);
	if (selectStudy.indexOf("+") >= 0) {
       selectStudy = replaceSubstring(selectStudy, "+", "[VELPLUS]");
    }

    windowName=window.open("selectStudy.jsp?openerform="+formname+"&selectStudy="+selectStudy,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=350,height=300");
	windowName.focus();
}


function openOrgWindow(formobj,formname) {

	formorg1=formobj.selOrgIds.value;

	/* Added By Amarnadh for bugzilla issue #3231 */
	formorg1=encodeString(formorg1);
	if (formorg1.indexOf("+") >= 0) {
       formorg1 = replaceSubstring(formorg1, "+", "[VELPLUS]");
       }
	windowName=window.open("selectOrg.jsp?openerform="+formname+"&selectOrgs="+formorg1,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=350,height=300");//km
	windowName.focus();
}

function openViewGroupWindow(formobj){
	grpIds = formobj.selGrpIds.value;

	if (formobj.selGrpNames.value == '')
	{
		alert("<%=MC.M_Selc_GrpFirst%>");/*alert("Please select a Group first");*****/
		return;
	}


	windowName=window.open("viewGroup.jsp?grpIds="+grpIds,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=350,height=300");
	windowName.focus();
}

function openViewStudyWindow(formobj){
	studyIds = formobj.selStudyIds.value;

	if (formobj.selStudy.value == '')
	{
		alert("<%=MC.M_PlsSel_StdFst%>");/*alert("Please select a <%=LC.Std_Study%> first");*****/
		return;
	}

	windowName=window.open("viewStudyPopup.jsp?studyIds="+studyIds,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=350,height=300");
	windowName.focus();
}

function openViewOrgWindow(formobj){

	orgIds = formobj.selOrgIds.value;

	if (formobj.selOrg.value == '')
	{
		alert("<%=MC.M_Selc_OrgFirst%>");/*alert("Please select an Organization first");*****/
		return;
	}
	windowName=window.open("viewOrg.jsp?orgIds="+orgIds,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=350,height=300");
	windowName.focus();
}

</SCRIPT>
<body>

<jsp:useBean id="objShareB" scope="request" class="com.velos.eres.web.objectShare.ObjectShareJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>


<%@ page import="com.velos.eres.service.util.*" %>

<%@ page language = "java" import = "com.velos.eres.business.site.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%>

<%
 HttpSession tSession = request.getSession(true);
 String sharedWith="";
 String grpids="";
 String shrdWith="";
 String grpNames="";
 String shrdWithNames="";
 String shrdWithIds="";
 String codeStatus="";
 int groupLen=0;
 int count=0;
 String selNames="";
 String mode="";

 if (sessionmaint.isValidSession(tSession))
	{
	String objNumber = request.getParameter("objNumber");
	String formobj = request.getParameter("formobject");
	String formnamevalue = request.getParameter("formnamevalue");
	mode = request.getParameter("mode");

	if( (mode == null)  || (mode.equals("null")))
	{
		 mode="M";
	}

	String accId=(String)tSession.getAttribute("accountId");
	String usrId = (String) tSession.getValue("userId");
	int iaccId=EJBUtil.stringToNum(accId);

	int fkObj = 0;
//SV, 10/28, so far no issues. REDTAG, what type effect will it have, to include mode = "N" here?
	if(mode.equals("M") || mode.equals("N"))
	{
		fkObj = EJBUtil.stringToNum(request.getParameter("fkObj"));
		sharedWith = request.getParameter("sharedWith");

		objShareB.getObjectShareDetails(fkObj, sharedWith,objNumber,usrId, accId);

	    shrdWithNames=objShareB.getShrdWithNames();
	    if(shrdWithNames==null){shrdWithNames="";}
		shrdWithIds=objShareB.getShrdWithIds();
	}%>


<input type="hidden" name="selUsrId" value=<%=usrId%>>
<input type="hidden" name="selAccId" value=<%=accId%>>
<input type="hidden" name="selGrpIds" value=<%=shrdWithIds%>>
<input type="hidden" name="selStudyIds" value=<%=shrdWithIds%>>
<input type="hidden" name="selOrgIds" value=<%=shrdWithIds%>>
<input name="mode" type=hidden value=<%=mode%>></td>
<input name="fkObj" type=hidden value=<%=fkObj%>>
<input name="objNumber" type=hidden value=<%=objNumber%>>
<input name="sharedWith" type=hidden value=<%=sharedWith%>>


<table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl midAlign">
	<tr>
	  <td width="18%"><br><%=LC.L_Shared_with%><%--Shared With*****--%></td>
	  <%if(sharedWith.equals("P")){%>
		  <td><br><input type="radio"name="rbSharedWith" value="P" checked><%=LC.L_Private%><%--Private*****--%></td>
	  <%}else{%>
	  <td><br><input type="radio"name="rbSharedWith" value="P"><%=LC.L_Private%><%--Private*****--%></td>
		  <%}%>
	  </tr>
	  <tr>
	  <td></td>
		<%if(sharedWith.equals("")|| sharedWith.equals("A")){%>
		  <td><br><input type="radio" name="rbSharedWith" value="A" checked><%=LC.L_All_AccUsers%><%--All Account Users*****--%></td>
		  <%}else{%>
		  <td><br><input type="radio" name="rbSharedWith" value="A"><%=LC.L_All_AccUsers%><%--All Account Users*****--%></td>
		   <%}%>
	  </tr>
	  <tr>
	  <td></td>
		<%if(sharedWith.equals("G")){%>
	   <td><br><input type="radio" name="rbSharedWith" value="G" checked><%=MC.M_AllUsr_InGrp%><%--All Users In a Group*****--%></td>
		 <%}else{%>
		  <td><br><input type="radio" name="rbSharedWith" value="G" ><%=MC.M_AllUsr_InGrp%><%--All Users In a Group*****--%></td>
		  <%}%>
	  <td><br><A HREF="#" onClick="openGroupWindow(<%=formobj%>,'<%=formnamevalue%>')"><%=LC.L_Select_Grp%><%--Select Group*****--%></A></td><!--km-->
	  <td><br><input type="text" name="selGrpNames" <%if(sharedWith.equals("G")){%> value="<%=shrdWithNames%>"<%}%> size=35 maxlength=40 readOnly=true>

		</td>
	  <td><br><A href="#" onClick="openViewGroupWindow(<%=formobj%>)"><%=LC.L_View_List%><%--View List*****--%></td>
	  </tr>
	  <tr>
	  <td></td>
	<%if(sharedWith.equals("S")){%>
	  <td><br><input type="radio" name="rbSharedWith" value="S" checked><%=MC.M_AllUsr_StdTeam%><%--All Users in <%=LC.Std_Study%> Team*****--%></td>
		<%}else{%>
	  <td><br><input type="radio" name="rbSharedWith" value="S" ><%=MC.M_AllUsr_StdTeam%><%--All Users in <%=LC.Std_Study%> Team*****--%></td>
		 <%}%>
	<td><br><A href="#" onClick="openStudyWindow(<%=formobj%>,'<%=formnamevalue%>')"><%=LC.L_Select_Study%><%--Select <%=LC.Std_Study%>*****--%></A></td>
	  <td><br><input type="text" name="selStudy" <%if(sharedWith.equals("S")){%>value="<%=shrdWithNames%>"<%}%> size=35 maxlength=40 readOnly=true></td>
	  <td><br><A href="#" onClick="openViewStudyWindow(<%=formobj%>)"><%=LC.L_View_List%><%--View List*****--%></td>
	  </tr>
	   <tr>
	  <td></td>
	  <%if(sharedWith.equals("O")){%>
	  <td><br><input type="radio" name="rbSharedWith" value="O" checked><%=MC.M_AllUsr_InOrg%><%--All Users in an Organization*****--%></td>
		<%}else{%>
		<td><br><input type="radio" name="rbSharedWith" value="O"><%=MC.M_AllUsr_InOrg%><%--All Users in an Organization*****--%></td>
		 <%}%>
	  <td><br><A href="#" onClick="openOrgWindow(<%=formobj%>,'<%=formnamevalue%>')"><%=LC.L_Select_Org%><%--Select Organization*****--%></A></td><!--km-->
	  <td><br><input type="text" name="selOrg" <%if(sharedWith.equals("O")){%>value="<%=shrdWithNames.replace("\"","''")%>" <%}%>size=35 maxlength=40 readOnly=true></td>
	  <td><br><A href="#" onClick="openViewOrgWindow(<%=formobj%>)"><%=LC.L_View_List%><%--View List*****--%></td>
	  </tr>
	  </table>



<%	}
else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>

</body>

</html>

