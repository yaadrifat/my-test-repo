<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.StringUtil,com.velos.esch.web.protvisit.ProtVisitJB,com.velos.esch.business.common.*" %>
<%@page import="org.json.*"%>
<%@ page import = "com.velos.eres.service.util.MC,com.aithent.audittrail.reports.AuditUtils,com.velos.esch.web.protvisit.ProtVisitResponse,com.velos.eres.service.util.LC"%>
<%@ page import = "com.velos.esch.service.util.EJBUtil,com.velos.eres.service.util.VelosResourceBundle"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="protVisitB" scope="request" class="com.velos.esch.web.protvisit.ProtVisitJB"/>

<%
	response.setContentType("application/json");
	String visitJSONString = request.getParameter("myVisitGrid");
	visitJSONString = StringUtil.replaceAll(visitJSONString, "null", "\"\"");
	visitJSONString = StringUtil.replaceAll(visitJSONString, "velquote", "\"");
	visitJSONString = visitJSONString.replaceAll("(\\r|\\n|\\r\\n)+", "");
	visitJSONString = visitJSONString.replaceAll("(br /)+", "");
	String updateString = request.getParameter("updateInfo");
	String deleteString = request.getParameter("deleteInfo");
	String src = request.getParameter("srcmenu");
	String protocolId = request.getParameter("protocolId");
	String calledFrom = request.getParameter("calledFrom");
	String mode = request.getParameter("mode");
	String fromPage = request.getParameter("fromPage");
	String calStatus = request.getParameter("calstatus");

	String visitId = "";
	String from = request.getParameter("from");
	int new_protocol_duration = 0;
	String duration = request.getParameter("duration");
	String calAssoc = request.getParameter("calassoc");
	request.setCharacterEncoding("UTF-8");

	calAssoc = (calAssoc == null) ? "" : calAssoc;
	int ret = 0;
	JSONObject jsObj = new JSONObject();
	HttpSession tSession = request.getSession(true);

	if (!sessionmaint.isValidSession(tSession)) {
		// Session is invalid; print an error and exit
		response.sendError(HttpServletResponse.SC_FORBIDDEN,
				"SESSION_TIME_OUT");
		jsObj.put("result", -1);
		jsObj.put("resultMsg", MC.M_UsrNot_LoggedIn/*"User is not logged in."*****/);
		out.println(jsObj.toString());
		return;
	}

	JSONArray visitArray = new JSONArray(visitJSONString);
	JSONArray updateArray = new JSONArray(updateString);
	String ip_add = (String) tSession.getValue("ipAdd");
	String usr = (String) tSession.getValue("userId");
	String accountId = (String) tSession.getValue("accountId");
	protVisitB.setProtocol_id(EJBUtil.stringToNum(protocolId));
	protVisitB.setCreator(usr);
	protVisitB.setip_add(ip_add);
	JSONObject gridRecord = null;
	ArrayList wrongVisitArray = new ArrayList();
	ArrayList wrongVisitNameArray = new ArrayList();
	
	//Modified for INF-18183 and BUG#7224 : Raviesh
	ProtVisitResponse visitResponse = protVisitB.validateVisits(
			visitJSONString, updateString, StringUtil.stringToNum(duration), calledFrom,calStatus,deleteString,AuditUtils.createArgs(session,"",LC.L_Cal_MngVsts));					
	ret = visitResponse.getVisitId();
	visitId = (new Integer(ret)).toString();
	if ((calledFrom.equals("P")) || (calledFrom.equals("S"))) {
		if (ret < 0){
			jsObj = protVisitB.getErrorMsgForManageVisits(visitResponse);
			out.println(jsObj.toString());
			return;
		}
	}
	jsObj.put("result", 0);
	jsObj.put("resultMsg", MC.M_ValidationComplete/*"Validation has been completed successfully."*****/);
	out.println(jsObj.toString());
%>





