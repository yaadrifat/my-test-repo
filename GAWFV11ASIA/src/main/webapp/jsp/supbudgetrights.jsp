<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=LC.L_Bgt_AccessRights%><%-- Budget Access Rights*****--%></title>
<%@ page import="com.velos.eres.service.util.*"%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.esch.business.common.BudgetDao" %>
<jsp:useBean id="grp" scope="request" class="com.velos.eres.web.group.GroupJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/><b></b>
</head>


<SCRIPT Language="JavaScript">

//Added by Manimaran for June Enhancement #GL2
function checkAll(formobj){
	totcount = formobj.totalrows.value;
	if(formobj.All.checked) {
	   for (i=0;i<totcount;i++) {
		   formobj.newr[i].checked = true;
		   formobj.edit[i].checked = true;
		   formobj.view[i].checked = true;
		   formobj.rights[i].value=7;
	   }
   }
   else{
	    for (i=0;i<totcount;i++) {
		   formobj.newr[i].checked = false;
		   formobj.edit[i].checked = false;
		   formobj.view[i].checked = false;
		   formobj.rights[i].value=0;
	   }
   }
}

function refreshRights(formobj,rows)
{


	for(j=0;j<3;j++)
		{
			if(formobj.budgetScope[j].checked== true){
				formobj.budgetScope[j].checked = false;
				formobj.accessFor.value="-";
			}
		}
	if(rows>1)
	{
		for(i=0;i<rows;i++)
			{
				formobj.newr[i].checked = false;
				formobj.edit[i].checked = false;
				formobj.view[i].checked = false;
				formobj.rights[i].value = 0;

			}
	}
	else {
				formobj.newr.checked = false;
				formobj.edit.checked = false;
				formobj.view.checked = false;
				formobj.rights.value = 0;
	}

}


function changeRights(obj,row, frmname)	{
	  selrow = row ;
	  totrows = frmname.totalrows.value;
	  if (totrows > 1)
		rights =frmname.rights[selrow].value;
	  else
		rights =frmname.rights.value;

	  objName = obj.name;
	  if (obj.checked)  {
       	if (objName == "newr")	{
			rights = parseInt(rights) + 1;
			if (!frmname.view[selrow].checked)	{
				frmname.view[selrow].checked = true;
				rights = parseInt(rights) + 4;
			}
		}
       	if (objName == "edit")	{
			rights = parseInt(rights) + 2;
			if (!frmname.view[selrow].checked){
				frmname.view[selrow].checked = true;
				rights = parseInt(rights) + 4;
			}
		}
       	if (objName == "view") rights = parseInt(rights) + 4;

		if (totrows > 1 )
			frmname.rights[selrow].value = rights;
		else
			frmname.rights.value = rights;
	  }else	{
	       	if (objName == "newr") rights = parseInt(rights) - 1;
    	   	if (objName == "edit") rights = parseInt(rights) - 2;
	       	if (objName == "view"){
			if (frmname.newr[selrow].checked)
			{
				alert("<%=MC.M_UsrRgt_CntRevoke%>");/*alert("The user has right to New or Edit. You can not revoke right to View");*****/
				frmname.view[selrow].checked = true;
			}
		    else if (frmname.edit[selrow].checked)
			{
		    	alert("<%=MC.M_UsrRgt_CntRevoke%>");/*alert("The user has right to New or Edit. You can not revoke right to View");*****/
				frmname.view[selrow].checked = true;
			}
			else
			{
			  rights = parseInt(rights) - 4;
			}
		}
		if (totrows > 1 )
			frmname.rights[selrow].value = rights;
		else
			frmname.rights.value = rights;
    	 }
}

function validate(formobj) {




	 if (!(validate_col('e-Signature',formobj.eSign))) return false

	<%--  if(isNaN(formobj.eSign.value) == true) {
		 alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
   } --%>
}

function openUserWindow(pgRight, formObj) {
	if (f_check_perm(pgRight,'N') == true) {
		leventId = "";
		luserType = "B"; //called from Budget Page
		lsrcmenu = "";
		lduration = "";
		lprotocolId = "";
		lcalledFrom = "";
		lmode = "";
		lfromPage = "";
		leventmode = "";
		lcalStatus = "";
		lbudgetId = formObj.budgetId.value;

    	windowName=window.open("addeventuser.jsp?&eventId="+leventId+"&userType="+luserType+"&srcmenu="+lsrcmenu+"&duration="+lduration+"&protocolId="+lprotocolId+"&calledFrom="+lcalledFrom+"&mode="+lmode+"&fromPage="+lfromPage+"&eventmode="+leventmode+"&calStatus="+lcalStatus+"&budgetId="+lbudgetId,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200");
		windowName.focus();
	} else {
		return false;
	}
}

function openUsers(formObj,userFlag) {
	studyId = formObj.bgtStudy.value;
	bgtSite = formObj.bgtSite.value;

	//if Organization is selected in Budget Scope, check whether org has been entered in budget
	 if (userFlag=="O") {
	 	if ((bgtSite == "null") || (bgtSite =="")) {
	 		alert("<%=MC.M_OrgNotSpecified_InBgt%>");/*alert("Organization not specified in Budget");*****/
			return false;
		}
	 }

 	 //if Study is selected in Budget Scope, check whether study has been entered in budget
	 if (userFlag=="S") {
	 	if ((studyId =="null") || (studyId =="")) {
	 		alert("<%=MC.M_StdNotSpecified_InBgt%>");/*alert("<%=LC.Std_Study%> not specified in Budget");*****/
			return false;
		}
	 }
   	windowName=window.open("budgetglobalusers.jsp?studyId="+studyId+"&userFlag="+userFlag+"&siteId="+bgtSite,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=400");
	windowName.focus();
}


function confirmBox(usrName,pgRight) {
	if (f_check_perm(pgRight,'E') == true) {
		 var paramArray = [usrName];
		if (confirm(getLocalizedMessageString("L_Rmv_FrmBgt",paramArray))) {/*if (confirm("Remove " + usrName + " from Budget?")) {*****/
		    return true;
		} else {
			return false;
		}
	} else {
		return false;
	}
}


function setAccessFor(val, formObj) {
	formObj.accessFor.value=val;
}

</SCRIPT>


<% String src="";
src= request.getParameter("srcmenu");
String budgetType=request.getParameter("budgetType");
String mode=request.getParameter("mode");
%>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<body>

<br>

<DIV class="browserDefault" id="div1">
<P class="sectionHeadings"><%=LC.L_Bgt_AccessRights%><%-- Budget Access Rights*****--%></P>

<Form name="budgetRights" id="bgtRightsFrm" method="post" action="updatesupbudrights.jsp" onSubmit="if (validate(document.budgetRights)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
<%
HttpSession tSession = request.getSession(true);
String selectedTab = request.getParameter("selectedTab");
String budgetId=request.getParameter("budgetId");
ArrayList ids = null;
ArrayList userIds = null;
ArrayList bgtUsrRights = null;
ArrayList bgtUsrLastNames = null;
ArrayList bgtUsrFirstNames = null;
String bgtUsrId=null;
String userId = null;
String bgtUsrRight = "";
String bgtUsrLastName = "";
String bgtUsrFirstName = "";
String bgtUsrFullName = "";
String budgetTemplate="";
int counter = 0;
int pageRight=0;

if(sessionmaint.isValidSession(tSession))  {

	String groupId = request.getParameter("groupId");
	groupId=(groupId==null)?"":groupId;

	int grpId=EJBUtil.stringToNum(groupId);
	grp.setGroupId(grpId);
  	grp.getGroupDetails();


	GrpRightsJB gRights = (GrpRightsJB) tSession.getValue("GRights");
	pageRight = Integer.parseInt(gRights.getFtrRightsByValue("MGRPRIGHTS"));

	if (pageRight > 0 )
	{

	ctrl.getControlValues("bgt_rights");
	int rows = ctrl.getCRows();




%>

   <Input type=hidden name="srcmenu" value="<%=src%>">
   <Input type=hidden name="selectedTab" value="<%=selectedTab%>">
     <Input type=hidden name="mode" value="<%=mode%>">
    <Input type=hidden name="groupId" value="<%=groupId%>">



   <P class="defComments"><%=LC.L_Define_AccessRights%><%-- Define Access Rights*****--%></P>
<%


	ArrayList feature =  ctrl.getCValue();
	ArrayList ftrDesc = ctrl.getCDesc();
	ArrayList ftrSeq = ctrl.getCSeq();
%>
    <Input type="hidden" name="totalrows" value=<%=rows%> >
    <TABLE width="50%" cellpadding="0" cellspacing="0" border="0" class="basetbl">
      <TH>&nbsp;</TH>
      <TH><%=LC.L_New%><%-- New*****--%></TH>
      <TH><%=LC.L_Edit%><%-- Edit*****--%></TH>
      <TH><%=LC.L_View%><%-- View*****--%></TH>
	  <!-- Modified by Amar for Bugzilla issue #3023 -->
	  <tr><td align="left" colspan="4"><input type="checkbox" name="All"  onClick="checkAll(document.budgetRights)">  <%=LC.L_SelOrDeSel_All%><%-- Select / Deselect All*****--%></td> </tr>
<%

	String bgtRights = grp.getGroupSupBudRights();

	for(int count=0;count<rows;count++){
	    String ftr = (String) feature.get(count);
    	String desc = (String) ftrDesc.get(count);
    	int seq = ((Integer) ftrSeq.get(count)).intValue();
		//get rights for the specific budget module
		String rights = String.valueOf(bgtRights.charAt(seq-1));

	%>
	   <TR>
       <Input type="hidden" name=rights value=<%=rights%> >
        <TD>
		<%= desc%>
		</TD>
		<%
		 if (rights.compareTo("7") == 0){//all rights
		 %>
        <TD align="center">
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>, document.budgetRights)" CHECKED>
        </TD>
        <TD align="center">
          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>, document.budgetRights)" CHECKED>
        </TD>
        <TD align="center">
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>, document.budgetRights)" CHECKED>
        </TD>
        <%}else if (rights.compareTo("1") == 0){//new right %>
        <TD align="center">
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>, document.budgetRights)" CHECKED>
        </TD>
        <TD align="center">
          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>, document.budgetRights)" >
        </TD>
        <TD align="center">
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>, document.budgetRights)">
        </TD>
        <%}else if(rights.compareTo("3") == 0){//new & edit right %>
        <TD align="center">
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>, document.budgetRights)" CHECKED>
        </TD>
        <TD align="center">
          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>, document.budgetRights)" CHECKED>
        </TD>
        <TD align="center">
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>, document.budgetRights)">
        </TD>
        <%}else if (rights.compareTo("2") == 0){//edit right %>
        <TD align="center">
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>, document.budgetRights)">
        </TD>
        <TD align="center">
          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>, document.budgetRights)" CHECKED>
        </TD>
        <TD align="center">
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>, document.budgetRights)">
        </TD>
        <%}else if (rights.compareTo("4") == 0){//view right %>
        <TD align="center">
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>, document.budgetRights)">
        </TD>
        <TD align="center">
          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>, document.budgetRights)">
        </TD>
        <TD align="center">
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>, document.budgetRights)" CHECKED>
        </TD>
        <%}else if (rights.compareTo("5") == 0){//new & view right%>
        <TD align="center">
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>, document.budgetRights)" CHECKED>
        </TD>
        <TD align="center">
          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>, document.budgetRights)">
        </TD>
        <TD align="center">
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>, document.budgetRights)" CHECKED>
        </TD>
        <%}else if (rights.compareTo("6") == 0){//edit & view right%>
        <TD align="center">
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>, document.budgetRights)" >
        </TD>
        <TD align="center">
          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>, document.budgetRights)" CHECKED>
        </TD>
        <TD align="center">
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>, document.budgetRights)" CHECKED>
        </TD>
        <%}else{ %>

        <TD align="center">
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>, document.budgetRights)">
        </TD>
        <TD align="center">
          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>, document.budgetRights)">
        </TD>
        <TD align="center">
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>, document.budgetRights)">
        </TD>
        <%}%>
      </TR>

	<%
	}
%>
  	 <tr height=15><td colspan="4"></td></tr>

	<%	if (pageRight >= 6) { %>
	<table  width="100%" cellpadding="0" cellspacing="0" border="0" >
	<tr><td>
	 <jsp:include page="submitBar.jsp" flush="true">
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="bgtRightsFrm"/>
			<jsp:param name="showDiscard" value="N"/>
	 </jsp:include>
	<% }%>
	</td></tr>
	</table>


   <%


	} //end of if body for page right

	else
	{
	%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

		} //end of else body for page right

  }
  else { //end of if body for session

   %>
   <jsp:include page="timeout.html" flush="true"/>
   <%
  }
%>

</Form>
<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</div>

<div class="mainMenu" id="emenu">
  <jsp:include page="getmenu.jsp" flush="true"/>
</div>

</body>
</html>

