<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title><%=LC.L_Cal_StatusHistory%><%--Calendar Status History*****--%></title>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<%@ page import="com.velos.eres.service.util.*,com.velos.eres.web.user.UserJB,com.velos.eres.web.grpRights.GrpRightsJB"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>



<SCRIPT Language="javascript">

 function  validate(){

     formobj=document.calendarStatus

     if (!(validate_col('Status',formobj.calStatus))) return false

   }

</SCRIPT>


<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="ctrldao" scope="request" class="com.velos.esch.business.common.EventAssocDao"/>



<% String src;
String studyId="";
String from = "calendarhistory";
src= request.getParameter("srcmenu");
if(request.getParameter("studyId")!=null && !request.getParameter("studyId").equals("")){
studyId=request.getParameter("studyId");
}
%>

<jsp:include page="panel.jsp" flush="true">

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>



<body>
<% if("Y".equals(CFG.Workflows_Enabled)&& (studyId!=null && !studyId.equals("") && !studyId.equals("0"))){ %>
<DIV class="BrowserTopn" id="divTab">
<%}else{ %>
<DIV class="browserDefault" id="div1">
<%} %>
<jsp:include page="studytabs.jsp" flush="true">
<jsp:param name="from" value="<%=from%>"/>
<jsp:param name="studyId" value="<%=studyId %>"/>
 </jsp:include>
 <% if("Y".equals(CFG.Workflows_Enabled)&& (studyId!=null && !studyId.equals("") && !studyId.equals("0"))){ %>
</DIV>
<div class="BrowserBotN BrowserBotN_S_3 workflowDivBig" id="div1">
<%}%>
  <%

	String protocolId ="";
	String protocolName ="";
 	String desc ="";
	String durationNum="";

	String calStatus = "";
	String name="";
	String changedOn="";
	String status="";	
	int counter;
	String note="";


	HttpSession tSession = request.getSession(true);

	if (sessionmaint.isValidSession(tSession))

	{

		String uName = (String) tSession.getValue("userName");

		int pageRight = 0;

		//GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");

        	//pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CGRP"));
		String tab = request.getParameter("selectedTab");


		pageRight=7;

		protocolId = request.getParameter("protocolId");


		ctrldao= eventassocB.getPrevStatus(EJBUtil.stringToNum(protocolId));

		ArrayList names= ctrldao.getNames();
		
		//JM: 31JAN2011: Enh-D-FIN9
		//ArrayList stats= ctrldao.getStatus();
		ArrayList stats= ctrldao.getStatCodes();	
		
		ArrayList protStatIds=ctrldao.getProtStatIds();
		ArrayList statusChangedOns=ctrldao.getStatusChangedOns();
		ArrayList notes=ctrldao.getNotes();
   	  	int len= protStatIds.size();
%>
  <%

	if ((pageRight >=6) || ((pageRight == 5 || pageRight == 7 )) )

	{

%>
  <br>


<Form name="protocolbrowser" method="post" action="" onsubmit="">

    <input type="hidden" name="srcmenu" value='<%=src%>'>
    <input type="hidden" name="selectedTab" value='<%=tab%>'>

   <table width="100%" cellspacing="0" cellpadding="0" border=0 >
    <tr >
      <td width = "70%">
        <P class = "defComments"><%=MC.M_ListHist_StdPcolCal%><%--The list below displays the status history for this <%=LC.Std_Study_Lower%> protocol calendar*****--%></P>
      </td>
    </tr>
   </table>

    <table width="100%" >
       <tr>
        	<th width="30%"> <%=LC.L_Changed_By%><%--Changed By*****--%></th>
	        <th width="30%"> <%=LC.L_Date%><%--Date*****--%>  </th>
	        <th width="20%"> <%=LC.L_Status%><%--Status*****--%> </th>
	        <th width="20%"> <%=LC.L_Notes%><%--Notes*****--%> </th>
      </tr>
       <%

    for(counter = 0;counter<len;counter++)
	{
		name=((names.get(counter)) == null)?"-":(names.get(counter)).toString();
		changedOn=((statusChangedOns.get(counter)) == null)?"-":(statusChangedOns.get(counter)).toString();


		status=((stats.get(counter)) == null)?"-":(stats.get(counter)).toString();
		note=((notes.get(counter)) == null)?"-":(notes.get(counter)).toString();
		

		if ((counter%2)==0) {

  %>
      <tr class="browserEvenRow">
        <%

		}

		else{

  %>
      <tr class="browserOddRow">
        <%

		}

  %>
        <td align=center><%=name%> </td>
     	<td align=center><%=changedOn%></td>
     	<td align=center ><%=status %></td>
     	<td align=center><%=note%></td>


      </tr>
      <%

		}

%>
<tr><td></td>

 <td align=center>
		<button onclick="window.history.back(); return false;"><%=LC.L_Back%></button>
      </td>
</tr>
    </table>
  </Form>

  <%

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>

<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/>
</div>

</body>

</html>

