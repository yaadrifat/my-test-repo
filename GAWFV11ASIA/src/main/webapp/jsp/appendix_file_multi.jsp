<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<title><%=MC.M_AddNew_VerOrDocu%><%--Add New Version/Document*****--%></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

</HEAD>

<% 
String src= request.getParameter("srcmenu");
String from = "appendixver";
%>

<jsp:include page="sessionlogging.jsp" flush="true"/> 
<jsp:include page="include.jsp" flush="true"/>
<SCRIPT Language="javascript">
function getElementByID(formobj, id) {
	var elems = document.getElementsByName(id)
	return elems[0];
}

</SCRIPT>

<BODY>
<jsp:useBean id ="sessionmaint" scope="request" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="compJB" scope="request" class="com.velos.eres.compliance.web.ComplianceJB"/>
<jsp:useBean id="studyJB" scope="session" class="com.velos.eres.web.study.StudyJB" />
<%@ page  language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*"%>
<%@page import="com.velos.eres.widget.business.common.UIFlxPageDao, com.velos.eres.web.study.*"%>

<DIV class="popDefault" id = "div1" style="width:auto"> 
  <%

HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession))
{

	String accId = (String) tSession.getValue("accountId"); 
	String docflag = request.getParameter("docflag")==null?"":request.getParameter("docflag");
	String userId = (String) tSession.getValue("userId");
	String uName = (String) tSession.getValue("userName");
	String sessStudyId = "";
	String studyNo = "";
	String studyTitle="";
	if(request.getParameter("studyId")!=null){
		sessStudyId=request.getParameter("studyId");
		studyJB.setId(StringUtil.stringToNum(sessStudyId));
		studyJB.getStudyDetails();
		studyNo=studyJB.getStudyNumber();
		studyTitle = studyJB.getStudyTitle();
		
	}
	else{
		sessStudyId=(String) tSession.getValue("studyId");
		studyNo=(String) tSession.getValue("studyNo");
	}
	String tab = request.getParameter("selectedTab");
	String studyVerId = request.getParameter("studyVerId");	
	String accMaxStorage = (String) tSession.getAttribute("accMaxStorage");

	int stId=EJBUtil.stringToNum(sessStudyId);
	CtrlDao ctrlDao = new CtrlDao();	
	long freeSpace = ctrlDao.getFreeSpace(EJBUtil.stringToNum(accId));
	String defUserGroup = (String) tSession.getAttribute("defUserGroup");
	
	StudyVerDao studyVerDao = new StudyVerDao();
	studyVerDao.getAllVers(stId);
	ArrayList verNumberList = studyVerDao.getStudyVerNumbers();
	ArrayList verCategoryList = studyVerDao.getStudyVerCategoryIds();
	
	StringBuffer numCatArray = new StringBuffer();
	if (verNumberList.size() > 0 && verNumberList.size() == verCategoryList.size()) {
	    for (int iX=0; iX<verNumberList.size(); iX++) {
	        if (iX>0) { numCatArray.append(","); }
	        numCatArray.append("\"").append(verNumberList.get(iX)).append("-")
		        .append(verCategoryList.get(iX)).append("\":").append(iX);
	    }
	}
	UIFlxPageDao uiFlxPageDao = new UIFlxPageDao();
	int versionNum = uiFlxPageDao.getHighestFlexPageVersion("er_study", StringUtil.stringToNum(sessStudyId));
	
	HashMap<String, Object> paramMap = new HashMap<String, Object>();
	paramMap.put("userId",(userId.equals(null) ? null : Integer.valueOf(userId)));
	paramMap.put("accountId",(accId.equals(null) ? null : Integer.valueOf(accId)));
	paramMap.put("studyId", (sessStudyId.equals(null) ? null: Integer.valueOf(sessStudyId)));

	studyJB.setId(StringUtil.stringToNum(sessStudyId));
	studyJB.getStudyDetails();	
	int mVerNum = 0;
	String latestVersionStr="";
	String curStatus = compJB.getCurrentStudyStatus(paramMap);
	int vNum = uiFlxPageDao.getHighestFlexPageVersion("er_study", Integer.valueOf(sessStudyId));	    
    mVerNum=uiFlxPageDao.getHighestMinorFlexPageVersion(vNum, "er_study", Integer.valueOf(sessStudyId));
    int rowExists = uiFlxPageDao.checkLockedVersionExists(studyJB.getId(), versionNum);
    if (rowExists < 1) {
    	rowExists = uiFlxPageDao.checkIrbApprovedExists(studyJB.getId(), versionNum);
    }

    if("crc_amend".equals(curStatus)) {
    	versionNum++;
    	mVerNum=0;
    } else if(mVerNum >= 0) {
    	if(rowExists == 1) {
    		mVerNum++;
    	} else {
	    	if (versionNum == 0){
	    		versionNum++;
	    	} else {
	    		mVerNum++;
	    	}
    	}
    } else {
    	versionNum++;
    	//mVerNum++;
    }

	latestVersionStr = versionNum + ".";
	latestVersionStr += (mVerNum > 9) ? mVerNum : "0" + mVerNum;
	//double nextVer = Double.valueOf(latestVersionStr);	
%>
<script>
function invalidVersionAndCategory(verNumber, category) {
	
	var numCatArray = {<%=numCatArray%>};
	var re1 = /\s+$/g;
	var re2 = /^\s+/g;
	verNumber = verNumber.replace(re1, "").replace(re2,"");
	var key = verNumber+"-"+category;
	var test = numCatArray[key];
	if (test == null) { return false; }
	return true;
}

function fixVerNumber(formobj) {
    if (!(validate_col('Category',getElementByID(formobj, "dStudyvercat1")))) return false;
    if (!(validate_col('File',getElementByID(formobj, "name1")))) return false;
    if (!(validate_col('Description',getElementByID(formobj, "desc1")))) return false;
	   for (var iX=1; iX<6; iX++) {
		     if (
				  ( getElementByID(formobj, "dStudyvercat"+iX).value != null && 
				    getElementByID(formobj, "dStudyvercat"+iX).value != '' ) ||
				  ( getElementByID(formobj, "name"+iX).value != null && 
				    getElementByID(formobj, "name"+iX).value != '' ) ||
				  ( getElementByID(formobj, "desc"+iX).value != null &&
				    getElementByID(formobj, "desc"+iX).value != '' ) ||
				  ( getElementByID(formobj, "dStudyvertype"+iX).value != null &&
					getElementByID(formobj, "dStudyvertype"+iX).value != '' ) || 
				  ( getElementByID(formobj, "versionDate"+iX).value != null &&
					getElementByID(formobj, "versionDate"+iX).value != '' )
				)
		 	 {
		    	 getElementByID(formobj, "verNumber"+iX).value = '<%=latestVersionStr%>';
		 	 } else {
		 		 break;
		 	 }
	   }
    return true;
}
function  validate(formobj){
<% if("LIND".equals(CFG.EIRB_MODE)) { %>
	if (!fixVerNumber(formobj)) {
		return false;
	}
<% } %>	
	   for (var iX=1; iX<6; iX++) {
	     if (
	    	  ( getElementByID(formobj, "verNumber"+iX).value != null &&
	    	    getElementByID(formobj, "verNumber"+iX).value != '' ) ||
			  ( getElementByID(formobj, "dStudyvercat"+iX).value != null && 
			    getElementByID(formobj, "dStudyvercat"+iX).value != '' ) ||
			  ( getElementByID(formobj, "name"+iX).value != null && 
			    getElementByID(formobj, "name"+iX).value != '' ) ||
			  ( getElementByID(formobj, "desc"+iX).value != null &&
			    getElementByID(formobj, "desc"+iX).value != '' ) ||
			  ( getElementByID(formobj, "dStudyvertype"+iX).value != null &&
				getElementByID(formobj, "dStudyvertype"+iX).value != '' ) || 
			  ( getElementByID(formobj, "versionDate"+iX).value != null &&
				getElementByID(formobj, "versionDate"+iX).value != '' )
			)
	 	 {
	     	 for (var iY=iX-1; iY>0; iY--) {
	     	     if (getElementByID(formobj, "verNumber"+iY).value == null ||
	     	     	 getElementByID(formobj, "verNumber"+iY).value == '')
	     	     {
	         	     alert("<%=MC.M_PlsRow_TopOrder%>");/*alert("Please use rows from the top in order.");*****/
	         	     return false;
	         	 }
	     	 }
	         if (!(validate_col('Version Number',getElementByID(formobj, "verNumber"+iX)))) return false;
	         if (!(validate_col('Category',getElementByID(formobj, "dStudyvercat"+iX)))) return false;
	         if (!(validate_col('File',getElementByID(formobj, "name"+iX)))) return false;
	         if (!(validate_col('Description',getElementByID(formobj, "desc"+iX)))) return false;
	     }
	   
	    	if( getElementByID(formobj, "name"+iX).value.indexOf("..")!=-1)
	    	  {/*alert("Special Character(..) not allowed for this Field");*****/
	    	var paramArray = [".."];
	    	alert(getLocalizedMessageString("M_SplCharNotAllowed", paramArray));	  
	    	getElementByID(formobj,"name"+iX).focus();
	    	return false;
	    	}	
	     
	     if(getElementByID(formobj,"desc"+iX).value.length>500){
		     alert(" <%=MC.M_SrtDesc_MaxChar%>");/*alert(" 'Short Description' exceeded maximum number of characters allowed.");*****/
		     getElementByID(formobj,"desc"+iX).focus();
		     return false;
	     }
	     
	      if(invalidVersionAndCategory(getElementByID(formobj,"verNumber"+iX).value,
	    		 getElementByID(formobj,"dStudyvercat"+iX).value)) {
		     alert("<%=MC.M_VerNum_CatSel%>");/*alert("The Version Number-Category combination you have entered already exists. Please select another.");*****/
		     return false;
		 } 
		 
	   }
	   if (!(validate_col('e-Sign',formobj.eSign))) return false
// 	   if(isNaN(formobj.eSign.value) == true) {
<%-- 	       alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/ --%>
// 	       formobj.eSign.focus();
// 	       return false;
// 	   }
}

</script>
<%	    
	
	String browser = request.getHeader("user-agent");
	String ddStudyvercat = "" ;
	CodeDao cd1=new CodeDao();
	cd1.getCodeValues("studyvercat");
	cd1.setCType("studyvercat");
	cd1.setForGroup(defUserGroup);
	String stdvercat=request.getParameter("dStudyvercat");
	if (stdvercat==null) stdvercat="";		
	if (stdvercat.equals("")){
		ddStudyvercat=cd1.toPullDown("dStudyvercat");
	} else{
		ddStudyvercat=cd1.toPullDown("dStudyvercat",EJBUtil.stringToNum(stdvercat),true);
	}

	String ddStudyvertype = "" ;
	CodeDao cd2=new CodeDao();
	cd2.getCodeValues("studyvertype");
	cd2.setCType("studyvertype");
	cd2.setForGroup(defUserGroup);
	String stdvertype=request.getParameter("dStudyvertype");
	if (stdvertype==null) stdvertype="";		

	if (stdvertype.equals("")){
		ddStudyvertype=cd2.toPullDown("dStudyvertype");
	} else{
		ddStudyvertype=cd2.toPullDown("dStudyvertype",EJBUtil.stringToNum(stdvertype),true);
	}

%>
  
<% 

	com.aithent.file.uploadDownload.Configuration.readSettings("eres");
	com.aithent.file.uploadDownload.Configuration.readUploadDownloadParam(com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD + "fileUploadDownload.xml", "study");
	String upld=com.aithent.file.uploadDownload.Configuration.UPLOADSERVLET;
	
%>
  <br><br>
  <Form name=upload action=<%=upld%> id="appFileMultiFrm" onsubmit="if (validate(document.upload)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}"  METHOD="POST" ENCTYPE="multipart/form-data">

 <P class="defComments"><b><%=LC.L_Study_Number%><%--<%LC.Std_Study%> Number*****--%>: </b><%=studyNo%></P>
 <P class="defComments"><b><%=LC.L_Study_Title%><%--<%=LC.Std_Title%> Title*****--%>: </b><%=studyTitle%></P>

   	<input type=hidden name=accId value=<%=accId%>>
  	<input type=hidden name=userId value=<%=userId%>>
  	<input type=hidden name=srcmenu value=<%=src%>>
    <table style="width:auto" cellspacing="0" cellpadding="0" border="0" class="outline">
      <tr>
      	<% if("LIND".equals(CFG.EIRB_MODE)) {  %> 
        	<th align="center" style="display:none"><%=LC.L_Version_Number%><%--Version Number*****--%><FONT class="Mandatory">*</FONT></th>
			<%-- INF-20084 Datepicker-- AGodara --%>
        	<th colspan = 1 align="center" style="display:none"><%=LC.L_Version_Date%><%--Version Date*****--%></th> 
        <% } else { %>
        	<th align="center"><%=LC.L_Version_Number%><%--Version Number*****--%><FONT class="Mandatory">*</FONT></th>
			<%-- INF-20084 Datepicker-- AGodara --%>
        	<th colspan = 1 align="center"><%=LC.L_Version_Date%><%--Version Date*****--%></th> 
         <% } %>
        <th align="center"><%=LC.L_Category%><%--Category*****--%><FONT class="Mandatory">*</FONT></th>
        <th align="center"><%=LC.L_Type%><%--Type*****--%></th>
        <th align="center"><%=LC.L_File%><%--File*****--%><FONT class="Mandatory">*</FONT></th>
        <th align="center"><%=LC.L_Description%><%--Description*****--%><FONT class="Mandatory">*</FONT></th>
      </tr>
   <% for (int iX=1; iX<6; iX++) {   %>
      <tr align="center"> 
        <% if("LIND".equals(CFG.EIRB_MODE)) {  %>
	        <td width="15%" style="display:none"> 
	          <input type=text name="verNumber<%=iX%>" size="15" >
	        </td>			
	        <td width="15%" align="center" style="display:none"><input type="text" name="versionDate<%=iX%>" class="datefield" readonly size=15   maxlength=20 value=''></td>
         <% } else { %>
         	 <td width="auto"> 
	          <input type=text name="verNumber<%=iX%>" size="15" >
	        </td>
			<%-- INF-20084 Datepicker-- AGodara --%>
	        <td width="auto" align="center"><input type="text" name="versionDate<%=iX%>" class="datefield" readonly size=15   maxlength=20 value=''></td>
	    <% } %>
	    <td width="auto"><%=ddStudyvercat.replaceAll("dStudyvercat","dStudyvercat"+iX)%></td>
        <td width="auto"><%=ddStudyvertype.replaceAll("dStudyvertype","dStudyvertype"+iX)%></td>
        <td width="auto"> 
	
          <input type=file name="name<%=iX%>" size="30" >
 	
        </td>
        <td width="auto"> 
          <input type=text name="desc<%=iX%>" size="30" > </TextArea>
        </td>
      </tr>
   <% } %>
    </table>
    <input type="hidden" name="type" value='file'>
    <input type="hidden" name="study" value=<%=stId%>>
    <input type="hidden" name="studyId" value=<%=stId%>>
    <input type="hidden" name="studyVer" value=<%=studyVerId%>>	
	
    <input type=hidden name=db value='eres'>
	<input type=hidden name=module value='study'>		  
	<input type=hidden name=tableName value='ER_STUDYAPNDX'>
	<input type=hidden name=columnName value='STUDYAPNDX_FILEOBJ'>
	<input type=hidden name=pkValue value='0'>
	<input type=hidden name=pkColumnName value='PK_STUDYAPNDX'>
	<input type=hidden name=maxFileSize value=<%=freeSpace%>>
	<input type=hidden name=accMaxStorage value=<%=accMaxStorage%>> 
	<input type=hidden name=nextPage value='../../velos/jsp/studyVerBrowser.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&studyId=<%=stId%>'>
	<input type=hidden name=successPage value='../../velos/jsp/studyVersuccessPage.jsp?'>
    <BR>

	<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="appFileMultiFrm"/>
		<jsp:param name="showDiscard" value="N"/>
	</jsp:include>
  </form>
  <% if("LIND".equals(CFG.EIRB_MODE)) { %>
  <script>
  var formobj = document.upload;
  for (var iX=1; iX<6; iX++) {
	  getElementByID(formobj, "verNumber"+iX).value = '';
	  getElementByID(formobj, "verNumber"+iX).readOnly = 'readOnly';
	  getElementByID(formobj, "verNumber"+iX).style.backgroundColor = '#E1E1E1';
  }
  </script>
  <% }  %>
  <%

}//end of if body for session

else

{

%>
  <jsp:include page="timeout_childwindow.jsp" flush="true"/>
  <%

}

%>
<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</div>
</body>

</html>

