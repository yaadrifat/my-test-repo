<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="calendar.js"></SCRIPT>
<jsp:include page="skinChoser.jsp" flush="true"/>

<SCRIPT  Language="JavaScript1.2">
</SCRIPT>

</HEAD>


<BODY>
  <jsp:useBean id="PayB" scope="request" class="com.velos.eres.web.milepayment.PaymentDetailJB"/>

  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
  <%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*"%>
  <%

	String src = null;

	
	String paymentPk = request.getParameter("paymentPk");
	String pageRight = request.getParameter("pR");
	String studyId = request.getParameter("studyId");
	String nextPage = "";
	nextPage = request.getParameter("nextPage");
	
	if (StringUtil.isEmpty(nextPage))
	{
		nextPage = "linkInvPayBrowser.jsp";
	}
	
	
HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession))

   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%   
   	String oldESign = (String) tSession.getValue("eSign");
    String eSign = request.getParameter("eSign");
    
  if(!(oldESign.equals(eSign))) {

	%>
	   	<jsp:include page="incorrectesign.jsp" flush="true"/>
	<%
  } else  {

		
		String ipAdd = (String) tSession.getValue("ipAdd");
		String usr = (String) tSession.getValue("userId");
		
		String arApplyAmount[] = null;
		String arApplyHoldbackAmount[] = null;
		String arLevel1Id[] = null;
		String arLevel2Id[] = null;
		String arLinkType[] = null;
		String arLinktoId[] = null;
		String arPkIds[] = null;
		String mileDetFKAchieveds[] = null;
		String arrholdbackFlag[]=null;
		 
		
		int count = 0;
		
		String applyAmount = "";
		String applyHoldbackAmount = "";
		String level1Id = "";
		String level2Id = "";
		String linkType = "";
		String linktoId = "";
		String pkId = "";
		String mileDetFKAchieved="";
	
		
		InvoiceDetailDao inv = new InvoiceDetailDao();
		
		count = EJBUtil.stringToNum(request.getParameter("totalCount"));
		
		
		
		if (count > 1)
		{
			
			arApplyAmount = request.getParameterValues("applyAmount");
			arApplyHoldbackAmount = request.getParameterValues("applyHoldback");
			arLevel1Id = request.getParameterValues("level1Id")	;
			arLevel2Id = request.getParameterValues("level2Id")	;
			arLinkType = request.getParameterValues("linkType")	;
			arLinktoId = request.getParameterValues("linktoId")	;
			arPkIds = request.getParameterValues("oldpk")	;
			mileDetFKAchieveds= request.getParameterValues("mileDetFKAchieved")	;
			arrholdbackFlag=request.getParameterValues("holdback");
	
		}
		else
		{
		
	
			
			applyAmount = request.getParameter("applyAmount");
			
			arApplyAmount = new String[1];
			arApplyAmount[0] = applyAmount;
			
			applyHoldbackAmount = request.getParameter("applyHoldback");
			arApplyHoldbackAmount = new String[1];
			arApplyHoldbackAmount[0] = applyHoldbackAmount;
			
			level1Id = request.getParameter("level1Id")	;
			
			arLevel1Id = new String[1];
			arLevel1Id[0] = level1Id ;
			
			level2Id = request.getParameter("level2Id")	;
			
			arLevel2Id = new String[1];
			arLevel2Id[0] = level2Id ;
		    
		    
			linkType = request.getParameter("linkType")	;
	   		
			arLinkType = new String[1];
			arLinkType[0] = linkType;
	   		
			linktoId = request.getParameter("linktoId")	;
	
			arLinktoId  = new String[1];
			arLinktoId[0] = linktoId ;
			
			pkId = request.getParameter("oldpk")	;
	
			arPkIds = new String[1];
			arPkIds[0] = pkId;
			
			mileDetFKAchieveds = new String[1];
			mileDetFKAchieved = request.getParameter("mileDetFKAchieved")	;
			mileDetFKAchieveds[0] = mileDetFKAchieved;
			arrholdbackFlag= new String[1];
			arrholdbackFlag[0]=request.getParameter("holdback");
			
		}
		
		PaymentDetailsDao pd  = new PaymentDetailsDao();
		String mileAchStr = "";
		
			for (int i = 0; i< count; i++)
			{
				 pd.setAmount(arApplyAmount[i]); 
				 pd.setFkPayment(paymentPk) ;
				 pd.setLinkedTo(arLinkType[i] ) ;
				 pd.setLinkToId(arLinktoId[i]) ;
				 pd.setIPAdd(ipAdd) ;
				 pd.setCreator(usr);
				 pd.setLastModifiedBy(usr);
				 pd.setLinkToLevel1(arLevel1Id[i]);
				 pd.setLinkToLevel2(arLevel2Id[i]);
				 pd.setId(arPkIds[i]);
				 if(arApplyHoldbackAmount != null)
				 {
				 pd.setHoldBackAppliedDueToDateList(Float.parseFloat(arApplyHoldbackAmount[i]));
				 }
				 if(arrholdbackFlag != null)
				 {
					 pd.setHoldBackFlag(arrholdbackFlag[i]);
				 }
				 if (mileDetFKAchieveds != null)
				 {
				 	mileAchStr = mileDetFKAchieveds[i];
				 }	
				 
				 if (StringUtil.isEmpty(mileAchStr))
				 {
				 	mileAchStr = "";
				 }
				 pd.setLinkMileAchieved(mileAchStr);
				  
			}
		
		
		
		int ret = 0;
		ret = PayB.setPaymentDetail(pd);
		 
		if (ret > 0)
		{
		
		
	%>
	<BR><BR><BR><BR><BR>
	<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully.*****--%> </p>
		
		<META HTTP-EQUIV=Refresh CONTENT="1; URL=<%=nextPage%>?paymentPk=<%=paymentPk%>&studyId=<%=studyId%>&pR=<%=pageRight%>">	
	
		<%
		} %>
	
	<%

	} // for e-sign
}//end of if body for session

else
{
%>
 <jsp:include page="timeout_childwindow.jsp" flush="true"/> 
  <%
}
%>

</BODY>

<script>

</script>

</HTML>





