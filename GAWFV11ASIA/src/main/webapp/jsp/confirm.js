<!-- Functions for Client Side Validations-->
 var Etr_MandantoryFlds=M_Etr_MandantoryFlds;
 var Etr_ValidDate=M_Etr_ValidDate;
 var Etr_ValidData=M_Etr_ValidData;
 var New_PermissionDenied=L_New_PermissionDenied;
 var Edit_PermDenied=L_Edit_PermDenied;
 var View_PermissionDenied=M_View_PermissionDenied;
 var whitespace = " \t\n\r";



 

 function validate_col(name,column){

        value=column.value

            if(value==''||value==null){

                /*alert("Please enter data in all mandatory fields")*****/
				 alert(Etr_MandantoryFlds)

                column.focus()

                return false;

            }else if (isWhitespace(value) == true){

                /*alert("Please enter valid data")*****/
				  alert(Etr_ValidData)

                column.focus()

                return false;

            }else {

                return true;

            }

    }

    

function isEmpty(s){

	return ((s == null) || (s.length == 0))

}



function isWhitespace(s){

    if (isEmpty(s)) return true

	

    // Search through string's characters one by one

    for (var i = 0; i < s.length; i++) {   

        var c = s.charAt(i)

        if (whitespace.indexOf(c) == -1) return false

    }

    // All characters are whitespace.

    return true

}



    function isDigit (as_char)

    {

        if(as_char == ' ')

        {

            return false;

        }

        

        if ((as_char >=0) && (as_char <= 9)){

            return true; 

        }

         else {

            return false; }

    }

    

    function isInteger(as_str)

    {

        if (as_str =='') {

            return true;

        }

        for (i=0;i < as_str.length;i++)

        {

            var c = as_str.charAt(i) ;

            if (!isDigit(c)) return false;

        }

        return true;

    }    





    function validate_date(date_col){

        date=date_col.value

        if(date==null || date=='') return true

        

        Month = date.substring(0,date.indexOf("/"));

        Day = date.substring(date.indexOf("/") + 1,date.lastIndexOf("/"));

        Year= date.substring(date.lastIndexOf("/") + 1,date.length);

       

        if(Month > 0 && Month <= 12 && Day > 0 && Day <= 31 && Year > 999 && Year <= 9999){

                if (Month==1 || Month==3 || Month==5 || Month==7 || Month==8 || Month==10 || Month==12){

                    validdays=31 

                }else if (Month==4 || Month==6 || Month==9 || Month==11){

                    validdays=30

                }else if (Month==2)  {

                    if (((Year % 4)==0) && ((Year % 100)!=0) || ((Year % 400)==0)) {

                        validdays=29;

                    }

                     else {

                      validdays=28;

                    }

                }else{

                    validdays=0

               }

             if(Day > validdays || Day <= 0){

                 /*alert('Please enter valid date')*****/
				 alert(Etr_ValidDate)

                 date_col.focus()

                 return false

             }

        }else{

             /*alert('Please enter valid date')*****/
				 alert(Etr_ValidDate)

            date_col.focus()

            return false

        }

    return true

    }



    //by sonia sahni to compare 2 dates

    function CompareDates(dt1,dt2,operator)

    {

        

        Month1 = new Number(dt1.substring(0,dt1.indexOf("/")));

        Day1 = new Number(dt1.substring(dt1.indexOf("/") + 1,dt1.lastIndexOf("/")));

        Year1= new Number(dt1.substring(dt1.lastIndexOf("/") + 1,dt1.length));

       

        Month2 = new Number(dt2.substring(0,dt2.indexOf("/")));

        Day2 = new Number(dt2.substring(dt2.indexOf("/") + 1,dt2.lastIndexOf("/")));

        Year2= new Number(dt2.substring(dt2.lastIndexOf("/") + 1,dt2.length));

        

        if (operator == '>')

        { 

            //CHECK FOR YR 

            if (Year1 < Year2)

            {

                return false;

            }

            else if (Year1 > Year2)

            {

                return true;

            }

            

            if (Month1 < Month2)

            {

                return false;

            }

            else if (Month1 > Month2)

            {

                return true;

            }

            

            if (Day1 <= Day2)

            {

                return false;

            }



         }

        

      return true

    }



    //by sonia sahni : compares two Date objects for '>' operator ; date: 18 jan 2001

      function compareDateObjects(dt1,dt2,operator)

    {

     

        Month1 = dt1.getMonth() ;

        Day1 = dt1.getDate();

        Year1= dt1.getFullYear();

       

        Month2 = dt2.getMonth() ;

        Day2 = dt2.getDate();

        Year2 = dt2.getFullYear();

        

        

        if (operator == '>')

        { 

            //CHECK FOR YR 

            if (Year1 < Year2)

            {

                return false;

            }

            else if (Year1 > Year2)

            {

                return true;

            }

            

            if (Month1 < Month2)

            {

                return false;

            }

            else if (Month1 > Month2)

            {

                return true;

            }

            

            if (Day1 <= Day2)

            {

                return false;

            }



         }

        

      return true; //this means date 1 is greater than date 2

    }





function f_check_perm(pageRight,perm){
	if (perm == "N") { //check whether logon user has New Permission

		if ((pageRight == "7") || (pageRight == "1") || 

			(pageRight == "3") || (pageRight == "5")){

			return true;

		}else{

			/*alert("New Permission Denied");	*****/
			alert(New_PermissionDenied);	

			return false;

		}

	}else if (perm == "E") { //check whether logon user has New Permission

		if ((pageRight == "7") || (pageRight == "2") || (pageRight == "3") 

			|| (pageRight == "6")){

			return true;

		}else{

			/*alert("Edit Permission Denied");*****/	
			alert(Edit_PermDenied);	

			return false;

		}

	}else if (perm == "V") { //check whether logon user has New Permission

		if ((pageRight == "7") || (pageRight == "4") || (pageRight == "5") 

			|| (pageRight == "6")){

			return true;

		}else{

			/*alert("View Permission Denied");	*****/
			alert(View_PermissionDenied);

			return false;

		}

	}

}

   