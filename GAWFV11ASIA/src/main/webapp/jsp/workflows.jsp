<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*,com.velos.eres.service.util.*"%>
<%@ page import="com.velos.eres.gems.business.*, org.json.JSONObject"%>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="formjs.js"></SCRIPT>
<%HttpServletResponse httpResponse = (HttpServletResponse) response;
httpResponse.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1
httpResponse.setHeader("Pragma", "no-cache"); // HTTP 1.0
httpResponse.setDateHeader("Expires", 0); %>
<style>
tr.spaceUnder > td
{
  padding-bottom: 0.50em !important;
}
</style>
<script>
var workflowFunctions = {
	workflowInstances: [],
	createWorkflowDialog: {}
};

var workflowBarFunctions = {
		createWorkflowDialogBar: {}
	};

	workflowBarFunctions.createWorkflowDialogBar = function (){
		//alert("Inside createWorkflowDialogBar")
		var workflowInstanceId = 0, prevWorkflowInstanceId = 0;
		var instanceCount = (workflowFunctions.workflowInstances).length;
		//alert("instanceCount:::"+instanceCount);
		var screenHeight = screen.height - 50;
		var wfDialogHeight = screenHeight/instanceCount;

		for (var indx=0; indx < instanceCount; indx++){
			//alert("Array size::"+workflowFunctions.workflowInstances.length);
			workflowInstanceId = workflowFunctions.workflowInstances[indx];
			prevWorkflowInstanceId = (indx == 0)? 0 : workflowFunctions.workflowInstances[indx-1];
			//alert("workflowInstanceId:::"+workflowInstanceId);
			//$j('#workflowTasks'+ workflowInstanceId +'Div').css('max-height', wfDialogHeight+'px');

			$j('#workflowTasks'+ workflowInstanceId +'Div').accordion('destroy')
			    .accordion({
					header: "> div > h3",
					autoHeight: false, collapsible: false, active: true, fillSpace: false, animated: false
			});
				
			//alert("Length of Section Header::::"+$j('#sectionHead'+ workflowInstanceId).length());
			//$j('#sectionHead'+ workflowInstanceId).click();
			$j('#workflowTasks'+ workflowInstanceId +'Div > div > h3').addClass("init");
			//$j('#workflowTasks'+ workflowInstanceId +'Div > div > h3').click();
			getToggleDiv($j('#workflowTasks'+ workflowInstanceId +'Div > div > h3'));
			/* $j('#workflowTasks'+ workflowInstanceId +'Div > div > h3').bind("click",function(){
				//alert("Inside Click");
				getToggleDiv($j('#workflowTasks'+ workflowInstanceId +'Div > div > h3'));
			}); */
			
		}
		return false;
	};
function getToggleDiv(el){
	//alert("Inside fn");
	$j(el).unbind("click");
	if($j(el).find("span").hasClass("ui-icon-triangle-1-s")){
		//alert("Inside if");
		
		$j(el).find("span").removeClass("ui-icon-triangle-1-s");
		$j(el).find("span").addClass("ui-icon-triangle-1-e");
		$j(el).next("div").hide().slideUp();
	}else if($j(el).find("span").hasClass("ui-icon-triangle-1-e")){
		$j(el).find("span").removeClass("ui-icon-triangle-1-e");
		$j(el).find("span").addClass("ui-icon-triangle-1-s");
		$j(el).next("div").show().slideDown();
	}
}
/* workflowFunctions.createWorkflowDialog = function (workflowInstanceId, prevWorkflowInstanceId){
	if (prevWorkflowInstanceId == 0){
		$j('#workflowTasks'+ workflowInstanceId +'Div').dialog({
			width: $j('#workflowTab').width()-1,
			minHeight: 50,
			closeOnEscape: false,
			resizable: false,
			closeText:'',
			open: function(event, ui) { $j(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').hide(); }
		}).dialog("widget").position({
			my: 'left top', 
			at: 'left top',
			of: $j('#workflowTab')
		});
	} else {
		$j('#workflowTasks'+ workflowInstanceId +'Div').dialog({
			width: $j('#workflowTab').width()-1,
			minHeight: 50,
			closeOnEscape: false,
			resizable: false,
			closeText:'',
			open: function(event, ui) { $j(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').hide(); }
		}).dialog("widget").position({
			my: 'top', 
			at: 'bottom',
			of: $j('#workflowTasks'+prevWorkflowInstanceId+'Div')
		});
		
	}
	return false;
}; */

function compressedText(elTxt){
	var screenWidth = screen.width;
	var screenHeight = screen.height;
	
	var compText = "";
	if(screenWidth>1280 || screenHeight>1024){
		
		if(elTxt!=null && elTxt.length>20){
			compText = elTxt.substring(0,16)+"...";
		}else{
			
			compText = elTxt; 
		}
	}else{
		if(elTxt!=null && elTxt.length>16){
			compText = elTxt.substring(0,12)+"...";
		}else{
			compText = elTxt;
		}
	}
	return compText;
}

</script>
<%
	String entityId = (String)request.getParameter("entityId");
	String workflow_types = (String)request.getParameter("workflowTypes");
	ArrayList<Integer> wfInstanceIds = new ArrayList<Integer>();
	if (StringUtil.isEmpty(workflow_types) && StringUtil.isEmpty(workflow_types)){
		return;
	}
	
	String wfPageId = (String)request.getParameter("wfPageId");

	String [] workflowTypes = (StringUtil.isEmpty(workflow_types))? 
			null : workflow_types.split(",");

	if (workflowTypes.length <= 0) return;

	String moreParams = (String)request.getParameter("params");
	
	
	Integer prevWorkflowInstanceId = 0;

	if ("Y".equals(CFG.Workflows_Enabled)) {

		for(int wfIndx=0; wfIndx < workflowTypes.length; wfIndx++){
			String workflowType = workflowTypes[wfIndx];
			if (StringUtil.isEmpty(workflowType)) continue;

			if (StringUtil.stringToNum(entityId) < 0) continue;
			
			WorkflowJB workflowJB = new WorkflowJB();
			Workflow wf = new Workflow();
			wf = workflowJB.findWorkflowByType(workflowType);
			
			if (null == wf) {return;}
			
			WorkflowInstanceJB workflowInstanceJB = new WorkflowInstanceJB();
			Integer result = 0;
			String nextDestination = "";
			WorkflowInstance workflowInstance = workflowInstanceJB.getWorkflowInstanceStatus(entityId, workflowType);

			Integer workflowInstanceId = (null == workflowInstance)? 0 : workflowInstance.getPkWfInstanceId();
			
			Integer workflowInstanceStatus = (null == workflowInstance)? 0 : workflowInstance.getWfIStatusFlag();
			workflowInstanceStatus = (null == workflowInstanceStatus)? 0 : workflowInstanceStatus;
	
			if (workflowInstanceId == null || workflowInstanceId == 0) {
				/* workflowInstanceId = workflowInstanceJB.startWorkflowInstance(entityId, workflowType);

				if (StringUtil.stringToNum(entityId) > 0){
					workflowInstanceJB.updateWorkflowInstance(entityId, workflowType, moreParams);
					if (workflowInstanceId != null || workflowInstanceId != 0) {
						//nextDestination = workflowInstanceJB.fetchNextTask(entityId, workflowType);
					}
				} */
			} else {
				wfInstanceIds.add(workflowInstanceId);
				if (StringUtil.stringToNum(entityId) > 0){
					workflowInstanceJB.updateWorkflowInstance(entityId, workflowType, moreParams);
					//nextDestination = workflowInstanceJB.fetchNextTask(entityId, workflowType);
				}
			}
	
			ArrayList workflowTasksList = new ArrayList();
			workflowTasksList = workflowInstanceJB.getWorkflowInstanceTasks(workflowInstanceId);

			int divZIndex = (10000*(wfIndx+1));
			int activeTasks =0;
			HashMap<String, Object> checkrecord = new HashMap<String, Object>();
			for (int taskIndx = 0; taskIndx < workflowTasksList.size(); taskIndx++){
				checkrecord = (HashMap<String, Object>) workflowTasksList.get(taskIndx);
				String completeFlag = (String)checkrecord.get("TASK_COMPLETEFLAG");
				if(completeFlag.equals("0") || completeFlag.equals("1")){
					System.out.println("Inside If:::");
					activeTasks++;
				}
			}
			if(activeTasks==0){
				continue;
			}
			
		%>
		
		<%-- Start of Task modal dialog --%>
		<div id="workflowTasks<%=workflowInstanceId%>Div" style="bottom:10px;" class="taskCls">
		<div id="sectionHead<%=workflowInstanceId%>Div" class="group">
		<h3 id="sectionHead<%=workflowInstanceId%>" onclick="getToggleDiv(this);">
		
		
		<a id="sectionAnchor<%=workflowInstanceId%>" href="#" style="font-size:13px;"><%=wf.getWorkflowName()%></a>
		
		
		</h3>
		<%-- Start of Task modal dialog --%>
		<div id="workflowAccordion<%=workflowInstanceId%>Div" title="<%=wf.getWorkflowName()%>" class="" style="display:none;">
			<div id='workflowTasks_EmbedDataHere'>
			<div style="top:10px;"><span>&nbsp;</span></div>
				<table class="workflowTasks" width="99%">
				<%
				int taskId = 0;
				String taskCompleteFlag = "0";
				HashMap<String, Object> record = new HashMap<String, Object>();
				for (int taskIndx = 0; taskIndx < workflowTasksList.size(); taskIndx++){
			        record = (HashMap<String, Object>) workflowTasksList.get(taskIndx);
			        taskId = StringUtil.stringToNum(""+record.get("PK_TASKID"));
			        if (taskId <= 0) continue;

			        %>
			        <%-- <%
			        	if(taskIndx==0){
			        %>
			        		<br/>
			        <%
			        	}
			        %> --%>
			        <!-- <td><a href='#' onclick="workflowFunctions.goToWFTask(<%=record.get("PK_TASKID")%>)">GoTo</a> -->
			        <%
				    taskCompleteFlag = (String)record.get("TASK_COMPLETEFLAG");
					if ("1".equals(taskCompleteFlag)){
				    %>
				       	<tr class="spaceUnder">
				       		<td id="flowNameHolder<%=taskId%>" width="80%" nowrap>&nbsp;<a id="wrkFlowName<%=taskId%>" title="<%=record.get("WA_NAME")%>" style="color:#696969;"><%=record.get("WA_NAME")%></a></td>
				       		<% if(StringUtil.isEmpty((String)record.get("WA_ONCLICK_JSON"))) {			        		
			        		 %>
				       			<td>&nbsp;<a href="javascript:void(0);"><img id="taskImg<%=taskId%>" style="position:absolute" class="headerImage" align="bottom" src="images/greencheck.png" border="0" title="<%=LC.L_Done%>" alt="<%=LC.L_Done%>" /></a></td>
				       		<% } else { %>
				       			<td>&nbsp;<a href="javascript:void(0);"><img id="taskImg<%=taskId%>" style="position:absolute" class="headerImage" align="bottom" src="images/greencheckform.png" border="0" title="<%=LC.L_Done%>" alt="<%=LC.L_Done%>" /></a></td>
				       		<% }  %>
				       	</tr>
						<%
				        String onclickJSONStr = (String)record.get("WA_ONCLICK_JSON");
						String onclickStr = "";
				        if (!StringUtil.isEmpty(onclickJSONStr)){
					        onclickStr = WorkflowUtil.getOnclickJS(session, wfPageId, taskId, onclickJSONStr);
					        %>
					        <%if (!StringUtil.isEmpty(onclickStr)){%>
								<script>
								<%=onclickStr%>
								</script>
							<%}%>
				        <%}%>
				    <%}%>
					<%if ("0".equals(taskCompleteFlag)){%>
			        	<tr class="spaceUnder">
			        		<td id="flowNameHolder<%=taskId%>" width="80%">&nbsp;<a id="wrkFlowName<%=taskId%>" title="<%=record.get("WA_NAME")%>" style="color:#696969;"><%=record.get("WA_NAME")%></a></td>
			        		<% if(StringUtil.isEmpty((String)record.get("WA_ONCLICK_JSON"))) {			        		
			        		 %>
			        			<td>&nbsp;<a href="javascript:void(0);"><img id="taskImg<%=taskId%>" style="position:absolute" class="headerImage" align="bottom" src="images/exclamation.png" border="0" title="<%=LC.L_Not_Done%>" alt="<%=LC.L_Not_Done%>"/></a></td>
			        		<% } else { %>
			        			<td>&nbsp;<a href="javascript:void(0);"><img id="taskImg<%=taskId%>" style="position:absolute" class="headerImage" align="bottom" src="images/exclamationform.png" border="0" title="<%=LC.L_Click_Here%>" alt="<%=LC.L_Not_Done%>"/></a></td>
			        		<% }  %>
			        	</tr>
						<%
				        String onclickJSONStr = (String)record.get("WA_ONCLICK_JSON");
						String onclickStr = "";
				        if (!StringUtil.isEmpty(onclickJSONStr)){
					        onclickStr = WorkflowUtil.getOnclickJS(session, wfPageId, taskId, onclickJSONStr);
					        %>
					        <%if (!StringUtil.isEmpty(onclickStr)){%>
								<script>
								<%=onclickStr%>
								</script>
							<%}%>
				        <%}%>
					<%}%>
					<%if ("-2".equals(taskCompleteFlag)){%>
			        	<tr class="spaceUnder">
			        		<td id="flowNameHolder<%=taskId%>" width="80%">&nbsp;<a id="wrkFlowName<%=taskId%>" title="<%=record.get("WA_NAME")%>" style="color:#696969;"><%=record.get("WA_NAME")%></a></td>
			        		<td>&nbsp;<a href="javascript:void(0);"><img id="taskImg<%=taskId%>" style="position:absolute" class="headerImage" align="bottom" src="images/More_info_icon_grey_ square_24x24.png" border="0" title="<%=LC.L_Required_Later%>" alt="<%=LC.L_Required_Later%>"/></a></td>
			        	</tr>
					<%}%>
					<script>
						var currtaskId=<%=taskId%>;
						
						$j("#wrkFlowName"+currtaskId).html(compressedText($j("#wrkFlowName"+currtaskId).html()));
						
			    	</script>
			    <%}%>
			    
				</table>
				<div style="top:10px;"><span>&nbsp;</span></div>
			</div>
			<input type='hidden' value="<%=entityId%>" id='wfEntityId<%=workflowInstanceId%>'></input>
			<input type='hidden' value="<%=workflowType%>" id='workflowType<%=workflowInstanceId%>'></input>
			<input type='hidden' value="<%=moreParams%>" id='moreParams<%=workflowInstanceId%>'></input>
		</div>
		<%-- End of Task modal dialog --%>
		
		<script>
			var len = (workflowFunctions.workflowInstances).length;
			workflowFunctions.workflowInstances[len] = <%=workflowInstanceId%>;
			
		</script>
		<%
		prevWorkflowInstanceId = workflowInstanceId;
		%>
		</div>
	</div>
	<br>
	<%} %>
	
	<%-- <% if(wfInstanceIds.size()>0){ 
	System.out.println("Inside if wf ids::::");
	%>
			<script>
			$j(document).ready(function(){
				<% for(int wiIndex=0;wiIndex < wfInstanceIds.size();wiIndex++){ %>
						var tempWfId = <%=wfInstanceIds.get(wiIndex)%>;
						/* alert("Inside loop tempWfId::"+tempWfId);
						alert("Length::::"+$j("#workflowTasks"+tempWfId+"Div").length());
						$j("#sectionHead"+tempWfId+"Div").click();
						$j("#workflowTasks"+tempWfId+"Div").append("<br/>"); */
				<% } %>
			});
			</script>
	<% } %> --%>
	<script>
	$j(document).ready(function() {
		workflowBarFunctions.createWorkflowDialogBar();
		<%--workflowFunctions.createWorkflowDialog(<%=workflowInstanceId%>, <%=prevWorkflowInstanceId%>);--%>
		/* $j(".BrowserBotN").css("left","14%");
		 $j(".BrowserBotN").css("width","85%"); 
		 $j(".BgtDivBottom").css("left","14%");
		 $j(".BgtDivBottom").css("width","85%"); */
		 
	});
	</script>
<%}%>
