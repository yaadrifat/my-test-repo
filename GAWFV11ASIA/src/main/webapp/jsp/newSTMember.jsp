<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<jsp:include page="velos_includes.jsp"></jsp:include>
<html>
<head>
 			<style>
			 table.basetbl 
			 td {width:60%;}
			 table.basetbl  
			 td:first-child { width:40%; }
			 </style>



<title>
	<%=MC.M_Study_Details%>
</title>



</head>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
 <SCRIPT Language="javascript">
	 
</SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id="teamB" scope="session" class="com.velos.eres.web.team.TeamJB"/>


<%@ page language = "java" import = "com.velos.eres.business.common.*,com.aithent.file.uploadDownload.*,java.util.*,com.velos.eres.business.user.impl.*,com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,java.util.*,java.io.*,org.w3c.dom.*"%>
<%@ page import="com.velos.eres.service.util.LC" %><%@ page import="com.velos.eres.service.util.MC" %>

<% String src;

//src= request.getParameter("srcmenu");


int ienet = 2;

String agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1)
     ienet = 0; //IE
    else
	ienet = 1;
	if(ienet == 0) {
%>
<body style="overflow:auto;"> <!--KM-->
<%
	} else {
%>
<body style="overflow:auto;">
<%
	}
%>
<br>
<DIV class="popDefault" id="div1" style="overflow:auto;height:85%;" >

  <%

	HttpSession tSession = request.getSession(true);



	//String categoryName ="";
	//String categoryDesc ="";
	String mode=request.getParameter("mode");

	int pageRight = 4;
	String dAccSites="";

	//int catLibId=0;

	if (sessionmaint.isValidSession(tSession))
	{
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
	<jsp:include page="include.jsp" flush="true"/>
<%
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
	CodeDao cdRole= new CodeDao();
	if (pageRight >=4)
	{
		 String acc = (String) tSession.getValue("accountId");
		 String studyId= request.getParameter("studyId");
		 int teamId = StringUtil.stringToNum(request.getParameter("teamId"));
		 teamB.setId(teamId);
		 teamB.getTeamDetails();
		 String userName = "";
		 String UserRole = "";
		 userB.setUserId(StringUtil.stringToNum(teamB.getTeamUser()));
		 userB.getUserDetails();
		 userName = userB.getUserFirstName()+" "+userB.getUserLastName();
		 
		 UserRole = cdRole.getCodeDescription(StringUtil.stringToNum(teamB.getTeamUserRole()));
	
	 
	 	////	%>



  <Form name="studyteam" id="studyteam"  method="post" action="saveNSTMember.jsp">
<!--Mukul Bug: 4170 Fixed Form id was passing instead of Form name -->
  <table width="98%" cellspacing="1" cellpadding="1">
  	   <tr><td colspan="4" ><p class="sectionheadings"><%=MC.M_Study_Details%><%--Add/ Edit Organization Details*****--%></p> </td></tr>
      	<tr >
        <td style="padding-left: 60px;"><b><%=LC.L_UserName%><%--Organization*****--%></b></td>
        <td ><%=userName%></td>
        <td align="left"><b><%=LC.L_Role%><%--Type*****--%></b></td>
        <td ><%=UserRole%></td>
      </tr>
   </table>
<div id="stmMoreDetailsDIV" style="margin: 1pt;" class="portlet portletstatus ui-widget ui-widget-border ui-helper-clearfix ui-corner-all">
		<div id="stmDetailsTab2content" onclick="toggleDiv('stmDetailsTab2');" class="portlet-header portletstatus ui-widget 
		ui-widget-header ui-widget-content ui-helper-clearfix ui-corner-all" style="background-color:#CCCCCC;">
			<span class="ui-icon ui-icon-triangle-1-s"></span>
		<%=MC.M_StudyMember_Details%>
		</div>
		<div id='stmDetailsTab2'  width="99%">
			<jsp:include page="moredetails.jsp" >
				<jsp:param name="modId" value="<%=teamId%>"/>
				<jsp:param name="modName" value="studyteam"/>
				<jsp:param name="includeMode" value="Y"/>
			</jsp:include>
		</div>
		<br/>
		<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="N"/>
		</jsp:include>
</div>

<div style="align:center;">

</div>


<%//if((pageRight == 5 && mode.equals("N")) || (pageRight == 6 && mode.equals("M")) || pageRight == 7){%>

		  <%//}%>



<input type="hidden" name="mode" value="<%=mode%>">
<input type="hidden" name="teamId" value="<%=teamId%>">
<input type="hidden" name="studyId" value="<%=studyId%>">
    <input type="hidden" name="userId" id="userId" value="<%=teamB.getTeamUser()%>">
</Form>
  <%

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>

<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</DIV>
</body>

</html>


