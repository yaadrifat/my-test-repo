<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<Link Rel=STYLESHEET HREF="common.css" type=text/css>

</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
<jsp:useBean id="teamB" scope="request" class="com.velos.eres.web.team.TeamJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.MC"%>
<%

int ret = 0;

String teamId;

String role;

String src;



int totrows =0;

int count=0; 


src=request.getParameter("src");

String tab = request.getParameter("selectedTab");
String studyId = request.getParameter("study");

String eSign = request.getParameter("eSign");

HttpSession tSession = request.getSession(true); 

String ipAdd = (String) tSession.getValue("ipAdd");
String usr = null;
usr = (String) tSession.getValue("userId");


 if (sessionmaint.isValidSession(tSession))
   {	
%>
<jsp:include page="sessionlogging.jsp" flush="true"/> 

<%   
   	String oldESign = (String) tSession.getValue("eSign");


	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {
totrows = EJBUtil.stringToNum(request.getParameter("totrows"));
teamB.setModifiedBy(usr);
teamB.setIpAdd(ipAdd);
if (totrows > 1){
	String[] teamIds = request.getParameterValues("teamId");
	String[] roles = request.getParameterValues("role");
	String[] prevroles = request.getParameterValues("teamRole");
	System.out.print("prevroles =" +prevroles);

	for (count = 0;count < totrows;count++){
		if(!(prevroles[count].equals(roles[count]))){
			teamB.setId(EJBUtil.stringToNum(teamIds[count]));
			teamB.getTeamDetails();
			teamB.setTeamUserRole(roles[count]);
			teamB.setModifiedBy(usr);
			teamB.setIpAdd(ipAdd);
			teamB.updateTeam();	
		}
		}

}else{
	teamId = request.getParameter("teamId");
	role = request.getParameter("role");
	teamB.setId(EJBUtil.stringToNum(teamId));
	teamB.getTeamDetails();
	teamB.setTeamUserRole(role);
	teamB.updateTeam();
}
%>
<br>
<br>
<br>
<br>
<br>
<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%-- Data was Saved successfully.*****--%> </p>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=teamBrowser.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&studyId=<%=studyId%>">

<%
}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
</BODY>

</HTML>





