<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true" />
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
	<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<jsp:include page="include.jsp" flush="true"/>
	<jsp:useBean id="submB" scope="request" class="com.velos.eres.web.submission.SubmissionJB"/>
	<jsp:useBean id="submBoardB" scope="request" class="com.velos.eres.web.submission.SubmissionBoardJB"/>
	<jsp:useBean id="submStatusB" scope="request" class="com.velos.eres.web.submission.SubmissionStatusJB"/>

	<%@ page language="java" import="com.velos.eres.business.study.*,com.velos.eres.business.common.*,java.util.*,java.text.*,com.velos.eres.service.util.*,com.velos.eres.web.account.AccountJB,com.velos.esch.business.common.*,com.velos.eres.compliance.web.*"%>
	<%@ page import="com.velos.eres.web.study.StudyJB,com.velos.eres.web.studyId.StudyIdJB"%>
	<%@ page import="com.velos.eres.widget.business.common.UIFlxPageDao,com.velos.eres.widget.service.util.FlxPageArchive"%>
	<%@page import="com.velos.eres.web.intercept.InterceptorJB"%>
	<%@page import="com.velos.eres.web.studyVer.StudyVerJB,com.velos.eres.business.common.StudyVerDao"%>
	<%@page import="com.velos.eres.web.statusHistory.StatusHistoryJB"%>
	<jsp:useBean id="complianceJB" scope="request" class="com.velos.eres.compliance.web.ComplianceJB"/>
	<%!private static final String VERSION_STATUS = "versionStatus";
	private static final String LETTER_F = "F";
	private static final String ER_STUDYVER_TABLE = "er_studyver";%>

	<%
		HttpSession tSession = request.getSession(true);
		String accId = (String) tSession.getValue("accountId");
		int iaccId = EJBUtil.stringToNum(accId);
		String studyId = (String) tSession.getValue("studyId");
		String grpId = (String) tSession.getValue("defUserGroup");
		String ipAdd = (String) tSession.getValue("ipAdd");
		String usr = (String) tSession.getValue("userId");
		int iusr = EJBUtil.stringToNum(usr);

		String src = StringUtil.htmlEncodeXss(request.getParameter("srcmenu"));
		String selectedTab = StringUtil.htmlEncodeXss(request.getParameter("selectedTab"));
		String eSign = request.getParameter("eSign");
		if (sessionmaint.isValidSession(tSession)) {
	%>
	<jsp:include page="sessionlogging.jsp" flush="true"/>
	<%
		String oldESign = (String) tSession.getValue("eSign");
		if (!oldESign.equals(eSign)) {
	%>
			<jsp:include page="incorrectesign.jsp" flush="true"/>
	<%
		} else {
			HashMap<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("userId", (usr.equals(null) ? null : Integer.valueOf(usr)));
			paramMap.put("accountId", (accId.equals(null) ? null : Integer.valueOf(accId)));
			paramMap.put("studyId", (studyId.equals(null) ? null : Integer.valueOf(studyId)));

			// Implement Flex Study part here
			StudyJB studyJB = new StudyJB();
			studyJB.setId(StringUtil.stringToNum(studyId));
			studyJB.getStudyDetails();
			String defUserGroup = (String) tSession.getAttribute("defUserGroup");
			StudyIdJB studyIdJB = new StudyIdJB();
			StudyIdDao sidDao = new StudyIdDao();
			sidDao = studyIdJB.getStudyIds(StringUtil.stringToNum(studyId), defUserGroup);

	    	FlxPageArchive archive = new FlxPageArchive();
	    	String fullVersion = archive.createOrUpdateResubmissionDraft(studyJB, sidDao, paramMap);

		    double versionNum = Double.valueOf(fullVersion).doubleValue();
		    // Freeze all categories of this version of this study
		    archive.freezeAttachments(StringUtil.stringToNum(studyId), paramMap, versionNum);

	    	/* Add form locking code here. */

		    //Locking the Study as Version is frozen
		    StudyDao studyDao = new StudyDao();
		    studyDao.lockUnlockStudy(StringUtil.stringToNum(studyId), 1);
		    int recentSubmPk = submB.getNewAmendSubmissionByFkStudy(studyId);
		    if(recentSubmPk > 0){
			    EIRBDao eIrbDao = new EIRBDao();
			    eIrbDao.getReviewBoards(EJBUtil.stringToNum(accId),EJBUtil.stringToNum(grpId));
		        ArrayList boardNameList = eIrbDao.getBoardNameList();
		        ArrayList boardIdList = eIrbDao.getBoardIdList();
		        StringBuffer submitToBoards=new StringBuffer("");
		        for (int iBoard=0; iBoard<boardIdList.size(); iBoard++) {
			    	if ("on".equals(request.getParameter("submitTo"+iBoard))) { 
			    		if(submitToBoards.toString().equals("")){
			    			submitToBoards.append(boardIdList.get(iBoard));
			    		}else{
			    			submitToBoards.append(","+boardIdList.get(iBoard));	
			    		}
			    		
			    	}
			    }
		        if(!submitToBoards.toString().equals("")){
		        	eIrbDao.updateSubmitToBoard(recentSubmPk,submitToBoards.toString());
		        }
		    }
			String resultMsg = null;
			if (!"0".equals(fullVersion)) {
				resultMsg = MC.M_Data_SvdSucc/*"Data was saved successfully"*****/;
			} else {
				resultMsg = MC.M_Err_SavingData/*"There was an error while saving data"*****/;
	%>
	<br>
	<br>
	<br>
	<br>
	<p class="successfulmsg" align=center><%=resultMsg%></p>
	<META HTTP-EQUIV=Refresh
		CONTENT="3; URL=studyCheckNSubmit?mode=M&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>">
</BODY>
</HTML>
<%
				return; // An error occurred; let's get out of here
			}
			String nextScreen = "flexProtocolChangeLog";
%>
		 <br><br><br><br>
		 <p class = "successfulmsg" align = center><%=resultMsg%></p>
		 <META HTTP-EQUIV=Refresh CONTENT="1; URL=<%=nextScreen%>?mode=M&selectedTab=irb_check_tab&studyId=<%=studyId%>">
		<%
		}// end of if body for e-sign
	}//end of if body for valid session
	else {
%>
<jsp:include page="timeout.html" flush="true"/>
<%
	}
%>

</BODY>
</HTML>
