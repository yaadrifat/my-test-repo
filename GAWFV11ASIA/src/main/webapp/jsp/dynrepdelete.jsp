<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">



<title><%=LC.L_Del_RptTemplate%><%--Delete Report Template*****--%></title>

<%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>

<SCRIPT>

function  validate(formobj){

	if (!(validate_col('e-Signature',formobj.eSign))) return false



	<%-- if(isNaN(formobj.eSign.value) == true) {

	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/

	formobj.eSign.focus();

	return false;

   } --%>

}

</SCRIPT>



</head>
<jsp:include page="skinChoser.jsp" flush="true"/>

<%@page import="com.aithent.audittrail.reports.AuditUtils"%>
<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>

<jsp:useBean id="dynrep" scope="request" class="com.velos.eres.web.dynrep.DynRepJB"/>
<jsp:useBean id="dynrepdt" scope="request" class="com.velos.eres.web.dynrepdt.DynRepDtJB"/>
<jsp:useBean id="dynrepfltr" scope="request" class="com.velos.eres.web.dynrepfltr.DynRepFltrJB"/>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/> 



<% String src;

	src= request.getParameter("srcmenu");

%>



<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>  



<BODY> 

<br>



<DIV class="formDefault" id="div1">

<% 

	ArrayList repDeleteIds=new ArrayList();
	ArrayList repFltrIds=new ArrayList();
	String mode= "";
	String calStatus= "";
	String repId="";	
	DynRepDao dynDao=new DynRepDao();
	DynRepDao dynDaoFltr=new DynRepDao();
	
HttpSession tSession = request.getSession(true); 
 if (sessionmaint.isValidSession(tSession))	{ 	

		
		mode= request.getParameter("mode");
		int ret=0;
		repId=request.getParameter("repId");
		repId=(repId==null)?"":repId;		
		String delMode=request.getParameter("delMode");
		
		if (delMode==null) {
			delMode="Del";
%>

	<FORM name="deletedynrep" method="post" id="deletedynrepfrm" action="dynrepdelete.jsp" onSubmit="if (validate(document.deletedynrep)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<br><br>
	
	<TABLE width="98%" cellspacing="0" cellpadding="0" >
		<tr><td align="center">
		<b><%=MC.M_PlsEtrEsign_Del%><%--Please enter e-Signature to proceed with deletion*****--%> </b>
		</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
	
		</table>
			
<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="deletedynrepfrm"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>


	 <input type="hidden" name="repId" value="<%=repId%>">
 	 <input type="hidden" name="delMode" value="<%=delMode%>">
  	 <input type="hidden" name="srcmenu" value="<%=src%>">
   	 <input type="hidden" name="mode" value="<%=mode%>">

  	 		 	

	 

	</FORM>

<%

	} else {

			String eSign = request.getParameter("eSign");	

			String oldESign = (String) tSession.getValue("eSign");

			if(!oldESign.equals(eSign)) {

%>

	 		  <jsp:include page="incorrectesign.jsp" flush="true"/>	

<%

			} else {
   
     dynDao=dynrep.getReportDetails(repId);
    dynDaoFltr=dynrep.getFilter(repId);
    repDeleteIds=dynDao.getRepColId();
    repFltrIds=dynDaoFltr.getFltrIds();
    
    dynrep.setId(EJBUtil.stringToNum(repId));
    // Modified for INF-18183 ::: Raviesh
    dynrep.removeDynRep(AuditUtils.createArgs(session,"",LC.L_Adhoc_Queries));
    
    dynrepdt.removeAllCols(repDeleteIds);
    dynrepfltr.removeDynFilters(repFltrIds);
    dynrep.removeFilters(repFltrIds);
    
    
   
		

	%>	
	<br><br><br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_Data_DelSucc%><%--Data deleted successfully.*****--%> </p>
	<META HTTP-EQUIV=Refresh CONTENT="1; URL=dynrepbrowse.jsp?srcmenu=<%=src%>&selectedTab=1">

	<%	}
		} //end esign
		
	 }//end of if body for session 

else { %>

 <jsp:include page="timeout.html" flush="true"/> 

 <% } %>

 <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

  

</DIV>



<div class ="mainMenu" id = "emenu">

<jsp:include page="getmenu.jsp" flush="true"/> 

</div>



</body>

</HTML>





