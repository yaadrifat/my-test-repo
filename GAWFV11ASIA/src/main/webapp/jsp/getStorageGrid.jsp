<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.StringTokenizer"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="storageB" scope="request" class="com.velos.eres.web.storage.StorageJB"/>
<jsp:useBean id="codeB" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="StorageStatJB" scope="request" class="com.velos.eres.web.storageStatus.StorageStatusJB" />
<jsp:useBean id="codeLstB" scope="page" class="com.velos.eres.web.codelst.CodelstJB" />

<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*"%>
<head>
<style type="text/css">
 table.sample {
	border-width: medium;
	border-style: groove;
	border-color: gray;
	border-collapse: separate;
	background-color: white;
}
table.sample th {
	border-width: 1px;
	padding: 1px;
	border-style: solid;
	border-color: gray;
	background-color: white;

}
table.sample td {
	border-width: medium;
	padding: 1px;
	border-style: groove;
	border-color: gray;
	border-collapse: separate;
	}

.tableTd {
    font-family: Verdana, Arial, Helvetica,  sans-serif;
    color:black;
    font-size:10pt;
    text-align:center;
}
 </style>

</script>
<%!
// Declare variables and methods
final int totalWidth = 650;
final int totalWidth2D = 630;
final int labelWidth = 100;
final int labelWidth2D = 100;
final String imgHeight = "\"20\"";
final String imgWidth  = "\"20\"";

String formTypeDescriptionPerLevel(String inValue, ArrayList typeCodepks, ArrayList typeCodeDescs) {
    StringBuffer descripSB = new StringBuffer();
    String[] combo = inValue.split(",");
    for (int iX=0; iX<combo.length; iX++) {
        String parts[] = combo[iX].split("@");
        descripSB.append(parts[0]).append(" ").append(getPluralizedDescrip(parts[0], parts[1],
                typeCodepks, typeCodeDescs)).append("<BR />");
    }
    descripSB.delete(descripSB.length()-6, descripSB.length());
    return descripSB.toString();
}
String getPluralizedDescrip(String inNum, String inValue, ArrayList typeCodepks, ArrayList typeCodeDescs) {
    String storageSubtype = (String)typeCodeDescs.get(typeCodepks.indexOf(new Integer(EJBUtil.stringToNum(inValue))));
    if ("1".equals(inNum)) {
        return storageSubtype;
    }
    if ("Box".equals(storageSubtype)) return "Boxes";
    if ("Shelf".equals(storageSubtype)) return "Shelves";
    return storageSubtype+"s";
}
String convertIntToAlphaLabel(int in) {
    if (in < 1 || in > 702) return "";
    int quotient = (in-1) / 26 - 1;
    int remainder = (in-1) % 26;
    char cQuotient = (char)(65 + quotient);
    char cRemainder = (char)(65 + remainder);
    StringBuffer sb = new StringBuffer();
    if (quotient < 0) {
        sb.append(cRemainder);
    } else {
        sb.append(cQuotient).append(cRemainder);
    }
    return sb.toString();
}
// End of declaration
%>

<%
 HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession)) {

    String name = "";
    String id = "";
    // gridFor values => ST=storage; BR=browser (list of top pk's); SP=specimen; EM=embedded
    String gridFor = "";
	int cellWidth = 0;
	String viewSpecimen="";
	String seltPatient="";
	int cellWidthRemaining = 0;
	int numCellRemaining = 0;
	int numLevelsDisplayed = 0;
	String storageId = "";
	String availImagePath ="";
	boolean tableIs2D = false;
	String selectLocationStr="";
	String selectLocationScript = "";
	Integer storageType=null;      // storage type for search result
	String child = request.getParameter("child");
	String parent = request.getParameter("parent");
	name  = request.getParameter("name");
	id = request.getParameter("storageId");
	gridFor = request.getParameter("gridFor");
	String topStorageName = request.getParameter("topStorageName");
	String pkTopStorage = request.getParameter("pkTopStorage");
	String userStorageType = request.getParameter("storageType"); // user-specified storage type for search parameter
	String browserType = request.getParameter("browser");
	String pkStorageForExplorer = request.getParameter("pkStorage");
	String explorer = request.getParameter("explorer");
	String selStudy = request.getParameter("selStudy")==null?"":request.getParameter("selStudy");
	String spSrchLocFlag = request.getParameter("srchLocFlag");
	String showviewcheck = request.getParameter("showviewcheck")==null?"":request.getParameter("showviewcheck");
	spSrchLocFlag = (spSrchLocFlag==null)?"":spSrchLocFlag;
	String specimenType = request.getParameter("specimenType");
	String storageStatus = request.getParameter("storageStatus");
	String studyIdStr = request.getParameter("selStudyIds");
	String PKStorageID = request.getParameter("PKStorageID")==null?"":request.getParameter("PKStorageID");
	String form = request.getParameter("callingform")==null?"":request.getParameter("callingform");
	String storagePKFldName = request.getParameter("storagePKFldName")==null?"":request.getParameter("storagePKFldName");
	String locationFldName = request.getParameter("locationFldName")==null?"":request.getParameter("locationFldName");
	String coordXFldName= request.getParameter("coordXFldName")==null?"":request.getParameter("coordXFldName");
	String coordYFldName = request.getParameter("coordYFldName")==null?"":request.getParameter("coordYFldName");
	String orderBy           = request.getParameter("orderBy");
	String orderType         = request.getParameter("orderType");
	String storageview = request.getParameter("storageview")==null?"":request.getParameter("storageview");//flag for storage hierarchy view
	
	String ignoreTemplate="";

	int accId = EJBUtil.stringToNum((String) tSession.getValue("accountId"));

	// dispType: B = browser view (list of top pk's); G = grid veiw
	String dispType = "";

	dispType = request.getParameter("dispType");
	//System.out.println("dispType" + dispType);
	//System.out.println("parent" + parent+"*");

	if(!showviewcheck.equalsIgnoreCase("") || !storageview.equalsIgnoreCase("")) {
	
	if (StringUtil.isEmpty(browserType))
	{
	    browserType = "0";
	}

	if (StringUtil.isEmpty(gridFor))
	{
		gridFor = "SP"; //storage
	}

	if (!StringUtil.isEmpty(topStorageName))
	{
	    topStorageName = StringUtil.htmlEncode(topStorageName);
	}

	if (!StringUtil.isEmpty(pkTopStorage))
	{
	    pkTopStorage = StringUtil.htmlEncode(pkTopStorage);
	}


	CodeDao cdStatus = new CodeDao();
	CodeDao cdStorageType = new CodeDao();

	ArrayList codepks = new ArrayList();
	ArrayList codeSubtypes = new ArrayList();
	ArrayList codeCustom1s = new ArrayList();
	ArrayList codeDescs = new ArrayList();


	ArrayList typeCodepks = new ArrayList();
	ArrayList typeCodeSubtypes = new ArrayList();
	ArrayList typeCodeCustom1s = new ArrayList();
	int typePos = 0;


	//cdStatus = codeB.getCodelstData(accId,"storage_stat");

	cdStatus.getCodeValues("storage_stat");
	cdStorageType.getCodeValues("store_type");


	codeSubtypes =  cdStatus.getCSubType();
	codeCustom1s = cdStatus.getCodeCustom1();
	codepks = cdStatus.getCId();
	codeDescs  = cdStatus.getCDesc();

	typeCodepks = cdStorageType.getCId();
	typeCodeSubtypes = cdStorageType.getCSubType();
	typeCodeCustom1s = cdStorageType.getCodeCustom1();
	ArrayList typeCodeDescs = cdStorageType.getCDesc();

	//get PK for KIT
	int kitLoc = 0;
	int kitCodePk = -1;

	kitLoc = typeCodeSubtypes.indexOf("Kit");

	if (kitLoc > -1) //get PK
	{
		kitCodePk = ((Integer)typeCodepks.get(kitLoc)).intValue();
	}

	Hashtable htArgs = new Hashtable();

	if (!StringUtil.isEmpty(parent))
	{
		htArgs.put("parent",parent);
	}

	if (!StringUtil.isEmpty(id))
	{
		htArgs.put("id",id);
	}


	if (!StringUtil.isEmpty(name))
	{
		htArgs.put("name",name);
	}



	if (StringUtil.isEmpty(dispType))
	{
		dispType = "G"; //grid
	}
	//52

	if (dispType.equals("B"))
	{
		htArgs.put("orderby"," lower_parent_name, lower_storage_name ");
	    htArgs.remove("gridView");

		if ( gridFor.equals("SP") || gridFor.equals("EM"))
		{
			htArgs.put("ignoreTemplate","1");
		}
		if ( !StringUtil.isEmpty(userStorageType) ) {
		    htArgs.put("storageType", userStorageType);
		} else {
		    htArgs.remove("storageType");
		}
		
		if ( !StringUtil.isEmpty(specimenType) ) {
		    htArgs.put("specimenType", specimenType);
		} else {
		    htArgs.remove("specimenType");
		}
		
		if ( !StringUtil.isEmpty(storageStatus) ) {
		    htArgs.put("storageStatus", storageStatus);
		} else {
		    htArgs.remove("storageStatus");
		}
		if ( !StringUtil.isEmpty(studyIdStr) ) {
		    htArgs.put("selStudyIds", studyIdStr);
		} else {
		    htArgs.remove("selStudyIds");
		}
		
		if ( !StringUtil.isEmpty(PKStorageID) ) {
		    htArgs.put("PKStorageID", PKStorageID);
		} else {
		    htArgs.remove("PKStorageID");
		}
		
		
	}  else {
		htArgs.put("gridView", "Y");
	}

	if (explorer != null)
	{
	    if (child != null) {
		    htArgs.put("gridView", "Y");
	        htArgs.put("searchUpward", "Y");
		    htArgs.remove("ignoreTemplate");
		    htArgs.put("child", String.valueOf(EJBUtil.stringToNum(child))); // sanitize child
	    } else {
	        htArgs.remove("searchUpward");
	        htArgs.remove("child");
	    }
	}


	StorageDao sdao = storageB.searchStorage(htArgs,accId);
	ArrayList sChain = sdao.getStorageChain();
	ArrayList tdColors = new ArrayList();

	ArrayList pkStorages = new ArrayList();
	ArrayList fkStorages = new ArrayList();
    ArrayList storageIds = new ArrayList();
    ArrayList storageNames = new ArrayList();
    ArrayList coordxs = new ArrayList();
    ArrayList coordys = new ArrayList();
    ArrayList statusCodes = new ArrayList();
    ArrayList storageDim1 =  new ArrayList();
    ArrayList storageDim2 =  new ArrayList();
    ArrayList specAllocationCount = new ArrayList();
    ArrayList childAllocationCount = new ArrayList();
    ArrayList childCapsCount = new ArrayList();
    ArrayList storageCapsUnit = new ArrayList();
    ArrayList storageMultiSpec = new ArrayList();
    ArrayList parentStorageName = new ArrayList();
    ArrayList storageTypes = new ArrayList();
    ArrayList storageLevel = new ArrayList();

    int len = 0;
    int cX = 0;
    int oldCY = -1;
    int cY = 0;
    int pkStorage = 0;
    String storageName = "";
    Integer statusCode = null;
    String codeSubType = "";
    String codeColor = "";
    String statusDesc = "";
    int codePos = 0;
    int typepos=0;
    int dim1 = 0;
    int dim2 = 0;
    int specAllocation = 0;
    int childAllocation = 0;
    int childCapCount = 0;
    String parentLocation = "";
    String selectSpecimen = "&nbsp;&nbsp;&nbsp;";
    String selectPatient = "";
    String typeImagePath = "";
    Integer typestorage=null;
    String codeDescs1 ="";
   	
    pkStorages = sdao.getPkStorages();
    fkStorages = sdao.getFkStorages();
    storageIds =  sdao.getStorageIds();
    storageNames = sdao.getStorageNames();

    coordxs =	sdao.getStorageCoordinateXs();
    coordys =	sdao.getStorageCoordinateYs();
    statusCodes = sdao.getStorageStatus();
    storageDim1 = sdao.getStorageDim1();
    storageDim2 = sdao.getStorageDim2();
    specAllocationCount = sdao.getSpecAllocationCount();
    childAllocationCount = sdao.getChildAllocationCount();
    childCapsCount = sdao.getStorageCapNums();
    parentStorageName = sdao.getStorageParent();
    storageTypes=sdao.getFkCodelstStorageTypes();
    storageLevel = sdao.getStorageLevel();
    storageCapsUnit = sdao.getStorageCapUnits();
    storageMultiSpec = sdao.getStorageMultiSpec();
    int intLevel = 0;
    int oldIntLevel = -1;
    int vBorderW = "1".equals(browserType) ? 3 : 5; // 3 pts for IE; 5 pts for FireFox
    if (!"EM".equals(gridFor)) vBorderW = 6;
    len = pkStorages.size();

    if (explorer != null) {
        topStorageName = sdao.getTopStorageNameForExplorer();
    }

    //loop
    String tabclassname = "";

    if (dispType.equals("G"))
    {
    	tabclassname = "tableDefault";
    }
    else
    {
    	tabclassname="tableDefault";
    }

    if (! StringUtil.isEmpty(topStorageName) && dispType.equals("G")) {
        if((!StringUtil.isEmpty(parent) && !StringUtil.isEmpty(pkTopStorage) &&
                parent.equals(pkTopStorage)) || explorer != null)
    	{
		if(gridFor.equals("EM")){
				String fkCodeLstStorStat="";
				String storId="";
				String storName="";
				String storSubType="";
				String storDesc="";
				String addSpecimen="";
				String addPat="";
				String latestStoerageCode="";
				String paravailImagePath="";
				ArrayList specList=null;
				ArrayList patList=null;
				ArrayList fkCodeLstStorStats=null;
				StorageStatusDao strStatDao = StorageStatJB.getStorageStausValues(EJBUtil.stringToNum(parent));
				fkCodeLstStorStats = strStatDao.getFkCodelstStorageStatuses();
				storageB.setPkStorage(EJBUtil.stringToNum(parent));
				storageB.getStorageDetails();
				storName=storageB.getStorageName();
				storSubType=storageB.getFkCodelstStorageType();
				storDesc=codeLstB.getCodeDescription(EJBUtil.stringToNum(storSubType));
				storId=storageB.getStorageId();
				CodeDao cdStoageStat = new CodeDao();
				latestStoerageCode = cdStoageStat.getCodeSubtype(EJBUtil.stringToNum(fkCodeLstStorStat = ((fkCodeLstStorStats.get(0))==null)?"":(fkCodeLstStorStats.get(0)).toString()));
				sdao.getSpecimensAndPatients(EJBUtil.stringToNum(parent),accId);
				specList=sdao.getSpecimens();
				patList=sdao.getSpecimenPatients();
				if(!specList.isEmpty()){
				for(int i=0;i<specList.size();i++)
				{
				if(!StringUtil.isEmpty(addSpecimen))
				addSpecimen=addSpecimen+","+specList.get(i);
				else		
				addSpecimen=specList.get(i).toString();
				}
				}
				if(!patList.isEmpty()){
				if(EJBUtil.stringToNum(patList.get(0).toString())==0 && patList.size()==1){}
				else{
				for(int i=0;i<patList.size();i++)
				{
				if(!StringUtil.isEmpty(addPat))
				addPat=addPat+","+patList.get(i);
				else
				addPat=patList.get(i).toString();
				}
				}
				}
				selectLocationScript = "\"goToStorageDetail("+parent+")\"";
				if(!StringUtil.isEmpty(addSpecimen)){
				 if("PartOccupied".equals(latestStoerageCode) || ("Occupied".equals(latestStoerageCode) && (specList.size())>1))
				  viewSpecimen = " <A href=\"#\" id=\"dialogue_area\" onClick=processAjaxCall('"+addSpecimen+"');>"+LC.L_View_Specimens/*View Specimens*****/+"</A>";
				 else
				 viewSpecimen = " <A href=\"#\" onClick=goToSpecimenDetails("+EJBUtil.stringToNum(addSpecimen)+")>"+LC.L_Sp/*SP*****/+"</A>";
				      if (!StringUtil.isEmpty(addPat)) {
		             if (!addPat.contains(","))
					 seltPatient = "&nbsp;<A href=\"#\" onClick=goToPatientDetails("+addPat+")>"+LC.L_P/*P*****/+"</A>";
		         } else {
		             seltPatient = "";
		         }
				 }
				if ("Occupied".equals(latestStoerageCode)) paravailImagePath = "images/occupied.jpg";
                    else if ("PartOccupied".equals(latestStoerageCode)) paravailImagePath = "images/part_occ.jpg";
                     else paravailImagePath = "images/available.jpg";
    		%>
			
    			<table class="<%=tabclassname%>"><TH><%= topStorageName%></TH> <tr><td onmouseover="return callOverlib('<%=storId%>','<%=storName%>',
				'<%=storDesc%>','<%=latestStoerageCode%>');" onmouseout="return nd();">
             <input type=image name="availabilityIcon" id="availabilityIcon" onClick="<%=selectLocationScript.replaceAll("^\"", "").replaceAll("\"$", "")%>;return false" src="<%=paravailImagePath%>"/>
			<%
			//specAllocation = EJBUtil.stringToNum((String)specAllocationCount.get(k));
			if("PartOccupied".equals(latestStoerageCode) || ("Occupied".equals(latestStoerageCode) && (specList.size())>1)){%>
			 <font size=1>
	    			 	<%=viewSpecimen%>
                        </font>
						<%}
						else{%>
						<font size=1>
	    			 	<%=viewSpecimen%><%=seltPatient%>
                        </font>
						<%}%>
			 </tr><td>
			   </table>
    		<%
			}
    	}
    }
    if(dispType.equals("B"))
    	{ %>
    <table class="<%=tabclassname%> basetbl" border = "0" width="100%" cellspacing="0" cellpadding="0">
    		<TH><%=LC.L_Storage_Name%><%--Storage Name*****--%></TH><TH><%=LC.L_Id_Upper%><%--ID*****--%></TH>
            <!--  <tr><td></td> commented this line for the bug fix 9106 -->
     <% }   %>
    <%
    // Gather cell information for display
    Hashtable tableInfo = new Hashtable();
    HashMap rowsPerLevel = new HashMap();
    for (int k = 0;k<len;k++)
    {
    	if (storageLevel.size() > 0) {
    	    // Collect storage type info and cell counts
        	storageType = new Integer(EJBUtil.stringToNum((String)storageTypes.get(k)));
        	Integer numThisType = (Integer)tableInfo.get(storageLevel.get(k)+"-"+storageType);
        	int numThisTypeInt = 0;
        	if (numThisType == null) { numThisTypeInt = 0; }
        	else { numThisTypeInt = numThisType.intValue(); }
    	    numThisTypeInt++;
        	tableInfo.put(storageLevel.get(k)+"-"+storageType, new Integer(numThisTypeInt));
        	rowsPerLevel.put(storageLevel.get(k), coordys.get(k));
    	}
    	if (sChain.contains(pkStorages.get(k))) {
    	    tdColors.add(k, "#FDFDDE");
    	} else {
        	cY = EJBUtil.stringToNum((String) coordys.get(k));
        	if (cY %2 == 1) tdColors.add(k, "#DADADA");
        	else tdColors.add(k, "#F7F7F7");
    	}
    }

    // Decipher storage types and cell count per level
    Enumeration enum1 = tableInfo.keys();
    HashMap subtypesPerLevel = new HashMap();
    HashMap countPerLevel = new HashMap();
    while(enum1.hasMoreElements()) {
        String sKey = (String)enum1.nextElement();
        String[] parts = sKey.split("-"); // parts[0] is level; parts[1] is type
        String descripPerLevel = (String)subtypesPerLevel.get(parts[0]);
        if (descripPerLevel == null) {
            descripPerLevel = tableInfo.get(sKey)+"@"+parts[1];
        } else {
            descripPerLevel = descripPerLevel+","+tableInfo.get(sKey)+"@"+parts[1];
        }
        subtypesPerLevel.put(parts[0], descripPerLevel);

        Integer thisTypeCount = (Integer) countPerLevel.get(parts[0]);
        if (thisTypeCount == null) {
            thisTypeCount = (Integer)tableInfo.get(sKey);
        } else {
            int tmpCount = thisTypeCount.intValue();
            tmpCount += ((Integer)tableInfo.get(sKey)).intValue();
            thisTypeCount = Integer.valueOf(tmpCount);
        }
        countPerLevel.put(parts[0], thisTypeCount);
    }

    Iterator keyIter = subtypesPerLevel.keySet().iterator();
    String sKey = null;
    while(keyIter.hasNext()) {
        sKey = (String)keyIter.next();
        String typeDescript = formTypeDescriptionPerLevel((String)subtypesPerLevel.get(sKey),
                typeCodepks, typeCodeDescs);
        subtypesPerLevel.put(sKey, typeDescript);
    }

    // Now display all the cells
    for (int k = 0;k<len;k++)
    {
    	cX = EJBUtil.stringToNum( (String) coordxs.get(k));
    	cY = EJBUtil.stringToNum( (String) coordys.get(k)) ;


    	pkStorage = ((Integer) pkStorages.get(k)).intValue();
    	storageName = (String) storageNames.get(k);
    	storageId = (String) storageIds.get(k);

    	statusCode = Integer.valueOf((String)statusCodes.get(k));
    	dim1 = EJBUtil.stringToNum((String)storageDim1.get(k));
    	dim2 = EJBUtil.stringToNum((String)storageDim2.get(k));
    	childAllocation = EJBUtil.stringToNum((String)childAllocationCount.get(k));
    	parentLocation = (String) parentStorageName.get(k);

    	storageType = new Integer(EJBUtil.stringToNum((String)storageTypes.get(k)));

    	if (storageLevel.size() > 0) {
    	    intLevel = EJBUtil.stringToNum((String)storageLevel.get(k));
    	}

    	if(StringUtil.isEmpty(parentLocation))
    	{
    		parentLocation = "-";
    	}

    	 codePos = codepks.indexOf(statusCode);

    	 if (codePos >=0)
    	 {
    	   codeSubType = (String)codeSubtypes.get(codePos);
    	   //codeColor = (String)codeCustom1s.get(codePos);
    	   statusDesc = (String)codeDescs.get(codePos);

    	   if (StringUtil.isEmpty(codeColor))
    	   {
    	   	//codeColor = "";
    	   }
    	 }
    	 /////////////

 			 typePos = typeCodepks.indexOf(storageType);
	      if (typePos >=0)
    	 {

    	   typeImagePath = (String)typeCodeCustom1s.get(typePos);


    	   if (StringUtil.isEmpty(typeImagePath))
    	   {
    	   	typeImagePath = "";
    	   }
    	 }else
    	 {
    	 	typeImagePath ="";
    	 }

	     if ("1".equals(explorer))
 		 {
 		     selectLocationScript = "\"goToDetailInParent("+pkStorage+")\"";
 		     selectLocationStr = "<A href=\"#\" onClick="+selectLocationScript+">"+LC.L_JumpTo/*JumpTo*****/+"</A>";
 		 }
	     // gridFor values => ST=storage; BR=browser (list of top pk's); SP=specimen; EM=embedded
	     else if (gridFor.equals("ST"))
		 {
    	     if ( ("Available".equals(codeSubType))  && childAllocation==0)
		     {
	     	     selectLocationScript = "\"selectStorage('"+storageName+"', 1 , 1 ,"+pkStorage+")\"";
		         selectLocationStr = "<A href=\"#\" onClick="+selectLocationScript+">"+LC.L_Select/*Select*****/+"</A>";
		     }
			  else if ("PartOccupied".equals(codeSubType))
		     {
		         selectLocationScript = "\"selectStorage('"+storageName+"', 1 , 1 ,"+pkStorage+")\"";
		         selectLocationStr = "<A href=\"#\" onClick="+selectLocationScript+">"+LC.L_Select/*Select*****/+"</A>";
		     }
		     else
		     {
		         selectLocationStr = selectLocationScript = "";
		     }
		 } else if (gridFor.equals("BR"))
		 {
		     selectLocationScript = "\"selectStorage('"+storageName+"', "+cX+" , "+cY+" ,"+pkStorage+")\"";
		     selectLocationStr = "<A href=\"#\" onClick="+selectLocationScript+">"+LC.L_Select/*Select*****/+"</A>";
		 } else if (gridFor.equals("EM"))
		 {
		     selectLocationScript = "\"goToStorageDetail("+pkStorage+")\"";
		     selectLocationStr = "<A href=\"storageunitdetails.jsp?srcmenu=tdmenubaritem6&selectedTab=2&mode=M&pkStorage="+pkStorage+"\">"+LC.L_Select/*Select*****/+"</A>";
		     if (sdao.getSpecimenPks(pkStorage) != null) {
		        if("PartOccupied".equals(codeSubType)|| ("Occupied".equals(codeSubType) && sdao.getSpecimenPks(pkStorage).contains(",")))
				 {
				  selectSpecimen = " <A href=\"#\" id=\"dialogue_area\" onClick=processAjaxCall('"+sdao.getSpecimenPks(pkStorage)+"');>"+LC.L_View_Specimens/*View Specimens*****/+"</A>";
		         }
				 else
				 {
				 selectSpecimen = " <A href=\"#\" onClick=goToSpecimenDetails("+sdao.getSpecimenPks(pkStorage)+")>"+LC.L_Sp/*SP*****/+"</A>";
				 }
		         if (sdao.getSpecimenPatientFks(pkStorage) != null) {
		             if (sdao.getSpecimenPatientFks(pkStorage).contains(",")) {
		                 //selectPatient = "&nbsp;<A href=\"#\" onClick=goToSpecimenBrowser("+pkStorage+")>SP</A>";
		             } else {
		                 selectPatient = "&nbsp;<A href=\"#\" onClick=goToPatientDetails("+sdao.getSpecimenPatientFks(pkStorage)+")>"+LC.L_P/*P*****/+"</A>";
		             }
		         } else {
		             selectPatient = "";
		         }
		     } else {
		         selectSpecimen = "&nbsp;&nbsp;&nbsp;";
		         selectPatient = "";
		     }
		 } else //for Specimen
		 {
 		     specAllocation = EJBUtil.stringToNum((String)specAllocationCount.get(k));
		     childCapCount = EJBUtil.stringToNum((String)childCapsCount.get(k));
		     if ("Y".equals(spSrchLocFlag) || ("Available".equals(codeSubType) && specAllocation == 0 && childCapCount > childAllocation) || ("PartOccupied".equals(codeSubType) && specAllocation>=1))
		     {
			  String storageCapacity = "", multiSpecimen = "", capacityUnit = "";
			  int storageCount = 0, storageChildCount = 0;
				//storageB.setPkStorage(pkStorage);
				//storageB.getStorageDetails();
				storageCapacity = (String)childCapsCount.get(k);
				multiSpecimen = (String)storageMultiSpec.get(k);
				capacityUnit = (String)storageCapsUnit.get(k);
				storageCount = EJBUtil.stringToNum((String)specAllocationCount.get(k));
				storageChildCount = EJBUtil.stringToNum((String)childAllocationCount.get(k));
				//System.out.println("storageCapacity :"+storageCapacity+"multiSpecimen: "+multiSpecimen+" "+capacityUnit+" "+storageCount+" "+storageChildCount);
		        selectLocationScript = "\"selectStorage4Specimen('"+storageName+"',"+pkStorage+" ,"+storageCapacity+" ,"+multiSpecimen+","+capacityUnit+","+storageCount+","+storageChildCount+")\"";
				selectLocationStr = "<A href=\"#\" onClick="+selectLocationScript+">"+LC.L_Select/*Select*****/+"</A> ";
		     }
		     else
		     {
		         selectLocationStr = selectLocationScript = "";
		     }
		 }

    	  /////////////

    	if(dispType.equals("G"))
    	{
	    	//create GRID!!!

	    	if (oldIntLevel != intLevel)
	    	{
	    	    // Level has changed; if the last level was 2D, close the table tag
	    	    if (tableIs2D) {
	    	    %>
                    </td></tr></table>
	    	    <%
	    	    }

	    	    // Calculate cell widths
	    	    String tmpLevel = "" + intLevel;
	    	    Integer tmpInteger = (Integer)countPerLevel.get(tmpLevel);
	    	    String tmpRows = (String)rowsPerLevel.get(tmpLevel);
	    	    int numRows = 1;
	    	    if (tmpRows == null) {
	    	        numRows = 1;
	    	    } else {
	    	        try {
	    	            numRows = Integer.parseInt(tmpRows);
	    	        } catch(Exception e) {
	    	            numRows = 1;
	    	        }
	    	    }
		    	int numPerLevel = tmpInteger == null ? 1 : tmpInteger.intValue();
		    	if (numRows > 1) {
		    	    numPerLevel /= numRows;
		    	    tableIs2D = true;
		    	} else {
		    	    tableIs2D = false;
		    	}
	    	    numCellRemaining = numPerLevel;
	    	    int borderWidth = vBorderW * (numPerLevel+2); // (num+1) is # cells (incl. label); add 1 to get # v-border
	    	    int totalWidthTmp = tableIs2D ? totalWidth2D : totalWidth;
	    	    int labelWidthTmp = tableIs2D ? labelWidth2D : labelWidth;
	    	    double tmpDouble = (double)(totalWidthTmp - labelWidthTmp - borderWidth);
	    	    cellWidth = (int)Math.round( tmpDouble / ((double)numPerLevel) );
	    	    cellWidthRemaining = totalWidthTmp - labelWidthTmp - borderWidth;
	        	%>
	        	<%
		    	numLevelsDisplayed++;
	    	    if (numLevelsDisplayed > 1) {
	    		%>
                </tr></table>
                <%
	    	    }
                %>
                <span id="span_storagechild<%=intLevel%>" style="float:left;">
                <%
                // if this table is 2D, add a grid label (which is also a table);
                // also make it a table within a table
                if (numRows > 1) {
                %>
                <table class="<%=tabclassname%>"><TH><%= parentStorageName.get(k)+" Grid View"%></TH></table>
                <table class="<%=tabclassname%>" >
                <tr><td>
                    <table class="<%=tabclassname%>" border = "1">
                    <tr class="browserOddRow">
	    			<td class=tdDefault width=<%=labelWidth2D%>>
                    <% if (! StringUtil.isEmpty(typeImagePath)) { %>
    				    <img src="<%=typeImagePath%>" width=<%=imgWidth%> height=<%=imgHeight%>></img>
    				<% } %>
	    			<%=subtypesPerLevel.get(Integer.toString(intLevel)) %>
	    			</td>
                    <%
                       for(int iX=1; iX<numPerLevel+1; iX++) {
                    %>
                        <td align="center"><%=convertIntToAlphaLabel(iX)%></td>
                    <%
                        }
                    %>
                    </tr>
                    <tr class="browserEvenRow">
	    			<td class=tdDefault align="center" width=<%=labelWidth2D%>><%=cY %></td>
                <%
                } else { // Not 2D => add type description label
                %>
                <table class="<%=tabclassname%>" border = "1">
                    <tr>
	    			<td class=tdDefault width=<%=labelWidth%>>
                    <table><tr>
                    <td class=tdDefault>
    				<% if (! StringUtil.isEmpty(typeImagePath)) { %>
    						<img src="<%=typeImagePath%>" width=<%=imgWidth%> height=<%=imgHeight%>></img>
    				<% } %>
                    </td><td class=tdDefault>
	    			<%=subtypesPerLevel.get(Integer.toString(intLevel))%>
                    </td></tr></table>
	    			</td>
             <% } %>

	    		<%
	    	}
	    	// IH for 3906 - Fixed col vs. row problem in Grid; swap X and Y
	    	else if (oldCY != cY)
	    	{
	    	    String trClassLabel = (cY%2)==0 ? "browserOddRow" : "browserEvenRow";
	    		%>
	    			<tr class=<%=trClassLabel %>>
	    			<td class=tdDefault align="center" width=<%=labelWidth2D%>><%=cY%></td>
	    		<%
	    	}
	    	%>

                        <%
                        if (numCellRemaining == 1) {
                            cellWidth = cellWidthRemaining;
                        } else {
                            cellWidthRemaining -= cellWidth;
                        }
                        numCellRemaining--;
                        String styleBG = "style=\"background-color:"+tdColors.get(k)+"\"";
        	    	    String storageSubtype = (String)typeCodeDescs.get(typeCodepks.indexOf(storageType));
                        %>
	    				<td id="td_level<%=intLevel+"_col"+cX+"_row"+cY%>" <%=styleBG%> class=tableTd width=<%=cellWidth%>
                        onmouseover="return callOverlib('<%=storageIds.get(k)%>','<%=storageNames.get(k)%>',
                            '<%=storageSubtype%>','<%=statusDesc%>');"
                        onmouseout="return nd();"
                        <% if (childAllocation > 0 && "1".equals(browserType)) { %>
                        onClick="callAjaxGetStorageChildren(<%=pkStorage%>,'<%=storageName%>','<%=gridFor%>',<%=parent%>,<%=pkTopStorage%>,
                            '<%=topStorageName%>',<%=intLevel%>,<%=cX%>,<%=cY%>,<%=explorer%>);"
                        <% } %>>
	    				<% if (childAllocation > 0) {
							 availImagePath = null;
                           if ("Occupied".equals(codeSubType)) availImagePath = "images/occupied.jpg";
                           else if ("PartOccupied".equals(codeSubType)) availImagePath = "images/part_occ.jpg";
                           else availImagePath = "images/available.jpg";
						%>
	    				<A href="#" onClick="callAjaxGetStorageChildren(<%=pkStorage%>,'<%=storageName%>','<%=gridFor%>',<%=parent%>,<%=pkTopStorage%>,
                            '<%=topStorageName%>',<%=intLevel%>,<%=cX%>,<%=cY%>,<%=explorer%>)"><%if(tableIs2D)out.print(convertIntToAlphaLabel(cX)+cY);else out.print(cX);%>
	    				</A>
	    				<BR>
                        <font size=1>
	    			  <input type=image name="availabilityIcon" id="availabilityIcon" onClick="<%=selectLocationScript.replaceAll("^\"", "").replaceAll("\"$", "")%>;return false"
                            src="<%=availImagePath%>"/>
							
                        </font>
                        <% } else if (tableIs2D) {
                           availImagePath = null;
                           if ("Occupied".equals(codeSubType)) availImagePath = "images/occupied.jpg";
                           else if ("PartOccupied".equals(codeSubType)) availImagePath = "images/part_occ.jpg";
                           else availImagePath = "images/available.jpg";
                        %>
                        <input type=image name="availabilityIcon" id="availabilityIcon"
                            onClick="<%=selectLocationScript.replaceAll("^\"", "").replaceAll("\"$", "")%>;return false"
                            src="<%=availImagePath%>"/>
	    				<% } else // No child and not 2D
	    				 {
						 availImagePath = null;
                           if ("Occupied".equals(codeSubType)) availImagePath = "images/occupied.jpg";
                           else if ("PartOccupied".equals(codeSubType)) availImagePath = "images/part_occ.jpg";
                           else availImagePath = "images/available.jpg";	
						
						 %>
						 <%=cX%>
						 <br>
						 <input type=image name="availabilityIcon" id="availabilityIcon"
                            onClick="<%=selectLocationScript.replaceAll("^\"", "").replaceAll("\"$", "")%>;return false"
                            src="<%=availImagePath%>"/>
	    				 	
	    				
	    				 <%
	    				 }%>
						 <%
						 specAllocation = EJBUtil.stringToNum((String)specAllocationCount.get(k));
						 if("PartOccupied".equals(codeSubType) || "Occupied".equals(codeSubType) && specAllocation>1){%>
                        <font size=1>
	    			 	<%=selectSpecimen%>
                        </font>
						<%}
						else{%>
						<font size=1>
	    			 	<%=selectSpecimen%><%=selectPatient%>
                        </font>
						<%}%>
						
	    				</td>
	    	<%

	    	oldCY = cY;
	    	oldIntLevel = intLevel;
    	}
    	else //browser mode
    	{
    	    String explorerParameter = "null";
    	    if (!"0".equals(pkStorageForExplorer)) {
    	        explorer = "1";
    	        explorerParameter = "'1'";
    	    }
    		%>
    				</tr>

					   <%if ((k%2)!=0) {
					    %>

					      <tr class="browserEvenRow">

					   <%

					   }
					   else {

					   %>

					      <tr class="browserOddRow">

					 <%
					    }
					 %>
                 	<td onClick="callAjaxGetStorageChildren(<%=pkStorage%>,'<%=storageName%>','<%=gridFor%>',<%=parent%>,<%=pkStorage%>,'<%=storageName%>',null,null,null,<%=explorer%>)">
    				<%if (childAllocation > 0) {  %>
    				 <A href="#" onClick="callAjaxGetStorageChildren(<%=pkStorage%>,'<%=storageName%>','<%=gridFor%>',<%=parent%>,<%=pkStorage%>,'<%=storageName%>',null,null,null,<%=explorer%>)"><%=storageName%></A>
    				 <% } else {%>
    				 <%=storageName%>
    				 <%}%>
    				 <BR><%=selectLocationStr %>
    				 </td>
                 	<td> <%=storageId%></td>
                 	
    				<%
                   	} }
   if (tableIs2D) {
      %>
          </td></tr></table>
      <%
      }
      %>
  	    </tr>
  	    </table>
  <%
  for(int iX=0; iX < numLevelsDisplayed; iX++) {
      %>
  </span>
      <%
  }
      
	 }else{
		 if (dispType.equals("B"))
			{
		 if ( gridFor.equals("SP"))
			{
			 ignoreTemplate="1";
			}
			}
    	 CodeDao cd = new CodeDao();
    	String pagenum  = request.getParameter("page");
        StringBuffer sqlbuf   = new StringBuffer();
        
        
        sqlbuf.append("select PK_STORAGE, STORAGE_ID,STORAGE_NAME, f_codelst_desc(o.fk_codelst_storage_type) as storage_type, o.FK_STORAGE fk_storage_main, STORAGE_CAP_NUMBER, STORAGE_COORDINATE_X,STORAGE_COORDINATE_Y,f_codelst_desc(FK_CODELST_CAPACITY_UNITS) as capacity_units,STORAGE_MULTI_SPECIMEN,nvl(storage_dim1_cellnum,0) storage_dim1_cellnum,nvl(storage_dim2_cellnum,0) storage_dim2_cellnum ");
        sqlbuf.append(" , nvl(f_codelst_desc((select FK_CODELST_STORAGE_STATUS from er_storage_status where FK_STORAGE = PK_STORAGE and ");
        sqlbuf.append(" pk_storage_status=(select max(pk_storage_status)from er_storage_status where fk_storage=PK_STORAGE and ");
        sqlbuf.append(" ss_start_date =(select max(ss_start_date) from er_storage_status where fk_storage =PK_STORAGE)))),'0') as STORAGE_STATUS, ");
        sqlbuf.append(" (select count(*) from er_specimen where fk_storage=pk_storage ) specimen_allocation_count, (select storage_name from er_storage p where p.pk_storage=o.fk_storage) parent_name, ");
        sqlbuf.append(" nvl((select lower(storage_name) from er_storage p where p.pk_storage=o.fk_storage),'-') lower_parent_name, lower(storage_name) lower_storage_name, (select count(*) from er_storage c where c.fk_storage=o.pk_storage ) child_allocation_count  from ER_STORAGE o ");
          if (!specimenType.equals("")) {
            
              sqlbuf.append("  ,ER_ALLOWED_ITEMS adt ");
        
           }  
       
          sqlbuf.append(" where ");
          sqlbuf.append(" fk_account = "+accId+" and o.fk_codelst_storage_type <> (select pk_codelst from er_Codelst where CODELST_TYPE = 'store_type' and  CODELST_SUBTYP = 'Kit')");
                   
            if (!ignoreTemplate.equals("")) {
               sqlbuf.append(" and nvl(storage_istemplate,0) = 0  " );
          
          }
            if (!name.equals("")) {
                sqlbuf.append(" and trim(lower(STORAGE_NAME)) like '%"+name.trim().toLowerCase()+"%' ");
            }
           
            if (!id.equals("")) {
                sqlbuf.append(" and trim(lower(STORAGE_ID)) like '%"+id.trim().toLowerCase()+"%' ");
            } 
            if (!userStorageType.equals("")) {
               
                    sqlbuf.append(" and o.fk_codelst_storage_type = ").append(userStorageType).append(" ");
                
            }
            if (!specimenType.equals("")) {
               
                    sqlbuf.append("  AND adt.FK_CODELST_SPECIMEN_TYPE = ").append(specimenType).append(" AND o.pk_storage=adt.fk_storage ");
                
            }
           
               if (!storageStatus.equals("") ) {
            	   
              	 sqlbuf.append(" AND " ).append(storageStatus).append("=(SELECT FK_CODELST_STORAGE_STATUS  FROM er_storage_status  WHERE FK_STORAGE = PK_STORAGE  AND pk_storage_status=(SELECT MAX(pk_storage_status)  FROM er_storage_status WHERE fk_storage  =PK_STORAGE  AND ss_start_date =(SELECT MAX(ss_start_date) FROM er_storage_status WHERE fk_storage =PK_STORAGE )))");
                    
               } 
            if (!studyIdStr.equals("")) {
             
                    sqlbuf.append(" and st.fk_study= ").append(studyIdStr).append(" ");
                
            }
            if (!PKStorageID.equals("")) {
               
                    sqlbuf.append(" and o.pk_Storage = ").append("'").append(PKStorageID).append("'");
                      
            } 
            /*  sqlbuf.append(" and o.fk_storage is null "); */
          
            String sql = sqlbuf.toString();





    			long rowsPerPage    = 0;
    			long totalPages     = 0;
    			long rowsReturned   = 0;
    			long showPages      = 0;
    			long totalRows      = 0;
    			long firstRec       = 0;
    			long lastRec        = 0;
    			boolean hasMore     = false;
    			boolean hasPrevious = false;


    			String storageTypTxtDesc = "";
    			String countsql = " Select count(*) from ( " + sql + " )";

    			   if (StringUtil.isEmpty(orderBy))
    				orderBy = "lower(STORAGE_ID)";

    			   if (StringUtil.isEmpty(orderType))
    			        orderType = "asc";

    			   if (orderBy.equals("LOWER(CAPACITY_UNITS)"))
    				   orderType = orderType + ", storage_cap_number " +orderType;

    			   rowsPerPage =   Configuration.ROWSPERBROWSERPAGE ;
    			   totalPages  =   Configuration.PAGEPERBROWSER ;
    			   int curPage = 0;
    			   long startPage = 1;
    			   long cntr = 0;
    			   if (pagenum == null)
    			       pagenum = "1";	


    			  curPage        = StringUtil.stringToNum(pagenum);
    	
    			  curPage = (curPage==0)?1:curPage;

    			  BrowserRows br = new BrowserRows();
    			   if (pagenum !=null)
    			   {
    		  	      br.getPageRows(curPage,rowsPerPage,sql,totalPages,countsql,orderBy,orderType);
    			   }
    			  rowsReturned   = br.getRowReturned();
    			  showPages      = br.getShowPages();
    			  startPage      = br.getStartPage();
    			  hasMore        = br.getHasMore();
    			  hasPrevious    = br.getHasPrevious();
    			  totalRows      = br.getTotalRows();
    			  firstRec       = br.getFirstRec();
    			  lastRec        = br.getLastRec();

    			  if(orderType.indexOf(",")!= -1) {
    				  int val = orderType.indexOf(",");
    				  orderType = orderType.substring(0,val);
    			  }

    		%>
		
    		<%
    		 String scannerReader = request.getParameter("scannerRead");
    		 scannerReader=(scannerReader==null)?"0":scannerReader;
    	      if(rowsReturned > 0) {
    			   String pkstorage;
    			   String strId ="";
    			   String strName = "";
    			   String studynum = ""; 
    			   String strType ="";
    			   int cx =0;
    			   int cy =0;
    			   String strCapacityNumber = "";
    			   String strCapacityUnits = " ";
    			   String strStatus = "";
    			   String rNum = "";
    			   String strlocation;
    			   String strTypeDesc ="";
    			   String strStatusDesc = "";
    			   
    			   
    			   
    			   
    			   if (scannerReader.equalsIgnoreCase("0") || (scannerReader.equalsIgnoreCase("1") && rowsReturned>1)){
    		%>
    		
	        <DIV class="tabFormBotN tabFormBotN_MI_1" id="div3" style="top:0px; overflow:auto" >

           <table class="basetbl outline" width="100%" cellspacing="0" cellpadding="0" border="0">
          
    		<TR>  <TH width=10% style="background-color:#eee; color:#696969"><%--Select*****--%></TH>
    		     <th width=10% style="background-color:#eee; color:#696969" onClick="setOrder(document.search,'STORAGE_ID','<%=orderType %>')" align =center><%=LC.L_Id_Upper%><%--ID*****--%> &loz;</th>
    		     <th width=14%  style="background-color:#eee; color:#696969" onClick="setOrder(document.search,'LOWER(STORAGE_NAME)','<%=orderType %>')" align =center><%=LC.L_Name%><%--Name*****--%>  &loz;</th>
    		     <th width=14% style="background-color:#eee; color:#696969" onClick="setOrder(document.search,'LOWER(STORAGE_TYPE)','<%=orderType %>')"align =center><%=LC.L_Type%><%--Type*****--%>  &loz;</th> <!--KM-->
    		     <th width=14% style="background-color:#eee; color:#696969" onClick="setOrder(document.search,'LOWER(PARENT_NAME)','<%=orderType %>')" align =center><%=LC.L_Location%><%--Location*****--%> &loz;</th>
    		     <th width=10% style="background-color:#eee; color:#696969" onClick="setOrder(document.search,'LOWER(CAPACITY_UNITS)','<%=orderType %>')" align =center><%=LC.L_Capacity%><%--Capacity*****--%> &loz;</th>
    		     <th width=12% style="background-color:#eee; color:#696969" onClick="setOrder(document.search,'LOWER(STORAGE_STATUS)','<%=orderType %>')" salign =center ><%=LC.L_Status%><%--Status*****--%>  &loz;</th>
    		    
    		   	</TR>
    	<%
    		    for(int i = 1 ; i <=rowsReturned ; i++) {
    		    strlocation="";
    		    int specAllocation=0;
    		    int childCapCount=0;
    		    int childAllocation=0;
    		    pkstorage = br.getBValues(i,"PK_STORAGE");

    		    strId     = br.getBValues(i,"STORAGE_ID");
    		    if(strId.length() > 35)
    		      strId = strId.substring(0,35) + "...";

    			studynum = br.getBValues(i,"studynum");
    		    studynum=(studynum==null)?"":studynum;
                strTypeDesc   = br.getBValues(i,"storage_type");


    			cx = EJBUtil.stringToNum(br.getBValues(i,"STORAGE_COORDINATE_X"));
    		    cy = EJBUtil.stringToNum(br.getBValues(i,"STORAGE_COORDINATE_Y"));


    			strStatusDesc = br.getBValues(i,"STORAGE_STATUS");


    		      strName   = br.getBValues(i,"STORAGE_NAME");
    		      if(strName.length() > 35)
    		      strName = strName.substring(0,35) + "...";
    		      strCapacityNumber = br.getBValues(i,"STORAGE_CAP_NUMBER");

    			  strCapacityUnits  = br.getBValues(i,"CAPACITY_UNITS");
    		     
    			 String strCapacityNumberAndUnits = "";


    		    if(strCapacityUnits == null || strCapacityNumber == null)
    		      strCapacityNumberAndUnits = "";

    		      else
    		      strCapacityNumberAndUnits = strCapacityNumber+" "+strCapacityUnits ;



    			strlocation =  br.getBValues(i,"PARENT_NAME");

    			if (StringUtil.isEmpty(strlocation))
    			{
    				strlocation= "";
    			}

    			rNum = br.getBValues(i,"RNUM");
    			specAllocation=EJBUtil.stringToNum((String)br.getBValues(i, "specimen_allocation_count"));
    			childCapCount=EJBUtil.stringToNum((String)br.getBValues(i, "STORAGE_CAP_NUMBER"));
    			childAllocation=EJBUtil.stringToNum((String)br.getBValues(i, "child_allocation_count"));
    			
    		    if ((i%2)!=0) {
    		    %>

    		      <tr class="browserEvenRow">

    		   <%

    		   }
    		   else {

    		   %>

    		      <tr class="browserOddRow">

    		 <%
    		    }
    		 %>
    		 <%
    		  if (gridFor.equals("ST"))
    		 {
        	     if ( (("Available".equals(strStatusDesc))  && childAllocation==0) ||  ("PartOccupied".equals(strStatusDesc)))
    		     {
    	     	     selectLocationScript = "\"selectStorage('"+strName+"', 1 , 1 ,"+pkstorage+")\"";
    		         selectLocationStr = "<A href=\"#\" onClick="+selectLocationScript+">"+LC.L_Select/*Select*****/+"</A>";
    		     }
    		  else
    		     {
    			  selectLocationStr = selectLocationScript=""; 
    		     }
    		 } 
    		 else if (gridFor.equals("BR"))
    			 {
    			     selectLocationScript = "\"selectStorage('"+strName+"', "+cx+" , "+cy+" ,"+pkstorage+")\"";
    			     selectLocationStr = "<A href=\"#\" onClick="+selectLocationScript+">"+LC.L_Select/*Select*****/+"</A>";
    			 }
    		 else if(gridFor.equals("SP")){
    		 if ("Y".equals(spSrchLocFlag) || ("Available".equals(strStatusDesc) && specAllocation == 0 && childCapCount > childAllocation) || ("PartOccupied".equals(strStatusDesc) && specAllocation>=1))
		     {
    			 
    			 selectLocationScript = "\"selectStorage4Specimen('"+strName+"',"+pkstorage+","+strId+")\"";
		             selectLocationStr = "<A href=\"#\" onClick="+selectLocationScript+">"+LC.L_Select/*Select*****/+"</A>";
		     }
    		 else{
    			 selectLocationStr = selectLocationScript=""; 
    		 }}
    		 %>
    		 
    		 
    		
   		         
   		         <td><%=selectLocationStr%></td>
    		    <td width =10%><%=strId%></A></td>
    		     <td width =14%><%=strName%> </td>
    		     <td width =14%><%=strTypeDesc%></td> 
    		     <td width =14%><%=strlocation%></td>
    		     <td width =10%><%=strCapacityNumberAndUnits%></td>
    		     <td width =12%><%=strStatusDesc%></td>
    		    

    		 <%
    		  }
    		%>
    		</table>
    	<%}%><P class="defComments"><%=MC.M_Your_SelcFiltersAre%><%--Your selected filters are*****--%>:

	        <%
		if(storageId.equals("All") || storageId == null || storageId.equals("") ) {
		%>
		<%=LC.L_Storage_Id%><%--Storage ID*****--%>:&nbsp;<B><I><u><%=LC.L_All%><%--All*****--%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 else {
		 %>
		 <%=LC.L_Storage_Id%><%--Storage ID*****--%>:&nbsp;<B><I><u><%=storageId%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 %>


		<%
		if(userStorageType.equals("All") || userStorageType == null || userStorageType.equals("")) {
		%>
		<%=LC.L_Storage_Type%><%--Storage
		 type*****--%>:&nbsp;<B><I><u><%=LC.L_All%><%--All*****--%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 else {
		 %>
		 <%=LC.L_Storage_Type%><%--Storage
		 type*****--%>:&nbsp;<B><I><u><%=cd.getCodeDescription(Integer.parseInt(userStorageType))%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 %>
		 
		 <%
		if(name.equals("All") || name == null || name.equals("")) {
		%>
		<%=LC.L_Name%><%--Name*****--%>:&nbsp;<B><I><u><%=LC.L_All%><%--All*****--%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 else {
		 %>
		 <%=LC.L_Name%><%--Name*****--%>:&nbsp;<B><I><u><%=name%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 %>
		 <%
		if(specimenType.equals("All") || specimenType == null || specimenType.equals("")) {
		%>
		<%=LC.L_Allowed_Specimens%><%--Allowed Specimens*****--%>:&nbsp;<B><I><u><%=LC.L_All%><%--All*****--%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 else {
		 %>
	    <%=LC.L_Allowed_Specimens%><%--Allowed Specimens*****--%>:&nbsp;<B><I><u><%=cd.getCodeDescription(Integer.parseInt(specimenType))%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 %>
		<%
		if(storageStatus.equals("All") || storageStatus == null || storageStatus.equals("")) {
		%>
		<%=LC.L_Status%><%--Status*****--%>:&nbsp;<B><I><u><%=LC.L_All%><%--All*****--%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 else {
		 %>
		 <%=LC.L_Status%><%--Status*****--%>:&nbsp;<B><I><u><%=cd.getCodeDescription(Integer.parseInt(storageStatus))%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 %>
		<%
		if(selStudy.equals("All") || selStudy == null || selStudy.equals("")) {
		%>
		<%=LC.L_Study%><%--<%=LC.Std_Study%>*****--%>:&nbsp;<B><I><u><%=LC.L_All%><%--All*****--%></u></I></B>&nbsp;
		 <%
		 }
		 else {
		 %>
		 <%=LC.L_Study%><%--<%=LC.Std_Study%>*****--%>:&nbsp;<B><I><u><%=selStudy%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 %>
		</P>
    	
    		
    		<table class="tableDefault" align=left  >
    				<tr valign="top"><td>

    			<% if (totalRows > 0) { Object[] arguments = {firstRec,lastRec,totalRows}; %>
    				<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%-- <font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
    			<%} %>	</td></tr></table>
          <table class="tableDefault" align=center height="50px">
			<tr valign="top">

	      <%
	       if (curPage==1) startPage=1;

	        for (int count = 1; count <= showPages;count++) {
	        cntr = (startPage - 1) + count;
	        if ((count == 1) && (hasPrevious)) {

	       %>

	  	<td> <A href="searchLocation.jsp?storagepk=<%=storagePKFldName %>&selStudy=<%=selStudy%>&locationName=<%=locationFldName%>&form=<%=form %>&srchLocFlag=<%=spSrchLocFlag %>&gridFor=<%=gridFor%>&selStudyIds=<%=studyIdStr%>&storageId=<%=id %>&storageName=<%=name%>&storageType=<%=userStorageType%>&specimenType=<%=specimenType%>&storageStatus=<%=storageStatus%>&coordx=<%=coordXFldName %>&coordy=<%=coordYFldName %>&page=<%=cntr-1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>">< <%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%> ></A>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<%
	  	}
		%>
		<td>
		<%

		 if (curPage  == cntr)
		 {
	        %>
			<FONT class = "pageNumber"><%= cntr %></Font>
	       <%
	       }
	      else
	        {
	       %>
	   <A href="searchLocation.jsp?storagepk=<%=storagePKFldName %>&selStudy=<%=selStudy%>&locationName=<%=locationFldName%>&form=<%=form %>&srchLocFlag=<%=spSrchLocFlag %>&gridFor=<%=gridFor %>&selStudyIds=<%=studyIdStr%>&storageId=<%=id %>&storageName=<%=name%>&storageType=<%=userStorageType%>&specimenType=<%=specimenType%>&storageStatus=<%=storageStatus%>&coordx=<%=coordXFldName %>&coordy=<%=coordYFldName %>&page=<%=cntr%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>"><%=cntr%></A>
	
	       <%
	    	}
		 %>
		</td>
		<%
		  }

		if (hasMore)
		{

	     %>
	     <td colspan = 3 align = center>
	  	&nbsp;&nbsp;&nbsp;&nbsp;<td> <A href="searchLocation.jsp?storagepk=<%=storagePKFldName %>&selStudy=<%=selStudy%>&locationName=<%=locationFldName%>&form=<%=form %>&srchLocFlag=<%=spSrchLocFlag %>&gridFor=<%=gridFor %>&selStudyIds=<%=studyIdStr%>&storageId=<%=id %>&storageName=<%=name%>&storageType=<%=userStorageType%>&specimenType=<%=specimenType%>&storageStatus=<%=storageStatus%>&coordx=<%=coordXFldName %>&coordy=<%=coordYFldName %>&page=<%=cntr+1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>">< <%=LC.L_Next%><%--Next*****--%> <%=totalPages%> ></A></td>
	
		<%
	  	}
		%>
	   </tr>
	  </table>
    		
    		 <%
    		   }

    		   else {
    		     %>
    		      <P class="defComments"><%=MC.M_Your_SelcFiltersAre%><%--Your selected filters are*****--%>:

    		        <%
    			if(storageId.equals("All") || storageId == null || storageId.equals("") ) {
    			%>
    			<%=LC.L_Storage_Id%><%--Storage ID*****--%>:&nbsp;<B><I><u><%=LC.L_All%><%--All*****--%></u></I></B>&nbsp;&nbsp;
    			 <%
    			 }
    			 else {
    			 %>
    			 <%=LC.L_Storage_Id%><%--Storage ID*****--%>:&nbsp;<B><I><u><%=storageId%></u></I></B>&nbsp;&nbsp;
    			 <%
    			 }
    			 %>


    			<%
    			if(userStorageType.equals("All") || userStorageType == null || userStorageType.equals("")) {
    			%>
    			<%=LC.L_Storage_Type%><%--Storage
    			 type*****--%>:&nbsp;<B><I><u><%=LC.L_All%><%--All*****--%></u></I></B>&nbsp;&nbsp;
    			 <%
    			 }
    			 else {
    			 %>
    			 <%=LC.L_Storage_Type%><%--Storage
    			 type*****--%>:&nbsp;<B><I><u><%=cd.getCodeDescription(Integer.parseInt(userStorageType))%></u></I></B>&nbsp;&nbsp;
    			 <%
    			 }
    			 %>
    		
    			 <%
    			if(name.equals("All") || name == null || name.equals("")) {
    			%>
    			<%=LC.L_Name%><%--Name*****--%>:&nbsp;<B><I><u><%=LC.L_All%><%--All*****--%></u></I></B>&nbsp;&nbsp;
    			 <%
    			 }
    			 else {
    			 %>
    			 <%=LC.L_Name%><%--Name*****--%>:&nbsp;<B><I><u><%=name%></u></I></B>&nbsp;&nbsp;
    			 <%
    			 }
    			 %>
    			<%
    			if(specimenType.equals("All") || specimenType == null || specimenType.equals("")) {
    			%>
    			<%=LC.L_Allowed_Specimens%><%--Allowed Specimens*****--%>:&nbsp;<B><I><u><%=LC.L_All%><%--All*****--%></u></I></B>&nbsp;&nbsp;
    			 <%
    			 }
    			 else {
    			 %>
    		    <%=LC.L_Allowed_Specimens%><%--Allowed Specimens*****--%>:&nbsp;<B><I><u><%=cd.getCodeDescription(Integer.parseInt(specimenType))%></u></I></B>&nbsp;&nbsp;
    			 <%
    			 }
    			 %>
    			<%
    			if(storageStatus.equals("All") || storageStatus == null || storageStatus.equals("")) {
    			%>
    			<%=LC.L_Status%><%--Status*****--%>:&nbsp;<B><I><u><%=LC.L_All%><%--All*****--%></u></I></B>&nbsp;&nbsp;
    			 <%
    			 }
    			 else {
    			 %>
    			 <%=LC.L_Status%><%--Status*****--%>:&nbsp;<B><I><u><%=cd.getCodeDescription(Integer.parseInt(storageStatus))%></u></I></B>&nbsp;&nbsp;
    			 <%
    			 }
    			 %>
    			<%
    			if(selStudy.equals("All") || selStudy == null || selStudy.equals("")) {
    			%>
    			<%=LC.L_Study%><%--<%=LC.Std_Study%>*****--%>:&nbsp;<B><I><u><%=LC.L_All%><%--All*****--%></u></I></B>&nbsp;
    			 <%
    			 }
    			 else {
    			 %>
    			 <%=LC.L_Study%><%--<%=LC.Std_Study%>*****--%>:&nbsp;<B><I><u><%=selStudy%></u></I></B>&nbsp;&nbsp;
    			 <%
    			 }
    			 %>
    			</P>
    		 <% if(pagenum != null) { // When the page opens the first time, do no search and don't display the following msg %>
    			<P class="defComments" align="center"><b><%=MC.M_NoRec_MdfCrit%><%--No matching records found. Please modify your criteria and try again.*****--%></b></P>
    	     <% } %>
    		       <%
    		  }
    		%>
    	  </div>    
	 
	<% }%>
                   	
	
	
<%}//session
%>
