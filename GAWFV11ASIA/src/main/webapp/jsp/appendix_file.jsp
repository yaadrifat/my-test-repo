<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>
<%
boolean isIrb = "irb_upload_tab".equals(request.getParameter("selectedTab")) ? true : false; 
String includeTabsJsp = isIrb ? ("LIND".equals(CFG.EIRB_MODE) ? "ecompNewTabs.jsp" : "irbnewtabs.jsp") : "studytabs.jsp";
if (isIrb) { 
%>
<title><%=MC.M_ResComp_NewUpldDocu%><%--Research Compliance >> New Application >> Upload Documents*****--%></title>
<% } else { %>
<title><%=MC.M_StdVer_FileDets%><%--Study >> Version >> File Details*****--%></title>
<% } %>





<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">



</HEAD>



<% String src;

src= request.getParameter("srcmenu");
String from = "appendixver";
String isPopupWin = "0";
if(request.getParameter("isPopupWin")!=null){
	isPopupWin = request.getParameter("isPopupWin");
}
%>



<jsp:include page="panel.jsp" flush="true">

<jsp:param name="src" value="<%=src%>"/>
<jsp:param name="isPopupWin" value="<%=isPopupWin%>"/>

</jsp:include>   



<SCRIPT Language="javascript">

 function  validate(formobj){

//     formobj=document.upload
	
     if (!(validate_col('File',formobj.name))) return false
     if (!(validate_col('Description',formobj.desc))) return false
     if(formobj.desc.value.length>500){  
	     alert("<%=MC.M_SrtDesc_MaxChar%>");/*alert("'Short Description' exceeded maximum number of characters allowed.");*****/
	     formobj.desc.focus();
	     return false;
     }     
     if (!(validate_col('e-Sign',formobj.eSign))) return false
// 	 if(isNaN(formobj.eSign.value) == true) {
<%-- 		 alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/ --%>
// 		formobj.eSign.focus();
// 		return false;
// 		}
 }

function openwin() {

      window.open("","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=600,height=450")

;}

</SCRIPT>

<BODY>
<jsp:useBean id ="sessionmaint" scope="request" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studyJB" scope="session" class="com.velos.eres.web.study.StudyJB" />
<%@ page  language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*"%>
<%
HttpSession tSession = request.getSession(true);
String sessStudyId = "";
if(request.getParameter("studyId")!=null){
	sessStudyId=request.getParameter("studyId");
}
else{
	sessStudyId=(String) tSession.getValue("studyId");
}
%>
<%if(!isPopupWin.equals("1")){%>
<DIV class="BrowserTopn" id="div1"> 
  <jsp:include page="<%=includeTabsJsp%>" flush="true">
  <jsp:param name="from" value="<%=from%>"/> 
  <jsp:param name="studyId" value="<%=sessStudyId %>"/>
  </jsp:include>
    </DIV>
    <%} %>
<% if("Y".equals(CFG.Workflows_Enabled)&& (sessStudyId!=null && !sessStudyId.equals("") && !sessStudyId.equals("0"))){ %>
<DIV class="BrowserBotN BrowserBotN_S_3 workflowDivBig" id = "div1" style="top:145px">
<% }else{ %>
<DIV class="BrowserBotN BrowserBotN_S_3" id = "div1">
<%} %>
  <%

 

if (sessionmaint.isValidSession(tSession))

{

	String accId = (String) tSession.getValue("accountId"); 
	String userId = (String) tSession.getValue("userId");
	String uName = (String) tSession.getValue("userName");


	String studyNo = "";
	if(request.getParameter("studyId")!=null){
		studyJB.setId(StringUtil.stringToNum(sessStudyId));
		studyJB.getStudyDetails();
		studyNo=studyJB.getStudyNumber();
	}
	else{
		studyNo=(String) tSession.getValue("studyNo");
	}

	String tab = request.getParameter("selectedTab");
	String studyVerId = request.getParameter("studyVerId");	

	String accMaxStorage = (String) tSession.getAttribute("accMaxStorage");

	int stId=EJBUtil.stringToNum(sessStudyId);
	
	CtrlDao ctrlDao = new CtrlDao();	
	long freeSpace = ctrlDao.getFreeSpace(EJBUtil.stringToNum(accId));
	String statusPk=request.getParameter("statusPk")==null?"":request.getParameter("statusPk");
	String statusDesc=request.getParameter("statusDesc")==null?"":request.getParameter("statusDesc");

%>
  
  <% 

	 com.aithent.file.uploadDownload.Configuration.readSettings("eres");
	com.aithent.file.uploadDownload.Configuration.readUploadDownloadParam(com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD + "fileUploadDownload.xml", "study");
	String upld=com.aithent.file.uploadDownload.Configuration.UPLOADSERVLET;

%>
	<Form name=upload id ="uploadfrm" action=<%=upld%> onsubmit="if (validate(document.upload)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); document.upload.submit(); setTimeout('document.upload.eSign.disabled=true', 10); document.getElementById('submit_btn').disabled=true;}"  METHOD="POST" ENCTYPE="multipart/form-data">

  	<input type=hidden name=accId value=<%=accId%>>
  	<input type=hidden name=userId value=<%=userId%>>
    <table width="100%" >
      <!--<tr > 
        <td width = "100%"> 
          <P class = "sectionHeadings"> Study:<%=studyNo%> </P>
        </td>
      </tr>-->
      <tr> 
        <td colspan="2" align="center"> 
          <P class = "defComments"> <b><%=MC.M_UploadDoc_StdsAppx%><%--Upload documents to your Study's Appendix*****--%> </b></P>
        </td>
      </tr>
    <tr><td>&nbsp;</td></tr>
      <tr> 
        <td width="20%" align="right">  <%=LC.L_File%><%--File*****--%><FONT class="Mandatory">* </FONT> </td>
        <td width="65%"> 
          <input type=file name=name size=40 >
        </td>
      </tr>
      <tr> 
        <td width="35%"> </td>
        <td width="65%"> 
          <P class="defComments"> <%=MC.M_Specify_FullFilePath%><%--Specify full path of the file.*****--%> </P>
        </td>
      </tr>
     <tr><td>&nbsp;</td></tr>
      <tr> 
        <td align="right"> <%=LC.L_Short_Desc%><%--Short Description*****--%><FONT class="Mandatory" >* </FONT> 
        </td>
        <td width="65%"> 
          <TextArea type=text name=desc row=3 cols=50 ></TextArea>
        </td>
      </tr>
      <tr> 
        <td width="35%"> </td>
        <td width="65%"> 
          <P class="defComments"> <%=MC.M_ShortDescFile_500CharMax%><%--Give a short description of your file (500 char max.)*****--%> </P>
        </td>
      </tr>
    </table>
    <table width="100%" >
      <tr><td>&nbsp;</td></tr>
      <tr> 
        <td  colspan=2> <%=MC.M_DoYouWant_InfoToPublic%><%--Do you want information in this section to be available to the public?*****--%> </td>
      </tr>
      <tr> 
        <td colspan=2> 
          <input type="Radio" name="pubflag" value=Y>
          <%=LC.L_Yes%><%--Yes*****--%> 
          <input type="Radio" name="pubflag" value=N CHECKED>
          <%=LC.L_No%><%--No*****--%> &nbsp; <A href="pubvsnonpub.htm" target="Information" onClick="openwin()"><%=MC.M_PublicVsNotPublic_Info%><%--What is Public vs Not Public Information?*****--%></A> </td>
      </tr>
    </table>
    <input type="hidden" name="type" value='file'>
    <input type="hidden" name="study" value=<%=stId%>>
    <input type="hidden" name="studyId" value=<%=stId%>>
    <input type="hidden" name="studyVer" value=<%=studyVerId%>>	
	
    <input type=hidden name=db value='eres'>
	<input type=hidden name=module value='study'>		  
	<input type=hidden name=tableName value='ER_STUDYAPNDX'>
	<input type=hidden name=columnName value='STUDYAPNDX_FILEOBJ'>
	<input type=hidden name=pkValue value='0'>
	<input type=hidden name=pkColumnName value='PK_STUDYAPNDX'>
	<input type=hidden name=maxFileSize value=<%=freeSpace%>>
	<input type=hidden name=accMaxStorage value=<%=accMaxStorage%>> 
	<input type=hidden name=nextPage value='../../velos/jsp/appendixbrowser.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&studyVerId=<%=studyVerId%>&studyId=<%=sessStudyId%>&isPopupWin=<%=isPopupWin%>&statusPk=<%=statusPk %>&statusDesc=<%=statusDesc %>'>
    <BR>

	<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="uploadfrm"/>
		<jsp:param name="showDiscard" value="N"/>
	</jsp:include>
	
	   
 </form>
  <%

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<DIV class="mainMenu" id = "emenu"> 
  <jsp:include page="getmenu.jsp" flush="true"/>
</DIV>
</body>

</html>

