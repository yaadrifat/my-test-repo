<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<Link Rel=STYLESHEET HREF="common.css" type=text/css>

<BODY>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<%@ page language = "java" import = "com.velos.esch.service.util.EJBUtil,com.velos.esch.business.common.*,java.util.*,com.velos.eres.service.util.*"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language="java" import="com.velos.esch.audit.web.AuditRowEschJB"%>


<%
 HttpSession tSession = request.getSession(true);
 if (sessionmaint.isValidSession(tSession))
 {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
String eSign = request.getParameter("eSign");

String oldESign = (String) tSession.getValue("eSign");

	if(!(oldESign.equals(eSign)))  {
	%>
	  <jsp:include page="incorrectesign.jsp" flush="true"/>
	<%
	} else {



	String actualDt = request.getParameter("actualDt");
	String prevDate = request.getParameter("prevDate");

	//actualDt = actualDt.toString().substring(5,7) + "/" + actualDt.toString().substring(8,10) + "/" + actualDt.toString().substring(0,4);


	Date st_date1 = DateUtil.stringToDate(actualDt,"");
	/* KM-11Dec09
	Date temp1=DateUtil.stringToDate(prevDate,"");
	java.sql.Date previousDate = DateUtil.dateToSqlDate(temp1);
	long days1=st_date1.getTime();
	long days2=temp1.getTime();
	long test=0;
	int differ=0; //(int) (days1-days2)60/60/24/1000;
	Integer diff=new Integer(differ);
	String difference=EJBUtil.integerToString(diff);
	out.println("diff is " + diff); */
	java.sql.Date actualDate = DateUtil.dateToSqlDate(st_date1);


	String eventId = request.getParameter("eventId");
	String patProtId= request.getParameter("patProtId");
	String statusflag= request.getParameter("statusflag");
	String patientCode= request.getParameter("patientCode");
	String studyVer= request.getParameter("studyVer");

	String calAssoc=request.getParameter("calassoc");
	calAssoc=(calAssoc==null)?"":calAssoc;

	String studyId=request.getParameter("studyId");
	studyId=(studyId==null)?"":studyId;

	String calId=request.getParameter("calId");
	calId=(calId==null)?"":calId;

	String statid= request.getParameter("statid");
	String pkey = request.getParameter("pkey");
	String statDesc=request.getParameter("statDesc");

	String synchsuggested = request.getParameter("synchsuggested");


	String ipAdd = (String) tSession.getValue("ipAdd");
   	String usr = (String) tSession.getValue("userId");

   	//JM: 23Aug2010, #4314
   	String protocolId=request.getParameter("protocolCalendar");
	protocolId=(protocolId==null)?"":protocolId;

	String neededStudyId=(String)tSession.getAttribute("studyId");
	neededStudyId=(neededStudyId==null)?"":neededStudyId;


	/*out.println("previousDate" +previousDate);
	out.println("difference" +difference);
	out.println("eventId" +eventId);
	out.println("patProtId" +patProtId);
	out.println("statusflag" +statusflag);
	out.println("ipAdd" +ipAdd);
	out.println("usr" +usr);


	if(differ>0)
	{
	difference="+" + difference ;
	}
	out.println("difference is rr" +difference);

	EventAssocDao eventassocdao=new EventAssocDao();

	//int test123=eventassocdao.ChangeActDate(prevDate,actualDt,eventId,patProtId,statusflag,ipAdd,usr);*/


	if (calAssoc.equals("S"))
	{
	  	eventdefB.ChangeActualDate(prevDate,actualDt,eventId,studyId,statusflag,ipAdd,usr,calAssoc,StringUtil.stringToNum(synchsuggested));
	}
	else{
		ArrayList movingEvents = null;
		if (!"1".equals(statusflag)){
			 movingEvents = eventdefB.findAllMovingEvents(prevDate,actualDt,eventId,patProtId,statusflag, calAssoc ,EJBUtil.stringToNum(synchsuggested));
		}
		
	  	eventdefB.ChangeActualDate(prevDate,actualDt,eventId,patProtId,statusflag,ipAdd,usr,calAssoc ,StringUtil.stringToNum(synchsuggested));
	  	
		String remarks = request.getParameter("remarks");
		if (!StringUtil.isEmpty(remarks)){
		  	if (null != movingEvents && movingEvents.size() > 0 ){
	  			AuditRowEschJB auditJB = new AuditRowEschJB();
	  			auditJB.setReasonForChangeOfScheduleEvents(movingEvents,StringUtil.stringToNum(usr),remarks);
			} else {
				AuditRowEschJB auditJB = new AuditRowEschJB();
				auditJB.setReasonForChangeOfScheduleEvent(StringUtil.stringToNum(eventId),StringUtil.stringToNum(usr),remarks);
			}
		}
	}




%>

<br><br><br><br>
<p class="successfulmsg" align=center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%> </p>
<form name="Updatedate" method="post">
	<input type=hidden name=pkey value=<%=pkey%>>
	<input type=hidden name=statDesc value=<%=statDesc%>>
	<input type=hidden name=patProtId value=<%=patProtId%>>
	<input type=hidden name=statid value=<%=statid%>>
	<input type=hidden name=studyVer value=<%=studyVer%>>
	<input type=hidden name=patientCode value=<%=patientCode%>>
	<input type=hidden name=calAssoc value=<%=calAssoc%>>
	<input type=hidden name=studyId value=<%=studyId%>>
	<input type=hidden name=calId value=<%=calId%>>


<table width="100%">
<tr>
<td align=center>

</td>
</tr>
</table>
</form>

<%
String ucsdCalled=(request.getParameter("ucsdCalled")==null)?"":request.getParameter("ucsdCalled");
if("called".equals(ucsdCalled)){
%>
		<script>
			setTimeout("self.close()",1000);
	  </script>
<%}else{ %>
<script>
	pkey=document.Updatedate.pkey.value;
	statDesc=document.Updatedate.statDesc.value;
	statid=document.Updatedate.statid.value;
	studyVer=document.Updatedate.studyVer.value;
	patientCode=document.Updatedate.patientCode.value;
	patProtId=document.Updatedate.patProtId.value;
	calAssoc=document.Updatedate.calAssoc.value;
	studyId=document.Updatedate.studyId.value;
	calId=document.Updatedate.calId.value;



	if(document.all)
	{

		if (calAssoc=="S")
		{
			window.opener.document.location = "studyadmincal.jsp?srcmenu=tdmenubaritem5&selectedTab=10&studyId="+studyId+"&calId="+calId;
		}
		else
		{
			//window.opener.document.location = "patientschedule.jsp?srcmenu=tdmenubaritem5&pkey=" + pkey + "&selectedTab=3&generate=N&page=patientEnroll&statDesc=" + statDesc + "&statid=" + statid + "&studyVer=" +studyVer +"&patientCode=" +patientCode+"&patProtId="+patProtId;
				window.opener.location.reload();


		}
	}
	else
	{
		if (calAssoc=="S")
		{
			window.opener.document.location = "studyadmincal.jsp?srcmenu=tdmenubaritem5&selectedTab=10&studyId="+studyId+"&calId="+calId;
		}
		else
		{
		//	window.opener.document.location = "patientschedule.jsp?srcmenu=tdmenubaritem5&pkey=" + pkey + "&selectedTab=3&generate=N&page=patientEnroll&statDesc=" + statDesc+ "&statid=" +statid+"&studyVer="+studyVer+"&patientCode="+patientCode+"&patProtId="+patProtId;
			window.opener.location.reload();

		}

	}
self.close();
</script>

<%}}//end of if for eSign



} //end of if for session
else{%>
<jsp:include page="timeout.html" flush="true"/>
<%}%>


</BODY>
</HTML>
