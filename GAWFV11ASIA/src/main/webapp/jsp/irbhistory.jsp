<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
<jsp:include page="include.jsp" flush="true"/>
<jsp:include page="skinChoser.jsp" flush="true"/>
<title><%=LC.L_Submission_StatusHistory%><%--Submission Status History*****--%></title>
</head>

<body style="overflow:auto;">
<DIV >
<jsp:useBean id ="sessionmaint" scope="request" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="submBoardB" scope="request" class="com.velos.eres.web.submission.SubmissionBoardJB"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<%@ page import="com.velos.eres.business.common.*,com.velos.eres.service.util.*" %>
<%@ page import="com.velos.eres.web.grpRights.GrpRightsJB" %>
<%@ page import="java.util.ArrayList,java.util.Calendar,java.util.Hashtable" %>
<%
HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession)) {
    String accountId = (String)tSession.getAttribute("accountId");
    String defUserGroup = (String) tSession.getAttribute("defUserGroup");
    String usrId = (String) tSession.getValue("userId");
    String usrFullName = UserDao.getUserFullName(EJBUtil.stringToNum(usrId));
    String submissionPK = StringUtil.htmlEncodeXss(request.getParameter("submissionPK"));
    String submissionBoardPK = StringUtil.htmlEncodeXss(request.getParameter("submissionBoardPK"));
    String selectedTab = StringUtil.htmlEncodeXss(request.getParameter("selectedTab"));
    String studyId = request.getParameter("studyId");
    GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
	int subSumRight = Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_SSUM"));
	int subNewGroupRight =  Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_SNS"));
	int subComplGroupRight =  Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_SCS"));
	int subAssignedGroupRight = Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_SAS"));
	int subPRGroupRight = Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_SPRS"));
	int subPIGroupRight = Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_SPI"));
	studyB.setId(EJBUtil.stringToNum(studyId));
	studyB.getStudyDetails();
	String studyNumber = studyB.getStudyNumber();
	String studyTitle = studyB.getStudyTitle();
	boolean showHistory = false;
	if (subSumRight == 4 || subSumRight >=6) { showHistory = true; } 
	if (subNewGroupRight == 4 || subNewGroupRight >=6) { showHistory = true; } 
	if (subComplGroupRight == 4 || subComplGroupRight >=6) { showHistory = true; } 
	if (subAssignedGroupRight == 4 || subAssignedGroupRight >=6) { showHistory = true; } 
	if (subPRGroupRight == 4 || subPRGroupRight >=6) { showHistory = true; } 
	if (subPIGroupRight == 4 || subPIGroupRight >=6) { showHistory = true; }
    EIRBDao eIrbDao = new EIRBDao();
  //  if (showHistory) {
   //     showHistory = eIrbDao.getSubmissionAccess(usrId,submissionPK);
  //  }
    if (submissionPK == null || submissionPK.length() == 0 
            || submissionBoardPK == null || submissionBoardPK.length() == 0
            || !showHistory) { 
%>
<script>
  self.close();
</script></body></html>
<%      return;
    }  %>
<%
    if (showHistory = true) {
        eIrbDao.getSubmissionHistory(EJBUtil.stringToNum(submissionPK),
                EJBUtil.stringToNum(submissionBoardPK));
        ArrayList historyDates = eIrbDao.getHistoryDates();
        ArrayList historyStatusDescs = eIrbDao.getHistoryStatusDescs();
        ArrayList historyEnteredByList = eIrbDao.getHistoryEnteredByList();
        ArrayList historyNotesList = eIrbDao.getHistoryNotesList();
%>
<table width="100%" cellspacing="0" cellpadding="0" Border="0">
<tr><td><P class="defComments"><b><%=LC.L_Study_Number%><%--<%LC.Std_Study%> Number*****--%>: </b><%=studyNumber%></P></td></tr>
<tr>  <td>&nbsp;&nbsp;</td></tr>
<tr><td><P class="defComments"><b><%=LC.L_Study_Title%><%--<%=LC.Std_Title%> Title*****--%>: </b><%=studyTitle%></P></td></tr>
<tr>  <td>&nbsp;&nbsp;</td></tr>
<tr><td><P class="sectionHeadings"><%=LC.L_Submission_History%><%--Submission History*****--%></P></td></tr>
</table>
<table width="100%" class="basetbl outline" align="center" cellspacing="0" cellpadding="0" border="0">
<tr>
<th width="20%"><%=LC.L_Submission_Date%><%--Submission Date*****--%></th>
<th width="30%"><%=LC.L_Submission_Status%><%--Submission Status*****--%></th>
<th width="15%"><%=LC.L_Entered_By%><%--Entered By*****--%></th>
<th width="35%"><%=LC.L_Notes%><%--Notes*****--%></th>
</tr> 
<%
        for (int iX=0; iX<historyStatusDescs.size(); iX++) {
            String notes = StringUtil.trueValue((String)historyNotesList.get(iX));
            notes = notes.replaceAll("&lt;br/&gt;","\n");
%>
        <tr align="left" valign="top"  class="plainrow" >
        <td  width="20%"><%=historyDates.get(iX) %></td>
        <td  width="30%"><%=historyStatusDescs.get(iX)%></td>
        <td  width="15%"><%=historyEnteredByList.get(iX) %></td>
        <td  width="35%"><%=notes%></td>
        </tr>
<%
        }
%>
</table>

<%
    }
} 
else { // session invalid
%>
<jsp:include page="timeout_childwindow.jsp" flush="true"/>
<%
}
%>
  <div class = "myHomebottomPanel">
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
</body>
</html>