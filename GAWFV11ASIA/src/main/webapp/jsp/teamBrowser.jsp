<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%
boolean isIrb = "irb_personnel_tab".equals(request.getParameter("selectedTab")) ? true : false;
if (isIrb) {
%>
<title><%=MC.M_ResComp_NewStdPers%><%--Research Compliance >> New Application >> <%=LC.Std_Study%> Personnel*****--%></title>
<% } else { %>
<title><%=LC.L_Study_Team%><%--<%=LC.Std_Study%> >> Team*****--%></title>
<% } %>
<%@ page import="com.velos.eres.service.util.*"%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.studyRights.StudyRightsJB" %>
<jsp:useBean id="sv" scope="request" class="com.velos.eres.web.studyView.StudyViewJB"/>
<SCRIPT language="javascript">
var screenWidth = screen.width;
var screenHeight = screen.height;

	function f_check_user(pright,mode,userType) {
		if (f_check_perm(pright,mode) == false)	{
			return false;
		}
	 //KM-to fix the Bug 2556(Not to allow deleted Non-System Study Team User to edit access rights.)

	 //KM-Modified for Enh.#S17

		if( userType == 'N' || userType=='X') {
		   alert("<%=MC.M_NonSysUsr_NotAces%>");/*alert("This user is a Non-System user and therefore does not have any access to the system");*****/
		   return false;
		}
	}
	function checkDelete(pright, mode, username, curTeam, studyDataMgr) {
		//check if user has right to delete
		//prompt a confirmation box for delete
		//check - user defined as the datamananager in study summary can not be deleted
		//alert("on delete");
		if (f_check_perm(pright,mode) == false){
			return false;
		}
		var paramArray = [username];
		if (curTeam == studyDataMgr){
			alert(getLocalizedMessageString("M_UsrDataMgr_CntRemFrmTeam",paramArray));/*alert(username + " is identified as Data Manager in <%=LC.L_Study%> Summary. This user can not be removed from the <%=LC.L_Study%> Team.");*****/
			return false;
		}
		if (confirm(getLocalizedMessageString("M_Del_UserFromStudyTeam",paramArray))){/*if (confirm("Delete '" + username + "' from <%=LC.L_Study%> Team?")){*****/
			return true;
		}else{
			return false;
		}

	}

	//Modified by Manimaran to fix the Bug2730
	//JM: 18Jan2008: #3203
	function checkDeleteSite(pright, mode, username,dmSiteId, siteId, dmUserName, enrPatCount, subType,
	activeCount, studyStatDesc, currStat, totalActiveCount) {


		if(dmSiteId == siteId){
			var paramArray = [dmUserName];
			alert(getLocalizedMessageString("M_InOrgIsManager_OrgCntDel",paramArray));/*alert(dmUserName+" in this Organization is Data Manager in <%=LC.L_Study%> Team. This organization cannot be deleted.");*****/
			return false;
		}
		if (f_check_perm(pright,mode) == false){
			return false;
		}

		if(enrPatCount>0)
		{
			alert("<%=MC.M_PatCurEnrl_CanNotDelOrgs%>");
			return false;
		}

  /*
  		var paramArray = [studyStatDesc];
		//Amarnadh
		//JM:  18Jan2008: #3203, Modified
		if ((activeCount == 1) && (enrPatCount > 0) && (subType == 'active') && totalActiveCount == 1)
			{
				//JM: 01Sept2008, added for, issue #3203
				alert(getLocalizedMessageString("M_PatCurEnrl_CanNotDelOrg",paramArray));/*alert("A <%=LC.Pat_Patient%> is currently enrolled to the <%=LC.L_Study%> and '"+studyStatDesc+"' status is linked to this Organization and hence cannot delete this Organization");*****/
				/*return false;
			}

		//JM: 08Jan2009, #3203
		if ((enrPatCount > 0) && (totalActiveCount == activeCount)){
			alert(getLocalizedMessageString("M_OnlyOrg_OneMore_OrgCntDel",paramArray));/*alert("Only Organization where one or more '"+studyStatDesc+"' status is linked and hence cannot delete this Organization");*****/
			/*return false;
		}

	*/

		if (currStat == 1) {
			alert("<%=MC.M_CntDelOrg_HasCurStdStat%>");/*alert("You cannot delete the organization that has current <%=LC.Std_Study_Lower%> status");*****/
			return false;
                }
     	var paramArray = [username];
		if (confirm(getLocalizedMessageString("M_Del_UserFromStudyTeam",paramArray))){/*if (confirm("Delete '" + username + "' from <%=LC.L_Study%> Team?")){*****/
			return true;
		} else{
			return false;
		}
	}
	function openwindowsupuser(study,pright){

			windowname=window.open("getTeamSupUser.jsp?studyId=" + study ,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=500,height=350 top=120,left=200 0, ");
			windowname.focus();

	}
//Added by Gopu to fix the bugzilla Issue #2665
	function openNewOrgWin(formobj,studyId,studySiteId,mode,siteId,pright){

		var chkMode = mode;

		if (chkMode == 'M')
		{
			chkMode = 'E'
		}
		if (f_check_perm(pright,chkMode) == false){
			return false;
		} else {
			param1="newOrganization.jsp?studyId=" + studyId+"&mode="+mode+"&studySiteId="+studySiteId+"&siteId="+siteId;
			windowName= window.open(param1,"information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=850,height=600,top=50,left=140");
			windowName.focus();
		}
	}
	//Added by Manimaran for the July-August Enhancement S4.
	function openeditstatus(teamId,statusId,studyId,teamStat,mode,userName,pright,emode,userType){
	//Added by Manimaran to fix the Bug2716
	//if (f_check_user(pright,emode,userType)==false) {
	  //  return false;
	//}
	//KM
	if (mode == 'M')
		chkMode = 'E';
	else
		chkMode = 'N';

	if (f_check_perm(pright,chkMode) == false)	{  //KM-Allowing NonSystem user to change the status.
			return false;
	}

	windowname=window.open("editStudyTeamStatus.jsp?moduleTable=er_studyteam&statusCodeType=teamstatus&teamId="+teamId+"&statusId="+statusId+"&studyId=" + studyId+"&teamStat="+teamStat+"&mode="+mode+"&userName='"+userName + "'" ,"Information","toolbar=no,resizable=yes,scrollbars=no,menubar=no,status=yes,width=650,height=350 top=120,left=200, ");
		windowname.focus();
	}
	function openeditstudyteam(teamId,studyId,mode,pright,emode){

		if (f_check_perm(pright,emode) == false)	{  //KM-Allowing NonSystem user to change the status.
				return false;
		}

		windowname=window.open("newSTMember.jsp?moduleTable=er_studyteam&teamId="+teamId+"&studyId=" + studyId+"&mode="+mode ,"studtTeamModify","toolbar=no,resizable=yes,scrollbars=no,menubar=no,status=yes,width=650,height=350 top=120,left=200, ");
		windowname.focus();
		}
	</SCRIPT>
</head>

<%
	String src;
	src= request.getParameter("srcmenu");
	String from = "team";

		String studyIdForTabs = "";
 	studyIdForTabs = request.getParameter("studyId");

%>
<jsp:include page="panel.jsp" flush="true">
	<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0">
<jsp:useBean id="studyStatB" scope="request" class="com.velos.eres.web.studyStatus.StudyStatusJB"/><!--JM:-->
	<jsp:useBean id="teamB" scope="session" class="com.velos.eres.web.team.TeamJB"/>
	<jsp:useBean id="studySiteB" scope="session" class="com.velos.eres.web.studySite.StudySiteJB"/>
	<jsp:useBean id="userB" scope="session" class="com.velos.eres.web.user.UserJB"/>
	<jsp:useBean id="studyB" scope="session" class="com.velos.eres.web.study.StudyJB"/>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<jsp:useBean id="addressUserB" scope="page" class="com.velos.eres.web.address.AddressJB" />
	<%@ page language = "java" import = "com.velos.eres.business.team.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil, com.velos.eres.service.util.StringUtil"%>

<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
	<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
	<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
	<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<%
   HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession)){
			String ipAdd = (String) tSession.getValue("ipAdd");
			String usr = null;
			usr = (String) tSession.getValue("userId");
	%>
	<DIV class="BrowserTopn" id="divtab">
    <% String includeTabsJsp = isIrb ? ("LIND".equals(CFG.EIRB_MODE) ? "ecompNewTabs.jsp" : "irbnewtabs.jsp") : "studytabs.jsp"; %>
	  <jsp:include page="<%=includeTabsJsp%>" flush="true">
	  <jsp:param name="from" value="<%=from%>"/>
	  	<jsp:param name="studyId" value="<%=studyIdForTabs%>"/>
	   </jsp:include>
	</DIV>

<%
String stid= request.getParameter("studyId");
if(stid==null || stid=="" || "0".equals(stid)){%>
<SCRIPT LANGUAGE="JavaScript">
	if(screenWidth>1280 || screenHeight>1024)
		document.write('<DIV class="BrowserBotN BrowserBotN_RC_1" id="div1"  style="height:77%;">')
	else
		document.write('<DIV class="BrowserBotN BrowserBotN_RC_1" id="div1">')
</SCRIPT>
	<%} else {%>
<SCRIPT LANGUAGE="JavaScript">
/* Condition to check configuration for workflow enabled */
<% if("Y".equals(CFG.Workflows_Enabled) && (stid!=null && !stid.equals("") && !stid.equals("0"))){ %>
		if(screenWidth>1280 || screenHeight>1024)
			document.write('<DIV class="BrowserBotN BrowserBotN_S_3 workflowDivBig" id="div1" style="height:75%; top:140px;" >')
		else
			document.write('<DIV class="BrowserBotN BrowserBotN_S_3 workflowDivSmall" id="div1">')
<% } else{ %>
		if(screenWidth>1280 || screenHeight>1024)
			document.write('<DIV class="BrowserBotN BrowserBotN_S_3" id="div1" style="height:77%;" >')
		else
			document.write('<DIV class="BrowserBotN BrowserBotN_S_3" id="div1"style="height:84%;">')
<% } %>
</SCRIPT>
<%}

		String uName = (String) tSession.getValue("userName");
		String study = (String) tSession.getValue("studyId");
		String studyDm  = "";
		if(study == null)
			study = "";
		if(!study.equals("")){
			studyB.setId(EJBUtil.stringToNum(study));
			studyB.getStudyDetails();
			studyDm  = studyB.getStudyAuthor();
		}
		String  userAddPhone="";
		String  userEmail = "";
		String dmSiteId = "";
		String dmUserName = "";
		String acc = (String) tSession.getValue("accountId");
		String uId = (String) tSession.getValue("userId");
		String tab = request.getParameter("selectedTab");
		StudyRightsJB stdRights = new StudyRightsJB();
		//String studyDm = (String) tSession.getAttribute("studyDataMgr");
		if(!study.equals("")){
			userB.setUserId(EJBUtil.stringToNum(studyDm));
			userB.getUserDetails();
			dmSiteId = userB.getUserSiteId();
			//System.out.println("dmSiteId ======== "+dmSiteId);
			dmUserName =  userB.getUserFirstName() +" "+ userB.getUserLastName();
			dmUserName = StringUtil.escapeSpecialCharJS(dmUserName);
		}
		int pageRight = 0;
		int studyDataMgr = 0;
		String dOrg = "";
		// change by salil cut and pasted in else part of check study null
	%>
	<%
		if(study == "" || study == null || "0".equals(study)||"0".equals(study)||study.equalsIgnoreCase("")) {
	%>
		<jsp:include page="studyDoesNotExist.jsp" flush="true"/>
	<%
		} else {
				// change by salil pasted here
				stdRights =(StudyRightsJB) tSession.getValue("studyRights");
					if ((stdRights.getFtrRights().size()) == 0){
						pageRight= 0;
					}else{
						pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYTEAM"));
					}
				TeamDao teamDao = new TeamDao();
				StudySiteDao studySiteDao = new StudySiteDao();
				StudyStatusDao studyStatDao = new StudyStatusDao();
				if (pageRight > 0 ){
					String selName = null;
					StringBuffer sb = new StringBuffer();
					Integer org ;
					String ddSite = "";
					String selOrg = request.getParameter("accsites") ;
					if(selOrg == null || selOrg.equals(""))
						selOrg = "0";
					int selOrgId = EJBUtil.stringToNum(selOrg);
				//	CodeDao cd  = new CodeDao();
					int accId = EJBUtil.stringToNum(acc);
					//System.out.println("accId ======== "+accId);
					int enrPatCount = 0;//JM
				//	cd.getAccountSites(accId);
					int studyId = EJBUtil.stringToNum(study);
					studySiteDao = studySiteB.getStudySiteTeamValues(studyId,selOrgId,EJBUtil.stringToNum(uId),EJBUtil.stringToNum(acc));
					ArrayList studySiteIds = studySiteDao.getStudySiteIds();
					ArrayList studySiteApndxCnts = studySiteDao.getStudySiteApndxCnts();
					ArrayList enrPatCnts = studySiteDao.getstudypatCnts();
					ArrayList siteIds1 = studySiteDao.getSiteIds();
					ArrayList siteTypes = studySiteDao.getStudySiteTypes();
					ArrayList localSizes = studySiteDao.getLSampleSize();
					ArrayList siteNames = studySiteDao.getSiteNames();
					ArrayList siteAccess = studySiteDao.getSiteAccess();
					ArrayList currStats = studySiteDao.getCurrStats();//KM
					int len1 = studySiteIds.size();
					//teamDao = teamB.getTeamValues(studyId, accId);
					//to be implemented
					teamDao = teamB.getTeamValuesBySite(studyId, accId, selOrgId);
					ArrayList teamIds 		= teamDao.getTeamIds();
					ArrayList teamRoles 		= teamDao.getTeamRoles();
					ArrayList teamUserLastNames 	= teamDao.getTeamUserLastNames();
					ArrayList teamUserFirstNames = teamDao.getTeamUserFirstNames();
					ArrayList userSiteNames = teamDao.getUsrSiteNames();
					ArrayList teamRights 	= teamDao.getTeamRights();
					ArrayList inexUsers	 	= teamDao.getInexUsers();
					ArrayList userIds = teamDao.getUserIds();
					ArrayList siteIds2 = teamDao.getSiteIds();
					//Added by IA 11.03.2006
					ArrayList userPhones = teamDao.getUserPhone();
					ArrayList userEmails = teamDao.getUserEmail();
					ArrayList userTypes = teamDao.getUserType();
					//End Added
					//km
					ArrayList teamStatus =teamDao.getTeamStatus();
					ArrayList teamStatusIds=teamDao.getTeamStatusIds();
					ArrayList teamUserTypes = teamDao.getTeamUserTypes();
					StudySiteDao ssDao = new StudySiteDao();
					ssDao	= studySiteB.getAllStudyTeamSites(studyId, accId);
					ArrayList arrSiteIds = ssDao.getSiteIds();
					ArrayList arrSiteNames = ssDao.getSiteNames();
					sb.append("<SELECT NAME='accsites'>") ;
					sb.append("<OPTION value=''>"+LC.L_All+"</OPTION>");/*sb.append("<OPTION value=''>All</OPTION>");*****/
					if (arrSiteIds.size() > 0){
						for (int counter = 0; counter <= arrSiteNames.size() -1 ; counter++){
							org = (Integer) arrSiteIds.get(counter);
							if(	org.intValue() == selOrgId)	{
								sb.append("<OPTION value = "+ org+" SELECTED>" + arrSiteNames.get(counter)+ "</OPTION>");
							}else{
								sb.append("<OPTION value = "+ org+">" + arrSiteNames.get(counter)+ "</OPTION>");
							}
						}
					}
					sb.append("</SELECT>");
					dOrg = sb.toString();
					String teamRole = null;
					String teamUserLastName = null;
					String teamUserFirstName = null;
					String teamRight = null;
					String inexUser = null;
					String paramName = null;
					String teamUserType = null;
					String siteName  = null;
					String siteId1 = null;
					String siteId2 = null;
					String siteAcc = "";
					String studySiteId = null;
					String studySiteApndxCnt= null;
					int istudySiteApndxCnt = 0;
					String siteType= null;
					String localSize = null;
					String currStat = null;//KM
					String subType = null;//Amarnadh
					boolean flag = true;
					int teamId = 0;
					int userId = 0;
					int len = teamIds.size();
					int counter = 0;
					int activeCount = 0; // Amarnadh
					int activeCountTmp = 0; // JM
					String usr_id = "";
					String orgName = "";
					String userType = "";
					String teamStat="";
					int teamStatusId=0;

//JM: 18Jan2008
					ArrayList subTypeStatusLst = null;
					ArrayList studyStatusIdLst = null;
					ArrayList studyStatusDescs = null;

		  			CodeDao  cd = new CodeDao();
		  			cd.getCodeId("studystat","active");
		  			String active = cd.getCodeDescription();


					String studyStatusId = "";
					String studyStatDesc = "";


					UserDao userDao = new UserDao();


					int totalActiveCount = 0;//JM



	%>
			  <Form name="teambrowser" method="post" action="" onsubmit="">
			  <div class="tmpHeight"></div>
					<Input type="hidden" name="selectedTab" value="<%=tab%>">
					<table width="99%" border="0" cellspacing="0" cellpadding="0" class="basetbl outline midAlign" >
					<tr height="7"></tr>
						<tr>
							<td width ="20%">
							<%=LC.L_Search_ByOrg%><%--Search by Organization*****--%>
							</td>
							<td width = "15%"><%=dOrg%></td>
							<td width = "15%">
								<P><button type="submit"><%=LC.L_Search%></button></P>
							</td>
							<td width = "35%" align="right">
								<!--            <A href="modifyTeam.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&right=<%=pageRight%>" >Click to add/edit study team users</A>  -->
								<A href="#" onClick="openwindowsupuser(<%=studyId%>,'<%=pageRight%>');"><%=MC.M_ViewSU_AcesToStd%><%--View Super Users with access to this <%=LC.Std_Study%>*****--%></A>
								</P>
							</td>
						</tr>
					</table>
					<table>
	<%
						int cnt1 = 0;
						int cnt2 = 0;
						int userCount = 0;
						int last=0;
						//added by ia 11.03.2006
						String userName = "";
						int flag1 = 0;
						int usrrecords = 0;
						int records = 0;
						//end added
						for(counter = 0;counter < len; counter++){
								siteId2=((siteIds2.get(counter)) == null)?"-":(siteIds2.get(counter)).toString();
							for(cnt1 = 0;cnt1 < len1; cnt1++){
								siteId1=((siteIds1.get(cnt1)) == null)?"-":(siteIds1.get(cnt1)).toString();
							if(siteId1.equals(siteId2))
										 {userCount++;}
							}
						}


/***********************************/
//JM: 05Nov2008: #3203


//commented by Sonia 05/21/09 - performAnce hurdle
//***************************************************************************************************************
/* for(cnt1 = 0;cnt1 < len1; cnt1++){

	siteId1=((siteIds1.get(cnt1)) == null)?"-":(siteIds1.get(cnt1)).toString();
	studyStatDao = studyStatB.getStudyStatusDesc(studyId, EJBUtil.stringToNum(siteId1), EJBUtil.stringToNum(uId), EJBUtil.stringToNum(acc));

	studyStatusIdLst = studyStatDao.getIds();
	int lenStudy = studyStatusIdLst.size();

	subTypeStatusLst = studyStatDao.getSubTypeStudyStats();

	for(int tmpCnt = 0;tmpCnt < lenStudy; tmpCnt++){

		subType =(((subTypeStatusLst.get(tmpCnt)) == null)?"-":(subTypeStatusLst.get(tmpCnt)).toString()).trim();

		if (subType.equals("active")){
			activeCountTmp++;
		}


	}

	totalActiveCount = activeCountTmp;

} */
//***************************************************************************************************************
String tmpSiteName="-";

						for(cnt1 = 0;cnt1 < len1; cnt1++){
							records = 0;
							siteId1=((siteIds1.get(cnt1)) == null)?"-":(siteIds1.get(cnt1)).toString();
							siteName=((siteNames.get(cnt1)) == null)?"-":(siteNames.get(cnt1)).toString();
							if(!siteName.equals("-")){
								tmpSiteName=StringUtil.escapeSpecialCharJS(siteName);
							}
							siteAcc=((siteAccess.get(cnt1)) == null)?"-":(siteAccess.get(cnt1)).toString();
							studySiteId=((studySiteIds.get(cnt1)) == null)?"-":(studySiteIds.get(cnt1)).toString();
							studySiteApndxCnt=((studySiteApndxCnts.get(cnt1)) == null)?"":(studySiteApndxCnts.get(cnt1)).toString();
							istudySiteApndxCnt = EJBUtil.stringToNum(studySiteApndxCnt);
							siteType=((siteTypes.get(cnt1)) == null)?"-":(siteTypes.get(cnt1)).toString();
							localSize =((localSizes.get(cnt1)) == null)?"-":(localSizes.get(cnt1)).toString();
							//Added by Manimaran to fix the Bug2730
							currStat = ((currStats.get(cnt1)) == null)?"":(currStats.get(cnt1)).toString();
							enrPatCount = (Integer)enrPatCnts.get(cnt1);
					//JM: 18Jan2008: added


					 /*studyStatDao = studyStatB.getStudyStatusDesc(studyId, EJBUtil.stringToNum(siteId1), EJBUtil.stringToNum(uId), EJBUtil.stringToNum(acc));


							studyStatusIdLst = studyStatDao.getIds();
							int lenStudy = studyStatusIdLst.size();

							subTypeStatusLst = studyStatDao.getSubTypeStudyStats();
							enrPatCount = studyStatDao.getEnrPatCnt();

//JM: 08JAN2009
							subType = "";
							activeCount = 0;

							for(int tmpCnt = 0;tmpCnt < lenStudy; tmpCnt++){

									subType =(((subTypeStatusLst.get(tmpCnt)) == null)?"-":(subTypeStatusLst.get(tmpCnt)).toString()).trim();


									//JM: 18Jan2008: added
									if (subType.equals("active")){
										activeCount++;
									}


							} */




						for(counter=0 ; counter<len; counter++) {



							flag1 = 0;
							usrrecords = 0;
							teamId=EJBUtil.stringToNum(((teamIds.get(counter)) == null)?"-":(teamIds.get(counter)).toString());
							teamRole=((teamRoles.get(counter)) == null)?"-":(teamRoles.get(counter)).toString();
							teamUserLastName=((teamUserLastNames.get(counter)) == null)?"-":(teamUserLastNames.get(counter)).toString();
							teamUserFirstName=((teamUserFirstNames.get(counter)) == null)?"-":(teamUserFirstNames.get(counter)).toString();
							teamRight=((teamRights.get(counter)) ==null)?"-":(teamRights.get(counter)).toString();
							inexUser=inexUsers.get(counter).toString();
							selName =  teamUserFirstName+" "+teamUserLastName;
							siteId2=((siteIds2.get(counter)) == null)?"-":(siteIds2.get(counter)).toString();
							paramName = "'" +  StringUtil.escapeSpecialCharJS(teamUserFirstName+" "+teamUserLastName) + "'";
							userId = ((Integer)userIds.get(counter)).intValue();
							teamStat=((teamStatus.get(counter)) == null)?"-":(teamStatus.get(counter)).toString();
							teamStatusId =EJBUtil.stringToNum(((teamStatusIds.get(counter)) == null)?"-":(teamStatusIds.get(counter)).toString());
							userName= teamUserFirstName+" "+teamUserLastName;
							userName = StringUtil.escapeSpecialCharJS(userName);
							paramName =paramName;

						//	userB.setUserId(userId);
						//	userB.getUserDetails();
						//	if (userB.getUserPerAddressId() != null) {												addressUserB.setAddId(EJBUtil.stringToNum(userB.getUserPerAddressId()));
						//	addressUserB.getAddressDetails();
						//	}
							userAddPhone= ((userPhones.get(counter)) == null)?"":(userPhones.get(counter)).toString();
							userEmail = ((userEmails.get(counter)) == null)?"":(userEmails.get(counter)).toString();
							userType = ((userTypes.get(counter)) == null)?"":(userTypes.get(counter)).toString();

							studyDataMgr = EJBUtil.stringToNum(studyDm);
							teamUserType = (String) teamUserTypes.get(counter);
							orgName = (String)userSiteNames.get(counter);

							selName = StringUtil.escapeSpecialCharJS(selName);

							if (orgName == null){
								orgName ="-";
							}
							if(inexUser.equals("1")){
								if (flag == true ){
									flag = false;
	%>
											<table width="99%" cellspacing="0" cellpadding="0" border="0" class="lhsFont">
												<tr><td>&nbsp;</td></tr>
												<tr>
												<td>&nbsp;</td>
													<td width = "55%">
														 <P class="sectionheadings"><%=LC.L_Std_Team%><%--<%=LC.Std_Study%> Team*****--%></P>
													</td>
													<td>
														<A href="#" onClick="openNewOrgWin(document.formlib,'<%=studyId%>','','N','','<%=pageRight%>')">	<%=LC.L_Add_NewOrg_Upper%><%--ADD NEW ORGANIZATION*****--%></A>
													</td>
													<td>
														<A href="modifyTeam.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&right=<%=pageRight%>&studyId=<%=stid%>"  ><%=MC.M_AddEdtStd_TeamMem_Upper%><%--ADD/EDIT <%=LC.Std_Study_Upper%> TEAM MEMBER*****--%></A>
													</td>
												</tr>
											</table>
											<table width="99%" border="0" cellspacing="0" cellpadding="0" class="outline midAlign" >
											<tr>
												<th width="20%"> <%=LC.L_Organization%><%--Organization*****--%> </th>
												<th width="20%"> <%=LC.L_User_Name%><%--User Name*****--%> </th>
												<th width="20%"> <%=LC.L_Role%><%--Role*****--%> </th>
												<th width="15%"> <%=LC.L_Access_Rights%><%--Access Rights*****--%> </th>
												<th> <%=LC.L_Status%><%--Status*****--%> </th>
												<th><%=LC.L_Delete%></th>
											</tr>
						<%
									} // end of if for flag check
								if(siteId1.equals(siteId2)) {
									records++;
								 }
								if(siteId1.equals(siteId2) ){
									//if(cnt1 == 0)
									if(counter == 0 ){
									//{//println name and users
						%>
<!-- Added by Gopu to fix the bugzilla Issue #2665 -->									<td>
<%
//JM: 16Jan2007: modified
//sonia, 01/29/07 removed Jnanamay's check, we need to check edit right in any case
%>
<tr>
	<td><P class = "defComments"><A href="#" onClick="openNewOrgWin(document.formlib,'<%=studyId%>','<%=studySiteId%>','M','<%=siteId1%>','<%=pageRight%>')">
											<%=siteName%></A>
		</P>
	</td>
	<td align="center">
	<b><%=siteType%></b><%if(istudySiteApndxCnt > 0){%><img src="../images/jpg/icon_clip.gif" ><%}%>
	</td>
	<td><B><%=LC.L_Local_SampleSize%><%--Local Sample Size*****--%>: <%=localSize%></B></td>
	<td></td>
	<td><%if(siteAcc.equals("1") && !(isIrb)){ %>
		<A  href="studystatusbrowser.jsp?srcmenu=<%=src%>&selectedTab=3&studyId=<%=studyId%>&selsite=<%=siteId1%>"><%=LC.L_Track_StdStatus%><%--Track <%=LC.Std_Study%> Status*****--%></A> <%}%>
	</td>
	

	<td align="center"><!--Modified by Manimaran to fix the Bug2730 -->&nbsp;<A  href="deleteStudySite.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&studySiteId=<%=studySiteId%>&right=<%=pageRight%>&userName=<%=selName%>&selsite=<%=siteId1%>&studyId=<%=studyId%>" onClick="return checkDeleteSite(<%=pageRight%>,'E','<%=tmpSiteName%>',<%=dmSiteId%>,<%=siteId1%>,'<%=dmUserName%>',<%=enrPatCount%>,'<%=subType%>',<%=activeCount%>,'<%=active%>',<%=currStat%>, <%=totalActiveCount%>);"><img src="./images/delete.gif" border="0"  title="<%=LC.L_Delete%>" alt="<%=LC.L_Delete%>"/></A></td>
	</tr>
									 <%if ((counter%2)==0) {%>
										<tr class="browserEvenRow">
									<%}else{%>
										<tr class="browserOddRow">
									<%}%>
									<td></td>
									<td><table width="100%"><tr><td width="30%" style="border: 0px;">
										<a href="mailto:<%=userEmail%>" onmouseover="return overlib('<%=LC.L_Phone%><%--Phone*****--%>: <%=userAddPhone%><br><%=LC.L_Email%><%--Email*****--%>: <%=userEmail%>',CAPTION,'<%=LC.L_User_Dets%><%--User Details*****--%>');" onmouseout="return nd();"><font size="2" weight="bold"><img title="<%=LC.L_Email%>" src="./images/Email.gif" border ="0"><!--@--></font></a>
										<a href="#" onClick="openeditstudyteam(<%=teamId%>,<%=studyId%>,'M',<%=pageRight%>,'E')"><img title="<%=LC.L_More_Info%>" src="./images/moreInfo.png" border ="0"><%//=LC.L_C%><%--C*****--%></a>
										</td><td style="border: 0px;">&nbsp;&nbsp;<%=teamUserFirstName%> <%=teamUserLastName%></td></tr></table>
									</td>
									<td> <%=teamRole%> </td>
									<td  align="center">
										<A href="studyrights.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&teamId=<%=teamId%>&right=<%=pageRight%>&userName=<%=selName%>&studyId=<%=studyId%>&userId=<%=userId%>" onClick="return f_check_user(<%=pageRight%>,'E','<%=userType%>');"><img title="<%=LC.L_Assign_Rights%>" src="./images/AccessRights.gif" border ="0"><%//=LC.L_Assign_Rights%><%--Access Rights*****--%></A>
									</td>
									 <!--Added by Manimaran for the July-August Enhancement S4.	-->
									<td  class="trheight">
									<!--Modified by Manimaran to fix the Bug2716 -->
									<table width="100%">
									  <tr>
									    <td width="50%" style="border: 0px;">
										<a href="#" onClick="openeditstatus(<%=teamId%>,<%=teamStatusId%>,<%=studyId%>,'<%=teamStat%>','N','<%=  userName %>',<%=pageRight%>,'E','<%=userType%>')"><img title="<%=LC.L_Edit%>" src="./images/edit.gif" border ="0"><%//=LC.L_C%><%--C*****--%></a>
										<A href="#" onClick="openeditstatus(<%=teamId%>,<%=teamStatusId%>,<%=studyId%>,'<%=teamStat%>','M','<%= userName %>',<%=pageRight%>,'E','<%=userType%>')"> <%=teamStat%></A> &nbsp;										
										</td>
										<td style="border: 0px;">
										<A href="showStudyTeamHistory.jsp?modulePk=<%=teamId%>&pageRight=<%=pageRight%>&srcmenu=<%=src%>&selectedTab=<%=tab%>&page=<%=page%>&from=studyTeamHistory&fromjsp=showStudyTeamHistory.jsp&userName=<%=userName %>&studyId=<%=studyId%>" ><img border="0" title="<%=LC.L_History%>" alt="<%=LC.L_History%>" src="./images/History.gif" ><%//=LC.L_H %><%--H*****--%></A>
										</td>
								       </tr>
									</table>									
									</td>
									
									<td  align="center"> 
										<% if( ("LIND".equals(CFG.EIRB_MODE) )) {
										 String[] result = CFG.Study_Role_Filter_Subtypes.split(",");
										  String[] codeSubTypes= new String[result.length];		  
									      for (int x=0; x<result.length; x++) {
									         codeSubTypes[x]=result[x];		         
									      }	
									    boolean found = false;
										CodeDao cdRole = new CodeDao(); 
										for( int i=0 ; i < codeSubTypes.length ; i++) {
											if(teamRole.equals(cdRole.getCodeDesc(cdRole.getCodeId("role", codeSubTypes[i]))) ) { 											
												found = true;
											}
										}
									 
										if( !found ) { %>
											<A href="deletefromteam.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&teamId=<%=teamId%>&right=<%=pageRight%>&userName=<%=selName%>&studyId=<%=studyId%>&userId=<%=userId%>" onClick="return checkDelete(<%=pageRight%>,'E', '<%=  userName%>',<%=userId%>,<%=studyDataMgr%>);"><img src="./images/delete.gif" border ="0"  title="<%=LC.L_Rem_FromTeam%>" alt="<%=LC.L_Rem_FromTeam%>"/><%//=LC.L_Rem_FromTeam%><%--Remove from Team*****--%></A>
										<% }										
									} else { %> 
										<A href="deletefromteam.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&teamId=<%=teamId%>&right=<%=pageRight%>&userName=<%=selName%>&studyId=<%=studyId%>&userId=<%=userId%>" onClick="return checkDelete(<%=pageRight%>,'E', '<%=  userName%>',<%=userId%>,<%=studyDataMgr%>);"><img src="./images/delete.gif" border ="0"  title="<%=LC.L_Rem_FromTeam%>" alt="<%=LC.L_Rem_FromTeam%>"/><%//=LC.L_Rem_FromTeam%><%--Remove from Team*****--%></A>
									<% } %>
									</td>
								</tr><%
									}else if(counter > 0 && !siteId2.equals(siteIds2.get(counter-1).toString())){
									//{//println name and users
									%>
							<tr>
								<tr>
<!-- Added by Gopu to fix the bugzilla Issue #2665 -->									<td>
<%
//JM: 16JAN2007, changed by sonia, check edit in any case
if(pageRight == 5){
%>
<P class = "defComments"><A href="#" onClick="openNewOrgWin(document.formlib,'<%=studyId%>','<%=studySiteId%>','M','<%=siteId1%>','<%=pageRight%>')"><%=siteName%></A>
		<% } else { %>								<P class = "defComments"><A href="#" onClick="openNewOrgWin(document.formlib,'<%=studyId%>','<%=studySiteId%>','M','<%=siteId1%>','<%=pageRight%>')"><%=siteName%></A>
										</P>
<%}%>
									</td>
									<td  align="center">
										<b><%=siteType%></b><%if(istudySiteApndxCnt > 0){%>
										<img align="right" src="../images/jpg/icon_clip.gif" ><%}%>
									</td>
									<td><B><%=LC.L_Local_SampleSize%><%--Local Sample Size*****--%>: <%=localSize%></B></td>
									<td><%if(siteAcc.equals("1")){%>
										<A href="studystatusbrowser.jsp?srcmenu=<%=src%>&selectedTab=3&studyId=<%=studyId%>&selsite=<%=siteId1%>"><%=LC.L_Track_StdStatus%><%--Track <%=LC.Std_Study%> Status*****--%></A><%}%></td>
									 <td> </td>
									 <td  align="center">   <!--Modified by Manimaran to fix the Bug2730 -->
										&nbsp;<A href="deleteStudySite.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&studySiteId=<%=studySiteId%>&right=<%=pageRight%>&userName=<%=selName%>&selsite=<%=siteId1%>&studyId=<%=studyId%>" onClick="return checkDeleteSite(<%=pageRight%>,'E', '<%=tmpSiteName%>',<%=dmSiteId%>,<%=siteId1%>,'<%=dmUserName%>',<%=enrPatCount%>,'<%=subType%>',<%=activeCount%>,'<%=active%>',<%=currStat%>, <%=totalActiveCount%>);"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0"/></A></td>
											</tr>
									 <%if ((counter%2)==0) {%>
											<tr class="browserEvenRow">
										<%} else {
										%>
											<tr class="browserOddRow">
										<%}%><td></td>
									 <td  align="center"><table width="100%"><tr><td width="30%" style="border: 0px;">
										<a href="mailto:<%=userEmail%>" onmouseover="return overlib('<%=LC.L_Phone%><%--Phone*****--%>: <%=userAddPhone%><br><%=LC.L_Email%><%--Email*****--%>: <%=userEmail%>',CAPTION,'<%=LC.L_User_Dets%><%--User Details*****--%>');" onmouseout="return nd();"><font size="2" weight="bold"><img title="<%=LC.L_Email%>" src="./images/Email.gif" border ="0"><!--@--></font></a>
										<a href="#" onClick="openeditstudyteam(<%=teamId%>,<%=studyId%>,'M',<%=pageRight%>,'E')"><img title="<%=LC.L_More_Info%>" src="./images/moreInfo.png" border ="0"><%//=LC.L_C%><%--C*****--%></a>
										</td><td style="border: 0px;"> &nbsp;&nbsp;<%=teamUserFirstName%> <%=teamUserLastName%></td></tr></table>
									 </td>
									 <td><%=teamRole%></td>
									 <td  align="center">
										<A href="studyrights.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&teamId=<%=teamId%>&right=<%=pageRight%>&userName=<%=selName%>&studyId=<%=studyId%>&userId=<%=userId%>" onClick="return f_check_user(<%=pageRight%>,'E','<%=userType%>');"><img title="<%=LC.L_Assign_Rights%>" src="./images/AccessRights.gif" border ="0"><%//=LC.L_Assign_Rights%><%--Access Rights*****--%></A>
									</td>
								<!--Added by Manimaran for the July-August Enhancement S4.	-->
									 <td><table width="100%"><tr><td width="50%" style="border: 0px;">
									       <!--Modified by Manimaran to fix the Bug2703 -->
										<!--Modified by Manimaran to fix the Bug2716 -->
											<a href="#" onClick="openeditstatus(<%=teamId%>,<%=teamStatusId%>,<%=studyId%>,'<%=teamStat%>','N','<%=userName%>',<%=pageRight%>,'E','<%=userType%>')"><img title="<%=LC.L_Edit%>" src="./images/edit.gif" border ="0"><%//=LC.L_C%><%--C*****--%></a>
											<A href="#" onClick="openeditstatus(<%=teamId%>,<%=teamStatusId%>,<%=studyId%>,'<%=teamStat%>','M','<%=userName%>',<%=pageRight%>,'E','<%=userType%>')"><%=teamStat%></A> 									
										</td>
										<td style="border: 0px;">
											<A href="showStudyTeamHistory.jsp?modulePk=<%=teamId%>&pageRight=<%=pageRight%>&srcmenu=<%=src%>&selectedTab=<%=tab%>&page=<%=page%>&from=studyTeamHistory&fromjsp=showStudyTeamHistory.jsp&userName=<%=userName%>&studyId=<%=studyId%>" ><img border="0" title="<%=LC.L_History%>" alt="<%=LC.L_History%>" src="./images/History.gif" ><%//=LC.L_H %><%--H*****--%></A>
										</td>
										</tr>
									    </table>
								  </td>
									<td  align="center">
										<% if( ("LIND".equals(CFG.EIRB_MODE) )) {
										 String[] result = CFG.Study_Role_Filter_Subtypes.split(",");
										  String[] codeSubTypes= new String[result.length];		  
									      for (int x=0; x<result.length; x++) {
									         codeSubTypes[x]=result[x];		         
									      }	
									    boolean found = false;
										CodeDao cdRole = new CodeDao(); 
										for( int i=0 ; i < codeSubTypes.length ; i++) {
											if(teamRole.equals(cdRole.getCodeDesc(cdRole.getCodeId("role", codeSubTypes[i]))) ) { 											
												found = true;
											}
										}
									 
										if( !found ) { %>
											<A href="deletefromteam.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&teamId=<%=teamId%>&right=<%=pageRight%>&userName=<%=selName%>&studyId=<%=studyId%>&userId=<%=userId%>" onClick="return checkDelete(<%=pageRight%>,'E', <%=paramName%>,<%=userId%>,<%=studyDataMgr%>);"><img title="<%=LC.L_Rem_FromTeam%>" src="./images/delete.gif" border ="0"><%//=LC.L_Rem_FromTeam%><%--Remove from Team*****--%></A>
										<% }
										
									} else { %> 
										<A href="deletefromteam.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&teamId=<%=teamId%>&right=<%=pageRight%>&userName=<%=selName%>&studyId=<%=studyId%>&userId=<%=userId%>" onClick="return checkDelete(<%=pageRight%>,'E', <%=paramName%>,<%=userId%>,<%=studyDataMgr%>);"><img title="<%=LC.L_Rem_FromTeam%>" src="./images/delete.gif" border ="0"><%//=LC.L_Rem_FromTeam%><%--Remove from Team*****--%></A>
									<% } %>
									</td>
							</tr>
							<%} else {
							//println name users
								if ((counter%2)==0) {
								%>
									<tr class="browserEvenRow">
								<%}	else {
								%>
									<tr class="browserOddRow">
								<%}%><td></td>
								<td  align="center"><table width="100%"><tr><td width="30%" style="border: 0px;">
									<A href="mailto:<%=userEmail%>" onmouseover="return overlib('<%=LC.L_Phone%><%--Phone*****--%>: <%=userAddPhone%><br><%=LC.L_Email%><%--Email*****--%>: <%=userEmail%>',CAPTION,'<%=LC.L_User_Dets%><%--User Details*****--%>');" onmouseout="return nd();"><font size="2" weight="bold"><img title="<%=LC.L_Email%>" src="./images/Email.gif" border ="0"><!--@--></font></A>
									<a href="#" onClick="openeditstudyteam(<%=teamId%>,<%=studyId%>,'M',<%=pageRight%>,'E')"><img title="<%=LC.L_More_Info%>" src="./images/moreInfo.png" border ="0"><%//=LC.L_C%><%--C*****--%></a>
									</td><td style="border: 0px;">&nbsp;&nbsp;<%=teamUserFirstName%> <%=teamUserLastName%></td></tr></table>
								</td>
								<td> <%=teamRole%> </td>
								<td  align="center">
									<A href="studyrights.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&teamId=<%=teamId%>&right=<%=pageRight%>&userName=<%=selName%>&studyId=<%=studyId%>&userId=<%=userId%>" onClick="return f_check_user(<%=pageRight%>,'E','<%=userType%>');"><img title="<%=LC.L_Assign_Rights%>" src="./images/AccessRights.gif" border ="0"><%//=LC.L_Assign_Rights%><%--Access Rights*****--%></A>
								</td>
									<!--Added by Manimaran for the July-August Enhancement S4.	-->
								<td><table width="100%"><tr><td width="50%" style="border: 0px;">
									<!--Modified by Manimaran to fix the Bug2703 -->
									<!--Modified by Manimaran to fix the Bug2716 -->
										<a href="#" onClick="openeditstatus(<%=teamId%>,<%=teamStatusId%>,<%=studyId%>,'<%=teamStat%>','N','<%=userName%>',<%=pageRight%>,'E','<%=userType%>')"><img title="<%=LC.L_Edit%>" src="./images/edit.gif" border ="0"><%//=LC.L_C%><%--C*****--%></a>
								      	<A href="#" onClick="openeditstatus(<%=teamId%>,<%=teamStatusId%>,<%=studyId%>,'<%=teamStat%>','M','<%=userName%>',<%=pageRight%>,'E','<%=userType%>')">  <%=teamStat%></A></td>
									  <td style="border: 0px;">
										<A href="showStudyTeamHistory.jsp?modulePk=<%=teamId%>&pageRight=<%=pageRight%>&srcmenu=<%=src%>&selectedTab=<%=tab%>&page=<%=page%>&from=studyTeamHistory&fromjsp=showStudyTeamHistory.jsp&userName=<%=userName%>&studyId=<%=studyId%>" ><img border="0" title="<%=LC.L_History%>" alt="<%=LC.L_History%>" src="./images/History.gif" ><%//=LC.L_H %><%--H*****--%></A>
									  </td>
								      </tr>
									</table>
								</td>
								<td  align="center">
									<% if( ("LIND".equals(CFG.EIRB_MODE) )) {
										 String[] result = CFG.Study_Role_Filter_Subtypes.split(",");
										  String[] codeSubTypes= new String[result.length];		  
									      for (int x=0; x<result.length; x++) {
									         codeSubTypes[x]=result[x];		         
									      }	
									    boolean found = false;
										CodeDao cdRole = new CodeDao(); 
										for( int i=0 ; i < codeSubTypes.length ; i++) {
											if(teamRole.equals(cdRole.getCodeDesc(cdRole.getCodeId("role", codeSubTypes[i]))) ) { 											
												found = true;
											}
										}
									 
										if( !found ) { %>
											<A href="deletefromteam.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&teamId=<%=teamId%>&right=<%=pageRight%>&userName=<%=selName%>&studyId=<%=studyId%>&userId=<%=userId%>" onClick="return checkDelete(<%=pageRight%>,'E', <%=paramName%>,<%=userId%>,<%=studyDataMgr%>);"><img title="<%=LC.L_Rem_FromTeam%>" src="./images/delete.gif" border ="0"><%//=LC.L_Rem_FromTeam%><%--Remove from Team*****--%></A>
										<% }
										
									} else { %>
										<A href="deletefromteam.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&teamId=<%=teamId%>&right=<%=pageRight%>&userName=<%=selName%>&studyId=<%=studyId%>&userId=<%=userId%>" onClick="return checkDelete(<%=pageRight%>,'E', <%=paramName%>,<%=userId%>,<%=studyDataMgr%>);"><img title="<%=LC.L_Rem_FromTeam%>" src="./images/delete.gif" border ="0"><%//=LC.L_Rem_FromTeam%><%--Remove from Team*****--%></A>
									<% } %>
								</td>
							</tr>
							<%	}
							}//siteId1.equals(siteId2)
							else {
							if(counter < len-1){
								continue;
							} else if(counter == len-1 && records == 0 ){
								last++;//print name stype
							%>
						<tr>
						<tr>
							<td>
								<P class = "defComments"><A href="#" onClick="openNewOrgWin(document.formlib,'<%=studyId%>','<%=studySiteId%>','M','<%=siteId1%>','<%=pageRight%>')"><%=siteName%></A></P>
							</td>
							<td  align="center">
								<b><%=siteType%></b><%if(istudySiteApndxCnt > 0){%><img src="../images/jpg/icon_clip.gif" ><%}%>
							</td>
							<td><B><%=LC.L_Local_SampleSize%><%--Local Sample Size*****--%>: <%=localSize%></B></td>
							<td>
								<%if(siteAcc.equals("1")){%>
									<A href="studystatusbrowser.jsp?srcmenu=<%=src%>&selectedTab=3&studyId=<%=studyId%>&selsite=<%=siteId1%>"><%=LC.L_Track_StdStatus%><%--Track Study Status*****--%></A>
								<%}%>
							</td>
							<td>
							</td>
							<td  align="center"><!--Modified by Manimaran to fix the Bug2730-->
								&nbsp;<A href="deleteStudySite.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&studySiteId=<%=studySiteId%>&right=<%=pageRight%>&userName=<%=selName%>&selsite=<%=siteId1%>&studyId=<%=studyId%>" onClick="return checkDeleteSite(<%=pageRight%>,'E', '<%=tmpSiteName%>',<%=dmSiteId%>,<%=siteId1%>,'<%=dmUserName%>',<%=enrPatCount%>,'<%=subType%>',<%=activeCount%>,'<%=active%>',<%=currStat%>, <%=totalActiveCount%>);"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" />
								</A>
							</td>
						</tr>
						<%}
								}//else !siteId1.equals(siteId2)
								}//for counter
							}// in user
						if(records == 0 && userCount!=0 && last==0)
							{%>
						<tr>
							<tr>
								<td>
									<P class = "defComments"><A href="#" onClick="openNewOrgWin(document.formlib,'<%=studyId%>','<%=studySiteId%>','M','<%=siteId1%>','<%=pageRight%>')"><%=siteName%></A>
									</P>
								</td>
								<td  align="center">
									<b><%=siteType%></b><%if(istudySiteApndxCnt > 0){%><img src="../images/jpg/icon_clip.gif" ><%}%>
								</td>
								<td><B><%=LC.L_Local_SampleSize%><%--Local Sample Size*****--%>: <%=localSize%></B></td>
								<td>
									<%if(siteAcc.equals("1")){%><A href="studystatusbrowser.jsp?srcmenu=<%=src%>&selectedTab=3&studyId=<%=studyId%>&selsite=<%=siteId1%>"><%=LC.L_Track_StdStatus%><%--Track <%=LC.Std_Study%> Status*****--%></A>
									<%}%>
								</td>
								 <td  align="center"><!--Modified by Manimaran to fix the Bug2730 -->
									<A href="deleteStudySite.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&studySiteId=<%=studySiteId%>&right=<%=pageRight%>&userName=<%=selName%>&selsite=<%=siteId1%>&studyId=<%=studyId%>" onClick="return checkDeleteSite(<%=pageRight%>,'E', '<%=tmpSiteName%>',<%=dmSiteId%>,<%=siteId1%>,'<%=dmUserName%>',<%=enrPatCount%>,'<%=subType%>',<%=activeCount%>,'<%=active%>',<%=currStat%>, <%=totalActiveCount%>);"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0"/>
									</A>
								</td>
							</tr>
							<%}
							}//for cnt1
							/////////////////////////
							////////////////////
				//get right for external user management
							int extRight = 0;
							extRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYEUSR"));

						%>
					</table>
					<br>
		  </Form>
					<% 	 } //end of if body for page right
						else {
					%>
					<jsp:include page="accessdenied.jsp" flush="true"/>
					<%
						} //end of else body for page right
			} //end of else body for check on studyId
	}//end of if body for session

	else{
	%>
	  <jsp:include page="timeout.html" flush="true"/>
	<%
	}
	%>
	<div>
		<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</div>
	<div class ="mainMenu" id="emenu">
		<jsp:include page="getmenu.jsp" flush="true"/>
	</div>
</body>
</html>
