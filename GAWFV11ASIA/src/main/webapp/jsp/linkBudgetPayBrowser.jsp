<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="jqueryUtils.jsp"></jsp:include>
<jsp:include page="skinChoser.jsp" flush="true"/>
<html>
<title><%=LC.L_Reconcile_PaymentDets%><%--Reconcile Payment Details*****--%></title>
<SCRIPT>

	
</SCRIPT>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
	<%-- Nicholas : Start --%>
	<%--<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>--%>
	<%-- Nicholas : End --%>
<SCRIPT LANGUAGE="JavaScript" src="calendar.js"></SCRIPT>
 
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="PayDetB" scope="page" class="com.velos.eres.web.milepayment.PaymentDetailJB" />

<%@ page language = "java" import = "com.velos.eres.business.common.PaymentDetailsDao,com.velos.eres.business.common.TeamDao,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*,com.velos.esch.business.common.*"%>
<body>

<% 
String studyId = request.getParameter("studyId");
int pageRight = EJBUtil.stringToNum(request.getParameter("pR"));
String paymentPk = request.getParameter("paymentPk");

%>
 
<jsp:include page="skinChoser.jsp" flush="true"/>
  <jsp:include page="paymenttabs.jsp" flush="true">
	<jsp:param name="studyId" value="<%=studyId%>"/>
	<jsp:param name="selectedTab" value="3"/>
	<jsp:param name="paymentPk" value="<%=paymentPk%>"/>
	<jsp:param name="pR" value="<%=pageRight%>"/>
  </jsp:include>
  
	<%-- Nicholas : Start --%>
	<jsp:include page="skinChoser.jsp" flush="true"/>
	<%-- Nicholas : End --%>
 

<%


		PaymentDetailsDao pdao = new PaymentDetailsDao ();

		pdao = PayDetB.getLinkedPaymentBrowser(EJBUtil.stringToNum(paymentPk),"B");
		
		ArrayList arId = new ArrayList ();
		ArrayList  arDesc = new ArrayList ();

		ArrayList arAmount = new ArrayList ();
		ArrayList  arLinkId = new ArrayList ();
			
		int count  = 0;
		String id = "";
		String linkId = "";
		String amount = "";
		String desc= "";
		
		if (pdao != null)
		{
			arId = pdao.getId();
			arDesc = pdao.getPaymentLinkDescription();

			arAmount = pdao.getAmount();
			arLinkId = pdao.getLinkToId();
			
			if (arId == null) 
			{
				arId = new ArrayList();
			}
			System.out.println(arDesc.size());
			System.out.println(arAmount.size());
			System.out.println(arLinkId.size());
		}
	 	%>
		<P class = "defComments"><b><A onClick="return f_check_perm(<%=pageRight%>,'N') " HREF="linkBudPayment.jsp?studyId=<%=studyId%>&paymentPk=<%=paymentPk%>&pR=<%=pageRight%>"><%=MC.M_ReconcilePment_Bgt%><%--To Reconcile this Payment with a Budget, Click Here to select a Budget*****--%></A></b> </P>

		<P class = "defComments"><%=MC.M_ReconciledRec_Listed%><%--Previously reconciled records are listed below*****--%>:</P>
			
	 <table width="100%" cellspacing="0" cellpadding="0" border="0" class="basetbl outline MidAlign">
      <tr> 
        <th><%=LC.L_Bgt_Number%><%--Budget Number*****--%></th>
        <th><%=LC.L_Amount%><%--Amount*****--%></th>
      </tr>		
 	  	
<%
	count = arId.size();
	
	
	 for(int counter = 0;counter<count ;counter++)
	{	
		 linkId =  (String) arLinkId.get(counter); 
		 desc =  (String) arDesc.get(counter); 
		 amount =  (String) arAmount.get(counter); 
	    
		if (StringUtil.isEmpty(amount))
		{
			amount = "0.0";
		}
		%>
		
	<%	 
		if(counter%2==0){ 
	%>	
		<TR class="browserEvenRow">	
	<%
	}else{
	%>
		<TR class="browserOddRow">
	<% 
	} 
	%>
	
	<td class="tdDefault" align="center"><A  href="budPaymentDetails.jsp?studyId=<%=studyId%>&paymentPk=<%=paymentPk%>&pR=<%=pageRight%>&budgetId=<%=linkId%>" ><%=desc%></A>
	 
	</td>
	<td class="tdDefault" align="center"><span class="numberfield" data-unitsymbol="" data-formatas="currency"><%=amount%></span></td>
	<!-- Mukul: BUG 4020 NumberUtil.roundOffNo() function used to roundoff number upto two decimal places -->
	</tr>
	
	
	<%
		
	}
	 
	 %>

</table>
 <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</body>
</html>