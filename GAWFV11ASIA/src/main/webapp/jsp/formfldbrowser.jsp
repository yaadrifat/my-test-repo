<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:useBean id="formlibB" scope="request" class="com.velos.eres.web.formLib.FormLibJB"/>

</HEAD>

<body>

<title><%=MC.M_Add_FldToFrm%><%--Add Fields to Form*****--%></title>

<SCRIPT Language="javascript">

var screenWidth = screen.width;
var screenHeight = screen.height;

function confirmBoxSec(secName,pageRight,codeStatus, secOffline) 
{
 	if (secOffline==1) 
	{
 		var paramArray = [secName];
	   alert(getLocalizedMessageString("M_CntDel_FormOffEdt",paramArray));/*alert("Cannot delete "+ secName + ". Form is in 'Offline for Editing' status.");*****/
	   return false;
	}
	
	if (f_check_perm(pageRight,'E') == true)
		 {	
				if ((codeStatus=="WIP") || (codeStatus=="Offline")) 
				{
					flname  = decodeString(secName);
					var paramArray = [flname];
					msg=getLocalizedMessageString("L_DelSec",paramArray);/*msg="Delete Section " + flname  ;*****/
			
					if (confirm(msg)) 
					{
						return true;
					}
					else
					{
						return false;
					}
				} // code status 
		
				else
					{
					alert("<%=LC.L_Edit_PermDenied%>");/*alert("Edit permission denied");*****/
					return false;
					}
		}  // perm 
		else
	 	{	
			//alert("Edit permission denied");
			return false;
		} 
	
	

}
function confirmBox(fieldName,pageRight,codeStatus,fldOffline,fieldDataType) 
{
	if ((fldOffline==1) && !(fieldDataType=="F" )) 
	{
	   var paramArray = [fieldName];
	   alert(getLocalizedMessageString("M_CntDel_FormOffEdt",paramArray));/*alert("Cannot delete "+ fieldName + ". Form is in 'Offline for Editing' status.");*****/
	   return false;
	}
		
	if (f_check_perm(pageRight,'E') == true) {
	  	if ((codeStatus=="WIP") || (codeStatus=="Offline") || (((fieldDataType=="F")) && (codeStatus!="Lockdown")) )
		{		
			flname  = decodeString(fieldName);
			flname=htmlDecode(flname);
			var paramArray = [flname];
			msg=getLocalizedMessageString("M_DelFrm_Form",paramArray);/*msg="Delete " + flname + " from the form?";*****/
		
			if (confirm(msg)) 
			{
				return true;
			}
			else
			{
				
				return false;
			}
		} 
	
		else
			{
			alert("<%=LC.L_Edit_PermDenied%>");/*alert("Edit permission denied");*****/
			return false;
			}
	}
	else {
		//alert("Edit permission denied");
		return false;
	}


}

function openAddSectionWin(formobj,pageRight,codeStatus) {
	calledFrom = formobj.calledFrom.value;
	lnkFrom = formobj.lnkFrom.value;	
	if ((f_check_perm(pageRight,'E') == true))
	{
		if ((codeStatus=="WIP") || (codeStatus=="Offline")){
		formId=formobj.formId.value;
		param1="newSection.jsp?&mode=N&formId="+formId+"&codeStatus="+codeStatus+"&calledFrom="+calledFrom+"&lnkFrom="+lnkFrom  ;
		windowName=window.open(param1,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=570,height=400,top=200 ,left=200");
		windowName.focus();
		}
		else{
			alert("<%=LC.L_Edit_PermDenied%>");/*alert("Edit permission denied");*****/
			}
	}
}

function openEditSectionWin(formobj,pageRight,codeStatus) {
	calledFrom = formobj.calledFrom.value;
	lnkFrom = formobj.lnkFrom.value;	
	if ((f_check_perm(pageRight,'E') == true))
	{
		if ((codeStatus=="WIP") || (codeStatus=="Offline")){
		formId=formobj.formId.value;
		param1="newSection.jsp?&mode=M&formId="+formId+"&codeStatus="+codeStatus+"&calledFrom="+calledFrom+"&lnkFrom="+lnkFrom  ;
		windowName=window.open(param1,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=570,height=400,top=200 ,left=200");
		windowName.focus();
		}
		else{
			alert("<%=LC.L_Edit_PermDenied%>");/*alert("Edit permission denied");*****/
			}
	}
}




function deletemultiplelnk(formobj,pageRight,codeStatus) {
	calledFrom = formobj.calledFrom.value;
	lnkFrom = formobj.lnkFrom.value;	
	if ((f_check_perm(pageRight,'E') == true))
	{
		if ((codeStatus=="WIP") || (codeStatus=="Offline")){
		formId=formobj.formId.value;
		param1="formflddelmultiple.jsp?&mode=M&formId="+formId+"&codeStatus="+codeStatus+"&calledFrom="+calledFrom+"&lnkFrom="+lnkFrom  ;
		windowName=window.open(param1,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=750,height=400,top=200 ,left=200");
		windowName.focus();
		}
		else{
			alert("<%=LC.L_Edit_PermDenied%>");/*alert("Edit permission denied");*****/
			}
	}
}






function openEditBoxWin(formobj,pageRight,codeStatus) {
	calledFrom = formobj.calledFrom.value;
	lnkFrom = formobj.lnkFrom.value;
	studyId = formobj.studyId.value;
						
	if ((f_check_perm(pageRight,'E') == true))
	{
		if ((codeStatus=="WIP") || (codeStatus=="Offline")){
		formId=formobj.formId.value;
		
		param1="editBoxSection.jsp?&mode=N&formId="+formId+"&codeStatus="+codeStatus+"&calledFrom="+calledFrom+"&lnkFrom="+lnkFrom+"&studyId="+studyId;
		windowName=window.open(param1,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=810,height=600,top=90 ,left=80");
		windowName.focus();
		}
		else{
			alert("<%=LC.L_Edit_PermDenied%>");/*alert("Edit permission denied");*****/
		 }
   }
}

function openFieldDisplayWin(formId,formFldId,formobj,pageRight,codeStatus) {
	 
		calledFrom = formobj.calledFrom.value;
		lnkFrom = formobj.lnkFrom.value;
		studyId = formobj.studyId.value;				
		param1="editBoxSection.jsp?&mode=M&formId="+formId+"&formFldId="+formFldId+"&codeStatus="+codeStatus+"&calledFrom="+calledFrom+"&lnkFrom="+lnkFrom+"&studyId="+studyId ;
		windowName=window.open(param1,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=810,height=600,top=90 ,left=80");
		windowName.focus();
}

function openPrinterFormatWin(formobj)
{
	//alert("inside openPrinterFormatWin");
	param1=formobj.printerLink.value ;
    windowName=window.open(param1,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes ,width=675,height=600,top=60,left=200");
	windowName.focus();

}

function openFormMetadata(formId)
{
	windowName=window.open("repRetrieve.jsp?repId=125&repName=Form Metadata&formId="+formId,"Information","toolbar=no,scrollbars=yes,resizable=no,menubar=no,status=yes");
	windowName.focus();
}


function openExcelWin(formobj)
{

	param1=formobj.excelLink.value ;
	//SV, 8/25/04, open each excel report in a new window. (_blank)	
	if (navigator.appName == "Netscape") 
		wname = "_new";
	else {
			wname = "_blank";
	}
    windowName=window.open(param1,wname,"toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=675,height=600,top=60,left=200");
	windowName.focus();
	
}

function openPreviewWin(formobj)
{
	formId=formobj.formId.value;
	param1="formpreview.jsp?formId="+formId;
	
	windowName=window.open(param1,"Information","toolbar=no,scrollbars=yes,resizable=no,menubar=no,status=yes,width=825,height=600 ,top=90 ,left=150");
	windowName.focus();
}

function openMultipleSectionWin(formobj,formId,pageRight,codeStatus)
{
	calledFrom = formobj.calledFrom.value;
	lnkFrom = formobj.lnkFrom.value;	
	studyId = formobj.studyId.value;	
	if (f_check_perm(pageRight,'E') == true)
	{
		if ((codeStatus=="WIP") || (codeStatus=="Offline")){
		param1="multipleChoiceSection.jsp?&mode=N&formId="+formId+"&codeStatus="+codeStatus+"&calledFrom="+calledFrom+"&lnkFrom="+lnkFrom+"&studyId="+studyId ;
		windowName=window.open(param1,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=820,height=680 ,top=25 ,left=150");
		windowName.focus();
		}
		else{
			alert("<%=LC.L_Edit_PermDenied%>");/*alert("Edit permission denied");*****/
		}
  }
}

function openMultipleSectionWinMode(formId,formFldId,formobj,pageRight,codeStatus)
{
 		calledFrom = formobj.calledFrom.value;
		lnkFrom = formobj.lnkFrom.value;	
		studyId = formobj.studyId.value;			
		param1="multipleChoiceSection.jsp?&mode=M&formId="+formId+"&formFldId="+formFldId+"&codeStatus="+codeStatus+"&calledFrom="+calledFrom+"&lnkFrom="+lnkFrom+"&studyId="+studyId;
		windowName=window.open(param1,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=820,height=680 ,top=25 ,left=150");
		windowName.focus();
}


function openSelFromLibWin(formobj,pageRight,codeStatus)
{

	if (f_check_perm(pageRight,'E') == true)
	{
		if ((codeStatus=="WIP") || (codeStatus=="Offline")){
		formId=formobj.formId.value;
		param1="addFieldToForm.jsp?formId="+formId+"&codeStatus="+codeStatus+"&mode=initial";	
		windowName=window.open(param1,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=850,height=600,top=40 ,left=100 ");
		windowName.focus();
		}
		else{
			alert("<%=LC.L_Edit_PermDenied%>");/*alert("Edit permission denied");*****/
		}
   }
}

function openCommentsWin(formobj,formId,pageRight,codeStatus)
{
	calledFrom = formobj.calledFrom.value;
	lnkFrom = formobj.lnkFrom.value;
	studyId = formobj.studyId.value;		
	if (f_check_perm(pageRight,'E') == true)
	{
		if ((codeStatus=="WIP") || (codeStatus=="Offline")){
		param1="commentType.jsp?&mode=N&formId="+formId+"&codeStatus="+codeStatus+"&calledFrom="+calledFrom+"&lnkFrom="+lnkFrom+"&studyId="+studyId  ;
		windowName=window.open(param1,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=670,height=450 ,top=200 ,left=250");
		windowName.focus();
		}
		else{
			alert("<%=LC.L_Edit_PermDenied%>");/*alert("Edit permission denied");*****/
		}
	}

	
}

function openTypeNew(formobj,formId,pageRight,codeStatus,windowPage)
{
	calledFrom = formobj.calledFrom.value;
	lnkFrom = formobj.lnkFrom.value;
	studyId = formobj.studyId.value;	
	if (f_check_perm(pageRight,'E') == true)
	{
		if ((codeStatus=="WIP") || (codeStatus=="Offline")){
		param1= windowPage+ "?&mode=N&formId="+formId+"&codeStatus="+codeStatus+"&calledFrom="+calledFrom+"&lnkFrom="+lnkFrom+"&studyId="+studyId  ;
		windowName=window.open(param1,"typeNew","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=650,height=450 ,top=200 ,left=250");
		windowName.focus();
		}
		else{
			alert("<%=LC.L_Edit_PermDenied%>");/*alert("Edit permission denied");*****/
		}
	}
}

function openTypeEdit(formId,formFldId,formobj,pageRight,codeStatus,windowPage)
{
 		calledFrom = formobj.calledFrom.value;
		lnkFrom = formobj.lnkFrom.value;
		studyId = formobj.studyId.value;				
		
		param1=windowPage + "?&mode=M&formId="+formId+"&formFldId="+formFldId+"&codeStatus="+codeStatus+"&calledFrom="+calledFrom+"&lnkFrom="+lnkFrom+"&studyId="+studyId ;
		windowName=window.open(param1,"TypeEdit","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=650,height=450,top=200 ,left=250");
		windowName.focus();	
}
	
function openEditCommentsWin(formId,formFldId,formobj,pageRight,codeStatus)
{
 		calledFrom = formobj.calledFrom.value;
		lnkFrom = formobj.lnkFrom.value;
		studyId = formobj.studyId.value;				
		param1="commentType.jsp?&mode=M&formId="+formId+"&formFldId="+formFldId+"&codeStatus="+codeStatus+"&calledFrom="+calledFrom+"&lnkFrom="+lnkFrom+"&studyId="+studyId ;
		windowName=window.open(param1,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=670,height=450,top=200 ,left=250");
		windowName.focus();	
}

function openHorizontalLineWin(formobj,formId,pageRight,codeStatus)
{
	calledFrom = formobj.calledFrom.value;
	lnkFrom = formobj.lnkFrom.value;	
	studyId = formobj.studyId.value;	
	if ((f_check_perm(pageRight,'E') == true))
	{
		if ((codeStatus=="WIP") || (codeStatus=="Offline")){
		param1="horizontalRuleType.jsp?&mode=N&formId="+formId+"&codeStatus="+codeStatus+"&calledFrom="+calledFrom+"&lnkFrom="+lnkFrom+"&studyId="+studyId ;
		windowName=window.open(param1,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=670,height=300 ,top=200 ,left=350");
		windowName.focus();
		}
		else{
			alert("<%=LC.L_Edit_PermDenied%>");/*alert("Edit permission denied");*****/
			}
	}

	
}


function openEditHorizontalLineWin(formId,formFldId,formobj,pageRight,codeStatus)
{
 		calledFrom = formobj.calledFrom.value;
		lnkFrom = formobj.lnkFrom.value;
		studyId = formobj.studyId.value;					
		param1="horizontalRuleType.jsp?&mode=M&formId="+formId+"&formFldId="+formFldId+"&codeStatus="+codeStatus+"&calledFrom="+calledFrom+"&lnkFrom="+lnkFrom+"&studyId="+studyId  ;
		windowName=window.open(param1,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=670,height=450 ,top=400 ,left=350");
		windowName.focus();
}

function changeSeq(formobj,formFldId,formFldSeq,otherFormFldId,otherSeq,calledFrom,codeStatus,lnkFrom,studyId)
{
	 formobj.action = "changefieldseq.jsp?lnkFrom="+lnkFrom+"&codeStatus="+codeStatus+"&calledFrom="+calledFrom+"&formFldId="+formFldId+"&formFldSeq="+formFldSeq+"&otherFormFldId="+otherFormFldId+"&otherSeq="+otherSeq+"&studyId="+studyId;
 	 formobj.submit;
}

function openEditLookupWin(formId,formFldId,formobj,pageRight,codeStatus)
{	
	
 		calledFrom = formobj.calledFrom.value;
		lnkFrom = formobj.lnkFrom.value;
		studyId = formobj.studyId.value;					
		param1="lookupType.jsp?&mode=M&formId="+formId+"&formFldId="+formFldId+"&codeStatus="+codeStatus+"&calledFrom="+calledFrom+"&lnkFrom="+lnkFrom+"&studyId="+studyId ;
		windowName=window.open(param1,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=730,height=550,top=90 ,left=250");
		windowName.focus();
	

}

function openNewLookupWin(formobj,pageRight,codeStatus) {
	calledFrom = formobj.calledFrom.value;
	lnkFrom = formobj.lnkFrom.value;
	studyId = formobj.studyId.value;	
	if ((f_check_perm(pageRight,'E') == true))
	{
		
		formId=formobj.formId.value;
		param1="lookupType.jsp?&mode=N&formId="+formId+"&codeStatus="+codeStatus+"&calledFrom="+calledFrom+"&lnkFrom="+lnkFrom+"&studyId="+studyId  ;
		windowName=window.open(param1,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=730,height=550, top=90 ,left=250");
		windowName.focus();
		
   }
}





function openWin(formId,pageRight,codeStatus) 
{
 if ((f_check_perm(pageRight,'E') == true))
	{
		if ((codeStatus=="WIP") || (codeStatus=="Offline")){
		windowName=window.open("copyfieldtoform.jsp?formId="+formId+"&codeStatus="+codeStatus,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=250,top=200,left=250");
		windowName.focus();
	}
		else{
			alert("<%=LC.L_Edit_PermDenied%>");/*alert("Edit permission denied");*****/
		 }
	}
}
</SCRIPT>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<% String src;
src= request.getParameter("srcmenu");
String selectedTab;
selectedTab = request.getParameter("selectedTab");
String codeStatus=request.getParameter("codeStatus");
String calledFrom=request.getParameter("calledFrom");
String lnkFrom=request.getParameter("lnkFrom");
String stdId=request.getParameter("studyId");
int studyId= EJBUtil.stringToNum(stdId);
int formId;
formId= EJBUtil.stringToNum(request.getParameter("formId"));
String mode=request.getParameter("mode");

ArrayList secIds1=null;
FormLibDao formLibDao1 = new FormLibDao();
formLibDao1 = formlibB.getSectionFieldInfo(formId);	
secIds1 = formLibDao1.getSecIds();

int FLDcount=secIds1.size();

%>

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   


<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id ="reportIO" scope="session" class="com.velos.eres.service.util.ReportIO"/>

<%@ page language = "java" import = "com.velos.eres.business.site.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.web.studyRights.StudyRightsJB"

%>


<%
 HttpSession tSession = request.getSession(true); 

 
  if (sessionmaint.isValidSession(tSession))
	{
	
%>



<%	
 if (formId==0) {		
%>
   <P class = "defComments"><FONT class="defComments"><%=MC.M_SaveFrm_BeforeDets%><%--Please save the Form first before entering details here.*****--%></FONT></P>  
<%
} else 

{

		 int pageRight = 0;	
		 GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
		 //pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MFLDLIB"));
		 //////rights by salil
		//int study_not_associated_right = Integer.parseInt(grpRights.getFtrRightsByValue("MACCFRMSNOSTUDY"));
		//int study_associated_right =Integer.parseInt(grpRights.getFtrRightsByValue("MACCFRMSWSTUDY"));
		int account_form_rights = 0;
					

		int stdFrmRight=0;
		String userIdFromSession = (String) tSession.getValue("userId");
			
		if( calledFrom.equals("A")){
	 	    if (lnkFrom.equals("S")) 
	 	       {  ArrayList tId ;
					tId = new ArrayList();
					
					if(studyId >0)
					{
						TeamDao teamDao = new TeamDao();
						teamDao.getTeamRights(studyId,EJBUtil.stringToNum(userIdFromSession));
						tId = teamDao.getTeamIds();
						if (tId.size() <=0)
						{
							stdFrmRight  = 0;
						} else {
							StudyRightsJB stdRights = new StudyRightsJB();
							stdRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));
							
							ArrayList teamRights ;
							teamRights  = new ArrayList();
							teamRights = teamDao.getTeamRights();
								 
							stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
							stdRights.loadStudyRights();
							 
							 		
							if ((stdRights.getFtrRights().size()) == 0){					
								 stdFrmRight= 0;		
							} else {								
								stdFrmRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMMANAG"));
							}
						}
					}
		 		   pageRight=stdFrmRight;

				   
		    	}
	 		else
	        	{
				account_form_rights = Integer.parseInt(grpRights.getFtrRightsByValue("MACCFRMS"));
			 	pageRight= account_form_rights;

		
			 }
	  	}
	 	if( calledFrom.equals("L"))
	 	{
	  	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MFRMLIB"));
	 	}
		if( calledFrom.equals("St"))
	 	{
		StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");	
	  	pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMMANAG"));
	 	}
		 /////end by salil 

		 if(pageRight >= 4)
		{
		 ArrayList secNames = null;
		 ArrayList secIds = null;
		 ArrayList fieldNames = null;
		 ArrayList fieldIds = null;
		 ArrayList fieldUniIds = null;
		 ArrayList fieldTypes = null;
		 ArrayList repeatFields = null;
		 ArrayList secFormats = null;
		 ArrayList formLibIds = null;
		 ArrayList formFldIds = null;
		 ArrayList formFldSeqs = null;
    	 	 ArrayList formFldNums = null;
    	 	 ArrayList fieldDataTypes = null;
		 ArrayList secOfflines = null;
 		 ArrayList fldOfflines = null;

		 String secName = "";
		 String secId = "";
		 String fieldName = "";
		 String fieldId = "";
		 String fieldUniId = "";
		 String fieldType = "";
		 String repeatField = "";
		 String secFormat = "";
		 String formFldId = ""; 
		 int fldOffline = 0;
		 int secOffline = 0;
		 
		 int formFldSeq = 0;
		 int formFldNum = 0;
		 int upperSeq = 0;
		 int lowerSeq = 0;		 
		
		 String tempFldName = ""; 
		 String fieldDataType = "";
 
		 FormLibDao formLibDao = new FormLibDao();
		 formLibDao = formlibB.getSectionFieldInfo(formId);	 	
		 secNames = formLibDao.getSecNames(); 	
		 secIds = formLibDao.getSecIds();	
		 fieldNames = formLibDao.getFieldNames();
		
		 System.out.println("**************FieldNames" + fieldNames);
		 fieldIds =	formLibDao.getFieldIds();	
		 fieldUniIds = formLibDao.getFieldUniIds();			 
		 fieldTypes = formLibDao.getFieldTypes();		 
		 repeatFields = formLibDao.getRepeatFields();		 
		 secFormats = formLibDao.getSecFormats();		
		 formFldIds = formLibDao.getFormFldIds();		
		 formFldSeqs = formLibDao.getFormFldSeqs();		
		 formFldNums = formLibDao.getFormFldNums();
		 secOfflines = formLibDao.getArrFormSecOfflines();		
		 fldOfflines = formLibDao.getArrFormFldOfflines(); 		
 		 
 		 fieldDataTypes = formLibDao.getArrFormFldDataTypes();
 		 String tempRepeatField1="";
 		 int count1=0;	
		int len= secIds.size(); 
		for(count1=0;count1<secIds.size();count1++)
		{
			tempRepeatField1=(repeatFields.get(count1)).toString();
			FLDcount+=Integer.parseInt(tempRepeatField1);
		}
		
		//int len1=fieldIds.size();
		// StringBuffer to view the Section's information in excel, explorer and netscape browsers
		StringBuffer sb = new StringBuffer();
		sb.append("<table width='100%' cellspacing='1' cellpadding='0'>");
		sb.append("<tr><th width='25%'>"+LC.L_Sequence+"</th>");/*sb.append("<tr><th width='25%'>Sequence</th>");*****/
		sb.append("<th width='30%'>"+LC.L_Fld_Name+"</th>");/*sb.append("<th width='30%'>Field Name</th>");*****/
		sb.append("<th width='25%'>"+LC.L_Fld_Id+"</th>");/*sb.append("<th width='25%'>Field ID</th>");*****/
		sb.append("<th width='20%'>"+LC.L_Fld_Type+"</th></tr>");/*sb.append("<th width='20%'>Field Type</th></tr>");*****/
	
		
 %>
		
<DIV class="BrowserTopn" id="div1">
<jsp:include page="formtabs.jsp" flush="true">
<jsp:param name="formId" value="<%=formId%>"/>
<jsp:param name="mode" value="<%=mode%>"/>
<jsp:param name="codeStatus" value="<%=codeStatus%>"/>
<jsp:param name="calledFrom" value="<%=calledFrom%>"/>
<jsp:param name="lnkFrom" value="<%=lnkFrom%>"/>
<jsp:param name="studyId" value="<%=studyId%>"/>
<jsp:param name="formCount" value="<%=FLDcount%>"/>
</jsp:include>

</div>
<SCRIPT LANGUAGE="JavaScript">

	if(screenWidth>1280 || screenHeight>1024)
		document.write('<DIV class="BrowserBotN BrowserBotN_FLaddfield_1" id="div2" style="height:65%;margin-top:45px">')
	else
		document.write('<DIV class="BrowserBotN BrowserBotN_FLaddfield_1" style="margin-top: 52px;height:76%" id="div2">')
</SCRIPT>
<Form name="formlib" action=""  method="post" > 
	<input type="hidden" name="selectedTab" value=<%=selectedTab%>>
<div class="tmpHeight"></div>
	<table cellpadding="2" width="99%" border="1" cellspacing="2" class="basetbl outline midAlign">
<tr><td colspan="13" align="center">
<%

if(calledFrom.equals("L") && !codeStatus.equals("WIP") ){%>
 <P class = "defComments"><FONT class="Mandatory"><%=MC.M_ChgNotSvd_ForActvFroznForm%><%--Only Lookup fields can be associated and modified in a Frozen form*****--%> &nbsp;&nbsp&nbsp;</FONT></font>
 </P>
	
	
	
<%}else if((calledFrom.equals("L") && !codeStatus.equals("WIP")) || ((calledFrom.equals("St")||calledFrom.equals("A")) && codeStatus.equals("Deactivated")) ){%>
 <P class = "defComments"><FONT class="Mandatory"><%=MC.M_AnyChgFrm_DeacModeNotSvd%><%--Any changes made to a form in 'Deactivated' mode will not be saved*****--%>  </FONT></P><%}
else if((calledFrom.equals("L") && !codeStatus.equals("WIP")) || ((calledFrom.equals("St")||calledFrom.equals("A")) && codeStatus.equals("Lockdown")) ){%>
 <P class = "defComments"><FONT class="Mandatory"><%=MC.M_AnyChgFrm_LkdwnModeNotSvd%><%--Any changes made to a form in 'Lockdown' mode will not be saved*****--%> </FONT></P>
 <%}
else{
	if ((calledFrom.equals("St")||calledFrom.equals("A")) && codeStatus.equals("Active")){
	
		
	%>	
	<br>	
		</td></tr>
		
	<%}else{
		if (codeStatus.equals("Offline")) {%>
 	 <P class="defComments"><FONT class="Mandatory"> <%=MC.M_ChgOfflineEdt_AppFrmAns%><%--Some changes made in an Offline for Editing form may be applicable only to new forms and not to previously answered forms.*****--%></FONT></P>
	 <%}%>
</td></tr>
	
	<tr height="50">
	<td align="center" valign="top" width="8%">
	<BR><A href="#" onClick="openSelFromLibWin(document.formlib,'<%=pageRight%>','<%=codeStatus%>')"><img src="./images/library.gif" title="<%=LC.L_Fld_Lib%><%--Field Library*****--%>" alt="<%=MC.M_Selc_FromFldLib %><%-- Select from Field Library*****--%>" border = none></A>
		
	</td>
	<td align="center" valign="top" width="8%">
	<BR><A href="#" onClick="openEditBoxWin(document.formlib,'<%=pageRight%>','<%=codeStatus%>')" ><img src="./images/box.gif" title="<%=LC.L_Edit_Box%><%--Edit Box*****--%>" alt="<%=MC.M_AddEdtBox_Fld %><%-- Add an Edit Box Field*****--%>" border = none></A>
	</td>
	<td align="center" valign="top" width="8%">
	<BR><A href="#" onClick="openMultipleSectionWin(document.formlib, '<%=formId%>','<%=pageRight%>','<%=codeStatus%>')"><img src="./images/choice.gif" title="<%=LC.L_Multi_Choice%><%--Multi Choice*****--%>" alt="<%=MC.M_AddMulti_ChoiceFld %><%-- Add a Multiple Choice Field*****--%>" border = none></A>
	</td>
		
	<td align="center" valign="top" width="8%">
	<BR><A href="#" onClick="openWin(<%=formId%>,'<%=pageRight%>','<%=codeStatus%>')"><img src="./images/CopyField.gif" title="<%=LC.L_Copy_Fld%><%--Copy Field*****--%>" alt="<%=MC.M_Copy_ExistingFld %><%-- Copy Field*****--%>" border = none></A>
	</td>	 
			 
	<td align="center" valign="top" width="8%">
	<BR><A href="#" onClick="openCommentsWin(document.formlib,'<%=formId%>','<%=pageRight%>','<%=codeStatus%>')"><img src="./images/comments.gif" title="<%=LC.L_Comments%><%--Comments*****--%>" alt="<%=MC.M_Add_AComment %><%-- Add a Comment*****--%>" border = none></A> 
	</td>
	<td align="center" valign="top" width="8%">
	<BR><A href="#" onClick="openNewLookupWin(document.formlib,'<%=pageRight%>','<%=codeStatus%>')" ><img src="./images/lookup.gif" title="<%=LC.L_Lookup%><%--Lookup*****--%>" alt="<%=MC.M_AddLookup_Fld %><%-- Add a Lookup Field*****--%>" border = none></A>
	</td>
	<td align="center" valign="top" width="8%">
	<BR><A href="#" onClick="openHorizontalLineWin(document.formlib,'<%=formId%>','<%=pageRight%>','<%=codeStatus%>')"><img src="./images/line.gif" title="<%=LC.L_Line_Break%><%--Line Break*****--%>" alt="<%=MC.M_AddA_LineBrk %><%-- Add a Line Break*****--%>" border = none></A> 	
	</td>
	<td align="center" valign="top" width="8%">
	<BR><A href="#" onClick="openAddSectionWin(document.formlib,'<%=pageRight%>','<%=codeStatus%>')"><img src="./images/section.gif" title="<%=LC.L_New_Sec%><%--New Section*****--%>" alt="<%=LC.L_AddA_Sec %><%-- Add a Section*****--%>" border = none></A>
	</td>

	<td align="center" valign="top" width="8%">
	<BR><A href="#" onClick="openEditSectionWin(document.formlib,'<%=pageRight%>','<%=codeStatus%>')"><img src="./images/EditSection.gif" title="<%=LC.L_Edit_Sec%><%--Edit Section*****--%>" alt="<%=LC.L_Edit_Sec%><%-- Edit a Section*****--%>" border = none></A>		
	</td>

	<td align="center" valign="top" width="7%">
	<BR><A href="#" onClick = "openFormMetadata(<%=formId%>)"><img src="./images/DataMap.gif" title="<%=LC.L_FormMetadata%>" alt="<%=LC.L_AddA_Sec %><%-- Add a Section*****--%>" border = none></A>		
	</td>
	
	<td align="center" valign="top" width="9%">
	<BR><A href="#" onClick="deletemultiplelnk(document.formlib,'<%=pageRight%>','<%=codeStatus%>')">
	<img src="./images/DeleteSelected.gif" title="<%=LC.L_Del_Multi%>" alt="<%=LC.L_Del_Multi %><%--Delete Multiple*****--%>" border = none></A>
	</td>
		
	<td align="center" valign="top" width="6%">
	<BR>	<A href="#" onClick="openTypeNew(document.formlib,'<%=formId%>','<%=pageRight%>','<%=codeStatus%>','spaceType.jsp')"><font size = -2><b><img border="0" title="<%=LC.L_Space_Break%>" alt="<%=LC.L_Space_Break%>" src="./images/SpaceBreak.gif" ><%//=LC.L_Space_Break%><%--Space Break*****--%></b></font></A>
		</td>
		
		<% if (!calledFrom.equals("L")) { %>
	<td align="center" valign="top" width="5%">
	<BR>	<A href="#" onClick="openTypeNew(document.formlib,'<%=formId%>','<%=pageRight%>','<%=codeStatus%>','linkFormType.jsp')"><img src="../images/jpg/FormLink.gif" title="<%=LC.L_Form_Link%>" border=0 ></A>
		</td> 
		<% } %>	
		
		</tr>	
<%}
}%>

	</table>
	<table width="100%" cellpadding="0" cellspacing="0" border="0" class="outline midAlign"> 
<!--	<tr>
	<td colspan="9">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<A href="#" onClick="openPreviewWin(document.formlib)">PREVIEW</A>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	 <A href="#" onClick="openWin(<%=formId%>,'<%=pageRight%>','<%=codeStatus%>')">COPY AN EXISTING FIELD</A>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<!--	<A href="#" onClick="openExcelWin(document.formlib)">EXPORT TO EXCEL</A>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 

	<A href="#" onClick = "openFormMetadata(<%=formId%>)">FORM DATAMAP</A>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	
	 <A href="#" onClick="openPrinterFormatWin(document.formlib)">PRINTER FRIENDLY FORMAT</A>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<A href="#" onClick="openEditSectionWin(document.formlib,'<%=pageRight%>','<%=codeStatus%>')">EDIT SECTIONS</A>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
	
 <!-- commented by Chn dev -->
	<!--% if (len1 >1 ){%-->

	<!--<A href="#" onClick="deletemultiplelnk(document.formlib,'<%=pageRight%>','<%=codeStatus%>')">DELETE MULTIPLE</A>-->
	<!--%}else {
		%-->
		
<!--	<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<--%}--%--> 

<!--	</tr> -->


	
</table>


<div class="tmpHeight"></div>
	<table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl midAlign">
					<tr> 
						<%if(pageRight > 4 && ((codeStatus.equals("WIP")) || (codeStatus.equals("Offline")))){%>
				        <th width="15%"><%=LC.L_Sequence%><%--Sequence*****--%></th>
						<%}%>
				        <th width="22%"><%=LC.L_Fld_Name%><%--Field Name*****--%></th>
				        <th width="23%"><%=LC.L_Fld_Id%><%--Field ID*****--%></th>
						<th width="18%"><%=LC.L_Fld_Type%><%--Field Type*****--%></th>
						 <th width="14%"><%=LC.L_Edit%><%--Modify*****--%></th>	
						<th width="13%"><%=LC.L_Delete%><%--Delete*****--%></th>
					</tr>
	
					<tr> 
<%
	int count=0;
	int i=0;
	int fmtcount=1;
	int repcount=1;
	String fldDisplayHref="";
	String deleteHref="";
	String upperFormFldId = "";
	String lowerFormFldId = "";
	String flag = "" ;
	String oldtempSecId="";
	//int tcount=0;
	for(count=0;count<len;count++)
	{
		String tempSecName="";
		String tempSecFormat="";
		String tempRepeatField="";
		String tempSecId = "";
		String secDeleteHref = ""; 						tempSecName=(secNames.get(count)).toString();			
		tempSecFormat=(secFormats.get(count)).toString(); 		
		tempRepeatField=(repeatFields.get(count)).toString();
		tempSecId = secIds.get(count).toString();
		formFldNum = (   (Integer)formFldNums.get(count)  ).intValue();
		fieldUniId = ((fieldUniIds.get(count)) == null)?"-":(fieldUniIds.get(count)).toString();
		secOffline = ((Integer)secOfflines.get(count)).intValue();
		fldOffline = ((Integer)fldOfflines.get(count)).intValue();

		// FOR THE SECTION WITH THE DEFAULT DATE FIELD
		// Commented by Manimaran to fix the Bugzilla issue #2637
		/*
		if ( 	fieldUniId.trim().equals( "er_def_date_01" ) ) 
		{
			flag = "Y" ;
		}
		else
		{
			flag = "N" ;	
		}
		*/
		flag="N";
				
		if ( count ==0 )
		{ 
			secName=tempSecName;
			secFormat=tempSecFormat;
			repeatField=tempRepeatField;
			// Added by Manimaran to fix the Bugzilla issue #2637
			for (i=0;i<secIds.size();i++)
			{
			     if (tempSecName.equals((secNames.get(i)).toString()) && (( (fieldUniIds.get(i))!=null) && ((fieldUniIds.get(i)).toString()).equals( "er_def_date_01" )))
			     {
			         flag = "Y" ;
			
			     }
			}
			
			if(secFormat.equals("N"))
			{
				secFormat=LC.L_Nontabular;/*secFormat="Non-Tabular";*****/
			}
			else if(secFormat.equals("T"))
			{
				secFormat=LC.L_Tabular;/*secFormat="Tabular";*****/
			}
			else
			{
				secFormat="-";
			}
			sb.append("<tr><td>"+secName+"</td>");
			sb.append("<td colspan=4 >"+LC.L_Repeat_Fld+": "+repeatField+" "+LC.L_Format+": "+secFormat+" </td></tr>");/*sb.append("<td colspan=4>Repeat Field: "+repeatField+" Format: "+secFormat+" <td></tr>");*****/
			secDeleteHref = "<A href= 'formSecDelete.jsp?flag="+flag+"&studyId="+studyId+"&lnkFrom="+lnkFrom+"&codeStatus="+codeStatus+"&calledFrom="+calledFrom+"&formSecId=" + tempSecId + "&formId=" + formId + "&mode=initial&srcmenu=" +src+"&selectedTab="+selectedTab+"' onClick=\"return confirmBoxSec('"+StringUtil.escapeSpecialCharJS(secName)+"','"+pageRight+"','"+codeStatus+"',"+secOffline+")\"><img src=\"./images/delete.gif\" title=\""+LC.L_Delete+"\" border=\"0\"/></A>" ;
		}
		else if ( ! tempSecId.equals((secIds.get(count-1)).toString())) 
		{
			secName=tempSecName;
			secFormat=tempSecFormat;
			repeatField=tempRepeatField;
			
			// Added by Manimaran to fix the Bugzilla issue #2637
			for (i=0;i<secIds.size();i++)
			{
			     if (tempSecId.equals((secIds.get(i)).toString()) && (( (fieldUniIds.get(i))!=null) && ((fieldUniIds.get(i)).toString()).equals( "er_def_date_01" )))
			     {
			              flag = "Y" ;
			
			     }
			}
			
			if(secFormat.equals("N"))
			{
				secFormat=LC.L_Nontabular;/*secFormat="Non-Tabular";*****/
			}
			else if(secFormat.equals("T"))
			{
				secFormat=LC.L_Tabular;/*secFormat="Tabular";*****/
			}
			else
			{
				secFormat="-";
			}
			fmtcount=1;
			repcount=1;
						
			secDeleteHref = "<A href= 'formSecDelete.jsp?flag="+flag+"&studyId="+studyId+"&lnkFrom="+lnkFrom+"&codeStatus="+codeStatus+"&calledFrom="+calledFrom+"&formSecId=" + tempSecId + "&formId=" + formId + "&mode=initial&srcmenu=" +src+"&selectedTab="+selectedTab+"' onClick=\"return confirmBoxSec('"+StringUtil.escapeSpecialCharJS(secName)+"','"+pageRight+"','"+codeStatus+"' ,"+secOffline+")\"><img src=\"./images/delete.gif\" title=\""+LC.L_Delete+"\" border=\"0\" /></A>" ;  
			sb.append("<tr><td>"+secName+"</td>");
			sb.append("<td colspan=4>"+LC.L_Repeat_Fld+": "+repeatField+" "+LC.L_Format+": "+secFormat+" <td></tr>");/*sb.append("<td colspan=4>Repeat Field: "+repeatField+" Format: "+secFormat+" <td></tr>");*****/					
		}
		else
		{
			secName="";
			secFormat=" ";
			repeatField=" ";
			fmtcount=0;
			repcount=0;
			secDeleteHref = "";
		}
		
		
		if((fieldNames.get(count)) != null)
		{
			fieldName = fieldNames.get(count).toString();
			if (fieldName.length()  >= 26)
			{
				fieldName=fieldName.substring(0,25) ;
				fieldName=fieldName.concat(" ...");
			}
		}
			
		fieldType = ((fieldTypes.get(count)) == null)?"-":(fieldTypes.get(count)).toString();
		fieldDataType = ((fieldDataTypes.get(count)) == null)?"-":(fieldDataTypes.get(count)).toString();
		formFldId = formFldIds.get(count).toString();
		formFldSeq = ((Integer)formFldSeqs.get(count)).intValue();

		if(fieldNames.get(count)==null)
		{
			fieldName = "-";
			fldDisplayHref="-";
			deleteHref="-";
		}
		else 
		{
			fieldName=StringUtil.replace(fieldName,"<","&lt;");
			fieldName=StringUtil.replace(fieldName,">","&gt;");
			if (count > 0) { 
		 	    upperFormFldId = formFldIds.get(count-1).toString();
			    upperSeq =  ((Integer)formFldSeqs.get(count -1)).intValue();
			}
			
			if (count < len-1) {
			    lowerFormFldId = formFldIds.get(count+1).toString();
			    lowerSeq =  ((Integer)formFldSeqs.get(count +1)).intValue();  
			}

			if(fieldType.equals("E"))
			{
				fldDisplayHref="<A href= \" # \" onClick=\"openFieldDisplayWin( '"+formId+"' , '"+formFldId+"' ,document.formlib,'"+pageRight+"','"+codeStatus+"')\" ><img title=\""+LC.L_MdfyField+"\" src=\"./images/edit.gif\" border =\"0\"></A>" ;//LC.L_Modify+/*MOdily*****/"</A>" ;				
			}
			else if( fieldType.equals("M") && (! fieldDataType.equals("ML")) )
			{
				fldDisplayHref="<A href= \" # \" onClick=\"openMultipleSectionWinMode('"+formId+"' , '"+formFldId+"' ,document.formlib,'"+pageRight+"','"+codeStatus+"')\" ><img title=\""+LC.L_MdfyField+"\" src=\"./images/edit.gif\" border =\"0\"></A>" ;//+LC.L_Modify+/*MOdily*****/"</A>" ;
			}
			else if( fieldType.equals("M") && ( fieldDataType.equals("ML")) )
			{
				fldDisplayHref="<A href= \" # \" onClick=\"openEditLookupWin('"+formId+"' , '"+formFldId+"' ,document.formlib,'"+pageRight+"','"+codeStatus+"')\" ><img title=\""+LC.L_MdfyField+"\" src=\"./images/edit.gif\" border =\"0\"></A>" ;//LC.L_Modify+/*MOdily*****/"</A>" ;
			}	
			else if(fieldType.equals("C"))
			{
				fldDisplayHref="<A href= \" # \" onClick=\"openEditCommentsWin('"+formId+"' , '"+formFldId+"' ,document.formlib,'"+pageRight+"','"+codeStatus+"')\" ><img title=\""+LC.L_MdfyField+"\" src=\"./images/edit.gif\" border =\"0\"></A>" ;//LC.L_Modify+/*MOdily*****/"</A>" ;
			}
			else if(fieldType.equals("H"))
			{
				fldDisplayHref="<A href= \" # \" onClick=\"openEditHorizontalLineWin('"+formId+"' , '"+formFldId+"' ,document.formlib,'"+pageRight+"','"+codeStatus+"')\" ><img title=\""+LC.L_MdfyField+"\" src=\"./images/edit.gif\" border =\"0\"></A>" ;//LC.L_Modify+/*MOdily*****/"</A>" ;
			}
			else if(fieldType.equals("S"))
			{
				fldDisplayHref="<A href= \" # \" onClick=\"openTypeEdit('"+formId+"' , '"+formFldId+"' ,document.formlib,'"+pageRight+"','"+codeStatus+"','spaceType.jsp')\" ><img title=\""+LC.L_MdfyField+"\" src=\"./images/edit.gif\" border =\"0\"></A>" ;//LC.L_Modify+/*MOdily*****/"</A>" ;
			}
			else if(fieldType.equals("F"))
			{
				fldDisplayHref="<A href= \" # \" onClick=\"openTypeEdit('"+formId+"' , '"+formFldId+"' ,document.formlib,'"+pageRight+"','"+codeStatus+"','linkFormType.jsp')\" ><img title=\""+LC.L_MdfyField+"\" src=\"./images/edit.gif\" border =\"0\"></A>" ;//LC.L_Modify+/*MOdily*****/"</A>" ;
			}
						
			tempFldName = StringUtil.encodeString(fieldName);
			if ( 	fieldUniId.trim().equals( "er_def_date_01" ) ) 
			{
				deleteHref = "" ;
			}
			else 
			{
			//Rohit Bug# 4525	
			//if( (calledFrom.equals("St") || calledFrom.equals("A")) && codeStatus.equals("Active")){				
			//	deleteHref="<A href= '#' onClick=\"alert('aaa');return 0;\"><img src=\"./images/delete.gif\" border=\"0\" align=\"left\"/></A>";
				
			//}else{
				if (fieldType.equals("F"))
					deleteHref="<A href= ' fielddelete.jsp?studyId="+studyId+"&nkFrom="+lnkFrom+"&codeStatus="+codeStatus+"&calledFrom="+calledFrom+"&formFldId=" + formFldId + "&formId=" + formId + "&mode=initial&srcmenu=" +src+"&selectedTab="+selectedTab+"' onClick=\"return confirmBox('"+tempFldName.replace("[VELSP]"," ")+"','"+pageRight+"','"+codeStatus+"',"+fldOffline+",'"+fieldType+"')\"><img src=\"./images/delete.gif\" title=\""+LC.L_Delete+"\" border=\"0\"  align=\"left\"/></A>";
				else
					deleteHref="<A href= ' fielddelete.jsp?studyId="+studyId+"&nkFrom="+lnkFrom+"&codeStatus="+codeStatus+"&calledFrom="+calledFrom+"&formFldId=" + formFldId + "&formId=" + formId + "&mode=initial&srcmenu=" +src+"&selectedTab="+selectedTab+"' onClick=\"return confirmBox('"+tempFldName.replace("[VELSP]"," ")+"','"+pageRight+"','"+codeStatus+"',"+fldOffline+",'"+fieldDataType+"')\"><img src=\"./images/delete.gif\" title=\""+LC.L_Delete+"\" border=\"0\"  align=\"left\"/></A>";
			//}		
			}
			
		}
%>
				 
<%		if(fmtcount==1)
		{
%>
			<tr height="20" ><td colspan="2"><P class="sectionHeadingsFrm"><%=secName%></P></td>					
			
			<td align="left" ><b><%=LC.L_Repeat_Flds%><%--Repeat Fields*****--%>:<%=repeatField%></b></td>
			<td  align="left" ><b><%=LC.L_Format%><%--Format*****--%>: <%=secFormat%></b></td>  
			
			<%if(pageRight > 4 && ((codeStatus.equals("WIP")) || (codeStatus.equals("Offline")))) {%>
				<td>&nbsp;</td>
<%			}
			if( (calledFrom.equals("St") || calledFrom.equals("A")) && codeStatus.equals("Active")){
				//Rohit Bug# 4525 : I know that after commenting below line the if and else part become same but I keep it for any future changes 
				//secDeleteHref = "<A href= '#' onClick=\"return 0;\"><img src=\"./images/delete.gif\" border=\"0\" align=\"left\"/></A>" ;
				%>
				<td><%=secDeleteHref%> </td>
	      <%}else {%>
	      
	      <td><%=secDeleteHref%> </td>
	      <%}
	      }%>
			</tr>	
				
				
	<%	if ((count%2)==0) 
		{	%>
			 <tr class="browserEvenRow"> 
			<%sb.append("<tr class='browserEvenRow'>");
		}else
		{       %>
			 <tr class="browserOddRow"> 
			<%sb.append("<tr class='browserOddRow'>");
		}       %>
				
		<%if(pageRight > 4 && ((codeStatus.equals("WIP")) || (codeStatus.equals("Offline"))))
		{ %>
			<td width="15%">

			<table border="0" width="100%">
				<tr>
				<td width="50%">
	
<% 
//	if(!oldtempSecId.equals(tempSecId)){
//		tcount=1;
//   }else{
//		tcount=tcount+1;
//	}
//	oldtempSecId=tempSecId;


			if ( formFldNum >= 1)
			{  
				if ( count ==0 )
				{	 %>  
    <input type="image" src="../images/jpg/down.gif" title="<%=LC.L_MoveUp%>" onclick="changeSeq(document.formlib,'<%=formFldId%>','<%=formFldSeq%>','<%=lowerFormFldId%>','<%=lowerSeq%>','<%=calledFrom%>','<%=codeStatus%>','<%=lnkFrom%>','<%=studyId%>')">

	</td><td align="right">(<%=formFldSeq%>)&nbsp;

	<%}
	else if ( ! tempSecId.equals((secIds.get(count-1)).toString()) ) 
	{ %>

	<input type="image" src="../images/jpg/down.gif" title="<%=LC.L_MoveUp%>" onclick="changeSeq(document.formlib,'<%=formFldId%>','<%=formFldSeq%>','<%=lowerFormFldId%>','<%=lowerSeq%>','<%=calledFrom%>','<%=codeStatus%>','<%=lnkFrom%>','<%=studyId%>')">
	</td><td align="right">(<%=formFldSeq%>)&nbsp;
	<%}
	else if ( (count+1) != len )    
	{ 
		if ( ! tempSecId.equals((secIds.get(count+1)).toString()))  							
		{ %>
		 <input type="image" src="../images/jpg/up.gif" title="<%=LC.L_MoveDown%>" onclick="changeSeq(document.formlib,'<%=formFldId%>','<%=formFldSeq%>','<%=upperFormFldId%>','<%=upperSeq%>','<%=calledFrom%>','<%=codeStatus%>','<%=lnkFrom%>','<%=studyId%>')">
	</td><td align="right">(<%=formFldSeq%>)&nbsp;
		  
		<%}
		else
		{ %>
		<input type="image" src="../images/jpg/up.gif" title="<%=LC.L_MoveDown%>" onclick="changeSeq(document.formlib,'<%=formFldId%>','<%=formFldSeq%>','<%=upperFormFldId%>','<%=upperSeq%>','<%=calledFrom%>','<%=codeStatus%>','<%=lnkFrom%>','<%=studyId%>')">
		<input type="image" src="../images/jpg/down.gif" title="<%=LC.L_MoveUp%>" onclick="changeSeq(document.formlib,'<%=formFldId%>','<%=formFldSeq%>','<%=lowerFormFldId%>','<%=lowerSeq%>','<%=calledFrom%>' ,'<%=codeStatus%>','<%=lnkFrom%>','<%=studyId%>')">
	</td><td align="right">(<%=formFldSeq%>)&nbsp;
		
		<%} 
						 
	} 
	else 
	{  //count ==len %>
	<input type="image" src="../images/jpg/up.gif" title="<%=LC.L_MoveDown%>" onclick="changeSeq(document.formlib,'<%=formFldId%>','<%=formFldSeq%>','<%=upperFormFldId%>','<%=upperSeq%>','<%=calledFrom%>','<%=codeStatus%>','<%=lnkFrom%>','<%=studyId%>')">
	</td><td align="right">(<%=formFldSeq%>)&nbsp;
	<%} //count ==len 
}  //end of formfldNum > 0	%>
</td></tr></table>
	</td>
<%} //end of rights %>  
				<td width="22%"><%=fieldName%></td>
				<td width="23%"><%=fieldUniId%></td>
				<%if(fieldType.equals("C"))
					{
					  fieldType=LC.L_Comment;/*fieldType="Comment";*****/
					}
				  else if(fieldType.equals("E"))
					{
					  fieldType=LC.L_Edit;/*fieldType="Edit";*****/
					}
			  		else if( fieldType.equals("M") && (! fieldDataType.equals("ML")) )
					{
					  fieldType=LC.L_Multiple_Choice;/*fieldType="Multiple Choice";*****/
					}
				else if( fieldType.equals("M") && (fieldDataType.equals("ML")) )
					{
					fieldType=LC.L_Lookup;/*fieldType="Lookup";*****/
					}
				  else if(fieldType.equals("H"))
					{
					  fieldType=LC.L_Horizontal_Line;/*fieldType="Horizontal Line";*****/
					}
				  else if(fieldType.equals("S"))
					{
					  fieldType=LC.L_Space_Break;/*fieldType="Space Break";*****/
					}	
				   else if(fieldType.equals("F"))
					{
					   fieldType=LC.L_Form_Link;/*fieldType="Form Link";*****/
					}	
						%>
				<td width="18%"><%=fieldType%></td>	
				<!--<td width="14%"><%//=fldDisplayHref%></td>-->
				<td width="14%" align="center"><%=fldDisplayHref%></td>
				<td width="13%"><%=deleteHref%></td>
				
			</tr>
	
						
<%
					sb.append("<td>"+formFldSeq+"</td>");
					sb.append("<td>"+fieldName+"</td>");
					sb.append("<td>"+fieldUniId+"</td>");
					sb.append("<td>"+fieldType+"</td></tr>");	
		
	}
%>
	
	</table>
						<%
						
					sb.append("</table>");	
					String str = sb.toString();


					String printerLink=reportIO.saveReportToDoc(str,"htm","frmfldbrowser");
					String excelLink=reportIO.saveReportToDoc(str,"xls","frmfldbrowser");

					%>
						<Input type="hidden" name="srcmenu" value=<%=src%> >
						<Input type="hidden" name="printerLink" value=<%=printerLink%> >
						<Input type="hidden" name="excelLink" value=<%=excelLink%> >
						<input name="formId" type=hidden value=<%=formId%>>
						<input name="formFldId" type=hidden value=<%=formFldId%>>
						<input name="mode" type=hidden value=<%=mode%>>
						<input name="calledFrom" type=hidden value=<%=calledFrom%>>
						<input name="lnkFrom" type=hidden value=<%=lnkFrom%>>
						<input name="studyId" type=hidden value=<%=studyId%>>
																													
		<%if(calledFrom.equals("St"))
			{%>
		<A href="studyprotocols.jsp?srcmenu=tdmenubaritem3&selectedTab=7&mode=<%=mode%>&studyId=<%=studyId%>&calledFrom=<%=calledFrom%>"><%=MC.M_BackTo_StdSetupPage%><%--Back to the <%=LC.Std_Study%> Setup page*****--%></A>
		<%}	
	   if(calledFrom.equals("L"))
		{%>
		<A href ="formLibraryBrowser.jsp?selectedTab=3&mode=final&srcmenu=<%=src%>"><%=MC.M_BackTo_FrmLib%><%--Back to the Form Library*****--%> </A>
		<%}
	    if(calledFrom.equals("A"))
		{%>
		<A href ="accountforms.jsp?selectedTab=&srcmenu=tdMenuBarItem2"><%=MC.M_BackTo_MgmtBrowser%><%--Back to the Form Management browser*****--%></A>
		<%}%>
</form>
	 <%
		
	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

} //end of formId=0
}
else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div><div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/></div>

</body>

</html>


			
