<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Form_Msg%><%--Form Messages*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>


<SCRIPT Language="javascript">

 function  validate(formobj)
 {
     checkQuote="N";
	 
	 //KM-#3967
	 if (formobj.msgType.length == undefined)
	 {
		     if (formobj.msgType.value == 'N') {
		 	 if (  ( formobj.nameList.value == null  )  ||   formobj.nameList.value == "" )
			 {  alert("<%=MC.M_SelUsr_ForSendNotific%>");/*alert("Please select Users for sending notifications");*****/
			    return false ;
			 }		 
		 } else if (formobj.msgType.value == 'P') {
		 	 if (  ( formobj.messageText.value == null  )  ||   formobj.messageText.value == "" )
			 {  alert("<%=MC.M_Etr_MsgText%>");/*alert("Please enter the Message Text");*****/
			    return false ;
			 }		 
		 }	

	 }
	 else {
	 
	 for (i=0;i<formobj.msgType.length;i++)
	 {
	   if (formobj.msgType[i].checked) {
   	     if (formobj.msgType[i].value == 'N') {
		 	 if (  ( formobj.nameList.value == null  )  ||   formobj.nameList.value == "" )
			 {  alert("<%=MC.M_SelUsr_ForSendNotific%>");/*alert("Please select Users for sending notifications");*****/
			    return false ;
			 }		 
		 } else if (formobj.msgType[i].value == 'P') {
		 	 if (  ( formobj.messageText.value == null  )  ||   formobj.messageText.value == "" )
			 {  alert("<%=MC.M_Etr_MsgText%>");/*alert("Please enter the Message Text");*****/
			    return false ;
			 }		 
		 }
	   }
	 }

	 }

	 	if(formobj.codeStatus.value != "WIP"){	
	 	 	if (  !(validate_col('e-Signature',formobj.eSign)     )   ) return false
	 		 
	 		<%-- if(isNaN(formobj.eSign.value) == true) 
	 			{
	 			alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	 			formobj.eSign.focus();
	 			 return false;
	 	   		} --%>
	 	 	}

	
	
   }
   
function openWin(userNames, userIds,formobj)
{
	
  var names =   formobj.nameList.value; 
  var usrlist  = formobj.userlist.value;
  
   while(names.indexOf(' ') != -1){
		   names = names.replace(' ','~');

	  }
	
  if(formobj.msgType.length == undefined ) {

	if ( formobj.msgType.value == "P" && formobj.msgType.checked )
	{
		alert("<%=MC.M_CntSeltUsr_InPopMsg%>");/*alert("Cannot select the user in case of pop messages");*****/
		
	}
	else if ( formobj.msgType.value == "N")
	{
		
		windowToOpen = "selectremoveusers.jsp?fname=&lname=&from=formNotify&mode=initial&userIds=" + usrlist + "&userNames=" + names ;
    	windowName=window.open(windowToOpen,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200");
		windowName.focus();
	}

	} else {

	if ( formobj.msgType[0].checked )
	{
		alert("<%=MC.M_CntSeltUsr_InPopMsg%>");/*alert("Cannot select the user in case of pop messages");*****/
		
	}
	else if ( formobj.msgType[1].value == "N")
	{
		
		windowToOpen = "selectremoveusers.jsp?fname=&lname=&from=formNotify&mode=initial&userIds=" + usrlist + "&userNames=" + names ;
    	windowName=window.open(windowToOpen,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200");
		windowName.focus();
	} 
		}
}


function  setPopupMessage(formobj )
{
	
	formobj.nameList.value = "";
	

}

</SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="formNotifyJB" scope="request"  class="com.velos.eres.web.formNotify.FormNotifyJB"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB"%>

<body>
<DIV class="popDefault" id="div1"> 
 <%

	HttpSession tSession = request.getSession(true);
	 
   if (sessionmaint.isValidSession(tSession))
   {
	  int pageRight = 0;
	 GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
	 pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MFRMLIB"));

	 String codeStatus = request.getParameter("codeStatus");
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
	<jsp:include page="include.jsp" flush="true"/>

<%
	if (pageRight >=4)
	{
	
		String mode=request.getParameter("mode");
		String formNotifyId="";
		String formLibId=request.getParameter("formLibId");
		String msgType="";
		String msgSendType="";
		String userlist="";
		String messageText="";
		String nameList="";
		
		
		String userNames ="";
		//out.println("mode"+mode);
		
		if (mode.equals("M"))
		{ 
				formNotifyId=request.getParameter("formNotifyId");
				//out.println("formNotifyId"+formNotifyId);
				formNotifyJB.setFormNotifyId(Integer.parseInt(formNotifyId));
				formNotifyJB.getFormNotifyDetails();
				msgType=formNotifyJB.getMsgType();
				msgSendType=formNotifyJB.getMsgSendType();
				messageText=formNotifyJB.getMsgText();
				userlist=formNotifyJB.getUserList();
				//nameList=formNotifyJB.getNameList( );
				nameList = ((formNotifyJB.getNameList()) == null)?"":(formNotifyJB.getNameList()).toString() ;
				
				
				
		}
		
%>

	

  	<P class="sectionHeadings"> <%=MC.M_FrmSet_FrmNotify%><%--Form Settings >> Form Notification*****--%> </P>
    <Form name="formNotify" id="formNotifyFrm" method="post" action="formNotifySubmit.jsp" onsubmit="if (validate(document.formNotify)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<Input type="hidden" name="mode" value=<%=mode%>>
	<Input type="hidden" name="codeStatus" value=<%=codeStatus%>> 
	<Input type="hidden" name="formNotifyId" value=<%=formNotifyId%>>
	<Input type="hidden" name="formLibId" value=<%=formLibId%>>
	<Input type="hidden" name="userlist" value=<%=userlist%>>
	
  	<table width="99%" cellspacing="0" cellpadding="0" border="0">
    <tr> 
      <td width="15%"><%=LC.L_Msg_Type%><%--Message Type*****--%></td>
      <td width="45%">
	  <% 
		//KM-3967
	   if ((!(codeStatus.equals("Freeze"))) && (!(codeStatus.equals("Active")))) {
	  
	   
	   if (msgType.equals("P")) 
	  {%>
	  		<input type="radio" name="msgType" value="P" Checked  onClick = "setPopupMessage(document.formNotify)"> <%=MC.M_PopUpMsg_OnSbmtFrm%><%--Pop-up Message when user submits form*****--%>
	  <%}  
	    else if (msgType.equals("") ) 
	  {%>
			   <input type="radio" name="msgType" value="P" Checked onClick = "setPopupMessage(document.formNotify)"> <%=MC.M_PopUpMsg_OnSbmtFrm%><%--Pop-up Message when user submits form*****--%> 
	  <%}
	  else
	  {%>
			   <input type="radio" name="msgType" value="P" onClick = "setPopupMessage(document.formNotify)"> <%=MC.M_PopUpMsg_OnSbmtFrm%><%--Pop-up Message when user submits form*****--%>
	  <%}
	   } else {

		if (msgType.equals("P")) { %>
			<input type="radio" name="msgType" value="P" Checked  onClick = "setPopupMessage(document.formNotify)"> <%=MC.M_PopUpMsg_OnSbmtFrm%><%--Pop-up Message when user submits form*****--%>

		<%}


	   }
	  %>
	 
	 
	  </td>
	   <td width="20%">
	   <%if (msgSendType.equals("F"))
	   {%>  <select name="msgSendType" >
   			<option value="F"Selected> <%=LC.L_First_Time%><%--First Time*****--%></option>
   			<option value="E"> <%=LC.L_Every_Time%><%--Every Time*****--%> </option>
   			</select>
		<%} else if (msgSendType.equals("E"))
		{%> <select name="msgSendType" >
   			<option value="F"> <%=LC.L_First_Time%><%--First Time*****--%></option>
   			<option value="E" Selected> <%=LC.L_Every_Time%><%--Every Time*****--%> </option>
   			</select>
		<%}%>
		 <%if (msgSendType.equals(""))
	   {%>  <select name="msgSendType" >
   			<option value="F"Selected> <%=LC.L_First_Time%><%--First Time*****--%></option>
   			<option value="E"> <%=LC.L_Every_Time%><%--Every Time*****--%> </option>
   			</select>
		<%} %> 
		</td>
	  </tr>
	  <tr>
		<td></td>
		<td>
		<% if (msgType.equals("N"))
		{%>
    		<input type="radio" name="msgType" value="N" Checked > <%=MC.M_Email_SentToUsr%><%--Notification Email sent to specified users*****--%>
		<% }
		 else if (msgType.equals("") && (codeStatus.equals("Freeze") || codeStatus.equals("Active") ) )
		{%>
    		<input type="radio" name="msgType" value="N" checked > <%=MC.M_Email_SentToUsr%><%--Notification Email sent to specified users*****--%>
			
		<%} 
		 else 
		{%>	
			<input type="radio" name="msgType" value="N" > <%=MC.M_Email_SentToUsr%><%--Notification Email sent to specified users*****--%>
		 <%} %>
		
		</td>
		<td></td>
	</tr>
	
	</table>
	
	<br>
	
	<%
		//userNames  = nameList.replace(' ','~');
	
	%>
	
	<table width="99%" cellspacing="0" cellpadding="0" border="0" class="midalign">
		<tr>
			<td width="35%"><%=MC.M_UsrToReceive_Notific%><%--Users to receive notification are*****--%> </td>
			<td width="30%"><input type="text" name=nameList size = 40 MAXLENGTH = 50 value="<%=nameList%>" readOnly="true"> </td>
				 
			<td width="20%"> <a href="#" onclick="openWin('<%=userNames%>','<%=userlist%>',document.formNotify )"><%=LC.L_SelOrDeSel_Usrs%><%--Select/Deselect user(s)*****--%> </a> </td> 
		</tr>			
	 		<tr height="10"><td colspan="3"></td></tr>
	 		<tr>
			 <td ><label><%=LC.L_Msg_Text%><%--Message Text*****--%></label><Font class="mandatory"> *</Font></td>
			 <td> <textarea name="messageText" cols="40" rows="5" ><%=messageText%></textarea></td>
			 <td>&nbsp;</td>
	  		</tr>		  
	</table>
	<br>
	<%
	String showSubmit ="";
	if (msgType.equals("P") && (codeStatus.equals("Freeze") || codeStatus.equals("Active") )) {
	showSubmit="N";
	}
			
	%>


	<%if(pageRight==6 || pageRight==7){if(codeStatus.equals("WIP")){
						%>
						<table align="center"><tr>
	  <td align="center"><br />
				<button type="submit" id="submit_btn" ondblclick="return false"><%=LC.L_Submit%></button>
			</td> 
	  </tr></table>
     
<%}else{ 
%>

	<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="editBoxFieldFrm"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include> 

<br>
	<% } } %>
     <br>

 	 <table width="100%" cellspacing="0" cellpadding="0">
      <td align=right> 

      </td> 
      </tr>
  </table>
  </Form>
<%

	} //end of if body for page right
	else
	{
%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
<%

	} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
<%

}

%>
 <div class = "myHomebottomPanel"> 
 <jsp:include page="bottompanel.jsp" flush="true"/>
 </div>
</body>

</html>


		
	
		
		
		
			
		
		
		
	
