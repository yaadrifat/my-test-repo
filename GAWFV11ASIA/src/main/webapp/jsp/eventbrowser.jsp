<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title><%=LC.L_Calendar_Events %><%-- Calendar >> Events*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*" %>

<script src="./js/jquery/jquery.min.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>
<SCRIPT language="javascript">

var table="";
$(document).ready(function(){
table=jQuery("#embedDataHere").html();
});

//KM-03Sep09
function callAjaxGetCatDD(formobj){
	   selval = formobj.cmbLibType.value;
 	   new VELOS.ajaxObject("getCodeDropDown.jsp", {
   		urlData:"selectedVal="+selval+"&ddName=categoryId&codeType=L&ddType=category&from=valAllCat" ,
		   reqType:"POST",
		   outputElement: "span_catId" } 
  
   ).startRequest();
}

function sorting(formObj,orderBy){
	
	if (formObj.orderType.value=="asc") {
		formObj.orderType.value= "desc";

	} else 	if (formObj.orderType.value=="desc") {
		formObj.orderType.value= "asc";
	}
		orderType=formObj.orderType.value;
		formObj.orderBy.value = orderBy;
		var protocolId=jQuery("#prtcolId").val();
		var calledfrom=jQuery("input[name='calledFrom']").val();
		var urlCall="manageVisitEvents.jsp?calledFrom="+calledfrom+"&protocolId="+protocolId+"&orderBy="+orderBy+"&orderType="+orderType+"&Dialog=PopUpSorting";
		jQuery.ajax({
			url : urlCall,
			cache: false,
			success : function (data) {
		 jQuery("#embedDataHere").html("");
      	 jQuery("#seq_events").html(data);
      	 dragAndDropFunc();
			}
		});
}

function checkBeforeClose()
{
		checkChanges();
		if((sequenceShuffleData!="" && sequenceShuffleData!="[]" ))
		{ 	
			var r=confirm("<%=MC.M_SaveBeforeMovingFwd%>");
			if (r)
			  {
				return false;
			  }
			else
			{
				flag=true;
			}
		}
	return true;
}

function closeMessageDialog(divId)
{
	jQuery("#"+divId).dialog("close");
}

function openMessageDialog(divId)
{
	var messageHgth = navigator.userAgent.indexOf("MSIE") != -1 ? 150 : 80;
	jQuery("#"+divId).dialog({
		height: messageHgth,width: 500,position: 'center',resizable: false,modal: true,autoOpen: false
    }).siblings('.ui-dialog-titlebar').remove();
	jQuery("#"+divId).dialog("open");	
}

function dragAndDropFunc()
{
	jQuery( "#seq_table tbody" ).sortable({
    	items: "tr:not(.noEvent)",
    	axis: "y" , scroll: true,
    	placeholder: "ui-state-highlight",
    	forcePlaceholderSize: true,
    	beforeStop: function( event, ui ) {return nd();}
    	
	});
}

var jSonEvents="";
var flag=false;
var contentChanged="";

function sequenceEventsDialog(grpsRights)
{
	if(grpsRights<6)
	{
		alert("<%=LC.L_Edit_PermDenied%>");
		return false;	
	}
	
	lcalStatus=jQuery("#calStus").val();
	codelstDesc=jQuery("#calstusDsc").val();
	if (lcalStatus == 'D' || lcalStatus == 'F' || lcalStatus == 'A') {		
		var paramArray = [codelstDesc];
		alert(getLocalizedMessageString("M_CntChngEvtSeq_CldrStat",paramArray));
		return false;
	}
	var calledfrom=jQuery("input[name='calledFrom']").val();
	var duration = jQuery("input[name='duration']").val();
	var protocolId=jQuery("#prtcolId").val();
	contentChanged="no";
    if(flag){
        flag=false;
        jQuery("#embedDataHere").html("");
        jQuery("#embedDataHere").html(table);
	}
	jQuery("#seq_events").dialog({ 
		height: 450,
		width: 550,
		resizable: false,
		position: 'center',
		modal: true,
		zIndex: -1,
		buttons: [
		          {
		              text: L_Save,
		              click: function() {
		              updateEventSequence();
		              }
		          }
		      ],
		close: function() {
		if(flag || contentChanged=="no"){
		$j("#seq_events").dialog("destroy"); 
		}
		else{
			$j("#seq_events").dialog("destroy");
			window.location="eventbrowser.jsp?protocolId="+protocolId+"&calledFrom="+calledfrom+"&duration="+duration+"&mode=M&srcmenu=&orderBy=Cost&orderType=asc&selectedTab=2";
		}
			},
			beforeClose: function(event, ui) { if(!checkBeforeClose()){ return false;} }
        });

	dragAndDropFunc();
	jSonEvents="";
	getDefaultSequence();
}

function getDefaultSequence(){
var indexNo=0;
if(jSonEvents.length>0)
{
	jSonEvents="";
}
	jSonEvents ="[";
	jQuery("#seq_table tbody tr").each( function() {
			indexNo++;
		    jSonEvents +="{";
		    jSonEvents +="\"index\":\""+indexNo+"\",";
    		var trValue =jQuery(this).html();
		    jQuery(this).find("td").each(function(index){
			 
	           	var tdValue =jQuery(this).html();
	        
	            if(tdValue!="")
    	    	{
	             	if(index==0)
	    	    		{
	    		    			jSonEvents +="\"eventId\":\""+tdValue+"\",";
	    	    		}
	    	    	else if (index==1)	
    	    		{
	        			jSonEvents +="\"seq\":\""+tdValue+"\",";
    	     		}
	    	    	else if (index==2)	
	    	    		{
	    	    			jSonEvents +="\"cat\":\""+tdValue+"\",";
	    	     		}
	    	    	else{
	    	    		tdValue = tdValue.replace(/[^a-zA-Z 0-9]+/g, "");
	    	    		jSonEvents +="\"name\":\""+tdValue+"\"";
	    	    	}
    	    	   }
    	    	});
	     	jSonEvents +="},";
    	 });
	  jSonEvents +="]";
	  jSonEvents  = jSonEvents.replace('},]','}]');
}

function updateEventSequence(){
	checkChanges();
	var calledfrom=jQuery("input[name='calledFrom']").val();
	var orderBy=jQuery("input[name='orderBy']").val();
	var protocolId=jQuery("#prtcolId").val();
	if((sequenceShuffleData!="" && sequenceShuffleData!="[]" ))
		{ 
			var urlCall="updateSequenceEvents.jsp?sequenceShuffleData="+sequenceShuffleData+"&calledFrom="+calledfrom+"&protocolId="+protocolId+"&Dialog=PopUp";
			openMessageDialog('progressMsg');
			jQuery.ajax({
					url : urlCall,
					success : function (data) {
					if(data.indexOf("Successful")>0)
					{
					 closeMessageDialog('progressMsg');	
					}else{
						closeMessageDialog('progressMsg');
						}
					var urlCall="manageVisitEvents.jsp?calledFrom="+calledfrom+"&protocolId="+protocolId+"&orderBy=Cost&orderType=asc&Dialog=PopUp";
					jQuery.ajax({
							url : urlCall,
							cache: false,
							success : function (data) {
							jQuery("#seq_events").html(data);
							dragAndDropFunc();
							}
							});
					jSonEventsSeq="";
					sequenceShuffleData="";
			    	}
					});
			contentChanged="yes";
		}else{
			alert(M_ThereNo_ChgToSave);
			return false;
		}
}

var sequenceShuffleData="";

function checkChanges(){
	changeEventsSequence();
		if(jSonEventsSeq !="[]" && jSonEventsSeq !="")
		{
			sequenceShuffleData="[";
			var eventJsonData = jQuery.parseJSON(jSonEvents);
			var eventSeqJson  = jQuery.parseJSON(jSonEventsSeq);
			jQuery.each(eventJsonData , function(key, value) { 
				var indexEvent=value.index;
				var eventNo=value.seq;
				var eventName=value.name;
				var eventId=value.eventId;
				jQuery.each(eventSeqJson , function(key, value) {
					var indexSeqEvent=value.index;
					var eventSeqNo=value.seq;
					var eventSeqId=value.eventId;
					var evntSeqName=value.name;
					
					if(indexSeqEvent == indexEvent && eventSeqNo!=eventNo ) 
					  {
						sequenceShuffleData +="{";
						sequenceShuffleData +="\"eventID\":\""+eventSeqId+"\",";
						sequenceShuffleData +="\"eventSeqNew\":\""+eventNo+"\"";
						sequenceShuffleData +="},";
					  } 
				  });
				
			  });
			sequenceShuffleData +="]";
			sequenceShuffleData  = sequenceShuffleData.replace('},]','}]');
		}
}

var jSonEventsSeq="";
var visitSeqName="";

function changeEventsSequence(){
	var indexNo=0;
	jSonEventsSeq ="[";
		 jQuery("#seq_table tbody tr").each( function(){
			
				indexNo++;
			    jSonEventsSeq +="{";
			    jSonEventsSeq +="\"index\":\""+indexNo+"\",";
				var trValue =jQuery(this).html();
			    jQuery(this).find("td").each(function(index){
			   	var tdValue =jQuery(this).html();
 	            if(tdValue!="")
	    	    	{
 	             	if(index==0)
		    	    		{
 	    		    			jSonEventsSeq +="\"eventId\":\""+tdValue+"\",";
		    	    		}
		    	    	else if (index==1)	
	    	    		{
		        			jSonEventsSeq +="\"seq\":\""+tdValue+"\",";
	    	     		}
		    	    	else if (index==2)	
		    	    		{
		    	    			jSonEventsSeq +="\"cat\":\""+tdValue+"\",";
		    	     		}
		    	    	else{
		    	    		tdValue = tdValue.replace(/[^a-zA-Z 0-9]+/g, "");
		    	    		jSonEventsSeq +="\"name\":\""+tdValue+"\"";
		    	    	}
	    	    	   }
	    	    	});
 	     	jSonEventsSeq +="},";
	    	 });
		  jSonEventsSeq +="]";
		  jSonEventsSeq  = jSonEventsSeq.replace('},]','}]');
}

//Added by Manimaran for Clickable sorting.

	function setOrder(formObj,orderBy) {

		if (formObj.orderType.value=="asc") {
			formObj.orderType.value= "desc";

		} else 	if (formObj.orderType.value=="desc") {
			formObj.orderType.value= "asc";
		}
			orderType=formObj.orderType.value;
			formObj.orderBy.value = orderBy;
			lsrc = formObj.srcmenu.value;
			/*YK 04Jan- SUT_1_Requirement_18489*/
			var resetSort="";
			if(orderBy=="")
			{
				resetSort="true";
			}
			formObj.action="eventbrowser.jsp?mode=M&srcmenu="+lsrc+"&orderBy="+orderBy+"&orderType="+orderType+"&selectedTab=2&resetSort="+resetSort; //KM
			formObj.submit();
	}


//Added by Manimaran for Enh.#C7
function removeEvents(formobj,pageRight) {


    var j = 0;
    var cnt = 0;
    selStrs = new Array();
    len = formobj.len.value;
  	lmode = formobj.mode.value;
	lduration = formobj.duration.value;
	lprotocolId = formobj.protocolId.value;
	lsrcmenu = formobj.srcmenu.value;
	lcalledFrom = formobj.calledFrom.value;
	lcalStatus = formobj.calStatus.value;
	lcalassoc = formobj.calassoc.value;
	resetSort = formobj.resetSort.value; /*YK 10Jan - Bug #5725*/

	//JM: 19Jun2008
	 var offlineflagcheck ;
	 var offlineflagcheckTot = '';
	 offlineflagVals = new Array();

	//JM: 22Feb2011: #5858
	 codelstDesc = formobj.calStatusDesc.value;


	if(lcalledFrom == 'L' || lcalledFrom == 'P')
	   ltabname ='event_def';
	else
	   ltabname ='event_assoc';


	//Added by Manimaran to fix the issue#3397
	if (lcalStatus == 'D' || lcalStatus == 'F' || lcalStatus == 'A') {
		//JM: 22Feb2011: #5858
		//alert("Cannot Delete Events in a Deactivated calendar");		
		var paramArray = [codelstDesc];
		alert(getLocalizedMessageString("M_CntDelEvt_CldrStat",paramArray));/*alert("Cannot Delete Events from a Calendar with status "+codelstDesc);*****/
		return false;
	}



   if (f_check_perm(pageRight,'E') == true) {


    if (len ==0) {
    	alert("<%=MC.M_SelEvts_ToDel%>");/*alert("Please select Event(s) to be Deleted");*****/
       return false;

    }


   if(len==1){
   		if (formobj.eventCheck.checked) {

   			//JM: 19Jun2008, #3468
			offlineflagVal = formobj.offlnFlag.value;
			if (offlineflagVal.indexOf("1") >=0){
				//JM: 22Feb2011: #5858
				//alert("Cannot Delete Event(s) from an Offline for Editing calendar");
				var paramArray = [codelstDesc];
				alert(getLocalizedMessageString("M_CntDelEvt_CldrStat",paramArray));/*alert("Cannot Delete Events from a Calendar with status "+codelstDesc);*****/
				return false;

			} else{

				/*YK 10Jan - Bug #5725*/
	   		 /*if (confirm('Are you sure you would like to delete the Selected Events')) {*****/
	   		 if (confirm("<%=MC.M_SureToDel_SelEvt%>")) {
	      	 window.open("deleteeventfromprot.jsp?searchFrom=search&selectedTab=2&srcmenu="+lsrcmenu+"&selStrs="+formobj.eventCheck.value
	      	 +"&duration="+lduration+"&protocolId="+lprotocolId+"&calledFrom="+lcalledFrom+"&tableName="+ltabname+"&mode="+lmode
	      	 +"&calStatus="+lcalStatus+"&fromPage=eventbrowser&from=initial&calassoc="+lcalassoc+"&calId=&resetSort="+resetSort,"_self");
	      	 cnt++;
	      	 }
      	 	}
      	 }
      	 else if (cnt==0){
      	   //JM: 19Jun2008, #3468
      	   alert("<%=MC.M_SelEvt_ToDel%>");/*alert("Please select the Event to be Deleted");*****/
      	   return false;
      	 }
   }else{


     for(i=0;i<len;i++) {
     	if (formobj.eventCheck[i].checked){
   		   selStrs[j] = formobj.eventCheck[i].value;
		   //JM: 19Jun2008, #3468
		   offlineflagVals[j] = formobj.offlnFlag[i].value;

   		   j++;
	       cnt++;
		}
	 }

     if (cnt >0) {
			//JM: 19Jun2008, #3468
     		for (g=0; g < offlineflagVals.length; g++){
			  		offlineflagcheck = offlineflagVals[g];
					offlineflagcheckTot =offlineflagcheckTot +  offlineflagcheck+ ',';
			}

			if (offlineflagcheckTot.indexOf("1") >=0){

				//JM: 22Feb2011: #5858
				//alert("Cannot Delete Visit(s)/Event(s) in an Offline for Editing calendar");
				var paramArray = [codelstDesc];
				alert(getLocalizedMessageString("M_CntDelEvt_CldrStat",paramArray));/*alert("Cannot Delete Events from a Calendar with status "+codelstDesc);*****/
				return false;

			}else{


				/*YK 10Jan - Bug #5725*/
       			/*if(confirm('Are you sure you would like to delete the Selected Events')) {*****/
       			if(confirm('<%=MC.M_SureToDel_SelEvt%>')) {
      				window.open("deleteeventfromprot.jsp?searchFrom=search&selectedTab=2&srcmenu="+lsrcmenu+"&selStrs="+selStrs
      				+"&duration="+lduration+"&protocolId="+lprotocolId+"&calledFrom="+lcalledFrom+"&tableName="+ltabname+"&mode="+lmode
      				+"&calStatus="+lcalStatus+"&fromPage=eventbrowser&from=initial&calassoc="+lcalassoc+"&calId=&resetSort="+resetSort,"_self");

       			}else{
          			return false;
       			}
  	 		}
	 }
  //}

   if(cnt==0)
   {
	   alert("<%=MC.M_SelEvts_ToDel%>");/*alert("Please select Event(s) to be Deleted");*****/
     return false;
   }
}//
  } else {

		return false;

	}

}


//KM
/*function confirmBox(eventName,pgRight, calStatus) {

	if ( (calStatus == 'F') || (calStatus == 'A')) {
		alert("Cannot Delete Events in a Frozen/Active calendar");
		return false;
	}
	if (f_check_perm(pgRight,'E') == true) {

		if (confirm("Delete " + eventName + " from Protocol?")) {

		    return true;

		} else {

			return false;

		}

	} else {

		return false;

	}

}
*/



function openwindow(formobj,link) {

 //KM-03Sep09
 //KM-Mandatory removed - #4254
  //if (!(validate_col('Event Library Type',formobj.cmbLibType))) return false
  
  var evtLibType = formobj.cmbLibType.value;
  /*if ((formobj.calStatus.value == 'F') || (formobj.calStatus.value == 'D')) {
	  alert("Calendar Status is 'Deactivated' Event cannot be Added"); //KM-Alert message changed.
	  return false;
	}*/

	//KM-Alert message changed as per the current requirement.-3395
	
	//JM: 22Feb2011: #5858
	 codelstDesc = formobj.calStatusDesc.value;
	
	//JM: 22Feb2011: #5858 blocked below and optimized
	/*
	if ((formobj.calStatus.value == 'F')) {
		alert("Calendar Status is 'Frozen' Event cannot be Added"); 
		return false;
	}

	if ((formobj.calStatus.value == 'D')) {
		alert("Calendar Status is 'Deactivated' Event cannot be Added"); 
		return false;
	}


	if ((formobj.calStatus.value == 'F') || (formobj.calStatus.value == 'A')) {
		alert("Cannot add new events in a Frozen/Active calendar");
		return false;
	}
	*/

	if ((formobj.calStatus.value == 'F') || (formobj.calStatus.value == 'A') || (formobj.calStatus.value == 'D')) {
		var paramArray = [codelstDesc];
		alert(getLocalizedMessageString("M_CntAddEvt_CldrStat",paramArray));/*alert("Cannot Add Events in a Calendar with status "+codelstDesc);*****/
		return false;
	}	

	if (document.all) {


		lcategory = formobj.categoryId.value;

	}

	else

	{

		lcategory = formobj.categoryId[formobj.categoryId.selectedIndex].value;

	}

	ind = lcategory.indexOf("%");



	if (ind < 0)
	{
		lcatId = "All";
	}	else
	{
		lcatId = lcategory.substring(0,ind);

	}

	lcatName = lcategory.substring(ind+1,lcategory.length);



	lmode = formobj.mode.value;


	lduration = formobj.duration.value;

	lprotocolId = formobj.protocolId.value;

	lsrcmenu = formobj.srcmenu.value;

	lcalledFrom = formobj.calledFrom.value;

	lcalStatus = formobj.calStatus.value;

	lcost = formobj.cost.value;
    lcalltime="New" ;
	//******************************************************************//

	//	            Vishal-07/19/2002									//

	//			To resize the selectevent new window.                   //

	//*****************************************************************//

  //windowName=window.open("selectevent.jsp?catId="+lcatId+"&catName="+lcatName+"&mode="+lmode+"&srcmenu="+lsrcmenu+"&duration="+lduration+"&protocolId="+lprotocolId+"&calledFrom="+lcalledFrom+"&calStatus="+lcalStatus+"&cost="+lcost,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=400");


//    windowName=window.open(link+"&catId="+lcatId+"&catName="+lcatName,"Information","screenX=" + xOffset +",screenY="+yOffset + ",toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=550");
    windowName=window.open(link+"&catId="+lcatId+"&catName="+lcatName+"&evtLibType="+evtLibType,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=1000,height=650 top=100,left=150 ");
    	windowName.focus();

    //End-V



}






</SCRIPT>







<% String src;
String eventOverlibParameter=",";
String cptOverlibParameter=",";
String descrpOverlibParameter=",";
String noteOverlibParameter=",";

src= request.getParameter("srcmenu");

%>



<jsp:include page="panel.jsp" flush="true">



<jsp:param name="src" value="<%=src%>"/>

</jsp:include>

<jsp:include page="ui-include.jsp" flush="true"></jsp:include>   



<body style="overflow:hidden;">

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>



<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>

<jsp:useBean id="ctrldao" scope="request" class="com.velos.esch.business.common.EventdefDao"/>



<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>



<jsp:useBean id="assocdao" scope="request" class="com.velos.esch.business.common.EventAssocDao"/>





<%@ page language = "java" import = "com.velos.eres.business.group.*,com.velos.eres.business.common.*,com.velos.esch.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.StringUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB"%> <!--KM-->



<% int pageRight = 0;

   HttpSession tSession = request.getSession(true);

%>






 <%

 if (sessionmaint.isValidSession(tSession))



 {
	   String protocolId= request.getParameter("protocolId");

	   String calledFrom= "";	
	   calledFrom= request.getParameter("calledFrom");

	 if (calledFrom.equals("S")) {

	   StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");

	   if ((stdRights.getFtrRights().size()) == 0){

			pageRight= 0;

	   }else{

			pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));

	   }

	} else if ((calledFrom.equals("P")) || (calledFrom.equals("L"))) {

		GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");

		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));

	}
	//KM-#5000
	%>
<DIV class="BrowserTopn" id = "div1">
	<jsp:include page="protocoltabs.jsp" flush="true"/>
</DIV>	
	<% if (pageRight > 0) {
		 if(protocolId == "" || protocolId == null || protocolId.equals("null") || protocolId.equals("")) {
}}
 if(protocolId == "" || protocolId == null || protocolId.equals("null") || protocolId.equals("")) {
	 
	 %>
	<DIV class="BrowserBotN BrowserBotN_CL_1" style="overflow:auto; height:80%">
	  <jsp:include page="calDoesNotExist.jsp" flush="true"/>
	</DIV>
	  <%

	}else {


	   int accId = EJBUtil.stringToNum((String) (tSession.getValue("accountId")));



	   String description="";


	   String eventId="";

	   String eventType="";

	   String eventName="";

	   String eventCost="";

	   String prevCost="-1";

	   String eventNameHide="";

	   String calStatus="";

	   String catId="";

	   String calAssoc="";

	   String note = "";//KM

	   String cptCode = "";

	   String catgName ="";

	   String offlineFlag ="";
	   String facilityName = "";//KV:SW-FIN2c 
	   String actualeventCost = "";//KV:SW-FIN2b
	   String eventLibraryType = ""; // Ankit: PCAL-20282


	   int counter = 0;

	   Integer id;

	   String uName = (String) tSession.getValue("userName");

	   String duration= "";

		String newDuration = "";

		newDuration = (String)  tSession.getAttribute("newduration");

		if (StringUtil.isEmpty(newDuration))
		{
			duration = request.getParameter("duration");
		}
		else
		{
			duration = newDuration ;
		}


	   String mode= request.getParameter("mode");
	   mode=(mode==null)?"":mode;
	   mode=(EJBUtil.stringToNum(protocolId)>0)?"M":mode;


	   calStatus= request.getParameter("calStatus");
	   
	   
	 //JM: 22Feb2011: #5858
	   String calStatDesc = "";
	   SchCodeDao schcodedao = new SchCodeDao();	   
	   
	   if (calledFrom.equals("P") || calledFrom.equals("L")) {
		   calStatDesc = schcodedao.getCodeDescription(schcodedao.getCodeId("calStatLib",calStatus));
	   }else{
		   calStatDesc = schcodedao.getCodeDescription(schcodedao.getCodeId("calStatStd",calStatus));
	   }	   
	   
	   

	   calAssoc= request.getParameter("calassoc");

	   calAssoc=(calAssoc==null)?"P":calAssoc;

	   String displayType= request.getParameter("displayType");
	   String displayDur= request.getParameter("displayDur");
	   String pageNo= request.getParameter("pageNo");



	   ArrayList eventIds= null;

	   ArrayList names= null;

	   ArrayList descriptions= null;

	   ArrayList event_types= null;

	   ArrayList eventCosts= null;

	   ArrayList notes = null; //KM

	   ArrayList cptCodes = null;

	   ArrayList catgNames = null;

	   ArrayList offlineFlags = null;
	   
	   ArrayList facilityIds = null; //KV: SW-FIN2c
	   
	   ArrayList actualeventCosts = null; //KV: SW-FIN2b 
	   
	   ArrayList eventLibraryTypes = null; //Ankit: PCAL-20282 



	  //  salil
	 

	  // end  by salil



		String orderBy="", orderType = "";

	   
		orderBy=request.getParameter("orderBy");
		/*YK 04Jan- SUT_1_Requirement_18489*/
		String resetSort="";
		resetSort = request.getParameter("resetSort"); 
		resetSort = (resetSort==null)?"":resetSort;
		if(resetSort.equalsIgnoreCase("true"))
		{
			orderBy="";
		}
		//Calendar Enhancement Cal-35776_No1:- For Sequence "Order By Cost"
		if (orderBy==null){
			orderBy="Cost";
			//orderBy="lower(EVENT_LIBRARY_TYPE) , lower(EVENT_CATEGORY) , lower(NAME)"; //KM-3394, Ankit: PCAL-20282
		}
		//End Enhancement
		orderType = request.getParameter("orderType");
		if (orderType == null) orderType = "asc";



		 if (calledFrom.equals("P") || calledFrom.equals("L")) {

		   ctrldao= eventdefB.getProtSelectedEvents(EJBUtil.stringToNum(protocolId),orderType,orderBy);

		   eventIds=ctrldao.getEvent_ids();

		   names= ctrldao.getNames();
		   descriptions= ctrldao.getDescriptions();
		   event_types= ctrldao.getEvent_types();
		   eventCosts= ctrldao.getCosts();
		   notes = ctrldao.getNotes();//KM
		   cptCodes = ctrldao.getCptCodes();
		   catgNames = ctrldao.getEventCategory();
		   facilityIds = ctrldao.getFacility();// KV: SW-FIN2c
		   actualeventCosts = ctrldao.getEventCostDetails();// KV: SW-FIN2b
		   eventLibraryTypes = ctrldao.getEventLibraryType(); // Ankit: PCAL-20282 
		}

		else{

		   assocdao= eventassocB.getProtSelectedEvents(EJBUtil.stringToNum(protocolId),orderType,orderBy);

		   eventIds=assocdao.getEvent_ids();

		   names= assocdao.getNames();

		   descriptions= assocdao.getDescriptions();

		   event_types= assocdao.getEvent_types();

		   eventCosts= assocdao.getCosts();

		   notes = assocdao.getNotes();//KM
		   cptCodes = assocdao.getCptCodes();
		   catgNames = assocdao.getEventCategory();
		   //JM: 19Jun2008, #3468
		   offlineFlags = 	assocdao.getOfflineFlags();
		   facilityIds = assocdao.getFacility();// KV: SW-FIN2c
		   actualeventCosts = assocdao.getEventCostDetails();// KV: SW-FIN2b
		   eventLibraryTypes = assocdao.getEventLibraryType(); // Ankit: PCAL-20282 
		}





	   int len = eventIds.size() ;





	 if (pageRight > 0 )

		{

	 %>


<DIV class="BrowserTopn" id="div1">

	<%

	   if(mode.equals("N")) {

	%>

	<%

	   } else if(mode.equals("M")) {

	%>

	<%

	   }

	%>



<jsp:include page="protocoltabs.jsp" flush="true"/>





</div>
<DIV class="BrowserBotN BrowserBotN_CL_1" id="div2" style="overflow:auto; height:80%">





	  <!--	JM: 12April2008 Modified for #C8.

	  <Form name="eventlibrary" method="post" action="visitlist.jsp?srcmenu=<%=src%>&selectedTab=3&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&displayType=D&displayDur=3&pageNo=1" onsubmit="">

	  -->

<!-- Fix #7061,BK,SEP/21/2011 -->

<!-- Fix Bug#7326 : Raviesh,02-Nov-2011 -->
	 <Form name="eventlibrary" method="post" action="manageVisits.jsp?protocolId=<%=protocolId%>&srcmenu=<%=src%>&selectedTab=3&mode=<%=mode%>&calledFrom=<%=calledFrom%>&calStatus=<%=calStatus%>&displayType=<%=displayType%>&pageNo=1&duration=<%=duration%>&displayDur=<%=displayDur%>&calassoc=<%=calAssoc%>" onsubmit="setOrder(document.eventlibrary,'')">

	<input type="hidden" name="orderType" value="<%=orderType%>">
	<input type="hidden" name="orderBy" value="<%=orderBy%>">  <!--KM-->
	<input type="hidden" name="srcmenu" value="<%=src%>">
	<input type="hidden" name="resetSort" value="<%=resetSort%>">  <!-- YK 04Jan- SUT_1_Requirement_18489 -->
	<div class="tmpHeight"></div>
	<table width="99%" cellspacing="0" cellpadding="0" class="basetbl midAlign outline">

	      <tr height="18">

	       <td width="75%">

		  <%


			
		int libTypeId = 0;
		String libTypePullDn = "";
		String cateId ="";
		SchCodeDao schDao = new SchCodeDao();
		schDao.getCodeValues("lib_type");

		libTypeId = schDao.getCodeId("lib_type","default");


		libTypePullDn = schDao.toPullDown("cmbLibType",libTypeId," onChange=callAjaxGetCatDD(document.eventlibrary);");

			
			
			
			
			
		ctrldao= eventdefB.getHeaderList(accId,"L",libTypeId);



		ArrayList catIds = ctrldao.getEvent_ids();

		ArrayList catNames = ctrldao.getNames();



		int length = catIds.size();

//


//		SchCodeDao schDao1 = new SchCodeDao();
//		schDao1.getValuesForCategory("L",String.valueOf(libTypeId),(String) (tSession.getValue("accountId")));//KM
//		cateId = schDao1.toPullDown("categoryId",0,"");


//






	%>


		<%=MC.M_EvtFromLib_IncludeCal%><%-- Select Events from your Library to include in this Calendar*****--%>		

		<%=libTypePullDn%>

	<%--	<span id = "span_catId"> <%=cateId%> </span>  --%>

		<span id = "span_catId">
		<select  name="categoryId">
		<option value="All"><%=LC.L_All%><%--All*****--%></option>
	<%

		for(int i = 0; i< length;i++)

		{

	%>

		<option value="<%=catIds.get(i)%>%<%=catNames.get(i)%>"> <%=catNames.get(i)%> </option>

	<%

		}

	%>

		</select>
			</span>


	     <% String link="selecteventus.jsp?mode="+mode+"&srcmenu="+src+"&duration="+duration+"&protocolId="+protocolId+"&calledFrom="+calledFrom+"&calStatus="+calStatus+"&cost="+prevCost+"&calltime=New"+"&calassoc="+calAssoc ;%>
	</td>
	<td>
		<button onclick="openwindow(document.eventlibrary, '<%=link%>')" type="button"><%=LC.L_Search%></button>


	<!--
		<A href="category.jsp?mode=<%=mode%>&catMode=N&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&fromPage=lib" onclick="return f_check_perm(<%=pageRight%>,'N')">Add a New Category</A>&nbsp;&nbsp;&nbsp;&nbsp;

	   <A href= "eventdetails.jsp?srcmenu=<%=src%>&eventmode=N&categoryId=<%=eventId%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&fromPage=eventbrowser&selectedTab=1" onclick="return f_check_perm(<%=pageRight%>,'N')" >Add a New Event</A>
	-->

	  </td></tr>

	  </Table>



	    <input type="hidden" name="srcmenu" value="<%=src%>">

	    <Input name="duration" type=hidden value=<%=duration%>>

	    <Input name="protocolId" type=hidden value=<%=protocolId%>>

	    <Input name="calledFrom" type=hidden value=<%=calledFrom%>>

  	    <Input name="calassoc" type=hidden value=<%=calAssoc%>>

	    <Input name="mode" type=hidden value=<%=mode%>>

		<Input name="calStatus" type=hidden value=<%=calStatus%>>
		
		<Input name="calStatusDesc" type=hidden value="<%=calStatDesc%>">


		<!-- KV:BugNo:4980-->
	    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="searchBg">
	    <tr height="6"><td></td></tr>
		  <tr >
			<td colspan="8">
		<table width="99%" border="0" cellspacing="0" cellpadding="0" class="midAlign">
			<%--YK 04Jan- SUT_1_Requirement_18489 --%>
			<tr class="lhsFont">
				<td width="10%" align="right"><A href="javascript:void(0);" onclick="sequenceEventsDialog(<%=pageRight%>)" name="sequenceEvents" id="sequenceEvents"><%=LC.L_Sequence_Events%><%-- Sequence Events*****--%></A></td>
		<!--Calendar Enhancement Cal-35776_No1:- For Remove the link "Reset Sort"-->
				<td width="8%" style="display:none"><A href="javascript:void(0);" onClick="setOrder(document.eventlibrary,'')" name="resetSortLink"><%=LC.L_Reset_Sort%><%-- Reset Sort*****--%></A></td>
		<!--End Enhancement-->
				<td  align="right"><a href="#" onClick="removeEvents(document.eventlibrary,<%=pageRight%>)"><%=LC.L_Rem_Evts%><%-- Remove Selected Event(s)*****--%></a> </td>
			</tr>

		</table>
		</td>
	 </tr>
	 <tr>
	      <table border="0" cellspacing="0" cellpadding="0" width="99%" class="outline midAlign">
	      <tr>
	      <!--Calendar Enhancemet Cal-35776_No1:- Add Column Sequence-->
	       <th width="10%" align="center" onClick="setOrder(document.eventlibrary,'(Cost)')"> <%=LC.L_Sequence%><%-- Event Library Type*****--%> &loz; </th>
	      <!--End Enhancemet-->
	       <th width="20%"  onClick="setOrder(document.eventlibrary,'lower(EVENT_LIBRARY_TYPE)')"> <%=LC.L_Evt_Lib%><%-- Event Library Type*****--%> &loz; </th>
			
	       <th width="20%"  onClick="setOrder(document.eventlibrary,'lower(EVENT_CATEGORY)')"> <%=LC.L_Evt_Cat%><%-- Event Category*****--%> &loz; </th>   <!--KM-->

	       <th width="18%"  onClick="setOrder(document.eventlibrary,'lower(NAME)')"> <%=LC.L_Event%><%-- Event*****--%> &loz; </th>

	       <th width="10%"  onClick="setOrder(document.eventlibrary,'lower(EVENT_CPTCODE)')"> <%=LC.L_Cpt_Upper%><%-- CPT*****--%> &loz; </th>

	       <th width="30%"  onClick="setOrder(document.eventlibrary,'lower(DESCRIPTION)')"> <%=LC.L_Description%><%-- Description*****--%> &loz; </th>

	       <th width="20%"  onClick="setOrder(document.eventlibrary,'lower(NOTES)')"> <%=LC.L_Notes%><%-- Notes*****--%> &loz;</th>
	       
	       <th width="20%" onClick="setOrder(document.eventlibrary,'lower(eventcost)')"> <%=LC.L_Cost%><%-- Cost*****--%> &loz;</th>
	
	       <th width="20%" onClick="setOrder(document.eventlibrary,'lower(Facilityname)')"> <%=LC.L_Facility%><%-- Facility*****--%> &loz;</th>
	
		   <th width="4%" > <%=LC.L_Select%><%-- Select*****--%> </th>
	      

	       <!-- th width="10%"> Delete </th-->
	     </tr>





	 <%

	    for(counter = 0;counter<len;counter++)

		{
	    	String overlibParameter="";
	    	 eventOverlibParameter="";
	    	 cptOverlibParameter="";
	    	 descrpOverlibParameter="";
	    	 noteOverlibParameter="";


		id = (Integer)eventIds.get(counter);

		eventId = id.toString();



	eventType=((event_types.get(counter)) == null)?"-":(event_types.get(counter)).toString();



	eventName=((names.get(counter)) == null)?"-":(names.get(counter)).toString();
   //if(eventName.length()>125 && eventName.length()<500) eventOverlibParameter=",WRAP, BORDER,2, CENTER,ABOVE,OFFSETY,25,STATUS,'Draggable with overflow scrollbar, caption and Close link',";
   if(eventName.length()>500 && eventName.length()<=1000) eventOverlibParameter=",WRAP, BORDER,2, CENTER,ABOVE,OFFSETY,50,STATUS,'Draggable with overflow scrollbar, caption and Close link',";
   else if(eventName.length()>1000 && eventName.length()<=2000)eventOverlibParameter=",WRAP, BORDER,2, CENTER,ABOVE,OFFSETY,125,STATUS,'Draggable with overflow scrollbar, caption and Close link',";
   else if(eventName.length()>2000)eventOverlibParameter=",WRAP, BORDER,2, CENTER,ABOVE,OFFSETY,175,STATUS,'Draggable with overflow scrollbar, caption and Close link',";
   else eventOverlibParameter=",ABOVE,";
		eventNameHide="event_name"+eventId;



	description=((descriptions.get(counter)) == null)?"-":(descriptions.get(counter)).toString();
	//if(description.length()>125 && description.length()<500) descrpOverlibParameter=",WRAP, BORDER,2, CENTER,ABOVE,OFFSETY,25,STATUS,'Draggable with overflow scrollbar, caption and Close link',";
	    if(description.length()>500 && description.length()<=1000) descrpOverlibParameter=",WRAP, BORDER,2, CENTER,ABOVE,OFFSETY,50,STATUS,'Draggable with overflow scrollbar, caption and Close link',";
	    else if(description.length()>1000 && description.length()<=2000)descrpOverlibParameter=",WRAP, BORDER,2, CENTER,ABOVE,OFFSETY,125,STATUS,'Draggable with overflow scrollbar, caption and Close link',";
	   else if(description.length()>2000) descrpOverlibParameter=",WRAP, BORDER,2, CENTER,ABOVE,OFFSETY,175,STATUS,'Draggable with overflow scrollbar, caption and Close link',";
	   else descrpOverlibParameter=",ABOVE,";
	//Calendar Enhancemet:- Cal-35776_No1
	eventCost=((eventCosts.get(counter)) != null)?(eventCosts.get(counter)).toString():"-";
	//End Enhancement

	note =((notes.get(counter)) == null)?"-":(notes.get(counter)).toString();//KM
	//if(note.length()>125 && note.length()<500) noteOverlibParameter=",WRAP, BORDER,2, CENTER,ABOVE,OFFSETY,25,STATUS,'Draggable with overflow scrollbar, caption and Close link',";
	   if(note.length()>500 && note.length()<=1000) noteOverlibParameter=",WRAP, BORDER,2, CENTER,ABOVE,OFFSETY,50,STATUS,'Draggable with overflow scrollbar, caption and Close link',";
	   else if(note.length()>1000 && note.length()<=2000)noteOverlibParameter=",WRAP, BORDER,2, CENTER,ABOVE,OFFSETY,125,STATUS,'Draggable with overflow scrollbar, caption and Close link',";
	   else if(note.length()>2000) noteOverlibParameter=",WRAP, BORDER,2, CENTER,ABOVE,OFFSETY,175,STATUS,'Draggable with overflow scrollbar, caption and Close link',";
	   else noteOverlibParameter=",ABOVE,";
	cptCode =((cptCodes.get(counter)) == null)?"-":(cptCodes.get(counter)).toString();
	//if(cptCode.length()>125 && cptCode.length()<500) cptOverlibParameter=",WRAP, BORDER,2, CENTER,ABOVE,OFFSETY,25,STATUS,'Draggable with overflow scrollbar, caption and Close link',";
	    if(cptCode.length()>500 && cptCode.length()<=1000) cptOverlibParameter=",WRAP, BORDER,2, CENTER,ABOVE,OFFSETY,25,STATUS,'Draggable with overflow scrollbar, caption and Close link',";
	    else if(cptCode.length()>1000 && cptCode.length()<=2000)cptOverlibParameter=",WRAP, BORDER,2, CENTER,ABOVE,OFFSETY,125,STATUS,'Draggable with overflow scrollbar, caption and Close link',";
	   else if(cptCode.length()>2000) cptOverlibParameter=",WRAP, BORDER,2, CENTER,ABOVE,OFFSETY,175,STATUS,'Draggable with overflow scrollbar, caption and Close link',";
	   else cptOverlibParameter=",ABOVE,";
	catgName = ((catgNames.get(counter)) == null)?"-":(catgNames.get(counter)).toString();
	
	facilityName =((facilityIds.get(counter)) == null)?"-":(facilityIds.get(counter)).toString();//KV:SW-FIN2c
	actualeventCost =((actualeventCosts.get(counter)) == null)?"-":(actualeventCosts.get(counter)).toString();//KV:SW-FIN2b
	eventLibraryType = ((eventLibraryTypes.get(counter)) == null)?"-":(eventLibraryTypes.get(counter)).toString(); //Ankit : PCAL-20282
	
	if (calledFrom.equals("S") ){
		offlineFlag = ((offlineFlags.get(counter)) == null)?"0":(offlineFlags.get(counter)).toString();
	}else{
		offlineFlag = "0";

	}

	if ((EJBUtil.stringToNum(eventCost)) > (EJBUtil.stringToNum(prevCost))){

		prevCost = eventCost;

	}





		if ((counter%2)==0) {

		%>

	      <tr class="browserEvenRow">

	  <%

			}

		else {

	  %>

	      <tr class="browserOddRow">

	   <%

			}



	   %>

	<%-- SV, 9/15/04, cal-enh-04, made event name a link --%>
	<%-- Ankit : PCAL-20282 --%>
	

	<!--Calendar Enhancement Cal-35776_No1-->
	<td><%=eventCost%></td>
	<!--End Enhancement-->
	<td> <%=eventLibraryType%> </td>

	<td> <%=catgName%> </td>


	<%

	//String param = "srcmenu=" + src + "&eventmode=M&eventId=" + eventId + "&duration=" + duration + "&protocolId=" + protocolId + "&calledFrom=" + calledFrom + "&mode=" + mode + "&calStatus=" + calStatus + "&fromPage=eventbrowser&selectedTab=1&calassoc="+calAssoc;
	%>
					<!-- td><A href =# onClick='paramHide("eventdetails.jsp","<!--%=param%>")'><!--%=eventName%></A> </td-->
					
		<td ><%if(eventName.length()>50){ %><%=eventName.substring(0,50) %><span onmouseover="return overlib(htmlEncode('<%=eventName%>')<%=eventOverlibParameter %>CAPTION,'<%=LC.L_Event_Name%>');" onmouseout="nd();">...</span><%}else{ %><%=eventName%><%} %></td>

		<INPUT name=<%=eventNameHide%> type=hidden value="<%=eventName%>">

		<INPUT name="eventId" type=hidden value="<%=eventId%>">

				 <%
	 overlibParameter="";
		     if(cptCode.length()>125){
		    	 
		    	 overlibParameter="WRAP, 0, BORDER,2, BELOW, CLOSECLICK,50,0, RELY,70,STATUS,'Draggable with overflow scrollbar, caption and Close link'";
		     }
		     %>  

		<td><%if(cptCode.length()>50){ %><%=cptCode.substring(0,50) %><span onmouseover="return overlib(htmlEncode('<%=cptCode%>')<%=cptOverlibParameter %>CAPTION,'<%=LC.L_Cpt_Code %>');" onmouseout="return nd();">...</span><%}else{ %><%=cptCode%><%} %> </td>
	 <%
	 overlibParameter="";
		     if(description.length()>125){
		    	 
		    	 overlibParameter="WRAP, 0, BORDER,2, BELOW, CLOSECLICK,50,0, RELY,70,STATUS,'Draggable with overflow scrollbar, caption and Close link'";
		     }
		     %>  
		
       <td><%if(description.length()>50){ %><%=description.substring(0,50) %><span onmouseover="return overlib(htmlEncode('<%=description%>')<%=descrpOverlibParameter %>CAPTION,'<%=LC.L_Description %>');" onmouseout="return nd();">...</span><%}else{ %><%=description%><%} %> </td>
	  <!--Modify notes for the bug id:- 24002-->
	  <%
	 overlibParameter="";
		     if(note.length()>125){
		    	 
		    	 overlibParameter="WRAP, 0, BORDER,2, BELOW, CLOSECLICK,50,0, RELY,70,STATUS,'Draggable with overflow scrollbar, caption and Close link'";
		     }
		     %> 
	  <td> <%if(note.length()>50){%><%=note.substring(0,50)%><span onmouseover="return overlib(htmlEncode('<%=note%>')<%=noteOverlibParameter %>CAPTION,'<%=LC.L_Notes%>');" onmouseout="return nd();">...</span><%}else{%><span onmouseover="return overlib(htmlEncode('<%=note%>'),CAPTION,'<%=LC.L_Notes%>');" onmouseout="return nd();"><%=note%></span><%}%></td>
	  <td> <%=actualeventCost%> </td>
	  <td> <%=facilityName%> </td>

	<td align="center">

	<% if (calledFrom.equals("P") || calledFrom.equals("L")) {%>


	<!--  A href="deleteeventfromprot.jsp?srcmenu=<%=src%>&eventId=<%=eventId%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&fromPage=eventbrowser&from=initial&catId=<%=catId%>&tableName=event_def&calassoc=<%=calAssoc%>" onclick="return confirmBox('<%=eventName%>', <%=pageRight%>, '<%=calStatus%>')"><img src="./images/delete.gif" border="0" align="left"/></A-->

	<input type ="checkbox" name="eventCheck" value=<%=eventId%> >
	<Input type="hidden" name="offlnFlag" value="<%=offlineFlag%>"><!--//JM: 19June2008, added for #3468-->

	<%} else {

	%>

	<!--  A href="deleteeventfromprot.jsp?srcmenu=<%=src%>&eventId=<%=eventId%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&fromPage=eventbrowser&from=initial&catId=<%=catId%>&tableName=event_assoc&calassoc=<%=calAssoc%>" onclick="return confirmBox('<%=eventName%>',<%=pageRight%>, '<%=calStatus%>')"><img src="./images/delete.gif" border="0" align="left"/></A-->
	<input type ="checkbox" name="eventCheck" value=<%=eventId%> >

	<Input type="hidden" name="offlnFlag" value="<%=offlineFlag%>"><!--//JM: 19June2008, added for #3468-->


	<%}

	%>

	</td>
	

	      </tr>

	 <%

			}

	%>

	    </table>
	</tr>
</table>
	  <br>

	  <br>

	  <Input name="cost" type=hidden value=<%=prevCost%>>
	  <input name="len" type =hidden value=<%=len%>>


</Form>

<div id="seq_events" title="<%=LC.L_Sequence_Events%>" style="display:none">
<div id='embedDataHere'><b><p class="defComments" align="center"><%=MC.M_DragEvt_ChgSeq%></p></b>
<table id="seq_table" border="1" width="100%" cellspacing="0" cellpadding="0">
	<input type="hidden" id="prtcolId" name="protocolId" value="<%=protocolId%>"></input>
	<input name="calStatus" id="calStus" type=hidden value=<%=calStatus%>>
	<input name="calStatusDesc" id="calstusDsc" type=hidden value="<%=calStatDesc%>">
    <thead>
      <tr height="35" style="background:#D8D8DA" class="noEvent">
        <th width="20%" onclick="sorting(document.eventlibrary,'(Cost)')"><%=LC.L_Sequence%> &loz;</th>
        <th width="40%" onclick="sorting(document.eventlibrary,'lower(EVENT_CATEGORY)')"><%=LC.L_Evt_Cat%> &loz;</th>
        <th width="40%" onclick="sorting(document.eventlibrary,'lower(NAME)')"><%=LC.L_Event_Name%> &loz;</th>
      </tr>
    </thead>
    <tbody onmouseover="this.style.cursor='move';">
    <%
    String orderby="Cost",ordertype="asc";
	 if (calledFrom.equals("P") || calledFrom.equals("L")) {

		   ctrldao= eventdefB.getProtSelectedEvents(EJBUtil.stringToNum(protocolId),ordertype,orderby);
		   eventIds=ctrldao.getEvent_ids();
		   names= ctrldao.getNames();	   
		   eventCosts= ctrldao.getCosts();
		   catgNames = ctrldao.getEventCategory();
		}
		else{
		   assocdao= eventassocB.getProtSelectedEvents(EJBUtil.stringToNum(protocolId),ordertype,orderby);
		   eventIds=assocdao.getEvent_ids();
		   names= assocdao.getNames();
		   eventCosts= assocdao.getCosts();
		   catgNames = assocdao.getEventCategory();
		}
     
    for(counter = 0;counter<len;counter++)
    {
    	id = (Integer)eventIds.get(counter);
		eventId = id.toString();
    	eventCost=((eventCosts.get(counter)) != null)?(eventCosts.get(counter)).toString():"-";
    	eventName=((names.get(counter)) == null)?"-":(names.get(counter)).toString();
    	catgName = ((catgNames.get(counter)) == null)?"-":(catgNames.get(counter)).toString();
    	
    	if ((counter%2)==0) {
		%>
			<tr class="browserEvenRow" height="25">
		 <%
		}
    	else {
		%>
    	    <tr class="browserOddRow" height="25">
    	 <%
    	}
    	%>
    	<td id="seq_id" style="display:none"><%=eventId%></td>
    	<td id="sequence" ><%=eventCost%></td>
    	<td id="cat"> <%=catgName%> </td>
    	<%
    	if(eventName.length()>=50){
    	String EvntName=eventName.substring(0,50);
    	eventOverlibParameter=",WRAP, BORDER,2, CENTER,BELOW,OFFSETY,10,STATUS,'Draggable with overflow scrollbar, caption and Close link',";
    	%>
    	<td id="evtname" ><span onmouseover="return overlib(htmlEncode('<%=eventName%>')<%=eventOverlibParameter %>CAPTION,'<%=LC.L_Event_Name%>');" onmouseout="nd();"><%=EvntName%>...</span></td>
    <%	}else{
    	%>
   	<td id="evtname" ><%=eventName%> </td>
	<%} %>
	</tr>
    	
 <%  }
   
    %>
    </tbody>
    
  </table>
</div>
	<div id='progressMsg' style="display:none;"><br><p class="sectionHeadings" align="center"><%=LC.L_Please_Wait%>... <img class="asIsImage_progbar"  src="../images/jpg/loading_pg.gif" /></p></div>
</div>



	 <%//SV, 9/15/04, cal-enh-04 %>
	<jsp:include page="paramHide.jsp" flush="true"/>


	 <%

		} //end of if body for page right

	else

	{

	%>
<DIV class="BrowserBotN" id = "div2">
	  <jsp:include page="accessdenied.jsp" flush="true"/>
</DIV>
	 <%

	 } //end of else body for page right
	}
}//end of if body for session

else

{

%>

  <jsp:include page="timeout.html" flush="true"/>

<%

}

%>



  <div>

    <jsp:include page="bottompanel.jsp" flush="true"/>

  </div>

</div>

<div class ="mainMenu" id = "emenu">

  <jsp:include page="getmenu.jsp" flush="true"/>

</div>

</body>

 



</html>

