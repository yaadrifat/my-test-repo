<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language = "java" import = "com.velos.eres.service.util.*,java.util.*"%>

<jsp:useBean id ="sessionmaint2" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="budgetcalB" scope="request"	class="com.velos.esch.web.budgetcal.BudgetcalJB" /> 
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<%@page import="com.velos.esch.business.common.SchCodeDao"%>
<%

   HttpSession tSession2 = request.getSession(true); 
   if (sessionmaint2.isValidSession(tSession2))
	{
	String bgtcalId = request.getParameter("bgtcalId");
	String studyId  = request.getParameter("studyId");
	String budProtId = "";
	String budProtType = "";
	String calendarStatus = "";
	String calendarStudy = "";
	String textDisp ="";
	
	//JM: 22Feb2011: #5861
	String calStatDesc_A = "";
	
	if (! bgtcalId.equals("0"))
	{
			
			budgetcalB.setBudgetcalId(EJBUtil.stringToNum(bgtcalId));
			budgetcalB.getBudgetcalDetails();
		 	budProtId = budgetcalB.getBudgetProtId();
		 	budProtType = budgetcalB.getBudgetProtType();
		 	if (budProtType.equals("L"))
		 	{
		 			calendarStatus = "";
					calendarStudy = "";
		 	}
		  	else 
		  	 {
						eventassocB.setEvent_id(EJBUtil.stringToNum(budProtId));
					 	eventassocB.getEventAssocDetails();
					 	
						calendarStudy = eventassocB.getChain_id();
						if (StringUtil.isEmpty(calendarStudy ))
					 	{
					 		calendarStudy  = "";
					 	}
						
						//calendarStatus = eventassocB.getStatus();
						//KM-#DFin9
						String calStatusPk = eventassocB.getStatCode();
						SchCodeDao scho = new SchCodeDao();
						calendarStatus = scho.getCodeSubtype(EJBUtil.stringToNum(calStatusPk)).trim();
						
						//JM: 22Feb2011: #5861
						calStatDesc_A = scho.getCodeDescription(scho.getCodeId("calStatStd","A"));	
												

					 	if (StringUtil.isEmpty(calendarStatus))
					 	{
					 		calendarStatus = "";
					 	}
		  	 }
		  	 
		   //KM-#4518-Additional message changes
		   if (! calendarStudy.equals(studyId) )
		   {
				String lblStd = LC.L_Study;
				Object[] arguments = {lblStd};
				textDisp = VelosResourceBundle.getMessageString("M_SelCalBgt_DiffCalMstone",arguments);
		   }
		   //V-FIN27 Commented as now milestones can be created for WIP calendars as well
		   /*else if (! calendarStatus.equals("A"))
		   {
				textDisp = "This Calendar is not "+calStatDesc_A+". You can only create 'Additional Milestones'.";
		   }*/
		   else
			   textDisp ="";
	  	 
 }
 if(!textDisp.equals("")) {
%>
<div class="validation-fail"> <%=textDisp%> </div>
<%} } %>