<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@page import="com.velos.eres.web.site.SiteJB,com.velos.eres.web.user.UserJB,com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.VelosResourceBundle"%>
<%@page import="com.velos.eres.business.common.CodeDao,java.util.*"%>
<%@page import="com.velos.login.jwt.*"%>
<html style="overflow-y: auto;">
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_StdGtSumm%><%--make ct study--%></title>
<style>
span.errorMessage { color:red; height:1.5em; text-align:right; }

</style>
<script type="text/javascript">
moreDetailsConfigArray=[];
moreDetailsConfigMap={};
orgMoreDetailsConfigArray=[];
codelstConfigMap={};
</script>
<%
String src = request.getParameter("srcmenu");
String selectedTab = request.getParameter("selectedTab");
String mode = request.getParameter("mode");
String includeMode = request.getParameter("includeMode");
List<String> allKeys = VelosResourceBundle.getKeys(VelosResourceBundle.CONFIG_BUNDLE,"MSD.");
List<String> allValues = new ArrayList<String>();
List<String> allCodeKeys = new ArrayList<String>();
List<String> allCodeValues = new ArrayList<String>();
System.out.println(allKeys);
Map<String,String> allconfigMap = new HashMap<String,String>();
String key="";
String value="";
String codeKey="";
Iterator selCodeItr= null;
Iterator setItr = allKeys.iterator();
int i=0;
while(setItr.hasNext()){
	key=(String)setItr.next();
	allconfigMap.put(key,VelosResourceBundle.getString(VelosResourceBundle.CONFIG_BUNDLE,key));
	if(key.indexOf("[VELSEP1]")<0){
		allCodeKeys.clear();
		allValues.add(key.substring(4,key.length()));
		allCodeKeys = VelosResourceBundle.getKeys(VelosResourceBundle.CONFIG_BUNDLE,key.substring(4,key.length()));
		System.out.println(allCodeKeys);
		selCodeItr=allCodeKeys.iterator();
		while(selCodeItr.hasNext()){
			codeKey=(String)selCodeItr.next();
			System.out.println(codeKey);
		%>
		<script type="text/javascript">
		codelstConfigMap['<%=codeKey%>']='<%=VelosResourceBundle.getString(VelosResourceBundle.CONFIG_BUNDLE,codeKey)%>';
		</script>
		<%}%>
		<script type="text/javascript">
		moreDetailsConfigArray['<%=i%>']='MSD.'+'<%=VelosResourceBundle.getString(VelosResourceBundle.CONFIG_BUNDLE,key) %>';
		orgMoreDetailsConfigArray['<%=i%>']='<%=key%>';
		moreDetailsConfigMap['MSD.'+'<%=VelosResourceBundle.getString(VelosResourceBundle.CONFIG_BUNDLE,key) %>']='<%=key.substring(4,key.length()) %>';
		</script>
	<%}else{
		allValues.add(key.substring(4,key.length()).substring(0,key.substring(4,key.length()).indexOf("[VELSEP1]")));
		String[] sepConfigKeys = StringUtil.strSplit(key.substring(4,key.length()),"[VELSEP1]");
		System.out.println(key.substring(4,key.length()));
		for(int k=0;k<sepConfigKeys.length;k++){
			allCodeKeys.clear();
			allCodeKeys = VelosResourceBundle.getKeys(VelosResourceBundle.CONFIG_BUNDLE,sepConfigKeys[k]);
			System.out.println(allCodeKeys);
			selCodeItr=allCodeKeys.iterator();
			while(selCodeItr.hasNext()){
				codeKey=(String)selCodeItr.next();
				System.out.println(codeKey);
		%>
		 <script type="text/javascript">
		 codelstConfigMap['<%=codeKey%>']='<%=VelosResourceBundle.getString(VelosResourceBundle.CONFIG_BUNDLE,codeKey)%>';
		 </script>
			<%}}%>
		<script type="text/javascript">
		 moreDetailsConfigArray['<%=i%>']='MSD.'+'<%=VelosResourceBundle.getString(VelosResourceBundle.CONFIG_BUNDLE,key) %>';
		 orgMoreDetailsConfigArray['<%=i%>']='<%=key%>';
		 moreDetailsConfigMap['MSD.'+'<%=VelosResourceBundle.getString(VelosResourceBundle.CONFIG_BUNDLE,key) %>']='<%=key.substring(4,key.length()) %>';
		 </script>
		<%}
	i++;
	}
System.out.println(allValues);
System.out.println(allconfigMap);
int pageRight = StringUtil.stringToNum(request.getParameter("keyRt"));
%>
<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC,com.velos.eres.service.util.CFG,com.velos.eres.business.studyNIHGrant.impl.StudyNIHGrantBean" %>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%
HttpSession tSession = request.getSession(true);
if (!sessionmaint.isValidSession(tSession))
	return;

String readonly = "readonly=\"readonly\"";
String accountId = (String) tSession.getValue("accountId");
String ipAdd = (String) tSession.getValue("ipAdd");
String studyId= (tSession.getValue("studyId")==null)?"0":(String)tSession.getValue("studyId");
String studyNo= (tSession.getValue("studyNo")==null)?"":(String)tSession.getValue("studyNo");
 String usr = null;
 usr = (String) tSession.getValue("userId");
 UserJB userB = new UserJB();
 userB.setUserId(StringUtil.stringToNum(usr));
 userB.getUserDetails();
 int userId = StringUtil.stringToNum(usr);
 int siteId = StringUtil.stringToNum(userB.getUserSiteId());
 int groupId = StringUtil.stringToNum(userB.getUserGrpDefault());
 String uFName = userB.getUserFirstName();
 String uLName = userB.getUserLastName();
 //tSession.setAttribute("jwttoken", AuthHelper.createJsonWebToken("plm_test_user", "ctms3633", Long.parseLong("2")) );
%>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<script src="js/jquery/jquery-1.4.4.js"></script>
<script  type="text/javascript" src="js/jquery/json2.min.js"></script>
<script src="./js/velosCustom/isquare.js"></script>
<SCRIPT type="text/javascript">
jwttoken = '<%=tSession.getAttribute("jwttoken")%>';	
	var studySummaryData= function(){
		//alert('Hello Submit Data');
		//alert(JSON.parse(jQuery("#createJsonData").val()));
		var action="";
		var nctNumber = jQuery("#nctNumber").val();
		var mode = jQuery('#mode').val();
		var urlData='';
		if(mode==='M')
			action="U";
		else
			action="I";
		$j.ajax({
			url:'ctAudit.jsp',
			type: "POST",
			async:false,
			data:"tableName=ct.gov&action="+action,
			success:(function (data){
				var response=$j(data);
				var message = response.filter('#resultMsg').val();				
				
			}),
			error:function(response) { alert(data); return false; }
		});	
					
		if(mode==='M'){
		if(jQuery("#createJsonData").val()===''){
			alert("No data to Update.");
			return false;
		}
		var cnf = confirm("Are you sure you want to update the study?");
		if (cnf == true) {
			$j('#dialog-message').dialog({ modal: true, width:400 });
			urlData = '<%=CFG.CT_URL%>'+'/api/v1/updateStudySummary';
			jQuery.ajax({
				type: "POST",
					 async:false,
					url: urlData,
					data: {args : JSON.parse(jQuery("#createJsonData").val()),
                			"jwtToken": jwttoken},
					dataType: 'json',
					success: function(data){
					var URL ='';
					<% if("LIND".equals(CFG.EIRB_MODE) || "Y".equals(CFG. FLEX_MODE)){%>
						URL = "flexStudyScreenLindFt?srcmenu=tdMenuBarItem3&selectedTab=1&mode=M&includeMode=N&nctNumber="+nctNumber+"&studyId=<%=studyId%>";
					<%}else{%>
						URL = "studyScreen.jsp?srcmenu=tdMenuBarItem3&selectedTab=1&mode=M&includeMode=N&nctNumber="+nctNumber+"&studyId=<%=studyId%>";
					<%}%>
					if(data.body){
						var errorType = JSON.stringify(data.body);
	         			var firstIndex = errorType.indexOf('<message>');
	         			var lastIndex = errorType.indexOf('</message>');
	         			if(firstIndex>0)
	         				errorType = errorType.slice(firstIndex+9,lastIndex)
	         			alert(errorType);
	         			return false;
						}else{
						window.opener.location.href = URL;
						setTimeout("self.close()",1000);
						}
	   				 },
					failure: function(errMsg) {
	    				alert(errMsg);
						}
			   });
			}
		}else
		{
		  $j('#dialog-message').dialog({ modal: true, width:400 });
		  urlData = '<%=CFG.CT_URL%>'+'/api/v1/createStudy';
		  jQuery.ajax({
			type: "POST",
				 async:false,
				url: urlData,
				data: {args : JSON.parse(jQuery("#createJsonData").val()),
              			"jwtToken": jwttoken},
				dataType: 'json',
				success: function(data){
				var URL ='';
				<% if("LIND".equals(CFG.EIRB_MODE) || "Y".equals(CFG. FLEX_MODE)){%>
					URL = "flexStudyScreenLindFt?srcmenu=tdMenuBarItem3&selectedTab=1&mode=M&includeMode=N&nctNumber="+nctNumber+"&studyId="+data.Response.results.result[1].objectId.PK;
				<%}else{%>
					URL = "studyScreen.jsp?srcmenu=tdMenuBarItem3&selectedTab=1&mode=M&includeMode=N&nctNumber="+nctNumber+"&studyId="+data.Response.results.result[1].objectId.PK;
				<%}%>
					window.opener.location.href = URL;
					setTimeout("self.close()",1000);
   				 },
				failure: function(errMsg) {
    				alert(errMsg);
					}
		   });
		}
		$j('#dialog-message').dialog("destroy");
			}

	 var StudyJsonType = function(flag){
		 var nctNumber = jQuery("#nctNumber").val();
		 var studyDetails = {};
		 if(nctNumber==null || nctNumber==""){
			 if(flag)
			 alert("Please provide NCT Number.");
			 return;
		 }else{
		 $j('#dialog-message').dialog({ modal: true, width:400 });
		 var mode = jQuery('#mode').val();
		 var studyNum='<%=studyNo%>';
	 if(mode==='M'){
		 jQuery.ajax({
			    type: "POST",
			    async:false,
			    crossDomain: true,
			    url: '<%=CFG.CT_URL%>'+'/api/v1/getStudy',
			    data: { args:{
             			StudyIdentifier: {
             				studyNumber: studyNum
         				}},
                        "jwtToken": jwttoken },
         		dataType: 'json',
			    success: function(data){
					if(data.body){
					var errorType = JSON.stringify(data.body);
         			var firstIndex = errorType.indexOf('<message>');
         			var lastIndex = errorType.indexOf('</message>');
         			if(firstIndex>0)
         				errorType = errorType.slice(firstIndex+9,lastIndex)
         			alert(errorType);
         			return false;
					}else{
						studyDetails = data;
					}
				    },
			    failure: function(errMsg) {
			        alert(errMsg);
			    }
					});
	 }
		 jQuery.ajax({
			    type: "GET",
			    async:false,
			    crossDomain: true,
			    url: '<%=CFG.CT_URL%>'+'/api/v1/getStudySummary',
			    data: { nctNumber: nctNumber.toUpperCase() },
			    contentType: "application/json; charset=utf-8",
			    dataType: "json",
			    success: function(data){
				    if(data.clinical_study==='not_found'){
				    	$j('#dialog-message').dialog("destroy");
				    	alert("No data found with NCT Number: "+nctNumber);
					    return false;
				    }
			        var orgJson = [];
			        var jsonObj = {};
			        var jsonArray = [];
			        var splitStr = [];
			        var checkTeamName = [];
			        var piJson = {};
			        var count = 0;
			        var codeJson={};
			        var phaseJson={};
			        var studyTypeJson={};
			        var sponsorNameJson={};
			        var statJson = {};
			        var studyTeamJson = {};
			        var moreStudyDetails=[];
			        var orgMoreStudyDetails=[];
			        var moreDetail={};
			        var statDate = "";
			        var diseaseSite="";
			        var months = {January:'01',February:'02',March:'03',April:'04',May:'05',June:'06',July:'07',
					        August:'08',September:'09',October:'10',November:'11',December:'12'};
				    if(studyNum===''){
			        	studyNum=data.clinical_study.id_info.org_study_id;
				    }
			        if(studyNum)
			        jQuery("#studyNumber").val(studyNum);
			 		//alert(moreDetailsConfigArray);
			 		//alert(orgMoreDetailsConfigArray);
			 		for(var k=0;k<moreDetailsConfigArray.length;k++){
				 		var fieldValue = moreDetailsConfigArray[k];
				 		var fieldName = fieldValue;
				 		fieldName =fieldName.substring(4, fieldName.length);
				 		//alert(fieldName);
				 		//alert(data.clinical_study[fieldName]);
				 		moreDetail={};
				 		if(fieldName.indexOf(".")<0){
				 			if(data.clinical_study[fieldName]){
					 			//alert(moreDetailsConfigMap[fieldValue]);
					 			moreDetail["key"]=moreDetailsConfigMap[fieldValue];
					 			moreDetail["type"]="studyidtype";
					 			if( typeof data.clinical_study[fieldName] === 'object'){
					 				jQuery("#"+moreDetailsConfigMap[fieldValue]).val(replaceAllSpaceNewLine(moreDetailsConfigMap[fieldValue],data.clinical_study[fieldName].textblock));
					 				moreDetail["value"]={"attributes":{"xsi:type":"xs:string","xmlns:xs":"http://www.w3.org/2001/XMLSchema","xmlns:xsi":"http://www.w3.org/2001/XMLSchema-instance"},
					 						"$value": replaceAllSpaceNewLine(moreDetailsConfigMap[fieldValue],data.clinical_study[fieldName].textblock)}
					 			}else{
					 				jQuery("#"+moreDetailsConfigMap[fieldValue]).val(replaceAllSpaceNewLine(moreDetailsConfigMap[fieldValue],data.clinical_study[fieldName]));
					 				moreDetail["value"]={"attributes":{"xsi:type":"xs:string","xmlns:xs":"http://www.w3.org/2001/XMLSchema","xmlns:xsi":"http://www.w3.org/2001/XMLSchema-instance"},
					 								"$value": replaceAllSpaceNewLine(moreDetailsConfigMap[fieldValue],data.clinical_study[fieldName])}
					 			}
					 			moreStudyDetails.push(moreDetail);
				 				}
				 		}else{//alert(fieldName);
					 			var splitValArray = fieldName.split(".");
					 			fieldNames = "";
						 		//alert(fieldName);
					 			for(var j=0;j<splitValArray.length;j++){
						 			if(j==0){
						 				fieldNames=data.clinical_study[splitValArray[j]];
					 					//alert(fieldNames);
						 			}else{
						 		  if(fieldNames){
							 			if(fieldNames.length>0){
								 			var strfieldVal="";
											for(var l=0;l<fieldNames.length;l++){
												if(l==0)
												    strfieldVal=fieldNames[l][splitValArray[j]];
												else{
													if(strfieldVal.indexOf(fieldNames[l][splitValArray[j]])<0)
													  strfieldVal=strfieldVal+","+fieldNames[l][splitValArray[j]];
												}
											}
											if(data.clinical_study.study_type==='Interventional' && orgMoreDetailsConfigArray[k].substring(4,orgMoreDetailsConfigArray[k].length)==='INTVN_TYP_M'){
												moreDetail["key"]=orgMoreDetailsConfigArray[k].substring(4,orgMoreDetailsConfigArray[k].length);
								 				moreDetail["type"]="studyidtype";
								 				jQuery("#"+orgMoreDetailsConfigArray[k].substring(4,orgMoreDetailsConfigArray[k].length)).val(replaceAllSpaceNewLine(orgMoreDetailsConfigArray[k].substring(4,orgMoreDetailsConfigArray[k].length),strfieldVal));
							 					moreDetail["value"]={"attributes":{"xsi:type":"xs:string","xmlns:xs":"http://www.w3.org/2001/XMLSchema","xmlns:xsi":"http://www.w3.org/2001/XMLSchema-instance"},
							 								"$value": replaceAllSpaceNewLine(orgMoreDetailsConfigArray[k].substring(4,orgMoreDetailsConfigArray[k].length),strfieldVal)}
							 					moreStudyDetails.push(moreDetail);
											}
											if(data.clinical_study.study_type==!'Interventional' && orgMoreDetailsConfigArray[k].substring(4,orgMoreDetailsConfigArray[k].length)==='EXPSR_INT_M'){
												moreDetail["key"]=orgMoreDetailsConfigArray[k].substring(4,orgMoreDetailsConfigArray[k].length);
									 			moreDetail["type"]="studyidtype";
									 			jQuery("#"+orgMoreDetailsConfigArray[k].substring(4,orgMoreDetailsConfigArray[k].length)).val(replaceAllSpaceNewLine(orgMoreDetailsConfigArray[k].substring(4,orgMoreDetailsConfigArray[k].length),strfieldVal));
								 				moreDetail["value"]={"attributes":{"xsi:type":"xs:string","xmlns:xs":"http://www.w3.org/2001/XMLSchema","xmlns:xsi":"http://www.w3.org/2001/XMLSchema-instance"},
								 								"$value": replaceAllSpaceNewLine(orgMoreDetailsConfigArray[k].substring(4,orgMoreDetailsConfigArray[k].length),strfieldVal)}
								 				moreStudyDetails.push(moreDetail);
											}
											
							 			}else{
						 					fieldNames=fieldNames[splitValArray[j].replace(/[0-9]/g, '')];
						 					if(fieldNames){
						 					if( typeof fieldNames === 'object'){
						 						if(fieldNames.textblock){
													moreDetail["key"]=moreDetailsConfigMap[fieldValue];
										 			moreDetail["type"]="studyidtype";
										 			jQuery("#"+moreDetailsConfigMap[fieldValue]).val(replaceAllSpaceNewLine(moreDetailsConfigMap[fieldValue],fieldNames.textblock));
									 				moreDetail["value"]={"attributes":{"xsi:type":"xs:string","xmlns:xs":"http://www.w3.org/2001/XMLSchema","xmlns:xsi":"http://www.w3.org/2001/XMLSchema-instance"},
									 								"$value": replaceAllSpaceNewLine(moreDetailsConfigMap[fieldValue],fieldNames.textblock)}
									 				moreStudyDetails.push(moreDetail);
												}
						 						}else{
							 						if(moreDetailsConfigMap[fieldValue].indexOf("[VELSEP1]")>0){
								 						var moreDetailKeys = moreDetailsConfigMap[fieldValue].split("[VELSEP1]");
														var moreFieldNames = fieldNames.split(" ");
								 						for(var ele=0;ele<moreDetailKeys.length;ele++){
								 							moreDetail={};
									 						if(ele==0)
									 							jQuery("#"+moreDetailKeys[ele]).val(replaceAllSpaceNewLine(moreDetailKeys[ele],fieldNames));
								 							if(moreFieldNames[ele]){
								 							moreDetail["key"]=moreDetailKeys[ele];
												 			moreDetail["type"]="studyidtype";
											 				moreDetail["value"]={"attributes":{"xsi:type":"xs:string","xmlns:xs":"http://www.w3.org/2001/XMLSchema","xmlns:xsi":"http://www.w3.org/2001/XMLSchema-instance"},
											 								"$value": replaceAllSpaceNewLine(moreDetailKeys[ele],moreFieldNames[ele])}
											 				moreStudyDetails.push(moreDetail);
								 							}else{
								 								moreDetail["key"]=moreDetailKeys[ele];
													 			moreDetail["type"]="studyidtype";
												 				moreDetail["value"]={"attributes":{"xsi:type":"xs:string","xmlns:xs":"http://www.w3.org/2001/XMLSchema","xmlns:xsi":"http://www.w3.org/2001/XMLSchema-instance"},
												 								"$value": ""}
												 				moreStudyDetails.push(moreDetail);
								 							}
								 						}
								 						
							 						}else{ 
							 						    var agetrim = "";
							 							if(splitValArray[j].replace(/[0-9]/g, '')==='maximum_age'){
														if(fieldNames.match(/[0-9]+/g)>0){
															//alert(getValueFromKey(orgMoreDetailsConfigArray[k].substring(4,orgMoreDetailsConfigArray[k].length)+".yes"));
															moreDetail["key"]=orgMoreDetailsConfigArray[k].substring(4,orgMoreDetailsConfigArray[k].length);
											 				moreDetail["type"]="studyidtype";
											 				jQuery("#"+orgMoreDetailsConfigArray[k].substring(4,orgMoreDetailsConfigArray[k].length)).val(getValueFromKey(orgMoreDetailsConfigArray[k].substring(4,orgMoreDetailsConfigArray[k].length)+".yes"));
										 					moreDetail["value"]={"attributes":{"xsi:type":"xs:string","xmlns:xs":"http://www.w3.org/2001/XMLSchema","xmlns:xsi":"http://www.w3.org/2001/XMLSchema-instance"},
										 								"$value": "yes"}
										 					moreStudyDetails.push(moreDetail);
							 							}else{
							 								moreDetail["key"]=orgMoreDetailsConfigArray[k].substring(4,orgMoreDetailsConfigArray[k].length);
											 				moreDetail["type"]="studyidtype";
											 				jQuery("#"+orgMoreDetailsConfigArray[k].substring(4,orgMoreDetailsConfigArray[k].length)).val(getValueFromKey(orgMoreDetailsConfigArray[k].substring(4,orgMoreDetailsConfigArray[k].length)+".no"));
										 					moreDetail["value"]={"attributes":{"xsi:type":"xs:string","xmlns:xs":"http://www.w3.org/2001/XMLSchema","xmlns:xsi":"http://www.w3.org/2001/XMLSchema-instance"},
										 								"$value": "no"}
										 					moreStudyDetails.push(moreDetail);
							 							}
														break;
														}else if(splitValArray[j].replace(/[0-9]/g, '')==='minimum_age'){
														if(fieldNames.match(/[0-9]+/g)>0){
															moreDetail["key"]=orgMoreDetailsConfigArray[k].substring(4,orgMoreDetailsConfigArray[k].length);
											 				moreDetail["type"]="studyidtype";
											 				jQuery("#"+orgMoreDetailsConfigArray[k].substring(4,orgMoreDetailsConfigArray[k].length)).val(getValueFromKey(orgMoreDetailsConfigArray[k].substring(4,orgMoreDetailsConfigArray[k].length)+".yes"));
										 					moreDetail["value"]={"attributes":{"xsi:type":"xs:string","xmlns:xs":"http://www.w3.org/2001/XMLSchema","xmlns:xsi":"http://www.w3.org/2001/XMLSchema-instance"},
										 								"$value": "yes"}
										 					moreStudyDetails.push(moreDetail);
														}else{
															moreDetail["key"]=orgMoreDetailsConfigArray[k].substring(4,orgMoreDetailsConfigArray[k].length);
											 				moreDetail["type"]="studyidtype";
											 				jQuery("#"+orgMoreDetailsConfigArray[k].substring(4,orgMoreDetailsConfigArray[k].length)).val(getValueFromKey(orgMoreDetailsConfigArray[k].substring(4,orgMoreDetailsConfigArray[k].length)+".no"));
										 					moreDetail["value"]={"attributes":{"xsi:type":"xs:string","xmlns:xs":"http://www.w3.org/2001/XMLSchema","xmlns:xsi":"http://www.w3.org/2001/XMLSchema-instance"},
										 								"$value": "no"}
										 					moreStudyDetails.push(moreDetail);
														}
														break;
														}
							 						if(data.clinical_study.study_type==='Interventional' && orgMoreDetailsConfigArray[k].substring(4,orgMoreDetailsConfigArray[k].length)==='INTVN_TYP_M'){
															moreDetail["key"]=orgMoreDetailsConfigArray[k].substring(4,orgMoreDetailsConfigArray[k].length);
											 				moreDetail["type"]="studyidtype";
											 				jQuery("#"+orgMoreDetailsConfigArray[k].substring(4,orgMoreDetailsConfigArray[k].length)).val(replaceAllSpaceNewLine(orgMoreDetailsConfigArray[k].substring(4,orgMoreDetailsConfigArray[k].length),fieldNames));
										 					moreDetail["value"]={"attributes":{"xsi:type":"xs:string","xmlns:xs":"http://www.w3.org/2001/XMLSchema","xmlns:xsi":"http://www.w3.org/2001/XMLSchema-instance"},
										 								"$value": replaceAllSpaceNewLine(orgMoreDetailsConfigArray[k].substring(4,orgMoreDetailsConfigArray[k].length),fieldNames)}
										 					moreStudyDetails.push(moreDetail);
														}
													else if(data.clinical_study.study_type!=='Interventional' && orgMoreDetailsConfigArray[k].substring(4,orgMoreDetailsConfigArray[k].length)==='EXPSR_INT_M'){
															moreDetail["key"]=orgMoreDetailsConfigArray[k].substring(4,orgMoreDetailsConfigArray[k].length);
												 			moreDetail["type"]="studyidtype";
												 			jQuery("#"+orgMoreDetailsConfigArray[k].substring(4,orgMoreDetailsConfigArray[k].length)).val(replaceAllSpaceNewLine(orgMoreDetailsConfigArray[k].substring(4,orgMoreDetailsConfigArray[k].length),fieldNames));
											 				moreDetail["value"]={"attributes":{"xsi:type":"xs:string","xmlns:xs":"http://www.w3.org/2001/XMLSchema","xmlns:xsi":"http://www.w3.org/2001/XMLSchema-instance"},
											 								"$value": replaceAllSpaceNewLine(orgMoreDetailsConfigArray[k].substring(4,orgMoreDetailsConfigArray[k].length),fieldNames)}
											 				moreStudyDetails.push(moreDetail);
														}else if(orgMoreDetailsConfigArray[k].substring(4,orgMoreDetailsConfigArray[k].length)!=='INTVN_TYP_M' && orgMoreDetailsConfigArray[k].substring(4,orgMoreDetailsConfigArray[k].length)!=='EXPSR_INT_M'){
						 							moreDetail["key"]=moreDetailsConfigMap[fieldValue];
										 			moreDetail["type"]="studyidtype";
										 			jQuery("#"+moreDetailsConfigMap[fieldValue]).val(replaceAllSpaceNewLine(moreDetailsConfigMap[fieldValue],fieldNames));
										 			
										 				moreDetail["value"]={"attributes":{"xsi:type":"xs:string","xmlns:xs":"http://www.w3.org/2001/XMLSchema","xmlns:xsi":"http://www.w3.org/2001/XMLSchema-instance"},
							 								"$value": replaceAllSpaceNewLine(moreDetailsConfigMap[fieldValue],fieldNames)}
									 				moreStudyDetails.push(moreDetail);
														}
							 						}
						 						}
						 					 //moreStudyDetails.push(moreDetail);
						 					}
							 			  }
						 				}
						 			}
					 			}
					 			
				 			}
				 		
			 		}
			 		orgMoreStudyDetails = studyDetails.StudySummary.moreStudyDetails;
			 		//console.log(JSON.stringify(orgMoreStudyDetails));
			        if(orgMoreStudyDetails){
				        var stringMoreStudyDetails = JSON.stringify(moreStudyDetails);
				        for(var k=0;k<orgMoreStudyDetails.length;k++){
					        if(jQuery("#"+orgMoreStudyDetails[k].key).val()===""){
					        	if(orgMoreStudyDetails[k]["value"])
					        	jQuery("#"+orgMoreStudyDetails[k].key).val(orgMoreStudyDetails[k]["value"]["$value"]);
					        }
					        if(stringMoreStudyDetails.search(orgMoreStudyDetails[k].key)<=0){
					        		orgMoreStudyDetails[k]["type"]="studyidtype";
					        		if(orgMoreStudyDetails[k]["value"])
					        			orgMoreStudyDetails[k]["value"]["attributes"]={"xsi:type":"xs:string","xmlns:xs":"http://www.w3.org/2001/XMLSchema","xmlns:xsi":"http://www.w3.org/2001/XMLSchema-instance"};
					        		moreStudyDetails.push(orgMoreStudyDetails[k]);
					        }
				        }
			        }
			 		studyDetails.StudySummary["moreStudyDetails"]=moreStudyDetails;
			 		if(data.clinical_study.official_title)
			 			studyDetails.StudySummary["studyTitle"]=data.clinical_study.official_title;
			 		if(!studyDetails.StudySummary.division)
			 			studyDetails.StudySummary["division"]={code:'Not_Applicable',type:'study_division'};
			 		if(!studyDetails.StudySummary.therapeuticArea)
			 			studyDetails.StudySummary["therapeuticArea"]={code:'Not_Applicable',type:'tarea'};
			 		if(!studyDetails.StudySummary.phase)
			 			studyDetails.StudySummary["phase"]={code:'phaseNA',type:'phase'};
			 		studyDetails.StudySummary["parentIdentifier"]={};
			        //alert(data.clinical_study["official_title"]);
			        /*if(data.clinical_study.official_title)
			        jQuery("#studyTitle").val(data.clinical_study.official_title);
			        if(data.clinical_study.study_type)
			        jQuery("#studyType").val(data.clinical_study.study_type);
			        if(data.clinical_study.sponsors.lead_sponsor.agency)
			        jQuery("#sponsorName").val(data.clinical_study.sponsors.lead_sponsor.agency);
			        if(data.clinical_study.brief_summary.textblock)
			        jQuery("#studySummary").val(data.clinical_study.brief_summary.textblock); 
			        if(data.clinical_study.phase) 
			        jQuery("#lPhase").val(data.clinical_study.phase);
			        if(data.clinical_study.study_design_info.primary_purpose)
			        jQuery("#pPurpose").val(data.clinical_study.study_design_info.primary_purpose);
			        if(data.clinical_study.study_design_info.masking)
			        jQuery("#blinding").val(data.clinical_study.study_design_info.masking);
			        if(data.clinical_study.overall_status)
			        jQuery("#studyStat").val(data.clinical_study.overall_status);
			        splitStr = data.clinical_study.firstreceived_date.split(" ");
			        statDate = splitStr[2]+'-'+months[splitStr[0]]+'-'+splitStr[1].slice(0,2)+'T00:00:00.000Z';
			        if(jQuery.isArray(data.clinical_study.condition)){
			        jQuery.each(data.clinical_study.condition,function(i){
			        	diseaseSite=diseaseSite+","+data.clinical_study.condition[i]
				        });
						diseaseSite = diseaseSite.substring(1);
						}
					else{
						diseaseSite = data.clinical_study.condition;
					}
			         
			        jQuery("#disSitename").val(diseaseSite);
			        jQuery("#studyKeywrds").val(diseaseSite.slice(0,50));*/
	if(mode==='N'){
			        orgItem = {}
					orgItem["PK"] = '<%=siteId%>';
					jsonObj = {}
					jsonObj["organizationIdentifier"] = orgItem;
					orgJson.push(jsonObj);
					orgItem = {}
	 				orgItem = {status:{code:"Active",type:"teamstatus"},
	 		 				teamRole:{code:"role_dm",type:"role"},
		 		 				userId:{PK:'<%=userId%>'},
		 		 				userType:"DEFAULT"};
		 		 	jsonArray.push(orgItem);
		 		 	checkTeamName.push('<%=uFName%>'+" "+'<%=uFName%>');
					jQuery("#fName").empty();
					jQuery("#lName").empty();
					jQuery("#uPhone").empty();
					jQuery("#uEmail").empty();
					jQuery("#organization").empty();
					if(jQuery.isArray(data.clinical_study.location)){
					jQuery.each(data.clinical_study.location,function(i){
						//alert(JSON.stringify(data.clinical_study.location[i].facility));
						//var  orgData = data.clinical_study.location[i].facility;
						//alert(JSON.stringify(orgData));
						var name=data.clinical_study.location[i].contact.last_name;
						var phone=data.clinical_study.location[i].contact.phone;
						var email=data.clinical_study.location[i].contact.email;
						var organization=data.clinical_study.location[i].facility.name;
						jQuery.ajax({
						    type: "POST",
						    async:false,
						    url: "updateGetSite.jsp",
						    data: {orgData : JSON.stringify(data.clinical_study.location[i].facility),
						    	   usrData : JSON.stringify(data.clinical_study.location[i].contact),
						    	   reqType : "orgusr"},
						    success: function(data){
							    var response=jQuery(data);				
								var sitePK = response.filter('#siteId').val();
								var userPK = response.filter('#userId').val();
								orgItem = {}
								orgItem["PK"] = sitePK;
								jsonObj = {}
								jsonObj["organizationIdentifier"] = orgItem;
								orgJson.push(jsonObj);
								splitStr=[];
								splitStr = name.split(" ");
								var len = splitStr.length;
								if(jQuery.inArray(splitStr[0]+" "+splitStr[len-1],checkTeamName)<=0){
								orgItem = {}
				 				orgItem = {status:{code:"Active",type:"teamstatus"},
				 		 				teamRole:{code:"role_prin",type:"role"},
					 		 				userId:{PK:userPK},
					 		 				userType:"DEFAULT"};
					 		 	jsonArray.push(orgItem);
					 		 	checkTeamName.push(splitStr[0]+" "+splitStr[len-1]);
					 		 	if(count==0)
					 		 		piJson = {PK:userPK}
					 		 	count++;
								}
						    },
						    failure: function(errMsg) {
						        alert(errMsg);
						    }
								});
						//orgJson = {parentIdentifier:{studyNumber:studyNum}, studyOrganizaton:[+''+orgJson+''+]};
						//alert(orgJson);
						
						//alert(name+" "+phone+" "+email);
						
						var fName="";
						var lName="";
						if(name!=null && name!=""){
							var nameArr = name.split(' ');
							fName=nameArr[0];
							if(nameArr[1]!=null && nameArr[1]!=""){
								lName=nameArr[1];
								if(nameArr[2]!=null && nameArr[2]!=""){
									lName=lName+" "+nameArr[2];
								}
							}
						 jQuery("#fName").append('<input readonly="readonly" name = "fName" type="text" value = "' + fName + '" />&nbsp;&nbsp;');
						 jQuery("#lName").append('<input readonly="readonly" name = "lName" type="text" value = "' + lName + '" />&nbsp;&nbsp;');
						 
						 }if(phone!=null && phone!=""){
						 jQuery("#uPhone").append('<input readonly="readonly" name = "phone" type="text" value = "' + phone + '" />&nbsp;&nbsp;');
						
						 }
						 if(email!=null && email!=""){
						 jQuery("#uEmail").append('<input readonly="readonly" name = "email" type="text" value = "' + email + '" />&nbsp;&nbsp;');
						
						 }
						 if(organization!=null && organization!=""){
							 jQuery("#organization").append('<input readonly="readonly" name = "organization" type="text" value = "' + organization + '" />&nbsp;&nbsp;');
						 }
						 //jQuery("#uEmail").append('<hr/>');
				 			});
						}else{
						  var name=data.clinical_study.location.contact.last_name;
						var phone=data.clinical_study.location.contact.phone;
						var email=data.clinical_study.location.contact.email;
						var organization=data.clinical_study.location.facility.name;
						jQuery.ajax({
						    type: "POST",
						    async:false,
						    url: "updateGetSite.jsp",
						    data: {orgData : JSON.stringify(data.clinical_study.location.facility),
						    	   usrData : JSON.stringify(data.clinical_study.location.contact),
						    	   reqType : "orgusr"},
						    success: function(data){
							    var response=jQuery(data);				
								var sitePK = response.filter('#siteId').val();
								var userPK = response.filter('#userId').val();
								orgItem = {}
								orgItem["PK"] = sitePK;
								jsonObj = {}
								jsonObj["organizationIdentifier"] = orgItem;
								orgJson.push(jsonObj);
								splitStr=[];
								splitStr = name.split(" ");
								var len = splitStr.length;
								if(jQuery.inArray(splitStr[0]+" "+splitStr[len-1],checkTeamName)<=0){
								orgItem = {}
				 				orgItem = {status:{code:"Active",type:"teamstatus"},
				 		 				teamRole:{code:"role_prin",type:"role"},
					 		 				userId:{PK:userPK},
					 		 				userType:"DEFAULT"};
					 		 	jsonArray.push(orgItem);
					 		 	checkTeamName.push(splitStr[0]+" "+splitStr[len-1]);
					 		 	if(count==0)
					 		 		piJson = {PK:userPK}
					 		 	count++;
								}
						    },
						    failure: function(errMsg) {
						        alert(errMsg);
						    }
								});
						//orgJson = {parentIdentifier:{studyNumber:studyNum}, studyOrganizaton:[+''+orgJson+''+]};
						//alert(orgJson);
						
						//alert(name+" "+phone+" "+email);
						
						var fName="";
						var lName="";
						if(name!=null && name!=""){
							var nameArr = name.split(' ');
							fName=nameArr[0];
							if(nameArr[1]!=null && nameArr[1]!=""){
								lName=nameArr[1];
								if(nameArr[2]!=null && nameArr[2]!=""){
									lName=lName+" "+nameArr[2];
								}
							}
						 jQuery("#fName").append('<input readonly="readonly" name = "fName" type="text" value = "' + fName + '" />&nbsp;&nbsp;');
						 jQuery("#lName").append('<input readonly="readonly" name = "lName" type="text" value = "' + lName + '" />&nbsp;&nbsp;');
						 
						 }if(phone!=null && phone!=""){
						 jQuery("#uPhone").append('<input readonly="readonly" name = "phone" type="text" value = "' + phone + '" />&nbsp;&nbsp;');
						
						 }
						 if(email!=null && email!=""){
						 jQuery("#uEmail").append('<input readonly="readonly" name = "email" type="text" value = "' + email + '" />&nbsp;&nbsp;');
						
						 }
						 if(organization!=null && organization!=""){
							 jQuery("#organization").append('<input readonly="readonly" name = "organization" type="text" value = "' + organization + '" />&nbsp;&nbsp;');
						 }
						}
		 			statJson = {parentIdentifier:{studyNumber:studyNum},
				 			studyStatus:[{currentForStudy:true,documentedBy:{PK:'<%=userId%>'},
					 			organizationId:{PK:'<%=siteId%>'},
				 				status:{code:'active',type:'studystat'},statusType:{code:'default',type:'studystat_type'},validFromDate:statDate}]};
			        }
		 			
	 				var studySumJson = {parentIdentifier:{studyNumber:studyNum},studyNumber:studyNum,studyTitle:data.clinical_study.official_title,
	 						isCtrpReportable:false,isFdaRegulated:false,isInvestigatorHeldIndIde:false,isStdDefPublic:false,isStdDesignPublic:false,isStdDetailPublic:false,isStdSponsorInfoPublic:false,nationalSampleSize:0,objective:'',PIMajorAuthor:false,
	 						summaryText:data.clinical_study.brief_summary.textblock,
	 						keywords:diseaseSite.slice(0,50),
	 						principalInvestigator:piJson,
	 						studyAuthor:{PK:'<%=userId%>'},
	 						therapeuticArea:{code:'tarea_30',type:'tarea'},
	 						division:{code:'study_divisi_15',type:'study_division'}};
			if(mode==='N'){
	 				jQuery.ajax({
					    type: "POST",
					    async:false,
					    url: "updateGetSite.jsp",
					    data: {phase : data.clinical_study.phase,
					    	   studyType : data.clinical_study.study_type,
					    	   sponsorName: data.clinical_study.sponsors.lead_sponsor.agency,
					    	   reqType : "stsum"},
					    success: function(data){
						    var response=jQuery(data);				
							var phaseSType = response.filter('#phaseSType').val();
							var studyTypeSType = response.filter('#studyTypeSType').val();
							var sponsorNameSType = response.filter('#sponsorNameSType').val()
							codeJson = {}
							if(phaseSType!=''){
								orgItem = {}
								orgItem = {code:phaseSType,type:'phase'};
								studySumJson["phase"] = orgItem;
							}
							if(studyTypeSType!=''){
								orgItem = {}
								orgItem = {code:studyTypeSType,type:'study_type'};
								studySumJson["studyType"]  = orgItem;
							}
							if(sponsorNameSType!=''){
								orgItem = {}
								orgItem = {code:sponsorNameSType,type:'sponsor'};
								studySumJson["sponsorName"]  = orgItem;
							}
					
					    },
					    failure: function(errMsg) {
					        alert(errMsg);
					    }
							});
					
					studyTeamJson = {parentIdentifier:{studyNumber:studyNum},
							studyTeamMember: jsonArray};
					
					}
				 	var studyJson = {};
				 	if(mode==='M'){
				 		studyJson ={StudyIdentifier:{studyNumber:studyNum},
				 					StudySummary:studyDetails.StudySummary
							        };
				 	}else{
				 		studyJson ={Study:{studyIdentifier:{studyNumber:studyNum},
					       studyOrganizations:{parentIdentifier:{studyNumber:studyNum},
				 		studyOrganizaton:orgJson},
					       studyStatuses:statJson,
					       studySummary:studySumJson,
					       studyTeamMembers:studyTeamJson
						        }};
				 	}
			       jQuery("#createJsonData").val(JSON.stringify(studyJson));

			        },
			    failure: function(errMsg) {
			        alert(errMsg);
			    }
					});
		 $j('#dialog-message').dialog("destroy");
		 }
	 }
	 $(window).load(function() {
			StudyJsonType(false);
			var isqNotConfigured = "[ISQ_URL_NOT_CONFIGURED]";
			var isqNoBeachheads = "{}";
		if('<%=CFG.ISQ_URL%>'!==isqNotConfigured && '<%=CFG.ISQ_BEACHHEADS%>'!==isqNoBeachheads){
			var path = window.document.location.pathname.substring(11);// get jsp path of the url
			isquare.isqObj = {
				isquareURL: '<%=CFG.ISQ_URL%>',
				isquareToken: '<%=CFG.ISQ_TOKEN%>',
				location: path,
				isquareBeachheads: '<%=CFG.ISQ_BEACHHEADS%>',
				isquareParams: {
					loggedInUser: <%=(String) tSession.getAttribute("userId")%>	
					}
				};
			if (isquare.isqObj){
				isquare.createBeachhead(isquare.isqObj);
				}
			}
		});
	function replaceAllSpaceNewLine(configKey,strVal){
		
		if(JSON.stringify(codelstConfigMap).search(configKey)<=0){

			strVal= strVal.replace(/  /g,'').replace(/\r\n/g,' ');
		}else{
			
			var strArray = strVal.split(",");
			for (var l=0;l<strArray.length;l++)
			{
				if(l==0){
					strVal = codelstConfigMap[configKey+"."+strArray[l].replace(/ /g,'_')];
				}else{
					strVal = strVal+","+codelstConfigMap[configKey+"."+strArray[l].replace(/ /g,'_')]
				}
			}
		}
		if(strVal)
			strVal=strVal.trim().substring(0,3980);
		else
			strVal="N/A";
		return strVal.trim().substring(0,3980);
	}
   function getValueFromKey(configKey){
		strVal='';
		if(JSON.stringify(codelstConfigMap).search(configKey)>0){

			strVal = codelstConfigMap[configKey];
		}else{
			
			strVal="N/A";
		}

		return strVal.trim().substring(0,3980);
	}
	 /*function fnClickOnce(e) {
			try {
		        var thisTimeSubmitted = new Date();
		        if (!lastTimeSubmitted) { return true; }
		        if (!thisTimeSubmitted) { return true; }
		        if ( thisTimeSubmitted - lastTimeSubmitted <= 1000 ) {
		            return false;
		        }
		        lastTimeSubmitted = thisTimeSubmitted;
			} catch(e) {}
		    return true;
		}*/
</SCRIPT>		
</head>
<div id="isquare"></div>
<body style="overflow-y: auto; height:100%">
	<%-- Nicholas : Start --%>
	<jsp:include page="skinChoser.jsp" flush="true"/>
	<%-- Nicholas : End --%>
<DIV class="popDiv" style="position: fixed;top: 0;left: 0;z-index: 999;width: 100%;">
<table style="background-color: #E2E2C5" width="100%">
<tr>
<th colspan="3" align="left">Import Data From CT.gov By</th>
</tr>
<tr class="searchBg">
<td width="10%"><%=LC.CTRP_DraftNCTNumber%></td>
<td width="40%">
<% if("M".equals(request.getParameter("mode"))){ %>
<input type="text" id="nctNumber" readonly="readonly" name="nctNumber" size = 40 MAXLENGTH = 100 value="<%=request.getParameter("nctNumber")%>">
<%}else{ %>
<input type="text" id="nctNumber" name="nctNumber" size = 40 MAXLENGTH = 100 value="<%=request.getParameter("nctNumber")%>">
<%} %>
<input type="hidden" id="mode" name="mode" value="<%=request.getParameter("mode")%>">
</td>
<td width="60%">
<% if("M".equals(request.getParameter("mode"))){ 
if ((mode.equals("M") && StringUtil.isAccessibleFor(pageRight, 'E'))){%>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button id="updateStudy" type="button" onclick="studySummaryData();">Import</button>
<%} %>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button id="disChanges" type="button" onclick="self.close();">Cancel</button>
<%}else{%>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button id="getCTStudy" type="button" onclick="StudyJsonType(true);">GetStudyDetails</button>
<%if ((mode.equals("N") && StringUtil.isAccessibleFor(pageRight, 'N'))){ %>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button id="createStudy" type="button" onclick="studySummaryData();">Submit</button>
<%} }%>
</td></tr>
</table>		
</DIV>
<DIV class="popDefault" style="height:auto;margin-top:5em;" id="ctDiv">
<h3 style="font-size: 1em; background-color: #CCCCCC;">Study Summary</h3>
<form method="post" name="ctaStudyForm" action="#" onsubmit="return false;">
<table width="100%" cellspacing="0" cellpadding="0" border="0" class="outline midAlign">
<tr>
<td><%=LC.L_Study_Number%></td>
<td><input type="text" <%=readonly%> id="studyNumber" name="studyNumber" size = 40 MAXLENGTH = 100 value="" /></td></tr>
<%
CodeDao cd1 = new CodeDao(); 
ArrayList cDesc;
ArrayList cSubType;
ArrayList codeCustom;
//while(setItr.hasNext()){
//	key=(String)setItr.next();
//	value=allconfigMap.get(key);
	cd1.getCodeValuesBySubTypesOrdBySeq("studyidtype",(String[])allValues.toArray(new String[allValues.size()]));
	cDesc = cd1.getCDesc();
	cSubType = cd1.getCSubType();
	codeCustom = cd1.getCodeCustom();
	for(i=0;i<cSubType.size();i++){
		if(codeCustom.get(i).toString().equalsIgnoreCase("textarea")){
	%>
	<tr><td width="20%"><%=cDesc.get(i).toString()%></td>
<td class="textareaheight"><TextArea <%=readonly%> id="<%=cSubType.get(i).toString()%>" name="<%=cSubType.get(i).toString()%>" rows=4 cols=93 MAXLENGTH = 1000></TextArea></td></tr>
	<%}else{ %>
<tr><td><%=cDesc.get(i).toString()%></td>
<td><input type="text" id="<%=cSubType.get(i).toString()%>" <%=readonly%> name="<%=cSubType.get(i).toString()%>" size="20" value=""/></td>
</tr>
<%		} 
			}
if("N".equals(request.getParameter("mode"))){%>
<tr><td width="20%"><%=LC.L_Title%></td>
<td  class="textareaheight"><TextArea <%=readonly%> id="studyTitle" name="studyTitle" rows=4 cols=93 MAXLENGTH = 1000></TextArea></td></tr>

<tr><td><%=LC.L_Summary%></td>
 <td  class="textareaheight"><TextArea <%=readonly%>  id="studySummary" name="studySummary" rows=4 cols=93 MAXLENGTH = 1000></TextArea></td></tr>
 
 <tr><td><%=LC.L_Phase%></td>
<td><input type="text" id="lPhase" <%=readonly%> name="lPhase" size="20" value=""/></td>
</tr>
<tr><td><%=LC.L_Study_Type%></td>
<td><input type="text" id="studyType" <%=readonly%> name="studyType" size = 40 MAXLENGTH = 100 value="" /></td></tr>	
	
<tr><td><%=LC.L_Sponsor_Name%></td>
<td><input type="text" id="sponsorName" <%=readonly%> name="sponsorName" size = 40 MAXLENGTH = 100 value="" /></td></tr>
<!--
"Principal Investigator initiated trials checkbox
and
Principal Investigator field
or
Type field" -->

<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td align="center"><input type="checkbox" onclick="return false" name="fda"><%=LC.L_FdaRegulatedStudy%></td></tr>

<!-- "Study Type field and Review Board field and Study Status field" -->
 <tr><td><%=LC.L_Disease_Site%></td>
 <td><input type="text" id="disSitename" <%=readonly%> name="disSitename" size="100" value=""/></td>
 </tr>

<tr><td><%=LC.L_Keywords%></td>
<td class="textareaheight">  <TextArea <%=readonly%> name="studyKeywrds" id="studyKeywrds" rows=2 cols = 50  MAXLENGTH = 500 ></TextArea></td>
</tr>

<tr><td><%=LC.L_Primary_Purpose%></td>
<td><input type="text" id="pPurpose" <%=readonly%> name="pPurpose" size="20" value=""/></td>
</tr>

<tr><td><%=LC.L_Blinding%></td>
<td><input type="text" id="blinding" <%=readonly%> name="blinding" size="20" value=""/></td>
</tr>

<tr><td><%=LC.L_Randomization%></td>
<td><input type="text" id="randomization" <%=readonly%> name="randomization" size="20" value=""/></td>
</tr>
<tr><td><%=LC.L_Study_Status%></td>
<td><input type="text" id="studyStat" <%=readonly%> name="studyStat" size="20" value=""/></td>
</tr>
<tr>
<td>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td>
</tr>
<tr>
<td colspan="2">
<span style="font-size: 1em; background-color: #CCCCCC;"><B>Study Team Members</B></span>
</td>
</tr>
<tr><td><%=LC.L_First_Name%></td>
<td id="fName"><input type="text" name="userFirstName" id="userFirstName"  <%=readonly%>  size = 35 MAXLENGTH = 30 Value=""></td>
</tr>

<tr><td><%=LC.L_Last_Name%></td>
<td id="lName"><input type="text" name="userLastName" <%=readonly%> id="lastName" size = 35 MAXLENGTH = 30 Value=""></td>
</tr>



<tr><td><%=LC.L_Phone%></td>
<td id="uPhone"><input type="text" name="userPhone" <%=readonly%> id="phone" size = 35 MAXLENGTH = 100 Value=""></td>
</tr>


<tr><td><%=LC.L_EhypMail%></td>
<td id="uEmail"><input type="text" name="userEmail" <%=readonly%> id="email" size = 35 MAXLENGTH = 100 Value=""></td>
</tr>

<tr><td>Organization</td>
<td id="organization"><input type="text" name="organization" <%=readonly%> id="organization1" size = 35 MAXLENGTH = 100 Value=""></td>
</tr>
<%} %>
<tr>
<td>
<input type="hidden" name="createJsonData" id="createJsonData" value="">
<input type=hidden id="nctNumberHidden" value=""/>
</td>
</tr>
<!--  <tr>
<td colspan="2" align="center">
<% //if("M".equals(request.getParameter("mode"))){ %>
<button type="button" onclick="studySummaryData();">Import</button>
<%//}else{ %>
<button type="button" onclick="studySummaryData();">Submit</button>
<%//} %>
</td>
</tr>-->
</table>
</form> 
</DIV>
<div style="display:none;" id="dialog-message" title="">
  <br>
  <p>Please wait...<img style="width:200px;" src="../images/jpg/loading_pg.gif"/></p>
</div>
</body>
<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>  
</html> 