<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title><%=LC.L_Pcol_TeamDets%><%--Protocol Team Details*****--%></title>



<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.business.common.*" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>


<SCRIPT Language="javascript">
 function  validate()
 {
     formobj=document.team;
     if (!(validate_col('E-Signature',formobj.eSign))) return false
//      if(isNaN(formobj.eSign.value) == true) 
//      {
<%-- 	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect E-Signature. Please enter again");*****/ --%>
// 	formobj.eSign.focus();
// 	return false;
//       }

	}
</SCRIPT>

<% String src;
src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   
<body>


<jsp:useBean id="teamB" scope="page" class="com.velos.eres.web.team.TeamJB"/>
<jsp:useBean id ="sessionmaint" scope="page" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.team.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.*"%>


<DIV class="browserDefault" id="div1">

<%HttpSession tSession = request.getSession(true);
  if (sessionmaint.isValidSession(tSession))
{
	String accId = (String) tSession.getValue("accountId");
	%>
  <br>
	<Form  name="search" method="post" action="modifyTeam1.jsp?mode=1">
		<br><br><br>
    <table class = tableDefault width="100%" cellspacing="0" cellpadding="0" border=0 >
      <tr> 
        <td class=tdDefault width=20%> <%=LC.L_User_Search%><%--User Search*****--%></td>
        <td class=tdDefault width=30%> <%=LC.L_User_FirstName%><%--User First Name*****--%>: <Input type=text name="fname"></td>
        <td class=tdDefault width=30%> <%=LC.L_User_Last_Name%><%--User Last Name*****--%>: <Input type=text name="lname"></td>
        <td class=tdDefault width=10%> 
        <button type="submit"><%=LC.L_Search%></button>
        </tr>
         
		</table>		
</Form>		

<%
String mode = request.getParameter("mode");

if (mode==null) mode = "0";

if(mode.equals("0"))
	{
	%>
	
	<%
				}
else
	{
	String ufname= request.getParameter("fname") ;
	String ulname=request.getParameter("lname") ;
	String grpId=request.getParameter("grpId") ;
	if ((ufname.equals("")) && (ulname.equals(""))){%>
	<P class="defComments"> <%=MC.M_PlsSrchCrit_ViewUsrList%><%--Please specify the Search criteria and then click on 
	'Go' to view the User List*****--%></P>
	 <%}
	 else
	  {	 
	  	
	  	
	  UserDao userDao = new UserDao();
	  userDao.searchGroupUsers(EJBUtil.stringToNum(accId),ulname,ufname,EJBUtil.stringToNum(grpId));	
	  String usrLastName = "";
	  String usrFirstName = "";
	  String usrMidName = "";
	  String usrId = "";
	  ArrayList usrIds = null;  
	  
	  ArrayList usrLastNames=null;
   	  ArrayList usrFirstNames=null;
   	  ArrayList usrMidNames=null;
   	  usrLastNames = userDao.getUsrLastNames();
	  usrFirstNames = userDao.getUsrFirstNames();
	  %>
	

    <table width="450" border=0>
      <tr> </tr>
      <%

	usrLastNames = userDao.getUsrLastNames();
	usrFirstNames = userDao.getUsrFirstNames();
	usrMidNames = userDao.getUsrMidNames();
    usrIds = userDao.getUsrIds();
    int i;
	int lenUsers = usrLastNames.size();
	%>
      <tr id ="browserBoldRow"> <%Object[] arguments1 = {lenUsers}; %> 
        <td width = 400> <%=MC.M_Tot_NumOfUsers%><%--Total Number of Users*****--%> : <%=VelosResourceBundle.getLabelString("L_Usr_S",arguments1)%><%-- <%= lenUsers%> User(s)*****--%> </td>
      </tr>
      <tr> 
        <th> <%=LC.L_Available_Users%><%--Available Users*****--%> </th>
        <th> <%=LC.L_Select%><%--Select--%> </th>
      </tr>
      <%
      for(i = 0 ; i < lenUsers ; i++)
      {
	usrLastName=((usrLastNames.get(i)) == null)?"-":(usrLastNames.get(i)).toString();
	usrFirstName=((usrFirstNames.get(i))==null)?"-":(usrFirstNames.get(i)).toString();
	usrMidName=((usrMidNames.get(i))==null)?"-":(usrMidNames.get(i)).toString();
	//usrId = ((Integer)usrIds.get(i)).toString();

	if ((i%2)==0) {
  	%>
      <tr class="browserEvenRow"> 
        <%
        }
        else{
  	%>
      <tr class="browserOddRow"> 
        <%
        }
  %>
  <td width =450>  <%=usrFirstName%>&nbsp;<%=usrLastName%></A> </td>
  <td width =50> 
  <Input type="checkbox" name="assign" value = "<%=usrId %>" onclick="changeCount(<%=i%>)"  >
  </td>
  </tr></tr>
  <%
  }
%>

</table>

<%

	  }
	}
}
%>




		
		
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</div>


<DIV class="mainMenu" id = "emenu"> 
  <jsp:include page="getmenu.jsp" flush="true"/>
</DIV>

</body>

</html>
