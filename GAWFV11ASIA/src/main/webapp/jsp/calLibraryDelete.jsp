<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=LC.L_Cal_Del%><%--Calendar Delete*****--%></title>

<!-- YK 05Aug11: Removed for Bug #6728 already using Whichcss_skin.js in panel.jsp -->
<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	<%-- if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   } --%>
}
</SCRIPT>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/> 

<%@ page language = "java" import = "com.velos.eres.business.section.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB"%>
<%@ page import = "com.aithent.audittrail.reports.AuditUtils"%>

<% 
String src;
src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>  

<BODY> 
<br>

<DIV class="formDefault" id="div1">
<%  
	
	String calId= "";
	String selectedTab="";
	
HttpSession tSession = request.getSession(true); 

 if (sessionmaint.isValidSession(tSession))	{ 	
		
		calId= request.getParameter("protocolId");
		
		selectedTab=request.getParameter("selectedTab");
		
		int ret=0;
		
		String delMode=request.getParameter("delMode");
	
		if (delMode==null) {				
			delMode="final";			
%>
	<FORM name="calDelete" id="calDeleteFrm" method="post" action="calLibraryDelete.jsp" onSubmit="if (validate(document.calDelete)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<br><br>
	
	<TABLE width="98%" cellspacing="0" cellpadding="0" >
		<tr><td align="center">
		<b><%=MC.M_PlsEtrEsign_Del%><%--Please enter e-Signature to proceed with deletion*****--%> </b>
		</td></tr>
		<tr><td>&nbsp;</td></tr>
		</table>
			
		<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="calDeleteFrm"/>
			<jsp:param name="showDiscard" value="N"/>
		</jsp:include>

  	 <input type="hidden" name="protocolId" value="<%=calId%>">

	 <Input type="hidden" name="srcmenu" value="<%=src%>" >

	 <input type="hidden" name="delMode" value="<%=delMode%>">

   	 <input type="hidden" name="selectedTab" value="<%=selectedTab%>">


	</FORM>
<%
	} else {
			String eSign = request.getParameter("eSign");	

			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>
 		  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
			} else {
				
			//Modified for INF-18183 ::: Ankit
			ret = eventdefB.calendarDelete(EJBUtil.stringToNum(calId),AuditUtils.createArgs(session,"",LC.L_Evt_Lib));  
			if (ret==-2) {%>
				<br><br><br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_CalCnt_RemdFromLib%><%--Calender could not be removed from Library*****--%> </p>
				<p align = center> <A type="submit" href="protocollist.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&calledFrom=P&mode=N"><%=LC.L_Back%></A> </p> 	
				
			<%}else { %>
				<br><br><br><br><br> <p class = "successfulmsg" align = center><%=MC.M_CalRem_FromLib%><%--Calendar removed from library*****--%></p>
				 <META HTTP-EQUIV=Refresh CONTENT="1; URL=protocollist.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&calledFrom=P&mode=N"> 				
			<%}
			} //end esign
	} //end of delMode	
  }//end of if body for session 
else { %>
 <jsp:include page="timeout.html" flush="true"/> 
 <% } %>
 <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
  
</DIV>

<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/> 
</div>

</body>
</HTML>


