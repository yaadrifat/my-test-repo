<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.business.common.CodeDao"%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></HEAD>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.MilestoneDao" %>

<Link Rel=STYLESHEET HREF="common.css" type=text/css>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="MsB" scope="request" class="com.velos.eres.web.milestone.MilestoneJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="budgetcalB" scope="request"	class="com.velos.esch.web.budgetcal.BudgetcalJB" />

<%@ page language = "java" import = "com.velos.eres.service.util.*,com.velos.esch.business.common.SchCodeDao"%>

<%
Map<String , Integer> sponsorAmountHm = new HashMap<String , Integer>();
String bgtcalId = request.getParameter("bgtcalId");
String studyId  = request.getParameter("studyId");
String budgetId = request.getParameter("budgetId");


String eSign = request.getParameter("eSign");
MilestoneDao msd = new MilestoneDao();



String count="";
String limit = "";
String patStatus = "";
String paymentType = "";
String paymentFor = "";
String eventStatus = "";
String rule = "";
String milestoneTypeSelected = "";
String dateFrom = "";  // Fin-22373 Ankit
String dateTo = "";

String holdbacks[]=null;
String limits[] = null;
String patStatuses[] = null;
String paymentTypes[] = null;
String paymentFors[] = null;
String milestoneStat[] = null;
String counts[] = null;
String eventStatuses[] = null;
String milestoneTypeCodes[] = null;
String rules = null;
String EMrule= "";
int milestoneStatus  =0;
float holdBack;

String sponsorAmountCode[] = null;
String sponsorAmountSelected[] = null;

String arMilestoneTypeSelected[] = null;

String dateFroms[] = null;
String dateTos[] = null;



String msType = "";



int cnt=0;
int ret = 0;

SchCodeDao schCdao = new SchCodeDao();

HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession)) {
 %>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
	String oldESign = (String) tSession.getValue("eSign");
	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
	} else {

	String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = (String) tSession.getValue("userId");



	String budProtId = "";
	String budProtType = "";
	String errMsg = "";
	String calendarStatus = "";
	String calendarStudy = "";

	rule = request.getParameter("rule");
    EMrule = request.getParameter("EMrule");

    holdbacks = request.getParameterValues("holdbackAmt");
    counts = request.getParameterValues("count");
    limits = request.getParameterValues("mLimit");
	patStatuses = request.getParameterValues("dStatus");
    paymentTypes = request.getParameterValues("dpayType");
    paymentFors = request.getParameterValues("dpayFor");
    milestoneStat = request.getParameterValues("milestoneStat");
    
    dateFroms = request.getParameterValues("dateFrom");
    dateTos = request.getParameterValues("dateTo");

    eventStatuses = request.getParameterValues("dEventStatus");
  	arMilestoneTypeSelected = request.getParameterValues("milestoneTypeSelected");
  	milestoneTypeCodes = request.getParameterValues("milestoneTypeCode");
  	/*FIN-22375 Date :16 Aug 2012 By : Yogendra Pratap */
  	sponsorAmountSelected = request.getParameterValues("sponsorAmountSelected");
  	sponsorAmountCode = request.getParameterValues("sponsorAmountCode");
  	/*Setting the Sponsor Amount Checked and Unchecked options*/
  	sponsorAmountHm.put("VMChecked" , Integer.parseInt(sponsorAmountSelected[0]));
  	sponsorAmountHm.put("EMChecked" , Integer.parseInt(sponsorAmountSelected[1]));
  	sponsorAmountHm.put("AMChecked" , Integer.parseInt(sponsorAmountSelected[2]));
  		String VMselected="";
		String EMselected="";

		VMselected = arMilestoneTypeSelected[0];
		EMselected = arMilestoneTypeSelected[1];
		
		
		//JM: 22Feb2011: #5873
		String calStatDesc = "";
		
	if (! bgtcalId.equals("0"))
	{
			budgetcalB.setBudgetcalId(EJBUtil.stringToNum(bgtcalId));
			budgetcalB.getBudgetcalDetails();

			//get protocol details
		 	budProtId = budgetcalB.getBudgetProtId();
		 	budProtType = budgetcalB.getBudgetProtType();


		 	//get Calendar details

		 	if (budProtType.equals("L"))
		 	{
		 			calendarStatus = "";
		 			calendarStudy = "";

		 	}
		  	else //study
		  	 {
		  		 		eventassocB.setEvent_id(EJBUtil.stringToNum(budProtId));
					 	eventassocB.getEventAssocDetails();
					 	calendarStudy = eventassocB.getChain_id();

					 	if (StringUtil.isEmpty(calendarStudy ))
					 	{
					 		calendarStudy  = "";
					 	}					 
					 
					 	//JM: 11FEB2011, #D-FIN9
					 	//calendarStatus = eventassocB.getStaus();
					 	calendarStatus = schCdao.getCodeSubtype(EJBUtil.stringToNum(eventassocB.getStatCode()));					 					 	

					 	if (StringUtil.isEmpty(calendarStatus))
					 	{
					 		calendarStatus = "";
					 	}
		  	 }

		  	 if (! calendarStudy.equals(studyId) )
		  	 {
		  	 	if (VMselected.equals("1") || EMselected.equals("1"))
				{
		  	 		errMsg = MC.M_CalNotLnkWith_BgtStd/*"The selected Calendar is not linked with the Budget's "+LC.Std_Study+". Please select a different Calendar or uncheck 'Visit'/'Event' options"*****/;
		  	 		ret = -3;
		  	 	}
		  	 }
		  	 else if (calendarStatus.equals("IA"))
		  	 {

		  		SchCodeDao cdStatus = new SchCodeDao();
		  		calStatDesc = cdStatus.getCodeDescription(EJBUtil.stringToNum(eventassocB.getStatCode()));
				if (VMselected.equals("1") || EMselected.equals("1"))
				{  Object[] arguments1 = {calStatDesc};
					errMsg =VelosResourceBundle.getMessageString("M_SelCalDiff_UnchkEvtOpt",arguments1); /*"The selected Calendar is "+calStatDesc+". Please select a different Calendar or uncheck 'Visit'/'Event' options";*****/
			  	 	ret = -3;
				}

		  	 }

  	 }

  	if (StringUtil.isEmpty(errMsg ))
  	{


			for (cnt = 0;cnt< 3; cnt++ )
			{

				milestoneTypeSelected = arMilestoneTypeSelected[cnt];

				if (milestoneTypeSelected.equals("1"))
				{
						if (cnt == 1)
						{
							rule = EMrule;

						}else if (cnt== 2) //AM
						{
							rule="0";
						}

							msType = milestoneTypeCodes[cnt];
							holdBack = Float.parseFloat(holdbacks[cnt]);
							count = counts[cnt];
							limit = limits[cnt];
						    patStatus = patStatuses[cnt];
						    paymentType = paymentTypes[cnt];
						    paymentFor = paymentFors[cnt];
						    milestoneStatus = Integer.parseInt(milestoneStat[cnt]);
							eventStatus = eventStatuses[cnt] ;
							
							dateFrom = dateFroms[cnt];
						    dateTo = dateTos[cnt];


						     if (StringUtil.isEmpty(count))
							     {
							     	count = "1";
							     }


						    msd.setHoldBack(holdBack);
							msd.setMilestoneRuleIds(rule);
							msd.setMilestoneTypes(msType);
							msd.setMilestoneEventStatuses(eventStatus);
							msd.setMilestoneCounts(count);
							msd.setPatientStatuses(patStatus);
							msd.setMilestoneLimits(limit);
						    msd.setMilestonePaymentFors( paymentFor) ;
						    msd.setMilestonePaymentTypes( paymentType) ;
						 	//msd.setMilestoneCalIds(protocolId);
						 	msd.setMilestoneStudyIds(studyId);
						 	/*set the sponsor amount checked status into the Dao( MilestoneDao )*/
						 	msd.setSopnsorAmountChkStatus(sponsorAmountHm);/*FIN-22375 Date :16 Aug 2012 By : Yogendra Pratap */
						 	msd.setMilestoneDateFrom(dateFrom) ;
						 	msd.setMilestoneDateTo(dateTo) ;
						 	msd.setMilestoneStatFK(milestoneStatus);

				}
			}
			//System.out.println("EJBUtil.stringToNum(bgtcalId)" + EJBUtil.stringToNum(bgtcalId));

				ret = MsB.createMultiMilestones(EJBUtil.stringToNum(usr), ipAdd, msd, EJBUtil.stringToNum(budgetId), EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(bgtcalId)) ;

		%>

		 <SCRIPT>
		   	//window.opener.location.reload();
			setTimeout("self.close()",1000);
		</SCRIPT>

<br><br><br><br><br>
<%
	}

if (ret >= 0) { %>

<p class = "successfulmsg" align = center><%=MC.M_Data_SavedSucc%><%--Data saved successfully.*****--%></p>

<% } else if (ret == -3)
{
	%>
		<BR><BR><BR><BR>
		<p class = "successfulmsg" align = center><%=errMsg%></p>
		<p align="center" ><button onclick="window.history.back();"><%=LC.L_Back%></button></p>

	<%

} %>

<%
}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>


</BODY>
</HTML>


