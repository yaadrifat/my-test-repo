<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<title><%=LC.L_Groups%><%-- Groups*****--%></title>
<head>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<%-- Nicholas : Start --%>
	<%--<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>--%>
	<%-- Nicholas : End --%>
	
	<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.business.common.UserSiteDao,com.velos.eres.service.util.*"%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB"/>
	<%-- Nicholas : Start --%>
	<jsp:include page="skinChoser.jsp" flush="true"/>
	<Link Rel=STYLESHEET HREF='./styles/velos_popup.css' type=text/css>
	<%-- Nicholas : End --%>
<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1) 
   	ienet = 0; //IE
else
	ienet = 1;

if (ienet == 0) {
%>
<body style="overflow:scroll">
<%} else {%>
<body>
<%}%>




<DIV class="popDefault" > 

<P class="sectionHeadings"> <%=LC.L_FrmLib_Usr%><%-- Form Library >> Users*****--%> </P>
<%
	HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession))	
{
 %>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<% 	
	ArrayList arrIds=null;
	ArrayList arrFirstNames=null;
	ArrayList arrLastNames=null;
	ArrayList arrMidNames=null;
	ArrayList arrGrpNames=null;
	String grpIds=request.getParameter("grpIds");
	String usrLastName="";
	String usrFirstName="";
	String usrMidName="";
	String grpName="";
	 
	UserDao usrDao=new UserDao();
	usrDao.getGroupUsers(grpIds);
	arrIds=usrDao.getUsrIds();
	arrLastNames=usrDao.getUsrLastNames();
	arrFirstNames=usrDao.getUsrFirstNames();
	arrGrpNames = usrDao.getUsrGrpNames();
	int rows = usrDao.getCRows();
	if(rows==0)
	{%>
		<P class="defComments"><%=MC.M_NoRecordsFound%><%-- No records found*****--%></P>
	
	<%}
else{%>

	<table class = tableDefault width="95%" cellspacing="0" cellpadding="0" border="0" >
    <tr> 
    <th  width="25%"> <%=LC.L_Group%><%-- Group*****--%> </th>
    <th   width="25%" > <%=LC.L_User_Name%><%-- User Name*****--%> </th>
	
    </tr>
<%
	
	for(int i = 0 ; i < rows ; i++)
	{
		
		grpName= arrGrpNames.get(i).toString();
		
		if ( i > 0)
		{
			if ( grpName.equals(arrGrpNames.get(i-1).toString() ) )
			{
				grpName="";
			}
		}

		usrLastName=((arrLastNames.get(i)) == null)?"-":(arrLastNames.get(i)).toString();
			
		usrFirstName=((arrFirstNames.get(i))==null)?"-":(arrFirstNames.get(i)).toString();
		
		if ((i%2)==0) 
		{

%>
      <tr class="browserEvenRow"> 
<%
		}

	   else
	   {
%>
      <tr class="browserOddRow"> 
<%

	   }
%>
	<td ><b><%= grpName%><b></td>

	<td >	<%=usrFirstName%>&nbsp;<%=usrLastName%></td>
	  </tr>
<%
		}//end of for statement
}//end of else
%>	</table>
<%
}//end of if body for session
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
</div>
</body>

</html>

