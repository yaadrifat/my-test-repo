<%@page import="com.velos.eres.widget.business.common.ResubmDraftDao"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML> 

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:include page="include.jsp" flush="true"/>	
<jsp:useBean id="submB" scope="request" class="com.velos.eres.web.submission.SubmissionJB"/>
<jsp:useBean id="submBoardB" scope="request" class="com.velos.eres.web.submission.SubmissionBoardJB"/>
<jsp:useBean id="submStatusB" scope="request" class="com.velos.eres.web.submission.SubmissionStatusJB"/>

<%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.business.common.*,java.util.*,java.text.*,com.velos.eres.service.util.*,com.velos.esch.business.common.*,com.velos.eres.compliance.web.*,org.json.*,org.apache.commons.lang.StringEscapeUtils"%>
<%@ page import="com.velos.eres.widget.service.util.FlxPageArchive" %>

<%!
private static final String DEFAULT_DUMMY_CLASS = "[Config_Submission_Interceptor_Class]";
private static final String VERSION_STATUS = "versionStatus";
private static final String LETTER_F = "F";
private static final String ER_STUDYVER_TABLE = "er_studyver";
%>

<%
  HttpSession tSession = request.getSession(true); 
  String studyId = (String) tSession.getValue("studyId");
  String src= StringUtil.htmlEncodeXss(request.getParameter("srcmenu"));
  String selectedTab = StringUtil.htmlEncodeXss(request.getParameter("selectedTab"));
  String eSign = request.getParameter("eSign");
  if (sessionmaint.isValidSession(tSession))
  {	
	
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%   
    String oldESign = (String) tSession.getValue("eSign");
	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {
		String resultMsg = null;
	    HashMap<String, Object> paramMap = new HashMap<String, Object>();
	    paramMap.put("request", request);
	    ResubmDraftDao resubmDraftDao = new ResubmDraftDao();
	    if (resubmDraftDao.saveResubmissionDraft(paramMap) > 0){
	        resultMsg = MC.M_Data_SvdSucc/*"Data was saved successfully"*****/;
	        String nextScreen = "LIND".equals(CFG.EIRB_MODE) ?  "flexResubmissionDraft" : "studyCheckNSubmit";
	%>
	  <br><br><br><br>
	  <p class = "successfulmsg" align = center><%=resultMsg%></p>
	  <META HTTP-EQUIV=Refresh CONTENT="1; URL=<%=nextScreen%>?mode=M&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>&autoPopulate=Y">
	<%
	    } else {
			resultMsg = MC.M_Err_SavingData/*"There was an error while saving data"*****/;
			%>
			<br>
			<br>
			<br>
			<br>
			<p class="successfulmsg" align=center><%=resultMsg%></p>
			<META HTTP-EQUIV=Refresh CONTENT="3; URL=studyCheckNSubmit?mode=M&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>">
 			<%
			return; // An error occurred; let's get out of here
		}
    }// end of if body for e-sign
  } //end of if body for valid session
  else
  {
  %>
  <jsp:include page="timeout.html" flush="true"/>
  <%
  }
%>

</BODY>
</HTML>
