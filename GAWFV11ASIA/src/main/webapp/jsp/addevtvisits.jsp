<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=MC.M_Add_EvtToVisits%><%--Add Events to Visits*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<jsp:include page="include.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<script type="text/javascript">
var flag=true;
 function getBrowserHeight() { 
  var intH = 0; 
  var intW = 0; 

  if(typeof window.innerWidth == 'number' ) { 
    intH = window.innerHeight; 
    intW = window.innerWidth; 
  } 
  else if(document.documentElement &&   (document.documentElement.clientWidth ||   document.documentElement.clientHeight)) { 
    intH = document.documentElement.clientHeight; 
    intW = document.documentElement.clientWidth; 
  } 
  else if(document.body && (document.body.clientWidth || document.body.clientHeight)) { 
    intH = document.body.clientHeight; 
    intW = document.body.clientWidth; 
  } 

  return { width: parseInt(intW), height: parseInt(intH) }; 
} 

function setLayerPosition() { 
  //var showmsgBar = document.getElementById("showmsgBar"); 
  var hidemsgBar = document.getElementById("hidemsgBar"); 

  var bws = getBrowserHeight(); 
  //showmsgBar.style.width = bws.width + "px"; 
  //showmsgBar.style.height = bws.height + "px"; 

  hidemsgBar.style.left = parseInt((bws.width - 350) / 2); 
  hidemsgBar.style.top = parseInt((bws.height - 200) / 2); 

  //showmsgBar = null; 
  hidemsgBar = null; 
} 

function showLayer() { 
	if($j.browser.msie){
		$j( "#dialogNotDelete" ).dialog({
			modal:true,
			height: '100%',
			width: '35%' ,
			close: function(ev, ui) {$j(this).dialog('destroy');  }
		});
	}
	else{
		$j( "#dialogNotDelete" ).dialog({
			modal:true,
			width: '35%' ,
			close: function(ev1, ui1) {$j(this).dialog('destroy');  }
		});
	} 

  //var showmsgBar = document.getElementById("showmsgBar"); 
  //var hidemsgBar = document.getElementById("hidemsgBar"); 

//showmsgBar.style.display = "block"; 
 //hidemsgBar.style.display = "block"; 
 //showmsgBar = null; 
  //hidemsgBar = null; 
  //flag=false;
} 


//window.onresize = setLayerPosition; 

//function to search and refresh events
 function fnSearchEvents(formobj, searchStr){
	if(flag==true){			
		formobj.searchEvent.value="on";
		protocolid=formobj.calProtocolId.value;
		if (formobj.searchEvent.value=="on"){

			formobj.searchVisit.value="off";

			if (formobj.searchEvent.value!='' && searchStr=='Refresh'){
				formobj.searchEvt.value="";
				formobj.action="addevtvisits.jsp?protocolid="+protocolid+"&searchEvt="+"";

			}else{

				//KM-3456
				formobj.action="addevtvisits.jsp?protocolid="+protocolid+"&searchEvt="+encodeString(formobj.searchEvt.value);
			}					
				formobj.submit();
		}
	}
	else{
	 return false;
	}
	
}


//function to search and refresh Visits
 function fnSearchVisits(formobj, searchStr){
	 if(flag==true){			
			formobj.searchVisit.value="on";
			protocolid=formobj.calProtocolId.value;

			if (formobj.searchVisit.value=="on"){
				formobj.searchEvent.value="off";

				if (formobj.searchVisit.value!='' && searchStr=='Refresh'){

					formobj.searchVisit.value="";
					formobj.action="addevtvisits.jsp?protocolid="+protocolid+"&searchVis="+"";

				}else{

					//KM-3456
					formobj.action="addevtvisits.jsp?protocolid="+protocolid+"&searchVis="+encodeString(formobj.searchVis.value);
				}
				formobj.submit();
			}
	}else{
		 return false;
	}

 }

//This function is for checking and unchecking all the evets

function checkAllEvent(formobj){


 	totcount=formobj.totcountEvents.value;



    if (formobj.chkAllEvt.checked){


    	if (totcount==1){

          	formobj.evIds.checked =true ;
		}else {

        	for (i=0;i<totcount;i++){

				formobj.evIds[i].checked=true;

         }
    }
    }else{
    	if (totcount==1){

			formobj.evIds.checked =false ;


		}else {

        	for (i=0;i<totcount;i++){

				formobj.evIds[i].checked=false;

        }
    }

    }

}
//This function is for checking and unchecking all the Visits
function checkAllVisit(formobj){

 	totcount=formobj.totcountVisits.value;

    if (formobj.chkAllVisit.checked){

    if (totcount==1){

          formobj.visIds.checked =true ;
	}
    else {

         for (i=0;i<totcount;i++){

			formobj.visIds[i].checked=true;

         }
    }

    }else{
    	if (totcount==1){

			formobj.visIds.checked =false ;

		}
    	else {
        for (i=0;i<totcount;i++){

			formobj.visIds[i].checked=false;

        }
    }

    }
}


function  validate(formobj){


	//if (!(validate_col('e-Signature',formobj.eSign))) return false


	totEcount=formobj.totcountEvents.value;
	totVcount=formobj.totcountVisits.value;


	var cntUnckdVisit = 0;
	var cntUnckdEvent = 0;

	if (totVcount==1){

		if (formobj.visIds.checked==false){

			cntUnckdVisit ++;
		}


	}else{

		for (i=0; i<totVcount; i++){

			if (formobj.visIds[i].checked==false){

				cntUnckdVisit ++;
			}
		}

	}

	if (totEcount==1){

		if (formobj.evIds.checked==false){

			cntUnckdEvent ++;
		}


	}else{

		for (j=0; j<totEcount; j++){

			if (formobj.evIds[j].checked==false){

				cntUnckdEvent ++;
			}
		}
	}

	if((totVcount==cntUnckdVisit) || (totEcount==cntUnckdEvent)){

	  showLayer() ;
	   //alert("<%=MC.M_PlsSelVst_EvtAdded%>");/*alert("Please select the Visit(s) and Events(s) to be added");*****/
	   flag=true;	  
	   return false;
	}

	/*if(isNaN(formobj.eSign.value) == true) {
	alert("Incorrect e-Signature. Please enter again");
	formobj.eSign.focus();
	return false;
   }*/

}

/*YK 04Jan- SUT_1_Requirement_18489*/
function resetOrder(formObj) {
	protocolid=formObj.calProtocolId.value;
	formObj.action="addevtvisits.jsp?protocolid="+protocolid+"&resetSort=true";
	formObj.submit();
}
/*YK 04Jan- SUT_1_Requirement_18489*/

</SCRIPT>
<script language="Javascript">
function key(e) {
var formObj = document.addeventvisit;
/* Bug#9716 16-May-2012 -Sudhir*/
var srchVst = document.addeventvisit.searchVisit.value
var srchEvnt= document.addeventvisit.searchEvent.value;
if(srchEvnt=="" && srchVst=="" && e.keyCode == 13) flag=false;
     if (e.keyCode == 13) {
       if(fnClickOnce(e)){
		   formObj.submit();
		}
     }
}

</script>
<body onkeypress="javascript:key(event);">

<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.StringUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB"%>

<jsp:useBean id="ctrldao" scope="request" class="com.velos.esch.business.common.EventdefDao"/>
<jsp:useBean id="assocdao" scope="request" class="com.velos.esch.business.common.EventAssocDao"/>

<jsp:useBean id="visitdao" scope="request" class="com.velos.esch.business.common.ProtVisitDao"/>
<jsp:useBean id="protVisitB" scope="request" class="com.velos.esch.web.protvisit.ProtVisitJB"/>


<%@ page language = "java" import = "com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB"%>
<%

   HttpSession tSession = request.getSession(true);
   int pageRight = 0;
   GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");

%>

<DIV class="popDefault" id="div1">

<%
	if (sessionmaint.isValidSession(tSession))
	{

		String protocolId=request.getParameter("protocolid");
		/*YK 04Jan- SUT_1_Requirement_18489*/
		String resetSort=request.getParameter("resetSort");
		resetSort = (resetSort==null)?"":resetSort;


		String calledFrm= request.getParameter("calledfrom");
		calledFrm = (calledFrm==null)?"":calledFrm;



		String tableName="";

 		if(calledFrm.equals("L") || calledFrm.equals("P")){
	   		tableName ="event_def";
	   	}else{
	   		tableName ="event_assoc";
	   	}




		String searchStrEvt= "";

		searchStrEvt= request.getParameter("searchEvt");
		searchStrEvt = StringUtil.decodeString(searchStrEvt);
		searchStrEvt = (searchStrEvt==null)?"":searchStrEvt;


		String searchStrVisit= "";

		searchStrVisit= request.getParameter("searchVis");
		searchStrVisit = StringUtil.decodeString(searchStrVisit);
		searchStrVisit = (searchStrVisit==null)?"":searchStrVisit;



		String mode=request.getParameter("mode");
		String duration=request.getParameter("duration");
		String calstatus=request.getParameter("calstatus");
		String calassoc=request.getParameter("calassoc");
		String displayType=request.getParameter("displayType");
		String pageNo=request.getParameter("pageNo");
		String displayDur=request.getParameter("displayDur");
		String calName=request.getParameter("calName");
		String srcmenu=request.getParameter("srcmenu");
		String selectedTab=request.getParameter("selectedTab");
		String vclick=request.getParameter("vclick");
		
        

	    ArrayList catNames = null;
		ArrayList eventIds= null;
		ArrayList names= null;
		ArrayList costs = null;


  	    String eventId="";
	    String name="";
	    String catName="";
	    String cost="";
		String eventOverlibParameter="";

	    int len = 0;

		//KM - #3456
		String escSearchStrEvt = searchStrEvt;
		escSearchStrEvt = StringUtil.replaceAll(escSearchStrEvt,"\\","\\\\" );
		escSearchStrEvt = StringUtil.replaceAll(escSearchStrEvt,"%", "\\%");
		escSearchStrEvt = StringUtil.replaceAll(escSearchStrEvt,"_", "\\_");

		if (calledFrm.equals("P") || calledFrm.equals("L")) {

		   ctrldao= eventdefB.getProtVisitAndEvents(EJBUtil.stringToNum(protocolId), escSearchStrEvt,resetSort); /*YK 04Jan- SUT_1_Requirement_18489*/

		   eventIds=ctrldao.getEvent_ids();
		   names= ctrldao.getNames();
		   catNames = ctrldao.getEventCategory();
		   costs = ctrldao.getCosts();
		  
		}

		else if (calledFrm.equals("S")){

		   assocdao= eventassocB.getProtVisitAndEvents(EJBUtil.stringToNum(protocolId), escSearchStrEvt,resetSort); /*YK 04Jan- SUT_1_Requirement_18489*/
		   eventIds=assocdao.getEvent_ids();
		   names= assocdao.getNames();
		   catNames = assocdao.getEventCategory();
		   costs = assocdao.getCosts();

		}



		len = eventIds.size() ;
%>

<P class="sectionHeadings" style="width:100%"> <%=MC.M_Cal_AddEvtVst%><%--Calendar >> Add Events to Visits*****--%></P>

<Form name="addeventvisit" id="addevtvisitfrm" method="post"  action="addevtvisitsave.jsp" >
	<table width="100%" border="0">
		<tr>
			<td  width="45%">
				<div style="overflow:auto;overflow-x:hidden; height:430; border:groove;">

		<table width="100%" cellspacing="0" cellpadding="0" border="0">
		  <tr>
	      	<td class="defComments">
	      	<input type="text" name="searchEvt" value="<%=searchStrEvt%>" size="20">
	      	<button type="button" onclick = "return fnSearchEvents(document.addeventvisit, '');"><%=LC.L_Search%></button>
	      	<A href=# onclick="return fnSearchEvents(document.addeventvisit, 'Refresh');"><%=LC.L_Refresh_List%><%--Refresh List*****--%> </A>

	      	</td>
		  </tr>


		  <tr>
		  <td><br><p class="defComments">
		  <% String Hyper_Link = "<A href=\"javascript:void(0);\" onClick=\"resetOrder(document.addeventvisit)\" name=\"resetSort\">"+LC.L_Reset_Sort+"</A>";
          Object[] arguments = {Hyper_Link}; %><%=VelosResourceBundle.getMessageString("M_AddEvt_ResetSort",arguments) %>
		  <%--Add the following selected Events(s) &nbsp;&nbsp;&nbsp;<A href="javascript:void(0);" onClick="resetOrder(document.addeventvisit)" name="resetSort">Reset Sort </A>*****--%></p> </td> <%-- YK 04Jan- SUT_1_Requirement_18489 --%>
		  </tr>

		</table>

				<table width="100%" cellspacing="1" cellpadding="0" border="0" align="">
				    <tr>
						<th width=10% align =center><%=LC.L_Select%><%--Select*****--%><input type="checkbox" name="chkAllEvt" value="" onClick="checkAllEvent(document.addeventvisit)"></th>
				        <th width="30%"> <%=LC.L_Evt_Cat%><%--Event Category*****--%></th>
				        <th width="60%"> <%=LC.L_Event%><%--Event*****--%> </th>
					</tr>
		<%
					for(int counter = 0;counter<len;counter++)
					{

						eventId=((eventIds.get(counter)) == null)?"":(eventIds.get(counter)).toString();
						name=((names.get(counter)) == null)?"-":(names.get(counter)).toString();
						catName=((catNames.get(counter)) == null)?"-":(catNames.get(counter)).toString();
						cost=((costs.get(counter)) == null)?"-":(costs.get(counter)).toString();
						
						if(name.length()>500 && name.length()<=1000)
							eventOverlibParameter=",WRAP, BORDER,2, CENTER,ABOVE,OFFSETY,50,STATUS,'Draggable with overflow scrollbar, caption and Close link',";
						else if(name.length()>1000 && name.length()<=2000)
							eventOverlibParameter=",WRAP, BORDER,2, CENTER,ABOVE,OFFSETY,100,STATUS,'Draggable with overflow scrollbar, caption and Close link',";
						else if(name.length()>2000)
							eventOverlibParameter=",WRAP, BORDER,2, CENTER,ABOVE,OFFSETY,200,STATUS,'Draggable with overflow scrollbar, caption and Close link',";
						else 
							eventOverlibParameter=",ABOVE,";
						
						if ((counter%2)==0)
						{
						%>
					      	<tr class="browserEvenRow">
						<%}else
						 { %>
				      		<tr class="browserOddRow">
						<%}  %>

						<td align ="center"><input type="checkbox" name="evIds" value="<%=eventId%>,<%=cost%>,<%= catName%>"></td>
						<td> <%=catName%></td>
						<!--Code for bug id 24009-->
						<td><%if(name.length()>50){ %><%=name.substring(0,50)%><span onmouseover="return overlib(htmlEncode('<%=name%>')<%=eventOverlibParameter%>CAPTION,'<%=LC.L_Event_Name%><%--<%=LC.Event_Name%> Name*****--%>');" onmouseout="return nd();">...</span><%}else{ %><%=name%><%} %></td>
			    	 	</tr>
					<%
			}//for loop
					%>
				</table>
				<input type="hidden" name="numSel" value="<%=len%>" >
			  </div>

			</td>	<!-- END OF THE TD OF THE FIELD BROWSER -->
			<td width="5%"  align="Center">
				<P class="defComments"><%=LC.L_AddEvt_ToVisits%><%--Add<br>Event(s)<br>   to<br>Visit(s)*****--%></P>
			</td>
			<td width="45%">
				<div style="overflow:auto;overflow-x:hidden; height:430; border:groove;" >
					<table width="100%" cellspacing="0" cellpadding="0" border="0">
		  <tr>
	      	<td class="defComments">
	      	<input type="text" name="searchVis" value="<%=searchStrVisit%>" size="20">
	      	<button type="button" onclick="return fnSearchVisits(document.addeventvisit, '');"><%=LC.L_Search%></button>

	      	<A href=# onclick="return fnSearchVisits(document.addeventvisit, 'Refresh');"><%=LC.L_Refresh_List%><%--Refresh List*****--%> </A>
	      	</td>
	  	  </tr>

	      <tr>
		  <td><br><p class="defComments"> <%=MC.M_Flw_SelcVisits%><%--To the following selected Visit(s)*****--%></p>
		  </td>

		  </tr>

	</table>
			<table width="100%" cellspacing="1" cellpadding="0" border="0" align="left">
			    <tr>
					<th width=10%  align =center ><%=LC.L_Select%><%--Select*****--%><input type="checkbox" name="chkAllVisit" value="" onClick="checkAllVisit(document.addeventvisit)"></th>
			        <th width="50%"> <%=LC.L_Visit%><%--Visit*****--%> </th>
				</tr>
					<%

					ArrayList visitIds = null;
					ArrayList visitNames= null;
					ArrayList displacements = null;


					String visitId = null;
					String visitName= null;
					String displacement= null;
					//KM-#3456
					String escSearchStrVisit =searchStrVisit;

				    escSearchStrVisit = StringUtil.replaceAll(escSearchStrVisit,"\\","\\\\" );
				    escSearchStrVisit = StringUtil.replaceAll(escSearchStrVisit,"%", "\\%");
				    escSearchStrVisit = StringUtil.replaceAll(escSearchStrVisit,"_", "\\_");


					//for visit part alone............
					visitdao = protVisitB.getProtocolVisits(EJBUtil.stringToNum(protocolId), escSearchStrVisit);

					visitIds = visitdao.getVisit_ids();
					visitNames = visitdao.getNames();
					displacements = visitdao.getDisplacements();


					int lenVisit = visitIds.size() ;
					for(int counter = 0;counter<lenVisit;counter++){

						visitId=((visitIds.get(counter)) == null)?"":(visitIds.get(counter)).toString();
						visitName=((visitNames.get(counter)) == null)?"-":(visitNames.get(counter)).toString();
						displacement = ((displacements.get(counter)) == null)?"-":(displacements.get(counter)).toString();


						if ((counter%2)==0)
						{

						%>

					      	<tr class="browserEvenRow">

						<%} else
						 { %>

				      		<tr class="browserOddRow">



						<%}  %>
						<td align ="center">
						<input type="checkbox" name="visIds" value="<%=visitId%>,<%=displacement%>">
						</td>
						<td> <%=visitName%></td>
						<input type="hidden" name="displacements" value="<%=displacement%>" >
					<%}%>

			</table>

			</div>


			</td>

		</tr>


				<input type="hidden" name="totcountEvents" Value="<%=len%>">
				<input type="hidden" name="totcountVisits" Value="<%=lenVisit%>">
				<input type="hidden" name="calProtocolId" value="<%=protocolId%>">
				<input type="hidden" name="resetSort" value="<%=resetSort%>"> <%--YK 04Jan- SUT_1_Requirement_18489 --%>
				<INPUT type="hidden" name="calledfrom"  value=<%=calledFrm%>>
				<INPUT type="hidden" name="eventName"  value="<%=name%>" >
				<INPUT type="hidden" name="tableName"  value=<%=tableName%>>

				<INPUT type="hidden" name="mode"  value=<%=mode%>>
				<INPUT type="hidden" name="duration"  value=<%=duration%>>
				<INPUT type="hidden" name="calstatus"  value=<%=calstatus%>>
				<INPUT type="hidden" name="calassoc"  value=<%=calassoc%>>

				<INPUT type="hidden" name="searchEvent"  value="">
				<INPUT type="hidden" name="searchVisit"  value="">
				<INPUT type="hidden" name="pageNo"  value=<%=pageNo%>>
				<INPUT type="hidden" name="displayType"  value=<%=displayType%>>
				<INPUT type="hidden" name="displayDur"  value=<%=displayDur%>>
				<INPUT type="hidden" name="calName"  value=<%=calName%>>
				<INPUT type="hidden" name="srcmenu"  value=<%=srcmenu%>>
				<INPUT type="hidden" name="selectedTab"  value=<%=selectedTab%>>
				<INPUT type="hidden" name="vclick"  value=<%=vclick%>>
				  


		</table>



<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="N"/>
			<jsp:param name="formID" value="addevtvisitfrm"/>
			<jsp:param name="showDiscard" value="N"/>
	</jsp:include>
	<br>





			</Form>
			
			<div id="dialogNotDelete" style="display:none; top: 0px;">
			<div><p class = "successfulmsg" align = center><b><%=MC.M_PlsSelVst_EvtAdded%></p> </br></div>
			<p align="center"><button id="closeBttnAftr" onclick="$j('#dialogNotDelete').dialog('close');return false;"><%=LC.L_Close%></button></p>
			</div>
			
		<%





	}//end of if body for session

	else

	{

	%>

		<jsp:include page="timeout.html" flush="true"/>

	<%

	}

	%>



</div>

<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>




</body>

</html>

