<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<%@ page language = "java" import="com.velos.eres.service.util.*" %>
<HEAD>

<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=windows-1252">

<TITLE><%=LC.L_Contact_Us%><%--Contact Us*****--%></TITLE>

</HEAD>



<BODY style = "overflow:auto">



<Link Rel=STYLESHEET HREF="common.css" type=text/css>



<DIV class="staticDefault">



<table width="100%" cellspacing="0" cellpadding="0" border=0>

<tr>

<td height="20">

</td>

</tr>

<tr>

<td width="60%">

<img src="../images/jpg/contactus.jpg"  align="right"></img>

</td>

<td id="aboutHeadings" width="30%" align="center">

<%=LC.L_Contact_Us%><%--Contact Us*****--%>

</td>



<td width="10%">

</td>

</tr>

</table>



<table width="100%" cellspacing="0" cellpadding="0" border=0>

<tr>

<td height="20">

</td>

</tr>

<tr>

<td><%=MC.M_WcomeQue_InfoServ%><%--We welcome your questions and suggestions.For more information about our products and services, or for Customer Support, please contact*****--%>: 

</td>

</tr>

</table>

<br>

<table width="100%" cellspacing="0" cellpadding="0" border=0>

<tr>

<td width = "40%"><b><%=LC.L_E_Mail%><%--e-Mail*****--%></b></td>



<td width = "60%"><A HREF="mailto:eresearch@velos.com" ><%=LC.L_EresVelos_Com%><%--eresearch@velos.com*****--%></a> </td>



</tr>



<tr>

<td width = "40%"><b><%=LC.L_Phone%><%--Phone*****--%>:</b></td>



<td width = "60%"><%=LC.L_Tel510_4010%><%--(510) 739 4010*****--%></td>



</tr>



<tr>

<td width = "40%"><b><%=LC.L_Fax_Upper%><%--FAX*****--%>:</b></td>



<td width = "60%"><%=LC.L_Tel510_4018%><%--(510) 739 4018*****--%></td>



</tr>



<tr>

<td width = "40%"><b><%=LC.L_Address%><%--Address*****--%>:</b></td>

<td width = "60%"><%=LC.L_Eres_HelpDesk%><%--eResearch Help Desk*****--%></td>

</tr>



<tr>

<td width = "40%"></td>

<td width = "60%"><%=LC.L_Walnut_Av%><%--2201, Walnut Avenue*****--%></td>

</tr>



<tr>

<td width = "40%"></td>

<td width = "60%"><%=LC.L_Suite208%><%--Suite 208*****--%></td>

</tr>



<tr>

<td width = "40%"></td>

<td width = "60%"><%=LC.L_Fremont_CA94538%><%--Fremont, CA 94538*****--%></td>

</tr>



</table>



<div>

<jsp:include page="bottompanel.jsp" flush="true"> 

</jsp:include>   

</div>



</BODY>



</DIV>



</HTML>

