<%@page import="com.velos.eres.web.network.NetworkJB"%>
<%@page import="com.velos.eres.web.site.SiteJB"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><%=MC.M_Org_User %><%--Organization >> Users*****--%></title>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
 <script src='./dashboard/js/jquery-1.11.1.js'></script>
 <script src="js/jquery/jquery-1.11.1.js"></script>
 <script src='./dashboard/js/jquery-ui-1.11.0.js'>
    </script>
 
 <style type="text/css">
 .ui-autocomplete {
    cursor: default;
    position: absolute;
    background: #FAFAFA;
    max-height: 100px;
    overflow-y: auto;
    overflow-x: hidden;
    }
    .ui-widget-content a text{
   color: :#FAFAFA
}
 </style>
<script>
//var netWorkId=$j("#networkId").val();

var $j = jQuery.noConflict();
$j( function() {
	fRenderAllNetworkUsers($j("#networkId").val());
    $j( "#addNWUsers" ).autocomplete({
        
        source : function(request, response) {
            $.ajax({
                 url : "userNetworkAutoCompleteJson.jsp",
                 type : "POST",
                 autofocus:true,

                 data : {
                	 term : request.term,
                	 networkId:$j("#networkId").val()  
                 },


                 
                 dataType : "json",
                 success: function(data){
                     response( $.map( data, function( item ) {
                         return {
                             label: item.value,
                             userId: item.key     // EDIT
                         }
                     }));
                  },
                  
          });
       },
       select : function(event, ui) {
           var assignUserId=ui.item.userId
    	   fSaveUsersNetwork(assignUserId); 
           $j("#addNWUsers").val('');
           return false;
       }
    
    });
  } );

function addrow(ntUsrPk,pageRight,historyId,src,networkid,nlevel,dNetADTRole){
	var urlCall="";
	jQuery.ajax({
		url: 'userUpdateNetwork.jsp?networkUser='+ntUsrPk+'&pageRight='+pageRight+'historyId='+historyId+'&operation=inserAddnl',
		cache: false,
		type: "POST",
		async:false,
		success : function (data) 
		{
			var response=jQuery(data);	
		}
	
	
	});	
	fRenderNetworkAdditonalUsers(ntUsrPk,pageRight,src,networkid,nlevel,dNetADTRole);
	
	
}
function fRenderNetworkAdditonalUsers(ntUsrPk,pageRight,src,networkid,nlevel,dNetADTRole){
	var urlCall="";
	jQuery.ajax({
		url: 'userUpdateNetwork.jsp?networkUser='+ntUsrPk+'&operation=fetch',
		cache: false,
		type: "POST",
		async:false,
		success : function (data) 
		{
			var response=jQuery(data);	
			var pk_nwaddnl = response.filter('#additonalcheckVal').val();
			var array = pk_nwaddnl.split(",");
			var res = dNetADTRole.replace(/@/g , "'");
			pk_nwaddnl = array[0];
			var historyid = array[1];
			var adtstatus= array[2];
			res = res.replace(/pkaddnl/g,pk_nwaddnl);
					
			var winparam1 = 'M';
			var winparam2 = 'N';
			var winparam3 = 'er_nwusers_addnlroles';
			var markup = "<table style='width:100%'><tr id='ntUsrAddlpk_"+pk_nwaddnl+"'><td id='ntUsrAddlpk_"+pk_nwaddnl+"'><div style='float:left;width:60%'><span id='span_"+pk_nwaddnl+"_0' onclick='openDDListMul(this.id,0,"+pageRight+");'>Select an option</span>"+res+" </div><div style='float:left;width:15%'><A href='#' id='span_addstatus_"+pk_nwaddnl+"'_'"+historyid+"' onclick='openWinStatus("+pageRight+",\""+winparam1+"\",\""+winparam3+"\","+pk_nwaddnl+","+historyid+");'>"+adtstatus+"</A></div><div style='float:left;width:18%'><A href='#' onclick='openWinStatus("+pageRight+",\""+winparam2+"\",\""+winparam3+"\","+pk_nwaddnl+",0);'><img style='height:19px;width:19px;' src='./images/edit.gif' border='0'/></A>&nbsp<A href='showinvoicehistory.jsp?modulePk="+pk_nwaddnl+"&pageRight="+pageRight+"&srcmenu="+src+"&from=ntwusrhistory&fromjsp=usersnetworksites.jsp&currentStatId="+historyid+"&moduleTable=er_nwusers_addnlroles&netWorkId="+networkid+"&nLevel="+nLevel+"'><img border='0' style='height:19px;width:19px;' src='./images/History.gif' ></A><a href='#' >&nbsp<img style='height:19px;width:19px;' src='./images/delete.gif' onclick='deleteUserNetworkAddRole("+pk_nwaddnl+");' title='Delete' border='0'></a></div></td></tr></table>";
			$j("#additon_role_"+ntUsrPk+"").append(markup);
			
			}
	
	
	});	
	
}
function fSaveUsersNetwork(assignUserId){
	var netWorkId=$j("#networkId").val();
	var urlCall="";
	jQuery.ajax({
		url: 'userUpdateNetwork.jsp?assignUserId='+assignUserId+'&netWorkId='+netWorkId+'&operation=I',
		cache: false,
		type: "POST",
		async:false,
		success : function (data) 
		{
			fRenderAllNetworkUsers(netWorkId);	
		}
	});	
}

function fRenderAllNetworkUsers(netWorkId){
	var schNwUser=document.getElementById("searchNWUsers").value;
	var nLevel = document.getElementById("nLevel").value;
	var from =  document.getElementById("from").value;
	jQuery.ajax({
		url: 'allNtUsersRender.jsp?netWorkId='+netWorkId+'&from='+from+'&nLevel='+nLevel+'&searchNWUser='+schNwUser,
		cache: false,
		type: "POST",
		async:false,
		success : function (data) 
		{
			jQuery("#mydiv").html(data);
		}
	});	
}
function fOpenNetworkUser(userPk,networkUsrId,nLevel,ntUsrType,from){
	var pageRight=document.getElementById("pageRight").value;
	if(ntUsrType=='NS'){
		if(from!='studyNetwork')
			from='networkTabs';
		windowName =window.open('nonSystemUserDetails.jsp?mode=M&srcmenu=tdMenuBarItem2&pageRight='+pageRight+'&networkUsrId='+networkUsrId+'&from='+from+'&userId='+userPk+'&nLevel='+nLevel+'&userType='+ntUsrType, 'Information1', 'toolbar=no,scrollbar=yes,resizable=yes,menubar=no,status=yes,width=900,height=500, top=120, left=120');
	}else
		windowName =window.open('userdetails.jsp?mode=M&srcmenu=tdMenuBarItem2&src=networkTabs&pageRight='+pageRight+'&networkUsrId='+networkUsrId+'&from='+from+'&userId='+userPk+'&nLevel='+nLevel+'&userType='+ntUsrType, 'Information1', 'toolbar=no,scrollbar=yes,resizable=yes,menubar=no,status=yes,width=900,height=500, top=120, left=120');
	windowName.focus();
}
function openDDListMul(Id,flag,pageRight){
	if (f_check_perm(pageRight,'E') == true)
	{
	if(flag==0){
		$j("#"+Id).hide();

		$j("#"+Id.replace("span","ntusradroleId")).show();
     }
	else{
      $j("#"+Id).hide();
      $j("#"+Id.replace("ntusradroleId","span")).text($j("#"+Id+" option:selected").text());
      var sliceId = $j("#"+Id).closest('tr').attr('id').split("_");
		updateNetworkUserADTLRole(sliceId[1],$j("#"+Id+" option:selected").val());
		$j("#"+Id.replace("ntusradroleId","span")).show();
	 }
	}
	}
function updateNetworkUserADTLRole(nwPkUser,trole){
	var urlCall="";
	jQuery.ajax({
		url: 'userUpdateNetwork.jsp?nwPkUser='+nwPkUser+'&trole='+trole+'&operation=AU',
		cache: false,
		type: "POST",
		async:false,
		success : function (data) 
		{
			//fRenderAllNetworkUsers($j("#networkId").val());	
		}
	});	
}

function openDDList(Id,flag,pageRight){

	if (f_check_perm(pageRight,'E') == true)
	{
	if(flag==0){
		$j("#"+Id).hide();
		$j("#"+Id.replace("span","ntusrroleId")).show();
      }
	else{
		
		$j("#"+Id).hide();
		
		$j("#"+Id.replace("ntusrroleId","span")).text($j("#"+Id+" option:selected").text());
		var sliceId = $j("#"+Id).closest('tr').attr('id').split("_");
		updateNetworkUserTRole(sliceId[1],$j("#"+Id+" option:selected").val());
		//alert(sliceId[3]);
		//if(sliceId[3]>0)
			//saveNetworkRow(sliceId[3],-1,0,0,$j("#"+Id+" option:selected").val());
		//else
			//saveNetworkRow(sliceId[1],-1,0,0,$j("#"+Id+" option:selected").val());;
		$j("#"+Id.replace("ntusrroleId","span")).show();
	  }
	}
}
function updateNetworkUserTRole(nwPkUser,trole){
	var urlCall="";
	jQuery.ajax({
		url: 'userUpdateNetwork.jsp?nwPkUser='+nwPkUser+'&trole='+trole+'&operation=U',
		cache: false,
		type: "POST",
		async:false,
		success : function (data) 
		{
			//fRenderAllNetworkUsers($j("#networkId").val());	
		}
	});	
}
function updateNetworkUserADTRole(nwPkUser,trole){
	var urlCall="";
	jQuery.ajax({
		url: 'userUpdateNetwork.jsp?nwPkUser='+nwPkUser+'&trole='+trole+'&operation=AD',
		cache: false,
		type: "POST",
		async:false,
		success : function (data) 
		{
			//fRenderAllNetworkUsers($j("#networkId").val());	
		}
	});	
}
function deleteUserNetwork(userNetworkPk){
	if (confirm(M_NtUsrDelMesg)) {
	var urlCall="";
	jQuery.ajax({
		url: 'userUpdateNetwork.jsp?nwPkUser='+userNetworkPk+'&operation=D',
		cache: false,
		type: "POST",
		async:false,
		success : function (data) 
		{
			fRenderAllNetworkUsers($j("#networkId").val());	
		}
	});	
	}else{
    	return false;
    }
}

function deleteUserNetworkAddRole(userNetworkPk){
	if (confirm(M_NtUsrDelMesg)) {
	var urlCall="";
	jQuery.ajax({
		url: 'userUpdateNetwork.jsp?nwPkUser='+userNetworkPk+'&operation=DAddRole',
		cache: false,
		type: "POST",
		async:false,
		success : function (data) 
		{
			fRenderAllNetworkUsers($j("#networkId").val());	
		}
	});	
	}else{
    	return false;
    }
}



function networkUserSitesAppendix(userNetworkPk,from,userPk,pageRight){
	if(from=="studyNetwork")
	{
		windowName =window.open('eventappendix.jsp?networkId='+userNetworkPk+'&userPk='+userPk+'&calledFrom=studyNetwork&fromPage=selectStudyNetwork&networkFlag=Network_User_doc&selectedTab=3', 'Information', 'toolbar=no,scrollbar=yes,resizable=yes,menubar=no,status=yes,width=950,height=400, top=100, left=100');
	}
	else
	{
		windowName =window.open('eventappendix.jsp?networkId='+userNetworkPk+'&pageRight='+pageRight+'&userPk='+userPk+'&calledFrom=NetWork&fromPage=selectNetwork&networkFlag=Network_User_doc&selectedTab=3', 'Information', 'toolbar=no,scrollbar=yes,resizable=yes,menubar=no,status=yes,width=820,height=450, top=100, left=100');
	}
	windowName.focus();
	}
function searchNWUsersByName(){
	fRenderAllNetworkUsers($j("#networkId").val());	
}

function openWinStatus(pgRight,mode,modulename, networkuserId,statusId){
	var checkType;

	if (mode=='N')
		checkType = 'N';
	else
		checkType = 'E';

			var otherParam;

				otherParam = "&statusCodeType=networkuserstat&sectionHeading=<%=MC.M_NtwUserStatus%>&statusId=" + statusId;

				if (f_check_perm(pgRight,checkType) == true) {

					windowName= window.open("editstatus.jsp?mode=" + mode + "&modulePk=" +  networkuserId +"&moduleTable="+modulename+ otherParam,"statusdetails","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=800,height=600");
					windowName.focus();
				}
		}

  </script>




<body>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.site.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%><%@page import="com.velos.eres.service.util.*"%>
<jsp:include page="jqueryUtils.jsp"></jsp:include> 
<%
HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession))
{
	String from=(request.getParameter("from")==null)?"":request.getParameter("from");
	String networkId=request.getParameter("networkId");
	System.out.println("networkId-"+networkId);
	String nLevel=request.getParameter("nLevel");
	int pageRight=EJBUtil.stringToNum(request.getParameter("pageRight")==null?"":request.getParameter("pageRight"));
	System.out.println("nLevel-"+nLevel);
	NetworkJB  nJB = new NetworkJB();
	nJB.setNetworkId(StringUtil.stringToInteger(networkId));
	nJB.getNetworkDetails();
	SiteJB siteJB = new SiteJB();
	siteJB.setSiteId(StringUtil.stringToInteger(nJB.getSiteId()));
	siteJB.getSiteDetails();
	String siteName = siteJB.getSiteName();
	String parentSiteId ="0";
	if(nJB.getNetworkMainId()!=null)
		parentSiteId = nJB.getNetworkMainId();
	else
		parentSiteId = StringUtil.integerToString(nJB.getNetworkId());
	nJB = new NetworkJB();
	nJB.setNetworkId(StringUtil.stringToInteger(parentSiteId));
	nJB.getNetworkDetails();
	siteJB = new SiteJB();
	siteJB.setSiteId(StringUtil.stringToInteger(nJB.getSiteId()));
	siteJB.getSiteDetails();
	String parentNetwork = siteJB.getSiteName();
	
%>
<input type="hidden" id="networkId" name="networkId" value="<%=networkId%>"/>
<input type="hidden" id="nLevel" name="nLevel" value="<%=nLevel%>"/>
<input type="hidden" id="from" value="<%=from%>"/>
<input type="hidden" id="pageRight" value="<%=pageRight%>"/>
<div align="center" style=" width: 99%;">
<%=LC.L_UsrAsscOrgn%> '<%=siteName %>'
</br>
<%=LC.L_InNetwork%> :'<%=parentNetwork %>'
</div>
<div>
<%=LC.L_Add%> <%=LC.L_Users%>:
<%if(from.equals("studyNetwork") || ((from.equals("networkTabs") || from.equals("")) && (pageRight==6 || pageRight==4))){%>
	<input id="addNWUsers" name="addNWUsers" placeholder="Type and search to Add user to Organization" type="text" value="" size="45"  disabled/>
<%}else{%>
	<input id="addNWUsers" name="addNWUsers" placeholder="Type and search to Add user to Organization" type="text" value="" size="45" />
<%}%>
</div>
<div style="position:absolute; right: 2%">
<%=LC.L_Search %>:<input id="searchNWUsers" name="searchNWUsers" onkeyup="searchNWUsersByName();" placeholder="<%=LC.L_Search %>" type="text" value="" size="30" />
</div>
<div class="tabDefBotN" id="mydiv" style="width:99%;  overflow:auto; height:320px;">

</div>
<%}else{ %>
<jsp:include page="timeout.html" flush="true"/>
  		<%}
	%>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

	<div class ="mainMenu" id="emenu"> 
  		<jsp:include page="getmenu.jsp" flush="true"/>
	</div>
</body>
</html>