<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%>
<%@ page import="com.velos.eres.service.util.*,com.aithent.audittrail.reports.AuditUtils"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<head>
	<title><%=LC.L_Del_FldAction%><%--Delete Field Action*****--%></title>
</head>

<jsp:include page="popupJS.js" flush="true"/>
<jsp:include page="include.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
	
<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

<%-- 	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   } --%>
}
</SCRIPT>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY> 
<br>

<DIV class="popDefault" id="div1">
<% 

HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession))	{
  
	String formId = request.getParameter("formId");
	String fieldAction = request.getParameter("fieldAction");
    String codeStatus = request.getParameter("codeStatus");
	String delMode=request.getParameter("delMode");
	if (delMode==null && !codeStatus.equals("WIP")) {
		delMode="final";
%>
	<FORM name="deleteFieldAction" id="delFldActFrm" method="post" action="deleteFieldAction.jsp" onSubmit="if (validate(document.deleteFieldAction)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<br>
	<P class="successfulmsg"><%=MC.M_Del_InterFldAction%><%--Delete Inter Field Action*****--%></P>
	<P class="defComments"><%=MC.M_Etr_Esign_ToProc%><%--Please enter e-Signature to proceed*****--%></P>	
	<table width="100%" cellpadding=0 cellspacing=0>
		<tr valign=baseline bgcolor="#cccccc">
			<td>
				<jsp:include page="submitBar.jsp" flush="true"> 
					<jsp:param name="displayESign" value="Y"/>
					<jsp:param name="formID" value="delFldActFrm"/>
					<jsp:param name="showDiscard" value="N"/>
				</jsp:include>
			</td>
		</tr>
	</table>

 	 <input type="hidden" name="delMode" value="<%=delMode%>">
  	 <input type="hidden" name="codeStatus" value="<%=codeStatus%>">
 	 <input type="hidden" name="formId" value="<%=formId%>">
	 <input type="hidden" name="fieldAction" value="<%=fieldAction%>">
	 <input type="hidden" name="calledFrom" value="<%=request.getParameter("calledFrom")%>"> <!--12-Oct-2011 Bug#7108 Ankit -->
	 </FORM>
<%
	} else {
			String eSign = request.getParameter("eSign");	
			String oldESign = (String) tSession.getValue("eSign");
			
			if((codeStatus.equals("WIP")) || ((!codeStatus.equals("WIP") && !codeStatus.equals("")) && (oldESign.equals(eSign)))){

				int ret=0;

				FieldActionDao fieldActionDao = new FieldActionDao();
				//Modified for INF-18183 ::: Ankit
				String calledFrom = request.getParameter("calledFrom");
				String moduleName = (calledFrom.equals("L"))? LC.L_Form_Lib : LC.L_Form_Mgmt;
				ret = fieldActionDao.deleteFieldAction(fieldAction,formId,AuditUtils.createArgs(session,"",moduleName));//Modified for INF-18183 ::: Ankit
				
%><br><BR><BR><BR><BR><%
if(ret == 0) {
%>
	<p class = "successfulmsg" align = center> <%=MC.M_FldAction_DelSucc%><%--Field Action was deleted successfully.*****--%></p>
<%
	} 
	 else {
%>
	<p class = "successfulmsg" align = center> <%=MC.M_FldAct_CntDel%><%--Field Action could not be deleted .*****--%> </p>
<%
	}
 %>
<% if (ret >= 0)
	{%>
	  <script>
			window.opener.location.reload();
			setTimeout("self.close()",1000);
	  </script>	  				
	<%
	} // end of if status got deleted 
	  else
		 {
	 	%>
		 	<table width="550" border = "0">
	 			<tr>
					<td align="center">
						<button onClick="window.self.close();"><%=LC.L_Close%></button>	 
					</td>
	   			</tr>
 			</table>	
		<%
	  }  
	
}else{	
%>
	 		  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
			} //end esign
 } //end of delMode
}//end of if body for session
else {
%>
<jsp:include page="timeout_childwindow.jsp" flush="true"/> 
<%
}
%>
   
</DIV>
<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</BODY>
</HTML>
