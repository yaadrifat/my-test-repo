<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=MC.M_Exclude_MultiLineItems%><%--Exclude Multiple Line Items*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>


<SCRIPT Language="javascript">


 function  validate(formobj){

 if (!(validate_col('e-Signature',formobj.eSign))) return false

	 var cnt  =0 ;
	 total = formobj.count.value;
	 selIds = new Array();

	 if(total == 1){
		 
	  if(formobj.chkItemId.checked == true){
		 cnt ++;	 
		}
	 }
	 if(total > 1){
		 		 
	 for(j=0;j<total;j++){
		 if(formobj.chkItemId[j].checked == true){
		 cnt ++;
		 }
		}
	 }
	 
	 if(cnt == 0){
	 alert("<%=MC.M_SelItemsExcluded_FromBgt%>");<%--//alert("Please select Line Item(s) to be excluded from the budget");--%>
	 return false;
	 }
	 
	if(total == 1)
	 {
	   if(formobj.chkItemId.checked == true){
			
			selIds[0] = formobj.lineitemId.value;

		}
	 
	 }
	 j=0;
	 if(total > 1)
	 {
		for(i=0;i<total;i++){
		if(formobj.chkItemId[i].checked == true){
			
			selIds[j] = formobj.lineitemId[i].value;
			j++;
		}

		}
	 }
	 formobj.selLineitemIds.value = selIds;
	  if (!(validate_col('e-Signature',formobj.eSign))) return false
<%-- 	 if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
    	} --%> 
 }
 
</SCRIPT>
<jsp:include page="skinChoser.jsp" flush="true"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="lineitemB" scope="session" class="com.velos.esch.web.lineitem.LineitemJB"/>
<jsp:useBean id="budgetcalB" scope="request"	class="com.velos.esch.web.budgetcal.BudgetcalJB" />
<jsp:useBean id="budgetB" scope="request"	class="com.velos.esch.web.budget.BudgetJB" />
	
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.esch.business.common.*,com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.esch.business.budgetcal.impl.*" %>

<body>
<%
int ienet = 2;

String agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1)
     ienet = 0; //IE
    else
	ienet = 1;
	if(ienet == 0) {
%>
<DIV class="popDefault" id="div1"> 
<%}else { %>
<DIV class="popDefault" id="div1" style="width: 94%;"> 
 <%}

   HttpSession tSession = request.getSession(true);
   if (sessionmaint.isValidSession(tSession))
   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
	<jsp:include page="include.jsp" flush="true"/>
	 <P class="sectionHeadings"> <%=MC.M_BgtOpn_ExclMulLine%><%--Budget >> Open >> Exclude Multiple Line Items*****--%> </P> 

<%
	
	int pageRight = 0;	
	String fromRt = request.getParameter("fromRt");
	if (fromRt==null || fromRt.equals("null")) fromRt="";
	%>
	<Input type="hidden" name="fromRt" value="<%=fromRt%>" />
	<%
	if (fromRt.equals("")){
		GrpRightsJB bRights = (GrpRightsJB) tSession.getAttribute("BRights");		
		pageRight = Integer.parseInt(bRights.getFtrRightsByValue("BGTDET"));
	} else{
		pageRight = EJBUtil.stringToNum(fromRt);
	}
	int bgtcalId = EJBUtil.stringToNum(request.getParameter("bgtcalId"));
	
	String from = request.getParameter("from");
	from = (from==null)?"":from; 
	
//	String budgetId = request.getParameter("budgetId");
//	String budgetTemplate = request.getParameter("budgetTemplate");
	
	
	%>
	<Form name="delMul" method="post" id="delMultLineItem" action="delMulLineitemsSubmit.jsp" onsubmit="if (validate(document.delMul)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
		<table>
		<%
	if (pageRight >= 4)
	{
		    ArrayList lineitemIds = null;
			
            ArrayList lineitemNames = null;
			
			ArrayList bgtSectionIds = null;
			
			ArrayList bgtSectionNames = null;
			
			budgetcalB.setBudgetcalId(bgtcalId);
			budgetcalB.getBudgetcalDetails();
			
			int budgetPK = 0;
			int fk_protocol = 0;	
			int budgetDefaultCalendar = 0;
			
			budgetPK =  EJBUtil.stringToNum(budgetcalB.getBudgetId());
			fk_protocol =  EJBUtil.stringToNum(budgetcalB.getBudgetProtId());	
			
			budgetB.setBudgetId(budgetPK);
			budgetB.getBudgetDetails();
			
			budgetDefaultCalendar=EJBUtil.stringToNum(budgetB.getBudgetDefaultCalendar());
			
			boolean isItDefaultCalendarForBudgetAsWell = false;
			
			if (budgetDefaultCalendar == fk_protocol)
			{
				isItDefaultCalendarForBudgetAsWell = true;
			}
			
			String fkevent = "";
			String fkvisit = "";
			
			LineitemDao lineitemDao = lineitemB.getLineitemsOfBgtcal(bgtcalId);
			
			lineitemIds = lineitemDao.getLineitemIds(); 
			
            lineitemNames = lineitemDao.getLineitemNames();
			
			bgtSectionIds = lineitemDao.getBgtSectionIds();
			
			bgtSectionNames = lineitemDao.getBgtSectionNames();
			
			ArrayList arFKVisits = null;
			ArrayList arFKEvents = null;
			
			arFKVisits = lineitemDao.getArFKVisits();
             arFKEvents = lineitemDao.getArFkEvents();
             
			int count = 0 ;
			int lineitemId = 0;
		    boolean show = false;
			
			for(int j = 0; j<lineitemIds.size(); j++)
			{
				lineitemId = ((Integer)lineitemIds.get(j)).intValue();
				
				if(lineitemId > 0 ){
					fkevent = (String )arFKEvents.get(j);
					fkvisit = (String )arFKVisits.get(j);
					
					if (StringUtil.isEmpty(fkevent ))
					{
						fkevent = "";
					}
					
					if (StringUtil.isEmpty(fkvisit ))
					{
						fkvisit = "";
					}
				
					if (from.equals("combBgt")){
						if (StringUtil.isEmpty(fkevent)){
							count ++;						
						}//include lineitem
					}else{
						if ( (!isItDefaultCalendarForBudgetAsWell) || (isItDefaultCalendarForBudgetAsWell && StringUtil.isEmpty(fkevent) ))
						{
							count ++;					
						}//include lineitem
					}
				}
				
			}
			if(count == 0){%>
				<br><br><br><P class="successfulmsg" align="center"><%=MC.M_NoLineItem_CanDel%><%--There are no Lineitems that can be deleted*****--%></P>
								
				<%}
				else{
			int totalRows = lineitemDao.getBRows();

			
		
			String lineitemName = "";

			int	bgtSectionId = 0;
		
			String bgtSectionName = "";

			int bgtSectionIdOld = 0;
			
			bgtSectionIdOld = ((Integer) lineitemIds.get(0)).intValue();
			
			for(int i=0 ; i<totalRows ; i++)
			{
				show=false;
				lineitemId = ((Integer) lineitemIds.get(i)).intValue();
		
				if(lineitemId > 0 ){
					////////////
					fkevent = (String )arFKEvents.get(i);
					fkvisit = (String )arFKVisits.get(i);
					
					if (StringUtil.isEmpty(fkevent ))
					{
						fkevent = "";
					}
					
					if (StringUtil.isEmpty(fkvisit ))
					{
						fkvisit = "";
					}
					
					if (from.equals("combBgt")){
						if(StringUtil.isEmpty(fkevent)){
							show=true;
						}//include lineitem
					}else{
						if ( (!isItDefaultCalendarForBudgetAsWell) || (isItDefaultCalendarForBudgetAsWell && StringUtil.isEmpty(fkevent) ))
						{
						 	show =true;						
						}//include lineitem
					}	
					if (show == true){
						///////////
						lineitemName = (String) lineitemNames.get(i);
		
						bgtSectionId = ((Integer) bgtSectionIds.get(i)).intValue();		
				
						bgtSectionName = (String) bgtSectionNames.get(i);

						if(bgtSectionIdOld != bgtSectionId) {
							%><tr><td><B><%=bgtSectionName%></B></td></tr>
						<%}%>
							<tr><td>&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;<%=lineitemName%></td>
								<td><input type=checkbox name="chkItemId">
								<input type="hidden" name="lineitemId" value=<%=lineitemId%>></td></tr>
							<%
			
						bgtSectionIdOld = bgtSectionId;
					}
				} //lineitem > 0
			}
	%>	
		</tr>
		</table>
		<%if(ienet == 0) {
%>
		<table width="77%" cellspacing="0" cellpadding="0" border="0">
		<%} else { %>
		<table width="67%" cellspacing="0" cellpadding="0" border="0">
		<%} %>
		<tr> <td width="60%">
				<jsp:include page="submitBar.jsp" flush="true"> 
					<jsp:param name="displayESign" value="Y"/>
					<jsp:param name="formID" value="delMultLineItem"/>
					<jsp:param name="showDiscard" value="N"/>
					<jsp:param name="noBR" value="Y"/>
				 </jsp:include>
		</td>
		</tr>
		</table>
	
	<input type="hidden" name = "count" value=<%=count%> >
	<br>

	<br>
	
	
<input type=hidden name=selLineitemIds>
  </Form>
  
<%
			}//count >0
	} //end of if body for page right
	else
	{
%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
<%

	} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
<%

}

%>
 <div class = "myHomebottomPanel"> 
 <jsp:include page="bottompanel.jsp" flush="true"/>
 </div>
 </div>
</body>

</html>


		
	
		
		
		
			
		
		
		
	




