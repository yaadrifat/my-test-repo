<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.services.map.ObjectMap, com.velos.services.map.ObjectMapService"%>
<%@page import="com.velos.services.util.ServicesUtil" %>
<%@page import="com.velos.eres.web.submission.SubmissionStatusJB" %>
<%@page import="com.velos.eres.widget.business.common.UIFlxPageDao"%>
<%@page import="com.velos.eres.business.common.StudyVerDao"%>
<%@page import="com.velos.eres.business.common.StudyDao"%>
<jsp:useBean id="complianceJB" scope="request" class="com.velos.eres.compliance.web.ComplianceJB"/>
<jsp:useBean id="studyJB" scope="session" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="statHistoryB" scope="request" class="com.velos.eres.web.statusHistory.StatusHistoryJB"/>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="include.jsp" flush="true"/>
<%HttpServletResponse httpResponse = (HttpServletResponse) response;
httpResponse.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1
httpResponse.setHeader("Pragma", "no-cache"); // HTTP 1.0
httpResponse.setDateHeader("Expires", 0); %>
<html>
<head>

<%!
static final String STR_TRUE = "true", STR_FALSE = "false";
%>
<% 
String studyOID = null;
String isPopupWin = "0";
String isPrint ="0";
if(request.getParameter("isPrint")!=null){
	isPrint=request.getParameter("isPrint");
}
String mode= request.getParameter("mode")==null?"":request.getParameter("mode");
if(request.getParameter("isPopupWin")!=null){
	isPopupWin = request.getParameter("isPopupWin");
	System.out.println("isPopupWin---->"+isPopupWin);
}

boolean isIrb = "irb_upload_tab".equals(request.getParameter("selectedTab")) ? true : false;
String includeTabsJsp = isIrb ? ("LIND".equals(CFG.EIRB_MODE) ? "ecompNewTabs.jsp" : "irbnewtabs.jsp") : "studytabs.jsp";
String jspName = isIrb
	? (("LIND".equals(CFG.EIRB_MODE)) ? "studyAttachmentsLindFt" : "studyVerBrowser_print.jsp")
	: (("LIND".equals(CFG.EIRB_MODE)) ? "studyAttachments" : "studyVerBrowser_print.jsp");

%>
<title><%=LC.L_Std_Ver%><%--<%=LC.L_Study%> >> Versions*****--%></title>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<% String src;
src= request.getParameter("srcmenu");
%>
<%if (!"LIND".equals(CFG.EIRB_MODE) || isPopupWin.equals("1")){%>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
<jsp:param name="isPopupWin" value="<%=isPopupWin%>"/>
</jsp:include>
<%} %>
<SCRIPT language="javascript">
var windowName;
var screenWidth = screen.width;
var screenHeight = screen.height;
function fdownload(formobj,pk,filename,statuspk,studyno,statusDesc)
{
	
	formobj.file.value = filename;
	formobj.pkValue.value = pk ;
	formobj.moduleName.value="Study";
	formobj.action=formobj.dnldurl.value;
	//formobj.action="postFileDownload.jsp?statuspk="+statuspk+"&studyno="+studyno+"&statusDesc="+statusDesc;
	//formobj.action=dnldurl;
	formobj.target = "myActionWin"
	
	//formobj.target = "_blank";
	formobj.method = "POST";
	window.open("","myActionWin","top=50,left=50,width=600,height=600,toolbar=0");
	formobj.submit();
}

var popCatNewLink = function popCatNewLink(oid) {
	window.open("<%=CFG.EIRB_CAT_NEW_LINK%>"+oid);
};

var popCatViewLink = function (oid) {
	window.open("<%=CFG.EIRB_CAT_VIEW_LINK%>"+oid);
};

function setOrder(formobj,orderBy)
{

	if (formobj.orderType.value=="desc") {
		formobj.orderType.value= "asc";
	} else 	if (formobj.orderType.value=="asc") {
		formobj.orderType.value= "desc";
	}

	formobj.orderBy.value= orderBy;


	formobj.submit();
}

function closeChildWindow() {
	if (windowName != null) {
		windowName.close();
	}
}

function confirmBox(ver,pgRight,num,category) {
	if (windowName != null) {
		windowName.close();
	}

	if (f_check_perm(pgRight,'E') == true) {
		if (num == 1)
		{
			//Added by Ganapathy on 04-15-05
			//alert("The default version can not be deleted.");
			alert("<%=MC.M_ThisVerStd_YouNotDel%>");/*alert("This is the only version left for this <%=LC.Std_Study_Lower%>. You can not delete this version.");*****/
			return false;
		}
		var paramArray = null;
		if (category) {
			paramArray = [category];
			msg = "Delete "+category+" category?";
		} else {
			paramArray = [ver];
			msg=getLocalizedMessageString("L_Del_StdVer",paramArray);/*msg="Delete <%=LC.L_Study%> Version " + ver+ "?";*****/
		}
		if (confirm(msg))
		{
    		return true;
		}
		else
		{
			return false;
		}
	} else {
		return false;
	}
}


</SCRIPT>
<body>

<div id="overDiv" style="visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<%
	String from = "version";
	String tab = request.getParameter("selectedTab");

	String studyIdForTabs = "";
 	studyIdForTabs = request.getParameter("studyId");
	if (studyIdForTabs == null) {
 		if (request.getSession(false) != null) {
 			studyIdForTabs = String.valueOf(request.getSession(false).getAttribute("studyId"));
 		}
 	}
 	
%>

<%
HttpSession tSession = request.getSession(true);


%>
<%
String stid= request.getParameter("studyId");
System.out.println("stid:::"+stid);
if("LIND".equals(CFG.EIRB_MODE) && !"N".equals(request.getParameter("mode"))){%>

<% } else if(stid==null || stid=="" || "0".equals(stid)) {%>
<SCRIPT LANGUAGE="JavaScript">
if(screenWidth>1280 || screenHeight>1024)
		document.write('<DIV class="BrowserBotN BrowserBotN_RC_1" id="div1"  style="border:0">')            //Done by Siddharth for #20709 bug
	else
		document.write('<DIV class="BrowserBotN BrowserBotN_RC_1" id="div1">')
</SCRIPT>
	<%} else {%>
<DIV  id="div1" style="top:128px;height:500px;overflow:auto;">
<SCRIPT LANGUAGE="JavaScript">
//$j("#div1").css("height","");
$j("#div1").css("position","");
//document.getElementById("div1").style.overflow="hidden";
	</SCRIPT>
<%}%>
	<jsp:useBean id="studyVerB" scope="request" class="com.velos.eres.web.studyVer.StudyVerJB"/>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<jsp:useBean id="appendixB" scope="request" class="com.velos.eres.web.appendix.AppendixJB"/>
	<%@ page language = "java" import = "com.velos.eres.business.ulink.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.EJBUtil"%>
	<%

	if (sessionmaint.isValidSession(tSession))
	{
		String studyId ="";
		if(isPopupWin.equals("1")){
			studyId = request.getParameter("studyId");
		}else{
			studyId = (String) tSession.getValue("studyId");
		}
		
		
		String usrId ;
		usrId = (String) tSession.getValue("userId");

		String uName = (String) tSession.getValue("userName");
		String defUserGroup = (String) tSession.getAttribute("defUserGroup");

		double pageRight = 0;
		int pageRightApndx = 0;

        if(studyId == "" || studyId == null || "0".equals(studyId.trim())) {
	%>
	  <jsp:include page="studyDoesNotExist.jsp" flush="true"/>
	  <%
	  } else {
			UIFlxPageDao uiFlxPage = new UIFlxPageDao();
		  	ArrayList<String> lockedVersions = uiFlxPage.getAllLockedStudyVersions(StringUtil.stringToNum(studyId));
		  	for (String version: lockedVersions){
		  		System.out.println("Locked Version::::"+version);
		  	}
	    	
		// change by salil  moving the closing bracket of else
	     ///change by salil on 15-sep-2003

		// int viewVer=0;
		 //int  secRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYSEC"));
		// int  apndxRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYAPNDX"));

		// if(secRight >=4 && apndxRight >=4){ viewVer = 4;}

	   
		    // Get or create OID of study to be passed to CAT 
			ObjectMapService objectMapService = ServicesUtil.getObjectMapService();
			if("LIND".equals(CFG.EIRB_MODE)) {
				ObjectMap objectMap = objectMapService.getOrCreateObjectMapFromPK("er_study", StringUtil.stringToInteger(studyIdForTabs));
				//System.out.println("Study OID="+objectMap.getOID());
				studyOID = objectMap.getOID();
		    }


		//JM: 11/17/2005
			String orderType = "";
			orderType = request.getParameter("orderType");
			//if (orderType==null) orderType="";
			String submissionDate = "";
			String approvalDate = "";
			
			if(request.getParameter("submissionDate")!=null){
				submissionDate = request.getParameter("submissionDate");
			}

			if(request.getParameter("approvalDate")!=null){
				approvalDate = request.getParameter("approvalDate");
			}
			
			if (EJBUtil.isEmpty(orderType))
			{
			orderType = "desc";
			}

			String orderBy = "";
			orderBy = request.getParameter("orderBy");
			if (orderBy==null)		orderBy="";
			if (EJBUtil.isEmpty(orderBy))
			orderBy = "PK_STUDYVER";






			String ddStudyvercat = "" ;
			CodeDao cd1=new CodeDao();
			cd1.getCodeValues("studyvercat");
			cd1.setCType("studyvercat");
	        cd1.setForGroup(defUserGroup);


			String studyvercat=request.getParameter("dStudyvercat");
			if (studyvercat==null) studyvercat="";


			

			String ddStudyvertype = "" ;
			CodeDao cd2=new CodeDao();
			cd2.getCodeValues("studyvertype");

			cd2.setCType("studyvertype");
	        cd2.setForGroup(defUserGroup);

			String studyvertype=request.getParameter("dStudyvertype");
			if (studyvertype==null) studyvertype="";

			if (studyvertype.equals("")){
			ddStudyvertype=cd2.toPullDown("dStudyvertype");
			}
			else{
			ddStudyvertype=cd2.toPullDown("dStudyvertype",EJBUtil.stringToNum(studyvertype),true);
			}


			String ddVersionStatus = "" ;
			CodeDao cd3=new CodeDao();
			cd3.getCodeValues("versionStatus");
			cd3.setCType("versionStatus");
	        cd3.setForGroup(defUserGroup);


			String versionStatus=request.getParameter("dVersionStatus");
			if (versionStatus==null) versionStatus="";

			if (versionStatus.equals("")){
			ddVersionStatus=cd3.toPullDown("dVersionStatus");
			}
			else{
			ddVersionStatus=cd3.toPullDown("dVersionStatus",EJBUtil.stringToNum(versionStatus),true);
			}

			String	dStudyvercat =	request.getParameter("dStudyvercat");
			String dStudyvertype =  request.getParameter("dStudyvertype");
			String dVersionStatus = request.getParameter("dVersionStatus");
		
			String appSubmissionType = request.getParameter("appSubmissionType");





			String studyVerNumber = request.getParameter("studyVerNumber");
			if (studyVerNumber==null) studyVerNumber="";

			String verCat = request.getParameter("dStudyvercat");
			if (verCat==null) verCat="";
			String verType = request.getParameter("dStudyvertype");
			if (verType==null) verType="";

			String verStat = request.getParameter("dVersionStatus");
			if (verStat==null) verStat="";


	%>
	<%if ("LIND".equals(CFG.EIRB_MODE) && !isPopupWin.equals("1")){ %>
		<Form name="studyverbrowserSearh" id="studyverbrowserSearh" method="post" action="<%=jspName%>?selectedTab=irb_upload_tab&mode=M&srcmenu=tdmenubaritem3&studyId=<%=studyId%>" onsubmit="">
	<%} else { %>
		<Form name="studyverbrowserSearh" id="studyverbrowserSearh" method="post" action="studyVerBrowser_print.jsp?studyId=<%=studyId%>" onsubmit="">
		<%if(isPopupWin.equals("1")){ %>
					<Input type="hidden" name="mode" value="N">
					<input type="hidden" name="isPopupWin" value=<%=isPopupWin %>>
					<input type="hidden" name="studyId" value=<%=studyIdForTabs %>>
				<%}%>
	<%} %>
				<Input type="hidden" name="selectedTab" value="<%=tab%>">


<%if(isPopupWin.equals("1")){
	String studyNumber=null;
    String studyTitle=null;
    String versionNum = "0";
    
    if(!studyId.equals("")){
    	studyJB.setId(EJBUtil.stringToNum(studyId));
    	studyJB.getStudyDetails();
		studyNumber = studyJB.getStudyNumber();
		studyTitle = studyJB.getStudyTitle();
		UIFlxPageDao uiFlxPageDao = new UIFlxPageDao();
		uiFlxPageDao.getHighestFlexPageFullVersion("er_study", Integer.parseInt(studyId));

		int versionMajor = uiFlxPageDao.getMajorVer();
		versionNum = String.valueOf(versionMajor);
		if (versionMajor > 0){
			versionNum += ".";
			int versionMinor = uiFlxPageDao.getMinorVer();
			versionNum += (versionMinor == 0)? "00" : ((versionMinor > 9)? versionMinor : "0" + versionMinor);
		}
    }
%>
<script language=javascript>
		var study_Title = htmlEncode('<%=studyTitle%>');
</script>

<%} %>

</form>
<%






				


//+++++++++++++++++++++++++++++++++++++JM pagination+++++++++++++++++++++++++++++++++++++++++++++//


		int curPage = 0;
		long startPage = 1;
		String stPage;
		long cntr = 0;
		String pagenum  = request.getParameter("page");


		if (pagenum == null)
		{
			pagenum = "1";
		}

		curPage = EJBUtil.stringToNum(pagenum);

		String count1 = "";
		String count2 = "";
		String countSql = "";
		String formSql = "";


		String sWhere = "";
        String vcWhere = "";
        String vtWhere = "";
        String vstWhere = "";
        String obWhere = "";
        String otWhere = "";
        String vcdWhere = "";
        String npWhere = "";


        StringBuffer sqlBuffer = new StringBuffer();
       // int intStudyId =EJBUtil.stringToNum(studyId);





	 	String mysqlSelect = "SELECT PK_STUDYVER,"
                    + " (select count(*) from er_studysec where fk_studyver = pk_studyver ) secCount, "
                    + " (select count(*) from er_studyapndx where fk_studyver = pk_studyver) apndxCount, "
                    + " case when (SELECT COUNT(*) FROM er_studyapndx WHERE fk_studyver = pk_studyver)>0 then "
                    + " PRINT_APNDX_DOCS(apndx.STUDYAPNDX_URI,apndx.PK_STUDYAPNDX,PK_STATUS, "
                    + "	    (SELECT study_number FROM er_study WHERE pk_study=a.FK_STUDY "
                    + "	    ),CODELST_DESC) "
                    + "	  else "
                    + "	    ' ' "
                    + "	  end "
                    + "	  apndxNames, "
                    + " apndx.STUDYAPNDX_URI file_name, "
                    + " apndx.STUDYAPNDX_DESC file_desc, "
                    + " a.FK_STUDY,"
                    + " STUDYVER_NUMBER,studyver_date,TO_CHAR(STUDYVER_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) as STUDYVER_DATE_STR,"
                    + " (select codelst_desc from er_codelst where pk_codelst=a.STUDYVER_CATEGORY) as STUDYVER_CATEGORY,"
                    + " (select codelst_desc from er_codelst where pk_codelst=a.STUDYVER_TYPE) as STUDYVER_TYPE,"
                    + " STUDYVER_NOTES,"
                    + " a.CREATOR,"
                    + " a.IP_ADD, CODELST_DESC STATUS_CODEDESC, CODELST_SUBTYP STATUS_CODESUBTYP,PK_STATUS,ROWNUM "
                    + " FROM ER_STUDYVER a , ER_STUDYAPNDX apndx, ( Select CODELST_DESC, CODELST_SUBTYP, PK_STATUS,STATUS_MODPK, FK_CODELST_STAT  from "
                    + " er_studyver i, ER_STATUS_HISTORY, er_codelst c Where i.fk_study = "+EJBUtil.stringToNum(studyId)
                    + " and i.PK_STUDYVER =  STATUS_MODPK and STATUS_MODTABLE =  'er_studyver'  "
                    + " and STATUS_END_DATE is null and c.pk_codelst = FK_CODELST_STAT ) stat "
                    + " WHERE a.FK_STUDY = " + EJBUtil.stringToNum(studyId)
                    + " AND FK_STUDYVER (+) = pk_studyver "
                    + " and STATUS_MODPK (+) = pk_studyver "
                    + " and STUDYVER_NUMBER <> '0.00' "
                    + " AND PK_STUDYVER in (select fk_studyver from er_studyapndx where studyapndx_fileobj is not null)";
                    
                if(!appSubmissionType.equals("prob_rpt") && !appSubmissionType.equals("cont_rev") && !appSubmissionType.equals("closure")){    
                  	npWhere = " AND stat.CODELST_SUBTYP <> 'NP'";
                }


				if ((studyVerNumber!= null) && (! studyVerNumber.trim().equals("") && ! studyVerNumber.trim().equals("null")))
				{
					sWhere = " and upper(a.STUDYVER_NUMBER) like upper('%";
					sWhere+=studyVerNumber.trim();
					sWhere+="%')";
				}

				vcWhere = " and a.STUDYVER_CATEGORY not in (select fk_codelst from er_codelst_hide where fk_grp = " +defUserGroup +" and codelst_type='studyvercat' and codelst_hide_table  = 1)";

				if ((verCat!= null) && (! verCat.trim().equals("") && ! verCat.trim().equals("null")))
				{
					if(!verCat.contains(",")){
						vcWhere = vcWhere  + " and upper(a.STUDYVER_CATEGORY) = upper(";
						vcWhere+=verCat;
						vcWhere+=")";
					}else{
						vcWhere = vcWhere  + " and a.STUDYVER_CATEGORY in (";
						vcWhere+=verCat;
						vcWhere+=")";
					}
				}

				if(!submissionDate.equals("") && !approvalDate.equals("")){
					vcdWhere = " and apndx.created_on BETWEEN TO_DATE('"+submissionDate+"', PKG_DATEUTIL.F_GET_DATETIMEFORMAT)"+
							   " AND TO_DATE('"+approvalDate+"', PKG_DATEUTIL.F_GET_DATETIMEFORMAT)";
				}else if(!submissionDate.equals("")){
					vcdWhere = " and apndx.created_on >= TO_DATE('"+submissionDate+"', PKG_DATEUTIL.F_GET_DATETIMEFORMAT)";
				}

				vtWhere = " and ( a.STUDYVER_TYPE is null or a.STUDYVER_TYPE not in (select fk_codelst from er_codelst_hide where fk_grp = " +defUserGroup +" and codelst_type='studyvertype' and codelst_hide_table  = 1) ) ";

				if ((verType!= null) && (! verType.trim().equals("") && ! verType.trim().equals("null")))
				{
					vtWhere = vtWhere + " and a.STUDYVER_TYPE = UPPER(" ;
					vtWhere+=verType;
					vtWhere+=")";
				}

				vstWhere = " and FK_CODELST_STAT not in (select fk_codelst from er_codelst_hide where fk_grp = " +defUserGroup +" and codelst_type='versionStatus' and codelst_hide_table  = 1)";

				if ((verStat!= null) && (! verStat.trim().equals("") && ! verStat.trim().equals("null")))
				{
					vstWhere = vstWhere + " and FK_CODELST_STAT = UPPER(" ;
					vstWhere+=verStat;
					vstWhere+=")";
				}
				if ((orderBy!= null) && (! orderBy.trim().equals("") && ! orderBy.trim().equals("null")))
				{
					obWhere = " order by " +orderBy+ " " + orderType;

				}
			//	else{
			//
			//		obWhere = " order by PK_STUDYVER DESC ";
			//	}
				
				System.out.println("mysqlSelect--->"+mysqlSelect);

				sqlBuffer.append(mysqlSelect);

				if(!appSubmissionType.equals("prob_rpt") && !appSubmissionType.equals("cont_rev") && !appSubmissionType.equals("closure")){
	            	sqlBuffer.append(npWhere);
	            }
	            if(sWhere!=null){
	            	sqlBuffer.append(sWhere);
	            }
	            if(vcWhere !=null){
	            	sqlBuffer.append(vcWhere );
	            }
	            if(vcdWhere !=null && !vcdWhere.equals("")){
	            	sqlBuffer.append(vcdWhere );
	            }
	            if(vtWhere!=null){
	            	sqlBuffer.append(vtWhere);
	            }
	            if(vstWhere !=null){
	            	sqlBuffer.append(vstWhere );
	            }
	            if(orderBy!=null){
	            	sqlBuffer.append(obWhere);
	            }

	            formSql=sqlBuffer.toString();
	            //out.println(formSql);
				System.out.println("formSql--->"+formSql);

				count1 = "select count(*) from  ( " ;
				count2 = ")"  ;
				countSql = count1 + formSql + count2 ;

				long rowsPerPage=0;
				long totalPages=0;
				long rowsReturned = 0;
				long showPages = 0;

			    boolean hasMore = false;
			    boolean hasPrevious = false;
			    long firstRec = 0;
			    long lastRec = 0;
			    long totalRows = 0;
			    long actualRec=0;

			    String type ="" ;
			    String type1="";

			    rowsPerPage =  500 ;
			    totalPages =Configuration.PAGEPERBROWSER ;



		        BrowserRows br = new BrowserRows();
		        br.getPageRows(curPage,rowsPerPage,formSql ,totalPages,countSql,orderBy,orderType);
		   	    rowsReturned = br.getRowReturned();
			    showPages = br.getShowPages();
			    startPage = br.getStartPage();
			    hasMore = br.getHasMore();
			    hasPrevious = br.getHasPrevious();
			    totalRows = br.getTotalRows();
			    firstRec = br.getFirstRec();
			    lastRec = br.getLastRec();

				String verStatus = null;
				String popCrfString = "";
				AppendixDao appendixDao =new AppendixDao();
				AppendixDao appendixDao1 =new AppendixDao();
				int counter = 0;
//+++++++++++++++++++++++++++++++++++++JM pagination+++++++++++++++++++++++++++++++++++++++++++++//


			%>


<div class="tmpHeight"></div>

	<%if ("LIND".equals(CFG.EIRB_MODE) && isPopupWin.equals("0")){ %>
	<form name="studyScreenForm">
		<input type="hidden" name="formStudy" value="<%=studyId %>" />
	</form>
	<Form name="studyverbrowser" method="post" action="<%=jspName%>?selectedTab=irb_upload_tab&mode=M&srcmenu=tdmenubaritem3&studyId=<%=studyId%>" onsubmit="">
	<%} else { %>
	<Form name="studyverbrowser" method="post" action="studyVerBrowser_print.jsp" onsubmit="">
	<%} %>
				<Input type="hidden" name="selectedTab" value="<%=tab%>">
				<Input type="hidden" name="orderType" value="<%=orderType%>">
				<Input type="hidden" name="orderBy" value="<%=orderBy%>">
				<Input type="hidden" name="page" value="<%=pagenum%>">
				
				<input type="hidden" name="rowsReturned" value=<%=rowsReturned%>>
				<input type="hidden" name="verCat" value=<%=verCat%>>
				<input type="hidden" name="verType" value=<%=verType%>>
				<input type="hidden" name="verStat" value=<%=verStat%>>


				<input type="hidden" name="dStudyvercat" value=<%=dStudyvercat%>>
				<input type="hidden" name="dStudyvertype" value=<%=dStudyvertype%>>
				<input type="hidden" name="dVersionStatus" value=<%=dVersionStatus%>>
				<input type="hidden" name="studyVerNumber" value=<%=studyVerNumber%>>
				<input type="hidden" name="studyId" value=<%=studyIdForTabs%>>
				<input type="hidden" name="appSubmissionType" value=<%=appSubmissionType %>>
				<%if(isPopupWin.equals("1")){ %>
					<Input type="hidden" name="mode" value="N">
					<input type="hidden" name="isPopupWin" value=<%=isPopupWin %>>
				<%}else{ %>
					<Input type="hidden" name="mode" value="M">
				<%} %>
				<table id="versionsTbl" class="outline midAlign" width="99%" style="table-layout:fixed" cellpadding="0" cellspacing="0" >
					<tr>
						<th width="10%" style="max-width:50px;border-right:1px solid #9d9d9d;color:#696969;height:30px;"><%=LC.L_Version%></th>	<!--Mukul: To fix Bug 4274  -->
						
						<th width="20%" style="border-right:1px solid #9d9d9d;color:#696969;height:30px;" onClick="setOrder(document.studyverbrowser,'lower(STUDYVER_CATEGORY)')"> <%=LC.L_Category%><%--Category*****--%> &loz;</th>
						
						
						<th width="20%" style="border-right:1px solid #9d9d9d;color:#696969;height:30px;" onClick="setOrder(document.studyverbrowser,'lower(FILE_NAME)')"> <%=LC.L_Appendix%><%--Appendix*****--%> &loz; </th>
						<th width="35%" style="border-right:1px solid #9d9d9d;color:#696969;height:30px; min-width:300px" onClick="setOrder(document.studyverbrowser,'lower(FILE_DESC)')"> <%=LC.L_Attach_Description%><%--Type*****--%> &loz; </th>
						<th width="14%" style="border-right:1px solid #9d9d9d;color:#696969;height:30px;" onClick="setOrder(document.studyverbrowser,'lower(STATUS_CODEDESC)')"> <%=LC.L_Attachment_Status%><%--Attachment Status*****--%> &loz; </th>
						
						
					</tr>
					<%




int i = 0;

	for(i = 1 ; i <=rowsReturned ; i++){


			int studyVerId = EJBUtil.stringToNum(br.getBValues(i,"PK_STUDYVER"));
			String studyVerStudyId = br.getBValues(i,"FK_STUDY");//
						  studyVerStudyId=(studyVerStudyId==null)?"":studyVerStudyId;
			String verNumber = br.getBValues(i,"STUDYVER_NUMBER");
			if(verNumber.equals("0") || verNumber.equals("0.00")) { continue; }
			
			verNumber=(verNumber==null)?"":verNumber;
			String statusSubType = br.getBValues(i,"STATUS_CODESUBTYP");
						  statusSubType=(statusSubType==null)?"":statusSubType;
			String statusDesc = br.getBValues(i,"STATUS_CODEDESC");
						  statusDesc=(statusDesc==null)?"":statusDesc;
			String statusPk = br.getBValues(i,"PK_STATUS");
						  statusPk=(statusPk==null)?"":statusPk;
			String studyVerNote = br.getBValues(i,"STUDYVER_NOTES");//
						  studyVerNote=(studyVerNote==null)?"":studyVerNote;
			int    secCount = EJBUtil.stringToNum(br.getBValues(i,"secCount"));
			int    apndxCount = EJBUtil.stringToNum(br.getBValues(i,"apndxCount"));
			String apndxNames = "";
			if(br.getBValues(i,"apndxNames")!=null){
				apndxNames=br.getBValues(i,"apndxNames");
			}
			String apndxDesc = "";
			if(br.getBValues(i,"file_desc")!=null){
				apndxDesc=br.getBValues(i,"file_desc");
			}
			int rowNum = EJBUtil.stringToNum(br.getBValues(i,"rownum"));

			String studyVerDate	  =	br.getBValues(i,"STUDYVER_DATE_str");
						  studyVerDate=(studyVerDate==null)?"-":studyVerDate;
			String studyVerCat = br.getBValues(i,"STUDYVER_CATEGORY");
						  studyVerCat=(studyVerCat==null)?"-":studyVerCat;
			String studyVerType = br.getBValues(i,"STUDYVER_TYPE");
						  studyVerType=(studyVerType==null)?"-":studyVerType;



		    appendixDao=appendixB.getByStudyIdAndType(studyVerId, "url");
			ArrayList appendixFile_Uris = appendixDao.getAppendixFile_Uris();


			appendixDao1=appendixB.getByStudyIdAndType(studyVerId, "file");
			ArrayList appendixFile_Uris1 = appendixDao1.getAppendixFile_Uris();
			int uriLen = appendixFile_Uris.size();
			int uriLen1 = appendixFile_Uris1.size();
			    popCrfString = "";
			    popCrfString = popCrfString+"<table width=300px cellspacing=0 cellpadding 0>";	

				if(uriLen==0)
						{
							popCrfString = popCrfString + "<tr><td colspan=2 align=left><B>"+LC.L_NoUrls_Attch+"</B><br></td><tr>";/*popCrfString = popCrfString + "<B>No URLs attached</B>";*****/
						}
				else{
					popCrfString = popCrfString + "<tr><td colspan=2 align=left>&#9679;&nbsp;<B>"+LC.L_My_Urls+"</B><br></td><tr>";/*popCrfString = popCrfString + "<LI><B>My URLs</B><UL>";*****/
						for (int k=0;k<uriLen;k++) {
							popCrfString = popCrfString + "<tr><td align=right width=15px >&nbsp;&nbsp;&nbsp;&#9675;</td><td align=left>" + appendixFile_Uris.get(k) + "</td></tr>";
						}
 	    			}
					if(uriLen1==0)
						{
						if(uriLen!=0)
								popCrfString = popCrfString + "" ;
							popCrfString = popCrfString + "<tr><td colspan=2 align=left><br><B>"+MC.M_NoFilesAttached+"</B><br></td><tr>";/*popCrfString = popCrfString + "<br><B>No Files attached</B>";*****/
						}
						else{
							if(uriLen!=0)
								popCrfString = popCrfString + "" ;

				popCrfString = popCrfString + "<tr><td colspan=2 align=left>&#9679;&nbsp;<B>"+LC.L_My_Files+"</B><br></td><tr>" ;/*popCrfString = popCrfString + "<LI><B>My Files</B><UL>" ;*****/
				for(int k1=0;k1<uriLen1;k1++){
					popCrfString = popCrfString + "<tr><td align=right width=15px >&nbsp;&nbsp;&nbsp;&#9675;</td><td align=left>" + appendixFile_Uris1.get(k1) + "</td></tr>";
				}
						}
				popCrfString = popCrfString+"</table>";
				//popCrfString = popCrfString + "</UL></UL>";



						if(statusSubType.equals("F")) {
							verStatus = "Freeze";
						} else
						{
						 verStatus = "";
						}


						if ((i%2)==0) {
						%>
							<tr class="browserEvenRow">
				        <%
						}
						else{
						%>
						    <tr class="browserOddRow">
				        <%
						}
						%>

						
							<td width="8%" style="word-wrap:normal"><B><%= verNumber%></B></td>
					
						

						<td width="20%" style="word-wrap:normal"><B><%=studyVerCat%></B>
						</td>

						

						
        				<td width="22%" style="word-wrap:normal">
        				
        				
        					<%=apndxNames.toString() %>
        				
        				
				        </td>
				        <td width="35%" style="min-width:250px;word-wrap:normal"><B><%=apndxDesc%></B>
						</td>
						<td width="14%" style="word-wrap:normal">

													 
							 <B><%= statusDesc%></B>
						
						</td>
						
						

					</tr>
				    <%

			 		} //end of for loop

					%>

				</table>
				<%if (isPopupWin.equals("1")){ %>
					<script>
				
						$j(document).find("#versionsTbl > tbody").addClass("isPopupWin");
					</script>
				<%} %>
				
					<script>
				
					
						//$j(document).find("#versionsTbl > tbody").css({"height","600px !important"});
					</script>
				
			<% if (rowsReturned == 0){%>
				<script>
				
				$j(document).find("#versionsTbl > tbody").addClass("NoScrollCont");
				</script>
			<%} %>	
<!--/////////////////////////JM pagination///////////////////////////////////////////////////-->
		<table >
		<tr><td>
		<% if (rowsReturned > 0)
		{
			Object[] arguments = {firstRec,lastRec,totalRows};
		%>
					<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%-- <font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
		<%} else {%>
			<font class="recNumber"> <%=MC.M_NoSrchResMatch_Criteria%><%--No search results matching given criteria found*****--%></font>
		<%}%>
		</td></tr>
		</table>
		<table align=center>
			<tr>
				<%	if (curPage==1) startPage=1;

				for ( int count = 1; count <= showPages;count++)
				{

		   				cntr = (startPage - 1) + count;

		   				if ((count == 1) && (hasPrevious))
						{
		   					if(isPopupWin.equals("1")){%>
		   						<td colspan = 2>
								<!-- km-to fix Bug 2442 -->
							  	<A href="studyVerBrowser_print.jsp?srcmenu=<%=src%>&isPopupWin=<%=isPopupWin %>&studyId=<%=studyId %>&page=<%=cntr-1%>&studyVerNumber=<%=studyVerNumber%>&dStudyvercat=<%=dStudyvercat%>&dStudyvertype=<%=dStudyvertype%>&dVersionStatus=<%=dVersionStatus%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&selectedTab=<%=tab%>&mode=N">< <%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%> > </A>
								&nbsp;&nbsp;&nbsp;&nbsp;
								</td>
		   					<%}else{%>
								<td colspan = 2>
								<!-- km-to fix Bug 2442 -->
							  	<A href="<%=jspName%>?srcmenu=<%=src%>&isPopupWin=<%=isPopupWin %>&studyId=<%=studyId %>&page=<%=cntr-1%>&studyVerNumber=<%=studyVerNumber%>&dStudyvercat=<%=dStudyvercat%>&dStudyvertype=<%=dStudyvertype%>&dVersionStatus=<%=dVersionStatus%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&selectedTab=<%=tab%>&mode=\"M\"">< <%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%> > </A>
								&nbsp;&nbsp;&nbsp;&nbsp;
								</td>
				
				<%		}}
				%>
					<td>
				<%
		 		 		if (curPage  == cntr)
				 		{
			    %>
								<FONT class = "pageNumber"><%= cntr %></Font>
		       	<%		}else
		       			{
		       			if(isPopupWin.equals("1")){%>
		       				<A href="studyVerBrowser_print.jsp?srcmenu=<%=src%>&isPopupWin=<%=isPopupWin %>&page=<%=cntr%>&studyVerNumber=<%=studyVerNumber%>&studyId=<%=studyId %>&dStudyvercat=<%=dStudyvercat%>&dStudyvertype=<%=dStudyvertype%>&dVersionStatus=<%=dVersionStatus%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&selectedTab=<%=tab%>&mode=N"><%= cntr%></A>
		       			<%}else{%>
		       				
								<A href="<%=jspName%>?srcmenu=<%=src%>&isPopupWin=<%=isPopupWin %>&page=<%=cntr%>&studyVerNumber=<%=studyVerNumber%>&studyId=<%=studyId %>&dStudyvercat=<%=dStudyvercat%>&dStudyvertype=<%=dStudyvertype%>&dVersionStatus=<%=dVersionStatus%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&selectedTab=<%=tab%>&appSubmissionType=<%=appSubmissionType %>&mode=\"M\""><%= cntr%></A>
		        
		        <%
		       			}
		        }
		        %>
					</td>
				<%
				}

				if (hasMore)
				{
					%>
					<td colspan = 3 align = center>&nbsp;&nbsp;&nbsp;&nbsp;
				    <A href="studyVerBrowser_print.jsp?mode=N&isPopupWin=<%=isPopupWin %>&page=<%=cntr+1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&studyId=<%=studyId %>	">< <%=LC.L_Next%><%--Next*****--%> <%=totalPages%>></A>
					</td>
				<%}
				%>

	   		</tr>
	    </table>

<!--///////////////////////////JM pagination/////////////////////////////////////////////////-->

	</Form>
<%
		
		}// end of else of study check
	}//end of if body for session
	else
	{
	%>
		<jsp:include page="timeout.html" flush="true"/>
	<%
	}
	%>
	<div>
    	<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
	
<%if("LIND".equals(CFG.EIRB_MODE)){ %>

        				<Form name="apendixbrowser" method="post" action="" onsubmit="">
        					<input type="hidden" name="file" value=""/>
        					<input type="hidden" name="pkValue" value=""/>
        					<input type="hidden" name="moduleName" value=""/>
        					<input type="hidden" name="tableName" value="ER_STUDYAPNDX">
    						<input type="hidden" name="columnName" value="STUDYAPNDX_FILEOBJ">
    						<input type="hidden" name="pkColumnName" value="PK_STUDYAPNDX">
    						<input type="hidden" name="db" value="eres">
    						<% 	String dnld;
							com.aithent.file.uploadDownload.Configuration.readSettings("eres");
							com.aithent.file.uploadDownload.Configuration.readUploadDownloadParam(com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD + "fileUploadDownload.xml", "study");		  
							dnld=com.aithent.file.uploadDownload.Configuration.DOWNLOADSERVLET ;
							//SV, 7/27/04, fix for bug #1201, where a file with embedded # sign will cause browser to look for a HTML section tag. 
							//Fix: Encode the file name here and servlets decodes it later. 
							String modDnld = dnld;%>
							<input type="hidden" name="dnldurl" value="<%=modDnld%>"/>
						</Form>


<%} %>
</DIV>
<div class ="mainMenu" id = "emenu" >
  <jsp:include page="getmenu.jsp" flush="true"/>
</div>

</body>

</html>