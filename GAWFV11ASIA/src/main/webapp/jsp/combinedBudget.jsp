<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<script language="JavaScript" src="budget.js"></script>
<head>
<!-- KV:Fixed Bug No.4888 Displayed Current Page Title -->
<title><%=LC.L_Study_Budget%><%--Study >> Budget*****--%></title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%
String mode =   request.getParameter("mode");

boolean isIrb = "irb_personnel_tab".equals(request.getParameter("selectedTab")) ? true : false; 

%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.studyRights.StudyRightsJB" %>
<jsp:useBean id="budgetB" scope="request"	class="com.velos.esch.web.budget.BudgetJB" />
<jsp:useBean id="userB" scope="session" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id="budgetUsersB" scope="request"	class="com.velos.esch.web.budgetUsers.BudgetUsersJB" />
<jsp:useBean id="studyB" scope="session" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="stdRightsB" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="grpB" scope="request"	class="com.velos.eres.web.group.GroupJB" />
<jsp:useBean id="ctrl" scope="request"	class="com.velos.eres.business.common.CtrlDao" />
<jsp:useBean id="grpRights" scope="request"	class="com.velos.eres.web.grpRights.GrpRightsJB" />

<%@ page language = "java" import = "com.velos.eres.business.common.*,com.velos.esch.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil, com.velos.eres.service.util.StringUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.*"%>
</head>

<%
	String src;
	src= request.getParameter("srcmenu");
	String from = "combBudget";

	String pageMode = request.getParameter("pageMode");
	if(pageMode == null) pageMode = "initial";

	String studyIdForTabs = "";
 	studyIdForTabs = request.getParameter("studyId");
 	
%>
<jsp:include page="panel.jsp" flush="true">
	<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<%
   HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession)){
	String studyId = request.getParameter("studyId");
	tSession.putValue("studyId", studyId);
	if(studyId== null) studyId = "";	

	String includeTabsJsp = isIrb ? "irbnewtabs.jsp" : "studytabs.jsp"; 
	%>
	<DIV class="BrowserTopn" id="divTop">    
	  <jsp:include page="<%=includeTabsJsp%>" flush="true">
	  <jsp:param name="from" value="<%=from%>"/>
	  	<jsp:param name="studyId" value="<%=studyIdForTabs%>"/>
	   </jsp:include>
	</DIV>
	<%
	if(studyId == "" || studyId == null|| "0".equals(studyId)||studyId.equalsIgnoreCase("")) {
		%> <DIV class="BrowserBotn BrowserBotN_S_1" id="divBot">   
				<jsp:include page="studyDoesNotExist.jsp" flush="true" /> 
			</DIV>
		<%
	} else {
	String budgetStudyId="";
	String bgtStat="", budgetTotalTotalCost="",budgetTotalCost="";
	SchCodeDao schD = new SchCodeDao();
	int perCodeId = schD.getCodeId("category","ctgry_per");

	String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = null;
	usr = (String) tSession.getValue("userId");
	
	String userIdFromSession = (String) tSession.getValue("userId");
	String acc = (String) tSession.getValue("accountId");
	
	int accId = EJBUtil.stringToNum(acc);
	int userId = EJBUtil.stringToNum(userIdFromSession);
	
	userB.setUserId(userId);
	userB.getUserDetails();

	String includedIn ="S";
	
	String selectedTab = request.getParameter("selectedTab");
	String selectedBgtcalId = "";
	
	int pageRight= 0;
	int appendxRight= 0;
	ArrayList bgtIds = new ArrayList();
	int budgetId = 0;
	
	ArrayList teamId = new ArrayList();
	TeamDao teamDao = new TeamDao();
	teamDao.getTeamRights(EJBUtil.stringToNum(studyIdForTabs),EJBUtil.stringToNum(userIdFromSession));
	teamId = teamDao.getTeamIds();

	if (teamId.size() <=0)
	{
		pageRight  = 0;
		StudyRightsJB stdRightstemp = new StudyRightsJB();
	}
	if (teamId.size() == 0) {
		pageRight=0 ;
	}else {
		stdRightsB.setId(EJBUtil.stringToNum(teamId.get(0).toString()));
	 	ArrayList teamRights ;
		teamRights = new ArrayList();
		teamRights = teamDao.getTeamRights();

		stdRightsB.setSuperRightsStringForStudy((String)teamRights.get(0));
		stdRightsB.loadStudyRights();

		if ((stdRightsB.getFtrRights().size()) == 0){
			pageRight= 0;
		}else{
			//checking study setup access unless a new access right for combined budget
			pageRight = Integer.parseInt(stdRightsB.getFtrRightsByValue("STUDYCAL"));
			appendxRight = pageRight;
		}
	}
	
	if (pageRight <=0){
		userB=(com.velos.eres.web.user.UserJB)tSession.getAttribute("currentUser");
		String defaultGrp=userB.getUserGrpDefault();
		grpB.setGroupId(EJBUtil.stringToNum(defaultGrp));
		grpB.getGroupDetails();
		
		String superUserBgtFlag = "";
		superUserBgtFlag= grpB.getGroupSupBudFlag();
		
		if (!superUserBgtFlag.equals("0")){
			String superRightStr = grpB.getGroupSupBudRights();
			int rightLen = superRightStr == null ? 0 : superRightStr.length();
	
			ctrl.getControlValues("bgt_rights");
	
			ArrayList feature =  ctrl.getCValue();
			ArrayList ftrDesc = ctrl.getCDesc();
			ArrayList ftrSeq = ctrl.getCSeq();
			ArrayList ftrRight = new ArrayList();
			String strR;
	
			for (int counter = 0; counter <= (rightLen - 1);counter ++)
			{
				strR = String.valueOf(superRightStr.charAt(counter));
				ftrRight.add(strR);
			}
	
			//grpRights bean has the methods for parding the rights so use its set methods
			grpRights.setGrSeq(ftrSeq);
	    	grpRights.setFtrRights(ftrRight);
			grpRights.setGrValue(feature);
			grpRights.setGrDesc(ftrDesc);
	
			//set the grpRights Bean in session so that it can be used in other
			//budget pages directly to get the pageRight.
	
			tSession.putValue("BRights",grpRights);
	
			if (rightLen > 0) {
				pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("BGTDET"));
				appendxRight = Integer.parseInt(grpRights.getFtrRightsByValue("BGTAPNDX"));
			}
		}
	}
	
	if (pageRight > 0){
		BudgetDao bgtDao = new BudgetDao(); 
		bgtDao.getCombBudgetForStudy(userId,EJBUtil.stringToNum(studyIdForTabs));
		
		bgtIds=bgtDao.getBgtIds(); 	
		
		if (bgtIds.size() > 0){
			budgetId = ((Integer) bgtIds.get(0)).intValue();	
		}
	}
%>
<body>
	<%
  	if(budgetId>0){
	    budgetB.setBudgetId(budgetId);
		budgetB.getBudgetDetails();
		budgetStudyId=budgetB.getBudgetStudyId();
	}%>
<!-- Condition to check configuration for workflow enabled -->
<% if("Y".equals(CFG.Workflows_Enabled) && (studyIdForTabs!=null && !studyIdForTabs.equals("") && !studyIdForTabs.equals("0"))){ %>
		<DIV class="BgtDivBottom workflowDivBig" id="divBottom" style="height:98%; top:150px">
<% } else{ %>
		<DIV class="BgtDivBottom" id="divBottom" style="height:98%;">
<% } %>
	
		<table border="0" width="100%" height="120%" cellpadding="3" cellspacing="3" class="midAlign"> 
			<tr> 
				<td valign="top" width="100%"> 
	<%
	if (pageRight > 0){		
		// change by salil cut and pasted in else part of check study null
		if(studyId == "" || studyId == null) {
		%>
		<jsp:include page="studyDoesNotExist.jsp" flush="true"/>
		<%
		} else {
	  	    if(budgetId>0){
		  	    budgetB.setBudgetId(budgetId);
				budgetB.getBudgetDetails();
				budgetStudyId=budgetB.getBudgetStudyId();
			}
			if(budgetId==0) {
				if(pageRight == 5 || pageRight == 7) {%> 
					<jsp:include page="newCombinedBudget.jsp" flush="true" />
				<%}else{%>
					<div class=formdefault>
						<BR/>
						<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0">
							<tr> 
								<td>
									<p class = "sectionHeadings"><font color="red"> <%=MC.M_YouCombPat_AccessRgt%><%--You do not have Combined Patient Budget Creation Access Right.*****--%> </font></p>
									<p class = "defComments"> <%=MC.M_ObtainRgt_RptErr%><%--To obtain appropriate rights or to report an error, please contact your account administrator.*****--%> <BR></p>
								</td>
							</tr>
						</table>
						<BR>
					</div>	
				<%} %>
			<%} else {			
				if (pageRight <= 0){%>
					<div class=formdefault>
						<BR/>
						<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0">
							<tr> 
								<td>
									<p class = "sectionHeadings"><font color="red"> <%=MC.M_ThereStdComb_NoAccess%><%--There is a Study level Combined Patient Budget defined. Unfortunately you do not have access to it.*****--%> </font></p>
									<p class = "defComments"> <%=MC.M_ObtainRgt_RptErr%><%--To obtain appropriate rights or to report an error, please contact your account administrator.*****--%> <BR></p>
								</td>
							</tr>
						</table>
						<BR>		
					</div>

				<%}else {%>
					<jsp:include page="patientbudget.jsp" flush="true">
						<jsp:param name="from" value="study"/>
						<jsp:param name="mode" value="M"/>
			            <jsp:param name="budgetId" value="<%=budgetId%>" />
						<jsp:param name="studyId" value="<%=studyId%>"/>
			            <jsp:param name="budgetTemplate" value="P" />
			            <jsp:param name="includedIn" value="P" />
						<jsp:param name="pageRight" value="<%=pageRight%>" />
						<jsp:param name="appendxRight" value="<%=appendxRight%>" />
			        </jsp:include>
				<%}//has budget access %>
			<%}//budgetexists%>
		<% } //end of else study exists%>
	<%}else{//page right%>
		<div class=formdefault>
			<BR/>
			<table>
				<tr> 
					<td>
						<p class = "sectionHeadings"><font color="red"> <%=MC.M_NotStdLevel_BgtAces%><%--Unfortunately you do not have Study level Combined Budget access.*****--%> </font></p>
						<p class = "defComments"> <%=MC.M_ObtainRgt_RptErr%><%--To obtain appropriate rights or to report an error, please contact your account administrator.*****--%> <BR></p>
					</td>
				</tr>
			</table>
			<BR>
		</div>
	<%}%>
			</td>
			</tr>
		</table>
	</DIV>
</body>
<%}%>
<%}//end of if body for session
	else{%> 
	 <jsp:include page="timeout.html" flush="true" /> 
<%}%>
<div><jsp:include page="bottompanel.jsp" flush="true" /></div>
<div class="mainMenu" id="emenu"><jsp:include page="getmenu.jsp" flush="true" /></div>



</html>
