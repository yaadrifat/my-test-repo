<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="include.jsp" flush="true"/>
<%@page import="com.velos.eres.service.util.StringUtil"%>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<title><%=MC.M_Add_FrmStd%><%--Add Forms to <%=LC.Std_Study%>*****--%></title>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>


<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %><%@page import="com.velos.eres.service.util.MC"%>

<body>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="linkedFormsB" scope="request"  class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>

<%@ page language = "java" import = "com.velos.eres.business.site.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.LC"

%>
<DIV class="popDefault" id="div1">

<%
 HttpSession tSession = request.getSession(true); 
 
 if (sessionmaint.isValidSession(tSession))
 {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
    String deSelFormId="";
	int firstpos=0;
   
   //indicates the form is being linked from Study or Account
   //A - acccount, S - Study			
    String linkFrom = request.getParameter("linkFrom");

  	String[] selForms = request.getParameterValues("selForms"); //selected form ids                	
    String[] deSelIds = request.getParameterValues("deSelIds"); //deselected form ids with checked checkboxes
	
    String[] deSelFormIds = request.getParameterValues("deSelFormIds");//all deselected form ids
		
    String[] deSelNames = request.getParameterValues("deSelNames");		
    String[] deSelDescs = request.getParameterValues("deSelDescs");
	                	
    String studyId = request.getParameter("lnkStudyId");
    String[] lnkToStudyIds = request.getParameterValues("lnkToStudyIds");
    String selStudyId = request.getParameter("selStudyId");
    String formType = request.getParameter("formType");		
             					
    int len=0;
    String pageMode = request.getParameter("pageMode");
    String btnClicked = request.getParameter("btnClicked");
		
	boolean exists = false;
	
   int deSelLen = 0;
   if (!(deSelFormIds==null))
   {		
	 deSelLen = deSelFormIds.length;
   }
		
%>
<form name="addForms" method="post" action="linkformstostudyacc.jsp" onsubmit="">
<input type="hidden" name="lnkStudyId" value="<%=studyId%>">
<input type="hidden" name="lnkToStudyIds" value="<%=lnkToStudyIds%>">
<input type="hidden" name="selStudyId" value="<%=selStudyId%>">
<input type="hidden" name="formType" value="<%=formType%>">
<input type="hidden" name="linkFrom" value="<%=linkFrom%>">

<%
  if (pageMode.equals("Select")) //user has clicked the toggling arrows
  {
  
  //user has clicked the down arrow. Pick all the seleced ids from the top browser and also pick the ids 
  //in the lower browser. combine both of them 
  
  if (btnClicked.equals("down")) { 
  //get all the selected ids
	if (!(selForms==null)) 
	{
		len = selForms.length;
	}

	for (int i=0;i< len;i++)
	{
		int strlen = (selForms[i]).length();
		firstpos = (selForms[i]).indexOf("[VELSEP]",1);
		String selId =  (selForms[i]).substring(0,firstpos);
	 	int secondpos = (selForms[i]).indexOf("[VELSEP]",firstpos+8);
		String selName =  (selForms[i]).substring(firstpos+8,secondpos);
		String selDesc = (selForms[i]).substring(secondpos+8,strlen);
%>
  		<input type="hidden" name="deSelIds" value="<%=selId%>">
  		<input type="hidden" name="deSelNames" value="<%=selName%>">		
		<input type="hidden" name="deSelDescs" value="<%=selDesc%>">
<%
              
    } //for loop
	
	 for (int i=0;i<deSelLen;i++) 
  	  { 
   	   //deselected ids are separated by *.remove * and get the id
		 deSelFormId = deSelFormIds[i];
	     firstpos = (deSelFormId).indexOf("*");
		 deSelFormId =  (deSelFormId).substring(0,firstpos); 		   
		   %>
       	  <input type="hidden" name="deSelIds" value="<%=deSelFormId%>">
   		  <input type="hidden" name="deSelNames" value="<%=deSelNames[i]%>">		
   		  <input type="hidden" name="deSelDescs" value="<%=deSelDescs[i]%>">	  	 
	 <%
	  }	//for loop
	
	
	} //end of btnClicked=down  
 else if (btnClicked.equals("up")) 
  {
  //user has clicked the up arrow. Pick all the deseleced ids the lower browser only.
  //no need to pick from the top browser 
  
      len=0;
 
       if (!(deSelIds==null))
        {
           len = deSelIds.length;
		}

		 
	 for (int i=0;i<deSelLen;i++) 
  	  { 
	  if (len > 0) {

    	  for (int j=0;j<len;j++) {	  
    	     if (deSelFormIds[i].equals(deSelIds[j])) {
			 	exists = true;
				break;
			 } else {
			 	exists = false;
			 }
    	  } //for loop
  		if (!(exists)) {
		   //deselected ids are separated by *
		   //remove * and get the id
		   deSelFormId = deSelFormIds[i];
		   firstpos = (deSelFormId).indexOf("*");
		   deSelFormId =  (deSelFormId).substring(0,firstpos); 		   
    %>
        	  <input type="hidden" name="deSelIds" value="<%=deSelFormId%>">
      		  <input type="hidden" name="deSelNames" value="<%=deSelNames[i]%>">		
    		  <input type="hidden" name="deSelDescs" value="<%=deSelDescs[i]%>">	  
    <%
      	} //if exists
		   

	  } 
	  else { 
	   	   //deselected ids are separated by *
		   //remove * and get the id
		   deSelFormId = deSelFormIds[i];
		   firstpos = (deSelFormId).indexOf("*");
		   deSelFormId =  (deSelFormId).substring(0,firstpos);
 		   
		   %>
       	  <input type="hidden" name="deSelIds" value="<%=deSelFormId%>">
   		  <input type="hidden" name="deSelNames" value="<%=deSelNames[i]%>">		
   		  <input type="hidden" name="deSelDescs" value="<%=deSelDescs[i]%>">	  	 
	 <%
	  }
   	  }
	 } //btnClicked=up 
%>
	</form>		
  <script>
	document.addForms.submit();
  </script>
<% 	  
    } //end of pageMode=select 
              
     else if(pageMode.equals("Submit")) //user has clicked submit button
     { 
           String eSign= request.getParameter("eSign");
           String oldESign = (String) tSession.getValue("eSign");
           if (!(oldESign.equals(eSign))) {
    		%>
       	 	 <jsp:include page="incorrectesign.jsp" flush="true"/>  	
       		<%
       		} else {

     			String[] formIds = request.getParameterValues("deSelFormOnlyIds");

    			//get the value of Display Form Link and Form Characteristics
    			int radioLen = formIds.length;
    			String[] formLinks = new String[radioLen];
    			String[] formChars = new String[radioLen];
    			
    			String radioName;
    			String formCharName;
    			for (int j=0;j<radioLen;j++)  {
    				radioName = "formLink"+j;
    				formCharName = "formChar"+j;
    				formLinks[j] = request.getParameter(radioName); 
    				formChars[j] = request.getParameter(formCharName);
    			}
    			int formLen = 0;
    			if (!(formIds==null)) 
    			{
    				formLen = formIds.length;
    			}
    			String[] formNames = request.getParameterValues("frmName");
    			for (int i=0;i<formLen;i++){		
    				formNames[i] = StringUtil.stripScript(formNames[i]);
    			}		
    			String[] formDescs = request.getParameterValues("frmDesc");		
    			String[] formOrgIds =  request.getParameterValues("formOrgIds");
    			String[] selGrpIds =  request.getParameterValues("selGrpIds");				
				
   			    String[] accFormStudy =  request.getParameterValues("accFormStudy");
    			
    			String ipAdd = (String) tSession.getValue("ipAdd");
    			String userId = (String) tSession.getValue("userId");
    			String accountId = (String) tSession.getValue("accountId");			
    
    			//set the LinkedFormsDao with the string arrays for insertion in db
    			LinkedFormsDao linkedFormsDao = new LinkedFormsDao();
    			linkedFormsDao.setFormId(formIds);
    			linkedFormsDao.setFormName(formNames);
    			linkedFormsDao.setFormDescription(formDescs);
    			linkedFormsDao.setFormDisplayType(formLinks);
    			linkedFormsDao.setFormEntryChar(formChars);
    			linkedFormsDao.setFormOrg(formOrgIds);
				
				linkedFormsDao.setFormGrp(selGrpIds);
				
				if (linkFrom.equals("S")) {				
				    linkedFormsDao.setStudyId(lnkToStudyIds);
				} else if (linkFrom.equals("A")) {
					if (accFormStudy==null) {				
						String[] noStudy = new String[1];
						noStudy[0] = ""; 
					   	linkedFormsDao.setStudyId(noStudy);
					} else {								
					   	linkedFormsDao.setStudyId(accFormStudy);
					}												
				}						
				
    			linkedFormsDao.setAccountId(accountId);
    			linkedFormsDao.setUserId(userId);
    			linkedFormsDao.setLnkFrom(linkFrom);
    			linkedFormsDao.setIpAdd(ipAdd);
    			
    			int ret=0;
    		    ret = linkedFormsB.linkFormsToStudyAccount(linkedFormsDao);
    			if (ret==0) {
				%>						
				<br><br><br><br><br>
				<p class = "successfulmsg" align = center><%=MC.M_Data_SavedSucc%><%--Data saved Successfully*****--%></p>
				<script>
				  window.opener.location.reload();
				  setTimeout("self.close()",1000);
				 </script>
				 <%
                } else if (ret==-1) {
				%>
          		<br><br><br><br><br>
				<p class = "successfulmsg" align = center><%=MC.M_Data_NotSvdSucc%><%--Data not saved Successfully*****--%></p>		
				<%} else if (ret==-3) {
				%>
				 <jsp:include page="duplicateformname.jsp" flush="true"/>				
				<%}
			}//esign
	  } //pageMode
    }//end of if body for session             	
  	else
   	{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}
%>                		
		
</div>
 <div class = "myHomebottomPanel">
     <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</body>

</html>


			
