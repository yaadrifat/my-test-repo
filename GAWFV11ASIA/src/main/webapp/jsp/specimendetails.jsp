	<% String contextpath=request.getContextPath();%>
	<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
	<jsp:include page="localization.jsp" flush="true"/>
	<jsp:include page="jqueryUtils.jsp" flush="true"/>
	<jsp:include page="moreDetailsInclude.jsp"  flush="true">
	<jsp:param name="modName" value="specimen"/>
	</jsp:include>
	<html>
	<head>
		<title> <%=MC.M_MngInv_SpmenDets%><%--Manage Inventory >> Specimen Details*****--%> </title>
	</head>
	
	
	<%@ page import="java.math.BigDecimal, java.math.RoundingMode,java.text.DecimalFormat, java.util.regex.Pattern,java.util.regex.Matcher" %>
	<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/styles/portlet.css" media="screen" />
	<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/common/scripts.js"></script>
	<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
	<!--<SCRIPT LANGUAGE="JavaScript" SRC="whichcss.js"></SCRIPT>--> <!-- YK 02Aug11: Commented for Bug #6728 already using Whichcss_skin.js in panel.jsp -->
	<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
	
	<jsp:include page="panel.jsp" flush="true">
	<jsp:param name="src" value="srctdmenubaritem6"/>
	</jsp:include>
	
	<style type="text/css">
 .ui-autocomplete {
    cursor: default;
    position: absolute;
    background: #FAFAFA;
    max-height: 100px;
    overflow-y: auto;
    overflow-x: hidden;
    }
    .ui-widget-content a text{
   color: :#FAFAFA
}
 </style>
	<SCRIPT language="javascript">
	var screenWidth = screen.width;
	var screenHeight = screen.height;
	var isIe = jQuery.browser.msie;
	 function f_edit(selctedbox,formobj,childpk,processType)
	{

		var selectedSts = eval("formobj.specstatus_"+childpk+".value");
		var indx = -1;
		if (selectedSts!= undefined && selectedSts != '' && selectedSts.length >0){
			var statusTyps = formobj.statusTyps.value;
			indx = statusTyps.indexOf(selectedSts); 	
		}
			
		if (selctedbox.checked)
		{
		    document.getElementById('childid_e_'+childpk).style.display='none';
            document.getElementById('child_id_e_'+childpk).style.display='block';

			document.getElementById('colldate_'+childpk).style.display='none';
			document.getElementById('colldate_e_' + childpk ).style.display='block';
	
			document.getElementById('spectype_'+childpk).style.display='none';
			document.getElementById('spectype_e_' + childpk ).style.display='block';
	
			document.getElementById('specstatus_'+childpk).style.display='none';
			document.getElementById('specstatus_e_' + childpk ).style.display='block';
	
			document.getElementById('specqnty_'+childpk).style.display='none';
			document.getElementById('specqnty_e_' + childpk ).style.display='block';
			
			// Bug#9971 Date:26-June-2012 Ankit
			if(processType > 0 && indx >-1){
				document.getElementById('specproctype_'+childpk).style.display='none';
				document.getElementById('specproctype_e_' + childpk ).style.display='block';
			}else{
				document.getElementById('specproctype_'+childpk).style.display='none';
				document.getElementById('specproctype_e_' + childpk ).style.display='none';
			}
			
			document.getElementById('specloc_'+childpk).style.display='none';
			document.getElementById('specloc_e_' + childpk ).style.display='block';

	
		}
		else
		{
	
	        document.getElementById('childid_e_'+childpk).style.display='block';		   
            document.getElementById('child_id_e_'+childpk).style.display='none';	

			document.getElementById('colldate_'+childpk).style.display='block';
			document.getElementById('colldate_e_' + childpk).style.display='none';
	
			document.getElementById('spectype_'+childpk).style.display='block';
			document.getElementById('spectype_e_' + childpk).style.display='none';
	
			document.getElementById('specstatus_'+childpk).style.display='block';
			document.getElementById('specstatus_e_' + childpk).style.display='none';
	
		  	document.getElementById('specqnty_'+childpk).style.display='block';
			document.getElementById('specqnty_e_' + childpk).style.display='none';
			/*if(processType != "" && indx >-1){
				document.getElementById('specproctype_'+childpk).style.display='block';
				document.getElementById('specproctype_e_' + childpk).style.display='none';
			}
			else{
				document.getElementById('specproctype_'+childpk).style.display='none';
				document.getElementById('specproctype_e_' + childpk).style.display='none';
			}*/
			document.getElementById('specproctype_'+childpk).style.display='block';
			document.getElementById('specproctype_e_' + childpk ).style.display='none';
			
		 	document.getElementById('specloc_'+childpk).style.display='block';
			document.getElementById('specloc_e_' + childpk).style.display='none';
	
		}
	}


 function printLabelchildspecimen() {
			var childspecimen ='';
		$j('input:checkbox[name=selectEdit]').each(function() 
				{    
				    if($j(this).is(':checked')) {	
							    	
				    	childspecimen += $j(this).val()+","; 
					      }
				      		
				});
		childspecimen=","+childspecimen.substring(0,childspecimen.length-1);
		if(childspecimen=="" || childspecimen.length==1  ){
			alert("Please select Specimen(s) to be Printed")
			return false
			}
		else{
			windowName =window.open("printMultiLabel.jsp?&selPks="+childspecimen, "Information", "toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=700,height=450, top=100, left=90");
			  if (windowName != null) { windowName.focus(); }

				} 
			 
				
			}//end of multiple print

	function validateChildSpecLocation(formobj)
	{
	
	  var arLoc = new Array();
	  var ctr = 0;
	  var childct = 0;
	  var i = 0;
	  var count = 0;
	  var size=0;
	  var sizeN=0;
	  var temp=0;
	  if (formobj.selectEdit!= undefined)
	  {
	   childct = formobj.selectEdit.length;
	   }
	   else
	   {
	   childct = 0;
	   }
	
	  //iterate through edited child specimens
	  if (childct > 1)
	  {
		  for (l=0;l<childct;l++)
		  {
		    if (formobj.selectEdit[l].checked)
		    {
		      specpk = formobj.selectEdit[l].value;
		      locval = eval("formobj.childlocation_pk_"+specpk+".value");
			  storageCapacity = eval("formobj.storageCapacity"+specpk+".value");
			  multiSpecimen = eval("formobj.multiSpecimen"+specpk+".value");
			  capacityUnit = eval("formobj.capacityUnit"+specpk+".value");
			  storageChildCount = eval("formobj.storageChildCount"+specpk+".value");
			  storageCount = eval("formobj.storageCount"+specpk+".value");
			  codelstItem= eval("formobj.codelstItem"+specpk+".value");
			  locationName=eval("formobj.childlocation_e_text_"+specpk+".value");
			  mainFKStorage=eval("formobj.mainFKStorage.value");
			  oldFKStorage=eval("formobj.oldFKStorage.value");
			 // alert("locval "+locval+"specpk: "+specpk+"storageCapacity: "+storageCapacity+"multiSpecimen: "+multiSpecimen+"capacityUnit: "+capacityUnit+"storageChildCount: "+storageChildCount+"storageCount: "+storageCount);
			  
		      var id = "";
		      id = eval("formobj.childid_"+specpk+".value");
		      //check if location exists

		      if (locval!= undefined && locval != '' && locval.length >0)
		      {
				  count=0;
				  i=0;
				  size=0;
				  arLoc[ctr] = locval;
		          ctr = ctr+1;
				  while(i<=arLoc.length)
				  {
				  if(arLoc[i]==locval)
				  count++;
				  i++;
				  }
			  if(multiSpecimen==1){
			  if (capacityUnit == codelstItem) {
			    size=(storageCapacity - storageChildCount) - parseInt(storageCount);
				sizeN=size;
				if(locval==mainFKStorage){
				if(mainFKStorage==oldFKStorage)
				{}
				else
				size=size-1;
				}
				if(size>=count || storageCapacity=='' || storageCapacity=='null' || sizeN==0){}
				else
				{
				  var paramArray = [locationName,sizeN,sizeN];
				  alert(getLocalizedMessageString("M_LocBeyond_Capacity",paramArray));
		          return false;
				}
				}
			  }
			else
			{
			if(locval==mainFKStorage){
			if(mainFKStorage!=oldFKStorage)
			count=count+1;
			}
			if (count>1)
		        {
		        	var paramArray = [id];
		            alert(getLocalizedMessageString("M_SelDuplLoc_UnqSpmen",paramArray));/*alert("You have selected a duplicate Location for the Child Specimen with ID:"+id+". Please select unique locations for all Child Specimens.");*****/
		          return false;
		        }
			}
		    }
	
		    //check if new status was selected, if yes, validate date
	
		    newstat = eval("formobj.specstatus_"+specpk+".value");
		    var oldStat = eval("formobj.old_status_pk_"+specpk+".value"); //Old status
		      if (newstat!= undefined && newstat != '' && newstat.length >0 && oldStat != newstat)
		      {
		      	//check if status date was selected
		      	 newdate =  eval("formobj.specstatusdate_e_"+specpk+".value");
	
		      	 if (newdate== undefined || newdate == '' || newdate.length <=0)
			      { var paramArray = [id];
			      	alert(getLocalizedMessageString("M_NewStat_SpmenStatDt",paramArray));/*alert("You have selected a new Status for Child Specimen with ID:"+id+". Please select a 'Status Date' for the same.");*****/
			      	 eval("formobj.specstatusdate_e_"+specpk+".focus()");
			      	return false;
			      }
		      }
		  }
		}
	
	  } //if
	
	  return true;
	
	}
// BT: INV-22459
function setDD(formobj)
{
	if(formobj.mode.value=='M')
	{
	optvalue=formobj.ddlist.value;
	if (optvalue.length>0)
	{
		var arrayofStrings=optvalue.split("||");
		if (arrayofStrings.length>1) {
			for (var j=0;j<arrayofStrings.length;j++)
			{
				var ddStr=arrayofStrings[j];
				var arrayofDD=ddStr.split(":");
				var ddcount=arrayofDD[0];
				var selvalue=arrayofDD[1];
				var ddFld = formobj['alternateId'+ddcount];
				var opt = null;
				if (ddFld && ddFld.options) {
					opt = ddFld.options;
					for (var i=0;i<opt.length;i++){
						if (opt[i].value==selvalue){
							ddFld.selectedIndex=i ;
						}
					}
				}
			}
		} else {
			var ddStr=arrayofStrings[0];
			var arrayofDD=ddStr.split(":");
			var ddcount=arrayofDD[0];
			var selvalue=arrayofDD[1];
			var ddFld = formobj['alternateId'+ddcount];
			if (ddFld && ddFld.options) {
			 	var opt = ddFld.options; 
				if (opt == undefined)
			    	opt = ddFld.options; 
				
				for (var i=0;i<opt.length;i++){
					if (opt[i].value==selvalue){
						ddFld.selectedIndex=i ;
					}
				}
			}
		}// end else
	}//optvalue.length>0	
}
}
	function  validate(formobj)
	{
		// Pritam Singh: - Child Specimen cannot be left blank
		if (formobj.mode.value =='M')
		{
			var Child_Count = document.getElementById('childcount').value;
			for(count=0;count<Child_Count;count++)
			{
				Child_PK = document.getElementById('childSpecIds_'+count).value;
				Child_Name = document.getElementById('childid_'+Child_PK).value;
				Child_Name = trimString(Child_Name);
				if(Child_Name.length==0)
				{
					alert("<%=MC.M_ChldSpec_CntBlank%>");
					document.getElementById('childid_'+Child_PK).focus();
					return false;
				}
				
				for(count1=0;count1<Child_Count;count1++)
				{
					Child_PK1 = document.getElementById('childSpecIds_'+count1).value;
					Child_Name1 = document.getElementById('childid_'+Child_PK1).value;
					Child_Name1 = trimString(Child_Name1);
					
					if((Child_Name == Child_Name1) && (count != count1))
					{
						var paramArray = [Child_Name];
						alert(getLocalizedMessageString("M_ChldSpec_Duplicate",paramArray));
						document.getElementById('childid_'+Child_PK).focus();
						document.getElementById('childid_'+Child_PK1).focus();
						return false;
					}
				}
			}
		}
		var s=formobj.timeHH.value+':'+formobj.timeMM.value+':'+formobj.timeSS.value;
		formobj.timeId.value=s;
		
		//selecting more than one Study is not permissible
		var stdstring = "";
		stdstring = formobj.selStudyIds.value;
	 	if (stdstring.indexOf(',')==-1){
	 	}else{
	 	 alert("<%=MC.M_Selc_OneStdOnly%>");/*alert("Please select one <%=LC.Std_Study%> only");*****/
	 	 formobj.selStudy.focus();
	 	 return false;
	 	}
	
		//selecting more than one Patient is not permissible
		patstring = formobj.patientIds.value;
	 	if (patstring.indexOf(',')==-1){
	 	}else{
	 	 alert("<%=MC.M_PlsSel_PatOnly%>");/*alert("Please select one <%=LC.Pat_Patient%> only");*****/
	 	 formobj.patientNames.focus();
	 	 return false;
	 	}
	
	
		//JM: 30Jan2008:
	
		if(isNaN(formobj.noOfChild.value) == true){
			alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a Valid Number");*****/
			formobj.noOfChild.focus();
			return false;
		}
	
		if(isNaN(formobj.quantity.value) == true){
			alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a Valid Number");*****/
			formobj.quantity.focus();
			return false;
		}
	
		//Yogendra Pratap Singh : Modified for Bug#-6673 : Date 26 Mar 2012
		if(formobj.quantity.value < 0){
			alert("<%=MC.M_Neg_Quantity_Not_Allowed%>");
			/*alert("Available Quantity can not be less than 0, please enter a valid Quantity for the Child Specimens.");****/
			return false;
		}
	
	
	
	
	//JM: 29Aug2007: blocked
	   // if (!(validate_col( 'Specimen ID' ,formobj.specimenId ))) return false;
	        if (!(validate_col( 'Type' ,formobj.specType ))) return false;
			if(document.getElementById("org_flag").value==1){
			if (!(validate_col( 'Organization' ,formobj.dPatSite )) ) return false;}
		if (!(validate_date(formobj.colDate))) return false	;
	
		/**** JM: 20May2009: added, enhacement #INVP2.7.1 *****/
	
		if (formobj.expQuantity.value < 0){//M, N
		    alert("<%=MC.M_PositiveExpected_Amt%>");/*alert("please enter a positive number for the Expected Amount");*****/
		    return false;
	    }
	
		if (formobj.expQuantity.value != ''){
			if (parseFloat(formobj.expQuantity.value)!= '0' && formobj.specExptdQntUnit_dd.value ==''){
			    alert("<%=MC.M_Sel_ExpectedAmtUnit%>");/*alert("please select the Expected Amount Unit");*****/
			    return false;
		    }
	    }
	   	if (formobj.expQuantity.value == '' && formobj.specExptdQntUnit_dd.value !=''){
		    alert("<%=MC.M_Etr_ExpectedAmt%>");/*alert("please enter the Expected Amount");*****/
		    formobj.expQuantity.focus();
		    return false;
	    }
	
		if(isNaN(formobj.expQuantity.value) == true){
			alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a Valid Number");*****/
			formobj.expQuantity.focus();
			return false;
		}

		if (formobj.origQuantity.value != '' && formobj.expQuantity.value ==''){
			alert("<%=MC.M_Etr_ExpectedAmt%>");/*alert("please enter the Expected Amount");*****/
		    formobj.expQuantity.focus();
		    return false;
	    }
	
	
		if (formobj.origQuantity.value < 0){
		    alert("<%=MC.M_PositiveCollected_Amt%>");/*alert("please enter a positive number for the Collected Amount");*****/
		    return false;
	    }
			
	
	  // to remove recalculate link
	    if (formobj.origQuantity.value != ''){
			  if ( parseFloat(formobj.origQuantity.value)!= '0'&& formobj.specBaseOrigQuUnit_dd.value ==''){
			     alert("<%=MC.M_Sel_CollAmtUnit%>");/*alert("please select the Collected Amount Unit");*****/
			    return false;
		    }
	    }
	
	
	    if (formobj.origQuantity.value == '' && formobj.specBaseOrigQuUnit_dd.value !=''){
		    alert("<%=MC.M_PlsEtrOrg_CltdAmt%>");/*alert("please enter the Original/Collected Amount");*****/
		    formobj.origQuantity.focus();
		    return false;
	    }
	
		if(isNaN(formobj.origQuantity.value) == true){
			alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a Valid Number");*****/
			formobj.origQuantity.focus();
			return false;
		}
	
	
	    if (formobj.avlQuantity.value != ''){
			if (parseFloat(formobj.avlQuantity.value) != '0'&& formobj.specQuantityUnit.value ==''){
	    		alert("<%=MC.M_Sel_CurrAmtUnit%>");/*alert("please select the Current Amount Unit");*****/
			    return false;
		    }
	    }
	
	
	   // if (formobj.avlQuantity.value == '' && formobj.specQuantityUnit.value !=''){
	   // alert("<%=MC.M_PlsEtrAvl_CltdAmt%>");/*alert("please enter the Available/Current Amount");*****/
	   // formobj.avlQuantity.focus();
	   // return false;
	    //}
	
	    if(isNaN(formobj.avlQuantity.value) == true){
	    alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a Valid Number");*****/
			formobj.avlQuantity.focus();
			return false;
		}
	
	
	//JM: 01Sept2009: #4222 - point(2)
		if (formobj.expQuantity.value != '' && formobj.origQuantity.value ==''){
		    //alert("<%=MC.M_Etr_CollectedAmt%>");/*alert("please enter the Collected Amount");*****/
		    alert("<%=MC.M_PlsEtrOrg_CltdAmt%>");/*alert("please enter the Original/Collected Amount");*****/
		    formobj.origQuantity.focus();
		    return false;
	    }
	
	
		if (formobj.specExptdQntUnit_dd.value != ''){
			if ( (formobj.specExptdQntUnit_dd.value == formobj.specBaseOrigQuUnit_dd.value) && (formobj.specBaseOrigQuUnit_dd.value == formobj.specQuantityUnit.value )){
	
			}else{
				alert("<%=MC.M_PlsSelUnit_ExpecCltAmt%>")/*alert("Please select the same Unit for Expected, Collected and Current Amount")*****/
				return false;
			}
		}else{
	
			if ( formobj.specBaseOrigQuUnit_dd.value == formobj.specQuantityUnit.value ){
	
			}else{
				alert("<%=MC.M_SelSameUnit_CltAndCurAmt%>")/*alert("Please select the same Unit for Collected and Current Amount")*****/
				return false;
			}
	
		}
	
	
		if ((formobj.collectionHH.value != '' || formobj.collectionMM.value != '' || formobj.collectionSS.value != '') && formobj.colDate.value == ''){
			if ((formobj.collectionHH.value != '00' || formobj.collectionMM.value != '00' || formobj.collectionSS.value != '00') && formobj.colDate.value == ''){
				alert("<%=MC.M_Etr_CollectionDt%>");/*alert("Please enter the Collection Date");*****/
				formobj.colDate.focus();
				return false;
			}
		}
	
		if ((formobj.processHH.value != '' || formobj.processMM.value != '' || formobj.processSS.value != '') && formobj.procDate.value == ''){
			if ((formobj.processHH.value != '00' || formobj.processMM.value != '00' || formobj.processSS.value != '00') && formobj.procDate.value == ''){
				alert("<%=MC.M_Etr_ProcessingDt%>");/*alert("Please enter the Processing Date");*****/
				formobj.procDate.focus();
				return false;
			}
		}
	
	
		if (formobj.mode.value =='M'){
		if ((formobj.eprocessHH.value != '' || formobj.eprocessMM.value != '' || formobj.eprocessSS.value != '') && formobj.eProcDate.value == ''){
			if ((formobj.eprocessHH.value != '00' || formobj.eprocessMM.value != '00' || formobj.eprocessSS.value != '00') && formobj.eProcDate.value == ''){
				alert("<%=MC.M_Etr_ProcessingDt%>");/*alert("Please enter the Processing Date");*****/
				formobj.eProcDate.focus();
				return false;
			}
		}
		}
	
	
	
	
	
		if ( !validateDataRange(formobj.collectionHH.value,'>=',0,'and','<',24) || !isInteger(formobj.collectionHH.value) )
		{
		   alert("<%=MC.M_Vldt_NumAsPositive%>");/*alert("Please enter a positive number below 24");*****/
			formobj.collectionHH.focus();
			return false;
		}
	
		if (!validateDataRange(formobj.collectionMM.value,'>=',0,'and','<',60) || !isInteger(formobj.collectionMM.value))
		{
		   alert("<%=MC.M_PlsEtrPtv_NumBlw60%>");/*alert("Please enter a positive number below 60");*****/
			formobj.collectionMM.focus();
			return false;
		}
	
	  	if (!validateDataRange(formobj.collectionSS.value,'>=',0,'and','<',60) || !isInteger(formobj.collectionSS.value) )
		{
	  		alert("<%=MC.M_PlsEtrPtv_NumBlw60%>");/*alert("Please enter a positive number below 60");*****/
			formobj.collectionSS.focus();
			return false;
		}
	  	if ( !validateDataRange(formobj.timeHH.value,'>=',0,'and','<',24) || !isInteger(formobj.timeHH.value) )
		{
		   alert("<%=MC.M_Vldt_NumAsPositive%>");/*alert("Please enter a positive number below 24");*****/
			formobj.collectionHH.focus();
			return false;
		}
	
		if (!validateDataRange(formobj.timeMM.value,'>=',0,'and','<',60) || !isInteger(formobj.timeMM.value))
		{
		   alert("<%=MC.M_PlsEtrPtv_NumBlw60%>");/*alert("Please enter a positive number below 60");*****/
			formobj.collectionMM.focus();
			return false;
		}
	
	  	if (!validateDataRange(formobj.timeSS.value,'>=',0,'and','<',60) || !isInteger(formobj.timeSS.value) )
		{
	  		alert("<%=MC.M_PlsEtrPtv_NumBlw60%>");/*alert("Please enter a positive number below 60");*****/
			formobj.collectionSS.focus();
			return false;
		}
	
		//JM: #4096(2, 3): added validation
	
		if (!(validate_date(formobj.procDate))) return false	;
		if (formobj.mode.value =='M'){
		 if (!(validate_date(formobj.eProcDate))) return false	;
		}
	
		if (!validateDataRange(formobj.processHH.value,'>=',0,'and','<',24) || !isInteger(formobj.processHH.value) )
		{
			alert("<%=MC.M_Vldt_NumAsPositive%>");/*alert("Please enter a positive number below 24");*****/
			formobj.processHH.focus();
			return false;
		}
	
		if (!validateDataRange(formobj.processMM.value,'>=',0,'and','<',60) || !isInteger(formobj.processMM.value))
		{
			alert("<%=MC.M_PlsEtrPtv_NumBlw60%>");/*alert("Please enter a positive number below 60");*****/
			formobj.processMM.focus();
			return false;
		}
	
	  	if (!validateDataRange(formobj.processSS.value,'>=',0,'and','<',60) || !isInteger(formobj.processSS.value) )
		{
	  		alert("<%=MC.M_PlsEtrPtv_NumBlw60%>");/*alert("Please enter a positive number below 60");*****/
			formobj.processSS.focus();
			return false;
		}
	
		//JM: #4096(2, 3): added validation
		if (formobj.mode.value =='M'){
		if (!validateDataRange(formobj.eprocessHH.value,'>=',0,'and','<',24) || !isInteger(formobj.eprocessHH.value) )
		{
			alert("<%=MC.M_Vldt_NumAsPositive%>");/*alert("Please enter a positive number below 24");*****/
			formobj.eprocessHH.focus();
			return false;
		}
	
		if (!validateDataRange(formobj.eprocessMM.value,'>=',0,'and','<',60) || !isInteger(formobj.eprocessMM.value) )
		{
			alert("<%=MC.M_PlsEtrPtv_NumBlw60%>");/*alert("Please enter a positive number below 60");*****/
			formobj.eprocessMM.focus();
			return false;
		}
	
	  	if (!validateDataRange(formobj.eprocessSS.value,'>=',0,'and','<',60) || !isInteger(formobj.eprocessSS.value) )
		{
	  		alert("<%=MC.M_PlsEtrPtv_NumBlw60%>");/*alert("Please enter a positive number below 60");*****/
			formobj.eprocessSS.focus();
			return false;
		}
		}
	
	
		//////
	
			if (!validateDataRange(formobj.TORemovalHH.value,'>=',0,'and','<=',24)  || !isInteger(formobj.TORemovalHH.value)  )
		{
		   alert("<%=MC.M_EtrNum_0To24%>");/*alert("Please enter a Number between 0 and 24");*****/
			formobj.TORemovalHH.focus();
			return false;
		}
	
		if (!validateDataRange(formobj.TORemovalMM.value,'>=',0,'and','<',60)  || !isInteger(formobj.TORemovalMM.value) )
		{
			alert("<%=MC.M_EtrNum_0To60%>");/*alert("Please enter a Number between 0 and 60");*****/
			formobj.TORemovalMM.focus();
			return false;
		}
	
	  	if (!validateDataRange(formobj.TORemovalSS.value,'>=',0,'and','<',60)  || !isInteger(formobj.TORemovalSS.value) )
		{
	  		alert("<%=MC.M_EtrNum_0To60%>");/*alert("Please enter a Number between 0 and 60");*****/
			formobj.TORemovalSS.focus();
			return false;
		}
	
	
			if (!validateDataRange(formobj.TOFreezeHH.value,'>=',0,'and','<=',24)  || !isInteger(formobj.TOFreezeHH.value) )
		{
				alert("<%=MC.M_EtrNum_0To24%>");/*alert("Please enter a Number between 0 and 24");*****/
			formobj.TOFreezeHH.focus();
			return false;
		}
	
		if (!validateDataRange(formobj.TOFreezeMM.value,'>=',0,'and','<',60 )  || !isInteger(formobj.TOFreezeMM.value))
		{
			alert("<%=MC.M_EtrNum_0To59%>");/*alert("Please enter a Number between 0 and 59");*****/
			formobj.TOFreezeMM.focus();
			return false;
		}
	
	  	if (!validateDataRange(formobj.TOFreezeSS.value,'>=',0,'and','<',60)  || !isInteger(formobj.TOFreezeSS.value))
		{
		   alert("<%=MC.M_EtrNum_0To59%>");/*alert("Please enter a Number between 0 and 59");*****/
			formobj.TOFreezeSS.focus();
			return false;
		}
	
		if (! validateChildSpecLocation(formobj))
		{
			return false;
		}
		
	if (!(validate_col('e-Signature',formobj.eSign))) return false;
		<%-- if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
	   } --%>
	
	   if (formobj.mode.value=='M'){
	   if (formobj.codeDesc.value !=''&& formobj.noOfChild.value !='' ){
		     var paramArray = [formobj.codeDesc.value];
			 alert(getLocalizedMessageString("M_SpmenStat_ChldPrmt", paramArray));/*alert("Specimen status is '"+formobj.codeDesc.value+"', no further creation of child specimen is permitted");*****/
			 return false;
		}
	   }
	
	 //Yogendra Pratap Singh : Modified for Bug#-6671 : Date 26 Mar 2012
		var parQuantity = formobj.avlQuantity.value;
		var numChild = formobj.noOfChild.value;
		var quantChild = formobj.quantity.value;
		parQuantity = (parQuantity - numChild * quantChild);
		if (parQuantity <0){
			alert("<%=MC.M_Neg_Curr_Amount_Not_Allowed%>");
			/*alert("The total Quantity of Child Specimen should not exceed the Current Amount.");****/
			return false;
		}
	/*****************************************************************************/
	//JM: 24Jul2009: #4127
	if (formobj.selectEdit!= undefined)
		  {
		   childct = formobj.selectEdit.length;
		   }
		   else
		   {
		   childct = 0;
		}
	
	var counter = 0;
	
	for (a=0;a<childct;a++){
		if (formobj.selectEdit[a].checked){
	
		counter ++;
	
		}
	
	}
	//JM: 27Aug2009: #4127
	//for an only existing child
	
	if (formobj.mode.value=='M'){
	
	if ((formobj.ecProcType.value =='' && formobj.eStorageLocation.value=='') && formobj.eProcDate.value!='' && counter==0){
	
		alert("<%=MC.M_Sel_Proc%>")/*alert("Please select either the Process")*****/
		return false;
	}
	
	
	if (formobj.selectEdit!= undefined){
	
	if(counter==0 && formobj.selectEdit.checked==true ){
	
		if ((formobj.ecProcType.value !='' || formobj.eStorageLocation.value !='') &&
		((document.getElementById('checkmeAllChild').checked == false)
			&&(document.getElementById('checkmeSelChild').checked == false))){
	
			alert("<%=MC.M_Selc_ApplyCriterion%>")/*alert("Please select an Applying criterion")*****/
			return false;
		}
	
		if ((document.getElementById('checkmeAllChild').checked == true) || (document.getElementById('checkmeSelChild').checked == true)){
	
			if (formobj.selectEdit.checked){
	
				if ((formobj.ecProcType.value =='' && formobj.eStorageLocation.value=='')  ){
	
					alert("<%=MC.M_Sel_Proc%>")/*alert("Please select either the Process or Location")*****/
					return false;
				}
			}
	
			if ((formobj.ecProcType.value !='' || formobj.eStorageLocation.value !='') ){
	
				return true;
	
			}
	
	
		}else if ((document.getElementById('checkmeAllChild').checked == false) &&	(document.getElementById('checkmeSelChild').checked == false)){
	
			if (formobj.selectEdit.checked){
	
				if ((formobj.ecProcType.value !='' || formobj.eStorageLocation.value!='') ){
	
					alert("<%=MC.M_Selc_ApplyCriterion%>")/*alert("Please select an Applying criterion")*****/
					return false;
	
				}else if ((formobj.ecProcType.value =='' && formobj.eStorageLocation.value=='')){
	
	//JM: 22Sept2009: #4127
					//alert("Please select either the Process or Location")
					//return false;
				}
	
			}
	
		}
	}
	
	
	
	if(counter==0 && formobj.selectEdit.checked==false){
	
	
		if ((formobj.ecProcType.value !='' || formobj.eStorageLocation.value !='') &&
		((document.getElementById('checkmeAllChild').checked == false)
			&&(document.getElementById('checkmeSelChild').checked == false))){
	
			alert("<%=MC.M_Sel_Proc%>")/*alert("Please select either the Process ")*****/
			return false;
		}
	
	
		if ((document.getElementById('checkmeAllChild').checked == true)
			|| (document.getElementById('checkmeSelChild').checked == true) ){
	
	
			if ((formobj.ecProcType.value =='' && formobj.eStorageLocation.value=='')  ){
	
				alert("<%=MC.M_Sel_Proc%>")/*alert("Please select either the Process")*****/
				return false;
			}
	
			if ((formobj.ecProcType.value !='' || formobj.eStorageLocation.value !='') && counter==0){
	
				if (document.getElementById('checkmeAllChild').checked == true){
	
					return true;
	
				}else{
					alert("<%=MC.M_SelChildSpmenId_ToEdit%>")/*alert("Please select the Child Specimen id to edit")*****/
					return false;
				}
	
			}
	
		}
	
	}
	
	}//if (formobj.selectEdit!= undefined)
	}//end of mode
	
	
	
	
	
	
	//for more than an existing child
	
	if (childct >0){
	
	
		if ((formobj.ecProcType.value !='' || formobj.eStorageLocation.value !='') && ((document.getElementById('checkmeAllChild').checked == false)
			&&(document.getElementById('checkmeSelChild').checked == false))){
			alert("<%=MC.M_Selc_ApplyCriterion%>")/*alert("Please select an Applying criterion")*****/
			return false;
		}
	
	
		if ((document.getElementById('checkmeAllChild').checked == true)
			|| (document.getElementById('checkmeSelChild').checked == true) ){
	
	
			if ((formobj.ecProcType.value =='' && formobj.eStorageLocation.value=='')  ){
	
				alert("<%=MC.M_Sel_Proc%>")/*alert("Please select either the Process")*****/
				return false;
			}
	
			if ((formobj.ecProcType.value !='' || formobj.eStorageLocation.value !='') && counter==0){
	
				if (document.getElementById('checkmeAllChild').checked == true){
	
					return true;
	
				}else{
					alert("<%=MC.M_SelChildSpmenId_ToEdit%>")/*alert("Please select the Child Specimen id(s) to edit")*****/
					return false;
				}
	
			}
	
		}
	
		//outside the for loop
	
	
		for (a=0;a<childct;a++){
	
			if (formobj.selectEdit[a].checked){
	
				if ((document.getElementById('checkmeAllChild').checked == true) || (document.getElementById('checkmeSelChild').checked == true)){
	
					if (formobj.selectEdit[a].checked){
	
						if ((formobj.ecProcType.value =='' && formobj.eStorageLocation.value=='')  ){
	
							alert("<%=MC.M_Sel_Proc%>")/*alert("Please select either the Process")*****/
							return false;
						}
					}
	
					if ((formobj.ecProcType.value !='' || formobj.eStorageLocation.value !='') ){
	
						return true;
	
					}
	
	
				}else if ((document.getElementById('checkmeAllChild').checked == false) &&			(document.getElementById('checkmeSelChild').checked == false)){
	
					if (formobj.selectEdit[a].checked){
	
						if ((formobj.ecProcType.value !='' || formobj.eStorageLocation.value!='') ){
	
							alert("<%=MC.M_Selc_ApplyCriterion%>")/*alert("Please select an Applying criterion")*****/
							return false;
	
						}else if ((formobj.ecProcType.value =='' && formobj.eStorageLocation.value=='')){
	//JM: 22Sept2009: #4127
							//alert("Please select either the Process or Location")
							//return false;
						}
	
					}
	
				}
	
			}//end of true check
	
		}//end of for
	
	
	if ((formobj.ecProcType.value =='' && formobj.eStorageLocation.value=='') && formobj.eProcDate.value!=''){
	
		alert("<%=MC.M_Sel_Proc%>")/*alert("Please select either the Process")*****/
		return false;
	}
	
	}//end of if
	/******************************************************************************/
	
	
		
	if(formobj.mode.value=='M'){
		if(formobj.prevCollQuantity.value != formobj.origQuantity.value){
			var i;
			var statuslen = document.getElementById('statuslength').value;
			var child_count = document.getElementById('childcount').value;
			if(child_count > 0){
				for (i=0;i<child_count;i++){
					var childcolqty=document.getElementById('specorigqnty_'+i).value;
					var childcurrqty=document.getElementById('specqnty_'+i).value;
					if(childcolqty > 0 || childcurrqty > 0){
						alert("<%=MC.M_Bfr_Modifyng_Spcmn_Coll_Amnt%>");
						formobj.origQuantity.value = formobj.prevCollQuantity.value;
						formobj.origQuantity.focus();
						return false ;}}}
			if(statuslen > 0){
				for(i=0;i<statuslen;i++){
					var specstatqty=document.getElementById('statusqty_'+i).value;
					if(specstatqty > 0){
						alert("<%=MC.M_Bfr_Modifyng_Spcmn_Coll_Amnt%>");
						formobj.origQuantity.value = formobj.prevCollQuantity.value;
						formobj.origQuantity.focus();
						return false;
					}}}}
	}


		if (formobj.selStudyIds.value == '' || formobj.patientIds.value == '') {
			if (!confirm(' <%=MC.M_SureWantLve_StdPatBlk%>')) {/*if (!confirm('Are you sure you want to leave the <%=LC.Std_Study%> and/or <%=LC.Pat_Patient%> ID field blank?')) {*****/
				if (formobj.selStudyIds.value == '') { formobj.selStudy.focus(); }
				else { formobj.patientNames.focus(); }
				return false;
			}
		}
	
	}
	
	function openUserWindow(frm) {
			windowName = window.open("usersearchdetails.jsp?fname=&lname=&from="+ frm,"TEST","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
			windowName.focus();
	}
	
	function openCommonUser(formname,idfldname,descfldname)
	{
			windowName = window.open("usersearchdetails.jsp?fname=&lname=&from=&genOpenerFormName="+formname+"&genOpenerUserIdFld="+idfldname+"&genOpenerUserNameFld="+ descfldname,"usercommon","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
			windowName.focus();
	}
	
	function openStudyWindow(formobj) {
		formobj.target="Lookup";
		formobj.method="post";
		formobj.action="multilookup.jsp?viewId=6013&form=specimen&seperator=,"+
	                  "&keyword=selStudy|STUDY_NUMBER~selStudyIds|LKP_PK|[VELHIDE]&maxselect=1";
	
		formWin =open('donotdelete.html','Lookup','resizable=1,status=0, width=850,height=550 top=100,left=100,menubar=no,scrollbars=1');
	
		if (formWin && !formWin.closed) formWin.focus();
		formobj.submit();
		formobj.target="_self";
		//JM: 21Aug2007: issue #3115
		formobj.action="updatespecimendetails.jsp";
		void(0);
	}
	
	
	
	function openLookup(formobj) {
		formobj.target="Lookup";
		formobj.method="post";
		formobj.action="multilookup.jsp?viewId=6017&form=specimen&seperator=,"+
	                  "&keyword=patientNames|PERSON_CODE~patientIds|LKP_PK|[VELHIDE]&maxselect=1";
	
		formWin =open('donotdelete.html','Lookup','resizable=1,status=0, width=850,height=550 top=100,left=100,menubar=no,scrollbars=1');
		if (formWin && !formWin.closed) formWin.focus();
		formobj.submit();
		formobj.target="_self";
		formobj.action="updatespecimendetails.jsp";
		void(0);
	}
	
	
	//JM: 01Jun2009, processLink introduced: decides if ddown item is processed ..
	
	function openWin(pkSpecimen,pkSpecimenStatus,mode,pgright, processLink, openFrom){
	
		var specUnitIndex = document.specimen.specQuantityUnit.selectedIndex;
		var specUnit = document.specimen.specQuantityUnit.options[specUnitIndex].value;
		//var avlQnty  = document.specimen.actavailQty.value;
		var avlQnty  = document.specimen.avlQuantity.value;
		//alert(document.specimen.avlQuantity.value);
		var currAmt;
		if(openFrom==''){
			currAmt=document.getElementById("currAmt").value;
		}
		else{
			currAmt=-1;
		}
	
		if (!f_check_perm(pgright,'E'))
			{
				return false;
			}
		if(!(parseFloat(currAmt)<0)){
	     	param = "specimenstatus.jsp?pkSpecimen="+pkSpecimen+"&pkSpecimenStatus="+pkSpecimenStatus+"&mode="+mode+"&processLink="+processLink+"&openFrom="+openFrom+"&specQtyUnit="+specUnit+"&avlQnty="+avlQnty+"&currAmt="+currAmt;
		}
		else{
			param = "specimenstatus.jsp?pkSpecimen="+pkSpecimen+"&pkSpecimenStatus="+pkSpecimenStatus+"&mode="+mode+"&processLink="+processLink+"&openFrom="+openFrom+"&specQtyUnit="+specUnit+"&avlQnty="+avlQnty;
		}
	
			var var_height = "";
			if (processLink !='')
			var_height = 500
			else
			var_height = 350
	
			windowName= window.open(param,"_new","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=false,width=1000,height="+var_height);
			windowName.focus();
	}
	
	
	//Added by Manimaran for deletion of Specimen status.
	function f_delete(pkSpecimenStat,len,pgright, procLnk, statQuanity, status)
	{
		if (!f_check_perm(pgright,'E'))
			{
				return false;
			}
		if( len > 1) {
		//Yogendra Pratap Singh : Modified for Bug#-6674 : Date 02 April 2012
			var actQty =parseFloat(specimen.avlQuantity.value); 
			var finalQty = actQty - statQuanity;
			var specId=document.specimen.specimenPkId.value;
			if((status=='Collected' || status=='Received')){	
				if(finalQty < 0){
					alert("<%=MC.M_After_Del_Neg_Not_Allowed%>");
					/*alert("Status can not delete. After deleting the status Current Amount becomes less then zero.");****/
					return false;
				}
			}
			if (confirm("<%=MC.M_WantDel_SpmenStat%>"))/*if (confirm("Do you want to delete this Specimen status?"))*****/
			{
				windowName= window.open("specimenstatusdelete.jsp?pkSpecimenStat="+pkSpecimenStat+"&specId="+specId,"statdel","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=650,height=350");
				windowName.focus();
			} else
			{
				return false;
			}
		}
		else
		{
	//JM: 15Jul2009: 4125
			if (procLnk!=''){
				if (confirm("<%=MC.M_WantDel_SpmenStat%>"))/*if (confirm("Do you want to delete this Specimen status?"))*****/
				{
					windowName= window.open("specimenstatusdelete.jsp?pkSpecimenStat="+pkSpecimenStat,"statdel","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=650,height=350");
					windowName.focus();
				} else
				{
					return false;
				}
			}else{
				alert("<%=MC.M_CntDel_LastStatSpmen%>");/*alert("You can not delete the last/only status for a Specimen");*****/
			}
		}
	}
	<!---code for bug id #24027-->
	function deleteSpecimens(pk_specimen,pageRight)
	{
		if (! f_check_perm(pageRight,'E'))
	    {
	    	return false;
	    }
	    var   msg= "<%=MC.M_ActDelSpmen_SelWantCont%>"/*"This action will also " +
			  //"delete all the child Specimens linked with the selected specimen(s). Are you " +
			  //"sure you want to continue? "*****/ ;
		if(confirm(msg)) 
		{
			window.open("deletespecimens.jsp?pk_specimen="+pk_specimen+"&flag=N","_self");
	    }
		else
		{
			return false;
		}
	  	   //window.open("deletespecimens.jsp?pk_specimen="+pk_specimen+"&flag=N");
	 }
	
	
	function openLocLookup(formobj, turn) {
	
	
		//KM-#3279
	/*	mode = formobj.mode.value;
	
		formobj.target="Lookup";
		formobj.method="post";
		formobj.action="multilookup.jsp?viewId=6050&form=specimen&seperator=,"+
	                  "&keyword=storageLoc|storage_name~mainFKStorage|pk_storage|[VELHIDE]&maxselect=1";
	
		formWin =open('donotdelete.html','Lookup','resizable=1,status=0, width=850,height=550 top=100,left=100,menubar=no,scrollbars=1');
		if (formWin && !formWin.closed) formWin.focus();
		formobj.submit();
		formobj.target="_self";
		formobj.action="updatespecimendetails.jsp";
		void(0);*/
		var PKStorageID=formobj.mainFKStorage.value;
		if (turn == 1){
		 windowName= window.open("searchLocation.jsp?gridFor=SP&form=specimen&locationName=storageLoc&storagepk=mainFKStorage&PKStorageID="+PKStorageID,"Grid","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=900,height=665, top=100, left=90");
		}else if (turn == 2){
	
		 windowName= window.open("searchLocation.jsp?gridFor=SP&form=specimen&locationName=storageLocation&storagepk=mainFKStorageId&PKStorageID="+PKStorageID,"Grid","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=900,height=665,top=100, left=90");
		}else if (turn == 3){
		 windowName= window.open("searchLocation.jsp?gridFor=SP&form=specimen&locationName=eStorageLocation&storagepk=eMainFKStorageId&PKStorageID="+PKStorageID,"Grid","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=900,height=665,top=100, left=90");
	
		}
	
		windowName.focus();
	}
	
	function removeLocation(formobj, turn) {
	
	
	
		if (turn == 1){
			formobj.storageLoc.value = "";
			formobj.mainFKStorage.value ="";
		}else if(turn==2){
			formobj.storageLocation.value = "";
			formobj.mainFKStorageId.value ="";
		}else if(turn==3){
			formobj.eStorageLocation.value = "";
			formobj.eMainFKStorageId.value ="";
	
		}
	}
	
	function removeLocationChild(formobj,specpk) {
	
	    fldname = document.getElementById("childlocation_e_text_" + specpk);
	    fldid  = document.getElementById("childlocation_pk_" + specpk);
	
		fldname.value = "";
		fldid.value ="";
	}
	
	
	//location lookup for child specimen
	function openLocLookupChild(formobj,specpk) {
	
	  	var namefld;
	  	var fkfld;
		var storageCapacity;
	  	var multiSpecimen;
		var capacityUnit;
	  	var storageCount;
		var storageChildCount;
	  	namefld = "childlocation_e_text_" + specpk;
	  	fkfld =  "childlocation_pk_" + specpk;
		storageCapacity =  "storageCapacity" + specpk;
		multiSpecimen =  "multiSpecimen" + specpk;
		capacityUnit =  "capacityUnit" + specpk;
		storageCount =  "storageCount" + specpk;
		storageChildCount =  "storageChildCount" + specpk;
		var PKStorageID=eval("formobj.childlocation_pk_"+specpk+".value");
		windowName= window.open("searchLocation.jsp?gridFor=SP&form=specimen&locationName="+namefld+"&storagepk="+fkfld+"&storageCapacity="+storageCapacity+"&multiSpecimen="+multiSpecimen+"&capacityUnit="+capacityUnit+"&storageCount="+storageCount+"&storageChildCount="+storageChildCount+"&PKStorageID="+PKStorageID,"Grid","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=900,height=665");
		windowName.focus();
	}
	
	
	
	
	function setCurrentTime(formObject, turn){
	var d = new Date();
	var curr_hour = d.getHours();
	var curr_min = d.getMinutes();
	
	var curr_sec = d.getSeconds();
	var curr_msec = d.getMilliseconds();
	
	 if (turn==1) {
		formObject.collectionHH.value=curr_hour;
	
		formObject.collectionMM.value=curr_min;
	
		formObject.collectionSS.value=curr_sec;
	 }else if (turn ==2){
	
		formObject.processHH.value=curr_hour;
	
		formObject.processMM.value=curr_min;
	
		formObject.processSS.value=curr_sec;
	 }else if(turn ==3){
	
		formObject.eprocessHH.value=curr_hour;
	
		formObject.eprocessMM.value=curr_min;
	
		formObject.eprocessSS.value=curr_sec;
	 }
	 else if(turn ==4){
			
			formObject.timeHH.value=curr_hour;
		
			formObject.timeMM.value=curr_min;
		
			formObject.timeSS.value=curr_sec;
		 }
	}
	
	function resetCurrentTime(formObject, turn){
	 if (turn==1) {
		formObject.collectionHH.value="00";
	
		formObject.collectionMM.value="00";
	
		formObject.collectionSS.value="00";
	 }else if (turn ==2){
	
		formObject.processHH.value="00";
	
		formObject.processMM.value="00";
	
		formObject.processSS.value="00";
	 }else if (turn ==3){
	
		formObject.eprocessHH.value="00";
	
		formObject.eprocessMM.value="00";
	
		formObject.eprocessSS.value="00";
	 }
	 else if (turn ==4){
			
			formObject.timeHH.value="00";
		
			formObject.timeMM.value="00";
		
			formObject.timeSS.value="00";
		 }
	}
	
	function autoCalQuant(formobj){
	
	 	num_child = formobj.noOfChild.value
		av_qnty = formobj.avlQuantity.value

		if(av_qnty == '' || av_qnty == 0){
			alert("Child specimen cannot be created when parent specimen's current amount is 0");
			formobj.noOfChild.value = '';
			return false;
		}
	
		if (num_child !='' && num_child > 0){
			var decimalPattern = /^(?:\d*)\.([1-9]{1,3})(?:\1)+$/;
			var wholePattern = /^(?:\d*)+$/;
			var res=(av_qnty / num_child);
			if(decimalPattern.test((res).toFixed(5)))
				{
			formobj.quantity.value= (av_qnty / num_child).toFixed(2);
				}
			else if(wholePattern.test(res))
				{
				formobj.quantity.value= (av_qnty / num_child).toFixed(1);
				}
			else
				{
				formobj.quantity.value= (av_qnty / num_child);
				}
		}else {
			formobj.quantity.value = '';
	
		}
	
	
	}
	
	function setCurrQty(){
		//setting available qty to actual available qty
		specimen.avlQuantity.value = specimen.actavailQty.value;
	}
	
	function autoCalCurrAmt(formobj){
		var currAmt=document.getElementById("currAmt").value;
		var newCollAmt;
				
	 	//if (formobj.mode.value=='N'){
	 	//	formobj.avlQuantity.value=formobj.origQuantity.value
	 	//	}
	 	if(formobj.mode.value=='M'){
	 		if(!(parseFloat(currAmt)<0))
			{
				if(parseFloat(currAmt)<parseFloat(formobj.origQuantity.value)){
					alert("<%=MC.M_Chld_QntyAmt_Exceeds_Parent_Not_Allowed%>");/*alert("The total Quantity of Child Specimen should not exceed the total Quantity of Parent Specimen.")*****/
					formobj.origQuantity.value=formobj.prevCollQuantity.value;
					formobj.avlQuantity.value=formobj.origavlQuantity.value
					return false;
				}
			}
	 		//if(parseFloat(formobj.prevCollQuantity.value)!=parseFloat(formobj.origQuantity.value)){
		 	//	if(parseFloat(formobj.prevCollQuantity.value)>parseFloat(formobj.origQuantity.value)){
		 	//		newCollAmt=parseFloat(formobj.prevCollQuantity.value)-parseFloat(formobj.origQuantity.value)
		 	//	}
		 	//	else if(parseFloat(formobj.prevCollQuantity.value)<parseFloat(formobj.origQuantity.value)){
		 	//		newCollAmt=parseFloat(formobj.origQuantity.value)-parseFloat(formobj.prevCollQuantity.value)
			//	}
				
			//	if(parseFloat(formobj.newCollAmt.value)!=parseFloat(newCollAmt)){
			//		formobj.newCollAmt.value=(parseFloat(newCollAmt)).toFixed(2);
		 	//		formobj.avlQuantity.value=parseFloat(formobj.origavlQuantity.value)+parseFloat(formobj.newCollAmt.value)
			//	}

				if(formobj.origQuantity.value==''){
					formobj.avlQuantity.value="";
				}
		 	
	 	}
	}
	
	function autoCalCurrDrpDwn(formobj){
		var specUnitIndex = formobj.specBaseOrigQuUnit_dd.selectedIndex;	
		formobj.specQuantityUnit.selectedIndex=specUnitIndex;
		
	}
	function hideUnhideProsTyp(status,formobj,childpk,processType){
		var selectedSts = status.options[status.selectedIndex].value;
		var indx = -1;
		if (formobj.statusTyps!= undefined && (selectedSts!= undefined && selectedSts != '' && selectedSts.length >0)){
			var statusTyps = formobj.statusTyps.value;
			indx = statusTyps.indexOf(selectedSts); 	
			if(indx > -1 && processType > 0 ){
				document.getElementById('specproctype_'+ childpk).style.display='none';
				document.getElementById('specproctype_e_' + childpk ).style.display='block';
			}else
			{
				document.getElementById('specproctype_'+childpk).style.display='none';
				document.getElementById('specproctype_e_' + childpk).style.display='none';
			}
		}
		else if(processType > 0)
		{
			document.getElementById('specproctype_'+childpk).style.display='none';
			document.getElementById('specproctype_e_' + childpk).style.display='none';
		}
		
		
	}
	
	// Pritam Singh: For INV 22399 - AJAX
	var gv_specimenPK, gv_specimenName;
	function CheckDuplicate(SpecName,SpecPK)
	{
		var Child_Count = document.getElementById('childcount').value;
		for(count=0;count<Child_Count;count++)
		{
			Child_PK = document.getElementById('childSpecIds_'+count).value;
			Child_Name = document.getElementById('childid_'+Child_PK).value;
			Child_Name = trimString(Child_Name);
			if((Child_Name==SpecName) && (SpecPK != Child_PK))
			{
				var paramArray = [Child_Name];
				alert(getLocalizedMessageString("M_ChldSpec_Duplicate",paramArray));
				document.getElementById('childid_'+Child_PK).focus();
				return false;
			}
		}
	}
	
	function processAjaxCall(v_specimenPK) 
	{
		gv_specimenPK = v_specimenPK;
		var v_specimenName = document.getElementById('childid_'+v_specimenPK).value;
		v_specimenName = trimString(v_specimenName);
		gv_specimenName = v_specimenName;
		xmlHttp = getXmlHttpObject();
		var urlParameters = "ajaxValidateSpecimen.jsp?v_pkSpecID="+v_specimenPK+"&v_specName="+v_specimenName;
		xmlHttp.onreadystatechange = stateChanged;
		xmlHttp.open("GET", urlParameters, true);
		xmlHttp.send(null);
	}	

	function getXmlHttpObject() {
		var xmlHttp = null;
		try {
			// Firefox, Opera 8.0+, Safari
			xmlHttp = new XMLHttpRequest();
		} catch (e) { //Internet Explorer
			try {
				xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
		}
		return xmlHttp;
	}
	
	// Purpose: To Trim the Passed String
	function trimString(stringToTrim) 
	{
		return stringToTrim.replace(/^\s+|\s+$/g, "");
	}

	function stateChanged() 
	{
		var resultArray;
		if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete") {
			if (xmlHttp.status == 200) 
			{
				showDataChild = xmlHttp.responseText;
				resultArray = trimString(showDataChild).split("$");
				var msg=resultArray[1];
				if(msg.length>1)
				{
					alert(msg);
					document.getElementById('childid_'+gv_specimenPK).focus();
					return;
				}
				CheckDuplicate(gv_specimenName, gv_specimenPK);
			}
			else
			{
				alert("Error occured in processing the page, Please try again.");
			}
		}
	}
function callAjaxGetAnatomic_siteDD(formobj){
 
	   selval = formobj.anatomic_site_dd.value;
 	   new VELOS.ajaxObject("getCodeDropDown.jsp", {
   		urlData:"selectedVal="+selval + "&ddName=tissue_site_dd&codeType=tissue_side&ddType=child" ,
		   reqType:"POST",
		   outputElement: "span_tissue_side" }

   ).startRequest();

}	
jQuery( function() {
	
	jQuery( "#userPathologistName" ).autocomplete({
        
        source : function(request, response) {
        	jQuery.ajax({
                 url : "userNetworkAutoCompleteJson.jsp",
                 type : "POST",
                 autofocus:true,

                 data : {
                	 term : request.term
                 },
				dataType : "json",
				async:false,
                 success: function(data){
                	 	 
                 
                     response( jQuery.map( data, function( item ) {
                         return {
                             label: item.name,
                             userId: item.pk_user     // EDIT
                         }
                     }));
                     
                  },
                  
          });
       },
       select : function(event, ui) {
    	   var assignUserId=ui.item.label
    	   jQuery("#userPathologistId").val(ui.item.userId);
           jQuery("#userPathologistName").val(assignUserId);
          
       }
    
    });
jQuery( "#userSurgeonName" ).autocomplete({
        
        source : function(request, response) {
        	jQuery.ajax({
                 url : "userNetworkAutoCompleteJson.jsp",
                 type : "POST",
                 autofocus:true,

                 data : {
                	 term : request.term
                 },
				dataType : "json",
				async:false,
                 success: function(data){
                	 	 
                 
                     response( jQuery.map( data, function( item ) {
                         return {
                             label: item.name,
                             userId: item.pk_user     // EDIT
                         }
                     }));
                     
                  },
                  
          });
       },
       select : function(event, ui) {
    	   var assignUserName=ui.item.label
    	   jQuery("#userSurgeonId").val(ui.item.userId);
           jQuery("#userSurgeonName").val(assignUserName);
          
       }
    
    });
    
jQuery( "#userCollTechName" ).autocomplete({
    
    source : function(request, response) {
    	jQuery.ajax({
             url : "userNetworkAutoCompleteJson.jsp",
             type : "POST",
             autofocus:true,

             data : {
            	 term : request.term
             },
			dataType : "json",
			async:false,
             success: function(data){
            	 	 
             
                 response( jQuery.map( data, function( item ) {
                     return {
                         label: item.name,
                         userId: item.pk_user     // EDIT
                     }
                 }));
                 
              },
              
      });
   },
   select : function(event, ui) {
	   var assignUserId=ui.item.label
	   jQuery("#userCollTechId").val(ui.item.userId);
       jQuery("#userCollTechName").val(assignUserId);
      
   }

});

jQuery( "#userProcTechName" ).autocomplete({
    
    source : function(request, response) {
    	jQuery.ajax({
             url : "userNetworkAutoCompleteJson.jsp",
             type : "POST",
             autofocus:true,

             data : {
            	 term : request.term
             },
			dataType : "json",
			async:false,
             success: function(data){
            	 	 
             
                 response( jQuery.map( data, function( item ) {
                     return {
                         label: item.name,
                         userId: item.pk_user     // EDIT
                     }
                 }));
                 
              },
              
      });
   },
   select : function(event, ui) {
	   var assignUserId=ui.item.label
	   jQuery("#userProcTechId").val(ui.item.userId);
       jQuery("#userProcTechName").val(assignUserId);
      
   }

});
  } );	

	
	</SCRIPT>
<jsp:include page="ui-include.jsp" flush="true"></jsp:include>
	
	
	<body style="overflow:auto;">
	
	
	
	
	<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.StringUtil"%>
	<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.person.*,com.velos.eres.service.util.*,com.velos.eres.web.user.*,com.velos.eres.web.grpRights.GrpRightsJB"%>
	<%@ page import="org.json.*"%>
	<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.StringUtil" %>
	<jsp:useBean id="usrSite" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" />
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<jsp:useBean id="specJB" scope="request" class="com.velos.eres.web.specimen.SpecimenJB"/>
	<jsp:useBean id="stdJB" scope="request" class="com.velos.eres.web.study.StudyJB"/>
	<jsp:useBean id="usrJB" scope="request" class="com.velos.eres.web.user.UserJB"/>
	<jsp:useBean id="patJB" scope="request" class="com.velos.eres.web.person.PersonJB"/>
	<jsp:useBean id="specStatJB" scope="request" class="com.velos.eres.web.specimenStatus.SpecimenStatusJB"/>
	<jsp:useBean id ="qtySpecJB" scope="request" class="com.velos.eres.web.specimen.SpecimenJB"/>
	<jsp:useBean id="storageB" scope="request" class="com.velos.eres.web.storage.StorageJB"/>
	<jsp:useBean id="eventAssocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
	<jsp:useBean id="pProtJB" scope="request" class="com.velos.eres.web.patProt.PatProtJB"/>
	<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
	<jsp:useBean id="commonB" scope="request" class="com.velos.eres.web.common.CommonJB"/>
	<jsp:useBean id="mdJB" scope="request" class="com.velos.eres.web.moreDetails.MoreDetailsJB"/>
	<%
	HttpSession tSession = request.getSession(true);
	
	if (sessionmaint.isValidSession(tSession))
	{
	
	String userIdFromSession = (String) tSession.getValue("userId");
	String userId="";
		String acc="";
	
	userId = (String) tSession.getValue("userId");
	int pageRight = 0;
	
		 GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");
	
		 pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MSPEC"));
	
		String defUserGroup = (String) tSession.getAttribute("defUserGroup");
	
	ArrayList pkIds = new ArrayList();
	ArrayList statusDates = new ArrayList();
	ArrayList statuses = new ArrayList();
	ArrayList quantities = new ArrayList();
	ArrayList studies = new ArrayList();
	ArrayList recipients = new ArrayList();
	ArrayList notes = new ArrayList();
	String statusDate = "";
	String status = "";
	String quantity = "";
	String study = "";
	String recipient = "";
	String note = "";
	String CopyFieldsTxt1 ="Description, Collection Date/Time, Owner, Anatomic Site, Tissue Side, Pathological Status, Study, Patient ID,Organization, Visit/Event, Patient Study ID, Notes, Pathologist, Surgeon, Collecting Technician, Processing Technician";
	/* String CopyFieldsTxt3 ="Pathologist, Surgeon, Collecting Technician, Processing Technician"; */
	
	String FromPatPage=request.getParameter("FromPatPage");
	if(FromPatPage==null){FromPatPage="";}
	String patientCode=request.getParameter("patientCode");
	if(patientCode==null){patientCode="";}
	String patientId1=request.getParameter("pkey");
	if(patientId1==null){patientId1="";}
	

	//JM: 27Jun2009: #INVP2.7.2 (part a)
	String isSpecReadonly = ctrl.getControlValue("specid_readonly");
	isSpecReadonly=(isSpecReadonly==null)?"0":isSpecReadonly;
	
	
	String mode= "";
	mode = request.getParameter("mode");
	
	// -- Add access control logic
	 if (pageRight < 4) { // No view access
	 %>
	 <jsp:include page="accessdenied.jsp" flush="true"/>
	 <div class = "myHomebottomPanel"><jsp:include page="bottompanel.jsp" flush="true"/></div></body></html>
	 <% return;
	 }
	 if (pageRight == 6) { // No new access
	     if (EJBUtil.isEmpty(mode) || "N".equals(mode)) {
	     %>
	     <jsp:include page="accessdenied.jsp" flush="true"/>
	     <div class = "myHomebottomPanel"><jsp:include page="bottompanel.jsp" flush="true"/></div></body></html>
	     <% return;
	     }
	 }
	// -- End of access control logic
	
	 if (EJBUtil.isEmpty(mode))
		mode="N";
	
		String  specId = "";
		String  alterId = "";
		String  descn = "";
		String  type = "";	String procType ="";
		String  specQuUnit = "";
		String  collDt = "";
		String  collDtFormatted = "";
		String  ownr = "", ownerName ="";
		String  avQty = "";
		String  stdy = "", studyNumber = "";
		String  pat = "", patientId ="";
		String  site = "";
		String noteDisp = "";
	
		String oldQty = "";
		String fkStorage = "";
		String oldFkStorage="";
		String storageLocationName="";
		int fkStorageInt = 0;
	
		String TOFreezeSS = "0";
		String TOFreezeHH = "0";
		String TOFreezeMM = "0";
	
		String TORemovalSS = "0";
		String TORemovalMM = "0";
		String TORemovalHH = "0";
	
	
		//JM: 20May2009:
		String collectionSS = "00";
		String collectionMM = "00";
		String collectionHH = "00";
	
	
		String processHH = "00";
		String processMM = "00";
		String processSS = "00";
	
	
		String eprocessHH = "00";
		String eprocessMM = "00";
		String eprocessSS = "00";
	
		String timeHH="00";
		String timeMM="00";
		String timeSS="00";
		
		String userProcTechId = "";
		String userProcTechName = "";
		String userCollTechId = "";
		String userCollTechName = "";
		String userSurgeonName = "";
		String userSurgeonId = "";
	
		String userPathologistName = "";
		String userPathologistId ="";
	
		String removalTime ="";
		String freezeTime = "";
		String events1PK = "";
		String eventVisitName = "";
	
	
		String  pkId = request.getParameter("pkId");
		
		SpecimenDao specimenDao=new SpecimenDao();
		float currAmt=specimenDao.getParentCurrAmt(StringUtil.stringToNum(pkId));
	
		if (StringUtil.isEmpty(pkId))
		{
			pkId = "";
		}
	
	    if (mode.equals("M")) {
	
	
		specJB.setPkSpecimen(EJBUtil.stringToNum(pkId));
		specJB.getSpecimenDetails();
	
		// -- Access control based on fk_study and fk_per
		String tmpFkStudy = specJB.getFkStudy();
		String tmpFkPer = specJB.getFkPer();
		System.out.println("tmpFkStudy="+tmpFkStudy+" tmpFkPer="+tmpFkPer);
		if (tmpFkStudy != null) {
		    TeamDao teamDao = new TeamDao();
		    teamDao.getTeamRights(EJBUtil.stringToNum(tmpFkStudy),EJBUtil.stringToNum(userId));
		    ArrayList teamIds = teamDao.getTeamIds();
		    if (teamIds == null || teamIds.size() < 1) {
		    %>
		        <jsp:include page="accessdenied.jsp" flush="true"/>
		        <div class = "myHomebottomPanel"><jsp:include page="bottompanel.jsp" flush="true"/></div></body></html>
		    <%  return;
		    }
		}
		if (tmpFkPer != null) {
			int tmpOrgRight = usrSite.getUserPatientFacilityRight(EJBUtil.stringToNum(userId),
			        EJBUtil.stringToNum(tmpFkPer));
		    if (tmpOrgRight < 4) {
			%>
		        <jsp:include page="accessdenied.jsp" flush="true"/>
		        <div class = "myHomebottomPanel"><jsp:include page="bottompanel.jsp" flush="true"/></div></body></html>
		    <%  return;
		    }
		}
		// -- End of access control based on fk_study and fk_per
	
		fkStorage = specJB.getFkStorage();
	
		if (StringUtil.isEmpty(fkStorage))
		{
			fkStorage = "";
	
		}
		oldFkStorage=fkStorage;
	
		fkStorageInt =EJBUtil.stringToNum(fkStorage);
	
		events1PK =  specJB.getFkSchEvents();
	
	
		if (! StringUtil.isEmpty(events1PK))
		{
		  Hashtable htReturn = new Hashtable();
		  String evname = "";
		  String visitname = "";
	
		  htReturn = eventAssocB.getVisitEventForScheduledEvent(events1PK);
	
		  if (htReturn != null)
		  {
		    if (htReturn.containsKey("eventName"))
		    {
		      evname = (String) htReturn.get("eventName");
		    }
	
		    if (htReturn.containsKey("visitName"))
		    {
		      visitname = (String) htReturn.get("visitName");
		    }
		    if (! StringUtil.isEmpty(evname))
		    {
		      eventVisitName = visitname + "/"+ evname;
		    }
		  }
		}
	
		if (fkStorageInt > 0)
		{
	
			storageB.setPkStorage(fkStorageInt);
			storageB.getStorageDetails();
			storageLocationName = storageB.getStorageName();
	
		}
	
	
		specId = specJB.getSpecimenId();
		specId=(specId==null)?"":specId;
	
		alterId = specJB.getSpecAltId();
		alterId=(alterId==null)?"":alterId;
	
		descn = specJB.getSpecDesc();
		descn=(descn==null)?"":descn;
	
		type  = specJB.getSpecType();
		type=(type==null)?"":type;
	
	
		//JM: 28Jan2008
		procType  = specJB.getSpecProcType();
		procType=(procType==null)?"":procType;
	
		specQuUnit  = specJB.getSpectQuntyUnits();
		specQuUnit=(specQuUnit==null)?"":specQuUnit;
	
		collDt = specJB.getSpecCollDate();
	
		Double avQnty=0.0;
		avQty = specJB.getSpecOrgQnty();
		avQty=(avQty==null)?"":avQty;
											  
		Pattern pattern=Pattern.compile("[0-9]*.(\\d){5}");
		 Pattern exp=Pattern.compile("[0-9]*.E[0-9]*");
		 Matcher matcher = pattern.matcher("");

		if(!avQty.equals(""))
		{
			matcher=exp.matcher(avQty);
			if(matcher.find())
			{
		
			avQty=new BigDecimal(avQty).toPlainString();	
			matcher = pattern.matcher(avQty);
			}
			else
			{
			 avQnty=Double.parseDouble(avQty);
			}
			if(matcher.find())
			{
				avQty= (new BigDecimal(avQty).setScale(5, RoundingMode.HALF_UP)).toPlainString();
				
				
			}
			
			else
			{
				avQty=avQnty.toString() ;
			}
		}		
		oldQty = avQty;
	
		stdy = specJB.getFkStudy();
		stdy=(stdy==null)?"":stdy;
	
		//get the study number to disply
		stdJB.setId(EJBUtil.stringToNum(stdy));
		stdJB.getStudyDetails();
		studyNumber = stdJB.getStudyNumber();
		studyNumber=(studyNumber==null)?"":studyNumber;
	
		pat = specJB.getFkPer();
		pat=(pat==null)?"":pat;
		patJB.setPersonPKId(EJBUtil.stringToNum(pat));
		patJB.getPersonDetails();
	
		patientId = patJB.getPersonPId();
		patientId=(patientId==null)?"":patientId;
	
	
		site = specJB.getFkSite();
		site=(site==null)?"":site;
	
		noteDisp = specJB.getSpecNote();
		}
	
		ownr = specJB.getSpecOwnerFkUser();
		//get the user name to display
		//JM: 17Aug2007: issue #3102
		if (mode.equals("N")) {
	
		ownr=userIdFromSession;
	
		}else{
	
		ownr=(ownr==null)?"":ownr;
	
		}
		usrJB.setUserId(EJBUtil.stringToNum(ownr));
		usrJB.getUserDetails();
		String fName=usrJB.getUserFirstName();
			fName=(fName==null)?"":fName;
		String lName=usrJB.getUserLastName();
			lName=(lName==null)?"":lName;
		ownerName =  fName + " " + lName ;
	
		//get other user names
	
		userProcTechId =   specJB.getSpecUserProcessingTech();
		userSurgeonId = specJB.getSpecUserSurgeon();
		userPathologistId = specJB.getSpecUserPathologist();
		userCollTechId = specJB.getSpecUserCollectingTech();
	    removalTime =   specJB.getSpecRemovalDatetime();
	    freezeTime =    specJB.getSpecFreezeDatetime();
	
	
	
		java.sql.Timestamp tsRemoval  = null ;
		 java.sql.Timestamp tsFreeze  = null ;
	
		 if (! StringUtil.isEmpty(removalTime))
		 {
			tsRemoval = DateUtil.stringToTimeStamp(removalTime, null);
	
			TORemovalSS = String.valueOf(tsRemoval.getSeconds());
			TORemovalMM = String.valueOf(tsRemoval.getMinutes());
			TORemovalHH = String.valueOf(tsRemoval.getHours());
	
	
		}
	
		if (! StringUtil.isEmpty(freezeTime))
		 {
			tsFreeze  = DateUtil.stringToTimeStamp(freezeTime , null);
	
			TOFreezeSS = String.valueOf(tsFreeze.getSeconds());
			TOFreezeMM = String.valueOf(tsFreeze.getMinutes());
			TOFreezeHH = String.valueOf(tsFreeze.getHours());
	
	
		}
	
	/* JM: 20May2009: added, enhacement #INVP2.7.1 */
		java.sql.Timestamp CollectionTime  = null ;
	
		 if (! StringUtil.isEmpty(collDt))
		 {
			CollectionTime = DateUtil.stringToTimeStamp(collDt, null);
	
			collectionSS = String.valueOf(CollectionTime.getSeconds());
			collectionSS=(collectionSS==null || collectionSS.equals("0"))?"00":collectionSS;
	
			collectionMM = String.valueOf(CollectionTime.getMinutes());
			collectionMM=(collectionMM==null || collectionMM.equals("0"))?"00":collectionMM;
	
			collectionHH = String.valueOf(CollectionTime.getHours());
			collectionHH=(collectionHH==null || collectionHH.equals("0"))?"00":collectionHH;
	
		 }
	
		//following is related to the time of removal and time of freeze too!
		collDtFormatted = DateUtil.dateToString(DateUtil.stringToTimeStamp(collDt, null));
	
	
	
		if (! StringUtil.isEmpty(userProcTechId))
		{
	
			usrJB.setUserId(EJBUtil.stringToNum(userProcTechId));
			usrJB.getUserDetails();
	
			fName=usrJB.getUserFirstName();
				fName=(fName==null)?"":fName;
	
			lName=usrJB.getUserLastName();
				lName=(lName==null)?"":lName;
	
			userProcTechName =  fName + " " + lName ;
	
		}	else
		{
		  userProcTechId = "";
		}
	
		if (! StringUtil.isEmpty(userCollTechId))
		{
	
			usrJB.setUserId(EJBUtil.stringToNum(userCollTechId));
			usrJB.getUserDetails();
	
			fName=usrJB.getUserFirstName();
				fName=(fName==null)?"":fName;
	
			lName=usrJB.getUserLastName();
				lName=(lName==null)?"":lName;
	
			userCollTechName =  fName + " " + lName ;
	
		}else
		{
		  userCollTechId = "";
		}
	
	
			if (! StringUtil.isEmpty(userSurgeonId))
		{
	
			usrJB.setUserId(EJBUtil.stringToNum(userSurgeonId));
			usrJB.getUserDetails();
	
			fName=usrJB.getUserFirstName();
				fName=(fName==null)?"":fName;
	
			lName=usrJB.getUserLastName();
				lName=(lName==null)?"":lName;
	
			userSurgeonName =  fName + " " + lName ;
	
		}
		else
		{
		  userSurgeonId = "";
		}
	
		if (! StringUtil.isEmpty(userPathologistId))
		{
	
			usrJB.setUserId(EJBUtil.stringToNum(userPathologistId));
			usrJB.getUserDetails();
	
			fName=usrJB.getUserFirstName();
				fName=(fName==null)?"":fName;
	
			lName=usrJB.getUserLastName();
				lName=(lName==null)?"":lName;
	
			userPathologistName =  fName + " " + lName ;
	
		}else
		{
		  userPathologistId = "";
		}
	
	
	
		CodeDao cdSpecTyp = new CodeDao();
		cdSpecTyp.getCodeValues("specimen_type");
		cdSpecTyp.setCType("specimen_type");
		cdSpecTyp.setForGroup(defUserGroup);
	
	
	
		String dSpecType = "";
	
		CodeDao cdSpecStat = new CodeDao();
		cdSpecStat.getCodeValues("specimen_stat");
		cdSpecStat.setCType("specimen_stat");
		cdSpecStat.setForGroup(defUserGroup);
	
	
	
		String specTyp =	request.getParameter("specType");
		if (specTyp ==null) specTyp ="";
	
		if (mode.equals("M")) {
		specTyp = type;
		}
	
		if (specTyp.equals("")){
	 	   dSpecType=cdSpecTyp.toPullDown("specType");
		}
		else{
	       dSpecType=cdSpecTyp.toPullDown("specType",EJBUtil.stringToNum(specTyp),true);
		}
	
	
		CodeDao cdSpecQuantUnit = new CodeDao();
		cdSpecQuantUnit.getCodeValues("spec_q_unit");
		String dSpecQuantUnit = "";
	
	
		String specQntUnit =	request.getParameter("specQuantityUnit");
		if (specQntUnit ==null) specQntUnit ="";
	
		if (mode.equals("M")) {
		specQntUnit = specQuUnit;
		}
	
		if (specQntUnit.equals("")){
	 	   dSpecQuantUnit=cdSpecQuantUnit.toPullDown("specQuantityUnit");
		}
		else{
	           dSpecQuantUnit=cdSpecQuantUnit.toPullDown("specQuantityUnit",EJBUtil.stringToNum(specQntUnit),true);
		}
	
	
	
		int inSite = 0;
		String selSite = "";
		selSite =  request.getParameter("dPatSite");
		if (selSite==null) selSite = "";
	
	if (mode.equals("M")) {
		selSite = site;
		}
	
		UserJB user = (UserJB) tSession.getValue("currentUser");
	
		String siteId = user.getUserSiteId();
	   	String accountId = user.getUserAccountId();
	
		UserSiteDao usd = new UserSiteDao ();
		ArrayList arSiteid = new ArrayList();
		ArrayList arSitedesc = new ArrayList();
		String ddSite = "";
	
		usd = usrSite.getSitesWithViewRight(EJBUtil.stringToNum(accountId),EJBUtil.stringToNum(userId));
	
		arSiteid = usd.getUserSiteSiteIds();
		arSitedesc = usd.getUserSiteNames();
	
		StringBuffer sbSite = new StringBuffer();
	
		sbSite.append("<SELECT NAME='dPatSite'>") ;
		sbSite.append("<option selected value='' >"+LC.L_Select_AnOption/*Select an Option*****/+"</option>") ;
			if (arSiteid.size() > 0)
			{
				for (int counter = 0; counter < arSiteid.size()  ; counter++)
				{
					inSite = Integer.parseInt((String)arSiteid.get(counter));
					if(inSite == EJBUtil.stringToNum(selSite)){
						sbSite.append("<OPTION value = "+ inSite+" selected>" + arSitedesc.get(counter)+ "</OPTION>");
					}else{
					sbSite.append("<OPTION value = "+ inSite+">" + arSitedesc.get(counter)+ "</OPTION>");
					}
	
				}
			}
	
		sbSite.append("</SELECT>");
		ddSite  = sbSite.toString();
	
	  String setvalueover="0";
	  int mndtry=0;
	  SettingsDao settingsDao=commonB.getSettingsInstance();
	  int modname=1;
	  String keyword="ACC_SPECIMEN_ORGANIZATION_ASSOC";
	  settingsDao.retrieveSettings(EJBUtil.stringToNum(accountId),modname,keyword);
	  ArrayList setvalueovers=settingsDao.getSettingValue();
	  if(setvalueovers.size()>0)
	  setvalueover=(setvalueovers.get(0)==null)?"":(setvalueovers.get(0)).toString();
	  mndtry =EJBUtil.stringToNum(setvalueover);
		//JM: 28Jan2008: INV # 4.2.7: Child Specimens
	
		CodeDao cdProcTyp = new CodeDao();
		cdProcTyp.getCodeValues("spec_proctype");
	
		cdProcTyp.setCType("spec_proctype");
		cdProcTyp.setForGroup(defUserGroup);
	
	
		String dpProcType = "";
		String dcProcType = "";
		String decProcType = "";
	
	
		String patientStudyId = "";
	
	//JM: 26May2009: #INVP2.7.1
		pProtJB.findCurrentPatProtDetails(EJBUtil.stringToNum(stdy), EJBUtil.stringToNum(pat));
		patientStudyId = pProtJB.getPatStudyId();
		patientStudyId=(patientStudyId==null)?"":patientStudyId;
	
	
	
		String pProcTyp =	request.getParameter("pProcType");
		if (pProcTyp ==null) pProcTyp ="";
	
		if (mode.equals("M")) {
			pProcTyp = procType;
		}
	
		//parent processing type dd
		if (pProcTyp.equals("")){
	 	   dpProcType=cdProcTyp.toPullDown("pProcType");
		}
		else{
	       dpProcType=cdProcTyp.toPullDown("pProcType",EJBUtil.stringToNum(pProcTyp),true);
		}
	
	//JM: 03June2009: #INV 2.29 (2)
		StringBuffer stStatbuffer = new StringBuffer();
		int len_procType = dpProcType.length();
		int strIndex = dpProcType.indexOf(">");
	
		String strSel =  dpProcType.substring(0, strIndex);
		strSel = strSel + " "+ "disabled>";
		stStatbuffer.append(strSel);
	
		String strApp = dpProcType.substring(strIndex+1, len_procType);
	
		stStatbuffer.append(strApp);
		dpProcType = stStatbuffer.toString();
	
	
	
	 //Child processing type dd, works in new mode alone
	   dcProcType=cdProcTyp.toPullDown("cProcType");
	
	 //JM: #INV 2.30
	   decProcType = cdProcTyp.toPullDown("ecProcType");
	
	   /*
	   StringBuffer stStatbuffer1 = new StringBuffer();
		int len_procType1 = decProcType.length();
		int strIndex1 = decProcType.indexOf(">");
	
		String strSel1 =  decProcType.substring(0, strIndex1);
		strSel1 = strSel1 + " "+ "style='width: 200px'>";
		stStatbuffer1.append(strSel1);
	
		String strApp1 = decProcType.substring(strIndex1+1, len_procType1);
	
		stStatbuffer1.append(strApp1);
		decProcType = stStatbuffer1.toString();
	   */
	
	
	
		int printmeParent = 0;
		String avParentQty ="";
		String parentPk ="";
	
		if (mode.equals("M")){
			parentPk = qtySpecJB.getParentSpecimenId(EJBUtil.stringToNum(pkId));
			parentPk=(parentPk==null)?"":parentPk;
	
			if (!parentPk.equals("")){
			qtySpecJB.setPkSpecimen(EJBUtil.stringToNum(parentPk));
			qtySpecJB.getSpecimenDetails();
			avParentQty = qtySpecJB.getSpecOrgQnty();
			avParentQty=(avParentQty==null)?"":avParentQty;
			}
		}
	
	
	//JM: 20May2009, #INVP2.7.1***************************//
	
		String fk_storage_kit = "";
		//To calculate for the fk_storage_kit
	
	
		CodeDao cdAnatomicSite = new CodeDao();
		cdAnatomicSite.getCodeValues("anatomic_site");
		String dAnatomicSite = "";
	
		String selAnatomicRetSite  = specJB.getSpecAnatomicSite();
		selAnatomicRetSite=(selAnatomicRetSite==null)?"":selAnatomicRetSite;
	
		String selAnatomicSite =	request.getParameter("anatomic_site_dd");
		if (selAnatomicSite ==null) selAnatomicSite ="";
	
		if (mode.equals("M")) {
		selAnatomicSite = selAnatomicRetSite;
		}
	
		if (selAnatomicSite.equals("")){
	 	   dAnatomicSite=cdAnatomicSite.toPullDown("anatomic_site_dd"," onChange=callAjaxGetAnatomic_siteDD(document.specimen);");
		}
		else{
	       dAnatomicSite=cdAnatomicSite.toPullDown("anatomic_site_dd",EJBUtil.stringToNum(selAnatomicSite)," onChange=callAjaxGetAnatomic_siteDD(document.specimen);");
		}
	
	
	
		CodeDao cdTissueSide = new CodeDao();
		//cdTissueSide.getCodeValues("tissue_side");
		cdTissueSide.getCodeValuesForCustom1("tissue_side",selAnatomicSite);
		String dTissueSide = "";
	
		String selTissueRetSide  = specJB.getSpecTissueSide();
		selTissueRetSide=(selTissueRetSide==null)?"":selTissueRetSide;
	
		String selTissueSide =	request.getParameter("tissue_site_dd");
		if (selTissueSide ==null) selTissueSide ="";
	
		if (mode.equals("M")) {
		selTissueSide = selTissueRetSide;
		}
	
		if (selTissueSide.equals("")){
	 	   dTissueSide=cdTissueSide.toPullDown("tissue_site_dd");
		}
		else{
	       dTissueSide=cdTissueSide.toPullDown("tissue_site_dd",EJBUtil.stringToNum(selTissueSide),true);
		}
	
	
		CodeDao cdPathologyStat = new CodeDao();
		cdPathologyStat.getCodeValues("pathology_stat");
		String dPathologyStat = "";
	
	
		String selPathologyRetStat  = specJB.getSpecPathologyStat();
		selPathologyRetStat=(selPathologyRetStat==null)?"":selPathologyRetStat;
	
		String selPathologyStat =	request.getParameter("pathology_stat_dd");
		if (selPathologyStat ==null) selPathologyStat ="";
	
		if (mode.equals("M")) {
		selPathologyStat = selPathologyRetStat;
		}
	
		if (selPathologyStat.equals("")){
	 	   dPathologyStat=cdPathologyStat.toPullDown("pathology_stat_dd");
		}
		else{
	       dPathologyStat=cdPathologyStat.toPullDown("pathology_stat_dd",EJBUtil.stringToNum(selPathologyStat),true);
		}
	
	
	//JM: 22May2009, added for #INVP2.7.1
	
		String specExptdQuUnit  = specJB.getSpecExpectedQUnits();
		specExptdQuUnit=(specExptdQuUnit==null)?"":specExptdQuUnit;
	
		String specExptdQntUnit =	request.getParameter("specExptdQntUnit_dd");
		if (specExptdQntUnit ==null) specExptdQntUnit ="";
		String dSpecExptdQuUnit = "";
	
		if (mode.equals("M")) {
		specExptdQntUnit = specExptdQuUnit;
		}
	
		if (specExptdQntUnit.equals("")){
	 	   dSpecExptdQuUnit=cdSpecQuantUnit.toPullDown("specExptdQntUnit_dd");
		}
		else{
	       dSpecExptdQuUnit=cdSpecQuantUnit.toPullDown("specExptdQntUnit_dd",EJBUtil.stringToNum(specExptdQntUnit),true);
		}
	
		String specBaseOrigQuUnit  = specJB.getSpecBaseOrigQUnit();
		specBaseOrigQuUnit=(specBaseOrigQuUnit==null)?"":specBaseOrigQuUnit;
	
		String specBaseOrigQntUnit =	request.getParameter("specBaseOrigQuUnit_dd");
		if (specBaseOrigQntUnit ==null) specBaseOrigQntUnit ="";
		String dSpecBaseOrigQuUnit = "";
	
		if (mode.equals("M")) {
		specBaseOrigQntUnit = specBaseOrigQuUnit;
		}
	
		//Modified for Bug#10152 : Raviesh
		if (specBaseOrigQntUnit.equals("")){
	 	   dSpecBaseOrigQuUnit=cdSpecQuantUnit.toPullDown("specBaseOrigQuUnit_dd");
	 	   dSpecBaseOrigQuUnit=dSpecBaseOrigQuUnit.replaceFirst("<SELECT","");
	       dSpecBaseOrigQuUnit="<SELECT"+" onchange=\"autoCalCurrDrpDwn(document.specimen);\""+dSpecBaseOrigQuUnit;
		}
		else{
	       dSpecBaseOrigQuUnit=cdSpecQuantUnit.toPullDown("specBaseOrigQuUnit_dd",EJBUtil.stringToNum(specBaseOrigQntUnit),true);
	       dSpecBaseOrigQuUnit=dSpecBaseOrigQuUnit.replaceFirst("<SELECT","");
	       dSpecBaseOrigQuUnit="<SELECT"+" onchange=\"autoCalCurrDrpDwn(document.specimen);\""+dSpecBaseOrigQuUnit;
		}
	
	
		String exptdQty =	specJB.getSpecExpectQuant();
		Double expectdQty=0.0;
		exptdQty = (exptdQty==null)?"":exptdQty;											
		 Pattern pattern=Pattern.compile("[0-9]*.(\\d){5}"); 
		 Pattern exp=Pattern.compile("[0-9]*.E[0-9]*");
		 Matcher matcher=pattern.matcher("");
		if (!exptdQty.equals("")){
			matcher=exp.matcher(exptdQty);
			if(matcher.find())
			{
			exptdQty=new BigDecimal(exptdQty).toPlainString();
			matcher = pattern.matcher(exptdQty);
			}
			else
			{
			 expectdQty=Double.parseDouble(exptdQty);
			}
			if(matcher.find())
			{
				exptdQty= (new BigDecimal(exptdQty).setScale(5, RoundingMode.HALF_UP)).toPlainString();
				
				
			}
			
			else
			{
				exptdQty=expectdQty.toString() ;
			}
		}
		String origQty =	specJB.getSpecBaseOrigQuant();
		Double origQnty=0.0;
		origQty=(origQty==null)?"":origQty;
		
		 BigDecimal actCurrentQty=new BigDecimal("0.0");
		if (!origQty.equals("")){
			matcher=exp.matcher(origQty);
			if(matcher.find())
			{
			origQty=new BigDecimal(origQty).toPlainString();
			matcher = pattern.matcher(origQty);
			}
			else
			{
			 origQnty=Double.parseDouble(origQty);
			}
			if(matcher.find())
			{
				origQty= (new BigDecimal(origQty).setScale(5, RoundingMode.HALF_UP)).toPlainString();
				
				
			}
			
			else
			{
				origQty=origQnty.toString() ;
			}
			actCurrentQty=new BigDecimal(origQty);
		}
	%>
	<%
	String sessUserId="";
	String modName="specimen";
	sessUserId =(String) tSession.getValue("userId");
	MoreDetailsDao mdDao = new MoreDetailsDao();
	mdDao = mdJB.getMoreDetails(StringUtil.stringToNum(pkId),modName,defUserGroup);
 	ArrayList modElementIdList  = new ArrayList();
	ArrayList modElementDescList = new ArrayList();
	ArrayList idList = new ArrayList();
	ArrayList modElementDataList = new ArrayList();
	ArrayList modElementKeysList = new ArrayList();
 	ArrayList recordTypeList = new ArrayList();
	ArrayList dispTypeList=new ArrayList();
	ArrayList dispDataList=new ArrayList();
	idList = mdDao.getId();
	modElementIdList =  mdDao.getMdElementIds();
	modElementDescList = mdDao.getMdElementDescs();
	modElementDataList = mdDao.getMdElementValues();
	modElementKeysList = mdDao.getMdElementKeys();
	recordTypeList = mdDao.getRecordTypes ();
	dispTypeList=mdDao.getDispTypes();
	dispDataList=mdDao.getDispDatas();
	 String strElementDesc="" ; 
 	 String strElementValue="" ;
  	 String strRecordType="" ;
  	 Integer iElementId;
   	 Integer iId;
   	  String subType="";
	 String disptype="";
	 String dispdata="";
	 String ddStr="";
	%>
	
	
	
<% if(FromPatPage.equals("patient")  || FromPatPage.equals("patientEnroll")){%>
<DIV class="BrowserTopn" id="divTab">
<jsp:include page="patienttabs.jsp" flush="true">
<jsp:param name="selectedTab" value="12"/>
<jsp:param name="page" value="<%=FromPatPage%>"/>
<jsp:param name="page" value="<%=patientCode%>"/>
<jsp:param name="pkey" value="<%=patientId1%>"/>
<jsp:param name="parentPk" value="<%=parentPk%>" />
<jsp:param name="specimenPk" value="<%=pkId%>" />
<jsp:param name="modespecimen" value="<%=mode%>" />
</jsp:include>
</DIV>

	<%} else{%>
	<!--Rohit:BugNo:4213 -->
	<input type="hidden" name="currAmt" id="currAmt" value="<%=currAmt%>">
	<div class="browserDefault" id="div1">
	
		<jsp:include page="inventorytabs.jsp" flush="true">
			<jsp:param name="selectedTab" value="1"/>
			<jsp:param name="specimenPk" value="<%=pkId%>" />
			<jsp:param name="parentPk" value="<%=parentPk%>" />
		</jsp:include>
<%} %>
	<table border=0 width="99%" cellspacing="0" cellpadding="0">
	<tr bgcolor="#FFF7DD" align="left" class="speca"><td>
	<%if (mode.equals("N") && (FromPatPage.equals("")  || FromPatPage.equals(""))){%>
		<a href="specimenbrowser.jsp?srcmenu=tdmenubaritem6&selectedTab=1&FromPatPage=<%=FromPatPage %>&patientCode=<%=patientCode%>&pkey=<%=patientId1%>"><img src="../images/jpg/search_pg.png" title="<%=LC.L_Specimen_Search%><%--Specimen Search*****--%>" border="0"></a>
	<%}%></td></tr></table>	</div>
	<div style="top:124px" class="BrowserBotN BrowserBotN_MI_4" id="div2">
	<Form name="specimen" id="specimendet" method="post" action="updatespecimendetails.jsp" onSubmit = "if (validate(document.specimen)== false) {setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<input type="hidden" name="mode" value="<%=mode%>">
	<input type="hidden" name="specimenPkId" value="<%=pkId%>">
	<input type="hidden" name="FromPatPage" value="<%=FromPatPage%>">
	<input type="hidden" name="patientCode" value="<%=patientCode%>">
	<input type="hidden" name="pkey" value="<%=patientId1%>">
	
	<input type="hidden" name="oldQuantity" value="<%=oldQty%>">
	<input type="hidden" name="parentSpecimen" value="<%=parentPk%>">
	<input type="hidden" name="avParentQuantity" value="<%=avParentQty%>">
	
	<input type="hidden" name="fk_storage_kit" value="<%=fk_storage_kit%>">
	<input type="hidden" name="accountId" value="<%=accountId%>">
	<% // The storageLocation and eStorageLocation hidden fields are substitutes
	   // for input text fields which were removed because they were not fully implemented
	%>
	<input type="hidden" name="storageLocation" value="">
	<input type="hidden" name="eStorageLocation" value="">
		<div id="SpecDetailsDIV" class="portlet portletstatus ui-widget ui-widget-border ui-helper-clearfix ui-corner-all">
		<div id="specimenDetailsSectioncontent" onclick="toggleDiv('specimenDetailsSection')" class="portlet-header portletstatus ui-widget 
		ui-widget-header ui-widget-content ui-helper-clearfix ui-corner-all" style="background-color:#CCCCCC;">
			<span class="ui-icon ui-icon-triangle-1-s"></span>
		<%=LC.L_Details%>
		</div>
		
	<div id="specimenDetailsSection" >
	<table width="100%" cellspacing="0" cellpadding="0" >
	<!--JM: 19May2009: #INV 2.35-->
	<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
	</tr>
	<tr>
			<td colspan="2"><%=MC.M_LveSpmenIDBlk_ForSysID%><%--Leave 'Specimen ID' field blank for system auto-generated ID*****--%></td>
	</tr>
	<tr>
		<td width = "45%">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
			<!--JM: 29Aug2007: added the help text-->
	
			<tr>
			<td class=tdDefault width="30%" align=right><%=LC.L_Specimen_Id%><%--Specimen ID*****--%>&nbsp;</td>
			<td class=tdDefault>
	
			<%if (isSpecReadonly.equals("1")){%>
			<Input type = "text" name="specimenId"  value="<%=specId%>" size=15 MAXLENGTH = 100 readonly>
			<%}else{%>
			<Input type = "text" name="specimenId"  value="<%=specId%>" size=15 MAXLENGTH = 100>
			<%}%>
	
	
			</td>
			</tr>
	
			<tr>
			<td class=tdDefault align="right"><%=LC.L_Alternate_Id%><%--Alternate ID*****--%>  &nbsp;</td>
			<td class=tdDefault><Input type = "text" name="altId"  value="<%=alterId%>" size=15 MAXLENGTH = 100>
			</td>
			</tr>
	
			<tr>
			<td class=tdDefault width="30%" align="right"><%=LC.L_Description%><%--Description*****--%> &nbsp; </td>
			<td class=tdDefault ><Input type = "text" name="description"   value="<%=descn%>" size=25 MAXLENGTH = 500 align = right>
			</td>
			</tr>
	
			<tr>
			<td class=tdDefault width="30%" align="right"><%=LC.L_Type%><%--Type*****--%><FONT class="Mandatory">* </FONT> &nbsp;</td>
			<td><%=dSpecType%></td>
			<td></td>
			</tr>
	
	<%-- INF-20084 Datepicker-- AGodara --%>
			<tr>
			<td class=tdDefault align=right><%=MC.M_Coll_DateAndTime%><%--Collection Date and Time*****--%>&nbsp;</td>
			<td class=tdDefault><Input type = "text" name="colDate"  value="<%=collDtFormatted%>" size=10 MAXLENGTH =20 class="datefield" >
			<input type="text" name="collectionHH" value="<%=collectionHH%>" maxlength=2 size=1>
			:<input type="text" name="collectionMM" value="<%=collectionMM%>" maxlength=2 size=1>
			:<input type="text" name="collectionSS" value="<%=collectionSS%>" maxlength=2 size =1>
			<a href="#" onclick="return setCurrentTime(document.specimen , 1)"><%=LC.L_Cur_Time%><%--Current Time*****--%></a>&nbsp;
			<a href="#" onclick="return resetCurrentTime(document.specimen, 1)"><%=LC.L_Reset%><%--Reset*****--%></a>
	
			 </td>
			</tr>
	
			<tr>
			<td class=tdDefault width="30%" align="right"><%=LC.L_Owner%><%--Owner*****--%> &nbsp;</td>
			<input type="hidden" name="creatorId" value="<%=ownr%>">
			<td class=tdDefault >
			<Input type = "text" name="createdBy"   value="<%=ownerName%>" size=25 align = right readonly>
			<A href="#" onClick="return openUserWindow('specimenUser')"><%=LC.L_Select%><%--Select*****--%></A></td>
			</tr>
	
			<tr>
			<td align="right"><%=LC.L_Location%><%--Location*****--%> &nbsp;</td>
			<td><Input type = "text" name="storageLoc" value = "<%=storageLocationName%>" size=25 align = right readonly>
			<Input type = "hidden" name="mainFKStorage" value = "<%=fkStorage%>" size=25 align = right>
			<Input type = "hidden" name="oldFKStorage" value = "<%=oldFkStorage%>" size=25 align = right>
			<A  href="javascript:void(0);" onClick="return openLocLookup(document.specimen, 1)" ><%=LC.L_Select%><%--Select*****--%></A>&nbsp;
			<A  href="#"  onClick="return removeLocation(document.specimen, 1)" ><%=LC.L_Remove%><%--Remove*****--%></A>&nbsp;
			</td>
			</tr>
			</table>
		</td>
	
		<td>
			<table  width="100%" cellpadding="0" cellspacing="0" border="0">
	
				<tr>
				<td align="right" style="padding-right:5px;"><%=LC.L_Associations%><%--Associations*****--%></td>
				<td colspan="2"></td>
				</tr>
				<tr>
				<td class=tdDefault align="right"><%=LC.L_Study%><%--<%=LC.Std_Study%>*****--%> &nbsp;</td>
				<input type="hidden" name="selStudyIds" value="<%=stdy%>">
				<td class=tdDefault><Input type = "text" name="selStudy"  value="<%=studyNumber%>" size=25 readonly >
					<!--<A href="#" onClick="openStudyWindow(document.specimen,'specimen')" >SELECT</A>-->
				<A href="#" onClick="openStudyWindow(document.specimen)" ><%=LC.L_Select%><%--Select*****--%></A>
				</td>
				</tr>
	
	
	
	
				<tr>
				<td class=tdDefault width="30%" align="right"> 	<%=LC.L_Patient_Id%><%--<%=LC.Pat_Patient%> ID*****--%> &nbsp;</td>
				<td class=tdDefault width="70%">
		<% if(FromPatPage.equals("patient")  || FromPatPage.equals("patientEnroll")){%>
				<Input type = "text" name="patientNames" value="<%=StringUtil.decodeString(patientCode)%>"  size=25  readonly>
				<%} else{%>
				<Input type = "text" name="patientNames" value="<%=patientId%>"  size=25  readonly>
				<% }%>
				<A href="javascript:void(0);" onClick="return openLookup(document.specimen)"><%=LC.L_Select%><%--Select*****--%></A><% if(pat != null && pat.length() > 0) { %>&nbsp;<A href="patientdetails.jsp?mode=M&srcmenu=tdmenubaritem5&selectedTab=1&page=patient&pkey=<%=pat%>"><%=LC.L_Details%><%--Details*****--%></a><% }  %>
				</td>
				<Input type="hidden" name="patientIds" value="<%=pat%>"">
				</tr>
	
				<tr>
					<td class=tdDefault width="15%" align="right">					
					<input type="hidden" id="org_flag" value="<%=mndtry%>"><%=LC.L_Organization%>
					<% if (mndtry==1) { %><FONT class="Mandatory">*</FONT><%}%> &nbsp;
					</td>
			   	<td><%=ddSite%></td>
				<td></td>
	
	
				</tr>
				<tr>
					<td align="right"> <%=LC.L_VstOrEvt%><%--Visit/Event*****--%> &nbsp;</td>
					<td><Input READONLY type = "text" name="visitOrEvent" value = "<%=eventVisitName%>" size=25 align = right>
	
					</td>
				</tr>
	
				<tr>
					<td align="right"><%=LC.L_Patient_StudyId%><%--<%=LC.Pat_Patient%> <%=LC.Std_Study%> ID*****--%> &nbsp;</td>
					<td><Input READONLY type = "text" name="patStudyId" value = "<%=patientStudyId%>" size=25 align = right>
	
					</td>
				</tr>
			</table>
		</td>
	</tr>
	
	</table>
	
	
	<table width="99%" cellspacing="0" cellpadding="0" >
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td width = "45%">
		 <table  width="99%" cellpadding="0" cellspacing="0" border="0">
			 <tr>
			    <td class=tdDefault width="30%" align="right"><%=LC.L_Anatomic_Site%><%--Anatomic Site*****--%>  &nbsp;</td>
				<td><%=dAnatomicSite%></td>
			 </tr>
			 <tr>
	
				<td class=tdDefault width="30%" align="right"><%=LC.L_Tissue_Side%><%--Tissue Side*****--%> &nbsp;</td>
				<td><span id = "span_tissue_side"> <%=dTissueSide%> </span></td>
			 </tr>
			 <tr>
	
				<td class=tdDefault width="30%" align="right"><%=LC.L_Pathological_Status%><%--Pathological Status*****--%>  &nbsp;</td>
				<td><%=dPathologyStat%></td>
			 </tr>
		 </table>
		</td>
	
		<td class=tdDefault width="17%" align="right"><%=LC.L_Notes%><%--Notes--%>  &nbsp;</td>
		<td width = "39%">
			<TextArea name="note" rows=4 cols=35 MAXLENGTH = 20000 ><%=noteDisp%></TextArea>
		</td>
	</tr>
	
	
	

	</table>
	
	
	<table width="100%" cellspacing="0" cellpadding="0" >
		<!--<tr>
		<td width="13%">Processing Type </td>
		    <td><%=dpProcType%></td>
	
		</tr>-->
		<tr>
			<td class=tdDefault align="right"width="13%"><%=LC.L_Pathologist%><%--Pathologist*****--%> &nbsp;</td>
			<input type="hidden" name="userPathologistId" id="userPathologistId" value="<%=userPathologistId%>">
			<td class=tdDefault width="18%">
			<Input type = "text" name="userPathologistName" id="userPathologistName"  value="<%=userPathologistName%>" size=25 align = right >
			<!--  <A href="#" onClick="return openCommonUser('specimen','userPathologistId','userPathologistName')"><%=LC.L_Select%><%--Select*****--%></A>--></td>
	
			<td class=tdDefault align="right"width="30%"><%=LC.L_Surgeon%><%--Surgeon*****--%> &nbsp;</td>
			<input type="hidden" name="userSurgeonId" id="userSurgeonId" value="<%=userSurgeonId%>">
			<td class=tdDefault >
			<Input type = "text" name="userSurgeonName" id="userSurgeonName"  value="<%=userSurgeonName%>" size=25 align = right >
			<!--  <A href="#" onClick="return openCommonUser('specimen','userSurgeonId','userSurgeonName')"><%=LC.L_Select%><%--Select*****--%></A>--></td>
		 </tr>
		<tr>
			<td class=tdDefault align="right"><%=LC.L_Collecting_Technician%><%--Collecting Technician*****--%> &nbsp;</td>
			<input type="hidden" name="userCollTechId" id="userCollTechId" value="<%=userCollTechId%>">
			<td class=tdDefault align="left">
			<Input type = "text" name="userCollTechName" id="userCollTechName"  value="<%=userCollTechName%>" size=25 align = right >
			<!-- <A href="#" onClick="return openCommonUser('specimen','userCollTechId','userCollTechName')"><%=LC.L_Select%><%--Select*****--%></A> --></td>
	
			<td class=tdDefault align="right"><%=LC.L_Processing_Technician%><%--Processing Technician*****--%> &nbsp;</td>
			<input type="hidden" name="userProcTechId" id="userProcTechId" value="<%=userProcTechId%>">
			<td class=tdDefault >
			<Input type = "text" name="userProcTechName" id="userProcTechName"   value="<%=userProcTechName%>" size=25 align = right >
			<!--  <A href="#" onClick="return openCommonUser('specimen','userProcTechId','userProcTechName')"><%=LC.L_Select%><%--Select*****--%></A>--></td>
		 </tr>
	
			<!--<td class=tdDefault>Time of Removal &nbsp;</td>-->
			<input type="hidden" name="TORemovalHH" value="<%=TORemovalHH%>" maxlength=2 size=2>
			<input type="hidden" name="TORemovalMM" value="<%=TORemovalMM%>" maxlength=2 size=2>
			<input type="hidden" name="TORemovalSS" value="<%=TORemovalSS%>" maxlength=2 size=2>
			<!--<td class=tdDefault>Time of Freezing &nbsp;</td>-->
			<input type="hidden" name="TOFreezeHH" value="<%=TOFreezeHH%>" maxlength=2 size=2>
			<input type="hidden" name="TOFreezeMM" value="<%=TOFreezeMM%>" maxlength=2 size=2>
			<input type="hidden" name="TOFreezeSS" value="<%=TOFreezeSS%>" maxlength=2 size=2>
			
	<tr><td colspan="5"  align="right" >
	<%=MC.M_CopyParent_SpmenDet%>&nbsp;&nbsp;&nbsp;&nbsp;
	<A href="#" onMouseOver="return overlib('<%="<tr><td><font size=1>"+"Fields to be copied"+":</font><font size=1> "+CopyFieldsTxt1+"</font></td></tr>"%>',CAPTION,'<%="Help"%>');" onMouseOut="return nd();"><img src="../images/help.png" width="17px" height="17" border="0" />  </A>
	<input type="checkbox"  name="cpPatDe_Section_1" checked>
	</td>
		</tr>
</table>
	
	</div></div>
	<% if(modElementIdList.size()>0){ %>
	<div id="specimenAddtnlDetailDiv" class="portlet portletstatus ui-widget ui-widget-border ui-helper-clearfix ui-corner-all">
					<div id="specimenMoreInfocontent" onclick="toggleDiv('specimenMoreInfo')" class="portlet-header portletstatus ui-widget 
					ui-widget-header ui-widget-content ui-helper-clearfix ui-corner-all" style="background-color:#CCCCCC;">
						<span class="ui-icon ui-icon-triangle-1-s"></span>
			<%=LC.L_More_Info%>
					</div>
		<div id='specimenMoreInfo'  width="99%">
	<table width="100%" cellpadding="0" cellspacing="0">
	<%-- 	<tr>
				<td colspan=7>
				<p class="SectionHeadings"><%=MC.M_AddlSpmen_Details%></p>
				</td>
				</tr> --%>
		
	<% 
	int counter = 0, cbcount=0, count=0; String strElementDesc1="";
	for (counter = 0; counter <= modElementIdList.size() -1 ; counter++)
		{
				count++;
				strElementDesc = (String) modElementDescList.get(counter); 
                strElementDesc1+=strElementDesc+" ";
 	 			strElementValue = (String) modElementDataList.get(counter); 
 	 		 	disptype=(String) dispTypeList.get(counter);
				dispdata=(String) dispDataList.get(counter);
				subType = (String) modElementKeysList.get(counter);
				if (disptype==null) disptype="";
				if (dispdata==null) dispdata="";
				
 	 			if (strElementValue == null)
 	 				strElementValue = "";
 	 				
  	 			strRecordType = (String) recordTypeList.get(counter); 
				iElementId = (Integer) modElementIdList.get(counter) ; 
   	 			iId = (Integer) idList.get(counter) ;  
   	 		if ((disptype.toLowerCase()).equals("dropdown")) {
				if (null != dispdata){
					dispdata = StringUtil.replaceAll(dispdata, "alternateId", "alternateId"+iElementId);
				}
			}
			
	%>
				<td width="28%" align="left">
																													  
																													 
				<%=strElementDesc%></td>
			    <td>
				<% if ((disptype.toLowerCase()).equals("dropdown")) {
				if (ddStr.length()==0) ddStr=(iElementId)+":"+strElementValue;
					else ddStr=ddStr+"||"+(iElementId)+":"+strElementValue;
				%>
				  <%=dispdata%></td>
				  <tr></tr>

				 <%}
				
					
				  else if((disptype.toLowerCase()).equals("input")) {
				  %> 		
				 <input type = "text"   id="alternateId<%=iElementId%>" name="alternateId<%=iElementId%>" value = "<%=strElementValue.trim()%>" size = "25" maxlength = "100" >
				  <%}
				 
				  else if((disptype.toLowerCase()).equals("time")) {
					  
					  if(strElementValue!="")
					  {
					  String[] parts = strElementValue.split(":");
					   timeHH = parts[0];
					   timeMM = parts[1];
					   timeSS = parts[2];
					  }
					  %> 		
					
					 <input type="text" name="timeHH" value="<%=timeHH%>" maxlength=2 size=1>
					:<input type="text" name="timeMM" value="<%=timeMM%>" maxlength=2 size=1>
					:<input type="text" name="timeSS" value="<%=timeSS%>" maxlength=2 size=1>
					<a href="#" onclick="return setCurrentTime(document.specimen, 4)"><%=LC.L_Cur_Time%><%--Current Time*****--%></a>&nbsp;
					<a href="#" onclick="return resetCurrentTime(document.specimen, 4)"><%=LC.L_Reset%><%--Reset*****--%></a>
					 <input type = "hidden" name = "alternateId<%=iElementId%>" id="timeId"  size = "25" maxlength = "100" >
					
					 <%}
				
				  else if((disptype.toLowerCase()).equals("readonly-input")){%>
					<input type="text" class='readonly-input' readonly ="readonly" id="alternateId<%=iElementId%>" name="alternateId<%=iElementId%>" value = "<%=strElementValue.trim()%>" size = "25" />
					<%}
				 else if ((disptype.toLowerCase()).equals("splfld")) {%> 
				  <%=dispdata%>
			<% }
				 else if((disptype.toLowerCase()).equals("textarea")) {%>
				  <textarea class="mdTextArea" name = "alternateId<%=iElementId%>"  rows="4" cols="50" ><%=strElementValue.trim()%></textarea>
				 <%} 
				
				 else if (("checkbox".equals(disptype))||("chkbox".equals(disptype))){
						//checkbox 
						if (!StringUtil.isEmpty(dispdata)){
							//checkbox-group
							List<String> strChkBxVals = Arrays.asList(strElementValue.split(","));
					        try{
					        	JSONObject chkboxesJSON = new JSONObject(dispdata);

							    int chkColCnt = 0;
							    try{
							    	chkColCnt = StringUtil.stringToNum(""+chkboxesJSON.get("chkColCnt"));
							    } catch(Exception e){
							    }

							    chkColCnt = (chkColCnt <= 0) ? 5 : chkColCnt;
								try {
									JSONArray checkboxArray = chkboxesJSON.getJSONArray("chkArray");

									for (int indx=0; indx < checkboxArray.length(); indx++){
										JSONObject checkboxJSON = (JSONObject)checkboxArray.get(indx);
								        try{
											String data = checkboxJSON.getString("data");
											String display = checkboxJSON.getString("display");
											String checked = "";
											
											if(strChkBxVals.contains(data)){
												checked = "checked";
											}
											if (indx % chkColCnt == 0){
											%>
												<br>
											<%
											}
							             %>
											<input type="checkbox" data-subtype="<%=subType %>" name="alternateId<%=iElementId%>Checks" value="<%=data.trim()%>" onClick="moreDetailsFunctions.setValue4ChkBoxGrp(this,<%=iElementId%>)"  <%=checked%> /> <%=display%> 
										<%
								       	}catch(Exception e){
								       		//e.printStackTrace();
									    	System.out.println("More Details: checkbox configuration error!");
								       	}
									}%>
								<%}catch(Exception e1){
									//e1.printStackTrace();
							    	System.out.println("More Details: checkbox configuration error!");
								} %>
								<input type="hidden" name="alternateId<%=iElementId%>" id="alternateId<%=iElementId%>" value="<%=strElementValue.trim() %>"/>
								<% 
					        }catch(Exception e){
					        	System.out.println("More Details: checkbox configuration error!");
					        }
						}else {
							cbcount=cbcount+1;
							%>
							<input type = "hidden" name = "alternateId<%=iElementId%>" value = "<%=strElementValue.trim()%>" size = "25" maxlength = "100" >
							<%if ((strElementValue.trim()).equals("Y")){%>
							 <input type="checkbox" data-subtype="<%=subType %>" name="alternate" value="<%=strElementValue.trim()%>" onClick="moreDetailsFunctions.setValue(this,<%=iElementId%>,<%=cbcount%>)" checked>
							<% }else{%>
							  <input type="checkbox"  data-subtype="<%=subType %>" name="alternate" value="<%=strElementValue.trim()%>" onClick="moreDetailsFunctions.setValue(this,<%=iElementId%>,<%=cbcount%>)">
							<%}%>
						<%}}
				 else if ("radio".equals(disptype)){
						//radio
						if (!StringUtil.isEmpty(dispdata)){
							//radio-group
							List<String> strradioVals = Arrays.asList(strElementValue.split(","));
					        try{
					        	JSONObject radiobtnJSON = new JSONObject(dispdata);

							    int chkColCnt = 0;
							    try{
							    	chkColCnt = StringUtil.stringToNum(""+radiobtnJSON.get("radioColCnt"));
							    } catch(Exception e){
							    }

							    chkColCnt = (chkColCnt <= 0) ? 5 : chkColCnt;
								try {
									JSONArray radiobtnArray = radiobtnJSON.getJSONArray("radioArray");

									for (int indx=0; indx < radiobtnArray.length(); indx++){
										JSONObject radioJSON = (JSONObject)radiobtnArray.get(indx);
								        try{
											String data = radioJSON.getString("data");
											String display = radioJSON.getString("display");
											String checked = "";
											
											if(strradioVals.contains(data)){
												checked = "checked";
											}
											if (indx % chkColCnt == 0){
											%>
												<br>
											<%
											}
							             %>
											<input type="radio" data-subtype="<%=subType %>" name="alternateId<%=iElementId%>Checks" value="<%=data.trim()%>" onClick="moreDetailsFunctions.setValue4ChkBoxGrp(this,<%=iElementId%>)"  <%=checked%> /> <%=display%> 
										<%
								       	}catch(Exception e){
								       		//e.printStackTrace();
									    	System.out.println("More Details: Radio configuration error!");
								       	}
									}%>
								<%}catch(Exception e1){
									//e1.printStackTrace();
							    	System.out.println("More Details: Radio configuration error!");
								} %>
								<input type="hidden" name="alternateId<%=iElementId%>" id="alternateId<%=iElementId%>" value="<%=strElementValue.trim() %>"/>
								<% 
					        }catch(Exception e){
					        	System.out.println("More Details: Radio configuration error!");
					        }
						}else {
							cbcount=cbcount+1;
							%>
							<input type = "hidden" name = "alternateId<%=iElementId%>" value = "<%=strElementValue.trim()%>" size = "25" maxlength = "100" >
							<%if ((strElementValue.trim()).equals("Y")){%>
							 <input type="radio" data-subtype="<%=subType %>" name="alternate" value="<%=strElementValue.trim()%>" onClick="moreDetailsFunctions.setValue(this,<%=iElementId%>,<%=cbcount%>)" checked>
							<% }else{%>
							  <input type="radio"  data-subtype="<%=subType %>" name="alternate" value="<%=strElementValue.trim()%>" onClick="moreDetailsFunctions.setValue(this,<%=iElementId%>,<%=cbcount%>)">
							<%}%>
						<%}}
				 else  if((disptype.toLowerCase()).equals("lookup")){
					    //lookup
					    String lkpKeyworkStr = "";
					    int lkpPK = 0;
					    try{
						    JSONObject lookupJSON = new JSONObject(dispdata);
						    lkpPK = StringUtil.stringToNum(""+lookupJSON.get("lookupPK"));
						    if (lkpPK <= 0){
						    	System.out.println("More Details: invalid lookup PK");
						    	continue;
						    }

						    JSONArray mappingArray = (JSONArray)lookupJSON.get("mapping");
							for (int indx=0; indx < mappingArray.length(); indx++){
								JSONObject lkpJSON = (JSONObject)mappingArray.get(indx);
						        try{
						        	String source = lkpJSON.getString("source").trim();
						            String target = lkpJSON.getString("target").trim();
						            if (StringUtil.isEmpty(source) || StringUtil.isEmpty(target)) continue;

						            target = ("alternateId".equals(target))? "alternateId"+iElementId : target;

						            if (!StringUtil.isEmpty(lkpKeyworkStr))
										lkpKeyworkStr += "~";
						            lkpKeyworkStr += target+"|"+source;
						        }catch(Exception e){
						        	System.out.println("More Details: invalid lookup mapping");
						        }
							}
							%>
							<input type="text" class='readonly-input' id="alternateId<%=iElementId%>" name="alternateId<%=iElementId%>" readonly value="<%=strElementValue.trim()%>" size="25" maxlength="100"/>
							<%if (lkpPK > 0){
								String selection = "";
								try {
						    		selection = (String)lookupJSON.get("selection");
								} catch (Exception e){}
						    	selection = (StringUtil.isEmpty(selection))? "single" : selection;
						    	%>
						    	<%if ("single".equals(selection)){%>
								<A id="alternateId<%=iElementId%>Link" href=# onClick="moreDetailsFunctions.openLookup(<%=lkpPK %>,'<%=lkpKeyworkStr%>')"><%=LC.L_Select%><%--Select*****--%></A>
								<%} %>
								<%if ("multi".equals(selection)){%>
								<A id="alternateId<%=iElementId%>Link" href=# onClick="moreDetailsFunctions.openMultiLookup(<%=lkpPK %>,'<%=lkpKeyworkStr%>')"><%=LC.L_Select%><%--Select*****--%></A>
								<%} %>
							<%}
					    } catch (Exception e){
					    	//e.printStackTrace();
					    	System.out.println("More Study Details: lookup configuration error!");
					    }			   
					
			}
				
				 else if ((disptype.toLowerCase()).equals("date")) {%>
				 <input name="alternateId<%=iElementId%>" type="text" class="datefield" size="10" readOnly  value="<%=strElementValue.trim()%>">
				<%}
				else
				 { %>
					 <input type = "text"   id="alternateId<%=iElementId%>" name="alternateId<%=iElementId%>" value = "<%=strElementValue.trim()%>" size = "25" maxlength = "100" >	 
				<% }
							%>
					</tr> 
				<input type = "hidden" name = "recordType<%=iElementId%>" value = "<%=strRecordType %>" ></td>
				<input type = "hidden" name = "id<%=iElementId%>" value = "<%=iId%>" >
				<input type = "hidden" name = "elementId" value = "<%=iElementId%>" >
				<input type = "hidden" name = "modName" value = "<%=modName%>" >
			&nbsp;</td>
				<% if(count==3){ count=0;	%>
				</tr> <%}%> 
	<%
		}	
	%>
	</tr>
	<tr><td colspan=7>&nbsp;</td></tr>
	<tr><td colspan=6 align="right"><%=MC.M_CopyParent_SpmenDet%><%--Copy the Parent Specimen Details*****--%>
			<A href="#" onMouseOver="return overlib('<%="<tr><td><font size=1>"+"Fields to be copied"+":</font><font size=1> "+strElementDesc1+"</font></td></tr>"%>',CAPTION,'<%="Help"%>');" onMouseOut="return nd();"><img src="../images/help.png" width="17px" height="17" border="0" />  </A>									
	<input type="checkbox" name="cpMoreDetails" checked></td></tr>
	</table>
	</div></div>
	<% } %>	
<input type="hidden" name="ddlist" value="<%=ddStr%>">
<div id="SpecQuanMoreDetailsDIV" class="portlet portletstatus ui-widget ui-widget-border ui-helper-clearfix ui-corner-all">
	<table width="100%" cellpadding="0" cellspacing="0">
				<tr>
				<td colspan=7>
				<p class="SectionHeadings"><%=LC.L_Quantity%><%--Quantity*****--%></p>
				</td>
	
				<tr>
	
				<td align=right width="13%"><%=LC.L_Expected_Amt%><%--Expected Amount*****--%>&nbsp;</td>
				<td width="15%">
				<Input type = "text" name="expQuantity"   value="<%=exptdQty%>" size=5 MAXLENGTH = 20 align = right>
					<%=dSpecExptdQuUnit%>
				</td>
	
				<td width="13%" align=right><%=LC.L_Collected_Amt%><%--Collected Amount*****--%>&nbsp;</td>
				<td width="24%">
				<Input type = "hidden" name="newCollAmt"   value="">
				<Input type = "hidden" name="prevCollQuantity"   value="<%=origQty%>">
				<Input type = "text" name="origQuantity"   value="<%=origQty%>" size=5 MAXLENGTH = 20 align = right   onblur="autoCalCurrAmt(document.specimen)" >
				<%=dSpecBaseOrigQuUnit%>
				<input type="checkbox" name="aplycolltocurr" checked>Apply to Current Amount
				</td>
	
				<td align=right width="10%"><%=LC.L_Cur_Amt%><%--Current Amount*****--%>&nbsp;</td>
				<td width="15%">
				<Input type = "hidden" name="origavlQuantity"   value="<%=avQty%>">
				<Input type = "text" name="avlQuantity" value="<%=avQty%>" size=5 readonly MAXLENGTH = 20>&nbsp;<%=dSpecQuantUnit%>
				<!--<A href="#" onClick="setCurrQty()"><%--=LC.L_ReCalculate--%><%--Re-Calculate*****--%></A>  --></td>
				</tr>
				<tr><td colspan=7>&nbsp;</td></tr>
	</table>
	</div>	
	<%
	if (mode.equals("M")){
		//JM: 16Oct2007: added for Specimen Status browser
		SpecimenStatusDao spStatDao = specStatJB.getSpecimenStausValues(EJBUtil.stringToNum(pkId));
	
		ArrayList pkSpecimenStats = spStatDao.getPkSpecimenStats();
	    ArrayList fkSpecimens = spStatDao.getFkSpecimens();
	    ArrayList ssDates = spStatDao.getSsDates();
	    ArrayList fkCodelstSpecimenStats = spStatDao.getFkCodelstSpecimenStats();
	    ArrayList ssQuantitys = spStatDao.getSsQuantitys();
	    ArrayList ssQuantityUnits = spStatDao.getSsQuantityUnits();
	    ArrayList ssActions = spStatDao.getSsActions();
	    ArrayList fkStudys = spStatDao.getFkStudys();
	    ArrayList fkUserRecpts = spStatDao.getFkUserRecpts();
	    ArrayList trackingNums = spStatDao.getTrackingNums();
	    ArrayList sSnotes = spStatDao.getSpecimenNotes();
	    ArrayList ssStatusBys = spStatDao.getSsStatusBys();
	    ArrayList ssFlags = spStatDao.getSsFlags();
	
		int len = pkSpecimenStats.size();
				String pkSpecimenStat = "", fkSpecimen = "",
			    ssDate = "", fkCodelstSpecimenStat = "", ssQuantity = "",
			    ssQuantityUnit = "", ssAction = "", fkStudy = "", fkUserRecpt = "",
			    trackingNum = "", sSnote = "", ssStatusBy = "", incDecflag ="", spCdlstSubTypLatest = "",
			    spCdlstDescLatest = "", specimenStatDesc="", specimenStatSubtyp="";
	
		CodeDao statCodeDao =new CodeDao();
		BigDecimal ssQty;
	
	%>
	
	<div id="SpecMoreDetailsDIV" class="portlet portletstatus ui-widget ui-widget-border ui-helper-clearfix ui-corner-all">
	<table  width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td>
		<p class="SectionHeadings"><%=LC.L_Status_Dets%><%--Status Details*****--%></p>
		</td>
	
		<td align="right">
		<A href="#" onclick="openWin('<%=pkId%>','','N',<%=pageRight%>, '','')"><img title="<%=LC.L_Add_New%>" src="./images/Create.gif" border ="0"/></A>
		</td>
	</tr>
	</table>
	
	
	<table class=tableDefault  width="100%" cellpadding="0" cellspacing="0" border="0">
	<TR>
	
		<th width=10% align =center><%=LC.L_Status_Date%><%--Status Date*****--%></th>
	     <th width=15% align =center><%=LC.L_Status%><%--Status*****--%></th>
	     <th width=10%  align =center><%=LC.L_Quantity%><%--Quantity*****--%></th>
	     <th width=15% align =center><%=LC.L_For_Study%><%--For <%=LC.Std_Study%>*****--%></th>
	     <th width=15% align =center><%=LC.L_Recipient%><%--Recipient*****--%></th>
	     <th width=30%  align =center ><%=LC.L_Notes%><%--Notes*****--%></th>
	 <th width=5%  align =center ><%=LC.L_Delete%><%--Delete*****--%></th>
	</TR>
	<%
	
	
		for(int cntr=0;cntr<len;cntr++)
	  	  {
			pkSpecimenStat = ((pkSpecimenStats.get(cntr))==null)?"":(pkSpecimenStats.get(cntr)).toString();
	
			fkSpecimen = ((fkSpecimens.get(cntr))==null)?"":(fkSpecimens.get(cntr)).toString();
	
			ssDate = ((ssDates.get(cntr))==null)?"":(ssDates.get(cntr)).toString();

			int dateLen = ssDate.length();
					 if(dateLen > 1){
					    try {
					        ssDate = DateUtil.dateToString(java.sql.Date.valueOf(ssDate.toString().substring(0,10)));
					    } catch(Exception e) {
						    String tmp1 = DateUtil.prepadYear(ssDate.toString().substring(0,10).trim()).substring(0, 10);
							ssDate = DateUtil.dateToString(java.sql.Date.valueOf(tmp1));
					    }
					 }
	
			fkCodelstSpecimenStat = ((fkCodelstSpecimenStats.get(cntr))==null)?"":(fkCodelstSpecimenStats.get(cntr)).toString();
	
			//JM: 03Sep2009:
			spCdlstSubTypLatest = statCodeDao.getCodeSubtype(EJBUtil.stringToNum((fkCodelstSpecimenStats.get(0)).toString()));
			if (spCdlstSubTypLatest.equals("Depleted")){
				spCdlstDescLatest	= statCodeDao.getCodeDescription(EJBUtil.stringToNum((fkCodelstSpecimenStats.get(0)).toString()));
			}
	
			specimenStatDesc = statCodeDao.getCodeDescription(EJBUtil.stringToNum(fkCodelstSpecimenStat));
			if(specimenStatDesc ==null)
				specimenStatDesc ="";
			
			specimenStatSubtyp = statCodeDao.getCodeSubtype(EJBUtil.stringToNum(fkCodelstSpecimenStat));
			if(specimenStatSubtyp ==null)
				specimenStatSubtyp ="";

			ssQuantity = ((ssQuantitys.get(cntr))==null)?"":(ssQuantitys.get(cntr)).toString();
	
			ssQuantity = ((ssQuantitys.get(cntr))==null)?"":(ssQuantitys.get(cntr)).toString();
			//ssQuantity=(new BigDecimal(ssQuantity).setScale(5, RoundingMode.HALF_UP)).toPlainString();
			Double ssQntity=0.0;
			if (!ssQuantity.equals("")){
				
				matcher=exp.matcher(ssQuantity);
				if(matcher.find())
				{
				ssQuantity=new BigDecimal(ssQuantity).toPlainString();
				matcher=pattern.matcher(ssQuantity);
				}
				else
				{
				 ssQntity=Double.parseDouble(ssQuantity);
				}
				if(matcher.find())
				{
					
					ssQuantity= (new BigDecimal(ssQuantity).setScale(5, RoundingMode.HALF_UP)).toPlainString();
					
					
				}
				
				else
				{
					ssQuantity=ssQntity.toString() ;
				}
			}
			incDecflag = cdSpecStat.getCodeCustomCol(EJBUtil.stringToNum(fkCodelstSpecimenStat));
			//JM: 26Aug2009
			incDecflag = (incDecflag==null)?"":incDecflag;
			ssQuantityUnit = ((ssQuantityUnits.get(cntr))==null)?"":(ssQuantityUnits.get(cntr)).toString();
			String qUnitDesc = statCodeDao.getCodeDescription(EJBUtil.stringToNum(ssQuantityUnit));
			if(qUnitDesc ==null) qUnitDesc ="";
	
			/*try {
			    BigDecimal bdAvQty = new BigDecimal(ssQuantity);
			    if (BigDecimal.ZERO.compareTo(bdAvQty) == 0) {
			        ssQuantity = "";
			        qUnitDesc = "";
			    }
			} catch (Exception e) {}*/
	
			//calculating actual available qty
			if (ssQuantity.equals(""))
			{
				ssQty = new BigDecimal("0");
			}
			else 
			{
				             ssQty = new BigDecimal("0");
			
					    matcher = pattern.matcher(ssQty.toString());
						Double ssQnty=Double.parseDouble(ssQty.toString());
						if(matcher.find())
						{
							
							ssQty= ssQty.setScale(5, RoundingMode.HALF_UP);
							
							
						}
						
						else
						{
							ssQty=new BigDecimal(ssQnty) ;
						}
					
	
			}
	//JM: 11Sept2009:
	if (Float.parseFloat(ssQuantity)==0.0 && qUnitDesc.equals("")){
		ssQuantity = "";
	}
	
	
			if (incDecflag.equals("+"))
				actCurrentQty = actCurrentQty .add( ssQty);
			else if (incDecflag.equals("-"))
				actCurrentQty = actCurrentQty .subtract( ssQty);
	
	
			ssAction = ((ssActions.get(cntr))==null)?"":(ssActions.get(cntr)).toString();
			fkStudy = ((fkStudys.get(cntr))==null)?"":(fkStudys.get(cntr)).toString();
			if (fkStudy ==null || fkStudy.equals("") || fkStudy.equals("0")) fkStudy ="";
			//get the study number to be displyed
			String studyNum = "";
			if (!StringUtil.isEmpty(fkStudy)){
			stdJB.setId(EJBUtil.stringToNum(fkStudy));
			stdJB.getStudyDetails();
			studyNum = stdJB.getStudyNumber();
			studyNum=(studyNum==null)?"":studyNum;
	
			}
			fkUserRecpt = ((fkUserRecpts.get(cntr))==null)?"":(fkUserRecpts.get(cntr)).toString();
			if (fkUserRecpt ==null || fkUserRecpt.equals("") || fkUserRecpt.equals("0")) fkUserRecpt ="";
	
	
	
			//get the user name to be displyed
			String usrName = "";
			if (!StringUtil.isEmpty(fkUserRecpt)){
	
			usrJB.setUserId(EJBUtil.stringToNum(fkUserRecpt));
			usrJB.getUserDetails();
	
			fName=usrJB.getUserFirstName();
				fName=(fName==null)?"":fName;
	
			lName=usrJB.getUserLastName();
				lName=(lName==null)?"":lName;
	
			usrName  =  fName + " " + lName ;
	
			}
	
	
			trackingNum = ((trackingNums.get(cntr))==null)?"":(trackingNums.get(cntr)).toString();
	
			sSnote = ((sSnotes.get(cntr))==null)?"":(sSnotes.get(cntr)).toString();
			if(sSnote.length()>50)
				sSnote = sSnote.substring(0,50) +"...";
	
			ssStatusBy = ((ssStatusBys.get(cntr))==null)?"":(ssStatusBys.get(cntr)).toString();
	
	
			if ((cntr%2)!=0) {
	     %>
	
		<tr style="background-color:#d7d7d7;">
			<%
	
			}
	
			else{
	
	  %>
		<tr style="background-color:#f7f7f7;">
			<%
	
			}
	  %>
	   <td width="5%"><%=ssDate%></td>
	   <td width="10%"><A href="#" onclick="openWin('<%=pkId%>','<%=pkSpecimenStat%>', 'M',<%=pageRight%>, '','')"><%=specimenStatDesc%></A></td>
	   <td width="10%"><%=ssQuantity%>&nbsp;<%=qUnitDesc%>
	   <input type="hidden" name="statusqty_<%=cntr %>" id="statusqty_<%=cntr %>" value="<%=ssQuantity %>">
	   <input type="hidden" name="statuslength" id="statuslength" value="<%=len %>"></td>
	   <td width="10%"><%=studyNum%></td>
	   <td width="15%"><%=usrName%></td>
	   <td width="20%"><%=sSnote%></td>
	   <td width="10%"><A href="#" onClick ="return f_delete('<%=pkSpecimenStat%>',<%=len%>,<%=pageRight%>, '','<%=ssQuantity%>','<%=specimenStatSubtyp%>')"><img src="./images/delete.gif" border="0" title="<%=LC.L_Delete%>" align="left"/></A></td><!--KM-->
	   </tr>
	
		<%
	
		}//end of for loop
		%>
	<input type="hidden" name="codeDesc" value="<%=spCdlstDescLatest%>">
	</table>
	</div>
	
	<%
	} //end Mode
	
	%>
	<%-- <div id="SpecMoreDetailsDIV" class="portlet portletstatus ui-widget ui-widget-border ui-helper-clearfix ui-corner-all">
		<div id="specimenServiceProvidercontent" onclick="toggleDiv('specimenServiceProvider');" class="portlet-header portletstatus ui-widget 
		ui-widget-header ui-widget-content ui-helper-clearfix ui-corner-all" style="background-color:#CCCCCC;">
			<span class="ui-icon ui-icon-triangle-1-s"></span>
		<%=LC.L_Service_Providers%>
		</div>
		
	<div id="specimenServiceProvider" >
		
	 <table width="100%" cellspacing="0" cellpadding="0" >
		<!--<tr>
		<td width="13%">Processing Type </td>
		    <td><%=dpProcType%></td>
	
		</tr>--><br>
		<tr>
			<td class=tdDefault align="right"width="13%"><%=LC.L_Pathologist%>Pathologist***** &nbsp;</td>
			<input type="hidden" name="userPathologistId" value="<%=userPathologistId%>">
			<td class=tdDefault align="center" width="18%">
			<Input type = "text" name="userPathologistName"   value="<%=userPathologistName%>" size=25 align = right readonly>
			<A href="#" onClick="return openCommonUser('specimen','userPathologistId','userPathologistName')"><%=LC.L_Select%>Select*****</A></td>
	
			<td class=tdDefault align="right"width="35%"><%=LC.L_Surgeon%>Surgeon***** &nbsp;</td>
			<input type="hidden" name="userSurgeonId" value="<%=userSurgeonId%>">
			<td class=tdDefault >
			<Input type = "text" name="userSurgeonName"   value="<%=userSurgeonName%>" size=25 align = right readonly>
			<A href="#" onClick="return openCommonUser('specimen','userSurgeonId','userSurgeonName')"><%=LC.L_Select%>Select*****</A></td>
		 </tr>
		<tr>
			<td class=tdDefault align="right"><%=LC.L_Collecting_Technician%>Collecting Technician***** &nbsp;</td>
			<input type="hidden" name="userCollTechId" value="<%=userCollTechId%>">
			<td class=tdDefault align="center">
			<Input type = "text" name="userCollTechName"   value="<%=userCollTechName%>" size=25 align = right readonly>
			<A href="#" onClick="return openCommonUser('specimen','userCollTechId','userCollTechName')"><%=LC.L_Select%>Select*****</A></td>
	
			<td class=tdDefault align="right"><%=LC.L_Processing_Technician%>Processing Technician***** &nbsp;</td>
			<input type="hidden" name="userProcTechId" value="<%=userProcTechId%>">
			<td class=tdDefault >
			<Input type = "text" name="userProcTechName"   value="<%=userProcTechName%>" size=25 align = right readonly>
			<A href="#" onClick="return openCommonUser('specimen','userProcTechId','userProcTechName')"><%=LC.L_Select%>Select*****</A></td>
		 </tr>
	
			<!--
			<tr>
			<td class=tdDefault>Time of Removal &nbsp;</td>
			<td><input type="text" name="TORemovalHH" value="<%=TORemovalHH%>" maxlength=2 size=2>:<input type="text" name="TORemovalMM" value="<%=TORemovalMM%>" maxlength=2 size=2>:
			<input type="text" name="TORemovalSS" value="<%=TORemovalSS%>" maxlength=2 size =2>
			 </td>
	
			<td class=tdDefault>Time of Freezing &nbsp;</td>
			<td><input type="text" name="TOFreezeHH" value="<%=TOFreezeHH%>" maxlength=2 size=2>:<input type="text" name="TOFreezeMM" value="<%=TOFreezeMM%>" maxlength=2 size=2>:
			<input type="text" name="TOFreezeSS" value="<%=TOFreezeSS%>" maxlength=2 size=2>
			 </td>
		 	</tr>
		 	-->
	
			<!--<td class=tdDefault>Time of Removal &nbsp;</td>-->
			<input type="hidden" name="TORemovalHH" value="<%=TORemovalHH%>" maxlength=2 size=2>
			<input type="hidden" name="TORemovalMM" value="<%=TORemovalMM%>" maxlength=2 size=2>
			<input type="hidden" name="TORemovalSS" value="<%=TORemovalSS%>" maxlength=2 size=2>
			<!--<td class=tdDefault>Time of Freezing &nbsp;</td>-->
			<input type="hidden" name="TOFreezeHH" value="<%=TOFreezeHH%>" maxlength=2 size=2>
			<input type="hidden" name="TOFreezeMM" value="<%=TOFreezeMM%>" maxlength=2 size=2>
			<input type="hidden" name="TOFreezeSS" value="<%=TOFreezeSS%>" maxlength=2 size=2>
	
	 <tr>
		 	<td colspan="5"  align="right" >
				
					&nbsp;<%=MC.M_CopyParent_SpmenDet%>Copy the Parent Specimen Details*****
					&nbsp;&nbsp;&nbsp;&nbsp;
					<A href="#" onMouseOver="return overlib('<%="<tr><td><font size=1>"+"Fields to be copied"+":</font><font size=1> "+CopyFieldsTxt3+"</font></td></tr>"%>',CAPTION,'<%="Help"%>');" onMouseOut="return nd();"><img src="../images/help.png" width="17px" height="17" border="0" />  </A>
					<input type="checkbox"  name="cpPatDe_Section_3" checked>		 		
		 	</td>
		  </tr>
	
	</table>
	</div></div>
	 --%>
	<!--JM: 28May2009---------------------------------------------------------------------------------------->
	<%
	
	int num_Proc_Stat = 0;
	
	if (mode.equals("M")){
	
		//JM: 16Oct2007: added for Specimen Status browser
		SpecimenStatusDao spStatDao = specStatJB.getSpecimenStausProcessValues(EJBUtil.stringToNum(pkId));
	
		ArrayList pkSpecimenStats = spStatDao.getPkSpecimenStats();
	    ArrayList fkSpecimens = spStatDao.getFkSpecimens();
	    ArrayList ssDates = spStatDao.getSsDates();
	    ArrayList fkCodelstSpecimenStats = spStatDao.getFkCodelstSpecimenStats();
	    ArrayList ssQuantitys = spStatDao.getSsQuantitys();
	    ArrayList ssQuantityUnits = spStatDao.getSsQuantityUnits();
	    ArrayList ssActions = spStatDao.getSsActions();
	    ArrayList fkStudys = spStatDao.getFkStudys();
	    ArrayList fkUserRecpts = spStatDao.getFkUserRecpts();
	    ArrayList trackingNums = spStatDao.getTrackingNums();
	    ArrayList sSnotes = spStatDao.getSpecimenNotes();
	    ArrayList ssStatusBys = spStatDao.getSsStatusBys();
	    ArrayList ssProcTypes = spStatDao.getSsProcTypes();
	
	
	
		int len = pkSpecimenStats.size();
				String pkSpecimenStat = "", fkSpecimen = "",
			    ssDate = "", fkCodelstSpecimenStat = "", ssQuantity = "",
			    ssQuantityUnit = "", ssAction = "", fkStudy = "", fkUserRecpt = "",
			    trackingNum = "", sSnote = "", ssStatusBy = "", ssProcType = "";
	
		CodeDao statCodeDao =new CodeDao();
	
		num_Proc_Stat = len;
	
	
	
	
	
	%>
	
	<div id="SpecMoreDetailsDIV" class="portlet portletstatus ui-widget ui-widget-border ui-helper-clearfix ui-corner-all">
	<table  width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td>
		<p class="SectionHeadings"><%=LC.L_Processing_Status%><%--Processing Status*****--%></p>
		</td>
	
		<td align="right">
		<A href="#" onclick="openWin('<%=pkId%>','','N',<%=pageRight%>, 'ProcLink','process')"><img title="<%=LC.L_Add_New%>" src="./images/Create.gif" border ="0"></A>
		</td>
	</tr>
	</table>
	
	
	<table class=tableDefault  width="100%" cellpadding="0" cellspacing="0" border="0">
	<TR>
		 <th width=10% align =center><%=LC.L_Status_Date%><%--Status Date*****--%></th>
	     <th width=10% align =center><%=LC.L_Status%><%--Status*****--%></th>
	     <th width=15% align =center><%=LC.L_Process%><%--Process*****--%></th>
	     <th width=10% align =center><%=LC.L_Quantity%><%--Quantity*****--%></th>
	     <th width=15% align =center><%=LC.L_For_Study%><%--For <%=LC.Std_Study%>*****--%></th>
	     <th width=15% align =center><%=LC.L_Recipient%><%--Recipient*****--%></th>
	     <th width=30% align =center><%=LC.L_Notes%><%--Notes*****--%></th>
	 	 <th width=5%  align =center><%=LC.L_Delete%><%--Delete*****--%></th>
	</TR>
	<%
	
		for(int cntr=0;cntr<len;cntr++)
	  	  {
			pkSpecimenStat = ((pkSpecimenStats.get(cntr))==null)?"":(pkSpecimenStats.get(cntr)).toString();
			fkSpecimen = ((fkSpecimens.get(cntr))==null)?"":(fkSpecimens.get(cntr)).toString();
	
			ssDate = ((ssDates.get(cntr))==null)?"":(ssDates.get(cntr)).toString();
			int dateLen = ssDate.length();
					 if(dateLen > 1){
					    try {
					        ssDate = DateUtil.dateToString(java.sql.Date.valueOf(ssDate.toString().substring(0,10)));
					    } catch(Exception e) {
						    String tmp1 = DateUtil.prepadYear(ssDate.toString().substring(0,10).trim()).substring(0, 10);
							ssDate = DateUtil.dateToString(java.sql.Date.valueOf(tmp1));
					    }
					 }
	
			fkCodelstSpecimenStat = ((fkCodelstSpecimenStats.get(cntr))==null)?"":(fkCodelstSpecimenStats.get(cntr)).toString();
			String specimenStatDesc = statCodeDao.getCodeDescription(EJBUtil.stringToNum(fkCodelstSpecimenStat));
			if(specimenStatDesc ==null)
				specimenStatDesc ="";
			
			String specimenStatSubtyp = statCodeDao.getCodeSubtype(EJBUtil.stringToNum(fkCodelstSpecimenStat));
			if(specimenStatSubtyp ==null)
				specimenStatSubtyp ="";
	
			ssQuantity = ((ssQuantitys.get(cntr))==null)?"":(ssQuantitys.get(cntr)).toString();
			 matcher=exp.matcher(ssQuantity);
			 Double ssQntity=0.0;
			 if(matcher.find())
			 {
				 ssQuantity=new BigDecimal(ssQuantity).toPlainString();
				matcher = pattern.matcher(ssQuantity);
			 }
			 else
			 {
				 ssQntity=Double.parseDouble(ssQuantity);
			 }
				if(matcher.find())
				{	
					ssQuantity= (new BigDecimal(ssQuantity).setScale(5, RoundingMode.HALF_UP)).toPlainString();
				}
				
				else
				{
					ssQuantity=ssQntity.toString() ;
				}
			ssQuantityUnit = ((ssQuantityUnits.get(cntr))==null)?"":(ssQuantityUnits.get(cntr)).toString();
			String qUnitDesc = statCodeDao.getCodeDescription(EJBUtil.stringToNum(ssQuantityUnit));
			if(qUnitDesc ==null) qUnitDesc ="";
	
			/*try {
			    BigDecimal bdAvQty = new BigDecimal(ssQuantity);
			    if (BigDecimal.ZERO.compareTo(bdAvQty) == 0) {
			        ssQuantity = "";
			        qUnitDesc = "";
			    }
			} catch (Exception e) {}*/
	
	
			ssAction = ((ssActions.get(cntr))==null)?"":(ssActions.get(cntr)).toString();
			fkStudy = ((fkStudys.get(cntr))==null)?"":(fkStudys.get(cntr)).toString();
			if (fkStudy ==null || fkStudy.equals("") || fkStudy.equals("0")) fkStudy ="";
			//get the study number to be displyed
			String studyNum = "";
			if (!StringUtil.isEmpty(fkStudy)){
			stdJB.setId(EJBUtil.stringToNum(fkStudy));
			stdJB.getStudyDetails();
			studyNum = stdJB.getStudyNumber();
			studyNum=(studyNum==null)?"":studyNum;
	
			}
			fkUserRecpt = ((fkUserRecpts.get(cntr))==null)?"":(fkUserRecpts.get(cntr)).toString();
			if (fkUserRecpt ==null || fkUserRecpt.equals("") || fkUserRecpt.equals("0")) fkUserRecpt ="";
	
	
	
			//get the user name to be displyed
			String usrName = "";
			if (!StringUtil.isEmpty(fkUserRecpt)){
	
			usrJB.setUserId(EJBUtil.stringToNum(fkUserRecpt));
			usrJB.getUserDetails();
	
			fName=usrJB.getUserFirstName();
				fName=(fName==null)?"":fName;
	
			lName=usrJB.getUserLastName();
				lName=(lName==null)?"":lName;
	
			usrName  =  fName + " " + lName ;
	
			}
	
	
			trackingNum = ((trackingNums.get(cntr))==null)?"":(trackingNums.get(cntr)).toString();
	
			sSnote = ((sSnotes.get(cntr))==null)?"":(sSnotes.get(cntr)).toString();
			if(sSnote.length()>50)
				sSnote = sSnote.substring(0,50) +"...";
	
			ssStatusBy = ((ssStatusBys.get(cntr))==null)?"":(ssStatusBys.get(cntr)).toString();
	
	
	
			ssProcType = ((ssProcTypes.get(cntr))==null)?"":(ssProcTypes.get(cntr)).toString();
	
	
			if ((cntr%2)!=0) {
	     %>
	
		<tr style="background-color:#d7d7d7;">
			<%
	
			}
	
			else{
	
	  %>
		<tr style="background-color:#f7f7f7;">
			<%
	
			}
	  %>
	   <td width="5%"><%=ssDate%></td>
	   <td width="10%"><A href="#" onclick="openWin('<%=pkId%>','<%=pkSpecimenStat%>', 'M',<%=pageRight%>, 'ProcLink','process')"><%=specimenStatDesc%></A></td>
	   <td width="10%"><%=ssProcType%>&nbsp;</td>
	   <td width="10%"><%=ssQuantity%>&nbsp;<%=qUnitDesc%></td>
	   <td width="10%"><%=studyNum%></td>
	   <td width="15%"><%=usrName%></td>
	   <td width="20%"><%=sSnote%></td>
	   <td width="10%"><A href="#" onClick ="return f_delete('<%=pkSpecimenStat%>',<%=len%>,'<%=pageRight%>', 'ProcLink','<%=ssQuantity%>','<%=specimenStatSubtyp%>')"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" align="left"/></A></td>
	   </tr>
	
		<%
	
		}//end of for loop
		%>
	</table>
	</div>
		<%
	} //end Mode
	
	%>
	<!---JM: 28May2009------------------------------------------------------------------->
	
	
	<div id="specimenChildDetailSec4" class="portlet portletstatus ui-widget ui-widget-border ui-helper-clearfix ui-corner-all">
	<div id='specimenChildDetailSec4'  width="99%">
	<table class=tableDefault  width="100%" cellpadding="0" cellspacing="0" border="0">
	
	<tr><td>&nbsp;</td></tr>
	<tr>
	<td ><p class="SectionHeadings"> <%=MC.M_Create_ChildSpmen%><%--Create New Child Specimens*****--%></p></td>
	</tr>
	</table>
	<table class=tableDefault  width="100%" cellpadding="0" cellspacing="0" border="0" >
	<tr>
	<td width="8%">#  <%=LC.L_OfChild_Spmen%><%--of Child Specimens*****--%></td>
	<td width="8%"><%=LC.L_Quantity%><%--Quantity*****--%></td>
	<td width="15%"><%=LC.L_Process%><%--Process*****--%></td>
	<td width="20%" colspan=3><%=MC.M_Processing_DateTime%><%--Processing Date and Time*****--%></td>
	</tr>
	
	<tr>
	<td width="8%">&nbsp; <input type="text" name="noOfChild" size=6 MAXLENGTH = 100 onblur="autoCalQuant(document.specimen)"></td>
	
	<td width="9%"><input type="text" name="quantity" size=6 MAXLENGTH = 100></td>
	<td width="12%"><%=dcProcType%></td>
	
	
	
	<input type="hidden" name="num_Processing_Stat" value="<%=num_Proc_Stat%>">
	
	<%-- INF-20084 Datepicker-- AGodara --%>
	<td width="27%"><Input type = "text" name="procDate"  value="" size=10 MAXLENGTH =20 class="datefield" >
	<input type="text" name="processHH" value="<%=processHH%>" maxlength=2 size=1>
	:<input type="text" name="processMM" value="<%=processMM%>" maxlength=2 size=1>
	:<input type="text" name="processSS" value="<%=processSS%>" maxlength=2 size=1>
	<a href="#" onclick="return setCurrentTime(document.specimen, 2)"><%=LC.L_Cur_Time%><%--Current Time*****--%></a>&nbsp;
	<a href="#" onclick="return resetCurrentTime(document.specimen, 2)"><%=LC.L_Reset%><%--Reset*****--%></a>
	&nbsp;<%=MC.M_CopyParent_SpmenDet%><%--Copy the Parent Specimen Details*****--%><A href="#" onMouseOver="return overlib('<%="<tr><td><font size=1>"+"Fields to be copied"+":</font><font size=1> "+CopyFieldsTxt1+", Type </font></td></tr>"%>',CAPTION,'<%="Help"%>');" onMouseOut="return nd();"><img src="../images/help.png" width="17px" height="17" border="0" />  </A><input type="checkbox" name="cpParentDet" checked>
	</td>
	</tr>
	
	
	
	</table>
	
	
		<%
		if (mode.equals("M")){
	
	
	//JM: 30Jan2008: 'INV # 4.2.7: Child Specimens': added for Specimen children browser
	
		SpecimenDao specDao = specJB.getSpecimenChilds(EJBUtil.stringToNum(pkId), EJBUtil.stringToNum(accountId));
	
		ArrayList pkSpecimens = specDao.getPkSpecimens();
	    ArrayList specimenIds = specDao.getSpecimenIds();
	    ArrayList specCollDates = specDao.getSpecCollDates();
	    ArrayList specTypes = specDao.getSpecTypes() ;
	    ArrayList statuss = specDao.getStatuss() ;
	    ArrayList specQntys = specDao.getSpecQntys();
	    ArrayList spectQuntyUnits = specDao.getSpectQuntyUnits() ;
	    ArrayList specOrigQntys = specDao.getSpecOrigQntys();
	    ArrayList specOrigQuntyUnits = specDao.getSpecOrigQuntyUnits() ;
	    ArrayList specProcTypes = specDao.getSpecProcTypes();
	
	    ArrayList specProcTypeIds = specDao.getSpecProcTypesPK();
	     ArrayList specTypeIds = specDao.getSpecTypesPK();
	
	    ArrayList spectQuntyUnitPKs = specDao.getSpecQuntyUnitPk() ;
	
	    ArrayList arSpecLocationDesc = specDao.getSpecLocationDescs();
	    ArrayList arSpecLocation = specDao.getSpecLocations();
		ArrayList statussFk = specDao.getStatusTypes() ;
	    int childLen = pkSpecimens.size();
	
	
		String pkSpecimen = "";
	    String specimenId = "";
	    String specCollDate = "";
	    String specType = "";
	    String specStatus = "";
	    String specQnty = "";
	    String spectQuntyUnit = "";
	    String specOrigQnty = "";
	    String specOrigQuntyUnit = "";
	    String specProcType = "";
	    String specLocation = "";
	    String specLocationPK = "";
	    String statusFk = "";

	    BigDecimal childSpecQty;
		%>
	<input type=hidden name="childcount" id="childcount" value=<%=childLen%>>
	
	<table class=tableDefault width="100%" border="0">
	<tr><td>&nbsp;</td></tr>
	<tr>
	<td><p class="SectionHeadings"> <%=LC.L_Existing_ChildSpecimens%><%--Existing Child Specimens*****--%></p></td>
	
	</tr>
	</table>
	
	<table class=tableDefault  width="100%" cellpadding="0" cellspacing="0" border="0" >
	<tr>
	<td width="35%"><%=LC.L_Process%><%--Process*****--%></td>
	<td width="30%"><%=MC.M_Processing_DateTime%><%--Processing Date and Time*****--%></td>
	</tr>
	
	<tr>
	<td width="40%"><%=decProcType%></td>
	
	<script>if(isIe==true){
		document.write('<td width="34%">');
	}else{
		document.write('<td width="31%">');
		}
	</script>
	<%-- INF-20084 Datepicker-- AGodara --%>
	<Input type = "text" name="eProcDate"  value="" size=10 MAXLENGTH =20 class="datefield" >
	<input type="text" name="eprocessHH" value="<%=eprocessHH%>" maxlength=2 size=1>
	:<input type="text" name="eprocessMM" value="<%=eprocessMM%>" maxlength=2 size=1>
	:<input type="text" name="eprocessSS" value="<%=eprocessSS%>" maxlength=2 size=1>
	<a href="#" onclick="return setCurrentTime(document.specimen, 3)"><%=LC.L_Cur_Time%><%--Current Time*****--%></a>&nbsp;
	<a href="#" onclick="return resetCurrentTime(document.specimen, 3)"><%=LC.L_Reset%><%--Reset*****--%></a>
	</td>
	
	</tr>
	
	<tr>
	<td>
	<input type="radio" name="checkme" id="checkmeAllChild" value="allChild" ><%=MC.M_ApplyAll_ChildSpmen%><%--Apply to All Child Specimens*****--%> <br>
	<input type="radio" name="checkme" id="checkmeSelChild" value="selChild" ><%=MC.M_ApplyTo_ChildSpmen%><%--Apply to Selected Child Specimens*****--%> <br>
	</td>
	<td>&nbsp;</td>
	</tr>
	
	</table><br>

<div align = right><table><tr><td align =right class="tabFormTopN"><A href="#"   onClick=" return printLabelchildspecimen();"><%=LC.L_Print_Label%> <%--PRINT LABEL*****--%></A>(Applies to Selected Child Specimens)</td></tr></table></div>
	<table  width="100%" cellpadding="0" cellspacing="0" border="0">
	
	
	<TR>
	      <th width="4%">Select/<%=LC.L_Edit%></th>
	      <th width=5%  align =center><%=LC.L_Delete%><%--Delete*****--%></th>
		 <th width=8% align =center><%=LC.L_Specimen_Id%><%--Specimen Id*****--%></th>
	     <th width=12% align =center><%=LC.L_Collection_Date%><%--Collection Date*****--%></th>
	     <th width=12%  align =center><%=LC.L_Type%><%--Type*****--%></th>
	     <th width=12% align =center><%=LC.L_Status%><%--Status*****--%></th>
		 <th width=12% align =center><%=LC.L_Original_Quantity%><%--Original Quantity*****--%></th>
	     <th width=12% align =center><%=LC.L_Cur_Qty%><%--Current Quantity*****--%></th>
	     <th width=12%  align =center ><%=LC.L_Process%><%--Process*****--%></th>
	 <th colspan="10"   align =center ><%=LC.L_Location%><%--Location*****--%></th>
	</TR>
	
	
		<%
	
	
	
		if (childLen==0){
		%>
	
			<TR>
	
		 <td colspan="10" align="center">  <%=MC.M_NoChildSpecimens%><%--No Child Specimens*****--%> <td width="4%">
	
		 	</TR>
	
		<%}
	CodeDao cdStatusTyp = new CodeDao();
	cdStatusTyp.getCodeValuesFilterCustom1("specimen_stat","process");
	ArrayList statusTyp = cdStatusTyp.getCId();
	String statusTypStr = "";
	if(statusTyp != null && statusTyp.size() > 0){
		for(int i=0;i<statusTyp.size();i++){
			statusTypStr = (Integer)statusTyp.get(i) + ",";
		}
		if(statusTypStr.length() > 0)
			statusTypStr = statusTypStr.substring(0,statusTypStr.length()-1);
	}
		
	
		for ( int childCntr = 0; childCntr < pkSpecimens.size(); childCntr++){
	
	
			pkSpecimen = ((pkSpecimens.get(childCntr))==null)?"":(pkSpecimens.get(childCntr)).toString();
			specimenId = ((specimenIds.get(childCntr))==null)?"":(specimenIds.get(childCntr)).toString();
			specCollDate = ((specCollDates.get(childCntr))==null)?"":(specCollDates.get(childCntr)).toString();
			int dateLen = specCollDate.length();
				 if(dateLen > 1){
				    try {
				        specCollDate = DateUtil.dateToString(java.sql.Date.valueOf(specCollDate.toString().substring(0,10)));
				    } catch(Exception e) {
					    String tmp1 = DateUtil.prepadYear(specCollDate.toString().substring(0,10).trim()).substring(0, 10);
					    specCollDate = DateUtil.dateToString(java.sql.Date.valueOf(tmp1));
				    }
				 }
	
	
			specType = ((specTypes.get(childCntr))==null)?"":(specTypes.get(childCntr)).toString();
			specStatus = ((statuss.get(childCntr))==null)?"":(statuss.get(childCntr)).toString();
			specQnty = ((specQntys.get(childCntr))==null)?"":(specQntys.get(childCntr)).toString();
			specOrigQnty = ((specOrigQntys.get(childCntr))==null)?"":(specOrigQntys.get(childCntr)).toString();
			matcher = pattern.matcher(specQnty);
			if(!specQnty.equals(""))
			{
				Double specQty=Double.parseDouble(specQnty);
				if(matcher.find())
				{
					specQnty= (new BigDecimal(specQnty).setScale(5, RoundingMode.HALF_UP)).toPlainString();
					
					
				}
				
				else
				{
					specQnty=specQty.toString() ;
				}
			}
			 matcher = pattern.matcher(specOrigQnty);
			if(!specOrigQnty.equals(""))
			{
				Double specOrigQty=Double.parseDouble(specOrigQnty);
				if(matcher.find())
				{
					specOrigQnty= (new BigDecimal(specOrigQnty).setScale(5, RoundingMode.HALF_UP)).toPlainString();
					
					
				}
				
				else
				{
					specOrigQnty=specOrigQty.toString() ;
				}
			}
			statusFk = ((statussFk.get(childCntr))==null)?"":(statussFk.get(childCntr)).toString();
			if (specOrigQnty.equals(""))
				childSpecQty = new BigDecimal("0");
			else childSpecQty = new BigDecimal(specOrigQnty);
	
			//calculating actual available qty
			actCurrentQty = actCurrentQty .subtract(childSpecQty);
			//actCurrentQty = (float) (Math.round(actCurrentQty*100.0f)/100.0f);
	
			spectQuntyUnit = ((spectQuntyUnits.get(childCntr))==null)?"":(spectQuntyUnits.get(childCntr)).toString();
			specOrigQuntyUnit = ((specOrigQuntyUnits.get(childCntr))==null)?"":(specOrigQuntyUnits.get(childCntr)).toString();
	
			specProcType = ((specProcTypes.get(childCntr))==null)?"":(specProcTypes.get(childCntr)).toString();
	
			specLocation= (String)arSpecLocationDesc.get(childCntr);
	
	    	specLocationPK = (String)arSpecLocation.get(childCntr);
	    String storageCapacity="";
		String multiSpecimen="";
		String capacityUnit="";	
		int storageCount = 0,storageChildCount = 0;
		int codelstItem = 0;
		CodeDao cd5 = new CodeDao();
		codelstItem = cd5.getCodeId("cap_unit", "Items");
		System.out.println("specLocationPKspecLocationPK :"+specLocationPK);
	    if (StringUtil.isEmpty(specLocation))
	    {
	    		specLocation = "";
	    		specLocationPK = "";
	    }
		else
		{
		storageB.setPkStorage(EJBUtil.stringToNum(specLocationPK));
		storageB.getStorageDetails();
		storageCapacity = storageB.getStorageCapNum();
		multiSpecimen = storageB.getMultiSpecimenStorage();
		capacityUnit = storageB.getFkCodelstCapUnits();
		storageCount = storageB.getStorageCount(specLocationPK);
		storageChildCount = storageB.getChildCount(specLocationPK);
		System.out.println("storageCapacity"+storageCapacity+" "+multiSpecimen+" "+capacityUnit+" "+storageCount+" "+storageChildCount);
		}
		String dChildSpecType = cdSpecTyp.toPullDown("specType_" + pkSpecimen,EJBUtil.stringToNum((String)specTypeIds.get(childCntr)));
		String dChildSpecStatus = cdSpecStat.toPullDown("specstatus_" + pkSpecimen,EJBUtil.stringToNum(statusFk));
		dChildSpecStatus = dChildSpecStatus.replaceFirst("<SELECT", "<SELECT onChange = hideUnhideProsTyp(this,document.specimen,'"+pkSpecimen+"',"+specProcType.length()+"); ");
		String dChildSpecprocType = cdProcTyp.toPullDown("specproctype_" + pkSpecimen,EJBUtil.stringToNum((String)specProcTypeIds.get(childCntr)));
	
		//String dspec_q_unit = cdSpecQuantUnit.toPullDown("spectquntyunit_" + specimenId,EJBUtil.stringToNum((String) spectQuntyUnitPKs.get(childCntr)));
	
	
		//String dChildSpecStatus = cdSpecStat.toPullDownUsingDescription("specstatus_" + specimenId,specStatus);
	
		if ((childCntr%2)!=0) {
	     %>
	
		<tr style="background-color:#d7d7d7;">
			<%
	
			}
	
			else{
	
	  %>
		<tr style="background-color:#f7f7f7;">
			<%
	
			}
	  %>
	
		<%-- Bug#9971 Date:26-June-2012 Ankit  --%>
		<td width="4%"><input type="checkbox" value=<%=pkSpecimen%> id="selectEdit<%=childCntr%>" name="selectEdit" onClick="f_edit(this,document.specimen,<%=pkSpecimen%>,<%=specProcType.length()%>)"/></td>
		<td width="4%"><A href="#" onClick =" return deleteSpecimens(<%=pkSpecimen%>,<%=pageRight%>)" ><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" align="left"/></A></td>
		 
		 <td width="8%" align =center>
		 
		  <span id="childid_e_<%=pkSpecimen%>" style="display: block;">
		  <A href="specimendetails.jsp?mode=M&pkId=<%=pkSpecimen%>&FromPatPage=<%=FromPatPage %>&patientCode=<%=patientCode%>&pkey=<%=patientId1%>"><%=specimenId%></A>
	      </span>
		 
		 <span id="child_id_e_<%=pkSpecimen%>" style="display: none ;">
         <input type = "hidden" value = <%=pkSpecimen%> id="childSpecIds_<%=childCntr%>" name="childSpecIds" >
	     <input id = "childid_<%=pkSpecimen%>" name="childid_<%=pkSpecimen%>" onblur="processAjaxCall('<%=pkSpecimen %>');"  type="text" value='<%=specimenId %>'/>
	     </span>
		</td></td>
	     <td width="12%" align =center><span id="colldate_<%=pkSpecimen%>"><%=specCollDate%></span>
	     <span id="colldate_e_<%=pkSpecimen%>" style="display: none;">
	     <input name="colldate_e_<%=pkSpecimen%>" class="datefield" size=10 type="text" value='<%=specCollDate%>' />
	      </span></td>
	
	     <td width="12%" align =center><span id="spectype_<%=pkSpecimen%>"><%=specType%></span>
	     <span id="spectype_e_<%=pkSpecimen%>" style="display: none;"><%=dChildSpecType%></span>     </td>
	     <td width="12%" align =center>
	
	     <span id="specstatus_<%=pkSpecimen%>"><%=specStatus%></span>
	     <span id="specstatus_e_<%=pkSpecimen%>" style="display: none;"><%=specStatus%><BR>
	     <%=LC.L_Change_Status%><%--Change Status*****--%>:<%=dChildSpecStatus%>
			<input name="old_status_pk_<%=pkSpecimen%>" type="hidden" value='<%=statusFk %>'/>
	     &nbsp;&nbsp;
	     <%-- INF-20084 Datepicker-- AGodara --%>
	     <input name="specstatusdate_e_<%=pkSpecimen%>" size=10 type="text" class="datefield"  />
	     </span>
	
	
	     </td>
		<td width="12%" align =center>
			<span id="specorigqnty_e_<%=pkSpecimen%>" style="display: none;">
	     		<input name="specorigqnty_e_<%=pkSpecimen%>" size=5 type="text" value='<%=specOrigQnty%>'/> &nbsp;<%=specOrigQuntyUnit%>
	   	  </span>
			<input type="hidden" name="specorigqnty_<%=childCntr %>" id="specorigqnty_<%=childCntr %>" value='<%=specOrigQnty%>'>
	     	<span id="specorigqnty_<%=pkSpecimen%>">
	     		<%=specOrigQnty%>&nbsp;<%=specOrigQuntyUnit%>
			</span>
	     </td>
		 <td width="12%" align =center>
			<span id="specqnty_e_<%=pkSpecimen%>" style="display: none;">
	    		<input name="specqnty_e_<%=pkSpecimen%>" size=5 type="text" value='<%=specQnty%>' readonly /> &nbsp;<%=spectQuntyUnit%>
	   	   </span>
			<input type="hidden" name="specqnty_<%=childCntr %>" id="specqnty_<%=childCntr %>" value='<%=specQnty%>'>
	     	<span id="specqnty_<%=pkSpecimen%>">
	     		<%=specQnty%>&nbsp;<%=spectQuntyUnit%>
			</span>
	     </td>
	     <td width="12%" align =center>
	     <span id="specproctype_<%=pkSpecimen%>"><%=specProcType%></span>
	     <span id="specproctype_e_<%=pkSpecimen%>" style="display: none;"><%=dChildSpecprocType%></span>     </td>
	
	     <td align =center colspan="10">
	
	     <span id="specloc_<%=pkSpecimen%>"><%=specLocation%></span>
	
	     <span id="specloc_e_<%=pkSpecimen%>" style="display: none;">
	    	<input id = "childlocation_e_text_<%=pkSpecimen%>"  name="childlocation_e_text_<%=pkSpecimen%>" size=15 type="text" readonly value='<%=specLocation %>'/>
	
	    	<A  href="javascript:void(0);" onClick="return openLocLookupChild(document.specimen,<%=pkSpecimen%>)" ><%=LC.L_Select%><%--Select*****--%></A>&nbsp;
	
			<A  href="#"  onClick="return removeLocationChild(document.specimen,<%=pkSpecimen%>)" ><%=LC.L_Remove%><%--Remove*****--%></A>&nbsp;
	
	     </span>
	     <input id = "childlocation_pk_<%=pkSpecimen%>" name="childlocation_pk_<%=pkSpecimen%>" size=10 type="hidden" value='<%=specLocationPK %>'/>
		 <input id = "storageCapacity<%=pkSpecimen%>" name="storageCapacity<%=pkSpecimen%>" size=10 type="hidden" value='<%=storageCapacity %>'/>
		 <input id = "multiSpecimen<%=pkSpecimen%>" name="multiSpecimen<%=pkSpecimen%>" size=10 type="hidden" value='<%=multiSpecimen %>'/>
	     <input id = "capacityUnit<%=pkSpecimen%>" name="capacityUnit<%=pkSpecimen%>" size=10 type="hidden" value='<%=capacityUnit %>'/>
		 <input id = "storageCount<%=pkSpecimen%>" name="storageCount<%=pkSpecimen%>" size=10 type="hidden" value='<%=storageCount %>'/>
		 <input id = "storageChildCount<%=pkSpecimen%>" name="storageChildCount<%=pkSpecimen%>" size=10 type="hidden" value='<%=storageChildCount %>'/>
		 <input id = "codelstItem<%=pkSpecimen%>" name="codelstItem<%=pkSpecimen%>" size=10 type="hidden" value='<%=codelstItem %>'/>
	     </td>
	  </tr>
	
	
	
	
		<%
	
	
		}// end of for loop,  childCntr
		%>
			<input type="hidden" name="statusTyps" value="<%=statusTypStr%>">
		</table>
	</div>
	</div>
		<%
		}//End of Modify mode
		%>
	
	<input type="hidden" name="actavailQty" value="<%=actCurrentQty%>">
	<%
	if ( pageRight > 5 || (pageRight == 5 && !"M".equals(mode))  )
	 { %><br>
	<jsp:include page="submitBar.jsp" flush="true">
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="specimendet"/>
			<jsp:param name="showDiscard" value="N"/>
	</jsp:include>
	
	<%
	 } //if for edit right
	
	} //end Session
	else {%>
	
	<!--JM: 18Sep2007, added, #Sonia feedback-->
	<jsp:include page="timeout.html" flush="true"/>
	
	
	<%}%>
	
	</Form>
	<script>
	setDD(document.specimen);
	</script>
		<div class = "myHomebottomPanel">
		    <jsp:include page="bottompanel.jsp" flush="true"/>
		</div>
	</div>
	</body>	
	</html>
<SCRIPT LANGUAGE="JavaScript">
	if(screenWidth>1280 || screenHeight>1024)
		$j("#div2").attr("style","height:78%; top:124px;");
</SCRIPT>