<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<%@ page import="java.util.*,com.velos.eres.service.util.StringUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.EJBUtil" %><%@page import="com.velos.eres.service.util.LC"%>
<%@ page import="com.velos.eres.web.objectSettings.ObjectSettingsCache, com.velos.eres.web.objectSettings.ObjectSettings, com.velos.eres.service.util.MC"%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="javascript">

function printLabelspecimen(specimenPk)
{
		selPks = specimenPk;
	  windowName =window.open("printMultiLabel.jsp?&selPks="+selPks, "Information", "toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=700,height=450, top=100, left=90");
	  if (windowName != null) { windowName.focus(); }
}
</script>

<%!
  private String getHrefBySubtype(String subtype) {
      if (subtype == null) { return "#"; }
      if ("1".equals(subtype)) {
          return "specimenbrowser.jsp?srcmenu=tdmenubaritem6&selectedTab=1";
      }
      if ("2".equals(subtype)) {
          return "storageadminbrowser.jsp?srcmenu=tdmenubaritem6&selectedTab=2";
      }
      if ("3".equals(subtype)) {
          return "storagekitbrowser.jsp?srcmenu=tdmenubaritem6&selectedTab=3";
      }
      if ("4".equals(subtype)) {
          return "preparationAreaBrowser.jsp?srcmenu=tdmenubaritem6&selectedTab=4";
      }

      return "#";
  }
%>




<%
String mode="M";
String selclass;
String userId="";
String acc="";
String tab= request.getParameter("selectedTab");
String specimenPk = "";
String parentPk="";
String perId = "", stdId = "";
String studyTitle = "";

HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession))
{

 	 int pageRightStorage = 0;
 	 int pageRightSpecimen = 0;

	 GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");

	 pageRightStorage = Integer.parseInt(grpRights.getFtrRightsByValue("MSTORAGE"));
	 pageRightSpecimen = Integer.parseInt(grpRights.getFtrRightsByValue("MSPEC"));

	acc = (String) tSession.getValue("accountId");
	userId = (String) tSession.getValue("userId");

	specimenPk = request.getParameter("specimenPk");
	parentPk=request.getParameter("parentPk");
	//JM: 07Jul2009: #INVP2.11
	perId = request.getParameter("pkey");
 	perId = (perId==null)?"":perId;

	stdId = request.getParameter("studyId");
	stdId = (stdId==null)?"":stdId;

	studyB.setId(EJBUtil.stringToNum(stdId));
    studyB.getStudyDetails();
	studyTitle = studyB.getStudyTitle();

%>
<!-- <table  class= "selectedTab" cellspacing="0" cellpadding="0" border="0">  -->
<table  cellspacing="0" cellpadding="0" border="0">
	<tr>

	<%
			ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
			ArrayList tabList = objCache.getAccountObjects(EJBUtil.stringToNum(acc), "inv_tab");

			for (int iX=0; iX<tabList.size(); iX++) {
            ObjectSettings settings = (ObjectSettings)tabList.get(iX);

    		if ("0".equals(settings.getObjVisible())) {
    		    continue;
    		}

            boolean showThisTab = false;

			if ("1".equals(settings.getObjSubType())) {
    		    if (pageRightSpecimen >= 4 )
    		    {
    		        showThisTab = true;
    		    }

    		}

			else if ("2".equals(settings.getObjSubType())) {
    		    if (pageRightStorage >= 4 )
    		    {
    		        showThisTab = true;
    		    }

    		}else if ("3".equals(settings.getObjSubType())) {
    		    //if (pageRightSpecimen >= 4 )
    		    //{
    		        showThisTab = true;
    		    //}

    		}
    		else if ("4".equals(settings.getObjSubType())) {    		 
				//KM-5788 and 5789
				if (pageRightSpecimen > 4 )
    		    {
    		        showThisTab = true;    	
				}
    		}
			else {
    		    showThisTab = true;
    		}

            if (!showThisTab) { continue; }

			if (tab == null) {
                selclass = "unselectedTab";
    		} else if (tab.equals(settings.getObjSubType())) {
                selclass = "selectedTab";
            } else {
                selclass = "unselectedTab";
            }

		%>
		<td  valign="TOP">
		<table class="<%=selclass%>"  cellspacing="0" cellpadding="0" border="0">
				<tr>
				<!-- <td rowspan="3" valign="top" wclassth="7">
						<img src="../images/leftgreytab.gif" width=8 height=20 border=0 alt="">
					</td> -->
					<td>
						<a href="<%=getHrefBySubtype(settings.getObjSubType())%>"><%=settings.getObjDispTxt()%></a>
					</td>
			     <!--    <td rowspan="3" valign="top" wclassth="7">
						<img src="../images/rightgreytab.gif" wclassth=7 height=20 border=0 alt="">
			        </td> -->
			   	</tr>
		</table>
		 </td>

		<%
        } // End of tabList loop
      %>

	</tr>
 <!--    <tr>
	     <td colspan=4 height=10></td><% // colspan here corresponds to the number of tabs %>
	</tr>  -->
</table>
<table class="tabBorder" width="99%" height="5"><tr><td></td></tr></table>

	<%
	if (tab.equals("1")&& pageRightSpecimen >= 4 && (! StringUtil.isEmpty(specimenPk)) )
	    	{
	    	%>
	<table width="99%" cellspacing="0" cellpadding="0">
		<tr bgcolor="#FFF7DD" align="left" class="speca">
			<td width="10%">
				<A href="specimenbrowser.jsp?srcmenu=tdmenubaritem6&selectedTab=1"><img src="../images/jpg/search_pg.png" title="<%=LC.L_Specimen_Search%><%--Specimen Search*****--%>" border="0"></A>
		
		<!--Add link on "Specimens" Tab to go back to the study summary-->
		<!--Study-->
		<%if(!stdId.equalsIgnoreCase("")){ %>
		    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId=<%=stdId%>"><span onmouseover="return overlib(htmlEncode('<%=studyTitle%>'),CAPTION,'<%=LC.L_Study_Title%>');" onmouseout="return nd();"><img id = "activeImage" class = "unSelectedImage imageDim" src="../images/jpg/study.gif"></span></a>
		<%}else{ %>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#"><span onmouseover="return overlib(htmlEncode('<%=studyTitle%>'),CAPTION,'<%=LC.L_Study_Title%>');" onmouseout="return nd();"><img id = "activeImage" class = "unSelectedImage imageDim" src="../images/jpg/study.gif"></span></a>
        <%} %>
       <!--/Study-->
			</td>
			<%if (!EJBUtil.isEmpty(parentPk)){%>
			<td align="center" width="3%">
				<A href="specimendetails.jsp?mode=M&pkId=<%=parentPk%>"><img src="../images/jpg/formleft.gif" title="<%=MC.M_BackParent_Spmen%>" border="0"></A>
			</td>
			<td width="70%" align="center"><%=MC.M_ThisChild_Spmen%></td>
			<%}%>
			<td align="right">
			
				<A href="#" onClick=" return printLabelspecimen(<%=specimenPk%>);" style="color:#3B9EC6;font-weight:bold;"><%=LC.L_Print_Label%><%--PRINT LABEL*****--%></A>&nbsp;&nbsp;&nbsp;&nbsp;

				<A href="specimendetails.jsp?srcmenu=tdmenubaritem6&selectedTab=1&mode=M&pkId=<%=specimenPk%>&pkey=<%=perId%>&studyId=<%=stdId%>"><img src="images/box.gif" title="<%=LC.L_Spmen_DetOrStat%><%--Specimen Details/ Status*****--%>" border="0"></A>
				&nbsp;&nbsp;&nbsp;&nbsp;
		
				<A href="specimenapndx.jsp?srcmenu=tdmenubaritem6&selectedTab=1&mode=M&pkId=<%=specimenPk%>&pkey=<%=perId%>&studyId=<%=stdId%>"><img src="images/Appendix.gif" title="<%=LC.L_Appendix%><%--Appendix*****--%>" border="0"></A>
		
				<!-- &nbsp;&nbsp;&nbsp;&nbsp;
				<A href="#" ><b>Access Rights</b></A> -->
		
				&nbsp;&nbsp;&nbsp;&nbsp;
				<A href="specimenForms.jsp?srcmenu=tdmenubaritem6&selectedTab=1&mode=M&specimenPk=<%=specimenPk%>&pkey=<%=perId%>&studyId=<%=stdId%>"><img src="images/Form.gif" title="<%=LC.L_Forms%><%--Forms*****--%>" border="0"></A>
		
				&nbsp;&nbsp;&nbsp;&nbsp;
				<A href="specimenlabbrowser.jsp?srcmenu=tdmenubaritem6&selectedTab=1&mode=M&specimenPk=<%=specimenPk%>&calledFromPg=specimen&pkey=<%=perId%>&studyId=<%=stdId%>&page=specimen"><img src="images/Org.png" title="<%=LC.L_Labs%><%--Labs*****--%>" border="0"></A>
			</td>
		</tr>
	</table>



<%
	} //if first tab is selected
 } %>