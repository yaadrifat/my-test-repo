<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_User_Search%><%--User Search*****--%></title>
</head>

	<jsp:include page="skinChoser.jsp" flush="true"/>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT type="text/javascript">


function setOrder(formobj,orderBy)
{

	if (formobj.orderType.value=="asc") {
		formobj.orderType.value= "desc";
	} else 	if (formobj.orderType.value=="desc") {
		formobj.orderType.value= "asc";
	}

	formobj.orderBy.value= orderBy;

	formobj.submit();
}

//JM: 11/09/2005

function checkall(formObj){

var len = 0;
var seluser=0;
//len=formObj.lenUsers.value;
len=formObj.rowsReturned.value;


seluser=formObj.selectedUsers.value;

if (len==1 && seluser==0){
	if (formObj.usercheckall.checked==true){
		formObj.usercheck.checked=true;
	}
	else if (formObj.usercheckall.checked==false){
		formObj.usercheck.checked=false;
	}
}

if (len==1 && seluser>0 ){
	if (formObj.usercheckall.checked==true){
		formObj.usercheck[0].checked=true;

	}
	else if (formObj.usercheckall.checked==false){
		formObj.usercheck[0].checked=false;


	}
}
else if (len>1){
	for (i=0; i<len; i++){
		if (formObj.usercheckall.checked==true){
			formObj.usercheck[i].checked=true;
		}
		else if (formObj.usercheckall.checked==false){
			formObj.usercheck[i].checked=false;
		}
	}
 }
}
</SCRIPT>
<SCRIPT>
function validate(formObj) {
  //var lenUsers = 0;
  var totalRows = 0;

	//lenUsers = parseInt(formObj.lenUsers.value);
	totalRows = parseInt(formObj.totalRows.value);

	var names = "";
	var ids = "";
	if(totalRows > 1) {
		for (var i = 0; i < totalRows; i++) {
			if(formObj.usercheck[i].checked){


				names = names + formObj.username[i].value + ";";
				ids = ids + formObj.userid[i].value + ",";


				}
		}
	}
	if(totalRows==1) {
  		if(formObj.usercheck.checked){

			names = names + formObj.username.value + ";";
			ids = ids + formObj.userid.value + ",";

		}
	}

	names = names.substring(0,names.length-1);
	ids = ids.substring(0,ids.length-1);

	if (formObj.from.value = "formNotify") {
	//set the users in FormNotify.jsp
     	if (document.layers) {
        	window.opener.document.div1.document.formNotify.nameList.value = names;
        	window.opener.document.div1.document.formNotify.userlist.value = ids;
    	} else {
    		window.opener.document.formNotify.nameList.value = names;
    		window.opener.document.formNotify.userlist.value = ids;
    	}
	}

	self.close();
}

</SCRIPT>


<% response.setHeader("Cache-Control", "max-age=1, must-revalidate");%>
<%@ page language = "java" import="com.velos.eres.service.util.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil"%>


<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id="teamB" scope="request" class="com.velos.eres.web.team.TeamJB"/>
<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%
int ienet = 2;

String agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1)
     ienet = 0; //IE
    else
	ienet = 1;
	if(ienet == 0) {
%>
<body style="overflow:scroll;">
<%
	} else {
%>
<body style="overflow:visible;">
<%
	}
%>
<%HttpSession tSession = request.getSession(true);
  if (sessionmaint.isValidSession(tSession))
{
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
	<jsp:include page="include.jsp" flush="true"/>

<%
	String from=request.getParameter("from") ;


	int accId = EJBUtil.stringToNum((String) tSession.getValue("accountId"));

	String userIds = "";
	String userNames = "";

	userIds = request.getParameter("userIds");
	if(userIds!=null){
	if(userIds.equals("null")) {
		userIds = "";
		}
	}
	String uName = (String) tSession.getValue("userName");


	String ufname= request.getParameter("fname") ;

	String ulname=request.getParameter("lname") ;


	String jobTyp=request.getParameter("jobType") ;
	if (jobTyp==null) jobTyp="";

	String jobVal = request.getParameter("jobType");
	if(jobVal==null) jobVal = "";

	String orgName=request.getParameter("accsites") ;

	String siteVal = request.getParameter("accsites");
	if(siteVal == null) siteVal = "";

	if(orgName==null) orgName="";

	String mode = request.getParameter("mode");
	if(mode == null) mode = "";
	int i=0;
    	String usrId = null;
	String usrName= "";

	userIds = ((userIds == null) || (userIds.equals("0")))?"":userIds;

	userNames = request.getParameter("userNames");

	userNames = ((userNames == null) || (userNames.equals("0")))?"":userNames;

	while(userNames.indexOf('~') != -1){
	  	userNames = userNames.replace('~',' ');
	}

	String gName = "";
	gName = request.getParameter("usrgrps");
	gName=(gName==null)?"":gName;

	String stTeam = "";
	stTeam = request.getParameter("ByStudy");
	stTeam=(stTeam==null)?"":stTeam;

	String fname = LC.L_All;/*String fname = "All";*****/
	String lname = LC.L_All;/*String lname = "All";*****/
	String jobtype = "All" ;
	String accsites = "All";
	String usrgrps  = "All";
	String ByStudy = "All";

	String jobDesc = LC.L_All;/*String jobDesc = "All";*****/
	String siteDesc = LC.L_All;/*String siteDesc = "All";*****/
	String gDesc = LC.L_All ;/*String gDesc = "All" ;*****/
	String tDesc = LC.L_All;/*String tDesc = "All";*****/

	fname = request.getParameter("fname");
	fname=(fname==null)?"All":fname;
	lname = request.getParameter("lname");
	lname=(lname==null)?"All":lname;
	jobtype = request.getParameter("jobType");
	accsites = request.getParameter("accsites");



	String userJobType = "";
	String dJobType = "";
	String dAccSites ="";
	String orgNam = "";
	UserDao userDao = new UserDao();
	CodeDao cd = new CodeDao();
	CodeDao cd2 = new CodeDao();
	cd2.getAccountSites(accId);
   	cd.getCodeValues("job_type");
	userJobType =userB.getUserCodelstJobtype();

	if (jobVal.equals(""))
		dJobType=cd.toPullDown("jobType");

	else
	 	dJobType=cd.toPullDown("jobType",EJBUtil.stringToNum(jobVal),true);

	String userSiteId = "";
	int primSiteId = 0;

		userB=(com.velos.eres.web.user.UserJB)tSession.getAttribute("currentUser");
		userSiteId = userB.getUserSiteId();
		userSiteId=(userSiteId==null)?"":userSiteId;
		primSiteId= EJBUtil.stringToNum(userSiteId);



	if ((siteVal.length()==0) && (mode.equals("initial"))){
		dAccSites=cd2.toPullDown("accsites",primSiteId,true);
		orgName=userSiteId;

		mode="M";
	}
	else
		dAccSites=cd2.toPullDown("accsites",EJBUtil.stringToNum(siteVal),true);




	jobDesc = cd.getCodeDesc(EJBUtil.stringToNum(jobtype));


	if (accsites==null)
	siteDesc = cd2.getCodeDesc(primSiteId);
	else{
	siteDesc = cd2.getCodeDesc(EJBUtil.stringToNum(accsites));
	}

	//JM: 11/09/2005: to make drop down of group
	    String dUsrGrps="";
	    String selGroup="";

	    CodeDao cd1 = new CodeDao();
	    cd1.getAccountGroups(accId);

	    selGroup=request.getParameter("usrgrps");
	    if (selGroup==null)  selGroup="";
	    if (selGroup.equals(""))
	    dUsrGrps = cd1.toPullDown("usrgrps");
	    else
	    dUsrGrps = cd1.toPullDown("usrgrps",EJBUtil.stringToNum(selGroup),true);

	    usrgrps =request.getParameter("usrgrps");
	    gDesc=cd1.getCodeDesc(EJBUtil.stringToNum(usrgrps));

		String dStudy="";
		String selstudy="";
		int selstudyId=0;
	  	selstudy=request.getParameter("ByStudy") ;
		if(selstudy==null ){
			selstudyId=0 ;
		}else {
			selstudyId = EJBUtil.stringToNum(selstudy);
		}

		StudyDao studyDao = new StudyDao();
		String suserId = (String) tSession.getValue("userId");
		studyDao.getStudyValuesForUsers(suserId);
		ArrayList studyIds = studyDao.getStudyIds();
		ArrayList studyNumbers = studyDao.getStudyNumbers();

		//make dropdowns of study
		dStudy=EJBUtil.createPullDown("ByStudy",selstudyId,studyIds,studyNumbers);

		ByStudy =request.getParameter("ByStudy");
		//JM: get the study number from the selected study id
		tDesc=studyDao.getStudy(EJBUtil.stringToNum(ByStudy));

		String orderBy = "";
		orderBy = request.getParameter("orderBy");


		if (EJBUtil.isEmpty(orderBy)){
		orderBy = " lower(u.USR_FIRSTNAME || ' ' || u.usr_lastname) ";
		}

		String orderType = "";
		orderType = request.getParameter("orderType");

		if (EJBUtil.isEmpty(orderType))
		{
		orderType = "asc";
		}
%>

<BR>
<P class="defComments">&nbsp;&nbsp;<%=LC.L_Search_Criteria%><%--Search Criteria*****--%></P>
<br>
<Form  name="search1" method="post" action="selectremoveusers.jsp">
  <input type="hidden" name="from" value="<%=from%>">
  <input type="hidden" name="userIds" value="<%=userIds%>">
  <input type="hidden" name="userNames"  value="<%=userNames%>">

  <Input type="hidden" name="selectedTab" value="1">
  <Input type="hidden" name="mode" value="<%=mode%>">






  <table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl" >
    <tr>
      <td class="subheadings"  width=15%> <%=LC.L_User_Search%><%--User Search*****--%></td>
      <td  width=15%> <%=LC.L_User_FirstName%><%--User First Name*****--%>:
      <%
      if(fname.equals("All")){
      %>
		<Input type=text name="fname" size=20>
	<%}else{%>
	   <!-- km -to fix Bug# 2308 -->
        	<Input type=text name="fname" size=20 value="<%=fname%>">
        <%}%>

      </td>
      <td  width=15%> <%=LC.L_User_Last_Name%><%--User Last Name*****--%>:
      <% if (lname.equals("All")){%>
      	  	<Input type=text name="lname" size=20>
	<%}else{%>
		<Input type=text name="lname" size=20 value="<%=lname%>">
	<%}%>
      </td>
	  <td width=20%> <%=LC.L_Job_Type%><%--Job Type*****--%>:
		 <%=dJobType%>
	  </td>
	  <td  width=20%> <%=LC.L_Organization_Name%><%--Organization Name*****--%>:

		 <%=dAccSites%>
	  </td>



      <!--td class=tdDefault valign="bottom"> text

	 <input type="image" src="../images/jpg/go_white.gif" align="bottom" border="0">
      </td-->

    </tr>

    <tr>
    <td width=15%>&nbsp;</td>
    <td  width=20%> <%=LC.L_Group_Name%><%--Group Name*****--%>:         <%=dUsrGrps %> </td>
    <td width=20%> <%=LC.L_Std_Team%><%--Study Team*****--%>:         <%=dStudy%> </td>
    <td >
     	 <button type="submit"><%=LC.L_Search%></button>
    </td></tr>
  </table>
<DIV >
<input type=hidden name="userIds" value="">
</Form>

  <%

	if(fname==null||fname.equals(""))
		fname = LC.L_All;/*fname = "All";****/
	if(lname==null||lname.equals(""))
		lname = LC.L_All;/*lname = "All";*****/
	if(jobDesc==null || jobDesc.equals(""))
		jobDesc = LC.L_All;/*jobDesc = "All";*****/
	if(siteDesc==null || siteDesc.equals(""))
		siteDesc = LC.L_All;/*siteDesc = "All";*****/
	if(gDesc==null || gDesc.equals(""))
		gDesc = LC.L_All;/*gDesc = "All";*****/
	if(tDesc==null || tDesc.equals(""))
		tDesc = LC.L_All;/*tDesc = "All";*****/




		String pagenum = "";
		int curPage = 0;
		long startPage = 1;
		String stPage;
		long cntr = 0;
		pagenum = request.getParameter("page");


		if (pagenum == null)
		{
			pagenum = "1";
		}
		curPage = EJBUtil.stringToNum(pagenum);


		String formSql = "";
		String countSql = "";
		StringBuffer completeSelect = new StringBuffer();
		String lWhere = null;
        String fWhere = null;
        String oWhere = null;
        String rWhere = null;
        String gWhere = null;
        String sWhere = null;
        String otWhere = null;

		if (from.equals("formNotify") ){
		  // get users from account
  		  //userDao.searchAccountUsers(accId,ulname,ufname,jobTyp,orgName,selGroup, selstudy,userIds);
	//}
			//KM-03/04/08
			String selectSt =
	  		   " SELECT distinct u.pk_user,u.USR_LASTNAME, "
                + "(select SITE_NAME from er_site where PK_SITE  = FK_SITEID) as site_name, "
                + " u.USR_FIRSTNAME, u.USR_MIDNAME,u.USR_STAT,u.USR_TYPE "
                + " from  ER_USER u   "
                + " Where u.USR_STAT in ('A','B') and u.USR_TYPE not in  ('X','P') and USER_HIDDEN <> 1 and u.fk_account = "+accId;


             if ((orgName!= null) && (! orgName.trim().equals("") && ! orgName.trim().equals("null"))){
                oWhere = " and UPPER(u.FK_SITEID) = UPPER(";
                oWhere += orgName;
                oWhere += ")";
            }

            if ((jobTyp != null) && (! jobTyp.trim().equals("") && ! jobTyp.trim().equals("null"))){
                rWhere = " and UPPER(u.FK_CODELST_JOBTYPE) = UPPER(";
                rWhere += jobTyp;
                rWhere += ")";
            }

            if ((ufname != null) && (! ufname.trim().equals("") && ! ufname.trim().equals("null"))){
                fWhere = " and UPPER(u.USR_FIRSTNAME) like UPPER('";
                fWhere += ufname.trim();
                fWhere += "%')";
            }

            if ((ulname != null) && (! ulname.trim().equals("") && ! ulname.trim().equals("null"))) {
                lWhere = " and UPPER(u.USR_LASTNAME) like UPPER('";
                lWhere += ulname.trim();
                lWhere += "%')";
            }


	    //km-All the User's belonging to the selected group should be retreived, default or not doesn't matter. (Bug:2306)
	    if ((gName != null) && (!gName.trim().equals("") && !gName.trim().equals("null")))
			{
				//gWhere =" and UPPER(u.fk_grp_default) = upper(";
				gWhere=" and u.pk_user in(select fk_user from er_usrgrp where upper(fk_grp)=upper(";
				gWhere +=gName;
				gWhere +="))";
			}

			if ((stTeam != null) && (!stTeam.trim().equals("") && !stTeam.trim().equals("null")))
			{
				sWhere =" and ( exists (select * from er_studyteam t where t.fk_user = pk_user and t.fk_study =  ";
				sWhere +=stTeam;
				sWhere +=" and nvl(t.study_team_usr_type,'D')='D' ) or pkg_superuser.F_Is_Superuser(pk_user, "+stTeam+") = 1  )";

			}

            completeSelect.append(selectSt);


           	if((userIds != null) && (! userIds.trim().equals("") )&& (! ulname.trim().equals("null") ))
           	{
            completeSelect.append(" and u.pk_user not in (" + userIds + ")");
            }

            if (fWhere != null) {
                completeSelect.append(fWhere);
            }

            if (lWhere != null) {
                completeSelect.append(lWhere);
            }

            if (oWhere != null) {
                completeSelect.append(oWhere);
            }

            if (rWhere != null) {
                completeSelect.append(rWhere);
            }
            if (gWhere != null) {
                completeSelect.append(gWhere);
            }
            if (sWhere != null) {
                completeSelect.append(sWhere);
            }


            formSql = completeSelect.toString();
            countSql = "Select count(*) from ( " + formSql + ")";

            //out.println("countSql------->>>"+countSql);

       }
			long rowsPerPage=0;
			long totalPages=0;
			long rowsReturned = 0;
			long showPages = 0;

			boolean hasMore = false;
			boolean hasPrevious = false;
			long firstRec = 0;
			long lastRec = 0;
			long totalRows = 0;

			String type ="" ;
			String type1="";

			rowsPerPage =  Configuration.MOREBROWSERROWS ;
			totalPages =Configuration.PAGEPERBROWSER ;



			BrowserRows br = new BrowserRows();
			br.getPageRows(curPage,rowsPerPage,formSql ,totalPages,countSql,orderBy,orderType);
			rowsReturned = br.getRowReturned();
			showPages = br.getShowPages();
			startPage = br.getStartPage();
			hasMore = br.getHasMore();
			hasPrevious = br.getHasPrevious();
			totalRows = br.getTotalRows();
			firstRec = br.getFirstRec();
			lastRec = br.getLastRec();


			String usrLastName = "";
			String usrFirstName = "";
			String usrMidName = "";
			String siteName = "";
			String userSiteName = "";
			String jobType = "";
			String usrStat = "";
			String usrType = "";


			int counter = 0;




%>



<P class="defComments"><%=MC.M_SelFilters_UsrFirstName%><%--The Selected Filters are: User First Name*****--%>: <B><I><u><%=fname%></u></I></B>&nbsp;&nbsp; <%=LC.L_User_Last_Name%><%--User Last Name*****--%>: <B><I><u><%=lname%></u></I></B>&nbsp;&nbsp; <%=LC.L_Job_Type%><%--Job Type*****--%>: <B><I><u><%=jobDesc%></u></I></B>&nbsp;&nbsp; <%=LC.L_Organization_Name%><%--Organization Name*****--%>: <B><I><u><%=siteDesc%></u></I></B>&nbsp;&nbsp; <%=LC.L_Group_Name%><%--Group Name*****--%>:<B><I><u><%=gDesc%></u></I></B>&nbsp;&nbsp; <%=LC.L_Std_Team%><%--Study Team*****--%>:<B><I><u><%=tDesc%></u></I></B></p>



  <Form name="userResult" id="usrResultFrm" method=post action="" onsubmit="if (validate(document.userResult)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

  <!--input type="hidden" name=userIds value=""-->
  <input type="hidden" name="from" value=<%=from%>>
  <input type="hidden" name=userNames  value=<%=userNames%>>

  <input type="hidden" name=orderBy value=<%=orderBy%>>
  <input type="hidden" name=orderType  value=<%=orderType%>>
  <input type="hidden" name=userIds value="<%=userIds%>">
  <input type="hidden" name="rowsReturned" value=<%=rowsReturned%>>
  <Input type="hidden" name="page" value="<%=pagenum%>">
  <Input type="hidden" name="mode" value="M">



  <input type="hidden" name=usrgrps  value=<%=gName%>>
  <input type="hidden" name="fname" value="<%=ufname%>">
  <Input type="hidden" name="lname" value="<%=ulname%>">
  <input type="hidden" name="jobType" value=<%=jobtype%>>
  <input type="hidden" name="accsites" value=<%=orgName%>>
  <input type="hidden" name="ByStudy" value=<%=stTeam%>>




    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="basetbl midalign">


    <%

	StringTokenizer namesST = new StringTokenizer(userNames,";");
	StringTokenizer idsST = new StringTokenizer(userIds,",");
	int selectedUsers = 0;
	selectedUsers = namesST.countTokens();

	%>
<input type="hidden" name="selectedUsers" value=<%=selectedUsers%>>
	<tr>
	  <% if (rowsReturned > 0){ %>


	  	<th width=15% >

		<INPUT TYPE="checkbox" NAME="usercheckall" onClick="checkall(document.userResult) ;">
		<B>&nbsp;&nbsp;<%=LC.L_Select_All%><%--Select All*****--%></B></th>

		<th width=25% onClick="setOrder(document.userResult,'lower(USR_FIRSTNAME)')" ><B><%=LC.L_First_Name%><%--First Name*****--%></B>
		</th>
		<th width=25%
		onClick="setOrder(document.userResult,'lower(USR_LASTNAME)')" ><B><%=LC.L_Last_Name%><%--Last Name*****--%></B>
		</th>
		<th onClick="setOrder(document.userResult,'lower(site_name)')" ><B><%=LC.L_Organization%><%--Organization*****--%></B>
		</th>
		<th onClick="setOrder(document.userResult,'lower(USR_STAT || USR_TYPE)')" ><B><%=LC.L_User_Type%><%--User Type*****--%></B>
		</th>



	<%}else{%>

	        <th width=35% align =center><P class=defcomments> <%=MC.M_NoSrchResMatch_Criteria%><%--No search results matching given criteria found*****--%></P></th>



       </tr>


<%

}

		int count = 0;



		for(count  = 1 ; count  <= rowsReturned ; count++)

	  	{


			usrLastName=br.getBValues(count,"USR_LASTNAME");
			usrLastName=(usrLastName==null)?"":usrLastName;

			usrFirstName=br.getBValues(count,"USR_FIRSTNAME");
			usrFirstName=(usrFirstName==null)?"":usrFirstName;

			orgNam=br.getBValues(count,"site_name");
			orgNam=(orgNam==null)?"":orgNam;

			usrId=br.getBValues(count,"pk_user");
			usrId=(usrId==null)?"":usrId;

			usrStat=br.getBValues(count,"USR_STAT");
			usrStat=(usrStat==null)?"":usrStat;

			usrType=br.getBValues(count,"USR_TYPE");
			usrType=(usrType==null)?"":usrType;


			if (usrStat.equals("A")){
			usrStat=LC.L_Active_AccUser;/*usrStat="Active Account User";*****/
			}
			else if (usrStat.equals("B")){
			usrStat=LC.L_Blocked_User;/*usrStat="Blocked User";*****/
			}
			else if (usrStat.equals("D")){
			usrStat=LC.L_Deactivated_User;/*usrStat="Deactivated User";*****/
			}


			if (usrType.equals("N")){
			usrStat=MC.M_Non_SystemUser;/*usrStat="Non System User";*****/
			}


			if ((count%2)==0) {
	%>
      <tr class="browserEvenRow">
        <%
			}else{
		  %>
      <tr class="browserOddRow">
        <% }

	usrName = usrFirstName + " " +usrLastName;
		   %>

	<td><input type=checkbox name='usercheck'>
        <td width =200> <%=usrFirstName%> </td>
	<td width =200> <%=usrLastName%> </td>
	<td width =200> <%=orgNam%></td>
	<td width=200> <%=usrStat%></td>
<input type=hidden name='username' value='<%=usrFirstName.trim()%><%=usrLastName.trim()%>'>
		<input type=hidden name='userid' value=<%=usrId%>>

      </tr>
      <%
	  	}
	  	rowsReturned = rowsReturned + selectedUsers;
	  %>
	<input type=hidden name=totalRows value=<%=rowsReturned%>>
<tr>

<%

		if(selectedUsers > 0) {
%>



		<td >

			<p class = "sectionHeadings" ><%=MC.M_Already_SelectedUsers%><%--Already Selected Users*****--%></p>

		</td>
		<td >

			<p class = "sectionHeadings" ><%=LC.L_First_Name%><%--First Name*****--%></p>

		</td>
		<td >

			<p class = "sectionHeadings" ><%=LC.L_Last_Name%><%--Last Name*****--%></p>

		</td>
		<td >

			<p class = "sectionHeadings" ><%=LC.L_Organization%><%--Organization*****--%></p>
		</td>
		<td >

			<p class = "sectionHeadings" ><%=LC.L_User_Type%><%--User Type*****--%></p>
		</td>
<%
		}
%>
</tr>

	  <%


		for(i = 0 ; i < selectedUsers ; i++)
	  	{
			usrName = namesST.nextToken();
			usrId = idsST.nextToken();

			userDao = userB.getUsersDetails(usrId);
			ArrayList organizationNames = userDao.getUsrSiteNames();
			ArrayList uFname = userDao.getUsrFirstNames();
			ArrayList uLname = userDao.getUsrLastNames();

			ArrayList uStatuses1 = userDao.getUsrStats();
			ArrayList uTypes1 = userDao.getUsrTypes();

			orgNam = ((organizationNames.get(0))==null)?"-":(organizationNames.get(0)).toString();
			String usrFname = ((uFname.get(0)) == null) ? "-":(uFname.get(0)).toString();
			String usrLname = ((uLname.get(0)) == null) ? "-":(uLname.get(0)).toString();


			usrStat = ((uStatuses1 .get(0))==null)?"-":(uStatuses1 .get(0)).toString();

			if (usrStat.equals("A")){
			usrStat=LC.L_Active_AccUser;/*usrStat="Active Account User";*****/
			}
			else if (usrStat.equals("B")){
			usrStat=LC.L_Blocked_User;/*usrStat="Blocked User";*****/
			}
			else if (usrStat.equals("D")){
			usrStat=LC.L_Deactivated_User;/*usrStat="Deactivated User";*****/
			}

			usrType = ((uTypes1.get(0))==null)? "-":(uTypes1.get(0)).toString();

			if (usrType.equals("N")){
			usrStat=MC.M_Non_SystemUser;/*usrStat="Non System User";*****/
			}


			if ((i%2)==0) {
	%>
      <tr class="browserEvenRow">
        <%
			}else{ %>
      <tr class="browserOddRow">
        <%


	}
	%>
		<td><input type=checkbox name='usercheck' checked>
	    <td width =200> <%=usrFname %> </td>
		<td width =200> <%=usrLname %> </td>
		<td width =200> <%=orgNam%> </td>
		<td width=200> <%=usrStat%></td>


		<input type=hidden name='username' value='<%=usrName%>'>
		<input type=hidden name='userid' value="<%=usrId%>">
	  </tr>



<% }
	if (totalRows==0) firstRec=0;
	if (rowsReturned > 0)
	{
	%>
<jsp:include page="submitBar.jsp" flush="true">
			<jsp:param name="displayESign" value="N"/>
			<jsp:param name="formID" value="usrResultFrm"/>
			<jsp:param name="showDiscard" value="N"/>
	</jsp:include>	</table>
<%if (totalRows!=0){ Object[] arguments = {firstRec,lastRec,totalRows}; %>
	<table>
		<tr><td>
		<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%--<font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
		</td></tr>
	</table>
	<%
	}

	}
	%>


	<table align=center>
	<tr>
	<%

		if (curPage==1) startPage=1;
	    for ( count = 1; count <= showPages;count++)
		{


   			cntr = (startPage - 1) + count;
	 		if ((count == 1) && (hasPrevious))
			{
			  %>
				<td colspan = 2>
			  	<A href="selectremoveusers.jsp?userNames=<%=userNames%>&userIds=<%=userIds%>&from=<%=from%>&accountId=<%=accId%>&fname=<%=ufname%>&lname=<%=ulname%>&jobType=<%=jobTyp%>&accsites=<%=orgName%>&usrgrps=<%=gName%>&ByStudy=<%=stTeam%>&page=<%=cntr-1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&mode=\"M\"">< <%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%> > </A>
				&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
				<%

  			}	%>
		<td>
		<%
 		 if (curPage  == cntr)
		 {
	    	 %>
				<FONT class = "pageNumber"><%= cntr %></Font>
       		<%
       	}else
        {	%>
		<A href="selectremoveusers.jsp?userNames=<%=userNames%>&userIds=<%=userIds%>&from=<%=from%>&accountId=<%=accId%>&fname=<%=ufname%>&lname=<%=ulname%>&jobType=<%=jobTyp%>&accsites=<%=orgName%>&usrgrps=<%=gName%>&ByStudy=<%=stTeam%>&page=<%=cntr%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&mode=\"M\""><%= cntr%></A>
       <%}%>
		</td>
		<%	}
		if (hasMore)
		{   %>
	   <td colspan = 3 align = center>&nbsp;&nbsp;&nbsp;&nbsp;
	   <A href="selectremoveusers.jsp?userNames=<%=userNames%>&userIds=<%=userIds%>&from=<%=from%>&accountId=<%=accId%>&fname=<%=ufname%>&lname=<%=ulname%>&jobType=<%=jobTyp%>&accsites=<%=orgName%>&usrgrps=<%=gName%>&ByStudy=<%=stTeam%>&page=<%=cntr+1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&mode=\"M\"">< <%=LC.L_Next%><%--Next*****--%> <%=totalPages%>></A>

		</td>
		<%	}	%>

	   </tr>
	</table>
  </Form>
  <%//}//end of Parameter check

%>

<%

}//end of if body for session
else{
%>
  <jsp:include page="timeout_childwindow.jsp" flush="true"/>
  <%
}

%>

</div>
<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</body>
</html>
