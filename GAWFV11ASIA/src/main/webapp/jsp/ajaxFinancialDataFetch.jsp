<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB"/><!--km-->
<jsp:useBean id="groupB" scope="page" class="com.velos.eres.web.group.GroupJB"/><!--km--> 
<%@ page import="com.velos.eres.business.common.*,java.sql.*,com.velos.eres.service.util.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,com.velos.impex.*,java.text.*" %>
<%@ page language = "java" import = "com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.EJBUtil,java.util.*,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration,java.sql.*,com.velos.eres.business.common.*,com.velos.eres.service.util.StringUtil"%>
<%@ page language = "java" import = "java.util.*,com.velos.eres.business.common.*"%>

<title><%=LC.L_Start_Export%><%--Start Export*****--%></title>

<head>

<SCRIPT LANGUAGE="JavaScript">
	function viewInvoice(inv) 
	{
      windowName= window.open("viewInvoice.jsp?invPk=" + inv,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=530,left=125,top=200")
      windowName.focus();      
	
	}
</SCRIPT>
</head>

<body>
<%
	//Declarations
	String src="tdMenuBarItem3";
	String studySql ="";
	String countSql;

	long rowsPerPage=0;
	long totalPages=0;	
	long rowsReturned = 0;
	long showPages = 0;
	long totalRows = 0;	   
	long firstRec = 0;
	long lastRec = 0;	   
	int openCount = 0;
	int resolvedCount = 0;
	int reopenCount = 0;
	int closedCount = 0;
	int enrolled = 0;

	String studyNumber = null;
	String studyTitle = null;
	String studyStatus = null;
	String studyActualDt = null;
	String studyIRB = null;
	String studyIRB2 = null;
	String studySite = null;
	String studyPI = null;

	int studyId=0;
	boolean hasMore = false;
	boolean hasPrevious = false;
	CodeDao  cd = new CodeDao();
	String codeLst= cd.getValForSpecificSubType();
	HttpSession tSession = request.getSession(true); 
	String userId= (String) tSession.getValue("userId");
	
	String pagenum = "";
	int curPage = 0;
	String orderBy = "";
	orderBy = request.getParameter("orderBy");

	String orderType = "";
	orderType = request.getParameter("orderType");

	BrowserRows pr;
	BrowserRows br;
	
	long startPage = 1;
	long cntr = 0;
	String pagenumView = request.getParameter("pageView");
	if (pagenumView == null) {pagenumView = "1";}
	int curPageView = EJBUtil.stringToNum(pagenumView);
	
	long startPageView = 1;
	long cntrView = 0;
	String groupName;
	String rightSeq;
	
	String orderByView = "";
	orderByView = request.getParameter("orderByView");

	String orderTypeView = "";
	orderTypeView = request.getParameter("orderTypeView");
	
	int usrId;
	String dProtocolStatus = "" ;
	CodeDao cd1 = new CodeDao();
	int pageRight;
	GrpRightsJB grpRights;
	ArrayList aRightSeq ;
	int iRightSeq ;
	
	int nodeNo = EJBUtil.stringToNum(request.getParameter("nodeNo"));
	int snodeNo = EJBUtil.stringToNum(request.getParameter("snodeNo"));
	String accountId= (String) tSession.getValue("accountId");
%>
<%
	cd1.getCodeValues("studystat"); 
	dProtocolStatus = cd1.toPullDown("protocolStatus");
	startPage = 1;
	cntr = 0;
	pagenumView = "";
	curPageView = 0;
	startPageView = 1;
	cntrView = 0;

	pagenum = request.getParameter("page");
	if (pagenum == null) {pagenum = "1";}
	curPage = EJBUtil.stringToNum(pagenum);

	orderBy = "";
	orderBy = request.getParameter("orderBy");

	if (EJBUtil.isEmpty(orderBy)) {orderBy = "4";}
	orderType = "";
	orderType = request.getParameter("orderType");

	if (orderType == null)	{orderType = "asc";}
	pagenumView = request.getParameter("pageView");
	if (pagenumView == null) {pagenumView = "1";}
	curPageView = EJBUtil.stringToNum(pagenumView);

	orderByView = "";
	orderByView = request.getParameter("orderByView");
	if (EJBUtil.isEmpty(orderByView)) { orderByView = "4";}

	orderTypeView = "";
	orderTypeView = request.getParameter("orderTypeView");
	if (orderTypeView == null) {orderTypeView = "asc";}
	
	if (sessionmaint.isValidSession(tSession)) {
		userId = (String) tSession.getValue("userId");
	
		//Added by Manimaran to give access right for default admin group to the delete link 
		usrId = EJBUtil.stringToNum(userId);
		userB.setUserId(usrId);
		userB.getUserDetails();
		String defaultGrp=userB.getUserGrpDefault();
		groupB.setGroupId(EJBUtil.stringToNum(defaultGrp));
		groupB.getGroupDetails();
		groupName = groupB.getGroupName();
	
		pageRight = 0;
		grpRights = (GrpRightsJB) 	tSession.getValue("GRights");		
		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));

		acmod.getControlValues("study_rights","STUDYMPAT");
		aRightSeq = acmod.getCSeq();
		rightSeq = aRightSeq.get(0).toString();
		iRightSeq = EJBUtil.stringToNum(rightSeq);
	
		if (pageRight > 0 )	{
			cd = new CodeDao();
			codeLst = cd.getValForSpecificSubType();
						
			/*nodeNo = (nodeNo==null)?"":nodeNo;
			snodeNo = (snodeNo==null)?"":snodeNo;*/
			
			switch (nodeNo){
			case 0:
				//Financial Summary
				studySql = "SELECT TO_CHAR(TO_DATE(actual_schdate),PKG_DATEUTIL.f_get_dateformat) AS actual_schdate, COUNT(a.pk_milestone) as cnt,SUM(a.milestone_amount) as total, pk_study, study_number "
				+ "FROM erv_milestones_4forecast a, sch_events1 y, ER_STUDY z "
				+ "WHERE pk_study = a.FK_STUDY "
				+ "AND (EXISTS(select * from er_studyteam bb where pk_study = bb.fk_study and bb.fk_user = "+userId+" and nvl(studyteam_status,'Active') = 'Active')"
				+ "OR Pkg_Phi.F_Is_Superuser("+ userId +", a.fk_study) = 1) "
				+ "AND (visit IN(SELECT milestone_visit FROM ER_MILESTONE x WHERE a.pk_milestone = x.pk_milestone) OR (a.fk_eventassoc = fk_assoc)) "
				+ "AND a.fk_cal = TO_NUMBER(session_id) "
				//+ "AND a.fk_site IN(SELECT xx.fk_site FROM ER_USERSITE xx WHERE xx.fk_user = "+ userId +" ) "
				+ "AND (study_prinv IN ( SELECT pk_user FROM ER_USER WHERE fk_account=("+ accountId +")) OR study_prinv IS NULL) "
				+ "AND TO_DATE(actual_schdate) BETWEEN TO_DATE(SYSDATE+1) AND TO_DATE(SYSDATE+7) "
				+ "GROUP BY actual_schdate, pk_study, study_number";
				break;
			case 1:
				//Budgets
				//By Modification Date and By Status
				String budgetSql ="select pk_study, nvl(b.STUDY_NUMBER,'-') study_number, a.PK_BUDGET pk_budget, a.budget_name budget_name, a.budget_version budget_version, "
				+ "(SELECT usr_firstname || ' ' || usr_lastName FROM er_user WHERE pk_user = budget_creator) AS created_by,  "
				+ "(CASE WHEN  budget_status ='W' THEN 'Work In Progress' WHEN budget_status ='F' THEN 'Freeze' WHEN budget_status ='T' THEN 'Budget Template' END) AS budget_status, "
				+ "CASE "
				+ "	WHEN (a.created_on IS NOT NULL AND a.last_modified_date IS NOT NULL) AND TRUNC(a.created_on) <= TRUNC(a.last_modified_date)THEN TRUNC(a.last_modified_date)  "
				+ "	WHEN (a.created_on IS NOT NULL AND a.last_modified_date IS NOT NULL) AND TRUNC(a.last_modified_date) < TRUNC(a.created_on) THEN TRUNC(a.created_on) "
				+ "	WHEN (a.created_on IS NULL AND a.last_modified_date IS NOT NULL) THEN TRUNC(a.last_modified_date) "
				//+ "	WHEN (a.created_on IS NOT NULL AND a.last_modified_date IS NULL) THEN TRUNC(a.created_on)  "
				+ "END AS moddate, "
				+ "CASE "
				+ "	WHEN (budget_type ='P')THEN 'Patient' "
				+ "	WHEN (budget_type ='S')THEN 'Study' "
				+ "	WHEN (budget_type ='C')THEN 'Comparative' "
				+ "END AS budget_type, "
				+ "case when length(b.study_title)>100 then substr(b.study_title,1,100)||'... ' else b.study_title end study_title, "
	   	   		+ "c.SITE_NAME "
	   	   		+ "from sch_budget a, er_study b, er_site c " 
				+ "where ((a.pk_budget in (select d.fk_budget from sch_bgtusers d " 
				+ "Where d.fk_user = " + userId+ " ))  OR " 
				+" (SELECT COUNT(*) FROM ER_GRPS WHERE pk_grp="+ defaultGrp  
				+" AND (grp_supbud_flag=1  AND (grp_supbud_rights IS NOT NULL OR to_number(grp_supbud_rights)>0)))>0 " 
				+"  )  AND a.fk_account="+ accountId +"	and " 
				+ "a.FK_SITE=c.PK_SITE(+) ";
					
				budgetSql  = budgetSql + " and  budget_calendar is null ";
				budgetSql  = budgetSql + " and  a.FK_STUDY=b.PK_STUDY(+) ";							
				budgetSql  = budgetSql + " and  nvl(a.budget_delflag,'Z') <> 'Y' ";
			
				studySql="SELECT DISTINCT pk_study, study_number, pk_budget, budget_name, budget_type, budget_version, created_by, budget_status, moddate, "
				+ "CASE "
				+ "WHEN TO_DATE(moddate) = TRUNC(SYSDATE)  THEN 1 "
				+ "WHEN (TO_DATE(moddate) < TRUNC(SYSDATE) AND TO_DATE(moddate) >= TRUNC(SYSDATE) -7) THEN 2 "
				+ "WHEN (TO_DATE(moddate) < TRUNC(SYSDATE) -7 AND TO_DATE(moddate) >= TRUNC(SYSDATE) -15) THEN 3 "
				+ "WHEN (TO_DATE(moddate) < TRUNC(SYSDATE) -15 AND TO_DATE(moddate) >= TRUNC(SYSDATE) -30) THEN 4 "
				+ "END AS lm_When "
				+ "FROM ( " 
				+ " select * from ("+budgetSql+" AND "
				+ " ( (((SELECT (settings_value) FROM ER_SETTINGS WHERE settings_modname=4 AND settings_modnum=" + defaultGrp  
				+ " AND settings_keyword = 'BUD_TAREA') LIKE '%'||TO_CHAR(b.fk_codelst_tarea)||'%') AND (fk_codelst_tarea IS NOT NULL)) "
				+ " OR	 (((SELECT (settings_value) FROM ER_SETTINGS WHERE settings_modname=4 AND settings_modnum=" + defaultGrp 
				+ " AND settings_keyword = 'BUD_DIVISION') LIKE '%'||TO_CHAR(b.study_division)||'%') AND (b.study_division IS NOT NULL)) "
				+ " OR "  
				+ " (1=(SELECT  Pkg_Util.f_compare('SELECT (settings_value) FROM ER_SETTINGS WHERE settings_modname=4 AND settings_modnum="+defaultGrp 
				+ " AND settings_keyword = ''BUD_DISSITE''', b.study_disease_site,',',',','SQL','') FROM dual) AND (study_disease_site IS NOT NULL) ) "
				+ " OR (0=(SELECT COUNT(*) FROM ER_SETTINGS WHERE settings_modname=4 AND settings_modnum="+defaultGrp
				+" AND settings_keyword IN ( 'BUD_TAREA','BUD_DISSITE','BUD_DIVISION')) ) " 
				+ " OR ((a.pk_budget in (select d.fk_budget from sch_bgtusers d " 
				+ " Where d.fk_user = " + userId+ " ))))))";
				switch(snodeNo){
				case 0:
					//Applicable only for dashboard Budgets By Last Modified Date 
					studySql = studySql 
					+ " WHERE (TRUNC(moddate) > TRUNC(SYSDATE) -30 OR TRUNC(moddate) > TRUNC(SYSDATE)-30) ";
					break;
				}
				studySql = studySql +" ORDER BY lm_when";
				break;
			case 2:
				//Milestones
				switch(snodeNo){
				case 0:
					//Achievements
					studySql = "SELECT DISTINCT pk_study, a.study_number, case when length(study_title)>100 then substr(study_title,1,100)||'... ' else study_title end study_title, "
					+ "CASE "
					+ "	 WHEN SUBSTR(TO_CHAR(ach_date, 'mm/dd/yyyy'),7,10) = SUBSTR(TO_CHAR(SYSDATE, 'mm/dd/yyyy'),7,10) THEN SUBSTR(TO_CHAR(SYSDATE, 'mm/dd/yyyy'),7,10) "
					+ "	 WHEN SUBSTR(TO_CHAR(ach_date, 'mm/dd/yyyy'),7,10) = SUBSTR(TO_CHAR(SYSDATE-365, 'mm/dd/yyyy'),7,10) THEN SUBSTR(TO_CHAR(SYSDATE-365, 'mm/dd/yyyy'),7,10) "
					+ "	 WHEN SUBSTR(TO_CHAR(ach_date, 'mm/dd/yyyy'),7,10) = SUBSTR(TO_CHAR(SYSDATE-(365*2), 'mm/dd/yyyy'),7,10) THEN SUBSTR(TO_CHAR(SYSDATE-(365*2), 'mm/dd/yyyy'),7,10) "
					+ "	 WHEN SUBSTR(TO_CHAR(ach_date, 'mm/dd/yyyy'),7,10) = SUBSTR(TO_CHAR(SYSDATE-(365*3), 'mm/dd/yyyy'),7,10) THEN SUBSTR(TO_CHAR(SYSDATE-(365*3), 'mm/dd/yyyy'),7,10) "
					+ "	 WHEN SUBSTR(TO_CHAR(ach_date, 'mm/dd/yyyy'),7,10) = SUBSTR(TO_CHAR(SYSDATE-(365*4), 'mm/dd/yyyy'),7,10) THEN SUBSTR(TO_CHAR(SYSDATE-(365*4), 'mm/dd/yyyy'),7,10) "
					+ "END AS yyyy, SUM(milestone_amount) as amount "
					+ "FROM erv_mileachieved  a, ER_STUDY b "
					+ "WHERE pk_study = fk_study "
					+ "AND (EXISTS (SELECT * FROM ER_STUDYTEAM bb WHERE pk_study = bb.fk_study AND bb.fk_user = "+ userId +" AND NVL(studyteam_status,'Active') = 'Active') "
					+ "OR Pkg_Phi.F_Is_Superuser("+ userId +", a.fk_study) = 1) "
					+ "AND ach_date BETWEEN f_to_date('01/01/' || SUBSTR(TO_CHAR(SYSDATE-(365*4), 'mm/dd/yyyy'),7,10)) AND f_to_date('12/31/' || SUBSTR(TO_CHAR(SYSDATE, 'mm/dd/yyyy'),7,10)) "
					+ "GROUP BY SUBSTR(TO_CHAR(ach_date, 'mm/dd/yyyy'),7,10), pk_study, a.study_number, study_title "
					+ "ORDER BY yyyy desc";
					break;
				case 1:
					//Forecasting
					studySql = "SELECT DISTINCT pk_study, z.study_number, case when length(study_title)>100 then substr(study_title,1,100)||'... ' else study_title end study_title, "
					+ "CASE "
					+ "	 WHEN SUBSTR(TO_CHAR(actual_schdate, 'mm/dd/yyyy'),7,10) = SUBSTR(TO_CHAR(SYSDATE, 'mm/dd/yyyy'),7,10) THEN SUBSTR(TO_CHAR(SYSDATE, 'mm/dd/yyyy'),7,10) "
					+ "	 WHEN SUBSTR(TO_CHAR(actual_schdate, 'mm/dd/yyyy'),7,10) = SUBSTR(TO_CHAR(SYSDATE+365, 'mm/dd/yyyy'),7,10) THEN SUBSTR(TO_CHAR(SYSDATE+365, 'mm/dd/yyyy'),7,10) "
					+ "	 WHEN SUBSTR(TO_CHAR(actual_schdate, 'mm/dd/yyyy'),7,10) = SUBSTR(TO_CHAR(SYSDATE+(365*2), 'mm/dd/yyyy'),7,10) THEN SUBSTR(TO_CHAR(SYSDATE+(365*2), 'mm/dd/yyyy'),7,10) "
					+ "	 WHEN SUBSTR(TO_CHAR(actual_schdate, 'mm/dd/yyyy'),7,10) = SUBSTR(TO_CHAR(SYSDATE+(365*3), 'mm/dd/yyyy'),7,10) THEN SUBSTR(TO_CHAR(SYSDATE+(365*3), 'mm/dd/yyyy'),7,10) "
					+ "	 WHEN SUBSTR(TO_CHAR(actual_schdate, 'mm/dd/yyyy'),7,10) = SUBSTR(TO_CHAR(SYSDATE+(365*4), 'mm/dd/yyyy'),7,10) THEN SUBSTR(TO_CHAR(SYSDATE+(365*4), 'mm/dd/yyyy'),7,10) "
					+ "END AS yyyy, COUNT(a.milestone_amount) AS COUNT, SUM(a.milestone_amount) as amount "
					+ "FROM erv_milestones_4forecast a, sch_events1 y, ER_STUDY z "
					+ "WHERE pk_study = a.FK_STUDY "
					+ "AND (EXISTS (SELECT * FROM ER_STUDYTEAM bb WHERE pk_study = bb.FK_STUDY AND bb.fk_user = "+ userId +" AND NVL(studyteam_status,'Active') = 'Active') "
					+ "OR Pkg_Phi.F_Is_Superuser("+ userId +", a.fk_study) = 1) "
					+ "AND (visit IN(SELECT milestone_visit FROM ER_MILESTONE x WHERE a.pk_milestone = x.pk_milestone) OR (a.fk_eventassoc = fk_assoc)) "
					+ "AND a.fk_cal = TO_NUMBER(session_id) "
					//+ "AND a.fk_site IN(SELECT xx.fk_site FROM ER_USERSITE xx WHERE xx.fk_user = "+ userId +" ) "
					+ "AND (study_prinv IN  (SELECT pk_user FROM ER_USER WHERE fk_account= "+ accountId +" ) OR study_prinv IS NULL) "
					+ "AND (actual_schdate <= SYSDATE+(365*4)) "
					+ "AND (to_number(SUBSTR(TO_CHAR(actual_schdate, 'mm/dd/yyyy'),7,10)) >= to_number(SUBSTR(TO_CHAR(SYSDATE, 'mm/dd/yyyy'),7,10))) "
					+ "GROUP BY SUBSTR(TO_CHAR(actual_schdate, 'mm/dd/yyyy'),7,10), pk_study, z.study_number, study_title "
					+ "ORDER BY yyyy";
	 
					break;
				case 2:
					//Receivables
					//modified by Sonia Abrol. table er_invoice_details is removed from main sql as it will not get counts for MS not achieved
					//since we are using subquerie sto get counts anyways, we dont need the table in main join
					
					studySql = "SELECT pk_study, study_number, study_title, achNInvCount, achPartInvCount, achOverInvCount "
					+ "FROM( "
					+ "SELECT pk_study, study_number, case when length(study_title)>100 then substr(study_title,1,100)||'... ' else study_title end study_title, "
					+ "SUM(msNotInvoiced) AS achNInvCount, "
					+ "SUM(CASE WHEN (amount_invoiced != 0) AND (amount_achieved > amount_invoiced) THEN msInvoiced END) AS achPartInvCount, "
					+ "SUM(CASE WHEN (amount_invoiced != 0) AND (amount_invoiced > amount_achieved) THEN msInvoiced END) AS achOverInvCount "
					+ "FROM ( "
					+ "SELECT pk_study, study_number, study_title, pk_milestone, amount_achieved * SUM(msInvoiced) AS amount_achieved,amount_invoiced,amount_received, SUM(msInvoiced) AS msInvoiced, SUM(msNotInvoiced) AS msNotInvoiced "
					+ "FROM ( "
					+ "SELECT pk_study, study_number, study_title, m.pk_milestone, "
					+ "TO_NUMBER(TRUNC(DECODE(milestone_count,0,(T.achcount * milestone_amount) ,1,(T.achcount * milestone_amount ) ,(FLOOR( ABS(T.achcount /milestone_count )) *  milestone_amount )),2)) AS amount_achieved, "
					+ "(SELECT SUM(idet.amount_invoiced) FROM ER_INVOICE_DETAIL idet WHERE    idet.fk_milestone = m.pk_milestone AND idet.fk_mileachieved = T.pk_mileachieved AND idet.DETAIL_TYPE = 'D'  ) amount_invoiced, "
					+ "TO_NUMBER(NVL(Pkg_Milestone_New. getMilePaymentAmount(m.pk_milestone,PKG_DATEUTIL.f_get_null_date_str,PKG_DATEUTIL.f_get_future_null_date_str,'payrec') ,0)) amount_received, "
					+ "TO_NUMBER(NVL(Pkg_Milestone_New.getMilePaymentAmount(m.pk_milestone, PKG_DATEUTIL.f_get_null_date_str,PKG_DATEUTIL.f_get_future_null_date_str, 'paymade') ,0)) amount_paid, "
					+ "CASE WHEN (SELECT NVL(SUM(idet.amount_invoiced),0) FROM ER_INVOICE_DETAIL idet WHERE  idet.fk_milestone = m.pk_milestone AND idet.fk_mileachieved = T.pk_mileachieved AND idet.DETAIL_TYPE = 'D'  )>0 THEN 1 ELSE 0 END AS msInvoiced, "
					+ "CASE WHEN (SELECT NVL(SUM(idet.amount_invoiced),0) FROM ER_INVOICE_DETAIL idet WHERE  idet.fk_milestone = m.pk_milestone AND idet.fk_mileachieved = T.pk_mileachieved AND idet.DETAIL_TYPE = 'D'  )=0 THEN 1 ELSE 0 END AS msNotInvoiced "
					+ "FROM  ER_STUDY s, "
					+ "(SELECT a.pk_mileachieved, a.FK_MILESTONE, 1 AS achcount FROM ERV_MILEACHIEVED a WHERE a.fk_account = "+ accountId +" AND  ACH_DATE >=  TO_DATE (' 01-Jan-1900')  AND  ACH_DATE <=  TO_DATE (' 01-Jan-3000')  AND (study_prinv IN   ( SELECT pk_user FROM ER_USER WHERE fk_account= "+ accountId +") OR study_prinv IS NULL) AND DECODE(MILESTONE_TYPE,'SM',1, (Pkg_User.f_chk_right_for_studysite(fk_study,"+ userId +",a.fk_site)) )  > 0  ORDER BY fk_milestone ) T, "
					+ "ER_MILESTONE m "
					+ "WHERE T.fk_milestone = m.pk_milestone AND m.fk_study = pk_study AND s.fk_account = "+ accountId +" "
					+ "AND (pk_study IN (SELECT h.fk_study FROM ER_STUDYTEAM h WHERE pk_study = h.fk_study AND h.fk_user = "+ userId +" AND NVL(studyteam_status,'Active') = 'Active') "
 					+ "OR Pkg_Phi.F_Is_Superuser("+ userId +", pk_study) = 1) "
					+ "ORDER BY study_number,pk_milestone "
					+ ") "
					+ "GROUP BY pk_study, study_number, study_title, pk_milestone, amount_achieved,amount_invoiced,amount_received, msInvoiced "
					+ ")GROUP BY pk_study, study_number, study_title "
					+ ")WHERE "
					+ "(achNInvCount != 0 AND  achNInvCount IS NOT NULL) OR (achPartInvCount  != 0 AND achPartInvCount IS NOT NULL) OR (achOverInvCount  != 0 AND achOverInvCount IS NOT NULL) "
					+ "ORDER BY 1 ";
						
					/*studySql = "SELECT pk_study, study_number, achNInvCount, achPartInvCount, achOverInvCount "
					+ "FROM( "
					+ "SELECT pk_study, study_number, "
					+ "SUM(CASE WHEN (amount_achieved != amount_paid) AND (amount_invoiced = 0) THEN 1 END) AS achNInvCount, "
					+ "SUM(CASE WHEN (amount_achieved != amount_paid) AND (amount_invoiced != 0) AND (amount_achieved > amount_invoiced) THEN 1 END) AS achPartInvCount, "
					+ "SUM(CASE WHEN (amount_achieved != amount_paid) AND (amount_invoiced != 0) AND (amount_invoiced > amount_achieved) THEN 1 END) AS achOverInvCount "
					+ "FROM ( "
					+ "SELECT pk_study, study_number, "
					+ "to_number(TRUNC(DECODE(milestone_count,0,(T.achcount * milestone_amount) ,1,(T.achcount * milestone_amount ) ,(FLOOR( ABS(T.achcount /milestone_count )) *  milestone_amount )),2)) AS amount_achieved, "
					+ "to_number(DECODE(milestone_paytype ,(SELECT pk_codelst FROM ER_CODELST WHERE trim(codelst_type) = 'milepaytype' AND codelst_subtyp = 'rec'),  Pkg_Milestone_New.getMileInvoiceAmount(m.pk_milestone,'01/01/1900','01/01/3000') ,0 )) amount_invoiced, "
					+ "to_number(NVL(Pkg_Milestone_New. getMilePaymentAmount(m.pk_milestone,'01/01/1900','01/01/3000','payrec') ,0)) amount_received, "
					+ "to_number(NVL(Pkg_Milestone_New.getMilePaymentAmount(m.pk_milestone, '01/01/1900','01/01/3000', 'paymade') ,0)) amount_paid "
					+ "FROM  ER_STUDY s, "
					+ "(SELECT FK_MILESTONE, 1 AS achcount FROM ERV_MILEACHIEVED a WHERE a.fk_account = "+ accountId +"  AND  ACH_DATE >=  TO_DATE (' 01-Jan-1900')  AND  ACH_DATE <=  TO_DATE (' 01-Jan-3000')  AND (study_prinv IN   ( SELECT pk_user FROM ER_USER WHERE fk_account= "+ accountId +") OR study_prinv IS NULL) AND DECODE(MILESTONE_TYPE,'SM',1, (Pkg_User.f_chk_right_for_studysite(fk_study,"+ userId +",a.fk_site)) )  > 0  ORDER BY fk_milestone ) T, "
					+ "ER_MILESTONE m "
					+ "WHERE T.fk_milestone = pk_milestone AND m.fk_study = pk_study AND s.fk_account = "+ accountId +" "
					+ "ORDER BY study_number,pk_milestone "
					+ ") GROUP BY pk_study, study_number "
					+ ")WHERE achNInvCount IS NOT NULL OR achPartInvCount IS NOT NULL OR achOverInvCount IS NOT NULL "
					+ "ORDER BY 1";*/
					
					break;
				}
				break;
			case 3:
				//Invoices
				switch(snodeNo){
				case 0:
					//By Invoice Date
					studySql = "SELECT pk_study, study_number, pk_invoice, inv_number, to_char(inv_date, PKG_DATEUTIL.f_get_dateformat) as inv_date, to_char(a.created_on, PKG_DATEUTIL.f_get_dateformat) AS created_on, "
					+ "CASE "
					+ "WHEN TO_DATE(a.inv_date) = TRUNC(SYSDATE)  THEN 1 "
					+ "WHEN (TO_DATE(a.inv_date) < TRUNC(SYSDATE) AND TO_DATE(a.inv_date) >= TRUNC(SYSDATE) -7) THEN 2 "
					+ "WHEN (TO_DATE(a.inv_date) < TRUNC(SYSDATE) -7 AND TO_DATE(a.inv_date) >= TRUNC(SYSDATE) -15) THEN 3 "
					+ "WHEN (TO_DATE(a.inv_date) < TRUNC(SYSDATE) -15 AND TO_DATE(a.inv_date) >= TRUNC(SYSDATE) -30) THEN 4 "
					+ "END AS gen_when "
					+ "FROM ER_INVOICE a, ER_STUDY b "
					+ "WHERE pk_study = fk_study "
					+ "AND (EXISTS (SELECT * FROM ER_STUDYTEAM bb WHERE pk_study = bb.fk_study AND bb.fk_user = "+ userId +" AND NVL(studyteam_status,'Active') = 'Active') "
					+ "OR Pkg_Phi.F_Is_Superuser("+ userId +", a.fk_study) = 1) "
					+ "AND fk_account = "+ accountId +" "
					+ "AND (a.inv_date <= TRUNC(SYSDATE)  AND a.inv_date >= TRUNC(SYSDATE) -30) "
					+ "ORDER BY 7";
					break;
				case 1:
					//Past Due
					studySql = "SELECT pk_study, study_number, pk_invoice, inv_number, TO_CHAR(inv_date, PKG_DATEUTIL.f_get_dateformat) AS inv_date, TO_CHAR(due_by, PKG_DATEUTIL.f_get_dateformat) AS due_by,  "
					+ "CASE "
					+ "	WHEN TO_DATE(due_by) = TRUNC(SYSDATE)  THEN 1 "
					+ "	WHEN (TO_DATE(due_by) < TRUNC(SYSDATE) AND TO_DATE(due_by) >= TRUNC(SYSDATE) -7) THEN 2 "
					+ "	WHEN (TO_DATE(due_by) < TRUNC(SYSDATE) -7 AND TO_DATE(due_by) >= TRUNC(SYSDATE) -15) THEN 3 "
					+ "	WHEN (TO_DATE(due_by) < TRUNC(SYSDATE) -15 AND TO_DATE(due_by) >= TRUNC(SYSDATE) -30) THEN 4 "
					+ "	WHEN (TO_DATE(due_by) < TRUNC(SYSDATE) -30) THEN 5 "
					+ "END AS due_when "
					+ "FROM "
					+ "(SELECT pk_study, study_number, pk_invoice, inv_number, inv_date, "
					+ "CASE WHEN f_to_number(inv_payment_dueby) IS NULL THEN inv_date "
					+ "ELSE inv_date + f_to_number(inv_payment_dueby) "
					+ "END AS due_by "
					+ "FROM ER_INVOICE a, ER_STUDY b "
					+ "WHERE pk_study = fk_study "
					+ "AND (EXISTS (SELECT * FROM ER_STUDYTEAM bb WHERE pk_study = bb.fk_study AND bb.fk_user = "+ userId +" AND NVL(studyteam_status,'Active') = 'Active') "
					+ "OR Pkg_Phi.F_Is_Superuser("+ userId +", a.fk_study) = 1) "
					+ "AND fk_account = "+ accountId +") "
					+ "WHERE (TO_DATE(due_by) < trunc(SYSDATE)) "
					+ "order by due_when, due_by ";
					break;
				}
				break;
			case 4:
				//Payments
				//By Payment Date & By Amount
				studySql = "SELECT pk_study, study_number, pk_milepayment, milepayment_date, milepayment_amt,"
				+ "CASE "
				+ "WHEN to_date(milepayment_date) = TRUNC(SYSDATE)  THEN 1 "
				+ "WHEN (to_date(milepayment_date) < TRUNC(SYSDATE) AND to_date(milepayment_date) >= TRUNC(SYSDATE) -7) THEN 2 "
				+ "WHEN (to_date(milepayment_date) < TRUNC(SYSDATE) -7 AND to_date(milepayment_date) >= TRUNC(SYSDATE) -15) THEN 3 "
				+ "WHEN (to_date(milepayment_date) < TRUNC(SYSDATE) -15 AND to_date(milepayment_date) >= TRUNC(SYSDATE) -30) THEN 4 "
				+ "END AS pay_when "
				+ "FROM "
				+ "(SELECT pk_study, study_number, pk_milepayment, to_char(milepayment_date) as milepayment_date, milepayment_amt "
				+ "FROM ER_MILEPAYMENT a, ER_STUDY b "
				+ "WHERE pk_study = fk_study "
				+ "AND (EXISTS (SELECT * FROM ER_STUDYTEAM bb WHERE pk_study = bb.fk_study AND bb.fk_user = "+ userId +" AND NVL(studyteam_status,'Active') = 'Active') "
				+ "OR Pkg_Phi.F_Is_Superuser("+ userId +", pk_study) = 1) "
				+ "AND fk_account = "+ accountId +" "
				+ "AND milepayment_delflag != 'Y') "
				+ "WHERE (to_date(milepayment_date) <= TRUNC(SYSDATE)  AND to_date(milepayment_date) >= TRUNC(SYSDATE) -30) "
				+ "ORDER BY pay_when";
				break;
			}
			//System.out.println(studySql);
			
			countSql = "select count(*) from (" + studySql + ")";
			rowsPerPage=0;
			totalPages=0;	
			rowsReturned = 0;
			showPages = 0;
			totalRows = 0;	   
			firstRec = 0;
			lastRec = 0;	   
			openCount = 0;
			resolvedCount = 0;
			reopenCount = 0;
			closedCount = 0;
			enrolled = 0;

			studyNumber = null;
			studyTitle = null;
			studyStatus = null;
			studyActualDt = null;
			studyIRB = null;
			studyIRB2 = null;
			studySite = null;
			studyPI = null;

			studyId=0;
			hasMore = false;
			hasPrevious = false;

			//		rowsPerPage =  Configuration.ROWSPERBROWSERPAGE ;
			rowsPerPage =  1000;
			totalPages =Configuration.PAGEPERBROWSER ;
			br = new BrowserRows();
			br.getPageRows(curPage,rowsPerPage,studySql,totalPages,countSql,orderBy,orderType);
			
			rowsReturned = br.getRowReturned();
			showPages = br.getShowPages();
			startPage = br.getStartPage();
			hasMore = br.getHasMore();
			hasPrevious = br.getHasPrevious();
			totalRows = br.getTotalRows();
			firstRec = br.getFirstRec();
			lastRec = br.getLastRec();
		%>
		<BR>
		
		<Input type="hidden" name="srcmenu" value="<%=src%>">
		<Input type="hidden" name="page" value="<%=curPage%>">
		<Input type="hidden" name="orderBy" value="<%=orderBy%>">
		<Input type="hidden" name="orderType" value="<%=orderType%>">
		<%
		String nodeName ="";
		
		switch (nodeNo){
	    	case 0:
	    		nodeName=LC.L_Fin_Summary/*"Financial Summary"*****/;
				break;
	    	case 1:
				switch(snodeNo){
	    	 	case 0:
		    		nodeName=MC.M_Bgt_ByModfDt/*"Budgets By Modification Date"*****/;
					break;
	    	 	case 1:
					nodeName=LC.L_Bgt_ByStat/*"Budgets By Status"*****/;
					break;
				}
				break;
			case 2:
				switch(snodeNo){
	    		case 0:
	    			nodeName=LC.L_Mstone_Achv/*"Milestones- Achievements"*****/;
					break;
	    	 	case 1:
	    	 		nodeName=LC.L_Mstone_Fcast/*"Milestones- Forecasting"*****/;
					break;
	    	 	case 2:
	    	 		nodeName=LC.L_Mstone_Recv/*"Milestones- Receivables"*****/;
					break;
				}
				break;
			case 3:
	    		switch(snodeNo){
	    		case 0:
	    			nodeName=MC.M_InvBy_InvDt/*"Invoices By Invoice Date"*****/;
					break;
	    	 	case 1:
	    	 		nodeName=MC.M_InvPast_Due/*"Invoices Past Due"*****/;
					break;
	    		}
	    		break;
			case 4:
				switch(snodeNo){
	    		case 0:
	    			nodeName=MC.M_Pment_PmentDt/*"Payments By Payment Date"*****/;
					break;
	    		case 1:
	    			nodeName=MC.M_Pment_ByAmt/*"Payments By Amount"*****/;
					break;
				}
	    		break;
		}%>
		<Table width="100%">
			<tr bgcolor="#dcdcdc" height="30">
				<td><b><%=nodeName%></b> </td>
			</tr>
		</Table><br>
	    <table width="100%" >
	    	<tr> <%
	    	switch (nodeNo){
	    	case 0:
	    		//Financial Summary%>
	    		<th width="3%"><%=LC.L_Date%><%--Date*****--%></th>
	        	<th width="3%"># <%=LC.L_Milestone%><%--Milestone*****--%></th>
	        	<th width="5%"><%=LC.L_TotalAmount%><%--Total Amount*****--%></th>
	    		<th width="8%"><%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%></th>
	    		<th width="2%"></th>
			</tr>
				<%
		    	for(int counter = 1;counter<=rowsReturned;counter++){
		    		studyId = EJBUtil.stringToNum(br.getBValues(counter,"pk_study"));
				    if ((counter%2)==0) { %>
		  	<tr class="browserEvenRow"> 
		    		<%}	else{%>
		  	<tr class="browserOddRow"> 
		    		<%}%>
				<td align="center"> <%=br.getBValues(counter,"actual_schdate")%> </td>
				<td align="center"> <%=br.getBValues(counter,"cnt")%> </td>
				<td align="center"> <%=br.getBValues(counter,"total")%> </td>
				<td> <%=br.getBValues(counter,"study_number")%> </td>
				<td>
	  				<a href="study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId=<%=studyId%>"><img src="../images/jpg/study.gif" alt="<%=LC.L_Std_Admin%><%--<%=LC.Std_Study%> Administration--%>" border=0 ></a>
		  		<%if(studyActualDt!=null /*&& EJBUtil.stringToNum(mPatRight) > 0 */ ){%>
					<a href="studypatients.jsp?srcmenu=tdmenubaritem5&srcmenu=<%=src%>&studyId=<%=studyId%>&patid=&patstatus=&selectedTab=2"><img src="../images/jpg/patient.gif" alt="<%=LC.L_PatMgmt_EnrlPat%><%--<%=LC.Pat_Patient%> Management - Enrolled <%=LC.Pat_Patients%>*****--%>" border=0 ></a>
				<%}%>
				</td>
		    	
			</tr>
				<%}%>
		</table>
	      		<%break;
			case 1:
	    		//Budgets
				//Added by Manimaran for the issue #4099
				int budgetrights = 0;
				budgetrights = Integer.parseInt(grpRights.getFtrRightsByValue("BUDGT"));
				switch(snodeNo){
	    	 	case 0:
	    		//By Modification Date%>
	    		<th width="8%"><%=LC.L_Modified%><%--Modified*****--%></th>
	        	<th width="8%"><%=LC.L_Budget_Name%><%--Budget Name*****--%></th>
				<th width="3%"><%=LC.L_Type%><%--Type*****--%></th>
	        	<th width="3%"><%=LC.L_Version%><%--Version*****--%></th>
				<th width="6%"><%=LC.L_Status%><%--Status*****--%></th>
				<th width="6%"><%=LC.L_Created_By%><%--Created By*****--%></th> 
				<th width="8%"><%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%></th>
	    		<th width="2%"></th>
			</tr>
				<%
		    	for(int counter = 1;counter<=rowsReturned;counter++){
		    		studyId = EJBUtil.stringToNum(br.getBValues(counter,"pk_study"));
		    		int budgetId = EJBUtil.stringToNum(br.getBValues(counter,"pk_budget"));
		    		int lmWhen = EJBUtil.stringToNum(br.getBValues(counter,"lm_When"));
				    if ((counter%2)==0) { %>
		  	<tr class="browserEvenRow"> 
		    		<%}	else{%>
		  	<tr class="browserOddRow"> 
		    		<%}
		    	switch(lmWhen){ 
		    		case 1:%>
	    				<td class="reportGrouping"><%=LC.L_Today%><%--Today*****--%></td>
		    		<%break;
		    		case 2:%>
	    				<td class="reportGrouping"><%=LC.L_Last_7_Days%><%--Last 7 Days*****--%></td>
		    		<%break;
		    		case 3:%>
	    				<td class="reportGrouping"><%=MC.M_Last8_15Days%><%--Last 8 to 15 Days*****--%></td>
		    		<%break;
		    		case 4:%>
	    				<td class="reportGrouping"><%=MC.M_Last16_30Days%><%--Last 16 to 30 Days*****--%></td>
		    		<%break;
		    		default:%>
	    				<td class="reportGrouping">-</td>
		    		<%break;
		    		}%>
				<td> 
				 <%
				 String budType = br.getBValues(counter,"budget_type");
				 
				 if(budgetrights > 2) {	
					if (budType.equals("Study")){%>
						<A href='studybudget.jsp?mode=M&budgetId=<%=budgetId%>&srcmenu=tdmenubaritem6&selectedTab=2'><%=br.getBValues(counter,"budget_name")%></A>
					<%}else{%>	    			
						<A href='patientbudget.jsp?mode=M&budgetId=<%=budgetId%>&srcmenu=tdmenubaritem6&selectedTab=2'><%=br.getBValues(counter,"budget_name")%></A>
		    		<%} 
				 } else { %>
					<%=br.getBValues(counter,"budget_name")%>
				  <%}%>
				</td>
				<td align="center"> <%=(budType==null)?"-":budType%> </td>
				<td align="center"> <%=(br.getBValues(counter,"budget_version") ==null)?"-":( br.getBValues(counter,"budget_version"))%> </td>
				<td> <%=br.getBValues(counter,"budget_status")%> </td>
				<td> <%=(br.getBValues(counter,"created_by") ==null)?"-":( br.getBValues(counter,"created_by"))%> </td>
				<td> <%=br.getBValues(counter,"study_number")%> </td>
				<td>
				<%if(studyId!=0){%>
	  				<a href="study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId=<%=studyId%>"><img src="../images/jpg/study.gif" alt="<%=LC.L_Std_Admin%><%--<%=LC.Std_Study%> Administration*****--%>" border=0 ></a>
		  		<%}%>
		  		<%if(studyActualDt!=null /*&& EJBUtil.stringToNum(mPatRight) > 0 */ ){%>
					<a href="studypatients.jsp?srcmenu=tdmenubaritem5&srcmenu=<%=src%>&studyId=<%=studyId%>&patid=&patstatus=&selectedTab=2"><img src="../images/jpg/patient.gif" alt="<%=LC.L_PatMgmt_EnrlPat%><%--<%=LC.Pat_Patient%> Management - Enrolled <%=LC.Pat_Patients%>*****--%>" border=0 ></a>
				<%}%>
				</td>
		    	
			</tr>
				<%}%>
		</table>
	      		<%break;
	      		case 1:
	      		//By Status%>
	      		<th width="6%"><%=LC.L_Status%><%--Status*****--%></th>
	        	<th width="8%"><%=LC.L_Budget_Name%><%--Budget Name*****--%></th>
				<th width="3%"><%=LC.L_Type%><%--Type*****--%></th>
	        	<th width="3%"><%=LC.L_Version%><%--Version*****--%></th>
				<th width="6%"><%=LC.L_Created_By%><%--Created By*****--%></th> 
	    		<th width="8%"><%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%></th>
	    		<th width="2%"></th>
			</tr>
				<%
		    	for(int counter = 1;counter<=rowsReturned;counter++){
		    		studyId = EJBUtil.stringToNum(br.getBValues(counter,"pk_study"));
		    		int budgetId = EJBUtil.stringToNum(br.getBValues(counter,"pk_budget"));
				    if ((counter%2)==0) { %>
		  	<tr class="browserEvenRow"> 
		    		<%}	else{%>
		  	<tr class="browserOddRow"> 
		    		<%}%>
		    	<td class="reportGrouping"> <%=br.getBValues(counter,"budget_status")%> </td>
				<td> 
				 <%String budType = br.getBValues(counter,"budget_type");
			 
				 if(budgetrights > 2) {	
					if (budType.equals("Study")){%>
						<A href='studybudget.jsp?mode=M&budgetId=<%=budgetId%>&srcmenu=tdmenubaritem6&selectedTab=2'><%=br.getBValues(counter,"budget_name")%></A>
					<%}else{%>	    			
						<A href='patientbudget.jsp?mode=M&budgetId=<%=budgetId%>&srcmenu=tdmenubaritem6&selectedTab=2'><%=br.getBValues(counter,"budget_name")%></A>
		    		<%} 
				 } else { %>
					<%=br.getBValues(counter,"budget_name")%>
				  <%}%>
				</td>
				<td align="center"> <%=(br.getBValues(counter,"budget_type") ==null)?"-":( br.getBValues(counter,"budget_type"))%> </td>
				<td align="center"> <%=(br.getBValues(counter,"budget_version") ==null)?"-":( br.getBValues(counter,"budget_version"))%> </td>
				<td> <%=(br.getBValues(counter,"created_by") ==null)?"-":( br.getBValues(counter,"created_by"))%> </td>
				
		    	<td> <%=br.getBValues(counter,"study_number")%> </td>
		    	<td>
	  			<%if(studyId!=0){%>
	  				<a href="study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId=<%=studyId%>"><img src="../images/jpg/study.gif" alt="<%=LC.L_Std_Admin%><%--<%=LC.Std_Study%> Administration*****--%>" border=0 ></a>
		  		<%}%>
		  		<%if(studyActualDt!=null /*&& EJBUtil.stringToNum(mPatRight) > 0 */ ){%>
					<a href="studypatients.jsp?srcmenu=tdmenubaritem5&srcmenu=<%=src%>&studyId=<%=studyId%>&patid=&patstatus=&selectedTab=2"><img src="../images/jpg/patient.gif" alt="<%=LC.L_PatMgmt_EnrlPat%><%--<%=LC.Pat_Patient%> Management - Enrolled <%=LC.Pat_Patients%>*****--%>" border=0 ></a>
				<%}%>
				</td>
			</tr>
				<%}%>
		</table>
	    	<%}
	    		break;
	    	case 2:
	    		//Milestones
				GrpRightsJB grprightsJB = (GrpRightsJB) tSession.getValue("GRights");		
				int mileGrpRight =0;
				mileGrpRight = Integer.parseInt(grprightsJB.getFtrRightsByValue("MILEST"));
				
				switch(snodeNo){
	    		case 0:
	    			//Achievements%>
	    		<th width="3%" align="center"><%=LC.L_Year%><%--Year*****--%></th>
	    		<th width="3%" align="center"><%=LC.L_Amount%><%--Amount*****--%></th>
	    		<th width="6%"><%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%></th>
	        	<th width="15%"><%=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%></th>
	    		<th width="1%"></th>
	    		<th width="1%"></th>
	        	
			</tr>
			<%
		    	for(int counter = 1;counter<=rowsReturned;counter++){
		    		studyId = EJBUtil.stringToNum(br.getBValues(counter,"pk_study"));
				    if ((counter%2)==0) { %>
		  	<tr class="browserEvenRow"> 
		    		<%}	else{%>
		  	<tr class="browserOddRow"> 
		    		<%}%>
		    	<td class="reportGrouping" align="center"> <%=br.getBValues(counter,"yyyy")%> </td>
		    	<td align="center"> <%=br.getBValues(counter,"amount")%> </td>
				<td> <%=br.getBValues(counter,"study_number")%> </td>
				<td> <%=br.getBValues(counter,"study_title")%> </td>
				<td>
					<% if (mileGrpRight >= 4)	{	%>
					<A href="milestone.jsp?srcmenu=tdmenubaritem7&selectedTab=1&studyId=<%=studyId%>"><%=LC.L_View_Mstones%><%--View Milestones*****--%></A>
					<% } %>	
					</td>
		    	<td>
	  				<a href="study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId=<%=studyId%>"><img src="../images/jpg/study.gif" alt="<%=LC.L_Std_Admin%><%--<%=LC.Std_Study%> Administration*****--%>" border=0 ></a>
		  		<%if(studyActualDt!=null /*&& EJBUtil.stringToNum(mPatRight) > 0 */ ){%>
					<a href="studypatients.jsp?srcmenu=tdmenubaritem5&srcmenu=<%=src%>&studyId=<%=studyId%>&patid=&patstatus=&selectedTab=2"><img src="../images/jpg/patient.gif" alt="<%=LC.L_PatMgmt_EnrlPat%><%--<%=LC.Pat_Patient%> Management - Enrolled <%=LC.Pat_Patients%>*****--%>" border=0 ></a>
				<%}%>
				</td>
			</tr>
				<%}%>
		</table><%
				break;
				case 1:
					//Forecasting%>
				<th width="3%" align="center"><%=LC.L_Year%><%--Year*****--%></th>
				<th width="3%" align="center"><%=LC.L_Count%><%--Count*****--%></th>
				<th width="3%" align="center"><%=LC.L_Amount%><%--Amount*****--%></th>
				<th width="6%"><%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%></th>
	        	<th width="15%"><%=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%></th>
				<th width="1%"></th>
				<th width="1%"></th>
			</tr>
			<%
		    	for(int counter = 1;counter<=rowsReturned;counter++){
		    		studyId = EJBUtil.stringToNum(br.getBValues(counter,"pk_study"));
				    if ((counter%2)==0) { %>
		  	<tr class="browserEvenRow"> 
		    		<%}	else{%>
		  	<tr class="browserOddRow"> 
		    		<%}%>
		    	<td class="reportGrouping" align="center"> <%=br.getBValues(counter,"yyyy")%> </td>
		    	<td align="center"> <%=br.getBValues(counter,"count")%> </td>
		    	<td align="center"> <%=br.getBValues(counter,"amount")%> </td>
				<td> <%=br.getBValues(counter,"study_number")%> </td>
				<td> <%=br.getBValues(counter,"study_title")%> </td>
				<td> 
				<% if (mileGrpRight >= 4)	{	%>
				<A href="milestone.jsp?srcmenu=tdmenubaritem7&selectedTab=1&studyId=<%=studyId%>"><%=LC.L_View_Mstones%><%--View Milestones*****--%></A> 
				<% } %>			
				</td>
		    	<td>
	  				<a href="study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId=<%=studyId%>"><img src="../images/jpg/study.gif" alt="<%=LC.L_Std_Admin%><%--<%=LC.Std_Study%> Administration*****--%>" border=0 ></a>
		  		<%if(studyActualDt!=null /*&& EJBUtil.stringToNum(mPatRight) > 0 */ ){%>
					<a href="studypatients.jsp?srcmenu=tdmenubaritem5&srcmenu=<%=src%>&studyId=<%=studyId%>&patid=&patstatus=&selectedTab=2"><img src="../images/jpg/patient.gif" alt="<%=LC.L_PatMgmt_EnrlPat%><%--<%=LC.Pat_Patient%> Management - Enrolled <%=LC.Pat_Patients%>*****--%>" border=0 ></a>
				<%}%>
				</td>
			</tr>
				<%}%>
			</table><%
				break;
				case 2:
					//Receivables%>
				<th width="3%"><%=LC.L_Achv_NtInvoiced%><%--Achieved/ Not Invoiced*****--%></th>
				<th width="3%"><%=LC.L_Achv_PartInvoiced%><%--Achieved/ Partially Invoiced*****--%></th>
				<th width="3%"><%=LC.L_Ach_OverInvoiced%><%--Achieved/ Over Invoiced*****--%></th>
	        	<th width="6%"><%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%></th>
				<th width="15%"><%=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%></th>
	        	<th width="1%"></th>
				<th width="1%"></th>
			</tr>
			<%
		    	for(int counter = 1;counter<=rowsReturned;counter++){
		    		studyId = EJBUtil.stringToNum(br.getBValues(counter,"pk_study"));
				    if ((counter%2)==0) { %>
		  	<tr class="browserEvenRow"> 
		    		<%}	else{%>
		  	<tr class="browserOddRow"> 
		    		<%}%>
		    	<td class="reportGrouping" align="center"> <%=(EJBUtil.stringToNum(br.getBValues(counter,"achNInvCount")) == 0)?"-":( br.getBValues(counter,"achNInvCount"))%> </td>
		    	<td class="reportGrouping" align="center"> <%=(br.getBValues(counter,"achPartInvCount") ==null)?"-":( br.getBValues(counter,"achPartInvCount"))%> </td>
		    	<td class="reportGrouping" align="center"> <%=(br.getBValues(counter,"achOverInvCount") ==null)?"-":( br.getBValues(counter,"achOverInvCount"))%> </td>
		    	<td> <%=br.getBValues(counter,"study_number")%> </td>
				<td> <%=br.getBValues(counter,"study_title")%> </td>
		    	<td>
				<% if (mileGrpRight >= 4)	{	%>		
				<A href="milestone.jsp?srcmenu=tdmenubaritem7&selectedTab=1&studyId=<%=studyId%>"><%=LC.L_View_Mstones%><%--View Milestones*****--%></A> 
				<%}%>		

				</td>
		    	<td>
	  				<a href="study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId=<%=studyId%>"><img src="../images/jpg/study.gif" alt="<%=LC.L_Std_Admin%><%--<%=LC.Std_Study%> Administration*****--%>" border=0 ></a>
		  		<%if(studyActualDt!=null /*&& EJBUtil.stringToNum(mPatRight) > 0 */ ){%>
					<a href="studypatients.jsp?srcmenu=tdmenubaritem5&srcmenu=<%=src%>&studyId=<%=studyId%>&patid=&patstatus=&selectedTab=2"><img src="../images/jpg/patient.gif" alt="<%=LC.L_PatMgmt_EnrlPat%><%--<%=LC.Pat_Patient%> Management - Enrolled <%=LC.Pat_Patients%>*****--%>" border=0 ></a>
				<%}%>
				</td>
			</tr>
				<%}%>
			</table><%
				break;
				}
	    	  	break;
	    	case 3:
	    		//Invoices
	    		switch(snodeNo){
	    		case 0:
	    		//By Invoice Date%>
				<th width="8%"><%=LC.L_Invs_Created%><%--Invoices Created*****--%></th>
	        	<th width="8%"><%=LC.L_Invoice%><%--Invoice*****--%> #</th>
	        	<th width="8%"><%=LC.L_Invoice_Date%><%--Invoice Date*****--%></th>
				<th width="8%"><%=LC.L_Generation_Date%><%--Generation Date*****--%></th> 
	    		<th width="8%"><%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%></th>
	    		<th width="2%"></th>
			</tr>
				<%
		    	for(int counter = 1;counter<=rowsReturned;counter++){
		    		studyId = EJBUtil.stringToNum(br.getBValues(counter,"pk_study"));
		    		int invoiceId = EJBUtil.stringToNum(br.getBValues(counter,"pk_invoice"));
		    		int genWhen = EJBUtil.stringToNum(br.getBValues(counter,"gen_When"));
				    if ((counter%2)==0) { %>
		  	<tr class="browserEvenRow"> 
		    		<%}	else{%>
		  	<tr class="browserOddRow"> 
		    		<%}
		    	switch(genWhen){ 
		    		case 1:%>
	    				<td class="reportGrouping"><%=LC.L_Today%><%--Today*****--%></td>
		    		<%break;
		    		case 2:%>
	    				<td class="reportGrouping"><%=LC.L_Last_7_Days%><%--Last 7 Days*****--%></td>
		    		<%break;
		    		case 3:%>
	    				<td class="reportGrouping"><%=MC.M_Last8_15Days%><%--Last 8 to 15 Days*****--%></td>
		    		<%break;
		    		case 4:%>
	    				<td class="reportGrouping"><%=MC.M_Last16_30Days%><%--Last 16 to 30 Days*****--%></td>
		    		<%break;
		    		default:%>
	    				<td class="reportGrouping">-</td>
		    		<%break;
		    		}%>
		    	<td align="center"> <A href="#" onClick="return viewInvoice('<%=invoiceId%>','V')"><%=br.getBValues(counter,"inv_number")%></A> </td>
				<td align="center"> <%=(br.getBValues(counter,"inv_date") ==null)?"-":( br.getBValues(counter,"inv_date"))%> </td>
				<td align="center"> <%=(br.getBValues(counter,"created_on") ==null)?"-":( br.getBValues(counter,"created_on"))%> </td>
				<td> <%=br.getBValues(counter,"study_number")%> </td>
				<td>
	  				<a href="study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId=<%=studyId%>"><img src="../images/jpg/study.gif" alt="<%=LC.L_Std_Admin%><%--<%=LC.Std_Study%> Administration*****--%>" border=0 ></a>
		  		<%if(studyActualDt!=null /*&& EJBUtil.stringToNum(mPatRight) > 0 */ ){%>
					<a href="studypatients.jsp?srcmenu=tdmenubaritem5&srcmenu=<%=src%>&studyId=<%=studyId%>&patid=&patstatus=&selectedTab=2"><img src="../images/jpg/patient.gif" alt="<%=LC.L_PatMgmt_EnrlPat%><%--<%=LC.Pat_Patient%> Management - Enrolled <%=LC.Pat_Patients%>*****--%>" border=0 ></a>
				<%}%>
				</td>
			</tr>
			<%}%>
		</table><%
				break;
				case 1:
				//Past Due%>
				<th width="10%"><%=LC.L_Past_DueFor%><%--Past Due For*****--%></th>
	        	<th width="8%"><%=LC.L_Invoice%><%--Invoice*****--%> #</th>
	        	<th width="8%"><%=LC.L_Invoice_Date%><%--Invoice Date*****--%></th>
				<th width="8%"><%=LC.L_Inv_DueDate%><%--Invoice Due Date*****--%></th> 
	    		<th width="8%"><%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%></th>
	    		<th width="2%"></th>
			</tr>
				<%
		    	for(int counter = 1;counter<=rowsReturned;counter++){
		    		studyId = EJBUtil.stringToNum(br.getBValues(counter,"pk_study"));
		    		int invoiceId = EJBUtil.stringToNum(br.getBValues(counter,"pk_invoice"));
		    		int dueWhen = EJBUtil.stringToNum(br.getBValues(counter,"due_When"));
				    if ((counter%2)==0) { %>
		  	<tr class="browserEvenRow"> 
		    		<%}	else{%>
		  	<tr class="browserOddRow"> 
		    		<%}
		    	switch(dueWhen){ 
		    		case 1:%>
	    				<td class="reportGrouping"><%=LC.L_Today%><%--Today*****--%></td>
		    		<%break;
		    		case 2:%>
	    				<td class="reportGrouping"><%=LC.L_Last_7_Days%><%--Last 7 Days*****--%></td>
		    		<%break;
		    		case 3:%>
	    				<td class="reportGrouping"><%=MC.M_Last8_15Days%><%--Last 8 to 15 Days*****--%></td>
		    		<%break;
		    		case 4:%>
	    				<td class="reportGrouping"><%=MC.M_Last16_30Days%><%--Last 16 to 30 Days*****--%></td>
		    		<%break;
		    		case 5:%>
	    				<td class="reportGrouping"><%=MC.M_GtThan_30Days%><%--Greater Than 30 Days*****--%></td>
		    		<%break;
		    		default:%>
	    				<td class="reportGrouping">-</td>
		    		<%break;
		    		}%>
		    	<td align="center"> <A href='#' onClick='return viewInvoice(<%=invoiceId%>)'><%=br.getBValues(counter,"inv_number")%></A> </td>
				<td align="center"> <%=(br.getBValues(counter,"inv_date") ==null)?"-":( br.getBValues(counter,"inv_date"))%> </td>
				<td align="center"> <%=(br.getBValues(counter,"due_by") ==null)?"-":( br.getBValues(counter,"due_by"))%> </td>
				<td> <%=br.getBValues(counter,"study_number")%> </td>
				<td>
	  				<a href="study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId=<%=studyId%>"><img src="../images/jpg/study.gif" alt="<%=LC.L_Std_Admin%><%--<%=LC.Std_Study%> Administration*****--%>" border=0 ></a>
		  		<%if(studyActualDt!=null /*&& EJBUtil.stringToNum(mPatRight) > 0 */ ){%>
					<a href="studypatients.jsp?srcmenu=tdmenubaritem5&srcmenu=<%=src%>&studyId=<%=studyId%>&patid=&patstatus=&selectedTab=2"><img src="../images/jpg/patient.gif" alt="<%=LC.L_PatMgmt_EnrlPat%><%--<%=LC.Pat_Patient%> Management - Enrolled <%=LC.Pat_Patients%>*****--%>" border=0 ></a>
				<%}%>
				</td>
			</tr>
	    		<%}%>
		</table><%
	    		break;
	    		}
    			break;
    		case 4:
	    		//Payments
	    		//By Payment Date & By Amount%>
				<th width="5%"><%=LC.L_Paid%><%--Paid*****--%></th>
				<th width="2%"><%=LC.L_Payment_Date%><%--Payment Date*****--%></th> 
				<th width="3%"><%=LC.L_Amount%><%--Amount*****--%></th> 
				<th width="5%"><%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%></th>
				<th width="2%"></th>
				<th width="1%"></th>
			</tr>
				<%

				//KM-#4099
				GrpRightsJB grprghtsJB = (GrpRightsJB) tSession.getValue("GRights");		
				mileGrpRight = Integer.parseInt(grprghtsJB.getFtrRightsByValue("MILEST"));
				
		    	for(int counter = 1;counter<=rowsReturned;counter++){
		    		studyId = EJBUtil.stringToNum(br.getBValues(counter,"pk_study"));
		    		int paymentId = EJBUtil.stringToNum(br.getBValues(counter,"pk_milepayment"));
		    		int payWhen = EJBUtil.stringToNum(br.getBValues(counter,"pay_When"));
				    if ((counter%2)==0) { %>
		  	<tr class="browserEvenRow"> 
		    		<%}	else{%>
		  	<tr class="browserOddRow"> 
		    		<%}
		    	switch(payWhen){ 
		    		case 1:%>
	    				<td class="reportGrouping"><%=LC.L_Today%><%--Today*****--%></td>
		    		<%break;
		    		case 2:%>
	    				<td class="reportGrouping"><%=LC.L_Last_7_Days%><%--Last 7 Days*****--%></td>
		    		<%break;
		    		case 3:%>
	    				<td class="reportGrouping"><%=MC.M_Last8_15Days%><%--Last 8 to 15 Days*****--%></td>
		    		<%break;
		    		case 4:%>
	    				<td class="reportGrouping"><%=MC.M_Last16_30Days%><%--Last 16 to 30 Days*****--%></td>
		    		<%break;
		    		default:%>
	    				<td class="reportGrouping">-</td>
		    		<%break;
		    		}%>
		    	<td align="center"> <%=(br.getBValues(counter,"milepayment_date") ==null)?"-":( br.getBValues(counter,"milepayment_date"))%> </td>
		    	<td align="center"> <%=br.getBValues(counter,"milepayment_amt")%> </td>
		    	<td> <%=br.getBValues(counter,"study_number")%> </td>
		    	<td>
					<% if (mileGrpRight >= 4)	{	%>			
					<A href='milepaymentbrowser.jsp?srcmenu=tdMenuBarItem7&selectedTab=4&studyId=<%=studyId%>'><%=LC.L_View_Payment %><%-- View Payment*****--%></A> 
					<%}%>	
					
					</td>
				<td>
	  				<a href="study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId=<%=studyId%>"><img src="../images/jpg/study.gif" alt="<%=LC.L_Std_Admin%><%--<%=LC.Std_Study%> Administration*****--%>" border=0 ></a>
		  		<%if(studyActualDt!=null /*&& EJBUtil.stringToNum(mPatRight) > 0 */ ){%>
					<a href="studypatients.jsp?srcmenu=tdmenubaritem5&srcmenu=<%=src%>&studyId=<%=studyId%>&patid=&patstatus=&selectedTab=2"><img src="../images/jpg/patient.gif" alt="<%=LC.L_PatMgmt_EnrlPat%><%--<%=LC.Pat_Patient%> Management - Enrolled <%=LC.Pat_Patients%>*****--%>" border=0 ></a>
				<%}%>
				</td>
			</tr>
	    		<%}%>
		</table><%
    			break;
	    	}%>
	    	
		<table>
			<tr>
				<td>
				<% if (totalRows > 0) { Object[] arguments = {firstRec,lastRec,totalRows}; %>
					<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%--<font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
				<%} else {%>
					<font class="recNumber"><%=MC.M_NoRecordsFound%><%--No Records Found*****--%></font>	
				<%}%>	
				</td>
			</tr>
		</table>	
		<%} //end of if body for page right
		else{
		%>
			<jsp:include page="accessdenied.jsp" flush="true"/>
		<%} //end of else body for page right
	} //end of if body for session
	else {%>
		<jsp:include page="timeout.html" flush="true"/>
	<%}%>

</body>

 

</html>