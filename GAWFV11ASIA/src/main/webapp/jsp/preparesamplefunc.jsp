<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<%
String prepflag="Y";
System.out.println("prepflag"+prepflag);
String fk_schevents1=request.getParameter("fk_schevents1")==null?"":request.getParameter("fk_schevents1");
String mode= "";
mode = request.getParameter("mode");
String pkey = request.getParameter("pkey")==null?"":request.getParameter("pkey");
String pkstoragekit=request.getParameter("pkstoragekit")==null?"":request.getParameter("pkstoragekit");
String storagedet="";
 String dProcSteps="";
 String dStorageCateg = "", dStorType="", dSpecimenType="", dSpecQuantUnit="",  dDisposition="" ,dStoreChildType="";
 CodeDao cdSpecProcSteps = new CodeDao();//Processing steps
 cdSpecProcSteps.getCodeValues("spec_proctype");
 dProcSteps = cdSpecProcSteps.toPullDown("processingsteps");

 CodeDao cdStorageType = new CodeDao();//storage Type
 cdStorageType.getCodeValuesSaveKit("store_type");
 dStorType = cdStorageType.toPullDown("storageType");

 CodeDao cdSpecType = new CodeDao();//Specimen Type
 cdSpecType.getCodeValues("specimen_type");
 dSpecimenType = cdSpecType.toPullDown("specimentype");

 CodeDao cdSpecQntUnit = new CodeDao();//Specimen Quantity unit
 cdSpecQntUnit.getCodeValues("spec_q_unit");
 dSpecQuantUnit = cdSpecQntUnit.toPullDown("specimenqunit");

 CodeDao cdDisposition = new CodeDao();//Disposition
 cdDisposition.getCodeValues("specdisposition");
 dDisposition = cdDisposition.toPullDown("disposition");

 //JM: 09Oct2009: #4311: to fetch the Enviornmental Constraints in Storage Kit Details from the er_codelst table
 CodeDao cdEcons = new CodeDao();
 cdEcons.getCodeValues("envt_cons");
 ArrayList codelstDescs=cdEcons.getCDesc();
 String codelstdesc="";
 int length=codelstDescs.size();
 String genStatus = "";

%>

<head>
	<title> <%=MC.M_MngInv_StrgKitDets%><%--Manage Inventory >> Storage Kit Details*****--%></title>
 </head>
 <body>


<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="srctdmenubaritem5"/>
<jsp:param name="prepflag" value="<%=prepflag%>"/>
</jsp:include>


<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.StringUtil,com.velos.eres.business.common.*,com.velos.eres.web.storageAllowedItems.* ,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.storageKit.StorageKitJB, com.velos.eres.web.storage.StorageJB, com.velos.eres.web.storageStatus.StorageStatusJB"%>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*" %>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="storageB" scope="request" class="com.velos.eres.web.storage.StorageJB"/>
<jsp:useBean id="storageKitB" scope="request" class="com.velos.eres.web.storage.StorageJB"/>
<jsp:useBean id="StorageStatJB" scope="request" class="com.velos.eres.web.storageStatus.StorageStatusJB"/>
<jsp:useBean id="stdJB" scope="request" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id="usrJB" scope="request" class="com.velos.eres.web.user.UserJB"/>

<%
HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession)) {

	 int pageRight = 0;

	 GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");

	 pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MSTORAGE"));

String selectedTab = request.getParameter("selectedTab");

String pkStorageKit="";
String storageId = "";
String storageName = "";
String storageType = "";
String notes ="";

String  user = "", userName ="";

String mainFKStorage = "";
String mainParentStorageName = "";

String acc = (String) tSession.getValue("accountId");


String chld1_pkStorage = "";
String chld1_pkStorageKit = "";

ArrayList chld1Pk_StorageKits = null;

ArrayList chld1_spec_types = null;
ArrayList chld1_proc_types = null;
ArrayList  chld1_proc_typSeqs = null;
ArrayList  chld1_quants = null;
ArrayList  chld1_quant_units = null;
ArrayList  chld1_disposns = null;
ArrayList  chld1_envt_consts = null;

String chld1_spec_type = "";
String chld1_proc_type = "";
String chld1_proc_typSeq = "";
String chld1_quant = "";
String chld1_quant_unit = "";
String chld1_quant_unitflg = "";
String chld1_disposn = "";
String chld1_envt_const = "";


String chld2_pkStorage = "";
String chld2_pkStorageKit = "";

ArrayList chld2Pk_StorageKits = null;
ArrayList chld2_spec_types = new ArrayList();
ArrayList chld2_proc_types = new ArrayList();
ArrayList  chld2_proc_typSeqs = new ArrayList();
ArrayList  chld2_quants = new ArrayList();
ArrayList  chld2_quant_units = new ArrayList();
ArrayList  chld2_disposns = new ArrayList();
ArrayList  chld2_envt_consts = new ArrayList();

String chld2_spec_type = "";
String chld2_proc_type = "";
String chld2_proc_typSeq = "";
String chld2_quant = "";
String chld2_quant_unit = "";
String chld2_disposn = "";
String chld2_envt_const = "";

String ParentKitName= "";

 CodeDao cdStorCateg= new CodeDao();//category
 cdStorCateg.getCodeValues("storecategory");
 dStorageCateg = cdStorCateg.toPullDown("storecate");
 String storecateg = "";
 //Added by Bikash for Invp 2.8 Storage Child Type 
 
 CodeDao cdStoreChildType= new CodeDao();//child  storage type
 //removed kit from kit storage type
 cdStoreChildType.getCodeValuesSaveKit("store_type");
 dStoreChildType = cdStoreChildType.toPullDown("storeChildType");
 String storeChildType = "";

String add_more = request.getParameter("addmore");
add_more = (add_more==null)?"1":add_more;


String counter_more = request.getParameter("counterBox");
counter_more = (counter_more==null)?"":counter_more;


String counter_more1 = request.getParameter("counterBox");
counter_more1 = (counter_more1==null)?"":counter_more1;

pkStorageKit = request.getParameter("pkstoragekit");

   if(mode.equals("M")) {

	storageB.setPkStorage(EJBUtil.stringToNum(pkStorageKit));
	storageB.getStorageDetails();


	storageId = storageB.getStorageId();
	storageName = storageB.getStorageName();
	storageType = storageB.getFkCodelstStorageType();
	storecateg = storageB.getStorageKitCategory();

	notes = storageB.getStorageNotes();
	mainFKStorage = storageB.getFkStorage();
		
	storeChildType = storageB.getFkCodelstChildStype();

}



StorageDao storDao = storageKitB.getAllChildren(EJBUtil.stringToNum(pkStorageKit));
ArrayList pk_Storages = storDao.getPkStorages();
ArrayList kit_names = storDao.getStorageNames();
ArrayList kit_storage_types = storDao.getFkCodelstStorageTypes();
ArrayList fk_Storages = storDao.getFkStorages();
//ArrayList quantity = storDao.getDefSpecQuants();



int len = pk_Storages.size();



int generalCnt = 0;
%>
<div class="tabDefBotN" id="div1" style="margin-top:-8%">

<Form name="prepareSamplesForm" id="prepareSamplesForm" method="post" onsubmit="if (setVals(document.prepareSamplesForm)== false) {setValidateFlag('false'); return false;} else {setValidateFlag('true'); return true;}">

 <input type="hidden" name="mode" value=<%=mode%>>
 <input type="hidden" name="pkStorageKit" value=<%=pkStorageKit%>>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<br>
<input type="hidden" name="counterBox" value="" size="2">
<tr>
<td ><P class = "sectionHeadings"> <%=LC.L_PrepSamp%><%--Prepare Samples*****--%></p></td>
</tr>
</table>
<table class=tableDefault id="prepSampleTable" width="100%" border=0 >
<tr>
	<td><%=LC.L_PerpSamp_All %><input type="checkbox" name="preparesmpls" id="preparesmpls" onclick="selectAll();" ></td>
	<td><%=LC.L_MEvntDone %><input type="checkbox" name="mrkevtdone" id="mrkevtdone"></td>
	<td><%=LC.L_Status_Date %> &nbsp;&nbsp;<input type="text" class="datefield" name="statusdate" id="statusdate"/></td>
</tr>
</table>
<table class=tableDefault id="myMainTable" width="100%" border=0 >
<TR id="myiRow0">
    <th width=10% align =center><%=LC.L_Select%><%--Name*****--%></th>
   <th width=10% align =center><%=LC.L_Name%><%--Name*****--%></th>
     <th width=20% align =center><%=LC.L_Storage_Type%><%--Storage Type*****--%></FONT></th>
     <th width=20%  align =center><%=LC.L_Specimen_Type%><%--Specimen Type*****--%></th>
     <th width=20% align =center colspan=2><%=LC.L_ExptQuant%><%--Quantity*****--%></th>
     <th width=20% align =center colspan=2><%=LC.L_App_CCQ%><%--Quantity*****--%></th>
</TR>

<%
String chkboxvalue="";
String abc="";
for(int i=0;i<len;i++){
	storagedet=storagedet +"," + pkstoragekit+"abc"+kit_storage_types.get(i).toString()+"abc"+"Y";
}

storagedet=storagedet.substring(1);
System.out.println("storagedet="+storagedet);


int baseOginalRows = 0;
int rowCount = 0;
baseOginalRows = 2;

String numExi = "";
numExi = request.getParameter("totalExtgRows");
numExi = (numExi ==null)?"2":numExi ;


%>

<input type = "hidden" name="storagedet" id="storagedet" value="<%=storagedet%>"/>
<input type = "hidden" name="totalExtgRows" value=<%=len%>>

<input type = "hidden" name="baseOrigRows" value=<%=len%>>

<input type = "hidden" name="oldExtgRows" value=<%=len%>>
<%

for (int n=0 ; n < len; n ++){

	if ((fk_Storages.get(n).toString()).equals(pkStorageKit)){
      rowCount ++;
//find out the details based upon the parameter and assign to display in edit mode

		StorageJB strChld1B = new StorageJB();
		SpecimenDao spdao = new SpecimenDao();
		chld1_pkStorage  = pk_Storages.get(n).toString();
		int chkdComp = spdao.getPrepSpcmnStoragId(EJBUtil.stringToNum(chld1_pkStorage),EJBUtil.stringToNum(fk_schevents1));
		strChld1B.setPkStorage(EJBUtil.stringToNum(chld1_pkStorage));
		strChld1B.getStorageDetails();

		storageName = strChld1B.getStorageName();
		ParentKitName = strChld1B.getStorageName();
		storageType = strChld1B.getFkCodelstStorageType();


		StorageKitJB strKitBChld1 = new StorageKitJB();
		StorageKitDao skChld1Dao = new StorageKitDao();
		 skChld1Dao= strKitBChld1.getStorageKitAttributes(EJBUtil.stringToNum(chld1_pkStorage));


		//skChld1Dao.getFkStorages();
		chld1Pk_StorageKits = skChld1Dao.getPkStorageKits();//need for edit mode calculation
		chld1_spec_types = skChld1Dao.getDefSpecimenTypes();
		chld1_proc_types = skChld1Dao.getDefProcTypes();//',' separated values
		chld1_proc_typSeqs = skChld1Dao.getDefProcSeqs();//',' separated values
		chld1_quants = skChld1Dao.getDefSpecQuants();
		chld1_quant_units = skChld1Dao.getDefSpecQuantUnits();
		chld1_disposns =  skChld1Dao.getKitSpecDispositions();
		chld1_envt_consts = skChld1Dao.getKitSpecEnvtCons();//Not ',' separated values


		chld1_pkStorageKit=((chld1Pk_StorageKits.get(0)) == null)?"":(chld1Pk_StorageKits.get(0)).toString();
		chld1_spec_type=((chld1_spec_types.get(0)) == null)?"":(chld1_spec_types.get(0)).toString();
		chld1_quant=((chld1_quants.get(0)) == null)?"":(chld1_quants.get(0)).toString();
		chld1_quant_unit=((chld1_quant_units.get(0)) == null)?"":(chld1_quant_units.get(0)).toString();
		if(chld1_quant_unit.equalsIgnoreCase("0")){
			chld1_quant_unitflg="Y";
		}
		else{
				chld1_quant_unitflg=chld1_quant_unit;
		}
	    
		chld1_proc_type=((chld1_proc_types.get(0)) == null)?"":(chld1_proc_types.get(0)).toString();
		chld1_proc_typSeq=((chld1_proc_typSeqs.get(0)) == null)?"":(chld1_proc_typSeqs.get(0)).toString();
		chld1_disposn=((chld1_disposns.get(0)) == null)?"":(chld1_disposns.get(0)).toString();
		chld1_envt_const=((chld1_envt_consts.get(0)) == null)?"":(chld1_envt_consts.get(0)).toString();


if (n>0){
generalCnt++;
%>



<!--Level-1 kits will lie here-->

<TR>
<TD colspan=10><hr/><INPUT type="checkbox" name="hiddenChk" style="visibility:hidden"/></TD>
</TR>


<%
//System.out.println("********n****"+n+ ":::"+ generalCnt + "::"+pk_Storages.get(n).toString());
}%>

<tr id="myiRow<%=n+1%>" >
<td>
<%if(!StringUtil.integerToString(chkdComp).equals(pk_Storages.get(n).toString())){ %>
<INPUT type="checkbox" name="selctprep" id="<%=pk_Storages.get(n).toString()+"_"+chld1_spec_type+"_"+chld1_quant+"_"+chld1_quant_unitflg%>" value="<%=pk_Storages.get(n).toString()+"_"+chld1_spec_type+"_"+chld1_quant+"_"+chld1_quant_unitflg%>"/>
<%} %>
</td>
<td>
<input type="text" name="kitCompName" value="<%=storageName%>" size="10" readonly disabled>
</td>

<input type="hidden" name="idsStOrages" value="<%=pk_Storages.get(n).toString()%>" >
<input type="hidden" name="idsKits" value="<%=chld1_pkStorageKit%>" >

<SCRIPT language="javascript">
function setVals(formobj)
{	
	var printIndices='';
	printIndices = "[INDEXSEP]0[INDEXSEP]";
		callAjaxForPrepare(printIndices);
		return true;	

}

function selectAll(){
	var items=document.getElementsByName('selctprep');
	if(jQuery("#preparesmpls").is(':checked')){
		for(var i=0; i<items.length; i++){
			if(items[i].type=='checkbox')
				items[i].checked=true;
		}
	}
	else{
		for(var i=0; i<items.length; i++){
			if(items[i].type=='checkbox')
				items[i].checked=false;
		}
	}
	
}
function callAjaxForPrepare(printIndices) {	
	var fk_schevents1='<%=fk_schevents1%>';
	var pkStorageKit='<%=pkStorageKit%>';

	var fieldActionIdAp ='';
	var fieldActionId ='';
	$j('input:checkbox[name=aplycolltocolndcurr]').each(function() {
		if($j(this).is(':checked')) {				    	
			fieldActionIdAp += $j(this).val()+",";			  
		      }
	});
	$j('input:checkbox[name=selctprep]').each(function() {    
		if($j(this).is(':checked')) {				    	
			fieldActionId += $j(this).val()+",";			  
			  }
	});
	
	
	fieldActionIdAp=fieldActionIdAp.substring(0,fieldActionIdAp.length-1);
	fieldActionId=fieldActionId.substring(0,fieldActionId.length-1);
	if(fieldActionId==''){
         alert("Please select atleast one 'Select' checkbox to continue");
         return false;
		}
	
	if(fieldActionIdAp.trim()!=''){
		var fieldActionIdAp_array = fieldActionIdAp.split(',');
		for(i = 0; i < fieldActionIdAp_array.length; i++){
			var amt=0.0;
			if(fieldActionId.indexOf(fieldActionIdAp_array[i])>=0){
				$j(".child_of_"+fieldActionIdAp_array[i]).each(function(){
		        amt=amt+parseFloat($j(this).val());
		    	});
				var parentAmt = $j("#parent_"+fieldActionIdAp_array[i]).val();
				amt = parseFloat(parentAmt)-amt;
				if(amt<0){
					alert("<%=MC.M_Update_Expected_Qnty%>");
					return false;
				}
			}
	  	}
	}
	
if(jQuery("#mrkevtdone").is(':checked')){
	var statusdate = document.getElementById('statusdate').value;
	if(statusdate == ''){
		alert("Please enter Status Date to Mark Event as Done");
		return false;
	}
	var retVal = confirm("Samples will be prepared based on the selected components. The event will be marked as 'Done'?");
    if( retVal == true ){
       callAjaxForMarkDone(fk_schevents1,statusdate);
    }
   // else{
   //    return false;
   // }
	
}
	jQuery.ajax({
		url:'updatePrepareSpecimen.jsp?fk_schevents1='+fk_schevents1+'&pkStorageKit='+pkStorageKit+'&fieldActionId='+fieldActionId+'&fieldActionIdAp='+fieldActionIdAp+'&prepflag=Y',
		type: "POST",
		async:false,
		data:"printIndices="+printIndices,
		success:(function (data){			
		}),
		error:function(response) { alert(data); return false; }
	});
	alert("Samples prepared Successfully");
	window.opener.location.reload();
   	setTimeout("self.close()",1000);
}

function callAjaxForMarkDone(fk_schevents1,statusdate){
	var pkey = '<%=pkey%>';
	var mode = 'S';
	jQuery.ajax({
		url:'promptMarkEventDone.jsp?prepflag="Y"&fk_sch_eventid='+fk_schevents1+'&statusdate='+statusdate+'&pkey='+pkey+'&mode='+mode,
		type: "POST",
		async:false,
		data:"mode="+mode,
		success:(function (data){			
		}),
		error:function(response) { alert(data); return false; }
	});
}
</SCRIPT>

<% if (mode.equals("N")){ %>
	<td class=tdDefault><%=dStorType%></td>
<%}else {
	dStorType = cdStorageType.toPullDownPrep("storageType",EJBUtil.stringToNum(storageType));
%>
	<td class=tdDefault><%=dStorType%></td>
<%}%>

<% if (mode.equals("N")){ %>
	<td class=tdDefault><%=dSpecimenType%></td>
<%}else {
	dSpecimenType = cdSpecType.toPullDownPrep("specimentype",EJBUtil.stringToNum(chld1_spec_type));
%>
	<td class=tdDefault><%=dSpecimenType%></td>
<%}%>
<td>
<input type="text" name="quantity" value="<%=chld1_quant%>" size="05" readonly disabled>
<input type="hidden" id="<%="parent_"+pk_Storages.get(n).toString()%>" value="<%=chld1_quant%>"/>
</td>
<% if (mode.equals("N")){ %>
	<td class=tdDefault><%=dSpecQuantUnit%></td>
<%}else {
	dSpecQuantUnit = cdSpecQntUnit.toPullDownPrep("specimenqunit",EJBUtil.stringToNum(chld1_quant_unit));
%>
	<td class=tdDefault><%=dSpecQuantUnit%></td>
<%}%>

<!--Processing Steps and sequence----------------->
<td>
	<input type="checkbox" name="aplycolltocolndcurr" id="<%=pk_Storages.get(n).toString()+"_A"%>" value="<%=pk_Storages.get(n).toString()%>">	
</td>

<input type="hidden" name="envtStringChecked" value="<%=chld1_envt_const.charAt(0)%><%=chld1_envt_const.charAt(1)%><%=chld1_envt_const.charAt(2)%>" >

<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td>
<td>
<input type="hidden" name="numSpecChildren" value="" size="03">
</td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input type="hidden" name="checkCopyChild" id="checkCopyChild" onClick="fnChk(document.storagekit)">
<input type="hidden" name="checkCopyChildStr">


<INPUT type="checkbox" name="hiddenChk" style="visibility:hidden"/>


</td>



</tr>



<%}

int dispCounter = 0;
int childCount = 0;
int cnt = 0;
while(cnt<len){
	if ((pk_Storages.get(n).toString()).equals(fk_Storages.get(cnt).toString())){
		childCount++;
	}
		cnt ++;
	
}
%>
<input type="hidden" name="childCount" value="<%=childCount%>">
<%
//-----------------------Level-2 kit will lie here------------------------------------------
	for (int z=1 ; z < len; z ++){

		if ((pk_Storages.get(n).toString()).equals(fk_Storages.get(z).toString())){
    rowCount ++;
	dispCounter++;
		//find out the details based upon the parameter and assign to display in edit mode




		StorageJB strchld2B = new StorageJB();


		 chld2_pkStorage  = pk_Storages.get(z).toString();

		strchld2B.setPkStorage(EJBUtil.stringToNum(chld2_pkStorage));
		strchld2B.getStorageDetails();

		storageName = strchld2B.getStorageName();
		storageType = strchld2B.getFkCodelstStorageType();


		StorageKitJB strKitBchld2 = new StorageKitJB();
		StorageKitDao skChld2Dao = new StorageKitDao();
			skChld2Dao = strKitBchld2.getStorageKitAttributes(EJBUtil.stringToNum(chld2_pkStorage));




		chld2Pk_StorageKits = skChld2Dao.getPkStorageKits();
		chld2_spec_types = skChld2Dao.getDefSpecimenTypes();
		chld2_proc_types = skChld2Dao.getDefProcTypes();
		chld2_proc_typSeqs = skChld2Dao.getDefProcSeqs();
		chld2_quants = skChld2Dao.getDefSpecQuants();
		chld2_quant_units = skChld2Dao.getDefSpecQuantUnits();
		chld2_disposns =  skChld2Dao.getKitSpecDispositions();
		chld2_envt_consts =  skChld2Dao.getKitSpecEnvtCons();


		chld2_pkStorageKit=((chld2Pk_StorageKits.get(0)) == null)?"":(chld2Pk_StorageKits.get(0)).toString();
		chld2_spec_type=((chld2_spec_types.get(0)) == null)?"":(chld2_spec_types.get(0)).toString();
		chld2_quant=((chld2_quants.get(0)) == null)?"":(chld2_quants.get(0)).toString();
		chld2_quant_unit=((chld2_quant_units.get(0)) == null)?"":(chld2_quant_units.get(0)).toString();
		chld2_proc_type=((chld2_proc_types.get(0)) == null)?"":(chld2_proc_types.get(0)).toString();
		chld2_proc_typSeq=((chld2_proc_typSeqs.get(0)) == null)?"":(chld2_proc_typSeqs.get(0)).toString();
		chld2_disposn=((chld2_disposns.get(0)) == null)?"":(chld2_disposns.get(0)).toString();
		chld2_envt_const=((chld2_envt_consts.get(0)) == null)?"":(chld2_envt_consts.get(0)).toString();



		%><!----------------------------level-2 children will go here----------------------------------------->
<%if (dispCounter==1){%>
<td>&nbsp;&nbsp;</td>
<td colspan="50%">
<p class = "defComments"> <%=LC.L_Child_SpecimenFor%><%--Child Specimen for*****--%> <%=ParentKitName%></p>
<INPUT type="checkbox" name="hiddenChk" style="visibility:hidden"/>
</td>



<%
generalCnt++;

}

//System.out.println("********z****"+z+ ":::"+ generalCnt + "::"+pk_Storages.get(z).toString());

%>
<tr id="myiRow<%=z%>" bgcolor="#ccccc0">
<td>
</td>
<td>
<input type="text" name="kitCompName" value="<%=storageName%>" size="10" readonly disabled>
</td>

<input type="hidden" name="idsStOrages" value="<%=pk_Storages.get(z).toString()%>" >
<input type="hidden" name="idsKits" value="<%=chld2_pkStorageKit%>" >

<% if (mode.equals("N")){ %>
	<td class=tdDefault><%=dStorType%></td>
<%}else {
	dStorType = cdStorageType.toPullDownPrep("storageType",EJBUtil.stringToNum(storageType));
%>
	<td class=tdDefault disabled><%=dStorType%></td>
<%}%>

<% if (mode.equals("N")){ %>
	<td class=tdDefault><%=dSpecimenType%></td>
<%}else {
	dSpecimenType = cdSpecType.toPullDownPrep("specimentype",EJBUtil.stringToNum(chld2_spec_type));
%>
	<td class=tdDefault><%=dSpecimenType%></td>
<%}%>
<td>
<input type="text" name="quantity" value="<%=chld2_quant%>" size="05" disabled>
<input type="hidden" class="child_of_<%=pk_Storages.get(n).toString()%>" id="<%="child_"+pk_Storages.get(z)%>" value="<%=chld2_quant%>"/>
</td>
<% if (mode.equals("N")){ %>
	<td class=tdDefault><%=dSpecQuantUnit%></td>
<%}else {
	dSpecQuantUnit = cdSpecQntUnit.toPullDownPrep("specimenqunit",EJBUtil.stringToNum(chld2_quant_unit));
%>
	<td class=tdDefault><%=dSpecQuantUnit%></td>
<%}%>

<!--Processing Steps and sequence----------------->
<td>
	 

<%String baseVal="2";

baseVal = request.getParameter("numDropDownPerRow");
baseVal = (baseVal ==null)?"2":baseVal ;

StringTokenizer idDropDownSt=new StringTokenizer(chld2_proc_type,",");
StringTokenizer idSeqSt=new StringTokenizer(chld2_proc_typSeq,",");


int indivIds =idDropDownSt.countTokens();
String procTypeStr = "" ;
String procSeq= "";
String envtConsStr= "";


if (indivIds==0){
	baseVal = baseVal;
}else{
	baseVal=indivIds+"";
}

%>
<input type="hidden" name="numDropDownPerRow" value="<%=baseVal%>">
<DIV id = "divprocess<%=rowCount-1%>">
<table id="myTable<%=z%>" name="myTable<%=z%>" border="0">
<%
for (int q= 0 ; q<EJBUtil.stringToNum(baseVal); q++){
	procTypeStr = idDropDownSt.nextToken();
	procSeq = idSeqSt.nextToken();
}%>
</table>
</DIV>
</td>
<input type="hidden" name="envtStringChecked" value="<%=chld2_envt_const.charAt(0)%><%=chld2_envt_const.charAt(1)%><%=chld2_envt_const.charAt(2)%>" >
<td>
<input type="hidden" name="numSpecChildren" value="" size="03">
</td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input type="hidden" name="checkCopyChild" id="checkCopyChild" onClick="fnChk(document.storagekit)">
<input type="hidden" name="checkCopyChildStr">

<INPUT type="checkbox" name="hiddenChk" style="visibility:hidden"/>
</td>
</tr>
		<%
		}
	}
}
%>
<input type="hidden" name="generalCnt" value="<%=generalCnt%>" >

<Input type = "hidden" name="lengthOfLoop" value="<%=len%>">

<Input type = "hidden" name="storageId" >
<Input type = "hidden" name="storageName">

</table>
<% if ( pageRight >=5  )
 {
%>
<BR>
<table width="98%" cellspacing="0" cellpadding="0">
<tr valign="center">
			<td width=35%>&nbsp;</td>
			<td>
<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="N"/>
		<jsp:param name="formID" value="storagekitid"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>
</td>
<td>
		<button onclick= "javascript:self.close()"><%=LC.L_Close%></button>
      </td>
      <td width=35%>&nbsp;</td>
		</tr>
		</table>
<% } %>
<!-- Bug#6320 10-Sep-2012 -Sudhir-->
<Input type = "hidden" name="rowId" value="<%=len%>" >
</Form>



<%
}//end of if body for session
	else {
%>
  <jsp:include page="timeout.html" flush="true"/>
<% } %>
	<div class = "myHomebottomPanel">
	    <jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</div>
</body>

</html>
<SCRIPT LANGUAGE="JavaScript">
	if(screenWidth>1280 || screenHeight>1024)
		$j("#div1").attr("style","height:80%;");
</SCRIPT>