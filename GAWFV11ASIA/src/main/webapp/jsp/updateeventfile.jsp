<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<Link Rel=STYLESHEET HREF="common.css" type=text/css>

</HEAD>



<jsp:include page="skinChoser.jsp" flush="true"/>

<BODY>
<%--
<DIV class="browserDefault" id = "div1"> 
--%>

<jsp:useBean id="eventdocB" scope="request" class="com.velos.esch.web.eventdoc.EventdocJB"/>
  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
  <%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*"%>
  <%

String	src = request.getParameter("srcmenu");



HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession))

   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%   
	String tab = request.getParameter("selectedTab");
	String docId = request.getParameter("docId");
	String desc = request.getParameter("desc");
	String version = request.getParameter("version")==null?"":request.getParameter("version");
	String eSign = request.getParameter("eSign");
	
	String docType = request.getParameter("docType");
	String docmode = request.getParameter("docmode");
	String eventId = request.getParameter("eventId");
	String duration = request.getParameter("duration");
	String protocolId = request.getParameter("protocolId");
	String calledFrom = request.getParameter("calledFrom");
	String mode = request.getParameter("mode");
	String fromPage = request.getParameter("fromPage");
	String calStatus = request.getParameter("calStatus");
	String eventmode = request.getParameter("eventmode");
	String displayType = request.getParameter("displayType");
	String displayDur = request.getParameter("displayDur");
	String networkId = request.getParameter("networkId");
	networkId = (networkId==null)?"":networkId;
	String siteId = (request.getParameter("siteId")==null)?"":request.getParameter("siteId");
	String userPk = (request.getParameter("userPk")==null)?"":request.getParameter("userPk");
	String selectedTab = (request.getParameter("selectedTab")==null)?"":request.getParameter("selectedTab");
	String networkFlag = request.getParameter("networkFlag");
	networkFlag = (networkFlag==null)?"":networkFlag;


	String oldESign = (String) tSession.getValue("eSign");
	//SV, 10/29, last of the files for propagation
	String propagateInVisitFlag = request.getParameter("propagateInVisitFlag"); //SV, 10/12/04, cal-enh-05
	String propagateInEventFlag = request.getParameter("propagateInEventFlag");
	String eventName = request.getParameter("eventName");
	String calAssoc = StringUtil.htmlEncodeXss(StringUtil.trueValue(request.getParameter("calassoc")));


	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {
	
	String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = null;
	usr = (String) tSession.getValue("userId");


	eventdocB.setDocId(EJBUtil.stringToNum(docId));
	eventdocB.getEventdocDetails();
	eventdocB.setDocDesc(desc);
	eventdocB.setModifiedBy(usr);
	eventdocB.setIpAdd(ipAdd);
	eventdocB.setDocVersion(version);
	eventdocB.updateEventdoc();
	
	

//	if ( calledFrom.equals("P") || calledFrom.equals("S")) {
		// SV, Cal-enh - 05, now check if the user wishes to propagate these changes to other events.
		if(!calledFrom.equals("NetWork")){
		
			if ((propagateInVisitFlag == null) || (propagateInVisitFlag.equals("")))
				propagateInVisitFlag = "N";

			if ((propagateInEventFlag == null) || (propagateInEventFlag.equals("")))
				propagateInEventFlag = "N";
			
			if ( (propagateInVisitFlag.equals("Y")) || (propagateInEventFlag.equals("Y"))) {
				//docmode = "N" - New/Add, "M"- modify/update, and calType = "P" - protocol calendar, S: study cal
				
				//Event Id is not populated  in eventDocB
				eventdocB.setEventId(eventId);
		   		eventdocB.propagateEventdoc(EJBUtil.stringToNum(protocolId), propagateInVisitFlag, propagateInEventFlag, docmode, calledFrom);  
			}
	}
//}
%>
<br>
<br>
<br>
<br>
<br>
<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%>  </p>

<META HTTP-EQUIV=Refresh CONTENT="1; URL=eventappendix.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&docType=<%=docType%>&docmode=<%=docmode%>&eventId=<%=eventId%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&eventmode=<%=eventmode%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&eventName=<%=eventName%>&calassoc=<%=calAssoc%>&networkId=<%=networkId%>&siteId=<%=siteId%>&networkFlag=<%=networkFlag%>&userPk=<%=userPk%>">

<%

}//end of if for eSign
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

</HTML>