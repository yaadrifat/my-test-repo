<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<Link Rel=STYLESHEET HREF="common.css" type=text/css>

</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
  <jsp:useBean id="crfNotifyB" scope="request" class="com.velos.esch.web.crfNotify.CrfNotifyJB"/>
  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
  <%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.MC"%>
  <%

	String src = null;
	src = request.getParameter("srcmenu");
	
	String eSign = request.getParameter("eSign");
	
	String crfNotifyId = request.getParameter("crfNotifyId");
	String mode = request.getParameter("mode");
	String crfNotifyToId = request.getParameter("crfNotifyToId");
	String protocolId = request.getParameter("protocolId");
	String studyId = request.getParameter("studyId");	
	String crfStat = request.getParameter("crfStat");	
	String globalFlag= request.getParameter("globalFlag");		
	String addFlag=request.getParameter("addFlag");	

	String from = request.getParameter("from");
	String selectedTab = request.getParameter("selectedTab");
	

	HttpSession tSession = request.getSession(true); 

	if (sessionmaint.isValidSession(tSession))
	   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/> 

<%	   
   		String oldESign = (String) tSession.getValue("eSign");
		String eventId = (String) tSession.getValue("eventId");
			if(!oldESign.equals(eSign)) {
		%>
	  	<jsp:include page="incorrectesign.jsp" flush="true"/>	
		<%
		} else {
	

		String ipAdd = (String) tSession.getValue("ipAdd");
		String usr = (String) tSession.getValue("userId");
		%>
	
		<%
		if(mode.equals("N")) {

		crfNotifyB.setCrfNotifyCodelstNotStatId(crfStat);
		crfNotifyB.setCrfNotifyUsersTo(crfNotifyToId);
		crfNotifyB.setCrfNotStudyId(studyId);
		crfNotifyB.setCrfNotProtocolId(protocolId);		
		crfNotifyB.setCreator(usr);
		crfNotifyB.setIpAdd(ipAdd);	
		crfNotifyB.setCrfNotGlobalFlag(globalFlag);
	
		crfNotifyB.setCrfNotifyDetails();		
		}
	
		if(mode.equals("M")) {
		crfNotifyB.setCrfNotifyId(EJBUtil.stringToNum(crfNotifyId));
		crfNotifyB.getCrfNotifyDetails();		
		crfNotifyB.setCrfNotifyCodelstNotStatId(crfStat);
		crfNotifyB.setCrfNotifyUsersTo(crfNotifyToId);
		crfNotifyB.setModifiedBy(usr);
		crfNotifyB.setIpAdd(ipAdd);
		crfNotifyB.updateCrfNotify();
		}		
		%>
<br>
<br>
<br>
<br>
<br>
<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%-- Data was Saved successfully.*****--%> </p>
<%
if(addFlag != null && addFlag.equals("Add"))
{
%>

<META HTTP-EQUIV=Refresh CONTENT="1; URL=studycrfnot.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&from=<%=from%>&mode=N&studyId=<%=studyId %>&globalFlag=<%=globalFlag%>&protocolId=<%=protocolId%>">

<%}%>


<META HTTP-EQUIV=Refresh CONTENT="1; URL=studycrfnotbrowse.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&from=<%=from%>&mode=<%=mode%>&globalFlag=<%=globalFlag%>&studyId=<%=studyId%>&protocolId=<%=protocolId%>">
		
<%
		}//end of if for eSign check
		}//end of if body for session

		else
	{
		%>
	  <jsp:include page="timeout.html" flush="true"/>
	  <%
	}
	%>

</BODY>

</HTML>
