<%
String src = request.getParameter("srcmenu");
String selectedTab = request.getParameter("selectedTab");
String mode = "";
mode = request.getParameter("mode");
String includeMode = request.getParameter("includeMode");

String patCode=request.getParameter("patCode");

patCode = (StringUtil.isEmpty(patCode))? "" : patCode;
String stid= request.getParameter("studyId");
int studyId = StringUtil.stringToNum(stid);
%>
<%if (!"Y".equals(includeMode)){
	int requestPatientId = 0;
	if (mode.equals("M")) {
		requestPatientId = StringUtil.stringToNum(request.getParameter("pkey"));
	}
	%>
	<meta http-equiv="Refresh" content="0; URL=/velos/jsp/patientDemogScreen.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&mode=<%=mode%>&pkey=<%=requestPatientId%>&page=patient&patCode=<%=patCode%>&studyId=<%=studyId%>&includeMode=Y">
	<%
	if (requestPatientId >= 0){
		return;
	}
	%>
<%}%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<!-- Gopu : import the objects for customized field -->
<%@ page import="com.velos.eres.web.user.ConfigFacade,com.velos.eres.web.user.ConfigObject, com.velos.eres.web.user.ConfigDetailsObject"%>
<%@ page language = "java" import = "com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB, com.velos.eres.web.user.UserJB,com.velos.remoteservice.demographics.*"%>

<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="userBFromSession" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="siteB" scope="request" class="com.velos.eres.web.site.SiteJB"/>
<jsp:useBean id="userSiteB" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" />
<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="patFacility" scope="request" class="com.velos.eres.web.patFacility.PatFacilityJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="codelst" scope="page" class="com.velos.eres.web.codelst.CodelstJB" />
<jsp:useBean id="commonB" scope="request" class="com.velos.eres.web.common.CommonJB"/>
<jsp:useBean id ="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id="grpk" scope="page" class="com.velos.eres.business.common.GroupDao" />

<%
String page1 = request.getParameter("page");
%>
<%
	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))	{
		UserJB userProvider = new UserJB();
		String uName = (String) tSession.getAttribute("userName");
		String userIdFromSession = (String) tSession.getAttribute("userId");
		userBFromSession = (UserJB) tSession.getAttribute("currentUser");
		String pageMode = request.getParameter("pageMode");
		if(pageMode == null)
			pageMode = "initial";
		String acc = (String) tSession.getAttribute("accountId");
		int grpid_no = 0;
		grpid_no = grpk.getGrpPk(EJBUtil.stringToNum(acc));
		//System.out.println("grpid_no" + grpid_no);
		
		//modifed by Gopu for MAHI Enahncement on 20th Feb 2005
		//Check for page customized mandatory field exists for the page by calling
		//the method populateObject([AccountId], [module/page]
		//if exists return the mandatory field info in Array List pgCustFields


		ConfigFacade cFacade=ConfigFacade.getConfigFacade();
		HashMap hashPgCustFld = new HashMap();
		ConfigDetailsObject cdoPgField = cFacade.populateObject(Integer.parseInt(acc), "patient");
		//if customized fields exists then put the field name and its value in hashmap
		//with key as the field name and value is the arrayList position at which the values can be referred to
		if (cdoPgField.getPcfField()!= null && cdoPgField.getPcfField().size() > 0) {
			for (int i=0;i<cdoPgField.getPcfField().size();i++){
				hashPgCustFld.put((String)cdoPgField.getPcfField().get(i), String.valueOf(i));
			}
		}

		String disableStr ="";
		String readOnlyStr ="";

		String statDesc = null;
		String statid = null;
		String studyVer = null;
		PatFacilityDao patFacilityDao = new PatFacilityDao();
		int facilityCount = 0;
		statDesc = request.getParameter("statDesc");
		statid = request.getParameter("statid");
		studyVer = request.getParameter("studyVer");
		String from = request.getParameter("from");
		CodeDao  cdOther = new CodeDao();
		int otherDthCause = cdOther.getCodeId("pat_dth_cause","other");
		CodeDao cdperdth = new CodeDao();
		int deadPerStat = cdperdth.getCodeId("patient_status","dead");
		int pageRight = 0;
		String calledFrom = "";
		String parPage = "";
		int userPrimOrg=0;
		int personPK = 0;
		String patientID = "";
		String organization = "";
		String patOrg = "";
		String specialityNames="";
		String specialityIds="";
		String addEthnicityNames="";
		String addRaceNames="";
		String userSpl = "";
		String splAccessRight= "0";
		int orgRight = 0;
		String patientFacilityId = "";
		int usrGroup = 0;
		boolean completeDet = false;
		String inputType = "text";
		String displayStar = "";
		int patDataDetail = 0;
		int deadStatPk = 0;
		int patStatusId = 0;
		boolean withSelect = true;
		String patPrimOrg="";
		String prevPatientId="";
		String othDthCause  = "";
		CodeDao cdDeathCause = new CodeDao();
		String dthCause ="";
		//cdDeathCause.getCodeValues("pat_dth_cause");
		cdDeathCause.getCodeValuesWithoutHide("pat_dth_cause");
		String dDthCause = "";
		personPK = StringUtil.stringToNum(request.getParameter("pkey"));
		int fdaStudy = 0;
		ArrayList<String> userSites=null;
		
		if (!(personPK==0)) {
			person.setPersonPKId(personPK);
			person.getPersonDetails();
			patientID = person.getPersonPId();
			patientID = (patientID==null)?"":patientID;
			prevPatientId=patientID;
			patStatusId= StringUtil.stringToNum(person.getPersonStatus());
			organization =   person.getPersonLocation() ;

			specialityIds = person.getPersonSplAccess();
			if(specialityIds==null || specialityIds.equals("")){
				specialityNames="";
				splAccessRight="1";
			}
			organization = (organization==null)?"":organization;

			patPrimOrg=organization;
			siteB.setSiteId(StringUtil.stringToNum(organization)) ;
			siteB.getSiteDetails();
			patOrg = siteB.getSiteName();
			//check whether the user has right to edit the patients of the patient org
			orgRight = userSiteB.getUserPatientFacilityRight(StringUtil.stringToNum(userIdFromSession),personPK);
			CodeDao cdSpeciality = new CodeDao();
			if(specialityIds==null){
				specialityIds = "";
			}
			if (specialityIds.length()>0) specialityNames=cdSpeciality.getCodeLstNames(specialityIds);
				if(specialityNames.equals("error"))
				{
					specialityNames="";
				}
				userSites = patFacilityDao.getPatientFacilitiesSiteIds(personPK,StringUtil.stringToNum(userIdFromSession));
				patFacilityDao = patFacility.getPatientFacilities(personPK);
				facilityCount = (patFacilityDao.getId()).size();
				/*PatStudyStatDao pdao = new PatStudyStatDao();
				fdaStudy = pdao.getPatientFdaStudies(personPK,StringUtil.stringToNum(userIdFromSession));*/
				fdaStudy = 1;
			}
			if(page1.equals("enrollPatientsearch")){
				patCode=request.getParameter("patCode");
			}
			mode = request.getParameter("mode");
			calledFrom="";
			usrGroup =StringUtil.stringToNum( userBFromSession.getUserGrpDefault());
			if (mode.equals("M")){
					calledFrom = request.getParameter("calledFrom");
					patDataDetail = person.getPatientCompleteDetailsAccessRight( StringUtil.stringToNum(userIdFromSession),usrGroup,personPK );
				if( patDataDetail >= 4 ){
					completeDet = true;
					inputType = "text";
					displayStar = "";
				}else {
					completeDet = false;
					inputType = "hidden";
					displayStar = "<font color = blue>*</font>";
				}
			}
			if (calledFrom == null || calledFrom.equals(""))	{
				calledFrom = "M"; //i.e to edit patient details as maintenance and not from a study
			}
		GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getAttribute("GRights");
		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MPATIENTS"));
		if ((mode.equals("M") && (pageRight >= 4 && orgRight >= 4)) || (mode.equals("N") && (pageRight == 5  || pageRight == 7 ) ))	{
		  String fname = "";
		  String mname = "";
		  String lname = "";
		  String aname = "";
		  String dob = "";
		  String gender = "";
		  String maritalStatus = "";
		  String bloodGroup = "";
		  String ethnicity = "";
		  String race = "";
		  String ssn = "";
		  String regDate = "";
		  String deathDate = "";
		  String regBy = "";
		  String regByName = "";
		  String add1 = "";
		  String add2 = "";
		  String city = "";
		  String state = "";
		  String zip = "";
		  String country = "";
		  String county = "";
		  String bphone = "";
		  String hphone= "";
		  String email = "";
		  String employment = "";
		  String education  = "";
		  String status = "";
		  String notes = "";
		  String dGender = "";
		  String dMarital = "";
		  String dBloodGroup = "";
		  String dEthnicity = "";
		  String dRace = "";
		  String dEmp = "";
		  String dEdu = "";
		  String dStatus = "";
		  String dTimeZone="";
		  String timeZone="";
		  String phyOther="";
		  String orgOther="";
		  String addEthnicityIds="";
		  String addRaceIds="";
		  String accessFlag = "0";
		  String accessString = LC.L_Revoked; /* String accessString = "Revoked"; *****/
		  CodeDao cdGender = new CodeDao();
		  CodeDao cdMaritalStatus = new CodeDao();
		  CodeDao cdBloodGrp = new CodeDao();
		  CodeDao cdEthnicity = new CodeDao();
		  CodeDao cdRace = new CodeDao();
		  CodeDao cdEmployment = new CodeDao();
		  CodeDao cdEducation = new CodeDao();
		  CodeDao cdSurvivalStatus = new CodeDao();
		  CodeDao cdTimeZone = new CodeDao();
		  cdGender.getCodeValues("gender");
		  cdMaritalStatus.getCodeValues("marital_st");
		  cdBloodGrp.getCodeValues("bloodgr");
		  cdEthnicity.getCodeValuesFilterCustom1("ethnicity", "eth_prim");
		  cdRace.getCodeValuesFilterCustom1("race", "race_prim");
		  cdEmployment.getCodeValues("employment");
		  cdEducation.getCodeValues("education");
		  cdSurvivalStatus.getCodeValues("patient_status");
		  cdTimeZone.getTimeZones();
		  //get code pk for patient status dead
		  ArrayList patstatSubTypes = new ArrayList();
		  ArrayList patstatCIds = new ArrayList();
		  //get death status id
		  deadStatPk = deadPerStat;
		 //show sites for which the user has new right i.e right to add a patient
		  UserSiteDao userSiteDao = userSiteB.getSitesWithNewRight(StringUtil.stringToNum(acc),StringUtil.stringToNum(userIdFromSession));
		  ArrayList siteIds = userSiteDao.getUserSiteSiteIds();
		  ArrayList siteDescs = userSiteDao.getUserSiteNames() ;
		  String siteId = "";
		  int len = siteIds.size();
		  boolean exists = false;
		  //Added for July-August'06 Enhancement (U2) - Admin Settings Patient Timezone.
		  String setvalueover="1";
		  String patTz="";
		  String patTzOverride="";
		  //check if the patient organization is already in the dropdown
		  for (int counter = 0; counter <= len-1 ; counter++) {
			siteId =  (String)siteIds.get(counter);
			if (siteId.equals(organization)) {
				exists = true;
				break;
			}
		  }

	    String surStatAtt="";
		String gendAtt ="";
	    String ethAtt ="";
	    String raceAtt ="";
		String maritalAtt="";
		String bloodAtt="";
		String empAtt ="";
		String eduAtt ="";
		String tzAtt ="";

		String SSNNotApplicable="0";//for SM
		String SSNNotApplicableDD="";//for SM

	    if (hashPgCustFld.containsKey("survivestat")) {
		    int fldNumSurStat= Integer.parseInt((String)hashPgCustFld.get("survivestat"));
		    surStatAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumSurStat));
		    if(surStatAtt == null) surStatAtt ="";
	    }


	    if (hashPgCustFld.containsKey("gender")) {
			int fldNumGender= Integer.parseInt((String)hashPgCustFld.get("gender"));
			gendAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumGender));
			if(gendAtt == null) gendAtt ="";
	    }

	   if (hashPgCustFld.containsKey("ethnicity")) {
			int fldNumEth= Integer.parseInt((String)hashPgCustFld.get("ethnicity"));
			ethAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumEth));
			if(ethAtt == null) ethAtt ="";
	   }

	   if (hashPgCustFld.containsKey("race")) {
			int fldNumRace= Integer.parseInt((String)hashPgCustFld.get("race"));
            raceAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumRace));

			if(raceAtt == null) raceAtt ="";
	   }


	   if (hashPgCustFld.containsKey("maritalstat")) {
			int fldNumMarital= Integer.parseInt((String)hashPgCustFld.get("maritalstat"));
            maritalAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumMarital));

			if(maritalAtt == null) maritalAtt ="";
	   }

	   if (hashPgCustFld.containsKey("bloodgrp")) {
			int fldNumBlood = Integer.parseInt((String)hashPgCustFld.get("bloodgrp"));
            bloodAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumBlood));

			if(bloodAtt == null) bloodAtt ="";
	   }


	   if (hashPgCustFld.containsKey("employment")) {
			int fldNumEmp = Integer.parseInt((String)hashPgCustFld.get("employment"));
            empAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumEmp));

			if(empAtt == null) empAtt ="";
	   }

	   if (hashPgCustFld.containsKey("education")) {
			int fldNumEdu = Integer.parseInt((String)hashPgCustFld.get("education"));
            eduAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumEdu));

			if(eduAtt == null) eduAtt ="";
	   }


	   if (hashPgCustFld.containsKey("timezone")) {
			int fldNumTz = Integer.parseInt((String)hashPgCustFld.get("timezone"));
            tzAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumTz));

		    if(tzAtt == null) tzAtt ="";
	   }



	   if (mode.equals("M")) {

		   	   SSNNotApplicable = person.getPersonMultiBirth();//for SM
		   	   String SSNNotApplicableSelectedYes="";
		   	   String SSNNotApplicableSelectedNo="";

		   	   if (StringUtil.isEmpty(SSNNotApplicable))
		   	   {

		   		   SSNNotApplicable = "0";

		   	   }

		   	   if ( SSNNotApplicable.equals("0")){
		   			SSNNotApplicableSelectedNo=" SELECTED ";
		   	   } else{
		   			SSNNotApplicableSelectedYes = " SELECTED ";
		   	   }

		   	   //for SM
		   	  SSNNotApplicableDD="<Select name=\"SSN_NA\" onChange=f_SSN_NA(this.value)><option "+SSNNotApplicableSelectedYes+" value=\"1\">"+LC.L_Yes/*Yes*****/+"</option><option value = \"0\" "+ SSNNotApplicableSelectedNo+ ">"+LC.L_No/*No*****/+"</option></Select>";

			   bloodGroup =  person.getPersonBloodGr();
			   bloodGroup = (bloodGroup==null)?"":bloodGroup;

				//FIX #6190 --START
				if (completeDet){
					dob =  person.getPersonDob();
					dob = (dob==null)?"":dob;
					fname  =  person.getPersonFname();
					fname = (fname==null)?"":fname;
					lname =  person.getPersonLname();
					lname = (lname==null)?"":lname;
					mname =   person.getPersonMname();
					mname = (mname==null)?"":mname;
					ssn  =  person.getPersonSSN();
					ssn = (ssn==null)?"":ssn;
					add1  =  person.getPersonAddress1();
					add1 = (add1==null)?"":add1;
					add2 =  person.getPersonAddress2();
					add2 = (add2==null)?"":add2;
					city =	person.getPersonCity();
					city = (city==null)?"":city;
					country = person.getPersonCountry();
					country = (country==null)?"":country;
					county = person.getPersonCounty();
					county = (county==null)?"":county;
					email	= person.getPersonEmail();
					email = (email==null)?"":email;
					hphone = person.getPersonHphone();
					hphone = (hphone==null)?"":hphone;
					bphone = person.getPersonBphone();
					bphone = (bphone==null)?"":bphone;
					state	= person.getPersonState();
					state = (state==null)?"":state;
					zip =	person.getPersonZip();
					zip = (zip==null)?"":zip;
					deathDate = person.getPersonDeathDate();
					deathDate = (deathDate==null)?"":deathDate;
					
					patientFacilityId = person.getPatientFacilityId();
					if (StringUtil.isEmpty(patientFacilityId ))
					patientFacilityId  = "";
				}
				//FIX #6190 --END
				
			   aname = person.getPersonAlias();
			   aname = (aname==null)?"":aname;
			   education =   person.getPersonEducation();
			   education = (education==null)?"":education;
			   employment  =  person.getPersonEmployment();
			   employment = (employment==null)?"":employment;
			   gender  =  person.getPersonGender();
			   gender = (gender==null)?"":gender;
			   maritalStatus =   person.getPersonMarital();
			   maritalStatus = (maritalStatus==null)?"":maritalStatus;
			   notes =   person.getPersonNotes();
			   notes = (notes==null)?"":notes.trim();
			   ethnicity =  person.getPersonEthnicity();
			   ethnicity = (ethnicity==null)?"":ethnicity;
			   race =  person.getPersonRace();
			   race = (race==null)?"":race;
			   dthCause = person.getPatDthCause();
			   dthCause = (dthCause==null)?"":dthCause;

				if(pageMode.equals("final")){
					 dthCause = request.getParameter("dthCause");
					 organization = request.getParameter("patorganization");
					 dGender = request.getParameter("dGender");
					 patientID = request.getParameter("patID");
				}
				addEthnicityIds = person.getPersonAddEthnicity();
				if (addEthnicityIds==null) addEthnicityIds="";
				if(addEthnicityIds.equals("")){
					addEthnicityNames="";
				}
				if (addEthnicityIds.length()>0) addEthnicityNames = cdEthnicity.getCodeLstNames(addEthnicityIds);
				if(addEthnicityNames.equals("error")){
				  addEthnicityNames="";
				}
				addRaceIds = person.getPersonAddRace();
				if (addRaceIds==null) addRaceIds="";
				if(addRaceIds.equals("")){
					addRaceNames="";
				}
				if (addRaceIds.length()>0) addRaceNames = cdRace.getCodeLstNames(addRaceIds);
				if(addRaceNames.equals("error")){
					addRaceNames="";
				}
				
				status =   person.getPersonStatus();
				status = (status==null)?"":status;

				if(tzAtt.equals("1") || tzAtt.equals("2"))
  			      dTimeZone = cdTimeZone.toPullDown("timeZone", StringUtil.stringToNum(timeZone),"disabled");
  		        else
			      dTimeZone = cdTimeZone.toPullDown("timeZone", StringUtil.stringToNum(timeZone));

				//dTimeZone = cdTimeZone.toPullDown("timeZone", StringUtil.stringToNum(timeZone));
				
				regDate = person.getPersonRegDate();
				regDate = (regDate==null)?"":regDate;
				othDthCause = person.getDthCauseOther();
				othDthCause = (othDthCause==null)?"":othDthCause;
				regBy = person.getPersonRegBy();
				regBy = (regBy==null)?"":regBy;
				timeZone=person.getTimeZoneId();
				timeZone = (timeZone==null)?"":timeZone;
				phyOther=person.getPhyOther();
				phyOther=(phyOther==null)?"":phyOther;
				orgOther=person.getOrgOther();
				orgOther=(orgOther==null)?"":orgOther;
				//get reg by name
				if(!regBy.equals("")){
					userProvider.setUserId(StringUtil.stringToNum(regBy));
					userProvider.getUserDetails();
					regByName = userProvider.getUserFirstName() + " " +userProvider.getUserLastName();
				}
				String usrId = (String) tSession.getAttribute("userId");
				String codelstSubType = "";
				userB.setUserId(StringUtil.stringToNum(usrId));
				userB.getUserDetails();
				userSpl= userB.getUserCodelstSpl();
				userSpl=(userSpl==null)?"":userSpl;
				codelst.setClstId(StringUtil.stringToNum(userSpl));
				codelst.getCodelstDetails();
				codelstSubType = codelst.getClstSubType();
				int numIds=0;
				String id="";
				int splRight = 0;
				if(codelstSubType!=null && codelstSubType.equals("adm_sp")){
					splAccessRight = "1";
				}else{
					splRight  = patFacility.getUserFacilitySpecialtyAccessRight(StringUtil.stringToNum(userSpl),personPK );
					if (splRight  >= 1)
					{
						splAccessRight = "1";
					}else{
						splAccessRight = "0";
					}
				}
			/*commented by Sonia Abrol, 10/12/2005, specialty access right is calculated using patFacility.getUserFacilitySpecialtyAccessRight */
			/*if(specialityIds!=null){
				StringTokenizer idsSpl=new StringTokenizer(specialityIds,",");
				numIds=idsSpl.countTokens();

				if(numIds==0)
				{
					splAccessRight = "1";
				}
				else if(codelstSubType!=null && codelstSubType.equals("adm_sp"))
				{
					splAccessRight = "1";
				}
				else
				{
					for(int i=0;i<numIds;i++)
					{
							id=idsSpl.nextToken();
							id=id.trim();
							userSpl=userSpl.trim();

							if(userSpl.equals(id))
							{
								splAccessRight = "1";

							}

					}


				}

			} */

			userPrimOrg = StringUtil.stringToNum(userB.getUserSiteId());

			if(gendAtt.equals("1") || gendAtt.equals("2"))
  			    dGender = cdGender.toPullDown("patgender", StringUtil.stringToNum(gender),"disabled");
  		    else
			    dGender = cdGender.toPullDown("patgender", StringUtil.stringToNum(gender));

			if(maritalAtt.equals("1") || maritalAtt.equals("2"))
  			    dMarital = cdMaritalStatus.toPullDown("patmarital",StringUtil.stringToNum(maritalStatus),"disabled");
  		    else
			    dMarital = cdMaritalStatus.toPullDown("patmarital",StringUtil.stringToNum(maritalStatus));

			if(bloodAtt.equals("1") || bloodAtt.equals("2"))
				dBloodGroup = cdBloodGrp.toPullDown("patblood", StringUtil.stringToNum(bloodGroup),"disabled");
			else
				dBloodGroup = cdBloodGrp.toPullDown("patblood", StringUtil.stringToNum(bloodGroup));

			if(ethAtt.equals("1") || ethAtt.equals("2"))
				dEthnicity = cdEthnicity.toPullDown("patethnicity", StringUtil.stringToNum(ethnicity),"disabled");
			else
	 	        dEthnicity = cdEthnicity.toPullDown("patethnicity", StringUtil.stringToNum(ethnicity));

			if (raceAtt.equals("1") || raceAtt.equals("2"))
				dRace = cdRace.toPullDown("patrace", StringUtil.stringToNum(race),"disabled");
			else
				dRace = cdRace.toPullDown("patrace", StringUtil.stringToNum(race));

			if (empAtt.equals("1") || empAtt.equals("2"))
				dEmp = cdEmployment.toPullDown("patemp", StringUtil.stringToNum(employment),"disabled");
			else
				dEmp = cdEmployment.toPullDown("patemp", StringUtil.stringToNum(employment));

			if (eduAtt.equals("1") || eduAtt.equals("2"))
				dEdu = cdEducation.toPullDown("patedu", StringUtil.stringToNum(education),"disabled");
			else
				dEdu = cdEducation.toPullDown("patedu", StringUtil.stringToNum(education));

			if(surStatAtt.equals("1") || surStatAtt.equals("2"))
  			    dStatus = cdSurvivalStatus.toPullDown("patstatus", StringUtil.stringToNum(status),"disabled");
		  	else
			    dStatus = cdSurvivalStatus.toPullDown("patstatus", StringUtil.stringToNum(status));

			/* Added for July-August'06 Enhancement (U2) - Default account level Admin settings for Session Timeout time, Number of days password will expire, Number of days e-Sign will expire */
			SettingsDao settingsDao=commonB.getSettingsInstance();
			int modname=1;
			String keyword="ACC_PAT_TZ_OVERRIDE";
			/*settingsDao.retrieveSettings(StringUtil.stringToNum(acc),modname,keyword);
			ArrayList setvalueovers=settingsDao.getSettingValue();
			if(setvalueovers!=null && setvalueovers.size()>0){
				setvalueover=(setvalueovers.get(0)==null)?"":(setvalueovers.get(0)).toString();
			}
			if(setvalueover.equals("")){
				dTimeZone = cdTimeZone.toPullDown("timeZone");
			}else{
				dTimeZone = cdTimeZone.toPullDown("timeZone", StringUtil.stringToNum(timeZone));
			}*/

		ArrayList keys=new ArrayList();
		keys.add("ACC_PAT_TZ_OVERRIDE");
		keys.add("ACC_PAT_TZ");
		settingsDao.retrieveSettings(StringUtil.stringToNum(acc),modname,keys);
		ArrayList keywords=settingsDao.getSettingKeyword();
		ArrayList setvalues=settingsDao.getSettingValue();
	if ((keywords!=null) && (keywords.size() >0)){
		for (int i=0;i<keywords.size();i++){
			if (((String)keywords.get(i)).equals("ACC_PAT_TZ")){
				patTz=(String)setvalues.get(i);
				patTz=(patTz==null)?"":patTz;
			} else {
				patTzOverride=(String)setvalues.get(i);
				patTzOverride=(patTzOverride==null)?"":patTzOverride;
			}
		}
	}
		/* Added for July-August'06 Enhancement (U2) - Default account level settings for User Timezone
	   */

	   if (patTzOverride.equals("")){


		  if(tzAtt.equals("1") || tzAtt.equals("2"))
  			  dTimeZone = cdTimeZone.toPullDown("timeZone","disabled");
  		  else
	          dTimeZone = cdTimeZone.toPullDown("timeZone");
	   } else {
		 if (timeZone.equals("")){
			 timeZone=patTz;
		 }
		  if(tzAtt.equals("1") || tzAtt.equals("2"))
  			  dTimeZone = cdTimeZone.toPullDown("timeZone", StringUtil.stringToNum(timeZone),"disabled");
  		  else
	          dTimeZone = cdTimeZone.toPullDown("timeZone", StringUtil.stringToNum(timeZone));

	   }

		%>
		<%
			///////////////////////////////////
			String enrollId = (String) tSession.getAttribute("enrollId");
			if(! page1.equals("enrollPatientsearch") ){
		%>
				<!--Modified by Gopu for fixed the Bug #2300 -->
				<% if((deadStatPk == patStatusId)){	%>
					<div>
					<!--Modified by Parminder Singh for fixed the Bug #10533  -->
					<!--  <br><br><br>				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<FONT class="Mandatory" size="1" color="red"><%=LC.L_Pat_DiedOn%><%--<%=LC.Pat_Patient%> Died on*****--%> <%=deathDate%></font>-->
					</div>
				<%}%>
		<% }	//not from enrollpatient search%>
		
		
		<% } else {
			completeDet = true; //all data is visible
			displayStar = ""; // no star will be visible


			if(gendAtt.equals("1") || gendAtt.equals("2")) //KM
				dGender = cdGender.toPullDown("patgender","disabled");
			else
				dGender = cdGender.toPullDown("patgender");

			if(maritalAtt.equals("1") || maritalAtt.equals("2"))
				dMarital = cdMaritalStatus.toPullDown("patmarital","disabled");
			else
				dMarital = cdMaritalStatus.toPullDown("patmarital");

			dBloodGroup = cdBloodGrp.toPullDown("patblood");

			if(ethAtt.equals("1") || ethAtt.equals("2")) //KM
				dEthnicity = cdEthnicity.toPullDown("patethnicity","disabled");
			else
				dEthnicity = cdEthnicity.toPullDown("patethnicity");

			if(raceAtt.equals("1") || raceAtt.equals("2"))
				dRace = cdRace.toPullDown("patrace","disabled");
			else
				dRace = cdRace.toPullDown("patrace");

			if(empAtt.equals("1") || empAtt.equals("2"))
				dEmp = cdEmployment.toPullDown("patemp","disabled");
			else
				dEmp = cdEmployment.toPullDown("patemp");

			if (eduAtt.equals("1") || eduAtt.equals("2"))
				dEdu = cdEducation.toPullDown("patedu","disabled");
			else
				dEdu = cdEducation.toPullDown("patedu");

			if (surStatAtt.equals("1") || surStatAtt.equals("2"))
  				dStatus = cdSurvivalStatus.toPullDown("patstatus", "disabled");
		  	else
				dStatus = cdSurvivalStatus.toPullDown("patstatus");

			if (tzAtt.equals("1") || tzAtt.equals("2"))
				dTimeZone = cdTimeZone.toPullDown("timeZone","disabled");
			else
				dTimeZone = cdTimeZone.toPullDown("timeZone");

		   }

			//dDthCause = cdDeathCause.toPullDown("dthCause",StringUtil.stringToNum(dthCause),withSelect);
			CodeDao cdaodth = new CodeDao();
			//cdaodth.getCodeValues("pat_dth_cause");
			cdaodth.getCodeValuesWithoutHide("pat_dth_cause");
			// dDthCause = cdao5.toPullDown("dthCause",StringUtil.stringToNum(dthCause),withSelect);
			ArrayList causeId = new ArrayList();
			causeId = cdaodth.getCId();
			ArrayList causeDesc = new ArrayList();
			causeDesc = cdaodth.getCDesc();
			int selDthCause = 0;
			StringBuffer strCause = new StringBuffer();
			Integer dthIdTemp = null;
			int dthIdSelTemp = 0;
			selDthCause = StringUtil.stringToNum(dthCause);
		//JM: 01.31.2006
		//	strCause.append("<SELECT NAME='dthCause' onChange='fAddTxt()'>") ;


		 //KM
		 String codAtt =""; //Added by Manimaran to disable the dropdown based on custom value.
	     if (hashPgCustFld.containsKey("cod")) {

				int fldNumCod= Integer.parseInt((String)hashPgCustFld.get("cod"));
				codAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumCod));

				if(codAtt == null) codAtt ="";
		  }

		  if(codAtt.equals("1") || codAtt.equals("2"))
 		     strCause.append("<SELECT NAME='dthCause' disabled>") ;
		  else
			strCause.append("<SELECT NAME='dthCause' >") ;

	  	 strCause.append("<OPTION value=''>"+LC.L_Select_AnOption/*Select an Option*****/+" </OPTION>") ;
		 for (int dcounter = 0; dcounter  < causeId.size()  ; dcounter++){
				dthIdTemp = (Integer) causeId.get(dcounter);
				if (selDthCause == dthIdTemp.intValue()){
					strCause.append("<OPTION value = "+ causeId.get(dcounter)+" SELECTED  >" + causeDesc.get(dcounter)+ "</OPTION>");
				}else{
					strCause.append("<OPTION value = "+ causeId.get(dcounter)+">" + causeDesc.get(dcounter)+ "</OPTION>");
				}
			}
			strCause.append("</SELECT>");
			dDthCause = strCause.toString();
		%>
<!--Modified by Gopu for fixed the Bug #2300 -->
		<%if(page1.equals("enrollPatientsearch") && (deadStatPk == patStatusId)){%>
			<DIV class="tabDefTop"  id="div1">
				<br><br><br>
			<FONT class="Mandatory" size="1" color="red"><%=LC.L_Pat_DiedOn%><%--<%=LC.Pat_Patient%> Died on*****--%> <%=deathDate%></font>
			</DIV>
		<%}%>
			<input type="hidden" name="srcmenu" Value="<%=src%>">
			<input type="hidden" name="pataccount" Value="<%=acc%>">
			<input type="hidden" name="pkey" Value="<%=personPK%>">
			<input type="hidden" name="page" Value="<%=page1%>">
			<Input type="hidden" id="mode" name="mode" value=<%=mode%>>
			<input type="hidden" name="pageMode" value="final">
			<Input type="hidden" name="patCode" value=<%=patCode%>>
			<Input type="hidden" name="studyId" value=<%=studyId%>>
			<Input type="hidden" name="statDesc" value=<%=statDesc%>>
			<Input type="hidden" name="statid" value=<%=statid%>>
			<Input type="hidden" name="studyVer" value=<%=studyVer%>>
			<Input type="hidden" name="calledFrom" value=<%=calledFrom%>>
			<Input type="hidden" name="deadStatPk" value=<%=deadStatPk%>>
			<input type="hidden" name="selSpecialityIds" value="<%=specialityIds%>">
			<input type="hidden" name="selAddEthnicityIds" value="<%=addEthnicityIds%>">
			<input type="hidden" name="selAddRaceIds" value="<%=addRaceIds%>">
			<input type="hidden" name="splAccessRight" value=<%=splAccessRight%>>
			<input type="hidden" name="from" value=<%=from%>>
			<input type="hidden" name="selectedTab" value=<%=selectedTab%>>
			<input type="hidden" name="page" value=<%=page1%>>
			<input type="hidden" name="pkey" value=<%=personPK%>>
			<input type="hidden" name="deadPerStat" value="<%=deadPerStat%>">
			<input type="hidden" id="siteId" name="siteId" value="<%=patPrimOrg%>">
			<input type="hidden" id="accountId" name="accountId" value="<%=acc%>">
			<input type="hidden" name="facilityCount" value=<%=facilityCount%>><!--JM: 12Dec06-->
			<input type="hidden" id="prevPatientId" name="prevPatientId" value="<%=prevPatientId%>">
			<%// code modified by Gupta Maddala 102407 %>
			  <input type=hidden id="aliasname" name="aliasname" size = 20 MAXLENGTH = 20 value='<%=aname%>' >
			<input type="hidden" id="phDetailI" name="phDetailI" value="<%=completeDet%>"/>
		
		<div class="tmpHeight"></div>
		<table width="99%" cellspacing="0" cellpadding="0"  height="55" border="0" class="basetbl outline midalign">
			<tr>
		<!--KM-->

	<%if (hashPgCustFld.containsKey("patid")) {

				int fldNum= Integer.parseInt((String)hashPgCustFld.get("patid"));
				String patMand = ((String)cdoPgField.getPcfMandatory().get(fldNum));
				String patLable = ((String)cdoPgField.getPcfLabel().get(fldNum));
				String patAtt = ((String)cdoPgField.getPcfAttribute().get(fldNum));

				if(patAtt == null) patAtt ="";
				if(patMand == null) patMand ="";


		if(!patAtt.equals("0")) {
				if(patLable !=null){
			%>
			<td width="20%">
			<%=patLable%>:
			<%} else {%> <td width="20%">
			<%=LC.L_Patient_Id%><%--<%=LC.Pat_Patient%> ID*****--%>:
			<%}
			if (patMand.equals("1")) {
				%> <FONT class="Mandatory" id="pgcustompatid">*  </FONT> &nbsp;&nbsp;&nbsp;&nbsp;
			   <% }
				if(patAtt.equals("1")) {
					disableStr = "disabled class='readonly-input'"; }
  				else if (patAtt.equals("2") ) {
					readOnlyStr = "readonly"; }
				%>
			</td>
			<td colspan="7">
				<input type="text" id="patid" name="patID" size = 20 MAXLENGTH = 20  <%=disableStr%> <%=readOnlyStr%> value='<%=patientID%>'onblur="if ( fnTrimSpaces(document.getElementById('patid').value.toUpperCase() )!= fnTrimSpaces( document.getElementById('prevPatientId').value.toUpperCase() ) ) { patientDetailsFunctions.displayFldValMsg('patient:'+this.id,0,'ajaxPatIdMessage','','<%=MC.M_PatId_AldyExst%><%--This <%=LC.Pat_Patient%> ID already exists for the selected organization or it\'s  parent/child organizations.*****--%>','siteId');} else { setMessageUsingInline('','patid','ajaxPatIdMessage','','')}">&nbsp;&nbsp;
			<%} else { %>
				<input type="hidden" id="patid" name="patID" size = 20 MAXLENGTH = 20  value='<%=patientID%>'/>
			<% } %>
			 </td>
		<%} else {%>
			<td width="20%">
				<%=LC.L_Patient_Id%><%--<%=LC.Pat_Patient%> ID*****--%>:<FONT class="Mandatory"  id="mandpatid" >* </FONT>
			</td>
			<td colspan="7">
				<input type="text" id="patid" name="patID" size = 20 MAXLENGTH = 20 
				<%if(!userBFromSession.getUserGrpDefault().equals(new Integer(grpid_no).toString())){%> 
				readonly
				<%} %>value='<%=patientID%>'onblur="if ( fnTrimSpaces(document.getElementById('patid').value.toUpperCase() )!= fnTrimSpaces( document.getElementById('prevPatientId').value.toUpperCase() ) ) { patientDetailsFunctions.displayFldValMsg('patient:'+this.id,0,'ajaxPatIdMessage','','<%=MC.M_PatId_AldyExst%><%--This <%=LC.Pat_Patient%> ID already exists for the selected organization or it\'s  parent/child organizations.*****--%>','siteId');} else { setMessageUsingInline('','patid','ajaxPatIdMessage','','')}"/>&nbsp;&nbsp;
			</td>
		<%}%>
 		</tr>
		 <tr>
		 <%if (hashPgCustFld.containsKey("survivestat")) {
			int fldNumSurStat= Integer.parseInt((String)hashPgCustFld.get("survivestat"));
			String surStatMand = ((String)cdoPgField.getPcfMandatory().get(fldNumSurStat));
			String surStatLable = ((String)cdoPgField.getPcfLabel().get(fldNumSurStat));
			surStatAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumSurStat));

			if(surStatAtt == null) surStatAtt ="";
			if(surStatMand == null) surStatMand ="";

		if (!surStatAtt.equals("0")) {
			if(surStatLable !=null){
			%><td width="20%">
			<%=surStatLable%>:
			<%} else {%> 
			<td width="20%">
			<%=LC.L_Survival_Status%><%--Survival Status*****--%>:
			<%}

			if (surStatMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatsstate">* </FONT>
			<%}%>
			</td>
			<td >
			<%=dStatus%>
			<%if (surStatAtt.equals("1")) {	%>
				<input type="hidden" name="patstatus" value="">
			<%} else if (surStatAtt.equals("2")){%>
				<input type="hidden" name="patstatus" value="<%=status%>">
			<%} %>
			</td>

		<%} else  {%>
			<input type="hidden" name="patstatus" value="<%=status%>">
		<% }
		} else {%>
	   <td width="20%"> <%=LC.L_Survival_Status%><%--Survival Status*****--%>: <FONT class="Mandatory" id="mandsstate">* </FONT> </td>
	    <td>
		<%=dStatus%>
	    </td>

		<%}%>

		<%if (hashPgCustFld.containsKey("dod")) { //KM

			int fldNumDod= Integer.parseInt((String)hashPgCustFld.get("dod"));

			String dodMand = ((String)cdoPgField.getPcfMandatory().get(fldNumDod));
			String dodLable = ((String)cdoPgField.getPcfLabel().get(fldNumDod));
			String dodAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumDod));


			disableStr ="";
			readOnlyStr ="";

			if(dodAtt == null) dodAtt ="";
			if(dodMand == null) dodMand ="";


			if(!dodAtt.equals("0")) {
			if(dodLable !=null){
			%>  <td width="10%" align="right">
			<%=dodLable%>: &nbsp;
			<%} else {%> <td width="10%">
			 <%=LC.L_Death_Date%><%--Death Date*****--%>: &nbsp;
			<%}
			if (dodMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatdod">* </FONT>
			<%
		 }%>

		 <%if(dodAtt.equals("1")) {
			 disableStr = "disabled class='readonly-input'"; }
		     else if (dodAtt.equals("2") ) {
			 readOnlyStr = "readonly"; }
		 %>

	    </td>
		<td >
			<%-- INF-20084 Datepicker-- AGodara --%>		
			<% if (completeDet){%>
				<%if (StringUtil.isEmpty(dodAtt)){ %>
					<input type="<%=inputType%>" name="patdeathdate" class="datefield" size = 10 MAXLENGTH = 11	value = <%=deathDate%>>
				<%} else if(dodAtt.equals("1") || dodAtt.equals("2")) {%>
					<input type=<%=inputType%> name="patdeathdate" <%=readOnlyStr%> size = 10 <%=disableStr%> MAXLENGTH = 11 value="<%=deathDate%>" ><%=displayStar%>
				<%} %>
			<%}else {%>
				<input type=<%=inputType%> name="patdeathdate" size = 10 readonly MAXLENGTH = 11 value="<%=deathDate%>" ><%=displayStar%>
			<%} %>
		</td>
		<%} else { %>
				<input type=hidden name="patdeathdate" size = 10 MAXLENGTH = 10 value="<%=deathDate%>" >
		<% } 
		} else {%>
			<td   align="right"><%=LC.L_Death_Date%><%--Death Date*****--%>: &nbsp;</td>
			<td >
				<% if (completeDet){%>
						<input type=<%=inputType%> name="patdeathdate" class="datefield" size = 10 MAXLENGTH = 11 value="<%=deathDate%>" > <%=displayStar%>
				<%} else {%>
						<input type=<%=inputType%> name="patdeathdate" readonly size = 10 MAXLENGTH = 11 value="<%=deathDate%>" > <%=displayStar%>
				<% } %>
			</td>
	   <%}%>

		<%if (hashPgCustFld.containsKey("cod")) {
			int fldNumCod= Integer.parseInt((String)hashPgCustFld.get("cod"));

			String codMand = ((String)cdoPgField.getPcfMandatory().get(fldNumCod));
			String codLable = ((String)cdoPgField.getPcfLabel().get(fldNumCod));
			codAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumCod));


			if(codAtt == null) codAtt ="";
			if(codMand == null) codMand ="";


			if(!codAtt.equals("0")) {
			if(codLable !=null){
			%><td width="10%" align="right">
			<%=codLable%>:&nbsp;
			<%} else {%> <td width="10%" align="right">
			 <%=LC.L_Cause%><%--Cause*****--%>:&nbsp;
			<%}
			if (codMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatcod">* </FONT>
			<%
		 }%>
		</td>
		<td>
			<%=dDthCause%> <%if(codAtt.equals("1")) {%> <input type="hidden" name="dthCause" value=""> <%}%><!--KM-->
		</td>

		  <%} else {  %>
		  <input type="hidden" name="dthCause" value=<%=dthCause%> > <!--KM-->
		  <% }
		} else {  //KM-if backend data is not available
		  %>
			 <td width="10%" align="right"><%=LC.L_Cause%><%--Cause*****--%>:&nbsp; </td>
			 <td > <%=dDthCause%></td>
		 <%}%>

		 <%if (hashPgCustFld.containsKey("speccause")) { //KM
			int fldNumSpec= Integer.parseInt((String)hashPgCustFld.get("speccause"));
			String specMand = ((String)cdoPgField.getPcfMandatory().get(fldNumSpec));
			String specLable = ((String)cdoPgField.getPcfLabel().get(fldNumSpec));
			String specAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumSpec));

			disableStr ="";
			readOnlyStr ="";
			if(specAtt == null) specAtt ="";
			if(specMand == null) specMand ="";

			if(!specAtt.equals("0")) {
			if(specLable !=null){
			%>  <td  align="right">
			<%=specLable%>: &nbsp;
			<%} else {%> <td  align="right">
			  <%=LC.L_Specify%><%--Specify*****--%>:&nbsp;
			<%}
			if (specMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatspeccause">* </FONT>
			<%
			} 
			
			if(specAtt.equals("1")) {
				disableStr = "disabled class='readonly-input'"; 
			} else if (specAtt.equals("2")) {
				readOnlyStr = "readonly"; 
			}
		 %>

		   </td> 
		   <td > <input type = text name="othDthCause" <%=disableStr%>  <%=readOnlyStr%> value="<%=othDthCause%>"></td>
		  <%} else { %>
		   <input type = hidden name="othDthCause" value="<%=othDthCause%>">

		  <% } 
		}  else { %>

  		  <td  align="right"><%=LC.L_Specify%><%--Specify*****--%>:&nbsp;</td> <td width="15%"> <input type = text name="othDthCause" value="<%=othDthCause%>"></td>

		<%}%>
		  </tr>
		</table>

		<table  width="100%" cellspacing="0" cellpadding="0" border="0"><tr><td><span id="ajaxPatIdMessage"></span></td></tr></table>

		<table width="100%" cellspacing="2" cellpadding="2" border="0" class="basetbl">
			<tr>
				<td width="50%" colspan="2"><br><p class = "sectionHeadings" ><%=LC.L_Personal_Dets%><%--Personal Details*****--%></p><br></td>
				<td width="50%" colspan="2"><br>
					<p class = "sectionHeadings" ><%=LC.L_Contact_Info%><%--Contact Information*****--%></p><br>
				</td>
			</tr>
			<tr id="showfldobMessage" style="display:none">
				<td colspan="6">
					<span id="fldobMessage" ></span>
				</td>
			</tr>
			<tr>
		<%if (hashPgCustFld.containsKey("fname")) {

			int fldNumFname= Integer.parseInt((String)hashPgCustFld.get("fname"));
			String fnameMand = ((String)cdoPgField.getPcfMandatory().get(fldNumFname));
			String fnameLable = ((String)cdoPgField.getPcfLabel().get(fldNumFname));
			String fnameAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumFname));


			disableStr ="";
			readOnlyStr ="";
			if(fnameAtt == null) fnameAtt ="";
			if(fnameMand == null) fnameMand ="";



			if(!fnameAtt.equals("0")) {

			if(fnameLable !=null){
			%>
			<td>
			<%=fnameLable%>
			<%} else {%>
			<td >
			<%=LC.L_First_Name_Patient%><%--First Name*****--%>
			<%}

			if (fnameMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatfname">* </FONT>
			<% }
			if(fnameAtt.equals("1")) {
			  disableStr = "disabled class='readonly-input'"; }
		    else if (fnameAtt.equals("2") ) {
			 readOnlyStr = "readonly"; }
  		   %>

			</td>
			<td >
					<input type=<%=inputType%> id ="patfname" name="patfname"  <%=disableStr%> <%if(!userBFromSession.getUserGrpDefault().equals(new Integer(grpid_no).toString())){%>readonly <%} %> <%=readOnlyStr%> size = 20 MAXLENGTH = 30 value='<%=fname%>' onblur="valChangeReturn=0;patientDetailsFunctions.displayFldValMsg('patient~fldob:patfname',0,'fldobMessage','','<%=MC.M_PatWithSameNameDobAldy%><%--A <%=LC.Pat_Patient%> already exists with same First Name,Last Name and Date Of Birth.*****--%>','patfname:patlname:patdob:accountId:patid')"><%=displayStar%>
			</td>

			<%}  else { %>

			<input type=hidden id ="patfname" name="patfname" size = 20 MAXLENGTH = 30 value='<%=fname%>' >

		 	<% } 
		} else {%>
			<td ><%=LC.L_First_Name_Patient%><%--First Name*****--%></td>
			<td >
				<input type=<%=inputType%> id ="patfname" name="patfname" size = 20 MAXLENGTH = 30 <%if(!userBFromSession.getUserGrpDefault().equals(new Integer(grpid_no).toString())){%> readonly<%} %> value='<%=fname%>' onblur="valChangeReturn=0;patientDetailsFunctions.displayFldValMsg('patient~fldob:patfname',0,'fldobMessage','','<%=MC.M_PatWithSameNameDobAldy%><%--A <%=LC.Pat_Patient%> already exists with same First Name,Last Name and Date Of Birth.*****--%>','patfname:patlname:patdob:accountId:patid')"><%=displayStar%>
			</td>

		<%}%>

		 <%if (hashPgCustFld.containsKey("address1")) {

			int fldNumAddress1= Integer.parseInt((String)hashPgCustFld.get("address1"));
			String address1Mand = ((String)cdoPgField.getPcfMandatory().get(fldNumAddress1));
			String address1Lable = ((String)cdoPgField.getPcfLabel().get(fldNumAddress1));
			String address1Att = ((String)cdoPgField.getPcfAttribute().get(fldNumAddress1));


			disableStr ="";
			readOnlyStr ="";
			if(address1Att == null) address1Att ="";
			if(address1Mand == null) address1Mand ="";

			if(!address1Att.equals("0")) {

			if(address1Lable !=null){
			%>
			<td >
			<%=address1Lable%>
			<%} else {%> <td >
			<%=LC.L_Address_1%><%--Address 1*****--%>
			<%}

			if (address1Mand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompataddress1">* </FONT>
			<% }
			if(address1Att.equals("1")) {
			  disableStr = "disabled class='readonly-input'"; }
		    else if (address1Att.equals("2") ) {
			 readOnlyStr = "readonly"; }
  		   %>
		   </td>
		   <td >
		   <input type=<%=inputType%> name="patadd1" size = 30 <%=disableStr%> <%=readOnlyStr%> <%if(!userBFromSession.getUserGrpDefault().equals(new Integer(grpid_no).toString())){%> readonly<%} %>  MAXLENGTH = 100 value='<%=add1%>'> <%=displayStar%>
		   </td>
		   <%} else { %>

		    <input type=hidden name="patadd1" size = 30 <%=disableStr%> <%=readOnlyStr%> MAXLENGTH = 100 value='<%=add1%>'/>
		  <%} 
		} else { %>
			<td ><%=LC.L_Address_1%><%--Address 1*****--%></td>
			<td >
					<input type=<%=inputType%> name="patadd1" size = 30 MAXLENGTH = 100 <%if(!userBFromSession.getUserGrpDefault().equals(new Integer(grpid_no).toString())){%> readonly<%} %>  value='<%=add1%>'><%=displayStar%>
			</td>

		<%}%>
		</tr>

		<tr>
			<%if (hashPgCustFld.containsKey("midname")) {

			int fldNumMidname= Integer.parseInt((String)hashPgCustFld.get("midname"));
			String midnameMand = ((String)cdoPgField.getPcfMandatory().get(fldNumMidname));
			String midnameLable = ((String)cdoPgField.getPcfLabel().get(fldNumMidname));
			String midnameAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumMidname));


			disableStr ="";
			readOnlyStr ="";
			if(midnameAtt == null) midnameAtt ="";
			if(midnameMand == null) midnameMand ="";

			if(!midnameAtt.equals("0")) {

			if(midnameLable !=null){
			%>
			<td >
			<%=midnameLable%>
			<%} else {%> <td >
			<%=LC.L_Middle_Name%><%--Middle Name*****--%>
			<%}

			if (midnameMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatmidname">* </FONT>
			<% }
			if(midnameAtt.equals("1")) {
			  disableStr = "disabled class='readonly-input'"; }
		    else if (midnameAtt.equals("2") ) {
			 readOnlyStr = "readonly"; }
  		   %>
		   </td>
		   <td >
		   <input type=<%=inputType%>  name="patmname" size = 20 <%if(!userBFromSession.getUserGrpDefault().equals(new Integer(grpid_no).toString())){%> readonly <%} %> <%=disableStr%> <%=readOnlyStr%> MAXLENGTH = 20 value='<%=mname%>'><%=displayStar%>
		   </td>
		   <%} else { %>

		   <input type=hidden  name="patmname" size = 20  MAXLENGTH = 20 value='<%=mname%>'>

		  <%} } else { %>
			<td ><%=LC.L_Middle_Name%><%--Middle Name*****--%></td>
			<td >
					<input type=<%=inputType%>  name="patmname" size = 20 MAXLENGTH = 20 <%if(!userBFromSession.getUserGrpDefault().equals(new Integer(grpid_no).toString())){%> readonly <%} %> value='<%=mname%>'><%=displayStar%>
			</td>

			<%}%>



			<%if (hashPgCustFld.containsKey("address2")) {

			int fldNumAddress2= Integer.parseInt((String)hashPgCustFld.get("address2"));
			String address2Mand = ((String)cdoPgField.getPcfMandatory().get(fldNumAddress2));
			String address2Lable = ((String)cdoPgField.getPcfLabel().get(fldNumAddress2));
			String address2Att = ((String)cdoPgField.getPcfAttribute().get(fldNumAddress2));


			disableStr ="";
			readOnlyStr ="";
			if(address2Att == null) address2Att ="";
			if(address2Mand == null) address2Mand ="";

			if(!address2Att.equals("0")) {

			if(address2Lable !=null){
			%>
			<td >
			<%=address2Lable%>
			<%} else {%> <td >
			<%=LC.L_Address_2%><%--Address 2*****--%>
			<%}

			if (address2Mand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompataddress2">* </FONT>
			<% }
			if(address2Att.equals("1")) {
			  disableStr = "disabled class='readonly-input'"; }
		    else if (address2Att.equals("2") ) {
			 readOnlyStr = "readonly"; }
  		   %>
		   </td>
		   <td >
		   <input type=<%=inputType%> name="patadd2" size = 30 <%if(!userBFromSession.getUserGrpDefault().equals(new Integer(grpid_no).toString())){%> readonly <%} %> <%=disableStr%> <%=readOnlyStr%> MAXLENGTH = 100 value='<%=add2%>'><%=displayStar%>
		   </td>
		   <%} else {%>

		   <input type="hidden" name="patadd2" size = 30  MAXLENGTH = 100 value='<%=add2%>'>

		   <%}
		} else { %>
			<td ><%=LC.L_Address_2%><%--Address 2*****--%></td>
			<td >
					<input type=<%=inputType%> name="patadd2" size = 30 MAXLENGTH = 100 <%if(!userBFromSession.getUserGrpDefault().equals(new Integer(grpid_no).toString())){%> readonly <%} %> value='<%=add2%>'><%=displayStar%>
			</td>

		<%}%>
		</tr>
		<tr>
			<%if (hashPgCustFld.containsKey("lname")) {

			int fldNumLname= Integer.parseInt((String)hashPgCustFld.get("lname"));
			String lnameMand = ((String)cdoPgField.getPcfMandatory().get(fldNumLname));
			String lnameLable = ((String)cdoPgField.getPcfLabel().get(fldNumLname));
			String lnameAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumLname));


			disableStr ="";
			readOnlyStr ="";
			if(lnameAtt == null) lnameAtt ="";
			if(lnameMand == null) lnameMand ="";


			if(!lnameAtt.equals("0")) {
			if(lnameLable !=null){
			%>
			<td >

			<%=lnameLable%>
			<%} else {%> <td width="20%">
			<%=LC.L_Last_Name_Patient%><%--Last Name*****--%>
			<%}

			if (lnameMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatlname">* </FONT>
			<% }

		   if(lnameAtt.equals("1")) {
			 disableStr = "disabled class='readonly-input'"; }
		   else if (lnameAtt.equals("2") ) {
			 readOnlyStr = "readonly"; }
		 %>
		 </td>
			<td >
				<input type=<%=inputType%> id="patlname" name="patlname"  <%=disableStr%> <%if(!userBFromSession.getUserGrpDefault().equals(new Integer(grpid_no).toString())){%> readonly <%} %> <%=readOnlyStr%> size = 20 MAXLENGTH = 20 value='<%=lname%>' onblur="valChangeReturn=0;patientDetailsFunctions.displayFldValMsg('patient~fldob:patlname',0,'fldobMessage','','<%=MC.M_PatWithSameNameDobAldy%><%--A <%=LC.Pat_Patient%> already exists with same First Name,Last Name and Date Of Birth.*****--%>','patfname:patlname:patdob:accountId:patid')"><%=displayStar%>
			</td>

			<%} else { %>

		<input type="hidden" id="patlname" name="patlname"   size = 20 MAXLENGTH = 30 value='<%=lname%>' >

			<%}
		} else { %>

				<td ><%=LC.L_Last_Name_Patient%><%--Last Name*****--%></td>
				<td >
					<input type=<%=inputType%> id="patlname" name="patlname" size = 20 MAXLENGTH = 20 <%if(!userBFromSession.getUserGrpDefault().equals(new Integer(grpid_no).toString())){%> readonly <%} %> value='<%=lname%>' onblur="valChangeReturn=0;patientDetailsFunctions.displayFldValMsg('patient~fldob:patlname',0,'fldobMessage','','<%=MC.M_PatWithSameNameDobAldy%><%--A <%=LC.Pat_Patient%> already exists with same First Name,Last Name and Date Of Birth.*****--%>','patfname:patlname:patdob:accountId:patid')"><%=displayStar%>
				</td>

		<%}%>

		<%if (hashPgCustFld.containsKey("city")) {
			int fldNumCity = Integer.parseInt((String)hashPgCustFld.get("city"));
			String cityMand = ((String)cdoPgField.getPcfMandatory().get(fldNumCity));
			String cityLable = ((String)cdoPgField.getPcfLabel().get(fldNumCity));
			String cityAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumCity));


			disableStr ="";
			readOnlyStr ="";
			if(cityAtt == null) cityAtt ="";
			if(cityMand == null) cityMand ="";

			if(!cityAtt.equals("0")) {

			if(cityLable !=null){
			%>
			<td width="20%">
			<%=cityLable%>
			<%} else {%> <td width="20%">
			<%=LC.L_City%><%--City*****--%>
			<%}

			if (cityMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatcity">* </FONT>
			<% }
			if(cityAtt.equals("1")) {
			  disableStr = "disabled class='readonly-input'"; }
		    else if (cityAtt.equals("2") ) {
			 readOnlyStr = "readonly"; }
  		   %>
		   </td>
		   <td >
		   <input type=<%=inputType%>  name="patcity" size = 20  <%=disableStr%> <%if(!userBFromSession.getUserGrpDefault().equals(new Integer(grpid_no).toString())){%> readonly <%} %> <%=readOnlyStr%> MAXLENGTH = 100 value='<%=city%>'><%=displayStar%>
		   </td>
		   <%} else { %>

		    <input type="hidden"  name="patcity" size = 20  MAXLENGTH = 100 value='<%=city%>'>

		  <%} 
		} else { %>
			<td ><%=LC.L_City%><%--City*****--%></td>
			<td >
					<input type=<%=inputType%>  name="patcity" size = 20 MAXLENGTH = 100 <%if(!userBFromSession.getUserGrpDefault().equals(new Integer(grpid_no).toString())){%> readonly<%} %> value='<%=city%>'><%=displayStar%>
			</td>

		<%}%>
			</tr>

			<tr>
				<%
				if (hashPgCustFld.containsKey("dob")) {
					int fldNum= Integer.parseInt((String)hashPgCustFld.get("dob"));
					String dobMand = ((String)cdoPgField.getPcfMandatory().get(fldNum));
					String dobLable = ((String)cdoPgField.getPcfLabel().get(fldNum));
					String dobAtt = ((String)cdoPgField.getPcfAttribute().get(fldNum));

					disableStr ="";
					readOnlyStr ="";
					if(dobAtt == null) dobAtt ="";
					if(dobMand == null) dobMand ="";//KM


					if(!dobAtt.equals("0")) {
					if(dobLable !=null){
					%> <td width="20%">
					<%=dobLable%>
					<%} else {%> <td width="20%">
					<%=LC.L_Date_OfBirth%><%--Date of Birth*****--%>
				  <%}
					if (dobMand.equals("1")) {
				  %>

				  <FONT class="Mandatory" id="pgcustompatdob">* </FONT>

				 <%}%>

				<%if(dobAtt.equals("1")) {
						disableStr = "disabled class='readonly-input'"; }
		 	      else if (dobAtt.equals("2") ) {
					 readOnlyStr = "readonly"; }
				 %>


				</td>
				<td >
					<%-- INF-20084 Datepicker-- AGodara --%>					
				<%if (completeDet){%>
					<%if (StringUtil.isEmpty(dobAtt)){ %>
						<input type="<%=inputType%>" name="patdob" id="patdob" class="datefield" <%if(!userBFromSession.getUserGrpDefault().equals(new Integer(grpid_no).toString())){%> readonly <%} %> size = 10 MAXLENGTH=12	value="<%=dob%>" onchange="valChangeReturn=0;patientDetailsFunctions.displayFldValMsg('patient~fldob:patdob',0,'fldobMessage','','<%=MC.M_PatWithSameNameDobAldy%><%--A <%=LC.Pat_Patient%> already exists with same First Name,Last Name and Date Of Birth.*****--%>','patfname:patlname:patdob:accountId:patid')">
					<%} else if(dobAtt.equals("1") || dobAtt.equals("2")) {%>
						<input type=<%=inputType%> name="patdob" id="patdob"  <%=disableStr%> <%=readOnlyStr%>  size = 10 MAXLENGTH = 12 value='<%=dob%>'onchange="valChangeReturn=0;patientDetailsFunctions.displayFldValMsg('patient~fldob:patdob',0,'fldobMessage','','<%=MC.M_PatWithSameNameDobAldy%><%--A <%=LC.Pat_Patient%> already exists with same First Name,Last Name and Date Of Birth.*****--%>','patfname:patlname:patdob:accountId:patid')"><%=displayStar%>
					<%}
				}else{%>
				 		<input type=<%=inputType%> name="patdob" id="patdob" readonly  size = 10 MAXLENGTH = 12 value='<%=dob%>'onchange="valChangeReturn=0;patientDetailsFunctions.displayFldValMsg('patient~fldob:patdob',0,'fldobMessage','','<%=MC.M_PatWithSameNameDobAldy%><%--A <%=LC.Pat_Patient%> already exists with same First Name,Last Name and Date Of Birth.*****--%>','patfname:patlname:patdob:accountId:patid')"><%=displayStar%>
				 <% }%> 
				</td>
				<%} else{%>
						<td></td><td><input type="hidden" name="patdob" id="patdob"  size = 10 MAXLENGTH = 12 value='<%=dob%>'	></td>
				
				<%}
			}else{ %>
					<td > <%=LC.L_Date_OfBirth%><%--Date of Birth*****--%> 	<FONT class="Mandatory" id="mandpatdob">* </FONT>
					</td>
					<td >
				<%if (completeDet) { %>
					<input type=<%=inputType%> name="patdob" class="datefield" id="patdob" <%if(!userBFromSession.getUserGrpDefault().equals(new Integer(grpid_no).toString())){%> readonly <%} %> size = 10 MAXLENGTH = 12 value='<%=dob%>'onchange="valChangeReturn=0;patientDetailsFunctions.displayFldValMsg('patient~fldob:patdob',0,'fldobMessage','','<%=MC.M_PatWithSameNameDobAldy%><%--A <%=LC.Pat_Patient%> already exists with same First Name,Last Name and Date Of Birth.*****--%>','patfname:patlname:patdob:accountId:patid')"><%=displayStar%>
				<%} else {%>
					<input type=<%=inputType%> name="patdob" id="patdob" readonly  size = 10 MAXLENGTH = 12 value='<%=dob%>'onchange="valChangeReturn=0;patientDetailsFunctions.displayFldValMsg('patient~fldob:patdob',0,'fldobMessage','','<%=MC.M_PatWithSameNameDobAldy%><%--A <%=LC.Pat_Patient%> already exists with same First Name,Last Name and Date Of Birth.*****--%>','patfname:patlname:patdob:accountId:patid')"><%=displayStar%>
				<% } %>
				</td>
			<%}%>

		<%if (hashPgCustFld.containsKey("state")) {

			int fldNumState = Integer.parseInt((String)hashPgCustFld.get("state"));
			String stateMand = ((String)cdoPgField.getPcfMandatory().get(fldNumState));
			String stateLable = ((String)cdoPgField.getPcfLabel().get(fldNumState));
			String stateAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumState));


			disableStr ="";
			readOnlyStr ="";
			if(stateAtt == null) stateAtt ="";
			if(stateMand == null) stateMand ="";

			if(!stateAtt.equals("0")) {

			if(stateLable !=null){
			%>
			<td width="20%">
			<%=stateLable%>
			<%} else {%> <td width="20%">
			<%=LC.L_State%><%--State*****--%>
			<%}

			if (stateMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatstate">* </FONT>
			<% }
			if(stateAtt.equals("1")) {
			  disableStr = "disabled class='readonly-input'"; }
		    else if (stateAtt.equals("2") ) {
			 readOnlyStr = "readonly"; }
  		   %>
		   </td>
		   <td >
		   <input type=<%=inputType%> name="patstate" size = 20 <%if(!userBFromSession.getUserGrpDefault().equals(new Integer(grpid_no).toString())){%> readonly <%} %>  <%=disableStr%> <%=readOnlyStr%> MAXLENGTH = 100 value='<%=state%>'><%=displayStar%>
		   </td>
		   <%} else { %>
		   <input type="hidden" name="patstate" size = 20  MAXLENGTH = 100 value='<%=state%>'>

		   <%}
		} else { %>
			<td ><%=LC.L_State%><%--State*****--%></td>
			<td>
				<input type=<%=inputType%> name="patstate" size = 20 MAXLENGTH = 100 <%if(!userBFromSession.getUserGrpDefault().equals(new Integer(grpid_no).toString())){%> readonly <%} %> value='<%=state%>'><%=displayStar%>
			</td>

		<%}%>

			</tr>
			<tr>
			<% if (hashPgCustFld.containsKey("gender")) {
					int fldNumGender= Integer.parseInt((String)hashPgCustFld.get("gender"));
					String gendMand = ((String)cdoPgField.getPcfMandatory().get(fldNumGender));
					String gendLable = ((String)cdoPgField.getPcfLabel().get(fldNumGender));
					gendAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumGender));


					if(gendMand == null) gendMand ="";//KM
					if(gendAtt == null) gendAtt ="";

					if(!gendAtt.equals("0")) {
						if(gendLable !=null){
						%> <td width="20%">
						<%=gendLable%>
						<%} else {%> <td width="20%">
						<%=LC.L_Gender%><%--Gender*****--%>
					<%}

					if (gendMand.equals("1")) {
					%>
				   <FONT class="Mandatory" id="pgcustompatgender">* </FONT>
					<% }
					 %>
 			      </td>
 			      <td >
 			      	<%=dGender%>
 			      	<%if(gendAtt.equals("1")) {%>
 			      		<input type="hidden" name="patgender" value="">
 			      	<%} else if(gendAtt.equals("2")) {%>
						<input type="hidden" name="patgender" value="<%=gender%>">
					<%} %>
				  </td>

				<%} else  { %>
				<input type="hidden" name="patgender" value="<%=gender%>">

				<%}
			} else { %>
				<td ><%=LC.L_Gender%><%--Gender*****--%> </td>
				<td ><%=dGender%></td>
			<%}%>

		<%if (hashPgCustFld.containsKey("county")) {

			int fldNumCounty = Integer.parseInt((String)hashPgCustFld.get("county"));
			String countyMand = ((String)cdoPgField.getPcfMandatory().get(fldNumCounty));
			String countyLable = ((String)cdoPgField.getPcfLabel().get(fldNumCounty));
			String countyAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumCounty));


			disableStr ="";
			readOnlyStr ="";
			if(countyAtt == null) countyAtt ="";
			if(countyMand == null) countyMand ="";

			if(!countyAtt.equals("0")) {

			if(countyLable !=null){
			%>
			<td >
			<%=countyLable%>
			<%} else {%> <td width="20%">
			<%=LC.L_County%><%--County*****--%>
			<%}

			if (countyMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatcounty">* </FONT>
			<% }
			if(countyAtt.equals("1")) {
			  disableStr = "disabled class='readonly-input'"; }
		    else if (countyAtt.equals("2") ) {
			  readOnlyStr = "readonly"; }
  		   %>
		   </td>
		   <td >
		   <input type=<%=inputType%> name="patcounty" size = 20  <%if(!userBFromSession.getUserGrpDefault().equals(new Integer(grpid_no).toString())){%> readonly <%} %> <%=disableStr%> <%=readOnlyStr%> MAXLENGTH = 100 value='<%=county%>'><%=displayStar%>
		   </td>
		   <%}  else { %>
		    <input type="hidden" name="patcounty" size = 20  MAXLENGTH = 100 value='<%=county%>'>

		  <%} 
		} else { %>
			<td ><%=LC.L_County%><%--County*****--%></td>
			<td >
				<input type=<%=inputType%> name="patcounty" size = 20 MAXLENGTH = 100 <%if(!userBFromSession.getUserGrpDefault().equals(new Integer(grpid_no).toString())){%> readonly <%} %> value='<%=county%>'><%=displayStar%>
			</td>

		<%}%>
			</tr>

			<tr>
			<%if (hashPgCustFld.containsKey("maritalstat")) {
					int fldNumMarital= Integer.parseInt((String)hashPgCustFld.get("maritalstat"));
					String maritalMand = ((String)cdoPgField.getPcfMandatory().get(fldNumMarital));
					String maritalLable = ((String)cdoPgField.getPcfLabel().get(fldNumMarital));
					maritalAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumMarital));


					if(maritalMand == null) maritalMand ="";
					if(maritalAtt == null) maritalAtt ="";

					if(!maritalAtt.equals("0")) {
						if(maritalLable !=null){
						%> <td >
						<%=maritalLable%>
						<%} else {%> <td width="20%">
						<%=LC.L_Marital_Status%><%--Marital Status*****--%>
					<%}

					if (maritalMand.equals("1")) {
					%>
				   <FONT class="Mandatory" id="pgcustompatmarital">* </FONT>
					<% }
					 %>
 			      </td>
 			      <td >
 			      <%=dMarital%>
 			      <%if(maritalAtt.equals("1")) {%>
 			      		<input type="hidden" name="patmarital" value="">
 			      <%} else if(maritalAtt.equals("2")) {%>
 			      		<input type="hidden" name="patmarital" value="<%=maritalStatus%>">
 			      <%} %>
 			      </td>

				<%} else  {%>

				<input type="hidden" name="patmarital" value=<%=maritalStatus%> >

			    <%}
			} else { %>
				<td ><%=LC.L_Marital_Status%><%--Marital Status*****--%></td>
				<td><%=dMarital%></td>
			<%}%>

		<%if (hashPgCustFld.containsKey("zipcode")) {

			int fldNumZip = Integer.parseInt((String)hashPgCustFld.get("zipcode"));
			String zipMand = ((String)cdoPgField.getPcfMandatory().get(fldNumZip));
			String zipLable = ((String)cdoPgField.getPcfLabel().get(fldNumZip));
			String zipAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumZip));


			disableStr ="";
			readOnlyStr ="";
			if(zipAtt == null) zipAtt ="";
			if(zipMand == null) zipMand ="";

			if(!zipAtt.equals("0")) {

			if(zipLable !=null){
			%>
			<td >
			<%=zipLable%>
			<%} else {%> <td width="20%">
			<%=LC.L_ZipOrPostal_Code%><%--Zip/Postal Code*****--%>
			<%}

			if (zipMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatzip">* </FONT>
			<% }
			if(zipAtt.equals("1")) {
			  disableStr = "disabled class='readonly-input'"; }
		    else if (zipAtt.equals("2") ) {
			  readOnlyStr = "readonly"; }
  		   %>
		   </td>
		   <td >
		   <input type=<%=inputType%> name="patzip" size = 20  <%if(!userBFromSession.getUserGrpDefault().equals(new Integer(grpid_no).toString())){%> readonly <%} %> <%=disableStr%> <%=readOnlyStr%> MAXLENGTH = 20 value='<%=zip%>'><%=displayStar%>
		   </td>
		   <%} else { %>

		   <input type="hidden" name="patzip" size = 20   MAXLENGTH = 20 value='<%=zip%>'>

		   <%}
		} else { %>
			<td ><%=LC.L_ZipOrPostal_Code%><%--Zip/Postal Code*****--%></td>
			<td >
				<input type=<%=inputType%> name="patzip" size = 20 MAXLENGTH = 20 <%if(!userBFromSession.getUserGrpDefault().equals(new Integer(grpid_no).toString())){%> readonly <%} %> value='<%=zip%>'><%=displayStar%>
			</td>

		<%}%>


			</tr>
			<tr>

			<% if (hashPgCustFld.containsKey("bloodgrp")) {
					int fldNumGender= Integer.parseInt((String)hashPgCustFld.get("bloodgrp"));
					String bloodMand = ((String)cdoPgField.getPcfMandatory().get(fldNumGender));
					String bloodLable = ((String)cdoPgField.getPcfLabel().get(fldNumGender));
					bloodAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumGender));


					if(bloodMand == null) bloodMand ="";
					if(bloodAtt == null) bloodAtt ="";

					if(!bloodAtt.equals("0")) {
						if(bloodLable !=null){
						%> <td width="20%">
						<%=bloodLable%>
						<%} else {%> <td width="20%">
						<%=LC.L_Blood_Grp%><%--Blood Group*****--%>
					<%}

					if (bloodMand.equals("1")) {
					%>
				   <FONT class="Mandatory" id="pgcustompatblood">* </FONT>
					<% }
					 %>
 			      </td>
 			      <td width="225">
 			      <%=dBloodGroup%>
 			      <%if(bloodAtt.equals("1")) {%>
 			      		<input type="hidden" name="patblood" value="">
 			      <%} else if(bloodAtt.equals("2")) {%>
 			      		<input type="hidden" name="patblood" value="<%=bloodGroup%>">
 			      <%} %> 
 			      </td>

				<%} else { %>
				 <input type="hidden" name="patblood" value="<%=bloodGroup%>">

				<%}
				} else { %>
				<td ><%=LC.L_Blood_Grp%><%--Blood Group*****--%></td>
				<td ><%=dBloodGroup%></td>
				<%}%>





			<%if (hashPgCustFld.containsKey("country")) {

			int fldNumCountry = Integer.parseInt((String)hashPgCustFld.get("country"));
			String countryMand = ((String)cdoPgField.getPcfMandatory().get(fldNumCountry));
			String countryLable = ((String)cdoPgField.getPcfLabel().get(fldNumCountry));
			String countryAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumCountry));


			disableStr ="";
			if(countryAtt == null) countryAtt ="";
			if(countryMand == null) countryMand ="";

			if(!countryAtt.equals("0")) {

			if(countryLable !=null){
			%>
			<td >
			<%=countryLable%>
			<%} else {%> <td width="20%">
			<%=LC.L_Country%><%--Country*****--%>
			<%}

			if (countryMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatcountry">* </FONT>
			<% }
			if(countryAtt.equals("1")) { disableStr = "disabled"; }
  		   %>
		   </td>
		   <td >
		   <input type=<%=inputType%> id="patcountry" name="patcountry" class='readonly-input' size ='30'  <%=disableStr%> READONLY MAXLENGTH = 100 value='<%=country%>'><%=displayStar%>
		   
		   <%if(userBFromSession.getUserGrpDefault().equals(new Integer(grpid_no).toString())){%> <A href="#" onClick="patientDetailsFunctions.openCountryLookup(patientDetailsFunctions.formObj)"><%=LC.L_Select_Country%><%--Select Country*****--%> </A><%} %>
		   </td>
		   <%} else { %>

		   <input type="hidden" id="patcountry" name="patcountry" size = 30   MAXLENGTH = 100 value='<%=country%>'>

		   <%}} else { %>
			<td ><%=LC.L_Country%><%--Country*****--%></td>
			<td>
				<input type=<%=inputType%> id="patcountry" name="patcountry" class='readonly-input' size = 30 READONLY MAXLENGTH = 100 value='<%=country%>'><%=displayStar%>
				<%if(userBFromSession.getUserGrpDefault().equals(new Integer(grpid_no).toString())){%>
				<A href="#" onClick="patientDetailsFunctions.openCountryLookup(patientDetailsFunctions.formObj)"><%=LC.L_Select_Country%><%--Select Country*****--%> </A>
				<% }%>
			</td>

			<%}%>


			</tr>

			<tr>

			<%if (hashPgCustFld.containsKey("ssn")) {

			int fldNumSsn = Integer.parseInt((String)hashPgCustFld.get("ssn"));
			String ssnMand = ((String)cdoPgField.getPcfMandatory().get(fldNumSsn));
			String ssnLable = ((String)cdoPgField.getPcfLabel().get(fldNumSsn));
			String ssnAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumSsn));

			if (SSNNotApplicable.equals("1"))//for SM
			{
				ssnAtt="1";

			}//For SM

			disableStr ="";
			readOnlyStr ="";
			if(ssnAtt == null) ssnAtt ="";
			if(ssnMand == null) ssnMand ="";

			if(!ssnAtt.equals("0")) {

			if(ssnLable !=null){
			%>
			<td ><%=LC.L_Ssn_NotApplicable%><%--SSN Not Applicable*****--%> &nbsp;</td>
			<td ><%=SSNNotApplicableDD%></td>
		</tr>
		<tr>
			<%} else {%>
			<td ><%=LC.L_Ssn_NotApplicable%><%--SSN Not Applicable*****--%> &nbsp;</td>
			<td ><%=SSNNotApplicableDD%></td>
		</tr>
		<tr>
			<%}
			if(ssnLable !=null){
			%>
				<td ><%=ssnLable%>
			<%} else {%>
				<td ><%=LC.L_Ssn%><%--SSN*****--%>
			<%}

			if (ssnMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatssn">* </FONT>
			<% }
			if(ssnAtt.equals("1")) {
			  disableStr = "disabled class='readonly-input'"; }
		    else if (ssnAtt.equals("2") ) {
			  readOnlyStr = "readonly"; }
  		   %>
		   </td>
		   <td >
		         <input type=<%=inputType%> name="patssn" size = 20  <%=disableStr%> <%=readOnlyStr%> MAXLENGTH = 20 value='<%=ssn%>'><%=displayStar%>
		   </td>
		   <%} else { %>
		   <input type="hidden" name="patssn" size = 20  MAXLENGTH = 20 value='<%=ssn%>'>

		  <%} } else { %>

		  	<% if (SSNNotApplicable.equals("1")){
		  			disableStr=" disabled class='readonly-input' ";
		  		} else {
		  				disableStr="";
		  		}%>
			<td ><%=LC.L_Ssn_NotApplicable%><%--SSN Not Applicable*****--%> &nbsp;</td>
			<td ><%=SSNNotApplicableDD%></td>
		</tr>
		<tr>
			<td ><%=LC.L_Ssn%><%--SSN*****--%></td>
			<td >
				<input type=<%=inputType%> <%=disableStr%> name="patssn" size = 20 MAXLENGTH = 20 value='<%=ssn%>'><%=displayStar%>
			</td>

			<%}%>



		 <%if (hashPgCustFld.containsKey("hphone")) {

			int fldNumHphone = Integer.parseInt((String)hashPgCustFld.get("hphone"));
			String hphoneMand = ((String)cdoPgField.getPcfMandatory().get(fldNumHphone));
			String hphoneLable = ((String)cdoPgField.getPcfLabel().get(fldNumHphone));
			String hphoneAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumHphone));


			disableStr ="";
			readOnlyStr ="";
			if(hphoneAtt == null) hphoneAtt ="";
			if(hphoneMand == null) hphoneMand ="";

			if(!hphoneAtt.equals("0")) {

			if(hphoneLable !=null){
			%>
			<td >
			<%=hphoneLable%>
			<%} else {%> <td width="20%">
			<%=LC.L_Home_Phone_S%><%--Home Phone(s)*****--%>
			<%}

			if (hphoneMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompathphone">* </FONT>
			<% }
			if(hphoneAtt.equals("1")) {
			  disableStr = "disabled class='readonly-input'"; }
		    else if (hphoneAtt.equals("2") ) {
			  readOnlyStr = "readonly"; }
  		   %>
		   </td>
		   <td >
		   <input type=<%=inputType%> name="pathphone" size = 30  <%=disableStr%> <%=readOnlyStr%> MAXLENGTH = 100 value='<%=hphone%>'><%=displayStar%>
		   </td>
		   <%} else {%>

		   <input type="hidden" name="pathphone" size = 30  MAXLENGTH = 100 value='<%=hphone%>'>
		   <%}} else { %>
			<td ><%=LC.L_Home_Phone_S%><%--Home Phone(s)*****--%></td>
			<td >
				<input type=<%=inputType%> name="pathphone" size = 30 MAXLENGTH = 100 value='<%=hphone%>'><%=displayStar%>
			</td>

			<%}%>


		 </tr>
		 <tr>

			<%if (hashPgCustFld.containsKey("email")) {

			int fldNumEmail = Integer.parseInt((String)hashPgCustFld.get("email"));
			String emailMand = ((String)cdoPgField.getPcfMandatory().get(fldNumEmail));
			String emailLable = ((String)cdoPgField.getPcfLabel().get(fldNumEmail));
			String emailAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumEmail));


			disableStr ="";
			readOnlyStr ="";
			if(emailAtt == null) emailAtt ="";
			if(emailMand == null) emailMand ="";

			if(!emailAtt.equals("0")) {

			if(emailLable !=null){
			%>
			<td >
			<%=emailLable%>
			<%} else {%> <td width="20%">
			<%=LC.L_EhypMail%><%--E-Mail*****--%>
			<%}

			if (emailMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatemail">* </FONT>
			<% }
			if(emailAtt.equals("1")) {
			  disableStr = "disabled class='readonly-input'"; }
		    else if (emailAtt.equals("2") ) {
			  readOnlyStr = "readonly"; }
  		   %>
		   </td>
		   <td >
		   <input type=<%=inputType%> name="patemail" size = 30  <%=disableStr%> <%=readOnlyStr%> MAXLENGTH = 100 value='<%=email%>'><%=displayStar%>
		   </td>
		   <%} else { %>
		   <input type="hidden" name="patemail" size = 30   MAXLENGTH = 100 value='<%=email%>'>

		   <%}
		} else { %>
			<td ><%=LC.L_EhypMail%><%--E-Mail*****--%></td>
			<td >
				<input type=<%=inputType%> name="patemail" size = 30 MAXLENGTH = 100 value='<%=email%>'><%=displayStar%>
			</td>

		<%}%>


		<%if (hashPgCustFld.containsKey("wphone")) {

			int fldNumWphone = Integer.parseInt((String)hashPgCustFld.get("wphone"));
			String wphoneMand = ((String)cdoPgField.getPcfMandatory().get(fldNumWphone));
			String wphoneLable = ((String)cdoPgField.getPcfLabel().get(fldNumWphone));
			String wphoneAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumWphone));


			disableStr ="";
			readOnlyStr ="";
			if(wphoneAtt == null) wphoneAtt ="";
			if(wphoneMand == null) wphoneMand ="";

			if(!wphoneAtt.equals("0")) {

			if(wphoneLable !=null){
			%>
			<td >
			<%=wphoneLable%>
			<%} else {%> <td width="20%">
			<%=LC.L_Work_Phone_S%><%--Work Phone(s*****--%>
			<%}

			if (wphoneMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatwphone">* </FONT>
			<% }
			if(wphoneAtt.equals("1")) {
			  disableStr = "disabled class='readonly-input'"; }
		    else if (wphoneAtt.equals("2") ) {
			  readOnlyStr = "readonly"; }
  		   %>
		   </td>
		   <td >
		   <input type=<%=inputType%> name="patbphone" size = 30  <%=disableStr%> <%=readOnlyStr%> MAXLENGTH = 100 value='<%=bphone%>'><%=displayStar%>
		   </td>
		   <%} else {%>

		   <input type="hidden" name="patbphone" size = 30  MAXLENGTH = 100 value='<%=bphone%>'>
		   <%}} else { %>
			<td ><%=LC.L_Work_Phone_S%><%--Work Phone(s*****--%></td>
			<td >
				<input type=<%=inputType%> name="patbphone" size = 30 MAXLENGTH = 100 value='<%=bphone%>'><%=displayStar%>
			</td>

			<%}%>


		 </tr>
		 <tr height="7"><td colspan="5"></td></tr>
		 <tr>

			 <%if (hashPgCustFld.containsKey("ethnicity")) {
			int fldNumEth= Integer.parseInt((String)hashPgCustFld.get("ethnicity"));
			String ethMand = ((String)cdoPgField.getPcfMandatory().get(fldNumEth));
			String ethLable = ((String)cdoPgField.getPcfLabel().get(fldNumEth));
			ethAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumEth));

			if(ethAtt == null) ethAtt ="";
			if(ethMand == null) ethMand ="";

			if(!ethAtt.equals("0")) {
			if(ethLable !=null){
			%><td width="20%" >
			<%=ethLable%>
			<%} else {%> <td width="20%" >
			<%=LC.L_Prim_Ethnicity%><%--Primary Ethnicity*****--%>
			<%}

			if (ethMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatethnicity">* </FONT>
			<% }
				 %>
			</td>
			<td width="30%">
				<%=dEthnicity%>
				<%if(ethAtt.equals("1")) {%>
					<input type="hidden" name="patethnicity" value="">
				<%} else if(ethAtt.equals("2")) {%>
					<input type="hidden" name="patethnicity" value="<%=ethnicity%>">
				<%}%>
			</td>

			<%} else  { %>

			<input type="hidden" name="patethnicity" value="<%=ethnicity%>">
			<%}
			} else {%>
			<td width="20%"> <%=LC.L_Prim_Ethnicity%><%--Primary Ethnicity*****--%> </td>
			<td width="30%">
				<%=dEthnicity%>
			</td>

			<% }%>

			<%if (hashPgCustFld.containsKey("additional1")) {


			int fldNumAdd1= Integer.parseInt((String)hashPgCustFld.get("additional1"));
			String add1Mand = ((String)cdoPgField.getPcfMandatory().get(fldNumAdd1));
			String add1Lable = ((String)cdoPgField.getPcfLabel().get(fldNumAdd1));
			String add1Att = ((String)cdoPgField.getPcfAttribute().get(fldNumAdd1));

			disableStr ="";
			if(add1Att == null) add1Att ="";
			if(add1Mand == null) add1Mand ="";



			if(!add1Att.equals("0")) {
			if(add1Lable !=null){
			%><td >
			<%=add1Lable%>
			<%} else {%> <td width="20%">
			<%=LC.L_Additional%><%--Additional*****--%>
			<%}

			if (add1Mand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatadditional1">* </FONT>
			<% }

			if(add1Att.equals("1")) {
			 disableStr = "disabled class='readonly-input'"; }
		     //else if (add1Att.equals("2") ) {
			 //readOnlyStr = "readonly"; }
		 %>
		 </td>
		 <td >
		<input type="text" name="txtAddEthnicity" Readonly size = 20  <%=disableStr%> MAXLENGTH = 500 value='<%=addEthnicityNames%>'>
		<%if(!add1Att.equals("1") &&  !add1Att.equals("2") ) {%>
		<A href="#" onClick="patientDetailsFunctions.openEthnicityWindow()"><%=LC.L_Select_Ethnicity%><%--Select Ethnicity*****--%></A>
		<%}%> 
		</td>
		<%} else { %>
		 <input type="hidden" name="txtAddEthnicity"  size = 20  MAXLENGTH = 500 value='<%=addEthnicityNames%>'>


		<%} 
	} else {%>
		<td ><%=LC.L_Additional%><%--Additional*****--%></td>
		<td >
			<input type="text" name="txtAddEthnicity" Readonly size = 20 MAXLENGTH = 500 value='<%=addEthnicityNames%>'>
			<A href="#" onClick="patientDetailsFunctions.openEthnicityWindow()"><%=LC.L_Select_Ethnicity%><%--Select Ethnicity*****--%></A>
		</td>
	<%}%>
		</tr>
		<tr>


			<%if (hashPgCustFld.containsKey("race")) {
			int fldNumRace= Integer.parseInt((String)hashPgCustFld.get("race"));

			String raceMand = ((String)cdoPgField.getPcfMandatory().get(fldNumRace));
			String raceLable = ((String)cdoPgField.getPcfLabel().get(fldNumRace));
			raceAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumRace));

			if(raceAtt == null) raceAtt ="";if(raceMand == null) raceMand ="";

			if(!raceAtt.equals("0")) {
			if(raceLable !=null){
			%>  <td width="20%" >
			<%=raceLable%>
			<%} else {%> <td width="20%" >
			<%=LC.L_Prim_Race%><%--Primary Race*****--%>
			<%}
			if (raceMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatrace">* </FONT>
			<%
				 }%>
			</td>
			<td >
				<%=dRace%>
				<%if(raceAtt.equals("1")) {%>
					<input type="hidden" name="patrace" value="">
				<%} else if(raceAtt.equals("2")) {%>
					<input type="hidden" name="patrace" value="<%=race%>">
				<%}%>
			</td>
			<%} else  {%>  <input type="hidden" name="patrace" value="<%=race%>"> <!--KM-->
			<%}

			} else { //KM-if custom data is not available
			%><td  ><%=LC.L_Prim_Race%><%--Primary Race*****--%>  </td>
			<td >
				<%=dRace%>
			</td>

			<%}%>



			<%if (hashPgCustFld.containsKey("additional2")) {


			int fldNumAdd2= Integer.parseInt((String)hashPgCustFld.get("additional2"));
			String add2Mand = ((String)cdoPgField.getPcfMandatory().get(fldNumAdd2));
			String add2Lable = ((String)cdoPgField.getPcfLabel().get(fldNumAdd2));
			String add2Att = ((String)cdoPgField.getPcfAttribute().get(fldNumAdd2));


			disableStr ="";
			readOnlyStr ="";
			if(add2Att == null) add2Att ="";if(add2Mand == null) add2Mand ="";




			if(!add2Att.equals("0")) {
			if(add2Lable !=null){
			%><td >
			<%=add2Lable%>
			<%} else {%> <td width="20%">
			<%=LC.L_Additional%><%--Additional*****--%>
			<%}

			if (add2Mand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatadditional2">* </FONT>
			<%
		 }%>


		  <%if(add2Att.equals("1")) {
			 disableStr = "disabled class='readonly-input'"; }
		     //else if (add2Att.equals("2") ) {
			 //readOnlyStr = "readonly"; }
		 %>

	   	</td>
			<td >
			<input type="text" name="txtAddRace"  <%=disableStr%>  Readonly size =20 MAXLENGTH = 500 value='<%=addRaceNames%>'>


			<%if(!add2Att.equals("1")  &&  !add2Att.equals("2") ) { %>

			<A href="#" onClick="patientDetailsFunctions.openRaceWindow()"><%=LC.L_Select_Race%><%--Select Race*****--%></A>

			 <%}%>  </td>

			<%}   else {%>
			<input type="hidden" name="txtAddRace"  size =20 MAXLENGTH = 500 value='<%=addRaceNames%>'>

			<%}} else {%>

			<td ><%=LC.L_Additional%><%--Additional*****--%></td>
			<td >
				<input type="text" name="txtAddRace" Readonly size =20 MAXLENGTH = 500 value='<%=addRaceNames%>'>
				<A href="#" onClick="patientDetailsFunctions.openRaceWindow()"><%=LC.L_Select_Race%><%--Select Race*****--%></A>
			</td>
		 <%}%>


		 </tr>
	</table>
	<br>			
	<table  width="100%" cellspacing="0" cellpadding="0" border="0" class="basetbl" >
	<tr><td><p class = "sectionHeadings" ><%=LC.L_Reg_Dets%><%--Registration Details*****--%></p></td></tr>
		<tr height="25">
			<td colspan = "4">

				<p class="defComments"> <%=MC.M_SaveBeforeAdding_RegDets%><%--Please save your changes before adding or modifying <%=LC.Pat_Patient_Lower%> registration details*****--%></p>
			</td>
		</tr>
		<tr>
			<td>
				<p class="sectionheadings" > <%=MC.M_PatRegTo_FlwOrgs%><%--The Patient is registered to the following Organization(s)*****--%>:</p>
			</td>
			<td align="right"><A onClick="patientDetailsFunctions.openPatFacility(<%=personPK%>,0, <%=facilityCount%>,<%=patDataDetail%>)" href = "#"><%=MC.M_RegTo_NewOrg%><%--REGISTER TO A NEW ORGANIZATION*****--%></A></td>
		</tr>
	</table>		
	<table  width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl outline midalign" >
		<tr>
			<th><%=LC.L_Organization%><%--Organization*****--%></th>
			<th><%=LC.L_Facility_Id%><%--Facility ID*****--%></th>
			<th><%=LC.L_Reg_Date%><%--Registration Date*****--%></th>
			<th><%=LC.L_Provider%><%--Provider*****--%></th>
			<th><%=LC.L_GrpOrDept%><%--Group/Department*****--%></th>
			<th><%=LC.L_Access%><%--Access*****--%></th>
		</tr>
			<% for (int k = 0; k < facilityCount ; k++) {
				  accessFlag = patFacilityDao.getPatAccessFlag(k);
				  if (StringUtil.isEmpty(accessFlag)){
					accessFlag = "0";
				  }
				  if (accessFlag.equals("0")){
					  accessString = LC.L_Revoked; /*accessString = "Revoked";*****/
				  }else{
					  accessString = LC.L_Granted; /*accessString = "Granted";*****/
				 }
				if ((k%2)!=0) {	%>
					  <tr class="browserEvenRow">
				<%  }else{	%>
					  <tr class="browserOddRow">
			<% } %>
			<td>
				<%if(userSites!=null && userSites.contains(patFacilityDao.getPatientSiteName(k))){ %>
					<A onClick="patientDetailsFunctions.openPatFacility(<%=personPK%>,<%=patFacilityDao.getId(k)%>, <%=facilityCount%>,<%=patDataDetail%>)" href = "#"> <%=patFacilityDao.getPatientSiteName(k)%> </A>
				<%}else{ %>
					 <%=patFacilityDao.getPatientSiteName(k)%>
				<%} %>
			</td>
			<td>
				<% if( patDataDetail >= 4 ) { %>
					<%=patFacilityDao.getPatientFacilityId(k)%>
				<% } else {
					%>
					<%=displayStar%>
				<% } %>

			</td>
			<td><%=patFacilityDao.getRegDate(k)%></td>
			<td><%=patFacilityDao.getRegisteredBy(k)%></td>
			<td><%=patFacilityDao.getPatSpecialtylAccess(k)%></td>
			<td><%=accessString%></td>
			<% if (patFacilityDao.getIsDefault(k).equals("1")){ %>
				<input type="hidden" name="patorganization" value="<%=patFacilityDao.getPatientSite(k)%>">
			<%} %>
		</tr>
		<% } %>
	</table>
		  <%//-- May Enhn%>
			<table  width="100%" cellspacing="0" cellpadding="0" border="0" class="basetbl" >
				<tr >
					<td colspan="2">
						<p class = "sectionHeadings" ><%=LC.L_Other%><%--Other*****--%></p><br>
					</td>
				</tr>

		 <!--KM------------------------------------------------>

				<% if (hashPgCustFld.containsKey("timezone")) {
					int fldNumTz= Integer.parseInt((String)hashPgCustFld.get("timezone"));
					String tzMand = ((String)cdoPgField.getPcfMandatory().get(fldNumTz));
					String tzLable = ((String)cdoPgField.getPcfLabel().get(fldNumTz));
					tzAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumTz));

					if(tzMand == null) tzMand ="";
					if(tzAtt == null) tzAtt ="";

					if(!tzAtt.equals("0")) {
						if(tzLable !=null){
						%> <tr> <td width="20%">
						<%=tzLable%>
						<%} else {%> <tr> <td width="20%">
						<%=LC.L_Time_Zone%><%--Time Zone*****--%>
					<%}

					if (tzMand.equals("1")) {
					%>
				   <FONT class="Mandatory" id="pgcustompattz">* </FONT>
					<% }
					 %>
 			      </td>

				   <%if(!patTz.equals("") && (StringUtil.stringToNum(patTzOverride))==0){	%>


						<td>
						<%if(tzAtt.equals("0")) {%>
								<input type="hidden" name="timeZone" value="">
							<%} else {%>
								<input type="hidden" name="timeZone" value="<%=timeZone%>">
							<%}%>
						<input type="text" name="timeZoneText" size=75 MAXLENGTH=100 readonly value="" style="border:none;background:#f7f7f7">
						<div style="visibility:hidden;height:10px;left:0px;">
							<%=dTimeZone%>
						</div>
						</td>
						<%}else { %>
						<td  colspan="3">
							<%=dTimeZone%>
							<%if(tzAtt.equals("0")) {%>
								<input type="hidden" name="timeZone" value="">
							<%} else {%>
								<input type="hidden" name="timeZone" value="<%=timeZone%>">
							<%}%>
						</td>
						 <%} %>

						 </tr>

				<%} else  { %>

				<input type="hidden" name="timeZone" value="<%=timeZone%>">

				<% }} else { %>
				<tr>
					<td width="20%"><%=LC.L_Time_Zone%><%--Time Zone*****--%> <FONT class="Mandatory">* </FONT> </td>
						<%
						if(!patTz.equals("") && (StringUtil.stringToNum(patTzOverride))==0){
						%>
					<td>
					<input type="hidden" name="timeZone" value="<%=timeZone%>">
						<input type="text" name="timeZoneText" size=75 MAXLENGTH=100 readonly value="" style="border:none;background:#f7f7f7">
							<div style="visibility:hidden;height:10px;left:0px;">
							<%=dTimeZone%>
							</div>
					</td>
				   <%}else { %>
					<td  colspan="3"> <%=dTimeZone%> </td>
					  <%} %>

				</tr>
				<%}%>


     	<!--KM----------------------------------------->



				<% if (hashPgCustFld.containsKey("employment")) {
					int fldNumEmp= Integer.parseInt((String)hashPgCustFld.get("employment"));
					String empMand = ((String)cdoPgField.getPcfMandatory().get(fldNumEmp));
					String empLable = ((String)cdoPgField.getPcfLabel().get(fldNumEmp));
					empAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumEmp));


					if(empMand == null) empMand ="";
					if(empAtt == null) empAtt ="";

					if(!empAtt.equals("0")) {
						if(empLable !=null){
						%> <tr> <td width="20%">
						<%=empLable%>
						<%} else {%> <tr> <td width="20%">
						<%=LC.L_Employment%><%--Employment*****--%>
					<%}

					if (empMand.equals("1")) {
					%>
				   <FONT class="Mandatory" id="pgcustompatemp">* </FONT>
					<% }
					 %>
 			      </td>
 			      <td width="80%">
 			      	<%=dEmp%>
 			      	<%if(empAtt.equals("1")) {%>
 			      		<input type="hidden" name="patemp" value="">
 			      	<%} else if(empAtt.equals("2")) {%>
 			      		<input type="hidden" name="patemp" value="<%=employment%>">
 			      	<%}%>
	            </td>
	            </tr>

				<%} else  { %>

				<input type="hidden" name="patemp" value="<%=employment%>">

				<% }} else { %>
				<tr>
					<td width="20%"><%=LC.L_Employment%><%--Employment*****--%></td>
					<td width="80%"><%=dEmp%></td>
				</tr>
				<%}%>




				<% if (hashPgCustFld.containsKey("education")) {
					int fldNumEdu= Integer.parseInt((String)hashPgCustFld.get("education"));
					String eduMand = ((String)cdoPgField.getPcfMandatory().get(fldNumEdu));
					String eduLable = ((String)cdoPgField.getPcfLabel().get(fldNumEdu));
					eduAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumEdu));


					if(eduMand == null) eduMand ="";
					if(eduAtt == null) eduAtt ="";

					if(!eduAtt.equals("0")) {
						if(eduLable !=null){
						%> <tr> <td width="20%">
						<%=eduLable%>
						<%} else {%> <tr> <td width="20%">
						<%=LC.L_Education%><%--Education*****--%>
					<%}

					if (eduMand.equals("1")) {
					%>
				   <FONT class="Mandatory" id="pgcustompatedu">* </FONT>
					<% }
					 %>
 			      </td>
 			      <td width="80%">
 			      	<%=dEdu%>
 			      	<%if(eduAtt.equals("1")) {%>
 			      		<input type="hidden" name="patedu" value="">
 			      	<%} else if(eduAtt.equals("2")) {%>
 			      		<input type="hidden" name="patedu" value="<%=education%>">
 			      	<%}%>
 			      </td>
 			     </tr>

				<%} else { %>

				<input type="hidden" name="patedu" value="<%=education%>">

                <%}	} else { %>
				<tr>
					<td width="20%"><%=LC.L_Education%><%--Education*****--%></td>
					<td width="80%"><%=dEdu%></td>
				</tr>
				<%}%>



			<%if (hashPgCustFld.containsKey("pnotes")) {

			int fldNumNotes = Integer.parseInt((String)hashPgCustFld.get("pnotes"));
			String notesMand = ((String)cdoPgField.getPcfMandatory().get(fldNumNotes));
			String notesLable = ((String)cdoPgField.getPcfLabel().get(fldNumNotes));
			String notesAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumNotes));


			disableStr = "";
			readOnlyStr ="";
			if(notesAtt == null) notesAtt ="";
			if(notesMand == null) notesMand ="";

			if(!notesAtt.equals("0")) {

			if(notesLable !=null){
			%><tr> 
			<td width="20%"><%=notesLable%>
			<%} else {%> <tr> <td width="20%">
			<%=LC.L_Notes%><%--Notes*****--%>
			<%}

			if (notesMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustompatnotes">* </FONT>
			<% }
			if(notesAtt.equals("1")) {
			  disableStr = "disabled class='readonly-input'"; }
		    else if (notesAtt.equals("2") ) {
			  readOnlyStr = "readonly"; }
  		   %>
		   </td>
		   <td colspan="2"  class="textareaheight">
  	 	   <textarea name="patnotes" rows=5 cols=50   <%=disableStr%> <%=readOnlyStr%>  MAXLENGTH = 4000><%=notes%></textarea>
		   </td>
		   </tr>
		   <%} else {%>
		   <textarea name="patnotes" rows="5" cols="50" style ="visibility:hidden"  MAXLENGTH="4000"><%=notes%></textarea>

		  <%} 
		} else { %>

  		  <tr>
			 <td width="20%"><%=LC.L_Notes%><%--Notes*****--%></td>
		  </tr>
		  <tr>
			 <td width="20%"></td>
			 <td width="80%"  class="textareaheight">
		  <textarea name="patnotes" rows=5 cols=50 MAXLENGTH = 4000><%=notes%></textarea>
		  </td>
		  </tr>
		<%}%>

		</table>
		<% if (fdaStudy == 1 && "M".equals(mode)){%>
		<hr/>
		<table width="100%" cellspacing="0" cellpadding="0" border="0" class="basetbl" >
			<tr>
		      <td width="20%"><%=LC.L_ReasonForChangeFDA%>
		     <!-- 	<%//if (fdaStudy == 1){%>&nbsp;<FONT class="Mandatory">* </FONT><%//} %>  --> 
		      </td>
			  <td  class="textareaheight"><textarea id="remarks" name="remarks" rows="3" cols="50" MAXLENGTH="4000"></textarea></td>
		  	</tr>
		</table>
		<%} %>
		<table width="100%" cellspacing="0" cellpadding="0" border="0" class="basetbl">
			<tr valing="middle">
				<%if ("L2_ON".equals(LC.L_Auth2_Switch)){%>
				<td width="20%"><%=LC.L_User_Name%>&nbsp;<FONT class="Mandatory">* </FONT></td>
		  		<td>
					<input type="password" id="userLogName" name="userLogName" value=""
				  	onkeyup="ajaxvalidate('misc:'+this.id,-1,'userLogNameMessage','<%=MC.M_Valid_UserName%>','<%=MC.M_Invalid_UserName %>','sessUserId')"/>
				  	<span id="userLogNameMessage"></span>
		  		</td>
		  		<%} %>
			</tr>
			<tr></tr>
		</table>
		<br>
		<input type="hidden" name="preorg" value="<%=organization%>">
		<Input type="hidden" id="fdaStudy" name="fdaStudy" value="<%=fdaStudy%>" >
		<%
		} //end of if body for page right
		else{
		%>
			<jsp:include page="accessdenied.jsp" flush="true"/>
		<%
		} //end of else body for page right
	}//end of if body for session
	else {%>
		<jsp:include page="timeout.html" flush="true"/>
	<%}%>
<div>
	<jsp:include page="bottompanel.jsp" flush="true"/>
</div>