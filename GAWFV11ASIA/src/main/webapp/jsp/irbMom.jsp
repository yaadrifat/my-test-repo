<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html><head></head>
<body onunload="alertUnsaved(document.mom)">
<script>
	function editMom()
{
	var formatTextAreaStyle = "width:620;height:550;";
 
	
	url="popupeditor.jsp?displayDIV=displayMom&formname=mom&formatfieldname=editBMomF&toolbar=Velos&length=32000000&formatTextAreaStyle="+formatTextAreaStyle;
	url=url+"&formatTextAreaWidth=1100&formatTextAreaHeight=900";
	
	     popupeditor=window.open(url,"popupeditor","toolbar=no,scrollbars=no,resizable=no,menubar=no,width=1450,height=1130,left=10,top=20");
	     popupeditor.focus();

	     document.mom.dirtyFlag.value = 'Y';

}

function saveMom(formobj)
{
	document.mom.dirtyFlag.value = 'N';
	formobj.saveMode.value="S";
	formobj.submit();
}

function deleteOldMom(formobj)
{
	formobj.editBMomF.value="";
	document.mom.dirtyFlag.value = 'N';
	formobj.saveMode.value="S";
	formobj.submit();
}

function alertUnsaved(formobj) {
	if (formobj.dirtyFlag.value != 'Y') {
		return;
	}
	document.mom.dirtyFlag.value = 'N';
	var yesSave = confirm("<%=MC.M_YouUnsavedData_SaveNow%>");/*var yesSave = confirm('You may have unsaved data. Save now? (Click OK to save or Cancel to discard.)');*****/
	if (yesSave) {
		return saveMom(formobj);
	}
	return;
}

</script>
	
<%@ page language = "java" import = "com.velos.eres.service.util.*,com.velos.eres.business.common.*,java.util.*"%>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<jsp:useBean id="sessionmaint" scope="session"	class="com.velos.eres.service.util.SessionMaint" />
<jsp:useBean id="subBoardJB" scope="page" class="com.velos.eres.web.submission.SubmissionBoardJB" />
<jsp:useBean id ="reportIO" scope="session" class="com.velos.eres.service.util.ReportIO"/>	
<% 
HttpSession tSession = request.getSession(true); 
 if (sessionmaint.isValidSession(tSession))	
 { 	
	
String revBoard =request.getParameter("revBoard");
String meetingDatePK =request.getParameter("meetingDatePK");
String saveMode =request.getParameter("saveMode");

if (StringUtil.isEmpty(saveMode))
{
	saveMode = "V";
}
if (saveMode.equals("S"))
{
	String momF=StringUtil.decodeString(request.getParameter("editBMomF"));
	//momF=(momF.replaceAll("\\n", "<br>")).replaceAll("&lt;br&gt;", "<br>");
	subBoardJB.updateReviewMeetingMOM(EJBUtil.stringToNum(meetingDatePK),momF);
}

String userId = (String) tSession.getValue("userId");
String mom = "";

mom = subBoardJB.getreviewMeetingMOM(EJBUtil.stringToNum(revBoard),EJBUtil.stringToNum(meetingDatePK));

		String printLink=reportIO.saveReportToDoc(mom,"htm","reporthtml");
		String wordLink =reportIO.saveReportToDoc(mom,"doc","reporthtml"); 
	
	%>
<form name="mom" action="irbMom.jsp" method="post">
	<A href="#" onclick="editMom()"><%=LC.L_Edit%><%--Edit*****--%></A>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<A href="#" onclick="return saveMom(document.mom);"><%=LC.L_Save_MeetingMinutes%><%--Save Meeting Minutes*****--%></A>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<A href="#" onclick="return deleteOldMom(document.mom);"><%=LC.L_Regenerate_MeetingMinutes%><%--Regenerate Meeting Minutes*****--%></A>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	
	<A href="<%=wordLink%>"><%=LC.L_Word_Format%><%--Word Format*****--%> </A>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<A href="<%=printLink%>"><%=LC.L_Printer_FriendlyFormat%><%--Printer Friendly Format*****--%></A>
 
		<DIV ID="displayMom"><%= mom %>
		 </DIV>
		
		<input type= "hidden" name="editBMomF" id="editBMomF" value="<%=StringUtil.encodeString(mom)%>">
		<input type= "hidden" name="saveMode" value="">
		
		<input type= "hidden" name="meetingDatePK" value="<%= meetingDatePK %>">
		<input type= "hidden" name="revBoard" value="<%=revBoard%>">		
        <input type= "hidden" name="dirtyFlag" value="N">
</form>
		<%
 
			
}//end of if body for session 
else { %>
 <jsp:include page="timeout.html" flush="true"/> 
 <% } %> 		
</body>