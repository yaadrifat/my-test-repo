<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML> 
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:include page="include.jsp" flush="true"/>	
<jsp:useBean id="submStatusB" scope="request" class="com.velos.eres.web.submission.SubmissionStatusJB"/>

<%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.business.common.*,java.util.*,java.text.*,com.velos.eres.service.util.*,com.velos.eres.web.account.AccountJB,com.velos.esch.business.common.*"%>
<%
  HttpSession tSession = request.getSession(true); 
  String eSign = request.getParameter("eSign");
  if (sessionmaint.isValidSession(tSession))
  {	
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%   
    String accId = (String) tSession.getValue("accountId");
    String studyId = "";
    int iaccId = EJBUtil.stringToNum(accId);
    String hiddenflag = request.getParameter("hiddenflag")==null?"":request.getParameter("hiddenflag");
   
    if(hiddenflag.equalsIgnoreCase("N")){
    	studyId = request.getParameter("studyId");
    	
    }else{
    	
    	  studyId = (String) tSession.getValue("studyId");
    }
    String selectedTab = StringUtil.htmlEncodeXss(request.getParameter("selectedTab"));
    int submId = EJBUtil.stringToNum(request.getParameter("submissionPK"));
    int submBoardId = EJBUtil.stringToNum(request.getParameter("submissionBoardPK"));
    String grpId = (String) tSession.getValue("defUserGroup");
    String ipAdd = (String) tSession.getValue("ipAdd");
    String usr = (String) tSession.getValue("userId");
    String oldESign = (String) tSession.getValue("eSign");
    String resultMsg = null;
    EIRBDao codelstsubm_stat = new EIRBDao();
    if(hiddenflag.equalsIgnoreCase("")){
    if(!oldESign.equals(eSign)) {
		
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} 
    else {
		 
       
	    
		if (submId < 1 || submBoardId < 1) {
            resultMsg = MC.M_Err_SavingData/*"There was an error while saving data"*****/;
            %>
            <jsp:include page="errorback.jsp" flush="true"><jsp:param name="errorMsg" value="<%=resultMsg%>"/></jsp:include>
            </BODY></HTML>
            <% return; // An error occurred; let's get out of here
        } // End of error
        
	    String statusDate = DateUtil.dateToString(Calendar.getInstance().getTime());
	    String statusNotes = request.getParameter("notes");
	   
	    int CurrentStatus =  codelstsubm_stat.getPkCodelstForPiResponded();
	    String oldCurrentStatus = CurrentStatus +"";
	    %>
	   
		
<%-- 	String piRespondedStatus = "";
		 
		    
	    CodeDao codeDao = new CodeDao();
     	piRespondedStatus =  String.valueOf(codeDao.getCodeId("subm_status", "pi_respond"));
     	
	    
		// set to subm status bean and create new status for PI responded
        submStatusB.setFkSubmission(String.valueOf(submId));
        submStatusB.setFkSubmissionBoard(String.valueOf(submBoardId)); 
        submStatusB.setFkSubmissionStatus(piRespondedStatus);
        submStatusB.setSubmissionEnteredBy(usr);
        submStatusB.setSubmissionStatusDate(statusDate);
        submStatusB.setSubmissionNotes(statusNotes);
        submStatusB.setCreator(usr);
        submStatusB.setLastModifiedBy(usr);
        submStatusB.setIpAdd(ipAdd);
        submStatusB.setSubmissionAssignedTo(null);
        submStatusB.setSubmissionCompletedBy(null);
        int newSubmStatusId = submStatusB.createSubmissionStatus();
		if (newSubmStatusId < 1) {
            resultMsg = MC.M_Err_SavingData/*"There was an error while saving data"*****/;
            %>
            <jsp:include page="errorback.jsp" flush="true"><jsp:param name="errorMsg" value="<%=resultMsg%>"/></jsp:include>
            </BODY></HTML>
            <% return; // An error occurred; let's get out of here
        } // End of error
       --%> 
        <%
        // set to subm status bean and create new
         codelstsubm_stat.getEnteredFormsLatestValue(submId,submBoardId);
        String form_name="";
        String form_seq="";
        int fk_formlib=0;
        int fk_filledform=0;
        int fk_formlibver=0;
        form_name=codelstsubm_stat.getIrbFormName();
        form_seq=codelstsubm_stat.getIrbFormSeq();
        fk_formlibver=codelstsubm_stat.getFormLibVer();
        fk_formlib=codelstsubm_stat.getFkformlibsumstat();
        fk_filledform=codelstsubm_stat.getFkfilledformlibsumstat();
        submStatusB.setFkSubmission(String.valueOf(submId));
        submStatusB.setFkSubmissionBoard(String.valueOf(submBoardId)); 
        submStatusB.setFkSubmissionStatus(oldCurrentStatus);
        submStatusB.setSubmissionEnteredBy(usr);
        submStatusB.setSubmissionStatusDate(statusDate);
        submStatusB.setSubmissionNotes(statusNotes);
        submStatusB.setCreator(usr);
        submStatusB.setLastModifiedBy(usr);
        submStatusB.setIpAdd(ipAdd);
        submStatusB.setFkfilledformlibsumstat(fk_filledform);
    	submStatusB.setFkformlibsumstat(fk_formlib);
    	submStatusB.setIrbFormName(form_name);
    	submStatusB.setIrbFormSeq(form_seq);
    	submStatusB.setFormLibVer(fk_formlibver);
        submStatusB.setSubmissionAssignedTo(null);
        submStatusB.setSubmissionCompletedBy(null);
         int newSubmStatusId = submStatusB.createSubmissionStatus();
         if(newSubmStatusId>0){
        	 
        	 codelstsubm_stat.updateIscurrentForFinalOutcomeTab(submId,submBoardId);
         }
		if (newSubmStatusId < 1) {
            resultMsg = MC.M_Err_SavingData/*"There was an error while saving data"*****/;
            %>
            <jsp:include page="errorback.jsp" flush="true"><jsp:param name="errorMsg" value="<%=resultMsg%>"/></jsp:include>
            </BODY></HTML>
            <% return; // An error occurred; let's get out of here
        } // End of error
        
        
        
        
        if (resultMsg == null) { resultMsg = MC.M_Data_SvdSucc/*"Data was saved successfully"*****/; }
	    
	    // All good now; show success message and refresh parent screen
	    String nextJsp = "irbpenditems.jsp";
	    String moreArgs = "";
	    if ("irb_act_tab".equals(selectedTab) || "irb_items_tab".equals(selectedTab) ||
	            "irb_saved_tab".equals(selectedTab)) {
	        nextJsp = "irbpenditems.jsp";
	    } else if ("irb_form_tab".equals(selectedTab)) {
	        nextJsp = "formfilledstudybrowser.jsp";
	        moreArgs = "&studyId="+studyId+"&submissionType="+StringUtil.htmlEncodeXss(request.getParameter("submissionType"));
	    }
%>
  <br><br><br><br>
  <p class = "successfulmsg" align = center><%=resultMsg%></p>
  <script>
  if (window.opener != null && window.opener.location != null && window.opener.location != undefined)
  {
  		window.opener.location.href = '<%=nextJsp%>?selectedTab=<%=selectedTab%>&mode=N<%=moreArgs%>';
  		setTimeout("self.close()",1000);
  }
  </script>
<%        
    
String updateuserchecked=codelstsubm_stat.getUserCheckedLatestValue(EJBUtil.stringToNum(studyId),usr,submId,submBoardId);
        
	
	
	
	
	}// end of if body for e-sign
    }
	
	else{
      	 int updatepksubmissionstatus = codelstsubm_stat.getPrevioustSubmissionStatus(submId,submBoardId); 
      	 int deletepksubmissionstatus = codelstsubm_stat.getdeleteCurrentSubmissionStatus(submId,submBoardId);
      	 if(deletepksubmissionstatus>0){
      		codelstsubm_stat.deleteSubmissionStatusFinalTab(deletepksubmissionstatus);
      		 
      		 
      	 }
      	 if(updatepksubmissionstatus>0)
      	 {
      		 codelstsubm_stat.updateSubmissionStatusFinalTab(updatepksubmissionstatus);
      		
      	 }
      	 
      	if (deletepksubmissionstatus>0) { resultMsg = MC.M_Data_DelSucc/*"Data deleted successfully."*****/; }
      	
      	String nextJsp = "irbsubmissions.jsp";
      %>	
       <br><br><br><br>
 <p class = "successfulmsg" align = center><%=resultMsg%></p>
 <script>
 if (window.opener != null && window.opener.location != null && window.opener.location != undefined)
 {
 		window.opener.location.href = '<%=nextJsp%>?selectedTab=<%=selectedTab%>&mode=N';
 		setTimeout("self.close()",2000);
 }
 </script>
     <% }
  } //end of if body for valid session
  else
  {
  %>
  <jsp:include page="timeout.html" flush="true"/>
  <%
  }
%>

</BODY>
</HTML>