<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</HEAD>


 
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="whichcss.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">

	
 function  validate(formobj)
 {	
    if (!(validate_col('e-Sign',formobj.eSign))) return false
// 	if(isNaN(formobj.eSign.value) == true) {
<%-- 		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/ --%>
// 		formobj.eSign.focus();
// 		return false;
// 	}	
 }
 
 function openOrgWindow(formobj)
  {
    var formorg1=formobj.formOrgIds.value;
	formorg1 = encodeString(formorg1);//KM-4Nov08
    windowName=window.open("selectOrg.jsp?openerform=selectForms&formRows=1&selectOrgs="+formorg1,"Information1","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=350,height=300");
	windowName.focus();
 }
 
 
 function openGroupWindow(formobj,counter,formRows) {
    var selGrpNames1=formobj.selGrpIds.value;
	selGrpNames1 =  encodeString(selGrpNames1);//KM-4Nov08
    windowName=window.open("selectGroup.jsp?openerform=selectForms&formRows=1&selectGroups="+selGrpNames1,"Information1","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=350,height=300");
	windowName.focus();
}

function getGroup(formobj,dispFld,dataFld)  
   {  
   	var grpNames1=formobj.grpIds.value;
	grpNames1 = encodeString(grpNames1);//KM-4Nov08
     if (formobj.noshare.checked==true){  
       windowName=window.open("selectGroup.jsp?openerform=selectForms&formRows=1&idFld="+dataFld+"&dispFld="+dispFld+"&selectGroups="+grpNames1,"Information1","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=350,height=300");  
       windowName.focus(); 
       } else  
       {  
       alert("<%=MC.M_SelKeepResp_BeforeProc%>");/*alert("Please select 'Keep response private to author' before proceeding");*****/  
       return;  
       }  
   }  
      
   function getUser(formobj)  
   {  
       if (formobj.noshare.checked)  
       {  
      
         var names = formobj.usrNames.value;  
      
         var ids = formobj.usrIds.value;  
      
          
          
      
          
      
         while(names.indexOf(' ') != -1){  
      
             names = names.replace(' ','~');  
      
         }  
      
          
      
        
      
   //alert(windowToOpen);  
      
         window.open("multipleusersearchdetails.jsp?dataFld=usrIds&dispFld=usrNames&fname=&lname=&from=selectForms&mode=initial&ids=" + ids + "&names=" + names ,"User","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")  
   }   
   else {  
	   alert("<%=MC.M_SelKeepResp_BeforeProc%>");/*alert("Please select 'Keep response private to author' before proceeding");*****/   
       return;  
   }  
      
   ;}  
      
   function keepPrivate(formobj)  
   {  
    if (formobj.noshare.checked)  
        formobj.noshareId.value="1";  
       else   
       {  
       formobj.noshareId.value="0";  
       formobj.usrIds.value="";  
       formobj.grpIds.value="";  
       formobj.usrNames.value="";  
       formobj.grpNames.value="";  
       }  
        
        
   }  


</SCRIPT>



<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
 
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="objShareB" scope="request" class="com.velos.eres.web.objectShare.ObjectShareJB"/> 
<jsp:useBean id="catLibJB" scope="request"  class="com.velos.eres.web.catLib.CatLibJB"/>
<jsp:useBean id="formLibB" scope="request" class="com.velos.eres.web.formLib.FormLibJB"/>
<jsp:useBean id="linkedFormsJB" scope="request"  class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />

<%@ page language = "java" import = "com.velos.eres.business.site.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*"%>
<DIV class="popDefault" id="div1">

<%
 HttpSession tSession = request.getSession(true); 
 
 if (sessionmaint.isValidSession(tSession))
 {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/> 
<jsp:include page="include.jsp" flush="true"/>

<%

	 int accountId = EJBUtil.stringToNum((String) tSession.getValue("accountId"));
	 String userId = (String) tSession.getValue("userId");
	 String linkedFormIdStr  =request.getParameter("linkedFormId") ; 
	 String statCodeId = request.getParameter("statCodeId");
	 int istatus = EJBUtil.stringToNum(statCodeId);
	 String changeVal = request.getParameter("changeVal");
	 String status = "";
	CodeDao cd = new CodeDao();
		int wipId=cd.getCodeId("frmstat","W");
		int activeId=cd.getCodeId("frmstat","A");
		int lockdownId = cd.getCodeId("frmstat","L");
		int deactivateId = cd.getCodeId("frmstat","D");
		int offlineId = cd.getCodeId("frmstat","O");
		int migrationId = cd.getCodeId("frmstat","M"); //KM
		String makeDisabled = ""; 
		if(wipId==istatus)
			{
			status = "WIP";
			}
		else if(activeId==istatus)
			{
				status = "Active";
			}
		else if(lockdownId == istatus)
			{
				status = "Lockdown";			
			}
		else if(deactivateId == istatus)
			{
				status = "Deactivated";			
			}
		else if(offlineId == istatus)
			{
				status = "Offline";			
			}
        else if(migrationId == istatus)
			{
				status = "MIP";
			}

	//KM-Modified for the issue #2888.
	if(status.equals("Offline") || status.equals("Lockdown")  || status.equals("Active") || status.equals("MIP") )
	{
		changeVal = "no";
		makeDisabled ="disabled";
	 }



	 int linkedFormId = EJBUtil.stringToNum( linkedFormIdStr);
	 int studyIdi  = 0 ;
	 LinkedFormsDao linkedFormsDao = new LinkedFormsDao( ); 
	 String lfDisplayType = "";
	 String lfEntry = "";
	 String studyId= "";
	 String orgNames = "";
	 String grpNames = "";
	 String formOrgIds="";
	 String grpIds="";
	 ArrayList formIds = new ArrayList();
	  
		
	 linkedFormsJB.setLinkedFormId(linkedFormId) ;
	 linkedFormsJB.getLinkedFormDetails();
	 linkedFormsDao =  linkedFormsJB.getLinkedFormsDao(); 
 
	 orgNames=(  linkedFormsDao.getOrgNames()   == null)?"":linkedFormsDao.getOrgNames( ) ;	
	 formOrgIds=(  linkedFormsDao.getOrgIds()   == null)?"":linkedFormsDao.getOrgIds( ) ;	 
  	 grpNames=(  linkedFormsDao.getGrpNames()   == null)?"":linkedFormsDao.getGrpNames( ) ;
	 grpIds=(  linkedFormsDao.getGrpIds()   == null)?"":linkedFormsDao.getGrpIds( ) ;
	
	 lfDisplayType = linkedFormsJB.getLFDisplayType(); 
	 lfEntry = linkedFormsJB.getLFEntry();
	 if (lfEntry==null) lfEntry=""; 
	 studyId = linkedFormsJB.getStudyId() ;
	 if (studyId==null) studyId="0";
	 
	 String formLinkedFrom = linkedFormsJB.getLnkFrom();
	 studyIdi =  EJBUtil.stringToNum(studyId) ;%>
	 <title>
		 <%
		  if(( lfDisplayType.trim()).equals("S")){%> 
	          <%=MC.M_EdtFrmLink_Std%><%--Edit the Form Linked to <%=LC.Std_Study%>*****--%> 
			<%}
			else if( lfDisplayType.equals("SP") ) 
			 { %>
			  <%=MC.M_EdtFrmLink_Pat%><%--Edit the Form Linked to <%=LC.Pat_Patient%>*****--%>			
			<%}%>
	</title>	<%
	 int formId = EJBUtil.stringToNum(linkedFormsJB.getFormLibId());
	 formLibB.setFormLibId(formId);
	 formLibB.getFormLibDetails();
	 String formName = formLibB.getFormLibName();

	 String userIdFromSession = (String) tSession.getValue("userId");

	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");

    String studyNum = "";
    int pageRight = 0;
	
	StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");	
	pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMMANAG"));
	
	
	String accId=(String) tSession.getValue("accId");  
          
          
        /*retrieve object share ids*/  
          
        objShareB.getObjectShareDetails(linkedFormId,"U","5",userIdFromSession, accId);  
        
           String usrNames=objShareB.getShrdWithNames();  
           String usrIds=objShareB.getShrdWithIds();  
           usrIds=(usrIds==null)?"":usrIds;  
           usrNames=(usrNames==null)?"":usrNames;  
            
           usrNames=usrNames.replace(',',';');  
            
           objShareB.getObjectShareDetails(linkedFormId,"G","4",userIdFromSession, accId);  
        
           String grpNamesResp=objShareB.getShrdWithNames();  
           String grpIdsResp=objShareB.getShrdWithIds();  
           grpIdsResp=(grpIdsResp==null)?"":grpIdsResp;  
           grpNamesResp=(grpNamesResp==null)?"":grpNamesResp;  
            
           objShareB.getObjectShareDetails(linkedFormId,"P","6",userIdFromSession, accId);  
           String noshareId=objShareB.getShrdWithIds();  
           noshareId=(noshareId==null)?"":noshareId;  
           System.out.println("NoshareId retrieved" + noshareId);  
           int len=noshareId.length();  
	   /*End object share id retrieve*/	

	if(studyIdi!=0)
	{
		 studyB.setId(studyIdi);
		 studyB.getStudyDetails();
		 studyNum = studyB.getStudyNumber();
	}	

    if (pageRight >= 4) 
    {

	String deFormOrg = "";
	String deFormOrgId = "";
	
	String deFormGrp = "";
	String deFormGrpId = "";
	
	String radioName = "";
	String formCharName = "";
	String accFormStudy = "";
%>
		
 <p class="sectionHeadings"> <%=MC.M_EdtFrm_StdAcc%><%--Edit Forms >> <%=LC.Std_Study%> Account*****--%> </p>

 <P><%=LC.L_Frm_Name%><%--Form Name*****--%>: <%=formName%> </P>
 <P><%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%>: <%=studyNum%> </P> 

<Form name="selectForms" id="selStdForms" method="post" action="attributesForStudyAccountSubmit.jsp" onsubmit="if (validate(document.selectForms)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

<input type="hidden" name="formLinkedFrom" value="<%=formLinkedFrom%>">
<input type="hidden" name="linkedFormId"  value="<%=linkedFormIdStr%>" >
<input type="hidden" name="accFormStudy" value=<%=studyId%>>
<input type="hidden" name="status" value="<%=status%>">
<input type = "hidden" name="hdnDispFormLink" value="<%=lfDisplayType%>">
<input type="hidden" name="hdnCharName" value="<%=lfEntry%>">
	<table width = "80%" border = "0" cellspacing="0" cellpadding="0">
	 <tr class="browserEvenRow"> <td ><%=LC.L_Disp_FormLink%><%--Display Form Link*****--%></td>
	 <td>
<%	  if(( lfDisplayType.trim()).equals("S")      ) 
	     { %>
			 <input type="radio" name="dispFormLink" value="S" Checked <%=makeDisabled%>><%=LC.L_Study%><%--<%=LC.Std_Study%>*****--%><br>
			 <input type="radio" name="dispFormLink" value="SP" <%=makeDisabled%>><%=LC.L_Patient%><%--<%=LC.Pat_Patient%>*****--%>
		<%}
			if( lfDisplayType.equals("SP") ) 
		{ %>
			 <input type="radio" name="dispFormLink" value="S"<%=makeDisabled%> ><%=LC.L_Study%><%--<%=LC.Std_Study%>*****--%><br>
			 <input type="radio" name="dispFormLink" value="SP" Checked <%=makeDisabled%>><%=LC.L_Patient%><%--<%=LC.Pat_Patient%>*****--%>
		<%}

%>
	 </td> 
	 </tr>
	 <tr class="browserOddRow"><td ><%=LC.L_Characteristic%><%--Characteristic*****--%></td>
 	 <td> 
	 <table border = "0" cellspacing="0" cellpadding="0">
	 <%						
	 if ( lfEntry.equals("M")) 
	  { %>		 <tr> 
				 <td><input type="radio" name="charName" value="M" Checked <%=makeDisabled%>><%=LC.L_Multi_Entry%><%--Multiple entry*****--%></td></tr>
				<!--  <input type="radio" name="charName" value="O" >Only once<br> -->
				 <tr><td><input type="radio" name="charName" value="E" <%=makeDisabled%>><%=LC.L_Only_OnceEditable%><%--Only once (Editable)*****--%></td></tr>			
                  <%}/* if ( lfEntry.equals("O")) 
				   //{ %>
				  <!-- <input type="radio" name="charName" value="M">Multiple entry<br>
				 <input type="radio" name="charName" value="O" Checked>Only once<br>
				 <input type="radio" name="charName" value="E">Only once (Editable) -->
				  <%/*}*/ if ( lfEntry.equals("E")) 
				   { %><tr><td>
				  <input type="radio" name="charName" value="M" <%=makeDisabled%>><%=LC.L_Multi_Entry%><%--Multiple entry*****--%></td></tr>
				 <!-- <input type="radio" name="charName" value="O" >Only once<br> -->
				 <tr><td><input type="radio" name="charName" value="E" Checked <%=makeDisabled%>><%=LC.L_Only_OnceEditable%><%--Only once (Editable)*****--%></td>
				 </tr>
				 <% } %>
				 <tr>  
                    <%if (len>0){%>  
                    <td><input Type="Checkbox" name="noshare" onclick="keepPrivate(document.selectForms)" checked><%=MC.M_KeepResp_PrivateToAuth%><%--Keep response private to author*****--%></td>  
                    <input type="hidden" name="noshareId" value="1">  
                     <%}else{%>  
                    <td><input Type="Checkbox" name="noshare" onclick="keepPrivate(document.selectForms)"><%=MC.M_KeepResp_PrivateToAuth%><%--Keep response private to author*****--%></td>  
                    <input type="hidden" name="noshareId" value="0">  
                    <% } %>    </tr>           
<tr><td colspan=2><i>(<%=MC.M_IfKeepRespOpt_SpcfyAces%><%--If 'Keep response private to author' option is selected, specify user/group with access to all responses*****--%>)</i></td></tr>  
           <tr>  
           <td><%=LC.L_User%><%--User*****--%>&nbsp;&nbsp; <input type="text" name="usrNames"  size = "50" value="<%=usrNames%>" readonly >  
           <A href="#" onClick="getUser(document.selectForms)"><%=LC.L_Select%><%--Select*****--%></A>     
           <input type="hidden" name="usrIds" value="<%=usrIds%>">  
           </td>                             
           </tr>  
           <tr>  
           <td><%=LC.L_Group%><%--Group*****--%>&nbsp;<input type="text" name="grpNames"  size = "50" value="<%=grpNamesResp%>" readonly >  
           <A href="#" onClick="getGroup(document.selectForms,'grpNames','grpIds' )"><%=LC.L_Select%><%--Select*****--%></A>     
           <input type="hidden" name="grpIds" value="<%=grpIdsResp%>">  
           </td>                             
           </tr>  
            
                
               <tr></tr></table>  

				 
		</td>	
	 </tr>
	 <tr class="browserEvenRow"><td ><%=LC.L_Filters%><%--Filters*****--%></td>
	 <td>
	 <table>
	 <tr>
	 <td width="15%"><%=LC.L_Organization%><%--Organization*****--%> </td>
	 <td><input type="text" name="formOrg"  size = "50" value="<%=orgNames%>" readonly >
	 <A href="#" onClick="openOrgWindow(document.selectForms )"><%=LC.L_Select%><%--Select*****--%></A>	<!--km-->
	 <input type="hidden" name="formOrgIds" value=<%=formOrgIds%>>
	 <input type="hidden" name="oldFormOrgIds" value=<%=formOrgIds%>>
 	 <br>	 	
	 </td>							
	 </tr>
	 <tr>
	 <td><%=LC.L_Group%><%--Group*****--%></td>
	 <td> <input type="text" name="selGrpNames"  size = "50" value="<%=grpNames%>" readonly >
	 <A href="#" onClick="openGroupWindow(document.selectForms)"><%=LC.L_Select%><%--Select*****--%></A></td><!--km-->	
	 <input type="hidden" name="selGrpIds" value="<%=grpIds%>">
	 <input type="hidden" name="oldGrpIds" value="<%=grpIds%>">
	 </td>							
	 </tr>
	 </table>
	 </td>
	 </tr>
	
	</table> 		

<br>
<%
	if ((formLinkedFrom.equals("S") && (pageRight==4 || pageRight==5)) || (formLinkedFrom.equals("A") && (pageRight==4 || pageRight==5)))
	{}
	else if(!status.equals("Deactivated")){%>

	
	
	<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="selStdForms"/>
			<jsp:param name="showDiscard" value="N"/>
	</jsp:include>
	
	
	
<%}
%>	<input type="hidden" name="studyIdi" value=<%=studyIdi%>>
</Form>

	 <%
		
	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right
	}
else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
</div>
<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</body>

</html>


			
