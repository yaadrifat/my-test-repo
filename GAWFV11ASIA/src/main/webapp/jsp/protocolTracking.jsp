<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=MC.M_Pcol_TrackInEres%><%--Protocol Tracking in eResearch*****--%></title>

<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<Link Rel=STYLESHEET HREF="common.css" type=text/css>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<SCRIPT Language="javascript">
 function  validate(){
     formobj=document.protocolTracking
     if (!(validate_col('Date',formobj.appRenDate))) return false
     if (!(validate_col('Protocol Status',formobj.protocolStatus))) return false
     if (!(validate_col('Documented By',formobj.documentedBy))) return false
     if (!(validate_col('Account Type',formobj.accountType))) return false
     if (!(validate_col('Primary Specilaity',formobj.primarySpeciality))) return false
     if (!(validate_col('Organization Name',formobj.siteName))) return false
     }
</SCRIPT>


<body>

<jsp:include page="panel.htm" flush="true"/>

<DIV class="formDefault">

<Form name="protocolTracking" method="post" action="sineup.jsp" onsubmit="return validate()">

<P class = "sectionHeadings">
 <%=MC.M_StdAprvl_RnewDets%><%--Study Approval/Renewal Details*****--%>
</P>

<table width="500" cellspacing="0" cellpadding="0" >
  <tr>
    <td width="300">
       <%=LC.L_Date%><%--Date*****--%> <FONT class="Mandatory">* </FONT>
    </td>
    <td>
	<input type="text" name="appRenDate" size = 15 MAXLENGTH = 20>
    </td>
  </tr>
   <tr>
   <td></td>
    <td>
	<A href=""><%=MC.M_Selc_DateFromCal%><%--Select Date from Calendar*****--%></A>
    </td>
  </tr>
  <tr>
    <td width="300">
       <%=LC.L_AppOrRen%><%--App/Ren*****--%> #
    </td>
    <td>
	<input type="text" name="appRenNum" size = 20 MAXLENGTH = 20>
    </td>
  </tr>
   <tr>
    <td width="300">
       <%=LC.L_AppOrRen_Dets%><%--App/Ren Status*****--%>
    </td>
    <td>
	<input type="text" name="appRenStatus" size = 20 MAXLENGTH = 20>
    </td>
  </tr>
   <tr>
    <td width="300">
       <%=LC.L_Pcol_Status%><%--Protocol Status*****--%> <FONT class="Mandatory">* </FONT>
    </td>
    <td>
	<input type="text" name="protocolStatus" size = 20 MAXLENGTH = 20>
    </td>
  </tr>
 <tr>
    <td width="300">
       <%=LC.L_Hspn%><%--HSPN*****--%> #
    </td>
    <td>
	<input type="text" name="hspnNum" size = 15 MAXLENGTH = 15>
    </td>
  </tr>
 <tr>
    <td width="300">
       <%=LC.L_Documented_By%><%--Documented By*****--%> <FONT class="Mandatory">* </FONT>
    </td>
    <td>
	<input type="text" name="documentedBy" size = 15 MAXLENGTH = 15>
    </td>
  </tr>
 <tr>
    <td width="300">
       <%=LC.L_Notes%><%--Notes*****--%>
    </td>
    <td>
	<TextArea name="protocolNotes" rows=3 cols=40 MAXLENGTH = 2000>
	</TextArea>
    </td>
  </tr>
</table>

<br>
<table width="200" cellspacing="0" cellpadding="0">
<td align=right>
<input type="Submit" name="submit" value="<%=LC.L_Submit%><%--Submit*****--%>">
</td>
<td align=right>
<input type="Reset" name="submit" value="<%=LC.L_Reset%><%--Reset*****--%>">
</td>

</tr>
</table>
</Form>
</div>
</body>
</html>
