<!-- 
	File Name: updateTeamStatus.jsp

	Author : Manimaran
	
	Created Date	: 08/09/2006

	Last Modified Date : 

	Purpose	: For July-August Enhancement(S4)
-->
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
	<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
	<jsp:useBean id="statusB" scope="request" class="com.velos.eres.web.statusHistory.StatusHistoryJB"/>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<jsp:useBean id="teamB" scope="request" class="com.velos.eres.web.team.TeamJB"/>
	<%@ page language = "java" import = "java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.business.common.*,com.velos.eres.service.util.*"%>
<%
    boolean updateTeamRecord = false;
	String src = null;
	src = request.getParameter("srcmenu");
	String eSign = request.getParameter("eSign");	
	String mode = request.getParameter("mode");
	String moduleTable = request.getParameter("moduleTable");
	String modulePk = request.getParameter("modulePk");
	String status = request.getParameter("status");
	String endStatusDate = request.getParameter("EndStatusDate");
	String teamStat=request.getParameter("teamStat");	
	String newStatCode=request.getParameter("newStatCode");	
	String isCurrent=request.getParameter("isCurrent");
	int isCurrentStat=EJBUtil.stringToNum(isCurrent);
	String currentStatId=request.getParameter("currentStatId");
	if (mode.equals("M")) {
   	    status = request.getParameter("statCode");
	}
	//Added by Gopu to fix the bugzilla Issue #2711	
	ArrayList subTypeStatus =null;
	CodeDao cdao = new CodeDao();
	cdao.getCodeValuesById(EJBUtil.stringToNum(status));
	subTypeStatus = (cdao.getCSubType());
	String subTypeStat = subTypeStatus.get(0).toString();	
	teamStat = subTypeStat;
	newStatCode = subTypeStat;
	/////
	String statusDate = request.getParameter("statusDate");
	String notes = request.getParameter("notes");		
	if (EJBUtil.isEmpty(mode))
	    mode = "N";
	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession)) {
%>
	    <jsp:include page="sessionlogging.jsp" flush="true"/> 
<%   
   	String oldESign = (String) tSession.getValue("eSign");	
	if(!oldESign.equals(eSign)) {
%>
		<jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {
			String ipAdd = (String) tSession.getValue("ipAdd");
			String usr = (String) tSession.getValue("userId");
			String statusId="";	
			statusId = request.getParameter("statusId"); 
			statusB.setStatusId(EJBUtil.stringToNum(statusId));
			statusB.getStatusHistoryDetails();
			
			if (mode.equals("N")) {
				if (isCurrentStat==1)
				   statusB.setIsCurrentStat("0");
				statusB.setModifiedBy(usr);
				statusB.setStatusEndDate(endStatusDate);
				statusB.updateStatusHistoryWithEndDate();			
//JM: 10Nov2006: 
				
				teamB.setId(EJBUtil.stringToNum(modulePk));
				teamB.getTeamDetails();
				teamB.setModifiedBy(usr);
				teamB.setIpAdd(ipAdd);
				
				if ( isCurrentStat == 1 ){
					
					updateTeamRecord = true;
					teamB.setTeamStatus(newStatCode);
					
				}
				
				
				if (newStatCode.equals("Active"))
				{
					teamB.setTeamUserType("D");
					updateTeamRecord = true;
				}	
				else if (newStatCode.equals("Deactivated"))
				{
					teamB.setTeamUserType("X");
					updateTeamRecord = true;
				}
				
				if (updateTeamRecord)
				{
					teamB.updateTeam();
				}	
				 
			}
			statusB.setIsCurrentStat(isCurrent);
			statusB.setStatusModuleId(modulePk);
			statusB.setStatusModuleTable(moduleTable);
			statusB.setStatusCodelstId(status);
			statusB.setStatusStartDate(statusDate);
			statusB.setStatusNotes(notes);
			statusB.setIpAdd(ipAdd);
			if (mode.equals("N")) {
				statusB.setCreator(usr);
				statusB.setModifiedBy(usr);
				statusB.setStatusEndDate("");
				statusB.setStatusHistoryDetailsWithEndDate();			
			} else if (mode.equals("M")) {
				statusB.setRecordType("M");	
				statusB.setModifiedBy(usr);
				statusB.updateStatusHistoryWithEndDate();
				if ((isCurrentStat==1) && (!statusId.equals(currentStatId))) {
					statusB.setStatusId(EJBUtil.stringToNum(currentStatId));
						statusB.getStatusHistoryDetails();
						statusB.setIsCurrentStat("0");
						statusB.updateStatusHistoryWithEndDate();
						teamB.setId(EJBUtil.stringToNum(modulePk));
						teamB.getTeamDetails();
						teamB.setTeamStatus(teamStat);
						teamB.setModifiedBy(usr);
						 	
						teamB.updateTeam();
				}				
			}	
%>
			  <br><br><br><br><br>
				 <p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%> </p>
			  <script>
					window.opener.location.reload();
					setTimeout("self.close()",1000);
			  </script>	  
		<%
		}//end of if for eSign check
	}//end of if body for session
	else {
%>
	  <jsp:include page="timeout.html" flush="true"/>
<%
	}
%>
</BODY>
</HTML>





