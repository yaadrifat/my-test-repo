<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>

<%@page import="com.velos.eres.web.studyRights.StudyRightsJB"%><html>
<jsp:useBean id="siteB" scope="request" class="com.velos.eres.web.site.SiteJB"/>
<jsp:useBean id="useRB" scope="page" class="com.velos.eres.web.user.UserJB" />
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@page language = "java" import="com.velos.eres.service.util.*" %> 

<% 
String commonformflag=request.getParameter("commonformflag")==null?"":request.getParameter("commonformflag");
if(commonformflag.equalsIgnoreCase("USR")){%>
<title><%--User Form Data*****--%><%=LC.L_Usr_FormData%></title>
<%}else if(commonformflag.equalsIgnoreCase("ORG")){ %>
<title><%--Organiztion Form Data*****--%><%=LC.L_Org_FormData%></title>
<%}else if(commonformflag.equalsIgnoreCase("NTW")) {%>
<title><%--Network Form Data*****--%><%=LC.L_Net_FormData%></title>
<%}else{%>
<title><%--*****Account Form Data--%><%=LC.L_Acc_FormData%></title>
<%} %>

<SCRIPT>

var screenWidth = screen.width;
var screenHeight = screen.height;

function openUserSearch(formobj,colName) {
//alert(formobj);
//alert(colName);
  //  windowName=window.open("formusersearch.jsp?formobj="+formobj+"&colName="+colName,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
//	windowName.focus();
}

function openPrintWin(formId,filledFormId,formLibVerNumber) {
  // windowName=window.open("formprint.jsp?formId="+formId+"&filledFormId="+filledFormId+"&formLibVerNumber="+formLibVerNumber+"&linkFrom=A","Information1","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
  // windowName.focus(); 
 var params = { 'formId' : formId, 'filledFormId': filledFormId,'formLibVerNumber' :formLibVerNumber,'linkFrom' : 'A'};
 
 var form = document.createElement("form");
 form.setAttribute("method", "post");
 form.setAttribute("action", "formprint.jsp");
 form.setAttribute("target", "Information1");
 for (var i in params)
 {
   if (params.hasOwnProperty(i))
   {
     var input = document.createElement('input');
     input.type = 'hidden';
     input.name = i;
     input.value = params[i];
     form.appendChild(input);
   }
 }
 document.body.appendChild(form);
 window.open(" ", "Information1", "toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200");
 form.submit();
 document.body.removeChild(form);
}
function openPrintWin1(formId,filledFormId,formLibVerNumber,commonformflag) {
	  //  windowName=window.open("formprint.jsp?formId="+formId+"&filledFormId="+filledFormId+"&formLibVerNumber="+formLibVerNumber+"&commonformflag="+commonformflag,"Information1","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
	  // windowName.focus(); 
	   var params = { 'formId' : formId, 'filledFormId': filledFormId,'formLibVerNumber' :formLibVerNumber,'commonformflag' : commonformflag};
	   
	   var form = document.createElement("form");
	   form.setAttribute("method", "post");
	   form.setAttribute("action", "formprint.jsp");
	   form.setAttribute("target", "Information1");
	   for (var i in params)
	   {
	     if (params.hasOwnProperty(i))
	     {
	       var input = document.createElement('input');
	       input.type = 'hidden';
	       input.name = i;
	       input.value = params[i];
	       form.appendChild(input);
	     }
	   }
	   document.body.appendChild(form);
	   window.open(" ", "Information1", "toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200");
	   form.submit();
	   document.body.removeChild(form);
	}
function openQueries(formId,filledFormId) {
	  windowName=window.open("addeditquery.jsp?formId="+formId+"&filledFormId="+filledFormId+"&from=3","Information1","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
   windowName.focus();
   
}
function  openReport(formobj,reportId,reportName,repFilledFormId,act,displayMode){
	/*formobj.repId.value = reportId;
	formobj.repName.value = reportName;
	formobj.repFilledFormId.value = repFilledFormId;
	formobj.filterType.value = "A";*/
	
	formobj.action="repRetrieve.jsp?repId="+reportId+"&repName="+reportName+"&repFilledFormId="+repFilledFormId+"&filterType=A&displayMode="+displayMode;
	formobj.target="_new";
	formobj.submit();
	formobj.action=act;
	formobj.target="";
	}
function confirmBox(pageRight,isLockdown,formStatusDesc) 
{
	if (f_check_perm(pageRight,'E') == true) 
		{
			msg="<%=LC.L_Del_ThisResponse%>";/*msg="Delete this Response?";*****/
			if(isLockdown==true){
				var paramArray = [formStatusDesc];
				alert(getLocalizedMessageString("M_CantDelete_LockDownFrmResp",paramArray));/*Lockdown status form response cannot be deleted.*****/
			      return false;}
			  else{
				if (confirm(msg)) 
				{
				   return true;
				}
				else
				{
				   return false;
				}
			  }
		} 
		else 
		{
			return false;
		}
}	

</SCRIPT>
</head>

<%
String src= request.getParameter("srcmenu");
String calledFromForm = "";
       calledFromForm = request.getParameter("calledFromForm");	 
       String modpk= request.getParameter("modpk")==null?"":request.getParameter("modpk");       
       String UserFName="";
       String UserLName="";
       String siteName="";
       String siteType="";
       String mainNetwork="";
       String desccrip = null;
       ArrayList orgName=null;
       ArrayList relationshipType=null;
       String UserPrimOrg=request.getParameter("UserPrimOrg")==null?"":request.getParameter("UserPrimOrg");
       if(commonformflag.equalsIgnoreCase("USR")){
    		useRB.setUserId(EJBUtil.stringToNum(modpk));
    		useRB.getUserDetails();
    		UserFName = useRB.getUserFirstName();
    		UserLName = useRB.getUserLastName();
    		
    		siteB.setSiteId(EJBUtil.stringToNum(UserPrimOrg));
    		siteB.getSiteDetails();
    		siteName = siteB.getSiteName();
    	}
       else if(commonformflag.equalsIgnoreCase("ORG")){
    		siteB.setSiteId(EJBUtil.stringToNum(modpk));
    		siteB.getSiteDetails();
    		siteName = siteB.getSiteName();
    		siteType=siteB.getSiteCodelstType();
    		CodeDao cdao=new CodeDao();
    		desccrip=cdao.getCodeDescription(EJBUtil.stringToNum(siteType));
    	}
    	else if(commonformflag.equalsIgnoreCase("NTW")){	
    		NetworkDao nwtDao=new NetworkDao();
    		nwtDao.getAppndxNtOrgDetails(StringUtil.stringToNum(modpk));
    		mainNetwork=nwtDao.getMainNetwork();
    		orgName=nwtDao.getSiteNameList();
    		relationshipType=nwtDao.getNetworkTypeDescList();
    	}
String eSignRequired = "";	   

	String fldMode = request.getParameter("fldMode");	   
	
	String outputTarget = "";

	outputTarget = request.getParameter("outputTarget");
	
	if(StringUtil.isEmpty(outputTarget))
	{
		outputTarget = "";
	}   

 	 String specimenPk = "";
    specimenPk = request.getParameter("specimenPk");
    
     if (StringUtil.isEmpty(specimenPk))
	 {
	 	specimenPk = "";
	 }  
     
      if (StringUtil.isEmpty(calledFromForm))
      {
     	calledFromForm = "";
      }
   
   if (calledFromForm.equals("")&& commonformflag.equalsIgnoreCase(""))   
   {

%>

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="srcmenu" value="<%=src%>"/>
</jsp:include>   
 <% }
 else
 { %>
 	<jsp:include page="include.jsp" flush="true"/>
 	
 	<%	
 }
 %>	

<%@page language = "java" import="com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.business.common.SaveFormDao,com.velos.eres.service.util.*,com.velos.eres.business.group.*,com.velos.eres.business.common.CodeDao,java.util.*,com.velos.eres.business.common.CtrlDao,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration,com.velos.eres.web.user.UserJB,com.velos.eres.business.common.*" %> 
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="lnkformsB" scope="request" class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
<jsp:useBean id="formLibB" scope="request" class="com.velos.eres.web.formLib.FormLibJB"/>
<jsp:useBean id="lnkformB" scope="request" class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>


<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>

<body style="overflow:auto;">
<script>
checkQuote='N';
</script>

<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div> 
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<%
if (calledFromForm.equals(""))   
   {
	if(commonformflag.equalsIgnoreCase("USR")){
		%>
		<table width="100%" align="center">
		<tr>
			<td>
			<%=LC.L_UserName%><%--<%=LC.L_UserName%> Number*****--%>:<%=UserFName+" "+UserLName%>&nbsp;&nbsp;
			</td>
		</tr>
		<tr>
			<td>
			<%=MC.M_Users_PrimaryOrg%><%--primary organization*****--%>:<%=siteName%>&nbsp;&nbsp;
		    </td>
		</tr>
		</table>
	<%}else if(commonformflag.equalsIgnoreCase("ORG")){ %>
		<table width="100%" align="center">
		<tr>
			<td>
			<B><%=LC.L_Organization_Name%><%--<%=LC.L_UserName%> Number*****--%>:</B><%=siteName %>&nbsp;&nbsp;
			</td>
		</tr>
		<tr>
			<td>
			<B><%=LC.L_Organization_Type%><%--primary organization*****--%>:</B><%=desccrip %>&nbsp;&nbsp;
			</td>
		</tr>
		</table>
	<% }else if(commonformflag.equalsIgnoreCase("NTW")){ %>
		<table width="100%" align="center">
		<tr>
	    	<td>
			<B><%=LC.L_Network_Name%></B><%--<%=LC.L_UserName%> Number*****--%><%=mainNetwork %>&nbsp;&nbsp;
			</td>
		</tr>
		<tr>
			<td>
			<B><%=LC.L_Organization_Name%>:</B><%=orgName.get(0)==null?"":orgName.get(0)  %>&nbsp;&nbsp;
	    	</td>
		</tr>
		<tr>
			<td>
			<B><%=LC.L_Relation_Type%>:</B><%=relationshipType.get(0)==null?"":relationshipType.get(0) %>&nbsp;&nbsp;
			</td>
		</tr>
</table>

<% }
%>
<SCRIPT LANGUAGE="JavaScript">
if (navigator.userAgent.indexOf("MSIE") == -1){
	if(screenWidth>1280 || screenHeight>1024){	
		document.write('<div class="formDefault" id="div1" style=height:500px;>');
	}else{
		document.write('<div class="formDefault" id="div1" style=height:740px;>');
	}
}else{
	document.write('<div class="formDefault" id="div1">');
}
</SCRIPT>
<%
   }else{%>
	<div>
<%}%>	
		
<%
HttpSession tSession = request.getSession(true); 
 if (sessionmaint.isValidSession(tSession))
 {
   	tSession.setAttribute("studyIdForFormQueries","");	
   	//String formId123="";  	
	String accFormId="";  
   	int pageRight = 0,formRights=0;	
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
	if(!commonformflag.equalsIgnoreCase("")){
		
		if(commonformflag.equalsIgnoreCase("ORG")|| commonformflag.equalsIgnoreCase("NTW")){
			pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MSITES"));
	}else{
		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MUSERS"));
	}
	}else{
			pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("ACTFRMSACC"));
			
	}
	formRights=Integer.parseInt(grpRights.getFtrRightsByValue("MACCFRMS"));
	
	session.setAttribute("formQueryRight",""+pageRight);

	//Bug#6638
	String accessStatus = "";
		if(isAccessibleFor(pageRight, 'E')){
			accessStatus="E";
		}else
		if(isAccessibleFor(pageRight, 'N') || isAccessibleFor(pageRight, 'V')){
			accessStatus="V";
		}
	
		
   	int filledFormUserAccessInt = lnkformB.getFilledFormUserAccess(
    		StringUtil.stringToNum(request.getParameter("filledFormId")),
    		StringUtil.stringToNum(request.getParameter("formId")),
    		StringUtil.stringToNum((String)tSession.getAttribute("userId")) );
   	
   	if (StringUtil.stringToNum(request.getParameter("formId")) > 0) {
   		FormLibDao formLibDao = new FormLibDao();
   		int fkAccountOfFormLib = 
   				formLibDao.getFkAccountOfFormLib(StringUtil.stringToNum(request.getParameter("formId")));
   		if (fkAccountOfFormLib == 0 || 
   				fkAccountOfFormLib != StringUtil.stringToNum((String)tSession.getAttribute("accountId"))) {
   			pageRight = 0;
   		}
   	}
   	if (StringUtil.stringToNum(request.getParameter("filledFormId")) > 0) {
   	   	FormResponseDao formResponseDao = new FormResponseDao();
   	   	int fkAccountOfFormResponse = 
   	   			formResponseDao.getFkAccountOfFilledAcctForm(StringUtil.stringToNum(request.getParameter("filledFormId")));
   		if (fkAccountOfFormResponse == 0 || 
   	   			fkAccountOfFormResponse != StringUtil.stringToNum((String)tSession.getAttribute("accountId"))) {
   	   		pageRight = 0;
   		}
   	}
   	
   	// -- Start of Bug 14656 fix
   	// Make sure that formId is a valid Account Form ID
	accFormId = request.getParameter("formId");
	int accFormIdInt = -1;
	if (!StringUtil.isEmpty(accFormId)) {
		UserJB userB1 = (UserJB) tSession.getAttribute("currentUser");
		LinkedFormsDao lnkFrmDao1 = new LinkedFormsDao();
		if(commonformflag.equalsIgnoreCase("")){
		 lnkFrmDao1 = lnkformB.getAccountForms(
				StringUtil.stringToNum((String)tSession.getAttribute("accountId")), 
				StringUtil.stringToNum((String)tSession.getAttribute("userId")),
				StringUtil.stringToNum(userB1.getUserSiteId()));
		}else{
			if(commonformflag.equalsIgnoreCase("ORG")){
			 lnkFrmDao1 = lnkformB.getOrganisationForms(
					StringUtil.stringToNum((String)tSession.getAttribute("accountId")), 
					StringUtil.stringToNum((String)tSession.getAttribute("userId")),
					StringUtil.stringToNum(userB1.getUserSiteId()));
			}
			else if(commonformflag.equalsIgnoreCase("NTW")){
				 lnkFrmDao1 = lnkformB.getNetworkForms(
						StringUtil.stringToNum((String)tSession.getAttribute("accountId")), 
						StringUtil.stringToNum((String)tSession.getAttribute("userId")),
						StringUtil.stringToNum(userB1.getUserSiteId()));
				}
			else{
				lnkFrmDao1 = lnkformB.getUserForms(
						StringUtil.stringToNum((String)tSession.getAttribute("accountId")), 
						StringUtil.stringToNum((String)tSession.getAttribute("userId")),
						StringUtil.stringToNum(userB1.getUserSiteId()));
			}}
		boolean isValidAccountFormId = false;
		try { accFormIdInt = Integer.parseInt(accFormId); } catch(Exception e) {}
		for (Object myFormId : lnkFrmDao1.getFormId()) {
			int myFormIdInt = ((Integer) myFormId).intValue();
			if (accFormIdInt == myFormIdInt) {
				isValidAccountFormId = true;
				break;
			}
		}
		if (!isValidAccountFormId) {
			pageRight = 0; // Invalid formId in request parameter => reject
		}
	}
	// -- End of Bug 14656 fix
	
	// -- Start of Bug 14659 fix
	// Make sure the formId and formLibVer combination is valid
	String formLibVer = request.getParameter("formLibVer");
	if (!StringUtil.isEmpty(formLibVer)) {
		FormLibDao formLibDao1 = new FormLibDao();
		int fkFormLib = formLibDao1.getFkFormLibForPkFormLibVer(StringUtil.stringToNum(formLibVer));
		if (accFormIdInt > 0 && accFormIdInt != fkFormLib) {
			pageRight = 0; // Invalid formId and formLibVer combination in request parameters => reject
		}
	}
	// -- End of Bug 14659 fix
	
	
	if   (  pageRight  >=4 && filledFormUserAccessInt > 0 ) 
	{
  		 
	   	 String mode = request.getParameter("mode");
   		 String filledFormId = request.getParameter("filledFormId");
		 String entryChar = request.getParameter("entryChar");
		 String formFillDt = request.getParameter("formFillDt");
		 String formPullDown = request.getParameter("formPullDown");
		 String 	 patientCode=request.getParameter("patientCode");		

		 String formLibVerNumber="";  
		 String findStr = "";
		 String findStr1 = "";
		 StringBuffer sbuffer = null; 
		 int len = 0;
		 int pos = 0;

         String formHtml = "";
		
		 //JM:	
		 //formLibB.setFormLibId(EJBUtil.stringToNum(formId123));
		 formLibB.setFormLibId(EJBUtil.stringToNum(accFormId));
			
		 formLibB.getFormLibDetails();
		 String formName=formLibB.getFormLibName();
	     String formStatus = formLibB.getFormLibStatus();
	     
	      eSignRequired =formLibB.getEsignRequired();
	   
	   if (StringUtil.isEmpty(eSignRequired))
	   {
	   		eSignRequired = "0";
	   }
	   
		//Added by Manimaran for November Enhancement F6
		//JM: 122206: blocked
	   	//formLibVerNumber = formLibB.getFormLibVersionNumber(EJBUtil.stringToNum(formLibVer));
		//added by IA 11.05.2006
		FormLibDao fd = new FormLibDao();
		if ((formLibVer==null)|| formLibVer.equals("")){
			
			formLibVer = fd.getLatestFormLibVersionPK(EJBUtil.stringToNum(accFormId));
		
		}
		//end added
//		getLatestFormLibVersionPK
		formLibVerNumber = formLibB.getFormLibVersionNumber(EJBUtil.stringToNum(formLibVer));
		
		if (StringUtil.isEmpty(formLibVerNumber))
		{
			formLibVerNumber = "";
		}
		 
		 CodeDao cdao = new CodeDao();
		 cdao.getCodeValuesById(EJBUtil.stringToNum(formStatus));
		 ArrayList arrSybType = cdao.getCSubType();
		 String statSubType = arrSybType.get(0).toString();		
		 
//***********************************************************************************cscscs***********************************

ArrayList arrFrmNames = null;
		 ArrayList arrNumEntries = null;
 		 ArrayList arrFrmIds = null;
		 ArrayList arrEntryChar = null;
		 ArrayList arrFrmInfo = new ArrayList();

		 //String entryChar="";
		 String numEntries = "";
		 String frmInfo= "";
		 int formId = 0;
		 String firstFormInfo = "";
		 SaveFormDao saveDao=new SaveFormDao();
		 String filledFromStatValue ="";
		 
		 
  	     String strFormId = request.getParameter("formPullDown");	 
  	     
		 
	 	 String userId = (String) tSession.getValue("userId");
	     UserJB userB = (UserJB) tSession.getValue("currentUser");
   	     String siteId = userB.getUserSiteId();
		 
		 String accId=(String)tSession.getAttribute("accountId");
		 int iaccId=EJBUtil.stringToNum(accId);
		 LinkedFormsDao lnkFrmDao = new LinkedFormsDao();


   if(commonformflag.equalsIgnoreCase("")){

		 lnkFrmDao = lnkformB.getAccountForms(iaccId, EJBUtil.stringToNum(userId), EJBUtil.stringToNum(siteId));
   }else{
	   if(commonformflag.equalsIgnoreCase("ORG")){
	   lnkFrmDao = lnkformB.getOrganisationForms(iaccId, EJBUtil.stringToNum(userId), EJBUtil.stringToNum(siteId));
	   }
	   else if(commonformflag.equalsIgnoreCase("NTW")){
		   lnkFrmDao = lnkformB.getNetworkForms(iaccId, EJBUtil.stringToNum(userId), EJBUtil.stringToNum(siteId));
		   }
	   else{
		   lnkFrmDao = lnkformB.getUserForms(iaccId, EJBUtil.stringToNum(userId), EJBUtil.stringToNum(siteId));   
		   
	   }} 
		 arrFrmIds = lnkFrmDao.getFormId();		 
		 arrFrmNames = lnkFrmDao.getFormName();
		
		 arrEntryChar = lnkFrmDao.getFormEntryChar();
		 arrNumEntries = lnkFrmDao.getFormSavedEntriesNum();
	
		 if (arrFrmIds.size() > 0) { 	
    		 if (strFormId==null) {
    		 	formId = EJBUtil.stringToNum(arrFrmIds.get(0).toString());
    			entryChar = arrEntryChar.get(0).toString();
    			numEntries = arrNumEntries.get(0).toString();		   
    			firstFormInfo = arrFrmIds.get(0).toString() + "*"+ entryChar + "*" + arrNumEntries.get(0).toString();
    		 }
    		 else {
    	 		 StringTokenizer strTokenizer = new StringTokenizer(strFormId,"*");
    	 		 
    	 		 if (calledFromForm.equals(""))
    	 		 {
    	 	     	formId = EJBUtil.stringToNum(strTokenizer.nextToken());
    			 	entryChar = strTokenizer.nextToken();
    		     	numEntries = strTokenizer.nextToken();
    			 	firstFormInfo = strFormId;	   
    			 }	
    			 else
    			 {
    			 	formId = EJBUtil.stringToNum(strTokenizer.nextToken());
    			 	entryChar = strTokenizer.nextToken();
    			 	
    			 	lnkformB.findByFormId(formId );
		   		 	numEntries = lnkformB.getLfDataCnt();
    			 	//get the number of times the form was answered
    			 	 
    			 	//prepare strFormId again
    			 	
    			 	strFormId = formId + "*" + entryChar + "*" + numEntries;
    			 	firstFormInfo = strFormId;	   
    			 
    			 }
    		 }
    	
    		 for (int i=0;i<arrFrmIds.size();i++)
    		 {  //store the formId, entryChar and num Entries separated with a *
    		  	 frmInfo = arrFrmIds.get(i).toString() + "*"+ arrEntryChar.get(i).toString() + "*" + arrNumEntries.get(i).toString();			 
    		 	 arrFrmInfo.add(frmInfo);		 
    		 }
    		 		
    				 
    	 	 String dformPullDown = EJBUtil.createPullDownWithStrNoSelect("formPullDown",firstFormInfo, arrFrmInfo , arrFrmNames);
    		 
			 StringBuffer formBuffer = new StringBuffer(dformPullDown);

			 //submit the form when the user selects the form name
			/* if (calledFromForm.equals(""))	 
			 {
			 	formBuffer.replace(0,7,"<SELECT onChange=\"document.myacctform.submit();\"");
			 }*/
			 //else
			 //{
			 	//formBuffer.replace(0,7,"<SELECT DISABLED onChange=\"document.accform.submit();\"");
			 //}	
			 
			 dformPullDown = formBuffer.toString();

			 //out.println(dformPullDown);

	String 	 studyId=request.getParameter("studyId");
	String 	 statDesc=request.getParameter("statDesc");
	String 	 statid=request.getParameter("statid");
	String 	 patProtId=request.getParameter("patProtId");
	//String 	 mode=request.getParameter("mode");
	String 	 pkey=request.getParameter("pkey");
	

//added for Mahi-2 enhancements dated on 1April05
		%>

		 <Form method="post" name="myacctform" action="formfilledaccountbrowser.jsp" onsubmit="">
		
		<input type="hidden" name="srcmenu" value=<%=src%>>
		<input type="hidden" name="selectedTab" value="11">
		<input type="hidden" name="studyId" value=<%=studyId%>>
		<input type="hidden" name="statDesc" value=<%=statDesc%>>
		<input type="hidden" name="statid" value=<%=statid%>>
		<input type="hidden" name="patProtId" value=<%=patProtId%>>
		<input type="hidden" name="patientCode" value=<%=patientCode%>>
		<input type="hidden" name="mode" value=<%=mode%>>														
		<input type="hidden" name="pkey" value=<%=pkey%>>		
		<input type="hidden" id="formRights" value=<%=formRights%>>
		<input type="hidden" name="commonformflag" value=<%=commonformflag%>>
		<input type="hidden" name="modpk" value=<%=modpk%>>
		<input type="hidden" name ="UserPrimOrg" value =<%=UserPrimOrg%>>  
    		
		
     		 	
	<table width="100%" align="center">

		<% if (calledFromForm.equals(""))	 { %>
		
	<tr  class="searchbg" height="38">
		
		<td width="90%" align="left"><%Object[] arguments = {dformPullDown};%><%=VelosResourceBundle.getLabelString("L_Jump_To_Form",arguments)%><%--Jump to Form : <%=dformPullDown%>*****--%>&nbsp;
		<button type="submit" onclick="javascript:fn_nextpage(document.myacctform.formPullDown.value) ;"><%=LC.L_Go%></button>
		</td>
		
	</tr>
	</table>
		<% } %>
		
		<script>
				function fn_nextpage(val)
		{
				 //window.location.href="acctformdetails.jsp?srcmenu=<%=src%>&formId=<%=formId%>&mode=N&formDispLocation=A&entryChar=<%=entryChar%>&formFillDt=<%=formFillDt%>&formPullDown=<%=firstFormInfo%>&calledFromForm=<%=calledFromForm%>"  ;
				 /*(selForm=document.myacctform.formPullDown.value;
				 arrayOfStr=selForm.split("*");
				 if (arrayOfStr.length>0)
				 selForm=arrayOfStr[0];*/
				 //document.myacctform.action="acctformdetails.jsp?srcmenu=<%=src%>&formId="+selForm+"&mode=N&formDispLocation=A&entryChar=<%=entryChar%>&formFillDt=<%=formFillDt%>&formPullDown=<%=firstFormInfo%>&calledFromForm=<%=calledFromForm%>"  ;	
				 document.myacctform.submit();
				 //document.myacctform.action="formfilledaccountbrowser.jsp";
		}
		</script>




<% } 
        if (mode.equals("N"))
         {
    	   //JM: 
	   //formHtml =  lnkformsB.getFormHtmlforPreview(EJBUtil.stringToNum(formId123));
       //formHtml =  lnkformsB.getFormHtmlforPreview(EJBUtil.stringToNum(accFormId));
  	   formHtml =  lnkformsB.getFormHtmlforPreviewNoRole(EJBUtil.stringToNum(accFormId));

         }
        else
        {
    	     
    	   saveDao =  lnkformsB.getFilledFormHtmlNoRole(EJBUtil.stringToNum(filledFormId),"A","Y");
			formHtml =  saveDao.getHtmlStr();
        }
        CodeDao codeLstStatus = new CodeDao();
        filledFromStatValue = codeLstStatus.getCodeDescription(saveDao.getFilledFormStat());
%>
	
		<table width="100%" >
		<tr> 
			 <td width="40%"><!--KM-to fix the Bug 2809 -->
			 	 <B><%=LC.L_Open_FormName+": "%></B><%=formName%><%-- <b> Open Form Name: </b> Form Name: </b><%=formName%>*****--%>&nbsp;&nbsp;&nbsp;&nbsp;
			 </td>&nbsp;&nbsp;
			
			<td width="60%" align="right" class="rhsfont">
				<%if (mode.equals("M")){%>
		 		<A onclick="openQueries(<%=accFormId%>,<%=filledFormId%>)" href=#> <%--Add/Edit Queries*****--%><%=LC.L_AddOrEdit_Queries%></A>&nbsp;
				<%}%>
					
			<%if(!commonformflag.equalsIgnoreCase("")){%>
						<A onclick="openPrintWin1(<%=accFormId%>,<%=filledFormId%>,'<%=formLibVerNumber%>','<%=commonformflag %>')" href=#> <%--Printer Friendly Format*****--%><img border="0" title="<%=LC.L_Printer_FriendlyFormat%>" alt="<%=LC.L_Printer_FriendlyFormat%>" src="./images/printer.gif"/></A>&nbsp;	
    			<%}else{%>	
			<A onclick="openPrintWin(<%=accFormId%>,<%=filledFormId%>,'<%=formLibVerNumber%>')" href=#> <%--Printer Friendly Format*****--%><img border="0" title="<%=LC.L_Printer_FriendlyFormat%>" alt="<%=LC.L_Printer_FriendlyFormat%>" src="./images/printer.gif"/></A>&nbsp;
			<%} %>
			<%if (entryChar.equals("E") && (!StringUtil.isEmpty(filledFormId)) ){%>
			<%String formDispLocation="";
			   if(!commonformflag.equalsIgnoreCase("")){
			  		 formDispLocation=commonformflag;
			  	}else
			  	{
			  		formDispLocation="A";
			  	}%>
		 		<A href="#" onclick="openReport(document.myacctform,'100','Form Audit Trail','<%=filledFormId%>','formfilledaccountbrowser.jsp?srcmenu=tdMenuBarItem1&page=1','')" title="<%=LC.L_ViewAud_Trial%><%--View Audit Trail*****--%>"><%--Audit*****--%><%=LC.L_Audit%></A>&nbsp;
		 			
		 		<A href="#" onclick="openReport(document.myacctform,'100','Track Changes','<%=filledFormId%>','formfilledaccountbrowser.jsp?srcmenu=tdMenuBarItem1&page=1','<%=accessStatus%>')" title="<%=LC.L_Track_Changes%><%--Track Changes*****--%>"><%--Track Changes*****--%><%=LC.L_Track_Changes%></A>&nbsp;
		 		
					<A onclick="return confirmBox(<%=pageRight%>,<%=saveDao.isLocked()%>,'<%=filledFromStatValue%>');" href="formrespdelete.jsp?srcmenu=<%=src%>&calledFromForm=<%=calledFromForm%>&formId=<%=formId%>&formLibVer=<%=formLibVer%>&mode=M&formDispLocation=<%=formDispLocation%>&filledFormId=<%=filledFormId%>&calledFrom=S&patientCode=<%=patientCode%>&entryChar=<%=entryChar%>&formFillDt=<%=formFillDt%>&formPullDown=<%=formPullDown%>&outputTarget=<%=outputTarget%>&commonformflag=<%=commonformflag%>&modpk=<%=modpk%>&UserPrimOrg=<%=UserPrimOrg%>"><img src="./images/delete.gif" border="0" /></A>
			<%}%>
			 </td>
			<!--Added by Manimaran for the November Enhancement F6-->
			<!--tr>
			<td width="40%">
			 	 <b> Version Number: <%=formLibVerNumber%></b>
			</td>
			</tr--> 
		</tr>
		</table>	</form>	   		

        <%		
		//Replace Submit image with button	
  		findStr ="<input type=\"image\" src=\"../images/jpg/Submit.gif\" align=\"absmiddle\" border=\"0\" id=\"submit_btn\">";
  		findStr1="<input type=\"image\" src=\"../images/jpg/Submit.gif\" align=\"absmiddle\" border=\"0\" id=\"submit_btn\">";
    	  
  		if(formHtml.indexOf(findStr)>-1) {
  			len = findStr.length();
  		    pos = formHtml.lastIndexOf(findStr);
  		}else{
  			len = findStr1.length();
  		    pos = formHtml.lastIndexOf(findStr1);
  		}
  	    if (pos > 0)
  	    {
  		    sbuffer = new StringBuffer(formHtml);
			if(pageRight==4  || (pageRight == 6 && mode.equals("N")) || (pageRight < 6 && mode.equals("M")) || (entryChar.equals("O")&& mode.equals("M")) || (statSubType.equals("L")) || (saveDao.isLocked())/*  || saveDao.isSoftLock() && !saveDao.isAllowedUnlock()  */){
  		    	sbuffer.replace(pos,pos+len,"");
  		    }else{
  		    	sbuffer.replace(pos,pos+len,"<button id=\"submit_btn\" type=\"submit\">"+LC.L_Submit+"</button>");
  		    }
  		    formHtml = sbuffer.toString();
  	    }
  	    
  	    findStr = "name=\"eSign\"";
	    pos = formHtml.lastIndexOf(findStr);
	    if(pos > 0)
	    {
	    	sbuffer = new StringBuffer(formHtml);
	    	sbuffer.insert(pos-1," autocomplete=\"off\" ");
	    	formHtml = sbuffer.toString();  	    	
	    }
	    
	    findStr ="cannot be greater than today's date";
  	    len = findStr.length();
  	    pos = formHtml.lastIndexOf(findStr);

  	    if (pos > 0)
  	    {
  		    sbuffer = new StringBuffer(formHtml);
			sbuffer.replace(pos,pos+len,"cannot be a future date.");
  		    formHtml = sbuffer.toString();
  	    }

		//in case of view right, remove the submit button
		//in case of lockdown status, remove the submit button
		//in case of new/edit mode add a hidden element of mode.
		//according to this mode the pop-up messages of First time and Every time would be displayed
		if(pageRight==4  || (pageRight == 6 && mode.equals("N")) || (pageRight < 6 && mode.equals("M")) || (entryChar.equals("O")&& mode.equals("M")) || (statSubType.equals("L")) || (saveDao.isLocked()) /*|| saveDao.isSoftLock() && !saveDao.isAllowedUnlock() */ ){
		   
     		// Disable the Form tag for issue 4632
		    formHtml = formHtml.replaceAll("onSubmit=\"return validationWithAjax\\(\\)\"","")
	          .replaceAll("action=\"updateFormData.jsp\"","action=\"javascript:void(0)\"")
	          .replaceAll("formobj.action=\"querymodal.jsp\"","formobj.action=\"javascript:void(0)\"");

			//Added by Manimaran for the issue 2941
			sbuffer = new StringBuffer(formHtml);
			Object[] arguments1 = {formLibVerNumber};sbuffer.append("<table><tr><td>"+VelosResourceBundle.getLabelString("L_Frm_VerNmber",arguments1)+"</td></tr></table>");/*sbuffer.append("<table><tr><td>Form Version Number: "+formLibVerNumber+"</td></tr></table>");*****/
			formHtml = sbuffer.toString();


			   
		} else {
		   findStr = "</Form>";
		   len = findStr.length();
		   pos = formHtml.lastIndexOf(findStr);
		   sbuffer = new StringBuffer(formHtml);
		   StringBuffer paramBuffer = new StringBuffer();
	   //  by salil on 13 oct 2003 to see if the check box type multiple choice exists 
	  //  or not and to handle the case
	  // when no option is selected in a check box type multiple choice the element name does not 
	  	// be  carried forward to the next page hence the "chkexists"  variable is used to see 
		// if the page has a multiple choice field or not .     
	
		   String chkexists ="";
		   String tochk="type=\"checkbox\"";
		   if(formHtml.indexOf(tochk) == -1 )
		   {
		     chkexists ="N";
		   }
		   else
		   {
		   	 chkexists ="Y";
		   }
		   
		   if(commonformflag.equalsIgnoreCase("")){
		   paramBuffer.append("<input type=\"hidden\" name=\"formDispLocation\" value='A'/>");
		   }
		   else{
			   paramBuffer.append("<input type=\"hidden\" name=\"formDispLocation\" value='"+commonformflag+"'/>");
		   }
		   paramBuffer.append("<input type=\"hidden\" name=\"formFillMode\" value='"+mode+"'/>");
		   paramBuffer.append("<input type=\"hidden\" id=\"formId\" name=\"formId\" value='"+formId+"'/>");
		   paramBuffer.append("<input type=\"hidden\" name=\"srcmenu\" value='"+src+"'/>");
		   paramBuffer.append("<input type=\"hidden\" name=\"formFillDt\" value='"+formFillDt+"'/>");
		   paramBuffer.append("<input type=\"hidden\" name=\"formPullDown\" value='"+formPullDown+"'/>");
		   paramBuffer.append("<input type=\"hidden\" name=\"calledFromForm\" value='"+calledFromForm+"'/>");
		   paramBuffer.append("<input type=\"hidden\" name=\"commonformflag\" value='"+commonformflag+"'/>");
		   paramBuffer.append("<input type=\"hidden\" name=\"modpk\" value='"+modpk+"'/>");
		   paramBuffer.append("<input type=\"hidden\" name=\"UserPrimOrg\" value='"+UserPrimOrg+"'/>");
           paramBuffer.append("<input type=\"hidden\" name=\"fldMode\" value='"+fldMode+"'/>");
           Object[] arguments2 = {formLibVerNumber};paramBuffer.append("<br><table><tr><td>"+VelosResourceBundle.getLabelString("L_Frm_VerNmber",arguments2)+"</td></tr></table>");/*paramBuffer.append("<br><table><tr><td>Form Version Number: "+formLibVerNumber+"</td></tr></table>");*****/
		
		   
			if (!mode.equals("N"))
		    {
		    	paramBuffer.append("<input type=\"hidden\" name=\"formFilledFormId\" value='"+filledFormId+"'/>");
		    	paramBuffer.append("<input type=\"hidden\" name=\"formLibVer\" value='"+formLibVer+"'/>");
		    	
			}		
			
			paramBuffer.append("<input type=\"hidden\" name=\"outputTarget\" value='"+outputTarget+"'/>");
			paramBuffer.append("<input type=\"hidden\" name=\"specimenPk\" value='"+specimenPk+"'/>");
			paramBuffer.append("<input type=\"hidden\" name=\"eSignRequired\" value='"+eSignRequired+"'/>");
			
			
			//also append the paramBuffer    
		   sbuffer.replace(pos,pos+len,"<input type=\"hidden\" name=\"mode\" value='"+mode+"'/> <input type=\"hidden\" name=\"chkexists\" value='"+chkexists+"'/>"+paramBuffer.toString()+"   </Form>");
		   
		   formHtml = sbuffer.toString();
		}
 
 //dont need session variables, appended the required param as hidden fields to the form
 			   
       // set session attributes
	 	/*tSession.setAttribute("formDispLocation" ,"A");
		tSession.setAttribute("formFillMode" , mode);
		tSession.setAttribute("formId" , formId);
		tSession.setAttribute("srcmenu" , src);	
		tSession.setAttribute("formFillDt" , formFillDt);
		tSession.setAttribute("formPullDown" , formPullDown);				
		
		if (mode.equals("N"))
	    {
		}
		else
		{
			tSession.setAttribute("formFilledFormId" , filledFormId);
			tSession.setAttribute("formLibVer" , formLibVer);			
		}*/	
		
 // end of session attributes		
		
 	
%>

<%=formHtml%>
<%

	}// end of if for page right 
	else
	{

	%>
	  <jsp:include page="accessdenied.jsp" flush="true"/>
	  <%

	} //end of else body for page right	
		
  } //end of if for session
 
 else 
 {  //else of if body for session
  %>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
 %>
<br>
<div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</div>
<%if (calledFromForm.equals(""))   
   {
%>	

 

 
<DIV class="mainMenu" id = "emenu"> 
  <jsp:include page="getmenu.jsp" flush="true"/>
</DIV>
<%
	}
%>
<script>
elementOverride=(document.getElementById("override_count"));
if (!(typeof(elementOverride)=="undefined") && (elementOverride!=null) ) {
if ((document.getElementById("override_count").value)>0) setDisableSubmit('false');
}
linkFormSubmit('fillform');</script>

<%

 if ( eSignRequired.equals("0")   )
{
	
  %>
  	<script>	
	  	elemeSign = (document.getElementById("eSign"));	
		elemeSignLabel = (document.getElementById("eSignLabel"));
		
		if (!(typeof(elemeSign)=="undefined") && (elemeSign!=null) ) {
			elemeSign.style.visibility = "hidden";
			elemeSign.value="11";
		}
		
		if (!(typeof(elemeSignLabel)=="undefined") && (elemeSignLabel!=null) ) {
			
			elemeSignLabel.style.visibility = "hidden";
		}
	</script>	
  <%	

}	

%>
 
</body>
</html>
<%!
private boolean isAccessibleFor(int pageRight, char type) {
    switch(type) {
    case 'V': return ((pageRight & 0x0004) >> 2) == 1;
    case 'E': return ((pageRight & 0x0002) >> 1) == 1;
    case 'N': return  (pageRight & 0x0001) == 1;
    }
    return false;
}
%>


