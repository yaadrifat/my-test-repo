<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<html>

<head>

<title><%=LC.L_Pat_PrevSch%><%--<%=LC.Pat_Patient%> Previous Schedule*****--%></title>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></head>

<jsp:include page="skinChoser.jsp" flush="true"/>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>

<%@ page import="com.velos.eres.service.util.LC"%><%@ page import="com.velos.eres.service.util.MC"%>

<%@ page language = "java" import = "com.velos.eres.business.section.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.DateUtil,com.velos.esch.business.common.*"%>

<jsp:useBean id="grpRights" scope="request" class="com.velos.eres.web.grpRights.GrpRightsJB"/>

<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>

<jsp:useBean id="ctrl1" scope="request" class="com.velos.eres.business.common.CtrlDao"/>



<%

int ienet = 2;



String agent1 = request.getHeader("USER-AGENT");

   if (agent1 != null && agent1.indexOf("MSIE") != -1)

     ienet = 0; //IE

    else

	ienet = 1;

	if(ienet == 0) {

%>

<body style="overflow:scroll;">

<%

	} else {

%>

<!-- Code for bug id:- #22134 -->
<body style="overflow:auto">

<%

	}

%>



<br>





  <%

   HttpSession tSession = request.getSession(true);

   if (sessionmaint.isValidSession(tSession))

   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
	String study = (String) tSession.getValue("studyId"); // "3334";

	String patientId=request.getParameter("patientId"); // "194";



	String event_id="";

	String patprotId="";

	String eveName="";

	String calName="";

	String disDate="";

	String disReason="";

	String eveStartDate="";

	String studyNum="";

	String eveStatus="";

	String fuzzyPeriod="";

	String prevPatProtId="";

	String actualDate="";



	String protDate="";



	Integer visit=null;
	String visitName = "";
	Integer fk_Visit=null;

	String prevMonth="";

	int pageRight= 7;

	String thisMonth="";

	String start_dt="";

	String prev_dt="00/00/0000";









	String schDate = "";

	String fuzzy_period="";	 /// to be removed

	String fuzzy_dates = ""; // to be removed

	   ArrayList monthName = new	ArrayList();

	ArrayList ctrlValue = new ArrayList();

	   monthName.add(LC.L_January/*"January"*****/);

	   monthName.add(LC.L_February/*"February"*****/);

	   monthName.add(LC.L_March/*"March"*****/);

	   monthName.add(LC.L_April/*"April"*****/);

	   monthName.add(LC.L_May/*"May"*****/);

	   monthName.add(LC.L_June/*"June"*****/);

	   monthName.add(LC.L_July/*"July"*****/);

	   monthName.add(LC.L_August/*"August"*****/);

	   monthName.add(LC.L_September/*"September"*****/);

	   monthName.add(LC.L_October/*"October"*****/);

	   monthName.add(LC.L_November/*"November"*****/);

	   monthName.add(LC.L_December/*"December"*****/);





	Integer rt = null;


	rt = (Integer)tSession.getValue("studyManagePat");

    pageRight = rt.intValue();

%>

  <form METHOD="POST">

  <table width="100%" cellspacing="0" cellpadding="0">

	<tr>

      <td align=center>

	<button onClick = "javascript:self.close();"><%=LC.L_Close%></button>

      </td>

      </tr>

  </table>

  </form>

<br>

<%

if (pageRight >= 4) {

     com.velos.esch.business.common.EventdefDao eventdao = new com.velos.esch.business.common.EventdefDao();

     eventdao = eventdefB.FetchPreviousSchedule(EJBUtil.stringToNum(study) ,EJBUtil.stringToNum(patientId)) ;



	 java.util.ArrayList event_ids=eventdao.getEvent_ids() ;

	 java.util.ArrayList patprotIds=eventdao.getPatprotId() ;

	 java.util.ArrayList eveNames= eventdao.getNames();

	 java.util.ArrayList calNames=eventdao.getCalName() ;

	 java.util.ArrayList disDates=eventdao.getDisDate() ;

	 java.util.ArrayList disReasons=eventdao.getDisReason() ;

	 java.util.ArrayList start_date_times = eventdao.getStart_date_times();

	 java.util.ArrayList actualDates=eventdao.getActualDate();

	 java.util.ArrayList studyNums = eventdao.getStudyNum();

	 java.util.ArrayList protstartDates = eventdao.getProtstartDate();



	 java.util.ArrayList eveStats= eventdao.getStatus();

	 java.util.ArrayList fuzzy_periods = eventdao.getFuzzy_periods();

	 java.util.ArrayList visits= eventdao.getVisit();

	 ArrayList arVisitNames = eventdao.getVisitName();
	 ArrayList arFKVisits = eventdao.getFk_visit();

///16aug2010 BK FIXED DEFECT 4512
	 ArrayList eventDurationAfterList = eventdao.getEventDurationAfter();
	 ArrayList eventDurationBeforeList = eventdao.getEventDurationBefore();
	 ArrayList eventFuzzyAfterList = eventdao.getEventFuzzyAfter();




	String modRight = (String) tSession.getValue("modRight");



	int modlen = modRight.length();



	ctrl.getControlValues("module");

	int ctrlrows = ctrl.getCRows();



	ArrayList feature =  ctrl.getCValue();

	ArrayList ftrDesc = ctrl.getCDesc();

	ArrayList ftrSeq = ctrl.getCSeq();

	ArrayList ftrRight = new ArrayList();

	String strR;

	for (int counter = 0; counter <= (modlen - 1);counter ++)

	{

		strR = String.valueOf(modRight.charAt(counter));

		ftrRight.add(strR);

	}



	grpRights.setGrSeq(ftrSeq);

	grpRights.setFtrRights(ftrRight);

	grpRights.setGrValue(feature);

	grpRights.setGrDesc(ftrDesc);



	int eptRight = 0;





	eptRight  = Integer.parseInt(grpRights.getFtrRightsByValue("MODEPT"));



//out.println("eptRight" +eptRight);











if(event_ids.size()==0){ %>



	<p class = "successfulmsg" align = center> <%=MC.M_NoDataFnd_ThisStd%><%--No Data was found for this <%=LC.Std_Study%>*****--%></p>

<%

	 }

	String st_exon = null;



/* out.println(event_ids.size());

out.println(patprotIds.size());

out.println(eveNames.size());

out.println(calNames.size());

out.println(disDates.size());

out.println(start_date_times.size());

out.println(studyNums.size());

out.println(eveStats.size());

out.println("" +fuzzy_periods.size()); */



	int j =0;

   Integer prevVisit = null ;

	int prevYr = 0;



	 for(int i=0; i<event_ids.size() ; i++) {



		Calendar cal = Calendar.getInstance();

		java.sql.Date st_date = (java.sql.Date)start_date_times.get(i);

		cal.setTime(st_date);

		int month = cal.get(cal.MONTH);

		int year = cal.get(cal.YEAR);

		thisMonth =(String) monthName.get(month);





		int yr = st_date.getYear();

		//yr = yr-1900;

		st_date.setYear(yr);

		java.sql.Date temp_date = (java.sql.Date)st_date.clone();



		eveName=(String)eveNames.get(i);

		Integer ev_id=(Integer) event_ids.get(i);



		event_id = ev_id.toString();

		eveStatus=(String)eveStats.get(i);

		patprotId=(String)patprotIds.get(i);



		studyNum=(String)studyNums.get(i);

		visit=(Integer)  visits.get(i);

		visitName = (String) arVisitNames.get(i);
		fk_Visit = (Integer) arFKVisits.get(i);


		java.sql.Date discontDate= (java.sql.Date)disDates.get(i);

		if(discontDate == null){

			disDate = "";

		} else {

			//disDate=(String)discontDate.toString();

	 		//disDate= disDate.toString().substring(5,7) + "/" + disDate.toString().substring(8,10) + "/" + disDate.toString		().substring(0,4);
			disDate = DateUtil.dateToString(discontDate);

		}





		java.sql.Date actDate= (java.sql.Date)actualDates.get(i);

		//actualDate=(String)actDate.toString();

 		//actualDate= actualDate.toString().substring(5,7) + "/" + actualDate.toString().substring(8,10) + "/" + 			actualDate.toString().substring(0,4);
		actualDate = DateUtil.dateToString(actDate);

		java.sql.Date protstartDate= (java.sql.Date)protstartDates.get(i);

		//protDate=(String)protstartDate.toString();

 		//protDate= protDate.toString().substring(5,7) + "/" + protDate.toString().substring(8,10) + "/" + 				protDate.toString().substring(0,4);
		protDate = DateUtil.dateToString(protstartDate);




		disReason=(String)disReasons.get(i);



		calName=(String)calNames.get(i);

		start_dt = st_date.toString();








		//out.print("Start Date" +start_dt +"Prev Date" +prev_dt);

		String str="";



		fuzzy_period = (String)fuzzy_periods.get(i);

		fuzzy_dates = "-";


		 if (StringUtil.isEmpty(fuzzy_period)) fuzzy_period="";

		//Added by Gopu to fix the Bugzilla issue #2347
		 String fuzzy_period_before_unit = (String)eventDurationBeforeList.get(i);
		 if (StringUtil.isEmpty(fuzzy_period_before_unit)) fuzzy_period_before_unit ="";

		 String fuzzy_period_after = (String)eventFuzzyAfterList.get(i);
		 if (StringUtil.isEmpty(fuzzy_period_after)) fuzzy_period_after="";

		 String fuzzy_period_after_unit = (String)eventDurationAfterList.get(i);
		 if (StringUtil.isEmpty(fuzzy_period_after_unit)) fuzzy_period_after_unit ="";

		String fuzzyPeriodB = fuzzy_period;
		String fuzzyPeriodA = fuzzy_period_after;

		String fuzzy_result_before = "0";
		String fuzzy_result_after = "0";

		String fuzzy_month ="0";


	///16aug2010 BK FIXED DEFECT 4512
		if(fuzzy_period_before_unit!= null && fuzzy_period_before_unit.equals("H")){
			fuzzy_result_before = "-1";
		}

		if(fuzzy_period_before_unit != null && fuzzy_period_before_unit.equals("D")){
			fuzzy_result_before = "-"+fuzzyPeriodB.trim();
			//System.out.println("fuzzy_result_before" +fuzzy_result_before + "fuzzyPeriodB.trim()" +fuzzyPeriodB.trim());
		}

		if(fuzzy_period_before_unit != null && fuzzy_period_before_unit.equals("W")){
			fuzzy_result_before = "-" + ((EJBUtil.stringToNum(fuzzyPeriodB.trim())  * 7));
		}



		if (fuzzy_period_after_unit!= null && fuzzy_period_after_unit.equals("H")){
			fuzzy_result_after ="1";
		}
		if (fuzzy_period_after_unit !=null && fuzzy_period_after_unit.equals("W")){
			fuzzy_result_after = "" +((EJBUtil.stringToNum(fuzzyPeriodA.trim())  * 7));
		}
		if (fuzzy_period_after_unit!= null && fuzzy_period_after_unit.equals("D")){
			fuzzy_result_after = fuzzyPeriodA.trim();
		}

  if(fuzzy_period_before_unit.equals("M") && !fuzzyPeriodB.equals("0")){
		fuzzy_month = "1";
  }


fuzzy_dates = "-";
String fuzzyResultBefore = "";
java.sql.Date temp_date1 = (java.sql.Date)st_date.clone();
java.sql.Date temp_date2 = (java.sql.Date)st_date.clone();
int day = 0;
String fuzzyPeriodBefore = "";
///16aug2010 BK FIXED DEFECT 4512


if ((fuzzy_period.trim().equals("0")) && (fuzzy_period_after.trim().equals("0"))){
		fuzzy_dates =  "-";
	 }
else if ((!(fuzzy_period.trim().equals("0"))) || ((!fuzzy_result_after.trim().equals("0"))) || ((!fuzzy_result_before.trim().equals("0")))|| fuzzy_month.equals("1"))
{

	fuzzy_period = fuzzy_period.trim();
	Integer disp;
	java.util.Calendar Cal = java.util.Calendar.getInstance();

	if((fuzzy_period.substring(0,1)).equals("+"))
	{

		fuzzy_dates = DateUtil.dateToString(st_date);

		fuzzy_dates += " -- ";
		fuzzyPeriod = fuzzy_period.substring(1,fuzzy_period.length());
		disp = new Integer(fuzzyPeriod);
		day = disp.intValue();


		Cal.setTime(temp_date);

	if(fuzzy_period_after_unit != null){
		if (fuzzy_period_after_unit.equals("M")){
			Cal.add(java.util.Calendar.MONTH,EJBUtil.stringToNum(fuzzyPeriodA.trim()));
		}else{
			Cal.add(java.util.Calendar.DATE, day);
		}




		temp_date1 = new java.sql.Date(Cal.get(java.util.Calendar.YEAR) -  1900,Cal.get(java.util.Calendar.MONTH),Cal.get(java.util.Calendar.DATE));



		fuzzy_dates += DateUtil.dateToString(temp_date1);
	} else {
		fuzzy_dates += "-";
	}
}
else if((fuzzy_period.substring(0,1)).equals("-"))
{

	fuzzyPeriod = fuzzy_period.substring(1,fuzzy_period.length());
	fuzzyPeriodBefore = "-" + fuzzyPeriod;
	disp = new Integer(fuzzyPeriodBefore);
	day = disp.intValue();

	Cal.setTime(temp_date);

if(fuzzy_period_before_unit !=null){

	if (fuzzy_period_before_unit.equals("M")){
			Cal.add(java.util.Calendar.MONTH, (new Integer("-"+fuzzyPeriodB.trim()).intValue()) );
	}else{
		Cal.add(java.util.Calendar.DATE, day);
	}


	temp_date1 = new java.sql.Date(Cal.get(java.util.Calendar.YEAR) -  1900,Cal.get(java.util.Calendar.MONTH),Cal.get(java.util.Calendar.DATE));



	fuzzy_dates = DateUtil.dateToString(temp_date1);

	fuzzy_dates += " -- ";


	fuzzy_dates += DateUtil.dateToString(st_date);
}else{
fuzzy_dates +="-";
}
} else
{

	fuzzyPeriod = fuzzy_period.substring(0,fuzzy_period.length());
	fuzzyResultBefore = fuzzy_result_before ;

	disp = new Integer(fuzzyResultBefore);//

	day = disp.intValue();

	Cal.setTime(temp_date);
	if (fuzzy_period_before_unit!=null && fuzzy_period_before_unit.equals("M")){



	 day=EJBUtil.stringToNum(fuzzyPeriodB.trim())*(-30);
	 Cal.add(java.util.Calendar.DATE, day);
	}else{
		Cal.add(java.util.Calendar.DATE, day);
	}
	temp_date1 = new java.sql.Date(Cal.get(java.util.Calendar.YEAR) -  1900,Cal.get(java.util.Calendar.MONTH),Cal.get(java.util.Calendar.DATE));

	fuzzy_dates = DateUtil.dateToString(temp_date1);

	fuzzy_dates += "--";


	disp = new Integer(fuzzy_result_after);

	day = disp.intValue();
	Cal.setTime(temp_date);
	if (fuzzy_period_after_unit!=null &&  fuzzy_period_after_unit.equals("M")){


	 day=EJBUtil.stringToNum(fuzzyPeriodA.trim())*(30);
	 Cal.add(java.util.Calendar.DATE, day);

	}else{
			Cal.add(java.util.Calendar.DATE, day);
	}

	temp_date2 = new java.sql.Date(Cal.get(java.util.Calendar.YEAR) -  1900,Cal.get(java.util.Calendar.MONTH),Cal.get(java.util.Calendar.DATE));

	fuzzy_dates += DateUtil.dateToString(temp_date2);

}

	}

		 //schDate= st_date.toString().substring(5,7) + "/" + st_date.toString().substring(8,10) + "/" + st_date.toString().substring(0,4);
		 schDate = DateUtil.dateToString(st_date);


		if (!(patprotId.equals(prevPatProtId))){

		    if (!(prevPatProtId.trim().equals("")))

		{
			prevVisit = null;
		%>

			</table>

		<%

		}

		%>





		<BR>

		<table border=0 cellPadding=0 cellSpacing=0 width="100%" bgcolor="Silver">

			<TR>

				<TD class=tdDefault width="50%"><B><%=LC.L_Study_No%><%--<%=LC.Std_Study%> No*****--%>:&nbsp&nbsp</B><%=studyNum%></TD>

				<TD class=tdDefault width="50%"><B><%=LC.L_Discont_Date%><%--Discontinuation date*****--%>:&nbsp&nbsp</B><%=disDate%></TD>

			</TR>

			<tr>

				<TD class=tdDefault> </TD>

				<TD class=tdDefault ><B><%=LC.L_Discont_Reason%><%--Discontinuation reason*****--%>:&nbsp&nbsp</B><%=disReason%></TD>

			</TR>

			<TR>

				<TD class=tdDefault><B><%=LC.L_Cal_Name%><%--Calendar name*****--%>:&nbsp&nbsp</B><%=calName%></TD>

				<TD class=tdDefault><B><%=LC.L_Start_Date%><%--Start Date*****--%>:&nbsp&nbsp</B><%=protDate%></TD>



			</TR>

		</table>







		<TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">

			  <TR>

			  <TH><p class = "reportHeadings"><%=LC.L_Visit%><%--Visit*****--%></p></TH>

			  <TH><p class = "reportHeadings"><%=LC.L_Suggested_SchdDate%><%--Suggested Scheduled Date*****--%></p></TH>



			<%

			if(eptRight==1){

			%>

	  <TH><p class = "reportHeadings"><%=LC.L_ActualScheduled_Dt%><%--Actual Scheduled Date*****--%></p></TH>



			<%

			}

			%>



			 <TH><p class = "reportHeadings"><%=LC.L_Event_Window%><%--Event Window*****--%></p></TH>

		      <TH><p class = "reportHeadings"><%=LC.L_Events%><%--Events*****--%>Rashi</p></TH>

		      <TH><p class = "reportHeadings"><%=LC.L_Event_Status%><%--Event Status*****--%></p></TH>



			  </TR>

	<%	}

		if ((year != prevYr) || (! thisMonth.equals(prevMonth))){%>





			<TR>

				<TD class=tdDefault Colspan=3><B><%=thisMonth%>&nbsp;&nbsp;<%=year%></B></TD>

			</TR>

	<%	}

      	if(j%2==0){ %>

			<TR class="browserEvenRow">

		<%}else{%>

			<TR class="browserOddRow">

		<% } %>









<%

			if(!visit.equals(prevVisit)){

%>



			<TD colspan = 5><%=visitName%></TD>


			<TD>&nbsp;</TD>

<%
		           	eveName=eveName.replace(' ','~');
	 	           	eveName=eveName.replace('~',' ');

%>

			</TR>

<%

			j++;

%>



<%

			if(j%2==0){

%>

			<TR class="browserEvenRow">

<%

			}else{

%>

			<TR class="browserOddRow">

<%

			}

			}

		//KM-#4369
		if(EJBUtil.isEmpty(actualDate)) {
			schDate = LC.L_Not_Defined/*"Not Defined"*****/;
			actualDate =LC.L_Not_Defined/*"Not Defined"*****/;
			fuzzy_dates = "-";
		}
%>



			<td >&nbsp;</td>

			<td align=center><%=schDate%></td>

			<%

			if(eptRight==1){

			%>

			<TD>

			<%=actualDate%>

			</TD>

			<%

			}

			%>



			<td align=center><%= fuzzy_dates%></td>

			<td align=center><%= eveName%></td>

			<td align=center><%= eveStatus%></td>

			

		     </TR>







		<% prevMonth = thisMonth;

			prev_dt = start_dt;

			prevPatProtId=patprotId;

			prevYr = year;

			prevVisit = visit;

			j++;



			}

		%>



</TABLE>

  <%



} else{

%>

  <jsp:include page="accessdenied.jsp" flush="true"/>



<%

} //end of if for access rights

} //end of if session times out



else



{



%>

  <jsp:include page="timeout_childwindow.jsp" flush="true"/>

  <%



} //end of else body for page right



%>



</body>

</html>







