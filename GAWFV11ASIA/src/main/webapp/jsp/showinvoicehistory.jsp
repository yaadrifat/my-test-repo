<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<%if((request.getParameter("from")==null?"":request.getParameter("from")).equalsIgnoreCase("ntwhistory")){ %>
	<title><%=MC.M_Ntwk_StatHist%><%--Network >> Status History*****--%></title>
	<%}else if(request.getParameter("from").equalsIgnoreCase("ntwusrhistory")){%>
	<title><%=MC.M_Org_User_StatHist%><%--Organization >> Users >> Status History*****--%></title>	
	<% }else{ %>
	<title><%=MC.M_Inv_StatHist%><%--Invoice >> Status History*****--%></title>
	<%} %>
	
	<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*" %>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<%
	String src;
	src= request.getParameter("srcmenu");
	String isPopupWin="0";
	Integer netId=0;
	String from = request.getParameter("from")==null?"":request.getParameter("from");
	String nLevel = "";
	String moduleTable = request.getParameter("moduleTable")==null?"er_invoice":request.getParameter("moduleTable");
	if(moduleTable.equalsIgnoreCase("er_nwusers") || (moduleTable.equalsIgnoreCase("er_nwsites") && from.equals("ntwhistory")) || moduleTable.equalsIgnoreCase("ER_NWUSERS_ADDNLROLES")){
		isPopupWin = "1";
		netId = EJBUtil.stringToNum(request.getParameter("netWorkId"));
		nLevel = (request.getParameter("nLevel")==null)?"":request.getParameter("nLevel");
	}
%>
<jsp:include page="panel.jsp" flush="true">
	<jsp:param name="src" value="<%=src%>"/> 
	<jsp:param name="isPopupWin" value="<%=isPopupWin %>"/>
</jsp:include>

<SCRIPT language="javascript"></SCRIPT>
<body>
<!-- Bug NO: 3976 fixed by PK -->
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"> </div> 
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<br>
<%
	HttpSession tSession = request.getSession(true);
	String tab = request.getParameter("selectedTab");
	String studyId=request.getParameter("studyId");
	String parentNtwId=request.getParameter("parentNtwId");
	String currentStatId = request.getParameter("currentStatId");
	String style="";
%>

    <%if(!from.equalsIgnoreCase("ntwusrhistory") && !from.equalsIgnoreCase("ntwhistory")){ %>
    <DIV class="BrowserTopn" id = "div1">
	 <jsp:include page="milestonetabs.jsp" flush="true">
		<jsp:param name="studyId" value="<%=studyId%>"/>
		<jsp:param name="from" value="<%=from%>"/>
		<jsp:param name="selectedTab" value="<%=tab%>"/>
	  </jsp:include>
	 </div>
	  <%}%>
    
    <%-- <% if(from.equalsIgnoreCase("ntwhistory")){ %>
	  <jsp:include page="accounttabs.jsp" flush="true"> 
			<jsp:param name="selectedTab" value="<%=tab%>"/>
		</jsp:include>
	  <%} %> --%>
	  
	  <%
	  if(from.equalsIgnoreCase("ntwusrhistory") || from.equalsIgnoreCase("ntwhistory")){
		  style="style=\"overflow:auto; height:80%;top:10%\"";
	  }else{
		  style="style=\"overflow:auto; height:60%\"";
	  }%>

<DIV class="BrowserBotN BrowserBotN_SiH_1" id="div1" <%=style%> > 
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<%@ page language = "java" import = "com.velos.eres.business.ulink.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%>
<%
	if (sessionmaint.isValidSession(tSession)){

		String modulePk = request.getParameter("modulePk");
		String fromjsp = request.getParameter("fromjsp");
		String invNumber = request.getParameter("invNumber");
		String pageRight = "";
		pageRight = request.getParameter("pageRight");
                if(pageRight == null)
                   pageRight ="0";
		
		int pgRightInt = EJBUtil.stringToNum(pageRight) ;
		
		String showDel = "";
		
		if (pgRightInt < 6)
		{
			showDel = "false";
		}
		else
		{
			showDel = "true";
		}
	 
		

 	        if (pgRightInt  >= 0 ){


%>
			<jsp:include page="statusHistory.jsp" flush="true">
				<jsp:param name="studyId" value="<%=studyId%>"/>
				<jsp:param name="modulePk" value="<%=modulePk%>"/>
				<jsp:param name="moduleTable" value="<%=moduleTable %>"/>
				<jsp:param name="srcmenu" value="<%=src%>"/>
				<jsp:param name="selectedTab" value="<%=tab%>"/>
				<jsp:param name="fromjsp" value="<%=fromjsp%>"/>
				<jsp:param name="pageRight" value="<%=pageRight%>"/>
				<jsp:param name="invNumber" value="<%=StringUtil.encodeString(invNumber)%>"/>
				<jsp:param name="currentStatId" value="<%=currentStatId%>"/>
				<jsp:param name="showDeleteLink" value="<%=showDel%>"/>
				<jsp:param name="showEditLink" value="false"/>
				<jsp:param name="netWorkId" value="<%=netId%>"/>
				<jsp:param name="nLevel" value="<%=nLevel %>"/>
				<jsp:param name="parentNtwId" value="<%=parentNtwId %>"/>

			</jsp:include>
<%		} //end of if body for page right
		else {
%>
			<jsp:include page="accessdenied.jsp" flush="true"/>
<%
		}//end of else body for page right
	}//end of if body for session
		else{
%>
			<jsp:include page="timeout.html" flush="true"/>

<%	}
%>
	<div>
    	<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</div>
	<div class ="mainMenu" id = "emenu" >
		<jsp:include page="getmenu.jsp" flush="true"/>
	</div>
</body>

</html>

