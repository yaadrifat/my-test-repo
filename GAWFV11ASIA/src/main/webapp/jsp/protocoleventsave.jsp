<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page autoFlush="true" buffer="64kb"%> 
<jsp:include page="localization.jsp"/>
<HTML>
<%@page import="com.velos.eres.audit.web.AuditRowEresJB"%>
<HEAD>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></HEAD>



<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>


<SCRIPT>

function fclose(formobj){

if (formobj.correctEsign.value == "yes") {

		window.opener.location.reload(true);

		self.close();

	}



}


</SCRIPT>


<BODY>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>

<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>

<jsp:useBean id="ctrldao" scope="request" class="com.velos.esch.business.common.EventdefDao"/> <!--KM-->

<jsp:useBean id="assocdao" scope="request" class="com.velos.esch.business.common.EventAssocDao"/>


<%@ page language = "java" import = "com.velos.esch.service.util.EJBUtil,com.velos.esch.web.eventuser.EventuserJB,java.util.*,com.velos.eres.service.util.*"%>




<Form name="form1" METHOD="POST">

<%

Object[] eventIdArray = new String[100];

ArrayList eventcheckedArray = new ArrayList();

String correctEsign="yes";

String eventcheck = "";

for (Enumeration e = request.getParameterNames() ; e.hasMoreElements() ;) {

      System.out.println(e.nextElement());

    }


boolean esignReq=true;
String submitMode=request.getParameter("submitMode");
String studyId=request.getParameter("studyId");
String src = request.getParameter("srcmenu");
String ucsdCalled=request.getParameter("ucsdStudyRoster");  
String duration = request.getParameter("duration");

String protocolId = request.getParameter("protocolId");
String eventid = request.getParameter("eventId");

String calledFrom = request.getParameter("calledFrom");
if("S".equals(calledFrom)){esignReq=false;}

String mode = request.getParameter("mode");

String calStatus = request.getParameter("calStatus");
String showsubmitbutt = request.getParameter("showsubmitbutt")==null?"":request.getParameter("showsubmitbutt");
String from = request.getParameter("from");

if(from == null) from = "";
String cost = "0";
if(from.equals("selecteventus")) {
	if (calledFrom.equals("P") || calledFrom.equals("L")) {
		cost = String.valueOf(eventdefB.getMaxCost(protocolId));
	}else if (calledFrom.equals("S")) {
	 	cost = String.valueOf(eventassocB.getMaxCost(protocolId));;
	}
} else {
	cost = request.getParameter("cost");
}

String eSign = request.getParameter("eSign");

String catId=request.getParameter("catId");

String catName=request.getParameter("catName");

 


//JM: 05May2008, added for, Enh. #PS16
String calledFromPg=request.getParameter("calledFromPage");
calledFromPg = (calledFromPg==null)?"":calledFromPg;

String visId=request.getParameter("visitId");
visId = (visId==null)?"":visId;

String displ=request.getParameter("displacement");
displ = (displ==null)?"0":displ;

//find out the max seq number in the existing rows for the same visit
int max_seq = eventassocB.getMaxSeqForVisitEvents(EJBUtil.stringToNum(visId),"-1", "event_assoc",EJBUtil.stringToNum(eventid) );


String[] strArrEventIds = new String[1000];

//JM: #3531

String patientId = request.getParameter("patId");
patientId = (patientId==null)?"":patientId;


String patProtKey = request.getParameter("patProtId");
patProtKey = (patProtKey==null)?"":patProtKey;


int eventcheckLength=0;

String[] eventdetailArray= new String[100];

eventdetailArray=request.getParameterValues("eventcheckbox_list");

eventIdArray=request.getParameterValues("eventcheckbox"); 
String[] hiddentEventsArr= request.getParameterValues("hiddentEventsArr");

 HttpSession tSession = request.getSession(true);

  if (sessionmaint.isValidSession(tSession))

  {
%>
<%
	if ((tSession.getAttribute("EventArray"))!= null)

	{

		eventcheckedArray=((ArrayList)(tSession.getAttribute("EventArray")));

	}

 }

if (submitMode.equals("refresh")) {

//	System.out.println("i am in refresh mode now");

	String[] eventtempArray = (String[])eventIdArray ;

    if (eventtempArray==null) {

                    //System.out.println("REFRESH MODE eventIdArray is null"+ eventcheckedArray.size());

                    if ((eventcheckedArray!=null) && (eventcheckedArray.size()!=0 )) {eventcheckedArray.clear(); }

                    eventcheckedArray =null;

                    %>	 <jsp:forward page="/jsp/selecteventus.jsp"></jsp:forward>

                    	  <%

                  }

	if ((eventtempArray!=null)|| (eventcheckedArray!=null)) {

	//System.out.println("REFRESH MODE-Length of session variable EventArray in Protocol event is" + eventcheckedArray.size());

	//System.out.println("REFRESH MODE-*&^^*&^^&^*&^&&^&^^^^^^^&&&&*******&&selected fields count is" + eventtempArray.length);

		int index=0;

	for (int i=0;i<eventcheckedArray.size();i++)

	{        index=0;

	         //System.out.println("REFRESH MODE-counter for ArrayList " + i );

		     String eventcheckString=(String)eventcheckedArray.get(i);

		     String compareString = eventcheckString.substring(0,(eventcheckString.indexOf("||") - 2));

		    // System.out.println("REFRESH MODE-arrayList Item " + compareString);



		for (int j=0;j<eventtempArray.length;j++){

			// System.out.println("REFRESH MODE-ArrayITEM" + eventtempArray[j]);

			if ((compareString.trim()).equals(eventtempArray[j].trim()))

 		{

			 index=1;

			 //System.out.println("REFRESH MODE-INDEX value set to " + index);

			 eventtempArray[j]="checked";

  			 break;

		}



  			 // Value is removed from tempArray to enable user to remove duplicates events. Any value that has been compared already is

  			 //removed from the temp array to avoid duplicacy.



		}//end for (int j=0;j<eventtempArray.size();j++)



			//System.out.println("REFRESH MODE-final INDEX value set to " + index);

			if (index==0) {

			//System.out.println("REFRESH MODE-item ot be removed from session ELEMent is " + ((String)eventcheckedArray.get(i)));

			eventcheckedArray.remove(i);

			i=i-1;

			} //emd for if (index==0)





		}//end  for (int i=0;i<eventcheckedArray.size();i++)

	 }//end  if ((eventtempArray.length>0)&& (eventcheckedArray.size()>0))



	 %>

	 	 <jsp:forward page="/jsp/selecteventus.jsp"></jsp:forward>

	  <%



 }//end for (submitMode.equals("refresh"))

if  (submitMode.equals("search")) {

	    // System.out.println("button clicked is SEARCH");

	 		if("called".equals(ucsdCalled)){%>
	 		
	 		 	<jsp:forward page="/jsp/selecteventus.jsp">

            <jsp:param name="catName" value='<%=catName%>'/>
	 	    <jsp:param name="showsubmitbutt" value='<%=showsubmitbutt%>'/>
            <jsp:param name="ucsdStudyRoster" value='<%=ucsdCalled%>'/> 
            <jsp:param name="studyId" value='<%=studyId%>'/>
   	    	<jsp:param name="mode" value="M"/>

   	    </jsp:forward>
   	
	 		
	 		<%}else{ %>

	 	 <jsp:forward page="/jsp/selecteventus.jsp">

	 	    <jsp:param name="catName" value='<%=catName%>'/>
	 	    <jsp:param name="showsubmitbutt" value='<%=showsubmitbutt%>'/>
   	    	<jsp:param name="mode" value="B"/>



	 	      </jsp:forward>

	  <% }}







/*for (int count=0;count<eventdetailArray.length;count++)

{

	eventcheck= ((String)eventdetailArray[count]);

	System.out.println("&&&&&&&String eventcheckbox in protocol event is" + eventcheck);

	eventcheck= eventcheck.substring(0,(eventcheck.indexOf("||")) - 2);

	System.out.println("&&&&&&&String eventcheck after substring in protocol event is" + eventcheck);

	eventIdArray[count]= eventcheck ;



}*/





if  (submitMode.equals("select"))

   {



 if (eventcheckedArray!=null) {eventcheckLength= eventcheckedArray.size();

  System.out.println("Length of session variable EventArray in Protocol event is" + eventcheckedArray.size());}

 if ( eventcheckLength > 0)

 {

	 for (int j=0;j<eventdetailArray.length;j++)

	 {

	 	 eventcheckedArray.add(eventcheckLength + j,eventdetailArray[j]);

	}

}

else {

	for(int i=0;i<eventdetailArray.length;i++)

		 	 {

	 	 // System.out.println("i value is" );

	 	 eventcheckedArray.add(i,eventdetailArray[i]);



	}

 }

   	// System.out.println("Length of session variable EventArray after for-loop in Protocol event is" + eventcheckedArray.size());

   	 tSession.putValue("EventArray",eventcheckedArray);

   	if("called".equals(ucsdCalled)){ %>
   	<jsp:forward page="/jsp/selecteventus.jsp">


            <jsp:param name="ucsdStudyRoster" value='<%=ucsdCalled%>'/> 
            <jsp:param name="studyId" value='<%=studyId%>'/>
   	    	<jsp:param name="mode" value="M"/>

   	    </jsp:forward>
   	
   	
   	<%}else{ %>

   	    <jsp:forward page="/jsp/selecteventus.jsp">

   	    	<jsp:param name="mode" value="B"/>

   	    </jsp:forward>

    <%}}

//out.println(cost);

Object[] catNameArray = new String[100];//KM

eventIdArray=request.getParameterValues("eventcheckbox");
catNameArray = request.getParameterValues("catNames");//km
////KM -issue 3403
Object[] eventNames;
eventNames = request.getParameterValues("eventnames");

//check whether any checkbox is un-checked
// eleminating un-checked event details from array :: bug#20023 > skushwaha [08/29/2014]
hiddentEventsArr= request.getParameterValues("hiddentEventsArr");
ArrayList<Object> finalEventList=null,finalCatNames=null,finalEventNames=null;
	if(hiddentEventsArr.length != eventIdArray.length)
	{
		finalEventList=new ArrayList<Object>();
		finalCatNames=new ArrayList<Object>();
		finalEventNames=new ArrayList<Object>();
		String temp=null;
			for(int i=0; i<eventIdArray.length; i++)	
			{
				temp=(String)eventIdArray[i];
				for(int j=0; j<hiddentEventsArr.length;j++)
				{
					if(((String)hiddentEventsArr[j]).equalsIgnoreCase(temp)){					
					finalEventList.add(i,temp);
					finalCatNames.add(i,catNameArray[j]);
					finalEventNames.add(i,eventNames[j]);
					}	
				}
			}
		eventIdArray=finalEventList.toArray();
		catNameArray=finalCatNames.toArray();
		eventNames=finalEventNames.toArray();
	}
	// check whether any checkbox is un-checked
	// eleminating un-checked event details from array :: bug#20023 > skushwaha [08/29/2014]

ArrayList evtNames = null;
ArrayList categorynames = null;

ArrayList uniqueEvtNames = new ArrayList();
ArrayList ignoreDuplicateIndex = new ArrayList();
ArrayList addevent = new ArrayList();

int newEventLength = 0;

String eveId = "";

if (calledFrom.equals("P") || calledFrom.equals("L")) {

	   ctrldao = eventdefB.getProtSelectedEvents(EJBUtil.stringToNum(protocolId),"asc","lower(name)");
	   evtNames = ctrldao.getNames();
	   categorynames = ctrldao.getEventCategory();
	   if(!evtNames.isEmpty()){
	   for(int m =0; m<evtNames.size(); m++){
		   String allevents = (String)evtNames.get(m);
		   String allcategory = (String)categorynames.get(m);
		   addevent.add(allevents+"/"+allcategory);
		   
	   }}
} else {
	   assocdao= eventassocB.getProtSelectedEvents(EJBUtil.stringToNum(protocolId),"asc","lower(name)");
	   evtNames = assocdao.getNames();
	   categorynames = assocdao.getEventCategory();
	   if(!evtNames.isEmpty()){
	   for(int m =0; m<evtNames.size(); m++){
		   String allevents = (String)evtNames.get(m);
		   String allcategory = (String)categorynames.get(m);
		   addevent.add(allevents+"/"+allcategory);
		   
	   }}
}

String evtName = "";	


//Bug#4651 and 3403
if (!calledFromPg.equals("patientEnroll")) {
	for(int m =0; m < eventNames.length; m++) {
		
		evtName = (String)eventNames[m];
		evtName = evtName.replaceAll("\\r\\n|\\r|\\n", " "); 
		 String catNam = (String)catNameArray[m];
	    
	    if (evtNames.contains(evtName)) {
	    	if(addevent.contains(evtName+"/"+catNam)){
			ignoreDuplicateIndex.add(new Integer(m));}
	    	else{
	    		addevent.add(evtName+"/"+catNam);
	    		uniqueEvtNames.add(evtName);
	    	}}
			else if (uniqueEvtNames.contains(evtName)){
				
				if(addevent.contains(evtName+"/"+catNam)){
					ignoreDuplicateIndex.add(new Integer(m));}
		    	else{
		    		addevent.add(evtName+"/"+catNam);
		    		uniqueEvtNames.add(evtName);
		    	}   	}
   		else{
   			addevent.add(evtName+"/"+catNam);
			uniqueEvtNames.add(evtName);
		}
	}
} else{
	for(int m =0; m < eventNames.length; m++) {
		evtName = (String)eventNames[m];
		uniqueEvtNames.add(evtName);
	}
}
	
newEventLength = eventNames.length - ignoreDuplicateIndex.size();

  
int costNum = EJBUtil.stringToNum(cost);


String eventId = "";

String eventName = "";


//HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession))
{
	String oldESign = (String) tSession.getValue("eSign");
	if("S".equals(calledFrom) && !calledFromPg.equals("patientEnroll"))
	{
    	eSign = oldESign;
    }

	if(!oldESign.equals(eSign))
	{
		correctEsign = "no";

%>

<Input type=hidden name=correctEsign value=<%=correctEsign%>>

  <jsp:include page="incorrectesign.jsp" flush="true"/>

<%

	}
	else
	{
		if("S".equals(calledFrom))
			esignReq=true;
		else
			esignReq=false;
	correctEsign = "yes";

	String ipAdd = (String) tSession.getValue("ipAdd");

	String usr = (String) tSession.getValue("userId");



    String[][] event_disp = new String[newEventLength][5];



			if (cost.equals("-1")) {

				costNum=0;

			}

			else{

				costNum++;

			}


		int counterForEvent_disp = -1;
		
		if  (eventIdArray.length > 0) {

		for(int k =0; k< eventIdArray.length; k++) {
			
			if (! ignoreDuplicateIndex.contains(new Integer(k))) //i.e. the index is not marked as duplicate in the logic above
			{
				counterForEvent_disp = counterForEvent_disp+1;
				
				cost=String.valueOf(costNum);
				
				eventId = (String)eventIdArray[k];
			  	event_disp[counterForEvent_disp][0]=eventId;

		 	  	//JM: 06May2008, added for, Enh. #PS16
		 	  	if (calledFrom.equals("S") && calledFromPg.equals("patientEnroll")) {
		 	  		event_disp[counterForEvent_disp][1]=displ;
		 	  	}
		 	  	else{
		 	  		event_disp[counterForEvent_disp][1]="0";
		 	  	}

				event_disp[counterForEvent_disp][2]=cost;

				if (calledFrom.equals("S") && calledFromPg.equals("patientEnroll")) {
					event_disp[counterForEvent_disp][3]=visId; //To set the fk_visit in the event_assoc field
				}

				event_disp[counterForEvent_disp][4] = (String)catNameArray[k]; //KM

				costNum++;
			}
		}



		if (calledFrom.equals("P") || calledFrom.equals("L")) {

			eventdefB.AddEventsToProtocol(protocolId,event_disp,0,0, usr, ipAdd);

			tSession.removeValue("EventArray");

		} else if (calledFrom.equals("S")) {

			if (calledFrom.equals("S") && calledFromPg.equals("patientEnroll")) {//JM: 05May2008, added for, Enh. #PS16


				//similar to the AddEventsToProtocol method, applied provisionally, insert record in event_assoc table with event_type='U'
				//eventassocB.updateUnscheduledEvents(protocolId,event_disp,0,0, max_seq, usr, ipAdd);
				strArrEventIds = eventassocB.updateUnscheduledEvents(protocolId,event_disp,0,0, max_seq, usr, ipAdd);



				//JM: 20May2008: #3531
				//eventassocB.updateUnscheduledEvents1(EJBUtil.stringToNum(protocolId),EJBUtil.stringToNum(usr), ipAdd, strArrEventIds);
				eventassocB.updateUnscheduledEvents1(EJBUtil.stringToNum(patientId), EJBUtil.stringToNum(patProtKey) , EJBUtil.stringToNum(protocolId),EJBUtil.stringToNum(usr), ipAdd, strArrEventIds);

				String remarks = request.getParameter("remarks");
				if (!StringUtil.isEmpty(remarks)){
					AuditRowEresJB auditJB = new AuditRowEresJB();
					auditJB.setReasonForChangeOfPatProt(StringUtil.stringToNum(patProtKey),StringUtil.stringToNum(usr),remarks);
				}

				tSession.removeValue("EventArray");


			}else{

		 		eventassocB.AddEventsToProtocol(protocolId,event_disp,0,0, usr, ipAdd);

		 		tSession.removeValue("EventArray");
		 	}

		}



 	} //end
 	%>
	<br><br><br><br><br>
	<jsp:include page="include.jsp" flush="true"/> 
	<p class="successfulmsg" align=center><%=MC.M_Data_SavedSucc%><%-- Data saved successfully.*****--%></p>
	<%if("called".equals(ucsdCalled)){ %>
		<script>
			//window.opener.location.reload();
			setTimeout("self.close()",3000);
		</script>
		<%}else{ %>
		<script>
			window.opener.location.reload();
			setTimeout("self.close()",3000);
		</script>
		<%} %>
 <%//end of if for eSign check

%>

	<Input type=hidden name=correctEsign value=<%=correctEsign%>>

<%
	}
}//end of if body for session

else

{

%>

	<jsp:include page="timeout.html" flush="true"/>

  	<%

}

%>

</FORM>

</BODY>

</HTML>





