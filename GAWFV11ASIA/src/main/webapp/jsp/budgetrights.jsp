<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html style="overflow:hidden;">
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=LC.L_Bgt_AccRgt%><%--Budget >> Access Rights*****--%></title>
<%@ page import="com.velos.eres.service.util.StringUtil,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.esch.business.common.BudgetDao,com.velos.eres.service.util.*" %>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id ="budgetUsersB" scope="request" class="com.velos.esch.web.budgetUsers.BudgetUsersJB"/>
<jsp:useBean id="budgetUsersDao" scope="request" class="com.velos.esch.business.common.BudgetUsersDao"/>
<jsp:useBean id="budgetB" scope="page" class="com.velos.esch.web.budget.BudgetJB"/>
<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/><b></b>
</head>


<SCRIPT Language="JavaScript">

var screenWidth = screen.width;
var screenHeight = screen.height;
var refreshRightClick='N';

function refreshRights(formobj,rows)
{
formobj.rfshClick.value='Y';
	
	for(j=0;j<3;j++)
		{
			if(formobj.budgetScope[j].checked== true){
				formobj.budgetScope[j].checked = false;
				formobj.accessFor.value="-";
			}
		}
	if(rows>1)
	{
		for(i=0;i<rows;i++)
			{
				formobj.newr[i].checked = false;
				formobj.edit[i].checked = false;
				formobj.view[i].checked = false;
				formobj.rights[i].value = 0;
				
			}
	}
	else {
				formobj.newr.checked = false;
				formobj.edit.checked = false;
				formobj.view.checked = false;
				formobj.rights.value = 0;
	}
	
}


function changeRights(obj,row, frmname)	{
	  selrow = row ;
	  totrows = frmname.totalrows.value;
	  if (totrows > 1)
		rights =frmname.rights[selrow].value;
	  else
		rights =frmname.rights.value;

	  objName = obj.name;
	  if (obj.checked)  {
       	if (objName == "newr")	{
			rights = parseInt(rights) + 1;
			if (!frmname.view[selrow].checked)	{ 
				frmname.view[selrow].checked = true;
				rights = parseInt(rights) + 4;
			}
		}
       	if (objName == "edit")	{
			rights = parseInt(rights) + 2;
			if (!frmname.view[selrow].checked){ 
				frmname.view[selrow].checked = true;
				rights = parseInt(rights) + 4;
			}
		}
       	if (objName == "view") rights = parseInt(rights) + 4;

		if (totrows > 1 )
			frmname.rights[selrow].value = rights;
		else
			frmname.rights.value = rights;
	  }else	{
	       	if (objName == "newr") rights = parseInt(rights) - 1;
    	   	if (objName == "edit") rights = parseInt(rights) - 2;
	       	if (objName == "view"){	
			if (frmname.newr[selrow].checked) 
			{
				alert("<%=MC.M_UsrRgt_CntRevoke%>");/*alert("The user has right to New or Edit. You can not revoke right to View");*****/
				frmname.view[selrow].checked = true;		
			}
		    else if (frmname.edit[selrow].checked) 
			{
		    	alert("<%=MC.M_UsrRgt_CntRevoke%>");/*alert("The user has right to New or Edit. You can not revoke right to View");*****/
				frmname.view[selrow].checked = true;		
			}
			else
			{		
			  rights = parseInt(rights) - 4;
			}
		}
		if (totrows > 1 )		
			frmname.rights[selrow].value = rights;
		else
			frmname.rights.value = rights;
    	 }
}
	
function validate(formobj) {
	 bgtScope = formobj.accessFor.value;
	 
	 
	 	//check Access Rights have been defined	 
	 len = formobj.rights.length;
	 var rightSelected=false;	 
	 for (i=0;i<len;i++) {
		if ((formobj.rights[i].value)!="0") {
			rightSelected=true;	
			break;	
		}
		else {	 	
			rightSelected=false;
		}
	 }
	 	 
	 //check if accessFor has been selected 
	 	 
	  
	 if (bgtScope=="-" && rightSelected==true) {
		alert("<%=MC.M_Selc_AcesRights%>");/*alert("Please select 'Access Rights for'");*****/
		return false; 			 
	 }



	if (rightSelected==false && bgtScope!="-") {	 
		alert("<%=MC.M_Dfn_AcesRights%>");/*alert("Please Define Access Rights");*****/
		return false;
	}
 
	 
	 //if Organization is selected in Budget Scope, check whether org has been entered in budget
	 if (bgtScope=="O") {
	 	if ((formobj.bgtSite.value == "null") || (formobj.bgtSite.value =="")) {
			alert("<%=MC.M_PlsSpecifyOrg_BgtRgt%>");/*alert("Please specify 'Organization' in budget before selecting rights for 'All Users of this Organization'");*****/
			return false; 		
		}
	 }
	 
  	 //if Study is selected in Budget Scope, check whether study has been entered in budget
	 if (bgtScope=="S") {
	 	if ((formobj.bgtStudy.value =="null") || (formobj.bgtStudy.value =="")) {
			alert("<%=MC.M_PlsSpecifyStd_Bgt%>");/*alert("Please specify '<%=LC.Std_Study%>' in budget before selecting rights for 'All Users within this <%=LC.Std_Study_Lower%> team'");*****/
			return false; 		
		}
	 }	
	
	 if (!(validate_col('e-Signature',formobj.eSign))) return false
	 	 
	<%--  if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
   } --%>
}	
var childWindow;
function openUserWindow(pgRight, formObj) {
	if (f_check_perm(pgRight,'N') == true) {
		leventId = "";
		luserType = "B"; //called from Budget Page
		lsrcmenu = "";
		lduration = "";
		lprotocolId = "";
		lcalledFrom = "";
		lmode = "";
		lfromPage = "";
		leventmode = "";
		lcalStatus = "";
		lbudgetId = formObj.budgetId.value;
		
		childWindow=window.open("addeventuser.jsp?&eventId="+leventId+"&userType="+luserType+"&srcmenu="+lsrcmenu+"&duration="+lduration+"&protocolId="+lprotocolId+"&calledFrom="+lcalledFrom+"&mode="+lmode+"&fromPage="+lfromPage+"&eventmode="+leventmode+"&calStatus="+lcalStatus+"&budgetId="+lbudgetId,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200");
		childWindow.focus();
	} else {
		return false;
	}	
}

function openUsers(formObj,userFlag) {
	studyId = formObj.bgtStudy.value;
	bgtSite = formObj.bgtSite.value;

	//if Organization is selected in Budget Scope, check whether org has been entered in budget
	 if (userFlag=="O") {
	 	if ((bgtSite == "null") || (bgtSite =="")) {
			alert("<%=MC.M_OrgNotSpecified_InBgt%>");/*alert("Organization not specified in Budget");*****/
			return false; 		
		}
	 }
	 
 	 //if Study is selected in Budget Scope, check whether study has been entered in budget
	 if (userFlag=="S") {
	 	if ((studyId =="null") || (studyId =="")) {
			alert("<%=MC.M_StdNotSpecified_InBgt%>");/*alert("<%=LC.Std_Study%> not specified in Budget");*****/
			return false; 		
		}
	 }
	 childWindow=window.open("budgetglobalusers.jsp?studyId="+studyId+"&userFlag="+userFlag+"&siteId="+bgtSite,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=400");
	 childWindow.focus();
}


function confirmBox(usrName,pgRight) {
	if (f_check_perm(pgRight,'E') == true) {
		var paramArray = [usrName];
		if (confirm(getLocalizedMessageString("L_Rmv_FrmBgt",paramArray))) {/*if (confirm("Remove " + usrName + " from Budget?")) {*****/
		    return true;
		} else {
			return false;
		}
	} else { 
		return false;
	}			
}


function setAccessFor(val, formObj) {
	formObj.accessFor.value=val;
}

function closeChildWindow() {
	if(childWindow) {
		childWindow.close();
	}
}

</SCRIPT>


<body style="overflow:hidden;" onunload="closeChildWindow();">

<% String src="";
src= request.getParameter("srcmenu");
String budgetType=request.getParameter("budgetType");
String mode=request.getParameter("mode");
int studyId= EJBUtil.stringToNum(request.getParameter("studyId"));
String budgetTemplate = request.getParameter("budgetTemplate");
String 	bottomdivClass="tabDefBotN";
if ("S".equals(request.getParameter("budgetTemplate"))) { %>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   
<% } else { 
bottomdivClass="popDefault";
%>
<jsp:include page="include.jsp" flush="true"/> 
<% }  %>

<% if ("S".equals(budgetTemplate)) { %>
<DIV class="tabDefTopN" id="div1">
	<jsp:include page="budgettabs.jsp" flush="true"> 
<jsp:param name="budgetType" value="<%=budgetType%>"/>
<jsp:param name="mode" value="<%=mode%>"/>
<jsp:param name="studyId" value="<%=studyId%>"/>
</jsp:include>
	</DIV>
<% }  %>
	
<Form name="budgetRights" id="bdgtrightsfrm" method="post" action="updatebudgetrights.jsp" onSubmit="if (validate(document.budgetRights)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}"> 
<%
HttpSession tSession = request.getSession(true);
String selectedTab = request.getParameter("selectedTab"); 
String budgetId=request.getParameter("budgetId");
ArrayList ids = null;
ArrayList userIds = null;
ArrayList bgtUsrRights = null;
ArrayList bgtUsrLastNames = null;
ArrayList bgtUsrFirstNames = null;
String bgtUsrId=null;
String userId = null;
String bgtUsrRight = "";
String bgtUsrLastName = "";
String bgtUsrFirstName = "";
String bgtUsrFullName = "";
int counter = 0;
int pageRight=0;

if(sessionmaint.isValidSession(tSession))  {

	int iBugtId = EJBUtil.stringToNum(budgetId);
	budgetTemplate=request.getParameter("budgetTemplate");
	if(iBugtId == 0) {
		%>
			<jsp:include page="budgetDoesNotExist.jsp" flush="true"/>
		<%

	} else { 
	GrpRightsJB bRights = (GrpRightsJB) tSession.getAttribute("BRights");		
    pageRight = Integer.parseInt(bRights.getFtrRightsByValue("BGTACCESS"));

    %>			 
	<!-- Bug#15399 Fix for 1360*768 resolution- Tarun Kumar -->	
 <%--  <SCRIPT LANGUAGE="JavaScript">

	//Marked as "height:auto" for the bug id 24107
	if(screenWidth>1280 || screenHeight>1024)
		document.write('<DIV class="<%=bottomdivClass%>" id="div2" style="height:auto;" >')
	else
		document.write('<DIV class="<%=bottomdivClass%>" id="div2" style="height:auto;" >')
	</SCRIPT>		--%>
		 
	<SCRIPT LANGUAGE="JavaScript">
	var isIE = jQuery.browser.msie;
	var screenWidth = screen.width;
	var screenHeight = screen.height;
	<%String budgetValue=request.getParameter("budgetTemplate");%>
	if(screenWidth>1280 || screenHeight>1024)
	{
		if(isIE==true)
		{
			<%
			if("P".equals(budgetValue) || "C".equals(budgetValue))
			{%>
			document.write('<DIV class="<%=bottomdivClass%>" id="div2" style="width:100%;height:90%;overflow:auto;position:absolute;border-bottom: 20%;">')
			<%}
			else
			{%>
			document.write('<DIV class="<%=bottomdivClass%>" id="div2" style="width:99%;height:80%;overflow:auto;position:absolute;border-bottom: 20%;">')
			<%}%>
		}
		else
		{
			<%
			if("P".equals(budgetValue) || "C".equals(budgetValue))
			{%>
			document.write('<DIV class="<%=bottomdivClass%>" id="div2" style="width:99%;height:85%;overflow:auto;position:absolute;border-bottom: 20%;">')
			<%}
			else
			{%>
			document.write('<DIV class="<%=bottomdivClass%>" id="div2" style="width:99%;height:80%;overflow:auto;position:absolute;border-bottom: 20%;">')
			<%}%>
		}
	}
	else
	{
		if(isIE==true)
		{
			<%
			if("P".equals(budgetValue) || "C".equals(budgetValue))
			{%>
			document.write('<DIV class="<%=bottomdivClass%>" id="div2" style="width:100%;height:90%;overflow:auto;position:absolute;border-bottom: 20%;">')
			<%}
			else
			{%>
			document.write('<DIV class="<%=bottomdivClass%>" id="div2" style="width:99%;height:80%;overflow:auto;position:absolute;border-bottom: 20%;">')
			<%}%>

		}
		else
		{
			<%
			if("P".equals(budgetValue) || "C".equals(budgetValue))
			{%>
			document.write('<DIV class="<%=bottomdivClass%>" id="div2" style="width:99%;height:85%;overflow:auto;position:absolute;border-bottom: 20%;">')
			<%}
			else
			{%>
			document.write('<DIV class="<%=bottomdivClass%>" id="div2" style="width:99%;height:85%;overflow:auto;position:absolute;border-bottom: 20%;">')
			<%}%>
			
		}		
	}
</SCRIPT>
	
  <%
	if (pageRight > 0 )
	{
	BudgetDao budgetDao = budgetB.getBudgetInfo(iBugtId);
	ctrl.getControlValues("bgt_rights");
	int rows = ctrl.getCRows();
	
	ArrayList bgtNames = budgetDao.getBgtNames(); 
	ArrayList bgtStats = budgetDao.getBgtStats();
	ArrayList bgtStudyTitles = budgetDao.getBgtStudyTitles();
	ArrayList bgtStudyNumbers = budgetDao.getBgtStudyNumbers();
	ArrayList bgtSites = budgetDao.getBgtSites();
	String bgtName = (String) bgtNames.get(0);
	String bgtStat = (String) bgtStats.get(0);
	String bgtStudyTitle = (String) bgtStudyTitles.get(0);
	String bgtStudyNumber = (String) bgtStudyNumbers.get(0);
	String bgtSiteName = (String) bgtSites.get(0);
	bgtName = (bgtName == null || bgtName.equals(""))?"-":bgtName;
	bgtName = StringUtil.stripScript(bgtName);
	Object[] arguments1 = {bgtName};
	bgtStat = (bgtStat == null || bgtStat.equals(""))?"-":bgtStat;
	if(bgtStat.equals("F")) {
		bgtStat = LC.L_Freeze;/*bgtStat = "Freeze";*****/
	} else if(bgtStat.equals("W")) {
		bgtStat = LC.L_Work_InProgress;/*bgtStat = "Work in Progress";*****/
	}
	bgtStudyTitle = (bgtStudyTitle == null || bgtStudyTitle.equals(""))?"-":bgtStudyTitle;
	bgtStudyNumber = (bgtStudyNumber == null || bgtStudyNumber.equals(""))?"-":bgtStudyNumber;
	Object[] arguments2 = {bgtStudyNumber};
	bgtSiteName = (bgtSiteName == null || bgtSiteName.equals(""))?"-":bgtSiteName;
	Object[] arguments3 = {bgtSiteName};
	
	budgetB.setBudgetId(EJBUtil.stringToNum(budgetId));
	budgetB.getBudgetDetails();
	String bgtScope = budgetB.getBudgetRScope();	
	String bgtSite = budgetB.getBudgetSiteId();
	String bgtStudy = budgetB.getBudgetStudyId();
	if (bgtScope==null) bgtScope="-";
%>
   <Input type=hidden name="budgetId" value="<%=budgetId%>">
   <Input type=hidden name="rfshClick" value="N">
   <Input type=hidden name="srcmenu" value="<%=src%>">
   <Input type=hidden name="bgtStudy" value="<%=bgtStudy%>">
   <Input type=hidden name="bgtSite" value="<%=bgtSite%>">   
   <Input type=hidden name="selectedTab" value="<%=selectedTab%>">
   <Input type=hidden name="budgetType" value="<%=budgetType%>">
   <Input type=hidden name="mode" value="<%=mode%>">       
   <Input type=hidden name="budgetTemplate" value="<%=budgetTemplate%>">

 	 <BR>
    <table width="98%" cellspacing="0" cellpadding="0">
	    <tr> 
			<td class=tdDefault width="50%"><%=VelosResourceBundle.getLabelString("L_BgtName",arguments1)%><%--Budget Name:</B>&nbsp;&nbsp;<%=bgtName%>*****--%></td>
		</tr>
	    <tr> 
			<td class=tdDefault width="50%"><%=VelosResourceBundle.getLabelString("L_StdNum",arguments2)%><%--Study Number:</B>&nbsp;&nbsp;<%=bgtStudyNumber%>*****--%></td>
		</tr>			
	    <tr> 
	        <td class=tdDefault width="50%"><%=VelosResourceBundle.getLabelString("L_Org_Dyna",arguments3)%><%--Organization:</B>&nbsp;&nbsp;<%=bgtSiteName%>*****--%></td>
		</tr>										
	</table>

   <BR>
   <table>
   <tr><td><P class="sectionHeadingsFrm"><%=LC.L_AccessRights_For%><%--Access Rights for*****--%></P></td></tr>
   <tr>
   <td>
   <Input type="hidden" name="accessFor" value="<%=bgtScope%>">
   <%if (bgtScope.equals("A")) { %>
   <Input type="radio" name="budgetScope" value="A" checked onclick="setAccessFor('A',document.budgetRights)"><A onclick="return openUsers(document.budgetRights,'A')" href="#"> <%=MC.M_AllUsr_OfThisAcc%><%--All Users of this Account*****--%></A>
   <%}else {%>
   <Input type="radio" name="budgetScope" value="A" onclick="setAccessFor('A',document.budgetRights)"><A onclick="return openUsers(document.budgetRights,'A')" href="#"><%=MC.M_AllUsr_OfThisAcc%><%--All Users of this Account*****--%></A>
   <%}%>   
   
   <%if (bgtScope.equals("O")) { %>
   <Input type="radio" name="budgetScope" value="O" checked onclick="setAccessFor('O',document.budgetRights)" ><A onclick="return openUsers(document.budgetRights,'O')" href="#"><%=MC.M_AllUsr_OfThisOrg%><%--All Users of this Organization*****--%></A>
	<%}else {%>
   <Input type="radio" name="budgetScope" value="O" onclick="setAccessFor('O',document.budgetRights)"><A onclick="return openUsers(document.budgetRights,'O')" href="#"><%=MC.M_AllUsr_OfThisOrg%><%--All Users of this Organization*****--%></A>
	<%}%>
	
	<%if (bgtScope.equals("S")) { %>        
   <Input type="radio" name="budgetScope" value="S" checked onclick="setAccessFor('S',document.budgetRights)"><A onclick="return openUsers(document.budgetRights,'S')" href="#"><%=MC.M_AllUsr_InStdTeam%><%--All Users within this <%=LC.Std_Study_Lower%> team*****--%> </A>
   <%}else {%>
   <Input type="radio" name="budgetScope" value="S" onclick="setAccessFor('S',document.budgetRights)"><A onclick="return openUsers(document.budgetRights,'S')" href="#"><%=MC.M_AllUsr_InStdTeam%><%--All Users within this <%=LC.Std_Study_Lower%> team*****--%></A>
	<%}%>   
	    &nbsp&nbsp&nbsp;<A onclick="return refreshRights(document.budgetRights,<%=rows%>)" href="#"><%=LC.L_Rem_SelectedOption%><%--Remove Selected Option*****--%></A>
   </td>
   	  
   	   
   <tr>
   </table> 

<%
	

	ArrayList feature =  ctrl.getCValue();
	ArrayList ftrDesc = ctrl.getCDesc();
	ArrayList ftrSeq = ctrl.getCSeq();
%>
    <Input type="hidden" name="totalrows" value=<%=rows%> >
    <TABLE width="80%" >
      <tr><td><P class="sectionHeadingsFrm"><%=LC.L_Define_AccessRights%><%--Define Access Rights*****--%></P></td></tr>
      <tr>
      <TH>&nbsp;</TH>
      <TH><%=LC.L_New%><%--New*****--%></TH>
      <TH><%=LC.L_Edit%><%--Edit*****--%></TH>
      <TH><%=LC.L_View%><%--View*****--%></TH>
      </tr>
<%

	String bgtRights = budgetB.getBudgetRights();

	for(int count=0;count<rows;count++){
	    String ftr = (String) feature.get(count);
    	String desc = (String) ftrDesc.get(count);
    	int seq = ((Integer) ftrSeq.get(count)).intValue();
		//get rights for the specific budget module		
		String rights = String.valueOf(bgtRights.charAt(seq-1));
		
	%>
	   <TR>
       <Input type="hidden" name=rights value=<%=rights%> >
        <TD>
		<%= desc%>
		</TD>
		<%  
		 if (rights.compareTo("7") == 0){//all rights 
		 %> 
        <TD align="center"> 
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>, document.budgetRights)" CHECKED>
        </TD>
        <TD align="center"> 
          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>, document.budgetRights)" CHECKED>
        </TD>
        <TD align="center"> 
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>, document.budgetRights)" CHECKED>
        </TD>
        <%}else if (rights.compareTo("1") == 0){//new right %> 
        <TD align="center"> 
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>, document.budgetRights)" CHECKED>
        </TD>
        <TD align="center"> 
          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>, document.budgetRights)" >
        </TD>
        <TD align="center"> 
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>, document.budgetRights)">
        </TD>
        <%}else if(rights.compareTo("3") == 0){//new & edit right %> 
        <TD align="center"> 
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>, document.budgetRights)" CHECKED>
        </TD>
        <TD align="center"> 
          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>, document.budgetRights)" CHECKED>
        </TD>
        <TD align="center"> 
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>, document.budgetRights)">
        </TD>
        <%}else if (rights.compareTo("2") == 0){//edit right %> 
        <TD align="center"> 
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>, document.budgetRights)">
        </TD>
        <TD align="center"> 
          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>, document.budgetRights)" CHECKED>
        </TD>
        <TD align="center"> 
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>, document.budgetRights)">
        </TD>
        <%}else if (rights.compareTo("4") == 0){//view right %> 
        <TD align="center"> 
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>, document.budgetRights)">
        </TD>
        <TD align="center"> 
          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>, document.budgetRights)">
        </TD>
        <TD align="center"> 
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>, document.budgetRights)" CHECKED>
        </TD>
        <%}else if (rights.compareTo("5") == 0){//new & view right%> 
        <TD align="center"> 
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>, document.budgetRights)" CHECKED>
        </TD>
        <TD align="center"> 
          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>, document.budgetRights)">
        </TD>
        <TD align="center"> 
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>, document.budgetRights)" CHECKED>
        </TD>
        <%}else if (rights.compareTo("6") == 0){//edit & view right%> 
        <TD align="center"> 
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>, document.budgetRights)" >
        </TD>
        <TD align="center"> 
          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>, document.budgetRights)" CHECKED>
        </TD>
        <TD align="center"> 
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>, document.budgetRights)" CHECKED>
        </TD>
        <%}else{ %>

        <TD align="center"> 
          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>, document.budgetRights)">
        </TD>
        <TD align="center"> 
          <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>, document.budgetRights)">
        </TD>
        <TD align="center"> 
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>, document.budgetRights)">
        </TD>
        <%}%>
      </TR>
      	  
	  	  	   
	<%
	}		 
%>
  	 <tr height=15><td colspan="4"></td></tr>
	
	<%	if (pageRight >= 6) { %>		 
	<jsp:include page="submitBar.jsp" flush="true"> 
				<jsp:param name="displayESign" value="Y"/>
				<jsp:param name="formID" value="bdgtrightsfrm"/>
				<jsp:param name="showDiscard" value="N"/>
		   </jsp:include>


	<% }%>	  
	
</table>

   <%
    budgetUsersDao= budgetUsersB.getUsersInBudget(EJBUtil.stringToNum(budgetId));
	ids=budgetUsersDao.getBgtUsers(); 
	bgtUsrRights=budgetUsersDao.getBgtUsersRights();
	bgtUsrLastNames=budgetUsersDao.getUsrLastNames();
	bgtUsrFirstNames=budgetUsersDao.getUsrFirstNames();
	userIds = budgetUsersDao.getUserIds();
	
	int len = ids.size() ;
	
	%>
    <table width="80%">
    	<BR>
   	<tr>
   	<td width="70%"><p class="defComments"><%=MC.M_AcesBgt_RgtViewEdit%><%--The following users have access to this budget. Click on Access Rights to view/edit rights.*****--%></p></td>
   	<td align=right width="30%"><A href=# onClick="openUserWindow(<%=pageRight%>,document.budgetRights)"><%=LC.L_Select_Users%><%--Select Users*****--%></A></td>
   	</tr>
   </table>   
      
   <table width="80%" >
   	<tr> 
   	<th width="50%"><%=LC.L_User_Name%><%--User Name*****--%> </th>
   	<th width="30%"><%=LC.L_Access_Rights%><%--Access Rights*****--%> </th>
   	<th width="20%"><%=LC.L_Delete%><%--Delete*****--%></th>
   	</tr>   
<%
    for(counter = 0;counter<len;counter++) {
		bgtUsrId = (String)ids.get(counter);
		bgtUsrRight=((bgtUsrRights.get(counter)) == null)?"-":(bgtUsrRights.get(counter)).toString();
		bgtUsrLastName=((bgtUsrLastNames.get(counter)) == null)?"-":(bgtUsrLastNames.get(counter)).toString();
		bgtUsrFirstName=((bgtUsrFirstNames.get(counter)) == null)?"-":(bgtUsrFirstNames.get(counter)).toString();
		bgtUsrFullName = bgtUsrFirstName + " " + bgtUsrLastName ;
		userId = (String)userIds.get(counter);
		
		
		if ((counter%2)==0) {
		%>
    	  <tr class="browserEvenRow"> 
       <%
   		}else {
       %>
           <tr class="browserOddRow"> 
        <%
   		}     
        %>	
    	<td><%=bgtUsrFullName%></td>
    	<td style="text-align: center;"><A onClick="return f_check_perm(<%=pageRight%>,'E')" href="budgetuserrights.jsp?srcmenu=<%=src%>&bgtUsrId=<%=bgtUsrId%>&budgetId=<%=budgetId%>&selectedTab=<%=selectedTab%>&budgetType=<%=budgetType%>&mode=<%=mode%>&userId=<%=userId%>&budgetTemplate=<%=budgetTemplate%>" ><img title="<%=LC.L_Assign_Rights%>" src="./images/AccessRights.gif" border ="0"/></A></td>
    	<INPUT name="bgtUsrId" type=hidden value="<%=bgtUsrId%>">
    	<INPUT name="bgtUsrFullName" type=hidden value="<%=bgtUsrFullName%>">					
    	<td align="center"><A href="budgetusersdelete.jsp?srcmenu=<%=src%>&bgtUsrId=<%=bgtUsrId%>&budgetId=<%=budgetId%>&selectedTab=<%=selectedTab%>&budgetType=<%=budgetType%>&mode=<%=mode%>&budgetTemplate=<%=budgetTemplate%>" onclick= "return confirmBox('<%= StringUtil.escapeSpecialCharJS(bgtUsrFullName)%>',<%=pageRight%>)"><img src="./images/delete.gif" border="0" title="<%=LC.L_Delete%>" align="left"/></A></td>																																						
	    </tr>
		 <%
		}
		%>
   </table>
	   
   <%	
	} //end of if body for page right

	else
	{
	%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%
		
		} //end of else body for page right  
	 }//if check for budget exist
  }
  else { //end of if body for session
   
   %>
   <jsp:include page="timeout.html" flush="true"/>
   <%
  }
%>

</Form>
<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</div>

<div class="mainMenu" id="emenu"> 
  <jsp:include page="getmenu.jsp" flush="true"/>
</div>

</body>
</html>

