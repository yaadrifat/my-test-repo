<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=MC.M_BgtAdd_EdtSec %><%-- Budget >> Add/Edit Section*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>


<%@ page import="com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.eres.web.user.UserJB,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.esch.business.common.*"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">



</head>



<SCRIPT Language="javascript">
 function  validate(formobj){  

	if (!(validate_col('<%=LC.L_Sequence%>',formobj.sequence))) return false;

	//bug #6631, #10854
	var seq = formobj.sequence.value;
    for (var i=0;i < seq.length;i++)
    {
        var c = seq.charAt(i) ;
		if (c == "."){
			alert("<%=MC.M_InvalidSeq_EtrVldNum%>");
			formobj.sequence.focus();			
			return false;
		}
    }

	if((!(isInteger(formobj.sequence.value))) && (!(isNegNum(formobj.sequence.value))))
	{
		alert("<%=MC.M_InvalidSeq_EtrVldNum%>");
		formobj.sequence.focus();
		return false;
	}


	if (!(validate_col('<%=LC.L_Sec_Name%>',formobj.secName))) return false
	
	if (!(checkquote(formobj.secName.value)))
	    {
	    	formobj.secName.focus();
		    return false;	
	    } 

	if (!(validateDataSize ( formobj.notes,1000,'<%=LC.L_Notes%>'))) return false		
		

	if (!(validate_col('<%=LC.L_Esignature%>',formobj.eSign))) return false		

// 	if(isNaN(formobj.eSign.value) == true) {

<%--  	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/ --%>
 
//  	formobj.eSign.focus();
 
//  	 return false;
//    }

   }
</SCRIPT>



<body>

<jsp:useBean id="bgtSectionB" scope="session" class="com.velos.esch.web.bgtSection.BgtSectionJB"/>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>



 <% String src = request.getParameter("srcmenu");

 	String budgetTemplate = request.getParameter("budgetTemplate");

	String mode =   request.getParameter("mode");

	String sectionMode =   request.getParameter("sectionMode");		

	String includedIn = request.getParameter("includedIn");
	
	if(StringUtil.isEmpty(includedIn))
	{
	 includedIn = "";
	}

	String fromRt = request.getParameter("fromRt");
	if (fromRt==null || fromRt.equals("null")) fromRt="";
%>

<% if ("S".equals(budgetTemplate)) { %>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<% } else { %>

	<jsp:include page="include.jsp" flush="true"/> 

<% } %>	
<br>

<%
	String 	bottomdivClass="popDefault";

	if (! (includedIn.equals("P") || 
	        (includedIn.equals("") && !"S".equals(budgetTemplate)))) 
	{ 
			bottomdivClass="tabDefBotN";
		%>
		


			<DIV class="tabDefTopN" id="div1">

			<jsp:include page="budgettabs.jsp" flush="true">

			<jsp:param name="budgetTemplate" value="<%=budgetTemplate%>"/>

			<jsp:param name="mode" value="<%=mode%>"/>		

			</jsp:include>
			</DIV>

	<% } %>
			<div class="<%=bottomdivClass%>" id="div2">	
		<BR>

  <%



	HttpSession tSession = request.getSession(true);

	if (sessionmaint.isValidSession(tSession))

	{	

	String selectedTab =   request.getParameter("selectedTab");

	String bgtSectionId=request.getParameter("bgtSectionId");

	String secType = "";
	String sequence="";
	String secName = "";
	String notes = "";

	String calId = request.getParameter("calId");

	String budgetId = request.getParameter("budgetId");

	if(sectionMode.equals("M"))

	{

	 bgtSectionB.setBgtSectionId(EJBUtil.stringToNum(bgtSectionId));

	 bgtSectionB.getBgtSectionDetails();
	 secType = bgtSectionB.getBgtSectionType();
	 secType = (secType==null)?"P":secType;
	  
	 sequence = bgtSectionB.getBgtSectionSequence();
	 secName = bgtSectionB.getBgtSectionName();
	 notes = bgtSectionB.getBgtSectionNotes();
	 notes = (notes==null)?"":notes;	 
	 calId = bgtSectionB.getBgtCal();
	}
	%>


<Form name="budgetsection" id="bdgtsection" method="post" action="updatebgtnewsection.jsp"  onSubmit="if (validate(document.budgetsection)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

<TABLE border=0 cellPadding=1 cellSpacing=1 width="98%">

 <% if (budgetTemplate.equals("P") || budgetTemplate.equals("C") ) {%>
<tr>
  <td width="20%"><%=LC.L_Sec_Type%><%--Section Type*****--%><FONT class="Mandatory">  * </FONT></td>
  <td>
  <select name="secType">
    <option value="P" <%if (secType.equals("P")) {%>Selected<%}%> ><%=LC.L_PerPatient_Fees%><%--Per <%=LC.Pat_Patient%> Fees*****--%></option>
    <option value="O" <%if (secType.equals("O")) {%>Selected<%}%> ><%=LC.L_One_TimeFees%><%--One Time Fees*****--%></option>		   
  </select>
  </td>
</tr>
<%}%>

<tr>

  <td> &nbsp;&nbsp;&nbsp; <%=LC.L_Sequence%><%--Sequence*****--%><FONT class="Mandatory">  * </FONT></td>

  <td>

  <input type=text name="sequence" maxlength=100 size=20 value='<%=sequence%>'>

  </td>

</tr>



<tr>

  <td> &nbsp;&nbsp;&nbsp; <%=LC.L_Sec_Name%><%--Section Name*****--%><FONT class="Mandatory">  * </FONT></td>
  <td>

  <input type="text" name="secName" maxlength=100 size=20 value= "<%=secName%>">

  </td>

 </tr>
<tr>
  <td> &nbsp;&nbsp;&nbsp; <%=LC.L_Notes%><%--Notes*****--%></td>

  <td>
	<TextArea name="notes" rows=3 cols=40 MAXLENGTH =1000><%=notes%></TextArea>
  </td>

</tr>


</table>


<input type="hidden" name="src" MAXLENGTH = 15 value="<%=src%>">

<input type="hidden" name="calId" MAXLENGTH = 15 value="<%=calId%>">

<input type="hidden" name="mode" MAXLENGTH = 15 value="<%=mode%>">

<input type="hidden" name="budgetId" MAXLENGTH = 15 value="<%=budgetId%>">

<input type="hidden" name="selectedTab" MAXLENGTH = 15 value="<%=selectedTab%>">

<input type="hidden" name="bgtSectionId" MAXLENGTH = 15 value="<%=bgtSectionId%>">

<input type="hidden" name="budgetTemplate" value="<%=budgetTemplate%>">

<input type="hidden" name="sectionMode" value="<%=sectionMode%>">
<input type="hidden" name="includedIn" value="<%=includedIn%>">

<input type="hidden" name="fromRt" value="<%=fromRt%>">

<br>

<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="bdgtsection"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>


	</Form>

	<%



	

}//end of if body for session



else



{

%>

  <jsp:include page="timeout.html" flush="true"/>

  <%

}



%>

  <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</div>
	
	<%

	if (! (includedIn.equals("P") || 
	        (includedIn.equals("") && !"S".equals(budgetTemplate)))) 
	{ 
		%>

		<div class ="mainMenu" id = "emenu" id = "emenu">

	<jsp:include page="getmenu.jsp" flush="true"/>

	</div>

<% } %>

</body>

</html>

