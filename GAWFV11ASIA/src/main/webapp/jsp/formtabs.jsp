<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ page language = "java" import = "com.velos.eres.business.site.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.LC"
%>
<%@ page import="com.velos.eres.web.objectSettings.ObjectSettingsCache, com.velos.eres.web.objectSettings.ObjectSettings"%>

<script>
	
	function openFormPreview(formId)
{
	windowName=window.open("formpreview.jsp?formId="+formId,"Information","toolbar=no,scrollbars=yes,resizable=no,menubar=no,status=yes,width=825,height=600,top=90,left=150");
	windowName.focus();
}

</script>
	
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="formlibB" scope="request" class="com.velos.eres.web.formLib.FormLibJB"/>
<%
  String selclass="";
  String tab= request.getParameter("selectedTab");
  String formId = request.getParameter("formId");
  String mode = request.getParameter("mode");
  String codeStatus =""; 
  codeStatus=request.getParameter("codeStatus");
  String calledFrom = request.getParameter("calledFrom");
  String lnkFrom = request.getParameter("lnkFrom");
  String studyId = request.getParameter("studyId");
  String formCount="";
   formCount=request.getParameter("formCount");
  //out.println("calledFrom"+calledFrom);

  int  formLibId =  0 ;
  
  String name = "";
  
  HttpSession tSession = request.getSession(true); 
  String acc = (String) tSession.getValue("accountId");

  ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
  ArrayList tabList = objCache.getAccountObjects(EJBUtil.stringToNum(acc), "form_tab");
  
  if (sessionmaint.isValidSession(tSession))
  {
	String uName = (String) tSession.getValue("userName");
	
	if (!(formId.equals(""))) {
	  formLibId = EJBUtil.stringToNum(formId);
  	  formlibB.setFormLibId(formLibId);
	  formlibB.getFormLibDetails();
	  name=formlibB.getFormLibName();
    }
%>

	<% if (tab.equals("1"))
	{ 
	   if(calledFrom.equals("St")){%>
		  
		   <%}
	  else if(calledFrom.equals("A")){%>
			
		   <%}	   
	   else{%>
		
		   <%}
	}
	else if (tab.equals("2"))
		{
		   if(calledFrom.equals("St")){%>
			  
			   <%}
		  else if(calledFrom.equals("A")){%>
		
		   <%}			   
		   else{%>
		
		   <%}
	}
	
	else if (tab.equals("3"))
		{
		   if(calledFrom.equals("St")){%>
			    
			   <%}
		   else if(calledFrom.equals("A")){%>
			
		   <%}		   
		   else{%>
		
			<%}
		}
}%>

<DIV>
	<!-- <table  class= "selectedTab" cellspacing="0" cellpadding="0" border="0">   -->
		<table cellspacing="0" cellpadding="0" border="0"> 
		<tr>


	<%

	for (int iX=0; iX<tabList.size(); iX++) {
	ObjectSettings settings = (ObjectSettings)tabList.get(iX);

	if ("0".equals(settings.getObjVisible())) {
		continue;
	}

	boolean showThisTab = false;
	if ("1".equals(settings.getObjSubType())) {
		 showThisTab = true; 
	} 
	else if ("2".equals(settings.getObjSubType())) {
		showThisTab = true; 
		
	}
	else if ("3".equals(settings.getObjSubType())) {
		showThisTab = true; 
	}
    else {
		showThisTab = true;
	}

	if (!showThisTab) { continue; } 


	if (tab == null) { 
		selclass = "unselectedTab";
	} else if (tab.equals(settings.getObjSubType())) {
		selclass = "selectedTab";
	} else {
		selclass = "unselectedTab";
	}
	 %>	



		<td  valign="TOP">
				<table class="<%=selclass%>"  cellspacing="0" cellpadding="0"  border="0">
					<tr>
				<!--  	<td class="<%=selclass%>" rowspan="3" valign="top" >
							<img src="../images/leftgreytab.gif" height=20 border=0 alt="">
						</td>  -->
						<td class="<%=selclass%>">
						
						<%if ("1".equals(settings.getObjSubType())) { %>
		
						<a href="formcreation.jsp?mode=<%=mode%>&calledFrom=<%=calledFrom%>&srcmenu=tdmenubaritem4&selectedTab=1&formLibId=<%=formId%>&lnkFrom=<%=lnkFrom%>&studyId=<%=studyId%>"><%=settings.getObjDispTxt()%></a>
						<%} else if ("2".equals(settings.getObjSubType())) {%>
						<a href="formfldbrowser.jsp?calledFrom=<%=calledFrom%>&srcmenu=tdmenubaritem4&selectedTab=2&formId=<%=formId%>&mode=<%=mode%>&codeStatus=<%=codeStatus%>&lnkFrom=<%=lnkFrom%>&studyId=<%=studyId%>"><%=settings.getObjDispTxt()%></a>
						
						<%} else if ("3".equals(settings.getObjSubType())) {%>
						<a href="formsettings.jsp?calledFrom=<%=calledFrom%>&srcmenu=tdmenubaritem4&selectedTab=3&formId=<%=formId%>&mode=<%=mode%>&codeStatus=<%=codeStatus%>&lnkFrom=<%=lnkFrom%>&studyId=<%=studyId%>"><%=settings.getObjDispTxt()%></a>
						<%}%>

						</td> 
				<!--	<td class="<%=selclass%>" rowspan="3" valign="top" >
							<img src="../images/rightgreytab.gif"  height=20 border=0 alt="">
						</td> -->
					</tr> 
				</table> 
		</td> 

<%}%>

	
			
		</tr>
   <!--  	<tr>
		     <td colspan=5 height=10></td>
	    </tr>  --> 
	</table>
<table class="tabBorder" width="99%" height="5"><tr><td></td></tr></table>	




	<% if ( mode.equals("M")) { %>
		<table> 
		<tr>
		<td><P class="sectionHeadingsFrm"><%=LC.L_Frm_Name %><%-- Form Name*****--%>:<%=name%> &nbsp;&nbsp;
			<A href="#" onClick="openFormPreview(<%=formId%>)"><!--<b>(<%=LC.L_Preview %><%-- Preview*****--%>) </b>-->
			<img src="./images/Preview.gif" title="<%=LC.L_Preview%>" alt="<%=LC.L_Preview %><%--Preview*****--%>" border ="0" align="absbotton">
			</A>
			<p><b><%=LC.L_Curent_DtaCptre_Flds%><%= formCount%> </b></p>
			<p><b>You can add only 950 fields in a Form.</b></p>
				</P>
			</td>
		</tr>
		</table>
	<%}%>
</DIV>

