<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title> <%=LC.L_Adhoc_Defn%><%--Ad-Hoc Query >> Filter Definition*****--%> </title>
	<%@ page language = "java" import = "java.util.*,com.velos.eres.web.dynrep.holder.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB"%>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<jsp:useBean id="dynrepB" scope="page" class="com.velos.eres.web.dynrep.DynRepJB" />
	<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
	<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script>
<%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>

function replaceFieldKeywords(sql)
{
	// for replacing field keywords for form default columns, not in use right now

		sql = replaceSubstring(sql,"veladhoc_studynumber"," (SELECT study_number FROM ER_STUDY WHERE pk_study=ID) ");
		sql = replaceSubstring(sql,"veladhoc_patcode","(SELECT per_code FROM ER_PER WHERE pk_per=ID )");
		sql = replaceSubstring(sql,"veladhoc_patstudyid"," (select NVL(patprot_patstdid,'<I>(Patient removed from study)</I>') from er_patprot where pk_patprot = fk_patprot) ");
		sql = replaceSubstring(sql,"veladhoc_formstatus"," (select codelst_desc from er_codelst where pk_codelst = form_completed) ");
		return sql
}


function changeMode(formobj,modeStr){

formobj.mode.value=modeStr;
if(isNaN(formobj.addrow.value) == true) {

	alert("<%=MC.M_InorrectRowsNum_ReEtr%>");/*alert("Incorrect number of rows specified. Please enter again");*****/

	formobj.addrow.focus();

	return false;
   }
}




function setType(formobj,count){
totcount=formobj.fldTypeCount.value; //total count from total number of fields

if (totcount==1){
index=formobj.fldName.selectedIndex;
if (index<totcount){
type=formobj.fldTypeOrig.value;
formobj.fldType.value=type;

}
}else if (totcount>1){

index=formobj.fldName[count].selectedIndex;

if (index<totcount){
type=formobj.fldTypeOrig[index].value
formobj.fldType[count].value=type;

}}
//JM: 28July2006
if (formobj.fldName[count].value=='nVal' )
	 	{
	 		alert("<%=MC.M_PlsSel_ValidFld%>");/*alert("Please select a valid field");*****/
	 		formobj.fldName[count].value = "";
	 		formobj.fldName[count].focus();
	 		return false;
	 	}
}

function saveFilter(formobj,mode)
{
	if (mode=='modifyPrevFilter')
	{
		if (formobj.selFltr.value=='')
		{
			alert("<%=MC.M_PlsSel_FtrFst%>");/*alert("Please select a filter first");*****/
			return false;
		}
	}

	if (mode == 'addrow')
	{
		if(isNaN(formobj.addrow.value) == true) {

		alert("<%=MC.M_InorrectRowsNum_ReEtr%>");/*alert("Incorrect number of rows specified. Please enter again");*****/

		formobj.addrow.focus();

		return false;

		}

		var fltrName=fnTrimSpaces(formobj.fltrName.value);
		if (fltrName.length==0 ){
			 alert("<%=MC.M_FilterName_ToProc%>");/*alert("Please specify a filter name to proceed.");*****/
 			 formobj.fltrName.focus();
 			return false;
 		}


}

	formobj.mode.value=mode;
	return setFilter(formobj);
	}

function nextPage(formobj)
{
	formobj.mode.value="next";
	return setFilter(formobj);
}

function setMainDateFilter(formobj)
{
    var dtFilterFrom;
    var dtFilterTo;
    var yr;
    var month;
    var monthYr;
    var lastDayOfMonth;
    var monthPrefix = "";


	if (formobj.dateRangeType[0].checked) //ALL
	{
		dtFilterFrom = "";
		dtFilterTo = "";
	}
	else if (formobj.dateRangeType[1].checked) //Status Filter
	{
		dtFilterFrom = formobj.statusFrom.value;
		dtFilterTo = formobj.statusTo.value;

		if(dtFilterFrom ==''||dtFilterFrom ==null){
			alert("<%=MC.M_PlsSel_FrmStat%>");/*alert("Please select a 'From Status'");*****/
			formobj.statusFrom.focus();
			return false;
		}

		if(dtFilterTo ==''||dtFilterTo ==null){
			alert("<%=MC.M_PlsSel_ToStat%>");/*alert("Please select a 'To Status'");*****/
			formobj.statusTo.focus();
			return false;
		}
	}
	else if (formobj.dateRangeType[2].checked) //Year Filter
	{


		yr = formobj.yearFilter.value;

		if (yr == '0')
		{
			alert("<%=MC.M_Selc_AnYear%>");/*alert("Please select an Year");*****/
			formobj.yearFilter.focus()
			return false;
		}

		dtFilterFrom = getFormattedDateString(  yr,  "01", "01");
		dtFilterTo = getFormattedDateString(  yr,  "31",  "12");
	}
	else if (formobj.dateRangeType[3].checked) //Month Filter
	{
		monthYr = formobj.monthFilterYear.value;
		month =  formobj.monthFilter.value;

		if (month == '0')
		{
			alert("<%=MC.M_Selc_Month%>");/*alert("Please select a Month");*****/
			formobj.monthFilter.focus()
			return false;
		}

		if (monthYr == '0')
		{
			alert("<%=MC.M_Selc_AnYear%>");/*alert("Please select an Year");*****/
			formobj.monthFilterYear.focus()
			return false;
		}
		lastDayOfMonth = getLastDayOfMonth(month,monthYr);

		if (month > 0 && month <10)
		{
			monthPrefix = "0";
		}
		else
		{
			monthPrefix = "";
		}
		dtFilterFrom = getFormattedDateString(  monthYr,  "01",  month);
		dtFilterTo = getFormattedDateString(  monthYr,  lastDayOfMonth,  month);


	} else if (formobj.dateRangeType[4].checked) //date Range Filter
	{

		dtFilterFrom = formobj.drDateFrom.value;
		dtFilterTo = formobj.drDateTo.value;

		if(dtFilterFrom ==''||dtFilterFrom ==null){
			alert("<%=MC.M_SelFromDt_ForRangeFilter%>");/*alert("Please select a 'From Date' for the Date Range filter");*****/
			return false;
		}
		if(dtFilterTo ==''||dtFilterTo  ==null){
			alert("<%=MC.M_SelToDt_ForRangeFilter%>");/*alert("Please select a 'To Date' for the Date Range filter");*****/
			return false;
		}

	}


	formobj.dtFilterDateFrom.value = dtFilterFrom;
	formobj.dtFilterDateTo.value = dtFilterTo;

	return true;

}

function setFilter(formobj){

	var fltrName="";
	var arrLen=5;
	formId=formobj.allFormIdStr.value;

	var dynType = "";
	dynType = formobj.dynType.value;

	var addToEverySQL="";
	var formIndexForPKData = -1;
	var mainTableName = "";
	var mainFilterColName = "";
	var mainTablePk = -1;
	var mainStudyCol = "";
	var mapIgnoreFilter = "";
	var accountCheckSQL = "";

	if (formId.length>0)
	  arrLen=(formId.split(";")).length;

	allForms=formId.split(";");

	arrayOfForms=new Array();
	arrayOfFilters=new Array();
	endBracDeserver=new Array();
	deserverCount=0;
	totcount=formobj.totcount.value;


	fltrName=fnTrimSpaces(formobj.fltrName.value);

	var startcount=0;
	var endcount=0;
	var filterTemp="";
	var filter="";
	var criteriaStr="";
	var extend="";
	var prevCount=-1;
	var anotherJoinOp = "";
	//do the validation first for other fields

	if (dynType != 'P') //its a patient report
	{
		if (formobj.dateRangeType[1].checked)
		{
			alert("<%=MC.M_PatFromStatDt_SelPatRpts%>");/*alert("Date Filter '<%=LC.Pat_Patient_Lower%> from status date of' can be selected for <%=LC.Pat_Patient%> reports only. Please select some other Date Filter.");*****/
			return false;
		}
	}

	if (! setMainDateFilter(formobj))
	{
		return false;
	}

	// if any date range filter is selected and no filter name is given, prompt for filter name

	if ((fltrName.length == 0) && (formobj.dateRangeType[1].checked || formobj.dateRangeType[2].checked || formobj.dateRangeType[3].checked || formobj.dateRangeType[4].checked))
	{
		alert("<%=MC.M_FilterName_ToProc%>");/*alert("Please specify a filter name to proceed.");*****/
	 	formobj.fltrName.focus();
	 	return false;
	}

	if (totcount==1){
		if (formobj.fldName.value.length>0 && fltrName.length==0 ){
		 alert("<%=MC.M_FilterName_ToProc%>");/*alert("Please specify a filter name to proceed.");*****/
		 formobj.fltrName.focus();
		 return false;
		 }
	} else {
		for (j=0;j<totcount;j++){
			if ((formobj.fldName[j].value.length>0) && (fltrName.length==0) ){
				alert("<%=MC.M_FilterName_ToProc%>");/*alert("Please specify a filter name to proceed.");*****/
				formobj.fltrName.focus();
				return false;
		 	} else {
	 			continue;
	 		}
	 	}
	}
	//end validation

	//check if any date filter is specified, if yes, prepare the dte filter string
	var defdateFilter = ""; var defdateFilterWithAnd = "";
	var defDateFilterFrom;
	var defDateFilterTo;
	var defdateStatusFilterWithAnd = "";
	var defdateStatusFilter = "";

	var defdateStatusFilterWithAndReplaced = "";
	var reportStudy = "";
	var defdateFilterWithAndMain = "";

	var matchCreatedOnDate = "";
	var matchLastModDate = "";

	reportStudy = formobj.reportStudy.value;
	var patStudyFilter = "";
	var respAccessRightWhereClause = " and ( pkg_filledform.fn_getUserAccess (fk_form,[:loggedinuser],creator) > 0 )"	;

	if ((dynType == 'P') && (reportStudy.length > 0)) //its a patient report for a selected study
	{
		patStudyFilter = " and ( (fk_patprot IS NULL) OR " +reportStudy + " = (SELECT fk_study FROM ER_PATPROT WHERE pk_patprot=fk_patprot)) " ;
	}


	if (formobj.dateRangeType[2].checked || formobj.dateRangeType[3].checked || formobj.dateRangeType[4].checked)
	{
		defDateFilterFrom = formobj.dtFilterDateFrom.value ;
		defDateFilterTo = formobj.dtFilterDateTo.value ;
		defdateFilter = " ( trunc(created_on) >= to_date('" + defDateFilterFrom + "',PKG_DATEUTIL.F_GET_DATEFORMAT) and trunc(created_on) <= to_date('" + defDateFilterTo + "',PKG_DATEUTIL.F_GET_DATEFORMAT) )";
		defdateFilterWithAndMain = " AND ( trunc(created_on) >= to_date('" + defDateFilterFrom + "',PKG_DATEUTIL.F_GET_DATEFORMAT) and trunc(created_on) <= to_date('" + defDateFilterTo + "',PKG_DATEUTIL.F_GET_DATEFORMAT) )";
	}

	if (dynType == 'P') //its a patient report
	{
		if (formobj.dateRangeType[1].checked)
		{
			defDateFilterFrom = formobj.dtFilterDateFrom.value ;
			defDateFilterTo = formobj.dtFilterDateTo.value ;

			defdateStatusFilter = " ( trunc(created_on) >= NVL(Pkg_Util.f_get_min_patstudystatdate([VELSTUDY],[VELPAT],'" + defDateFilterFrom + "'),TO_DATE(PKG_DATEUTIL.f_get_future_null_date_str,PKG_DATEUTIL.F_GET_DATEFORMAT)) ";
			defdateStatusFilter = defdateStatusFilter + "  and trunc(created_on) <= NVL(Pkg_Util.f_get_min_patstudystatdate([VELSTUDY],[VELPAT],'" + defDateFilterTo + "'),SYSDATE) )";

			defdateStatusFilterWithAnd = " AND ( trunc(created_on) >= NVL(Pkg_Util.f_get_min_patstudystatdate([VELSTUDY],[VELPAT],'" + defDateFilterFrom + "'),TO_DATE(PKG_DATEUTIL.f_get_future_null_date_str,PKG_DATEUTIL.F_GET_DATEFORMAT)) ";
			defdateStatusFilterWithAnd = defdateStatusFilterWithAnd + "  and trunc(created_on) <= NVL(Pkg_Util.f_get_min_patstudystatdate([VELSTUDY],[VELPAT],'" + defDateFilterTo + "'),SYSDATE) )";
		}
	}
	//

	if (totcount==1){
		for (i=0;i<totcount;i++){
			if (formobj.exclude.checked)
				continue;
			startBrac=formobj.startBracket.value;
			if (formobj.startBracket.checked)
				startcount++;
			fldName=formobj.fldName.value;
			arrayOfFldName=fldName.split("[VELSEP]");
			formId=arrayOfFldName[0];

			formIndexForPKData = getFormPKDataIndex(formobj,formId);

			if (formIndexForPKData != -2 ) //-2 if no data found
			{
				if (formIndexForPKData  == -1) //there is only one record so no array
				{
					mainTableName = formobj.mapPKTableName.value;
					mainTablePk  = formobj.mapTablePK.value;
					mainFilterColName = formobj.mapTableFilterCol.value;
					mainStudyCol = formobj.mapMapStudyColumn.value;
					mapIgnoreFilter = formobj.mapIgnoreFilter.value;
				}
				else
				{
					mainTableName = formobj.mapPKTableName[formIndexForPKData].value;
					mainTablePk  = formobj.mapTablePK[formIndexForPKData].value;
					mainFilterColName = formobj.mapTableFilterCol[formIndexForPKData].value;
					mainStudyCol = formobj.mapMapStudyColumn[formIndexForPKData].value;
					mapIgnoreFilter = formobj.mapIgnoreFilter[formIndexForPKData].value;
				}

				addToEverySQL =  "select distinct " + mainTablePk + " as ID from " + mainTableName ;

				if (dynType == 'A')
				{
					accountCheckSQL = " and " + mainTablePk + " = [:VELACCOUNT]";
				}

				if (dynType == 'S') // for study adHoc, the PK column is fk/pk study
				{
					mainStudyCol = mainTablePk;
				}

			}

			fldName=arrayOfFldName[1];
			criteria=formobj.scriteria.value;
			fltdata=formobj.fltData.value;
			endBrac=formobj.endBracket.value;
			fldType=formobj.fldType.value;
			if (formobj.startBracket.checked)
			{
			   endcount++;
			}
			//extend=formobj.extend.value;

			if (fldName.length==0 && fltdata.length==0 && criteria.length==0)
				continue;
			if (((fldName.length>0) && fltdata.length==0) && criteria.length==0){
				alert("<%=MC.M_SelValue_ForQualifier%>");/*alert("Please select a value for the Qualifier.");*****/
				formobj.scriteria.focus();
				return false;
			}

			if (((fldName.length>0) && fltdata.length>0) && criteria.length==0){
				alert("<%=MC.M_SelValue_ForQualifier%>");/*alert("Please select a value for the Qualifier.");*****/
				formobj.scriteria.focus();
				return false;
			}

			if ((fldName.length==0) && (fltdata.length>0 && criteria.length>0)){
				alert("<%=MC.M_Selc_FldFld%>");/*alert("Please select a filter Field.");*****/
				formobj.fldName.focus();
				return false;
			}

			if (isNaN(formId))
			{
				//check for core table field filter, restrict for certain criteria type:
				if (criteria=="first" || criteria=="latest" || criteria=="highest" || criteria=="lowest")
				{
					alert("<%=MC.M_FwgOptEres_HighPlsAppr%>");/*alert("Following Filter Criteria options are not applicable to eResearch Core Forms: /n First Value,Latest Value,Highest Value, Lowest Value /n Please select an appropriate Criteria.");*****/
					formobj.scriteria.focus();
					return false;
				}
			}

			if (((fldName.length>0) && criteria.length>0) &&  fltdata.length==0 && (criteria!="first" && criteria!="latest" && criteria!="highest" && criteria!="lowest" && criteria!="isnull" && criteria!="isnotnull")){
				var paramArray = [(i+1)];
				alert(getLocalizedMessageString("M_Row_PlsEtrVal",paramArray));/*alert("Row#"+(i+1)+"-Please enter a value.");*****/
				formobj.fltData.focus();
				return false;
			}
			if ((fldType=="ED") && (criteria=="isequalto" || criteria=="isgt" || criteria=="islt" || criteria=="isgte" || criteria=="islte" )){
				if (!validate_date(formobj.fltData)) return false;
			}
			if ((fldType=="EN") && (criteria=="isequalto" || criteria=="isgt" || criteria=="islt" || criteria=="isgte" || criteria=="islte" )){
				if(isNaN(formobj.fltData.value) == true) {
					alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid number.");*****/
					formobj.fltData.focus();
					return false;
				}
			}

			if ((fldName.length==0 && fltdata.length==0) &&  (criteria.length>0 ))
				continue;
			if ((fldName.length==0 && criteria.length==0) && (fltdata.length>0))
				continue;

			if (formobj.startBracket.checked){
				filterTemp="(";
			}
			if (criteria=="isnull"){
				if (fldName.indexOf("|")>-1){

					tempArray=fldName.split("|");
					for(count=0;count<tempArray.length;count++){

					    if (criteriaStr.length==0)
					    	criteriaStr="(("+tempArray[count]+") is null ";
					    else
					   		criteriaStr=criteriaStr+" or ("+tempArray[count]+") is null ";
					}
	    			criteriaStr=criteriaStr+")";
	    		} else{
	   				criteriaStr="("+fldName+")  is null"
	   			}
  			}

  			if (criteria=="isnotnull"){
 				if (fldName.indexOf("|")>-1){

					tempArray=fldName.split("|");
					for(count=0;count<tempArray.length;count++){

						if (criteriaStr.length==0)
							criteriaStr="(("+tempArray[count]+") is not null ";
						else
							criteriaStr=criteriaStr+" or ("+tempArray[count]+") is not null ";
					}
					criteriaStr=criteriaStr+")";
				} else{
					criteriaStr="("+fldName+")  is not null"
				}
  			}

			if (criteria=="contains"){
				if (fldName.indexOf("|")>-1){
					tempArray=fldName.split("|");

					for(count=0;count<tempArray.length;count++){
						if (criteriaStr.length==0)
							criteriaStr="lower("+tempArray[count]+") like lower('%"+ fltdata +"%')";
						else
							criteriaStr=criteriaStr+" or lower("+fldName+") like lower('%"+ fltdata +"%')";
					}
				} else{
					criteriaStr="lower("+fldName+") like lower('%"+ fltdata +"%')"
				}
			}

			if (criteria=="notcontains")
			{
				if (fldName.indexOf("|")>-1){
					tempArray=fldName.split("|");

					criteriaStr = " ( LOWER('[repsep]'||"

					for(count=0;count<tempArray.length;count++)
					{
						if (count==0){
						 	criteriaStr = criteriaStr + tempArray[count];
						}else{
							criteriaStr = criteriaStr + "|| '[repsep]' || " + tempArray[count];
						}
					}
					if (fldType=="MD" || fldType=="MR" ){
  						criteriaStr = criteriaStr + "||'[repsep]') NOT LIKE LOWER('%[repsep]%"+ fltdata +"%[VELSEP1]%[repsep]%') ) ";
					} else {
					   criteriaStr = criteriaStr + "||'[repsep]') NOT LIKE LOWER('%[repsep]%"+ fltdata +"%[repsep]%') ) ";
					}
				} else{
					criteriaStr="lower(nvl("+fldName+",'' )) not like lower('%"+ fltdata +"%')"
				}

 			}


			if (criteria=="isequalto"){
 				if (fldType=="ED") {
					if (isNaN(formId)){
						matchCreatedOnDate = fldName.match("created_on");
						matchLastModDate = fldName.match("last_modified_date");

						if (matchCreatedOnDate == null && matchLastModDate == null)	{
   							criteriaStr="F_TO_DATE("+fldName+") = TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
   						} else 	{
   							if (matchCreatedOnDate == "created_on" ){
   								criteriaStr="F_TO_DATE(pkg_util.date_to_char(created_on)) = TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
   							} else if (matchLastModDate == "last_modified_date"){
   									criteriaStr="F_TO_DATE(pkg_util.date_to_char(last_modified_date)) = TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
   							}
   						}
 					}else{
 						criteriaStr=+fldName+" = TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
 					}
 				}
 				else if (fldType=="EN") criteriaStr="pkg_util.f_to_number(nvl(" + fldName +",'')) = TO_NUMBER('"+ fltdata +"')"
				else if (fldType=="MD" || fldType=="MR" )
	   				criteriaStr="lower("+fldName+") like lower('"+ fltdata +"[VELSEP1]%')"
				else if (fldType=="CL"){
     				criteriaStr="dbms_lob.compare("+fldName+", '"+ fltdata +"') = 0"
				}
				else  criteriaStr="lower("+fldName+") = lower('"+ fltdata +"')"
			}

			if (criteria=="isnotequal"){
				if (fldType=="ED")
				{
					if (! isNaN(formId))
					{
 						matchCreatedOnDate = fldName.match("created_on");
						matchLastModDate = fldName.match("last_modified_date");
 						if (matchCreatedOnDate == null && matchLastModDate == null)
						{
 							criteriaStr="F_TO_DATE(nvl("+fldName+",f_get_null_date_str)) != TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
 						}
						else
   						{
   							if (matchCreatedOnDate == "created_on" )
   							{
					   			criteriaStr= "F_TO_DATE(nvl(pkg_util.date_to_char(created_on),f_get_null_date_str)) != TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
					   		}
					   		else if (matchLastModDate == "last_modified_date")
					   		{
					   			criteriaStr="F_TO_DATE(nvl(pkg_util.date_to_char(last_modified_date),f_get_null_date_str)) != TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
					   		}
   						}
					}
 					else
 					{
 	  					criteriaStr=" nvl("+fldName+",TO_DATE(f_get_null_date_str,PKG_DATEUTIL.F_GET_DATEFORMAT) ) != TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
 					}
				}
				else if (fldType=="EN") criteriaStr="pkg_util.f_to_number(nvl(" + fldName +",0))  != TO_NUMBER('"+ fltdata +"')"
				else if (fldType=="MD" || fldType=="MR" )
					   criteriaStr="lower("+fldName+") not like lower('"+ fltdata +"[VELSEP1]%')"
				else if (fldType=="CL"){
				     criteriaStr="dbms_lob.compare("+fldName+", '"+ fltdata +"') != 0"
				}
				else criteriaStr="lower(nvl("+fldName+",'')) != lower('"+ fltdata +"')"
			}

			if (criteria=="start"){
				criteriaStr="lower("+fldName+") like lower('"+ fltdata +"%')"
			}
			if (criteria=="ends"){
				criteriaStr="lower("+fldName+") like lower('%"+ fltdata +"')"
			}
			if (criteria=="isgt"){
				if (fldType=="ED")
				{
					if (isNaN(formId))
					{
						matchCreatedOnDate = fldName.match("created_on");
						matchLastModDate = fldName.match("last_modified_date");

						if (matchCreatedOnDate == null && matchLastModDate == null)
						{
							criteriaStr="F_TO_DATE("+fldName+") > TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
						}
						else
						{
							if (matchCreatedOnDate == "created_on" )
							{
							criteriaStr="F_TO_DATE(pkg_util.date_to_char(created_on)) > TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
							}
							else if (matchLastModDate == "last_modified_date")
							{
								criteriaStr="F_TO_DATE(pkg_util.date_to_char(last_modified_date)) > TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
							}
						}
					} else 	{
  						criteriaStr=+fldName+" > TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
  					}
 				}
   				else if (fldType=="EN") criteriaStr="pkg_util.f_to_number(nvl(" + fldName +",'') ) > TO_NUMBER('"+ fltdata +"')"
   				else if (fldType=="CL"){
   			       criteriaStr="dbms_lob.compare("+fldName+", '"+ fltdata +"') > 0"
   			  	}
   				else   criteriaStr="lower("+fldName+") > lower('%"+ fltdata +"%')"
   			}
  			if (criteria=="islt"){
				if (fldType=="ED")
				{
			    	if (isNaN(formId))
					{
						matchCreatedOnDate = fldName.match("created_on");
						matchLastModDate = fldName.match("last_modified_date");

						if (matchCreatedOnDate == null && matchLastModDate == null)
						{
					  		criteriaStr="F_TO_DATE("+fldName+") < TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
					  	}
					  	else
					   	{
					   		if (matchCreatedOnDate == "created_on" )
					   		{
					   			criteriaStr="F_TO_DATE(pkg_util.date_to_char(created_on)) < TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
					   		}
					   		else if (matchLastModDate == "last_modified_date")
					   		{
					   			criteriaStr="F_TO_DATE(pkg_util.date_to_char(last_modified_date)) < TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
					   		}
					   	}
				  	}
				  	else
				  	{
					  	criteriaStr= fldName+" < TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
				  	}
			  	}
			    else if (fldType=="EN") criteriaStr="pkg_util.f_to_number(nvl(" + fldName +",'') )< TO_NUMBER('"+ fltdata +"')"
			    else if (fldType=="CL"){
	   			       criteriaStr="dbms_lob.compare("+fldName+", '"+ fltdata +"') < 0"
	   			  	}
			   	else criteriaStr="lower("+fldName+") < lower('%"+ fltdata +"%')"
			}
  			if (criteria=="isgte"){
    			if (fldType=="ED")
    			{
			    	if (isNaN(formId))
					{
				    	matchCreatedOnDate = fldName.match("created_on");
						matchLastModDate = fldName.match("last_modified_date");

						if (matchCreatedOnDate == null && matchLastModDate == null)
						{

					    	criteriaStr="F_TO_DATE("+fldName+") >= TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
					    }
				    	else
					   	{
							if (matchCreatedOnDate == "created_on" )
					   		{
					   			criteriaStr="F_TO_DATE(pkg_util.date_to_char(created_on)) >= TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
					   		}
					   		else if (matchLastModDate == "last_modified_date")
					   		{
					   			criteriaStr="F_TO_DATE(pkg_util.date_to_char(last_modified_date)) >= TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
					   		}
		   				}
	    			}
	    			else
	    			{
		    			criteriaStr= fldName+ " >= TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
	    			}
    			}
     			else if (fldType=="EN") criteriaStr="pkg_util.f_to_number(nvl(" + fldName +",'') ) >= TO_NUMBER('"+ fltdata +"')"
     			else if (fldType=="CL"){
   			       criteriaStr="dbms_lob.compare("+fldName+", '"+ fltdata +"') >= 0"
   			  	}
   				else criteriaStr="lower("+fldName+") >= lower('%"+ fltdata +"%')"
  			}

  		  	if (criteria=="islte"){
   				if (fldType=="ED")
   				{
   					if (isNaN(formId))
					{
						matchCreatedOnDate = fldName.match("created_on");
						matchLastModDate = fldName.match("last_modified_date");

						if (matchCreatedOnDate == null && matchLastModDate == null)
						{
				   		criteriaStr="F_TO_DATE("+fldName+") <= TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
				   		}
				   		else
					   	{
					   		if (matchCreatedOnDate == "created_on" )
					   		{
					   			criteriaStr="F_TO_DATE(pkg_util.date_to_char(created_on)) <= TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
					   		}
					   		else if (matchLastModDate == "last_modified_date")
					   		{
					   			criteriaStr="F_TO_DATE(pkg_util.date_to_char(last_modified_date)) <= TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
					   		}
					   	}

				   	}
				   	else
				   	{
					   	criteriaStr= fldName+" <= TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
				   	}
			   	}
				else if (fldType=="EN") criteriaStr="pkg_util.f_to_number(nvl(" + fldName +",'') ) <= TO_NUMBER('"+ fltdata +"')"
				else if (fldType=="CL"){
	   				criteriaStr="dbms_lob.compare("+fldName+", '"+ fltdata +"') <= 0"
				}
				else criteriaStr="lower("+fldName+") <= lower('%"+ fltdata +"%')"
			}

			if (criteria=="first"){
   				if (! isNaN(formId))
   				{
   					criteriaStr="filldate=(select min(filldate) from er_formslinear where fk_form="+formId+")";
   				}
   				else
   				{
   					criteriaStr="";
   				}
  			}
  			if (criteria=="latest"){
  	  			if (! isNaN(formId))
  	  			{
   					criteriaStr="filldate=(select max(filldate) from er_formslinear where fk_form="+formId+")";
   	  			}
   	  			else
	   			{
	   				criteriaStr="";
	   			}
  			}

		  	if (criteria=="lowest"){
   	  			if (! isNaN(formId))
  	  			{
	   				criteriaStr="lower("+fldName+") =(select min(lower("+ fldName+")) from er_formslinear where fk_form="+ formId+")"
	  			}
	  			else
	   			{
	   				criteriaStr="";
	   			}
  			}

		 	if (criteria=="highest"){
		  		if (! isNaN(formId))
		  	  	{
			   		criteriaStr="lower("+fldName+") =(select max(lower("+ fldName+")) from er_formslinear where fk_form ="+ formId+")"
			  	}
			  	else
			   	{
			   		criteriaStr="";
			   	}
			}
  			if (criteriaStr.length>0){
  				filter=filter+filterTemp+criteriaStr;
  				if (formobj.endBracket.checked){
    				filter=filter+" )";
  				}
  			}
 		}//end of For loop

		defdateFilterWithAnd = defdateFilterWithAndMain;

		//prepare patient study status date range filter
		if (dynType == 'P' && defdateStatusFilterWithAnd.length > 0)
		{
			defdateStatusFilterWithAndReplaced = defdateStatusFilterWithAnd;
			defdateStatusFilterWithAndReplaced = replaceSubstring(defdateStatusFilterWithAndReplaced,"[VELPAT]",mainTablePk);

			if (! isNaN(formId))
			{
				if 	(reportStudy.length == 0)
				{
					defdateStatusFilterWithAndReplaced = defdateStatusFilter;
					defdateStatusFilterWithAndReplaced = replaceSubstring(defdateStatusFilterWithAndReplaced,"[VELPAT]",mainTablePk);

					defdateStatusFilterWithAndReplaced	= replaceSubstring(defdateStatusFilterWithAndReplaced,"[VELSTUDY]","(SELECT fk_study FROM ER_PATPROT WHERE pk_patprot=fk_patprot)");
					defdateStatusFilterWithAndReplaced = " and ((fk_patprot is null) or " + defdateStatusFilterWithAndReplaced + " ) ";
				}
			}
			else
			{
				if (mainStudyCol.length > 0 && reportStudy.length == 0)
				{
					defdateStatusFilterWithAndReplaced	= replaceSubstring(defdateStatusFilterWithAndReplaced,"[VELSTUDY]",mainStudyCol);
				}
			}

			if (reportStudy.length > 0)
			{
				defdateStatusFilterWithAndReplaced	= replaceSubstring(defdateStatusFilterWithAndReplaced,"[VELSTUDY]",reportStudy);
			}

			if (isNaN(formId) && reportStudy.length == 0 && mainStudyCol.length == 0)
		 	{
		 		defdateStatusFilterWithAndReplaced = "";
		 	}
		}

		if (isNaN(formId) && mapIgnoreFilter.length > 0)
		{
	 		defdateStatusFilterWithAndReplaced = "";
	 		defdateFilterWithAnd = "";
	 	}

		if (! isNaN(formId))
		{
			filter = (addToEverySQL+" where "+ mainFilterColName +" = "+formId + patStudyFilter + defdateFilterWithAnd +  " and " +filter + respAccessRightWhereClause);
			//for patient staus date range
		}
		else
		{
			filter = (addToEverySQL+ " where " +filter + defdateFilterWithAnd + accountCheckSQL);
			// in case of core forms, append the patient status date range filter

			if (mainStudyCol.length > 0 && reportStudy.length > 0)
			{
				// add filter for study
				filter = filter + " and  " + mainStudyCol + " = " + reportStudy;
			}
		}

		filter = filter + defdateStatusFilterWithAndReplaced;

	}
	else{ //else for totcount WHEN ROWS > 1

		criteriaStr="";
		extendSibling="";
		extendSibling="";
		addStartBrac="";
		addEndBrac="";


		for(i=0;i<totcount;i++){

			filterTemp="";
			criteriaStr="";
			if (formobj.exclude[i].checked)
		 		continue;
			startBrac=formobj.startBracket[i].value;
			if (formobj.startBracket[i].checked)
			{
				startcount++;
				addStartBrac="Y";
			}
			else
			{
				addStartBrac = "";
			}
 			fldName=formobj.fldName[i].value;

 			if (fldName.length>0)
			{
				arrayOfFldName=fldName.split("[VELSEP]");
				formId=arrayOfFldName[0];

				formIndexForPKData = getFormPKDataIndex(formobj,formId);

				if (formIndexForPKData != -2 ) //-2 if no data found
				{
 	 				if (formIndexForPKData  == -1) //there is only one record so no array
 	 				{
			 	  	 	mainTableName = formobj.mapPKTableName.value;
			 			mainTablePk  = formobj.mapTablePK.value;
			 			mainFilterColName = formobj.mapTableFilterCol.value;
			 			mainStudyCol = formobj.mapMapStudyColumn.value;
			 			mapIgnoreFilter = formobj.mapIgnoreFilter.value;
				 	}
				 	else
				 	{
				 		mainTableName = formobj.mapPKTableName[formIndexForPKData].value;
						mainTablePk  = formobj.mapTablePK[formIndexForPKData].value;
				 		mainFilterColName = formobj.mapTableFilterCol[formIndexForPKData].value;
				 		mainStudyCol = formobj.mapMapStudyColumn[formIndexForPKData].value;
				 		mapIgnoreFilter = formobj.mapIgnoreFilter[formIndexForPKData].value;
				 	}

 					addToEverySQL =  "select distinct " + mainTablePk + " as ID from " + mainTableName ;

					if (dynType == 'A')
			       	{
			        		accountCheckSQL = " and " + mainTablePk + " = [:VELACCOUNT]";

			       	}

	 				if (dynType == 'S') // for study adHoc, the PK column is fk/pk study
					{
						mainStudyCol = mainTablePk;
					}
  				}
  				fldName=arrayOfFldName[1];
			}

			criteria=formobj.scriteria[i].value;
			fltdata=formobj.fltData[i].value;
			endBrac=formobj.endBracket[i].value;
			fldType=formobj.fldType[i].value;

			if (formobj.endBracket[i].checked)
			   endcount++;
			if ((fldName.length==0 && fltdata.length==0) &&  (criteria.length>0))
				continue;
			if ((fldName.length==0 && criteria.length==0) && (fltdata.length>0))
				continue;
			if (((fldName.length>0) && fltdata.length==0) && criteria.length==0){
				var paramArray = [(i+1)];
				alert(getLocalizedMessageString("M_Row_PlsSelQualifier",paramArray));/*alert("Row#"+(i+1)+"-Please select a value for the Qualifier.");*****/
				formobj.scriteria[i].focus();
				return false;
			}
			if ((fldName.length==0) && (fltdata.length==0 &&  criteria.length==0))
				continue;

			if ((i>0) && (filter.length>0))  {
 				extend=formobj.extend[prevCount].value;
			}else {
 				extend="";
 			}
 			if (extend=="and") extendSibling='intersect';
 			if (extend=="or") extendSibling='union';
 			prevCount=i;

			if (((fldName.length>0) && fltdata.length>0) && criteria.length==0){
				var paramArray = [(i+1)];
				alert(getLocalizedMessageString("M_Row_PlsSelQualifier",paramArray));/*alert("Row#"+(i+1)+"-Please select a value for the Qualifier.");*****/
				formobj.scriteria[i].focus();
				return false;
			}

			if ((fldName.length==0) && (fltdata.length>0 && criteria.length>0)){
				var paramArray = [(i+1)];
				alert(getLocalizedMessageString("M_Row_PlsSelFld",paramArray));/*alert("Row#"+(i+1)+"-Please select a filter Field.");*****/
				formobj.fldName[i].focus();
				return false;
			}

			if (isNaN(formId))
			{
				//check for core table field filter, restrict for certain criteria type:
				if (criteria=="first" || criteria=="latest" || criteria=="highest" || criteria=="lowest")
				{
					alert("<%=MC.M_FlwgCritEres_1FstVal3High%>");/*alert("Following Filter Criteria options are not applicable to eResearch Core Forms: \n\n 1. First Value\n 2. Latest Value \n 3. Highest Value\n 4. Lowest Value \n\n Please select an appropriate Criteria.");*****/
					formobj.scriteria[i].focus();
					return false;
				}
			}

			if (((fldName.length>0) && criteria.length>0) &&  fltdata.length==0 && (criteria!="first" && criteria!="latest" && criteria!="highest" && criteria!="lowest" && criteria!="isnull" && criteria!="isnotnull")){
				var paramArray = [(i+1)];
				alert(getLocalizedMessageString("M_Row_PlsEtrVal",paramArray));/*alert("Row#"+(i+1)+"-Please enter a value.");*****/
				formobj.fltData[i].focus();
				return false;
			}

			if ((fldType=="ED") && (criteria=="isequalto" || criteria=="isgt" || criteria=="islt" || criteria=="isgte" || criteria=="islte" )){
				if (!validate_date(formobj.fltData[i])) return false;
			}
			if ((fldType=="EN") && (criteria=="isequalto" || criteria=="isgt" || criteria=="islt" || criteria=="isgte" || criteria=="islte" )){
				if(isNaN(formobj.fltData[i].value) == true) {
					alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid number.");*****/
					formobj.fltData[i].focus();
					return false;
   				}
			}
			if (formobj.startBracket[i].checked){
	 			filterTemp=" "+extendSibling+" (";
  			}
  			else {
  	 			filterTemp=" "+extendSibling+" ";
  			}
  			if (criteria=="isnull"){
   				if (fldName.indexOf("|")>-1){
    				tempArray=fldName.split("|");
    				for(count=0;count<tempArray.length;count++){
					    if (criteriaStr.length==0)
					    	criteriaStr="(("+tempArray[count]+") is null ";
					    else
					    	criteriaStr=criteriaStr+" or ("+tempArray[count]+") is null";
					}
					criteriaStr=criteriaStr+")";
				} else{
				   criteriaStr="("+fldName+") is null"
				}
  			}

			if (criteria=="isnotnull"){
 				if (fldName.indexOf("|")>-1){

 					tempArray=fldName.split("|");
    				for(count=0;count<tempArray.length;count++){

						if (criteriaStr.length==0)
  							criteriaStr="(("+tempArray[count]+") is not null ";
  						else
  							criteriaStr=criteriaStr+" or ("+tempArray[count]+") is not null ";
  					}
  					criteriaStr=criteriaStr+")";
  				} else{
 					criteriaStr="("+fldName+")  is not null"
 				}
			}

			if (criteria=="contains"){
  				if (fldName.indexOf("|")>-1){
   					tempArray=fldName.split("|");
					for(count=0;count<tempArray.length;count++){
						if (criteriaStr.length==0)
					   		criteriaStr="(lower("+tempArray[count]+") like lower('%"+ fltdata +"%')";
					   	else
					   		criteriaStr=criteriaStr+" or lower("+tempArray[count]+") like lower('%"+ fltdata +"%')";
					}
					criteriaStr=criteriaStr+")";
				} else{
					criteriaStr="lower("+fldName+") like lower('%"+ fltdata +"%')"
  				}
 			}

   			if (criteria=="notcontains")
 			{
   				if (fldName.indexOf("|")>-1){
    				tempArray=fldName.split("|");

    				criteriaStr = " ( LOWER('[repsep]'||"

    				for(count=0;count<tempArray.length;count++)
    				{
      					if (count==0){
       						criteriaStr = criteriaStr + tempArray[count];
      					}
      					else
      					{
      						criteriaStr = criteriaStr + "|| '[repsep]' || " + tempArray[count];
      					}
    				}
			    	if (fldType=="MD" || fldType=="MR" )
				    {
				      	criteriaStr = criteriaStr + "||'[repsep]') NOT LIKE LOWER('%[repsep]%"+ fltdata +"%[VELSEP1]%[repsep]%') ) ";
				    }
				    else
				    {
				      	criteriaStr = criteriaStr + "||'[repsep]') NOT LIKE LOWER('%[repsep]%"+ fltdata +"%[repsep]%') ) ";
				    }
			   }
			   else{

			   		criteriaStr="lower(nvl("+fldName+",'' )) not like lower('%"+ fltdata +"%')"
			   }
			}

  			if (criteria=="isequalto"){
				if (fldName.indexOf("|")>-1){
			    	tempArray=fldName.split("|");
			    	for(count=0;count<tempArray.length;count++){

			    		if (criteriaStr.length==0){
				    		if (fldType=="ED")
				    		{
				   		 		if (! isNaN(formId))
		  	  					{
	    		 					criteriaStr="(F_TO_DATE("+tempArray[count]+") = TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
						    	}
	    						else
	    						{
	    							criteriaStr="( " + tempArray[count] + " = TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
	    						}
	    					}
	    					else if (fldType=="EN")
	    					{
	     						criteriaStr="(pkg_util.f_to_number(nvl(" + tempArray[count] +",'') ) =TO_NUMBER('"+ fltdata +"')"
	     					}
	    					else if (fldType=="MD" || fldType=="MR" )
	    					{
	  	   						criteriaStr="(lower("+tempArray[count]+") like lower('"+ fltdata +"[VELSEP1]%')"
	  	 					}
	    					else
	    					{
	    						criteriaStr="(lower("+tempArray[count]+") = lower('"+ fltdata +"')"
	    					}
    					}
    					else{
							if (fldType=="ED")
							{
								if (! isNaN(formId))
								{
									criteriaStr=criteriaStr +" or F_TO_DATE("+tempArray[count]+") = TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
								}
								else
								{
									criteriaStr=criteriaStr +" or "+tempArray[count]+" = TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
								}
							}
							else if (fldType=="EN") criteriaStr=criteriaStr +" or pkg_util.f_to_number(nvl(" + tempArray[count] +",'') ) = TO_NUMBER('"+ fltdata +"')"
							else if (fldType=="MD" || fldType=="MR" )
						  	   	criteriaStr= criteriaStr + " or lower("+tempArray[count]+") like lower('"+ fltdata +"[VELSEP1]%')"
						  	else if (fldType=="CL"){
					   			criteriaStr="dbms_lob.compare("+fldName+", '"+ fltdata +"') = 0"
					   		}
							else criteriaStr=criteriaStr +" or lower("+tempArray[count]+") = lower('"+ fltdata +"')"
						}
    				}
    				criteriaStr=criteriaStr+")";
    			}else{
    				if (fldType=="ED")
    				{
				    	if (! isNaN(formId))
						{
						 	matchCreatedOnDate = fldName.match("created_on");
							matchLastModDate = fldName.match("last_modified_date");

							if (matchCreatedOnDate == null && matchLastModDate == null)
							{
						 	    criteriaStr="F_TO_DATE("+fldName+") = TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
						 	}
							else
						   	{
						   		if (matchCreatedOnDate == "created_on" )
						   		{
						   			criteriaStr="F_TO_DATE(pkg_util.date_to_char(created_on)) = TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
						   		}
						   		else if (matchLastModDate == "last_modified_date")
						   		{
						   			criteriaStr="F_TO_DATE(pkg_util.date_to_char(last_modified_date)) = TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
						   		}
						   	}
					    }
					    else
					    {
							criteriaStr= fldName+ " = TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
					    }
					}
    				else if (fldType=="EN") criteriaStr="pkg_util.f_to_number(nvl(" + fldName +",'') ) = TO_NUMBER('"+ fltdata +"')"
				    else if (fldType=="MD" || fldType=="MR" )
						criteriaStr="lower("+fldName+") like lower('"+ fltdata +"[VELSEP1]%')"
					else if (fldType=="CL"){
	   			       criteriaStr="dbms_lob.compare("+fldName+", '"+ fltdata +"') = 0"
	   			  	}
				    else  criteriaStr="lower("+fldName+") = lower('"+ fltdata +"')"
				}
  			}

   			if (criteria=="isnotequal"){
				if (fldName.indexOf("|")>-1){
			   		tempArray=fldName.split("|");

			    	criteriaStr = " ( LOWER('[repsep]'||"

			    	for(count=0;count<tempArray.length;count++)
			    	{
			      		if (count==0){
			       			criteriaStr = criteriaStr + tempArray[count];
			      		}
			      		else
			      		{
			      			criteriaStr = criteriaStr + "|| '[repsep]' || " + tempArray[count];
			      		}
			    	}
			    	if (fldType=="MD" || fldType=="MR" ){
			      		criteriaStr = criteriaStr + "||'[repsep]') NOT LIKE LOWER('%[repsep]"+ fltdata +"[VELSEP1]%[repsep]%') ) ";
			    	}else if (fldType=="CL"){
		   				criteriaStr="dbms_lob.compare("+fldName+", '"+ fltdata +"') != 0"
		   			}else criteriaStr = criteriaStr + "||'[repsep]') NOT LIKE LOWER('%[repsep]"+ fltdata +"[repsep]%') ) ";
    			}else{
    				if (fldType=="ED")
				    {
				    	if (! isNaN(formId))
						{
						    matchCreatedOnDate = fldName.match("created_on");
							matchLastModDate = fldName.match("last_modified_date");

							if (matchCreatedOnDate == null && matchLastModDate == null)
							{
					     		criteriaStr="F_TO_DATE(nvl("+fldName+" ,f_get_null_date_str)) != TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
				     		}
							else
						   	{
						   		if (matchCreatedOnDate == "created_on" )
						   		{
						   			criteriaStr="F_TO_DATE(nvl(pkg_util.date_to_char(created_on),f_get_null_date_str)) != TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"

						   		}
						   		else if (matchLastModDate == "last_modified_date")
						   		{
							   		criteriaStr="F_TO_DATE(nvl(pkg_util.date_to_char(last_modified_date) ,f_get_null_date_str)) != TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"

						   		}
						   	}
					     }
					     else
					     {
						   criteriaStr=" nvl("+fldName+" ,TO_DATE(f_get_null_date_str,PKG_DATEUTIL.F_GET_DATEFORMAT) ) != TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
					     }

				    }
				    else if (fldType=="EN") criteriaStr="pkg_util.f_to_number(nvl(" + fldName +",0) ) != TO_NUMBER('"+ fltdata +"')"
				    else if (fldType=="MD" || fldType=="MR" )
				  	   criteriaStr="lower(nvl("+fldName+",' ')) not like lower('"+ fltdata +"[VELSEP1]%')"
				  	else if (fldType=="CL"){
				        criteriaStr="dbms_lob.compare("+fldName+", '"+ fltdata +"') != 0"
				    }
				    else  criteriaStr="lower(nvl("+fldName+",'')) != lower('"+ fltdata +"')"
    			}
  			}

   		  	if (criteria=="start"){
			   	if (fldName.indexOf("|")>-1){
			   		tempArray=fldName.split("|");
			    	for(count=0;count<tempArray.length;count++){
					    if (criteriaStr.length==0)
					    	criteriaStr="(lower("+tempArray[count]+") like lower('"+ fltdata +"%')"
					    else
					    	criteriaStr=criteriaStr+" or lower("+tempArray[count]+") like lower('"+ fltdata +"%')"
					}
					criteriaStr=criteriaStr+")";
				}else{
					criteriaStr="lower("+fldName+") like lower('"+ fltdata +"%')"
				}
			}

			if (criteria=="ends"){
				if (fldName.indexOf("|")>-1){
			    	tempArray=fldName.split("|");
			    	for(count=0;count<tempArray.length;count++){
			    		if (criteriaStr.length==0)
			    			criteriaStr="(lower("+tempArray[count]+") like lower('%"+ fltdata +"')"
			    		else
			    			criteriaStr=criteriaStr + " or lower("+tempArray[count]+") like lower('%"+ fltdata +"')"
			    	}
			    	criteriaStr=criteriaStr+")";
			  	}else{
			    	criteriaStr="lower("+fldName+") like lower('%"+ fltdata +"')"
			    }
			}

		  	if (criteria=="isgt"){
   				if (fldName.indexOf("|")>-1){
				    tempArray=fldName.split("|");
				    for(count=0;count<tempArray.length;count++){
				    if (criteriaStr.length==0){
				    	if (fldType=="ED")
				      	{
					    	if (! isNaN(formId))
						 	{
					      		criteriaStr="(F_To_DATE("+tempArray[count]+") > TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
					      	}
					      	else
					      	{
					      		criteriaStr="( "+tempArray[count]+" > TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
					      	}
				      	}
				    	else if (fldType=="EN") criteriaStr="( pkg_util.f_to_number(nvl(" + tempArray[count] +",'')) > TO_NUMBER('"+ fltdata +"')"
			    		else if (fldType=="CL"){
		   					criteriaStr="dbms_lob.compare("+fldName+", '"+ fltdata +"') > 0"
		   				} else criteriaStr="(lower("+tempArray[count]+") > lower('%"+ fltdata +"%')"
				    }else{
					    if (fldType=="ED")
					    {
					    	if (! isNaN(formId))
							{
						    	criteriaStr=criteriaStr + " or F_TO_DATE("+tempArray[count]+") > TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
						    }
						    else
						    {
						    	criteriaStr=criteriaStr + " or "+tempArray[count]+" > TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
						    }
						}
					    else if (fldType=="EN") criteriaStr=criteriaStr + " or pkg_util.f_to_number(nvl(" + tempArray[count] +",'') ) > TO_NUMBER('"+ fltdata +"')"
						else if (fldType=="CL"){
						    criteriaStr="dbms_lob.compare("+fldName+", '"+ fltdata +"') > 0"
						}
					    else criteriaStr=criteriaStr + " or lower("+tempArray[count]+") > lower('%"+ fltdata +"%')"
					}
    			}
    			criteriaStr=criteriaStr+")";
   			}else{
				if (fldType=="ED")
   				{
     	 			if (! isNaN(formId))
		 			{
					 	matchCreatedOnDate = fldName.match("created_on");
						matchLastModDate = fldName.match("last_modified_date");

						if (matchCreatedOnDate == null && matchLastModDate == null)
						{
					 		    criteriaStr="F_TO_DATE("+fldName+") > TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
					    }
					    else
					    {
					   		if (matchCreatedOnDate == "created_on" )
					   		{
					   			criteriaStr="F_TO_DATE(pkg_util.date_to_char(created_on)) > TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
					   		}
					   		else if (matchLastModDate == "last_modified_date")
					   		{
					   			criteriaStr="F_TO_DATE(pkg_util.date_to_char(last_modified_date)) > TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
					   		}
					   	}
					}
					else
					{
						criteriaStr= fldName+" > TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
					}
				}
			   	else if (fldType=="EN") criteriaStr="pkg_util.f_to_number(nvl(" + fldName +",'') ) > TO_NUMBER('"+ fltdata +"')"
			   	else if (fldType=="CL"){
				    criteriaStr="dbms_lob.compare("+fldName+", '"+ fltdata +"') > 0"
				}
			   	else criteriaStr="lower("+fldName+") > lower('%"+ fltdata +"%')"
			}
		}

		if (criteria=="islt"){
  			if (fldName.indexOf("|")>-1){
    			tempArray=fldName.split("|");
    			for(count=0;count<tempArray.length;count++){
    				if (criteriaStr.length==0){
    					if (fldType=="ED")
    					{
        					if (! isNaN(formId))
		 					{
								criteriaStr="(F_TO_DATE("+tempArray[count]+") < TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
							}
							else
							{
								criteriaStr="("+tempArray[count]+" < TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
							}
					    }
						else if (fldType=="EN") criteriaStr="(pkg_util.f_to_number(nvl(" + tempArray[count] +",'') ) < TO_NUMBER('"+ fltdata +"')"
						else if (fldType=="CL"){
		   					criteriaStr="dbms_lob.compare("+fldName+", '"+ fltdata +"') < 0"
		   				}
						else criteriaStr="(lower("+tempArray[count]+") < lower('%"+ fltdata +"%')"
					}else{
						if (fldType=="ED")
						{
							if (! isNaN(formId))
							{
							   	criteriaStr=criteriaStr+" or F_TO_DATE("+tempArray[count]+") < TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
							}
							else
							{
								criteriaStr=criteriaStr+" or "+tempArray[count]+" < TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
							}
						}
	    				else if (fldType=="EN") criteriaStr=criteriaStr+" or pkg_util.f_to_number(nvl(" + tempArray[count] +",'') ) < TO_NUMBER('"+ fltdata +"')"
	    				else if (fldType=="CL"){
		   					criteriaStr="dbms_lob.compare("+fldName+", '"+ fltdata +"') < 0"
		   				}
	    				else  criteriaStr=criteriaStr+" or lower("+tempArray[count]+") < lower('%"+ fltdata +"%')"
	    			}
	    		}
	    		criteriaStr=criteriaStr+")";
	   		}else{
	   			if (fldType=="ED")
	   			{
	     			if (! isNaN(formId))
			 		{

		   				matchCreatedOnDate = fldName.match("created_on");
						matchLastModDate = fldName.match("last_modified_date");

						if (matchCreatedOnDate == null && matchLastModDate == null)
						{
		   		  			criteriaStr="F_TO_DATE("+fldName+") < TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
		   				}
		   				else
		   				{
			   				if (matchCreatedOnDate == "created_on" )
			   				{
			   					criteriaStr="F_TO_DATE(pkg_util.date_to_char(created_on)) < TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
			   				}
			   				else if (matchLastModDate == "last_modified_date")
			   				{
			   					criteriaStr="F_TO_DATE(pkg_util.date_to_char(last_modified_date)) < TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
			   				}
			   			}
		   			}
		   			else
		   			{
		   				criteriaStr= fldName+" < TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
		   			}
	   			}
	   			else if (fldType=="EN") criteriaStr="pkg_util.f_to_number(nvl(" + fldName +",'') ) < TO_NUMBER('"+ fltdata +"')"
	   			else if (fldType=="CL"){
					criteriaStr="dbms_lob.compare("+fldName+", '"+ fltdata +"') < 0"
				}
	   			else criteriaStr="lower("+fldName+") < lower('%"+ fltdata +"%')"
	   		}
	  	}

		if (criteria=="isgte"){
			if (fldName.indexOf("|")>-1){
		    	tempArray=fldName.split("|");
		    	for(count=0;count<tempArray.length;count++){
		    		if (criteriaStr.length==0){
		    			if (fldType=="ED")
		    			{
		       				if (! isNaN(formId))
							{
								criteriaStr="(F_TO_DATE("+tempArray[count]+") >= TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
		    				}else {
		    					criteriaStr="( "+tempArray[count]+" >= TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
		    				}
		    			}
						else if (fldType=="EN") criteriaStr="(pkg_util.f_to_number(nvl(" + tempArray[count] +",'') ) >= TO_NUMBER('"+ fltdata +"')"
						else if (fldType=="CL"){
		   					criteriaStr="dbms_lob.compare("+fldName+", '"+ fltdata +"') >= 0"
		   				}else criteriaStr="(lower("+tempArray[count]+") >= lower('%"+ fltdata +"%')"
					}
					else{
						if (fldType=="ED")
						{
					       	if (! isNaN(formId))
							{
					    		criteriaStr=criteriaStr+ " or F_TO_DATE("+tempArray[count]+") >= TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
					    	}
					    	else
					    	{
					    		criteriaStr=criteriaStr+ " or "+tempArray[count]+" >= TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
					    	}

    					}
    					else if (fldType=="EN") criteriaStr=criteriaStr+ " or pkg_util.f_to_number(nvl(" + tempArray[count] +",'') ) >= TO_NUMBER('"+ fltdata +"')"
    					else if (fldType=="CL"){
    	   					criteriaStr="dbms_lob.compare("+fldName+", '"+ fltdata +"') >= 0"
    	   				}
    					else criteriaStr=criteriaStr+ " or lower("+tempArray[count]+") >= lower('%"+ fltdata +"%')"
					}
				}
				criteriaStr=criteriaStr+")";
			}else{
   				if (fldType=="ED")
   				{
   	  				if (! isNaN(formId))
					{
						matchCreatedOnDate = fldName.match("created_on");
						matchLastModDate = fldName.match("last_modified_date");

						if (matchCreatedOnDate == null && matchLastModDate == null)
						{
		   	 				criteriaStr="F_TO_DATE("+fldName+") >= TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
		   	 			}
	   	 				else
		   				{
		   					if (matchCreatedOnDate == "created_on" )
		   					{
		   						criteriaStr="F_TO_DATE(pkg_util.date_to_char(created_on)) >= TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
		   					}
		   					else if (matchLastModDate == "last_modified_date")
		   					{
		   						criteriaStr="F_TO_DATE(pkg_util.date_to_char(last_modified_date)) >= TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
		   					}
		   				}
	   				}
	   				else
	   				{
	   	 				criteriaStr=fldName+" >= TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
	   				}
   				}
   				else if (fldType=="EN") criteriaStr="pkg_util.f_to_number(nvl(" + fldName +",'') )>= TO_NUMBER('"+ fltdata +"')"
   				else if (fldType=="CL"){
   					criteriaStr="dbms_lob.compare("+fldName+", '"+ fltdata +"') >= 0"
   				}
   				else criteriaStr="lower("+fldName+") >= lower('%"+ fltdata +"%')"
   			}
  		}

		if (criteria=="islte"){
			if (fldName.indexOf("|")>-1){
		    	tempArray=fldName.split("|");
		    	for(count=0;count<tempArray.length;count++){
		    		if (criteriaStr.length==0){
		    			if (fldType=="ED")
		    			{
		    	  			if (! isNaN(formId))
				   			{
		    					criteriaStr="(F_TO_DATE("+tempArray[count]+") <= TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
		    	  			}
		    	  			else
		    	  			{
		    	  				criteriaStr="( "+tempArray[count]+" <= TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
		    	  			}
		    			}
						else if (fldType=="EN") criteriaStr="(pkg_util.f_to_number(nvl(" + tempArray[count] +",'') ) <= TO_NUMBER('"+ fltdata +"')"
						else if (fldType=="CL"){
		   					criteriaStr="dbms_lob.compare("+fldName+", '"+ fltdata +"') <= 0"
		   				}
		    			else criteriaStr="(lower("+tempArray[count]+") <= lower('%"+ fltdata +"%')"
					}else{
						if (fldType=="ED")
						{
   	  						if (! isNaN(formId))
							{
								criteriaStr=criteriaStr+ "or F_TO_DATE("+tempArray[count]+") <= TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
							}
							else
							{
								criteriaStr=criteriaStr+ "or "+tempArray[count]+" <= TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
							}
						}
						else if (fldType=="EN") criteriaStr=criteriaStr+ "or pkg_util.f_to_number(nvl(" + tempArray[count] +",'') ) <= TO_NUMBER('"+ fltdata +"')"
						else if (fldType=="CL"){
		   					criteriaStr="dbms_lob.compare("+fldName+", '"+ fltdata +"') <= 0"
		   				}
						else criteriaStr=criteriaStr+ "or lower("+tempArray[count]+") <= lower('%"+ fltdata +"%')"
					}
				}
    			criteriaStr=criteriaStr+")";
   			}else{
   				if (fldType=="ED")
				{
			    	if (! isNaN(formId))
					{
						matchCreatedOnDate = fldName.match("created_on");
						matchLastModDate = fldName.match("last_modified_date");

						if (matchCreatedOnDate == null && matchLastModDate == null)
						{
					  		criteriaStr="F_TO_DATE("+fldName+") <= TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
					  	}
					  	else
					   	{
					   		if (matchCreatedOnDate == "created_on" )
					   		{
					   			criteriaStr="F_TO_DATE(pkg_util.date_to_char(created_on)) <= TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
					   		}
					   		else if (matchLastModDate == "last_modified_date")
					   		{
					   			criteriaStr="F_TO_DATE(pkg_util.date_to_char(last_modified_date)) <= TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
					   		}
					   	}
				   	}
				   	else
				   	{
						criteriaStr= fldName+ " <= TO_DATE('"+ fltdata +"',PKG_DATEUTIL.F_GET_DATEFORMAT)"
				   	}
				}
   				else if (fldType=="EN") criteriaStr="pkg_util.f_to_number(nvl(" + fldName +",'')) <= TO_NUMBER('"+ fltdata +"')"
   				else if (fldType=="CL"){
   					criteriaStr="dbms_lob.compare("+fldName+", '"+ fltdata +"') <= 0"
   				}
   				else criteriaStr="lower("+fldName+") <= lower('%"+ fltdata +"%')"
   			}
  		}

	  	if (criteria=="first"){
	 		if (! isNaN(formId))
			{
	   			criteriaStr="filldate=(select min(filldate) from er_formslinear where fk_form="+formId+")";
	   		}
	   		else
	   		{
	   			criteriaStr="";
	   		}
		}
	  	if (criteria=="latest"){
	  		if (! isNaN(formId))
			{
	   			criteriaStr="filldate=(select max(filldate) from er_formslinear where fk_form="+formId+")";
	   		}
	   		else
	   		{
	   			criteriaStr="";
	   		}
		}

		if (criteria=="lowest")
  		{
  			if (! isNaN(formId))
			{
	   			if (fldName.indexOf("|")>-1)
	   			{
		    		tempArray=fldName.split("|");
		    		for(count=0;count<tempArray.length;count++){
		    			if (criteriaStr.length==0)
		    				criteriaStr="(lower("+tempArray[count]+") =(select min(lower("+ tempArray[count]+")) from er_formslinear where fk_form="+ formId+")"
		    			else
		    				criteriaStr=criteriaStr + " or lower("+tempArray[count]+") =(select min(lower("+ tempArray[count]+")) from er_formslinear where fk_form="+ formId+")"
		    		}
		    		criteriaStr=criteriaStr+")";
	   			}
	   			else
	   			{
	   				criteriaStr="lower("+fldName+") =(select min(lower("+ fldName+")) from er_formslinear where fk_form="+ formId+")"
	   			}
			}
	 		else
   	 		{
   				criteriaStr="";
   			}
  		} //

  		if (criteria=="highest"){
  			if (! isNaN(formId))
			{
  	   			if (fldName.indexOf("|")>-1)
  	   			{
	    			tempArray=fldName.split("|");
				    for(count=0;count<tempArray.length;count++){
				    	if (criteriaStr.length==0)
				    		criteriaStr="(lower("+tempArray[count]+") =(select max(lower("+ tempArray[count]+")) from er_formslinear where fk_form="+ formId+")"
				    	else
				    		criteriaStr=criteriaStr + " or lower("+tempArray[count]+") =(select max(lower("+ tempArray[count]+")) from er_formslinear where fk_form="+ formId+")"
				    }
				    criteriaStr=criteriaStr+")";
				}else{
					criteriaStr="lower("+fldName+") =(select max(lower("+ fldName+")) from er_formslinear where fk_form="+ formId+")"
				}
	 		}
			else
   			{
   				criteriaStr="";
   			}
		}
	  	//alert("filter"+filter);
	  	//alert("filterTemp" + filterTemp);

		//alert(arrayOfForms.push(formId));

  		defdateFilterWithAnd = defdateFilterWithAndMain;

  		if (criteriaStr.length>0)
  		{

  	  		//prepare patient study status date range filter
			if (dynType == 'P' && defdateStatusFilterWithAnd.length > 0)
			{
				defdateStatusFilterWithAndReplaced = defdateStatusFilterWithAnd;

				defdateStatusFilterWithAndReplaced = replaceSubstring(defdateStatusFilterWithAndReplaced,"[VELPAT]",mainTablePk);

				if (! isNaN(formId))
				{
					if 	(reportStudy.length == 0)
					{
						defdateStatusFilterWithAndReplaced = defdateStatusFilter;
						defdateStatusFilterWithAndReplaced = replaceSubstring(defdateStatusFilterWithAndReplaced,"[VELPAT]",mainTablePk);

						defdateStatusFilterWithAndReplaced	= replaceSubstring(defdateStatusFilterWithAndReplaced,"[VELSTUDY]","(SELECT fk_study FROM ER_PATPROT WHERE pk_patprot=fk_patprot)");
						defdateStatusFilterWithAndReplaced = " and ((fk_patprot is null) or " + defdateStatusFilterWithAndReplaced + " ) ";
					}
				}
				else
				{
					if (mainStudyCol.length > 0 && reportStudy.length == 0)
					{
						defdateStatusFilterWithAndReplaced	= replaceSubstring(defdateStatusFilterWithAndReplaced,"[VELSTUDY]",mainStudyCol);
					}
				}

				if (reportStudy.length > 0)
				{
					defdateStatusFilterWithAndReplaced	= replaceSubstring(defdateStatusFilterWithAndReplaced,"[VELSTUDY]",reportStudy);
				}

				if (isNaN(formId) && reportStudy.length == '0' && mainStudyCol.length =='0')
			 	{
			 		defdateStatusFilterWithAndReplaced = "";
			 	}
			}

			if (isNaN(formId) && mapIgnoreFilter.length > 0)
		 	{
		 		defdateStatusFilterWithAndReplaced = "";
		 		defdateFilterWithAnd = "";
		 	}

	  		if (! isNaN(formId))
			{
				filter = filter+filterTemp+ "("+addToEverySQL+" where "+ mainFilterColName + " = " + formId + patStudyFilter + " and " + criteriaStr + defdateFilterWithAnd + defdateStatusFilterWithAndReplaced +  respAccessRightWhereClause + ")" ;

				// in case of regular forms, the patient status filter is appended in preview
			}
			else
			{
				filter = filter+filterTemp+ "("+addToEverySQL+" where " +criteriaStr + defdateFilterWithAnd + defdateStatusFilterWithAndReplaced + accountCheckSQL ;

				if (mainStudyCol.length > 0 && reportStudy.length > 0)
				{
					// add filter for study
					filter = filter + " and  " + mainStudyCol + " = " + reportStudy;
				}

				filter = filter + ")";
			}

		  	if (formobj.endBracket[i].checked){
			    filter=filter+" )";
			    addEndBrac="Y";
			}
	  		criteriaStr = criteriaStr + defdateFilterWithAnd +  defdateStatusFilterWithAndReplaced ;
  		}

  		index=IndexOf(arrayOfForms,formId);
  		//alert("Row" + i +" "+ addStartBrac);

		if (index<0)
  		{
     		arrayOfForms.push(formId);

     		if (addStartBrac=="Y")
    		{
      			tempCriteria="("+criteriaStr;

      			arrayOfFilters.push(tempCriteria);
	     		if (IndexOf(endBracDeserver,formId)<0)
	     		{
	     	  		endBracDeserver.push(formId);
	     		}
    		}
    		else {
  				/*   for (var z=0;z<arrayOfFilters.length;z++)
        			alert("Array Of forms when addstartBracc NOT Y " + arrayOfFilters[z]);*/
      			arrayOfFilters.push(criteriaStr);
    		}
  		}
  		else
  		{
			if (addStartBrac=="Y")
    		{
				tempCriteria="("+criteriaStr;
      			//arrayOfFilters.push(tempCriteria);
      			if (IndexOf(endBracDeserver,formId)<0)
         			endBracDeserver.push(formId);
    			} else {
     				tempCriteria=criteriaStr;
    			}
		       arrayOfFilters[index]=arrayOfFilters[index]+ " " + extend+" "+tempCriteria;
  			}

			if (addEndBrac=="Y")
  			{
  				if (endBracDeserver.length>0)
  				{
					for (var j=0;j<endBracDeserver.length;j++)
				   	{
				    	index=IndexOf(arrayOfForms,endBracDeserver[j]);
				    	if (index>=0) arrayOfFilters[index]=arrayOfFilters[index]+")";
				     		addEndBrac="";
				     	addStartBrac="";
				   	}
				  	endBracDeserver.splice(0,endBracDeserver.length);
  				}
  			}
    	}//end for
	}//end else totcount

	if (startcount!=endcount){
		alert("<%=MC.M_BeginBrac_MismatchVerify%>");/*alert("Begin Bracket/End Bracket Mismatch. Please Verify");*****/
		return false;
	}

	//window.opener.document.forms["dynreport"].elements["repFilter"].value=filter;

	var choppedFilter="";
	var anyCoreFilterSelected = false; // to check if any core filter was selected

	for (var k=0;k<arrayOfFilters.length;k++)
	{
		//alert("in array of filters");

	  	if (isNaN(arrayOfForms[k]))
	  	{
	  		anyCoreFilterSelected = true;
	  	}

		if (choppedFilter.length==0)
	 		choppedFilter=arrayOfForms[k]+"[VELSEP]("+arrayOfFilters[k] + ")"   ;
		else
	 		choppedFilter=choppedFilter+"[VELFILTERSEP]"+arrayOfForms[k]+"[VELSEP]("+arrayOfFilters[k] + ")"  ;

		//alert("in array of filters choppedFilter" + choppedFilter);
	}
	//alert("Processed Forms" + arrayOfForms.toString());

	/* Run a loop for all the forms and if no filter was specified for this form
	 add a select Id from er_formsliner for that form*/
	 var anotherFilter="";
	 var anotherFilterAdded = false;

 	for (var z=0;z<allForms.length;z++)
 	{
   		findIndex=IndexOf(arrayOfForms,allForms[z]);
	   	if (findIndex<0)
	   	{
	   	   formIndexForPKData = getFormPKDataIndex(formobj,allForms[z]);

	   	   	if (formIndexForPKData != -2 ) //-2 if no data found
			{
			 	if (formIndexForPKData  == -1) //there is only one record so no array
			 	{
		 	  	 	mainTableName = formobj.mapPKTableName.value;
		 			mainTablePk  = formobj.mapTablePK.value;
		 			mainFilterColName = formobj.mapTableFilterCol.value;
		 			mapIgnoreFilter = formobj.mapIgnoreFilter.value;
		 			mainStudyCol = formobj.mapMapStudyColumn.value;
			 	}
				else
			 	{
			 		mainTableName = formobj.mapPKTableName[formIndexForPKData].value;
					mainTablePk  = formobj.mapTablePK[formIndexForPKData].value;
			 		mainFilterColName = formobj.mapTableFilterCol[formIndexForPKData].value;
			 		mapIgnoreFilter = formobj.mapIgnoreFilter[formIndexForPKData].value;
				 	mainStudyCol = formobj.mapMapStudyColumn[formIndexForPKData].value;
			 	}

			 	addToEverySQL =  "select distinct " + mainTablePk + " as ID from " + mainTableName ;
			 	if (dynType == 'A')
            	{
            		accountCheckSQL = " and " + mainTablePk + " = [:VELACCOUNT]";
			    }
			    else
			    {
			    	accountCheckSQL = "";
			    }

			 	//prepare patient status date range string
		 		defdateFilterWithAnd = defdateFilterWithAndMain;

		 		if (dynType == 'S') // for study adHoc, the PK column is fk/pk study
				{
					mainStudyCol = mainTablePk;
				}


		 		if (dynType == 'P' && defdateStatusFilterWithAnd.length > 0)
		 		{
					defdateStatusFilterWithAndReplaced = defdateStatusFilterWithAnd;

					defdateStatusFilterWithAndReplaced = replaceSubstring(defdateStatusFilterWithAndReplaced,"[VELPAT]",mainTablePk);

					if (! isNaN( allForms[z] ))
					{
						if 	(reportStudy.length == 0)
						{
							defdateStatusFilterWithAndReplaced = defdateStatusFilter;
							defdateStatusFilterWithAndReplaced = replaceSubstring(defdateStatusFilterWithAndReplaced,"[VELPAT]",mainTablePk);

							defdateStatusFilterWithAndReplaced	= replaceSubstring(defdateStatusFilterWithAndReplaced,"[VELSTUDY]","(SELECT fk_study FROM ER_PATPROT WHERE pk_patprot=fk_patprot)");
							defdateStatusFilterWithAndReplaced = " and ((fk_patprot is null) or " + defdateStatusFilterWithAndReplaced + " ) ";
						}
					}
					else
					{
						if (mainStudyCol.length > 0 && reportStudy.length == 0)
						{
							defdateStatusFilterWithAndReplaced	= replaceSubstring(defdateStatusFilterWithAndReplaced,"[VELSTUDY]",mainStudyCol);
						}
					}

					if (reportStudy.length > 0)
					{
						defdateStatusFilterWithAndReplaced	= replaceSubstring(defdateStatusFilterWithAndReplaced,"[VELSTUDY]",reportStudy);
					}

					if (isNaN(allForms[z]) && reportStudy.length == '0' && mainStudyCol.length =='0')
					{
						defdateStatusFilterWithAndReplaced = "";
					}
				}
				if (isNaN(allForms[z]) && mapIgnoreFilter.length > 0)
			 	{
	 				defdateStatusFilterWithAndReplaced = "";
	 				defdateFilterWithAnd = "";
	 			}

		     	if (anotherFilter.length==0)
			    {
			    	if (!isNaN(allForms[z]))
			     	{
			        	anotherFilter="( ( " + addToEverySQL + " where fk_form = "+allForms[z]+ patStudyFilter + defdateFilterWithAnd + defdateStatusFilterWithAndReplaced +  respAccessRightWhereClause + " )";
			         	anotherFilterAdded = true; // to append a corresponding closing bracket to main filter
			         	//added an extra bracket for first filter

		         		//modified by Sonia Abrol, we dont need to intersect with core filter. If a core filter is applied, it will work only on that 'core'
		         		anotherJoinOp = " union ";
			        }
			        else
			        {
			        	anotherFilter=" ( " + addToEverySQL + " where " + mainTablePk +" > 0 " + accountCheckSQL + defdateFilterWithAnd + defdateStatusFilterWithAndReplaced ;

						if (mainStudyCol.length > 0 && reportStudy.length > 0)
						{
							// add filter for study
							anotherFilter = anotherFilter + " and  " + mainStudyCol + " = " + reportStudy;
						}

			         	anotherFilter = anotherFilter + " )";
			         	anotherJoinOp = " union ";
			        }

			     	if (filter.length > 0)
			     	{
			     	 	 anotherFilter = anotherJoinOp + anotherFilter;
			     	 }
			     }
			     else
			     {
			     	 if (!isNaN(allForms[z]))
			     	 {
			     	 	anotherFilter=anotherFilter+" union (" + addToEverySQL + " where fk_form="+allForms[z]+ patStudyFilter + defdateFilterWithAnd + defdateStatusFilterWithAndReplaced +  respAccessRightWhereClause +")";
			     	 }
			     	 else
			     	 {
			     	 	anotherFilter=anotherFilter+" union(" + addToEverySQL + " where " + mainTablePk +" > 0 " + accountCheckSQL  + defdateFilterWithAnd + defdateStatusFilterWithAndReplaced ;

			     	 	if (mainStudyCol.length > 0 && reportStudy.length > 0)
						{
							// add filter for study
							anotherFilter = anotherFilter + " and  " + mainStudyCol + " = " + reportStudy;
						}
			    	 	anotherFilter = anotherFilter + " )";
			     	}
				}
		  	} // if form pk info is found; i.e there were any fields selected for the form
		}//no filter assigned to form
 	}

	if (anotherFilter.length>0)
	{
 		if (filter.length>0)
  			filter=filter + anotherFilter;
  		else
  			filter=anotherFilter;
	}

	if (anotherFilterAdded)
	{
		filter = filter + " ) " ;
	}
	if (filter.length>0) filter="(" + filter+")" ;
	//alert("filter" + filter);
	//alert("choppedFilter" + choppedFilter);
	formobj.filter.value=filter;

	formobj.choppedFilter.value=choppedFilter;

	if (formobj.mode.value == 'newfilter' || formobj.mode.value == 'modifyPrevFilter' || formobj.mode.value == 'addrow')
	{
		formobj.action="mdynfilter.jsp";
	}
	else
	{
		formobj.mode.value="next";
		formobj.action="mdynorder.jsp";
	}
	//window.close();
}

function setInclude(formobj,count){
totcount=formobj.totcount.value;
if (totcount==1){

formobj.include.checked=true;
} else
{
formobj.include[count].checked=true;
}


}

function getFormPKDataIndex(formobj,formid)
{
	var formCount = 0;
	var bFound = false;

	if (formobj.mapTablePKFormID.length == undefined) //its a singlw row and not an array
	{
		if (formobj.mapTablePKFormID.value == formid)
			{
				return -1;
			}
			else
			{
				return -2;
			}

	}
	else
	{
		formCount = formobj.mapTablePKFormID.length;

		for (ctr=0;ctr<formCount;ctr++)
		 {
				if (formobj.mapTablePKFormID[ctr].value == formid)
				{
					bFound = true;
					break;
				}

		 }

		if (bFound == false)
		{
			ctr = -2;
		}

		return ctr;
	}
}

</script>

</head>

<% String src,sql="",tempStr="",sqlStr="",repMode="";
   int strlen=0,firstpos=-1,secondpos=-1;
src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true">

<jsp:param name="src" value="<%=src%>"/>
<jsp:param name="sess" value="keep"/>

</jsp:include>



<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1)
   	ienet = 0; //IE
else
	ienet = 1;

if (ienet == 0) {
%>
<body style="overflow:scroll">
<%} else {%>
<body>
<%}%>

<br>

 <DIV id="div1">
<%



	String selectedTab = request.getParameter("selectedTab");
	String value="",patientID="",studyNumber="",orderStr="",order="",flgMatch="",fltrName="",fltrDD="",prevId="",fldType="",dynType="",selFltr="";
	String[] scriteria=null,fldData=null,fldSelect= null,fldSelectArray=null;
	int personPk=0,studyPk=0,index=-1,arraySize=0,row=0,colLen=0;
	DynRepDao dynDao=new DynRepDao();
	ArrayList selectedfld=null;
	ArrayList flltDtIds=new ArrayList();
	ArrayList dataList=new ArrayList() ;
	ArrayList prevSessColId=new ArrayList();
	ArrayList tempList=new ArrayList();
	ArrayList startBracList=new ArrayList();
	ArrayList endBracList=new ArrayList();
	ArrayList scriteriaList=new ArrayList();
	ArrayList fldDataList=new ArrayList();
	ArrayList sessCol=new ArrayList();
	ArrayList sessColName=new ArrayList();
	ArrayList sessColDisp=new ArrayList();
	ArrayList sessColSeq=new ArrayList();
	ArrayList sessColType=new ArrayList();
	ArrayList sessColStr=new ArrayList();
	ArrayList sessColWidth=new ArrayList();
	ArrayList sessColFormat=new ArrayList();
	ArrayList sessColId=new ArrayList();

	String[] fldName=null;
	String[] fldCol=null;
	String[] fldNameSelect=null;
	String[] extend=null;
	ArrayList fldTypeList=new ArrayList();
	ArrayList fldNameSelList=new ArrayList();
	ArrayList extendList=new ArrayList();
	ArrayList excludeList=new ArrayList();
	ArrayList multiFltrIds=new ArrayList();
	ArrayList multiFltrNames=new ArrayList();

	String defDateFilterType = "";
	String defDateFilterDateFrom = "";
	String defDateFilterDateTo = "";

	String prevSelForm = "";

	Hashtable htFormPKInfo = new Hashtable();
	int fldSelectArrayCount = 0;

	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))
	{
	ReportHolder container=(ReportHolder)tSession.getAttribute("reportHolder");
		if (container==null){%>
	<META HTTP-EQUIV=Refresh CONTENT="0; URL=dynrepbrowse.jsp?srcmenu=<%=src%>">

	<%}else {

	  int pageRight = 0;
  	  GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");
      pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("ADHOC"));

	String mode = request.getParameter("mode");
	mode=(mode==null)?"":mode;

	String formIdStr=request.getParameter("formIdStr");

	String allFormIdStr = "";

	String acc = (String) tSession.getValue("accountId");
	int accId = EJBUtil.stringToNum(acc);

	String reportStudy = "";

	reportStudy = container.getStudyId();

	if (StringUtil.isEmpty(reportStudy))
	{
		reportStudy = "";
	}

	String ignoreFilters = request.getParameter("ignoreFilters");

	if (formIdStr==null) formIdStr="";
	if (formIdStr.length()==0){
	formIdStr=container.getFormIdStr();
	}
	if (formIdStr==null) formIdStr="";

	repMode=container.getReportMode();
	if (repMode==null) repMode="";
	dynType=container.getReportType();
	if (dynType==null) dynType="";
	selFltr=request.getParameter("selFltr");

	selFltr=(selFltr==null)?"":selFltr;
	FilterContainer filterContainer=null;
	String selValue = "";;


	multiFltrIds=container.getFilterIds();
	multiFltrNames=container.getFilterNames();

	if (EJBUtil.isEmpty(selFltr))
	{

		if (multiFltrIds!=null){
			if (multiFltrIds.size()>= 1)
			{
			 	selValue= (String)multiFltrIds.get(0);
			 	selFltr = selValue;
				filterContainer = container.getFilterObject(selFltr);
				filterContainer.Display();
			}
		}

	//////////
		/*Collection fltrSet = (container.getFilterObjects()).values();
		Iterator itrFltr = fltrSet.iterator();

		if (itrFltr.hasNext()) {
			filterContainer = (FilterContainer) itrFltr.next();

			selValue = filterContainer.getFilterId();
			filterContainer.Display();
			selFltr = filterContainer.getFilterId();

		} */
	}
	else
	{
		selValue=selFltr;
		filterContainer = container.getFilterObject(selFltr);

	}






	fltrName=request.getParameter("fltrName");
	fltrName=(fltrName==null)?"":fltrName;

	if (fltrName.length()==0)
	{
	 if (filterContainer!=null) fltrName=filterContainer.getFilterName();
	 fltrName=(fltrName==null)?"":fltrName;
	}

	defDateFilterType = request.getParameter("dateRangeType");
	if (StringUtil.isEmpty(defDateFilterType))
		defDateFilterType = "";

	defDateFilterDateFrom  = request.getParameter("dtFilterDateFrom");

	if (StringUtil.isEmpty(defDateFilterDateFrom))
		defDateFilterDateFrom = "";

	defDateFilterDateTo  = request.getParameter("dtFilterDateTo");

	if (StringUtil.isEmpty(defDateFilterDateTo))
		defDateFilterDateTo = "";

	//to add new rows to filter
	if (mode.equals("addrow") || (mode.equals("chgfltr")) || (mode.equals("newfilter")) || (mode.equals("modifyPrevFilter")) ){
		fldName=request.getParameterValues("fldLocal");
		fldCol=request.getParameterValues("fldLocalCol");
		for(int len=0;len<fldName.length;len++){
		System.out.println(" count " + len + " with fldname " + fldName[len] + " and  fldCol " + fldCol[len] );
		}
	} // end of (mode.equals("addrow") || (mode.equals("chgfltr")) )
	else // to save the selected fields
	{
		fldSelectArray=(String[])request.getParameterValues("fldSelect");
		/*Add the fields of selected form to reportHolder*/
		if (fldSelectArray==null)
		{
			fldSelectArray=new String[0];
			fldSelectArrayCount = 0;
		}
		else
		{
			fldSelectArrayCount = fldSelectArray.length;
		}

		prevSelForm = request.getParameter("prevSelForm");

		if (! StringUtil.isEmpty(prevSelForm))
		{
			// if fldSelectArray.length > 0, and the prevSelForm is of corelookup type
			if (prevSelForm.startsWith("C") && (fldSelectArrayCount > 0))
			{
				container.setSelectedCoreLookupformIds(prevSelForm);
			}

			if (prevSelForm.startsWith("C") && (fldSelectArrayCount <= 0))
			{
				container.removeSelectedCoreLookupformId(prevSelForm);
			}

		}


		 if (! StringUtil.isEmpty(ignoreFilters))
			{
			container.setIgnoreFilters(prevSelForm,ignoreFilters);
		 }


		FieldContainer fldContainer=container.getFldObject(request.getParameter("selForm"));
		if ((fldContainer==null) && fldSelectArray.length>0)
		    fldContainer=container.newFldObject(prevSelForm);
		  if (fldContainer!=null)
		  {
		  fldContainer.reset();
		 for(int z=0;z<fldSelectArray.length;z++)
			{
			 String[] tempArray=StringUtil.strSplit(fldSelectArray[z],"[$]",false);
			 fldContainer.setFieldStr(fldSelectArray[z]);

			 System.out.println("tempArray[0]" + tempArray[0]);

			 fldContainer.setFieldSequence(tempArray[0]);
			 fldContainer.setFieldName(tempArray[1]);
			 fldContainer.setFieldDisplayName(tempArray[2]);
			 fldContainer.setFieldWidth(tempArray[3]);
			 fldContainer.setFieldColId(tempArray[4]);
			 fldContainer.setFieldType(tempArray[5]);
			 fldContainer.setFieldFormat(tempArray[6]);
			 fldContainer.setFieldDbId(tempArray[7]);
			 fldContainer.setUseUniqueIds(tempArray[8]);
			 fldContainer.setUseDataValues(tempArray[9]);
			  fldContainer.setCalcSums(tempArray[10]);
			 fldContainer.setCalcPers(tempArray[11]);
			  fldContainer.setFieldRepeatNumber(tempArray[12]);

			  if(tempArray.length>=14)
			 {
			   fldContainer.setFieldKeyword(tempArray[13]);
			   }

			}//end for Z loop
		prevSessColId=fldContainer.getFieldPrevColId();
		//Remove the unselected fields
		sessColId=fldContainer.getFieldDbId();
		if (prevSessColId!=null) colLen=prevSessColId.size();
		System.out.println("colLen"+colLen);
		if (colLen>0)
		{
			for (int k=0;k<colLen;k++){
				System.out.println("If "+sessColId+ " contains "+prevSessColId.get(k));
				if (sessColId.contains(prevSessColId.get(k)))
				{
				}else
				{
					System.out.println("Add minus to"+prevSessColId.get(k));
					if (!(sessColId.contains("-"+(String)prevSessColId.get(k))))
					fldContainer.setFieldDbId("-"+(String)prevSessColId.get(k));
				}
			}//end for
		}

		if (request.getParameterValues("fldCol")!=null )
		 fldContainer.setFieldList(EJBUtil.strArrToArrayList(request.getParameterValues("fldCol")));
		if (request.getParameterValues("fldId")!=null)
		 fldContainer.setFieldPkList(EJBUtil.strArrToArrayList(request.getParameterValues("fldId")));
		 if ((request.getParameterValues("fldName"))!=null)
		 fldContainer.setFieldNameList(EJBUtil.strArrToArrayList(request.getParameterValues("fldName")));
		 if ((request.getParameterValues("fldType"))!=null)
		 fldContainer.setFieldTypeList(EJBUtil.strArrToArrayList(request.getParameterValues("fldType")));

		 if ((request.getParameterValues("fldType"))!=null)
		 fldContainer.setFieldTypesNoRepeatFields(EJBUtil.strArrToArrayList(request.getParameterValues("fldType")));

		  if ((request.getParameterValues("fldKey"))!=null)
	  fldContainer.setFieldKeyword(EJBUtil.strArrToArrayList(request.getParameterValues("fldKey")));



		 fldContainer.setPkTableName(request.getParameter("mapPKTableName"));
		 fldContainer.setMainFilterColName(request.getParameter("mapTableFilterCol"));
		 fldContainer.setPkColName(request.getParameter("mapTablePK"));

		 fldContainer.setMapStudyColumn(request.getParameter("mapStudyColumn"));

		 // set fld sequence
		 fldContainer.setFieldSequence(request.getParameter("fldSeq"));

		 if (! StringUtil.isEmpty(request.getParameter("mapStudyColumn")) )
		 {
		 	container.setCheckForStudyRights(true);
		 }

		 container.Display();

		}


		//fldName=request.getParameterValues("fldName");
		//System.out.println("VISHAL");
		container.Display();

		//fldSelect=request.getParameterValues("fldSelect");

		//call compileFormfields 	no matter what
		container.compileFormFields();
	}//end of to save the selected fields



	fldCol=(String[])(container.getMasterFieldId()).toArray(new String[container.getMasterFieldId().size()]);
	fldName=(String[])(container.getMasterFieldName()).toArray(new String[container.getMasterFieldName().size()]);
	fldTypeList=container.getMasterFieldType();

	allFormIdStr = container.getAllFormIdStr();
	htFormPKInfo = container.getHtFormTablePKInfo();

	//save previous filter and present blank filter rows to add new filter
	// when called for modifying another filter, show that filter's rows and save previous filter
	if (mode.equals("newfilter") || mode.equals("modifyPrevFilter") || mode.equals("addrow"))
	{
		ArrayList fldNameSelListNew=new ArrayList();
		ArrayList fltrDtIdListNew=new ArrayList();
		ArrayList excludeListNew=new ArrayList();
		ArrayList startBracListNew=new ArrayList();
		ArrayList endBracListNew=new ArrayList();
		ArrayList scriteriaListNew=new ArrayList();
		ArrayList fldDataListNew=new ArrayList();
		ArrayList extendListNew=new ArrayList();

		String[] fldNameSelectNew=null;
		String[] scriteriaNew=null,fldDataNew=null,extendNew = null;
		String[] fltrDtIdNew= request.getParameterValues("fltrDtId");
		String[] excludeNew = request.getParameterValues("exclude");
		String[] startBracketNew=request.getParameterValues("startBracket");
		String[] endBracketNew=request.getParameterValues("endBracket");
		String	fltrNameNew = request.getParameter("fltrName");

		String fltrIdNew = request.getParameter("prevFilter");

		String filterNew=request.getParameter("filter");
		String choppedFilterNew = request.getParameter("choppedFilter");


		if (StringUtil.isEmpty(choppedFilterNew)){
			choppedFilterNew= "";	}

		if (StringUtil.isEmpty(filterNew))	{
		 filterNew = "";	}

		if (StringUtil.isEmpty(fltrIdNew))	{
			fltrIdNew = "";
		}

		if (StringUtil.isEmpty(fltrNameNew)){
			fltrNameNew = "";	}

		fldNameSelectNew=request.getParameterValues("fldName");

		if (fldNameSelectNew!=null)
					fldNameSelListNew=EJBUtil.strArrToArrayList(fldNameSelectNew);

				if (fltrDtIdNew!=null)
				fltrDtIdListNew=EJBUtil.strArrToArrayList(fltrDtIdNew);

				if (excludeNew!=null)
					excludeListNew=EJBUtil.strArrToArrayList(excludeNew);

				if (startBracketNew!=null)
					startBracListNew=EJBUtil.strArrToArrayList(startBracketNew);

				if (endBracketNew!=null)
					endBracListNew=EJBUtil.strArrToArrayList(endBracketNew);

				//Preprocess excludeList,startBracList,endBracList to add -1 for chgeckboxes not selected,"" is stored for rows for unchecled chkbox
				if (fldNameSelListNew!=null)
				{
				for(int z=0;z<fldNameSelListNew.size();z++)
				{
					if (excludeListNew.indexOf(EJBUtil.integerToString(new Integer(z)))>=0){}
					else{
						excludeListNew.add(z,"");
					}

					 if (startBracListNew.indexOf(EJBUtil.integerToString(new Integer(z)))>=0){}
					 else{
						startBracListNew.add(z,"");
						}
					if (endBracListNew.indexOf(EJBUtil.integerToString(new Integer(z)))>=0){}
						else{
						endBracListNew.add(z,"");
						}


					}//end for
				}//end if



				scriteriaNew=request.getParameterValues("scriteria");
				if (scriteriaNew!=null)
				scriteriaListNew=EJBUtil.strArrToArrayList(scriteriaNew);

				fldDataNew=request.getParameterValues("fltData");
				if (fldDataNew!=null)
				fldDataListNew=EJBUtil.strArrToArrayList(fldDataNew);

				extendNew=request.getParameterValues("extend");
				if (extendNew!=null)
				extendListNew=EJBUtil.strArrToArrayList(extendNew);




		FilterContainer fltrContainer=container.getFilterObject((fltrIdNew));


		if ((fltrContainer==null) && (fldNameSelListNew.size()>0) && (! StringUtil.isEmpty(fltrNameNew)) )
		{

			if (EJBUtil.isEmpty(fltrIdNew) || fltrIdNew.equals("0") )
			{
				fltrContainer=container.newFilterObject(fltrNameNew);

				if (mode.equals("addrow"))
				{
					selFltr = fltrNameNew; // the selected filter is the new filter
					selValue = 	fltrNameNew; // the selected filter is the new filter
					fltrName = fltrNameNew;
				}
				fltrContainer.reset();
				fltrContainer.setFilterMode("N");

				if (! StringUtil.isEmpty(fltrNameNew))
				{
					container.setFilterIds(fltrNameNew);
					container.setFilterNames(fltrNameNew);
				}
			}
			else
			{

				fltrContainer=container.newFilterObject(fltrIdNew);
				fltrContainer.reset();
				fltrContainer.setFilterMode("M");
			}
		}
		else if ((fltrContainer != null) && (fldNameSelListNew.size()>0) && (! StringUtil.isEmpty(fltrNameNew)) )
			{
				fltrContainer.reset();
				fltrContainer.setFilterMode("M");

			}



		if (fltrContainer != null)
		{

				fltrContainer.setFilterDateRangeType(defDateFilterType);
				fltrContainer.setFilterDateRangeTo(defDateFilterDateTo);
				fltrContainer.setFilterDateRangeFrom(defDateFilterDateFrom);

				if (fltrDtIdListNew!=null)
				{
				if (fltrDtIdListNew.size()>0) fltrContainer.setFilterDbId(fltrDtIdListNew);
				}
				if (excludeListNew!=null) {
				if (excludeListNew.size()>0) fltrContainer.setFilterExclude(excludeListNew);
				}
				if (startBracListNew!=null){
				if (startBracListNew.size()>0) fltrContainer.setFilterBbrac(startBracListNew);
				}
				if (fltrNameNew.length()>0) fltrContainer.setFilterName(fltrNameNew);
				if ((fltrIdNew.length()>0)) fltrContainer.setFilterId(fltrIdNew);

				if (endBracListNew!=null){
				if (endBracListNew.size()>0) fltrContainer.setFilterEbrac(endBracListNew);
				}
				if (fldNameSelListNew!=null){
				if (fldNameSelListNew.size()>0)	fltrContainer.setFilterCols(fldNameSelListNew);
				}
				if (scriteriaListNew!=null) {
				if (scriteriaListNew.size()>0) fltrContainer.setFilterQualifier(scriteriaListNew);
				}
				if (fldDataListNew!=null){
				if (fldDataListNew.size()>0)  fltrContainer.setFilterData(fldDataListNew);
				}
				if (extendListNew!=null) {
				if (extendListNew.size()>0) fltrContainer.setFilterOper(extendListNew);
				}


				fltrContainer.setFilterStr(filterNew);
				System.out.println("choppedFilterNew" + choppedFilterNew);
				String[] tempArray=StringUtil.strSplit(choppedFilterNew,"[VELFILTERSEP]",false);
				String [] processArray;
				for (int i=0;i<tempArray.length;i++)
				{
					processArray=StringUtil.strSplit(tempArray[i],"[VELSEP]",false);
				    fltrContainer.setFormIds(processArray[0]);
				    fltrContainer.setChoppedFilters(processArray[1]);

				}
		} // for if (fltrContainer != null)

		container.Display();



		if (mode.equals("modifyPrevFilter")  )
		{
			selFltr=request.getParameter("selFltr");
			if (StringUtil.isEmpty(selFltr))
			 selFltr="";
			selValue = selFltr;
		}
		else if (mode.equals("addrow") )
		{

		}
		else
		{
			//reset for new filter
			selFltr = "";
			selValue = "";
		}

		if (!mode.equals("addrow"))
		{
		fltrName = "";
		filterContainer = null;
		defDateFilterType = "";
		defDateFilterDateFrom = "";
		defDateFilterDateTo = "";
		}

	} //for if (mode.equals("newfilter") || mode.equals("modifyPrevFilter"))


	multiFltrIds=container.getFilterIds();
	multiFltrNames=container.getFilterNames();

	if (multiFltrIds!=null){
	if (multiFltrIds.size()>= 1)
	{
		if (StringUtil.isEmpty(selValue) && !(mode.equals("newfilter")) )
		{
		 	selValue= (String)multiFltrIds.get(0);
		 	selFltr = selValue;
		 	filterContainer = null;
		 	if (multiFltrNames.size()>=1) fltrName=(String)multiFltrNames.get(0);

		}
		fltrDD=EJBUtil.createPullDownWithStr("selFltr",selValue,multiFltrIds,multiFltrNames);
	}

	fltrName=(fltrName==null)?"":fltrName;
	}


	tSession.setAttribute("reportHolder",container);
	// Set the selected fields session object


	container.Display();

	//end colLen




	//end here
	String addrow=request.getParameter("addrow");
	if (addrow==null) addrow="";
	else row=EJBUtil.stringToNum(addrow);

	if (mode.equals("addrow")){
	String[] startBracket=request.getParameterValues("startBracket");
	String[] endBracket=request.getParameterValues("endBracket");
	String[] exclude = request.getParameterValues("exclude");
	String[] fltDt=request.getParameterValues("fltrDtId");
	String[] fldTypeArray=request.getParameterValues("fldTypeOrig");
	if (fltDt!=null)
	flltDtIds=EJBUtil.strArrToArrayList(fltDt);
	if (startBracket!=null)
	startBracList=EJBUtil.strArrToArrayList(startBracket);
	if (endBracket!=null)
	endBracList=EJBUtil.strArrToArrayList(endBracket);
	fldNameSelect=request.getParameterValues("fldName");
	if (fldNameSelect!=null)
	fldNameSelList=EJBUtil.strArrToArrayList(fldNameSelect);
	scriteria=request.getParameterValues("scriteria");
	if (scriteria!=null)
	scriteriaList=EJBUtil.strArrToArrayList(scriteria);
	fldData=request.getParameterValues("fltData");
	if (fldData!=null)
	fldDataList=EJBUtil.strArrToArrayList(fldData);
	extend=request.getParameterValues("extend");
	if (extend!=null)
	extendList=EJBUtil.strArrToArrayList(extend);
	System.out.println(fldNameSelect);
	if (exclude!=null)
	excludeList=EJBUtil.strArrToArrayList(exclude);
	if (fldTypeArray!=null)
	fldTypeList=EJBUtil.strArrToArrayList(fldTypeArray);
	}



	if (fldNameSelList.size()==0)
	{

	 if (filterContainer==null)
	  filterContainer=container.getFilterObject(selFltr);


	  if (filterContainer!=null)
	  {
		flltDtIds=filterContainer.getFilterDbId();
		startBracList=filterContainer.getFilterBbrac();
		endBracList=filterContainer.getFilterEbrac();
		fldNameSelList=filterContainer.getFilterCols();
		scriteriaList=filterContainer.getFilterQualifier();
		fldDataList=filterContainer.getFilterData();
		extendList=filterContainer.getFilterOper();
		excludeList=filterContainer.getFilterExclude();
		fltrName = filterContainer.getFilterName();

		defDateFilterType = filterContainer.getFilterDateRangeType();
		defDateFilterDateTo = filterContainer.getFilterDateRangeTo();
		defDateFilterDateFrom = filterContainer.getFilterDateRangeFrom();

	}
	}


	int counter=0;
	String name="";
	StringBuffer fldDD=new StringBuffer();

	int totCount= 0 ; //EJBUtil.stringToNum(request.getParameter("totcount"));

	if (totCount==0) totCount=10;
	totCount=totCount+row;

	if (mode.equals("newfilter"))
		totCount = 10;

	 //Retrieve the field types
	 //if (fieldContainer!=null) fldTypeList=fieldContainer.getField("sessColType");

	 //end retrieve field types
	 //Retrieve columns names from database,problem appears in modify mode,if there was not filter selected and user comes straight
	 //by clicking on top link instead from previosu page.
	 /*if (repMode.equals("M")){
	 DynRepDao dyndao=dynrepB.getMapData(formId);
	 dyndao.preProcessMap();
	if ((fldName==null) || (fldName.length==0)){
	 fldCol= ((String[])(dyndao.getMapCols()).toArray(new String [ (dyndao.getMapCols()).size() ]));
	fldName=((String[])(dyndao.getMapDispNames()).toArray(new String [ (dyndao.getMapDispNames()).size() ]));
	fldTypeList=dyndao.getMapColTypes();
	}
	}*/
	//end for retrieve column name from database.


%>

<form name="dynadvanced" method="post">

<div class="tabDefTopN" id="divTab">


<table align="center" border="0" cellpadding="0" cellspacing="0" width="98%">

<tr bgcolor="#dcdcdc" align="center">
<%if (container.getFormIdStr()!=null){%>
<td><A href="mdynreptype.jsp?mode=link&srcmenu=<%=src%>&dynType=<%=dynType%>"><%=LC.L_Select_RptType%><%--Select Report Type*****--%></A></td>
<%}else{%>
<td><%=LC.L_Select_RptType%><%--Select Report Type*****--%></td>
<%}%>
<td>>></td>
<%if ((container.getFldObjects()).size()>0){%>
<td><A href="mdynselectflds.jsp?mode=link&srcmenu=<%=src%>"><%=LC.L_Select_Flds%><%--Select Fields*****--%></A></td>

<%}else{%>
<td><%=LC.L_Select_Flds%><%--Select Fields*****--%></td>
<%}%>
<td>>></td>
<%if ((container.getFilterObjects()).size()>0){%>
<td><A href="mdynfilter.jsp?mode=link&srcmenu=<%=src%>"><font color="red"><%=LC.L_Filter_Criteria%><%--Filter Criteria*****--%></font></A></td>

<%}else{%>
<td><font color="red"><%=LC.L_Filter_Criteria%><%--Filter Criteria*****--%></font></td>
<%}%>
<td>>></td>
<%if ((container.getSortObjects()).size()>0){%>
<td><A href="mdynorder.jsp?mode=link&srcmenu=<%=src%>"><%=LC.L_Sort_Criteria%><%--Sort Criteria*****--%></A></td>
<%}else{%>
<td><%=LC.L_Sort_Criteria%><%--Sort Criteria*****--%></td>
<%}%>
<td>>></td>
<%if (container.getReportName().length()>0){%>
<td><A href="mdynrep.jsp?mode=link&srcmenu=<%=src%>"><%=LC.L_Preview_Save%><%--Preview & Save*****--%></A></td>
<%}else{%>
<td><%=LC.L_Preview_Save%><%--Preview & Save*****--%></td>
<%}%>

<td width="25%" align="right">
<input name="direction" type="hidden" value="forward">
<button type="submit" onClick ="return nextPage(document.dynadvanced);"><%=LC.L_Next%></button>
</td>
</tr>

</table>
</div>

<DIV class="tabDefBotN" id="div1">
<input type="hidden" name="formId" value="<%=formIdStr%>")>
<input type="hidden" name="allFormIdStr" value="<%=allFormIdStr%>")>

<input type="hidden" name="filter" value="")>
<input type="hidden" name="choppedFilter" value="")>



<table border = 0 width="100%" >
<tr valign="middle">

<td width="28%">
<% if (multiFltrIds!=null){
  if (multiFltrIds.size()>=1){%>
<b><%=LC.L_Select_Filter%><%--Select Filter*****--%>:</b><%=fltrDD%>&nbsp;
<button type="submit" onClick = "return saveFilter(document.dynadvanced,'modifyPrevFilter');"><%=LC.L_Go%></button>

<%}}else{%>
&nbsp;
<Input Type="hidden" name="selFltr" value="<%=selValue%>">
<%}%>
</td>

<td width="14%">
	<% if (pageRight > 4) { %>
		<button type="submit" onClick ="return saveFilter(document.dynadvanced,'newfilter');"><%=LC.L_New%></button>
<% } %>	&nbsp;
</td>

<%if ( StringUtil.isEmpty(fltrName) ){%>
<td width="30%" align="center"><b><%=LC.L_Specify_FilterName%><%--Specify Filter Name*****--%> </b>&nbsp;&nbsp;<Input Type="text" size="15" name="fltrName" value="<%=fltrName%>" ></td>
<%}else{%>
<Input Type="hidden" name="fltrName" value="<%=fltrName%>" >
<%}%>

<td width="20%"><%=LC.L_Add%><%--Add*****--%>&nbsp; <input type="text" name="addrow" value="0" size="2" maxlength="2">&nbsp;<%=LC.L_More_Rows%><%--more rows*****--%></td>

<td>
	<% if (pageRight > 4) { %>	
	<button type="submit" onClick="return saveFilter(document.dynadvanced,'addrow');"><%=LC.L_Refresh%></button>
	<% } %>
</td>

</tr>

<!-- <%if ( StringUtil.isEmpty(fltrName) ){%>
<tr>
<td colspan="5" align="center"><b>Specify Filter Name </b>&nbsp;&nbsp;<Input Type="text" size="45" name="fltrName" value="<%=fltrName%>" ></td>
</tr>
<%}else{%>
<Input Type="hidden" name="fltrName" value="<%=fltrName%>" >
<%}%> -->


</table>

<input type="hidden" name="mode" value="chgfltr">
<input type="hidden" name="prevFilter" value="<%=selValue%>">
<input type="hidden" name="dynType" value="<%=dynType%>">
<input type="hidden" name="reportStudy" value="<%=reportStudy%>">
<input type="hidden" name="srcmenu" value="<%=src%>">

<%
	//Iterate through htFormPKInfo and create the hidden fields for filter - table pk info
	Hashtable htIgnoreFilters = new Hashtable();
	htIgnoreFilters = container.getIgnoreFilters(); //also get ignore filter info

	if (htFormPKInfo !=null)
	{
		Collection collPK = htFormPKInfo.keySet();
		Iterator itrPK = collPK.iterator();
		String strPKForm = "";
		String strPkCol = "";
		String strPKTable = "";
		String strFilterCol = "";
		String strMapStudyColumn = "";
		String strIgnoreFilters = "";

			while (itrPK.hasNext()) {
					Hashtable htFormInfo = new Hashtable();
					strPKForm = (String) itrPK.next();
					htFormInfo = (Hashtable) htFormPKInfo.get(strPKForm);
					strPkCol = (String) htFormInfo.get("mapTablePK");
					strPKTable = (String) htFormInfo.get("mapPKTableName");
					strFilterCol = (String) htFormInfo.get("mapTableFilterCol");
					strMapStudyColumn = (String) htFormInfo.get("mapStudyColumn");

					if (htIgnoreFilters != null && htIgnoreFilters.containsKey(strPKForm))
					{
						strIgnoreFilters = (String)	htIgnoreFilters.get(strPKForm);
					}
					else
					{
						strIgnoreFilters = "";
					}

					//print info in hidden fields
					if (htFormInfo != null)
					{%>
					<input type="hidden" name="mapTablePKFormID" value="<%=strPKForm%>">
					<input type="hidden" name="mapTablePK" value="<%=strPkCol%>">
					<input type="hidden" name="mapPKTableName" value="<%=strPKTable%>">
					<input type="hidden" name="mapTableFilterCol" value="<%=strFilterCol%>">
					<input type="hidden" name="mapMapStudyColumn" value="<%=strMapStudyColumn%>">
					<input type ="hidden" name="mapIgnoreFilter" value="<%=strIgnoreFilters%>">

					<%}	//htFormInfo != null

		 	    }
	}//htFormPKInfo!=null

	%>

<%
	CodeDao cdStatus = new CodeDao();
	cdStatus.getCodeValues("patStatus",accId);

	ArrayList arCid = new ArrayList();
	arCid = cdStatus.getCId();

   	ArrayList arCSubType = new ArrayList();
	arCSubType = cdStatus.getCSubType();

   	ArrayList cDesc = new ArrayList();
	cDesc = cdStatus.getCDesc();

	
	ArrayList cHide = new ArrayList();
	cHide=cdStatus.getCodeHide();
	String drFromDD = ""; String drToDD  = ""; String statusDDfromSelValue = "";String statusDDToSelValue = "";
	String selMonth = ""; String selMonthYear = ""; String selYear="";

	if (StringUtil.isEmpty(defDateFilterType))
	{
		defDateFilterType = "A";
	}
	if (defDateFilterType.equals("A"))
	{
		defDateFilterDateFrom = "";
		defDateFilterDateTo = "";

	}


	if (defDateFilterType.equals("PS"))
	{
		statusDDfromSelValue = defDateFilterDateFrom ;
		statusDDToSelValue = defDateFilterDateTo ;

	}

	if (defDateFilterType.equals("Y"))
	{
		if (defDateFilterDateFrom.length() >= 10)
		{
			selYear = String.valueOf(DateUtil.getYear(defDateFilterDateFrom));
		}
	}

	if (defDateFilterType.equals("M"))
	{

		if (defDateFilterDateFrom.length() >= 10)
		{
			selMonthYear = String.valueOf(DateUtil.getYear(defDateFilterDateFrom));
			selMonth=  String.valueOf(DateUtil.getMonth(defDateFilterDateFrom));

								;

			if (selMonth.startsWith("0"))
			{
				selMonth = selMonth.substring(1);
			}
		}
	}


	//drFromDD = EJBUtil.createPullDownWithStr("statusFrom",statusDDfromSelValue,arCSubType,cDesc);
	drFromDD = EJBUtil.createPullDownWithStrIfHidden("statusFrom",statusDDfromSelValue,arCSubType,cDesc,cHide);
	//drToDD = EJBUtil.createPullDownWithStr("statusTo",statusDDToSelValue,arCSubType,cDesc);
	drToDD = EJBUtil.createPullDownWithStrIfHidden("statusTo",statusDDToSelValue,arCSubType,cDesc,cHide);
	String yrFilterDD = ""; String monthFilterDD = ""; String monthFilterYearDD = "";

	yrFilterDD = DateUtil.getYearDropDown("yearFilter",selYear,false);
	monthFilterDD = DateUtil.getMonthDropDown("monthFilter",selMonth,false);
	monthFilterYearDD = DateUtil.getYearDropDown("monthFilterYear",selMonthYear,false);

%>
	<HR>

	<Input type="hidden" name="dtFilterDateFrom" SIZE=10 value=<%=defDateFilterDateFrom%>>
	<Input type="hidden" name="dtFilterDateTo" SIZE=10 value=<%=defDateFilterDateTo%>>

<table class="tableDefault" width="100%" border="0">
	<tr>
		<td colspan = 2><b><%=MC.M_DtFtr_DispDtEtr%><%--Date Filter - Display data entered for*****--%>:</b></td></tr>
		<tr><td width="20%"><input type = "radio" name="dateRangeType" value="A"
		<%if(StringUtil.isEmpty(defDateFilterType) || defDateFilterType.equals("A")) { %> CHECKED <% } %> ><i> <%=LC.L_All%><%--All*****--%></i>
		</td><td colspan=2> <input type = "radio" name="dateRangeType" value="PS"
		<% if( defDateFilterType.equals("PS")) { %> CHECKED <% } %>	>
			<i><%=MC.M_PatFrom_StatusDt%><%--Patient from status date of*****--%> </i><%=drFromDD%> <i> <%=LC.L_To%><%--To*****--%> </i><%=drToDD%>
		</td>
	</tr>
	<tr>

		<td width="20%">
			<input type = "radio" name="dateRangeType" value="Y"
			<% if( defDateFilterType.equals("Y")) { %> CHECKED <% } %> ><i> <%=LC.L_Year%><%--Year*****--%> </i><%=yrFilterDD%>
		</td>
		<td>
			<input type = "radio" name="dateRangeType" value="M"
			<% if( defDateFilterType.equals("M")) { %> CHECKED <% } %> > <i><%=LC.L_Month%><%--Month*****--%> </i><%=monthFilterDD%>&nbsp;<%=monthFilterYearDD%>
		</td>
		<td>
			<input type = "radio" name="dateRangeType" value="DR"
			<% if( defDateFilterType.equals("DR")) { %> CHECKED <% } %>	><i> <%=LC.L_DateRange_Frm%><%--Date Range: From*****--%></i>
<%-- INF-20084 Datepicker-- AGodara --%>
			<Input type=text name="drDateFrom" class="datefield" READONLY SIZE=10
				<% if( defDateFilterType.equals("DR")) { %> value = <%=defDateFilterDateFrom%>  <% } %>>
								
			&nbsp; <i><%=LC.L_To%><%--To*****--%> </i>
			<Input type=text name="drDateTo" class="datefield" READONLY SIZE=10
				<% if( defDateFilterType.equals("DR")) { %> value = <%=defDateFilterDateTo %>  <% } %> >
				
		</td>
	</tr>
</table>
				<HR>
<table class="tableDefault" width="100%" border="1">
<tr>
<th width="10%"><%=LC.L_Exclude%><%--Exclude*****--%></th>
<th width="15%"><%=LC.L_Start_Bracket%><%--Start Bracket*****--%></th>
<th width="20%"><%=LC.L_Select_Fld%><%--Select Field*****--%></th>
<th width="10%"><%=LC.L_Criteria%><%--Criteria*****--%></th>
<th width="20%"><%=LC.L_Value%><%--Value*****--%></th>
<th width="15%"><%=LC.L_End_Bracket%><%--End Bracket*****--%></th>
<th width="10%"><%=LC.L_AndOrOr%><%--And/Or*****--%></th>
</tr>

<input type="hidden" name="fldTypeCount" value="<%=fldTypeList.size()%>">
<%
for (int i = 0; i < fldTypeList.size(); i++)
	{	%>
	<input type="hidden" name="fldTypeOrig" value="<%=fldTypeList.get(i)%>">
<%
	}



if (fldNameSelList.size() > 0) // if there are selected/or index of selected fields
{
	totCount = fldNameSelList.size();
	totCount = totCount + row;
	if (totCount < 10) totCount = 10;
}

%>
	<input type="hidden" name="totcount" value="<%=totCount%>">
<%
for (int i=0;i<(totCount);i++){

  if ((i%2)==0)
  {%>
  <tr class="browserEvenRow">
   <%}else{ %>
   <tr class="browserOddRow">
   <%}
   if (i<(fldName.length)){%>
     <input type="hidden" name="fldLocal" value="<%=fldName[i]%>">
     <input type="hidden" name="fldLocalCol" value="<%=fldCol[i]%>">

     <%}else{%>
     <input type="hidden" name="fldLocal" value="">
     <input type="hidden" name="fldLocalCol" value="">
     <%}%>
      <td width="10%">
   <%if (excludeList.indexOf((new Integer(i)).toString())>-1){%>
   <p align="center"><input type="checkbox" name="exclude" value="<%=i%>"   checked></p>
   <%}else{%>
   <p align="center"><input type="checkbox" name="exclude" value="<%=i%>" ></p>
   <%}%>
     </td>
   <td width="15%">
   <%if (startBracList.indexOf((new Integer(i)).toString())>-1){%>
   <p align="center"><input type="checkbox" name="startBracket" value="<%=i%>"   checked></p>
   <%}else{%>
   <p align="center"><input type="checkbox" name="startBracket" value="<%=i%>" ></p>
   <%}%>
   </td>
   <%if (i<(flltDtIds.size())){%>
        <input type="hidden" name="fltrDtId" value=<%=flltDtIds.get(i)%>>
	<%}else{%>
	<input type="hidden" name="fltrDtId" value="0">
	<%}%>

   <td width="20%"><%
   if (fldName!=null){
   flgMatch="N";
   %>
	<SELECT NAME='fldName' onChange="setType(document.dynadvanced,<%=i%>)">
	<%for (counter = 0; counter < (fldName.length) ; counter++){
		name =fldCol[counter];
		name = name.trim();
		if (name==null) name="";
		/*if (name.length()>=32) name=name.substring(0,30);
		if (name.indexOf("?")>=0)
		name=name.replace('?','q');
		if (name.indexOf("\\")>=0)
		name=name.replace('\\','S');*/
		if (name.length()>0){


		if ((fldNameSelList.size()>0) && (i<fldNameSelList.size())){

		if (name.equals(((String)fldNameSelList.get(i)).trim() )){
		flgMatch="Y";



		if (counter<fldTypeList.size())
		fldType=(String)fldTypeList.get(counter);


		if (fldType==null) fldType="";
		%>
		<OPTION value ='<%=name%>' selected><%=fldName[counter]%></OPTION>;
		<%}else{

		%>

		<OPTION value='<%=name%>'><%=fldName[counter]%></OPTION>
		<%}
		}
		else{

		%>
		<OPTION value='<%=name%>'><%=fldName[counter]%></OPTION>
		<%}
		}

	}
	if (flgMatch.equals("N")){%>
		<OPTION value=''  selected><%=LC.L_Select_AnOption%><%--Select an Option*****--%></OPTION>
		<%}else{%>
	<OPTION value='' ><%=LC.L_Select_AnOption%><%--Select an Option*****--%></OPTION>
	<%}%>
	</SELECT>
	<%}%>
	</td>
     <input type="hidden" name="fldType" value="<%=(fldType.length()==0)?"":fldType%>">



      <td width="10%"> <select size="1" name="scriteria">
      		  <option value="contains" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("contains")){%> Selected <%}}%>><%=LC.L_Contains%><%--Contains*****--%></option>
      		<option value="notcontains" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("notcontains")){%> Selected <%}}%>><%=LC.L_Does_NotContain%><%--Does Not Contain*****--%></option>
		  <option value="isnull" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("isnull")){%> Selected <%}}%>><%=LC.L_Is_Null%><%--Is Null*****--%></option>
		  <option value="isnotnull" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("isnotnull")){%> Selected <%}}%>><%=LC.L_Is_NotNull%><%--Is Not Null*****--%></option>
		  <option value="isnotequal" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("isnotequal")){%> Selected <%}}%>><%=MC.M_Is_NotEqualTo%><%--Is Not Equal To*****--%></option>
  			<option value="isequalto" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("isequalto")){%> Selected <%}}%>><%=LC.L_Is_EqualTo%><%--Is Equal To*****--%></option>
			  <option value="start" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("start")){%> Selected <%}}%>><%=LC.L_Begins_With%><%--Begins with*****--%></option>
			  <option value="ends" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("ends")){%> Selected <%}}%>><%=LC.L_Ends_With%><%--Ends with*****--%></option>
			  <option value="isgt" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("isgt")){%> Selected <%}}%>><%=LC.L_Is_GreaterThan%><%--Is Greater Than*****--%></option>
			  <option value="islt" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("islt")){%> Selected <%}}%>><%=LC.L_Is_LessThan%><%--Is Less Than*****--%></option>
			  <option value="isgte" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("isgte")){%> Selected <%}}%>><%=MC.M_Is_GtrThanEqual%><%--Is Greater than Equal*****--%></option>
			  <option value="islte" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("islte")){%> Selected <%}}%>><%=MC.M_Is_LessThanEqual%><%--Is Less than Equal*****--%></option>
			  <option value="first" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("first")){%> Selected <%}}%>><%=LC.L_First_Value%><%--First Value*****--%></option>
			  <option value="latest" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("latest")){%> Selected <%}}%>><%=LC.L_Latest_Value%><%--Latest Value*****--%></option>
			  <option value="highest" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("highest")){%> Selected <%}}%>><%=LC.L_Highest_Val%><%--Highest Value*****--%></option>
			  <option value="lowest" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("lowest")){%> Selected <%}}%>><%=LC.L_Lowest_Value%><%--Lowest Value*****--%></option>
			  <option value='' <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if (((String)scriteriaList.get(i)).length()==0){%> Selected <%}}else{%>Selected<%}%>><%=LC.L_Select_AnOption%><%--Select an Option*****--%></option>
			  </select></td>
	<td width="20%"><input type="text" name="fltData" maxLength="50"  value='<%=(fldDataList.size()>0 && i<fldDataList.size())?((fldDataList.get(i)==null)?"":fldDataList.get(i)):""%>'></td>
	<td width="15%">
	<%if (endBracList.indexOf((new Integer(i)).toString())>-1){%>
   <p align="center"><input type="checkbox" name="endBracket" value="<%=i%>" checked></p>
   <%}else{%>
   <p align="center"><input type="checkbox" name="endBracket" value="<%=i%>"></p>
   <%}%>
	</td>
	<td width="10%"><select size="1" name="extend">

<option value="and" <%if ((extendList.size()>0) && (i<extendList.size())) {if ((extendList.get(i)).equals("and")){%> Selected <%}}%>><%=LC.L_And%><%--And*****--%></option>
<option value="or" <%if ((extendList.size()>0) && (i<extendList.size())) {if ((extendList.get(i)).equals("or")){%> Selected <%}}%>><%=LC.L_Or%><%--Or*****--%></option>
</select></td>


   <!--<td width="10%"><select size="1" name="fldOrder">
   <option value="">Select an Option</option>
<option value="Asc">Ascending</option>
<option value="Desc">Descending</option>
</select></td><td width="65%" ></td>-->



 </tr>
<%}%>
</table>
<br>
<!--<table width="100%" cellspacing="0" cellpadding="0">
    <td width="40%"></td>
    <td><input name="direction" type="hidden" value="forward">
		<input type="image" src="../images/jpg/Next.gif" align="absmiddle"  border="0" onClick ="return nextPage(document.dynadvanced);">
		<A href=""><img src="../images/jpg/Next.gif" align="absmiddle" border="0"></img></A>
      </td>
      </tr>
  </table>-->
</DIV>
  <input type="hidden" name="sess" value="keep">
</form>
<%
} // end for (attributes==null)
} else {  //else of if body for session

%>
  <jsp:include page="timeout.html" flush="true"/>

  <%
}


%>
 <div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</DIV>
  <div class ="mainMenu" id = "emenu">
  <jsp:include page="getmenu.jsp" flush="true"/>
  </div>


</body>
</html>
