<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="eventAssocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="codeLst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="grpRights" scope="request" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<%@page import="com.velos.eres.business.common.*,com.velos.eres.service.util.*"%>
<%@page import="com.velos.esch.business.common.*,com.velos.esch.web.eventassoc.EventAssocJB"%>
<%@page import="com.velos.remoteservice.lab.*,java.net.URLEncoder,com.velos.eres.business.section.*"%>
<%@page import="com.velos.eres.web.user.UserJB,com.velos.eres.web.studyRights.StudyRightsJB" %>
<%@page import="java.util.*,java.io.*"%>
<%@page import="org.json.*,org.w3c.dom.*"%>

  <%
   HttpSession tSession = request.getSession(true);
   if (sessionmaint.isValidSession(tSession))
   {

    int eventId = StringUtil.stringToNum(request.getParameter("eventId"));
    if (eventId == 0) { eventId = -1; }
    
    String source = (String)request.getParameter("source");
    source = (null == source)? "" : source;
    if(source.equals(LC.L_Event_Name)|| source.equals(LC.L_Cpt_Code)|| source.equals(LC.L_Description)){
    	StringBuffer st=new StringBuffer();
    String 	mName=request.getParameter("mName");
    mName = (null == mName)? "" : mName;
    int row=0;
    int Addrow=0;
    String OverlibParameter=""; 
    if(mName.length()>50)
    	 OverlibParameter=",WRAP, BORDER,2, CENTER,BELOW,OFFSETY,20,STATUS,'Draggable with overflow scrollbar, caption and Close link',";
    else
    	 OverlibParameter=",ABOVE,";

    if(mName.length()>50){
    	row=mName.length()/50;
    	for(int i=0;i<row;i++){
        	st.append(mName.substring(i*50,50*(i+1)));
        	
        	}
    	if((mName.length() % 50)!=0 ){
    		st.append(mName.substring(mName.length()-(mName.length() % 50),mName.length()));
    	}
        }
    else st.append("<b>"+mName+"</b>");
    out.print("overlib('"+st +"'"+OverlibParameter+"CAPTION,'"+source+"');");
    return;
    }
    String eventType = (String)request.getParameter("eventType");
    eventType = (null == eventType)? "E" : eventType;
    
	int userId = StringUtil.stringToNum((String) tSession.getValue("userId"));
    int studyId = StringUtil.stringToNum((String)request.getParameter("studyId"));
    int finDetRight =0;
    //if ("S".equals(source)){
    	if (studyId > 0){
    		TeamDao teamDao = new TeamDao();
    	    teamDao.getTeamRights(studyId,  userId);
    	    ArrayList tId = teamDao.getTeamIds();
    	    
    		StudyRightsJB stdTeamRights = null;
    		if (tId != null && tId.size() > 0) {
    			stdTeamRights = new StudyRightsJB();
    			stdTeamRights.setId(StringUtil.stringToNum(tId.get(0).toString()));
    			ArrayList teamRights = teamDao.getTeamRights();
    			stdTeamRights.setSuperRightsStringForStudy((String)teamRights.get(0));
    			stdTeamRights.loadStudyRights();
    		}
    		tSession.putValue("studyId", ""+studyId);
  	    	tSession.setAttribute("studyRights", stdTeamRights);

    		if (stdTeamRights == null || stdTeamRights.getFtrRights() == null || stdTeamRights.getFtrRights().size() == 0) {
    			finDetRight = 0;
    		} else {
    			finDetRight=Integer.parseInt(stdTeamRights.getFtrRightsByValue("STUDYFIN"));
    		}
    	}
    //}

	int accountId = EJBUtil.stringToNum((String)tSession.getValue("accountId"));
    String eventname = "";
	if ((eventId > 0))
	{
		String mouseover = "";
		String OverlibParameter="";
		if (null != source){
			if ("P".equals(source) || "L".equals(source)){
				if (studyId == 0){
					//Library Event/Calendar
					EventdefDao defDao = new EventdefDao();
					mouseover = defDao.getEventMouseOver(eventId, eventType);
				} else {
					//Library Calendar in a Budget linked to a study
					EventdefDao defDao = new EventdefDao();
					mouseover = defDao.getEventMouseOver(eventId, eventType, finDetRight);
				}
			}else if ("S".equals(source)){
				//Study Setup or Admin Schedule
				EventAssocDao assocDao = new EventAssocDao();
				mouseover = assocDao.getEventMouseOver(eventId, eventType, finDetRight);

			}else if("SA".equals(source)){
				EventAssocJB assocJB = new EventAssocJB();
				assocJB.setEvent_id(eventId);
				assocJB.getEventAssocDetails();
				eventname = assocJB.getName();
				if(eventname!=null){
					if(eventname.length()>500 && eventname.length()<=1000) OverlibParameter=",WRAP, BORDER,2, CENTER,ABOVE,OFFSETY,50,STATUS,'Draggable with overflow scrollbar, caption and Close link',";
	  		    	   else if(eventname.length()>1000 && eventname.length()<=2000)OverlibParameter=",WRAP, BORDER,2, CENTER,ABOVE,OFFSETY,125,STATUS,'Draggable with overflow scrollbar, caption and Close link',";
	  		    	   else if(eventname.length()>2000)OverlibParameter=",WRAP, BORDER,2, CENTER,ABOVE,OFFSETY,175,STATUS,'Draggable with overflow scrollbar, caption and Close link',";
	  		    	   else OverlibParameter=",ABOVE,";
					mouseover=eventname; }
				else{
					mouseover="";
				}
			}
		}
		if(mouseover.length()>80 && !"SA".equals(source))
		{
			OverlibParameter=",WRAP, BORDER,2, CENTER,BELOW,OFFSETY,20,STATUS,'Draggable with overflow scrollbar, caption and Close link',";
		}
		if ("SA".equals(source))
 			out.print("overlib('"+ mouseover + "'"+OverlibParameter+"CAPTION,'"+LC.L_Event_Name+"');");
 		else
 			out.print("overlib('"+ mouseover + "'"+OverlibParameter+"CAPTION,'"+LC.L_Event_Details+"');");
	}
} //end of if session times out
else
{
%>
 <jsp:include page="timeout.html" flush="true"/>
  <%
} //end of else body for page right
%>
