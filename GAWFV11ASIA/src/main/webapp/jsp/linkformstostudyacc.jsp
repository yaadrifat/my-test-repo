<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ page import="com.velos.eres.service.util.*"%>
</HEAD>

<%
 //get the page from where the form is being linked
 //A-account, S-study
  String linkFrom = request.getParameter("linkFrom");
%>
<title><%=LC.L_Add_Forms%><%--Add Forms*****--%></title>


<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" SRC="whichcss.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">

//Added by Manimaran for Enhancement F4
function newForm(formobj,url){
   window.opener.location=url;//KM-to fix the Bug2744
   this.window.close();
}



 function  validate(formobj,num){
 //call validate only when submit is clicked
 checkQuote="N";

  if (formobj.btnClicked.value=="Submit") {

 	formobj.pageMode.value="Submit" ;

	//check form has been selected
	if (num==0) {
	   alert("<%=MC.M_NoRec_InFrmSec%>");/*alert("No records found in Forms to be Linked section.");*****/
	   return false;
	}

	//check form name has been entered

 	for (i=0;i<num;i++)
	{
			if (num > 1) {
				if ((formobj.frmName[i].value) == "") {
				    alert("<%=MC.M_Etr_FrmName%>");/*alert("Please enter Form name.");*****/
					formobj.frmName[i].focus();
					return false;
				}
			} else {
				if ((formobj.frmName.value) == "") {
				    alert("<%=MC.M_Etr_FrmName%>");/*alert("Please enter Form name.");*****/
					formobj.frmName.focus();
					return false;
				}
			}
	} //for end

    if (!(validate_col('e-Signature',formobj.eSign))) return false

	/*if(isNaN(formobj.eSign.value) == true) {
		alert*/<%--("<%=MC.M_IncorrEsign_EtrAgain%>");--%>
		/*alert("Incorrect e-Signature. Please enter again");
		formobj.eSign.focus();
		return false;
   	}*/
	var i=0;
	var j=0;

	formLinkLen= eval("formobj.formLink"+i+".length");

	for(i=0;i<num;i++)
	  {
		for(j=0;j<formLinkLen;j++)
		{
		  if( (formobj.linkFrom.value!="S" ) && (eval("formobj.formLink"+i+"["+j+"].checked") && (eval("formobj.formLink"+i+"["+j+"].value")=="S" ||
			eval("formobj.formLink"+i+"["+j+"].value")=="SP")))
			{
		    if(num == 1){
				if(!(validate_col('Study',formobj.accFormStudy)))
				{
				 return false;
				}
			}
			else {
			if(!(validate_col('Study',formobj.accFormStudy[i])))
				{
				 return false;
				}
			}
			}

		}
	  }


  } else {//btn clicked
	return false;
  }

 }


 function setMode(formobj, flag, btnClicked,num)
 {
    formobj.btnClicked.value=btnClicked;
 	if(flag=="Select")
	{
		formobj.pageMode.value="Select" ;
	}

//check if any checkbox is selected
    if (btnClicked=="down") {

		for (i=0;i<num;i++) {
			if (num > 1) {
				if (formobj.selForms[i].checked) {
					formobj.submit();
					return true;
				}
			} else {
				if (formobj.selForms.checked) {
					formobj.submit();
					return true;
				}
			}
		}
		alert("<%=MC.M_Selc_Frm%>");/*alert("Please Select a Form.");*****/
		// to fix the Bug #2366
		formobj.btnClicked.value="Submit";
		return false;
	}

//check if any checkbox is de-selected
    if (btnClicked=="up") {
		for (i=0;i<num;i++) {
			if (num > 1) {
				if (formobj.deSelIds[i].checked) {
					formobj.submit();
					return true;
				}
			} else {
				if (formobj.deSelIds.checked) {
					formobj.submit();
					return true;
				}
			}
		}
		alert("<%=MC.M_Deselect_Frm%>");/*alert("Please Deselect a Form.");*****/
		// to fix the Bug #2366
		formobj.btnClicked.value="Submit";
		return false;
	}
}

 function openOrgWindow(counter,formRows,formobj) {
    //km-to fix Bug 2320  Error 500 while clicking on the Organization / Group link
    //Added by Manimaran to fix Bug 2409.
    if (formRows==1)
      formorg1=formobj.formOrgIds.value;
    else
       formorg1=eval("formobj.formOrgIds["+counter+"].value");

	//KM-4Nov08
	formorg1 = encodeString(formorg1);
    windowName=window.open("selectOrg.jsp?openerform=selectForms&counter="+counter+"&formRows="+formRows+"&selectOrgs="+formorg1,"Information1","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=350,height=300");
	windowName.focus();
 }




 function openwin1(fid) {
	windowName=window.open("formpreview.jsp?formId="+fid,"1","toolbar=no,scrollbars=yes,resizable=no,menubar=no,status=yes,width=825,height=600,Left=50,Top,150")
	//windowName.moveTo(100,50);
	windowName.focus();
}

 function openGroupWindow(counter,formRows,formobj) {
    //km-to fix Bug 2320  Error 500 while clicking on the Organization / Group link
    //Added by Manimaran to fix Bug 2409.

    if (formRows==1)
       selGrpNames1 =formobj.selGrpIds.value;
    else
       selGrpNames1=eval("formobj.selGrpIds["+counter+"].value");
	//KM-4Nov08
	selGrpNames1 = encodeString(selGrpNames1);

    windowName=window.open("selectGroup.jsp?openerform=selectForms&counter="+counter+"&formRows="+formRows+"&selectGroups="+selGrpNames1,"Information1","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=350,height=300");
	windowName.focus();
}

</SCRIPT>



<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.VelosResourceBundle" %>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<%! long prefix =0; %>

<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1)
   	ienet = 0; //IE
else
	ienet = 1;

if (ienet == 0) {
%>
<body style="overflow:scroll">
<%} else {%>
<body>
<%}%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="catLibJB" scope="request"  class="com.velos.eres.web.catLib.CatLibJB"/>
<jsp:useBean id="linkedFormsB" scope="request"  class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>

<!-- Added by Gopu to fix the bugzilla Issue #2406 -->
<jsp:useBean id ="userB" scope="page" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id ="userSiteB" scope="request" class="com.velos.eres.web.userSite.UserSiteJB"/>
<%@ page language = "java" import = "com.velos.eres.business.site.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB"

%>
<DIV class="popDefault" id="div1">

<%
 HttpSession tSession = request.getSession(true);

 if (sessionmaint.isValidSession(tSession))
 {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
	<jsp:include page="include.jsp" flush="true"/>

<%







 int pageRight = 7;

 if (pageRight > 0)
 {
  	 int len=0;

	 int accountId = EJBUtil.stringToNum((String) tSession.getValue("accountId"));
	 String userId = (String) tSession.getValue("userId");
	 String lnkStudyId = request.getParameter("lnkStudyId");

 	 String deSelIds[] = request.getParameterValues("deSelIds");
	 String deSelNames[] = request.getParameterValues("deSelNames");
	 String deSelDescs[] = request.getParameterValues("deSelDescs");

	 String catLibType = "T";

	 ArrayList formTypeIds = new ArrayList();
	 ArrayList formTypeNames = new ArrayList();
	 ArrayList studyIds = new ArrayList();
	 ArrayList studyNumbers = new ArrayList();
	 ArrayList formIds = new ArrayList();
 	 ArrayList formNames = new ArrayList();
	 ArrayList formDescs = new ArrayList();
	 ArrayList catlibNames = new ArrayList();
	 ArrayList formStatus = new ArrayList();//KM
	 String formId="";
	 String formName="";
	 String formDesc="";
	 String selName = "";
	 String catlibName = "";
	 String formStat = "";//KM
	 int i=0;
     int formTypeId = 0;
	 int cnt=0;

	 formName = request.getParameter("formName");
	 if (formName==null) formName="";
	 String formType = request.getParameter("formType");
	 if (!(formType==null))  formTypeId = EJBUtil.stringToNum(formType);

	 int studyId = EJBUtil.stringToNum(request.getParameter("selStudyId"));

 	 //form type dropdown
	 String formTypeStr="";
	 int forLastAll = 0 ;
	 CatLibDao catLibDao = new CatLibDao();
	 catLibDao= catLibJB.getCategoriesWithAllOption(accountId,catLibType,forLastAll);
	 formTypeIds = catLibDao.getCatLibIds();
	 formTypeNames= catLibDao.getCatLibNames();
	 formTypeStr=EJBUtil.createPullDown("formType", formTypeId, formTypeIds, formTypeNames);

	 //study dropdown
	 String study="";
	 StudyDao studyDao = new StudyDao();
	 studyDao.getStudyValuesForUsers(userId);
	 studyIds = studyDao.getStudyIds();
	 studyNumbers = studyDao.getStudyNumbers();
	 study=EJBUtil.createPullDown("selStudyId", studyId, studyIds, studyNumbers);


	 LinkedFormsDao linkedFormsDao = new LinkedFormsDao();
	//Added  by gopu To fix the bugzilla Issue #2406
	//To get the forms which have access rights other than the primary organization
		userB.setUserId(EJBUtil.stringToNum(userId));
		userB.getUserDetails();
		int userPrimOrg = EJBUtil.stringToNum(userB.getUserSiteId());
		ArrayList userSiteIds = new ArrayList();
		ArrayList userSiteSiteIds = new ArrayList();
     	ArrayList userSiteRights = new ArrayList();
		UserSiteDao userSiteDao = userSiteB.getUserSiteTree(accountId,EJBUtil.stringToNum(userId));
		userSiteIds = userSiteDao.getUserSiteIds();
		userSiteSiteIds = userSiteDao.getUserSiteSiteIds();
		userSiteRights = userSiteDao.getUserSiteRights();
		int len1 = userSiteIds.size();
			String siteIdAcc="";
			int accrights=0;
			int userSiteSiteId=0;
			int userSiteRight=0;
			for (int count=0;count<len1;count++) {
	 		userSiteSiteId = EJBUtil.stringToNum(((userSiteSiteIds.get(count)) == null)?"-":(userSiteSiteIds.get(count)).toString());
     			userSiteRight = EJBUtil.stringToNum(((userSiteRights.get(count)) == null)?"-":(userSiteRights.get(count)).toString());

			if (userPrimOrg!=userSiteSiteId)
			   accrights=userSiteRight;
			if(accrights!=0)
				siteIdAcc=siteIdAcc+(userSiteSiteId+",");
			}
		        siteIdAcc=siteIdAcc+userPrimOrg;
		/////////

	 linkedFormsDao = linkedFormsB.getFormListForLinkToStudy(accountId, EJBUtil.stringToNum(userId),formName.trim(),formTypeId, studyId,siteIdAcc);

     formIds = linkedFormsDao.getFormId();
	 formNames = linkedFormsDao.getFormName();
	 formDescs = linkedFormsDao.getFormDescription();
     catlibNames = linkedFormsDao.getCatlibName();
     formStatus = linkedFormsDao.getFormStatus();


 %>

<P class="sectionHeadingsFrm"><%=LC.L_Search_Frm%><%--Search a Form*****--%></P>
<Form name="formlist" action="linkformstostudyacc.jsp"  method="post" >
<input type="hidden" name="lnkStudyId" value=<%=lnkStudyId%>>
<input type="hidden" name="linkFrom" value="<%=linkFrom%>">
<table width="99%" border="0" cellspacing="0" cellpadding="0" class="basetbl midAlign">
<tr>
<td width="30%"><%=LC.L_Frm_Name%><%--Form Name*****--%> <BR><input type="text" name="formName" value='<%=formName%>'></td>
<td width="30%"><%=LC.L_Form_Type%><%--Form Type*****--%> <BR><%=formTypeStr%></td>
<td width="25%"><%=LC.L_Study%><%--<%=LC.Std_Study%>*****--%> <BR><%=study%></td>
<td width="5%"><button type="submit"><%=LC.L_Search%></button></td>
</tr>
</table>
<%   len = formIds.size();
	 if (!(deSelIds==null))
	 {
	   cnt =deSelIds.length;
	 }
%>

<% if (linkFrom.equals("S")) {%>
<P class="defComments"><%=MC.M_FrmLib_CreateNewFrm%><%--The following are the Forms currently listed in your Library. Select the Form that you wish to associate with your <%=LC.Std_Study%> or go to the Form Library to create a new Form.*****--%></P>
<%}else if (linkFrom.equals("A")) {%>
<P class="defComments"><%=MC.M_FrmCurList_FrmCreateNew%><%--The following are the Forms currently listed in your Library. Select the Form that you wish to associate with your Account or go to the Form Library to create a new Form.*****--%></P>
<%}%>

<!--Added by Manimaran for the Enhancement F4 -->
<table width="99%" border="0" cellspacing="0" cellpadding="0" >
<tr><td align="right">
<A href="javascript:newForm(document.formlist,'formcreation.jsp?srcmenu=tdmenubaritem4&calledFrom=L&mode=N&selectedTab=1&formLibId=<%=formId%>')" onClick="return f_check_perm(<%=pageRight%>,'N')"><%=MC.M_CreateNew_Frm_Upper%><%--CREATE A NEW FORM*****--%></A></td>
</tr>
</table>
</form>

<Form name="selectForms" id="selFrm" method="post" action="addformtostudyacc.jsp" onsubmit="if (validate(selectForms,<%=cnt%>)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

<input type="hidden" name="btnClicked" value="Submit">
<input type="hidden" name="pageMode" value="">
<input type="hidden" name="lnkStudyId" value=<%=lnkStudyId%>>
<input type="hidden" name="selStudyId" value="<%=studyId%>">
<input type="hidden" name="formType" value="<%=formType%>">
<input type="hidden" name="linkFrom" value="<%=linkFrom%>">

<table width="99%" border="0" cellspacing="0" cellpadding="0" class="basetbl midAlign">
<tr>
<th width="10%"><%=LC.L_Select%><%--Select*****--%></th>
<th width="30%"><%=LC.L_Form_Name%><%--Form Name*****--%></th>
<th width="20%"><%=LC.L_Form_Type%><%--Form Type*****--%></th>
<th width="30%"><%=LC.L_Description%><%--Description*****--%></th>
<th width="10%"><%=LC.L_Status%><%--Status*****--%></th><!--KM-->
</tr>
<%

   for (i=0;i<formIds.size();i++)
   {
   	formId = formIds.get(i).toString();
	formName = formNames.get(i).toString();
	formDesc = formDescs.get(i).toString();
	formStat = formStatus.get(i).toString();//KM
	if(catlibNames.get(i) != null){
	catlibName = catlibNames.get(i).toString();
	}
	else{
	catlibName = "-";
	}

	//the form id, name and desc are passed with ; separator
	String selForms = formId+"[VELSEP]"+formName+"[VELSEP]"+formDesc;

    if ((i%2)==0) {
%>
	 <tr class="browserEvenRow">
<%} else {%>
	 <tr class="browserOddRow">
<%}%>
	 <td><input type="checkbox" name="selForms" value="<%=selForms%>"></td>
	 <td ><A href="#" onClick=openwin1(<%=formId%>)><%=formName%></A></td>
	<td><%=catlibName%></td>
	 <td><%=formDesc%></td>
	 <td><%=formStat%></td><!--KM-->
	 </tr>
 <%} //for loop
 %>
 <tr>
 <td colspan="2"  align="center">
  <img src="../images/jpg/formdown.gif" align="absmiddle" border="0" onClick="return setMode(document.selectForms,'Select','down',<%=len%>)">
 </td>
 <td  align="left">
  <img src="../images/jpg/formup.gif" align="absmiddle" border="0" onClick="return setMode(document.selectForms,'Select','up',<%=cnt%>)">
<!--  <A>Instructions</A> -->
 </td>
 </tr>
</table>


<table width="100%" border="0" cellspacing="0" cellpadding="0" class="basetbl midAlign">
<tr>
<td colspan=6><P class="sectionHeadingsFrm"><%=MC.M_Frm_ToLnk%><%--Forms to be Linked*****--%></P></td>
</tr>
<tr>
<th width="5%"><%=LC.L_Deselect%><%--Deselect*****--%></th>
<th width="20%"><%=LC.L_Name%><%--Name*****--%></th>
<th width="20%"><%=LC.L_Description%><%--Description*****--%></th>
<th width="20%"><%=LC.L_Disp_FormLink%><%--Display Form Link*****--%></th>
<th width="20%"><%=LC.L_Characteristic%><%--Characteristic*****--%></th>
<th width="15%"><%=LC.L_Filters%><%--Filters*****--%></th>
</tr>

<%
	String deFormOrg = "";
	String deFormOrgId = "";
	String radioName = "";
	String formCharName = "";
	String accFormStudy = "";
	accFormStudy = EJBUtil.createPullDown("accFormStudy", 0, studyIds, studyNumbers);

	for(int counter = 0;counter<cnt;counter++)
	{
	    prefix = prefix + 1;
		//make the dynamic element name for Display Form Link and Characteristics
		 radioName = "formLink"+counter;
		 formCharName = "formChar"+counter;

		if ((counter%2)==0)
		{
	%>
      	<tr class="browserEvenRow">
	<%} else
	 { %>
   		<tr class="browserOddRow">
	<%}  %>
		<td> <input type="checkbox" name="deSelIds" value="<%=deSelIds[counter]%>*<%=prefix%>">
		<input type="hidden" name="deSelFormIds" value="<%=deSelIds[counter]%>*<%=prefix%>"> </td>
		<input type="hidden" name="deSelFormOnlyIds" value="<%=deSelIds[counter]%>"> 
		<input type="hidden" name="lnkToStudyIds" value=<%=lnkStudyId%>>
		</td>
		</td>
		<td> <input type="text" name="frmName" maxlength="50" value="<%=deSelNames[counter]%>"></input></td>
		<td> <input type="text" name="frmDesc" maxlength="255" value="<%=deSelDescs[counter]%>"></input></td>
		<td>
			 <% if (linkFrom.equals("S")) { %>
			 <input type="radio" name="<%=radioName%>" value="S" Checked><%=LC.L_Study%><%--<%=LC.Std_Study%>*****--%><br>
			 <input type="radio" name="<%=radioName%>" value="SP"><%=LC.L_Patient%><%--<%=LC.Pat_Patient%>*****--%>
			 <%} else if (linkFrom.equals("A")) {  //// added and modified by salil%>
			  <%
			  GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
			int	study_not_associated_right = 7;//Integer.parseInt(grpRights.getFtrRightsByValue("MACCFRMSNOSTUDY"));
			int study_associated_right = 7;//Integer.parseInt(grpRights.getFtrRightsByValue("MACCFRMSWSTUDY"));
			     String modRight = (String) tSession.getValue("modRight");
	 int patProfileSeq = 0, formLibSeq = 0;
	 acmod.getControlValues("module");
	 ArrayList acmodfeature =  acmod.getCValue();
	 ArrayList acmodftrSeq = acmod.getCSeq();
	 char formLibAppRight = '0';
	 char patProAppRight = '0';
	 formLibSeq = acmodfeature.indexOf("MODFORMS");
	 patProfileSeq = acmodfeature.indexOf("MODPATPROF");
 	 formLibSeq = ((Integer) acmodftrSeq.get(formLibSeq)).intValue();
 	 patProfileSeq = ((Integer) acmodftrSeq.get(patProfileSeq)).intValue();
     formLibAppRight = modRight.charAt(formLibSeq - 1);
     patProAppRight = modRight.charAt(patProfileSeq - 1);


	   if ((String.valueOf(formLibAppRight).compareTo("1") != 0)&&(String.valueOf(patProAppRight).compareTo("1") == 0))
	{%>
			<input type="radio" name="<%=radioName%>" value="PA" Checked><%=LC.L_PatLvl_All%><%--<%=LC.Pat_Patient%> Level (All)*****--%><br>
	<%}
	else {%>

    		<input type="radio" name="<%=radioName%>" value="A" Checked><%=LC.L_Acc_Level%><%--Account Level*****--%><br>
			 <input type="radio" name="<%=radioName%>" value="SA"><%=LC.L_StdLvl_All%><%--<%=LC.Std_Study%> Level (All)*****--%><br>
			 <input type="radio" name="<%=radioName%>" value="PS"><%=MC.M_PatLvl_AllStd%><%--Patient Level (All Studies)*****--%><br>
			<input type="radio" name="<%=radioName%>" value="PR"><%=MC.M_PatLvl_AllStdRest%><%--<%=LC.Pat_Patient%> Level (All <%=LC.Std_Studies%> - Restricted)*****--%><br>
			 <input type="radio" name="<%=radioName%>" value="PA"><%=LC.L_PatLvl_All%><%--<%=LC.Pat_Patient%> Level (All)*****--%><br>
			 <input type="radio" name="<%=radioName%>" value="S"><%=LC.L_Spec_StdLvl%><%--Specific <%=LC.Std_Study%> Level*****--%><br>
			 <input type="radio" name="<%=radioName%>" value="SP"><%=MC.M_SpecStd_PatLvl%><%--Specific <%=LC.Std_Study%> <%=LC.Pat_Patient%> Level*****--%><br>
			 <input type="radio" name="<%=radioName%>" value="USR"><%=LC.L_Usr_Level%><%-- User Level*****--%><br>
			 <input type="radio" name="<%=radioName%>" value="ORG"><%=LC.L_Org_Level%><%--Organization Level*****--%><br>
			 <input type="radio" name="<%=radioName%>" value="NTW"><%=LC.L_Netwrk_Level%><%-- Network Level*****--%><br>
			 <%=LC.L_Study%><%--<%=LC.Std_Study%>*****--%> <%=accFormStudy%>


		      <%}


	}


%>
		</td>
		<td> <input type="radio" name="<%=formCharName%>" value="M" Checked><%=LC.L_Multi_Entry%><%--Multiple entry*****--%><br>
			<!--  <input type="radio" name="<%=formCharName%>" value="O" Checked><%=LC.L_Only_Once%><%--Only once*****--%><br> -->
			 <input type="radio" name="<%=formCharName%>" value="E"><%=LC.L_Only_OnceEditable%><%--Only once (Editable)*****--%>
		</td>
		<td><%=LC.L_Organization%><%--Organization*****--%> <input type="text" name="formOrg" value="" Readonly>
		<A href="#" onClick="openOrgWindow(<%=counter%>,<%=cnt%>,document.selectForms)"><%=LC.L_Select%><%--Select*****--%></A><!--km-->
		<br><br>
		<%=LC.L_Group%><%--Group*****--%> <input type="text" name="selGrpNames" value="" Readonly>
		<A href="#" onClick="openGroupWindow(<%=counter%>,<%=cnt%>,document.selectForms)"><%=LC.L_Select%><%--Select*****--%></A><!--km-->

		</td>

		<input type="hidden" name="formOrgIds" value="">
		<input type="hidden" name="selGrpIds" value="">
		<input type="hidden" name="deSelNames" value="<%=deSelNames[counter]%>">
		<input type="hidden" name="deSelDescs" value="<%=deSelDescs[counter]%>">
		</tr>
	<%}%>

</table>
<br>


<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="selFrm"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>


</Form>

	 <%

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right
	}
else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
</div>


<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</body>

</html>