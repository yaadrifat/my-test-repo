<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">



<title><%=LC.L_Evt_Lib%><%--Event Library*****--%></title>





<SCRIPT>

function  validate(formobj){

	if (!(validate_col('e-Signature',formobj.eSign))) return false



	<%-- if(isNaN(formobj.eSign.value) == true) {

		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/

	formobj.eSign.focus();

	return false;

   } --%>

}

</SCRIPT>



</head>
<jsp:include page="skinChoser.jsp" flush="true"/>


<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%>
<%@ page import="com.aithent.audittrail.reports.AuditUtils,com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>

<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/> 



<% String src;

	src= request.getParameter("srcmenu");

%>



<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>  



<BODY> 

<br>



<DIV class="formDefault" id="div1">

<% 

	String duration= "";

	String protocolId= "";

   	String calledFrom= "";

	String mode= "";

	String calStatus= "";

	String eventId="";	



	

HttpSession tSession = request.getSession(true); 

 if (sessionmaint.isValidSession(tSession))	{ 	

		duration= request.getParameter("duration");

		protocolId= request.getParameter("protocolId");

   		calledFrom= request.getParameter("calledFrom");

		mode= request.getParameter("mode");

		calStatus= request.getParameter("calStatus");



		eventId=request.getParameter("eventId"); 	

		int evId=EJBUtil.stringToNum(eventId); 	



		String eventType=request.getParameter("eventType");		 

		int ret=0;

		

		String delMode=request.getParameter("delMode");

		if (delMode==null) {

			delMode="final";

%>

	<FORM name="deleteEvent" id="delEvtCat" method="post" action="eventdelete.jsp" onSubmit="if (validate(document.deleteEvent)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

<br><br>
	
	<TABLE width="98%" cellspacing="0" cellpadding="0" >
		<tr><td align="center">
		<b><%=MC.M_PlsEtrEsign_Del%><%--Please enter e-Signature to proceed with deletion*****--%> </b>
		</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
	
		</table>
		<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="delEvtCat"/>
			<jsp:param name="showDiscard" value="N"/>
		</jsp:include>


 	 <input type="hidden" name="delMode" value="<%=delMode%>">

  	 <input type="hidden" name="srcmenu" value="<%=src%>">

  	 <input type="hidden" name="eventId" value="<%=eventId%>">

  	 <input type="hidden" name="duration" value="<%=duration%>">

  	 <input type="hidden" name="protocolId" value="<%=protocolId%>">

  	 <input type="hidden" name="calledFrom" value="<%=calledFrom%>">

  	 <input type="hidden" name="mode" value="<%=mode%>">

  	 <input type="hidden" name="calStatus" value="<%=calStatus%>">

  	 <input type="hidden" name="eventType" value="<%=eventType%>">		 	

	 

	</FORM>

<%

	} else {

			String eSign = request.getParameter("eSign");	

			String oldESign = (String) tSession.getValue("eSign");

			if(!oldESign.equals(eSign)) {

%>

	 		  <jsp:include page="incorrectesign.jsp" flush="true"/>	

<%

			} else {

	

		

		if (eventType.equals("L")) {
			// Modified for INF-18183 ::: Ankit
			ret = eventdefB.removeEventdefHeader(evId,AuditUtils.createArgs(session,"",LC.L_Evt_Lib));  

		} else {
			// Modified for INF-18183 ::: Ankit
			ret = eventdefB.removeEventdefChild(evId,AuditUtils.createArgs(session,"",LC.L_Evt_Lib));  

		}



		if (ret==-1) {%>



			<br><br><br><br><br><br>



			<table width=100%>

				<tr>

				<td align=center>



				<p class = "successfulmsg">

					<% if (eventType.equals("L")) {%>

						<%=MC.M_Cat_CntDel%><%--The Category cannot be deleted.*****--%>

					<%} else { %>

						<%=MC.M_Evt_CntDel%><%--The Event cannot be deleted.*****--%>

					<%}%>

				</p>

				</td>

				</tr>



				<tr height=20></tr>

				<tr>

				<td align=center>

				<%if (calledFrom.equals("L")){%>

					<A href="eventlibrary.jsp?srcmenu=<%=src%>&selectedTab=2&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>" type="submit"><%=LC.L_Back%></A>

				<%}else {%>

					<A href="eventbrowser.jsp?srcmenu=<%=src%>&selectedTab=2&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>" type="submit"><%=LC.L_Back%></A>				

				<%}%>					

				</td>		

				</tr>		

			</table>				

		<%}else { %>

			<br><br><br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_Data_DelSucc%><%--Data deleted successfully.*****--%> </p>

		<%	if (calledFrom.equals("L")){%>

				<META HTTP-EQUIV=Refresh CONTENT="1; URL=eventlibrary.jsp?srcmenu=<%=src%>&selectedTab=2&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%> ">

			<%}else {%>

				<META HTTP-EQUIV=Refresh CONTENT="1; URL=eventbrowser.jsp?srcmenu=<%=src%>&selectedTab=2&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%> ">

			<%}

		}

		} //end esign

	} //end of delMode	

	 }//end of if body for session 

else { %>

 <jsp:include page="timeout.html" flush="true"/> 

 <% } %>

 <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

  

</DIV>



<div class ="mainMenu" id = "emenu">

<jsp:include page="getmenu.jsp" flush="true"/> 

</div>



</body>

</HTML>





