<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Update_MultiSch%><%--Update Multiple Schedules*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
<SCRIPT Language="javascript">


  function  validateForm(formobj){
  	if (!(validate_col('select action',formobj.multsch))) return false
  }



 function  validate(formobj){

	if (!(validate_col('e-Signature',formobj.eSign))) return false

	<%-- if(isNaN(formobj.eSign.value) == true) {
	    alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	    formobj.eSign.focus();
	    return false;
    } --%>

    /*
    if(formobj.check1.checked == false && formobj.check2.checked == false && formobj.check3.checked == false && formobj.check4.checked == false) {
       alert("For the successful submission of this page, at least one check box must be selected");
       return false;
    }
   */

    if(formobj.optVal.value == "1") {
	  if(formobj.discSchCal.value == '' || formobj.discDate.value == '' || formobj.discReason.value == '' ) {
		  alert("<%=MC.M_FldMustFill_ForSelOpt%>");/*alert("Corresponding fields must be filled out for the selected option");*****/

		  /*if(formobj.discSchCal.value == '') {
		     formobj.discSchCal.focus();
		     return false;
		  }

		  if(formobj.discDate.value == '') {
		     formobj.discDate.focus();
		     return false;
		  }

		  if(formobj.discReason.value == '') {
		     formobj.discReason.focus();
		     return false;
		  }*/

		  return false;
	  }

	  if (formobj.newschcal.value != '' && formobj.newSchStartDate.value == '')
	  {
	     alert("<%=MC.M_PatSchStartDt_ForSch%>");/*alert("Select <%=LC.Pat_Patient%> Schedule Start Date for New Schedules");*****/
	     return false;
	  }

	  if (formobj.newSchStartDate.value != '' && formobj.newschcal.value == '')
	  {
	     alert("<%=MC.M_SelCal_ForNewSch%>");/*alert("Select a Calendar for a New Schedule");*****/
	     return false;
	  }

   }





   if(formobj.optVal.value=="2") {
       if( formobj.dateOccur.value == '' || formobj.allevedate.value == '' || formobj.dstatusDisc.value == '' || formobj.allevestatdate.value == '' ||formobj.eveStatDiscCal.value == ''){
		  alert("<%=MC.M_FldMustFill_ForSelOpt%>");/*alert("Corresponding fields must be filled out for the selected option");*****/
		  return false;
	  }
   }

	if(formobj.optVal.value=="3") {
       if( formobj.dateOccur1.value == '' || formobj.eventStatCurrCal.value == '' || formobj.alleventdate.value == '' || formobj.dstatusCurr.value == '' ||formobj.alleventstatdate.value == ''){
		  alert("<%=MC.M_FldMustFill_ForSelOpt%>");/*alert("Corresponding fields must be filled out for the selected option");*****/
		  return false;
	  }
   }


   /*if(formobj.check4.checked) {
       if( formobj.newSchAllCal.value == '' || formobj.patschstdate.value == '' || formobj.patenrolldtcheck.checked == false){
		  alert("Corresponding fields must be filled out for the selected checkbox");
		  return false;
	  }
   }*/

    if(formobj.optVal.value=="4") {
       if( formobj.newSchAllCal.value == ''){
    	   alert("<%=MC.M_FldMustFill_ForSelOpt%>");/*alert("Corresponding fields must be filled out for the selected option");*****/
		  return false;
	   }

	   if( formobj.newSchAllCal.value != ''){
	       if(formobj.patschstdate.value == '' && formobj.patenrolldtcheck.checked == false) {
	          alert("<%=MC.M_PatSch_StartDt%>");/*alert("Select <%=LC.Pat_Patient%> Schedule Start Date Or, Use <%=LC.Pat_Patient%>'s Enrollment Date as Scheduled Start Date");*****/
	          return false;
		   }

	   }

	   if( formobj.newSchAllCal.value != ''){
	       if(formobj.patschstdate.value != '' && formobj.patenrolldtcheck.checked) {
	          alert("<%=MC.M_SelPatSch_PatEnrlDt%>");/*alert("Select either <%=LC.Pat_Patient%> Schedule Start Date Or, Use <%=LC.Pat_Patient%>'s Enrollment Date as Scheduled Start Date");*****/
	          return false;
		   }

	   }

	}

 	/*

 	if( (formobj.discSchCal.value != '' || formobj.discDate.value != '' || formobj.discReason.value != '' || formobj.newschcal.value != '' ||formobj.newSchStartDate.value != '') && (formobj.check1.checked == false)){
        alert("Corresponding checkbox must be checked for the fields filled");
        return false;
    }



    if( (formobj.dateOccur.value != '' || formobj.allevedate.value != '' || formobj.dstatusDisc.value != '' || formobj.allevestatdate.value != '' ||formobj.eveStatDiscCal.value != '') && (formobj.check2.checked == false)){
		 alert("Corresponding checkbox must be checked for the fields filled");
		  return false;
	}


    if( (formobj.dateOccur1.value != '' || formobj.eventStatCurrCal.value != '' || formobj.alleventdate.value != '' || formobj.dstatusCurr.value != '' ||formobj.alleventstatdate.value != '') && (formobj.check3.checked == false)){
		  alert("Corresponding checkbox must be checked for the fields filled");
		  return false;
	  }


    if( (formobj.newSchAllCal.value != '' || formobj.patschstdate.value != '' || formobj.patenrolldtcheck.checked == true) && (formobj.check4.checked == false) ) {
	   alert("Corresponding checkbox must be checked for the fields filled");
	   return false;
	}

	*/
 }



   function openWindow(formobj){

    if(formobj.patenrolldtcheck.checked){
		 formobj.patschstdate.value = '';
	 }

   }

   function openLookup(formobj,url,filter) {

		var tempValue;
		var urlL="";
		formobj.target="Lookup";
		formobj.method="post";
		urlL="getlookup.jsp?"+url;
		if (filter.length>0){

			if (filter.indexOf('updatemultschedules')>=0){

	  			tempValue="";
	  			tempValue=formobj.paramstudyId.value;

	  				if ((tempValue == 'null') || tempValue == '0'){
	  					alert("<%=MC.M_NoCalAval_ForPortal%>");/*alert("There is no Calendar available for this portal");*****/
	   				return;
	    				}else{
	   					filter += tempValue;
					}
	  		}

			urlL=urlL+"&"+filter;
	 	}
		formobj.action=urlL;

		formWin =open('donotdelete.html','Lookup','resizable=1,status=0, width=850,height=550 top=100,left=100,menubar=no,scrollbars=1');
		if (formWin && !formWin.closed) formWin.focus();
		formobj.submit();
		formobj.target="";
		formobj.action="updatemultipleschedules.jsp";
		void(0);
	}


</SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import = "java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.esch.business.common.*"%>
<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>


<% String src;

src= request.getParameter("srcmenu");

%>

<body>
<br>
<DIV class="popDefault" id="div1">.
  <%

	String from = request.getParameter("from");

	HttpSession tSession = request.getSession(true);


	if (sessionmaint.isValidSession(tSession))
	{
 %>
  <jsp:include page="sessionlogging.jsp" flush="true"/>
	 <jsp:include page="include.jsp" flush="true"/>

  <%
  		String ipAdd = (String) tSession.getValue("ipAdd");
		String usr = (String) tSession.getValue("userId");
		String study = (String) tSession.getValue("studyId");

		SchCodeDao cd = new SchCodeDao();
		String dEvStatDisc = "";
		String dEvStatCurr = "";
		String dEvStatDisct ="";
		String dEvStatGen ="";



		String evParmStatus = request.getParameter("dstatus");
		int tempStat = EJBUtil.stringToNum(evParmStatus);


		String flag =request.getParameter("flag");//KM


		if( flag == null)
			flag ="";


		cd.getCodeValues("eventstatus",0);
		//dEvStat	 =  cd.toPullDown("status");
		StringBuffer mainStr = new StringBuffer();
		StringBuffer mainStrg = new StringBuffer();
		StringBuffer discStr = new StringBuffer();
		StringBuffer genStr = new StringBuffer();

		int cdcounter = 0;
	 	mainStr.append("<SELECT NAME='dstatusDisc'><Option value='' Selected>"+LC.L_Select_AnOption/*Select an Option*****/+"</option>") ;
		for (cdcounter = 0; cdcounter <= cd.getCDesc().size() -1 ; cdcounter++)
		{
			mainStr.append("<OPTION value = "+ cd.getCId().get(cdcounter)+">" + cd.getCDesc().get(cdcounter)+ "</OPTION>");
		}

		mainStr.append("</SELECT>");
		dEvStatDisc = mainStr.toString();


		cdcounter = 0;
		mainStrg.append("<SELECT NAME='dstatusCurr'><Option value='' Selected>"+LC.L_Select_AnOption/*Select an Option*****/+"</option>") ;
		for (cdcounter = 0; cdcounter <= cd.getCDesc().size() -1 ; cdcounter++)
		{
				mainStrg.append("<OPTION value = "+ cd.getCId().get(cdcounter)+">" + cd.getCDesc().get(cdcounter)+ "</OPTION>");
		}

		mainStrg.append("</SELECT>");
		dEvStatCurr = mainStrg.toString();


		cdcounter = 0;
		discStr.append("<SELECT NAME='dstatusDiscon'><Option value='' Selected>"+LC.L_Select_AnOption/*Select an Option*****/+"</option>") ;
		for (cdcounter = 0; cdcounter <= cd.getCDesc().size() -1 ; cdcounter++)
		{
				discStr.append("<OPTION value = "+ cd.getCId().get(cdcounter)+">" + cd.getCDesc().get(cdcounter)+ "</OPTION>");
		}

		discStr.append("</SELECT>");
		dEvStatDisct = discStr.toString();


		cdcounter = 0;
		genStr.append("<SELECT NAME='dstatusGen'><Option value='' Selected>"+LC.L_Select_AnOption/*Select an Option*****/+"</option>") ;
		for (cdcounter = 0; cdcounter <= cd.getCDesc().size() -1 ; cdcounter++)
		{
				genStr.append("<OPTION value = "+ cd.getCId().get(cdcounter)+">" + cd.getCDesc().get(cdcounter)+ "</OPTION>");
		}

		genStr.append("</SELECT>");
		dEvStatGen = genStr.toString();


%>
<P class="SectionHeadings"> <%=LC.L_Update_MultiSch%><%--Update Multiple Schedules*****--%> </P>

<%

if(from.equals("initial") || flag.equals("1"))
{

  String smenu = request.getParameter("srcmenu");
  String frm = LC.L_Final;/*String frm = "final";*****/
  String checkStr =LC.L_Selected_Lower;/*String checkStr ="selected";*****/
  String checkStr1 ="";
  String checkStr2 ="";
  String checkStr3 ="";
  String checkStr4 ="";
  String opVal = "";

  if (flag.equals("1")) {
	  checkStr ="";
	  opVal = request.getParameter("multsch");
	  if(opVal.equals("1"))
		  checkStr1 =LC.L_Selected_Lower;/*checkStr1 ="selected";*****/
	  if(opVal.equals("2"))
		  checkStr2 =LC.L_Selected_Lower;/*checkStr2 ="selected";*****/
	  if(opVal.equals("3"))
		  checkStr3 =LC.L_Selected_Lower;/*checkStr3 ="selected";*****/
	  if(opVal.equals("4"))
		  checkStr4 =LC.L_Selected_Lower;/*checkStr4 ="selected";*****/

  }
%>
<Form name="multSchForm" method="post" action="updatemultschedules.jsp" onsubmit ="return validateForm(document.multSchForm)">
<TABLE width="100%" cellspacing="0" cellpadding="0" >

<tr>
<td align="right"><%=MC.M_SelcAction_ToPerformed%><%--Select Action to be Performed*****--%>: <FONT class="Mandatory">* </FONT></td>
<td align="center"><select name="multsch">
			<option value="" <%=checkStr%>><%=LC.L_Select_AnOption%><%--Select an Option*****--%></option>
			<option value="1" <%=checkStr1%>> <%=MC.M_DiscontSchFor_PatToCal%><%--Discontinue Schedules for all  <%=LC.Pat_Patients%> assigned to a Calendar*****--%> </option>
			<option value="2" <%=checkStr2%>> <%=MC.M_UpdtEvtIn_DiscontPatSch%><%--Update Event Status in Discontinued <%=LC.Pat_Patient%> Schedules for a Calendar*****--%> </option>
			<option value="3" <%=checkStr3%>> <%=MC.M_UpdtEvtStat_CurPatSch%><%--Update Event Status in Current <%=LC.Pat_Patient%> Schedules for a Calendar*****--%> </option>
			<option value="4" <%=checkStr4%>> <%=MC.M_GenrNewSch_AllPatCal%><%--Generate a new Schedule for all <%=LC.Pat_Patients%> for a Calendar******--%> </option>
</select>
</td>
<td align="left">
<button type="submit"><%=LC.L_Go%></button>
</td>
</tr>
</TABLE>
<input type="hidden" name="srcmenu" value=<%=smenu%>>
<input type="hidden" name="from" value=<%=frm%>>
<input type="hidden" name="flag" value="1"> <!-- KM -->
</Form>
<%}

if (flag.equals("1")){
String optVal = request.getParameter("multsch");


%>

<Form name="multSchedules" id="multSchedules" method="post" action="updatemultipleschedules.jsp" onsubmit ="if (validate(document.multSchedules)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
<input type="hidden" name ="paramstudyId" value=<%=study%> >

<input type="hidden" name="paramprotCalId1" value="">
<input type="hidden" name="paramprotCalId2" value="0">
<input type="hidden" name="paramprotCalId3" value="">
<input type="hidden" name="paramprotCalId4" value="">
<input type="hidden" name="paramprotCalId5" value="">



<TABLE width="100%" cellspacing="5" cellpadding="0" >

		<% if(optVal.equals("1")) { %>
		<tr><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td></tr>
		<tr><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td></tr>
		<tr><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td></tr>

		<tr>
			<td width="55%" align="right">
			<!--  input type="checkbox" name="check1" --> <%=MC.M_DiscontSchFor_PatToCal%><%--Discontinue Schedules for all  <%=LC.Pat_Patients%> assigned to a Calendar*****--%>: &nbsp;&nbsp;&nbsp;&nbsp;
			</td>
			<td width="45%"> <input type="text" name="discSchCal" size="40" READONLY> <A href="javascript:void(0);" onClick="return openLookup(document.multSchedules,'viewId=6016&form=multSchedules&seperator=,&defaultvalue=[ALL]&keyword=discSchCal|name~paramprotCalId1|event_id|[VELHIDE]','dfilter=updatemultschedules|')"><%=LC.L_Select_Cal%><%--Select Calendar*****--%></A> </td>
		</tr>
		<tr>
			<td width="55%" align="right"> <%=LC.L_Discont_Date%><%--Discontinuation Date*****--%>:   &nbsp;&nbsp;&nbsp;&nbsp; </td>
<%-- INF-20084 Datepicker-- AGodara --%>			
			<td width="45%"><input type="text"  name="discDate" class="datefield" size="21" MAXLENGTH =20 READONLY></td>
		</tr>


		<tr>
			<td width="55%" align="right"> <%=LC.L_Discont_Reason%><%--Discontinuation Reason*****--%>: &nbsp;&nbsp;&nbsp;&nbsp;  </td>
			<td width="45%"> <input type="text" name="discReason" size="40"></td>
		</tr>

		<tr>
			<td width="55%" align="right"><%=MC.M_UpldDiscntCal_EvtDt%><%--Update Event Status in all Discontinued Schedules of this &nbsp;&nbsp;&nbsp;&nbsp;  <br> Calendar(for Events on and after Discontinuation Date)*****--%>: &nbsp;&nbsp;&nbsp;&nbsp;  </td>
			<td width="45%"> <%=dEvStatDisct%> </td>
		</tr>

		<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
		<tr>
			<td width="55%" align="right"> <%=MC.M_GenNewSch_PatCal%><%-- And Generate New Schedules for these <%=LC.Pat_Patients%> with Calendar*****--%>:   &nbsp;&nbsp;&nbsp;&nbsp; </td>
			<td width="45%"> <input type="text" name="newschcal" size="40" READONLY> <A href="javascript:void(0);" onClick="return openLookup(document.multSchedules,'viewId=6016&form=multSchedules&seperator=,&defaultvalue=[ALL]&keyword=newschcal|name~paramprotCalId2|event_id|[VELHIDE]','dfilter=updatemultschedules|')"><%=LC.L_Select_Cal%><%--Select Calendar*****--%></A></td>
		</tr>
		<tr>
			<td width="55%" align="right"> <%=MC.M_StartDt_PatNewSchs%><%--<%=LC.Pat_Patient%> Schedule Start Date for New Schedules--%>:   &nbsp;&nbsp;&nbsp;&nbsp; </td>
			<td width="45%">
			<select name="newSchStartDate">
			<option selected value=""><%=LC.L_Select_AnOption%><%--Select an Option*****--%></option>
			<option value="S"><%=MC.M_SameStartDt_DisconPatSch%><%--Same as Start Date of <%=LC.Pat_Patient%>'s Schedule being Discontinued*****--%> </option>
			<option value="E"><%=MC.M_SameAs_PatEnrlDt%><%--Same as <%=LC.Pat_Patient%>'s Enrollment Date*****--%> </option>
			</select>
			</td>
		</tr>

		<tr>
			<td width="55%" align="right"> <%=MC.M_UpldCurCal_EvtDiscnt%><%-- Update Event Status in all Current Schedules of this calendar &nbsp;&nbsp;&nbsp;&nbsp; <br>(for Events before Discontinuation Date)*****--%>: &nbsp;&nbsp;&nbsp;&nbsp;  </td>
			<td width="45%"><%=dEvStatGen%> </td>
		</tr>

		<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
		<input type ="hidden" name="optVal" value =<%=optVal%>>

		<% }

		 if(optVal.equals("2")) {
		 %>

		<tr><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td></tr>
		<tr><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td></tr>
		<tr><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td></tr>


		<tr>
			<td width="55%" align="right">
			<!--  input type="checkbox" name="check2"--> <%=MC.M_UpdtEvt_PatSchForCal%><%--Update Event Status in Discontinued <%=LC.Pat_Patient%> Schedules for Calendar*****--%>:  &nbsp;&nbsp;&nbsp;&nbsp;
			</td>
			<td width="55%"> <input type="text" name="eveStatDiscCal" size="40" READONLY> <A href="javascript:void(0);" onClick="return openLookup(document.multSchedules,'viewId=6016&form=multSchedules&seperator=,&defaultvalue=[ALL]&keyword=eveStatDiscCal|name~paramprotCalId3|event_id|[VELHIDE]','dfilter=updatemultschedules|')"><%=LC.L_Select_Cal%><%--Select Calendar*****--%></A> </td>
		</tr>

		<tr>
			<td width="55%" align="right"> <%=MC.M_UpdtAll_EvtsSchd%><%--Update all Events scheduled to occur*****--%> &nbsp;

			<select name="dateOccur">
			<option selected value=""> <%=LC.L_Select_AnOption%><%--Select an Option*****--%> </option>
			<option value='B'> <%=LC.L_Before%><%--Before*****--%> </option>
			<option value='OB'> <%=LC.L_On_AndBefore%><%--On and Before*****--%></option>
			<option value='A'> <%=LC.L_After%><%--After*****--%> </option>
			<option value='OA'> <%=LC.L_On_AndAfter%><%--On and After*****--%> </option>
			</select>
			  &nbsp;&nbsp;&nbsp;&nbsp;</td>
<%-- INF-20084 Datepicker-- AGodara --%>			  
			<td width="45%"><input type="text" name="allevedate" class="datefield" size="21" READONLY></td>

		</tr>

		<tr>
			<td width="55%" align="right"> <%=MC.M_UpdtEvt_WithStat%><%--Update these Events with Event Status*****--%>:&nbsp;&nbsp;&nbsp;&nbsp; </td>
			<td width="45%"> <%=dEvStatDisc%> </td></td>
		</tr>

		<tr>
			<td width="55%" align="right"> <%=MC.M_Evt_StatusDate%><%--and Event Status Date*****--%>:&nbsp;&nbsp;&nbsp;&nbsp; </td>
<%-- INF-20084 Datepicker-- AGodara --%>	
			<td width="45%"><input type="text" name="allevestatdate" class="datefield" size="21" READONLY></td>
		</tr>

		<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
		<input type ="hidden" name="optVal" value =<%=optVal%>>

		<% }

		 if(optVal.equals("3")) {
		 %>

		<tr><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td></tr>
		<tr><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td></tr>
		<tr><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td></tr>


		<tr>
			<td width="55%" align="right">
			<!--  input type="checkbox" name="check3"--> <%=MC.M_UpldEvtCur_PatSchCal%><%-- Update Event Status in Current <%=LC.Pat_Patient%> Schedules for Calendar*****--%>: &nbsp;&nbsp;&nbsp;&nbsp;
			</td>
			<td width="45%"> <input type="text" name="eventStatCurrCal" size="40" READONLY> <A href="javascript:void(0);" onClick="return openLookup(document.multSchedules,'viewId=6016&form=multSchedules&seperator=,&defaultvalue=[ALL]&keyword=eventStatCurrCal|name~paramprotCalId4|event_id|[VELHIDE]','dfilter=updatemultschedules|')"><%=LC.L_Select_Cal%><%--Select Calendar*****--%></A> </td>
		</tr>

		<tr>
			<td width="55%" align="right"> <%=MC.M_UpdtAll_EvtsSchd%><%--Update all Events scheduled to occur*****--%> &nbsp;

			<select name="dateOccur1">
			<option selected value=""> <%=LC.L_Select_AnOption%><%--Select an Option*****--%> </option>
			<option value='B'> <%=LC.L_Before%><%--Before*****--%> </option>
			<option value='OB'> <%=LC.L_On_AndBefore%><%--On and Before*****--%></option>
			<option value='A'> <%=LC.L_After%><%--After*****--%> </option>
			<option value='OA'> <%=LC.L_On_AndAfter%><%--On and After*****--%> </option>
			</select>

			&nbsp;&nbsp;&nbsp;&nbsp;  </td>
<%-- INF-20084 Datepicker-- AGodara --%>			
			<td width="45%"><input type="text" name="alleventdate" class="datefield" size="21" READONLY></td>
		</tr>

		<tr>
			<td width="55%" align="right"> <%=MC.M_UpdtEvt_WithStat%><%--Update these Events with Event Status*****--%>:&nbsp;&nbsp;&nbsp;&nbsp; </td>
			<td width="45%"> <%=dEvStatCurr%></td>
		</tr>

		<tr>
			<td width="55%" align="right"> <%=MC.M_Evt_StatusDate%><%--and Event Status Date*****--%>:&nbsp;&nbsp;&nbsp;&nbsp; </td>
<%-- INF-20084 Datepicker-- AGodara --%>
			<td width="45%"><input type="text" name="alleventstatdate" class="datefield" size="21" READONLY></td>
			</tr>


		<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
		<input type ="hidden" name="optVal" value =<%=optVal%>>

		<% }

		 if(optVal.equals("4")) {
		 %>
		<tr><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td></tr>
		<tr><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td></tr>
		<tr><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td></tr>

		<tr>
			<td width="55%" align="right">
			<!--  input type="checkbox" name="check4"--> <%=MC.M_GenNewSch_ForCal%><%--Generate a new Schedule for all <%=LC.Pat_Patients%> (without a current Schedule) &nbsp;&nbsp;&nbsp;<br>for Calendar*****--%>: &nbsp;&nbsp;&nbsp;
			</td>
			<td width="45%"> <input type="text" name="newSchAllCal" size="40" READONLY> <A href="javascript:void(0);" onClick="return openLookup(document.multSchedules,'viewId=6016&form=multSchedules&seperator=,&defaultvalue=[ALL]&keyword=newSchAllCal|name~paramprotCalId5|event_id|[VELHIDE]','dfilter=updatemultschedules|')"><%=LC.L_Select_Cal%><%--Select Calendar*****--%></A> </td>
		</tr>
		<tr>
			<td width="55%" align="right"> <%=MC.M_PatSch_StrtDt%><%--<%=LC.Pat_Patient%> Schedule Start Date*****--%>:&nbsp;&nbsp;&nbsp;&nbsp; </td>
<%-- INF-20084 Datepicker-- AGodara --%>
			<td width="45%"><input type="text" name="patschstdate" class="datefield" size="21" READONLY></td>
		</tr>


		<tr>
			<td width="55%" align="right"> <%=MC.M_OrPatEnrl_SchStDt%><%--Or, Use <%=LC.Pat_Patient%>'s Enrollment Date as Scheduled Start Date*****--%>:&nbsp;&nbsp;&nbsp;&nbsp; </td>
			<td width="45%"> <input type="checkbox" name="patenrolldtcheck" onClick = "return openWindow(document.multSchedules)"> </td>
		</tr>



		<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
		<input type ="hidden" name="optVal" value =<%=optVal%>>
		<%} %>

		</table>
		<table width="100%" cellspacing="0" cellpadding="0" >




		<jsp:include page="submitBar.jsp" flush="true">
				<jsp:param name="displayESign" value="Y"/>
				<jsp:param name="formID" value="multSchedules"/>
				<jsp:param name="showDiscard" value="N"/>
		</jsp:include>


		<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
		<% if(optVal.equals("1") || optVal.equals("4")) { %>
		<tr><td> <p class="defComments"> <%=MC.M_NtPatEnrl_SelApplStat%><%--Note: Wherever <%=LC.Pat_Patient_Lower%> enrollment date is selected as an option it's applicable to <%=LC.Pat_Patients_Lower%> having an enrolled status.*****--%></p></td></tr>
	    <%} %>
 </table>
 <%} %>
</form>

<%


} else {  //else of if body for session

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>

</div>

<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>

</body>

</html>


