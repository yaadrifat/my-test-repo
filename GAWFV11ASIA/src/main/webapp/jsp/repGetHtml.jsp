<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.EJBUtil"%>

<%@page import="com.velos.eres.service.util.StringUtil"%><html>
	<body>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/><%@page import="com.velos.eres.service.util.LC"%><%@page import="com.velos.eres.service.util.MC"%>


<%@ page language = "java" import="java.io.*"%>
<%
   HttpSession tSession = request.getSession(true);
   if (null == request.getHeader("referer")) {
%>
<h1><%=LC.L_Forbidden%><%--Forbidden*****--%></h1>
<p><%=MC.M_NotPerm_ToAcesServer%><%--You do not have permission to access this server or file.*****--%></p>
</body>
</html>
<%
            return;
   }

      if (sessionmaint.isValidSession(tSession))
    {


		String htmlFileName=request.getParameter("htmlFileName");
		String filePath=request.getParameter("filePath");
		String fileDnPath=request.getParameter("fileDnPath");
		Integer repId = new Integer((request.getParameter("repId")==null) ? "0" : request.getParameter("repId"));
		String calledfrom = request.getParameter("calledfrom");
		if(EJBUtil.isEmpty(calledfrom)){
			calledfrom = LC.L_Rpt_Central;
		}
		String requestURL 	= StringUtil.encodeString(request.getRequestURL().toString());
		String queryString 	= StringUtil.encodeString(request.getQueryString());

		String path=  fileDnPath + "?file=" + htmlFileName +"&mod=R&repId="+repId+"&moduleName="+calledfrom+"&filePath="+StringUtil.encodeString(filePath)+"&requestURL="+requestURL+"&queryString="+queryString;
	
%>	
<%=MC.M_Close_AfterDld%><%--Please close after downloading*****--%>....
	 
	<jsp:include page="exportData.jsp" flush="true"> 
	<jsp:param name="url" value="<%=path%>"/>
	</jsp:include>   
 
<%
		}
		
%>
	   </body>
	</html>
	