<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<%
	String fromPage = request.getParameter("fromPage");

	if (fromPage.equals("eventbrowser") || fromPage.equals("eventlibrary") || fromPage.equals("patientschedule") ) {		
	%>
		<title> <%=MC.M_EvtLib_ApdxEvtUrl%><%--Event Library >> Event Appendix >> Event Url*****--%> </title>
	<%
	} else {
		if(fromPage.equals("selectNetwork")){%>
		<title>Network URL Page</title>	
	<%
	}else if(fromPage.equals("userDetails"))
		{%>
		<title><%=LC.L_Usr_URLDets%></title>
		<%}
		  else if(fromPage.equals("siteDetails"))
	 	{%>
		<title><%=LC.L_Org_URLDets%></title>
		<%}
		  else{
	 			%>
	 				<title><%=LC.L_Evt_UrlPage%><%--Event Url Page*****--%></title>	
	 			<%
	 	}
	}
	
%>



<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>

<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">



</head>



<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<!--Virendra: Fixed Bug no. 4734, added com.velos.eres.service.util.StringUtil  -->
<%@ page language = "java" import = "com.velos.eres.business.common.*,com.velos.eres.service.util.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.esch.business.common.EventInfoDao,com.velos.eres.service.util.StringUtil"%>

<jsp:useBean id="eventdocB" scope="request" class="com.velos.esch.web.eventdoc.EventdocJB"/>

<%if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt")||(request.getParameter("fromPage")).equals("selectNetwork")||(request.getParameter("fromPage")).equals("siteDetails")||(request.getParameter("fromPage")).equals("userDetails"))

{ %>



<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<!-- YK 05Aug11: Removed for Bug #6728 already using Whichcss_skin.js in panel.jsp -->





 <% } //end for if ((request.getParameter("fromPage")).equals("selectEvent"))



	%>



<SCRIPT Language="javascript">



 function  validate(formobj){



//     formobj=document.addurl;

	 if(formobj.desc.value.length>500){
    	 alert("<%=MC.M_ShortDesc_MaxCharsAllwd%>");/*alert(" 'Short Description' exceeded maximum number of characters allowed.");*****/
	     formobj.desc.focus();
	     return false;
     } 
     if (!(checkquote(formobj.desc.value))) return false 

     if (!(validate_col('URL',formobj.name))) return false

     if (!(validate_col('Description',formobj.desc))) return false

     if (!(validate_col('eSign',formobj.eSign))) return false





	<%-- if(isNaN(formobj.eSign.value) == true) {

	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect E-Signature. Please enter again");*****/

	formobj.eSign.focus();

	return false;

   } --%>





}





</SCRIPT>





<% String src;



src= request.getParameter("srcmenu");

//String eventName= request.getParameter("eventName");
String eventName= "";



%>



<% if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt")||(request.getParameter("fromPage")).equals("selectNetwork")||(request.getParameter("fromPage")).equals("siteDetails")||(request.getParameter("fromPage")).equals("userDetails")){

%>
<jsp:include page="include.jsp" flush="true"/>
<%


}

 else{

%>

<jsp:include page="panel.jsp" flush="true">



<jsp:param name="src" value="<%=src%>"/>



</jsp:include>

<%}%>





<body>



<% if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt")||(request.getParameter("fromPage")).equals("selectNetwork")||(request.getParameter("fromPage")).equals("siteDetails")||(request.getParameter("fromPage")).equals("userDetails")){%>

		<DIV class="popDefault" id="div1">
		<br>
	<%	}

else { %>

<DIV class="formDefault" id="div1">

<%}%>

<%

	String duration = request.getParameter("duration");

	String protocolId = request.getParameter("protocolId");

	String calledFrom = request.getParameter("calledFrom");

	String eventId = request.getParameter("eventId");

	String docmode = request.getParameter("docmode");

	String mode = request.getParameter("mode");

	String calStatus = request.getParameter("calStatus");

	String eventmode = request.getParameter("eventmode");

	String displayDur=request.getParameter("displayDur");

	String displayType=request.getParameter("displayType");
	
	String networkId=(request.getParameter("networkId")==null)?"":request.getParameter("networkId");
	
	String siteId=(request.getParameter("siteId")==null)?"":request.getParameter("siteId");
	
	String userPk = (request.getParameter("userPk")==null)?"":request.getParameter("userPk");
	
	String selectedTab=(request.getParameter("selectedTab")==null)?"":request.getParameter("selectedTab");
	
	String networkFlag=(request.getParameter("networkFlag")==null)?"":request.getParameter("networkFlag");
	
	String networkIdflag=(request.getParameter("networkIdflag")==null)?"":request.getParameter("networkIdflag");


	String eventdoc = "";

	String eventdocDesc= "";

	String eventdocId = "";
	//Virendra: Fixed Bug no. 4734, added int addAnotherWidth = 0;
	 int addAnotherWidth = 0;



	HttpSession tSession = request.getSession(true);



	if (sessionmaint.isValidSession(tSession))

	{

	    String calAssoc = request.getParameter("calassoc");
        calAssoc = (calAssoc == null) ? "" : calAssoc;

	   String uName = (String) tSession.getValue("userName");



	//KM
if(networkFlag.equalsIgnoreCase("")){
	if (calledFrom.equals("P")||calledFrom.equals("L"))
    {
       	  eventdefB.setEvent_id(EJBUtil.stringToNum(eventId));
       	  eventdefB.getEventdefDetails();
          eventName = eventdefB.getName();
//JM: 12Nov2009: #4399
	  	  eventName = (eventName==null)?"":eventName;
	  	  if (eventName.length()>1000){//for the old existing event names > 1000 or larger
	  		eventName=eventName.substring(0,1000);
	  	  }
    }else{
       	  eventassocB.setEvent_id(EJBUtil.stringToNum(eventId));
          eventassocB.getEventAssocDetails();
       	  eventName = eventassocB.getName();
//JM: 12Nov2009: #4399
	  	  eventName = (eventName==null)?"":eventName;
	  	  if (eventName.length()>1000){//for the old existing event names > 1000 or larger
	  		eventName=eventName.substring(0,1000);
	  	  }

     }
}

	if (fromPage.equals("eventbrowser") || fromPage.equals("eventlibrary")|| fromPage.equals("selectNetwork") || fromPage.equals("siteDetails") ||(request.getParameter("fromPage")).equals("userDetails"))
	{
	%>
	<!-- P class="sectionHeadings"> Event Library >> Event Appendix >> Event Url  </P-->
	<%
	}else{
		
		String calName = (String) tSession.getValue("protocolname");
	%>
	<P class="sectionHeadings"> <%Object[] arguments = {calName};%><%=VelosResourceBundle.getMessageString("M_PcolCalEvt_EvtUrl",arguments)%><%--Protocol Calendar [ <%=calName%> ] >> Event Appendix >> Event Url*****--%> </P>
	<%}if(networkFlag.equalsIgnoreCase("")){%>


<jsp:include page="eventtabs.jsp" flush="true">
<jsp:param name="duration" value="<%=duration%>"/>
<jsp:param name="protocolId" value="<%=protocolId%>"/>
<jsp:param name="calledFrom" value="<%=calledFrom%>"/>
<jsp:param name="fromPage" value="<%=fromPage%>"/>
<jsp:param name="mode" value="<%=mode%>"/>
<jsp:param name="displayDur" value="<%=displayDur%>"/>
<jsp:param name="displayType" value="<%=displayType%>"/>
<jsp:param name="eventId" value="<%=eventId%>"/>
<jsp:param name="src" value="<%=src%>"/>
<jsp:param name="eventName" value="<%=eventName%>"/>
<jsp:param name="calassoc" value="<%=calAssoc%>"/>

</jsp:include>

<%}

if(docmode.equals("M")) {

   eventdocId = request.getParameter("docId");

   eventdocB.setDocId(EJBUtil.stringToNum(eventdocId));

   eventdocB.getEventdocDetails();

   eventdocDesc = eventdocB.getDocDesc();

   eventdoc = eventdocB.getDocName();

%>
<form name="addurl" METHOD=POST action="urlsave.jsp?eventdocId=<%=eventdocId%>&docmode=<%=docmode%>&eventId=<%=eventId%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&eventmode=<%=eventmode%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&calassoc=<%=calAssoc%>&&networkId=<%=networkId%>&siteId=<%=siteId%>&selectedTab=<%=selectedTab%>&networkFlag=<%=networkFlag%>&userPk=<%=userPk%>" onsubmit = "return validate(document.addurl)">


<%} else {

%>
<form name="addurl" id="addeveurl" METHOD=POST action="urlsave.jsp?docmode=<%=docmode%>&eventId=<%=eventId%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&eventmode=<%=eventmode%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&calassoc=<%=calAssoc%>&networkId=<%=networkId%>&siteId=<%=siteId%>&selectedTab=<%=selectedTab%>&networkFlag=<%=networkFlag%>&userPk=<%=userPk%>"  onsubmit = "if (validate(document.addurl)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">


<%}

%>
<input type=hidden name=calassoc value=<%=calAssoc%>>
<table width="99%" cellspacing="0" cellpadding="0" border="0" class="midAlign">

      <tr>

        <td>

          <P class = "defComments">
          <%if(fromPage.equals("selectNetwork")){ %>
          Add Links to your Network
          <%}else if((request.getParameter("fromPage")).equals("siteDetails")||(request.getParameter("fromPage")).equals("userDetails")){%>
          <%}else{ %>
           <%=MC.M_AddLnk_ToPcolsEvt%><%--Add Links to your Protocol's Event.*****--%> </P>
	<%}%>
        </td>

      </tr>

    </table>

    <table class="formDefault" width="99%" cellspacing="0" cellpadding="0" border="0" class="midAlign" >

      <tr>

        <td width="35%"> <%=LC.L_Url_Upper%><%--URL*****--%> <FONT class="Mandatory">* </FONT> </td>

        <td width="65%">

          <input type=text name=name value='<%=eventdoc%>' MAXLENGTH=255 size=50>

        </td>

      </tr>

      <tr>

        <td width="35%"> </td>

        <td width="65%">

          <P class="defComments"> <%=MC.M_EtrCpltUrl_255Max%><%--Enter complete url eg. 'http://www.centerwatch.com'

            or 'ftp://ftp.centerwatch.com' (255 char max.)*****--%> </P>

        </td>

      </tr>

      <tr>

        <td class=tdDefault width="35%"> <%=LC.L_Short_Desc%><%--Short Description*****--%> <FONT class="Mandatory" >* </FONT>

        </td>

        <td class=tdDefault width="65%">

          <%-- <input type=text name=desc MAXLENGTH=100 value='<%=eventdocDesc%>' size=40> --%>
        <TextArea type=text name=desc row=3 cols=50 ><%=eventdocDesc%></TextArea>
        </td>

      </tr>

      <tr>

        <td width="35%"> </td>

        <td width="65%">

          <P class="defComments"> <%=MC.M_NameToLnk_500CharMax%><%--Give a friendly name to your link. (500 char

            max.)--%> </P>

        </td>

      </tr>

	  </table>

<jsp:include page="propagateEventUpdate.jsp" flush="true">
<jsp:param name="fromPage" value="<%=fromPage%>"/>
<jsp:param name="formName" value="addurl"/>
<jsp:param name="eventName" value="<%=eventName%>"/>
</jsp:include>

<table width="100%" cellspacing="0" cellpadding="0" border="0" class="midAlign">
	<tr align="center">
		<%if ((request.getParameter("fromPage")).equals("selectEvent")){ %>
		<td>
			<button onclick="window.history.back();"><%=LC.L_Back%></button>
		</td>
		<%}%>
		<!-- Virendra: Fixed Bug no. 4734, added td with addAnotherWidth  -->
		<td bgcolor="<%=StringUtil.eSignBgcolor%>" valign="baseline" align="left" width="85%" <%=addAnotherWidth > 0 ? "" : "colspan=5" %>>
	 		<jsp:include page="submitBar.jsp" flush="true">
				<jsp:param name="displayESign" value="Y"/>
				<jsp:param name="formID" value="addeveurl"/>
				<jsp:param name="showDiscard" value="N"/>
				<jsp:param name="noBR" value="Y"/>
			</jsp:include>
		</td>
		
	</tr>
</table>

</form>



<%



} else {  //else of if body for session



%>

  <jsp:include page="timeout.html" flush="true"/>

  <%



}



%>



  <%if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt")){}

else {

%>




</div>

<div class ="mainMenu" id = "emenu">

<jsp:include page="getmenu.jsp" flush="true"/>

</div>

	<% }%>

 <div class = "myHomebottomPanel">
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>


</body>
</html>





