<%--
This JSP displays a patient schedule categorized by month and then visit
in accordion UI. When the user expands a visit accordion, it makes an AJAX call
to patientschedulebyvisit.jsp which returns the events in that visit.
 --%>
 <%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%!
private final static String ShowLessIcon = "../images/jpg/less.jpg";
private final static String ShowMoreIcon = "../images/jpg/more.jpg";
private final static String ShowThisElem = "display:block";
private final static String HideThisElem = "display:none";
private final static String LastSelVisit = "lastSelVisit";
%>
<%@page import="com.velos.esch.business.common.SchCodeDao"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_Study%><%=LC.L_MngPat_Sch.substring(6,26)%><%--Manage <%=LC.Pat_Patient%> >> Schedule*****--%></title>
<meta http-equiv=expires content="text/html; charset=UTF-8">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="-1">
<meta http-equiv="Cache-Control" content="no-cache">
<meta CONTENT="private" HTTP-EQUIV="CACHE-CONTROL">
<meta CONTENT="no-store" HTTP-EQUIV="CACHE-CONTROL">
<meta CONTENT="must-revalidate" HTTP-EQUIV="CACHE-CONTROL">
<meta CONTENT="post-check=0,pre-check=0" HTTP-EQUIV="CACHE-CONTROL">
<style type="text/css">
	label.simple {
	  font-family:Verdana,Arial,Helvetica,sans-serif;
	  color:black;
	  font-size:8pt;
	}
	tr.pastSchRow TD {
	  font-family: Verdana, Arial, Helvetica,  sans-serif;
	  color:black;
	  font-size:8pt;
	  border-style:groove;
	  border-bottom-width:thin;
	  border-top-width:thin;
	  border-left-width:thin;
	  border-right-width:thin;
	}
</style>
<link rel="stylesheet" href="js/jquery/themes/base/jquery.ui.all.css">
<link rel="stylesheet" href="js/jquery/demos/demos.css">
<script src="validations.js"></script>
<script src="js/velos/velosUtil.js"></script>
<script src="js/jquery/jquery-1.4.4.js"></script>
<script>
<%-- To avoid conflict with Prototype library --%>
jQuery.noConflict();
var $j = jQuery;
</script>
<script src="js/jquery/ui/jquery.ui.core.js"></script>
<script src="js/jquery/ui/jquery.ui.widget.js"></script>
<script src="js/jquery/ui/jquery.ui.mouse.js"></script>
<script src="js/jquery/ui/jquery.ui.draggable.js"></script>
<script src="js/jquery/ui/jquery.ui.position.js"></script>
<script src="js/jquery/ui/jquery.ui.resizable.js"></script>
<script src="js/jquery/ui/jquery.ui.accordion.js"></script>
<script src="js/jquery/ui/jquery.ui.dialog.js"></script>

<script language=javascript>
var isIE6 = isIE6Browser();
var isIE = jQuery.browser.msie;
var eventObjects = [];
var lastTimeSubmitted = 0;
var screenWidth = screen.width;
var screenHeight = screen.height;
<%-- defaultHeight used for the modal dialog --%>
var defaultHeight = navigator.userAgent.indexOf("MSIE") != -1 ? 890 : 500;

<%-- These functions are called by clicking links in patientschedulebyvisit.jsp --%>
/*Akshi : Fixed Bug#7026 */
function callfunc(storageid,eventid,fk_schevents1){
	var pkey='<%=request.getParameter("pkey")%>';   
	var studyId='<%=request.getParameter("studyId")%>';
	windowName =window.open('preparesamplefunc.jsp?srcmenu=tdmenubaritem6&selectedTab=3&pkstoragekit='+storageid+'&pkey='+pkey+'&studyId='+studyId+'&fk_schevents1='+fk_schevents1+'&eventid='+eventid+'&mode=M&prepflag=Y', 'Information2', 'toolbar=no,scrollbar=yes,resizable=yes,menubar=no,status=yes,width=900,height=500, top=100, left=80');
	  windowName.focus();
	}				  
function f_getKit(eventid,fk_sch_events1) {
	spanelem = document.getElementById("spec_"+eventid);
	if(spanelem.style.display == 'block')
	{
		spanelem.style.display = 'none';
	}
	else 
	{
		spanelem.style.display = 'block';
	}
	
			  
																								 
			   
			   
			  
							
   
													 
   
	   
 	new VELOS.ajaxObject("getEventKitForms.jsp", {
   		urlData:"eventId=" + eventid + "&fk_sch_events1="+fk_sch_events1,
		   reqType:"POST",
		   outputElement: "spec_"+eventid }
	).startRequest();
}

function f_hideKit(eventid) {
	spanelem = document.getElementById("spec_"+eventid);
	spanelem.style.display = 'none';
}

//JM: 05May2008, added for #PS16
function fnOpenEventsWindow(formobj, link, vis, pageRight,orgRight, calStatus){
	if (calStatus != 'A') {
	  return false;
	}

    //JM: 07Jul2008, #3541
	if (f_check_perm_org(pageRight,orgRight,'E')) {
		var paramArray = [vis];
		msg = getLocalizedMessageString("M_WantToUnSch_EvtsVisit",paramArray);/*"Do you want to add Unscheduled Event(s) to the Visit:+" "+vis + " ?"*****/
		if(confirm(msg)) {
			windowName=window.open(link,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=1000,height=650,top=100,left=150 ");
			windowName.focus();
			return true;
		} else {
			return false;
		}
	}
}

function openDeleteWindow(patientId,studyId,pageRight,orgRight) {
	if (f_check_perm_org(pageRight,orgRight,'E')) {
	    window.open("deleteschedules.jsp?patientId="+patientId+"&studyId="+studyId,
	        'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=600,height=400,top=100,left=100');
	    return true;
	} else {
	    return false;
	}
}

function setFilter(formobj,forFiltersFlag){
	 /*
		 In this jsp there is submit on 2 events
		 1. on "GO" for initiating the filter search
		 2. on "GENERATE" when a new patient has been added or a new calendar is allotted

		 On pressing "GO"  generateGoFlag gets the value of "P"
		 On pressing "GENERATE" generateGoFlag gets the value of "F"
		 Note that both t182he event can never happen simultaneously . So depending on the value of
		 generateGoFlag we set the value of "generate" variable and depending on that the new schedule is generated
		 or the filter criteria is passed on etc.

	*/
	submitFlag = formobj.generateGoFlag.value ;

	if (    submitFlag == "P"  )
	{
		if (    forFiltersFlag == "P"  )
		{
			//alert("Value" +document.sch.generate.value);
			visitcnt = formobj.visit.options.selectedIndex;
			formobj.visitext.value = formobj.visit.options[visitcnt].text;
			eventscnt =formobj.event.options.selectedIndex;
			formobj.eventext.value = formobj.event.options[eventscnt].text;
			formobj.generate.value = "F";
			statusscnt = formobj.dstatus.options.selectedIndex;
			formobj.statustext.value = formobj.dstatus.options[statusscnt].text;
			mcnt = formobj.month.options.selectedIndex;
			ycnt = formobj.year.options.selectedIndex;
			formobj.monthyrtext.value = formobj.month.options[mcnt].text + "/" + formobj.year.options[ycnt].text ;
		}
		if (    forFiltersFlag == "F"  )
		{
			formobj.generate.value = "F";
		}
		//formobj.submit();
		return true;
	}
	else if (    submitFlag == "G"  )
	{
		formobj.generate.value = "Y";
		 return true ;
	}
}

function fopen(link,pageRight,orgRight)
{
	if (f_check_perm_org(pageRight,orgRight,'E'))
	{
		window.open(link,'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=640,height=500')
		return true;
	}
	else
	{
		return false;
	}
}

function viewevent(link)
{
	window.open(link,'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=600,height=400')
	return true;
}

function editMultipleEvents(link,pageRight,orgRight)
{
	if (f_check_perm_org(pageRight,orgRight,'E'))
	{	window.open(link,'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=1250,height=500,left=150')
		return true;
	}
	else
	{
		return false;
	}
}

function viewschedule(link)
{
	window.open(link,'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=600,height=400')
	return true;
}

function changedate(link,pageRight,orgRight)
{
	if (f_check_perm_org(pageRight,orgRight,'E'))
	{
		window.open(link,'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=450')
		return true;
	} else
	{
		return false;
	}
}

function openWinStatus(patId,patStatPk,pageRight,study,orgRight)
{
	//windowName= window.open("patstudystatus.jsp?changeStatusMode="+changeStatusMode+"&statid=" + patStatPk + "&pkey=" + patId,"patstatusdetails","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=500,height=500");
	//windowName.focus();

	changeStatusMode = "yes";

	if (f_check_perm_org(pageRight,orgRight,'N'))
	{
		windowName= window.open("patstudystatus.jsp?studyId="+study+"&changeStatusMode=yes&statid=" + patStatPk + "&pkey=" + patId,"patstatusdetails","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=550,height=560");
		windowName.focus();
	}
}

function openWinProtocol(pageRight,orgRight, study,patient)
{
	if (f_check_perm_org(pageRight,orgRight,'E'))
	{
		windowName= window.open("patientprotocol.jsp?studyId="+study+"&pkey=" + patient,"patprotocol","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=750,height=500");
		windowName.focus();
		return true;
	}
	else
		{
			return false;
		}

}

function viewlabdetails(link)
{
	window.open(link,'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes');
	return true;
}

function showMoreFilters() {
	var e = document.getElementById('moreFilters');
	if (e.src.indexOf("less.jpg") > -1) {
		document.getElementById('moreFilterTable').style.display = 'none';
		e.src = "<%=ShowMoreIcon%>";
		e.setAttribute("title","<%=MC.M_AddFilters%>");
	} else {
		document.getElementById('moreFilterTable').style.display = 'block';
		e.src = "<%=ShowLessIcon%>";
		e.setAttribute("title","<%=LC.L_ReduceFilters%>");
	}
	return false;
}

var addlParams = { "event": "", "dstatus":"", "year":"0", "month":"0", "date_range":"" }

function setEventInAddlParams(selObj) {
	if (!selObj) { return; }
	addlParams['event'] = selObj[selObj.selectedIndex].value;
}

function setDStatusInAddlParams(selObj) {
	if (!selObj) { return; }
	addlParams['dstatus'] = selObj[selObj.selectedIndex].value;
}

function setYearInAddlParams(selObj) {
	if (!selObj) { return; }
	addlParams['year'] = selObj[selObj.selectedIndex].value;
}

function setMonthInAddlParams(selObj) {
	if (!selObj) { return; }
	addlParams['month'] = selObj[selObj.selectedIndex].value;
}

function setDateRangeDD(fillObj) {
	if (!fillObj) { return; }
	if (fillObj.value == 'ALL') { return; }
	for(var iX=0; iX<$('date_range').options.length; iX++) {
		if ($('date_range').options[iX].value == fillObj.value) {
			$('date_range').selectedIndex = iX;
			break;
		}
	}
}

function setDateRangeInAddlParams(selObj) {
	if (!selObj) { return; }
	addlParams['date_range'] = selObj[selObj.selectedIndex].value;
}

function clearMonthAndYear() {
	document.sch.month.selectedIndex = 0;
	document.sch.year.selectedIndex = 0;
}

function clearDateRange() {
	document.sch.date_range.selectedIndex = 0;
}

function fnOnceEnterKeyPress(e) {
	var evt = (e) || window.event;
    if (!evt) { return 0; }
	try {
        var code = evt.charCode || evt.keyCode;
        if (code == 13 || code == 10) {
            var thisTimeSubmitted = new Date();
            if (!lastTimeSubmitted) { lastTimeSubmitted = 0; }
            if (!thisTimeSubmitted) { return 0; }
            if ( thisTimeSubmitted - lastTimeSubmitted <= 1000 ) {
                return -1;
            }
            lastTimeSubmitted = thisTimeSubmitted;
            return 1;
        }
	} catch(e) {}
	return 0;
}

function fnClickOnce(e) {
	try {
        var thisTimeSubmitted = new Date();
        if (!lastTimeSubmitted) { return true; }
        if (!thisTimeSubmitted) { return true; }
        if ( thisTimeSubmitted - lastTimeSubmitted <= 1000 ) {
            return false;
        }
        lastTimeSubmitted = thisTimeSubmitted;
	} catch(e) {}
    return true;
}

function validateOnSubmit(formobj) {
	if (isClosing) { return false; }
	var hasSelected = false;
	setCheckQuote('N');
	for (var iX=0; iX<eventObjects.length; iX++) {
		if (formobj['selEvent'+iX].checked) {
			hasSelected = true;
			if (formobj['eventStatus'+iX].selectedIndex == formobj['eventStatus'+iX].options.length-1) {
				alert('<%=MC.M_PlsSel_Stat%>');/*alert('Please select a status.');*****/
				formobj['eventStatus'+iX].focus();
				return false;
			}
			if (!formobj['statusValid'+iX].value || 
					formobj['statusValid'+iX].value.length == 0) {
				alert('<%=MC.M_PlsSel_Date%>');/*alert('Please enter a date.');*****/
				formobj['statusValid'+iX].focus();
				return false;
			}
			if (PAT_SCHED_EDIT_COVERAGE_MANDATORY == 1) {
				if (formobj['covType'+iX].selectedIndex == formobj['covType'+iX].options.length-1) {
					alert('<%=MC.M_PlsSel_CvgType%>');<%-- alert('Please select <%=LC.Evt_CoverageType%>.');*****--%>
					formobj['covType'+iX].focus();
					return false;
				}
			}
			if (formobj['covType'+iX].selectedIndex != formobj['covType'+iX].options.length-1) {
				//FIX #6306
				if (formobj['covType'+iX].value != formobj['covTypeFor'+iX].value) {
					if (!formobj['reason'+iX].value || formobj['reason'+iX].value.length == 0) {
						alert('<%=MC.M_PlsSelReason_Chg%>');<%-- alert('Please enter <%=LC.Reason_For_Change_CT_Short%>.');*****--%>
						formobj['reason'+iX].disabled = false;
						formobj['reason'+iX].focus();
						return false;
					}
				}
			}
			
			if (formobj['reason'+iX].value && formobj['reason'+iX].value.length > 4000) { // 4000 char limit
				var paramArray = [formobj['reason'+iX].value.length];
				alert(getLocalizedMessageString("M_ChgExcd4000_Char",paramArray));<%-- alert('<%=LC.Reason_For_Change_CT_Short%> may not exceed 4000 characters. Currently at '
						+formobj['reason'+iX].value.length+'.');*****--%>
				formobj['reason'+iX].focus();
				return false;
			}
		}
	}
	if (!hasSelected) {
		alert('<%=MC.M_SelEvt_ToEdit%>');/*alert('Please select an event to edit.');*****/
		return false;
	}
	var fdaVal = document.getElementById("fdaStudy").value;
	if (fdaVal == "1"){
		if (  !(validate_col('<%=LC.L_ReasonForChangeFDA%>',formobj.remarks))) return false;
	}
	if (formobj.eSign.value == "") {
		alert("<%=MC.M_PlsEnterEsign%>");/*alert("Please enter e-Signature");*****/
		formobj.eSign.focus();
		return false;
	}
	if (!(validate_col('e-Signature',formobj.eSign))) { return false; }
	<%-- if (isNaN(formobj.eSign.value) == true || (formobj.eSign.value.length<=3)) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
	} --%>
	if($j("#eSignMessage").text()==L_Invalid_Esign){
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		$j("#eSign").focus();
		return false;
	}

	enableDisableReasonOnSubmit();
	showProgressMessage();
	if (isIE6) { submitData(); }
	else { setTimeout('submitData()', 100); }
	return false;
}

function enableDisableReasonOnSubmit() {
	for (var iX=0; iX<eventObjects.length; iX++) {
		if (!formobj['selEvent'+iX].checked) { continue; }
		if (formobj['reason'+iX].value && formobj['reason'+iX].value.length > 0 
				&& formobj['reason'+iX].disabled) {
			formobj['reason'+iX].disabled = false;
			formobj['reason'+iX].readOnly = true;
		}
	}
}

function submitData() {
	$j.ajax({
		url:"updateEvtVisitPatSched.jsp",
		type:"POST",
		async:false,
		data:$j("#editVisitForm").serialize(),
		dataType:"json",
		success: handleSuccess1,
		error:function(data) { alert(data.statusText); return false; }
	});
}

function handleSuccess1(data) {
	if (!data && !data.result) { hideSplashMessage(); return false; }
	if (data.result < 0) { hideSplashMessage(); alert(data.resultMsg); return false; }
	showSplashMessage();
	var reloadTime = 1000; // FF
	if (navigator.userAgent.indexOf("MSIE") != -1) { 1990; } // IE
	setTimeout('window.location.reload();', reloadTime);
	setTimeout('$j("#editVisitDiv").dialog("close"); hideSplashMessage(); ', 2000);
}

function showProgressMessage() {
	document.editVisitForm.style.display = 'none';
	$('progressMsg').style.display = 'block';
	$('successMsg').style.display = 'none';
}

function showSplashMessage() {
	document.editVisitForm.style.display = 'none';
	$('progressMsg').style.display = 'none';
	$('successMsg').style.display = 'block';
}

function hideSplashMessage() {
	document.editVisitForm.style.display = 'block';
	$('progressMsg').style.display = 'none';
	$('successMsg').style.display = 'none';
}

</script>
</head>

<%
String src="";
src= request.getParameter("srcmenu");
String calAssoc=request.getParameter("calassoc");
calAssoc=(calAssoc==null)?"":calAssoc;
boolean isIE = false;
try { isIE = request.getHeader("USER-AGENT").toUpperCase().indexOf("MSIE") != -1; } catch(Exception e) {}
String todayStr = DateUtil.dateToString(Calendar.getInstance().getTime());

// Get coverage type legend
SchCodeDao scd = new SchCodeDao();
scd.getCodeValues("coverage_type");
ArrayList covSubTypeList = scd.getCSubType();
ArrayList covDescList = scd.getCDesc();
ArrayList<String> covCodeHide = scd.getCodeHide();
StringBuffer legendSB = new StringBuffer();
StringBuffer covMenuSB = new StringBuffer("<select id='covPowerBar' name='covPowerBar' >");
covMenuSB.append("<option value=''>"+LC.L_Select_AnOption/*Select an Option*****/+"</option>");
for (int iX=0; iX<covSubTypeList.size(); iX++) {
	covMenuSB.append("<option value='").append(covSubTypeList.get(iX)).append("' >");
	covMenuSB.append(covSubTypeList.get(iX)).append("</option>");
    String covSub1 = ((String)covSubTypeList.get(iX)).replaceAll("'", "\\\\'");
    String covDesc1 = ((String)covDescList.get(iX)).replaceAll("'", "\\\\'");
    String covIsHidden = covCodeHide.get(iX);
    if(StringUtil.isEmpty(covIsHidden))covIsHidden="N";
    if (covSub1 == null || covDesc1 == null) { continue; }
	if(covIsHidden.equalsIgnoreCase("Y")){
		legendSB.append("<b>").append(covSub1).append("</b>").append("=").append(covDesc1).append(" ["+LC.L_Hidden+"]").append("<br/>");
	}else{
		legendSB.append("<b>").append(covSub1).append("</b>").append("=").append(covDesc1).append("<br/>");
	}
}
covMenuSB.append("</select>");
String covTypeLegend = legendSB.toString();
String covMenuDDPowerBar = covMenuSB.toString();
String calledFrom = (request.getParameter("calledFrom")==null)?"":request.getParameter("calledFrom");
%>

<% if (!(calAssoc.equals("S"))) { 
if(!calledFrom.equals("patRoster")){
%>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<%}else{%>
<jsp:include page="include.jsp" flush="true"/>
	<%-- Nicholas : Start --%>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%} %>
<script>
var selectedVisit = 0;
var selectedVisitName = '';
var isClosing = false;

// INF-20084 Datepicker-- AGodara	
function formDateInputField(eCounter, date) {
	if (!date) { date = ''; }
		return  '<input name="statusValid'+eCounter+'" type="text" class="datefield" size=12 readonly value="'+date+'" />';
	}
function fnOpenEditVisitWindow(formobj, link, vName, vId, pageRight,orgRight, calStatus) {
	selectedVisit = vId;
	selectedVisitName = vName;
	//KM-#5977
	if (vName != null && vName.length > 20)
		vName = vName.substr(0,20)+'...';
	$('vName').innerHTML = vName;
	if (calStatus != 'A') {
		return false;
	}
	if (!f_check_perm_org(pageRight, orgRight, 'E')) { return false; }

	//KM-#5977
	//msg = "Would you like to edit visit: "+vName+"?";
	//if(!confirm(msg)) { return false; }


	<%-- IE6 does not hide DD elements behind the modal window. So manually hide and show. --%>
	beforeOpeningDialog();

	$('embedDataHere').innerHTML = ''; // Wipe out last data for performance
    if (navigator.userAgent.indexOf("MSIE") != -1) {
		$j("#editVisitDiv").dialog('open'); // For IE it's faster to open dialog and then load
		loadEditVisitData(vId);
    } else {
    	$j("#editVisitDiv").dialog('open');
		loadEditVisitData(vId); // For FF, it's faster to load and then open
    }
	clearDialogFields();
}

function loadEditVisitData(vId) {
	if ($('editVisitData'+vId)) {
		var innerHtm=$('editVisitData'+vId).innerHTML;
		innerHtm=innerHtm.replace(/datefield hasDatepicker/g,'datefield');
		$('embedDataHere').innerHTML = innerHtm;//$('editVisitData'+vId).innerHTML;
	} else {
		$('embedDataHere').innerHTML = '';
	}

	var jObj = $j.parseJSON($("hiddenData"+vId).innerHTML);
	eventObjects = jObj.eventObjects;
	//for (var iX=0; iX<jObj.eventObjects.length; iX++) {
	//	var eDate = jObj.eventObjects[iX].eDate;
	//	if (!eDate) { eDate = '' }
		//$('embedDataHere').innerHTML = $('embedDataHere').innerHTML.replace("[VEL_INPUT_DT"+iX+"]", 
		//	formDateInputField(iX, eDate) );
	//}
  	if (PAT_SCHED_EDIT_COVERAGE_MANDATORY == 1) {
 	  	$('cov_mandatory_mark').innerHTML = $('cov_mandatory_mark').innerHTML.replace(/\[VEL_MANDATORY_MARK\]/,'<FONT class="mandatory">*</FONT>');
 	} else {
  	  	$('cov_mandatory_mark').innerHTML = $('cov_mandatory_mark').innerHTML.replace(/\[VEL_MANDATORY_MARK\]/,'');
  	}
	disableOnDialogOpen();
}

function createEditVisitDialog(vId) {
	var editVisitHeight = defaultHeight;
	$j("#editVisitDiv").dialog( { height: editVisitHeight, width: 850, modal: true, 
		resizable: false, autoOpen:false, closeText:'',
		dragStart: function(event, ui) { editVisitHeight = $j("#editVisitDiv").dialog( "option", "height" ); },
		dragStop: function(event, ui) { $j("#editVisitDiv").dialog( "option", "height", editVisitHeight ); },
		resizeStop: function(event, ui) { editVisitHeight = $j("#editVisitDiv").dialog( "option", "height" ) },
		beforeClose: function(event, ui) { beforeClosingDialog(); }
		} );
}

function openAddlInfoWindow(cnt, eId) {
	var addlInfoHeight = navigator.userAgent.indexOf("MSIE") != -1 ? 350 : 220;
	if (cnt > 1) { addlInfoHeight = navigator.userAgent.indexOf("MSIE") != -1 ? 700 : 445; }
	$j("#addlInfoForEvt"+eId).dialog( { height: addlInfoHeight, width: 550, modal: true, resizable: false,
		closeText:'',
		dragStart: function(event, ui) { addlInfoHeight = $j("#addlInfoForEvt"+eId).dialog( "option", "height" ); },
		dragStop: function(event, ui) { $j("#addlInfoForEvt"+eId).dialog( "option", "height", addlInfoHeight ); },
		resizeStop: function(event, ui) { addlInfoHeight = $j("#addlInfoForEvt"+eId).dialog( "option", "height" );
		}
	} );
}

function replaceSpecialCharsForOverlib(inTxt) {
	if (!inTxt) { return ''; }
	var reg_exp = /\'/g;
	var reg_dbqt = /\"/g;
	var reg_lt  = /&lt;/g;
	var reg_gt  = /&gt;/g;
	var reg_cr  = /\r/g;
	var reg_lf  = /\n/g;
	return inTxt.replace(reg_exp, "\\'").replace(reg_dbqt, "&quot;")
	    .replace(reg_lt, "<").replace(reg_gt, ">")
	    .replace(reg_cr, "").replace(reg_lf, "<br/>");
}

<%-- Use 'visibility' not 'display' --%>
function beforeOpeningDialog() {
	if (!isIE6) { return; }
	document.sch.availableSch.style.visibility = 'hidden';
	document.sch.visit.style.visibility = 'hidden';
	document.sch.date_range.style.visibility = 'hidden';
	document.sch.month.style.visibility = 'hidden';
	document.sch.year.style.visibility = 'hidden';
	document.sch.event.style.visibility = 'hidden';
	document.sch.dstatus.style.visibility = 'hidden';
}

function beforeClosingDialog() {
	if (!isIE6) { return; }
	document.sch.availableSch.style.visibility = 'visible';
	document.sch.visit.style.visibility = 'visible';
	document.sch.date_range.style.visibility = 'visible';
	document.sch.month.style.visibility = 'visible';
	document.sch.year.style.visibility = 'visible';
	document.sch.event.style.visibility = 'visible';
	document.sch.dstatus.style.visibility = 'visible';
}

function clearDialogFields() {
	isClosing = false;
	resetColumns(); // reset eSign elements
	document.editVisitForm.eventStatusAll.selectedIndex = document.editVisitForm.eventStatusAll.options.length-1;
	document.editVisitForm['statusValidAll'].value = '<%=todayStr%>';
}
</script>

<%} %>
<%-- Put this overriding style after panel.jsp --%>


<jsp:include page="ui-include.jsp" flush="true"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="patEnrollB" scope="request" class="com.velos.eres.web.patProt.PatProtJB"/>
<jsp:useBean id="eventAssocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="codeLst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="grpRights" scope="request" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="ctrl1" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="userSiteB" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" />
<jsp:useBean id="patB" scope="request" class="com.velos.eres.web.patStudyStat.PatStudyStatJB"/>
<jsp:useBean id="bRows" scope="request" class="com.velos.eres.web.browserRows.BrowserRowsJB"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="siteB" scope="request" class="com.velos.eres.web.site.SiteJB"/>

<jsp:useBean id="protVisitB" scope="request" class="com.velos.esch.web.protvisit.ProtVisitJB"/><!--//JM: 05May2008, added for, Enh. #PS16-->
<jsp:useBean id="labServiceFactory" scope="session" class="com.velos.remoteservice.lab.LabServiceFactory" />

<%@page import = "com.velos.remoteservice.lab.*,java.net.URLEncoder,com.velos.eres.business.section.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.esch.business.common.*"%>
<%@page import="java.text.SimpleDateFormat"%>

<body style="overflow:hidden">


<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1100;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="formjs.js"><!-- FORM JS--></script>

<%
	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession)) {

	Integer lastSelVisitInSession = (Integer)tSession.getAttribute(LastSelVisit);
    String defUserGroup = (String) tSession.getAttribute("defUserGroup");

    // Get event status DD
    SchCodeDao scdForEvtStat = new SchCodeDao();
    scdForEvtStat.getCodeValues("eventstatus",0);
    scdForEvtStat.setCType("eventstatus");
    scdForEvtStat.setForGroup(defUserGroup);
    String scdForEvtStatDD = scdForEvtStat.toPullDown("eventStatusAll", 0, "");

	int formLibSeq = 0;
	char formLibAppRight = '0';
	//put the values in session
	String study = request.getParameter("studyId");
	if(study == null || study.equals("null"))
	{
		study = (String) tSession.getValue("studyId");
	}
	else
	{
		tSession.setAttribute("studyId",study);
	}

	String enrollId = "";
	String statid = "";

	String calStatus= request.getParameter("calStatus"); //JM: 05May2008, added for, Enh. #PS16

	String usr = (String) tSession.getValue("userId");
	int accountId = EJBUtil.stringToNum((String)tSession.getValue("accountId"));
    int pageRight = 0;
 	int orgRight = 0;
 	int patStudyStatpk = 0;
	int calstat = 0;   
    SchCodeDao schcodeDao = new SchCodeDao();
    calstat =  schcodeDao.getCodeId("calStatStd", "D");
    

	//******************GET STUDY TEAM RIGHTS and put the same in session, this will be a separate obj as no study is selected //***************************************************************
    TeamDao teamDao = new TeamDao();
  int study_cord = teamDao.getStudyCordinator(EJBUtil.stringToNum(study),EJBUtil.stringToNum(usr));
   
    teamDao.getTeamRights(EJBUtil.stringToNum(study),EJBUtil.stringToNum(usr));
    ArrayList tId = teamDao.getTeamIds();
    int patdetright = 0;
    if (tId.size() == 0)
	{
    	pageRight=0 ;
    }
	else
	{
    	stdRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));
    	 	ArrayList teamRights ;
						teamRights  = new ArrayList();
						teamRights = teamDao.getTeamRights();

						stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
						stdRights.loadStudyRights();

    	tSession.setAttribute("studyRights",stdRights);
    	if ((stdRights.getFtrRights().size()) == 0)
		{
    	 	pageRight= 0;
    		patdetright = 0;
    	}
		else
		{
    		pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYMPAT"));
    		patdetright = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYVPDET"));
    		tSession.setAttribute("studyManagePat",new Integer(pageRight));
    		tSession.setAttribute("studyViewPatDet",new Integer(patdetright));
    	}
    }

	//boolean genSchedule = true;
	SchCodeDao cd = new SchCodeDao();
	String dEvStat = "";
	int stTempPk;

	String evParmStatus = request.getParameter("dstatus");
	int tempStat = EJBUtil.stringToNum(evParmStatus);

	cd.getCodeValues("eventstatus",0);
	cd.setCType("eventstatus");
	cd.setForGroup(defUserGroup);
	cd.getHiddenCodelstDataForUser();


	String hideCode = "";


	//dEvStat	 =  cd.toPullDown("status");
	StringBuffer mainStr = new StringBuffer();
	int cdcounter = 0;
 	mainStr.append("<SELECT NAME='dstatus' id='dstatus' onchange='setDStatusInAddlParams(this);'><Option value='' Selected>"+LC.L_All/*All*****/+"</option>") ;
	for (cdcounter = 0; cdcounter <= cd.getCDesc().size() -1 ; cdcounter++)
	{
		stTempPk = ((Integer) cd.getCId().get(cdcounter)).intValue() ;

		hideCode = cd.getCodeHide(cdcounter);

		if (tempStat == stTempPk )
		{
		mainStr.append("<OPTION SELECTED value = "+ cd.getCId().get(cdcounter)+">" + cd.getCDesc().get(cdcounter)+ "</OPTION>");
		}
		else
		{
		   if (! StringUtil.isEmpty(hideCode) && (!hideCode.equals("Y")))
		   {
		      mainStr.append("<OPTION value = "+ cd.getCId().get(cdcounter)+">" + cd.getCDesc().get(cdcounter)+ "</OPTION>");
		    }

		}
	}
	//mainStr.append("<OPTION value='' SELECTED> Select an Option</OPTION>");
	mainStr.append("</SELECT>");
	dEvStat = mainStr.toString();


	String modRight = (String) tSession.getValue("modRight");
	int modlen = modRight.length();

	ctrl.getControlValues("module");
	int ctrlrows = ctrl.getCRows();

	ArrayList feature =  ctrl.getCValue();
	ArrayList ftrDesc = ctrl.getCDesc();
	ArrayList ftrSeq = ctrl.getCSeq();
	ArrayList ftrRight = new ArrayList();
	ArrayList crfNamesArr = new ArrayList();

	String strR;
	ArrayList ctrlValue = new ArrayList();

	for (int counter = 0; counter <= (modlen - 1);counter ++)
	{
		strR = String.valueOf(modRight.charAt(counter));
		ftrRight.add(strR);
	}

	grpRights.setGrSeq(ftrSeq);
        grpRights.setFtrRights(ftrRight);
	grpRights.setGrValue(feature);
	grpRights.setGrDesc(ftrDesc);
	int eptRight = 0;
	eptRight  = Integer.parseInt(grpRights.getFtrRightsByValue("MODEPT"));

	int personPK = 0;
	int siteId = 0;
	String patientId = "";

	personPK = EJBUtil.stringToNum(request.getParameter("pkey"));
	String pkey = request.getParameter("pkey");


    person.setPersonPKId(personPK);
	person.getPersonDetails();
	patientId = person.getPersonPId();

	siteId = EJBUtil.stringToNum(person.getPersonLocation());
	String userIdFromSession = (String) tSession.getValue("userId");

	orgRight = userSiteB.getMaxRightForStudyPatient(EJBUtil.stringToNum(userIdFromSession), personPK , EJBUtil.stringToNum(study) );
	if (orgRight > 0)
	{
		//System.out.println("patient schedule orgRight" + orgRight);
		orgRight = 7;
	}

	if ((pageRight >= 4) && (orgRight >= 4))
	{


	String statDesc=request.getParameter("statDesc");
	String studyVer=request.getParameter("studyVer");
	if ( studyVer == null ) studyVer = "";


	String studyTitle = "";
	String studyNumber = "";
	studyB.setId(EJBUtil.stringToNum(study));
    studyB.getStudyDetails();
    studyTitle = studyB.getStudyTitle();
    int fdaStudy = ("1".equals(studyB.getFdaRegulatedStudy()))? 1:0;

    //Added by IA 1.04.2006 Bug#2811
// 		studyTitle = StringUtil.htmlDecode(studyTitle);
	//studyTitle = StringUtil.htmlDecode(studyTitle);
    //End added by IA 1.04.2006

	studyNumber = studyB.getStudyNumber();

	//String uId = (String) tSession.getValue("userId");
	//int userId = EJBUtil.stringToNum(uId);

	String uName = (String) tSession.getValue("userName");
	String ipAdd = (String) tSession.getValue("ipAdd");
	int totalDefinedVisits = 0;

//	if ((pageRight >= 4) && (orgRight >= 4))
//	{

		ILabService labService = labServiceFactory.getService();
		boolean isLabServicePresent = (labService == null) ? false : true;

		String evDesc = "";
		String subType ="";
		String protocolId="";
		String startdt="";
		String status="";//G-Generate Schedule,S-Show Schedule also
		String schDate = "";
		String acDate = "";
		String scheduleStatus = "";
		String scheduleStatusText = "";
		String page1 = request.getParameter("page");
		String selectedTab =  request.getParameter("selectedTab");
		String generateGoFlag = "P";
		String generate = request.getParameter("generate");
		String crfHrf="";

		StringBuffer sbAvailableSchedulesDropDown = new StringBuffer();
		ScheduleDao schDao = new ScheduleDao();
		int schedulesCount = 0;
		String availSchedulePatProtId = "";
		String selectedString = "";
		String selectedEnrollId = "";
		int selectedSchedule = 0;

 		selectedEnrollId = request.getParameter("availableSch");
		patEnrollB.findCurrentPatProtDetails(EJBUtil.stringToNum(study),EJBUtil.stringToNum(pkey));
		enrollId = String.valueOf(patEnrollB.getPatProtId());

		if (enrollId.equals("0"))
		{
			enrollId = "";
		}

		tSession.setAttribute("enrollId",enrollId);

		//get available schedules for the patient, sonia Abrol, 11/15/06
		schDao = patEnrollB.getAvailableSchedules(pkey,  study);

		//if selectedEnrollId is empty, that means show the current schedule
		if (StringUtil.isEmpty(selectedEnrollId))
		{
			selectedEnrollId = enrollId;//where enrollID is the current schedule
		}

		if (schDao != null)
		{
			sbAvailableSchedulesDropDown.append("<select id='availableSch' name='availableSch'>");
			sbAvailableSchedulesDropDown.append("<option value='' >"+LC.L_Select_AnOption/*Select an Option*****/+"</option>");
			schedulesCount = (schDao.getPatProtIds()).size();
			boolean scheduleExists = false;

			if (schedulesCount > 0){
				ArrayList patProtIds = schDao.getPatProtIds();
				if (patProtIds.contains(selectedEnrollId)){
					scheduleExists = true;
				}
			}

			for (int ctr=0; ctr<schedulesCount; ctr++)
			{
				selectedString = "";
				availSchedulePatProtId = schDao.getPatProtIds(ctr);

				if (!scheduleExists && ctr == 0){
					selectedString = " SELECTED ";
					selectedEnrollId = selectedEnrollId;
					//selectedEnrollId = availSchedulePatProtId;
					enrollId = selectedEnrollId;
					selectedSchedule = ctr+1;
				}else{
					if (! StringUtil.isEmpty(selectedEnrollId))
					{
						if (selectedEnrollId.equals(availSchedulePatProtId))
					 	{
				 			selectedString = " SELECTED ";
							selectedSchedule = ctr+1;
					 	}
					}
				}
				sbAvailableSchedulesDropDown.append("<option "+selectedString+ " value='" + availSchedulePatProtId +"'>"+ schDao.getProtocolNames(ctr) +", "+ schDao.getProtocolStartDates(ctr) +"</option>");
			}
			sbAvailableSchedulesDropDown.append("</select>");
		}

		String selDay = "";

		if (selectedEnrollId != enrollId)
		{
			patEnrollB.setPatProtId(EJBUtil.stringToNum(selectedEnrollId)); //get patprot details for old patprot
			patEnrollB.getPatProtDetails();
		}

		startdt = patEnrollB.getPatProtStartDt();
		protocolId =patEnrollB.getPatProtProtocolId();
		selDay =  patEnrollB.getPatProtStartDay();
		scheduleStatus = patEnrollB.getPatProtStat();

		if (! StringUtil.isEmpty(scheduleStatus))
		{
			if (scheduleStatus.equals("0"))
			{
				scheduleStatusText = "<font size=1 color=\"red\">"+LC.L_Discontinued/*Discontinued*****/+"</font>";
			}
			else if (scheduleStatus.equals("1") && (!StringUtil.isEmpty(protocolId)) )
			{
				scheduleStatusText = "<font size=1 color=\"green\">"+LC.L_Current/*Current*****/+"</font>";
			}
		}
		else
		{
			scheduleStatusText = "";
		}

		if (selDay == null || selDay.trim().equals(""))
		{
			selDay = "0";
		}

		String orderBy = "";
		orderBy = request.getParameter("orderBy");
		String orderType = "";
		orderType = request.getParameter("orderType");
		if (orderType == null)
		{
			orderType = "";
		}

		int evCRFCount = 0;

		ArrayList crfIds = new ArrayList();
		ArrayList crfNames = new ArrayList();
		ArrayList crfformEntryChars = new ArrayList();
		ArrayList crfLinkedFormIds = new ArrayList();
		ArrayList crfformSavedCounts = new ArrayList();
		ArrayList crfFormTypes = new ArrayList();
		ArrayList crfDisplayInSpecs = new ArrayList();

		Integer crfId = null;
		String crfName = "";
		String crfFormEntryChar = "";
		String crfformSavedCount = "";
		String crfLinkedForm = "";
		String crfString = "";
		String popCrfString = "";
		String protName= "";
		String protDur = "";
		String calstatus="";
		Date st_date1 = null;
		Integer prot_id = null;
		String displayInSpec = "";
		String crfType = "";

		if(protocolId != null && (!EJBUtil.isEmpty(selectedEnrollId)))
		{
			eventAssocB.setEvent_id(EJBUtil.stringToNum(protocolId));
			eventAssocB.getEventAssocDetails();
			protName= 	eventAssocB.getName();
			protDur = 	eventAssocB.getDuration();
			calstatus= eventAssocB.getStatCode();
			prot_id = new Integer(protocolId);
		}
		else
		{
			protName = MC.M_NoAssoc_Cal/*"No Associated Calendar"*****/;
		}

		String patStudyStat = "";
		String patStatSubType = "";
		CodeDao cdStat = new CodeDao();

		patB.getPatStudyCurrentStatus(study, pkey);
		patStudyStatpk = patB.getId();
		patStudyStat = patB.getPatStudyStat();
		statid = patStudyStat ;
		cdStat.getCodeValuesById(EJBUtil.stringToNum(patStudyStat));

		ArrayList arSubType = new ArrayList();
		arSubType = cdStat.getCSubType();

		if (arSubType.size() > 0 )
		{
			patStatSubType = (String) arSubType.get(0) ;
		}

	    String visit = request.getParameter("visit");

		String visitxt = request.getParameter("visitext");
		String year = request.getParameter("year");
		String month = request.getParameter("month");
		String event = request.getParameter("event");
		String evtStatus = request.getParameter("dstatus");
		String evttxt = request.getParameter("eventext");
		String durtxt = request.getParameter("monthyrtext");
		String ststxt = request.getParameter("statustext");
		String dateRange = request.getParameter("date_range");

		if ( visit == null ) { visit  = "0" ; }
		if ( visitxt == null )  { visitxt = "ALL"; }
		if ( year == null )  year  = "0" ;
		if ( month == null )  month  = "0" ;
		if ( event == null )  event  = "All" ;
		if ( "".equals(event) )  event  = "All" ;
		if ( evtStatus == null )  evtStatus   = "" ;
		if ( durtxt == null ) durtxt = "ALL";
		if ( evttxt == null ) evttxt = "ALL" ;
		if ( ststxt == null )	ststxt = "ALL" ;
		if ( dateRange == null) dateRange = "ALL";
		dateRange = StringUtil.htmlEncodeXss(dateRange.trim());
		event = event.trim();
		
		
	String patProtId = selectedEnrollId;
	int pkPatProt = EJBUtil.stringToNum(patProtId);
	ArrayList visitMonths = null;
	ArrayList listOfVisitFKsByMonth = null;
	ArrayList listOfVisitNamesByMonth = null;
	ArrayList listOfStartDatesByMonth = null;
	ArrayList listOfActualDatesByMonth = null;
	ArrayList listOfVisitWindowsByMonth = null;
	ArrayList listOfEvtCountsByMonth = null;
	ArrayList listOfDoneCountsByMonth = null;
	ArrayList listOfPastsByMonth = null;
	
	HashMap paramMap = new HashMap();
	
	if (pkPatProt > 0) {
		if (!"0".equals(visit)) {
			paramMap.put("visit", new Integer(visit));
		}
		if (!"All".equals(event)) {
		    paramMap.put("event", event);
		}
		if (!"".equals(evtStatus)) {
		    paramMap.put("eventStatus", new Integer(evtStatus));
		}
		if (!"0".equals(year)) {
		    paramMap.put("year", new Integer(year));
		}
		if (!"0".equals(month)) {
		    paramMap.put("month", new Integer(month));
		}
		if (!"ALL".equals(dateRange)) {
		    paramMap.put("dateRange", dateRange);
		}
		ScheduleDao schDao1 = protVisitB.getVisitsByMonth(EJBUtil.stringToNum(patProtId), paramMap);
		visitMonths = schDao1.getVisitMonths();
		listOfVisitFKsByMonth = schDao1.getListOfVisitFKsByMonth();
		listOfVisitNamesByMonth = schDao1.getListOfVisitNamesByMonth();
		listOfStartDatesByMonth = schDao1.getListOfStartDatesByMonth();
		listOfActualDatesByMonth = schDao1.getListOfActualDatesByMonth();
		listOfVisitWindowsByMonth = protVisitB.getListOfVisitWindowsByMonth();
		listOfEvtCountsByMonth = schDao1.getListOfEvtCountsByMonth();
		listOfDoneCountsByMonth = schDao1.getListOfDoneCountsByMonth();
		listOfPastsByMonth = schDao1.getListOfPastsByMonth();
	}
	if (visitMonths == null) { visitMonths = new ArrayList(); }
	if (listOfVisitFKsByMonth == null) { listOfVisitFKsByMonth = new ArrayList(); }
	if (listOfVisitNamesByMonth == null) { listOfVisitNamesByMonth = new ArrayList(); }
	if (listOfStartDatesByMonth == null) { listOfStartDatesByMonth = new ArrayList(); }
	if (listOfActualDatesByMonth == null) { listOfActualDatesByMonth = new ArrayList(); }
	if (listOfVisitWindowsByMonth == null) { listOfVisitWindowsByMonth = new ArrayList(); }
	if (listOfEvtCountsByMonth == null) { listOfEvtCountsByMonth = new ArrayList(); }
	if (listOfDoneCountsByMonth == null) { listOfDoneCountsByMonth = new ArrayList(); }
	if (listOfPastsByMonth == null) { listOfPastsByMonth = new ArrayList(); }
%>

<script>
$j(document).ready(function() {
<%  for(int iX=0; iX<visitMonths.size(); iX++) { %>
	$j("#accordion<%=iX%>").accordion({
    	autoHeight: false, collapsible: true, active: false, fillSpace: false, animated: false
    });
<%  } %>
  });
function applyToSelected() {
	var formobj = document.editVisitForm;
	var selArray = []
	for(var iX=0; iX<eventObjects.length; iX++) {
		if (formobj['selEvent'+iX].checked) { selArray.push(iX); }
	}
	if (selArray.length == 0) {
		alert('<%=MC.M_SelEvt_ToEdit%>');/*alert('Please select an event to edit.');*****/
		return false;
	}
	for (var iS=0; iS<selArray.length; iS++) {
		if (formobj['statusValidAll'].value && formobj['statusValidAll'].value.length > 0) {
			formobj['statusValid'+selArray[iS]].value = formobj['statusValidAll'].value;
		}
		// If Event Status selected is 'Select an Option', don't apply it
		if (formobj.eventStatusAll.selectedIndex 
				== formobj.eventStatusAll.options.length-1) { continue; }
		formobj['eventStatus'+selArray[iS]].selectedIndex = formobj.eventStatusAll.selectedIndex;
	}
}
function applyToAll() {
	
	var formobj = document.editVisitForm;
	
	for(var iX=0; iX<eventObjects.length; iX++) {
		if (formobj['statusValidAll'].value && formobj['statusValidAll'].value.length > 0) {
			formobj['statusValid'+iX].value = formobj['statusValidAll'].value;
		}
		formobj['selEvent'+iX].checked = true;
		// If Event Status selected is 'Select an Option', don't apply it
		if (formobj.eventStatusAll.selectedIndex 
				== formobj.eventStatusAll.options.length-1) { continue; }
		formobj['eventStatus'+iX].selectedIndex = formobj.eventStatusAll.selectedIndex;
	}
	$j('#embedDataHere').find('input, select').removeAttr('disabled');
	openCal();
	 //$j('input:checkbox').attr('checked','checked');
	//for(var iX=0; iX<eventObjects.length; iX++) {
	//	toggleRowEdit(iX);
	//}
}

function toggleRowEdit(row) {
	var formobj = document.editVisitForm;
	if (formobj['selEvent'+row].checked) {
		formobj['eventStatus'+row].disabled = false;
		formobj['statusValid'+row].disabled = false;
		formobj['covType'+row].disabled = false;
		openCal();
	} else {
		formobj['eventStatus'+row].disabled = true;
		formobj['statusValid'+row].disabled = true;
		formobj['covType'+row].disabled = true;
	}
}

function enableReasonOnChange(row) {
	var formobj = document.editVisitForm;
	var selectedCTValue = formobj['covType'+row].options[formobj['covType'+row].selectedIndex].value;
	if (selectedCTValue == '') { selectedCTValue = 0; }
	if (formobj['covTypeFor'+row].value == selectedCTValue) {
		formobj['reason'+row].disabled = true;
	} else {
		formobj['reason'+row].disabled = false;
	}
	if (formobj['covTypeFor'+row].value == 0 && selectedCTValue == 0) {
		formobj['reason'+row].value = '';
	}
}

function disableOnDialogOpen() {
	var formobj = document.editVisitForm;
	for(var iX=0; iX<eventObjects.length; iX++) {
		formobj['eventStatus'+iX].disabled = true;
		formobj['statusValid'+iX].disabled = true;
		formobj['covType'+iX].disabled = true;
		formobj['reason'+iX].disabled = true;
	}
}
</script>

<%-- Start of Edit Visit modal dialog --%>
<div id="editVisitDiv" title="<%=LC.L_Edit_Visit %>" class="BrowserBotN" style="display:none; top: 0px;">
<form name="editVisitForm" id="editVisitForm" action="" method="post" onSubmit="return validateOnSubmit(this);"
      style="background-color:transparent">
<table width="95%" border="0" cellspacing="0" >
<tr><td colspan="4"></td></tr>
<tr>

<td width="25%" nowrap> &nbsp; <br/> <%=LC.L_Visit_Name%><%--Visit Name*****--%>: <span id="vName"> </span></td>
<td width="4.5%">&nbsp;</td>
<td width="18%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Status%><%--Status*****--%><br/><%=scdForEvtStatDD.toString()%></td>
<%-- INF-20084 Datepicker-- AGodara --%>
<td width="15%" align="left" nowrap>&nbsp;<%=LC.L_Status_Date%><%--Status Date*****--%><br/><input name='statusValidAll' type='text' class="datefield" size="12" readonly /></td>
<td><b><%=LC.L_Apply_To%><%--Apply to*****--%> </b><br/><button onclick="applyToAll(); return false;"><%=LC.L_All%></button>&nbsp;<%=LC.L_Or_Lower%><%--or*****--%>&nbsp;<button onclick="applyToSelected(); return false;"><%=LC.L_Selected%></button></td>
</tr>
<tr><td colspan="6"><hr/></td></tr>
</table>
<%-- The next table is to display labels for the table below with the scrollbar --%>
<table width="95%" border="0" cellspacing="0" style="border-style:none; border-width:0" class="basetbl">
<tr bgcolor='white'>
<th width="24%"><%=LC.L_Edit%><%--Edit*****--%></th>
<th width="18%" align="left">&nbsp;<%=LC.L_Status%><%--Status*****--%><FONT class="Mandatory">*</FONT></th>
<th width="15%" align="left">&nbsp;<%=LC.L_Status_Date%><%--Status Date*****--%><FONT class="Mandatory">*</FONT></th>
<th width="22%" align="left" nowrap="nowrap"><div id="cov_mandatory_mark"><%=LC.L_Coverage_Type%><%--<%=LC.Evt_CoverageType%>*****--%>[VEL_MANDATORY_MARK]
<a href="javascript:void(0)" onmouseover="return overlib('<%=covTypeLegend%>',CAPTION,'<%=LC.L_Coverage_TypeLegend%><%--<%=LC.Evt_CoverageType%> Legend*****--%>');" 
    onmouseout="return nd();"><img src="../images/jpg/help.jpg" align="absmiddle" border="0"></a></div>
</th>
<th width="19%" align="left" ><%=MC.M_ReasonChg_CvgType%></th>
</tr>
</table>
<div style="height:<%=isIE?"250px":"280px"%>; overflow-y:scroll">
<div id='embedDataHere'></div>
</div>
<table width="95%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td width="150px"></td>
    <td><%=LC.L_ReasonForChangeFDA%>
     	<%if (fdaStudy == 1){%><FONT class="Mandatory">* </FONT><%} %>
	</td>
	<td><textarea id="remarks" name="remarks" rows="2" cols="20" MAXLENGTH="4000"></textarea></td>
</tr>
<tr>
<%if ("userpxd".equals(Configuration.eSignConf)) {%>
    <td width="150px"><span id="eSignMessage" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;"></span></td>
	<td id="eSignLabel" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;">e-Password<FONT class="Mandatory">*</FONT>&nbsp</td>
<%-- Aug-24-2011 Bug #6702 agodara --%>
	<td><input type="password" name="eSign" id="eSign" autocomplete="off" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;" 
    onkeyup="ajaxvalidate('pass:'+this.id,-1,'eSignMessage','Valid-Password','Invalid-Password','sessUserId')"></input>&nbsp;&nbsp;&nbsp;&nbsp;</td>
	
	<% } else{%>
	<td width="150px"><span id="eSignMessage" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;"></span></td>
	<td id="eSignLabel" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;"><%=LC.L_Esignature%><%--e-Signature*****--%><FONT class="Mandatory">*</FONT>&nbsp</td>
<%-- Aug-24-2011 Bug #6702 agodara --%>
	<td><input type="password" name="eSign" id="eSign" autocomplete="off" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;" maxlength="8" 
    onkeyup="ajaxvalidate('misc:'+this.id,4,'eSignMessage','Valid e-Sign','Invalid e-Sign','sessUserId')"
    onkeypress="if (fnOnceEnterKeyPress(event)>0) { validateOnSubmit(document.editVisitForm); return false; } "></input>&nbsp;&nbsp;&nbsp;&nbsp;</td>
	
	<%} %>
	<td>&nbsp;&nbsp;<button type="submit" id="submit_btn" onclick="return fnClickOnce(event); " ondblclick="return false"><%=LC.L_Submit%></button></td>
	<td>&nbsp;&nbsp;&nbsp;&nbsp;<button id="close_btn" onclick='isClosing=true;$j("#editVisitDiv").dialog("close")'><%=LC.L_Close%></button></td>
</tr>
</table>
</form>
<div id='progressMsg' style="display:none;"><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><p class="sectionHeadings" align="center"> <%=LC.L_PleaseWait_Dots %><%-- Please wait...*****--%> </p></div>
<div id='successMsg' style="display:none;"><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><p class="sectionHeadings" align="center"> <%=MC.M_Data_SvdSucc %><%-- Data was saved successfully*****--%> </p></div>
</div>
<%-- End of Edit Visit modal dialog --%>

<DIV class="BrowserTopn" id="div1">
<% if (!calAssoc.equals("S")) { 
if(!calledFrom.equals("patRoster")){%>
	<jsp:include page="patienttabs.jsp" flush="true">
	<jsp:param name="pkey" value="<%=pkey%>"/>
	<jsp:param name="patientCode" value="<%=StringUtil.encodeString(patientId)%>"/>
	<jsp:param name="patProtId" value="<%=enrollId%>"/>
	<jsp:param name="studyId" value="<%=study%>"/>
	<jsp:param name="studyVer" value="<%=studyVer%>"/>
	<jsp:param name="selectedTab" value="<%=selectedTab%>"/>
	<jsp:param name="page" value="<%=page1%>"/>
	<jsp:param name="skipLinkedForms" value="Y"/>
	</jsp:include>
	<%} %>
</DIV>
<%if(!calledFrom.equals("patRoster")){%>
<SCRIPT LANGUAGE="JavaScript">
	if(screenWidth>1280 || screenHeight>1024){
		if(isIE==true){
			document.write('<DIV class="tabFormBotN tabFormBotN_PSc_1" id="div2" style="height:72%;">')
		}else{
			document.write('<DIV class="tabFormBotN tabFormBotN_PSc_1" id="div2" style="height:68%;">')
		}
	}else{
		document.write('<DIV class="tabFormBotN tabFormBotN_PSc_1" id="div2">')
	}
</SCRIPT>
<%}else{ %>
<SCRIPT LANGUAGE="JavaScript">
	if(screenWidth>1280 || screenHeight>1024){
		if(isIE==true){
			document.write('<DIV class="tabFormBotN_PSc_1" id="div2" style="height:94%; overflow:auto;">')
		}else{
			document.write('<DIV class="tabFormBotN_PSc_1" id="div2" style="height:94%; overflow:auto">')
		}
	}else{
		document.write('<DIV class="tabFormBotN_PSc_1" id="div2" style="height:94%; overflow:auto;">')
	}
</SCRIPT>
<%} }

	if((!EJBUtil.isEmpty(selectedEnrollId)) && (!calledFrom.equals("patRoster")))
	{
	%>

	<table width="98%"  border="0">
	<tr>

	<%
	studyTitle = StringUtil.escapeSpecialCharJS(studyTitle);
	%>

	<script language=javascript>
		var varViewTitle = htmlEncode('<%=studyNumber%>');
		var study_Title = htmlEncode('<%=studyTitle%>');
	</script>

		<td align="center">

		<!--Add study icon in place of study link-->

		<%=LC.L_Study%><%--<%=LC.Std_Study%>*****--%> #:&nbsp;<%=studyNumber%>&nbsp;<a href="study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId=<%=study%>"><img id = "activeImage" class = "unSelectedImage imageDim" src="../images/jpg/study.gif" onmouseover="return overlib(varViewTitle,CAPTION,'<%=LC.L_Study_Number%>' ,RIGHT , BELOW );" onmouseout="return nd();" ></a> &nbsp;&nbsp;&nbsp;&nbsp;

		<a href="#" onmouseover="return overlib(study_Title,CAPTION,'<%=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%>');" onmouseout="return nd();"><img border="0" src="./images/View.gif"/></a>  &nbsp;&nbsp;&nbsp;&nbsp;
		<%=LC.L_Calendar%><%--Calendar*****--%>: <%=protName%> &nbsp;&nbsp;&nbsp;&nbsp;
		<%=LC.L_Pat_Startdate%><%--Pat.Start Date*****--%>: <%=startdt%> &nbsp;&nbsp;&nbsp;&nbsp;
		<%=LC.L_Schedule%><%--Schedule*****--%>: <%=scheduleStatusText%> &nbsp;&nbsp;&nbsp;&nbsp;

		<%
		//Modified by Manimaran for November Enhancement PS4.
		if(! (patStatSubType.trim().equalsIgnoreCase("lockdown")) )
		{
			%>

		</td></tr>
		<tr><td align="center">
			<A href="#" onClick="openWinProtocol('<%=pageRight%>','<%=orgRight%>', '<%=study%>','<%=pkey%>')"><%=MC.M_Edit_CalOrDt%><%--Edit Calendar/Date*****--%></A>
		<%
		}
		else
		{
		%>
			&nbsp;
			<%
		}
		%>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<A href="#" onClick="return  viewschedule('patientpreschedule.jsp?patientId=<%=pkey%>&studyId=<%=study%>')"><%=LC.L_View_Previous%><%--View Previous*****--%></A>

	<Input type="hidden" name="srcmenu" value=<%=src%>>

    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <%
    if(! (patStatSubType.trim().equalsIgnoreCase("lockdown")) )
		{
			%>
        <A href="#" align =right
	onClick="openDeleteWindow(<%=pkey%>,<%=study%>,<%=pageRight%>,<%=orgRight%>)"> <%=LC.L_Del_Sch%><%--Delete Schedule*****--%></A>
	<% } %>


         </td>

	 </tr>
	</table>




	<%
		}

	if ((pageRight >= 4) && (orgRight >= 4))
	{

	%>



<%

if(protocolId != null && (!EJBUtil.isEmpty(selectedEnrollId)))
{
%>

<Form name="sch" method=get action="patientschedule.jsp?nextpage=1&mode=M" onsubmit="return setFilter(document.sch,'P' ) ;"   >
<Input type="hidden" name="srcmenu" value=<%=src%>>
<Input type="hidden" name="selectedTab" value=<%=selectedTab%>>
<Input type="hidden" name="status" value=S>
<Input type="hidden" name="generate" <%=generate%>>
<Input type="hidden" name="pkey" id="pkey" value=<%=pkey%>>
<Input type="hidden" name="study" id="study" value=<%=study%>>
<Input type="hidden" name="statDesc" value=<%=statDesc%>>
<Input type="hidden" name="statid" value=<%=statid%>>
<Input type="hidden" name="studyVer" value=<%=studyVer%>>
<Input type="hidden" name="page" value=<%=page1%>>
<Input type="hidden" name="patProtId" value=<%=selectedEnrollId%>>
<input type="hidden" id="fdaStudy" name="fdaStudy" value="<%=fdaStudy%>">


<!--JM: 05May2008, Added for the #PS16--->
<Input type="hidden" name="calStatus" value="<%=calStatus%>">


<!-- Added the following for new CRf implementation: -->

<Input type="hidden" name="formPatprot" value=<%=selectedEnrollId%>>
<Input type="hidden" name="formStudy" value=<%=study%>>
<Input type="hidden" name="patStudyId" value=<%=study%>>
<Input type="hidden" name="formPatient" value=<%=pkey%>>


	<%

			//Modified by Manimaran for November Enhancement PS4.
			if((patStatSubType.trim().equalsIgnoreCase("lockdown")))
			{ %>
				<P class = "defComments"><FONT class="Mandatory"><%=MC.M_PatStdLkdown_CntChgStat%><%--Patient's <%=LC.Std_Study_Lower%> status is 'Lockdown'. You cannot change Event status.*****--%> </FONT>
				<A onclick="openWinStatus('<%=pkey%>','<%=patStudyStatpk%>','<%=pageRight%>','<%=study%>','<%=orgRight%>')"  href="#"  ><%=LC.L_Click_Here%><%--Click Here*****--%></A>
				<FONT class="Mandatory"> <%=MC.M_ToChg_TheStat%><%--to change the status.*****--%></FONT>
				</P>

		<%	}
		 %>

		<!-- patient protoocl header was here-->

<%
			String event_id="";
			String chain_id="";
			String event_type="";
			String name="";
			String eventDisplayName = "";
			String eventDisplayNameTrim ="";
			String notes="";
			String eventNotes="";
			String cost="";
			String cost_description="";
			String duration="";
			String user_id="";
			String linked_uri="";
			String fuzzy_period="";
			String msg_to="";
			String sts="";
			String description="";
			String displacement="";
			String org_id="";
			String fuzzy_dates = "";
			StringBuffer sbFilterWhereClause = new StringBuffer();
			String scheduleWhereForCRF = "";
			Hashtable htSchedule = new Hashtable();
			Hashtable htCRF = new Hashtable();
			Hashtable htCRFVisit = new Hashtable();
			
			String cssForMoreFilters = HideThisElem;
			String iconForMoreFilters = ShowMoreIcon;

//JM: blocked for consistency in P and S

			if (! EJBUtil.isEmpty(startdt))
			{
				startdt = startdt.toString().substring(6,10) + "-" + startdt.toString().substring(0,2) + "-" + startdt.toString().substring(3,5);
			}

				if (!"0".equals(year) || !"0".equals(month) || !"All".equals(event)
				        || !"".equals(evtStatus) || !"ALL".equals(dateRange)) {
				    cssForMoreFilters = ShowThisElem;
				    iconForMoreFilters = ShowLessIcon;
				}
				
	 		com.velos.esch.business.common.EventdefDao eventdao = new com.velos.esch.business.common.EventdefDao();
	 		eventdao = eventdefB.getScheduleEventsVisits(EJBUtil.stringToNum(selectedEnrollId));
			totalDefinedVisits = eventdao.getTotalDefinedVisitsInSchedule(EJBUtil.stringToNum(selectedEnrollId));

			java.util.ArrayList visits = eventdao.getVisit();
			java.util.ArrayList visitnames = eventdao.getVisitName();

			String visitDD="<option value=\"0\" Selected>"+LC.L_All/*All*****/+"</option>"  ;
			String visitName = "";


		 	for(int cnt=1; cnt<=visits.size();cnt++)
			{
				int  tempVisit = Integer.parseInt(visit);


				Integer integerVisit = new Integer(0);
				integerVisit = (Integer) visits.get(cnt-1) ;

				int intVisit = 0;
				intVisit = integerVisit.intValue();


				if (totalDefinedVisits > 0)	{
					visitName = visitnames.get(cnt-1).toString();
				}
				else {
					visitName =  "Visit"+cnt;
				}
				if ( tempVisit == intVisit  )
				{

						visitDD = visitDD +"<option value=" + intVisit +" Selected> " + visitName  +"</option>";


				}
				else
				{

						visitDD = visitDD +"<option value=" + intVisit +" > " + visitName  +"</option>";

				}
			}


%>
<%

			//get event names from eventdao - sonia

			java.util.ArrayList eventDescs = eventdao.getNames();
			//Virendra: Fixed Bug No.#4951, Removed duplicate entries
			HashSet hashSet = new HashSet(eventDescs);
			eventDescs = new ArrayList(hashSet) ;
	 		Collections.sort(eventDescs);

			//com.velos.esch.business.common.EventdefDao ctrldao = new com.velos.esch.business.common.EventdefDao();

			String eventsDD="<option value=\"All\" Selected>"+LC.L_All/*All*****/+"</option>"  ;



			for( int cnt=1; cnt<=eventDescs.size();cnt++)
			{
			 	String tempEvent = eventDescs.get(cnt - 1).toString() ;
			 	String tempEventSubString=eventDescs.get(cnt - 1).toString() ;

			 	// Mukul Bug #-4277  Fixed
				if(tempEventSubString.length()>50)
					tempEventSubString=tempEventSubString.substring(0,50)+"...";
					if ( tempEvent.trim().equals(event))
				{

					eventsDD = eventsDD +"<option value='" +eventDescs.get(cnt - 1) +"' Selected>" +tempEventSubString +" </option>";
				}
				else
					eventsDD = eventsDD +"<option value='" +eventDescs.get(cnt - 1) +"'>" +tempEventSubString +"</option>";
			}



	%>
<%if("patRoster".equals(calledFrom)){ %>
<Input type="hidden" name="calledFrom" value="patRoster">
<%} %>
<Input type="hidden" name="visitext" value="<%=visitxt%>">
<Input type="hidden" name="eventext"    value="<%=evttxt%>" >
<Input type="hidden" name="statustext"  value="<%=ststxt%>" >
<Input type="hidden" name="monthyrtext"  value="<%=durtxt%>" >


<Input type="hidden" name="filEvent"  value="<%=event%>" >
<Input type="hidden" name="filMonth"  value="<%=month%>" >
<Input type="hidden" name="filYear"  value="<%=year%>" >
<Input type="hidden" name="filVisit"  value="<%=visit%>" >
<Input type="hidden" name="filStatus"  value="<%=evtStatus%>" >
<Input type="hidden" name="filDateRange" id="filDateRange" value="<%=dateRange%>"></Input>

				<!--The Selected filters are:
			<U>Visit</u>: <B><I><%=visitxt%></I></B>&nbsp;&nbsp;&nbsp;&nbsp;<U>Month/Year</u>:<B><I><%=durtxt%></I></B>
			&nbsp;&nbsp;&nbsp;&nbsp;<u>Event</u>:<B><I><%=evttxt%></I></B>&nbsp;&nbsp;&nbsp;&nbsp;<u>Status</u>:<B><I><%=ststxt%></I></B>
			&nbsp;&nbsp;&nbsp;&nbsp;
			-->
		    <table border=0 cellPadding=1 cellSpacing=1 width="98%"><tr><td>
		  <%=LC.L_Select_Sch%><%--Select Schedule*****--%>:</label> <%= sbAvailableSchedulesDropDown.toString() %>
		  &nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Visit%><%--Visit*****--%>:</label>&nbsp;&nbsp;<select name="visit"><%=visitDD%></select>
          &nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="return showMoreFilters();"></a>&nbsp;<input type="image" onclick="return showMoreFilters();" id="moreFilters" name="moreFilters" title="<%=MC.M_AddFilters%>" src="<%=iconForMoreFilters%>"/>
		  &nbsp;&nbsp;&nbsp;&nbsp;<button type="submit"><%=LC.L_Search%></button>
		  <%if(calstat==EJBUtil.stringToNum(calstatus)){
		  if(study_cord>0){%>
		  <b><font size="3" color="red"><%=MC.M_Cal_Deact%></font></b>
		  <%}} %>
		    </td>
		    <td align="right">
				<%
				//user cannot change the event status if patient is lockdown
				//Modified by Manimaran for November Enhancement PS4.
				if(!(patStatSubType.trim().equalsIgnoreCase("lockdown")))
				{ %>
				<A HREF=# onClick='return editMultipleEvents("editMulEventDetails.jsp?patProtId=<%=selectedEnrollId%>&pkey=<%=pkey%>&studyId=<%=study%>","<%=pageRight%>","<%=orgRight%>")'><%=LC.L_Edit_MultiEvts%><%--EDIT MULTIPLE EVENTS*****--%></A>
				<%}%>
			</td> 
		    </tr></table>
		  <Input type="hidden" name="generateGoFlag" value=<%=generateGoFlag%>>

<%

			String prevMonth="";
			String thisMonth="";
			Integer newVisit ;
			Integer prevVisit = null ;
			int prevYr = 0;
			int visitCnt = 0;
			String start_dt="";
			String prev_dt="00/00/0000";
			visit ="";
			int crfCount = 0;
			Calendar cal2 = Calendar.getInstance();
			int currYear = cal2.get(cal2.YEAR);
			String ddDateRange = EJBUtil.getRelativeTimeDDFull("date_range\' id=\'date_range\' onChange=\'setDateRangeInAddlParams(this);clearMonthAndYear();", "", true);
%>

				<TABLE id="moreFilterTable" style="<%=cssForMoreFilters%>" border=0 cellPadding=1 cellSpacing=1 width="98%">
				<tr>
				<td bordercolor="gray" style="border-style:dotted;border-width:1px" nowrap="nowrap" width="400px">
				<%=LC.L_Date_Range%><%--Date Range*****--%>:&nbsp;<%=ddDateRange%>
				&nbsp;-<%=LC.L_Or_Lower%><%--or*****--%>-&nbsp;<%=LC.L_MonthOrYear%><%--Month/Year*****--%>:&nbsp;
				<Select name="month" id="month" onchange="setMonthInAddlParams(this);clearDateRange()" >
				<option value="0" Selected><%=LC.L_All%><%--All*****--%></option>
			    <% int cnt = 0;
        		for (cnt= 1; cnt<= 12;cnt ++)
				{
		 			int tempMonth =Integer.parseInt(month) ;


					if ( tempMonth == cnt )
					{%>
							<option value=<%=cnt%> Selected><%=cnt%></option>
				<%}
					else
					{%>
	        		<option value=<%=cnt%>><%=cnt%></option>
				<%} //end else
    		   } //end for of month%>
				</Select>
				<Select name="year" id="year" onchange="setYearInAddlParams(this);clearDateRange()" >
					<option value="0" Selected><%=LC.L_All%><%--All*****--%></option>
		    	<%
		    	StringBuffer yearOptSB = new StringBuffer();
				int i = currYear - 50;
				currYear = currYear + 10;
			    int count = 0;
    			for (count= currYear; count >= i ;count--) {
					int tempYear = Integer.parseInt(year);
		   			if (count == currYear) {
						if ( count == tempYear ) {
						    yearOptSB.append("<option value=").append(count).append(" Selected>");
						    yearOptSB.append(count).append("</option>");
						} else { 
						    yearOptSB.append("<option value=").append(count).append(" >");
						    yearOptSB.append(count).append("</option>");
						}
				    } else {
						if ( count == tempYear ) {
						    yearOptSB.append("<option value=").append(count).append(" Selected>");
						    yearOptSB.append(count).append("</option>");
						} else {
						    yearOptSB.append("<option value=").append(count).append(" >");
						    yearOptSB.append(count).append("</option>");
						}
			    	}
		        }  //end of for loop year %>
				<%=yearOptSB.toString()%>
				</select>
				</td><td>
			&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Event%><%--Event*****--%>:&nbsp;
			<Select name="event" id="event" onchange="setEventInAddlParams(this);" >	<%=eventsDD%>	</Select>
			&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Status%><%--Status*****--%>:&nbsp;	
			<%=dEvStat%></td>

			</tr>
</Table>
</Form>

<%
	StringBuffer visitIdMapSB = new StringBuffer();
	StringBuffer reverseVisitIdMapSB = new StringBuffer();
	StringBuffer numVisitsByMonthSB = new StringBuffer();
	for (int iX=0; iX<listOfVisitNamesByMonth.size(); iX++ ) {
	    ArrayList visitFKs = (ArrayList)listOfVisitFKsByMonth.get(iX);
	    for (int iY=0; iY<visitFKs.size(); iY++) {
	        visitIdMapSB.append("\"").append(iX).append("-").append(iY).append("\"");
	        visitIdMapSB.append(":").append(visitFKs.get(iY)).append(", ");
	        reverseVisitIdMapSB.append(visitFKs.get(iY)).append(":");
	        reverseVisitIdMapSB.append("\"").append(iX).append("-").append(iY).append("\"").append(", ");
	    }
	    numVisitsByMonthSB.append(iX).append(":").append(visitFKs.size()).append(", ");
	}
	if (visitIdMapSB.length() > 2 && ", ".equals(visitIdMapSB.substring(visitIdMapSB.length()-2))) {
	    visitIdMapSB.setLength(visitIdMapSB.length()-2);
	}
	if (reverseVisitIdMapSB.length() > 2 
	        && ", ".equals(reverseVisitIdMapSB.substring(reverseVisitIdMapSB.length()-2))) {
	    reverseVisitIdMapSB.setLength(reverseVisitIdMapSB.length()-2);
	}
	if (numVisitsByMonthSB.length() > 2 && 
	        ", ".equals(numVisitsByMonthSB.substring(numVisitsByMonthSB.length()-2))) {
	    numVisitsByMonthSB.setLength(numVisitsByMonthSB.length()-2);
	}
%>
<script>
    $j.ajaxSetup({cache:false});
    setEventInAddlParams($('event'));
    setDStatusInAddlParams($('dstatus'));
    setMonthInAddlParams($('month'));
    setYearInAddlParams($('year'));
    setDateRangeDD($('filDateRange'));
    setDateRangeInAddlParams($('date_range'));
    
    var getScheduleUrl = function(selMonth, selVisit) {
        // follow this file 'patientschedulebyvisit.jsp' to fix the bug 8217
    	var vUrl = 'patientschedulebyvisit.jsp?visitId='+visitIdMap[selMonth+"-"+selVisit]+
		'&pkey=<%=personPK%>&studyId=<%=EJBUtil.stringToNum(study)%>&availableSch=<%=StringUtil.htmlEncodeXss(selectedEnrollId)%>';
		if (addlParams['event'] != null && addlParams['event'] != 'All') {
			vUrl += '&event='+encodeURI(addlParams['event']);
		}
		if (addlParams['dstatus'] != null && addlParams['dstatus'] != '') {
			vUrl += '&dstatus='+encodeURI(addlParams['dstatus']);
		}
		if (addlParams['month'] != null && addlParams['month'] != '0') {
			vUrl += '&month='+encodeURI(addlParams['month']);
		}
		if (addlParams['year'] != null && addlParams['year'] != '0') {
			vUrl += '&year='+encodeURI(addlParams['year']);
		}
		if (addlParams['date_range'] != null && addlParams['date_range'] != '') {
			vUrl += '&date_range='+encodeURI(addlParams['date_range']);
		}
		if('<%=calledFrom%>'=='patRoster')
			vUrl+='&calldFrom=<%=calledFrom%>';
		return vUrl;
    }
    
	var visitIdMap = { <%=visitIdMapSB.toString()%> };
	var reverseVisitIdMap = { <%=reverseVisitIdMapSB.toString()%> };
	var numVisitsByMonth = { <%=numVisitsByMonthSB.toString()%> }
	var totalNumAccordions = <%=visitMonths.size()%>;
	var loadVisits = function() {
		var myCount = 0;
		var maxCount = 1; // load only up to this max to conserve memory
		for (var myMonth=0; myMonth<<%=listOfVisitNamesByMonth.size()%>; myMonth++) {
			for (var myVisit=0; myVisit<numVisitsByMonth[myMonth]; myVisit++) {
				var vUrl = 'patientschedulebyvisit.jsp?visitId='+visitIdMap[myMonth+"-"+myVisit]+
					'&pkey=<%=personPK%>&studyId=<%=EJBUtil.stringToNum(study)%>';
				if('<%=calledFrom%>'=='patRoster')
					vUrl+='&calldFrom=<%=calledFrom%>';
				$j('#month'+myMonth+'visit'+myVisit).load(getScheduleUrl(myMonth, myVisit));
				if (++myCount > maxCount) { return; }
			}
		}
	}

	var collapseThisMonth = function(selMonth) {
		for (var iX=0; iX<totalNumAccordions; iX++) {
			if (iX == selMonth) { continue; }
			$j("#accordion"+iX).accordion( "activate" , false);
		}
	}
	
  	var callAppServer = function(selMonth, selVisit) {
  	if (selMonth == undefined || selMonth == null) return;
  	if (selVisit == undefined || selVisit == null) return;
  	var active = $j( "#accordion"+selMonth ).accordion( "option", "active" );
  	var needToCall = false;
  	if (typeof(active) == "boolean" ) { needToCall = true; }
  	if (typeof(active) == "number" ) { if (selVisit != active) { needToCall = true; } }
  	if (needToCall) {
  		$j.ajax({ url:"patientscheduletracking.jsp?visitId="+visitIdMap[selMonth+"-"+selVisit] });
  	}
  	var myObj = $j('#month'+selMonth+'visit'+selVisit);
  	if (needToCall && myObj[0] && myObj[0].innerHTML.indexOf('Events...') > -1) {
	  		needToCall = true;
	  	} else {
	  		needToCall = false;
	  	}
  	if (needToCall) {
  	  var urlParam = "fk_visit="+visitIdMap[selMonth+"-"+selVisit]+"&patProtId=<%=patProtId%>";
  	         $j.ajax({
  		url:"fetchEventCount.jsp",
  		type:"GET",
  		async:false,
  		data:urlParam,
  		cache: false,
		success: function (data) {
  	  	myObj = $j('#month'+selMonth+'visit'+selVisit);
  	  	myObj[0].innerHTML='';
  	  	myObj[0].innerHTML=data;
  	  	
  	  		$j('#month'+selMonth+'visit'+selVisit).load(getScheduleUrl(selMonth, selVisit), 
  	  				function() { createEditVisitDialog(visitIdMap[selMonth+"-"+selVisit]); });
  	  	
  	  			},
  		error:function(data) { alert(data); return false; }
  	          });
		}
  	}

  	$j(document).ready(function() {
    	// loadVisits(); // Cache some visits
<%  if (lastSelVisitInSession != null) { %>
		var monthAndVisit = reverseVisitIdMap['<%=lastSelVisitInSession%>'];
		if (monthAndVisit) {
			var parts = monthAndVisit.split("-");
			if ($j("#accordion"+parts[0]) && $j('#month'+parts[0]+'visit'+parts[1])) {
				callAppServer(parts[0], parts[1]);
				$j("#accordion"+parts[0]).accordion("option", "active", parseInt(parts[1],10));
			}
		}
<%  } else {  // This AJAX is to remove LastSelVisit from session %>
		$j.ajax({ url:"patientscheduletracking.jsp" });
<%  }  %>
	});
</script>

<div id="bgPanel" class="ui-widget-content" style="padding:3px; width:98%; min-width:775px" >
<%
	SimpleDateFormat inFormat = new SimpleDateFormat("yyyy.MM");
	SimpleDateFormat outFormat = null;
	String appDateFormat = Configuration.getAppDateFormat();
	if (appDateFormat.indexOf("M") < appDateFormat.indexOf("y")) {
	    outFormat = new SimpleDateFormat("MMMMM yyyy");
	} else {
	    outFormat = new SimpleDateFormat("yyyy MMMMM");
	}
	for (int iMonth=0; iMonth<visitMonths.size(); iMonth++) {
	    StringBuffer monthDisplaySB = new StringBuffer();
	    String displayMonth = null;
	    try {
	        if (visitMonths.get(iMonth) == null || "".equals(visitMonths.get(iMonth)) ) {
	            displayMonth = LC.L_Not_Defined/*"Not Defined"*****/; 
	        } else  {
	            displayMonth = outFormat.format(inFormat.parse((String)visitMonths.get(iMonth)));
	        }
	    } catch(Exception e) {
	        displayMonth = LC.L_Not_Defined/*"Not Defined"*****/;
	    }
	    monthDisplaySB.append("<table><tr>");
	    monthDisplaySB.append("<td width='392px' align='left'>").append(displayMonth);
	    monthDisplaySB.append("&nbsp;"+LC.L_Visit/*Visit*****/+"</td>");
	    monthDisplaySB.append("<td width='101px' align='center'>"+LC.L_Suggested_Date/*Suggested Date*****/+"</td>");
	    monthDisplaySB.append("<td width='101px' align='center'>"+LC.L_Scheduled_Date/*Scheduled Date*****/+"</td>");
	    monthDisplaySB.append("<td width='151px' align='center'>"+LC.L_Visit_Window/*Visit Window*****/+"</td>");
	    monthDisplaySB.append("</tr></table>");
%>
  <label style="font-size:9pt"><%=monthDisplaySB.toString()%></label>
  <div id="accordion<%=iMonth%>">
<%
	    ArrayList listOfVisitNames = null;
	    ArrayList listOfStartDates = null;
	    ArrayList listOfActualDates = null;
	    ArrayList listOfVisitWindows = null;
	    ArrayList listOfEvtCounts = null;
	    ArrayList listOfDoneCounts = null;
	    ArrayList listOfPasts = null;
        if (listOfVisitNamesByMonth.size() > iMonth) {
            listOfVisitNames = (ArrayList)listOfVisitNamesByMonth.get(iMonth);
            listOfStartDates = (ArrayList)listOfStartDatesByMonth.get(iMonth);
            listOfActualDates = (ArrayList)listOfActualDatesByMonth.get(iMonth);
            listOfVisitWindows = (ArrayList) listOfVisitWindowsByMonth.get(iMonth);
            listOfEvtCounts = (ArrayList) listOfEvtCountsByMonth.get(iMonth);
            listOfDoneCounts = (ArrayList) listOfDoneCountsByMonth.get(iMonth);
            listOfPasts = (ArrayList) listOfPastsByMonth.get(iMonth);
        }
        if (listOfVisitNames == null) { listOfVisitNames = new ArrayList(); }
        if (listOfStartDates == null) { listOfStartDates = new ArrayList(); }
        if (listOfActualDates == null) { listOfActualDates = new ArrayList(); }
        if (listOfVisitWindows == null) { listOfVisitWindows = new ArrayList(); }
        if (listOfEvtCounts == null) { listOfEvtCounts = new ArrayList(); }
        if (listOfDoneCounts == null) { listOfDoneCounts = new ArrayList(); }
        if (listOfPasts == null) { listOfPasts = new ArrayList(); }
	    for (int iVisit=0; iVisit<listOfVisitNames.size(); iVisit++) {
	        StringBuffer visitDisplaySB = new StringBuffer();
	        visitDisplaySB.append("<span style='width:346px; position:absolute'>");
	        visitDisplaySB.append((String)listOfVisitNames.get(iVisit));
	        visitDisplaySB.append("</span>&nbsp;&nbsp;<span style='left:404px; position:absolute'>");
	        if (((String)listOfActualDates.get(iVisit)).length() > 0) {
		        visitDisplaySB.append((String)listOfStartDates.get(iVisit));
	        } else {
	            visitDisplaySB.append(LC.L_Not_Defined/*"Not Defined"*****/);
	        }
	        visitDisplaySB.append("</span>&nbsp;&nbsp;<span style='left:504px; position:absolute'>");
	        if (((String)listOfActualDates.get(iVisit)).length() > 0) {
	            visitDisplaySB.append((String)listOfActualDates.get(iVisit));
	        } else {
	            visitDisplaySB.append(LC.L_Not_Defined/*"Not Defined"*****/);
	        }
	        visitDisplaySB.append("</span>&nbsp;&nbsp;<span style='left:598px; position:absolute'>");
	        if (((String)listOfVisitWindows.get(iVisit)).length() > 0 ) {
	            visitDisplaySB.append((String)listOfVisitWindows.get(iVisit));
	        }
	        visitDisplaySB.append("</span>");
	        int doneCount = ((Integer)listOfDoneCounts.get(iVisit)).intValue();
	        int evtCount = ((Integer)listOfEvtCounts.get(iVisit)).intValue();
%>	    
	<h3 onclick='callAppServer(<%=iMonth%>,<%=iVisit%>,addlParams);collapseThisMonth(<%=iMonth%>);'>
	<a href="javascript:void(0);">
	<% if (((Integer)listOfPasts.get(iVisit)).intValue() == 1) { %>
	<img src='images/exclamation.png' width='15px' height='15px' vspace="-5" align="bottom" border="0"/>
	<% } else if (doneCount > 0 && doneCount == evtCount) { %>
	<img src='images/greencheck.png' width='15px' height='15px' vspace="-5" align="bottom" border="0"/>
	<% } else { %>
	<img style="visibility:hidden" src='images/greencheck.png' width='15px' height='15px' vspace="-5" align="bottom" border="0"/>
	<% }  %>
	&nbsp;&nbsp;<%=visitDisplaySB.toString()%></a></h3>
	<div id="month<%=iMonth%>visit<%=iVisit%>">
		<p><%=LC.L_Loading%> Events<%--Loading*****--%>...<img class="asIsImage_progbar" src="../images/jpg/loading_pg.gif" /></p>
	</div>
<%      } // End of visit loop %>
  </div>
  <br/>
<%	} // End of month loop %>
<%  if(visitMonths == null || visitMonths.size() == 0) { %>
	<div><p style="font-size:9pt"><%=MC.M_NoRecordsFound%><%--No records found*****--%></p></div>
<%  } %>
</div> <%-- End of div class="ui-widget-content" --%>

<%
 }
 else if (EJBUtil.isEmpty(protocolId ) && ( !EJBUtil.isEmpty(selectedEnrollId)))
 {
 	//else for if of protocolid != null

 	if (schedulesCount > 0)
    	{
		%>
		<Form name="sch2" method=get action="patientschedule.jsp?nextpage=1&mode=M"  >
		<Input type="hidden" name="srcmenu" value=<%=src%>>
			<Input type="hidden" name="selectedTab" value=<%=selectedTab%>>
			<Input type="hidden" name="status" value=S>
			<Input type="hidden" name="generate" <%=generate%>>
			<Input type="hidden" name="pkey" value=<%=pkey%>>
			<Input type="hidden" name="page" value=<%=page1%>>
			<Input type="hidden" name="patProtId" value=<%=selectedEnrollId%>>

			<!-- Added the following for new CRf implementation: -->

			<Input type="hidden" name="formPatprot" value=<%=selectedEnrollId%>>
			<Input type="hidden" name="formStudy" value=<%=study%>>
			<Input type="hidden" name="patStudyId" value=<%=study%>>
			<Input type="hidden" name="formPatient" value=<%=pkey%>>


		<B> <%=MC.M_SelDiscnt_PatSch%><%--Select a Discontinued <%=LC.Pat_Patient%> Schedule*****--%>: </B>  <%= sbAvailableSchedulesDropDown.toString() %>&nbsp;<button type="submit"><%=LC.L_Search%></button>
		</form>

		<% } %>


<P class="defComments">
	<%=MC.M_PatNotAssign_ToPcolCal%><%--This <%=LC.Pat_Patient_Lower%> has not been assigned to any Protocol Calendar.*****--%>
</P>
<%
  }
    else
    {
    	%>

	  	<P class = "defComments">
			<%=MC.M_NoStdStatAdd_PatAssignPCol%><%--There is no <%=LC.Std_Study_Lower%> status added for this <%=LC.Pat_Patient_Lower%>. Please add a <%=LC.Std_Study_Lower%> status before assigning a Protocol.*****--%>
		</P>

		<%
    }
%>

<script>
	window.onload = function() { 
		var ddList = document.getElementById('availableSch');
		if (ddList)
			ddList.options.selectedIndex = <%=selectedSchedule%>;
	};
</script>

<%
 }else{
%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
<%
}
}
else{
	%>
	  <jsp:include page="accessdenied.jsp" flush="true"/>
	<%
	}
} //end of if session times out
else
{
%>
 <jsp:include page="timeout.html" flush="true"/>
  <%
} //end of else body for page right
%>
</div>
  <div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</body>
</html>
