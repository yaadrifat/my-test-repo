<%@page import="com.velos.eres.widget.business.common.ResubmDraftDao"%>
<%@page import="com.velos.eres.web.study.StudyJB"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC,com.velos.eres.service.util.DateUtil"%> 
<%@page import="org.json.JSONObject,org.json.JSONArray" %>
<%@page import="com.velos.eres.widget.service.util.FlxPageCache, com.velos.eres.service.util.StringUtil"%>
<%@page import="com.velos.eres.web.grpRights.GrpRightsJB"%>
<%@page import="com.velos.eres.web.studyRights.StudyRightsJB"%>
<%@page import="com.velos.eres.business.common.*,com.velos.eres.service.util.*"%>
<%@page import="com.velos.eres.widget.business.common.UIFlxPageDao"%>
<%@page import="com.velos.eres.compliance.web.ComplianceJB" %>
<%@page import="java.util.*,java.io.*"%>
<%@page import="com.velos.eres.web.submission.SubmissionStatusJB" %>
<%!
static final String STR_TRUE = "true", STR_FALSE = "false";
%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="grpRights" scope="session" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id="stdRights" scope="session" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="studyJB" scope="session" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="complianceJB" scope="request" class="com.velos.eres.compliance.web.ComplianceJB"/>
<jsp:useBean id="flxPageJB" scope="request" class="com.velos.eres.widget.service.util.FlxPageJB"/>
<jsp:include page="ui-include.jsp" flush="true"/>
<script>
$j(document).ready(function(){
	var screenWidth = screen.width;
	var screenHeight = screen.height;

	if(screenHeight < 769){
		$j("div.flxPageContainer").css({height:"500px"});
		$j("div.flxPageScroll").css({height:"400px"});
	} else {
		$j("div.flxPageContainer").css({height:"555px"});
		if (navigator.userAgent.indexOf("MSIE") > -1) {
			$j("div.flxPageScroll").css({height:"465px"});
		} else {
			$j("div.flxPageScroll").css({height:"700px"});
		}
	}
});
</script>
<%
String isNewAmendment="false";
session.setAttribute("isNewAmendment", isNewAmendment);
HttpSession tSession = request.getSession(false); 
if (!sessionmaint.isValidSession(tSession)) {
	return;
}
CodeDao cdDao2 = new CodeDao();
int studyCond2Id = cdDao2.getCodeId("studyidtype", "studyCond2");
int studyAdmDivId = cdDao2.getCodeId("studyidtype", "studyAdmDiv");

String userId = (String)tSession.getAttribute("userId");
String studyId = (String)request.getParameter("studyId");
String mode = "N"; 
String createType = (String)request.getParameter("createType");
if (!"A".equals(createType)) { createType = "D"; }

if(studyId == null || studyId.equals("null")){
	studyId = "0";
	studyJB = null;
} else {
	if (StringUtil.stringToNum(studyId) < 1){
		studyId="0";
		studyJB = null;
	} else {
		studyJB = new StudyJB();
		studyJB.setId(StringUtil.stringToNum(studyId));
		studyJB.getStudyDetails();
		mode = "M";
		createType = studyJB.getStudyCreationType();
		HashMap sysDataMap = studyJB.getStudySysData(request);
		if (null == sysDataMap)
			return;
		String autoGenStudy = (String) sysDataMap.get("autoGenStudy");
		tSession.setAttribute("autoGenStudyForJS",autoGenStudy);
		tSession.setAttribute("studyNo",sysDataMap.get("studyNo"));
		
		String roleCodePk = (String) sysDataMap.get("studyRoleCodePk");
		tSession.setAttribute("studyRoleCodePk",roleCodePk);

		stdRights = (StudyRightsJB) sysDataMap.get("studyRights");
		tSession.setAttribute("studyRights",stdRights);

		int stdRight = StringUtil.stringToNum((sysDataMap.get("stdSummRight").toString()));
		tSession.setAttribute("studyRightForJS",stdRight);
	}
	tSession.setAttribute("studyId",studyId);
	tSession.setAttribute("studyJB",studyJB);
}

int accountId = StringUtil.stringToNum((String)tSession.getAttribute("accountId"));
int pageRight = 0;

if (StringUtil.stringToNum(studyId) == 0){
	grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));
} else {
	//******************GET STUDY TEAM RIGHTS and put the same in session, this will be a separate obj as no study is selected //***************************************************************
    TeamDao teamDao = new TeamDao();
    teamDao.getTeamRights(StringUtil.stringToNum(studyId),StringUtil.stringToNum(userId));
    ArrayList tId = teamDao.getTeamIds();
    if (tId.size() == 0)
	{
    	pageRight=0 ;
    }
	else
	{
    	stdRights.setId(StringUtil.stringToNum(tId.get(0).toString()));
   	 	ArrayList teamRights ;
		teamRights  = new ArrayList();
		teamRights = teamDao.getTeamRights();

		stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
		stdRights.loadStudyRights();

    	tSession.setAttribute("studyRights",stdRights);
    	if ((stdRights.getFtrRights().size()) == 0)
		{
    	 	pageRight= 0;
    	}
		else
		{
    		pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYSUM"));
    	}
    }
}
if (!StringUtil.isAccessibleFor(pageRight, 'V')) {
%>	
	<jsp:include page="accessdenied.jsp" flush="true"/>
<%
	return;
}

FlxPageCache.reloadStudyPage();

String jPageAvailSections = FlxPageCache.getStudyPageSections();
%>
<link rel="stylesheet" type="text/css" href="styles/flexPage.css">

<script>
var pageSectionRegistry = <%=jPageAvailSections.toString()%>;
var activeSection = 0;

var showNotification = function(){
	
};

var sectionHeaderClick = function(sectionNo, force){
	activeSection = sectionNo;
	if($j("#sectionAnchor"+sectionNo).length>0){
		if($j.trim($j("#sectionAnchor"+sectionNo).text())==$j.trim("Locations, Study Team and Collaborators")){
			sectionHeaderForStudyTeam(sectionNo);
			
		}
		else if($j.trim($j("#sectionAnchor"+sectionNo).text())==$j.trim("IND/IDE Information")){
			sectionHeaderForIndIde(sectionNo);
		}
	}
	if (sectionNo < 1) return;

	force = (!force)? false : force;
	if (!force && !document.getElementById('sectionLoading'+ sectionNo)){
		return;
	}
	
	var vUrl = 'flexStudyPageBySection.jsp?sectionNo='+sectionNo +'&studyId=<%=StringUtil.stringToNum(studyId)%>';
	$j('#sectionBody'+ sectionNo).load(vUrl, function(){});	
	return;
};

var sectionHeaderForStudyTeam = function(sectionNo) {
	if ($j('#sectionBody'+ sectionNo).html()) {
		return;
	}
	var vUrl = 'flexStudyPageSectStudyTeam.jsp?sectionNo='+sectionNo +'&studyId=<%=StringUtil.stringToNum(studyId)%>';
	$j('#sectionBody'+ sectionNo).load(vUrl, function(){});
	return;
}

var sectionHeaderForIndIde = function(sectionNo) {
	if ($j('#sectionBody'+ sectionNo).html()) {
		return;
	}
	var vUrl = 'flexStudyPageSectStudyIndIde.jsp?sectionNo='+sectionNo +'&studyId=<%=StringUtil.stringToNum(studyId)%>';
	$j('#sectionBody'+ sectionNo).load(vUrl, function(){});
	return;
}

var loadSections = function(){
	var secCount = 0;
	for(var indx=0; indx < pageSectionRegistry.length; indx++){
		var sectionJSObject = pageSectionRegistry[indx];
		pageSections[indx] = sectionJSObject;
		secCount++;
	}
	return pageSections;
};
  
var createSectionAccordion =  function(sectionNo){
	$j('#sectionAccordion'+sectionNo)
    .accordion('destroy')
    .accordion({
		header: "> div > h3",
		autoHeight: false, collapsible: true, active: false, fillSpace: false, animated: false
	});
};

var getPageSectionUrl = function(selSection) {
	var vUrl = 'flexStudyPageBySection.jsp?sectionNo='+selSection +'&studyId=<%=StringUtil.stringToNum(studyId)%>';
	
	return vUrl;
}

var fldWDataCountArray = [];

var secFldCountArray = [];

var showAllSectionProgressBar = function() {
	for (var indx=0; indx < pageSectionRegistry.length; indx++) {
		var sectionNo = indx+1;
		showSectionProgressBar(sectionNo, fldWDataCountArray[sectionNo], secFldCountArray[sectionNo]);
	}
}

var renderSections = function(){
	$j('#sectionAccordions').html('');
	var additionalSectionHTML = '';
	for (var indx=0; indx < pageSectionRegistry.length; indx++){
		var sectionNo = indx+1;
		var jsSectionObject = pageSectionRegistry[indx];

		var secFldCount = 0;
		var lablesOnlyCount=0;
		var secType = jsSectionObject['secType'];
		secType = (!secType)? "generic" : secType;

		if (secType == "generic"){
			var sectionFldArray = jsSectionObject['fields'];
			sectionFldArray = (!sectionFldArray)? [] : sectionFldArray;
			secFldCount = sectionFldArray.length;
			for (var indx1=0; indx1 < sectionFldArray.length; indx1++){
				var sectionFldArrayObject = sectionFldArray[indx1];
				var labelonly = sectionFldArrayObject['labelOnly'];
				if(labelonly == 1) {
					lablesOnlyCount++;
				}
			}
			secFldCount = secFldCount - lablesOnlyCount;
		} else {
			secFldCount = "";
		}
		
		additionalSectionHTML += '<div id="sectionAccordion'+ sectionNo +'" onClick="sectionHeaderClick('+sectionNo+');">'
		+'<div id="sectionHead'+ sectionNo +'" class="group">'
		+'<h3 id="sectionHeader'+ sectionNo +'">'
		+'<table width="100%">'
		+'<tr>'
		+'<td width="70%">'
		+'<input type="hidden" id="sectionSeq" name="sectionSeq" value="'+ sectionNo +'"/>'
		+'<a id="sectionAnchor'+ sectionNo +'" href="#" >'+jsSectionObject.title+'&nbsp;&nbsp;'
		+'<span id="sectionTitleSpan'+ sectionNo +'" style="display:none"><input type="text" id="sectionTitle" name="sectionTitle" value="'+jsSectionObject.title+'"/></span>&nbsp;'
		+'<input type="hidden" id="secFldWDataCount'+ sectionNo +'" value=""/>'
		+'<input type="hidden" id="secFldCount'+ sectionNo +'" value="'+secFldCount+'"/>'
		+'</a>'
		+'</td>'
		+'<td id="secFldWDataCountTD'+sectionNo+'" align="right" width="10%"></td>'
		+'<td align="right" width="10%">'
		+'<div id="progressBar'+ sectionNo +'"></div>'
		+'</td>'
		+'<td id="secFldCountTD'+sectionNo+'" align="left" width="5%">'+secFldCount+'</td>'
		+'</tr>'
		+'</table>'
		+'</h3>'
		+'<div id="sectionBody'+ sectionNo +'">'
		+'<br/><p id="sectionLoading'+ sectionNo +'">Loading...<img style="width:200px;" src="../images/jpg/loading_pg.gif"/></p>'
		+'</div>'
		+'</div>'
		+'</div>';

		$j('#sectionAccordions').append(additionalSectionHTML);
		additionalSectionHTML = '';

		createSectionAccordion(sectionNo);
	}
	//openFirstSection(1);
	
	//$j('#sectionAnchor'+activeSection).trigger("click");
    //sectionHeaderClick(activeSection);
};

var openFirstSection = function() {
	sectionHeaderClick(1);
	$j('#sectionAccordion1').accordion({active:0});
}

var replicateESign = function(id) {
	$('esign').value = $(id).value;
	ajaxvalidate('misc:esign', 4, id+'_eSign',
            '<%=LC.L_Valid_Esign%>', '<%=LC.L_Invalid_Esign%>', 'sessUserId');
};

var showSectionProgressBar = function (sectionNo, fldWDataCount, secFldCount){
	if (!secFldCount || secFldCount == 0) { return; }
	
	//console.log("Section: "+sectionNo+" secFldCount: "+secFldCount+ " fldWDataCount: "+fldWDataCount);
	$j("#secFldWDataCount"+sectionNo).val(fldWDataCount);
	$j("#secFldWDataCountTD"+sectionNo).html(fldWDataCount);

	$j( "#progressBar" + sectionNo).progressbar({ max: secFldCount, value: fldWDataCount });
	var progressbar = $j( "#progressBar" + sectionNo ),
      	progressbarValue = progressbar.find( ".ui-progressbar-value" );
	
	progressbarValue.css({
         "background": '#87ceeb'
       });
	progressbar.css({
		"height": "7px"
       });
};
</script>
<%--Include JSP --%>
<jsp:include page="studyScreenInclude.jsp" flush="true"/>
<jsp:include page="studySubmissionScreenInclude.jsp" flush="true"/>
<%----%>
<%
// First check whether the study should be frozen based on study status history
boolean isLocked= complianceJB.getIsLockedStudy();

// Second check whether the study should be frozen based on a flag which is set by the versioning & freezing logic
if (!isLocked) {
	if (studyJB != null && "1".equals(studyJB.getStudyIsLocked())) {
		isLocked = true;
	}
}

boolean isFrozen = false;
String isSubmittedWithNoAck = null;
UIFlxPageDao uiFlxPageDao = new UIFlxPageDao();
// Third check whether the study was recently submitted or resubmitted based on submission status

boolean isIrbApproved = complianceJB.getIsIrbApproved();
if (!isIrbApproved) {
	/*
	int submStatusCodelstPk = uiFlxPageDao.getFlexLatestSubmissionStatus(StringUtil.stringToNum(studyId));
	if (submStatusCodelstPk > 0) {
		CodeDao submCodeDao = new CodeDao();
		String submStatusSubtype = submCodeDao.getCodeSubtype(submStatusCodelstPk);
		if ("submitted".equals(submStatusSubtype) || "resubmitted".equals(submStatusSubtype)) {
			SubmissionStatusJB submissionStatusJB = new SubmissionStatusJB();
			submissionStatusJB.getSubmissionStatusDetails(uiFlxPageDao.getSubmissionStatusPk());
			Date submStatDate = submissionStatusJB.getSubmissionStatusDate();
			long timeDiff = System.currentTimeMillis() - submStatDate.getTime();
			System.out.println("timeDiff="+timeDiff);
			isSubmittedWithNoAck = (timeDiff < 600000L)? STR_FALSE : STR_TRUE;
		}
	}*/
}

boolean showLockedProtocolVersion = false;
boolean showLockedResubmVersion = false;
ResubmDraftDao resubmDraftDao = new ResubmDraftDao();
UIFlxPageDao uiFlxDao = new UIFlxPageDao();
if (isIrbApproved && uiFlxDao.chkPageSubmited(StringUtil.stringToNum(studyId))){
	showLockedProtocolVersion =  true;
	showLockedResubmVersion = false;
} else {
	if (isLocked){
		if (StringUtil.isEmpty(isSubmittedWithNoAck)){
			// Study is frozen but not submitted yet 
			int highestResubmDraftVersion = -1;
			highestResubmDraftVersion = resubmDraftDao.getHighestResubmDraftVersion("er_study", StringUtil.stringToNum(studyId));
			if (highestResubmDraftVersion > 0){
				// Re-submission record exists.
				showLockedProtocolVersion =  false;
				showLockedResubmVersion = true;
				isFrozen = true;
			} else {
				// 1. Studies submitted only once 
				// 2. Old studies which are locked but do not have re-submission draft record (old data prior to re-submission draft enh)
				showLockedProtocolVersion =  true;
				showLockedResubmVersion = false;
			}
		} else {
			if (STR_FALSE.equals(isSubmittedWithNoAck)){
				showLockedProtocolVersion =  true;
				showLockedResubmVersion = false;
			} else if (STR_TRUE.equals(isSubmittedWithNoAck)){
				showLockedProtocolVersion =  false;
				showLockedResubmVersion = false;
			}			
		}
	}
}

if (showLockedProtocolVersion){
	out.println(uiFlxPageDao.getFlexPageHtmlOfHighestVersion("er_study", StringUtil.stringToNum(studyId)));
	return;
} else {
	if (showLockedResubmVersion){
		out.println(resubmDraftDao.getResubmDraftPageHtmlOfHighestVersion("er_study", StringUtil.stringToNum(studyId)));
		return;
	}
}

if (! isFrozen) {
	flxPageJB.setStudyId(studyId);
	flxPageJB.setStudyJB(studyJB);
	flxPageJB.setDefUserGroup((String)tSession.getAttribute("defUserGroup"));
	flxPageJB.setHttpSession(tSession);
%>
<form name="studyScreenForm" id="studyScreenForm" method="post" action="#" onSubmit="return studyScreenFunctions.validateStudyScreen();">
	<input type="hidden" id="formStudy" name ="formStudy" value="<%=studyId%>" />
	<input type="hidden" id="mode" name ="mode" value="<%=mode%>" />
	<input type="hidden" id="createType" name ="createType" value="<%=createType != null ? createType : ""%>" />
	<div class="flxPageFixedTop">
	<table id="saveButtonRow" width="99%" >
			<tr style="float:top; ">
				<td align="right">
					<button type="button" id="toggleExpandCollapseAll" alt=""  title="Expand All">Expand All</button>			
					<%--<img id="toggleExpandCollapseAll" src="../images/jpg/ListExpand.gif"  alt="" class="asIsImage" title="Expand All" />&nbsp; --%>
				</td>
				<td align="right">
					<%if ((mode.equals("N") && StringUtil.isAccessibleFor(pageRight, 'N')) || (mode.equals("M") && StringUtil.isAccessibleFor(pageRight, 'E'))){%>
						<jsp:include page="submitBar.jsp" flush="true">
						<jsp:param name="displayESign" value="N"/>
						<jsp:param name="formID" value="studyForm"/>
						<jsp:param name="showDiscard" value="N"/>
						<jsp:param name="showSubmit" value="N"/>
						<jsp:param name="showSave" value="Y"/>
						</jsp:include>
					<%}%>
				</td>
			</tr>
	</table>
	</div>
	<SCRIPT LANGUAGE="JavaScript">
	var isIE = jQuery.browser.msie;
	var screenWidth = screen.width;
	var screenHeight = screen.height;
	if(screenWidth>1280 || screenHeight>1024)
	{
		if(isIE==true)
		{
			document.write('<div class="flxPageScroll" style="width:85%;height:64.5%;overflow:auto;position:absolute;border-bottom: 20%;">')
		}
		else
		{
			document.write('<div class="flxPageScroll" style="width:85%;height:64.5%;overflow:auto;position:absolute;border-bottom: 20%;">')
		}
	}
	else
	{
		if(isIE==true)
		{
			document.write('<div class="flxPageScroll" style="width:85%;height:64.5%;overflow:auto;position:absolute;border-bottom: 20%;">')
		}
		else
		{
			document.write('<div class="flxPageScroll" style="width:85%;height:64.5%;overflow:auto;position:absolute;border-bottom: 20%;">')
		}		
	}
</SCRIPT>
	
	<%if(!"LIND".equals(CFG.EIRB_MODE) && "Y".equals(CFG. FLEX_MODE)){ %>
	<table id="fixHeaderFooterTbl" style="padding-left:15px;" width="99%" cellspacing="0" cellpadding="0" border="0" class="midAlign">
	<%}else{ %>
	<table id="fixHeaderFooterTbl" width="99%" cellspacing="0" cellpadding="0" border="0" class="midAlign">
	<%} %>
				<tr>
					<td colspan="2">
						<div onmouseout='return nd();'>
						    <div onmouseout='return nd();'>
						        <div style="margin-top:0em">
						             <div style="margin-top:0em" onmouseout='return nd();'>
						             <%=flxPageJB.generateFlxHtml()%>
						             <!--       <div id="sectionAccordions"></div>    -->
						             </div>
							    </div>
						    </div>
						    <div onmouseout='return nd();'>
						        <a href="#" id="end_of_gadget_home"></a>
						        <input id="esign" name="esign" type="hidden" ></input> 
						    </div>
						</div>
					</td>
				</tr>
			<tr>
				<td style="position:absolute; right: 34%">
					<%if ((mode.equals("N") && StringUtil.isAccessibleFor(pageRight, 'N')) || (mode.equals("M") && StringUtil.isAccessibleFor(pageRight, 'E'))){%>
						<jsp:include page="submitBar.jsp" flush="true">
						<jsp:param name="displayESign" value="N"/>
						<jsp:param name="formID" value="studyForm"/>
						<jsp:param name="showDiscard" value="N"/>
						<jsp:param name="showSubmit" value="N"/>
						<jsp:param name="showSave" value="Y"/>
						</jsp:include>
					<%}%>
				</td>
			</tr>
	</table>
	</div>
<%if ((mode.equals("N") && StringUtil.isAccessibleFor(pageRight, 'N')) || (mode.equals("M") && StringUtil.isAccessibleFor(pageRight, 'E'))){%>
	<br>
	<%-- <jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="studyForm"/>
		<jsp:param name="showDiscard" value="N"/>
	</jsp:include> --%>

<%}%>
</form>
<div id="tooltip" style="z-index:100;"></div>
<%} //!isFrozen %>
<script src="./js/velosCustom/flexStudySectionScreen.js"></script>
<script>
<% if ("M".equals(mode)) { %>
$j(".BrowserBotN_RR_1").removeClass("BrowserBotN_RR_1").addClass("BrowserBotN_S_3");
<% } else { %>
$j(".BrowserBotN_RR_1").removeClass("BrowserBotN_RR_1").addClass("BrowserBotN_RC_1");
<% }%>

jQuery(function() {
	renderSections();
	$j('#toggleExpandCollapseAll').click(studySubmScreenFunctions.toggleExpandCollapseAllClick);
});

$j(document).ready(function(){
	moreStudyDetFunctions.fixTextAreas();
	//showAllSectionProgressBar();
	studyFunctions.callAjaxGetSecondTareaDD(studyFunctions.formObj,<%=studyAdmDivId%>,<%=studyCond2Id%>);
	$j("#studyScreenForm table > tbody > tr > span ").each(function(){
		if($j(this).hasClass("mdaccReqd")){
			$j(this).remove();
		}
	});
	
	$j("#studyScreenForm h3 ").each(function(){
		$j(this).bind("click",function(){
			$j("#studyScreenForm .activeSection ").each(function(){
				$j(this).removeClass("activeSection");
			});
			if($j(this).hasClass("ui-state-active")){
				$j(this).addClass("activeSection");
			}
		});
	});
});
<%if(!"LIND".equals(CFG.EIRB_MODE) && "Y".equals(CFG. FLEX_MODE) && mode.equals("N")){ %>
$j("#workflowpan").css({display:'none'});
$j("Table.unselectedTab TD").css({height:'18px'});
$j("Table.selectedTab TD").css({height:'18px'});
<%} if(!"LIND".equals(CFG.EIRB_MODE) && "Y".equals(CFG. FLEX_MODE) && mode.equals("M")){%>
$j(".tabBorder").css({width:'100%'});
$j("#fixHeaderFooterTbl").css({width:'100%'});
$j("Table.unselectedTab TD").css({height:'18px'});
$j("Table.selectedTab TD").css({height:'18px'});
<%}%>
</script>
