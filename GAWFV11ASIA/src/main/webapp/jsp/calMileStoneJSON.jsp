<%--
* File Name 	: calMileStoneJSON.jsp 
* Created on	: 2/24/11
* Created By	: YogeshKumar/AshuKhatkar
* Enhancement	: 8_10.0DFIV12 
* Purpose		: Genrate JSON data to display Event-Visit Grid .
*
*
--%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList,java.util.HashMap,org.json.*"%>
<%@page import="com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>
<%@page import="com.velos.esch.business.common.EventdefDao"%>
<%@page import="com.velos.esch.business.common.EventAssocDao"%>


<%@page import="com.velos.esch.business.common.SchCodeDao"%><jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="protVisitB" scope="request" class="com.velos.esch.web.protvisit.ProtVisitJB"/>
<jsp:useBean id="visitdao" scope="request" class="com.velos.esch.business.common.ProtVisitDao"/>

<%
    request.setCharacterEncoding("UTF-8");
    response.setContentType("application/json");
    // response.setContentType("text/html");

	HttpSession tSession = request.getSession(true);
    JSONObject jsObj = new JSONObject();
    if (!sessionmaint.isValidSession(tSession)) {
        // Session is invalid; print an error and exit
        response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");
        jsObj.put("error", new Integer(-1));
        jsObj.put("errorMsg", MC.M_UsrNot_LoggedIn/*"User is not logged in."*****/);
	    out.println(jsObj.toString());
        return;
    }
    
	String protocolId = request.getParameter("protocolId");
	if(protocolId == "" || protocolId == null || protocolId.equals("null") || protocolId.equals("")) {
	    // A valid protocol ID is required; print an error and exit
        jsObj.put("error", new Integer(-2));
        jsObj.put("errorMsg", MC.M_PcolId_Invalid/*"Protocol ID is invalid."*****/);
	    out.println(jsObj.toString());
	    return;
	}
	
    // Define page-wide variables here
    StringBuffer sb = new StringBuffer();
    String calledFrom = request.getParameter("calledFrom");
    String visitNameId = request.getParameter("visitList");
	if (visitNameId==null) visitNameId = "";
	
	String evtName = "";
	evtName =request.getParameter("searchEvt");
	evtName=(evtName==null)?"":evtName;
	
    boolean isLibCal = false;
    boolean isStudyCal = false;
    String protocolName = null;
    String calStatus = null;
    int errorCode = 0;
    ArrayList eventIds = null, eventNames = null;
    ArrayList eventVisitIds = null;
    ArrayList eventOfflines = null;
    ArrayList newEventIds = null;/*YK :: DFIN12 BUG #5954*/
   
	ArrayList protocolIds=null;
	ArrayList names=null ;
	ArrayList descriptions= null;
	ArrayList eventTypes=null;
	ArrayList eventDurs= null;
	ArrayList displacements= null;
	ArrayList orgIds= null;
	ArrayList costs= null;
	ArrayList monthsArr = null; //SV, 10/25/04
	ArrayList weeksArr = null ;
	ArrayList daysArr = null ;
	
	ArrayList categoryIds = null;
	ArrayList sequnces = null;
	ArrayList flagsStrings = null;
	ArrayList offLineFlags = null;
	ArrayList hideFlags = null;
	ArrayList facilityIds = null; //KV:SW-FIN2c
	ArrayList actualeventCosts = null; //KV: SW-FIN2b
  
	if ("S".equals(calledFrom)) {
        isStudyCal = true;
    } else {
        jsObj.put("error", new Integer(-3));
        jsObj.put("errorMsg", MC.M_CalType_Miss/*"Calendar Type is missing."*****/);
	    out.println(jsObj.toString());
        return;
    }
    String calStatusPk = "";
    SchCodeDao scho = new SchCodeDao();
	if (isStudyCal) {
        EventAssocDao assocdao = eventassocB.getEventVisitMileStoneGrid(EJBUtil.stringToNum(protocolId));/*YK :: DFIN12 BUG #5954*/
        eventIds = assocdao.getEvent_ids();
        newEventIds = assocdao.getNew_event_ids();/*YK :: DFIN12 BUG #5954*/
   		eventVisitIds = assocdao.getEventVisits();
   		eventNames = assocdao.getNames();
   		costs=assocdao.getCosts();
   		eventOfflines = assocdao.getOfflineFlags(); // Unique to event_assoc, not in event_def
   		eventassocB.setEvent_id(EJBUtil.stringToNum(protocolId));
   		eventassocB.getEventAssocDetails();
   		protocolName = eventassocB.getName();
   			//KM-#DFin9
		calStatusPk = eventassocB.getStatCode();
		calStatus = scho.getCodeSubtype(EJBUtil.stringToNum(calStatusPk)).trim();
    }

    
    ArrayList visitIds = null;
	ArrayList visitNames = null;
	ArrayList visitDisplacements = null;
	String visitId = null;
	String visitName = null;
	
	visitdao = protVisitB.getProtocolVisits(EJBUtil.stringToNum(protocolId));
	visitIds = visitdao.getVisit_ids();
	visitNames = visitdao.getNames();
	visitDisplacements = visitdao.getDisplacements();

    int noOfEvents = eventIds == null ? 0 : eventIds.size();
    if (protocolName == null) { protocolName = ""; }
    int noOfVisits = visitIds == null ? 0 : visitIds.size();
    
    
    jsObj.put("error", errorCode);
    jsObj.put("errorMsg", "");
    jsObj.put("noOfEvents", noOfEvents);
    
    JSONArray jsEvents = new JSONArray();
    for (int iX=0; iX<eventIds.size(); iX++) {
        JSONObject jsObj1 = new JSONObject();
        jsObj1.put(String.valueOf(eventIds.get(iX)), eventNames.get(iX));
        jsEvents.put(jsObj1);
    }
    //AK: Implemented enh 'D-FIN12 calendar status amendment 5th April 11
    JSONArray jsEventName = new JSONArray();
    for (int iX=0; iX<newEventIds.size(); iX++) {

        JSONObject jsObj1 = new JSONObject();
        if (((Integer)eventIds.get(iX)).intValue() == 0) { continue; }
        jsObj1.put("key", "e"+String.valueOf(newEventIds.get(iX)));
        jsObj1.put("value",eventNames.get(iX));
        jsEventName.put(jsObj1);
    }
    jsObj.put("eventNames", jsEventName);
    jsObj.put("events", jsEvents);
    jsObj.put("protocolName", protocolName);
    jsObj.put("calStatus", calStatus);
    jsObj.put("noOfVisits", noOfVisits);
    
    JSONArray jsColArray = new JSONArray();
    {
    	JSONObject jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "event");
    	jsObjTemp1.put("label", LC.L_Event/*"Event"*****/);
    	jsColArray.put(jsObjTemp1);
    	JSONObject jsObjTemp2 = new JSONObject();
    	jsObjTemp2.put("key", "eventId");
    	jsObjTemp2.put("label", LC.L_EventId/*"Event ID"*****/);
    	jsColArray.put(jsObjTemp2);
    	JSONObject jsObjTemp3 = new JSONObject();
    	jsObjTemp3.put("key", "selectAll");
    	jsObjTemp3.put("label", LC.L_Select_AllVisits/*"Select All Visits"*****/);
    	jsColArray.put(jsObjTemp3);
    }
    JSONArray jsDisplacements = new JSONArray();
    for (int iX=0; iX<noOfVisits; iX++) {
        JSONObject jsObj1 = new JSONObject();
        jsObj1.put("key", "v"+String.valueOf(visitIds.get(iX)));
        jsObj1.put("value", visitDisplacements.get(iX));
        jsDisplacements.put(jsObj1);
        JSONObject jsObjTemp = new JSONObject();
        jsObjTemp.put("key", "v"+(visitIds.get(iX)));
        StringBuffer sb1 = new StringBuffer((String)visitNames.get(iX));
        sb1.append(" <input type='checkbox' ");
        sb1.append("name='").append("all_v").append(visitIds.get(iX)).append("'");
        sb1.append("id='").append("all_v").append(visitIds.get(iX)).append("'");
        sb1.append(" onclick='VELOS.calMileStoneGrid.checkAll(\"v").append(visitIds.get(iX)).append("\");'");
        sb1.append(" onmouseover=\"overlib('"+MC.M_ChkCrt_VstMstone+/*"Checking this will create a Visit Level Milestone.*****/"',CAPTION,'"+LC.L_VstLvl_Mstone/*Visit Level Milestone*****/+"');\" ");//AK: Implemented enh 'D-FIN12 calendar status amendment (5th April 11).
        sb1.append(" onmouseout=\"nd();\" ");
        sb1.append("> ");
        jsObjTemp.put("label", sb1.toString());
        jsObjTemp.put("seq", "v"+(iX+1));
        jsColArray.put(jsObjTemp);
    }
    jsObj.put("displacements", jsDisplacements);
    jsObj.put("colArray", jsColArray);
    
    if (eventOfflines != null) {
        JSONArray offlineKeys = new JSONArray();
        JSONArray offlineValues = new JSONArray();
        for (int iX=0; iX<eventOfflines.size(); iX++) {
        	if (((Integer)eventIds.get(iX)).intValue() == 0
        	        || ((Integer)eventVisitIds.get(iX)).intValue() == 0
        	        || eventOfflines.get(iX) == null) { continue; }
            JSONObject jsObjTemp1 = new JSONObject();
        	jsObjTemp1.put("key", "e"+eventIds.get(iX)+"v"+eventVisitIds.get(iX));
        	offlineKeys.put(jsObjTemp1);
            JSONObject jsObjTemp2 = new JSONObject();
        	jsObjTemp2.put("value", eventOfflines.get(iX));
        	offlineValues.put(jsObjTemp2);
        }
        jsObj.put("offlineKeys", offlineKeys);
        jsObj.put("offlineValues", offlineValues);
    }
    
  	ArrayList dataList = new ArrayList();
    for (int iX=0; iX<eventIds.size(); iX++) {
        if (((Integer)eventIds.get(iX)).intValue() < 1) { continue; }
           if (dataList.size() == 0) {
            HashMap map1 = new HashMap();
            map1.put("event", (String)eventNames.get(iX));
            map1.put("eventId", eventIds.get(iX));
            map1.put("selectAll", eventIds.get(iX));
            map1.put("cost", (String)costs.get(iX));
            map1.put("v"+eventVisitIds.get(iX), "e"+newEventIds.get(iX)+"v"+eventVisitIds.get(iX));/*YK :: DFIN12 BUG #5954*/
            dataList.add(map1);
           
        } else {
            HashMap map1 = (HashMap)dataList.get(dataList.size()-1);
            if ((((Integer)map1.get("eventId")).intValue() == ((Integer)eventIds.get(iX)).intValue())) {
                map1.put("v"+eventVisitIds.get(iX),  "e"+newEventIds.get(iX)+"v"+eventVisitIds.get(iX));/*YK :: DFIN12 BUG #5954*/
                dataList.set(dataList.size()-1, map1);
               
            } else {
                HashMap map2 = new HashMap();
                map2.put("event", (String)eventNames.get(iX));
                map2.put("eventId", eventIds.get(iX));
                map2.put("selectAll", eventIds.get(iX));
                map2.put("cost", (String)costs.get(iX));
                map2.put("v"+eventVisitIds.get(iX),"e"+newEventIds.get(iX)+"v"+eventVisitIds.get(iX));/*YK :: DFIN12 BUG #5954*/
                dataList.add(map2);
            }
        }
    }
    
  
    dataList = eventassocB.sortListByKeyword(dataList, "cost");
   
    JSONArray jsEventVisits = new JSONArray();
    for (int iX=0; iX<dataList.size(); iX++) {
        jsEventVisits.put(EJBUtil.hashMapToJSON((HashMap)dataList.get(iX)));
    }
  
    jsObj.put("dataArray", jsEventVisits);
    out.println(jsObj.toString());

    
    // <html><head></head><body></body></html>
%>

<%! // Define Java functions here
private String createCheckbox(String eventId, String visitId, boolean checked) {
	StringBuffer sb = new StringBuffer();
	sb.append("<input type='checkbox' ");
	sb.append(" name='e").append(eventId).append("v").append(visitId).append("' ");
	sb.append(" id='e").append(eventId).append("v").append(visitId).append("' ");
	if (checked) {
	    sb.append(" checked ");
	}
	sb.append(" >");
		return sb.toString();
}
%>
