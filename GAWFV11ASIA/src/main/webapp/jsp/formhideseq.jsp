<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=MC.M_FrmDisp_SeqOpt%><%--Form Display and Sequencing Options*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="whichcss.js"></SCRIPT>

<SCRIPT>

 function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	<%-- if(isNaN(formobj.eSign.value) == true) {

	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/

	formobj.eSign.focus();

	return false;

   } --%>
 }


</SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="linkedformsdao" scope="request" class="com.velos.eres.business.common.LinkedFormsDao"/>
<jsp:useBean id="linkedformsB" scope="request" class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
<jsp:useBean id="commonB" scope="request" class="com.velos.eres.web.common.CommonJB"/>

<%@ page language = "java" import = "com.velos.eres.business.section.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB"%>
<jsp:include page="include.jsp" flush="true"/>
<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1)
   	ienet = 0; //IE
else
	ienet = 1;

if (ienet == 0) {
%>
<body style="overflow:scroll">
<%} else {%>
<body>
<%}%>

<BR>
<DIV class="popDefault" id="div1">

  <%
   HttpSession tSession = request.getSession(true);
   if (sessionmaint.isValidSession(tSession))
   {
   	 String studyNumber = (String) tSession.getValue("studyNo");
	 String calledFrom = request.getParameter("calledFrom");

	 SettingsDao settingsDao=commonB.getSettingsInstance();
	 ArrayList hiddenForms=new ArrayList();
	 ArrayList settingPKs=new ArrayList();

	 if (calledFrom == null) calledFrom="-";
	 String studyId = request.getParameter("studyId");
	 String pageHeading = "";
   	 String hideHeading = "";

	 int lfPk=0,index=-1 ;
	 String formDisplayType = "";
	 String linkedto="";
	 String formname="";
	 String formForPatients = "";
	 String formForStudies = "";
	 String hidecheck = "",hiddenFormId="";
	 String patProfileCheck = "";
     String accountId = (String) tSession.getValue("accountId");
      	 int accId=EJBUtil.stringToNum(accountId);

     String sessionInventoryRight = "";

	sessionInventoryRight  = (String) tSession.getAttribute("sessionInventoryRight");

	if (StringUtil.isEmpty(sessionInventoryRight))
	{
		sessionInventoryRight= "0";
	}

	if (calledFrom.equals("S"))
	{
	  sessionInventoryRight= "0";
	}

	String userId=(String) tSession.getValue("userId");
	int usrId=EJBUtil.stringToNum(userId);

	 int totallen = 0;

	 String subHeading1 = "";
	 String subHeading2 = "";
 	 String subHeading3 = "";
	 String subHeading4 = "";
	 String subHeading5 = "";
	 String dispInSpec = "";
	 String checkDispInSpec = "";

	 if (calledFrom.equals("S"))
	 {
	 settingsDao=commonB.retrieveSettings(EJBUtil.stringToNum(studyId),3,"FORM_HIDE");
	 hiddenForms=settingsDao.getSettingValue();
	 settingPKs=settingsDao.getSettingPK();

	 }

	 //get Active and Lockdown Forms
	 if (calledFrom.equals("A")) {
	     linkedto="'PS','PR','PA','A','SA'";
	 	 linkedformsdao=linkedformsB.getActiveFormsLinkedToAccount(accId,linkedto);
		 pageHeading = MC.M_MngAcc_FrmMgmtDispSeq;/*pageHeading = "Manage Account >> Form Management >> Form Display and Sequencing";*****/
		 subHeading1 = MC.M_FrmLnkTo_AllPat;/*subHeading1 = "Forms Linked to All "+LC.Pat_Patients;*****/
		 subHeading2 = MC.M_FrmLnkPats_OnAllStd;/*subHeading2 = "Forms Linked to "+LC.Pat_Patients+" on All "+LC.Std_Studies;*****/
		 subHeading3 = MC.M_FrmLnkTo_AllStd;/*subHeading3 = "Forms Linked to All "+LC.Std_Studies;*****/
		 subHeading4 = MC.M_Frm_LnkToAcc;/*subHeading4 = "Forms Linked to Account";*****/
		 subHeading5 = MC.M_FrmLkdPat_AllStdRest;/*subHeading5 = "Forms Linked to "+LC.Pat_Patients+" on All "+LC.Std_Studies+" - Restricted";*****/
		 hideHeading = LC.L_Hide;/*hideHeading = "Hide";*****/
	 } else  if (calledFrom.equals("S")) {
	     linkedformsdao=linkedformsB.getStudyLinkedActiveForms(EJBUtil.stringToNum(studyId));
 		 pageHeading = MC.M_MngPcolStd_FrmDispSeq;/*pageHeading = "Manage Protocols >> "+LC.Std_Study+" Setup >> Form Display and Sequencing";*****/
		 subHeading1 = MC.M_FrmLnkPats_ToStd;/*subHeading1 = "Forms Linked to "+LC.Pat_Patients+" of this "+LC.Std_Study;*****/
 		 subHeading2 = MC.M_FrmLnk_ThisStd;/*subHeading2 = "Forms Linked to this "+LC.Std_Study;*****/
		 subHeading3 = MC.M_FrmLnkTo_AllStd;/*subHeading3 = "Forms Linked to All "+LC.Std_Studies;*****/
		 subHeading4 = MC.M_FrmLkdPat_Std;/*subHeading4 = "Forms Linked to "+LC.Pat_Patient+"(All "+LC.Std_Studies+")";*****/
		 subHeading5 = MC.M_FrmLkdPat_StdRest;/*subHeading5 = "Forms Linked to "+LC.Pat_Patient+"(All "+LC.Std_Studies+" - Restricted)";*****/
		 hideHeading = MC.M_Hide_InThisStd;/*hideHeading = "Hide in this "+LC.Std_Study;*****/
	 }

	 ArrayList formnames= linkedformsdao.getFormName();
	 ArrayList formDispTypes = linkedformsdao.getFormLFDisplayType();
	 ArrayList lfPks = linkedformsdao.getLFId() ;
	 ArrayList hides = linkedformsdao.getLfHides() ;
	 ArrayList seqs = linkedformsdao.getLfDispSeqs() ;
   	 ArrayList dispPatProfiles = linkedformsdao.getLfDispPatProfiles();
   	 ArrayList arDispInSpec = linkedformsdao.getLfDispSepecimens();

   	 int len=lfPks.size();
	 int counter = 0;
	 int hideVal = 0;
	 int seq = 0;
   	 int patProfileVal = 0;

	 //Arraylists to store patient forms
	 ArrayList lfPksForPat = new ArrayList();
	 ArrayList namesForPat = new ArrayList();
	 ArrayList hidesForPat = new ArrayList();
	 ArrayList seqsForPat = new ArrayList();
	 ArrayList dispInSpecForPat = new ArrayList();

 	 //Arraylists to store study patient forms
	 ArrayList lfPksForStudy = new ArrayList();
	 ArrayList namesForStudy = new ArrayList();
	 ArrayList hidesForStudy = new ArrayList();
	 ArrayList seqsForStudy = new ArrayList();
     ArrayList patProfileForStudy = new ArrayList();
     ArrayList dispInSpecForStudy = new ArrayList();

 	 //Arraylists to store all study forms
	 ArrayList lfPksForAllStudy = new ArrayList();
	 ArrayList namesForAllStudy = new ArrayList();
	 ArrayList hidesForAllStudy = new ArrayList();
	 ArrayList seqsForAllStudy = new ArrayList();
	 ArrayList dispInSpecForAllStudy = new ArrayList();

	 //Arraylists to store all study forms
	 ArrayList lfPksForPS = new ArrayList();
	 ArrayList namesForPS = new ArrayList();
	 ArrayList hidesForPS = new ArrayList();
	 ArrayList seqsForPS = new ArrayList();
	 ArrayList dispInSpecForPS = new ArrayList();

	 //Arraylists to store all study forms
	 ArrayList lfPksForPRA = new ArrayList();
	 ArrayList namesForPRA = new ArrayList();
	 ArrayList hidesForPRA = new ArrayList();
	 ArrayList seqsForPRA = new ArrayList();
	 ArrayList patProfilePRA=new ArrayList();
	 ArrayList dispInSpecForPRA = new ArrayList();

	 //Arraylists to store all study forms
	 ArrayList lfPksForPR = new ArrayList();
	 ArrayList namesForPR = new ArrayList();
	 ArrayList hidesForPR = new ArrayList();
	 ArrayList seqsForPR = new ArrayList();
	 ArrayList dispInSpecForPR = new ArrayList();

	 //Arraylists to store all account forms
	 ArrayList lfPksForAllAccount = new ArrayList();
	 ArrayList namesForAllAccount = new ArrayList();
	 ArrayList hidesForAllAccount = new ArrayList();
	 ArrayList seqsForAllAccount = new ArrayList();
	 ArrayList dispInSpecForAllAccount= new ArrayList();

	//separate out the forms linked to study and forms linked to patient of this study
	//separate out the forms linked to all studies and all acounts
    for(counter = 0;counter<len;counter++)
	{
		formDisplayType = ((formDispTypes.get(counter)) == null)?"-":(formDispTypes.get(counter)).toString();
		formDisplayType = formDisplayType.trim();

		if (calledFrom.equals("S")) { //from study setup
    		if (formDisplayType.equals("S")) { //this study
    		   lfPksForStudy.add(lfPks.get(counter));
    		   namesForStudy.add(formnames.get(counter));
    		   hidesForStudy.add(hides.get(counter));
    		   seqsForStudy.add(seqs.get(counter));

    		   dispInSpecForStudy.add(arDispInSpec.get(counter));

    		} else if (formDisplayType.equals("SP")) {//patients in this study
    		   lfPksForPat.add(lfPks.get(counter));
    		   namesForPat.add(formnames.get(counter));
    		   hidesForPat.add(hides.get(counter));
    		   seqsForPat.add(seqs.get(counter));
    		   dispInSpecForPat.add(arDispInSpec.get(counter));

    		}
		} //calledfrom S
		else if (calledFrom.equals("A")) //from account form management
		{
			if (formDisplayType.equals("PS")) { //patients on all studies
    		   lfPksForStudy.add(lfPks.get(counter));
    		   namesForStudy.add(formnames.get(counter));
    		   hidesForStudy.add(hides.get(counter));
    		   seqsForStudy.add(seqs.get(counter));
		       patProfileForStudy.add(dispPatProfiles.get(counter));
		       dispInSpecForStudy.add(arDispInSpec.get(counter));

    		} if (formDisplayType.equals("PR")) { //patients on all studies
    		   lfPksForPRA.add(lfPks.get(counter));
    		   namesForPRA.add(formnames.get(counter));
    		   hidesForPRA.add(hides.get(counter));
    		   seqsForPRA.add(seqs.get(counter));
		       patProfilePRA.add(dispPatProfiles.get(counter));

		       dispInSpecForPRA.add(arDispInSpec.get(counter));

    		}
		else if (formDisplayType.equals("PA")) { //all patients
    		   lfPksForPat.add(lfPks.get(counter));
    		   namesForPat.add(formnames.get(counter));
    		   hidesForPat.add(hides.get(counter));
    		   seqsForPat.add(seqs.get(counter));
    		   dispInSpecForPat.add(arDispInSpec.get(counter));

			} else if (formDisplayType.equals("A")) { //all accounts
    		   lfPksForAllAccount.add(lfPks.get(counter));
    		   namesForAllAccount.add(formnames.get(counter));
    		   hidesForAllAccount.add(hides.get(counter));
    		   seqsForAllAccount.add(seqs.get(counter));
    		   dispInSpecForAllAccount.add(arDispInSpec.get(counter));

    		} else if (formDisplayType.equals("SA")) { //all studies
    		   lfPksForAllStudy.add(lfPks.get(counter));
    		   namesForAllStudy.add(formnames.get(counter));
    		   hidesForAllStudy.add(hides.get(counter));
    		   seqsForAllStudy.add(seqs.get(counter));
    		   dispInSpecForAllStudy.add(arDispInSpec.get(counter));
			}
		}
	}//for

	//populate ArrayLists to store all studies and All(Patient studies form)
	 //which are displayed in when calledFrom.equals("S")
	 if (calledFrom.equals("S"))
	 {
	 linkedformsdao=linkedformsB.getStudyLinkedActiveForms(EJBUtil.stringToNum(studyId),accId,usrId);
	 formnames= linkedformsdao.getFormName();
	 formDispTypes = linkedformsdao.getFormLFDisplayType();
	 lfPks = linkedformsdao.getLFId() ;
	 hides = linkedformsdao.getLfHides() ;
	 seqs = linkedformsdao.getLfDispSeqs() ;
   	 dispPatProfiles = linkedformsdao.getLfDispPatProfiles();
   	 arDispInSpec  = linkedformsdao.getLfDispSepecimens();

	 len=lfPks.size();

	 for(counter = 0;counter<len;counter++)
	{
		formDisplayType = ((formDispTypes.get(counter)) == null)?"-":(formDispTypes.get(counter)).toString();
		formDisplayType = formDisplayType.trim();
		if (formDisplayType.equals("PS")) { //patients on all studies
    		   lfPksForPS.add(lfPks.get(counter));
    		   namesForPS.add(formnames.get(counter));
    		   hidesForPS.add(hides.get(counter));
    		   seqsForPS.add(seqs.get(counter));
    		   dispInSpecForPS.add(arDispInSpec.get(counter));
	  	}
		if (formDisplayType.equals("PR")) { //patients on all studies
    		   lfPksForPR.add(lfPks.get(counter));
    		   namesForPR.add(formnames.get(counter));
    		   hidesForPR.add(hides.get(counter));
    		   seqsForPR.add(seqs.get(counter));

    		   dispInSpecForPR.add(arDispInSpec.get(counter));
	  	}

		if (formDisplayType.equals("SA")) { //all studies
    		   lfPksForAllStudy.add(lfPks.get(counter));
    		   namesForAllStudy.add(formnames.get(counter));
    		   hidesForAllStudy.add(hides.get(counter));
    		   seqsForAllStudy.add(seqs.get(counter));

    		   dispInSpecForAllStudy.add(arDispInSpec.get(counter));

		}

	}
	        lfPksForPS.add(new Integer("-1"));
	    namesForPS.add(LC.L_Lab);/*namesForPS.add("Lab");*****/
		hidesForPS.add(new Integer(0));
		seqsForPS.add(new Integer(0));
		dispInSpecForPS.add("0");
	 }

%>

<p class="sectionHeadings"><%=pageHeading%></p>

<% if (!(studyId==null)) {%>
    <P><%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%>: <%=studyNumber%></P>
<%}%>

	<Form  name="formhideseq" id="frmhideseq" method="post" action="updateformhideseq.jsp"  onsubmit = "if (validate(document.formhideseq) == false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<input type="hidden" name="calledfrom" value='<%=calledFrom%>'>
	<input type="hidden" name="studyid" value='<%=studyId%>'>
	<% if (calledFrom.equals("S"))
                {%>
	<table width="99%" border="0" cellspacing="0" cellpadding="0" class="basetbl" >
       <tr>
         <th width="20%"><%=LC.L_ActiveOrLkdownFrm%><%--Active / Lockdown Forms*****--%></th>
         <th width="10%"><%=hideHeading%></th>
         <th width="25%"><%=LC.L_Display_Seq%><%--Display Sequence*****--%></th>
         <%
         if (sessionInventoryRight.equals("1")) {%>
         <th width="35%"><%=MC.M_Disp_InSpmenMgmt%><%--Display in Specimen Management*****--%></th>
         <% } %>
      </tr>
<%} else if (calledFrom.equals("A")) {%>
	<table width="99%"  border="0" cellspacing="0" cellpadding="0" class="basetbl">
       <tr>
         <th width="20%"><%=LC.L_ActiveOrLkdownFrm%><%--Active / Lockdown Forms*****--%></th>
         <th width="10%"><%=hideHeading%></th>
         <th width="20%"><%=LC.L_Display_Seq%><%--Display Sequence*****--%></th>
         <th width="25%"><%=MC.M_DispIn_PatProfile%><%--Display in <%=LC.Pat_Patient%> Profile*****--%></th>
         <%
         if (sessionInventoryRight.equals("1")) {%>
         <th width="35%"><%=MC.M_Disp_InSpmenMgmt%><%--Display in Specimen Management*****--%></th>
         <% } %>


      </tr>
     <%}%>
	  <tr>
	  	 <td colspan="5"><P class="sectionHeadingsFrm"><%=subHeading1%></P></td>
	  </tr>
<%
	//display forms linked to patients
   	len=lfPksForPat.size();
	totallen = len;

    for(counter = 0;counter<len;counter++)
	{
		formname= ((namesForPat.get(counter)) == null)?"-":(namesForPat.get(counter)).toString();
		lfPk = ((Integer )lfPksForPat.get(counter)).intValue();
		hideVal = ((Integer )hidesForPat.get(counter)).intValue();

		dispInSpec = (String) dispInSpecForPat.get(counter);
		if (StringUtil.isEmpty(dispInSpec))
		{
			dispInSpec = "0";
		}
		if (dispInSpec.equals("1"))
		{
			checkDispInSpec = " CHECKED ";
		}
		else
		{
			checkDispInSpec = "";
		}

		if (hideVal== 1) {
		   hidecheck="Checked";
		} else {
		   hidecheck = "";
		}
		seq = ((Integer )seqsForPat.get(counter)).intValue();

		if ((counter%2)==0) {   %>

      <tr class="browserEvenRow">
        <%

		} else {

  %>
      <tr class="browserOddRow">
  <%}%>

        <td><%=formname%><input type="hidden" name="lfPk" value="<%=lfPk%>"></td>
		<td align="center"><input type="checkbox" <%=hidecheck%> name="hideCheckedVal" value="<%=lfPk%>"></td>
		<td align="center"><input type="text" name="dispSeq" size="5" value="<%=seq%>"></td>
		<% if (calledFrom.equals("A")) { %>
			<td>&nbsp;</td>
		<% } %>

		<%
         if (sessionInventoryRight.equals("1")) {%>

			<td align="center"><input type="checkbox" <%=checkDispInSpec %> name="chkDispInSpec" value="<%=lfPk%>"></td>
		<% }%>

      </tr>
      <%
	}//for
%>

  	  <tr height="15">
	  	 <td></td>
	  </tr>

	  <tr>
	  	 <td  colspan="5"><P class="sectionHeadingsFrm"><%=subHeading2%></P></td>
	  </tr>
<%
	//display forms linked to this Study
   	len=lfPksForStudy.size();

	totallen = totallen + len;

    for(counter = 0;counter<len;counter++)
	{
		formname= ((namesForStudy.get(counter)) == null)?"-":(namesForStudy.get(counter)).toString();
		lfPk = ((Integer )lfPksForStudy.get(counter)).intValue();
		hideVal = ((Integer )hidesForStudy.get(counter)).intValue();

		dispInSpec = (String) dispInSpecForStudy.get(counter);

		if (StringUtil.isEmpty(dispInSpec))
		{
			dispInSpec = "0";
		}
		if (dispInSpec.equals("1"))
		{
			checkDispInSpec = " CHECKED ";
		}
		else
		{
			checkDispInSpec = "";
		}


		if (hideVal== 1) {
		   hidecheck="Checked";
		} else {
		   hidecheck = "";
		}

		if (calledFrom.equals("A")){
            patProfileVal = ((Integer )patProfileForStudy.get(counter)).intValue();
            if (patProfileVal== 1) {
                patProfileCheck="Checked";
            } else {
                patProfileCheck = "";
            }
       }

		seq = ((Integer )seqsForStudy.get(counter)).intValue();
		if ((counter%2)==0) {   %>

      <tr class="browserEvenRow">
        <%

		} else {

  %>
      <tr class="browserOddRow">
  <%}%>
        <td><%=formname%><input type="hidden" name="lfPk" value="<%=lfPk%>"></td>
		<td align="center"><input type="checkbox" <%=hidecheck%> name="hideCheckedVal" value="<%=lfPk%>"></td>
		<td align="center"><input type="text" name="dispSeq" size="5" value="<%=seq%>"></td>
 		<%  if (calledFrom.equals("A")){	%>
		<td align="center"><input type="checkbox" <%=patProfileCheck%> name="patProfileCheckedVal" value="<%=lfPk%>"></td>
		<%}
		%>

		<%
         if (sessionInventoryRight.equals("1")) {%>
			<td align="center"><input type="checkbox" <%=checkDispInSpec %> name="chkDispInSpec" value="<%=lfPk%>"></td>
		<% }%>

      </tr>
      <%
	}//for
	//display forms linked to this Study
   	len=lfPksForPRA.size();
	if (len>0){
	%>
	<tr height="15">
	  	 <td></td>
	  </tr>

	  <tr>
	  	 <td  colspan="5"><P class="sectionHeadingsFrm"><%=subHeading5%></P></td>
	  </tr>
<%

	totallen = totallen + len;

    for(counter = 0;counter<len;counter++)
	{
		formname= ((namesForPRA.get(counter)) == null)?"-":(namesForPRA.get(counter)).toString();
		lfPk = ((Integer )lfPksForPRA.get(counter)).intValue();
		hideVal = ((Integer )hidesForPRA.get(counter)).intValue();

		dispInSpec = (String) dispInSpecForPRA.get(counter);

		if (StringUtil.isEmpty(dispInSpec))
		{
			dispInSpec = "0";
		}
		if (dispInSpec.equals("1"))
		{
			checkDispInSpec = " CHECKED ";
		}
		else
		{
			checkDispInSpec = "";
		}


		if (hideVal== 1) {
		   hidecheck="Checked";
		} else {
		   hidecheck = "";
		}

		if (calledFrom.equals("A")){
            patProfileVal = ((Integer )patProfilePRA.get(counter)).intValue();
            if (patProfileVal== 1) {
                patProfileCheck="Checked";
            } else {
                patProfileCheck = "";
            }
       }

		seq = ((Integer )seqsForPRA.get(counter)).intValue();
		if ((counter%2)==0) {   %>

      <tr class="browserEvenRow">
        <%

		} else {

  %>
      <tr class="browserOddRow">
  <%}%>
        <td><%=formname%><input type="hidden" name="lfPk" value="<%=lfPk%>"></td>
		<td align="center"><input type="checkbox" <%=hidecheck%> name="hideCheckedVal" value="<%=lfPk%>"></td>
		<td align="center"><input type="text" name="dispSeq" size="5" value="<%=seq%>"></td>
 		<%  if (calledFrom.equals("A")){	%>
		<td align="center"><input type="checkbox" <%=patProfileCheck%> name="patProfileCheckedVal" value="<%=lfPk%>"></td>
		<%}
		else
		{
			%>
				<td>&nbsp;</td>
			<%
		}
		%>

		<%
         if (sessionInventoryRight.equals("1")) {%>
			<td align="center"><input type="checkbox" <%=checkDispInSpec %> name="chkDispInSpec" value="<%=lfPk%>"></td>
		<% } %>


      </tr>
      <%
	}//end for loop
	}//end for len>0

%>
<% if (calledFrom.equals("A"))
{ //show all account and all studies forms also
%>
  	  <tr height="15">
	  	 <td></td>
	  </tr>

	  <tr>
	  	 <td  colspan="5"><P class="sectionHeadingsFrm"><%=subHeading3%></P></td>
	  </tr>
<%
	//display forms linked to all Studies
   	len=lfPksForAllStudy.size();

	totallen = totallen + len;

    for(counter = 0;counter<len;counter++)
	{
		formname= ((namesForAllStudy.get(counter)) == null)?"-":(namesForAllStudy.get(counter)).toString();
		lfPk = ((Integer )lfPksForAllStudy.get(counter)).intValue();
		hideVal = ((Integer )hidesForAllStudy.get(counter)).intValue();


		dispInSpec = (String) dispInSpecForAllStudy.get(counter);

		if (StringUtil.isEmpty(dispInSpec))
		{
			dispInSpec = "0";
		}
		if (dispInSpec.equals("1"))
		{
			checkDispInSpec = " CHECKED ";
		}
		else
		{
			checkDispInSpec = "";
		}


		if (hideVal== 1) {
		   hidecheck="Checked";
		} else {
		   hidecheck = "";
		}

		seq = ((Integer )seqsForAllStudy.get(counter)).intValue();
		if ((counter%2)==0) {   %>

      <tr class="browserEvenRow">
        <%

		} else {

  %>
      <tr class="browserOddRow">
  <%}%>
        <td><%=formname%><input type="hidden" name="lfPk" value="<%=lfPk%>"></td>
		<td align="center"><input type="checkbox" <%=hidecheck%> name="hideCheckedVal" value="<%=lfPk%>"></td>
		<td align="center"><input type="text" name="dispSeq" size="5" value="<%=seq%>"></td>

		<td>&nbsp;</td>
		<%
         if (sessionInventoryRight.equals("1")) {%>
		<td align="center"><input type="checkbox" <%=checkDispInSpec %> name="chkDispInSpec" value="<%=lfPk%>"></td>
		<% } %>

      </tr>
      <%
	}//for
%>

  	  <tr height="15">
	  	 <td></td>
	  </tr>
<%%>

	  <tr>
	  	 <td  colspan="5"><P class="sectionHeadingsFrm"><%=subHeading4%></P></td>
	  </tr>
<%
	//display forms linked to all Accounts
   	len=lfPksForAllAccount.size();

	totallen = totallen + len;

    for(counter = 0;counter<len;counter++)
	{
		formname= ((namesForAllAccount.get(counter)) == null)?"-":(namesForAllAccount.get(counter)).toString();
		lfPk = ((Integer )lfPksForAllAccount.get(counter)).intValue();
		hideVal = ((Integer )hidesForAllAccount.get(counter)).intValue();

		dispInSpec = (String) dispInSpecForAllAccount.get(counter);
		if (StringUtil.isEmpty(dispInSpec))
		{
			dispInSpec = "0";
		}
		if (dispInSpec.equals("1"))
		{
			checkDispInSpec = " CHECKED ";
		}
		else
		{
			checkDispInSpec = "";
		}


		if (hideVal== 1) {
		   hidecheck="Checked";
		} else {
		   hidecheck = "";
		}

		seq = ((Integer )seqsForAllAccount.get(counter)).intValue();
		if ((counter%2)==0) {   %>

      <tr class="browserEvenRow">
        <%

		} else {

  %>
      <tr class="browserOddRow">
  <%}%>
        <td><%=formname%><input type="hidden" name="lfPk" value="<%=lfPk%>"></td>
		<td align="center"><input type="checkbox" <%=hidecheck%> name="hideCheckedVal" value="<%=lfPk%>"></td>
		<td align="center"><input type="text" name="dispSeq" size="5" value="<%=seq%>"></td>

		<td>&nbsp;</td>
		<%
         if (sessionInventoryRight.equals("1")) {%>
		<td align="center"><input type="checkbox" <%=checkDispInSpec %> name="chkDispInSpec" value="<%=lfPk%>"></td>
		<% }%>


      </tr>
      <%
	}//for

}%>

<% if (calledFrom.equals("S"))
{ //show all patient studies and all studies forms also

	len=lfPksForAllStudy.size();
	if (len>0) {
%>
  	  <tr height="15">
	  	 <td></td>
	  </tr>

	  <tr>
	  	 <td><P class="sectionheadings"><%=subHeading3%></P></td>
	  </tr>
<%
	//display forms linked to all Studies
   	//len=lfPksForAllStudy.size();

	totallen = totallen + len;

    for(counter = 0;counter<len;counter++)
	{
		formname= ((namesForAllStudy.get(counter)) == null)?"-":(namesForAllStudy.get(counter)).toString();
		lfPk = ((Integer )lfPksForAllStudy.get(counter)).intValue();
		index=hiddenForms.indexOf(((Integer )lfPksForAllStudy.get(counter)).toString());


		dispInSpec = (String) dispInSpecForAllStudy.get(counter);
		if (StringUtil.isEmpty(dispInSpec))
		{
			dispInSpec = "0";
		}
		if (dispInSpec.equals("1"))
		{
			checkDispInSpec = " CHECKED ";
		}
		else
		{
			checkDispInSpec = "";
		}

		if (index>=0)
		{
		  hidecheck="Checked";
		  hiddenFormId=(String)hiddenForms.get(index);
		}
		else
		{
		  hidecheck = "";
		  hiddenFormId="";
		  }

		//hideVal = ((Integer )hidesForAllStudy.get(counter)).intValue();
		//seq = ((Integer )seqsForAllStudy.get(counter)).intValue();
		if ((counter%2)==0) {   %>

      <tr class="browserEvenRow">
        <%

		} else {

  %>
      <tr class="browserOddRow">
  <%}%>
        	<td><%=formname%><input type="hidden" name="lfPkS" value="<%=lfPk%>"></td>
		<td align="center"><input type="checkbox" <%=hidecheck%> name="hideCheckedValS" value="<%=lfPk%>"></td>
		<%if (index>=0){%> <input type="hidden" name="hiddenformid" value='<%=hiddenFormId%>'><%}%>
		<!--<td align="center"><input type="text" name="dispSeqAS" size="5" value="<%=seq%>"></td>-->

		<td>&nbsp;</td>
		<%
         if (sessionInventoryRight.equals("1")) {%>
			<td align="center"><input type="checkbox" <%=checkDispInSpec %> name="chkDispInSpec" value="<%=lfPk%>"></td>
		<% } %>

      </tr>
      <%
	}//for end
	}//end of if len>0
	//display forms linked to all Accounts
   	len=lfPksForPS.size();
	if (len>0){
%>

  	  <tr height="15">
	  	 <td></td>
	  </tr>
<%%>

	  <tr>
	  	 <td><P class="sectionheadings"><%=subHeading4%></P></td>
	  </tr>
<%


	totallen = totallen + len;

    for(counter = 0;counter<len;counter++)
	{
		formname= ((namesForPS.get(counter)) == null)?"-":(namesForPS.get(counter)).toString();
		lfPk = ((Integer )lfPksForPS.get(counter)).intValue();
		index=hiddenForms.indexOf(((Integer )lfPksForPS.get(counter)).toString());


		dispInSpec = (String) dispInSpecForPS.get(counter);

		if (StringUtil.isEmpty(dispInSpec))
		{
			dispInSpec = "0";
		}
		if (dispInSpec.equals("1"))
		{
			checkDispInSpec = " CHECKED ";
		}
		else
		{
			checkDispInSpec = "";
		}

		if (index>=0)
		{
		  hidecheck="Checked";
		  hiddenFormId=(String)hiddenForms.get(index);
		}
		else
		{
		  hidecheck = "";
		  hiddenFormId="";
		  }
		//seq = ((Integer )seqsForPS.get(counter)).intValue();
		if ((counter%2)==0) {   %>

      <tr class="browserEvenRow">
        <%

		} else {

  %>
      <tr class="browserOddRow">
  <%}%>
        	<td><%=formname%><input type="hidden" name="lfPkS" value="<%=lfPk%>"></td>
	    	<td align="center"><input type="checkbox" <%=hidecheck%> name="hideCheckedValS" value="<%=lfPk%>"></td>
		<!--<td align="center"><input type="text" name="dispSeqPS" size="5" value="<%=seq%>"></td>-->
           <%if (index>=0){%> <input type="hidden" name="hiddenformid" value='<%=hiddenFormId%>'><%}%>

           <td>&nbsp;</td>
           <%
         if (sessionInventoryRight.equals("1")) {%>
			<td align="center"><input type="checkbox" <%=checkDispInSpec %> name="chkDispInSpec" value="<%=lfPk%>"></td>
			<% }%>



      </tr>
      <%
	}//for
	} //end for len>0
	//display forms linked to all Accounts
   	len=lfPksForPR.size();
	if (len>0){
	%>
	 <tr height="15">
	  	 <td></td>
	  </tr>
	  <tr>
	  	 <td><P class="sectionheadings"><%=subHeading5%></P></td>
	  </tr>
<%


	totallen = totallen + len;

    for(counter = 0;counter<len;counter++)
	{
		formname= ((namesForPR.get(counter)) == null)?"-":(namesForPR.get(counter)).toString();
		lfPk = ((Integer )lfPksForPR.get(counter)).intValue();
		index=hiddenForms.indexOf(((Integer )lfPksForPR.get(counter)).toString());


		dispInSpec = (String) dispInSpecForPR.get(counter);
		if (StringUtil.isEmpty(dispInSpec))
		{
			dispInSpec = "0";
		}
		if (dispInSpec.equals("1"))
		{
			checkDispInSpec = " CHECKED ";
		}
		else
		{
			checkDispInSpec = "";
		}


		if (index>=0)
		{
		  hidecheck="Checked";
		  hiddenFormId=(String)hiddenForms.get(index);
		}
		else
		{
		  hidecheck = "";
		  hiddenFormId="";
		  }
		//seq = ((Integer )seqsForPS.get(counter)).intValue();
		if ((counter%2)==0) {   %>

      <tr class="browserEvenRow">
        <%

		} else {

  %>
      <tr class="browserOddRow">
  <%}%>
        	<td><%=formname%><input type="hidden" name="lfPkS" value="<%=lfPk%>"></td>
	    	<td align="center"><input type="checkbox" <%=hidecheck%> name="hideCheckedValS" value="<%=lfPk%>"></td>
		<!--<td align="center"><input type="text" name="dispSeqPS" size="5" value="<%=seq%>"></td>-->
           <%if (index>=0){%> <input type="hidden" name="hiddenformid" value='<%=hiddenFormId%>'><%}%>

           <td>&nbsp;</td>
           <%
         if (sessionInventoryRight.equals("1")) {%>
		<td align="center"><input type="checkbox" <%=checkDispInSpec %> name="chkDispInSpec" value="<%=lfPk%>"></td>
		<% } %>

      </tr>
      <%
	}//for
	}//end for len>0


}%>

    </table>

<br>
<table width="99%" cellspacing="0" cellpadding="0" border="0" class="midAlign">
	<tr>
		<td width="60%" align=left>
		<% if (totallen > 0) {%>
			<jsp:include page="submitBar.jsp" flush="true">
				<jsp:param name="displayESign" value="Y"/>
				<jsp:param name="formID" value="frmhideseq"/>
				<jsp:param name="showDiscard" value="N"/>
				<jsp:param name="noBR" value="Y"/>
			</jsp:include>
		<%}%>
		</td>
		<td width="5%"></td>
		<td width="15%" align=left>
		   	<button onclick="self.close();"><%=LC.L_Close%></button>
		</td>
	</tr>
</table>


  </Form>

<%	} //end of if session times out

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

} //end of else body for session time out

%>
</div>

<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</body>

</html>
