<%@page import="com.velos.eres.web.study.StudyJB"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList,java.util.HashMap,org.json.*"%>
<%@page import="com.velos.esch.business.common.EventdefDao"%>
<%@page import="com.velos.esch.business.common.EventAssocDao"%>
<%@page import="com.velos.esch.business.common.SchCodeDao"%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="teamB" scope="session" class="com.velos.eres.web.team.TeamJB"/>
<jsp:useBean id="studySiteB" scope="session" class="com.velos.eres.web.studySite.StudySiteJB"/>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.studyRights.StudyRightsJB" %>
<%@ page language="java" import="java.util.*,com.velos.eres.service.util.*"%>
<%@ page language="java" import="com.velos.eres.business.team.*,com.velos.eres.business.common.*"%>


<%
    request.setCharacterEncoding("UTF-8");
    response.setContentType("application/json");
    // response.setContentType("text/html");

	HttpSession tSession = request.getSession(true);
    JSONObject jsObj = new JSONObject();
    if (!sessionmaint.isValidSession(tSession)) {
        // Session is invalid; print an error and exit
        response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");
        jsObj.put("error", new Integer(-1));
        jsObj.put("errorMsg", MC.M_UsrNot_LoggedIn/*User is not logged in.*****/);
	    out.println(jsObj.toString());
        return;
    }
    
	String studyId = request.getParameter("studyId");
	if(studyId == "" || studyId == null || studyId.equals("null") || studyId.equals("")) {
	    // A valid Study ID is required; print an error and exit
        jsObj.put("error", new Integer(-2));
        jsObj.put("errorMsg", "Study ID is invalid.");
	    out.println(jsObj.toString());
	    return;
	}
	
	int pageRight = 7;//0;
	/*if (StringUtil.stringToNum(studyId) == 0){
		grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));
	} else {
		//******************GET STUDY TEAM RIGHTS and put the same in session, this will be a separate obj as no study is selected //***************************************************************
	    TeamDao teamDao = new TeamDao();
	    teamDao.getTeamRights(StringUtil.stringToNum(studyId),StringUtil.stringToNum(userId));
	    ArrayList tId = teamDao.getTeamIds();
	    if (tId.size() == 0)
		{
	    	pageRight=0 ;
	    }
		else
		{
	    	stdRights.setId(StringUtil.stringToNum(tId.get(0).toString()));
	   	 	ArrayList teamRights ;
			teamRights  = new ArrayList();
			teamRights = teamDao.getTeamRights();

			stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
			stdRights.loadStudyRights();

	    	tSession.setAttribute("studyRights",stdRights);
	    	if ((stdRights.getFtrRights().size()) == 0)
			{
	    	 	pageRight= 0;
	    	}
			else
			{
	    		pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYTEAM"));
	    	}
	    }
	}*/
	
	if (pageRight > 0 ){
		int accId = StringUtil.stringToNum((String) tSession.getAttribute("accountId"));
		String groupId = (String) tSession.getAttribute("defUserGroup");
		int iStudyId = StringUtil.stringToNum(request.getParameter("studyId"));
		
		StudyJB studyB = new StudyJB();
		studyB.setId(iStudyId);
		studyB.getStudyDetails();
		int studyDm = StringUtil.stringToNum((String)studyB.getStudyAuthor());
		jsObj.put("studyDm", studyDm);

		int selOrgId = StringUtil.stringToNum(request.getParameter("selSiteId"));
		if (selOrgId <= 0) return;

		int gridIndex = StringUtil.stringToNum(request.getParameter("gridIndex"));
		
		StudySiteDao ssDao = new StudySiteDao();
		//ssDao	= studySiteB.getStudyTeamSites(iStudyId, accId);
		ssDao.getStudyTeamSites(iStudyId, accId);
		ArrayList arrSiteIds = ssDao.getSiteIds();
		ArrayList arrSiteNames = ssDao.getSiteNames();
		ArrayList arrSiteTypes = ssDao.getStudySiteTypes();

		int indx = -1;
		indx = arrSiteIds.indexOf(selOrgId);
		if (indx < 0) return;
		String selOrg = (indx < 0)? "" : (String) arrSiteNames.get(indx);
		String selOrgType = (indx < 0)? "" : (String) arrSiteTypes.get(indx);
		selOrgType = (StringUtil.isEmpty(selOrgType)? "" : selOrgType);

		TeamDao teamDao = new TeamDao();
		StudySiteDao studySiteDao = new StudySiteDao();
		StudyStatusDao studyStatDao = new StudyStatusDao();
		studySiteDao = studySiteB.getStudySiteTeamValues(iStudyId,selOrgId,StringUtil.stringToNum((String)tSession.getAttribute("userId")), accId);

		ArrayList studySiteIds = studySiteDao.getStudySiteIds();
		ArrayList studySiteApndxCnts = studySiteDao.getStudySiteApndxCnts();
		ArrayList siteIds1 = studySiteDao.getSiteIds();
		ArrayList siteTypes = studySiteDao.getStudySiteTypes();
		ArrayList localSizes = studySiteDao.getLSampleSize();
		ArrayList siteNames = studySiteDao.getSiteNames();
		ArrayList siteAccess = studySiteDao.getSiteAccess();
		ArrayList currStats = studySiteDao.getCurrStats();//KM
		int len1 = studySiteIds.size();
		//teamDao = teamB.getTeamValues(studyId, accId);
		//to be implemented
		teamDao = teamB.getTeamValuesBySite(iStudyId, accId, selOrgId);
		ArrayList teamIds 		= teamDao.getTeamIds();
		ArrayList teamRoleIds 		= teamDao.getTeamRoleIds();
		ArrayList teamRoles 		= teamDao.getTeamRoles();
		ArrayList teamUserLastNames 	= teamDao.getTeamUserLastNames();
		ArrayList teamUserFirstNames = teamDao.getTeamUserFirstNames();
		ArrayList userSiteNames = teamDao.getUsrSiteNames();
		ArrayList teamRights 	= teamDao.getTeamRights();
		ArrayList inexUsers	 	= teamDao.getInexUsers();
		ArrayList userIds = teamDao.getUserIds();
		ArrayList siteIds2 = teamDao.getSiteIds();
		//Added by IA 11.03.2006
		ArrayList userPhones = teamDao.getUserPhone();
		ArrayList userEmails = teamDao.getUserEmail();
		ArrayList userTypes = teamDao.getUserType();
		//End Added
		//km
		ArrayList teamStatus =teamDao.getTeamStatus();
		ArrayList teamStatusIds=teamDao.getTeamStatusIds();
		ArrayList teamUserTypes = teamDao.getTeamUserTypes();

		CodeDao cdRole = new CodeDao();
		cdRole.getCodeValuesWithoutHide("role");
			
		ArrayList roleList = cdRole.getCDesc();
		ArrayList roleIdList = cdRole.getCId();
		
		jsObj.put("roleList", roleList);
		jsObj.put("roleIdList", roleIdList);
		
		jsObj.put("selOrgId", selOrgId);
		
		UserDao userDao = new UserDao();
		userDao.getAvailableTeamUsers(StringUtil.stringToNum(studyId),accId, "", "", ""+selOrgId, "", "");

		ArrayList usrIds = userDao.getUsrIds();
		ArrayList usrFirstNames = userDao.getUsrFirstNames(); 
		ArrayList usrLastNames = userDao.getUsrLastNames();
	
		ArrayList usrFullNames = new ArrayList();
		
		for (indx = 0; indx < usrFirstNames.size(); indx++){
			String fName = (String)usrFirstNames.get(indx);
			fName = StringUtil.isEmpty(fName)? "-" : fName;
			String lName = (String)usrLastNames.get(indx);
			lName = StringUtil.isEmpty(lName)? "-" : lName;
			usrFullNames.add(fName + " " + lName);
		} 
		jsObj.put("userIds", usrIds);
		jsObj.put("userNames", usrFullNames);

	    JSONArray jsColArray = new JSONArray();
	    {
	    	JSONObject jsObjTemp1 = new JSONObject();
	    	jsObjTemp1.put("key", "teamId");
	    	jsObjTemp1.put("label", "teamId");
	    	jsObjTemp1.put("hidden", "true");    	
	    	jsColArray.put(jsObjTemp1);

	    	jsObjTemp1 = new JSONObject();
	    	jsObjTemp1.put("key", "orgId");
	    	jsObjTemp1.put("label", "orgId");
	    	jsObjTemp1.put("hidden", "true");
	    	jsColArray.put(jsObjTemp1);

	    	JSONArray jsChildrenArray = new JSONArray();
	    	jsObjTemp1 = new JSONObject();
	    	jsObjTemp1.put("key", "userId");
	    	jsObjTemp1.put("label", "");
	    	jsObjTemp1.put("hidden", "true");
	    	jsColArray.put(jsObjTemp1);
	    	
	    	jsObjTemp1 = new JSONObject();
	    	jsObjTemp1.put("key", "user");
	    	jsObjTemp1.put("label", selOrg);
	    	jsColArray.put(jsObjTemp1);
	        
	    	jsObjTemp1 = new JSONObject();
	    	jsObjTemp1.put("key", "roleId");
	    	jsObjTemp1.put("label", "");
	    	jsObjTemp1.put("hidden", "true");
	    	jsColArray.put(jsObjTemp1);

	    	jsObjTemp1 = new JSONObject();
	    	jsObjTemp1.put("key", "role");
	    	jsObjTemp1.put("label", LC.L_Role);
	    	jsColArray.put(jsObjTemp1);

	    	jsObjTemp1 = new JSONObject();
	    	jsObjTemp1.put("key", "phone");
	    	jsObjTemp1.put("label", LC.L_Phone);
	    	jsColArray.put(jsObjTemp1);
	    	
	    	jsObjTemp1 = new JSONObject();
	    	jsObjTemp1.put("key", "email");
	    	jsObjTemp1.put("label", LC.L_E_Mail);
	    	jsColArray.put(jsObjTemp1);

	    	/*jsObjTemp1 = new JSONObject();
			jsObjTemp1.put("key", "siteName");
			jsObjTemp1.put("label", selOrg);
			jsObjTemp1.put("children", jsChildrenArray);
			jsColArray.put(jsObjTemp1);*/
			
	    	jsObjTemp1 = new JSONObject();
	    	jsObjTemp1.put("key", "delete");
	    	jsObjTemp1.put("label", "<img id='addRows"+ gridIndex +"' name='addRows' title='"+ LC.L_Add +"' src='../images/add.png'/>");
	    	jsObjTemp1.put("action", "delete");
	    	jsColArray.put(jsObjTemp1);
	    }
	    jsObj.put("colArray", jsColArray);
	    
	    ArrayList dataList = new ArrayList();
	    for (int iX=0; iX<teamIds.size(); iX++) {
	        if (((Integer)teamIds.get(iX)).intValue() < 1) { continue; }

			String teamRight = null;
			String inexUser = null;
			String siteId1 = null;
			String siteId2 = null;
			String siteAcc = "";
			String studySiteId = null;
			String studySiteApndxCnt= null;
			int istudySiteApndxCnt = 0;
			String siteType= null;
			String localSize = null;
			String currStat = null;//KM
			String subType = null;//Amarnadh
			boolean flag = true;
			int teamId = 0;
			int userId = 0;
			String usr_id = "";
			String orgName = "";
			String userType = "";
			String teamStat="";
			int teamStatusId=0;
			
            HashMap map1 = new HashMap();
            map1.put("teamId", teamIds.get(iX));
            
            map1.put("orgId", selOrgId);
            
            int teamUserId = ((Integer)userIds.get(iX)).intValue();
			map1.put("userId", teamUserId);

            String name = (String) teamUserFirstNames.get(iX);
            if (!StringUtil.isEmpty(name)){
            	name += " " + (String) teamUserLastNames.get(iX);
            }
            map1.put("user", name);

            map1.put("roleId", teamRoleIds.get(iX));

            String role = (String) teamRoles.get(iX);
            map1.put("role", role);

	        String phone = (String) userPhones.get(iX);
            map1.put("phone", phone);

            String email = (String) userEmails.get(iX);
            map1.put("email", email);

            dataList.add(map1);
	    }
	
	    
	    JSONArray jsTeamUsers = new JSONArray();
	    for (int iX=0; iX<dataList.size(); iX++) {
	    	jsTeamUsers.put(EJBUtil.hashMapToJSON((HashMap)dataList.get(iX)));
	    }
	
	    jsObj.put("dataArray", jsTeamUsers);
	}

	out.println(jsObj.toString());
%>

