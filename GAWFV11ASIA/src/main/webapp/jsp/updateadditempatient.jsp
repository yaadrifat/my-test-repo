<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.math.BigDecimal"%>

<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
	<jsp:useBean id="budgetB" scope="session" class="com.velos.esch.web.budget.BudgetJB"/>
	<jsp:useBean id="lineitemB" scope="session" class="com.velos.esch.web.lineitem.LineitemJB"/>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<jsp:useBean id="bgtSectionB" scope="session" class="com.velos.esch.web.bgtSection.BgtSectionJB"/>

  <%@ page language = "java" import = "com.velos.eres.business.common.*,com.velos.esch.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.StringUtil"%>
  <%
  
	String bgtcalId = request.getParameter("bgtcalId");	
	String eSign = request.getParameter("eSign");
//	String budgetType = request.getParameter("budgetType");
 
	String lineitemMode = request.getParameter("lineitemMode");	
	String lineitemInPerSec = "";
	lineitemInPerSec = request.getParameter("lineitemInPerSec");

	String lineCount= request.getParameter("count");	
	String templateType = request.getParameter("templateType");	

	if (StringUtil.isEmpty(templateType))
	{
		templateType="";
	}

	HttpSession tSession = request.getSession(true); 

	if(sessionmaint.isValidSession(tSession))

   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%   
  	 	String oldESign = (String) tSession.getValue("eSign");
		if(!oldESign.equals(eSign)) {
    		%>
       	 	 <jsp:include page="incorrectesign.jsp" flush="true"/>  	
       		<%
       		} else {
		String ipAdd = (String) tSession.getValue("ipAdd");
		String usr = (String) tSession.getValue("userId");      
		int i = 0;
		String budgetName=request.getParameter("budgetName");

		String bgtSectionId = request.getParameter("bgtSectionId");
		String category[] = request.getParameterValues("cmbCtgry");
		//Rohit CCF-FIN21
		String tmid[] = null;
		String cdm[] = null;
		String sectionLineItemPK[] = request.getParameterValues("sectionLineItemPK");
		String bSection = request.getParameter("bSection");
			
			
		String unitCost[] = request.getParameterValues("unitCost");
		String numUnit[] = request.getParameterValues("noUnits");
		String costType[] = request.getParameterValues("dCodeType");
		
		String applyIndirect[] = request.getParameterValues("hidAppIndirects");
		String applyDiscount[] = request.getParameterValues("hidDiscountChkbox");
		
		
		
		String sponsorAmount[]= request.getParameterValues("sponsorAmount");	
		//Added by:Yogendra||Date:12/12/12||Enhancement:FIN-22469
		String lineItemPatientCount =null;
		if(lineitemMode.equals("M")) {
			lineItemPatientCount = request.getParameter("patno");
		}else{
			lineItemPatientCount = request.getParameter("lineItemPatientCount");
		}
		if(StringUtil.isEmpty(lineItemPatientCount) 
				|| lineItemPatientCount.equalsIgnoreCase("0")){
			lineItemPatientCount="1";
		}
		String hidApplyPatientCountChkbox[]= request.getParameterValues("hidApplyPatientCountChkbox");
		//end

		String eventdata[] = request.getParameterValues("eventdata");
		
		
		String rate[]  = null;
		String cptCode[] = null;
		 
		 rate = request.getParameterValues("rate");
		
		cptCode = request.getParameterValues("cptCode");

		String description[] = request.getParameterValues("desc");

    	String budgetId = request.getParameter("budgetId");
    	//Bug# 3610
    	int bgtId = EJBUtil.stringToNum(budgetId);
    	budgetB.getBudgetInfo(bgtId);
    	budgetB.setModifiedBy(usr);
    	budgetB.setLastModifiedDate(DateUtil.dateToString(new Date()));
    	int rete = budgetB.updateBudget();
    	
		int total = 0;
		int len = 0;
		
		if (eventdata == null)
		{
			
			len = 0;
		}
		else
		{
			len = eventdata.length;
		}
		 

		for (int cnt=0; cnt<len; cnt++)
				{
		
					if(!eventdata[cnt].equals(""))
					{
						total = total +1;
					
					}

				}

		String[] strArrlineitemDesc =  new String[total];
  		String[] strArrlineitemName = new String[total];
		

		ArrayList bgtSecIds= new  ArrayList(); 
		ArrayList eventNames = new ArrayList(); 
		ArrayList sponsorUnit = new ArrayList();
		
		 
		ArrayList cptCodes = new ArrayList(); 

		ArrayList descriptions = new ArrayList(); 
		ArrayList creators = new ArrayList();
		ArrayList ipAdds = new  ArrayList(); 
		ArrayList categories = new ArrayList();
		ArrayList tmids = new ArrayList();
		ArrayList cdms = new ArrayList();
		
		ArrayList costTypes = new ArrayList();
		ArrayList unitCosts = new ArrayList();
		ArrayList numUnits = new ArrayList();
		ArrayList applyIndirects = new ArrayList();
		ArrayList applyDiscounts= new ArrayList();
		ArrayList sectionLineItemPKs= new ArrayList();
	
		ArrayList sponsorAmounts = new ArrayList();
		ArrayList hidApplyPatientChecked = new ArrayList(); //Added by YPS 06 Dec 2012 
				
		LineitemDao lineitemDao= new LineitemDao();

		String creator="";	
		String msg="";
		int count = 0;
		int output=0;
		String lineitemId = "";
		
	
		if(lineitemMode.equals("M")) {
		
			lineitemId = request.getParameter("lineitemId");
			 
		}

		count = EJBUtil.stringToNum(lineCount);
			
		int indx =0 ;
		
	if(lineitemMode.equals("M") && bSection.equals("false")){
	for(i=0;i< count; i++) {
 		if (eventdata[i] != null && !(eventdata[i].equals("")))
  		{
	
			String itemName = eventdata[i];
			String desc = description[i];
			String ctgry = category[i];
			//Rohit CCF-FIN21
			String itemtmid = null;
			String itemcdm = null;
			String cpt = "";
			String rte = "0";
			String sponsorAmountVal="0";
			String hidApplyPatientCheckedVal="0"; //Added by YPS 06 Dec 2012
			
			if(ctgry==null || ctgry.equals(""))
				ctgry = "0";
			
			
					
			lineitemB.setLineitemId(EJBUtil.stringToNum(lineitemId));
			lineitemB.getLineitemDetails();
			
			
			lineitemB.setLineitemName(itemName);
			lineitemB.setLineitemCategory(ctgry);
			lineitemB.setLineitemTMID(itemtmid);
			lineitemB.setLineitemCDM(itemcdm);	
			
			
			if (templateType.equals("C"))
			{
				if (sponsorAmount != null)
				{
					sponsorAmountVal = sponsorAmount[i];
				}
				//Added by:Yogendra||Date:12/12/12||Enhancement:FIN-22469
				if(null!=hidApplyPatientCountChkbox){
					hidApplyPatientCheckedVal = hidApplyPatientCountChkbox[i];
				}
				if(StringUtil.isEmpty(lineItemPatientCount) 
						|| lineItemPatientCount.equals("0")){
					lineItemPatientCount="1";
				}
				 BigDecimal spAmount = new BigDecimal(sponsorAmountVal);
		         BigDecimal patCount = new BigDecimal(Float.parseFloat(lineItemPatientCount));
				lineitemB.setLineItemSponsorAmount(spAmount.toString());		
				lineitemB.setLineitemApplyPatientCount(hidApplyPatientCheckedVal);
			}
			
			
			cpt = cptCode[i];
			lineitemB.setLineitemCptCode(cpt);
			
			if(lineitemInPerSec.equals("1")){
		    rte = rate[i];
			if(rte==null || rte.equals(""))
				rte = "0";
			lineitemB.setLineitemNotes(desc);
			//lineitemB.setLineitemSponsorUnit(rte);
			
							
			}
			else{
		
			lineitemB.setLineitemDesc(desc);
			
			}
			
			//new columns
			
						
			lineitemB.setLineItemCostType(costType[i] );
                	
            lineitemB.setLineitemSponsorUnit( unitCost[i]);
            lineitemB.setLineitemClinicNOfUnit(numUnit[i]);
                	
            lineitemB.setLineitemAppIndirects(applyIndirect[i] );
            lineitemB.setLineitemInCostDisc(applyDiscount[i]);
            
            // Ankit: Bug-11399 Date- 18 Sep 2012 
            BigDecimal bd1 = new BigDecimal(unitCost[i]);
            BigDecimal bd2 = new BigDecimal(Float.parseFloat(numUnit[i]));

			// SM bug #14739
            double doubleOtherCost = (bd1.multiply(bd2)).doubleValue();
            lineitemB.setLineitemOtherCost(""+NumberUtil.roundOffNo(doubleOtherCost));

			} //null check
  		} //counter loop

			//KM-#5897

			String bgtSectionType = request.getParameter("bgtSectionType");
			if (StringUtil.isEmpty(bgtSectionType))
			{
				bgtSectionType="";
			}
			
			if (bgtSectionType.equals("P"))
			{
					String patno = request.getParameter("patno");
					patno= (patno==null)?"":patno;
					
					String applyToAll = request.getParameter("applyToAll");
					
					if (StringUtil.isEmpty(applyToAll ))
					{
						applyToAll ="0";
					}
					
					bgtSectionB.setBgtSectionId(EJBUtil.stringToNum(bgtSectionId));

	 				bgtSectionB.getBgtSectionDetails();	
	 				
					patno= (patno.equals(""))?"1":patno;
	 				bgtSectionB.setBgtSectionPatNo(patno);		
	 				bgtSectionB.setModifiedBy(usr);
	 				bgtSectionB.setIpAdd(ipAdd);
	 				bgtSectionB.updateBgtSection();	
	 				
					if (applyToAll != null && applyToAll.equals("1"))
					{
						//apply to all per pat sections
					
						bgtSectionB.applyNumberOfPatientsToAllSections(bgtSectionId,bgtcalId,patno,usr,ipAdd);	
					}
			}
	 } //mode		
	 
	 if(lineitemMode.equals("N") || ( lineitemMode.equals("M") && bSection.equals("true") ))
	 {
		if ( lineitemMode.equals("M") && bSection.equals("true") )
		{
			String bgtSectionType = request.getParameter("bgtSectionType");
			if (StringUtil.isEmpty(bgtSectionType))
			{
				bgtSectionType="";
			}
			
			if (bgtSectionType.equals("P"))
			{
					String patno = request.getParameter("patno");
					patno= (patno==null)?"":patno;
					
					String applyToAll = request.getParameter("applyToAll");
					
					if (StringUtil.isEmpty(applyToAll ))
					{
						applyToAll ="0";
					}
					
					bgtSectionB.setBgtSectionId(EJBUtil.stringToNum(bgtSectionId));

	 				bgtSectionB.getBgtSectionDetails();	
	 				
					patno= (patno.equals(""))?"1":patno;
	 				bgtSectionB.setBgtSectionPatNo(patno);		
	 				bgtSectionB.setModifiedBy(usr);
	 				bgtSectionB.setIpAdd(ipAdd);
	 				bgtSectionB.updateBgtSection();	
	 				
					if (applyToAll != null && applyToAll.equals("1"))
					{
						//apply to all per pat sections
					
						bgtSectionB.applyNumberOfPatientsToAllSections(bgtSectionId,bgtcalId,patno,usr,ipAdd);	
					}
					
			
			}
		
		}
		 
	 			//int cnt = eventdata.length;
				int internalLen= 0 ;
				for ( int j=0 ; j< count ; j++)
				{
					if   (     !( EJBUtil.isEmpty (eventdata[j]  )   )     )
					{	
						 
						eventNames.add(internalLen,eventdata[j]) ;	
						cptCodes.add(internalLen,cptCode[j]) ;
						descriptions.add(internalLen,description[j]) ;
						creators.add(internalLen,usr) ;
						categories.add(internalLen,category[j]) ;
						//Rohit CCF-FIN21
						tmids.add(internalLen,null) ;
						cdms.add(internalLen,null) ;
						
						costTypes.add(internalLen,costType[j]);
						unitCosts.add(internalLen,unitCost[j]);
						
						numUnits.add(internalLen,numUnit[j]);
						applyIndirects.add(internalLen,applyIndirect[j]);
						applyDiscounts.add(internalLen,applyDiscount[j]);
						
						sectionLineItemPKs.add(internalLen,sectionLineItemPK[j]);
						
						
						if (templateType.equals("C"))
						{			
							sponsorAmounts.add(internalLen,sponsorAmount[j]);
							//Added by:Yogendra||Date:12/12/12||Enhancement:FIN-22469
							hidApplyPatientChecked.add(internalLen,hidApplyPatientCountChkbox[j]);
						}
						else
						{
							sponsorAmounts.add("");
						}	
												
						internalLen = internalLen + 1 ;
						
								
					}
			
					
				}
					
				
	 }
		int val = strArrlineitemName.length;
	
	if(lineitemMode.equals("M") && bSection.equals("false")){
		lineitemB.setModifiedBy(usr); //Amarnadh #3010	
		output = lineitemB.updateLineitem();
	}else{
		Hashtable htMoreData = new Hashtable();
		
		htMoreData.put("costTypes",costTypes);
		htMoreData.put("unitCosts",unitCosts);
		htMoreData.put("numUnits",numUnits);
		htMoreData.put("applyIndirects",applyIndirects);
		htMoreData.put("applyDiscounts",applyDiscounts);
		htMoreData.put("bSection",bSection);
		htMoreData.put("sectionLineItemPKs",sectionLineItemPKs);
		htMoreData.put("sponsorAmounts",sponsorAmounts);
		htMoreData.put("lineItemPatientCount",lineItemPatientCount);
		htMoreData.put("hidApplyPatientChecked",hidApplyPatientChecked);
		htMoreData.put("templateType",templateType);
		//for bSection==true, insertAllLineItems will handle updates
		output= lineitemB.insertAllLineItems(eventNames,descriptions,cptCodes,categories,tmids,cdms,bgtSectionId,ipAdd,usr,htMoreData);
	}
 
	
	if (output == 0) {
		//tSession.setAttribute("lineItemDeleted", "Y");  --KM
		msg = MC.M_Data_SavedSucc;/*msg = "Data saved successfully";*****/
   	}else {
		msg = MC.M_Data_NotSaved;/*msg = "Data not saved";*****/
	}	
	%>

 <script>
	window.opener.location.reload();
	setTimeout("self.close()",1000);
</script>
 
<br>
<br>
<br>
<br>
<br>
<p class = "successfulmsg" align = center> <%=msg%> </p>
	<%	} // end of esign
		}//end of if body for session
	else
	{
	%>
	<jsp:include page="timeout.html" flush="true"/>
	<%
	}	
	%>
	</BODY>
	</HTML>
