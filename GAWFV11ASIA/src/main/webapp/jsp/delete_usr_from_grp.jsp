<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML> 
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<TITLE><%=MC.M_Del_UserFromGrp%><%--Delete User from Group*****--%></TITLE>
</HEAD>

<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

<%-- 	if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   } --%>
}
</SCRIPT>


<% String src;

src= request.getParameter("srcmenu");

%>

<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>   
<jsp:include page="skinChoser.jsp" flush="true"/>


<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%><%@page import="com.velos.eres.service.util.*,com.aithent.audittrail.reports.AuditUtils"%>

<jsp:useBean id="usrGrpB" scope="request" class="com.velos.eres.web.usrGrp.UsrGrpJB"/>
<jsp:useBean id="userB" scope="session" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<body>

<br>

<DIV class="formDefault" id="div1">

<% 

HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession))	{
	int usrGrpId = EJBUtil.stringToNum(request.getParameter("usrGrpId"));
	String delMode=request.getParameter("delMode");
	if (delMode==null) {
		delMode="final";
%>

	<FORM name="deleteUsr" id="delusrfrmgrp" method="post" action="delete_usr_from_grp.jsp" onSubmit="if (validate(document.deleteUsr)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<br><br>	
	<TABLE width="98%" cellspacing="0" cellpadding="0" >
		<tr><td align="center">
		<b><%=MC.M_PlsEtrEsign_Del%><%--Please enter e-Signature to proceed with deletion*****--%> </b>
		</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
	
		</table>
			
	<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="delusrfrmgrp"/>
			<jsp:param name="showDiscard" value="N"/>
	</jsp:include>


 	 <input type="hidden" name="delMode" value="<%=delMode%>">
  	 <input type="hidden" name="srcmenu" value="<%=src%>">
  	 <input type="hidden" name="usrGrpId" value="<%=usrGrpId%>">	 
	</FORM>
<%
	} else {
			String eSign = request.getParameter("eSign");	
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>
	 		  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
			} else {
	
	String strUserId = "";
	String strGroupId = "";
	String defGroup = "";

	usrGrpB.setUsrGrpId(usrGrpId);
	usrGrpB.getUsrGrpDetails();

	strUserId = usrGrpB.getUsrGrpUserId();
	strGroupId = usrGrpB.getUsrGrpGroupId();
	
	userB.setUserId(Integer.parseInt(strUserId));
	userB.getUserDetails();
	
	defGroup = userB.getUserGrpDefault();
	
	if (Integer.parseInt(defGroup) == Integer.parseInt(strGroupId)) {
	%>
 		<br> <br> <br> <br> <p class = "successfulmsg" align = center><%=MC.M_TryRemUsrGrp_ChgDefGrp%><%--You are trying to remove the user from his/her default group. Please change the default group of this user from user's profile and then try again.*****--%> </p>
		<p align = center> <A type="submit" href="accountbrowser.jsp?srcmenu=<%=src%>"><%=LC.L_Back%></A> </p> 	
	<%
	} else {
	
		int i = 0;
		// Modified for INF-18183 ::: AGodara 
		i = usrGrpB.removeUsrFromGrp(AuditUtils.createArgs(tSession,"",LC.L_Manage_Acc));
%>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<%
		if(i == 0) {
%>
<p class = "successfulmsg" align = center> <%=MC.M_UsrDel_FrmGrpSucc%><%--The user has been deleted from this group successfully.*****--%></p>
<%
		} else {
%>
<p class = "successfulmsg" align = center> <%=MC.M_UsrCntDel_FromThisGrp%><%--The user could not be deleted from this group.*****--%> </p>
<%
		}
	}//end of defGroup
%>
 <META HTTP-EQUIV=Refresh CONTENT="3; URL=accountbrowser.jsp?srcmenu=<%=src%>"> 
<%	
		} //end esign
	} //end of delMode
}//end of if body for session

else

{

%>

<jsp:include page="timeout.html" flush="true"/> 

<%

}

%>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
  
</DIV>

<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/> 
</div>

</body>

</HTML>
