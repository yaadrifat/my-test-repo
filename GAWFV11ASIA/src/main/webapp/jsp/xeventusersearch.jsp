<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_Evt_UserPage %><%-- Event User Page*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<Link Rel=STYLESHEET HREF="common.css" type=text/css>

</head>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.esch.business.common.EventInfoDao,com.velos.eres.service.util.*"%>
<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB"/>

<% response.setHeader("Cache-Control", "max-age=1, must-revalidate");%>


<% 
String userType = request.getParameter("userType");
String src = request.getParameter("srcmenu");
	String duration = request.getParameter("duration");   
	String protocolId = request.getParameter("protocolId");   
	String calledFrom = request.getParameter("calledFrom");   
	String eventId = request.getParameter("eventId");
	String mode = request.getParameter("mode");
	String fromPage = request.getParameter("fromPage");
%>
	
<body>
<DIV id="div1"> 
<%


	HttpSession tSession = request.getSession(true); 

	if (sessionmaint.isValidSession(tSession))
	{
	   String uName = (String) tSession.getValue("userName");
	   UserDao userDao = userB.getUsersForEvent(EJBUtil.stringToNum(eventId),userType);
	   ArrayList userIds = userDao.getUsrIds();
	   ArrayList userLnames = userDao.getUsrLastNames();
	   ArrayList userFnames = userDao.getUsrFirstNames();
	   int rows = userDao.getCRows();

	   String userId = "";
	   String userLname = "";	
	   String userFname = "";	
	   String userName = "";	
  
if(userType.equals("U")) {
%>
   <P class="sectionHeadings"> <%=MC.M_PcolCalEvt_EvtUsr %><%-- Protocol Calendar >> Event Details >> Event User*****--%> </P>
<%
} else {
%>
   <P class="sectionHeadings"> <%=MC.M_PcolCalEvt_UsrMail %><%-- Protocol Calendar >> Event Details >> Users for Mail*****--%> </P>
<%
}

%>
 
  <Form  name="search" method="post" action="addeventuser.jsp?userType=<%=userType%>&eventId=<%=eventId%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>"  >
    <table class = tableDefault width="80%" cellspacing="0" cellpadding="0" border=0 >
      <tr> 
        <td class=tdDefault width=20%> <%=LC.L_User_Search %><%-- User Search*****--%></td>
        <td class=tdDefault width=25%> <%=LC.L_First_Name %><%-- First Name*****--%>: 
          <Input type=text name="fname">
        </td>
        <td class=tdDefault width=25%> <%=LC.L_Last_Name %><%-- Last Name*****--%>: 
          <Input type=text name="lname">
        </td>
        <td class=tdDefault width=10%> 
          <button type="submit"><%=LC.L_Search%></button>
        </td>
      </tr>
    </table>
  </Form>
<%
if(rows > 0) {
%>
  <Form name="user" method="post" >
    <table width="80%" cellspacing="0" cellpadding="0" border=0 >
      <tr> 
        <th width="80%"> <%=LC.L_User_Name %><%-- User Name*****--%> </th>
      </tr>
<%
   for(int i = 0;i<rows;i++){	
	userId = ((Integer) userIds.get(i)).toString();
	userLname = (String) userLnames.get(i);
	userFname = (String) userFnames.get(i);
	userName = userLname + ", " + userFname;	

		if ((i%2)==0) {

  %>
      <tr class="browserEvenRow"> 
        <%

		}

		else{

  %>
      <tr class="browserOddRow"> 
        <%

		}

  %>
        <td> <%=userName%></td>
      </tr>
<%
   }

%>
    </table>
  </Form>
<%
} else { // else of if for rows>0
%>
<P class = "defComments"> 
<%=MC.M_NoUsrAttached_ToEvt %><%-- No users currently attached to this event*****--%>
</P>
  <%
} // end of if for rows>0
}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>

  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>

</body>
</html>



