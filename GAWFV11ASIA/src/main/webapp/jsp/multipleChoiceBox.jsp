<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Fld_MultiChoice%><%--Field Multiple Choice*****--%> </title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<jsp:include page="include.jsp" flush="true"/>
</head>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="whichcss.js"></SCRIPT>
<script type="text/javascript" src="./FCKeditor/fckeditor.js"></script>
<script type="text/javascript">
	checkQuote="N";
	var oFCKeditor;
	var fck;	
     function init()
      {
       	enableSubmit(true);
      /*oFCKeditor = new FCKeditor("nameta") ;
      oFCKeditor.BasePath = './FCKeditor/';
      oFCKeditor.Width = editorWidth ;
      oFCKeditor.Height = editorHeight ; // 400 pixels
      oFCKeditor.ToolbarSet="Velos";
      //oFCKeditor.Config['SkinPath'] = '/FCKeditor/editor/skins/office2003/' ;
      oFCKeditor.ReplaceTextarea() ;*/
      	document.multipleBoxField.category.focus();
      }
       function processFormat(formobj,mode)
      {
       
      	if (mode=="D")
	{
	 if (formobj.nameta.value.length>0)
	 formobj.nameta.value="[VELDISABLE]"+formobj.nameta.value;
	 //showHide("eformat","S");
	 //showHide("dformat","H");
	 document.poplayer=document.getElementById("dformat");
	 document.poplayer.innerHTML="<A href='javascript:void(0)' onClick='processFormat(document.multipleBoxField,\"E\")'><img src=\"./images/enableformat.gif\" alt=\"<%=LC.L_Enable_Formatting%>/*Enable Formatting*****/\" border = 0 align=absbotton></A>";
	 
	} else if (mode=="R")
	{
	   if (confirm("<%=MC.M_RemFmt_ForFldName%>"))/*if (confirm("Remove formatting for field name?"))*****/
	   {
	    formobj.nameta.value="";
	    
	   }
	} else if(mode=="E")
	{
	 formobj.nameta.value=replaceSubstring(formobj.nameta.value,"[VELDISABLE]","");
	 //showHide("eformat","H");
	 //showHide("dformat","S");
	 document.poplayer=document.getElementById("dformat");
	 document.poplayer.innerHTML="<A href='javascript:void(0)' onClick='processFormat(document.multipleBoxField,\"D\")'><img src=\"./images/disableformat.gif\" alt=\"<%=LC.L_Disable_Formatting%>/*Disable Formatting*****/\" border = 0 align=absbotton></A>";
	}
	
      }
    </script>

	<script type="text/javascript">
     overRideChar("#");
   overRideChar("&");
   overRideFld("name");
   var editor;
    function FCKeditor_OnComplete(oFCKeditor)
	{
	 fck= FCKeditorAPI.GetInstance('nameta');
	 
	}
  </script>




<SCRIPT Language="javascript">
window.onload=function()
 {
  init();
 }
function toRefresh(formobj)
{
		if(formobj.toRefresh.value=="yes")
		{
			window.opener.location.reload();
		}
}

 function confirmBox(respName,pgRight,totRows) {
 //can't delete all the responses
	if (totRows==1) {
	   alert("<%=MC.M_CntDel_AllRespMultiFld%>");/*alert("You cannot delete all the reponses of Multiple Choice Field.");*****/
	   return false;
	}
	 
	if (f_check_perm(pgRight,'E') == true) {
	var paramArray = [respName];
	msg=getLocalizedMessageString("L_Del_FrmResp",paramArray);/*msg="Delete " + respName + " from Responses?";*****/

	if (confirm(msg)) 
	{
    	return true;
	}
	else
	{
		return false;
	}
	} else {
		return false;
	}
 }


 function  validate(formobj)
 {	
    	
    if (!(validate_col('Category',formobj.category))) return false
    
    
    //if (!(validate_col_spl('Field Name',formobj.name))) return false;
    
    
    
    if (!(validate_col('Field Name',formobj.name)))  return false;
	 
	 
	 
	  if (checkChar(formobj.name.value,"'"))
	 {
	   alert("Special Character(') not allowed for this Field");
	    formobj.name.focus();
	    return false;
	 }
	
	 
	     
    if ((formobj.name.value.length)>2000)
	{
	  alert("<%=MC.M_FldNameMax2000_CrtCont%>");/*alert("'Field Name' length exceeds the maximum allowed limit of 2000 characters.\n Please correct to continue.");*****/
	   formobj.name.focus();
	  return false;
	}
	
	//set the value from textarea editor to hidden field
	
	/*if (formobj.nameta.value.length>0)
	formobj.nameta.value=htmlEncode(formobj.nameta.value);*/
	 	
	
	
    if (!(validate_col_spl('Field Uniqueid',formobj.uniqueId))) return false;

     if ((fnTrimSpaces((formobj.uniqueId.value).toLowerCase())=='er_def_date_01'))
	{
	  alert("<%=MC.M_ErDefDt01_SysKwrdFld%>");/*alert("'er_def_date_01' is a system reserved keyword. Please specify another Field Id.");*****/
	  formobj.uniqueId.focus();
	  return false;
	}
	
	if(!( validateDataSize(formobj.instructions,1000,'Field Help'))) return false;
		

	
	if (! (splcharcheck(formobj.keyword.value)))
	{
		  formobj.keyword.focus();
		  return false;
	}
	if (! (checkquote(formobj.instructions.value)))
	{
		  formobj.instructions.focus();
		  return false;
	}
	
	 if (! (splcharcheckForXSL(formobj.instructions.value)))
	{
		  formobj.instructions.focus();
		  return false;
	} 
	
	  if(isNaN(formobj.colcount.value) == true)
		   {
	     		alert("<%=MC.M_Vldt_NumColumn%>");/*alert("Please enter a valid number in columns.");*****/
				 formobj.colcount.focus();
				 return false;
			} 	

	if   (  formobj.editBoxType[1].checked  )
	{
		
		 if (!(validate_col('ColCount',formobj.colcount))) return false;
		   if(isNaN(formobj.colcount.value) == true)
		   {
			   alert("<%=MC.M_Vldt_NumColumn%>");/*alert("Please enter a valid number in columns.");*****/
				 formobj.colcount.focus();
				 return false;
			}
		 
	} 
	if   ( formobj.editBoxType[2].checked) 
	{
		
		 if (!(validate_col('ColCount',formobj.colcount))) return false;
		  if(isNaN(formobj.colcount.value) == true)
		   {
			  alert("<%=MC.M_Vldt_NumColumn%>");/*alert("Please enter a valid number in columns.");*****/
				 formobj.colcount.focus();
				 return false;
			}
				 
	}
		 
		 
		 
	
	if (!(validate_col('e-Signature',formobj.eSign))) return false;
	
	if (formobj.mode.value=="N") 
	{
		return validateForNewMode(formobj);
		
	}
	
	totRows = formobj.totRows.value;
	
	if (formobj.mode.value=="M") {
	    respEntered = false;

if (totRows > 1) {

//check duplicate display value 

	var len,currentValue1,currentValue2;
	var currentDataValue1,currentDataValue2;	
	len=formobj.totRows.value;
	
	var flagDispMod=0;
	var flagDataValDup = 0;	

	for (ir=0;ir<len;ir++){
		currentValue1=fnTrimSpaces(document.multipleBoxField.txtDispVal[ir].value);
		
		currentDataValue1=fnTrimSpaces(document.multipleBoxField.txtDataVal[ir].value);
			
		if (currentValue1.length==0){
			document.multipleBoxField.txtDispVal[ir].value="";
			continue;
		}
		
		/*if (currentDataValue1.length==0){
			document.multipleBoxField.txtDataVal[ir].value="";
			continue;
		}*/
		
		
		for(jr=0;jr<len;jr++){
			if (ir!=jr){
				currentValue2=fnTrimSpaces(document.multipleBoxField.txtDispVal[jr].value);
				
				currentDataValue2=fnTrimSpaces(document.multipleBoxField.txtDataVal[jr].value);
				 
				if(currentValue2.toLowerCase()==currentValue1.toLowerCase()){
					flagDispMod=flagDispMod+1;
					//return false;

			   }
			   	
			   	if (currentDataValue1.length > 0) { 
			   		if(currentDataValue2.toLowerCase()==currentDataValue1.toLowerCase()){
					flagDataValDup=flagDataValDup+1;
				
			     }
			   }
			   
			}
		}
	}
	
	
	
		
	// In case "add more responses", checks duplicate display value entry while  previous responses exist
	// modified by J. Majumdar on 14Feb05
	if(document.multipleBoxField.newTxtDispVal){ 
				
		if(document.multipleBoxField.newTxtDispVal.length>=2){
			len1=document.multipleBoxField.newTxtDispVal.length;			
			
		}
		else{
			len1=1;			
		}
		
			
		for (irn=0;irn<len;irn++){
			
			currentValue1=fnTrimSpaces(document.multipleBoxField.txtDispVal[irn].value);
			
			currentDataValue1=fnTrimSpaces(document.multipleBoxField.txtDataVal[irn].value);
				
			if (currentValue1.length==0){
				document.multipleBoxField.txtDispVal[irn].value="";
				continue;
			}
			
			/*if (currentDataValue1.length==0){
				document.multipleBoxField.txtDataVal[irn].value="";
				continue;
			}*/
			
			for(jrn=0;jrn<len1;jrn++){
				// "add more responses" are more than 1
				if(document.multipleBoxField.newTxtDispVal.length>=2){
				
				currentValue2=fnTrimSpaces(document.multipleBoxField.newTxtDispVal[jrn].value);
				
				currentDataValue2=fnTrimSpaces(document.multipleBoxField.newTxtDataVal[jrn].value);
				
				if (currentValue2.length==0){
					
					document.multipleBoxField.newTxtDispVal[jrn].value="";
					continue;

				}
				 
				/*if (currentDataValue2.length==0){
					
					document.multipleBoxField.newTxtDataVal[jrn].value="";
					continue;

				}
				*/
				
				}	
				
				else 	// "add more responses" is equal to 1 
					
				{
					
				currentValue2=fnTrimSpaces(document.multipleBoxField.newTxtDispVal.value); 				
				currentDataValue2=fnTrimSpaces(document.multipleBoxField.newTxtDataVal.value);

				if (currentValue2.length==0){
					document.multipleBoxField.newTxtDispVal.value="";
					continue;

				}
				
				/*if (currentDataValue2.length==0){
					document.multipleBoxField.newTxtDataVal.value="";
					continue;

				} */
				
				}
				if(currentValue2.toLowerCase()==currentValue1.toLowerCase()){
					flagDispMod=flagDispMod+1;
			   }
			   
			   if (currentDataValue1.length > 0) {
				   if(currentDataValue2.toLowerCase()==currentDataValue1.toLowerCase()){
						flagDataValDup=flagDataValDup+1;
				   }
			   }
			   
            }
		}
	}
	

	if(flagDispMod>0){
		alert("<%=MC.M_DupDisp_EtrDiffVal%>");/*alert("Duplicate Display Value exists. Please enter different value(s).");*****/
		return false;
	}

	if(flagDataValDup>0){
		alert("<%=MC.M_ValueExist_EtrDiff%>");/*alert("Duplicate Data Value exists. Please enter different value(s).");*****/
		return false;
	}





		
        	for(cnt = 0; cnt<totRows;cnt++){
    		//check for valid sequence
    			 if(isNaN(formobj.txtSeq[cnt].value) == true) {
    			     alert("<%=MC.M_InvalidSeq_EtrVldNum%>");/*alert("Invalid Sequence. Please enter a valid number.");*****/
    				 formobj.txtSeq[cnt].focus();
    				 return false;
    			}
    			
    		//check for valid score
    			if(isNaN(formobj.txtScore[cnt].value) == true) {
    			     alert("<%=MC.M_InvalidScore_ReEtr%>");/*alert("Invalid Score. Please enter a valid number.");*****/
    				 formobj.txtScore[cnt].focus();
    				 return false;
    			}
    			
    		//check if display val and data value are entered if seq has been entered 
			//fnTrimSpaces function is newly applied
     	   if ((formobj.txtSeq[cnt].value != "")){
    			    respEntered = true;
        			if(fnTrimSpaces(formobj.txtDispVal[cnt].value)=='' ){
                        alert("<%=MC.M_Etr_MandantoryFlds%>")/*alert("Please enter data in all mandatory fields")*****/
                        formobj.txtDispVal[cnt].focus()
                        return false;
        			}
        			
        	     }    
    			 
    			 //check if sequence and data value are entered if display value has been entered			 
         	     if ((formobj.txtDispVal[cnt].value != "")){
        			if(formobj.txtSeq[cnt].value=='' ){
        				alert("<%=MC.M_Etr_MandantoryFlds%>")/*alert("Please enter data in all mandatory fields")*****/
                        formobj.txtSeq[cnt].focus()
                        return false;
        			}
        			
    			 }
    			 
    	 		 //check if sequence and display value are entered if data value has been entered			 
         	     if ((formobj.txtDataVal[cnt].value != "")){
        			if(formobj.txtSeq[cnt].value=='' ){
        				alert("<%=MC.M_Etr_MandantoryFlds%>")/*alert("Please enter data in all mandatory fields")*****/
                        formobj.txtSeq[cnt].focus()
                        return false;
        			}
        			if(formobj.txtDispVal[cnt].value=='' ){
        				alert("<%=MC.M_Etr_MandantoryFlds%>")/*alert("Please enter data in all mandatory fields")*****/
                        formobj.txtDispVal[cnt].focus()
                        return false;
        			}			 			 
    			 }
    			 
        	}//for
    	   if (respEntered==false) 
    		{
    		   alert("<%=MC.M_Etr_MandantoryFlds%>")/*alert("Please enter data in all mandatory fields")*****/
    		 formobj.txtSeq[0].focus();
    		 return false;
    		}
			
		} else {//else if totRows
		    	//check for valid sequence
    			 if(isNaN(formobj.txtSeq.value) == true) {
    				 alert("<%=MC.M_InvalidSeq_EtrVldNum%>");/*alert("Invalid Sequence. Please enter a valid number.");*****/
    				 formobj.txtSeq.focus();
    				 return false;
    			}
    			
    		//check for valid score
    			if(isNaN(formobj.txtScore.value) == true) {
    				alert("<%=MC.M_InvalidScore_ReEtr%>");/*alert("Invalid Score. Please enter a valid number.");*****/
    				 formobj.txtScore.focus();
    				 return false;
    			}
    			
    		//check if display val and data value are entered if seq has been entered                
     	    /* if ((formobj.txtSeq.value != "")){
    			    respEntered = true;
        			if(formobj.txtDispVal.value=='' ){
                        alert("Please enter data in all mandatory fields")
                        formobj.txtDispVal.focus()
                        return false;
        			}
        			
        	     }*/    
    			 
    			 //check if sequence and data value are entered if display value has been entered			 
         	     if ((formobj.txtDispVal.value != "")){
        			if(formobj.txtSeq.value=='' ){
        				alert("<%=MC.M_Etr_MandantoryFlds%>")/*alert("Please enter data in all mandatory fields")*****/
                        formobj.txtSeq.focus()
                        return false;
        			}
        			
    			 }
    			 
    	 		 //check if sequence and display value are entered if data value has been entered			 
         	     if ((formobj.txtDataVal.value != "")){
        			if(formobj.txtSeq.value=='' ){
        				alert("<%=MC.M_Etr_MandantoryFlds%>")/*alert("Please enter data in all mandatory fields")*****/
                        formobj.txtSeq.focus()
                        return false;
        			}
        			if(formobj.txtDispVal.value=='' ){
        				alert("<%=MC.M_Etr_MandantoryFlds%>")/*alert("Please enter data in all mandatory fields")*****/
                        formobj.txtDispVal.focus()
                        return false;
        			}			 			 
    			 }
		
    	   /* if (respEntered==false) 
    		{
    		 alert("Please enter data in all mandatory fields");
    		 formobj.txtSeq.focus();
    		 return false;
    		}*/

		// In case "add more responses", checks duplicate display value entry while existing response is equal to 1 
		//added by J. Majumdar date 14Feb05
		
		var flagDispMod1=0; var flagDataValDup = 0; var currentDataValue1; var currentDataValue2;
		
				 if(document.multipleBoxField.newTxtDispVal){
					 
					 currentValue1=fnTrimSpaces(document.multipleBoxField.txtDispVal.value);
					 
					 currentDataValue1=fnTrimSpaces(document.multipleBoxField.txtDataVal.value);
					
					if (currentValue1.length==0)
						document.multipleBoxField.txtDispVal.value="";
					
					
					/*if (currentDataValue1.length==0)
						document.multipleBoxField.txtDataVal.value="";*/
					

					if(document.multipleBoxField.newTxtDispVal.length>=2){
						len=document.multipleBoxField.newTxtDispVal.length
					}else{
						len=1;	
					}

					 for(jrne=0;jrne<len;jrne++){
						// "add more responses" are more than 1
							if(document.multipleBoxField.newTxtDispVal.length>=2)
							{
								currentValue2=fnTrimSpaces(document.multipleBoxField.newTxtDispVal[jrne].value); 
								
								currentDataValue2=fnTrimSpaces(document.multipleBoxField.newTxtDataVal[jrne].value);

								if (currentValue2.length==0){
								
								document.multipleBoxField.newTxtDispVal[jrne].value="";
								continue;
							}
							
							/*if (currentDataValue2.length==0){
								
								document.multipleBoxField.newTxtDataVal[jrne].value="";
								continue;
							} */
							
							}
							else{
						// "add more responses" is equal to 1
								currentValue2=fnTrimSpaces(document.multipleBoxField.newTxtDispVal.value);
								
								currentDataValue2=fnTrimSpaces(document.multipleBoxField.newTxtDataVal.value);
									
								if (currentValue2.length==0){
								
								document.multipleBoxField.newTxtDispVal.value="";
								continue;
							}
								
								/*if (currentDataValue2.length==0){
								
								document.multipleBoxField.newTxtDataVal.value="";
								continue;
							}*/

							}							

					
							if(currentValue2.toLowerCase()==currentValue1.toLowerCase()){
									flagDispMod1=flagDispMod1+1;
							}
							
							if (currentDataValue1.length > 0) {
								if(currentDataValue2.toLowerCase()==currentDataValue1.toLowerCase()){
										flagDataValDup=flagDataValDup+1;
								}
							}
							
							
				     }
							
				if(flagDispMod1>0){
					alert("<%=MC.M_DupDisp_EtrDiffVal%>");/*alert("Duplicate Display Value exists. Please enter different value(s).");*****/
					return false;
				}
				
				if(flagDataValDup>0){
					alert("<%=MC.M_ValueExist_EtrDiff%>");/*alert("Duplicate Data Value exists. Please enter different value(s).");*****/
					return false;
				}
				
				
				
			}
			


				
		//new add 
		
		
		
		
		}//end if totRows
	} //mode=M
	
	if (!(validate_col('e-Signature',formobj.eSign))) return false;
	if (formobj.fromRefresh.value=="true") 
	{
		return validateForNewMode(formobj);
	}
	
	if (!(validate_col('e-Signature',formobj.eSign))) return false;
	/*if(isNaN(formobj.eSign.value) == true) 
	{
		alert*/<%--("<%=MC.M_IncorrEsign_EtrAgain%>");--%>
		/*alert("Incorrect e-Signature. Please enter again");
		formobj.eSign.focus();
		return false;
   	}*/

	return true;
  }

  
function validateForNewMode(formobj)
{
	 totRows = formobj.defRespNo.value;
	 countDisp = 0;
	 respEntered = false;
	 if (totRows>1) {
    	for(cnt = 0; cnt<totRows;cnt++){





	
		//check for valid sequence
			 if(isNaN(formobj.newTxtSeq[cnt].value) == true) {
				 alert("<%=MC.M_InvalidSeq_EtrVldNum%>");/*alert("Invalid Sequence. Please enter a valid number.");*****/
				 formobj.newTxtSeq[cnt].focus();
				 return false;
			}
			
		//check for valid score
			if(isNaN(formobj.newTxtScore[cnt].value) == true) {
				alert("<%=MC.M_InvalidScore_ReEtr%>");/*alert("Invalid Score. Please enter a valid number.");*****/
				 formobj.newTxtScore[cnt].focus();
				 return false;
			}
			
		//check if display val entered if seq has been entered 
 	/*     if ((formobj.newTxtSeq[cnt].value != "")){                                 
			    respEntered = true;
    			if(formobj.newTxtDispVal[cnt].value=='' ){
                    alert("Please enter data in all mandatory fields")
                    formobj.newTxtDispVal[cnt].focus()
                    return false;
    			}
    			
    	     }*/    
			 
			 //check if sequence entered if display value has been entered			 
     	     if ((formobj.newTxtDispVal[cnt].value != "")){
    			if(formobj.newTxtSeq[cnt].value=='' ){
    				alert("<%=MC.M_Etr_MandantoryFlds%>")/*alert("Please enter data in all mandatory fields")*****/
                    formobj.newTxtSeq[cnt].focus()
                    return false;
    			}
    			
			 }
			 
	 		 //check if sequence and display value are entered if data value has been entered			 
     	     if ((formobj.newTxtDataVal[cnt].value != "")){
    			if(formobj.newTxtSeq[cnt].value=='' ){
    				alert("<%=MC.M_Etr_MandantoryFlds%>")/*alert("Please enter data in all mandatory fields")*****/
                    formobj.newTxtSeq[cnt].focus()
                    return false;
    			}
    			if(formobj.newTxtDispVal[cnt].value=='' ){
    				alert("<%=MC.M_Etr_MandantoryFlds%>")/*alert("Please enter data in all mandatory fields")*****/
                    formobj.newTxtDispVal[cnt].focus()
                    return false;
    			}			 			 
			 }
			 
    	}//for

	// checks duplicate display value entry, new mode
	var len,currentValue1,currentValue2,currentDataValue1,currentDataValue2;
			
	len=totRows;
	var flagDispNew=0,flagDataNew=0;	

	for (ir=0;ir<len;ir++){
		currentValue1=fnTrimSpaces(document.multipleBoxField.newTxtDispVal[ir].value);
		
		currentDataValue1=fnTrimSpaces(document.multipleBoxField.newTxtDataVal[ir].value);
		
		
		if (currentValue1.length==0){
			document.multipleBoxField.newTxtDispVal[ir].value=""; 
			continue;

		}
		
		/*if (currentDataValue1.length==0){
			document.multipleBoxField.newTxtDataVal[ir].value=""; 
			continue;

		}*/
		
		for(jr=0;jr<len;jr++){
			if (ir!=jr){
				currentValue2=fnTrimSpaces(document.multipleBoxField.newTxtDispVal[jr].value); 
				
				currentDataValue2=fnTrimSpaces(document.multipleBoxField.newTxtDataVal[jr].value);

			if (currentValue2.length==0){
						document.multipleBoxField.newTxtDispVal[jr].value="";
						continue;

					}

			/*if (currentDataValue2.length==0){
						document.multipleBoxField.newTxtDataVal[jr].value="";
						continue;

					} */
					
					
				if(currentValue2.toLowerCase()==currentValue1.toLowerCase()){
					flagDispNew=flagDispNew+1;
					//return false;

			   }
			   
			   if (currentDataValue1.length > 0) {
				   	if(currentDataValue2.toLowerCase()==currentDataValue1.toLowerCase()){
						flagDataNew=flagDataNew+1;
						//return false;
	
				   }
			  }			   
			   
			   
			   
			}
		}
	}

	if(flagDispNew>0){
		alert("<%=MC.M_DupDisp_EtrDiffVal%>");/*alert("Duplicate Display Value exists. Please enter different value(s).");*****/
		return false;
	}
	
	if(flagDataNew>0){
		alert("<%=MC.M_ValueExist_EtrDiff%>");/*alert("Duplicate Data Value exists. Please enter different value(s).");*****/
		return false;
	}

	 /* if (respEntered==false) 
		{
		 alert("Please enter data in all mandatory fields");
		 formobj.newTxtSeq[0].focus();
		 return false;
		}*/
		 for(cnt = 0; cnt<totRows;cnt++){
				
				if (fnTrimSpaces(formobj.newTxtDispVal[cnt].value)=='')  //fnTrimSpaces added 
					{
					countDisp++;
				}
			
			}

	
	
	if(countDisp == totRows)
	{
		
		alert("<%=MC.M_EtrAtleast_OneResp%>");/*alert("Please enter atleast one Response");*****/
		formobj.newTxtDispVal[0].focus();
		return false;

	}
			
	} else {//else if totRows

		//check for valid sequence
			 if(isNaN(formobj.newTxtSeq.value) == true) {
				 alert("<%=MC.M_InvalidSeq_EtrVldNum%>");/*alert("Invalid Sequence. Please enter a valid number.");*****/
				 formobj.newTxtSeq.focus();
				 return false;
			}
			
		//check for valid score
			if(isNaN(formobj.newTxtScore.value) == true) {
				alert("<%=MC.M_InvalidScore_ReEtr%>");/*alert("Invalid Score. Please enter a valid number.");*****/
				 formobj.newTxtScore.focus();
				 return false;
			}
			
		//check if display val entered if seq has been entered						
 	     /*if ((formobj.newTxtSeq.value != "")){
			    respEntered = true;
    			if(formobj.newTxtDispVal.value=='' ){
                    alert("Please enter data in all mandatory fields")
                    formobj.newTxtDispVal.focus()
                    return false;
    			}
    			
    	     }*/    
			 
			 //check if sequence entered if display value has been entered			 
     	     if ((formobj.newTxtDispVal.value != "")){
    			if(formobj.newTxtSeq.value=='' ){
    				alert("<%=MC.M_Etr_MandantoryFlds%>")/*alert("Please enter data in all mandatory fields")*****/
                    formobj.newTxtSeq.focus()
                    return false;
    			}
			 }
			 
	 		 //check if sequence and display value are entered if data value has been entered			 
     	     if ((formobj.newTxtDataVal.value != "")){
    			if(formobj.newTxtSeq.value=='' ){
    				alert("<%=MC.M_Etr_MandantoryFlds%>")/*alert("Please enter data in all mandatory fields")*****/
                    formobj.newTxtSeq.focus()
                    return false;
    			}
    			if(formobj.newTxtDispVal.value=='' ){
    				alert("<%=MC.M_Etr_MandantoryFlds%>")/*alert("Please enter data in all mandatory fields")*****/
                    formobj.newTxtDispVal.focus()
                    return false;
    			}			 			 
			 }
			 
	   /* if (respEntered==false) 
		{
		 alert("Please enter data in all mandatory fields");
		 formobj.newTxtSeq.focus();
		 return false;
		}*/
	
	if (!(validate_col('e-Signature',formobj.eSign))) return false;
	
	}//end if totRows
	
	return true;
}
  

function setDefVal(formobj,totRows,modRows) 
{
   modRows = formobj.totRows.value;
   if (totRows > 1 ) {
	for (i=0;i<totRows;i++)
	{
		if (formobj.newRdDefault[i].checked) 
			{
				formobj.newHdDefault[i].value="1"
			} else {
				formobj.newHdDefault[i].value="0"
			}
	}
  } else {
		if (formobj.newRdDefault.checked){
			formobj.newHdDefault.value="1"
		} else {
			formobj.newHdDefault.value="0"
		}
  } //if

	if (modRows==1) {	
		if (formobj.rdDefault.checked) {
			formobj.rdDefault.checked = false;
			formobj.hdDefault.value="0";			
		}
	} else  {

		for (i=0;i<modRows;i++) {
			 if (formobj.rdDefault[i].checked) {
			 	formobj.rdDefault[i].checked = false;
				formobj.hdDefault[i].value="0"				
			 }
	    }
	} 		   
  
}

function setHdDefVal(formobj,totRows) 
{	
  newRows  = formobj.defRespNo.value;
  if (totRows > 1) { 
	for (i=0;i<totRows;i++)
	{
		if (formobj.rdDefault[i].checked) 
			{
				formobj.hdDefault[i].value="1"
			} else {
				formobj.hdDefault[i].value="0"
			}
	}
 } else {
		if (formobj.rdDefault.checked){
			formobj.hdDefault.value="1"
		} else {
			formobj.hdDefault.value="0"
		}	
 }//end if

 if (newRows==1) {	
	if (formobj.newRdDefault.value=="on") {
		formobj.newHdDefault.value="0";
		 formobj.newRdDefault.checked = false;
		
	}
 } else  {
	for (i=0;i<newRows;i++) {
	  if (formobj.newRdDefault[i].value=="on") {
		   formobj.newHdDefault[i].value="0";
		  formobj.newRdDefault[i].checked = false;
		
	  }
	}
 } 
 
}

function refreshPage(formobj)
{
	if(formobj.moreRespNo.value=='' ){
       alert("<%=MC.M_PlsEtr_NumResp%>")/*alert("Please enter the number for More Responses.")*****/
       formobj.moreRespNo.focus()
       return false;
	}			 			 

	 if(isNaN(formobj.moreRespNo.value) == true) {
	     alert("<%=MC.M_PlsValid_NumResp%>");/*alert("Please enter a valid number in Add More Responses.");*****/
		 formobj.moreRespNo.focus();
		 return false;
	}
	    
    if (confirm("<%=MC.M_Refreshing_RowsSvdata%>"))/*if (confirm("Refreshing the number of rows will save the data entered above. Do you wish to continue?"))*****/
	 {
	   formobj.refresh.value = "true"; 
	   if (validate(formobj)){
	   	  formobj.submit();
	   }
	} else {
		return false;
	}
}

function removeDefault(formobj,totRows)
{
 newRows  = formobj.defRespNo.value;
 var count=0;
if (formobj.mode.value=="M") {
	if (totRows > 1) { 
		for (i=0;i<totRows;i++)
			{
			if (formobj.rdDefault[i].checked) 
				{
					formobj.hdDefault[i].value="0"
					formobj.rdDefault[i].checked= false
					count++;
				}
			}
		}  

	else {
		if (formobj.rdDefault.checked){
			formobj.hdDefault.value="0"
			formobj.rdDefault.checked= false
			count++;
		  } 
		}
}
else if (formobj.mode.value=="N") {

if (newRows==1) {	
	if (formobj.newRdDefault.checked) {
	   formobj.newRdDefault.checked = false;
	   formobj.newHdDefault.value="0"
	   count++;
	}
 } else  {
	for (i=0;i<newRows;i++) {
	  if (formobj.newRdDefault[i].checked) {
		formobj.newRdDefault[i].checked = false;
		formobj.newHdDefault[i].value="0"
		count++;
	  }
	 }
 }		
}
	if(count==0)
	{
	alert("<%=MC.M_No_DefRespSelc%>");/*alert("No default response selected");*****/
	}
}

  
</SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="fieldRespB" scope="request" class="com.velos.eres.web.fldResp.FldRespJB"/>
<jsp:useBean id="fieldLibJB" scope="request"  class="com.velos.eres.web.fieldLib.FieldLibJB"/>
<jsp:useBean id="catLibJB" scope="request"  class="com.velos.eres.web.catLib.CatLibJB"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB"%>

<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1) 
   	ienet = 0; //IE
else
	ienet = 1;

if (ienet == 0) {
%>
<body style="overflow:scroll" onUnLoad="toRefresh(document.multipleBoxField);">
<%} else {%>
<body onUnLoad="toRefresh(document.multipleBoxField);">
<%}%>
<%--For Bug# 8205:Yogendra--%>
<jsp:include page="popupPanel.jsp" flush="true"/>
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div> 
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<DIV class="popDefault" id="div1"> 
 <%

	HttpSession tSession = request.getSession(true);
	int pageRight = 0;	
	
   if (sessionmaint.isValidSession(tSession))
   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/> 
	<jsp:include page="include.jsp" flush="true"/>

<%  GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MFLDLIB"));
	if (pageRight >=4)
	{
		
		//the number of defualt responses
		int fldRespCount = 0 ; 
	  Configuration conf = new Configuration();
	  fldRespCount = conf.getFldRespCount(conf.ERES_HOME +"eresearch.xml");
		
		String mode=request.getParameter("mode");
		String refresh=request.getParameter("refresh"); //indicates whether refresh has been clicked by the user
		if (refresh==null) refresh="false";
		int moreRespNo=EJBUtil.stringToNum(request.getParameter("moreRespNo")); //indicates the no of rows to be added				
		
		String fieldLibId="";
		String fldCategory="";
		String fldName="";
		String fldNameFormat="";
		String formatDisable="N";
		String fldExpLabel = "";
		String fldDesc = "";
		String fldUniqId="";
		String fldKeyword="";
		String fldInstructions="";
		String fldType="";
		String fldDataType="";
		String fldMoreColumns="";
		String fldColCount="";
		String fldCharsNo="";
		String sortOrder="";
		
		
		//anu
		int defVal = 0;
		int fldRespId=0;
		int totRows=0;
		int defRespNo=0;
		int seq = 0;
		String dispVal = "";
		String dataVal = "";
		String score = "";
		ArrayList arrRespId = new ArrayList();
		ArrayList arrSeq=new ArrayList();
		ArrayList arrScore=new ArrayList();
		ArrayList arrDispVal=new ArrayList();
		ArrayList arrDataVal=new ArrayList();
		ArrayList arrDefaultVal=new ArrayList();

		//anu
		String toRefresh = request.getParameter("toRefresh");
		if(toRefresh==null)
		{
			toRefresh = "";
		}

		String accountId=(String)tSession.getAttribute("accountId");
		int tempAccountId=EJBUtil.stringToNum(accountId);
		String catLibType="C" ;
		CatLibDao catLibDao= new CatLibDao();
		catLibDao= catLibJB.getAllCategories(tempAccountId,catLibType);
		ArrayList id= new ArrayList();
		ArrayList desc= new ArrayList();
		String pullDown;
		int rows;
		rows=catLibDao.getRows();
		
		id = catLibDao.getCatLibIds();
		desc= catLibDao.getCatLibNames();
		pullDown=EJBUtil.createPullDown("category", 0, id, desc);
		pullDown = pullDown.replaceFirst("WIDTH:177px","WIDTH:250px");
						
		
		if  ( (mode.equals("M") )  || (mode.equals("RO"))  )
		{ 
				fieldLibId=request.getParameter("fieldLibId");
				fieldLibJB.setFieldLibId(Integer.parseInt(fieldLibId));
				fieldLibJB.getFieldLibDetails();
				
				fldExpLabel = ((fieldLibJB.getExpLabel())== null)?"":(fieldLibJB.getExpLabel()) ; 		
				fldCategory=fieldLibJB.getCatLibId();
				pullDown=EJBUtil.createPullDown("category", EJBUtil.stringToNum(fldCategory), id, desc);
				pullDown = pullDown.replaceFirst("WIDTH:177px","WIDTH:250px");
				fldName=fieldLibJB.getFldName();
				fldNameFormat=fieldLibJB.getFldNameFormat();

				
				fldNameFormat=(fldNameFormat==null)?"":fldNameFormat;
				
				if (fldNameFormat.length()>0)
				{
				if (fldNameFormat.indexOf("[VELDISABLE]")>=0)
				formatDisable="Y";
				}
				
				fldDesc=fieldLibJB.getFldDesc();
				if(fldDesc == null)
					fldDesc="";
				fldUniqId=fieldLibJB.getFldUniqId();
				fldKeyword=fieldLibJB.getFldKeyword();
				if(fldKeyword == null)
					fldKeyword="";
				fldType=fieldLibJB.getFldType();
				fldDataType=fieldLibJB.getFldDataType();
				if (fldDataType == null)
					fldDataType = "";
				fldColCount=fieldLibJB.getFldColCount();
				if (fldColCount == null)
					fldColCount = "";
				fldInstructions=fieldLibJB.getFldInstructions();
				if(fldInstructions == null)
					fldInstructions = "";
					
				sortOrder=fieldLibJB.getFldSortOrder();
				sortOrder=(sortOrder==null)?"":sortOrder;

				
			//anu
			FldRespDao fldRespDao = fieldRespB.getResponsesForField(EJBUtil.stringToNum(fieldLibId));	
			arrRespId = fldRespDao.getArrFldRespIds();
			arrSeq=fldRespDao.getArrSequences();	
			arrScore=fldRespDao.getArrScores();	
			arrDispVal=fldRespDao.getArrDispVals();
			arrDataVal=fldRespDao.getArrDataVals();
			arrDefaultVal=fldRespDao.getArrDefaultValues();

			totRows=arrSeq.size();
	
			//anu	
				
	
		}
		
%>

	   <Form name="multipleBoxField" id="multBoxField"  method="post" action="multipleChoiceSubmit.jsp" onsubmit="if (validate(document.multipleBoxField)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	
	<Input type="hidden" name="mode" value=<%=mode%> >
	<Input type="hidden" name="fieldLibId" value=<%=fieldLibId%> >
	<Input type="hidden" name="refresh" value="false" >
	<Input type="hidden" name="fromRefresh" value=<%=refresh%> >
	<Input type="hidden" name="toRefresh" value=<%=toRefresh%>>
		  
	<table width="90%" cellspacing="1" cellpadding="2" border="0">
		<tr height="25"><td colspan="3"><P class="sectionHeadings"> <%=MC.M_FldType_MultiCbox_Upper%><%--FIELD TYPE:  MULTIPLE CHOICE BOX*****--%> </td></tr>
    <tr class="browserEvenRow"> 
      <td width="20%"><%=LC.L_Category%><%--Category*****--%> <FONT class="Mandatory">* </FONT></td>
        <td width="60%"><%=pullDown%> </td>
	  </tr>

  <tr class="browserEvenRow">
		<td width= "20%"> <%=LC.L_Fld_Name%><%--Field Name*****--%> <FONT class="Mandatory">* </FONT></td>
		<td width="30%">
		<!--Modified by Manimaran for #3941 -->
		<input type="hidden" name="nameta" size = 40 MAXLENGTH = 100 value="<%=fldNameFormat%>"> 
		<!-- Commented and Modified by Gopu to fix the bugzilla Issues #2595 -->
			<!--input type="text" name="name" size = 40 MAXLENGTH = 50 value="<%=fldName%>"--> 
			<input type="text" name="name" size = 60 MAXLENGTH = 1000 value="<%=fldName%>"> 
		<!--textarea id="name" name="name" rows="" cols="200" style="width:400;height:100;"><%=fldName%></textarea-->
		</td>
	<td bgcolor="#cccccc"><A href="javascript:void(0);" onClick="openPopupEditor('multipleBoxField','name','nameta','Velos',2000)">
	<!-- <img src="./images/format.gif" title="<%=LC.L_Format%>" alt="<%=LC.L_Format%> <%--Launch Editor*****--%>" border = 0 >--><%=LC.L_Format%></A>&nbsp;
	<% if (fldNameFormat.length()>0)
	{
	if (formatDisable.equals("N")){%>
	<DIV id="dformat"><A href="javascript:void(0);" onClick="processFormat(document.multipleBoxField,'D')"><img src="./images/disableformat.gif" alt="<%=LC.L_Disable_Formatting%><%--Disable Formatting*****--%>" border = 0 align=absbotton></A></DIV>
	<%} else if (formatDisable.equals("Y")){%>
	<BR><DIV id="dformat"><A href="javascript:void(0);" onClick="processFormat(document.multipleBoxField,'E')"><img src="./images/enableformat.gif" alt="<%=LC.L_Enable_Formatting%><%--Enable Formatting*****--%>" border = 0 ></A></DIV>
	<%}%>
	<A href="javascript:void(0);" onClick="processFormat(document.multipleBoxField,'R')"><img src="./images/removeformat.gif" alt="<%=LC.L_Rem_Format%><%--Remove Formatting*****--%>" border = 0 ></A>
	<%}%>		
	</td>
		</tr>

		<tr class="browserEvenRow">
		<td width="20%"> <%=LC.L_Fld_Id%><%--Field ID*****--%> <FONT class="Mandatory">* </FONT></td>
		<td width ="20%"><input type="text" name="uniqueId" size = 40 MAXLENGTH = 50 value="<%=fldUniqId%>"></td>
		</tr>
		<tr>

			<tr class="browserEvenRow"><td width="30%" >&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Multi_ChoiceType%><%--Multiple Choice Type*****--%> </td><td>
			<% if (fldDataType.equals("")) 
			{ %>
			<input type="radio" name="editBoxType" value="MD"  onclick=  " " CHECKED> <%=LC.L_Drop_Down%><%--Drop Down*****--%>
			<input type="radio" name="editBoxType" value="MC"  onclick=  ""> <%=LC.L_Checkbox%><%--Checkbox*****--%>
			<input type="radio" name="editBoxType" value="MR"  onclick=  " "> <%=LC.L_Radio_Button%><%--Radio Button*****--%>
			
			<% } %>
			<% if (fldDataType.equals("MD")) 
			{ %>
			<input type="radio" name="editBoxType" value="MD"  onclick=  " "CHECKED> <%=LC.L_Drop_Down%><%--Drop Down*****--%>
			<input type="radio" name="editBoxType" value="MC"  onclick=  " "> <%=LC.L_Checkbox%><%--Checkbox*****--%>
			<input type="radio" name="editBoxType" value="MR"  onclick=  ""> <%=LC.L_Radio_Button%><%--Radio Button*****--%>
			<% } %> 
			<% if (fldDataType.equals("MC")) 
			{ %>
			<input type="radio" name="editBoxType" value="MD"  onclick=  " "> <%=LC.L_Drop_Down%><%--Drop Down*****--%>
			<input type="radio" name="editBoxType" value="MC"  onclick=  " " CHECKED> <%=LC.L_Checkbox%><%--Checkbox*****--%>
			<input type="radio" name="editBoxType" value="MR"  onclick=  " "> <%=LC.L_Radio_Button%><%--Radio Button*****--%>
			<% } %>
			<% if (fldDataType.equals("MR")) 
			{ %>
			<input type="radio" name="editBoxType" value="MD"  onclick=  " "> <%=LC.L_Drop_Down%><%--Drop Down*****--%>
			<input type="radio" name="editBoxType" value="MC"  onclick=  " "> <%=LC.L_Checkbox%><%--Checkbox*****--%>
			<input type="radio" name="editBoxType" value="MR"  onclick=  " "CHECKED> <%=LC.L_Radio_Button%><%--Radio Button*****--%>
			<% } %>
		
			</tr>		

		<tr class="browserEvenRow">
		<td width ="20%">&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Fld_Label%><%--Field Label*****--%></td>
			  <%if(fldExpLabel.equals("1")){%>
		  <td width = "30%"><input type="checkBox" name="expLabel" value="<%=fldExpLabel%>" checked><%=LC.L_Expand_FldLabel%><%--Expand Field Label*****--%>
			  <%}else{%>
		  <td width = "30%"><input type="checkBox" name="expLabel" value="<%=fldExpLabel%>" ><%=LC.L_Expand_FldLabel%><%--Expand Field Label*****--%>
			  <%}%>
		</tr>



		<tr class="browserEvenRow">
		<td width= "20%"> &nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Fld_Desc%><%--Field Description*****--%></td>
		<td width="30%"><input type="text" name="desc" size = 60 MAXLENGTH = 500 value="<%=fldDesc%>"> </td>
		</tr>
		<tr class="browserEvenRow">		
		<td width="20%"> &nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Search_Keyword_S%><%--Search Keyword(s)*****--%> <img src="../images/jpg/help.jpg" onmouseover="return overlib('<%=MC.M_EtrOneOrMore_Kwrd%><%--Enter one or more keywords separated by a comma for use in querying.*****--%>',CAPTION,'<%=LC.L_Search_Keyword_S%><%--Search Keyword(s)*****--%>');" onmouseout="return nd();"></td>
		<td width ="20%"><input type="text" name="keyword" size = 60 MAXLENGTH = 50 value="<%=fldKeyword%>"></td>
		</tr>


	 	<tr class="browserEvenRow">
		 <td><label><%=MC.M_FldHelp_OnMouseOver%><%--Field Help (On Mouse Over)*****--%></label></td>
		 <td> <textarea name="instructions" cols="40" rows="2" ><%=fldInstructions%></textarea></td>
		</tr>
		<tr class="browserEvenRow">
			<td width="20%"> <%=MC.M_ChkBox_RadioBtn%><%--Check Box/Radio Button Display*****--%> </td>
			<% if (   mode.equals("N")  )  
				{ %>
				<td width= "20%"><input type="text" name="colcount" size =2 MAXLENGTH = 2 value="1"> <%=LC.L_Columns%><%--Columns*****--%></td>
			<%} else 
				{%>
				<td width= "20%"><input type="text" name="colcount" size =2 MAXLENGTH = 2 value="<%=fldColCount%>"> <%=LC.L_Columns%><%--Columns*****--%></td>
			<%}%>	
			
		</tr>
		</tr>
		</table>
	
		<table width="80%" cellspacing="0" cellpadding="0" border=" 0">
		<tr><br/></tr>
		<tr><td width="40%"><P class="sectionHeadingsFrm"> <%=LC.L_Multi_ChoiceResponses%><%--Multiple Choice: Responses*****--%></P></td>
			<td>
	 <%if (sortOrder.length()>0){%>
	  <input type="checkBox" name="sortOrder" value="<%=sortOrder%>" checked>
	  <%} else{%>
	  <input type="checkBox" name="sortOrder" value="A"> 
	  <%}%> <%=MC.M_AutoSeq_Alpha%><%--Auto Sequence (alphabetically)*****--%>
	 &nbsp;&nbsp;<A href=# onclick="removeDefault(document.multipleBoxField,<%=totRows%>)"><%=LC.L_Rem_Default%><%--Remove Default*****--%></A>
	 </td>
	 	 </tr>
	 	 <tr><td>&nbsp;</td></tr>

	</table>

	<!--anu-->
		
  <table width="80%" cellspacing="0" cellpadding="0">
  
  <tr>
    <th width="15%"><%=LC.L_Sequence%><%--Sequence*****--%> <FONT class="Mandatory">* </FONT></th>
	<th width="20%"><%=LC.L_Display_Val%><%--Display Value*****--%> <FONT class="Mandatory">* </FONT></th>
	<th width="20%"><%=LC.L_Data_Value%><%--Data Value*****--%></th>
	<th width="10%"><%=LC.L_Score%><%--Score*****--%></th>
	<th width="8%"><%=LC.L_Default%><%--Default*****--%></th>
	<th width="8%">  <% if (!mode.equals("RO") && ((pageRight == 5 && mode.equals("N")) || (pageRight == 6 && mode.equals("M")) || pageRight == 7))
	     {%>
	     <%=LC.L_Delete%>
	     <%}%></th>
  </tr>
  
 <%
  //modify mode 
 if (mode.equals("M"))
{
 for(int i=0;i<totRows;i++) 
  {
    fldRespId = ((Integer)arrRespId.get(i)).intValue();
	seq = ((Integer)arrSeq.get(i)).intValue();
	dispVal = (String)arrDispVal.get(i);
    dataVal = (String)arrDataVal.get(i);
	dataVal = (        dataVal      == null)?"":(     dataVal      ) ;
	
	score = (String)arrScore.get(i);	
	
	if (EJBUtil.isEmpty(score))
		score = "";
	defVal = ((Integer)arrDefaultVal.get(i)).intValue();
	
	 	 
 %>  	  	  	  
      <tr> 
        <td> 	 
		 <input name="fldRespId" type="hidden" value=<%=fldRespId%> >
  		 <input type="text" name="txtSeq" size = 5 MAXLENGTH = 10 value="<%=seq%>">
        </td>
    
	    <td> 
		<input type="text" name="txtDispVal" size = 20 MAXLENGTH = 100 value="<%=dispVal%>">
        </td>
	  
	     <td>
		 <input type="text" name="txtDataVal" size = 20 MAXLENGTH = 100 value="<%=dataVal%>">
        </td>
		
	   <td> 
		 <input type="text" name="txtScore" size = 10 MAXLENGTH = 20 value="<%=score%>">
        </td>
	  
	  <td> 
	  	 <% if (defVal==1) {%> 
		 	<input type="radio" name="rdDefault" size = 20 MAXLENGTH = 50 checked onclick="setHdDefVal(document.multipleBoxField,<%=totRows%>)" >			
		 <%} else {%>
 		 	<input type="radio" name="rdDefault" size = 20 MAXLENGTH = 50 onclick="setHdDefVal(document.multipleBoxField,<%=totRows%>)" >
		 <%}%>
		 
		 <input type="hidden" name="hdDefault" value=<%=defVal%>> 
	  </td>
	     
	  <td> 
	  <% if (!mode.equals("RO") && ((pageRight == 5 && mode.equals("N")) || (pageRight == 6 && mode.equals("M")) || pageRight == 7))
	     {%>
		    <A href="fldrespdelete.jsp?fieldRespId=<%=fldRespId%>&fieldLibId=<%=fieldLibId%>&calledFrom=L" onClick="return confirmBox('<%=StringUtil.escapeSpecialCharJS(dispVal)%>',<%=pageRight%>,<%=totRows%>)"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" alt="<%=LC.L_Delete%>" border="0" align="left"/></A>
		 <%}%> 	   
      </td>
		<%if(mode.equals("N")) {
			%>
		<input type="hidden" name="hdRecordType" value="N">
			
		<% } else if(mode.equals("M")) {
		    %>
		<input type="hidden" name="hdRecordType" value="M">
			
		<% } else if(mode.equals("D")) {
		   %>
		<input type="hidden" name="hdRecordType" value="D">
			 
    </tr>
    	
		
		<%
		} //for else if
			} //for loop
		%>

<%} //mode=M

	
	int seqVal = 0;
	if((mode.equals("M")) && (refresh.equals("true")))  { 
	FldRespDao fldRespDao = new FldRespDao();
	fldRespDao = fieldRespB.getMaxRespSeqForFldInLib(EJBUtil.stringToNum(fieldLibId));
	
	seqVal  = EJBUtil.stringToNum(fldRespDao.getMaxSeq());	
	}
 
  if ((mode.equals("N")) || (refresh.equals("true")))   
  {
	   int seqNum = 0;
  //new mode
 //defRespNo should come from XML
  defRespNo = fldRespCount;

  if (refresh.equals("true")) 
  {
   	 defRespNo = moreRespNo;  
  %>
  	<Input type="hidden" name="respFromRefresh" value="true" >
  <%
  }
  
 for(int i=0;i<defRespNo;i++) 
 {%>
    <tr> 
        <td> 
  		 <input type="text" name="newTxtSeq" size = 5 MAXLENGTH = 50 <%if((mode.equals("N")) && !(refresh.equals("true"))){%>value=<%=++seqNum%><%}else if((mode.equals("M")) && (refresh.equals("true"))){%>value=<%=++seqVal%><%}%>>
        </td>
    
	    <td> 
		<input type="text" name="newTxtDispVal" size = 20 MAXLENGTH = 100 value="">
        </td>
	  
	     <td>
		 <input type="text" name="newTxtDataVal" size = 20 MAXLENGTH = 100 value="">
        </td>
		
	   <td> 
		 <input type="text" name="newTxtScore" size = 10 MAXLENGTH = 50 value="0"></input>
        </td>
	  
	  <td>
	     <%if ((i==0)&& (mode.equals("N"))) {%> 
		 <input type="radio" name="newRdDefault"  size = 20 onclick="setDefVal(document.multipleBoxField,<%=defRespNo%>)">
		 <%}else {%>
		 <input type="radio" name="newRdDefault"  size = 20 onclick="setDefVal(document.multipleBoxField,<%=defRespNo%>)">
		 <%}%>		 
		 <input type="hidden" name="newHdDefault" value="0"> 	   
     </td>
 		 <input type="hidden" name="newHdRecordType" value="0"> 	   
	  <td> &nbsp;
     </td>
    </tr>
 
 
<% } //loop for despRespNo 
%> 
<% 
 } //mode=N
%>

  </table>
 <input type="hidden" name="totRows" value=<%=totRows%>>		 
 <input type="hidden" name="defRespNo" value=<%=defRespNo%>>

<table width="80%">
		<td width="28%"><P class="defComments"><%=MC.M_Add_MoreRespRows%><%--Add More Response Rows*****--%></P></td>
		 <td width="10%"><input type="text" name="moreRespNo" size =3 MAXLENGTH = 3 value="<%=fldMoreColumns%>"></td>
		 <td width="60%"><% String Hyper_Link = "<A href=# onclick=\"refreshPage(document.multipleBoxField)\">"+LC.L_Refresh+"</A>"; Object[] arguments = {Hyper_Link}; %><%=VelosResourceBundle.getMessageString("M_EtrEsignClk_Refresh",arguments) %>
          <%--Enter the desired number of new rows in the box to the left, your e-Signature below and then click <A href=# onclick="refreshPage(document.multipleBoxField)">Refresh</A>*****--%></td>
		 </table>

	<!--anu-->
	
	<br>
	<% if (!mode.equals("RO") && ((pageRight == 5 && mode.equals("N")) || (pageRight == 6 && mode.equals("M")) || pageRight == 7))
 	{   %>
		<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="multBoxField"/>
		<jsp:param name="showDiscard" value="N"/>
		</jsp:include>
	<%}%>
     <br>

 	 <table width="100%" cellspacing="0" cellpadding="0">
      <td align=right> 
	<!--	<input type="image" src="../images/jpg/Submit.gif" align="absmiddle" border="0" >-->
      </td> 
      </tr>
  </table>
  </Form>
  

<%				
			
	} //end of if body for page right
	else
	{
%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
<%

	} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true" />
<%

}

%>
 <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</body>

</html>


		
	
		
		
		
			
		
		
		
	




