<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<jsp:include page="localization.jsp" flush="true"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:include page="skinChoser.jsp" flush="true"/>
<jsp:useBean id="repB" scope="request" class="com.velos.eres.web.report.ReportJBNew"/>
<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id="codeLst" scope="session" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />

<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,javax.xml.transform.TransformerFactory,javax.xml.transform.Transformer,javax.xml.transform.Source,javax.xml.transform.stream.StreamSource,javax.xml.transform.stream.StreamResult,javax.xml.transform.TransformerException,javax.xml.transform.TransformerConfigurationException,java.io.*,java.text.*,java.net.URL,com.velos.eres.web.user.UserJB,com.velos.eres.web.grpRights.*"%>
<%  if (!"db".equals(request.getParameter("includedIn"))) {  // db = dashboard %>
<script>
window.name="reportDisplay";
</script>
<%  }  %>
<%

	HttpSession tSession = request.getSession(true); 
	
	String accName = (String) tSession.getValue("accName");
	accName=(accName==null)?"default":accName;
	String accSkinId = ""; 
	String usersSkinId = "";
	String userSkin = "";
	String accSkin = "";
	String skin = "default";
	accSkinId = (String) tSession.getValue("accSkin");
	UserJB userB1 = (UserJB) tSession.getValue("currentUser");      
	usersSkinId = userB1.getUserSkin();
	userSkin= codeLst.getCodeCustomCol(EJBUtil.stringToNum(usersSkinId) );
	accSkin= codeLst.getCodeCustomCol(EJBUtil.stringToNum(accSkinId) );
	if (accSkin == null || accSkin.equals("") || accSkin.equals("default") ){
		accSkin = "accSkin";
	}
	else{
		skin = accSkin;
	}
	if (userSkin != null && !userSkin.equals("") ){
		skin = userSkin;
	}
	%>
	
	<script>
	whichcss_skin("<%=skin%>");
	if (navigator.userAgent.indexOf("MSIE") > -1) {
		document.write("<style>html, body { overflow: auto; }</style>");
	} else {
		document.write("<style>html, body { overflow: visible; }</style>");
	}
	</script>
<%


	String repDate="";
	String repTitle="";

	String repName=request.getParameter("repName");
	String dtFrom=request.getParameter("dateFrom");
	String dtTo=request.getParameter("dateTo");
	String selSiteIds = request.getParameter("selSiteIds");
	String budgetPk = request.getParameter("budgetPk");
	int repId = EJBUtil.stringToNum(request.getParameter("repId")); //JM: 17Mar08, moved up here from below

	/*Custom label processing*/
	int startIndx =0;
	int stopIndx =0;
	String labelKeyword ="";
	String labelString ="";
	String messageKeyword ="";
	String messageString ="";
	String interimXSL = "";
	String messageKey ="";
	String messageParaKeyword []=null;
	//JM: 17Mar08
	String itrValueFltr="";
	String filterStrActual="";
	String strProvider ="";
	String tobeReplaced ="";
	ArrayList tmpStrNames =new ArrayList();
	ArrayList tmpStrValues =new ArrayList();



	ArrayList filterKey=new ArrayList();
	ArrayList filterValue=new ArrayList();
	ArrayList selFilter=new ArrayList();
	ArrayList selFilterValue=new ArrayList();
	//get all the filters defined on previous page, All the filter
	//defined start with 'param',go through the parameter list,get the vales
	//for all the attributes that start with param and dump in an array
	java.util.Map filterMap=request.getParameterMap();
	Collection abc=filterMap.keySet();

	Iterator itr=abc.iterator();
	String itrKey="";
	String itrValue="";
	String filterStr="",tempStr="";

	while (itr.hasNext())
	{
	     itrKey=(String)itr.next();

	     itrValue=request.getParameter(itrKey);
	     itrValue=(itrValue==null)?"":itrValue;


//JM: 17Mar2008
		if (itrKey.equals("keyFilters"+repId)){
			if (itrKey.startsWith("keyFilters")) {
			 	itrValueFltr=itrValue.replace(":" , ",");
			}
		}


		if(itrKey.equals("paramorgId") && (repId==128 || repId==130 || repId==136 || repId==137 || repId==138 || repId==142 || repId==143 || repId==145 || repId==154 || repId==155)){
    		itrValue=(itrValue.equals(""))?"-1":itrValue;
    	}
	     
	    if (itrKey.startsWith("param"))
	    {
	        itrKey=itrKey.replace("param","");
		    filterKey.add(itrKey);
		    filterValue.add(itrValue);
		}

	    if (itrKey.startsWith("sel"))
	    {
	        tempStr=itrKey.replace("sel","");
	        selFilter.add(itrKey.replace("sel",""));
	        tempStr=request.getParameter("key"+itrKey.replace("sel",""));
	        tempStr=(tempStr==null)?"":tempStr;

			//JM: 17Mar2008
			tmpStrNames.add(tempStr);
			tmpStrValues.add(itrValue);

	       if (tempStr.length()>0){
			if (filterStr.length()==0)
		        filterStr="["+tempStr+":"+itrValue+"]";
			else
			    filterStr=filterStr+" ["+tempStr+":"+itrValue+"]";
	   //     selFilterValue.add(filterStr);
		}
	    }



} //end of while



//JM: 17Mar2008

	    for (int j=0; j < selFilter.size() ; j++){
			strProvider = selFilter.get(j).toString();

			if ((itrValueFltr.indexOf(strProvider)>=0)){

				if (filterStr.length()==0)
		        	filterStrActual="["+tmpStrNames.get(j).toString()+":"+tmpStrValues.get(j).toString()+"]";
				else
			    	filterStrActual=filterStrActual+"["+tmpStrNames.get(j).toString()+":"+tmpStrValues.get(j).toString()+"]";

     		}
	  }//end of for looop


	filterStr=StringUtil.replace(filterStrActual,"[ALL]",LC.L_All.toUpperCase()/*"ALL"*****/);
	//System.out.println("Final Filter set" + filterKey + " Final Filter Values " + filterValue + "\n Selected fil;ter values"+selFilter + "with values"+selFilterValue+"\n FilterStr"+filterStr);

	//end getting values

	String repArgs = "";
	String repArgsDisplay = "";
	String monthString = null;
	String filterType = request.getParameter("filterType");

	String month = request.getParameter("month");
	String year = null;
	String format = "";

	if(filterType == null) filterType = "";

	if(filterType.equals("2")) { //Year
		year = request.getParameter("year");
		if (!(year.equals(""))) {
 			
			dtFrom = DateUtil.getFormattedDateString(year,"01","01");
			dtTo = DateUtil.getFormattedDateString(year,"31","12");
		}
	}

	if(filterType.equals("3")) { //Month
		year = request.getParameter("year1");
		
		if (!(year.equals(""))) {
 			dtFrom = DateUtil.getFormattedDateString(year,"01",month);
			 
		}
 		
		dtTo = DateUtil.getFormattedDateString(year,String.valueOf(DateUtil.getMonthMaxNumberOfDays(month,year)),month);
	}

	if(filterType.equals("1")) { //All Dates
 		
		dtFrom = DateUtil.getFromDateStringForNullDate();
		dtTo = DateUtil.getToDateStringForNullDate();
	}

	if(dtFrom == null) dtFrom = "";
	dtFrom = dtFrom.trim();
	if (!(dtFrom.equals(""))) {
         if(dtTo.equals("")){
        	 dtTo=DateUtil.getCurrentDate() ;
         }
		if(filterType.equals("1")) { //All Dates
			repArgsDisplay = LC.L_DtRange_All/*"Date Range:ALL"*****/;
		}
	
		else {
			repArgsDisplay = LC.L_Date_Range/*"Date Range*****/+":"+dtFrom + " "+LC.L_To/*To*****/+" " + dtTo ;
		}

		repArgs= dtFrom + ":" + dtTo;
		System.out.println("repArgs-"+repArgs);

		filterKey.add("fromDate");
		filterValue.add(dtFrom);
		filterKey.add("toDate");
		filterValue.add(dtTo);
	}
	else if((dtFrom.equals("")) && !(dtTo.equals(""))){
		repArgsDisplay = LC.L_To/*To*****/+" " + dtTo ;
		filterKey.add("fromDate");
		dtFrom=DateUtil.getFromDateStringForNullDate();
		filterValue.add(dtFrom);
		filterKey.add("toDate");
		filterValue.add(dtTo);
	} 



//JM: blocked
	//int repId = EJBUtil.stringToNum(request.getParameter("repId"));


//JM: 12Mar2008
	String filterChk = request.getParameter("fltrChk");
	filterChk = (filterChk==null)?"":filterChk;

	String dwonLoadChk = request.getParameter("dnldChk");
	dwonLoadChk = (dwonLoadChk==null)?"":dwonLoadChk;



	int repXslId = 0;

	String dispStyle=request.getParameter("dispStyle");
	String studyPk=request.getParameter("studyPk");
	String protId=request.getParameter("protId");
%>
<%
	String patid=request.getParameter("id");


	String params="";

	String hdr_file_name="";
	String ftr_file_name="";
	String filePath="";
	String hdrfile="";
	String ftrfile="";
	String hdrFilePath="";
	String ftrFilePath="";
	String fileName="";
	String xml=null;
	String calledfrom = request.getParameter("calledfrom");
   if (sessionmaint.isValidSession(tSession))
   {
	try{	   
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>

<%
	String uName =(String) tSession.getValue("userName");
	int acc = EJBUtil.stringToNum((String) (tSession.getValue("accountId")));
	String accountId= (String) (tSession.getValue("accountId"));
	int userId = EJBUtil.stringToNum((String) (tSession.getValue("userId")));
	userB = (UserJB)tSession.getValue("currentUser");
	String FilledFormId = "";
	
	int pageRight=-1,formRights=0;
	
	if(repId == 156){
	TeamDao teamDao = new TeamDao();
    teamDao.getTeamRights(EJBUtil.stringToNum(studyPk),userId);
    ArrayList tId = teamDao.getTeamIds();
    Hashtable htMoreParams = new Hashtable();
    String ignoreRights = "";
    ignoreRights = (String) tSession.getValue("pp_ignoreAllRights") ;
    String roleCodePk = "";
    
    if (tId.size() == 0) 
	{ 
    	pageRight=0 ;
    }
	else 
	{
		ArrayList arRoles = new ArrayList();
		arRoles = teamDao.getTeamRoleIds();		
		
		if (arRoles != null && arRoles.size() >0 )	
		{
			roleCodePk = (String) arRoles.get(0);
			
			if (StringUtil.isEmpty(roleCodePk))
			{
				roleCodePk="";
			}
		}	
		else
		{
			roleCodePk ="";
		}
		
		htMoreParams.put("teamRolePK",roleCodePk);
		
		stdRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));
		ArrayList teamRights ;
		teamRights  = new ArrayList();
		teamRights = teamDao.getTeamRights();
			 
		stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
		stdRights.loadStudyRights();
	 
	 
    	if ((stdRights.getFtrRights().size()) == 0)
		{
    	 	pageRight= 0;
    	 	formRights=0;
    	}
		else
		{
    		pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMACC"));
    		formRights = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMMANAG"));
    	}
    }
    /// entered for having rights for study linked forms 
	
    if (StringUtil.isEmpty(ignoreRights))
	{
		ignoreRights = "false";
	}
	
	if (ignoreRights.equals("true"))
	{
		pageRight=7;
		formRights=7;   
	}
	}
	
	if(pageRight==-1 || pageRight>= 4){			

	
	GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");
    pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("REPORTS"));

    
	
	Calendar now = Calendar.getInstance();
	repDate = DateUtil.getCurrentDateTime();

	ReportDaoNew rD =new ReportDaoNew();

	//The parameters are separated by :
	//They are segregated in the stored procedure SP_GENXML and then passed to report sql
//System.out.println("**************" + params);
	rD.setFilterKey(filterKey.toArray());
	rD.setFilterValue(filterValue.toArray());
	repXslId=repId;
	int success=rD.createReport(repId,acc);

	ArrayList repXml = rD.getRepXml();
	
	
	if (repXml == null || repXml.size() == 0) { Object[] arguments = {repName}; //no data found
%>
		<br><br><br><br><br><br><br><br><br>
		<P class="sectionHeadings">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=VelosResourceBundle.getMessageString("M_NoDataFnd_Rpt",arguments)%><%--No data found for the Report '<%=repName%>'.*****--%></P>
<%		throw new Exception(VelosResourceBundle.getMessageString("M_NoDataFnd_Rpt",arguments));
	}



	//rD=repB.getRepXsl(repXslId);
	repTitle=repName;
	rD.getReportXsl(repXslId);
	ArrayList repXsl = rD.getXsls();
	System.out.println("repXsl:;;;"+repXsl);
	Object temp;
	
	String xsl=null;


	temp=repXml.get(0);

	if (!(temp == null))
	{
		xml=temp.toString();
		xml = StringUtil.replace(xml,"&amp;#","&#");
			
	}
	else
	{
		out.println(MC.M_ErrIn_GetXml/*"Error in getting XML"*****/);
		Rlog.fatal("Report","ERROR in getting XML");
		throw new Exception(MC.M_ErrIn_GetXml);
	}
	
    try{
		temp=repXsl.get(0);
	} catch (Exception e) {
		out.println(MC.M_ErrIn_GetXsl/*"Error in getting XSL."*****/);
		e.printStackTrace();
		Rlog.fatal("Report","ERROR in getting XSL. Record for report " +repId +"does not exist in database.");
		throw new Exception(MC.M_ErrIn_GetXsl);
	}


	int lenStr=0, lenTotal=0;
	int indexStr = 0, subIndex1 = 0, subIndex2 = 0;
	String xslStr1="", xslStr2="", tmpStr= "";


	if (!(temp == null))	{
		xsl=temp.toString();

		xsl = StringUtil.replace(xsl,"[VELFLOATFORMATDEF]",NumberUtil.getXSLFloatFormatDef());
		xsl = StringUtil.replace(xsl,"[VELNUMFORMATDEF]",NumberUtil.getXSLNumFormatDef());
	
		//JM: 12Mar2008 added the below line..
		if(filterChk.equals("1")){
		indexStr = xsl.indexOf("<xsl:value-of select=\"$argsStr\"");
		 if (indexStr!=-1){ //report: 15, 8
			xslStr1 = xsl.substring(0, indexStr);
			subIndex1 = (xslStr1.toLowerCase()).lastIndexOf("<tr>");
			xslStr1 = xslStr1.substring(0, subIndex1);

			xslStr2 = xsl.substring(indexStr);
			tmpStr = "</tr>";
			lenStr = tmpStr.length();
			subIndex2 =(xslStr2.toLowerCase()).indexOf(tmpStr);
			lenTotal = (subIndex2 + lenStr);
			xslStr2 = xslStr2.substring(lenTotal);
			xsl = xslStr1+xslStr2;
		 }
		}

		if(dwonLoadChk.equals("1")){
		//Modified For Bug# 8599 | Date: 20th Feb 2012 | By:YPS			
		//indexStr = xsl.indexOf(MC.M_Download_ReportIn/*"Download the report in*****/+":");
		indexStr = xsl.indexOf("VELMESSGE[M_Download_ReportIn]");
		 if (indexStr!=-1){
			xslStr1 = xsl.substring(0, indexStr);
			subIndex1 = (xslStr1.toLowerCase()).lastIndexOf("<table");
			xslStr1 = xslStr1.substring(0, subIndex1);

			xslStr2 = xsl.substring(indexStr);
			tmpStr = "</table>";
			lenStr = tmpStr.length();
			subIndex2 = (xslStr2.toLowerCase()).indexOf(tmpStr);
			lenTotal = (subIndex2 + lenStr);
			xslStr2 = xslStr2.substring(lenTotal);
			xsl = xslStr1+xslStr2;
		 }
		}


	}
	else
	{
		out.println(MC.M_ErrIn_GetXsl/*"Error in getting XSL."*****/);
		Rlog.fatal("Report","ERROR in getting XSL. Record for report " +repId +"does not exist in database.");
		throw new Exception(MC.M_ErrIn_GetXsl);
	}

	if ((xsl.length())==0) {
		out.println(MC.M_ErrIn_GetXsl/*"Error in getting XSL."*****/);
		Rlog.fatal("Report","ERROR in getting XSL. Empty XSL found in database for report " +repId);
		throw new Exception(MC.M_ErrIn_GetXsl);
	}

//out.println(protId + "<br>");
//out.println(patid + "<br>");
//out.println(studyPk + "<br>");

//out.println(repArgs + "<br>");
//out.println("params " + params + " repId " + repId);

	if ((xml.length())==34) {
		Object[] arguments = {repName}; //no data found
%>

		<br><br><br><br><br><br><br><br><br>
		<P class="sectionHeadings">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=VelosResourceBundle.getMessageString("M_NoDataFnd_Rpt",arguments)%><%--No data found for the Report '<%=repName%>'.*****--%></P>
<%		throw new Exception(VelosResourceBundle.getMessageString("M_NoDataFnd_Rpt",arguments));
	}


	//get hdr and ftr
	//Changed By Deepali
	rD=repB.getRepHdFtr(repId,acc,userId);

	byte[] hdrByteArray=rD.getHdrFile();
	byte[] ftrByteArray=rD.getFtrFile();
	String hdrflag, ftrflag;
	hdr_file_name="temph["+ now.get(now.DAY_OF_MONTH) + now.get(Calendar.MONTH) + (now.get(now.YEAR) - 1900)+ now.get(now.HOUR_OF_DAY)+ now.get(now.MINUTE)+ now.get(now.SECOND) + "].jpg";
	Configuration.readReportParam(Configuration.ERES_HOME + "eresearch.xml");
	filePath = Configuration.REPTEMPFILEPATH;

	hdrfile=filePath+ "/" + hdr_file_name;
	ftr_file_name="tempf["+ now.get(now.DAY_OF_MONTH) + now.get(Calendar.MONTH) + (now.get(now.YEAR) - 1900)+ now.get(now.HOUR_OF_DAY)+ now.get(now.MINUTE)+ now.get(now.SECOND) + "].jpg";
	ftrfile=filePath+ "/" + ftr_file_name;

	hdrFilePath="../temp/"+hdr_file_name;
	ftrFilePath="../temp/"+ftr_file_name;
//out.println(params);
	//check for byte array
	if (!(hdrByteArray ==null))
	{
		hdrflag="1";
		ByteArrayInputStream fin=new ByteArrayInputStream(hdrByteArray);

		BufferedInputStream fbin=new BufferedInputStream(fin);

		File fo=new File(hdrfile);

		FileOutputStream fout = new FileOutputStream(fo);
		Rlog.debug("3","after output stream");
		int c ;
		while ((c = fbin.read()) != -1){
				fout.write(c);
			}
		fbin.close();
		fout.close();
		}
	else
	{
		hdrflag="0";
	}


		//check for length of byte array
	if (!(ftrByteArray ==null))
		{
		ftrflag="1";
		ByteArrayInputStream fin1=new ByteArrayInputStream(ftrByteArray);
		BufferedInputStream fbin1=new BufferedInputStream(fin1);
		File fo1=new File(ftrfile);
		FileOutputStream fout1 = new FileOutputStream(fo1);
		int c1 ;
		while ((c1 = fbin1.read()) != -1){

			fout1.write(c1);
		}

		fbin1.close();
		fout1.close();
	}
	else
	{
		ftrflag="0";
	}


	//get the folder name
	Configuration.readAppendixParam(Configuration.ERES_HOME + "eresearch.xml");
	com.aithent.file.uploadDownload.Configuration.readSettings("eres");
	com.aithent.file.uploadDownload.Configuration.readUploadDownloadParam(com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD + "fileUploadDownload.xml", "study");
	String fileDnPath = com.aithent.file.uploadDownload.Configuration.DOWNLOADSERVLET ;


	Configuration.readReportParam(Configuration.ERES_HOME + "eresearch.xml");
	filePath = Configuration.REPDWNLDPATH;
	Rlog.debug("report", filePath);
	//make the file name
	fileName="reporthtml"+"["+System.currentTimeMillis()+ "].html" ;
	//make the complete file name
	String htmlFile = filePath + "/"+fileName;
	response.setContentType("text/html");

	interimXSL = xsl;
	
	try{
		while(interimXSL.contains("VELLABEL[")){
			startIndx = interimXSL.indexOf("VELLABEL[") + "VELLABEL[".length();
			stopIndx = interimXSL.indexOf("]",startIndx);
			//KM-14Oct10-Localization of Labels
			labelKeyword = interimXSL.substring(startIndx, stopIndx);
			labelString = LC.getLabelByKey(labelKeyword);
			interimXSL=StringUtil.replaceAll(interimXSL, "VELLABEL["+labelKeyword+"]", labelString);
		}
		
		while(interimXSL.contains("VELMESSGE[")){
			startIndx = interimXSL.indexOf("VELMESSGE[") + "VELMESSGE[".length();
			stopIndx = interimXSL.indexOf("]",startIndx);
			messageKeyword = interimXSL.substring(startIndx, stopIndx);
			messageString = MC.getMessageByKey(messageKeyword);
			interimXSL=StringUtil.replaceAll(interimXSL, "VELMESSGE["+messageKeyword+"]", messageString);
		}
		while(interimXSL.contains("VELPARAMESSGE[")){
			startIndx = interimXSL.indexOf("VELPARAMESSGE[") + "VELPARAMESSGE[".length();
			stopIndx = interimXSL.indexOf("]",startIndx);
			messageKeyword = interimXSL.substring(startIndx, stopIndx);
			messageKey=messageKeyword.substring(0, messageKeyword.indexOf("||"));
			messageKey=messageKey.trim();
			messageParaKeyword=messageKeyword.substring(messageKeyword.indexOf("||")+2).split(java.util.regex.Pattern.quote("||"));
			messageString = VelosResourceBundle.getMessageString(messageKey,messageParaKeyword);
			interimXSL=StringUtil.replaceAll(interimXSL, "VELPARAMESSGE["+messageKeyword+"]", messageString);
		}
		if(!interimXSL.contains("word.GIF")){
			if(interimXSL.contains(LC.L_Word_Format)){
				tobeReplaced = "<img border=\"0\" title=\""+LC.L_Word_Format+"\" alt=\""+LC.L_Word_Format+"\" src=\"./images/word.GIF\"></img>";
				interimXSL=StringUtil.replace(interimXSL, LC.L_Word_Format, tobeReplaced);
			}
			if(interimXSL.contains(LC.L_Excel_Format)){
				tobeReplaced = "<img border=\"0\" title=\""+LC.L_Excel_Format+"\" alt=\""+LC.L_Excel_Format+"\" src=\"./images/excel.GIF\"></img>";
				interimXSL=StringUtil.replace(interimXSL,LC.L_Excel_Format, tobeReplaced);
			}
			if(interimXSL.contains(LC.L_Printer_FriendlyFormat)){
				tobeReplaced = "<img border=\"0\" src=\"./images/printer.gif\" title=\""+LC.L_Printer_FriendlyFormat+"\" alt=\""+LC.L_Printer_FriendlyFormat+"\"></img>";
				interimXSL=StringUtil.replace(interimXSL, LC.L_Printer_FriendlyFormat, tobeReplaced);
			}
		}
		xsl = interimXSL;
	}catch (Exception e)
	{
  		out.write("Error in replacing label string: " + e.getMessage());
 	}

	 try
    {

		//first save the output in html file
		TransformerFactory tFactory1 = TransformerFactory.newInstance();
		Reader mR1=new StringReader(xml);
		Reader sR1=new StringReader(xsl);
		Source xmlSource1=new StreamSource(mR1);
		Source xslSource1 = new StreamSource(sR1);
		TimeZone tz = TimeZone.getDefault();


 		Transformer transformer1 = tFactory1.newTransformer(xslSource1);
		//Set the params
		transformer1.setParameter("hdrFileName", hdrFilePath);
		transformer1.setParameter("ftrFileName", ftrFilePath);
		transformer1.setParameter("repTitle",repTitle);
		transformer1.setParameter("repName",repName);
		transformer1.setParameter("repBy",uName);
		transformer1.setParameter("repDate",repDate);
		////KM-Filters displayed in the report.
		while(filterStr.contains("VELLABEL[")){
			startIndx = filterStr.indexOf("VELLABEL[") + "VELLABEL[".length();
			stopIndx = filterStr.indexOf("]",startIndx);
			labelKeyword = filterStr.substring(startIndx, stopIndx);
			labelString = LC.getLabelByKey(labelKeyword);
			filterStr=StringUtil.replaceAll(filterStr, "VELLABEL["+labelKeyword+"]", labelString);
		}
		while(filterStr.contains("VELMESSGE[")){
			startIndx = filterStr.indexOf("VELMESSGE[") + "VELMESSGE[".length();
			stopIndx = filterStr.indexOf("]",startIndx);
			messageKeyword = filterStr.substring(startIndx, stopIndx);
			messageString = MC.getMessageByKey(messageKeyword);
			filterStr=StringUtil.replaceAll(filterStr, "VELMESSGE["+messageKeyword+"]", messageString);
		}
		while(filterStr.contains("VELPARAMESSGE[")){
			startIndx = filterStr.indexOf("VELPARAMESSGE[") + "VELPARAMESSGE[".length();
			stopIndx = filterStr.indexOf("]",startIndx);
			messageKeyword = filterStr.substring(startIndx, stopIndx);
			messageKey=messageKeyword.substring(0, messageKeyword.indexOf("||"));
			messageKey=messageKey.trim();
			messageParaKeyword=messageKeyword.substring(messageKeyword.indexOf("||")+2).split(java.util.regex.Pattern.quote("||"));
			messageString = VelosResourceBundle.getMessageString(messageKey,messageParaKeyword);
			filterStr=StringUtil.replaceAll(filterStr, "VELPARAMESSGE["+messageKeyword+"]", messageString);
		}
		if(!filterStr.contains("word.GIF")){
			if(filterStr.contains(LC.L_Word_Format)){
				tobeReplaced = "<img border=\"0\" title=\""+LC.L_Word_Format+"\" alt=\""+LC.L_Word_Format+"\" src=\"./images/word.GIF\"></img>";
				filterStr=StringUtil.replace(filterStr, LC.L_Word_Format, tobeReplaced);
			}
			if(filterStr.contains(LC.L_Excel_Format)){
				tobeReplaced = "<img border=\"0\" title=\""+LC.L_Excel_Format+"\" alt=\""+LC.L_Excel_Format+"\" src=\"./images/excel.GIF\"></img>";
				filterStr=StringUtil.replace(filterStr,LC.L_Excel_Format, tobeReplaced);
			}
			if(filterStr.contains(LC.L_Printer_FriendlyFormat)){
				tobeReplaced = "<img border=\"0\" src=\"./images/printer.gif\" title=\""+LC.L_Printer_FriendlyFormat+"\" alt=\""+LC.L_Printer_FriendlyFormat+"\"></img>";
				filterStr=StringUtil.replace(filterStr, LC.L_Printer_FriendlyFormat, tobeReplaced);
			}
		}
		transformer1.setParameter("argsStr","["+repArgsDisplay+"] "+ StringUtil.replace(filterStr,"&amp;#","&#").replace(",",",\n"));//To Fix Bug# 9895
		transformer1.setParameter("cond","F");
		transformer1.setParameter("tz", tz.getDisplayName());
		transformer1.setParameter("wd","");
		transformer1.setParameter("xd","");
		transformer1.setParameter("pd","");
		transformer1.setParameter("hd", "");
		transformer1.setParameter("hdrflag", hdrflag);
		transformer1.setParameter("ftrflag", ftrflag);
		transformer1.setParameter("dl", "");
		transformer1.setParameter("numFormat",NumberUtil.getXSLNumFormat());
		transformer1.setParameter("floatFormat",NumberUtil.getXSLFloatFormat());
		transformer1.setParameter("numFormatLocale",Configuration.getNumFormatLocale());
		transformer1.setOutputProperty("encoding", "UTF-8");



		// Perform the transformation, sending the output to html file
/*SV, 8/26, transformer didn't seem to close the stream properly that, next jsp (repGetExcel.jsp) kept getting 0(zero) bytes for file length,
	even though the file existed and was openable outside the application. So, fix is to, open and close file outside of transformer.

	  	transformer1.transform(xmlSource1, new StreamResult(htmlFile));
*/
  	
		FileOutputStream fhtml=new FileOutputStream(htmlFile);
		transformer1.transform(xmlSource1, new StreamResult(fhtml));
		fhtml.close();
		
	  	try
		{	
			File f=new File(htmlFile);
			int len=(int)f.length();

			//out.println(len);

			FileInputStream fIn=new FileInputStream (f);
			byte[] b=new byte[(len+2)];
			int i =fIn.read(b, 0, len);
		//	System.out.println(i);
			fIn.close();
			String fileStr=new String(b);
			fileStr=fileStr.trim();

			StringBuffer fileStrBuff=new StringBuffer(fileStr);

			int pos2=-1;
			int pos1=-1;

			while(fileStr.indexOf("<A") > 0){
				pos2=-1;
				pos1 = fileStr.indexOf("<A");
				if (pos1>=0) 
					 pos2 = fileStr.indexOf(">",pos1);
				if ((pos1>=0) && (pos2>=0))
					fileStrBuff.replace(pos1, pos2+1, "");
				fileStr=fileStrBuff.toString();
				
				pos2=-1;
				pos1 = fileStr.indexOf("</A");
				if (pos1>=0) 
					 pos2 = fileStr.indexOf(">",pos1);
				if ((pos1>=0) && (pos2>=0))
					fileStrBuff.replace(pos1, pos2+1, "");
				fileStr=fileStrBuff.toString();
			}
			
			b=fileStr.getBytes();
			len=b.length;
			
			fhtml=new FileOutputStream(htmlFile);
			fhtml.write(b,0,len);
			fhtml.close();
		}
		catch (Exception e)
		{
			System.out.println("Error in creating HTML file"+e+e.getMessage());
		}
	  	
		//now send it to console
	  	TransformerFactory tFactory = TransformerFactory.newInstance();
		Reader mR=new StringReader(xml);
		Reader sR=new StringReader(xsl);
		Source xmlSource=new StreamSource(mR);
		Source xslSource = new StreamSource(sR);
		// Generate the transformer.
			Transformer transformer = tFactory.newTransformer(xslSource);
		/*Changes for: INF-22402 Date:10/04/2012 By:Yogendra Pratap Singh*/
		String wordLink = "repGetWord.jsp?htmlFile=" + htmlFile +"&fileDnPath="+fileDnPath+"&filePath="+filePath+"&repId="+repId+"&calledfrom="+calledfrom;
		String excelLink = "repGetExcel.jsp?htmlFile=" + htmlFile +"&fileDnPath="+fileDnPath+"&filePath="+filePath+"&repId="+repId+"&calledfrom="+calledfrom;
		String pdfLink = "repGetPdf.jsp?htmlFile=" + htmlFile +"&fileDnPath="+fileDnPath+"&filePath="+filePath+"&repId="+repId+"&calledfrom="+calledfrom;
		String printLink = "repGetHtml.jsp?htmlFileName=" + fileName +"&fileDnPath="+fileDnPath+"&filePath="+filePath+"&repId="+repId+"&calledfrom="+calledfrom;
		StringBuffer filterBuf = new StringBuffer("|");
		for(int iX=0; iX<filterKey.size(); iX++) {
		    filterBuf.append(filterKey.get(iX)).append("=").append(filterValue.get(iX)).append("|");
		}
		filterBuf.setLength(filterBuf.length()-1);
		String labelLink = "exportData.jsp?url=velos-doc.pdf?repId="+repId+filterBuf.toString()+"&filePath="+filePath+"&moduleName="+calledfrom;

		if (pageRight < 6)
		{
			String strRightMsg = "javascript:alert(&quot;"+MC.M_NoRgtTo_DldRpt/*You do not have access rights to download the report*****/+"&quot;);";
			wordLink = strRightMsg;
			excelLink = strRightMsg;
			pdfLink = strRightMsg;
			printLink = strRightMsg;
			labelLink = strRightMsg;
		}

		
		//Set the param for header and footer
		transformer.setParameter("hdrFileName", hdrFilePath);
		transformer.setParameter("ftrFileName", ftrFilePath);
		transformer.setParameter("repTitle",repTitle);
		transformer.setParameter("repName",repName);
		transformer.setParameter("repBy",uName);
		transformer.setParameter("repDate",repDate);
		//KM-Filters displayed in the report.
		while(filterStr.contains("VELLABEL[")){
			startIndx = filterStr.indexOf("VELLABEL[") + "VELLABEL[".length();
			stopIndx = filterStr.indexOf("]",startIndx);
			labelKeyword = filterStr.substring(startIndx, stopIndx);
			labelString = LC.getLabelByKey(labelKeyword);
			filterStr=StringUtil.replaceAll(filterStr, "VELLABEL["+labelKeyword+"]", labelString);
		}
		
		while(filterStr.contains("VELMESSGE[")){
			startIndx = filterStr.indexOf("VELMESSGE[") + "VELMESSGE[".length();
			stopIndx = filterStr.indexOf("]",startIndx);
			messageKeyword = filterStr.substring(startIndx, stopIndx);
			messageString = MC.getMessageByKey(messageKeyword);
			filterStr=StringUtil.replaceAll(filterStr, "VELMESSGE["+messageKeyword+"]", messageString);
		}
		while(filterStr.contains("VELPARAMESSGE[")){
			startIndx = filterStr.indexOf("VELPARAMESSGE[") + "VELPARAMESSGE[".length();
			stopIndx = filterStr.indexOf("]",startIndx);
			messageKeyword = filterStr.substring(startIndx, stopIndx);
			messageKey=messageKeyword.substring(0, messageKeyword.indexOf("||"));
			messageKey=messageKey.trim();
			messageParaKeyword=messageKeyword.substring(messageKeyword.indexOf("||")+2).split(java.util.regex.Pattern.quote("||"));
			messageString = VelosResourceBundle.getMessageString(messageKey,messageParaKeyword);
			filterStr=StringUtil.replaceAll(filterStr, "VELPARAMESSGE["+messageKeyword+"]", messageString);
		}
		if(!filterStr.contains("word.GIF")){
			if(filterStr.contains(LC.L_Word_Format)){
				tobeReplaced = "<img border=\"0\" title=\""+LC.L_Word_Format+"\" alt=\""+LC.L_Word_Format+"\" src=\"./images/word.GIF\"></img>";
				filterStr=StringUtil.replace(filterStr, LC.L_Word_Format, tobeReplaced);
			}
			if(filterStr.contains(LC.L_Excel_Format)){
				tobeReplaced = "<img border=\"0\" title=\""+LC.L_Excel_Format+"\" alt=\""+LC.L_Excel_Format+"\" src=\"./images/excel.GIF\"></img>";
				filterStr=StringUtil.replace(filterStr,LC.L_Excel_Format, tobeReplaced);
			}
			if(filterStr.contains(LC.L_Printer_FriendlyFormat)){
				tobeReplaced = "<img border=\"0\" src=\"./images/printer.gif\" title=\""+LC.L_Printer_FriendlyFormat+"\" alt=\""+LC.L_Printer_FriendlyFormat+"\"></img>";
				filterStr=StringUtil.replace(filterStr, LC.L_Printer_FriendlyFormat, tobeReplaced);
			}
		}
		transformer.setParameter("argsStr","["+repArgsDisplay+"] "+ StringUtil.replace(filterStr,"&amp;#","&#").replace(",",",\n"));//To Fix Bug# 9798:17 May 2012
		transformer.setParameter("cond","T");
		transformer.setParameter("wd",wordLink);
		transformer.setParameter("xd",excelLink);
		transformer.setParameter("pd",pdfLink);
		transformer.setParameter("hd",printLink);
		transformer.setParameter("ld",labelLink);
		transformer.setParameter("hdrflag", hdrflag);
		transformer.setParameter("ftrflag", ftrflag);
		transformer.setParameter("dl", fileDnPath);
		transformer.setParameter("tz", tz.getDisplayName());
		
		transformer.setParameter("studyApndxParam", "?tableName=ER_STUDYAPNDX&columnName=STUDYAPNDX_FILEOBJ&pkColumnName=PK_STUDYAPNDX&module=study&db=eres&pkValue="); //KM-20Aug08
		transformer.setParameter("numFormat",NumberUtil.getXSLNumFormat());
		transformer.setParameter("floatFormat",NumberUtil.getXSLFloatFormat());
		transformer.setParameter("numFormatLocale",Configuration.getNumFormatLocale());
		transformer.setOutputProperty("encoding", "UTF-8");
				%>

<%		// Perform the transformation, sending the output to the response.
      	transformer.transform(xmlSource, new StreamResult(out));

    }
	catch (Exception e)
	{
  		out.write(e.getMessage());
 	}
	}else{
			%>
			  <jsp:include page="accessdenied.jsp" flush="true"/>
			<%
		}
		
	}catch(Exception ex){
		Rlog.error("report",ex.getMessage());
		return;
	}finally{
		/*Changes for: INF-22402 Date:10/04/2012 By:Yogendra Pratap Singh*/
		if(EJBUtil.isEmpty(calledfrom)){
			calledfrom = LC.L_Rpt_Central;
		}
		int downloadFlag=0;
		String downloadFormat = null;
	%>
		<jsp:include page="userReportLogging.jsp" flush="true">
		<jsp:param 	value="<%=repId %>" 		name="repId"/>
		<jsp:param 	value="<%=fileName%>" 		name="fileName"/>
		<jsp:param 	value="<%=filePath %>"		name="filePath"/>
		<jsp:param 	value="<%=calledfrom%>"		name="moduleName"/>
		<jsp:param 	value="<%=xml%>" 			name="repXml"/>
		<jsp:param 	value="<%=downloadFlag%>"	name="downloadFlag"/>
		<jsp:param value="<%=downloadFormat %>" name="downloadFormat"/>
		</jsp:include>
<%	}
} //end of if session times out

else

{

%>

<jsp:include page="timeout_childwindow.jsp" flush="true"/>


<%

}

%>
