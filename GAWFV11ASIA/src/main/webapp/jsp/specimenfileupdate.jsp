<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
</HEAD>


<BODY>
<%--
<DIV class="browserDefault" id = "div1">
--%>

<jsp:useBean id="specimenApndxB" scope="page" class="com.velos.eres.web.specimenApndx.SpecimenApndxJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java"  import="com.velos.esch.business.common.*,java.util.*,com.velos.eres.service.util.*"%>
  <%

String	src = request.getParameter("srcmenu");
String eSign = request.getParameter("eSign");


String selectedTab = request.getParameter("selectedTab");
String mode = request.getParameter("mode");
String specApndxId = request.getParameter("specId");
String specimenId = request.getParameter("specimenId");

String pkId =request.getParameter("pkId");

//JM: 21Jul2009: issue no-4174
String perId = "", stdId = "";
perId = request.getParameter("pkey");
perId = (perId==null)?"":perId;

stdId = request.getParameter("studyId");
stdId = (stdId==null)?"":stdId;

String FromPatPage=request.getParameter("FromPatPage");
if(FromPatPage==null)
{
	FromPatPage="";
}
String patientCode=request.getParameter("patientCode");
if(patientCode==null){
	patientCode="";
}




HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession))

   {
   String usr = null;
   usr = (String) tSession.getValue("userId");
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
   	String oldESign = (String) tSession.getValue("eSign");


	if(!oldESign.equals(eSign)) {
%>
   	  <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
	} else {
	        String desc = request.getParameter("specimenApndxDesc");


	        specimenApndxB.setSpecimenApndxId(EJBUtil.stringToNum(specimenId));
		specimenApndxB.getSpecimenApndxDetails();
		specApndxId = Integer.toString(specimenApndxB.getSpecimenApndxId());
		specimenApndxB.setSpecApndxDesc(desc);
		specimenApndxB.setModifiedBy(usr);

		int i=specimenApndxB.updateSpecimenApndx();
%>
<br>
<br>
<br>
<br>
<br>
<p class = "sectionHeadings" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully.*****--%> </p>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=specimenapndx.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&pkId=<%=pkId%>&pkey=<%=perId%>&studyId=<%=stdId%>&FromPatPage=<%=FromPatPage%>&patientCode=<%=StringUtil.encodeString(patientCode)%>" >

<%
	}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

</HTML>