<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
   
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%
String tab1 = request.getParameter("selectedTab");
boolean isIrb = false; 
if (tab1 != null && tab1.startsWith("irb")) { isIrb = true; } 
if (isIrb) {
%>
<title><%=MC.M_ResCompNew_AppStd%><%--Research Compliance >> New Application >>  <%=LC.Std_Study%> Personnel*****--%></title>
<% } else { %>
<title><%=MC.M_StdTeam_AssnUsr%><%--<%=LC.Std_Study%> >> Team >> Assign Users*****--%></title>
<% } %>
<SCRIPT Language="javascript">
	
if (document.getElementById('eSign')) { document.getElementById('eSign').disabled=false; } 
if (document.getElementById('submit_btn')) { document.getElementById('submit_btn').disabled=false; }
function  blockSubmit() {
	 setTimeout('document.getElementById(\'eSign\').disabled=true', 10);
	 setTimeout('document.getElementById(\'submit_btn\').disabled=true', 10);
}
function changeCount(row)
{
	  	
	  selrow = row ;
     //alert(selrow +obj.name);
	  checkedrows = document.users.checkedrows.value;
	  totusers = document.users.totalrows.value;
	  rows = parseInt(checkedrows);	
	  usernum = parseInt(totusers);	
    if (usernum > 1)
	{
       if (document.users.assign[selrow].checked)
       { 
			rows = parseInt(rows) + 1;

		}

	   else
		{
			rows = parseInt(rows) - 1;

		}

	}else{

	  if (document.users.assign.checked)

		{ 

			rows = parseInt(rows) + 1;

		}

	  else

		{

			rows = parseInt(rows) - 1;

		}



	}	

	document.users.checkedrows.value = rows;

	}



function validate(formobj)

{

    // formobj=document.users;
     ls_tot =formobj.lusers.value;
     
     // #5370 01/28/2011 @Ankit
     //whether checkbox is checked or not
     var chk = false;
     if (ls_tot == 1)	{
    	 if (formobj.assign.checked==false)
         {
       		alert("<%=MC.M_Check_TheUser%>");/*alert("Please check the user.");*****/
			return false;
		 }
     } 
     else   {
         for (ll_cnt = 0;ll_cnt < ls_tot;ll_cnt ++)  {
             if (formobj.assign[ll_cnt].checked==true)	{	
        		chk = true; 
        		break;			 
			 }
         }
         if(chk == false)
         {
        	alert("<%=MC.M_PlsSel_TheUsr%>");/*alert("Please select the user.");*****/
			return false;
         } 
     }
     
 
     //whether role has been entered if the checkbox is checked
	 if (ls_tot == 1)
     {
         if (formobj.assign != '' && formobj.assign.value != null)
         {
            if (!(validate_col('Role',formobj.role))) return false;
         } 
     } else
     {
         for (ll_cnt = 0;ll_cnt < ls_tot;ll_cnt ++)  {
			 if ( formobj.assign[ll_cnt].checked)
             {	
			   if (!(validate_col('Role',formobj.role[ll_cnt]))) return false;

             } 
         }
     }
	 
	 //whether checkbox is checked if role is selected
     if (ls_tot == 1)
     {
         if (formobj.role.value != '' && formobj.role.value != null)
         {
            if (formobj.assign.checked==false){
            	alert("<%=MC.M_Check_TheUser%>");/*alert("Please check the user.");*****/
				return false;
			}
         } 
     } else   {
         for (ll_cnt = 0;ll_cnt < ls_tot;ll_cnt ++)  {
	         if (formobj.role[ll_cnt].value != '' && formobj.role[ll_cnt].value != null)
             {				 
			   if (formobj.assign[ll_cnt].checked==false){
				   alert("<%=MC.M_PlsSel_TheUsr%>");/*alert("Please select the user.");*****/
					return false;
				}

             } 
         }
     }
     
     

	if (!(validate_col('e-Signature',formobj.eSign))) return false
// 	if(isNaN(formobj.eSign.value) == true) {
<%-- 		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/ --%>
// 		formobj.eSign.focus();
// 		return false;
// 	}	 
	 
	 
}

	



</SCRIPT>



<% String src;
  src= request.getParameter("srcmenu");
  String from = "team";
%>


<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>   


<%@ page   	import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.business.common.*" %>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>
<body>
<jsp:useBean id="groupB" scope="session" class="com.velos.eres.web.group.GroupJB"/>
<jsp:useBean id="userB" scope="session" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.*"%>
<%
HttpSession tSession = request.getSession(true);
String study = "";
if (sessionmaint.isValidSession(tSession))

{
	study = (String) tSession.getValue("studyId");
}
%>
<DIV class="BrowserTopn" id = "div1"> 
<% String includeTabsJsp = isIrb ? "irbnewtabs.jsp" : "studytabs.jsp"; %>
  <jsp:include page="<%=includeTabsJsp%>" flush="true">
  <jsp:param name="from" value="<%=from%>"/> 
  <jsp:param name="studyId" value="<%=study %>"/>  
  </jsp:include>
    </div>
    <% if("Y".equals(CFG.Workflows_Enabled) && (study!=null && !study.equals("") && !study.equals("0"))){ %>

<SCRIPT LANGUAGE="JavaScript">
	var isIE = jQuery.browser.msie;
	var screenWidth = screen.width;
	var screenHeight = screen.height;
	if(screenWidth>1280 || screenHeight>1024)
	{
		if(isIE==true)
		{
			document.write('<DIV class="BrowserBotN BrowserBotN_S_1 workflowDivBig" id="div1" style="height:80%">')
		}
		else
		{
			document.write('<DIV class="BrowserBotN BrowserBotN_S_1 workflowDivBig" id="div1" style="height:80%">')
		}
	}
	else
	{
		document.write('<DIV class="BrowserBotN BrowserBotN_S_1 workflowDivBig" id="div1">')
	}
</SCRIPT>
<%}else{ %>

<SCRIPT LANGUAGE="JavaScript">
	var isIE = jQuery.browser.msie;
	var screenWidth = screen.width;
	var screenHeight = screen.height;
	if(screenWidth>1280 || screenHeight>1024)
	{
		if(isIE==true)
		{
			document.write('<DIV class="BrowserBotN BrowserBotN_S_1" id="div1" style="height:78%">')
		}
		else
		{
			document.write('<DIV class="BrowserBotN BrowserBotN_S_1" id="div1" style="height:80%">')
		}
	}
	else
	{
		document.write('<DIV class="BrowserBotN BrowserBotN_S_1" id="div1">')
	}
</SCRIPT>

<%} %>
  <%

    UserDao userDao=new UserDao(); 
    

   if (sessionmaint.isValidSession(tSession))

   {
	int studyId= EJBUtil.stringToNum(study);
	String ufname= request.getParameter("fname") ;
	String ulname=request.getParameter("lname") ;
	String tab= request.getParameter("selectedTab");

//	JM : 21April05	
	String orgName=request.getParameter("accsites"); 
	String group=request.getParameter("accgroup");
	String jobType=request.getParameter("jobType");	



	//out.print(ufname +" " +ulname);	

	//if (request.getParameter("fname")==null){ ufname="%" ;}

	//if (request.getParameter("lname") ==null){ ulname="%";	}



	Integer codeId;

	int count;

	String role;

	CodeDao cdRole = new CodeDao();   

	if(! ("LIND".equals(CFG.EIRB_MODE) )) { 
	  	  cdRole.getCodeValues("role");
	  } else {		  
		  String[] result = CFG.Study_Role_Filter_Subtypes.split(",");
		  String[] codeSubTypes= new String[result.length];		  
	      for (int x=0; x<result.length; x++) {
	         codeSubTypes[x]=result[x];		         
	      }	
	   cdRole.getCodeValuesFilteredBySubtypes("role", codeSubTypes);		  
	} 

	ArrayList cDesc= cdRole.getCDesc();

	ArrayList cId= cdRole.getCId();

	/* StringBuffer mainStr = new StringBuffer();

	mainStr.append("<SELECT NAME=role >") ;

	for (count = 0; count <= cDesc.size() -1 ; count++){

		codeId = (Integer) cId.get(count);

		mainStr.append("<OPTION value = "+ codeId+">" + cDesc.get(count)+ "</OPTION>");			

	}

	mainStr.append("<OPTION value =\"\" SELECTED> </OPTION>");

	mainStr.append("</SELECT>");

        role = mainStr.toString(); */
    
    role = cdRole.toPullDown("role");        

	String uName = (String) tSession.getValue("userName");

	int pageRight = 0;

	String accId = (String) tSession.getValue("accountId");

	pageRight = EJBUtil.stringToNum(request.getParameter("right")); 



	//GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		

	//pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));

   if (pageRight > 0 )

	 {
//	 modified for using search criteria like orgName,group,jobType JM : 21April05
	 userDao.getAvailableTeamUsers(studyId,EJBUtil.stringToNum(accId),ulname.trim(),ufname.trim(),orgName,group,jobType);        ArrayList usrLastNames;
          ArrayList usrFirstNames;
          ArrayList usrMidNames;
          ArrayList usrIds;       
          ArrayList usrSiteNames;	
          ArrayList usrStats;
	  ArrayList usrTypes;

         String usrLastName = null;
 	 String usrFirstName = null;
	 String usrMidName = null;
	 String usrId=null;		
	 String usrSiteName=null;
	 
	 //code added for  #S12 enhamcement by KN
	
	 String usrStat = null;
	 String usrType = null;
	
	
	 //

	 int counter = 0;

%>

  <Form name="users" id="userstoteam" method="post" action="updatestudyteam.jsp" onSubmit = "if (validate(document.users)== false) { setValidateFlag('false'); return false; } else { blockSubmit(); setValidateFlag('true'); return true;}">
    <Input type="hidden" name="checkedrows" value=0>
    <Input type="hidden" name= "src" value= <%=src%> >
    <Input type="hidden" name= "studyId" value= <%=studyId%> >
    <Input type="hidden" name="selectedTab" value="<%=tab%>">
    <Input type="hidden" name="right" value="<%=pageRight%>">
    <table width="98%" cellspacing="0" cellpadding="0" border=0>
      <tr > 
        <td > 
          <P class = "sectionHeadings"> <%=MC.M_Assign_UsersToTeam%><%--Assign Users to Team*****--%> </P>
        </td>
      </tr>
      <tr>
		<td align="right">
			<button onclick="window.history.back(); return false;"><%=LC.L_Back%></button>
		</td>	
	  </tr>
    </table>
    <table width="98%" border=0>
      <tr> 
     <th width="15%"> <%=LC.L_First_Name%><%--First Name*****--%> </th>
	<th width="15%"> <%=LC.L_Last_Name%><%--Last Name*****--%> </th>
	<th width="20%"> <%=LC.L_Organization%><%--Organization*****--%> </th>
	<th width="15%"> <%=LC.L_User_Type%><%--User Type*****--%> </th>
        <th width="8%"><%=LC.L_Select%><%--Select*****--%> </th>
	<th width="25%"><%=LC.L_Role%><%--Role*****--%></th>
      </tr>
      <%

		usrLastNames = userDao.getUsrLastNames();
		
		usrFirstNames = userDao.getUsrFirstNames();

		usrMidNames = userDao.getUsrMidNames();

            	usrIds = userDao.getUsrIds();   
		
	    	usrSiteNames = userDao.getUsrSiteNames();
		
		//code added for  #S12 enhamcement by KN	
		
		usrStats=userDao.getUsrStats();
		
		usrTypes=userDao.getUsrTypes();
		
		int i;
		int lenUsers = usrLastNames.size();


	%>
      <tr id ="browserBoldRow"><%Object[] arguments1 = {lenUsers}; %> 
        <td colspan="6"> <%=MC.M_Tot_NumOfUsers%><%--Total Number of Users*****--%> : <%=VelosResourceBundle.getLabelString("L_Usr_S",arguments1)%><%-- <%= lenUsers%> User(s)*****--%> </td>
      </tr>
      <Input type="hidden" name = "lusers" value= <%=lenUsers%> >
      <%

	       for(i = 0 ; i < lenUsers ; i++)

               {

		 usrLastName=((usrLastNames.get(i)) == null)?"-":(usrLastNames.get(i)).toString();
		 usrFirstName=((usrFirstNames.get(i))==null)?"-":(usrFirstNames.get(i)).toString();
		 usrMidName=((usrMidNames.get(i))==null)?"-":(usrMidNames.get(i)).toString();
		 usrId = ((Integer)usrIds.get(i)).toString();
		
// 		JM : 21April05		
		 usrSiteName=((usrSiteNames.get(i)) == null)?"-":(usrSiteNames.get(i)).toString();
		 
		 
		 //code added for  #S12 enhamcement by KN
		 
		 usrStat=((usrStats.get(i)) == null)?"-":(usrStats.get(i)).toString();
		 usrType=((usrTypes.get(i)) == null)?"-":(usrTypes.get(i)).toString();
		 
		 if (usrStat.equals("A")){
		     usrStat=LC.L_Active_AccUser/*"Active Account User"*****/;
		 }
		 else if (usrStat.equals("B")){
		     usrStat=LC.L_Blocked_User/*"Blocked User"*****/;
		 }
		 else if (usrStat.equals("D")){
		     usrStat=LC.L_Deactivated_User/*"Deactivated User"*****/;
		 }
		 
		 if (usrType.equals("N")){  //KM-03Apr08
			 if (usrStat.equals(LC.L_Deactivated_User/*"Deactivated User"*****/))
		         usrStat=MC.M_DeactNon_SysUsr/*"Deactivated Non System User"*****/;
			 else
		         usrStat=MC.M_Non_SystemUser/*"Non System User"*****/;  
		 }
		 
		 
		if ((i%2)==0) {

  %>
      <tr class="browserEvenRow"> 
        <%

		}

		else{

  %>
      <tr class="browserOddRow"> 
        <%

		}

  %>
        <td width =250>  <%=usrFirstName%>  
          <input type="hidden" name="userId" value="<%=usrId%>">
        </td>		
		<td width =250>  <%=usrLastName%>
        </td>	
		<td width =250>  <%=usrSiteName%>            
        </td>
	 </td>	
		<td width =250>  <%=usrStat%>            
        </td>

        <td width =50> 
          <Input type="checkbox" name="assign" value = "<%=usrId%>"onclick="changeCount(<%=i%>)"  >
          	  
        </td>
        <td> <%=role%> </td>
      </tr>
      <%

		}

%>
    <Input type="hidden" name="totalrows" value=<%=lenUsers%>  >
    </table>
	
	<% 
		String displayESign="Y";
	
		if (pageRight > 4){ 
		  String showSubmit = "";
		  if  (!(lenUsers > 0)) {
		     showSubmit = "N";
		     displayESign = "N";
	      }
	%>
	
	<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="<%=displayESign %>"/>
			<jsp:param name="formID" value="userstoteam"/>
			<jsp:param name="showDiscard" value="N"/>
			<jsp:param name="showSubmit" value="<%=showSubmit%>"/>
	</jsp:include>
	
	<%}%>
	

  </Form>
  <%

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id = "emenu"> 
  <jsp:include page="getmenu.jsp" flush="true"/>
</div>
</body>

</html>
