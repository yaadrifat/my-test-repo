<%@page import="java.util.LinkedHashMap"%>
<%@page import="com.velos.eres.widget.service.util.GadgetUtil"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.web.studyRights.StudyRightsJB"%>
<%@page import="com.velos.eres.widget.web.GadgetFinancialJB"%>
<%@page import="java.io.FileOutputStream,java.io.OutputStreamWriter"%>
<%@page import="java.text.DecimalFormat,java.util.ArrayList"%>
<%@page import="org.json.JSONObject,org.json.JSONArray,org.json.JSONException,com.velos.eres.service.util.*"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%
HttpSession tSession = request.getSession(true);
String sessId = tSession.getId();
if (sessId.length()>8) { sessId = sessId.substring(0,8); }
sessId = Security.encrypt(sessId);
char[] chs = sessId.toCharArray();
StringBuffer sb1 = new StringBuffer();
DecimalFormat df = new DecimalFormat("000");
for (int iX=0; iX<chs.length; iX++) {
    sb1.append(df.format((int)chs[iX]));
}
String keySessId = sb1.toString();
%>
<%--
if (!keySessId.equals(request.getParameter("key"))) {
--%>
<%--
    <h1><%=LC.L_Forbidden%><%--Forbidden*****--></h1>
<%--    <p><%=MC.M_NotPerm_ToAcesServer%><%--You do not have permission to access this server or file.*****--%></p>
<%--    </body>
    </html>
     --%>
<%--
    return;
} --%>
<%
if (sessionmaint.isValidSession(tSession)) {
	String userId = (String) tSession.getValue("userId");	
	GadgetFinancialJB gadgetFinJB = new GadgetFinancialJB(userId);
	String studyId = request.getParameter("studyId");
	String studyNumber = request.getParameter("studyNum");
	String viewName = request.getParameter("view");
	String calledFrom = request.getParameter("calledFrom");
	if (StringUtil.isEmpty(viewName) || StringUtil.isEmpty(calledFrom)){
	%>
	    <h1><%=LC.L_Forbidden%><%--Forbidden*****--%></h1>
	    <p><%=MC.M_NotPerm_ToAcesServer%><%--You do not have permission to access this server or file.*****--%></p>
	<%
	    return;
	}
   	int pageRight = 0;
   	StudyRightsJB stdRights = new StudyRightsJB();
   	pageRight = stdRights.getStudyRightsForModule(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(userId), "MILESTONES");
	if (StringUtil.isAccessibleFor(pageRight,'V')) {
		String headerStr = "";
		String jsString = null;
		JSONObject jsObj = null;

		if ("invoiceable".equals(calledFrom)){
			try{
		    	jsString = gadgetFinJB.fetchInvoiceablesJSON(studyId);
		    	headerStr = VelosResourceBundle.getLabelString("L_Parameter_Report", studyNumber + " " + LC.L_Invoiceable);
				jsObj = new JSONObject(jsString);
			} catch(Exception e) {
			    Rlog.fatal("gadgetFinancialExport", "Error while calling fetchInvoiceablesJSON "+e);
			}
		}
		if ("invoiced".equals(calledFrom)){
			try{
		    	jsString = gadgetFinJB.fetchInvoicedJSON(studyId, viewName);
		    	headerStr = VelosResourceBundle.getLabelString("L_Parameter_Report", studyNumber + " " + LC.L_Invoiced);
				jsObj = new JSONObject(jsString);
			} catch(Exception e) {
			    Rlog.fatal("gadgetFinancialExport", "Error while calling fetchInvoicedJSON "+e);
			}
		}
		if ("uninvoiced".equals(calledFrom)){
			try{
		    	jsString = gadgetFinJB.fetchUninvoicedJSON(studyId);
		    	headerStr = VelosResourceBundle.getLabelString("L_Parameter_Report", studyNumber + " " + LC.L_Uninvoiced);
				jsObj = new JSONObject(jsString);
			} catch(Exception e) {
			    Rlog.fatal("gadgetFinancialExport", "Error while calling fetchUninvoicedJSON "+e);
			}
		}
		if ("collected".equals(calledFrom)){
			try{
		    	jsString = gadgetFinJB.fetchCollectedJSON(studyId);
		    	headerStr = VelosResourceBundle.getLabelString("L_Parameter_Report", studyNumber + " " + LC.L_Collected);
				jsObj = new JSONObject(jsString);
			} catch(Exception e) {
			    Rlog.fatal("gadgetFinancialExport", "Error while calling fetchCollectedJSON "+e);
			}
		}
		if ("outstanding".equals(calledFrom)){
			try{
		    	jsString = gadgetFinJB.fetchOutstandingJSON(studyId);
		    	headerStr = VelosResourceBundle.getLabelString("L_Parameter_Report", studyNumber + " " + LC.L_Outstanding);
				jsObj = new JSONObject(jsString);
			} catch(Exception e) {
			    Rlog.fatal("gadgetFinancialExport", "Error while calling fetchOutstandingJSON "+e);
			}
		}
		if ("receipts".equals(calledFrom)){
			try{
		    	jsString = gadgetFinJB.fetchReceiptsJSON(studyId);
		    	headerStr = VelosResourceBundle.getLabelString("L_Parameter_Report", studyNumber + " " + LC.L_Receipts);
				jsObj = new JSONObject(jsString);
			} catch(Exception e) {
			    Rlog.fatal("gadgetFinancialExport", "Error while calling fetchReceiptsJSON "+e);
			}
		}
		if ("disbursements".equals(calledFrom)){
			try{
		    	jsString = gadgetFinJB.fetchDisbursementsJSON(studyId);
		    	headerStr = VelosResourceBundle.getLabelString("L_Parameter_Report", studyNumber + " " + LC.L_Disbursements);
				jsObj = new JSONObject(jsString);
			} catch(Exception e) {
			    Rlog.fatal("gadgetFinancialExport", "Error while calling fetchDisbursementsJSON "+e);
			}
		}
		if ("netCash".equals(calledFrom)){
			try{
		    	jsString = gadgetFinJB.fetchNetCashJSON(studyId);
		    	headerStr = VelosResourceBundle.getLabelString("L_Parameter_Report", studyNumber + " " + LC.L_NetCash);
				jsObj = new JSONObject(jsString);
			} catch(Exception e) {
			    Rlog.fatal("gadgetFinancialExport", "Error while calling fetchNetCashJSON "+e);
			}
		}

		if (jsObj == null) { jsObj = new JSONObject(); }
		String colArray = jsObj.get("colArray").toString();
		String dataArray = jsObj.get("dataArray").toString();
		String format = "csv";
		String filename = null;
		String contentApp = null;

		boolean isIE = false;
		try { isIE = request.getHeader("USER-AGENT").toUpperCase().indexOf("MSIE") != -1; } catch(Exception e) {}
		
	    LinkedHashMap<String, Object> formattedFile = new LinkedHashMap<String, Object>();
	    formattedFile = GadgetUtil.getExportFile(colArray, dataArray, headerStr, "html");
		StringBuffer sb =  new StringBuffer();
	    sb.append(formattedFile.get("fileData"));
	    
	    formattedFile = new LinkedHashMap<String, Object>();
	    formattedFile = GadgetUtil.getExportFile(colArray, dataArray, headerStr, format);
	    filename = (String)formattedFile.get("fileName") ;
	    contentApp = (String)formattedFile.get("contentApp");
		StringBuffer completeFile =  new StringBuffer();
		completeFile.append(formattedFile.get("formattedFile"));
	    
	    //html File creation
		Configuration.readAppendixParam(Configuration.ERES_HOME+"eresearch.xml");
		com.aithent.file.uploadDownload.Configuration.readSettings("eres");
		com.aithent.file.uploadDownload.Configuration.readUploadDownloadParam(com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD + "fileUploadDownload.xml", "study");
		FileOutputStream fos = null;
		OutputStreamWriter osw = null;
		String filepath = null;
		String filefolder = null;
		String path=null;
		try {
			path=com.aithent.file.uploadDownload.Configuration.DOWNLOADSERVLET;
			filefolder = Configuration.DOWNLOADFOLDER;
	        filepath = EJBUtil.getActualPath(Configuration.DOWNLOADFOLDER, filename);
		    fos = new FileOutputStream(filepath);
		    osw = new OutputStreamWriter(fos);
		    char[] c = completeFile.toString().toCharArray(); 
		    osw.write(c, 0, c.length);
		    osw.flush();
		    fos.flush();
		} catch(Exception e) {
		    Rlog.fatal("gadgetFinancialExport", "Error creating download file: "+e);
		} finally {
		    try { osw.close(); } catch(Exception e) {}
		    try { fos.close(); } catch(Exception e) {}
		}
		
		String wordLink = "repGetWord.jsp?htmlFile=" + filepath +"&fileDnPath="+path+"&filePath="+filefolder+"&repId=0&calledfrom="+calledFrom;
		String excelLink = "repGetExcel.jsp?htmlFile=" + filepath +"&fileDnPath="+path+"&filePath="+filefolder+"&repId=0&calledfrom="+calledFrom;
		String printLink = "repGetHtml.jsp?htmlFileName=" + filepath +"&fileDnPath="+path+"&filePath="+filefolder+"&repId=0&calledfrom="+calledFrom;
		String csvLink = path+"?file="+filename+"&mod=R&repId=0&moduleName=invoiceable&filePath="+filefolder+"";
%>
	<table width="100%" >
       	<tr class="reportGreyRow">
       		<td class="reportPanel"> 
       			<%=MC.M_Download_ReportIn%><!-- Download the report in -->: 
       			<%--<A href="<%=wordLink%>" >
       				<img border="0" title="<%=LC.L_Word_Format%>" alt="<%=LC.L_Word_Format%>" src="./images/word.GIF" ></img>
       			</A>&nbsp;
       			<A href="<%=excelLink%>" >
       				<img border="0" title="<%=LC.L_Excel_Format%>" alt="<%=LC.L_Excel_Format%>" src="./images/excel.GIF" ></img>
       			</A>&nbsp;
			    <A href="<%=printLink%>" >
       				<img src="./images/printer.gif" border="0" title="<%=LC.L_Printer_FriendlyFormat%>" alt="<%=LC.L_Printer_FriendlyFormat%>"></img>
       			</A>&nbsp; --%>
       			<A href="<%=csvLink%>">
       			<img src="../images/jpg/CSV.gif" border="0" title="<%=LC.L_CSV_Format%>" alt="<%=LC.L_CSV_Format%>"></img>
       			</A>
       		</td>
       	</tr>
	</table><BR/>
	<%	
	    StringBuffer totalSB =  new StringBuffer();
		totalSB.append("<div style='height:"+(isIE?"400px":"430px")+"; overflow-y:scroll'>");
		totalSB.append(sb);
		totalSB.append("</div>");
	%>
	<%=totalSB.toString()%>
<%
	} // end of pageRight
	else {
	%>
		<jsp:include page="accessdenied.jsp" flush="true"/>
	<%
	} // end of else of pageRight
} else { // else of valid session
%>
<jsp:include page="timeout.html" flush="true"/>
<%
} // end of else of valid session
%>


