<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML> 
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<TITLE><%=LC.L_DelPat_Std%><%--Delete <%=LC.Pat_Patient%> and <%=LC.Std_Study%>*****--%></TITLE>
</HEAD>

<SCRIPT Language="javascript">
function  validate(formobj) {
	//INF-22237a 14-Mar-2012 @Ankit
	var delId = formobj.delId.value;
	var delMode = formobj.delMode.value;
	if(delId=="studypat" && delMode=="final")
	{
		var fdaRegulated = document.getElementById("FDARegulated").value;
		var reasonDel = document.getElementById("reason_del").value;
		reasonDel = reasonDel.replace(/^\s+|\s+$/g, "");
		document.getElementById("reason_del").value = reasonDel;
		if(fdaRegulated=="1" && reasonDel.length<=0)
		{
			alert("<%=MC.M_Etr_MandantoryFlds%>");
			document.getElementById("reason_del").focus();
			return false;
		}
	}
    if (!(validate_col('e-Signature',formobj.eSign))) 
        return false;

   <%--  if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
    } --%>
}
</SCRIPT>

<% String src;

src= request.getParameter("srcmenu");

%>
<jsp:include page="skinChoser.jsp" flush="true"/>
<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>



<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.*,com.aithent.audittrail.reports.AuditUtils"%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id ="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/> 

<jsp:useBean id ="personB" scope="request" class="com.velos.eres.web.person.PersonJB"/> 

<body>

<br>

<DIV class="formDefault" id="div1">

<% 

HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession)) {
    String patientId=request.getParameter("patientId");
    patientId=(patientId==null)?"":patientId;
    //km- to fix the Bug# 2408.
    String patientCode=StringUtil.decodeString(request.getParameter("patientCode"));
    patientCode=(patientCode==null)?"":patientCode;
    String studyNo=StringUtil.decodeString(request.getParameter("studyNo"));
    studyNo=(studyNo==null)?"":studyNo;
    String studyId=request.getParameter("studyId");
    studyId=(studyId==null)?"":studyId;
    String delId=request.getParameter("delId");
    String delMode=request.getParameter("delMode");
    
    // to track patient enrollment deleted by
    String patProtId = request.getParameter("patProtId");
    if (StringUtil.isEmpty(patProtId))
    {
    	patProtId = "";
    }
    
	
    if (delMode==null) {
        delMode="final";
%>

	<FORM name="deleteUsr" id="delpatstudyid" method="post" action="deletepatstudy.jsp" onSubmit="if (validate(document.deleteUsr)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<br><br>
	
	<% if (delId.equals("study")) {Object[] arguments1 = {studyNo}; %>
	
	<P class="defComments">	<%=VelosResourceBundle.getMessageString("M_DelCntRtrv_StdEsign",arguments1)%><%--You are about to delete Study&nbsp;<font color="red"><B><%=studyNo%></B></font>.  Once deleted, the data cannot be retrieved.  If you are sure that you would like to delete this study,<br> enter your e-signature below and click 'Submit'.*****--%></P>	
	<% }else if (delId.equals("patient")) { Object[] arguments2 = {patientCode};%>
	<P class="defComments">	<%=VelosResourceBundle.getMessageString("M_DelCntRtrv_PatEsign",arguments2)%><%--You are about to delete Patient&nbsp;<font color="red"><B><%=patientCode%></B></font>.  Once deleted, the data cannot be retrieved.  If you are sure that you would like to delete this patient,<br>enter your e-signature below and click 'Submit'.*****--%></P>	
	<% } else if (delId.equals("studypat")) { Object[] arguments3 = {patientCode,studyNo};
		//INF-22237a 14-Mar-2012 @Ankit
		String fdaRegulated="";
		studyId = studyId==null?"0":studyId;
		studyB.setId(StringUtil.stringToNum(studyId));
    	studyB.getStudyDetails();
    	fdaRegulated=studyB.getFdaRegulatedStudy();
		fdaRegulated = fdaRegulated==null?"0":fdaRegulated;
	%>
	
	<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
		<tr>
		<td align="right" valign="top" width="50%"><%=LC.L_Reason_ForDelete %> 
		<%if(fdaRegulated.equals("1")) {%>
		<font class="Mandatory">*</font>
		<%} %>&nbsp;
		</td>
		<Input type="hidden" name="FDARegulated" id="FDARegulated" value="<%=fdaRegulated %>">
		<td><textarea maxlength="4000" name="reason_del" id="reason_del"></textarea>
		<br><font class="Mandatory"><%=MC.M_Limit4000_Char %></font>
		</td>
		</tr>
	</table>
	<P class="defComments">	<%=VelosResourceBundle.getMessageString("M_DelCntRtrv_PatStdEsign",arguments3)%><%--You are about to delete Patient&nbsp;<font color="red"><B><%=patientCode%></B></font>&nbsp; from Study &nbsp;<font color="red"><B> <%=studyNo%></B></font>.  Once deleted, the data cannot be retrieved.  If you are sure that you would like to remove this patient from this study,<br>enter your e-signature below and click 'Submit'.*****--%></P>	
	<% } %>
	<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="delpatstudyid"/>
			<jsp:param name="showDiscard" value="N"/>
	</jsp:include>
	 <input type="hidden" name="delId" value="<%=delId%>">
	 <input type="hidden" name="delMode" value="<%=delMode%>">
	 <input type ="hidden" name="studyId" value="<%=studyId%>">
	 <input type="hidden" name="patientId" value="<%=patientId%>">

 	 <input type="hidden" name="patProtId" value="<%=patProtId%>">
	 

	</FORM>
	<% } else {
			String eSign = request.getParameter("eSign");	
			String oldESign = (String) tSession.getValue("eSign");
			
			if(!oldESign.equals(eSign)) {
			
%>
	 		  <jsp:include page="incorrectesign.jsp" flush="true"/>

<%
				} else {
				
		 String usr = null;
		 usr = (String) tSession.getValue("userId");
				
		// Modified for INF-18183 ::: AGodara 			
		
		if (delId.equals("study")) {
		    studyB.deleteStudy(EJBUtil.stringToNum(studyId),AuditUtils.createArgs(tSession,"",LC.L_Study));
		}
		// Amarnadh 
		if (delId.equals("studyId")) {
		    studyB.deleteStudy(EJBUtil.stringToNum(studyId),usr,AuditUtils.createArgs(tSession,"",LC.L_Study));
		}
		 if (delId.equals("patient")) {
		    personB.deletePatient(EJBUtil.stringToNum(patientId), usr,AuditUtils.createArgs(tSession,"",LC.L_Mng_Pats));
		}
		 if (delId.equals("studypat")) {    
			 String reason_del = request.getParameter("reason_del");//INF-22237a 14-Mar-2012 @Ankit
		    studyB.deletePatStudy(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(patientId),usr,patProtId,AuditUtils.createArgs(tSession,reason_del,LC.L_Mng_Pats));
		}

	
%>
<!--Added By Amarnadh for Issue #3218  -->
<BR> <BR> <BR> <BR> <BR> <BR> <BR> <BR> <BR>
<% if (delId.equals("studyId")) { %>
<p class = "successfulmsg" align = center> <%=MC.M_Std_DelSucc%><%--The <%=LC.Std_Study_Lower%> was deleted successfully*****--%></p>
<META HTTP-EQUIV=Refresh CONTENT="3; URL=advStudysearchpg.jsp?srcmenu=<%=src%>">
<% }else if (delId.equals("study")) { %>
<p class = "successfulmsg" align = center> <%=MC.M_Std_DelSucc%><%--The <%=LC.Std_Study_Lower%> was deleted successfully*****--%></p>
<META HTTP-EQUIV=Refresh CONTENT="3; URL=studybrowserpg.jsp?srcmenu=<%=src%>"> 
<% }else if (delId.equals("patient")) { %>
<p class = "successfulmsg" align = center> <%=MC.M_PatDel_Succ%><%--The <%=LC.Pat_Patient%> was deleted successfully*****--%></p>
<META HTTP-EQUIV=Refresh CONTENT="3; URL=allPatient.jsp?searchFrom=search&selectedTab=1&srcmenu=<%=src%>">
<% }else if (delId.equals("studypat")) { %>
<p class = "successfulmsg" align = center> <%=MC.M_PatRemStd_Succ%><%--The <%=LC.Pat_Patient%> was removed from the <%=LC.Std_Study_Lower%> successfully*****--%></p>
<META HTTP-EQUIV=Refresh CONTENT="3; URL=studypatients.jsp?srcmenu=<%=src%>&selectedTab=2">
<%}%>
<%
}
}
}//end of if body for session	
else
{
%>

<jsp:include page="timeout.html" flush="true"/>

<%

}

%>
  <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
  
</DIV>

<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/>
</div>

</body>

</HTML>

