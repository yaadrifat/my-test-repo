<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
<link rel="shortcut icon" type="image/x-icon" href="./favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>
<br />

<table width="680" height="100" style="margin-left: 10px;">

<tr><td>
<p class = "sectionheadings" style="color: blue; font-size: 16px;">
<%=MC.M_AppVerNotMatchedDbVer %><%-- Application version doesn''t match with the current database version.*****--%>
</p>
</td></tr>

<tr><td class="tdDefault" align="right">
<%=MC.M_Velos_EresError %><%-- Velos eResearch error*****--%>
<hr>
</td></tr>

<tr><td class="tdDefault">
<%=MC.M_Velos_EresCantRedirct %><%-- You cannot be redirected to the Velos eResearch login page at this time, please contact Customer Support at:\\n customersupport@velos.com*****--%>
</td></tr>

</table>