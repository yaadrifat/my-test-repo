<?xml version="1.0" ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xdb="http://xmlns.oracle.com/xdb"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<xsl:template match="/">
		<HTML>
			<HEAD>
				<SCRIPT language="javascript">
					var formWin = null; function validate(formobj) {

					if (!(validate_col("Data Entry
					Date",formobj.er_def_date_01))) return false ; if (!
					formobj.er_def_date_01.disabled) { if
					(!(validate_date(formobj.er_def_date_01))) return
					false ; }



					if (!(validate_col("Primary Seq
					No.",formobj.fld10050_15574_18145))) return false ;
					if (! formobj.fld10050_15574_18145.disabled) {
					if(isDecimal(formobj.fld10050_15574_18145.value) ==
					false) { alert("Please enter a valid Number");
					formobj.fld10050_15574_18145.focus(); return false;
					} }

					if (! formobj.fld10050_15574_18145.disabled) {
					if(validateDataRange(formobj.fld10050_15574_18145.value,"&gt;=",1.0,"and","&lt;=",9.0)
					== false) { alert("Please enter a Number which is
					Greater than equal to 1.0 and Less than equal to 9.0
					for Primary Seq No." );
					formobj.fld10050_15574_18145.focus(); return false;
					} }

					if (!(validate_col("Primary Dx
					Date",formobj.fld10050_15575_18146))) return false ;
					if (! formobj.fld10050_15575_18146.disabled) { if
					(!(validate_date(formobj.fld10050_15575_18146)))
					return false ; } if (!
					formobj.fld10050_15575_18146.disabled) { if
					(CompareDates(formobj.fld10050_15575_18146.value,formobj.er_def_date_01.value,'&gt;'))
					{ alert("Primary Dx Date cannot be greater than Date
					of Data Entry");
					formobj.fld10050_15575_18146.focus(); return false;
					} }

















					if (!(validate_col("Site
					Code",formobj.fld10050_15570_18141))) return false ;





					if (!(validate_col("Diagnosis
					Site",formobj.fld10050_15581_18152))) return false ;



					if (!(validate_col("Hist
					Code",formobj.fld10050_15571_18142))) return false ;





					if
					(!(validate_col("Histology",formobj.fld10050_15582_18153)))
					return false ;





					if
					(!(validate_chk_radio("Staged?",formobj.fld10050_15584_18155)))
					return false ;









































					if (! formobj.fld10050_15600_18172.disabled) { if
					(!(validate_date(formobj.fld10050_15600_18172)))
					return false ; } if (!
					formobj.fld10050_15600_18172.disabled) { if
					(CompareDates(formobj.fld10050_15600_18172.value,formobj.er_def_date_01.value,'&gt;'))
					{ alert("Progression/Recurrence Date cannot be
					greater than Date of Data Entry");
					formobj.fld10050_15600_18172.focus(); return false;
					} }





					if (! formobj.fld10050_15672_18244.disabled) {
					if(isDecimal(formobj.fld10050_15672_18244.value) ==
					false) { alert("Please enter a valid Number");
					formobj.fld10050_15672_18244.focus(); return false;
					} }















					if (! formobj.fld10050_15601_18173.disabled) { if
					(!(validate_date(formobj.fld10050_15601_18173)))
					return false ; } if (!
					formobj.fld10050_15601_18173.disabled) { if
					(CompareDates(formobj.fld10050_15601_18173.value,formobj.er_def_date_01.value,'&gt;'))
					{ alert("Progression/Recurrence Date cannot be
					greater than Date of Data Entry");
					formobj.fld10050_15601_18173.focus(); return false;
					} }





					if (! formobj.fld10050_15673_18245.disabled) {
					if(isDecimal(formobj.fld10050_15673_18245.value) ==
					false) { alert("Please enter a valid Number");
					formobj.fld10050_15673_18245.focus(); return false;
					} }















					if (! formobj.fld10050_15602_18174.disabled) { if
					(!(validate_date(formobj.fld10050_15602_18174)))
					return false ; } if (!
					formobj.fld10050_15602_18174.disabled) { if
					(CompareDates(formobj.fld10050_15602_18174.value,formobj.er_def_date_01.value,'&gt;'))
					{ alert("Progression/Recurrence Date cannot be
					greater than Date of Data Entry");
					formobj.fld10050_15602_18174.focus(); return false;
					} }





					if (! formobj.fld10050_15674_18246.disabled) {
					if(isDecimal(formobj.fld10050_15674_18246.value) ==
					false) { alert("Please enter a valid Number");
					formobj.fld10050_15674_18246.focus(); return false;
					} }















					if (! formobj.fld10050_15603_18175.disabled) { if
					(!(validate_date(formobj.fld10050_15603_18175)))
					return false ; } if (!
					formobj.fld10050_15603_18175.disabled) { if
					(CompareDates(formobj.fld10050_15603_18175.value,formobj.er_def_date_01.value,'&gt;'))
					{ alert("Progression/Recurrence Date cannot be
					greater than Date of Data Entry");
					formobj.fld10050_15603_18175.focus(); return false;
					} }





					if (! formobj.fld10050_15675_18247.disabled) {
					if(isDecimal(formobj.fld10050_15675_18247.value) ==
					false) { alert("Please enter a valid Number");
					formobj.fld10050_15675_18247.focus(); return false;
					} }















					if (! formobj.fld10050_15604_18176.disabled) { if
					(!(validate_date(formobj.fld10050_15604_18176)))
					return false ; } if (!
					formobj.fld10050_15604_18176.disabled) { if
					(CompareDates(formobj.fld10050_15604_18176.value,formobj.er_def_date_01.value,'&gt;'))
					{ alert("Progression/Recurrence Date cannot be
					greater than Date of Data Entry");
					formobj.fld10050_15604_18176.focus(); return false;
					} }





					if (! formobj.fld10050_15676_18248.disabled) {
					if(isDecimal(formobj.fld10050_15676_18248.value) ==
					false) { alert("Please enter a valid Number");
					formobj.fld10050_15676_18248.focus(); return false;
					} }















					if (! formobj.fld10050_15605_18177.disabled) { if
					(!(validate_date(formobj.fld10050_15605_18177)))
					return false ; } if (!
					formobj.fld10050_15605_18177.disabled) { if
					(CompareDates(formobj.fld10050_15605_18177.value,formobj.er_def_date_01.value,'&gt;'))
					{ alert("Progression/Recurrence Date cannot be
					greater than Date of Data Entry");
					formobj.fld10050_15605_18177.focus(); return false;
					} }





					if (! formobj.fld10050_15677_18249.disabled) {
					if(isDecimal(formobj.fld10050_15677_18249.value) ==
					false) { alert("Please enter a valid Number");
					formobj.fld10050_15677_18249.focus(); return false;
					} }















					if (! formobj.fld10050_15606_18178.disabled) { if
					(!(validate_date(formobj.fld10050_15606_18178)))
					return false ; } if (!
					formobj.fld10050_15606_18178.disabled) { if
					(CompareDates(formobj.fld10050_15606_18178.value,formobj.er_def_date_01.value,'&gt;'))
					{ alert("Progression/Recurrence Date cannot be
					greater than Date of Data Entry");
					formobj.fld10050_15606_18178.focus(); return false;
					} }





					if (! formobj.fld10050_15678_18250.disabled) {
					if(isDecimal(formobj.fld10050_15678_18250.value) ==
					false) { alert("Please enter a valid Number");
					formobj.fld10050_15678_18250.focus(); return false;
					} }















					if (! formobj.fld10050_15607_18179.disabled) { if
					(!(validate_date(formobj.fld10050_15607_18179)))
					return false ; } if (!
					formobj.fld10050_15607_18179.disabled) { if
					(CompareDates(formobj.fld10050_15607_18179.value,formobj.er_def_date_01.value,'&gt;'))
					{ alert("Progression/Recurrence Date cannot be
					greater than Date of Data Entry");
					formobj.fld10050_15607_18179.focus(); return false;
					} }





					if (! formobj.fld10050_15679_18251.disabled) {
					if(isDecimal(formobj.fld10050_15679_18251.value) ==
					false) { alert("Please enter a valid Number");
					formobj.fld10050_15679_18251.focus(); return false;
					} }















					if (! formobj.fld10050_15608_18180.disabled) { if
					(!(validate_date(formobj.fld10050_15608_18180)))
					return false ; } if (!
					formobj.fld10050_15608_18180.disabled) { if
					(CompareDates(formobj.fld10050_15608_18180.value,formobj.er_def_date_01.value,'&gt;'))
					{ alert("Progression/Recurrence Date cannot be
					greater than Date of Data Entry");
					formobj.fld10050_15608_18180.focus(); return false;
					} }





					if (! formobj.fld10050_15680_18252.disabled) {
					if(isDecimal(formobj.fld10050_15680_18252.value) ==
					false) { alert("Please enter a valid Number");
					formobj.fld10050_15680_18252.focus(); return false;
					} }















					if (! formobj.fld10050_15609_18181.disabled) { if
					(!(validate_date(formobj.fld10050_15609_18181)))
					return false ; } if (!
					formobj.fld10050_15609_18181.disabled) { if
					(CompareDates(formobj.fld10050_15609_18181.value,formobj.er_def_date_01.value,'&gt;'))
					{ alert("Progression/Recurrence Date cannot be
					greater than Date of Data Entry");
					formobj.fld10050_15609_18181.focus(); return false;
					} }





					if (! formobj.fld10050_15681_18253.disabled) {
					if(isDecimal(formobj.fld10050_15681_18253.value) ==
					false) { alert("Please enter a valid Number");
					formobj.fld10050_15681_18253.focus(); return false;
					} }














					if ( !(validate_col( 'e-Signature',formobj.eSign) )
					) return false if(isNaN(formobj.eSign.value) ==
					true) { alert("Incorrect e-Signature. Please enter
					again"); formobj.eSign.focus(); return false; }

					if ( !(validate_col( 'Form
					Status',formobj.er_def_formstat) ) ) return false;

					if (formobj.override_count.value>0) {
					formobj.action=&quot;querymodal.jsp&quot;;
					formobj.target="formwin"; modalWin =
					window.open("donotdelete.html","formwin","resizable=1,status=0,
					width=850,height=550
					top=100,left=100,menubar=no,scrollbars=1"); if
					(modalWin &amp;&amp; !modalWin.closed)
					modalWin.focus(); formobj.submit(); void(0);} }
					function openlookup(p_viewid,
					p_filter,p_kword,p_lkpType) { if (p_lkpType=="L"){
					windowName = window.open("getlookup.jsp?viewId="+
					p_viewid + "&amp;form=er_fillform1&amp;dfilter="+
					p_filter + "&amp;keyword="+p_kword,
					"LookUp","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=550
					top=100,left=100"); }else if (p_lkpType=="A") {
					windowName = window.open("dynreplookup.jsp?repId="+
					p_viewid + "&amp;form=er_fillform1&amp;dfilter="+
					p_filter + "&amp;keyword="+p_kword,
					"LookUp","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=550
					top=100,left=100"); } else { windowName =
					window.open("getlookup.jsp?viewId="+ p_viewid +
					"&amp;form=er_fillform1&amp;dfilter="+ p_filter +
					"&amp;keyword="+p_kword,
					"LookUp","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=550
					top=100,left=100"); } windowName.focus(); }

					function performAction(formobj, currentfld) { var
					actualField ; if (currentfld.type == undefined) {
					actualField = currentfld[0]; } else { actualField =
					currentfld; } if (actualField.name ==
					&apos;fld10050_15576_18147&apos;) { if
					(actualField.name ==
					&apos;fld10050_15576_18147&apos;) {

					var srcArr = new Array(); var targetArr = new
					Array(); var conditionValArr = new Array(); var
					conditionType; var actionType; var fld; fld =
					formobj.fld10050_15576_18147 ; conditionType =
					&quot;velEquals&quot; ; actionType =
					&quot;disabled&quot; ; targetArr[0] =
					formobj.fld10050_15577_18148 ; conditionValArr[0] =
					&quot;UMHS&quot; ; conditionValArr[1] = &quot;Prov -
					Sfld&quot; ; conditionValArr[2] = &quot;Prov -
					Novi&quot; ; conditionValArr[3] = &quot;VA&quot; ;
					conditionValArr[4] = &quot;Foote&quot; ;
					conditionValArr[5] = &quot;Ingham&quot; ;
					conditionValArr[6] = &quot;Alpena&quot; ;
					conditionValArr[7] = &quot;CMCH&quot; ;
					conditionValArr[8] = &quot;Unknown&quot; ;


					changeFieldStateMultiple(fld,conditionType,conditionValArr,targetArr,actionType);

					} if (actualField.name ==
					&apos;fld10050_15576_18147&apos;) {

					var srcArr = new Array(); var targetArr = new
					Array(); var conditionValArr = new Array(); var
					conditionType; var actionType; var fld; fld =
					formobj.fld10050_15576_18147 ; conditionType =
					&quot;velEquals&quot; ; actionType =
					&quot;disabled&quot; ; targetArr[0] =
					formobj.fld10050_15597_18169 ; conditionValArr[0] =
					&quot;UMHS&quot; ; conditionValArr[1] =
					&quot;VA&quot; ; conditionValArr[2] =
					&quot;Foote&quot; ; conditionValArr[3] =
					&quot;Ingham&quot; ; conditionValArr[4] =
					&quot;Alpena&quot; ; conditionValArr[5] =
					&quot;CMCH&quot; ; conditionValArr[6] =
					&quot;Other&quot; ; conditionValArr[7] =
					&quot;Unknown&quot; ;


					changeFieldStateMultiple(fld,conditionType,conditionValArr,targetArr,actionType);

					} return ; } if (actualField.name ==
					&apos;fld10050_15584_18155&apos;) { if
					(actualField.name ==
					&apos;fld10050_15584_18155&apos;) {

					var srcArr = new Array(); var targetArr = new
					Array(); var conditionValArr = new Array(); var
					conditionType; var actionType; var fld; fld =
					formobj.fld10050_15584_18155 ; conditionType =
					&quot;velEquals&quot; ; actionType =
					&quot;disabled&quot; ; targetArr[0] =
					formobj.fld10050_15585_18156 ; targetArr[1] =
					formobj.fld10050_15586_18157 ; targetArr[2] =
					formobj.fld10050_15587_18158 ; targetArr[3] =
					formobj.fld10050_15588_18159 ; targetArr[4] =
					formobj.fld10050_15589_18160 ; conditionValArr[0] =
					&quot;No&quot; ; conditionValArr[1] =
					&quot;Unknown&quot; ;


					changeFieldStateMultiple(fld,conditionType,conditionValArr,targetArr,actionType);

					} return ; } if (actualField.name ==
					&apos;fld10050_15592_18163&apos;) { if
					(actualField.name ==
					&apos;fld10050_15592_18163&apos;) {

					var srcArr = new Array(); var targetArr = new
					Array(); var conditionValArr = new Array(); var
					conditionType; var actionType; var fld; fld =
					formobj.fld10050_15592_18163 ; conditionType =
					&quot;velNotEquals&quot; ; actionType =
					&quot;disabled&quot; ; targetArr[0] =
					formobj.fld10050_15593_18164 ; conditionValArr[0] =
					&quot;FIGO&quot; ; conditionValArr[1] =
					&quot;DUKES&quot; ; conditionValArr[2] =
					&quot;GLEASON&quot; ; conditionValArr[3] =
					&quot;NWTS&quot; ; conditionValArr[4] = &quot;ANN
					ARBOR&quot; ;


					changeFieldStateMultiple(fld,conditionType,conditionValArr,targetArr,actionType);

					} return ; }

					}
				</SCRIPT>
			</HEAD>
			<BODY id="forms" onFocus="callActionScript();">
				<xsl:apply-templates select="rowset" />
			</BODY>
		</HTML>
	</xsl:template>
	<xsl:template match="rowset">
		<Form name="er_fillform1" method="post"
			action="updateFormData.jsp"
			onSubmit="return validate(document.er_fillform1);">
			<table class="dynFormTable" width="100%" border="0">
				<tr></tr>
			</table>
			<P class="dynSection">Primary Diagnosis</P>
			<table class="dynFormTable" width="100%" border="0">
				<tr>
					<td>
						<label for="er_def_date_01">
							Data Entry Date
							<font class="mandatory">*</font>
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="er_def_date_01" name="er_def_date_01" class="datefield"
							readonly="true" type="text" size="10"
							onChange="performAction(document.er_fillform1,document.er_fillform1.er_def_date_01)">
							<xsl:attribute name="value">
								<xsl:value-of select="er_def_date_01" />
							</xsl:attribute>
						</input>
						
					</td>
					<td width='15%' align="left">
						<label>
							&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;
						</label>
						&#xa0;&#xa0;&#xa0;
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%" border="0">
				<tr></tr>
				<tr>
					<td>
						<label for="fld10050_15574_18145">
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Primary
							Seq No.&amp;lt;/font&amp;gt;
							<font class="mandatory">*</font>
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="fld10050_15574_18145"
							name="fld10050_15574_18145" type="text" maxlength="10" size="10"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15574_18145)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15574_18145" />
							</xsl:attribute>
						</input>
					</td>
					<td>
						<label
							onmouseover="return overlib('Date diagnosed by this facility.  This date is used statistically for studying outcomes.',CAPTION,'Help');"
							onmouseout="return nd();" for="fld10050_15575_18146">
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Primary Dx
							Date&amp;lt;/font&amp;gt;
							<font class="mandatory">*</font>
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input
							onmouseover="return overlib('Date diagnosed by this facility.  This date is used statistically for studying outcomes.',CAPTION,'Help');"
							onmouseout="return nd();" id="fld10050_15575_18146"
							name="fld10050_15575_18146" class="datefield" readonly="true" type="text" size="10"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15575_18146)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15575_18146" />
							</xsl:attribute>
						</input>
						
					</td>
					<td width='15%' align="left">
						<label>
							&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;
						</label>
						&#xa0;&#xa0;&#xa0;
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%" border="0">
				<tr></tr>
				<tr>
					<td>
						<label>
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Diagnosis
							Hospital&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<select id="fld10050_15576_18147"
							name="fld10050_15576_18147"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15576_18147)">
							<option>
								<xsl:attribute name="value"></xsl:attribute>
								Select an Option
							</option>
							<xsl:for-each
								select="fld10050_15576_18147/resp">
								<option>
									<xsl:attribute name="value">
										<xsl:value-of select="@dataval" />
									</xsl:attribute>
									<xsl:if test="@selected='1'">
										<xsl:attribute name="selected" />
									</xsl:if>
									<xsl:value-of select="@dispval" />
								</option>
							</xsl:for-each>
						</select>
					</td>
					<td>
						<label>
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Clinic (if
							Providence)&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<select id="fld10050_15597_18169"
							name="fld10050_15597_18169"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15597_18169)">
							<option>
								<xsl:attribute name="value"></xsl:attribute>
								Select an Option
							</option>
							<xsl:for-each
								select="fld10050_15597_18169/resp">
								<option>
									<xsl:attribute name="value">
										<xsl:value-of select="@dataval" />
									</xsl:attribute>
									<xsl:if test="@selected='1'">
										<xsl:attribute name="selected" />
									</xsl:if>
									<xsl:value-of select="@dispval" />
								</option>
							</xsl:for-each>
						</select>
					</td>
					<td>
						<label
							onmouseover="return overlib('If not diagnosed at a UMHS affiliated site, enter the facility here, if known.',CAPTION,'Help');"
							onmouseout="return nd();" for="fld10050_15577_18148">
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;If Other,
							where?&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input
							onmouseover="return overlib('If not diagnosed at a UMHS affiliated site, enter the facility here, if known.',CAPTION,'Help');"
							onmouseout="return nd();" id="fld10050_15577_18148"
							name="fld10050_15577_18148" type="text" maxlength="60" size="60"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15577_18148)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15577_18148" />
							</xsl:attribute>
						</input>
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%" border="0">
				<tr></tr>
				<tr>
					<xsl:variable name="colcount"
						select="fld10050_15579_18150/@colcount" />
					<td width='15%' align="left">
						<label>
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Laterality&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
					</td>
					<td>
						<table width="100%" border="0">
							<tr>
								<td>
									<table width="100%">
										<xsl:for-each
											select="fld10050_15579_18150/resp">
											<td width="20%">
												<input
													name="fld10050_15579_18150" type="radio"
													onClick="performAction(document.er_fillform1,document.er_fillform1.fld10050_15579_18150)">
													<xsl:attribute
														name="value">
														<xsl:value-of
															select="@dataval" />
													</xsl:attribute>
													<xsl:attribute
														name="id">
														<xsl:value-of
															select="@dispval" />
													</xsl:attribute>
													<xsl:if
														test="@checked='1'">
														<xsl:attribute
															name="checked" />
													</xsl:if>
													<xsl:value-of
														select="@dispval" />
												</input>
											</td>
											<xsl:if
												test="number(position() mod $colcount)=0">
												<tr></tr>
											</xsl:if>
										</xsl:for-each>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td width='15%' align="left">
						<label>
							&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;
						</label>
						&#xa0;&#xa0;&#xa0;
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%" border="0">
				<tr></tr>
				<tr>
					<td>
						<label for="fld10050_15570_18141">
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Site
							Code&amp;lt;/font&amp;gt;
							<font class="mandatory">*</font>
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="fld10050_15570_18141"
							name="fld10050_15570_18141" readonly="true" type="text"
							maxlength="8" size="8"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15570_18141)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15570_18141" />
							</xsl:attribute>
						</input>
						<A href="Javascript:void(0)"
							onClick="return openlookup(1000001,'','fld10050_15570_18141|icd-o-3-site_code~fld10050_15581_18152|icd-o-3-site_desc','L')">
							Select Dx Site
						</A>
						&#xa0;&#xa0;&#xa0;
					</td>
					<td>
						<label for="fld10050_15581_18152">
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Diagnosis
							Site&amp;lt;/font&amp;gt;
							<font class="mandatory">*</font>
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="fld10050_15581_18152"
							name="fld10050_15581_18152" readonly="true" type="text"
							maxlength="100" size="85"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15581_18152)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15581_18152" />
							</xsl:attribute>
						</input>
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%" border="0">
				<tr></tr>
				<tr>
					<td>
						<label for="fld10050_15571_18142">
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Hist
							Code&amp;lt;/font&amp;gt;
							<font class="mandatory">*</font>
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="fld10050_15571_18142"
							name="fld10050_15571_18142" readonly="true" type="text"
							maxlength="8" size="8"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15571_18142)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15571_18142" />
							</xsl:attribute>
						</input>
						<A href="Javascript:void(0)"
							onClick="return openlookup(1000000,'','fld10050_15571_18142|icd-o-3-hist_code~fld10050_15582_18153|icd-o-3-hist_desc','L')">
							Select Histology
						</A>
						&#xa0;&#xa0;&#xa0;
					</td>
					<td>
						<label for="fld10050_15582_18153">
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Histology&amp;lt;/font&amp;gt;
							<font class="mandatory">*</font>
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="fld10050_15582_18153"
							name="fld10050_15582_18153" readonly="true" type="text"
							maxlength="100" size="85"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15582_18153)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15582_18153" />
							</xsl:attribute>
						</input>
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%" border="0">
				<tr></tr>
				<tr>
					<td width='100%' align="left">
						<label>
							&lt;strong&gt;&lt;font
							color=&amp;quot;#0000ff&amp;quot;
							size=&amp;quot;2&amp;quot;&gt;Stage/Grade&lt;/font&gt;&lt;/strong&gt;
						</label>
						&#xa0;&#xa0;&#xa0;
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%" border="0">
				<tr></tr>
				<tr>
					<xsl:variable name="colcount"
						select="fld10050_15584_18155/@colcount" />
					<td>
						<label>
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Staged?&amp;lt;/font&amp;gt;
							<font class="mandatory">*</font>
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<table width="100%" border="0">
							<tr>
								<td>
									<table width="100%">
										<xsl:for-each
											select="fld10050_15584_18155/resp">
											<td width="33%">
												<input
													name="fld10050_15584_18155" type="radio"
													onClick="performAction(document.er_fillform1,document.er_fillform1.fld10050_15584_18155)">
													<xsl:attribute
														name="value">
														<xsl:value-of
															select="@dataval" />
													</xsl:attribute>
													<xsl:attribute
														name="id">
														<xsl:value-of
															select="@dispval" />
													</xsl:attribute>
													<xsl:if
														test="@checked='1'">
														<xsl:attribute
															name="checked" />
													</xsl:if>
													<xsl:value-of
														select="@dispval" />
												</input>
											</td>
											<xsl:if
												test="number(position() mod $colcount)=0">
												<tr></tr>
											</xsl:if>
										</xsl:for-each>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td>
						<label
							onmouseover="return overlib('Summary Stage',CAPTION,'Help');"
							onmouseout="return nd();" for="fld10050_15585_18156">
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Stage&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input
							onmouseover="return overlib('Summary Stage',CAPTION,'Help');"
							onmouseout="return nd();" id="fld10050_15585_18156"
							name="fld10050_15585_18156" type="text" maxlength="5" size="5"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15585_18156)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15585_18156" />
							</xsl:attribute>
						</input>
					</td>
					<td>
						<label for="fld10050_15586_18157">
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;T&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="fld10050_15586_18157"
							name="fld10050_15586_18157" type="text" maxlength="5" size="5"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15586_18157)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15586_18157" />
							</xsl:attribute>
						</input>
					</td>
					<td>
						<label for="fld10050_15587_18158">
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;N&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="fld10050_15587_18158"
							name="fld10050_15587_18158" type="text" maxlength="5" size="5"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15587_18158)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15587_18158" />
							</xsl:attribute>
						</input>
					</td>
					<td>
						<label for="fld10050_15588_18159">
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;M&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="fld10050_15588_18159"
							name="fld10050_15588_18159" type="text" maxlength="5" size="5"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15588_18159)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15588_18159" />
							</xsl:attribute>
						</input>
					</td>
					<xsl:variable name="colcount"
						select="fld10050_15589_18160/@colcount" />
					<td>
						<label>
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Clinical/Path
							Staging?&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<table width="100%" border="0">
							<tr>
								<td>
									<table width="100%">
										<xsl:for-each
											select="fld10050_15589_18160/resp">
											<td width="33%">
												<input
													name="fld10050_15589_18160" type="radio"
													onClick="performAction(document.er_fillform1,document.er_fillform1.fld10050_15589_18160)">
													<xsl:attribute
														name="value">
														<xsl:value-of
															select="@dataval" />
													</xsl:attribute>
													<xsl:attribute
														name="id">
														<xsl:value-of
															select="@dispval" />
													</xsl:attribute>
													<xsl:if
														test="@checked='1'">
														<xsl:attribute
															name="checked" />
													</xsl:if>
													<xsl:value-of
														select="@dispval" />
												</input>
											</td>
											<xsl:if
												test="number(position() mod $colcount)=0">
												<tr></tr>
											</xsl:if>
										</xsl:for-each>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%" border="0">
				<tr></tr>
				<tr>
					<td>
						<label>
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Grade&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<select id="fld10050_15590_18161"
							name="fld10050_15590_18161"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15590_18161)">
							<option>
								<xsl:attribute name="value"></xsl:attribute>
								Select an Option
							</option>
							<xsl:for-each
								select="fld10050_15590_18161/resp">
								<option>
									<xsl:attribute name="value">
										<xsl:value-of select="@dataval" />
									</xsl:attribute>
									<xsl:if test="@selected='1'">
										<xsl:attribute name="selected" />
									</xsl:if>
									<xsl:value-of select="@dispval" />
								</option>
							</xsl:for-each>
						</select>
					</td>
					<td width='25%' align="right">
						<label>
							&lt;font
							size=&amp;quot;2&amp;quot;&gt;Additional
							Staging/Scoring:&lt;/font&gt;
						</label>
						&#xa0;&#xa0;&#xa0;
					</td>
					<td>
						<label>
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Scoring
							System&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<select id="fld10050_15592_18163"
							name="fld10050_15592_18163"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15592_18163)">
							<option>
								<xsl:attribute name="value"></xsl:attribute>
								Select an Option
							</option>
							<xsl:for-each
								select="fld10050_15592_18163/resp">
								<option>
									<xsl:attribute name="value">
										<xsl:value-of select="@dataval" />
									</xsl:attribute>
									<xsl:if test="@selected='1'">
										<xsl:attribute name="selected" />
									</xsl:if>
									<xsl:value-of select="@dispval" />
								</option>
							</xsl:for-each>
						</select>
					</td>
					<td>
						<label for="fld10050_15593_18164">
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Value&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="fld10050_15593_18164"
							name="fld10050_15593_18164" type="text" maxlength="10" size="10"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15593_18164)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15593_18164" />
							</xsl:attribute>
						</input>
					</td>
					<td width='15%' align="left">
						<label>&#xa0;</label>
						&#xa0;&#xa0;&#xa0;
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%" border="0">
				<tr></tr>
				<tr>
					<td>
						<label for="fld10050_15595_18166">
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Diagnosis
							Notes&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="fld10050_15595_18166"
							name="fld10050_15595_18166" type="text" maxlength="80" size="80"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15595_18166)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15595_18166" />
							</xsl:attribute>
						</input>
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%" border="0">
				<tr></tr>
			</table>
			<br />
			<hr />
			<br />
			<table class="dynFormTable" width="100%">
				<tr></tr>
			</table>
			<table class="dynFormTable" width="100%" border="0">
				<tr></tr>
				<tr>
					<td width='15%' align="left">
						<label>
							&lt;font color=&amp;quot;#ff0000&amp;quot;
							size=&amp;quot;3&amp;quot;&gt;&lt;strong&gt;For
							each Recurrence/Progression date, fill out a
							section below.&lt;br/&gt;
							&lt;/strong&gt;&lt;/font&gt;
						</label>
						&#xa0;&#xa0;&#xa0;
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%" border="0">
				<tr></tr>
			</table>
			<br />
			<hr />
			<br />
			<table class="dynFormTable" width="100%">
				<tr></tr>
			</table>
			<P class="dynSection">
				Progressive/Recurrent Disease (up to 10)
			</P>
			<table class="dynFormTable" width="100%" border="0">
				<tr></tr>
				<tr>
					<td>
						<label for="fld10050_15600_18172">
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Progression/Recurrence
							Date&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="fld10050_15600_18172"
							name="fld10050_15600_18172" class="datefield" readonly="true" type="text" size="10"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15600_18172)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15600_18172" />
							</xsl:attribute>
						</input>
						
					</td>
					<xsl:variable name="colcount"
						select="fld10050_15610_18182/@colcount" />
					<td>
						<label>
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Is this a
							&amp;lt;strong&amp;gt;recurrence&amp;lt;/strong&amp;gt;
							(as opposed to
							progression)?&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<table width="100%" border="0">
							<tr>
								<td>
									<table width="100%">
										<xsl:for-each
											select="fld10050_15610_18182/resp">
											<td width="33%">
												<input
													name="fld10050_15610_18182" type="radio"
													onClick="performAction(document.er_fillform1,document.er_fillform1.fld10050_15610_18182)">
													<xsl:attribute
														name="value">
														<xsl:value-of
															select="@dataval" />
													</xsl:attribute>
													<xsl:attribute
														name="id">
														<xsl:value-of
															select="@dispval" />
													</xsl:attribute>
													<xsl:if
														test="@checked='1'">
														<xsl:attribute
															name="checked" />
													</xsl:if>
													<xsl:value-of
														select="@dispval" />
												</input>
											</td>
											<xsl:if
												test="number(position() mod $colcount)=0">
												<tr></tr>
											</xsl:if>
										</xsl:for-each>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td>
						<label for="fld10050_15672_18244">
							&amp;lt;font
							color=&amp;quot;#808080&amp;quot;
							size=&amp;quot;2&amp;quot;&amp;gt;Recur
							ID&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="fld10050_15672_18244"
							name="fld10050_15672_18244" readonly="true" type="text"
							maxlength="8" size="8"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15672_18244)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15672_18244" />
							</xsl:attribute>
						</input>
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%" border="0">
				<tr></tr>
				<tr>
					<xsl:variable name="colcount"
						select="fld10050_15630_18202/@colcount" />
					<td>
						<label>
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Extent/Site
							(check all that
							apply &amp;lt;strong&amp;gt;on
							given date&amp;lt;/strong&amp;gt;)&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<table width="100%" border="0">
							<tr>
								<td>
									<table width="100%">
										<xsl:for-each
											select="fld10050_15630_18202/resp">
											<td width="25%">
												<input
													name="fld10050_15630_18202" type="checkbox"
													onClick="performAction(document.er_fillform1,document.er_fillform1.fld10050_15630_18202)">
													<xsl:attribute
														name="value">
														<xsl:value-of
															select="@dataval" />
													</xsl:attribute>
													<xsl:attribute
														name="id">
														<xsl:value-of
															select="@dispval" />
													</xsl:attribute>
													<xsl:if
														test="@checked='1'">
														<xsl:attribute
															name="checked" />
													</xsl:if>
													<xsl:value-of
														select="@dispval" />
												</input>
											</td>
											<xsl:if
												test="number(position() mod $colcount)=0">
												<tr></tr>
											</xsl:if>
										</xsl:for-each>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td>
						<label for="fld10050_15640_18212">
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;If
							&amp;lt;strong&amp;gt;Other&amp;lt;/strong&amp;gt;,
							specify&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="fld10050_15640_18212"
							name="fld10050_15640_18212" type="text" maxlength="40" size="40"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15640_18212)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15640_18212" />
							</xsl:attribute>
						</input>
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%" border="0">
				<tr></tr>
				<tr>
					<td>
						<label>
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Notes&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<textarea id="fld10050_15660_18232"
							name="fld10050_15660_18232" rows="2" cols="75" wrap="physical"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15660_18232)">
							<xsl:value-of select="fld10050_15660_18232" />
						</textarea>
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%" border="0">
				<tr></tr>
			</table>
			<br />
			<hr />
			<br />
			<table class="dynFormTable" width="100%">
				<tr></tr>
			</table>
			<table class="dynFormTable" width="100%">
				<tr>
					<td>
						<label for="fld10050_15601_18173">
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Progression/Recurrence
							Date&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="fld10050_15601_18173"
							name="fld10050_15601_18173" class="datefield" readonly="true" type="text" size="10"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15601_18173)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15601_18173" />
							</xsl:attribute>
						</input>
						
					</td>
					<xsl:variable name="colcount"
						select="fld10050_15611_18183/@colcount" />
					<td>
						<label>
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Is this a
							&amp;lt;strong&amp;gt;recurrence&amp;lt;/strong&amp;gt;
							(as opposed to
							progression)?&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<table width="100%" border="0">
							<tr>
								<td>
									<table width="100%">
										<xsl:for-each
											select="fld10050_15611_18183/resp">
											<td width="33%">
												<input
													name="fld10050_15611_18183" type="radio"
													onClick="performAction(document.er_fillform1,document.er_fillform1.fld10050_15611_18183)">
													<xsl:attribute
														name="value">
														<xsl:value-of
															select="@dataval" />
													</xsl:attribute>
													<xsl:attribute
														name="id">
														<xsl:value-of
															select="@dispval" />
													</xsl:attribute>
													<xsl:if
														test="@checked='1'">
														<xsl:attribute
															name="checked" />
													</xsl:if>
													<xsl:value-of
														select="@dispval" />
												</input>
											</td>
											<xsl:if
												test="number(position() mod $colcount)=0">
												<tr></tr>
											</xsl:if>
										</xsl:for-each>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td>
						<label for="fld10050_15673_18245">
							&amp;lt;font
							color=&amp;quot;#808080&amp;quot;
							size=&amp;quot;2&amp;quot;&amp;gt;Recur
							ID&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="fld10050_15673_18245"
							name="fld10050_15673_18245" readonly="true" type="text"
							maxlength="8" size="8"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15673_18245)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15673_18245" />
							</xsl:attribute>
						</input>
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%">
				<tr>
					<xsl:variable name="colcount"
						select="fld10050_15631_18203/@colcount" />
					<td>
						<label>
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Extent/Site
							(check all that
							apply &amp;lt;strong&amp;gt;on
							given date&amp;lt;/strong&amp;gt;)&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<table width="100%" border="0">
							<tr>
								<td>
									<table width="100%">
										<xsl:for-each
											select="fld10050_15631_18203/resp">
											<td width="25%">
												<input
													name="fld10050_15631_18203" type="checkbox"
													onClick="performAction(document.er_fillform1,document.er_fillform1.fld10050_15631_18203)">
													<xsl:attribute
														name="value">
														<xsl:value-of
															select="@dataval" />
													</xsl:attribute>
													<xsl:attribute
														name="id">
														<xsl:value-of
															select="@dispval" />
													</xsl:attribute>
													<xsl:if
														test="@checked='1'">
														<xsl:attribute
															name="checked" />
													</xsl:if>
													<xsl:value-of
														select="@dispval" />
												</input>
											</td>
											<xsl:if
												test="number(position() mod $colcount)=0">
												<tr></tr>
											</xsl:if>
										</xsl:for-each>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td>
						<label for="fld10050_15641_18213">
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;If
							&amp;lt;strong&amp;gt;Other&amp;lt;/strong&amp;gt;,
							specify&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="fld10050_15641_18213"
							name="fld10050_15641_18213" type="text" maxlength="40" size="40"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15641_18213)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15641_18213" />
							</xsl:attribute>
						</input>
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%">
				<tr>
					<td>
						<label>
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Notes&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<textarea id="fld10050_15661_18233"
							name="fld10050_15661_18233" rows="2" cols="75" wrap="physical"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15661_18233)">
							<xsl:value-of select="fld10050_15661_18233" />
						</textarea>
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%">
				<tr>
					<td colspan="2">
						<br />
						<hr />
						<br />
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%">
				<tr>
					<td>
						<label for="fld10050_15602_18174">
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Progression/Recurrence
							Date&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="fld10050_15602_18174"
							name="fld10050_15602_18174" class="datefield" readonly="true" type="text" size="10"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15602_18174)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15602_18174" />
							</xsl:attribute>
						</input>
						
					</td>
					<xsl:variable name="colcount"
						select="fld10050_15612_18184/@colcount" />
					<td>
						<label>
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Is this a
							&amp;lt;strong&amp;gt;recurrence&amp;lt;/strong&amp;gt;
							(as opposed to
							progression)?&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<table width="100%" border="0">
							<tr>
								<td>
									<table width="100%">
										<xsl:for-each
											select="fld10050_15612_18184/resp">
											<td width="33%">
												<input
													name="fld10050_15612_18184" type="radio"
													onClick="performAction(document.er_fillform1,document.er_fillform1.fld10050_15612_18184)">
													<xsl:attribute
														name="value">
														<xsl:value-of
															select="@dataval" />
													</xsl:attribute>
													<xsl:attribute
														name="id">
														<xsl:value-of
															select="@dispval" />
													</xsl:attribute>
													<xsl:if
														test="@checked='1'">
														<xsl:attribute
															name="checked" />
													</xsl:if>
													<xsl:value-of
														select="@dispval" />
												</input>
											</td>
											<xsl:if
												test="number(position() mod $colcount)=0">
												<tr></tr>
											</xsl:if>
										</xsl:for-each>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td>
						<label for="fld10050_15674_18246">
							&amp;lt;font
							color=&amp;quot;#808080&amp;quot;
							size=&amp;quot;2&amp;quot;&amp;gt;Recur
							ID&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="fld10050_15674_18246"
							name="fld10050_15674_18246" readonly="true" type="text"
							maxlength="8" size="8"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15674_18246)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15674_18246" />
							</xsl:attribute>
						</input>
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%">
				<tr>
					<xsl:variable name="colcount"
						select="fld10050_15632_18204/@colcount" />
					<td>
						<label>
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Extent/Site
							(check all that
							apply &amp;lt;strong&amp;gt;on
							given date&amp;lt;/strong&amp;gt;)&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<table width="100%" border="0">
							<tr>
								<td>
									<table width="100%">
										<xsl:for-each
											select="fld10050_15632_18204/resp">
											<td width="25%">
												<input
													name="fld10050_15632_18204" type="checkbox"
													onClick="performAction(document.er_fillform1,document.er_fillform1.fld10050_15632_18204)">
													<xsl:attribute
														name="value">
														<xsl:value-of
															select="@dataval" />
													</xsl:attribute>
													<xsl:attribute
														name="id">
														<xsl:value-of
															select="@dispval" />
													</xsl:attribute>
													<xsl:if
														test="@checked='1'">
														<xsl:attribute
															name="checked" />
													</xsl:if>
													<xsl:value-of
														select="@dispval" />
												</input>
											</td>
											<xsl:if
												test="number(position() mod $colcount)=0">
												<tr></tr>
											</xsl:if>
										</xsl:for-each>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td>
						<label for="fld10050_15642_18214">
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;If
							&amp;lt;strong&amp;gt;Other&amp;lt;/strong&amp;gt;,
							specify&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="fld10050_15642_18214"
							name="fld10050_15642_18214" type="text" maxlength="40" size="40"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15642_18214)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15642_18214" />
							</xsl:attribute>
						</input>
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%">
				<tr>
					<td>
						<label>
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Notes&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<textarea id="fld10050_15662_18234"
							name="fld10050_15662_18234" rows="2" cols="75" wrap="physical"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15662_18234)">
							<xsl:value-of select="fld10050_15662_18234" />
						</textarea>
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%">
				<tr>
					<td colspan="2">
						<br />
						<hr />
						<br />
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%">
				<tr>
					<td>
						<label for="fld10050_15603_18175">
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Progression/Recurrence
							Date&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="fld10050_15603_18175"
							name="fld10050_15603_18175" class="datefield" readonly="true" type="text" size="10"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15603_18175)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15603_18175" />
							</xsl:attribute>
						</input>
						
					</td>
					<xsl:variable name="colcount"
						select="fld10050_15613_18185/@colcount" />
					<td>
						<label>
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Is this a
							&amp;lt;strong&amp;gt;recurrence&amp;lt;/strong&amp;gt;
							(as opposed to
							progression)?&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<table width="100%" border="0">
							<tr>
								<td>
									<table width="100%">
										<xsl:for-each
											select="fld10050_15613_18185/resp">
											<td width="33%">
												<input
													name="fld10050_15613_18185" type="radio"
													onClick="performAction(document.er_fillform1,document.er_fillform1.fld10050_15613_18185)">
													<xsl:attribute
														name="value">
														<xsl:value-of
															select="@dataval" />
													</xsl:attribute>
													<xsl:attribute
														name="id">
														<xsl:value-of
															select="@dispval" />
													</xsl:attribute>
													<xsl:if
														test="@checked='1'">
														<xsl:attribute
															name="checked" />
													</xsl:if>
													<xsl:value-of
														select="@dispval" />
												</input>
											</td>
											<xsl:if
												test="number(position() mod $colcount)=0">
												<tr></tr>
											</xsl:if>
										</xsl:for-each>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td>
						<label for="fld10050_15675_18247">
							&amp;lt;font
							color=&amp;quot;#808080&amp;quot;
							size=&amp;quot;2&amp;quot;&amp;gt;Recur
							ID&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="fld10050_15675_18247"
							name="fld10050_15675_18247" readonly="true" type="text"
							maxlength="8" size="8"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15675_18247)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15675_18247" />
							</xsl:attribute>
						</input>
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%">
				<tr>
					<xsl:variable name="colcount"
						select="fld10050_15633_18205/@colcount" />
					<td>
						<label>
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Extent/Site
							(check all that
							apply &amp;lt;strong&amp;gt;on
							given date&amp;lt;/strong&amp;gt;)&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<table width="100%" border="0">
							<tr>
								<td>
									<table width="100%">
										<xsl:for-each
											select="fld10050_15633_18205/resp">
											<td width="25%">
												<input
													name="fld10050_15633_18205" type="checkbox"
													onClick="performAction(document.er_fillform1,document.er_fillform1.fld10050_15633_18205)">
													<xsl:attribute
														name="value">
														<xsl:value-of
															select="@dataval" />
													</xsl:attribute>
													<xsl:attribute
														name="id">
														<xsl:value-of
															select="@dispval" />
													</xsl:attribute>
													<xsl:if
														test="@checked='1'">
														<xsl:attribute
															name="checked" />
													</xsl:if>
													<xsl:value-of
														select="@dispval" />
												</input>
											</td>
											<xsl:if
												test="number(position() mod $colcount)=0">
												<tr></tr>
											</xsl:if>
										</xsl:for-each>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td>
						<label for="fld10050_15643_18215">
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;If
							&amp;lt;strong&amp;gt;Other&amp;lt;/strong&amp;gt;,
							specify&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="fld10050_15643_18215"
							name="fld10050_15643_18215" type="text" maxlength="40" size="40"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15643_18215)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15643_18215" />
							</xsl:attribute>
						</input>
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%">
				<tr>
					<td>
						<label>
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Notes&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<textarea id="fld10050_15663_18235"
							name="fld10050_15663_18235" rows="2" cols="75" wrap="physical"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15663_18235)">
							<xsl:value-of select="fld10050_15663_18235" />
						</textarea>
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%">
				<tr>
					<td colspan="2">
						<br />
						<hr />
						<br />
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%">
				<tr>
					<td>
						<label for="fld10050_15604_18176">
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Progression/Recurrence
							Date&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="fld10050_15604_18176"
							name="fld10050_15604_18176" class="datefield" readonly="true" type="text" size="10"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15604_18176)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15604_18176" />
							</xsl:attribute>
						</input>
						
					</td>
					<xsl:variable name="colcount"
						select="fld10050_15614_18186/@colcount" />
					<td>
						<label>
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Is this a
							&amp;lt;strong&amp;gt;recurrence&amp;lt;/strong&amp;gt;
							(as opposed to
							progression)?&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<table width="100%" border="0">
							<tr>
								<td>
									<table width="100%">
										<xsl:for-each
											select="fld10050_15614_18186/resp">
											<td width="33%">
												<input
													name="fld10050_15614_18186" type="radio"
													onClick="performAction(document.er_fillform1,document.er_fillform1.fld10050_15614_18186)">
													<xsl:attribute
														name="value">
														<xsl:value-of
															select="@dataval" />
													</xsl:attribute>
													<xsl:attribute
														name="id">
														<xsl:value-of
															select="@dispval" />
													</xsl:attribute>
													<xsl:if
														test="@checked='1'">
														<xsl:attribute
															name="checked" />
													</xsl:if>
													<xsl:value-of
														select="@dispval" />
												</input>
											</td>
											<xsl:if
												test="number(position() mod $colcount)=0">
												<tr></tr>
											</xsl:if>
										</xsl:for-each>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td>
						<label for="fld10050_15676_18248">
							&amp;lt;font
							color=&amp;quot;#808080&amp;quot;
							size=&amp;quot;2&amp;quot;&amp;gt;Recur
							ID&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="fld10050_15676_18248"
							name="fld10050_15676_18248" readonly="true" type="text"
							maxlength="8" size="8"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15676_18248)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15676_18248" />
							</xsl:attribute>
						</input>
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%">
				<tr>
					<xsl:variable name="colcount"
						select="fld10050_15634_18206/@colcount" />
					<td>
						<label>
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Extent/Site
							(check all that
							apply &amp;lt;strong&amp;gt;on
							given date&amp;lt;/strong&amp;gt;)&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<table width="100%" border="0">
							<tr>
								<td>
									<table width="100%">
										<xsl:for-each
											select="fld10050_15634_18206/resp">
											<td width="25%">
												<input
													name="fld10050_15634_18206" type="checkbox"
													onClick="performAction(document.er_fillform1,document.er_fillform1.fld10050_15634_18206)">
													<xsl:attribute
														name="value">
														<xsl:value-of
															select="@dataval" />
													</xsl:attribute>
													<xsl:attribute
														name="id">
														<xsl:value-of
															select="@dispval" />
													</xsl:attribute>
													<xsl:if
														test="@checked='1'">
														<xsl:attribute
															name="checked" />
													</xsl:if>
													<xsl:value-of
														select="@dispval" />
												</input>
											</td>
											<xsl:if
												test="number(position() mod $colcount)=0">
												<tr></tr>
											</xsl:if>
										</xsl:for-each>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td>
						<label for="fld10050_15644_18216">
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;If
							&amp;lt;strong&amp;gt;Other&amp;lt;/strong&amp;gt;,
							specify&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="fld10050_15644_18216"
							name="fld10050_15644_18216" type="text" maxlength="40" size="40"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15644_18216)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15644_18216" />
							</xsl:attribute>
						</input>
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%">
				<tr>
					<td>
						<label>
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Notes&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<textarea id="fld10050_15664_18236"
							name="fld10050_15664_18236" rows="2" cols="75" wrap="physical"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15664_18236)">
							<xsl:value-of select="fld10050_15664_18236" />
						</textarea>
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%">
				<tr>
					<td colspan="2">
						<br />
						<hr />
						<br />
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%">
				<tr>
					<td>
						<label for="fld10050_15605_18177">
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Progression/Recurrence
							Date&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="fld10050_15605_18177"
							name="fld10050_15605_18177" type="text" size="10" class="datefield" readonly="true"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15605_18177)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15605_18177" />
							</xsl:attribute>
						</input>
						
					</td>
					<xsl:variable name="colcount"
						select="fld10050_15615_18187/@colcount" />
					<td>
						<label>
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Is this a
							&amp;lt;strong&amp;gt;recurrence&amp;lt;/strong&amp;gt;
							(as opposed to
							progression)?&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<table width="100%" border="0">
							<tr>
								<td>
									<table width="100%">
										<xsl:for-each
											select="fld10050_15615_18187/resp">
											<td width="33%">
												<input
													name="fld10050_15615_18187" type="radio"
													onClick="performAction(document.er_fillform1,document.er_fillform1.fld10050_15615_18187)">
													<xsl:attribute
														name="value">
														<xsl:value-of
															select="@dataval" />
													</xsl:attribute>
													<xsl:attribute
														name="id">
														<xsl:value-of
															select="@dispval" />
													</xsl:attribute>
													<xsl:if
														test="@checked='1'">
														<xsl:attribute
															name="checked" />
													</xsl:if>
													<xsl:value-of
														select="@dispval" />
												</input>
											</td>
											<xsl:if
												test="number(position() mod $colcount)=0">
												<tr></tr>
											</xsl:if>
										</xsl:for-each>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td>
						<label for="fld10050_15677_18249">
							&amp;lt;font
							color=&amp;quot;#808080&amp;quot;
							size=&amp;quot;2&amp;quot;&amp;gt;Recur
							ID&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="fld10050_15677_18249"
							name="fld10050_15677_18249" readonly="true" type="text"
							maxlength="8" size="8"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15677_18249)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15677_18249" />
							</xsl:attribute>
						</input>
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%">
				<tr>
					<xsl:variable name="colcount"
						select="fld10050_15635_18207/@colcount" />
					<td>
						<label>
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Extent/Site
							(check all that
							apply &amp;lt;strong&amp;gt;on
							given date&amp;lt;/strong&amp;gt;)&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<table width="100%" border="0">
							<tr>
								<td>
									<table width="100%">
										<xsl:for-each
											select="fld10050_15635_18207/resp">
											<td width="25%">
												<input
													name="fld10050_15635_18207" type="checkbox"
													onClick="performAction(document.er_fillform1,document.er_fillform1.fld10050_15635_18207)">
													<xsl:attribute
														name="value">
														<xsl:value-of
															select="@dataval" />
													</xsl:attribute>
													<xsl:attribute
														name="id">
														<xsl:value-of
															select="@dispval" />
													</xsl:attribute>
													<xsl:if
														test="@checked='1'">
														<xsl:attribute
															name="checked" />
													</xsl:if>
													<xsl:value-of
														select="@dispval" />
												</input>
											</td>
											<xsl:if
												test="number(position() mod $colcount)=0">
												<tr></tr>
											</xsl:if>
										</xsl:for-each>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td>
						<label for="fld10050_15645_18217">
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;If
							&amp;lt;strong&amp;gt;Other&amp;lt;/strong&amp;gt;,
							specify&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="fld10050_15645_18217"
							name="fld10050_15645_18217" type="text" maxlength="40" size="40"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15645_18217)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15645_18217" />
							</xsl:attribute>
						</input>
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%">
				<tr>
					<td>
						<label>
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Notes&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<textarea id="fld10050_15665_18237"
							name="fld10050_15665_18237" rows="2" cols="75" wrap="physical"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15665_18237)">
							<xsl:value-of select="fld10050_15665_18237" />
						</textarea>
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%">
				<tr>
					<td colspan="2">
						<br />
						<hr />
						<br />
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%">
				<tr>
					<td>
						<label for="fld10050_15606_18178">
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Progression/Recurrence
							Date&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="fld10050_15606_18178"
							name="fld10050_15606_18178" type="text" size="10" class="datefield" readonly="true"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15606_18178)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15606_18178" />
							</xsl:attribute>
						</input>
						
					</td>
					<xsl:variable name="colcount"
						select="fld10050_15616_18188/@colcount" />
					<td>
						<label>
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Is this a
							&amp;lt;strong&amp;gt;recurrence&amp;lt;/strong&amp;gt;
							(as opposed to
							progression)?&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<table width="100%" border="0">
							<tr>
								<td>
									<table width="100%">
										<xsl:for-each
											select="fld10050_15616_18188/resp">
											<td width="33%">
												<input
													name="fld10050_15616_18188" type="radio"
													onClick="performAction(document.er_fillform1,document.er_fillform1.fld10050_15616_18188)">
													<xsl:attribute
														name="value">
														<xsl:value-of
															select="@dataval" />
													</xsl:attribute>
													<xsl:attribute
														name="id">
														<xsl:value-of
															select="@dispval" />
													</xsl:attribute>
													<xsl:if
														test="@checked='1'">
														<xsl:attribute
															name="checked" />
													</xsl:if>
													<xsl:value-of
														select="@dispval" />
												</input>
											</td>
											<xsl:if
												test="number(position() mod $colcount)=0">
												<tr></tr>
											</xsl:if>
										</xsl:for-each>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td>
						<label for="fld10050_15678_18250">
							&amp;lt;font
							color=&amp;quot;#808080&amp;quot;
							size=&amp;quot;2&amp;quot;&amp;gt;Recur
							ID&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="fld10050_15678_18250"
							name="fld10050_15678_18250" readonly="true" type="text"
							maxlength="8" size="8"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15678_18250)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15678_18250" />
							</xsl:attribute>
						</input>
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%">
				<tr>
					<xsl:variable name="colcount"
						select="fld10050_15636_18208/@colcount" />
					<td>
						<label>
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Extent/Site
							(check all that
							apply &amp;lt;strong&amp;gt;on
							given date&amp;lt;/strong&amp;gt;)&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<table width="100%" border="0">
							<tr>
								<td>
									<table width="100%">
										<xsl:for-each
											select="fld10050_15636_18208/resp">
											<td width="25%">
												<input
													name="fld10050_15636_18208" type="checkbox"
													onClick="performAction(document.er_fillform1,document.er_fillform1.fld10050_15636_18208)">
													<xsl:attribute
														name="value">
														<xsl:value-of
															select="@dataval" />
													</xsl:attribute>
													<xsl:attribute
														name="id">
														<xsl:value-of
															select="@dispval" />
													</xsl:attribute>
													<xsl:if
														test="@checked='1'">
														<xsl:attribute
															name="checked" />
													</xsl:if>
													<xsl:value-of
														select="@dispval" />
												</input>
											</td>
											<xsl:if
												test="number(position() mod $colcount)=0">
												<tr></tr>
											</xsl:if>
										</xsl:for-each>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td>
						<label for="fld10050_15646_18218">
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;If
							&amp;lt;strong&amp;gt;Other&amp;lt;/strong&amp;gt;,
							specify&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="fld10050_15646_18218"
							name="fld10050_15646_18218" type="text" maxlength="40" size="40"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15646_18218)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15646_18218" />
							</xsl:attribute>
						</input>
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%">
				<tr>
					<td>
						<label>
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Notes&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<textarea id="fld10050_15666_18238"
							name="fld10050_15666_18238" rows="2" cols="75" wrap="physical"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15666_18238)">
							<xsl:value-of select="fld10050_15666_18238" />
						</textarea>
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%">
				<tr>
					<td colspan="2">
						<br />
						<hr />
						<br />
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%">
				<tr>
					<td>
						<label for="fld10050_15607_18179">
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Progression/Recurrence
							Date&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="fld10050_15607_18179"
							name="fld10050_15607_18179" type="text" size="10" class="datefield" readonly="true"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15607_18179)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15607_18179" />
							</xsl:attribute>
						</input>
						
					</td>
					<xsl:variable name="colcount"
						select="fld10050_15617_18189/@colcount" />
					<td>
						<label>
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Is this a
							&amp;lt;strong&amp;gt;recurrence&amp;lt;/strong&amp;gt;
							(as opposed to
							progression)?&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<table width="100%" border="0">
							<tr>
								<td>
									<table width="100%">
										<xsl:for-each
											select="fld10050_15617_18189/resp">
											<td width="33%">
												<input
													name="fld10050_15617_18189" type="radio"
													onClick="performAction(document.er_fillform1,document.er_fillform1.fld10050_15617_18189)">
													<xsl:attribute
														name="value">
														<xsl:value-of
															select="@dataval" />
													</xsl:attribute>
													<xsl:attribute
														name="id">
														<xsl:value-of
															select="@dispval" />
													</xsl:attribute>
													<xsl:if
														test="@checked='1'">
														<xsl:attribute
															name="checked" />
													</xsl:if>
													<xsl:value-of
														select="@dispval" />
												</input>
											</td>
											<xsl:if
												test="number(position() mod $colcount)=0">
												<tr></tr>
											</xsl:if>
										</xsl:for-each>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td>
						<label for="fld10050_15679_18251">
							&amp;lt;font
							color=&amp;quot;#808080&amp;quot;
							size=&amp;quot;2&amp;quot;&amp;gt;Recur
							ID&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="fld10050_15679_18251"
							name="fld10050_15679_18251" readonly="true" type="text"
							maxlength="8" size="8"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15679_18251)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15679_18251" />
							</xsl:attribute>
						</input>
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%">
				<tr>
					<xsl:variable name="colcount"
						select="fld10050_15637_18209/@colcount" />
					<td>
						<label>
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Extent/Site
							(check all that
							apply &amp;lt;strong&amp;gt;on
							given date&amp;lt;/strong&amp;gt;)&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<table width="100%" border="0">
							<tr>
								<td>
									<table width="100%">
										<xsl:for-each
											select="fld10050_15637_18209/resp">
											<td width="25%">
												<input
													name="fld10050_15637_18209" type="checkbox"
													onClick="performAction(document.er_fillform1,document.er_fillform1.fld10050_15637_18209)">
													<xsl:attribute
														name="value">
														<xsl:value-of
															select="@dataval" />
													</xsl:attribute>
													<xsl:attribute
														name="id">
														<xsl:value-of
															select="@dispval" />
													</xsl:attribute>
													<xsl:if
														test="@checked='1'">
														<xsl:attribute
															name="checked" />
													</xsl:if>
													<xsl:value-of
														select="@dispval" />
												</input>
											</td>
											<xsl:if
												test="number(position() mod $colcount)=0">
												<tr></tr>
											</xsl:if>
										</xsl:for-each>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td>
						<label for="fld10050_15647_18219">
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;If
							&amp;lt;strong&amp;gt;Other&amp;lt;/strong&amp;gt;,
							specify&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="fld10050_15647_18219"
							name="fld10050_15647_18219" type="text" maxlength="40" size="40"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15647_18219)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15647_18219" />
							</xsl:attribute>
						</input>
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%">
				<tr>
					<td>
						<label>
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Notes&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<textarea id="fld10050_15667_18239"
							name="fld10050_15667_18239" rows="2" cols="75" wrap="physical"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15667_18239)">
							<xsl:value-of select="fld10050_15667_18239" />
						</textarea>
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%">
				<tr>
					<td colspan="2">
						<br />
						<hr />
						<br />
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%">
				<tr>
					<td>
						<label for="fld10050_15608_18180">
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Progression/Recurrence
							Date&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="fld10050_15608_18180"
							name="fld10050_15608_18180" type="text" size="10" class="datefield" readonly="true"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15608_18180)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15608_18180" />
							</xsl:attribute>
						</input>
						
					</td>
					<xsl:variable name="colcount"
						select="fld10050_15618_18190/@colcount" />
					<td>
						<label>
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Is this a
							&amp;lt;strong&amp;gt;recurrence&amp;lt;/strong&amp;gt;
							(as opposed to
							progression)?&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<table width="100%" border="0">
							<tr>
								<td>
									<table width="100%">
										<xsl:for-each
											select="fld10050_15618_18190/resp">
											<td width="33%">
												<input
													name="fld10050_15618_18190" type="radio"
													onClick="performAction(document.er_fillform1,document.er_fillform1.fld10050_15618_18190)">
													<xsl:attribute
														name="value">
														<xsl:value-of
															select="@dataval" />
													</xsl:attribute>
													<xsl:attribute
														name="id">
														<xsl:value-of
															select="@dispval" />
													</xsl:attribute>
													<xsl:if
														test="@checked='1'">
														<xsl:attribute
															name="checked" />
													</xsl:if>
													<xsl:value-of
														select="@dispval" />
												</input>
											</td>
											<xsl:if
												test="number(position() mod $colcount)=0">
												<tr></tr>
											</xsl:if>
										</xsl:for-each>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td>
						<label for="fld10050_15680_18252">
							&amp;lt;font
							color=&amp;quot;#808080&amp;quot;
							size=&amp;quot;2&amp;quot;&amp;gt;Recur
							ID&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="fld10050_15680_18252"
							name="fld10050_15680_18252" readonly="true" type="text"
							maxlength="8" size="8"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15680_18252)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15680_18252" />
							</xsl:attribute>
						</input>
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%">
				<tr>
					<xsl:variable name="colcount"
						select="fld10050_15638_18210/@colcount" />
					<td>
						<label>
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Extent/Site
							(check all that
							apply &amp;lt;strong&amp;gt;on
							given date&amp;lt;/strong&amp;gt;)&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<table width="100%" border="0">
							<tr>
								<td>
									<table width="100%">
										<xsl:for-each
											select="fld10050_15638_18210/resp">
											<td width="25%">
												<input
													name="fld10050_15638_18210" type="checkbox"
													onClick="performAction(document.er_fillform1,document.er_fillform1.fld10050_15638_18210)">
													<xsl:attribute
														name="value">
														<xsl:value-of
															select="@dataval" />
													</xsl:attribute>
													<xsl:attribute
														name="id">
														<xsl:value-of
															select="@dispval" />
													</xsl:attribute>
													<xsl:if
														test="@checked='1'">
														<xsl:attribute
															name="checked" />
													</xsl:if>
													<xsl:value-of
														select="@dispval" />
												</input>
											</td>
											<xsl:if
												test="number(position() mod $colcount)=0">
												<tr></tr>
											</xsl:if>
										</xsl:for-each>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td>
						<label for="fld10050_15648_18220">
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;If
							&amp;lt;strong&amp;gt;Other&amp;lt;/strong&amp;gt;,
							specify&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="fld10050_15648_18220"
							name="fld10050_15648_18220" type="text" maxlength="40" size="40"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15648_18220)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15648_18220" />
							</xsl:attribute>
						</input>
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%">
				<tr>
					<td>
						<label>
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Notes&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<textarea id="fld10050_15668_18240"
							name="fld10050_15668_18240" rows="2" cols="75" wrap="physical"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15668_18240)">
							<xsl:value-of select="fld10050_15668_18240" />
						</textarea>
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%">
				<tr>
					<td colspan="2">
						<br />
						<hr />
						<br />
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%">
				<tr>
					<td>
						<label for="fld10050_15609_18181">
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Progression/Recurrence
							Date&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="fld10050_15609_18181"
							name="fld10050_15609_18181" type="text" size="10" class="datefield" readonly="true"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15609_18181)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15609_18181" />
							</xsl:attribute>
						</input>
						
					</td>
					<xsl:variable name="colcount"
						select="fld10050_15619_18191/@colcount" />
					<td>
						<label>
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Is this a
							&amp;lt;strong&amp;gt;recurrence&amp;lt;/strong&amp;gt;
							(as opposed to
							progression)?&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<table width="100%" border="0">
							<tr>
								<td>
									<table width="100%">
										<xsl:for-each
											select="fld10050_15619_18191/resp">
											<td width="33%">
												<input
													name="fld10050_15619_18191" type="radio"
													onClick="performAction(document.er_fillform1,document.er_fillform1.fld10050_15619_18191)">
													<xsl:attribute
														name="value">
														<xsl:value-of
															select="@dataval" />
													</xsl:attribute>
													<xsl:attribute
														name="id">
														<xsl:value-of
															select="@dispval" />
													</xsl:attribute>
													<xsl:if
														test="@checked='1'">
														<xsl:attribute
															name="checked" />
													</xsl:if>
													<xsl:value-of
														select="@dispval" />
												</input>
											</td>
											<xsl:if
												test="number(position() mod $colcount)=0">
												<tr></tr>
											</xsl:if>
										</xsl:for-each>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td>
						<label for="fld10050_15681_18253">
							&amp;lt;font
							color=&amp;quot;#808080&amp;quot;
							size=&amp;quot;2&amp;quot;&amp;gt;Recur
							ID&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="fld10050_15681_18253"
							name="fld10050_15681_18253" readonly="true" type="text"
							maxlength="8" size="8"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15681_18253)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15681_18253" />
							</xsl:attribute>
						</input>
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%">
				<tr>
					<xsl:variable name="colcount"
						select="fld10050_15639_18211/@colcount" />
					<td>
						<label>
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Extent/Site
							(check all that
							apply &amp;lt;strong&amp;gt;on
							given date&amp;lt;/strong&amp;gt;)&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<table width="100%" border="0">
							<tr>
								<td>
									<table width="100%">
										<xsl:for-each
											select="fld10050_15639_18211/resp">
											<td width="25%">
												<input
													name="fld10050_15639_18211" type="checkbox"
													onClick="performAction(document.er_fillform1,document.er_fillform1.fld10050_15639_18211)">
													<xsl:attribute
														name="value">
														<xsl:value-of
															select="@dataval" />
													</xsl:attribute>
													<xsl:attribute
														name="id">
														<xsl:value-of
															select="@dispval" />
													</xsl:attribute>
													<xsl:if
														test="@checked='1'">
														<xsl:attribute
															name="checked" />
													</xsl:if>
													<xsl:value-of
														select="@dispval" />
												</input>
											</td>
											<xsl:if
												test="number(position() mod $colcount)=0">
												<tr></tr>
											</xsl:if>
										</xsl:for-each>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td>
						<label for="fld10050_15649_18221">
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;If
							&amp;lt;strong&amp;gt;Other&amp;lt;/strong&amp;gt;,
							specify&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<input id="fld10050_15649_18221"
							name="fld10050_15649_18221" type="text" maxlength="40" size="40"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15649_18221)">
							<xsl:attribute name="value">
								<xsl:value-of
									select="fld10050_15649_18221" />
							</xsl:attribute>
						</input>
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%">
				<tr>
					<td>
						<label>
							&amp;lt;font
							size=&amp;quot;2&amp;quot;&amp;gt;Notes&amp;lt;/font&amp;gt;
						</label>
						&#xa0;&#xa0;&#xa0;
						<BR />
						<textarea id="fld10050_15669_18241"
							name="fld10050_15669_18241" rows="2" cols="75" wrap="physical"
							onChange="performAction(document.er_fillform1,document.er_fillform1.fld10050_15669_18241)">
							<xsl:value-of select="fld10050_15669_18241" />
						</textarea>
					</td>
				</tr>
			</table>
			<table class="dynFormTable" width="100%">
				<tr>
					<td colspan="2">
						<br />
						<hr />
						<br />
					</td>
				</tr>
			</table>
			<script>
				function callActionScript() { { var srcArr = new
				Array(); var targetArr = new Array(); var
				conditionValArr = new Array(); var conditionType; var
				actionType; var fld; fld =
				document.er_fillform1.fld10050_15576_18147 ;
				conditionType = "velEquals" ; actionType = "disabled" ;
				targetArr[0] =
				document.er_fillform1.fld10050_15577_18148 ;
				conditionValArr[0] = "UMHS" ; conditionValArr[1] = "Prov
				- Sfld" ; conditionValArr[2] = "Prov - Novi" ;
				conditionValArr[3] = "VA" ; conditionValArr[4] = "Foote"
				; conditionValArr[5] = "Ingham" ; conditionValArr[6] =
				"Alpena" ; conditionValArr[7] = "CMCH" ;
				conditionValArr[8] = "Unknown" ;


				changeFieldStateMultiple(fld,conditionType,conditionValArr,targetArr,actionType);
				}{ var srcArr = new Array(); var targetArr = new
				Array(); var conditionValArr = new Array(); var
				conditionType; var actionType; var fld; fld =
				document.er_fillform1.fld10050_15576_18147 ;
				conditionType = "velEquals" ; actionType = "disabled" ;
				targetArr[0] =
				document.er_fillform1.fld10050_15597_18169 ;
				conditionValArr[0] = "UMHS" ; conditionValArr[1] = "VA"
				; conditionValArr[2] = "Foote" ; conditionValArr[3] =
				"Ingham" ; conditionValArr[4] = "Alpena" ;
				conditionValArr[5] = "CMCH" ; conditionValArr[6] =
				"Other" ; conditionValArr[7] = "Unknown" ;


				changeFieldStateMultiple(fld,conditionType,conditionValArr,targetArr,actionType);
				}{ var srcArr = new Array(); var targetArr = new
				Array(); var conditionValArr = new Array(); var
				conditionType; var actionType; var fld; fld =
				document.er_fillform1.fld10050_15584_18155 ;
				conditionType = "velEquals" ; actionType = "disabled" ;
				targetArr[0] =
				document.er_fillform1.fld10050_15585_18156 ;
				targetArr[1] =
				document.er_fillform1.fld10050_15586_18157 ;
				targetArr[2] =
				document.er_fillform1.fld10050_15587_18158 ;
				targetArr[3] =
				document.er_fillform1.fld10050_15588_18159 ;
				targetArr[4] =
				document.er_fillform1.fld10050_15589_18160 ;
				conditionValArr[0] = "No" ; conditionValArr[1] =
				"Unknown" ;


				changeFieldStateMultiple(fld,conditionType,conditionValArr,targetArr,actionType);
				}{ var srcArr = new Array(); var targetArr = new
				Array(); var conditionValArr = new Array(); var
				conditionType; var actionType; var fld; fld =
				document.er_fillform1.fld10050_15592_18163 ;
				conditionType = "velNotEquals" ; actionType = "disabled"
				; targetArr[0] =
				document.er_fillform1.fld10050_15593_18164 ;
				conditionValArr[0] = "FIGO" ; conditionValArr[1] =
				"DUKES" ; conditionValArr[2] = "GLEASON" ;
				conditionValArr[3] = "NWTS" ; conditionValArr[4] = "ANN
				ARBOR" ;


				changeFieldStateMultiple(fld,conditionType,conditionValArr,targetArr,actionType);
				} } callActionScript();
			</script>
			<br />
			<table class="dynFormTable" width="100%">
				<tr>
					<td width="15%">
						e-Signature
						<FONT class="Mandatory">*</FONT>
					</td>
					<td width="30%">
						<input type="password" name="eSign"
							maxlength="8" autocomplete="off" />
					</td>
					<td width="15%">
						<input type="image"
							src="../images/jpg/Submit.gif" align="absmiddle" border="0" />
					</td>
				</tr>
			</table>
			<input type="hidden" name="override_count" value="0" />
		</Form>
	</xsl:template>
</xsl:stylesheet>
