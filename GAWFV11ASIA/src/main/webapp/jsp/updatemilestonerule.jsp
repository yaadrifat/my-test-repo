<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML> 
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
 </HEAD>
 <jsp:include page="skinChoser.jsp" flush="true"/>
 
 <BODY> 
 <%@ page language = "java" import = "com.velos.eres.business.common.*,com.velos.eres.service.util.*,java.util.*,javax.mail.*, javax.mail.internet.*, javax.activation.*"%> <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
 <jsp:useBean id="milestoneB" scope="page" class="com.velos.eres.web.milestone.MilestoneJB" />
   <%  int ret = 0;
 	String eSign = request.getParameter("eSign");
	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))

	   {	
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%   	
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
	%>
	<jsp:include page="incorrectesign.jsp" flush="true"/>	
	<%
			} else {
  				String userId = (String) tSession.getValue("userId");
				String ipAdd = (String) tSession.getValue("ipAdd");
			 	String milestoneRuleId = request.getParameter("milestoneRuleId");
				String milestoneAmount = request.getParameter("milestoneAmount");

				
				String count="";
				String limit = "";
				String patStatus = "";
				String paymentType = "";
				String paymentFor = "";
				String eventStatus = "";
				String milestoneType = "";
				String oldMilestoneStat = "";
				String description = "";
				String milestoneStat= "";
				
				count = request.getParameter("count");
				limit = request.getParameter("mLimit");
			    patStatus = request.getParameter("dStatus");
			    paymentType = request.getParameter("dpayType");
			    paymentFor = request.getParameter("dpayFor");
				milestoneStat = request.getParameter("milestoneStat");
			    eventStatus = request.getParameter("dEventStatus");
   			    milestoneType = request.getParameter("milestoneType");
   			    String milestoneId = request.getParameter("milestoneId");	
				

				if(milestoneType.equals("AM"))
				{
					 description = request.getParameter("description");//KM-3301
					 
					 if (StringUtil.isEmpty(description ))
					 {
					 	description ="";
					 }
					 else
					 {
					 	description = description.trim(); 
					 }
				}


				milestoneB.setId(EJBUtil.stringToNum(milestoneId));		
				milestoneB.getMilestoneDetails();
				
   			    //KM-#DFIN7
				oldMilestoneStat = milestoneB.getMilestoneStatFK();
				CodeDao cdStat = new CodeDao();
				int wipPk = cdStat.getCodeId("milestone_stat","WIP");
				String wipPkStr = wipPk +"";

				int actPk = cdStat.getCodeId("milestone_stat","A");
				String actPkStr = actPk + "";

				int inactPk = cdStat.getCodeId("milestone_stat","IA");
				String inactPkStr = inactPk + "";
			    
				//KM-02Jun10-#4964
				if ( ( (!milestoneStat.equals(actPkStr) && !milestoneStat.equals(inactPkStr) )  || (!oldMilestoneStat.equals(actPkStr) &&		    !oldMilestoneStat.equals(inactPkStr))) &&  !oldMilestoneStat.equals(actPkStr) )
				{
	    			if (StringUtil.isEmpty(count))
	    			{
	    				count = "-1";
	    			}
	    			
					if(milestoneAmount.equals(".") || milestoneAmount.equalsIgnoreCase("") || milestoneAmount.equalsIgnoreCase("null")){
						milestoneAmount="0";
					}
				
			 
					if (milestoneType.equals("PM"))
				    {
					    milestoneB.setMilestoneRuleId("0");
				    }
				    else
				    {
						milestoneB.setMilestoneRuleId(milestoneRuleId);
					}
					
				
						milestoneB.setMilestoneAmount(milestoneAmount);		
						milestoneB.setMilestoneDelFlag("N");		
						milestoneB.setModifiedBy(userId);
						milestoneB.setIpAdd(ipAdd);
						
						milestoneB.setMilestoneStatFK(milestoneStat);
					    milestoneB.setMilestoneLimit( limit) ;
					    milestoneB.setMilestonePayFor( paymentFor) ;
					    milestoneB.setMilestonePayType( paymentType) ;
					    milestoneB.setMilestoneStatus( patStatus );
					    milestoneB.setMilestoneCount(count);
						milestoneB.setMilestoneDescription(description);//KM
					    
					    //JM: 22May2007
					    if (milestoneType.equals("EM") || milestoneType.equals("VM"))
					    {
					    	milestoneB.setMilestoneEventStatus(eventStatus);
					    }	
		  }			    
		  else
		  {			//set only pay for and status
				    milestoneB.setMilestonePayFor( paymentFor) ;				
					milestoneB.setMilestoneStatFK(milestoneStat);
					// Date:-14-Feb-2012 Bug#8554 Ankit
					milestoneB.setModifiedBy(userId);
					milestoneB.setIpAdd(ipAdd);
		  }

				ret = milestoneB.updateMilestone();
   if (ret == 0) {
%>
<SCRIPT>

   	window.opener.location.reload();
	self.close();

</SCRIPT>	
<%	
   } else {
%>   
	<br><br><br><br><br>
	<p class = "successfulmsg" align = center> <%=MC.M_DataCnt_Svd %><%-- Data could not be saved.*****--%></p>
	<br>
	<button onClick = "self.close()"><%=LC.L_Close%></button>
<%	
   }
}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY> 
</HTML>
