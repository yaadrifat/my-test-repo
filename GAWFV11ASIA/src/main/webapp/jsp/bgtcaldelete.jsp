<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@page import="com.aithent.audittrail.reports.AuditUtils,com.velos.eres.service.util.LC"%>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=LC.L_Del_PcolCal%><%--Delete Protocol Calendar*****--%></title>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
	
<SCRIPT>
function  validate(formobj){
if (!(validate_col('e-Signature',formobj.eSign))) return false;

// 	if(isNaN(formobj.eSign.value) == true) {
<%-- 	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/ --%>
// 	formobj.eSign.focus();
// 	return false;
//    }
	window.opener.location.href=window.opener.location.href;   //Fixed for Bug#7356:Akshi	vinayak
   return true;
  
}


</SCRIPT>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.*"%>
<jsp:useBean id="bgtcalB" scope="request" class="com.velos.esch.web.budgetcal.BudgetcalJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/> 
<jsp:include page="include.jsp" flush="true"/>


<BODY> 
<br>

<DIV>
<% 
	
	String bgtcalId="";	

	
	HttpSession tSession = request.getSession(true); 

	 if (sessionmaint.isValidSession(tSession))	{
  	 %>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<% 	
	
		bgtcalId=request.getParameter("bgtcalId");
		String budgetId=request.getParameter("budgetId");
		String selectedTab=request.getParameter("selectedTab");
		
		int ret=0;
		
		String delMode=request.getParameter("delMode");
		if (delMode==null) {
			delMode="final";
%>

	<FORM name="bgtcaldelete" id="bgtcaldelfrmid" method="post" action="bgtcaldelete.jsp" onSubmit="if (validate(document.bgtcaldelete)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
 	 <input type="hidden" name="delMode" value="<%=delMode%>">
  	 <input type="hidden" name="bgtcalId" value="<%=bgtcalId%>">
	 <input type="hidden" name="budgetId" value="<%=budgetId%>">
	
	<br><br>

	<P class="defComments"><%=MC.M_EsignToProc_DelCalFromBgt%><%--Please enter e-Signature to proceed with Calendar Delete from Budget.*****--%></P>

		
	<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="bgtcaldelfrmid"/>
			<jsp:param name="showDiscard" value="N"/>
	</jsp:include>
	</FORM>
<%
	} else {
			String eSign = request.getParameter("eSign");	
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>
	 		  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
			} else {	
		bgtcalB.setBudgetcalId(EJBUtil.stringToNum(bgtcalId));
		
		bgtcalB.getBudgetcalDetails();
		bgtcalB.setModifiedBy((String)tSession.getValue("userId"));
		bgtcalB.setBudgetDelFlag("Y");
		//Modified for INF-18183 ::: Akshi
		ret=bgtcalB.updateBudgetcal(AuditUtils.createArgs(session,"",LC.L_Budget));		

		%>
		<br><br><br><br><br><br>
		
		<p class = "successfulmsg" align = center> <%=MC.M_Data_DelSucc%><%--Data deleted successfully.*****--%> </p>
		<Script>
		window.opener.location.reload();
		self.close();	
		</Script>
	
			
<%	
		} //end esign
	} //end of delMode	
 }//end of if body for session 
else { %>
 <jsp:include page="timeout.html" flush="true"/> 
 <% } %>
 <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
  
</DIV>
</body>
</HTML>
