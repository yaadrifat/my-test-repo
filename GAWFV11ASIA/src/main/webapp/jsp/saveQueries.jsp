<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</HEAD>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<BODY>
<jsp:useBean id="formQuery" scope="request" class="com.velos.eres.web.formQuery.FormQueryJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import ="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*" %>
<% HttpSession tSession = request.getSession(true); 
 int iret = -1;
 int iupret = 0;
 int iformLibId=0;
 int from = 0;
 String patientId = "";
 String selectedTab = "";
 String studyId = "";
 String patProtId = "";
 String statDesc = "";
 String statid = "";
 String patientCode = "";
 String fkcrf = "";
 String schevent = "";
 //String filledFormId =request.getParameter("filledFormId");
 String filledFormId =(String)tSession.getAttribute("filledFormId");
 System.out.println("**************filledFormId"+filledFormId);
 filledFormId=(filledFormId==null)?"":filledFormId;
 String formFillMode = (String)tSession.getAttribute("formFillMode");
		String formPullDown= (String)tSession.getAttribute("formPullDown");
		String formFillDt= (String)tSession.getAttribute("formFillDt");	
  //String mode = request.getParameter("formFillMode");
  String mode = formFillMode;
 
 //String formDispLocation = request.getParameter("formDispLocation");
 String formDispLocation =(String)tSession.getAttribute("formDispLocation");
 if (formDispLocation.equals("PA"))
		{
			patientId = (String) tSession.getAttribute("formPatient");
			selectedTab =(String) tSession.getAttribute("selectedTab");						
		
		}
		else if (formDispLocation.equals("A")) //called from account
		{
			//sk.setAccountId(accountId);
		}
		else if (formDispLocation.equals("S") || formDispLocation.equals("SA"))
		{
			studyId = (String) tSession.getAttribute("formStudy");
			selectedTab =(String) tSession.getAttribute("selectedTab");						
		//	sk.setStudyId(studyId);
		}
		else if (formDispLocation.equals("SP"))
		{
			patientId = (String) tSession.getAttribute("formPatient");
			selectedTab =(String) tSession.getAttribute("selectedTab");						
			patProtId = (String) tSession.getAttribute("formPatprot");
			studyId = (String) tSession.getAttribute("patStudyId");
			statDesc = (String) tSession.getAttribute("statDesc");
			statid = (String) tSession.getAttribute("statid");
			patientCode = (String) tSession.getAttribute("patientCode");		
			//sk.setPatprotId(patProtId);
			//sk.setPatientId(patientId);									
		}
	    else if (formDispLocation.equals("C")){
			fkcrf = (String) tSession.getAttribute("fkcrf");
			schevent = (String) tSession.getAttribute("schevent");			
			patientId = (String) tSession.getAttribute("patientId");
			statDesc = (String) tSession.getAttribute("statDesc");
			statid = (String) tSession.getAttribute("statid");
			patProtId = (String) tSession.getAttribute("patProtId");		
			patientCode = (String) tSession.getAttribute("patientCode");			
			studyId = (String) tSession.getAttribute("formStudy");
			
			//sk.setFkCrf(fkcrf);
			//sk.setSchEvent(schevent);
		}
 
 
 
 
 String calledFrom =request.getParameter("calledFrom");
 int openId = 0;
 if(calledFrom.equals("PA"))
 	 from = 1;
 if(calledFrom.equals("S") || calledFrom.equals("SA"))
 	 from = 2;
 if(calledFrom.equals("A"))
 	 from = 3;
 if(calledFrom.equals("SP"))
 	 from = 4;
 if(calledFrom.equals("C"))
 	 from = 5;
 
 if(mode.equals("N")){
 	 		CodeDao cd = new CodeDao();
			openId = cd.getCodeId("query_status", "open");
	}
 
  
  String src = (String) tSession.getAttribute("srcmenu");
	
  
  String[] fieldSysId =request.getParameterValues("fieldSysId");
  int len = fieldSysId.length;
  
  String[] fldValidation =request.getParameterValues("fldValidation");
  
  String[] reasons =request.getParameterValues("reasons");
  
  String[] stat = new String[len];
 
 
  
  
  String eSign = request.getParameter("eSign");
  for(int i=0;i<len;i++)
  	  {
  	  	   
  	  	
  	  	     if(mode.equals("N"))
  	  	     	 stat[i] = String.valueOf(openId);
  	  	  
  	  }
  
 


 String accId = (String) tSession.getValue("accountId");

 String ipAdd = (String) tSession.getValue("ipAdd");

 String usr = (String) tSession.getValue("userId");
 



 if (sessionmaint.isValidSession(tSession)) {

%>
	
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%	
String oldESign = (String) tSession.getValue("eSign");

	if(!oldESign.equals(eSign)) {

%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} 
else
	{%>

	  


	<%int ret = 0;
	

	if(mode.equals("M"))
    	{
	/*	iformLibId=Integer.parseInt(formLibId);
		formlibB.setFormLibId(iformLibId);
		formlibB.getFormLibDetails();
		formlibB.setFormLibName(name);
		formlibB.setCatLibId(type);
		formlibB.setFormLibDesc(desc);
		formlibB.setFormLibStatus(status);
		formlibB.setIpAdd(ipAdd);
		formlibB.setCreator(usr);
		formlibB.setRecordType(mode);
		formlibB.setLastModifiedBy(usr);
		
		/*else
			{
			formlibB.setFormLibSharedWith("");
			formlibB.setSharedWithIds("");
			}*/
		//set the refresh flag to 1 indicating that the xsl needs to be refreshed
	/*	formlibB.setFormLibXslRefresh("1");
		iupret=formlibB.updateFormLib();*/
		}
  else
		{
			
	/*insertIntoFormQuery(int filledFormId, int calledFrom, int queryType, 
			String[] queryTypeIds, String[] fldSysIds, String[] notes, String[] status,
			int creator, String ipAdd)*/
		formQuery.insertIntoFormQuery(EJBUtil.stringToNum(filledFormId), from,1,reasons, fieldSysId, fldValidation, stat, EJBUtil.stringToNum(usr), ipAdd);

    /*formQueryB.setIpAdd(ipAdd);
	formQueryB.setCreator(usr);
	formQueryB.setFormLibName(name);
	formQueryB.setFormLibDesc(desc);
	formQueryB.setCatLibId(type);
	formlibB.setRecordType(mode);
	formlibB.setLastModifiedBy(usr);
	formlibB.setFormLibStatus(status);
	formlibB.setAccountId(accId);*/


	
}
		tSession.removeAttribute("formDispLocation");
		tSession.removeAttribute("formFillMode");
		tSession.removeAttribute("formId");
		tSession.removeAttribute("formFilledFormId");
		tSession.removeAttribute("srcmenu");
		tSession.removeAttribute("formFillDt");
		tSession.removeAttribute("formLibVer");	
		tSession.removeAttribute("filledFormId");
		
		if (formDispLocation.equals("PA"))
		{
			tSession.removeAttribute("formPatient");			
			tSession.removeAttribute("selectedTab");						
			tSession.removeAttribute("formPullDown");					
	//	if(cnt == 0){	%>
		    <META HTTP-EQUIV=Refresh CONTENT="2; URL=formfilledpatbrowser.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&pkey=<%=patientId%>&patProtId=<%=patProtId%>&calledFrom=S&statDesc=<%=statDesc%>&statid=<%=statid%>&patientCode=<%=patientCode%>&formFillDt=<%=formFillDt%>&formPullDown=<%=formPullDown%>">
		<%//}
		}
		else if(formDispLocation.equals("S") || formDispLocation.equals("SA") )
		{
			tSession.removeAttribute("formStudy");
			tSession.removeAttribute("selectedTab");						
			tSession.removeAttribute("formPullDown");				
		//if(cnt == 0){	%>
			<META HTTP-EQUIV=Refresh CONTENT="2; URL=formfilledstudybrowser.jsp?srcmenu=<%=src%>&studyId=<%=studyId%>&selectedTab=<%=selectedTab%>&formFillDt=<%=formFillDt%>&formPullDown=<%=formPullDown%>">
		<%//}
		}
		else if (formDispLocation.equals("A"))//called from Account
		{
		//if(cnt == 0){%> 
			<META HTTP-EQUIV=Refresh CONTENT="2; URL=formfilledaccountbrowser.jsp?srcmenu=<%=src%>&formFillDt=<%=formFillDt%>&formPullDown=<%=formPullDown%>"> 
		<%//}
		} 
		else if (formDispLocation.equals("SP"))
		{
			tSession.removeAttribute("formPatient");
			tSession.removeAttribute("formPatprot");
			tSession.removeAttribute("patStudyId");
			tSession.removeAttribute("statid");
			tSession.removeAttribute("statDesc");
			tSession.removeAttribute("selectedTab");
			tSession.removeAttribute("patientCode");		
			tSession.removeAttribute("formPullDown");						
		//if(cnt == 0){%>
		
		<META HTTP-EQUIV=Refresh CONTENT="2; URL=formfilledstdpatbrowser.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&pkey=<%=patientId%>&mode=M&patProtId=<%=patProtId%>&studyId=<%=studyId%>&calledFrom=S&statDesc=<%=statDesc%>&statid=<%=statid%>&patientCode=<%=patientCode%>&formFillDt=<%=formFillDt%>&formPullDown=<%=formPullDown%>">
		<%//}
		}
	    //added by salil for crf forms
		else if (formDispLocation.equals("C"))
		{
		 	tSession.removeAttribute("fkcrf");
		 	tSession.removeAttribute("schevent");
		 	tSession.removeAttribute("pkey");
		 	tSession.removeAttribute("statDesc");
		 	tSession.removeAttribute("statid");
		 	tSession.removeAttribute("patProtId");
		 	tSession.removeAttribute("patientCode");
		 	tSession.removeAttribute("formStudy");																			
	//	if(cnt == 0){%>
		
		<META HTTP-EQUIV=Refresh CONTENT="2; URL=patientschedule.jsp?srcmenu=tdMenuBarItem5&selectedTab=3&mode=M&pkey=<%=patientId%>&patProtId=<%=patProtId%>&patientCode=<%=patientCode%>&page=patientEnroll&studyId=<%=studyId%>&generate=N&statDesc=<%=statDesc%>&statid=<%=statid%>&studyVer=null">	
		<%//}
		}

	
	
		 if (iformLibId == -3 )
			{%>
			<br><br><br><br><br>
			<p class = "sectionHeadings" align = center > <%=MC.M_UnqFrmName_EtrUnqFrmName%><%--The unique form name already exists. Please enter a different unique form name*****--%></p>
			<center><button tabindex=2 onClick="history.go(-1);"><%=LC.L_Back%></button>
		<%
		} else if(iupret != 0 ||  iformLibId < 0 ) {
		%> 
		<br><br><br><br><br>
			<p class = "sectionHeadings" align = center> <%=MC.M_Data_NotSvdSucc%><%--Data not saved successfully*****--%> </p>
			<button name="back" onclick="window.history.back();"><%=LC.L_Back%></button></p>
		<% return;
		} 
	
	else {%>
		<br><br><br><br><br><p class = "sectionHeadings" align = center>
		<%=MC.M_Data_SavedSucc%><%--Data saved successfully*****--%></p>	
		<script>
		window.close();
		</script>
		
	<%}
	}
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

</HTML>

