<%@page import="com.velos.eres.web.network.NetworkJB"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<jsp:include page="velos_includes.jsp"></jsp:include>
<html>
<head>
	<% if(!"networkTabs".equals(request.getParameter("src")) && !"networkStudyTab".equals(request.getParameter("src"))){%>
	<title><%=MC.M_MngAcc_OrgDets%><%--Manage Account >> Organization Details*****--%></title>
	<%}else{%>
	<title>Network>> Organization Details<%--Manage Account >> Organization Details*****--%></title>
	<%} %>
	<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
	<jsp:useBean id="siteB" scope="request" class="com.velos.eres.web.site.SiteJB"/>
	<jsp:useBean id="addressB" scope="request" class="com.velos.eres.web.address.AddressJB"/>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<%@ page language = "java" import = "com.velos.eres.service.util.*,com.velos.eres.business.common.*"%>

<% 
	String srcmenu;
		srcmenu = request.getParameter("srcmenu");
	String src="";
		src= request.getParameter("src")==null?"":request.getParameter("src");
if(!"networkTabs".equals(src) && !"networkStudyTab".equals(src)){%>
<jsp:include page="panel.jsp" flush="true"> 
	<jsp:param name="src" value="<%=srcmenu%>"/>
</jsp:include>
<%}else{%>
<jsp:include page="include.jsp" flush="true"/>
	<%-- Nicholas : Start --%>
	<jsp:include page="skinChoser.jsp" flush="true"/>
	<%-- Nicholas : End --%>
<%} %>
<SCRIPT Language="javascript">

var screenWidth = screen.width;
var screenHeight = screen.height;

// Added by Gopu for September Enhancement #U3
	function  validate(formobj) {		
		var parentOrg = formobj.siteParent.options[formobj.siteParent.selectedIndex].value;		
		var siteIdOrg = formobj.siteId.value;
		if (siteIdOrg == parentOrg) {
			alert("<%=MC.M_CntSameOrg_AsParentOrg%>");/*alert("Cannot assign the same Organization as a Parent Organization");*****/
			formobj.siteParent.focus();
			return false;
		}
	//     formobj=document.site
//		if (!(validate_col('e-Signature',formobj.eSign))) return false
//		if(isNaN(formobj.eSign.value) == true) {
//			alert("Incorrect e-Signature. Please enter again");
//			formobj.eSign.focus();
//			return false;
//		}

		if (!(validate_col('Organization Name',formobj.siteName))) return false   
		if (!(validate_col('Type',formobj.siteType))) return false
		//document.site.submit();
	}
	// Added by Amar for Bugzilla issue # 3026	
		function checkinput(obj,max){
		var result = true;
		if (obj.value.length >= max){
		result = false;
		}
		if (window.event)
		window.event.returnValue = result;
		return result;	
	} 
   function lookupPOOrganization(Pkval,DisVal){	    
			windowName=window.open("getlookup.jsp?viewId=&viewName=PO Organization&form=sitefrm&dfilter=PoOrganization&keyword="+Pkval+"|ORGANIZATION_PO_ID~"+DisVal+"|ORGANIZATION_PO_ID","Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=550 top=100,left=100");
			windowName.focus();
	}

   function openformwin(siteId){
		windowform = window.open('formfilledaccountbrowser.jsp?modpk='+siteId+'&srcmenu=tdMenuBarItem1&commonformflag=ORG', '_blank','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=900,height=700,left=10,top=100');
		windowform.focus();
		
	}
   
   function sitesAppendix(networkId,siteId,pageRight){
		var siteName=document.getElementById("siteName").value;
		var siteType=document.getElementById("siteType").value;
		windowName =window.open('eventappendix.jsp?networkId='+networkId+'&siteId='+siteId+'&siteName='+siteName+'&siteType='+siteType+'&pageRight='+pageRight+'&calledFrom=Site&fromPage=siteDetails&networkFlag=Org_doc&selectedTab=1&mode=M', 'Information', 'toolbar=no,scrollbar=yes,resizable=yes,menubar=no,status=yes,width=1100,height=600, top=100, left=100');
		  windowName.focus();
		}

</SCRIPT>
<%if(!"networkTabs".equals(src) && !"networkStudyTab".equals(src)){ %>
<DIV class="tabDefTopN" id="divTab"> 
	<jsp:include page="accounttabs.jsp" flush="true"> 
	<jsp:param name="selectedTab" value="1"/>
	</jsp:include>
</DIV>
<SCRIPT LANGUAGE="JavaScript">
	if(screenWidth>1280 || screenHeight>1024)
		document.write('<DIV class="tabDefBotN" id="div1" style="height:80%;">')
	else
		document.write('<DIV class="tabDefBotN" id="div1">')
</SCRIPT>
<%}else{ %>	
<SCRIPT LANGUAGE="JavaScript">
	if(screenWidth>1280 || screenHeight>1024)
		document.write('<DIV class="tabDefBotN" id="div1" style="height:95%; top: 5px;">')
	else
		document.write('<DIV class="tabDefBotN" id="div1" style="top: 5px;">')
</SCRIPT>
<%}
	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession)) {
		String uName = (String) tSession.getValue("userName");	
        String mode = "";
		String accId = (String) tSession.getValue("accountId");	
		String ddSiteParent = null;
		CodeDao cdSite = new CodeDao();
		cdSite.getAccountSites(Integer.parseInt(accId)); 
		mode = request.getParameter("mode");
		int pageRight = 0;
		GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MSITES"));
// change by salil 
	if ((mode.equals("M") && pageRight >=4) || (mode.equals("N") && (pageRight == 5 || pageRight == 7 ))) {
		int siteId=0;  
		int siteAddId=0;
		int modname=1;
		String siteName = "";
		String ctepid = "";
		String siteType = "";
		String siteInfo = "";
		String siteParent = "";
		String siteStatus = "";
		String siteAddress = "";
		String siteCity = "";
		String siteState = "";
		String siteZip = "";
		String siteCountry = "";
		String sitePhone = "";
		String siteEmail = "";
		String siteNotes = "";   // Amarnadh  
		String siteAccount = "";
		String siteIdentifier = "";	
		String poIdentifier = "";
		String siteHidden = "";//KM
		siteId = EJBUtil.stringToNum(request.getParameter("siteId"));
		String parentNtwId = (request.getParameter("parentNtwId")==null)?"0":request.getParameter("parentNtwId");
		String nLevel = (request.getParameter("nLevel")==null)?"":request.getParameter("nLevel");
		String networkId = (request.getParameter("networkId")==null)?"0":request.getParameter("networkId");
		String site = request.getParameter("siteParentOrg");	//gopu
		String dSiteType = "" ;
		CodeDao cdType = new CodeDao();
		cdType.getCodeValues("site_type"); 
		if (mode.equals("M")) {
			siteB.setSiteId(siteId);
			siteB.getSiteDetails();
			siteId = siteB.getSiteId();
			siteName = siteB.getSiteName();
			ctepid = siteB.getCtepId();

			if(StringUtil.isEmpty(ctepid)){
				ctepid="";
			}

			siteStatus = siteB.getSiteStatus();
			
			if (StringUtil.isEmpty(siteStatus))
			{
				siteStatus="";
			}

		/*Modified by Amarnadh ,Added an attribute siteNotes for June Enhancement '07  #U7 */

			siteNotes = siteB.getSiteNotes();

			siteIdentifier = siteB.getSiteIdentifier();
			poIdentifier = siteB.getPoIdentifier();
			poIdentifier=StringUtil.escapeSpecialCharHTML(poIdentifier);
			siteHidden = siteB.getSiteHidden(); //KM
			
				if (StringUtil.isEmpty(siteIdentifier)) {
					siteIdentifier = "";
				}	
				if (StringUtil.isEmpty(poIdentifier)) {
					poIdentifier = "";
				}
				siteType = siteB.getSiteCodelstType();
				siteInfo = siteB.getSiteInfo();
				siteInfo = (   siteInfo  == null      )?"":(  siteInfo ) ;	 
				siteParent = siteB.getSiteParent();
				dSiteType = cdType.toPullDown("siteType",EJBUtil.stringToNum(siteType));
				ddSiteParent = cdSite.toPullDown("siteParent",EJBUtil.stringToNum(siteParent));	
				siteAddId= EJBUtil.stringToNum(siteB.getSitePerAdd()); 
				addressB.setAddId(siteAddId);
				addressB.getAddressDetails();
				siteAddress = addressB.getAddPri();
				siteAddress =   (   siteAddress   == null      )?"":(  siteAddress  ) ;	
				siteCity = addressB.getAddCity();
				siteCity =   (   siteCity   == null      )?"":(  siteCity  ) ;	
				siteState = addressB.getAddState();
				siteState =   (   siteState   == null      )?"":(  siteState  ) ;	
				siteZip = addressB.getAddZip();
				siteZip =   (   siteZip   == null      )?"":(  siteZip  ) ;	
				siteCountry = addressB.getAddCountry();
				siteCountry =   (   siteCountry   == null      )?"":(  siteCountry  ) ;	
				sitePhone = addressB.getAddPhone();
				sitePhone =   (   sitePhone   == null      )?"":(  sitePhone  ) ;	
				siteEmail = addressB.getAddEmail();
				siteEmail =   (   siteEmail   == null      )?"":(  siteEmail  ) ;	

				
				siteNotes =  ( siteNotes == null  )?" " : (siteNotes ) ;

			
				siteAccount = siteB.getSiteAccountId();
				
	   } else {

			ddSiteParent = cdSite.toPullDown("siteParent");	
			dSiteType = cdType.toPullDown("siteType");

		  } 
%>
	<Form name="site" id="sitefrm" method="post" action="updatesite.jsp" onsubmit ="if (validate(document.site)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	 <%if (mode.equals("M") && (src.equalsIgnoreCase("") || src.equals("networkTabs"))) {%>
	   <P class = "sectionHeadings" style="height:20px">&nbsp;&nbsp;&nbsp;
	   <img title="Appendix" src="./images/Appendix.gif" style="cursor: pointer;position: absolute; right: 45px" onclick="sitesAppendix('<%=siteId %>','<%=siteId %>','<%=pageRight%>')" border="0" right="0">
	   <img title="Forms" src="./images/Form.gif" style="cursor: pointer;position: absolute; right: 18px" onclick="openformwin(<%=siteId%>)" border="0"  right="0">
		</P>
		<%} %>
		<div style="border:1">
		<div id="orgDetailsDIV" class="portlet portletstatus ui-widget ui-widget-border ui-helper-clearfix ui-corner-all">
					<div id="orgDetailsTab1content" onclick="toggleDiv('orgDetailsTab1')" class="portlet-header portletstatus ui-widget 
					ui-widget-header ui-widget-content ui-helper-clearfix ui-corner-all" style="background-color:#CCCCCC;">
						<span class="ui-icon ui-icon-triangle-1-s"></span>
			<%=LC.L_Org_Dets%>
			
		</div>
				<div id='orgDetailsTab1'  width="99%">	
		<!-- Added by Gopu for September Enhancement'06 #U3 -->
		<input type="hidden" name="siteParentOrg" value ="<%=site%>">
		
		<!--Commented by Gopu for September Enhancement #U3 -->
	<!--input type="text" name="siteId" Value="<%=siteId%>"-->
	<input type="hidden" name="siteAddId" Value="<%=siteAddId%>">
	<input type="hidden" name="mode" Value="<%=mode%>">
	
		<table width="98%" cellspacing="0" cellpadding="0">
			<tr>
				<td width="20%" align="left"> <%=LC.L_Organization_Name%><%--Organization Name*****--%> <FONT class="Mandatory">* </FONT> </td>
				<td  align="left"> 
					<input type="text" name="siteName" id="siteName" size = 50 MAXLENGTH = 50 Value="<%=siteName.replace("\"","''")%>">
				</td>
			</tr>
			<tr> 
		        <td  align="left"> <%=LC.L_Type%><%--Type*****--%> <FONT class="Mandatory">* </FONT> </td>
				<td  align="left"> <%=dSiteType%> 
				<input type="hidden" name="siteTypeH" id="siteTypeH" value="<%=dSiteType%>"></td>
			</tr>
			<tr> 
				<td align="left"> <%=LC.L_Description%><%--Description*****--%> </td>
				<td  align="left"> 
					<input type="text" name="siteInfo" size = 50 MAXLENGTH = 2000 Value="<%=siteInfo%>">
		        </td>
			</tr>
			<tr> 
				<td  align="left"><%=LC.L_Parent_Org%><%--Parent Organization*****--%> </td>
		        <td  align="left"> <%=ddSiteParent%></td>					
			</tr>
			<tr> 
				<td  align="left"> <%=LC.L_Site_Id%><%--Site ID*****--%></td>
	    	    <td  align="left"><%--Yogendra Pratap : Modified for ACC-22253: Increase the front-end limit for SITE ID field to 50 :14 March 2012 --%>
					<input type="text" name="siteIdentifier" size = 50 MAXLENGTH = 50 Value="<%=siteIdentifier%>">
		        </td>
			</tr>
			<tr> 
				<td  align="left"> <%=LC.L_NCI_POID%><%--NCI PO-ID*****--%></td>
	    	    <td  align="left">
					<input type="text" name="poIdentifier" id="poIdentifier" size = 50 MAXLENGTH = 50 Value="<%=poIdentifier%>">&nbsp;&nbsp;&nbsp;
					<a href="javascript:lookupPOOrganization('poIdentifier','poIdentifier');" ><%=LC.L_Select %></a>
		        </td>
			</tr>
			<tr> 
				<td  align="left"> <%=LC.L_Address%><%--Address*****--%> </td>
			    <td  align="left"> 
					<input type="text" name="siteAddress" size = 50 MAXLENGTH = 50 Value="<%=siteAddress%>">
				</td>				
			</tr>
			<tr> 
				<td  align="left"> <%=LC.L_City%><%--City*****--%> </td>
				<td  align="left"> 
				  <input type="text" name="siteCity" size = 35  MAXLENGTH = 30 Value="<%=siteCity%>">
				</td>
			</tr>
			<tr> 
				<td  align="left"> <%=LC.L_State%><%--State*****--%> </td>
		        <td  align="left"> 
					<input type="text" name="siteState" size = 35 MAXLENGTH = 30 Value="<%=siteState%>">
				</td>
			</tr>
			<tr> 
				<td  align="left"><%=LC.L_ZipOrPostal_Code%><%--Zip/ Postal Code*****--%> </td>
				<td  align="left"> 
					<input type="text" name="siteZip" size = 35 MAXLENGTH = 15 Value="<%=siteZip%>">
				</td>
			</tr>
			<tr> 
				<td  align="left"> <%=LC.L_Country%><%--Country*****--%> </td>
				<td  align="left"> 
					<input type="text" name="siteCountry" size = 35 MAXLENGTH = 30 Value="<%=siteCountry%>">
				</td>
			</tr>
			<tr>
				<td  align="left">  <%=LC.L_Contact_Phone%><%--Contact Phone*****--%> </td>
				<td  align="left"> 
					<input type="text" name="sitePhone" size = 35 MAXLENGTH = 100 Value="<%=sitePhone%>">
				</td>
			</tr>
			<tr> 
				<td  align="left">  <%=LC.L_Contact_Email%><%--Contact E-Mail*****--%> </td>
		        <td  align="left"> 
					<input type="text" name="siteEmail" size = 35 MAXLENGTH = 100 Value="<%=siteEmail%>">
				</td>
			</tr>

			<!--Modified by Amarnadh ,Added an attribute siteNotes for June Enhancement '07  #U7 -->
			<tr>

				<td  align="left">  <%=LC.L_Notes%><%--Notes*****--%> </td>
			    <td  align="left"width="60%"> <TEXTAREA name="siteNotes"  rows=3 cols=50 MAXLENGTH=256 onkeyup="if(this.value.length > 256) this.value=this.value.substr(0,256)" onkeypress="checkinput(this,255)" onclick="checkinput(this,256)"><%=siteNotes%></TEXTAREA> </td>
			</tr>  
			<tr>
			<td  align="left"><%=LC.L_CTEP_ID%></td>
			<td  align="left"><input type="text" id="ctepId" name="ctepId" size = 35 MAXLENGTH = 100  Value="<%=ctepid%>"></td>
			</tr>

	   <% 
	   	//Added by Manimaran for Enh.#U11
		  String checkStr ="";
	    if (siteHidden.equals("1"))
		   checkStr = "checked";
	
   	    if (mode.equals("M")){ %>
	       <tr><td colspan="2"  align="left"> <input type="checkbox" name="siteHidden" <%=checkStr%>> <%=MC.M_HideOrg_InOrgLkUp%><%--Hide this Organization in Organization and User selection Lookups*****--%></td></tr>
	   <%}%>
			<tr>   
				<td  align="left"> 
					<input type="hidden" name="siteAccount" MAXLENGTH = 15 Value="<%=siteAccount%>">
				</td>
			</tr>
			<tr> 
				<td  align="left"> 
					<input type="hidden" name="siteId" MAXLENGTH = 15 Value="<%=siteId%>">
					<input type="hidden" name="networkId" Value="<%=networkId%>">
					<input type="hidden" name="siteStatus" MAXLENGTH = 15 Value="<%=siteStatus%>">
					<input type="hidden" name="srcmenu" Value="<%=srcmenu%>">
					<input type="hidden" name="src" Value="<%=src%>">
					<input type="hidden" name="nLevel" Value="<%=nLevel%>">
					<input type="hidden" name="parentNtwId" Value="<%=parentNtwId%>">
				</td>
			</tr>
		</table>
		</div>
		</div>
<div id="orgMoreDetailsDIV" class="portlet portletstatus ui-widget ui-widget-border ui-helper-clearfix ui-corner-all">
		<div id="orgDetailsTab2content" onclick="toggleDiv('orgDetailsTab2');" class="portlet-header portletstatus ui-widget 
		ui-widget-header ui-widget-content ui-helper-clearfix ui-corner-all" style="background-color:#CCCCCC;">
			<span class="ui-icon ui-icon-triangle-1-s"></span>
		<%=LC.L_More_OrgDets%>
		</div>
		<div id='orgDetailsTab2'  width="99%">
			<jsp:include page="moredetails.jsp" flush="true">
				<jsp:param name="modId" value="<%=siteId%>"/>
				<jsp:param name="modName" value="org"/>
				<jsp:param name="includeMode" value="Y"/>
			</jsp:include>
		</div>
</div>
<%if(!"".equals(nLevel)) {
NetworkJB nJB = new NetworkJB();
nJB.setNetworkId(StringUtil.stringToInteger(networkId));
nJB.getNetworkDetails();
CodeDao cd1 = new CodeDao();
cd1.getCodeValues("networkstat");
String dSiteStat="";
dSiteStat = cd1.toPullDown("ntSiteStat",StringUtil.stringToInteger(nJB.getNetworkStatusId()),"disabled='disabled'");
%>
<input type="hidden" name="ntSiteStat"  Value="<%=StringUtil.stringToInteger(nJB.getNetworkStatusId())%>">
<div id="orgMoreDetailsDIV<%=nLevel%>" class="portlet portletstatus ui-widget ui-widget-border ui-helper-clearfix ui-corner-all">
		<div id="org_<%=nLevel%>content" onclick="toggleDiv('org_<%=nLevel%>');" class="portlet-header portletstatus ui-widget 
		ui-widget-header ui-widget-content ui-helper-clearfix ui-corner-all" style="background-color:#CCCCCC;">
			<span class="ui-icon ui-icon-triangle-1-s"></span>
		More Organization Network Details Level <%=StringUtil.stringToInteger(nLevel)+1%>
		</div>
		<div id='org_<%=nLevel%>'  width="99%">
			<jsp:include page="moredetails.jsp" flush="true">
				<jsp:param name="modId" value="<%=networkId%>"/>
				<jsp:param name="modName" value="org"/>
				<jsp:param name="nwLevel" value="<%=nLevel%>"/>
				<jsp:param name="includeMode" value="Y"/>
			</jsp:include>
		</div>
</div>
<div id="siteSection3" name="siteSection" class="portlet portletstatus ui-widget ui-widget-border ui-helper-clearfix ui-corner-all">
<div>
<table width="100%" cellspacing="0" cellpadding="0" bgcolor="#cccccc" class= "basetbl" border="0">
<tr valign="middle">
<td class="tdDefault" width="12%"><%=LC.L_NtSite_Status%></td>
<td width="13%"><%=dSiteStat%></td>
</tr>
</table>
</div>
<%if("networkStudyTab".equals(src)){%>
	<div style="position:absolute; left:45%">
		<button type="button" id="submit_btn" onclick="window.close()" ondblclick="return false"><%=LC.L_Close%></button>
	</div>
<%}%>
</div>
<%} %>
		</div>
	<%	if (((mode.equals("M") && pageRight >=6) || (mode.equals("N") && (pageRight == 5 || pageRight == 7 ))) && !"networkStudyTab".equals(src)) {%>
	<br>
	<table width="98%" cellspacing="0" cellpadding="0">
	 <!--  	<td width="50%" align="right" bgcolor="#cccccc"> <span id="eSignMessage"></span>
				e-Signature <FONT class="Mandatory">* </FONT>
	   </td>

	   <td width="20%" bgcolor="#cccccc">
		<input type="password" name="eSign" id="eSign" maxlength="8" onkeyup="ajaxvalidate('misc:'+this.id,4,'eSignMessage','Valid e-Sign','Invalid e-Sign','sessUserId')">
	    </td>	-->
		<tr valign="center">
			<td width=35%>&nbsp;</td>
			<td>
				<jsp:include page="submitBar.jsp" flush="true"> 
					<jsp:param name="displayESign" value="N"/>
					<jsp:param name="formID" value="sitefrm"/>
					<jsp:param name="showDiscard" value="N"/>
					<jsp:param name="noBR" value="Y"/>
				</jsp:include>  
			</td>
		</tr>
	</table>
	<% } %>
  </Form>

<%} //end of if body for page right

else { %>

  <jsp:include page="accessdenied.jsp" flush="true"/>
<%
		} //end of else body for page right
	}//end of if body for session
	else {
%>
  <jsp:include page="timeout.html" flush="true"/>
<% } %>

	<div class = "myHomebottomPanel"> 
	<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</div>

<div class ="mainMenu" id="emenu"> 
	<jsp:include page="getmenu.jsp" flush="true"/>
</div>

</body>

</html>