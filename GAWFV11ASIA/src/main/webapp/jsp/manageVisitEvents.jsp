<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%--PCAL-20461 For loading Events under the Visit--%>

<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import ="com.velos.eres.business.group.*,java.net.URLEncoder,com.velos.eres.business.common.*,
com.velos.esch.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.StringUtil,
com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB, 
com.velos.esch.web.eventdef.EventdefJB, com.velos.esch.web.eventassoc.EventAssocJB, com.velos.eres.service.util.LC, com.velos.eres.service.util.MC"%>

<jsp:useBean id="eventlistdao" scope="request" class="com.velos.esch.business.common.EventdefDao"/>
<jsp:useBean id="assoclistdao" scope="request" class="com.velos.esch.business.common.EventAssocDao"/>
<%@page import="java.util.ArrayList"%>
<%
   HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession)){
		String protocolName = "";
		String calledFrom = request.getParameter("calledFrom");
		String calStatus = request.getParameter("calStatus");
		String dialog=request.getParameter("Dialog");
		dialog=(dialog==null)?"":dialog;
		String orderBy=request.getParameter("orderBy");
		String orderType=request.getParameter("orderType");
		boolean isLibCal = false;
		boolean isStudyCal = false;
		String eventType="";
		String overlibParameter="";
		
		eventType = "P";
		String mode = request.getParameter("mode");
		String displayType=request.getParameter("displayType");

		Integer id;
		String eventName="";
		String eventCost="";
		String catgName="";
		ArrayList eventNames= null;
		ArrayList eventCosts= null;
		ArrayList catgNames = null;
		ArrayList eventIds=null;
		ArrayList eventTypes=null;
		ArrayList eventVisitIds = null;
		ArrayList sequences = null;
		ArrayList offLineFlags = null;
		ArrayList hideFlags = null;
		ArrayList eventLibraryType = null;
		ArrayList cptCodes = null;
		 
	    
	  if ("P".equals(calledFrom) || "L".equals(calledFrom) ) {
	        isLibCal = true;
	    } else if ("S".equals(calledFrom)) {
	        isStudyCal = true;
	    } else {
	        out.println(LC.L_NoCal_Found);
	        return;
	    }
	  
	  String protocolId = request.getParameter("protocolId");
	  SchCodeDao scho = new SchCodeDao();
	  
	  String visitNameId = request.getParameter("visitList");
		if (visitNameId==null) visitNameId = "";

		String evtName = "";
		evtName =request.getParameter("searchEvt");
		evtName=(evtName==null)?"":evtName;
		
		
	    if (calledFrom.equals("P")  || calledFrom.equals("L"))
			{
			if(dialog.equals("PopUp") || dialog.equals("PopUpSorting")){
	    		   eventlistdao=eventdefB.getProtSelectedEvents(EJBUtil.stringToNum(protocolId),orderType,orderBy);
	    		   eventCosts=eventlistdao.getCosts();
	    		   catgNames = eventlistdao.getEventCategory();
			} 
	    	else{
			   	  eventlistdao= eventdefB.getAllProtSelectedVisitsEvents(EJBUtil.stringToNum(protocolId), visitNameId, evtName);
			   	  eventTypes= eventlistdao.getEvent_types();
			   	  eventVisitIds = eventlistdao.getFk_visit();
			   	  sequences = eventlistdao.getEventSequences();
			   	  cptCodes = eventlistdao.getCptCodes();
			   	  eventLibraryType = eventlistdao.getEventLibraryType();
	    	    }
			   eventNames= eventlistdao.getNames();
			   eventIds=eventlistdao.getEvent_ids() ;
			}
			else if (calledFrom.equals("S"))
			{  //called From Study
			   if(dialog.equals("PopUp") ||  dialog.equals("PopUpSorting")){
					assoclistdao= eventassocB.getProtSelectedEvents(EJBUtil.stringToNum(protocolId), orderType, orderBy);	
					eventCosts=assoclistdao.getCosts();
					catgNames = assoclistdao.getEventCategory();
			   }
			   else{
			   		assoclistdao= eventassocB.getAllProtSelectedVisitsEvents(EJBUtil.stringToNum(protocolId), visitNameId, evtName);
			   	   eventVisitIds = assoclistdao.getEventVisits();
				   eventTypes= assoclistdao.getEvent_types();
				   sequences = assoclistdao.getEventSequences();
				   offLineFlags = assoclistdao.getOfflineFlags();
				   hideFlags = assoclistdao.getHideFlags();
				   eventLibraryType = assoclistdao.getEventLibraryType();
				   cptCodes = assoclistdao.getCptCodes();
			   }
			   eventNames= assoclistdao.getNames();
			   eventIds=assoclistdao.getEvent_ids() ;	  
			}
		
	
		int noOfEvents = eventIds == null ? 0 : eventIds.size();
	   
		if(dialog.equals("PopUp") || dialog.equals("PopUpSorting")){
		    %>
		    
		    <div id='embedDataHere'><b><p class="defComments" align="center"><%=MC.M_DragEvt_ChgSeq%></p></b>
		    <table id="seq_table" border="1" width="100%" cellspacing="0" cellpadding="0">
		    <input type="hidden" id="prtcolId" name="protocolId" value="<%=protocolId%>"></input>
		    <thead>
		      <tr height="35" style="background:#D8D8DA" class="noEvent">
		        <th width="20%" onclick="sorting(document.eventlibrary,'(Cost)')"><%=LC.L_Sequence%> &loz;</th>
		        <th width="40%" onclick="sorting(document.eventlibrary,'lower(EVENT_CATEGORY)')"><%=LC.L_Evt_Cat%> &loz;</th>
		        <th width="40%" onclick="sorting(document.eventlibrary,'lower(NAME)')"><%=LC.L_Event_Name%> &loz;</th>
		      </tr>
		    </thead>
		    <tbody onmouseover="this.style.cursor='move';">
		    <%
		    
		    for (int iY=0; iY<noOfEvents; iY++){
		    	
		    	id = (Integer)eventIds.get(iY);
		    	eventCost=((eventCosts.get(iY)) != null)?(eventCosts.get(iY)).toString():"-";
		    	eventName=((eventNames.get(iY)) == null)?"-":(eventNames.get(iY)).toString();
		    	catgName = ((catgNames.get(iY)) == null)?"-":(catgNames.get(iY)).toString();
		    	
		    	if ((iY%2)==0) {
				%>
					<tr class="browserEvenRow" height="25">
				 <%
				}
		    	else {
				%>
		    	    <tr class="browserOddRow" height="25">
		    	 <%
		    	}
		    	%>
		    	<td id="seq_id" style="display:none"><%=id.toString()%></td>
		    	<td id="sequence" ><%=eventCost%></td>
		    	<td id="cat"> <%=catgName%> </td>
		    	<%
		    	if(eventName.length()>=50){
		        	String EvntName=eventName.substring(0,50);
		        	overlibParameter=",WRAP, BORDER,2, CENTER,BELOW,OFFSETY,10,STATUS,'Draggable with overflow scrollbar, caption and Close link',";
		        	%>
					<td id="evtname" ><span onmouseover="return overlib(htmlEncode('<%=eventName%>')<%=overlibParameter %>CAPTION,'<%=LC.L_Event_Name%>');" onmouseout="nd();"><%=EvntName%>...</span></td>
		        <%	}else{
		    		%>
		    		<td id="evtname"><%=eventName%> </td>
	 			<%} %>								
				</tr>
		 <%   	
		    }
		%>
		 </tbody>
		 </table>
		</div>
		<div id='progressMsg' style="display:none;"><br><p class="sectionHeadings" align="center"><%=LC.L_Please_Wait%>... <img class="asIsImage_progbar"  src="../images/jpg/loading_pg.gif" /></p></div>
		<% 
		} 
		
		else
		{
		String offLineFlag ="";
		String hideFlag ="";
		int visitCount=0;
	    String eventRow="";
	    for (int iY=0; iY<noOfEvents; iY++){
					if (calledFrom.equals("S")){

						hideFlag = ((hideFlags.get(iY)) == null)?"":(hideFlags.get(iY)).toString();

						}
					String eventVisitID= eventVisitIds.get(iY).toString();
					if(!eventIds.get(iY).equals(0) && eventVisitID.equals(visitNameId) )
					{
						visitCount++;
						String visitName=(String)eventNames.get(iY);
						String cptCode=(String)cptCodes.get(iY);
						String libType= (String)eventLibraryType.get(iY);
						libType = scho.getCodeDescription(EJBUtil.stringToNum(libType));
						StringBuffer toolTip=  new StringBuffer();
						toolTip.append("<b>").append(LC.L_Event_Name).append(": </b>")
						.append((visitName.length()>25?visitName.substring(0,25)+"...":visitName)+"<br>")
						.append("<b>").append(LC.L_Evt_Lib).append(": </b>")
						.append((libType.length()>25?libType.substring(0,25)+"...":libType)+"<br>") //YK:Fixed Bug#7397
						.append("<b>").append(LC.L_Event+" ").append(LC.L_Cpt_Code).append(": </b>")
						.append((cptCode==null?" - ":cptCode)+"<br>");
						if(visitName.length()>500)
							overlibParameter=",WRAP, BORDER,2, CENTER,BELOW,OFFSETY,10,STATUS,'Draggable with overflow scrollbar, caption and Close link',";
						else
							overlibParameter=",BELOW,";
								%>
					<tr id="<%=visitCount %>"  style="cursor:n-resize;" onmouseover="this.style.cursor='move'; return overlib('<%=visitName%>'<%=overlibParameter %>CAPTION,'<%=LC.L_Event_Name%>');" onmouseout="return nd();">
						<td style="border-bottom: 1px solid #CCC; border-right: 1px solid #CCC; display: none;" class="eventMove"><%=eventIds.get(iY)%></td>
						<td style="border-bottom: 1px solid #CCC; border-right: 1px solid #CCC;" class="eventMove"><%=sequences.get(iY)%></td>
						<%
						if(visitName.length()>50){%>
						<td style="border-bottom: 1px solid #CCC; border-right: 1px solid #CCC;" class="eventMove"><%=visitName.substring(0,50) %>...</td>
						<% }
						else{%>
						<td style="border-bottom: 1px solid #CCC; border-right: 1px solid #CCC;" class="eventMove"><%=visitName %></td>
						<% }%>
						<td style="text-align: center; border-bottom: 1px solid #CCC;" class="eventMove">
						<%if(calStatus.equals("O")){ %>
						<%if (hideFlag.equals("1")){%>
						<input id="chk<%=eventIds.get(iY)%><%=eventVisitID%>" style="cursor:default;" type="checkbox" checked onclick="checkUncheck('<%=eventIds.get(iY)%><%=eventVisitID%>');"/>
						<input type="hidden" id="chkHid<%=eventIds.get(iY)%><%=eventVisitID%>" value="checked"></input>
						<%}else{%>
						<input id="chk<%=eventIds.get(iY)%><%=eventVisitID%>" style="cursor:default;" type="checkbox" onclick="checkUncheck('<%=eventIds.get(iY)%><%=eventVisitID%>');"/>
						<input type="hidden" id="chkHid<%=eventIds.get(iY)%><%=eventVisitID%>" value="unchecked"></input>
						<%}%>
						<%}else{
							%>
							<input id="chk<%=eventIds.get(iY)%><%=eventVisitID%>" style="cursor:default;" type="checkbox" disabled/>
							<%
						}%>
						</td>
						
					</tr>	
				<% }
			}

				if(visitCount==0)
				{ %>
				<tr class="noEvent">
				<td colspan="4" ><%=MC.M_NoEvt_InThisVisit%></td>
				</tr>	
				<%}
				%>
				<tr class="noEvent" height="25">
				<td style="border-bottom: 1px solid #CCC; border-right: 1px solid #CCC;>&nbsp;"></td>
				<td style="border-bottom: 1px solid #CCC; border-right: 1px solid #CCC;>&nbsp;"></td>
				<td style="border-bottom: 1px solid #CCC; ">&nbsp;</td>
				</tr>
   <%
	  }
}//end of if body for session
else
{
	%>
	<jsp:include page="timeout.html" flush="true" /> 
	<%
	}
%>

