<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.aithent.audittrail.reports.AuditUtils"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB"/>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>


<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	<%-- if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   } --%>
}
</SCRIPT>

<% String src;
	src= request.getParameter("srcmenu");
%>
<jsp:include page="skinChoser.jsp" flush="true"/>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>  

<BODY> 
<br>

<DIV class="formDefault" id="div1">
<% 
HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession))	{
	String userId =request.getParameter("userId");
	int userPK =EJBUtil.stringToNum(userId);
	
	String delMode=request.getParameter("delMode");
	
	if (delMode==null) {
		delMode="final";
%>
	<FORM name="deleteUser" id="deleteUserFrm" method="post" action="deleteUser.jsp" onSubmit="if (validate(document.deleteUser)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<br><br>	
	<P class="defComments"><%=MC.M_EsignToProc_WithDelUsr%><%--Please enter e-Signature to proceed with delete User .*****--%></P>	
	
	<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="deleteUserFrm"/>
			<jsp:param name="showDiscard" value="N"/>
	</jsp:include>

 	 <input type="hidden" name="delMode" value="<%=delMode%>">
  	 <input type="hidden" name="srcmenu" value="<%=src%>">
 	 <input type="hidden" name="userId" value="<%=userId%>">
	 
	</FORM>
<%
	} else {
			String eSign = request.getParameter("eSign");	
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>
	 		  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
			} else {
	
	userB.setUserId(userPK);
	int i=0;
	//a non system user is not deleted physically
	// it is removed from the list logically by changing the userType to 'X'
	userB.getUserDetails();
	userB.setUserType("X");
	// Modified for INF-18183 ::: AGodara
	i = userB.updateUser(AuditUtils.createArgs(tSession,"",LC.L_Manage_Acc));

	String viewList= "NS";
	%>
		<br><br><br><br>
		<%
if(i >= 0) {
%>
<p class = "successfulmsg" align = center> <%=MC.M_Usr_DelSucc%><%--The user has been deleted successfully.*****--%></p>
<%
		} else {
%>
<p class = "successfulmsg" align = center> <%=MC.M_Usr_CntDel%><%--The user could not be deleted .*****--%> </p>
<%
		}%>
	<META HTTP-EQUIV=Refresh CONTENT="1; URL=accountbrowser.jsp?srcmenu=<%=src%>&cmbViewList=<%=viewList%>" >
	<%
	
	} //end esign
	} //end of delMode
}//end of if body for session
else{
%>
<jsp:include page="timeout.html" flush="true"/> 
<%
}
%>

  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
  
</DIV>

<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/> 
</div>

</BODY>
</HTML>

