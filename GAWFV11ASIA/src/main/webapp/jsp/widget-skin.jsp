<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="codeLst" scope="session" class="com.velos.eres.web.codelst.CodelstJB"/>
<%@ page language="java" import="java.util.*,com.velos.eres.web.user.UserJB, com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB"%>

<%
HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession)) {
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
	int patientRights = Integer.parseInt(grpRights.getFtrRightsByValue("MPATIENTS"));
	int studyRights = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));
	UserJB userB1 = (UserJB) tSession.getValue("currentUser");
	String accName = (String) tSession.getValue("accName");
	String accId = (String) tSession.getValue("accountId");
	accName=(accName==null)?"default":accName;
	String accSkinId = "";
	String usersSkinId = userB1.getUserSkin();
	String userSkin = "";
	String accSkin = (String) tSession.getValue("accSkin");
	String skin = "default";
	userSkin= codeLst.getCodeCustomCol(StringUtil.stringToNum(usersSkinId) );
	accSkin= codeLst.getCodeCustomCol(StringUtil.stringToNum(accSkinId) );

	if (accSkin == null || accSkin.equals("") || accSkin.equals("default") ){
		accSkin = "accSkin";
	} else {
		skin = accSkin;
	}

	if (userSkin != null && !userSkin.equals("") ){
		skin = userSkin;
	}

%>
<script> whichcss_skin("<%=skin%>");</script>

<% }  %>


<SCRIPT type="text/javascript">

/*Dynamically put CSS for FireFox and IE depending on screen resolution*/
if ((document.layers) || (navigator.appName == 'Netscape') || (navigator.appName == 'Opera')){
	if (screen.width=='800'){
		document.write('<LINK id=smthemenewprint-css rel=stylesheet type=text/css href="styles/mgPanel/mgPanelstyle_FF_800.css" />');
	} else {
		document.write('<LINK id=smthemenewprint-css rel=stylesheet type=text/css href="styles/mgPanel/mgPanelstyle_FF.css" />');
	}
} else {
	if (screen.width=='800'){
		document.write('<LINK id=smthemenewprint-css rel=stylesheet type=text/css href="styles/mgPanel/mgPanelstyle_IE_800.css" />');
	} else {
		document.write('<LINK id=smthemenewprint-css rel=stylesheet type=text/css href="styles/mgPanel/mgPanelstyle_IE.css" />');
	}
}

</SCRIPT>
