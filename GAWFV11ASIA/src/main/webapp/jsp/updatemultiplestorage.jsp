<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

</HEAD>
<jsp:include page="skinChoser.jsp" flush="true" />
<body>

  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
  <jsp:useBean id="strB" scope="request" class="com.velos.eres.web.storage.StorageJB"/>
  <jsp:useBean id="storageStatB" scope="page" class="com.velos.eres.web.storageStatus.StorageStatusJB"/>
  <%@page language = "java" import = "com.velos.eres.service.util.*,com.velos.eres.web.storage.*,java.util.*,java.text.*,com.velos.eres.web.storageStatus.*,com.velos.eres.business.common.*"%>

  <%

	String eSign = request.getParameter("eSign");
	HttpSession tSession = request.getSession(true);

	if (sessionmaint.isValidSession(tSession))

   {
     %>
     <jsp:include page="sessionlogging.jsp" flush="true"/>

     <%

   	String oldESign = (String) tSession.getValue("eSign");

	//if(!oldESign.equals(eSign))
	//{
%>
  	 
<%
	//}
	//else
  	//{
		String ipAdd = (String) tSession.getValue("ipAdd");
    	String usr = null;
	    usr = (String) tSession.getValue("userId");
		String accId = (String) tSession.getValue("accountId");//KM-AccountId Added for the new requirement.

		String id = "";
		String name ="";
		String dStore ="", template_type = "", altId= "", storage_unit_class= "";
		//String location ="";
		String cells1 ="";
		String namingConv ="";
		String positioning ="";
		String cells2 = "";
		String namingConvn ="";
		String positioningn ="";
		String storageCoordinateX ="";
		String storageCoordinateY ="";
		int ret = 0;
		int defaultItem = 0;
		CodeDao cd = new CodeDao();
		defaultItem = cd.getCodeId("cap_unit","Items");
		String ItemCode = defaultItem+"";

		String ids[] = null;
		String names[] =null;
		String dStores[] =null;

		String templateTypes[] =null;
		String altIds[] =null;
		String storageUnitClasses[] =null;

		String cells1s[] =null;
		String namingConvs[] =null;
		String positionings[] =null;
		String cells2s[] = null;
		String namingConvns[] =null;
		String positioningns[] =null;

		String counts[] =null;
		String totRows = "1";

		boolean proceed=true;
		int count=0;
		int countId =0;
		String strName="";
		String strId ="";

		counts = request.getParameterValues("id");
		totRows = (new Integer(counts.length)).toString();

			ids = request.getParameterValues("id");
		    names = request.getParameterValues("name");
   			dStores = request.getParameterValues("dStore");

   			templateTypes = request.getParameterValues("templatetype");
   			altIds = request.getParameterValues("alternateId");
   			storageUnitClasses = request.getParameterValues("storageunitclass");


			cells1s = request.getParameterValues("cells1");
		    namingConvs = request.getParameterValues("namingConv");
			positionings = request.getParameterValues("positioning");
			cells2s = request.getParameterValues("cells2");
		    namingConvns = request.getParameterValues("namingConvn");
			positioningns = request.getParameterValues("positioningn");


			for(int test=0 ;test<EJBUtil.stringToNum(totRows);test++){

			   strId=ids[test];



			   if((strId!= null) && (strId.trim()!= "")){
				countId = strB.getCountId(strId.trim(),accId);//KM
		   }

		   if (countId>0){
		  		  proceed=false;
				  break;
			   }
		   }

       if(proceed==false) {
%>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<% if(countId >0) {%>
<p class = "successfulmsg" align = center> <%=MC.M_StrIdExst_EtrNew %><%-- The One of the Storage ID that you have entered already
  exists. Please enter a new Id.*****--%> </p>

  <table align=center>
    <tr width="100%">  <!--KM-3021-->
      <td width="100%" >
	   <input type="button" name="return" value="<%=LC.L_Back%><%--Back*****--%>" onClick="window.history.back()">
      </td>
    </tr>
  </table>

<%  } } else {

			int dim1;
			int dim2;
			int cellCap;
			int cret=0;
			int sret=0;

			String storageId ="";
			String storageStatusCode ="";
			CodeDao cdao =new CodeDao();
			int defaultStat = cdao.getCodeId("storage_stat","Available");
			storageStatusCode = defaultStat+"";

			Date dt = new java.util.Date();
			String statDate = DateUtil.dateToString(dt);


			for (int cnt =0;cnt< EJBUtil.stringToNum(totRows); cnt++ ){
				id = ids[cnt];
				name =names[cnt];
				dStore = dStores[cnt];

				template_type = templateTypes[cnt];
				altId= altIds[cnt];
				storage_unit_class = storageUnitClasses[cnt];

				cells1 = cells1s[cnt];
				namingConv = namingConvs[cnt];
				positioning = positionings[cnt];
				cells2 = cells2s[cnt];
				namingConvn = namingConvns[cnt];
				positioningn = positioningns[cnt];



				if (((name != null) && (!name.trim().equals(""))) && ( ! StringUtil.isEmpty(name) )){
				StorageJB storageB = new StorageJB();

				if(id.equals("")) {
  				   // id = storageB.getStorageIdAuto();
  				   id = storageB.getStorageIdAuto(dStore, EJBUtil.stringToNum(accId));
  		           id=(id==null)?"":id;
				}
				else
					storageB.getStorageIdAuto();//KM- to fix the issue 3141


				dim1 =EJBUtil.stringToNum(cells1.trim());
				dim2 =EJBUtil.stringToNum(cells2.trim());
				cellCap = dim1*dim2;

				storageB.setStorageId(id.trim());
				storageB.setStorageName(name.trim());
				storageB.setFkCodelstStorageType(dStore);
				storageB.setStorageCapNum(cellCap+"");
				storageB.setFkCodelstCapUnits(ItemCode);
				//storageB.setStorageStatus("Available");//KM
				storageB.setStorageDim1CellNum(cells1);
				storageB.setStorageDim1Naming(namingConv);
				storageB.setStorageDim1Order(positioning);
			    storageB.setStorageDim2CellNum(cells2);
				storageB.setStorageDim2Naming(namingConvn);
				storageB.setStorageDim2Order(positioningn);
				storageB.setTemplate("0");
				//storageB.setStorageCoordinateX(storageCoordinateX);
				//storageB.setStorageCoordinateY(storageCoordinateY);

				storageB.setCreator(usr);
				storageB.setIpAdd(ipAdd);
				storageB.setAccountId(accId);

				 //JM: 04Aug2009: #INVP2.15-(part-1)
				storageB.setStorageTemplateType(template_type);
				storageB.setStorageAlternateId(altId);
				storageB.setStorageUnitClass(storage_unit_class);

				ret = storageB.setStorageDetails();

				//KM-091307--Added for Child storage unit records insertion.
				if(ret>=0) {

					String a[]=new String[cellCap];

					int m=0;
					int n=0;
					int r=0;

					if(namingConv.equals("azz")) {
					for (int k=1;k<=dim1;k++) {
						 for (int l=1;l<=dim2;l++) {
							if(k > 26) {
							   n=Math.abs(k/26);
							   r = k%26;
							   a[m] = name.trim()+"-"+(char)(n+64)+""+(char)(r+64)+""+l;//KM-100307
							}
							else {
							  a[m] = name.trim()+"-"+(char)(k+64)+""+l;
							}
							m++;
						}
					}
					}

			  if(namingConv.equals("zza")) {
				for (int k=1;k<=dim1;k++) {
					 for (int l=1;l<=dim2;l++) {
						n=Math.abs(k/26);
						if(k>26)
						   r = k%26;
						else
						   r=k;
						a[m] = name.trim()+"-"+(char)(90-n)+""+(char)(91-r)+""+l;
						m++;
					}
				  }
			   }

		int s=0;
		int t=0;
		if (((positioning.equals("LR") && namingConv.equals("azz")) ||((positioning.equals("LR") && namingConv.equals("zza")))) &&
			((positioningn.equals("BF")  && namingConvn.equals("1n")) || (positioningn.equals("FB")  && namingConvn.equals("n1")))) {
		   s=1;
		   t=1;
		}

		if (((positioning.equals("LR") && namingConv.equals("azz")) || ((positioning.equals("LR") && namingConv.equals("zza")))) &&
			((positioningn.equals("FB")  && namingConvn.equals("1n")) || (positioningn.equals("BF")  && namingConvn.equals("n1")))) {
		   s=1;
		   t=dim2;
		}


		if (((positioning.equals("RL") && namingConv.equals("azz")) ||  ((positioning.equals("RL") && namingConv.equals("zza"))))  &&
			((positioningn.equals("FB")  && namingConvn.equals("1n")) || (positioningn.equals("BF")  && namingConvn.equals("n1")))) {
		   s=dim1;
		   t=dim2;
		}


		if (((positioning.equals("RL") && namingConv.equals("azz")) || ((positioning.equals("RL") && namingConv.equals("zza")))) &&
			((positioningn.equals("FB")  && namingConvn.equals("n1")) || (positioningn.equals("BF")  && namingConvn.equals("1n")))) {
		   s=dim1;
		   t=1;

		}

		if (cellCap > 1) {//KM-122407
		   for (int j=0;j<cellCap;j++) {
			StorageJB strgB = new StorageJB();
			// storageId = storageB.getStorageIdAuto();
			storageId = storageB.getStorageIdAuto(dStore, EJBUtil.stringToNum(accId));
			storageId=(storageId==null)?"":storageId;
			strgB.setCreator(usr);

			strgB.setStorageId(storageId);
			strgB.setStorageName(a[j]);
		    strgB.setFkStorage(ret+"");
			//strgB.setStorageStatus("Available");//KM
			strgB.setStorageCapNum("1");//KM-100307
			strgB.setFkCodelstCapUnits(ItemCode);
			strgB.setIpAdd(ipAdd);
			strgB.setAccountId(accId);//KM-Added for new requirement change.
			strgB.setFkCodelstStorageType(dStore); // child gets the same storage type as parent by default

			 //JM: 04Aug2009: #INVP2.15-(part-1)
				strgB.setStorageTemplateType(template_type);
				strgB.setStorageAlternateId(altId);
				strgB.setStorageUnitClass(storage_unit_class);


		   if (((positioning.equals("LR") && namingConv.equals("azz")) ||((positioning.equals("LR") && namingConv.equals("zza")))) &&
			((positioningn.equals("BF")  && namingConvn.equals("1n")) || (positioningn.equals("FB")  && namingConvn.equals("n1")))) {
			  strgB.setStorageCoordinateX(s+"");
			  strgB.setStorageCoordinateY(t+"");
			if(t<=dim2)
			   t=t+1;

			if(t>dim2){
			   t=1;
			   s=s+1;
		    }
			}


			if (((positioning.equals("LR") && namingConv.equals("azz")) || ((positioning.equals("LR") &&namingConv.equals("zza"))))&&
			((positioningn.equals("FB")  && namingConvn.equals("1n")) || (positioningn.equals("BF")  && namingConvn.equals("n1")))) {

			  strgB.setStorageCoordinateX(s+"");
			  strgB.setStorageCoordinateY(t+"");
			if(t<=dim2)
			   t=t-1;
			if(t<1){
			   t=dim2;
			   s=s+1;
		    }
			}

		  if(((positioning.equals("RL") && namingConv.equals("azz")) || ((positioning.equals("RL") && namingConv.equals("zza")))) &&
			((positioningn.equals("FB")  && namingConvn.equals("1n")) || (positioningn.equals("BF")  && namingConvn.equals("n1")))) {
			  strgB.setStorageCoordinateX(s+"");
			  strgB.setStorageCoordinateY(t+"");

			if(t<=dim2)
			   t=t-1;
			if(t<1){
			   t=dim2;
			   s=s-1;
		    }
			}


			if(((positioning.equals("RL") && namingConv.equals("azz")) || ((positioning.equals("RL") && namingConv.equals("zza")))) &&
			((positioningn.equals("FB")  && namingConvn.equals("n1")) || (positioningn.equals("BF")  && namingConvn.equals("1n")))) {
			  strgB.setStorageCoordinateX(s+"");
			  strgB.setStorageCoordinateY(t+"");
			if(t<=dim2)
			   t=t+1;
			if(t>dim2){
			   t=1;
			   s=s-1;
		    }
			}

			cret = strgB.setStorageDetails();

			StorageStatusJB strStatB = new StorageStatusJB();
			strStatB.setFkStorage(cret+"");
			strStatB.setSsStartDate(statDate);
			strStatB.setFkCodelstStorageStat(storageStatusCode);
			strStatB.setIpAdd(ipAdd);
			strStatB.setCreator(usr);
			sret=strStatB.setStorageStatusDetails();

		    }

		}

			StorageStatusJB stStatB = new StorageStatusJB();

			stStatB.setFkStorage(ret+"");
			stStatB.setSsStartDate(statDate);
			stStatB.setFkCodelstStorageStat(storageStatusCode);
			stStatB.setIpAdd(ipAdd);
			stStatB.setCreator(usr);
			sret=stStatB.setStorageStatusDetails();

		}
				}
			}

%>

 <br>
      <br>
      <br>
      <br>
      <br>
	  <% if (ret > 0) { %>
      <p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc %><%-- Data was saved successfully*****--%> </p>
      <SCRIPT>
			window.opener.location.reload();
           	setTimeout("self.close()",1000);
      </SCRIPT>

	  <% } else if(ret == -2) { %>
	  <p class = "successfulmsg" align = center> <%=MC.M_DataNotSvd_Succ %><%-- Data was not saved successfully*****--%> </p>
      <SCRIPT>
			window.opener.location.reload();
           	setTimeout("self.close()",1000);
      </SCRIPT>

	  <%}%>
<%
}//KM
//}//end of eSign check
}//end session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
	<div class = "myHomebottomPanel">
	    <jsp:include page="bottompanel.jsp" flush="true"/>
	</div>

</BODY>
</HTML>
