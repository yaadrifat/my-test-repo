<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></HEAD>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<BODY>
<jsp:useBean id="linkB" scope="request" class="com.velos.eres.web.ulink.ULinkJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.business.common.*,com.velos.eres.service.util.*"%>
<%
   int ret=0;
   int lnkId =0;
   String userId = null;
   String accId ="";
   String URI ="";
   String desc ="";
   String grpName ="";
   String mode,msg= "";	
   String lnkType = "";
   String src=null;
   String tab = "";
   String descArray[] =null; 	
   String URIArray[]=null; 
   String grpNameArray[]=null; 


   src  = request.getParameter("src");
   lnkId  = Integer.parseInt(request.getParameter("lnkId"));
   lnkType = request.getParameter("lnkType");
   
   String eSign = request.getParameter("eSign");
HttpSession tSession = request.getSession(true);  
if (sessionmaint.isValidSession(tSession))
   {	
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%   
   	String oldESign = (String) tSession.getValue("eSign");


	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {

   String ipAdd = (String) tSession.getValue("ipAdd");
   String usr = (String) tSession.getValue("userId");

   if(lnkType.equals("user")) {
      userId = request.getParameter("lnkUserId");
   }
   
   accId = request.getParameter("lnkAccId");
   mode = request.getParameter("mode");
   tab = request.getParameter("selectedTab");
   String totRows= request.getParameter("totrows");	

   descArray = request.getParameterValues("lnkDesc");
   URIArray = request.getParameterValues("lnkURI");
   grpNameArray = request.getParameterValues("lnkGrpName");

for(int i=0; i<EJBUtil.stringToNum(totRows);i++)
	{	

   desc=(String)descArray[i];
   URI=(String)URIArray[i];
	   grpName=(String)grpNameArray[i];
//JM: 03072006: modified: if condition
	//if(!(grpName.equals(""))){		
	if(!(desc.equals(""))){		
	
	   linkB.setLnkId(lnkId);
	   linkB.setLnkUserId(userId);
	   linkB.setLnkDesc(desc);
	   linkB.setLnkURI(URI);
	   linkB.setLnkGrpName(grpName);
	   linkB.setLnkAccId(accId);
	   linkB.setCreator(usr); 
	   linkB.setIpAdd(ipAdd);
	   linkB.setULinkDetails(); //Inserting a new User Link
		
	}


 }
%>


<br><br><br><br><br><br><br>


<p class = "sectionHeadings" align = center> <%=MC.M_Data_SvdSucc %><%-- Data was saved successfully*****--%> </p>



<br>
<br>
<br>
<br>
<br>
<%
   if(lnkType.equals("user")) {
%>
<META HTTP-EQUIV=Refresh CONTENT="2; URL=ulinkBrowser.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>">
<%
   } else {
%>
<META HTTP-EQUIV=Refresh CONTENT="2; URL=accountlinkbrowser.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>">
<%
   }
%>

<%
}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>
</HTML>
