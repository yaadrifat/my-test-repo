<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@page import="java.sql.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*,java.util.StringTokenizer,com.velos.eres.web.user.UserJB"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>


<%
	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))
	{
		ResultSet rsResults = null;
		Statement stmt = null;
		Connection conn = null;
	    String Message="";
		int Count=0;
		CommonDAO cd = new CommonDAO();

		UserJB user = (UserJB) tSession.getValue("currentUser");
	   	String accountId = user.getUserAccountId();
		
		String v_specName = request.getParameter("v_specName");
		int v_pkSpecID = Integer.parseInt(request.getParameter("v_pkSpecID"));
		if(v_specName.length()<1)
		{
			Message = MC.M_ChldSpec_CntBlank;
		}
		else
		{
			//System.out.print(v_pkSpecID+"============= AJAX Activated ============="+v_specName);
			try
			{
				conn = cd.getConnection();
				stmt=conn.createStatement();
				String sqlQuery = "SELECT COUNT(*) FROM er_specimen WHERE SPEC_ID = '"+v_specName+"' AND PK_SPECIMEN <> "+v_pkSpecID
				+" AND FK_ACCOUNT='"+accountId+"'";
				ResultSet rs = stmt.executeQuery(sqlQuery); 
				if(rs.next())
				{
					Count  = rs.getInt(1);
					if(Count>0)
					{
						Message = MC.M_SpmenIdExst_GiveDiff;
					}
				}
				conn.close();
			}
			catch (Exception ex)
			{
				Rlog.fatal("ajaxValidateSpecimen.jsp","In specimen details, EXCEPTION IN FETCHING FROM eres.er_specimen"+ ex);
			}
			finally
			{
				try
				{
					if (conn != null) { conn.close(); }
				}
				catch (Exception e){ }
			}
		}
		//System.out.println("$"+Message);
		out.println("$"+Message);
	}
	else
	{
		%><jsp:include page="timeout.html" flush="true"/><%
	} //end of else body for session time out
%>

