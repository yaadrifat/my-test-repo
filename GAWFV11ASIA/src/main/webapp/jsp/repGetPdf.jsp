<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.itextpdf.text.Rectangle"%>
<%@page import="com.itextpdf.text.Document"%>
<%@page import="com.itextpdf.text.pdf.PdfWriter"%>
<%@page import="com.itextpdf.text.PageSize"%>
<%@page import="com.itextpdf.tool.xml.XMLWorkerHelper"%>
<html>
	<body>
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">

<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,java.io.*,java.util.ArrayList,javax.xml.transform.TransformerFactory,javax.xml.transform.Transformer,javax.xml.transform.Source,javax.xml.transform.stream.StreamSource,javax.xml.transform.stream.StreamResult,javax.xml.transform.TransformerException,javax.xml.transform.TransformerConfigurationException,java.io.*,java.text.*,java.net.URL,com.velos.eres.web.user.UserJB,com.velos.eres.web.grpRights.*"%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="repB" scope="request" class="com.velos.eres.web.report.ReportJBNew"/>

<%
	HttpSession tSession = request.getSession(true);
 	if (sessionmaint.isValidSession(tSession))
	{
 		String htmlFile=request.getParameter("htmlFile");
 		String filePath=request.getParameter("filePath");
 		String fileDnPath=request.getParameter("fileDnPath");
 		String requestURL 	= StringUtil.encodeString(request.getRequestURL().toString());
 		String queryString 	= StringUtil.encodeString(request.getQueryString());
 		Integer repId = new Integer((request.getParameter("repId")==null) ? "0" : request.getParameter("repId"));
 		String calledfrom = request.getParameter("calledfrom");
 		if(EJBUtil.isEmpty(calledfrom)){
 			calledfrom = LC.L_Rpt_Central;
 		}

	Calendar now1 = Calendar.getInstance();
	String pdfFileName="reportpdf"+"["+ System.currentTimeMillis() + "].pdf" ;
	String pdfFile=filePath + "/" + pdfFileName ;
	String findStr = "";
	String findStr1 = "";
    int len = 0;
    String htmlstr1 ="";
    String htmlstr2 ="";
    int pos1;
    int pos2;
    String repLogName="";
    String repDate="";
    String path1=request.getContextPath();
    String basepath=request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path1;
	try
	{	
		BufferedReader br = new BufferedReader(new FileReader(htmlFile));
		String line=null;
		StringBuffer sb = new StringBuffer();
		while(((line = br.readLine()) != null))
		{
			line=line+"\n";
			sb.append(line);
		}
		String sbNew = sb.toString();
		sbNew = sbNew.replaceFirst("<META http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">","<META http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"></META>");
		sbNew = sbNew.replaceFirst("font-size: 10px","font-size: 9px");
		
		//Note #1:The below comment should be uncomment in case of Client/Developer's code deployed in Windows OS
		//sbNew = sbNew.replaceFirst("<img src=\"../images/jpg/mdalogo.png\" class=\"asIsImage\">","<IMG  src='"+basepath+"/jsp/images/mdalogo.png\' class=\"asIsImage\"></IMG>");
		//Note #1: END
		
		//Note #2:The below comment should be uncomment in case of Client/Developer's code deployed in Linux OS
		sbNew = sbNew.replaceFirst("<img src=\"../images/jpg/mdalogo.png\" class=\"asIsImage\">","<IMG  src=\"/u01/velos/eResearch_jboss510/server/eresearch/deploy/velos.ear/velos.war/images/jpg/mdalogo.png\" class=\"asIsImage\"></IMG>");
		//Note #1: END
		
		
		sbNew = sbNew.replaceFirst("<TD WIDTH=\"45%\" ALIGN=\"LEFT\" class=\"reportData\">Confidential </TD>","<TD WIDTH=\"35%\" ALIGN=\"LEFT\" class=\"reportData\"> </TD>");
		sbNew = sbNew.replaceFirst("<TD WIDTH=\"35%\" ALIGN=\"CENTER\" class=\"reportData\">Page 1</TD>","<TD WIDTH=\"35%\" ALIGN=\"CENTER\" class=\"reportData\"></TD>");
		sbNew = sbNew.replaceAll("<BR>","<BR></BR>");
		sbNew = sbNew.replaceAll("<BR/>","<BR></BR>");
		sbNew = sbNew.replaceAll("<HR style=\"margin:1px\" COLOR=\"BLACK\" SIZE=\"3\">","<HR style=\"margin:1px\" COLOR=\"BLACK\" SIZE=\"3\"></HR>");
		sbNew = sbNew.replaceAll("<HR style=\"border: 1px dashed GREY;margin:-1px\">","<HR style=\"border: 1px dashed GREY;margin:-1px\"></HR>");
		sbNew = sbNew.replaceAll("<link type=\"text/css\" href=\"./styles/common.css\" rel=\"stylesheet\">","<link type=\"text/css\" href=\"./styles/common.css\" rel=\"stylesheet\"></link>");
		findStr = "<TD ALIGN=\"LEFT\" WIDTH=\"50%\" class=\"reportFooter\">";
		findStr1="</TD>";
        htmlstr1=sbNew.toString();
        len = findStr.length();
        pos1 = htmlstr1.indexOf(findStr);
        if(pos1!=-1){
        pos2 = htmlstr1.indexOf(findStr1,pos1);
        htmlstr2=htmlstr1.substring(pos1+len, pos2);
        repDate = htmlstr2;
        sbNew = sbNew.replaceAll(htmlstr2,"&nbsp;");
        }
        pos1 = htmlstr1.lastIndexOf(findStr);
        if(pos1!=-1){
        pos2 = htmlstr1.indexOf(findStr1,pos1);
        htmlstr2=htmlstr1.substring(pos1+len, pos2);
        repLogName = htmlstr2;
        sbNew = sbNew.replaceAll(htmlstr2,"&nbsp;");
        }
        findStr = "<TD ALIGN=\"RIGHT\" WIDTH=\"50%\" class=\"reportFooter\">";
        len = findStr.length();
        pos1 = htmlstr1.indexOf(findStr);
        if(pos1!=-1){
        pos2 = htmlstr1.indexOf(findStr1,pos1);
        htmlstr2=htmlstr1.substring(pos1+len, pos2);
        sbNew = sbNew.replaceAll(htmlstr2,"<BR></BR><BR></BR>");
        }
//		sbNew = sbNew.replaceAll("size="+ "\"" + "2" + "\"","size="+ "\"" + "3" + "\"");r
//		sbNew = sbNew.replaceAll("size="+ "\"" + "1" + "\"","size="+ "\"" + "2" + "\"");
		br.close();
		//System.out.println("@94==>>"+sbNew);
		BufferedWriter outNew = new BufferedWriter( new FileWriter( htmlFile) );  
        outNew.write(sbNew);  
        outNew.close();  
          
        
        Document document = new Document(PageSize.A3, 36, 36, 54, 36);
        
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(pdfFile));
        TableHeader event = new TableHeader();
        event.setRepDate(repDate);
        event.setRepLogName(repLogName);
        writer.setPageEvent(event);
        document.open();
        XMLWorkerHelper.getInstance().parseXHtml(writer, document,
                new FileInputStream(htmlFile)); 
       //System.out.println("@110==>>"+htmlFile);
        
        //HTMLWorker htmlWorker = new HTMLWorker(document);
        //htmlWorker.parse(new StringReader(sb.toString()));

         document.close();
		}
		catch (Exception e)
		{
			System.out.println("Error in Creation of PDF file" + e + e.getMessage());
		}
	
		//String path=fileDnPath + "?file=" + pdfFileName +"&mod=R";
		String path= fileDnPath + "?file=" + pdfFileName +"&mod=R&repId="+repId+"&moduleName="+calledfrom+"&filePath="+StringUtil.encodeString(filePath)+"&requestURL="+requestURL+"&queryString="+queryString;
		
%>

	<form name="dummy" method="post" action="<%=path%>" target="_self">
		<input type ="hidden" name="d" />
	</form>

<script>
	document.dummy.submit();
</script>
	
	<% } //session
		%> 
	
</body>
</html>