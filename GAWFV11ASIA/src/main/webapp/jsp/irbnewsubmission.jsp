<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>eCompliance >> Submissions >> Search	<%-- Search*****--%></title>
<body onload="refreshPaginator(); onLoad();" style="overflow:hidden;">
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<jsp:include page="panel.jsp" flush="true"/>
<jsp:include page="ui-include.jsp" flush="true"/> 
<jsp:useBean id="tokenJB" scope="request" class="com.velos.eres.web.userSession.UserSessionTokenJB"/>

<div class="BrowserBotN  BrowserBotN_RC_2"  id="div1">
<%HttpSession tSession = request.getSession(true); %>

<script>


function checkAll(study,pksubmissionstatus,loginUser,studyNumber){
	var result = confirm("Do you want to hide submission for study "+studyNumber+" within the Submission Alerts page ?");
if (result){
	jQuery.ajax({
		url:'updateEcompStat',
		type: 'GET',
		dataType:'json',
		global: true,
		async: false,
		data: {"study": study,"loginUser":loginUser,"pksubmissionstatus":pksubmissionstatus,"checkflag":'Y'
			},
			
			
		success: function(resp) 
		{		
			
					
	},
	error:function(resp){
			
	}
			});
	
}
paginate_study.render();
}
function unCheckAll(study,pksubmissionstatus,loginUser,studyNumber){
	var result = confirm("Do you want to unhide submission for study "+studyNumber+" within the Submission Alerts page ?");
if (result){
	jQuery.ajax({
		url:'afterDelUpdateEcompStat',
		type: 'GET',
		dataType:'json',
		global: true,
		async: false,
		data: {"study": study,"loginUser":loginUser,"pksubmissionstatus":pksubmissionstatus,"checkflag":'Y'
			},
			
			
		success: function(resp) 
		{		
		
					
	},
	error:function(resp){
			
	}
			});
	
}

paginate_study.render();
}
var paginate_study = null;

function opensvwin(id) {
       windowName = window.open("studyview.jsp?studyVPk="+ id,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=500,height=400")
     windowName.focus();
;}



studyNumber=function(elCell, oRecord, oColumn, oData)
{
	
var htmlStr="";
var study=oRecord.getData("PK_STUDY");
var studyNumber=oRecord.getData("STUDY_NUMBER");
if (!study) study="";
if (!studyNumber) studyNumber="";
jQuery(".yui-dt0-col-STUDY_NUMBER").css("width","10%");
if(studyNumber.length>20)
{
	var  studyNumExceedingLimit=studyNumber.substring(0,20);
	studyNumber=studyNumExceedingLimit+"<span onmouseover=\"return overlib(\'"+"<h4>"+studyNumber+"</h4>"+"\',CAPTION,'<%=LC.L_Study_Number%><%--<%=LC.L_Study_Number%> Study Number*****--%>' ,LEFT , ABOVE);\" onmouseout=\"return nd();\"\>...</span>";

}

//htmlStr="<A onClick='return checkEdit();' HREF=\"irbnewcheck.jsp?srcmenu=tdMenuBarItem3&selectedTab=irb_check_tab&mode=M&submissionType=new_app&studyId="+study+"\">"
//    +studyNumber+"</A> ";
	htmlStr="<A href='#' onClick='return checkEditAndLoad(\"irbnewinit.jsp?irbFlag=Y&srcmenu=tdMenuBarItem3&selectedTab=irb_init_tab&mode=S&tabFlag=Y&studyId="+study+"\")'>"
    +studyNumber+"</A> ";      
	elCell.innerHTML=htmlStr;
}

titleLink = 
	function(elCell, oRecord, oColumn, oData)
	  {
		var record=oRecord;
        var htmlStr="";

		var title = record.getData("STUDY_TITLE"); 
		title = ((title==null) || (!title) )? "-":title;
		title = htmlEncode(title);
		title = escapeSingleQuoteEtc(title);
		jQuery(".yui-dt0-col-STUDY_TITLE").css("width","50%");
		//Modified for S-22306 Enhancement : Raviesh			
		htmlStr=title;
		elCell.innerHTML = htmlStr;
	
      }


SeqLink=function(elCell, oRecord, oColumn, oData){
	var htmlStr="";
	var study=oRecord.getData("PK_STUDY");
	var studyNumber=oRecord.getData("STUDY_NUMBER");
	var pkSubmission = oRecord.getData("PK_SUBMISSION");
	var submissionBoardPK = oRecord.getData("PK_SUBMISSION_BOARD");
	var submissionStatus = oRecord.getData("SUBMISSION_STATUS");
	var submissionStatusdesc = oRecord.getData("SUBMISSION_STATUS_DESC");
	var pksubmissionStatus = oRecord.getData("PK_SUBMISSION_STATUS");
	var fk_formlib = oRecord.getData("FK_FORMLIB");
	var SUBMISSION_DESC = oRecord.getData("SUBMISSION_DESC");
	var fk_filledform = oRecord.getData("FK_FILLEDFORM");
	var submission_typesubtyp=oRecord.getData("SUBMISSION_TYPE_SUBTYPE");
	var formLibVer=oRecord.getData("FK_FORMLIBVER");
	var formSeq=oRecord.getData("FORM_SEQ");
    var submissionType='';
    if(submission_typesubtyp=='study_amend'){
    	submissionType='irb_ongo_amd';
        }
    else if(submission_typesubtyp=='prob_rpt'){
    	submissionType='irb_ongo_prob';
          }
    else if(submission_typesubtyp=='closure'){
    	submissionType='irb_ongo_clos';
          }
    else if(submission_typesubtyp=='cont_rev'){
    	submissionType='irb_ongo_cont';
          }
	
	if (!study) study="";
	if (!studyNumber) studyNumber="";
	if(submission_typesubtyp=='new_app'){
		if(formSeq){
			htmlStr="<A href='#' onClick='return checkEditAndLoad(\"irbnewinit.jsp?irbFlag=Y&srcmenu=tdMenuBarItem3&selectedTab=irb_init_tab&mode=M&studyId="+study+"\")'>"
		+formSeq+"</A> ";
		}
		else{
			if(formSeq==null || formSeq==undefined){
				formSeq='';}
			htmlStr=""+formSeq+"";
			}
		}
	else{
		if(formSeq){
    htmlStr="<A HREF=\"javascript:void(0);\"onClick = 'openStudyWin("+study+","+fk_formlib+","+fk_filledform+","+formLibVer+","+formSeq+","+"\""+submissionType+"\")'>"+
    formSeq+"</A> ";
		
		}
		else{
		
			if(formSeq==null || formSeq==undefined){
				formSeq='';}
			//alert("2");
			htmlStr=""+formSeq+"";
		}
	}
	elCell.innerHTML=htmlStr;
	}
letterLink=function(elCell, oRecord, oColumn, oData)
{
var htmlStr="";
var study=oRecord.getData("PK_STUDY");
var pkSubmission = oRecord.getData("PK_SUBMISSION");
var submissionBoardPK = oRecord.getData("PK_SUBMISSION_BOARD");
var submissionStatus = oRecord.getData("SUBMISSION_STATUS");
var submissionStatusdesc = oRecord.getData("SUBMISSION_STATUS_DESC");
var pksubmissionStatus = oRecord.getData("PK_SUBMISSION_STATUS");
var codelst_coll1 = oRecord.getData("CODELST_COL_1");
var codelst_subtyp = oRecord.getData("CODELST_SUBTYP");
var submNotes=oRecord.getData("SUBMISSION_NOTES");
if (!submNotes) submNotes = "";
var reg_exp = /\'/g;
var reg_dbqt = /\"/g;
var reg_lt  = /&lt;/g;
var reg_gt  = /&gt;/g;
var reg_cr  = /\r/g;
var reg_lf  = /\n/g;
submNotes = submNotes.replace(reg_exp, "\\'").replace(reg_dbqt, "&quot;").replace(reg_lt, "<").replace(reg_gt, ">")
		                     .replace(reg_cr, "").replace(reg_lf, "<br/>");
if (!study) study="";
if (!pkSubmission) return;
if (!submissionBoardPK) return;
if (!submissionStatus) return;
if(codelst_coll1){
	htmlStr="<A title=\"<%=LC.L_Outcome_Letter%><%--Outcome Letter*****--%>\" HREF=\"javascript:void(0);\" onclick=\"openLetterWin("+study+","+pkSubmission+","+submissionBoardPK+","+submissionStatus+","+pksubmissionStatus+")\">"+
	submissionStatusdesc+"</A> ";
}
else{
	if(codelst_subtyp=='pi_resp_req'){
		htmlStr="<a href=\"#\" onclick=\"openNotesWindow("+pkSubmission+","+pksubmissionStatus+")\" onmouseover=\"return overlib('"+submNotes+"',CAPTION,'<%=LC.L_Notes%><%--Notes*****--%>');\" onmouseout=\"return nd();\">"+
		submissionStatusdesc+"</a>";
		}
	else{
		
	htmlStr=""+submissionStatusdesc+"";
	}
}
	elCell.innerHTML=htmlStr;
}

function openLetterWin(study,pkSubmission,submissionBoardPK,submissionStatus,pksubmissionStatus,selectedTab)
{

	   windowName = window.open("irbletter.jsp?&studyId="+study+"&submissionPK="+pkSubmission+"&submissionBoardPK="+ submissionBoardPK+"&submissionStatus="+submissionStatus+"&pksubmissionStatus="+pksubmissionStatus+"&selectedTab="+selectedTab,"actionWin","toolbar=no,scrollbars=no,resizable=yes,menubar=no,status=yes,width=700,height=700,left=10,top=10");
	   windowName.focus();
	}
function openStudyWin(study,fk_formlib,fk_filledform,formLibVer,formSeq,submissionType)
{
	var srcmenu=null;
    var hiddenflag='';
    var selTypeperm='';
    var calledFromForm='';
    var selectedTab=null;
    var mode='M';
    var pkey=null;
    var formDispLocation = 'S';
    var patProtId=null;
    var calledFrom='S';
    var statDesc=null;
    var statid=null;
    var patientCode=null;
    var entryChar='M';
    var schevent='';
    var fk_sch_events1='';
    var fkcrf=null;
    var formFillDt='ALL';
    var responseId='';
    var parentId='';
    var fkStorageForSpecimen='';
    var showPanel='False';
    var formCategory='irb_sub';
    var hiddenflag='';
    var hide='';
    var addeditqueryFlag='N'
	   windowName = window.open("studyformdetails.jsp?srcmenu="+srcmenu+"&selTypeperm="+selTypeperm+"&calledFromForm="+calledFromForm+"&selectedTab="
			   +selectedTab+"&formId="+fk_formlib+"&formLibVer="+formLibVer+"&mode="+mode+"&studyId="+study+"&pkey="+pkey+"&formDispLocation="+formDispLocation+"&filledFormId="
			   +fk_filledform+"&patProtId="+patProtId+"&calledFrom="+calledFrom+"&statDesc="+statDesc+"&statid="+statid+"&patientCode="+patientCode+"&entryChar="+entryChar+"&schevent="
			   +schevent+"&checkForOld="+study+"&fk_sch_events1="+fk_sch_events1+"&fkcrf="+fkcrf+"&formFillDt="+formFillDt+"&formPullDown="+fk_formlib+"*"+mode+"*"+formSeq+"&responseId="+responseId+"&parentId="
			   +parentId+"&fkStorageForSpecimen="+fkStorageForSpecimen+"&showPanel="+showPanel+"&submissionType="+submissionType+"&formCategory="+formCategory+"&hiddenflag="+hiddenflag+"&hide="+hide+"&addeditqueryFlag="+addeditqueryFlag,"actionWin","toolbar=no,scrollbars=no,resizable=yes,menubar=no,status=yes,width=700,height=700,left=10,top=10");
	   windowName.focus();
	}
function openNotesWindow(pkSubmission,pksubmissionStatus)
{
	windowName = window.open("irbnotes.jsp?submissionPK="+pkSubmission+"&submissionStatusPK="+pksubmissionStatus,"actionWin","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=600,height=400,left=100,top=10");
	windowName.focus();
}
function checkSearch(formobj)
{ 
 paginate_study.runFilter();
}
function checkEditAndLoad(nextLocation)
{
	window.location.href = nextLocation;
}
     
 $E.addListener(window, "load", function() { 
	 $('CSRFToken').value="<%=StringUtil.trueValue(tokenJB.createTokenForUser((String) tSession.getAttribute("userId")==null?0:StringUtil.stringToNum((String) tSession.getAttribute("userId"))))%>";
  paginate_study=new VELOS.Paginator('ecompsubmissions',{
 			sortDir:"asc", 
				sortKey:"",
				defaultParam:"userId,accountId,grpId",
				filterParam:"submissionType,revType,searchCriteria,description,excldinactive,CSRFToken",
				dataTable:'serverpagination_study',
				rowSelection:[5,10,15],
				navigation: true /*,
				searchEnable:false*/
								
				});

				paginate_study.runFilter();
				paginate_study.render();

			
			 } 
				 )
				 
				 
			 
			 
 </script>
<style>
.textboxirb{border:1px solid black;
border-radius:5px;}
</style>
<%
String src="tdMenuBarItem3";
%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/> 
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB"/><!--km-->
<jsp:useBean id="groupB" scope="page" class="com.velos.eres.web.group.GroupJB"/><!--km--> 
<%@ page language = "java" import = "com.velos.eres.web.user.UserJB" %>
<%@ page language = "java" import = "com.velos.eres.web.grpRights.GrpRightsJB,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration,com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.*,java.sql.*,com.velos.eres.business.common.*"%>  
<%
String refreshBool = "false";
String myUserId = null;
String accountId1 = null;
int grpId1 = 0;
HttpSession mySession = request.getSession(false);
if (sessionmaint.isValidSession(mySession)) {
	if (request.getParameter("searchCriteria") != null) {
		refreshBool = "true";
	}
}
%>
<script>

var refreshPaginator = function() {
	if (<%=refreshBool%>) {
		try {
			paginate_study = new VELOS.Paginator('ecompsubmissions',{
	 				sortDir:"asc", 
	 				sortKey:"",
					defaultParam:"userId,accountId,grpId",
					filterParam:"submissionType,revType,searchCriteria,description,excldinactive",
					dataTable:'serverpagination_study',
					rowSelection:[5,10,15],
					navigation: true,
					noIndexGeneration: true
					});

			paginate_study.runFilter();
		} catch(e) {}
	}
}
</script>
<body  class="yui-skin-sam">

<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>


<div class="browserDefault" id="div1"   style="height:90%; position:absolute;top:5px;">
<%

String pagenum = "";
int curPage = 0;
long startPage = 1;
long cntr = 0;
String pagenumView = "";
int curPageView = 0;
long startPageView = 1;
long cntrView = 0;

pagenum = request.getParameter("page");
if (pagenum == null)
{
    pagenum = "1";
}
curPage = EJBUtil.stringToNum(pagenum);

String orderBy = "";
orderBy = request.getParameter("orderBy");



String orderType = "";
orderType = request.getParameter("orderType");

if (orderType == null)
{
    orderType = "asc";
}


pagenumView = request.getParameter("pageView");
if (pagenumView == null)
{
    pagenumView = "1";
}
curPageView = EJBUtil.stringToNum(pagenumView);

String orderByView = "";
orderByView = request.getParameter("orderByView");
String userId ="" ;
String orderTypeView = "";
orderTypeView = request.getParameter("orderTypeView");

if (orderTypeView == null)
{
    orderTypeView = "asc";
}
//HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession)) {
    userId = (String) tSession.getValue("userId");
    String defUserGroup = (String) tSession.getAttribute("defUserGroup");
    
    
    String accountId = (String) tSession.getValue("accountId");
    
    //Added by Manimaran to give access right for default admin group to the delete link 
	int usrId = EJBUtil.stringToNum(userId);
	userB.setUserId(usrId);
	userB.getUserDetails();
	String defGroup = userB.getUserGrpDefault();
	int grpId=EJBUtil.stringToNum(defGroup);
	groupB.setGroupId(EJBUtil.stringToNum(defGroup));
	groupB.getGroupDetails();
	String groupName = groupB.getGroupName();

String studySql="" ;
String countSql="" ;
String combineSql="" ;
String searchFilter="" ;
String searchFiltercount="" ;
String studyActualDt = null;  
String studyTeamRght = null;  
String tabsubtype  = "";  
      
       acmod.getControlValues("study_rights","STUDYMPAT");  
       ArrayList aRightSeq = acmod.getCSeq();  
       String rightSeq = aRightSeq.get(0).toString();  
       int iRightSeq = EJBUtil.stringToNum(rightSeq);  



CodeDao  cDao= new CodeDao();
int pmtclsId = cDao.getCodeId("studystat", "prmnt_cls");


String pmtclsDesc = cDao.getCodeDescription();


//get default super user rights , returns null if user does not belong to a super user group
String superuserRights = "";

superuserRights = GroupDao.getDefaultStudySuperUserRights(userId);
String submissionType = request.getParameter("submissionType");
	CodeDao cdPhase = new CodeDao();
	//cdPhase.getCodeValues("phase");
	//cdPhase.setCType("phase");
	cdPhase.setForGroup(defUserGroup);
		
	String revType = request.getParameter("revType");
	String ddSubType="";
	String ddRevType="";

	CodeDao cdDD = new CodeDao();
	cdDD.getCodeValues("submission",EJBUtil.stringToNum(accountId));
	cdDD.setCType("submission");
	ddSubType = cdDD.toPullDown("submissionType id='submissionType'",submissionType);
	
	cdDD.resetDao();

	cdDD.getCodeValues("subm_status",EJBUtil.stringToNum(accountId));
	cdDD.setCType("revType");
	cdDD.setForGroup(defUserGroup);
	ddRevType = cdDD.toPullDown("revType id='revType'",revType);

//     userId = (String) tSession.getValue("userId");
    int pageRight = 0;
    GrpRightsJB grpRights = (GrpRightsJB)     tSession.getValue("GRights");        
    pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));
    
    if (pageRight > 0 )    {
    	Object[] arguments = {pmtclsDesc};
    	String searchCriteria = request.getParameter("searchCriteria");
    %>
   <script>
checkbox_hide=function(elCell, oRecord, oColumn, oData)
{	
	var htmlStr="";
	var loginUser = <%=usrId%>
	loginUser = loginUser+'';
	var study=oRecord.getData("PK_STUDY");
	var studyNumber=oRecord.getData("STUDY_NUMBER");
	var pksubmissionstatus = oRecord.getData("PK_SUBMISSION_STATUS");
	//alert("pksubmissionstatus"+pksubmissionstatus);
	var checkeduser = oRecord.getData("USER_CHECKED");
	//alert("checkeduser"+checkeduser);
	if(checkeduser!=null){
		var chkUser = checkeduser.split(",");
	  var chkuser = chkUser.indexOf(loginUser);
	  
	   if (chkuser>=0){
		   htmlStr = "<input type ='checkbox' name='checkallrow'  checked ='checked' onClick = 'unCheckAll("+study+","+pksubmissionstatus+","+loginUser+","+"\""+studyNumber+"\")'/>";
		   }
	   else{
		   htmlStr = "<input type ='checkbox' name='checkallrow' onClick = 'checkAll("+study+","+pksubmissionstatus+","+loginUser+","+"\""+studyNumber+"\")'/>";
		   }
	   
	}

	else{
	htmlStr = "<input type ='checkbox' name='checkallrow' onClick = 'checkAll("+study+","+pksubmissionstatus+","+loginUser+","+"\""+studyNumber+"\")'/>";
	}
elCell.innerHTML=htmlStr;
}
</script>
  <Form name="irbnewsubfrm" METHOD="POST" onsubmit="return false;" >

   <table cellspacing="0" cellpadding="0" border="0" width="99%">
    	<tr><td><p class="sectionHeadings lhsFont">&nbsp;&nbsp;Submission Alerts</p></td></tr>
    </table>
	
    <table cellspacing="0" style="border-collapse: seprate" cellpadding="0" border="0" class="basetbl outline midAlign" width="99%">
    	<tr>
           
    		<td>Search By <%=LC.L_Submission_Type%><%--Submission Type*****--%></td>
    		<td>
    			<%=ddSubType %>
    	    </td>

    		<td>
    		<%=LC.L_Submission_Status%>
    		</td>
				
    		<td>
    			<%=ddRevType %>
    			 
      	   <a style="margin-left:10%" href="advStudysearchpg.jsp" ><%=LC.L_Adva_Search%></a> 
      	    </td>
      	       
      	</tr>
      	
      	<tr>	
    		<td>
    			<%=MC.M_Std_NumOrTitle%><%--<%=LC.Std_Study%> Number or Title*****--%>		  
    		</td>
    		
    		<td>
    		    <input type="text" id="searchCriteria" name="searchCriteria" size="15" value="">
      	    </td>
      	    
      	    <td>
      	    Submission Description
      	    </td>
      	    <td>
      	   
    		    <input type="text" id="description" name="description" size="15" value="">
 	
				<input type="checkbox" name="excldinactive" id="excldinactive"  >Include Hidden Items
				&nbsp;&nbsp;<button type="submit" onClick="checkSearch(document.irbnewsubfrm);"><%=LC.L_Search%></button>
      	    </td>
	</tr>
		
    	</table>
    <a href="irbnewinit.jsp?srcmenu=tdMenuBarItem3&selectedTab=irb_init_tab&mode=N" style="color:blue"><%=LC.L_initiate_new_app%></a>  &nbsp&nbsp <a href="irbongoing.jsp?mode=N" style="color:blue"><%=LC.L_ong_Stud%></a>

        <div >
    <div id="serverpagination_study" ></div>
    </div>
	<input type="hidden" id="userId" value="<%=userId%>">
	<input type="hidden" id="CSRFToken" name="CSRFToken">
  </Form>

    <%
    } //end of if body for page right
    else{
%>
    <%@include file="accessdenied.jsp" %>
<%
    } //end of else body for page right
}//end of if body for session
else {
%>
    <jsp:include page="timeout.html" flush="true"/>
<%}
%>
<div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</div>

<DIV class="mainMenu" id = "emenu"> 
  <jsp:include page="getmenu.jsp" flush="true"/>      
</DIV>
</div>
</body>
</html>