<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.velos.eres.service.util.LC"%>
<%@ page import="com.velos.eres.web.grpRights.GrpRightsJB,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration,com.velos.eres.service.util.StringUtil,java.sql.*,com.velos.eres.business.common.*"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB"/><!--km-->
<jsp:useBean id="tokenJB" scope="request" class="com.velos.eres.web.userSession.UserSessionTokenJB"/>
<jsp:useBean id="groupB" scope="page" class="com.velos.eres.web.group.GroupJB"/><!--km-->

<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="ui-include.jsp" flush="true"/>
<%
String selectedTab = StringUtil.htmlEncodeXss(request.getParameter("tabsubtype"));
String moduleName  = StringUtil.htmlEncodeXss(request.getParameter("tabtype"));
String originalTabType  = "";
HttpSession tSession = request.getSession(true);
if ("ecompPi".equals(moduleName)) {
    moduleName = "irbPend";
} else if ("irbReview".equals(moduleName)) {
	moduleName = "irbReview";
} else{
    moduleName = "irbSub";
}
originalTabType = moduleName;
if (moduleName.equals("irbSub") && selectedTab.equals("irb_appr_tab")) {
	moduleName = "irbPend"; //get the final outcome tab - same as final outcome in PIs area
}
%>

<script>
var paginate_study;
window.onload = initEvents;
function initEvents() {
	document.getElementById('searchCriteria').onkeypress = readCharacter;
}
function readCharacter(evt) {
	evt = (evt) || window.event;
    if (evt) {
        var code = evt.charCode || evt.keyCode;
        if (code == 13 || code == 10) {
         	paginate_study.runFilter();
        }
    }
}
function checkSearch(formobj) {
 	paginate_study.runFilter();
}

$E.addListener(window, "load", function() {
	 $('CSRFToken').value="<%=StringUtil.trueValue(tokenJB.createTokenForUser((String)tSession.getAttribute("userId")==null?0:StringUtil.stringToNum((String)tSession.getAttribute("userId"))))%>";
    paginate_study=new VELOS.Paginator('<%=moduleName%>',{
 				sortDir:"asc",
				sortKey:"STUDY_NUMBER",
				defaultParam:"userId,accountId,grpId,tabsubtype,revType",
				filterParam:"submissionType,revType,searchCriteria,studyPI,CSRFToken",
				dataTable:'serverpagination_study',
				rowSelection:[5,10,15,25,50,75,100]
				});
	paginate_study.render();
	elem = document.getElementById("yui-gen2");
});

titleLink = function(elCell, oRecord, oColumn, oData) {
	var studyTitle=oRecord.getData("STUDY_TITLE");
	var reg_exp = /\'/g;
	studyTitle = studyTitle.replace(reg_exp, "\\'");
	reg_exp = /[\r\n|\n]/g;
	studyTitle = studyTitle.replace(reg_exp, " ");
 	elCell.innerHTML="<a href=\"#\" onmouseover=\"return overlib('"+studyTitle+"',CAPTION,'<%=LC.L_Study_Title%>');\" onmouseout=\"return nd();\"><img border=\"0\" src=\"./images/View.gif\" /></a>";
};

studyNumber = function(elCell, oRecord, oColumn, oData) {
	var htmlStr="";
	var study=oRecord.getData("PK_STUDY");
	var studyNumber=oRecord.getData("STUDY_NUMBER");
	var submTypeSubtype=oRecord.getData("SUBMISSION_TYPE_SUBTYPE");
	var submTypeDesc=oRecord.getData("SUBMISSION_TYPE_DESC");
	if (!study) study="";
	if (!studyNumber) studyNumber="";
	var fkFormlib = oRecord.getData("FK_FORMLIB");
	var lfEntrychar = oRecord.getData("LF_ENTRYCHAR");
	var savedcount = oRecord.getData("SAVEDCOUNT");
	if (!fkFormlib) fkFormlib="";
	if (!lfEntrychar) lfEntrychar="";
	if (!savedcount) savedcount="";
	var formValue = fkFormlib+'*'+lfEntrychar+'*'+savedcount;

	<%  if ("irb_saved_tab".equals(selectedTab)) { %>
	       if ('new_app' == submTypeSubtype) {
 		       htmlStr="<A href='#' onClick='return checkEditAndLoad(\"flexStudyScreen?srcmenu=tdMenuBarItem3&selectedTab=irb_init_tab&mode=M&studyId="+study+"&submissionType="+submTypeSubtype+"\")'>"
	           +studyNumber+"</A> ";
	       } else {
	           htmlStr="<A href='#' onClick='return checkEditAndLoad(\"formfilledstudybrowser.jsp?selectedTab=8&tabsubtype=irb_ongoing_menu&mode=M&formValue="+formValue+"&studyId="+study+"\")'>"
	    	   +studyNumber+"</A> ";
		   }
	<%  } else if ("irb_items_tab".equals(selectedTab)) { %>
           if ('new_app' == submTypeSubtype) {
	           htmlStr="<A href='#' onClick='return checkEditAndLoad(\"flexStudyScreen?srcmenu=tdMenuBarItem3&selectedTab=irb_init_tab&mode=M&studyId="+study+"&submissionType="+submTypeSubtype+"\")'>"
               +studyNumber+"</A> ";
           } else {
               htmlStr="<A href='#' onClick='return checkEditAndLoad(\"formfilledstudybrowser.jsp?selectedTab=8&tabsubtype=irb_ongoing_menu&mode=M&formValue="+formValue+"&studyId="+study+"\")'>"
 	           +studyNumber+"</A> ";
	       }
	<%  } else { %>
           if ('new_app' == submTypeSubtype) {
 		       htmlStr="<A href='#' onClick='return checkEditAndLoad(\"irbnewinit.jsp?srcmenu=tdMenuBarItem3&selectedTab=irb_init_tab&mode=M&studyId="+study+"&submissionType="+submTypeSubtype+"\")'>"
		       +studyNumber+"</A> ";
           } else {
               htmlStr="<A href='#' onClick='return checkEditAndLoad(\"study.jsp?srcmenu=tdMenuBarItem3&selectedTab=1&mode=M&studyId="+study+"\")'>"
               +studyNumber+"</A> ";
           }
	<%  }  %>
 	elCell.innerHTML=htmlStr;
};

actionLink = function(elCell, oRecord, oColumn, oData) {
	var htmlStr="";
	var study=oRecord.getData("PK_STUDY");
	var pkSubmission = oRecord.getData("PK_SUBMISSION");
	var submissionBoardPK = oRecord.getData("PK_SUBMISSION_BOARD");

	if (!study) study="";
	if (!pkSubmission) return;
	if (!submissionBoardPK) return;

 	htmlStr="<A title=\"<%=LC.L_Action%>\" HREF=\"javascript:void(0);\" onclick=\"openActionWin("+study+","+pkSubmission+","+submissionBoardPK+",'<%=selectedTab%>')\">"+
		"<img src=\"../images/jpg/action.gif\" border=\"0\" align=\"left\"/></A> ";
 	elCell.innerHTML=htmlStr;
};

formsLink = function(elCell, oRecord, oColumn, oData) {
	var htmlStr="";
	var study=oRecord.getData("PK_STUDY");
	var pkSubmission = oRecord.getData("PK_SUBMISSION");
	var submissionBoardPK = oRecord.getData("PK_SUBMISSION_BOARD");
	var appSubmissionType	= oRecord.getData("SUBMISSION_TYPE_SUBTYPE");

	if (!study) study="";

 	htmlStr="<A title=\"<%=LC.L_Forms%><%--Forms*****--%>\" HREF=\"javascript:void(0);\" onclick=\"openFormsWin('"+ $('tabsubtype').value+"', "+study+","+pkSubmission+","+submissionBoardPK+",'"+appSubmissionType+"')\">"+
		"<img src=\"./images/Form.gif\" border=\"0\" align=\"left\"/></A> ";
 	elCell.innerHTML=htmlStr;
};

reviewLink = function(elCell, oRecord, oColumn, oData) {
	var htmlStr="";
	var study=oRecord.getData("PK_STUDY");
	var pkSubmission = oRecord.getData("PK_SUBMISSION");
	var submissionBoardPK = oRecord.getData("PK_SUBMISSION_BOARD");
	var appSubmissionType	= oRecord.getData("SUBMISSION_TYPE_SUBTYPE");

	if (!study) study="";

 	htmlStr="<A title=\"<%=LC.L_Review%><%--Review***** --%>\" HREF=\"javascript:void(0);\" onclick=\"openReviewWin('"+ $('tabsubtype').value+"', "+study+","+pkSubmission+","+submissionBoardPK+",'"+appSubmissionType+"')\">"+
		"<img src=\"./images/Review.gif\" border=\"0\" align=\"left\"/></A> ";
 	elCell.innerHTML=htmlStr;
};

notesLink = function(elCell, oRecord, oColumn, oData) {
	var submNotes=oRecord.getData("SUBMISSION_NOTES");
	var pkSubmission = oRecord.getData("PK_SUBMISSION");
	var submissionStatusPK = oRecord.getData("PK_SUBMISSION_STATUS");
	if (!submNotes) submNotes = "";
	var reg_exp = /\'/g;
	var reg_dbqt = /\"/g;
	var reg_lt  = /&lt;/g;
	var reg_gt  = /&gt;/g;
	var reg_cr  = /\r/g;
	var reg_lf  = /\n/g;
	submNotes = submNotes.replace(reg_exp, "\\'").replace(reg_dbqt, "&quot;").replace(reg_lt, "<").replace(reg_gt, ">")
		                 .replace(reg_cr, "").replace(reg_lf, "<br/>");
 	elCell.innerHTML="<a href=\"#\" onclick=\"openNotesWindow("+pkSubmission+","+submissionStatusPK+")\" onmouseover=\"return overlib('"+submNotes+"',CAPTION,'<%=LC.L_Notes%><%--Notes*****--%>');\" onmouseout=\"return nd();\"><%=LC.L_View_Notes%><%--View Notes*****--%></a>";
};

sendLink = function(elCell, oRecord, oColumn, oData) {
	var htmlStr = "";
	var study = oRecord.getData("PK_STUDY");
	var pkSubmission = oRecord.getData("PK_SUBMISSION");
	var submissionBoardPK = oRecord.getData("PK_SUBMISSION_BOARD");

	if (!study) study="";
	if (!pkSubmission) return;
	if (!submissionBoardPK) return;

 	htmlStr="<A title=\"<%=LC.L_Action%><%--Action*****--%>\" HREF=\"javascript:void(0);\" onclick=\"openSendBackWin("+study+","+pkSubmission+","+submissionBoardPK+",'<%=selectedTab%>')\">"+
		"<img src=\"../images/jpg/action.gif\" border=\"0\" align=\"left\"/></A> ";
 	elCell.innerHTML=htmlStr;
};

letterLink = function(elCell, oRecord, oColumn, oData) {
	var htmlStr="";
	var study=oRecord.getData("PK_STUDY");
	var pkSubmission = oRecord.getData("PK_SUBMISSION");
	var submissionBoardPK = oRecord.getData("PK_SUBMISSION_BOARD");
	var submissionStatus = oRecord.getData("SUBMISSION_STATUS");

	if (!study) study="";
	if (!pkSubmission) return;
	if (!submissionBoardPK) return;
	if (!submissionStatus) return;

 	htmlStr="<A title=\"<%=LC.L_Outcome_Letter%><%--Outcome Letter*****--%>\" HREF=\"javascript:void(0);\" onclick=\"openLetterWin("+study+","+pkSubmission+","+submissionBoardPK+","+submissionStatus+",'<%=selectedTab%>')\">"+
		"<img src=\"../images/jpg/OutcomeLetter.gif\" border=\"0\" align=\"left\"/></A> ";
 	elCell.innerHTML=htmlStr;
};

historyLink = function(elCell, oRecord, oColumn, oData) {
	var htmlStr="";
	var study=oRecord.getData("PK_STUDY");
	var pkSubmission = oRecord.getData("PK_SUBMISSION");
	var submissionBoardPK = oRecord.getData("PK_SUBMISSION_BOARD");
	var appSubmissionType = oRecord.getData("SUBMISSION_TYPE_SUBTYPE");
	var submissionStatus = oRecord.getData("SUBMISSION_STATUS_DESC");

	if (!study) study="";
	if (!submissionStatus) submissionStatus="";

 	htmlStr=submissionStatus+" <A title=\"<%=LC.L_History%><%--History*****--%>\" HREF=\"javascript:void(0);\" onclick=\"openHistoryWin('"+ $('tabsubtype').value+"', "+study+","+pkSubmission+","+submissionBoardPK+",'"+appSubmissionType+"')\">"+
		"<%=LC.L_H%></A> ";
 	elCell.innerHTML=htmlStr;
};

function openReviewWin(selectedTab,study,pkSubmission,submissionBoardPK,appSubmissionType) {
	if (checkSubmissionRight('E') == false) {
		return false;
	}
	var w = screen.availWidth-100;
	windowName = window.open("irbReview.jsp?appSubmissionType="+appSubmissionType+"&studyId="+study+"&submissionPK="+pkSubmission+"&submissionBoardPK="+ submissionBoardPK+"&tabsubtype="+selectedTab,"revWin","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width="+w+",height=600,left=10,top=100");
	windowName.focus();
}

function openActionWin(study,pkSubmission,submissionBoardPK,selectedTab) {
	if (checkSubmissionRight('E') == false) {
		return false;
	}
	windowName = window.open("irbactionwin.jsp?&studyId="+study+"&submissionPK="+pkSubmission+"&submissionBoardPK="+ submissionBoardPK+"&selectedTab="+selectedTab,"actionWin","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=900,height=700,left=10,top=100");
	windowName.focus();
}

function openSendBackWin(study,pkSubmission,submissionBoardPK,selectedTab) {
	if (checkSubmissionRight('E') == false) {
		return false;
	}
	windowName = window.open("irbsendback.jsp?&studyId="+study+"&submissionPK="+pkSubmission+"&submissionBoardPK="+ submissionBoardPK+"&selectedTab="+selectedTab,"actionWin","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=600,height=400,left=100,top=10");
	windowName.focus();
}

function openLetterWin(study,pkSubmission,submissionBoardPK,submissionStatus,selectedTab) {
	if (checkSubmissionRight('E') == false) {
		return false;
	}
	windowName = window.open("irbletter.jsp?&studyId="+study+"&submissionPK="+pkSubmission+"&submissionBoardPK="+ submissionBoardPK+"&submissionStatus="+submissionStatus+"&selectedTab="+selectedTab,"actionWin","toolbar=no,scrollbars=no,resizable=yes,menubar=no,status=yes,width=700,height=700,left=10,top=10");
	windowName.focus();
}

function openFormsWin(tabsubtype,study,pkSubmission,submissionBoardPK,appSubmissionType) {
	if (checkSubmissionRight('E') == false) {
		return false;
	}
	var w = screen.availWidth-100;
	windowName = window.open("irbReview.jsp?appSubmissionType="+appSubmissionType+"&tabsubtype="+tabsubtype+"&studyId="+study+"&submissionPK="+pkSubmission+"&submissionBoardPK="+ submissionBoardPK,"revWin","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width="+w+",height=800,left=10,top=100");
	windowName.focus();
}

function openHistoryWin(tabsubtype,study,pkSubmission,submissionBoardPK,appSubmissionType) {
	if (checkSubmissionRight('V') == false) {
		return false;
	}
	var w = screen.availWidth-100;
	windowName = window.open("irbhistory.jsp?appSubmissionType="+appSubmissionType+"&tabsubtype="+tabsubtype+"&studyId="+study+"&submissionPK="+pkSubmission+"&submissionBoardPK="+ submissionBoardPK,"revWin","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=100");
	windowName.focus();
}

function openNotesWindow(pkSubmission,submissionStatusPK) {
	if (checkSubmissionRight('V') == false) {
		return false;
	}
	windowName = window.open("irbnotes.jsp?submissionPK="+pkSubmission+"&submissionStatusPK="+submissionStatusPK,"actionWin","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=600,height=400,left=100,top=10");
	windowName.focus();
}

function checkSubmissionRight(mode) {
  	var elem = document.getElementById("submissionBrowserRight");
	var submissionBrowserRight ;

	if (elem != null) {
		submissionBrowserRight = elem.value;
	}
	if (f_check_perm(submissionBrowserRight,mode) == false)	{
		return false;
	}
}

function checkEdit() {
  	if (checkSubmissionRight('E') == false) {
		return false;
	}
}

function checkEditAndLoad(nextLocation) {
  	if (checkSubmissionRight('E') == false) {
		return false;
	}
	window.location.href = nextLocation;
}

</script>

<body class="yui-skin-sam">
<%
HashMap hmDisplayLabel = new HashMap();
hmDisplayLabel.put("irb_new_tab",""+MC.M_NewSubRecv_Rev/*New Submissions Received for Review*****/+"");
hmDisplayLabel.put("irb_assigned_tab",""+MC.M_SubAssg_AdminRev/*Submissions Assigned for Administrative Review*****/+"");
hmDisplayLabel.put("irb_compl_tab",""+MC.M_SubComp_AdminRev/*Submissions Completed Administrative Review*****/+"");
hmDisplayLabel.put("irb_pend_rev",""+LC.L_SubPend_Rev/*Submissions Pending Review*****/+"");
hmDisplayLabel.put("irb_post_tab",""+MC.M_SubComp_CmmtRev/*Submissions Completed Committee/Member Review*****/+"");
hmDisplayLabel.put("irb_pend_tab",""+MC.M_SubPend_PlResp/*Submissions Pending PI Response*****/+"");
hmDisplayLabel.put("irb_act_tab",""+LC.L_Action_Required/*Action Required*****/+"");
hmDisplayLabel.put("irb_items_tab",""+MC.M_ItemPend_RevApvl/*Items Pending Review/Approval*****/+"");
hmDisplayLabel.put("irb_saved_tab",""+LC.L_Saved_Items/*Saved Items*****/+"");

//HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession)) {
    String userId ="";
    userId = (String) tSession.getValue("userId");
    String defUserGroup = (String) tSession.getAttribute("defUserGroup");
    String accountId = (String) tSession.getValue("accountId");
    String tabsubtype  = "";
    tabsubtype = request.getParameter("tabsubtype");
    String hideRevTypeFilter = request.getParameter("hideRevTypeFilter");;
    if (StringUtil.isEmpty(hideRevTypeFilter)) {
    	hideRevTypeFilter = "N";
    }

    String submissionBrowserRight = request.getParameter("submissionBrowserRight");;
    if (StringUtil.isEmpty(submissionBrowserRight)) {
    	submissionBrowserRight = "0";
    }

    String revType = request.getParameter("revType");
    if (StringUtil.isEmpty(revType)) {
    	revType = "0";
    }

	String displayLabel = (String) hmDisplayLabel.get(tabsubtype);
	if (displayLabel == null) { displayLabel = ""; }
	int usrId = EJBUtil.stringToNum(userId);
	userB.setUserId(usrId);
	userB.getUserDetails();
	String ddSubType="";
	String ddRevType="";
	int pageRight = 7;
	CodeDao cdDD = new CodeDao();
   	cdDD.getCodeValues("submission",EJBUtil.stringToNum(accountId));
   	cdDD.setCType("submission");
	cdDD.setForGroup(defUserGroup);
	ddSubType = cdDD.toPullDown("submissionType id='submissionType'",0);
	cdDD.resetDao();
	cdDD.getCodeValues("revType",EJBUtil.stringToNum(accountId));
	cdDD.setCType("revType");
	cdDD.setForGroup(defUserGroup);
	ddRevType = cdDD.toPullDown("revType id='revType'",revType);
    if (pageRight > 0) {
    %>
  <Form name="frmSubmission" method="POST" onsubmit="checkSearch(this)">
    <input type="hidden" id="accountId" value="<%=accountId%>">
    <input type="hidden" id="userId" value="<%=userId%>">
    <input type="hidden" id="grpId" value="<%=defUserGroup%>">
    <input type="hidden" id="tabsubtype" value="<%=tabsubtype%>">
    <input type="hidden" id="submissionBrowserRight" value="<%=submissionBrowserRight%>">
    <input type="hidden" id="CSRFToken" name="CSRFToken">
	<% if(displayLabel != null && displayLabel.length() != 0){ %>
    <table cellspacing="0" cellpadding="0" border="0" width="99%">
    	<tr><td><p class="sectionHeadings lhsFont">&nbsp;&nbsp;<%=displayLabel %></p></td></tr>
    </table>
	<% } %>
    <table cellspacing="0" cellpadding="0" border="0" class="basetbl outline midAlign" width="99%">
    	<tr  >
            <td><%=LC.L_Filter_By%><%--Filter By*****--%></td>
    		<td><%=LC.L_Submission_Type%><%--Submission Type*****--%></td>
    		<td>
    			&nbsp;<%=ddSubType%>
    	    </td>

    <% if (hideRevTypeFilter.equals("N")) { %>
    		<td><%=LC.L_Review_Type%><%--Review Type*****--%></td>
    		<td>
    				&nbsp;<%=ddRevType%>
      	    </td>
    <% }  //hide filter = N
       else {
     %>
      	    <input type="hidden" name="revType" id='revType'  value="<%=revType%>">
    <%
       }
     %>
    		<td><%=MC.M_Std_NumOrTitle%></td>
    		<td>
    		    <input type="text" id="searchCriteria" name="searchCriteria" size="15" value="">
      	    </td>
	      	<td>&nbsp;<button onClick="paginate_study.runFilter()"><%=LC.L_Search%></button></td>
    	</tr>
    	    <input type=hidden id = "studyPI" name="studyPI" value=''>

    <% if (originalTabType.equals("irbSub") && selectedTab.equals("irb_appr_tab")) { %>
		<tr>
			<td colspan=6 align="right"><%=LC.L_Search_ByPi%><%--Search by PI*****--%></td>
			<td colspan=2>
				<input type=text name="studyPIName" value='' readonly >
				<A HREF="#" onClick='openCommonUserSearchWindow("frmSubmission","studyPI","studyPIName")' ><%=LC.L_Select_User%><%--Select User*****--%></A>
			</td>
		</tr>
	<% } %>
    </table>

    <div >
    <div id="serverpagination_study" ></div>
    </div>
  </Form>
    <%
    } //end of if body for page right
    else{
%>
    <%@include file="accessdenied.jsp" %>
<%
    } //end of else body for page right
} //end of if body for session
else {
%>
    <jsp:include page="timeout.html" flush="true"/>
<%
}
%>
<div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
</div>
