<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

	<head>
		<title><%=LC.L_Milestones_Reports %></title>
		<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	</head>
	
	<%
		String src;
		src= request.getParameter("srcmenu");
		System.out.print("srcmenu  ="+src+"\n\n");
		String study1= request.getParameter("studyId");

 		String selectedTab = request.getParameter("selectedTab");
 		System.out.print("selectedTab  ="+selectedTab+"\n\n");
 	%>
		
	<jsp:include page="panel.jsp" flush="true">
		<jsp:param name="src" value="<%=src%>"/>
	</jsp:include>
	
	<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" >

 		<script language="JavaScript" src="overlib.js"> <!-- overLIB (c) Erik Bosrup --></script>
		<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
		<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

		<jsp:useBean id="milestoneB" scope="request" class="com.velos.eres.web.milestone.MilestoneJB"/>
		<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
		<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
		<jsp:useBean id="stdRightsB" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
		<jsp:useBean id="repdao" scope="request" class="com.velos.eres.business.common.ReportDaoNew"/>
		<jsp:useBean id="repdao1" scope="request" class="com.velos.eres.business.common.ReportDaoNew"/>
		<jsp:useBean id="grpRights" scope="request" class="com.velos.eres.web.grpRights.GrpRightsJB" />

		<%@ page language = "java" import = "com.velos.esch.business.common.SchCodeDao,com.velos.eres.business.common.MilestoneDao,com.velos.eres.business.common.TeamDao,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.DateUtil,com.velos.eres.business.common.MilepaymentDao,com.velos.eres.service.util.*"%>

		<%
			HttpSession tSession = request.getSession(true);
			System.out.print("tsession  ="+tSession+"\n\n");
			if (sessionmaint.isValidSession(tSession))
			{
				grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
				System.out.print("GrpRights  ="+grpRights+"\n\n");
				int pageRight = 0;
				pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("REPORTS"));
				System.out.print("pageRight  ="+pageRight+"\n\n");
				
				String studynumber=request.getParameter("studyNumber");
				String studyPk = request.getParameter("studyId");
		%>


		<DIV class="BrowserTopn" id="div1">
  			<jsp:include page="milestonetabs.jsp" flush="true">
				<jsp:param name="studyNumber" value="<%=StringUtil.encodeString(study1)%>"/>
				<jsp:param name="studyPk" value="<%=studyPk%>"/>
				<jsp:param name="selectedTab" value="<%=selectedTab%>"/>
  			</jsp:include>
		</DIV>
		
		<br>
		
 		<div id="overDiv" style="position:absolute; visibility:collapse; z-index:1000;"> </div>
 
 		<DIV class="BrowserBotN BrowserBotN_M_2" id="div2">
		<!-- include report central-->
			<%
				if (pageRight>=4)
		   		{
		   	%>
					<jsp:include page="reportcentral.jsp" flush="true">
						<jsp:param name="srcmenu" value="<%=src%>"/>
						<jsp:param name="repcat" value="<%=LC.L_Milestones.substring(0,9).toLowerCase() %>"/>
						<jsp:param name="calledfrom" value="financial"/>
						<jsp:param name="studyPk" value="<%=studyPk%>"/>
						<jsp:param name="studyNumber" value="<%=studynumber%>"/>
					</jsp:include>	

			<%
				}    //end of else body for page right
				else
				{      
			%>
					<jsp:include page="accessdenied.jsp" flush="true"/>
			<%
				}
			}
			
			else
			{
		%>

		<jsp:include page="timeout.html" flush="true"/>

	<%
	}
	%>
	
	<div> 
    	<jsp:include page="bottompanel.jsp" flush="true"/>
  	</div>
  	
	</div>
	
	<div class ="mainMenu" id="emenu"> 
  		<jsp:include page="getmenu.jsp" flush="true"/>
	</div>

</body>

</html>