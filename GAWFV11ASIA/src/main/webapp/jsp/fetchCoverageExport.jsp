<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.web.grpRights.GrpRightsJB"%>
<%@page import="com.velos.eres.web.studyRights.StudyRightsJB"%>
<%@page import="com.velos.eres.service.util.Configuration,com.velos.eres.service.util.EJBUtil"%>
<%@page import="com.velos.eres.service.util.Security"%>
<%@page import="com.velos.esch.web.protvisit.ProtVisitCoverageJB"%>
<%@page import="com.velos.esch.service.util.Rlog"%>
<%@page import="java.io.FileOutputStream,java.io.OutputStreamWriter"%>
<%@page import="java.text.DecimalFormat,java.util.ArrayList"%>
<%@page import="org.json.JSONObject,org.json.JSONArray,org.json.JSONException,com.velos.eres.service.util.*"%>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<%@page import="com.velos.esch.business.common.SchCodeDao"%>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<html>
<head><title><%=LC.L_Coverage_AnalysisExport%><%--Coverage Analysis Export*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META CONTENT="no-cache" HTTP-EQUIV="PRAGMA">
<META CONTENT="private" HTTP-EQUIV="CACHE-CONTROL">
<META CONTENT="no-store" HTTP-EQUIV="CACHE-CONTROL">
<META CONTENT="must-revalidate" HTTP-EQUIV="CACHE-CONTROL">
<META CONTENT="post-check=0,pre-check=0" HTTP-EQUIV="CACHE-CONTROL">
<META CONTENT="-1" HTTP-EQUIV="Expires">
</head>
<body>  
<%
HttpSession tSession = request.getSession(false);
String tableName = null;
String protocolId = request.getParameter("protocolId");
String duration = null;
String sessId = tSession.getId();
if (sessId.length()>8) { sessId = sessId.substring(0,8); }
sessId = Security.encrypt(sessId);
char[] chs = sessId.toCharArray();
StringBuffer sb1 = new StringBuffer();	
DecimalFormat df = new DecimalFormat("000");
for (int iX=0; iX<chs.length; iX++) {
    sb1.append(df.format((int)chs[iX]));
}
String keySessId = sb1.toString();

if (!keySessId.equals(request.getParameter("key"))) {
%>
    <h1><%=LC.L_Forbidden%><%--Forbidden*****--%></h1>
    <p><%=MC.M_NotPerm_ToAcesServer%><%--You do not have permission to access this server or file.*****--%></p>    
    </body>
    </html>
<%  
    return;
}
if (sessionmaint.isValidSession(tSession)){
	ProtVisitCoverageJB pvCoverageJB = new ProtVisitCoverageJB();
	
	String calledFrom = request.getParameter("calledFrom");
	
	String exportToFlag = request.getParameter("exportToFlag")==null?"":request.getParameter("exportToFlag");
	
	String requestURL = StringUtil.encodeString(request.getRequestURL().toString());
	String queryString = StringUtil.encodeString(request.getQueryString());
    int pageRight = 0;
    int finDetRight = 0;
	if (calledFrom.equals("S")) {
		StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");
		if ((stdRights.getFtrRights().size()) == 0){
			pageRight= 0;
			finDetRight = 0;
		} else {
			pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));
			finDetRight=Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFIN"));
		}
	} else if ((calledFrom.equals("P")) || (calledFrom.equals("L"))) {
		GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));
		finDetRight = pageRight;
	}
	if (pageRight > 0) {
		String jsString = null;
		JSONObject jsObj = null;
		try {
			if(exportToFlag.equalsIgnoreCase("exportToFlagofprscode")){
				jsString = pvCoverageJB.fetchCoverageJSONExportToFlag(protocolId, calledFrom, finDetRight);
			}
			else{
				jsString = pvCoverageJB.fetchCoverageJSON(protocolId, calledFrom, finDetRight);
			}
		    
			jsObj = new JSONObject(jsString);
		} catch(Exception e) {
		    Rlog.fatal("fetchCoverageExport", "Error while calling fetchCoverageJSON "+e);
		}
		if (jsObj == null) { jsObj = new JSONObject(); }
		String calName = (String)jsObj.get("protocolName");
		JSONArray colArray = (JSONArray)jsObj.get("colArray");
		JSONArray dataArray = (JSONArray)jsObj.get("dataArray");
		String format = request.getParameter("format");
		
		
		
		String filename = null;
		String contentApp = null;
		if ("excel".equals(format)) {
		    filename = "reportexcel["+System.currentTimeMillis()+"].xls" ;
		    contentApp = "application/vnd.ms-excel; ";
		} else if ("word".equals(format)) {
		    filename = "reportword["+System.currentTimeMillis()+"].doc" ;
		    contentApp = "application/vnd.ms-word; ";
		} else if ("html".equals(format)) {
		    filename = "reporthtml["+System.currentTimeMillis()+"].html" ;
		    contentApp = "text/html; ";
		}
		String filenamexls = "reportexcel["+System.currentTimeMillis()+"].xls" ;
		
		StringBuffer sb = new StringBuffer();
		StringBuffer sb2 = new StringBuffer();
		sb.append("<HTML><HEAD>");
		sb.append("<META http-equiv=\"Content-Type\" content=\"").append(contentApp).append("charset=UTF-8\"/>");
		sb.append("<META CONTENT=\"no-cache\" HTTP-EQUIV=\"PRAGMA\">");
		sb.append("<META CONTENT=\"private\" HTTP-EQUIV=\"CACHE-CONTROL\">");
		sb.append("<META CONTENT=\"no-store\" HTTP-EQUIV=\"CACHE-CONTROL\">");
		sb.append("<META CONTENT=\"must-revalidate\" HTTP-EQUIV=\"CACHE-CONTROL\">");
		sb.append("<META CONTENT=\"post-check=0,pre-check=0\" HTTP-EQUIV=\"CACHE-CONTROL\">");
		sb.append("<META CONTENT=\"-1\" HTTP-EQUIV=\"Expires\">");
		sb.append("</HEAD>");
		sb.append("<BODY>");
		sb.append("<table><tr><td colspan=\"3\"><p style=\"font-weight:bold;background-color: #e1dce0;\">");
		
		
				  if(exportToFlag.equalsIgnoreCase("exportToFlagofprscode")){
					  sb.append(MC.M_STUDY_CALENDAR_REPORT/*Coverage Analysis for Calendar*****/+": ").append(calName);										  
				  }else{
					  sb.append(MC.M_Cvrg_AnalyForCal/*Coverage Analysis for Calendar*****/+": ").append(calName).append("</p></td></tr></table>");
				  }
		
				  
				  
		if (dataArray.length() > 0) {
		    ArrayList visitIdList = new ArrayList();
		    sb2.append("<TABLE BORDER='1' ALIGN='CENTER' WIDTH='100%'>");
		    if (colArray.length() > 0) {
		        sb2.append("<tr>");
		        for (int iX=0; iX<colArray.length(); iX++) {
		            String key = (String)((JSONObject)colArray.get(iX)).get("key");
		            String hideExport = 
		            ((JSONObject)colArray.get(iX)).has("hideExport")?
		            		(String)((JSONObject)colArray.get(iX)).get("hideExport") : "false";

		            if ("eventId".equals(key)) { continue; }
		            if ("true".equals(hideExport)) { continue; }
		            
		            sb2.append("<th>").append(((JSONObject)colArray.get(iX)).get("label")).append("</th>");
		            if (key.startsWith("v")) { visitIdList.add(key); }
		        }
		        sb2.append("</tr>");
		    }
		    int noteCounter = 0;
		    ArrayList notesList = new ArrayList();
		    for (int iX=0; iX<dataArray.length(); iX++) {
		        JSONObject rowData = (JSONObject)dataArray.get(iX);
			    sb2.append("<tr>");
			    sb2.append("<td>").append(rowData.get("event")).append("</td>");
			    sb2.append("<td>").append(rowData.get("eventCPT")).append("</td>");
			
			    
			    if(exportToFlag.equalsIgnoreCase("exportToFlagofprscode")){
			    	
			    	sb2.append("<td>").append(rowData.get("prscodes")).append("</td>");
			    	sb2.append("<td>").append(rowData.get("servicecodes")).append("</td>");			   	
				   			    	
			    }else{
			    	sb2.append("<td>").append(rowData.get("eventAddCode")).append("</td>");
			    	
			    }
			    for (int iV=0; iV<visitIdList.size(); iV++) {
			        sb2.append("<td>");
			        String cellData = null;
			        try { cellData = (String)rowData.get((String)visitIdList.get(iV)); } catch(JSONException e) {}
			        if (cellData != null) {
			            if (!cellData.startsWith("<b>"+LC.L_X/*X*****/+"</b>")) { cellData = "<b>"+LC.L_X/*X*****/+"</b> "+cellData;}
			            sb2.append(cellData);
			        }
			        String notesData = null;
			        try { notesData = (String)rowData.get("notes_"+visitIdList.get(iV)); } catch(JSONException e) {}
			        if (notesData != null && notesData.trim().length() > 0) {
			            sb2.append(" &#167;<sup>").append(++noteCounter).append("</sup>");
			            notesList.add(notesData);
			        }
			        if (cellData == null && (notesData == null || notesData.trim().length() == 0)) {
			            sb2.append("");
			        }
			        sb2.append("</td>");
			    }
		        sb2.append("<td>").append(rowData.get("eventCovNotes")).append("</td>");
		        sb2.append("</tr>");
		    }
		    
		    sb2.append("</TABLE>");
		    sb2.append("<br/><table><tr><td><b>X</b>="+LC.L_Evt_VstSel/*Event-Visit selected*****/+"</td></tr></table>");
		    sb2.append("<br/><table><tr><td colspan=\"3\">").append(jsObj.get("covTypeLegend")).append("</td></tr></table>");
		    
		    if (notesList.size() > 0) {
		        sb2.append("<br/><table>");
		        sb2.append("<tr><td colspan=\"3\">&#167;</td></tr>");
		        for (int iX=0; iX<notesList.size(); iX++) {
		            sb2.append("<tr valign='top'><td align='left' colspan=\"3\">").append(iX+1).append(" &ndash; ");
		            sb2.append((String)notesList.get(iX)).append("</td></tr>");
		        }
		        sb2.append("</table>");
		    }
		}
		
		
				
		
	///////////////////////////////////////////////////////////////////
	StringBuffer urlParamSBForCACal = null;
	String calassoc = StringUtil.trueValue(request.getParameter("calassoc"));
	if (pageRight > 0) {

	} // end of pageRight > 0
	protocolId = request.getParameter("protocolId");
	if(protocolId == null || protocolId == "" || protocolId.equals("null") || protocolId.equals("")) {
	} else {  
		  finDetRight = 0;
			if (calledFrom.equals("S")) {
				StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");
				if ((stdRights.getFtrRights().size()) == 0){
					pageRight= 0;
					finDetRight = pageRight;
				} else {
					pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));
					finDetRight=Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFIN"));
				}
			} else if ((calledFrom.equals("P")) || (calledFrom.equals("L"))) {
				GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
				pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));
				finDetRight = pageRight;
			}
		
		
		if (pageRight > 0) {
		    
	 		if("L".equals(calledFrom) || "P".equals(calledFrom)){
		   		tableName ="event_def";
				eventdefB.setEvent_id(EJBUtil.stringToNum(protocolId));
				eventdefB.getEventdefDetails();
		   	} else {
		   		tableName ="event_assoc";
				eventassocB.setEvent_id(EJBUtil.stringToNum(protocolId));
				eventassocB.getEventAssocDetails();
		   	}
	 	}
		
		 duration = StringUtil.trueValue(request.getParameter("duration"));
		 urlParamSBForCACal = new StringBuffer();
		 urlParamSBForCACal.append("protocolId=").append(StringUtil.htmlEncodeXss(protocolId))
		.append("&calledFrom=").append(StringUtil.htmlEncodeXss(calledFrom))
		.append("&calledfrom=").append(StringUtil.htmlEncodeXss(calledFrom))
		.append("&duration=").append(StringUtil.htmlEncodeXss(duration))
		.append("&calassoc=").append(StringUtil.htmlEncodeXss(calassoc))
		.append("&calstatus=").append(StringUtil.htmlEncodeXss(request.getParameter("calStatus")))
		.append("&mode=").append(StringUtil.htmlEncodeXss(request.getParameter("mode")))
		.append("&calProtocolId=").append(StringUtil.htmlEncodeXss(protocolId))
		.append("&tableName=").append(tableName)
		.append("&pr=").append(pageRight)
		.append("&fin=").append(finDetRight)
		.append("&fin=").append(finDetRight)
		;
		
		
 
	
	}
	
	
	//////////////////////////////////
	
	sb2.append("<script type='text/javascript'>");		
		sb2.append("function openExport(format) {");
		sb2.append("var newWin = window.open('donotdelete.html','exportWin','toolbar=no,scrollbars=yes,resizable=yes,menubar=yes,status=no,left=20,top=20');");
		sb2.append("newWin.document.write(\"<html>\"); ");
		sb2.append("newWin.document.write(\"<head><title>"+LC.L_Coverage_AnalysisExport+"</title></head><body>\");");
		//newWin.document.write('<head><title>Coverage Analysis Export</title></head><body>\");");
		sb2.append("newWin.document.write(\"<form name='dummy' target='_self' action='http://"+request.getServerName()+":"+request.getServerPort()+"/velos/jsp/fetchCoverageExport.jsp?exportToFlag=exportToFlagofprscode&\");");
		
		
		sb2.append("newWin.document.write(\"<input type='hidden' id='format' name='format' value='"+format+"'/>\"); ");
		sb2.append("newWin.document.write(\"<input type='hidden' id='protocolId' name='protocolId' value='"+request.getParameter("duration")+"'/>\"); ");
		sb2.append("newWin.document.write(\"<input type='hidden' id='calledFrom' name='calledFrom' value='"+calledFrom+"'/>\"); ");
		 
		sb2.append("newWin.document.write(\"<input type='hidden' id='calledfrom' name='calledfrom' value='"+StringUtil.htmlEncodeXss(calledFrom)+"'/>\"); ");
		sb2.append("newWin.document.write(\"<input type='hidden' id='duration' name='duration' value='"+StringUtil.htmlEncodeXss(duration)+"'/>\"); ");
		sb2.append("newWin.document.write(\"<input type='hidden' id='calassoc' name='calassoc' value='"+calassoc+"'/>\"); ");
		
		sb2.append("newWin.document.write(\"<input type='hidden' id='calstatus' name='calstatus' value='"+StringUtil.htmlEncodeXss(request.getParameter("calStatus"))+"'/>\"); ");
		//sb2.append("newWin.document.write(\"<input type='hidden' id='calstatus' name='calstatus' value='A'/>\"); ");
		
		sb2.append("newWin.document.write(\"<input type='hidden' id='mode' name='mode' value='"+StringUtil.htmlEncodeXss(request.getParameter("mode"))+"'/>\"); ");
		sb2.append("newWin.document.write(\"<input type='hidden' id='calassoc' name='calassoc' value='"+calassoc+"'/>\"); ");
		
		sb2.append("newWin.document.write(\"<input type='hidden' id='calProtocolId' name='calProtocolId' value='"+StringUtil.htmlEncodeXss(protocolId)+"'/>\"); ");
		sb2.append("newWin.document.write(\"<input type='hidden' id='tableName' name='tableName' value='"+tableName+"'/>\"); ");
			
		sb2.append("newWin.document.write(\"<input type='hidden' id='key' name='key' value='"+keySessId+"'/>\"); ");
		sb2.append("newWin.document.write(\"<input type='hidden' id='filename' name='filename' value='"+filenamexls+"'/>\"); ");
		//sb2.append("newWin.document.write(\"<input type='hidden' id='format' name='format' value='excel'/>\"); ");
		//sb2.append("newWin.document.write(\"<input type='hidden' id='filename' name='filename' value='"+filename+"'/>\"); ");
	 
		
		sb2.append("newWin.document.write(\"</form></body></html>\"); ");
		sb2.append("newWin.document.close(); ");
		sb2.append("newWin.document.dummy.submit(); ");
		sb2.append("} ");
		sb2.append("</script> ");
		sb2.append("</BODY></HTML>");

		
		
		Configuration.readAppendixParam(Configuration.ERES_HOME+"eresearch.xml");
		FileOutputStream fos = null;
		OutputStreamWriter osw = null;
		String filepath = null;
		String path=null;
		try {
			path=Configuration.DOWNLOADFOLDER;
	        filepath = EJBUtil.getActualPath(Configuration.DOWNLOADFOLDER, filename);
		    fos = new FileOutputStream(filepath);
		    osw = new OutputStreamWriter(fos);
		    
		    char[] c = null;
		    if(exportToFlag.equalsIgnoreCase("exportToFlagofprscode")){
		    	c = (sb.toString()
		    			+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='#' onclick=\"openExport('excel')\" id='eebtn'><img style='cursor: hand'; src='http://"+request.getServerName()+":"+request.getServerPort()+"/velos/images/jpg/excel.GIF'/></a></p> "
		    			+sb2.toString()).toCharArray();
			  }else{
				  	c = (sb.toString()+sb2.toString()).toCharArray();
			  }
		    osw.write(c, 0, c.length);
		    osw.flush();
		    fos.flush();
		} catch(Exception e) {
		    Rlog.fatal("fetchCoverageExport", "Error creating download file: "+e);
		} finally {
		    try { osw.close(); } catch(Exception e) {}
		    try { fos.close(); } catch(Exception e) {}
		}
		
		
		Configuration.readAppendixParam(Configuration.ERES_HOME+"eresearch.xml");
		FileOutputStream fos2 = null;
		OutputStreamWriter osw2 = null;
		String filepath2 = null;
		String path2=null;
		try {
			path=Configuration.DOWNLOADFOLDER;
	        filepath = EJBUtil.getActualPath(Configuration.DOWNLOADFOLDER, filenamexls);
		    fos = new FileOutputStream(filepath);
		    osw = new OutputStreamWriter(fos);
		    char[] c = (sb.toString()+sb2.toString()).toCharArray(); 
		    osw.write(c, 0, c.length);
		    osw.flush();
		    fos.flush();
		} catch(Exception e) {
		    Rlog.fatal("fetchCoverageExport", "Error creating download file: "+e);
		} finally {
		    try { osw.close(); } catch(Exception e) {}
		    try { fos.close(); } catch(Exception e) {}
		}

		
		
%>

		<form name="dummy" method="post" action="/velos/servlet/Download" target="_self">
		<%if(filename == null){
			filename = request.getParameter("filename");} %>
			<input type ="hidden" name="file" id="file" value="<%=filename%>"/>
			<input type ="hidden" name="mod" id="mod" value="R"/>
			<%--Changes For INF-22402 By :YPS --%>
			<input type="hidden" name="moduleName" value="<%=LC.L_Calendar%>" />
			<input type="hidden" name="tableName" value="" />
			<input type="hidden" name="filePath" value="<%=StringUtil.encodeString(path)%>"/>
			<input type="hidden" name="requestURL" value="<%=requestURL%>" />
			<input type="hidden" name="queryString" value="<%=queryString%>" />
		</form>
<script type='text/javascript'>
document.dummy.submit();
</script>
<%
	} // end of pageRight
	else {
	%>
		<jsp:include page="accessdenied.jsp" flush="true"/>
	<%
	} // end of else of pageRight
} else { // else of valid session
%>
<jsp:include page="timeout.html" flush="true"/>
<%
} // end of else of valid session
%>
<div> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</div>
<div class ="mainMenu" id = "emenu" > 
  <jsp:include page="getmenu.jsp" flush="true"/> 
</div>
</body>
</html>