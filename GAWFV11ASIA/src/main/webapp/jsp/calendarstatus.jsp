<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title><%=LC.L_Cal_Status%><%--Calendar Status*****--%></title>



<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,java.text.*" %>

<%@ page import="com.velos.esch.business.common.SchCodeDao,com.velos.eres.service.util.*,com.velos.eres.web.user.UserJB,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.user.UserJB"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>



<SCRIPT Language="javascript">

 function  validate(formobj){




     if (!(validate_col('Status',formobj.calStatus))) return false
     if (!(validate_col('Date',formobj.StatusDate))) return false;
     if (!(validate_col('Status Changed By',formobj.ChangedBy))) return false;
	 if (formobj.calStatus.value==formobj.prevStat.value){
		 alert("<%=MC.M_Chg_TheStat%>");/*alert("Please change the status");*****/
		return false;
		}

	//For PCAL-22446  By :Parminder Singh
		if (formobj.calassoc.value=='P' && formobj.calStatus.value=='A'|| formobj.calStatus.value=='O')
		    {
	         if(!(validate_chk_radio("at least one option",formobj.statusflag)))
	         {   
	         return false;
	         }
		    }


	//JM: 30May2008, atlease one option should be selected
	//JM: 30June2008, modified for the issue #3551
	if (formobj.prevStat.value=='O' && formobj.calassoc.value=='P'){
		if ( (formobj.calStatus.value=='A') &&  (formobj.patSchChk[0].checked==false)&&(formobj.patSchChk[1].checked==false)&&(formobj.patSchChk[2].checked==false)){
			alert("<%=MC.M_SelAtLeast_OneOpt%>");/*alert("Please select at least one option");*****/
			return false;
		}
	}

//JM: 30June2008, #3592
if (formobj.prevStat.value=='O' && formobj.calassoc.value=='P'){
if (formobj.patSchChk[2].checked==true){

	if ( (formobj.patstat.value !='') && (formobj.dateTxt.value=='')){
		alert("<%=MC.M_EtrDtFor_PatStat%>")/*alert("Please enter the Date for the <%=LC.Pat_Patient%> Status")*****/
		return false;
	}
	else if((formobj.patstat.value =='')  && (formobj.dateTxt.value!='')){
		alert("<%=MC.M_Selc_PatStatus%>")/*alert("Please select the <%=LC.Pat_Patient%> Status")*****/
		return false;
	}
	else if ((formobj.patstat.value =='') && (formobj.dateTxt.value=='')){
		alert("<%=MC.M_SelPatStat_EtrDt%>")/*alert("Please select the <%=LC.Pat_Patient%> Status and enter the Date")*****/
		return false;
	}

}}



	 if (!(validate_col('e-Signature',formobj.eSign))) return false

     <%-- if(isNaN(formobj.eSign.value) == true) {
    	 alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
	  } --%>

		
		  
   }

</SCRIPT>

<SCRIPT language="JavaScript1.1">
function openwin12() {
      windowName=window.open("usersearchdetails.jsp?from=calenderstatus&fname=&lname=","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
	windowName.focus();
;}
</SCRIPT>


<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*"%>


<% String src;
String studyIdtabs="";
HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession))

{
	if(request.getParameter("studyId")!=null && !request.getParameter("studyId").equals("")){
		studyIdtabs = request.getParameter("studyId");
	}
}
String from = "calenderstatus";
src= request.getParameter("srcmenu");

%>

<jsp:include page="panel.jsp" flush="true">

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>



<body>
<DIV class="BrowserTopn" id="div1">
<jsp:include page="studytabs.jsp" flush="true">
<jsp:param name="from" value="<%=from%>"/>
<jsp:param name="studyId" value="<%=studyIdtabs %>"/>
 </jsp:include>
</DIV>
<% if("Y".equals(CFG.Workflows_Enabled)&& (studyIdtabs!=null && !studyIdtabs.equals("") && !studyIdtabs.equals("0"))){ %>
<DIV class="BrowserBotN BrowserBotN_S_3 workflowDivBig">
<%}else{ %>
<DIV class="BrowserBotN BrowserBotN_S_3">
<%} %>
  <%

	String protocolId ="";
	String protocolName ="";
 	String desc ="";
	String durationNum="";
	String notes="";
	String changedOn="";
	String changedBy="";
	String UserFName="";
	String UserLName="";
	String mode = request.getParameter("mode");
	//KM-#5882
	String studyId = request.getParameter("studyId");
	String patSchUpdate ="";//For PCAL-22446  By :Parminder Singh
	String calStatus = "";
	if (sessionmaint.isValidSession(tSession))

	{
		String uName = (String) tSession.getValue("userName");
		String usr = (String) tSession.getValue("userId");

		String  calAssoc=request.getParameter("calassoc");
		calAssoc=(calAssoc==null)?"":calAssoc;

		int pageRight = 0;
 		StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");
	  	if ((stdRights.getFtrRights().size()) == 0){
		 	pageRight= 0;
		}else{
			pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));
		}

	if (pageRight >=4)
	{
		protocolId = request.getParameter("protocolId");
		eventassocB.setEvent_id(EJBUtil.stringToNum(protocolId));

		eventassocB.getEventAssocDetails();
		protocolName = eventassocB.getName();
		notes=eventassocB.getNotes();
		if(notes==null)
			notes="";
		changedOn=eventassocB.getStatusDate();
		changedBy=eventassocB.getStatusUser();
		
		UserJB user=new UserJB();
		user.setUserId(EJBUtil.stringToNum(changedBy));
		user.getUserDetails();
		
		UserFName = user.getUserFirstName();
		UserLName = user.getUserLastName();
		//For PCAL-22446  By :Parminder Singh
		patSchUpdate = eventassocB.getPatSchUpdate();//PCAL-22446 Enhancement
		patSchUpdate=(patSchUpdate==null || patSchUpdate.equalsIgnoreCase("X") )?"":patSchUpdate;

		//JM: 08FEB2011, #D-FIN9
		//calStatus = eventassocB.getStatus();
		SchCodeDao cd = new SchCodeDao();
		calStatus = cd.getCodeSubtype(EJBUtil.stringToNum(eventassocB.getStatCode()));
		calStatus = (calStatus==null)?"":calStatus;
		desc = eventassocB.getDescription();

	
		//KM-#4516
		if (desc == null || desc.equals("null"))
			desc ="";
		durationNum = eventassocB.getDuration();
		calAssoc=eventassocB.getCalAssocTo();//JM: 05Aug2008


		//JM: 28Apr2008, added for, Enh. #C9

		//String selStat = request.getParameter("patstat");
		//selStat = (selStat==null)?"":selStat;

		String dEvStat ="";


		CodeDao cDao = new CodeDao();
		cDao.getCodeValues("patStatus",0);


		StringBuffer sb = new StringBuffer();

		int cnt = 0;
	 	sb.append("<SELECT NAME='patstat'>");
	 	sb.append("<Option value='' Selected>"+LC.L_Select_AnOption+"</option>") ;/*sb.append("<Option value='' Selected>Select an Option</option>") ;*****/
		for (cnt = 0; cnt <= cDao.getCDesc().size() -1 ; cnt++)
		{
			sb.append("<OPTION value = "+ cDao.getCId().get(cnt)+">" + cDao.getCDesc().get(cnt)+ "</OPTION>");
		}

		sb.append("</SELECT>");
		dEvStat = sb.toString();



	//JM: 08FEB2011, #D-FIN9
		StringBuffer sbCalStat = new StringBuffer();
		int cnt1 = 0;
		String dCalStat = "";
  		SchCodeDao calStatDao = new SchCodeDao();


		if (pageRight == 4) {
		%>
		   <P class = "defComments"><FONT class="Mandatory"><%=MC.M_OnlyViewPerm_ForEvt%><%--You have only View permission for the Event.*****--%></Font></P>
		<%} %>
	<P class = "defComments"> <%=MC.M_Chg_TheStat%><%--Please change the Status*****--%> </P>

 <Form name="calendarStatus" id ="calStatFrm" method="post" action="updateprotocolstatus.jsp" onsubmit="if (validate(document.calendarStatus)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
 
    <table width="100%" cellspacing="0" cellpadding="0" border="0" class="basetbl">
      <tr>
        <td width="30%"> <%=LC.L_Protocol_CalName%><%--Protocol Calendar Name*****--%></td>
        <td width=70%>
          <input type="text" name="protocolName" size = 15 MAXLENGTH = 50 	value="<%=protocolName%>" READONLY>
        </td>
	    <input type="hidden" name="desc" size = 15 MAXLENGTH = 50 	value="<%=desc%>" >

	    <input type="hidden" name="durationNum" size = 15 MAXLENGTH = 50 value="<%=durationNum %>">

 		<input type="hidden" name="prevStat" size = 15 MAXLENGTH = 50 value="<%= calStatus %>">

	    <input type="hidden" name="srcmenu" size = 15 MAXLENGTH = 50 value="<%=src%>">
	    
	    <input type="hidden" name="srcmenu" size = 15 MAXLENGTH = 50 value="<%=studyIdtabs%>">


      </tr>
      <tr>
       <!--KM-#5925-->
		<td > <%=LC.L_Status%><%--Status*****--%> <FONT class="Mandatory">* </FONT> </td>
        <td >
<%
    //JM: 08FEB2011, #D-FIN9: Study team role based calendar status access
    	String roleCodePk="";

	    //KM-#5882
		if (! StringUtil.isEmpty(studyId) && EJBUtil.stringToNum(studyId) > 0)
		{
			ArrayList tId = new ArrayList();

			TeamDao teamDao = new TeamDao();
			teamDao.getTeamRights(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(usr));
			tId = teamDao.getTeamIds();

			if (tId != null && tId.size() > 0)
			{
				ArrayList arRoles = new ArrayList();
				arRoles = teamDao.getTeamRoleIds();

				if (arRoles != null && arRoles.size() >0 )
				{
					roleCodePk = (String) arRoles.get(0);

					if (StringUtil.isEmpty(roleCodePk))
					{
						roleCodePk="";
					}
				}
				else
				{
					roleCodePk ="";
				}

			}
			else
			{
				roleCodePk ="";
			}
		} else
			{
			  roleCodePk ="";

			}


		
	    //JM: 08FEB2011, #D-FIN9: Study team role based calendar status access
	    //calStatDao.getCodeValuesForStudyRole("calStatStd",roleCodePk);
			calStatDao.getCodeValuesForStudyRoleHideCustom("calStatStd",roleCodePk);

		String subTypeVar = "";
		String cdDescVar = "";
		int len = calStatDao.getCDesc().size();


    sbCalStat.append("<select  name=\"calStatus\" >");

	//KM-#5925
	sbCalStat.append("<OPTION value ='' SELECTED>"+LC.L_Select_AnOption+"</OPTION>");/*sbCalStat.append("<OPTION value ='' SELECTED>Select an Option</OPTION>");*****/
	
for (cnt1 = 0; cnt1 <=  len-1 ; cnt1++){

	subTypeVar = (String)calStatDao.getCSubType().get(cnt1);
	cdDescVar = (String)calStatDao.getCDesc().get(cnt1);

	if ((!(calStatus ==null)) && (calStatus.equals("W"))) {

		if (!subTypeVar.equals("O")){
			if (subTypeVar.equals("W")){
				sbCalStat.append("<OPTION value="+ subTypeVar+" selected>" + cdDescVar+ "</OPTION>");
			}else{
				sbCalStat.append("<OPTION value="+ subTypeVar+">" + cdDescVar+ "</OPTION>");
			}
		}
	} else if ((!(calStatus ==null)) && (calStatus.equals("A"))) {
		if (!subTypeVar.equals("W")){
			if (!subTypeVar.equals("O")){
				if (subTypeVar.equals("A")){
					sbCalStat.append("<OPTION value="+ subTypeVar+" selected>" + cdDescVar+ "</OPTION>");
				}else if (subTypeVar.equals("D")){  // YK DFIN9 Bugs #5875
					sbCalStat.append("<OPTION value="+ subTypeVar+">" + cdDescVar+ "</OPTION>");
				}
			}else if (calAssoc.equals("P") && subTypeVar.equals("O")){
				sbCalStat.append("<OPTION value="+ subTypeVar+">" + cdDescVar+ "</OPTION>");
			}
		}
	} else if ((!(calStatus ==null)) && (calStatus.equals("O")) ) {
		if (!subTypeVar.equals("W")){
			if (!subTypeVar.equals("O") && (subTypeVar.equals("D") || subTypeVar.equals("A"))){  // YK DFIN9 Bugs #5876
				sbCalStat.append("<OPTION value="+ subTypeVar+">" + cdDescVar+ "</OPTION>");
			}else if (calAssoc.equals("P") && subTypeVar.equals("O")){
				sbCalStat.append("<OPTION value="+ subTypeVar+" Selected>" + cdDescVar+ "</OPTION>");
			}
		}
	} else if ((!(calStatus ==null)) && (calStatus.equals("D"))) {
		if (subTypeVar.equals("D")){  // YK DFIN9 Bugs #5877 & #5881
				sbCalStat.append("<OPTION value="+ subTypeVar+" selected>" + cdDescVar+ "</OPTION>");
		}
	} else{
		if (!subTypeVar.equals("O")){
			if(calStatus.equals(subTypeVar)) // YK DFIN9 Bugs #5878 & #5881
			{
				sbCalStat.append("<OPTION value="+ subTypeVar+" selected>" + cdDescVar+ "</OPTION>");
			}else
			{
				sbCalStat.append("<OPTION value="+ subTypeVar+" >" + cdDescVar+ "</OPTION>");
			}
		}

	}

}

sbCalStat.append("</SELECT>");
dCalStat = sbCalStat.toString();

%><%=dCalStat%>

          <A href="calendarstatusmodes.htm" target="Information" onClick="openwin()"><%=MC.M_What_ThisMean%><%--What does this mean?*****--%></A>
			</td>
		</tr>
<%-- INF-20084 Datepicker-- AGodara --%>
		<tr>
        	<td	> <%=LC.L_Date%><%--Date*****--%> <FONT class="Mandatory">* </FONT></td>
        	<td ><input type="text" name="StatusDate" value="<%=changedOn %>" class="datefield" size = 15 MAXLENGTH = 50	READONLY></td>
	</tr>
	<tr>
        <td > <%=LC.L_Status_ChangedBy%><%--Status Changed By*****--%> <FONT class="Mandatory">* </FONT> </td>
        <td>
          <input type="text" name="ChangedBy" size = 20 MAXLENGTH = 50 READONLY	value="<%=UserFName+" "+UserLName%>">
	    <input type="hidden" name="ChangedById" size = 15  value=<%=changedBy%> >
	<A href="#" onclick="openwin12()"><%=LC.L_User_Search%><%--User Search*****--%></A>
	  </td>

	</tr>
	<tr>
        <td > <%=LC.L_Notes%><%--Notes*****--%></td>
        <td  class="textareaheight">
	<TEXTAREA name="Notes"  rows=3 cols=30><%=notes%></TEXTAREA></td>
	</tr>
			<!--Start For PCAL-22446  By :Parminder Singh-->
	<%	if (calAssoc.equals("P"))
		{ %>
			<tr height="3"><td></td></tr>
				<tr>
	<td colspan="3" ><Font class=""><%=MC.M_PatSch_Update%><%--Allow Patient Schedile to be Updated the following ways(you must select at least one option)
*****--%></Font></td>

</tr>
    <tr>
    <td colspan = 2 class=tdDefault>
    
    <Input type="checkbox" name="statusflag" value="1" 
     <% if(patSchUpdate.indexOf("1")!=-1)
	  {%> checked="true" <%} %>
    > <Font class=""><%=MC.M_MoveThisEvt_only%><%--Move this event only.*****--%></Font>
    </td>
	</tr>

<!--     <tr>
    <td colspan = 2 class=tdDefault>
    <Input type="checkbox" name="statusflag" value="2" 
    <% if(patSchUpdate.indexOf("2")!=-1)
	  {%> checked="true" <%} %>
    > <Font class=""><%=MC.M_MoveEvtSchOnCur_VisitAccrd%><%--Move the events scheduled on current and next visit accordingly.*****--%></Font>


    </td>
  </tr>
 -->
 
  <tr>
    <td colspan = 2 class=tdDefault>
    <Input type="checkbox" name="statusflag" value="3" 
    <% if(patSchUpdate.indexOf("3")!=-1)
	  {%> checked="true" <%} %>
    >	<Font class=""><%=MC.M_MoveAllEvtsVisit%><%--Move all events of this visit.*****--%></Font>
    </td>
  </tr>
 
   <tr>
    <td colspan = 2 class=tdDefault>
    <Input type="checkbox" name="statusflag" value="4" 
    <% if(patSchUpdate.indexOf("4")!=-1)
	  {%> checked="true" <%} %>
    >	<Font class=""><%=MC.M_MoveSbqt_depEvts_Accrd%><%--Move all dependent subsequent events accordingly.*****--%></Font>
    </td>
  </tr>
 
 
    <tr>
    <td colspan = 2 class=tdDefault>
    <Input type="checkbox" name="statusflag" value="0" 
    <% if(patSchUpdate.indexOf("0")!=-1)
	  {%> checked="true" <%} %>
    >	<Font class=""><%=MC.M_MoveSbqtEvts_Accrd%><%--Move All subsequent events accordingly.*****--%></Font>
    </td>
  </tr>

 

  		<%}%>
	<!--End For PCAL-22446  By :Parminder Singh-->


      <tr>
        <td>
          <input type="hidden" name="protocolId" MAXLENGTH = 15 value="<%=protocolId%>">
         <input type="hidden" name="calassoc" MAXLENGTH = 15 value="<%=calAssoc%>">
        </td>
      </tr>
    <input type="hidden" name="src" MAXLENGTH = 15 value="<%=src%>">
</table>
<!--//JM: 28Apr2008, added for, Enh. #C9 -->

<table width="100%" cellspacing="0" cellpadding="0">
 	<%	if (calStatus.equals("O")  && calAssoc.equals("P") )
		{ %>

<br>
				<P class = "defComments"><FONT class="Mandatory"><%=MC.M_Hide_UnhideSch%><%-- Hide and Unhide feature will work only for the New Schedules.*****--%></Font></P>

				<tr>
				<td >
				<input type="radio" name="patSchChk" value="1"><%=MC.M_ToAllPatSch_OnCal%><%-- Apply to all <%=LC.Pat_Patient%> Schedules based on this Calendar*****--%>
				</td>
				</tr>

				<tr>
				<td >
				<input type="radio" name="patSchChk" value="2"><%=MC.M_ApplyCurPatSch_IgnorSch%><%-- Apply only to 'Current' <%=LC.Pat_Patient%> Schedules based on this Calendar(ignores Discontinued Schedules)*****--%>
				</td>
				</tr>


				<tr>
				<td>
				<input type="radio" name="patSchChk" value="3" ><%=MC.M_ApplyCurPatSch_WithStat%><%-- Apply only to 'Current' <%=LC.Pat_Patient%> Schedules based on this Calendar for <%=LC.Pat_Patients%> with a Status of*****--%>
				</td>
				</tr>
				<tr>
					<td ><%=dEvStat%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<%=LC.L_On_OrAfter%><%--on or after*****--%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<%-- INF-20084 Datepicker-- AGodara --%>				
					<input type="text" name="dateTxt" class="datefield" size="15" READONLY></td>
				</tr>



		<% } else
		{
			%>
				<input type="hidden" name="patSchChk" value="0">
				<input type="hidden" name="patSchChk" value="0">
				<input type="hidden" name="patSchChk" value="0">

			<%
		}%>
</table>

<!--//JM: 28Apr2008, added for, Enh. #C9 -->
<%if ((mode.equals("M") && pageRight >=6) || (mode.equals("N") && (pageRight == 5 || pageRight == 7 )) ) {
	%>

<table width="100%" cellspacing="0" cellpadding="0" border="0" class="basetbl">
	<tr>
		<td>
			<jsp:include page="submitBar.jsp" flush="true"> 
				<jsp:param name="displayESign" value="Y"/>
				<jsp:param name="formID" value="calStatFrm"/>
				<jsp:param name="showDiscard" value="N"/>
			</jsp:include>
		</td>
	</tr>
</table>   
   <%}%>


    <br>

  </Form>
  <%

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</DIV><div class ="mainMenu" id = "emenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/></div>
</body>

</html>

