<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<head>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<%@ page import="com.velos.eres.business.common.*, com.velos.eres.service.util.*,com.velos.eres.web.user.UserJB,com.velos.eres.web.grpRights.GrpRightsJB"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script>
function openWinStatus(patId,patStatPk,pgRight,mode,study,orgRight,currentStatId,po)
{
	var changeStatusMode;
	var permissionCode;

	if (mode == 'N')
	{
		changeStatusMode = "yes";
	}
	else
	{
		changeStatusMode = "no"
	}

	if (mode == 'M')
	{
		permissionCode = 'E'
	}
	else
	{
		permissionCode = mode;
	}



	if (f_check_perm_org(pgRight,orgRight, permissionCode) == true) {
		windowName= window.open("patstudystatus.jsp?patStudiesEnrollingOrg="+po+"&studyId="+study+"&changeStatusMode="+changeStatusMode+"&statid=" + patStatPk + "&pkey=" + patId+"&currentStatId="+currentStatId,"patstatusdetails","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=650,height=560");
		windowName.focus();
	}
}

function f_delete(patStudyStatPk,studyId,pageRight,orgRight,currentStatId) {

	//Added by Manimaran for September Enhancement S8.
	if (patStudyStatPk==currentStatId) {
	    alert("<%=MC.M_CntDel_CurPatStat%>");/*alert("You can not delete this current status for the <%=LC.Pat_Patient_Lower%>");*****/
	    return false;
	}

	if (f_check_perm_org(pageRight,orgRight,'E'))
	{

		if (confirm("<%=MC.M_DoYouWant_DelPatStdStat%>"))/*if (confirm("Do you want to delete this <%=LC.Std_Study_Lower%> status for the <%=LC.Pat_Patient_Lower%>?"))*****/
		{
			windowName= window.open("patstudystatusdelete.jsp?patStudyStatPk="+patStudyStatPk+"&studyId="+studyId,"patstatdel","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=650,height=350");
			windowName.focus();
		} else
		{
			return false
		}
	} else
	{
		return false;
	}//end of edit right
} //end of function

</script>
</head>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

  <%
	String studyId = "";
	String patId = "";
	String note = "";
	String status = "";
	String sDate = "";
	String eDate = "";
	String encodedStudyNum = "";
	String reasonDesc = "";



	PatStudyStatDao ps = new PatStudyStatDao();
	int len = 0;
	int startDateLen = 0;
	int endDateLen = 0;

	String studyNum = "";

	String patStatPk = "";
	String prevPatStudyStatPK = "";

	int pageRight = 0;
	int orgRight = 0;

	int patProtIdInt = 0;
	String patProtId = "";

	studyId= request.getParameter("studyId");
	patId = request.getParameter("patientId");

	pageRight 	= StringUtil.stringToNum(request.getParameter("pageRight"));
	orgRight 	= StringUtil.stringToNum(request.getParameter("orgRight"));
	patProtId = request.getParameter("patProtId");

	String patStudiesEnrollingOrg4New = request.getParameter("patStudiesEnrollingOrg");

	if (StringUtil.isEmpty(patStudiesEnrollingOrg4New) || patStudiesEnrollingOrg4New.equals("null"))
	{
      patStudiesEnrollingOrg4New = "";
	}

	if (!StringUtil.isEmpty(patProtId))
		patProtIdInt = StringUtil.stringToNum(patProtId);
	  else
	  	patProtIdInt = -1;

	encodedStudyNum	=  StringUtil.encodeString(studyNum);

	ps.getPatStatHistory(StringUtil.stringToNum(patId), StringUtil.stringToNum(studyId));


		ArrayList stats = new ArrayList();
		ArrayList notes = new ArrayList();
		ArrayList stDate = new ArrayList();
		//ArrayList endDate = new ArrayList();
		ArrayList patStatPks = new ArrayList();
		ArrayList patientStudyStatusReasons = new ArrayList();
		ArrayList currentStats =  new ArrayList();//KM
		stDate = ps.getPatientStatusStartDate();
		//endDate = ps.getPatientStatusEndDate();

		notes = ps.getPatientStatusNotes();
		stats = ps.getPatientStatusDescs();
		patStatPks = ps.getPatientStatusPk();
		patientStudyStatusReasons = ps.getPatientStudyStatusReasons();
		currentStats = ps.getCurrentStats();
  	  	len= ps.getCRows();

		//Added by Manimaran for September Enhancement S8.
		String currentStatId="";
		String currentVal="";
		for (int i=0;i<len;i++) {
		     currentVal = (currentStats.get(i) == null)?"":(currentStats.get(i)).toString();
		     if ( currentVal.equals("1")) {
			  currentStatId = (String)patStatPks.get(i).toString();
		     }
    		}

		//get latest status
  	  	if (patStatPks.size() > 0 )
  	  	{
  	  		prevPatStudyStatPK = (patStatPks.get(0)).toString();
  	  	}
	%>
 	   <table width="100%" cellspacing="0" cellpadding="0" border=0 >
	   <tr >
	      <td width = "70%">
	        <P class = "defComments"><%=MC.M_ListDisp_PatStd%><%--The list below displays <%=LC.Pat_Patient_Lower%>'s <%=LC.Std_Study_Lower%> status history*****--%>:</P>
	      </td>
	    <td align="center" width = "30%">
	    	<A  id="addnewlink" onclick="openWinStatus('<%=patId%>','<%=prevPatStudyStatPK%>','<%=pageRight%>','N','<%=studyId%>','<%=orgRight%>','<%=currentStatId%>','')"  href ="#" Title="<%=LC.L_Add_NewStatus%><%--Add New Status*****--%>">
	    		<%=LC.L_Add_NewStatus%><%--Add New Status*****--%>
	    	</A>
	    </td>
	    </tr>
	   </table>
   <table width="100%" >
       <tr>
        	<th width="20%"> <%=LC.L_Status%><%--Status*****--%> </th>
        	<th width="20%"> <%=LC.L_Status_Date%><%--Status Date*****--%></th>
	       <!-- <th width="30%"> End Date </th>-->
	    	<th width="20%"> <%=LC.L_Reason%><%--Reason*****--%> </th>
	        <th width="30%"> <%=LC.L_Notes%><%--Notes*****--%> </th>
	        <th width="10%"><%=LC.L_Delete%><%--Delete*****--%></th><!-- Bug#9995 04-Jun-2012 -Sudhir--><!-- Added for UI Review Quick Fixes Enhancement ID:8.03 : Raviesh -->
      </tr>
       <%
	    for(int counter = 0;counter<len;counter++)
		{

		sDate=((stDate.get(counter)) == null)?"-":(stDate.get(counter)).toString();
		startDateLen=sDate.length();

		if (startDateLen > 1) {
			sDate = DateUtil.dateToString(java.sql.Date.valueOf(sDate.substring(0,10)));

		}



		/*eDate=((endDate.get(counter)) == null)?"-":(endDate.get(counter)).toString();
		 endDateLen=eDate.length();
			if (endDateLen > 1) {
			eDate = DateUtil.dateToString(java.sql.Date.valueOf(eDate.substring(0,10)));
		}*/

		status=((stats.get(counter)) == null)?"-":(stats.get(counter)).toString();
		note=((notes.get(counter)) == null)?"-":(notes.get(counter)).toString();
		patStatPk = (patStatPks.get(counter)).toString();
		reasonDesc = ((patientStudyStatusReasons.get(counter)) == null)?"-":(patientStudyStatusReasons.get(counter)).toString();

		if ((counter%2)==0) {
  %>

      <tr class="browserEvenRow">
    <%
		}
	else{
  %>
      <tr class="browserOddRow">
     <%
		}
  %>

		<td align=center ><a onclick="openWinStatus('<%=patId%>','<%=patStatPk%>','<%=pageRight%>','M','<%=studyId%>','<%=orgRight%>','<%=currentStatId%>','')" href="#"><%=status%></a></td>
        <td align=center><%=sDate%> </td>
     	<!--<td align=center>eDate was printed here</td>   -->
     	<td align=center><%=reasonDesc%> </td>
     	<td align=left><%=note%></td>
		<td align="center"><A href="#" onClick="return f_delete(<%=patStatPk%>,'<%=studyId%>',<%=pageRight%>,'<%=orgRight%>','<%=currentStatId%>')"><!--Bug#9995 04-Jun-2012 -Sudhir--> <img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0"/></A></td>
        </tr>

      <%
  }
%>

 </table>
<%
	 if (patProtIdInt == 0)
	 {

		out.println("<script>");

		out.println("openWinStatus('"+ patId + "','"+ prevPatStudyStatPK+ "','"+ pageRight+ "','N','"+ studyId+ "','"+ orgRight+ "','','"+patStudiesEnrollingOrg4New+"')");
		out.println("</script>");

	 }
	 %>
