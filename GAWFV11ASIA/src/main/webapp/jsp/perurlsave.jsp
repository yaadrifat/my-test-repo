<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<HEAD>
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></HEAD>
<%@ page import="java.util.*,com.velos.esch.service.util.EJBUtil,com.velos.eres.service.util.MC" %>
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="perApndxB" scope="page" class="com.velos.eres.web.perApndx.PerApndxJB"/>


<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	<%-- if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   } --%>
}
</SCRIPT>

<% String src;
	src= request.getParameter("srcmenu");

%>

<BODY>
<br>

<%
String mode = request.getParameter("mode");
String perApndxId=request.getParameter("perApndxId");	  
String patId=request.getParameter("patId");
String perApndxDate=request.getParameter("perApndxDate");
String perApndxDesc=request.getParameter("perApndxDesc");
String perApndxUri=request.getParameter("perApndxUri");
String selectedTab=request.getParameter("selectedTab");

String studyId = null;
String statDesc = null;
String statid = null;
String studyVer = null;
studyId = request.getParameter("studyId"); 		
statDesc = request.getParameter("statDesc");
statid = request.getParameter("statid");
studyVer = request.getParameter("studyVer");
studyId = request.getParameter("studyId");
	
String eSign = request.getParameter("eSign");

HttpSession tSession = request.getSession(true);  
String ipAdd = (String) tSession.getValue("ipAdd");
String usr = null;
usr = (String) tSession.getValue("userId");

if (sessionmaint.isValidSession(tSession)) {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%	
	String oldESign = (String) tSession.getValue("eSign");

	if (!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {
%>
	<form METHOD="POST">
  	 <input type="hidden" name="srcmenu" value="<%=src%>">
  	 <input type="hidden" name="perApndxId" value="<%=perApndxId%>">
  	 <input type="hidden" name="mode" value="<%=mode%>">
  	 <input type="hidden" name="patId" value="<%=patId%>">
  	 <input type="hidden" name="selectedTab" value="<%=selectedTab%>">	 	 
	</FORM>
<%
	
		if(mode.equals("M")) {
		   perApndxB.setPerApndxId(EJBUtil.stringToNum(perApndxId));
		   perApndxB.getPerApndxDetails();
		    perApndxB.setModifiedBy(usr);//Added by Manimaran to fix the Bug2959
	   	}

   		perApndxB.setPerApndxDate(perApndxDate);
   		perApndxB.setPerApndxDesc(perApndxDesc);
   		perApndxB.setPerApndxType("U");
		perApndxB.setPerApndxUri(perApndxUri);	
		perApndxB.setPerApndxPerId(patId);
   
	   perApndxB.setIpAdd(ipAdd);
	   //perApndxB.setModifiedBy(usr);
	   
	   if(mode.equals("N")) {
	   	   perApndxB.setCreator(usr);  
		   perApndxB.setPerApndxDetails();
		 }
		else {   
			perApndxB.updatePerApndx();
		}
			
%>

<br><br><br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully.*****--%> </p>

<META HTTP-EQUIV=Refresh CONTENT="1; URL=perapndx.jsp?pkey=<%=patId%>&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&page=patientEnroll&studyId=<%=studyId%>&statDesc=<%=statDesc%>&statid=<%=statid%>&studyVer=<%=studyVer%>&calledFrom=S" >

<%
	}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
 
</BODY>
</HTML>


