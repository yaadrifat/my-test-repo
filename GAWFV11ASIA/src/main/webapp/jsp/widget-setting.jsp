<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
String contextpath=request.getContextPath();
%>
<%@ page import="com.velos.eres.widget.business.domain.Widget,com.velos.eres.widget.service.util.WidgetCache" %>
<%@ page import="java.util.List,com.velos.eres.service.util.*"%>
<% 
WidgetCache widgetCache = WidgetCache.getInstance();
List<Widget> widgetList =  widgetCache.getWidgetListByPageCode(request.getParameter("pageId"));
request.setAttribute("widgetlist",widgetList);
%>
<!--<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery-1.4.4.min.js"></script>
--><script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery-ui-1.8.7.custom.min.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.ui.core.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.validate.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/ui/jquery.ui.widget.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/ui/jquery.ui.mouse.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/ui/jquery.ui.sortable.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/ui/jquery.ui.resizable.js"></script>
<!--<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery-latest.js"></script>-->
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery-tootltip.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.columnmanager.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.columnmanager.min.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.columnmanager.pack.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.clickmenu.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.clickmenu.pack.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.dataTables.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.numeric.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.decimalMask.1.1.1.min.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery-fieldselection.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.jclock.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/common/table2csv.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.caret.1.02.min.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.treeview.js" ></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/common/jquery-easy-confirm-dialog.js" ></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery-ui-timepicker-addon.js" ></script>
<style>
.ui-resizable-helper { border: 2px dotted #00F; }
</style>
<script>
var $j = jQuery;

$j(document).ready(function(){
	var flag =true;
	var flag1 = true;
	$j( ".column" ).sortable({
		connectWith: ".column"
	});
	$j( ".column" ).sortable({ distance: 200 });
	<s:iterator value="#request.widgetlist" >
	// $j('#'+'<s:property value="widgetParentDivId" />').offset({ top: 100, left: 300 });
	// $j('#'+'<s:property value="widgetParentDivId" />').css('width': 700px, 'height': 500px);
	
	 if('<s:property value="isMinimizable" />'=='true')
	 {
		 var parentDiv = '<s:property value="widgetDivId" />'+'parent';
		 var widgetDiv = '<s:property value="widgetDivId" />';
		$j('#'+'<s:property value="widgetDivId" />').find(".portlet-header .ui-icon").bind("click",function() {

			if('<s:property value="isClosable" />'=='true')
			 {
		    	if($j(this).is(".ui-icon-close"))
				{
						
				$j(this).parent().parent().parent().css('display','none');
					
				}
			 }
			if($j(this).is(".ui-icon-newwin"))
			{
				var maxId = $j(this).parent().parent().attr("id");
				//alert(maxId);
				maximizeScreen(maxId);		
			}		
			
			if($j(this).is(".ui-icon-minusthick") || $j(this).is(".ui-icon-plusthick"))
			{
				$j(this).toggleClass("ui-icon-minusthick").toggleClass("ui-icon-plusthick");
				
				$j(this).parents(".portlet:first").find(".portlet-content").toggle();
				
				var divid = $j(this).parent().parent().attr("id");
				if(flag)
				divHeight=document.getElementById(divid).style.height;
				if(divHeight=='100%')
				{
					divHeight = 'auto';
					}	
			    $j(this).parents(".portlet:first").css('height',divHeight);
				
			}
			
			flag=false;			
			});
	 }

	 if('<s:property value="isResizable" />'=='true')
	 {
		  var minHeight = '<s:property value="resizeMinHeight" />';
		  var minWidth = '<s:property value="resizeMinWidth" />';
		  var maxHeight = '<s:property value="resizeMaxHeight" />';
		  var maxWidth = '<s:property value="resizeMaxWidth" />';
		  $j('#'+parentDiv).resizable({helper: "ui-resizable-helper"});		  
		  $j('#'+parentDiv).resizable({minHeight: minHeight});
		  $j('#'+parentDiv).resizable({minWidth: minWidth});	
		  $j('#'+parentDiv).resizable({maxHeight: maxHeight});
		  $j('#'+parentDiv).resizable({maxWidth: maxWidth});			 
		 // $j('#'+parentDiv).resizable("option", "minWidth",minWidth );		  
	 }

	 if('<s:property value="isDragable" />'=='true')
	 {
	  //$j('#'+parentDiv).draggable();
	 }		
	 //$j('#'+parentDiv).droppable({
	 //     drop: function() { alert('dropped'); }
	//    });
	
	//for Setting the Height on the div
	$j('#'+'<s:property value="widgetDivId" />').css('height','100%');
	$j('#'+'<s:property value="widgetDivId" />').find('.portlet-content').each(function(){
          $j(this).css('height','100%');
		});
	</s:iterator>		
	//$j( ".column" ).disableSelection();	
});
</script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/common/common_custom_methods.js" ></script>