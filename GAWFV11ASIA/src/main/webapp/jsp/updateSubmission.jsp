<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML> 
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:include page="include.jsp" flush="true"/>	
<jsp:useBean id="submB" scope="request" class="com.velos.eres.web.submission.SubmissionJB"/>
<jsp:useBean id="statusB" scope="request" class="com.velos.eres.web.statusHistory.StatusHistoryJB"/>
<jsp:useBean id="submBoardB" scope="request" class="com.velos.eres.web.submission.SubmissionBoardJB"/>
<jsp:useBean id="defaultSubmBoardB" scope="request" class="com.velos.eres.web.submission.SubmissionBoardJB"/>
<jsp:useBean id="submStatusB" scope="request" class="com.velos.eres.web.submission.SubmissionStatusJB"/>

<jsp:useBean id="defSubmStatusB" scope="request" class="com.velos.eres.web.submission.SubmissionStatusJB"/>

<%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.business.common.*,java.util.*,java.text.*,com.velos.eres.service.util.*,com.velos.eres.web.account.AccountJB,com.velos.esch.business.common.*"%>
<%
  HttpSession tSession = request.getSession(true); 
  // tSession.removeAttribute("IRBParams"); // clear parameters that might have been passed to forms
  String accId = (String) tSession.getValue("accountId");
  int iaccId = EJBUtil.stringToNum(accId);
  String studyId = (String) tSession.getValue("studyId");
  String grpId = (String) tSession.getValue("defUserGroup");
  String ipAdd = (String) tSession.getValue("ipAdd");
  String usr = (String) tSession.getValue("userId");
  int iusr = EJBUtil.stringToNum(usr);
  CodeDao codedao = new CodeDao();
  int codeId=codedao.getCodeId("subm_status","approved");
  int irbcodeId=codedao.getCodeId("subm_status","gov-approved");  
  String codeID = codeId+"";
  String irbcodeID = irbcodeId+"";
  String src= StringUtil.htmlEncodeXss(request.getParameter("srcmenu"));
  String selectedTab = StringUtil.htmlEncodeXss(request.getParameter("selectedTab"));
  String submissionPK = StringUtil.htmlEncodeXss(request.getParameter("submissionPK"));
  String submissionBoardPK = StringUtil.htmlEncodeXss(request.getParameter("submissionBoardPK"));
  String provisoflag = request.getParameter("provisoflag")==null?"":request.getParameter("provisoflag");
  String eSign = request.getParameter("eSign");
  Calendar cal = Calendar.getInstance(TimeZone.getDefault());
  
  String DATE_FORMAT = "hh:mm:ss a";

	java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(DATE_FORMAT);       
        
  String time = sdf.format(cal.getTime());
  String defaultReviewBoard = request.getParameter("defaultBoardPK");
  String fKReviewBoardGov = request.getParameter("fKReviewBoardGov");

  boolean bSetFinalStatus  = false;
  int defSubmissionBoardPK=0;
  String updateuserchecked="";
  
  if (StringUtil.isEmpty(defaultReviewBoard))
  {
  		defaultReviewBoard = "";
  }	
  	  
  if (sessionmaint.isValidSession(tSession))
  {	
	
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%   
    String oldESign = (String) tSession.getValue("eSign");
	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {
	    // Figure out which board was selected
        EIRBDao eIrbDao = new EIRBDao();
        eIrbDao.getReviewBoards(EJBUtil.stringToNum(accId),EJBUtil.stringToNum(grpId));
        ArrayList boardNameList = eIrbDao.getBoardNameList();
        System.out.println("hem"+boardNameList.get(0));
        ArrayList boardIdList = eIrbDao.getBoardIdList();
        boolean hasNewSubmission = false;
        for (int iBoard=0; iBoard<boardIdList.size(); iBoard++) {
            if (!"on".equals(request.getParameter("submitTo"+iBoard))) { continue; }
            hasNewSubmission = true;
        }
        //EIRBDao codelstsubm_stat = new EIRBDao();
        eIrbDao.getEnteredFormsLatestValue(EJBUtil.stringToNum(submissionPK),EJBUtil.stringToNum(submissionBoardPK));
        String form_name="";
        String form_seq="";
        int fk_formlib=0;
        int fk_filledform=0;
        int fk_formlibver=0;
        int submBoardId = 0;
        form_name=eIrbDao.getIrbFormName();
        form_seq=eIrbDao.getIrbFormSeq();
        fk_formlibver=eIrbDao.getFormLibVer();
        fk_formlib=eIrbDao.getFkformlibsumstat();
        fk_filledform=eIrbDao.getFkfilledformlibsumstat();
      	int old_codelst_stat =   eIrbDao.getOldSubmStatus(EJBUtil.stringToNum(submissionPK),EJBUtil.stringToNum(submissionBoardPK));
        
        String resultMsg = null;
        
        // If submissionPK was passed in, use it for all subsequent updates
        int newSubmId = EJBUtil.stringToNum(submissionPK);
        
        // Create a new submission bean and its status if a new submission is made by the user
        if (hasNewSubmission && newSubmId < 1) {
            submB.setStudy(studyId);
            submB.setIpAdd(ipAdd);
            submB.setCreator(usr);
            submB.setLastModifiedBy(usr);
            newSubmId = submB.createSubmission();
            if( newSubmId > 0 ) {
                resultMsg = MC.M_Data_SvdSucc/*"Data was saved successfully"*****/;
            } else {
                resultMsg = MC.M_Err_SavingData/*"There was an error while saving data"*****/;
%>
<jsp:include page="errorback.jsp" flush="true"><jsp:param name="errorMsg" value="<%=resultMsg%>"/></jsp:include>
</BODY>
</HTML>
<%              return; // An error occurred; let's get out of here
            }

            // Create a new submission status for submission
            eIrbDao.getCurrentOverallStatus(EJBUtil.stringToNum(studyId), newSubmId);
            String currentOverallStatus = eIrbDao.getCurrentOverallStatus();
            // If the overall status does not already exist for this study, create a new one
            if (currentOverallStatus == null || currentOverallStatus.length() == 0) {
                submStatusB.setFkSubmission(String.valueOf(newSubmId));
                submStatusB.setFkSubmissionBoard(null); // For overall status, this is null
                submStatusB.setSubmissionEnteredBy(usr);
                submStatusB.setCreator(usr);
                submStatusB.setLastModifiedBy(usr);
                submStatusB.setIpAdd(ipAdd);
                submStatusB.setSubmissionStatusDate(null);
                submStatusB.setSubmissionAssignedTo(null);
                submStatusB.setSubmissionCompletedBy(null);
                submStatusB.setSubmissionNotes(null);
                submStatusB.setSubmissionStatusBySubtype("submitted");

                int newSubmStatusId = submStatusB.createSubmissionStatus();
                if(newSubmStatusId > 1 && provisoflag.equalsIgnoreCase("N")){
                	eIrbDao.updateSubmissionStatusProvisoFlag(EJBUtil.stringToNum(submissionPK),EJBUtil.stringToNum(submissionBoardPK));
                }
                if( newSubmStatusId < 1 ) {
                    resultMsg = MC.M_Err_SavingData/*"There was an error while saving data"*****/;
%>
<jsp:include page="errorback.jsp" flush="true"><jsp:param name="errorMsg" value="<%=resultMsg%>"/></jsp:include>
</BODY>
</HTML>
<%              return; // An error occurred; let's get out of here
                } // End of error
            } // End of create new submission status
        } // End of create new submission
        
        // If a new submission was not asked to be created, need to find an existing one 
        if (newSubmId < 1) {
            newSubmId = submB.getExistingSubmissionByFkStudy(studyId);
        }
        // If the existing was not found because this study has advanced to on-going stage,
        // create a new submission
        if (newSubmId < 1) {
            submB.setStudy(studyId);
            submB.setIpAdd(ipAdd);
            submB.setCreator(usr);
            submB.setLastModifiedBy(usr);
            newSubmId = submB.createSubmission();
        }
        
	    for (int iBoard=0; iBoard<boardIdList.size(); iBoard++) {
	        if (!"on".equals(request.getParameter("submitTo"+iBoard))) { continue; }

	        // Create a new submission_board bean for each board selected
		    submBoardB.setFkSubmission(String.valueOf(newSubmId));
		    submBoardB.setFkReviewBoard((String) boardIdList.get(iBoard));
		    submBoardB.setCreator(usr);
		    submBoardB.setLastModifiedBy(usr);
		    submBoardB.setIpAdd(ipAdd);
		    submBoardB.setFkReviewMeeting(null);
		    submBoardB.setSubmissionReviewer(null);
		    submBoardB.setSubmissionReviewType(null);
		    
		    Hashtable ht = submBoardB.createSubmissionBoard();
		    
		    int newSubmBoardId = ((Integer)ht.get("id")).intValue(); 
		    if (newSubmBoardId < 1) {
		        resultMsg = MC.M_Err_SavingData/*"There was an error while saving data"*****/;
		        break;
		    }
		    
		    // Create a new submission status bean for this submission_board
            submStatusB.setFkSubmission(String.valueOf(newSubmId));
            submStatusB.setFkSubmissionBoard(String.valueOf(newSubmBoardId));
            submStatusB.setSubmissionEnteredBy(usr);
            submStatusB.setCreator(usr);
            submStatusB.setLastModifiedBy(usr);
            submStatusB.setIpAdd(ipAdd);
            submStatusB.setFkfilledformlibsumstat(fk_filledform);
        	submStatusB.setFkformlibsumstat(fk_formlib);
        	submStatusB.setIrbFormName(form_name);
        	submStatusB.setIrbFormSeq(form_seq);
        	submStatusB.setFormLibVer(fk_formlibver);
            submStatusB.setSubmissionStatusDate(null);
            submStatusB.setSubmissionAssignedTo(null);
            submStatusB.setSubmissionCompletedBy(null);
            submStatusB.setSubmissionNotes(null);
		    if (ht.containsKey("previouslySubmitted")) {
		        submStatusB.setSubmissionStatusBySubtype("resubmitted");
		    } else {
		        submStatusB.setSubmissionStatusBySubtype("submitted");
		    }
            
            int newSubmStatusIdForBoard = submStatusB.createSubmissionStatus();
            if( newSubmStatusIdForBoard < 1 ) {
                resultMsg = MC.M_Err_SavingData/*"There was an error while saving data"*****/;
                break;
            }
            if (resultMsg == null) { resultMsg = MC.M_Data_SvdSucc/*"Data was saved successfully"*****/; }
	    } // End of board loop
        
	    // Change the current overall status
	    if ("on".equals(request.getParameter("overallcheck"))) {
	        String fkStatus = StringUtil.htmlEncodeXss(request.getParameter("submissionStatusOverall"));
	        String enteredBy = StringUtil.htmlEncodeXss(request.getParameter("overallEnteredById"));
	        if (enteredBy == null || enteredBy.length() == 0) { enteredBy = usr; }
	        String statusDate = StringUtil.htmlEncodeXss(request.getParameter("overallDate"));
	        if (statusDate == null || statusDate.length() == 0) {
	            statusDate = DateUtil.dateToString(Calendar.getInstance().getTime());
	        }
	        String statusNotes = request.getParameter("overallNotes");
	        if (statusNotes != null) {
	            statusNotes = statusNotes.replaceAll("\r\n","<br/>").replaceAll("\n","");
	        }
	        statusNotes = StringUtil.htmlEncodeXss(statusNotes);
	        
            submStatusB.setFkSubmission(String.valueOf(newSubmId));
            submStatusB.setFkSubmissionBoard(null); // For overall status, this is null
            submStatusB.setFkSubmissionStatus(fkStatus);
            submStatusB.setSubmissionEnteredBy(enteredBy);
            submStatusB.setSubmissionStatusDate(statusDate);
            submStatusB.setSubmissionNotes(statusNotes);
            submStatusB.setCreator(usr);
            submStatusB.setLastModifiedBy(usr);
            submStatusB.setIpAdd(ipAdd);
            submStatusB.setSubmissionAssignedTo(null);
            submStatusB.setSubmissionCompletedBy(null);
            int newSubmStatusId = submStatusB.createSubmissionStatus();
            
            if( newSubmStatusId < 1 ) {
                resultMsg = MC.M_Err_SavingData/*"There was an error while saving data"*****/;
%>
<jsp:include page="errorback.jsp" flush="true"><jsp:param name="errorMsg" value="<%=resultMsg%>"/></jsp:include>
</BODY>
</HTML>
<%              return; // An error occurred; let's get out of here
            } // End of error
            
            //insert the same status as default review board's status
            
            	       
            defSubmissionBoardPK = defaultSubmBoardB.findByFkSubmissionAndBoard(EJBUtil.stringToNum(submissionPK), EJBUtil.stringToNum(fKReviewBoardGov));
            
            if (defSubmissionBoardPK > 0)
            {
	            		
	            defSubmStatusB.setFkSubmission(submissionPK);
	            defSubmStatusB.setFkSubmissionBoard(String.valueOf(defSubmissionBoardPK));  
	            defSubmStatusB.setFkSubmissionStatus(fkStatus);
	            defSubmStatusB.setSubmissionEnteredBy(enteredBy);
	            defSubmStatusB.setSubmissionStatusDate(statusDate);
	            defSubmStatusB.setSubmissionNotes(statusNotes);
	            defSubmStatusB.setCreator(usr);
	            defSubmStatusB.setLastModifiedBy(usr);
	            defSubmStatusB.setFkfilledformlibsumstat(fk_filledform);
	            defSubmStatusB.setFkformlibsumstat(fk_formlib);
	            defSubmStatusB.setIrbFormName(form_name);
	            defSubmStatusB.setIrbFormSeq(form_seq);
	            defSubmStatusB.setFormLibVer(fk_formlibver);
	            defSubmStatusB.setIpAdd(ipAdd);
	            defSubmStatusB.setSubmissionAssignedTo(null);
	            defSubmStatusB.setSubmissionCompletedBy(null);
	            
	            bSetFinalStatus = true;
            }
            
            
            // Update also the status in the er_submission record
            submB.setStudy(studyId);
            submB.setSubmissionStatus(fkStatus);
            submB.setLastModifiedBy(usr);
            submB.setCreator(null);        // this will not be updated
            submB.setSubmissionFlag(null); // this will not be updated
            submB.setSubmissionType(null); // this will not be updated
            int updateResult = submB.updateSubmissionByFkStudy();
            if(fkStatus.equalsIgnoreCase(codeID) ||fkStatus.equalsIgnoreCase(irbcodeID) ){
            	String strDate = statusDate +" "+ time ;
            	String Updateuser = eIrbDao.getUpdateUser(EJBUtil.stringToNum(studyId));
            	eIrbDao.updateIscurrentForEcompStatus(EJBUtil.stringToNum(studyId));
            	
            	statusB.setStatusModuleId(studyId);
            	statusB.setStatusModuleTable("er_study");
            	statusB.setStatusCodelstId(fkStatus);
            	statusB.setStatusStartDate(statusDate);
            	statusB.setCreator(usr);
            	statusB.setIpAdd(ipAdd);
            	statusB.setModifiedBy(usr);
            	statusB.setRecordType("N");
            	statusB.setIsCurrentStat("1");
            	statusB.setIsUserChecked(Updateuser);
            	statusB.setTimeStamp(strDate);
            	statusB.setStatusHistoryDetailsWithEndDate();

            	
            	
            }
            if( updateResult < 1 ) {
                resultMsg = MC.M_Err_SavingData/*"There was an error while saving data"*****/;
%>
<jsp:include page="errorback.jsp" flush="true"><jsp:param name="errorMsg" value="<%=resultMsg%>"/></jsp:include>
</BODY>
</HTML>
<%              return; // An error occurred; let's get out of here
            } // End of error
            if (resultMsg == null) { resultMsg = MC.M_Data_SvdSucc/*"Data was saved successfully"*****/; }
	    } // End of change overall status
	    
	    // Update er_submission_board and status for each board
	    for (int iBoard=0; iBoard<boardIdList.size(); iBoard++) {
	    	String a = (String)request.getParameter("submissionStatus"+iBoard);
	    	System.out.println("a"+a);
	        if (request.getParameter("submissionStatus"+iBoard) == null ||
	                ((String)request.getParameter("submissionStatus"+iBoard)).trim().length() == 0) {
	            continue;
	        }
            String submissionReviewSB = StringUtil.htmlEncodeXss(request.getParameter("submissionReview"+iBoard));
            String meetingReviewersSB = StringUtil.htmlEncodeXss(request.getParameter("reviewerIds"+iBoard));
            String meetingDateSB = StringUtil.htmlEncodeXss(request.getParameter("meetingDate"+iBoard));
			submBoardB.setFkSubmission(String.valueOf(newSubmId));
			submBoardB.setFkReviewBoard((String)boardIdList.get(iBoard));
			submBoardB.setFkReviewMeeting(meetingDateSB);
			submBoardB.setSubmissionReviewer(meetingReviewersSB);
			submBoardB.setSubmissionReviewType(submissionReviewSB);
			submBoardB.setLastModifiedBy(usr);
			submBoardId = submBoardB.updateSubmissionBoard();
			
			if (submBoardId < 1) {
                resultMsg = MC.M_Err_SavingData/*"There was an error while saving data"*****/;
%>
<jsp:include page="errorback.jsp" flush="true"><jsp:param name="errorMsg" value="<%=resultMsg%>"/></jsp:include>
</BODY>
</HTML>
<%              return; // An error occurred; let's get out of here
            } // End of error
            if (resultMsg == null) { resultMsg = MC.M_Data_SvdSucc/*"Data was saved successfully"*****/; }
			
			// insert new status
	        String fkStatus = StringUtil.htmlEncodeXss(request.getParameter("submissionStatus"+iBoard));
	        String enteredBy = StringUtil.htmlEncodeXss(request.getParameter("irbEnteredById"+iBoard));
	        if (enteredBy == null || enteredBy.length() == 0) { enteredBy = usr; }
	        String statusDate = StringUtil.htmlEncodeXss(request.getParameter("actionDate"+iBoard));
	        if (statusDate == null || statusDate.length() == 0) {
	            statusDate = DateUtil.dateToString(Calendar.getInstance().getTime());
	        }
	        String assignedTo = StringUtil.htmlEncodeXss(request.getParameter("irbAssignedToId"+iBoard));
	        String statusNotes = request.getParameter("notes"+iBoard);
	        /*
	        if (statusNotes != null) {
	            statusNotes = statusNotes.replaceAll("\r\n","<br/>").replaceAll("\n","<br/>");
	        }
	        statusNotes = StringUtil.htmlEncodeXss(statusNotes);
	        */
	        
            submStatusB.setFkSubmission(String.valueOf(newSubmId));
            submStatusB.setFkSubmissionBoard(String.valueOf(submBoardId)); 
            submStatusB.setFkSubmissionStatus(fkStatus);
            submStatusB.setSubmissionEnteredBy(enteredBy);
            submStatusB.setSubmissionStatusDate(statusDate);
            submStatusB.setSubmissionNotes(statusNotes);
            submStatusB.setCreator(usr);
            submStatusB.setLastModifiedBy(usr);
            submStatusB.setIpAdd(ipAdd);
            submStatusB.setFkfilledformlibsumstat(fk_filledform);
        	submStatusB.setFkformlibsumstat(fk_formlib);
        	submStatusB.setIrbFormName(form_name);
        	submStatusB.setIrbFormSeq(form_seq);
        	submStatusB.setFormLibVer(fk_formlibver);
            submStatusB.setSubmissionAssignedTo(assignedTo);
            submStatusB.setSubmissionCompletedBy(null);
            int newSubmStatusId = submStatusB.createSubmissionStatus();
            if(newSubmStatusId>1){
  			   eIrbDao.updateIscurrentForFinalOutcomeTab(EJBUtil.stringToNum(submissionPK),EJBUtil.stringToNum(submissionBoardPK));
  	            }
            if(newSubmStatusId > 1 && provisoflag.equalsIgnoreCase("N")){
            if(old_codelst_stat != EJBUtil.stringToNum(fkStatus)){
            	
            	eIrbDao.updateSubmissionStatusProvisoFlag(EJBUtil.stringToNum(submissionPK),EJBUtil.stringToNum(submissionBoardPK));
            }}
            updateuserchecked=eIrbDao.getUserCheckedLatestValue(EJBUtil.stringToNum(studyId),usr,newSubmId,submBoardId);
            
			if (newSubmStatusId < 1) {
                resultMsg = MC.M_Err_SavingData/*"There was an error while saving data"*****/;
%>
<jsp:include page="errorback.jsp" flush="true"><jsp:param name="errorMsg" value="<%=resultMsg%>"/></jsp:include>
</BODY>
</HTML>
<%              return; // An error occurred; let's get out of here
            } // End of error
            if (resultMsg == null) { resultMsg = MC.M_Data_SvdSucc/*"Data was saved successfully"*****/; }
	    }
	    
	    if (bSetFinalStatus)
	    {
		   int newDefSubmStatusId = defSubmStatusB.createSubmissionStatus();
		   if(newDefSubmStatusId>1){
			   eIrbDao.updateIscurrentForFinalOutcomeTab(EJBUtil.stringToNum(submissionPK),EJBUtil.stringToNum(submissionBoardPK));
	            }
		   if(provisoflag.equalsIgnoreCase("N")&& newDefSubmStatusId > 1){
			   eIrbDao.updateSubmissionStatusProvisoFlag(EJBUtil.stringToNum(submissionPK),EJBUtil.stringToNum(submissionBoardPK));
		   }
		   updateuserchecked=eIrbDao.getUserCheckedLatestValue(EJBUtil.stringToNum(studyId),usr,newSubmId,submBoardId);
		   
		   if( newDefSubmStatusId  < 1 ) {
	                resultMsg = MC.M_Err_SavingData/*"There was an error while saving data"*****/;
		%>
		<jsp:include page="errorback.jsp" flush="true"><jsp:param name="errorMsg" value="<%=resultMsg%>"/></jsp:include>
		</BODY>
		</HTML>
		<%              return; // An error occurred; let's get out of here
		            } // End of error
		            
		}// bSetFinalStatus = true
		
		          
	    // No action was selected
	    if (resultMsg == null) {
	        resultMsg = MC.M_NoActTaken_SelOneAct/*"No action was taken. Please select at least one action."*****/;
	        %>
	        <jsp:include page="errorback.jsp" flush="true"><jsp:param name="errorMsg" value="<%=resultMsg%>"/></jsp:include>
	        </BODY></HTML>
	        <%  return;
	    }
	    
	    // All good now; show success message 
	    String nextJsp = "irbsubmissions.jsp";
	    String moreArgs = "";
	    if ("irb_act_tab".equals(selectedTab) || "irb_items_tab".equals(selectedTab) ||
	            "irb_saved_tab".equals(selectedTab)) {
	        nextJsp = "irbpenditems.jsp";
	    } else if ("irb_form_tab".equals(selectedTab)) {
	        nextJsp = "formfilledstudybrowser.jsp";
	        moreArgs = "&studyId="+studyId+"&submissionType="+StringUtil.htmlEncodeXss(request.getParameter("submissionType"));
	    }
	    
	    //By Aakriti Maggo	
            if (defSubmissionBoardPK > 0){
	    	submBoardId= defSubmissionBoardPK ;
	    }
	  //  String updateuserchecked=eIrbDao.getUserCheckedLatestValue(EJBUtil.stringToNum(studyId),usr,newSubmId,submBoardId);
	    
%>
  <br><br><br><br>
  <p class = "successfulmsg" align = center><%=resultMsg%></p>
    
  <script>
  try {
	    if (window.opener != undefined && window.opener != null && window.opener.reloadOpener != undefined) {
	    	window.opener.reloadOpener();
	  		setTimeout("self.close()",1000);
	    } else if (window.opener != undefined && window.opener != null && window.opener.reloadIrbForms != undefined) {
	    	window.opener.reloadIrbForms();
	  		setTimeout("self.close()",1000);
	    } else if (window.opener != null && window.opener.location != null && window.opener.location != undefined) {
	  		window.opener.location.reload();
	  		setTimeout("self.close()",1000);
	  	} else {
	  	  	<%
	  	  	StringBuffer nextPage = new StringBuffer();
	  	  	nextPage.append("window.location.href='irbactionwin.jsp?selectedTab=").append(selectedTab)
	  	  	.append("&submissionPK=").append(submissionPK)
	  	  	.append("&submissionBoardPK=").append(submissionBoardPK)
	  	  	.append("&studyId=").append(studyId).append("'");
	  	  	%>
	  		setTimeout("<%=nextPage.toString()%>",1000);
	  	}
	 
  } catch(e) {
		setTimeout("self.close()",1000);
  }
  </script>
<%
    }// end of if body for e-sign
  } //end of if body for valid session
  else
  {
  %>
  <jsp:include page="timeout.html" flush="true"/>
  <%
  }
%>

</BODY>
</HTML>