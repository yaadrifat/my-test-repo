<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.json.*"%>
<%@ page language = "java" import = "com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.esch.business.common.*,java.util.*"%>
<jsp:useBean id ="sessionmaint2" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@page import="com.velos.eres.web.user.*" %>
<%
	HttpSession tSession2 = request.getSession(true);
	request.setCharacterEncoding("UTF-8");
	response.setContentType("application/json");
	JSONObject jsObj = new JSONObject();
	
	if (!sessionmaint2.isValidSession(tSession2)) {
		// Session is invalid; print an error and exit
		response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");
		jsObj.put("result", -1);
		jsObj.put("resultMsg", MC.M_UsrNot_LoggedIn);/*jsObj.put("resultMsg", "User is not logged in.");*****/
		out.println(jsObj.toString());
		return;
	}

   	if (sessionmaint2.isValidSession(tSession2)){
		
   		
   		String userFname = (String) request.getParameter("userFname");
   		String accId = (String) tSession2.getValue("accountId");
		String mode = (String) request.getParameter("mode");
		String userId = (String) tSession2.getValue("userId");
		String userLname = (String) request.getParameter("userLname");		
		String email = (String) request.getParameter("email");
		String userLogin = (String) request.getParameter("userLogin");
		
		UserJB userJB = new UserJB();
		
		Integer countUserNames = userJB.getCount(userLname, userFname, accId, mode, userId);		
		
		if(userLogin!=null && userLogin.length()!=0){
			Integer userLogins = userJB.getCount(userLogin);
			jsObj.put("userLogins", userLogins);
			
		}
		
		if(email!=null && email.length()!=0){
			Integer countUserMails = userJB.getCount(email, accId, mode, userId);
			jsObj.put("countUserMails", countUserMails);
			
		}
		
		
		jsObj.put("countUserNames", countUserNames);
		

		out.println(jsObj.toString());

	} %>