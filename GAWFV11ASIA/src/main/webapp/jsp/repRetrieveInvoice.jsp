<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:useBean id="repB" scope="request" class="com.velos.eres.web.report.ReportJBNew"/>
<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%!
private String getHTTPPathFromURL(HttpServletRequest request){
	if (null == request.getRequestURL()) { return ""; }

	final String findMe = "/velos/";
	String requestURL = (request.getRequestURL()).toString();
	int indx = requestURL.indexOf(findMe);
	if (indx < 0) return "";

	return requestURL.substring(0, indx);
}
%>

<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,javax.xml.transform.TransformerFactory,javax.xml.transform.Transformer,javax.xml.transform.Source,javax.xml.transform.stream.StreamSource,javax.xml.transform.stream.StreamResult,javax.xml.transform.TransformerException,javax.xml.transform.TransformerConfigurationException,java.io.*,java.text.*,java.net.URL,com.velos.eres.web.user.UserJB,com.velos.eres.web.grpRights.*"%>
<div class = "myHomebottomPanel">
		<jsp:include page="bottompanel.jsp" flush="true"/>
 </div>
<%


	String repTitle="&nbsp;";

	String repName=request.getParameter("repName");
	repName = LC.L_Invoice/*"Invoice"*****/;
	String repArgsDisplay = "";
	String format ="";

	int repId =0;
	int pkInv =0;
	repId = EJBUtil.stringToNum(request.getParameter("repList")); 
	pkInv = EJBUtil.stringToNum(request.getParameter("invPk"));
	
	/*Custom label processing*/
	int startIndx =0;
	int stopIndx =0;
	String labelKeyword ="";
	String labelString ="";
	String messageKeyword ="";
	String messageString ="";
	String interimXSL = "";
	String messageKey ="";
	String tobeReplaced ="";
	String messageParaKeyword []=null;
	
	String showSelect = request.getParameter("showSelect");

	
	int repXslId = 0;

	String params="";	

	String hdr_file_name="";
	String ftr_file_name="";
	String filePath="";
	String hdrfile="";
	String ftrfile="";	
	String hdrFilePath="";
	String ftrFilePath="";
	String fileName="";
	String xml=null;
	String calledfrom = request.getParameter("calledfrom");
	int successflag=1;
	if(EJBUtil.isEmpty(calledfrom)){
		calledfrom = LC.L_Rpt_Central;
	}
	HttpSession tSession = request.getSession(true); 
	

   if (sessionmaint.isValidSession(tSession))
   {
	   try{
%>
<jsp:include page="sessionlogging.jsp" flush="true"/> 

<%
	String uName =(String) tSession.getValue("userName");	 
	int acc = EJBUtil.stringToNum((String) (tSession.getValue("accountId")));
	String accountId= (String) (tSession.getValue("accountId"));
	int userId = EJBUtil.stringToNum((String) (tSession.getValue("userId")));
	userB = (UserJB)tSession.getValue("currentUser");
	String FilledFormId = "";
	
	int pageRight = 0; 
	GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");		
    pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("REPORTS"));
    
    	
    String repDate="";
    params = pkInv +"";
    repXslId =  repId;
    
    Calendar now = Calendar.getInstance();
	 	repDate = DateUtil.getCurrentDate() ;

	ReportDaoNew rD =new ReportDaoNew();
	
	//The parameters are separated by :
	//They are segregated in the stored procedure SP_GENXML and then passed to report sql
//System.out.println("**************" + params);	 

	rD=repB.getRepXml(repId,acc,params);
	
	ArrayList repXml = rD.getRepXml();
	if (repXml == null || repXml.size() == 0) { //no data found
%>
		<br><br><br><br><br><br><br><br><br>
		<P class="sectionHeadings">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%Object[] arguments= {repName}; %>
	    <%=VelosResourceBundle.getMessageString("M_NoDataFnd_Rpt",arguments)%><%--No data found for the Report'<%=repName%>'*****--%> .</P>
<%		throw new Exception(VelosResourceBundle.getMessageString("M_NoDataFnd_Rpt",arguments));
	}
System.out.println("params:: "+params);	
System.out.println("===================================================");	 
//System.out.println(repXml);	 
System.out.println("===================================================");	 

	rD=repB.getRepXsl(repXslId);
	ArrayList repXsl = rD.getXsls();
		
	Object temp;
	
	String xsl=null;
	

	temp=repXml.get(0);				
	
	if (!(temp == null)) 
	{
		xml=temp.toString();
		//replace the encoded characters
		xml = StringUtil.replace(xml,"&amp;#","&#");
		
	}
	else
	{
		out.println(MC.M_ErrIn_GetXml/*"Error in getting XML"*****/);
		Rlog.fatal("Report","ERROR in getting XML");			
		throw new Exception(MC.M_ErrIn_GetXml);
	}
	
    try{	
		temp=repXsl.get(0);
	} catch (Exception e) {
		out.println(MC.M_ErrIn_GetXsl/*"Error in getting XSL."*****/);
		Rlog.fatal("Report","ERROR in getting XSL. Record for report " +repId +"does not exist in database.");
		throw new Exception(MC.M_ErrIn_GetXsl);
	}	
	
	if (!(temp == null))	{
		xsl=temp.toString();
		xsl = StringUtil.replace(xsl,"[VELFLOATFORMATDEF]",NumberUtil.getXSLFloatFormatDef());
		xsl = StringUtil.replace(xsl,"[VELNUMFORMATDEF]",NumberUtil.getXSLNumFormatDef());
	}
	else
	{
		out.println(MC.M_ErrIn_GetXsl/*"Error in getting XSL."*****/);
		Rlog.fatal("Report","ERROR in getting XSL. Record for report " +repId +"does not exist in database.");
		throw new Exception(MC.M_ErrIn_GetXsl);
	}
	
	if ((xsl.length())==0) {
		out.println(MC.M_ErrIn_GetXsl/*"Error in getting XSL."*****/);
		Rlog.fatal("Report","ERROR in getting XSL. Empty XSL found in database for report " +repId);
		throw new Exception(MC.M_ErrIn_GetXsl);
	} 
	
	
	



	if ((xml.length())==34) { //no data found
%>
	
		<br><br><br><br><br><br><br><br><br>
		<P class="sectionHeadings">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=MC.M_NoData_ForRpt%><%--No data found for the Report*****--%> '<%=repName%>'.</P>				
<%		throw new Exception(MC.M_NoData_ForRpt);
	}
	
	
	//get hdr and ftr
	
	rD=repB.getRepHdFtr(repId,acc,userId);
	
	byte[] hdrByteArray=rD.getHdrFile();
	byte[] ftrByteArray=rD.getFtrFile();
	String hdrflag, ftrflag;	
	hdr_file_name="temph["+ now.get(now.DAY_OF_MONTH) + now.get(Calendar.MONTH) + (now.get(now.YEAR) - 1900)+ now.get(now.HOUR_OF_DAY)+ now.get(now.MINUTE)+ now.get(now.SECOND) + "].jpg";
	Configuration.readReportParam(Configuration.ERES_HOME + "eresearch.xml");
	filePath = Configuration.REPTEMPFILEPATH;
	
	hdrfile=filePath+ "/" + hdr_file_name;
	ftr_file_name="tempf["+ now.get(now.DAY_OF_MONTH) + now.get(Calendar.MONTH) + (now.get(now.YEAR) - 1900)+ now.get(now.HOUR_OF_DAY)+ now.get(now.MINUTE)+ now.get(now.SECOND) + "].jpg";
	ftrfile=filePath+ "/" + ftr_file_name;

	hdrFilePath="../temp/"+hdr_file_name;
	ftrFilePath="../temp/"+ftr_file_name;
//out.println(params);		
	//check for byte array
	if (!(hdrByteArray ==null)) 
	{
		hdrflag="1";
		ByteArrayInputStream fin=new ByteArrayInputStream(hdrByteArray);
					
		BufferedInputStream fbin=new BufferedInputStream(fin);
		
		File fo=new File(hdrfile);

		FileOutputStream fout = new FileOutputStream(fo);
		Rlog.debug("3","after output stream");		
		int c ;
		while ((c = fbin.read()) != -1){
				fout.write(c);
			}
		fbin.close();
		fout.close();
		}	
	else
	{
		hdrflag="0";	
	}
		
		
		//check for length of byte array
	if (!(ftrByteArray ==null))
		{	
		ftrflag="1";
		ByteArrayInputStream fin1=new ByteArrayInputStream(ftrByteArray);
		BufferedInputStream fbin1=new BufferedInputStream(fin1);
		File fo1=new File(ftrfile);
		FileOutputStream fout1 = new FileOutputStream(fo1);
		int c1 ;
		while ((c1 = fbin1.read()) != -1){

			fout1.write(c1);
		}
		
		fbin1.close();
		fout1.close();
	}
	else
	{
		ftrflag="0";	
	}

		
	//get the folder name
	Configuration.readAppendixParam(Configuration.ERES_HOME + "eresearch.xml");
	com.aithent.file.uploadDownload.Configuration.readSettings("eres");
	com.aithent.file.uploadDownload.Configuration.readUploadDownloadParam(com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD + "fileUploadDownload.xml", "study");		  
	String fileDnPath = com.aithent.file.uploadDownload.Configuration.DOWNLOADSERVLET ;
	
	Configuration.readReportParam(Configuration.ERES_HOME + "eresearch.xml");
	filePath = Configuration.REPDWNLDPATH;
	Rlog.debug("report", filePath);
	//make the file name
	fileName="reporthtml"+"["+ now.get(now.DAY_OF_MONTH) + now.get(Calendar.MONTH) + (now.get(now.YEAR) - 1900)+ now.get(now.HOUR_OF_DAY)+ now.get(now.MINUTE)+ now.get(now.SECOND) + "].html" ;
	//make the complete file name
	String htmlFile = filePath + "/"+fileName;
	response.setContentType("text/html");    

	interimXSL = xsl;
	
	try{
		while(interimXSL.contains("VELLABEL[")){
			startIndx = interimXSL.indexOf("VELLABEL[") + "VELLABEL[".length();
			stopIndx = interimXSL.indexOf("]",startIndx);
			
			labelKeyword = interimXSL.substring(startIndx, stopIndx);
			labelString = LC.getLabelByKey(labelKeyword);
			
			interimXSL=StringUtil.replaceAll(interimXSL, "VELLABEL["+labelKeyword+"]", labelString);
		}

		while(interimXSL.contains("VELMESSGE[")){
			startIndx = interimXSL.indexOf("VELMESSGE[") + "VELMESSGE[".length();
			stopIndx = interimXSL.indexOf("]",startIndx);
			messageKeyword = interimXSL.substring(startIndx, stopIndx);
			messageString = MC.getMessageByKey(messageKeyword);
			interimXSL=StringUtil.replaceAll(interimXSL, "VELMESSGE["+messageKeyword+"]", messageString);
		}
		while(interimXSL.contains("VELPARAMESSGE[")){
			startIndx = interimXSL.indexOf("VELPARAMESSGE[") + "VELPARAMESSGE[".length();
			stopIndx = interimXSL.indexOf("]",startIndx);
			messageKeyword = interimXSL.substring(startIndx, stopIndx);
			messageKey=messageKeyword.substring(0, messageKeyword.indexOf("||"));
			messageKey=messageKey.trim();
			messageParaKeyword=messageKeyword.substring(messageKeyword.indexOf("||")+2).split(java.util.regex.Pattern.quote("||"));
			messageString = VelosResourceBundle.getMessageString(messageKey,messageParaKeyword);
			interimXSL=StringUtil.replaceAll(interimXSL, "VELPARAMESSGE["+messageKeyword+"]", messageString);
		}
		if(!interimXSL.contains("word.GIF")){
			if(interimXSL.contains(LC.L_Word_Format)){
				tobeReplaced = "<img border=\"0\" title=\""+LC.L_Word_Format+"\" alt=\""+LC.L_Word_Format+"\" src=\"./images/word.GIF\"></img>";
				interimXSL=StringUtil.replace(interimXSL, LC.L_Word_Format, tobeReplaced);
			}
			if(interimXSL.contains(LC.L_Excel_Format)){
				tobeReplaced = "<img border=\"0\" title=\""+LC.L_Excel_Format+"\" alt=\""+LC.L_Excel_Format+"\" src=\"./images/excel.GIF\"></img>";
				interimXSL=StringUtil.replace(interimXSL,LC.L_Excel_Format, tobeReplaced);
			}
			if(interimXSL.contains(LC.L_Printer_FriendlyFormat)){
				tobeReplaced = "<img border=\"0\" src=\"./images/printer.gif\" title=\""+LC.L_Printer_FriendlyFormat+"\" alt=\""+LC.L_Printer_FriendlyFormat+"\"></img>";
				interimXSL=StringUtil.replace(interimXSL, LC.L_Printer_FriendlyFormat, tobeReplaced);
			}
		}
		xsl = interimXSL;
	}catch (Exception e)
	{
  		out.write(MC.M_Err_ReplacingLabel/*"Error in replacing label string"*****/+": " + e.getMessage());
 	}
	
	 try
    {	
	
		//first save the output in html file
		//Read the Http path and read the images
		String url = getHTTPPathFromURL(request); 
		String xslForHTMLFile=xsl.replace("../images", url+"/velos/images");

		TransformerFactory tFactory1 = TransformerFactory.newInstance();
		Reader mR1=new StringReader(xml); 
		Reader sR1=new StringReader(xslForHTMLFile); 
		Source xmlSource1=new StreamSource(mR1);
		Source xslSource1 = new StreamSource(sR1);

 		Transformer transformer1 = tFactory1.newTransformer(xslSource1);
		//Set the params
		transformer1.setParameter("hdrFileName", hdrFilePath);
		transformer1.setParameter("ftrFileName", ftrFilePath);
		transformer1.setParameter("repTitle",repTitle);
		transformer1.setParameter("repName",repName);
		transformer1.setParameter("repBy",uName);
		transformer1.setParameter("repDate",repDate);
		transformer1.setParameter("argsStr",repArgsDisplay);
		transformer1.setParameter("cond","F");
		transformer1.setParameter("wd","");
		transformer1.setParameter("xd","");
		transformer1.setParameter("hd", "");
		transformer1.setParameter("hdrflag", hdrflag);
		transformer1.setParameter("ftrflag", ftrflag);
		transformer1.setParameter("dl", "");
		transformer1.setParameter("numFormat",NumberUtil.getXSLNumFormat());
		transformer1.setParameter("floatFormat",NumberUtil.getXSLFloatFormat());
		transformer1.setOutputProperty("encoding", "UTF-8");		
	
		// Perform the transformation, sending the output to html file
/*SV, 8/26, transformer didn't seem to close the stream properly that, next jsp (repGetExcel.jsp) kept getting 0(zero) bytes for file length,
	even though the file existed and was openable outside the application. So, fix is to, open and close file outside of transformer.
		
	  	transformer1.transform(xmlSource1, new StreamResult(htmlFile));
*/
		FileOutputStream fhtml=new FileOutputStream(htmlFile);
	  	transformer1.transform(xmlSource1, new StreamResult(fhtml));
	  	fhtml.close();
		//now send it to console			
	  	TransformerFactory tFactory = TransformerFactory.newInstance();
		Reader mR=new StringReader(xml); 
		Reader sR=new StringReader(xsl); 
		Source xmlSource=new StreamSource(mR);
		Source xslSource = new StreamSource(sR);
		// Generate the transformer.
		Transformer transformer = tFactory.newTransformer(xslSource);
		/*Changes for: INF-22402 Date:10/04/2012 By:Yogendra Pratap Singh*/
		String wordLink = "repGetWord.jsp?htmlFile=" + htmlFile +"&fileDnPath="+fileDnPath+"&filePath="+filePath+"&repId="+repId+"&calledfrom="+calledfrom;
		String excelLink = "repGetExcel.jsp?htmlFile=" + htmlFile +"&fileDnPath="+fileDnPath+"&filePath="+filePath+"&repId="+repId+"&calledfrom="+calledfrom;
		String printLink = "repGetHtml.jsp?htmlFileName=" + fileName +"&fileDnPath="+fileDnPath+"&filePath="+filePath+"&repId="+repId+"&calledfrom="+calledfrom;
		
		if (pageRight < 6)
		{
			String strRightMsg = "javascript:alert(&quot;"+MC.M_NoRgtTo_DldRpt/*You do not have access rights to download the report*****/+"&quot;);";
			wordLink = strRightMsg;
			excelLink = strRightMsg;
			printLink = strRightMsg;
		}		
		
		//Set the param for header and footer
		transformer.setParameter("hdrFileName", hdrFilePath);
		transformer.setParameter("ftrFileName", ftrFilePath);
		transformer.setParameter("repTitle",repTitle);
		transformer.setParameter("repName",repName);
		transformer.setParameter("repBy",uName);
		transformer.setParameter("repDate",repDate);
		transformer.setParameter("argsStr",repArgsDisplay);
		transformer.setParameter("cond","T");
		transformer.setParameter("wd",wordLink);
		transformer.setParameter("xd",excelLink);
		transformer.setParameter("hd",printLink);
		transformer.setParameter("hdrflag", hdrflag);
		transformer.setParameter("ftrflag", ftrflag);
		transformer.setParameter("dl", fileDnPath);
		transformer.setParameter("studyApndxParam", "?tableName=ER_STUDYAPNDX&columnName=STUDYAPNDX_FILEOBJ&pkColumnName=PK_STUDYAPNDX&module=study&db=eres&pkValue=");
		transformer.setOutputProperty("encoding", "UTF-8");
		transformer.setParameter("numFormat",NumberUtil.getXSLNumFormat());
		transformer.setParameter("floatFormat",NumberUtil.getXSLFloatFormat());
		//only for report 107, mock schedule
		
		if (repId == 107)
		{
		 	transformer.setParameter("showSelect", showSelect);
		 	transformer.setParameter("cond","F");
		 
		}
						  	  
		// Perform the transformation, sending the output to the response.
      	transformer.transform(xmlSource, new StreamResult(out));
      	
			  
    }
	catch (Exception e)
	{
  		out.write(e.getMessage());
 	}
    }catch(Exception exp){
    	successflag=0;
    	Rlog.error("invoice report",exp.getMessage());
    	return;
    }finally{
		/*Changes for: INF-22402 Date:10/04/2012 By:Yogendra Pratap Singh*/
		int downloadFlag=0;
		String downloadFormat = null;
		if(fileName.indexOf("xls")!=-1){
			downloadFlag = 1;
			downloadFormat = LC.L_Excel_Format;
		}else if(fileName.indexOf("doc")!=-1){
			downloadFlag = 1;
			downloadFormat = LC.L_Word_Format;
		}else if(fileName.indexOf("pdf")!=-1){
			downloadFlag = 1;
			downloadFormat = LC.L_Pdf;
		}
%>
	<jsp:include page="userReportLogging.jsp" flush="true">
		<jsp:param value="<%=repId %>" 			name="repId" />
		<jsp:param value="<%=fileName%>" 		name="fileName" />
		<jsp:param value="<%=filePath %>" 		name="filePath" />
		<jsp:param value="<%=calledfrom%>" 		name="moduleName" />
		<jsp:param value="<%=xml%>" 			name="repXml" />
		<jsp:param value="<%=downloadFlag%>" 	name="downloadFlag" />
		<jsp:param value="<%=downloadFormat%>" 	name="downloadFormat" />
		<jsp:param value="reportInvoice"		name="reportInvoice"/>
		<jsp:param value="<%=successflag%>"		name="successflag"/>
	</jsp:include>
<%
	   }
} //end of if session times out

else

{

%>

<jsp:include page="timeout_childwindow.jsp" flush="true" />


<%

} 

%>
