<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_Irbstud_Srch%><%--Manage Studies >> Search*****--%></title>
<body onload="refreshPaginator(); onLoad();" style="overflow:hidden;">
<jsp:include page="panel.jsp" flush="true"/>
<jsp:include page="ui-include.jsp" flush="true"/> 
<jsp:useBean id="tokenJB" scope="request" class="com.velos.eres.web.userSession.UserSessionTokenJB"/>

<%HttpSession tSession = request.getSession(true); %>
<script>
var paginate_study = null;
 
//km
function confirmBox(name){
	var paramArray = [name];
    if (confirm(getLocalizedMessageString("M_Want_DelStd",paramArray)))/*if (confirm("Do you want to delete <%=LC.Std_Study%> "+ name+"?" ))*****/
	return true;
    else
	return false;   
    
}//km

function opensvwin(id) {
       windowName = window.open("studyview.jsp?studyVPk="+ id,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=500,height=400")
     windowName.focus();
;}
 
function checkAll(study,loginUser,studyNumber){
	var result = confirm("Do you want to hide submission for study "+studyNumber+" within the Submission Alerts page ?");
if (result){
	jQuery.ajax({
		url:'updateEcompStat',
		type: 'GET',
		dataType:'json',
		global: true,
		async: false,
		data: {"study": study,"loginUser":loginUser
			},
			
			
		success: function(resp) 
		{		
			
					
	},
	error:function(resp){
			
	}
			});
	
}
paginate_study.render();
}
function unCheckAll(study,loginUser,studyNumber){
	var result = confirm("Do you want to unhide submission for study "+studyNumber+" within the Submission Alerts page ?");
if (result){
	jQuery.ajax({
		url:'afterDelUpdateEcompStat',
		type: 'GET',
		dataType:'json',
		global: true,
		async: false,
		data: {"study": study,"loginUser":loginUser
			},
			
			
		success: function(resp) 
		{		
		
					
	},
	error:function(resp){
			
	}
			});
	
}

paginate_study.render();
}
function checkSearch(formobj)
{ 
    /*   if( !(formobj.Tarea.checked) &&!(formobj.team.checked)&& !(formobj.kywrd.checked) && !(formobj.sec.checked) && !(formobj.dmg.checked) && !(formobj.appndx.checked) && !(formobj.stdtype.checked) && !(formobj.stdnumb.checked)
    && !(formobj.stdphase.checked) && !(formobj.stdstatus.checked) && !(formobj.sponsor.checked) && !(formobj.morestddetails.checked) && !(formobj.excldprmtclsr.checked) && (formobj.searchCriteria.value!=""))

    {
    
    alert("Please select at least one Search option");

    
    return false;
    
    }
 	if( ((formobj.Tarea.checked) || (formobj.team.checked) || (formobj.kywrd.checked) || (formobj.sec.checked) || (formobj.dmg.checked) || (formobj.appndx.checked) || (formobj.stdtype.checked) || (formobj.stdnumb.checked)
    || (formobj.stdphase.checked) || (formobj.stdstatus.checked) || (formobj.sponsor.checked) || (formobj.morestddetails.checked)) && (formobj.searchCriteria.value==""))
  
    {
    
    alert("Please enter Search text");
    formobj.searchCriteria.focus();
    return false;
    }
    
    */

 paginate_study.runFilter();
}


studyNumber=function(elCell, oRecord, oColumn, oData)
{
var ecompstatus_subtype=oRecord.getData("STATUS_SUBTYP");
//alert("status for ecomp= "+status1);
var htmlStr="";
var study=oRecord.getData("PK_STUDY");
var studyNumber=oRecord.getData("STUDY_NUMBER");
if (!study) study="";
if (!studyNumber) studyNumber="";
//htmlStr="<A onClick='return checkEdit();' HREF=\"irbnewcheck.jsp?srcmenu=tdMenuBarItem3&selectedTab=irb_check_tab&mode=M&submissionType=new_app&studyId="+study+"\">"
//    +studyNumber+"</A> ";


if(ecompstatus_subtype=='hrec_strt')
{	
	htmlStr="<A href='#' onClick='return checkEditAndLoad(\"irbnewinit.jsp?irbFlag=Y&srcmenu=tdMenuBarItem3&selectedTab=irb_init_tab&mode=M&studyId="+study+"\")'>"
    +studyNumber+"</A> "; 
    }
else{
	htmlStr="<A href='#' onClick='return checkEditAndLoad(\"irbnewinit.jsp?irbFlag=Y&srcmenu=tdMenuBarItem3&selectedTab=irb_init_tab&mode=S&tabFlag=Y&studyId="+study+"\")'>"
    +studyNumber+"</A> "; 
}    
	elCell.innerHTML=htmlStr;
}

   statusLink  =function(elCell, oRecord, oColumn, oData){
			
			var record=oRecord;
            var htmlStr="";
            var cl="";
          
			
			var sub=record.getData("STATUS_SUBTYPE"); 
            sub=((sub==null) || (!sub) )?"":sub;
            if (sub=="temp_cls") cl="Red"; 
            if (sub=="active") cl="Green";
            
			var nt=record.getData("STUDYSTAT_NOTE");
            nt=((nt==null) || (!nt) )?"":nt;
            nt=htmlEncode(nt);
            nt = escapeSingleQuoteEtc(nt);
            var tt="<tr><td><font size=2><%=LC.L_Note%><%--Note*****--%>:</font><font size=1>"+nt+"</font></td></tr><tr><td><font size=2><%=LC.L_Organizations%><%--Organizations*****--%>:</font><font size=1> " +record.getData("SITE_NAME")
            +"</font></td></tr>";
			/*Added by sudhir for s-22307 enhancement on 12-03-2012*/		
			if(record.getData("STATUS")==".."){
				htmlStr="<div align=\"center\" ><A href=\"studystatusbrowser.jsp?srcmenu=tdmenubaritem3&selectedTab=3&studyId="+record.getData("PK_STUDY")+ "\" onmouseover=\"return overlib('<%=MC.M_Status_Cannot_Be_Displayed%>',CAPTION,'<%=LC.L_Study_Status%>')\"; onmouseout=\"return nd();\"><img src=\"../images/help.png\" border=\"0\"/></A></div>";
			}else{	/*Added by sudhir for s-22307 enhancement on 12-03-2012*/		
				htmlStr="<A href=\"studystatusbrowser.jsp?srcmenu=tdmenubaritem3&selectedTab=3&studyId="+record.getData("PK_STUDY")+ "\" onmouseover=\"return overlib('"+tt+"',CAPTION,'"+record.getData("STUDYSTAT_DATE_DATESORT")+"')\"; onmouseout=\"return nd();\"><FONT class=\""+cl+"\">"+record.getData("STATUS")+"</FONT> </A>";
			}
			elCell.innerHTML=htmlStr;
			
          }

	//Added by Manimaran to implement view title mouse over link	
	titleLink = 
		function(elCell, oRecord, oColumn, oData)
		  {
			var record=oRecord;
            var htmlStr="";

			var title = record.getData("STUDY_TITLE"); 
			title = ((title==null) || (!title) )? "-":title;
			title = htmlEncode(title);
			title = escapeSingleQuoteEtc(title);
			
			//Modified for S-22306 Enhancement : Raviesh
					
			htmlStr=title;
			elCell.innerHTML = htmlStr;
		
          }
    
    //Added for S-22306 Enhancement : Raviesh
	studyNumLink = 
		function(elCell, oRecord, oColumn, oData)
		  {
			var record=oRecord;
            var htmlStr="";

			var studyNum = record.getData("STUDY_NUMBER"); 
			studyNum = ((studyNum==null) || (!studyNum) )? "-":studyNum;
			studyNum = htmlEncode(studyNum);
			studyNum = escapeSingleQuoteEtc(studyNum);

			if(studyNum.length>50)
			{
				var studyNumExceedingLimit=studyNum.substring(0,50);
				htmlStr=studyNumExceedingLimit+"&nbsp;<img src=\"./images/More.png\" class='asIsImage' border=\"0\" onmouseover=\"return overlib(\'"+studyNum+"\',CAPTION,'<%=LC.L_Study_Number%><%--<%=LC.L_Study_Number%> Study Number*****--%>' ,LEFT , ABOVE);\" onmouseout=\"return nd();\"\>";
			}else{			
			htmlStr="&nbsp;&nbsp;"+studyNum;
			}
			elCell.innerHTML = htmlStr;		
          }

        
 $E.addListener(window, "load", function() { 

	 $('CSRFToken').value="<%=StringUtil.trueValue(tokenJB.createTokenForUser((String) tSession.getAttribute("userId")==null?0:StringUtil.stringToNum((String) tSession.getAttribute("userId"))))%>";
  paginate_study=new VELOS.Paginator('ecompstudies',{
 				sortDir:"asc", 
				sortKey:"STUDY_NUMBER",
				defaultParam:"userId,accountId,grpId,superuserRights,pmtclsId,tabsubtype,CSRFToken",
				filterParam:"dmg,searchCriteria",
				dataTable:'serverpagination_study',
				rowSelection:[5,10,15],
				navigation: true /*,
				searchEnable:false*/
								
				});
				paginate_study.runFilter();
				paginate_study.render();
				
	
	/*var paginate_pat=new VELOS.Paginator('allPatient',{
 				sortDir:"asc", 
				sortKey:"PERSON_CODE",
				defaultParam:"userId,accountId,grpId",
				filterParam:"patCode,patage,siteId,patName,gender,regby,pstat,studyId,speciality",
				dataTable:'serverpagination_pat',
				navigation:false
				});
				paginate_pat.render();*/
			
			 } 
				 ) 
				 
				 
    function checkEditAndLoad(nextLocation)
    {
		window.location.href = nextLocation;
    }
				 
 </script>
<style>
.textboxirb{border:1px solid black;
border-radius:5px;}
</style>
<%
String src="tdMenuBarItem3";
%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/> 
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB"/><!--km-->
<jsp:useBean id="groupB" scope="page" class="com.velos.eres.web.group.GroupJB"/><!--km--> 
<%@ page language = "java" import = "com.velos.eres.web.user.UserJB" %>
<%@ page language = "java" import = "com.velos.eres.web.grpRights.GrpRightsJB,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration,com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.*,java.sql.*,com.velos.eres.business.common.*"%>  
<%
String refreshBool = "false";
String myUserId = null;
String accountId1 = null;
int grpId1 = 0;
HttpSession mySession = request.getSession(false);
if (sessionmaint.isValidSession(mySession)) {
	if (request.getParameter("searchCriteria") != null) {
		refreshBool = "true";
	}
}
%>
<script>
var refreshPaginator = function() {
	$('CSRFToken').value="<%=StringUtil.trueValue(tokenJB.createTokenForUser((String) tSession.getAttribute("userId")==null?0:StringUtil.stringToNum((String) tSession.getAttribute("userId"))))%>";
	if (<%=refreshBool%>) {
		try {
			paginate_study = new VELOS.Paginator('ecompstudies',{
	 				sortDir:"asc", 
					sortKey:"STUDY_NUMBER",
					defaultParam:"userId,accountId,grpId,superuserRights,pmtclsId,tabsubtype,CSRFToken",
					filterParam:"dmg,searchCriteria",
                    dataTable:'serverpagination_study',
					rowSelection:[5,10,15],
					navigation: true,
					noIndexGeneration: true
					});

			paginate_study.runFilter();
		} catch(e) {}
	}
}
</script>
<body  class="yui-skin-sam">
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>


 
<!-- Page drastically modified by Sonia Abrol, 02/02/06. If anybody needs to refer the old code, pls. extract from CVS history
removed the redundant code, unions, unnecessary joins
-->

<div class="browserDefault" id="div1" style="height:90%; position:absolute;">
<%

String pagenum = "";
int curPage = 0;
long startPage = 1;
long cntr = 0;
String pagenumView = "";
int curPageView = 0;
long startPageView = 1;
long cntrView = 0;

pagenum = request.getParameter("page");
if (pagenum == null)
{
    pagenum = "1";
}
curPage = EJBUtil.stringToNum(pagenum);

String orderBy = "";
orderBy = request.getParameter("orderBy");



String orderType = "";
orderType = request.getParameter("orderType");

if (orderType == null)
{
    orderType = "asc";
}


pagenumView = request.getParameter("pageView");
if (pagenumView == null)
{
    pagenumView = "1";
}
curPageView = EJBUtil.stringToNum(pagenumView);

String orderByView = "";
orderByView = request.getParameter("orderByView");
String userId ="" ;
String orderTypeView = "";
orderTypeView = request.getParameter("orderTypeView");

if (orderTypeView == null)
{
    orderTypeView = "asc";
}
//HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession)) {
    userId = (String) tSession.getValue("userId");
    String defUserGroup = (String) tSession.getAttribute("defUserGroup");
    
    
    String accountId = (String) tSession.getValue("accountId");
    
    //Added by Manimaran to give access right for default admin group to the delete link 
	int usrId = EJBUtil.stringToNum(userId);
	userB.setUserId(usrId);
	userB.getUserDetails();
	String defGroup = userB.getUserGrpDefault();
	int grpId=EJBUtil.stringToNum(defGroup);
	groupB.setGroupId(EJBUtil.stringToNum(defGroup));
	groupB.getGroupDetails();
	String groupName = groupB.getGroupName();
	//km
	%>
<script>
checkbox_hide=function(elCell, oRecord, oColumn, oData)
{	
	var htmlStr="";
	var loginUser = <%=usrId%>
	loginUser = loginUser+'';
	var study=oRecord.getData("PK_STUDY");
	var studyNumber=oRecord.getData("STUDY_NUMBER");
	//alert("studyNumber"+studyNumber);
	var checkeduser = oRecord.getData("USER_CHECKED");
	if(checkeduser!=null){
		var chkUser = checkeduser.split(",");
	  var chkuser = chkUser.indexOf(loginUser);
	   if (chkuser>=0){
		   htmlStr = "<input type ='checkbox' name='checkallrow'  checked ='checked' onClick = 'unCheckAll("+study+","+loginUser+","+"\""+studyNumber+"\")'/>";
		   }
	   else{
		   htmlStr = "<input type ='checkbox' name='checkallrow' onClick = 'checkAll("+study+","+loginUser+","+"\""+studyNumber+"\")'/>";
		   }
	   
	}

	else{
	htmlStr = "<input type ='checkbox' name='checkallrow' onClick = 'checkAll("+study+","+loginUser+","+"\""+studyNumber+"\")'/>";
	}
elCell.innerHTML=htmlStr;
}
</script>

<%
String studySql="" ;
String countSql="" ;
String combineSql="" ;
String searchFilter="" ;
String searchFiltercount="" ;
String studyActualDt = null;  
String studyTeamRght = null;  
String tabsubtype  = "";  
      
       acmod.getControlValues("study_rights","STUDYMPAT");  
       ArrayList aRightSeq = acmod.getCSeq();  
       String rightSeq = aRightSeq.get(0).toString();  
       int iRightSeq = EJBUtil.stringToNum(rightSeq);  



CodeDao  cDao= new CodeDao();
int pmtclsId = cDao.getCodeId("studystat", "prmnt_cls");


String pmtclsDesc = cDao.getCodeDescription();


//get default super user rights , returns null if user does not belong to a super user group
String superuserRights = "";

superuserRights = GroupDao.getDefaultStudySuperUserRights(userId);

	CodeDao cdPhase = new CodeDao();
	cdPhase.getCodeValues("phase");
	cdPhase.setCType("phase");
	cdPhase.setForGroup(defUserGroup);
		
		
	String ddPhase = "";
	ddPhase = cdPhase.toPullDown("stdphase",0); 
	
	CodeDao cdStat = new CodeDao();
	cdStat.getCodeValues("studystat");
	
	cdStat.setCType("studystat");
	cdStat.setForGroup(defUserGroup);
	
	
	String stdstatus = "",currentStatus = "";
	
	stdstatus = cdStat.toPullDown("stdstatus",0," id = 'stdstatus'"); 
	currentStatus = cdStat.toPullDown("currentStatus",0," id = 'currentStat'");
	 



//     userId = (String) tSession.getValue("userId");
    int pageRight = 0;
    GrpRightsJB grpRights = (GrpRightsJB)     tSession.getValue("GRights");        
    pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));
    
    if (pageRight > 0 )    {
    	Object[] arguments = {pmtclsDesc};
    	String searchCriteria = request.getParameter("searchCriteria");

    
    %>
        
  <Form name="advStudysearchpg" METHOD="POST" onsubmit="return false;">

    <table width="100%" cellspacing="0" cellpadding="0" class="basetbl" style="border-collapse: separate;">
    <!--  <tr height="5"><td></td></tr> -->
    <tr>
		<td><p><%=LC.Std_Studies %></p></td>
	</tr>
	<tr style="text-align:right" >
    	<td width="15%"><%=LC.L_Sear_by_std_or_ttl %>: &nbsp&nbsp&nbsp<Input type=text name="searchCriteria" class="textboxirb" id="searchCriteria"  
    		value="<%=StringUtil.htmlEncodeXss(StringUtil.trueValue(searchCriteria)) %>">
    	</td>
    	
    	<td width="30%" align="left">
    	<button type="submit" onClick="checkSearch(document.advStudysearchpg);"><%=LC.L_Search%></button>
    	
    	<a style="margin-left:10%" href="advStudysearchpg.jsp" ><%=LC.L_Adva_Search%></a> 
    	</td>
            
       
           
       
  
     </tr>
     <tr style="text-align:right">
        <td><%=LC.L_PI%><%--PI*****--%>:&nbsp&nbsp&nbsp <input type="text" class="textboxirb"   name="dmg" id="dmg"></td> 
      </tr> 
      
      
   
  
                   
               
    </table>
    <a href="irbnewinit.jsp?srcmenu=tdMenuBarItem3&selectedTab=irb_init_tab&mode=N" style="color:blue"><%=LC.L_initiate_new_app%></a>  &nbsp&nbsp <a href="irbongoing.jsp?mode=N" style="color:blue"><%=LC.L_ong_Stud%></a>

        
    <input type="hidden" id="srcmenu" value="<%=src%>">
    <input type="hidden" name="page" value="<%=curPage%>">
    <input type="hidden" id="accountId" value="<%=accountId%>">
    <input type="hidden" id="userId" value="<%=userId%>">
    <input type="hidden" id="grpId" value="<%=grpId%>">
    <input type="hidden" id="groupName" value="<%=groupName%>">
    <input type="hidden" id="rightSeq" value="<%=iRightSeq%>">
    <input type="hidden" id="superuserRights" value="<%=superuserRights%>">
    <input type="hidden" id="pmtclsId" value="<%=pmtclsId%>">
    <input type="hidden" id="moduleName"  name="moduleName" value="Study">
    <input type="hidden" id="tabsubtype" value="<%=tabsubtype%>">
    <input type="hidden" id="CSRFToken" name="CSRFToken">
    
       
        <div >
    <div id="serverpagination_study" ></div>
    </div>
    

    
    
    
  </Form>

    <%
    } //end of if body for page right
    else{
%>
    <%@include file="accessdenied.jsp" %>
<%
    } //end of else body for page right
}//end of if body for session
else {
%>
    
    <jsp:include page="timeout.html" flush="true"/>
<%}
%>
<div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</div>

<DIV class="mainMenu" id = "emenu"> 
  <jsp:include page="getmenu.jsp" flush="true"/>      
</DIV>

</body>
</html>
