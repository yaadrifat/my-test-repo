<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@page import="com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.LC"%>
<%@page import="org.json.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint" />
<jsp:useBean id="ntDao" scope="request" class="com.velos.eres.business.common.NetworkDao" />
<link rel="stylesheet" href="styles/default/common.css" />

<title><%=LC.L_DispAllNtwk%></title>
<script>
function selectNetwork(networkId){
	window.opener.fetchNetworkRows(networkId);
	window.opener.selectedNtwId=networkId;
	window.close();
}
</script>

<style>

#msg {
color: #1D536D;
font-size: 13px;
font-weight: bold;
background-color: #E3E3E3;
margin: 0px 0px 0px 0px;
}

</style>

</head>
<body>
<%
HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession))
{
String acc = (String) tSession.getValue("accountId");
String networkId=(request.getParameter("networkId")==null)?"":request.getParameter("networkId");
JSONArray maplist = new JSONArray();
maplist= ntDao.autocompleteNetworks("",EJBUtil.stringToNum(acc),networkId);
JSONObject data=null;

%>

<p id="msg" ><%=LC.L_Sel_NtwDisp%></p>

<br/>
<div style="width:100%;overflow:auto; height:500px;">
<table width="100%" cellspacing="0" cellpadding="4" border="1" class="outline midAlign">
      <tr style="background-color: #D3D3D3;">
        <th width="60%" ><%=LC.L_Organization_Name %></th>
        <th width="30%" ><%=LC.L_Relation_Type%></th>
        <th width="10%" align="center"><%=LC.L_Select%></th>
      </tr>
      <%
      for(int i=0;i<maplist.length();i++){
    	  data=maplist.getJSONObject(i);
  	 %>
  	  <tr>
      <td><%=data.getString("network_name") %></td>
      <td><%=data.getString("relnType") %></td>
      <td align="center"><a href="#" onclick="selectNetwork(<%=data.getString("pk_nwsites")%>)"><%=LC.L_Select%></a></td>
      </tr>
      <%}%>
</table>
</div>
<%
}//end of if body for session
else
{%>
  	<jsp:include page="timeout.html" flush="true"/>
<%}%>
</body>
</html>