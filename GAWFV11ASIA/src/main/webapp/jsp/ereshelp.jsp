<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns="http://www.w3.org/TR/REC-html40">
<%@page import="com.velos.eres.service.util.MC"%>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Word.Document>
<meta name=Generator content="Microsoft Word 9">
<meta name=Originator content="Microsoft Word 9">

<link rel=File-List
href="./Velos%20eResearch%20User%20Manual(v-4.0-2.3).online%20help_files/filelist.xml">
<link rel=Edit-Time-Data
href="./Velos%20eResearch%20User%20Manual(v-4.0-2.3).online%20help_files/editdata.mso">

<title><%=MC.M_Velos_EresHelp%><%--Velos eResearch Help*****--%></title>

<style>

/* ereshelp.css */

/*

 /* Font Definitions */
@font-face
	{font-family:Wingdings;
	panose-1:5 0 0 0 0 0 0 0 0 0;
	mso-font-charset:2;
	mso-generic-font-family:auto;
	mso-font-pitch:variable;
	mso-font-signature:0 268435456 0 0 -2147483648 0;}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;
	mso-font-charset:0;
	mso-generic-font-family:swiss;
	mso-font-pitch:variable;
	mso-font-signature:553679495 -2147483648 8 0 66047 0;}
@font-face
	{font-family:"Arial Black";
	panose-1:2 11 10 4 2 1 2 2 2 4;
	mso-font-charset:0;
	mso-generic-font-family:swiss;
	mso-font-pitch:variable;
	mso-font-signature:647 0 0 0 159 0;}
@font-face
	{font-family:Impact;
	panose-1:2 11 8 6 3 9 2 5 2 4;
	mso-font-charset:0;
	mso-generic-font-family:swiss;
	mso-font-pitch:variable;
	mso-font-signature:647 0 0 0 159 0;}
@font-face
	{font-family:"MS Outlook";
	panose-1:5 0 0 0 0 0 0 0 0 0;
	mso-font-charset:2;
	mso-generic-font-family:auto;
	mso-font-pitch:variable;
	mso-font-signature:0 268435456 0 0 -2147483648 0;}
 /* Style Definitions */
p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-parent:"";
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
h1
	{mso-style-next:Normal;
	margin:0in;
	margin-bottom:.0001pt;
	text-align:justify;
	mso-pagination:widow-orphan;
	page-break-after:avoid;
	mso-outline-level:1;
	font-size:22.0pt;
	mso-bidi-font-size:10.0pt;
	font-family:Tahoma;
	mso-bidi-font-family:"Times New Roman";
	color:teal;
	mso-font-kerning:0pt;
	mso-bidi-font-weight:normal;}
h2
	{mso-style-next:Normal;
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	page-break-after:avoid;
	mso-outline-level:2;
	font-size:12.0pt;
	mso-bidi-font-size:10.0pt;
	font-family:"Times New Roman";
	font-weight:normal;}
h3
	{mso-style-next:Normal;
	margin-top:12.0pt;
	margin-right:0in;
	margin-bottom:3.0pt;
	margin-left:0in;
	mso-pagination:widow-orphan;
	page-break-after:avoid;
	mso-outline-level:3;
	font-size:12.0pt;
	mso-bidi-font-size:10.0pt;
	font-family:Arial;
	mso-bidi-font-family:"Times New Roman";
	font-weight:normal;}
h4
	{mso-style-next:Normal;
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	page-break-after:avoid;
	mso-outline-level:4;
	font-size:11.0pt;
	mso-bidi-font-size:10.0pt;
	font-family:"Times New Roman";
	color:teal;
	mso-bidi-font-weight:normal;
	font-style:italic;
	mso-bidi-font-style:normal;
	text-decoration:underline;
	text-underline:single;}
h5
	{mso-style-next:Normal;
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	page-break-after:avoid;
	mso-outline-level:5;
	font-size:24.0pt;
	mso-bidi-font-size:10.0pt;
	font-family:Tahoma;
	mso-bidi-font-family:"Times New Roman";
	color:blue;
	text-shadow:auto;
	mso-bidi-font-weight:normal;}
h6
	{mso-style-next:Normal;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:4.5in;
	margin-bottom:.0001pt;
	text-indent:.5in;
	mso-pagination:widow-orphan;
	page-break-after:avoid;
	mso-outline-level:6;
	font-size:12.0pt;
	mso-bidi-font-size:10.0pt;
	font-family:Tahoma;
	mso-bidi-font-family:"Times New Roman";
	color:teal;
	text-shadow:auto;
	font-weight:normal;
	font-style:italic;
	mso-bidi-font-style:normal;}
p.MsoHeading7, li.MsoHeading7, div.MsoHeading7
	{mso-style-next:Normal;
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	page-break-after:avoid;
	mso-outline-level:7;
	font-size:12.0pt;
	mso-bidi-font-size:10.0pt;
	font-family:Arial;
	mso-fareast-font-family:"Times New Roman";
	mso-bidi-font-family:"Times New Roman";
	color:purple;
	font-weight:bold;
	mso-bidi-font-weight:normal;
	font-style:italic;
	mso-bidi-font-style:normal;}
p.MsoHeading8, li.MsoHeading8, div.MsoHeading8
	{mso-style-next:Normal;
	margin:0in;
	margin-bottom:.0001pt;
	text-align:justify;
	line-height:13.15pt;
	mso-pagination:widow-orphan;
	page-break-after:avoid;
	mso-outline-level:8;
	font-size:10.0pt;
	mso-bidi-font-size:8.0pt;
	font-family:Arial;
	mso-fareast-font-family:"Times New Roman";
	font-weight:bold;}
p.MsoHeading9, li.MsoHeading9, div.MsoHeading9
	{mso-style-next:Normal;
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	page-break-after:avoid;
	mso-outline-level:9;
	font-size:16.0pt;
	mso-bidi-font-size:10.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";
	color:white;
	font-weight:bold;
	mso-bidi-font-weight:normal;}
p.MsoIndex1, li.MsoIndex1, div.MsoIndex1
	{mso-style-update:auto;
	mso-style-next:Normal;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:12.0pt;
	margin-bottom:.0001pt;
	text-indent:-12.0pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
p.MsoIndex2, li.MsoIndex2, div.MsoIndex2
	{mso-style-update:auto;
	mso-style-next:Normal;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:24.0pt;
	margin-bottom:.0001pt;
	text-indent:-12.0pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
p.MsoIndex3, li.MsoIndex3, div.MsoIndex3
	{mso-style-update:auto;
	mso-style-next:Normal;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:.5in;
	margin-bottom:.0001pt;
	text-indent:-12.0pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
p.MsoIndex4, li.MsoIndex4, div.MsoIndex4
	{mso-style-update:auto;
	mso-style-next:Normal;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:48.0pt;
	margin-bottom:.0001pt;
	text-indent:-12.0pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
p.MsoIndex5, li.MsoIndex5, div.MsoIndex5
	{mso-style-update:auto;
	mso-style-next:Normal;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:60.0pt;
	margin-bottom:.0001pt;
	text-indent:-12.0pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
p.MsoIndex6, li.MsoIndex6, div.MsoIndex6
	{mso-style-update:auto;
	mso-style-next:Normal;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:1.0in;
	margin-bottom:.0001pt;
	text-indent:-12.0pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
p.MsoIndex7, li.MsoIndex7, div.MsoIndex7
	{mso-style-update:auto;
	mso-style-next:Normal;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:84.0pt;
	margin-bottom:.0001pt;
	text-indent:-12.0pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
p.MsoIndex8, li.MsoIndex8, div.MsoIndex8
	{mso-style-update:auto;
	mso-style-next:Normal;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:96.0pt;
	margin-bottom:.0001pt;
	text-indent:-12.0pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
p.MsoIndex9, li.MsoIndex9, div.MsoIndex9
	{mso-style-update:auto;
	mso-style-next:Normal;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:1.5in;
	margin-bottom:.0001pt;
	text-indent:-12.0pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
p.MsoToc1, li.MsoToc1, div.MsoToc1
	{mso-style-update:auto;
	mso-style-next:Normal;
	margin-top:.25in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	mso-bidi-font-size:14.0pt;
	font-family:Arial;
	mso-fareast-font-family:"Times New Roman";
	mso-bidi-font-family:"Times New Roman";
	text-transform:uppercase;
	font-weight:bold;}
p.MsoToc2, li.MsoToc2, div.MsoToc2
	{mso-style-update:auto;
	mso-style-next:Normal;
	margin-top:12.0pt;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	mso-ansi-font-size:12.0pt;
	mso-bidi-font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";
	font-weight:bold;}
p.MsoToc3, li.MsoToc3, div.MsoToc3
	{mso-style-update:auto;
	mso-style-next:Normal;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:16.0pt;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	mso-ansi-font-size:12.0pt;
	mso-bidi-font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
p.MsoToc4, li.MsoToc4, div.MsoToc4
	{mso-style-update:auto;
	mso-style-next:Normal;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:24.0pt;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	mso-ansi-font-size:12.0pt;
	mso-bidi-font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
p.MsoToc5, li.MsoToc5, div.MsoToc5
	{mso-style-update:auto;
	mso-style-next:Normal;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:.5in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	mso-ansi-font-size:12.0pt;
	mso-bidi-font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
p.MsoToc6, li.MsoToc6, div.MsoToc6
	{mso-style-update:auto;
	mso-style-next:Normal;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:48.0pt;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	mso-ansi-font-size:12.0pt;
	mso-bidi-font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
p.MsoToc7, li.MsoToc7, div.MsoToc7
	{mso-style-update:auto;
	mso-style-next:Normal;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:60.0pt;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	mso-ansi-font-size:12.0pt;
	mso-bidi-font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
p.MsoToc8, li.MsoToc8, div.MsoToc8
	{mso-style-update:auto;
	mso-style-next:Normal;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:1.0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	mso-ansi-font-size:12.0pt;
	mso-bidi-font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
p.MsoToc9, li.MsoToc9, div.MsoToc9
	{mso-style-update:auto;
	mso-style-next:Normal;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:84.0pt;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	mso-ansi-font-size:12.0pt;
	mso-bidi-font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
p.MsoHeader, li.MsoHeader, div.MsoHeader
	{margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	tab-stops:center 3.0in right 6.0in;
	font-size:11.0pt;
	mso-bidi-font-size:10.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";
	color:navy;}
p.MsoFooter, li.MsoFooter, div.MsoFooter
	{margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	tab-stops:center 3.0in right 6.0in;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
p.MsoIndexHeading, li.MsoIndexHeading, div.MsoIndexHeading
	{mso-style-next:"Index 1";
	margin-top:6.0pt;
	margin-right:0in;
	margin-bottom:6.0pt;
	margin-left:0in;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";
	font-weight:bold;
	font-style:italic;}
p.MsoBodyText, li.MsoBodyText, div.MsoBodyText
	{margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:Arial;
	mso-fareast-font-family:"Times New Roman";
	mso-bidi-font-family:"Times New Roman";}
p.MsoBodyTextIndent, li.MsoBodyTextIndent, div.MsoBodyTextIndent
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:.3in;
	margin-bottom:.0001pt;
	text-align:justify;
	line-height:150%;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
p.MsoBodyText2, li.MsoBodyText2, div.MsoBodyText2
	{margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	mso-bidi-font-size:12.0pt;
	font-family:Arial;
	mso-fareast-font-family:"Times New Roman";}
p.MsoBodyText3, li.MsoBodyText3, div.MsoBodyText3
	{margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";
	color:black;}
p.MsoBodyTextIndent2, li.MsoBodyTextIndent2, div.MsoBodyTextIndent2
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:.5in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	mso-bidi-font-size:12.0pt;
	font-family:Arial;
	mso-fareast-font-family:"Times New Roman";}
p.MsoBodyTextIndent3, li.MsoBodyTextIndent3, div.MsoBodyTextIndent3
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:.5in;
	margin-bottom:.0001pt;
	text-indent:.5in;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	mso-bidi-font-size:12.0pt;
	font-family:Arial;
	mso-fareast-font-family:"Times New Roman";}
a:link, span.MsoHyperlink
	{color:blue;
	text-decoration:underline;
	text-underline:single;}
a:visited, span.MsoHyperlinkFollowed
	{color:purple;
	text-decoration:underline;
	text-underline:single;}
p.sectionheadings, li.sectionheadings, div.sectionheadings
	{mso-style-name:sectionheadings;
	margin-right:0in;
	mso-margin-top-alt:auto;
	mso-margin-bottom-alt:auto;
	margin-left:0in;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
ins
	{mso-style-type:export-only;
	text-decoration:none;}
span.msoDel
	{mso-style-type:export-only;
	mso-style-name:"";
	text-decoration:line-through;
	display:none;
	color:red;}
@page Section1
	{size:8.5in 11.0in;
	margin:1.0in 1.25in 1.0in 1.25in;
	mso-header-margin:.5in;
	mso-footer-margin:.5in;
	mso-title-page:yes;
	mso-even-footer:url("./Velos%20eResearch%20User%20Manual(v-4.0-2.3).online%20help_files/header.htm") ef1;
	mso-footer:url("./Velos%20eResearch%20User%20Manual(v-4.0-2.3).online%20help_files/header.htm") f1;
	mso-paper-source:0;}
div.Section1
	{page:Section1;}
 /* List Definitions */
@list l0
	{mso-list-id:21521299;
	mso-list-type:hybrid;
	mso-list-template-ids:-2110493882 338838512 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l0:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l0:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l1
	{mso-list-id:28072002;
	mso-list-type:hybrid;
	mso-list-template-ids:1813443992 67698689 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l1:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l2
	{mso-list-id:33696227;
	mso-list-type:hybrid;
	mso-list-template-ids:-2110493882 338838512 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l2:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l2:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l3
	{mso-list-id:68046223;
	mso-list-type:hybrid;
	mso-list-template-ids:931417456 67698689 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l3:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l4
	{mso-list-id:77220198;
	mso-list-type:hybrid;
	mso-list-template-ids:595460274 67698689 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l4:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l5
	{mso-list-id:110366749;
	mso-list-type:hybrid;
	mso-list-template-ids:-1806530394 -1 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l5:level1
	{mso-level-start-at:0;
	mso-level-number-format:bullet;
	mso-level-text:-;
	mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	margin-left:1.0in;
	text-indent:-.25in;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
@list l6
	{mso-list-id:113718415;
	mso-list-type:hybrid;
	mso-list-template-ids:-199464116 -1591600162 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l6:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l7
	{mso-list-id:127212174;
	mso-list-type:hybrid;
	mso-list-template-ids:1958143720 4250440 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l7:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l8
	{mso-list-id:173960769;
	mso-list-type:simple;
	mso-list-template-ids:1633214798;}
@list l8:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F032;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	margin-left:.25in;
	text-indent:-.25in;
	mso-ansi-font-size:36.0pt;
	font-family:Wingdings;
	mso-ansi-font-weight:normal;
	mso-ansi-font-style:normal;}
@list l9
	{mso-list-id:178198339;
	mso-list-type:hybrid;
	mso-list-template-ids:1497244336 67698689 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l9:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.25in;
	mso-level-number-position:left;
	margin-left:.25in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l10
	{mso-list-id:194583682;
	mso-list-type:simple;
	mso-list-template-ids:-1362964406;}
@list l10:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F02D;
	mso-level-tab-stop:.25in;
	mso-level-number-position:left;
	margin-left:.25in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l11
	{mso-list-id:209153966;
	mso-list-type:hybrid;
	mso-list-template-ids:1709606790 338838512 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l11:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l12
	{mso-list-id:233930060;
	mso-list-type:hybrid;
	mso-list-template-ids:1552199632 338838512 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l12:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l13
	{mso-list-id:274097220;
	mso-list-type:hybrid;
	mso-list-template-ids:1628362646 236599918 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l13:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l14
	{mso-list-id:284315373;
	mso-list-type:hybrid;
	mso-list-template-ids:-2110493882 4250440 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l14:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l14:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l15
	{mso-list-id:286938403;
	mso-list-type:hybrid;
	mso-list-template-ids:-1342828106 338838512 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l15:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l16
	{mso-list-id:290479669;
	mso-list-type:hybrid;
	mso-list-template-ids:1628362646 -323033788 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l16:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l17
	{mso-list-id:294915697;
	mso-list-type:hybrid;
	mso-list-template-ids:1628362646 1075489038 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l17:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l18
	{mso-list-id:312833505;
	mso-list-type:hybrid;
	mso-list-template-ids:911668304 67698689 67698703 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l18:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l18:level2
	{mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l19
	{mso-list-id:329913023;
	mso-list-type:hybrid;
	mso-list-template-ids:-1204378972 67698703 67698689 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l19:level1
	{mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	margin-left:1.0in;
	text-indent:-.25in;}
@list l19:level2
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:1.5in;
	mso-level-number-position:left;
	margin-left:1.5in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l20
	{mso-list-id:345057770;
	mso-list-type:hybrid;
	mso-list-template-ids:-2110493882 271456790 -2089134726 -1 -1 -1 -1 -1 -1 -1;}
@list l20:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l20:level2
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l21
	{mso-list-id:349644408;
	mso-list-type:hybrid;
	mso-list-template-ids:1628362646 978739626 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l21:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l22
	{mso-list-id:357002969;
	mso-list-type:hybrid;
	mso-list-template-ids:1628362646 2046334174 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l22:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l23
	{mso-list-id:367337850;
	mso-list-type:hybrid;
	mso-list-template-ids:1628362646 -417152524 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l23:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l24
	{mso-list-id:378358494;
	mso-list-type:hybrid;
	mso-list-template-ids:-1194132976 67698703 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l24:level1
	{mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l25
	{mso-list-id:382366787;
	mso-list-type:hybrid;
	mso-list-template-ids:151964432 67698689 -2089134726 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l25:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l25:level2
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l26
	{mso-list-id:389117080;
	mso-list-type:hybrid;
	mso-list-template-ids:782637080 -1 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l26:level1
	{mso-level-start-at:0;
	mso-level-number-format:bullet;
	mso-level-text:-;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
@list l27
	{mso-list-id:389691074;
	mso-list-type:hybrid;
	mso-list-template-ids:-158294092 67698689 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l27:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l28
	{mso-list-id:395903999;
	mso-list-type:hybrid;
	mso-list-template-ids:451981192 67698689 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l28:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l28:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l29
	{mso-list-id:398402324;
	mso-list-type:simple;
	mso-list-template-ids:-1490921248;}
@list l29:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0D8;
	mso-level-tab-stop:.45in;
	mso-level-number-position:left;
	margin-left:.25in;
	text-indent:-.05in;
	font-family:Wingdings;}
@list l30
	{mso-list-id:409350579;
	mso-list-type:hybrid;
	mso-list-template-ids:1729263460 1076410732 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l30:level1
	{mso-level-text:"%1\. ";
	mso-level-tab-stop:.75in;
	mso-level-number-position:left;
	margin-left:.75in;
	text-indent:-.25in;
	mso-ansi-font-size:12.0pt;
	font-family:"Times New Roman";
	mso-ansi-font-weight:normal;
	mso-ansi-font-style:normal;
	text-decoration:none;
	text-underline:none;
	text-decoration:none;
	text-line-through:none;}
@list l31
	{mso-list-id:412749200;
	mso-list-type:hybrid;
	mso-list-template-ids:-2110493882 1595825106 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l31:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l31:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l32
	{mso-list-id:417868587;
	mso-list-type:hybrid;
	mso-list-template-ids:-2110493882 -1334512380 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l32:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l32:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l33
	{mso-list-id:418136050;
	mso-list-type:hybrid;
	mso-list-template-ids:1628362646 2046334174 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l33:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l34
	{mso-list-id:421730742;
	mso-list-type:hybrid;
	mso-list-template-ids:1628362646 2000611264 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l34:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l35
	{mso-list-id:435950570;
	mso-list-type:hybrid;
	mso-list-template-ids:1442726980 67698689 67698703 67698689 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l35:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l35:level2
	{mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l35:level3
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:1.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l36
	{mso-list-id:439687232;
	mso-list-type:hybrid;
	mso-list-template-ids:-463562964 -1 -2089134726 -1 -1 -1 -1 -1 -1 -1;}
@list l36:level1
	{mso-level-text:"%1\)";
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l36:level2
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l37
	{mso-list-id:456068360;
	mso-list-type:hybrid;
	mso-list-template-ids:-1278310382 -1 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l37:level1
	{mso-level-start-at:0;
	mso-level-number-format:bullet;
	mso-level-text:-;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
@list l38
	{mso-list-id:456678555;
	mso-list-type:hybrid;
	mso-list-template-ids:-1016295018 67698703 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l38:level1
	{mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	margin-left:1.0in;
	text-indent:-.25in;}
@list l39
	{mso-list-id:458501751;
	mso-list-type:hybrid;
	mso-list-template-ids:-2110493882 1508559062 -2089134726 -1 -1 -1 -1 -1 -1 -1;}
@list l39:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l39:level2
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l40
	{mso-list-id:462816883;
	mso-list-type:hybrid;
	mso-list-template-ids:1628362646 50515974 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l40:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l41
	{mso-list-id:463695448;
	mso-list-type:hybrid;
	mso-list-template-ids:-1806530394 4250440 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l41:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l42
	{mso-list-id:467819453;
	mso-list-type:simple;
	mso-list-template-ids:-1362964406;}
@list l42:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F02D;
	mso-level-tab-stop:.25in;
	mso-level-number-position:left;
	margin-left:.25in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l43
	{mso-list-id:478768206;
	mso-list-type:hybrid;
	mso-list-template-ids:1236434480 4250440 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l43:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l44
	{mso-list-id:479149490;
	mso-list-type:hybrid;
	mso-list-template-ids:1628362646 -1733140328 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l44:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l45
	{mso-list-id:521012507;
	mso-list-type:hybrid;
	mso-list-template-ids:-199464116 -1591600162 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l45:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l46
	{mso-list-id:523135292;
	mso-list-type:hybrid;
	mso-list-template-ids:-2110493882 4250440 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l46:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l46:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l47
	{mso-list-id:533814377;
	mso-list-type:simple;
	mso-list-template-ids:-1490921248;}
@list l47:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0D8;
	mso-level-tab-stop:.45in;
	mso-level-number-position:left;
	margin-left:.25in;
	text-indent:-.05in;
	font-family:Wingdings;}
@list l48
	{mso-list-id:547835360;
	mso-list-type:hybrid;
	mso-list-template-ids:2076474152 67698689 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l48:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l49
	{mso-list-id:548416725;
	mso-list-type:hybrid;
	mso-list-template-ids:1628362646 917287444 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l49:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l49:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l50
	{mso-list-id:550308544;
	mso-list-type:hybrid;
	mso-list-template-ids:1761410286 4250440 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l50:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l51
	{mso-list-id:562645081;
	mso-list-type:hybrid;
	mso-list-template-ids:1774749554 67698689 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l51:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l52
	{mso-list-id:579488484;
	mso-list-type:simple;
	mso-list-template-ids:67698689;}
@list l52:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.25in;
	mso-level-number-position:left;
	margin-left:.25in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l53
	{mso-list-id:581258683;
	mso-list-type:hybrid;
	mso-list-template-ids:-2110493882 338838512 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l53:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l53:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l54
	{mso-list-id:585457524;
	mso-list-type:simple;
	mso-list-template-ids:-1362964406;}
@list l54:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F02D;
	mso-level-tab-stop:.25in;
	mso-level-number-position:left;
	margin-left:.25in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l55
	{mso-list-id:611014441;
	mso-list-type:hybrid;
	mso-list-template-ids:1628362646 -2107486770 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l55:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l55:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l56
	{mso-list-id:622931835;
	mso-list-type:hybrid;
	mso-list-template-ids:-1978599526 -1 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l56:level1
	{mso-level-start-at:0;
	mso-level-number-format:bullet;
	mso-level-text:-;
	mso-level-tab-stop:1.75in;
	mso-level-number-position:left;
	margin-left:1.75in;
	text-indent:-.25in;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
@list l57
	{mso-list-id:624000619;
	mso-list-type:hybrid;
	mso-list-template-ids:1805133204 -1 67698689 -1 -1 -1 -1 -1 -1 -1;}
@list l57:level1
	{mso-level-start-at:0;
	mso-level-number-format:bullet;
	mso-level-text:-;
	mso-level-tab-stop:.8in;
	mso-level-number-position:left;
	margin-left:.8in;
	text-indent:-.3in;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
@list l57:level2
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:1.35in;
	mso-level-number-position:left;
	margin-left:1.35in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l58
	{mso-list-id:633171859;
	mso-list-type:hybrid;
	mso-list-template-ids:1628362646 2046334174 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l58:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l59
	{mso-list-id:639384658;
	mso-list-type:hybrid;
	mso-list-template-ids:-1393792286 -1 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l59:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	text-indent:-.2in;
	font-family:Symbol;}
@list l60
	{mso-list-id:648021574;
	mso-list-type:hybrid;
	mso-list-template-ids:-2110493882 338838512 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l60:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l60:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l61
	{mso-list-id:654262772;
	mso-list-type:hybrid;
	mso-list-template-ids:-2110493882 338838512 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l61:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l61:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l62
	{mso-list-id:663625257;
	mso-list-type:hybrid;
	mso-list-template-ids:-325803022 -1 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l62:level1
	{mso-level-start-at:0;
	mso-level-number-format:bullet;
	mso-level-text:-;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
@list l62:level2
	{mso-level-start-at:0;
	mso-level-number-format:bullet;
	mso-level-text:-;
	mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
@list l63
	{mso-list-id:668018943;
	mso-list-type:hybrid;
	mso-list-template-ids:55360632 67698689 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l63:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l64
	{mso-list-id:668604184;
	mso-list-type:hybrid;
	mso-list-template-ids:2009264154 67698689 67698703 67698689 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l64:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l64:level2
	{mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l64:level3
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:1.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l65
	{mso-list-id:678432153;
	mso-list-type:hybrid;
	mso-list-template-ids:-920776840 338838512 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l65:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l65:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l65:level3
	{mso-level-number-format:bullet;
	mso-level-text:\F0A7;
	mso-level-tab-stop:1.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Wingdings;}
@list l66
	{mso-list-id:686560981;
	mso-list-type:hybrid;
	mso-list-template-ids:912050528 67698689 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l66:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l67
	{mso-list-id:717820243;
	mso-list-type:hybrid;
	mso-list-template-ids:2080639110 -1099786390 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l67:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.45in;
	mso-level-number-position:left;
	margin-left:.45in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l68
	{mso-list-id:722368022;
	mso-list-type:hybrid;
	mso-list-template-ids:1410215574 -567875834 -2089134726 -1 -1 -1 -1 -1 -1 -1;}
@list l68:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l68:level2
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l69
	{mso-list-id:723988641;
	mso-list-type:hybrid;
	mso-list-template-ids:55360632 13430468 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l69:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l70
	{mso-list-id:754012922;
	mso-list-type:hybrid;
	mso-list-template-ids:578186938 -1 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l70:level1
	{mso-level-start-at:0;
	mso-level-number-format:bullet;
	mso-level-text:-;
	mso-level-tab-stop:.45in;
	mso-level-number-position:left;
	margin-left:.45in;
	text-indent:-.3in;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
@list l70:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l71
	{mso-list-id:772937267;
	mso-list-type:hybrid;
	mso-list-template-ids:1028060010 338838512 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l71:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l72
	{mso-list-id:781266786;
	mso-list-type:hybrid;
	mso-list-template-ids:-1393792286 338838512 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l72:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l73
	{mso-list-id:786503887;
	mso-list-type:hybrid;
	mso-list-template-ids:911668304 67698689 67698703 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l73:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l73:level2
	{mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l74
	{mso-list-id:847478024;
	mso-list-type:hybrid;
	mso-list-template-ids:-1011978446 67698689 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l74:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l75
	{mso-list-id:856845248;
	mso-list-type:hybrid;
	mso-list-template-ids:-598551676 67698689 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l75:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l76
	{mso-list-id:856961186;
	mso-list-type:hybrid;
	mso-list-template-ids:-158294092 13430468 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l76:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l77
	{mso-list-id:857307458;
	mso-list-type:hybrid;
	mso-list-template-ids:-2110493882 338838512 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l77:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l77:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l78
	{mso-list-id:859468061;
	mso-list-type:hybrid;
	mso-list-template-ids:-1939421284 67698689 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l78:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	margin-left:1.0in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l79
	{mso-list-id:859516062;
	mso-list-type:hybrid;
	mso-list-template-ids:451981192 13430468 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l79:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l79:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l80
	{mso-list-id:861014883;
	mso-list-type:simple;
	mso-list-template-ids:-1310162068;}
@list l80:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F071;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.25in;
	text-indent:.05in;
	mso-ansi-font-size:8.0pt;
	font-family:Wingdings;}
@list l81
	{mso-list-id:873882574;
	mso-list-type:hybrid;
	mso-list-template-ids:1628362646 2046334174 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l81:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l82
	{mso-list-id:880559023;
	mso-list-type:hybrid;
	mso-list-template-ids:-1725035950 67698689 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l82:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l83
	{mso-list-id:890725626;
	mso-list-type:hybrid;
	mso-list-template-ids:2009264154 -1591600162 67698703 67698689 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l83:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l83:level2
	{mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l83:level3
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:1.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l84
	{mso-list-id:897471877;
	mso-list-type:hybrid;
	mso-list-template-ids:2076474152 -1471806594 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l84:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l85
	{mso-list-id:899940920;
	mso-list-type:hybrid;
	mso-list-template-ids:-2110493882 -1 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l85:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	text-indent:-.2in;
	font-family:Symbol;}
@list l85:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l86
	{mso-list-id:907618534;
	mso-list-type:hybrid;
	mso-list-template-ids:55360632 13430468 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l86:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l87
	{mso-list-id:917592074;
	mso-list-type:hybrid;
	mso-list-template-ids:-1894240102 67698703 67698689 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l87:level1
	{mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	margin-left:1.0in;
	text-indent:-.25in;}
@list l87:level2
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:1.5in;
	mso-level-number-position:left;
	margin-left:1.5in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l88
	{mso-list-id:932517499;
	mso-list-type:hybrid;
	mso-list-template-ids:1558211522 338838512 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l88:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l89
	{mso-list-id:939030009;
	mso-list-type:hybrid;
	mso-list-template-ids:1628362646 -1729828694 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l89:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l90
	{mso-list-id:951935642;
	mso-list-type:hybrid;
	mso-list-template-ids:-2110493882 338838512 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l90:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l90:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l91
	{mso-list-id:974917167;
	mso-list-type:hybrid;
	mso-list-template-ids:1628362646 -138790306 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l91:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l92
	{mso-list-id:978265921;
	mso-list-type:hybrid;
	mso-list-template-ids:-2110493882 4250440 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l92:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l92:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l93
	{mso-list-id:999389893;
	mso-list-type:hybrid;
	mso-list-template-ids:1552199632 -1 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l93:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	text-indent:-.2in;
	font-family:Symbol;}
@list l94
	{mso-list-id:1007486085;
	mso-list-type:hybrid;
	mso-list-template-ids:1558211522 4250440 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l94:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l95
	{mso-list-id:1012416449;
	mso-list-type:hybrid;
	mso-list-template-ids:587901230 67698689 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l95:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l96
	{mso-list-id:1050108246;
	mso-list-type:hybrid;
	mso-list-template-ids:-1252344316 -1 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l96:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	text-indent:-.2in;
	font-family:Symbol;}
@list l97
	{mso-list-id:1065837620;
	mso-list-type:hybrid;
	mso-list-template-ids:1628362646 -138790306 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l97:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l97:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l98
	{mso-list-id:1105688490;
	mso-list-type:hybrid;
	mso-list-template-ids:136461386 338838512 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l98:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l99
	{mso-list-id:1116371511;
	mso-list-type:hybrid;
	mso-list-template-ids:-1615037350 67698689 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l99:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l100
	{mso-list-id:1123424997;
	mso-list-type:hybrid;
	mso-list-template-ids:-133392424 67698689 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l100:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l101
	{mso-list-id:1134249773;
	mso-list-type:hybrid;
	mso-list-template-ids:-1613722300 4250440 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l101:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l102
	{mso-list-id:1146434149;
	mso-list-type:hybrid;
	mso-list-template-ids:595460274 385924394 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l102:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l103
	{mso-list-id:1173377077;
	mso-list-type:hybrid;
	mso-list-template-ids:-1806530394 338838512 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l103:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l104
	{mso-list-id:1174879859;
	mso-list-type:hybrid;
	mso-list-template-ids:662371442 1481808254 371888550 383149800 1221876920 -1853697356 30941460 1968470536 165308652 -2117968656;}
@list l104:level1
	{mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l105
	{mso-list-id:1245609755;
	mso-list-type:hybrid;
	mso-list-template-ids:-2110493882 338838512 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l105:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l105:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l106
	{mso-list-id:1247499263;
	mso-list-type:hybrid;
	mso-list-template-ids:55360632 13430468 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l106:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l107
	{mso-list-id:1249341808;
	mso-list-type:hybrid;
	mso-list-template-ids:1628362646 2046334174 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l107:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l108
	{mso-list-id:1262185056;
	mso-list-type:hybrid;
	mso-list-template-ids:1628362646 -138790306 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l108:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l109
	{mso-list-id:1278633429;
	mso-list-type:hybrid;
	mso-list-template-ids:-1278310382 -1 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l109:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l110
	{mso-list-id:1278946914;
	mso-list-type:hybrid;
	mso-list-template-ids:1474723166 67698703 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l110:level1
	{mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	margin-left:1.0in;
	text-indent:-.25in;}
@list l111
	{mso-list-id:1285696448;
	mso-list-type:hybrid;
	mso-list-template-ids:477900112 -2131222658 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l111:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l112
	{mso-list-id:1294873509;
	mso-list-type:hybrid;
	mso-list-template-ids:-1806530394 4250440 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l112:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l113
	{mso-list-id:1329823215;
	mso-list-type:hybrid;
	mso-list-template-ids:508195402 67698689 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l113:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l114
	{mso-list-id:1333609755;
	mso-list-type:hybrid;
	mso-list-template-ids:-2110493882 338838512 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l114:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l114:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l115
	{mso-list-id:1333679302;
	mso-list-type:hybrid;
	mso-list-template-ids:595460274 13430468 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l115:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l116
	{mso-list-id:1338271542;
	mso-list-type:hybrid;
	mso-list-template-ids:-1598769100 67698703 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l116:level1
	{mso-level-tab-stop:.25in;
	mso-level-number-position:left;
	margin-left:.25in;
	text-indent:-.25in;}
@list l117
	{mso-list-id:1348751112;
	mso-list-type:hybrid;
	mso-list-template-ids:1628362646 659199004 67698703 67698689 -1 -1 -1 -1 -1 -1;}
@list l117:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l117:level2
	{mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l117:level3
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:1.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l118
	{mso-list-id:1351374139;
	mso-list-type:hybrid;
	mso-list-template-ids:-336056164 -1611786894 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l118:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l119
	{mso-list-id:1355113261;
	mso-list-type:simple;
	mso-list-template-ids:-836051794;}
@list l119:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F071;
	mso-level-tab-stop:.75in;
	mso-level-number-position:left;
	margin-left:.25in;
	text-indent:.25in;
	mso-ansi-font-size:8.0pt;
	font-family:Wingdings;}
@list l120
	{mso-list-id:1362512578;
	mso-list-type:hybrid;
	mso-list-template-ids:1628362646 -138790306 67698703 67698689 -1 -1 -1 -1 -1 -1;}
@list l120:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l120:level2
	{mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l120:level3
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:1.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l121
	{mso-list-id:1366104491;
	mso-list-type:hybrid;
	mso-list-template-ids:-1806530394 338838512 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l121:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l122
	{mso-list-id:1373574875;
	mso-list-type:hybrid;
	mso-list-template-ids:1958143720 338838512 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l122:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l123
	{mso-list-id:1378623385;
	mso-list-type:hybrid;
	mso-list-template-ids:1110474834 338838512 -2089134726 67698689 1010960700 -1 -1 -1 -1 -1;}
@list l123:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l123:level2
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l123:level3
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:1.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l123:level4
	{mso-level-start-at:0;
	mso-level-number-format:bullet;
	mso-level-text:-;
	mso-level-tab-stop:2.0in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
@list l124
	{mso-list-id:1390422638;
	mso-list-type:hybrid;
	mso-list-template-ids:1628362646 535184854 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l124:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l125
	{mso-list-id:1394306243;
	mso-list-type:hybrid;
	mso-list-template-ids:1628362646 -1934431898 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l125:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l126
	{mso-list-id:1395079276;
	mso-list-type:hybrid;
	mso-list-template-ids:1761410286 338838512 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l126:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l127
	{mso-list-id:1432512609;
	mso-list-type:hybrid;
	mso-list-template-ids:-2110493882 338838512 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l127:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l127:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l128
	{mso-list-id:1443955216;
	mso-list-type:hybrid;
	mso-list-template-ids:-1022838544 67698689 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l128:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l129
	{mso-list-id:1448503873;
	mso-list-type:simple;
	mso-list-template-ids:-99707962;}
@list l129:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F04D;
	mso-level-tab-stop:.6in;
	mso-level-number-position:left;
	margin-left:.6in;
	text-indent:-.6in;
	mso-ansi-font-size:36.0pt;
	font-family:Wingdings;
	mso-ansi-font-weight:normal;
	mso-ansi-font-style:normal;}
@list l130
	{mso-list-id:1454907778;
	mso-list-type:hybrid;
	mso-list-template-ids:508195402 2046334174 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l130:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l131
	{mso-list-id:1470053758;
	mso-list-type:hybrid;
	mso-list-template-ids:-1216329086 -1 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l131:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l132
	{mso-list-id:1498156041;
	mso-list-type:hybrid;
	mso-list-template-ids:-755434990 -1 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l132:level1
	{mso-level-start-at:0;
	mso-level-number-format:bullet;
	mso-level-text:-;
	mso-level-tab-stop:.45in;
	mso-level-number-position:left;
	margin-left:.45in;
	text-indent:-.3in;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
@list l133
	{mso-list-id:1504474524;
	mso-list-type:hybrid;
	mso-list-template-ids:-1806530394 338838512 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l133:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l134
	{mso-list-id:1532919361;
	mso-list-type:hybrid;
	mso-list-template-ids:-1891482286 338838512 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l134:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l135
	{mso-list-id:1544362349;
	mso-list-type:hybrid;
	mso-list-template-ids:749010696 1202363528 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l135:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	margin-left:.45in;
	text-indent:-.2in;
	font-family:Symbol;}
@list l136
	{mso-list-id:1545410521;
	mso-list-type:hybrid;
	mso-list-template-ids:72399938 338838512 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l136:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l137
	{mso-list-id:1546672532;
	mso-list-type:hybrid;
	mso-list-template-ids:1628362646 -138790306 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l137:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l137:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l138
	{mso-list-id:1553269976;
	mso-list-type:hybrid;
	mso-list-template-ids:-531625882 -1 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l138:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	text-indent:-.2in;
	font-family:Symbol;}
@list l139
	{mso-list-id:1582906106;
	mso-list-type:hybrid;
	mso-list-template-ids:-1289329154 67698703 67698689 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l139:level1
	{mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l139:level2
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l140
	{mso-list-id:1589923923;
	mso-list-type:hybrid;
	mso-list-template-ids:-1984145282 67698689 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l140:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.65in;
	mso-level-number-position:left;
	margin-left:.65in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l141
	{mso-list-id:1590697267;
	mso-list-type:hybrid;
	mso-list-template-ids:-1225888144 67698703 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l141:level1
	{mso-level-tab-stop:.25in;
	mso-level-number-position:left;
	margin-left:.25in;
	text-indent:-.25in;}
@list l142
	{mso-list-id:1613130413;
	mso-list-type:hybrid;
	mso-list-template-ids:1518354546 -1466260444 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l142:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.4in;
	mso-level-number-position:left;
	margin-left:.4in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l143
	{mso-list-id:1614167949;
	mso-list-type:hybrid;
	mso-list-template-ids:-1613722300 67698689 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l143:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l144
	{mso-list-id:1635329978;
	mso-list-type:hybrid;
	mso-list-template-ids:-2110493882 338838512 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l144:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l144:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l145
	{mso-list-id:1663581998;
	mso-list-type:hybrid;
	mso-list-template-ids:1628362646 164924892 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l145:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l146
	{mso-list-id:1684236219;
	mso-list-type:hybrid;
	mso-list-template-ids:-2110493882 13430468 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l146:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l146:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l147
	{mso-list-id:1686665826;
	mso-list-type:simple;
	mso-list-template-ids:1976718368;}
@list l147:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F042;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	margin-left:.3in;
	text-indent:-.3in;
	mso-ansi-font-size:22.0pt;
	font-family:"MS Outlook";
	mso-ansi-font-weight:normal;
	mso-ansi-font-style:normal;}
@list l148
	{mso-list-id:1700157213;
	mso-list-type:simple;
	mso-list-template-ids:-377615514;}
@list l148:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F071;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.25in;
	text-indent:.05in;
	mso-ansi-font-size:8.0pt;
	font-family:Wingdings;}
@list l149
	{mso-list-id:1700279107;
	mso-list-type:hybrid;
	mso-list-template-ids:-1308845690 67698689 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l149:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l149:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l150
	{mso-list-id:1702314839;
	mso-list-type:simple;
	mso-list-template-ids:67698689;}
@list l150:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.25in;
	mso-level-number-position:left;
	margin-left:.25in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l151
	{mso-list-id:1706785157;
	mso-list-type:hybrid;
	mso-list-template-ids:-920776840 4250440 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l151:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l151:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l151:level3
	{mso-level-number-format:bullet;
	mso-level-text:\F0A7;
	mso-level-tab-stop:1.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Wingdings;}
@list l152
	{mso-list-id:1724451304;
	mso-list-type:hybrid;
	mso-list-template-ids:55360632 13430468 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l152:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l153
	{mso-list-id:1724596844;
	mso-list-type:simple;
	mso-list-template-ids:-1490921248;}
@list l153:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0D8;
	mso-level-tab-stop:.45in;
	mso-level-number-position:left;
	margin-left:.25in;
	text-indent:-.05in;
	font-family:Wingdings;}
@list l154
	{mso-list-id:1745251488;
	mso-list-type:hybrid;
	mso-list-template-ids:226029870 338838512 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l154:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l155
	{mso-list-id:1745254879;
	mso-list-type:hybrid;
	mso-list-template-ids:1958143720 -1 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l155:level1
	{mso-level-start-at:0;
	mso-level-number-format:bullet;
	mso-level-text:-;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
@list l156
	{mso-list-id:1758206862;
	mso-list-type:hybrid;
	mso-list-template-ids:-2110493882 338838512 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l156:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l156:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l157
	{mso-list-id:1759253663;
	mso-list-type:simple;
	mso-list-template-ids:-1490921248;}
@list l157:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0D8;
	mso-level-tab-stop:.45in;
	mso-level-number-position:left;
	margin-left:.25in;
	text-indent:-.05in;
	font-family:Wingdings;}
@list l158
	{mso-list-id:1762334372;
	mso-list-type:hybrid;
	mso-list-template-ids:1628362646 2046334174 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l158:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l159
	{mso-list-id:1790124948;
	mso-list-type:hybrid;
	mso-list-template-ids:1410215574 -1 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l159:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	text-indent:-.2in;
	font-family:Symbol;}
@list l160
	{mso-list-id:1809129601;
	mso-list-type:simple;
	mso-list-template-ids:-1362964406;}
@list l160:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F02D;
	mso-level-tab-stop:.25in;
	mso-level-number-position:left;
	margin-left:.25in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l161
	{mso-list-id:1809737747;
	mso-list-type:hybrid;
	mso-list-template-ids:-931648796 338838512 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l161:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l161:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l162
	{mso-list-id:1813015169;
	mso-list-type:hybrid;
	mso-list-template-ids:1236434480 338838512 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l162:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l163
	{mso-list-id:1818909840;
	mso-list-type:hybrid;
	mso-list-template-ids:-1961466250 67698689 67698703 67698689 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l163:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l163:level2
	{mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l163:level3
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:1.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l164
	{mso-list-id:1830826134;
	mso-list-type:simple;
	mso-list-template-ids:-1490921248;}
@list l164:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0D8;
	mso-level-tab-stop:.45in;
	mso-level-number-position:left;
	margin-left:.25in;
	text-indent:-.05in;
	font-family:Wingdings;}
@list l165
	{mso-list-id:1855068489;
	mso-list-type:hybrid;
	mso-list-template-ids:1958143720 4250440 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l165:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l166
	{mso-list-id:1861162414;
	mso-list-type:hybrid;
	mso-list-template-ids:1729263460 1940949618 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l166:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l167
	{mso-list-id:1868568523;
	mso-list-type:hybrid;
	mso-list-template-ids:299271622 67698703 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l167:level1
	{mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l168
	{mso-list-id:1883327381;
	mso-list-type:hybrid;
	mso-list-template-ids:931417456 13430468 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l168:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l169
	{mso-list-id:1886209190;
	mso-list-type:hybrid;
	mso-list-template-ids:1628362646 2096132962 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l169:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l170
	{mso-list-id:1896114621;
	mso-list-type:hybrid;
	mso-list-template-ids:-2137476640 338838512 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l170:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l171
	{mso-list-id:1907253665;
	mso-list-type:hybrid;
	mso-list-template-ids:1028060010 -1 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l171:level1
	{mso-level-start-at:0;
	mso-level-number-format:bullet;
	mso-level-text:-;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
@list l172
	{mso-list-id:1911885881;
	mso-list-type:simple;
	mso-list-template-ids:-1490921248;}
@list l172:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0D8;
	mso-level-tab-stop:.45in;
	mso-level-number-position:left;
	margin-left:.25in;
	text-indent:-.05in;
	font-family:Wingdings;}
@list l173
	{mso-list-id:1913656101;
	mso-list-type:hybrid;
	mso-list-template-ids:995158246 67698703 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l173:level1
	{mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l174
	{mso-list-id:1917670134;
	mso-list-type:hybrid;
	mso-list-template-ids:1813443992 13430468 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l174:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l175
	{mso-list-id:1919364635;
	mso-list-type:hybrid;
	mso-list-template-ids:-199464116 67698689 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l175:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l176
	{mso-list-id:1921719230;
	mso-list-type:simple;
	mso-list-template-ids:-318332020;}
@list l176:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F041;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.55in;
	mso-ansi-font-size:20.0pt;
	font-family:"MS Outlook";
	mso-ansi-font-weight:normal;
	mso-ansi-font-style:normal;}
@list l177
	{mso-list-id:1933081765;
	mso-list-type:hybrid;
	mso-list-template-ids:-1806530394 4250440 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l177:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l178
	{mso-list-id:1939023248;
	mso-list-type:simple;
	mso-list-template-ids:-1362964406;}
@list l178:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F02D;
	mso-level-tab-stop:.25in;
	mso-level-number-position:left;
	margin-left:.25in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l179
	{mso-list-id:1954703213;
	mso-list-type:simple;
	mso-list-template-ids:-1362964406;}
@list l179:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F02D;
	mso-level-tab-stop:.25in;
	mso-level-number-position:left;
	margin-left:.25in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l180
	{mso-list-id:1963610280;
	mso-list-type:hybrid;
	mso-list-template-ids:1628362646 -48751814 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l180:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l181
	{mso-list-id:2010019984;
	mso-list-type:hybrid;
	mso-list-template-ids:-2110493882 -1400107644 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l181:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l181:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l182
	{mso-list-id:2018313676;
	mso-list-type:hybrid;
	mso-list-template-ids:911668304 1595825106 67698703 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l182:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l182:level2
	{mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l183
	{mso-list-id:2044133479;
	mso-list-type:hybrid;
	mso-list-template-ids:1558211522 67698689 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l183:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l184
	{mso-list-id:2046175752;
	mso-list-type:hybrid;
	mso-list-template-ids:-2062620532 67698689 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l184:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l185
	{mso-list-id:2049522067;
	mso-list-type:hybrid;
	mso-list-template-ids:1628362646 1940949618 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l185:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l186
	{mso-list-id:2056613774;
	mso-list-type:hybrid;
	mso-list-template-ids:-345458688 67698689 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l186:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l187
	{mso-list-id:2061442898;
	mso-list-type:hybrid;
	mso-list-template-ids:-2110493882 4250440 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l187:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l187:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l188
	{mso-list-id:2066828051;
	mso-list-type:hybrid;
	mso-list-template-ids:-616421072 -1 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l188:level1
	{mso-level-start-at:0;
	mso-level-number-format:bullet;
	mso-level-text:-;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
@list l189
	{mso-list-id:2072380491;
	mso-list-type:simple;
	mso-list-template-ids:957614692;}
@list l189:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F046;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	margin-left:.3in;
	text-indent:-.3in;
	mso-ansi-font-size:36.0pt;
	font-family:Wingdings;
	mso-ansi-font-weight:normal;
	mso-ansi-font-style:normal;}
@list l190
	{mso-list-id:2082555218;
	mso-list-type:hybrid;
	mso-list-template-ids:1958143720 338838512 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l190:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l191
	{mso-list-id:2091996860;
	mso-list-type:hybrid;
	mso-list-template-ids:1729263460 67698689 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l191:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l192
	{mso-list-id:2101944991;
	mso-list-type:hybrid;
	mso-list-template-ids:-1238462056 -1 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l192:level1
	{mso-level-start-at:0;
	mso-level-number-format:bullet;
	mso-level-text:-;
	mso-level-tab-stop:.8in;
	mso-level-number-position:left;
	margin-left:.8in;
	text-indent:-.3in;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
@list l193
	{mso-list-id:2108572954;
	mso-list-type:hybrid;
	mso-list-template-ids:1628362646 -1611786894 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l193:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l194
	{mso-list-id:2110156084;
	mso-list-type:hybrid;
	mso-list-template-ids:1628362646 -1 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l194:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	text-indent:-.2in;
	font-family:Symbol;}
@list l195
	{mso-list-id:2113742957;
	mso-list-type:hybrid;
	mso-list-template-ids:-2110493882 4250440 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l195:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l195:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l196
	{mso-list-id:2128885180;
	mso-list-type:simple;
	mso-list-template-ids:936180768;}
@list l196:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F036;
	mso-level-tab-stop:.6in;
	mso-level-number-position:left;
	margin-left:.6in;
	text-indent:-.6in;
	mso-ansi-font-size:36.0pt;
	font-family:Wingdings;
	mso-ansi-font-weight:normal;
	mso-ansi-font-style:normal;}
@list l197
	{mso-list-id:2143037178;
	mso-list-type:hybrid;
	mso-list-template-ids:-409534766 338838512 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l197:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.55in;
	mso-level-number-position:left;
	margin-left:.55in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l198
	{mso-list-id:2146924648;
	mso-list-type:hybrid;
	mso-list-template-ids:-2110493882 1595825106 -1 -1 -1 -1 -1 -1 -1 -1;}
@list l198:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:.5in;
	mso-level-number-position:left;
	text-indent:-.25in;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;} 
@list l198:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
ol
	{margin-bottom:0in;}
ul
	{margin-bottom:0in;}
</style>

</head>
<body lang=EN-US link=blue vlink=purple style='tab-interval:.5in'>

	<jsp:include page="ereshelpinclude.jsp" flush="true" />

 
</body>

</html>
