<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%!
String TAreaDisSiteFlag = "TAreaDisSite_ON";
%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_DiseaseSite_Lookup%><%--Disease Site Lookup*****--%></title>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>


<SCRIPT LANGUAGE="JavaScript" src="validations.js"></SCRIPT>
<SCRIPT Language="Javascript1.2">



	

function setSites(formobj) {

  var maxDisSitesStr = "<%=LC.Config_DisSite_MaxCount%>";
  var maxDiseaseSites = parseInt(maxDisSitesStr, 10); 
  var selDiseaseSites = 0;

  var lenSites = 0;
  	from=formobj.from.value;
	datafld=formobj.datafld.value;
	dispfld=formobj.dispfld.value;
	lenSites = parseInt(formobj.len.value);

	var names = "";
	var ids = "";

	if(lenSites > 1) {
		for (var i = 0; i < lenSites; i++) {
			if(formobj.select[i].checked){
				selDiseaseSites++;
				names = names + formobj.siteName[i].value + ";";
				ids = ids + formobj.siteId[i].value + ",";
			}		
		}
	}

	if(lenSites==1) {
  		if(formobj.select.checked){
			selDiseaseSites++;
			names = names + formobj.siteName.value + ";";
			ids = ids + formobj.siteId.value + ",";		
		}		
	}

	if (selDiseaseSites > 0 && maxDiseaseSites > -1 && (selDiseaseSites > maxDiseaseSites)){
		alert('<%=VelosResourceBundle.getMessageString("M_ExceededMaxDisSites", LC.Config_DisSite_MaxCount)%>');
		return false;
	}
	names=decodeString(names);
	names = names.substring(0,names.length-1);
	ids = ids.substring(0,ids.length-1);
	window.opener.document.forms[from].elements[datafld].value=ids;
	window.opener.document.forms[from].elements[dispfld].value=names;
	window.opener.document.forms[from].elements[dispfld].focus();
	self.close();

}





</SCRIPT>







<jsp:useBean id="userB" scope="session" class="com.velos.eres.web.user.UserJB"/>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<%@page import="com.velos.eres.web.study.StudyJB"%>
<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.service.util.LC,com.velos.eres.service.util.MC,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.*"%>





<% response.setHeader("Cache-Control", "max-age=1, must-revalidate");%>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">



</head>



<%

int ienet = 2;



String agent1 = request.getHeader("USER-AGENT");

   if (agent1 != null && agent1.indexOf("MSIE") != -1) 

     ienet = 0; //IE

    else

	ienet = 1;

	if(ienet == 0) {	

%>

<body style="overflow:scroll;">

<%

	} else {

%>

<body>

<%

	}

%>

<%HttpSession tSession = request.getSession(true);

  if (sessionmaint.isValidSession(tSession))

{
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<jsp:include page="include.jsp" flush="true"/>
<%
	String from=request.getParameter("from") ;
	String datafld=request.getParameter("datafld") ;
	String dispfld=request.getParameter("dispfld") ;
	
	int selectedSites=0;
	String names = "";
	String ids = "";
	ArrayList cId=null,cName=null, cHide = null;
	names = StringUtil.decodeString(request.getParameter("names"));
	names=(names==null)?"":(names);
	ids = request.getParameter("ids");
	while(names.indexOf('~') != -1){
	  	names = names.replace('~',' ');
	}

	String tArea = request.getParameter("tArea");
	String rownum=request.getParameter("rownum");
	String accountId = (String) tSession.getValue("accountId");
	String userJobType = "";
	String dJobType = "";
	String dAccSites ="";
	CodeDao cd = new CodeDao();

	if (TAreaDisSiteFlag.equals(LC.Config_TAreaDisSite_Switch)){
		if (!StringUtil.isEmpty(tArea)){
			CodeDao tAreaCD = new CodeDao();
			cd.getCodeValuesFilterCustom1("disease_site", tAreaCD.getCodeSubtype(StringUtil.stringToNum(tArea)));
		}
	} else {
		cd.getCodeValues("disease_site");
	}
	cId=cd.getCId();
	cName=cd.getCDesc();
	cHide = cd.getCodeHide();

	String name="",id="", hiddenCode = "";
	//StringTokenizer namesSt=new StringTokenizer(names,";");
	StringTokenizer idsSt=new StringTokenizer(ids,",");
	selectedSites=idsSt.countTokens();
	
		

%>





<DIV class="popDefault"> 
<form name="selectSite" method="POST" id="selectSiteFrm" onSubmit="setSites(document.selectSite)">
<% if (selectedSites>0){%>
<table><tr><td><p class = "sectionHeadings" ><%=MC.M_Already_SelectedSites%><%--Already Selected Sites*****--%>:</p></td></tr></table>
<table width="100%">
		
      <tr class = "popupHeader"> 
         <th width="10%"><%=LC.L_Select%><%--Select*****--%></th>
         <th width="90%"><%=LC.L_Disease_Site%><%--Disease Site*****--%></th>
      </tr>
   <%for (int i=0;i<selectedSites;i++){
   	id=idsSt.nextToken();
   	name=cd.getCodeDescription(EJBUtil.stringToNum(id));
	//name=namesSt.nextToken();
   	if ((i%2)==0) {
	%>
      <tr class="browserEvenRow"> 
  <%
		}
	else {
  %>
      <tr class="browserOddRow"> 
   <%
		}
	%>
	<td width="10%"><input type="checkbox"  name="select" checked></td>
	<td width="90%"><%=name%></td>
	<input type="hidden" name="siteId" value="<%=id%>">
	<input type="hidden" name="siteName" value="<%=StringUtil.encodeString(name)%>">
	 </tr>
	 
<%}%>
</table>
<%}%>
	 <br><br>

	<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="N"/>
		<jsp:param name="formID" value="selectSiteFrm"/>
		<jsp:param name="showDiscard" value="Y"/>
	</jsp:include>

 <br>
<%if (!"-1".equals(LC.Config_DisSite_MaxCount)){%>
	<table><tr><td><P class="defComments"><%=VelosResourceBundle.getMessageString("M_MaxSel_Allwd", ""+LC.Config_DisSite_MaxCount)%><%--Maximum {0} selection(s) allowed.*****--%></P></td></tr></table>	
<%} %>
<br>
<table><tr><td><P class="defComments"> <%=MC.M_SelFrom_FlwList%><%--Please select from the following list*****--%>:</P></td></tr></table>	
	
 <table width="100%">
      <tr class = "popupHeader"> 
         <th width="10%"><%=LC.L_Select%><%--Select*****--%></th>
         <th width="90%"><%=LC.L_Disease_Site%><%--Disease Site*****--%></th>
      </tr>
      <input type="hidden" name="from" value="<%=from%>">
      <input type="hidden" name="datafld" value="<%=datafld%>">
      <input type="hidden" name="dispfld" value="<%=dispfld%>">
 <%
 int displayedRows = 0;
 %>
 <%
 String commaDelIds = (","+ids+",");
 for(int i=0;i<cId.size();i++){
  	id=StringUtil.integerToString((Integer)cId.get(i));
	name=(String)cName.get(i);
	hiddenCode = (String)cHide.get(i);
	hiddenCode = StringUtil.isEmpty(hiddenCode)? "N": hiddenCode;

	if ("Y".equals(hiddenCode)) continue;
	if (commaDelIds.contains(','+id +',')) continue;
%>
  <%if ((displayedRows%2)==0) {%>
      <tr class="browserEvenRow"> 
  <%} else {%>
      <tr class="browserOddRow"> 
  <%}%>
<td width="10%"><input type="checkbox" name="select"></td><td width="90%"><%=name%></td>
<input type="hidden" name="siteId" value="<%=id%>">
<input type="hidden" name="siteName" value="<%=StringUtil.encodeString(name)%>">

 </tr>
 <%displayedRows++;%>
 <%}%>
       <input type="hidden" name="len" value="<%=displayedRows+selectedSites%>">
	</table>
	

<jsp:include page="submitBar.jsp" flush="true"> 
	<jsp:param name="displayESign" value="N"/>
	<jsp:param name="formID" value="selectSiteFrm"/>
	<jsp:param name="showDiscard" value="Y"/>
</jsp:include>

</form>
	<%
 }//end of if body for session

else{

%>

  <jsp:include page="timeout_childwindow.jsp" flush="true"/>

  <%

}


%>

</div>

<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>

</body>
</html>

