<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<style type="text/css">
.yui-dt-liner { white-space:normal; } 
.yui-skin-sam .yui-dt td.up { background-color: #efe; } 
.yui-skin-sam .yui-dt td.down { background-color: #fee; }
.table-basic { cellspacing:0px;border-spacing:0px;border-style:groove;border-bottom-width:thin;border-top-width:thin;border-left-width:thin;border-right-width:thin; }
.table-basic th { cellspacing:0px;border-spacing:0px;border-style:groove;border-bottom-width:thin;border-top-width:thin;border-left-width:thin;border-right-width:thin; }
.table-basic td { cellspacing:0px;border-spacing:0px;border-style:groove;border-bottom-width:thin;border-top-width:thin;border-left-width:thin;border-right-width:thin; }
</style>
 <script type="text/javascript" src="js/yui/build/yahoo-dom-event/yahoo-dom-event.js"></script>
 <style>
 
 table.viewallcass th {
	white-space: normal;
	font-weight: normal;
	font-size: 92%;
	border-style: none solid none none;
	padding-right: 4px;
	padding-top: 4px;
	height : 40px;
	word-wrap: break-word !important;         /* All browsers since IE 5.5+ */
    overflow-wrap: break-word !important;     /* Renamed property in CSS3 draft spec */
}
#firstTd{
margin-right: 10px;
font-size: 100%;
font-weight: normal;
vertical-align: middle;
}
 
  table.viewallcass td  {
		border-style: none solid none none;
}

th p.std{
	width: 128px ;
	
}
td p.std{
	width: 128px ;	
	word-wrap: break-word !important;         
    overflow-wrap: break-word !important;    
}  

th  p.setwidth {
	width: 213px  !important;	
}

td p.setwidth {
	overflow: hidden !important;
	width: 213px  !important;
    word-wrap: break-word !important;         /* All browsers since IE 5.5+ */
    overflow-wrap: break-word !important;     /* Renamed property in CSS3 draft spec */
} 	 
 
</style>
<link type="text/css" href="js/yui/build/datatable/assets/skins/sam/datatable.css" />
<%@page import="com.velos.eres.web.objectSettings.ObjectSettingsCache,com.velos.eres.web.objectSettings.ObjectSettings"%>
<%@page import="com.velos.eres.web.grpRights.GrpRightsJB"%>
<%@page import="com.velos.eres.business.ulink.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.EJBUtil"%>
<%@page import="com.velos.esch.business.common.SchCodeDao"%>
<%@page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*" %>
<%@page import="java.text.DecimalFormat" %>
<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="commonB" scope="request" class="com.velos.eres.web.common.CommonJB"/>
 <%
ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
ArrayList tabList = null;
String titleStr = LC.L_Calendar;/*String titleStr = "Calendar";*****/
if (sessionmaint.isValidSession(request.getSession(true)))
{
    String accId = (String) request.getSession(true).getValue("accountId");
    
    ArrayList tempList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "protocol_tab");
    for (int iX=0; iX<tempList.size(); iX++) {
        ObjectSettings settings = (ObjectSettings) tempList.get(iX);
        if ("7".equals(settings.getObjSubType())) {
            if (settings.getObjDispTxt() != null) {
            	Object[] arguments = {titleStr,settings.getObjDispTxt()};
            	titleStr = VelosResourceBundle.getLabelString("L_Gtgt",arguments);
            	/*titleStr +=  " >> " + settings.getObjDispTxt();*****/
            }
            break;
        } 
    }
    tempList = null;
}
String src= StringUtil.htmlEncodeXss(request.getParameter("srcmenu"));
%>
<title><%=titleStr %></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META CONTENT="no-cache" HTTP-EQUIV="PRAGMA">
<META CONTENT="private" HTTP-EQUIV="CACHE-CONTROL">
<META CONTENT="no-store" HTTP-EQUIV="CACHE-CONTROL">
<META CONTENT="must-revalidate" HTTP-EQUIV="CACHE-CONTROL">
<META CONTENT="post-check=0,pre-check=0" HTTP-EQUIV="CACHE-CONTROL">
<META CONTENT="-1" HTTP-EQUIV="Expires">
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<jsp:include page="ui-include.jsp" flush="true"/>
<script  type="text/javascript" src="js/jquery/json2.min.js"></script>
</head>

<% 
HttpSession tSession = request.getSession(true); 
String vclick1=request.getParameter("vclick");
vclick1 = (vclick1==null)?"1":vclick1;
String accountId = "";
String studyId = "";
String grpId = "";
String usrId = "";
if (sessionmaint.isValidSession(tSession)) {
	accountId = (String) tSession.getValue("accountId");
	studyId = (String) tSession.getValue("studyId");
	grpId = (String) tSession.getValue("defUserGroup");
	usrId = (String) tSession.getValue("userId");
	String sessId = tSession.getId();
	if (sessId.length()>8) { sessId = sessId.substring(0,8); }
	sessId = Security.encrypt(sessId);
	char[] chs = sessId.toCharArray();
	StringBuffer sb = new StringBuffer();
	DecimalFormat df = new DecimalFormat("000");
	for (int iX=0; iX<chs.length; iX++) {
	    sb.append(df.format((int)chs[iX]));
	}
	String keySessId = sb.toString();

	if ("N".equals(request.getParameter("mode"))) {
		studyId = "";
		tSession.removeAttribute("studyId");
	}
%>
<body class="yui-skin-sam yui-dt yui-dt-liner" style="overflow:hidden;">
<div id="overDiv" style="position:absolute;visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
<%
	String from = "version";
	String tab = request.getParameter("selectedTab");
    String calassoc = StringUtil.trueValue(request.getParameter("calassoc"));
	String studyIdForTabs = request.getParameter("studyId");
    String includeTabsJsp = "irbnewtabs.jsp";
    int pageRight = 0;
    int finDetRight = 0;
	String calledFrom = request.getParameter("calledFrom");
	if (calledFrom.equals("S")) {
		StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");
		if ((stdRights.getFtrRights().size()) == 0){
			pageRight= 0;
			finDetRight = pageRight;
		} else {
			pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));
			finDetRight=Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFIN"));
		}
	} else if ((calledFrom.equals("P")) || (calledFrom.equals("L"))) {
		GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));
		finDetRight = pageRight;
	}
	%>
	<DIV class="BrowserTopn" id = "div1">
		<jsp:include page="protocoltabs.jsp" flush="true">
		<jsp:param name="calassoc" value="<%=StringUtil.htmlEncodeXss(calassoc)%>" />
		<jsp:param name="selectedTab" value="<%=StringUtil.htmlEncodeXss(tab)%>" />
		</jsp:include>
	</DIV>
	<DIV class="BrowserBotN BrowserBotN_CL_1" id = "div2" style="overflow: auto;">
	<%
	if (pageRight > 0) {

	} // end of pageRight > 0
	String protocolId = request.getParameter("protocolId");
	if(protocolId == null || protocolId == "" || protocolId.equals("null") || protocolId.equals("")) {
%>
	<jsp:include page="calDoesNotExist.jsp" flush="true"/>
<%
	} else {
	String calStatus = null;
	SchCodeDao scho = new SchCodeDao();
	String calStatusPk = "";
	String calStatDesc = "";
	if (pageRight > 0) {
	    String tableName = null;
 		if("L".equals(calledFrom) || "P".equals(calledFrom)){
	   		tableName ="event_def";
			eventdefB.setEvent_id(EJBUtil.stringToNum(protocolId));
			eventdefB.getEventdefDetails();
			//KM-#DFin9
			calStatusPk = eventdefB.getStatCode();
			calStatus = scho.getCodeSubtype(EJBUtil.stringToNum(calStatusPk)).trim();
			calStatDesc = scho.getCodeDescription(EJBUtil.stringToNum(calStatusPk));

	   	} else {
	   		tableName ="event_assoc";
			eventassocB.setEvent_id(EJBUtil.stringToNum(protocolId));
			eventassocB.getEventAssocDetails();
			//KM-#DFin9
			calStatusPk = eventassocB.getStatCode();
			calStatus = scho.getCodeSubtype(EJBUtil.stringToNum(calStatusPk)).trim();
			calStatDesc = scho.getCodeDescription(EJBUtil.stringToNum(calStatusPk));

	   	}
		String duration = StringUtil.trueValue(request.getParameter("duration"));
		StringBuffer urlParamSB = new StringBuffer();
		urlParamSB.append("protocolId=").append(StringUtil.htmlEncodeXss(protocolId))
		.append("&calledFrom=").append(StringUtil.htmlEncodeXss(calledFrom))
		.append("&calledfrom=").append(StringUtil.htmlEncodeXss(calledFrom))
		.append("&duration=").append(StringUtil.htmlEncodeXss(duration))
		.append("&calassoc=").append(StringUtil.htmlEncodeXss(calassoc))
		.append("&calstatus=").append(StringUtil.htmlEncodeXss(request.getParameter("calStatus")))
		.append("&mode=").append(StringUtil.htmlEncodeXss(request.getParameter("mode")))
		.append("&calProtocolId=").append(StringUtil.htmlEncodeXss(protocolId))
		.append("&tableName=").append(tableName)
		.append("&pr=").append(pageRight)
		.append("&fin=").append(finDetRight);
		;
		
		SchCodeDao schDao = new SchCodeDao();
		schDao.getCodeValues("coverage_type");
		ArrayList covSubTypeList = schDao.getCSubType();
		ArrayList covDescList = schDao.getCDesc();
		ArrayList<String> covCodeHide = schDao.getCodeHide();
		StringBuffer sb1 = new StringBuffer();
		for (int iX=0; iX<covSubTypeList.size(); iX++) {
		    String covSub1 = ((String)covSubTypeList.get(iX)).replaceAll("'", "\\\\'");
		    String covDesc1 = ((String)covDescList.get(iX)).replaceAll("'", "\\\\'");
		    String covIsHidden = covCodeHide.get(iX);
		    if(StringUtil.isEmpty(covIsHidden))covIsHidden="N";
			if (covSub1 == null || covDesc1 == null) { continue; }
			if(covIsHidden.equalsIgnoreCase("Y")){
				sb1.append("<b>").append(covSub1).append("</b>").append("=").append(covDesc1).append(" ["+LC.L_Hidden+"]").append("<br/>");
			}else{
				sb1.append("<b>").append(covSub1).append("</b>").append("=").append(covDesc1).append("<br/>");
			}
		}
		String covTypeLegend = sb1.toString();
%>
<script>
var screenHeight = screen.height;
var screenWidth = screen.width;
var topScroll = 0;
var leftScroll = 0;
function searchClear(){
	$j("#eventName").val('');
	reloadCoverageGrid();
}
var viewAllFlag=1;
var totalEventCount=<%=LC.Config_CovAnal_Event_records%>;
var totalVisitCount=<%=LC.Config_CovAnal_Visit_records%>;
var initEventSize=1;
var limitEventSize=totalEventCount;
var initVisitSize=1;
var limitVisitSize=totalVisitCount;
var oldEventName;
function reloadGridAsFetch(eventType){
var totalEventRecords = $j("#totalEventRecords").val();
var totalVisitRecords = $j("#totalVisitRecords").val();
	if(eventType=='previousEvents' && initEventSize>totalEventCount){
	initEventSize=initEventSize-totalEventCount;
	limitEventSize=limitEventSize-totalEventCount;
	reloadCoverageGrid();
	}else if(eventType=='nextEvents' && limitEventSize<totalEventRecords){
	initEventSize=initEventSize+totalEventCount;
	limitEventSize=limitEventSize+totalEventCount;
	reloadCoverageGrid();
	}else if(eventType=='firstEvents' && initEventSize>totalEventCount){
	initEventSize=1;
	limitEventSize=totalEventCount;
	reloadCoverageGrid();
	}else if(eventType=='lastEvents' && limitEventSize<totalEventRecords){
	var rem = totalEventRecords%totalEventCount;
	if(rem==0)
	rem= totalEventCount;
	initEventSize = totalEventRecords-rem+1;
	limitEventSize = totalEventRecords-rem+totalEventCount;
	reloadCoverageGrid();
	}else if(eventType=='previousVisits' && limitVisitSize>totalVisitCount){
	initVisitSize=initVisitSize-totalVisitCount;
	limitVisitSize=limitVisitSize-totalVisitCount;
	reloadCoverageGrid();
	}else if(eventType=='nextVisits' && limitVisitSize<totalVisitRecords){
	initVisitSize=initVisitSize+totalVisitCount;
	limitVisitSize=limitVisitSize+totalVisitCount;
	reloadCoverageGrid();
	}else if(eventType=='firstVisits' && initVisitSize>totalVisitCount){
	initVisitSize=1;
	limitVisitSize=totalVisitCount;
	reloadCoverageGrid();
	}else if(eventType=='lastVisits' && limitVisitSize<totalVisitRecords){
	var rem = totalVisitRecords%totalVisitCount;
	if(rem==0)
	rem= totalVisitCount;
	initVisitSize = totalVisitRecords-rem+1;
	limitVisitSize = totalVisitRecords-rem+totalVisitCount;
	reloadCoverageGrid();
	}
}

function fnOnceEnterKeyPress(e) {
	var evt = (e) || window.event;
    if (!evt) { return 0; }
	try {
        var code = evt.charCode || evt.keyCode;
        if (code == 13 || code == 10) {
            var thisTimeSubmitted = new Date();
            if (!lastTimeSubmitted) { lastTimeSubmitted = 0; }
            if (!thisTimeSubmitted) { return 0; }
            if ( thisTimeSubmitted - lastTimeSubmitted <= 1000 ) {
                return -1;
            }
            lastTimeSubmitted = thisTimeSubmitted;
            return 1;
        }
	} catch(e) {}
	return 0;
}
var vclick="<%=vclick1%>";;
function reloadGridAsFetchViewAll(){
	togglePaginationHide();
	reloadCoverageGrid();
}
function togglePaginationHide(){
	if(vclick==0){
		document.getElementById("viewAllVisit").innerHTML="View By Pagination";
	$j('#visitsSpan').css('display', 'none');
	$j('#eventsSpan').css('display', 'none');
	}
	if(vclick==1){
		$j('#visitsSpan').css('display', 'block');
		$j('#eventsSpan').css('display', 'block');
		document.getElementById("viewAllVisit").innerHTML="View All";
	}
	if(vclick==0){
		vclick=1;
	}else{
		vclick=0;
	}
	
	
}
function reloadCoverageGrid() {
	var urlParamSB = "<%=urlParamSB.toString()%>";
	var eventName=$j("#eventName").val();
	if(oldEventName!=eventName){
	initEventSize=1;	
	limitEventSize=totalEventCount;
	oldEventName = eventName;
	}
	if(viewAllFlag){
		if(vclick==1){
        urlParamSB=urlParamSB+"&viewAllFlag=1&eventName="+eventName;
        reloadCoverageTableGrid(urlParamSB);
        $j(".vdiv").scrollLeft(leftScroll);
	    $j(".vdiv").scrollTop(topScroll);
	    myscroll();
        return false;
		}
		else{
			
		urlParamSB= urlParamSB+"&initEventSize="+initEventSize+"&limitEventSize="+limitEventSize+"&initVisitSize="+initVisitSize+"&limitVisitSize="+limitVisitSize+"&eventName="+eventName;
		}
		
	}
	YAHOO.example.CoverageGrid = function() {
		var args = {
			urlParams: urlParamSB,
			dataTable: "coveragegrid"
		};
		myCoverageGrid = new VELOS.coverageGrid('fetchCoverageJSON.jsp', args);
		myCoverageGrid.startRequest();
    }();
}

function reloadCoverageTableGrid(urlParam) {
	//var L_Event_Name=L_Event_Name;
	 showPanel();
	   var urlParamSB=urlParam;
	    $j.ajax({
	        url: 'fetchCoverageJSON.jsp',
	        type: 'POST',
	        async: false,
	        dataType: 'json',
	        data: urlParamSB,
	        cache: false,
	        success: function (data) {
	    	visitIdList = [];
	    	colArray=data.colArray;
			dataArray=data.dataArray;
			calName = data.protocolName;
			covSubTypes = data.covSubTypes;
			covPks = data.covPks;
			eventLabelArray = [];
			eventCovNotesArray = [];
			eventCovArray = [];
			visitIdArray = [];
			visitLabelArray = [];
			var myDisplacements = data.displacements;
			visitDisplacementArray = [];
	    	var createTab='';
	    	var brow='mozilla';
	    	var vcount='0';
	    	jQuery.each(jQuery.browser, function(i, val) {
	    	if(val==true){

	    	brow=i.toString();
	    	}
	    	});
	    	var myCoverageOption="";
	    	myCoverageOption=myCoverageOption+"<option value=\"\" onclick=\"VELOS.coverageGrid.selectDefaultCoverageType(tempId);\">"+"</option>";
			for (var iX=0; iX<covSubTypes.length; iX++) {
				 var keySub = covSubTypes[iX].key;
				var keyPk = covPks[iX].key;
				myCoverageOption=myCoverageOption+"<option value=\""+keyPk+"\">"+keySub+"</option>";
			}
			covOptionsStr = myCoverageOption;
			coverageType=myCoverageOption;
			createTab="<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" ><tr><th id=\"firstTd\" style=\"background: #D8D8DA url(\"../../../../assets/skins/sam/sprite.png\") repeat-x scroll 0px 0px;background-image: url(\"../../images/skin_default/bg-tbheading.jpg\");background-repeat: repeat-x;\">"+colArray[0].label+"</th><th style=\"background: #D8D8DA url(\"../../../../assets/skins/sam/sprite.png\") repeat-x scroll 0px 0px;background-image: url(\"../../images/skin_default/bg-tbheading.jpg\");background-repeat: repeat-x;\">";
			if(brow=='msie'){
				if(screenWidth>1280 || screenHeight>1024){	 
				createTab=createTab + "<div id='divHeader'  class='hdiv'  style='overflow:hidden;WIDTH: 1090px;' align='left'><TABLE  class='viewallcass'><tr>";

				}
				else{
					createTab=createTab + "<div id='divHeader'  class='hdiv'  style='overflow:hidden;width:1005px;' align='left'><TABLE  class='viewallcass'><tr>";
					}
				}

				else{
					if(screenWidth>1280 || screenHeight>1024) {			
						createTab=createTab + "<div id='divHeader'  class='hdiv'  style='overflow:hidden;width:1060px;' align='left'><TABLE  class='viewallcass'><tr>";
					}
					else{					
						createTab=createTab + "<div id='divHeader'  class='hdiv'  style='overflow:hidden;width:770.4px;' align='left'><TABLE  class='viewallcass'><tr>";
						}
				}
		    if (colArray.length > 0) {
		    var iZ=0;
		       for (var iX=0; iX<colArray.length; iX++) {
		    	    var key = colArray[iX].key;
		    	    var hideExport = 'false';
		    	    var hideexp = JSON.stringify(colArray[iX]);
		            if(hideexp.search("hideExport")>0)
		            	hideExport = colArray[iX].hideExport;
		            if (!colArray[iX].label) { continue; }
		            if(key=='eventId' || key == 'event'){continue;}
		            visitLabelArray[colArray[iX].key] = colArray[iX].label;
		            if(hideExport=='true'){continue;}
		            if (colArray[iX].key && colArray[iX].key.match(/^v[0-9]+$/)) {
						visitIdArray.push(colArray[iX].key);
						if (myDisplacements) {
							visitDisplacementArray[myDisplacements[iZ].key] = myDisplacements[iZ].value;
							iZ++;}
					}
		            if(iX ==0){}
		            else{
		            	vcount++;
		            	createTab=createTab+'<th style=\"background: #D8D8DA url(\"../../../../assets/skins/sam/sprite.png\") repeat-x scroll 0px 0px;background-image: url(\"../../images/skin_default/bg-tbheading.jpg\");background-repeat: repeat-x;\"><div class="tableHeader"><p class="std">'+colArray[iX].label;
		            	}
		            if (key.match(/^v[0-9]+$/)) {
		            	var visit=key.substring(1);
		            	var onChangeMethod="onChange=\""+"VELOS.coverageGrid.htmlSaveCoverageTypeByVisit("+visit+");"+"\"";
			            
		            	var covSelect="<select id=\""+"all_"+key+"\""+" name=\"all_"+key+"\""+onChangeMethod+">"+myCoverageOption.replace('tempId',"'v"+visit+"'")+"</select>";
		            
		            	createTab=createTab+covSelect;
		            	visitIdList.push(key);
		                     }
		            
		        }
		       createTab=createTab+'</p></div></th>';
		    }
		    createTab=createTab+'</tr></table></div></td></tr>';
		    createTab=createTab+"<tr><td valign=\"top\"><div id=\"firstcol\" style='overflow:hidden;width:220px;max-height:350px;px;float:left' class='cdiv'><table>";			 
			if(dataArray.length==0)
			{
				createTab=createTab+"";
			}
			else
			{
				createTab=createTab+"replaceIt";
			}
			createTab=createTab+"</table></div></td><td valign=\"top\">";	
			var replaceIt=''; 
		    var noteCounter = 0;
		    var notesList = [];
		    var countvisit=vcount*20;
				    if (dataArray.length > 0) {
				    	if(brow=='msie'){
					    	if(screenWidth>1280 || screenHeight>1024){	 
					    	createTab=createTab+"<div id='table_div'  class='vdiv' onscroll='return  myscroll();' style='overflow:scroll;overflow-y:hidden;WIDTH: 1090px;' align='left'><TABLE cellspacing='0px'  class='viewallcass'>";
					    	}
					    	else{
					    		createTab=createTab+"<div id='table_div'  class='vdiv' onscroll='return  myscroll();' style='overflow:scroll;overflow-y:hidden;max-height:370px;width:1005px;' align='left'><TABLE cellspacing='0px'  class='viewallcass'>";
						    	}
					    }   
						    else{
						    	if(screenWidth>1280 || screenHeight>1024) {			    	
					    	createTab=createTab+"<div id='table_div'  class='vdiv' onscroll='return  myscroll();' style='overflow:scroll;max-height:370px;width:1080px;' align='left'><TABLE cellspacing='0px'  class='viewallcass'>";
					    	}       
					    	else{		    		
					    		createTab=createTab+"<div id='table_div'  class='vdiv' onscroll='return  myscroll();' style='overflow:scroll;max-height:370px;width:790.4px;' align='left'><TABLE cellspacing='0px'  class='viewallcass'>";
						    	}
							    }
						    
		    for (var iX=0; iX<dataArray.length; iX++) {
		    	var checkedVisit=JSON.stringify(dataArray[iX]);
				var eid = dataArray[iX].eventId;
				eventLabelArray['e'+eid] = dataArray[iX].event;
				eventCovNotesArray['e'+eid] = dataArray[iX].eventCovNotes;
				eventCovArray['e'+eid] = "&nbsp;";
		        if ((iX%2)==0) {
			        if(iX==0)
		        		createTab=createTab+"<tr id='yui-rec"+iX+"' onclick = 'VELOS.coverageGrid.rowDialog("+dataArray[iX].eventId+")' class=\"browserEvenRow\">";
		        	else
		        		createTab=createTab+"<tr id='yui-rec"+iX+"' onclick = 'VELOS.coverageGrid.rowDialog("+dataArray[iX].eventId+")' class=\"browserEvenRow\">";
		      		}
		      		else{
		      			createTab=createTab+"<tr id='yui-rec"+iX+"' onclick = 'VELOS.coverageGrid.rowDialog("+ dataArray[iX].eventId+")' class=\"browserOddRow\">";
		 	}
		        
			var onChangeMethod="onChange=\""+"VELOS.coverageGrid.htmlSaveCoverageTypeByEvent("+dataArray[iX].eventId+");"+"\"";
		        var covSelect="<span style=\""+"float:right\">"+"<select onmousedown=\"VELOS.coverageGrid.getEventDown()\" id=\""+"all_"+dataArray[iX].eventId+"\""+" name=\"all_"+dataArray[iX].eventId+"\""+onChangeMethod+">"+myCoverageOption.replace('tempId',"'e"+dataArray[iX].eventId+"'")+"</select></span>";
		        // Remove the mouse over functionality according to the bug no #23462
          	var length1=(dataArray[iX].event).length;
          	var eventOverlibParameter='';
			if(length1>500 && length1<=1000)
				eventOverlibParameter=',WRAP, BORDER,2, CENTER,ABOVE,OFFSETY,30,STATUS,\'Draggable with overflow scrollbar, caption and Close link\',';
			else if(length1>1000 && length1<=2000)
				eventOverlibParameter=',WRAP, BORDER,2, CENTER,ABOVE,OFFSETY,50,STATUS,\'Draggable with overflow scrollbar, caption and Close link\',';
			else if(length1>2000)
				eventOverlibParameter=',WRAP, BORDER,2, CENTER,ABOVE,OFFSETY,140,STATUS,\'Draggable with overflow scrollbar, caption and Close link\',';
			else 
				eventOverlibParameter=',ABOVE,';
		        if ((iX%2)==0)
			    {
		        	//var length1=(dataArray[iX].event).length;
					var str=htmlEncode(dataArray[iX].event);
					var dollarIndex=str.lastIndexOf("$");
					if(dollarIndex==(length1-1))
					{
						str=str+" ";											
					}
					if(length1>50)
					{
						var substring1=(dataArray[iX].event).substring(0,50);
						replaceIt = replaceIt +'<tr id="yui-rect'+iX+'" class=\"browserEvenRow\"><td class="tableFirstCol"><p class="setwidth">'+substring1+covSelect+'<span onmouseout="return nd()" onmouseover="return overlib(\''+str+'\''+eventOverlibParameter+'CAPTION,\''+L_Event_Name+'\');">...</span></p></td></tr>';			        
		        		//replaceIt = replaceIt +'<tr id="yui-rect'+iX+'" class=\"browserEvenRow\"><td class="tableFirstCol"><p class="setwidth">'+dataArray[iX].event+covSelect+'nnnnn</p></span></td></tr>';
					}
					else
					{
						replaceIt = replaceIt +'<tr id="yui-rect'+iX+'" class=\"browserEvenRow\"><td class="tableFirstCol"><p class="setwidth"><span onmouseout="return nd()" onmouseover="return overlib(\''+str+'\',CAPTION,\''+L_Event_Name+'\');">'+dataArray[iX].event+covSelect+'</span></p></td></tr>';
					}
				}
		        else
		        {
		        	//var length1=(dataArray[iX].event).length;
					var str=htmlEncode(dataArray[iX].event);
					var dollarIndex=str.lastIndexOf("$");
					if(dollarIndex==(length1-1))
					{
						str=str+" ";													
					}
					if(length1>50)
					{
						var substring1=(dataArray[iX].event).substring(0,50);
						replaceIt = replaceIt +'<tr id="yui-rect'+iX+'" class=\"browserOddRow\"><td class="tableFirstCol"><p class="setwidth">'+substring1+covSelect+'<span onmouseout="return nd()" onmouseover="return overlib(\''+str+'\''+eventOverlibParameter+'CAPTION,\''+L_Event_Name+'\');">...</span></p></td></tr>';
					}
					else
					{
						replaceIt = replaceIt +'<tr id="yui-rect'+iX+'" class=\"browserOddRow\"><td class="tableFirstCol"><p class="setwidth"><span onmouseout="return nd()" onmouseover="return overlib(\''+str+'\',CAPTION,\''+L_Event_Name+'\');">'+dataArray[iX].event+covSelect+'</span></p></td></tr>';
		        	}
		        	//replaceIt = replaceIt +'<tr id="yui-rect'+iX+'" class=\"browserOddRow\"><td class="tableFirstCol"><p class="setwidth">'+dataArray[iX].event+covSelect+'</p></span></td></tr>';  
		        }
		        var length2=(dataArray[iX].eventCPT).length;
		        var cptOverlibParameter='';
				if(length2>500 && length2<=1000)
					cptOverlibParameter=',WRAP, BORDER,2, CENTER,ABOVE,OFFSETY,50,STATUS,\'Draggable with overflow scrollbar, caption and Close link\',';
				else if(length2>1000 && length2<=2000)
					cptOverlibParameter=',WRAP, BORDER,2, CENTER,ABOVE,OFFSETY,100,STATUS,\'Draggable with overflow scrollbar, caption and Close link\',';
				else if(length2>2000)
					cptOverlibParameter=',WRAP, BORDER,2, CENTER,ABOVE,OFFSETY,200,STATUS,\'Draggable with overflow scrollbar, caption and Close link\',';
				else 
					cptOverlibParameter=',ABOVE,';
		        
				var str=htmlEncode(dataArray[iX].eventCPT);
				if(length2>50)
				{
					var substring2=(dataArray[iX].eventCPT).substring(0,50);
					createTab=createTab+'<td><p class="std">';
		            createTab=createTab+substring2+'<span onmouseout="return nd()" onmouseover="return overlib(\''+str+'\''+cptOverlibParameter+'CAPTION,\''+L_Cpt_Code+'\');">...</span></p></td>';			
				}
				else
				{
					createTab=createTab+'<td><p class="std">';
		            createTab=createTab+'<span onmouseout="return nd()" onmouseover="return overlib(\''+str+'\',CAPTION,\''+L_Cpt_Code+'\');">'+dataArray[iX].eventCPT+'</span></p></td>';
				}
		        
	            //createTab=createTab+"<td><p class='std'>";
	            //createTab=createTab+dataArray[iX].eventCPT+"</p></td>";
	         // Remove the mouse over functionality according to the bug no #23462
	            for (var iV=0; iV<visitIdList.length; iV++) {
	            	var visidId = visitIdList[iV];
	            	createTab=createTab+"<td><p class='std'>";
	            	//createTab=createTab+"";
			        var cellData = '';
			        cellData = dataArray[iX][visidId]; 
			        if(cellData==undefined)
			        	cellData = '';
			        if (cellData != '') {
			            if (cellData.search("<b>X</b>")>0) { cellData = "<b>X</b> "+cellData;}
			            createTab=createTab+cellData;
			        }
			        var notesData = '';
			        notesData = dataArray[iX]["notes_"+visidId]; 
			        if(notesData==undefined)
			        	notesData = '';
			        if (notesData != '' && notesData.length > 0) {
			        	createTab=createTab+" <img class=\"headerImage\" align=\"bottom\" src=\"images/clip.jpg\"/><sup>"+(++noteCounter)+"</sup>";
			            notesList.push(notesData);
			        }
			        if (cellData == ''  && (notesData == '' || notesData.trim().length == 0)) {
			        	createTab=createTab+"&nbsp;";
			        }
			        createTab=createTab+"</span></p></td>";
			    }
			    createTab=createTab+"<td style='border-right: 1px solid gray;'><p class='std'>"+dataArray[iX].eventCovNotes+"</p></td>";
			    createTab=createTab+"</tr>";
		    }
		    createTab = createTab.replace("replaceIt",replaceIt);
		    createTab=createTab+ '</table></div></td></tr></table>';
		    if (notesList.length > 0) {
		    	createTab=createTab+"<br/><Table border=\"0%\">";
		    	createTab=createTab+"<tr><td colspan=\"2\"><img align=\"bottom\" src=\"images/clip.jpg\" width=\"17\" height=\"15\"/></td></tr>";
		        for (var iX=0; iX<notesList.length; iX++) {
		        	createTab=createTab+"<tr valign='top'><td align='left' colspan=\"2\">"+(iX+1)+" &ndash; ";
		        	createTab=createTab+notesList[iX]+"</td></tr>";
		        }
		        createTab=createTab+"</table>";
		    }
		    }
		    else{
		    	
		    	if(countvisit>6)
		    		{
		    		if(brow=='msie'){
				    	if(screenWidth>1280 || screenHeight>1024){	 
				    	createTab=createTab+"<div id='table_div'  class='vdiv' onscroll='return  myscroll();' style='overflow:scroll;overflow-y:hidden;WIDTH: 1090px;' align='left'><TABLE cellspacing='0px'  class='viewallcass' width='"+countvisit+"%'>";
				    	}
				    	else{
				    		createTab=createTab+"<div id='table_div'  class='vdiv' onscroll='return  myscroll();' style='overflow:scroll;overflow-y:hidden;max-height:370px;width:1005px;' align='left'><TABLE cellspacing='0px'  class='viewallcass' width='"+countvisit+"%'>";
					    	}
				    }   
					    else{
					    	if(screenWidth>1280 || screenHeight>1024) {			    	
				    	createTab=createTab+"<div id='table_div'  class='vdiv' onscroll='return  myscroll();' style='overflow:scroll;max-height:370px;width:1080px;' align='left'><TABLE cellspacing='0px'  class='viewallcass' width='"+countvisit+"%'>";
				    	}       
				    	else{		    		
				    		createTab=createTab+"<div id='table_div'  class='vdiv' onscroll='return  myscroll();' style='overflow:scroll;max-height:370px;width:790.4px;' align='left'><TABLE cellspacing='0px'  class='viewallcass' width='"+countvisit+"%'>";
					    	}
						    }
		    		createTab=createTab+"<tr><td colspan="+visitIdList.length+3+"> No Records Found</td></tr>";
		    		
		    		}
		    	else{
		    		if(brow=='msie'){
				    	if(screenWidth>1280 || screenHeight>1024){	 
				    	createTab=createTab+"<div id='table_div'  class='vdiv' onscroll='return  myscroll();' style='overflow:scroll;overflow-y:hidden;WIDTH: 1090px;' align='left'><TABLE cellspacing='0px'  class='viewallcass'>";
				    	}
				    	else{
				    		createTab=createTab+"<div id='table_div'  class='vdiv' onscroll='return  myscroll();' style='overflow:scroll;overflow-y:hidden;max-height:370px;width:1005px;' align='left'><TABLE cellspacing='0px'  class='viewallcass'>";
					    	}
				    }   
					    else{
					    	if(screenWidth>1280 || screenHeight>1024) {			    	
				    	createTab=createTab+"<div id='table_div'  class='vdiv' onscroll='return  myscroll();' style='overflow:scroll;max-height:370px;width:1080px;' align='left'><TABLE cellspacing='0px'  class='viewallcass'>";
				    	}       
				    	else{		    		
				    		createTab=createTab+"<div id='table_div'  class='vdiv' onscroll='return  myscroll();' style='overflow:scroll;max-height:370px;width:790.4px;' align='left'><TABLE cellspacing='0px'  class='viewallcass'>";
					    	}
						    }
		    		createTab=createTab+"<tr><td colspan="+visitIdList.length+3+"> No Records Found</td></tr>";
		    		
		    	}
		    	
		    		
		    }
		    var eventNam = $j("#eventName").val();
		    createTab=createTab+"<input type=hidden id='calledFrom' name='calledFrom' value='<%=StringUtil.htmlEncodeXss(calledFrom)%>'/>";
		    createTab=createTab+"<input type=hidden id='calstatus' name='calstatus' value='<%=StringUtil.htmlEncodeXss(request.getParameter("calStatus"))%>'/>";
		    createTab=createTab+"<input type=hidden id='eventName' name='eventName' value='"+eventNam+"'/>";
		    createTab=createTab+"<input type=hidden id='protocolId' name='protocolId' value='<%=StringUtil.htmlEncodeXss(protocolId)%>'/>";
	    	document.getElementById('coveragegrid').innerHTML = '';
	    	document.getElementById('coveragenotes').innerHTML = '';
		document.getElementById('coveragegrid').innerHTML = createTab;
		hidePanel();
		var colCount=$j('#firstcol>table tr').length;
		$j('.tableFirstCol').css("width",$j('#firstTd').width());
		for(var i=0;i<colCount;i++){
			if($j('#yui-rect'+i).height()>$j('#yui-rec'+i).height()){
				$j('#yui-rec'+i).css("height",$j('#yui-rect'+i).height());
			}
			else{
				$j('#yui-rect'+i).css("height",$j('#yui-rec'+i).height());
			}
		}
		incomingUrlParams = urlParamSB;
		var myParams = [];
		if (incomingUrlParams) {
			incomingUrlParams = incomingUrlParams.replace("'", "\"");
			myParams = incomingUrlParams.split("&");
			for (var iX=0; iX<myParams.length; iX++) {
				if (myParams[iX].match(/^pr[=]/)) {
					part = myParams[iX].split("=");
					pageRight = part[1];
					break;
				}
			}
			
		}
	        },
	        error: function (xhr, status, errorThrown) {
	            
	        }
	    });
	    return false;
}
YAHOO.util.Event.addListener(window, "load", function() {
	reloadCoverageGrid();
});
function openExport(format) {
	var newWin = window.open("donotdelete.html","exportWin","toolbar=no,scrollbars=yes,resizable=yes,menubar=yes,status=no,left=20,top=20");
	newWin.document.write('<html>');
	newWin.document.write('<head><title><%=LC.L_Coverage_AnalysisExport%></title></head><body>');/*newWin.document.write('<head><title>Coverage Analysis Export</title></head><body>');*****/
	newWin.document.write('<form name="dummy" target="_self" action="fetchCoverageExport.jsp?');
	newWin.document.write('format='+format+'&');
	newWin.document.write('<%=urlParamSB.toString()%>');
	newWin.document.write('" method="POST">');
	newWin.document.write('<input type="hidden" id="key" name="key" ');
	newWin.document.write(' value="<%=keySessId%>"/>');
	newWin.document.write('</form>');
	newWin.document.write('</body></html>');
	newWin.document.close();
	newWin.document.dummy.submit();
}
</script>

<%
	int accId = EJBUtil.stringToNum((String) tSession.getAttribute("accountId"));
	SettingsDao setDao = commonB.getSettingsInstance();
	setDao.getSettingsValue("COVERAGE_ANALYSIS_MODE", accId, 0, 0);
	ArrayList setValArray = setDao.getSettingValue();
	String covMode = null;
	if (setValArray != null && setValArray.size() > 0) {
		covMode = (String)setValArray.get(0);
	}
	if (covMode == null || covMode.length() == 0) { covMode = "V"; } // Make view-first the default mode
	StringBuffer secondElement = new StringBuffer();
	if (covMode.equals("V")) {
	    secondElement.append("<input type='hidden' id='save_changes' name='save_changes'/><FONT class='Mandatory'>");
	    if ((calStatus.equals("A")) || (calStatus.equals("F")) || !isAccessibleFor(pageRight, 'E')) {
	    	secondElement.append("("+MC.M_ClkView_ChgSvd+")");/*secondElement.append("(Click row to view; changes cannot be saved)");*****/
	    } else {
	    	secondElement.append("("+MC.M_ClkRow_ToEdt+")");/*secondElement.append("(Click row to edit)");*****/
	    }
	    secondElement.append("</FONT>");
	} else if (covMode.equals("E")) {
		secondElement.append("<button type='submit' id='save_changes' name='save_changes'")
			.append(" onclick=\"if (f_check_perm("+pageRight+",'E')) { void(0); } \">"+LC.L_Preview_AndSave+"</button>");
	}
%>

<%--
class="basetbl midAlign"
@commented for bug 21942 
--%>
  <table width="99%" cellspacing="" cellpadding="2px" border="0"  style="margin-left: 5px">
  <tr ><td><P class="defComments"><%=LC.L_Cal_Status%><%-- Calendar Status*****--%>: <%=calStatDesc%></P></td></tr>
  <tr>
  <!--KM-#DFin9-->
  <td > <%=LC.L_Search%>&nbsp;<%=LC.L_Event%> &nbsp;
  	  	<span id="searchEvent"><input type="text"  size="10"  name="eventName" id="eventName"  onkeyup="reloadCoverageGrid();" onkeypress="if (fnOnceEnterKeyPress(event)>0) {reloadCoverageGrid();} ">&nbsp;<!--<button type="submit" onClick="reloadCoverageGrid();"><%=LC.L_Search%>&nbsp;<%=LC.L_Event%></button>-->&nbsp;<button type="submit" onClick="searchClear();" ><%=LC.L_Clear %>&nbsp;<%=LC.L_Search%></button></span>
  	&nbsp;<%if(("N".equalsIgnoreCase(LC.VIEW_ALL))){ %>
  	<a id="viewAllVisit" href="#" onclick="reloadGridAsFetchViewAll('viewAllVisit');">View All</a>
  	<%} else { %>
  	<a id="viewAllVisit" href="#"  style="display:none;" onclick="reloadGridAsFetchViewAll('viewAllVisit');">View All</a>
  	<%} %>
  </td>
  
  
  <td align="Right"><%=secondElement.toString()%>
  <input type=hidden id='covTypeLegend' name='covTypeLegend' value="<%=covTypeLegend%>"/>
	&nbsp;<a href="javascript:void(0)" onmouseover="return overlib('<%=covTypeLegend%>',CAPTION,'<%=LC.L_Coverage_TypeLegend%><%-- Coverage Type Legend*****--%>');" onmouseout="return nd();"><%=LC.L_Coverage_TypeLegend%><%-- Coverage Type Legend*****--%></a>
 	&nbsp;<%=LC.L_Export_To%><%-- Export to*****--%>:&nbsp;&nbsp;<a href="javascript:openExport('html');"><img title="<%=LC.L_Printer_Friendly %><%-- Printer Friendly*****--%>" alt="<%=LC.L_Printer_Friendly %><%-- Printer Friendly*****--%>" align="bottom" border="0" src="../images/jpg/printer.gif" width="21" height="20"/></a>
 	&nbsp;<a href="javascript:openExport('excel');"><img title="<%=LC.L_Export_ToExcel%><%-- Export to Excel*****--%>" alt="<%=LC.L_Export_ToExcel%><%-- Export to Excel*****--%>" align="bottom" border="0" src="../images/jpg/excel.GIF"/></a>
	&nbsp;<a href="javascript:openExport('word');"><img title="<%=LC.L_ExportTo_Word%><%-- Export to Word*****--%>" alt="<%=LC.L_ExportTo_Word%><%-- Export to Word*****--%>" align="bottom" border="0" src="../images/jpg/word.GIF"/></a>
     
    </td>
    <td align="Right">
   
    <span id="visitsSpan">&nbsp;<a id="firstVisits" name="firstVisits" href="#" onclick="reloadGridAsFetch('firstVisits');"><%=LC.L_First%></a>
    &nbsp;<a id="previousVisits" href="#" onclick="reloadGridAsFetch('previousVisits');"><%=LC.L_Previous_Visits%></a>
    &nbsp;<a id="nextVisits" href="#" onclick="reloadGridAsFetch('nextVisits');"><%=LC.L_Next_Visits%></a>
    &nbsp;<a id="lastVisits" href="#" onclick="reloadGridAsFetch('lastVisits');"><%=LC.L_Last%></a></span>
    </td>
  </tr>
  </table>
  <div id="coveragegrid" onmouseout="return nd();" class="yui-dt yui-dt-scrollable"></div>
  <table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl midAlign"  >
  <tr height="7"></tr>
  <tr>
  <!--KM-#DFin9-->
  <td width="20%">
     <span id="eventsSpan">&nbsp;&nbsp;<a id="firstEvents" href="#" onclick="reloadGridAsFetch('firstEvents');"><%=LC.L_First%></a>
    &nbsp;&nbsp;<a id="previousEvents" href="#" onclick="reloadGridAsFetch('previousEvents');"><%=LC.L_Previous_Events%></a>
    &nbsp;&nbsp;<a id="nextEvents" href="#" onclick="reloadGridAsFetch('nextEvents');"><%=LC.L_Next_Events%></a>
    &nbsp;&nbsp;<a id="lastEvents" href="#" onclick="reloadGridAsFetch('lastEvents');"><%=LC.L_Last%></a></span>
  </td>
  </tr>
    <tr height="7"></tr>
  </table>
  
  <div  id="coveragenotes"></div>
  
  <input type="hidden" name="calledFrom" id="calledFrom" value="<%=calledFrom%>"/>
  <input type="hidden" name="studyId" id="studyId" value="<%=studyId%>"/>
<%
  } // end of pageRight
  else {
%>
<!--12-04-2011 #6013 @Ankit  -->
	<jsp:include page="accessdenied.jsp" flush="true"/>
<%
  } // end of else of pageRight
} // end of else of invalid protocolId
} else { // else of valid session
%>
  <jsp:include page="timeout.html" flush="true"/>
<%
} // end of else of valid session
%>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id = "emenu" > 
  <jsp:include page="getmenu.jsp" flush="true"/> 
</div>
<div id="visitconfirm" style="position:absolute;display:none; z-index:1000;">
<p>Do You Want To save the changes:</p>
<input type="button" value="Ok" id="saveVisit" onclick="save()">
<input type="button" value="Cancel" id="cancel" onclick="cancel()">
<script>
function cancel()
{
document.getElementById("visitconfirm").visibility="hidden";
//document.getElementById("visitconfirm").display="none";
}
if("<%=vclick1%>"==1){
	document.getElementById("viewAllVisit").innerHTML="View By Pagination";
	$j('#visitsSpan').css('display', 'none');
	$j('#eventsSpan').css('display', 'none');
}

function myscroll(){
    $j(".hdiv").scrollLeft($j(".vdiv").scrollLeft());
    $j('.cdiv').scrollTop($j('.vdiv').scrollTop());
};//end of my scroll

</script>
</div>
</body>
</html>
<%!
private boolean isAccessibleFor(int pageRight, char type) {
    switch(type) {
    case 'V': return ((pageRight & 0x0004) >> 2) == 1;
    case 'E': return ((pageRight & 0x0002) >> 1) == 1;
    case 'N': return  (pageRight & 0x0001) == 1;
    }
    return false;
}
%>