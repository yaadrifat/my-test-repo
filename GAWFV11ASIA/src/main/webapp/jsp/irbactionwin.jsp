<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
<script language="JavaScript" src="formjs.js"><!-- FORM JS--></script>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<jsp:include page="include.jsp" flush="true"/>
<style>html,body { overflow:visible; } </style>
<jsp:useBean id ="sessionmaint" scope="request" class="com.velos.eres.service.util.SessionMaint"/>
<SCRIPT LANGUAGE="JavaScript">
<%
CodeDao codeDao = new CodeDao();
String assignedForAdminReview = "0";
String assignedForPostReview = "0";

String reviewerStatus  = "0";

if (sessionmaint.isValidSession(request.getSession())) {
    codeDao.setForGroup((String)request.getSession().getAttribute("defUserGroup"));
    codeDao.getCodeValues("subm_status");
    ArrayList codeSubtypes1 = codeDao.getCSubType();
    ArrayList cIds1 = codeDao.getCId();
    for(int iX=0; iX<codeSubtypes1.size(); iX++) {
        if ("admin_review".equals((String)codeSubtypes1.get(iX))) {
            assignedForAdminReview = ""+cIds1.get(iX);
        }
        if ("asgn_pr".equals((String)codeSubtypes1.get(iX))) {
            assignedForPostReview = ""+cIds1.get(iX);
        }
        
        if ("reviewer".equals((String)codeSubtypes1.get(iX))) {
            reviewerStatus  = ""+cIds1.get(iX);
        }
        
        
    }
}

%>
function submitMainForm(formobj) {
	formobj.action = "updateSubmission.jsp";
}
function openUserSearch(frm) {
	if (frm != null && frm != '') {
		if (document.getElementById(frm) != null) {
			if (document.getElementById(frm).disabled) { return; }
		}
	}
    windowName = window.open("usersearchdetails.jsp?fname=&lname=&from="+ frm,"TEST","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=500,left=100,top=200");
    windowName.focus();
}
function getElementByID(formobj, id) {
	var elems = document.getElementsByName(id)
	return elems[0];
}
function applyFromPowerBar(formobj, totalB) {
	for(var boardNum=0; boardNum<totalB; boardNum++) {
		getElementByID(formobj,"editBoard"+boardNum).checked = true;
		enableRow(formobj, boardNum);
	    if (getElementByID(formobj,"donotapplyempty").checked) {
    		var idx1 = getElementByID(formobj,"submissionStatus").selectedIndex;
    		var opt1 = getElementByID(formobj,"submissionStatus").options;
    		if (opt1[idx1].value != '') {
    			getElementByID(formobj,"submissionStatus"+boardNum).selectedIndex = idx1;
    		}
		    if (getElementByID(formobj,"actionDate").value != null && 
		    		getElementByID(formobj,"actionDate").value != '') {
		    	getElementByID(formobj,"actionDate"+boardNum).value = 
		    		getElementByID(formobj,"actionDate").value;
			}
		    if (getElementByID(formobj,"irbEnteredBy").value != null &&
		    		getElementByID(formobj,"irbEnteredBy").value != '') {
		    	getElementByID(formobj,"irbEnteredBy"+boardNum).value =
	        		getElementByID(formobj,"irbEnteredBy").value;
		    	getElementByID(formobj,"irbEnteredById"+boardNum).value =
		        	getElementByID(formobj,"irbEnteredById").value;
		    }
		    if (getElementByID(formobj,"irbAssignedTo").value != null &&
		    		getElementByID(formobj,"irbAssignedTo").value != '') {
		    	getElementByID(formobj,"irbAssignedTo"+boardNum).value =
	        		getElementByID(formobj,"irbAssignedTo").value;
		    	getElementByID(formobj,"irbAssignedToId"+boardNum).value =
		        	getElementByID(formobj,"irbAssignedToId").value;
		    }
		    var idx2 = getElementByID(formobj,"submissionReview").selectedIndex;
		    var opt2 = getElementByID(formobj,"submissionReview").options;
		    if (opt2[idx2].value != '') {
		    	getElementByID(formobj,"submissionReview"+boardNum).selectedIndex = idx2;
		    }
	    } else {
    		var idx = getElementByID(formobj,"submissionStatus").selectedIndex;
    		getElementByID(formobj,"submissionStatus"+boardNum).selectedIndex = idx;
    		getElementByID(formobj,"actionDate"+boardNum).value = 
    			getElementByID(formobj,"actionDate").value;
    		getElementByID(formobj,"irbEnteredBy"+boardNum).value =
    			getElementByID(formobj,"irbEnteredBy").value;
    		getElementByID(formobj,"irbEnteredById"+boardNum).value =
		    	getElementByID(formobj,"irbEnteredById").value;
    		getElementByID(formobj,"irbAssignedTo"+boardNum).value =
    			getElementByID(formobj,"irbAssignedTo").value;
    		getElementByID(formobj,"irbAssignedToId"+boardNum).value =
		    	getElementByID(formobj,"irbAssignedToId").value;
    		getElementByID(formobj,"submissionReview"+boardNum).selectedIndex = 
        		getElementByID(formobj,"submissionReview").selectedIndex;
	    }
	}
}
function toggleEditOverall() {
	if (document.getElementById("overallcheck").checked) {
		document.getElementById("editOverall").style.visibility = "visible";
	} else {
		document.getElementById("editOverall").style.visibility = "hidden";
	}
}
function enableRowAndCheck(formobj, boardNum) {
	if (getElementByID(formobj,"submitTo"+boardNum).checked) {
		getElementByID(formobj,"editBoard"+boardNum).checked = true;
	} else {
		getElementByID(formobj,"editBoard"+boardNum).checked = false;
	}
	enableRow(formobj, boardNum);
}
function enableRow(formobj, boardNum) {
	var elem = getElementByID(formobj,"submissionStatus"+boardNum);
	var check = getElementByID(formobj,"editBoard"+boardNum);
	if (check.checked) {
		if (getElementByID(formobj,"submitTo"+boardNum) != null) {
			getElementByID(formobj,"submitTo"+boardNum).checked = true;
		}
		getElementByID(formobj,"submissionStatus"+boardNum).disabled = false;
		getElementByID(formobj,"actionDate"+boardNum).disabled = false;
		getElementByID(formobj,"irbEnteredBy"+boardNum).disabled = false;
		getElementByID(formobj,"irbAssignedTo"+boardNum).disabled = false;
		getElementByID(formobj,"submissionReview"+boardNum).disabled = false;
		getElementByID(formobj,"meetingDate"+boardNum).disabled = false;
		getElementByID(formobj,"reviewerFirstNames"+boardNum).disabled = false;
		getElementByID(formobj,"notes"+boardNum).disabled = false;
		openCal();
	} else {
		getElementByID(formobj,"submissionStatus"+boardNum).disabled = true;
		getElementByID(formobj,"actionDate"+boardNum).disabled = true;
		getElementByID(formobj,"irbEnteredBy"+boardNum).disabled = true;
		getElementByID(formobj,"irbAssignedTo"+boardNum).disabled = true;
		getElementByID(formobj,"submissionReview"+boardNum).disabled = true;
		getElementByID(formobj,"meetingDate"+boardNum).disabled = true;
		getElementByID(formobj,"reviewerFirstNames"+boardNum).disabled = true;
		getElementByID(formobj,"notes"+boardNum).disabled = true;
	}
}
function openLookup(formobj,url,filter,caller) {
	if (caller != null && caller != '') {
		if (document.getElementById(caller) != null) {
			if (document.getElementById(caller).disabled) { return; }
		}
	}
	var tempValue;
	var urlL="";
	
	formobj.target="Lookup";
	formobj.method="post";
	urlL="multilookup.jsp?"+url;
	if (filter.length>0){
	if (filter.indexOf(':studyId')>=0)
	{
	  tempValue="";
	  tempValue=formobj.paramstudyId.value;
	  filter=replaceSubstring(filter,":studyId",tempValue);
	}
	urlL=urlL+"&"+filter;
	}
	formobj.action=urlL;

	formWin =open('donotdelete.html','Lookup','resizable=1,status=0, width=850,height=550 top=100,left=100,menubar=no,scrollbars=1');
	if (formWin && !formWin.closed) formWin.focus();
	formobj.submit();
	formobj.target="";
	formobj.action = "updateSubmission.jsp";
}
</SCRIPT>
<title><%=LC.L_Action_Page%><%--Action Page*****--%></title>
</head>
<body>
<DIV style="font-family:Verdana,Arial,Helvetica,sans-serif">
<form name="mainForm" id="mainForm" action="updateSubmission.jsp" onSubmit="if(validate(this)==false){setValidateFlag('false');return false;}else{submitMainForm(this);}" method="post">
<jsp:useBean id="submBoardB" scope="request" class="com.velos.eres.web.submission.SubmissionBoardJB"/>
<%@ page import="com.velos.eres.business.common.*,com.velos.eres.service.util.*" %>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<%@ page import="java.util.ArrayList,java.util.Calendar,java.util.Hashtable" %>
<%
HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession)) {
    // tSession.removeAttribute("IRBParams"); // clear parameters that might have been passed to forms
    boolean isMSIE = false;
    if (request.getHeader("USER-AGENT").toLowerCase().indexOf("msie") != -1) { isMSIE = true; }
    
    CodeDao cdSubmStatus = new CodeDao();
    cdSubmStatus.setForGroup((String)tSession.getAttribute("defUserGroup"));
    
    cdSubmStatus.getCodeValuesFilterCustom1("subm_status",""); //get all statuses that are not marked as final or overall 
    
    cdSubmStatus.setCType("subm_status");
    String cdStatusPowerBarMenu = cdSubmStatus.toPullDown("submissionStatus");

    CodeDao cdSubmStatusOverall = new CodeDao();
    // cdSubmStatusOverall.getCodeValues("subm_status", "overall");
    cdSubmStatusOverall.setForGroup((String)tSession.getAttribute("defUserGroup"));
    cdSubmStatusOverall.getCodeValuesFilterCustom1("subm_status", "overall");
    
    CodeDao cdSubmReview = new CodeDao();
    cdSubmReview.getCodeValues("revType");
    cdSubmReview.setCType("revType");
    String cdReviewPowerBarMenu = cdSubmReview.toPullDown("submissionReview");
    
    String accountId = (String)tSession.getAttribute("accountId");
    String submissionBoardPK = request.getParameter("submissionBoardPK");
    String provisoflag = request.getParameter("provisoflag")==null?"":request.getParameter("provisoflag");
    int defaultBoardPK = 0;
    int fKReviewBoardGov = 0;
    
    defaultBoardPK = submBoardB.getDefaultReviewBoardWithCheckGroupAccess(EJBUtil.stringToNum(accountId), EJBUtil.stringToNum( (String)tSession.getAttribute("defUserGroup") ) );
    fKReviewBoardGov = submBoardB.getDefaultReviewBoardWithCheckGroupAccess(EJBUtil.stringToNum(accountId), EJBUtil.stringToNum( (String)tSession.getAttribute("defUserGroup") ),EJBUtil.stringToNum(submissionBoardPK) );	
    String studyId = request.getParameter("studyId");
	tSession.setAttribute("studyId", StringUtil.htmlEncodeXss(studyId));
    String defUserGroup = (String) tSession.getAttribute("defUserGroup");
    String usrId = (String) tSession.getValue("userId");
    String usrFullName = UserDao.getUserFullName(EJBUtil.stringToNum(usrId));
    String submissionPK = request.getParameter("submissionPK");
    
    studyB.setId(EJBUtil.stringToNum(studyId));
	studyB.getStudyDetails();
	String studyNumber = studyB.getStudyNumber();
	String studyTitle = studyB.getStudyTitle();
	
    EIRBDao eIrbDao = new EIRBDao();
    eIrbDao.getReviewBoards(EJBUtil.stringToNum(accountId), EJBUtil.stringToNum(defUserGroup));
    ArrayList boardNameList = eIrbDao.getBoardNameList();
    
    ArrayList boardIdList = eIrbDao.getBoardIdList();
    
    ReviewMeetingDao reviewMeetingDao = new ReviewMeetingDao();
    reviewMeetingDao.getReviewMeetings(EJBUtil.stringToNum(accountId), 
            EJBUtil.stringToNum(defUserGroup), boardIdList);
    
    String actionDateStr = DateUtil.dateToString(Calendar.getInstance().getTime());
    String irbEnteredByStr = usrFullName;
    String irbAssignedToStr = "";
    String meetingDateStr = "";
    String reviewerFirstNamesStr = "";
    
    String[] actionDateStrs = new String[boardNameList.size()];
    String[] irbEnteredByStrs = new String[boardNameList.size()];
    String[] irbEnteredByIds = new String[boardNameList.size()];
    String[] irbAssignedToStrs = new String[boardNameList.size()];
    String[] irbAssignedToIds = new String[boardNameList.size()];
    String[] meetingDateStrs = new String[boardNameList.size()];
    String[] reviewerFirstNamesStrs = new String[boardNameList.size()];
    String[] irbStatusNotes = new String[boardNameList.size()];
    
    String cdSubmStatusMenus[] = new String[boardNameList.size()];
    String cdSubmReviewMenus[] = new String[boardNameList.size()];
    
    eIrbDao.getCurrentStatuses(EJBUtil.stringToNum(accountId),EJBUtil.stringToNum(studyId),
            EJBUtil.stringToNum(defUserGroup), EJBUtil.stringToNum(usrId),
            EJBUtil.stringToNum(submissionPK));
    ArrayList statusList = eIrbDao.getCurrentStatuses();
    ArrayList statusCodeList = eIrbDao.getCurrentStatusCodes();
    ArrayList statusDateList = eIrbDao.getCurrentStatusDates();
    ArrayList statusEnteredByList = eIrbDao.getCurrentStatusEnteredBy();
    ArrayList statusAssignedToList = eIrbDao.getCurrentStatusAssignedTo();
    ArrayList statusNotesList = eIrbDao.getCurrentStatusNotes();

    for(int iX=0; iX<boardNameList.size(); iX++) {
        String actionDate = (String)statusDateList.get(iX);
        if (actionDate == null || actionDate.length() == 0) {
            actionDate = DateUtil.dateToString(Calendar.getInstance().getTime());
        }
        actionDateStrs[iX] = actionDate;
        // Handle enteredBy for each board
        String enteredBy = (String)statusEnteredByList.get(iX);
        if (enteredBy == null || enteredBy.length() == 0) {
            enteredBy = usrFullName;
            irbEnteredByIds[iX] = usrId;
        } else {
            String[] enteredByArray = enteredBy.split(", ");
            if (enteredByArray.length > 2) {
                enteredBy = enteredByArray[2]+" "+enteredByArray[1];
                irbEnteredByIds[iX] = enteredByArray[0];
            } else {
                enteredBy = usrFullName;
                irbEnteredByIds[iX] = usrId;
            }
        }
        irbEnteredByStrs[iX] = enteredBy;
        // Handle assignedTo for each board
        String assignedTo = (String)statusAssignedToList.get(iX);
        if (assignedTo == null || assignedTo.length() == 0) {
            assignedTo = "";
            irbAssignedToIds[iX] = "";
        } else {
            String[] assignedToArray = assignedTo.split(", ");
            if (assignedToArray.length > 2) {
                assignedTo = assignedToArray[2]+" "+assignedToArray[1];
                irbAssignedToIds[iX] = assignedToArray[0];
            } else {
                assignedTo = "";
                irbAssignedToIds[iX] = "";
            }
        }
        irbAssignedToStrs[iX] = assignedTo;
        // Handle notes
        if (statusNotesList.get(iX) != null) {
            String theNotes = (String)statusNotesList.get(iX);
            if (theNotes != null) {
                theNotes = theNotes.replaceAll("&lt;br/&gt;","\n");
            }
            irbStatusNotes[iX] = theNotes;
        } else {
            irbStatusNotes[iX] = "";
        }
    }
    
    if (studyId == null || studyId.length() == 0) { 
%>
<script>
  self.close();
</script>
</form>
</body>
</html>
<%      return;
    }  %>
<script>
function validate(formobj) {

	setCheckQuote('N');
			
	if (!(validate_col('e-Signature',formobj.eSign))) return false;
 
	var actionExists = false;
	for (var boardNum=0; boardNum < <%=Integer.valueOf(boardNameList.size())%>; boardNum++) {
		if ( getElementByID(formobj,"editBoard"+boardNum).checked == false) { continue; }
		if ( getElementByID(formobj,"submissionStatus"+boardNum).value == '') {
			alert('<%=MC.M_PlsSel_Action%>');/*alert('Please select an Action');*****/
			return false;
		}
		actionExists = true;
		if ( getElementByID(formobj,"submissionStatus"+boardNum).value==<%=assignedForAdminReview%>
		  || getElementByID(formobj,"submissionStatus"+boardNum).value==<%=assignedForPostReview%> ) {
			  if (getElementByID(formobj,"irbAssignedToId"+boardNum).value == '') {
			      alert('<%=MC.M_Specify_AssignedTo%>');/*alert('Please specify Assigned To.');*****/
			      return false;
			  }
		}
		
		//check for reviewer
		if ( getElementByID(formobj,"submissionStatus"+boardNum).value==<%=reviewerStatus%> ) {
			  if (getElementByID(formobj,"submissionReview"+boardNum).value == '') {
			      alert('<%=MC.M_Selc_ReviewType%>');/*alert('Please select a Review Type');*****/
			      getElementByID(formobj,"submissionReview"+boardNum).focus();
			      return false;
			  }
		}
		
		
		
			
			
		if (getElementByID(formobj,"notes"+boardNum) != null 
				&& getElementByID(formobj,"notes"+boardNum).value.length > 4000 ) {
			var paramArray = [getElementByID(formobj,"notes"+boardNum).value.length];
			alert(getLocalizedMessageString("M_NotesExcd_4kCur",paramArray));/*alert('Notes may not exceed 4000 characters. Currently '+getElementByID(formobj,"notes"+boardNum).value.length);*****/
			return false;
		}
	}
	for (var boardNum=0; boardNum < <%=Integer.valueOf(boardNameList.size())%>; boardNum++) {
		if ( getElementByID(formobj,"submitTo"+boardNum) == null) { continue; }
		if ( getElementByID(formobj,"submitTo"+boardNum).checked == false) { continue; }
		actionExists = true;
	}
	if (getElementByID(formobj,"overallcheck").checked == true) { actionExists = true; }
	
	if (!actionExists) {
		alert('<%=MC.M_SelAtLeast_OneCheckbox%>');/*alert('Please select at least one Edit checkbox.');*****/
		return false;
	}
	
	if (document.getElementById("overallcheck").checked == true) //check for mandatory fields
	{
			if (!(validate_col('Final Outcome Status',formobj.submissionStatusOverall))) return false;		
			if (!(validate_col('Final Outcome Status Date',formobj.overallDate))) return false;
			if (!(validate_col('Final Outcome Status Entered By',formobj.overallEnteredBy))) return false;
	}
		
    return true;
}
</script>
<table width="100%" cellspacing="0" cellpadding="0" Border="0">
<tr><td><P class="sectionHeadings"><%=LC.L_Enter_NewAction%><%--Enter New Action*****--%></P></td>
</tr>
<tr><td><P class="defComments"><b><%=LC.L_Study_Number%><%--<%LC.Std_Study%> Number*****--%>: </b><%=studyNumber%></P></td></tr>
<tr>  <td>&nbsp;&nbsp;</td></tr>
<tr><td><P class="defComments"><b><%=LC.L_Study_Title%><%--<%=LC.Std_Title%> Title*****--%>: </b><%=studyTitle%></P></td></tr>
<tr>  <td>&nbsp;&nbsp;</td></tr>

<tr style="visibility:hidden">
<td align="right" style="font-size:8pt;"><a href="#overall_anchor"><%=LC.L_Overall_AppStatus%><%--Overall Application Status*****--%></a></td>
<td><br/></td>
</tr>
</table>
<table width="100%" cellspacing="0" cellpadding="1" border="1" frame="box"
       style="border-color:gray;border-style:solid;font-size:8pt;">
       
<% if (isMSIE) { %>
<tr bordercolor="#f7f7f7"><td colspan="8"><hr size="1" style="color:gray"/></td></tr>
<% } %>
<%
  for(int iBoard=0; iBoard<boardNameList.size(); iBoard++) {
      submBoardB.findByPkSubmissionBoard(EJBUtil.stringToNum(request.getParameter("submissionBoardPK")));
      String fkReviewBoard = submBoardB.getFkReviewBoard();
      String boardColor = "#f7f7f7";
      if (fkReviewBoard.equals(boardIdList.get(iBoard))) {
          boardColor = "#ffffe0";
      }
      String statusLabel = statusList.get(iBoard) == null ?""
//    		  MC.M_AppNtSub_ChkSub/*Application Not Submitted<br/>Check to Submit*****/+"<input id=\"submitTo"+iBoard+"\" name=\"submitTo"+
  //            iBoard+"\" type=\"checkbox\" onClick=\"enableRowAndCheck(document.mainForm,'"+iBoard+"')\"/>" 
                :(String)statusList.get(iBoard)+" "+LC.L_On/*on*****/+"<br/>"+(String)statusDateList.get(iBoard);
      String scriptForStatus = " onChange=\"enableRow(document.mainForm,'"+iBoard+"')\"";
      cdSubmStatusMenus[iBoard] = cdSubmStatus.toPullDown("submissionStatus"+iBoard, 
              EJBUtil.stringToNum((String)statusCodeList.get(iBoard)), "disabled");

      int flag = submBoardB.findByFkSubmissionAndBoard(EJBUtil.stringToNum(submissionPK),
              (Integer.valueOf((String)boardIdList.get(iBoard))).intValue());
      String fkReviewMeeting = null;
      String submissionReviewType = null;
      String reviewerIdList = null;
      String reviewerFNList = null;
      String reviewerLNList = null;
      String reviewerFullNameList = null;
      if (flag > 0) {
          fkReviewMeeting = submBoardB.getFkReviewMeeting();
          submissionReviewType = submBoardB.getSubmissionReviewType();
          reviewerIdList = submBoardB.getSubmissionReviewer();
          reviewerFNList = submBoardB.getReviewerFirstNameList();
          reviewerLNList = submBoardB.getReviewerLastNameList();
          reviewerFullNameList = submBoardB.getReviewerFullNameList();
        /*  if(reviewerFullNameList!=null){
        	  reviewerFullNameList= reviewerLNList+";"+" "+reviewerFNList;
          }*/
         
          
      }
      if (reviewerIdList == null) { reviewerIdList = ""; }
      if (reviewerFNList == null) { reviewerFNList = ""; }
      if (reviewerLNList == null) { reviewerLNList = ""; }
      if (reviewerFullNameList == null) { reviewerFullNameList = ""; }
      meetingDateStrs[iBoard] = reviewMeetingDao.getMeetingDropDown(iBoard, 
              (String)boardIdList.get(iBoard), fkReviewMeeting);
      reviewerFirstNamesStrs[iBoard] = reviewerFNList;

      cdSubmReviewMenus[iBoard] = cdSubmReview.toPullDown("submissionReview"+iBoard, 
              EJBUtil.stringToNum(submissionReviewType), "disabled"); // Initally always disabled
%>
<tr bordercolor="#f7f7f7"><td colspan="8">&nbsp;</td></tr>
<tr bordercolor="<%=boardColor%>" bgcolor="<%=boardColor%>">
  <td width="20%"><b><%=(String)boardNameList.get(iBoard)%></b></td>
  <td width="2%">
  <%  if (fkReviewBoard.equals(boardIdList.get(iBoard))) {%>
         <%=LC.L_Edit%><%--Edit*****--%><br/><input type="checkbox" id="editBoard<%=iBoard%>" name="editBoard<%=iBoard%>" onClick="enableRow(document.mainForm,'<%=iBoard%>')" />
    <% }else{%> 
    <input type="checkbox" id="editBoard<%=iBoard%>" style="display:none" name="editBoard<%=iBoard%>" onClick="enableRow(document.mainForm,'<%=iBoard%>')" /> 
    <%} %>
 
  </td>
  <td width="26%"><%=LC.L_Select_Action%><%--Select Action*****--%><FONT class="Mandatory">*</FONT><br><%=cdSubmStatusMenus[iBoard] %></td>
  <td width="18%"><%=LC.L_Date%><%--Date*****--%><br/>
<%-- INF-20084 Datepicker-- AGodara --%>  
    <input type="text" id="actionDate<%=iBoard%>" class="datefield" name="actionDate<%=iBoard%>" size="10" MAXLENGTH="20" 
      value="<%=actionDateStrs[iBoard]%>" READONLY disabled/>
  </td>
  <td width="18%"><%=LC.L_Entered_By%><%--Entered By*****--%><br/>
    <input type="hidden" id="irbEnteredById<%=iBoard%>" name="irbEnteredById<%=iBoard%>" value="<%=irbEnteredByIds[iBoard]%>">
    <input type="text" id="irbEnteredBy<%=iBoard%>" name="irbEnteredBy<%=iBoard%>" size="10" MAXLENGTH="40" 
      value="<%=irbEnteredByStrs[iBoard]%>" READONLY disabled/>&nbsp;<A href="javascript:void(0);" 
      onClick="return openUserSearch('irbEnteredBy'+<%=iBoard%>)"><img id="user_open" 
      src="./images/user.png" align="absmiddle" border="0" width="25" height="25" /></A>
  </td>
  <td width="18%"><%=LC.L_Assigned_To%><%--Assigned To*****--%><br/>
    <input type="hidden" id="irbAssignedToId<%=iBoard%>" name="irbAssignedToId<%=iBoard%>" value="<%=irbAssignedToIds[iBoard]%>">
    <input type="text" id="irbAssignedTo<%=iBoard%>" name="irbAssignedTo<%=iBoard%>" size="10" MAXLENGTH="40" 
      value="<%=irbAssignedToStrs[iBoard]%>" READONLY disabled/>&nbsp;<A href="javascript:void(0);"
      onClick="return openUserSearch('irbAssignedTo'+<%=iBoard%>)"><img id="user_open" 
      src="./images/user.png" align="absmiddle" border="0"width="25" height="25" /></A>
  </td>
  <DIV id="userdataDIV<%=iBoard%>">
    <input type="hidden" id="reviewerIds<%=iBoard%>" name="reviewerIds<%=iBoard%>" value="<%=reviewerIdList%>" />
    <Input TYPE="hidden" NAME="reviewerFullNames<%=iBoard%>" VALUE="<%=reviewerFullNameList%>">
	<Input TYPE="hidden" NAME="dummy1<%=iBoard%>" VALUE="<%=reviewerLNList%>">
  </DIV>

</tr>
<tr bordercolor="<%=boardColor%>" bgcolor="<%=boardColor%>">
  <td width="26%"><%=statusLabel%></td>
  <td>&nbsp;</td>
  <td width="40%" colspan="1"><%=LC.L_Notes%><%--Notes*****--%><br/><TextArea id="notes<%=iBoard%>" name="notes<%=iBoard%>" rows="2" cols="25" disabled><%=irbStatusNotes[iBoard]%></TextArea></td>
  <td width="18%"><%=LC.L_Review_Type%><%--Review Type*****--%><br><%=cdSubmReviewMenus[iBoard]%></td>
  <td width="18%"><%=LC.L_Meeting_Date%><%--Meeting Date*****--%><br><%=meetingDateStrs[iBoard]%></td>
  <td><DIV id="userDIV<%=iBoard%>"><%=LC.L_Reviewers%><%--Reviewers*****--%><br>
    <input type="text" id="reviewerFirstNames<%=iBoard%>" name="reviewerFirstNames<%=iBoard%>" size="10" MAXLENGTH="400" 
      value="<%=reviewerFullNameList%>" READONLY disabled/>&nbsp;
<A href="javascript:void(0);" 
onClick="return openLookup(document.mainForm,
'viewId=6000&form=mainForm&seperator=,&defaultvalue=&keyword=reviewerFullNames<%=iBoard%>|USRFNAME|[VELHIDE]~dummy1<%=iBoard%>|USRLNAME|[VELHIDE]~reviewerIds<%=iBoard%>|USRPK|[VELHIDE]~reviewerFirstNames<%=iBoard%>|[VELEXPR]*[VELKEYWORD=USRFNAME]*[VELSTR=]*[VELSPACE]*[VELKEYWORD=USRLNAME]',
'','reviewerFirstNames<%=iBoard%>')">
<img id="user_open" src="./images/user.png" align="absmiddle" border="0" width="25" height="25" /></A>
  </DIV>
  </td>
</tr>

<%
  } // End of loop for iBoard

  eIrbDao.getCurrentOverallStatus(EJBUtil.stringToNum(studyId), EJBUtil.stringToNum(submissionPK));
  String currentOverallStatus = eIrbDao.getCurrentOverallStatus();
  
  ArrayList currentOverallStatus1=eIrbDao.getCurrentOverallStatus1();
  String currentOverallStatusDesc = eIrbDao.getCurrentOverallStatusDesc();
  ArrayList currentOverallStatusDesc1 = eIrbDao.getCurrentOverallStatusDesc1();
  String currentOverallStatusDate = eIrbDao.getCurrentOverallStatusDate();
  ArrayList currentOverallStatusDate1 = eIrbDao.getCurrentOverallStatusDate1();
  String currentOverallEnteredById = eIrbDao.getCurrentOverallEnteredById();
  ArrayList currentOverallEnteredById1 = eIrbDao.getCurrentOverallEnteredById1();
  String currentOverallEnteredBy = eIrbDao.getCurrentOverallEnteredBy();
  ArrayList currentOverallEnteredBy1 = eIrbDao.getCurrentOverallEnteredBy1();
  String currentOverallNotes = eIrbDao.getCurrentOverallNotes();
  ArrayList currentOverallNotes1 = eIrbDao.getCurrentOverallNotes1();
  ArrayList reviewBoardList=eIrbDao.getReviewBoard();
  
  if (currentOverallEnteredById == null || currentOverallEnteredById.length() == 0) {
      currentOverallEnteredById = "";
      currentOverallEnteredBy = "";
  }
  if (currentOverallNotes != null) {
      currentOverallNotes = currentOverallNotes.replaceAll("&lt;br/&gt;","\n");
  }
%>
</table>
	<br>
	<div class="popDefault">
		<table width="100%" cellspacing="0" cellpadding="0" Border="0" style="visibility:block" >
		<tr><td><br /></td></tr>
		<tr><td><P class="sectionHeadings"><%=LC.L_Final_OutcomeStatus%><%--Final Outcome Status*****--%></P></td></tr>
		<tr><td><br /></td></tr>
		</table>
		<table width="100%" cellspacing="0" cellpadding="0" Border="0"   align="center" style="visibility:block">
		<tr align="center" style="visibility:block">
		 <% if (defaultBoardPK > 0 ) { %> 
		 	 <th width="10%"><%=LC.L_Edit%><%--Edit*****--%></th>
		 <% } %>
		 		 
		    <th width="25%"><%=LC.L_Status%><%--Status*****--%><FONT class="Mandatory">*</FONT></th>
		    <th width="15%"><%=LC.L_Date%><%--Date*****--%><FONT class="Mandatory">*</FONT></th>
		    <th width="20%"><%=LC.L_Entered_By%><%--Entered By*****--%><FONT class="Mandatory">*</FONT></th>
		    <th width="20%">Review Board<%--Status*****--%></th>
		    <th width="35%"><%=LC.L_Notes%><%--Notes*****--%></th>
		</tr>
		<%if(eIrbDao.getRowCount()==0){ %>
		<tr align="center" style="visibility:block">
		  <% if (defaultBoardPK > 0 ) {
			 
		  %> 
		  
			  <td>
			    <input type="checkbox" id="overallcheck" name="overallcheck" onClick="toggleEditOverall()"/> 
			  </td>
		  
			
		  <%}%>
		   		  	  
		  <td> </td>
		  <td> </td>
		  <td> </td>
		  <td> </td>	 
		  <td><TextArea rows="1" cols="25" READONLY disabled></TextArea></td>
		</tr>
		<%} %>
		<% for(int i=0;i<eIrbDao.getRowCount();i++){ %>
		<tr align="center" style="visibility:block">
		  <% if (defaultBoardPK > 0 ) {
			  if(i==0){
		  %> 
		  
			  <td>
			    <input type="checkbox" id="overallcheck" name="overallcheck" onClick="toggleEditOverall()"/> 
			  </td>
		  <% }else{%>
			  <td> </td>
		  <%}}%>
		   		  	  
		  <td><%=currentOverallStatusDesc1.get(i) %></td>
		  <td><%=currentOverallStatusDate1.get(i) %></td>
		  <td><%=currentOverallEnteredBy1.get(i) %></td>
		  <td><%=reviewBoardList.get(i) %></td>	 
		  <%
		  String notes="";
		  if(currentOverallNotes1.get(i)!=null){
			  notes=currentOverallNotes1.get(i).toString(); 
		  }
		  
		  %>
		  <td><TextArea rows="1" cols="25" READONLY disabled><%=notes%></TextArea></td>
		</tr>
		<%} %>
		<tr id="editOverall" align="center" style="visibility:hidden">
		  <td>&nbsp;<input type="hidden" id="overallStatus" name="overallStatus" value="<%=currentOverallStatus %>"></td>
		<%
		  String cdStatusOverallMenu = cdSubmStatusOverall.toPullDown("submissionStatusOverall",
		          EJBUtil.stringToNum(currentOverallStatus));
		  String overallDateStr = currentOverallStatusDate;
		  if (overallDateStr != null || overallDateStr.length() == 0) {
		      DateUtil.dateToString(Calendar.getInstance().getTime());
		  }

		%>
		  <td><%=cdStatusOverallMenu%></td>
		  <td> 
<%-- INF-20084 Datepicker-- AGodara --%>		  
		    <input type="text" id="overallDate" class="datefield" name="overallDate" size="10" MAXLENGTH="20" 
		      value="<%=overallDateStr%>" READONLY />
		  </td>
		  <td>
		    <input type="hidden" id="provisoflag" name="provisoflag" value="<%=provisoflag%>" />
		    <input type="hidden" id="submissionPK" name="submissionPK" value="<%=EJBUtil.stringToNum(submissionPK)%>" />
		    <input type="hidden" id="submissionBoardPK" name="submissionBoardPK" value="<%=EJBUtil.stringToNum(submissionBoardPK)%>" />
		    <input type="hidden" id="overallEnteredById" name="overallEnteredById" value="<%=currentOverallEnteredById%>" />
		    <input type="text" id="overallEnteredBy" name="overallEnteredBy" size="10" MAXLENGTH="40" 
		      value="<%=currentOverallEnteredBy%>" READONLY />&nbsp;<A href="javascript:void(0);"
		      onClick="return openUserSearch('overallEnteredBy')"><img id="user_open" 
		      src="./images/user.png" align="absmiddle" border="0" width="25" height="25" /></A>
		  </td>
		  <td></td>
		  <td colspan="2"><TextArea id="overallNotes" name="overallNotes" rows="2" cols="25"><%=currentOverallNotes%></TextArea>
		</tr>
		</table>
</table>
			
 <input type="hidden" id="defaultBoardPK" name="defaultBoardPK" value="<%=defaultBoardPK%>" />
  <input type="hidden" id="fKReviewBoardGov" name="fKReviewBoardGov" value="<%=fKReviewBoardGov%>" />
    	
<div class="popDefault" style="width:100%">
<table width="100%"><tr><td></td><td>
<jsp:include page="submitBar.jsp" flush="true"> 
  <jsp:param name="displayESign" value="Y"/>
  <jsp:param name="formID" value="mainForm"/>
  <jsp:param name="showDiscard" value="N"/>
</jsp:include>
</td></tr></table>
</div>
<%    
} 
else { // session invalid
%>
<jsp:include page="timeout_childwindow.jsp" flush="true"/>
<%
}
%>
  <div class = "myHomebottomPanel">
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
</form>
</body>
</html>