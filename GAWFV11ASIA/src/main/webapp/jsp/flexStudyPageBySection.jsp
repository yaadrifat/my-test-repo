<%--
This JSP inherited most of the logic from patientschedule.jsp. This JSP gets called 
by patientschedule.jsp via AJAX for a particular visit and returns the events in that visit.
 --%>
<%@page import="com.velos.eres.widget.service.util.FlxPageSections"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="grpRights" scope="request" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="studyJB" scope="session" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="altId" scope="request" class="com.velos.eres.web.studyId.StudyIdJB"/>
<%@page import="com.velos.eres.business.common.*,com.velos.eres.service.util.*"%>
<%@page import="com.velos.eres.web.studyRights.StudyRightsJB"%>
<%@page import="com.velos.eres.web.grpRights.GrpRightsJB"%>
<%@page import="com.velos.eres.web.study.StudyJB"%>
<%@page import="com.velos.esch.business.common.*"%>
<%@page import="com.velos.eres.widget.service.util.FlxPageCache,com.velos.eres.widget.service.util.FlxPageFields"%>
<%@page import="com.velos.eres.widget.service.util.FlxPageFieldHtml"%>
<%@page import="java.util.*,java.io.*"%>
<%@page import="org.json.*,org.w3c.dom.*"%>
<%
String jPageAvailSections = FlxPageCache.getStudyPageSections();
JSONArray jsSectionArray = (StringUtil.isEmpty(jPageAvailSections)? new JSONArray() :  new JSONArray(jPageAvailSections));
%>
<script>
var showSectionProgressBar = function (sectionNo, fldWDataCount){
	var secFldCount = parseInt($j("#secFldCount"+sectionNo).val());
	
	//console.log("Section: "+sectionNo+" secFldCount: "+secFldCount+ " fldWDataCount: "+fldWDataCount);
	$j("#secFldWDataCount"+sectionNo).val(fldWDataCount);
	$j("#secFldWDataCountTD"+sectionNo).html(fldWDataCount);

	$j( "#progressBar" + sectionNo).progressbar({ max: secFldCount, value: fldWDataCount });
	var progressbar = $j( "#progressBar" + sectionNo ),
      	progressbarValue = progressbar.find( ".ui-progressbar-value" );
	
	progressbarValue.css({
         "background": '#87ceeb'
       });
	progressbar.css({
		"height": "7px"
       });
}
</script>
<script src="./js/velosCustom/flexStudySectionScreen.js"></script>
<script language="JavaScript" src="formjs.js"><!-- FORM JS--></script>
  <%
   HttpSession tSession = request.getSession(true);
   if (sessionmaint.isValidSession(tSession))
   {

    int sectionNo = StringUtil.stringToNum(request.getParameter("sectionNo"));

    String defUserGroup = (String) tSession.getAttribute("defUserGroup");

	int formLibSeq = 0;
	char formLibAppRight = '0';
	//put the values in session
	String studyId = request.getParameter("studyId");
	if(studyId == null || studyId.equals("null")) {
		studyId = (String) tSession.getAttribute("studyId");
		studyJB = null;
	} else 	{
		if (StringUtil.stringToNum(studyId) < 1){
			studyId="0";
			studyJB = null;
		} else {
			studyJB = new StudyJB();
			studyJB.setId(StringUtil.stringToNum(studyId));
			studyJB.getStudyDetails();
		}
		tSession.setAttribute("studyId",studyId);
	}
	
	String usr = (String) tSession.getAttribute("userId");
	int accountId = StringUtil.stringToNum((String)tSession.getAttribute("accountId"));
    int pageRight = 0;

    if (StringUtil.stringToNum(studyId) == 0){
    	grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
   		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));
    } else {
		//******************GET STUDY TEAM RIGHTS and put the same in session, this will be a separate obj as no study is selected //***************************************************************
	    TeamDao teamDao = new TeamDao();
	    teamDao.getTeamRights(StringUtil.stringToNum(studyId),StringUtil.stringToNum(usr));
	    ArrayList tId = teamDao.getTeamIds();
	    if (tId.size() == 0)
		{
	    	pageRight=0 ;
	    }
		else
		{
	    	stdRights.setId(StringUtil.stringToNum(tId.get(0).toString()));
	   	 	ArrayList teamRights ;
			teamRights  = new ArrayList();
			teamRights = teamDao.getTeamRights();
	
			stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
			stdRights.loadStudyRights();
	
	    	tSession.setAttribute("studyRights",stdRights);
	    	if ((stdRights.getFtrRights().size()) == 0)
			{
	    	 	pageRight= 0;
	    	}
			else
			{
	    		pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYSUM"));
	    	}
	    }
    }

	if (StringUtil.isAccessibleFor(pageRight, 'V')) {
		if(sectionNo > 0) {
			JSONObject jsSectionObject = jsSectionArray.getJSONObject(sectionNo-1);

			String secType = "";
			String title = "";
			try{
				secType = jsSectionObject.getString("secType");
				title = jsSectionObject.getString("title");
			} catch (JSONException e){
				secType = "generic";
			}
			secType = (StringUtil.isEmpty(secType))? "generic" : secType;

			if ("predef".equals(secType)){
				System.out.println("predefined");
				String secModule = "";
				try{
					secModule = jsSectionObject.getString("secModule");
					if (StringUtil.isEmpty(secModule)){
						return;
					}
					if (secModule.equals((FlxPageSections.STUDY_STUDYTEAM).name())){
						%>
						<jsp:include page="flexStudyPageSectStudyTeam.jsp" >
							<jsp:param name="studyId" value="<%=studyId%>"/>
							<jsp:param name="sectionNo" value="<%=sectionNo%>"/>
						</jsp:include>
						<%
					}
					if (secModule.equals((FlxPageSections.STUDY_STUDYINDIDE).name())){
						%>
						<jsp:include page="flexStudyPageSectStudyIndIde.jsp" >
							<jsp:param name="studyId" value="<%=studyId%>"/>
							<jsp:param name="sectionNo" value="<%=sectionNo%>"/>
						</jsp:include>
						<%
					}
				} catch (JSONException e){
					secModule = null;
					return;
				}
				%>
				<script>
					var sectionNo = <%=sectionNo%>;
					$j("#secFldCountTD"+sectionNo).html("");
					$j("#secFldWDataCountTD"+sectionNo).html("");
				</script>
				<%
			}
			if ("generic".equals(secType)){
				JSONArray jsSectFldArray = null;
				try {
					jsSectFldArray = jsSectionObject.getJSONArray("fields");
				} catch(JSONException e) {}
				jsSectFldArray = (null == jsSectFldArray)? new JSONArray() : jsSectFldArray;
	
				StudyIdDao sidDao = new StudyIdDao();
				sidDao = altId.getStudyIds(StringUtil.stringToNum(studyId),defUserGroup);
			%>
			<div id="tableContainer<%=sectionNo%>">
				<TABLE class="basetbl" border="0" width="100%">
			<%
				int rowCount =0;
				int fldWDataCount = 0;
				int labelOnlyCount=0;
				for(int i=0; i < jsSectFldArray.length(); i++) {
					JSONObject jsSectFldObject = jsSectFldArray.getJSONObject(i);
					String fieldType = jsSectFldObject.getString("type");
					String flxFieldId = (jsSectFldObject.get("flxFieldId")).toString();
					if (StringUtil.isEmpty(flxFieldId) && !"studySection".equals(fieldType)) {
						continue;
					}					
					if("Medical and Investigational Devices".equals(title)) {						
						String labelonly = jsSectFldObject.getString("labelOnly");
						if( labelonly != null ) {
							if(Integer.parseInt(labelonly) == 1) {
								labelOnlyCount++;
							}
						} 
					}		
					if(rowCount%2==0) {%>
					<TR class="">
					<% } else { %>
					<TR class="">
					<%}%>
					<%if ("STD".equals(fieldType)){ %>
						<%
						FlxPageFields enumFlxPageField = FlxPageFields.getFlxPageFieldByKey(flxFieldId);
						FlxPageFieldHtml flxPageFieldHtml = new FlxPageFieldHtml();
						JSONObject fldHTMLJON = flxPageFieldHtml.getPageFieldHtmlStd(enumFlxPageField, studyJB, tSession);

						String fldHTML = "", fldValue = "";
						if (null != fldHTMLJON){
							fldHTML = fldHTMLJON.getString("fldHTML");
							fldValue = fldHTMLJON.getString("fldValue");
						}
						if (!StringUtil.isEmpty(fldValue)) fldWDataCount++;
						%>
						<%=fldHTML%>
					<%} %>
					<%if ("MSD".equals(fieldType)){ %>
						<%
						FlxPageFieldHtml flxPageFieldHtml = new FlxPageFieldHtml();
						JSONObject fldHTMLJON = flxPageFieldHtml.getFlxPageFieldHtmlMd("STUDY", flxFieldId, sidDao);

						String fldHTML = "", fldValue = "", fldType ="";
						if (null != fldHTMLJON){
							fldHTML = fldHTMLJON.getString("fldHTML");
							fldValue = fldHTMLJON.getString("fldValue");
							fldType = fldHTMLJON.getString("fldType");
						}
						if (!StringUtil.isEmpty(fldValue) || "splfld".equals(fldType)) fldWDataCount++;					
						%>
						<%=fldHTML%>
					<%} %>
					<%if ("studySection".equals(fieldType)){ %>
						<%
						FlxPageFieldHtml flxPageFieldHtml = new FlxPageFieldHtml();
						JSONObject fldHTMLJON = flxPageFieldHtml.getFlxPageStudySectionHtml(flxFieldId, tSession, studyJB);
						String fldHTML = "", fldValue = "";
						if (null != fldHTMLJON){
							fldHTML = fldHTMLJON.getString("fldHTML");
							fldValue = fldHTMLJON.getString("fldValue");
						}
						if (!StringUtil.isEmpty(fldValue)) fldWDataCount++;
						%>
						<%=fldHTML%>
					<%} %>
					</TR>
				<%} 
				fldWDataCount = fldWDataCount - labelOnlyCount; %>
				</TABLE>
			</div>
			<div id="hiddenData<%=sectionNo%>" style="display:none"></div>
			<script>
				showSectionProgressBar(<%=sectionNo%>, <%=fldWDataCount%>);
				jQuery(document).ready(openCal);
				moreStudyDetFunctions.fixTextAreas();
			</script>
			<%} %>
		<%} else if (sectionNo <= 0){%>
		<P class="defComments">Invalid Section Number</P>
		<% } %>
	<% } else {%>
	  <jsp:include page="accessdenied.jsp" flush="true"/>
	<% }
} //end of if session times out
else
{
%>
	<jsp:include page="timeout.html" flush="true"/>
<%
} //end of else body for page right
%>
