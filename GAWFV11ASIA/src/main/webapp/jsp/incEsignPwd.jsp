<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@page import="com.velos.eres.service.util.Configuration,com.velos.eres.service.util.MC" %>
<p class = "sectionHeadings">
<% if ("userpxd".equals(Configuration.eSignConf)) {%>
<%=MC.M_EtrWrongPassword_CLkBack%><%--You have entered a wrong e-Password. Please click on Back button to enter again.*****--%>
<%}
else{ %>
<%=MC.M_EtrWrongEsign_ClkBack%><%--You have entered a wrong e-Signature. Please click on Back button to enter again.*****--%>
<%} %>
</p>