<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC,com.velos.eres.service.util.DateUtil"%> 
<%@page import="org.json.JSONObject,org.json.JSONArray" %>
<%@page import="com.velos.eres.widget.service.util.FlxPageCache"%>
<%@page import="com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.FlxPageUtil"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%
HttpSession tSession = request.getSession(false); 
if (!sessionmaint.isValidSession(tSession)) {
	return;
}
String userId = (String)tSession.getAttribute("userId");
FlxPageCache.reloadStudyPage();

JSONArray jPageAvailFields = FlxPageCache.getStudyPageFieldRegistryForConfig(tSession);
JSONArray jPagePredefSections = FlxPageCache.getStudyPageSectionRegistryForConfig();

int accId = StringUtil.stringToNum((String)tSession.getAttribute("accountId"));
JSONArray jPageAvailStudySections = FlxPageCache.getStudyPageStudySectionRegistryForConfig(accId);

String jPageAvailSections = FlxPageCache.getStudyPageSections();
%>
<link rel="stylesheet" type="text/css" href="styles/flexPage.css">

<script>
var pageFieldRegistry = <%=jPageAvailFields.toString()%>;
var pageSectionRegistry = <%=jPageAvailSections.toString()%>;
var pageStudySectionRegistry = <%=jPageAvailStudySections.toString()%>;
var activeSection = 0;
var toggleRegistryListImgGlobal;

var deleteField = function(openingImg, secIndex, fieldNo){
	if((!openingImg) || secIndex < 0 || fieldNo < 0) return;

	var sectionObject = new Object(pageSectionRegistry[secIndex]);
	var sectionFldArray = sectionObject['fields'];
	sectionFldArray = (!sectionFldArray)? [] : sectionFldArray;
	if (sectionFldArray.length <= 0) return;
	
	var fieldObject = sectionFldArray[fieldNo-1]; 
	var fieldType = fieldObject['type'];

	var confirmText = '';
	if (fieldType != 'studySection'){
		confirmText =  'Are you sure you want to delete field "'+fieldObject['label']+'"?';
	}else {
		confirmText =  'Are you sure you want to delete Study Section "'+fieldObject['label']+'"?';
	}

	if(confirm(confirmText)){
		sectionFldArray.splice(fieldNo-1, 1);
		sectionObject['fields'] = sectionFldArray;
		
		pageSectionRegistry[secIndex] = sectionObject;
		saveSections();

		/*var fieldKeyword = fieldObject['flxFieldId'];

		if (fieldType && fieldKeyword){
			$j("[key='velos-"+fieldType+"-"+ fieldKeyword +"']").removeClass('gdt-glist-disabled').removeClass('yui3-dd-dragging');
		}*/
	}
};

var sectionHeaderClick = function(secIndex){
	activeSection = secIndex;
	if (secIndex < 0) return;

	var sectionNo = secIndex+1;
	if (document.getElementById('sectionFldTbl'+ sectionNo)){
		return;
	}

	var sectionFieldsHTML = '';
	var sectionObject = (pageSectionRegistry[secIndex]);
	var secType = sectionObject['secType'];
	secType = (!secType)? "generic" : secType;
	
	if (secType == 'predef'){
		activeSection = -1;
		sectionFieldsHTML = '<p>Fields cannot be added to a predefined section.</p>';
		$j('#sectionBody'+sectionNo).html(sectionFieldsHTML);
		return;
	}
	
	var sectionFldArray = sectionObject['fields'];
	sectionFldArray = (!sectionFldArray)? [] : sectionFldArray;

	sectionFieldsHTML ='<br/>'
	+'<table id="sectionFldTbl'+ sectionNo +'" class="baseTbl outline" width="80%">'
	+'<tr>'
	+'<th width="5%">Sequence</th>'
	+'<th width="25%">Field Keyword</th>'
	+'<th width="25%">Field Label</th>'
	+'<th width="5%">Delete</th>'
	+'</tr>';
	
	if (sectionFldArray.length == 0){
		sectionFieldsHTML+='<tr>'
		+'<td colspan="3">No fields/ study sections selected</td>'
		+'</tr>'
	} else{
		for (var indx=0; indx < sectionFldArray.length; indx++){
			var fieldObject = sectionFldArray[indx];

			sectionFieldsHTML+='<tr ';
			if (indx%2 == 0){
				sectionFieldsHTML+='class="browserEvenRow"';
			} else {
				sectionFieldsHTML+='class="browserOddRow"';
			}
			sectionFieldsHTML+='>'
			+'<td width="5%">'+(indx+1)+'</td>'
			+'<td width="25%">'+fieldObject.flxFieldId+'</td>'
			+'<td width="25%">'+fieldObject.label+'</td>';
			
			if (fieldObject.type != 'studySection'){
				if (fieldObject.flxFieldId != 'STUDY_STUDYNUMBER' && fieldObject.flxFieldId != 'STUDY_STUDYTITLE' && fieldObject.flxFieldId != 'STUDY_DATAMANAGER'){
					sectionFieldsHTML+='<td width="5%"><img id="deleteFldImg'+ sectionNo +'" align="center" src="./images/delete.gif" alt="" class="headerImage" onclick="deleteField(this, '+secIndex+ ',' + (indx+1) +'); "/></td>';
				} else {
					sectionFieldsHTML+='<td width="5%"></td>';
				}
			} else {
				sectionFieldsHTML+='<td width="5%"><img id="deleteFldImg'+ sectionNo +'" align="center" src="./images/delete.gif" alt="" class="headerImage" onclick="deleteField(this, '+secIndex+ ',' + (indx+1) +'); "/></td>';
			}

			sectionFieldsHTML+='</tr>';
		}
	}
	sectionFieldsHTML+='</table><br/>';
	$j('#sectionBody'+sectionNo).html(sectionFieldsHTML);
};

var checkDuplicateSectionModule =  function (secIndex, secModule){
	if(secIndex < 0) return true;
	
	for(var indx=0; indx < pageSectionRegistry.length; indx++){
		if (indx == secIndex) continue;

		var sectionJSObject = pageSectionRegistry[indx];
		if (sectionJSObject.secType == 'predef' && sectionJSObject.secModule == secModule){
			return true;
		}
	}
	return false;
};

var setSectionTitle =  function(){
	var secIndex = $j('#editSecIndex').val();
	if(secIndex < 0) return;
	
	var sectionNo = (secIndex+1);
	var sectionObject = pageSectionRegistry[secIndex];
	
	var secType = $j('#editSectionType').val();
	secType = (!secType)? "generic" : secType;
	
	if (secType != 'predef'){
		delete sectionObject['secModule'];
	} else {
		if (secType == 'predef'){
			delete sectionObject['fields'];
			var secModule = $j('#editSectionModule').val();
			if (checkDuplicateSectionModule(secIndex, secModule)){
				alert('Cannot define multiple sections of the type-' + secModule);
				return false;
			}
		}
	}
	//Control reaches here only if the section values look right 
	$j("#sectionTitleSpan"+sectionNo).children("input[id*='sectionTitle']").val($j('#editSectionTitle').val());

	sectionObject['title'] = $j('#editSectionTitle').val();
	pageSectionRegistry[secIndex] = sectionObject;
	sectionObject['secType'] = secType;
	if (secType == 'predef'){
		sectionObject['secModule'] = secModule;
	}
	$j('#editSectDiv').dialog('close');

	saveSections();
};

var editSection = function(openingImg, secIndex){
	if((!openingImg) || secIndex < 0) return;
	var sectionObject = {};
	sectionObject = new Object(pageSectionRegistry[secIndex]);

	$j('#editSecIndex').val(secIndex);
	$j('#editSectionTitle').val(sectionObject['title']);
	
	var secType = sectionObject['secType'];
	secType = (!secType || secType.length == 0)? "generic" : secType;
	$j('#editSectionType').val(secType);

	if (secType == 'predef'){
		var secModule = sectionObject['secModule'];
		secModule = (!secModule || secModule.length == 0)? "" : secModule;
		$j('#editSectionModule').val(secModule);	
	}
	$j('#editSectionType').change();

	$j('#editSectDiv').dialog({
		minHeight: 150,
		maxHeight: 200,
		maxWidth: 600,
		minWidth: 400,
		closeText:'',
		modal: true
	}).dialog("widget").position({
		my: 'left', 
		at: 'right bottom-20',
		of: $j('#'+openingImg.id)
	});
	$j('#editSectionTitle').focus();
};

var deleteSection = function(openingImg, secIndex){
	if((!openingImg) || (secIndex < 0)) return;
	var sectionObject = new Object(pageSectionRegistry[secIndex]); //Section being deleted

	if(confirm('Are you sure you want to delete Section "'+sectionObject['title']+'"?\n\nThe fields included in this section will be deleted.')){
		pageSectionRegistry.splice(secIndex, 1);	
		saveSections();
	}
};

var saveSections = function (){
	var pageId = '<%=FlxPageUtil.getFlexStudyPageName()%>';
	if (!pageId || pageId == '') return;

	var modPageSectionRegistry = JSON.parse(JSON.stringify(pageSectionRegistry));
	for (var iSection = 0; iSection < modPageSectionRegistry.length; iSection++){
		var sectionObject = new Object(modPageSectionRegistry[iSection]);
		
		var sectionFldArray = new Object(sectionObject['fields']);
    	sectionFldArray = (!sectionFldArray || sectionFldArray.length <= 0)? [] : sectionFldArray;
    	if (sectionFldArray.length <= 0){
    		delete fieldObject['fields'];
    		continue;
    	}
    	
		for (var iField = 0; iField < sectionFldArray.length; iField++){
	    	var fieldObject = new Object(sectionFldArray[iField]); 
	        delete fieldObject['seq'];
			delete fieldObject['label'];
			
			sectionFldArray[iField] = fieldObject;
		}
		if (sectionFldArray && sectionFldArray.length > 0){
			sectionObject['fields'] = sectionFldArray;
		}
		modPageSectionRegistry[iSection] = sectionObject;
	}

	var list = {};
	list = {name: 'Study Screen', sections: modPageSectionRegistry};
	
	//JSON encode the cookie data and save it in DB
	var cookie = JSON.stringify(list);
	//alert(cookie);
	jQuery.ajax({
   		url:'flexPageCookie.jsp',
   		async: true,
   		cache: false,
   		type: 'POST',
   		data: {
   			pageId: pageId,
   			type: 'typeSaveCookie',
   			cookie: cookie
   		},
   		success: function(resp) {
   			resetSessionWarningOnTimer();
   			renderSections();
   		}
   	});	
};

var reorderSections = function( event, ui ) {
    // IE doesn't register the blur when sorting
    // so trigger focusout handlers to remove .ui-state-focus
    ui.item.children( "h3" ).triggerHandler( "focusout" );
    
    var reorderedSectionRegistry = [];
    
    var sectionsReordered = false;
    var oldIndx = 0, newIndx = 0;
    var sectionSeqs = document.getElementsByName('sectionSeq');
    for (var indx=0; indx < sectionSeqs.length; indx++){
    	oldIndx = sectionSeqs[indx].value;
    	newIndx = indx+1;

    	if (oldIndx != newIndx){
    		sectionSeqs[indx].value = newIndx;

    		var sectionJSObject = pageSectionRegistry[oldIndx-1];
    		sectionJSObject['seq'] = newIndx;
    		reorderedSectionRegistry[indx] = sectionJSObject;

    		sectionsReordered = true;
    	} else {
    		reorderedSectionRegistry[indx] = pageSectionRegistry[indx];
    	}
    }
    if (sectionsReordered){
	    pageSectionRegistry = reorderedSectionRegistry;
    	saveSections();
    }
};
  
var createSectionAccordion =  function(){
	$j('#sectionAccordion')
    .accordion('destroy')
    .accordion({
		header: "> div > h3",
		autoHeight: false, collapsible: true, active: false, fillSpace: false, animated: false
	}).sortable({
        axis: "y",
        handle: "h3",
        stop: reorderSections
    });
};

var renderSections = function(){
	$j('#sectionAccordion').html('');
	var additionalSectionHTML = '';
	for (var indx=0; indx < pageSectionRegistry.length; indx++){
		var sectionNo = indx+1;
		var sectionObject = new Object(pageSectionRegistry[indx]);
		additionalSectionHTML += '<div id="sectionHead'+ sectionNo +'" class="group" onClick="sectionHeaderClick('+indx+');">'
		+'<h3 id="sectionHeader'+ sectionNo +'"><input type="hidden" id="sectionSeq" name="sectionSeq" value="'+ sectionNo +'"/>'
		+'<a id="sectionAnchor'+ sectionNo +'" href="#">'+sectionObject.title+'&nbsp;&nbsp;'
		+'<span id="sectionTitleSpan'+ sectionNo +'" style="display:none"><input type="text" id="sectionTitle" name="sectionTitle" value="'+sectionObject.title+'"/></span>&nbsp;'
		+'<img id="editSectImg'+ sectionNo +'" src="./images/edit_icon.png" alt="" class="headerImage" onclick="editSection(this, '+indx+'); "/>';

		var secSpecial = sectionObject['secSpecial'];
		if (!secSpecial){
			additionalSectionHTML += '<img id="deleteSectImg'+ sectionNo +'" align="right" src="./images/delete.gif" alt="" class="headerImage" onclick="deleteSection(this, '+ indx +'); "/>';
		}

		additionalSectionHTML += '</a>'
		+'</h3>'
		+'<div id="sectionBody'+ sectionNo +'"><p>Loading...</p>'
		+'<img class="asIsImage_progbar" src="../images/jpg/loading_pg.gif"/><br/>'
		+'</div>'
		+'</div>';
	}
	$j('#sectionAccordion').append(additionalSectionHTML);
	createSectionAccordion();

	openSection(activeSection);
};

var openSection = function (secIndex){
	sectionHeaderClick(secIndex);
	$j('#sectionAccordion').accordion({active: secIndex});
};

var addSections = function(){
	var addSectionCount = $j('#addSectionCount').val();
	
	var secCount = pageSectionRegistry.length;
	for(indx=0; indx < addSectionCount; indx++){
		secCount++;
		pageSectionRegistry[secCount-1] = {
			'seq': secCount,
			'title': 'New Section',
			'secType': 'generic' 
		};	
	}
	saveSections();
};

</script>

<div id="doc3" class="yui-skin-sam yui-dt yui-dt-liner yui3-skin-sam yui-t2 yui3-loading" onmouseout='return nd();'>
<jsp:include page="ui-include.jsp" flush="true" />
<!-- End of gadget include JSP's -->

    <div id="pd" onmouseover='return nd();' onmouseout='return nd();'>
    	<table width="98%">
	    	<tr>
	    		<td>
			        <a id="fieldListLink" href="javascript:if(toggleRegistryListImgGlobal) {toggleRegistryListImgGlobal();}"> 
			            <img id="toggleGadgetList" title="<%=MC.M_Ggt_HideList%>" src="../images/jpg/ListCollapse.jpg" style="border:none;"/>
			        </a>
			    </td>
			    <td align="right">
	        		&nbsp;<input id="addSectionCount" type="text" value="1" size="2" maxlength="2" class="index" onkeydown = "return validateNumber(event);"/> New Section(s)
					<img id="addRows" name="addRows" title="<%=LC.L_Add%>" src="../images/add.png" onClick="addSections();"/> 
				</td>
			</tr>
		</table>
    </div>
    <div id="bd" onmouseout='return nd();'>
        <div id="yui-main" class="yui-g" style="margin-top:0em">
             <div id="play" style="margin-top:0em" onmouseout='return nd();'>
                 <ul style="float:left; " id="list0" >
                 <li style="list-style-type:none; " >
                  <div id="fieldList" >
                     <div id="feedsSTD" class="flxPageFeeds gdt-glist" >
	                     <p>Standard Fields</p>
	                     <ul id="STDFields" class="gdt-glist-enabled"></ul>
                     </div>
                     <br/>
                     <div id="feedsMSD" class="flxPageFeeds gdt-glist" >
	                     <p>More Study Details Fields</p>
	                     <ul id="SMDFields" class="gdt-glist-enabled"></ul>
                     </div>
                     <br/>
                     <div id="feedsStudySections" class="flxPageFeeds gdt-glist" >
	                     <p>Study Sections</p>
	                     <ul id="StudySections" class="gdt-glist-enabled"></ul>
                     </div>
                 </div>
                 </li>
                 </ul>
                 <ul class="list" id="list1" onmouseout='return nd();'>
                 <div id="sectionAccordion" class="flxPageContainer" width="97%"></div>
                 </ul>
             </div>
	    </div>
    </div>
    <div id="ft" onmouseout='return nd();'>
        <a href="#" id="end_of_gadget_home"></a>
        <input id="esign" name="esign" type="hidden" ></input> 
    </div>
</div>

<script>
jQuery(function() {
	renderSections();
	$j('#editSectionType').change(function(){
		var editSectionType = $j('#editSectionType').val();
		editSectionType = (!editSectionType)? "generic" : editSectionType;
		if (editSectionType == "generic"){
			$j('#editModuleRow').hide();
			$j('#editSectionModule').hide();
		} else {
			$j('#editModuleRow').show();
			$j('#editSectionModule').show();
		}
	});
	$j('#editSectionType').change();
	$j('#submitSectDiv').click(	function (){
		setSectionTitle(); 
	});
});
//Use loader to grab the modules needed
YUI().use('panel', 'dd-plugin', 'dd', 'anim', 'yql', 'cookie', 'json', 'node', 
		'io', 'io-base', 'overlay', 'event', 'widget-anim', function(Y) {
    //Make this an Event Target so we can bubble to it
    var Portal = function() {
        Portal.superclass.constructor.apply(this, arguments);
    };
    Portal.NAME = 'portal';
    Y.extend(Portal, Y.Base);
    //This is our new bubble target..
    Y.Portal = new Portal();
    
    // Handle toggle gadget list
    var toggleRegistryListImg = function(e) {
    	var myAnim = new Y.Anim({
    		node: Y.one('#list0'),
    		from: { width: function(node) { if (node._node.clientWidth > 100) { return '15%'; }  return '0%'; } },
    		to: { width: function(node) { if (node._node.clientWidth > 100) { return '0%'; }  return '15%'; } },
    		duration: '.50',
            easing: Y.Easing.easeOut
    	});
    	
		myAnim.run();
    	if ( $j('#toggleGadgetList').attr('src') == '../images/jpg/ListCollapse.jpg' ) {
    		setTimeout(hideGadgetListImg, 170)
    	} else {
    		setTimeout(showGadgetListImg, 50);
    	}
    	
    };
    toggleRegistryListImgGlobal = toggleRegistryListImg;
    var hideGadgetListImg = function() {
		$j("ul.list").css({width:"98%"});
		$j('#toggleGadgetList').attr('src', '../images/jpg/ListExpand.jpg');
		$j('#toggleGadgetList').attr('title', "<%=MC.M_Ggt_ShowList%>");
		Y.one('#list0').hide();
    }
    
    var showGadgetListImg = function() {
		$j('#toggleGadgetList').attr('src', '../images/jpg/ListCollapse.jpg');
		$j('#toggleGadgetList').attr('title', "<%=MC.M_Ggt_HideList%>");
		Y.one('#list0').show();
		$j("ul.list").css({width:"82%"});
    }
    
    Y.one('#toggleGadgetList').on('click', toggleRegistryListImg);
    if ( $j('#toggleGadgetList').attr('src') == '../images/jpg/ListCollapse.jpg' ) {
    	hideGadgetListImg();
    	$j("ul.list").css({width:"98%"});
    }

    //Setup some private variables..
    var goingUp = false, lastY = 0, trans = {};

    //Simple method for stopping event propagation
    //Using this so we can detach it later
    var stopper = function(e) {
        e.stopPropagation();
    };
 
    //Get the order, placement and minned state of the modules and save them to a cookie
    var _setCookies = function() {        
    	alert('in setCookies');
        //JSON encode the cookie data and save it in DB
        saveSections();
    };
    
    var _isMaximized = function() {
    	var isMaxed = false;
    	try {
    		isMaxed = Y.one('#list1').getStyle('width') == '98%';
    	} catch(e) {}
    	return isMaxed;
    }
 
    //Helper method for creating the feed DD on the left
    var _createFeedDD = function(node, data) {
        //Create the DD instance
        var dd = new Y.DD.Drag({
            node: node,
            data: data,
            bubbles: Y.Portal
        }).plug(Y.Plugin.DDProxy, {
            moveOnEnd: false,
            borderStyle: 'none'
        });
        //Setup some stopper events
        dd.on('drag:start', _handleStart);
        dd.on('drag:end', stopper);
        dd.on('drag:drophit', stopper);
    };
    
    // Helper method to handle click event on the max icon
    var _nodeClickMax = function(div) {
    	var anim1 = new Y.Anim({
            node: '#list1'
        });
    	
    	//if (Y.one('#list1').getStyle('width') != '98%') {
    		hideGadgetListImg();
    		anim1.setAttrs({
    			from: { width: function(node) { return '98%'; } },
                to: { width: function(node) { return '98%'; } },
                duration: '.50',
                easing: Y.Easing.easeOut,
                iteration: 1
            });
    		anim2.setAttrs({
    			from: { width: function(node) { return '98%'; } },
                to: { width: function(node) { return '98%'; } },
                duration: '.50',
                easing: Y.Easing.easeOut,
                iteration: 1
            });
    		anim2.on('end', function() {
    			alert('anim2 end');
    	    	_setCookies();
    			var myNode = div.get('parentNode');
    			var myMatches = myNode.get('className').match(/\bvelos-[\S]+\b/g);
    			var gadgetId = null;
    			if (myMatches) {
    				gadgetId = myMatches[0].substring(6);
        			$('end_of_gadget_home').focus();
    				$('a_max_'+gadgetId).focus();
    			}
    		});
    	//} 
		anim1.run();
		anim2.run();
    };
    
    //This creates the module, either from a drag event or from the cookie load
    var setupModDD = function(mod, data, dd) {
        var node = mod;
        node.addClass('velos-'+data.id);
        
        //Remove the event's on the original drag instance
        dd.detachAll('drag:start');
        dd.detachAll('drag:end');
        dd.detachAll('drag:drophit');
        
        //It's a target
        dd.set('target', true);
        //Setup the handles
        dd.addHandle('h2').addInvalid('a');
        
        //Remove the mouse listeners on this node
        dd._unprep();
        //Update a new node
        dd.set('node', mod);
        //Reset the mouse handlers
        dd._prep();
 
    };
 
    //Helper method to create the markup for the module..
    var _createMod = function(feed) {
    	var myLabel = feed.label ? feed.label : '';
    	var myFlxFieldId = feed.flxFieldId ? feed.flxFieldId : '';
        var str = '<tr>'
        	+'<td></td>'
        	+'<td>'+ myFlxFieldId +'</td>'
        	+'<td>'+ myLabel +'</td>'
        	+'</tr>';
        return Y.Node.create(str);
    };
    
    var _parseCookie = function(obj) {
        //Walk the data
        Y.each(obj, function(v, k) {
            //Get the node for the list
            var list = Y.one('#' + k);
            if (k == 'misc') {
            	if (v[0].max) {
            		hideGadgetListImg();
            		Y.one('#list1').setStyle('width', '98%');
            	}
            }
            //Walk the items in this list
            Y.each(v, function(v2, k2) {
                //Get the drag for it
                var drag = Y.DD.DDM.getDrag('#' + v2.id);
                if (!drag || !drag.get('data')) return;
                //Create the module
                var mod = _createMod(drag.get('data'));
                if (v2.min) {
                    //If it's minned add some CSS
                    mod.one('div.mod').addClass('minned');
                    mod.one('div.inner').setStyle('height', '0px');
                }
                //Add it to the list
                list.appendChild(mod);
                //Set the drag listeners
                drag.get('node').addClass('gdt-glist-disabled');
                drag.set('node', mod);
                drag.set('dragNode', Y.DD.DDM._proxy);
                drag.detachAll('drag:start');
                drag.detachAll('drag:end');
                drag.detachAll('drag:drophit');
                drag._unprep();
                //Setup the new Drag listeners
                setupModDD(mod, drag.get('data'), drag);
            });
        });
    };
    

    //Handle the start Drag event on the left side
    var _handleStart = function(e) {
        //Stop the event
        stopper(e);

        //Some private vars
        var drag = this,
            list3 = Y.one('#sectionFldTbl'+(activeSection+1)),
            mod = _createMod(drag.get('data'));

        //Add it to the first list
        list3.appendChild(mod);

        //Set the item on the left column disabled.
        drag.get('node').addClass('gdt-glist-disabled');
        //Set the node on the instance
        drag.set('node', mod);
        //Add some styles to the proxy node.
        drag.get('dragNode').setStyles({
            opacity: '.5',
            borderStyle: 'none',
            width: '320px',
            height: '61px'
        });
        //Update the innerHTML of the proxy with the innerHTML of the module
        var srcId = drag.get('data').flxFieldId;
        drag.get('dragNode').set('innerHTML', drag.get('node').get('innerHTML'));
        //set the inner module to hidden
        //drag.get('node').one('div.mod').setStyle('visibility', 'hidden');
        //add a class for styling
        drag.get('node').addClass('moving');
        drag.get('node').addClass('velos-'+srcId);
        //Setup the DD instance
        setupModDD(mod, drag.get('data'), drag);
 
        //Remove the listener
        this.detach('drag:start', _handleStart);
        
        var feed = drag.get('data');
        var fieldObject = null;
        if (feed.type != 'studySection'){
        	fieldObject = pageFieldRegistry[feed.seq];
        } else {
        	fieldObject = pageStudySectionRegistry[feed.seq];
        }
        //alert(activeSection + ' ' + feed.seq + ' ' + JSON.stringify(fieldObject));
        
        var sectionObject = new Object(pageSectionRegistry[activeSection]);
    	var sectionFldArray = sectionObject['fields'];
    	sectionFldArray = (!sectionFldArray || sectionFldArray.length <= 0)? [] : sectionFldArray;

    	sectionFldArray.push(fieldObject);
    	sectionObject['fields'] = sectionFldArray;
    	pageSectionRegistry[activeSection] = sectionObject;
    };
    
    //Walk through the feeds list and create the list on the left
    var feedList = Y.one('#feedsSTD ul[id="STDFields"]');
    Y.each(pageFieldRegistry, function(v, k) {
    	if (v.type != 'STD') return;

		var li = "";
       	li = Y.Node.create('<li id="velos-fld-' + k + '" key="velos-STD-'+ v.flxFieldId +'">' + v.label + '</li>');
        feedList.appendChild(li);
        //Create the DD instance for this item
        _createFeedDD(li, v);
    });

    var feedList = Y.one('#feedsMSD ul[id="SMDFields"]');
    Y.each(pageFieldRegistry, function(v, k) {
    	if (v.type != 'MSD') return;

		var li = "";
       	li = Y.Node.create('<li id="velos-fld-' + k + '" key="velos-MSD-'+ v.flxFieldId +'">' + v.label + '</li>');
        feedList.appendChild(li);
        //Create the DD instance for this item
        _createFeedDD(li, v);
    });

    var feedList = Y.one('#feedsStudySections ul');
    Y.each(pageStudySectionRegistry, function(v, k) {
		var li = "";
       	li = Y.Node.create('<li id="velos-fld-' + k + '" key="velos-SEC-'+ v.flxFieldId +'">' + v.label + '</li>');
        feedList.appendChild(li);
        //Create the DD instance for this item
        _createFeedDD(li, v);
    });

    //This does the calculations for when and where to move a module
    var _moveMod = function(drag, drop) {
        if (drag.get('node').hasClass('item')) {
            var dragNode = drag.get('node'),
                dropNode = drop.get('node'),
                append = false,
                padding = 30,
                xy = drag.mouseXY,
                region = drop.region,
                middle1 = region.top + ((region.bottom - region.top) / 2),
                middle2 = region.left + ((region.right - region.left) / 2),
                dir = false,
                dir1 = false,
                dir2 = false;
                
                //We could do something a little more fancy here, but we won't ;)
                if ((xy[1] < (region.top + padding))) {
                    dir1 = 'top';
                }
                if ((region.bottom - padding) < xy[1]) {
                    dir1 = 'bottom';
                }
                if ((region.right - padding) < xy[0]) {
                    dir2 = 'right';
                }
                if ((xy[0] < (region.left + padding))) {
                    dir2 = 'left';
                }
                dir = dir2;
                if (dir2 === false) {
                    dir = dir1;
                }
                switch (dir) {
                    case 'top':
                        var next = dropNode.get('nextSibling');
                        if (next) {
                            dropNode = next;
                        } else {
                            append = true;
                        }
                        break;
                    case 'bottom':
                        break;
                    case 'right':
                    case 'left':
                        break;
                }
            
 
            if ((dropNode !== null) && dir) {
                if (dropNode && dropNode.get('parentNode')) {
                    if (!append) {
                        dropNode.get('parentNode').insertBefore(dragNode, dropNode);
                    } else {
                        dropNode.get('parentNode').appendChild(dragNode);
                    }
                }
            }
            //Resync all the targets because something moved..
            Y.Lang.later(50, Y, function() {
                Y.DD.DDM.syncActiveShims(true);
            });
        }
    };
 
    /*
    Handle the drop:enter event
    Now when we get a drop enter event, we check to see if the target is an LI, then we know it's out module.
    Here is where we move the module around in the DOM.    
    */
    Y.Portal.on('drop:enter', function(e) {
        if (!e.drag || !e.drop || (e.drop !== e.target)) {
            return false;
        }
        if (e.drop.get('node').get('tagName').toLowerCase() === 'li') {
            if (e.drop.get('node').hasClass('item')) {
                _moveMod(e.drag, e.drop);
            }
        }
    });
 
    //Handle the drag:drag event
    //On drag we need to know if they are moved up or down so we can place the module in the proper DOM location.
    Y.Portal.on('drag:drag', function(e) {
        var y = e.target.mouseXY[1];
        if (y < lastY) {
            goingUp = true;
        } else {
            goingUp = false;
        }
        lastY = y;
    });
 
    /*
    Handle the drop:hit event
    Now that we have a drop on the target, we check to see if the drop was not on a LI.
    This means they dropped on the empty space of the UL.
    */
    Y.Portal.on('drag:drophit', function(e) {
        var drop = e.drop.get('node'),
            drag = e.drag.get('node');
 
        if (drop.get('tagName').toLowerCase() !== 'li') {
            if (!drop.contains(drag)) {
                drop.appendChild(drag);
            }
        }
    });
 
    //Handle the drag:start event
    //Use some CSS here to make our dragging better looking.
    Y.Portal.on('drag:start', function(e) {
        var drag = e.target;
        if (drag.target) {
            drag.target.set('locked', true);
        }
        drag.get('dragNode').set('innerHTML', drag.get('node').get('innerHTML'));
        drag.get('dragNode').setStyle('opacity','.5');
       // drag.get('node').one('div.mod').setStyle('visibility', 'hidden');
        drag.get('node').addClass('moving');
    });
 
    //Handle the drag:end event
    //Replace some of the styles we changed on start drag.
    Y.Portal.on('drag:end', function(e) {
        var drag = e.target;
        if (drag.target) {
            drag.target.set('locked', false);
        }
        drag.get('node').setStyle('visibility', '');
        //drag.get('node').one('div.mod').setStyle('visibility', '');
        drag.get('node').removeClass('moving');
        drag.get('dragNode').set('innerHTML', '');

        saveSections();
    });
    
 
    //Handle going over a UL, for empty lists
    Y.Portal.on('drop:over', function(e) {
        var drop = e.drop.get('node'),
            drag = e.drag.get('node');
 
        if (drop.get('tagName').toLowerCase() !== 'li') {
            if (!drop.contains(drag)) {
                drop.appendChild(drag);
                Y.Lang.later(50, Y, function() {
                    Y.DD.DDM.syncActiveShims(true);
                });
            }
        }
    });
 
    //Create simple targets for the main lists..
    var uls = Y.all('#play ul.list');
    uls.each(function(v, k) {
        var tar = new Y.DD.Drop({
            node: v,
            padding: '20 0',
            bubbles: Y.Portal
        });
    });
    
    
    //Get the cookie data
    var cookie = null;
    /*var getGadgetCookie = function() {
    	jQuery.ajax({
    		url:'gadgetCookie.jsp',
    		data:{type:'typeGetCookie'},
    		async: false,
    		cache: false,
    		success: function(resp) {
    			resetSessionWarningOnTimer();
    			cookie = resp;
    			if (cookie == null || jQuery.trim(cookie) == 'null') { cookie = null; }
    			// if cookie is null the very first time, add Quick Access gadget 
    			if (!cookie) {
    				cookie = '{"list1":[{"id":"gadgetQuickAccess","min":false}]}';
    			}
    			var parsedCookie = Y.JSON.parse(cookie);
    			if (!parsedCookie.list1) {
    				showGadgetListImg();
    			}
    		}
    	});
    }();*/
    
    if (!cookie) { cookie = Y.Cookie.getSub('yui', 'portal'); }
    
    if (cookie) {
        //JSON parse the stored data
    	_parseCookie(Y.JSON.parse(cookie));
    }
}); // End of YUI().use
$('fieldListLink').focus();
$j('#feedsSTD').attr('style','max-height:100px;');
$j('#feedsMSD').attr('style','max-height:100px;');
$j('#feedsStudySections').attr('style','max-height:100px;');

$j('#list1').attr('style','min-height:450px;');
$j(document).ready(function(){
	var screenWidth = screen.width;
	var screenHeight = screen.height;

	if(screenHeight<=768){
		$j("div.flxPageContainer").css({height:"550px"});
	} else {
		$j("div.flxPageContainer").css({height:"770px"});
	}
});
</script>
<div id="editSectDiv" name="editSectDiv" title="Edit Section" style="display:none">
	<table width="100%">
		<tr>
			<td width="30%">Name</td>
			<td><input id='editSectionTitle' name='editSectionTitle' maxlength='100' size='50' value=''/></td>
		</tr>
		<tr>
			<td width="30%">Type &nbsp;
				<img class="headerImage" src="../images/jpg/help.jpg" onmouseover="return overlib('Fields and Section layout cannot be defined for predefined sections.',CAPTION,'Section Type(s)');" onmouseout="return nd();">
			</td>
			<td>
				<select id='editSectionType' name='editSectionType'>
					<option value="generic">Generic Section</option>
					<option value="predef">Predefined Section</option>
				</select>
			</td>
		</tr>
		<tr id="editModuleRow">
			<td width="30%">Module&nbsp;</td>
			<td>
				<select id='editSectionModule' name='editSectionModule'>
				<% for (int indx = 0; indx < jPagePredefSections.length(); indx++){%>
					<%
					JSONObject jsPredefSection = jPagePredefSections.getJSONObject(indx);
					String flxSectionId = jsPredefSection.getString("flxSectionId");
					String flxSectionName = jsPredefSection.getString("label");
					if (StringUtil.isEmpty(flxSectionId) || StringUtil.isEmpty(flxSectionName)){
						continue;
					}
					%>
					<option value="<%=flxSectionId%>"><%=flxSectionName%></option>
				<%} %>
				</select>
			</td>
		</tr>
	</table>
	<br>
	<button type='button' id='submitSectDiv'>Submit</button>
	<input type="hidden" id="editSecIndex" value=""/>
</div>
