<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Specimen_Appx%><%--Specimen Appendix*****--%> </title>


<SCRIPT language="javascript">

function confirmBox(fileName,pgRight) {

	if (! f_check_perm(pgRight,'E'))
	{
		return false;
	}
	var paramArray = [fileName];
	if (confirm(getLocalizedMessageString("M_Del_FrmSpmenApdx",paramArray))) {/*if (confirm("Delete " + fileName + " from specimen appendix?")) {*****/
		 return true;
       }
	else{
		return false;
	}

}

function fdownload(formobj,pk,filename,dnldurl)
{
	formobj.file.value = filename;
	formobj.pkValue.value = pk ;
	formobj.dnldurl.value=dnldurl;
	formobj.action="postFileDownload.jsp";
	//formobj.action=dnldurl;

	formobj.target = "_filedownload";
	formobj.method = "POST";
	formobj.submit();
}

</SCRIPT>
</head>

<%@ page language = "java" import = "java.util.*,com.aithent.file.uploadDownload.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.StringUtil,com. velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.specimenApndx.SpecimenApndxJB,com.velos.eres.business.common.SpecimenApndxDao"%><%@page import="com.velos.eres.service.util.LC"%><%@page import="com.velos.eres.service.util.MC"%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="userSiteB" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" />
<jsp:useBean id="codeLst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="specimenApndxB" scope="page" class="com.velos.eres.web.specimenApndx.SpecimenApndxJB"/>

<% 
String src = request.getParameter("srcmenu");
String selectedTab = request.getParameter("selectedTab");
String FromPatPage = request.getParameter("FromPatPage");
if(FromPatPage==null){
	FromPatPage="";
}
String patientCode=request.getParameter("patientCode");
if(patientCode==null){
	patientCode="";
}

%>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<br>
<body>

        <%


	HttpSession tSession = request.getSession(true);
	String specimenId = request.getParameter("pkId");
	if (sessionmaint.isValidSession(tSession))
	{

	 int pageRight = 0;

	 GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");

	 pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MSPEC"));


	String userIdFromSession = (String) tSession.getValue("userId");
     	int siteId = 0;
		String calledFrom = request.getParameter("calledFrom");
		if (calledFrom == null || calledFrom.equals(""))
		{
			calledFrom = "M";
		}

	//JM: 21Jul2009: issue no-4174
	String perId = "", stdId = "";
	perId = request.getParameter("pkey");
 	perId = (perId==null)?"":perId;

	stdId = request.getParameter("studyId");
	stdId = (stdId==null)?"":stdId;

		%>
<% if(FromPatPage.equals("patient")  || FromPatPage.equals("patientEnroll")){%>
<DIV class="BrowserTopn" id="divTab">
<jsp:include page="patienttabs.jsp" flush="true">
<jsp:param name="selectedTab" value="12"/>
<jsp:param name="page" value="<%=FromPatPage%>"/>
<jsp:param name="page" value="<%=patientCode%>"/>
<jsp:param name="pkey" value="<%=perId%>"/>
</jsp:include>
</DIV>

	<%} else{%>
		
  <!--Rohit:BugNo:4213 -->
  <DIV class="browserDefault" id="div1">
	<jsp:include page="inventorytabs.jsp" flush="true">
		<jsp:param name="selectedTab" value="1"/>
		<jsp:param name="specimenPk" value="<%=specimenId%>"/>
	</jsp:include>
	   <!-- <table width="95%" cellspacing="0" cellpadding="0">
	    <tr>
			<td class=tdDefault width="50%"><B> Specimen Id:</B>&nbsp;&nbsp;<%=specimenId%></td>
		</tr>

		</table> -->
</div>
<%} %>
<table width="100%" cellspacing="0" cellpadding="0">


		</table>
<DIV class="tabDefBot tabDefBot_spApndx" id="div2">
<%
           ArrayList specApndxIds   = new ArrayList();
           ArrayList fkSpecimen    = new ArrayList();
	   ArrayList specApndxDescs = new ArrayList();
	   ArrayList specApndxUris = new ArrayList();
	   ArrayList specApndxTypes = new ArrayList();
	   String dnld;
	   Configuration.readSettings("eres");
	   Configuration.readUploadDownloadParam(Configuration.FILE_UPLOAD_DOWNLOAD + "fileUploadDownload.xml", "specimen");
	   dnld=Configuration.DOWNLOADSERVLET ;
	   String modDnld = "";

	   SpecimenApndxJB specimenAJB  = new SpecimenApndxJB();
           SpecimenApndxDao specimenApndxdao=specimenAJB.getSpecimenApndxUris(EJBUtil.stringToNum(specimenId));
	   specApndxIds    = specimenApndxdao.getSpecApndxIds();
	   fkSpecimen = specimenApndxdao.getFkSpecimen();
	   specApndxDescs = specimenApndxdao.getSpecApndxDescs();
	   specApndxUris = specimenApndxdao.getSpecApndxUris();
	   specApndxTypes = specimenApndxdao.getSpecApndxTypes();

	  if (pageRight > 0  ){
	 		String incorrectFile = request.getParameter("incorrectFile");
			incorrectFile = (incorrectFile==null)?"":incorrectFile.toString();
			String space = request.getParameter("outOfSpace");
			space = (space==null)?"":space.toString();
			if(incorrectFile.equals("1")) {
			%>
				<br><br><br><br>
				<table width=100%>
				<tr><td align=center>
				<p class = "sectionHeadings">
					<%=MC.M_FileNotExist_ChkPath%><%--File does not exist. Please check the file path and name.*****--%>
				</p>
				</td>
				</tr>
				<tr height=20></tr>
				<tr>
				<td align=center>
			<button onclick="window.history.go(-2);"><%=LC.L_Back%></button>
			</td>
			</tr>
		</table>
		<%
		}
		else {
		      if(space.equals("1")) {
		      %>
				<br><br><br><br>
				<table width=100%>
				<tr><td align=center>
				<p class = "sectionHeadings">
				<%=MC.M_UploadSpace_ContAdmin%><%--The space allotted to you for file uploading has exhausted. Please contact Velos Administrator.*****--%>
				</p>
				</td>
				</tr>
				<tr height=20></tr>
				<tr>
				<td align=center>
			<button onclick="window.history.go(-2);"><%=LC.L_Back%></button>
			</td>
			</tr>
		    </table>
<%
		 }
			else
		  	{
		             int count = 0;
			}
		}
}
	   String specApndxId= "";
		String specApndxDesc="";
		String specApndxUri = "";
		String specApndxType="";
       %>


	<form METHOD=POST action="" name="spec">


<P class = "defComments_txt"><%=MC.M_FlwWebPgDocu_LnkSpmen%><%--The following web pages and documents are linked to this Specimen:*****--%><br> </P>

<TABLE width="100%">
 
<TR>

<TD WIDTH="50%" valign="top">
<table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl outline midalign">
      <tr> 
        <th width="100%"> <%=LC.L_My_Files%><%-- My Files*****--%> </th>
      </tr>
     
      <tr> 
        <td> 
		  <A href="specimenapndxfile.jsp? &mode=N&selectedTab=<%=selectedTab%>&srcmenu=<%=src%>&saApndxType=F&specId=<%=specApndxId%>&pkId=<%=specimenId%>&pkey=<%=perId%>&studyId=<%=stdId%>&FromPatPage=<%=FromPatPage%>&patientCode=<%=StringUtil.encodeString(patientCode)%>" onClick="return f_check_perm(<%=pageRight%>,'N') ;"><%=MC.M_ClickHere_UploadNewFile%><%--Upload Document*****--%></A>
        </td>
      </tr> 
    </table>
    <div style="width:99%; max-height:340px; overflow:auto">
<TABLE  cellspacing="0" cellpadding="0" class="basetbl outline midalign">
	<tr>
		<th width="30%"><%=LC.L_File_Name%><%-- File name*****--%></th>
		<th width="60%"><%=LC.L_Description%><%-- Description*****--%></th>
		<th width="5%"><%=LC.L_Edit%><%-- Edit*****--%></th>
		<th width="5%"><%=LC.L_Delete%><%-- Delete*****--%></th>   
	</tr>
   <%
   for(int i=0;i<specApndxIds.size(); i++) {

	specApndxId= ((Integer)specApndxIds.get(i)).toString();
	specApndxDesc=(String)specApndxDescs.get(i);
	specApndxUri =(String)specApndxUris.get(i);
	specApndxType=(String)specApndxTypes.get(i);


if ((i%2)==0) {
  %>
      <tr id="browserEvenRow">
        <%
		}
		else{
  %>
      <tr id="browserOddRow">
        <%
		}
		modDnld = dnld + "?file=" + specApndxUri ;

		if(specApndxType.equals("F")){

  %>     <td>
<span style="overflow: hidden; width: 100px; word-wrap: break-word; display: block;">
   	<A href="#" onClick="fdownload(document.spec,<%=specApndxId%>,'<%=StringUtil.encodeString(specApndxUri)%>','<%=dnld%>');return false;" >
   	

  <%=specApndxUri %>

  </A></span> </td>
	<td><span style="overflow: hidden; width: 250px; word-wrap: break-word; display: block;"> <%=specApndxDesc%> </span></td>
	<td width=15%>
	<A HREF="specimenapndxfile.jsp?mode=M&srcmenu=<%=src%>&pkId=<%=specimenId%>&selectedTab=<%=selectedTab%>&specimenId=<%=specApndxId%>&pkey=<%=perId%>&studyId=<%=stdId%>&FromPatPage=<%=FromPatPage%>&patientCode=<%=StringUtil.encodeString(patientCode)%>" onClick="return f_check_perm(<%=pageRight%>,'E') ;" ><img border="0" title="<%=LC.L_Edit%>" alt="<%=LC.L_Edit%>" src="./images/edit.gif" ><%//=LC.L_Edit %><%-- Edit*****--%></A>
	 </td>
	 <td width=15%  align="center" >
	<A HREF="specimenapndxdelete.jsp?specApndxId=<%=specApndxId%>&delMode=null&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&specApndxType=<%=specApndxType%>&specId=<%=specimenId%>&pkId=<%=specimenId%>&pkey=<%=perId%>&studyId=<%=stdId%>&FromPatPage=<%=FromPatPage%>&patientCode=<%=StringUtil.encodeString(patientCode)%>" onClick="return confirmBox('<%=specApndxUri%>',<%=pageRight%>)"><img src="./images/delete.gif" border="0" title="<%=LC.L_Delete%>" alt="<%=LC.L_Delete%>"/></A>

	</td>
	</tr>
 <%
 }
}
%>
	</TABLE>
	</div>

</TD>
<TD WIDTH="50%" valign="top">
 <table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl outline midalign" >
     <!-- <tr > 
        <td width = "100%"> 
          <P class = "defComments"><%=MC.M_PcolAppx_AsFollows%><%-- Protocol Appendix is as follows*****--%>:</P>
        </td>
      </tr>-->
      <tr> 
        <th width="100%" > <%=LC.L_My_Links%><%-- My Links*****--%> </th>
      </tr>
      
      <tr> 
        <td> 
		 <A href="addspecimenurl.jsp? &mode=N&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&pkId=<%=specimenId%>&pkey=<%=perId%>&studyId=<%=stdId%>&FromPatPage=<%=FromPatPage%>&patientCode=<%=StringUtil.encodeString(patientCode)%>" onClick="return f_check_perm(<%=pageRight%>,'N');"><%=MC.M_ClickHere_AddNewLink%><%--Add New URL*****--%></A>
        </td>
      </tr>
    </table>
    <div style="width:99%; max-height:350px; overflow:auto">
<TABLE  cellspacing="0" cellpadding="0" class="basetbl outline midalign">
	<tr>
		<th width="30%"><%=LC.L_Url_Upper%><%-- URL*****--%></th>
		<th width="60%"><%=LC.L_Description%><%-- Description*****--%></th>
		<th width="5%"><%=LC.L_Edit%><%-- Edit*****--%></th>
		<th width="5%"><%=LC.L_Delete%><%-- Delete*****--%></th>
   </tr>
     <%
    

   for(int i=0;i<specApndxIds.size(); i++) {

	specApndxId= ((Integer)specApndxIds.get(i)).toString();
	specApndxDesc=(String)specApndxDescs.get(i);
	specApndxUri =(String)specApndxUris.get(i);
	specApndxType=(String)specApndxTypes.get(i);


 if ((i%2)!=0) {
   %>
      <tr id="browserEvenRow">
        <%
		}
		else{
  %>
      <tr id="browserOddRow">
        <%
		}
		modDnld = dnld + "?file=" + specApndxUri ;

		if(specApndxType.equals("U")){

  %>
 <td><span style="overflow: hidden; width: 100px; word-wrap: break-word; display: block;"><A href="<%=specApndxUri%>" target="_new"><%=specApndxUri%></A></span></td><td><span style="overflow: hidden; width: 250px; word-wrap: break-word; display: block;"> <%=specApndxDesc%></span> </td>
	<td width=15%>
	<A HREF="addspecimenurl.jsp?mode=M&srcmenu=<%=src%>&pkId=<%=specimenId%>&selectedTab=<%=selectedTab%>
	&specimenId=<%=specApndxId%>&pkey=<%=perId%>&studyId=<%=stdId%>&FromPatPage=<%=FromPatPage%>&patientCode=<%=StringUtil.encodeString(patientCode)%>" onClick="return f_check_perm(<%=pageRight%>,'E');"><img border="0" title="<%=LC.L_Edit%>" alt="<%=LC.L_Edit%>" src="./images/edit.gif" ><%//=LC.L_Edit %><%-- Edit*****--%></A>
	 </td>
	 <td width=15% align="center" >
	 <!--KV:BugNo:3955 -->
	<A HREF="specimenapndxdelete.jsp?specApndxId=<%=specApndxId%>&delMode=null&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&specApndxType=<%=specApndxType%>&specId=<%=specimenId%>&pkId=<%=specimenId%>&pkey=<%=perId%>&studyId=<%=stdId%>&FromPatPage=<%=FromPatPage%>&patientCode=<%=StringUtil.encodeString(patientCode)%>" onClick="return confirmBox('<%=specApndxUri%>',<%=pageRight%>)"><img src="./images/delete.gif" border="0" title="<%=LC.L_Delete%>" alt="<%=LC.L_Delete%>"/></A>

	</td>

 <%
 }
}
%>
</tr>
 </table>
 </div>
</TD>
</TR>
</table>
	<input type="hidden" name="tableName" value="ER_SPECIMEN_APPNDX">
    <input type="hidden" name="columnName" value="SA_FILEOBJ">
    <input type="hidden" name="pkColumnName" value="PK_SPECIMEN_APPNDX">
    <input type="hidden" name="module" value="specimen">
    <input type="hidden" name="db" value="eres">
    <input type="hidden" name="pkValue" value="">
    <input type="hidden" name="file" value="">
    <input type="hidden" name="dnldurl" value="">


	</form>
<%
}//session
%>
  <div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</div>
<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/>
</div>
<input type=hidden name="specimenId" value=<%=specimenId%>>
	</body>

</html>