<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.json.*"%>
<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.*"%>
<jsp:useBean id="lineitemB" scope="session" class="com.velos.esch.web.lineitem.LineitemJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/> 

<% 
request.setCharacterEncoding("UTF-8");
response.setContentType("text/html");
HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession))
{
  %>
  <jsp:include page="sessionlogging.jsp" flush="true"/>

  <%
%>      
<%
  int ret= 0;
  int budgetId = StringUtil.stringToNum(request.getParameter("budgetId"));	
  ret = lineitemB.removeIndirects(budgetId);	 	
	    %>
   <input type="hidden" name="retval" id="retval" value="<%=ret %>"/>

<%
}	
else{
%>	
<jsp:include page="timeout.html" flush="true"/>
<%
}
%>


