<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:include page="localization.jsp" flush="true"/><%@page import="com.velos.eres.service.util.*"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_Prepare_Specimen%><%--Prepare Specimen*****--%></title>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="formjs.js"></SCRIPT>
<%--
<SCRIPT LANGUAGE="JavaScript" SRC="js/velos/prototype.js"></SCRIPT>
--%>
<SCRIPT LANGUAGE="JavaScript" SRC="js/dojo/dojo.js"></SCRIPT>
<script type="text/javascript" src="js/yui/build/yahoo-dom-event/yahoo-dom-event.js"></script>
<SCRIPT LANGUAGE="JavaScript" SRC="js/velos/ajaxengine.js"></SCRIPT>
<jsp:include page="ui-include.jsp" flush="true"/>
<jsp:include page="include.jsp" flush="true"/>
<script type="text/javascript">

function setVals(formobj,eSign)
{	
	var printIndices='';
	printIndices = "[INDEXSEP]";
	var totalchecked = 0;	
	
	if (!(validate_col('e-Signature',formobj.eSign))) return false
	
//        if(isNaN(formobj.eSign.value) == true) {
<%--     	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/ --%>
// 		formobj.eSign.focus();
// 		return false;
//    		}	
	/* YK 16MAR Enhancement No.3(Print Label Link Error)*/
	if(formobj.paramVals != undefined){
		if((formobj.paramVals.value!=null || formobj.paramVals.value!="")){
			alert("<%=MC.M_Specimen_AlreadyPrepared%>");/*alert("Specimen already Prepared");*****/
			return false;
		}
		formobj.checkParamVals.value=formobj.paramVals.value;
		}
	if(formobj.checkRow != undefined){
		var checks= formobj.checkRow;
	if(formobj.checkRow.length == undefined && formobj.checkRow.value=="0" && formobj.checkRow.checked){
		//alert("hi");
	printIndices = "[INDEXSEP]0[INDEXSEP]";
	totalchecked++;
 	}
	else{
	for(i=0;i<checks.length;i++){	
		if(checks[i].checked){	
			printIndices = printIndices + i + "[INDEXSEP]";
		totalchecked++;
		}
  	}
	}
	document.getElementById('span_cart').innerHTML = "<p class = \"sectionHeadings\" align = center><%=LC.L_Preparing_Specimen%><%--Preparing Specimen*****--%><img class=\"asIsImage_progbar\" src=\"../images/jpg/loading_pg.gif\" align = \"bottom\"/></p>";
	callAjaxForPrepare(printIndices,eSign);
	if(printIndices == "[INDEXSEP]"){var temp = $j('.successfulmsg').text();
	if(temp.indexOf("You have entered a wrong e-Signature.")!=-1)
		return false;	
	else
		return true;
	}
	else
		return false;	

	}
  else{
	  alert("<%=MC.M_No_DataInCart%>");/*alert("No data in cart!");*****/
		return false;
	}	
}
function callAjaxForPrepare(printIndices,eSign) {	
	//BUG#20411
	$j.ajax({
		url:'updatePrepareSpecimen.jsp',
		type: "POST",
		async:false,
		data:"printIndices="+printIndices+"&eSign="+eSign,
		success:(function (data){
			document.getElementById('span_cart').innerHTML = '';
			document.getElementById('eSign').value = '';
			ajaxvalidate('misc:eSign',4,'eSignMessage',L_Valid_Esign,L_Invalid_Esign,'sessUserId');
			document.getElementById('span_cart').innerHTML = data;
			
		}),
		error:function(response) { alert(data); return false; }
	});
}	
//function to select all rows
function checkAll()
{
var formobj=document.manageCartFrm;
var checks= formobj.checkRow;
if(formobj.checkallrow.checked){
	if(formobj.checkRow.length == undefined && formobj.checkRow.value=="0")
		{
			formobj.checkRow.checked = true;
		}
	else{
		for(i=0;i<formobj.checkRow.length;i++){			
			formobj.checkRow[i].checked=true;
			}			
	  	}
	}
else{
	if(formobj.checkRow.length == undefined && formobj.checkRow.value=="0")
	{
		formobj.checkRow.checked = false;
	}
else{
	for(i=0;i<formobj.checkRow.length;i++){	
		formobj.checkRow[i].checked=false;
		 }			
  	}
	}
}
function printLabelWin (selPks)
{
	windowName =window.open("printMultiLabel.jsp?&selPks="+selPks, "Information", "toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=700,height=450, top=100, left=90");
	windowName.focus();	
}
</script>
</head>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:include page="include.jsp" flush="true"></jsp:include>
<body>
<%HttpSession tSession = request.getSession(true); 
tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession))
	{	
 %> 
<form name="manageCartFrm" id="manageCartFrm" METHOD="POST"  onsubmit="if (setVals(document.manageCartFrm,document.getElementById('eSign').value)== false) {setValidateFlag('false'); return false;} else {setValidateFlag('true'); return true;}">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td width="30%" colspan = "2">&nbsp;</td><td>&nbsp;</td></tr>
<tr>
<td  align="left"  bgcolor = "#cccccc" style="vertical-align: bottom" colspan = "2"><b><%=MC.M_Prepare_SampPrintLabels%><%-- Prepare Samples and Print Label(s)*****--%></b></td>
<td align="right" bgcolor = "#cccccc" >
<jsp:include page="submitBar.jsp" flush="true">
<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="markDoneFrm"/>
		<jsp:param name="esignConfigure" value="userpxd"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include> 
</td>
</tr>
<tr><td  colspan = "3"><font class = "comments"><%=MC.M_SelFlwSpmen_DefPrintAll%><%-- You
have selected following item(s) to prepare specimen(s). By default labels will
be printed for all prepared specimen(s). Please use 'Print Label(s)' check box
if you would like to print label(s) selectively.*****--%>
</font></td></tr>
<tr><td colspan ="3" ><span id="span_cart"></span></td></tr>
<tr><td colspan = "3">&nbsp;</td></tr>
</table>
<jsp:include page="preparationCart.jsp" flush="true"> 
		<jsp:param name="checkboxdisplay" value="Y"/>	
		<jsp:param name="checkName" value="Print Labels"/>
		<jsp:param name="checkedTrue" value="checked"/>	
</jsp:include> 
</form>
<%}
else{
%>

<jsp:include page="timeout.html" flush='true'></jsp:include>
<%}%>

	<div class = "myHomebottomPanel">
	<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</body>
</html>