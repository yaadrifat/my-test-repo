<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title><%=MC.M_PersAcc_MultiLnks%><%--Personalize Account > Add Multiple Links*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<%@ page import="com.velos.eres.service.util.*,com.velos.eres.web.user.UserJB,com.velos.eres.web.grpRights.GrpRightsJB"%>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>



<SCRIPT Language="javascript">

 function  validate(formobj){


	  //KM-to fix Bug 2512
    	  // formobj=document.link

		mycount=0;
		mycount1=0;

	 for(cnt = 0; cnt<5;cnt++){
		if((formobj.lnkURI[cnt].value=="")){
		mycount++;
		}}

	 for(cnt = 0; cnt<5;cnt++){
		if((formobj.lnkURI[cnt].value=="")){
		mycount1++;
		}}


	
		if((mycount==5)&(mycount1==5)){
		alert("<%=MC.M_EtrAlteast_OneLnk%>");/*alert("Please Enter alteast one Link");*****/
		formobj.lnkURI[0].focus();
		return false;
		} 




	 for(cnt = 0; cnt<5;cnt++){
		if((formobj.lnkGrpName[cnt].value!="")&&(formobj.lnkDesc[cnt].value=="")){
		alert("<%=MC.M_Etr_LnkDesc%>");/*alert("Please Enter the Link Desc");*****/
		formobj.lnkDesc[cnt].focus();
		return false;
		}
	 }



	 for(cnt = 0; cnt<5;cnt++){
		if((formobj.lnkURI[cnt].value=="")&&(formobj.lnkDesc[cnt].value!="")){
		alert("<%=MC.M_Etr_Url%> ");/*alert("Please Enter the URL ");*****/
		formobj.lnkURI[cnt].focus();
		return false;
		}
	 }


	 for(cnt = 0; cnt<5;cnt++){
		if((formobj.lnkURI[cnt].value!="")&&(formobj.lnkDesc[cnt].value=="")){
		alert("<%=MC.M_Etr_LnkDesc%>");/*alert("Please Enter the Link Desc");*****/
		formobj.lnkDesc[cnt].focus();
		return false;
		}
	 }

	  if (!(validate_col('e-Signature',formobj.eSign))) return false

<%-- if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }--%>

     }

</SCRIPT>



<% String src;

src= request.getParameter("srcmenu");

%>



<jsp:include page="panel.jsp" flush="true">

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>



<jsp:useBean id="linkB" scope="session" class="com.velos.eres.web.ulink.ULinkJB"/>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>



<body>
  <%

	int lnkId =0;

	String userId ="";

	String accId ="";

	String URI ="";

	String desc ="";

	String grpName ="";

	String mode="";

	HttpSession tSession = request.getSession(true);

	if (sessionmaint.isValidSession(tSession))

	{

	String uName = (String) tSession.getValue("userName");

        mode = request.getParameter("mode");
	String tab = request.getParameter("selectedTab");
	String lnkType = request.getParameter("lnkType");

	int pageRight = 0;

		GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");

		if(lnkType.equals("user")) {
        	   //pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("HPERSHOME"));
			   pageRight = 7;
		} else {
		   pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MACCLINKS"));
		}

	if ((mode.equals("M") && pageRight >=6) || (mode.equals("N") && (pageRight == 5 || pageRight == 7 )) )

	{
	      lnkId = EJBUtil.stringToNum(request.getParameter("lnkId"));

		UserJB user = (UserJB) (tSession.getValue("currentUser"));
		userId  = String.valueOf(user.getUserId());
		accId = user.getUserAccountId();



%>

<DIV class="tabDefTopN" id="divtab">
<%
   if(lnkType.equals("user")) {
%>

<%
   } else {
%>

<%
   }
%>
  <jsp:include page="personalizetabs.jsp" flush="true"/>

</DIV>
<DIV class="tabDefBotN" id="div1">
	<!-- KV:Fixed Bug No.4602 Put Mandatory Field Validation -->
  <Form name="link" id="linkForm" method="post" action="savelinklist.jsp" onSubmit="if (validate(document.link)==false) {setValidateFlag('false');return false;} else {setValidateFlag('true'); return true;}">

      <P class = "sectionHeadings"> <%=LC.L_Enter_LinkDets%><%--Enter Link Details*****--%> </P>
     <table width="99%" cellspacing="0" cellpadding="0" border="0" class="MidAlign">

     <tr>
      <td colspan="3">
        <P class="defComments"> - <%=MC.M_LnkSecHead_EtrName%><%--To include this link under an existing section heading,
          enter it's name here. If you provide a new name, the link will be placed
          under the new Section Heading.*****--%> </P>
       </td>
      </tr>
      <tr>
        <td colspan="3">
          <P class="defComments"> - <%=MC.M_EtrCpltUrl_Mylink255Max %><%-- Enter complete url eg. 'http://www.myLink.com'
            or &nbsp 'ftp://ftp.myLink.com'(255 char max.)*****--%> </P>
        </td>
	</tr>
      <tr><td>&nbsp </td></tr>
      <tr class="outline">
      	  <th width=25% align="center"><%=LC.L_Sec_Heading%><%--Section Heading*****--%></th>
		<th width=40% align="center"><%=LC.L_Url_Upper%><%--URL*****--%> <FONT class="Mandatory">* </FONT> </th>
		<th width=35% align="center"><%=LC.L_Description%><%--Description*****--%> <FONT class="Mandatory">* </FONT> </th>
		</tr>


	<% for (int i = 0; i< 5; i++) { %>
	  <tr>
		<td width=25% align="center">
		<input type="text" name="lnkGrpName" size = 20 MAXLENGTH = 50 value="">
        </td>
        <td width=40% align="center">
          <input type="text" name="lnkURI" size = 35 MAXLENGTH = 300 value="">
	  </td>

	  <td width=35% align="center">
           <input type="text" name="lnkDesc" size = 30 MAXLENGTH = 300 value="">
	  </td>
      </tr>
	<% } %>

    </table>
    <br>
    <input type="hidden" name="lnkAccId" value="<%=accId%>">
    <input type="hidden" name="lnkUserId" value="<%=userId%>">
    <input type="hidden" name="lnkType" value="<%=lnkType%>">
    <input type="hidden" name="mode" value="<%=mode%>">
    <input type="hidden" name="lnkId" value="<%=lnkId%>">
    <input type="hidden" name="src" value="<%=src%>">
    <Input type="hidden" name="selectedTab" value="<%=tab%>">
    <input type="hidden" name="totrows" value="5">
<table width="100%" cellspacing="0" cellpadding="0" class="esignStyle"><tr><td>
<!--JM: 19June2009: implementing common Submit Bar in all pages -->
 	<jsp:include page="submitBar.jsp" flush="true">
	<jsp:param name="displayESign" value="Y"/>
	<jsp:param name="formID" value="linkForm"/>
	<jsp:param name="showDiscard" value="N"/>
	</jsp:include>
</td></tr></table>

  </Form>
  <%

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div class = "myHomebottomPanel">
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id = "emenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/>
</div>
</body>
</html>
