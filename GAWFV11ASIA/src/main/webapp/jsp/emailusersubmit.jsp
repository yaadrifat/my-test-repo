<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
</HEAD>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<BODY>

<%@ page language = "java" import = "com.velos.eres.business.common.*,com.velos.eres.service.util.*,java.util.*,com.velos.eresmailer.*"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="addressUserB" scope="page" class="com.velos.eres.web.address.AddressJB" />
<%

String cnt,email;


String messageTo = null;

String userId = null;
String userEmail = null;
int rows  = 0;

String mailText = null;
String message = null;
message=request.getParameter("txtAreamessage");

String subject=request.getParameter("subject");
String mailedTo=request.getParameter("To");

//String mailText1,mailText2;///
String userMailStatus = "";

String src;
src=request.getParameter("srcmenu");
src=(src==null)?"":src;

HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession)) {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%	
	String supportEmail = Configuration.SUPPORTEMAIL;
    String eSign = request.getParameter("eSign");	
    String oldESign = (String) tSession.getValue("eSign");

	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {





 CtrlDao userCtrl = new CtrlDao();	
 userCtrl.getControlValues("eresuser");
 rows = userCtrl.getCRows();
 if (rows > 0)
	{
	   userEmail = (String) userCtrl.getCDesc().get(0);
	}

rows = 0;

String msgFrom = request.getParameter("messagefrom");



String messageFooter = "\n\n"+MC.M_EmailContain_SecureInfo/*This email transmission and any documents, files or previous email messages attached to it may contain information that is confidential or legally privileged. If you are not the intended recipient or a person responsible for delivering this transmission to the intended recipient you are hereby notified that you must not read this transmission and that any disclosure, copying, printing, distribution or use of this transmission is strictly prohibited. If you have received this transmission in error, please immediately notify the sender by telephone or return email and delete the original transmission and its attachments without reading or saving in any manner." *****/;
mailText = message+messageFooter;

	//send mail
			try{
   				   	VMailer vm = new VMailer();
			    	VMailMessage msgObj = new VMailMessage();  
    		
					msgObj.setMessageFrom(userEmail);
					msgObj.setMessageFromDescription("Velos eResearch");
					msgObj.setMessageTo(mailedTo);
					msgObj.setMessageSubject(request.getParameter("subject")) ;
					msgObj.setMessageSentDate(new Date());
					msgObj.setMessageText(mailText);

					vm.setVMailMessage(msgObj);
					userMailStatus = vm.sendMail();
				//	System.out.println("userMailStatus" + userMailStatus); 	
					// Added by gopu to fix the Bugzilla Issues #2095
					//eresDispatcher edis = new eresDispatcher();
					//boolean mailSent = edis.sentMail(mailedTo,subject, mailText,userEmail); 
				   					
			  }
			 catch(Exception  m)
			  {	
			  	userMailStatus = m.toString();
			  }

//end of sending mail


	%>

	
	
<br>
<br>
<br>
<br>
<br>
<br>
<br>

 <%
   if ( ! EJBUtil.isEmpty(userMailStatus) )
	{
  %>
	<p class = "redMessage" align = center> <%=MC.M_MailNotSent_FlwReason%><%--Your mail could not be sent 
		  because of the following reason(s)*****--%>: </p>
	<br>
	<p class = "redMessage"  align = center> 
	<ul>
    <li> <%=MC.M_EtrIncorrect_EmailAddr%><%--You have entered an incorrect e-mail address*****--%> </li>
    <li> <%=MC.M_IfMoreEmail_SeprByComma%><%--If you have entered more than one e-mail address, they are not separated 
      by a ',' (comma)*****--%></li>
    <li> <%=MC.M_ProbWith_MailServer%><%--There is some problem with the mail server*****--%> </li>
   	<li><%=MC.M_CnctSupport_WithMsg%><%--Please contact Customer Support with the following message*****--%>:<BR><%=userMailStatus%></li>
	</ul></p>

    <table width=100%>
    <tr>
    <td align=center width=50%>
    
    		<button onclick="window.history.back();"><%=LC.L_Back%></button>
    </td>		
    </tr>		
    </table>	
  <%
  	}
	else
	{
  %>
	<p class = "sectionHeadings" align = center> <%=MC.M_EmailSent_Succ%><%--Your mail has been sent successfully*****--%></p>
	<META HTTP-EQUIV=Refresh CONTENT="1; URL=accountbrowser.jsp?srcmenu=<%=src%>">
  <%
  	} //message sent

	} //end of if for esign
	}else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
} //end of if for session 
%>

</BODY>
</HTML>


