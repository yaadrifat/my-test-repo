<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<jsp:useBean id="budget" scope="page" class="com.velos.esch.web.budget.BudgetJB"/>
	
  <%@ page language = "java" import = "com.velos.eres.business.common.*,com.velos.eres.service.util.*,java.util.*,com.velos.esch.business.common.*"%>
    
  <%  
	
	HttpSession tSession = request.getSession(true); 

	if(sessionmaint.isValidSession(tSession))

   {
	  String eSign = request.getParameter("eSign");
	   String ipAdd = (String) tSession.getValue("ipAdd");
	   String src= request.getParameter("srcmenu");
	   
	
		String uId = (String) tSession.getValue("userId");
		int userId = EJBUtil.stringToNum(uId);
	BudgetDao budgetDao = new BudgetDao();
   
	String sAcc= (String) tSession.getValue("accountId");
%> 
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%   
  	 	String oldESign = (String) tSession.getValue("eSign");
		String budgetId =request.getParameter("budsel");
		
		String from = request.getParameter("from");
		// modified by gopu to fix the issues #1917 for trim leading and trailing spaces
		String budgetName = request.getParameter("budname").trim();
		
		String budgetVer = request.getParameter("budver");
		

		if(!oldESign.equals(eSign)) {
    		%>
       	 	 <jsp:include page="incorrectesign.jsp" flush="true"/>  	
       		<%
       		} else 
				{
				if (budgetVer.trim().equals(""))
					{ 
					   budgetVer = "null";
				 }
				
		if(budgetDao.checkDupBudget(EJBUtil.stringToNum(sAcc),budgetName,budgetVer, "N", EJBUtil.stringToNum(budgetId))==0) 
		{

		

		 if(budget.copyBudget(EJBUtil.stringToNum(budgetId),budgetName,budgetVer,userId,ipAdd) == -2){
			 if (budgetVer.trim().equals("null"))
			{ 
				budgetVer = "";
			}
       	 %>		
		 <br><br><br><br><br><p class = "successfulmsg" align = center>		
		 <%=MC.M_BgtNameBgtVer_AldyExst%><%-- The Budget Name and Budget Version combination that you have entered already exists. Please enter a new name or version.*****--%> 
		 <Br><Br>			 
		 <button onclick="window.history.back();"><%=LC.L_Back%></button>
		 </p>		
		<% return;  
		 }
		 else{%>
			<br><br><br><br><br><P class = "successfulmsg" align = center> <%=MC.M_Bgt_CopiedSucc%><%-- The budget has been copied successfully.*****--%> </P>
			<Br><Br>
			   <META HTTP-EQUIV=Refresh CONTENT="1; URL=budgetbrowserpg.jsp?srcmenu=<%=src%>">  
		<%}	

	    }
		else{%>
			<br><br><br><br><br><p class = "successfulmsg" align = center>		
		 <%=MC.M_BgtNameBgtVer_AldyExst%><%-- The Budget Name and Budget Version combination that you have entered already exists. Please enter a new name or version.*****--%> 
		 <Br><Br>			 
		 <button onclick="window.history.back();"><%=LC.L_Back%></button>
		 </p>		   
		<%}	
			
		} // end of esign
		}//end of if body for session
	else
	{
	%>
	<jsp:include page="timeout.html" flush="true"/>
	<%
	}	
	%>
	</BODY>
	</HTML>
