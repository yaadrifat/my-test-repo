<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title>

</title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="js/yui/build/yahoo/yahoo-min.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="js/yui/build/event/event-min.js"></SCRIPT>
<SCRIPT Language="javascript">

 function  validate(formobj){

 if(formobj.thread.value == 'new'){
 if (!(validate_col('Field',formobj.field))) return false;
 }

 if (formobj.cmbQueryResp){
 	if (!(validate_col('Query type',formobj.cmbQueryResp))) return false;
 }

 if (!(validate_col('Query Status',formobj.cmbStatus))) return false;

 //Commented as per EDC_AT4 
 //if (!(validate_col('Entered By',formobj.enteredByName))) return false

 //if (!(validate_col('Entered On',formobj.enteredOn))) return false



    if (!(validate_col('e-Signature',formobj.eSign))) return false


	<%-- if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   	} --%>
   	/*AK 03May-2011 Bug #5896*/
    var charCount =formobj.instructions.value.length;
    var paramArray = [charCount];
    document.getElementById("enteredCount").innerHTML=getLocalizedMessageString("L_Etr_Char",paramArray);/*document.getElementById("enteredCount").innerHTML=" Entered Characters: "+charCount;*****/
	if(parseInt(charCount)>4000)
	{
		alert("<%=MC.M_ExcdLimit4000_PlsComt%>")/*alert("You have exceeded the limit of 4000 characters.\n Please edit your comments.")*****/
		formobj.instructions.focus();
		return false;
	}
	
	return true;

   }
 
 function refreshPanel()
 {
 	parent.document.getElementById("preview").contentWindow.location.reload();
 }
 
	function openwinQuery() {
    windowNameQ = window.open("usersearchdetails.jsp?fname=&lname=&from=addnewquery","QueryUser","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
	windowNameQ.focus();
}
function countCharactersDefault(){
	countCharacters(document.addnewquery, 4000);
}


</SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="fieldlibB" scope="request" class="com.velos.eres.web.fieldLib.FieldLibJB"/>

<jsp:useBean id="queryStatusB" scope="request" class="com.velos.eres.web.formQueryStatus.FormQueryStatusJB"/>
<jsp:useBean id="groupB" scope="request" class="com.velos.eres.web.group.GroupJB"/>
<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB"/>
<!-- Virendra: Fixed Bug no. 4708, added import  com.velos.eres.service.util.StringUtil-->
<%@ page language = "java" import = "java.text.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.StringUtil"%>


<% String src;
//Virendra: Fixed Bug no. 4708, added variable addAnotherWidth
int addAnotherWidth = 0;

src= request.getParameter("srcmenu");

%>
<script>
  YAHOO.util.Event.onDOMReady(countCharactersDefault);
</script>

<body style="overflow:visible"> <%-- YK 06Feb-2011 Bug #5694 --%>
<%--Removed onload defined by YK for #5694, instead wrote onDONReady --%>
<br>
<DIV class="popDefault" id="div1">

  <%
	
    String siteId=request.getParameter("site_id");
	HttpSession tSession = request.getSession(true);
     String adveventId=request.getParameter("adveventId");
     String advFieldId=request.getParameter("advFieldId");
	 String pkey=request.getParameter("pkey");
	 String calledFrom=request.getParameter("calledFrom");
	 String formType=request.getParameter("formType");//calling form jupiter
	 String dashBoard=request.getParameter("dashBoard")==null?"":request.getParameter("dashBoard");
	String mode=request.getParameter("mode");
	if(mode==null)
	mode="";

 /*<!-- YK Bug#4822 19May2011  -->	*/
	int pageRight =0 ;
	pageRight = EJBUtil.stringToNum((String)session.getAttribute("formQueryRight"));
	if("Advrse_Form".equals(formType)||"PatStudy_Form".equals(formType)){
		if("jupiter".equals(dashBoard)){
			pageRight=EJBUtil.stringToNum(request.getParameter("pageRight"));
		}else{
	  	  if(pageRight==0){
	  		  pageRight=7; 
	  	  }
		 }
	    }
	int formId = EJBUtil.stringToNum(request.getParameter("formId"));

	String filledFormId =  request.getParameter("filledFormId");
	String from = request.getParameter("from");



	if (sessionmaint.isValidSession(tSession))
	{
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
	<jsp:include page="include.jsp" flush="true"/>
<%
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");


	if (pageRight >=4)

	{

		String txtname="";
		String txtdesc="";
		String fldPullDown = "";
		String fieldName = "";
		String formQueryId = "";
		String queryStatusId = "";
		String enteredByName = "";


		String note = "";
		String enteredOn = "";
		String enteredBy = "";
		//String type = "";
		String typeId= "";
		String statusId = "";
		String fname = "";
		String lname = "";

		String thread = request.getParameter("thread");
		String pageFrom = request.getParameter("page");
		String entrdBy = request.getParameter("entrdBy");
		
		String studyId ="";
		
    	studyId = request.getParameter("studyId");
	 	
	 	if (StringUtil.isEmpty(studyId))
	 	{
	 		studyId="";
	 	}
     
     
     	String userIdFromSession = (String) tSession.getValue("userId");
     	UserDao uDao = new UserDao();
		uDao.setUsrIds(EJBUtil.stringToInteger(userIdFromSession));
		uDao.getUsersDetails(userIdFromSession);
		String groupId = "";
		groupId = (String) uDao.getUsrDefGrps().get(0);
		
		if(entrdBy == null)
		entrdBy = "";
		//out.println("entrdBy:"+entrdBy);
		if(pageFrom == null)
		pageFrom = "";
		if(thread == null)
		 thread = "";
		 if(thread.equals("old")){
		 fieldName= request.getParameter("fieldName");
		 formQueryId = request.getParameter("formQueryId");

		 }
		String strQueryType = request.getParameter("queryType");

		if(mode.equals("M")){
		queryStatusId = request.getParameter("queryStatusId");

		queryStatusB.setFormQueryStatusId(EJBUtil.stringToNum(queryStatusId));
		queryStatusB.getFormQueryStatusDetails();
		note = queryStatusB.getQueryNotes();
		if(note == null)
		note = "";
		strQueryType = queryStatusB.getFormQueryType() ;
		typeId= queryStatusB.getQueryTypeId();
		enteredBy = queryStatusB.getEnteredBy();

		userB.setUserId(EJBUtil.stringToNum(enteredBy));
		userB.getUserDetails();


		fname = userB.getUserFirstName();
 		if(fname == null)
		fname = "";
		lname = userB.getUserLastName();
		if(lname == null)
		lname = "";
    	enteredByName = fname + " " + lname;
		enteredOn = queryStatusB.getEnteredOn();
		if(enteredOn == null)
		enteredOn = "";
		statusId = queryStatusB.getQueryStatusId();
	}


		fieldName=(fieldName==null)?"":fieldName;
		if (fieldName.length()>0)
		 fieldName=StringUtil.decodeString(fieldName);
		String enteredId = "";


		if(strQueryType == null)
		strQueryType = "";
		int queryType = EJBUtil.stringToNum(strQueryType);
		//out.println("queryType::"+queryType);

		if((!mode.equals("M")) || (queryType == 3 && entrdBy.equals("system-generated"))){
		enteredByName = 	(String) tSession.getValue("userName");
		enteredBy= 	(String) tSession.getValue("userId");
		}
		
		CodeDao cd1 = new CodeDao();
		String roleCodePk="";
		String roleSubType="";
		
		if (! StringUtil.isEmpty(studyId) && EJBUtil.stringToNum(studyId) > 0)
		{
			ArrayList tId = new ArrayList();
			
			TeamDao teamDao = new TeamDao();
			teamDao.getTeamRights(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(userIdFromSession));
			tId = teamDao.getTeamIds();
							
			if (tId != null && tId.size() > 0)
			{
				ArrayList arRoleIds = new ArrayList();
				arRoleIds = teamDao.getTeamRoleIds();
							
				if (arRoleIds != null && arRoleIds.size() >0 )	
				{
					roleCodePk = (String) arRoleIds.get(0);
					roleSubType = cd1.getCodeSubtype(EJBUtil.stringToNum(roleCodePk));
					
					if (StringUtil.isEmpty(roleCodePk))
					{
						roleCodePk="";
					}
				}	
				else
				{
					roleCodePk ="";
				}
				
			}	
			else
			{
				roleCodePk ="";
			}
			
			//Bug #5993
			if (roleCodePk.equals("")){
				int grpId = Integer.parseInt(groupId);
				groupB.setGroupId(grpId);
				groupB.getGroupDetails();
				if (groupB.getGroupSuperUserFlag().equals("1")){
					String rolePK = groupB.getRoleId();
					if (rolePK == null || rolePK.equals("0")){
						roleCodePk = "";
						roleSubType = "";
					}else{
						roleCodePk = rolePK;
						roleSubType = cd1.getCodeSubtype(EJBUtil.stringToNum(roleCodePk));
					}
				}
			}
		} else
			{			
			  roleCodePk ="";
			}  
		
	 	cd1.getCodeValuesForStudyRole("query_status",roleCodePk);
		
		String statusPullDn = "";
		if(mode.equals("M"))
		statusPullDn = cd1.toPullDown("cmbStatus", EJBUtil.stringToNum(statusId), false);
		else
		statusPullDn = cd1.toPullDown("cmbStatus");

		if("adverseEvent".equals(calledFrom)){
			fldPullDown=getAdverseFieldName(EJBUtil.stringToNum(advFieldId));
			
		}else if("Advrse_Form".equals(formType)){ //calling from jupiter
			//fldPullDown=fieldName;
			fldPullDown="<select name='field'>"
				+"<option  value='11012'>Adverse Event ID</option>"
				+"<option  value='11013'>Adverse Event Type</option>"
				+"<option  value='11014'>Category</option>"
				+"<option  value='11015'>Adverse Event Name</option>"
				+"<option  value='11016'>Toxicity</option>"
				+"<option  value='11017'>Toxicity Description</option>"
				+"<option  value='11018'>Severity/Grade</option>"
				+"<option  value='11019'>MedDRA code</option>"
				+"<option  value='11020'>Dictionary</option>"
				+"<option  value='11021'>Other Description</option>"
				+"<option  value='11022'>Treatment Course</option>"
				+"<option  value='11023'>Start Date</option>"
				+"<option  value='11024'>Stop Date</option>"
				+"<option  value='11025'>AE Discovery Date</option>"
				+"<option  value='11026'>AE Logged Date</option>"
				+"<option  value='11027'>Entered By</option>"
				+"<option  value='11028'>Reported By</option>"
				+"<option  value='11029'>Attribution</option>"
				+"<option  value='11030'>Linked To</option>"
				+"<option  value='11031'>Outcome Type</option>"
				+"<option  value='11032'>Action</option>"
				+"<option  value='11033'>Additional Information</option>"
				+"<option  value='11034'>The Following were Notified</option>"
				+"<option  value='11035'>Notes</option>"
				+"<option  value='11036'>Reason For Change (FDA Audit)</option>"
				+"<option  value='11037'>AE Status</option>"
				+"<option  value='11038'>Severity/Grade Description</option>"
				+"<option  value='11039'>Recovery Description</option>"
				+"<option  value='11040'>Outcome Notes</option>"
				+"<option  value='11041'>Adverse Event More Details</option>"
				+"<option value='' selected=''>Select an option</option>"
			
			    +"</select>";
		}
		else if("PatStudy_Form".equals(formType)){
			fldPullDown="<select name='field'>"
			+"<option  value='13012'>Attachment Status</option>"
			+"<option  value='13013'>Reason</option>"
			+"<option  value='13014'>Status Date</option>"
			+"<option  value='13015'>Notes</option>"
			+"<option  value='13016'>Next Follow-up Date</option>"
			+"<option  value='13017'>Patient Study ID</option>"
			+"<option  value='13018'>Enrolling Site</option>"
			+"<option  value='13019'>Assigned To</option>"
			+"<option  value='13020'>Physician</option>"
			+"<option  value='13021'>Treatment Location</option>"
			+"<option  value='13022'>Treating Organization</option>"
			+"<option  value='13023'>Disease Code</option>"
			+"<option  value='13024'>Anatomic Site</option>"
			+"<option  value='13025'>Evaluable Flag</option>"
			+"<option  value='13027'>Evaluable Status</option>"
			+"<option  value='13028'>Unevaluable Status</option>"
			+"<option  value='13029'>Status</option>"
			+"<option  value='13030'>Survival Status</option>"
			+"<option  value='13031'>Date of Death</option>"
			+"<option  value='13032'>Cause of Death</option>"
			+"<option  value='13033'>Specify Cause</option>"
			+"<option  value='13034'>Death Related to Study</option>"
			+"<option  value='13035'>Reason of Death Related to Study</option>"
			+"<option  value='13036'>Current Status</option>"
			+"<option  value='13037'>Screen Number</option>"
			+"<option  value='13038'>Screen By</option>"
			+"<option  value='13039'>Screen OutCome</option>"
			+"<option  value='13040'>Randomization Number</option>"
			+"<option  value='13041'>Enrolled By</option>"
			+"<option  value='13042'>Version No</option>"
			+"<option value='' selected=''>Select an option</option>"
			 +"</select>";
		}
		else{
		
		FieldLibDao fDao = new FieldLibDao();

		//fDao = fieldlibB.getFieldsInformation(formId);
		// Only edit and multiple choice type fields are seen
		fDao = fieldlibB.getEditMultipleFields(formId);

		ArrayList fldNames = fDao.getFldName();
		ArrayList fieldIds= fDao.getFieldLibId();

		fldPullDown=EJBUtil.createPullDown("field", 0, fieldIds, fldNames);
		
		}
		CodeDao cd2 = new CodeDao();
		if(queryType ==2 || queryType == 1)
		cd2.getCodeValues("form_query");
		else
		cd2.getCodeValues("form_resp");
		
		String queryRespPulln = "";
		if(mode.equals("M"))
		 queryRespPulln = cd2.toPullDown("cmbQueryResp", EJBUtil.stringToNum(typeId), false);
		 else
		 queryRespPulln = cd2.toPullDown("cmbQueryResp");
		 if(!mode.equals("M"))

		 //JM: 11Mar2009
		  
		 enteredOn = DateUtil.dateToString(new Date());



%>

  <P class="defcomments"><b>
 <%-- YK 03DEC-- EDC_AT3/4 REQ--%>
  <%if(mode.equals("M")){
			    
			if(strQueryType.equals("2")){%>
			<%=LC.L_FrmQry_View%><%--Form Query >> View*****--%>
			<%}
			if(strQueryType.equals("3")){%>
			<%=LC.L_QryResp_View%><%--Query Responses >> View*****--%>
			<%}
	}else{
			if(strQueryType.equals("2")){%>
			<%=LC.L_FrmQry_AddNew%><%--Form Query >> Add New*****--%>
			<%}
			if(strQueryType.equals("3")){%>
			<%=MC.M_QryResp_AddNew%><%--Query Responses >> Add New*****--%>
			<%}
	}%>
 <%-- YK 03DEC-- EDC_AT3/4 REQ--%>
	 </b> </P>

  <Form name="addnewquery" id="addnewqry" method="post" action="updateaddnewquery.jsp" onsubmit="if (validate(document.addnewquery)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true');refreshPanel(); return true;}" >

  <table width="99%" cellspacing="0" cellpadding="0">
  <%-- YK 03DEC-- EDC_AT3/4 REQ--%>
  <%if(!thread.equals("new")){%>
  <tr height="20">
  <td width="18%"><%=LC.L_Query_Id%><%--Query ID*****--%></td>    <%-- YK 17DEC-- BUG#5660--%>
  <td width="80%"><%=formQueryId%></td>
  <%-- YK 03DEC-- EDC_AT3/4 REQ--%>
  </tr>
  <%}%>	
 
  <tr height="20">
  <td > <%=LC.L_Fld_Name%><%--Field Name*****--%> <FONT class="Mandatory">* </FONT></td>
  <td >
  <%if(thread.equals("new")){%>
  <%=fldPullDown%>
  <%}else{%>
  <%=fieldName%>
  <%}%>	
  </td>
  </tr>
<%if (!studyId.equals("")){ %>
	<tr>
	<%if(queryType ==2 || queryType ==1){%>
		<td ><%=LC.L_Query_Type%><%--Query Type*****--%><FONT class="Mandatory">* </FONT>  </td>
		<td><%=queryRespPulln%></td>
	<%}else{%>
		<%if (roleSubType.equals("role_coord")){ %>
		 <td > <%=LC.L_Response%><%--Response*****--%><FONT class="Mandatory">* </FONT>  </td>
		 <td><%=queryRespPulln%></td>
	  	<%} %>
	<%}%>
	</tr>
<%} else {%>
	<tr>
		  <%if(queryType ==2 || queryType ==1){%>
		  <td ><%=LC.L_Query_Type%><%--Query Type*****--%><FONT class="Mandatory">* </FONT>  </td>
		  <%}else{%>
		  <td > <%=LC.L_Response%><%--Response*****--%><FONT class="Mandatory">* </FONT>  </td>
		  <%}%>
		  <td><%=queryRespPulln%></td>
	  </tr>
<%} %>
<%-- YK 03DEC-- EDC_AT3/4 REQ--%>
  <tr>
	<td > <%=LC.L_Query_Status%><%--Query Status*****--%>  <FONT class="Mandatory">* </FONT>  </td>
  	<td ><%=statusPullDn%></td>
  </tr>
      <%-- YK 03DEC-- EDC_AT3/4 REQ--%>
		<%if(mode.equals("M")){%> 
		<tr>     
        <td > <%=LC.L_Entered_By%><%--Entered By*****--%>  <FONT class="Mandatory">* </FONT></td>
        <td >
		<input type="hidden" name="enteredById" value=<%=enteredBy%>>
          <input type="text" name="enteredByName" size = 20 MAXLENGTH = 100 value='<%=enteredByName%>' READONLY>&nbsp
		  <A href="#" onclick="return openwinQuery()"><%=LC.L_Select_User%><%--Select User*****--%></A>
        </td>
         </tr>
        <%}else {%>
       
		<input type="hidden" name="enteredById" value=<%=enteredBy%>>
          <input type="hidden" name="enteredByName" size = 20 MAXLENGTH = 100 value='<%=enteredByName%>' READONLY>&nbsp
         <%} %>
     
      
      <%if(mode.equals("M")){%>
       <tr>
<%-- INF-20084 Datepicker-- AGodara --%>
        <td> <%=LC.L_Entered_On%><%--Entered On*****--%>  <FONT class="Mandatory">* </FONT></td>
        <td>
        	<input type="text" name="enteredOn" class="datefield" size = 20 MAXLENGTH = 100 value='<%=enteredOn%>' READONLY>
       	</td>
       </tr>
        <%}else {%>
       <input type="hidden" name="enteredOn" size = 20 MAXLENGTH = 100 value='<%=enteredOn%>' READONLY>
        <%} %>
	<%-- YK 03DEC-- EDC_AT3/4 REQ--%>	
		<tr>
		<td > <%=LC.L_Comments%><%--Comments*****--%> </td>
		 <td >
			 <textarea name="instructions" cols="50" rows="4"  MAXLENGTH=4000 ><%=note%></textarea>  <div id="charCount"><%=LC.L_CharAllwd_4000%><%--Characters allowed: 4000*****--%></div> <div id="enteredCount"> <div></div>  <%-- YK 17DEC-- EDC_AT4--%> <%-- YK 06Feb-2011 Bug #5694 --%>
        </td>



  </tr>
  </table>
  <br>
<table cellspacing="0" cellpadding="0" width="100%" bgcolor="#cccccc">
<tr>
	<td align=right>
		<%if("Advrse_Form".equals(formType)){%>
		<%if(pageFrom.equals("H")){%>
		<A type="submit" href= "formQueryHistory.jsp?studyId=<%=studyId%>&formId=<%=formId%>&from=<%=from%>&filledFormId=<%=filledFormId%>&formQueryId=<%=formQueryId%>&fieldName=<%=StringUtil.encodeString(fieldName)%>&formType=<%=formType %>&fieldId=<%=advFieldId %>&dashBoard=<%=dashBoard%>&pageRight=<%=pageRight%>"><%=LC.L_Back%></A> <!-- YK Bug#4822 19May2011  -->	
		<%}else{%>
		<A type="submit" href= "addeditquery.jsp?studyId=<%=studyId%>&formId=<%=formId%>&from=<%=from%>&filledFormId=<%=filledFormId%>&formType=<%=formType %>&dashBoard=<%=dashBoard%>"><%=LC.L_Back%></A><!-- YK Bug#4822 19May2011  -->	
		<%}%>
		<%}
		
		else if("PatStudy_Form".equals(formType)){%>
		<%if(pageFrom.equals("H")){%>
		<A type="submit" href= "formQueryHistory.jsp?studyId=<%=studyId%>&formId=<%=formId%>&from=<%=from%>&filledFormId=<%=filledFormId%>&formQueryId=<%=formQueryId%>&fieldName=<%=StringUtil.encodeString(fieldName)%>&formType=<%=formType %>&fieldId=<%=advFieldId %>&dashBoard=<%=dashBoard%>&pageRight=<%=pageRight%>"><%=LC.L_Back%></A> <!-- YK Bug#4822 19May2011  -->
		<%}else{%>
		<A type="submit" href= "addeditquery.jsp?studyId=<%=studyId%>&formId=<%=formId%>&from=<%=from%>&filledFormId=<%=filledFormId%>&formType=<%=formType %>&dashBoard=<%=dashBoard%>"><%=LC.L_Back%></A><!-- YK Bug#4822 19May2011  -->
		<%}%>
		<%}

		else{%>
		<%if(pageFrom.equals("H")){%>
		<A type="submit" href= "formQueryHistory.jsp?studyId=<%=studyId%>&formId=<%=formId%>&from=<%=from%>&filledFormId=<%=filledFormId%>&formQueryId=<%=formQueryId%>&fieldName=<%=StringUtil.encodeString(fieldName)%>"><%=LC.L_Back%></A> <!-- YK Bug#4822 19May2011  -->	
		<%}else{%>
		<A type="submit" href= "addeditquery.jsp?studyId=<%=studyId%>&formId=<%=formId%>&from=<%=from%>&filledFormId=<%=filledFormId%>"><%=LC.L_Back%></A><!-- YK Bug#4822 19May2011  -->	
		<%}%>
		<%}%>
		
		
		
		
		<!--<button onclick="window.history.back();"><%=LC.L_Back%></button>-->
	</td>
	<!-- Virendra: Fixed Bug no. 4708, added td with addAnotherWidth  -->
	<td bgcolor="<%=StringUtil.eSignBgcolor%>" valign="baseline" align="left" width="85%" <%=addAnotherWidth > 0 ? "" : "colspan=5" %>>
		<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="addnewqry"/>
			<jsp:param name="showDiscard" value="N"/>
			<jsp:param name="noBR" value="Y"/>
		</jsp:include>
	</td>
</tr>
</table>
<%if(pageFrom.equals("H")){%>
<%if("Advrse_Form".equals(formType)|| "PatStudy_Form".equals(formType)){%>
<A href= "addeditquery.jsp?studyId=<%=studyId%>&formId=<%=formId%>&from=<%=from%>&filledFormId=<%=filledFormId%>&formType=<%=formType %>&fieldId=<%=advFieldId %>&dashBoard=<%=dashBoard%>"><%=MC.M_BackTo_QryBrowser%><%--Back to main query browser*****--%></A><!-- YK Bug#4822 19May2011  --><!-- vjha Added some variable when calling from jupiter 04Apr2015  -->	
<%}else if("PatStudy_Form".equals(formType)){ System.out.println("2.patstudy form block");%>
<%}else { %>
<A href= "addeditquery.jsp?studyId=<%=studyId%>&formId=<%=formId%>&from=<%=from%>&filledFormId=<%=filledFormId%>"><%=MC.M_BackTo_QryBrowser%><%--Back to main query browser*****--%></A><!-- YK Bug#4822 19May2011  -->	
<%}
}%>
<input type="hidden" name="thread" value="<%=thread%>">
<input type="hidden" name="filledFormId" value="<%=filledFormId%>">
<input type="hidden" name="formId" value="<%=formId%>">
<input type="hidden" name="from" value="<%=from%>">
<input type="hidden" name="formQueryId" value="<%=formQueryId%>">
<input type="hidden" name="queryType" value="<%=queryType%>">
<input type="hidden" name="queryStatusId" value="<%=queryStatusId%>">
<input type="hidden" name="fieldName" value="<%=fieldName%>">
<input type="hidden" name="mode" value="<%=mode%>">
<input type="hidden" name="studyId" value="<%=studyId%>">
<input type="hidden" name="calledFrom" value="<%=calledFrom%>">
<input type="hidden" name="adveventId" value="<%=adveventId%>">
<input type="hidden" name="advFieldId" value="<%=advFieldId%>">
<input type="hidden" name="formType" value="<%=formType%>">
<input type="hidden" name="site_id" value="<%=siteId%>">
<input type="hidden" name="dashBoard" value="<%=dashBoard%>">
<input type="hidden" name="pageRight" value="<%=pageRight %>">

</Form>
  <%

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>

</body>

</html>
<%!
private String getAdverseFieldName(int fieldId){
	String fieldName="";
	switch(fieldId){
	case 11012: fieldName="Adverse Event ID";
	break;
	case 11013: fieldName="Adverse Event Type";
    break;
	case 11014: fieldName="Category";
    break;
	case 11015: fieldName="Adverse Event Name";
    break;
	case 11016: fieldName="Toxicity";
    break;
	case 11017: fieldName="Toxicity Description";
    break;
	case 11018: fieldName="Severity/Grade";
    break;
	case 11019: fieldName="MedDRA code";
    break;
	case 11020: fieldName="Dictionary";
    break;
	case 11021: fieldName="Other Description";
    break;
	case 11022: fieldName="Treatment Course";
    break;
	case 11023: fieldName="Start Date";
    break;
	case 11024: fieldName="Stop Date";
    break;
	case 11025: fieldName="AE Discovery Date";
    break;
	case 11026: fieldName="AE Logged Date ";
    break;
	case 11027: fieldName="Entered By";
    break;
	case 11028: fieldName="Reported By";
    break;
	case 11029: fieldName="Attribution";
    break;
	case 11030: fieldName="Linked To ";
    break;
	case 11031: fieldName="Outcome Information";
    break;
	case 11032: fieldName="Action";
    break;
	case 11033: fieldName="Additional Information";
    break;
	case 11034: fieldName="The Following were Notified";
    break;
	case 11035: fieldName="Notes";
    break;
	case 11036: fieldName="Reason For Change (FDA Audit)";
    break;
	case 11037: fieldName="AE Status";
    break;
	case 11038: fieldName="Severity/Grade Description";
    break;
	case 11039: fieldName="Recovery Description";
    break;
	case 11040: fieldName="Outcome Notes";
    break;
	case 11041: fieldName="Adverse Event More Details";
    break;
	}
	return fieldName;
}
private String getPatStudyFieldName(int fieldId){
	String fieldName="";
	switch(fieldId){
	case 13012: fieldName="Attachment Status";
    break;
	case 13013: fieldName="Reason";
    break;
	case 13014: fieldName="Status Date";
    break;
	case 13015: fieldName="Notes";
    break;
	case 13016: fieldName="Next Follow-up Date";
    break;
	case 13017: fieldName="Patient Study ID";
    break;
	case 13018: fieldName="Enrolling Site";
    break;
	case 13019: fieldName="Assigned To";
    break;
	case 13020: fieldName="Physician";
    break;
	case 13021: fieldName="Treatment Location";
    break;
	case 13022: fieldName="Treating Organization";
    break;
	case 13023: fieldName="Disease Code";
    break;
	case 13024: fieldName="Anatomic Site";
    break;
	case 13025: fieldName="Evaluable Flag";
    break;
	case 13027: fieldName="Evaluable Status";
    break;
	case 13028: fieldName="Unevaluable Status";
    break;
	case 13029: fieldName="Status";
    break;
	case 13030: fieldName="Survival Status";
    break;
	case 13031: fieldName="Date of Death";
    break;
	case 13032: fieldName="Cause of Death";
    break;
	case 13033: fieldName="Specify Cause";
    break;
	case 13034: fieldName="Death Related to Study";
    break;
	case 13035: fieldName="Reason of Death Related to Study";
    break;
	case 13036: fieldName="Current Status";
    break;
	case 13037: fieldName="Screen Number";
    break;
	case 13038: fieldName="Screen By";
    break;
	case 13039: fieldName="Screen OutCome";
    break;
	case 13040: fieldName="Randomization Number";
    break;
	case 13041: fieldName="Enrolled By";
    break;
	case 13042: fieldName="Version No";
    break;
	
	}
	return fieldName;
}
%>

