<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></HEAD>



<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<%@ page import="com.velos.eres.audit.web.AuditRowEresJB,com.velos.esch.audit.web.AuditRowEschJB" %>
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
<jsp:useBean id="patEnrollB" scope="request" class="com.velos.eres.web.patProt.PatProtJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>



<%@ page language = "java" import = "com.velos.eres.business.common.*,com.velos.eres.service.util.*,java.util.*,com.velos.eres.business.patProt.*,com.velos.eres.web.patStudyStat.*,com.velos.esch.business.common.*"%>

<%
  int patProtPK  = 0;

  String mode = null;

   mode = request.getParameter("mode");

   String  accStartDt="";


	String protocolId=request.getParameter("protocolId");
	int saved = 0;

	String patientId = request.getParameter("patientId");
	String protocolStDt = request.getParameter("protStDate");

	String studyId= request.getParameter("studyId");
	String calldFrom = (request.getParameter("calldFrom")==null)?"":request.getParameter("calldFrom");
	String prevProtocol = request.getParameter("prevProtocolId");
	String eSign = request.getParameter("eSign");
	String calledFrom  = request.getParameter("calledFrom");

	String discDate = "";
	String newSelDay = null;
	String discReason = "";
	String dayFlag = "";
	String prevSelday = "";
	int oldpatProtPK = 0;
	String retUser = "";
	String retAlert = "";
	String retNot = "";
	String remarks = "";

	if(calledFrom.equals("disc"))
			{
				discDate = request.getParameter("discDate");
				discReason = request.getParameter("discReason");
				remarks = request.getParameter("remarks");

			}

	 retAlert = request.getParameter("retalerts");
	 if(retAlert == null) retAlert = "on";

	newSelDay = request.getParameter("selDay");
	prevSelday = request.getParameter("prevSelDay");

	if (EJBUtil.isEmpty(newSelDay) || "null".equals(newSelDay))
		newSelDay = null;

	if (EJBUtil.isEmpty(prevSelday ))
		prevSelday = null;

	HttpSession tSession = request.getSession(true);

	if (sessionmaint.isValidSession(tSession))

	{
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
		String oldESign = (String) tSession.getValue("eSign");

	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
	} else {

	String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = null;
	usr = (String) tSession.getValue("userId");


	int id = 0,ret=0;;

	if (protocolId == null)	 protocolId="";
	if (prevProtocol.equals("null")) prevProtocol="";


	if (protocolStDt == null) protocolStDt="";


	if (mode.equals("M"))
	{



		String prevprotStDate = request.getParameter("prevprotStDate");
		int personPK = EJBUtil.stringToNum(request.getParameter("personPK"));


		if (EJBUtil.isEmpty(prevProtocol))
		{
			prevProtocol = protocolId;
			prevprotStDate = protocolStDt;
			prevSelday = newSelDay;
		}

		if ((!(prevProtocol.equals(protocolId))) || (!(prevprotStDate.equals(protocolStDt))) 
				|| ((prevSelday ==  null && newSelDay != null) ||(prevSelday !=  null && newSelDay == null) 
				|| (prevSelday !=  null && newSelDay != null && !(prevSelday.equals(newSelDay)))))
		{



             Date st_date1 = DateUtil.stringToDate(protocolStDt,"");
	       java.sql.Date protStDate = DateUtil.dateToSqlDate(st_date1);

	        Date st_date2 = DateUtil.stringToDate(prevprotStDate,"");
	       java.sql.Date protStDate1 = DateUtil.dateToSqlDate(st_date2);

			eventdefB.DeactivateEvents(EJBUtil.stringToNum(prevProtocol),personPK,protStDate1,"P");

			/*if (mode.equals("M")){
				if (!StringUtil.isEmpty(remarks)){
					int userId = StringUtil.stringToNum((String)tSession.getAttribute("userId"));
					AuditRowEschJB schAuditJB = new AuditRowEschJB();
					schAuditJB.setReasonForChangeOfDeactiveEvents(personPK, StringUtil.stringToNum(prevProtocol), protStDate1, 
					userId, remarks);
				}
			}*/
	}

		if ((prevProtocol.equals(protocolId))&&(prevprotStDate.equals(protocolStDt)) 
				&& ((prevSelday ==  null && newSelDay == null)
				|| (prevSelday !=  null && newSelDay != null && (prevSelday.equals(newSelDay))))){


			patProtPK = EJBUtil.stringToNum(request.getParameter("patProtId"));
			oldpatProtPK = EJBUtil.stringToNum(request.getParameter("patProtId"));

			patEnrollB.setPatProtId(patProtPK);
		    patEnrollB.getPatProtDetails();
			if (protocolId.equals("")) protocolId = null;
					patEnrollB.setPatProtStartDt(protocolStDt );
					patEnrollB.setPatProtProtocolId(protocolId);
					patEnrollB.setPatProtStat("1"); //Stands for active
					patEnrollB.setModifiedBy(usr);
					patEnrollB.setIpAdd(ipAdd);
					patEnrollB.setPatProtStartDay(newSelDay);
			 	   saved = patEnrollB.updatePatProt();
		}else{


			patProtPK = EJBUtil.stringToNum(request.getParameter("patProtId"));
			oldpatProtPK = EJBUtil.stringToNum(request.getParameter("patProtId"));

			patEnrollB.setPatProtId(patProtPK);
		    patEnrollB.getPatProtDetails();
			patEnrollB.setPatProtStat("0"); //Stands for inactive
			patEnrollB.setModifiedBy(usr);
			patEnrollB.setIpAdd(ipAdd);

			//make changes for discontinuation details

			if(calledFrom.equals("disc"))
			{
				patEnrollB.setPatProtReason(discReason);
				patEnrollB.setPatProtDiscDt(discDate);
				//Modified by Manimaran on 6,july06 to fix the Bug:2599
				EventdefDao evedefdao=new EventdefDao();
				evedefdao.msgStatusUpdateForSchdiscontinue(EJBUtil.stringToNum(studyId),patProtPK);
			}

    		saved = patEnrollB.updatePatProt(); // update the previous status

			if (protocolId.equals("")) protocolId = null;


				patEnrollB.setPatProtStat("1"); //Stands for active
				patEnrollB.setPatProtStartDt(protocolStDt );
				patEnrollB.setPatProtProtocolId(protocolId);
				patEnrollB.setCreator(usr);
				patEnrollB.setIpAdd(ipAdd);
				patEnrollB.setPatProtStartDay(newSelDay);

				if(calledFrom.equals("disc"))
				{
   					patEnrollB.setPatProtReason("");
   					patEnrollB.setPatProtDiscDt("");
		   			patEnrollB.setPatProtStartDay(newSelDay);
				}

			  	patEnrollB.setPatProtDetails();
			   	patProtPK = patEnrollB.getPatProtId();


		}

		if ("M".equals(mode)){
			if (!StringUtil.isEmpty(remarks)){
				int userId = StringUtil.stringToNum((String)tSession.getAttribute("userId"));
				AuditRowEresJB eresAuditJB = new AuditRowEresJB();
				eresAuditJB.setReasonForChangeOfPatProt(oldpatProtPK, userId, remarks);
			}
		}

		//Sonia, 11/16/06 - generate new schedule, then we dont need generate button in patientschedule.jsp
		if (! StringUtil.isEmpty(protocolId))
		{
			Date dt1 = null;
			dt1 = DateUtil.stringToDate(protocolStDt,"");
			java.sql.Date dtProtStDate = DateUtil.dateToSqlDate(dt1);
			Integer selectedDisp = (null == newSelDay)? null : EJBUtil.stringToNum(newSelDay);
			eventdefB.GenerateSchedule(EJBUtil.stringToNum(protocolId),personPK,dtProtStDate,patProtPK,selectedDisp,usr,ipAdd);
		}

		
		//JM: 24Aug2010, #5203
		
		if (oldpatProtPK!=0){

			eventdefB.updatePatTxtArmsForTheNewPatProt(oldpatProtPK, patProtPK );

			if ("M".equals(mode)){
				if (!StringUtil.isEmpty(remarks)){
					int userId = StringUtil.stringToNum((String)tSession.getAttribute("userId"));
					AuditRowEresJB eresAuditJB = new AuditRowEresJB();
					eresAuditJB.setReasonForChangeOfAllTxArms(patProtPK, userId, remarks);
				}
			}
		}


	}
	  if (ret!=-3) {
	  if (saved == 0)
	  {

	  if(mode.equals("M") && oldpatProtPK != patProtPK)
	  {
	   if (retAlert.equals("on"))
	   {
	    eventdefB.copyNotifySetting(oldpatProtPK ,patProtPK,1 ,	EJBUtil.stringToNum(usr),
		ipAdd );
		}
		else
		{
		eventdefB.copyNotifySetting(0 ,	patProtPK,	0 ,	EJBUtil.stringToNum(usr),ipAdd );
		}
	  }

%>

<br>
<br>
<br>
<br>
<br>

<p class = "successfulmsg" align = center> <%=MC.M_Data_SavedSucc%><%--Data saved successfully*****--%> </p>

<%
}
else
{
%>
<br>
<br>
<br>
<br>
<br>
<p class = "successfulmsg" align = center> <%=MC.M_DataCnt_Svd%><%--Data could not be saved.*****--%></p>

<%
}
if("ucsdStdRoster".equals(calldFrom)){
%>
	  <script>
			//window.opener.location.reload();
			setTimeout("self.close()",1000);
	  </script>
<%}else{ %>
	  <script>
			window.opener.location.reload();
			setTimeout("self.close()",1000);
	  </script>
<%}
}// end if ret<>-3
}//end of if for eSign check
}//end of if body for session
else
{
%>
<jsp:include page="timeout.html" flush="true"/>
<%
}
%>

</BODY>
</HTML>

