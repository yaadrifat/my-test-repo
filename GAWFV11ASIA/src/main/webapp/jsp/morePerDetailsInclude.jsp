<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC" %>
<script type="text/javascript">
var morePatientDetFunctions = {
	formObj: {},

	openlookup: {},
	openMultiLookup: {},
	morePatientDetCodeJSON: {},
	getMorePatientDetCode: {},
	getMorePatientDetCodePK: {},
	getMorePatientDetField: {},

	setValue4ChkBoxGrp: {},
	setValue: {},
	setDD: {},
	validate: {},
	fixTextAreas: {}
};

morePatientDetFunctions.fixTextAreas = function(){
	
	//Maximum possible database limit is 4000 charcters
	var characters= 4000;
	$j(".mdTextArea").keyup(function(){
	    
		if($j(this).val().length > characters){
	        $j(this).val($j(this).val().substr(0, characters));
		}	
	});
}

morePatientDetFunctions.openLookup = function (viewId, keyword){
	if (!viewId) return;
	var dfilter = '';
	var formName = morePatientDetFunctions.formObj.name;
	windowName = window.open("getlookup.jsp?viewId="+viewId+"&form="+formName+"&dfilter="+dfilter+"&keyword="+keyword,"Information",
			"toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=550 top=100,left=100 0, ");
	windowName.focus();
};

morePatientDetFunctions.openMultiLookup = function (viewId, keyword){
	if (!viewId) return;
	var dfilter = '';
	var formName = morePatientDetFunctions.formObj.name;
	windowName = window.open("multilookup.jsp?viewId="+viewId+"&form="+formName+"&seperator=,&dfilter="+dfilter+"&keyword="+keyword,"Information",
			"toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=550 top=100,left=100 0, ");
	windowName.focus();
};

morePatientDetFunctions.setValue4ChkBoxGrp = function(obj,iElementId) {
	var chkFlds = document.getElementsByName('alternateId'+iElementId+"Checks");
	var hiddenInputFld =  document.getElementById('alternateId'+iElementId);
	hiddenInputFld.value = '';
	for (var indx = 0; indx < chkFlds.length; indx++){
		if (chkFlds[indx].checked){
			var checkValue = chkFlds[indx].value;
			if (hiddenInputFld.value.length==0){
				hiddenInputFld.value = checkValue;
			} else {
				hiddenInputFld.value += ','+checkValue;
			}
		}
	}
};

morePatientDetFunctions.setValue = function(iElementId,cbcount){
	var chkFld = document.getElementById('alternateIdCheck'+cbcount);
	var value = chkFld.value;
	if (value=="Y") {
		chkFld.value="N";
		chkFld.checked=false;
	} else if ((value.length==0) || (value=="N"))  {
		chkFld.value="Y";
		chkFld.checked=true;
	} else { // <== there is some junk data in the DB column already
		chkFld.value="Y";
		chkFld.checked=true;
	}
};

morePatientDetFunctions.setDD = function (){
	var formobj = morePatientDetFunctions.formObj;
	var optvalue=formobj.ddlist.value;
	if (optvalue.length>0)
	{
		var arrayofStrings=optvalue.split("||");
		if (arrayofStrings.length>1) {
			for (var j=0;j<arrayofStrings.length;j++)
			{
				var ddStr=arrayofStrings[j];
				var arrayofDD=ddStr.split(":");
				var ddcount=arrayofDD[0];
				var selvalue=arrayofDD[1];
				var ddFld = formobj['alternateId'+ddcount];
				if (ddFld && ddFld.options) {
					var opt = ddFld.options;
					for (var i=0;i<opt.length;i++){
						if (opt[i].value==selvalue){
							ddFld.selectedIndex=i ;
						}
					}
				}
			}
		} else {
			var ddStr=arrayofStrings[0];
			var arrayofDD=ddStr.split(":");
			var ddcount=arrayofDD[0];
			var selvalue=arrayofDD[1];
			var ddFld = formobj['alternateId'+ddcount];
			if (ddFld && ddFld.options) {
			 	var opt = ddFld.options; 
				if (opt == undefined)
			    	opt = ddFld.options; 
				
				for (var i=0;i<opt.length;i++){
					if (opt[i].value==selvalue){
						ddFld.selectedIndex=i ;
					}
				}
			}
		}// end else
	}//optvalue.length>0	
};

morePatientDetFunctions.validate = function() {
	var formobj = morePatientDetFunctions.formObj;
 	 if (!(validate_col('e-Signature',formobj.eSign))) return false;

   <%--   if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
	   } --%>

	return true;
};
</script>
<script type="text/javascript">
jQuery(document).ready( function() {
	if (document.patientEnrollForm){
		morePatientDetFunctions.formObj = document.patientEnrollForm;
	} else {
		morePatientDetFunctions.formObj = document.patientDemogForm;
	}
	
	morePatientDetFunctions.setDD(morePatientDetFunctions.formObj);
	morePatientDetFunctions.fixTextAreas();
});
</script>