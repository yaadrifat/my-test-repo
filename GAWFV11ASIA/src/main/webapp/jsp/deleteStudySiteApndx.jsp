<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">



<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	<%-- if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   } --%>
}
</SCRIPT>
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<jsp:useBean id="studySiteApndxB" scope="request" class="com.velos.eres.web.studySiteApndx.StudySiteApndxJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import ="com.velos.eres.business.common.*,java.util.*, com.velos.eres.service.util.*,com.aithent.audittrail.reports.AuditUtils,com.velos.eres.service.util.MC" %>
<jsp:include page="include.jsp" flush="true"/>
<BODY>
<DIV class="popDefault" id="div1">
<%
 

HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession))	{
	String studySiteApndxId = request.getParameter("studySiteApndxId");
	String studySiteId = request.getParameter("studySiteId");
	String studyId = request.getParameter("studyId");


	String delMode=request.getParameter("delMode");
	
%>
	<FORM name="deleteSite" id="delSiteApndx" method="post" action="deleteStudySiteApndx.jsp" onSubmit="if (validate(document.deleteSite)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<br><br>
	<%if (delMode==null) {
		delMode="final";%>	
	<P class="defComments"><%=MC.M_EsignToProc_WithFileDel%><%-- Please enter e-Signature to proceed with File/Link Delete.*****--%></P>	
	

<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="delSiteApndx"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>


 	 <input type="hidden" name="delMode" value="<%=delMode%>">  	
	 <input type="hidden" name="studySiteApndxId" value="<%=studySiteApndxId%>">
	 <input type="hidden" name="studySiteId" value="<%=studySiteId%>">
	 <input type="hidden" name="studyId" value="<%=studyId%>">
		  
	</FORM>
<%
	} else {
			String eSign = request.getParameter("eSign");	
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>
	 		  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
			} else {
	studySiteApndxB.setStudySiteApndxId(EJBUtil.stringToNum(studySiteApndxId));

	int i=0;
	// Modified for INF-18183 ::: AGodara 
	i=studySiteApndxB.removeStudySiteApndx(AuditUtils.createArgs(tSession,"",LC.L_Study));    //Changed for Bug#7604 : Raviesh
	%><br><br><br><br><br><p class = "successfulmsg" align = center>
			<%=MC.M_Data_DelSucc%><%-- Data deleted successfully*****--%></p>	
		
	<META HTTP-EQUIV=Refresh CONTENT="1; URL=newOrganization.jsp?mode=M&studyId=<%=studyId%>&studySiteId=<%=studySiteId%>">
<%
	} //end esign
	} //end of delMode	
}//end of if body for session

else {
%>
<jsp:include page="timeout.html" flush="true"/> 
<%
}
%>

<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</BODY>

</HTML>





