<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Update_FldAction%><%--Update Field Action*****--%></title>

<%@ page import="com.velos.eres.service.util.*,java.util.*,com.velos.eres.business.common.*, com.velos.eres.business.fieldAction.* " %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<head>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import = "java.util.*,com.velos.eres.business.common.*"%>

<body>
<br>
<DIV id="div1"> 

<%
	HttpSession tSession = request.getSession(true);
	String codeStatus = request.getParameter("codeStatus");
	if (sessionmaint.isValidSession(tSession))
	{

%>	
		<jsp:include page="sessionlogging.jsp" flush="true"/>
 <%   
	/*	String eSign= request.getParameter("eSign");
    	String oldESign = (String) tSession.getValue("eSign");
	*/
		String param = "";
		String paramVal = "";
		String paramName = "";
		String mode= "";
		int ret = 0;
		
		FieldActionStateKeeper fsk = new FieldActionStateKeeper();
		FieldActionDao fldActionDao = new FieldActionDao();

		   
			String eSign= request.getParameter("eSign");
			String oldESign = (String) tSession.getValue("eSign");
			if((codeStatus.equals("WIP")) || ((!codeStatus.equals("WIP") && !codeStatus.equals("")) && (oldESign.equals(eSign)))){

	   		String formId = "";
			String actionType = "";
			String actionCondition  = "";
			String sourceField = "";
		 	Hashtable htFldActionInfo = new Hashtable();
			String[] arrVal ;
			String fieldActionId = "";
		
			Enumeration paramNames = request.getParameterNames();
			
			ArrayList alParam = new ArrayList ();
		
			while (paramNames.hasMoreElements())
			{
	   			param = (String) paramNames.nextElement();
				
				if ((param.toLowerCase()).startsWith("vel"))
				{
					if ( ! alParam.contains(param))
					{
						alParam.add(param);
						System.out.println("param..>" + param);
					}
				}	
			
			}

			for (int k = 0; k < alParam.size() ; k++ )
			{
	   			paramName = (String) alParam.get(k);
				ArrayList alParamVal = new ArrayList ();
				
				
				if ( request.getParameterValues(paramName) != null ) 
				{				
					paramVal = "";
	
    				for (int i = 0; i < request.getParameterValues(paramName).length; i++ ) 
    				{						
						paramVal = request.getParameterValues(paramName)[i] ;
			 	 	   //System.out.println("param*********" + paramName);
			 	 	   //System.out.println("param...." + paramVal);
						//check if the paramVal has values separated by [VELSEP], if yes, make them as separate values
						if (paramVal.indexOf("[VELSEP]") >= 0)
						{		
						 arrVal = StringUtil.strSplit (paramVal, "[VELSEP]");	
						
						 //iterate through array
						 	 for (int cnt = 0; cnt < arrVal.length; cnt++)
						 	 {
						 	 	  alParamVal.add(arrVal[cnt]);
						 	 	 // System.out.println("param..split value.." + arrVal[cnt]);
						 	 }
						}
						else
						{
							alParamVal.add(paramVal);
						}	
										
    				}
				}
				if (alParamVal != null){
					htFldActionInfo.put("[" + paramName.toUpperCase() + "]",alParamVal);
				}
					
			}
			
				
	//get data from session

		String ipAdd = (String) tSession.getValue("ipAdd");
        String usr = (String) tSession.getValue("userId");
	    String accountId = (String) tSession.getValue("accountId");
	    

	//get parameters for statekeeper
		
		 formId = request.getParameter("formId");
		 actionType = request.getParameter("actionType");
		 actionCondition = request.getParameter("actionCondition");
		 sourceField =	   request.getParameter("velsourcefldid");
		 fieldActionId =   request.getParameter("fieldActionId");
		 
		 mode = request.getParameter("mode");
		 fsk.setFldForm(formId );
		 fsk.setFldActionType(actionType);
		 fsk.setFldActionCondition(actionCondition);
		 
		 if (paramVal.indexOf("[VELSEP]") < 0)
		  {		
			 fsk.setFldId(sourceField );
		  } 
		  
		 fsk.setFldActionInfo(htFldActionInfo);
		 
		 if (mode.equals("N"))
		 {
			 fsk.setCreator(usr);
		 }
		 else
		 {
		 	 fsk.setFldActionId(EJBUtil.stringToNum(fieldActionId ));
		 	 fsk.setModifiedBy(usr);
		 	
		 }	 
		 fsk.setIpAdd(ipAdd);	

	// save the field Action
		
		ret = fldActionDao.setFieldActionData(fsk,mode);
		
		if (ret>=0) {
			
			//generate the action script
			fldActionDao.generateFieldActionsScript(formId);		
		%>
 		
			
		<BR><BR><BR><BR><BR>
		<P class="successfulmsg" align="center"><%=MC.M_Data_SavedSucc%><%--Data Saved Successfully--%></P>
		<script>
			window.opener.location.reload();
			setTimeout("self.close()",1000);
		</script>

		<% } else {%>		
		<BR><BR><BR><BR><BR>
		<P class="successfulmsg" align="center"><%=MC.M_Data_NotSvdSucc%><%--Data not Saved Successfully*****--%></P>
		
		<%
		}//end of else for Error Code Msg Display
			}//end of if old esign
			else{  
		%>
				<jsp:include page="incorrectesign.jsp" flush="true"/> 
		<%	  
					}//end of else of incorrect of esign */
		     }//end of if body for session 	
			
			else
	{
%>
	  <jsp:include page="timeout.html" flush="true"/>
<%
	
} //end of else of the Session 
 

%>
</div>
</body>
</html>
