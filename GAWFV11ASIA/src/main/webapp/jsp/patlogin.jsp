<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Pat_Login%><%--<%=LC.Pat_Patient%> Login*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<jsp:include page="include.jsp" flush="true"/>
</head>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="whichcss.js"></SCRIPT>

<SCRIPT>
function callEnable1(){

	document.getElementById('patPassword').value = "";
	document.getElementById('confpatPassword').value= "";

	if (document.getElementById('chkpwds').checked==true){
		document.getElementById('patPassword').readOnly = false;
		document.getElementById('confpatPassword').readOnly = false;
		document.getElementById('chkpwds').value = 1;
	}
	else {
		document.getElementById('patPassword').readOnly = true;
		document.getElementById('confpatPassword').readOnly = true;
		$j("#passPortion").remove();
		document.getElementById('chkpwds').value = 0;
	}
}




function callDisable1(){

	if (document.getElementById('mode').value =='M'){
		document.getElementById('patPassword').readOnly = true;
		document.getElementById('confpatPassword').readOnly = true;
		//JM: added on 07Aug2007 #3082
		if ((document.getElementById('chkpwds').checked==true))
		 		document.getElementById('chkpwds').checked=false;

	 }

}


function callDisable(){

	if (document.getElementById('mode').value =='M'){		
		if ((document.getElementById('chkpwds').checked==true)){
			document.getElementById('chkpwds').checked=false;
		}		 		

	 }

}

function  validate(formobj){
		var defPassFlag = formobj.defPassFlag.value;
		var createLoginFlag = formobj.createLoginFlag.value;


			if (formobj.mode.value == 'M'){

				 if (formobj.chkpwds.checked) {
				     formobj.isChecked.value=1;
				} else {
				    formobj.isChecked.value=0;
				}
        		// Added by Amarnadh to fix the issue #3055 trim leading  and/or  trailing spaces for the Login Name
				formobj.patLogin.value=fnTrimSpaces(formobj.patLogin.value);

			}  else {
				if (formobj.mode.value == 'N'){

					formobj.patLogin.value=fnTrimSpaces(formobj.patLogin.value);
				}

			}



				//if (!(validate_col('email',formobj.email))) return false

				if (!(validate_col('Login',formobj.patLogin))) return false;


				if (formobj.mode.value != 'M' && defPassFlag=="0"){
					if (!(validate_col('Password',formobj.patPassword))) return false;
					if (!(validate_col('Password',formobj.confpatPassword))) return false;
				}

				if (!(validate_col('logout-time',formobj.logOutTime))) return false;
				if (!(validate_col('patStat',formobj.patStat))) return false;
				if (!(validate_col('e-Signature',formobj.eSign))) return false;


				//JM: 10Sep2007: commented above and added below, issue #3080
				if (!isEmpty(formobj.email.value))
				{
					if(formobj.email.value.search("@") == -1) {
						alert("<%=MC.M_EtrValid_EmailAddr%>");/*alert("Please enter a valid email address.");*****/
						formobj.email.focus();
						return false;
			 		}
				}

				//Portal Login Password Validation Check for INF-18669 : Raviesh
  				var isSubmitFlag=$j("#isSubmitFlag").val();
  				if(isSubmitFlag=="false"){
  					alert("<%=MC.M_PlzEtr_validPass%>");
  					formobj.patPassword.focus();
  					return false;
  				}

				if (formobj.mode.value != 'M' ){

					if (!isEmpty(formobj.patPassword.value) || !isEmpty(formobj.confpatPassword.value))
					{
						if(formobj.patPassword.value != formobj.confpatPassword.value){
							alert("<%=MC.M_PwdConfirmPwd_NotSame%>");/*alert("Values in 'Password' and 'Confirm Password' are not same. Please enter again.");*****/
							formobj.patPassword.focus();
							return false;
						}

						if ( formobj.patPassword.value.indexOf(' ') == 0 || ( formobj.patPassword.value.lastIndexOf(' ') ==  (formobj.patPassword.value.length - 1) ) )	{
						    alert("<%=MC.M_PwdCntLeadTrlSpace_ReEtr%>");/*alert("Password cannot contain leading or trailing spaces. Please enter again.");*****/
						    formobj.patPassword.focus()
						    return false;
						}

						if( formobj.patPassword.value.length < 8){
							alert("<%=MC.M_PwdMust8CharLong_ReEtr%>");/*alert("Password must be atleast 8 characters long. Please enter again.");*****/
							formobj.patPassword.focus();
							return false;
						}


						if( fnTrimSpaces(formobj.patPassword.value) == fnTrimSpaces(formobj.patLogin.value)){
							alert("<%=MC.M_PwdCntSameLogin_ReEtr%>");/*alert("Password cannot be same as Login Name. Please enter again.");*****/
							formobj.patPassword.focus()
							return false;
						}

					}


				}else{
				 if (document.getElementById('chkpwds').checked==true){

				 	if (!(validate_col('Password',formobj.patPassword))) return false
					if (!(validate_col('Password',formobj.confpatPassword))) return false

					if(formobj.patPassword.value != formobj.confpatPassword.value){
						alert("<%=MC.M_PwdConfirmPwd_NotSame%>");/*alert("Values in 'Password' and 'Confirm Password' are not same. Please enter again.");*****/
						formobj.patPassword.focus();
						return false;
					}
					if ( formobj.patPassword.value.indexOf(' ') == 0 || ( formobj.patPassword.value.lastIndexOf(' ') ==  (formobj.patPassword.value.length - 1) ) )	{
						alert("<%=MC.M_PwdCntLeadTrlSpace_ReEtr%>");/*alert("Password cannot contain leading or trailing spaces. Please enter again.");*****/
					    formobj.patPassword.focus()
					    return false;
					}
					if( formobj.patPassword.value.length < 8){
						alert("<%=MC.M_PwdMust8CharLong_ReEtr%>");/*alert("Password must be atleast 8 characters long. Please enter again.");*****/
						formobj.patPassword.focus();
						return false;
					}
					if( formobj.patPassword.value == formobj.patLogin.value){
						alert("<%=MC.M_PwdCntSameLogin_ReEtr%>");/*alert("Password cannot be same as Login Name. Please enter again.");*****/
						formobj.patPassword.focus()
						return false;
					}
				 }
				// formobj.repatPassword.value = formobj.patPassword.value;
				}
				if(isNaN(formobj.logOutTime.value) == true) {
					alert("<%=MC.M_LoutTimeHasToNum_EtrValid%>");/*alert("Logout Time has to be a Number. Please enter a valid Number");*****/
					formobj.logOutTime.focus();
		 			return false;
   				}


				<%-- if(isNaN(formobj.eSign.value) == true){
					alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
        			formobj.eSign.focus();
        			return false;
  				} --%>
  				//JM: 16NOV2010: #3080
  				callDisable();


		}
</SCRIPT>

<script>
$j(document).ready(function(){
	$j(".passwordCheck").passStrength({
		shortPass: 		"validation-fail",
		valid    :      "validation-pass",
		Invalid  :      "validation-fail",
		okPass:         "",
		messageloc:     1,
		comeFrom:       3 
		
	});
})
</script>

<jsp:useBean id="patloginJB" scope="request" class="com.velos.eres.web.patLogin.PatLoginJB"/>
<jsp:useBean id="personB" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.DateUtil,com.velos.eres.business.common.*,java.text.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.business.person.impl.PersonBean,com.velos.eres.web.grpRights.GrpRightsJB"%>
<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>

<%
int ienet = 2;
boolean hasAccess  = false;

String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1)
   	ienet = 0; //IE
   	else
	ienet = 1;

if (ienet == 0) {
%>
<body style="overflow:scroll">
<%} else {%>
<body>
<%}
String tab= request.getParameter("selectedTab");
HttpSession tSession = request.getSession(true);

String userId="";
String acc="";
if (sessionmaint.isValidSession(tSession))
{
	acc = (String) tSession.getValue("accId");
	userId = (String) tSession.getValue("userId");

	int perId = 0;
	perId = EJBUtil.stringToNum(request.getParameter("personId"));

//	JM: 14Jun2007
	int pageRight = 0;
	GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");
	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("PORTALAD"));



	String portalId = request.getParameter("portalId");//er_portal_logins.fk_portal OR er_portal.pk_portal

	int portalLoginId = 0;
	portalLoginId = EJBUtil.stringToNum(request.getParameter("portalLoginId"));//pk of er_portal_logins

	String defPassFlag = request.getParameter("defPassFlag");
	String createLoginFlag =  request.getParameter("createLoginFlag");
	String loginReadonly = "";

	if (StringUtil.isEmpty(defPassFlag ))
	{
		defPassFlag = "0";
	}

	if (StringUtil.isEmpty(createLoginFlag ))
	{
		createLoginFlag = "0";
	}

	String loginReadOnly = "";

	if (createLoginFlag.equals("1"))
	{
		loginReadonly = " READONLY ";
	}

	String ipAdd = (String) tSession.getValue("ipAdd");

	String  plId = "";
	String  plIdType = "";
	String  plEmail = "";
	String  plLogin = "";
	String  plPassword = "";
	String  plStatus = "";
	String  plLogOutTime = "15"; //default
	String  fkPortal = "";
	String portalObjTyp = request.getParameter("portalObjType");

	int compRight = EJBUtil.stringToNum(request.getParameter("comp"));




	  String mode = "";
	  mode = request.getParameter("mode");

	   String dStatus = "";
	   CodeDao cDao = new CodeDao();

		cDao.getCodeValues("patLoginStat");

		String pstat = request.getParameter("patStat");

		if (pstat==null) pstat="";

		if (pstat.equals("")){
			dStatus=cDao.toPullDown("patStat");
		}
		else{
			dStatus=cDao.toPullDown("patStat",EJBUtil.stringToNum(pstat),true);
		}

	     personB.setPersonPKId(perId);
     	 personB.getPersonDetails();
     	 String perEmail = personB.getPersonEmail();
     	 perEmail=(perEmail==null)?"":perEmail;


	      if (mode.equals("M")) {
	      patloginJB.setId(portalLoginId);
  		  patloginJB.getPatLoginDetails();
		  plId = patloginJB.getPlId();
		  plIdType = patloginJB.getPlIdType();
		  plLogin = patloginJB.getPlLogin();
		  plLogin=(plLogin==null)?"":plLogin;

		  plPassword = patloginJB.getPlPassword();
		  plPassword=(plPassword==null)?"":plPassword;

		  plStatus = patloginJB.getPlStatus();
		  plLogOutTime = patloginJB.getPlLogOutTime();
	      dStatus=cDao.toPullDown("patStat",EJBUtil.stringToNum(plStatus),true);
//	      fkPortal = patloginJB.getFkPortal();
	      }



		String emailFld="";

		if (compRight >=4)
		{
			emailFld = "text";
		}
		else
		{
			emailFld  = "hidden";
		}


%>
<BR>
<DIV class="popDefault" id="div1">


	<Form  id="patientlogin_form" name="patientlogin" method="post" action="updatepatlogin.jsp"  onsubmit = "if (validate(document.patientlogin)==false) {setValidateFlag('false');return false;} else {setValidateFlag('true'); return true;}">
<BR>
<P class = "sectionHeadings">
<%=LC.L_Pat_Login%><%--<%=LC.Pat_Patient%> Login*****--%>
</P>
<input type="hidden" name="perId" Value="<%=perId%>"/>
<input type="hidden" name="portalId" id="portalId" Value="<%=portalId%>"/>
<input type="hidden" name="portalLoginId" id="portalLoginId" Value="<%=portalLoginId%>"/>
<input type="hidden" id = "mode" name="mode" Value="<%=mode%>"/>
<input type="hidden" name="ipAdd" Value="<%=ipAdd%>"/>
<input type="hidden" name="portalObjTyp" Value="<%=portalObjTyp%>"/>
<input type="hidden" name="defPassFlag" value="<%=defPassFlag%>"/>
<input type="hidden" name="createLoginFlag" value="<%=createLoginFlag%>"/>
<input type="hidden" name="patLogin1" id="patLogin1" value="<%=plLogin%>"/>

<table width="80%" cellspacing="0" cellpadding="0" border=0>
	<tr>
		<td><%=LC.L_Email_Address%><%--E-Mail Address*****--%></td>
		<td>
			<input type="<%=emailFld%>" name="email" size = "30" MAXLENGTH = 100 value="<%=perEmail%>"/>

		<% if (compRight < 4)
			{
				out.println("****");
			}
			%>
		</td>
	</tr>

	<tr>
		<td><%=LC.L_Login_Name%><%--Login Name*****--%><FONT class="Mandatory">* </FONT></td>
		<td>
			<input type="text" name="patLogin"  id="patLogin" size = "30" MAXLENGTH = 30  <%=loginReadonly%> value="<%=plLogin%>"/>
		</td>
	</tr>


	<% if (mode.equals("M")){ %>
	<tr>
		<td><%=LC.L_Change_PatPwd%><%--Change <%=LC.Pat_Patient%> Password*****--%></td>

	 <%
		 String disableCHK = "";
	 	if (createLoginFlag.equals("1")) {
	 		disableCHK = " DISABLED ";
	 	}

	 %>
		<td><input type="checkbox" <%=disableCHK%> onclick ="return callEnable1();" name="chkpwds" id="chkpwds" value="0"/></td>
	</tr>
	<%} %>

	<% if (mode.equals("N"))
	{
		if (defPassFlag.equals("1"))
		{
	 %>
		<tr> <td colspan="4"><p class="defComments"><%=MC.M_PortalPwd_ProvPwdOverride%><%--This portal has a ''Default Password'' specified. In this case Password is not mandatory. Please provide a password only if you wish to override the default password*****--%>: </p></td></tr>
	 <% }%>
	<tr>
		<td width="30%"><%=LC.L_Password%><%--Password*****--%><FONT class="Mandatory">* </FONT></td>
		<td width="70%">
			<input type="password" name="patPassword" id="patPassword"  class="passwordCheck" size = "25" MAXLENGTH = "15"   />
		</td>
	</tr>
	
	<tr>
		<td >
			<A HREF="pwdtips.htm" target="_new"><%=MC.M_SelcPwdCrt%></A>
		</td>
	</tr>

	<tr>
		<td><%=LC.L_Confirm_Pwd%><%--Confirm Password*****--%><FONT class="Mandatory">* </FONT></td>
		<td>
			<input type="password" name="confpatPassword" id="confpatPassword"  size = "25" MAXLENGTH = "15" />
		</td>
	</tr>
	<% }

	if (mode.equals("M")){ %>

	<tr>
		<td width="30%"><%=LC.L_Password%><%--Password*****--%></td>
		<td width="70%">
			<input type="password" name="patPassword" id="patPassword" class="passwordCheck" size = "25" MAXLENGTH = "15"  />
		</td>
	</tr>
	
	<tr>
		<td >
			<A HREF="pwdtips.htm" target="_new"><%=MC.M_SelcPwdCrt%></A>
		</td>
	</tr>

	<tr>
		<td><%=LC.L_Confirm_Pwd%><%--Confirm Password*****--%></td>
		<td>
			<input type="password" name="confpatPassword" id ="confpatPassword"   size = "25" MAXLENGTH = "15" />
		</td>
	</tr>
	<tr>
		<td>
			<input type="hidden" name="hidPatPassword" id ="hidPatPassword"  size = "25" MAXLENGTH = "15"  value="<%=plPassword%>"/>
		</td>
	</tr>
	<%} %>
	<tr>
		<td>
			<input name="isChecked" type="hidden"  value=""/>
		</td>
	</tr>
	<tr>
		<td><%=LC.L_Logout_Time%><%--Logout Time*****--%><FONT class="Mandatory">* </FONT></td>
		<td>
			<input type="text" name="logOutTime"  size = "25" MAXLENGTH = "3"  value="<%=plLogOutTime%>" />
		</td>
	</tr>

	<tr>
		<td><%=LC.L_Status%><%--Status*****--%><FONT class="Mandatory">* </FONT></td>
		<td>
		<%=dStatus%>
		</td>
	</tr>
</table>
	<% //KM-#4765
		if ( pageRight>=6 ) {
 	hasAccess = true; %>
			<jsp:include page="submitBar.jsp" flush="true">
				<jsp:param name="displayESign" value="Y"/>
				<jsp:param name="formID" value="patientlogin_form"/>
				<jsp:param name="showDiscard" value="N"/>
			</jsp:include>
	<%} %>
	
	<input type="hidden" id="isSubmitFlag" value="" />
	
</Form>

<%	} //end of if session times out

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

} //end of else body for session time out

%>

<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>

</div>
</body>

<script>callDisable1();</script>



</html>
