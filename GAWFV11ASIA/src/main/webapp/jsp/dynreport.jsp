<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title><%=LC.L_Dynamic_RptCreation%><%--Dynamic Report Creation*****--%></title>
	<%@ page language = "java" import = "java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB"%>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<jsp:useBean id="dynrepB" scope="page" class="com.velos.eres.web.dynrep.DynRepJB" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<SCRIPT>
window.name = "dynrep";
var formWin = null;
 function  validate(formobj){
     if (!(validate_col('eSign',formobj.eSign))) return false


// 	if(isNaN(formobj.eSign.value) == true) {

<%-- 	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect E-Signature. Please enter again");*****/ --%>

// 	formobj.eSign.focus();

// 	return false;
//    }

}
function openFormWin(formobj) {
formId=formobj.formId.value;
prevId=formobj.prevId.value;
if (formId==prevId)
{}
else
{
alert("<%=MC.M_SelFlds_ForFrm%>");/*alert("Please Select the Fields for Selected Form.");*****/
return false;
}
formobj.target="formWin";
formobj.action="dynpreview.jsp"
formWin = open('','formWin','resizable=1,status=0, width=850,height=550 top=100,left=100,scrollbars=1');
if (formWin && !formWin.closed) formWin.focus();
//formobj.submit();
document.dynreport.submit();
void(0);
}


function openLookup(formobj){
accId=formobj.accId.value;
userId=formobj.userId.value;
studyId=formobj.study1.value;
if (studyId.length==0){
alert("<%=MC.M_PlsSel_StdFirst%>");/*alert("Please Select a Study First");*****/
return;
}
 
//filter1=" F2.FK_ACCOUNT ="+ accId + " AND F2.FK_FORMLIB = F1.PK_FORMLIB and " +   
//" F2.record_type <> 'D'   AND F4.FK_FORMLIB = F1.PK_FORMLIB   and F4.formstat_enddate IS NULL " +  
// "  and lf_displaytype IN ( 'S','SP','A','SA','PA' )  and F4.fk_codelst_stat in (select pk_codelst from er_codelst where lower(codelst_desc)='active') " ;
    //alert(filter);
    filter=" ab.Fk_study= "+ studyId + "  and  ab.FK_FORMLIB=b.pk_formlib and e.FK_FORMLIB=b.pk_formlib and " +  
 	 "  ab.record_type <> 'D'  and e.formstat_enddate IS NULL   "  ;
windowname=window.open("getlookup.jsp?viewId=&viewName=Form Browser&form=dynreport&dfilter=" + filter + "&keyword=formName|VELFORMNAME~formId|VELFORMPK" ,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=550 top=100,left=100 0, "); 
windowname.focus();	
}
function selectFlds(formobj){
formId=formobj.formId.value;
if (formId.length==0){
alert("<%=MC.M_Selc_Frm%>");/*alert("Please select a form");*****/
return false ;
}

formName=formobj.formName.value;
src=formobj.srcmenu.value;
selectedTab=formobj.selectedTab.value;
repHeader=formobj.repHeader.value;
repFooter=formobj.repFooter.value;
windowname=window.open("dynrepselect.jsp?formId=" +formId + "&srcmenu=" + src+"&selectedTab="+selectedTab+"&formName="+formName+"&repHeader="+repHeader+"&repFooter="+repFooter  ,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=850,height=550 top=100,left=100 0, "); 
windowname.focus();
}
</SCRIPT>
<% String src,tempStr="",fldName="",fldCol="";
   int strlen=0,firstpos=-1,secondpos=-1;
src= request.getParameter("srcmenu");
System.out.println(src);

%>



<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>   

<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1) 
   	ienet = 0; //IE
else
	ienet = 1;

if (ienet == 0) {
%>
<body style="overflow:scroll">
<%} else {%>
<body>
<%}%>
<DIV class="formDefault" id="div1"> 
<%
        String formId="",formName="",formType="";
	String mode = request.getParameter("mode");
	String selectedTab = request.getParameter("selectedTab");
	ArrayList selectedfld=null;
	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))
	{
	 formId= request.getParameter("formId");
	 formName=request.getParameter("formName");
	 formType=request.getParameter("formType");
	 String accId = (String) tSession.getValue("accountId");
	 String userId=	(String) tSession.getValue("userId");
	 String userIdFromSession = (String) tSession.getValue("userId");
	 if( formName==null) formName="";
	 if( formId==null) formId="";
	 int counter=0;

	int studyId=0;

	StringBuffer study=new StringBuffer();	

	StudyDao studyDao = new StudyDao();	

	studyDao.getReportStudyValuesForUsers(userId);



	study.append("<SELECT NAME=study1>") ;

	for (counter = 0; counter <= (studyDao.getStudyNumbers()).size() -1 ; counter++){

		studyId = ((Integer)((studyDao.getStudyIds()).get(counter))).intValue();

		study.append("<OPTION value = "+ studyId +">" + (studyDao.getStudyNumbers()).get(counter)+ "</OPTION>");

	}

	study.append("<OPTION value=''  selected>"+LC.L_Select_AnOption/*Select an Option*****/+"</OPTION>");

	study.append("</SELECT>");

 System.out.println(study);

	 
%>

<P class="sectionHeadings"> <%=LC.L_DynaRpt_Creat%><%--Dynamic Report >> Create*****--%> </P>

<%

	if (mode.equals("M")) {
		
	}

%>

<form name="dynreport" METHOD=POST  onsubmit="return validate(document.dynreport);">
<input type=hidden name="mode" value=<%=mode%>>
<input type=hidden name="srcmenu" value=<%=src%>>
<input type=hidden name="selectedTab" value=<%=selectedTab%>>
<input type=hidden name="accId" value="<%=accId%>">
<input type=hidden name="userId" value="<%=userIdFromSession%>">
<input type="hidden" name="formId" readonly value="<%=formId%>">
<input type="hidden" name="formType" readonly value="<%=formType%>">
<input type="hidden" name="prevId" readonly value="<%=formId%>">

<table width="100%" >
    <tr><td width="15%"><%=LC.L_Select_AStd%><%--Select a Study*****--%></td><td width="50%"><%=study%></td></tr>
    <tr><td width="15%"><%=LC.L_Frm_Name%><%--Form Name*****--%></td><td width="50%"><input type="text" name="formName" readonly value="<%=formName%>"><A href="#" onClick="openLookup(document.dynreport)"><%=LC.L_Select%><%--Select*****--%></A></td></tr>
    <!--<tr><td>Report Name</td><td><input type="text" name="repName" value=""></td></tr>-->
    </table>
    <%if (mode.equals("fldselect")){
    	String select[]=request.getParameterValues("select");
	selectedfld=EJBUtil.strArrToArrayList(select);
	%>
<P class="sectionHeadings"> <%=LC.L_Selected_Flds%><%--Selected Fields*****--%> </P>	
<table width="100%" Border="1">
  <tr>
  <TH width="25%"><%=LC.L_Selected_Flds%><%--Selected Fields*****--%></TH>
  <TH width="35%"><%=LC.L_Display_Name%><%--Display Name*****--%></TH>
  <TH width="10%"><%=LC.L_Sort_By%><%--Sort By*****--%></TH>
  <TH width="5%"><%=LC.L_Width%><%--Width*****--%></TH>
   </tr>
    <%for(int i=0;i<selectedfld.size();i++){
    		tempStr=(String)selectedfld.get(i);
		strlen = tempStr.length();		
        	firstpos = (tempStr).indexOf("[VEL]",1);	
		fldName = (tempStr).substring(0,firstpos);	
		fldCol = (tempStr).substring(firstpos+5,strlen);%>
    <tr><td width="25%"><%=StringUtil.decodeString(fldName)%></td>
    <td width="35%"><input type="text" name="fldDispName" value="<%=StringUtil.decodeString(fldName)%>" size="35"></td>
    <td width="10%"><select size="1" name="fldOrder">
 <option value=""><%=LC.L_Select_AnOption%><%--Select an Option*****--%></option>   
<option value="Asc"><%=LC.L_Ascending%><%--Ascending*****--%></option>
<option value="Desc"><%=LC.L_Descending%><%--Descending*****--%></option>
</select></td>  
<td width="5%"><input type="text" name="fldWidth" size="2">%</td>
     <td width="60%"><input type="hidden" name="fldCol" value=<%=fldCol%>></td>
     <input type="hidden" name="fldName" value="<%=StringUtil.decodeString(fldName)%>">
         
    </tr>
    	
    
    <%}//end for loop
    %>
    </table>
    <table width="100%">
    <tr><td width="25%"></td><td width="25%"><A href="#" onClick="selectFlds(document.dynreport)"><%=LC.L_Select_Flds%><%--Select Fields*****--%></A></td>
    <td><A href="#" onClick="openFormWin(document.dynreport);"><%=LC.L_Preview%><%--Preview*****--%></td></tr>
        <!--<td><A href="#" onClick="return openFormWin(document.dynreport);document.dynreport.submit();void(0)">Preview</td></tr>-->
     </table>
     
    <% }else {%>
    <table width="100%">
    <tr><td width="15%"><%=LC.L_Fields%><%--Fields*****--%></td><td width="50%"><%=MC.M_NoFldsSelected%><%--No Fields Selected*****--%>&nbsp;&nbsp;<A href="#" onClick="selectFlds(document.dynreport)"><%=LC.L_Select_Flds%><%--Select Fields*****--%></A></td></tr>
    </table>
    
    <%}%>
    <table>
    <tr><td width="15%"><%=LC.L_Rpt_HdrOrFtr%><%--Report Header/Footer*****--%></td><td width="50%"><input type="text" name="repHeader">
    <input type="text" name="repFooter"></td>
    
    </tr>
    </table>
    <!--<table width="100%" >
   <tr><td width="15%">

		e-Signature <FONT class="Mandatory">* </FONT>

	   </td>

	   <td width="50%">

		<input type="passwrd" name="eSign" maxlength="8">

	   </td>

	</tr>
<tr><td></td>
<td>
		<input type="image" src="../images/jpg/Submit.gif" align="absmiddle" onClick = "submit" border="0">
</td>

</tr>

	</table>-->

</form>

<%

} else {  //else of if body for session

%>
  <jsp:include page="timeout.html" flush="true"/>

  <%
}


%>
 <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</DIV>
  <div class ="mainMenu" id = "emenu">
  <jsp:include page="getmenu.jsp" flush="true"/>
  </div>


</body>
</html>
