<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title><%=LC.L_Import%><%--Import*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*" %>

<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">

</head>

<SCRIPT Language="javascript">

 function  validate(formobj){

	  if (!(validate_col('file',formobj.expfile))) return false
	  
	  if (! TestFileType(formobj.expfile.value, ['.vel'])) return false 
		
      //if (!(validate_col('Esign',formobj.eSign))) return false

	 /*if(isNaN(formobj.eSign.value) == true) {
		alert("Incorrect e-Signature. Please enter again");
		formobj.eSign.focus();
		return false;	}*/
			
	}
	
	function TestFileType( fileName, fileTypes ) {
      if (!fileName) return;
      
      dots = fileName.split(".")
      //get the part AFTER the LAST period.
      fileType = "." + dots[dots.length-1];
      
	 
      if (fileTypes.join(".").indexOf(fileType) != -1) 
	  {
		return true;
	  }
	  else
	  {
		  var paramArray = [fileTypes.join(" .")];
		  alert(getLocalizedMessageString("M_PlsVldFile_TryAgain",paramArray));/*alert("Please select a valid: [" + (fileTypes.join(" .")) + "] file and try again.");*****/
		 return false;
	  }
   
     }

</SCRIPT>



<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.impex.*,java.text.*"%>

<% String src;
	src= request.getParameter("srcmenu");
	src = "";
	
	String mode = request.getParameter("mode");
	
	if (EJBUtil.isEmpty(mode ))
	{
		mode = "initial";
	}
%>

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   

<body>

<br>

<DIV class="formDefault" id="div1">

<P class="sectionHeadings"> <%=MC.M_Import_RevrsA2aData%><%--Import Reverse A2A Data*****--%></P>	 
		
<p class="defComments"><%=LC.L_PlsSel_The%><%--Please select the*****--%> <font color="red"><b> <%=LC.L_Expmanual_Vel%><%--expManual[*].vel*****--%> </b></font> <%=LC.L_File_Lower%><%--file*****--%> :</p>

  <%

	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))

	{

	    String usr = null;
		usr = (String) tSession.getValue("userId");
		String uName = (String) tSession.getValue("userName");
		String accId = (String) tSession.getValue("accountId");
	 	String ipAdd = (String) tSession.getValue("ipAdd");	

	%>
	 <P class = "userName"><%= uName %></p>
	 
	 <% if (mode.equals("initial"))
	 	{
	 	 %>
	 	<form name="imp" method="POST" action="importReverseFile.jsp" onsubmit="return validate(document.imp);">  
	    <input type="hidden" name = "impAccountId"  value = <%=accId%>>
  	 	<input type="hidden" name = "impUserId"  value = <%=usr%>>
   	 	<input type="hidden" name = "impIpAdd"  value = <%=ipAdd%>>
   	 	<input type="hidden" name = "mode"  value = "final">
	 	 
		<table>
		<tr> 
        <td width="35%"> <%=LC.L_File%><%--File*****--%> <FONT class="Mandatory">* </FONT> </td>
        <td width="65%"> 
          <input type=file name=expfile size=40 accept="text/.vel">
        </td>
      </tr>
	</table>
	
	<table width = 70% border = 0><tr><td align = "right">
      <tr>
	
	<!--  <td><br>
   	        e-Signature <FONT class="Mandatory">* </FONT>
	</td>
	<td><br>
       	<input type="password" name="eSign" maxlength="8">
	</td> -->
	<td align = "right"><br>
		<button type="submit"><%=LC.L_Submit%></button>
	</td></tr>
	</table> 
	
	</form>
   
	<%
	}
	else
	{
		String reqFilePath = "";
		String folderPath = "";
		String expId = "";
		String siteCode = "";
		
		Hashtable impHash = new Hashtable();
		Hashtable  htData = new Hashtable();
		ObjectInputStream in = null;	   
		FileInputStream fis = null;
		int reqId = 0;

		
		reqFilePath = request.getParameter("expfile");
		
		try{
			//folderPath = Impex.getFolderPath(reqFilePath);

	    	fis = new FileInputStream(reqFilePath);
			in = new ObjectInputStream(fis);
		 
			impHash = (Hashtable)in.readObject();
			in.close();
			
			System.out.println("The import information is:");
			
			expId = (String) impHash.get("velosExpId");
			siteCode = (String) impHash.get("velosSourceSiteCode");
			
			System.out.println("\n velosExpId : " + expId);
			System.out.println("\n velosSourceSiteCode : " + siteCode);
			
			htData = (Hashtable) impHash.get("data");
			
			System.out.println("Read Message Object, Object contains data for " + htData.size() + " modules");
			
			////
				// get current date time 
			 
			 Calendar now = Calendar.getInstance();
			 String requestDate = "";
			 requestDate = "" + now.get(now.DAY_OF_MONTH) + now.get(now.MONTH) + (now.get(now.YEAR) - 1900) ;
			 Calendar calToday=  Calendar.getInstance();
			 String format="yyyy-MM-dd HH:mm:ss";
			 SimpleDateFormat todayFormat=new SimpleDateFormat(format);
			 requestDate = todayFormat.format(calToday.getTime());
			 
			 //Prepare Import Request

			 ImpexRequest impReq = new ImpexRequest();
			 Impex impexObj = new Impex();
		
   			  impReq.setReqStatus("0"); //import not started
			  impexObj.setRequestType("IR"); //for Reverse Import	
			  //set datetime
		 	  impReq.setReqDateTime(requestDate);
			  impReq.setSiteCode(siteCode);
			  impReq.setDataExpId(EJBUtil.stringToNum(expId)); //set the expId of the original export req from SITE
			  
			  //set the data received via JMS MSG
			  impReq.setHtData(htData);
			  
			  // Log this import request

			  impexObj.setImpexRequest(impReq);
			  reqId = impexObj.recordImportRequest();
			  
			  System.out.println("Import Request ID" + reqId);
				  
			 // calls a wrapper method, synchronized, the method will execute the request
			 //according to the Request Type
			
			  System.out.println("htData!!!!!!" + htData.toString());
			  impexObj.start();
			  //***********wait for the thread to finish!!!   
			  try{
				  impexObj.join();
			  }
			  catch (InterruptedException ie)
			  {
				 System.out.println("Received a Message and got InterruptedException" + ie );
			  }
			  //Impex.impexWrapper(impexObj);
				
			 //cleanup time!!!!!!******************	
			 impexObj.resetMe();
			 impReq.resetMe();
			 
			 if (htData != null)
			   {
				   for (Enumeration x = htData.elements() ; x.hasMoreElements() ;) 
				   {
					   htData.remove(x.nextElement());
				   }
			   }
			  
			 if (impHash != null)
			   {
				   for (Enumeration x = impHash.elements() ; x.hasMoreElements() ;) 
				   {
					   impHash.remove(x.nextElement());
				   }
			   }
			    
			   
			 htData = null;
			 impReq = null;
			 impexObj = null;
			 impHash = null;
			 
			 //end of cleanup !!*********************
				
				
				
			///////////////////
			
		}
	    	catch (Exception ex) 
			{
				System.out.println("Exception in IMPEX --> Reverse Import" + ex.toString());	 			    
			}	
			   	
		
		//mode is final, start import then
			%>
			
			<%=LC.L_Imported_Lower%><%--imported*****--%>.......
			
			<%
			
	}
	

} else {  //else of if body for session



%>

  <jsp:include page="timeout.html" flush="true"/>

  <%

}


%>


  <div> 

    <jsp:include page="bottompanel.jsp" flush="true"/>

  </div>



</div>

<div class ="mainMenu" id = "emenu">
	<jsp:include page="getmenu.jsp" flush="true"/>
</div>
</body>

</html>





