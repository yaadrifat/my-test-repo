<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></HEAD>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<Link Rel=STYLESHEET HREF="common.css" type=text/css>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<BODY>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventuser" scope="request" class="com.velos.esch.web.eventuser.EventuserJB"/>
<%@ page language = "java" import = "com.velos.esch.service.util.EJBUtil,com.velos.esch.web.eventuser.EventuserJB,com.velos.eres.service.util.*,com.aithent.audittrail.reports.AuditUtils"%>

<%

String src = request.getParameter("srcmenu");
String duration = request.getParameter("duration");
String protocolId = request.getParameter("protocolId");
String calledFrom = request.getParameter("calledFrom");
String eventId = request.getParameter("eventId");
String mode = request.getParameter("mode");
String fromPage = request.getParameter("fromPage");
String calStatus = request.getParameter("calStatus");
String eventmode = request.getParameter("eventmode");
String displayDur=request.getParameter("displayDur");
String displayType=request.getParameter("displayType");	 
String eSign = request.getParameter("eSign");	

String propagateInVisitFlag = request.getParameter("propagateInVisitFlag"); //SV, 9/16/04, cal-enh-05
String propagateInEventFlag = request.getParameter("propagateInEventFlag");
String eventName = request.getParameter("eventName"); //SV, 10/28/04

String calAssoc=request.getParameter("calassoc");
calAssoc=(calAssoc==null)?"":calAssoc;

String[] roleId = request.getParameterValues("roleId");

String[] selRole = request.getParameterValues("selRole");

// change by salil
String[] arDurationDD = request.getParameterValues("durationDD");
//end change
String[] arDurationHH = request.getParameterValues("durationHH");
String[] arDurationMM = request.getParameterValues("durationMM");
String[] arDurationSS = request.getParameterValues("durationSS");

String[] arNotes = request.getParameterValues("notes");

int rows = 0;

if(selRole != null) {
   rows = selRole.length; 
}

String durationStr = null;
String selRoleId = null;
ArrayList roleIds = new ArrayList();
ArrayList evDuration = new ArrayList();
ArrayList evNotes = new ArrayList();

HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession)) {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%	
	String oldESign = (String) tSession.getValue("eSign");


	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {
   String ipAdd = (String) tSession.getValue("ipAdd");
   String usr = (String) tSession.getValue("userId");
   // Modified for INF-18183 ::: Raviesh
   eventuser.removeEventusers(EJBUtil.stringToNum(eventId),"R",AuditUtils.createArgs(session,"",LC.L_Evt_Lib));
   
	// by salil
	String ddval = null;
	String hhval = null;
   	String mmval = null;
   	String ssval = null;

   for(int i =0; i<rows; i++) {
	
	selRoleId = selRole[i];

	if (EJBUtil.stringToNum(selRoleId) > 0 )
	{	
		ddval = arDurationDD[i];
		hhval = arDurationHH[i];
		mmval = arDurationMM[i];
		ssval = arDurationSS[i];
		
		if ( ddval.trim().length() == 0 ) ddval = "00";
		if ( hhval.trim().length() == 0 ) hhval = "00";
		if ( mmval.trim().length() == 0 ) mmval = "00";
		if ( ssval.trim().length() == 0 ) ssval = "00";				 
		
	    durationStr = ((ddval.trim().length() == 1) ?  "0" + ddval : ddval) + ":" +((hhval.trim().length() == 1) ?  "0" + hhval : hhval) + ":" + ((mmval.trim().length() == 1) ?  "0" + mmval : mmval) + ":" + ((ssval.trim().length() == 1) ?  "0" + ssval : ssval); 

		roleIds.add(selRoleId);
		evDuration.add(durationStr);
		evNotes.add(arNotes[i]);
		
	}
   }
   
	eventuser.setUserIds(roleIds);
	eventuser.setUserType("R");
	eventuser.setEventId(eventId);
	eventuser.setCreator(usr); 
	eventuser.setIpAdd(ipAdd);
	
	eventuser.setEvDuration(evDuration);
	eventuser.setEvNotes(evNotes);
	if(eventmode.equals("M") && mode.equals("M")){
		eventuser.setModifiedBy(usr);
		eventuser.setLastModifiedDate(DateUtil.stringToDate(DateUtil.getCurrentDate()));
	}
	eventuser.setEventuserDetails();

   	// Cal-enh - 05, now check if the user wishes to propagate these changes to other events.
	if ((propagateInVisitFlag == null) || (propagateInVisitFlag.equals("")))
		propagateInVisitFlag = "N";
	if ((propagateInEventFlag == null) || (propagateInEventFlag.equals("")))
		propagateInEventFlag = "N";
	
	if ( (propagateInVisitFlag.equals("Y")) || (propagateInEventFlag.equals("Y"))) {
		//"M" - modify/update, "P" - library/protocol calendar.
   		eventuser.propagateEventuser(EJBUtil.stringToNum(protocolId), propagateInVisitFlag, propagateInEventFlag, "M", calledFrom, "EVENT_USER");  
	}

%>

<br><br><br><br><br><p class = "sectionHeadings" align = center><%=MC.M_Data_SavedSucc%><%--Data saved successfully.*****--%></p>

<META HTTP-EQUIV=Refresh CONTENT="1; URL=eventresource.jsp?srcmenu=<%=src%>&duration=<%=duration%>&eventId=<%=eventId%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&selectedTab=5&calStatus=<%=calStatus%>&eventmode=<%=eventmode%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&eventName=<%=eventName%>&calassoc=<%=calAssoc%>">
<%
}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>


</BODY>
</HTML>





