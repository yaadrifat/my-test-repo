This is where the database patches are stored for every build.

Developers, add your scripts in the next build in the "in" folder.

Build manager, put the final scripts in the next build in the "out" folder.

Thanks.
