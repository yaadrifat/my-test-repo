DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where column_name = 'MP_FORMTYPE'
      and table_name = 'ER_MAPFORM';

  if (v_column_exists > 0) then
      execute immediate 'alter table ER_MAPFORM modify MP_FORMTYPE  VARCHAR2(5 BYTE)';
  end if;
  commit;
end;

/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,398,3,'03_alter_ermapform.sql',sysdate,'v11 #799');

commit; 
