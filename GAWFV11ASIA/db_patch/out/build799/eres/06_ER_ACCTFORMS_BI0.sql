create or replace TRIGGER "ERES"."ER_ACCTFORMS_BI0" BEFORE INSERT ON ER_ACCTFORMS        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
usr := 'New User' ;


  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'ER_ACCTFORMS',erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'ACCTFORMS_FILLDATE', :NEW.ACCTFORMS_FILLDATE);
       --Audit_Trail.column_insert (raid, 'ACCTFORMS_XML', :NEW.ACCTFORMS_XML);
       Audit_Trail.column_insert (raid, 'CREATED_ON', TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr);
       Audit_Trail.column_insert (raid, 'FK_ACCOUNT', :NEW.FK_ACCOUNT);
       Audit_Trail.column_insert (raid, 'FK_FORMLIB', :NEW.FK_FORMLIB);
       Audit_Trail.column_insert (raid, 'FK_FORMLIBVER', :NEW.FK_FORMLIBVER);
       Audit_Trail.column_insert (raid, 'FK_SPECIMEN', :NEW.FK_SPECIMEN);
       Audit_Trail.column_insert (raid, 'FORM_COMPLETED', :NEW.FORM_COMPLETED);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'ISVALID', :NEW.ISVALID);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'NOTIFICATION_SENT', :NEW.NOTIFICATION_SENT);
       Audit_Trail.column_insert (raid, 'PK_ACCTFORMS', :NEW.PK_ACCTFORMS);
       Audit_Trail.column_insert (raid, 'PROCESS_DATA', :NEW.PROCESS_DATA);
       Audit_Trail.column_insert (raid, 'RECORD_TYPE', :NEW.RECORD_TYPE);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
	   Audit_Trail.column_insert (raid, 'ACCT_FORMTYPE', :NEW.ACCT_FORMTYPE);
	   Audit_Trail.column_insert (raid, 'MOD_PK', :NEW.MOD_PK);

COMMIT;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,398,6,'06_ER_ACCTFORMS_BI0.sql',sysdate,'v11 #799');

commit; 

