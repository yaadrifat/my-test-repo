set define off;
create or replace PACKAGE BODY      "PKG_FORM" AS
PROCEDURE SP_INSERT_FLDRESPONSES(p_field VARCHAR2, p_resp_seqs ARRAY_STRING, p_resp_dispvals ARRAY_STRING,
p_resp_datavals ARRAY_STRING, p_resp_scores ARRAY_STRING, p_resp_isdefaults ARRAY_STRING,
     p_record_types ARRAY_STRING, p_user VARCHAR2, p_ipadd VARCHAR2, o_ret OUT NUMBER)
AS
/****************************************************************************************************
   ** Procedure to insert multiple respones for a field in case of multiple choice field types,
   ** all the records are received in the form of arrays.
   ** Response Sequence and display value are mandatory so should have value in it
   ** Author: Sonika Talwar 4th July 2003
   ** Input parameter: fieldId
   ** Input parameter: Array of response sequences
   ** Input parameter: Array of display values
   ** Input parameter: Array of data values
   ** Input parameter: Array of scores
   ** Input parameter: Array of isdefault
   ** Input parameter: Array of record type
   ** Input parameter: creator of record (userId)
   ** Input parameter: IP Address
   ** Output parameter: 0 for successful insert, -1 for error
   ** Modified by Sonika April 28, 04 - if data value has not been entered, put display value in it
   **/
v_cnt NUMBER;
i NUMBER;
v_field NUMBER;
v_resp_seq NUMBER;
v_resp_dispval VARCHAR2(300);
v_resp_dataval VARCHAR2(300);
v_resp_score NUMBER;
v_resp_isdefault NUMBER;
v_record_type CHAR;
BEGIN
v_field := TO_NUMBER(p_field);
v_cnt := p_resp_seqs.COUNT(); --get the # of elements in array
   i:=1;
   WHILE i <= v_cnt LOOP
    IF (LENGTH(p_resp_seqs(i)) > 0) THEN
      v_resp_seq := TO_NUMBER(p_resp_seqs(i));
      v_resp_dispval := p_resp_dispvals(i);
     v_resp_dataval := p_resp_datavals(i);
     v_resp_score := TO_NUMBER(p_resp_scores(i));
     v_resp_isdefault := TO_NUMBER(p_resp_isdefaults(i));
     v_record_type := p_record_types(i);
      BEGIN
        INSERT INTO ER_FLDRESP
           (pk_fldresp, fk_field, fldresp_seq,
          fldresp_dispval, fldresp_dataval,
          fldresp_score, fldresp_isdefault,
          record_type, creator, last_modified_by,
           last_modified_date, created_on, ip_add)
         VALUES(seq_er_fldresp.NEXTVAL, v_field, v_resp_seq,
            v_resp_dispval, NVL(v_resp_dataval,v_resp_dispval),
           v_resp_score, v_resp_isdefault,
           v_record_type, p_user, p_user,
           SYSDATE, SYSDATE, p_ipadd);
      EXCEPTION  WHEN OTHERS THEN
        P('ERROR');
        o_ret:=-1;
       RETURN;
      END ;--end of insert begin
     i := i+1;
    ELSE
        i := i+1; --do not insert null values
    END IF;
   END LOOP; --v_cnt loop
   COMMIT;
   o_ret:=0;
 /**
   Statement For Testing this SP
   call pkg_form.SP_INSERT_FLDRESPONSES('3',ARRAY_STRING('1','2'),  ARRAY_STRING('resp1','resp2'),ARRAY_STRING('data1','data2'),ARRAY_STRING('0','0'),ARRAY_STRING('1','0'),ARRAY_STRING('N','N'),'510','12.1.1.1',:a)
 **/
END; --end of sp_insert_fldrespones
-----------------------------------------------------------------------------------------------------
PROCEDURE SP_UPDATE_FLDRESPONSES(p_pk_fieldresps ARRAY_STRING, p_resp_seqs ARRAY_STRING, p_resp_dispvals ARRAY_STRING,
                   p_resp_datavals ARRAY_STRING, p_resp_scores ARRAY_STRING, p_resp_isdefaults ARRAY_STRING,
                p_record_types ARRAY_STRING, p_user VARCHAR2, p_ipadd VARCHAR2, o_ret OUT NUMBER)
AS
 /****************************************************************************************************
   ** Procedure to update multiple respones for a field in case of multiple choice field types,
   ** all the records are received in the form of arrays.
   ** Response Sequence and display value are mandatory so should have value in it
   ** Author: Sonika Talwar 7th July 2003
   ** Input parameter: Array of pk fieldResp Ids
   ** Input parameter: Array of response sequences
   ** Input parameter: Array of display values
   ** Input parameter: Array of data values
   ** Input parameter: Array of scores
   ** Input parameter: Array of isdefault
   ** Input parameter: record type
   ** Input parameter: creator of record (userId)
   ** Input parameter: IP Address
   ** Output parameter: 0 for successful update, -1 for error
   ** Modified by Sonika April 28, 04 - if data value has not been entered, put display value in it
   **/
   v_cnt NUMBER;
   i NUMBER;
   v_pkfield_resp NUMBER;
   v_resp_seq NUMBER;
   v_resp_dispval VARCHAR2(300);
   v_resp_dataval VARCHAR2(300);
   v_resp_score NUMBER;
   v_resp_isdefault NUMBER;
   v_record_type CHAR;
BEGIN
   i:=1;
   v_cnt := p_pk_fieldresps.COUNT(); --get the # of elements in array
     v_cnt := p_resp_isdefaults.COUNT();
   WHILE i <= v_cnt LOOP
      v_pkfield_resp := TO_NUMBER(p_pk_fieldresps(i));
      v_resp_seq := TO_NUMBER(p_resp_seqs(i));
      v_resp_dispval := p_resp_dispvals(i);
     v_resp_dataval := p_resp_datavals(i);
     v_resp_score := TO_NUMBER(p_resp_scores(i));
     v_resp_isdefault := TO_NUMBER(p_resp_isdefaults(i));
     v_record_type := p_record_types(i);
     BEGIN
        UPDATE ER_FLDRESP
         SET fldresp_seq = v_resp_seq,
          fldresp_dispval = v_resp_dispval,
          fldresp_dataval =  NVL(v_resp_dataval,v_resp_dispval),
          fldresp_score = v_resp_score,
          fldresp_isdefault = v_resp_isdefault,
          record_type = v_record_type,
           last_modified_by = p_user,
           last_modified_date = SYSDATE,
          ip_add = p_ipadd
       WHERE pk_fldresp = v_pkfield_resp;
    EXCEPTION  WHEN OTHERS THEN
         P('ERROR');
         o_ret:=-1;
       RETURN;
    END ;--end of update begin
   i := i+1;
   END LOOP; --v_cnt loop
  COMMIT;
  o_ret:=0;
 /**
   Statement For Testing this SP
   call pkg_form.SP_UPDATE_FLDRESPONSES(ARRAY_STRING('36','37'),ARRAY_STRING('0','1'),  ARRAY_STRING('resptest1','resptest2'),ARRAY_STRING('test','test'),ARRAY_STRING('0','0'),ARRAY_STRING('0','0'),ARRAY_STRING('N','N'),'510','12.1.1.1',:a)
 **/
END; --end of sp_update_fldrespones
------------------------------------------------------------------------------------------------------------------
PROCEDURE SP_COPYFIELD_TOLIB(p_pk_field NUMBER, p_libflag CHAR, p_user VARCHAR2, p_ipadd VARCHAR2, o_ret OUT NUMBER)
AS
 /****************************************************************************************************
   ** Procedure to copy field to a library
   ** Author: Sonika Talwar 8th July 2003
   ** Input parameter: PK of the field to be copied
   ** Input parameter: location of the field, whether field exisits in library or associated to a form
   ** Input parameter: creator of record (userId)
   ** Input parameter: IP Address
   ** Output parameter: pk of newly copied field, -1 for error
   **/
   v_newfield NUMBER;
   v_newuniqueid NUMBER;
   v_newfldValidate NUMBER;
BEGIN
   -- The new field id generated from the seq_er_fldlib sequence.
    SELECT seq_er_fldlib.NEXTVAL
    INTO v_newfield
    FROM dual;
    SELECT seq_flduniqueid.NEXTVAL
    INTO v_newuniqueid
    FROM dual;
    SELECT seq_er_fldvalidate.NEXTVAL
    INTO v_newfldValidate
    FROM dual;
    BEGIN
    INSERT INTO ER_FLDLIB
         (PK_FIELD, FK_CATLIB, FK_ACCOUNT, FLD_LIBFLAG, FLD_NAME,
        FLD_DESC, FLD_UNIQUEID, FLD_SYSTEMID, FLD_KEYWORD, FLD_TYPE,
        FLD_DATATYPE, FLD_INSTRUCTIONS, FLD_LENGTH, FLD_DECIMAL,
        FLD_LINESNO, FLD_CHARSNO, FLD_DEFRESP, FK_LOOKUP, FLD_ISUNIQUE,
        FLD_ISREADONLY, FLD_ISVISIBLE, FLD_COLCOUNT, FLD_FORMAT, FLD_REPEATFLAG,
        FLD_BOLD, FLD_ITALICS, FLD_SAMELINE,
        FLD_ALIGN,
         FLD_UNDERLINE, FLD_COLOR,
        FLD_FONT, RECORD_TYPE,FLD_FONTSIZE,FLD_LKPDATAVAL, FLD_LKPDISPVAL,
        CREATOR, CREATED_ON, IP_ADD,FLD_TODAYCHECK,FLD_OVERRIDE_MANDATORY,FLD_OVERRIDE_FORMAT,FLD_OVERRIDE_RANGE,FLD_OVERRIDE_DATE,FLD_EXPLABEL,
        FLD_HIDELABEL,   FLD_HIDERESPLABEL,     FLD_DISPLAY_WIDTH,    FLD_LINKEDFORM,       FLD_RESPALIGN,FLD_NAME_FORMATTED,FLD_SORTORDER
        )
      SELECT v_newfield, FK_CATLIB, FK_ACCOUNT, p_libflag, FLD_NAME,
        FLD_DESC, SUBSTR(FLD_UNIQUEID ,1,50), NULL, FLD_KEYWORD, FLD_TYPE,
        FLD_DATATYPE, FLD_INSTRUCTIONS, FLD_LENGTH, FLD_DECIMAL,
        FLD_LINESNO, FLD_CHARSNO, FLD_DEFRESP, FK_LOOKUP, FLD_ISUNIQUE,
        FLD_ISREADONLY, FLD_ISVISIBLE, FLD_COLCOUNT, FLD_FORMAT, FLD_REPEATFLAG,
        FLD_BOLD, FLD_ITALICS, FLD_SAMELINE,
         FLD_ALIGN,
        FLD_UNDERLINE, FLD_COLOR,
        FLD_FONT, 'N',FLD_FONTSIZE,FLD_LKPDATAVAL, FLD_LKPDISPVAL,
        p_user, SYSDATE, p_ipadd,FLD_TODAYCHECK,FLD_OVERRIDE_MANDATORY,FLD_OVERRIDE_FORMAT,FLD_OVERRIDE_RANGE,FLD_OVERRIDE_DATE,FLD_EXPLABEL,
        FLD_HIDELABEL,   FLD_HIDERESPLABEL,     FLD_DISPLAY_WIDTH,    FLD_LINKEDFORM,       FLD_RESPALIGN,FLD_NAME_FORMATTED,FLD_SORTORDER
      FROM ER_FLDLIB
     WHERE pk_field = p_pk_field;
      IF SQL%FOUND THEN
          DBMS_OUTPUT.PUT_LINE ('Insert into field library ' || TO_CHAR(1)) ;
      ELSE
          DBMS_OUTPUT.PUT_LINE ('Field not found in field library ' ) ;
          o_ret :=-1;
      RETURN;
     END IF;
    END;
---------------------------------Insert records for all validations ----------------------------------------


BEGIN
    INSERT INTO ER_FLDVALIDATE
         (PK_FLDVALIDATE,FK_FLDLIB,FLDVALIDATE_OP1,FLDVALIDATE_VAL1,FLDVALIDATE_LOGOP1,FLDVALIDATE_OP2,
          FLDVALIDATE_VAL2,FLDVALIDATE_LOGOP2,FLDVALIDATE_JAVASCR,RECORD_TYPE,CREATOR,CREATED_ON, IP_ADD)
    SELECT v_newfldValidate, v_newfield, FLDVALIDATE_OP1,FLDVALIDATE_VAL1,FLDVALIDATE_LOGOP1,FLDVALIDATE_OP2,
          FLDVALIDATE_VAL2,FLDVALIDATE_LOGOP2,FLDVALIDATE_JAVASCR,'N',p_user, SYSDATE, p_ipadd
      FROM ER_FLDVALIDATE
      WHERE fk_fldlib = p_pk_field
      AND NVL(record_type,'Z') <> 'D' ;
      IF SQL%FOUND THEN
          DBMS_OUTPUT.PUT_LINE ('Insert into fldvalidate ' || TO_CHAR(1)) ;
      ELSE
          DBMS_OUTPUT.PUT_LINE ('Field not found in fldvalidate ' ) ;
     END IF;
    END;
-------------------------
    -- The second insert for all the field responses
    BEGIN
    INSERT INTO ER_FLDRESP
         (PK_FLDRESP,FK_FIELD,FLDRESP_SEQ,FLDRESP_DISPVAL,FLDRESP_DATAVAL,
        FLDRESP_SCORE,FLDRESP_ISDEFAULT,RECORD_TYPE,CREATOR,
        CREATED_ON,IP_ADD)
      SELECT seq_er_fldresp.NEXTVAL, v_newfield, FLDRESP_SEQ,FLDRESP_DISPVAL,FLDRESP_DATAVAL,
        FLDRESP_SCORE,FLDRESP_ISDEFAULT,DECODE(NVL(record_type,'N'),'H','H','N'),p_user,
        SYSDATE, p_ipadd
      FROM ER_FLDRESP
     WHERE fk_field = p_pk_field
       AND NVL(record_type,'Z') <> 'D' ;
      IF SQL%FOUND THEN
          DBMS_OUTPUT.PUT_LINE ('Insert into field responses ' || TO_CHAR(1)) ;
      END IF;
    END;
    o_ret:= v_newfield;
    COMMIT;
 /**
   Statement For Testing this SP
   call pkg_form.SP_COPYFIELD_TOLIB(3,'L', 510, '12.12.12.12',:a);
 **/
END; --end of sp_copyfield_tolib
-----------------------------------------------------------------------------------------------------
PROCEDURE SP_FORMSHAREWITH(p_formid NUMBER, p_pk_sharewith_ids VARCHAR2,
                p_sharewith_type CHAR, p_user VARCHAR2, p_ipadd VARCHAR2, p_mode VARCHAR2, o_ret OUT NUMBER)
AS
 /****************************************************************************************************
   ** Procedure to implement the 'share with' relationship of a form with - users of a Study/Organization/Group
   ** Author: Sonia Sahni 15th July 2003
   ** Input parameter: formid - PK of the form
   ** Input parameter: comma separated String of the pks of the entity (group/study/organization) to share the form with
   ** Input parameter: account
   ** Input parameter: the entity  type (group/study/organization)
   ** Input parameter: creator of record (userId)
   ** Input parameter: IP Address
   **
   ** Output parameter: 0 for successful completion, -1 for error
   **/
   v_pk_sharewith_id NUMBER;
   V_STR      VARCHAR2 (2001) DEFAULT p_pk_sharewith_ids || ',';
   V_PARAMS   VARCHAR2 (2001) DEFAULT p_pk_sharewith_ids || ',';
   V_POS      NUMBER         := 0;
   V_CNT      NUMBER         := 0;
   v_date_field  NUMBER         := 0;
   v_date_formfld NUMBER         := 0;
BEGIN
   DELETE FROM ER_FORMSHAREWITH
           WHERE  FK_FORMLIB = p_formid;
     LOOP
       V_CNT := V_CNT + 1;
       V_POS := INSTR (V_STR, ',');
       V_PARAMS := SUBSTR (V_STR, 1, V_POS - 1);
        EXIT WHEN V_PARAMS IS NULL;
      v_pk_sharewith_id := TO_NUMBER(V_PARAMS);
     BEGIN
           --insert a record for the organization/study/group
         INSERT INTO ER_FORMSHAREWITH(PK_FSW,FK_FORMLIB,FSW_ID ,FSW_TYPE,CREATOR,RECORD_TYPE,CREATED_ON,IP_ADD)
         VALUES (SEQ_ER_FORMSHAREWITH.NEXTVAL,p_formid,v_pk_sharewith_id,p_sharewith_type,p_user,'N',SYSDATE,p_ipadd);
    EXCEPTION  WHEN OTHERS THEN
         P('ERROR');
         o_ret:=-1;
       RETURN;
    END ;--end of update begin
      IF p_sharewith_type = 'S' THEN
         -- insert records for the entire study team
         INSERT INTO ER_FORMSHAREWITH(PK_FSW,FK_FORMLIB,FSW_ID ,FSW_TYPE,CREATOR,RECORD_TYPE,CREATED_ON,IP_ADD)
         SELECT  SEQ_ER_FORMSHAREWITH.NEXTVAL,p_formid,fk_user,'U',p_user ,'N',SYSDATE,p_ipadd
         FROM ER_STUDYTEAM
         WHERE FK_STUDY = v_pk_sharewith_id
         AND NVL(STUDY_TEAM_USR_TYPE,'D')='D' ;
       ELSIF p_sharewith_type = 'O' THEN
          -- insert records for the organization users
          -- Since 'All Users in an Organization' should apply to user's primary organization only, insert
          -- only those users whose primary organization is the one which has been selected
         INSERT INTO ER_FORMSHAREWITH(PK_FSW,FK_FORMLIB,FSW_ID ,FSW_TYPE,CREATOR,RECORD_TYPE,CREATED_ON,IP_ADD)
         SELECT  SEQ_ER_FORMSHAREWITH.NEXTVAL,p_formid,pk_user,'U',p_user ,'N',SYSDATE,p_ipadd
         FROM ER_USER
         WHERE FK_SITEID = v_pk_sharewith_id ;
        ELSIF p_sharewith_type = 'G' THEN
          -- insert records for the group users
         INSERT INTO ER_FORMSHAREWITH(PK_FSW,FK_FORMLIB,FSW_ID ,FSW_TYPE,CREATOR,RECORD_TYPE,CREATED_ON,IP_ADD)
         SELECT  SEQ_ER_FORMSHAREWITH.NEXTVAL,p_formid,fk_user,'U',p_user ,'N',SYSDATE,p_ipadd
         FROM ER_USRGRP
         WHERE FK_GRP = v_pk_sharewith_id ;
          ELSIF p_sharewith_type = 'A' THEN
          -- insert records for all account users
         INSERT INTO ER_FORMSHAREWITH(PK_FSW,FK_FORMLIB,FSW_ID ,FSW_TYPE,CREATOR,RECORD_TYPE,CREATED_ON,IP_ADD)
         SELECT  SEQ_ER_FORMSHAREWITH.NEXTVAL,p_formid,pk_user,'U',p_user ,'N',SYSDATE,p_ipadd
         FROM ER_USER
         WHERE FK_ACCOUNT = v_pk_sharewith_id ;
          ELSIF p_sharewith_type = 'P' THEN
          -- insert records for all account users
         INSERT INTO ER_FORMSHAREWITH(PK_FSW,FK_FORMLIB,FSW_ID ,FSW_TYPE,CREATOR,RECORD_TYPE,CREATED_ON,IP_ADD)
         VALUES (SEQ_ER_FORMSHAREWITH.NEXTVAL,p_formid,v_pk_sharewith_id ,'U',p_user ,'N',SYSDATE,p_ipadd) ;
     END IF;
    V_STR := SUBSTR (V_STR, V_POS + 1);
   END LOOP; --v_cnt loop
  IF p_mode = 'N' THEN
    SP_FORMDEFAULTDATA ( p_formid, v_date_field, v_date_formfld);
      o_ret := v_date_formfld;
  ELSE
      o_ret := 0;
  END IF;
  COMMIT;
END; --end of sp_formsharewith
------------------------------------------------------------------------------------------------------------------
 PROCEDURE SP_GETFIELDXML(p_pk_field NUMBER, p_fldname VARCHAR2, p_fldkeyword VARCHAR2, p_flduniqueid VARCHAR2,
p_browserflag NUMBER, p_systemid VARCHAR2, p_fldtype CHAR , p_fld_datatype VARCHAR2, p_fld_defresp VARCHAR2, p_colcount NUMBER,p_fldlinesno NUMBER,
p_fld_origsysid IN VARCHAR2, p_pksec NUMBER,p_secname VARCHAR2,p_fld_charsno NUMBER,p_sortorder VARCHAR2,o_xml OUT CLOB)
AS
 /****************************************************************************************************
   ** Procedure to get XML dtagram for a field
   ** Author: Sonia Sahni 17th July 2003
   ** Modified by Sonika on Dec 19, 03 to add no_lines attribute in xml for textarea element
   ** Modified by Sonia Abrol, 11/17/06 to handle hidden responses
   ** Input parameter:  p_pk_field  - PK of the form
   ** Input parameter:  p_fldname - field name
   ** Input parameter:  p_fldkeyword - field keyword(s)
   ** Input parameter:  p_flduniqueid - field unique id
   ** Input parameter:  p_browserflag - browser flag
   ** Input parameter:  p_systemid - field systemid
   ** Input parameter:  p_fldtype - field type
   ** Input parameter:  p_fld_datatype    - field datatype
   ** Input parameter:  p_fldlinesno --for keeping a track of the no of line for edit box field (Text Area/Edit BoX)
   ** Output parameter: o_xml - the xml datagram for the fiel
   **/
   v_xml CLOB;
   v_respxml CLOB;
   v_temp CLOB;
   str_col_count VARCHAR2(50);
   v_str_linesno VARCHAR2(50);
   v_name VARCHAR2(4000);
   v_fldresp_dataval VARCHAR2(4000);
   v_fldresp_dispval VARCHAR2(4000);
   v_repcount NUMBER := 0;
   v_unique_id VARCHAR2(4000);
   v_resporder VARCHAR2(250);
  v_fld_defresp VARCHAR2(600);
  v_fldkeyword VARCHAR2(300);
  v_fld_datatype VARCHAR2(4000);
BEGIN
 IF p_fld_datatype = 'MC' OR p_fld_datatype = 'MR' THEN
    str_col_count := ' colcount = "' || p_colcount || '"';
  ELSIF p_fld_datatype = 'MD' THEN
    str_col_count := ' colcount = "0"';
  ELSE
    str_col_count := '';
  END IF;
  --added by Sonika on Dec 19, 03 for textarea number of lines
  IF p_fldlinesno > 1 THEN  --this indicates textarea field
    v_str_linesno := ' linesno = "'||p_fldlinesno||'" ';
  END IF;
 v_name := p_fldname;
 v_unique_id := p_flduniqueid;
 --replace special characters
 v_name :=   Pkg_Util.f_escapeSpecialCharsForXML(v_name);
  v_unique_id  := Pkg_Util.f_escapeSpecialCharsForXML( v_unique_id );
  v_fld_defresp:= Pkg_Util.f_escapeSpecialCharsForXML(p_fld_defresp);
  v_fldkeyword:=Pkg_Util.f_escapeSpecialCharsForXML(p_fldkeyword);
 --modified by Sonika on Dec 19, 03 for textarea number of lines
 --modified by Sonika on Jan 07, 03 to make an attribute in which the user selected data of multiple check boxes can be stored with a separator
 --checkboxesdata would be used for that purpose
 v_xml := '<' || LOWER(p_systemid) || ' name = "' || v_name || '" keyword = "' || v_fldkeyword || '" uid = "' ||   v_unique_id  ||
 '" browser = "'|| p_browserflag ||'"' || str_col_count || v_str_linesno || ' type = "' || p_fld_datatype ||
 '" origsysid = "' || p_fld_origsysid || '" systemid = "' || p_systemid  ||'" checkboxesdata="" pksec = "' ||
 p_pksec  || '"  secname = "' || p_secname || '"  pkfld = "'||p_pk_field||'" fldcharsno =  "'||p_fld_charsno|| '">';
         --check the field type and data type
         IF p_fldtype = 'M' AND (p_fld_datatype = 'MD' OR p_fld_datatype = 'MC' OR p_fld_datatype = 'MR') THEN
            --get XML for field responses
             --if the data value hasn't been entered by the user, consider display value as the data value
             --modified by sonia abrol,03/09/2005, the display value was getting appended to data value even when data value was not null
                 Plog.DEBUG(pCTX,'starting getfiedlxml');
             Plog.DEBUG(pCTX,'sort'||p_sortorder);
        IF  (p_sortorder='A') THEN
 FOR i IN (SELECT pk_fldresp,fldresp_seq, NVL(fldresp_dataval,fldresp_dispval) fldresp_dataval, fldresp_dispval,fldresp_score,
                            fldresp_isdefault
                            FROM ER_FLDRESP
                            WHERE fk_field = p_pk_field
                            AND NVL(record_type,'Z') <> 'D'AND NVL(record_type,'Z') <> 'H'
                            ORDER BY LOWER(fldresp_dispval))
                  LOOP
                        v_repcount := v_repcount + 1;
                        v_fldresp_dataval := i.fldresp_dataval;
                        v_fldresp_dispval := i.fldresp_dispval;
                        v_fldresp_dataval :=  Pkg_Util.f_escapeSpecialCharsForXML(v_fldresp_dataval);
                        v_fldresp_dispval :=  Pkg_Util.f_escapeSpecialCharsForXML(v_fldresp_dispval);
                    IF  p_fld_datatype = 'MD'  THEN
                        v_temp:='<resp no = "' || i.fldresp_seq || '" selected = "' || i.fldresp_isdefault || '" dataval = "' || (v_fldresp_dataval) || '" dispval = "' || v_fldresp_dispval || '" score = "' || i.fldresp_score ||'" respcount = "' || v_repcount || '"></resp>';
                        --  dbms_lob.append( v_respxml, v_temp );
                        v_respxml:=v_respxml||v_temp;
                        ELSE
                            v_temp:='<resp no = "' || i.fldresp_seq || '" checked = "' || i.fldresp_isdefault || '" dataval = "' || (v_fldresp_dataval) || '" dispval = "' || v_fldresp_dispval || '" score = "' || i.fldresp_score ||'" respcount = "' || v_repcount || '"></resp>';
--                          v_respxml := v_respxml || '<resp no = "' || i.fldresp_seq || '" checked = "' || i.fldresp_isdefault || '" dataval = "' || v_fldresp_dataval || '" dispval = "' || v_fldresp_dispval || '" score = "' || i.fldresp_score ||'" respcount = "' || v_repcount || '"></resp>';
--                          dbms_lob.append( v_respxml, v_temp );
                                v_respxml:=v_respxml||v_temp;
                        END IF;
                  END LOOP;
         ELSE
 FOR i IN (SELECT pk_fldresp,fldresp_seq, NVL(fldresp_dataval,fldresp_dispval) fldresp_dataval, fldresp_dispval,fldresp_score,
                            fldresp_isdefault
                            FROM ER_FLDRESP
                            WHERE fk_field = p_pk_field
                            AND NVL(record_type,'Z') <> 'D' AND NVL(record_type,'Z') <> 'H'
                            ORDER BY fldresp_seq)
                  LOOP
                        v_repcount := v_repcount + 1;
                        v_fldresp_dataval := i.fldresp_dataval;
                        v_fldresp_dispval := i.fldresp_dispval;
                        v_fldresp_dataval :=   Pkg_Util.f_escapeSpecialCharsForXML(v_fldresp_dataval);
                        v_fldresp_dispval :=   Pkg_Util.f_escapeSpecialCharsForXML(v_fldresp_dispval);
                    IF  p_fld_datatype = 'MD'  THEN
                        v_temp:='<resp no = "' || i.fldresp_seq || '" selected = "' || i.fldresp_isdefault || '" dataval = "' || (v_fldresp_dataval) || '" dispval = "' || v_fldresp_dispval || '" score = "' || i.fldresp_score ||'" respcount = "' || v_repcount || '"></resp>';
                        --  dbms_lob.append( v_respxml, v_temp );
                        v_respxml:=v_respxml||v_temp;
                        ELSE
                            v_temp:='<resp no = "' || i.fldresp_seq || '" checked = "' || i.fldresp_isdefault || '" dataval = "' || (v_fldresp_dataval) || '" dispval = "' || (v_fldresp_dispval) || '" score = "' || i.fldresp_score ||'" respcount = "' || v_repcount || '"></resp>';
--                          v_respxml := v_respxml || '<resp no = "' || i.fldresp_seq || '" checked = "' || i.fldresp_isdefault || '" dataval = "' || v_fldresp_dataval || '" dispval = "' || v_fldresp_dispval || '" score = "' || i.fldresp_score ||'" respcount = "' || v_repcount || '"></resp>';
--                          dbms_lob.append( v_respxml, v_temp );
                                v_respxml:=v_respxml||v_temp;
                        END IF;
                  END LOOP;
         END IF;
                    v_xml := v_xml || v_respxml;
         ELSIF   p_fldtype = 'E' THEN
           IF p_fldlinesno > 1 THEN  --this indicates textarea field
               IF LENGTH(LTRIM(RTRIM(p_fld_defresp))) > 0  THEN
                v_xml := v_xml || v_fld_defresp;
                ELSE
                 v_xml := v_xml || 'er_textarea_tag'  ;--salil 16 oct 2003 added to remove the unwanted <TD> 's inside the text area
                END IF;
            ELSE   -- simple edit box field
                IF LENGTH(LTRIM(RTRIM(p_fld_defresp))) > 0  THEN
                 v_xml := v_xml || v_fld_defresp;
                ELSE
                 v_xml := v_xml || ''  ;--salil 16 oct 2003
                END IF;
            END IF ;
        END IF;
 v_xml := v_xml || '</' || LOWER(p_systemid) || '>';
o_xml:= v_xml;
END; -- end of SP_GETFIELDXML
-----------------------------------------------------------------------------------------------------
PROCEDURE SP_GET_FORM_HTML (p_formid NUMBER, o_html  OUT CLOB )
IS
 /****************************************************************************************************
   ** SP to generate from html from xml and xsl. The function also replaces &quot; with ' and &apos; with ' after transformation
   ** this is required because this is a known bug in transform in oracle 9.2.0.1 release
   ** Author: Sonika Talwar 18th July 2003
   ** Input parameter: formId
   ** Returns: form html as clob
   **/
  --v_xmldata    XMLType;
  --v_xsldata    XMLType;
  v_html       XMLTYPE;
  --v_xslclob    CLOB;
  --v_xmlclob    CLOB;
  --v_htmlclob   CLOB;
  v_returnhtml CLOB;
  v_len NUMBER;
  i NUMBER;
  j NUMBER;
  tempstr VARCHAR2(4000);
BEGIN
--    SELECT sys.XMLTYPE.createXML(XMLTransform(a.form_xml,(SELECT sys.XMLTYPE.createXML(b.form_xsl) FROM
--                                    ER_FORMLIB b WHERE pk_formlib = p_formid )).getClobVal())
--    INTO  v_html
--    FROM  ER_FORMLIB a WHERE pk_formlib = p_formid ;
    --VELDQUOTE was replaced in main clob to preserve special html encoding &#34; for double quotes
        -- while using FCKeditor, we replace  Double quotes with   &#34; to support those fields on browser and througut the application --VA
      SELECT
             sys.XMLTYPE.createXML(XMLTransform(a.form_xml,(SELECT sys.XMLTYPE.createXML(REPLACE(REPLACE(REPLACE(b.form_xsl,'&nbsp;','&#xa0;'),'&nbsp','&#xa0;'), '&#34;','VELDQUOTE')) FROM ER_FORMLIB b WHERE pk_formlib = p_formid )).getClobVal()).getClobVal()
      INTO  o_html
      FROM  ER_FORMLIB a WHERE pk_formlib = p_formid ;
       -- Return the transformed XML instance as a CLOB value.
--     o_html := v_html.getClobVal();
     v_len := LENGTH(o_html);
     i:=1;
      WHILE i<=v_len
      LOOP
          tempstr := dbms_lob.SUBSTR(o_html,3000,i);
           tempstr := REPLACE(tempstr,'&quot;','"');
          tempstr := REPLACE(tempstr,'&apos;','''');
          tempstr := REPLACE(tempstr,'&amp;','&'); --replace an ampersand
          tempstr := REPLACE(tempstr, '&lt;' , '<');
          tempstr := REPLACE(tempstr, '&gt;', '>');
--            tempstr := REPLACE(tempstr, '\\', '\');
          tempstr := REPLACE(tempstr,'er_textarea_tag<','<');
          tempstr := REPLACE(tempstr, '&#xa0;' , ' ');
                --VELDQUOTE was replaced in main clob to preserve special html encoding &#34; for double quotes
        -- while using FCKeditor, we replace  Double quotes with   &#34; to support those fields on browser and througut the application
             tempstr := REPLACE(tempstr,'VELDQUOTE','"');
          v_returnhtml := v_returnhtml || tempstr;
           i := i + 3000 ;
      END LOOP;
        o_html := v_returnhtml;
        o_html := REPLACE(o_html,'&amp;','&');
        o_html := REPLACE(o_html,'&quot;','"');
        o_html := REPLACE(o_html,'&apos;','''');
        o_html := REPLACE(o_html,'&lt;', '<');
        o_html := REPLACE(o_html,'&gt;', '>');
    --  o_html := REPLACE(o_html,'\\', '\');
        o_html := REPLACE(o_html, '&#xa0;' , ' ');
        --VELDQUOTE was replaced in main clob to preserve special html encoding &#34; for double quotes
        -- while using FCKeditor, we replace  Double quotes with   &#34; to support those fields on browser and througut the application
         o_html := REPLACE(o_html,'VELDQUOTE','"');
        --salil 17 oct 2003
        -- replace the er_textarea_tag added to the text area field
        -- "er_textarea_tag<"is being replaced with "<" since replace  didn't work  when the string to be replaced is blank
        o_html := REPLACE(o_html,'er_textarea_tag<','<');
  EXCEPTION  WHEN OTHERS THEN
       RAISE_APPLICATION_ERROR (-20102, 'Exception occurred in SP_GET_FORM_HTML :'||SQLERRM);
  END ; --end of SP_GET_FORM_HTML
FUNCTION F_GENERATEFORM_HTML(p_formid NUMBER) RETURN CLOB
AS
 /****************************************************************************************************
   ** Function to generate from html from xml and xsl. The function also replaces &quot; with ' and &apos; with ' after transformation
   ** this is required because this is a known bug in transform in oracle 9.2.0.1 release
   ** Author: Sonika Talwar 18th July 2003
   ** Input parameter: formId
   ** Returns: form html as clob
   *************************************SHOULD NOT BE NOT IN USE********************************************************
   **/
  v_xmldata    XMLTYPE;
  v_xsldata    XMLTYPE;
  v_html       XMLTYPE;
  v_xslclob    CLOB;
  v_xmlclob    CLOB;
  v_htmlclob   CLOB;
  v_returnhtml CLOB;
  v_len NUMBER;
  i NUMBER;
  j NUMBER;
  tempstr VARCHAR2(4000);
  BEGIN
     /* Select sys.xmltype.createXML(XMLTransform(a.form_xml,(Select sys.xmltype.createXML(b.form_xsl) from
                                      er_formlib b Where pk_formlib = p_formid )).getClobVal())
      into  v_html
      from  er_formlib a Where pk_formlib = p_formid ;*/
      /*Select (sys.xmltype.createXML(XMLTransform(a.form_xml,(Select sys.xmltype.createXML(b.form_xsl) from
                                      er_formlib b Where pk_formlib = p_formid )).getClobVal())).getClobVal()
      into  v_htmlclob
      from  er_formlib a Where pk_formlib = p_formid ;*/
      SELECT  form_xsl, form_xml
         INTO v_xslclob, v_xmldata
         FROM  ER_FORMLIB
        WHERE pk_formlib=p_formid;
       -- Since XMLType.transform() method takes an XSL stylesheet as XMLType instance,
      -- use the XMLType.createXML method to convert the XSL content received as CLOB
      -- into an XMLType instance.
 v_xslclob := REPLACE(v_xslclob,'&nbsp;','&#xa0;');
 v_xslclob := REPLACE(v_xslclob,'&nbsp','&#xa0;');
      v_xsldata := XMLTYPE.createXML(v_xslclob);
       -- Use the XMLtype.transform() function to get the transformed XML instance.
      -- This function applies the stylesheet to the XML document and returns a transformed
       -- XML instance.
      v_html := v_xmldata.transform(v_xsldata);
       -- Return the transformed XML instance as a CLOB value.
       v_htmlclob := v_html.getClobVal();
       /*v_len := length(v_htmlclob);
       i:=1;
      while i<=v_len
      loop
         tempstr := dbms_lob.substr(v_htmlclob,4000,i);
          tempstr := replace(tempstr,'&quot;','''');
          tempstr := replace(tempstr,'&apos;','''');
          tempstr := replace(tempstr,'&amp;','&'); --replace an ampersand
          tempstr := replace(tempstr, '&lt;' , '<');
          tempstr := replace(tempstr, '&gt;', '>');
          tempstr := replace(tempstr, '\\', '\');
        v_returnhtml := v_returnhtml || tempstr;
        i := i + 4000;
      end loop; */
        v_htmlclob := REPLACE(v_htmlclob,'&quot;','"');
        v_htmlclob := REPLACE(v_htmlclob,'&apos;','''');
        v_htmlclob := REPLACE(v_htmlclob,'&amp;','&');
        v_htmlclob := REPLACE(v_htmlclob,'&lt;', '<');
        v_htmlclob := REPLACE(v_htmlclob,'&gt;', '>');
--      v_htmlclob := REPLACE(v_htmlclob,'\\', '\');
        --salil 17 oct 2003
        -- replace the er_textarea_tag added to the text area field
        -- "er_textarea_tag<"is being replaced with "<" since replace  didn't work  when the string to be replaced is blank
        v_htmlclob := REPLACE(v_htmlclob,'er_textarea_tag<','<');
      RETURN v_htmlclob ;
  EXCEPTION  WHEN OTHERS THEN
       RAISE_APPLICATION_ERROR (-20102, 'Exception occurred in F_GENERATEFORM_HTML :'||SQLERRM);
  END ; --end of f_generateform_html
-----------------------------------------------------------------------------------
/*
   ** Stored procedure to insert default data for a form
   ** Author: Sonia Kaura 18th July 2003
   ** Input parameter: p_pk_form - form id
   ** Output parameter: o_out_field - pk of default date field
   ** Output parameter: o_out_formfld - pk of form field
*/
PROCEDURE SP_FORMDEFAULTDATA ( p_pk_form  IN NUMBER , o_out_field   OUT NUMBER , o_out_formfld OUT NUMBER  )
AS
/*******************/
v_accountId NUMBER ;
v_creator NUMBER ;
v_ipAdd  VARCHAR2(15) ;
v_newFormSec NUMBER ;
v_newFldLib NUMBER ;
v_newFormFld NUMBER ;
v_newuniqueid NUMBER ;
v_count NUMBER;
BEGIN
        SELECT COUNT(*)
        INTO v_count
        FROM ER_FORMSEC
        WHERE  FK_FORMLIB = p_pk_form ;
        IF v_count = 0 THEN
                SELECT FK_ACCOUNT , CREATOR ,  IP_ADD
                INTO
                v_accountId ,
                v_creator ,
                v_ipAdd
                FROM ER_FORMLIB
                WHERE PK_FORMLIB = p_pk_form ;
                --new pk for the ER_FORMSEC table
                SELECT SEQ_ER_FORMSEC.NEXTVAL
                INTO
                v_newFormSec
                FROM DUAL ;
                --insert into the ER_FORMSEC table
                INSERT INTO ER_FORMSEC
                ( PK_FORMSEC , FK_FORMLIB ,
                  FORMSEC_NAME , FORMSEC_SEQ ,
                  FORMSEC_FMT , FORMSEC_REPNO ,
                  RECORD_TYPE , CREATOR ,
                  CREATED_ON , IP_ADD )
                  VALUES ( v_newFormSec, p_pk_form ,'Section 1 ' ,1,
                          'N', 0 , 'N' ,v_creator,SYSDATE , v_ipAdd );
                -- new pk for the ER_FLDLIB
                SELECT SEQ_ER_FLDLIB.NEXTVAL
                INTO
                v_newFldLib
                FROM DUAL ;
                --new value for the ER_FLDLIB.FLD_UNIQUEID
                SELECT SEQ_FLDUNIQUEID.NEXTVAL
                INTO v_newuniqueid
                FROM DUAL;
                --insert into ER_FLDLIB
                INSERT INTO ER_FLDLIB
                ( PK_FIELD , FK_ACCOUNT ,FLD_LIBFLAG ,
                  FLD_NAME , FLD_UNIQUEID , FLD_DATATYPE , FLD_TYPE,
                  RECORD_TYPE , CREATOR , CREATED_ON , IP_ADD, FLD_SYSTEMID)
                VALUES  ( v_newFldLib ,v_accountId , 'F' ,
                          'Data Entry Date', 'er_def_date_01' , 'ED', 'E',
                          'N', v_creator , SYSDATE , v_ipAdd, 'er_def_date_01') ;
                --substr('date_' || v_newuniqueid || '_' ,1,50)
                -- new pk for the ER_FORMFLD
                SELECT SEQ_ER_FORMFLD.NEXTVAL
                INTO
                v_newFormFld
                FROM DUAL ;
                --insert into ER_FORMFLD
                INSERT INTO ER_FORMFLD
                ( PK_FORMFLD , FK_FORMSEC , FK_FIELD ,
                  FORMFLD_SEQ , FORMFLD_MANDATORY , FORMFLD_BROWSERFLG ,
                  RECORD_TYPE , CREATOR , CREATED_ON , IP_ADD )
                VALUES ( v_newFormFld , v_newFormSec , v_newFldLib ,
                         1 ,1 , 1 , 'N' , v_creator , SYSDATE , v_ipAdd);
                COMMIT;
            o_out_field:= v_newFldLib;
            o_out_formfld := v_newFormFld ;
        ELSE
            o_out_field:= 0;
            o_out_formfld := 0;
        END IF;
 END;
 -----------------------------------------------------------------------------------
    PROCEDURE SP_SAVEFORM (p_formid NUMBER, p_param ARRAY_STRING, p_paramvalues ARRAY_STRING )
    AS
     /****************************************************************************************************
   ** Procedure to get XML dtagram for a field
   ** Author: Sonia Sahni 20th July 2003
   ** Input parameter:  p_formid - PK of the form
   ******************************************NOT IN USE
   ********************************************************************************************************/
    v_cnt NUMBER;
    i NUMBER := 1;
    v_param VARCHAR2(50);
    v_paramvalues VARCHAR2(4000);
    v_blankxml    CLOB;
    v_savexml CLOB;
    v_doc       dbms_xmldom.DOMDocument;
    v_doc2       dbms_xmldom.DOMDocument;
    ndoc      dbms_xmldom.DOMNode;
    buf       VARCHAR2(2000);
    nodelist  dbms_xmldom.DOMNodelist;
    docelem   dbms_xmldom.DOMElement;
    node      dbms_xmldom.DOMNode;
    childnode dbms_xmldom.DOMNode;
    nodelistSize NUMBER := 0;
    nodelistSizeCount NUMBER := 0;
     myParser    dbms_xmlparser.Parser;
     --for xml save
    --ctx dbms_xmlsave.ctxType ;
    BEGIN
     v_cnt := p_param.COUNT(); --get the # of elements in array
     --get blank xml for the form
     SELECT c.form_xml.getClobVal()
     INTO v_blankxml
     FROM ER_FORMLIB c
     WHERE pk_formlib = p_formid;
    -- Create DOMDocument handle:
   P('0');
     myParser := dbms_xmlparser.newParser; --**
     dbms_xmlparser.parseClob(myParser, v_blankxml);
     v_doc     := dbms_xmlparser.getDocument(myParser);
     v_doc2     := dbms_xmlparser.getDocument(myParser);
     --v_doc     := dbms_xmldom.newDOMDocument(v_blankxml);
     docelem := dbms_xmldom.getDocumentElement( v_doc );
     --ndoc    := dbms_xmldom.makeNode(v_doc);
     -- Get First Child Of the Node
         --dbms_output.put_line('Before:'||buf);
   P('1');
    WHILE i <= v_cnt LOOP --iterate through each parameter
      v_param := TO_CHAR(p_param(i));
      v_paramvalues := TO_CHAR(p_paramvalues(i));
      nodelist := dbms_xmldom.getElementsByTagName(docelem, v_param);
      nodelistSize := dbms_xmldom.getLength(nodelist) ;
      nodelistSizeCount := 0;
      WHILE nodelistSizeCount < nodelistSize LOOP
             node := dbms_xmldom.item(nodelist,nodelistSizeCount);
            IF  (dbms_xmldom.isNull(node) = FALSE)  THEN
                          P('got node');
                 childnode := dbms_xmldom.getFirstChild(node);
                     IF  (dbms_xmldom.isNull(childnode) = FALSE)  THEN
                         P('got child node');
                         P(dbms_xmldom.getNodeValue(childnode));
                         -- Manipulate:
                         dbms_xmldom.setNodeValue(childnode, v_paramvalues);
                         P(dbms_xmldom.getNodeValue(childnode));
                    END IF;
             END IF;
             nodelistSizeCount :=  nodelistSizeCount + 1;
      END LOOP;
      i := i+1;
    END LOOP; --end of v_cnt loop
    P('before write to clob');
    dbms_xmldom.writeToClob(v_doc2,v_savexml );
    P(dbms_lob.SUBSTR(v_savexml,10,1));
    P('after write to clob');
     /*update er_formlib
     set form_xml = v_savexml
     where  pk_formlib = 1; */
--  commit;
    END;
 -----------------------------------------------------------------------------------
 PROCEDURE    sp_copy_multiple_flds_from_lib (p_section STRING , p_field_ids ARRAY_STRING, p_user VARCHAR2, p_ipadd VARCHAR2, o_ret OUT NUMBER, o_pk_formfld OUT ARRAY_STRING, o_pk_fldlib OUT ARRAY_STRING)
 AS
/****************************************************************************************************
   ** Procedure to add selected fields to a form section
   ** Data would be inserted first in er_fldlib and then in er_formfld
   ** Author: Sonika Talwar 17th July 2003
   ** Input parameter: PK of the form in which the fields have to be inserted
   ** Input parameter: Form section id in which the fields have to be inserted
   ** Input parameter: Field ids in an array
   ** Input parameter: creator of record (userId)
   ** Input parameter: IP Address
   ** Output parameter: 0 for success, -1 for error
   ** Output parameter: array of new generated pks of er_formfld
   ** Output parameter: array of new generated pks of er_fldlib
   **/
   v_fldcnt NUMBER;
   i NUMBER;
   v_newfld NUMBER;
   v_fld_tocopy NUMBER;
   v_new_formfld NUMBER;
   v_fldseq NUMBER;
   v_fldValidatePk  NUMBER;
   v_arr_newformflds ARRAY_STRING := ARRAY_STRING();
   v_arr_newflds ARRAY_STRING := ARRAY_STRING();
BEGIN
    --for each selected field, create a new field in er_fldlib with fldlib_flag=F
    --F indicates that the field is associated with a form
    v_fldcnt := p_field_ids.COUNT();
    i := 1;
    WHILE i <= v_fldcnt LOOP
      BEGIN
         v_fld_tocopy := TO_NUMBER(p_field_ids(i));
         Pkg_Form.SP_COPYFIELD_TOLIB(v_fld_tocopy, 'F', p_user, p_ipadd, v_newfld);
        IF v_newfld > 0 THEN
           --insert the field in er_formlib to make the association of the form/section and field
           --get the maximum field sequence within the section and accordingly increment field sequence
            SELECT NVL(MAX(formfld_seq),0) INTO v_fldseq
            FROM ER_FORMFLD
            WHERE fk_formsec=p_section;
            v_fldseq := v_fldseq + 1;
           -- The new form field id generated from the seq_er_formfld sequence.
             SELECT seq_er_formfld.NEXTVAL
             INTO v_new_formfld
             FROM dual;
           INSERT INTO ER_FORMFLD
             (PK_FORMFLD, FK_FORMSEC, FK_FIELD,
                FORMFLD_SEQ, FORMFLD_MANDATORY,
             FORMFLD_BROWSERFLG, CREATOR,
             RECORD_TYPE, CREATED_ON, IP_ADD)
           VALUES (v_new_formfld, p_section, v_newfld,
                   v_fldseq, 0,
                 0, p_user,
                 'N', SYSDATE, p_ipadd);
            /*    SELECT seq_er_fldvalidate.NEXTVAL
             INTO v_fldValidatePk
             FROM dual;
             INSERT INTO ER_FLDVALIDATE(pk_fldvalidate,fk_fldlib,record_type,creator,created_on,ip_add)
             VALUES (v_fldValidatePk,v_newfld,'N',p_user,SYSDATE,p_ipadd);*/
            -- added by sonia abrol, 12/13/05, this pc of code was missing
              v_arr_newformflds.EXTEND;
           v_arr_newformflds(i) := v_new_formfld;
           --------
           v_arr_newflds.EXTEND;
           v_arr_newflds(i) := v_newfld;
            Plog.DEBUG(pCTX,'in sp_copy_multiple_flds_from_lib: new field' || v_newfld);
        END IF;
     EXCEPTION  WHEN OTHERS THEN
                         Plog.FATAL(pCTX,'exception in sp_copy_multiple_flds_from_lib:' || SQLERRM);
        o_ret:=-1;
       RETURN;
      END ;--end of insert begin
     i := i+1;
  END LOOP; --v_cnt loop
  COMMIT;
  o_ret:=0;
  o_pk_formfld := v_arr_newformflds;
  o_pk_fldlib := v_arr_newflds;

  Plog.DEBUG(pCTX,'in sp_copy_multiple_flds_from_lib: set arrays');
  /*--sql for testing
  declare
  a ARRAY_STRING;
  b ARRAY_STRING;
  c number := 0;
  begin
  pkg_form.sp_copy_multiple_flds_from_lib(40 , ARRAY_STRING(199), 510, '12', c, b,a );
  dbms_output.put_line(a(1)) ;
  dbms_output.put_line(b(1)) ;
  end ;
  */
END; --end of sp_copy_multiple_flds_from_lib


    PROCEDURE SP_GETFILLEDFORM_HTML(p_pkfilledform IN NUMBER, p_formdisplaytype IN VARCHAR2, o_xml OUT CLOB, o_formstat OUT NUMBER,o_xsl OUT CLOB,o_specimen OUT NUMBER)
AS
 /****************************************************************************************************
   ** Procedure to generate form html for filled form
   ** Author: Sonia Sahni 31st July 2003
   ** Modified by Sonika Talwar May 11, 04, to get html according to form version
   ** Input parameter: p_pkfilledform  id of filled form record
   ** Input parameter: p_formid id of associated form
   ** Input parameter: p_formtype type of filled form
   ** Output parameter: o_xml returns form xml as clob
   ** Output parameter: o_formstat returns filled form status
   ** Output parameter: o_xsl returns form xsl

   ** Modified by Sonia Abrol, 11/02/05, to return xml and xsl clobs, we will do the transformation in java
   **/

  v_xslclob    CLOB;
  v_xmlclob    CLOB;

  v_formstatus NUMBER;
  tempstr LONG;
  v_len NUMBER;
  i NUMBER;
  v_specimen NUMBER;
  BEGIN
     -- get filled form XML and XSL
    IF p_formdisplaytype = 'S' OR p_formdisplaytype = 'SA' THEN
        SELECT e.STUDYFORMS_XML.getClobval(), FORMLIBVER_XSL, FORM_COMPLETED ,fk_specimen
        INTO V_XMLCLOB, V_XSLCLOB, V_FORMSTATUS ,v_specimen
        FROM ER_STUDYFORMS e, ER_FORMLIBVER
        WHERE PK_STUDYFORMS = p_pkfilledform
        AND PK_FORMLIBVER = FK_FORMLIBVER ;
   ELSIF p_formdisplaytype = 'SP' OR p_formdisplaytype = 'PA' THEN
        SELECT e.PATFORMS_XML.getclobval(), FORMLIBVER_XSL, FORM_COMPLETED ,fk_specimen
        INTO V_XMLCLOB,  V_XSLCLOB, V_FORMSTATUS ,v_specimen
        FROM ER_PATFORMS e, ER_FORMLIBVER
        WHERE PK_PATFORMS = p_pkfilledform
        AND PK_FORMLIBVER = FK_FORMLIBVER;
   ELSIF p_formdisplaytype = 'A' THEN
        SELECT e.ACCTFORMS_XML.getClobVal(), FORMLIBVER_XSL, FORM_COMPLETED ,fk_specimen
        INTO V_XMLCLOB , V_XSLCLOB, V_FORMSTATUS ,v_specimen
        FROM ER_ACCTFORMS e, ER_FORMLIBVER
        WHERE PK_ACCTFORMS = p_pkfilledform
        AND PK_FORMLIBVER = FK_FORMLIBVER ;
   ELSIF p_formdisplaytype = 'C' THEN
        SELECT e.CRFFORMS_XML.getclobval(), FORMLIBVER_XSL, FORM_COMPLETED
        INTO V_XMLCLOB, V_XSLCLOB, V_FORMSTATUS
        FROM ER_CRFFORMS e, ER_FORMLIBVER
        WHERE PK_CRFFORMS = p_pkfilledform
        AND PK_FORMLIBVER = FK_FORMLIBVER ;
   END IF;

     o_formstat := v_formstatus;
      o_xml := V_XMLCLOB;
      o_xsl :=  V_XSLCLOB;
      o_specimen := v_specimen ;

      /* -- Since XMLType.transform() method takes an XSL stylesheet as XMLType instance,
      -- use the XMLType.createXML method to convert the XSL content received as CLOB
      -- into an XMLType instance.
      v_xslclob := REPLACE(v_xslclob,'&nbsp;','&#xa0;');
      v_xslclob := REPLACE(v_xslclob,'&nbsp','&#xa0;');
      v_xslclob:=REPLACE(v_xslclob,'&#34;','VELDQUOTE');
      v_xsldata := XMLTYPE.createXML(v_xslclob);
            -- Use the XMLtype.transform() function to get the transformed XML instance.
      -- This function applies the stylesheet to the XML document and returns a transformed
       -- XML instance.
      v_html := v_xmldata.transform(v_xsldata);
      -- Return the transformed XML instance as a CLOB value.
      o_html := v_html.getClobVal();
      v_len := LENGTH(o_html);
      i:=1;
      WHILE i<=v_len
      LOOP
          tempstr := dbms_lob.SUBSTR(o_html,4000,i);
          tempstr := REPLACE(tempstr,'&quot;','"');
          tempstr := REPLACE(tempstr,'&apos;','''');
          tempstr := REPLACE(tempstr,'&amp;','&'); --replace an ampersand
          tempstr := REPLACE(tempstr, '&lt;' , '<');
          tempstr := REPLACE(tempstr, '&gt;', '>');
--            tempstr := REPLACE(tempstr, '\\', '\');
          tempstr := REPLACE(tempstr,'er_textarea_tag<','<');
          tempstr := REPLACE(tempstr, '&#xa0;' , ' ');
            --VELDQUOTE was replaced in main clob to preserve special html encoding &#34; for double quotes
        -- while using FCKeditor, we replace  Double quotes with   &#34; to support those fields on browser and througut the application
             tempstr := REPLACE(tempstr,'VELDQUOTE','"');
          v_returnhtml := v_returnhtml || tempstr;
          i := i + 4000 ;
      END LOOP;
         O_html := v_returnhtml;
        O_html := REPLACE(O_html,'&amp;','&');
        O_html := REPLACE(O_html,'&quot;','"');
        O_html := REPLACE(O_html,'&apos;','''');
        O_html := REPLACE(O_html,'&lt;', '<');
        O_html := REPLACE(O_html,'&gt;', '>');
--      O_html := REPLACE(O_html,'\\', '\');
        O_html := REPLACE(O_html, '&#xa0;' , ' ');
        --VELDQUOTE was replaced in main clob to preserve special html encoding &#34; for double quotes
        -- while using FCKeditor, we replace  Double quotes with   &#34; to support those fields on browser and througut the application
         O_html := REPLACE(O_html,'VELDQUOTE','"');
        O_html := REPLACE(O_html,'er_textarea_tag<','<'); */



      ---------------------
  EXCEPTION  WHEN OTHERS THEN
       RAISE_APPLICATION_ERROR (-20102, 'Exception occurred in SP_GETFILLEDFORM_HTML :'||SQLERRM);
  END ; --end of SP_GETFILLEDFORM_HTML
----------------------------------------------------------------------------------------------------
PROCEDURE SP_COPYFIELD_TOFORM(p_pk_formfld NUMBER, p_formsec NUMBER,p_field NUMBER, p_user VARCHAR2, p_ipadd VARCHAR2, o_ret_pkField OUT NUMBER, o_ret_pkFormFld OUT NUMBER )
AS
  /****************************************************************************************************
   ** Procedure to copy form field to a form. A field is created in er_formlib with libflag = F, then in er_formfld
   ** Author: Anu Khanna 29/07/2003
   ** Input parameter: PK of the form whose field is to be copied
   ** Input parameter: pk of form section to which it has to be copied
   ** Input parameter: pk of field which is to be copied
   ** Input parameter: creator of record (userId)
   ** Input parameter: IP Address
   ** Output parameter: pk of newly copied field, -1 for error
   ** Output parameter: pk of form field
   **/
  v_newfield NUMBER;
  v_mandatory NUMBER;
  v_browserflag NUMBER;
  v_fldseq NUMBER;
  v_newformfld NUMBER;
BEGIN
  -- Procedure to copy field to a library
  Pkg_Form.SP_COPYFIELD_TOLIB(p_field, 'F', p_user, p_ipadd ,v_newfield);
  --The new form field id generated from the seq_er_formfld sequence.
  SELECT seq_er_formfld.NEXTVAL
  INTO v_newformfld
  FROM dual;
  -- To find the mandatory and browserflg values of the existing field to be copied
  SELECT NVL(formfld_mandatory,0), NVL(formfld_browserflg,0)
  INTO v_mandatory, v_browserflag
  FROM ER_FORMFLD
  WHERE fk_field = p_field;
  -- To generate sequence of field within the section
  SELECT NVL(MAX(formfld_seq),0)+1
  INTO v_fldseq
  FROM ER_FORMFLD
  WHERE fk_formsec = p_formsec ;
 BEGIN
   -- To insert values for the copy of the desired field .
   INSERT INTO ER_FORMFLD
   (pk_formfld, fk_formsec, fk_field, formfld_seq,
   formfld_mandatory, formfld_browserflg, formfld_xsl,
   formfld_javascr, record_type, creator,
   created_on, ip_add)
   VALUES (v_newformfld, p_formsec, v_newfield, v_fldseq ,
   v_mandatory, v_browserflag, NULL, NULL ,'N', p_user,
   SYSDATE, p_ipadd);
   IF SQL%FOUND THEN
    DBMS_OUTPUT.PUT_LINE('Insert into form ' || TO_CHAR(1));
   ELSE
    DBMS_OUTPUT.PUT_LINE('Form not Found ');
    o_ret_pkField :=-1;
    RETURN;
   END IF;
 END;
 o_ret_pkField:= v_newfield;
 o_ret_pkFormFld:=v_newformfld;
 COMMIT;
END; -- end of SP_COPYFIELD_TOFORM Procedure
/* Sonia Sahni*/
PROCEDURE SP_INSERT_PAT_FORM(p_form IN NUMBER, p_patient IN NUMBER,p_patprot IN NUMBER, p_formxml IN CLOB,
p_completed IN NUMBER, p_creator IN NUMBER, p_valid IN NUMBER, p_ipAdd IN VARCHAR2, p_disptype IN VARCHAR2 ,  o_ret OUT NUMBER,
 p_schevent IN NUMBER ,p_specimen IN VARCHAR2)
AS
 /****************************************************************************************************
   ** Procedure to insert a filled patient related form
   ** Author: Sonia Sahni 7th Aug 2003
   ** Input parameter p_form  - Form ID
   ** Input parameter p_patient  - Patient ID
   ** Input parameter p_patprot  - Enrollment ID
   ** Input parameter p_formxml - Form xml
   ** Input parameter p_completed - Form compeletion status
   ** Input parameter p_creator - Form record creator
   ** Input parameter p_valid - Form validity flag
   ** Input parameter p_ipAdd - User's IP ADD
   ** Input parameter p_disptype - Form's Display Location
   ** Output parameter p_ret - Returns primary key of new record
   ** Modified by Sonika Talwar on March 12, 04 to store patient_id in case of patient study forms also.
   **/
  v_xmlclob    CLOB;
  v_pkfilledform NUMBER;
  v_formlibver NUMBER;
  BEGIN
  v_xmlclob :=  p_formxml;
  SELECT SEQ_ER_PATFORMS.NEXTVAL
  INTO  v_pkfilledform
  FROM dual;
  SELECT MAX(pk_formlibver)
  INTO v_formlibver
  FROM ER_FORMLIBVER
  WHERE fk_formlib = p_form;
  IF p_disptype = 'SP' THEN

    INSERT INTO ER_PATFORMS(PK_PATFORMS,FK_FORMLIB,FK_PER,FK_PATPROT,PATFORMS_XML,FORM_COMPLETED,RECORD_TYPE,
                        CREATOR,ISVALID ,CREATED_ON,IP_ADD, FK_FORMLIBVER, FK_SCH_EVENTS1,FK_SPECIMEN)
            VALUES ( v_pkfilledform,p_form,p_patient,p_patprot, XMLTYPE(v_xmlclob),
            p_completed ,'N', p_creator,p_valid, SYSDATE, p_ipAdd, v_formlibver,DECODE(p_schevent,0,NULL,p_schevent),DECODE(p_specimen,0 , NULL,p_specimen ) );

  ELSIF p_disptype = 'PA' THEN

        INSERT INTO ER_PATFORMS(PK_PATFORMS,FK_FORMLIB,FK_PER,PATFORMS_XML,FORM_COMPLETED,RECORD_TYPE,
                        CREATOR,ISVALID ,CREATED_ON,IP_ADD, FK_FORMLIBVER, FK_SPECIMEN)
            VALUES ( v_pkfilledform,p_form,p_patient, XMLTYPE(v_xmlclob),
            p_completed ,'N', p_creator,p_valid, SYSDATE, p_ipAdd, v_formlibver ,p_specimen);
  END IF;
   o_ret := v_pkfilledform;
  EXCEPTION  WHEN OTHERS THEN
       RAISE_APPLICATION_ERROR (-20102, 'Exception occurred in SP_INSERT_PAT_FORM :'||SQLERRM);
  END ; --end of SP_INSERT_PAT_FORM
/*********************************************/
/* Sonia Sahni*/
PROCEDURE SP_INSERT_STUDY_FORM(p_form IN NUMBER, p_study IN NUMBER, p_formxml IN CLOB,
p_completed IN NUMBER, p_creator IN NUMBER, p_valid IN NUMBER, p_ipAdd IN VARCHAR2, o_ret OUT NUMBER ,p_specimen IN VARCHAR2)
AS
 /****************************************************************************************************
   ** Procedure to insert a filled study related form
   ** Author: Sonia Sahni 7th Aug 2003
   ** Input parameter p_form  - Form ID
   ** Input parameter p_study  - Study ID
   ** Input parameter p_formxml - Form xml
   ** Input parameter p_completed - Form compeletion status
   ** Input parameter p_creator - Form record creator
   ** Input parameter p_valid - Form validity flag
   ** Input parameter p_ipAdd - User's IP ADD
   ** Output parameter p_ret - Returns primary key of new record
   **/
  v_xmlclob    CLOB;
  v_pkfilledform NUMBER;
  v_formlibver NUMBER;
  BEGIN
  v_xmlclob :=  p_formxml;
  SELECT SEQ_ER_STUDYFORMS.NEXTVAL
  INTO  v_pkfilledform
  FROM dual;
  SELECT MAX(pk_formlibver)
  INTO v_formlibver
  FROM ER_FORMLIBVER
  WHERE fk_formlib = p_form;
    INSERT INTO ER_STUDYFORMS(PK_STUDYFORMS,FK_FORMLIB,FK_STUDY,
            STUDYFORMS_XML,FORM_COMPLETED,ISVALID,CREATOR,RECORD_TYPE ,CREATED_ON,IP_ADD,FK_FORMLIBVER,FK_SPECIMEN)
            VALUES ( v_pkfilledform,p_form,p_study,
            XMLTYPE(v_xmlclob), p_completed ,p_valid,p_creator,'N', SYSDATE, p_ipAdd,v_formlibver ,DECODE(p_specimen,0 , NULL,p_specimen ) );

            o_ret := v_pkfilledform;
  EXCEPTION  WHEN OTHERS THEN
       RAISE_APPLICATION_ERROR (-20102, 'Exception occurred in SP_INSERT_STUDY_FORM :'||SQLERRM);
  END ; --end of SP_INSERT_STUDY_FORM
/*********************************************/
/* Sonia Sahni*/
PROCEDURE SP_INSERT_ACCT_FORM(p_form IN NUMBER, p_account IN NUMBER, p_formxml IN CLOB,
p_completed IN NUMBER, p_creator IN NUMBER, p_valid IN NUMBER, p_ipAdd IN VARCHAR2, o_ret OUT NUMBER ,p_specimen IN VARCHAR2 , p_commonformflag IN VARCHAR2 , p_modPK IN NUMBER)
AS
 /****************************************************************************************************
   ** Procedure to insert a filled account related form
   ** Author: Sonia Sahni 7th Aug 2003
   ** Input parameter p_form  - Form ID
   ** Input parameter p_account  - Account
   ** Input parameter p_formxml - Form xml
   ** Input parameter p_completed - Form compeletion status
   ** Input parameter p_creator - Form record creator
   ** Input parameter p_valid - Form validity flag
   ** Input parameter p_ipAdd - User's IP ADD
   ** Output parameter p_ret - Returns primary key of new record
   **/
  v_xmlclob    CLOB;
  v_pkfilledform NUMBER;
  v_formlibver NUMBER;
  pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SP_INSERT_ACCOUNT', pLEVEL  => Plog.LFATAL);
  BEGIN
 
  v_xmlclob :=  p_formxml;
  SELECT SEQ_ER_ACCTFORMS.NEXTVAL
  INTO  v_pkfilledform
  FROM dual;
  SELECT MAX(pk_formlibver)
  INTO v_formlibver
  FROM ER_FORMLIBVER
  WHERE fk_formlib = p_form;
  plog.FATAL(pCTX, 'p_modPK:'|| p_modPK );
  if p_modPK < 1 then
   plog.FATAL(pCTX, 'enter if:'|| p_modPK );
  INSERT INTO ER_ACCTFORMS(PK_ACCTFORMS,FK_FORMLIB,FK_ACCOUNT,
            ACCTFORMS_XML,RECORD_TYPE,FORM_COMPLETED,
            CREATOR,ISVALID ,CREATED_ON,IP_ADD,FK_FORMLIBVER,fk_specimen)
    VALUES ( v_pkfilledform,p_form,p_account,
            XMLTYPE(v_xmlclob), 'N', p_completed ,p_creator,p_valid, SYSDATE, p_ipAdd, v_formlibver ,DECODE(p_specimen,0 , NULL,p_specimen ) );
else
plog.FATAL(pCTX, 'enter else:'|| p_modPK );
            INSERT INTO ER_ACCTFORMS(PK_ACCTFORMS,FK_FORMLIB,FK_ACCOUNT,
            ACCTFORMS_XML,RECORD_TYPE,FORM_COMPLETED,
            CREATOR,ISVALID ,CREATED_ON,IP_ADD,FK_FORMLIBVER,fk_specimen,ACCT_FORMTYPE,MOD_PK)
    VALUES ( v_pkfilledform,p_form,p_account,
            XMLTYPE(v_xmlclob), 'N', p_completed ,p_creator,p_valid, SYSDATE, p_ipAdd, v_formlibver ,DECODE(p_specimen,0 , NULL,p_specimen ),p_commonformflag,p_modPK );
            end if;
        o_ret := v_pkfilledform;
        plog.FATAL(pCTX, 'o_ret:'|| o_ret );
  EXCEPTION  WHEN OTHERS THEN
       RAISE_APPLICATION_ERROR (-20102, 'Exception occurred in SP_INSERT_ACCT_FORM :'||SQLERRM);
  END ; --end of SP_INSERT_ACCT_FORM
/*********************************************/
    PROCEDURE SP_REPEAT_FIELD(p_pk_field NUMBER, p_user VARCHAR2, p_ipadd VARCHAR2,  o_ret OUT NUMBER)
    AS
    /****************************************************************************************************
    ** Procedure to copy field to a library
    ** Author: Sonia Sahni 7th August 2003
    ** Input parameter: PK of the field to be copied
    ** Input parameter: creator of record (userId)
    ** Input parameter: IP Address
    ** Output parameter: pk of newly copied field, -1 for error
    **/
    v_newfield NUMBER;
    v_newuniqueid NUMBER;
    v_newfldvalidate NUMBER;
    v_fld_systemid_old VARCHAR2(50) ;
    v_fld_systemid_new VARCHAR2(50) ;
    BEGIN
        -- The new field id generated from the seq_er_fldlib sequence.
        SELECT seq_er_fldlib.NEXTVAL, seq_flduniqueid.NEXTVAL
        INTO v_newfield, v_newuniqueid
        FROM dual;
        SELECT seq_er_fldvalidate.NEXTVAL
        INTO v_newfldvalidate
        FROM dual;
        BEGIN
            INSERT INTO ER_FLDLIB
            (PK_FIELD, FK_CATLIB, FK_ACCOUNT, FLD_LIBFLAG, FLD_NAME,
            FLD_DESC, FLD_UNIQUEID, FLD_SYSTEMID, FLD_KEYWORD, FLD_TYPE,
            FLD_DATATYPE, FLD_INSTRUCTIONS, FLD_LENGTH, FLD_DECIMAL,
            FLD_LINESNO, FLD_CHARSNO, FLD_DEFRESP, FK_LOOKUP, FLD_ISUNIQUE,
            FLD_ISREADONLY, FLD_ISVISIBLE, FLD_COLCOUNT, FLD_FORMAT, FLD_REPEATFLAG,
            FLD_BOLD, FLD_ITALICS, FLD_SAMELINE,
            FLD_ALIGN,
            FLD_UNDERLINE, FLD_COLOR,
            FLD_FONT, RECORD_TYPE,FLD_FONTSIZE,FLD_LKPDATAVAL, FLD_LKPDISPVAL,
            CREATOR, CREATED_ON, IP_ADD,FLD_TODAYCHECK,FLD_OVERRIDE_MANDATORY,FLD_OVERRIDE_FORMAT,FLD_OVERRIDE_RANGE,FLD_OVERRIDE_DATE,FLD_EXPLABEL,
            FLD_HIDELABEL,   FLD_HIDERESPLABEL,     FLD_DISPLAY_WIDTH,    FLD_LINKEDFORM,       FLD_RESPALIGN,FLD_NAME_FORMATTED,FLD_SORTORDER)
            SELECT v_newfield, FK_CATLIB, FK_ACCOUNT, 'F', FLD_NAME,
            FLD_DESC, SUBSTR(FLD_UNIQUEID ,1,50), NULL, FLD_KEYWORD, FLD_TYPE,
            FLD_DATATYPE, FLD_INSTRUCTIONS, FLD_LENGTH, FLD_DECIMAL,
            FLD_LINESNO, FLD_CHARSNO, FLD_DEFRESP, FK_LOOKUP, FLD_ISUNIQUE,
            FLD_ISREADONLY, FLD_ISVISIBLE, FLD_COLCOUNT, FLD_FORMAT, 1,
            FLD_BOLD, FLD_ITALICS, FLD_SAMELINE,
            FLD_ALIGN ,
            FLD_UNDERLINE, FLD_COLOR,
            FLD_FONT, 'N',FLD_FONTSIZE,FLD_LKPDATAVAL, FLD_LKPDISPVAL,
            p_user, SYSDATE, p_ipadd,FLD_TODAYCHECK,FLD_OVERRIDE_MANDATORY,FLD_OVERRIDE_FORMAT,FLD_OVERRIDE_RANGE,FLD_OVERRIDE_DATE,FLD_EXPLABEL,
            FLD_HIDELABEL,   FLD_HIDERESPLABEL,     FLD_DISPLAY_WIDTH,    FLD_LINKEDFORM,       FLD_RESPALIGN,FLD_NAME_FORMATTED,FLD_SORTORDER
            FROM ER_FLDLIB
            WHERE pk_field = p_pk_field;
              --p(' i am in repeat pkf form ' || v_newfield  );
          IF SQL%FOUND THEN
        SELECT fld_systemid
        INTO v_fld_systemid_old
        FROM ER_FLDLIB
        WHERE pk_field = p_pk_field ;
          SELECT fld_systemid
          INTO v_fld_systemid_new
          FROM ER_FLDLIB
          WHERE pk_field = v_newfield ;
        BEGIN
         INSERT INTO ER_FLDVALIDATE
             (PK_FLDVALIDATE,FK_FLDLIB,FLDVALIDATE_OP1,FLDVALIDATE_VAL1,
              FLDVALIDATE_LOGOP1,FLDVALIDATE_OP2,
              FLDVALIDATE_VAL2,FLDVALIDATE_LOGOP2,
              FLDVALIDATE_JAVASCR,
             RECORD_TYPE,CREATOR,CREATED_ON, IP_ADD)
        SELECT v_newfldvalidate, v_newfield, FLDVALIDATE_OP1,FLDVALIDATE_VAL1,
          FLDVALIDATE_LOGOP1,FLDVALIDATE_OP2,
          FLDVALIDATE_VAL2,FLDVALIDATE_LOGOP2,
         REPLACE( FLDVALIDATE_JAVASCR , v_fld_systemid_old ,v_fld_systemid_new ) ,
         'N',p_user, SYSDATE, p_ipadd
      FROM ER_FLDVALIDATE
      WHERE fk_fldlib = p_pk_field;
        EXCEPTION WHEN NO_DATA_FOUND THEN
              P('no data');
    END;
                    -- The second insert for all the field responses
            INSERT INTO ER_FLDRESP
            (PK_FLDRESP,FK_FIELD,FLDRESP_SEQ,FLDRESP_DISPVAL,FLDRESP_DATAVAL,
            FLDRESP_SCORE,FLDRESP_ISDEFAULT,RECORD_TYPE,CREATOR,
            CREATED_ON,IP_ADD)
            SELECT seq_er_fldresp.NEXTVAL, v_newfield, FLDRESP_SEQ,FLDRESP_DISPVAL,FLDRESP_DATAVAL,
            FLDRESP_SCORE,FLDRESP_ISDEFAULT,DECODE(NVL(record_type,'N'),'H','H','N'),p_user,
            SYSDATE, p_ipadd
            FROM ER_FLDRESP
            WHERE fk_field = p_pk_field
            AND record_type <> 'D';
            IF SQL%FOUND THEN
               o_ret:= v_newfield;
            END IF;
          ELSE
            o_ret :=-1;
            RETURN;
          END IF;
        END;
        o_ret:= v_newfield;
    END; --end of SP_REPEAT_FIELD
-----------------------------------------------------------------------------------------------------
PROCEDURE SP_UPDATE_FILLEDFORM(p_pkfilledform IN NUMBER, p_formxml IN CLOB,
p_completed IN NUMBER, p_modifiedby IN NUMBER, p_valid IN NUMBER, p_ipAdd IN VARCHAR2, p_disptype IN VARCHAR2 , o_ret OUT NUMBER)
AS
 /****************************************************************************************************
   ** Procedure to update a filled account related form
   ** Author: Sonia Sahni 11th Aug 2003
   ** Input parameter p_pkfilledform  - Form ID
   ** Input parameter p_formxml - Form xml
   ** Input parameter p_completed - Form compeletion status
   ** Input parameter p_modifiedby - Form record modified by
   ** Input parameter p_valid - Form validity flag
   ** Input parameter p_ipAdd - User's IP ADD
   ** Input parameter p_disptype - Form display type
   ** Output parameter p_ret - Returns primary key of new record
   **/
  v_xmlclob    CLOB;
  BEGIN
  v_xmlclob :=  p_formxml;
  IF p_disptype = 'S' OR p_disptype = 'SA' THEN
    UPDATE ER_STUDYFORMS
    SET STUDYFORMS_XML =  XMLTYPE(v_xmlclob),
    FORM_COMPLETED = p_completed ,
    ISVALID  = p_valid, LAST_MODIFIED_BY = p_modifiedby, RECORD_TYPE = 'M' ,
    LAST_MODIFIED_DATE  = SYSDATE,IP_ADD = p_ipAdd
    WHERE PK_STUDYFORMS = p_pkfilledform;
 END IF;
 IF p_disptype = 'SP' OR p_disptype = 'PA' THEN
    UPDATE ER_PATFORMS
    SET PATFORMS_XML =  XMLTYPE(v_xmlclob),
    FORM_COMPLETED = p_completed ,
    ISVALID  = p_valid, LAST_MODIFIED_BY = p_modifiedby, RECORD_TYPE = 'M' ,
    LAST_MODIFIED_DATE  = SYSDATE,IP_ADD = p_ipAdd
    WHERE PK_PATFORMS = p_pkfilledform;
 END IF;
  IF p_disptype = 'A' THEN
    UPDATE ER_ACCTFORMS
    SET ACCTFORMS_XML =  XMLTYPE(v_xmlclob),
    FORM_COMPLETED = p_completed ,
    ISVALID  = p_valid, LAST_MODIFIED_BY = p_modifiedby, RECORD_TYPE = 'M' ,
    LAST_MODIFIED_DATE  = SYSDATE,IP_ADD = p_ipAdd
    WHERE PK_ACCTFORMS = p_pkfilledform and ACCT_FORMTYPE is null;
  END IF;
   IF p_disptype = 'ORG' or p_disptype = 'USR' THEN
    UPDATE ER_ACCTFORMS
    SET ACCTFORMS_XML =  XMLTYPE(v_xmlclob),
    FORM_COMPLETED = p_completed ,
    ISVALID  = p_valid, LAST_MODIFIED_BY = p_modifiedby, RECORD_TYPE = 'M' ,
    LAST_MODIFIED_DATE  = SYSDATE,IP_ADD = p_ipAdd
    WHERE PK_ACCTFORMS = p_pkfilledform and ACCT_FORMTYPE is  not null;
  END IF;
  
  IF p_disptype = 'C' THEN
    UPDATE ER_CRFFORMS
    SET CRFFORMS_XML =  XMLTYPE(v_xmlclob),
    FORM_COMPLETED = p_completed ,
    ISVALID  = p_valid, LAST_MODIFIED_BY = p_modifiedby, RECORD_TYPE = 'M' ,
    LAST_MODIFIED_DATE  = SYSDATE,IP_ADD = p_ipAdd
    WHERE PK_CRFFORMS = p_pkfilledform;
  END IF;
  o_ret := 0;
  EXCEPTION  WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR (-20102, 'Exception occurred in SP_UPDATE_FILLEDFORM :'||SQLERRM);
      o_ret := -1;
  END ; --end of SP_UPDATE_FILLEDFORM
------------------------------------------------------------------------------------------------------------------
PROCEDURE SP_SETFORMFLD_BROWSERFLAG(SETBROWSER_ONE VARCHAR2, SETBROWSER_ZERO VARCHAR2, LAST_MODBY NUMBER,
RECORD_TYPE VARCHAR2, p_ipadd VARCHAR2, o_ret OUT NUMBER)
AS
/****************************************************************************************************
   ** Procedure to set value of browserFlag to 1
   ** if the field is selected to be displayed in the browser
   ** it also sets the value of form_xslrefresh to 1 in er_formlib
   ** for the corresponding form id
   ** Author: Anu Khanna 14.08/2003
   ** Input parameter: Comma Separated Ids whose browserflag is to be set 1
   ** Input parameter: Comma Separated Ids whose browserflag is to be set 0
   ** Input parameter: last modified by
   ** Input parameter: record type as modified
   ** Input parameter: IP Address
   ** Output parameter: 1 if successful and -1 for error
   **/
str1 VARCHAR2(4000);
str2 VARCHAR2(4000);

mapformSQLStringOne long;
mapformSQLStringZero long;

BEGIN
  --to set value of browserflag to 1 for fields which are selected to be displayed in browser

begin

    IF  SETBROWSER_ONE IS NOT NULL THEN
          str1 := 'UPDATE ER_FORMFLD
         SET FORMFLD_BROWSERFLG = 1,
         LAST_MODIFIED_BY = ' ||LAST_MODBY || ',
         ip_add = '''||p_ipadd ||''' ,
         record_type='''||RECORD_TYPE ||'''
         WHERE PK_FORMFLD IN(' ||SETBROWSER_ONE ||')';
         EXECUTE IMMEDIATE str1;


         mapformSQLStringOne := 'UPDATE ER_MAPFORM  SET MP_BROWSER = 1 WHERE MP_PKFLD  IN( Select fk_field from er_formfld where pk_formfld in (' ||
         SETBROWSER_ONE || ' ) )';

         EXECUTE IMMEDIATE mapformSQLStringOne;

    END IF;



    --to set value of browserflag to 0 for fields which are not to be displayed in browser
    IF SETBROWSER_ZERO IS NOT NULL THEN
         str2 := 'UPDATE ER_FORMFLD
         SET FORMFLD_BROWSERFLG = 0,
         LAST_MODIFIED_BY = ' ||LAST_MODBY || ' ,
         ip_add = '''||p_ipadd ||''' ,
         record_type='''||RECORD_TYPE ||'''
         WHERE PK_FORMFLD IN(' ||SETBROWSER_ZERO ||')';

         EXECUTE IMMEDIATE str2;

         mapformSQLStringZero := 'UPDATE ER_MAPFORM  SET MP_BROWSER = 0 WHERE MP_PKFLD IN( Select fk_field from er_formfld where pk_formfld in (' ||
             SETBROWSER_ZERO || ' ) )';

         EXECUTE IMMEDIATE mapformSQLStringZero;

    END IF;

 COMMIT;
     o_ret :=1;
exception when others then
    plog.fatal('Exception in pkg_form.sp_setFORMFLD_BROWSERFLAG:'||sqlerrm);
    o_ret :=-1;
end ;



END; -- end of SP_SETFORMFLD_BROWSERFLAG Procedure
-----------------------------------------------------------------------------------------------------------------------------------
/********************/
-------------------------------------
PROCEDURE    SP_FLD_COPY (p_org_field_lib_id NUMBER , p_new_field_lib_id NUMBER , o_ret OUT NUMBER)
AS
/***************************************************************************************************************
   ** Procedure to copy the changes in the updated fields to the copied fields in ER_FLDLIB and
   ** delete the default responses corresponding to the copied fields if they exist and and make entry for new responses
   ** Author: Sonia Kaura 6th August 2003
   ** Input parameter: p_org_field_lib_id - PK of the original field
   ** Input parameter: PK of the field to which changes need to be copied
   **
   ** Output parameter: 1 for successful completion, -1 for error
   **/
    v_fld_libflag  CHAR(1)  ;
    v_fld_name VARCHAR2(2000) ;
    v_fld_name_formatted VARCHAR2(4000) ;
    v_sortorder VARCHAR(5);
    v_fld_desc VARCHAR2(1000) ;
    v_fld_uniqueid VARCHAR2(50);
    v_fld_keyword VARCHAR2(255) ;
    v_fld_type CHAR(1) ;
    v_fld_datatype  VARCHAR2(2) ;
    v_fld_instructions VARCHAR2(1000) ;
    v_fld_length NUMBER ;
    v_fld_decimal NUMBER ;
    v_fld_linesno NUMBER ;
    v_fld_charsno NUMBER ;
    v_fld_defresp  VARCHAR2(500) ;
    v_fk_lookup NUMBER ;
    v_fld_isunique NUMBER ;
    v_fld_isreadonly NUMBER ;
    v_fld_isvisible NUMBER ;
    v_fld_colcount NUMBER ;
    v_fld_format   VARCHAR2(100) ;
    v_fld_repeatflag NUMBER ;
    v_fld_bold NUMBER ;
    v_fld_italics NUMBER ;
    v_fld_sameline NUMBER ;
    v_fld_align VARCHAR2(15) ;
    v_fld_underline NUMBER ;
    v_fld_color VARCHAR2(15) ;
    v_fld_font VARCHAR2(50) ;
    v_fld_fontsize NUMBER ;
    v_record_type CHAR(1) ;
    v_fld_lkpdataval VARCHAR2(1000);
    v_fld_lkpdispval VARCHAR2(1000);
    v_ip_add VARCHAR2(15) ;
    v_last_modified_by  NUMBER ;
    v_fld_todaycheck NUMBER;
    v_fld_override_mand NUMBER;
    v_fld_override_format NUMBER;
    v_fld_override_range NUMBER;
    v_fld_override_date NUMBER;
    v_fld_explabel NUMBER;
    v_fldval_op1 CHAR(2);
    v_fldval_val1 VARCHAR2(100);
    v_fldval_logop1 VARCHAR2(10);
    v_fldval_op2 CHAR(2);
    v_fldval_val2 VARCHAR2(100);
    v_fldval_logop2 VARCHAR2(10);
    v_fldval_javascr VARCHAR2(4000);
    record_type CHAR(1);
    creator NUMBER;
    created_on DATE;
    ip_add VARCHAR2(15);
    v_fld_systemid_old VARCHAR2(50);
    v_fld_systemid_new VARCHAR2(50);
    V_FLD_HIDELABEL NUMBER;
    V_FLD_HIDERESPLABEL NUMBER;
    V_FLD_DISPLAY_WIDTH NUMBER;
    V_FLD_LINKEDFORM NUMBER;
      V_FLD_RESPALIGN VARCHAR2(16);
    BEGIN
            SELECT
        FLD_LIBFLAG,
        FLD_NAME,
        FLD_DESC,
        FLD_UNIQUEID,
        FLD_KEYWORD,
        FLD_TYPE,
        FLD_DATATYPE,
        FLD_INSTRUCTIONS,
        FLD_LENGTH,
        FLD_DECIMAL,
        FLD_LINESNO,
        FLD_CHARSNO,
        FLD_DEFRESP,
        FK_LOOKUP,
        FLD_ISUNIQUE,
        FLD_ISREADONLY,
        FLD_ISVISIBLE,
        FLD_COLCOUNT,
        FLD_FORMAT,
        FLD_REPEATFLAG,
        FLD_BOLD,
        FLD_ITALICS,
        FLD_SAMELINE,
        FLD_ALIGN,
        FLD_UNDERLINE,
        FLD_COLOR,
        FLD_FONT,
        RECORD_TYPE,
        FLD_FONTSIZE,
        FLD_LKPDATAVAL,
        FLD_LKPDISPVAL,
        IP_ADD,
          LAST_MODIFIED_BY,
        FLD_TODAYCHECK,
        FLD_OVERRIDE_MANDATORY,
        FLD_OVERRIDE_FORMAT,
        FLD_OVERRIDE_RANGE,
        FLD_OVERRIDE_DATE,
          FLD_SYSTEMID,
          FLD_EXPLABEL,
          FLD_HIDELABEL,   FLD_HIDERESPLABEL,     FLD_DISPLAY_WIDTH,    FLD_LINKEDFORM,       FLD_RESPALIGN,FLD_NAME_FORMATTED,FLD_SORTORDER
        INTO
        v_fld_libflag,
        v_fld_name,
        v_fld_desc,
        v_fld_uniqueid,
        v_fld_keyword,
        v_fld_type,
        v_fld_datatype,
        v_fld_instructions,
        v_fld_length,
        v_fld_decimal,
        v_fld_linesno,
        v_fld_charsno,
        v_fld_defresp,
        v_fk_lookup,
        v_fld_isunique,
        v_fld_isreadonly,
        v_fld_isvisible,
        v_fld_colcount,
        v_fld_format,
        v_fld_repeatflag,
        v_fld_bold,
        v_fld_italics,
        v_fld_sameline,
        v_fld_align,
        v_fld_underline,
        v_fld_color,
        v_fld_font,
        v_record_type,
        v_fld_fontsize,
        v_fld_lkpdataval,
        v_fld_lkpdispval,
        v_ip_add ,
        v_last_modified_by,
        v_fld_todaycheck,
        v_fld_override_mand,
        v_fld_override_format,
        v_fld_override_range,
        v_fld_override_date,
        v_fld_systemid_old,
        v_fld_explabel,
        V_FLD_HIDELABEL,   V_FLD_HIDERESPLABEL,     V_FLD_DISPLAY_WIDTH,    V_FLD_LINKEDFORM,       V_FLD_RESPALIGN
        ,v_fld_name_formatted
        ,v_sortorder
        FROM ER_FLDLIB
        WHERE pk_field = p_org_field_lib_id  ;
        UPDATE ER_FLDLIB
        SET
        FLD_LIBFLAG = v_fld_libflag ,
        FLD_NAME = v_fld_name ,
        FLD_DESC = v_fld_uniqueid ,
        FLD_UNIQUEID = v_fld_uniqueid ,
        FLD_KEYWORD = v_fld_keyword ,
        FLD_TYPE = v_fld_type ,
        FLD_DATATYPE = v_fld_datatype ,
        FLD_INSTRUCTIONS = v_fld_instructions,
        FLD_LENGTH = v_fld_length ,
        FLD_DECIMAL = v_fld_decimal ,
        FLD_LINESNO = v_fld_linesno ,
        FLD_CHARSNO = v_fld_charsno ,
        FLD_DEFRESP = v_fld_defresp,
        FK_LOOKUP = v_fk_lookup ,
        FLD_ISUNIQUE = v_fld_isunique,
        FLD_ISREADONLY = v_fld_isreadonly ,
        FLD_ISVISIBLE = v_fld_isvisible ,
        FLD_COLCOUNT = v_fld_colcount ,
        FLD_FORMAT = v_fld_format ,
        FLD_REPEATFLAG = v_fld_repeatflag ,
        FLD_BOLD = v_fld_bold ,
        FLD_ITALICS = v_fld_italics ,
        FLD_SAMELINE = v_fld_sameline ,
        FLD_ALIGN = v_fld_align ,
        FLD_UNDERLINE = v_fld_underline ,
        FLD_COLOR = v_fld_color ,
        FLD_FONT = v_fld_font ,
        RECORD_TYPE = v_record_type ,
        FLD_FONTSIZE = v_fld_fontsize ,
        FLD_LKPDATAVAL = v_fld_lkpdataval ,
        FLD_LKPDISPVAL = v_fld_lkpdispval ,
        IP_ADD = v_ip_add ,
        LAST_MODIFIED_BY = v_last_modified_by,
        FLD_TODAYCHECK = v_fld_todaycheck,
        FLD_OVERRIDE_MANDATORY= v_fld_override_mand,
        FLD_OVERRIDE_FORMAT= v_fld_override_format,
        FLD_OVERRIDE_RANGE= v_fld_override_range,
        FLD_OVERRIDE_DATE= v_fld_override_date,
        FLD_EXPLABEL = v_fld_explabel,
        FLD_HIDELABEL = V_FLD_HIDELABEL,   FLD_HIDERESPLABEL = V_FLD_HIDERESPLABEL,
        FLD_DISPLAY_WIDTH = V_FLD_DISPLAY_WIDTH,    FLD_LINKEDFORM = V_FLD_LINKEDFORM,       FLD_RESPALIGN = V_FLD_RESPALIGN
        ,FLD_NAME_FORMATTED=v_fld_name_formatted
        ,FLD_SORTORDER=v_sortorder
        WHERE pk_field = p_new_field_lib_id ;
          IF SQL%FOUND THEN
            DBMS_OUTPUT.PUT_LINE ('Insert into field library ' || TO_CHAR(1)) ;
             DELETE FROM ER_FLDRESP
            WHERE fk_field = p_new_field_lib_id ;
            INSERT INTO ER_FLDRESP
            ( PK_FLDRESP,
             FK_FIELD,
             FLDRESP_SEQ,
              FLDRESP_DISPVAL,
             FLDRESP_DATAVAL,
             FLDRESP_SCORE,
             FLDRESP_ISDEFAULT ,
             RECORD_TYPE ,
             CREATOR ,
             LAST_MODIFIED_BY ,
             IP_ADD ,
             CREATED_ON ,
             LAST_MODIFIED_DATE )
             SELECT
             seq_er_fldresp.NEXTVAL,
             p_new_field_lib_id,
             FLDRESP_SEQ,
             FLDRESP_DISPVAL,
             FLDRESP_DATAVAL,
             FLDRESP_SCORE,
             FLDRESP_ISDEFAULT,
             DECODE(NVL(record_type,'N'),'H','H','N'),
             CREATOR ,
             LAST_MODIFIED_BY,
             IP_ADD ,
             CREATED_ON,
             SYSDATE
             FROM ER_FLDRESP
             WHERE fk_field = p_org_field_lib_id
             AND NVL(record_type,'Z') <> 'D' ;
             o_ret := 1 ;
          ELSE
           o_ret := -1;
        RETURN ;
        END IF ;

        SELECT FLD_SYSTEMID
        INTO v_fld_systemid_new
        FROM ER_FLDLIB
        WHERE PK_FIELD = p_new_field_lib_id    ;
          BEGIN
        SELECT FLDVALIDATE_OP1,FLDVALIDATE_VAL1,
               FLDVALIDATE_LOGOP1,FLDVALIDATE_OP2,
               FLDVALIDATE_VAL2,FLDVALIDATE_LOGOP2,
               REPLACE( FLDVALIDATE_JAVASCR , v_fld_systemid_old ,v_fld_systemid_new ),
               RECORD_TYPE,CREATOR,CREATED_ON, IP_ADD
          INTO v_fldval_op1,v_fldval_val1,
               v_fldval_logop1,v_fldval_op2,
               v_fldval_val2,v_fldval_logop2,
               v_fldval_javascr,
               record_type,creator,created_on,ip_add
            FROM ER_FLDVALIDATE
            WHERE fk_fldlib = p_org_field_lib_id;
        UPDATE ER_FLDVALIDATE
         SET
             FLDVALIDATE_OP1=v_fldval_op1,
             FLDVALIDATE_VAL1=v_fldval_val1,
             FLDVALIDATE_LOGOP1=v_fldval_logop1,
             FLDVALIDATE_OP2=v_fldval_op2,
             FLDVALIDATE_VAL2=v_fldval_val2,
             FLDVALIDATE_LOGOP2=v_fldval_logop2,
             FLDVALIDATE_JAVASCR=v_fldval_javascr,
             RECORD_TYPE=record_type,
             CREATOR=creator,
             CREATED_ON=created_on,
             IP_ADD=ip_add
            WHERE fk_fldlib = p_new_field_lib_id ;
           EXCEPTION WHEN NO_DATA_FOUND THEN
             P('no data');
         END;
    END ; -- END OF SP_FLD_COPY
-------------------------------------------------------------------------------------
PROCEDURE SP_COPY_MULTIPLE_FORMS(p_form_ids ARRAY_STRING,p_form_desc VARCHAR2 , p_form_type NUMBER , p_linked_form_flag CHAR ,
    p_stdacc_id NUMBER, p_user_id NUMBER , p_form_name VARCHAR2, p_disp_type VARCHAR2, p_ip_add VARCHAR2  , o_ret_number OUT NUMBER)
AS
/***************************************************************************************************************
   ** Procedure to copy the multiple forms and merge them into one form
   ** Author: Sonia Kaura 22nd August 2003
   ** Input parameter:Array of the formids to be merged
   ** Input parameter: Form Description
   ** Input parameter: The Category to which the new form would be belong
   ** Input parameter: Different handling if the Forms are linked to Account 'A' or 'S'
   ** Input parameter: The pk of the study if the form is linked to a study
   ** Input parameter: The pk of  the user
   ** Input parameter: The form name for the new merged form
   ** Input parameter: The linked form display type
   ** Input parameter: The Ipadd
   ** Output parameter: The pk of the new form  for successful completion, -1 for error
   ** Modified by Salil on Oct 09, 2003 for generating the section sequence instead of copying as such
   ** Modified by Anu on Dec 05, 2003 for copying form messages
   ** Modified by Sonika on Dec 09, 2003 to prevent creation of default section and date field
   **/
v_formcnt NUMBER;
v_form_tocopy   NUMBER ;
v_pk_formlib NUMBER ;
v_pk_search_formlib NUMBER ;
v_org_fieldlib_id NUMBER ;
v_pk_formnotify NUMBER;
v_new_formsec_id NUMBER ;
v_pk_ercodelst NUMBER ;
v_fk_account NUMBER ;
v_ret NUMBER ;
v_ret1 NUMBER ;
v_ret2 NUMBER ;
v_ret_def_formfld NUMBER ;
k NUMBER;
f NUMBER;
v_formfld_javascr VARCHAR2(4000) ;
v_formfld_xsl   VARCHAR2(4000);
v_date_pk_formfld NUMBER ;
v_form_desc VARCHAR2(4000);
v_sec_count NUMBER;
v_form_crflnk NUMBER;
v_sysids_xml CLOB;
v_oldsys VARCHAR2(50);
v_newsys VARCHAR2(50);
v_newfield NUMBER;
--added by Sonika on Dec 09,2003
v_copy_defdate BOOLEAN;
-- added by sonia on may 28 04
v_new_fldaction NUMBER;

v_form_refresh_value NUMBER := 1;
v_form_preserve_formatting NUMBER := 0;

v_form_xsl CLOB;
 v_form_xml CLOB;
 v_form_viewxsl CLOB;
 v_form_custom_js CLOB;
 v_form_activation_js CLOB;

 v_form_saveformat NUMBER := 0;
 v_dontcopy boolean := false;

BEGIN
    v_form_refresh_value := 1;

     v_formcnt := p_form_ids.COUNT();

     IF  v_formcnt  = 1 THEN -- settings applicable only if one form is slected
        -- get form refresh setting from controltable

        BEGIN
             SELECT form_desc INTO v_form_desc FROM ER_FORMLIB WHERE PK_FORMLIB = p_form_ids(1);

             SELECT NVL(FORM_SAVEFORMAT,0) INTO v_form_preserve_formatting FROM ER_FORMLIB  WHERE PK_FORMLIB = p_form_ids(1) ;

             IF v_form_preserve_formatting = 1 THEN
                v_form_refresh_value := 0;  -- form will not be refreshed, formatting will be preserved
               v_form_saveformat := 1;  -- column value for form_saveformat
            ELSE
                v_form_refresh_value := 1; -- form will be refreshed, formatting will not be preserved
                v_form_saveformat := 0;  -- column value for form_saveformat
             END IF;
        EXCEPTION WHEN NO_DATA_FOUND THEN
                v_form_refresh_value := 1;
              v_form_saveformat := 0;  -- column value for form_saveformat
        END;
    ELSE -- if there are more than one forms, refresh flag will be 1
         v_form_refresh_value := 1;
          v_form_saveformat := 0; -- column value for form_saveformat
    END IF;


     --get the account id from one of the forms given form merger
      v_fk_account := 0 ;
     SELECT fk_account
     INTO v_fk_account
     FROM ER_USER WHERE
     pk_user = p_user_id;
     v_pk_search_formlib := -3 ;
    --- Check uniqueness in its own type (anu-- 7th June 05).
    IF  p_disp_type  is not null  THEN
      --- Check for uniqueness of name by three large display types (Study, Account, Patient)
      IF (trim(p_disp_type) = 'S') THEN
        select count(*) into v_pk_search_formlib from ER_LINKEDFORMS L, ER_FORMLIB F where
          L.FK_FORMLIB = F.PK_FORMLIB and lower(F.FORM_NAME) = lower(p_form_name) and
          NVL(L.record_type,'Z') <> 'D' and  L.FK_ACCOUNT = v_fk_account and
          trim(L.LF_DISPLAYTYPE) = 'S' and L.FK_STUDY = p_stdacc_id;
      ELSIF (p_disp_type = 'SA') THEN
        select count(*) into v_pk_search_formlib from ER_LINKEDFORMS L, ER_FORMLIB F where
          L.FK_FORMLIB = F.PK_FORMLIB and lower(F.FORM_NAME) = lower(p_form_name) and
          NVL(L.record_type,'Z') <> 'D' and
          L.FK_ACCOUNT = v_fk_account and
          L.LF_DISPLAYTYPE = 'SA';
      ELSIF (trim(p_disp_type) = 'A') THEN
        select count(*) into v_pk_search_formlib from ER_LINKEDFORMS L, ER_FORMLIB F where
          L.FK_FORMLIB = F.PK_FORMLIB and lower(F.FORM_NAME) = lower(p_form_name) and
          NVL(L.record_type,'Z') <> 'D' and
          L.FK_ACCOUNT = v_fk_account and
          trim(L.LF_DISPLAYTYPE) = 'A' and
          trim(p_disp_type) = 'A';
      ELSIF (trim(p_disp_type) = 'SP') THEN
        select count(*) into v_pk_search_formlib from ER_LINKEDFORMS L, ER_FORMLIB F where
          L.FK_FORMLIB = F.PK_FORMLIB and lower(F.FORM_NAME) = lower(p_form_name) and
          NVL(L.record_type,'Z') <> 'D' and
          L.FK_ACCOUNT = v_fk_account and
          trim(L.LF_DISPLAYTYPE) = 'SP' and
          L.FK_STUDY = p_stdacc_id;
      ELSIF (trim(p_disp_type) = 'PS') THEN
        select count(*) into v_pk_search_formlib from ER_LINKEDFORMS L, ER_FORMLIB F where
          L.FK_FORMLIB = F.PK_FORMLIB and lower(F.FORM_NAME) = lower(p_form_name) and
          NVL(L.record_type,'Z') <> 'D' and
          L.FK_ACCOUNT = v_fk_account and
          trim(L.LF_DISPLAYTYPE) = 'PS';
      ELSIF (trim(p_disp_type) = 'PA') THEN
        select count(*) into v_pk_search_formlib from ER_LINKEDFORMS L, ER_FORMLIB F where
          L.FK_FORMLIB = F.PK_FORMLIB and lower(F.FORM_NAME) = lower(p_form_name) and
          NVL(L.record_type,'Z') <> 'D' and
          L.FK_ACCOUNT = v_fk_account and
          trim(L.LF_DISPLAYTYPE) = 'PA';
      ELSIF (trim(p_disp_type) = 'PR') THEN
        select count(*) into v_pk_search_formlib from ER_LINKEDFORMS L, ER_FORMLIB F where
          L.FK_FORMLIB = F.PK_FORMLIB and lower(F.FORM_NAME) = lower(p_form_name) and
          NVL(L.record_type,'Z') <> 'D' and
          L.FK_ACCOUNT = v_fk_account and
          L.LF_DISPLAYTYPE = 'PR';
      END IF;

    ELSE
      -- for p_disp_type is null, do the traditional duplicate name check
      IF  p_linked_form_flag = 'S'  THEN
      -- in case of study linked forms check for name uniqueness within study
        SELECT COUNT(*)
        INTO v_pk_search_formlib
        FROM ER_FORMLIB, ER_LINKEDFORMS
        WHERE LOWER(trim(form_name)) = LOWER(trim(p_form_name))
        AND ER_LINKEDFORMS.fk_formlib = ER_FORMLIB.pk_formlib
        AND FK_STUDY = P_STDACC_ID
        AND ER_FORMLIB.form_linkto = 'S'
        AND NVL(ER_LINKEDFORMS.record_type,'Z') <> 'D';
      ELSIF  p_linked_form_flag = 'A' THEN
        -- in case of account linked forms check for name uniqueness within account
        SELECT COUNT(*)
        INTO v_pk_search_formlib
        FROM ER_FORMLIB, ER_LINKEDFORMS
        WHERE LOWER(trim(form_name)) = LOWER(trim(p_form_name))
        AND ER_LINKEDFORMS.fk_formlib = ER_FORMLIB.pk_formlib
        AND ER_LINKEDFORMS.FK_ACCOUNT = V_FK_ACCOUNT
        AND ER_FORMLIB.form_linkto = 'A'
        AND NVL(ER_LINKEDFORMS.record_type,'Z') <> 'D';
      ELSIF  p_linked_form_flag = 'L' THEN
        -- in case of library forms check for name uniqueness within account
        SELECT COUNT(*)
        INTO v_pk_search_formlib
        FROM ER_FORMLIB
        WHERE LOWER(trim(form_name)) = LOWER(trim(p_form_name))
        AND fk_account = v_fk_account
        AND form_linkto = 'L'
        AND NVL(ER_FORMLIB.record_type,'Z') <> 'D';
      ELSIF p_linked_form_flag <> 'C' THEN
        -- for all other forms check for name uniqueness within account
        SELECT COUNT(*)
        INTO v_pk_search_formlib
        FROM ER_FORMLIB
        WHERE LOWER(trim(form_name)) = LOWER(trim(p_form_name))
        AND fk_account = v_fk_account
        AND NVL(ER_FORMLIB.record_type,'Z') <> 'D';
      END IF;
    END IF;

    IF  v_pk_search_formlib > 0  THEN
      o_ret_number := -3 ;
      RETURN ;
    END IF ;
    --get the pk for the new form which can be one form or merge of more forms
    SELECT seq_er_formlib.NEXTVAL
    INTO v_pk_formlib FROM dual ;
    --select the pk of the work in progress from the er_codelst
    IF p_linked_form_flag = 'S' OR p_linked_form_flag = 'A'  OR p_linked_form_flag = 'C' THEN
       SELECT pk_codelst
      INTO v_pk_ercodelst
      FROM ER_CODELST
      WHERE codelst_type = 'frmstat' AND codelst_subtyp = 'W' ;
    ELSE
       SELECT pk_codelst
      INTO v_pk_ercodelst
      FROM ER_CODELST
      WHERE codelst_type = 'frmlibstat' AND codelst_subtyp = 'W' ;
    END IF;
    --insert the new form

    -- get form xml and xsls
    IF  v_form_refresh_value = 0 THEN

        SELECT form_xsl, e.form_xml.getClobVal(),  form_viewxsl , form_custom_js, form_activation_js
        INTO v_form_xsl, v_form_xml,  v_form_viewxsl , v_form_custom_js, v_form_activation_js
        FROM ER_FORMLIB e WHERE pk_formlib = p_form_ids( 1 );

    END IF;

    IF p_form_type = 0 THEN
    INSERT INTO ER_FORMLIB
    (
        PK_FORMLIB,  FK_ACCOUNT, FORM_NAME , FORM_DESC, FORM_STATUS , FORM_LINKTO,
         FORM_XSLREFRESH , FORM_SHAREDWITH , CREATOR ,  RECORD_TYPE , CREATED_ON   , IP_ADD , form_saveformat
    )
     VALUES
    (v_pk_formlib ,  v_fk_account , p_form_name , v_form_desc, v_pk_ercodelst ,p_linked_form_flag,
        v_form_refresh_value , 'A' , p_user_id , 'N' , SYSDATE ,  p_ip_add , v_form_saveformat);

    ELSE
    INSERT INTO ER_FORMLIB
    (
        PK_FORMLIB, FK_CATLIB , FK_ACCOUNT, FORM_NAME , FORM_DESC, FORM_STATUS , FORM_LINKTO,
         FORM_XSLREFRESH , FORM_SHAREDWITH , CREATOR ,  RECORD_TYPE , CREATED_ON   , IP_ADD, form_saveformat
    )
     VALUES
    (
         v_pk_formlib , p_form_type , v_fk_account , p_form_name , v_form_desc, v_pk_ercodelst ,p_linked_form_flag,
        v_form_refresh_value , 'A' , p_user_id , 'N' , SYSDATE ,  p_ip_add, v_form_saveformat
    ) ;
  END IF;
     v_ret1 := -2 ;
    v_ret_def_formfld := -2 ;
----commented by Sonika on Dec 09,2003, no need to add a default section and data entry date when form is linked or copied
/*
    pkg_form.SP_FORMDEFAULTDATA ( v_pk_formlib,  v_ret1 , v_ret_def_formfld );
    if ( v_ret_def_formfld < 0 ) then
    o_ret_number := v_ret_def_formfld ;
     return ;
    end if ;
    v_ret := -2 ;
*/
    IF (NVL(p_linked_form_flag,'Z') <> 'S'  OR NVL(p_linked_form_flag,'Z') <> 'C') THEN
        --pkg_form.SP_FORMSHAREWITH( v_pk_formlib, to_char(p_user_id)  ,'P', to_char(p_user_id) , p_ip_add , 'N', v_ret );
--changed mode from 'N' to C' to prevent creation of default section and date field when form is copied/linked by Sonika on Dec 09,2003
        --SP_OBJECTSHAREWITH is used now to make it generic instead of pkg_form.SP_FORMSHAREWITH -- by anu.
        Sp_Objectsharewith( v_pk_formlib,'1', TO_CHAR(v_fk_account)  ,'A', TO_CHAR(p_user_id) , p_ip_add , v_ret );
    END IF ;
    ----added by Sonika on Dec 09,2003
    v_copy_defdate := FALSE;
    --select the xsl of the old default date
 ----commented by Sonika on Dec 09,2003
/*
    select formfld_xsl , formfld_javascr
    into v_formfld_xsl , v_formfld_javascr
    from  er_formfld , er_fldlib , er_formsec
    where
    fk_formsec = pk_formsec
    and  fk_field = pk_field and
    fld_systemid = 'er_def_date_01' and
    er_formfld.record_type <> 'D' and fk_formlib =  to_number( p_form_ids(1) ) ;
    --update the default date field xsls
    update er_formfld
    set
    formfld_xsl = v_formfld_xsl  ,
     formfld_javascr = v_formfld_javascr
     where  pk_formfld =  v_ret_def_formfld ;
*/
    --iterate over all the form lib ids
     k := 1 ;
     --by salil 9 oct 2003
     v_sec_count := 1;
    WHILE k <= v_formcnt LOOP
        v_form_tocopy := TO_NUMBER( p_form_ids( k ) );
   ------------------To insert formnotify data of each form ----
       FOR i IN ( SELECT pk_fn FROM ER_FORMNOTIFY WHERE record_type <> 'D' AND fk_formlib = v_form_tocopy )
       LOOP
          SELECT seq_er_formnotify.NEXTVAL
          INTO v_pk_formnotify FROM dual ;
        INSERT INTO ER_FORMNOTIFY (pk_fn,fk_formlib,fn_msgtype,fn_msgtext,fn_sendtype,fn_users,
                               record_type,creator,created_on,ip_add)
           SELECT v_pk_formnotify,v_pk_formlib,fn_msgtype,fn_msgtext,fn_sendtype,fn_users,
                                   'N',p_user_id,SYSDATE,p_ip_add
                        FROM ER_FORMNOTIFY
                        WHERE pk_fn=i.pk_fn;
      END LOOP; --formnotify loop
     -------------------------
       --p('inside while loop ' );
       /* by Sonia Sahni 05/27/04*************
       copy field actions data of the old for for the new form. Later on we will replace the
       new values
       ***************************************************/
       FOR s IN (SELECT  PK_FLDACTION,FLDACTION_TYPE, FK_FIELD, FLDACTION_CONDITION
       FROM ER_FLDACTION
       WHERE fk_form = v_form_tocopy)
       LOOP
             SELECT  SEQ_ER_FLDACTION.NEXTVAL
             INTO v_new_fldaction
             FROM dual;
             INSERT INTO ER_FLDACTION (  PK_FLDACTION , FK_FORM, FLDACTION_TYPE, FK_FIELD, FLDACTION_CONDITION)
             VALUES (v_new_fldaction,v_pk_formlib, s.FLDACTION_TYPE, s.FK_FIELD, s.FLDACTION_CONDITION);
             --insert er_fldactioninfo for this fldaction
             INSERT INTO ER_FLDACTIONINFO (PK_FLDACTIONINFO,FK_FLDACTION, INFO_TYPE ,INFO_VALUE )
             SELECT   SEQ_ER_FLDACTIONINFO.NEXTVAL, v_new_fldaction, INFO_TYPE ,INFO_VALUE
             FROM ER_FLDACTIONINFO
             WHERE FK_FLDACTION = s.PK_FLDACTION;
       END LOOP;
       /************** Sonia - END OF CHANGE *******************/
       FOR i IN ( SELECT pk_formsec FROM ER_FORMSEC WHERE record_type <> 'D' AND fk_formlib = v_form_tocopy ORDER BY formsec_seq)
        LOOP
         --get the pk for the new record for the section
         SELECT seq_er_formsec.NEXTVAL
         INTO v_new_formsec_id
         FROM dual ;
         --by salil 9 oct 2003
         INSERT INTO ER_FORMSEC (
         PK_FORMSEC , FK_FORMLIB , FORMSEC_NAME , FORMSEC_SEQ ,
           FORMSEC_FMT ,FORMSEC_REPNO , RECORD_TYPE ,CREATOR , CREATED_ON  ,IP_ADD )
           SELECT v_new_formsec_id , v_pk_formlib ,  FORMSEC_NAME ,   v_sec_count ,
          FORMSEC_FMT , FORMSEC_REPNO , 'N' , p_user_id , SYSDATE , p_ip_add
         FROM ER_FORMSEC
         WHERE pk_formsec = i.pk_formsec ;

          IF    v_form_refresh_value = 0 THEN
                    --replace pksec in formxml
                     v_form_xml := REPLACE ( v_form_xml, 'pksec = "' || i.pk_formsec || '"', 'pksec = "' || v_new_formsec_id || '"');
        END IF;


         v_sysids_xml := '';
         v_sysids_xml := '<ROWS>';
        ----changed by Sonika on Dec 09,2003, copy default date also
         /*  for j in ( select pk_formfld, fk_field ,fld_systemid
                    from er_formfld , er_fldlib
                    where fk_formsec = i.pk_formsec  and
                    fk_field = pk_field and
                    fld_systemid <> 'er_def_date_01' and
                    er_formfld.record_type <> 'D' and NVL(fld_datatype,'-') <> 'ML'
                    )*/
         FOR j IN ( SELECT pk_formfld, fk_field ,fld_systemid,fld_type
                    FROM ER_FORMFLD , ER_FLDLIB
                    WHERE fk_formsec = i.pk_formsec  AND
                    fk_field = pk_field AND
                    ER_FORMFLD.record_type <> 'D' AND NVL(fld_datatype,'-') <> 'ML'
                    )
             LOOP
                v_dontcopy := false;

                if (j.fld_type = 'F' and  p_linked_form_flag = 'L' ) THEN
                        v_dontcopy := true;
                end if;


                 if (v_dontcopy = false) then --copy the field
                   --get the orginial fieldid from er_formfld
                        v_org_fieldlib_id := j.fk_field;
                        v_oldsys := j.fld_systemid;
                      --added by Sonika on Dec 09,2003
                      --incase of multiple copies, the default date should be copied only once
                      IF (v_oldsys = 'er_def_date_01') THEN
                            IF (v_copy_defdate=FALSE) THEN
                               -- one more parameter added (v_formcnt) to check whether one form or more than one forms are copied
                                   Pkg_Form.SP_COPYFLD_FOR_FORM ( v_org_fieldlib_id , v_new_formsec_id , p_user_id , p_ip_add , v_fk_account ,v_formcnt, v_newfield ,v_newsys) ;
                               v_sysids_xml := v_sysids_xml || '<' || v_oldsys || '>' || v_newsys || '</' || v_oldsys || '>';
                                v_copy_defdate:=TRUE;

                            --replace system id in form's xml/xsl
                                IF  v_form_refresh_value = 0 THEN
                                            SP_REPLACE_SYSIDS (v_oldsys, v_newsys,  v_form_xsl ,  v_form_xml , v_form_viewxsl ,v_form_custom_js,v_form_activation_js );
                                        --replace pkfld with enw pk_field
                                           v_form_xml := REPLACE ( v_form_xml , 'pkfld = "' || v_org_fieldlib_id  || '"', 'pkfld = "' ||  v_newfield || '"');
                                           SP_REPLACE_REPEAT_SYSIDS (v_org_fieldlib_id, v_newfield,v_form_xsl , v_form_xml , v_form_viewxsl ,v_form_custom_js,v_form_activation_js  );

                                END IF;

                            END IF;
                      ELSE
                            -- one more parameter added (v_formcnt) to check whether one form or more than one forms are copied
                                Pkg_Form.SP_COPYFLD_FOR_FORM ( v_org_fieldlib_id , v_new_formsec_id , p_user_id , p_ip_add , v_fk_account ,v_formcnt, v_newfield ,v_newsys) ;
                            v_sysids_xml := v_sysids_xml || '<' || v_oldsys || '>' || v_newsys || '</' || v_oldsys || '>';

                            --replace system id in form's xml/xsl
                            IF  v_form_refresh_value = 0 THEN
                                        SP_REPLACE_SYSIDS (v_oldsys, v_newsys,  v_form_xsl ,  v_form_xml , v_form_viewxsl ,v_form_custom_js,v_form_activation_js );
                                        --replace pkfld with enw pk_field
                                        v_form_xml := REPLACE ( v_form_xml , 'pkfld = "' || v_org_fieldlib_id  || '"', 'pkfld = "' ||  v_newfield || '"');
                                        SP_REPLACE_REPEAT_SYSIDS (v_org_fieldlib_id, v_newfield,v_form_xsl , v_form_xml , v_form_viewxsl ,v_form_custom_js,v_form_activation_js  );
                            END IF;

                      END IF;

                 end if; -- for v_dontcopy

             END LOOP ; -- end loop for all fields
             v_sysids_xml := v_sysids_xml || '</ROWS>';
             FOR j IN ( SELECT pk_formfld, fk_field ,fld_systemid
                    FROM ER_FORMFLD , ER_FLDLIB
                    WHERE fk_formsec = i.pk_formsec  AND
                    fk_field = pk_field AND
                    fld_systemid <> 'er_def_date_01' AND
                    ER_FORMFLD.record_type <> 'D' AND NVL(fld_datatype,'-') = 'ML'
                    )
             LOOP
           --get the orginial fieldid from er_formfld
                v_org_fieldlib_id := j.fk_field;
            -- one more parameter added (v_formcnt) to check whether one form or more than one forms are copied
                SP_COPYLOOKUPFLD_FOR_FORM  ( v_org_fieldlib_id , v_new_formsec_id , p_user_id , p_ip_add , v_fk_account ,v_formcnt,  v_sysids_xml, v_newfield ,v_newsys ) ;

                            --replace system id in form's xml/xsl
            IF  v_form_refresh_value = 0 THEN
                        SP_REPLACE_SYSIDS (j.fld_systemid, v_newsys,  v_form_xsl ,  v_form_xml , v_form_viewxsl ,v_form_custom_js,v_form_activation_js );
                                --replace pkfld with enw pk_field
                           v_form_xml := REPLACE ( v_form_xml , 'pkfld = "' || v_org_fieldlib_id  || '"', 'pkfld = "' ||  v_newfield || '"');
                           SP_REPLACE_REPEAT_SYSIDS (v_org_fieldlib_id, v_newfield,v_form_xsl , v_form_xml , v_form_viewxsl ,v_form_custom_js,v_form_activation_js  );

            END IF;

             END LOOP ;
             --by salil 9 oct 2003
       v_sec_count := v_sec_count + 1;
          END LOOP ;
       k := k + 1 ;
    END LOOP ;
    o_ret_number := v_pk_formlib ;

        IF  v_form_refresh_value = 0 THEN
                            UPDATE ER_FORMLIB SET form_xsl = v_form_xsl , form_xml = XMLTYPE.createXML(v_form_xml) ,
                            form_viewxsl = v_form_viewxsl  ,
                            form_custom_js = v_form_custom_js, form_activation_js = v_form_activation_js , form_xslrefresh = 0
                            WHERE pk_formlib = v_pk_formlib ;
        END IF;


    COMMIT ;
    END    ;
   -- end of SP_COPY_MULTIPLE_FORMS
---------------------------------------------------
PROCEDURE SP_COPYFLD_FOR_FORM ( p_org_field_lib_id NUMBER , p_formsec NUMBER , p_user_id NUMBER , p_ip_add VARCHAR2  , p_account_id NUMBER ,p_formcnt NUMBER ,o_ret OUT NUMBER, o_newsys OUT VARCHAR2 )
/***************************************************************************************************************
   ** Procedure to copy fields of the form to a new form
   ** Author: Sonia Kaura 22nd August 2003
   ** Input parameter:pk of the original field to be copied
   ** Input parameter: The pk of the new section to which the fields are copied
   ** Input parameter: The pk of the user
   ** Input parameter: The Ipadd
   ** Input parameter: The account id passed as parameter instead of read again
   ** Output parameter: The 1  for successful completion, -1 for error
   ** o_newsys - system id of the new field
   **/
   AS
    v_newfldlib_id      NUMBER ;
    v_newuniqueid       NUMBER;
    v_newformfld_id     NUMBER ;
    v_oldformfld_id     NUMBER ;
    v_newfldvalidate   NUMBER;
    v_fld_systemid_old VARCHAR2(50) ;
    v_fld_systemid_new VARCHAR2(50) ;
    v_formfld_javascr VARCHAR2(4000) ;
    v_formfld_javascr_init VARCHAR2(4000) ;
    v_formfld_xsl   VARCHAR2(4000);
    v_formfld_xsl_init VARCHAR2(4000);
    v_formuniqueid VARCHAR2(50);
    v_new_form NUMBER;
    v_new_pkfldresp NUMBER;
    BEGIN
        --select the pk of the new field
        SELECT seq_er_fldlib.NEXTVAL,seq_flduniqueid.NEXTVAL
        INTO v_newfldlib_id, v_newuniqueid
        FROM dual ;
        SELECT seq_er_fldvalidate.NEXTVAL
        INTO v_newfldvalidate
        FROM dual;
    --changed by Sonika on Dec 09,2003, added system_id for default data entry date
         INSERT INTO ER_FLDLIB (
        PK_FIELD ,     FK_ACCOUNT ,      FLD_LIBFLAG,  FLD_NAME,
        FLD_DESC,
        FLD_UNIQUEID,
        FLD_SYSTEMID,
        FLD_KEYWORD,  FLD_TYPE,
        FLD_DATATYPE,  FLD_INSTRUCTIONS, FLD_LENGTH,   FLD_DECIMAL,
        FLD_LINESNO,   FLD_CHARSNO,      FLD_DEFRESP,  FK_LOOKUP,
        FLD_ISUNIQUE,  FLD_ISREADONLY,   FLD_ISVISIBLE,FLD_COLCOUNT,
        FLD_FORMAT,    FLD_REPEATFLAG,   FLD_BOLD,     FLD_ITALICS,
        FLD_SAMELINE,
        FLD_ALIGN,
          FLD_UNDERLINE,FLD_COLOR,
        FLD_FONT,      RECORD_TYPE,      FLD_FONTSIZE, FLD_LKPDATAVAL,
        FLD_LKPDISPVAL,IP_ADD,           CREATOR ,     CREATED_ON ,
        FLD_TODAYCHECK, FLD_OVERRIDE_MANDATORY,FLD_OVERRIDE_FORMAT,FLD_OVERRIDE_RANGE,FLD_OVERRIDE_DATE,FLD_EXPLABEL,
        FLD_HIDELABEL,   FLD_HIDERESPLABEL,     FLD_DISPLAY_WIDTH,       FLD_RESPALIGN,FLD_NAME_FORMATTED,FLD_SORTORDER,fld_linkedform)
        SELECT v_newfldlib_id , p_account_id ,  FLD_LIBFLAG , fld_name ,
        FLD_DESC,
        CASE WHEN  fld_type = 'C'  OR fld_type='H' THEN ''
             WHEN (fld_type='E' AND FLD_systemID='er_def_date_01') THEN 'er_def_date_01'
             WHEN fld_type='E' OR fld_type='M' THEN  SUBSTR(FLD_UNIQUEID ,1,50)  END ,
        CASE WHEN (fld_type='E' AND FLD_systemID='er_def_date_01') THEN 'er_def_date_01' END,
        FLD_KEYWORD , FLD_TYPE ,
        FLD_DATATYPE , FLD_INSTRUCTIONS ,FLD_LENGTH  , FLD_DECIMAL  ,
        FLD_LINESNO,   FLD_CHARSNO,      FLD_DEFRESP,  FK_LOOKUP,
        FLD_ISUNIQUE,  FLD_ISREADONLY,   FLD_ISVISIBLE,FLD_COLCOUNT,
        FLD_FORMAT,    FLD_REPEATFLAG,   FLD_BOLD,     FLD_ITALICS,
        FLD_SAMELINE,
          FLD_ALIGN,
        FLD_UNDERLINE,FLD_COLOR,
        FLD_FONT,      'N',              FLD_FONTSIZE, FLD_LKPDATAVAL,
        FLD_LKPDISPVAL, p_ip_add ,       p_user_id,    SYSDATE,
        FLD_TODAYCHECK, FLD_OVERRIDE_MANDATORY,FLD_OVERRIDE_FORMAT,FLD_OVERRIDE_RANGE,FLD_OVERRIDE_DATE,FLD_EXPLABEL,
        FLD_HIDELABEL,   FLD_HIDERESPLABEL,     FLD_DISPLAY_WIDTH,      FLD_RESPALIGN,FLD_NAME_FORMATTED,FLD_SORTORDER,fld_linkedform
        FROM ER_FLDLIB
        WHERE pk_field = p_org_field_lib_id ;
     IF SQL%FOUND THEN
         --we get the old systemid and the new system id from er_fldlib for the current field
        --and the org field, these are used for replacing the old systemids in xsl and javascript
        SELECT fld_systemid
        INTO v_fld_systemid_old
         FROM ER_FLDLIB
        WHERE pk_field = p_org_field_lib_id ;
        SELECT fld_systemid
        INTO v_fld_systemid_new
        FROM ER_FLDLIB
        WHERE pk_field = v_newfldlib_id ;
        --------------------------------Insert records for all validations ----------------------------------------
    BEGIN
    INSERT INTO ER_FLDVALIDATE
         (PK_FLDVALIDATE,FK_FLDLIB,FLDVALIDATE_OP1,FLDVALIDATE_VAL1,
        FLDVALIDATE_LOGOP1,FLDVALIDATE_OP2,
          FLDVALIDATE_VAL2,FLDVALIDATE_LOGOP2,
          FLDVALIDATE_JAVASCR,
          RECORD_TYPE,CREATOR,CREATED_ON, IP_ADD)
    SELECT v_newfldValidate, v_newfldlib_id, FLDVALIDATE_OP1,FLDVALIDATE_VAL1,
          FLDVALIDATE_LOGOP1,FLDVALIDATE_OP2,
          FLDVALIDATE_VAL2,FLDVALIDATE_LOGOP2,
         REPLACE( FLDVALIDATE_JAVASCR , v_fld_systemid_old ,v_fld_systemid_new ) ,
         'N',p_user_id, SYSDATE, p_ip_add
      FROM ER_FLDVALIDATE
      WHERE fk_fldlib = p_org_field_lib_id ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
        P('no validations');
    END ;
    /* by Sonia Sahni 05/27/04*************
       replace the  new field's system id and primary key value in form's field actions
     ***************************************************/
      -- get form id for the forms section
      SELECT fk_formlib
      INTO v_new_form
      FROM ER_FORMSEC WHERE pk_formsec =  p_formsec ;
      -- replace keywords in er_fldaction info
      UPDATE ER_FLDACTIONINFO SET INFO_VALUE = v_newfldlib_id
      WHERE FK_FLDACTION   IN (SELECT PK_FLDACTION   FROM ER_FLDACTION WHERE fk_form =  v_new_form ) AND
      INFO_VALUE = TO_CHAR(p_org_field_lib_id) AND
      INFO_TYPE  IN (SELECT KEYWORD FROM ER_KEYWORD WHERE KEYWORD_TYPE  = 'pkfield') ;
      UPDATE ER_FLDACTIONINFO SET INFO_VALUE = v_fld_systemid_new
      WHERE FK_FLDACTION   IN (SELECT PK_FLDACTION   FROM ER_FLDACTION WHERE fk_form =  v_new_form ) AND
      INFO_VALUE = v_fld_systemid_old AND
      INFO_TYPE  IN (SELECT KEYWORD FROM ER_KEYWORD WHERE KEYWORD_TYPE  = 'systemid') ;
     -- copy responses one by one so that we can replace response id type keywords in er_fldactioninfo
      FOR s IN ( SELECT  PK_FLDRESP, FLDRESP_SEQ,  FLDRESP_DISPVAL, FLDRESP_DATAVAL,
                  FLDRESP_SCORE, FLDRESP_ISDEFAULT,record_type
                  FROM ER_FLDRESP
                  WHERE fk_field = p_org_field_lib_id
                  AND NVL(record_type,'Z') <> 'D' )
      LOOP
          SELECT seq_er_fldresp.NEXTVAL
          INTO v_new_pkfldresp
          FROM dual;
          INSERT INTO ER_FLDRESP
          (  PK_FLDRESP, FK_FIELD, FLDRESP_SEQ,
              FLDRESP_DISPVAL,
             FLDRESP_DATAVAL,
             FLDRESP_SCORE,
             FLDRESP_ISDEFAULT ,
             RECORD_TYPE ,
             CREATOR ,
             IP_ADD ,
             CREATED_ON  )
         VALUES (v_new_pkfldresp,v_newfldlib_id,s.FLDRESP_SEQ,s.FLDRESP_DISPVAL,s.FLDRESP_DATAVAL,
         s.FLDRESP_SCORE,s.FLDRESP_ISDEFAULT,DECODE(NVL(s.record_type,'N'),'H','H','N'), p_user_id ,  p_ip_add,
              SYSDATE)   ;
      -- replace keywords in er_fldaction info
       UPDATE ER_FLDACTIONINFO SET INFO_VALUE = v_new_pkfldresp
       WHERE FK_FLDACTION   IN (SELECT PK_FLDACTION   FROM ER_FLDACTION WHERE fk_form =  v_new_form ) AND
       INFO_VALUE = TO_CHAR(s.PK_FLDRESP) AND
       INFO_TYPE  IN (SELECT KEYWORD FROM ER_KEYWORD WHERE KEYWORD_TYPE  = 'pkresp') ;
      END LOOP;
     -- replace olf fld if in er_fldaction record
      UPDATE ER_FLDACTION
      SET fk_field =  v_newfldlib_id
      WHERE fk_form = v_new_form AND fk_field = p_org_field_lib_id ;
    /* End of change  - Sonia Sahni 05/27/04********** */
         o_ret := v_newfldlib_id ;
          ELSE
           o_ret := -1;
        RETURN ;
        END IF ;
        --we enter a new record in er_formfld corresponding to this entry
        SELECT seq_er_formfld.NEXTVAL
        INTO v_newformfld_id
        FROM dual ;
        -- changed by sonia 05/28/04 one sql should be enough
        --get the pk_formfld corresponding to this field as there is one-to-one correspondence
        --between fk_field and pk_formfld
        --we get the formfld_xsl , formfld_javascr or the old er_formfld record
        SELECT pk_formfld, formfld_xsl , formfld_javascr
        INTO v_oldformfld_id, v_formfld_xsl_init , v_formfld_javascr_init
        FROM ER_FORMFLD
        WHERE fk_field = p_org_field_lib_id ;
        /*select formfld_xsl , formfld_javascr
        into  v_formfld_xsl_init , v_formfld_javascr_init
        from er_formfld
        where pk_formfld = v_oldformfld_id  ;*/
        v_formfld_javascr := REPLACE( v_formfld_javascr_init , v_fld_systemid_old ,v_fld_systemid_new ) ;
        v_formfld_xsl   := REPLACE ( v_formfld_xsl_init , v_fld_systemid_old , v_fld_systemid_new );
        o_newsys := v_fld_systemid_new; -- sonia sahni
    -- To set the browserflag of copied form to 0 when more than one form is copied and set browserflag to the corresponding
    -- browserflag values of the copied forms when more than one form is copied
    --changed by Sonika on Dec 09, 2003 to set the browserflag of data entry date to 1
     IF p_formcnt = 1 THEN
        INSERT INTO ER_FORMFLD (
        PK_FORMFLD,         FK_FORMSEC ,        FK_FIELD  , FORMFLD_SEQ ,
        FORMFLD_MANDATORY ,
        FORMFLD_BROWSERFLG,
        FORMFLD_XSL,FORMFLD_JAVASCR ,
          CREATOR ,           RECORD_TYPE  ,        CREATED_ON, IP_ADD )
        SELECT v_newformfld_id , p_formsec , v_newfldlib_id , FORMFLD_SEQ ,
        FORMFLD_MANDATORY,
        CASE WHEN v_fld_systemid_new='er_def_date_01' THEN 1 ELSE FORMFLD_BROWSERFLG END,
        v_formfld_xsl ,  v_formfld_javascr ,
        p_user_id ,  'N' , SYSDATE , p_ip_add
        FROM ER_FORMFLD
        WHERE pk_formfld = v_oldformfld_id;
    ELSE
        INSERT INTO ER_FORMFLD (
        PK_FORMFLD,         FK_FORMSEC ,        FK_FIELD  , FORMFLD_SEQ ,
        FORMFLD_MANDATORY ,
        FORMFLD_BROWSERFLG,
         FORMFLD_XSL,FORMFLD_JAVASCR ,
          CREATOR ,           RECORD_TYPE  ,        CREATED_ON, IP_ADD )
        SELECT v_newformfld_id , p_formsec , v_newfldlib_id , FORMFLD_SEQ ,
        FORMFLD_MANDATORY,
         CASE WHEN v_fld_systemid_new='er_def_date_01' THEN 1 ELSE 0 END ,
        v_formfld_xsl ,  v_formfld_javascr ,
        p_user_id ,  'N' , SYSDATE , p_ip_add
        FROM ER_FORMFLD
        WHERE pk_formfld = v_oldformfld_id;
     END IF;
     COMMIT ;
    END ;
    --end of SP_COPYFLD_FOR_FORM
    PROCEDURE SP_CLUBFORMXSL(p_formid NUMBER, o_html OUT CLOB)
 /****************************************************************************************************
   ** Procedure to club xsl, java script  for a form
   ** Author: Sonia Sahni 16th July 2003
   ** Input parameter: formid - PK of the form
   ** Output parameter: html
   ** Modified by Sonika Talwar on March 23, 04 to bring the lookup link close to the field if its on same line
   ** Modified by Sonia Sahni on March 30, 04 to use new form field form_custom_js
   ** Modified by Sonia Sahni on April 16, 04 to do error handling for lookup
   ** Modified by Sonika Talwar on April 19, 04 to handle expand field attribute
   ** Modified by Sonika Talwar on April 22, 04 to remove help icon addition incase of Tabular section
   **/
AS
   v_fldxsl VARCHAR2(4000);
   v_fldjs  VARCHAR2(4000);
   v_fld_valdjs VARCHAR2(4000);
   v_clubxsl CLOB;
   v_clubjs CLOB;
   v_formxsl CLOB;
   v_xslstarttags VARCHAR2(4000);
   v_xslmidtags VARCHAR2(4000);
   v_xslendtags VARCHAR2(4000);
   v_esigntags VARCHAR2(4000);
   v_firsttime_msg VARCHAR2(4000);
   v_everytime_msg VARCHAR2(4000);
   v_fldsameline NUMBER;
   v_fldseq NUMBER;
   v_section_seq NUMBER;
   v_secname VARCHAR2(4000);
   v_prevsecname VARCHAR2(4000);
   v_formname VARCHAR2(50);
   v_pksec NUMBER;
   v_prevsec NUMBER := 0;
   v_fldname VARCHAR2(2000);
   v_fldkeyword VARCHAR2(255);
   v_flduniqueid VARCHAR2(50);
   v_fldbrowserflag NUMBER;
   v_fldsystemid VARCHAR2(50);
   v_pk_field NUMBER;
   v_popmsg VARCHAR2(4000);
   v_fldtype CHAR(1);
   v_fld_datatype VARCHAR2(2);
   v_fld_defresp VARCHAR2(500);
   v_formstarttag VARCHAR2(500);
   v_formendtag VARCHAR2(100);
   v_xslns VARCHAR2(1000);
   v_colcount NUMBER;
   --salil
   v_fldlinesno NUMBER ;
   v_xslrefresh NUMBER := 1;
   v_count NUMBER := 1;
   v_sec_count NUMBER := 1;
   v_fldxml CLOB;
   out_fldxml CLOB; --------
   v_secrepno NUMBER := 0;
   v_secformat CHAR(1) ;
   v_prev_sec_format CHAR(1) ;
   v_repfldxsl VARCHAR2(4000);
   v_repfldjs  VARCHAR2(4000);
   v_repfldvalidatejs VARCHAR2(4000);
   v_rep_isvisible NUMBER;
   v_alert_msg VARCHAR2(4000);
   v_pkformfld NUMBER;
   v_pk_repformfld NUMBER;
   v_repfldsameline NUMBER;
   v_repfldname VARCHAR2(2000);
   v_repfldkeyword VARCHAR2(255);
   v_repflduniqueid VARCHAR2(50);
   v_repfldbrowserflag NUMBER;
   v_repfldsystemid VARCHAR2(50);
   v_rep_pk_field NUMBER;
   v_repfldtype CHAR(1);
   v_rep_fld_datatype VARCHAR2(2);
   v_rep_fld_defresp VARCHAR2(500);
   v_repcolcount NUMBER;
   --salil
   v_repfldlinesno NUMBER ;
   repout_fldxml CLOB; -----------
   v_prev_sec_repno NUMBER :=0 ;
   v_prevset NUMBER := 0;
   v_repfld_set NUMBER := 0;
   v_repfld_count NUMBER := 1;
   v_strlen NUMBER;
   v_pos_1 NUMBER := 0;
   v_pos_2 NUMBER := 0;
   v_pos_endtd NUMBER :=0;
   len NUMBER := 0;
   v_repfld_xsl_2 VARCHAR2(4000);
   v_repfld_xsl_3 VARCHAR2(4000);
   v_grid_title_xsl VARCHAR2(100);
   v_sec_grid_header CLOB;
   v_lookupjs VARCHAR2(1000);
   v_form_validatejs VARCHAR2(4000);
   v_lenxml NUMBER;
   v_counter NUMBER;
   v_tempstr VARCHAR2(4000);
   v_tempclubxsl CLOB;
   v_fld_esign_validate VARCHAR2(4000);
   v_fld_status_validate VARCHAR2(500);
   v_origsystemid VARCHAR2(50);
   v_reporigsystemid VARCHAR2(50);
   v_custom_js CLOB;
   --vishal
   v_fld_charsno NUMBER;
   v_rep_charsno NUMBER;
   v_isvisible NUMBER;
   v_fld_enablejs VARCHAR2(32000);
   v_form_activation_js CLOB;
   v_override_count NUMBER;
   v_override_variable VARCHAR2(250);
   v_override_js VARCHAR2(4000);
   v_fld_hidelabel NUMBER;
   v_fld_align VARCHAR2(16);
   v_sortorder VARCHAR(10);

   v_consent_status number;
   v_notconsent_status number;

   v_displayvalflag number;
   v_byPassValidationsJS varchar2(32000);


BEGIN
--check for xslrefresh column in table
    BEGIN
        SELECT NVL(form_xslrefresh,1), form_custom_js, form_activation_js
        INTO v_xslrefresh, v_custom_js, v_form_activation_js
        FROM ER_FORMLIB
        WHERE  pk_formlib = p_formid;
    EXCEPTION WHEN NO_DATA_FOUND THEN
         v_xslrefresh := 0;
    END ;

  IF v_xslrefresh = 1 THEN
  -- find out the count of override fields to check if there is any soft check created on this form. If created, we need to add a variable in generated
   -- form XML to show that soft check is defined on this form
   --VA 02/01/2005
      SELECT NVL(SUM(NVL(fld_override_mandatory,0) + NVL(fld_override_format,0)+ NVL(fld_override_range,0)+NVL(fld_override_date,0)),0)  INTO v_override_count  FROM ERV_FORMFLDS
   WHERE  pk_formlib = p_formid   AND NVL(fld_isvisible,0) <> 1;
    --add xsl namespace tags

   begin
       select pk_codelst
       into v_consent_status
       from er_codelst
       where codelst_type = 'fillformstat' and codelst_subtyp = 'consent';
   exception when no_data_found then
     v_consent_status := 0;
   end;

    begin
       select pk_codelst
       into v_notconsent_status
       from er_codelst
       where codelst_type = 'fillformstat' and codelst_subtyp = 'notconsent';
   exception when no_data_found then
     v_notconsent_status := 0;
   end;

   begin
       select f_to_number(CTRL_VALUE)
       into v_displayvalflag
       from er_ctrltab
       where ctrl_key = 'use_disp'  ;
   exception when no_data_found then
     v_displayvalflag := 1;
   end;


    v_xslns := '<?xml version="1.0" ?> <xsl:stylesheet version="1.0" ';
    v_xslns := v_xslns || 'xmlns:xsl="http://www.w3.org/1999/XSL/Transform" ';
    v_xslns := v_xslns || 'xmlns:xdb="http://xmlns.oracle.com/xdb" ';
    v_xslns := v_xslns || 'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"> ' || CHR(13);
    v_xslns := v_xslns || '[VELNUMFORMATDEF]' || CHR(13);
    v_xslns := v_xslns || '[VELFLOATFORMATDEF]' || CHR(13);
    v_xslns := v_xslns || '<xsl:param name="numFormat"/>' || CHR(13);
    v_xslns := v_xslns || '<xsl:param name="floatFormat"/>' || CHR(13);
    v_xslstarttags := ' <xsl:template match="/"> ';
    v_xslstarttags :=  v_xslstarttags  || '<HTML> ' || CHR(13) || '<HEAD> ' || CHR(13);
    v_xslmidtags := ' </HEAD> ' || CHR(13) || '<BODY id="forms">' || CHR(13) || '<xsl:apply-templates select="rowset"/> ';
    v_xslmidtags := v_xslmidtags || CHR(13) || '</BODY> ' || CHR(13) ||' </HTML> ' || CHR(13) || '</xsl:template> ' || CHR(13) || '<xsl:template match="rowset"> ';
    v_xslendtags := '</xsl:template> ' || CHR(13) ||' </xsl:stylesheet> ';
    v_formstarttag := '<Form name="er_fillform1"  id="fillform"  method="post" action="updateFormData.jsp"  onSubmit="return validationWithAjax()">';

    --add hidden div for specimen code
    v_formstarttag := v_formstarttag || '<span style="display:none;" id="spec_span"></span>';

    v_formendtag := '</Form>';
    v_esigntags := '<br/><table class="dynFormTable" width="100%"> ' || CHR(13);

    v_esigntags := v_esigntags || '<tr id = "consenting_radio_tr" style="display:none;"><td width="100%"> <table class="dynFormTable" width="250px" border="0"><tr><td width="100%"><input id = "id_vel_consenting" type="radio" name="vel_consenting" value="'|| v_consent_status ||
    '" /><b>I agree to participate</b></td></tr><tr><td width="100%"><input type="radio" CHECKED="true" id = "id_vel_consenting" name="vel_consenting" value="'|| v_notconsent_status
    ||'"/><b>I do not agree to participate</b></td></tr></table>  </td></tr>';

    v_esigntags := v_esigntags || '<tr><td width="15%" id="eSignLabel">e-Signature <FONT class="Mandatory">* </FONT> </td>'  || CHR(13);
    v_esigntags := v_esigntags || '<td width="30%"><input type="password" autocomplete="off" name="eSign" maxlength="8" id="eSign" onkeyup="ajaxvalidate(''misc:''+this.id,4,''eSignMessage'',''Valid e-Sign'',''Invalid e-Sign'',''sessUserId'')"/><span id="eSignMessage"></span> </td> '|| CHR(13);
    v_esigntags := v_esigntags || '<td width="15%">' || CHR(13);
    v_esigntags := v_esigntags || '<input type="image" src="../images/jpg/Submit.gif" align="absmiddle" border="0" id="submit_btn" /> '|| CHR(13);
    v_esigntags := v_esigntags || '</td></tr></table> ';
    v_override_variable:= '<input type="hidden" name="override_count"  id="override_count"  value="'|| v_override_count||'" /> '|| CHR(13);
    v_clubxsl :=  v_clubxsl  || '<table class="dynFormTable" width="100%" border="0"><tr>';
    -- salil
    FOR i IN (SELECT pk_formfld,form_name , pk_formsec, formsec_name,formsec_seq ,formfld_seq ,NVL(fld_sameline,0) fld_sameline,formfld_xsl ,
              NVL(formfld_javascr,' ') formfld_javascr,fld_name,fld_keyword,fld_uniqueid,fld_charsno,
              formfld_browserflg,fld_systemid, pk_field,fld_type,fld_datatype, fld_defresp, fld_colcount , fld_linesno , formsec_fmt,NVL(formsec_repno,0) formsec_repno,fld_isvisible,
              NVL(fld_hidelabel,0)  fld_hidelabel, NVL(fld_align,'left') fld_align,fld_sortorder
              FROM erv_formflds
              WHERE  pk_formlib = p_formid
              AND NVL(fld_isvisible,0) <> 1
              UNION
              SELECT NULL, NULL , pk_formsec, formsec_name,formsec_seq ,NULL ,NULL,NULL ,
              NULL,NULL,NULL,NULL,NULL,
              NULL,NULL, NULL,NULL,NULL, NULL, NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
                 FROM ER_FORMSEC
                WHERE fk_formlib = p_formid
                AND NVL(record_type,'Z') <> 'D'
              AND pk_formsec NOT IN (SELECT pk_formsec
              FROM erv_formflds WHERE pk_formlib = p_formid)
              ORDER BY formsec_seq,pk_formsec,formfld_seq
              )
    LOOP
       v_formname := i.form_name;
       v_pksec := i.pk_formsec;
       v_secname := i.formsec_name;
       v_section_seq := i.formsec_seq;
       v_fldseq := i.formfld_seq;
       v_fldsameline := i.fld_sameline;
       v_fldxsl := i.formfld_xsl;
       v_fldjs  := i.formfld_javascr;
        v_fldname := i.fld_name;
         v_fldkeyword := i.fld_keyword;
       v_flduniqueid := i.fld_uniqueid;
       v_fld_charsno := i.fld_charsno ;
       v_fldbrowserflag := i.formfld_browserflg;
       v_fldsystemid := i.fld_systemid;
       v_pk_field := i.pk_field;
       v_fldtype :=  i.fld_type;
       v_fld_datatype := i.fld_datatype;
       v_fld_defresp := i.fld_defresp;
       v_colcount := i.fld_colcount;
       v_fldlinesno := i.fld_linesno;
       v_secformat := i.formsec_fmt;
       v_secrepno :=  i.formsec_repno;
       v_pkformfld := i.pk_formfld;
       v_origsystemid := i.fld_systemid;
       v_isvisible := i.fld_isvisible;
       v_sortorder:=i.fld_sortorder;
       v_fld_hidelabel := i.fld_hidelabel;
       v_fld_align := i.fld_align;
       --the grey out multiple choice fields have been disbaled in field XSL, so they would not be available in the
       --update page. Thus before submit enable these fields
       --replace speacial characters
        --v_fldxsl := REPLACE(v_fldxsl,'&', '&amp;');
        --v_fldxsl := PKG_UTIL.f_escapeSpecialChars(v_fldxsl);
        v_secname := Pkg_Util.f_escapeSpecialCharsForXML(v_secname);
        --commented by sonia abrol, 03/11/05, i dont know why the disabled fields were getting enabled. we do not need to submit disabled fields anyways
       /*IF (v_fldtype='M' AND v_isvisible=2) THEN
          v_fld_enablejs := v_fld_enablejs || 'formobj.'||v_fldsystemid||'.disabled=false; ' || CHR(13);
       END IF;*/
       --javascript
        v_clubjs := v_clubjs || CHR(13) || v_fldjs || CHR(13) ;
        BEGIN
           --get javascript related to validation
            SELECT fldvalidate_javascr
            INTO v_fld_valdjs
            FROM ER_FLDVALIDATE
            WHERE fk_fldlib = v_pk_field;
            v_clubjs := v_clubjs || CHR(13) || v_fld_valdjs || CHR(13) ;
            EXCEPTION WHEN NO_DATA_FOUND THEN
              P('no validation');
        END;
         --cut label for grid
         IF v_secformat = 'T' THEN
         -------------------------------
         -- if condition to hide horizontal line for tabular section
             IF v_fldtype = 'H' THEN
                v_fldxsl := '&#xa0;';
             END IF;
             --modified by sonia abrol, 03/16/05 to use a function to cut the label
            SELECT  f_cut_fieldlabel(v_fldxsl,v_fldtype,v_fld_datatype,v_fld_align,v_pk_field)
            INTO v_fldxsl FROM dual;
            /*  IF v_fldtype <> 'C' AND v_fldtype <> 'H' AND NVL(v_fld_datatype,'Z') <> 'ML'  AND v_fldtype <> 'F' THEN
                --modified by sonika on Nov 21, 03 search only for <td width=''15%'' as align option has also been added
                --modified by sonika on April 19, 04 search only for <td
                   v_pos_1 := INSTR( v_fldxsl, '<td') ;
                   v_repfld_xsl_2 := SUBSTR(v_fldxsl, 0 ,v_pos_1-1);
                 --  p('v_repfld_xsl_2 ' ||v_repfld_xsl_2);
                    len := LENGTH(v_fldxsl);
                   v_pos_2 := INSTR(v_fldxsl , '</label>');
                   --v_strlen := length( '</label>  &#xa0;&#xa0;&#xa0;</td>' );
                      --find </td> end tag of label
                       v_pos_endtd := INSTR(v_fldxsl , '</td>',v_pos_2);
                           --# of characters between </label> and </td>
                        v_strlen := LENGTH('</td>');
                        v_strlen := (v_pos_endtd - v_pos_2) + v_strlen;
                      v_repfld_xsl_3 := SUBSTR( v_fldxsl , v_pos_2+v_strlen , len-1 ) ;
                  -- p('v_repfld_xsl_3 ' ||v_repfld_xsl_3);
                      v_fldxsl := v_repfld_xsl_2 || v_repfld_xsl_3 ;
                END IF ;
            */
                v_fldsameline := 1;
         END IF;
       IF v_count > 1 AND v_fldsameline = 0 AND v_fldtype <> 'H' THEN
          v_fldxsl := '</tr><tr>' || v_fldxsl ;
       ELSIF v_count > 1 AND v_fldtype = 'H' AND v_secformat <> 'T' THEN
          v_fldxsl := '</tr></table>' || v_fldxsl || '<table class="dynFormTable" width="100%"><tr>' ;
       END IF;
       IF v_prevsec <> v_pksec THEN -- if form section changes, create section tag
           -- for section repeat fields of the previous section
              IF v_prev_sec_repno  > 0 THEN
                  v_repfldbrowserflag := 0;
                  v_repfld_count := 1;
                  FOR x IN (SELECT repfrmfld.PK_REPFORMFLD, repfrmfld.FK_FORMFLD,repfrmfld.FK_FORMSEC,repfrmfld.FK_FIELD,repfrmfld.REPFORMFLD_SEQ,
                             repfrmfld.REPFORMFLD_XSL,repfrmfld.REPFORMFLD_JAVASCR,repfrmfld.REPFORMFLD_SET, NVL(rep.FLD_SAMELINE,0) FLD_SAMELINE,rep.FLD_NAME,
                             rep.FLD_KEYWORD, rep.FLD_UNIQUEID,rep.FLD_SYSTEMID,rep.FLD_TYPE,  rep.FLD_DATATYPE,
                             rep.FLD_DEFRESP ,rep.FLD_COLCOUNT,rep.FLD_LINESNO, orig.FLD_SYSTEMID orig_systemid,
                             rep.fld_charsno,rep.fld_isvisible,rep.FLD_SORTORDER
                             FROM ER_REPFORMFLD repfrmfld, ER_FLDLIB rep, ER_FORMFLD, ER_FLDLIB orig
                             WHERE repfrmfld.FK_FORMSEC = v_prevsec AND
                             rep.PK_FIELD = repfrmfld.FK_FIELD AND
                             NVL(rep.fld_isvisible,0) <> 1 AND
                             NVL(repfrmfld.record_type,'Z') <> 'D' AND
                             ER_FORMFLD.PK_FORMFLD = repfrmfld.fk_formfld AND
                             orig.PK_FIELD = ER_FORMFLD.fk_field
                             ORDER BY REPFORMFLD_SET,REPFORMFLD_SEQ
                             )
                  LOOP
                      v_pk_repformfld := x.PK_REPFORMFLD;
                     v_rep_pk_field := x.FK_FIELD;
                      v_repfldxsl := x.REPFORMFLD_XSL;
                     v_repfldjs  := x.REPFORMFLD_JAVASCR;
                     v_repfldsameline  :=x.FLD_SAMELINE;
                     v_repfldname := x.FLD_NAME;
                     v_repfldkeyword := x.FLD_KEYWORD;
                     v_repflduniqueid :=  x.FLD_UNIQUEID ;
                     v_repfldsystemid := x.FLD_SYSTEMID;
                     v_repfldtype := x.FLD_TYPE ;
                     v_rep_fld_datatype := x.FLD_DATATYPE ;
                     v_rep_fld_defresp := x.FLD_DEFRESP ;
                     v_repcolcount := x.FLD_COLCOUNT;
                     v_repfld_set := x.REPFORMFLD_SET;
                     --salil
                     v_repfldlinesno := x.FLD_LINESNO ;
                     v_reporigsystemid := x.orig_systemid;
                     v_rep_charsno  :=    x.fld_charsno;
                     v_rep_isvisible := x.fld_isvisible;
                     v_sortorder:=x.fld_sortorder;
                        --the grey out multiple choice fields have been disbaled in field XSL, so they would not be available in the
                         --update page. Thus before submit enable these fields
                        IF (v_repfldtype='M' AND v_rep_isvisible=2) THEN
                           v_fld_enablejs := v_fld_enablejs || 'formobj.'||v_repfldsystemid||'.disabled=false; ' ||CHR(13);
                     END IF;
                          IF v_repfldtype = 'H' THEN
                        v_repfldxsl := '<td colspan="2">' || v_repfldxsl || '</td>';
                     END IF;
                     -- add xsl
                    IF v_prev_sec_format = 'T' THEN
                    -- if condition to hide horizontal line for tabular section
                     IF v_repfldtype = 'H' THEN
                        v_repfldxsl := '&#xa0;';
                     END IF;
                       IF v_prevset <> v_repfld_set AND v_repfld_count = 1 THEN
                         v_repfldsameline := 0;
                       ELSIF v_repfld_count > 1 AND v_prevset = v_repfld_set THEN
                         v_repfldsameline := 1;
                       ELSE
                          v_repfldsameline := 0;
                       END IF;
                    END IF;
                    --v_repfldxsl := REPLACE(v_repfldxsl,'&', '&amp;');
                    --v_repfldxsl := PKG_UTIL.f_escapeSpecialChars(v_repfldxsl);
                    IF  v_repfldsameline = 0  THEN
                    IF v_prev_sec_format = 'T' THEN -- sonia 16th june for tabular alignment
                        v_clubxsl := v_clubxsl || '</tr><tr>' || v_repfldxsl ; -- sonia 16th june for tabular alignment
                     ELSE -- sonia 16th june for tabular alignment
                       --changed by Sonika on April 19, 04
                       v_clubxsl := v_clubxsl || '</tr> </table> <table class="dynFormTable" width="100%" border="0"> <tr>' || v_repfldxsl ;
                       END IF; -- sonia 16th june for tabular alignment
                    ELSE
                          --added by Sonika on March 23, 04
                        --in case of lookup if the user sets same line as true for the lookup field
                         --then the lookup link should come close to the field, it should not be in separate td
                          --so remove </td> from the previous field xsl and <td> from the lookup field xsl
                          IF NVL(v_rep_fld_datatype,'Z')='ML' THEN
                            v_clubxsl := SUBSTR(v_clubxsl,1,INSTR(v_clubxsl,'</td>',-1,1)-1);
                             v_repfldxsl := SUBSTR(v_repfldxsl,INSTR(v_repfldxsl,'<td>',1,1)+4,LENGTH(v_repfldxsl));
                           END IF;
                       v_clubxsl := v_clubxsl || v_repfldxsl ;
                    END IF;
                    v_clubjs := v_clubjs || CHR(13) || v_repfldjs || CHR(13) ;
                    BEGIN
                    --get javascript related to validation
                       SELECT repfldvalidate_javascr
                       INTO v_repfldvalidatejs
                       FROM ER_REPFLDVALIDATE
                       WHERE fk_repformfld = v_pk_repformfld;
                        v_clubjs := v_clubjs || CHR(13) || v_repfldvalidatejs || CHR(13) ;
                       EXCEPTION WHEN NO_DATA_FOUND THEN
                              P('no validation');
                   END;
                    IF v_repfldtype = 'E' OR v_repfldtype = 'M' THEN
                         --salil
                         SP_GETFIELDXML(v_rep_pk_field, v_repfldname, v_repfldkeyword,v_repflduniqueid,v_repfldbrowserflag,
                                      v_repfldsystemid, v_repfldtype, v_rep_fld_datatype, v_rep_fld_defresp,
                                      v_repcolcount,v_repfldlinesno,v_reporigsystemid,v_prevsec,v_prevsecname ,v_rep_charsno,v_sortorder,repout_fldxml);
                         v_fldxml := v_fldxml || repout_fldxml;
                   END IF;
                   v_prevset := v_repfld_set ;
                   v_repfld_count := v_repfld_count + 1;
                  END LOOP ;
              END IF;
          v_sec_count := 0;
         IF v_secformat = 'T' THEN
              --v_fldxsl := REPLACE(v_fldxsl,'&', '&amp;');
              SP_GET_SECTION_HEADER (p_formid, v_pksec , v_sec_grid_header);
              --sonia 21st June, changed border=0 for tabular, reverted back the grid change for tabular section
              --replace special characters
              --v_sec_grid_header  :=  PKG_UTIL.f_escapeSpecialChars(v_sec_grid_header);
              v_clubxsl := v_clubxsl || '</tr></table><P class="dynSection" > ' || v_secname || '</P><table class="dynFormTable" width="100%" border="0"><tr>' || v_sec_grid_header || '</tr><tr>' || v_fldxsl ;
         ELSE
              v_clubxsl := v_clubxsl || '</tr></table><P class="dynSection" > ' || v_secname || '</P><table class="dynFormTable" width="100%" border="0"><tr>' || v_fldxsl ;
         END IF;
       ELSE
          v_sec_count := v_sec_count + 1;
        --replace special characters
         -- v_fldxsl := REPLACE(v_fldxsl,'&', '&amp;');
          IF  v_fldsameline = 0  THEN
              v_clubxsl := v_clubxsl || '</tr> </table> <table class="dynFormTable" width="100%" border="0"> <tr>' || v_fldxsl ;
          ELSE
             --added by Sonika on March 23, 04
             --in case of lookup if the user sets same line as true for the lookup field
             --then the lookup link should come close to the field, it should not be in separate td
             --so remove </td> from the previous field xsl and <td> from the lookup field xsl
              IF NVL(v_fld_datatype,'Z')='ML' THEN
                v_clubxsl := SUBSTR(v_clubxsl,1,INSTR(v_clubxsl,'</td>',-1,1)-1);
                 v_fldxsl := SUBSTR(v_fldxsl,INSTR(v_fldxsl,'<td>',1,1)+4,LENGTH(v_fldxsl));
             END IF;
             v_clubxsl := v_clubxsl || v_fldxsl ;
          END IF;
       END IF;
     IF v_fldtype = 'E' OR v_fldtype = 'M' THEN
          --salil
           SP_GETFIELDXML(v_pk_field, v_fldname, v_fldkeyword,v_flduniqueid,v_fldbrowserflag,v_fldsystemid,
           v_fldtype, v_fld_datatype, v_fld_defresp, v_colcount,v_fldlinesno,v_origsystemid,v_pksec,v_secname,v_fld_charsno,v_sortorder,out_fldxml)  ;
           v_fldxml := v_fldxml || out_fldxml;
      END IF;
        v_prevsec := v_pksec;
        v_prevsecname := v_secname;
        v_prev_sec_repno := v_secrepno;
        v_prev_sec_format := v_secformat;
        v_count := v_count + 1;
    END LOOP ;
    -------------------------------------------------------------------
          IF v_prev_sec_repno > 0 AND v_prevsec > 0 THEN -- for last section
                  v_repfld_count := 1;
                  v_repfldbrowserflag := 0;
                  v_prevset := 0;
                  FOR y IN (SELECT repfrmfld.PK_REPFORMFLD, repfrmfld.FK_FORMFLD,repfrmfld.FK_FORMSEC,repfrmfld.FK_FIELD,repfrmfld.REPFORMFLD_SEQ,
                             repfrmfld.REPFORMFLD_XSL,repfrmfld.REPFORMFLD_JAVASCR,repfrmfld.REPFORMFLD_SET, NVL(rep.FLD_SAMELINE,0) FLD_SAMELINE,rep.FLD_NAME,
                             rep.FLD_KEYWORD, rep.FLD_UNIQUEID,rep.FLD_SYSTEMID,rep.FLD_TYPE,  rep.FLD_DATATYPE,
                             rep.FLD_DEFRESP ,rep.FLD_COLCOUNT,rep.FLD_LINESNO, orig.FLD_SYSTEMID orig_systemid,
                             rep.fld_charsno,rep.fld_isvisible,rep.fld_sortorder
                             FROM ER_REPFORMFLD repfrmfld, ER_FLDLIB rep, ER_FORMFLD, ER_FLDLIB orig
                             WHERE repfrmfld.FK_FORMSEC = v_prevsec AND
                             NVL(rep.fld_isvisible,0) <> 1 AND
                             rep.PK_FIELD = repfrmfld.FK_FIELD AND
                             NVL(repfrmfld.record_type,'Z') <> 'D' AND
                             ER_FORMFLD.PK_FORMFLD = repfrmfld.fk_formfld AND
                             orig.PK_FIELD = ER_FORMFLD.fk_field
                             ORDER BY REPFORMFLD_SET,REPFORMFLD_SEQ
                             )
                  LOOP
                      v_pk_repformfld := y.PK_REPFORMFLD;
                     v_rep_pk_field := y.FK_FIELD;
                      v_repfldxsl := y.REPFORMFLD_XSL;
                     v_repfldjs  := y.REPFORMFLD_JAVASCR;
                     v_repfldsameline  := y.FLD_SAMELINE;
                     v_repfldname := y.FLD_NAME;
                     v_repfldkeyword := y.FLD_KEYWORD;
                     v_repflduniqueid :=  y.FLD_UNIQUEID ;
                     v_repfldsystemid := y.FLD_SYSTEMID;
                     v_repfldtype := y.FLD_TYPE ;
                     v_rep_fld_datatype := y.FLD_DATATYPE ;
                     v_rep_fld_defresp := y.FLD_DEFRESP ;
                     v_repcolcount := y.FLD_COLCOUNT;
                     v_repfldlinesno := y.FLD_LINESNO ; --salil
                     v_repfld_set := y.REPFORMFLD_SET;
                     v_reporigsystemid := y.orig_systemid;
                     v_rep_charsno := y.fld_charsno;
                     v_rep_isvisible := y.fld_isvisible;
                     v_sortorder:=y.fld_sortorder;
                        --the grey out multiple choice fields have been disbaled in field XSL, so they would not be available in the
                         --update page. Thus before submit enable these fields
                        IF (v_repfldtype='M' AND v_rep_isvisible=2) THEN
                           v_fld_enablejs := v_fld_enablejs || 'formobj.'||v_repfldsystemid||'.disabled=false; '||CHR(13);
                     END IF;
                         IF v_repfldtype = 'H' THEN
                        v_repfldxsl := '<td colspan="2">' || v_repfldxsl || '</td>';
                     END IF;
                     -- add xsl
                    IF v_prev_sec_format = 'T' THEN
                     -- if condition to hide horizontal line for tabular section
                     IF v_repfldtype = 'H' THEN
                        v_repfldxsl := '&#xa0;';
                     END IF;
                     IF v_isvisible = '1' THEN
                        v_repfldxsl := '&#xa0;';
                     END IF;
                       IF v_prevset <> v_repfld_set AND v_repfld_count = 1 THEN
                         v_repfldsameline := 0;
                       ELSIF v_repfld_count > 1 AND v_prevset = v_repfld_set THEN
                         v_repfldsameline := 1;
                       ELSE
                          v_repfldsameline := 0;
                       END IF;
                    END IF;
                     --v_repfldxsl := REPLACE(v_repfldxsl,'&', '&amp;');
                     --v_repfldxsl  :=  PKG_UTIL.f_escapeSpecialChars(v_repfldxsl );
                    IF  v_repfldsameline = 0  THEN
                        IF v_prev_sec_format = 'T' THEN -- sonia 16th june for tabular alignment
                          v_clubxsl := v_clubxsl || '</tr><tr>' || v_repfldxsl ;-- sonia 16th june for tabular alignment
                        ELSE -- sonia 16th june for tabular alignment
                       --changed by Sonika, removed table
                      v_clubxsl := v_clubxsl || '</tr> </table> <table class="dynFormTable" width="100%"> <tr>' || v_repfldxsl ;
                       END IF;-- sonia 16th june for tabular alignment
                    ELSE
                        --added by Sonika on March 23, 04
                        --in case of lookup if the user sets same line as true for the lookup field
                         --then the lookup link should come close to the field, it should not be in separate td
                          --so remove </td> from the previous field xsl and <td> from the lookup field xsl
                          IF NVL(v_rep_fld_datatype,'Z')='ML' THEN
                            v_clubxsl := SUBSTR(v_clubxsl,1,INSTR(v_clubxsl,'</td>',-1,1)-1);
                             v_repfldxsl := SUBSTR(v_repfldxsl,INSTR(v_repfldxsl,'<td>',1,1)+4,LENGTH(v_repfldxsl));
                           END IF;
                       v_clubxsl := v_clubxsl || v_repfldxsl ;
                    END IF;
                    v_clubjs := v_clubjs || CHR(13) || v_repfldjs || CHR(13) ;
                    BEGIN
                       --get javascript related to validation
                         SELECT repfldvalidate_javascr
                         INTO v_repfldvalidatejs
                         FROM ER_REPFLDVALIDATE
                         WHERE fk_repformfld = v_pk_repformfld;
                         v_clubjs := v_clubjs || CHR(13) || v_repfldvalidatejs || CHR(13) ;
                         EXCEPTION WHEN NO_DATA_FOUND THEN
                             P('no validation');
                    END;
                    IF v_repfldtype = 'E' OR v_repfldtype = 'M' THEN
                     SP_GETFIELDXML(v_rep_pk_field, v_repfldname, v_repfldkeyword,v_repflduniqueid,v_repfldbrowserflag,
                                  v_repfldsystemid, v_repfldtype, v_rep_fld_datatype, v_rep_fld_defresp,
                                  v_repcolcount,v_repfldlinesno,v_reporigsystemid,v_prevsec,v_prevsecname,v_rep_charsno,v_sortorder,repout_fldxml);
                     v_fldxml := v_fldxml || repout_fldxml;
                   END IF;
                   v_prevset := v_repfld_set ;
                   v_repfld_count := v_repfld_count + 1;
                  END LOOP ;
              END IF;
    --------------------------------------------------------------------
    --get First time pop-up messages and Every time pop-up messages
    SP_POPMESSAGE(p_formid,v_firsttime_msg,v_everytime_msg);
    IF (LENGTH(v_firsttime_msg) <> 0) THEN
         v_alert_msg := ' if (formobj.mode.value=="N") { alert("' || Pkg_Util.f_escapeSpecialChars(v_firsttime_msg) || '"); }';
    END IF;
    IF (LENGTH(v_everytime_msg) <> 0) THEN
         v_alert_msg := v_alert_msg || ' alert("' || Pkg_Util.f_escapeSpecialChars(v_everytime_msg) || '");';
    END IF;
     v_lookupjs := CHR(13) || ' function openlookup(p_viewid, p_filter,p_kword,p_lkpType) { '  || CHR(13) ||
                   'if (p_lkpType=="L"){ '|| CHR(13) ||
                   ' windowName = window.open("multilookup.jsp?maxselect=1&amp;viewId="+ p_viewid + "&amp;form=er_fillform1&amp;dfilter="+ p_filter + "&amp;keyword="+p_kword, "LookUp","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=550 top=100,left=100"); ' || CHR(13) ||
                   '}else if (p_lkpType=="A") {' || CHR(13) ||
                   ' windowName = window.open("dynreplookup.jsp?repId="+ p_viewid + "&amp;form=er_fillform1&amp;dfilter="+ p_filter + "&amp;keyword="+p_kword, "LookUp","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=550 top=100,left=100"); ' || CHR(13) ||
                   '}'  || CHR(13) ||
                    ' else { '|| CHR(13) ||
                   ' windowName = window.open("multilookup.jsp?maxselect=1&amp;viewId="+ p_viewid + "&amp;form=er_fillform1&amp;dfilter="+ p_filter + "&amp;keyword="+p_kword, "LookUp","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=550 top=100,left=100"); ' || CHR(13) ||
                   '} ' || CHR(13) ||
                  ' windowName.focus();' || CHR(13) ||
                  '}' || CHR(13)  ;


     v_fld_esign_validate  :=   ' if (  ! (validateFormESign(document.er_fillform1)) ) { setValidateFlag(''false''); return false;  } ' ;




    v_fld_status_validate :=  ' if (  ! (validateFormStatus(document.er_fillform1)) ) { setValidateFlag(''false''); return false;  } ';

    v_byPassValidationsJS  :=  CHR(13) || v_fld_esign_validate||  CHR(13)|| v_fld_status_validate ||  CHR(13) || ' if (byPassValidationsForIncomplete(document.er_fillform1) == true)     {   setValidateFlag(''true'');      return true;     }; ' || CHR(13);

    v_form_validatejs:='function validationWithAjax() { ';

    v_form_validatejs:= v_form_validatejs || v_byPassValidationsJS  ||   ' if (validate(document.er_fillform1)==false) {setValidateFlag(''false'');return false;} else {setValidateFlag(''true''); return true;}}';


        v_override_js:= '   if (parseInt(formobj.override_count.value) >0) {'||CHR(13)

                                      || 'formobj.action=&quot;querymodal.jsp&quot;;'||CHR(13)
                                      ||' formobj.target="formwin";'||CHR(13)
                                      ||'modalWin = window.open("donotdelete.html","formwin","resizable=1,status=0, width=850,height=550 top=100,left=100,menubar=no,scrollbars=1");'||CHR(13)
                                      ||'if (modalWin  &amp;&amp;  !modalWin.closed) modalWin.focus();'||CHR(13)
                                      ||'formobj.submit();'||CHR(13)
                                      ||'void(0);}'        ;
    -- create javascript tag
     --v_clubjs := '<![CDATA[<script  language="javascript"> ' || chr(13) ||'  function  validate(formobj) { alert("' || v_everytime_msg || '"); ' || chr(13)  || v_clubjs || ' }  </script>]]> ';
    -- replace spl chars in custom js
     v_custom_js := REPLACE(v_custom_js,'&', '&amp;');
     v_custom_js := REPLACE(v_custom_js,'"','&quot;');
     v_custom_js := REPLACE(v_custom_js,'''','&apos;');
      v_custom_js := REPLACE(v_custom_js,'<', '&lt;');
     v_custom_js := REPLACE(v_custom_js,'>', '&gt;');
--   v_custom_js := REPLACE(v_custom_js,'\', '\\');
    --- v_custom_js := PKG_UTIL.f_escapeSpecialChars(v_custom_js);
     /************Sonia - for field Actions, 05/11/04*********************************/
     -- create function for custom js
      v_custom_js := 'function performActionWrapper(formobj, currentfld){ ripLocaleFromAll(); performAction(formobj, currentfld); applyLocaleToAll();}' || CHR(13) ||' function performAction(formobj, currentfld)' ||
                    CHR(13) || '{  v_displayvalflag = ' || v_displayvalflag || ';' ||
                    v_custom_js || CHR(13) ||
                    '}'  ;
     /************Sonia - for field Actions, 05/11/04*********************************/
     -- include v_custom_js
    --include v_fld_enablejs
--  v_alert_msg :=  Pkg_Util.f_escapeSpecialChars(v_alert_msg);
    v_clubjs := '<script  language="javascript"> ' || CHR(13) || ' var formWin = null;'||CHR(13) ||'function validationWrapper(){ripLocaleFromAll(); if( validationWithAjax()==false){applyLocaleToAll(); return false;}else{return true;} }' ||CHR(13) || 'function  validate(formobj) { '||  CHR(13) ||  v_clubjs || v_fld_esign_validate|| CHR(13) ||  CHR(13)|| v_fld_status_validate || v_alert_msg || CHR(13) || v_fld_enablejs || CHR(13) ||v_override_js|| ' } ' || v_lookupjs  || CHR(13) ||  v_custom_js || CHR(13) ||v_form_validatejs||CHR(13)||'  </script>  ';
    v_clubxsl := v_clubxsl || '</tr></table>';
--   p('v_clubxsl ' || v_clubxsl);
     --combine the xsl namespace tags, xsl start tags, javascript,form tags ,club xsl, xsl end tags
     --to make the entire form xsl
    /******** Sonia 05/18/04 for form inter field actions ********/
     -- added v_form_activation_js   before v_esigntags;
    /******** end of change Sonia 05/18/04 for form inter field actions ********/
     -- process v_form_activation to replace formobj. with document.er_fillform1.
     /* Sonia Abrol, 03/16/05, to put the activation script in a function and then call that function. this function may  be called again by <body>  onFocus*/
     v_form_activation_js := REPLACE(v_form_activation_js,'formobj.','document.er_fillform1.');
      v_form_activation_js := REPLACE(v_form_activation_js,'<script>','{');
      v_form_activation_js := REPLACE(v_form_activation_js,'</script>','}');
      v_form_activation_js := ' <script> function callActionScriptWrapper(){ ripLocaleFromAll(); callActionScript(); applyLocaleToAll();}' || CHR(13) || 'function callActionScript() { v_displayvalflag = ' || v_displayvalflag || ';' ||
            CHR(13) || v_form_activation_js   || CHR(13) ||  '  } '  || CHR(13) ||  ' callActionScript();' || f_getformaction(p_formid) ||'</script>';
--    v_form_activation_js:=REPLACE(v_form_activation_js,'&#039;','\''');
--    v_clubjs:=REPLACE(v_clubjs,'&#039;','\''');
     v_formxsl := v_xslns || v_xslstarttags ||  v_clubjs ||v_xslmidtags ||  v_formstarttag || v_clubxsl || v_form_activation_js || v_esigntags||v_override_variable || v_formendtag || v_xslendtags ;
     --v_formxsl := v_xslns || v_xslstarttags || v_xslmidtags ||  v_formstarttag || v_clubxsl || v_esigntags || v_formendtag || v_xslendtags ;
     v_fldxml := '<rowset>' || v_fldxml || '</rowset>';
--p('v_fldxml' || v_fldxml);
     --replace special characters
      /*v_lenxml := length(v_fldxml);
      v_counter := 1;
      while v_counter <= v_lenxml
      loop
         v_tempstr := dbms_lob.substr(v_fldxml,4000,v_counter);
         v_tempstr := replace(v_tempstr,'"','&quot;');
         v_tempstr := replace(v_tempstr,'''','&apos;');
         v_tempstr := replace(v_tempstr,'&', '&amp;'); --replace an ampersand
         --v_tempstr := replace(v_tempstr,'<', '&lt;');
         --v_tempstr := replace(v_tempstr,'>', '&gt;');
         --v_tempstr := replace(v_tempstr,'\', '\\');
         v_tempclubxsl := v_tempclubxsl || v_tempstr;
        v_counter := v_counter + 4000-1;
      end loop;
     v_fldxml := v_tempclubxsl ;*/
 COMMIT;
     UPDATE ER_FORMLIB
     SET form_xslrefresh = 0,
     form_xml = XMLTYPE(v_fldxml), form_xsl = v_formxsl
     --FORM_VIEWXSL = v_fldxml, form_xsl = v_formxsl
     WHERE  pk_formlib = p_formid;
     COMMIT;
   END IF; -- if for v_xslrefresh = 1
    --get the form html

    -- commented by Sonia abrol, HTML will not be returned from database, we will process xml/xsl in java
    --SP_GET_FORM_HTML(p_formid, o_html);

END; -- end of SP_CLUBFORMXSL
------------------------------------------------------------------------------------------
PROCEDURE SP_GET_SECTION_HEADER (p_formid NUMBER, p_section NUMBER, o_xsl OUT CLOB)
AS
 /****************************************************************************************************
   ** Procedure to get XSl for tabular type section's header
   ** Author: Sonia Sahni 27 Aug 2003
   ** Modified by Sonika on Dec 15, 03 for not generating the header for horizontal line
   ** Modified by Anu on june 03, 04 to show help icon with the label of the field.
   ** Input parameter: formid - PK of the form
   ** Input parameter: section id  - PK of the section
   ** Output parameter: headerXSL
   ** Modified by Sonia Abrol, 03/14/05, to use fld_name directly for the header.
   **/
   v_fldxsl VARCHAR2(4000);
   v_fld_esign_validate VARCHAR2(4000);
   v_sec_grid_label_beg_pos NUMBER;
   v_sec_grid_label_end_pos  NUMBER;
   v_sec_grid_header_label VARCHAR2(1000);
   v_help_beg_pos NUMBER;
   v_help_end_pos NUMBER;
   v_help_icon VARCHAR2(1000);
   v_sec_grid_header CLOB;
   v_fld_type CHAR(1) ;
   v_fld_datatype VARCHAR2(3);
   v_fldname VARCHAR2(4000);
BEGIN
    FOR i IN (SELECT  formfld_seq , formfld_xsl , fld_type , fld_datatype, fld_systemid,fld_name
          FROM ER_FORMFLD, ER_FLDLIB
          WHERE  fk_formsec = p_section AND
          pk_field = fk_field AND
          NVL(fld_isvisible,0) <> 1 AND
          ER_FORMFLD.record_type <> 'D' AND fld_type <> 'H' AND NVL(fld_datatype,'Z') <> 'ML'
          ORDER BY formfld_seq  )
    LOOP
       v_fldxsl := i.formfld_xsl;
       v_fld_type := i.fld_type;
       v_fld_datatype := i.fld_datatype;
       v_fldname := i.fld_name;
       IF v_fld_type = 'S' THEN
       v_sec_grid_header_label := '<td>Space Break</td>';
       ELSIF v_fld_type = 'F' THEN
         v_sec_grid_header_label := '<td>Form Link</td>';
       ELSIF v_fld_datatype = 'ML' THEN
           v_sec_grid_header_label := '<td>&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;</td>';
       --added by Sonika as per issue # 984, they want Comments as header in case of Comments type field
       ELSIF v_fld_type = 'C' THEN
           v_sec_grid_header_label := '<td>Comments</td>';
       ELSE
           v_sec_grid_label_beg_pos := INSTR(v_fldxsl, '<label');
           v_sec_grid_label_end_pos  := INSTR(v_fldxsl, '</label>');
           v_sec_grid_label_end_pos := v_sec_grid_label_end_pos -  v_sec_grid_label_beg_pos + 8;
           v_help_beg_pos := INSTR(v_fldxsl, '<img src="../images/jpg/help.jpg"');
           IF(v_help_beg_pos > 0 ) THEN
                v_help_end_pos := INSTR(v_fldxsl ,'</img>');
                v_help_end_pos := v_help_end_pos - v_help_beg_pos + 6;
                v_help_icon := SUBSTR(v_fldxsl,v_help_beg_pos,v_help_end_pos );
                v_sec_grid_header_label := '<td>' || SUBSTR(v_fldxsl,v_sec_grid_label_beg_pos,v_sec_grid_label_end_pos )
                                                  || '</td>';
           ELSE
           v_sec_grid_header_label := '<td>' || SUBSTR(v_fldxsl,v_sec_grid_label_beg_pos,v_sec_grid_label_end_pos ) || '</td>';
           END IF;
        --v_sec_grid_header_label := '<td>' || v_fldname || '</td>';
        END IF;
       v_sec_grid_header := v_sec_grid_header || v_sec_grid_header_label  ;
    END LOOP;
    --p(substr(v_sec_grid_header,1,1000));
    o_xsl := v_sec_grid_header;
    --o_xsl :='<td>hello</td>';
END; -- end of SP_GET_SECTION_HEADER
-------------------------------------------------------------------------------------------------------------------
PROCEDURE SP_POPMESSAGE (p_formlib_id NUMBER, o_popmsg_f OUT VARCHAR , o_popmsg_e OUT VARCHAR )
AS
/***************************************************************************************************************
   ** Procedure to combine all pop messages from ER_FORMNOTIFY
   ** Author: Sonia Kaura 8th September 2003
   ** Input parameter:pk of the ER_FORMLIB
   ** Output parameter: All the clubbed pop up messages to come up the FIRST_TIME
   ** Output parameter: All the clubbed pop up messages to come up EVERY_TIME
   **/
 v_popup_msgf VARCHAR(4000) ;
 v_popup_msge VARCHAR(4000) ;
    BEGIN
    v_popup_msgf := NULL ;
    v_popup_msge := NULL ;
    FOR i IN  ( SELECT fn_msgtext , fn_sendtype  FROM ER_FORMNOTIFY WHERE  fk_formlib = p_formlib_id
                AND  fn_msgtype = 'P' AND record_type <> 'D' )
    LOOP
    IF  v_popup_msgf IS NOT NULL  AND i.fn_sendtype = 'F'    THEN
    v_popup_msgf :=  v_popup_msgf || '.' || i.fn_msgtext || '.' ;
    ELSE
        IF i.fn_sendtype = 'F' THEN
          v_popup_msgf := i.fn_msgtext;
       END IF ;
    END IF ;
    IF  v_popup_msge  IS NOT NULL  AND i.fn_sendtype = 'E'  THEN
    v_popup_msge :=  v_popup_msge || '.' || i.fn_msgtext || '.' ;
    ELSE
         IF i.fn_sendtype = 'E' THEN
           v_popup_msge := i.fn_msgtext;
        END IF ;
    END IF ;
    END LOOP ;
    o_popmsg_f := v_popup_msgf ;
    o_popmsg_e := v_popup_msge ;
    END ; --end of SP_POPMESSAGE
    ---------------------------
PROCEDURE SP_DEF_SEC_DELETE (p_pk_formsec NUMBER,p_fk_formlib NUMBER,p_user VARCHAR2,p_ipadd VARCHAR2,o_ret OUT NUMBER)
AS
/***************************************************************************************************************
   ** Procedure to delete all the fields from a section except for the default date field when the user
   ** wants to delete the section
   ** Author: Sonia Kaura 7th October 2003
   ** Input parameter:pk of the ER_FORMSEC
   ** Input parameter:pk of the ER_FORMLIB
   ** Input parameter:id of the user
   ** Input parameter:ipadd
   ** Output parameter: if the procedur is successful
   **/
    BEGIN
            UPDATE ER_FORMFLD
          SET record_type = 'D'
          WHERE fk_formsec = p_pk_formsec AND pk_formfld NOT IN ( SELECT pk_formfld FROM ER_FORMFLD , ER_FLDLIB
                                                                WHERE fk_formsec = p_pk_formsec
                                                      AND pk_field = fk_field
                                                      AND  fld_uniqueid  = 'er_def_date_01'   ) ;
           UPDATE ER_FORMSEC
         SET
         RECORD_TYPE = 'M' , LAST_MODIFIED_BY = p_user , IP_ADD = p_ipadd  ;
           UPDATE ER_FORMLIB
           SET FORM_XSLREFRESH  = 1
           WHERE pk_formlib = p_fk_formlib ;
         o_ret := 1 ;
         COMMIT ;
    END ;
    --END OF SP_DEF_SEC_DELETE procedure
--------------------------------------------------------------
   PROCEDURE testxml
   AS
      v_clob CLOB;
      val VARCHAR2(50);
   BEGIN
        v_clob := '<row><A>1</A><B>2</B></row>';
        SP_SEARCH_IN_XML (v_clob, 'A', val );
        P('A' || val);
        SP_SEARCH_IN_XML (v_clob, 'B', val );
        P('B' || val);
   END;
    PROCEDURE SP_SEARCH_IN_XML (p_xml CLOB, p_searchparam VARCHAR2, o_value OUT STRING )
    AS
   /****************************************************************************************************
   ** ** Author: Sonia Sahni 21 st OCT 2003
   **
   ********************************************************************************************************/
    v_cnt NUMBER;
    i NUMBER := 1;
    v_param VARCHAR2(50);
    v_paramvalues VARCHAR2(4000);
    v_blankxml    CLOB;
    v_savexml CLOB;
    v_doc       dbms_xmldom.DOMDocument;
    ndoc      dbms_xmldom.DOMNode;
    buf       VARCHAR2(2000);
    nodelist  dbms_xmldom.DOMNodelist;
    docelem   dbms_xmldom.DOMElement;
    node      dbms_xmldom.DOMNode;
    childnode dbms_xmldom.DOMNode;
    nodelistSize NUMBER := 0;
    nodelistSizeCount NUMBER := 0;
     myParser    dbms_xmlparser.Parser;
     --for xml save
    --ctx dbms_xmlsave.ctxType ;
    BEGIN
     --get blank xml for the form
     v_blankxml := p_xml;
     -- Create DOMDocument handle:
     myParser := dbms_xmlparser.newParser; --**
     dbms_xmlparser.parseClob(myParser, v_blankxml);
     v_doc     := dbms_xmlparser.getDocument(myParser);
     --v_doc     := dbms_xmldom.newDOMDocument(v_blankxml);
     docelem := dbms_xmldom.getDocumentElement( v_doc );
     --ndoc    := dbms_xmldom.makeNode(v_doc);
     -- Get First Child Of the Node
      v_param := p_searchparam;
      nodelist := dbms_xmldom.getElementsByTagName(docelem, v_param);
      nodelistSize := dbms_xmldom.getLength(nodelist) ;
      nodelistSizeCount := 0;
      WHILE nodelistSizeCount < nodelistSize LOOP
             node := dbms_xmldom.item(nodelist,nodelistSizeCount);
            IF  (dbms_xmldom.isNull(node) = FALSE)  THEN
                          --p('got node');
                 childnode := dbms_xmldom.getFirstChild(node);
                     IF  (dbms_xmldom.isNull(childnode) = FALSE)  THEN
                         --p('got child node');
                         --p(dbms_xmldom.getNodeValue(childnode));
                         o_value := dbms_xmldom.getNodeValue(childnode);
                    END IF;
            END IF;
             nodelistSizeCount :=  nodelistSizeCount + 1;
      END LOOP;
    END;
PROCEDURE SP_COPYLOOKUPFLD_FOR_FORM ( p_org_field_lib_id NUMBER , p_formsec NUMBER , p_user_id NUMBER , p_ip_add VARCHAR2  , p_account_id NUMBER ,p_formcnt NUMBER ,p_oldfldxml CLOB, o_ret OUT NUMBER, o_newsys OUT VARCHAR2 )
/***************************************************************************************************************
   ** Procedure to copy fields of the form to a new form
   ** Author: Sonia Sahni 22nd Oct 2003
   ** Input parameter:pk of the original field to be copied
   ** Input parameter: The pk of the new section to which the fields are copied
   ** Input parameter: The pk of the user
   ** Input parameter: The Ipadd
   ** Input parameter: The account id passed as parameter instead of read again
   ** Output parameter: The 1  for successful completion, -1 for error
   ** o_newsys - system id of the new field
   **/
   AS
    v_newfldlib_id      NUMBER ;
    v_newuniqueid       NUMBER;
    v_newformfld_id     NUMBER ;
    v_oldformfld_id     NUMBER ;
    v_fld_systemid_old VARCHAR2(50) ;
    v_fld_systemid_new VARCHAR2(50) ;
    v_formfld_javascr VARCHAR2(4000) ;
    v_formfld_javascr_init VARCHAR2(4000) ;
    v_formfld_xsl   VARCHAR2(4000);
     v_formfld_xsl_init VARCHAR2(4000);
    v_formuniqueid VARCHAR2(50);
    v_old_lkpdispval VARCHAR2(2000);
    v_new_lkpdispval VARCHAR2(2000);
    v_old_lkp_secfld_sysid VARCHAR2(50);
    v_new_lkp_secfld_sysid VARCHAR2(50);
    V_ARRDISP_SYSID Types.SMALL_STRING_ARRAY := Types.SMALL_STRING_ARRAY ();
    v_ret_replkp NUMBER;
    BEGIN
        --select the pk of the new field
        SELECT seq_er_fldlib.NEXTVAL,seq_flduniqueid.NEXTVAL
        INTO v_newfldlib_id, v_newuniqueid
        FROM dual ;
        --get the pk_formfld corresponding to this field as there is one-to-one correspondence
        --between fk_field and pk_formfld
        --we get the formfld_xsl , formfld_javascr or the old er_formfld record
        SELECT FLD_LKPDISPVAL, pk_formfld, formfld_xsl , formfld_javascr
        INTO v_old_lkpdispval, v_oldformfld_id, v_formfld_xsl_init , v_formfld_javascr_init
        FROM ER_FLDLIB, ER_FORMFLD
        WHERE pk_field = p_org_field_lib_id AND fk_field = pk_field;
        v_new_lkpdispval := v_old_lkpdispval;
        /**************** LOOKUP *************/
            -- get list of system ids in the old lookup field
            V_ARRDISP_SYSID := Pkg_Lookup.SP_GET_LKP_SYSIDS(v_old_lkpdispval);
            -- iterate through the ols sysids in V_ARRDISP_SYSID and replace the new display string
             FOR s IN 1..V_ARRDISP_SYSID.COUNT -- put old sys id and pk_field in array
               LOOP
                   --get old sysid
                      v_old_lkp_secfld_sysid := trim(V_ARRDISP_SYSID(s));
                    -- get new sysid
                     SP_SEARCH_IN_XML (p_oldfldxml, v_old_lkp_secfld_sysid, v_new_lkp_secfld_sysid);
                     -- replace in new fld's display val
                     v_new_lkpdispval := REPLACE(v_new_lkpdispval, v_old_lkp_secfld_sysid, v_new_lkp_secfld_sysid);
                     --replace for new flds' XSL and JS
                     v_formfld_xsl_init := REPLACE(v_formfld_xsl_init, v_old_lkp_secfld_sysid, v_new_lkp_secfld_sysid);
                     v_formfld_javascr_init := REPLACE(v_formfld_javascr_init, v_old_lkp_secfld_sysid, v_new_lkp_secfld_sysid);
              END LOOP;
        /**************** LOOKUP *************/
         INSERT INTO ER_FLDLIB (
        PK_FIELD ,     FK_ACCOUNT ,      FLD_LIBFLAG,  FLD_NAME,
        FLD_DESC,      FLD_UNIQUEID,     FLD_KEYWORD,  FLD_TYPE,
        FLD_DATATYPE,  FLD_INSTRUCTIONS, FLD_LENGTH,   FLD_DECIMAL,
        FLD_LINESNO,   FLD_CHARSNO,      FLD_DEFRESP,  FK_LOOKUP,
        FLD_ISUNIQUE,  FLD_ISREADONLY,   FLD_ISVISIBLE,FLD_COLCOUNT,
        FLD_FORMAT,    FLD_REPEATFLAG,   FLD_BOLD,     FLD_ITALICS,
        FLD_SAMELINE,  FLD_ALIGN,        FLD_UNDERLINE,FLD_COLOR,
        FLD_FONT,      RECORD_TYPE,      FLD_FONTSIZE, FLD_LKPDATAVAL,
        FLD_LKPDISPVAL,IP_ADD,           CREATOR ,     CREATED_ON,
        FLD_TODAYCHECK, FLD_OVERRIDE_MANDATORY,FLD_OVERRIDE_FORMAT,FLD_OVERRIDE_RANGE,FLD_OVERRIDE_DATE, FLD_LKPTYPE,  FLD_EXPLABEL ,
        FLD_HIDELABEL,   FLD_DISPLAY_WIDTH    )
        SELECT v_newfldlib_id , p_account_id ,  FLD_LIBFLAG , fld_name ,
        FLD_DESC,
        CASE WHEN  fld_type = 'C'  OR fld_type='H' THEN '' WHEN fld_type='E' OR fld_type='M' THEN  SUBSTR(FLD_UNIQUEID ,1,50)  END ,
        FLD_KEYWORD , FLD_TYPE ,
        FLD_DATATYPE , FLD_INSTRUCTIONS ,FLD_LENGTH  , FLD_DECIMAL  ,
        FLD_LINESNO,   FLD_CHARSNO,      FLD_DEFRESP,  FK_LOOKUP,
        FLD_ISUNIQUE,  FLD_ISREADONLY,   FLD_ISVISIBLE,FLD_COLCOUNT,
        FLD_FORMAT,    FLD_REPEATFLAG,   FLD_BOLD,     FLD_ITALICS,
        FLD_SAMELINE,  FLD_ALIGN,        FLD_UNDERLINE,FLD_COLOR,
        FLD_FONT,      'N',              FLD_FONTSIZE, FLD_LKPDATAVAL,
        v_new_lkpdispval, p_ip_add ,       p_user_id,    SYSDATE,
        FLD_TODAYCHECK, FLD_OVERRIDE_MANDATORY, FLD_OVERRIDE_FORMAT,FLD_OVERRIDE_RANGE,FLD_OVERRIDE_DATE,FLD_LKPTYPE,  FLD_EXPLABEL,
        FLD_HIDELABEL,   FLD_DISPLAY_WIDTH
        FROM ER_FLDLIB
        WHERE pk_field = p_org_field_lib_id ;
         IF SQL%FOUND THEN
          INSERT INTO ER_FLDRESP
          (  PK_FLDRESP,
             FK_FIELD,
             FLDRESP_SEQ,
              FLDRESP_DISPVAL,
             FLDRESP_DATAVAL,
             FLDRESP_SCORE,
             FLDRESP_ISDEFAULT ,
             RECORD_TYPE ,
             CREATOR ,
             IP_ADD ,
             CREATED_ON  )
          SELECT
              seq_er_fldresp.NEXTVAL,
              v_newfldlib_id,
              FLDRESP_SEQ,
              FLDRESP_DISPVAL,
              FLDRESP_DATAVAL,
              FLDRESP_SCORE,
              FLDRESP_ISDEFAULT,
              DECODE(NVL(record_type,'N'),'H','H','N'),
              p_user_id ,
              p_ip_add,
              SYSDATE
         FROM ER_FLDRESP
         WHERE fk_field = p_org_field_lib_id
           AND NVL(record_type,'Z') <> 'D' ;
         o_ret := v_newfldlib_id ;
          ELSE
           o_ret := -1;
        RETURN ;
        END IF ;
        --we enter a new record in er_formfld corresponding to this entry
        SELECT seq_er_formfld.NEXTVAL
        INTO v_newformfld_id
        FROM dual ;
        --we get the old systemid and the new system id from er_fldlib for the current field
        --and the org field
        SELECT fld_systemid
        INTO v_fld_systemid_old
         FROM ER_FLDLIB
        WHERE pk_field = p_org_field_lib_id ;
        SELECT fld_systemid
        INTO v_fld_systemid_new
        FROM ER_FLDLIB
        WHERE pk_field = v_newfldlib_id ;
        v_formfld_javascr := REPLACE( v_formfld_javascr_init , v_fld_systemid_old ,v_fld_systemid_new ) ;
        v_formfld_xsl   := REPLACE ( v_formfld_xsl_init , v_fld_systemid_old , v_fld_systemid_new );
        o_newsys := v_fld_systemid_new; -- sonia sahni
    -- To set the browserflag of copied form to 0 when more than one form is copied and set browserflag to the corresponding
    -- browserflag values of the copied forms when more than one form is copied
     IF p_formcnt = 1 THEN
        INSERT INTO ER_FORMFLD (
        PK_FORMFLD,         FK_FORMSEC ,        FK_FIELD  , FORMFLD_SEQ ,
        FORMFLD_MANDATORY , FORMFLD_BROWSERFLG, FORMFLD_XSL,FORMFLD_JAVASCR ,
          CREATOR ,           RECORD_TYPE  ,        CREATED_ON, IP_ADD )
        SELECT v_newformfld_id , p_formsec , v_newfldlib_id , FORMFLD_SEQ ,
        FORMFLD_MANDATORY, FORMFLD_BROWSERFLG ,v_formfld_xsl ,  v_formfld_javascr ,
        p_user_id ,  'N' , SYSDATE , p_ip_add
        FROM ER_FORMFLD
        WHERE pk_formfld = v_oldformfld_id;
    ELSE
        INSERT INTO ER_FORMFLD (
        PK_FORMFLD,         FK_FORMSEC ,        FK_FIELD  , FORMFLD_SEQ ,
        FORMFLD_MANDATORY , FORMFLD_BROWSERFLG, FORMFLD_XSL,FORMFLD_JAVASCR ,
          CREATOR ,           RECORD_TYPE  ,        CREATED_ON, IP_ADD )
        SELECT v_newformfld_id , p_formsec , v_newfldlib_id , FORMFLD_SEQ ,
        FORMFLD_MANDATORY, 0 ,v_formfld_xsl ,  v_formfld_javascr ,
        p_user_id ,  'N' , SYSDATE , p_ip_add
        FROM ER_FORMFLD
        WHERE pk_formfld = v_oldformfld_id;
     END IF;
        -- repeat new lookup field in repeat section
        Pkg_Lookup.SP_REPEAT_LOOKUP ( v_newfldlib_id ,v_newformfld_id , p_formsec,v_ret_replkp );
     COMMIT ;
    END ;
    --end of SP_COPYLOOKUPFLD_FOR_FORM
-------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------
PROCEDURE SP_INSERT_CRF_FORM(p_form IN NUMBER, p_formxml IN CLOB,p_schevent IN NUMBER, p_fkcrf IN NUMBER, p_completed IN NUMBER,
                             p_creator IN NUMBER, p_valid IN NUMBER, p_ipAdd IN VARCHAR2, o_ret OUT NUMBER)
AS
 /****************************************************************************************************
   ** Procedure to insert a filled crf related form
   ** Author: Sonika Talwar 2nd Dec 2003
   ** Input parameter p_form  - Form ID
   ** Input parameter p_formxml - Form xml
   ** Input parameter p_schevent  - Event Id
   ** Input parameter p_fkcrf  - Crf id of sch_crf table
   ** Input parameter p_completed - Form compeletion status
   ** Input parameter p_creator - Form record creator
   ** Input parameter p_valid - Form validity flag
   ** Input parameter p_ipAdd - User's IP ADD
   ** Output parameter o_ret - Returns primary key of new record
   **/
  v_xmlclob    CLOB;
  v_pkfilledform NUMBER;
  v_formlibver NUMBER;
  BEGIN
  v_xmlclob :=  p_formxml;
  SELECT SEQ_ER_CRFFORMS.NEXTVAL
  INTO  v_pkfilledform
  FROM dual;
  SELECT MAX(pk_formlibver)
  INTO v_formlibver
  FROM ER_FORMLIBVER
  WHERE fk_formlib = p_form;
   INSERT INTO ER_CRFFORMS(PK_CRFFORMS,FK_FORMLIB,FK_SCHEVENT1,FK_CRF,CRFFORMS_XML,FORM_COMPLETED,RECORD_TYPE,
                CREATOR,ISVALID ,CREATED_ON,IP_ADD,FK_FORMLIBVER)
          VALUES ( v_pkfilledform,p_form,p_schevent,p_fkcrf, XMLTYPE(v_xmlclob),
            p_completed ,'N', p_creator,p_valid,  SYSDATE, p_ipAdd, v_formlibver );
  o_ret := v_pkfilledform;
  EXCEPTION  WHEN OTHERS THEN
       RAISE_APPLICATION_ERROR (-20102, 'Exception occurred in SP_INSERT_CRF_FORM :'||SQLERRM);
  END ; --end of SP_INSERT_CRF_FORM
 PROCEDURE SP_UPDT_INSRT_FORSTATUSCHNG(p_formLibId IN NUMBER, p_status IN NUMBER, p_creator IN NUMBER, p_ipAdd IN VARCHAR2, o_ret OUT NUMBER)
AS
 /****************************************************************************************************
   ** Procedure to update the enddate and insert a new record in er_formmstat when the status of a form is changed
   ** Author: Anu Khanna 19/April/04.
   ** Input parameter p_formLibId - Form ID
   ** Input parameter p_status - Form status
   ** Input parameter p_creator - Form record creator
   ** Input parameter p_ipAdd - User's IP ADD
   ** Output parameter o_ret - Returns primary key of new record
   **/
  v_pkformstat NUMBER;
  BEGIN
  UPDATE ER_FORMSTAT
  SET formstat_enddate = SYSDATE, last_modified_by = p_creator, ip_add = p_ipAdd
  WHERE fk_formlib=p_formLibId
  AND formstat_enddate IS NULL ;
  SELECT seq_er_formstat.NEXTVAL
  INTO  v_pkformstat
  FROM dual;
  INSERT INTO ER_FORMSTAT
                        (pk_formstat,
                        fk_formlib,
                        fk_codelst_stat,
                        formstat_stdate,
                        formstat_changedby,
                        record_type,
                        creator,
                        created_on,
                        ip_add)
                 VALUES(v_pkformstat,
                        p_formLibId,
                        p_status,
                        SYSDATE,
                        p_creator,
                        'N',
                        p_creator,
                        SYSDATE,
                        p_ipAdd);
  o_ret := v_pkformstat;
  EXCEPTION  WHEN OTHERS THEN
       RAISE_APPLICATION_ERROR (-20102, 'Exception occurred in SP_UPDATE_INSERT_FORSTATUSCHANGE:'||SQLERRM);
  END ; --end of SP_UPDATE_INSERT_FORSTATUSCHANGE
   /*************************************************************
 Function cut the field label from the XSL, wil be used for fields in tabular sections
 Parameters:
 P_FLDXSL the actual field XSL
 P_FLDTYPE the field type
 P_FLDDATATYPE the field data type
 P_FLDALIGN  the field alignment attribute
 P_PKFIELD the primary key of the field (for future use, if any other attribute is required,
  then we just change in the function to get more data
 returns : the new field xsl
 ************************/
 FUNCTION f_cut_fieldlabel(P_FLDXSL VARCHAR2,P_FLDTYPE VARCHAR2,P_FLDDATATYPE VARCHAR2, P_FLDALIGN VARCHAR2,P_PKFIELD NUMBER) RETURN VARCHAR2
    AS
    v_fldxsl VARCHAR2(4000) := P_FLDXSL;
    v_pos_1 NUMBER;
    v_repfld_xsl_2 VARCHAR2(4000);
    len NUMBER;
    v_pos_2 NUMBER;
    v_pos_endtd  NUMBER;
    v_strlen NUMBER;
    v_repfld_xsl_3 VARCHAR2(4000);
    v_fld_align VARCHAR2(20);
   BEGIN
    IF P_FLDTYPE  = 'C' OR P_FLDTYPE  = 'H' OR NVL(P_FLDDATATYPE,'Z')= 'ML'  OR P_FLDTYPE = 'F'  OR P_FLDTYPE = 'S' THEN
        RETURN P_FLDXSL ; --we dont have to change XSL for such cases
    END IF;
    v_fld_align := NVL(P_FLDALIGN,'left');
    IF v_fld_align = 'top' THEN
        P('top');
            v_pos_1 := INSTR( v_fldxsl, '<label') ;
            v_repfld_xsl_2 := SUBSTR(v_fldxsl, 0 ,v_pos_1-1);
            len := LENGTH(v_fldxsl);
            v_pos_2 := INSTR(v_fldxsl , '</label>');
            --look for first <BR/> after the label, that should be pos2
            v_pos_endtd  := INSTR(v_fldxsl , '<BR/>',v_pos_2);
           --# of characters between </label> and <BR/>
           v_strlen := LENGTH('<BR/>');
           v_strlen := (v_pos_endtd - v_pos_2) + v_strlen;
           v_repfld_xsl_3 := SUBSTR( v_fldxsl , v_pos_2+v_strlen , len-1 ) ;
           v_fldxsl := v_repfld_xsl_2 || v_repfld_xsl_3 ;
    ELSE
        v_pos_1 := INSTR( v_fldxsl, '<td') ;
        v_repfld_xsl_2 := SUBSTR(v_fldxsl, 0 ,v_pos_1-1);
        --  p('v_repfld_xsl_2 ' ||v_repfld_xsl_2);
        len := LENGTH(v_fldxsl);
        v_pos_2 := INSTR(v_fldxsl , '</label>');
         --v_strlen := length( '</label>  &#xa0;&#xa0;&#xa0;</td>' );
         --find </td> end tag of label
             v_pos_endtd := INSTR(v_fldxsl , '</td>',v_pos_2);
             --# of characters between </label> and </td>
         v_strlen := LENGTH('</td>');
         v_strlen := (v_pos_endtd - v_pos_2) + v_strlen;
         v_repfld_xsl_3 := SUBSTR( v_fldxsl , v_pos_2+v_strlen , len-1 ) ;
         -- p('v_repfld_xsl_3 ' ||v_repfld_xsl_3);
         v_fldxsl := v_repfld_xsl_2 || v_repfld_xsl_3 ;
    END IF;
    RETURN v_fldxsl;
END;


/* Replace the old system ids with new system ids*/
PROCEDURE SP_REPLACE_SYSIDS (p_oldsys VARCHAR2, p_newsys VARCHAR2,
 io_formxsl IN OUT CLOB , io_formxml IN OUT CLOB , io_formviewxsl IN OUT CLOB ,io_formcustomjs IN OUT CLOB, io_activejs  IN OUT CLOB  )
 AS
 BEGIN
               io_formxsl := REPLACE ( io_formxsl , p_oldsys, p_newsys);

              io_formxml := REPLACE ( io_formxml , p_oldsys, p_newsys);

              io_formviewxsl := REPLACE ( io_formviewxsl , p_oldsys, p_newsys);
              io_formcustomjs := REPLACE ( io_formcustomjs , p_oldsys, p_newsys);
              io_activejs := REPLACE ( io_activejs , p_oldsys, p_newsys);

 END;


 /* Replace the old system ids with new system ids in Repeat Fields*/
PROCEDURE SP_REPLACE_REPEAT_SYSIDS (p_oldfld NUMBER, p_newfld NUMBER,
 io_formxsl IN OUT CLOB , io_formxml IN OUT CLOB , io_formviewxsl IN OUT CLOB ,io_formcustomjs IN OUT CLOB, io_activejs  IN OUT CLOB  )
 AS
 v_old_sys VARCHAR2(50);
  v_new_sys VARCHAR2(50);
  v_ctr NUMBER;

  V_ARR_OLDSYSID Types.SMALL_STRING_ARRAY := Types.SMALL_STRING_ARRAY ();
  V_ARR_OLDREPFLD Types.SMALL_STRING_ARRAY := Types.SMALL_STRING_ARRAY ();


 BEGIN

       --get old field id's repeat fields and their system ids

        v_ctr := 1;
       FOR i IN (   SELECT fld.pk_field, fld.fld_systemid ,rf.fk_formsec
                             FROM ER_REPFORMFLD rf  , ER_FLDLIB fld, ER_FORMFLD ff
                              WHERE ff.fk_field = p_oldfld AND  fk_formfld = pk_formfld AND fld.pk_field = rf.fk_field ORDER BY 1)
        LOOP
                      V_ARR_OLDSYSID.EXTEND;
                       V_ARR_OLDSYSID(v_ctr)  := i.fld_systemid;

                       V_ARR_OLDREPFLD.EXTEND;
                       V_ARR_OLDREPFLD(v_ctr) := i.pk_field;


                       v_ctr := v_ctr +1;
            END LOOP;

        v_ctr := 1;
      --get new field id's repeat fields and their system ids
         FOR i IN (   SELECT fld.pk_field, fld.fld_systemid,rf.fk_formsec
                             FROM ER_REPFORMFLD rf  , ER_FLDLIB fld, ER_FORMFLD ff
                              WHERE ff.fk_field = p_newfld AND  fk_formfld = pk_formfld AND fld.pk_field = rf.fk_field ORDER BY 1)
        LOOP
                     v_old_sys := '';

                     IF  v_ctr <= V_ARR_OLDSYSID.COUNT THEN
                             v_old_sys :=   V_ARR_OLDSYSID(v_ctr)  ;
                             v_new_sys := i.fld_systemid ;
                             SP_REPLACE_SYSIDS (v_old_sys , v_new_sys , io_formxsl , io_formxml  , io_formviewxsl ,io_formcustomjs , io_activejs );

                             io_formxml := REPLACE ( io_formxml , 'pkfld = "' || V_ARR_OLDREPFLD(v_ctr) || '"', 'pkfld = "' ||  i.pk_field  || '"');

                            -- io_formxml := REPLACE ( io_formxml , p_oldsys, p_newsys);
                     END IF;

                       v_ctr := v_ctr +1;
            END LOOP;


 END;
 FUNCTION f_getFormAction(P_FORM NUMBER) RETURN CLOB
  AS
  v_formAction CLOB:= 'if ($E) {'|| CHR(13);
  v_srcFld VARCHAR2(4000);
  BEGIN
  FOR  i IN (SELECT formaction_sourcefld INTO v_srcFld FROM ER_FORMACTION WHERE formaction_sourceform=p_form)
  LOOP
    v_formAction:=v_formAction||CHR(13)||'$E.addListener("'||i.formaction_sourcefld||'","blur",triggerFormAction,this);';
   END LOOP;
   v_formAction:=v_formAction||CHR(13)||'}';
    RETURN v_formAction;
   EXCEPTION WHEN OTHERS THEN
 RAISE_APPLICATION_ERROR (-20102, 'f_getFormAction'||SQLERRM);
END;

END Pkg_Form;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,398,10,'10_PKG_FORM_BODY.sql',sysdate,'v11 #799');

commit; 
