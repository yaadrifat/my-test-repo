set define off;

DECLARE
  v_item_exists number := 0;  
BEGIN

 	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'studyPurpose' and codelst_subtyp = 'basicScience';
	if (v_item_exists = 1) then
		UPDATE ER_CODELST SET CODELST_SEQ=1 where codelst_type = 'studyPurpose' and codelst_subtyp = 'basicScience';
	end if;

 	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'studyPurpose' and codelst_subtyp = 'diagnostic';
	if (v_item_exists = 1) then
		UPDATE ER_CODELST SET CODELST_SEQ=2 where codelst_type = 'studyPurpose' and codelst_subtyp = 'diagnostic';
	end if;

 	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'studyPurpose' and codelst_subtyp = 'healthSvcRsrch';
	if (v_item_exists = 1) then
		UPDATE ER_CODELST SET CODELST_SEQ=3 where codelst_type = 'studyPurpose' and codelst_subtyp = 'healthSvcRsrch';
	end if;

 	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'studyPurpose' and codelst_subtyp = 'prevention';
	if (v_item_exists = 1) then
		UPDATE ER_CODELST SET CODELST_SEQ=4 where codelst_type = 'studyPurpose' and codelst_subtyp = 'prevention';
	end if;

 	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'studyPurpose' and codelst_subtyp = 'screening';
	if (v_item_exists = 1) then
		UPDATE ER_CODELST SET CODELST_SEQ=5 where codelst_type = 'studyPurpose' and codelst_subtyp = 'screening';
	end if;

 	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'studyPurpose' and codelst_subtyp = 'supportiveCare';
	if (v_item_exists = 1) then
		UPDATE ER_CODELST SET CODELST_SEQ=6 where codelst_type = 'studyPurpose' and codelst_subtyp = 'supportiveCare';
	end if;

 	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'studyPurpose' and codelst_subtyp = 'treatment';
	if (v_item_exists = 1) then
		UPDATE ER_CODELST SET CODELST_SEQ=7 where codelst_type = 'studyPurpose' and codelst_subtyp = 'treatment';
	end if;

 	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'studyPurpose' and codelst_subtyp = 'other';
	if (v_item_exists = 1) then
		UPDATE ER_CODELST SET CODELST_SEQ=8 where codelst_type = 'studyPurpose' and codelst_subtyp = 'other';
	end if;	

COMMIT;
end;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,304,6,'06_ER_CODELST_UPDATE.sql',sysdate,'v9.3.0 #705');

commit;