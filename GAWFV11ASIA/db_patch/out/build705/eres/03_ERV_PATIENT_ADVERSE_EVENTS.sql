CREATE OR REPLACE FORCE VIEW ERV_PATIENT_ADVERSE_EVENTS
(
   PATIENT_ID,
   STUDY_NUMBER,
   PATIENT_STUDY_ID,
   ADVERSE_EVENT,
   GRADE,
   STUDY_TITLE,
   TYPE,
   DESCRIPTION,
   TREATMENT_COURSE,
   START_DATE,
   STOP_DATE,
   ENTERED_BY,
   RELATIONSHIP,
   OUTCOME,
   ACTION,
   RECOVERY_DESCRIPTION,
   OUTCOME_NOTES,
   ADDITIONAL_INFORMATION,
   NOTIFIED,
   NOTES,
   FK_PER,
   PK_STUDY,
   CREATED_ON,
   FK_ACCOUNT,
   MEDDRA_CODE,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   PATAE_PAT_NAME,
   PATAE_DEATHDT,
   PATAE_DICTIONARY,
   RID,
   PATAE_RESPONSE_ID,
   CATEGORY,
   TOXICITY,
   TOXICTY_DESC,
   GRADE_DESC
)
AS
   SELECT   per_code AS patient_id,
            study_number,
            patprot_patstdid AS patient_study_id,
            ae_name AS adverse_event,
            ae_grade AS grade,
            study_title,
            (SELECT   codelst_desc
               FROM   sch_codelst
              WHERE   pk_codelst = fk_codlst_aetype)
               AS TYPE,
            ae_desc AS description,
            ae_treatment_course AS treatment_course,
            TRUNC (ae_stdate) AS start_date,
            TRUNC (ae_enddate) AS stop_date,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = ae_enterby)
               AS entered_by,
            (SELECT   codelst_desc
               FROM   sch_codelst
              WHERE   pk_codelst = ae_relationship)
               AS relationship,
            DECODE (TO_NUMBER (ae_outtype),
                    0, '',
                    F_Getae_Outcome_Add_Info (pk_adveve,
                                              ae_outdate,
                                              'outcome',
                                              NULL))
               AS outcome,
            (SELECT   codelst_desc
               FROM   sch_codelst
              WHERE   pk_codelst = fk_codelst_outaction)
               AS action,
            (SELECT   codelst_desc
               FROM   sch_codelst
              WHERE   pk_codelst = ae_recovery_desc)
               AS recovery_description,
            ae_outnotes AS outcome_notes,
            DECODE (TO_NUMBER (ae_addinfo),
                    0, '',
                    F_Getae_Outcome_Add_Info (pk_adveve,
                                              ae_outdate,
                                              'adve_info',
                                              NULL))
               AS additional_information,
            F_Getae_Notify (pk_adveve) notified,
            ae_notes notes,
            pk_per AS fk_per,
            pk_study,
            y.created_on CREATED_ON,
            ER_PER.fk_account AS fk_account,
            meddra meddra_code,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = Y.CREATOR)
               creator,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = Y.LAST_MODIFIED_BY)
               Last_modified_by,
            pkg_util.date_to_char_with_time (Y.LAST_MODIFIED_DATE)
               LAST_MODIFIED_DATE,
            (SELECT   PERSON_FNAME || ' ' || PERSON_LNAME
               FROM   EPAT.PERSON
              WHERE   PK_PER = PK_PERSON)
               PATAE_PAT_NAME,
            (SELECT   TRUNC (PERSON_DEATHDT)
               FROM   EPAT.PERSON
              WHERE   PK_PER = PK_PERSON)
               PATAE_DEATHDT,
            Y.DICTIONARY PATAE_DICTIONARY,
            Y.RID,
            PK_ADVEVE PATAE_RESPONSE_ID,
            AE_CATEGORY,
            AE_TOXICITY,
            AE_TOXICTY_DESC,
            AE_GRADE_DESC
     FROM   ER_PER,
            ER_PATPROT x,
            sch_adverseve y,
            ER_STUDY
    WHERE       pk_per = x.fk_per
            AND x.patprot_stat = 1
            AND pk_per = y.fk_per
            AND pk_study = x.fk_study
            AND pk_study = y.fk_study;
/
CREATE OR REPLACE SYNONYM "ESCH"."ERV_PATIENT_ADVERSE_EVENTS" FOR ERV_PATIENT_ADVERSE_EVENTS;
CREATE OR REPLACE SYNONYM "EPAT"."ERV_PATIENT_ADVERSE_EVENTS" FOR ERV_PATIENT_ADVERSE_EVENTS;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,304,3,'03_ERV_PATIENT_ADVERSE_EVENTS.sql',sysdate,'v9.3.0 #705');

commit;


