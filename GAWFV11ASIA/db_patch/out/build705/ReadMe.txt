/////*********This readMe is specific to v9.3.0 build #705**********////

*********************************************************************************************

In Build#704 we had released REP-22595 (CCSG Data Table 3) enhancement. For this we have released a "set of instructions" to configure "Study Type" & "Organization Type".
We have made few changes in these configuration settings. In this Build#705, we have released the "Velos eResearch Technical HowTo - Configuring Study Types.docx" in this regard.
Please follow the instructions mentioned in the document to configure  "Study Type". 

For  "Organization Type" Please note that there is no configuration required in this regard. In order to revert the earlier configuration settings done in build#704, please execute the following SQL

 update er_codelst set  codelst_custom_col = null  where codelst_type = 'studySiteType' and codelst_subtyp = 'primary'; commit;


NOTE:On "Study Summary Page" , there is one checkbox(CCSG data table 4 reportable) introduced. This  functionality is  related to one upcoming enhancement which will be released later.Please ignore this field as of now.



