/////*********This readMe is specific to v10.1 build #778	  **********////

The following enhancement is released in the build:

1. Regular bug fixes 39 items.
2. Integrated HF 5 & 6 of Build#776 into this build. These items covers 21 Priority fixes and also 3 enhancements. 

    1. FIN-39106 -- Milestone Amendment :   For a given study, visit milestones have been achieved and invoices have been created. The study is then amended and milestone amounts needs to be changed as of amendment date.  The original milestones are inactivated and new milestones are created with the amended date as the start date with the new amount. Since the milestone is activated, the system doesn�t allow the user to modify the end date of the milestone and therefore the only way to stop the milestone from achieving is to inactivate the milestone. But in the invoice screen, the achieved milestones are not shown as the milestones were inactivated. For details please go through , FIN-39106 -- Milestone Amendment Sep 26 2016 v0.2.docx  &  2. FIN-39106 -- Milestone Amendment Oct 27 2016 v0.4.doc
2. Bring the "Edit Multiple Events" link back to Patient Schedule page  
3. Create Study Flex Page  (S-39824 -- Study Summary Flex Mode Dec 14 2016 v0.1.docx)

