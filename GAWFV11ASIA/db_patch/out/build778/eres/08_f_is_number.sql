CREATE or REPLACE  FUNCTION F_IS_NUMBER (p_string IN VARCHAR2)
   RETURN INT
IS
   v_new_num NUMBER;
BEGIN
   v_new_num := TO_NUMBER(p_string);
   RETURN 1;
EXCEPTION
WHEN VALUE_ERROR THEN
   RETURN 0;
END F_IS_NUMBER;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,377,8,'08_f_is_number.sql',sysdate,'v10.1 #778');

commit;
