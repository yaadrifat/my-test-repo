create or replace
TRIGGER "ERES"."ER_STUDYTEAM_AD1" 
AFTER DELETE
ON ERES.ER_STUDYTEAM
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE


 v_old_fk_study NUMBER;
 v_old_fk_user NUMBER;
 v_old_pk NUMBER;--JM;

/* Author : Sonia Sahni
Purpose : Remove the deleted user from the study budget access rights
*/
BEGIN


 v_old_fk_study :=  :OLD.fk_study ;
 v_old_fk_user := :OLD.fk_user ;

 --JM: 08Nov2006
 v_old_pk := :OLD.PK_STUDYTEAM;

 UPDATE ER_STATUS_HISTORY set LAST_MODIFIED_BY=:OLD.LAST_MODIFIED_BY where status_modtable ='er_studyteam' AND
  status_modpk IN (v_old_pk );
 DELETE FROM ER_STATUS_HISTORY WHERE status_modtable ='er_studyteam' AND
  status_modpk IN (v_old_pk );


Pkg_Studystat.sp_revoke_studyteam_access(v_old_fk_study , v_old_fk_user);


END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,377,7,'07_studyteam_ad1.sql',sysdate,'v10.1 #778');

commit;
