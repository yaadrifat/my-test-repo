create or replace
PROCEDURE        "SP_UPDATECLOB" (colValue  IN CLOB,tableName varchar2,colName varchar2,filterStr varchar2) IS
--CREATE OR REPLACE PROCEDURE sp_updateClob(tableName varchar2,colName varchar2,filterStr varchar2) IS

--  v_sql Varchar2(32000);
  v_sql long;
   v_empty Varchar2(10000);
  c_clob CLOB ;
  cursor_name INTEGER;
   rows_processed INTEGER;

   v_clob_char varchar2(7000);

   pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SP_UPDATECLOB', pLEVEL  => Plog.LFATAL);

 BEGIN
 --first do emptyBlob
 Plog.fatal(pCTX,'col  : table Name ' ||colName||':'|| tableName );
 v_empty:='update '||tableName||' set '||colname||'=EMPTY_CLOB() '||filterStr ;
 execute immediate v_empty ;
 commit;

 c_clob := colValue;

 --Plog.DEBUG(pCTX,'c_clob' || c_clob);

 v_clob_char := rtrim(ltrim(DBMS_LOB.SUBSTR(c_clob,4000,1)));

 Plog.DEBUG(pCTX,'v_clob_char ' || v_clob_char );

 Plog.DEBUG(pCTX,'length(v_clob_char) ' || length(v_clob_char) );

  if (nvl(length(v_clob_char),0) <= 0 )then
     c_clob := null;
 end if;

 cursor_name := dbms_sql.open_cursor;
DBMS_SQL.PARSE(cursor_name, 'UPDATE  ' || tableName || '  set  ' || colname
|| '  = :clobval  ' || filterStr, dbms_sql.native);

DBMS_SQL.BIND_VARIABLE(cursor_name, ':clobval', c_clob);

rows_processed := dbms_sql.execute(cursor_name);
   COMMIT;
	DBMS_SQL.close_cursor(cursor_name);
 -- Exception handling.
    Exception  When others then
raise_application_error(-20000, 'Unknown Exception Raised: '||sqlcode||' '||sqlerrm);

 END sp_updateClob;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,377,2,'02_sp_updateclob.sql',sysdate,'v10.1 #778');

commit; 
 