Declare
  row_check number;
  table_check number;
Begin
  select count(*) into table_check from user_tab_cols where TABLE_NAME = 'ER_BROWSERCONF';
  if(table_check>0) then
	select count(*) into row_check from er_browserconf where browserconf_colname='FINANCIAL_RIGHTS';
	if(row_check=0) then
      insert into er_browserconf(pk_browserconf,fk_browser,browserconf_colname,browserconf_seq) values(seq_er_browserconf.nextval,(select pk_browser from er_browser where browser_module='advSearch'),'FINANCIAL_RIGHTS',(select max(browserconf_seq) from er_browserconf where fk_browser=(select pk_browser from er_browser where browser_module='advSearch'))+1);   
    end if;
  end if;
commit;
End;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,377,11,'11_er_browserconf.sql',sysdate,'v10.1 #778');

commit;

