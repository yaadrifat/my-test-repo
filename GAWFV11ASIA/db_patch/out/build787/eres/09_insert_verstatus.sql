declare
v_exists_flag number:=0;
begin
select count(*) into v_exists_flag from er_codelst where codelst_type='versionStatus' and codelst_subtyp='NP';
if v_exists_flag = 0 then

  insert into er_codelst (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CREATED_ON,CODELST_STUDY_ROLE) values
  (SEQ_ER_CODELST.nextval,'versionStatus','NP','Non-Packet','N',17,sysdate,'default_data');
  commit;
  
end if;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,387,9,'09_insert_verstatus.sql',sysdate,'v11 #787');

commit;
