set define off;
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*)  into v_record_exists
  from er_browserconf where fk_browser=(select pk_browser from er_browser where browser_module='preparea') 
  and BROWSERCONF_COLNAME='PAT_STUDYID';
  if (v_record_exists = 0) then
  INSERT INTO er_browserconf 
	(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ,BROWSERCONF_SETTINGS,BROWSERCONF_EXPLABEL)
	VALUES(SEQ_ER_BROWSERCONF.nextval ,(select pk_browser from er_browser where browser_module='preparea'),'PAT_STUDYID',(select max(BROWSERCONF_SEQ)+1 from er_browserconf  where fk_browser=(select pk_browser from er_browser where browser_module='preparea')),'{"key":"PAT_STUDYID", "label":"Patient Study ID",  "resizeable":true,"sortable":true}','Patient Study Id');
  end if;
end;  
/  
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*)  into v_record_exists
  from er_browserconf where fk_browser=(select pk_browser from er_browser where browser_module='preparea') 
  and BROWSERCONF_COLNAME='SITE_NAME';
  if (v_record_exists = 0) then
	INSERT INTO er_browserconf 
	(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ,BROWSERCONF_SETTINGS,BROWSERCONF_EXPLABEL)
	VALUES(SEQ_ER_BROWSERCONF.nextval ,(select pk_browser from er_browser where browser_module='preparea'),'SITE_NAME',(select max(BROWSERCONF_SEQ)+1 from er_browserconf  where fk_browser=(select pk_browser from er_browser where browser_module='preparea')),'{"key":"SITE_NAME", "label":"Enrolling Site",  "resizeable":true,"sortable":true}','Enrolling Site');
  end if;
end; 
/ 
DECLARE
  v_record_exists number := 0;  
BEGIN
   Select count(*)  into v_record_exists
  from er_browserconf where fk_browser=(select pk_browser from er_browser where browser_module='preparea') 
  and BROWSERCONF_COLNAME='EVENT_STATUS_DESC';
  if (v_record_exists = 0) then
	INSERT INTO er_browserconf 
	(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ,BROWSERCONF_SETTINGS,BROWSERCONF_EXPLABEL)
	VALUES(SEQ_ER_BROWSERCONF.nextval ,(select pk_browser from er_browser where browser_module='preparea'),'EVENT_STATUS_DESC',(select max(BROWSERCONF_SEQ)+1 from er_browserconf  where fk_browser=(select pk_browser from er_browser where browser_module='preparea')),'{"key":"EVENT_STATUS_DESC", "label":"Event Status",  "resizeable":true,"sortable":true}','Event Status');
 end if;
end;
/
 
DECLARE
  v_record_exists number := 0;  
BEGIN
   Select count(*)  into v_record_exists
  from er_browserconf where fk_browser=(select pk_browser from er_browser where browser_module='preparea') 
  and BROWSERCONF_COLNAME='START_DATE_TIME';
  if (v_record_exists = 0) then
	INSERT INTO er_browserconf 
	(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ,BROWSERCONF_SETTINGS,BROWSERCONF_EXPLABEL)
	VALUES(SEQ_ER_BROWSERCONF.nextval ,(select pk_browser from er_browser where browser_module='preparea'),'START_DATE_TIME',(select max(BROWSERCONF_SEQ)+1 from er_browserconf  where fk_browser=(select pk_browser from er_browser where browser_module='preparea')),'{"key":"START_DATE_TIME", "label":"Event Status Date",  "resizeable":true,"sortable":true}','Event Status Date');
 end if;
end;
/ 
DECLARE
  v_record_exists number := 0;  
BEGIN
   Select count(*)  into v_record_exists
  from er_browserconf where fk_browser=(select pk_browser from er_browser where browser_module='preparea') 
  and BROWSERCONF_COLNAME='PREPARED_SAMPLE';
  if (v_record_exists = 0) then
 INSERT INTO er_browserconf 
	(PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ,BROWSERCONF_SETTINGS,BROWSERCONF_EXPLABEL)
	VALUES(SEQ_ER_BROWSERCONF.nextval ,(select pk_browser from er_browser where browser_module='preparea'),'PREPARED_SAMPLE',(select max(BROWSERCONF_SEQ)+1 from er_browserconf  where fk_browser=(select pk_browser from er_browser where browser_module='preparea')),'{"key":"PREPARED_SAMPLE", "label":"Prepared",  "resizeable":true,"sortable":true}','Prepared Sample');
 end if;
end; 
/ 
DECLARE 
	  v_record_exists number;  
	
BEGIN
		select count(*) into v_record_exists from er_browserconf where fk_browser=(select pk_browser from er_browser where browser_module='preparea')
 		and BROWSERCONF_COLNAME='CHECK_DATA';
		IF (v_record_exists > 0) then	
			UPDATE
			er_browserconf  SET                  
			BROWSERCONF_SEQ=(select max(BROWSERCONF_SEQ+1) from er_browserconf  where fk_browser=(select pk_browser from er_browser where browser_module='preparea')) 
            WHERE  
		    FK_BROWSER=(select pk_browser from er_browser where browser_module='preparea') AND BROWSERCONF_COLNAME='CHECK_DATA';
			UPDATE
			er_browserconf  SET                  
			BROWSERCONF_SEQ=BROWSERCONF_SEQ-1 
            WHERE  
		    FK_BROWSER=(select pk_browser from er_browser where browser_module='preparea') AND BROWSERCONF_SEQ>(select browserconf_seq  from er_browserconf where fk_browser=(select pk_browser from er_browser where browser_module='preparea')
 	     	and BROWSERCONF_COLNAME='STORAGE_KIT');		

		END IF;
 end;
/
commit; 

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,387,3,'03_Browserconf_update.sql',sysdate,'v11 #787');

commit;
