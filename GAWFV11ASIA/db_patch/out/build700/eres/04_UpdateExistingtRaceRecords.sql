set define off;

update er_codelst set codelst_custom_col1 = 'race_prim,race_second' where codelst_type = 'race';

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,299,4,'04_UpdateExistingtRaceRecords.sql',sysdate,'v9.3.0 #700');

commit;