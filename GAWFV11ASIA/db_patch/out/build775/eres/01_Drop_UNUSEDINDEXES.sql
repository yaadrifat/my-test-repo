set define off;

DECLARE
    i INTEGER;
BEGIN
    SELECT COUNT(*) INTO i FROM user_indexes WHERE index_name = 'INDX_FK_OBJ';
    IF i > 0 THEN
        EXECUTE IMMEDIATE 'DROP INDEX INDX_FK_OBJ';
    END IF;  
    i:=0;	
	SELECT COUNT(*) INTO i FROM user_indexes WHERE index_name = 'INDX_OBJECTSHARE_ID';
    IF i > 0 THEN
        EXECUTE IMMEDIATE 'DROP INDEX INDX_OBJECTSHARE_ID';
    END IF;
	i:=0;
	SELECT COUNT(*) INTO i FROM user_indexes WHERE index_name = 'IDX_OBJECTSHARE_COMP';
    IF i > 0 THEN
        EXECUTE IMMEDIATE 'DROP INDEX IDX_OBJECTSHARE_COMP';
    END IF;
	i:=0;
	SELECT COUNT(*) INTO i FROM user_indexes WHERE index_name = 'XIF64ER_FORMLIB';
    IF i > 0 THEN
        EXECUTE IMMEDIATE 'DROP INDEX XIF64ER_FORMLIB';
    END IF;
	i:=0;
	SELECT COUNT(*) INTO i FROM user_indexes WHERE index_name = 'XIF65ER_FORMLIB';
    IF i > 0 THEN
        EXECUTE IMMEDIATE 'DROP INDEX XIF65ER_FORMLIB';
    END IF;	
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,374,1,'01_Drop_UNUSEDINDEXES.sql',sysdate,'v10 #775');

commit;
