create or replace
FUNCTION F_Create_Aexml(p_study VARCHAR2,p_site VARCHAR2,p_startdate DATE, p_enddate DATE)
RETURN  CLOB
AS
v_xml CLOB;

v_studynum VARCHAR2(100);
v_studytitle VARCHAR2(4000);
v_site VARCHAR2(200);
v_sname VARCHAR2(100);

v_count NUMBER;
v_temp VARCHAR2(30);
tab_study split_tbl;
tab_org split_tbl;
v_study VARCHAR2(20);
--v_org VARCHAR2(20);
v_counter NUMBER :=1;
v_armxml VARCHAR2(30000);

pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'f_create_aexml', pLEVEL  => Plog.LFATAL);

V_ARM_SET          Types.SMALL_STRING_ARRAY := Types.SMALL_STRING_ARRAY ();
V_ADV_ARMNAME          Types.SMALL_STRING_ARRAY := Types.SMALL_STRING_ARRAY ();
V_ADV_ARMXML          Types.SMALL_STRING_ARRAY := Types.SMALL_STRING_ARRAY ();
v_ct NUMBER:=0;
v_arm  VARCHAR2(250);
 bfound BOOLEAN := FALSE;
v_per_grade0 NUMBER;
v_per_grade1 NUMBER;
v_per_grade2 NUMBER;
v_per_grade3 NUMBER;
v_per_grade4 NUMBER;
v_per_grade5 NUMBER;
v_toxicity_total NUMBER;

v_tper_grade0 NUMBER;
v_tper_grade1 NUMBER;
v_tper_grade2 NUMBER;
v_tper_grade3 NUMBER;
v_tper_grade4 NUMBER;
v_tper_grade5 NUMBER;


V_PER_ELEMENTS VARCHAR2(2000);
V_TPER_ELEMENTS VARCHAR2(2000);
--Patients having adverse events (ae+sae)
v_allae_pat_count NUMBER := 0;
--Patients having serious adverse events
v_sae_pat_count NUMBER:= 0;
--Total adverse events (ae+sae)
v_all_ae_count NUMBER:= 0;

v_serious_type NUMBER;
v_lkpview_name VARCHAR2(20);
v_lkp_view NUMBER;

 v_temp_str VARCHAR2(30000);

 -- to run a loop based on adverse events subtypes

 V_LOOP_SET   Types.SMALL_STRING_ARRAY := Types.SMALL_STRING_ARRAY ();

BEGIN

--DBMS_LOB.CREATETEMPORARY(v_xml,true);

v_temp_str :=  v_temp_str || '<ROWSET>';

-- dbms_lob.writeappend( v_xml, length(v_temp_str), v_temp_str );
v_xml := v_xml || v_temp_str;


SELECT Pkg_Util.f_split(p_study) INTO tab_study FROM dual;

SELECT Pkg_Util.f_split(p_site) INTO tab_org FROM dual;

 FOR i IN (SELECT COLUMN_VALUE AS study FROM TABLE(tab_study))
 LOOP
 v_study:=i.study;
 v_site := '';
 v_allae_pat_count := 0;
 v_sae_pat_count := 0;
 v_all_ae_count := 0;



-- FOR j IN (SELECT COLUMN_VALUE AS org FROM TABLE(tab_org))
-- LOOP
--v_org:=j.org;
-- plog.DEBUG(pCTX,'site-'||v_org);

SELECT study_number, study_title INTO v_studynum, v_studytitle FROM ER_STUDY WHERE pk_study  = v_study;

/*select pkg_util.f_join(cursor(SELECT site_name FROM ER_SITE WHERE pk_site in ( SELECT COLUMN_VALUE FROM TABLE(tab_org) )),',')
into v_site
from dual;*/

-- get total enrolled patients for the date range to calculate percentage
 SELECT COUNT(DISTINCT fk_per) INTO v_toxicity_total FROM ER_PATPROT,ER_PER
 WHERE fk_study = v_study AND patprot_stat = 1 AND
 patprot_enroldt IS NOT NULL AND pk_per = fk_per AND fk_site IN (SELECT COLUMN_VALUE FROM TABLE(tab_org));


-- get site names(s)
 FOR s IN (SELECT COLUMN_VALUE FROM TABLE(tab_org))
 LOOP

	 SELECT REPLACE(site_name, '&', '&amp;') INTO v_sname FROM ER_SITE WHERE pk_site = s.COLUMN_VALUE;

 	 v_site := v_site || ',' || v_sname;
 END LOOP;

 IF (LENGTH(v_site) > 1) THEN
 	 v_site := SUBSTR(v_site,2);
 END IF;

   -- get total Patients with Adverse events and Serious AEs

  SELECT COUNT(DISTINCT fk_per) adcount INTO v_allae_pat_count
  FROM  sch_adverseve
  WHERE fk_study =  v_study AND
  fk_per IN (SELECT DISTINCT fk_per FROM ER_PATPROT, ER_PER
  	 WHERE pk_per = fk_per AND fk_site IN (SELECT COLUMN_VALUE FROM TABLE(tab_org)) AND fk_study = v_study AND patprot_stat = 1) AND
  (ae_stdate BETWEEN p_startdate AND p_enddate ) AND
  (ae_stdate BETWEEN NVL(Pkg_Util.f_get_min_patstudystatdate(v_study,fk_per,'active'), p_startdate) AND (NVL(Pkg_Util.f_get_max_patstudystatdate(v_study,fk_per,'offstudy'),p_enddate) + 30) ) AND
  AE_TOXICITY IS NOT NULL ;


 --get serious adverse events
  SELECT Pkg_Impex.getSchCodePk('al_sadve','adve_type') INTO v_serious_type
  FROM dual;

 -- get total Patients with Serious AEs

  SELECT COUNT(DISTINCT fk_per) adcount INTO v_sae_pat_count
  FROM  sch_adverseve
  WHERE fk_study =  v_study AND
  fk_per IN (SELECT DISTINCT fk_per FROM ER_PATPROT, ER_PER
	   WHERE pk_per = fk_per AND fk_site IN (SELECT COLUMN_VALUE FROM TABLE(tab_org)) AND fk_study = v_study AND patprot_stat = 1) AND
  (ae_stdate BETWEEN p_startdate AND p_enddate ) AND
  (ae_stdate BETWEEN NVL(Pkg_Util.f_get_min_patstudystatdate(v_study,fk_per,'active'), p_startdate) AND (NVL(Pkg_Util.f_get_max_patstudystatdate(v_study,fk_per,'offstudy'),p_enddate) + 30) ) AND
  AE_TOXICITY IS NOT NULL AND fk_codlst_aetype = v_serious_type;

 -- get total Adverse events and Serious AE counts

  SELECT COUNT(fk_per) adcount INTO v_all_ae_count
  FROM  sch_adverseve
  WHERE fk_study =  v_study AND
  fk_per IN (SELECT DISTINCT fk_per FROM ER_PATPROT, ER_PER
	   WHERE pk_per = fk_per AND fk_site IN (SELECT COLUMN_VALUE FROM TABLE(tab_org)) AND fk_study = v_study AND patprot_stat = 1) AND
  (ae_stdate BETWEEN p_startdate AND p_enddate ) AND
  (ae_stdate BETWEEN NVL(Pkg_Util.f_get_min_patstudystatdate(v_study,fk_per,'active'), p_startdate) AND (NVL(Pkg_Util.f_get_max_patstudystatdate(v_study,fk_per,'offstudy'),p_enddate) + 30) ) AND
  AE_TOXICITY IS NOT NULL ;


	SELECT NVL(study_advlkp_ver,0)
	INTO v_lkp_view FROM ER_STUDY WHERE pk_study = v_study;

	IF  v_lkp_view > 0 THEN
		BEGIN
			SELECT lkpview_name
			INTO v_lkpview_name FROM ER_LKPVIEW WHERE pk_lkpview = v_lkp_view;
		EXCEPTION WHEN NO_DATA_FOUND THEN
			v_lkpview_name := 'None';
		END;
	ELSE
	  	   v_lkpview_name := 'None';
	END IF;


   v_temp_str := '';
   v_temp_str :=  '<ROW num="'||v_counter||'">';

   v_counter:=v_counter+1;

      --KM--to fix the Bug.2658
   v_studytitle := Pkg_Util.f_escapespecialcharsforxml(v_studytitle);



   v_temp_str := v_temp_str || '<STUDY_NUM>' || v_studynum ||  '</STUDY_NUM><STUDY_TITLE>' || v_studytitle ||
   '</STUDY_TITLE><SITE>' ||v_site || '</SITE><ADVALL_PATCOUNT>'|| v_allae_pat_count ||'</ADVALL_PATCOUNT><ADVS_PATCOUNT>'||
   v_sae_pat_count ||'</ADVS_PATCOUNT><ADVALL_COUNT>'|| v_all_ae_count ||'</ADVALL_COUNT>' ||
   '<TOXICITY_SCALE>'|| v_lkpview_name||'</TOXICITY_SCALE><TOTAL_ENR_PAT>'|| v_toxicity_total ||'</TOTAL_ENR_PAT><TXS>';

--	dbms_lob.writeappend( v_xml, length(v_temp_str), v_temp_str );
	v_xml := v_xml || v_temp_str;

 --get all txArms for the study

 v_temp_str := '';

 V_ARM_SET      := Types.SMALL_STRING_ARRAY ();
  v_ct := 0;
 FOR txA IN (SELECT tx_name , NVL(tx_desc,'&#xa0;' ) tx_desc  FROM ER_STUDYTXARM WHERE fk_study = v_study)
 LOOP

    v_temp_str := v_temp_str  || '<STUDYTX  txdesc="' || txA.tx_desc  ||  '"  >' || txA.tx_name  ||  '</STUDYTX>' ;

	v_ct  := v_ct + 1;
    V_ARM_SET.EXTEND;
    V_ARM_SET (v_ct) := txA.tx_name ;

 END LOOP;


 v_temp_str:= v_temp_str ||  '</TXS><TOXICITIES>' ;
-- dbms_lob.writeappend( v_xml, length(v_temp_str), v_temp_str );
	v_xml := v_xml || v_temp_str;

V_LOOP_SET.EXTEND;
		  V_LOOP_SET(1) := 'al_sadve'; -- for serious adverse events

	V_LOOP_SET.EXTEND;
   V_LOOP_SET(2) := 'ALL' ;-- for all adverse events

   FOR cd IN 1..V_LOOP_SET.COUNT -- loop for adverse event code settings
       LOOP

				FOR i IN (SELECT REPLACE(REPLACE(AE_TOXICITY, '<', '&lt;'),'>','&gt;') as AE_TOXICITY, SUM(grade0) AS grade0,SUM(grade1) AS grade1,SUM(grade2) AS grade2,SUM(grade3) AS grade3,SUM(grade4) AS grade4,SUM(grade5) AS grade5 FROM
				(
				SELECT AE_TOXICITY,DECODE(ae_grade,0,cnt,0) AS grade0,DECODE(ae_grade,1,cnt,0) AS grade1,DECODE(ae_grade,2,cnt,0) AS grade2,DECODE(ae_grade,3,cnt,0) AS grade3,DECODE(ae_grade,4,cnt,0) AS grade4,DECODE(ae_grade,5,cnt,0) AS grade5
				FROM (
				SELECT AE_TOXICITY,ae_grade,COUNT(fk_per) AS cnt
				FROM sch_adverseve, sch_codelst
				WHERE fk_study =  v_study AND
				fk_per IN (SELECT DISTINCT fk_per FROM ER_PATPROT, ER_PER
					   WHERE pk_per = fk_per AND fk_site IN (SELECT COLUMN_VALUE FROM TABLE(tab_org)) AND fk_study = v_study AND patprot_stat = 1) AND
				(ae_stdate BETWEEN p_startdate AND p_enddate ) AND
				(ae_stdate BETWEEN NVL(Pkg_Util.f_get_min_patstudystatdate(v_study,fk_per,'active'), p_startdate) AND (NVL(Pkg_Util.f_get_max_patstudystatdate(v_study,fk_per,'offstudy'),p_enddate) + 30) ) AND
				AE_TOXICITY IS NOT NULL AND
				fk_codlst_aetype = pk_codelst AND codelst_subtyp IN (SELECT COLUMN_VALUE FROM TABLE(Pkg_Adv. f_get_adv_subtypes( V_LOOP_SET(cd)) ) )
				GROUP BY  AE_TOXICITY,ae_grade)
				)
				GROUP BY AE_TOXICITY
				ORDER BY AE_TOXICITY
				)
				LOOP

			 -- loop to find out tx arms for the adv

			v_temp_str := '';

			 v_armxml := ' ';
			 v_ct := 0;
			 V_ADV_ARMNAME    := Types.SMALL_STRING_ARRAY ();
			 V_ADV_ARMXML       := Types.SMALL_STRING_ARRAY ();


			 FOR adv IN (

			 SELECT  AE_TOXICITY, SUM(grade0) AS grade0,SUM(grade1) AS grade1,SUM(grade2) AS grade2,SUM(grade3) AS grade3,SUM(grade4) AS grade4,SUM(grade5) AS grade5, pk_studytxarm ,tx_name FROM (
			SELECT AE_TOXICITY,DECODE(ae_grade,0,cnt,0) AS grade0,DECODE(ae_grade,1,cnt,0) AS grade1,DECODE(ae_grade,2,cnt,0) AS grade2,DECODE(ae_grade,3,cnt,0) AS grade3,DECODE(ae_grade,4,cnt,0) AS grade4,DECODE(ae_grade,5,cnt,0) AS grade5 , pk_studytxarm ,tx_name ,ae_grade
			FROM (
			SELECT DISTINCT AE_TOXICITY,ae_grade,COUNT(fk_per) AS cnt,pk_studytxarm ,tx_name
			FROM sch_adverseve, sch_codelst, ER_STUDYTXARM
			WHERE sch_adverseve.fk_study =  v_study  AND
			fk_per IN (SELECT DISTINCT fk_per FROM ER_PATPROT, ER_PER WHERE pk_per = fk_per AND fk_study = v_study AND
				   patprot_stat = 1    AND fk_site IN (SELECT COLUMN_VALUE FROM TABLE(tab_org)) ) AND
			  AE_TOXICITY IS NOT NULL AND AE_TOXICITY =  i.AE_TOXICITY  AND
			  (ae_stdate BETWEEN NVL(Pkg_Util.f_get_min_patstudystatdate(v_study,fk_per,'active'), p_startdate) AND (NVL(Pkg_Util.f_get_max_patstudystatdate(v_study,fk_per,'offstudy'),p_enddate) + 30) ) AND
			  		fk_codlst_aetype = pk_codelst AND
					codelst_subtyp IN (SELECT COLUMN_VALUE FROM TABLE(Pkg_Adv. f_get_adv_subtypes( V_LOOP_SET(cd)) ) )
					AND pk_studytxarm  IN
			( SELECT DISTINCT fk_studytxarm FROM ER_PATPROT P , ER_PATTXARM A, ER_STUDYTXARM st
			WHERE P.fk_per = sch_adverseve.fk_per  AND P.fk_study =v_study  AND ae_stdate >= a.tx_start_date AND fk_studytxarm = pk_studytxarm AND st.fk_study =P.fk_study AND
			P.pk_patprot = a.fk_patprot AND ((ae_stdate <= a.tx_end_date OR a.tx_end_date  IS NULL) ) )
			 GROUP BY AE_TOXICITY,ae_grade,pk_studytxarm,tx_name)
			)
			GROUP BY AE_TOXICITY,pk_studytxarm ,tx_name
			ORDER BY AE_TOXICITY,pk_studytxarm,tx_name
			 ----------------------
			     )
			 LOOP

			  -------------------- calculate percentage for TX arms
			  	v_tper_grade0  :=   NVL(adv.grade0,0);
			  	v_tper_grade1  :=   NVL(adv.grade1,0);
			  	v_tper_grade2  :=   NVL(adv.grade2,0);
			  	v_tper_grade3  :=   NVL(adv.grade3,0);
			  	v_tper_grade4  :=   NVL(adv.grade4,0);
			  	v_tper_grade5  :=   NVL(adv.grade5,0);

			  	    IF v_all_ae_count > 0 THEN
						 IF (v_tper_grade0  > 0) THEN
						 	v_tper_grade0   := TRUNC ((v_tper_grade0/ v_all_ae_count) * 100 ,2);
						 END IF;
						 IF (v_tper_grade1  > 0) THEN
						 	v_tper_grade1  := TRUNC ((v_tper_grade1  / v_all_ae_count) * 100,2);
						 END IF;
				 		 IF (v_tper_grade2  > 0) THEN
						 	v_tper_grade2  := TRUNC ((v_tper_grade2 / v_all_ae_count) * 100,2);
						 END IF;
				 		 IF (v_tper_grade3  > 0) THEN
						 	v_tper_grade3  := TRUNC ((v_tper_grade3  / v_all_ae_count) * 100,2);
						 END IF;
						 IF (v_tper_grade4  > 0) THEN
						 	v_tper_grade4  := TRUNC ((v_tper_grade4  / v_all_ae_count) * 100,2);
						 END IF;
				 		 IF (v_tper_grade5  > 0) THEN
						 	v_tper_grade5  := TRUNC ((v_tper_grade5 / v_all_ae_count) * 100,2);
						 END IF;
					END IF;


			  -------------------- end of calculate percentage of TX_ARM
			  V_TPER_ELEMENTS := '<GRADE0_TPER>('|| v_tper_grade0 ||'%)</GRADE0_TPER><GRADE1_TPER>('|| v_tper_grade1 ||'%)</GRADE1_TPER><GRADE2_TPER>('|| v_tper_grade2 ||
			  '%)</GRADE2_TPER><GRADE3_TPER>('|| v_tper_grade3 ||'%)</GRADE3_TPER><GRADE4_TPER>('|| v_tper_grade4 ||'%)</GRADE4_TPER><GRADE5_TPER>('|| v_tper_grade5 ||
			  '%)</GRADE5_TPER>';

			   v_temp_str :=  '<TXNAME name="'||  adv.tx_NAME||'"><name>' || adv.tx_NAME || '</name><GRADE1>' || TO_CHAR(adv.GRADE1) || '</GRADE1><GRADE2>' || TO_CHAR(adv.GRADE2) ||
			    '</GRADE2><GRADE3>' || TO_CHAR(adv.GRADE3) || '</GRADE3><GRADE4>' || TO_CHAR(adv.GRADE4) || '</GRADE4><GRADE5>' ||
				 TO_CHAR(adv.GRADE5) || '</GRADE5>'||  V_TPER_ELEMENTS ||'</TXNAME>';



			 	v_ct  := v_ct + 1;
			    V_ADV_ARMNAME .EXTEND ;
			    V_ADV_ARMNAME (v_ct) := adv.tx_NAME ;

			    V_ADV_ARMXML.EXTEND ;
			    V_ADV_ARMXML (v_ct) :=   v_temp_str  ;

			 END LOOP;
			  Plog.DEBUG(pCTX,'v_arm count'||v_ARM_SET.COUNT);
			 --run through main arm set array
				 v_temp_str:= '';
				 v_armxml:= ' ';

			       FOR s IN 1..V_ARM_SET.COUNT --
			                LOOP
					     	  bfound := FALSE;
							  v_arm := V_ARM_SET(s);
							  Plog.DEBUG(pCTX,'v_arm'||v_arm);

									  IF  V_ADV_ARMNAME.COUNT > 0 THEN

											         FOR n IN 1..V_ADV_ARMNAME.COUNT --for all arm name for adv event
					                                 LOOP --
							     	              	 	  	  IF v_arm =  V_ADV_ARMNAME(n) THEN

															  	  v_armxml  :=  v_armxml || V_ADV_ARMXML(n);
																  bfound := TRUE;
															  EXIT;
															  END IF;

												  END LOOP; -- arm name for adv

												 IF ( bfound = FALSE) THEN
												 				 v_armxml  :=  v_armxml ||  '<TXNAME name="'||  v_arm||'"><name>' || v_arm  || '</name><GRADE1>0</GRADE1><GRADE2>0</GRADE2><GRADE3>0</GRADE3><GRADE4>0</GRADE4><GRADE5>0</GRADE5></TXNAME>';
												 END IF;
									ELSE -- no arm count
													 		 v_armxml :=  v_armxml || '<TXNAME  name="'||  v_arm||'"><name>' || v_arm  || '</name><GRADE1>0</GRADE1><GRADE2>0</GRADE2><GRADE3>0</GRADE3><GRADE4>0</GRADE4><GRADE5>0</GRADE5></TXNAME>';

						END IF;

			  END LOOP;

			  	-- 04/17/06, get data for percentage

				BEGIN
				SELECT SUM(grade0) AS grade0,SUM(grade1) AS grade1,SUM(grade2) AS grade2,
				SUM(grade3) AS grade3,SUM(grade4) AS grade4,SUM(grade5) AS grade5
				INTO v_per_grade0,v_per_grade1,v_per_grade2,v_per_grade3,v_per_grade4,v_per_grade5
				FROM ( SELECT AE_TOXICITY,DECODE(ae_grade,0,cnt,0) AS grade0,DECODE(ae_grade,1,cnt,0) AS grade1,DECODE(ae_grade,2,cnt,0) AS grade2,DECODE(ae_grade,3,cnt,0) AS grade3,DECODE(ae_grade,4,cnt,0) AS grade4,
				DECODE(ae_grade,5,cnt,0) AS grade5 FROM (
					 SELECT  AE_TOXICITY,ae_grade,COUNT(fk_per) AS cnt
					 FROM sch_adverseve, sch_codelst
					 WHERE fk_study =  v_study AND AE_TOXICITY =  i.AE_TOXICITY  AND 	 fk_codlst_aetype = pk_codelst AND
					 codelst_subtyp IN (SELECT COLUMN_VALUE FROM TABLE(Pkg_Adv. f_get_adv_subtypes( V_LOOP_SET(cd)) ) )  AND
					 fk_per IN (SELECT DISTINCT fk_per FROM ER_PATPROT, ER_PER WHERE pk_per = fk_per
					 AND fk_study = v_study AND patprot_stat = 1
					 AND fk_site IN (SELECT COLUMN_VALUE FROM TABLE(tab_org)) ) AND
					 ae_stdate BETWEEN p_startdate AND p_enddate AND
					 (ae_stdate BETWEEN NVL(Pkg_Util.f_get_min_patstudystatdate(v_study,fk_per,'active'), p_startdate) AND (NVL(Pkg_Util.f_get_max_patstudystatdate(v_study,fk_per,'offstudy'),p_enddate) + 30) ) AND
					 AE_TOXICITY IS NOT NULL
					 GROUP BY  AE_TOXICITY,ae_grade)
					 )
					 GROUP BY  AE_TOXICITY
					 ORDER BY AE_TOXICITY;

					 --v_toxicity_total := v_per_grade0 + v_per_grade1 + v_per_grade2 + v_per_grade3 + v_per_grade4 + v_per_grade5 ;


					 IF v_all_ae_count > 0 THEN
						 IF (v_per_grade0 > 0) THEN
						 	v_per_grade0 := TRUNC ((v_per_grade0 / v_all_ae_count) * 100 ,2);
						 END IF;
						 IF (v_per_grade1 > 0) THEN
						 	v_per_grade1 := TRUNC ((v_per_grade1 / v_all_ae_count) * 100,2);
						 END IF;
				 		 IF (v_per_grade2 > 0) THEN
						 	v_per_grade2 := TRUNC ((v_per_grade2 / v_all_ae_count) * 100,2);
						 END IF;
				 		 IF (v_per_grade3 > 0) THEN
						 	v_per_grade3 := TRUNC ((v_per_grade3 / v_all_ae_count) * 100,2);
						 END IF;
						 IF (v_per_grade4 > 0) THEN
						 	v_per_grade4 := TRUNC ((v_per_grade4 / v_all_ae_count) * 100,2);
						 END IF;
				 		 IF (v_per_grade5 > 0) THEN
						 	v_per_grade5 := TRUNC ((v_per_grade5 / v_all_ae_count) * 100,2);
						 END IF;
					END IF;
				EXCEPTION WHEN OTHERS THEN
					v_toxicity_total := 0;
					v_per_grade0 := 0; v_per_grade1 := 0; v_per_grade2 := 0; v_per_grade3 := 0; v_per_grade4 := 0; v_per_grade5 := 0;

				END;
				----------------------
				--PERCENTAGE ELEMENTS
				--
				V_PER_ELEMENTS := '<GRADE0_PER>('|| v_per_grade0 ||'%)</GRADE0_PER><GRADE1_PER>('|| v_per_grade1 ||'%)</GRADE1_PER><GRADE2_PER>('|| v_per_grade2 ||'%)</GRADE2_PER><GRADE3_PER>('|| v_per_grade3 ||'%)</GRADE3_PER><GRADE4_PER>('|| v_per_grade4 ||'%)</GRADE4_PER><GRADE5_PER>('|| v_per_grade5 ||'%)</GRADE5_PER>';

				IF V_LOOP_SET(cd) = 'al_sadve' THEN
					  v_temp_Str := '<SAE><TYPE>' || V_LOOP_SET(cd)  || '</TYPE><NAME>' || i.AE_TOXICITY || '</NAME><GRADE0>' || TO_CHAR(i.GRADE0) || '</GRADE0><GRADE1>' || TO_CHAR(i.GRADE1) || '</GRADE1><GRADE2>' || TO_CHAR(i.GRADE2) || '</GRADE2><GRADE3>' || TO_CHAR(i.GRADE3) || '</GRADE3><GRADE4>' || TO_CHAR(i.GRADE4) || '</GRADE4><GRADE5>' || TO_CHAR(i.GRADE5) || '</GRADE5>'|| V_PER_ELEMENTS ;

--			 		   dbms_lob.writeappend( v_xml, length(v_temp_Str), v_temp_Str );
--					   dbms_lob.WRITEAPPEND (  v_xml, length(v_armxml),v_armxml);
						v_xml := v_xml || v_temp_str;
						v_xml := v_xml || v_armxml;
					    v_temp_Str := '</SAE>';
--			 		   dbms_lob.writeappend( v_xml, length(v_temp_Str), v_temp_Str );
						v_xml := v_xml || v_temp_str;

					 ELSE
					 -- add all aes to <AE>
					  v_temp_Str :=  '<AE><TYPE>' || V_LOOP_SET(cd) || '</TYPE><NAME>' || i.AE_TOXICITY || '</NAME><GRADE0>' || TO_CHAR(i.GRADE0) || '</GRADE0><GRADE1>' || TO_CHAR(i.GRADE1) || '</GRADE1><GRADE2>' || TO_CHAR(i.GRADE2) || '</GRADE2><GRADE3>' || TO_CHAR(i.GRADE3) || '</GRADE3><GRADE4>' || TO_CHAR(i.GRADE4) || '</GRADE4><GRADE5>' || TO_CHAR(i.GRADE5) || '</GRADE5>'|| V_PER_ELEMENTS ;
--					  dbms_lob.writeappend( v_xml, length(v_temp_Str), v_temp_Str );
--					  dbms_lob.WRITEAPPEND (  v_xml, length(v_armxml),v_armxml);
						v_xml := v_xml || v_temp_str;
						v_xml := v_xml || v_armxml;

					   v_temp_Str := '</AE>';
--					 dbms_lob.writeappend( v_xml, length(v_temp_Str), v_temp_Str );
					v_xml := v_xml || v_temp_str;

				END IF;




			END LOOP;
			END LOOP; -- for loopset

			v_temp_Str :=  '</TOXICITIES></ROW>';
--			dbms_lob.writeappend( v_xml, length(v_temp_Str), v_temp_Str );
			v_xml := v_xml || v_temp_str;


END LOOP; -- for study

	  v_temp_Str :=  '</ROWSET>';
--	  dbms_lob.writeappend( v_xml, length(v_temp_Str), v_temp_Str );
		v_xml := v_xml || v_temp_str;

--plog.DEBUG(pCTX,'final-'||v_xml);
RETURN 	   v_xml;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,374,2,'02_F_Create_Aexml.sql',sysdate,'v10 #775');

commit;
