/////*********This readMe is specific to v10 build #775	  **********////

Vtiger  Ticket Resolutions that  are delivered to UCSD as  a patch and now  integrated in Build:
 1. 38132  ---  Latency when trying to navigate to ad-hoc query page                                                                                                     
 2. 33480  ---  Form activation latency                                                                                                                                                 
 3. 37866  ---  V10_Build768_QA02: Latency Issues with opening form library                                                                                        
 4. 33537  ---  User activation latency                                                                                                                                                                     
 ------ For the above tickets New  indexes are created . Some existing indexes are also  dropped 

 5.38389     V10_Build774_QA02_'Sequence_Events'_window_doesnt display correctly in IE11                                                       
    Screen size(Dailog box) reduced in IE 11,  observed on Specific version of IE11.0.32. 

 6.38387  ---   V10_Build774_QA02_LoginScreen_IE11                                                                                                                                   
     It is  observed on Specific version of IE11.0.32.Here in some of the minor versions of IE11.0.32 , this bug is replicated.

 7.37753 --- V10_build768_QA02: Time Zone mandatory field is missing in the patient demographics                                           
    Bug Zilla No: 24464. When we configured the  time zone field  from database , then we are able to replicate this issue. 

 8.37985 --- V10_Build768_QA02_Study Filter in Specimen Browser does not display study number when using eSample.    
    Bug Zilla No: 24651

9. 38384 : ---  "Getting inconsistent Rows Per Page results when we do an Advanced Study Search while using the same search criteria"  
     No bug no assigned to this issue.

10 38437:  --  Add Multiple Events to Visits function. 
     Bug Zilla No: 24477  reopened. 



	
	




