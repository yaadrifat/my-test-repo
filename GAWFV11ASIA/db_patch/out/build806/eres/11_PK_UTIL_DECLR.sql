SET DEFINE OFF;
create or replace
PACKAGE      "PKG_UTIL" IS
/******************************************************************************
   NAME:       PKG_UTIL
   PURPOSE:    Contains common functions and procedures
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        5/21/2004   1.Sonia Sahni    Created this package., f_getValueFromDelimitedString
   1.1        5/24/2004   1.Sonia Sahni    f_getControlSetting
   1.2        08/04/2004  1. Sonia Sahni  Changed package name from pkg_erescommon to pkg_common
   1.2.1   08/04/2004   Vishal Abrol   Changed Name to pkg_util from pkg_common
   1.3        08/00/04     Sonia Sahni     Added function f_getCodePk
   1.4       09/27/04    Sonia Sahni added function f_escapeSpecialChars
    1.5       09/29/04    Sonia Sahni added function f_escapeSpecialCharsForXML
    1.6       10/01/04    Sonia Sahni added function f_unescapeSpecialCharsForXML
 1.7        02/08/05   Sonia Abrol  added fn f_checkStudyTeamRight
 1.8     05/10/05    Vishal Abrol Added f_join,f_split and f_compare
 1.9     02/17/06    Sonia Abrol Added date_to_char_with_time
******************************************************************************/
   FUNCTION f_getValueFromDelimitedString ( inCompleteString VARCHAR2, inDelimiterOccurrence NUMBER, inDelimiter VARCHAR2 ) RETURN VARCHAR2;
   FUNCTION f_getControlSetting ( inCtrlKey VARCHAR2 )  RETURN VARCHAR2;
   FUNCTION f_to_number (p_numb VARCHAR2) RETURN NUMBER;
   FUNCTION f_getCodePk (p_codelst_subtype VARCHAR2, p_codetype VARCHAR2) RETURN NUMBER;
   FUNCTION f_checkRight (p_rightstr VARCHAR2,p_modkey VARCHAR2  , p_mod  VARCHAR2) RETURN NUMBER;
      FUNCTION f_escapeSpecialChars(p_string VARCHAR2) RETURN VARCHAR2;
   FUNCTION f_escapeSpecialCharsForXML(p_string VARCHAR2) RETURN VARCHAR2;
   FUNCTION f_escapespecialcharsforxmlClob (p_string clob) RETURN CLOB; 
   FUNCTION f_unescapeSpecialCharsForXML(p_string VARCHAR2) RETURN VARCHAR2;
   FUNCTION f_encode(p_str CLOB) RETURN CLOB;
    FUNCTION f_decode(p_str CLOB) RETURN CLOB;
   FUNCTION f_checkStudyTeamRight(p_study NUMBER,p_user NUMBER, p_modkey VARCHAR2) RETURN NUMBER;
   FUNCTION f_getCodeValues (p_id VARCHAR2,p_sep VARCHAR2,p_res_sep VARCHAR2) RETURN VARCHAR;
   FUNCTION f_get_min_patstudystatdate(p_study NUMBER,p_per NUMBER , p_codesubtype VARCHAR2) RETURN DATE;
   FUNCTION f_get_max_patstudystatdate(p_study NUMBER,p_per NUMBER , p_codesubtype VARCHAR2) RETURN DATE;

   FUNCTION f_JOIN
(
    p_cursor sys_refcursor,
    p_del VARCHAR2 := ','
) RETURN VARCHAR2;
FUNCTION f_SPLIT
(
    p_list VARCHAR2,
    p_del VARCHAR2 :=','
) RETURN split_tbl pipelined;

FUNCTION f_compare(
p_str VARCHAR2
,p_other VARCHAR2
,p_strdlm VARCHAR2:=','
,p_otherdlm VARCHAR2:=','
,p_strtype VARCHAR2:=''
,p_othertype VARCHAR2:=''
) RETURN NUMBER;
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'PKG_UTIL', pLEVEL  => PLOG.LFATAL);
   FUNCTION f_JOIN_CLOB
(
    p_cursor sys_refcursor,
    p_del VARCHAR2 := ','
) RETURN CLOB;
FUNCTION date_to_char(pdate DATE) RETURN VARCHAR2;

/* To convert a date to char, with time part
date: 02/17/06
Author : Sonia Abrol
*/
FUNCTION date_to_char_with_time(pdate DATE) RETURN VARCHAR2;

/* get a list of code subtypes from ids*/

 FUNCTION f_getCodeSubtypes (p_id VARCHAR2,p_sep VARCHAR2,p_res_sep VARCHAR2) RETURN VARCHAR2;

 /* returns a list of code pks (based on code subtypes and code types
 By- Sonia Abrol 03/23/06*/
  FUNCTION f_getMultipleCodePks (p_subtype_str VARCHAR2,p_sep VARCHAR2,p_res_sep VARCHAR2, p_code_type VARCHAR2) RETURN VARCHAR2;
FUNCTION f_get_child_parent_org ( p_organization VARCHAR2)   RETURN VARCHAR2;

 FUNCTION f_getControlSequence ( inCtrlKey VARCHAR2 ,inCtrlValue VARCHAR2)  RETURN NUMBER;

 FUNCTION f_getStudyRight (p_rightstr VARCHAR2,p_seq NUMBER) RETURN NUMBER;

 FUNCTION f_datediff(p_date DATE, p_base_date DATE := SYSDATE,p_inclunit char:='Y') RETURN varchar;

 FUNCTION f_decipher_codelist(p_codeStr varchar2, p_codetype varchar2,p_table varchar2) RETURN varchar;

FUNCTION replaceClob (
srcClob IN CLOB,
replaceStr IN VARCHAR2,
replaceWith IN VARCHAR2)
RETURN CLOB;

END Pkg_Util;
/
 
 
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,405,11,'11_PK_UTIL_DECLR.sql',sysdate,'v11 #806');
commit; 