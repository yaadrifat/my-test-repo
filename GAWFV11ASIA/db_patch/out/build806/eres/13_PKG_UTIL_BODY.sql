SET DEFINE OFF;
create or replace
PACKAGE BODY      Pkg_Util
AS
   FUNCTION f_getvaluefromdelimitedstring (
      incompletestring        VARCHAR2,
      indelimiteroccurrence   NUMBER,
      indelimiter             VARCHAR2
   )
      RETURN VARCHAR2
   IS
      v_value      VARCHAR2 (4000);
      v_pos1       NUMBER;
      v_pos2       NUMBER;
      v_valuelen   NUMBER;
   BEGIN
      -- get the position of the delimter in the string
      v_pos1 :=
              INSTR (incompletestring, indelimiter, 1, indelimiteroccurrence);
      -- add the length of the delimter to the first position
      v_pos1 := v_pos1 + NVL (LENGTH (indelimiter), 0);
      -- get the next position for this delimter
      v_pos2 :=
          INSTR (incompletestring, indelimiter, 1, indelimiteroccurrence + 1);
      v_valuelen := v_pos2 - v_pos1;
      -- extract the value
      v_value := SUBSTR (incompletestring, v_pos1, v_valuelen);
      RETURN v_value;
   END;

   FUNCTION f_getcontrolsetting (inctrlkey VARCHAR2)
      RETURN VARCHAR2
   IS
   BEGIN
      P ('');                                              -- not implemented
   END;

   FUNCTION f_to_number (p_numb VARCHAR2)
      RETURN NUMBER
   AS
      v_numb   NUMBER;
   BEGIN
-- SELECT TO_Number(p_numb) INTO v_numb FROM dual  ;
      v_numb := TO_NUMBER (p_numb);
      RETURN v_numb;
   EXCEPTION
      WHEN OTHERS
      THEN
         v_numb := 0;
         RETURN v_numb;
   END;

/* By Sonia Sahni
   Date - 8/9/04
  returns pk_codelst for a codelst_type and a codelst_subtyp. returns -1 if code not found
  Similar function exists in pkg_impex also. Wll remove it from there later
  */
   FUNCTION f_getcodepk (p_codelst_subtype VARCHAR2, p_codetype VARCHAR2)
      RETURN NUMBER
   IS
      v_pk_code   NUMBER;
   BEGIN
      BEGIN
         SELECT pk_codelst
           INTO v_pk_code
           FROM ER_CODELST
          WHERE TRIM (codelst_type) = TRIM (p_codetype)
            AND TRIM (codelst_subtyp) = TRIM (p_codelst_subtype);

         RETURN v_pk_code;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN -1;
      END;
   END;

/***********************************************************************************************
FUNCTION TO VALIDATE RIGHT flag independent OF which rights u r checking.
PARAMETERS:
 RightString - Actual RIGHT string WITH which you want TO VALIDATE RIGHT
MODULE - whether study_rights,app_rights ..
modulekey - e.g STUDYFORMACC
RETURN
 1 IF THE RIGHT >=4 ELSE returns 0.
*************************************************************************************/
   FUNCTION f_checkright (
      p_rightstr   VARCHAR2,
      p_modkey     VARCHAR2,
      p_mod        VARCHAR2
   )
      RETURN NUMBER
   AS
      vret       NUMBER;
      vsql       VARCHAR2 (250);
      vlen       NUMBER;
      vcounter   NUMBER;
      vvalue     NUMBER;
   BEGIN
      vcounter := 0;
      vsql :=
            'SELECT ctrl_seq FROM ER_CTRLTAB WHERE ctrl_value='''
         || p_modkey
         || ''' AND ctrl_key='''
         || p_mod
         || '''';

      EXECUTE IMMEDIATE vsql
                   INTO vret;

---parse the sequence to get char at 18th position
      vlen := LENGTH (p_rightstr);
      vvalue := TO_NUMBER (SUBSTR (p_rightstr, vret, 1));

      IF vvalue >= 4
      THEN                          --changed by Sonia, 4 is the minimum right
         vret := 1;
      ELSE
         vret := 0;
      END IF;

      plog.fatal (pctx, 'vret :' || vret);
      RETURN vret;
   EXCEPTION
      WHEN OTHERS
      THEN
         plog.fatal (pctx,
                     'Exception occurred in PKG_UTIL.f_checkright :'
                     || SQLERRM
                    );
         vret := 0;
         RETURN vret;
   END;

/***********************************************************************************************
Function to escape following special characters in a string for XSLs:
&
Required for strings that will be used in XSL to avoid parsing errors

p_string String in which special characters needs to be replaced
Returns a string with special characters replaced

 Date : 09/27/04
*************************************************************************************/
   FUNCTION f_escapespecialchars (p_string VARCHAR2)
      RETURN VARCHAR2
   AS
      v_str   VARCHAR2 (4000);
   BEGIN
      v_str := p_string;

      IF TRIM (NVL (p_string, '')) = ''
      THEN
         RETURN v_str;
      END IF;

      v_str := REPLACE (v_str, '&', '&amp;');
      v_str := REPLACE (v_str, '<', '&lt;');
      v_str := REPLACE (v_str, '>', '&gt;');
      v_str := REPLACE (v_str, '"', '\"');
      RETURN v_str;
   END;

     /***********************************************************************************************
   Function to escape following special characters in a string for XML:
   &
    "
    '
    <
    >
    \
   Required for strings that will be used in XML to avoid parsing errors

   p_string String in which special characters needs to be replaced
   Returns a string with special characters replaced

    Date : 09/29/04
   *************************************************************************************/
   FUNCTION f_escapespecialcharsforxml (p_string VARCHAR2)
      RETURN VARCHAR2
   AS
      v_str   VARCHAR2 (4000);
   BEGIN
      v_str := p_string;

      IF TRIM (NVL (p_string, '')) = ''
      THEN
         RETURN v_str;
      END IF;

      v_str := REPLACE (v_str, '&', '&amp;');
      v_str := REPLACE (v_str, '"', '&quot;');
      v_str := REPLACE (v_str, '''', '&apos;');
      v_str := REPLACE (v_str, '<', '&lt;');
      v_str := REPLACE (v_str, '>', '&gt;');
      -- v_str := REPLACE(v_str,'\', '\\');
      RETURN v_str;
   END;


 FUNCTION f_escapespecialcharsforxmlClob (p_string clob) 
      RETURN clob
   AS
      v_str   clob;
   BEGIN
      v_str := p_string;

      IF TRIM (NVL (p_string, '')) = ''
      THEN
         RETURN v_str;
      END IF;

      v_str := REPLACE (v_str, '&', '&amp;');
      v_str := REPLACE (v_str, '"', '&quot;');
      v_str := REPLACE (v_str, '''', '&apos;');
      v_str := REPLACE (v_str, '<', '&lt;');
      v_str := REPLACE (v_str, '>', '&gt;');
      -- v_str := REPLACE(v_str,'\', '\\');
      RETURN v_str;
   END;

     /***********************************************************************************************
   Function to un escape following special characters in a string (that was escaped for XML):
   &
    "
    '
    <
    >
    \

   p_string String in which escaped characters needs to be replaced by respective special characters
   Returns an un escaped string

    Date : 10/01/04
   *************************************************************************************/
   FUNCTION f_unescapespecialcharsforxml (p_string VARCHAR2)
      RETURN VARCHAR2
   AS
      v_str   VARCHAR2 (30000);
   BEGIN
      v_str := p_string;

      IF TRIM (NVL (p_string, '')) = ''
      THEN
         RETURN v_str;
      END IF;

      v_str := REPLACE (v_str, '&quot;', '"');
      v_str := REPLACE (v_str, '&apos;', '''');
      v_str := REPLACE (v_str, '&lt;', '<');
      v_str := REPLACE (v_str, '&gt;', '>');
--     v_str := REPLACE(v_str, '\\','\');
      v_str := REPLACE (v_str, '&amp;', '&');
      RETURN v_str;
   END;

     /*************************************************************
     Function to validate a Study Team right for a user. Checks the right for study team as well as if user
     is a super user. study team rights takes precedence over the super user rights
   PARAMETERS:
    p_study - PK Study
    p_user - Pk User
    modulekey - the study module for which we need to check the right e.g STUDYFORMACC
   RETURN
    1 IF THE RIGHT >=4 ELSE returns 0.
     ************************/
   FUNCTION f_checkstudyteamright (
      p_study    NUMBER,
      p_user     NUMBER,
      p_modkey   VARCHAR2
   )
      RETURN NUMBER
   AS
      v_right   NUMBER := 0;
      v_study_team_rights Varchar2(100);
   BEGIN
      BEGIN
         SELECT f_checkright (study_team_rights, p_modkey, 'study_rights')
           INTO v_right
           FROM ER_STUDYTEAM
          WHERE fk_study = p_study
            AND fk_user = p_user
            AND NVL (study_team_usr_type, 'D') = 'D';

         IF v_right >= 0
         THEN
            RETURN v_right;
         ELSE
            v_right := -1;
         END IF;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_right := -1;
      END;

      IF v_right = -1
      THEN
          -- the user is not part of the study team, check if its a super user
         v_right := 0;

         BEGIN

             select nvl(grp_supusr_rights,'')
             into v_study_team_rights
             from er_grps, er_user
             where pk_user = p_user and fk_grp_default = pk_grp and grp_supusr_flag = 1;

            SELECT f_checkright (v_study_team_rights, p_modkey, 'study_rights')
              INTO v_right
              FROM dual
             WHERE  pkg_superuser.F_Is_Superuser(p_user, p_study) = 1 ;

         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN                                       -- NOT A SUPER USER TOO
               v_right := 0;
         END;

         RETURN v_right;
      END IF;                                            --end of v_right = -1
   END;

/**
  f_get_min_patstudystatdate to get the minimum patient study status date for a status subtype
  */
   FUNCTION f_get_min_patstudystatdate (
      p_study         NUMBER,
      p_per           NUMBER,
      p_codesubtype   VARCHAR2
   )
      RETURN DATE
   AS
      v_date   DATE;
   BEGIN
      BEGIN
         SELECT MIN (patstudystat_date)
           INTO v_date
           FROM ER_PATSTUDYSTAT
          WHERE fk_per = p_per
            AND fk_study = p_study
            AND fk_codelst_stat = f_getcodepk (p_codesubtype, 'patStatus');

         v_date := TRUNC (v_date);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN NULL;
      END;

      RETURN v_date;
   END;

      /*************************************************************
     Function to deocde Code values from concatenated primary keys to Code Values
   PARAMETERS:
    p_id - Codelist PK String
    p_sep - PK String ID seperator
     p_res_sep  - Seperator to use between code values
   RETURN
   Decoded string from the primary keys
   Author : Vishal abrol
     ****************************************************************************************************/
   FUNCTION f_getcodevalues (p_id VARCHAR2, p_sep VARCHAR2, p_res_sep VARCHAR2)
      RETURN VARCHAR
   IS
      v_str       VARCHAR2 (2001) DEFAULT p_id || p_sep;
      v_sqlstr    VARCHAR2 (4000);
      v_params    VARCHAR2 (2001) DEFAULT p_id || p_sep;
      v_pos       NUMBER          := 0;
      v_cnt       NUMBER          := 0;
      v_namelst   VARCHAR2 (2000);
      v_name      VARCHAR2 (250);
   BEGIN
      LOOP
         v_cnt := v_cnt + 1;
         v_pos := INSTR (v_str, p_sep);
         v_params := SUBSTR (v_str, 1, v_pos - 1);
         EXIT WHEN v_params IS NULL;

         SELECT codelst_desc
           INTO v_name
           FROM ER_CODELST
          WHERE pk_codelst = v_params;

         IF v_namelst IS NULL
         THEN
            v_namelst := v_name;
         ELSE
            v_namelst := v_namelst || p_res_sep || v_name;
         END IF;

         v_str := SUBSTR (v_str, v_pos + 1);
         DBMS_OUTPUT.PUT_LINE ('v_namelst = ' || v_namelst);
      END LOOP;

      DBMS_OUTPUT.PUT_LINE (TO_CHAR (LENGTH (v_namelst)));
      RETURN v_namelst;
   END;

     /**********************************************************************************************
    Function to join values with provided delimiter
   PARAMETERS:
    p_cursor - Cursor containing the values
    p_del - delimiter to use -Default is ','
   RETURN
   Delimited String delimited with provided delimiter
   *****************************************************************
   USAGE: This SQL will return:
   SQL> select join(cursor(select ename from emp),',') from dual;
   SMITH,ALLEN,WARD,JONES,MARTIN,BLAKE,CLARK,SCOTT,KING,TURNER,ADAMS,
   JAMES,FORD,MILLER
   ********************************************************************
   Author : Vishal abrol
    ****************************************************************************************************/
   FUNCTION f_join (p_cursor sys_refcursor, p_del VARCHAR2 := ',')
      RETURN VARCHAR2
   IS
      l_value    VARCHAR2 (32767);
      l_result   VARCHAR2 (32767);
   BEGIN
      LOOP
         FETCH p_cursor
          INTO l_value;

         EXIT WHEN p_cursor%NOTFOUND;

         IF l_result IS NOT NULL
         THEN
            l_result := l_result || p_del;
         END IF;

         l_result := l_result || l_value;
      END LOOP;

      CLOSE p_cursor;

      RETURN l_result;
   END f_join;

    /**********************************************************************************************
    Function to Split delimited String
   PARAMETERS:
    p_list - Delimited String
    p_del - delimiter  Default is ','
   RETURN
   pipeline split_tbl with resultant values
   *****************************************************************
   USAGE: This SQL will return:
   SQL> select * from table(split('one,two,three',','));
   one
   two
   three
   ********************************************************************
   Author : Vishal abrol
    ****************************************************************************************************/
   FUNCTION f_split (p_list VARCHAR2, p_del VARCHAR2 := ',')
      RETURN split_tbl PIPELINED
   IS
      l_idx     PLS_INTEGER;
      l_list    VARCHAR2 (32767) := p_list;
      l_value   VARCHAR2 (32767);
   BEGIN
      LOOP
         l_idx := INSTR (l_list, p_del);

         IF l_idx > 0
         THEN
            PIPE ROW (SUBSTR (l_list, 1, l_idx - 1));
            l_list := SUBSTR (l_list, l_idx + LENGTH (p_del));
         ELSE
            PIPE ROW (l_list);
            EXIT;
         END IF;
      END LOOP;

      RETURN;
   END f_split;

/***********************************************************************************************************************
Method to compare delimited Strings as like operator. First string is
splitted and each splitted value is compared using IN clause with
the other Splitted string values.
Same way other operator are added and can be extended.
p_str - First comparison String e.g, 1,2,3 or select emp_name from emp- SQL SHOULD RETURN ONE VALUE AND ROW
p_other- second comparison String e.g 4,5,6 or SQL SHOULD RETURN ONE VALUE AND ROW
p_strdlm - delimiter for the first string, default is ','
p_otherdlm- delimiter for second string, default is ','
p_strtype - type for first String, 'SQL' if a SQL Statement
p_othertype- type for second String,'SQL' if a SQL statement

Return:
1 if found - 0 for not found
****************************************************************************************************************************/
   FUNCTION f_compare (
      p_str         VARCHAR2,
      p_other       VARCHAR2,
      p_strdlm      VARCHAR2 := ',',
      p_otherdlm    VARCHAR2 := ',',
      p_strtype     VARCHAR2 := '',
      p_othertype   VARCHAR2 := ''
   )
      RETURN NUMBER
   IS
      v_cursor       sys_refcursor;
      v_strvalue     VARCHAR2 (32767);
      v_othervalue   VARCHAR2 (32767);
      v_count        NUMBER;
   BEGIN
      IF p_strtype = 'SQL'
      THEN
         EXECUTE IMMEDIATE p_str
                      INTO v_strvalue;
      ELSE
         v_strvalue := p_str;
      END IF;

      IF p_othertype = 'SQL'
      THEN
         EXECUTE IMMEDIATE p_other
                      INTO v_othervalue;
      ELSE
         v_othervalue := p_other;
      END IF;

      FOR i IN (SELECT COLUMN_VALUE
                  FROM TABLE (Pkg_Util.f_split (v_strvalue, p_strdlm)))
      LOOP
         SELECT COUNT (*)
           INTO v_count
           FROM DUAL
          WHERE i.COLUMN_VALUE IN (
                      SELECT COLUMN_VALUE
                        FROM TABLE (Pkg_Util.f_split (v_othervalue,
                                                      p_otherdlm)
                                   ));

         IF v_count > 0
         THEN
            RETURN 1;
         END IF;
      END LOOP;

      RETURN 0;
   END f_compare;

   FUNCTION date_to_char (pdate DATE)
      RETURN VARCHAR2
   IS
      v_date   VARCHAR2 (20);
   BEGIN
      BEGIN
         SELECT TO_CHAR (pdate, PKG_DATEUTIL.F_GET_DATEFORMAT)
           INTO v_date
           FROM DUAL;

         RETURN v_date;
      EXCEPTION
         WHEN OTHERS
         THEN
            RETURN '';
      END;
   END;

/************************************************************************************************************
FUNCTION TO encode special characters WITH Velos Internal  Keywords
************************************************************************************************************/
   FUNCTION f_encode (p_str CLOB)
      RETURN CLOB
   AS
      v_str   CLOB;
   BEGIN
      v_str := p_str;

      IF (NVL (DBMS_LOB.getlength (v_str), 0) = 0)
      THEN
         RETURN v_str;
      END IF;

      IF (INSTR (v_str, '''') >= 0)
      THEN
         v_str := REPLACE (v_str, '''', '[VELSQUOTE]');
      END IF;

      RETURN v_str;
   END;

/************************************************************************************************************
FUNCTION TO decode Velos Internal  Keywords with special characters
************************************************************************************************************/
   FUNCTION f_decode (p_str CLOB)
      RETURN CLOB
   AS
      v_str   CLOB;
   BEGIN
      v_str := p_str;

      IF (NVL (DBMS_LOB.getlength (v_str), 0) = 0)
      THEN
         RETURN v_str;
      END IF;

      IF (INSTR (v_str, '[VELSQUOTE]') >= 0)
      THEN
         v_str := REPLACE (v_str, '[VELSQUOTE]', '''');
      END IF;

      RETURN v_str;
   END;

/* To convert a date to char, with time part
date: 02/17/06
Author : Sonia Abrol
*/
   FUNCTION date_to_char_with_time (pdate DATE)
      RETURN VARCHAR2
   IS
      v_date   VARCHAR2 (20);
   BEGIN
      BEGIN
         SELECT TO_CHAR (pdate, PKG_DATEUTIL.f_get_datetimeformat)
           INTO v_date
           FROM DUAL;

         RETURN v_date;
      EXCEPTION
         WHEN OTHERS
         THEN
            RETURN '';
      END;
   END;

   FUNCTION f_join_clob (p_cursor sys_refcursor, p_del VARCHAR2 := ',')
      RETURN CLOB
   IS
      l_value    VARCHAR2 (32767);
      l_result   CLOB;
   BEGIN
      LOOP
         FETCH p_cursor
          INTO l_value;

         EXIT WHEN p_cursor%NOTFOUND;

         IF l_result IS NOT NULL
         THEN
            l_result := l_result || p_del;
         END IF;

         l_result := l_result || l_value;
      END LOOP;

      CLOSE p_cursor;

      RETURN l_result;
   END f_join_clob;

    /*************************************************************
  Function to deocde Code values from concatenated primary keys to Code Subtypes
PARAMETERS:
 p_id - Codelist PK String
 p_sep - PK String ID seperator
  p_res_sep  - Seperator to use between code subtypes
RETURN
Decoded string from the primary keys
Author : Sonia Abrol, 03/23/06
  ****************************************************************************************************/

 FUNCTION f_getCodeSubtypes (p_id VARCHAR2,p_sep VARCHAR2,p_res_sep VARCHAR2)
RETURN VARCHAR2
IS
   V_STR      VARCHAR2 (2001) DEFAULT p_id || p_sep;
   V_SQLSTR   VARCHAR2 (4000);
   V_PARAMS   VARCHAR2 (2001) DEFAULT p_id || p_sep;
   V_POS      NUMBER         := 0;
   V_CNT      NUMBER         := 0;
   v_namelst VARCHAR2(2000) ;
   v_name VARCHAR2(70) ;
BEGIN
   LOOP
       V_CNT := V_CNT + 1;
       V_POS := INSTR (V_STR, p_sep);
       V_PARAMS := SUBSTR (V_STR, 1, V_POS - 1);
        EXIT WHEN V_PARAMS IS NULL;
        SELECT codelst_subtyp
          INTO v_name
         FROM ER_CODELST
        WHERE pk_codelst = v_params ;
        IF v_namelst IS NULL THEN
          v_namelst :=  v_name ;
        ELSE
          v_namelst :=  v_namelst ||p_res_sep || v_name  ;
        END IF ;
      V_STR := SUBSTR (V_STR, V_POS + 1);
     END LOOP ;
  RETURN v_namelst ;
END ;

 FUNCTION f_getMultipleCodePks (p_subtype_str VARCHAR2,p_sep VARCHAR2,p_res_sep VARCHAR2, p_code_type VARCHAR2)
RETURN VARCHAR2
IS
   V_STR      VARCHAR2 (2001) DEFAULT p_subtype_str || p_sep;
   V_SQLSTR   VARCHAR2 (4000);
   V_PARAMS   VARCHAR2 (2001) DEFAULT p_subtype_str  || p_sep;
   V_POS      NUMBER         := 0;
   V_CNT      NUMBER         := 0;
   v_lst VARCHAR2(2000) ;
   v_pk  NUMBER ;
BEGIN
   LOOP

       V_CNT := V_CNT + 1;
       V_POS := INSTR (V_STR, p_sep);
       V_PARAMS := SUBSTR (V_STR, 1, V_POS - 1);

        EXIT WHEN V_PARAMS IS NULL;

    SELECT     Pkg_Impex.getCodePk (v_params, p_code_type) INTO v_pk  FROM dual;

    IF v_pk  >  0 THEN
         IF v_lst IS NULL THEN
           v_lst :=  v_pk ;
         ELSE
           v_lst  :=  v_lst  || p_res_sep || v_pk  ;
         END IF ;
  END IF;

     V_STR := SUBSTR (V_STR, V_POS + 1);
     END LOOP ;
  RETURN v_lst ;
END ;

/**
   Sonia Abrol, 04/19/06, get the max date for patient study status
  f_get_max_patstudystatdate to get the minimum patient study status date for a status subtype
  */
   FUNCTION f_get_max_patstudystatdate (
      p_study         NUMBER,
      p_per           NUMBER,
      p_codesubtype   VARCHAR2
   )
      RETURN DATE
   AS
      v_date   DATE;
   BEGIN
      BEGIN
         SELECT MAX (patstudystat_date)
           INTO v_date
           FROM ER_PATSTUDYSTAT
          WHERE fk_per = p_per
            AND fk_study = p_study
            AND fk_codelst_stat = f_getcodepk (p_codesubtype, 'patStatus');

         v_date := TRUNC (v_date);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN NULL;
      END;

      RETURN v_date;
   END;


   FUNCTION f_get_child_parent_org ( p_organization VARCHAR2)
   RETURN VARCHAR2
   AS
   v_parent VARCHAR2(1000);
   v_parent_temp VARCHAR2(1000);
   v_site NUMBER;
   v_organizations VARCHAR2(4000);
   v_organizations_child VARCHAR2(4000);
  -- modified on 08/17/09 for CL13462
   v_sql_childs   VARCHAR2(4000);
  v_count_childs NUMBER;
v_cur_childs  Gk_Cv_Types.GenericCursorType;

   BEGIN


   BEGIN
       v_parent := 0;

           SELECT site_parent INTO v_parent FROM ER_SITE WHERE pk_site = TO_NUMBER ( p_organization);
   EXCEPTION
   WHEN NO_DATA_FOUND THEN
            RETURN p_organization;

   END;

   v_parent_temp := 0;

   LOOP
          BEGIN
                 SELECT site_parent INTO  v_parent_temp FROM ER_SITE WHERE pk_site  = v_parent ;

               EXCEPTION
                           WHEN NO_DATA_FOUND THEN
                           EXIT WHEN 1=1;
        END;
          EXIT WHEN v_parent_temp IS NULL OR v_parent_temp = 0;
           v_parent:= v_parent_temp;
   END LOOP;

IF v_parent = 0  OR v_parent  IS NULL THEN

v_parent := p_organization;

END IF ;

v_organizations :=  v_parent;
v_organizations_child :=  v_parent;

  LOOP

        v_sql_childs := 'Select pk_site from er_site where site_parent in ( ' || v_organizations_child ||  ')';
        v_count_childs := 0 ;
     -- Plog.DEBUG(pCTX,v_sql_childs);
        OPEN v_cur_childs FOR v_sql_childs;
        LOOP
                FETCH v_cur_childs INTO  v_site ;

                EXIT WHEN v_cur_childs%NOTFOUND;

        IF v_count_childs = 0 THEN

           v_organizations_child := v_site;

        ELSE

             v_organizations_child :=  v_organizations_child || ',' || v_site;

        END IF ;

         v_organizations :=  v_organizations  || ',' || v_site;
        v_count_childs  := v_count_childs + 1;
        END LOOP;

        EXIT WHEN v_count_childs = 0;

   END LOOP;

   RETURN v_organizations;
   END;


   FUNCTION f_getControlSequence ( inCtrlKey VARCHAR2 ,inCtrlValue Varchar2)  RETURN Number
   IS
       v_seq Number;
   BEGIN

       begin
           SELECT ctrl_seq
           into v_seq
           FROM ER_CTRLTAB WHERE ctrl_key = inCtrlKey  and ctrl_value=inCtrlValue ;
       exception when NO_DATA_FOUND then
           v_seq := -1;
       end ;

           return v_seq;

   END;

   FUNCTION f_getStudyRight (p_rightstr VARCHAR2,p_seq Number) RETURN NUMBER
   IS

       vlen Number;
       value Number := 0;

   BEGIn

      vlen := LENGTH (p_rightstr);

      value := nvl(TO_NUMBER (SUBSTR (p_rightstr, p_seq, 1)),0);

      return value;

   END;
 /* KLUDGE : Function need to be revised to make it generic and support passing units as parameters.
Due to lack of time, just making it functional : KLUDGE*/
FUNCTION f_datediff(p_date DATE, p_base_date DATE := SYSDATE,p_inclunit char:='Y') RETURN varchar

IS

      v_diff NUMBER(10) := 0;
      v_unit varchar2(10) :='Days';

BEGIN
      --v_diff:=trunc(p_base_date)-trunc(p_date);
      if ((p_date is null) or length(p_date)=0) then
      return '';
      end if;
      v_diff:=TRUNC(MONTHS_BETWEEN(p_base_date,p_date));
      if (v_diff=0) then
      v_diff:=trunc(p_base_date) - trunc (p_date) + 1;
      if (v_diff=0) then
      v_diff:=1;
      end if;
      v_unit:='Days';
      elsif (v_diff>=12)  then
       v_unit:='Years';
       v_diff:=trunc(v_diff/12);
      elsif ( (v_diff>0) or (v_diff<21)) then
      v_unit:='Months';
      end if;
      if (p_inclunit='Y') then
     RETURN to_char(v_diff) || ' ' || v_unit ;
     else
     RETURN to_char(v_diff);
     end if;


END;

    /*************************************************************
  Function to deocde concatenated bits for codelst to Code Subtypes
PARAMETERS:
 p_codeStr - Codelst Bit String e.g 000010 or 01010111
 p_codeType - Code Type to process for these bits
RETURN
Decoded string
Author : Vishal Abrol, 03/23/06

KLUDGE: This is Done for only SCH schema. Another parameter needs to be added to pass  table/schema
  ****************************************************************************************************/

FUNCTION f_decipher_codelist(p_codeStr varchar2, p_codeType varchar2,p_table varchar2) RETURN varchar
IS
     type code_desc_tab is table of sch_codelst.codelst_desc%TYPE index by binary_integer;
     code_desc code_desc_tab;
      counter number:=1;
      s_len number:=0;
      bit number:=-1;
      returnStr varchar2(4000):='';
      v_sql varchar2(2000) :='';
      v_cdesc varchar2(500);
      v_cur Gk_Cv_Types.GenericCursorType;
BEGIN

      if ((p_codeStr is null) or length(p_codeStr)=0) then
      return '';
      end if;
       v_sql := 'select codelst_desc from  '|| p_table ||'  where codelst_type='''|| p_codeType||''' order by codelst_seq';
        OPEN v_cur FOR v_sql;
        LOOP
                FETCH v_cur INTO  v_cdesc ;

                EXIT WHEN v_cur%NOTFOUND;

      code_desc(counter):=v_cdesc;
     counter:= counter + 1;
     end loop;

     s_len:=length(p_codeStr);
     for j in 1..s_len
     loop
     bit:=substr(p_codeStr,j,1);
     if (bit='1') then
     if ((length(trim(returnStr))=0) or (returnstr is null)) then
     returnStr:=code_desc(j);
     else
     returnStr:=returnStr||','||code_desc(j);
     end if;
     end if;--end for bit=1
      end loop;
     return returnStr;
END;


FUNCTION replaceClob (
srcClob IN CLOB,
replaceStr IN VARCHAR2,
replaceWith IN VARCHAR2)
RETURN CLOB IS

vBuffer    VARCHAR2 (32767);
l_amount   BINARY_INTEGER := 32767;
l_pos      PLS_INTEGER := 1;
l_clob_len PLS_INTEGER;
newClob    CLOB := EMPTY_CLOB;

BEGIN
  -- initalize the new clob
  dbms_lob.createtemporary(newClob,TRUE);

  l_clob_len := dbms_lob.getlength(srcClob);

  WHILE l_pos < l_clob_len
  LOOP
    dbms_lob.read(srcClob, l_amount, l_pos, vBuffer);

    IF vBuffer IS NOT NULL THEN
      -- replace the text
      vBuffer := replace(vBuffer, replaceStr, replaceWith);
      -- write it to the new clob
      dbms_lob.writeappend(newClob, LENGTH(vBuffer), vBuffer);
    END IF;
    l_pos := l_pos + l_amount;
  END LOOP;

  RETURN newClob;
EXCEPTION
  WHEN OTHERS THEN
    RAISE;
END;

END Pkg_Util;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,405,13,'13_PKG_UTIL_BODY.sql',sysdate,'v11 #806');
commit; 