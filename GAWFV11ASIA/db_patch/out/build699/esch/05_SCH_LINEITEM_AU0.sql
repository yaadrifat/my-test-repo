create or replace
TRIGGER SCH_LINEITEM_AU0
AFTER UPDATE
OF PK_LINEITEM,
FK_BGTSECTION,
LINEITEM_DESC,
LINEITEM_SPONSORUNIT,
LINEITEM_CLINICNOFUNIT,
LINEITEM_OTHERCOST,
LINEITEM_DELFLAG,
RID,
CREATOR,
LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,
CREATED_ON,
IP_ADD,
LINEITEM_NAME,
LINEITEM_NOTES,
LINEITEM_INPERSEC,
LINEITEM_STDCARECOST,
LINEITEM_RESCOST,
LINEITEM_INVCOST,
LINEITEM_INCOSTDISC,
LINEITEM_CPTCODE,
LINEITEM_REPEAT,
LINEITEM_PARENTID,
LINEITEM_APPLYINFUTURE,
FK_CODELST_CATEGORY,
LINEITEM_TMID,
LINEITEM_CDM,
LINEITEM_APPLYINDIRECTS,
LINEITEM_TOTALCOST,
LINEITEM_SPONSORAMOUNT,
LINEITEM_VARIANCE,FK_EVENT,
lineitem_seq,
fk_codelst_cost_type,
SUBCOST_ITEM_FLAG,LINEITEM_APPLYPATIENTCOUNT
ON SCH_LINEITEM REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR(2000);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:new.last_modified_by);
  audit_trail.record_transaction
    (raid, 'SCH_LINEITEM', :OLD.rid, 'U', usr);

  IF NVL(:OLD.pk_lineitem,0) !=
     NVL(:NEW.pk_lineitem,0) THEN
     audit_trail.column_update
       (raid, 'PK_LINEITEM',
       :OLD.pk_lineitem, :NEW.pk_lineitem);
  END IF;
  IF NVL(:OLD.fk_bgtsection,0) !=
     NVL(:NEW.fk_bgtsection,0) THEN
     audit_trail.column_update
       (raid, 'FK_BGTSECTION',
       :OLD.fk_bgtsection, :NEW.fk_bgtsection);
  END IF;
  IF NVL(:OLD.lineitem_desc,' ') !=
     NVL(:NEW.lineitem_desc,' ') THEN
     audit_trail.column_update
       (raid, 'LINEITEM_DESC',
       :OLD.lineitem_desc, :NEW.lineitem_desc);
  END IF;

  IF NVL(:OLD.lineitem_sponsorunit,' ') !=
     NVL(:NEW.lineitem_sponsorunit,' ') THEN
     audit_trail.column_update
       (raid, 'LINEITEM_SPONSORUNIT',
       :OLD.lineitem_sponsorunit, :NEW.lineitem_sponsorunit);
  END IF;

  IF NVL(:OLD.lineitem_clinicnofunit,' ') !=
     NVL(:NEW.lineitem_clinicnofunit,' ') THEN
     audit_trail.column_update
       (raid, 'LINEITEM_CLINICNOFUNIT',
       :OLD.lineitem_clinicnofunit, :NEW.lineitem_clinicnofunit);
  END IF;

  IF NVL(:OLD.lineitem_othercost,' ') !=
     NVL(:NEW.lineitem_othercost,' ') THEN
     audit_trail.column_update
       (raid, 'LINEITEM_OTHERCOST',
       :OLD.lineitem_othercost, :NEW.lineitem_othercost);
  END IF;


  IF NVL(:OLD.lineitem_delflag,' ') !=
     NVL(:NEW.lineitem_delflag,' ') THEN
     audit_trail.column_update
       (raid, 'LINEITEM_DELFLAG',
       :OLD.lineitem_delflag, :NEW.lineitem_delflag);
  END IF;
  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;
  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       :OLD.last_modified_by, :NEW.last_modified_by);
  END IF;
  IF NVL(:OLD.last_modified_date,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
                   to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));

  END IF;
  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;
  IF NVL(:OLD.lineitem_name,' ') !=
     NVL(:NEW.lineitem_name,' ') THEN
     audit_trail.column_update
       (raid, 'LINEITEM_NAME',
       :OLD.lineitem_name, :NEW.lineitem_name);
  END IF;

  IF NVL(:OLD.lineitem_notes,' ') !=
     NVL(:NEW.lineitem_notes,' ') THEN
     audit_trail.column_update
       (raid, 'LINEITEM_NOTES',
       :OLD.lineitem_notes, :NEW.lineitem_notes);
  END IF;
  IF NVL(:OLD.lineitem_inpersec,0) !=
     NVL(:NEW.lineitem_inpersec,0) THEN
     audit_trail.column_update
       (raid, 'LINEITEM_inpersec',
       :OLD.lineitem_inpersec, :NEW.lineitem_inpersec);
  END IF;

  IF NVL(:OLD.lineitem_stdcarecost,0) !=
     NVL(:NEW.lineitem_stdcarecost,0) THEN
     audit_trail.column_update
       (raid, 'LINEITEM_STDCARECOST',
       :OLD.lineitem_stdcarecost, :NEW.lineitem_stdcarecost);
  END IF;
  IF NVL(:OLD.lineitem_rescost,0) !=
     NVL(:NEW.lineitem_rescost,0) THEN
     audit_trail.column_update
       (raid, 'LINEITEM_RESCOST',
       :OLD.lineitem_rescost, :NEW.lineitem_rescost);
  END IF;
  IF NVL(:OLD.lineitem_invcost,0) !=
     NVL(:NEW.lineitem_invcost,0) THEN
     audit_trail.column_update
       (raid, 'LINEITEM_INVCOST',
       :OLD.lineitem_invcost, :NEW.lineitem_invcost);
  END IF;
  IF NVL(:OLD.lineitem_incostdisc,0) !=
     NVL(:NEW.lineitem_incostdisc,0) THEN
     audit_trail.column_update
       (raid, 'LINEITEM_INCOSTDISC',
       :OLD.lineitem_incostdisc, :NEW.lineitem_incostdisc);
  END IF;
  IF NVL(:OLD.lineitem_cptcode,' ') !=
     NVL(:NEW.lineitem_cptcode,' ') THEN
     audit_trail.column_update
       (raid, 'LINEITEM_CPTCODE',
       :OLD.lineitem_cptcode, :NEW.lineitem_cptcode);
  END IF;
  IF NVL(:OLD.lineitem_repeat,0) !=
     NVL(:NEW.lineitem_repeat,0) THEN
     audit_trail.column_update
       (raid, 'LINEITEM_REPEAT',
       :OLD.lineitem_repeat, :NEW.lineitem_repeat);
  END IF;
  IF NVL(:OLD.lineitem_parentid,0) !=
     NVL(:NEW.lineitem_parentid,0) THEN
     audit_trail.column_update
       (raid, 'LINEITEM_PARENTID',
       :OLD.lineitem_parentid, :NEW.lineitem_parentid);
  END IF;
  IF NVL(:OLD.lineitem_applyinfuture,0) !=
     NVL(:NEW.lineitem_applyinfuture,0) THEN
     audit_trail.column_update
       (raid, 'LINEITEM_APPLYINFUTURE',
       :OLD.lineitem_applyinfuture, :NEW.lineitem_applyinfuture);
  END IF;

  ----

  IF NVL(:OLD.fk_codelst_category,0) !=
     NVL(:NEW.fk_codelst_category,0) THEN
     audit_trail.column_update
       (raid, 'FK_CODELST_CATEGORY',
       :OLD.fk_codelst_category, :NEW.fk_codelst_category);
  END IF;

  IF NVL(:OLD.lineitem_tmid,' ') !=
     NVL(:NEW.lineitem_tmid,' ') THEN
     audit_trail.column_update
       (raid, 'LINEITEM_TMID',
       :OLD.lineitem_tmid, :NEW.lineitem_tmid);
  END IF;

  IF NVL(:OLD.lineitem_cdm,' ') !=
     NVL(:NEW.lineitem_cdm,' ') THEN
     audit_trail.column_update
       (raid, 'LINEITEM_CDM',
       :OLD.lineitem_cdm, :NEW.lineitem_cdm);
  END IF;

  IF NVL(:OLD.lineitem_applyindirects,' ') !=
     NVL(:NEW.lineitem_applyindirects,' ') THEN
     audit_trail.column_update
       (raid, 'LINEITEM_APPLYINDIRECTS',
       :OLD.lineitem_applyindirects, :NEW.lineitem_applyindirects);
  END IF;

--JM: 02/16/2007
  IF NVL(:OLD.lineitem_totalcost,0) !=
     NVL(:NEW.lineitem_totalcost,0) THEN
     audit_trail.column_update
       (raid, 'LINEITEM_TOTALCOST',
       :OLD.lineitem_totalcost, :NEW.lineitem_totalcost);
  END IF;
  IF NVL(:OLD.lineitem_sponsoramount,0) !=
     NVL(:NEW.lineitem_sponsoramount,0) THEN
     audit_trail.column_update
       (raid, 'LINEITEM_SPONSORAMOUNT',
       :OLD.lineitem_sponsoramount, :NEW.lineitem_sponsoramount);
  END IF;
  IF NVL(:OLD.lineitem_variance,0) !=
     NVL(:NEW.lineitem_variance,0) THEN
     audit_trail.column_update
       (raid, 'LINEITEM_VARIANCE',
       :OLD.lineitem_variance, :NEW.lineitem_variance);
  END IF;

  IF NVL(:OLD.fk_event,0) !=
     NVL(:NEW.fk_event,0) THEN
     audit_trail.column_update
       (raid, 'FK_EVENT',
       :OLD.fk_event, :NEW.fk_event);
  END IF;

  IF NVL(:OLD.lineitem_seq,0) !=
     NVL(:NEW.lineitem_seq,0) THEN
     audit_trail.column_update
       (raid, 'LINEITEM_SEQ',
       :OLD.lineitem_seq, :NEW.lineitem_seq);
  END IF;

IF NVL(:OLD.fk_codelst_cost_type,0) !=
     NVL(:NEW.fk_codelst_cost_type,0) THEN
     audit_trail.column_update
       (raid, 'FK_CODELST_COST_TYPE',
       :OLD.fk_codelst_cost_type, :NEW.fk_codelst_cost_type);
  END IF;

IF NVL(:OLD.SUBCOST_ITEM_FLAG,0) !=
     NVL(:NEW.SUBCOST_ITEM_FLAG,0) THEN
     audit_trail.column_update
       (raid, 'SUBCOST_ITEM_FLAG',
       :OLD.SUBCOST_ITEM_FLAG, :NEW.SUBCOST_ITEM_FLAG);
END IF;

IF NVL(:OLD.LINEITEM_APPLYPATIENTCOUNT,0) !=
     NVL(:NEW.LINEITEM_APPLYPATIENTCOUNT,0) THEN
     audit_trail.column_update
       (raid, 'LINEITEM_APPLYPATIENTCOUNT',
       :OLD.LINEITEM_APPLYPATIENTCOUNT, :NEW.LINEITEM_APPLYPATIENTCOUNT);
END IF;


END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,298,5,'05_SCH_LINEITEM_AU0.sql',sysdate,'v9.3.0 #699');