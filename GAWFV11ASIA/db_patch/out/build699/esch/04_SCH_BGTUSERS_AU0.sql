create or replace
TRIGGER "ESCH"."SCH_BGTUSERS_AU0" 
AFTER UPDATE OF BGTUSERS_RIGHTS,BGTUSERS_TYPE,CREATED_ON,CREATOR,FK_BUDGET,FK_USER,IP_ADD,PK_BGTUSERS,RID
ON SCH_BGTUSERS REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr varchar2(100);
BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:new.last_modified_by);
  audit_trail.record_transaction
    (raid, 'SCH_BGTUSERS', :OLD.rid, 'U', usr);
  IF NVL(:OLD.pk_bgtusers,0) !=
     NVL(:NEW.pk_bgtusers,0) THEN
     audit_trail.column_update
       (raid, 'PK_BGTUSERS',
       :OLD.pk_bgtusers, :NEW.pk_bgtusers);
  END IF;
  IF NVL(:OLD.fk_budget,0) !=
     NVL(:NEW.fk_budget,0) THEN
     audit_trail.column_update
       (raid, 'FK_BUDGET',
       :OLD.fk_budget, :NEW.fk_budget);
  END IF;
  IF NVL(:OLD.fk_user,0) !=
     NVL(:NEW.fk_user,0) THEN
     audit_trail.column_update
       (raid, 'FK_USER',
       :OLD.fk_user, :NEW.fk_user);
  END IF;
  IF NVL(:OLD.bgtusers_rights,' ') !=
     NVL(:NEW.bgtusers_rights,' ') THEN
     audit_trail.column_update
       (raid, 'BGTUSERS_RIGHTS',
       :OLD.bgtusers_rights, :NEW.bgtusers_rights);
  END IF;
  IF NVL(:OLD.bgtusers_type,' ') !=
     NVL(:NEW.bgtusers_type,' ') THEN
     audit_trail.column_update
       (raid, 'BGTUSERS_TYPE',
       :OLD.bgtusers_type, :NEW.bgtusers_type);
  END IF;
  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;
  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       :OLD.last_modified_by, :NEW.last_modified_by);
  END IF;
  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;
END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,298,4,'04_SCH_BGTUSERS_AU0.sql',sysdate,'v9.3.0 #699');


