/////*********This readMe is specific to v10.1 build #786	  **********////


*********************************************************************************************************************************************************************
Network tab items: 

1. Study Network Tab implementation 
2. Networks Management -- Compared the requirements document and listed down the gaps. 
        I.a.iv -- By default, the tab should display all networks in a collapsed view. 
        I.a.v -- The tab should provide the ability to search for an organization throughout the networks. The search results should display all instances of that organization. The user should be able to apply a filter for org type.
        I.h.i.2 -- This page will function just like appendix works in other areas of the application.  
        See Mockup 6 and 7 for Documents   Done except Link part on attachment document.

3. Gaps found by the customer:
        Networks Management: 

            i)   While associating an organization to a network, it is hard to understand the level on which it will get associated to. It would be better to have the field highlighted under which the organization will be associated.
            ii)  The left panel (list of organizations) should be collapsed by default; only the right panel should be displayed.
            iii) User search needs to be enhanced to include other attributes. This is a use case when multiple people have the same first and last name, then how to identify the right person that needs to be added.-Not Done
            iV)  User status column needs to be added in Users section on networks.
4.General : 
             i)    Vertical scroll bar missing on the workflow panels
             ii)   All button colors are faded upon mouse hover
             iii)  Page real estate issues on Study Details and ad-hoc forms.
	     iV)   Save button at the bottom of Study Details page. It needs to be in centered.

**********************************************************************************************************************************************************************
The enhancements released in this build are

1. eC-41097 : IRB Chair Workflow
2. eC-41094 : Chair Signature Meeting Minutes
3. eC-40928 : Outcome Letter Preview
4. eC-41111 : Document Stamping
5. S-41054  : 4th indicator for Study Workflow panel
6. eC � 41375: eCompliance Grid Changes 
7. eC - 41385: Reviewer Area Search
**********************************************************************************************************************************************************************

NOTE:
1.To Appear your sig nature on documents (for eC-41111: Document Stamping)  

2. Please update your configBundle_custom.properties file with following key,
    In case of testing for St.Lukes update it like,
       a. DOCUMENT_STAMP_SIGNATURE=St. Luke's Health System IRB<br/>IRB NUMBER: <studyNumber><br/>IRB APPROVAL DATE: <approve_status_date>
    other wise to switch off this  Please update the key like,   
	b. DOCUMENT_STAMP_SIGNATURE=TBD

**********************************************************************************************************************************************************************

Not Implemented: Network tab
      I.e.ii -- This field should be an in-line editing field � dropdown field.
      I.e.v -- If an organization is made inactive, all sub-organizations under that organization should be made inactive as well. If the user will make a sub-organization active, the parent organizations will automatically become active.
      I.e.vi -- If an organization is made active, all sub-organizations under that organization will be made active as well.




