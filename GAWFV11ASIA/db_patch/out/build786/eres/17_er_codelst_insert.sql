set define off;
DECLARE
  v_cnt NUMBER;
  countFlag NUMBER(5);
 
 BEGIN
    SELECT COUNT(1) INTO countFlag FROM USER_TABLES WHERE TABLE_NAME='ER_CODELST';
    if (countFlag > 0) then
	
		SELECT COUNT(*)  INTO v_cnt from er_codelst  WHERE codelst_type ='sntwStat'  AND CODELST_SUBTYP='A';
		IF(v_cnt <1) THEN				
			INSERT INTO ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE)				
			VALUES(SEQ_ER_CODELST.NEXTVAL,'sntwStat','A','Available','N');
		END IF;	
		
		SELECT COUNT(*)  INTO v_cnt from er_codelst  WHERE codelst_type ='sntwStat'  AND CODELST_SUBTYP='E';
		IF(v_cnt <1) THEN  
			INSERT INTO ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE)				
			VALUES(SEQ_ER_CODELST.NEXTVAL,'sntwStat','E','Eligible to Participation','N');
		END IF;				
		SELECT COUNT(*)  INTO v_cnt from er_codelst  WHERE codelst_type ='sntwStat'  AND CODELST_SUBTYP='NP';
		IF(v_cnt <1) THEN  
			INSERT INTO ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE)				
			VALUES(SEQ_ER_CODELST.NEXTVAL,'sntwStat','NP','Not Participating','N');
		END IF;		
SELECT COUNT(*)  INTO v_cnt from er_codelst  WHERE codelst_type ='sntwStat'  AND CODELST_SUBTYP='OA';
		IF(v_cnt <1) THEN  
			INSERT INTO ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE)				
			VALUES(SEQ_ER_CODELST.NEXTVAL,'sntwStat','OA','Open to Accrual','N');
		END IF;				
commit;
end if;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,385,17,'17_er_codelst_insert.sql',sysdate,'v11 #786');

commit;