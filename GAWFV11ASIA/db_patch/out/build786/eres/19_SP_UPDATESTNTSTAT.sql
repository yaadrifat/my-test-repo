create or replace PROCEDURE "SP_UPDATESTNTSTAT" (p_netId in number,p_studyId in number,p_userId in varchar2, p_ipAdd in varchar2)
IS
v_codelstPk number;
BEGIN
  --update Study Network Status data
select pk_codelst into v_codelstPk from er_codelst where codelst_type='sntwStat' and codelst_subTyp='A';
FOR i IN (SELECT pk_nwsites  FROM ER_NWSITES START WITH pk_nwsites = p_netId CONNECT BY PRIOR pk_nwsites = FK_NWSITES order by pk_nwsites desc)
LOOP
Insert into ER_STATUS_HISTORY(PK_STATUS,STATUS_MODPK,STATUS_MODTABLE,FK_CODELST_STAT,STATUS_DATE,CREATOR,RECORD_TYPE,CREATED_ON,IP_ADD,STATUS_ISCURRENT,STATUS_PARENTMODPK) 
values (SEQ_ER_STATUS_HISTORY.nextval,i.pk_nwsites,'er_nwsites',v_codelstPk,sysdate,p_userId,'N',sysdate,p_ipAdd,1,p_studyId);
END LOOP;
COMMIT;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,385,19,'19_SP_UPDATESTNTSTAT.sql',sysdate,'v11 #786');

commit;