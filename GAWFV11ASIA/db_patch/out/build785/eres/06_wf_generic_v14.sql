create or replace PACKAGE        "PKG_WF_GENERIC" 
IS

--version 1.4

    PROCEDURE SP_CREATE_NEW_WORKFLOW(
      p_workflow_type VARCHAR2, p_workflow_displayname VARCHAR2,
      p_activity_name        VARCHAR2 ,p_activity_displayname        VARCHAR2 , p_activity_sequence number,
      p_activity_trigger Varchar2,p_activity_trigger_code Varchar2 ,p_possible_delvalues Varchar2);

 PROCEDURE SP_CREATE_NEW_WORKFLOW_COMP(
      p_workflow_type VARCHAR2,p_workflow_displayname VARCHAR2,
      p_activity_name        VARCHAR2,p_activity_displayname        VARCHAR2, p_activity_sequence number,
      p_activity_trigger1 Varchar2, p_activity_trigger_code1 Varchar2,p_possible_delvalues1 Varchar2,
      p_activity_trigger2 Varchar2, p_activity_trigger_code2 Varchar2,p_possible_delvalues2 Varchar2);
      
      
  PROCEDURE SP_CREATE_NEW_WORKFLOW_MULTI(
      p_workflow_type VARCHAR2,p_workflow_displayname VARCHAR2,
      p_activity_name        VARCHAR2,p_activity_displayname        VARCHAR2, p_activity_sequence number,
      p_activity_trigger Varchar2, p_activity_trigger_code Varchar2,p_possible_delvalues Varchar2,
      p_notrequired_ifnotmet Varchar2, p_activity_form Varchar2);
	  
  PROCEDURE SP_ADD_ACTIVITY_FOR_OLD_WFI;
      
      
  pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'PKG_WF_GENERIC', pLEVEL  => Plog.LFATAL);
  
   
END PKG_WF_GENERIC;
/

create or replace PACKAGE BODY        "PKG_WF_GENERIC" 
AS
  
  --Sonia Abrol, 26th May 2016 to create generic procs to create new workflows
  /*

   version 1.4

  p_activity_trigger: studystatus
                      studyform
                      
  
  p_activity_trigger_code:
  
1. for p_activity_trigger= 'studystatus', p_activity_trigger_code = code subtype of the study status for the activity
2. for p_activity_trigger= 'studyform', p_activity_trigger_code = pk of the form - active, linked study form 
3. for p_activity_trigger = 'studyMSD',p_activity_trigger_code = code subtype of the more study details field for the activity
  p_possible_delvalues - for more study details (or any other comparision field - pass * (asterisk) delimited possible values
  
  example - to check if the field value could be sponor or industry - pass sponsor*industry in p_possible_delvalues
  */

  PROCEDURE SP_CREATE_NEW_WORKFLOW(
      p_workflow_type VARCHAR2,p_workflow_displayname VARCHAR2,
      p_activity_name        VARCHAR2,p_activity_displayname        VARCHAR2, p_activity_sequence number,
      p_activity_trigger Varchar2, p_activity_trigger_code Varchar2,p_possible_delvalues Varchar2 )
    AS
  v_exists NUMBER := 0;
  v_nextVal NUMBER := 0;
  v_PK_WORKFLOW NUMBER := 0;
  v_PK_ACTIVITY NUMBER := 0;
  v_PK_WORKFLOWACTIVITY NUMBER := 0;
  v_activity_logic Varchar2(4000);
  v_wa_name Varchar2(4000);
  v_json varchar2(1000);
  
  v_formname varchar2(1000);
  
  begin


           SELECT count(*) INTO v_exists FROM WORKFLOW WHERE WORKFLOW_TYPE = p_workflow_type;


            IF (v_exists = 0) THEN
                v_PK_WORKFLOW := SQ_WORKFLOW.NEXTVAL();

                    Insert into WORKFLOW (PK_WORKFLOW, WORKFLOW_NAME, WORKFLOW_DESC, WORKFLOW_TYPE)
                    values (v_PK_WORKFLOW,p_workflow_displayname,p_workflow_displayname,p_workflow_type);
            
                  COMMIT;
            
            ELSE
                SELECT PK_WORKFLOW INTO v_PK_WORKFLOW FROM WORKFLOW WHERE WORKFLOW_TYPE = p_workflow_type;
            
            END IF;

   --Add Activities

   -------------------------------------------------------------------------------------------------------
   --Adding activity  in the activity and workflow_activity table---
   -------------------------------------------------------------------------------------------------------
   SELECT count(*) INTO v_exists FROM ACTIVITY WHERE ACTIVITY_NAME = p_activity_name;
  
   IF (v_exists = 0) THEN
       v_PK_ACTIVITY := SQ_ACTIVITY.NEXTVAL();

       Insert into ACTIVITY (PK_ACTIVITY,ACTIVITY_NAME,ACTIVITY_DESC)
       values (v_PK_ACTIVITY,p_activity_name,p_activity_displayname);

       COMMIT;

   ELSE
       
       SELECT PK_ACTIVITY INTO v_PK_ACTIVITY FROM ACTIVITY WHERE ACTIVITY_NAME = p_activity_name;

   END IF;
  
   IF (v_PK_ACTIVITY > 0) THEN
       --Workflow and activity association
       SELECT COUNT(*) INTO v_exists FROM WORKFLOW_ACTIVITY WHERE FK_WORKFLOWID = v_PK_WORKFLOW AND FK_ACTIVITYID = v_PK_ACTIVITY;
  
       IF (v_exists = 0) THEN
           v_PK_WORKFLOWACTIVITY := SQ_WORKFLOWACTIVITY.NEXTVAL();

           if p_activity_trigger = 'studystatus' THEN

                v_activity_logic := 'SELECT PKG_WF_GENERIC_RULES.F_studystat_activity(:1,''' || p_activity_trigger_code ||  ''') FROM DUAL';

                v_wa_name := 'select codelst_desc from er_codelst where CODELST_TYPE = ''studystat'' AND CODELST_SUBTYP = ''' || p_activity_trigger_code || '''';

           elsif p_activity_trigger = 'studyform' then
                
                v_activity_logic := 'SELECT PKG_WF_GENERIC_RULES.F_study_form_activity(:1,''' || p_activity_trigger_code ||  ''') FROM DUAL';

                
                v_json := '{"moduleId":"ERES_FORM", "moduleDet":{"formId":'''|| p_activity_trigger_code ||  '''}}';
                
                 v_wa_name := p_activity_displayname;
                 
            elsif p_activity_trigger = 'studyMSD' then
                
                v_activity_logic := 'SELECT PKG_WF_GENERIC_RULES.F_MSD_activity(:1,''' || p_activity_trigger_code ||  ''','''|| p_possible_delvalues || ''') FROM DUAL';
                v_wa_name := p_activity_displayname;       
           
           --SELECT PKG_WORKFLOW_RULES_DEMO.F_In_Review(:1) FROM DUAL
           ELSE
                v_activity_logic := 'select 1 from dual';
                v_wa_name := p_activity_displayname; 

           end if;

           Insert into WORKFLOW_ACTIVITY (PK_WORKFLOWACTIVITY, WA_NAME, FK_WORKFLOWID, FK_ACTIVITYID, WA_SEQUENCE, WA_TERMINATEFLAG, WA_DISPLAYQUERY, WA_ONCLICK_JSON)
           values (v_PK_WORKFLOWACTIVITY, v_wa_name, v_PK_WORKFLOW, v_PK_ACTIVITY, p_activity_sequence, 0, 
            v_activity_logic, v_json);
           
           COMMIT;
       END IF;
   END IF;


  
    END SP_CREATE_NEW_WORKFLOW;
    
      --Sonia Abrol, 26th May 2016 to create generic procs to create new workflows
  /*
  
  p_activity_trigger: studystatus
                      studyform
                      
  
  p_activity_trigger_code:
  
1. for p_activity_trigger1= 'studystatus', p_activity_trigger_code1 = code subtype of the study status for the activity
2. for p_activity_trigger1= 'studyform', p_activity_trigger_code1 = pk of the form - active, linked study form 
3. for p_activity_trigger1 = 'studyMSD',p_activity_trigger_code1 = code subtype of the more study details field for the activity
  p_possible_delvalues1 - for more study details (or any other comparision field - pass * (asterisk) delimited possible values
  
  example - to check if the field value could be sponor or industry - pass sponsor*industry in p_possible_delvalues
  
  repeat above for set 2 variables
  
  
  */

  PROCEDURE SP_CREATE_NEW_WORKFLOW_COMP(
      p_workflow_type VARCHAR2,p_workflow_displayname VARCHAR2,
      p_activity_name        VARCHAR2,p_activity_displayname        VARCHAR2, p_activity_sequence number,
      p_activity_trigger1 Varchar2, p_activity_trigger_code1 Varchar2,p_possible_delvalues1 Varchar2,
      p_activity_trigger2 Varchar2, p_activity_trigger_code2 Varchar2,p_possible_delvalues2 Varchar2)
    AS
  v_exists NUMBER := 0;
  v_nextVal NUMBER := 0;
  v_PK_WORKFLOW NUMBER := 0;
  v_PK_ACTIVITY NUMBER := 0;
  v_PK_WORKFLOWACTIVITY NUMBER := 0;
  v_activity_logic Varchar2(4000);
  v_wa_name Varchar2(4000);
  v_json varchar2(1000);
  
  v_formname varchar2(1000);
  
  begin


           SELECT count(*) INTO v_exists FROM WORKFLOW WHERE WORKFLOW_TYPE = p_workflow_type;


            IF (v_exists = 0) THEN
                v_PK_WORKFLOW := SQ_WORKFLOW.NEXTVAL();

                    Insert into WORKFLOW (PK_WORKFLOW, WORKFLOW_NAME, WORKFLOW_DESC, WORKFLOW_TYPE)
                    values (v_PK_WORKFLOW,p_workflow_displayname,p_workflow_displayname,p_workflow_type);
            
                  COMMIT;
            
            ELSE
                SELECT PK_WORKFLOW INTO v_PK_WORKFLOW FROM WORKFLOW WHERE WORKFLOW_TYPE = p_workflow_type;
            
            END IF;

   --Add Activities

   -------------------------------------------------------------------------------------------------------
   --Adding activity  in the activity and workflow_activity table---
   -------------------------------------------------------------------------------------------------------
   SELECT count(*) INTO v_exists FROM ACTIVITY WHERE ACTIVITY_NAME = p_activity_name;
  
   IF (v_exists = 0) THEN
       v_PK_ACTIVITY := SQ_ACTIVITY.NEXTVAL();

       Insert into ACTIVITY (PK_ACTIVITY,ACTIVITY_NAME,ACTIVITY_DESC)
       values (v_PK_ACTIVITY,p_activity_name,p_activity_displayname);

       COMMIT;

   ELSE
       
       SELECT PK_ACTIVITY INTO v_PK_ACTIVITY FROM ACTIVITY WHERE ACTIVITY_NAME = p_activity_name;

   END IF;
  
   IF (v_PK_ACTIVITY > 0) THEN
       --Workflow and activity association
       SELECT COUNT(*) INTO v_exists FROM WORKFLOW_ACTIVITY WHERE FK_WORKFLOWID = v_PK_WORKFLOW AND FK_ACTIVITYID = v_PK_ACTIVITY;
  
       IF (v_exists = 0) THEN
           v_PK_WORKFLOWACTIVITY := SQ_WORKFLOWACTIVITY.NEXTVAL();

            v_wa_name := 'select '''|| p_activity_displayname || ''' from dual' ;
            
           if p_activity_trigger1 = 'studyform'  then
                
                v_json := '{"moduleId":"ERES_FORM", "moduleDet":{"formId":'''|| p_activity_trigger_code1 ||  '''}}';
            end if;
            
            if  p_activity_trigger2 = 'studyform'  then
                
                v_json := '{"moduleId":"ERES_FORM", "moduleDet":{"formId":'''|| p_activity_trigger_code2 ||  '''}}';
                
             end if;
             
              v_activity_logic := 'SELECT PKG_WF_GENERIC_RULES.F_2_combination_activity(:1,
              ''' || p_activity_trigger1  ||  ''', ''' || p_activity_trigger_code1 ||  ''','''|| p_possible_delvalues1 || ''', 
               ''' || p_activity_trigger2  ||  ''', ''' || p_activity_trigger_code2 ||  ''','''|| p_possible_delvalues2 || ''' ) FROM DUAL';
              
           Insert into WORKFLOW_ACTIVITY (PK_WORKFLOWACTIVITY, WA_NAME, FK_WORKFLOWID, FK_ACTIVITYID, WA_SEQUENCE, WA_TERMINATEFLAG, WA_DISPLAYQUERY, WA_ONCLICK_JSON)
           values (v_PK_WORKFLOWACTIVITY, v_wa_name, v_PK_WORKFLOW, v_PK_ACTIVITY, p_activity_sequence, 0, 
            v_activity_logic, v_json);
           
           COMMIT;
       END IF;
   END IF;


  
    END SP_CREATE_NEW_WORKFLOW_COMP;


    --Sonia Abrol, 26th June 2016 to create generic procs to create new workflows and workflow activities based on multiple conditions
  /*
  
  p_activity_trigger: studystatus
                      studyform
                      
  
  p_activity_trigger_code:
  
1. for p_activity_trigger= 'studystatus', p_activity_trigger_code = code subtype of the study status for the activity
2. for p_activity_trigger= 'studyform', p_activity_trigger_code = pk of the form - active, linked study form 
3. for p_activity_trigger = 'studyMSD',p_activity_trigger_code = code subtype of the more study details field for the activity
  p_possible_delvalues - for more study details (or any other comparision field - pass * (asterisk) delimited possible values
4. p_activity_trigger = 'studysummary', 
 p_activity_trigger_code1 = 
                   name of the field in er_study : for all text fields that can be compared from er_study table OR
                                            for all codelist fields in er_study
                    OR
                   indide : check if indide data exists
                   OR
                   disease_site : for disease site check - ALTHOUGH disease site is not handled yet

  p_notrequired_ifnotmet - flag to indicate if a condition if NOT MET makes the activity NOT REQUIRED. Pass 1 if a condition when met
 makes an activity NOT REQUIRED

  example - to check if the field value could be sponor or industry - pass sponsor*industry in p_possible_delvalues
  
  all 3 params can accept multiple condition sets by delimiting with a | symbol. This way
  we are not limiting number of conditions an activity can require
  
  
  */

  PROCEDURE SP_CREATE_NEW_WORKFLOW_MULTI(
      p_workflow_type VARCHAR2,p_workflow_displayname VARCHAR2,
      p_activity_name        VARCHAR2,p_activity_displayname        VARCHAR2, p_activity_sequence number,
      p_activity_trigger Varchar2, p_activity_trigger_code Varchar2,p_possible_delvalues Varchar2,
      p_notrequired_ifnotmet Varchar2, p_activity_form Varchar2)
    AS
  v_exists NUMBER := 0;
  v_nextVal NUMBER := 0;
  v_PK_WORKFLOW NUMBER := 0;
  v_PK_ACTIVITY NUMBER := 0;
  v_PK_WORKFLOWACTIVITY NUMBER := 0;
  v_activity_logic Varchar2(4000);
  v_wa_name Varchar2(4000);
  v_json varchar2(1000);
  
  v_formname varchar2(1000);
  
 


  begin

  

           SELECT count(*) INTO v_exists FROM WORKFLOW WHERE WORKFLOW_TYPE = p_workflow_type;


            IF (v_exists = 0) THEN
                v_PK_WORKFLOW := SQ_WORKFLOW.NEXTVAL();

                    Insert into WORKFLOW (PK_WORKFLOW, WORKFLOW_NAME, WORKFLOW_DESC, WORKFLOW_TYPE)
                    values (v_PK_WORKFLOW,p_workflow_displayname,p_workflow_displayname,p_workflow_type);
            
                  COMMIT;
            
            ELSE
                SELECT PK_WORKFLOW INTO v_PK_WORKFLOW FROM WORKFLOW WHERE WORKFLOW_TYPE = p_workflow_type;
            
            END IF;

   --Add Activities

   -------------------------------------------------------------------------------------------------------
   --Adding activity  in the activity and workflow_activity table---
   -------------------------------------------------------------------------------------------------------
   SELECT count(*) INTO v_exists FROM ACTIVITY WHERE ACTIVITY_NAME = p_activity_name;
  
   IF (v_exists = 0) THEN
       v_PK_ACTIVITY := SQ_ACTIVITY.NEXTVAL();

       Insert into ACTIVITY (PK_ACTIVITY,ACTIVITY_NAME,ACTIVITY_DESC)
       values (v_PK_ACTIVITY,p_activity_name,p_activity_displayname);

       COMMIT;

   ELSE
       
       SELECT PK_ACTIVITY INTO v_PK_ACTIVITY FROM ACTIVITY WHERE ACTIVITY_NAME = p_activity_name;

   END IF;
  
   IF (v_PK_ACTIVITY > 0) THEN
       --Workflow and activity association
       SELECT COUNT(*) INTO v_exists FROM WORKFLOW_ACTIVITY WHERE FK_WORKFLOWID = v_PK_WORKFLOW AND FK_ACTIVITYID = v_PK_ACTIVITY;
  
       IF (v_exists = 0) THEN
           v_PK_WORKFLOWACTIVITY := SQ_WORKFLOWACTIVITY.NEXTVAL();

            v_wa_name := 'select '''|| p_activity_displayname || ''' from dual' ;
            
           if nvl(f_to_number(p_activity_form),0) > 0 then
                
                v_json := '{"moduleId":"ERES_FORM", "moduleDet":{"formId":'''|| p_activity_form ||  '''}}';
            end if;
            
            
             
              v_activity_logic := 'SELECT PKG_WF_GENERIC_RULES.F_multi_combination_activity(:1,
              ''' || p_activity_trigger  ||  ''', ''' || p_activity_trigger_code ||  ''','''|| p_possible_delvalues || ''', 
               ''' || p_notrequired_ifnotmet  ||  ''' ) FROM DUAL';
              
           Insert into WORKFLOW_ACTIVITY (PK_WORKFLOWACTIVITY, WA_NAME, FK_WORKFLOWID, FK_ACTIVITYID, WA_SEQUENCE, WA_TERMINATEFLAG, WA_DISPLAYQUERY, WA_ONCLICK_JSON)
           values (v_PK_WORKFLOWACTIVITY, v_wa_name, v_PK_WORKFLOW, v_PK_ACTIVITY, p_activity_sequence, 0, 
            v_activity_logic, v_json);
           
           COMMIT;
       END IF;
   END IF;


  
    END SP_CREATE_NEW_WORKFLOW_MULTI;
	
	PROCEDURE SP_ADD_ACTIVITY_FOR_OLD_WFI IS
	PRAGMA AUTONOMOUS_TRANSACTION;
	--Procedure for adding new task to the existing workflow instances

	 v_not_wfid number :=0;
	begin
	--Looping all the existing workflow instances created
	for exist_wf_instance in (select PK_WFINSTANCEID,FK_ENTITYID,FK_WORKFLOWID from workflow_instance)
	loop
	--Looping all workflow activities available for workflow
	  for all_WF_activities in (select PK_WORKFLOWACTIVITY,WA_NAME from workflow_activity where FK_WORKFLOWID=exist_wf_instance.FK_WORKFLOWID)
	  loop
	  --Check for availability of workflow activity in task table
		select count(*) into v_not_wfid FROM task t WHERE t.fk_wfinstanceid=exist_wf_instance.PK_WFINSTANCEID
		and fk_entity=exist_wf_instance.FK_ENTITYID and t.FK_WORKFLOWACTIVITY=all_WF_activities.PK_WORKFLOWACTIVITY;
		if v_not_wfid=0 then
		--Creating new task for the missing or newly added workflow activity
		  insert into task (PK_TASKID,FK_WORKFLOWACTIVITY,FK_ENTITY,FK_WFINSTANCEID,TASK_NAME,DELETEDFLAG) values
		  (SQ_TASK.nextval, all_WF_activities.PK_WORKFLOWACTIVITY,exist_wf_instance.FK_ENTITYID,exist_wf_instance.PK_WFINSTANCEID,all_WF_activities.WA_NAME,0);
		  commit;
		end if;
	  end loop;

	v_not_wfid:=0;
	end loop;
	end SP_ADD_ACTIVITY_FOR_OLD_WFI;
    
END PKG_WF_GENERIC;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,384,6,'06_wf_generic_v14.sql',sysdate,'v10.1 #785');

commit;