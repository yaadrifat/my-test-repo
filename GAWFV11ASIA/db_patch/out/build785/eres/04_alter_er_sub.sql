set define off;
set serveroutput on;
declare
v_col_exists number:=0;
begin
select count(*) into v_col_exists from USER_TAB_COLUMNS where TABLE_NAME='ER_SUBMISSION' and column_name='AMEND_FLAG';
if v_col_exists = 0 then
  execute immediate('Alter table ER_SUBMISSION add AMEND_FLAG number');
else
  dbms_output.put_line('Column already exists');
end if;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,384,4,'04_alter_er_sub.sql',sysdate,'v10.1 #785');

commit;