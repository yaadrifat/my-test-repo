set define off;
DECLARE
  v_record_exists NUMBER ;
  v_repfiltermap_colomn CLOB;
BEGIN
  SELECT COUNT(*)
  INTO v_record_exists
  FROM er_repfiltermap
  WHERE repfiltermap_repcat='data safety monitoring';
  
  IF (v_record_exists = 0) THEN
  v_repfiltermap_colomn:='<td><DIV id="studyDIV"><Font class=comments><A href="javascript:openLookup(document.reports,''viewId=6013&form=reports&seperator=,&defaultvalue=[SELSTUDYPK]&keyword=selstudyId|STUDY_NUMBER~paramstudyId|LKP_PK|[VELHIDE]'','''')">Select Study</Font></DIV></td><td><DIV id="studydataDIV"> <input TYPE="text" NAME="selstudyId" readonly value="[SELSTUDYNUMBER]"><input TYPE="hidden" NAME="paramstudyId" value="[SELSTUDYPK]"></DIV></td>';
    INSERT INTO
  er_repfiltermap (PK_REPFILTERMAP,FK_REPFILTER,REPFILTERMAP_REPCAT,REPFILTERMAP_SEQ,REPFILTERMAP_MANDATORY,REPFILTERMAP_COLUMN,REPFILTERMAP_DISPDIV) 
  VALUES (SEQ_ER_REPFILTERMAP.NEXTVAL,(select pk_repfilter from er_repfilter where repfilter_keyword='studyId'),'data safety monitoring',1,'Y',v_repfiltermap_colomn,'studyDIV,studydataDIV');
  END IF;
 commit; 
END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,383,5,'05_er_repfiltermap.sql',sysdate,'v10.1 #784');

commit;
