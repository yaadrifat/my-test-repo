DECLARE 
	table_check number;
	row_check number;
	update_sql clob;

BEGIN
	SELECT count(*)
	INTO table_check
	FROM user_tab_cols
	WHERE TABLE_NAME = 'ER_REPORT';
	
	IF (table_check > 0) then
		/*SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report = 79;
		IF (row_check <> 0) then		
			update_sql:='SELECT study_number,pk_study, study_nsamplsize as study_samplsize,(SELECT site_name FROM ER_SITE WHERE pk_site = a.fk_site) AS site_name,
		(SELECT TO_CHAR(MIN(patstudystat_date),PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT x,ER_PATPROT y WHERE x.fk_per = y.fk_per AND b.pk_study = y.fk_study AND b.pk_study = x.fk_study AND y.fk_site_enrolling = a.fk_site AND  fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''enrolled'')) AS  first_enrolled_date,
		(SELECT COUNT(*) FROM vda.vda_v_pat_accrual WHERE fk_study = b.pk_study AND FK_SITE_ENROLLING = a.fk_site) AS  tot_pt,
		(SELECT COUNT(*) FROM ER_PATSTUDYSTAT x,ER_PATPROT y WHERE x.fk_per = y.fk_per AND b.pk_study = y.fk_study AND b.pk_study = x.fk_study AND y.fk_site_enrolling = a.fk_site AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''enrolled'')) AS  tot_enrolled,
		(SELECT COUNT(*) FROM ER_PATSTUDYSTAT x,ER_PATPROT y WHERE x.fk_per = y.fk_per AND b.pk_study = y.fk_study AND b.pk_study = x.fk_study AND y.fk_site_enrolling = a.fk_site AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''screening'')) AS  tot_screened,
		(SELECT COUNT(*) FROM ER_PATSTUDYSTAT x,ER_PATPROT y WHERE x.fk_per = y.fk_per AND b.pk_study = y.fk_study AND b.pk_study = x.fk_study AND y.fk_site_enrolling = a.fk_site AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''scrfail'')) AS  tot_screenfail,
		(SELECT COUNT(*) FROM ER_PATSTUDYSTAT x,ER_PATPROT y WHERE x.fk_per = y.fk_per AND b.pk_study = y.fk_study AND b.pk_study = x.fk_study AND y.fk_site_enrolling = a.fk_site AND  current_stat = 1 AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''active'')) AS  tot_active,
		(SELECT COUNT(*) FROM ER_PATSTUDYSTAT x,ER_PATPROT y WHERE x.fk_per = y.fk_per AND b.pk_study = y.fk_study AND b.pk_study = x.fk_study AND y.fk_site_enrolling = a.fk_site AND  current_stat = 1 AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''followup'')) AS  tot_followup,
		(SELECT COUNT(*) FROM ER_PATSTUDYSTAT x,ER_PATPROT y WHERE x.fk_per = y.fk_per AND b.pk_study = y.fk_study AND b.pk_study = x.fk_study AND y.fk_site_enrolling = a.fk_site AND  current_stat = 1 AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''offstudy'')) AS  tot_offstudy,
		(SELECT COUNT(*) FROM ER_PATSTUDYSTAT x,ER_PATPROT y WHERE x.fk_per = y.fk_per AND b.pk_study = y.fk_study AND b.pk_study = x.fk_study AND y.fk_site_enrolling = a.fk_site AND  current_stat = 1 AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''offtreat'')) AS  tot_offtreat
		FROM ER_STUDYSITES a,ER_STUDY b
		WHERE pk_study = fk_study AND
		pk_study IN (:studyId) AND
		a.fk_site IN (:orgId)
		order by study_number';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report = 79;					
		END IF;*/

	
	
	
		/*SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report = 82;

		IF (row_check <> 0) then
			INSERT INTO er_report (
				pk_report
				,rep_name
				,rep_desc
				,fk_account
				,fk_codelst_type
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable
				)
			VALUES(
				 82
				,'Active/Enrolling Protocols'
				,'Active Enrolling Protocols'
				,0
				,4835
				,'Y'
				,'Study Number, Start Date, Title,  Phase, Research Type, Study Type, Division, Therapeutic Area, Principal Investigator'
				,'Date, User, Organization, Therapeutic Area, Division, Study'
				,1
				,'allsite_rep'
				,''
				,'date:tAreaId:studyId:studyDivId,:userId:orgId'
				);
				
				update_sql:=' Select
		STUDY_NUMBER,
		TO_CHAR(study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) STUDYACTUAL_DATE,
		STUDY_TITLE,
		(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_tarea) AS TA,
		(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_phase) AS PHASE,
		(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_restype) AS RESEARCH_TYPE,
		(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_type) AS STUDY_TYPE,
		(SELECT  ER_USER.usr_firstname || '' '' ||  ER_USER.usr_lastname
             FROM ER_USER
            WHERE pk_user = ER_STUDY.study_prinv) || DECODE(STUDY_OTHERPRINV,NULL,'''','', '' || STUDY_OTHERPRINV) AS PI,
		(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = study_division) AS division
		FROM ER_STUDY  WHERE fk_account = :sessAccId
		AND study_actualdt <= TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
		AND fk_codelst_tarea IN (:tAreaId)
		AND pk_study IN (:studyId)
		AND (study_division IN (:studyDivId) OR study_division IS NULL)
		AND ( study_prinv IN (:userId) OR study_prinv IS NULL )
		AND EXISTS  (SELECT fk_study FROM ER_STUDYSITES WHERE fk_study=pk_study AND fk_site IN (:orgId) )
		AND EXISTS (SELECT * FROM ER_STUDYSTAT WHERE pk_study = fk_study AND fk_codelst_studystat IN
		(SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''studystat'' AND codelst_subtyp = ''active'' )
		AND studystat_date <=  TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) )
		AND NOT EXISTS (SELECT * FROM ER_STUDYSTAT WHERE pk_study = fk_study AND fk_codelst_studystat IN (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''studystat'' AND codelst_subtyp IN (''prmnt_cls'',''active_cls'') )
		AND studystat_date <  TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) )
		ORDER BY TA';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report = 82;	
		END IF;*/

	
	
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=117;

		IF (row_check <> 0) then				
				update_sql:='SELECT study_number,
		(SELECT COUNT(*) FROM ER_PATPROT,person WHERE fk_study = a.pk_study AND patprot_stat = 1 AND PATPROT_ENROLDT IS NOT NULL AND pk_person = fk_per AND FK_SITE_ENROLLING IN (:orgId)  ) AS ctr_to_date,
		(SELECT COUNT(*) FROM ER_PATPROT,person WHERE fk_study = a.pk_study AND patprot_stat = 1 AND patprot_enroldt BETWEEN 	TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
		AND pk_person = fk_per AND FK_SITE_ENROLLING IN (:orgId)  ) AS ctr_12_mos,
		race_ne,race_white,race_asian,race_notrep,race_indala,race_blkafr,race_hwnisl,race_unknown,ethnicity_ne,ethnicity_hispanic,ethnicity_unknown,ethnicity_nonhispanic,ethnicity_notreported,gender_ne,gender_male,gender_female,gender_other,gender_unknown
		FROM (SELECT fk_study,SUM(race_ne) AS race_ne,SUM(race_white) AS race_white,SUM(race_asian) AS race_asian,SUM(race_notrep) AS  race_notrep,SUM(race_indala) AS race_indala,SUM(race_blkafr) AS race_blkafr,
		SUM(race_hwnisl) AS race_hwnisl,SUM(race_unknown) AS race_unknown,SUM(ethnicity_ne) AS ethnicity_ne,SUM(ethnicity_hispanic) AS ethnicity_hispanic,SUM(ethnicity_unknown) AS ethnicity_unknown,
		SUM(ethnicity_nonhispanic) AS ethnicity_nonhispanic,SUM(ethnicity_notreported) AS ethnicity_notreported,
		SUM(gender_ne) AS gender_ne,SUM(gender_male) AS gender_male,SUM(gender_female) AS gender_female,SUM(gender_other) AS gender_other,
		SUM(gender_unknown) AS gender_unknown FROM (SELECT fk_study, DECODE(race,NULL,1,0) AS race_ne,DECODE(race,''race_white'',1,0) AS race_white,DECODE(race,''race_asian'',1,0) AS race_asian,
		DECODE(race,''race_notrep'',1,0) AS race_notrep, DECODE(race,''race_indala'',1,0) AS race_indala, DECODE(race,''race_blkafr'',1,0) AS race_blkafr,
		DECODE(race,''race_hwnisl'',1,0) AS race_hwnisl, DECODE(race,''race_unknown'',1,0) AS race_unknown,DECODE(ethnicity,NULL,1,0) AS ethnicity_ne,
		DECODE(ethnicity,''hispanic'',1,0) AS ethnicity_hispanic, DECODE(ethnicity,''Unknown'',1,0) AS ethnicity_unknown,DECODE(ethnicity,''nonhispanic'',1,0) AS ethnicity_nonhispanic,DECODE(ethnicity,''notreported'',1,0) AS 	ethnicity_notreported,
		DECODE(gender,NULL,1,0) AS gender_ne, DECODE(gender,''male'',1,0) AS gender_male,
		DECODE(gender,''female'',1,0) AS gender_female,
		DECODE(gender,''other'',1,0) AS gender_other,
		DECODE(gender,''Unknown'',1,0) AS gender_unknown FROM  (
		SELECT fk_study,
		(SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = fk_codelst_race) AS race,
		(SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = fk_codelst_gender) AS gender,
		(SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = fk_codelst_ethnicity) AS ethnicity
		FROM EPAT.person, ER_PATPROT a
		WHERE pk_person = fk_per AND FK_SITE_ENROLLING IN (:orgId) AND
		patprot_enroldt BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND 		TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND
		patprot_stat = 1
		)) GROUP BY fk_study
		), ER_STUDY a WHERE pk_study = fk_study AND fk_account = :sessAccId
		AND fk_study IN (:studyId)';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql,
		rep_columns='Study Number, Accrual to Date, Accrual in Selected Period, American Indian or Alaska Native, Asian, Black or African American, Native Hawaiian or Other Pacific Islander, Not Reported, White, Unknown, Race Not Entered, Hispanic or Latino, Non-Hispanic, Not reported, Unknown, Ethinicity Not Entered, Female, Male, Other, Unknown, Gender Not Entered',
		rep_filterby='Date (Enrollment Date) </br> Study (Study Number) </br> Organization (Enrolling Site)',
		rep_filterapplicable='date:orgId:studyId' WHERE pk_report=117;	
		END IF; 

	
	
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=121;

		IF (row_check <> 0) then
				update_sql:='SELECT nvl(studysite_lsamplesize,0)studysite_lsamplesize,
		study_number,(SELECT site_name FROM ER_SITE WHERE pk_site = a.fk_site) AS site_name,
		(SELECT COUNT(*) FROM vda.vda_v_pat_accrual WHERE fk_study = pk_study AND FK_SITE_ENROLLING = a.fk_site AND TO_CHAR(TRUNC(patprot_enroldt),''yyyymm'') = TO_CHAR(TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT),''yyyy'') || ''01'') AS jan_acr,
		(SELECT COUNT(*) FROM vda.vda_v_pat_accrual WHERE fk_study = pk_study AND FK_SITE_ENROLLING = a.fk_site AND TO_CHAR(TRUNC(patprot_enroldt),''yyyymm'') = TO_CHAR(TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT),''yyyy'') || ''02'') AS feb_acr,
		(SELECT COUNT(*) FROM vda.vda_v_pat_accrual WHERE fk_study = pk_study AND FK_SITE_ENROLLING = a.fk_site AND TO_CHAR(TRUNC(patprot_enroldt),''yyyymm'') = TO_CHAR(TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT),''yyyy'') || ''03'') AS mar_acr,  
		(SELECT COUNT(*) FROM vda.vda_v_pat_accrual WHERE fk_study = pk_study AND FK_SITE_ENROLLING = a.fk_site AND TO_CHAR(TRUNC(patprot_enroldt),''yyyymm'') = TO_CHAR(TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT),''yyyy'') || ''04'') AS apr_acr,
		(SELECT COUNT(*) FROM vda.vda_v_pat_accrual WHERE fk_study = pk_study AND FK_SITE_ENROLLING = a.fk_site AND TO_CHAR(TRUNC(patprot_enroldt),''yyyymm'') = TO_CHAR(TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT),''yyyy'') || ''05'') AS may_acr,
		(SELECT COUNT(*) FROM vda.vda_v_pat_accrual WHERE fk_study = pk_study AND FK_SITE_ENROLLING = a.fk_site AND TO_CHAR(TRUNC(patprot_enroldt),''yyyymm'') = TO_CHAR(TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT),''yyyy'') || ''06'') AS jun_acr,
		(SELECT COUNT(*) FROM vda.vda_v_pat_accrual WHERE fk_study = pk_study AND FK_SITE_ENROLLING = a.fk_site AND TO_CHAR(TRUNC(patprot_enroldt),''yyyymm'') = TO_CHAR(TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT),''yyyy'') || ''07'') AS jul_acr,
		(SELECT COUNT(*) FROM vda.vda_v_pat_accrual WHERE fk_study = pk_study AND FK_SITE_ENROLLING = a.fk_site AND TO_CHAR(TRUNC(patprot_enroldt),''yyyymm'') = TO_CHAR(TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT),''yyyy'') || ''08'') AS aug_acr,
		(SELECT COUNT(*) FROM vda.vda_v_pat_accrual WHERE fk_study = pk_study AND FK_SITE_ENROLLING = a.fk_site AND TO_CHAR(TRUNC(patprot_enroldt),''yyyymm'') = TO_CHAR(TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT),''yyyy'') || ''09'') AS sep_acr, 
		(SELECT COUNT(*) FROM vda.vda_v_pat_accrual WHERE fk_study = pk_study AND FK_SITE_ENROLLING = a.fk_site AND TO_CHAR(TRUNC(patprot_enroldt),''yyyymm'') = TO_CHAR(TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT),''yyyy'') || ''10'') AS oct_acr,
		(SELECT COUNT(*) FROM vda.vda_v_pat_accrual WHERE fk_study = pk_study AND FK_SITE_ENROLLING = a.fk_site AND TO_CHAR(TRUNC(patprot_enroldt),''yyyymm'') = TO_CHAR(TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT),''yyyy'') || ''11'') AS nov_acr,
		(SELECT COUNT(*) FROM vda.vda_v_pat_accrual WHERE fk_study = pk_study AND FK_SITE_ENROLLING = a.fk_site AND TO_CHAR(TRUNC(patprot_enroldt),''yyyymm'') = TO_CHAR(TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT),''yyyy'') || ''12'') AS dec_acr,
		(SELECT COUNT(*) FROM vda.vda_v_pat_accrual WHERE fk_study = pk_study AND FK_SITE_ENROLLING = a.fk_site) AS total_acr,
		(SELECT COUNT(*) FROM vda.vda_v_pat_accrual WHERE fk_study = pk_study AND FK_SITE_ENROLLING = a.fk_site AND TO_CHAR(TRUNC(patprot_enroldt),''yyyy'') = TO_CHAR(TO_NUMBER(TO_CHAR(TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT),''yyyy''))) - 1)  AS prevyr_acr,
		(SELECT COUNT(*) FROM vda.vda_v_pat_accrual WHERE fk_study = pk_study AND FK_SITE_ENROLLING = a.fk_site AND TO_CHAR(TRUNC(patprot_enroldt),''yyyy'') = TO_CHAR(TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT),''yyyy'')) AS curyr_total_acr,study_title AS study_title_complete,
		study_nsamplsize AS study_nsamplsize,
		( SELECT SUM(study_nsamplsize) FROM ER_STUDY WHERE pk_study IN (:studyId) ) AS study_nsamplsize_sum
		FROM ER_STUDYSITES a,ER_STUDY b
		WHERE (SELECT COUNT(*) FROM vda.vda_v_pat_accrual WHERE fk_study = pk_study AND FK_SITE_ENROLLING = a.fk_site ) > 0 and pk_study = fk_study AND pk_study IN (:studyId)';
		
		UPDATE ER_REPORT SET REP_NAME='Annual Accrual Trend (Study Level/ All Sites)',REP_DESC='Annual Accrual Trend (Study Level/ All Sites)',REP_COLUMNS='Study Number, Accrual Goals, Accrual To Date, Accrual Previous Year, Accrual Current Year, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec',REP_FILTERBY='Date (Enrollment Date) </br> Study (Study Number) </br> Organization (Enrolling Site)',REP_FILTERAPPLICABLE='date:studyId:orgId',REP_SQL_CLOB=update_sql WHERE pk_report=121;		
		END IF; 
	
	
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=251;

		IF (row_check = 0) then
			INSERT INTO er_report (
				pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable
				)
			VALUES(
				 251
				,'Accrual by Year'
				,'Accrual by Year'
				,0
				,'N'
				,'Study Number, Division, Therapeutic Area, Phase, Principal Investigator, Enrolling Site, Enrolling Year, # Patients Enrolled'
				,'Study (Study Number) </br> Organization (Enrolling Site)'
				,1
				,'allsite_rep'
				,''
				,'studyId:orgId'
				);
				
				update_sql:='select FK_STUDY,
		(select STUDY_NUMBER from vda.vda_v_study_summary where pk_study = fk_study) as STUDY_NUMBER,
		(select STUDY_DIVISION from vda.vda_v_study_summary where pk_study = fk_study) as STUDY_DIVISION,
		(select STUDY_TAREA from vda.vda_v_study_summary where pk_study = fk_study) as STUDY_TAREA,
		(select STUDY_PHASE from vda.vda_v_study_summary where pk_study = fk_study) as STUDY_PHASE,
		(select STUDY_PI from vda.vda_v_study_summary where pk_study = fk_study) as STUDY_PI,
		FK_SITE_ENROLLING,
		PSTAT_ENROLL_SITE,
		PATPROT_ENROLL_YEAR,
		ACCRUAL_COUNT
		from VDA.VDA_V_PAT_ACCRUAL_YEAR
		WHERE fk_site_enrolling IN (:orgId) AND fk_study IN (:studyId)';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=251;	
		END IF; 
		
	
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=252;

		IF (row_check = 0) then
			INSERT INTO er_report (
				pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable
				)
			VALUES(
				 252
				,'Studies with No Enrollment'
				,'Studies with No Enrollment'
				,0
				,'N'
				,'Days Since Study Activation, Study Opened to Enrollment, Study Number, Title, Division, Therapeutic Area, Principal Investigator, Overall Target Accrual, Current Accrual'
				,'Study (Study Number)'
				,1
				,'allsite_rep'
				,''
				,'studyId'
				);
				
				update_sql:='SELECT 
		study_number,
		pk_study,
		study_title,
		(select study_PI from vda.vda_v_study_summary where vda.vda_v_study_summary.pk_study = b.pk_study) as STUDY_PI,
		(select study_division from vda.vda_v_study_summary where vda.vda_v_study_summary.pk_study = b.pk_study) as STUDY_DIVISION,
		(select study_tarea from vda.vda_v_study_summary where vda.vda_v_study_summary.pk_study = b.pk_study) as STUDY_TAREA,
		TO_CHAR((select study_actualdt from vda.vda_v_study_summary where vda.vda_v_study_summary.pk_study = b.pk_study) ,PKG_DATEUTIL.F_GET_DATEFORMAT) as STUDY_ACTUALDT,
		(SELECT COUNT(*) FROM vda.vda_v_pat_accrual WHERE fk_study = pk_study ) AS total_acr,
		(SELECT sysdate - to_date(study_actualdt) FROM DUAL) as DIFF,
		study_nsamplsize AS study_nsamplsize
		FROM ER_STUDY b
		WHERE (SELECT COUNT(*) FROM vda.vda_v_pat_accrual WHERE fk_study = pk_study ) = 0 AND study_actualdt is not null AND pk_study IN (:studyId)';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=252;		
		END IF;
	
	
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=253;

		IF (row_check = 0) then
			INSERT INTO er_report (
				pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable
				)
			VALUES(
				 253
				,'Accrual Performance Overview'
				,'Accrual Performance Overview'
				,0
				,'N'
				,'# Enrolled by Year, # Enrolled by PI, # Enrolled by Division, Total Studies With Enrollment, Total Enrollment Count, Study Number, Principal Investigator, Study Opened to Enrollment, Study Closed/Retired, # Enrolled'
				,'Date (Enrollment Date) </br> Study (Study Number) </br> Organization (Enrolling Site)'
				,1
				,'allsite_rep'
				,''
				,'date:studyId:orgId'
				);
				
				update_sql:='select 
		STUDY_NUMBER,
		(select study_division from VDA.VDA_V_STUDY_SUMMARY where VDA.VDA_V_STUDY_SUMMARY.pk_study = VDA.VDA_V_PAT_ACCRUAL.fk_study) as STUDY_DIVISION,
		(select study_phase from VDA.VDA_V_STUDY_SUMMARY where VDA.VDA_V_STUDY_SUMMARY.pk_study = VDA.VDA_V_PAT_ACCRUAL.fk_study) as STUDY_PHASE,
		(select study_pi from VDA.VDA_V_STUDY_SUMMARY where VDA.VDA_V_STUDY_SUMMARY.pk_study = VDA.VDA_V_PAT_ACCRUAL.fk_study) as STUDY_PI,
		TO_CHAR((select study_actualdt from VDA.VDA_V_STUDY_SUMMARY where VDA.VDA_V_STUDY_SUMMARY.pk_study = VDA.VDA_V_PAT_ACCRUAL.fk_study),PKG_DATEUTIL.F_GET_DATEFORMAT) as STUDY_ACTUALDT,
		TO_CHAR((select last_perm_closure from vda.vda_v_studystatus_metrics where vda.vda_v_studystatus_metrics.pk_study = VDA.VDA_V_PAT_ACCRUAL.fk_study),PKG_DATEUTIL.F_GET_DATEFORMAT) as STUDY_CLOSEDT,
		PSTAT_ENROLL_SITE,
		PSTAT_PAT_STUD_ID,
		PSTAT_ENROLLED_BY,
		TO_CHAR(PATPROT_ENROLDT,PKG_DATEUTIL.F_GET_DATEFORMAT) PATPROT_ENROLDT,
		(select patprot_enroll_year from VDA.VDA_V_PAT_ACCRUAL_ALL where VDA.VDA_V_PAT_ACCRUAL_ALL.fk_study = VDA.VDA_V_PAT_ACCRUAL.fk_study and VDA.VDA_V_PAT_ACCRUAL_ALL.fk_per = VDA.VDA_V_PAT_ACCRUAL.fk_per) as ENROLL_YEAR,
		PSTAT_ASSIGNED_TO,
		PSTAT_PHYSICIAN,
		FK_STUDY,
		FK_SITE_ENROLLING
		from VDA.VDA_V_PAT_ACCRUAL
		WHERE fk_site_enrolling IN (:orgId) 
		AND TRUNC(PATPROT_ENROLDT) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
		AND fk_study IN (:studyId)
		order by ENROLL_YEAR asc';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=253;		
		END IF; 
	
	
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=254;

		IF (row_check = 0) then
			INSERT INTO er_report (
				pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterkeyword
				,rep_filterapplicable
				)
			VALUES(
				 254
				,'Annual Accrual Trend (By Study/Site)'
				,'Annual Accrual Trend (By Study/Site)'
				,0
				,'N'
				,'Study Number, Organization, Local Accrual Goal, Accrual To Date, Accrual Previous Year, Accrual Current Year, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec'
				,'Date (Enrollment Date) </br> Study (Study Number) </br> Organization (Enrolling Site)'
				,1
				,'allsite_rep'
				,''
				,'[DATEFILTER]YMD:studyId:orgId'
				,'date:studyId:orgId'
				);
				
				update_sql:=' SELECT nvl(studysite_lsamplesize,0)studysite_lsamplesize,
		study_number,(SELECT site_name FROM ER_SITE WHERE pk_site = a.fk_site) AS site_name,
		(SELECT COUNT(*) FROM vda.vda_v_pat_accrual WHERE fk_study = pk_study AND FK_SITE_ENROLLING = a.fk_site AND TO_CHAR(TRUNC(patprot_enroldt),''yyyymm'') = TO_CHAR(TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT),''yyyy'') || ''01'') AS jan_acr,
		(SELECT COUNT(*) FROM vda.vda_v_pat_accrual WHERE fk_study = pk_study AND FK_SITE_ENROLLING = a.fk_site AND TO_CHAR(TRUNC(patprot_enroldt),''yyyymm'') = TO_CHAR(TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT),''yyyy'') || ''02'') AS feb_acr,
		(SELECT COUNT(*) FROM vda.vda_v_pat_accrual WHERE fk_study = pk_study AND FK_SITE_ENROLLING = a.fk_site AND TO_CHAR(TRUNC(patprot_enroldt),''yyyymm'') = TO_CHAR(TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT),''yyyy'') || ''03'') AS mar_acr,  
		(SELECT COUNT(*) FROM vda.vda_v_pat_accrual WHERE fk_study = pk_study AND FK_SITE_ENROLLING = a.fk_site AND TO_CHAR(TRUNC(patprot_enroldt),''yyyymm'') = TO_CHAR(TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT),''yyyy'') || ''04'') AS apr_acr,
		(SELECT COUNT(*) FROM vda.vda_v_pat_accrual WHERE fk_study = pk_study AND FK_SITE_ENROLLING = a.fk_site AND TO_CHAR(TRUNC(patprot_enroldt),''yyyymm'') = TO_CHAR(TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT),''yyyy'') || ''05'') AS may_acr,
		(SELECT COUNT(*) FROM vda.vda_v_pat_accrual WHERE fk_study = pk_study AND FK_SITE_ENROLLING = a.fk_site AND TO_CHAR(TRUNC(patprot_enroldt),''yyyymm'') = TO_CHAR(TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT),''yyyy'') || ''06'') AS jun_acr,
		(SELECT COUNT(*) FROM vda.vda_v_pat_accrual WHERE fk_study = pk_study AND FK_SITE_ENROLLING = a.fk_site AND TO_CHAR(TRUNC(patprot_enroldt),''yyyymm'') = TO_CHAR(TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT),''yyyy'') || ''07'') AS jul_acr,
		(SELECT COUNT(*) FROM vda.vda_v_pat_accrual WHERE fk_study = pk_study AND FK_SITE_ENROLLING = a.fk_site AND TO_CHAR(TRUNC(patprot_enroldt),''yyyymm'') = TO_CHAR(TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT),''yyyy'') || ''08'') AS aug_acr,
		(SELECT COUNT(*) FROM vda.vda_v_pat_accrual WHERE fk_study = pk_study AND FK_SITE_ENROLLING = a.fk_site AND TO_CHAR(TRUNC(patprot_enroldt),''yyyymm'') = TO_CHAR(TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT),''yyyy'') || ''09'') AS sep_acr, 
		(SELECT COUNT(*) FROM vda.vda_v_pat_accrual WHERE fk_study = pk_study AND FK_SITE_ENROLLING = a.fk_site AND TO_CHAR(TRUNC(patprot_enroldt),''yyyymm'') = TO_CHAR(TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT),''yyyy'') || ''10'') AS oct_acr,
		(SELECT COUNT(*) FROM vda.vda_v_pat_accrual WHERE fk_study = pk_study AND FK_SITE_ENROLLING = a.fk_site AND TO_CHAR(TRUNC(patprot_enroldt),''yyyymm'') = TO_CHAR(TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT),''yyyy'') || ''11'') AS nov_acr,
		(SELECT COUNT(*) FROM vda.vda_v_pat_accrual WHERE fk_study = pk_study AND FK_SITE_ENROLLING = a.fk_site AND TO_CHAR(TRUNC(patprot_enroldt),''yyyymm'') = TO_CHAR(TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT),''yyyy'') || ''12'') AS dec_acr,
		(SELECT COUNT(*) FROM vda.vda_v_pat_accrual WHERE fk_study = pk_study AND FK_SITE_ENROLLING = a.fk_site) AS total_acr,
		(SELECT COUNT(*) FROM vda.vda_v_pat_accrual WHERE fk_study = pk_study AND FK_SITE_ENROLLING = a.fk_site AND TO_CHAR(TRUNC(patprot_enroldt),''yyyy'') = TO_CHAR(TO_NUMBER(TO_CHAR(TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT),''yyyy''))) - 1)  AS prevyr_acr,
		(SELECT COUNT(*) FROM vda.vda_v_pat_accrual WHERE fk_study = pk_study AND FK_SITE_ENROLLING = a.fk_site AND TO_CHAR(TRUNC(patprot_enroldt),''yyyy'') = TO_CHAR(TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT),''yyyy'')) AS curyr_total_acr,
		study_title AS study_title_complete,
		study_nsamplsize AS study_nsamplsize
		FROM ER_STUDYSITES a,ER_STUDY b
		WHERE (SELECT COUNT(*) FROM vda.vda_v_pat_accrual WHERE fk_study = pk_study AND FK_SITE_ENROLLING = a.fk_site ) > 0 and pk_study = fk_study AND a.fk_site IN (:orgId) AND pk_study IN (:studyId)';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=254;	
		END IF;
	
	
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=255;

		IF (row_check = 0) then
			INSERT INTO er_report (
				pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable
				)
			VALUES(
				 255
				,'Accrual by Study Site and Patient Status'
				,'Accrual by Study Site and Patient Status'
				,0
				,'N'
				,'Study Number, Organization, Type, Local Sample Size, First Patient Enrolled, # Screened, # Screen Failures, # Enrolled, # Off Treatment, # In Followup, # Off Study'
				,'Study (Study Number) </br> Organization (Study Site)'
				,1
				,'allsite_rep'
				,''
				,'studyId:orgId'
				);
				
		update_sql:='select 
		fk_study,
		fk_site,
		study_number,
		site_name,
		studysite_type,
		studysite_lsamplesize,
		TO_CHAR((select min(patprot_enroldt) from vda.vda_v_pat_accrual where vda.vda_v_pat_accrual.fk_study = a.fk_study and vda.vda_v_pat_accrual.fk_site_enrolling = a.fk_site),PKG_DATEUTIL.F_GET_DATEFORMAT) as first_enrolled_date,
		(SELECT COUNT(*) FROM vda.vda_v_pat_accrual WHERE vda.vda_v_pat_accrual.fk_study = a.fk_study AND vda.vda_v_pat_accrual.FK_SITE_ENROLLING = a.fk_site) AS  tot_pt,
		(SELECT COUNT(*) FROM ER_PATSTUDYSTAT x,ER_PATPROT y WHERE x.fk_per = y.fk_per AND a.fk_study = x.fk_study AND a.fk_study = y.fk_study AND y.fk_site_enrolling = a.fk_site AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''screening'')) AS  tot_screened,
		(SELECT COUNT(*) FROM ER_PATSTUDYSTAT x,ER_PATPROT y WHERE x.fk_per = y.fk_per AND a.fk_study = x.fk_study AND a.fk_study = y.fk_study AND y.fk_site_enrolling = a.fk_site AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''scrfail'')) AS  tot_screenfail,
		(SELECT COUNT(*) FROM ER_PATSTUDYSTAT x,ER_PATPROT y WHERE x.fk_per = y.fk_per AND a.fk_study = x.fk_study AND a.fk_study = y.fk_study AND y.fk_site_enrolling = a.fk_site AND  current_stat = 1 AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''followup'')) AS  tot_followup,
		(SELECT COUNT(*) FROM ER_PATSTUDYSTAT x,ER_PATPROT y WHERE x.fk_per = y.fk_per AND a.fk_study = x.fk_study AND a.fk_study = y.fk_study AND y.fk_site_enrolling = a.fk_site AND  current_stat = 1 AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''offstudy'')) AS  tot_offstudy,
		(SELECT COUNT(*) FROM ER_PATSTUDYSTAT x,ER_PATPROT y WHERE x.fk_per = y.fk_per AND a.fk_study = x.fk_study AND a.fk_study = y.fk_study AND y.fk_site_enrolling = a.fk_site AND  current_stat = 1 AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST 	WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''offtreat'')) AS  tot_offtreat
		from vda.vda_v_studysites a where include_in_reports = ''Yes''
		and fk_study IN (:studyId) and fk_site in (:orgId)';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=255;		
		END IF; 
	END IF;
				
COMMIT;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,382,3,'03_er_report_patch_Acc.sql',sysdate,'v10.1 #783');

commit;
