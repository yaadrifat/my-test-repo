declare
begin
	delete  from  er_repxsl where pk_repxsl in (128,129,130,136,137,138,139,140,141,142,143,144,145,154,155);
commit;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,382,10,'10_del_er_bio_repxsl.sql',sysdate,'v10.1 #783');

commit;
