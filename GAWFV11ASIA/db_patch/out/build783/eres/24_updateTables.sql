set define off;

update er_codelst set codelst_hide='Y'  where codelst_type='report' and codelst_subtyp='rep_ae_onestd';
commit; 

update er_codelst set codelst_hide='Y'  where codelst_type='report' and codelst_subtyp='rep_mpr';
commit;

update er_invoice SET inv_payment_dueby = '0' where IS_NUMBER(inv_payment_dueby)=0;
commit;



INSERT INTO track_patches
VALUES(seq_track_patches.nextval,382,24,'24_updateTables.sql',sysdate,'v10.1 #783');

commit;