declare
begin
	delete  from  er_repxsl where pk_repxsl in (97,181,182,254,256,257,258,265);
commit;
end;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,382,16,'16_del_er_repxsl.sql',sysdate,'v10.1 #783');

commit;