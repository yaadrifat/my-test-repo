DECLARE 
	table_check number;
	row_check number;

BEGIN
	SELECT count(*)
	INTO table_check
	FROM user_tab_cols
	WHERE TABLE_NAME = 'ER_REPORT';
	
	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=257;

		IF (row_check > 0) then
			UPDATE ER_REPORT SET REP_COLUMNS='Date Milestone Achievement, Study Number, Milestone Description, Enrolling Site, Patient Study ID,  Milestone Amount, Holdback,Payment Date, Reconciled, Outstanding' WHERE pk_report=257;					
		END IF; 
	END IF;
	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=258;

		IF (row_check > 0) then
			UPDATE ER_REPORT SET REP_COLUMNS='Date Milestone Achievement, Study Number, Payment For, Milestone Description, Enrolling Site, Patient Study ID,  Milestone Amount, Holdback,Payment Date, Reconciled, Outstanding' WHERE pk_report=258;					
		END IF; 
	END IF;
	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=265;

		IF (row_check > 0) then
			UPDATE ER_REPORT SET REP_COLUMNS='Date Milestone Achievement, Study Number, Milestone Description, Milestone Amount, Holdback,Payment Date, Reconciled, Outstanding' WHERE pk_report=265;					
		END IF; 
	END IF;
	commit;
end;

/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,382,20,'20_25573.sql',sysdate,'v10.1 #783');

commit;