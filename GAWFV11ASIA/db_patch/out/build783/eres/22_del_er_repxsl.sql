set define off;
delete from er_repxsl where pk_repxsl in (257,258,265);
commit;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,382,22,'22_del_er_repxsl.sql',sysdate,'v10.1 #783');

commit;