declare
begin
	delete  from  er_repxsl where pk_repxsl in (66,81,93,95,115,126,127);
commit;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,382,7,'07_del_er_pat_repxsl.sql',sysdate,'v10.1 #783');

commit;
