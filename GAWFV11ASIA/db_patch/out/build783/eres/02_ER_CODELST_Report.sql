DECLARE
	table_check number;
	row_check number;

BEGIN

	SELECT count(*)
	INTO table_check
	FROM user_tab_cols
	WHERE TABLE_NAME = 'ER_CODELST';

	
	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM er_codelst
		WHERE codelst_subtyp = 'allsite_rep' AND codelst_type='report';

		IF (row_check > 0) then
			UPDATE er_codelst 
			SET codelst_desc='Accrual Reports', codelst_custom_col='accrual' 
			WHERE codelst_subtyp='allsite_rep' AND codelst_type='report';	
			UPDATE ER_REPFILTERMAP 
			SET REPFILTERMAP_REPCAT='accrual' 
			WHERE REPFILTERMAP_REPCAT='data safety monitoring';
		END IF;

	
	
	
		SELECT count(*)
		INTO row_check
		FROM er_codelst
		WHERE codelst_subtyp = 'ms_rep' AND codelst_type='report';

		IF (row_check > 0) then
			UPDATE er_codelst 
			SET codelst_desc='Milestones, Invoices, Payments and Reconciliation'
			WHERE codelst_subtyp='ms_rep' AND codelst_type='report';			
		END IF;

	
	
	
		SELECT count(*)
		INTO row_check
		FROM er_codelst
		WHERE codelst_subtyp = 'mp_rep' AND codelst_type='report';

		IF (row_check > 0) then
			UPDATE er_codelst 
			SET codelst_desc='Budget, Coverage and Estimates (select one study only where applicable)'
			WHERE codelst_subtyp='mp_rep' AND codelst_type='report';			
		END IF;


	
	
		SELECT count(*)
		INTO row_check
		FROM er_codelst
		WHERE codelst_subtyp = 'rep_pcr' AND codelst_type='report';

		IF (row_check > 0) then
			UPDATE er_codelst 
			SET codelst_desc='Routine and Research Tickets (select one study and one patient only)', codelst_custom_col='financial'
			WHERE codelst_subtyp='rep_pcr' AND codelst_type='report';
		END IF;

	
	
	
		SELECT count(*)
		INTO row_check
		FROM er_codelst
		WHERE codelst_subtyp = 'rep_inv' AND codelst_type='report';

		IF (row_check > 0) then
			UPDATE er_codelst 
			SET codelst_custom_col='biospecimen'
			WHERE codelst_subtyp='rep_inv' AND codelst_type='report';
			UPDATE ER_REPFILTERMAP 
			SET REPFILTERMAP_REPCAT='biospecimen' 
			WHERE REPFILTERMAP_REPCAT='inventory';			
		END IF;

	
	
	
		SELECT count(*)
		INTO row_check
		FROM er_codelst
		WHERE codelst_subtyp = 'rep_storage' AND codelst_type='report';

		IF (row_check > 0) then
			UPDATE er_codelst 
			SET codelst_custom_col='biospecimen'
			WHERE codelst_subtyp='rep_storage' AND codelst_type='report';
		END IF;

	
	
	
		SELECT count(*)
		INTO row_check
		FROM er_codelst
		WHERE codelst_subtyp = 'rep_specimen' AND codelst_type='report';

		IF (row_check > 0) then
			UPDATE er_codelst 
			SET codelst_custom_col='biospecimen'
			WHERE codelst_subtyp='rep_specimen' AND codelst_type='report';		
		END IF;

	
	
	
		SELECT count(*)
		INTO row_check
		FROM er_codelst
		WHERE codelst_subtyp = 'rep_ae' AND codelst_type='report';

		IF (row_check > 0) then
			UPDATE er_codelst 
			SET codelst_desc='Schedule and Data Management', codelst_custom_col='patient'
			WHERE codelst_subtyp='rep_ae' AND codelst_type='report';	
		END IF;

	
	
	
		SELECT count(*)
		INTO row_check
		FROM er_codelst
		WHERE codelst_subtyp = 'rep_apr' AND codelst_type='report';

		IF (row_check > 0) then
			UPDATE er_codelst 
			SET codelst_desc='Demographics and Status'
			WHERE codelst_subtyp='rep_apr' AND codelst_type='report';			
		END IF;

	
	
	
		SELECT count(*)
		INTO row_check
		FROM er_codelst
		WHERE codelst_subtyp = 'rep_psr' AND codelst_type='report';

		IF (row_check > 0) then
			UPDATE er_codelst 
			SET codelst_desc='Patient Specific (select one study and one patient only)'
			WHERE codelst_subtyp='rep_psr' AND codelst_type='report';			
		END IF;

	
	
	
		SELECT count(*)
		INTO row_check
		FROM er_codelst
		WHERE codelst_subtyp = 'rep_pat_onestd' AND codelst_type='report';

		IF (row_check > 0) then
			UPDATE er_codelst 
			SET codelst_custom_col='study'
			WHERE codelst_subtyp='rep_pat_onestd' AND codelst_type='report';	
		END IF;
		
	END IF;
	
	COMMIT;
	
END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,382,2,'02_ER_CODELST_Report.sql',sysdate,'v10.1 #783');

commit;
