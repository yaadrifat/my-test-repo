set define off;
DECLARE 
	table_check number;
	row_check number;
	update_sql varchar2(4000);
BEGIN
	SELECT count(*)
	INTO table_check
	FROM user_tab_cols
	WHERE TABLE_NAME = 'ER_REPFILTERMAP';
	
	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM ER_REPFILTERMAP
		WHERE repfiltermap_repcat='financial' and fk_repfilter=(select pk_repfilter from er_repfilter where repfilter_keyword='studyId');
    
		IF(row_check>0) then
			update_sql:='<td><DIV id="studyDIV"><Font class=comments><A href="javascript:openLookup(document.reports,''viewId=6013&form=reports&seperator=,&defaultvalue=[ALL]&keyword=selstudyId|STUDY_NUMBER~paramstudyId|LKP_PK|[VELHIDE]'','''')">Select Study</Font></DIV></td>
						<td><DIV id="studydataDIV"> <input TYPE="text" NAME="selstudyId" readonly value="[ALL]"><input TYPE="hidden" NAME="paramstudyId" value="[ALL]"></DIV></td>';
			Update ER_REPFILTERMAP set REPFILTERMAP_COLUMN=update_sql
			where repfiltermap_repcat='financial' and fk_repfilter=(select pk_repfilter from er_repfilter where repfilter_keyword='studyId');
		End if;
		SELECT count(*)
		INTO row_check
		FROM ER_REPFILTERMAP
		WHERE repfiltermap_repcat='patient' and fk_repfilter=(select pk_repfilter from er_repfilter where repfilter_keyword='studyId');
    
		IF(row_check>0) then
			update_sql:='<td><DIV id="studyDIV"><Font class=comments><A href="javascript:openLookup(document.reports,''viewId=6013&form=reports&seperator=,&defaultvalue=[ALL]&keyword=selstudyId|STUDY_NUMBER~paramstudyId|LKP_PK|[VELHIDE]'','''')">Select Study</Font></DIV></td>
						<td><DIV id="studydataDIV"> <input TYPE="text" NAME="selstudyId" readonly value="[ALL]"><input TYPE="hidden" NAME="paramstudyId" value="[ALL]"></DIV></td>';
			Update ER_REPFILTERMAP set REPFILTERMAP_COLUMN=update_sql
			where repfiltermap_repcat='patient' and fk_repfilter=(select pk_repfilter from er_repfilter where repfilter_keyword='studyId');
		End if;
	End if;
commit;
End;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,382,19,'19_update_repfiltermap.sql',sysdate,'v10.1 #783');

commit;