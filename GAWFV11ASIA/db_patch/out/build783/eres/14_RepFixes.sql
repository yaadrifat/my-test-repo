DECLARE 
	table_check number;
	row_check number;
	update_sql clob;
BEGIN
	SELECT count(*)
	INTO table_check
	FROM user_tab_cols
	WHERE TABLE_NAME = 'ER_REPORT';
	
	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=1;
		
		IF (row_check > 0) then	
			UPDATE 
				ER_REPORT 
			SET 
				rep_type='rep_spr'					
			WHERE 
				pk_report=1;			
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=280;
		
		IF (row_check > 0) then	
			UPDATE 
				ER_REPORT 
			SET 
				REP_FILTERAPPLICABLE='date:studyId:orgId'					
			WHERE 
				pk_report=280;			
		END IF;
		
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=145;
		
		IF (row_check > 0) then	
			UPDATE 
				ER_REPORT 
			SET 
				REP_COLUMNS='Specimen Details, Specimen Status, Procedure Type, Hand-Off Date, Process Date, Process Quantity, Process Units, Recipient, Status By, For Study',
				REP_FILTERBY='Date, Study, Organization'
			WHERE 
				pk_report=145;			
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=138;
		
		IF (row_check > 0) then	
			UPDATE 
				ER_REPORT 
			SET 
				REP_COLUMNS='Specimen Details, Specimen Status, Procedure Type, Hand-Off Date, Process Date, Process Quantity, Process Units, Recipient, Status By, For Study',
				REP_FILTERBY='Date, Study, Organization'
			WHERE 
				pk_report=138;			
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=139;
		
		IF (row_check > 0) then	
			UPDATE 
				ER_REPORT 
			SET 
				REP_COLUMNS='Parent Storage Details, Child Storage ID, Child Storage Name, Child Storage Alternate ID, Child Storage Type, Child Storage Unit Class, Child Storage Status'
			WHERE 
				pk_report=139;			
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=142;
		
		IF (row_check > 0) then	
			UPDATE 
				ER_REPORT 
			SET 
				REP_COLUMNS='Specimen ID, Child Specimen Count, Parent Specimen ID, Study Number, Specimen Type, Specimen Quantity',
				REP_FILTERBY='Date, Study, Storage, Organization'
			WHERE 
				pk_report=142;			
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=121;

		IF (row_check <> 0) then		
		UPDATE ER_REPORT SET REP_FILTERKEYWORD='[DATEFILTER]YMD:studyId',REP_FILTERAPPLICABLE='date:studyId' WHERE pk_report=121;		
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=181;

		IF (row_check <> 0) then	
         update_sql:=' SELECT
a.study_number,
a.pk_study,
a.study_title,
DECODE(study_prinv,NULL,DECODE(study_otherprinv,NULL,'''',study_otherprinv),(SELECT usr_lastname || '','' || usr_firstname FROM ER_USER WHERE pk_user = study_prinv)

|| DECODE(study_otherprinv,NULL,'''',''; '' || study_otherprinv)) AS PI,
F_Getlocal_Samplesize(a.pk_study) AS study_samplsize,
study_nsamplsize,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = study_division) AS division,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_phase) AS phase,
(SELECT TO_CHAR(max(studystat_date),PKG_DATEUTIL.F_GET_DATEFORMAT)
FROM ER_STUDYSTAT WHERE fk_study = a.pk_study AND fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''studystat'' AND codelst_subtyp =

''active_cls'')
) AS closed_to_accr_dt,
(SELECT TO_CHAR(max(studystat_date),PKG_DATEUTIL.F_GET_DATEFORMAT)
FROM ER_STUDYSTAT WHERE fk_study = a.pk_study AND fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''studystat'' AND codelst_subtyp =

(select ctrl_value from ER_CTRLTAB where ctrl_key = ''irb_app_stat''))
) AS chrstat_validfrm_dt,
(SELECT TO_CHAR(max(studystat_validt),PKG_DATEUTIL.F_GET_DATEFORMAT)
FROM ER_STUDYSTAT  WHERE fk_study = a.pk_study AND fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''studystat'' AND codelst_subtyp =

(select ctrl_value from ER_CTRLTAB where ctrl_key = ''irb_app_stat''))
) AS chrstat_validuntil_dt,
(select distinct sstat_study_status from vda.vda_v_studystat where sstat_current_stat = ''Yes'' and fk_study = a.pk_study) as currentstat,
TO_CHAR(first_irb_approved,PKG_DATEUTIL.F_GET_DATEFORMAT) first_irb_approved,
TO_CHAR(first_active,PKG_DATEUTIL.F_GET_DATEFORMAT) first_active,
TO_CHAR(last_perm_closure,PKG_DATEUTIL.F_GET_DATEFORMAT) last_perm_closure,
days_irb_approval, days_notactive_to_active,
(SELECT site_name FROM ER_SITE WHERE pk_site = fk_site) AS site_name,
(SELECT TO_CHAR(MIN(patstudystat_date),PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT x,ER_PER y WHERE pk_per = fk_per AND a.pk_study = x.fk_study AND y.fk_site

= c.fk_site AND  fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''enrolled'')) AS  first_enrolled_date,
(SELECT COUNT(*) FROM ER_PATSTUDYSTAT x,ER_PER y WHERE pk_per = fk_per AND a.pk_study = x.fk_study AND y.fk_site = c.fk_site AND  fk_codelst_stat = (SELECT pk_codelst

FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''enrolled'')) AS  tot_enrolled,
(SELECT COUNT(*) FROM ER_PATSTUDYSTAT x,ER_PER y WHERE pk_per = fk_per AND a.pk_study = x.fk_study AND y.fk_site = c.fk_site AND  fk_codelst_stat = (SELECT pk_codelst

FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''screening'')) AS  tot_screened,
(SELECT COUNT(*) FROM ER_PATSTUDYSTAT x,ER_PER y WHERE pk_per = fk_per AND a.pk_study = x.fk_study AND y.fk_site = c.fk_site AND  fk_codelst_stat = (SELECT pk_codelst

FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''scrfail'')) AS  tot_screenfail,
(SELECT COUNT(*) FROM ER_PATSTUDYSTAT x,ER_PER y WHERE pk_per = fk_per AND a.pk_study = x.fk_study AND y.fk_site = c.fk_site AND fk_codelst_stat

= (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''followup'')) AS  tot_followup,
(SELECT COUNT(*) FROM ER_PATSTUDYSTAT x,ER_PER y WHERE pk_per = fk_per AND a.pk_study = x.fk_study AND y.fk_site = c.fk_site AND fk_codelst_stat

= (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''offstudy'')) AS  tot_offstudy,
(SELECT COUNT(*) FROM ER_PATSTUDYSTAT x,ER_PER y WHERE pk_per = fk_per AND a.pk_study = x.fk_study AND y.fk_site = c.fk_site AND fk_codelst_stat

= (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''offtreat'')) AS  tot_offtreat
FROM ER_STUDY a, vda.vda_v_studystatus_metrics b, ER_STUDYSITES c
where a.pk_study = b.pk_study and a.pk_study = fk_study
AND a.pk_study IN (:studyId)';		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=281;	
		END IF;
		
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=255;
		
		IF (row_check > 0) then	
			
			update_sql:=' select
		fk_study,
		fk_site,
		study_number,
		site_name,
		studysite_type,
		studysite_lsamplesize,
		TO_CHAR((select min(patprot_enroldt) from vda.vda_v_pat_accrual where vda.vda_v_pat_accrual.fk_study = a.fk_study and vda.vda_v_pat_accrual.fk_site_enrolling = a.fk_site),PKG_DATEUTIL.F_GET_DATEFORMAT) as first_enrolled_date,
		(SELECT COUNT(*) FROM vda.vda_v_pat_accrual WHERE vda.vda_v_pat_accrual.fk_study = a.fk_study AND vda.vda_v_pat_accrual.FK_SITE_ENROLLING = a.fk_site) AS  tot_pt,
		(SELECT COUNT(*) FROM ER_PATSTUDYSTAT x,ER_PATPROT y WHERE x.fk_per = y.fk_per AND a.fk_study = x.fk_study AND a.fk_study = y.fk_study AND y.fk_site_enrolling = a.fk_site AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''screening'')) AS  tot_screened,
		(SELECT COUNT(*) FROM ER_PATSTUDYSTAT x,ER_PATPROT y WHERE x.fk_per = y.fk_per AND a.fk_study = x.fk_study AND a.fk_study = y.fk_study AND y.fk_site_enrolling = a.fk_site AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''scrfail'')) AS  tot_screenfail,
		(SELECT COUNT(*) FROM ER_PATSTUDYSTAT x,ER_PATPROT y WHERE x.fk_per = y.fk_per AND a.fk_study = x.fk_study AND a.fk_study = y.fk_study AND y.fk_site_enrolling = a.fk_site  AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''followup'')) AS  tot_followup,
		(SELECT COUNT(*) FROM ER_PATSTUDYSTAT x,ER_PATPROT y WHERE x.fk_per = y.fk_per AND a.fk_study = x.fk_study AND a.fk_study = y.fk_study AND y.fk_site_enrolling = a.fk_site  AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''offstudy'')) AS  tot_offstudy,
		(SELECT COUNT(*) FROM ER_PATSTUDYSTAT x,ER_PATPROT y WHERE x.fk_per = y.fk_per AND a.fk_study = x.fk_study AND a.fk_study = y.fk_study AND y.fk_site_enrolling = a.fk_site AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST 	WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''offtreat'')) AS  tot_offtreat
		from vda.vda_v_studysites a where include_in_reports = ''Yes''
		and fk_study IN (:studyId) and fk_site in (:orgId)';
		  
			UPDATE 
				ER_REPORT 
			SET 
				REP_SQL_CLOB=update_sql				
			WHERE 				
				pk_report=255;
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=280;
		
		IF (row_check > 0) then	
			UPDATE 
				ER_REPORT 
			SET 
				REP_FILTERAPPLICABLE='date:studyId:orgId'					
			WHERE 				
				pk_report=280;
		END IF;
				SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=276;

		IF (row_check > 0) then
		update_sql:=' select
fk_study,
study_number,
enrolling_site_name,
pat_study_id,
calendar_name,
calendar_stat,
visit_name,
evrec_event,
evrec_resource,
TO_CHAR(ptsch_actsch_date,PKG_DATEUTIL.F_GET_DATEFORMAT) ptsch_actsch_date,
ptsch_event_stat,
duration_minutes,
to_char(duration_minutes/60,''9999999999.99'') as duration_hours
from VDA.vda_v_studycal_resource_bypat where ptsch_event_stat_subtype != ''ev_done'' and calendar_stat_subtype != ''D''
AND  TRUNC(ptsch_actsch_date) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
and fk_study IN (:studyId)';
UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=276;
    END IF;
			SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=279;

		IF (row_check > 0) then
		update_sql:=' select
fk_study,
study_number,
enrolling_site_name,
pat_study_id,
calendar_name,
calendar_stat,
visit_name,
evrec_event,
evrec_resource,
TO_CHAR(ptsch_actsch_date,PKG_DATEUTIL.F_GET_DATEFORMAT) ptsch_actsch_date,
ptsch_event_stat,
duration_minutes,
to_char(duration_minutes/60,''9999999999.99'') as duration_hours
from VDA.vda_v_studycal_resource_bypat where ptsch_event_stat = ''ev_done''
AND  TRUNC(ptsch_actsch_date) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
and fk_study IN (:studyId)';
UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=279;					
		END IF;
		    SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=273;
  
    if(row_check>0) then
      update er_report set REP_COLUMNS='Study Number, Study Start Date, Organization, Status Type, Study Status, Current Status, Date From, Date Until, Notes, Documented By' where pk_report=273;
    end if;
  end if;
  
  if(table_check>0) then
    SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=275;
    
    if(row_check>0) then
      update er_report set REP_COLUMNS='Study Number, Study Entered On, Initial IRB Approval, Days to IRB Approval, Study Activation Date, Days to Activation, Study Closure Date, Days to Closure' where pk_report=275;
    end if;
  end if;
  
  if(row_check>0) then
    SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=281;
    
    if(row_check>0) then
      update er_report set REP_COLUMNS='Study Number, Title, PI, Division, Phase, IRB Approval Date, Open to Enrollment Date, Closed to Enrollment Date, Current Status, Current Accrual % of Target, Accrual Per Enrolling Site by Status' where pk_report=281;
    end if;
			SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=258;

		IF (row_check > 0) then
			update_sql:='select
study_number,
milestone_type,
msach_milestone_desc,
fk_site_enrolling,
enrolling_site,
msach_ptstudy_id,
TO_CHAR(msach_ach_date,PKG_DATEUTIL.F_GET_DATEFORMAT) msach_ach_date,
milestone_amt,
milestone_holdback,
payment_type,
payment_for,
fk_study,
fk_milestone,
inv_amt,
inv_num,
rec_amt,
payment_dt
from VDA.vda_v_milestone_achvd_det
where PAYMENT_SUBTYPE = ''pay''
AND fk_study IN (:studyId)
AND TRUNC(MSACH_ACH_DATE) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=258;					
		END IF;
END IF;		
	
	COMMIT;
END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,382,14,'14_RepFixes.sql',sysdate,'v10.1 #783');

commit;