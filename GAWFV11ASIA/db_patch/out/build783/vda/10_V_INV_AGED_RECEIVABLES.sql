CREATE OR REPLACE FORCE VIEW "VDA"."VDA_V_INV_AGED_RECEIVABLES" ("PK_INVOICE", "STUDY_NUMBER", "INVOICE_DATE", "INVOICE_DUEDATE", "STUDY_SPONSOR", "INV_NUMBER", "AMOUNT_OVERDUE", "0_30D_OVERDUE", "31_60D_OVERDUE", "61_90D_OVERDUE", "OVER_90D_OVERDUE", "INVOICE_AMOUNT", "PAYMENT", "PK_STUDY")
AS
  SELECT PK_INVOICE,
    STUDY_NUMBER,
    INV_DATE INVOICE_DATE,
    (INV_DATE + eres.F_CALC_DAYS (INV_PAYUNIT, INV_PAYMENT_DUEBY)) AS INVOICE_DUEDATE,
    DECODE (FK_CODELST_SPONSOR, NULL, STUDY_SPONSOR, eres.F_GET_CODELSTDESC (FK_CODELST_SPONSOR)) STUDY_SPONSOR,
    INV_NUMBER,
    (SUM (NVL (AMOUNT_INVOICED, 0)) - NVL (
    (SELECT SUM(NVL(mp_amount,0) + NVL(mp_holdback_amount,0))
    FROM eres.ER_MILEPAYMENT_DETAILS
    WHERE MP_LINKTO_ID = PK_INVOICE
    AND MP_LINKTO_TYPE = 'P'
    ), 0 )) AS AMOUNT_OVERDUE,
    CASE
      WHEN (TRUNC(SYSDATE -
        (SELECT (INV_DATE + eres.F_CALC_DAYS ( INV_PAYUNIT, INV_PAYMENT_DUEBY )) AS INVDUEDATE
        FROM eres.ER_INVOICE
        WHERE PK_INVOICE = INV.PK_INVOICE
        ))) BETWEEN 0 AND 30
      THEN (SUM (NVL (AMOUNT_INVOICED, 0)) - NVL (
        (SELECT SUM(NVL(mp_amount,0) + NVL(mp_holdback_amount,0))
        FROM eres.ER_MILEPAYMENT_DETAILS
        WHERE MP_LINKTO_ID = PK_INVOICE
        AND MP_LINKTO_TYPE = 'P'
        ), 0 ))
      ELSE 0
    END AS "0_30D_OVERDUE",
    CASE
      WHEN (TRUNC(SYSDATE -
        (SELECT (INV_DATE + eres.F_CALC_DAYS ( INV_PAYUNIT, INV_PAYMENT_DUEBY )) AS INVDUEDATE
        FROM eres.ER_INVOICE
        WHERE PK_INVOICE = INV.PK_INVOICE
        ))) BETWEEN 31 AND 60
      THEN (SUM (NVL (AMOUNT_INVOICED, 0)) - NVL (
        (SELECT SUM(NVL(mp_amount,0) + NVL(mp_holdback_amount,0))
        FROM eres.ER_MILEPAYMENT_DETAILS
        WHERE MP_LINKTO_ID = PK_INVOICE
        AND MP_LINKTO_TYPE = 'P'
        ), 0 ))
      ELSE 0
    END AS "31_60D_OVERDUE",
    CASE
      WHEN (TRUNC(SYSDATE -
        (SELECT (INV_DATE + eres.F_CALC_DAYS ( INV_PAYUNIT, INV_PAYMENT_DUEBY )) AS INVDUEDATE
        FROM eres.ER_INVOICE
        WHERE PK_INVOICE = INV.PK_INVOICE
        ))) BETWEEN 61 AND 90
      THEN ( (SUM (NVL (AMOUNT_INVOICED, 0))) - NVL (
        (SELECT SUM(NVL(mp_amount,0) + NVL(mp_holdback_amount,0))
        FROM eres.ER_MILEPAYMENT_DETAILS
        WHERE MP_LINKTO_ID = PK_INVOICE
        AND MP_LINKTO_TYPE = 'P'
        ), 0 ))
      ELSE 0
    END AS "61_90D_OVERDUE",
    CASE
      WHEN (TRUNC(SYSDATE -
        (SELECT (INV_DATE + eres.F_CALC_DAYS ( INV_PAYUNIT, INV_PAYMENT_DUEBY )) AS INVDUEDATE
        FROM eres.ER_INVOICE
        WHERE PK_INVOICE = INV.PK_INVOICE
        )))              > 90
      THEN (SUM (NVL (AMOUNT_INVOICED, 0)) - NVL (
        (SELECT SUM(NVL(mp_amount,0) + NVL(mp_holdback_amount,0))
        FROM eres.ER_MILEPAYMENT_DETAILS
        WHERE MP_LINKTO_ID = PK_INVOICE
        AND MP_LINKTO_TYPE = 'P'
        ), 0 ))
      ELSE 0
    END                              AS "OVER_90D_OVERDUE",
    (SUM (NVL (AMOUNT_INVOICED, 0))) AS INVOICE_AMOUNT,
    NVL (
    (SELECT SUM(NVL(mp_amount,0) + NVL(mp_holdback_amount,0))
    FROM eres.ER_MILEPAYMENT_DETAILS
    WHERE MP_LINKTO_ID = PK_INVOICE
    AND MP_LINKTO_TYPE = 'P'
    ), 0 ) AS PAYMENT,
    PK_STUDY
  FROM eres.ER_STUDY STUDY,
    eres.ER_INVOICE INV,
    eres.ER_INVOICE_DETAIL INVDETAIL
  WHERE STUDY.PK_STUDY      = INV.FK_STUDY
  AND INV.PK_INVOICE        = INVDETAIL.FK_INV
  AND INVDETAIL.DETAIL_TYPE = 'H'
  GROUP BY PK_INVOICE,
    STUDY_NUMBER,
    INV_DATE,
    INV_DATE + eres.F_CALC_DAYS (INV_PAYUNIT, INV_PAYMENT_DUEBY),
    DECODE (FK_CODELST_SPONSOR, NULL, STUDY_SPONSOR, eres.F_GET_CODELSTDESC (FK_CODELST_SPONSOR)),
    INV_NUMBER,
    PK_STUDY
  HAVING (SUM (NVL (AMOUNT_INVOICED, 0)) - NVL (
    (SELECT SUM(NVL(mp_amount,0) + NVL(mp_holdback_amount,0))
    FROM eres.ER_MILEPAYMENT_DETAILS
    WHERE MP_LINKTO_ID = PK_INVOICE
    AND MP_LINKTO_TYPE = 'P'
    ), 0 ))            > 0;
  COMMENT ON COLUMN "VDA"."VDA_V_INV_AGED_RECEIVABLES"."PK_INVOICE"
IS
  'Primary Key of Invoice';
  COMMENT ON COLUMN "VDA"."VDA_V_INV_AGED_RECEIVABLES"."STUDY_NUMBER"
IS
  'The Study Number';
  COMMENT ON COLUMN "VDA"."VDA_V_INV_AGED_RECEIVABLES"."INVOICE_DATE"
IS
  'The Invoice Due Date';
  COMMENT ON COLUMN "VDA"."VDA_V_INV_AGED_RECEIVABLES"."STUDY_SPONSOR"
IS
  'The Study Sponsor (from
summary), if study sponsor field is empty, it gets ''if other''';
  COMMENT ON COLUMN "VDA"."VDA_V_INV_AGED_RECEIVABLES"."INV_NUMBER"
IS
  'The Invoice Number';
  COMMENT ON COLUMN "VDA"."VDA_V_INV_AGED_RECEIVABLES"."AMOUNT_OVERDUE"
IS
  'The amount overdue for the
invoice';
  COMMENT ON COLUMN "VDA"."VDA_V_INV_AGED_RECEIVABLES"."0_30D_OVERDUE"
IS
  'The amount overdue for 0
-30 days';
  COMMENT ON COLUMN "VDA"."VDA_V_INV_AGED_RECEIVABLES"."31_60D_OVERDUE"
IS
  'The amount overdue for
31-60  days';
  COMMENT ON COLUMN "VDA"."VDA_V_INV_AGED_RECEIVABLES"."61_90D_OVERDUE"
IS
  'The amount overdue for
61-90 days';
  COMMENT ON COLUMN "VDA"."VDA_V_INV_AGED_RECEIVABLES"."OVER_90D_OVERDUE"
IS
  'The amount overdue
forover 90 days';
  COMMENT ON COLUMN "VDA"."VDA_V_INV_AGED_RECEIVABLES"."INVOICE_AMOUNT"
IS
  'The original invoice
amount';
  COMMENT ON COLUMN "VDA"."VDA_V_INV_AGED_RECEIVABLES"."PAYMENT"
IS
  'The payment amount (if any)
reconciled with the invoice';
  COMMENT ON COLUMN "VDA"."VDA_V_INV_AGED_RECEIVABLES"."PK_STUDY"
IS
  'Primary Key of the Study';
  COMMENT ON TABLE "VDA"."VDA_V_INV_AGED_RECEIVABLES"
IS
  'This view provides access to aged
receivables information. The receivables are calculated by linking with payments that are
reconciled with invoives and eliminating any invoice that is fully reconciled with a
payment.';
/

INSERT INTO VDA_TRACK_PATCHES
(
  PATCH_PK       ,
  DB_VER_MJR     ,
  DB_VER_MNR     ,
  DB_PATCH_NAME  ,
  FIRED_ON       ,
  APP_VERSION    
) VALUES(SEQ_VDA_TRACK_PATCHES.nextval,10,0,'10_V_INV_AGED_RECEIVABLES.sql',sysdate,'VDA1.4#18Reports') ;

commit	;	    