create or replace TRIGGER ER_STATWORKFLOW_AU0
AFTER UPDATE
ON ER_CODELST REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
v_old_statusdesc varchar2(4000);
v_new_statusdesc varchar2(4000);

begin
/* Trigger to update workflow Description when status description updated for status driven workflows */
if (:new.CODELST_TYPE='studystat') then
if (:new.CODELST_DESC!=:old.CODELST_DESC) then
     update ACTIVITY set ACTIVITY_DESC=:new.CODELST_DESC where ACTIVITY_DESC=:old.CODELST_DESC;
     update WORKFLOW_ACTIVITY set WA_NAME=:new.CODELST_DESC where WA_NAME=:old.CODELST_DESC;
     commit;
end if;
end if;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,361,3,'03_TR_ER_STATWORKFLOW_AU0.sql',sysdate,'v10 #762');

commit;
