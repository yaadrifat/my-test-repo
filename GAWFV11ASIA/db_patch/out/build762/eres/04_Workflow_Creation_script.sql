set define off;

declare
v_exists NUMBER := 0;
v_nextVal NUMBER := 0;
v_PK_WORKFLOW NUMBER := 0;
v_PK_ACTIVITY NUMBER := 0;
v_PK_WORKFLOWACTIVITY NUMBER := 0;
begin
   SELECT count(*) INTO v_exists FROM WORKFLOW WHERE WORKFLOW_TYPE = 'STUDY_ACTIVATION';
	
   IF (v_exists = 0) THEN
       v_PK_WORKFLOW := SQ_WORKFLOW.NEXTVAL();

       Insert into WORKFLOW (PK_WORKFLOW, WORKFLOW_NAME, WORKFLOW_DESC, WORKFLOW_TYPE)
       values (v_PK_WORKFLOW,'Study Initiation','Study Initiation','STUDY_ACTIVATION');
       COMMIT;
   ELSE
       SELECT PK_WORKFLOW INTO v_PK_WORKFLOW FROM WORKFLOW WHERE WORKFLOW_TYPE = 'STUDY_ACTIVATION';
   END IF;

   -------------------------------------------------------------------------------------------------------
   --Adding activity named "Protocol Activation" in the activity and workflow_activity table---
   -------------------------------------------------------------------------------------------------------
   SELECT count(*) INTO v_exists FROM ACTIVITY WHERE ACTIVITY_NAME = 'protActivation';
	
   IF (v_exists = 0) THEN
       v_PK_ACTIVITY := SQ_ACTIVITY.NEXTVAL();

       Insert into ACTIVITY (PK_ACTIVITY,ACTIVITY_NAME,ACTIVITY_DESC)
       values (v_PK_ACTIVITY,'protActivation',(select codelst_desc from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'active' and codelst_hide='N'));
       COMMIT;
   ELSE
       SELECT PK_ACTIVITY INTO v_PK_ACTIVITY FROM ACTIVITY WHERE ACTIVITY_NAME = 'protActivation';
   END IF;
	
   IF (v_PK_ACTIVITY > 0) THEN
       --Workflow and activity association
       SELECT COUNT(*) INTO v_exists FROM WORKFLOW_ACTIVITY WHERE FK_WORKFLOWID = v_PK_WORKFLOW AND FK_ACTIVITYID = v_PK_ACTIVITY;
	
       IF (v_exists = 0) THEN
           v_PK_WORKFLOWACTIVITY := SQ_WORKFLOWACTIVITY.NEXTVAL();

           Insert into WORKFLOW_ACTIVITY (PK_WORKFLOWACTIVITY, WA_NAME, FK_WORKFLOWID, FK_ACTIVITYID, WA_SEQUENCE, WA_TERMINATEFLAG, WA_DISPLAYQUERY)
           values (v_PK_WORKFLOWACTIVITY, (select codelst_desc from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'active' and codelst_hide='N'), v_PK_WORKFLOW, v_PK_ACTIVITY, 1, 0, 'SELECT PKG_WORKFLOW_RULES.F_studyActive_Enrolling(:1) FROM DUAL');
           COMMIT;
       END IF;
   END IF;
   
   SELECT PK_WORKFLOW INTO v_PK_WORKFLOW FROM WORKFLOW WHERE WORKFLOW_TYPE = 'STUDY_INITIATION';
   SELECT PK_ACTIVITY INTO v_PK_ACTIVITY FROM ACTIVITY WHERE ACTIVITY_NAME = 'IRB_Review';
   
   IF (v_PK_ACTIVITY >0) then
     update ACTIVITY set ACTIVITY_DESC=(select codelst_desc from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'app_CHR' and codelst_hide='N') where PK_ACTIVITY=v_PK_ACTIVITY;
     update WORKFLOW_ACTIVITY set WA_NAME=(select codelst_desc from ER_CODELST where CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'app_CHR' and codelst_hide='N') where FK_ACTIVITYID=v_PK_ACTIVITY and FK_WORKFLOWID=v_PK_WORKFLOW;
     commit;
   END IF;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,361,4,'04_Workflow_Creation_script.sql',sysdate,'v10 #762');

commit;