declare
update_sql varchar(4000);
update_sql_new varchar(4000);

begin
update_sql:='SELECT
PROT_NAME,
PROT_DESC,
EVENT_ID,
PROT_DURATION,
VISIT_NAME,
VISIT_DESC,
nvl(trim(VISIT_INTERVAL),''No Interval Defined'') as VISIT_INTERVAL,
EVENT_NAME,
EVENT_DESC,
DECODE(trim(vwindow_bef),''(BEFORE)'','''',vwindow_bef) AS vwindow_bef,
DECODE(trim(vwindow_aft),''(AFTER)'','''',vwindow_aft) AS vwindow_aft,
EVENT_COST ,
RESOURCES,
APPENDIX,
MESSAGES,
FORMS,insert_after,decode(nvl(hide_flag,0),1,''Yes'',''No'')
 is_hidden,
 COVERAGE_TYPE,
EVENT_CPTCODE
,visit_win_before, visit_win_after
FROM  erv_calendar_template
WHERE prot_id = ~1 and nvl(EVENT_TYPE,''A'') <>''U''
ORDER BY displacement,PK_PROTOCOL_VISIT,event_sequence';

update_sql_new:='SELECT
PROT_ID,PROT_NAME,PROT_DESC,
EVENT_ID,
PROT_DURATION,decode(displacement,null,''No Interval'',nvl(sch_date_grp, ''No Interval'')) as sch_date_grp,
decode(displacement,null,''No Interval Defined'',nvl(sch_date_grp_disp, ''No Interval Defined'')) as sch_date_grp_disp,
pkg_dateutil.f_get_first_dayofyear_str AS cal_start_date,
decode(displacement,null,''Not Defined'',(TO_CHAR(SCH_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT))) AS sch_date,
TO_CHAR((sch_date - FUZZY_DURATION_bef),PKG_DATEUTIL.F_GET_DATEFORMAT) || '' - '' || TO_CHAR((sch_date + FUZZY_DURATION_aft),PKG_DATEUTIL.F_GET_DATEFORMAT) AS fuzzy_dates,
VISIT_NAME,
VISIT_DESC,
VISIT_INTERVAL,
EVENT_NAME,
EVENT_DESC,
FORMS,
displacement,
COVERAGE_TYPE, visit_win_before, visit_win_after
FROM erv_calendar_template
WHERE prot_id = ~1 and nvl(EVENT_TYPE,''A'') <>''U''  and
nvl(HIDE_FLAG,0)=0
ORDER BY SCH_DATE,PK_PROTOCOL_VISIT,event_sequence';

update er_report set rep_sql=update_sql,rep_sql_clob=update_sql where pk_report=106;
update er_report set rep_sql=update_sql_new,rep_sql_clob=update_sql_new where pk_report=107;
commit;

Update ER_REPORT set REP_SQL='SELECT inv_number,
  int_acc_num,
  (SELECT study_number FROM er_study WHERE pk_study = a.fk_study
  ) AS studynum,
  (SELECT nct_number FROM er_study WHERE pk_study = a.fk_study
  ) AS nctnum,
  (SELECT study_title FROM er_study WHERE pk_study = a.fk_study
  ) AS studytitle,
  TO_CHAR(inv_date,PKG_DATEUTIL.F_GET_DATEFORMAT) inv_date,
  DECODE(trim(inv_payunit), ''D'' , TO_CHAR(to_date(TO_CHAR(inv_date, PKG_DATEUTIL.F_GET_DATEFORMAT), PKG_DATEUTIL.F_GET_DATEFORMAT) + inv_payment_dueby , PKG_DATEUTIL.F_GET_DATEFORMAT ), ''W'' , TO_CHAR(to_date(TO_CHAR(inv_date, PKG_DATEUTIL.F_GET_DATEFORMAT), PKG_DATEUTIL.F_GET_DATEFORMAT) + inv_payment_dueby * 7 , PKG_DATEUTIL.F_GET_DATEFORMAT ), ''M'' , TO_CHAR(add_months(to_date(inv_date) , inv_payment_dueby),PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(add_months(to_date(inv_date) , inv_payment_dueby * 12),PKG_DATEUTIL.F_GET_DATEFORMAT) ) AS duedays,
  (SELECT SITE_NAME
  FROM ER_SITE,
    ER_USER
  WHERE PK_USER = INV_ADDRESSEDTO
  AND PK_SITE   = FK_SITEID
  ) AS orgto,
  (SELECT USR_FIRSTNAME
    || '' ''
    || USR_LASTNAME
  FROM ER_USER
  WHERE PK_USER = INV_ADDRESSEDTO
  ) AS addto,
  (SELECT address
  FROM ER_ADD,
    ER_USER
  WHERE pk_add = fk_peradd
  AND PK_USER  = INV_ADDRESSEDTO
  ) AS addto_add,
  (SELECT add_city
    || '' ''
    || add_state
  FROM ER_ADD,
    ER_USER
  WHERE pk_add = fk_peradd
  AND PK_USER  = INV_ADDRESSEDTO
  ) AS addto_add1,
  (SELECT add_country
    || '' ''
    || add_zipcode
  FROM ER_ADD,
    ER_USER
  WHERE pk_add = fk_peradd
  AND PK_USER  = INV_ADDRESSEDTO
  ) AS addto_add2,
  (SELECT SITE_NAME
  FROM ER_SITE,
    ER_USER
  WHERE PK_USER = inv_sentfrom
  AND PK_SITE   = FK_SITEID
  ) AS orgfrom,
  (SELECT USR_FIRSTNAME
    || '' ''
    || USR_LASTNAME
  FROM ER_USER
  WHERE PK_USER = inv_sentfrom
  ) AS sentfrom,
  (SELECT address
  FROM ER_ADD,
    ER_USER
  WHERE pk_add = fk_peradd
  AND PK_USER  = inv_sentfrom
  ) AS sentfrom_add,
  (SELECT add_city
    || '' ''
    || add_state
  FROM ER_ADD,
    ER_USER
  WHERE pk_add = fk_peradd
  AND PK_USER  = inv_sentfrom
  ) AS sentfrom_add1,
  (SELECT add_country
    || '' ''
    || add_zipcode
  FROM ER_ADD,
    ER_USER
  WHERE pk_add = fk_peradd
  AND PK_USER  = inv_sentfrom
  ) AS sentfrom_add2,
  (SELECT patprot_patstdid
  FROM ER_PATPROT
  WHERE ER_PATPROT.fk_study = a.fk_study
  AND ER_PATPROT.fk_per     = b.fk_per
  AND patprot_stat          = 1
  ) fk_per,
  milestones_achieved,
  TO_CHAR(achieved_on,PKG_DATEUTIL.F_GET_DATEFORMAT) achieved_on,
  amount_due,
  ROUND(amount_invoiced,2) amount_invoiced,
  ROUND(amount_holdback,2) amount_holdback,
  detail_type,
  display_detail,
  inv_notes,
  milestone_type AS TYPE,
  (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = milestone_status
  ) AS status,
  (SELECT name FROM esch.event_assoc WHERE event_id = fk_cal
  ) AS cal,
  (SELECT name FROM esch.event_assoc WHERE event_id = fk_eventassoc
  ) AS event,
  (SELECT visit_name
  FROM esch.sch_protocol_visit
  WHERE pk_protocol_visit = fk_visit
  ) AS visit,
  NVL(trim(
  (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = milestone_status
  )
  || '' ''
  ||
  (SELECT visit_name
  FROM esch.sch_protocol_visit
  WHERE pk_protocol_visit = fk_visit
  )),''All'')
  || ''; ''     AS milestone_type,
  fk_per AS pk_per
FROM ER_INVOICE a,
  ER_INVOICE_DETAIL b,
  er_milestone c
WHERE fk_inv         = pk_invoice
AND fk_milestone     = pk_milestone
AND pk_invoice       = ~1
AND NVL(b.fk_per,0) <> 0
AND milestone_type  <> ''SM'''where pk_report = 131;
  commit;

Update ER_REPORT set REP_SQL='SELECT inv_number,
  int_acc_num,
  (SELECT study_number FROM er_study WHERE pk_study = a.fk_study
  ) AS studynum,
  (SELECT nct_number FROM er_study WHERE pk_study = a.fk_study
  ) AS nctnum,
  (SELECT study_title FROM er_study WHERE pk_study = a.fk_study
  ) AS studytitle,
  TO_CHAR(inv_date,PKG_DATEUTIL.F_GET_DATEFORMAT) inv_date,
  DECODE(trim(inv_payunit), ''D'' , TO_CHAR(to_date(TO_CHAR(inv_date, PKG_DATEUTIL.F_GET_DATEFORMAT), PKG_DATEUTIL.F_GET_DATEFORMAT) + inv_payment_dueby , PKG_DATEUTIL.F_GET_DATEFORMAT ), ''W'' , TO_CHAR(to_date(TO_CHAR(inv_date, PKG_DATEUTIL.F_GET_DATEFORMAT), PKG_DATEUTIL.F_GET_DATEFORMAT) + inv_payment_dueby * 7 , PKG_DATEUTIL.F_GET_DATEFORMAT ), ''M'' , TO_CHAR(add_months(to_date(inv_date) , inv_payment_dueby),PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(add_months(to_date(inv_date) , inv_payment_dueby * 12),PKG_DATEUTIL.F_GET_DATEFORMAT) ) AS duedays,
  (SELECT SITE_NAME
  FROM ER_SITE,
    ER_USER
  WHERE PK_USER = INV_ADDRESSEDTO
  AND PK_SITE   = FK_SITEID
  ) AS orgto,
  (SELECT USR_FIRSTNAME
    || '' ''
    || USR_LASTNAME
  FROM ER_USER
  WHERE PK_USER = INV_ADDRESSEDTO
  ) AS addto,
  (SELECT address
  FROM ER_ADD,
    ER_USER
  WHERE pk_add = fk_peradd
  AND PK_USER  = INV_ADDRESSEDTO
  ) AS addto_add,
  (SELECT add_city
    || '' ''
    || add_state
  FROM ER_ADD,
    ER_USER
  WHERE pk_add = fk_peradd
  AND PK_USER  = INV_ADDRESSEDTO
  ) AS addto_add1,
  (SELECT add_country
    || '' ''
    || add_zipcode
  FROM ER_ADD,
    ER_USER
  WHERE pk_add = fk_peradd
  AND PK_USER  = INV_ADDRESSEDTO
  ) AS addto_add2,
  (SELECT SITE_NAME
  FROM ER_SITE,
    ER_USER
  WHERE PK_USER = inv_sentfrom
  AND PK_SITE   = FK_SITEID
  ) AS orgfrom,
  (SELECT USR_FIRSTNAME
    || '' ''
    || USR_LASTNAME
  FROM ER_USER
  WHERE PK_USER = inv_sentfrom
  ) AS sentfrom,
  (SELECT address
  FROM ER_ADD,
    ER_USER
  WHERE pk_add = fk_peradd
  AND PK_USER  = inv_sentfrom
  ) AS sentfrom_add,
  (SELECT add_city
    || '' ''
    || add_state
  FROM ER_ADD,
    ER_USER
  WHERE pk_add = fk_peradd
  AND PK_USER  = inv_sentfrom
  ) AS sentfrom_add1,
  (SELECT add_country
    || '' ''
    || add_zipcode
  FROM ER_ADD,
    ER_USER
  WHERE pk_add = fk_peradd
  AND PK_USER  = inv_sentfrom
  ) AS sentfrom_add2,
  (SELECT patprot_patstdid
  FROM ER_PATPROT
  WHERE ER_PATPROT.fk_study = a.fk_study
  AND ER_PATPROT.fk_per     = b.fk_per
  AND patprot_stat          = 1
  ) fk_per,
  milestones_achieved,
  TO_CHAR(achieved_on,PKG_DATEUTIL.F_GET_DATEFORMAT) achieved_on,
  amount_due,
  ROUND(amount_invoiced,2) amount_invoiced,
  ROUND(amount_holdback,2) amount_holdback,
  detail_type,
  display_detail,
  inv_notes,
  milestone_type AS TYPE,
  (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = milestone_status
  ) AS status,
  (SELECT name FROM esch.event_assoc WHERE event_id = fk_cal
  ) AS cal,
  (SELECT name FROM esch.event_assoc WHERE event_id = fk_eventassoc
  ) AS event,
  (SELECT visit_name
  FROM esch.sch_protocol_visit
  WHERE pk_protocol_visit = fk_visit
  ) AS visit,
  NVL(trim(
  (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = milestone_status
  )
  || '' ''
  ||
  (SELECT MILESTONE_DESCRIPTION
  FROM ER_MILESTONE
  WHERE b.FK_MILESTONE = PK_MILESTONE
  )
  || '' ''
  ||
  (SELECT visit_name
  FROM esch.sch_protocol_visit
  WHERE pk_protocol_visit = fk_visit
  )),''All'')
  || ''; '' AS milestone_type ,
  DECODE(detail_type,''H'',ROUND(amount_invoiced,2),0.00) AMOUNT_INVOICED_FOR_GT,
  DECODE(DISPLAY_DETAIL,0,ROUND(amount_holdback,2),0.00) AMOUNT_HOLDBACK_FOR_GT
FROM ER_INVOICE a,
  ER_INVOICE_DETAIL b,
  er_milestone c
WHERE fk_inv     = pk_invoice
AND fk_milestone = pk_milestone
AND pk_invoice   = ~1' where pk_report = 132;
 commit;

 Update ER_REPORT set REP_SQL='SELECT inv_number,
  (SELECT study_number FROM er_study WHERE pk_study = a.fk_study
  ) AS studynum,
  (SELECT nct_number FROM er_study WHERE pk_study = a.fk_study
  ) AS nctnum,
  (SELECT study_title FROM er_study WHERE pk_study = a.fk_study
  ) AS studytitle,
  TO_CHAR(inv_date,PKG_DATEUTIL.F_GET_DATEFORMAT) inv_date,
  DECODE(trim(inv_payunit), ''D'' , TO_CHAR(to_date(TO_CHAR(inv_date, PKG_DATEUTIL.F_GET_DATEFORMAT), PKG_DATEUTIL.F_GET_DATEFORMAT) + inv_payment_dueby , PKG_DATEUTIL.F_GET_DATEFORMAT ), ''W'' , TO_CHAR(to_date(TO_CHAR(inv_date, PKG_DATEUTIL.F_GET_DATEFORMAT), PKG_DATEUTIL.F_GET_DATEFORMAT) + inv_payment_dueby * 7 , PKG_DATEUTIL.F_GET_DATEFORMAT ), ''M'' , TO_CHAR(add_months(to_date(inv_date) , inv_payment_dueby),PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(add_months(to_date(inv_date) , inv_payment_dueby * 12),PKG_DATEUTIL.F_GET_DATEFORMAT) ) AS duedays,
  (SELECT SITE_NAME
  FROM ER_SITE,
    ER_USER
  WHERE PK_USER = INV_ADDRESSEDTO
  AND PK_SITE   = FK_SITEID
  ) AS ORG_TO,
  (SELECT USR_FIRSTNAME
    || '' ''
    || USR_LASTNAME
  FROM ER_USER
  WHERE PK_USER = INV_ADDRESSEDTO
  ) AS INVOICE_TO,
  (SELECT address
  FROM ER_ADD,
    ER_USER
  WHERE pk_add = fk_peradd
  AND PK_USER  = INV_ADDRESSEDTO
  ) AS addto_add,
  (SELECT DECODE(add_city , NULL, '''' , add_city
    ||'', '')
    || DECODE (add_state,NULL, '''' , add_state
    || '', '' )
    || add_zipcode
  FROM ER_ADD,
    ER_USER
  WHERE pk_add = fk_peradd
  AND PK_USER  = INV_ADDRESSEDTO
  ) AS addto_add1,
  (SELECT add_country
  FROM ER_ADD,
    ER_USER
  WHERE pk_add = fk_peradd
  AND PK_USER  = INV_ADDRESSEDTO
  ) AS addto_add2,
  (SELECT SITE_NAME
  FROM ER_SITE,
    ER_USER
  WHERE PK_USER = inv_sentfrom
  AND PK_SITE   = FK_SITEID
  ) AS ORG_FROM,
  (SELECT USR_FIRSTNAME
    ||'' ''
    || USR_LASTNAME
  FROM ER_USER
  WHERE PK_USER = inv_sentfrom
  ) AS PAYMENT_TO,
  (SELECT address
  FROM ER_ADD,
    ER_USER
  WHERE pk_add = fk_peradd
  AND PK_USER  = inv_sentfrom
  ) AS sentfrom_add,
  (SELECT DECODE(add_city , NULL, '''' , add_city
    ||'', '')
    || DECODE (add_state,NULL, '''' , add_state
    || '', '' )
    || add_zipcode
  FROM ER_ADD,
    ER_USER
  WHERE pk_add = fk_peradd
  AND PK_USER  = inv_sentfrom
  ) AS sentfrom_add1,
  (SELECT add_country
  FROM ER_ADD,
    ER_USER
  WHERE pk_add = fk_peradd
  AND PK_USER  = inv_sentfrom
  ) AS sentfrom_add2,
  (SELECT patprot_patstdid
  FROM ER_PATPROT
  WHERE ER_PATPROT.fk_study = a.fk_study
  AND ER_PATPROT.fk_per     = b.fk_per
  AND patprot_stat          = 1
  ) fk_per,
  DECODE(milestones_achieved,NULL,1,0,1,milestones_achieved) milestones_achieved,
  DECODE(DETAIL_TYPE,''D'',TO_CHAR(achieved_on,PKG_DATEUTIL.F_GET_DATEFORMAT),''-'') achieved_on,
  ROUND(amount_invoiced,2) amount_invoiced,
  ROUND(amount_holdback,2) amount_holdback,
  detail_type,
  display_detail,
  inv_notes,
  MILESTONE_TYPE AS TYPE,
  NVL(DECODE(MILESTONE_TYPE,''AM'',
  (SELECT MILESTONE_DESCRIPTION
  FROM ER_MILESTONE
  WHERE b.FK_MILESTONE=PK_MILESTONE
  ), ''VM'',
  (SELECT visit_name
  FROM esch.sch_protocol_visit
  WHERE pk_protocol_visit = fk_visit
  ), ''EM'',
  (SELECT visit_name
  FROM esch.sch_protocol_visit
  WHERE pk_protocol_visit = FK_VISIT
  )
  || ''; '',
  (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = milestone_status
  )),''All'') AS milestone_type ,
  DECODE(MILESTONE_TYPE,''EM'',
  (SELECT name FROM esch.EVENT_ASSOC WHERE EVENT_ID = FK_EVENTASSOC
  ),'';'') AS event_name,
  DECODE(detail_type,''H'',ROUND(amount_invoiced,2),0.00) AMOUNT_INVOICED_FOR_GT,
  DECODE(DISPLAY_DETAIL,0,ROUND(amount_holdback,2),0.00) AMOUNT_HOLDBACK_FOR_GT,
  (SELECT u.USR_LASTNAME
    || '',''
    || u.USR_FIRSTNAME
  FROM er_study,
    er_user u
  WHERE pk_study          = a.fk_study
  AND er_study.STUDY_PRINV=u.pk_user
  ) AS PI
FROM ER_INVOICE a,
  ER_INVOICE_DETAIL b,
  er_milestone c
WHERE fk_inv     = pk_invoice
AND fk_milestone = pk_milestone
AND pk_invoice   = ~1' where pk_report = 180;
  commit;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,361,1,'01_er_report_update.sql',sysdate,'v10 #762');

commit;