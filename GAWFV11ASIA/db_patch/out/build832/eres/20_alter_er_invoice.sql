DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) INTO v_column_exists
    from user_tab_cols
    where column_name = 'PREPFLAG'
      and table_name = 'ER_SPECIMEN';
     
  if (v_column_exists = 0) then
      execute immediate 'alter table ER_SPECIMEN add (PREPFLAG varchar2(20))';
  end if;
end;
/
COMMENT ON COLUMN "ERES"."ER_SPECIMEN"."PREPFLAG" IS 'This column stores the value Y if the sample is prepared from schedule page';
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,431,20,'20_alter_er_invoice.sql',sysdate,'v11 #832');
commit;
/