DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) INTO v_column_exists
    from user_tab_cols
    where column_name = 'PREPAREKITID'
      and table_name = 'ER_SPECIMEN';
      
  if (v_column_exists = 0) then
      execute immediate 'alter table ER_SPECIMEN add (PREPAREKITID Number(20))';
  end if;
end;
/
COMMENT ON COLUMN "ERES"."ER_SPECIMEN"."PREPAREKITID" IS 'This column stores the value of storage kit component(pk of er_storage) to which sample is prepared from schedule page';
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,431,21,'21_alter_er_invoice.sql',sysdate,'v11 #832');
commit;
/
