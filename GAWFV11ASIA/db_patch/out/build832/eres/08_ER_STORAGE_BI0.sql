set define off;
create or replace TRIGGER "ERES"."ER_STORAGE_BI0"
   BEFORE INSERT
   ON ERES.ER_STORAGE
   FOR EACH ROW
DECLARE
   raid          NUMBER (10);
   erid          NUMBER (10);
   usr           VARCHAR (2000);
   insert_data   CLOB;
BEGIN
   --Created by Manimaran for Audit Insert
   BEGIN
      --KM-#3635
      SELECT      TO_CHAR (pk_user)
               || ', '
               || usr_lastname
               || ', '
               || usr_firstname
        INTO   usr
        FROM   ER_USER
       WHERE   pk_user = :NEW.creator;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         USR := 'New User';
   END;

   SELECT   TRUNC (seq_rid.NEXTVAL) INTO erid FROM DUAL;

   :NEW.rid := erid;

   SELECT   seq_audit.NEXTVAL INTO raid FROM DUAL;

   audit_trail.record_transaction (raid,
                                   'ER_STORAGE',
                                   erid,
                                   'I',
                                   usr);

      Audit_Trail.column_insert (raid, 'PK_STORAGE', :NEW.PK_STORAGE);
      Audit_Trail.column_insert (raid, 'STORAGE_ID', :NEW.STORAGE_ID);
      Audit_Trail.column_insert (raid, 'STORAGE_NAME', :NEW.STORAGE_NAME);
      Audit_Trail.column_insert (raid, 'FK_CODELST_STORAGE_TYPE', :NEW.FK_CODELST_STORAGE_TYPE);
      Audit_Trail.column_insert (raid, 'FK_STORAGE', :NEW.FK_STORAGE);
      Audit_Trail.column_insert (raid, 'STORAGE_CAP_NUMBER', :NEW.STORAGE_CAP_NUMBER);
      Audit_Trail.column_insert (raid, 'FK_CODELST_CAPACITY_UNITS', :NEW.FK_CODELST_CAPACITY_UNITS);
      Audit_Trail.column_insert (raid, 'STORAGE_DIM1_CELLNUM', :NEW.STORAGE_DIM1_CELLNUM);
      Audit_Trail.column_insert (raid, 'STORAGE_DIM1_NAMING', :NEW.STORAGE_DIM1_NAMING);
      Audit_Trail.column_insert (raid, 'STORAGE_DIM1_ORDER', :NEW.STORAGE_DIM1_ORDER);
      Audit_Trail.column_insert (raid, 'STORAGE_DIM2_CELLNUM', :NEW.STORAGE_DIM2_CELLNUM);
      Audit_Trail.column_insert (raid, 'STORAGE_DIM2_NAMING', :NEW.STORAGE_DIM2_NAMING);
      Audit_Trail.column_insert (raid, 'STORAGE_DIM2_ORDER', :NEW.STORAGE_DIM2_ORDER);
      --Audit_Trail.column_insert (raid, 'STORAGE_NOTES', :NEW.STORAGE_NOTES);
      Audit_Trail.column_insert (raid, 'STORAGE_AVAIL_UNIT_COUNT', :NEW.STORAGE_AVAIL_UNIT_COUNT);
      Audit_Trail.column_insert (raid, 'STORAGE_COORDINATE_X', :NEW.STORAGE_COORDINATE_X);
      Audit_Trail.column_insert (raid, 'STORAGE_COORDINATE_Y', :NEW.STORAGE_COORDINATE_Y);
      Audit_Trail.column_insert (raid, 'STORAGE_LABEL', :NEW.STORAGE_LABEL);
      Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
      Audit_Trail.column_insert (raid, 'CREATOR', :NEW.CREATOR);
      Audit_Trail.column_insert (raid, 'CREATED_ON', TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
      Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', :NEW.LAST_MODIFIED_BY);
      Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
      Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
      Audit_Trail.column_insert (raid, 'FK_CODELST_CHILD_STYPE', :NEW.FK_CODELST_CHILD_STYPE);
      Audit_Trail.column_insert (raid, 'FK_ACCOUNT', :NEW.FK_ACCOUNT);
      Audit_Trail.column_insert (raid, 'STORAGE_ISTEMPLATE', :NEW.STORAGE_ISTEMPLATE);
      Audit_Trail.column_insert (raid, 'STORAGE_TEMPLATE_TYPE', :NEW.STORAGE_TEMPLATE_TYPE);
      Audit_Trail.column_insert (raid, 'STORAGE_ALTERNALEID', :NEW.STORAGE_ALTERNALEID);
      Audit_Trail.column_insert (raid, 'STORAGE_UNIT_CLASS', :NEW.STORAGE_UNIT_CLASS);
      Audit_Trail.column_insert (raid, 'STORAGE_KIT_CATEGORY', :NEW.STORAGE_KIT_CATEGORY);
      Audit_Trail.column_insert (raid, 'FK_STORAGE_KIT', :NEW.FK_STORAGE_KIT);
      Audit_Trail.column_insert (raid, 'STORAGE_MULTI_SPECIMEN', :NEW.STORAGE_MULTI_SPECIMEN);

   --KM-Template field added.
   insert_data :=
         :NEW.PK_STORAGE
      || '|'
      || :NEW.STORAGE_ID
      || '|'
      || :NEW.STORAGE_NAME
      || '|'
      || :NEW.FK_CODELST_STORAGE_TYPE
      || '|'
      || :NEW.FK_STORAGE
      || '|'
      || :NEW.STORAGE_CAP_NUMBER
      || '|'
      || :NEW.FK_CODELST_CAPACITY_UNITS
      || '|'
      ||                                      --:NEW.STORAGE_STATUS||'|'||--KM
        :NEW.STORAGE_DIM1_CELLNUM
      || '|'
      || :NEW.STORAGE_DIM1_NAMING
      || '|'
      || :NEW.STORAGE_DIM1_ORDER
      || '|'
      || :NEW.STORAGE_DIM2_CELLNUM
      || '|'
      || :NEW.STORAGE_DIM2_NAMING
      || '|'
      || :NEW.STORAGE_DIM1_ORDER
      || '|'
      || :NEW.STORAGE_AVAIL_UNIT_COUNT
      || '|'
      || :NEW.STORAGE_COORDINATE_X
      || '|'
      || :NEW.STORAGE_COORDINATE_Y
      || '|'
      || :NEW.STORAGE_LABEL
      || '|'
      || :NEW.RID
      || '|'
      || :NEW.CREATOR
      || '|'
      || :NEW.LAST_MODIFIED_BY
      || '|'
      || TO_CHAR (:NEW.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT)
      || '|'
      || TO_CHAR (:NEW.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT)
      || '|'
      || :NEW.IP_ADD
      || '|'
      || :NEW.FK_ACCOUNT
      || '|'
      || :NEW.STORAGE_ISTEMPLATE
      || '|'
      || :NEW.STORAGE_TEMPLATE_TYPE
      || '|'
      || :NEW.STORAGE_ALTERNALEID
      || '|'
      || :NEW.STORAGE_UNIT_CLASS
      || '|'
      || :NEW.STORAGE_KIT_CATEGORY
      || '|'
      || :NEW.FK_STORAGE_KIT
      || '|'
      || :NEW.STORAGE_MULTI_SPECIMEN;                            --YK Bug#5827

 --  INSERT INTO AUDIT_INSERT (raid, row_data)
 --    VALUES   (raid, insert_data);
END;



/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,431,8,'08_ER_STORAGE_BI0.sql',sysdate,'v11 #832');

commit;
/