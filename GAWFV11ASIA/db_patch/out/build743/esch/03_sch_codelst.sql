DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from SCH_CODELST where CODELST_TYPE='category' and CODELST_SUBTYP = 'ctgry_sfaculty';
  if (v_record_exists = 0) then
	INSERT INTO SCH_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SCH_CODELST_SEQ1.nextval)+1 , NULL, 'category', 'ctgry_sfaculty', 'Salary-Faculty', 'N', 
		(select max(CODELST_SEQ)+1 from SCH_CODELST where CODELST_TYPE='category'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, NULL, NULL 
	    );
	 
	  commit;
  end if;
  
  Select count(*) into v_record_exists
    from SCH_CODELST where CODELST_TYPE='category' and CODELST_SUBTYP = 'ctgry_sclass';
  if (v_record_exists = 0) then
	INSERT INTO SCH_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SCH_CODELST_SEQ1.nextval)+1 , NULL, 'category', 'ctgry_sclass', 'Salary-Classified', 'N', 
		(select max(CODELST_SEQ)+1 from SCH_CODELST where CODELST_TYPE='category'), NULL, NULL, NULL, 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), NULL, NULL, NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,342,3,'03_sch_codelst.sql',sysdate,'v9.3.0 #743');

commit;