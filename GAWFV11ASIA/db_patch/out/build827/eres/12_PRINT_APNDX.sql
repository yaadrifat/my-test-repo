create or replace FUNCTION PRINT_APNDX_DOCS ( p_docName IN VARCHAR2,p_apndxId IN NUMBER,p_statusPk IN NUMBER,p_studyNo IN VARCHAR2,p_statusDesc IN VARCHAR2 ) RETURN VARCHAR2 AUTHID CURRENT_USER AS
     
     lc_str VARCHAR2(4000);
     lc_colval VARCHAR2(4000);
     v_dnld_html_2 VARCHAR2(4000);
     v_dnld_html_3 VARCHAR2(4000);
     v_dnld_html_4 VARCHAR2(4000);
    
     BEGIN
     v_dnld_html_2:='<span style="overflow: hidden; word-wrap: break-word; display: block;">';
     v_dnld_html_4:='</A></span>';
     v_dnld_html_3:='<A href="#" onClick="fdownload(document.apendixbrowser,'||p_apndxId||','''||p_docName||''','''||p_statusPk||''','''||p_studyNo||''','''||p_statusDesc||''');return false;" >';
     lc_str :=   v_dnld_html_2 || '<B>' || p_docName || '</B>' || v_dnld_html_3 || ' View ' || v_dnld_html_4;
    
     RETURN lc_str;
     END;
	 /
	 
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,426,12,'12_PRINT_APNDX.sql',sysdate,'v11 #827');
commit;