  set define off;
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_LKPCOL where LKPCOL_TABLE='REP_ORG_MORE_DETAILS' and LKPCOL_KEYWORD='FIELD_NAME' and FK_LKPLIB =(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Org Details' and LKPTYPE_TYPE='dyn_a');
  if (v_record_exists = 0) then

INSERT
INTO ER_LKPCOL
  (
    PK_LKPCOL,
    FK_LKPLIB,
    LKPCOL_NAME,
    LKPCOL_DISPVAL,
    LKPCOL_DATATYPE,
    LKPCOL_LEN,
    LKPCOL_TABLE,
    LKPCOL_KEYWORD
  )
  VALUES
  (
    SEQ_ER_LKPCOL.nextval,(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Org Details' and LKPTYPE_TYPE='dyn_a'),
    'FIELD_NAME',
    'Field Name',
    'varchar',
    NULL,
    'REP_ORG_MORE_DETAILS',
    'FIELD_NAME'
  );
  
  end if;
end;
/

  set define off;
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_LKPCOL where LKPCOL_TABLE='REP_ORG_MORE_DETAILS' and LKPCOL_KEYWORD='FIELD_VALUE' and FK_LKPLIB =(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Org Details' and LKPTYPE_TYPE='dyn_a');
  if (v_record_exists = 0) then
INSERT
INTO ER_LKPCOL
  (
    PK_LKPCOL,
    FK_LKPLIB,
    LKPCOL_NAME,
    LKPCOL_DISPVAL,
    LKPCOL_DATATYPE,
    LKPCOL_LEN,
    LKPCOL_TABLE,
    LKPCOL_KEYWORD
  )
  VALUES
  (
    SEQ_ER_LKPCOL.nextval,(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Org Details' and LKPTYPE_TYPE='dyn_a'),
    'FIELD_VALUE',
    'Field Value',
    'varchar',
    NULL,
    'REP_ORG_MORE_DETAILS',
    'FIELD_VALUE'
  );
  
  end if;
end;
/

  set define off;
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_LKPCOL where LKPCOL_TABLE='REP_ORG_MORE_DETAILS' and LKPCOL_KEYWORD='CREATED_ON' and FK_LKPLIB =(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Org Details' and LKPTYPE_TYPE='dyn_a');
  if (v_record_exists = 0) then
  
  
INSERT
INTO ER_LKPCOL
  (
    PK_LKPCOL,
    FK_LKPLIB,
    LKPCOL_NAME,
    LKPCOL_DISPVAL,
    LKPCOL_DATATYPE,
    LKPCOL_LEN,
    LKPCOL_TABLE,
    LKPCOL_KEYWORD
  )
  VALUES
  (
    SEQ_ER_LKPCOL.nextval,(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Org Details' and LKPTYPE_TYPE='dyn_a'),
    'CREATED_ON',
    'Created On',
    'date',
    NULL,
    'REP_ORG_MORE_DETAILS',
    'CREATED_ON'
  );
    end if;
end;
/

  set define off;
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_LKPCOL where LKPCOL_TABLE='REP_ORG_MORE_DETAILS' and LKPCOL_KEYWORD='CREATOR' and FK_LKPLIB =(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Org Details' and LKPTYPE_TYPE='dyn_a');
  if (v_record_exists = 0) then
INSERT
INTO ER_LKPCOL
  (
    PK_LKPCOL,
    FK_LKPLIB,
    LKPCOL_NAME,
    LKPCOL_DISPVAL,
    LKPCOL_DATATYPE,
    LKPCOL_LEN,
    LKPCOL_TABLE,
    LKPCOL_KEYWORD
  )
  VALUES
  (
    SEQ_ER_LKPCOL.nextval,(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Org Details' and LKPTYPE_TYPE='dyn_a'),
    'CREATOR',
    'Creator',
    'varchar',
    NULL,
    'REP_ORG_MORE_DETAILS',
    'CREATOR'
  );
    end if;
end;
/

  set define off;
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_LKPCOL where LKPCOL_TABLE='REP_ORG_MORE_DETAILS' and LKPCOL_KEYWORD='LAST_MODIFIED_BY' and FK_LKPLIB =(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Org Details' and LKPTYPE_TYPE='dyn_a');
  if (v_record_exists = 0) then
INSERT
INTO ER_LKPCOL
  (
    PK_LKPCOL,
    FK_LKPLIB,
    LKPCOL_NAME,
    LKPCOL_DISPVAL,
    LKPCOL_DATATYPE,
    LKPCOL_LEN,
    LKPCOL_TABLE,
    LKPCOL_KEYWORD
  )
  VALUES
  (
    SEQ_ER_LKPCOL.nextval,(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Org Details' and LKPTYPE_TYPE='dyn_a'),
    'LAST_MODIFIED_BY',
    'Last Modified By',
    'varchar',
    NULL,
    'REP_ORG_MORE_DETAILS',
    'LAST_MODIFIED_BY'
  );
    end if;
end;
/

  set define off;
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_LKPCOL where LKPCOL_TABLE='REP_ORG_MORE_DETAILS' and LKPCOL_KEYWORD='LAST_MODIFIED_DATE' and FK_LKPLIB =(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Org Details' and LKPTYPE_TYPE='dyn_a');
  if (v_record_exists = 0) then
INSERT
INTO ER_LKPCOL
  (
    PK_LKPCOL,
    FK_LKPLIB,
    LKPCOL_NAME,
    LKPCOL_DISPVAL,
    LKPCOL_DATATYPE,
    LKPCOL_LEN,
    LKPCOL_TABLE,
    LKPCOL_KEYWORD
  )
  VALUES
  (
    SEQ_ER_LKPCOL.nextval,(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Org Details' and LKPTYPE_TYPE='dyn_a'),
    'LAST_MODIFIED_DATE',
    'Last Modified Date',
    'date',
    NULL,
    'REP_ORG_MORE_DETAILS',
    'LAST_MODIFIED_DATE'
  );
    end if;
end;
/

  set define off;
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_LKPCOL where LKPCOL_TABLE='REP_ORG_MORE_DETAILS' and LKPCOL_KEYWORD='SITE_RESPONSE_ID' and FK_LKPLIB =(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Org Details' and LKPTYPE_TYPE='dyn_a');
  if (v_record_exists = 0) then
INSERT
INTO ER_LKPCOL
  (
    PK_LKPCOL,
    FK_LKPLIB,
    LKPCOL_NAME,
    LKPCOL_DISPVAL,
    LKPCOL_DATATYPE,
    LKPCOL_LEN,
    LKPCOL_TABLE,
    LKPCOL_KEYWORD
  )
  VALUES
  (
    SEQ_ER_LKPCOL.nextval,(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Org Details' and LKPTYPE_TYPE='dyn_a'),
    'RESPONSE_ID',
    'Response ID',
    'number',
    NULL,
    'REP_ORG_MORE_DETAILS',
    'SITE_RESPONSE_ID'
  );
    end if;
end;
/

  set define off;
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_LKPCOL where LKPCOL_TABLE='REP_ORG_MORE_DETAILS' and LKPCOL_KEYWORD='SITE_NAME' and FK_LKPLIB =(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Org Details' and LKPTYPE_TYPE='dyn_a');
  if (v_record_exists = 0) then
INSERT
INTO ER_LKPCOL
  (
    PK_LKPCOL,
    FK_LKPLIB,
    LKPCOL_NAME,
    LKPCOL_DISPVAL,
    LKPCOL_DATATYPE,
    LKPCOL_LEN,
    LKPCOL_TABLE,
    LKPCOL_KEYWORD
  )
  VALUES
  (
    SEQ_ER_LKPCOL.nextval,(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Org Details' and LKPTYPE_TYPE='dyn_a'),
    'SITE_NAME',
    'Site Name',
    'varchar',
    NULL,
    'REP_ORG_MORE_DETAILS',
    'SITE_NAME'
  );
    end if;
end;
/

  set define off;
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_LKPCOL where LKPCOL_TABLE='REP_ORG_MORE_DETAILS' and LKPCOL_KEYWORD='LKP_PK' and FK_LKPLIB =(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Org Details' and LKPTYPE_TYPE='dyn_a');
  if (v_record_exists = 0) then
INSERT
INTO ER_LKPCOL
  (
    PK_LKPCOL,
    FK_LKPLIB,
    LKPCOL_NAME,
    LKPCOL_DISPVAL,
    LKPCOL_DATATYPE,
    LKPCOL_LEN,
    LKPCOL_TABLE,
    LKPCOL_KEYWORD
  )
  VALUES
  (
    SEQ_ER_LKPCOL.nextval,(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Org Details' and LKPTYPE_TYPE='dyn_a'),
    'FK_ACCOUNT',
    'fk_account',
    'number',
    NULL,
    'REP_ORG_MORE_DETAILS',
    'LKP_PK'
  );
    end if;
end;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,436,3,'03_er_lkpcol.sql',sysdate,'v11 #837');
/
commit;	