set define off;
declare
	update_sql CLOB;
	table_check number;

begin
	SELECT count(*)
	INTO table_check FROM user_tables
	WHERE TABLE_NAME = 'ER_REPORT';
	
	if(table_check>0) then
		update_sql:='select
study_number,
milestone_type,
msach_milestone_desc,
fk_site_enrolling,
enrolling_site,
NVL(msach_ptstudy_id,''(Patient removed from study)'') msach_ptstudy_id,
TO_CHAR(msach_ach_date,PKG_DATEUTIL.F_GET_DATEFORMAT) msach_ach_date,
(milestone_amt/DECODE((select MSRUL_PT_COUNT from VDA.VDA_V_MILESTONES a where
VDA.vda_v_milestone_achvd_det.fk_study = a.fk_study AND a.pk_milestone = VDA.vda_v_milestone_achvd_det.fk_milestone),-1,1,null,1,
0,1,
(select MSRUL_PT_COUNT from VDA.VDA_V_MILESTONES a where
VDA.vda_v_milestone_achvd_det.fk_study = a.fk_study AND a.pk_milestone = VDA.vda_v_milestone_achvd_det.fk_milestone))) milestone_amt,
milestone_holdback,
payment_type,
payment_for,
fk_study,
fk_milestone,
inv_amt,
inv_num,
rec_amt,
payment_dt
from VDA.vda_v_milestone_achvd_det
where PAYMENT_SUBTYPE != ''pay''
AND fk_study IN (:studyId)
AND TRUNC(MSACH_ACH_DATE) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)';
	
		update er_report set rep_sql=update_sql,rep_sql_clob=update_sql where pk_report=257;
		
		update_sql:='select
study_number,
milestone_type,
msach_milestone_desc,
fk_site_enrolling,
enrolling_site,
NVL(msach_ptstudy_id,''(Patient removed from study)'') msach_ptstudy_id,
TO_CHAR(msach_ach_date,PKG_DATEUTIL.F_GET_DATEFORMAT) msach_ach_date,
(milestone_amt/DECODE((select MSRUL_PT_COUNT from VDA.VDA_V_MILESTONES a where
VDA.vda_v_milestone_achvd_det.fk_study = a.fk_study AND a.pk_milestone = VDA.vda_v_milestone_achvd_det.fk_milestone),-1,1,null,1,
0,1,
(select MSRUL_PT_COUNT from VDA.VDA_V_MILESTONES a where
VDA.vda_v_milestone_achvd_det.fk_study = a.fk_study AND a.pk_milestone = VDA.vda_v_milestone_achvd_det.fk_milestone))) milestone_amt,
milestone_holdback,
payment_type,
payment_for,
fk_study,
fk_milestone,
inv_amt,
inv_num,
rec_amt,
payment_dt
from VDA.vda_v_milestone_achvd_det
where PAYMENT_SUBTYPE = ''pay''
AND fk_study IN (:studyId)
AND TRUNC(MSACH_ACH_DATE) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)';
		
		update er_report set rep_sql=update_sql,rep_sql_clob=update_sql where pk_report=258;
		
		update_sql:='select distinct
b.study_number,
study_division,
study_pi,
study_coordinator,
nvl((select sum(milestone_amt/DECODE((select MSRUL_PT_COUNT from VDA.VDA_V_MILESTONES b where
c.fk_study = b.fk_study AND b.pk_milestone = c.fk_milestone),-1,1,null,1,0,1,
(select MSRUL_PT_COUNT from VDA.VDA_V_MILESTONES b where
c.fk_study = b.fk_study AND b.pk_milestone = c.fk_milestone))) from VDA.vda_v_milestone_achvd_det c where c.fk_study = a.fk_study and payment_subtype != ''pay''),''0'') as milestone_amt,
nvl((select sum(milestone_holdback) from VDA.vda_v_milestone_achvd_det b where b.fk_study = a.fk_study and payment_subtype != ''pay''),''0'') as holdback_amt,
fk_study,
nvl((select sum(amount_invoiced) from er_invoice_detail where er_invoice_detail.fk_study = a.fk_study and detail_type=''H''),''0'') as inv_amt,
(select sum(NVL(mp_amount,0))+sum(NVL(MP_HOLDBACK_AMOUNT,0)) from er_milepayment_details where fk_mileachieved in (select pk_mileachieved from VDA.vda_v_milestone_achvd_det b where
b.fk_study = a.fk_study and payment_subtype != ''pay'' )
and MP_LINKTO_TYPE=''P'') as rec_amt
from VDA.vda_v_milestone_achvd_det a, VDA.vda_v_study_summary b
where payment_subtype != ''pay''
and fk_study = pk_study
AND fk_study IN (:studyId)
AND TRUNC(MSACH_ACH_DATE) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)';
		
		update er_report set rep_sql=update_sql,rep_sql_clob=update_sql where pk_report=263;
	
	End if;
commit;
end;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,414,3,'03_ER_REPORT.sql',sysdate,'v11 #815');
commit; 