 DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where column_name = 'SA_DESC'
      and table_name = 'ER_SPECIMEN_APPNDX';

  if (v_column_exists > 0) then
      execute immediate 'alter table ER_SPECIMEN_APPNDX modify SA_DESC  varchar2(500 byte)';
  end if;
  commit;
end;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,414,1,'01_alter er_specimen_apndx.sql',sysdate,'v11 #815');
commit;