SET DEFINE OFF;
DECLARE
  v_seq NUMBER :=0;
  v_cnt NUMBER :=0;
  BEGIN
  SELECT COUNT(*) INTO v_cnt FROM ER_LKPVIEW WHERE LKPVIEW_KEYWORD='mstudyTeam';
  IF (v_cnt=0) THEN
	SELECT SEQ_ER_LKPLIB.nextval INTO v_seq FROM dual;
Insert into ER_LKPLIB (PK_LKPLIB,LKPTYPE_NAME,LKPTYPE_DESC,LKPTYPE_TYPE,FK_ACCOUNT,FK_SITE) values (v_seq,'dynReports','More Studyteam Details','dyn_s',null,null);

Insert into ER_LKPVIEW (PK_LKPVIEW,LKPVIEW_NAME,LKPVIEW_DESC,FK_LKPLIB,LKPVIEW_RETDATA,LKPVIEW_RETDISP,LKPVIEW_ISACTIVE,LKPVIEW_IGNOREFILTER,LKPVIEW_FILTER,LKPVIEW_KEYWORD) values (v_seq,'More Studyteam Details',null,v_seq,null,null,null,null,'fk_account=[:ACCID]','mstudyTeam');

Insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD) values ((select max(pk_lkpcol+1) from er_lkpcol),v_seq,'PK_MOREDETAILS','PK_MOREDETAILS','varchar2',200,'ERV_STUDYTEAM_MORE_DETAILS','PK_MOREDETAILS');
Insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD) values ((select max(pk_lkpcol+1) from er_lkpcol),v_seq,'PK_STUDYTEAM','PK_STUDYTEAM','varchar2',200,'ERV_STUDYTEAM_MORE_DETAILS','PK_STUDYTEAM');
Insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD) values ((select max(pk_lkpcol+1) from er_lkpcol),v_seq,'STUDYTEAM_STATUS','STUDYTEAM STATUS','varchar2',200,'ERV_STUDYTEAM_MORE_DETAILS','STUDYTEAM_STATUS');
Insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD) values ((select max(pk_lkpcol+1) from er_lkpcol),v_seq,'FIELD_NAME','Field Name','varchar2',200,'ERV_STUDYTEAM_MORE_DETAILS','FIELD_NAME');
Insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD) values ((select max(pk_lkpcol+1) from er_lkpcol),v_seq,'FIELD_VALUE','Field Value','varchar2',500,'ERV_STUDYTEAM_MORE_DETAILS','FIELD_VALUE');
Insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD) values ((select max(pk_lkpcol+1) from er_lkpcol),v_seq,'CREATED_ON','Created On','varchar2',500,'ERV_STUDYTEAM_MORE_DETAILS','CREATED_ON');
Insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD) values ((select max(pk_lkpcol+1) from er_lkpcol),v_seq,'CREATOR','Creator','varchar2',100,'ERV_STUDYTEAM_MORE_DETAILS','CREATOR');
Insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD) values ((select max(pk_lkpcol+1) from er_lkpcol),v_seq,'LAST_MODIFIED_BY','Last Modified By','varchar2',100,'ERV_STUDYTEAM_MORE_DETAILS','LAST_MODIFIED_BY');
Insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD) values ((select max(pk_lkpcol+1) from er_lkpcol),v_seq,'LAST_MODIFIED_DATE','Last Modified Date','varchar2',500,'ERV_STUDYTEAM_MORE_DETAILS','LAST_MODIFIED_DATE');
Insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD) values ((select max(pk_lkpcol+1) from er_lkpcol),v_seq,'RID','RID','varchar2',500,'ERV_STUDYTEAM_MORE_DETAILS','RID');
Insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD) values ((select max(pk_lkpcol+1) from er_lkpcol),v_seq,'RESPONSE_ID','RESPONSE ID','varchar2',500,'ERV_STUDYTEAM_MORE_DETAILS','RESPONSE_ID');


Insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,FK_LKPCOL,LKPVIEW_IS_SEARCH,LKPVIEW_SEQ,FK_LKPVIEW,LKPVIEW_DISPLEN,LKPVIEW_IS_DISPLAY) values ((select max(pk_lkpviewcol+1) from er_lkpviewcol),(select pk_lkpcol from er_lkpcol where LKPCOL_KEYWORD='PK_MOREDETAILS' and fk_lkplib=v_seq),'Y',10,v_seq,'10%','Y');
Insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,FK_LKPCOL,LKPVIEW_IS_SEARCH,LKPVIEW_SEQ,FK_LKPVIEW,LKPVIEW_DISPLEN,LKPVIEW_IS_DISPLAY) values ((select max(pk_lkpviewcol+1) from er_lkpviewcol),(select pk_lkpcol from er_lkpcol where LKPCOL_KEYWORD='PK_STUDYTEAM' and fk_lkplib=v_seq),'Y',10,v_seq,'10%','Y');
Insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,FK_LKPCOL,LKPVIEW_IS_SEARCH,LKPVIEW_SEQ,FK_LKPVIEW,LKPVIEW_DISPLEN,LKPVIEW_IS_DISPLAY) values ((select max(pk_lkpviewcol+1) from er_lkpviewcol),(select pk_lkpcol from er_lkpcol where LKPCOL_KEYWORD='STUDYTEAM_STATUS' and fk_lkplib=v_seq),'Y',10,v_seq,'10%','Y');
Insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,FK_LKPCOL,LKPVIEW_IS_SEARCH,LKPVIEW_SEQ,FK_LKPVIEW,LKPVIEW_DISPLEN,LKPVIEW_IS_DISPLAY) values ((select max(pk_lkpviewcol+1) from er_lkpviewcol),(select pk_lkpcol from er_lkpcol where LKPCOL_KEYWORD='FIELD_NAME' and fk_lkplib=v_seq),'Y',10,v_seq,'10%','Y');
Insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,FK_LKPCOL,LKPVIEW_IS_SEARCH,LKPVIEW_SEQ,FK_LKPVIEW,LKPVIEW_DISPLEN,LKPVIEW_IS_DISPLAY) values ((select max(pk_lkpviewcol+1) from er_lkpviewcol),(select pk_lkpcol from er_lkpcol where LKPCOL_KEYWORD='FIELD_VALUE' and fk_lkplib=v_seq),'Y',9,v_seq,'10%','Y');
Insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,FK_LKPCOL,LKPVIEW_IS_SEARCH,LKPVIEW_SEQ,FK_LKPVIEW,LKPVIEW_DISPLEN,LKPVIEW_IS_DISPLAY) values ((select max(pk_lkpviewcol+1) from er_lkpviewcol),(select pk_lkpcol from er_lkpcol where LKPCOL_KEYWORD='CREATED_ON' and fk_lkplib=v_seq),'Y',10,v_seq,'10%','Y');
Insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,FK_LKPCOL,LKPVIEW_IS_SEARCH,LKPVIEW_SEQ,FK_LKPVIEW,LKPVIEW_DISPLEN,LKPVIEW_IS_DISPLAY) values ((select max(pk_lkpviewcol+1) from er_lkpviewcol),(select pk_lkpcol from er_lkpcol where LKPCOL_KEYWORD='CREATOR' and fk_lkplib=v_seq),'Y',10,v_seq,'10%','Y');
Insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,FK_LKPCOL,LKPVIEW_IS_SEARCH,LKPVIEW_SEQ,FK_LKPVIEW,LKPVIEW_DISPLEN,LKPVIEW_IS_DISPLAY) values ((select max(pk_lkpviewcol+1) from er_lkpviewcol),(select pk_lkpcol from er_lkpcol where LKPCOL_KEYWORD='LAST_MODIFIED_BY' and fk_lkplib=v_seq),'Y',10,v_seq,'10%','Y');
Insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,FK_LKPCOL,LKPVIEW_IS_SEARCH,LKPVIEW_SEQ,FK_LKPVIEW,LKPVIEW_DISPLEN,LKPVIEW_IS_DISPLAY) values ((select max(pk_lkpviewcol+1) from er_lkpviewcol),(select pk_lkpcol from er_lkpcol where LKPCOL_KEYWORD='LAST_MODIFIED_DATE' and fk_lkplib=v_seq),'Y',10,v_seq,'10%','Y');
Insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,FK_LKPCOL,LKPVIEW_IS_SEARCH,LKPVIEW_SEQ,FK_LKPVIEW,LKPVIEW_DISPLEN,LKPVIEW_IS_DISPLAY) values ((select max(pk_lkpviewcol+1) from er_lkpviewcol),(select pk_lkpcol from er_lkpcol where LKPCOL_KEYWORD='RID' and fk_lkplib=v_seq),'Y',10,v_seq,'10%','Y');
Insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,FK_LKPCOL,LKPVIEW_IS_SEARCH,LKPVIEW_SEQ,FK_LKPVIEW,LKPVIEW_DISPLEN,LKPVIEW_IS_DISPLAY) values ((select max(pk_lkpviewcol+1) from er_lkpviewcol),(select pk_lkpcol from er_lkpcol where LKPCOL_KEYWORD='RESPONSE_ID' and fk_lkplib=v_seq),'Y',10,v_seq,'10%','Y');

END IF;
END;
/
commit;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,407,2,'02_Er_LKPSAVE.sql',sysdate,'v11 #808');
commit; 