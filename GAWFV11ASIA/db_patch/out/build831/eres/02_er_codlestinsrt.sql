set define off;
	
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='theme' and CODELST_SUBTYP = 'th_stdPatRost';
  if (v_record_exists = 0) then
	INSERT INTO ER_CODELST 
		(PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_MAINT, CREATOR,
		LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1, CODELST_STUDY_ROLE, CODELST_KIND)
	VALUES(
		(SEQ_ER_CODELST.nextval) , NULL, 'theme', 'th_stdPatRost', 'Study Patient Roster', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='theme'), NULL, NULL, NULL, 
		sysdate, sysdate, NULL, 'studyCoordinatorPage.jsp', NULL, NULL, NULL 
	    );
	 
	  commit;
  end if;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,430,2,'02_er_codlestinsrt.sql',sysdate,'v11 #831');
commit;
/