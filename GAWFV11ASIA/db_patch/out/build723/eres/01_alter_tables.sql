set define off;
set serveroutput on;

DECLARE
  v_item_exists number := 0;  
BEGIN
select count(*) into v_item_exists from user_tab_cols where TABLE_NAME = 'UI_FLX_PAGEVER'
and COLUMN_NAME = 'PAGE_MINOR_VER';
if (v_item_exists = 0) then
  execute immediate 'ALTER TABLE ERES.UI_FLX_PAGEVER ADD (PAGE_MINOR_VER NUMBER)';
  dbms_output.put_line('Col PAGE_MINOR_VER added');
else
  dbms_output.put_line('Col PAGE_MINOR_VER already exists');
end if;

END;
/

COMMENT ON COLUMN 
ERES.UI_FLX_PAGEVER.PAGE_MINOR_VER IS 
'This column stores the minor version associated with the page';

DECLARE
  v_item_exists number := 0;  
BEGIN
select count(*) into v_item_exists from user_tab_cols where TABLE_NAME = 'UI_FLX_PAGEVER'
and COLUMN_NAME = 'FK_SUBMISSION';
if (v_item_exists = 0) then
  execute immediate 'ALTER TABLE ERES.UI_FLX_PAGEVER ADD (FK_SUBMISSION NUMBER)';
  dbms_output.put_line('Col FK_SUBMISSION added');
else
  dbms_output.put_line('Col FK_SUBMISSION already exists');
end if;

END;
/

COMMENT ON COLUMN 
ERES.UI_FLX_PAGEVER.FK_SUBMISSION IS 
'This column stores the foreign key to the ER_SUBMISSION table';

DECLARE
  v_item_exists number := 0;  
BEGIN
select count(*) into v_item_exists from user_tab_cols where TABLE_NAME = 'ER_SUBMISSION_STATUS'
and COLUMN_NAME = 'FK_STUDYSTAT';
if (v_item_exists = 0) then
  execute immediate 'ALTER TABLE ERES.ER_SUBMISSION_STATUS ADD (FK_STUDYSTAT NUMBER)';
  dbms_output.put_line('Col FK_STUDYSTAT added');
else
  dbms_output.put_line('Col FK_STUDYSTAT already exists');
end if;

END;
/

COMMENT ON COLUMN 
ERES.ER_SUBMISSION_STATUS.FK_STUDYSTAT IS 
'This column stores the foreign key to the ER_STUDYSTAT table if there is a related study status';

DECLARE
  v_item_exists number := 0;  
BEGIN
select count(*) into v_item_exists from user_tab_cols where TABLE_NAME = 'ER_STUDY'
and COLUMN_NAME = 'STUDY_IS_LOCKED';
if (v_item_exists = 0) then
  execute immediate 'ALTER TABLE ERES.ER_STUDY ADD (STUDY_IS_LOCKED  NUMBER DEFAULT 0)';
  dbms_output.put_line('Col STUDY_IS_LOCKED added');
else
  dbms_output.put_line('Col STUDY_IS_LOCKED already exists');
end if;

END;
/

COMMENT ON COLUMN 
ERES.ER_STUDY.STUDY_IS_LOCKED IS 
'This column stores the flag to lock/unlock protocol when a new version is created. 0=unlocked, 1=locked';

DECLARE
  v_item_exists number := 0;  
BEGIN
select count(*) into v_item_exists from user_tab_cols where TABLE_NAME = 'ER_STUDY'
and COLUMN_NAME = 'STUDY_IS_BLOCKED';
if (v_item_exists = 0) then
  execute immediate 'ALTER TABLE ERES.ER_STUDY ADD (STUDY_IS_BLOCKED  NUMBER DEFAULT 0)';
  dbms_output.put_line('Col STUDY_IS_BLOCKED added');
else
  dbms_output.put_line('Col STUDY_IS_BLOCKED already exists');
end if;

END;
/

COMMENT ON COLUMN 
ERES.ER_STUDY.STUDY_IS_BLOCKED IS 
'This column stores the flag to block/unblock submission. 0=unblocked, 1=blocked';

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,322,1,'01_alter_tables.sql',sysdate,'v9.3.0 #723');

commit;
