DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) INTO v_column_exists
    from user_tab_cols
    where column_name = 'INV_PAYMENT_DUEDATE'
      and table_name = 'ER_INVOICE';
      --and owner = 'SCOTT --*might be required if you are using all/dba views

  if (v_column_exists = 0) then
      execute immediate 'alter table er_invoice add (INV_PAYMENT_DUEDATE date)';
  end if;
end;
/
COMMENT ON COLUMN "ERES"."ER_INVOICE"."INV_PAYMENT_DUEDATE" IS 'This column stores the invoice PaymentDueDate';
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,416,6,'06_alter_er_invoice.sql',sysdate,'v11 #817');
commit;
/
