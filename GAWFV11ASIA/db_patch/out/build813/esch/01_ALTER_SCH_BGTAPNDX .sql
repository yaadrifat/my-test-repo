DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where column_name = 'BGTAPNDX_DESC'
      and table_name = 'SCH_BGTAPNDX';

  if (v_column_exists > 0) then
      execute immediate 'alter table SCH_BGTAPNDX modify BGTAPNDX_DESC  varchar2(500 byte)';
  end if;
  commit;
end;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,412,1,'01_ALTER_SCH_BGTAPNDX .sql',sysdate,'v11 #813');

commit; 
