SET DEFINE OFF;

DECLARE
  v_record_exists number := 0;  
BEGIN

  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='org_0';
if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_0','baysianCmpste','Bayesian component included in Site1','N',6,'dropdown','<SELECT id="alternateId" NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "Yes">Yes</OPTION><OPTION value = "No">No</OPTION></SELECT>');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_0','contactno','Contact No1','N',3,null,null);

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_0','drugMnftrsite','Manufacturer Site1','N',2,'chkbox','{chkArray:[{data: ''option1'', display:''MDACC/Regional Centers''}, {data: ''option2'', display:''Non-MDACC Hospital/Clinic Healthcare Facility/Harris County Hospital District Facility''}, {data: ''option3'', display:''Community Center/Shopping Mall (e.g. religious facility)''}, {data: ''option4'', display:''Educational Campus (e.g. elementary or secondary school or a university campus''}, {data: ''option5'', display:"Subject''s Home (list information on city; county; state; or country where the home is located below)"}, {data: ''option6'', display:''Internet Research (list website names below)''}, {data: ''option7'', display:''International Site''}, {data: ''option8'', display:''Other''} ]}');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_0','exclsnorg','Exclusion Organization1','N',4,'lookup','{lookupPK:2, selection:"single", mapping:[{source:"nci3_shrtname", target:"alternateId"}]}');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_0','orgsitee','Orgaization Test1','N',5,'chkbox',null);

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_0','sitename','Site Alternative Name1','N',1,'textarea',null);
end if;
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='org_1';
if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_1','baysianCmpste','Bayesian component included in Site2','N',6,'dropdown','<SELECT id="alternateId" NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "Yes">Yes</OPTION><OPTION value = "No">No</OPTION></SELECT>');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_1','contactno','Contact No2','N',3,null,null);

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_1','drugMnftrsite','Manufacturer Site2','N',2,'chkbox','{chkArray:[{data: ''option1'', display:''MDACC/Regional Centers''}, {data: ''option2'', display:''Non-MDACC Hospital/Clinic Healthcare Facility/Harris County Hospital District Facility''}, {data: ''option3'', display:''Community Center/Shopping Mall (e.g. religious facility)''}, {data: ''option4'', display:''Educational Campus (e.g. elementary or secondary school or a university campus''}, {data: ''option5'', display:"Subject''s Home (list information on city; county; state; or country where the home is located below)"}, {data: ''option6'', display:''Internet Research (list website names below)''}, {data: ''option7'', display:''International Site''}, {data: ''option8'', display:''Other''} ]}');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_1','exclsnorg','Exclusion Organization2','N',4,'lookup','{lookupPK:2, selection:"single", mapping:[{source:"nci3_shrtname", target:"alternateId"}]}');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_1','orgsitee','Orgaization Test2','N',5,'chkbox',null);

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_1','sitename','Site Alternative Name2','N',1,'textarea',null);
end if;

  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='org_2';
if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_2','baysianCmpste','Bayesian component included in Site3','N',6,'dropdown','<SELECT id="alternateId" NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "Yes">Yes</OPTION><OPTION value = "No">No</OPTION></SELECT>');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_2','contactno','Contact No3','N',3,null,null);

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_2','drugMnftrsite','Manufacturer Site3','N',2,'chkbox','{chkArray:[{data: ''option1'', display:''MDACC/Regional Centers''}, {data: ''option2'', display:''Non-MDACC Hospital/Clinic Healthcare Facility/Harris County Hospital District Facility''}, {data: ''option3'', display:''Community Center/Shopping Mall (e.g. religious facility)''}, {data: ''option4'', display:''Educational Campus (e.g. elementary or secondary school or a university campus''}, {data: ''option5'', display:"Subject''s Home (list information on city; county; state; or country where the home is located below)"}, {data: ''option6'', display:''Internet Research (list website names below)''}, {data: ''option7'', display:''International Site''}, {data: ''option8'', display:''Other''} ]}');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_2','exclsnorg','Exclusion Organization3','N',4,'lookup','{lookupPK:2, selection:"single", mapping:[{source:"nci3_shrtname", target:"alternateId"}]}');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_2','orgsitee','Orgaization Test3','N',5,'chkbox',null);

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_2','sitename','Site Alternative Name3','N',1,'textarea',null);
end if;

  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='org_3';
if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_3','baysianCmpste','Bayesian component included in Site4','N',6,'dropdown','<SELECT id="alternateId" NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "Yes">Yes</OPTION><OPTION value = "No">No</OPTION></SELECT>');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_3','contactno','Contact No4','N',3,null,null);

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_3','drugMnftrsite','Manufacturer Site4','N',2,'chkbox','{chkArray:[{data: ''option1'', display:''MDACC/Regional Centers''}, {data: ''option2'', display:''Non-MDACC Hospital/Clinic Healthcare Facility/Harris County Hospital District Facility''}, {data: ''option3'', display:''Community Center/Shopping Mall (e.g. religious facility)''}, {data: ''option4'', display:''Educational Campus (e.g. elementary or secondary school or a university campus''}, {data: ''option5'', display:"Subject''s Home (list information on city; county; state; or country where the home is located below)"}, {data: ''option6'', display:''Internet Research (list website names below)''}, {data: ''option7'', display:''International Site''}, {data: ''option8'', display:''Other''} ]}');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_3','exclsnorg','Exclusion Organization4','N',4,'lookup','{lookupPK:2, selection:"single", mapping:[{source:"nci3_shrtname", target:"alternateId"}]}');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_3','orgsitee','Orgaization Test4','N',5,'chkbox',null);

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_3','sitename','Site Alternative Name4','N',1,'textarea',null);
end if;

  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='org_4';
if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_4','baysianCmpste','Bayesian component included in Site5','N',6,'dropdown','<SELECT id="alternateId" NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "Yes">Yes</OPTION><OPTION value = "No">No</OPTION></SELECT>');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_4','contactno','Contact No5','N',3,null,null);

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_4','drugMnftrsite','Manufacturer Site5','N',2,'chkbox','{chkArray:[{data: ''option1'', display:''MDACC/Regional Centers''}, {data: ''option2'', display:''Non-MDACC Hospital/Clinic Healthcare Facility/Harris County Hospital District Facility''}, {data: ''option3'', display:''Community Center/Shopping Mall (e.g. religious facility)''}, {data: ''option4'', display:''Educational Campus (e.g. elementary or secondary school or a university campus''}, {data: ''option5'', display:"Subject''s Home (list information on city; county; state; or country where the home is located below)"}, {data: ''option6'', display:''Internet Research (list website names below)''}, {data: ''option7'', display:''International Site''}, {data: ''option8'', display:''Other''} ]}');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_4','exclsnorg','Exclusion Organization5','N',4,'lookup','{lookupPK:2, selection:"single", mapping:[{source:"nci3_shrtname", target:"alternateId"}]}');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_4','orgsitee','Orgaization Test5','N',5,'chkbox',null);

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_4','sitename','Site Alternative Name5','N',1,'textarea',null);
end if;

  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='org_5';
if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_5','baysianCmpste','Bayesian component included in Site6','N',6,'dropdown','<SELECT id="alternateId" NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "Yes">Yes</OPTION><OPTION value = "No">No</OPTION></SELECT>');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_5','contactno','Contact No6','N',3,null,null);

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_5','drugMnftrsite','Manufacturer Site6','N',2,'chkbox','{chkArray:[{data: ''option1'', display:''MDACC/Regional Centers''}, {data: ''option2'', display:''Non-MDACC Hospital/Clinic Healthcare Facility/Harris County Hospital District Facility''}, {data: ''option3'', display:''Community Center/Shopping Mall (e.g. religious facility)''}, {data: ''option4'', display:''Educational Campus (e.g. elementary or secondary school or a university campus''}, {data: ''option5'', display:"Subject''s Home (list information on city; county; state; or country where the home is located below)"}, {data: ''option6'', display:''Internet Research (list website names below)''}, {data: ''option7'', display:''International Site''}, {data: ''option8'', display:''Other''} ]}');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_5','exclsnorg','Exclusion Organization6','N',4,'lookup','{lookupPK:2, selection:"single", mapping:[{source:"nci3_shrtname", target:"alternateId"}]}');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_5','orgsitee','Orgaization Test6','N',5,'chkbox',null);

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_5','sitename','Site Alternative Name6','N',1,'textarea',null);
end if;

  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='org_6';
if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_6','baysianCmpste','Bayesian component included in Site7','N',6,'dropdown','<SELECT id="alternateId" NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "Yes">Yes</OPTION><OPTION value = "No">No</OPTION></SELECT>');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_6','contactno','Contact No7','N',3,null,null);

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_6','drugMnftrsite','Manufacturer Site7','N',2,'chkbox','{chkArray:[{data: ''option1'', display:''MDACC/Regional Centers''}, {data: ''option2'', display:''Non-MDACC Hospital/Clinic Healthcare Facility/Harris County Hospital District Facility''}, {data: ''option3'', display:''Community Center/Shopping Mall (e.g. religious facility)''}, {data: ''option4'', display:''Educational Campus (e.g. elementary or secondary school or a university campus''}, {data: ''option5'', display:"Subject''s Home (list information on city; county; state; or country where the home is located below)"}, {data: ''option6'', display:''Internet Research (list website names below)''}, {data: ''option7'', display:''International Site''}, {data: ''option8'', display:''Other''} ]}');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_6','exclsnorg','Exclusion Organization7','N',4,'lookup','{lookupPK:2, selection:"single", mapping:[{source:"nci3_shrtname", target:"alternateId"}]}');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_6','orgsitee','Orgaization Test7','N',5,'chkbox',null);

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'org_6','sitename','Site Alternative Name7','N',1,'textarea',null);
end if;

  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='user_0';
if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_0','baysianCmpste','Bayesian component included in User1','N',6,'dropdown','<SELECT id="alternateId" NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "Yes">Yes</OPTION><OPTION value = "No">No</OPTION></SELECT>');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_0','contactno','Contact No1','N',3,null,null);

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_0','drugMnftruser','Manufacturer User1','N',2,'chkbox','{chkArray:[{data: ''option1'', display:''MDACC/Regional Centers''}, {data: ''option2'', display:''Non-MDACC Hospital/Clinic Healthcare Facility/Harris County Hospital District Facility''}, {data: ''option3'', display:''Community Center/Shopping Mall (e.g. religious facility)''}, {data: ''option4'', display:''Educational Campus (e.g. elementary or secondary school or a university campus''}, {data: ''option5'', display:"Subject''s Home (list information on city; county; state; or country where the home is located below)"}, {data: ''option6'', display:''Internet Research (list website names below)''}, {data: ''option7'', display:''International Site''}, {data: ''option8'', display:''Other''} ]}');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_0','exclsnusr','Exclusion User1','N',4,'lookup','{lookupPK:2, selection:"single", mapping:[{source:"nci3_shrtname", target:"alternateId"}]}');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_0','orguser','User Test1','N',5,'chkbox',null);

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_0','username','User Alternative Name1','N',1,'textarea',null);
end if;
  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='user_1';
if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_1','baysianCmpste','Bayesian component included in User2','N',6,'dropdown','<SELECT id="alternateId" NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "Yes">Yes</OPTION><OPTION value = "No">No</OPTION></SELECT>');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_1','contactno','Contact No2','N',3,null,null);

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_1','drugMnftrUser','Manufacturer User2','N',2,'chkbox','{chkArray:[{data: ''option1'', display:''MDACC/Regional Centers''}, {data: ''option2'', display:''Non-MDACC Hospital/Clinic Healthcare Facility/Harris County Hospital District Facility''}, {data: ''option3'', display:''Community Center/Shopping Mall (e.g. religious facility)''}, {data: ''option4'', display:''Educational Campus (e.g. elementary or secondary school or a university campus''}, {data: ''option5'', display:"Subject''s Home (list information on city; county; state; or country where the home is located below)"}, {data: ''option6'', display:''Internet Research (list website names below)''}, {data: ''option7'', display:''International Site''}, {data: ''option8'', display:''Other''} ]}');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_1','exclsnusr','Exclusion User2','N',4,'lookup','{lookupPK:2, selection:"single", mapping:[{source:"nci3_shrtname", target:"alternateId"}]}');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_1','orguser','User Test2','N',5,'chkbox',null);

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_1','username','user Alternative Name2','N',1,'textarea',null);
end if;

  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='user_2';
if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_2','baysianCmpste','Bayesian component included in User3','N',6,'dropdown','<SELECT id="alternateId" NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "Yes">Yes</OPTION><OPTION value = "No">No</OPTION></SELECT>');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_2','contactno','Contact No3','N',3,null,null);

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_2','drugMnftruser','Manufacturer User3','N',2,'chkbox','{chkArray:[{data: ''option1'', display:''MDACC/Regional Centers''}, {data: ''option2'', display:''Non-MDACC Hospital/Clinic Healthcare Facility/Harris County Hospital District Facility''}, {data: ''option3'', display:''Community Center/Shopping Mall (e.g. religious facility)''}, {data: ''option4'', display:''Educational Campus (e.g. elementary or secondary school or a university campus''}, {data: ''option5'', display:"Subject''s Home (list information on city; county; state; or country where the home is located below)"}, {data: ''option6'', display:''Internet Research (list website names below)''}, {data: ''option7'', display:''International Site''}, {data: ''option8'', display:''Other''} ]}');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_2','exclsnusr','Exclusion User3','N',4,'lookup','{lookupPK:2, selection:"single", mapping:[{source:"nci3_shrtname", target:"alternateId"}]}');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_2','orguser','User Test3','N',5,'chkbox',null);

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_2','username','User Alternative Name3','N',1,'textarea',null);
end if;

  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='user_3';
if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_3','baysianCmpste','Bayesian component included in User4','N',6,'dropdown','<SELECT id="alternateId" NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "Yes">Yes</OPTION><OPTION value = "No">No</OPTION></SELECT>');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_3','contactno','Contact No4','N',3,null,null);

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_3','drugMnftruser','Manufacturer User4','N',2,'chkbox','{chkArray:[{data: ''option1'', display:''MDACC/Regional Centers''}, {data: ''option2'', display:''Non-MDACC Hospital/Clinic Healthcare Facility/Harris County Hospital District Facility''}, {data: ''option3'', display:''Community Center/Shopping Mall (e.g. religious facility)''}, {data: ''option4'', display:''Educational Campus (e.g. elementary or secondary school or a university campus''}, {data: ''option5'', display:"Subject''s Home (list information on city; county; state; or country where the home is located below)"}, {data: ''option6'', display:''Internet Research (list website names below)''}, {data: ''option7'', display:''International Site''}, {data: ''option8'', display:''Other''} ]}');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_3','exclsnusr','Exclusion User4','N',4,'lookup','{lookupPK:2, selection:"single", mapping:[{source:"nci3_shrtname", target:"alternateId"}]}');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_3','orguser','User Test4','N',5,'chkbox',null);

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_3','username','User Alternative Name4','N',1,'textarea',null);
end if;

  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='user_4';
if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_4','baysianCmpste','Bayesian component included in User5','N',6,'dropdown','<SELECT id="alternateId" NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "Yes">Yes</OPTION><OPTION value = "No">No</OPTION></SELECT>');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_4','contactno','Contact No5','N',3,null,null);

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_4','drugMnftruser','Manufacturer User5','N',2,'chkbox','{chkArray:[{data: ''option1'', display:''MDACC/Regional Centers''}, {data: ''option2'', display:''Non-MDACC Hospital/Clinic Healthcare Facility/Harris County Hospital District Facility''}, {data: ''option3'', display:''Community Center/Shopping Mall (e.g. religious facility)''}, {data: ''option4'', display:''Educational Campus (e.g. elementary or secondary school or a university campus''}, {data: ''option5'', display:"Subject''s Home (list information on city; county; state; or country where the home is located below)"}, {data: ''option6'', display:''Internet Research (list website names below)''}, {data: ''option7'', display:''International Site''}, {data: ''option8'', display:''Other''} ]}');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_4','exclsnusr','Exclusion User5','N',4,'lookup','{lookupPK:2, selection:"single", mapping:[{source:"nci3_shrtname", target:"alternateId"}]}');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_4','orguser','User Test5','N',5,'chkbox',null);

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_4','username','User Alternative Name5','N',1,'textarea',null);
end if;

  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='user_5';
if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_5','baysianCmpste','Bayesian component included in User6','N',6,'dropdown','<SELECT id="alternateId" NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "Yes">Yes</OPTION><OPTION value = "No">No</OPTION></SELECT>');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_5','contactno','Contact No6','N',3,null,null);

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_5','drugMnftruser','Manufacturer User6','N',2,'chkbox','{chkArray:[{data: ''option1'', display:''MDACC/Regional Centers''}, {data: ''option2'', display:''Non-MDACC Hospital/Clinic Healthcare Facility/Harris County Hospital District Facility''}, {data: ''option3'', display:''Community Center/Shopping Mall (e.g. religious facility)''}, {data: ''option4'', display:''Educational Campus (e.g. elementary or secondary school or a university campus''}, {data: ''option5'', display:"Subject''s Home (list information on city; county; state; or country where the home is located below)"}, {data: ''option6'', display:''Internet Research (list website names below)''}, {data: ''option7'', display:''International Site''}, {data: ''option8'', display:''Other''} ]}');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_5','exclsnusr','Exclusion User6','N',4,'lookup','{lookupPK:2, selection:"single", mapping:[{source:"nci3_shrtname", target:"alternateId"}]}');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_5','orguser','User Test6','N',5,'chkbox',null);

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_5','username','User Alternative Name6','N',1,'textarea',null);
end if;

  Select count(*) into v_record_exists
    from ER_CODELST where CODELST_TYPE='user_6';
if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_6','baysianCmpste','Bayesian component included in User7','N',6,'dropdown','<SELECT id="alternateId" NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "Yes">Yes</OPTION><OPTION value = "No">No</OPTION></SELECT>');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_6','contactno','Contact No7','N',3,null,null);

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_6','drugMnftruser','Manufacturer User7','N',2,'chkbox','{chkArray:[{data: ''option1'', display:''MDACC/Regional Centers''}, {data: ''option2'', display:''Non-MDACC Hospital/Clinic Healthcare Facility/Harris County Hospital District Facility''}, {data: ''option3'', display:''Community Center/Shopping Mall (e.g. religious facility)''}, {data: ''option4'', display:''Educational Campus (e.g. elementary or secondary school or a university campus''}, {data: ''option5'', display:"Subject''s Home (list information on city; county; state; or country where the home is located below)"}, {data: ''option6'', display:''Internet Research (list website names below)''}, {data: ''option7'', display:''International Site''}, {data: ''option8'', display:''Other''} ]}');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_6','exclsnusr','Exclusion User7','N',4,'lookup','{lookupPK:2, selection:"single", mapping:[{source:"nci3_shrtname", target:"alternateId"}]}');

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_6','orguser','User Test7','N',5,'chkbox',null);

Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) values (SEQ_ER_CODELST.nextval,null,'user_6','username','User Alternative Name7','N',1,'textarea',null);
end if;
commit;
end;
/



INSERT INTO track_patches
VALUES(seq_track_patches.nextval,379,2,'02_er_codelst_insert.sql',sysdate,'v10.1 #780');

commit;

