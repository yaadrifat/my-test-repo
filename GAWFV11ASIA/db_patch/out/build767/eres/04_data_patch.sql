set define off;
declare
null_index number;
begin
null_index:=1;
for x in(select PK_BROWSERSEARCH, BROWSERSEARCH_NAME  from ER_BROWSERSEARCH where BROWSERSEARCH_NAME is null)
loop
update ER_BROWSERSEARCH set BROWSERSEARCH_NAME='velos_'||null_index where PK_BROWSERSEARCH= x.PK_BROWSERSEARCH;
dbms_output.put_line(null_index);
null_index:=null_index+1;
End loop;
commit;
end;
/


