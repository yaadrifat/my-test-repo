set define off;
CREATE OR REPLACE
  FUNCTION "F_FORM_MANDCHECK"(
      p_formid      NUMBER,
      p_filled_form NUMBER)
    RETURN NUMBER
  AS
    v_ValCheck VARCHAR2(4000);
    v_num      NUMBER:=0;
  BEGIN
    FOR i IN
    (SELECT MP_MAPCOLNAME
    FROM er_mapform,
      er_formfld
    WHERE MP_PKFLD        = FK_FIELD
    AND FK_FORM           =p_formid
    AND FORMFLD_MANDATORY = 1
    )
    LOOP
      dbms_output.put_line(i.MP_MAPCOLNAME);
      EXECUTE immediate 'select '||i.MP_MAPCOLNAME||' from er_formslinear where FK_FORM = '||p_formid||' and FK_FILLEDFORM = '||p_filled_form INTO v_ValCheck;
      IF v_ValCheck IS NULL THEN
        dbms_output.put_line('test'||i.MP_MAPCOLNAME);
        v_num:=1;
      END IF;
    END LOOP;
    RETURN v_num ;
  END ;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,366,3,'03_F_Form_MandCheck.sql',sysdate,'v10 #767');

commit;