set define off;

BEGIN
    UPDATE er_studystat SET STUDYSTAT_HSPN = NULL;
  commit;
  
END;
/


DECLARE
  v_column_exists number;

BEGIN
  Select count(*) into v_column_exists
  from user_tab_cols
  where TABLE_NAME = 'ER_STUDYSTAT'
  AND COLUMN_NAME = 'STUDYSTAT_HSPN';
    
  IF(v_column_exists != 0) THEN
    execute immediate 'alter table ER_STUDYSTAT modify STUDYSTAT_HSPN varchar2(5 byte)';
  END IF;
  commit;
  
END;
/
  
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,357,2,'02_update_erstudy.sql',sysdate,'v10 #758');

commit;
