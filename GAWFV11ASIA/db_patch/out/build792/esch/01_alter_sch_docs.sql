set define off;
declare
  v_table_check number;
  v_row_check number;
begin
  select count(*) into v_table_check from user_tables where table_name='SCH_DOCS';
  if v_table_check > 0
  then
    select count(*) into v_row_check from user_tab_cols where table_name='SCH_DOCS' and column_name='DOC_VERSION';
    if v_row_check = 0
    then
      execute immediate 'alter table SCH_DOCS add DOC_VERSION VARCHAR2(15 BYTE)';
    end if;
  end if;
  commit;
end;
/
comment on column "ESCH"."SCH_DOCS"."DOC_VERSION" is 'This column stores version of the document uploaded through Network tab.';
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,391,1,'01_alter_sch_docs.sql',sysdate,'v11 #792');

commit;