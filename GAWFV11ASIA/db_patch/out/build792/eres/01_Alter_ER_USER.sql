set define off;
Declare
cnt number:=0;
Begin
for i in (select count(*),usr_logname from er_user where usr_logname is not null group by usr_logname having COUNT(*)>1) 
loop
SYS.DBMS_OUTPUT.PUT_LINE(i.usr_logname);
for k in (select rownum,a.* from (select pk_user,usr_logname from er_user where usr_logname=i.usr_logname order by pk_user) a) 
loop
if (k.rownum>1) then
SYS.DBMS_OUTPUT.PUT_LINE(k.pk_user);
update er_user set usr_logname =usr_logname||(k.rownum-1) where pk_user=k.pk_user;
end if;
end loop;
commit;
end loop;
End;
/

ALTER TABLE ER_USER
ADD CONSTRAINT usrlogname_unique UNIQUE (USR_LOGNAME);

	INSERT INTO track_patches
	VALUES(seq_track_patches.nextval,391,1,'01_Alter_ER_USER.sql',sysdate,'v11 #792');

	commit; 