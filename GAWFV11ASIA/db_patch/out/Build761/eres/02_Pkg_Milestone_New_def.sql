create or replace
PACKAGE      Pkg_Milestone_New
AS

 PROCEDURE SP_TRANSFER_ACH_PM;
 PROCEDURE SP_TRANSFER_ACH_VMEM;
 PROCEDURE   SP_TRANSFER_ACH_SM;

 FUNCTION f_getMilestoneSetting(p_account IN NUMBER, p_FK_CODELST_SET_MILESTATUS in number) RETURN number;
 FUNCTION f_getMilestoneDesc(p_milestoneid IN NUMBER) RETURN clob;

 PROCEDURE   sp_process_milestones;

pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'PKG_MILESTONE_NEW', pLEVEL  => Plog.LFATAL);

FUNCTION F_GET_MILE_DESC_SQL(P_FROM_DATE IN VARCHAR2,  P_TO_DATE IN VARCHAR2, p_site IN VARCHAR2,p_study IN VARCHAR2, p_Division IN VARCHAR2,
p_tarea IN VARCHAR2, p_restype IN VARCHAR2, p_Sponsor IN VARCHAR2, p_studystatus IN VARCHAR2, p_user IN VARCHAR2,p_Patient IN VARCHAR2,
p_budget IN VARCHAR2,p_account IN NUMBER,p_loggedinuser IN NUMBER) RETURN clob;

FUNCTION F_GET_MILE_INTERVAL_XML(P_FROM_DATE IN VARCHAR2,  P_TO_DATE IN VARCHAR2,p_study IN VARCHAR2, p_Division IN VARCHAR2,
p_tarea IN VARCHAR2, p_restype IN VARCHAR2, p_Sponsor IN VARCHAR2, p_studystatus IN VARCHAR2, p_user IN VARCHAR2,
p_account IN NUMBER ,p_loggedinuser IN NUMBER) RETURN CLOB;

FUNCTION F_GET_MILE_ARAP_XML(P_FROM_DATE IN VARCHAR2,  P_TO_DATE IN VARCHAR2,p_study IN VARCHAR2, p_Division IN VARCHAR2,
p_tarea IN VARCHAR2, p_restype IN VARCHAR2, p_Sponsor IN VARCHAR2, p_studystatus IN VARCHAR2, p_user IN VARCHAR2,
p_account IN NUMBER ,p_loggedinuser IN NUMBER) RETURN CLOB;

FUNCTION getMileInvoiceAmount(p_pkmilestone NUMBER, p_fromdate VARCHAR2, p_to_date VARCHAR2) RETURN NUMBER;

FUNCTION getMilePaymentAmount(p_pkmilestone NUMBER, p_fromdate VARCHAR2, p_to_date VARCHAR2 ,p_payment_type VARCHAR2) RETURN NUMBER;

FUNCTION F_GET_MILE_PAY_DISCREPANCY_XML(P_FROM_DATE IN VARCHAR2,  P_TO_DATE IN VARCHAR2,p_study IN VARCHAR2, p_Division IN VARCHAR2,
p_tarea IN VARCHAR2, p_restype IN VARCHAR2, p_Sponsor IN VARCHAR2, p_studystatus IN VARCHAR2, p_user IN VARCHAR2,
p_account IN NUMBER ,p_loggedinuser IN NUMBER) RETURN CLOB;

FUNCTION F_GET_MILE_FORECAST_XML(P_FROM_DATE IN VARCHAR2, P_TO_DATE IN VARCHAR2,p_study IN VARCHAR2, p_Division IN VARCHAR2,
p_tarea IN VARCHAR2, p_restype IN VARCHAR2, p_Sponsor IN VARCHAR2, p_studystatus IN VARCHAR2, p_user IN VARCHAR2,
p_account IN NUMBER ,p_loggedinuser IN NUMBER) RETURN CLOB;

FUNCTION F_GET_MILE_FOREDTL_XML(P_FROM_DATE IN VARCHAR2, P_TO_DATE IN VARCHAR2,
p_site IN VARCHAR2, p_study IN VARCHAR2, p_Division IN VARCHAR2,
p_tarea IN VARCHAR2, p_restype IN VARCHAR2, p_Sponsor IN VARCHAR2, p_studystatus IN VARCHAR2, p_user IN VARCHAR2,
p_patient IN VARCHAR2,p_account IN NUMBER ,p_loggedinuser IN NUMBER) RETURN CLOB;

FUNCTION F_GET_MILE_FORECAST_COUNT(p_study IN NUMBER, p_rule IN VARCHAR2, p_count IN NUMBER,p_user IN NUMBER,p_fromdate IN VARCHAR2,
 p_todate IN VARCHAR2 ,p_event IN VARCHAR2, p_visit IN NUMBER,p_cal IN NUMBER) RETURN NUMBER;

 PROCEDURE   SP_SYNCH_INV_DETAILS(p_inv IN NUMBER, p_user IN NUMBER, p_ipAdd IN VARCHAR2, o_ret OUT NUMBER);

 PROCEDURE   SP_CREATE_VM_MILESTONES(p_budget IN NUMBER, p_study IN NUMBER, p_user IN NUMBER, p_ipAdd IN VARCHAR2, p_bgtcalId  IN NUMBER,
 P_Rule In Number, P_Eventstatus In Number,P_Count In Number, P_Patstatus In Number,P_Limit In Number, P_Payment_Type In Number,
 p_paymentfor IN NUMBER, p_vm_sponsorcheck IN NUMBER, p_dateFrom IN Date, p_dateTo IN Date, p_mileStatus IN NUMBER,p_holdback IN NUMBER, o_ret OUT NUMBER);

 PROCEDURE   SP_CREATE_EM_MILESTONES(p_budget IN NUMBER, p_study IN NUMBER, p_user IN NUMBER, p_ipAdd IN VARCHAR2, p_bgtcalId  IN NUMBER,
 P_Rule In Number, P_Eventstatus In Number,P_Count In Number, P_Patstatus In Number,P_Limit In Number, P_Payment_Type In Number,
 p_paymentfor IN NUMBER, p_em_sponsorcheck IN NUMBER, p_dateFrom IN Date, p_dateTo IN Date, p_mileStatus IN NUMBER,p_holdback IN NUMBER, o_ret OUT NUMBER);

Procedure   Sp_Create_Am_Milestones(P_Budget In Number, P_Study In Number, P_User In Number, P_Ipadd In Varchar2, P_Payment_Type In Number,
 p_paymentfor IN NUMBER,  o_ret OUT NUMBER,p_bgtcalid NUMBER, p_am_sponsorcheck IN NUMBER, p_mileStatus IN NUMBER,p_holdback IN NUMBER);

END Pkg_Milestone_New;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,360,2,'02_Pkg_Milestone_New_def.sql',sysdate,'v10 #761');

commit;