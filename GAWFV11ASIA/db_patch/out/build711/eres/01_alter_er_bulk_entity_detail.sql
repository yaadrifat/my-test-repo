SET define OFF;

-- Changing the Column data type

ALTER TABLE ERES.ER_BULK_ENTITY_DETAIL
MODIFY LAST_MODIFIED_BY VARCHAR2(255 CHAR);
COMMIT;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,310,1,'01_alter_er_bulk_entity_detail.sql',sysdate,'v9.3.0 #711');

commit;


