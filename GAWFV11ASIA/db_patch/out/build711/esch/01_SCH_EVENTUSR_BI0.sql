CREATE  OR REPLACE TRIGGER SCH_EVENTUSR_BI0 BEFORE INSERT ON SCH_EVENTUSR        REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
    DECLARE
    raid NUMBER(10);
    usr VARCHAR(2000);
    usrModBy VARCHAR(2000);
    erid NUMBER(10);
    codeList_desc VARCHAR(4000);
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
 SELECT seq_audit.NEXTVAL INTO raid FROM dual;
 usr := 'New User' ;
 usrModBy := getuser(:NEW.last_modified_by);

 
  BEGIN
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
     INTO usr FROM er_user
  WHERE pk_user = :NEW.creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
    END;
 Audit_Trail.record_transaction (raid, 'SCH_EVENTUSR', erid, 'I', usr);
       Audit_Trail.column_insert (raid, 'CREATED_ON',TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'CREATOR', usr );
       Audit_Trail.column_insert (raid, 'EVENTUSR', :NEW.EVENTUSR);
       Audit_Trail.column_insert (raid, 'EVENTUSR_DURATION', :NEW.EVENTUSR_DURATION);
       Audit_Trail.column_insert (raid, 'EVENTUSR_NOTES', :NEW.EVENTUSR_NOTES);
       Audit_Trail.column_insert (raid, 'EVENTUSR_TYPE', :NEW.EVENTUSR_TYPE);
       Audit_Trail.column_insert (raid, 'FK_EVENT', :NEW.FK_EVENT);
       Audit_Trail.column_insert (raid, 'IP_ADD', :NEW.IP_ADD);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_BY', usrModBy);
       Audit_Trail.column_insert (raid, 'LAST_MODIFIED_DATE',TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
       Audit_Trail.column_insert (raid, 'PK_EVENTUSR', :NEW.PK_EVENTUSR);
       Audit_Trail.column_insert (raid, 'PROPAGATE_FROM', :NEW.PROPAGATE_FROM);
       Audit_Trail.column_insert (raid, 'RID', :NEW.RID);
COMMIT;
END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,310,1,'01_SCH_EVENTUSR_BI0.sql',sysdate,'v9.3.0 #711');

commit;

