/////*********This readMe is specific to v11 build #826(compatible with Oracle 12c)**********////
*********************************************************************************************************************************************************************
1. Regular bug fixing .

To upgrade jdbc driver(ojdbc8) with compatible oracle 12c ,We need to do the following steps before start the wildfly server:
	1.Remove the old jdbc driver jar(classes10G-10.2.0.jar,ojdbc7-12.1.0.1.jar) from <wildfly server>\standalone\deployments\velos.ear\lib
	2.Set jar(ojdbc8.jar instead of ojdbc7-12.1.0.1.jar)  to reource-root under <resources> xml tag in file module.xml(<wildfly server>\modules\system\layers\base\com\oracle\main\module.xml) 
	3.Add jar(ojdbc8.jar) in folder main (<wildfly server>\modules\system\layers\base\com\oracle\main)
	5.Delete old jar(ojdbc7-12.1.0.1.jar) from folder main(<wildfly server>\modules\system\layers\base\com\oracle\main)
	4.Change the version_client and version_server 8 to 12a in sqlnet.ora file where oracle 12c install.
 	Example:
		Currently sqlnet.ora file contains
   			NAMES.DIRECTORY_PATH=(TNSNAMES)
   			SQLNET.ALLOWED_LOGON_VERSION_CLIENT=8
   			SQLNET.ALLOWED_LOGON_VERSION_SERVER=8


