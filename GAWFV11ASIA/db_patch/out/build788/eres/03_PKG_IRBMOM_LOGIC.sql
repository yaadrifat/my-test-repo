create or replace PACKAGE BODY      PKG_IRBMOM_LOGIC AS
/******************************************************************************
   NAME:       PKG_IRBMOM_LOGIC
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        7/27/2009   Sonia Abrol          1. Created this package.

   This package is designed to provide custom logic to generate Minute of Meeting document for eIRB.
   Different accounts can have different MOM requirements, so for AS setup, each account with IRB will have its own function to return MOM
   follow following conventions:

   1. Pls see default function - f_generateMOM(p_board IN NUMBER,p_meeting_datepk Number) as an example.

   2. The Function will take two parameters : p_board - Primary Key of review board
                                                 p_meeting_datepk - PK of er_review_meeting
   3. The function will return a CLOB- generated MOM

******************************************************************************/

Function f_generateMOM(p_board IN NUMBER,p_meeting_datepk Number) return clob
AS
    v_mom_clob clob;
    v_boardname er_review_board.review_board_name%TYPE;
    v_meeting_date date;

    v_meeting_date_formatted varchar2(50);
    v_cont_rev_text clob;
    v_rev_amd_text clob;
    v_saved_meeting clob;

    v_orgname ER_SETTINGS.SETTINGS_VALUE%TYPE;
    v_account number;

begin

    begin
        select msgtxt_long into
        v_mom_clob
        from sch_msgtxt
        where msgtxt_type='irb_mom_h';

        select review_board_name,meeting_date,trim(to_char(meeting_date,'MONTH')) ||to_char(meeting_date,',DD YYYY'),meeting_minutes,er_review_board.fk_account
         into v_boardname,v_meeting_date,v_meeting_date_formatted,v_saved_meeting ,v_account
        from er_review_board,er_review_meeting
        where pk_review_board = p_board and fk_review_board =pk_review_board  and pk_review_meeting = p_meeting_datepk ;

        begin
            select SETTINGS_VALUE
            into v_orgname
            from ER_SETTINGS
            where  SETTINGS_KEYWORD = 'IRB_ORGNAME' and SETTINGS_MODNAME = 1 and SETTINGS_MODNUM = v_account;

        exception when no_data_found then
            v_orgname := '[Undefined]';

        end;
        --replace the template with data.

        if (v_saved_meeting is not null and dbms_lob.getlength(v_saved_meeting)>0 ) then

        v_mom_clob := v_saved_meeting ;

        else

            v_mom_clob := v_mom_clob || f_get_submissionContent(p_board ,p_meeting_datepk , v_account );

            v_mom_clob := v_mom_clob || f_get_othertopic(p_meeting_datepk,v_account);

            v_mom_clob := v_mom_clob || f_get_momFooterTemplate(v_account) ;


            v_mom_clob := pkg_util.replaceclob(v_mom_clob ,'{VEL_BOARDNAME}',v_boardname);
            v_mom_clob := pkg_util.replaceclob(v_mom_clob ,'{VEL_MEETINGDATE}',v_meeting_date_formatted);

            v_mom_clob := pkg_util.replaceclob(v_mom_clob ,'{VEL_CONT_REV_TEXT}',v_cont_rev_text);
            v_mom_clob := pkg_util.replaceclob(v_mom_clob ,'{VEL_REV_AMD_TEXT}',v_rev_amd_text);
            v_mom_clob := pkg_util.replaceclob(v_mom_clob ,'{VEL_ORGNAME}',v_orgname);


            /*{VEL_REV_PREVDEF_TEXT}
            {VEL_REV_NEW_TEXT}
            {VEL_REV_INTPROB_TEXT}
            {VEL_REV_EXTPROB_TEXT}
            {VEL_REV_COMP_TEXT}
     */

        end if;
    exception when no_data_found then
        v_mom_clob := '';
    end;
    return v_mom_clob ;

end;

Function f_get_othertopic(p_meeting_datepk in Number,p_account in number) return CLOB
is
v_other clob;
v_other_template clob;
v_title sch_msgtxt.MSGTXT_SHORT%TYPE;

Begin

    begin
        select MSGTXT_SHORT,msgtxt_long into
        v_title, v_other_template
        from sch_msgtxt
        where msgtxt_type='irb_topic' and nvl(fk_account,0) = p_account ;
    exception when no_data_found then

        select MSGTXT_SHORT,msgtxt_long into
        v_title, v_other_template
        from sch_msgtxt
        where msgtxt_type='irb_topic' ;

    end;

    for k in (select topic_number, meeting_topic from er_review_meeting_topic where fk_review_meeting =   p_meeting_datepk order by lower(topic_number) asc )
    loop
        v_other := v_other ||
                pkg_util.replaceClob(pkg_util.replaceClob(v_other_template, '{VEL_TOPICNUM}',k.topic_number),'{VEL_TOPIC}',k.meeting_topic);

    end loop;

    v_other := v_title || v_other;
    return v_other;

End;


Function f_get_momFooterTemplate(p_account in Number) return CLOB
is

v_footer_template clob;


Begin

    begin

        select msgtxt_long into
        v_footer_template
        from sch_msgtxt
        where msgtxt_type='irb_footer' and nvl(fk_account,0) = p_account;

    exception when no_data_found then

        select msgtxt_long into
        v_footer_template
        from sch_msgtxt
        where msgtxt_type='irb_footer' ;

    end;

       return v_footer_template ;

End;

Function f_get_submissionContent(p_board in number,p_meeting_datepk in Number, p_account in Number) return CLOB
IS
v_sub_template clob;
v_submission_desc varchar2(500);
v_counter number;

v_sub_text clob;
v_complete_text clob;
v_provisos_text clob;

Begin

    begin
        select msgtxt_long into
        v_sub_template
        from sch_msgtxt
        where msgtxt_type='irb_subtxt' and nvl(fk_account,0) = p_account ;

    exception when no_data_found then

        select msgtxt_long into
        v_sub_template
        from sch_msgtxt
        where msgtxt_type='irb_subtxt' ;

    end;


    -- get all submission types
    v_counter := 1;
    for k in (select pk_codelst, codelst_subtyp, codelst_desc from er_codelst where codelst_type = 'submission' order by lower(codelst_desc) )
    loop
            --find submission for each submission type
            v_counter := v_counter+1;
            if k.codelst_subtyp='cont_rev'
            then
            v_submission_desc := '<BR><B><U>' || to_char(v_counter,'RN') || '. Request for ' || k.codelst_desc || '</U></B><BR><BR>';
            else
            v_submission_desc := '<BR><B><U>' || to_char(v_counter,'RN') || '. Request for ' || k.codelst_desc || ' Review</U></B><BR><BR>';
            end if; 
            
            v_complete_text := v_complete_text ||  v_submission_desc ;

            for j in (select pk_submission,fk_study,fk_review_board,(select study_number from er_study where pk_study = fk_study) study_number,
            (select study_title from er_study where pk_study = fk_study) study_title,
            (SELECT (NVL(max(PAGE_VER), 0)|| '.'||CASE WHEN page_minor_ver=0 THEN '00' WHEN page_minor_ver>9 THEN TO_CHAR(page_minor_ver) ELSE '0' || TO_CHAR(page_minor_ver) END)
              FROM ui_flx_pagever WHERE page_mod_table='er_study' AND page_mod_pk=fk_study AND page_date=(select max(page_date) from ui_flx_pagever where page_mod_pk =fk_study)
              group by page_ver,page_minor_ver) version
            from er_submission_board ,er_submission where fk_review_board = p_board and fk_review_meeting = p_meeting_datepk
            and pk_submission = fk_submission and submission_type = k.pk_codelst order by er_submission.created_on  )
            loop
                    v_sub_text    :=   v_sub_template;

                    v_sub_text := pkg_UTIl.replaceClob(v_sub_text ,'{VEL_STUDYNUMBER}',j.study_number);

                    v_sub_text := pkg_UTIl.replaceClob(v_sub_text ,'{VEL_STUDYTITLE}',j.study_title);

                    if j.version is null
                    then
                      v_sub_text := pkg_UTIl.replaceClob(v_sub_text ,'{VEL_STUDYVER}','0');
                    else
                      v_sub_text := pkg_UTIl.replaceClob(v_sub_text ,'{VEL_STUDYVER}',j.version);
                    end if;

                    -- provisos

                    v_sub_text := f_get_submissionProvisos(j.pk_submission ,v_sub_text);


                    v_complete_text := v_complete_text ||  v_sub_text;
            end loop;

    end loop; -- submission type
    return v_complete_text;
End;

 Function f_get_submissionProvisos(p_submission Number,p_template clob) return CLOB
 IS
    v_return_provisos clob;

    v_proviso_text clob;
    v_proviso_complete clob;
    v_proviso_pos number;

    v_before_text clob;
    v_after_text clob;
    
    v_review_board_name varchar2(500);
    pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'sp_submit_ongoing', pLEVEL  => Plog.LFATAL);
 Begin
    
        v_review_board_name := '';
       
        v_return_provisos := p_template;

        for j in (select review_board_name ,proviso,proviso_date from er_submission_proviso , er_review_board , er_submission_board where
        er_submission_proviso.fk_submission = p_submission and er_submission_board.fk_submission =  er_submission_proviso.fk_submission
        and er_submission_board.pk_submission_board = er_submission_proviso.fk_submission_board and
        er_submission_board.fk_review_board =  pk_review_board order by lower(review_board_name) )
        loop
       
        --Plog.fatal(pCTX,'Review board name' || j.review_board_name);
        --Plog.fatal(pCTX,'V Review board name' || v_review_board_name);
          if (v_review_board_name=j.review_board_name)
          then
              v_proviso_text := v_proviso_text || '<BR>' || j.proviso || ' &nbsp;&nbsp;&nbsp;&nbsp;<b>Entered On:</b> ' || j.proviso_date ;
          else
             
             v_proviso_text := v_proviso_text || '<BR><b><u>' || j.review_board_name || ' Provisos </u></b><BR>' || j.proviso || '  &nbsp;&nbsp;&nbsp;&nbsp;<b>Entered On:</b> ' || j.proviso_date ;
          end if; 
           v_review_board_name := j.review_board_name;

        end loop;

        v_proviso_pos := dbms_lob.instr(v_return_provisos,'{VEL_PROVISOS}',1,1);

        v_before_text := dbms_lob.substr(v_return_provisos,v_proviso_pos-1,1);


        --v_after_text :=  dbms_lob.substr(v_return_provisos,dbms_lob.getlength(v_return_provisos),v_proviso_pos+14);
        --    v_return_provisos :=v_before_text ||  v_proviso_text || v_after_text;
        v_after_text :=  dbms_lob.substr(v_return_provisos,dbms_lob.getlength(v_return_provisos),v_proviso_pos+1);
        v_return_provisos :=v_before_text || v_after_text || v_proviso_text || '<BR><BR>';



        return v_return_provisos ;
 end;

END PKG_IRBMOM_LOGIC;
/

	INSERT INTO track_patches
	VALUES(seq_track_patches.nextval,387,3,'03_PKG_IRBMOM_LOGIC.sql',sysdate,'v11 #788');
	commit;
