set define off;
DECLARE
  v_column_exists number;

BEGIN
  Select count(*) into v_column_exists
  from user_tab_cols
  where TABLE_NAME = 'SCH_LINEITEM'
  AND COLUMN_NAME = 'LINEITEM_CPTCODE';
    
IF(v_column_exists != 0) THEN
    execute immediate 'alter table SCH_LINEITEM modify LINEITEM_CPTCODE varchar2(4000 BYTE)';
END IF;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,362,2,'02_SCH_LINEITEM_Update.sql',sysdate,'v10 #763');

commit;
 
  