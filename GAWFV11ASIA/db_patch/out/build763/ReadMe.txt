/////*********This readMe is specific to v10 build #763	  **********////

1. This build is going to integrate Work flow panel into traditional study related pages . Also the mandatory specification of default organization name in configBundle_custom.properties file to access Protocol build module is now removed from configuration.  

2. Create configBundle_custom.properties file in VELOS_HOME folder and include the following content between START to END. If this file already exists, just replace the already existing content with the following.  

/////********* START **********////

# Start every key with the category prefix
# Arrange keys alphabetically
# All the keys here should also be added to CFG.java
# Make the configuration change in configBundle_custom.properties, not in this file.

# Configures therapeutic area-specific disease site on Study page
# Allowed values: N = no, Y = yes
Study_TAreaDiseaseSite=N

# To enable Workflows
Workflows_Enabled=Y

# To enable Patient Workflow use PatEnrollWf_ON
Workflow_PatEnroll_WfSwitch=N

# To configure Patient Workflow type
Workflow_PatEnroll_WfType=[Workflow_PatEnroll_WfType]

# To configure Flex-Study Screen Workflow types
Workflow_FlexStudy_WfTypes=STUDY_INITIATION,STUDY_ACTIVATION,STUDY_CLOSURE

Config_Submission_Interceptor_Class=com.velos.eres.web.intercept.SubmissionInterceptorJB
#Config_Submission_Validation_Interceptor_Class=com.velos.eres.web.intercept.SubmissionValidationInterceptorJB

# To disable the link 'Add multiple Adverse Events' from AE browser 
AE_DISABLE_MULAE_LINK=N

# To configure eIRB mode: D=Default
EIRB_MODE=D

#EIRB_DEFAULT_STUDY_STAT_ORG=ORGNAME
 
# Study status color codes
study.savedItems=Not Submitted
studystat.crcSubmitted.colorcode=Pink
studystat.crcResubmitted.colorcode=Salmon
studystat.crcInRev.colorcode=RosyBrown
studystat.crcApproved.colorcode=Thistle
studystat.crcDisapproved.colorcode=YellowGreen
studystat.crcTempHold.colorcode=PaleGoldenRod
studystat.medAncRevPend.colorcode=DarkGoldenRod
studystat.medAncRevCmp.colorcode=Yellow
studystat.crcReturn.colorcode=LimeGreen
studystat.irb_add_info.colorcode=SandyBrown
studystat.irb_in_rev.colorcode=Khaki
studystat.app_CHR.colorcode=Gold
studystat.rej_CHR.colorcode=Wheat
studystat.irb_defer.colorcode=LightYellow
studystat.irb_human.colorcode=HoneyDew
studystat.irb_not_human.colorcode=Beige
studystat.irb_temp_hold.colorcode=LightBlue
studystat.irb_terminated.colorcode=LightGreen
studystat.irb_withdrawn.colorcode=LightGrey
studystat.not_active.colorcode=Lavender
studystat.sys_err_soa.colorcode=Magenta
studystat.sys_err_click.colorcode=Violet

ISQ_URL=http://localhost:8000

ISQ_AUTH=admin:4778dbb4124fa57ba3582771d4799d27

Submission_Teaugo_Url=http://127.0.0.1/velos-mda-core/soa-protocol

Submission_Username=interfaceUser

Submission_Token=DE596639423D0AE331055B0ABCA2CDB

#ISQ_BEACHHEADS='{'\"editVisit\": ['{'\"_id\": \"546b88e4c68e642392a00048\" '}', '{'\"_id\": \"546b88bfc68e642392a00052\" '}'], \"markDone\": ['{'\"_id\": \"546b88bfc68e642392a00049\" '}'], \"editMultiEvent\": ['{'\"_id\": \"546b88bfc68e642392a00051\" '}'], \"flexScreen\":['{'\"_id\": \"546b88e4c68e642392a00050\" '}'] '}'

/////********* END **********////


3. Do the following changes in the configBundle_custom.properties file

  a) Please change the key 'Workflows_Enabled' to either 'Y' or 'N' to enable('Y') or disable('N') the work flow.
  b) Please change the key 'EIRB_MODE' to either 'D' or 'LIND'. The Default 'D' mode will allow access through the tradition eCompliance features and the 'LIND' mode can enable access to Protocol build(LindFT) features. 