set define off;
DECLARE
index_exists NUMBER;
BEGIN
index_exists := 0;
SELECT count(*) INTO index_exists FROM user_indexes 
WHERE index_name='IDX_PATPROT_STAT' AND table_name = 'ER_PATPROT';

IF index_exists > 0 THEN
execute immediate 'drop index eres.IDX_PATPROT_STAT';
END IF;
execute immediate 'CREATE  INDEX "ERES"."IDX_PATPROT_STAT" ON "ERES"."ER_PATPROT" ("PATPROT_STAT")';

SELECT count(*) INTO index_exists FROM user_indexes 
WHERE index_name='IDX_ER_CTRLTAB_KEY' AND table_name = 'ER_CTRLTAB';
IF index_exists > 0 THEN
execute immediate 'drop index eres.IDX_ER_CTRLTAB_KEY';
END IF;
execute immediate 'CREATE  INDEX "ERES"."IDX_ER_CTRLTAB_KEY" ON "ERES"."ER_CTRLTAB" ("CTRL_KEY","CTRL_VALUE")';

SELECT count(*) INTO index_exists FROM user_indexes 
WHERE index_name='IDX_PS_PER_STUDY' AND table_name = 'ER_PATSTUDYSTAT';
IF index_exists > 0 THEN
execute immediate 'drop index eres.IDX_PS_PER_STUDY';
END IF;
execute immediate 'CREATE INDEX "ERES"."IDX_PS_PER_STUDY" ON "ERES"."ER_PATSTUDYSTAT" ("FK_PER","FK_STUDY","FK_CODELST_STAT")';
END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,427,7,'07_invoice_perf_indexes.sql',sysdate,'v11 #828');

commit;	
/