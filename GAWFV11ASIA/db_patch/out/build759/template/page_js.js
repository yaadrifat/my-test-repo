var studyScreenJS = {
    validate: {},
    showHideFields: {},
    runInterFieldActions: {}
};

studyScreenJS.validate = function(){
    return true;
};

studyScreenJS.runInterFieldActions = function(fld, action, type){
    if(action == 'hide'){
        if(type == 'splfld'){
            fld.style.display = "none";
        }
        else{
            fld.parentNode.parentNode.style.display = "none";
        }
    }
    else if(action == 'show'){
        if(type == 'splfld'){
            fld.style.display = "";
        }
        else{
            fld.parentNode.parentNode.style.display = "";
        }
    }
};

studyScreenJS.showHideFields = function(fldType){
    
    var biosafety = moreStudyDetFunctions.getMoreStudyDetField("biosafety");
    var biosafety1 = moreStudyDetFunctions.getMoreStudyDetField("biosafety1");
    var biosafety2 = moreStudyDetFunctions.getMoreStudyDetField("biosafety2");
    var biosafety3 = moreStudyDetFunctions.getMoreStudyDetField("biosafety3");
    
    var compnsation = moreStudyDetFunctions.getMoreStudyDetField("compnsation");
    var cmpnsation1 = moreStudyDetFunctions.getMoreStudyDetField("cmpnsation1");
    
    var confdlty = moreStudyDetFunctions.getMoreStudyDetField("confdlty");
    var confdlty1 = moreStudyDetFunctions.getMoreStudyDetField("confdlty1");
    var confdlty2 = moreStudyDetFunctions.getMoreStudyDetField("confdlty2");
    var confdlty3 = moreStudyDetFunctions.getMoreStudyDetField("confdlty3");
    var confdlty4 = moreStudyDetFunctions.getMoreStudyDetField("confdlty4");
    
    var confsecprod = moreStudyDetFunctions.getMoreStudyDetField("confsecprod");
    var confsecprod1 = moreStudyDetFunctions.getMoreStudyDetField("confsecprod1");
    var confsecprod2 = moreStudyDetFunctions.getMoreStudyDetField("confsecprod2");
    var confsecprod3 = moreStudyDetFunctions.getMoreStudyDetField("confsecprod3");
    var confsecprod4 = moreStudyDetFunctions.getMoreStudyDetField("confsecprod4");
    
    var dataPoints = moreStudyDetFunctions.getMoreStudyDetField("dataPoints");
    var dataPoints1 = moreStudyDetFunctions.getMoreStudyDetField("dataPoints1");
    var describeText = moreStudyDetFunctions.getMoreStudyDetField("describeText");
    var describeRecord = moreStudyDetFunctions.getMoreStudyDetField("describeRecord");
    
    var eligibility = moreStudyDetFunctions.getMoreStudyDetField("eligibility");
    var inclusion = moreStudyDetFunctions.getMoreStudyDetField("inclusion");
    var exclusion = moreStudyDetFunctions.getMoreStudyDetField("exclusion");
    var juvinileElig = moreStudyDetFunctions.getMoreStudyDetField("juvinileElig");
    var elderElig = moreStudyDetFunctions.getMoreStudyDetField("elderElig");
    var pregEligiblity = moreStudyDetFunctions.getMoreStudyDetField("pregEligiblity");
    var prisonorElig = moreStudyDetFunctions.getMoreStudyDetField("prisonorElig");

    var fda = moreStudyDetFunctions.getMoreStudyDetField("fda");
    var disease = moreStudyDetFunctions.getMoreStudyDetField("disease");
    var dose = moreStudyDetFunctions.getMoreStudyDetField("dose");
    var route = moreStudyDetFunctions.getMoreStudyDetField("route");
    var nonDrugStudy = moreStudyDetFunctions.getMoreStudyDetField("nonDrugStudy");

    var finanRev = moreStudyDetFunctions.getMoreStudyDetField("finanRev");
    var requirements = moreStudyDetFunctions.getMoreStudyDetField("requirements");
    var cycles = moreStudyDetFunctions.getMoreStudyDetField("cycles");
    var lab = moreStudyDetFunctions.getMoreStudyDetField("lab");
    var EKG = moreStudyDetFunctions.getMoreStudyDetField("EKG");
    var reimbursement = moreStudyDetFunctions.getMoreStudyDetField("reimbursement");
    var diagImaging = moreStudyDetFunctions.getMoreStudyDetField("diagImaging");
    var radiology = moreStudyDetFunctions.getMoreStudyDetField("radiology");
    var independentSOC = moreStudyDetFunctions.getMoreStudyDetField("independentSOC");
    var addlInfo = moreStudyDetFunctions.getMoreStudyDetField("addlInfo");

    var funding = moreStudyDetFunctions.getMoreStudyDetField("funding");
    var fundSrc = moreStudyDetFunctions.getMoreStudyDetField("fundSrc");
    var spnsrName = moreStudyDetFunctions.getMoreStudyDetField("spnsrName");
    var finSupp = moreStudyDetFunctions.getMoreStudyDetField("finSupp");
    var spnsrshp = moreStudyDetFunctions.getMoreStudyDetField("spnsrshp");
    var spnsrshp1 = moreStudyDetFunctions.getMoreStudyDetField("spnsrshp1");
    var finSupp1 = moreStudyDetFunctions.getMoreStudyDetField("finSupp1");
    var finSupp2 = moreStudyDetFunctions.getMoreStudyDetField("finSupp2");
    var finSupp3 = moreStudyDetFunctions.getMoreStudyDetField("finSupp3");
    var finSupp4 = moreStudyDetFunctions.getMoreStudyDetField("finSupp4");
    var grant = moreStudyDetFunctions.getMoreStudyDetField("grant");

    var intellProp = moreStudyDetFunctions.getMoreStudyDetField("intellProp");
    var intellProp1 = moreStudyDetFunctions.getMoreStudyDetField("intellProp1");
    var intellProp2 = moreStudyDetFunctions.getMoreStudyDetField("intellProp2");
    var intellProp3 = moreStudyDetFunctions.getMoreStudyDetField("intellProp3");
    var intellProp4 = moreStudyDetFunctions.getMoreStudyDetField("intellProp4");
    var intellProp5 = moreStudyDetFunctions.getMoreStudyDetField("intellProp5");
    
    var ide = moreStudyDetFunctions.getMoreStudyDetField("ide");
    var ide1 = moreStudyDetFunctions.getMoreStudyDetField("ide1");
    var ide2 = moreStudyDetFunctions.getMoreStudyDetField("ide2");
    var ide3 = moreStudyDetFunctions.getMoreStudyDetField("ide3");
    var ide4 = moreStudyDetFunctions.getMoreStudyDetField("ide4");
    var ide5 = moreStudyDetFunctions.getMoreStudyDetField("ide5");
    
    var nci = moreStudyDetFunctions.getMoreStudyDetField("nci");
    var nci1 = moreStudyDetFunctions.getMoreStudyDetField("nci1");
    var labtest = moreStudyDetFunctions.getMoreStudyDetField("labtest");

    var stayDurationn = moreStudyDetFunctions.getMoreStudyDetField("stayDurationn");

    var mfg = moreStudyDetFunctions.getMoreStudyDetField("mfg");
    var mfg1 = moreStudyDetFunctions.getMoreStudyDetField("mfg1");

    var invstgBroc = moreStudyDetFunctions.getMoreStudyDetField("invstgBroc");
    var invstgBrocVer = moreStudyDetFunctions.getMoreStudyDetField("invstgBrocVer");
    var invstgBrocDate = moreStudyDetFunctions.getMoreStudyDetField("invstgBrocDate");
    
    var mta = moreStudyDetFunctions.getMoreStudyDetField("mta");
    var mta1 = moreStudyDetFunctions.getMoreStudyDetField("mta1");
    var mta2 = moreStudyDetFunctions.getMoreStudyDetField("mta2");
    var mta3 = moreStudyDetFunctions.getMoreStudyDetField("mta3");
    
    var nseq = moreStudyDetFunctions.getMoreStudyDetField("nseq");
    var nseq1 = moreStudyDetFunctions.getMoreStudyDetField("nseq1");
    var nseq2 = moreStudyDetFunctions.getMoreStudyDetField("nseq2");
    var nseq3 = moreStudyDetFunctions.getMoreStudyDetField("nseq3");
    var nseq4 = moreStudyDetFunctions.getMoreStudyDetField("nseq4");    

    var orderSet = moreStudyDetFunctions.getMoreStudyDetField("orderSet");
    var orderSet1 = moreStudyDetFunctions.getMoreStudyDetField("orderSet1");

    var preCli = moreStudyDetFunctions.getMoreStudyDetField("preCli");
    var preCli1 = moreStudyDetFunctions.getMoreStudyDetField("preCli1");

    var protoMontr = moreStudyDetFunctions.getMoreStudyDetField("protoMontr");
    var protoMontr1 = moreStudyDetFunctions.getMoreStudyDetField("protoMontr1");
    var protoMontr2 = moreStudyDetFunctions.getMoreStudyDetField("protoMontr2");
    var protoMontr3 = moreStudyDetFunctions.getMoreStudyDetField("protoMontr3");
    var protoMontr4 = moreStudyDetFunctions.getMoreStudyDetField("protoMontr4");

    var radMat = moreStudyDetFunctions.getMoreStudyDetField("radMat");
    var radMat1 = moreStudyDetFunctions.getMoreStudyDetField("radMat1");

    var resLoc = moreStudyDetFunctions.getMoreStudyDetField("resLoc");
    var resLoc1 = moreStudyDetFunctions.getMoreStudyDetField("resLoc1");
    var resLoc2 = moreStudyDetFunctions.getMoreStudyDetField("resLoc2");
    var resLoc3 = moreStudyDetFunctions.getMoreStudyDetField("resLoc3");
    var resLoc4 = moreStudyDetFunctions.getMoreStudyDetField("resLoc4");
    var resLoc5 = moreStudyDetFunctions.getMoreStudyDetField("resLoc5");
    var resLoc6 = moreStudyDetFunctions.getMoreStudyDetField("resLoc6");
    var resLoc7 = moreStudyDetFunctions.getMoreStudyDetField("resLoc7");
    var resLoc8 = moreStudyDetFunctions.getMoreStudyDetField("resLoc8");
    
    var resPop = moreStudyDetFunctions.getMoreStudyDetField("resPop");
    var resPop1 = moreStudyDetFunctions.getMoreStudyDetField("resPop1");
    var resPop2 = moreStudyDetFunctions.getMoreStudyDetField("resPop2");    
    var resPop3 = moreStudyDetFunctions.getMoreStudyDetField("resPop3");    

    var ST = moreStudyDetFunctions.getMoreStudyDetField("ST");
    
    var studyLoc = moreStudyDetFunctions.getMoreStudyDetField("studyLoc");
    var studyLocAddInfo = moreStudyDetFunctions.getMoreStudyDetField("studyLocAddInfo");

    var partPop = moreStudyDetFunctions.getMoreStudyDetField("partPop");
    var partPop1 = moreStudyDetFunctions.getMoreStudyDetField("partPop1");

    var specimen = moreStudyDetFunctions.getMoreStudyDetField("specimen");
    var specimen1 = moreStudyDetFunctions.getMoreStudyDetField("specimen1");
    var specimen2 = moreStudyDetFunctions.getMoreStudyDetField("specimen2");
    var specimen3 = moreStudyDetFunctions.getMoreStudyDetField("specimen3");
    var specimen4 = moreStudyDetFunctions.getMoreStudyDetField("specimen4");
    var specimen5 = moreStudyDetFunctions.getMoreStudyDetField("specimen5");
    var specimen6 = moreStudyDetFunctions.getMoreStudyDetField("specimen6");
    var specimen7 = moreStudyDetFunctions.getMoreStudyDetField("specimen7");
    var specimen8 = moreStudyDetFunctions.getMoreStudyDetField("specimen8");
    var specimen9 = moreStudyDetFunctions.getMoreStudyDetField("specimen9");
    var specimen10 = moreStudyDetFunctions.getMoreStudyDetField("specimen10");
    var specimen11 = moreStudyDetFunctions.getMoreStudyDetField("specimen11");
    var specimen12 = moreStudyDetFunctions.getMoreStudyDetField("specimen12");
    var specimen13 = moreStudyDetFunctions.getMoreStudyDetField("specimen13");
    var specimen14 = moreStudyDetFunctions.getMoreStudyDetField("specimen14");
    var specimen15 = moreStudyDetFunctions.getMoreStudyDetField("specimen15");
    var specimen16 = moreStudyDetFunctions.getMoreStudyDetField("specimen16");

    var icd = moreStudyDetFunctions.getMoreStudyDetField("icd");
    var icd1 = moreStudyDetFunctions.getMoreStudyDetField("icd1");
    var icd2 = moreStudyDetFunctions.getMoreStudyDetField("icd2");
    var icd3 = moreStudyDetFunctions.getMoreStudyDetField("icd3");
    var icd4 = moreStudyDetFunctions.getMoreStudyDetField("icd4");
    var icd5 = moreStudyDetFunctions.getMoreStudyDetField("icd5");
    var icd6 = moreStudyDetFunctions.getMoreStudyDetField("icd6");

    var fundSrc = moreStudyDetFunctions.getMoreStudyDetField("fundSrc");
    var fundSrc1 = moreStudyDetFunctions.getMoreStudyDetField("fundSrc1");
    var fundSrc2 = moreStudyDetFunctions.getMoreStudyDetField("fundSrc2");
    var fundSrc3 = moreStudyDetFunctions.getMoreStudyDetField("fundSrc3");

    var traineeInfo = moreStudyDetFunctions.getMoreStudyDetField("traineeInfo");
    var traineeInfo1 = moreStudyDetFunctions.getMoreStudyDetField("traineeInfo1");
    var traineeInfo2 = moreStudyDetFunctions.getMoreStudyDetField("traineeInfo2");
    var traineeInfo3 = moreStudyDetFunctions.getMoreStudyDetField("traineeInfo3");
    var traineeInfo4 = moreStudyDetFunctions.getMoreStudyDetField("traineeInfo4");
    var traineeInfo5 = moreStudyDetFunctions.getMoreStudyDetField("traineeInfo5");
    var traineeInfo6 = moreStudyDetFunctions.getMoreStudyDetField("traineeInfo6");
    var traineeInfo7 = moreStudyDetFunctions.getMoreStudyDetField("traineeInfo7");
    
    var resDesc = moreStudyDetFunctions.getMoreStudyDetField("resDesc");
    var resDesc1 = moreStudyDetFunctions.getMoreStudyDetField("resDesc1");
    var resDesc2 = moreStudyDetFunctions.getMoreStudyDetField("resDesc2");

    var diseaseGrp = moreStudyDetFunctions.getMoreStudyDetField("diseaseGrp");

    var propPlan = moreStudyDetFunctions.getMoreStudyDetField("propPlan");
    var propPlan1 = moreStudyDetFunctions.getMoreStudyDetField("propPlan1");
    var propPlan2 = moreStudyDetFunctions.getMoreStudyDetField("propPlan2");

    var studySchema = moreStudyDetFunctions.getMoreStudyDetField("studySchema");

    var accrual = moreStudyDetFunctions.getMoreStudyDetField("accrual");
    var accrual1 = moreStudyDetFunctions.getMoreStudyDetField("accrual1");
    var accrual2 = moreStudyDetFunctions.getMoreStudyDetField("accrual2");
    var accrual3 = moreStudyDetFunctions.getMoreStudyDetField("accrual3");
    var accrual4 = moreStudyDetFunctions.getMoreStudyDetField("accrual4");
    var accrual5 = moreStudyDetFunctions.getMoreStudyDetField("accrual5");
    var accrual6 = moreStudyDetFunctions.getMoreStudyDetField("accrual6");
    var accrual7 = moreStudyDetFunctions.getMoreStudyDetField("accrual7");
    var accrual8 = moreStudyDetFunctions.getMoreStudyDetField("accrual8");
    var accrual9 = moreStudyDetFunctions.getMoreStudyDetField("accrual9");
    var accrual10 = moreStudyDetFunctions.getMoreStudyDetField("accrual10");
    var accrual11 = moreStudyDetFunctions.getMoreStudyDetField("accrual11");
    var accrual12 = moreStudyDetFunctions.getMoreStudyDetField("accrual12");

    var statCons = moreStudyDetFunctions.getMoreStudyDetField("statCons");
    var statCons1 = moreStudyDetFunctions.getMoreStudyDetField("statCons1");
    var statCons2 = moreStudyDetFunctions.getMoreStudyDetField("statCons2");
    var statCons3 = moreStudyDetFunctions.getMoreStudyDetField("statCons3");
    var statCons4 = moreStudyDetFunctions.getMoreStudyDetField("statCons4");

    var protNum = moreStudyDetFunctions.getMoreStudyDetField("protNum");
    var protTitle = moreStudyDetFunctions.getMoreStudyDetField("protTitle");
    var studyChair = moreStudyDetFunctions.getMoreStudyDetField("studyChair");
    var objectives = moreStudyDetFunctions.getMoreStudyDetField("objectives");
    var rationale = moreStudyDetFunctions.getMoreStudyDetField("rationale");

    var treatmnts = moreStudyDetFunctions.getMoreStudyDetField("treatmnts");
    
    var dsmb = moreStudyDetFunctions.getMoreStudyDetField("dsmb");

    var ind = moreStudyDetFunctions.getMoreStudyDetField("ind");
    var ind1 = moreStudyDetFunctions.getMoreStudyDetField("ind1");
    var ind2 = moreStudyDetFunctions.getMoreStudyDetField("ind2");
    var ind3 = moreStudyDetFunctions.getMoreStudyDetField("ind3");
    var ind4 = moreStudyDetFunctions.getMoreStudyDetField("ind4");

    var ide = moreStudyDetFunctions.getMoreStudyDetField("ide");
    var ide6 = moreStudyDetFunctions.getMoreStudyDetField("ide6");
    var ide7 = moreStudyDetFunctions.getMoreStudyDetField("ide7");
    var ide8 = moreStudyDetFunctions.getMoreStudyDetField("ide8");
    var ide9 = moreStudyDetFunctions.getMoreStudyDetField("ide9");
    var ide10 = moreStudyDetFunctions.getMoreStudyDetField("ide10");
    var ide11 = moreStudyDetFunctions.getMoreStudyDetField("ide11");
    var ide12 = moreStudyDetFunctions.getMoreStudyDetField("ide12");
    var ide13 = moreStudyDetFunctions.getMoreStudyDetField("ide13");
    var ide14 = moreStudyDetFunctions.getMoreStudyDetField("ide14");

    var typeOfStudy = moreStudyDetFunctions.getMoreStudyDetField("typeOfStudy");

    var studyCond = moreStudyDetFunctions.getMoreStudyDetField("studyCond");
    var studyCond1 = moreStudyDetFunctions.getMoreStudyDetField("studyCond1");
    var studyCond2 = moreStudyDetFunctions.getMoreStudyDetField("studyCond2");
    var studyCond3 = moreStudyDetFunctions.getMoreStudyDetField("studyCond3");
    var studyCond4 = moreStudyDetFunctions.getMoreStudyDetField("studyCond4");
    var studyCond5 = moreStudyDetFunctions.getMoreStudyDetField("studyCond5");
    var studyCond6 = moreStudyDetFunctions.getMoreStudyDetField("studyCond6");
    var studyCond7 = moreStudyDetFunctions.getMoreStudyDetField("studyCond7");
    var studyCond8 = moreStudyDetFunctions.getMoreStudyDetField("studyCond8");

    var treatLoc = moreStudyDetFunctions.getMoreStudyDetField("treatLoc");
    var retVisits = moreStudyDetFunctions.getMoreStudyDetField("retVisits");
    var hmeCare = moreStudyDetFunctions.getMoreStudyDetField("hmeCare");
    var pubDisp = moreStudyDetFunctions.getMoreStudyDetField("pubDisp");

    var studyTeam = moreStudyDetFunctions.getMoreStudyDetField("studyTeam");
    var studyTeam1 = moreStudyDetFunctions.getMoreStudyDetField("studyTeam1");
    var studyTeam2 = moreStudyDetFunctions.getMoreStudyDetField("studyTeam2");
    var studyTeam3 = moreStudyDetFunctions.getMoreStudyDetField("studyTeam3");
    var studyTeam4 = moreStudyDetFunctions.getMoreStudyDetField("studyTeam4");
    
    var proDes = moreStudyDetFunctions.getMoreStudyDetField("proDes");
    var proDes1 = moreStudyDetFunctions.getMoreStudyDetField("proDes1");
    var proDes2 = moreStudyDetFunctions.getMoreStudyDetField("proDes2");

    var ctrc = moreStudyDetFunctions.getMoreStudyDetField("ctrc");
    var ctrc1 = moreStudyDetFunctions.getMoreStudyDetField("ctrc1");

    var gmpFacility = moreStudyDetFunctions.getMoreStudyDetField("gmpFacility");

    var labData = moreStudyDetFunctions.getMoreStudyDetField("labData");
    var labData1 = moreStudyDetFunctions.getMoreStudyDetField("labData1");
    var labData2 = moreStudyDetFunctions.getMoreStudyDetField("labData2");
    var labData3 = moreStudyDetFunctions.getMoreStudyDetField("labData3");

    var bioMark = moreStudyDetFunctions.getMoreStudyDetField("bioMark");
    var bioMark1 = moreStudyDetFunctions.getMoreStudyDetField("bioMark1");
    var bioMark2 = moreStudyDetFunctions.getMoreStudyDetField("bioMark2");
    var bioMark3 = moreStudyDetFunctions.getMoreStudyDetField("bioMark3");
    var bioMark4 = moreStudyDetFunctions.getMoreStudyDetField("bioMark4");
    var bioMark5 = moreStudyDetFunctions.getMoreStudyDetField("bioMark5");

    var priorProto = moreStudyDetFunctions.getMoreStudyDetField("priorProto");

    var addnMemoRecpnt = moreStudyDetFunctions.getMoreStudyDetField("addnMemoRecpnt");

    var unit = moreStudyDetFunctions.getMoreStudyDetField("unit");

    var protoType = moreStudyDetFunctions.getMoreStudyDetField("protoType");
    var protoPhase = moreStudyDetFunctions.getMoreStudyDetField("protoPhase");
    var versionStatus = moreStudyDetFunctions.getMoreStudyDetField("versionStatus");
    
    var bioPharm = moreStudyDetFunctions.getMoreStudyDetField("bioPharm");
    var specCategories = moreStudyDetFunctions.getMoreStudyDetField("specCategories");
    var discountOffer = moreStudyDetFunctions.getMoreStudyDetField("discountOffer");

    var ccsg = moreStudyDetFunctions.getMoreStudyDetField("ccsg");

    var sysGenVersion = moreStudyDetFunctions.getMoreStudyDetField("sysGenVersion");
    var sysGenCommRev = moreStudyDetFunctions.getMoreStudyDetField("sysGenCommRev");
    var sysGenIRBApprv = moreStudyDetFunctions.getMoreStudyDetField("sysGenIRBApprv");
    var sysGenSubm = moreStudyDetFunctions.getMoreStudyDetField("sysGenSubm");
    var sysGenSubmBy = moreStudyDetFunctions.getMoreStudyDetField("sysGenSubmBy");

    var bioMedHideSpl = [traineeInfo, mta, mta2, mta3];
    var bioMedHide = [protoType, priorProto, traineeInfo1, traineeInfo2, traineeInfo3, traineeInfo4, traineeInfo5, traineeInfo6, traineeInfo7, mta1];
    var bioMedShowSpl = [bioMark, labData, ctrc, proDes, studyTeam, studyCond, ide, ide14, ind, ind3, statCons, accrual, propPlan, resDesc, fundSrc, icd, specimen15, specimen16, specimen, partPop, studyLoc, resLoc, radMat, protoMontr, orderSet, nseq, mfg, ide, intellProp, intellProp2, biosafety, compnsation, confdlty, dataPoints, eligibility, fda, resPop];
    var bioMedShow = [ccsg, resLoc8, icd6, discountOffer, specCategories, bioPharm, protoPhase, bioMark1, bioMark2, bioMark3, bioMark4, bioMark5, labData1, labData2, gmpFacility, ctrc1, proDes1, proDes2, finSupp4, studyTeam1, studyTeam2, studyTeam3, studyTeam4, pubDisp, retVisits, treatLoc, studyCond8, studyCond7, studyCond6, studyCond5, studyCond4, studyCond3, studyCond1, ide6, ide7, ide8, ide9, ide10, ide11, ide12, ide13, ind1, ind2, ind4, resLoc6, resLoc7, dsmb, treatmnts, protTitle, studyChair, objectives, rationale, statCons1, statCons2, statCons3, statCons4, accrual1, accrual2, accrual3, accrual4, accrual5, accrual6, accrual7, accrual8, accrual9, accrual10, accrual11, accrual12, studySchema, propPlan1, propPlan2, diseaseGrp, resDesc1, resDesc2, fundSrc1, fundSrc2, fundSrc3, icd1, icd2, icd3, icd4, icd5, specimen5, specimen6, specimen7, specimen8, specimen9, specimen10, specimen11, specimen12, specimen13, specimen14, resPop3, specimen2, specimen3, specimen4, specimen1, partPop1, studyLocAddInfo, ST, resLoc1, resLoc2, resLoc3, resLoc4, resLoc5, radMat1, protoMontr1, protoMontr2, protoMontr3, protoMontr4, orderSet1, nseq1, nseq2, nseq3, nseq4, mfg1, stayDurationn, labtest, nci, nci1, ide1, ide2, ide3, ide4, ide5, intellProp5, intellProp4, intellProp3, intellProp1, spnsrshp1, spnsrName, fundSrc, grant, biosafety1, biosafety2, biosafety3, cmpnsation1, confdlty1, confdlty2, confdlty3, confdlty4, confsecprod, confsecprod1, confsecprod2, confsecprod3, confsecprod4, dataPoints1, describeText, describeRecord, inclusion, exclusion, juvinileElig, elderElig, pregEligiblity, prisonorElig, disease, dose, route, nonDrugStudy, resPop1, resPop2];
    
    var socBehvHideSpl = [ctrc, radMat, biosafety, mta, mta2, mta3];
    var socBehvHide = [discountOffer, specCategories, bioPharm, protoType, priorProto, ctrc1, specimen1, radMat1, biosafety1, biosafety2, biosafety3, mta1];
    var socBehvShowSpl = [bioMark, labData, proDes, studyTeam, studyCond, traineeInfo, ide, ide14, ind, ind3, statCons, accrual, propPlan, resDesc, fundSrc, icd, specimen15, specimen16, specimen, partPop, studyLoc, resLoc, protoMontr, orderSet, nseq, mfg, ide, intellProp, intellProp2, compnsation, confdlty, dataPoints, eligibility, fda, resPop];
    var socBehvShow = [ccsg, resLoc8, icd6, protoPhase, bioMark1, bioMark2, bioMark3, bioMark4, bioMark5, labData1, labData2, gmpFacility, proDes1, proDes2, finSupp4, studyTeam1, studyTeam2, studyTeam3, studyTeam4, pubDisp, retVisits, treatLoc, studyCond8, studyCond7, studyCond6, studyCond5, studyCond4, studyCond3, studyCond1, traineeInfo1, traineeInfo2, traineeInfo3, traineeInfo4, traineeInfo5, traineeInfo6, traineeInfo7, ide6, ide7, ide8, ide9, ide10, ide11, ide12, ide13, ind1, ind2, ind4, resLoc6, resLoc7, dsmb, treatmnts, protTitle, studyChair, objectives, rationale, statCons1, statCons2, statCons3, statCons4, accrual1, accrual2, accrual3, accrual4, accrual5, accrual6, accrual7, accrual8, accrual9, accrual10, accrual11, accrual12, studySchema, propPlan1, propPlan2, diseaseGrp, resDesc1, resDesc2, fundSrc1, fundSrc2, fundSrc3, icd1, icd2, icd3, icd4, icd5, specimen5, specimen6, specimen7, specimen8, specimen9, specimen10, specimen11, specimen12, specimen13, specimen14, resPop3, specimen2, specimen3, specimen4, partPop1, studyLocAddInfo, ST, resLoc1, resLoc2, resLoc3, resLoc4, resLoc5, protoMontr1, protoMontr2, protoMontr3, protoMontr4, orderSet1, nseq1, nseq2, nseq3, nseq4, mfg1, stayDurationn, labtest, nci, nci1, ide1, ide2, ide3, ide4, ide5, intellProp5, intellProp4, intellProp3, intellProp1, spnsrshp1, spnsrName, fundSrc, grant, cmpnsation1, confdlty1, confdlty2, confdlty3, confdlty4, confsecprod, confsecprod1, confsecprod2, confsecprod3, confsecprod4, dataPoints1, describeText, describeRecord, inclusion, exclusion, juvinileElig, elderElig, pregEligiblity, prisonorElig, disease, dose, route, nonDrugStudy, resPop1, resPop2];
    
    var bioSpecHideSpl = [ctrc, ide, ind, ide14, ind3, propPlan, radMat, protoMontr, nseq, ide, fda, mta, mta2, mta3];
    var bioSpecHide = [discountOffer, specCategories, bioPharm, protoPhase, protoType, priorProto, gmpFacility, ctrc1, pubDisp, studyCond3, ide6, ide7, ide8, ide9, ide10, ide11, ide12, ide13, ind1, ind2, ind4, dsmb, studySchema, propPlan1, propPlan2, radMat1, protoMontr1, protoMontr2, protoMontr3, protoMontr4, nseq1, nseq2, nseq3, nseq4, labtest, ide1, ide2, ide3, ide4, ide5, intellProp3, disease, dose, route, nonDrugStudy, mta1, statCons4];
    var bioSpecShowSpl = [bioMark, labData, proDes, studyTeam, studyCond, traineeInfo, statCons, accrual, resDesc, fundSrc, icd, specimen15, specimen16, specimen, partPop, studyLoc, resLoc, mfg, intellProp, intellProp2, biosafety, compnsation, confdlty, dataPoints, eligibility, resPop];
    var bioSpecShow = [ccsg, resLoc8, icd6, bioMark1, bioMark2, bioMark3, bioMark4, bioMark5, labData1, labData2, proDes1, proDes2, finSupp4, studyTeam1, studyTeam2, studyTeam3, studyTeam4, retVisits, treatLoc, studyCond8, studyCond7, studyCond6, studyCond5, studyCond4, studyCond1, traineeInfo1, traineeInfo2, traineeInfo3, traineeInfo4, traineeInfo5, traineeInfo6, traineeInfo7, resLoc6, resLoc7, protTitle, studyChair, objectives, rationale, statCons1, statCons2, statCons3, accrual1, accrual2, accrual3, accrual4, accrual5, accrual6, accrual7, accrual8, accrual9, accrual10, accrual11, accrual12, diseaseGrp, resDesc1, resDesc2, fundSrc1, fundSrc2, fundSrc3, icd1, icd2, icd3, icd4, icd5, specimen5, specimen6, specimen7, specimen8, specimen9, specimen10, specimen11, specimen12, specimen13, specimen14, resPop3, specimen2, specimen3, specimen4, specimen1, partPop1, studyLocAddInfo, ST, resLoc1, resLoc2, resLoc3, resLoc4, resLoc5, mfg1, stayDurationn, nci, nci1, intellProp5, intellProp4, intellProp1, spnsrshp1, spnsrName, fundSrc, grant, biosafety1, biosafety2, biosafety3, cmpnsation1, confdlty1, confdlty2, confdlty3, confdlty4, confsecprod, confsecprod1, confsecprod2, confsecprod3, confsecprod4, dataPoints1, describeText, describeRecord, inclusion, exclusion, juvinileElig, elderElig, pregEligiblity, prisonorElig, resPop1, resPop2];
    
    var humUseHideSpl = [bioMark, ctrc, proDes, traineeInfo, statCons, propPlan, resDesc, specimen15, specimen16, protoMontr, nseq, mfg, compnsation, mta, mta2, mta3];
    var humUseHide = [ccsg, icd6, discountOffer, specCategories, bioPharm, protoPhase, protoType, priorProto, bioMark1, bioMark2, bioMark3, bioMark4, bioMark5, ctrc1, proDes1, proDes2, finSupp4, pubDisp, retVisits, studyCond3, studyCond1, traineeInfo1, traineeInfo2, traineeInfo3, traineeInfo4, traineeInfo5, traineeInfo6, traineeInfo7, dsmb, objectives, statCons1, statCons2, statCons3, statCons4, studySchema, propPlan1, propPlan2, resDesc1, resDesc2, specimen5, specimen6, specimen7, specimen8, specimen9, specimen10, specimen11, specimen12, specimen13, specimen14, specimen2, specimen3, specimen4, protoMontr1, protoMontr2, protoMontr3, protoMontr4, nseq1, nseq2, nseq3, nseq4, mfg1, stayDurationn, nci, nci1, cmpnsation1, mta1];
    var humUseShowSpl = [labData, studyTeam, studyCond, ide, ide14, ind, ind3, accrual, fundSrc, icd, specimen, partPop, studyLoc, resLoc, radMat, orderSet, ide, intellProp, intellProp2, biosafety, confdlty, dataPoints, eligibility, fda, resPop];
    var humUseShow = [resLoc8, labData1, labData2, gmpFacility, studyTeam1, studyTeam2, studyTeam3, studyTeam4, treatLoc, studyCond8, studyCond7, studyCond6, studyCond5, studyCond4, ide6, ide7, ide8, ide9, ide10, ide11, ide12, ide13, ind1, ind2, ind4, resLoc6, resLoc7, treatmnts, protTitle, studyChair, rationale, accrual1, accrual2, accrual3, accrual4, accrual5, accrual6, accrual7, accrual8, accrual9, accrual10, accrual11, accrual12, diseaseGrp, fundSrc1, fundSrc2, fundSrc3, icd1, icd2, icd3, icd4, icd5, resPop3, specimen1, partPop1, studyLocAddInfo, ST, resLoc1, resLoc2, resLoc3, resLoc4, resLoc5, radMat1, orderSet1, labtest, ide1, ide2, ide3, ide4, ide5, intellProp5, intellProp4, intellProp3, intellProp1, spnsrshp1, spnsrName, fundSrc, grant, biosafety1, biosafety2, biosafety3, confdlty1, confdlty2, confdlty3, confdlty4, confsecprod, confsecprod1, confsecprod2, confsecprod3, confsecprod4, dataPoints1, describeText, describeRecord, inclusion, exclusion, juvinileElig, elderElig, pregEligiblity, prisonorElig, disease, dose, route, nonDrugStudy, resPop1, resPop2];
    
    var emergUseHideSpl = [bioMark, labData, ctrc, proDes, traineeInfo, statCons, propPlan, resDesc, specimen15, specimen16, specimen, protoMontr, nseq, mfg, compnsation, mta, mta2, mta3];
    var emergUseHide = [ccsg, icd6, discountOffer, specCategories, bioPharm, protoPhase, protoType, priorProto, bioMark1, bioMark2, bioMark3, bioMark4, bioMark5, labData1, labData2, ctrc1, proDes1, proDes2, finSupp4, pubDisp, retVisits, studyCond3, studyCond1, traineeInfo1, traineeInfo2, traineeInfo3, traineeInfo4, traineeInfo5, traineeInfo6, traineeInfo7, dsmb, objectives, statCons1, statCons2, statCons3, statCons4, studySchema, propPlan1, propPlan2, resDesc1, resDesc2, specimen5, specimen6, specimen7, specimen8, specimen9, specimen10, specimen11, specimen12, specimen13, specimen14, specimen2, specimen3, specimen4, protoMontr1, protoMontr2, protoMontr3, protoMontr4, nseq1, nseq2, nseq3, nseq4, mfg1, stayDurationn, labtest, nci, nci1, biosafety1, cmpnsation1, mta1];
    var emergUseShowSpl = [studyTeam, studyCond, ide, ide14, ind, ind3, accrual, fundSrc, icd, partPop, studyLoc, resLoc, radMat, ide, intellProp2, intellProp, biosafety, confdlty, dataPoints, eligibility, fda, resPop];
    var emergUseShow = [resLoc8, gmpFacility, studyTeam1, studyTeam2, studyTeam3, studyTeam4, treatLoc, studyCond8, studyCond7, studyCond6, studyCond5, studyCond4, ide6, ide7, ide8, ide9, ide10, ide11, ide12, ide13, ind1, ind2, ind4, resLoc6, resLoc7, treatmnts, protTitle, studyChair, rationale, accrual1, accrual2, accrual3, accrual4, accrual5, accrual6, accrual7, accrual8, accrual9, accrual10, accrual11, accrual12, diseaseGrp, fundSrc1, fundSrc2, fundSrc3, icd1, icd2, icd3, icd4, icd5, resPop3, specimen1, partPop1, studyLocAddInfo, ST, resLoc1, resLoc2, resLoc3, resLoc4, resLoc5, radMat1, ide1, ide2, ide3, ide4, ide5, intellProp5, intellProp4, intellProp3, intellProp1, spnsrshp1, spnsrName, fundSrc, grant, biosafety2, biosafety3, confdlty1, confdlty2, confdlty3, confdlty4, confsecprod, confsecprod1, confsecprod2, confsecprod3, confsecprod4, dataPoints1, describeText, describeRecord, inclusion, exclusion, juvinileElig, elderElig, pregEligiblity, prisonorElig, disease, dose, route, nonDrugStudy, resPop1, resPop2];
    
    var nonHsrHideSpl = [bioMark, labData, ctrc, proDes, studyTeam, traineeInfo, ide, ide14, ind, ind3, statCons, propPlan, resDesc, icd, specimen15, specimen16, partPop, radMat, protoMontr, nseq, mfg, ide, intellProp2, intellProp, biosafety, compnsation, eligibility, fda, mta, mta2, mta3, resPop];
    var nonHsrHide = [ccsg, icd6, discountOffer, specCategories, bioPharm, protoPhase, protoType, priorProto, bioMark1, bioMark2, bioMark3, bioMark4, bioMark5, labData1, labData2, gmpFacility, ctrc1, proDes1, proDes2, finSupp4, studyTeam1, studyTeam2, studyTeam3, studyTeam4, pubDisp, retVisits, treatLoc, studyCond3, studyCond1, traineeInfo1, traineeInfo2, traineeInfo3, traineeInfo4, traineeInfo5, traineeInfo6, traineeInfo7, ide6, ide7, ide8, ide9, ide10, ide11, ide12, ide13, ind1, ind2, ind4, dsmb, statCons1, statCons2, statCons3, statCons4, studySchema, propPlan1, propPlan2, resDesc1, resDesc2, specimen5, specimen6, specimen7, specimen8, specimen9, specimen10, specimen11, specimen12, specimen13, specimen14, resPop3, partPop1, ST, radMat1, protoMontr1, protoMontr2, protoMontr3, protoMontr4, nseq1, nseq2, nseq3, nseq4, mfg1, stayDurationn, labtest, nci, nci1, ide1, ide2, ide3, ide4, ide5, intellProp5, intellProp4, intellProp3, intellProp1, biosafety1, biosafety2, biosafety3, cmpnsation1, inclusion, exclusion, juvinileElig, elderElig, pregEligiblity, prisonorElig, disease, dose, route, nonDrugStudy, mta1, resPop1, resPop2];
    var nonHsrShowSpl = [studyCond, accrual, fundSrc, studyLoc, resLoc, confdlty, dataPoints];
    var nonHsrShow = [resLoc8, studyCond8, studyCond7, studyCond6, studyCond5, studyCond4, resLoc6, resLoc7, treatmnts, protTitle, studyChair, objectives, rationale, accrual1, accrual2, accrual3, accrual4, accrual5, accrual6, accrual7, accrual8, accrual9, accrual10, accrual11, accrual12, diseaseGrp, fundSrc1, fundSrc2, fundSrc3, icd1, icd2, icd3, icd4, icd5, studyLocAddInfo, resLoc1, resLoc2, resLoc3, resLoc4, resLoc5, spnsrshp1, spnsrName, fundSrc, grant, confdlty1, confdlty2, confdlty3, confdlty4, confsecprod, confsecprod1, confsecprod2, confsecprod3, confsecprod4, dataPoints1, describeText, describeRecord];
    
    switch(fldType){
    case 'bioMed':
        for(var count= 0; count<bioMedHideSpl.length; count++) {
            studyScreenJS.runInterFieldActions(bioMedHideSpl[count], 'hide', 'splfld');
        }
        for(var count= 0; count<bioMedHide.length; count++) {
            studyScreenJS.runInterFieldActions(bioMedHide[count], 'hide');
        }
        for(var count= 0; count<bioMedShowSpl.length; count++) {
            studyScreenJS.runInterFieldActions(bioMedShowSpl[count], 'show', 'splfld');
        }
        for(var count= 0; count<bioMedShow.length; count++) {
            studyScreenJS.runInterFieldActions(bioMedShow[count], 'show');
        }
        break;
    case 'socBehv':
        for(var count= 0; count<socBehvHideSpl.length; count++) {
            studyScreenJS.runInterFieldActions(socBehvHideSpl[count], 'hide', 'splfld');
        }
        for(var count= 0; count<socBehvHide.length; count++) {
            studyScreenJS.runInterFieldActions(socBehvHide[count], 'hide');
        }
        for(var count= 0; count<socBehvShowSpl.length; count++) {
            studyScreenJS.runInterFieldActions(socBehvShowSpl[count], 'show', 'splfld');
        }
        for(var count= 0; count<socBehvShow.length; count++) {
            studyScreenJS.runInterFieldActions(socBehvShow[count], 'show');
        }
        break;
    case 'bioSpec':
        for(var count= 0; count<bioSpecHideSpl.length; count++) {
            studyScreenJS.runInterFieldActions(bioSpecHideSpl[count], 'hide', 'splfld');
        }
        for(var count= 0; count<bioSpecHide.length; count++) {
            studyScreenJS.runInterFieldActions(bioSpecHide[count], 'hide');
        }
        for(var count= 0; count<bioSpecShowSpl.length; count++) {
            studyScreenJS.runInterFieldActions(bioSpecShowSpl[count], 'show', 'splfld');
        }
        for(var count= 0; count<bioSpecShow.length; count++) {
            studyScreenJS.runInterFieldActions(bioSpecShow[count], 'show');
        }
        break;
    case 'humUse':
        for(var count= 0; count<humUseHideSpl.length; count++) {
            studyScreenJS.runInterFieldActions(humUseHideSpl[count], 'hide', 'splfld');
        }
        for(var count= 0; count<humUseHide.length; count++) {
            studyScreenJS.runInterFieldActions(humUseHide[count], 'hide');
        }
        for(var count= 0; count<humUseShowSpl.length; count++) {
            studyScreenJS.runInterFieldActions(humUseShowSpl[count], 'show', 'splfld');
        }
        for(var count= 0; count<humUseShow.length; count++) {
            studyScreenJS.runInterFieldActions(humUseShow[count], 'show');
        }
        break;
    case 'emergUse':
        for(var count= 0; count<emergUseHideSpl.length; count++) {
            studyScreenJS.runInterFieldActions(emergUseHideSpl[count], 'hide', 'splfld');
        }
        for(var count= 0; count<emergUseHide.length; count++) {
            studyScreenJS.runInterFieldActions(emergUseHide[count], 'hide');
        }
        for(var count= 0; count<emergUseShowSpl.length; count++) {
            studyScreenJS.runInterFieldActions(emergUseShowSpl[count], 'show', 'splfld');
        }
        for(var count= 0; count<emergUseShow.length; count++) {
            studyScreenJS.runInterFieldActions(emergUseShow[count], 'show');
        }
        break;
    case 'nonHsr':
        for(var count= 0; count<nonHsrHideSpl.length; count++) {
            studyScreenJS.runInterFieldActions(nonHsrHideSpl[count], 'hide', 'splfld');
        }
        for(var count= 0; count<nonHsrHide.length; count++) {
            studyScreenJS.runInterFieldActions(nonHsrHide[count], 'hide');
        }
        for(var count= 0; count<nonHsrShowSpl.length; count++) {
            studyScreenJS.runInterFieldActions(nonHsrShowSpl[count], 'show', 'splfld');
        }
        for(var count= 0; count<nonHsrShow.length; count++) {
            studyScreenJS.runInterFieldActions(nonHsrShow[count], 'show');
        }
        break;
    default: break;
    }
};

$j(document).ready(function(){    
    studyScreenJS.validate();
    
    var templateType = moreStudyDetFunctions.getMoreStudyDetField("templateType");

    if(templateType) {
        studyScreenJS.showHideFields(templateType.value);

        if (!templateType.addEventListener) {
            templateType.on('change', function(e){
                templateType = moreStudyDetFunctions.getMoreStudyDetField("templateType");
                studyScreenJS.showHideFields(templateType.value);
                return false;
            });
        } else {
            document.querySelector('#'+templateType.id).addEventListener('change', function(e){
                templateType = moreStudyDetFunctions.getMoreStudyDetField("templateType");
                studyScreenJS.showHideFields(templateType.value);
                return false;
            });
        }
    }
});