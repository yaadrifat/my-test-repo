
SET DEFINE OFF;
declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'id_ccg';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','id_ccg','Coordinating Group Number','N',50,null,151,null,null,to_date('30-JAN-04','DD-MON-RR'),to_date('30-JAN-04','DD-MON-RR'),null,null,null,null,null);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'id_ccn';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','id_ccn','NCI Number','N',60,null,152,null,null,to_date('30-JAN-04','DD-MON-RR'),to_date('30-JAN-04','DD-MON-RR'),null,null,null,null,null);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'id_swog';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','id_swog','SWOG Number','N',70,null,153,null,null,to_date('30-JAN-04','DD-MON-RR'),to_date('30-JAN-04','DD-MON-RR'),null,null,null,null,null);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'id_other';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','id_other','Other Number','N',80,null,154,null,null,to_date('30-JAN-04','DD-MON-RR'),to_date('30-JAN-04','DD-MON-RR'),null,null,null,null,null);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'cancer_ctr_num';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','cancer_ctr_num','Cancer Center Number','N',10,null,348,null,null,to_date('30-JAN-04','DD-MON-RR'),to_date('30-JAN-04','DD-MON-RR'),null,null,null,null,null);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'grant_number';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','grant_number','Grant Number','N',20,null,349,null,null,to_date('30-JAN-04','DD-MON-RR'),to_date('30-JAN-04','DD-MON-RR'),null,null,null,null,null);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'invBrochureDate';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','invBrochureDate','Investigational Brochure Date','N',220,null,101,null,null,to_date('30-JAN-04','DD-MON-RR'),to_date('30-JAN-04','DD-MON-RR'),null,null,'<script language="JavaScript"><!-- date=Date() document.write(date) //--> </script>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'irb_number';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','irb_number','IRB Number','N',11,null,102,null,null,to_date('30-JAN-04','DD-MON-RR'),to_date('30-JAN-04','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundingSource';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','fundingSource','Funding Source','N',81,null,429,null,null,to_date('30-JAN-04','DD-MON-RR'),to_date('30-JAN-04','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'sum4_prg';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','sum4_prg','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Program Code','N',260,null,20714,null,null,to_date('01-MAR-04','DD-MON-RR'),to_date('01-MAR-04','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'sum4_na';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','sum4_na','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Epidemiologic or other Observational Studies','N',250,null,20715,null,null,to_date('01-MAR-04','DD-MON-RR'),to_date('01-MAR-04','DD-MON-RR'),null,'chkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'sum4_beh';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','sum4_beh','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Trials Involving other Interventions','N',240,null,20716,null,null,to_date('01-MAR-04','DD-MON-RR'),to_date('01-MAR-04','DD-MON-RR'),null,'chkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'sum4_agent';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','sum4_agent','Study type (Summary 4): Agent or Device','N',230,null,20717,null,null,to_date('01-MAR-04','DD-MON-RR'),to_date('01-MAR-04','DD-MON-RR'),null,'chkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'sum4_comp';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','sum4_comp','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Companion, Ancillary or Correlative Studies','N',null,null,90724,null,null,to_date('08-FEB-07','DD-MON-RR'),to_date('08-FEB-07','DD-MON-RR'),null,'chkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyidtype_261';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','studyidtype_261','ClinicalTrials.Gov #','N',261,null,122635,null,null,to_date('24-DEC-09','DD-MON-RR'),to_date('24-DEC-09','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyidtype_262';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','studyidtype_262','Research Account Number','N',262,null,135722,null,null,to_date('04-AUG-10','DD-MON-RR'),to_date('04-AUG-10','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyidtype_263';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','studyidtype_263','Diagnosis Code','N',3,null,137102,null,null,to_date('19-AUG-10','DD-MON-RR'),to_date('19-AUG-10','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyidtype_0';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','studyidtype_0','Short Title','N',11,null,155195,null,null,to_date('09-MAR-11','DD-MON-RR'),to_date('09-MAR-11','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyidtype_1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','studyidtype_1','FDA approved drug, used in approved manner: a.','N',2,null,155196,null,null,to_date('09-MAR-11','DD-MON-RR'),to_date('09-MAR-11','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyidtype_2';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','studyidtype_2','FDA approved drug, used in approved manner: b.','N',2,null,155198,null,null,to_date('09-MAR-11','DD-MON-RR'),to_date('09-MAR-11','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyidtype_270';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','studyidtype_270','CMS Qualifying Clinical Trial?','N',1,null,264978,null,null,null,to_date('30-DEC-11','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> 		<OPTION value = "">Select an option</OPTION> 		<OPTION value = "Yes">Yes</OPTION> 		<OPTION value = "No">No</OPTION> 		</SELECT> ',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'investiDevice';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','investiDevice','Does the study include an investigational device?','N',261,null,282420,null,null,to_date('31-JAN-11','DD-MON-RR'),to_date('07-MAY-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> 		<OPTION value = "">Select an option</OPTION> 		<OPTION value = "Yes">Yes (Complete Investigational Device Form by clicking on the Study Form tab)</OPTION> 		<OPTION value = "No">No</OPTION> 		</SELECT> ',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyOccurs';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','studyOccurs','Study occurs in an in - patient setting','N',262,null,282421,null,null,to_date('31-JAN-11','DD-MON-RR'),to_date('07-MAY-14','DD-MON-RR'),null,'chkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyProvide';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','studyProvide','Study provides radiation','N',263,null,282422,null,null,to_date('31-JAN-11','DD-MON-RR'),to_date('07-MAY-14','DD-MON-RR'),null,'chkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'juvinileEligYes';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','juvinileEligYes','Additional Comment','N',432,null,285106,null,null,null,to_date('04-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'recrutPopIncarc';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','recrutPopIncarc','Will the recruitment population at M. D. Anderson include persons who are incarcerated at time of enrollment (e.g., prisoners) or likely to become incarcerated during the study?','N',433,null,285104,null,null,null,to_date('04-AUG-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'grantingAgency';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','grantingAgency','Does the Study have a Sponsor, Supporter or Granting Agency?','N',434,null,285103,null,null,null,to_date('05-AUG-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'grntnAgncyData';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','grntnAgncyData','Will the Sponsor/Supporter/Granting Agency receive data?','N',435,null,285102,null,null,null,to_date('05-AUG-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'radioComp';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','radioComp','Does this protocol include the administration of a radioactive compound (or drug) to a patient intended to obtain basic information regarding metabolism (including kinetics, distribution, and localization) of the drug or regarding human physiology, pathophysiology, biochemistry, but not intended for immediate therapeutic, diagnostic, or similar purposes or to determine the safety and effectiveness of the drug in humans for such purposes (i.e. to carry out a clinical study)?','N',436,null,285101,null,null,null,to_date('05-AUG-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'radioCompAvail';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','radioCompAvail','<br>Is the radioactive compound (or drug) FDA approved and/or commercially available?','N',437,null,285100,null,null,null,to_date('05-AUG-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'clicResrCtgry';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','clicResrCtgry','Clinical Research Category','N',438,null,285099,null,null,null,to_date('05-AUG-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "INT">Interventional (INT)</OPTION> <OPTION value = "OBS">Observational (OBS)</OPTION> <OPTION value = "ANCCOR">Ancillary or Correlative (ANC/COR)</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'preClincData';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','preClincData','<A href="#"><span onmouseover="return overlib(''Summarize the available non-clinical data (published or available unpublished data) that could have clinical significance..'', CAPTION,''Pre-clinical Data'',LEFT, ABOVE);" onmouseout="return nd();" alt=""> Pre-clinical Data </span></A>','N',441,null,285098,null,null,null,to_date('05-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'preCliDrugDev';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','preCliDrugDev','Was preclinical work to develop the therapy or methodology being tested in this protocol performed at UTMDACC or by UTMDACC faculty?','N',439,null,285097,null,null,null,to_date('05-AUG-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'stdyDisease';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','stdyDisease','<A href="#"> <img src="../images/jpg/help.jpg" border="0"  onmouseover="return overlib(''This section should contain a background discussion of the target disease state to which the investigational product(s) hold promise, and any pathophysiology relevant to potential study treatment action.'', CAPTION,''Study Disease'',LEFT, ABOVE);" onmouseout="return nd();" alt=""> </img> </A> Study Disease (Description of Disease)','N',440,null,285096,null,null,null,to_date('06-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'clincDataToDate';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','clincDataToDate','Clinical Data to Date <br> <i><small>Summarize the available clinical study data (published or available unpublished data) with relevance to the protocol under construction -- if none is available, include a statement that there is no available clinical research data to date on the investigational product.</small></i>','N',442,null,285095,null,null,null,to_date('06-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'lngTrmFollowUp';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','lngTrmFollowUp','Long Term Follow-up','N',455,null,285094,null,null,null,to_date('06-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'patAssessmnt';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','patAssessmnt','Patient Assessments','N',449,null,285093,null,null,null,to_date('06-AUG-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'womenPartici';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','womenPartici','Women','N',446,null,285092,null,null,null,to_date('06-AUG-14','DD-MON-RR'),null,'dropdown','<SELECT id="alternateId" NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'explrtObjctv';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','explrtObjctv','Exploratory Objective','N',444,null,285091,null,null,null,to_date('06-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'corrStdBkgrnd';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','corrStdBkgrnd','Correlative Studies Background','N',443,null,285090,null,null,null,to_date('06-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'elgbltyCriteria';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','elgbltyCriteria','<h4>Eligibility Criteria</h4>','N',445,null,285089,null,null,null,to_date('06-AUG-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'subRecrScreen';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','subRecrScreen','<h4>Subject Recruitment and Screening</h4>','N',447,null,285088,null,null,null,to_date('06-AUG-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'stdyCalndr';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','stdyCalndr','Study Calendar/Schedule of Events','N',448,null,285087,null,null,null,to_date('06-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'endTheraEvals';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','endTheraEvals','<li>End of Therapy Evaluations','N',453,null,285086,null,null,null,to_date('06-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'medMontrng';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','medMontrng','Medical Monitoring','N',451,null,285085,null,null,null,to_date('06-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'baselineEvals';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','baselineEvals','<li>Screening/Baseline','N',450,null,285084,null,null,null,to_date('06-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'onstdyEvals';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','onstdyEvals','<li>On-study/Treatment Evaluations','N',452,null,285083,null,null,null,to_date('06-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'followUp';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','followUp','<li>Follow-up','N',454,null,285082,null,null,null,to_date('06-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'womenParticiYes';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','womenParticiYes','Additional Comment','N',456,null,285081,null,null,null,to_date('07-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'recrStrategy';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','recrStrategy','<i>Recruitment Strategy</i>','N',468,null,285080,null,null,null,to_date('07-AUG-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'diseaseGrp';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','diseaseGrp','<i>Disease Group</i>','N',466,null,285079,null,null,null,to_date('07-AUG-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'priInvConsor';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','priInvConsor','Principal Investigator for the Consortium','N',461,null,285078,null,null,null,to_date('07-AUG-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'pregEligbltyYes';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','pregEligbltyYes','Additional Comment','N',459,null,285077,null,null,null,to_date('07-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'minorPartici';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','minorPartici','Minorities','N',457,null,285076,null,null,null,to_date('07-AUG-14','DD-MON-RR'),null,'dropdown','<SELECT id="alternateId" NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'minorParticiYes';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','minorParticiYes','Additional Comment','N',458,null,285075,null,null,null,to_date('07-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'consortiName';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','consortiName','Consortium Name','N',460,null,285074,null,null,null,to_date('07-AUG-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseRationale';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','doseRationale','Dose Rationale','N',464,null,285073,null,null,null,to_date('07-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'descDrug';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','descDrug','Description of Drug','N',462,null,285072,null,null,null,to_date('07-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'descDevice';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','descDevice','Description of Device','N',463,null,285071,null,null,null,to_date('11-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'trtmntArms';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','trtmntArms','Treatment Arms','N',465,null,285070,null,null,null,to_date('11-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'subRecrLoc';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','subRecrLoc','<i>Location of Subject Recruitment</i>','N',467,null,285069,null,null,null,to_date('11-AUG-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'constProced';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','constProced','<i>Consenting Procedures</i>','N',469,null,285068,null,null,null,to_date('11-AUG-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'spnsrdProt';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','spnsrdProt','Protocol Template','N',472,null,285067,null,null,null,to_date('11-AUG-14','DD-MON-RR'),null,'dropdown','{codelst_type:"LindTemplateTyp"}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'tarPopPat';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','tarPopPat','Targeted Population of Patients','N',470,null,285066,null,null,null,to_date('11-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'stdyIncludes';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','stdyIncludes','Study includes','N',471,null,285065,null,null,null,to_date('12-AUG-14','DD-MON-RR'),null,'checkbox','{chkArray:[ {data: "option1", display:"Drug"}, {data: "option2", display:"Device"}, {data: "option3", display:"Imaging"}, {data: "option4", display:"Surgical"}, {data: "option5", display:"Radiotherapy"}, {data: "option6", display:"Cell Therapeutic"}, {data: "option7", display:"Gene Therapy"} ]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'protAttUpld';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','protAttUpld','Protocol attachment uploaded','N',473,null,285064,null,null,null,to_date('12-AUG-14','DD-MON-RR'),null,'dropdown','<SELECT id="alternateId" NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'stdRemCriteria';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','stdRemCriteria','Criteria for Removal of Participants','N',476,null,285063,null,null,null,to_date('12-AUG-14','DD-MON-RR'),null,'checkbox','{chkArray:[ {data: "option1", display:"Option1"}, {data: "option2", display:"Option2"}, {data: "option3", display:"Other"} ]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'wthdrwSubHow';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','wthdrwSubHow','When and how to withdraw subjects','N',474,null,285062,null,null,null,to_date('12-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'wthdrwFollow';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','wthdrwFollow','Data collection','N',475,null,285061,null,null,null,to_date('12-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'trtmntPlan';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','trtmntPlan','Treatment Plan','N',477,null,285060,null,null,null,to_date('12-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'rplSubject';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','rplSubject','Criteria for Replacement of Participants','N',486,null,285059,null,null,null,to_date('12-AUG-14','DD-MON-RR'),null,'hidden-input',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'blndTrtGrp';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','blndTrtGrp','Blinding Description','N',484,null,285058,null,null,null,to_date('12-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'accrualHead';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','accrualHead','<h4>Accrual</>','N',481,null,285057,null,null,null,to_date('12-AUG-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'scndryEndPts';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','scndryEndPts','Secondary Endpoints','N',479,null,285056,null,null,null,to_date('12-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'stdDesEndPts';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','stdDesEndPts','<h4>Study Design and Endpoints</>','N',478,null,285055,null,null,null,to_date('12-AUG-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'expEndPts';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','expEndPts','Exploratory Endpoints','N',480,null,285054,null,null,null,to_date('12-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'trtmntGrpHead';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','trtmntGrpHead','<h4>Methods for Assigning Subjects to Treatment Groups</>','N',482,null,285053,null,null,null,to_date('12-AUG-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'rndmnTrtGrp';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','rndmnTrtGrp','Randomization notes','N',483,null,285052,null,null,null,to_date('12-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'unblnTrtGrp';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','unblnTrtGrp','Unblinding notes','N',485,null,285051,null,null,null,to_date('12-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'statAnalPlanHd';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','statAnalPlanHd','<h4>Statistical Analysis Plan</>','N',487,null,285050,null,null,null,to_date('12-AUG-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'statAnalPlan';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','statAnalPlan','Statistical Analysis Plan','N',488,null,285049,null,null,null,to_date('12-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'intrMonHead';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','intrMonHead','<h4>Interim Monitoring Plan</>','N',489,null,285048,null,null,null,to_date('12-AUG-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundnSrc';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','fundnSrc','<h4>Funding Source</>','N',501,null,285047,null,null,null,to_date('12-AUG-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'repIRB';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','repIRB','Reporting to IRB','N',496,null,285046,null,null,null,to_date('12-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'repSAEs';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','repSAEs','Report of SAEs and Unanticipated Problems','N',494,null,285045,null,null,null,to_date('12-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'defSAE';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','defSAE','Define SAEs','N',492,null,285044,null,null,null,to_date('12-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'cohrtSummHead';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','cohrtSummHead','<h4>Cohort Summaries</>','N',490,null,285043,null,null,null,to_date('12-AUG-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'cohrtSumm';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','cohrtSumm','Cohort Summaries','N',491,null,285042,null,null,null,to_date('12-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'recrdAE';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','recrdAE','Recording of AEs','N',493,null,285041,null,null,null,to_date('12-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'repSponsr';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','repSponsr','Reporting to Sponsor','N',495,null,285040,null,null,null,to_date('12-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dsmbExt';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','dsmbExt','Independent/External','N',497,null,285039,null,null,null,to_date('12-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'publPlan';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','publPlan','Publication Plan','N',503,null,285032,null,null,null,to_date('13-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dataConfPlan';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','dataConfPlan','<h4>Data Confidentiality Plan</>','N',498,null,285031,null,null,null,to_date('13-AUG-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'recRetention';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','recRetention','<h4>Records Retention</>','N',500,null,285030,null,null,null,to_date('13-AUG-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dataCollTool';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','dataCollTool','<h4>Data Collection Tool</>','N',499,null,285029,null,null,null,to_date('13-AUG-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'subStipndPay';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','subStipndPay','<h4>Subject Stipends/Payments</>','N',502,null,285028,null,null,null,to_date('13-AUG-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'selMoonshot';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','selMoonshot','Select a Moonshot','N',505,null,285027,null,null,null,to_date('14-AUG-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "option1">APOLLO</OPTION> <OPTION value = "option2">Big Data</OPTION> <OPTION value = "option3">Cancer Control and Prevention</OPTION> <OPTION value = "option4">Center for the Co-Clinical (CCCT)</OPTION> <OPTION value = "option5">Clinical Genomics</OPTION> <OPTION value = "option6">Immunotherapy</OPTION> <OPTION value = "option7">Institute for Applied Cancer Science</OPTION> <OPTION value = "option8">Institute for Personal Cancer Therapy</OPTION> <OPTION value = "option9">Proteomics</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'references';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','references','References','N',504,null,285026,null,null,null,to_date('14-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'medclDevcsHd';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','medclDevcsHd','<h4>Medical Devices</>','N',515,null,285025,null,null,null,to_date('14-AUG-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'subCmplMntrngHd';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','subCmplMntrngHd','<h4>Subject Compliance Monitoring</>','N',513,null,285024,null,null,null,to_date('14-AUG-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drgMngmntHd';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','drgMngmntHd','<h4>Drug Management</>','N',511,null,285023,null,null,null,to_date('14-AUG-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'concomTheraHd';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','concomTheraHd','<h4>Concomitant Therapy</>','N',509,null,285022,null,null,null,to_date('14-AUG-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'blndinProcdrsHd';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','blndinProcdrsHd','<h4>Blinding of Procedures</>','N',510,null,285021,null,null,null,to_date('15-AUG-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'devMangmntHd';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','devMangmntHd','<h4>Device Management</>','N',512,null,285020,null,null,null,to_date('15-AUG-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'radioisotopeHd';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','radioisotopeHd','<h4>Contrast Agents or Radioisotopes</>','N',514,null,285019,null,null,null,to_date('15-AUG-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'invstgnDevHd';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','invstgnDevHd','<h4>Investigational Devices</>','N',516,null,285018,null,null,null,to_date('15-AUG-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'labCorrStdsHd';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','labCorrStdsHd','<h4>Laboratory Correlative Studies</>','N',518,null,285017,null,null,null,to_date('15-AUG-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'biomrkrStdsHd';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','biomrkrStdsHd','<h4>Biomarker Studies</>','N',517,null,285016,null,null,null,to_date('15-AUG-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'trtmntRegPlanHd';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','trtmntRegPlanHd','<h4>Treatment Regimen/Plan</>','N',508,null,285015,null,null,null,to_date('15-AUG-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ideInfrmtnHd';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','ideInfrmtnHd','<h4>IDE Information</>','N',507,null,285014,null,null,null,to_date('15-AUG-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'indInfrmtnHd';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','indInfrmtnHd','<h4>IND Information</>','N',506,null,285013,null,null,null,to_date('15-AUG-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drgSupMfgr';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','drgSupMfgr','Drug Supplier/Manufacturer','N',526,null,285012,null,null,null,to_date('15-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drgMngntFile';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','drgMngntFile','Do you have a drug management file?','N',525,null,285011,null,null,null,to_date('15-AUG-14','DD-MON-RR'),null,'dropdown','<SELECT id="alternateId" NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'subComplMontr';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','subComplMontr','Subject Compliance Monitoring','N',521,null,285010,null,null,null,to_date('15-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'concomThera';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','concomThera','Concomitant Therapy','N',519,null,285009,null,null,null,to_date('15-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'trtmntDuration';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','trtmntDuration','Treatment duration','N',520,null,285008,null,null,null,to_date('15-AUG-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'radstpAddnCmn';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','radstpAddnCmn','Additional Comments','N',522,null,285007,null,null,null,to_date('15-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'commIncCritra';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','commIncCritra','Inclusion Criteria (institutional required options)','N',523,null,285006,null,null,null,to_date('15-AUG-14','DD-MON-RR'),null,'checkbox','{chkArray:[ {data: "option1", display:"Age >/= 18 years old"}, {data: "option2", display:"All ages"}, {data: "option3", display:"Male"}, {data: "option4", display:"Female"}, {data: "option5", display:"Pregnant women"} ]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'commExcCritra';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','commExcCritra','Exclusion Criteria (institutional required options)','N',524,null,285005,null,null,null,to_date('15-AUG-14','DD-MON-RR'),null,'checkbox','{chkArray:[ {data: "option1", display:"Age >/= 18 years old"}, {data: "option2", display:"Male"}, {data: "option3", display:"Female"}, {data: "option4", display:"Pregnant women"} ]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'receiptDrgSup';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','receiptDrgSup','Receipt of Drug Supplies','N',527,null,285004,null,null,null,to_date('15-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drgStorage';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','drgStorage','Storage','N',529,null,285003,null,null,null,to_date('15-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'retDestStdDrg';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','retDestStdDrg','Return/Destruction of Study Drug','N',531,null,285002,null,null,null,to_date('15-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'stdyDrgDispn';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','stdyDrgDispn','Dispensing of Study Drug','N',530,null,285001,null,null,null,to_date('19-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'devMngntFile';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','devMngntFile','Do you have a device management file?','N',532,null,285000,null,null,null,to_date('19-AUG-14','DD-MON-RR'),null,'dropdown','<SELECT id="alternateId" NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'coopGrpNum';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','coopGrpNum','Cooperative Group Number','N',560,null,285927,null,null,null,to_date('22-SEP-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'consrtmNum';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','consrtmNum','Consortium Number','N',561,null,285928,null,null,null,to_date('22-SEP-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nciEtctnNum';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','nciEtctnNum','NCI ETCTN Number','N',562,null,285931,null,null,null,to_date('22-SEP-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'prepAdmDrgPkg';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','prepAdmDrgPkg','Preparation and Administration of Drug and Packaging','N',528,null,284999,null,null,null,to_date('26-AUG-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'devSupMfgr';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','devSupMfgr','Device Supplier/Manufacturer','N',533,null,284998,null,null,null,to_date('02-SEP-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'devStorage';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','devStorage','Storage of Device','N',535,null,284997,null,null,null,to_date('02-SEP-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'receiptDevice';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','receiptDevice','Receipt of Device','N',534,null,284996,null,null,null,to_date('02-SEP-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'returnDevice';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','returnDevice','Return of Device','N',536,null,284995,null,null,null,to_date('02-SEP-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'labCorrStdies';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','labCorrStdies','Laboratory Correlative Studies','N',537,null,284994,null,null,null,to_date('02-SEP-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'payProratd';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','payProratd','Will payments be prorated?','N',539,null,284993,null,null,null,to_date('04-SEP-14','DD-MON-RR'),null,'dropdown','<SELECT id="alternateId" NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'subCompReimb';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','subCompReimb','Will subjects receive compensation or reimbursements?','N',538,null,284992,null,null,null,to_date('04-SEP-14','DD-MON-RR'),null,'dropdown','<SELECT id="alternateId" NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'descrbProRatd';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','descrbProRatd','Describe','N',540,null,284991,null,null,null,to_date('04-SEP-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'othPrRevFnd';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','othPrRevFnd','Other peer-reviewed funding','N',541,null,284990,null,null,null,to_date('09-SEP-14','DD-MON-RR'),null,'dropdown','<SELECT id="alternateId" NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "option1">Industry</OPTION> <OPTION value = "option2">Departmental Funds</OPTION> <OPTION value = "option3">Donor Funds</OPTION> <OPTION value = "option4">Unfunded</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'othrNumbrs';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','othrNumbrs','<h4>Other Numbers</h4>','N',543,null,284989,null,null,null,to_date('09-SEP-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'siteNumber';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','siteNumber','Site Number','N',544,null,284988,null,null,null,to_date('09-SEP-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ps_cfs';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','ps_cfs','PS CFS (Resource One)','N',545,null,284987,null,null,null,to_date('09-SEP-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fredNum';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','fredNum','FReD#','N',546,null,284986,null,null,null,to_date('09-SEP-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'labTestsHead';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','labTestsHead','<h4>Laboratory Tests</h4>','N',547,null,284985,null,null,null,to_date('09-SEP-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'labTstPatMat';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','labTstPatMat','Will laboratory tests be performed on patient materials?','N',548,null,284984,null,null,null,to_date('09-SEP-14','DD-MON-RR'),null,'dropdown','<SELECT id="alternateId" NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'cliaAttch';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','cliaAttch','If Other, provide name of the performing laboratory identification and contact information (attach CLIA certificate)','N',551,null,284983,null,null,null,to_date('09-SEP-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'patEnrEligTrtmt';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','patEnrEligTrtmt','Will these tests be used for diagnosis, prevention, or treatment or health assessment purposes, including biomarker or genomic tests, for determining patient enrollment/eligibility/treatment assignment?','N',549,null,284982,null,null,null,to_date('09-SEP-14','DD-MON-RR'),null,'dropdown','<SELECT id="alternateId" NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'patEnrEliTrtLab';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','patEnrEliTrtLab','Please select the laboratories that will be used for conducting tests for diagnosis, prevention, or treatment or health assessment purposes, including biomarker or genomic tests, for determining patient enrollment/eligibility/treatment assignment','N',550,null,284981,null,null,null,to_date('09-SEP-14','DD-MON-RR'),null,'checkbox','{chkArray:[{data: ''option1'', display:''Division of Pathology & Laboratory Medicine (CLIA Certified Laboratory)''}, {data: ''option2'', display:''Cellular Therapies CLIA Certified Laboratory (Department of Stem Cell Therapy CLIA Certified Laboratory)''}, {data: ''option3'', display:''Other''}]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'namePurTest';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','namePurTest','Provide the name of the test(s), the purpose of the test, and the patient materials required (e.g. archived tissue, slides, and/or blood)','N',552,null,284980,null,null,null,to_date('09-SEP-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'patMatTestOthr';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','patMatTestOthr','Specify Other','N',554,null,284979,null,null,null,to_date('09-SEP-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'patMatTest';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','patMatTest','What type of patient materials will be collected/used for test for diagnosis, prevention or treatment or health assessment purposes, including for determining Patient enrollment/eligibility/treatment assignment?','N',553,null,284978,null,null,null,to_date('09-SEP-14','DD-MON-RR'),null,'checkbox','{chkArray:[{data: ''option1'', display:''Archived pathology material (e.g. tissue sections on unstained slides)''}, {data: ''option2'', display:''Fresh Tissue in media''}, {data: ''option3'', display:''Fresh Frozen Tissue''}, {data: ''option4'', display:''Serum/Blood''}, {data: ''option5'', display:''Other body fluids''}]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'patMatResLab';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','patMatResLab','Will patient materials (blood, tissue sections on unstained slides, slides) need to be sent to a research (non-CLIA certified) laboratory?','N',555,null,284977,null,null,null,to_date('09-SEP-14','DD-MON-RR'),null,'dropdown','<SELECT id="alternateId" NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'patMatResLabY2';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','patMatResLabY2','Is this a research non-CLIA laboratory outside of MD Anderson?','N',557,null,284976,null,null,null,to_date('09-SEP-14','DD-MON-RR'),null,'dropdown','<SELECT id="alternateId" NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'patMatResLabY1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','patMatResLabY1','Provide the laboratory name and address that specimens will be sent to','N',556,null,284975,null,null,null,to_date('09-SEP-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'patMatResLabY3';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','patMatResLabY3','What type of patient materials need to be sent to a research (non-CLIA certified) laboratory?','N',558,null,284974,null,null,null,to_date('09-SEP-14','DD-MON-RR'),null,'checkbox','{chkArray:[{data: ''option1'', display:''Archived pathology material (e.g. tissue sections on unstained slides)''}, {data: ''option2'', display:''Fresh Tissue''}, {data: ''option3'', display:''Serum/Blood''}, {data: ''option4'', display:''Other body fluids''}]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nonCliaOthr';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','nonCliaOthr','Specify Other','N',559,null,284973,null,null,null,to_date('09-SEP-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'offStdyCri';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','offStdyCri','Off Study Criteria','N',null,null,288034,null,null,null,to_date('06-OCT-14','DD-MON-RR'),null,'checkbox','{chkArray:[ {data: "option1", display:"Death"}, {data: "option2", display:"Disease Progression"}, {data: "option3", display:"Disease Relapse"}, {data: "option4", display:"Drug Resistant"}, {data: "option5", display:"Intercurrent Illness"}, {data: "option6", display:"Lost to Follow-Up"}, {data: "option7", display:"New Malignancy"}, {data: "option8", display:"Not Able to Comply with Study Requirements"}, {data: "option9", display:"Patient Decision to Discontinue Study"}, {data: "option10", display:"Patient Non-Compliance"}, {data: "option11", display:"Patient Withdrawal of Consent"}, {data: "option12", display:"Physician Discretion"}, {data: "option13", display:"Pregnancy"}, {data: "option14", display:"Toxicity"}, {data: "option15", display:"Treatment Completion"}, {data: "option16", display:"Other"} ]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'othrOffStdyCri';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','othrOffStdyCri','Specify other criteria','N',null,null,288035,null,null,null,to_date('06-OCT-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'OtrStdRemCrit';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','OtrStdRemCrit','Specify other criteria','N',null,null,288037,null,null,null,to_date('06-OCT-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'selOthrObjEntry';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','selOthrObjEntry','Select other objectives for entry','N',null,null,288218,null,null,null,to_date('07-OCT-14','DD-MON-RR'),null,'checkbox','{chkArray:[ {data: "option1", display:"Secondary Objective"}, {data: "option2", display:"Exploratory Objective"}, {data: "option3", display:"Biomarker Objective"} ]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'biomrkrObj';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','biomrkrObj','Biomarker Objective','N',null,null,288219,null,null,null,to_date('07-OCT-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'knownBiomrkr';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','knownBiomrkr','Are they known Biomarkers?','N',null,null,288222,null,null,null,to_date('07-OCT-14','DD-MON-RR'),null,'dropdown','<SELECT id="alternateId" NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'biomrkrType';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','biomrkrType','Biomarker type','N',null,null,288223,null,null,null,to_date('07-OCT-14','DD-MON-RR'),null,'dropdown','{codelst_type:"LindBiomrkrType"}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'biomrkrSpeOth';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','biomrkrSpeOth','Specify other','N',null,null,288224,null,null,null,to_date('07-OCT-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'testNameQ';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','testNameQ','What are you calling the test? Use commonly accepted test.','N',null,null,288225,null,null,null,to_date('07-OCT-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ifGeneList';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','ifGeneList','If gene, use www.genecard.org to pull name from list','N',null,null,288226,null,null,null,to_date('07-OCT-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'biomrkrCall';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','biomrkrCall','What are you calling the biomarker?','N',null,null,288269,null,null,null,to_date('07-OCT-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'levelQues';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','levelQues','What level are you trying to look at?','N',null,null,288283,null,null,null,to_date('07-OCT-14','DD-MON-RR'),null,'dropdown','<SELECT id="alternateId" NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "dna">DNA</OPTION> <OPTION value = "rna">RNA</OPTION> <OPTION value = "protein">Protein</OPTION> <OPTION value = "methyl">Methylation</OPTION> <OPTION value = "mrna">mRNA</OPTION> <OPTION value = "snp">SNP</OPTION> <OPTION value = "metabolo">Metabolomics</OPTION> <OPTION value = "other">Other</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'measureQues';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','measureQues','Where are you measuring?','N',null,null,288290,null,null,null,to_date('07-OCT-14','DD-MON-RR'),null,'checkbox','{chkArray:[ {data: "option1", display:"Biopsy- Tumor tissue"}, {data: "option2", display:"Biopsy- Normal tissue"}, {data: "option3", display:"Urine"}, {data: "option4", display:"Blood - Serum"}, {data: "option5", display:"Blood - Plasma"}, {data: "option6", display:"CSF"}, {data: "option7", display:"Pleural effusion"}, {data: "option8", display:"Other"} ]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'timePoints';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','timePoints','Time points','N',null,null,288291,null,null,null,to_date('07-OCT-14','DD-MON-RR'),null,'checkbox','{chkArray:[ {data: "option1", display:"Pretreatment"}, {data: "option2", display:"During treatment"}, {data: "option3", display:"Post treatment"} ]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'methodology';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','methodology','Methodology','N',null,null,288292,null,null,null,to_date('07-OCT-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dsmbCompostn';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','dsmbCompostn','DSMB Composition','N',null,null,288294,null,null,null,to_date('07-OCT-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'bayesianComp';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','bayesianComp','Bayesian component included in protocol','N',null,null,288295,null,null,null,to_date('07-OCT-14','DD-MON-RR'),null,'dropdown','<SELECT id="alternateId" NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'expdBiostat';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','expdBiostat','Are you requesting an expedited biostatistical review for this protocol?','N',null,null,288578,null,null,null,to_date('09-OCT-14','DD-MON-RR'),null,'checkbox','{chkArray:[ {data: "option1", display:"Bypass CRC/PBHSRC Review<br>This study is NCI-sponsored, CTEP-approved, and/or NCI cooperative group-approved trial, or peer-reviwed by an NIH-approved mechanism and IRB-approved at an NCI designated cancer center. Please attach appropriate documentation (e.g. NCI-approval letter or IRB approval memo) to support the request to bypass CRC/PBHSRC review."}, {data: "option2", display:"Expedited Biostatistical Review<br>This study is an industry-sponsored, multi-center clinical trial already activated at an NCI designated cancer center and/or has been approved by the Office of the Vice Provost for Fast-Track designation. Please attach appropriate documentation (e.g. Sponsor memorandum,ClinicalTrial.gov listing verifying activation at an NCI-designated cancer center, OVP Fast Track Approval memorandum) to support the request for expedited biostatistical review."} ]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'authrzdUsr';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','authrzdUsr','Authorized User','N',null,null,288724,null,null,null,to_date('10-OCT-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'authrztnNum';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','authrztnNum','Authorization Number (or state that authorization application is under Radiation Safety Committee Review)','N',null,null,288725,null,null,null,to_date('10-OCT-14','DD-MON-RR'),null,null,'<SELECT id="alternateId" NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'incldAdminstr';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','incldAdminstr','<br>Does this protocol include the administration of a radioactive compound (or drug) to a patient intended to obtain basic information regarding metabolism (including kinetics, distribution, and localization) of the drug or regarding human physiology, pathophysiology, or biochemistry, but not intended for immediate therapeutic, diagnostic, or similar purposes or to determine the safety and effectiveness of the drug in humans for such purposes (i.e. to carry out a clinical study)?','N',null,null,288726,null,null,null,to_date('10-OCT-14','DD-MON-RR'),null,'dropdown','<SELECT id="alternateId" NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'faciltyInvstg';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','faciltyInvstg','Name the facility where the investigational agent is labeled or bound to the radioisotope (include the name of the laboratory at MDACC or the name of the company if outside MDACC)','N',null,null,288727,null,null,null,to_date('10-OCT-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nihRacRev';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','nihRacRev','Does the study require NIH/RAC review?','N',null,null,288728,null,null,null,to_date('10-OCT-14','DD-MON-RR'),null,'dropdown','<SELECT id="alternateId" NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nihRacRevComm';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','nihRacRevComm','Comment','N',null,null,288739,null,null,null,to_date('10-OCT-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'biosafety2Comm';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','biosafety2Comm','Comment','N',null,null,288740,null,null,null,to_date('10-OCT-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'invAntcpAEs';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','invAntcpAEs','If investigational, provide list of anticipated AEs','N',null,null,288753,null,null,null,to_date('10-OCT-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'commDrugInsrt';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','commDrugInsrt','If commercially available, include drug insert','N',null,null,288755,null,null,null,to_date('10-OCT-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nom_rev';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','nom_rev','Nominated Reviewer','N',1900,null,303327,null,null,null,to_date('03-NOV-14','DD-MON-RR'),null,'lookup','{lookupPK:6000, selection:"multi", mapping:[{source:"USRFULLNAME", target:"alternateId"},{source:"USRLOGIN", target:"alternateId16403"}]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nom_alt_rev';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','nom_alt_rev','Nominated Reviewer (alternate)','N',1902,null,303329,null,null,null,to_date('03-NOV-14','DD-MON-RR'),null,'hidden-input','{lookupPK:6000, selection:"single", mapping:[{source:"USRFULLNAME", target:"alternateId"},{source:"USRLOGIN", target:"alternateId16404"}]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'prot_ready';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','prot_ready','Protocol ready for pre-reviews and signoffs','N',null,null,303330,null,null,null,to_date('03-NOV-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'prot_req_ind';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','prot_req_ind','Does this protocol require an IND?','N',null,null,303438,null,null,null,to_date('03-NOV-14','DD-MON-RR'),null,'dropdown','<SELECT id="alternateId" NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'meet_exem_crit';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','meet_exem_crit','Confirm that the protocol <br>meets all criteria for exemption according to 21CFR312.2(b) <br>noted here?','N',null,null,303439,null,null,null,to_date('03-NOV-14','DD-MON-RR'),null,'checkbox','{chkArray:[{data: "option1", display:"<br>(b) Exemptions (1) The clinical investigation of a drug product that is lawfully marketed in the <br>United States is exempt from the requirements of <br>this part if all of the following apply:<br>(i) The investigation is not intended to be reported to FDA as a well-controlled study in support of a new indication for use not intended to be used to suport any other significant change in the labeling for this drug<br>(ii) If the drug that is undergoing investigation is lawfully marketed as a prescription drug product, the investigation is not intended to support a significant change in the advertising for the product<br>(iii) The investigation does not involve a route of administration of dosage level or use in a patient population or other factor that significantly increases the risks (or decreases the acceptability of the risks) associated with the use of the drug product<br>(iv) The investigation is conducted in compliance with the requirements for institutional review set forth in part 56 and with the requirements for informed consent set forth in part 50; and<br>(v) The investigation is conducted in compliance with the requirements of 312.7<br>at an NCI designated cancer center."}]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'exem_ratnl';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','exem_ratnl','Rationale for exemption','N',null,null,303443,null,null,null,to_date('03-NOV-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nom_rev_id';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','nom_rev_id','Nominated Reviewer User ID','Y',1901,null,305170,null,null,null,to_date('06-NOV-14','DD-MON-RR'),null,'hidden-input',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nom_alt_rev_id';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','nom_alt_rev_id','Nominated Reviewer (alternate) User ID','Y',1903,null,305171,null,null,null,to_date('06-NOV-14','DD-MON-RR'),null,'hidden-input',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugAgent1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','drugAgent1','Agent-1','N',null,null,314691,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'dropdown','{codelst_type:"LindDrugAgent"}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugMnftr1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','drugMnftr1','Manufacturer','N',null,null,314692,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'lookup','{lookupPK:100040, selection:"single", mapping:[{source:"Sponsor_Name", target:"alternateId"}]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugAgent2';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','drugAgent2','Agent-2','N',null,null,314693,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'dropdown','{codelst_type:"LindDrugAgent"}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dose2';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','dose2','Dose','N',null,null,314694,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseRatnl2';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','doseRatnl2','Dose Rationale','N',null,null,314695,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'routeAdmn2';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','routeAdmn2','Route of Administration','N',null,null,314696,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'dropdown','{codelst_type:"LindRouteAdmin"}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugMnftr2';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','drugMnftr2','Manufacturer','N',null,null,314697,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'lookup','{lookupPK:100040, selection:"single", mapping:[{source:"Sponsor_Name", target:"alternateId"}]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugAgent3';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','drugAgent3','Agent-3','N',null,null,314698,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'dropdown','{codelst_type:"LindDrugAgent"}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dose3';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','dose3','Dose','N',null,null,314699,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseRatnl3';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','doseRatnl3','Dose Rationale','N',null,null,314700,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'routeAdmn3';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','routeAdmn3','Route of Administration','N',null,null,314701,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'dropdown','{codelst_type:"LindRouteAdmin"}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugMnftr3';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','drugMnftr3','Manufacturer','N',null,null,314702,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'lookup','{lookupPK:100040, selection:"single", mapping:[{source:"Sponsor_Name", target:"alternateId"}]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugAgent4';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','drugAgent4','Agent-4','N',null,null,314703,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'dropdown','{codelst_type:"LindDrugAgent"}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dose4';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','dose4','Dose','N',null,null,314704,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseRatnl4';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','doseRatnl4','Dose Rationale','N',null,null,314705,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'routeAdmn4';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','routeAdmn4','Route of Administration','N',null,null,314706,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'dropdown','{codelst_type:"LindRouteAdmin"}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugMnftr4';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','drugMnftr4','Manufacturer','N',null,null,314707,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'lookup','{lookupPK:100040, selection:"single", mapping:[{source:"Sponsor_Name", target:"alternateId"}]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugAgent5';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','drugAgent5','Agent-5','N',null,null,314708,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'dropdown','{codelst_type:"LindDrugAgent"}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dose5';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','dose5','Dose','N',null,null,314709,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseRatnl5';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','doseRatnl5','Dose Rationale','N',null,null,314710,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'routeAdmn5';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','routeAdmn5','Route of Administration','N',null,null,314711,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'dropdown','{codelst_type:"LindRouteAdmin"}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugMnftr5';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','drugMnftr5','Manufacturer','N',null,null,314712,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'lookup','{lookupPK:100040, selection:"single", mapping:[{source:"Sponsor_Name", target:"alternateId"}]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugAgent6';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','drugAgent6','Agent-6','N',null,null,314713,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'dropdown','{codelst_type:"LindDrugAgent"}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dose6';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','dose6','Dose','N',null,null,314714,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseRatnl6';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','doseRatnl6','Dose Rationale','N',null,null,314715,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'routeAdmn6';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','routeAdmn6','Route of Administration','N',null,null,314716,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'dropdown','{codelst_type:"LindRouteAdmin"}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugMnftr6';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','drugMnftr6','Manufacturer','N',null,null,314717,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'lookup','{lookupPK:100040, selection:"single", mapping:[{source:"Sponsor_Name", target:"alternateId"}]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugAgent7';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','drugAgent7','Agent-7','N',null,null,314718,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'dropdown','{codelst_type:"LindDrugAgent"}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dose7';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','dose7','Dose','N',null,null,314719,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseRatnl7';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','doseRatnl7','Dose Rationale','N',null,null,314720,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'routeAdmn7';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','routeAdmn7','Route of Administration','N',null,null,314721,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'dropdown','{codelst_type:"LindRouteAdmin"}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugMnftr7';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','drugMnftr7','Manufacturer','N',null,null,314722,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'lookup','{lookupPK:100040, selection:"single", mapping:[{source:"Sponsor_Name", target:"alternateId"}]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugAgent8';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','drugAgent8','Agent-8','N',null,null,314723,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'dropdown','{codelst_type:"LindDrugAgent"}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dose8';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','dose8','Dose','N',null,null,314724,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseRatnl8';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','doseRatnl8','Dose Rationale','N',null,null,314725,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'routeAdmn8';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','routeAdmn8','Route of Administration','N',null,null,314726,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'dropdown','{codelst_type:"LindRouteAdmin"}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugMnftr8';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','drugMnftr8','Manufacturer','N',null,null,314727,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'lookup','{lookupPK:100040, selection:"single", mapping:[{source:"Sponsor_Name", target:"alternateId"}]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugAgent9';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','drugAgent9','Agent-9','N',null,null,314728,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'dropdown','{codelst_type:"LindDrugAgent"}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dose9';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','dose9','Dose','N',null,null,314729,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseRatnl9';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','doseRatnl9','Dose Rationale','N',null,null,314730,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'routeAdmn9';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','routeAdmn9','Route of Administration','N',null,null,314731,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'dropdown','{codelst_type:"LindRouteAdmin"}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugMnftr9';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','drugMnftr9','Manufacturer','N',null,null,314732,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'lookup','{lookupPK:100040, selection:"single", mapping:[{source:"Sponsor_Name", target:"alternateId"}]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugAgent10';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','drugAgent10','Agent-10','N',null,null,314733,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'dropdown','{codelst_type:"LindDrugAgent"}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dose10';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','dose10','Dose','N',null,null,314734,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseRatnl10';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','doseRatnl10','Dose Rationale','N',null,null,314735,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'routeAdmn10';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','routeAdmn10','Route of Administration','N',null,null,314736,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'dropdown','{codelst_type:"LindRouteAdmin"}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugMnftr10';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','drugMnftr10','Manufacturer','N',null,null,314737,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'lookup','{lookupPK:100040, selection:"single", mapping:[{source:"Sponsor_Name", target:"alternateId"}]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'deviceAgent1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','deviceAgent1','Device-1','N',null,null,314738,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'dropdown','{codelst_type:"LindDeviceAgent"}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'riskAssmt1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','riskAssmt1','Risk Assessment Form 1','N',null,null,314739,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'deviceAgent2';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','deviceAgent2','Device-2','N',null,null,314740,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'dropdown','{codelst_type:"LindDeviceAgent"}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'riskAssmt2';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','riskAssmt2','Risk Assessment Form 2','N',null,null,314741,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'deviceAgent3';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','deviceAgent3','Device-3','N',null,null,314742,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'dropdown','{codelst_type:"LindDeviceAgent"}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'riskAssmt3';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','riskAssmt3','Risk Assessment Form 3','N',null,null,314743,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'deviceAgent4';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','deviceAgent4','Device-4','N',null,null,314744,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'dropdown','{codelst_type:"LindDeviceAgent"}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'riskAssmt4';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','riskAssmt4','Risk Assessment Form 4','N',null,null,314745,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'deviceAgent5';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','deviceAgent5','Device-5','N',null,null,314746,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'dropdown','{codelst_type:"LindDeviceAgent"}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'riskAssmt5';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','riskAssmt5','Risk Assessment Form 5','N',null,null,314747,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'devMnftr1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','devMnftr1','Manufacturer','N',null,null,314765,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'lookup','{lookupPK:100040, selection:"single", mapping:[{source:"Sponsor_Name", target:"alternateId"}]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'devMnftr2';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','devMnftr2','Manufacturer','N',null,null,314766,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'lookup','{lookupPK:100040, selection:"single", mapping:[{source:"Sponsor_Name", target:"alternateId"}]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'devMnftr3';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','devMnftr3','Manufacturer','N',null,null,314767,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'lookup','{lookupPK:100040, selection:"single", mapping:[{source:"Sponsor_Name", target:"alternateId"}]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'devMnftr4';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','devMnftr4','Manufacturer','N',null,null,314768,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'lookup','{lookupPK:100040, selection:"single", mapping:[{source:"Sponsor_Name", target:"alternateId"}]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'devMnftr5';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','devMnftr5','Manufacturer','N',null,null,314769,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'lookup','{lookupPK:100040, selection:"single", mapping:[{source:"Sponsor_Name", target:"alternateId"}]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'sponsorName1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','sponsorName1','Sponsor Name 1','N',null,null,314773,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'lookup','{lookupPK:100040, selection:"single", mapping:[{source:"Sponsor_Name", target:"alternateId"}]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundingType1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','fundingType1','Funding Type','N',null,null,314774,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'dropdown','{codelst_type:"LindFundingType"}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'sponsorName2';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','sponsorName2','Sponsor Name 2','N',null,null,314775,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'lookup','{lookupPK:100040, selection:"single", mapping:[{source:"Sponsor_Name", target:"alternateId"}]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundingType2';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','fundingType2','Funding Type','N',null,null,314776,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'dropdown','{codelst_type:"LindFundingType"}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'sponsorName3';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','sponsorName3','Sponsor Name 3','N',null,null,314777,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'lookup','{lookupPK:100040, selection:"single", mapping:[{source:"Sponsor_Name", target:"alternateId"}]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundingType3';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','fundingType3','Funding Type','N',null,null,314778,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'dropdown','{codelst_type:"LindFundingType"}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'sponsorName4';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','sponsorName4','Sponsor Name 4','N',null,null,314779,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'lookup','{lookupPK:100040, selection:"single", mapping:[{source:"Sponsor_Name", target:"alternateId"}]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundingType4';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','fundingType4','Funding Type','N',null,null,314780,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'dropdown','{codelst_type:"LindFundingType"}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'sponsorName5';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','sponsorName5','Sponsor Name 5','N',null,null,314781,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'lookup','{lookupPK:100040, selection:"single", mapping:[{source:"Sponsor_Name", target:"alternateId"}]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundingType5';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','fundingType5','Funding Type','N',null,null,314782,null,null,null,to_date('01-DEC-14','DD-MON-RR'),null,'dropdown','{codelst_type:"LindFundingType"}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'incluCriteria';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','incluCriteria','Inclusion Criteria','N',null,null,320484,null,null,null,to_date('10-DEC-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'incluCriteriaNa';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','incluCriteriaNa','Inclusion Criteria N/A','N',null,null,320485,null,null,null,to_date('10-DEC-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'excluCriteria';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','excluCriteria','Exclusion Criteria','N',null,null,320486,null,null,null,to_date('10-DEC-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'excluCriteriaNa';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','excluCriteriaNa','Exclusion Criteria N/A','N',null,null,320487,null,null,null,to_date('10-DEC-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dept_chairs';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','dept_chairs','Department Chair','N',1,null,321083,null,null,null,to_date('11-DEC-14','DD-MON-RR'),null,'lookup','{lookupPK:6000, selection:"multi", mapping:[{source:"USRFULLNAME", target:"alternateId"},{source:"USRLOGIN", target:"alternateId16804"}]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dept_chairs_ids';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','dept_chairs_ids','Department Chair User ID','Y',1,null,321084,null,null,null,to_date('11-DEC-14','DD-MON-RR'),null,'hidden-input',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'critAdvEvents';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','critAdvEvents','What criteria are you using for classifying Adverse Events?','N',null,null,330432,null,null,null,to_date('23-DEC-14','DD-MON-RR'),null,'dropdown','<SELECT id="alternateId" NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "CTC2.0">CTC2.0</OPTION> <OPTION value = "CTCAE3.0">CTCAE3.0</OPTION> <OPTION value = "CTCAE3.M10">CTCAE3.M10</OPTION> <OPTION value = "CTCAE4.03">CTCAE4.03</OPTION> <OPTION value = "Other">Other</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'critAdvEvntsOth';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','critAdvEvntsOth','Other','N',null,null,330433,null,null,null,to_date('23-DEC-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'freshBiopsyNeed';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','freshBiopsyNeed','Are fresh biopsies needed?','N',null,null,330663,null,null,null,to_date('24-DEC-14','DD-MON-RR'),null,'dropdown','<SELECT id="alternateId" NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'freshBiopsyYes';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','freshBiopsyYes','If yes, please describe','N',null,null,330664,null,null,null,to_date('24-DEC-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundProvChk1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','fundProvChk1','Funding Provided','N',null,null,330924,null,null,null,to_date('28-DEC-14','DD-MON-RR'),null,'checkbox','{chkArray:[ {data: "option1", display:"Personnel"}, {data: "option2", display:"Patient Care"}, {data: "option3", display:"Startup Costs"}, {data: "option4", display:"Study Agent / Device"}, {data: "option5", display:"Participant Remunerations"}, {data: "option6", display:"Ancillary Services"}, ]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundProvChk2';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','fundProvChk2','Funding Provided','N',null,null,330925,null,null,null,to_date('28-DEC-14','DD-MON-RR'),null,'checkbox','{chkArray:[ {data: "option1", display:"Personnel"}, {data: "option2", display:"Patient Care"}, {data: "option3", display:"Startup Costs"}, {data: "option4", display:"Study Agent / Device"}, {data: "option5", display:"Participant Remunerations"}, {data: "option6", display:"Ancillary Services"}, ]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundProvChk3';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','fundProvChk3','Funding Provided','N',null,null,330926,null,null,null,to_date('28-DEC-14','DD-MON-RR'),null,'checkbox','{chkArray:[ {data: "option1", display:"Personnel"}, {data: "option2", display:"Patient Care"}, {data: "option3", display:"Startup Costs"}, {data: "option4", display:"Study Agent / Device"}, {data: "option5", display:"Participant Remunerations"}, {data: "option6", display:"Ancillary Services"}, ]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundProvChk4';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','fundProvChk4','Funding Provided','N',null,null,330927,null,null,null,to_date('28-DEC-14','DD-MON-RR'),null,'checkbox','{chkArray:[ {data: "option1", display:"Personnel"}, {data: "option2", display:"Patient Care"}, {data: "option3", display:"Startup Costs"}, {data: "option4", display:"Study Agent / Device"}, {data: "option5", display:"Participant Remunerations"}, {data: "option6", display:"Ancillary Services"}, ]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundProvChk5';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','fundProvChk5','Funding Provided','N',null,null,330928,null,null,null,to_date('28-DEC-14','DD-MON-RR'),null,'checkbox','{chkArray:[ {data: "option1", display:"Personnel"}, {data: "option2", display:"Patient Care"}, {data: "option3", display:"Startup Costs"}, {data: "option4", display:"Study Agent / Device"}, {data: "option5", display:"Participant Remunerations"}, {data: "option6", display:"Ancillary Services"}, ]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyAdmDiv';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','studyAdmDiv','Administrative Division','N',1,null,337302,null,null,null,to_date('24-JAN-15','DD-MON-RR'),null,'dropdown',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dataPoints1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','dataPoints1','What types of data points will be collected, received or recorded?','N',101,null,284955,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'checkbox','{chkArray:[{data: ''option1'', display:''Names or initials''},{data: ''option2'', display:''ALL geographic subdivisions smaller such as city, county, neighborhood''},{data: ''option3'', display:''All elements of dates smaller than a year (e.g., birth date, admission, discharge, death, etc.)''},{data: ''option4'', display:''Phone numbers''},{data: ''option5'', display:''Fax numbers''},{data: ''option6'', display:''E-mail addresses''},{data: ''option7'', display:''Social security numbers''},{data: ''option8'', display:''Medical record Number''},{data: ''option9'', display:''Health plan beneficiary number or code''},{data: ''option10'', display:''Any other account numbers''},{data: ''option11'', display:''Certificate/license numbers''},{data: ''option12'', display:''Vehicle identifiers''},{data: ''option13'', display:''Device identification numbers''},{data: ''option14'', display:''Web URL�s''},{data: ''option15'', display:''Internet IP address numbers''},{data: ''option16'', display:''Biometric identifiers (e.g., fingerprints, voice prints, retina scans, etc.)''},{data: ''option17'', display:''Full face photographs or comparable images''},{data: ''option18'', display:''Any other unique number, characteristic or code, such as employee ID number or student ID number''},{data: ''option19'', display:''None of the Above''}]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'describeText';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','describeText','Please describe: ','N',102,null,284954,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'input',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'describeRecord';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','describeRecord','Describe how data will be recorded','N',103,null,284953,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'inclusion';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','inclusion','Inclusion Criteria (other)','N',105,null,284952,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'lookup','{lookupPK:2, selection:"single", mapping:[{source:"Short Name", target:"alternateId"}]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'exclusion';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','exclusion','Exclusion Criteria (other)','N',106,null,284951,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'lookup','{lookupPK:2, selection:"single", mapping:[{source:"Short Name", target:"alternateId"}]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'juvinileElig';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','juvinileElig','Pediatrics','N',107,null,284950,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT id="alternateId" NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT> ',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'elderElig';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','elderElig','Elderly','N',108,null,284949,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'pregEligiblity';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','pregEligiblity','Pregnant Women','N',109,null,284948,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT id="alternateId"  NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'prisonorElig';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','prisonorElig','Prisoners','N',110,null,284947,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dose';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','dose','Dose','N',113,null,284946,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'EKG';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','EKG','Will this trial require EKGs in which the sponsor will be providing an EKG machine? ','N',120,null,284945,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'lab';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','lab','Are there any send out labs to a Central Laboratory?','N',119,null,284944,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'cycles';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','cycles','How many cycles will be covered in the Coverage Analysis?','N',118,null,284943,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'input',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'requirements';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','requirements','<i>Requirements for Medical Coverage of Routine Costs</>','N',117,null,284942,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nonDrugStudy';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','nonDrugStudy','Non-Drug Study','N',115,null,284941,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'route';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','route','Route of Administration','N',114,null,284940,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','{codelst_type:"LindRouteAdmin"}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'reimbursement';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','reimbursement','Will this trial require the sponsor to reimburse MDACC for commercially available drugs used as investigational agents?','N',121,null,284939,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'blndnUnbln';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','blndnUnbln','Blinding/Unblinding','N',125,null,284938,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'independentSOC';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','independentSOC','If applicable, will research personnel draw all Non - Standard of Care (SOC) labs associated with the trial that are collected independently of SOC lab work?','N',124,null,284937,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'radiology';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','radiology','Will this trial require the use of research- related items or services in Interventional Radiology?','N',123,null,284936,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'diagImaging';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','diagImaging','Will this trial require the use of research- related items or services in Diagnostic Imaging?','N',122,null,284935,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'biosafety1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','biosafety1','Does the study involve the use of Recombinant DNA technology?','N',51,null,284934,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'biosafety2';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','biosafety2','Does this study involve the use of organisms that are infectious to humans?','N',52,null,284933,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'biosafety3';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','biosafety3','Does the study involve human/animal tissue other than blood derived hematopoietic stem cells?','N',53,null,284932,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'cmpnsation1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','cmpnsation1','Describe','N',55,null,284931,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'confdlty1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','confdlty1','Will there be a link to identify subjects?','N',57,null,284930,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT id="alternateId" NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'confdlty2';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','confdlty2','Describe how you will protect the subject''s identity.','N',58,null,284929,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'confdlty3';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','confdlty3','Describe provisions taken to maintain confidentiality of specimens/data.','N',59,null,284928,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'confdlty4';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','confdlty4','Describe the security plan for data including where data will be stored, how long the data will be stored and what will be don''t with the data once the study is terminated.','N',60,null,284927,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'confsecprod';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','confsecprod','Please confirm that these data will be collected, managed, and distributed in accordance with the M.D. Anderson Confidentiality Policy (ADM0264).','N',61,null,284926,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'confsecprod1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','confsecprod1','If No,  Please Describe:','N',62,null,284925,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'confsecprod2';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','confsecprod2','Select all data storage procedures/mechanisms that apply.','N',63,null,284924,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'checkbox','{chkArray:[{data: ''option1'', display:''Paper records''},{data: ''option2'', display:''PC/Laptop/etc.''},{data: ''option3'', display:''External Storage (CD/Disk/USB Drive/etc)''},{data: ''option4'', display:''Server''}]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'confsecprod3';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','confsecprod3','Select all data storage procedures/mechanisms that apply.','N',64,null,284923,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'checkbox','{chkArray:[{data: ''option1'', display:''Secured by locked file cabinet''},{data: ''option2'', display:''Secured by a locked office or other secured area.''},{data: ''option3'', display:''Secured by password protection''}]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'confsecprod4';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','confsecprod4','List other data storage types and procedures/mechanisms:','N',65,null,284922,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundSrc';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','fundSrc','Type of Support - Industry Funding, Grant, Other','N',152,null,284921,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'spnsrName';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','spnsrName','Name of Sponsor, Supporter or Granting Agency:','N',154,null,284920,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'finSupp';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','finSupp','How is the proposed study to be financially supported?','N',156,null,284919,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT id="alternateId" NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'spnsrshp';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','spnsrshp','Type(s) of support','N',158,null,284918,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'checkbox','{chkArray:[{data: ''option1'', display:''Option 1''},{data: ''option2'', display:''Option 2''},{data: ''option3'', display:''Option 3''}]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'spnsrshp1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','spnsrshp1','Indicate if the sponsor/supporter/granting agency will receive data','N',160,null,284917,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT id="alternateId" NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'finSupp1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','finSupp1','Type of funding: ','N',162,null,284916,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT id="alternateId" NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "option1">NCI</OPTION> <OPTION value = "option2">NIH (other than NCI)</OPTION> <OPTION value = "option3">DOD</OPTION> <OPTION value = "option4">Other peer-reviewed funding (e.g. NSF, ACS, etc.)</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'finSupp2';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','finSupp2','Source of Agent/Device:','N',164,null,284915,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'input',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'finSupp3';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','finSupp3','List any device(s) which will be used (if applicable):  ','N',166,null,284914,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'input',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'grant';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','grant','COEUS Number','N',168,null,284913,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'input',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'homeCare1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','homeCare1','Specify what, if any, treatment may be given at home.','N',172,null,284912,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'input',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'intellProp1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','intellProp1','Intellectual Property','N',176,null,284911,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'intellProp2';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','intellProp2','(If Yes) You may need to submit an Invention Disclosure Report to the Office of Technology Commercialization at 713-745-9602 or visit the Intranet site','N',178,null,284910,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'intellProp3';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','intellProp3','1. Does this study include any agents, devices, or radioactive compound (or drug) manufactured at MD Anderson Cancer Center or by a contract manufacturer? ','N',180,null,284909,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'intellProp4';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','intellProp4','(If Yes) 2. Is the technology licensed by MD Anderson Cancer Center? ','N',182,null,284908,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'intellProp5';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','intellProp5','(If Yes) 4. Where is "drug" being manufactured?','N',184,null,284907,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'input',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ide1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','ide1','Intended as an implant? ','N',188,null,284906,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ide2';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','ide2','Purported or represented to be for use supporting or sustaining human life? ','N',190,null,284905,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ide3';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','ide3','For use of substantial importance in diagnosing, curing, mitigating, or treating disease, or otherwise preventing impairment of human health? ','N',192,null,284904,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ide4';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','ide4','Has the device been modified in a manner not approved by the FDA?','N',194,null,284903,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> <OPTION value = "NA">N/A</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ide5';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','ide5','Who will be responsible for the costs associated with the use of the device?','N',196,null,284902,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "PrtcpntOrThrdPrtyProv">Participant/Third Party Provider</OPTION> <OPTION value = "Spnsr">Sponsor</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'break';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','break','<br>','N',197,null,284901,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nci';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','nci','Is this an NCI-Cancer Therapy Evaluation Protocol (CTEP)? ','N',198,null,284900,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nci1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','nci1','Is this an NCI-Division of Cancer Prevention Protocol (DCP)? ','N',200,null,284899,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'mdacc';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','mdacc','Is this MDACC Study Initiated?','N',202,null,284898,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'labtest';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','labtest','Is there any biomarker testing in this study being used to determine patient/participant eligibility, treatment assignment, or management of patient/participant care?','N',204,null,284897,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> <OPTION value = "NA">Not applicable for this protocol</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'stayDurationn';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','stayDurationn','Length of Stay','N',206,null,284896,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'input',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'mfg1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','mfg1','Will you manufacture in full or in part (split manufacturing) a drug or biological product at the MD Anderson Cancer Center for the proposed clinical study?','N',209,null,284895,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'mta1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','mta1','Materials Transfer Agreement (select one)','N',211,null,284894,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT id="alternateId" NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'mta2';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','mta2','An MTA is a contract that governs the transfer of tangible research materials such as biological materials (e.g., reagents, cell lines, plasmids, and vectors).  MTAs may also be used for chemical compounds and even some types of software. Contact RESEARCH ADMINISTRATION (713-745-6633) for Assistance.','N',212,null,284893,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nseq1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','nseq1','Will non-MDACC approved equipment be used in this study? ','N',222,null,284892,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nseq2';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','nseq2','Yes,Has a plan been devised for training personnel?','N',224,null,284891,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nseq3';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','nseq3','Describe the plan to train personnel.','N',226,null,284890,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nseq4';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','nseq4','Has the Patient Care: Materials, Use, and Evaluation (MUE) Committee approved the non-standard equipment for use?','N',228,null,284889,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'orderSet1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','orderSet1','Will a research protocol require a physician order set?','N',231,null,284888,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'preCli1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','preCli1','Preclinical Drug Development','N',234,null,284887,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'protoMontr1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','protoMontr1','Does this protocol have a planned interim analysis?','N',236,null,284886,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dataSftyMntrPln';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','dataSftyMntrPln','Data Safety and Monitoring Plan','N',237,null,284885,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'protoMontr3';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','protoMontr3','Summary of planned interim analyses including stopping rules for futility, efficacy and/or toxicity','N',238,null,284884,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'protoMontr4';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','protoMontr4','Rationale for no interim analyses','N',239,null,284883,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'radMat1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','radMat1','Does the study involve the administration of radioisotopes or a radioisotope labeled agent?','N',241,null,284882,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> <OPTION value = "NA">N/A</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'resLoc1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','resLoc1','Is MDACC the lead institution? ','N',243,null,284881,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT id="alternateId" NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'resLoc2';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','resLoc2','If MDACC is the lead institution is this a consortium study? ','N',244,null,284880,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT id="alternateId" NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'resloc3';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','resloc3','Are there international sites participating?','N',245,null,284879,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'resPop2';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','resPop2','Does this research include MDACC employees as participants?','N',251,null,284878,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT id="alternateId" NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'resPop3';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','resPop3','Explain if the responses are anonymous, who will be recruiting, and when will recruitment occur','N',252,null,284877,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ST';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','ST','Short Title','N',null,null,284876,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'id_sponsor';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','id_sponsor','Sponsor Protocol Number','N',null,null,284875,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'templateType';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','templateType','Select Template','N',1,null,284874,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "bioMed">Biomedical</OPTION> <OPTION value = "socBehv">Social Behavioral</OPTION> <OPTION value = "bioSpec">Biospecimen/Data Use</OPTION> <OPTION value = "humUse">Compassionate Use/Humanitarian Use</OPTION> <OPTION value = "emergUse">Emergency Use</OPTION> <OPTION value = "nonHsr">Non-HSR</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyLocAddInfo';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','studyLocAddInfo','Name and location for sites where research will be conducted','N',254,null,284873,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'partPop1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','partPop1','Expected Age Range of Participants:  ','N',256,null,284872,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'specimen1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','specimen1','Will this include embryonic stem cells?','N',258,null,284871,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'specimen2';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','specimen2','List the source of specimens/data:  (Select all that apply) (requires at least 1 choice)','N',259,null,284870,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "Yes">option1</OPTION><OPTION value = "No">option2</OPTION><OPTION value = "In Process">option3</OPTION></SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'specimen3';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','specimen3','Institutional Tissue Bank: (Select all that apply)','N',260,null,284869,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "Yes">option1</OPTION><OPTION value = "No">option2</OPTION><OPTION value = "In Process">option3</OPTION></SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'specimen4';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','specimen4','If any of the asterisked (*) choices are selected, please provide name of database or source of specimens/data: ','N',261,null,284868,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'resPop1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','resPop1','Total Expected Number of Prospective Participants (for example, one participant may have 10 samples, 10 participants would have 100 samples):','N',250,null,284867,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'specimen5';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','specimen5','Will specimens/data be shared with an entity, person or organization outside of MD Anderson?','N',262,null,284866,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'specimen6';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','specimen6','If Yes,  Please list each entity, person or organization that the data will be shared with and indicate if a data use agreement or a materials transfer agreement has been implemented.','N',263,null,284865,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'specimen7';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','specimen7','Enter Name of Entity, Person or Organization Rationale for Data Sharing:  (Select All That Apply) (requires at least 1 choice)','N',264,null,284864,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "Yes">option1</OPTION><OPTION value = "No">option2</OPTION><OPTION value = "In Process">option3</OPTION></SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'specimen8';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','specimen8','Data analysis','N',265,null,284863,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'specimen9';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','specimen9','Requirement Procedures','N',266,null,284862,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'specimen10';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','specimen10','Testing','N',267,null,284861,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'specimen11';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','specimen11','Administrative Survey','N',268,null,284860,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'specimen12';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','specimen12','Development of New Product or Project','N',269,null,284859,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'specimen13';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','specimen13','Other: Please describe:','N',270,null,284858,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'specimen14';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','specimen14','Data Use Agreement: (Select One)','N',271,null,284857,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "Yes">Yes</OPTION><OPTION value = "No">No</OPTION><OPTION value = "In Process">In Process</OPTION></SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'specimen15';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','specimen15','A written agreement between a health care component and a person requesting a disclosure of protected health information (PHI) contained in a limited data set. Data use agreements must meet the requirements of limited data set procedure. Contact LEGAL SERVICES (713-745-6633) for Assistance','N',272,null,284856,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'icd1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','icd1','Select process','N',275,null,284855,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "option1">Project will utilize a waiver of informed consent/authorization (e.g. subjects previously signed consent for future use of specimens/data)</OPTION><OPTION value = "option2">Project will utilize the questionnaire statement for surveys (attach survey or questionnaire)</OPTION><OPTION value = "option3">Project staff will obtain verbal consent (e.g. telephone or in person) (attach verbal script or information sheet)</OPTION></SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'icd2';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','icd2','Describe the consent process and the staff involved.','N',276,null,284854,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'icd4';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','icd4','Remember to �Compose? Research Information Document for Exempt Research Long & Short PA will include this choice:','N',278,null,284853,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "option1">Project staff will obtain prospective written consent</OPTION><OPTION value = "option2"> Other</OPTION></SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'icd5';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','icd5','If selected ''Other'', please explain','N',279,null,284852,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundSrc1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','fundSrc1','Does the Study have a Sponsor Supporter or Granting Agency? ','N',281,null,284851,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "Yes">Yes</OPTION><OPTION value = "No">No</OPTION></SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundSrc2';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','fundSrc2','Supporter','N',282,null,284850,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundSrc3';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','fundSrc3','Type of Support','N',283,null,284849,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "option1">Industry Funding</OPTION><OPTION value = "option2">Grant</OPTION><OPTION value = "option3">Other</OPTION></SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'resDesc1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','resDesc1','Does this project involve analysis or collection of data relating to the performance of clinical procedures, interventions, or therapies?','N',286,null,284848,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "option1">Yes</OPTION><OPTION value = "option2">No</OPTION></SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'resDesc2';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','resDesc2','Are any of the clinical procedures, interventions, or therapies being analyzed considered non-standard of care at the MD Anderson main campus? ','N',287,null,284847,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'audtnInspctn';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','audtnInspctn','Auditing and Inspecting','N',292,null,284846,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'accrual1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','accrual1','Estimated number enrolled per month at MDACC','N',294,null,284845,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'accrual2';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','accrual2','Total number enrolled at MDACC','N',295,null,284844,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'accrual5';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','accrual5','Total number screened at MDACC','N',298,null,284843,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'accrual6';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','accrual6','Estimated number screened per month at MDACC','N',299,null,284842,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'accrual7';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','accrual7','Total number screened at all sites','N',300,null,284841,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'accrual8';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','accrual8','Accrual comments','N',301,null,284840,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'accrual9';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','accrual9','Only at other sites, Is this a cooperative study?','N',302,null,284839,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'accrual10';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','accrual10','Cooperative Group Name','N',303,null,284838,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'accrual11';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','accrual11','Principal Investigator for the Cooperative Group','N',304,null,284837,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'accrual12';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','accrual12','Total number enrolled at all sites','N',305,null,284836,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'statCons1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','statCons1','Primary Endpoints','N',307,null,284835,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'statCons2';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','statCons2','Sample size justification','N',308,null,284834,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'statCons3';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','statCons3','Study Design','N',309,null,284833,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'(null)',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'statCons4';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','statCons4','Stopping rules for futility, efficacy, and/or toxicity','N',310,null,284832,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyChair';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','studyChair','Study Chair','N',313,null,284831,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'secObjective';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','secObjective','Secondary Objective','N',314,null,284830,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'stdyRationale';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','stdyRationale','Study Rationale','N',315,null,284829,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'treatmnts';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','treatmnts','Treatment Agents/Devices/Interventions ','N',316,null,284828,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dsmbInt';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','dsmbInt','Data Safety and Monitoring Board','N',317,null,284827,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','{codelst_type:"LindDSMB"}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'resLoc6';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','resLoc6','Is this a cooperative study?','N',245,null,284826,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT id="alternateId" NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'resLoc7';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','resLoc7','Collaborating Site and Institution IRB Approval Letter','N',246,null,284825,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'indReqYes';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','indReqYes','Who is the IND Holder/Regulatory Sponsor?','N',319,null,284824,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'indReq';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','indReq','Does this protocol require an IND?','N',320,null,284823,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ind3';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','ind3','IND Required, please attach the Investigator�s Brochure','N',321,null,284822,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ind4';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','ind4','No IND Required, Select Exemption','N',322,null,284821,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ide6';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','ide6','Does this study utilize an Investigational Device?','N',324,null,284820,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> <OPTION value = "NA">N/A</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ide7';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','ide7','If No or N/A, Comments','N',325,null,284819,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ide8';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','ide8','Yes, Name of Device','N',326,null,284818,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ide9';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','ide9','Yes, Manufacturer','N',327,null,284817,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ide10';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','ide10','Risk Assessment Attachment (from sponsor or manufacturer)','N',328,null,284816,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ide11';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','ide11','Is the study being conducted under an Investigational Device Exemption (IDE)? ','N',329,null,284815,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ide12';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','ide12','Exemption, IDE Holder','N',330,null,284814,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ide13';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','ide13','Exemption,IDE Number','N',331,null,284813,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ide14';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','ide14','Exemption, Please Attach the FDA Approval Letter','N',332,null,284812,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'traineeInfo1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','traineeInfo1','Is this research being conducted as a partial fulfillment for completion of a degree?','N',334,null,284811,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'traineeInfo2';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','traineeInfo2','Please provide the following information for the Thesis/Dissertation Project Committee that is overseeing this research','N',335,null,284810,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'traineeInfo3';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','traineeInfo3','Name of Chair or Advisor','N',336,null,284809,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'traineeInfo4';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','traineeInfo4','Institution Name','N',337,null,284808,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'traineeInfo5';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','traineeInfo5','Address','N',338,null,284807,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'traineeInfo6';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','traineeInfo6','Telephone Number','N',339,null,284806,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'traineeInfo7';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','traineeInfo7','Email Address','N',340,null,284805,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'typeOfStudy';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','typeOfStudy','What is the type of study?','N',341,null,284804,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyCond1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','studyCond1','Role of MDACC Principal Investigator','N',343,null,284803,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'checkbox','{chkArray:[{data: ''option1'', display:'' Major role in writing the protocol''}, {data: ''option2'', display:''Assisted in design of the study''}, {data: ''option3'', display:''Study Principal Investigator (including anticipated first-author)''}, {data: ''option4'', display:''Study Co-Principal Investigator (including anticipated co-author)''}, {data: ''option5'', display:''Participation only (justification for M.D. Anderson participation only as required)''}]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyCond2';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','studyCond2','Administrative Department','N',344,null,284802,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyCond3';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','studyCond3','Who initiated this protocol?','N',345,null,284801,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "MDAACInv">MDACC Investigator</OPTION> <OPTION value = "NCINIH">NCI/NIH</OPTION> <OPTION value = "IndstryOrOthr">Industry/Other</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyCond4';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','studyCond4','Department','N',346,null,284800,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyCond5';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','studyCond5','Phone','N',347,null,284799,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyCond6';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','studyCond6','HR Data- Division','N',348,null,284798,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyCond7';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','studyCond7','Co-Investigators','N',349,null,284797,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyCond8';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','studyCond8','Study Chairperson Email address','N',350,null,284796,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'treatLoc';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','treatLoc','Location of Treatment','N',351,null,284795,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT id="alternateId" NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "inPat">In Patient</OPTION> <OPTION value = "outPat">Out Patient</OPTION> <OPTION value = "inAndOut">In Patient and Out Patient</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'retVisits';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','retVisits','How often must participants come to MDACC?','N',352,null,284794,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'hmeCare';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','hmeCare','Specify what, if any, treatment may be given at home.','N',353,null,284793,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'pubDisp';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','pubDisp','Public Display','N',354,null,284792,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyTeam1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','studyTeam1','Name of Person at MDACC Responsible for Data Management:','N',356,null,284791,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyTeam2';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','studyTeam2','Additional Contact','N',357,null,284790,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyTeam3';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','studyTeam3','Study Team: Roles','N',358,null,284789,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyTeam4';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','studyTeam4','Collaborators','N',359,null,284788,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'finSupp4';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','finSupp4','Which Cancer Center Support Grant (CCSG) - NCI Core Grant Program does this protocol relate to?','N',164,null,284787,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'proDes1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','proDes1','Did you participate in the design of this study?','N',361,null,284786,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "YesComp">Yes -- Completely</OPTION> <OPTION value = "YesPart">Yes -- Partially</OPTION> <OPTION value = "NoSponsrDes">No -- Sponsor Designed</OPTION> <OPTION value = "NoProsStat">No prospective statistical design in this study</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'proDes2';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','proDes2','Did an MDACC Biostatistician participate in the study design?','N',362,null,284785,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','{codelst_type:"LindProDes2"}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ctrc1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','ctrc1','Will the Clinical and Translational Research Center (CTRC) be used in this study?','N',364,null,284784,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'gmpFacility';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','gmpFacility','Will the protocol require the services of the Cell Therapy Lab Good Manufacturing Practice (GMP) facility or products to be manufactured using the GMP facility?','N',365,null,284783,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'labData1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','labData1','Will a UTMDACC investigator perform pharmacologic, molecular or other laboratory studies on patient specimens?','N',367,null,284782,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'labData2';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','labData2','Yes, What type of tissues will be used?','N',368,null,284781,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'labData3';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','labData3','What is the type of study?','N',369,null,284780,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'bioMark1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','bioMark1','Are you measuring Biomarkers?','N',371,null,284779,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'bioMark2';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','bioMark2','Yes, What type of tissues will be used?','N',372,null,284778,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'bioMark3';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','bioMark3','Yes,The marker(s) are being evaluated in (check all that apply):','N',373,null,284777,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "option1">Option1</OPTION><OPTION value = "option2">Option2</OPTION><OPTION value = "option3">Option3</OPTION></SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'bioMark4';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','bioMark4','The marker(s) are being evaluated to predict (check all that apply):','N',374,null,284776,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "option1">Option1</OPTION><OPTION value = "option2">Option2</OPTION><OPTION value = "option3">Option3</OPTION></SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'bioMark5';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','bioMark5','Would there be an opportunity to consider marker evaluation as a component of this study if necessary funding were available?','N',375,null,284775,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'priorProto';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','priorProto','Prior protocol at M. D. Anderson:','N',376,null,284774,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'addnMemoRecpnt';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','addnMemoRecpnt','Additional Memo Recipients','N',377,null,284773,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'(null)',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'unit';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','unit','Unit','N',378,null,284772,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'protoType';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','protoType','Protocol Type','N',379,null,284771,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'protoPhase';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','protoPhase','Protocol Phase','N',380,null,284770,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'versionStatus';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','versionStatus','Version Status','N',381,null,284769,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'bioPharm';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','bioPharm','Is this a Pharma/Biotech sponsored trial that requires research biopsies and/or imaging?','N',382,null,284768,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'specCategories';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','specCategories','Does this research fit into one of the following categories below: 1. Phase 1 first in man trial of novel agents or devices 2. Phase 1 trial of a novel agent or device 3�..','N',383,null,284767,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'discountOffer';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','discountOffer','Your protocol may be eligible for the ReCINT program funding, would you like to apply for the discount rate of 59% to research biopsies and/or scan charges?','N',384,null,284766,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'icd6';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','icd6','Are you requiring subjects to sign a consent form for this protocol?','N',279,null,284765,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'ccsg';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','ccsg','CCSG Study Source','N',244,null,284764,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "ExtPeerRev">Externally Peer-Reviewed</OPTION> <OPTION value = "Industrial">Industrial</OPTION> <OPTION value = "Institutional">Institutional</OPTION> <OPTION value = "National">National</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseModfctn';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','doseModfctn','Dose Modification','N',385,null,284763,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'sysGenCommRev';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','sysGenCommRev','Committee Review Date','N',386,null,284762,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'sysGenIRBApprv';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','sysGenIRBApprv','IRB Approved Date','N',387,null,284761,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'sysGenSubm';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','sysGenSubm','Protocol Submission','N',388,null,284760,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'sysGenSubmBy';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','sysGenSubmBy','Submitted by','N',389,null,284759,null,null,null,to_date('26-JUN-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'medCov1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','medCov1','Is the purpose of the study to evaluate an intervention or service that falls within a Medicare benefit category (e.g., physicians'' service, inpatient hospital services, diagnostic tests, etc.) and that is not excluded from coverage (e.g., cosmetic surgery, hearing aids, etc.)?','N',390,null,284758,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No -- the Study is Not a Qualifying Trial</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'medCov2';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','medCov2','Does the trial have therapeutic intent and is not designed to exclusively test toxicity or disease pathophysiology?','N',391,null,284757,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No -- the Study is Not a Qualifying Trial</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'medCov3';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','medCov3','Does the trial enroll patients with diagnosed disease (excepts include diagnostic clinical trials which may enroll healthy volunteers in control group)?','N',392,null,284756,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No -- the Study is Not a Qualifying Trial</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = '21CFR1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','21CFR1','Do you store your data electronically?','N',393,null,284755,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = '21CFR2';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','21CFR2','Is your data used in IND, IDE or FDA regulated Activity?','N',394,null,284754,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = '21CFR3';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','21CFR3','Is your data stored in Microsoft Excel or Microsoft Access?','N',395,null,284753,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = '21CFR4';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','21CFR4','If Yes, skip questions #4-6 below','N',396,null,284752,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = '21CFR5';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','21CFR5','What is the name of your database? ','N',397,null,284751,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "option1">Option1</OPTION><OPTION value = "option2">Option2</OPTION><OPTION value = "option3">Option3</OPTION></SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = '21CFR6';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','21CFR6','If database name not found in the list please write the name of your database.','N',398,null,284750,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = '21CFR7';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','21CFR7','Name, phone number and email address of your database technical contact.','N',399,null,284749,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'consentForm';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','consentForm','Are you requiring subjects to sign a consent form for this protocol?','N',400,null,284748,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'consentFormType';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','consentFormType','If yes','N',401,null,284747,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,'dropdown','<SELECT NAME="alternateId"><OPTION value = "">Select an option</OPTION><OPTION value = "option1">Clinical Research Category Interventional (INT)</OPTION><OPTION value = "option2">Observational(OBS) Ancillary</OPTION><OPTION value = "option3">Ccorrelative (ANC/COR)</OPTION></SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'protApp1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','protApp1','1. Is the activity a systematic investigation of multiple human subjects, including research testing and evaluation?      Yes= proceed to #2, No=1a question below','N',402,null,284746,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'protApp2';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','protApp2','2. Is the activity designed to test a hypothesis, permit conclusions to be drawn, and thereby to develop or contribute to generalizable knowledge?  Yes= proceed to #3, No =2a question below','N',403,null,284745,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'protApp3';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','protApp3','3. Are all specimen/data collected from subjects who are known to be deceased?  Yes= proceed to #4, No = Proceed to exemption questions','N',404,null,284744,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'protApp4';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','protApp4','4. Will the research involve the collection of analysis of genetic information from specimens/data of deceased subjects?  Yes = proceed to exemption questions, No = question 4a below','N',405,null,284743,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'protApp1a';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','protApp1a','1a. Will the data be listed or described separately for each subject, ie, no aggregate data will be compiled? Yes= case report No= Practice of Medicine Provide summary or description accordingly. ','N',406,null,284742,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'protApp2a';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','protApp2a','2a. Will the data be listed or described separately for each subject, ie, no aggregate data will be compiled? Yes= case report No= Quality Improvement Provide description or summary accordingly. ','N',407,null,284741,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'protApp4a';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','protApp4a','4a. Will the research impact living individuals? Yes or No = Proceed to question No.5 ','N',408,null,284740,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'protApp5';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','protApp5','5. Please describe how you will verify that subjects are deceased?  NOT HUMAN SUBJECTS RESEARCH, Provide a description to verify Not huma subjects research','N',409,null,284739,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'exemp45CFR1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','exemp45CFR1','1. Will the research be conducted in established or commonly accepted educational settings, involving normal education practices?  (This may include schools,  colleges, and other sites where educational activities regularly occur.)   Yes or No= proceed to #2','N',410,null,284738,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'exemp45CFR2';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','exemp45CFR2',' 2. Will the research involve the use of educational tests, survey procedures, or observation of public behavior? Yes=  Question 2a. or No = Proceed to #4','N',411,null,284737,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'exemp45CFR3';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','exemp45CFR3','3. Not applicable to MDACC research (Contact the IRB if you feel this applies) ','N',412,null,284736,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'exemp45CFR4';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','exemp45CFR4','4. Does the research only involve the use or study of existing (already available or on the shelf) data, documents, records, or pathological or diagnostic specimens?   Yes= 4a question below proceed to #5, No = proceed to question 5','N',413,null,284735,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'exemp45CFR5';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','exemp45CFR5','5. Does the research evaluate or examine public benefit or service programs, such as demonstration or dissemination projects? Y/N= proceed to #6','N',414,null,284734,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'exemp45CFR6';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','exemp45CFR6','6. Does the research involve taset and food quality evaluations?  Y/N= proceed with Additional Information.','N',415,null,284733,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'exemp45CFR2a';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','exemp45CFR2a','2a.  Will the research involve survey procedures, interview procedures, or observation of public behavior where the investigatore or research staff partifcipates in the activities being observed?Yes= questions 2b No = proceed to to question #4','N',416,null,284732,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'exemp45CFR2b';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','exemp45CFR2b','2b. Will the information obtained be recorded in such a manner that human subjects can be identified, directly or through identifiers linked to the subjects?   Y/N  = proceed to question 4 ','N',417,null,284731,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'exemp45CFR4a';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','exemp45CFR4a','4a. Are the data, documents, records, or pathological or diagnostic specimens publicly available?   Yes = proceed to question 5 or no= question 4b, then proceed to #5','N',418,null,284730,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'exemp45CFR4b';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','exemp45CFR4b',' 4b.  Will the existing information be recorded in such a manner that human subjects can e identified, directly or through identifiers linked to the subjects? **(see help text for a list of identifiers) Y/N  proceed to question #5','N',419,null,284729,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'protApp6';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','protApp6','If yes, Is the activity designed to test a hypothesis, permit conclusions to be drawn, and thereby to develop or contribute to generalizable knowledge?','N',420,null,284728,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'protApp7';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','protApp7','If no, Will data be listed or described separately  for each subject, i.e., no aggregate data  will be compiled? Yes=Case report No=Practice of Medicine ','N',421,null,284727,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'protApp8';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','protApp8','If yes to 217 Are all specimen/data collected from subjects who are known to be deceased?','N',422,null,284726,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,'checkbox',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'resPop4';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','resPop4','Is there any direct interaction or direct observation of subjects?','N',423,null,284725,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,'dropdown','<SELECT id="alternateId" NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'resPop5';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','resPop5','Describe your direct interaction or direct observation of subjects','N',424,null,284724,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'resPop6';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','resPop6','(from resPop5, proceed to next section) Location Where Research Will be Conducted','N',425,null,284723,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'resPop7';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','resPop7','If No= Skip to Section, Participant Population','N',426,null,284722,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,'splfld',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'resPop8';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','resPop8','Location where research will be conducted','N',427,null,284721,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,'checkbox','{chkArray:[{data: ''option1'', display:''MDACC/Regional Centers''}, {data: ''option2'', display:''Non-MDACC Hospital/Clinic Healthcare Facility/Harris County Hospital District Facility''}, {data: ''option3'', display:''Community Center/Shopping Mall (e.g. religious facility)''}, {data: ''option4'', display:''Educational Campus (e.g. elementary or secondary school or a university campus''}, {data: ''option5'', display:"Subject''s Home (list information on city; county; state; or country where the home is located below)"}, {data: ''option6'', display:''Internet Research (list website names below)''}, {data: ''option7'', display:''International Site''}, {data: ''option8'', display:''Other''} ]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'resPop9';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','resPop9','Identify the name and location for each site selected above (list locations, Regional Cancer Centers, web sites, etc.)','N',428,null,284720,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,'textarea',null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'juvinileEligNo';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','juvinileEligNo','Studies that exclude children must have appropriate justification.  Please select all that apply: ','N',428,null,284719,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,'checkbox','{chkArray:[{data: ''option1'', display:''Phase I or Phase II study targeting cancer that is very unusual in pediatrics (e.g., prostate, lung, breast, chronic lymophocytic leukemia,etc.)''}, {data: ''option2'', display:''FDA has required sponsor to limit to age 18 and above (applicable only to Phase I studies).''}, {data: ''option3'', display:''Phase II or III study with no Phase I data for the drug in pediatrics. Please provide a letter from the Sponsor stating if a Phase I study planned for patients <18 years of age. (May include file attachment)''}, {data: ''option4'', display:''Phase I study to include participants with diseases that occur in pediatrics (e.g., leukemia, lymphoma, sarcomas, brain tumors), but sponsor will not allow patients <18 years of age. Please include a justification letter from the Sponsor. (May include file attachment)''}, {data: ''option5'', display:''Other''} ]}',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'juvinileEligNo1';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','juvinileEligNo1','If other:','N',429,null,284718,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'biosafety4';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','biosafety4','Provide the IBC protocol number or contact the IBC coordinator for information at 713-563-3879.','N',430,null,284717,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,null,null,null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'biosafety5';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE,CODELST_KIND) values (seq_er_codelst.nextval,null,'studyidtype','biosafety5','Are you using Tisseel or Evicel or similar?','N',431,null,284716,null,null,null,to_date('08-JUL-14','DD-MON-RR'),null,'dropdown','<SELECT id="alternateId" NAME="alternateId"> <OPTION value = "">Select an option</OPTION> <OPTION value = "Yes">Yes</OPTION> <OPTION value = "No">No</OPTION> <OPTION value = "NA">N/A</OPTION> </SELECT>',null,null); 
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,358,1,'01_studyDetailFields_insert.sql',sysdate,'v10 #759');

commit;
