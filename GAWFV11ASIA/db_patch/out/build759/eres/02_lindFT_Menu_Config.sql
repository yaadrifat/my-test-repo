set define off;

DECLARE
  v_item_exists number := 0;  
BEGIN

select count(*) into v_item_exists from ER_OBJECT_SETTINGS where OBJECT_TYPE = 'TM' and OBJECT_SUBTYPE = 'irb_menu' and OBJECT_NAME='top_menu';
if (v_item_exists = 0) then
  Insert into ERES.ER_OBJECT_SETTINGS
   (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
    OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
  Values
   (SEQ_ER_OBJECT_SETTINGS.nextval, 'TM', 'irb_menu', 'top_menu', 4, 
    1, 'Protocols', 0);
  commit;
  dbms_output.put_line('One row inserted');
else
  update ER_OBJECT_SETTINGS set OBJECT_DISPLAYTEXT='Protocols' where OBJECT_TYPE = 'TM' and OBJECT_SUBTYPE = 'irb_menu' and OBJECT_NAME='top_menu';
  commit;
  dbms_output.put_line('Row already exists');
end if;

select count(*) into v_item_exists from ER_OBJECT_SETTINGS where OBJECT_TYPE = 'M' and OBJECT_SUBTYPE = 'new_menu' and OBJECT_NAME='irb_menu';
if (v_item_exists = 0) then
  Insert into ERES.ER_OBJECT_SETTINGS
   (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
    OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
  Values
   (SEQ_ER_OBJECT_SETTINGS.nextval, 'M', 'new_menu', 'irb_menu', 2, 
    1, 'New Protocol', 0);
  commit;
  dbms_output.put_line('One row inserted');
else
  update ER_OBJECT_SETTINGS set OBJECT_DISPLAYTEXT='New Protocol' where OBJECT_TYPE = 'M' and OBJECT_SUBTYPE = 'new_menu' and OBJECT_NAME='irb_menu';
  commit;
  dbms_output.put_line('Row already exists');
end if;

select count(*) into v_item_exists from ER_OBJECT_SETTINGS where OBJECT_TYPE = 'T' and OBJECT_SUBTYPE = 'irb_items_tab' and OBJECT_NAME='irb_pend_tab';
if (v_item_exists = 0) then
  Insert into ERES.ER_OBJECT_SETTINGS
   (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
    OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
  Values
   (SEQ_ER_OBJECT_SETTINGS.nextval, 'T', 'irb_items_tab', 'irb_pend_tab', 3, 
    1, 'Drafts and Submissions', 0);
  commit;
  dbms_output.put_line('One row inserted');
else
  update ER_OBJECT_SETTINGS set OBJECT_DISPLAYTEXT='Drafts and Submissions' where OBJECT_TYPE = 'T' and OBJECT_SUBTYPE = 'irb_items_tab' and OBJECT_NAME='irb_pend_tab';
  commit;
  dbms_output.put_line('Row already exists');
end if;


select count(*) into v_item_exists from ER_OBJECT_SETTINGS where OBJECT_TYPE = 'M' and OBJECT_SUBTYPE = 'study_menu' and OBJECT_NAME='irb_menu';
if (v_item_exists = 0) then
  Insert into ERES.ER_OBJECT_SETTINGS
   (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
    OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
  Values
   (SEQ_ER_OBJECT_SETTINGS.nextval, 'M', 'study_menu', 'irb_menu', 10, 
    1, 'All Protocols', 0);
  commit;
  dbms_output.put_line('One row inserted');
else
  update ER_OBJECT_SETTINGS set OBJECT_DISPLAYTEXT='All Protocols' where OBJECT_TYPE = 'M' and OBJECT_SUBTYPE = 'study_menu' and OBJECT_NAME='irb_menu';
  commit;
  dbms_output.put_line('Row already exists');
end if;

end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,358,2,'02_lindFT_Menu_Config.sql',sysdate,'v10 #759');

commit;
