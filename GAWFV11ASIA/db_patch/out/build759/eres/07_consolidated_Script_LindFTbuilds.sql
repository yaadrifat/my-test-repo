/* Consolidated Script for code merge of Lind FT builds released between builds 1 to 32 */

/* LE build #01 Scripts */

set define off;

DECLARE
  v_item_exists number := 0; 
BEGIN
select count(*) into v_item_exists from user_tab_cols where TABLE_NAME = 'UI_FLX_PAGEVER'
and COLUMN_NAME = 'PAGE_ATTACHDIFF';
if (v_item_exists = 0) then
  execute immediate 'ALTER TABLE ERES.UI_FLX_PAGEVER ADD (PAGE_ATTACHDIFF CLOB)';
  dbms_output.put_line('Col PAGE_ATTACHDIFF added');
else
  dbms_output.put_line('Col PAGE_ATTACHDIFF already exists');
end if;
 
END;
/
 
COMMENT ON COLUMN
ERES.UI_FLX_PAGEVER.PAGE_ATTACHDIFF IS
'This column stores the attachment difference of current and previous version';

-->agent -dropdown
-->dose -free text
-->Dose rationale --free text
-->Route of administration--dropdown
-->manufacturer--lookup


declare
v_CodeExists number := 0;
begin
	
	---1---
	-- inserting the second set of drug/device repeating fields--(part of the first set already exists , so will not be adding those again:
	--the fields that already existed are codelst_subtyp = 'dose' , codelst_subtyp = 'doseRationale' , codelst_subtyp = 'route'
	--so we are just going to have these fields as is and then the sequence starts from 2, 3 ..and so on.
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugAgent1';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugAgent1', 'Agent1', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugMnftr1';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugMnftr1', 'Manufacturer', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	---2------
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugAgent2';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugAgent2', 'Agent2', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dose2';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'dose2', 'Dose', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseRatnl2';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'doseRatnl2', 'Dose Rationale', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'routeAdmn2';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'routeAdmn2', 'Route of Administration', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugMnftr2';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugMnftr2', 'Manufacturer', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

--- 3-----

SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugAgent3';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugAgent3', 'Agent3', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dose3';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'dose3', 'Dose', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseRatnl3';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'doseRatnl3', 'Dose Rationale', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'routeAdmn3';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'routeAdmn3', 'Route of Administration', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugMnftr3';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugMnftr3', 'Manufacturer', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

--4--
SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugAgent4';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugAgent4', 'Agent4', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dose4';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'dose4', 'Dose', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseRatnl4';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'doseRatnl4', 'Dose Rationale', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'routeAdmn4';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'routeAdmn4', 'Route of Administration', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugMnftr4';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugMnftr4', 'Manufacturer', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

--5--
SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugAgent5';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugAgent5', 'Agent5', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dose5';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'dose5', 'Dose', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseRatnl5';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'doseRatnl5', 'Dose Rationale', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'routeAdmn5';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'routeAdmn5', 'Route of Administration', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugMnftr5';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugMnftr5', 'Manufacturer', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;


--6--

SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugAgent6';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugAgent6', 'Agent6', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dose6';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'dose6', 'Dose', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseRatnl6';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'doseRatnl6', 'Dose Rationale', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'routeAdmn6';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'routeAdmn6', 'Route of Administration', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugMnftr6';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugMnftr6', 'Manufacturer', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;


--7--

SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugAgent7';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugAgent7', 'Agent7', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dose7';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'dose7', 'Dose', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseRatnl7';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'doseRatnl7', 'Dose Rationale', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'routeAdmn7';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'routeAdmn7', 'Route of Administration', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugMnftr7';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugMnftr7', 'Manufacturer', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

--8--

SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugAgent8';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugAgent8', 'Agent8', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dose8';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'dose8', 'Dose', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseRatnl8';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'doseRatnl8', 'Dose Rationale', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'routeAdmn8';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'routeAdmn8', 'Route of Administration', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugMnftr8';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugMnftr8', 'Manufacturer', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
		
	END IF;

--9--
SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugAgent9';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugAgent9', 'Agent9', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dose9';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'dose9', 'Dose', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseRatnl9';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'doseRatnl9', 'Dose Rationale', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'routeAdmn9';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'routeAdmn9', 'Route of Administration', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugMnftr9';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugMnftr9', 'Manufacturer', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;



--10--
SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugAgent10';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugAgent10', 'Agent10', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dose10';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'dose10', 'Dose', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'doseRatnl10';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'doseRatnl10', 'Dose Rationale', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'routeAdmn10';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'routeAdmn10', 'Route of Administration', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'drugMnftr10';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'drugMnftr10', 'Manufacturer', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;


COMMIT;
END;
/

-->device -dropdown
-->manufacturer -lookup
-->risk assessment attachment --checkbox


declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'deviceAgent1';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'deviceAgent1', 'Device 1', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	--change this to lookup !!!!!!!!!!!-----------------------------------------
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'devMnftr1';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'devMnftr1', 'Manufacturer', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'riskAssmt1';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'riskAssmt1', 'Risk Assessment Attachment', 'checkbox');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	
	---2---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'deviceAgent2';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'deviceAgent2', 'Device 2', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'devMnftr2';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'devMnftr2', 'Manufacturer', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'riskAssmt2';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'riskAssmt2', 'Risk Assessment Attachment', 'checkbox');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	---3---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'deviceAgent3';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'deviceAgent3', 'Device 3', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'devMnftr3';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'devMnftr3', 'Manufacturer', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'riskAssmt3';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'riskAssmt3', 'Risk Assessment Attachment', 'checkbox');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	---4---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'deviceAgent4';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'deviceAgent4', 'Device 4', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'devMnftr4';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'devMnftr4', 'Manufacturer', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'riskAssmt4';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'riskAssmt4', 'Risk Assessment Attachment', 'checkbox');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	---5---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'deviceAgent5';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'deviceAgent5', 'Device 5', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'devMnftr5';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'devMnftr5', 'Manufacturer', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'riskAssmt5';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'riskAssmt5', 'Risk Assessment Attachment', 'checkbox');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	

COMMIT;
END;
/

-->sponsor name -lookup (Sponsor List in 9.2)
-->funding type -dropdown


declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'sponsorName1';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'sponsorName1', 'Sponsor Name1', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundingType1';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'fundingType1', 'Funding Type', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	
	---2---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'sponsorName2';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'sponsorName2', 'Sponsor Name 2', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundingType2';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'fundingType2', 'Funding Type', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	---3---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'sponsorName3';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'sponsorName3', 'Sponsor Name 3', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundingType3';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'fundingType3', 'Funding Type', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	---4---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'sponsorName4';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'sponsorName4', 'Sponsor Name 4', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundingType4';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'fundingType4', 'Funding Type', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	
	---5---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'sponsorName5';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'sponsorName5', 'Sponsor Name 5', 'lookup');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundingType5';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'fundingType5', 'Funding Type', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	

COMMIT;
END;
/

UPDATE ER_CODELST
SET CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "deptFunds">Departmental Funds</OPTION>
<OPTION value = "dod">DOD</OPTION>
<OPTION value = "donorFunds">Donor Funds</OPTION>
<OPTION value = "industry">Industry</OPTION>
<OPTION value = "NCI">NCI</OPTION>
<OPTION value = "nih">NIH (other than NCI)</OPTION>
<OPTION value = "peerRevFund">Other peer reviewed funding (e.g. NSF, ACS, etc.)</OPTION>
<OPTION value = "unfunded">Unfunded</OPTION>
</SELECT>'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP IN(
'fundingType1',
'fundingType2',
'fundingType3',
'fundingType4',
'fundingType5');

--setting the lookup field
update er_codelst  set codelst_custom_col1 = '{lookupPK:100040, selection:"single", mapping:[{source:"Sponsor_Name", target:"alternateId"}]}' 
where codelst_subtyp in ('sponsorName1', 'sponsorName2', 'sponsorName3', 'sponsorName4', 'sponsorName5'); 

	
--changing the name of the field that says Grant Serial#/Contract# to COEUS#
update er_codelst set codelst_desc = ' COEUS#' where  codelst_subtyp = 'grant' ;

COMMIT;

---adding the dropdown options to the device agent dropdown. There are 5 of these dropdowns on the screen.
UPDATE ER_CODELST
SET CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "adapthence">Adapter henceforth</OPTION>
<OPTION value = "affyGene">Affymetrix Gene Chip U133A</OPTION>
<OPTION value = "bipapVision">BIPAP Vision Ventilatory Support System</OPTION>
<OPTION value = "biocomp">Biocompatibles LC Bead (100-300) loaded with Doxorubicin HCl</OPTION>
<OPTION value = "bioSeal">Bio-Seal Track Plug</OPTION>
<OPTION value = "block">B-lock solution</OPTION>
<OPTION value = "caphosol">Caphosol</OPTION>
<OPTION value = "cavSpine">Cavity SpineWand</OPTION>
<OPTION value = "cliniMACS">CliniMACS device</OPTION>
<OPTION value = "cryocare">Cryocare</OPTION>
<OPTION value = "daVinci">da Vinci Robotic Surgical System</OPTION>
<OPTION value = "exAblate">ExAblate device</OPTION>
<OPTION value = "galilMed">Galil Medical Cryoprobes</OPTION>
<OPTION value = "goldFid">Gold Fiducial Marker</OPTION>
<OPTION value = "imageGuide">Image Guide Microscope - a high-resolution microendoscope (HRME)</OPTION>
<OPTION value = "imagio">Imagio Breast Imaging System</OPTION>
<OPTION value = "indocyanine">Indocyanine green fluorescence lymphography system</OPTION>
<OPTION value = "infrared">Infrared camera</OPTION>
<OPTION value = "inSpectra">InSpectra Tissue Oxygenation Monitor Model 650 and Spot Check Monitor Model 300</OPTION>
<OPTION value = "integrated">Integrated BRACAnalysis</OPTION>
<OPTION value = "intensity">Intensity Modulated Radiotherapy</OPTION>
<OPTION value = "iodine125">Iodine-125 radioactive seeds</OPTION>
<OPTION value = "louis3D">LOUIS-3D (Laser OPtoacoustic Ultrasonic Imaging System</OPTION>
<OPTION value = "lowCoh">Low Coherence Enhanced Backscattering (LEBS)</OPTION>
<OPTION value = "mdaApp">MDA Applicator</OPTION>
<OPTION value = "mirena">Mirena (Levonorgestrel Contraceptive IUD)</OPTION>
<OPTION value = "pemFlex">PEMFlex Solo II High Resolution PET Scanner</OPTION>
<OPTION value = "pointSpectro">Point spectroscopy probe(POS) and PS2</OPTION>
<OPTION value = "prostateImm">Prostate Immobilization Treatment Device</OPTION>
<OPTION value = "senoRxCon">SenoRx Contura MLB applicator</OPTION>
<OPTION value = "SERI">SERI Surgical Scaffold</OPTION>
<OPTION value = "SIR">SIR-spheres</OPTION>
<OPTION value = "sonablate">Sonablate 500 (Sonablate)</OPTION>
<OPTION value = "spectra">Spectra Optia Apheresis System</OPTION>
<OPTION value = "vapotherm">The Vapotherm 2000i Respiratory Therapy Device</OPTION>
<OPTION value = "strattice">Strattice Tissue Matrix</OPTION>
<OPTION value = "syntheCel">SyntheCelTM Dura Replacement</OPTION>
<OPTION value = "ISLLC">The ISLLC Implantable Drug Delivery System (IDDS)</OPTION>
<OPTION value = "visica">The Visica 2 Treatment System</OPTION>
<OPTION value = "theraSphere">TheraSphere</OPTION>
<OPTION value = "transducer">Transducer-tipped catheter Mikro-Tip SPC-320, transducer Millar, and Power Lab 8/30 amplifier</OPTION>
<OPTION value = "truFreeze">truFreezeTM System</OPTION>
<OPTION value = "UB500">UB500</OPTION>
<OPTION value = "viOptix">ViOptix continuous transcutaneous tissue oximetry device</OPTION>
<OPTION value = "vendys">VENDYS-Model 6000 B/C</OPTION>
<OPTION value = "veridex">Veridex CellSearch assay</OPTION>
<OPTION value = "visica">Visica 2 Treatment System</OPTION>
</SELECT>'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP IN('deviceAgent1' , 'deviceAgent2', 'deviceAgent3', 'deviceAgent4', 'deviceAgent5');

--setting up the look up field
update er_codelst  set codelst_custom_col1 = '{lookupPK:100040, selection:"single", mapping:[{source:"Sponsor_Name", target:"alternateId"}]}' where 
codelst_subtyp in ('devMnftr1', 'devMnftr2', 'devMnftr3', 'devMnftr4', 'devMnftr5');

COMMIT;

---adding the dropdown options to the drug agent dropdown. There are 10 of these dropdowns on the screen.


UPDATE ER_CODELST
SET CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "abiraterone">abiraterone</OPTION>
<OPTION value = "abraxane">abraxane</OPTION>
<OPTION value = "ABT-199">ABT-199</OPTION>
<OPTION value = "ABT-888">ABT-888 (veliparib)</OPTION>
<OPTION value = "AC220">AC220</OPTION>
<OPTION value = "AG-013736">AG-013736</OPTION>
<OPTION value = "AMG-337">AMG-337</OPTION>
<OPTION value = "AP-24534">AP-24534</OPTION>
<OPTION value = "ARRY-614">ARRY-614</OPTION>
<OPTION value = "AZD-1208">AZD-1208</OPTION>
<OPTION value = "birinipant">birinipant</OPTION>
<OPTION value = "BKM-120">BKM-120</OPTION>
<OPTION value = "BMS-833923">BMS-833923</OPTION>
<OPTION value = "bortezomib">bortezomib</OPTION>
<OPTION value = "Bosutinib">Bosutinib</OPTION>
<OPTION value = "CC-223">CC-223</OPTION>
<OPTION value = "cediranib">cediranib (AZD-2171)</OPTION>
<OPTION value = "CMX-001">CMX-001</OPTION>
<OPTION value = "CPX-351">CPX-351</OPTION>
<OPTION value = "crenolanib">Crenolanib</OPTION>
<OPTION value = "CWP-23229">CWP-232291</OPTION>
<OPTION value = "dasatinib">dasatinib</OPTION>
<OPTION value = "denileukin">denileukin diftitox</OPTION>
<OPTION value = "E-75acetate">E-75 acetate</OPTION>
<OPTION value = "eflornithine">eflornithine</OPTION>
<OPTION value = "entinostat">Entinostat</OPTION>
<OPTION value = "erlotinib">erlotinib</OPTION>
<OPTION value = "everolimus">everolimus (RAD-001)</OPTION>
<OPTION value = "GDC-0941">GDC-0941</OPTION>
<OPTION value = "GS-1101">GS-1101</OPTION>
<OPTION value = "GSK-1120212">GSK-1120212</OPTION>
<OPTION value = "dabrafenib">GSK-2118436 (dabrafenib)</OPTION>
<OPTION value = "GSK-525762">GSK-525762</OPTION>
<OPTION value = "ibrutinib">ibrutinib</OPTION>
<OPTION value = "ICL-670">ICL-670</OPTION>
<OPTION value = "INCB-018424">INCB-018424</OPTION>
<OPTION value = "KB-004">KB-004</OPTION>
<OPTION value = "lacosamide">lacosamide</OPTION>
<OPTION value = "Lapatinib">Lapatinib</OPTION>
<OPTION value = "LDE-225">LDE-225</OPTION>
<OPTION value = "LEE-011">LEE-011</OPTION>
<OPTION value = "lenalidomide">lenalidomide(CC-5013)</OPTION>
<OPTION value = "LY-2784544">LY-2784544</OPTION>
<OPTION value = "LY-2940680">LY-2940680</OPTION>
<OPTION value = "LY-3009120">LY-3009120</OPTION>
<OPTION value = "masatinib">masatinib</OPTION>
<OPTION value = "MEK-162">MEK-162</OPTION>
<OPTION value = "MK-1775">MK-1775</OPTION>
<OPTION value = "MK-2206">MK-2206</OPTION>
<OPTION value = "MRX-34-">MRX-34</OPTION>
<OPTION value = "olaparib">olaparib</OPTION>
<OPTION value = "Omacetaxine">Omacetaxine</OPTION>
<OPTION value = "pazopanib">pazopanib</OPTION>
<OPTION value = "PKC-412">PKC-412</OPTION>
<OPTION value = "pomalidomide">pomalidomide</OPTION>
<OPTION value = "Ponatinib">Ponatinib</OPTION>
<OPTION value = "Ponatinib-AP">Ponatinib(AP-24534)</OPTION>
<OPTION value = "posaconazole">posaconazole</OPTION>
<OPTION value = "pracinostat">pracinostat</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "Rigosertib>Rigosertib</OPTION>
<OPTION value = "rucaparib">rucaparib</OPTION>
<OPTION value = "Ruxolitinib">ruxolitinib</OPTION>
<OPTION value = "S-1">S-1</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "SAR-302503">SAR-302503</OPTION>
<OPTION value = "topiramate">topiramate</OPTION>
<OPTION value = "volasertib">volasertib</OPTION>
<OPTION value = "vorinostat">vorinostat</OPTION>
<OPTION value = "VST-1001">VST-1001</OPTION>
<OPTION value = "XL-184">XL-184</OPTION>
</SELECT>'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP IN('drugAgent1',
'drugAgent2',
'drugAgent3',
'drugAgent4',
'drugAgent5',
'drugAgent6',
'drugAgent7',
'drugAgent8',
'drugAgent9',
'drugAgent10');

--setting up the lookup field
update er_codelst  set codelst_custom_col1 = '{lookupPK:100040, selection:"single", mapping:[{source:"Sponsor_Name", target:"alternateId"}]}' 
where codelst_subtyp in ('drugMnftr1', 'drugMnftr2', 'drugMnftr3', 'drugMnftr4', 'drugMnftr5','drugMnftr6', 'drugMnftr7','drugMnftr8', 'drugMnftr9', 'drugMnftr10'); 

COMMIT;

---adding the dropdown options to the route of admn dropdown. There are 10 of these dropdowns on the screen.
--2----
UPDATE ER_CODELST
SET CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Aural">Aural</OPTION>
<OPTION value = "Epidural">Epidural</OPTION>
<OPTION value = "Inhalation">Inhalation</OPTION>
<OPTION value = "Intraarterial">Intraarterial</OPTION>
<OPTION value = "Intradermal">Intradermal</OPTION>
<OPTION value = "Intralesional">Intralesional</OPTION>
<OPTION value = "Intramucosal">Intramucosal</OPTION>
<OPTION value = "Intramuscular">Intramuscular</OPTION>
<OPTION value = "Intranasal">Intranasal</OPTION>
<OPTION value = "Intraocular">Intraocular</OPTION>
<OPTION value = "Intraosseous">Intraosseous</OPTION>
<OPTION value = "Intraperitoneal">Intraperitoneal</OPTION>
<OPTION value = "Intrapleural">Intrapleural</OPTION>
<OPTION value = "Intrathecal">Intrathecal</OPTION>
<OPTION value = "Intratumoral">Intratumoral</OPTION>
<OPTION value = "Intravenous">Intravenous</OPTION>
<OPTION value = "Intravesicular">Intravesicular</OPTION>
<OPTION value = "Ocular">Ocular</OPTION>
<OPTION value = "Oral">Oral</OPTION>
<OPTION value = "Rectal">Rectal</OPTION>
<OPTION value = "Subcutaneous">Subcutaneous</OPTION>
<OPTION value = "Sublingual">Sublingual</OPTION>
<OPTION value = "Topical">Topical</OPTION>
<OPTION value = "Transdermal">Transdermal</OPTION>
<OPTION value = "Vaginal">Vaginal</OPTION>
</SELECT>'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP IN(
'routeAdmn2',
'routeAdmn3',
'routeAdmn4',
'routeAdmn5',
'routeAdmn6',
'routeAdmn7',
'routeAdmn8',
'routeAdmn9',
'routeAdmn10');

COMMIT;

/* LE build #02 Scripts */

declare
v_CodeExists number := 0;
begin
	
SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'role' AND CODELST_SUBTYP = 'Collaborators';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_hide)
        values (seq_er_codelst.nextval, 'role', 'Collaborators', 'Collaborators', 'N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	
COMMIT;
END;
/

update ER_OBJECT_SETTINGS set OBJECT_DISPLAYTEXT = 'New Protocol' where OBJECT_NAME = 'irb_menu' and OBJECT_SUBTYPE = 'new_menu';

-- Fixed Bug # 20707 
update ER_OBJECT_SETTINGS set object_sequence= 7  where object_name='irb_new_tab' and object_subtype='irb_approv_tab';

-- Fixed Bug # 20516
update ER_OBJECT_SETTINGS set OBJECT_DISPLAYTEXT = 'Attachments' where object_name='irb_new_tab' and object_subtype='irb_upload_tab' and fk_account=0;

commit;

-- Added these 3 fields in Subject selection.
-->Inclusion Criteria - textarea
-->Inclusion Criteria N/A - textarea
-->Exclusion Criteria - textarea
-->Exclusion Criteria N/A - textarea

declare
v_CodeExists number := 0;
begin
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'incluCriteria';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'incluCriteria', 'Inclusion Criteria', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
		
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'incluCriteriaNa';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'incluCriteriaNa', 'Inclusion Criteria N/A', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;	
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'excluCriteria';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'excluCriteria', 'Exclusion Criteria', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'excluCriteriaNa';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'excluCriteriaNa', 'Exclusion Criteria N/A', 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;	
	
COMMIT;
END;
/

update er_codelst set codelst_desc='MDACC' where codelst_type='INDIDEHolder' and codelst_subtyp='organization';

commit;

declare
v_CodeExists number := 0;
v_PkCode number := 0;
begin
	
SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dept_chairs_ids';	
IF (v_CodeExists = 0) THEN
	INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ, CODELST_CUSTOM_COL)
	VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'dept_chairs_ids', 'Department Chair User ID','N',1, 'readonly-input');
	COMMIT;
	dbms_output.put_line('One row inserted');
ELSE
	dbms_output.put_line('Row already exists');
END IF;

SELECT PK_CODELST INTO v_PkCode FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dept_chairs_ids';
SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dept_chairs';
IF (v_CodeExists = 0) THEN
	INSERT INTO ER_CODELST (PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_SEQ, CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1)
	VALUES (SEQ_ER_CODELST.NEXTVAL, 'studyidtype', 'dept_chairs', 'Department Chair',1, 'lookup', '{lookupPK:6000, selection:"multi", mapping:[{source:"USRFULLNAME", target:"alternateId"},{source:"USRLOGIN", target:"alternateId'||v_PkCode||'"}]}');
	COMMIT;
	dbms_output.put_line('One row inserted');
ELSE
	dbms_output.put_line('Row already exists');
END IF;

	
END;
/

create or replace PROCEDURE        "SP_AUTOGEN_STUDY" (p_account number, p_studynum out varchar2 )
as
V_STUDYNO VARCHAR2(100);
v_studynumber varchar2(100)  := null ;
v_sql varchar2(4000);
V_LAST_ID NUMBER;
v_Count number;
BEGIN
    WHILE V_STUDYNUMBER is null
    LOOP
    SELECT STUDYNUM_AUTOGEN_SQL,(STUDYNUM_LASTID + 1) AS STUDYNUM_LASTID INTO V_SQL,V_LAST_ID FROM ER_ACCOUNT_SETTINGS WHERE FK_ACCOUNT = P_ACCOUNT;
    EXECUTE IMMEDIATE V_SQL INTO V_STUDYNO;
    SELECT COUNT(*) INTO V_COUNT FROM ER_STUDY WHERE STUDY_NUMBER = V_STUDYNO || V_LAST_ID;
    UPDATE ER_ACCOUNT_SETTINGS SET STUDYNUM_LASTID = V_LAST_ID  WHERE FK_ACCOUNT = P_ACCOUNT;
    if(V_COUNT = 0) then
    EXECUTE IMMEDIATE V_SQL INTO V_STUDYNUMBER;
    v_studynumber:=RPAD(v_studynumber,6,'0');
    P_STUDYNUM := V_STUDYNUMBER || V_LAST_ID;
    END IF;
    end LOOP ;
End;
/

/* LE build #03 Scripts */

declare
v_CodeExists number := 0;
v_PkCode number := 0;
begin
  SELECT PK_CODELST INTO v_PkCode FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nom_rev_id';
  if (v_PkCode is not null) then
    update ER_CODELST set CODELST_CUSTOM_COL1=
    '{lookupPK:6000, selection:"single", mapping:[{source:"USRFULLNAME", target:"alternateId"},{source:"USRLOGIN", target:"alternateId'||v_PkCode||'"}]}' where 
    CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nom_rev';
      COMMIT;
    dbms_output.put_line('One row updated');
  end if;
  SELECT PK_CODELST INTO v_PkCode FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nom_alt_rev_id';
  if (v_PkCode is not null) then
    update ER_CODELST set CODELST_CUSTOM_COL1=
    '{lookupPK:6000, selection:"single", mapping:[{source:"USRFULLNAME", target:"alternateId"},{source:"USRLOGIN", target:"alternateId'||v_PkCode||'"}]}' where 
    CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nom_alt_rev';
      COMMIT;
    dbms_output.put_line('One row updated');
  end if;
  SELECT PK_CODELST INTO v_PkCode FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dept_chairs_ids';
  if (v_PkCode is not null) then
    update ER_CODELST set codelst_custom_col='hidden-input' where 
    CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dept_chairs_ids';
      COMMIT;
    dbms_output.put_line('One row updated');
  end if;
  SELECT PK_CODELST INTO v_PkCode FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nom_rev_id';
  if (v_PkCode is not null) then
    update ER_CODELST set codelst_custom_col='hidden-input' where 
    CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nom_rev_id';
      COMMIT;
    dbms_output.put_line('One row updated');
  end if;
  SELECT PK_CODELST INTO v_PkCode FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nom_alt_rev_id';
  if (v_PkCode is not null) then
    update ER_CODELST set codelst_custom_col='hidden-input' where 
    CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'nom_alt_rev_id';
      COMMIT;
    dbms_output.put_line('One row updated');
  end if;
end;
/

CREATE OR REPLACE PROCEDURE ERES."SP_AUTOGEN_PATIENT" (p_account number, p_patientid out varchar2 )
as
v_patientid varchar2(100);
v_sql varchar2(4000);
v_last_id number;
Begin
    select patientid_autogen_sql,(patientid_lastid + 1) as patientid_lastid into v_sql,v_last_id from er_account_settings where fk_account = p_account and
      PATIENTID_AUTOGEN_SQL is not null;
    update er_account_settings set patientid_lastid = v_last_id  where fk_account = p_account;
    execute immediate v_sql into v_patientid;
    p_patientid := v_patientid || v_last_id;
End ;
/

create or replace PROCEDURE        "SP_AUTOGEN_STUDY" (p_account number, p_studynum out varchar2 )
as
V_STUDYNO VARCHAR2(100);
v_studynumber varchar2(100)  := null ;
v_sql varchar2(4000);
V_LAST_ID NUMBER;
v_Count number;
BEGIN
    WHILE V_STUDYNUMBER is null
    LOOP
    SELECT STUDYNUM_AUTOGEN_SQL,(STUDYNUM_LASTID + 1) AS STUDYNUM_LASTID INTO V_SQL,V_LAST_ID FROM ER_ACCOUNT_SETTINGS WHERE FK_ACCOUNT = P_ACCOUNT and
      STUDYNUM_AUTOGEN_SQL is not null;
    EXECUTE IMMEDIATE V_SQL INTO V_STUDYNO;
    SELECT COUNT(*) INTO V_COUNT FROM ER_STUDY WHERE STUDY_NUMBER = V_STUDYNO || V_LAST_ID;
    UPDATE ER_ACCOUNT_SETTINGS SET STUDYNUM_LASTID = V_LAST_ID  WHERE FK_ACCOUNT = P_ACCOUNT;
    if(V_COUNT = 0) then
    EXECUTE IMMEDIATE V_SQL INTO V_STUDYNUMBER;
    v_studynumber:=RPAD(v_studynumber,6,'0');
    P_STUDYNUM := V_STUDYNUMBER || V_LAST_ID;
    END IF;
    end LOOP ;
End;
/

/* LE build #04 Scripts */

--For issue with updation in seq #5
--crcAppealed

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcAppealed';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'crcAppealed', 'CRC Appealed', 'departmental,Study','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--crcApproved

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcApproved';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'crcApproved', 'CRC Approved', 'departmental','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--crcAprvContngs

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcAprvContngs';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'crcAprvContngs', 'CRC Approved With Contingencies', 'departmental','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--crcAprvMinorCon

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcAprvMinorCon';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'crcAprvMinorCon', 'CRC Approved With Minor Contingencies', 'departmental','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--crcAprvContngs

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcAprvContgncs';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'crcAprvContgncs', 'CRC Approved With Major Contingencies', 'departmental','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--crcDeferred

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcDeferred';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'crcDeferred', 'CRC Deferred', 'departmental','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--crcDisapproved

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcDisapproved';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'crcDisapproved', 'CRC Disapproved', 'departmental','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--crcInRev

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcInRev';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'crcInRev', 'CRC In Review', 'departmental','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--crcReturn

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcReturn';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'crcReturn', 'CRC Modifications Required', 'departmental','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--crcRejected

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcRejected';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'crcRejected', 'CRC Rejected', 'departmental','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--crcSentReviewer

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcSentReviewer';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'crcSentReviewer', 'CRC Sent To Reviewers', 'departmental','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--crcSentReviewer

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcSentReviewer';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'crcSentReviewer', 'CRC Sent To Reviewers', 'departmental','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--crcTempHold

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcTempHold';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'crcTempHold', 'CRC Temporary Hold', 'departmental','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--crcPbhsrcRejctd

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcPbhsrcRejctd';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'crcPbhsrcRejctd', 'CRC/PBHSRC Rejected', 'departmental','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--crcResubmitted

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcResubmitted';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'crcResubmitted', 'HRPP Resubmitted', 'departmental','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--crcSubmitted

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'crcSubmitted';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'crcSubmitted', 'HRPP Submitted', 'departmental','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--medAncRevCmp

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'medAncRevCmp';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'medAncRevCmp', 'Medical or Ancillary Review Completed', 'departmental','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--medAncRevPend

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'medAncRevPend';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'medAncRevPend', 'Medical or Ancillary Review Pending', 'departmental','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--irb_work_start

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irb_work_start';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'irb_work_start', 'IRB: Application Work Started', 'irb','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--app_CHR

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'app_CHR';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'app_CHR', 'IRB: Approved', 'irb','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--irb_defer

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irb_defer';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'irb_defer', 'IRB: Deferred', 'irb','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--rej_CHR

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'rej_CHR';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'rej_CHR', 'IRB: Disapproved', 'irb','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--irb_human

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irb_human';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'irb_human', 'IRB: Human Research, Not Engaged', 'irb','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--irb_in_rev

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irb_in_rev';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'irb_in_rev', 'IRB: In Review', 'irb','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--irb_init_subm

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irb_init_subm';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'irb_init_subm', 'IRB: Initial Submission', 'irb','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--irb_add_info

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irb_add_info';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'irb_add_info', 'IRB: Modifications Required', 'irb','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--irb_not_human

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irb_not_human';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'irb_not_human', 'IRB: Not Human Research', 'irb','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--pi_resp_rev

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'pi_resp_rev';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'pi_resp_rev', 'IRB: PI Response Sent to Reviewers', 'irb','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--irb_ammend

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irb_ammend';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'irb_ammend', 'IRB: Submission - Amendment', 'irb','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--irb_renew

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irb_renew';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'irb_renew', 'IRB: Submission - Renewal', 'irb','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--irb_temp_hold

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irb_temp_hold';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'irb_temp_hold', 'IRB: Temporary Hold', 'irb','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--irb_terminated

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irb_terminated';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'irb_terminated', 'IRB: Terminated', 'irb','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--irb_withdrawn

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat' AND CODELST_SUBTYP = 'irb_withdrawn';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col1,codelst_hide,codelst_study_role)
        values (seq_er_codelst.nextval, 'studystat', 'irb_withdrawn', 'IRB: Withdrawn', 'irb','N','default_data');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--For issue with update in seq #6

--dsmbInt

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'dsmbInt';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col,codelst_hide)
        values (seq_er_codelst.nextval, 'studyidtype', 'dsmbInt', 'Data Safety and Monitoring Board', 'dropdown','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--expdBiostat

declare
v_CodeExists number := 0;
begin
	--need to review custom col1 update
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'expdBiostat';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col,codelst_hide)
        values (seq_er_codelst.nextval, 'studyidtype', 'expdBiostat', 'Are you requesting an expedited biostatistical review for this protocol?', 'checkbox','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

DECLARE
v_ItemExists number := 0;
v_LkpId number := 100090;
BEGIN

select count(*) into v_ItemExists from ER_LKPLIB where LKPTYPE_NAME like 'Sponsors%';
if (v_ItemExists > 0) then
  dbms_output.put_line('Sponsors Lookup already exists');
  return;
end if;
select count(*) into v_ItemExists from ER_LKPLIB where PK_LKPLIB = v_LkpId;
if (v_ItemExists > 0) then
  dbms_output.put_line('Lookup ID '||v_LkpId||' already exists');
  return;
end if;

Insert into ER_LKPLIB (PK_LKPLIB,LKPTYPE_NAME,LKPTYPE_VERSION,LKPTYPE_DESC,
LKPTYPE_TYPE,FK_ACCOUNT,FK_SITE) values (v_LkpId,'Sponsors','1.0','Sponsors Lookup',null,null,null);

Insert into ER_LKPVIEW (PK_LKPVIEW,LKPVIEW_NAME,LKPVIEW_DESC,FK_LKPLIB,
LKPVIEW_RETDATA,LKPVIEW_RETDISP,LKPVIEW_ISACTIVE,LKPVIEW_IGNOREFILTER,
LKPVIEW_FILTER,LKPVIEW_KEYWORD) values (v_LkpId,'Sponsors','Sponsors Lookup',v_LkpId,null,null,null,null,null,'CustLkp'||v_LkpId);

Insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,
LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD) values 
((select NVL(max(PK_LKPCOL),0)+1 from ER_LKPCOL),v_LkpId,'custom002','Sponsor_Number','varchar2',null,'ER_LKPDATA','Sponsor_Number');

Insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,FK_LKPCOL,LKPVIEW_IS_SEARCH,LKPVIEW_SEQ,
FK_LKPVIEW,LKPVIEW_DISPLEN,LKPVIEW_IS_DISPLAY) values 
((select NVL(max(PK_LKPVIEWCOL),0)+1 from ER_LKPVIEWCOL), (select NVL(max(PK_LKPCOL),0) from ER_LKPCOL),'Y',1,v_LkpId,'50%','Y');

Insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,
LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD) values 
((select NVL(max(PK_LKPCOL),0)+1 from ER_LKPCOL),v_LkpId,'custom001','Sponsor_Name','varchar2',null,'ER_LKPDATA','Sponsor_Name');

Insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,FK_LKPCOL,LKPVIEW_IS_SEARCH,LKPVIEW_SEQ,
FK_LKPVIEW,LKPVIEW_DISPLEN,LKPVIEW_IS_DISPLAY) values 
((select NVL(max(PK_LKPVIEWCOL),0)+1 from ER_LKPVIEWCOL), (select NVL(max(PK_LKPCOL),0) from ER_LKPCOL),'Y',2,v_LkpId,'50%','Y');

COMMIT;

dbms_output.put_line('Lookup ID '||v_LkpId||' structure is created for Sponsors');

END;
/

-- 04

--05

update ER_CODELST set CODELST_DESC='CRC Appealed' where CODELST_TYPE='studystat' and CODELST_SUBTYP='crcAppealed';
update ER_CODELST set CODELST_DESC='CRC Approved' where CODELST_TYPE='studystat' and CODELST_SUBTYP='crcApproved';
update ER_CODELST set CODELST_DESC='CRC Approved With Contingencies' where 
 CODELST_TYPE='studystat' and CODELST_SUBTYP='crcAprvContngs';
update ER_CODELST set CODELST_DESC='CRC Approved With Major Contingencies' where 
 CODELST_TYPE='studystat' and CODELST_SUBTYP='crcAprvContgncs';
update ER_CODELST set CODELST_DESC='CRC Approved With Minor Contingencies' where 
 CODELST_TYPE='studystat' and CODELST_SUBTYP='crcAprvMinorCon';
update ER_CODELST set CODELST_DESC='CRC Deferred' where CODELST_TYPE='studystat' and CODELST_SUBTYP='crcDeferred';
update ER_CODELST set CODELST_DESC='CRC Disapproved' where CODELST_TYPE='studystat' and CODELST_SUBTYP='crcDisapproved';
update ER_CODELST set CODELST_DESC='CRC In Review' where CODELST_TYPE='studystat' and CODELST_SUBTYP='crcInRev';
update ER_CODELST set CODELST_DESC='CRC Modifications Required' where CODELST_TYPE='studystat' and CODELST_SUBTYP='crcReturn';
update ER_CODELST set CODELST_DESC='CRC Rejected' where CODELST_TYPE='studystat' and CODELST_SUBTYP='crcRejected';
update ER_CODELST set CODELST_DESC='CRC Sent To Reviewers' where CODELST_TYPE='studystat' and CODELST_SUBTYP='crcSentReviewer';
update ER_CODELST set CODELST_DESC='CRC Temporary Hold' where CODELST_TYPE='studystat' and CODELST_SUBTYP='crcTempHold';
update ER_CODELST set CODELST_DESC='CRC/PBHSRC Rejected' where CODELST_TYPE='studystat' and CODELST_SUBTYP='crcPbhsrcRejctd';
update ER_CODELST set CODELST_DESC='HRPP Resubmitted' where CODELST_TYPE='studystat' and CODELST_SUBTYP='crcResubmitted';
update ER_CODELST set CODELST_DESC='HRPP Submitted' where CODELST_TYPE='studystat' and CODELST_SUBTYP='crcSubmitted';
update ER_CODELST set CODELST_DESC='Medical or Ancillary Review Completed' where CODELST_TYPE='studystat' and CODELST_SUBTYP='medAncRevCmp';
update ER_CODELST set CODELST_DESC='Medical or Ancillary Review Pending' where CODELST_TYPE='studystat' and CODELST_SUBTYP='medAncRevPend';

update ER_CODELST set CODELST_DESC='IRB: Application Work Started' where  CODELST_TYPE='studystat' and CODELST_SUBTYP='irb_work_start';
update ER_CODELST set CODELST_DESC='IRB: Approved' where CODELST_TYPE='studystat' and CODELST_SUBTYP='app_CHR';
update ER_CODELST set CODELST_DESC='IRB: Deferred' where CODELST_TYPE='studystat' and CODELST_SUBTYP='irb_defer';
update ER_CODELST set CODELST_DESC='IRB: Disapproved' where CODELST_TYPE='studystat' and CODELST_SUBTYP='rej_CHR';
update ER_CODELST set CODELST_DESC='IRB: Human Research, Not Engaged' where CODELST_TYPE='studystat' and CODELST_SUBTYP='irb_human';
update ER_CODELST set CODELST_DESC='IRB: In Review' where CODELST_TYPE='studystat' and CODELST_SUBTYP='irb_in_rev';
update ER_CODELST set CODELST_DESC='IRB: Initial Submission' where CODELST_TYPE='studystat' and CODELST_SUBTYP='irb_init_subm';
update ER_CODELST set CODELST_DESC='IRB: Modifications Required' where CODELST_TYPE='studystat' and CODELST_SUBTYP='irb_add_info';
update ER_CODELST set CODELST_DESC='IRB: Not Human Research' where CODELST_TYPE='studystat' and CODELST_SUBTYP='irb_not_human';
update ER_CODELST set CODELST_DESC='IRB: PI Response Sent to Reviewers' where CODELST_TYPE='studystat' and CODELST_SUBTYP='pi_resp_rev';
update ER_CODELST set CODELST_DESC='IRB: Submission - Amendment' where CODELST_TYPE='studystat' and CODELST_SUBTYP='irb_ammend';
update ER_CODELST set CODELST_DESC='IRB: Submission - Renewal' where CODELST_TYPE='studystat' and CODELST_SUBTYP='irb_renew';
update ER_CODELST set CODELST_DESC='IRB: Temporary Hold' where CODELST_TYPE='studystat' and CODELST_SUBTYP='irb_temp_hold';
update ER_CODELST set CODELST_DESC='IRB: Terminated' where CODELST_TYPE='studystat' and CODELST_SUBTYP='irb_terminated';
update ER_CODELST set CODELST_DESC='IRB: Withdrawn' where CODELST_TYPE='studystat' and CODELST_SUBTYP='irb_withdrawn';

commit;

--06

update er_codelst set codelst_custom_col1= '<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "mdaDsmb">MD ANDERSON DSMB</OPTION>
<OPTION value = "mdaExtdsmb">MD ANDERSON  External DSMB </OPTION>
<OPTION value = "indpDsmb">Independent DSMB - This is a DSMB comprised of members who are not affiliated with the research or sponsor in any way.</OPTION>
<OPTION value = "intDsmb">Internal DSMB � This is a DSMB comprised of study investigators and/or sponsor employees/contractors.</OPTION>
<OPTION value = "notAppl">Not Applicable</OPTION>
</SELECT>' where codelst_subtyp='dsmbInt' and codelst_desc='Data Safety and Monitoring Board'; 

commit;

--07

-->What criteria are you using for classifying Adverse Events
-->Other : Displayed if 'What critera are you using for classifying adverse events' is 'Other'


declare
v_CodeExists number := 0;
begin
	
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'critAdvEvents';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'critAdvEvents', 'What criteria are you using for classifying Adverse Events?', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'critAdvEvntsOth';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc)
        values (seq_er_codelst.nextval, 'studyidtype', 'critAdvEvntsOth', 'Other');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
COMMIT;
END;
/

--08

---adding the dropdown options to the What critera are you using for classifying adverse events dropdown.

UPDATE ER_CODELST
SET CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "CTC2.0">CTC2.0</OPTION>
<OPTION value = "CTCAE3.0">CTCAE3.0</OPTION>
<OPTION value = "CTCAE3.M10">CTCAE3.M10</OPTION>
<OPTION value = "CTCAE4.03">CTCAE4.03</OPTION>
<OPTION value = "Other">Other</OPTION>
</SELECT>'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP = 'critAdvEvents';

UPDATE ER_CODELST
SET CODELST_CUSTOM_COL = 'textarea'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP = 'critAdvEvntsOth';
COMMIT;

--09

UPDATE ER_CODELST
SET CODELST_DESC = 'Are you requesting an expedited biostatistical review for this protocol?'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP = 'expdBiostat';
COMMIT;

/* LE build #05 Scripts */

--01

declare
v_CodeExists number := 0;
v_CodeSeq number := 0;
begin
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'role' AND CODELST_SUBTYP = 'role_21' AND codelst_desc = 'Collaborators';
	IF (v_CodeExists > 0) THEN
		SELECT CODELST_SEQ INTO v_CodeSeq FROM ER_CODELST WHERE CODELST_TYPE = 'role' AND CODELST_SUBTYP = 'role_21' AND codelst_desc = 'Collaborators';
		update ER_CODELST set CODELST_HIDE='Y' WHERE CODELST_TYPE = 'role' AND CODELST_SUBTYP = 'role_21' AND codelst_desc = 'Collaborators';
		update ER_CODELST set CODELST_SEQ=v_CodeSeq WHERE CODELST_TYPE = 'role' AND CODELST_SUBTYP = 'Collaborators' AND codelst_desc = 'Collaborators';
		commit;
		dbms_output.put_line('Collaborators codelst row fixed');
	END IF;
end;
/

--02

-->Are fresh biopsies needed?
-->If yes, please describe : Displayed if 'Are fresh biopsies needed' is 'Yes'


declare
v_CodeExists number := 0;
begin
	
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'freshBiopsyNeed';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'freshBiopsyNeed', 'Are fresh biopsies needed?', 'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'freshBiopsyYes';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'freshBiopsyYes', 'If yes, please describe' , 'textarea');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
COMMIT;
END;
/

--03

UPDATE ER_CODELST
SET CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP = 'freshBiopsyNeed';

COMMIT;

--04

--Biomarker and Correlative Studies : �Methodology � How?� to �Methodology�

update er_codelst set codelst_desc = 'Methodology' where CODELST_TYPE='studyidtype' AND codelst_subtyp = 'methodology' ;
 
-- In �Did an MDACC Biostatistician participate in the study design?�  remove double hyphens from values
UPDATE ER_CODELST
SET CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "YesComp">Yes Completely</OPTION>
<OPTION value = "YesPart">Yes Partially</OPTION>
<OPTION value = "NoSponsrDes">No Sponsor provided biostatistical design</OPTION>
<OPTION value = "NoProsStat">No prospective statistical design in this study</OPTION>
</SELECT>'
where CODELST_TYPE='studyidtype' AND codelst_subtyp = 'proDes2';

--Make first field into �Sponsor Name 1� (currently Sponsor Name1)
update er_codelst set codelst_desc = 'Sponsor Name 1' where codelst_type = 'studyidtype' and codelst_subtyp = 'sponsorName1' ;
update er_codelst set codelst_desc = 'Sponsor Name 2' where codelst_type = 'studyidtype' and codelst_subtyp = 'sponsorName2' ;
update er_codelst set codelst_desc = 'Sponsor Name 3' where codelst_type = 'studyidtype' and codelst_subtyp = 'sponsorName3' ;
update er_codelst set codelst_desc = 'Sponsor Name 4' where codelst_type = 'studyidtype' and codelst_subtyp = 'sponsorName4' ;
update er_codelst set codelst_desc = 'Sponsor Name 5' where codelst_type = 'studyidtype' and codelst_subtyp = 'sponsorName5' ;

--Device 1, Device 2, Device 3, etc. turns into Device-1, Device-2, etc. to stay consistent with Agent
update er_codelst set codelst_desc = 'Device-1' where codelst_type = 'studyidtype' and codelst_subtyp = 'deviceAgent1' ;
update er_codelst set codelst_desc = 'Device-2' where codelst_type = 'studyidtype' and codelst_subtyp = 'deviceAgent2' ;
update er_codelst set codelst_desc = 'Device-3' where codelst_type = 'studyidtype' and codelst_subtyp = 'deviceAgent3' ;
update er_codelst set codelst_desc = 'Device-4' where codelst_type = 'studyidtype' and codelst_subtyp = 'deviceAgent4' ;
update er_codelst set codelst_desc = 'Device-5' where codelst_type = 'studyidtype' and codelst_subtyp = 'deviceAgent5' ;

--For Agent 1, Agent 2, etc. fields should be relabeled into Agent-1, Agent-2, etc
update er_codelst set codelst_desc = 'Agent-1' where codelst_type = 'studyidtype' and codelst_subtyp = 'drugAgent1' ;
update er_codelst set codelst_desc = 'Agent-2' where codelst_type = 'studyidtype' and codelst_subtyp = 'drugAgent2' ;
update er_codelst set codelst_desc = 'Agent-3' where codelst_type = 'studyidtype' and codelst_subtyp = 'drugAgent3' ;
update er_codelst set codelst_desc = 'Agent-4' where codelst_type = 'studyidtype' and codelst_subtyp = 'drugAgent4' ;
update er_codelst set codelst_desc = 'Agent-5' where codelst_type = 'studyidtype' and codelst_subtyp = 'drugAgent5' ;
update er_codelst set codelst_desc = 'Agent-6' where codelst_type = 'studyidtype' and codelst_subtyp = 'drugAgent6' ;
update er_codelst set codelst_desc = 'Agent-7' where codelst_type = 'studyidtype' and codelst_subtyp = 'drugAgent7' ;
update er_codelst set codelst_desc = 'Agent-8' where codelst_type = 'studyidtype' and codelst_subtyp = 'drugAgent8' ;
update er_codelst set codelst_desc = 'Agent-9' where codelst_type = 'studyidtype' and codelst_subtyp = 'drugAgent9' ;
update er_codelst set codelst_desc = 'Agent-10' where codelst_type = 'studyidtype' and codelst_subtyp = 'drugAgent10' ;

COMMIT;

--05

-->funding provided checkbox


declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundProvChk1';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'fundProvChk1', 'Funding Provided', 'checkbox');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	--2--
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundProvChk2';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'fundProvChk2', 'Funding Provided', 'checkbox');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	--3--
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundProvChk3';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'fundProvChk3', 'Funding Provided', 'checkbox');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	--4--
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundProvChk4';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'fundProvChk4', 'Funding Provided', 'checkbox');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	--5--
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'fundProvChk5';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col)
        values (seq_er_codelst.nextval, 'studyidtype', 'fundProvChk5', 'Funding Provided', 'checkbox');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	
	
COMMIT;
END;
/

/* LE build #06 Scripts */

--02

declare
v_CodeExists number := 0;
begin
	
SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyvercat' AND CODELST_SUBTYP = 'prot_priority';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_hide, codelst_seq)
        values (seq_er_codelst.nextval, 'studyvercat', 'prot_priority', 'Protocol Prioritization', 'N', 6);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	
COMMIT;
END;
/

--04

update er_codelst set codelst_custom_col='hidden-input' where codelst_subtyp='rplSubject'; 
commit;

/* LE build #07 Scripts */

--01

CREATE OR REPLACE FUNCTION ERES."F_NON_FUTURE_DATE" (p_date DATE ) RETURN DATE
AS
v_today DATE := trunc(sysdate);
BEGIN
 if (p_date > v_today) then
   return v_today;
 end if;
return trunc(p_date);
END ;
/

--02

---hiding some of the dropdown options to match the values given in the config doc -bug id: 20981 

update er_codelst set codelst_hide = 'Y' where pk_codelst = '7801' and codelst_type = 'study_division'  and codelst_subtyp = 'CANCER';
update er_codelst set codelst_hide = 'Y' where pk_codelst = '7802' and codelst_type = 'study_division'  and codelst_subtyp = 'PEDS';
update er_codelst set codelst_hide = 'Y' where pk_codelst = '7803' and codelst_type = 'study_division'  and codelst_subtyp = 'MEDICINE';
update er_codelst set codelst_hide = 'Y' where pk_codelst = '7805' and codelst_type = 'study_division'  and codelst_subtyp = 'SURGERY';

---changing the label of the field from Blindin Notes to Blinding Description
update er_codelst set codelst_desc = 'Blinding Description' where codelst_subtyp = 'blndTrtGrp' and codelst_type = 'studyidtype';

COMMIT;

/* LE build #01 Scripts */

-- Missing entries insert

--studyCond2

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyCond2';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col,codelst_hide)
        values (seq_er_codelst.nextval, 'studyidtype', 'studyCond2', 'Administrative Department', 'dropdown','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--doubleBlind

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'blinding' AND CODELST_SUBTYP = 'doubleBlind';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col,codelst_hide)
        values (seq_er_codelst.nextval, 'blinding', 'doubleBlind', 'Double-Blind', 'dropdown','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--spnsrshp1

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'spnsrshp1';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col,codelst_hide)
        values (seq_er_codelst.nextval, 'studyidtype', 'spnsrshp1', 'Indicate if the sponsor/supporter/granting agency will receive data', 'dropdown','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--For seq #3

--offStdyCri

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'offStdyCri';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col,codelst_hide)
        values (seq_er_codelst.nextval, 'studyidtype', 'offStdyCri', 'Off Study Criteria', 'checkbox','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--studyscope_3

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyscope' AND CODELST_SUBTYP = 'studyscope_3';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_hide)
        values (seq_er_codelst.nextval, 'studyscope', 'studyscope_3', 'Multi-Center, MDACC lead','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--For seq #4

--pilotFeasibilty

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'phase' AND CODELST_SUBTYP = 'pilotFeasibilty';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc,codelst_hide)
        values (seq_er_codelst.nextval, 'phase', 'pilotFeasibilty', 'Feasibility with N/A','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--For seq#8

--randomized
declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'randomization' AND CODELST_SUBTYP = 'randomized';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE) 
		values (seq_er_codelst.nextval,null,'randomization','randomized','Adaptive Randomization','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--other
declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'randomization' AND CODELST_SUBTYP = 'other';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE) 
		values (seq_er_codelst.nextval,null,'randomization','other','Other','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--none
declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'randomization' AND CODELST_SUBTYP = 'none';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE) 
		values (seq_er_codelst.nextval,null,'randomization','none','None','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--blckRndmztn
declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'randomization' AND CODELST_SUBTYP = 'blckRndmztn';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE) 
		values (seq_er_codelst.nextval,null,'randomization','blckRndmztn','Block Randomization','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--lstRndmztn
declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'randomization' AND CODELST_SUBTYP = 'lstRndmztn';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE) 
		values (seq_er_codelst.nextval,null,'randomization','lstRndmztn','Simple Randomization','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--PocockRndm
declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'randomization' AND CODELST_SUBTYP = 'PocockRndm';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE) 
		values (seq_er_codelst.nextval,null,'randomization','PocockRndm','Stratified Randomization','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--For seq #10

--vp_cr
declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'rev_board' AND CODELST_SUBTYP = 'vp_cr';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE) 
		values (seq_er_codelst.nextval,null,'rev_board','vp_cr','VP of Clinical Research Administration Determination','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--crc_cc
declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'rev_board' AND CODELST_SUBTYP = 'crc_cc';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE) 
		values (seq_er_codelst.nextval,null,'rev_board','crc_cc','Clinical Research Committee','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--irb_cc
declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'rev_board' AND CODELST_SUBTYP = 'irb_cc';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE) 
		values (seq_er_codelst.nextval,null,'rev_board','irb_cc','Institutional Review Board','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--ancillary
declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'rev_board' AND CODELST_SUBTYP = 'ancillary';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE) 
		values (seq_er_codelst.nextval,null,'rev_board','ancillary','Ancillary Review Response','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--medical
declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'rev_board' AND CODELST_SUBTYP = 'medical';	
	IF (v_CodeExists = 0) THEN
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE) 
		values (seq_er_codelst.nextval,null,'rev_board','medical','Medical Review Response','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--01

update ER_CODELST set
CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "DICRC">DICRC</OPTION>
<OPTION value = "dept">Department</OPTION>
<OPTION value = "fedFund">Federal Funding</OPTION>
<OPTION value = "foundation">Foundation</OPTION>
<OPTION value = "privInd">Private Industry</OPTION>
<OPTION value = "privRecint">Private Industry /ReCINT</OPTION>
<OPTION value = "RPCCC">RPCCC</OPTION>
</SELECT>' where CODELST_TYPE='studyidtype' and 
CODELST_SUBTYP in ('fundingType1', 'fundingType2', 'fundingType3',
'fundingType4', 'fundingType5');

-- Study Division
---- Hide QA items
update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='study_division' and
CODELST_SUBTYP in ('cc', 'cardio', 'med', 'neuro', 'surg', 'other',
'study_divisi_31', 'pulmonary');
commit;
---- Add new items if not existing
declare
v_CodeExists number := 0;
begin
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'study_division' AND CODELST_SUBTYP = 'study_divisio_6';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_seq)
        values (seq_er_codelst.nextval, 'study_division', 'study_divisio_6', 'Anesthesiology & Critical Care', 6);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'study_division' AND CODELST_SUBTYP = 'study_divisio_7';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_seq)
        values (seq_er_codelst.nextval, 'study_division', 'study_divisio_7', 'Basic Science', 7);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'study_division' AND CODELST_SUBTYP = 'study_divisio_8';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_seq)
        values (seq_er_codelst.nextval, 'study_division', 'study_divisio_8', 'Cancer Medicine', 8);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'study_division' AND CODELST_SUBTYP = 'study_divisio_9';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_seq)
        values (seq_er_codelst.nextval, 'study_division', 'study_divisio_9', 'Cancer Prevention & Population Sciences', 9);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'study_division' AND CODELST_SUBTYP = 'study_divisi_10';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_seq)
        values (seq_er_codelst.nextval, 'study_division', 'study_divisi_10', 'Diagnostic Imaging', 10);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'study_division' AND CODELST_SUBTYP = 'study_divisi_11';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_seq)
        values (seq_er_codelst.nextval, 'study_division', 'study_divisi_11', 'EVP Physician in Chief Area', 11);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'study_division' AND CODELST_SUBTYP = 'study_divisi_12';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_seq)
        values (seq_er_codelst.nextval, 'study_division', 'study_divisi_12', 'Internal Medicine', 12);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'study_division' AND CODELST_SUBTYP = 'study_divisi_13';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_seq)
        values (seq_er_codelst.nextval, 'study_division', 'study_divisi_13', 'Pathology/Lab Medicine', 13);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'study_division' AND CODELST_SUBTYP = 'study_divisi_14';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_seq)
        values (seq_er_codelst.nextval, 'study_division', 'study_divisi_14', 'Pediatrics', 14);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'study_division' AND CODELST_SUBTYP = 'study_divisi_15';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_seq)
        values (seq_er_codelst.nextval, 'study_division', 'study_divisi_15', 'Radiation Oncology', 15);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'study_division' AND CODELST_SUBTYP = 'sur';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_seq)
        values (seq_er_codelst.nextval, 'study_division', 'sur', 'Surgery', 20);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	commit;
end;
/
-- End of Study Division

-- TArea = Department
---- Hide QA items
update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='tarea' and
CODELST_SUBTYP in ('lip', 'aging', 'alds', 'allergy', 'alzheimer', 'blood',
'brain', 'tareaBreast', 'cancer', 'dental', 'diabetes', 'ent', 'esm', 'eye',
'gastro', 'heart', 'hypertension', 'impotence', 'infertility', 'lipid', 'liver',
'lung', 'mentalhealth', 'obesity', 'other', 'pain', 'pediatric', 'prostate',
'renal', 'reproductive', 'seizures', 'std', 'urogenital');
---- Fix QA items
update ER_CODELST set CODELST_DESC='Genetics',CODELST_HIDE='N',CODELST_SEQ=130,CODELST_CUSTOM_COL1='study_divisio_7' where 
CODELST_TYPE='tarea' and CODELST_SUBTYP in ('tarea_36');
update ER_CODELST set CODELST_DESC='Genitourinary Medical Oncology',CODELST_HIDE='N',CODELST_SEQ=135,CODELST_CUSTOM_COL1='study_divisio_8' where 
CODELST_TYPE='tarea' and CODELST_SUBTYP in ('tarea_37');
update ER_CODELST set CODELST_DESC='Genomic Medicine',CODELST_HIDE='N',CODELST_SEQ=140,CODELST_CUSTOM_COL1='study_divisio_8' where 
CODELST_TYPE='tarea' and CODELST_SUBTYP in ('tarea_38');

commit;
---- Add new items if not existing
declare
v_CodeExists number := 0;
begin
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_14';	
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_14','Anesthesiology & Perioperative Medicine','N',1,'study_divisio_6');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_15';	
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_15','Behavioral Science','N',15,'study_divisio_9');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_16';	
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_16','Bioinformatics & Computational Biology','N',20,'study_divisio_7');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_17';	
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_17','Biostatistics','N',25,'study_divisio_7');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_18';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_18','Breast Medical Oncology','N',35,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_20';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_20','Cancer Biology','N',40,'study_divisio_7');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_21';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_21','Cancer System Imaging','N',45,'study_divisi_10');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_22';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_22','Cardiology','N',50,'study_divisi_12');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_23';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_23','Clinical Cancer Prevention','N',55,'study_divisio_9');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_24';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_24','Critical Care','N',60,'study_divisio_6');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_25';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_25','Dermatology','N',70,'study_divisi_12');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_26';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_26','Diagnostic Radiology','N',75,'study_divisi_10');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_27';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_27','Emergency Medicine','N',80,'study_divisi_12');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_28';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_28','Endocrine Neoplasia and HD','N',85,'study_divisi_12');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_29';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_29','Epidemiology','N',90,'study_divisio_9');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_30';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_30','Experimental Radiation Oncology','N',95,'study_divisi_15');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_31';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_31','Experimental Therapeutics','N',100,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_32';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_32','Gastroenterology Hepat & Nutr','N',105,'study_divisi_12');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_33';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_33','Gastrointestinal Medical Oncology','N',115,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_34';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_34','General Internal Medicine','N',120,'study_divisi_12');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_35';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_35','General Oncology','N',125,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_36';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_36','Genetics','N',130,'study_divisio_7');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_37';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_37','Genitourinary Medical Oncology','N',135,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_38';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_38','Genomic Medicine','N',140,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_39';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_39','Gynecologic Oncology & Reproductive Medicine','N',150,'sur');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_40';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_40','Head & Neck Surgery','N',160,'sur');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_41';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_41','Health Disparities Research','N',165,'study_divisio_9');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_42';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_42','Health Services Research','N',170,'study_divisio_9');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_43';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_43','Hematopathology','N',175,'study_divisi_13');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_44';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_44','Imaging Physics','N',185,'study_divisi_10');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_45';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_45','Immunology','N',190,'study_divisio_7');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_46';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_46','Infectious Diseases','N',195,'study_divisi_12');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_47';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_47','Interventional Radiology','N',200,'study_divisi_10');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_48';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_48','Investigational Cancer Therapeutics','N',205,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_49';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_49','Laboratory Medicine','N',210,'study_divisi_13');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_50';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_50','Leukemia','N',215,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_51';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_51','Lymphoma/Myeloma','N',220,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_52';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_52','Melanoma Medical Oncology','N',225,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_53';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_53','Molecular Carcinogenesis','N',230,'study_divisio_7');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_54';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_54','Molecular Pathology','N',235,'study_divisi_13');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_55';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_55','Molecular and Cellular Oncology','N',240,'study_divisio_7');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_56';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_56','Neuro-Oncology','N',245,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_57';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_57','Neurosurgery','N',255,'sur');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_58';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_58','Nuclear Medicine','N',265,'study_divisi_10');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_59';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_59','Orthopaedic Oncology','N',275,'sur');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_61';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_61','Pain Medicine','N',280,'study_divisio_6');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_62';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_62','Palliative Care & Rehabilitation Medicine','N',285,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_63';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_63','Pathology','N',290,'study_divisi_13');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_64';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_64','Pediatrics','N',295,'study_divisi_14');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_65';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_65','Plastic Surgery','N',300,'sur');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_67';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_67','Proton Therapy','N',305,'study_divisi_15');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_68';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_68','Psychiatry','N',310,'study_divisi_11');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_69';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_69','Pulmonary Medicine','N',315,'study_divisi_12');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_70';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_70','Radiation Oncology','N',320,'study_divisi_15');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_71';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_71','Radiation Physics','N',325,'study_divisi_15');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_73';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_73','Respiratory Care','N',330,'study_divisio_6');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_74';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_74','Sarcoma Medical Oncology - Cytokine & Supportive Oncology','N',335,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_75';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_75','Stem Cell Transplantation','N',340,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_76';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_76','Surgical Oncology','N',345,'sur');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_77';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_77','Symptom Research','N',350,'study_divisi_12');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_78';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_78','Systems Biology','N',355,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_79';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_79','Thoracic & Cardiovascular Surgery','N',360,'sur');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_80';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_80','Thoracic/Head & Neck Medical Oncology','N',370,'study_divisio_8');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_81';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_81','Urology','N',380,'sur');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_83';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_83','Veterinary Medicine & Surgery','N',385,'study_divisio_7');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'tarea' AND CODELST_SUBTYP = 'tarea_82';
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL1) values (seq_er_codelst.nextval,null,'tarea','tarea_82','Veterinary Sciences','N',390,'study_divisio_7');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
-- End of TArea = Department
commit;
end;
/

update ER_CODELST set CODELST_DESC='Administrative Department' where CODELST_TYPE='studyidtype' and CODELST_SUBTYP='studyCond2';
commit;

declare
v_CodeExists number := 0;
begin
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'studyAdmDiv';	
	IF (v_CodeExists = 0) THEN
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL) values (seq_er_codelst.nextval,null,'studyidtype','studyAdmDiv','Administrative Division','N',1,'dropdown');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	commit;
end;
/

update ER_CODELST set CODELST_DESC='Cooperative Group / Other Externally Peer Reviewed' where CODELST_TYPE='research_type' and CODELST_SUBTYP='coop';
update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='research_type' and CODELST_SUBTYP='none';

--Bug 21213 : Field: Blinding Change drop down choice Double - Blinded to Double - Blind 
update er_codelst set codelst_desc = 'Double-Blind' where codelst_type = 'blinding' and codelst_subtyp = 'doubleBlind' ;

commit;

--02

update er_codelst set codelst_custom_col = 'dropdown'  where codelst_subtyp = 'spnsrshp1' ;
---Indicate if the sponsor/supporter/granting agency will receive data -- Should be a dropdown instead of check box.
UPDATE ER_CODELST
SET CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "yes">Yes</OPTION>
<OPTION value = "no">No</OPTION>
</SELECT>'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP = 'spnsrshp1';

---adding the dropdown options to the funding type dropdown. There are 5 of these dropdowns on the screen.
UPDATE ER_CODELST
SET CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "NCI">NCI</OPTION>
<OPTION value = "nih">NIH (other than NCI)</OPTION>
<OPTION value = "dod">DOD</OPTION>
<OPTION value = "peerRevFund">Other peer reviewed funding (e.g. NSF, ACS, etc.)</OPTION>
<OPTION value = "privInd">Private Industry</OPTION>
<OPTION value = "privFound">Private Foundation</OPTION>
<OPTION value = "deptFunds">Departmental Funds</OPTION>
<OPTION value = "donorFunds">Donor Funds</OPTION>
<OPTION value = "unfunded">Unfunded</OPTION>
<OPTION value = "coopGrp">Cooperative group</OPTION>
</SELECT>'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP IN(
'fundingType1',
'fundingType2',
'fundingType3',
'fundingType4',
'fundingType5');

--adding the options to the checkbox
UPDATE ER_CODELST 
SET CODELST_CUSTOM_COL1='{chkArray:[
{data: "option1", display:"Personnel"},
{data: "option2", display:"Patient Care"},
{data: "option3", display:"Startup Costs"},
{data: "option4", display:"Study Agent / Device"},
{data: "option5", display:"Participant Remunerations"},
{data: "option6", display:"Ancillary Services"},
]}'
WHERE CODELST_TYPE='studyidtype'  AND CODELST_SUBTYP IN(
'fundProvChk1',
'fundProvChk2',
'fundProvChk3',
'fundProvChk4',
'fundProvChk5');

COMMIT;

--03

UPDATE ER_CODELST 
SET CODELST_CUSTOM_COL='checkbox',
CODELST_CUSTOM_COL1='{chkArray:[
{data: "option1", display:"Death"},
{data: "option2", display:"Disease Progression"},
{data: "option3", display:"Disease Relapse"},
{data: "option4", display:"Drug Resistant"},
{data: "option5", display:"Intercurrent Illness"},
{data: "option6", display:"Lost to Follow-Up"},
{data: "option7", display:"New Malignancy"},
{data: "option8", display:"Not Able to Comply with Study Requirements"},
{data: "option9", display:"Patient Decision to Discontinue Study"},
{data: "option10", display:"Patient Non-Compliance"},
{data: "option11", display:"Patient Withdrawal of Consent"},
{data: "option12", display:"Physician Discretion"},
{data: "option13", display:"Pregnancy"},
{data: "option14", display:"Toxicity"},
{data: "option15", display:"Treatment Completion"},
{data: "option16", display:"Other"}
]}'
WHERE CODELST_TYPE='studyidtype' AND CODELST_SUBTYP='offStdyCri';

--for bug id 20984
update er_codelst set codelst_seq = '1' where CODELST_TYPE='studyscope' and codelst_subtyp = 'std_cen_single' ;
update er_codelst set codelst_seq = '2' where CODELST_TYPE='studyscope' and codelst_subtyp = 'studyscope_3' ;
update er_codelst set codelst_seq = '5' where CODELST_TYPE='studyscope' and codelst_subtyp = 'std_cen_multi' ;

COMMIT;

--04

--bug 21141
--updating subtypes to Velos supported subtypes
update er_codelst set codelst_subtyp = 'phaseIII' where codelst_type = 'phase' and (codelst_desc = 'Phase III');
update er_codelst set codelst_subtyp = 'phaseI/II' where codelst_type = 'phase' and (codelst_desc = 'Phase I/II');
update er_codelst set codelst_subtyp = 'phaseIV' where codelst_type = 'phase' and (codelst_desc = 'Phase IV');
update er_codelst set codelst_subtyp = 'phaseIV/V' where codelst_type = 'phase' and (codelst_desc = 'Phase IV/V');
update er_codelst set codelst_subtyp = 'phaseII' where codelst_type = 'phase' and (codelst_desc = 'Phase II');
update er_codelst set codelst_subtyp = 'phaseII/III' where codelst_type = 'phase' and (codelst_desc = 'Phase II/III');
update er_codelst set codelst_subtyp = 'phaseI' where codelst_type = 'phase' and (codelst_desc = 'Phase I');

update er_codelst set codelst_subtyp = 'phaseZero' where codelst_type = 'phase' and (codelst_desc = 'Phase 0');
commit;

--hiding Not applicable
update er_codelst set codelst_hide = 'Y' where codelst_type = 'phase' and codelst_subtyp = 'phaseNA';
commit;

--Create or update code-items
DECLARE
  v_item_exists number := 0;  
BEGIN
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'phase' and codelst_desc = 'Phase 0';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'phase','phaseZero','Phase 0','N',5, null);
		dbms_output.put_line('Code-list item Type:phase Subtype:phaseZero  inserted');
	else
		update er_codelst set codelst_subtyp = 'phaseZero', codelst_seq = 5 where codelst_type = 'phase' and (codelst_desc = 'Phase 0');
		dbms_output.put_line('Code-list item Type:phase Subtype:phaseZero already exists');
	end if;
	
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'phase' and codelst_desc = 'Pilot';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'phase','pilot','Pilot','N',40, null);
		dbms_output.put_line('Code-list item Type:phase Subtype:pilot inserted');
	else
		update er_codelst set codelst_subtyp = 'pilot', codelst_seq = 40 where codelst_type = 'phase' and (codelst_desc = 'Pilot');
		dbms_output.put_line('Code-list item Type:phase Subtype:pilot already exists');
	end if;
	
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'phase' and codelst_desc = 'Other';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'phase','other','Other','N',45, null);
		dbms_output.put_line('Code-list item Type:phase Subtype:other inserted');
	else
		update er_codelst set codelst_subtyp = 'other', codelst_seq = 45 where codelst_type = 'phase' and (codelst_desc = 'Other');
		dbms_output.put_line('Code-list item Type:phase Subtype:other already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'phase' and codelst_desc = 'Current Practice';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'phase','currPractice','Current Practice','N',50, null);
		dbms_output.put_line('Code-list item Type:phase Subtype:currPractice inserted');
	else
		update er_codelst set codelst_subtyp = 'currPractice', codelst_seq = 50 where codelst_type = 'phase' and (codelst_desc = 'Current Practice');
		dbms_output.put_line('Code-list item Type:phase Subtype:currPractice already exists');
	end if;
	
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'phase' and codelst_desc = 'Compassionate';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'phase','compassionate','Compassionate','N',55, null);
		dbms_output.put_line('Code-list item Type:phase Subtype:compassionate inserted');
	else
		update er_codelst set codelst_subtyp = 'compassionate', codelst_seq = 55 where codelst_type = 'phase' and (codelst_desc = 'Compassionate');
		dbms_output.put_line('Code-list item Type:phase Subtype:compassionate already exists');
	end if;
	
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'phase' and codelst_desc = 'Lab';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'phase','lab','Lab','N',60, null);
		dbms_output.put_line('Code-list item Type:phase Subtype:Lab inserted');
	else
		update er_codelst set codelst_subtyp = 'lab', codelst_seq = 60 where codelst_type = 'phase' and (codelst_desc = 'Lab');
		dbms_output.put_line('Code-list item Type:phase Subtype:lab already exists');
	end if;
	
	commit;

end;
/

--updating code-item descriptions and sequence
update er_codelst set codelst_desc = 'Phase 1', codelst_seq = 10 where codelst_type = 'phase' and codelst_subtyp = 'phaseI';
update er_codelst set codelst_desc = 'Phase 2', codelst_seq = 15 where codelst_type = 'phase' and codelst_subtyp = 'phaseII';
update er_codelst set codelst_desc = 'Phase 3', codelst_seq = 20 where codelst_type = 'phase' and codelst_subtyp = 'phaseIII';
update er_codelst set codelst_desc = 'Phase 1/2', codelst_seq = 25 where codelst_type = 'phase' and codelst_subtyp = 'phaseI/II';
update er_codelst set codelst_desc = 'Phase 2-3', codelst_seq = 30 where codelst_type = 'phase' and codelst_subtyp = 'phaseII/III';
update er_codelst set codelst_desc = 'Phase 4', codelst_seq = 35 where codelst_type = 'phase' and codelst_subtyp = 'phaseIV';
update er_codelst set codelst_seq = 40 where codelst_type = 'phase' and codelst_subtyp = 'pilot';
update er_codelst set codelst_desc = 'Feasability with N/A', codelst_seq = 65 where codelst_type = 'phase' and codelst_subtyp = 'pilotFeasibilty';
commit;

--hiding code-items which are extra
update er_codelst set codelst_hide = 'Y' where codelst_type = 'phase' and codelst_subtyp = 'phaseIIIorIV';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'phase' and codelst_subtyp = 'phaseIV/V';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'phase' and codelst_subtyp = 'phaseV';
commit;

--05

--bug 20896
update er_codelst set codelst_hide = 'Y' where codelst_type = 'INDIDEHolder' and codelst_subtyp = 'PI';

commit;

--06

--bug 21139
--updating code subtype
update er_codelst set codelst_subtyp = 'ancillary' where codelst_type = 'study_type' and codelst_desc = 'Ancillary';
update er_codelst set codelst_subtyp = 'diagnostic' where codelst_type = 'study_type' and codelst_desc = 'Diagnostic';
commit;

--hiding code-items which are extra
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'ancillary';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'behavioral';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'companion';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'compassionUse';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'diagnostic';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'earlyDetection';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'epidemiological';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'studyTypeNone';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'study';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'prevention';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'qol';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'screening';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'standardofCare';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'therapeutic';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp = 'tissueBankLab';
commit;

update er_codelst set codelst_desc = 'Observational (OBS)', codelst_seq = 10 where codelst_type = 'study_type' and codelst_subtyp = 'observational';
update er_codelst set codelst_desc = 'Ancillary or Correlative (ANC/COR)', codelst_seq = 15 where codelst_type = 'study_type' and codelst_subtyp = 'correlative';
commit;

--Create or update code-items
DECLARE
  v_item_exists number := 0;  
BEGIN
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'study_type' and codelst_desc = 'Interventional (INT)';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'study_type','interventional','Interventional (INT)','N',5, null);
		dbms_output.put_line('Code-list item Type:study_type Subtype:interventional  inserted');
	else
		update er_codelst set codelst_subtyp = 'interventional', codelst_seq = 5 where codelst_type = 'study_type' and (codelst_desc = 'Interventional (INT)');
		dbms_output.put_line('Code-list item Type:study_type Subtype:interventional already exists');
	end if;
	commit;
end;
/

update er_codelst set codelst_hide = 'Y' where codelst_type = 'study_type' and codelst_subtyp not in ('interventional','observational','correlative');
commit;

--07

--bug 21140
update er_codelst set codelst_desc = 'Diagnostic (DIA)', codelst_seq = 5 where codelst_type = 'studyPurpose' and codelst_subtyp = 'diagnostic';
update er_codelst set codelst_desc = 'Basic Science (BAS)', codelst_seq = 10 where codelst_type = 'studyPurpose' and codelst_subtyp = 'basicScience';
update er_codelst set codelst_desc = 'Health Services Research (HSR)', codelst_seq = 20 where codelst_type = 'studyPurpose' and codelst_subtyp = 'healthSvcRsrch';
update er_codelst set codelst_desc = 'Prevention (PRE)', codelst_seq = 25 where codelst_type = 'studyPurpose' and codelst_subtyp = 'prevention';
update er_codelst set codelst_desc = 'Screening (SCR)', codelst_seq = 30 where codelst_type = 'studyPurpose' and codelst_subtyp = 'screening';
update er_codelst set codelst_desc = 'Supportive Care (SUP)', codelst_seq = 35 where codelst_type = 'studyPurpose' and codelst_subtyp = 'supportiveCare';
update er_codelst set codelst_desc = 'Treatment (TRE)', codelst_seq = 40 where codelst_type = 'studyPurpose' and codelst_subtyp = 'treatment';
update er_codelst set codelst_desc = 'Other (OTH)', codelst_seq = 50 where codelst_type = 'studyPurpose' and codelst_subtyp = 'other';

commit;

--Create or update code-items
DECLARE
  v_item_exists number := 0;
BEGIN
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'studyPurpose' and codelst_desc = 'Epidemiologic Trial (EPI)';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'studyPurpose','epidemiologic','Epidemiologic Trial (EPI)','N',15, null);
		dbms_output.put_line('Code-list item Type:studyPurpose Subtype:epidemiologic  inserted');
	else
		update er_codelst set codelst_subtyp = 'epidemiologic', codelst_seq = 15 where codelst_type = 'studyPurpose' and (codelst_desc = 'Epidemiologic Trial (EPI)');
		dbms_output.put_line('Code-list item Type:studyPurpose Subtype:epidemiologic already exists');
	end if;
	
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'studyPurpose' and codelst_desc = 'Observational Trial (Obs)';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'studyPurpose','observational','Observational Trial (Obs)','N',45, null);
		dbms_output.put_line('Code-list item Type:studyPurpose Subtype:observational  inserted');
	else
		update er_codelst set codelst_subtyp = 'observational', codelst_seq = 45 where codelst_type = 'studyPurpose' and (codelst_desc = 'Observational Trial (Obs)');
		dbms_output.put_line('Code-list item Type:studyPurpose Subtype:observational already exists');
	end if;
	
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'studyPurpose' and codelst_desc = 'Outcome Trial (Out)';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'studyPurpose','outcomeTrial','Outcome Trial (Out)','N',55, null);
		dbms_output.put_line('Code-list item Type:studyPurpose Subtype:outcomeTrial  inserted');
	else
		update er_codelst set codelst_subtyp = 'outcomeTrial', codelst_seq = 55 where codelst_type = 'studyPurpose' and (codelst_desc = 'Outcome Trial (Out)');
		dbms_output.put_line('Code-list item Type:studyPurpose Subtype:outcomeTrial already exists');
	end if;
	commit;
end;
/

--08

update er_codelst set codelst_hide = 'Y' where codelst_type = 'randomization' and codelst_desc = 'Bayesian Model Averaging, Continuous Reassessment Method (BMA-CRM)';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'randomization' and codelst_subtyp = 'blckRndmztn' and codelst_desc = 'Block Randomization';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'randomization' and codelst_desc = 'EffTox Dose-finding';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'randomization' and codelst_desc = 'Equal Randomization';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'randomization' and codelst_desc = 'List Randomization';
update er_codelst set codelst_hide = 'Y' where codelst_type = 'randomization' and codelst_desc = 'Pocock-Simon Stratified Randomization';

commit;

update er_codelst set codelst_seq = 10 where codelst_type = 'randomization' and codelst_subtyp = 'other';
update er_codelst set codelst_seq = 9 where codelst_type = 'randomization' and codelst_desc = 'Stratified Randomization';

commit;

--09

--bug 20987
update er_codelst set codelst_custom_col1 = '<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "abiraterone">abiraterone</OPTION>
<OPTION value = "abraxane">abraxane</OPTION>
<OPTION value = "ABT-199">ABT-199</OPTION>
<OPTION value = "ABT-888">ABT-888 (veliparib)</OPTION>
<OPTION value = "AC220">AC220</OPTION>
<OPTION value = "AG-013736">AG-013736</OPTION>
<OPTION value = "AMG-337">AMG-337</OPTION>
<OPTION value = "AP-24534">AP-24534</OPTION>
<OPTION value = "ARRY-614">ARRY-614</OPTION>
<OPTION value = "AZD-1208">AZD-1208</OPTION>
<OPTION value = "birinipant">birinipant</OPTION>
<OPTION value = "BKM-120">BKM-120</OPTION>
<OPTION value = "BMS-833923">BMS-833923</OPTION>
<OPTION value = "bortezomib">bortezomib</OPTION>
<OPTION value = "Bosutinib">Bosutinib</OPTION>
<OPTION value = "CC-223">CC-223</OPTION>
<OPTION value = "cediranib">cediranib (AZD-2171)</OPTION>
<OPTION value = "CMX-001">CMX-001</OPTION>
<OPTION value = "CPX-351">CPX-351</OPTION>
<OPTION value = "crenolanib">Crenolanib</OPTION>
<OPTION value = "CWP-23229">CWP-232291</OPTION>
<OPTION value = "dasatinib">dasatinib</OPTION>
<OPTION value = "denileukin">denileukin diftitox</OPTION>
<OPTION value = "E-75acetate">E-75 acetate</OPTION>
<OPTION value = "eflornithine">eflornithine</OPTION>
<OPTION value = "entinostat">Entinostat</OPTION>
<OPTION value = "erlotinib">erlotinib</OPTION>
<OPTION value = "everolimus">everolimus (RAD-001)</OPTION>
<OPTION value = "GDC-0941">GDC-0941</OPTION>
<OPTION value = "GS-1101">GS-1101</OPTION>
<OPTION value = "GSK-1120212">GSK-1120212</OPTION>
<OPTION value = "dabrafenib">GSK-2118436 (dabrafenib)</OPTION>
<OPTION value = "GSK-525762">GSK-525762</OPTION>
<OPTION value = "ibrutinib">ibrutinib</OPTION>
<OPTION value = "ICL-670">ICL-670</OPTION>
<OPTION value = "INCB-018424">INCB-018424</OPTION>
<OPTION value = "KB-004">KB-004</OPTION>
<OPTION value = "lacosamide">lacosamide</OPTION>
<OPTION value = "Lapatinib">Lapatinib</OPTION>
<OPTION value = "LDE-225">LDE-225</OPTION>
<OPTION value = "LEE-011">LEE-011</OPTION>
<OPTION value = "lenalidomide">lenalidomide(CC-5013)</OPTION>
<OPTION value = "LY-2784544">LY-2784544</OPTION>
<OPTION value = "LY-2940680">LY-2940680</OPTION>
<OPTION value = "LY-3009120">LY-3009120</OPTION>
<OPTION value = "masatinib">masatinib</OPTION>
<OPTION value = "MEK-162">MEK-162</OPTION>
<OPTION value = "MK-1775">MK-1775</OPTION>
<OPTION value = "MK-2206">MK-2206</OPTION>
<OPTION value = "MRX-34-">MRX-34</OPTION>
<OPTION value = "olaparib">olaparib</OPTION>
<OPTION value = "Omacetaxine">Omacetaxine</OPTION>
<OPTION value = "panobinostat (LBH-589)">panobinostat (LBH-589)</OPTION>
<OPTION value = "pazopanib">pazopanib</OPTION>
<OPTION value = "PF-0449913">PF-0449913</OPTION>
<OPTION value = "PKC-412">PKC-412</OPTION>
<OPTION value = "pomalidomide">pomalidomide</OPTION>
<OPTION value = "Ponatinib">Ponatinib</OPTION>
<OPTION value = "Ponatinib-AP">Ponatinib(AP-24534)</OPTION>
<OPTION value = "posaconazole">posaconazole</OPTION>
<OPTION value = "pracinostat">pracinostat</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "Rigosertib>Rigosertib</OPTION>
<OPTION value = "rucaparib">rucaparib</OPTION>
<OPTION value = "Ruxolitinib">ruxolitinib</OPTION>
<OPTION value = "S-1">S-1</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "SAR-302503">SAR-302503</OPTION>
<OPTION value = "topiramate">topiramate</OPTION>
<OPTION value = "volasertib">volasertib</OPTION>
<OPTION value = "vorinostat">vorinostat</OPTION>
<OPTION value = "VST-1001">VST-1001</OPTION>
<OPTION value = "XL-184">XL-184</OPTION>
</SELECT>' where codelst_type = 'studyidtype' and codelst_subtyp = 'drugAgent1';

commit;

--10

--updating the code-item descriptions 
update er_codelst set codelst_desc = 'VP of Clinical Resaerch Adminsitration Determination', codelst_seq = 13 where codelst_type = 'rev_board' and codelst_subtyp = 'vp_cr';
update er_codelst set codelst_desc = 'Clinical Research Committee', codelst_seq = 2 where codelst_type = 'rev_board' and codelst_subtyp = 'crc_cc';
update er_codelst set codelst_desc = 'Institutional Review Board', codelst_seq = 6 where codelst_type = 'rev_board' and codelst_subtyp = 'irb_cc';
update er_codelst set codelst_desc = 'Ancillary Review Response', codelst_seq = 1 where codelst_type = 'rev_board' and codelst_subtyp = 'ancillary';
update er_codelst set codelst_desc = 'Medical Review Response', codelst_seq = 7 where codelst_type = 'rev_board' and codelst_subtyp = 'medical';
commit;

--Create or update code-items
DECLARE
  v_item_exists number := 0;  
BEGIN
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'rev_board' and codelst_desc = 'Data and Safety Monitoring Board';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'rev_board','rev_dataSafety','Data and Safety Monitoring Board','N',3, null);
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_dataSafety  inserted');
	else
		update er_codelst set codelst_subtyp = 'rev_dataSafety', codelst_seq = 3 where codelst_type = 'rev_board' and (codelst_desc = 'Data and Safety Monitoring Board');
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_dataSafety already exists');
	end if;
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'rev_board' and codelst_desc = 'EPAAC';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'rev_board','rev_EPAAC','EPAAC','N',4, null);
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_EPAAC  inserted');
	else
		update er_codelst set codelst_subtyp = 'rev_EPAAC', codelst_seq = 4 where codelst_type = 'rev_board' and (codelst_desc = 'EPAAC');
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_EPAAC already exists');
	end if;
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'rev_board' and codelst_desc = 'Exeuctive Session - IRB3';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'rev_board','rev_ExecSession','Exeuctive Session - IRB3','N',5, null);
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_ExecSession  inserted');
	else
		update er_codelst set codelst_subtyp = 'rev_ExecSession', codelst_seq = 5 where codelst_type = 'rev_board' and (codelst_desc = 'Exeuctive Session - IRB3');
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_ExecSession already exists');
	end if;
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'rev_board' and codelst_desc = 'Office of Protocol Research';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'rev_board','rev_ProtRes','Office of Protocol Research','N',8, null);
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_ProtRes  inserted');
	else
		update er_codelst set codelst_subtyp = 'rev_ProtRes', codelst_seq = 8 where codelst_type = 'rev_board' and (codelst_desc = 'Office of Protocol Research');
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_ProtRes already exists');
	end if;
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'rev_board' and codelst_desc = 'PI Initiated';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'rev_board','rev_PIInit','PI Initiated','N',9, null);
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_PIInit  inserted');
	else
		update er_codelst set codelst_subtyp = 'rev_PIInit', codelst_seq = 9 where codelst_type = 'rev_board' and (codelst_desc = 'PI Initiated');
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_PIInit already exists');
	end if;
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'rev_board' and codelst_desc = 'Psychosocial Behavior Health Services Research Committee';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'rev_board','rev_PBHSRC','Psychosocial Behavior Health Services Research Committee','N',10, null);
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_PBHSRC  inserted');
	else
		update er_codelst set codelst_subtyp = 'rev_PBHSRC', codelst_seq = 10 where codelst_type = 'rev_board' and (codelst_desc = 'Psychosocial Behavior Health Services Research Committee');
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_PBHSRC already exists');
	end if;
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'rev_board' and codelst_desc = 'Response To Compliance Audit/Monitoring Report';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'rev_board','rev_CompAudit','Response To Compliance Audit/Monitoring Report','N',11, null);
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_CompAudit  inserted');
	else
		update er_codelst set codelst_subtyp = 'rev_CompAudit', codelst_seq = 11 where codelst_type = 'rev_board' and (codelst_desc = 'Response To Compliance Audit/Monitoring Report');
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_CompAudit already exists');
	end if;
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'rev_board' and codelst_desc = 'Sponsor Request';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'rev_board','rev_SponsorReq','Sponsor Request','N',12, null);
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_SponsorReq  inserted');
	else
		update er_codelst set codelst_subtyp = 'rev_SponsorReq', codelst_seq = 12 where codelst_type = 'rev_board' and (codelst_desc = 'Sponsor Request');
		dbms_output.put_line('Code-list item Type:rev_board Subtype:rev_SponsorReq already exists');
	end if;
	commit;
end;
/

--Hiding code-items
update er_codelst set codelst_hide = 'Y' where codelst_type = 'rev_board' and codelst_subtyp not in ('vp_cr','crc_cc', 'irb_cc','ancillary','medical','rev_dataSafety',
'rev_EPAAC','rev_ExecSession', 'rev_ProtRes', 'rev_PIInit', 'rev_PBHSRC', 'rev_CompAudit', 'rev_SponsorReq');
commit;

/* LE build #09 Scripts */

--01

DECLARE
   CURSOR c_nonsystem_nologin IS SELECT * FROM ER_USER WHERE USR_TYPE = 'N' and USR_LOGNAME is null;
   r_er_user er_user%ROWTYPE;
BEGIN
    OPEN c_nonsystem_nologin;
    LOOP
        FETCH c_nonsystem_nologin INTO r_er_user;
        EXIT WHEN c_nonsystem_nologin%NOTFOUND;
        BEGIN
            --dbms_output.put_line(r_er_user.usr_lastname);
            update ER_USER set USR_LOGNAME = 'nonsys-'||r_er_user.PK_USER where PK_USER = r_er_user.PK_USER;
        END;
    END LOOP;
    COMMIT;
    CLOSE c_nonsystem_nologin;
END;
/

--02

DECLARE
  v_item_exists number := 0;  
BEGIN
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'phase' and CODELST_SUBTYP='pilotFeasibilty';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'phase','pilotFeasibilty','Feasability with N/A','N', 65, null);
		dbms_output.put_line('New row inserted');
	else
		dbms_output.put_line('New row inserted');
	end if;
	
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'studyscope' and CODELST_SUBTYP='studyscope_3';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'studyscope','studyscope_3','Multi-Center, MDACC lead','N', 3, null);
		dbms_output.put_line('New row inserted');
	else
		dbms_output.put_line('Row already exists');
	end if;
END;
/

update ER_CODELST set CODELST_CUSTOM_COL1='interventional' where codelst_type='studyPurpose' and 
CODELST_SUBTYP in ('diagnostic','prevention','screening','supportiveCare','treatment');
update ER_CODELST set CODELST_CUSTOM_COL1='interventional,correlative,observational' where codelst_type='studyPurpose' and 
CODELST_SUBTYP in ('healthSvcRsrch');
update ER_CODELST set CODELST_CUSTOM_COL1='correlative,observational' where codelst_type='studyPurpose' and 
CODELST_SUBTYP in ('epidemiologic','observational','other','outcomeTrial');
update ER_CODELST set CODELST_CUSTOM_COL1='correlative' where codelst_type='studyPurpose' and 
CODELST_SUBTYP in ('basicScience');

update ER_CODELST set CODELST_CUSTOM_COL1='irb' where codelst_type='studystat' and 
CODELST_SUBTYP in ('irb_add_info');

commit;

/* LE build #10 Scripts */

--01

update ERES.ER_CODELST set CODELST_CUSTOM_COL1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "YesComp">Yes -- Completely</OPTION>
<OPTION value = "YesPart">Yes -- Partially</OPTION>
<OPTION value = "NoSponsrDes">No -- Sponsor provided biostatistical design</OPTION>
<OPTION value = "NoProsStat">No prospective statistical design in this study</OPTION>
</SELECT>'
where CODELST_TYPE='studyidtype' and CODELST_SUBTYP='proDes2';

update ERES.ER_CODELST set CODELST_DESC='Risk Assessment Form 1' where
CODELST_TYPE='studyidtype' and CODELST_SUBTYP='riskAssmt1';
update ERES.ER_CODELST set CODELST_DESC='Risk Assessment Form 2' where
CODELST_TYPE='studyidtype' and CODELST_SUBTYP='riskAssmt2';
update ERES.ER_CODELST set CODELST_DESC='Risk Assessment Form 3' where
CODELST_TYPE='studyidtype' and CODELST_SUBTYP='riskAssmt3';
update ERES.ER_CODELST set CODELST_DESC='Risk Assessment Form 4' where
CODELST_TYPE='studyidtype' and CODELST_SUBTYP='riskAssmt4';
update ERES.ER_CODELST set CODELST_DESC='Risk Assessment Form 5' where
CODELST_TYPE='studyidtype' and CODELST_SUBTYP='riskAssmt5';

update ERES.ER_CODELST set CODELST_DESC='Multi-Center' where
CODELST_TYPE='studyscope' and CODELST_SUBTYP='std_cen_multi';
update ERES.ER_CODELST set CODELST_DESC='Multi-Center, MDACC lead' where
CODELST_TYPE='studyscope' and CODELST_SUBTYP='studyscope_3';

update ERES.ER_CODELST set CODELST_CUSTOM_COL1=
'<SELECT NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>' where
CODELST_TYPE='studyidtype' and CODELST_SUBTYP='biosafety3';

update ERES.ER_CODELST set CODELST_HIDE='Y' where
CODELST_TYPE='studySiteType' and CODELST_SUBTYP='responsible';

update ERES.ER_CODELST set CODELST_DESC='Feasibility with N/A' where
CODELST_TYPE='phase' and CODELST_SUBTYP='pilotFeasibilty';

update ER_CODELST set CODELST_DESC='Phase 5', CODELST_HIDE='Y' where
CODELST_TYPE='phase' and CODELST_SUBTYP='phase_5';

update ER_CODELST set CODELST_DESC='Phase 1/2', CODELST_HIDE='N' where
CODELST_TYPE='phase' and CODELST_SUBTYP='phaseI/II';

update ER_CODELST set CODELST_SUBTYP='phaseII/III', CODELST_DESC='Phase 2-3', 
CODELST_HIDE='N',  CODELST_SEQ = 30 where
CODELST_TYPE='phase' and CODELST_SUBTYP='phase_3';

commit;

/* LE build #11 Scripts */

--Missing entries inserts

--For seq #1

--invAntcpAEs

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'invAntcpAEs';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col,codelst_hide)
        values (seq_er_codelst.nextval, 'studyidtype', 'invAntcpAEs', 'If investigational, provide list of anticipated AEs', 'textarea','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--repSAEs

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyidtype' AND CODELST_SUBTYP = 'repSAEs';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_custom_col,codelst_hide)
        values (seq_er_codelst.nextval, 'studyidtype', 'repSAEs', 'Report of SAEs and Unanticipated Problems', 'textarea','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--blinded

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'blinding' AND CODELST_SUBTYP = 'blinded';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc)
        values (seq_er_codelst.nextval, 'blinding', 'blinded', 'Blinded');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--For seq #3

--dept_chair

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyvercat' AND CODELST_SUBTYP = 'dept_chair';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_hide)
        values (seq_er_codelst.nextval, 'studyvercat', 'dept_chair', 'Protocol Prioritization', 'N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--01

update ER_CODELST set CODELST_HIDE='N' where
CODELST_TYPE='randomization' and CODELST_SUBTYP='blckRndmztn';

update ER_CODELST set CODELST_DESC = 'If investigational, provide list of anticipated AEs' where
CODELST_TYPE='studyidtype' and CODELST_SUBTYP='invAntcpAEs';

update ER_CODELST set CODELST_DESC = 'Report of SAEs and Unanticipated Problems' where
CODELST_TYPE='studyidtype' and CODELST_SUBTYP='repSAEs';

update ER_CODELST set CODELST_HIDE = 'Y' where
CODELST_TYPE='blinding' and CODELST_SUBTYP='blinded';

declare
v_CodeExists number := 0;
begin
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'blinding' AND CODELST_SUBTYP = 'doubleBlind';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_hide, codelst_seq)
        values (seq_er_codelst.nextval, 'blinding', 'doubleBlind', 'Double-Blind', 'N', 2);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	commit;
end;
/

--02

-- Fixed Bug 21352 - Configuration: Study Drug: Agents: a value is missing.
--  Fixed option <OPTION value = "Rigosertib">Rigosertib</OPTION> for all drugAgents.

update er_codelst
set codelst_custom_col1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "abiraterone">abiraterone</OPTION>
<OPTION value = "abraxane">abraxane</OPTION>
<OPTION value = "ABT-199">ABT-199</OPTION>
<OPTION value = "ABT-888">ABT-888 (veliparib)</OPTION>
<OPTION value = "AC220">AC220</OPTION>
<OPTION value = "AG-013736">AG-013736</OPTION>
<OPTION value = "AMG-337">AMG-337</OPTION>
<OPTION value = "AP-24534">AP-24534</OPTION>
<OPTION value = "ARRY-614">ARRY-614</OPTION>
<OPTION value = "AZD-1208">AZD-1208</OPTION>
<OPTION value = "birinipant">birinipant</OPTION>
<OPTION value = "BKM-120">BKM-120</OPTION>
<OPTION value = "BMS-833923">BMS-833923</OPTION>
<OPTION value = "bortezomib">bortezomib</OPTION>
<OPTION value = "Bosutinib">Bosutinib</OPTION>
<OPTION value = "CC-223">CC-223</OPTION>
<OPTION value = "cediranib">cediranib (AZD-2171)</OPTION>
<OPTION value = "CMX-001">CMX-001</OPTION>
<OPTION value = "CPX-351">CPX-351</OPTION>
<OPTION value = "crenolanib">Crenolanib</OPTION>
<OPTION value = "CWP-23229">CWP-232291</OPTION>
<OPTION value = "dasatinib">dasatinib</OPTION>
<OPTION value = "denileukin">denileukin diftitox</OPTION>
<OPTION value = "E-75acetate">E-75 acetate</OPTION>
<OPTION value = "eflornithine">eflornithine</OPTION>
<OPTION value = "entinostat">Entinostat</OPTION>
<OPTION value = "erlotinib">erlotinib</OPTION>
<OPTION value = "everolimus">everolimus (RAD-001)</OPTION>
<OPTION value = "GDC-0941">GDC-0941</OPTION>
<OPTION value = "GS-1101">GS-1101</OPTION>
<OPTION value = "GSK-1120212">GSK-1120212</OPTION>
<OPTION value = "dabrafenib">GSK-2118436 (dabrafenib)</OPTION>
<OPTION value = "GSK-525762">GSK-525762</OPTION>
<OPTION value = "ibrutinib">ibrutinib</OPTION>
<OPTION value = "ICL-670">ICL-670</OPTION>
<OPTION value = "INCB-018424">INCB-018424</OPTION>
<OPTION value = "KB-004">KB-004</OPTION>
<OPTION value = "lacosamide">lacosamide</OPTION>
<OPTION value = "Lapatinib">Lapatinib</OPTION>
<OPTION value = "LDE-225">LDE-225</OPTION>
<OPTION value = "LEE-011">LEE-011</OPTION>
<OPTION value = "lenalidomide">lenalidomide(CC-5013)</OPTION>
<OPTION value = "LY-2784544">LY-2784544</OPTION>
<OPTION value = "LY-2940680">LY-2940680</OPTION>
<OPTION value = "LY-3009120">LY-3009120</OPTION>
<OPTION value = "masatinib">masatinib</OPTION>
<OPTION value = "MEK-162">MEK-162</OPTION>
<OPTION value = "MK-1775">MK-1775</OPTION>
<OPTION value = "MK-2206">MK-2206</OPTION>
<OPTION value = "MRX-34-">MRX-34</OPTION>
<OPTION value = "olaparib">olaparib</OPTION>
<OPTION value = "Omacetaxine">Omacetaxine</OPTION>
<OPTION value = "panobinostat (LBH-589)">panobinostat (LBH-589)</OPTION>
<OPTION value = "pazopanib">pazopanib</OPTION>
<OPTION value = "PF-0449913">PF-0449913</OPTION>
<OPTION value = "PKC-412">PKC-412</OPTION>
<OPTION value = "pomalidomide">pomalidomide</OPTION>
<OPTION value = "Ponatinib">Ponatinib</OPTION>
<OPTION value = "Ponatinib-AP">Ponatinib(AP-24534)</OPTION>
<OPTION value = "posaconazole">posaconazole</OPTION>
<OPTION value = "pracinostat">pracinostat</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "Rigosertib">Rigosertib</OPTION>
<OPTION value = "rucaparib">rucaparib</OPTION>
<OPTION value = "Ruxolitinib">ruxolitinib</OPTION>
<OPTION value = "S-1">S-1</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "SAR-302503">SAR-302503</OPTION>
<OPTION value = "topiramate">topiramate</OPTION>
<OPTION value = "volasertib">volasertib</OPTION>
<OPTION value = "vorinostat">vorinostat</OPTION>
<OPTION value = "VST-1001">VST-1001</OPTION>
<OPTION value = "XL-184">XL-184</OPTION>
</SELECT>'
where codelst_subtyp='drugAgent1';

update er_codelst
set codelst_custom_col1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "abiraterone">abiraterone</OPTION>
<OPTION value = "abraxane">abraxane</OPTION>
<OPTION value = "ABT-199">ABT-199</OPTION>
<OPTION value = "ABT-888">ABT-888 (veliparib)</OPTION>
<OPTION value = "AC220">AC220</OPTION>
<OPTION value = "AG-013736">AG-013736</OPTION>
<OPTION value = "AMG-337">AMG-337</OPTION>
<OPTION value = "AP-24534">AP-24534</OPTION>
<OPTION value = "ARRY-614">ARRY-614</OPTION>
<OPTION value = "AZD-1208">AZD-1208</OPTION>
<OPTION value = "birinipant">birinipant</OPTION>
<OPTION value = "BKM-120">BKM-120</OPTION>
<OPTION value = "BMS-833923">BMS-833923</OPTION>
<OPTION value = "bortezomib">bortezomib</OPTION>
<OPTION value = "Bosutinib">Bosutinib</OPTION>
<OPTION value = "CC-223">CC-223</OPTION>
<OPTION value = "cediranib">cediranib (AZD-2171)</OPTION>
<OPTION value = "CMX-001">CMX-001</OPTION>
<OPTION value = "CPX-351">CPX-351</OPTION>
<OPTION value = "crenolanib">Crenolanib</OPTION>
<OPTION value = "CWP-23229">CWP-232291</OPTION>
<OPTION value = "dasatinib">dasatinib</OPTION>
<OPTION value = "denileukin">denileukin diftitox</OPTION>
<OPTION value = "E-75acetate">E-75 acetate</OPTION>
<OPTION value = "eflornithine">eflornithine</OPTION>
<OPTION value = "entinostat">Entinostat</OPTION>
<OPTION value = "erlotinib">erlotinib</OPTION>
<OPTION value = "everolimus">everolimus (RAD-001)</OPTION>
<OPTION value = "GDC-0941">GDC-0941</OPTION>
<OPTION value = "GS-1101">GS-1101</OPTION>
<OPTION value = "GSK-1120212">GSK-1120212</OPTION>
<OPTION value = "dabrafenib">GSK-2118436 (dabrafenib)</OPTION>
<OPTION value = "GSK-525762">GSK-525762</OPTION>
<OPTION value = "ibrutinib">ibrutinib</OPTION>
<OPTION value = "ICL-670">ICL-670</OPTION>
<OPTION value = "INCB-018424">INCB-018424</OPTION>
<OPTION value = "KB-004">KB-004</OPTION>
<OPTION value = "lacosamide">lacosamide</OPTION>
<OPTION value = "Lapatinib">Lapatinib</OPTION>
<OPTION value = "LDE-225">LDE-225</OPTION>
<OPTION value = "LEE-011">LEE-011</OPTION>
<OPTION value = "lenalidomide">lenalidomide(CC-5013)</OPTION>
<OPTION value = "LY-2784544">LY-2784544</OPTION>
<OPTION value = "LY-2940680">LY-2940680</OPTION>
<OPTION value = "LY-3009120">LY-3009120</OPTION>
<OPTION value = "masatinib">masatinib</OPTION>
<OPTION value = "MEK-162">MEK-162</OPTION>
<OPTION value = "MK-1775">MK-1775</OPTION>
<OPTION value = "MK-2206">MK-2206</OPTION>
<OPTION value = "MRX-34-">MRX-34</OPTION>
<OPTION value = "olaparib">olaparib</OPTION>
<OPTION value = "Omacetaxine">Omacetaxine</OPTION>
<OPTION value = "pazopanib">pazopanib</OPTION>
<OPTION value = "PKC-412">PKC-412</OPTION>
<OPTION value = "pomalidomide">pomalidomide</OPTION>
<OPTION value = "Ponatinib">Ponatinib</OPTION>
<OPTION value = "Ponatinib-AP">Ponatinib(AP-24534)</OPTION>
<OPTION value = "posaconazole">posaconazole</OPTION>
<OPTION value = "pracinostat">pracinostat</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "Rigosertib">Rigosertib</OPTION>
<OPTION value = "rucaparib">rucaparib</OPTION>
<OPTION value = "Ruxolitinib">ruxolitinib</OPTION>
<OPTION value = "S-1">S-1</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "SAR-302503">SAR-302503</OPTION>
<OPTION value = "topiramate">topiramate</OPTION>
<OPTION value = "volasertib">volasertib</OPTION>
<OPTION value = "vorinostat">vorinostat</OPTION>
<OPTION value = "VST-1001">VST-1001</OPTION>
<OPTION value = "XL-184">XL-184</OPTION>
</SELECT>'
where codelst_subtyp='drugAgent2';

update er_codelst
set codelst_custom_col1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "abiraterone">abiraterone</OPTION>
<OPTION value = "abraxane">abraxane</OPTION>
<OPTION value = "ABT-199">ABT-199</OPTION>
<OPTION value = "ABT-888">ABT-888 (veliparib)</OPTION>
<OPTION value = "AC220">AC220</OPTION>
<OPTION value = "AG-013736">AG-013736</OPTION>
<OPTION value = "AMG-337">AMG-337</OPTION>
<OPTION value = "AP-24534">AP-24534</OPTION>
<OPTION value = "ARRY-614">ARRY-614</OPTION>
<OPTION value = "AZD-1208">AZD-1208</OPTION>
<OPTION value = "birinipant">birinipant</OPTION>
<OPTION value = "BKM-120">BKM-120</OPTION>
<OPTION value = "BMS-833923">BMS-833923</OPTION>
<OPTION value = "bortezomib">bortezomib</OPTION>
<OPTION value = "Bosutinib">Bosutinib</OPTION>
<OPTION value = "CC-223">CC-223</OPTION>
<OPTION value = "cediranib">cediranib (AZD-2171)</OPTION>
<OPTION value = "CMX-001">CMX-001</OPTION>
<OPTION value = "CPX-351">CPX-351</OPTION>
<OPTION value = "crenolanib">Crenolanib</OPTION>
<OPTION value = "CWP-23229">CWP-232291</OPTION>
<OPTION value = "dasatinib">dasatinib</OPTION>
<OPTION value = "denileukin">denileukin diftitox</OPTION>
<OPTION value = "E-75acetate">E-75 acetate</OPTION>
<OPTION value = "eflornithine">eflornithine</OPTION>
<OPTION value = "entinostat">Entinostat</OPTION>
<OPTION value = "erlotinib">erlotinib</OPTION>
<OPTION value = "everolimus">everolimus (RAD-001)</OPTION>
<OPTION value = "GDC-0941">GDC-0941</OPTION>
<OPTION value = "GS-1101">GS-1101</OPTION>
<OPTION value = "GSK-1120212">GSK-1120212</OPTION>
<OPTION value = "dabrafenib">GSK-2118436 (dabrafenib)</OPTION>
<OPTION value = "GSK-525762">GSK-525762</OPTION>
<OPTION value = "ibrutinib">ibrutinib</OPTION>
<OPTION value = "ICL-670">ICL-670</OPTION>
<OPTION value = "INCB-018424">INCB-018424</OPTION>
<OPTION value = "KB-004">KB-004</OPTION>
<OPTION value = "lacosamide">lacosamide</OPTION>
<OPTION value = "Lapatinib">Lapatinib</OPTION>
<OPTION value = "LDE-225">LDE-225</OPTION>
<OPTION value = "LEE-011">LEE-011</OPTION>
<OPTION value = "lenalidomide">lenalidomide(CC-5013)</OPTION>
<OPTION value = "LY-2784544">LY-2784544</OPTION>
<OPTION value = "LY-2940680">LY-2940680</OPTION>
<OPTION value = "LY-3009120">LY-3009120</OPTION>
<OPTION value = "masatinib">masatinib</OPTION>
<OPTION value = "MEK-162">MEK-162</OPTION>
<OPTION value = "MK-1775">MK-1775</OPTION>
<OPTION value = "MK-2206">MK-2206</OPTION>
<OPTION value = "MRX-34-">MRX-34</OPTION>
<OPTION value = "olaparib">olaparib</OPTION>
<OPTION value = "Omacetaxine">Omacetaxine</OPTION>
<OPTION value = "pazopanib">pazopanib</OPTION>
<OPTION value = "PKC-412">PKC-412</OPTION>
<OPTION value = "pomalidomide">pomalidomide</OPTION>
<OPTION value = "Ponatinib">Ponatinib</OPTION>
<OPTION value = "Ponatinib-AP">Ponatinib(AP-24534)</OPTION>
<OPTION value = "posaconazole">posaconazole</OPTION>
<OPTION value = "pracinostat">pracinostat</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "Rigosertib">Rigosertib</OPTION>
<OPTION value = "rucaparib">rucaparib</OPTION>
<OPTION value = "Ruxolitinib">ruxolitinib</OPTION>
<OPTION value = "S-1">S-1</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "SAR-302503">SAR-302503</OPTION>
<OPTION value = "topiramate">topiramate</OPTION>
<OPTION value = "volasertib">volasertib</OPTION>
<OPTION value = "vorinostat">vorinostat</OPTION>
<OPTION value = "VST-1001">VST-1001</OPTION>
<OPTION value = "XL-184">XL-184</OPTION>
</SELECT>'
where codelst_subtyp='drugAgent3';

update er_codelst
set codelst_custom_col1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "abiraterone">abiraterone</OPTION>
<OPTION value = "abraxane">abraxane</OPTION>
<OPTION value = "ABT-199">ABT-199</OPTION>
<OPTION value = "ABT-888">ABT-888 (veliparib)</OPTION>
<OPTION value = "AC220">AC220</OPTION>
<OPTION value = "AG-013736">AG-013736</OPTION>
<OPTION value = "AMG-337">AMG-337</OPTION>
<OPTION value = "AP-24534">AP-24534</OPTION>
<OPTION value = "ARRY-614">ARRY-614</OPTION>
<OPTION value = "AZD-1208">AZD-1208</OPTION>
<OPTION value = "birinipant">birinipant</OPTION>
<OPTION value = "BKM-120">BKM-120</OPTION>
<OPTION value = "BMS-833923">BMS-833923</OPTION>
<OPTION value = "bortezomib">bortezomib</OPTION>
<OPTION value = "Bosutinib">Bosutinib</OPTION>
<OPTION value = "CC-223">CC-223</OPTION>
<OPTION value = "cediranib">cediranib (AZD-2171)</OPTION>
<OPTION value = "CMX-001">CMX-001</OPTION>
<OPTION value = "CPX-351">CPX-351</OPTION>
<OPTION value = "crenolanib">Crenolanib</OPTION>
<OPTION value = "CWP-23229">CWP-232291</OPTION>
<OPTION value = "dasatinib">dasatinib</OPTION>
<OPTION value = "denileukin">denileukin diftitox</OPTION>
<OPTION value = "E-75acetate">E-75 acetate</OPTION>
<OPTION value = "eflornithine">eflornithine</OPTION>
<OPTION value = "entinostat">Entinostat</OPTION>
<OPTION value = "erlotinib">erlotinib</OPTION>
<OPTION value = "everolimus">everolimus (RAD-001)</OPTION>
<OPTION value = "GDC-0941">GDC-0941</OPTION>
<OPTION value = "GS-1101">GS-1101</OPTION>
<OPTION value = "GSK-1120212">GSK-1120212</OPTION>
<OPTION value = "dabrafenib">GSK-2118436 (dabrafenib)</OPTION>
<OPTION value = "GSK-525762">GSK-525762</OPTION>
<OPTION value = "ibrutinib">ibrutinib</OPTION>
<OPTION value = "ICL-670">ICL-670</OPTION>
<OPTION value = "INCB-018424">INCB-018424</OPTION>
<OPTION value = "KB-004">KB-004</OPTION>
<OPTION value = "lacosamide">lacosamide</OPTION>
<OPTION value = "Lapatinib">Lapatinib</OPTION>
<OPTION value = "LDE-225">LDE-225</OPTION>
<OPTION value = "LEE-011">LEE-011</OPTION>
<OPTION value = "lenalidomide">lenalidomide(CC-5013)</OPTION>
<OPTION value = "LY-2784544">LY-2784544</OPTION>
<OPTION value = "LY-2940680">LY-2940680</OPTION>
<OPTION value = "LY-3009120">LY-3009120</OPTION>
<OPTION value = "masatinib">masatinib</OPTION>
<OPTION value = "MEK-162">MEK-162</OPTION>
<OPTION value = "MK-1775">MK-1775</OPTION>
<OPTION value = "MK-2206">MK-2206</OPTION>
<OPTION value = "MRX-34-">MRX-34</OPTION>
<OPTION value = "olaparib">olaparib</OPTION>
<OPTION value = "Omacetaxine">Omacetaxine</OPTION>
<OPTION value = "pazopanib">pazopanib</OPTION>
<OPTION value = "PKC-412">PKC-412</OPTION>
<OPTION value = "pomalidomide">pomalidomide</OPTION>
<OPTION value = "Ponatinib">Ponatinib</OPTION>
<OPTION value = "Ponatinib-AP">Ponatinib(AP-24534)</OPTION>
<OPTION value = "posaconazole">posaconazole</OPTION>
<OPTION value = "pracinostat">pracinostat</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "Rigosertib">Rigosertib</OPTION>
<OPTION value = "rucaparib">rucaparib</OPTION>
<OPTION value = "Ruxolitinib">ruxolitinib</OPTION>
<OPTION value = "S-1">S-1</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "SAR-302503">SAR-302503</OPTION>
<OPTION value = "topiramate">topiramate</OPTION>
<OPTION value = "volasertib">volasertib</OPTION>
<OPTION value = "vorinostat">vorinostat</OPTION>
<OPTION value = "VST-1001">VST-1001</OPTION>
<OPTION value = "XL-184">XL-184</OPTION>
</SELECT>'
where codelst_subtyp='drugAgent4';

update er_codelst
set codelst_custom_col1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "abiraterone">abiraterone</OPTION>
<OPTION value = "abraxane">abraxane</OPTION>
<OPTION value = "ABT-199">ABT-199</OPTION>
<OPTION value = "ABT-888">ABT-888 (veliparib)</OPTION>
<OPTION value = "AC220">AC220</OPTION>
<OPTION value = "AG-013736">AG-013736</OPTION>
<OPTION value = "AMG-337">AMG-337</OPTION>
<OPTION value = "AP-24534">AP-24534</OPTION>
<OPTION value = "ARRY-614">ARRY-614</OPTION>
<OPTION value = "AZD-1208">AZD-1208</OPTION>
<OPTION value = "birinipant">birinipant</OPTION>
<OPTION value = "BKM-120">BKM-120</OPTION>
<OPTION value = "BMS-833923">BMS-833923</OPTION>
<OPTION value = "bortezomib">bortezomib</OPTION>
<OPTION value = "Bosutinib">Bosutinib</OPTION>
<OPTION value = "CC-223">CC-223</OPTION>
<OPTION value = "cediranib">cediranib (AZD-2171)</OPTION>
<OPTION value = "CMX-001">CMX-001</OPTION>
<OPTION value = "CPX-351">CPX-351</OPTION>
<OPTION value = "crenolanib">Crenolanib</OPTION>
<OPTION value = "CWP-23229">CWP-232291</OPTION>
<OPTION value = "dasatinib">dasatinib</OPTION>
<OPTION value = "denileukin">denileukin diftitox</OPTION>
<OPTION value = "E-75acetate">E-75 acetate</OPTION>
<OPTION value = "eflornithine">eflornithine</OPTION>
<OPTION value = "entinostat">Entinostat</OPTION>
<OPTION value = "erlotinib">erlotinib</OPTION>
<OPTION value = "everolimus">everolimus (RAD-001)</OPTION>
<OPTION value = "GDC-0941">GDC-0941</OPTION>
<OPTION value = "GS-1101">GS-1101</OPTION>
<OPTION value = "GSK-1120212">GSK-1120212</OPTION>
<OPTION value = "dabrafenib">GSK-2118436 (dabrafenib)</OPTION>
<OPTION value = "GSK-525762">GSK-525762</OPTION>
<OPTION value = "ibrutinib">ibrutinib</OPTION>
<OPTION value = "ICL-670">ICL-670</OPTION>
<OPTION value = "INCB-018424">INCB-018424</OPTION>
<OPTION value = "KB-004">KB-004</OPTION>
<OPTION value = "lacosamide">lacosamide</OPTION>
<OPTION value = "Lapatinib">Lapatinib</OPTION>
<OPTION value = "LDE-225">LDE-225</OPTION>
<OPTION value = "LEE-011">LEE-011</OPTION>
<OPTION value = "lenalidomide">lenalidomide(CC-5013)</OPTION>
<OPTION value = "LY-2784544">LY-2784544</OPTION>
<OPTION value = "LY-2940680">LY-2940680</OPTION>
<OPTION value = "LY-3009120">LY-3009120</OPTION>
<OPTION value = "masatinib">masatinib</OPTION>
<OPTION value = "MEK-162">MEK-162</OPTION>
<OPTION value = "MK-1775">MK-1775</OPTION>
<OPTION value = "MK-2206">MK-2206</OPTION>
<OPTION value = "MRX-34-">MRX-34</OPTION>
<OPTION value = "olaparib">olaparib</OPTION>
<OPTION value = "Omacetaxine">Omacetaxine</OPTION>
<OPTION value = "pazopanib">pazopanib</OPTION>
<OPTION value = "PKC-412">PKC-412</OPTION>
<OPTION value = "pomalidomide">pomalidomide</OPTION>
<OPTION value = "Ponatinib">Ponatinib</OPTION>
<OPTION value = "Ponatinib-AP">Ponatinib(AP-24534)</OPTION>
<OPTION value = "posaconazole">posaconazole</OPTION>
<OPTION value = "pracinostat">pracinostat</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "Rigosertib">Rigosertib</OPTION>
<OPTION value = "rucaparib">rucaparib</OPTION>
<OPTION value = "Ruxolitinib">ruxolitinib</OPTION>
<OPTION value = "S-1">S-1</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "SAR-302503">SAR-302503</OPTION>
<OPTION value = "topiramate">topiramate</OPTION>
<OPTION value = "volasertib">volasertib</OPTION>
<OPTION value = "vorinostat">vorinostat</OPTION>
<OPTION value = "VST-1001">VST-1001</OPTION>
<OPTION value = "XL-184">XL-184</OPTION>
</SELECT>'
where codelst_subtyp='drugAgent5';

update er_codelst
set codelst_custom_col1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "abiraterone">abiraterone</OPTION>
<OPTION value = "abraxane">abraxane</OPTION>
<OPTION value = "ABT-199">ABT-199</OPTION>
<OPTION value = "ABT-888">ABT-888 (veliparib)</OPTION>
<OPTION value = "AC220">AC220</OPTION>
<OPTION value = "AG-013736">AG-013736</OPTION>
<OPTION value = "AMG-337">AMG-337</OPTION>
<OPTION value = "AP-24534">AP-24534</OPTION>
<OPTION value = "ARRY-614">ARRY-614</OPTION>
<OPTION value = "AZD-1208">AZD-1208</OPTION>
<OPTION value = "birinipant">birinipant</OPTION>
<OPTION value = "BKM-120">BKM-120</OPTION>
<OPTION value = "BMS-833923">BMS-833923</OPTION>
<OPTION value = "bortezomib">bortezomib</OPTION>
<OPTION value = "Bosutinib">Bosutinib</OPTION>
<OPTION value = "CC-223">CC-223</OPTION>
<OPTION value = "cediranib">cediranib (AZD-2171)</OPTION>
<OPTION value = "CMX-001">CMX-001</OPTION>
<OPTION value = "CPX-351">CPX-351</OPTION>
<OPTION value = "crenolanib">Crenolanib</OPTION>
<OPTION value = "CWP-23229">CWP-232291</OPTION>
<OPTION value = "dasatinib">dasatinib</OPTION>
<OPTION value = "denileukin">denileukin diftitox</OPTION>
<OPTION value = "E-75acetate">E-75 acetate</OPTION>
<OPTION value = "eflornithine">eflornithine</OPTION>
<OPTION value = "entinostat">Entinostat</OPTION>
<OPTION value = "erlotinib">erlotinib</OPTION>
<OPTION value = "everolimus">everolimus (RAD-001)</OPTION>
<OPTION value = "GDC-0941">GDC-0941</OPTION>
<OPTION value = "GS-1101">GS-1101</OPTION>
<OPTION value = "GSK-1120212">GSK-1120212</OPTION>
<OPTION value = "dabrafenib">GSK-2118436 (dabrafenib)</OPTION>
<OPTION value = "GSK-525762">GSK-525762</OPTION>
<OPTION value = "ibrutinib">ibrutinib</OPTION>
<OPTION value = "ICL-670">ICL-670</OPTION>
<OPTION value = "INCB-018424">INCB-018424</OPTION>
<OPTION value = "KB-004">KB-004</OPTION>
<OPTION value = "lacosamide">lacosamide</OPTION>
<OPTION value = "Lapatinib">Lapatinib</OPTION>
<OPTION value = "LDE-225">LDE-225</OPTION>
<OPTION value = "LEE-011">LEE-011</OPTION>
<OPTION value = "lenalidomide">lenalidomide(CC-5013)</OPTION>
<OPTION value = "LY-2784544">LY-2784544</OPTION>
<OPTION value = "LY-2940680">LY-2940680</OPTION>
<OPTION value = "LY-3009120">LY-3009120</OPTION>
<OPTION value = "masatinib">masatinib</OPTION>
<OPTION value = "MEK-162">MEK-162</OPTION>
<OPTION value = "MK-1775">MK-1775</OPTION>
<OPTION value = "MK-2206">MK-2206</OPTION>
<OPTION value = "MRX-34-">MRX-34</OPTION>
<OPTION value = "olaparib">olaparib</OPTION>
<OPTION value = "Omacetaxine">Omacetaxine</OPTION>
<OPTION value = "pazopanib">pazopanib</OPTION>
<OPTION value = "PKC-412">PKC-412</OPTION>
<OPTION value = "pomalidomide">pomalidomide</OPTION>
<OPTION value = "Ponatinib">Ponatinib</OPTION>
<OPTION value = "Ponatinib-AP">Ponatinib(AP-24534)</OPTION>
<OPTION value = "posaconazole">posaconazole</OPTION>
<OPTION value = "pracinostat">pracinostat</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "Rigosertib">Rigosertib</OPTION>
<OPTION value = "rucaparib">rucaparib</OPTION>
<OPTION value = "Ruxolitinib">ruxolitinib</OPTION>
<OPTION value = "S-1">S-1</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "SAR-302503">SAR-302503</OPTION>
<OPTION value = "topiramate">topiramate</OPTION>
<OPTION value = "volasertib">volasertib</OPTION>
<OPTION value = "vorinostat">vorinostat</OPTION>
<OPTION value = "VST-1001">VST-1001</OPTION>
<OPTION value = "XL-184">XL-184</OPTION>
</SELECT>'
where codelst_subtyp='drugAgent6';

update er_codelst
set codelst_custom_col1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "abiraterone">abiraterone</OPTION>
<OPTION value = "abraxane">abraxane</OPTION>
<OPTION value = "ABT-199">ABT-199</OPTION>
<OPTION value = "ABT-888">ABT-888 (veliparib)</OPTION>
<OPTION value = "AC220">AC220</OPTION>
<OPTION value = "AG-013736">AG-013736</OPTION>
<OPTION value = "AMG-337">AMG-337</OPTION>
<OPTION value = "AP-24534">AP-24534</OPTION>
<OPTION value = "ARRY-614">ARRY-614</OPTION>
<OPTION value = "AZD-1208">AZD-1208</OPTION>
<OPTION value = "birinipant">birinipant</OPTION>
<OPTION value = "BKM-120">BKM-120</OPTION>
<OPTION value = "BMS-833923">BMS-833923</OPTION>
<OPTION value = "bortezomib">bortezomib</OPTION>
<OPTION value = "Bosutinib">Bosutinib</OPTION>
<OPTION value = "CC-223">CC-223</OPTION>
<OPTION value = "cediranib">cediranib (AZD-2171)</OPTION>
<OPTION value = "CMX-001">CMX-001</OPTION>
<OPTION value = "CPX-351">CPX-351</OPTION>
<OPTION value = "crenolanib">Crenolanib</OPTION>
<OPTION value = "CWP-23229">CWP-232291</OPTION>
<OPTION value = "dasatinib">dasatinib</OPTION>
<OPTION value = "denileukin">denileukin diftitox</OPTION>
<OPTION value = "E-75acetate">E-75 acetate</OPTION>
<OPTION value = "eflornithine">eflornithine</OPTION>
<OPTION value = "entinostat">Entinostat</OPTION>
<OPTION value = "erlotinib">erlotinib</OPTION>
<OPTION value = "everolimus">everolimus (RAD-001)</OPTION>
<OPTION value = "GDC-0941">GDC-0941</OPTION>
<OPTION value = "GS-1101">GS-1101</OPTION>
<OPTION value = "GSK-1120212">GSK-1120212</OPTION>
<OPTION value = "dabrafenib">GSK-2118436 (dabrafenib)</OPTION>
<OPTION value = "GSK-525762">GSK-525762</OPTION>
<OPTION value = "ibrutinib">ibrutinib</OPTION>
<OPTION value = "ICL-670">ICL-670</OPTION>
<OPTION value = "INCB-018424">INCB-018424</OPTION>
<OPTION value = "KB-004">KB-004</OPTION>
<OPTION value = "lacosamide">lacosamide</OPTION>
<OPTION value = "Lapatinib">Lapatinib</OPTION>
<OPTION value = "LDE-225">LDE-225</OPTION>
<OPTION value = "LEE-011">LEE-011</OPTION>
<OPTION value = "lenalidomide">lenalidomide(CC-5013)</OPTION>
<OPTION value = "LY-2784544">LY-2784544</OPTION>
<OPTION value = "LY-2940680">LY-2940680</OPTION>
<OPTION value = "LY-3009120">LY-3009120</OPTION>
<OPTION value = "masatinib">masatinib</OPTION>
<OPTION value = "MEK-162">MEK-162</OPTION>
<OPTION value = "MK-1775">MK-1775</OPTION>
<OPTION value = "MK-2206">MK-2206</OPTION>
<OPTION value = "MRX-34-">MRX-34</OPTION>
<OPTION value = "olaparib">olaparib</OPTION>
<OPTION value = "Omacetaxine">Omacetaxine</OPTION>
<OPTION value = "pazopanib">pazopanib</OPTION>
<OPTION value = "PKC-412">PKC-412</OPTION>
<OPTION value = "pomalidomide">pomalidomide</OPTION>
<OPTION value = "Ponatinib">Ponatinib</OPTION>
<OPTION value = "Ponatinib-AP">Ponatinib(AP-24534)</OPTION>
<OPTION value = "posaconazole">posaconazole</OPTION>
<OPTION value = "pracinostat">pracinostat</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "Rigosertib">Rigosertib</OPTION>
<OPTION value = "rucaparib">rucaparib</OPTION>
<OPTION value = "Ruxolitinib">ruxolitinib</OPTION>
<OPTION value = "S-1">S-1</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "SAR-302503">SAR-302503</OPTION>
<OPTION value = "topiramate">topiramate</OPTION>
<OPTION value = "volasertib">volasertib</OPTION>
<OPTION value = "vorinostat">vorinostat</OPTION>
<OPTION value = "VST-1001">VST-1001</OPTION>
<OPTION value = "XL-184">XL-184</OPTION>
</SELECT>'
where codelst_subtyp='drugAgent7';

update er_codelst
set codelst_custom_col1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "abiraterone">abiraterone</OPTION>
<OPTION value = "abraxane">abraxane</OPTION>
<OPTION value = "ABT-199">ABT-199</OPTION>
<OPTION value = "ABT-888">ABT-888 (veliparib)</OPTION>
<OPTION value = "AC220">AC220</OPTION>
<OPTION value = "AG-013736">AG-013736</OPTION>
<OPTION value = "AMG-337">AMG-337</OPTION>
<OPTION value = "AP-24534">AP-24534</OPTION>
<OPTION value = "ARRY-614">ARRY-614</OPTION>
<OPTION value = "AZD-1208">AZD-1208</OPTION>
<OPTION value = "birinipant">birinipant</OPTION>
<OPTION value = "BKM-120">BKM-120</OPTION>
<OPTION value = "BMS-833923">BMS-833923</OPTION>
<OPTION value = "bortezomib">bortezomib</OPTION>
<OPTION value = "Bosutinib">Bosutinib</OPTION>
<OPTION value = "CC-223">CC-223</OPTION>
<OPTION value = "cediranib">cediranib (AZD-2171)</OPTION>
<OPTION value = "CMX-001">CMX-001</OPTION>
<OPTION value = "CPX-351">CPX-351</OPTION>
<OPTION value = "crenolanib">Crenolanib</OPTION>
<OPTION value = "CWP-23229">CWP-232291</OPTION>
<OPTION value = "dasatinib">dasatinib</OPTION>
<OPTION value = "denileukin">denileukin diftitox</OPTION>
<OPTION value = "E-75acetate">E-75 acetate</OPTION>
<OPTION value = "eflornithine">eflornithine</OPTION>
<OPTION value = "entinostat">Entinostat</OPTION>
<OPTION value = "erlotinib">erlotinib</OPTION>
<OPTION value = "everolimus">everolimus (RAD-001)</OPTION>
<OPTION value = "GDC-0941">GDC-0941</OPTION>
<OPTION value = "GS-1101">GS-1101</OPTION>
<OPTION value = "GSK-1120212">GSK-1120212</OPTION>
<OPTION value = "dabrafenib">GSK-2118436 (dabrafenib)</OPTION>
<OPTION value = "GSK-525762">GSK-525762</OPTION>
<OPTION value = "ibrutinib">ibrutinib</OPTION>
<OPTION value = "ICL-670">ICL-670</OPTION>
<OPTION value = "INCB-018424">INCB-018424</OPTION>
<OPTION value = "KB-004">KB-004</OPTION>
<OPTION value = "lacosamide">lacosamide</OPTION>
<OPTION value = "Lapatinib">Lapatinib</OPTION>
<OPTION value = "LDE-225">LDE-225</OPTION>
<OPTION value = "LEE-011">LEE-011</OPTION>
<OPTION value = "lenalidomide">lenalidomide(CC-5013)</OPTION>
<OPTION value = "LY-2784544">LY-2784544</OPTION>
<OPTION value = "LY-2940680">LY-2940680</OPTION>
<OPTION value = "LY-3009120">LY-3009120</OPTION>
<OPTION value = "masatinib">masatinib</OPTION>
<OPTION value = "MEK-162">MEK-162</OPTION>
<OPTION value = "MK-1775">MK-1775</OPTION>
<OPTION value = "MK-2206">MK-2206</OPTION>
<OPTION value = "MRX-34-">MRX-34</OPTION>
<OPTION value = "olaparib">olaparib</OPTION>
<OPTION value = "Omacetaxine">Omacetaxine</OPTION>
<OPTION value = "pazopanib">pazopanib</OPTION>
<OPTION value = "PKC-412">PKC-412</OPTION>
<OPTION value = "pomalidomide">pomalidomide</OPTION>
<OPTION value = "Ponatinib">Ponatinib</OPTION>
<OPTION value = "Ponatinib-AP">Ponatinib(AP-24534)</OPTION>
<OPTION value = "posaconazole">posaconazole</OPTION>
<OPTION value = "pracinostat">pracinostat</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "Rigosertib">Rigosertib</OPTION>
<OPTION value = "rucaparib">rucaparib</OPTION>
<OPTION value = "Ruxolitinib">ruxolitinib</OPTION>
<OPTION value = "S-1">S-1</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "SAR-302503">SAR-302503</OPTION>
<OPTION value = "topiramate">topiramate</OPTION>
<OPTION value = "volasertib">volasertib</OPTION>
<OPTION value = "vorinostat">vorinostat</OPTION>
<OPTION value = "VST-1001">VST-1001</OPTION>
<OPTION value = "XL-184">XL-184</OPTION>
</SELECT>'
where codelst_subtyp='drugAgent8';

update er_codelst
set codelst_custom_col1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "abiraterone">abiraterone</OPTION>
<OPTION value = "abraxane">abraxane</OPTION>
<OPTION value = "ABT-199">ABT-199</OPTION>
<OPTION value = "ABT-888">ABT-888 (veliparib)</OPTION>
<OPTION value = "AC220">AC220</OPTION>
<OPTION value = "AG-013736">AG-013736</OPTION>
<OPTION value = "AMG-337">AMG-337</OPTION>
<OPTION value = "AP-24534">AP-24534</OPTION>
<OPTION value = "ARRY-614">ARRY-614</OPTION>
<OPTION value = "AZD-1208">AZD-1208</OPTION>
<OPTION value = "birinipant">birinipant</OPTION>
<OPTION value = "BKM-120">BKM-120</OPTION>
<OPTION value = "BMS-833923">BMS-833923</OPTION>
<OPTION value = "bortezomib">bortezomib</OPTION>
<OPTION value = "Bosutinib">Bosutinib</OPTION>
<OPTION value = "CC-223">CC-223</OPTION>
<OPTION value = "cediranib">cediranib (AZD-2171)</OPTION>
<OPTION value = "CMX-001">CMX-001</OPTION>
<OPTION value = "CPX-351">CPX-351</OPTION>
<OPTION value = "crenolanib">Crenolanib</OPTION>
<OPTION value = "CWP-23229">CWP-232291</OPTION>
<OPTION value = "dasatinib">dasatinib</OPTION>
<OPTION value = "denileukin">denileukin diftitox</OPTION>
<OPTION value = "E-75acetate">E-75 acetate</OPTION>
<OPTION value = "eflornithine">eflornithine</OPTION>
<OPTION value = "entinostat">Entinostat</OPTION>
<OPTION value = "erlotinib">erlotinib</OPTION>
<OPTION value = "everolimus">everolimus (RAD-001)</OPTION>
<OPTION value = "GDC-0941">GDC-0941</OPTION>
<OPTION value = "GS-1101">GS-1101</OPTION>
<OPTION value = "GSK-1120212">GSK-1120212</OPTION>
<OPTION value = "dabrafenib">GSK-2118436 (dabrafenib)</OPTION>
<OPTION value = "GSK-525762">GSK-525762</OPTION>
<OPTION value = "ibrutinib">ibrutinib</OPTION>
<OPTION value = "ICL-670">ICL-670</OPTION>
<OPTION value = "INCB-018424">INCB-018424</OPTION>
<OPTION value = "KB-004">KB-004</OPTION>
<OPTION value = "lacosamide">lacosamide</OPTION>
<OPTION value = "Lapatinib">Lapatinib</OPTION>
<OPTION value = "LDE-225">LDE-225</OPTION>
<OPTION value = "LEE-011">LEE-011</OPTION>
<OPTION value = "lenalidomide">lenalidomide(CC-5013)</OPTION>
<OPTION value = "LY-2784544">LY-2784544</OPTION>
<OPTION value = "LY-2940680">LY-2940680</OPTION>
<OPTION value = "LY-3009120">LY-3009120</OPTION>
<OPTION value = "masatinib">masatinib</OPTION>
<OPTION value = "MEK-162">MEK-162</OPTION>
<OPTION value = "MK-1775">MK-1775</OPTION>
<OPTION value = "MK-2206">MK-2206</OPTION>
<OPTION value = "MRX-34-">MRX-34</OPTION>
<OPTION value = "olaparib">olaparib</OPTION>
<OPTION value = "Omacetaxine">Omacetaxine</OPTION>
<OPTION value = "pazopanib">pazopanib</OPTION>
<OPTION value = "PKC-412">PKC-412</OPTION>
<OPTION value = "pomalidomide">pomalidomide</OPTION>
<OPTION value = "Ponatinib">Ponatinib</OPTION>
<OPTION value = "Ponatinib-AP">Ponatinib(AP-24534)</OPTION>
<OPTION value = "posaconazole">posaconazole</OPTION>
<OPTION value = "pracinostat">pracinostat</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "Rigosertib">Rigosertib</OPTION>
<OPTION value = "rucaparib">rucaparib</OPTION>
<OPTION value = "Ruxolitinib">ruxolitinib</OPTION>
<OPTION value = "S-1">S-1</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "SAR-302503">SAR-302503</OPTION>
<OPTION value = "topiramate">topiramate</OPTION>
<OPTION value = "volasertib">volasertib</OPTION>
<OPTION value = "vorinostat">vorinostat</OPTION>
<OPTION value = "VST-1001">VST-1001</OPTION>
<OPTION value = "XL-184">XL-184</OPTION>
</SELECT>'
where codelst_subtyp='drugAgent9';

update er_codelst
set codelst_custom_col1='<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "abiraterone">abiraterone</OPTION>
<OPTION value = "abraxane">abraxane</OPTION>
<OPTION value = "ABT-199">ABT-199</OPTION>
<OPTION value = "ABT-888">ABT-888 (veliparib)</OPTION>
<OPTION value = "AC220">AC220</OPTION>
<OPTION value = "AG-013736">AG-013736</OPTION>
<OPTION value = "AMG-337">AMG-337</OPTION>
<OPTION value = "AP-24534">AP-24534</OPTION>
<OPTION value = "ARRY-614">ARRY-614</OPTION>
<OPTION value = "AZD-1208">AZD-1208</OPTION>
<OPTION value = "birinipant">birinipant</OPTION>
<OPTION value = "BKM-120">BKM-120</OPTION>
<OPTION value = "BMS-833923">BMS-833923</OPTION>
<OPTION value = "bortezomib">bortezomib</OPTION>
<OPTION value = "Bosutinib">Bosutinib</OPTION>
<OPTION value = "CC-223">CC-223</OPTION>
<OPTION value = "cediranib">cediranib (AZD-2171)</OPTION>
<OPTION value = "CMX-001">CMX-001</OPTION>
<OPTION value = "CPX-351">CPX-351</OPTION>
<OPTION value = "crenolanib">Crenolanib</OPTION>
<OPTION value = "CWP-23229">CWP-232291</OPTION>
<OPTION value = "dasatinib">dasatinib</OPTION>
<OPTION value = "denileukin">denileukin diftitox</OPTION>
<OPTION value = "E-75acetate">E-75 acetate</OPTION>
<OPTION value = "eflornithine">eflornithine</OPTION>
<OPTION value = "entinostat">Entinostat</OPTION>
<OPTION value = "erlotinib">erlotinib</OPTION>
<OPTION value = "everolimus">everolimus (RAD-001)</OPTION>
<OPTION value = "GDC-0941">GDC-0941</OPTION>
<OPTION value = "GS-1101">GS-1101</OPTION>
<OPTION value = "GSK-1120212">GSK-1120212</OPTION>
<OPTION value = "dabrafenib">GSK-2118436 (dabrafenib)</OPTION>
<OPTION value = "GSK-525762">GSK-525762</OPTION>
<OPTION value = "ibrutinib">ibrutinib</OPTION>
<OPTION value = "ICL-670">ICL-670</OPTION>
<OPTION value = "INCB-018424">INCB-018424</OPTION>
<OPTION value = "KB-004">KB-004</OPTION>
<OPTION value = "lacosamide">lacosamide</OPTION>
<OPTION value = "Lapatinib">Lapatinib</OPTION>
<OPTION value = "LDE-225">LDE-225</OPTION>
<OPTION value = "LEE-011">LEE-011</OPTION>
<OPTION value = "lenalidomide">lenalidomide(CC-5013)</OPTION>
<OPTION value = "LY-2784544">LY-2784544</OPTION>
<OPTION value = "LY-2940680">LY-2940680</OPTION>
<OPTION value = "LY-3009120">LY-3009120</OPTION>
<OPTION value = "masatinib">masatinib</OPTION>
<OPTION value = "MEK-162">MEK-162</OPTION>
<OPTION value = "MK-1775">MK-1775</OPTION>
<OPTION value = "MK-2206">MK-2206</OPTION>
<OPTION value = "MRX-34-">MRX-34</OPTION>
<OPTION value = "olaparib">olaparib</OPTION>
<OPTION value = "Omacetaxine">Omacetaxine</OPTION>
<OPTION value = "pazopanib">pazopanib</OPTION>
<OPTION value = "PKC-412">PKC-412</OPTION>
<OPTION value = "pomalidomide">pomalidomide</OPTION>
<OPTION value = "Ponatinib">Ponatinib</OPTION>
<OPTION value = "Ponatinib-AP">Ponatinib(AP-24534)</OPTION>
<OPTION value = "posaconazole">posaconazole</OPTION>
<OPTION value = "pracinostat">pracinostat</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "Rigosertib">Rigosertib</OPTION>
<OPTION value = "rucaparib">rucaparib</OPTION>
<OPTION value = "Ruxolitinib">ruxolitinib</OPTION>
<OPTION value = "S-1">S-1</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "SAR-302503">SAR-302503</OPTION>
<OPTION value = "topiramate">topiramate</OPTION>
<OPTION value = "volasertib">volasertib</OPTION>
<OPTION value = "vorinostat">vorinostat</OPTION>
<OPTION value = "VST-1001">VST-1001</OPTION>
<OPTION value = "XL-184">XL-184</OPTION>
</SELECT>'
where codelst_subtyp='drugAgent10';

commit;

--03

-- Fixed Bug # 21272

--Dropdown values should be:  
-- Old ones:
-- Protocol
-- Investigational Brochure
-- Protocol Prioritization
--Updates: Jerd Phichitkul 2015-02-12 13:21:59 PST (Currently 'Department Chair Review' category is passing the document to Click
--instead of the 'Protocol Prioritization' category. )
--1. Hide 'Protocol Prioritization' category.
--2. Rename 'Department Chair Review' to 'Protocol Prioritization'.

update er_codelst
set codelst_hide='Y'
where codelst_type='studyvercat'
and codelst_subtyp='prot_priority';

update er_codelst
set codelst_hide='Y'
where codelst_type='studyvercat'
and codelst_subtyp='inf_consent';

--Hiding this after Umer's confirmation
update er_codelst
set codelst_hide='Y'
where codelst_type='studyvercat'
and codelst_subtyp='studyvercat_7';

update er_codelst
set codelst_desc='Protocol Prioritization', codelst_seq=8
where codelst_type='studyvercat'
and codelst_subtyp='dept_chair';


update er_codelst
set codelst_seq= 1
where codelst_type='studyvercat'
and codelst_subtyp='protocol';

update er_codelst
set codelst_seq= 7
where codelst_type='studyvercat'
and codelst_subtyp='inv_brochure';


commit;

--04

--Dropdown values should be:  
-- Old ones:
-- Protocol
-- Investigational Brochure
-- Protocol Prioritization

-- New ones
-- External Collaborator Signature
-- Toxicity Form
-- Sponsor Provided Files
-- Other Documents

set define off;
set serveroutput on;

declare
v_CodeExists number := 0;
begin
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyvercat' AND CODELST_SUBTYP = 'collab_sign';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_hide, codelst_seq)
        values (seq_er_codelst.nextval, 'studyvercat', 'collab_sign', 'External Collaborator Signature', 'N', 2);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyvercat' AND CODELST_SUBTYP = 'other_docs';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_hide, codelst_seq)
        values (seq_er_codelst.nextval, 'studyvercat', 'other_docs', 'Other Documents', 'N', 6);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyvercat' AND CODELST_SUBTYP = 'toxic_form';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_hide, codelst_seq)
        values (seq_er_codelst.nextval, 'studyvercat', 'toxic_form', 'Toxicity Form', 'N', 3);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyvercat' AND CODELST_SUBTYP = 'spons_file';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_hide, codelst_seq)
        values (seq_er_codelst.nextval, 'studyvercat', 'spons_file', 'Sponsor Provided Files', 'N', 4);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
	
COMMIT;
END;
/

--06

update er_codelst set CODELST_CUSTOM_COL1 = '{chkArray:[
{data: "option1", display:"Bypass CRC/PBHSRC Review<br>This study is NCI-sponsored, CTEP-approved, and/or NCI cooperative group-approved trial, or peer-reviwed by an NIH-approved mechanism and IRB-approved at an NCI designated cancer center. ---- Please attach appropriate documentation (e.g. NCI-approval letter or IRB approval memo) to support the request to bypass CRC/PBHSRC review.----"},
{data: "option2", display:"Expedited Biostatistical Review<br>This study is an industry-sponsored, multi-center clinical trial already activated at an NCI designated cancer center and/or has been approved by the Office of the Vice Provost for Fast-Track designation? ----- Please attached appropriate documentation (e.g. Sponsor memorandum,ClinicalTrial.gov listing verifying activation at an NCI-designated cancer center, OVP Fast Track Approval memorandum) to support the request for expedited biostatistical review.-----"}
]}' where codelst_type = 'studyidtype' and codelst_subtyp = 'expdBiostat';

commit;

/* LE build #12 Scripts */

--Missing entries insert

--For seq #1

--role_20

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'role' AND CODELST_SUBTYP = 'role_20';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_hide)
        values (seq_er_codelst.nextval, 'role', 'role_20', 'Co-PI', 'N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--01

update ER_CODELST set CODELST_SUBTYP='CoPi' where
CODELST_TYPE='role' and CODELST_SUBTYP='role_20' and CODELST_DESC='Co-PI';

commit;

/* LE build #13 Scripts */

--01

update ER_CODELST set CODELST_HIDE='Y' where
CODELST_TYPE='studyvercat' and CODELST_SUBTYP='studyvercat_5';

update ER_CODELST set CODELST_HIDE='Y' where
CODELST_TYPE='studyvercat' and CODELST_SUBTYP='studyvercat_6';

commit;

--02

delete ER_STUDY_INDIDE where nvl(FK_STUDY, 0) = 0;

commit;

--03

update er_codelst set codelst_desc = 'COEUS Number' where codelst_type = 'studyidtype' and codelst_subtyp = 'grant';

update er_codelst set codelst_desc = 'If gene, use www.genecard.org to pull name from list' where codelst_type = 'studyidtype' and codelst_subtyp = 'ifGeneList';

commit;

--NCI program codes

Update er_codelst set codelst_seq = 1 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'CCR';
Update er_codelst set codelst_seq = 3 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'CDP';
Update er_codelst set codelst_seq = 4 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'CIP';
Update er_codelst set codelst_seq = 5 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'CTEP';
Update er_codelst set codelst_seq = 6 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'DCB';
Update er_codelst set codelst_seq = 7 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'DCCPS';
Update er_codelst set codelst_seq = 8 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'DCEG';
Update er_codelst set codelst_seq = 9 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'DCP';
Update er_codelst set codelst_seq = 10 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'DEA';
Update er_codelst set codelst_seq = 11 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'DTP';
Update er_codelst set codelst_seq = 12 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'N/A';
Update er_codelst set codelst_seq = 14, codelst_desc='OSB/SPORE' where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'OSB/SPOREs';
Update er_codelst set codelst_seq = 15 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'RRP';
Update er_codelst set codelst_seq = 16 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and codelst_subtyp = 'TRP';
commit;

declare
v_exists integer := 0;
begin
	select count(*) into v_exists from er_codelst where CODELST_TYPE ='ProgramCode' AND CODELST_DESC ='CCT/CTB';
	if (v_exists <= 0) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, codelst_seq ) 
		values (seq_er_codelst.nextval,'ProgramCode' ,'CCTCTB', 'CCT/CTB', 'NCI',2); 
	else 
		Update er_codelst set codelst_seq = 2, codelst_subtyp = 'CCTCTB' where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and CODELST_DESC ='CCT/CTB';
	end if;
	commit;

	select count(*) into v_exists from er_codelst where CODELST_TYPE ='ProgramCode' AND CODELST_DESC ='OD';
	if (v_exists <= 0) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, codelst_seq ) 
		values (seq_er_codelst.nextval,'ProgramCode','OD', 'OD', 'NCI',13); 
	else
		Update er_codelst set codelst_seq = 13, codelst_subtyp = 'OD' where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NCI' and CODELST_DESC ='OD';
	end if;
	commit;
end;
/

--NIH program codes

Update er_codelst set codelst_desc='NIH Office of the Director (OD)',codelst_seq = 1 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'OD';
Update er_codelst set codelst_desc='National Eye Institute (NEI)',codelst_seq = 3 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NEI';
Update er_codelst set codelst_desc='National Heart, Lung, and Blood Institute (NHLBI)',codelst_seq = 4 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NHLBI';
Update er_codelst set codelst_desc='National Human Genome Research Institute (NHGRI)',codelst_seq = 5 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NHGRI';
Update er_codelst set codelst_desc='National Institute on Aging (NIA)',codelst_seq = 6 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NIA';
Update er_codelst set codelst_desc='National Institute on Alcohol Abuse and Alcoholism (NIAAA)',codelst_seq = 7 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NIAAA';
Update er_codelst set codelst_desc='National Institute of Allergy and Infectious Diseases (NIAID)',codelst_seq = 8 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NIAID';
Update er_codelst set codelst_desc='National Institute of Arthritis and Musculoskeletal and Skin Diseases (NIAMS)',codelst_seq = 9 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NIAMS';
Update er_codelst set codelst_desc='National Institute of Biomedical Imaging and Bioengineering (NIBIB)',codelst_seq = 10 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NIBIB';
Update er_codelst set codelst_desc='Eunice Kennedy Shriver�National Institute of Child Health and Human Development (NICHD)',codelst_seq = 11 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NICHD';
Update er_codelst set codelst_desc='National Institute on Deafness and Other Communication Disorders (NIDCD)',codelst_seq = 12 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NIDCD';
Update er_codelst set codelst_desc='National Institute of Dental and Craniofacial Research (NIDCR)',codelst_seq = 13 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NIDCR';
Update er_codelst set codelst_desc='National Institute of Diabetes and Digestive and Kidney Diseases (NIDDK)',codelst_seq = 14 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NIDDK';
Update er_codelst set codelst_desc='National Institute on Drug Abuse (NIDA)�',codelst_seq = 15 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NIDA';
Update er_codelst set codelst_desc='National Institute of Environmental Health Sciences (NIEHS)',codelst_seq = 16 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NIEHS';
Update er_codelst set codelst_desc='National Institute of General Medical Sciences (NIGMS)',codelst_seq = 17 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NIGMS';
Update er_codelst set codelst_desc='National Institute of Mental Health (NIMH)',codelst_seq = 18 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NIMH';
Update er_codelst set codelst_desc='National Institute of Neurological Disorders and Stroke (NINDS)�',codelst_seq = 20 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NINDS';
Update er_codelst set codelst_desc='National Institute of Nursing Research (NINR)',codelst_seq = 21 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NINR';
Update er_codelst set codelst_desc='National Library of Medicine (NLM)',codelst_seq = 22 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NLM';
Update er_codelst set codelst_desc='Center for Information Technology (CIT)',codelst_seq = 23 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'CIT';
Update er_codelst set codelst_desc='Center for Scientific Review (CSR)',codelst_seq = 24 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'CSR';
Update er_codelst set codelst_desc='Fogarty International Center (FIC)',codelst_seq = 25 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'FIC';
Update er_codelst set codelst_desc='NIH Clinical Center (CC)',codelst_seq = 28 where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'CC';
commit;


declare
v_exists integer := 0;
begin
	select count(*) into v_exists from er_codelst where CODELST_TYPE ='ProgramCode' AND CODELST_CUSTOM_COL='NIH' AND CODELST_DESC ='National Cancer Institute (NCI)';
	if (v_exists <= 0) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, codelst_seq ) 
		values (seq_er_codelst.nextval,'ProgramCode','NCI', 'National Cancer Institute (NCI)', 'NIH',2);
	else 
		Update er_codelst set codelst_seq = 2, codelst_subtyp = 'NCI' where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' 
		and CODELST_DESC ='National Cancer Institute (NCI)';
	end if;

	select count(*) into v_exists from er_codelst where CODELST_TYPE ='ProgramCode' AND CODELST_CUSTOM_COL='NIH' AND CODELST_DESC ='National Institute on Minority Health and Health Disparities (NIMHD)';
	if (v_exists <= 0) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, codelst_seq ) 
		values (seq_er_codelst.nextval,'ProgramCode','NIMHD', 'National Institute on Minority Health and Health Disparities (NIMHD)', 'NIH',19);
	else 
		Update er_codelst set codelst_seq = 19, codelst_subtyp = 'NIMHD' where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' 
		and CODELST_DESC ='National Institute on Minority Health and Health Disparities (NIMHD)';
	end if;

	select count(*) into v_exists from er_codelst where CODELST_TYPE ='ProgramCode' AND CODELST_CUSTOM_COL='NIH' 
	AND trim(CODELST_DESC) ='National Center for Complementary and Integrative Health (NCCIH)';
	if (v_exists <= 0) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, codelst_seq ) 
		values (seq_er_codelst.nextval,'ProgramCode','NCCIH', 'National Center for Complementary and Integrative Health (NCCIH)', 'NIH',26);
	else
		Update er_codelst set codelst_seq = 26, codelst_subtyp = 'NCCIH' where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and trim(CODELST_DESC) ='National Center for Complementary and Integrative Health (NCCIH)';
	end if;
	
	select count(*) into v_exists from er_codelst where CODELST_TYPE ='ProgramCode' AND CODELST_CUSTOM_COL='NIH' 
	AND CODELST_DESC ='National Center for Advancing Translational Sciences (NCATS)';
	if (v_exists <= 0) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL, codelst_seq ) 
		values (seq_er_codelst.nextval,'ProgramCode','NCATS', 'National Center for Advancing Translational Sciences (NCATS)', 'NIH',27);
	else
		Update er_codelst set codelst_seq = 27, codelst_subtyp = 'NCATS' where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and CODELST_DESC ='National Center for Advancing Translational Sciences (NCATS)';
	end if;
	commit;
end;
/

Update er_codelst set codelst_hide='Y' where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NCCAM';
Update er_codelst set codelst_hide='Y' where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NCMHD';
Update er_codelst set codelst_hide='Y' where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NCRR';
commit;

/* LE build #14 Scripts */

--01

update ER_CODELST set CODELST_TYPE='studyvercat1' where
CODELST_TYPE='studyvercat' and CODELST_SUBTYP='studyvercat_5';

update ER_CODELST set CODELST_TYPE='studyvercat1' where
CODELST_TYPE='studyvercat' and CODELST_SUBTYP='studyvercat_6';

update ER_CODELST set CODELST_DESC='VP of Clinical Research Administration Determination' where
CODELST_TYPE='rev_board' and CODELST_SUBTYP='vp_cr';

commit;

--02

Update er_codelst set codelst_hide='Y', CODELST_TYPE='ProgramCode1' where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NCCAM';
Update er_codelst set codelst_hide='Y', CODELST_TYPE='ProgramCode1' where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NCMHD';
Update er_codelst set codelst_hide='Y', CODELST_TYPE='ProgramCode1' where CODELST_TYPE='ProgramCode' AND CODELST_CUSTOM_COL='NIH' and codelst_subtyp = 'NCRR';
commit;

--Study drug agent
update er_codelst set codelst_custom_col1 = '<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "abiraterone">abiraterone</OPTION>
<OPTION value = "abraxane">abraxane</OPTION>
<OPTION value = "ABT-199">ABT-199</OPTION>
<OPTION value = "ABT-888">ABT-888 (veliparib)</OPTION>
<OPTION value = "AC220">AC220</OPTION>
<OPTION value = "AG-013736">AG-013736</OPTION>
<OPTION value = "AMG-337">AMG-337</OPTION>
<OPTION value = "AP-24534">AP-24534</OPTION>
<OPTION value = "ARRY-614">ARRY-614</OPTION>
<OPTION value = "AZD-1208">AZD-1208</OPTION>
<OPTION value = "birinipant">birinipant</OPTION>
<OPTION value = "BKM-120">BKM-120</OPTION>
<OPTION value = "BMS-833923">BMS-833923</OPTION>
<OPTION value = "bortezomib">bortezomib</OPTION>
<OPTION value = "Bosutinib">Bosutinib</OPTION>
<OPTION value = "CC-223">CC-223</OPTION>
<OPTION value = "cediranib">cediranib (AZD-2171)</OPTION>
<OPTION value = "CMX-001">CMX-001</OPTION>
<OPTION value = "CPX-351">CPX-351</OPTION>
<OPTION value = "crenolanib">Crenolanib</OPTION>
<OPTION value = "CWP-23229">CWP-232291</OPTION>
<OPTION value = "dasatinib">dasatinib</OPTION>
<OPTION value = "denileukin">denileukin diftitox</OPTION>
<OPTION value = "E-75acetate">E-75 acetate</OPTION>
<OPTION value = "eflornithine">eflornithine</OPTION>
<OPTION value = "entinostat">Entinostat</OPTION>
<OPTION value = "erlotinib">erlotinib</OPTION>
<OPTION value = "everolimus">everolimus (RAD-001)</OPTION>
<OPTION value = "GDC-0941">GDC-0941</OPTION>
<OPTION value = "GS-1101">GS-1101</OPTION>
<OPTION value = "GSK-1120212">GSK-1120212</OPTION>
<OPTION value = "dabrafenib">GSK-2118436 (dabrafenib)</OPTION>
<OPTION value = "GSK-525762">GSK-525762</OPTION>
<OPTION value = "ibrutinib">ibrutinib</OPTION>
<OPTION value = "ICL-670">ICL-670</OPTION>
<OPTION value = "INCB-018424">INCB-018424</OPTION>
<OPTION value = "KB-004">KB-004</OPTION>
<OPTION value = "lacosamide">lacosamide</OPTION>
<OPTION value = "Lapatinib">Lapatinib</OPTION>
<OPTION value = "LDE-225">LDE-225</OPTION>
<OPTION value = "LEE-011">LEE-011</OPTION>
<OPTION value = "lenalidomide">lenalidomide(CC-5013)</OPTION>
<OPTION value = "LY-2784544">LY-2784544</OPTION>
<OPTION value = "LY-2940680">LY-2940680</OPTION>
<OPTION value = "LY-3009120">LY-3009120</OPTION>
<OPTION value = "masatinib">masatinib</OPTION>
<OPTION value = "MEK-162">MEK-162</OPTION>
<OPTION value = "MK-1775">MK-1775</OPTION>
<OPTION value = "MK-2206">MK-2206</OPTION>
<OPTION value = "MRX-34-">MRX-34</OPTION>
<OPTION value = "olaparib">olaparib</OPTION>
<OPTION value = "Omacetaxine">Omacetaxine</OPTION>
<OPTION value = "panobinostat (LBH-589)">panobinostat (LBH-589)</OPTION>
<OPTION value = "pazopanib">pazopanib</OPTION>
<OPTION value = "PF-0449913">PF-0449913</OPTION>
<OPTION value = "PKC-412">PKC-412</OPTION>
<OPTION value = "pomalidomide">pomalidomide</OPTION>
<OPTION value = "Ponatinib">Ponatinib</OPTION>
<OPTION value = "Ponatinib-AP">Ponatinib(AP-24534)</OPTION>
<OPTION value = "posaconazole">posaconazole</OPTION>
<OPTION value = "pracinostat">pracinostat</OPTION>
<OPTION value = "PRI0724">PRI0724</OPTION>
<OPTION value = "Rigosertib">Rigosertib</OPTION>
<OPTION value = "rucaparib">rucaparib</OPTION>
<OPTION value = "Ruxolitinib">Ruxolitinib</OPTION>
<OPTION value = "S-1">S-1</OPTION>
<OPTION value = "SAR-302503">SAR-302503</OPTION>
<OPTION value = "topiramate">topiramate</OPTION>
<OPTION value = "volasertib">volasertib</OPTION>
<OPTION value = "vorinostat">vorinostat</OPTION>
<OPTION value = "VST-1001">VST-1001</OPTION>
<OPTION value = "XL-184">XL-184</OPTION>
</SELECT>' where codelst_type = 'studyidtype' and codelst_subtyp like 'drugAgent%'; 

commit;


update er_codelst set codelst_custom_col1 = '{chkArray:[
{data: "option1", display:"Bypass CRC/PBHSRC Review<br>This study is NCI-sponsored, CTEP-approved, and/or NCI cooperative group-approved trial, or peer-reviwed by an NIH-approved mechanism and IRB-approved at an NCI designated cancer center. Please attach appropriate documentation (e.g. NCI-approval letter or IRB approval memo) to support the request to bypass CRC/PBHSRC review."},
{data: "option2", display:"Expedited Biostatistical Review<br>This study is an industry-sponsored, multi-center clinical trial already activated at an NCI designated cancer center and/or has been approved by the Office of the Vice Provost for Fast-Track designation. Please attach appropriate documentation (e.g. Sponsor memorandum,ClinicalTrial.gov listing verifying activation at an NCI-designated cancer center, OVP Fast Track Approval memorandum) to support the request for expedited biostatistical review."}
]}' where codelst_type = 'studyidtype' and codelst_subtyp like 'expdBiostat'; 

commit;

/* LE build #15 Scripts */

--01

declare
v_exists number := 0;
begin
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='adapthence'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'adapthence','Adapter henceforth',1);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='affyGene'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'affyGene','Affymetrix Gene Chip U133A',2);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='bipapVision'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'bipapVision','BIPAP Vision Ventilatory Support System',3);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='biocomp'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'biocomp','Biocompatibles LC Bead (100-300) loaded with Doxorubicin HCl',4);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='bioSeal'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'bioSeal','Bio-Seal Track Plug',5);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='block'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'block','B-lock solution',6);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='caphosol'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'caphosol','Caphosol',7);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='cavSpine'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'cavSpine','Cavity SpineWand',8);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='cliniMACS'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'cliniMACS','CliniMACS device',9);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='cryocare'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'cryocare','Cryocare',10);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='daVinci'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'daVinci','da Vinci Robotic Surgical System',11);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='exAblate'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'exAblate','ExAblate device',12);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='galilMed'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'galilMed','Galil Medical Cryoprobes',13);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='goldFid'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'goldFid','Gold Fiducial Marker',14);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='imageGuide'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'imageGuide','Image Guide Microscope - a high-resolution microendoscope (HRME)',15);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='imagio'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'imagio','Imagio Breast Imaging System',16);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='indocyanine'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'indocyanine','Indocyanine green fluorescence lymphography system',17);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='infrared'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'infrared','Infrared camera',18);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='inSpectra'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'inSpectra','InSpectra Tissue Oxygenation Monitor Model 650 and Spot Check Monitor Model 300',19);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='integrated'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'integrated','Integrated BRACAnalysis',20);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='intensity'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'intensity','Intensity Modulated Radiotherapy',21);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='iodine125'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'iodine125','Iodine-125 radioactive seeds',22);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='louis3D'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'louis3D','LOUIS-3D (Laser OPtoacoustic Ultrasonic Imaging System',23);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='lowCoh'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'lowCoh','Low Coherence Enhanced Backscattering (LEBS)',24);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='mdaApp'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'mdaApp','MDA Applicator',25);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='mirena'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'mirena','Mirena (Levonorgestrel Contraceptive IUD)',26);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='pemFlex'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'pemFlex','PEMFlex Solo II High Resolution PET Scanner',27);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='pointSpectro'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'pointSpectro','Point spectroscopy probe(POS) and PS2',28);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='prostateImm'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'prostateImm','Prostate Immobilization Treatment Device',29);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='senoRxCon'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'senoRxCon','SenoRx Contura MLB applicator',30);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='SERI'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'SERI','SERI Surgical Scaffold',31);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='SIR'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'SIR','SIR-spheres',32);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='sonablate'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'sonablate','Sonablate 500 (Sonablate)',33);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='spectra'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'spectra','Spectra Optia Apheresis System',34);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='vapotherm'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'vapotherm','The Vapotherm 2000i Respiratory Therapy Device',35);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='strattice'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'strattice','Strattice Tissue Matrix',36);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='syntheCel'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'syntheCel','SyntheCelTM Dura Replacement',37);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='ISLLC'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'ISLLC','The ISLLC Implantable Drug Delivery System (IDDS)',38);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='visica'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'visica','The Visica 2 Treatment System',39);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='theraSphere'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'theraSphere','TheraSphere',40);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='transducer'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'transducer','Transducer-tipped catheter Mikro-Tip SPC-320, transducer Millar, and Power Lab 8/30 amplifier',41);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='truFreeze'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'truFreeze','truFreezeTM System',42);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='UB500'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'UB500','UB500',43);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='viOptix'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'viOptix','ViOptix continuous transcutaneous tissue oximetry device',44);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='vendys'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'vendys','VENDYS-Model 6000 B/C',45);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindDeviceAgent' and codelst_subtyp ='veridex'; 
	if (v_exists = 0) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDeviceAgent' ,'veridex','Veridex CellSearch assay',46);
	end if;
	commit;
end;
/

update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDeviceAgent"}' where codelst_type='studyidtype' and codelst_subtyp='deviceAgent1';
update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDeviceAgent"}' where codelst_type='studyidtype' and codelst_subtyp='deviceAgent2';
update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDeviceAgent"}' where codelst_type='studyidtype' and codelst_subtyp='deviceAgent3';
update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDeviceAgent"}' where codelst_type='studyidtype' and codelst_subtyp='deviceAgent4';
update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDeviceAgent"}' where codelst_type='studyidtype' and codelst_subtyp='deviceAgent5';
commit;

--02

update er_studyid set studyid_id ='panobinostat' where studyid_id = 'panobinostat (LBH-589)' and fk_codelst_idtype in (select pk_codelst from er_codelst 
where codelst_type ='studyidtype' and codelst_subtyp in ('drugAgent1', 'drugAgent2', 'drugAgent3', 'drugAgent4', 'drugAgent5',
'drugAgent6','drugAgent7','drugAgent8','drugAgent9','drugAgent10'));

commit;

declare
v_exists number :=0;
begin
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='abiraterone';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'abiraterone','abiraterone',1);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='abraxane';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'abraxane','abraxane',2);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='ABT-199';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'ABT-199','ABT-199',3);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='ABT-888';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'ABT-888','ABT-888 (veliparib)',4);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='AC220';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'AC220','AC220',5);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='AG-013736';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'AG-013736','AG-013736',6);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='AMG-337';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'AMG-337','AMG-337',7);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='AP-24534';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'AP-24534','AP-24534',8);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='ARRY-614';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'ARRY-614','ARRY-614',9);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='AZD-1208';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'AZD-1208','AZD-1208',10);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='birinipant';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'birinipant','birinipant',11);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='BKM-120';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'BKM-120','BKM-120',12);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='BMS-833923';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'BMS-833923','BMS-833923',13);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='bortezomib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'bortezomib','bortezomib',14);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='Bosutinib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'Bosutinib','Bosutinib',15);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='CC-223';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'CC-223','CC-223',16);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='cediranib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'cediranib','cediranib (AZD-2171)',17);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='CMX-001';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'CMX-001','CMX-001',18);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='CPX-351';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'CPX-351','CPX-351',19);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='crenolanib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'crenolanib','Crenolanib',20);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='CWP-23229';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'CWP-23229','CWP-232291',21);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='dasatinib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'dasatinib','dasatinib',22);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='denileukin';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'denileukin','denileukin diftitox',23);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='E-75acetate';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'E-75acetate','E-75 acetate',24);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='eflornithine';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'eflornithine','eflornithine',25);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='entinostat';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'entinostat','Entinostat',26);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='erlotinib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'erlotinib','erlotinib',27);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='everolimus';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'everolimus','everolimus (RAD-001)',28);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='GDC-0941';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'GDC-0941','GDC-0941',29);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='GS-1101';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'GS-1101','GS-1101',30);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='GSK-1120212';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'GSK-1120212','GSK-1120212',31);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='dabrafenib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'dabrafenib','GSK-2118436 (dabrafenib)',32);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='GSK-525762';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'GSK-525762','GSK-525762',33);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='ibrutinib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'ibrutinib','ibrutinib',34);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='ICL-670';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'ICL-670','ICL-670',35);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='INCB-018424';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'INCB-018424','INCB-018424',36);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='KB-004';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'KB-004','KB-004',37);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='lacosamide';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'lacosamide','lacosamide',38);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='Lapatinib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'Lapatinib','Lapatinib',39);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='LDE-225';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'LDE-225','LDE-225',40);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='LEE-011';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'LEE-011','LEE-011',41);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='lenalidomide';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'lenalidomide','lenalidomide(CC-5013)',42);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='LY-2784544';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'LY-2784544','LY-2784544',43);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='LY-2940680';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'LY-2940680','LY-2940680',44);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='LY-3009120';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'LY-3009120','LY-3009120',45);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='masatinib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'masatinib','masatinib',46);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='MEK-162';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'MEK-162','MEK-162',47);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='MK-1775';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'MK-1775','MK-1775',48);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='MK-2206';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'MK-2206','MK-2206',49);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='MRX-34';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'MRX-34','MRX-34',50);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='olaparib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'olaparib','olaparib',51);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='Omacetaxine';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'Omacetaxine','Omacetaxine',52);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='panobinostat';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'panobinostat','panobinostat (LBH-589)',53);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='pazopanib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'pazopanib','pazopanib',54);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='PF-0449913';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'PF-0449913','PF-0449913',55);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='PKC-412';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'PKC-412','PKC-412',56);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='pomalidomide';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'pomalidomide','pomalidomide',57);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='Ponatinib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'Ponatinib','Ponatinib',58);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='Ponatinib-AP';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'Ponatinib-AP','Ponatinib(AP-24534)',59);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='posaconazole';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'posaconazole','posaconazole',60);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='pracinostat';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'pracinostat','pracinostat',61);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='PRI0724';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'PRI0724','PRI0724',62);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='Rigosertib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'Rigosertib','Rigosertib',63);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='rucaparib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'rucaparib','rucaparib',64);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='Ruxolitinib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'Ruxolitinib','Ruxolitinib',65);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='S-1';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'S-1','S-1',66);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='SAR-302503';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'SAR-302503','SAR-302503',67);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='topiramate';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'topiramate','topiramate',68);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='volasertib';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'volasertib','volasertib',69);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='vorinostat';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'vorinostat','vorinostat',70);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='VST-1001';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'VST-1001','VST-1001',71);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDrugAgent' and codelst_subtyp ='XL-184';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindDrugAgent' ,'XL-184','XL-184',72);
	END IF;
	commit;
end;
/


update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDrugAgent"}' where codelst_type='studyidtype' and codelst_subtyp='drugAgent1';
update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDrugAgent"}' where codelst_type='studyidtype' and codelst_subtyp='drugAgent2';
update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDrugAgent"}' where codelst_type='studyidtype' and codelst_subtyp='drugAgent3';
update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDrugAgent"}' where codelst_type='studyidtype' and codelst_subtyp='drugAgent4';
update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDrugAgent"}' where codelst_type='studyidtype' and codelst_subtyp='drugAgent5';
update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDrugAgent"}' where codelst_type='studyidtype' and codelst_subtyp='drugAgent6';
update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDrugAgent"}' where codelst_type='studyidtype' and codelst_subtyp='drugAgent7';
update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDrugAgent"}' where codelst_type='studyidtype' and codelst_subtyp='drugAgent8';
update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDrugAgent"}' where codelst_type='studyidtype' and codelst_subtyp='drugAgent9';
update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDrugAgent"}' where codelst_type='studyidtype' and codelst_subtyp='drugAgent10';
commit;

--03

declare
v_exists number :=0;
begin
	select count(*) into v_exists from er_codelst where codelst_type='LindProDes2' and codelst_subtyp ='YesComp';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq)
		values (seq_er_codelst.nextval,'LindProDes2' ,'YesComp','Yes -- Completely',1);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindProDes2' and codelst_subtyp ='YesPart';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindProDes2' ,'YesPart','Yes -- Partially',2);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindProDes2' and codelst_subtyp ='NoSponsrDes';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindProDes2' ,'NoSponsrDes','No -- Sponsor provided biostatistical design',3);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindProDes2' and codelst_subtyp ='NoProsStat';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindProDes2' ,'NoProsStat','No prospective statistical design in this study',4);
	END IF;
	commit;
end;
/


update er_codelst set codelst_custom_col1 = '{codelst_type:"LindProDes2"}' where codelst_type='studyidtype' and codelst_subtyp='proDes2';

commit;

--04

declare
v_exists number :=0;
begin
	select count(*) into v_exists from er_codelst where codelst_type='LindTemplateTyp' and codelst_subtyp ='coopGrp';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq, CODELST_HIDE)
		values (seq_er_codelst.nextval,'LindTemplateTyp' ,'coopGrp','Cooperative Group',1,'Y');
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindTemplateTyp' and codelst_subtyp ='consrtm';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq, CODELST_HIDE )
		values (seq_er_codelst.nextval,'LindTemplateTyp' ,'consrtm','Consortium',2, 'Y');
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindTemplateTyp' and codelst_subtyp ='nciEtctn';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq, CODELST_HIDE )
		values (seq_er_codelst.nextval,'LindTemplateTyp' ,'nciEtctn','NCI ETCTN',3, 'Y');
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindTemplateTyp' and codelst_subtyp ='indstrStdy';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindTemplateTyp' ,'indstrStdy','Industry Study',4);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindTemplateTyp' and codelst_subtyp ='MDACCInvIni';
	if (v_exists < 1) then
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq )
		values (seq_er_codelst.nextval,'LindTemplateTyp' ,'MDACCInvIni','MDACC Investigator Initiated',5);
	END IF;
	commit;
end;
/


update er_codelst set codelst_custom_col1 = '{codelst_type:"LindTemplateTyp"}' where codelst_type='studyidtype' and codelst_subtyp='spnsrdProt';

commit;

--05

declare
v_exists number :=0;
begin
	select count(*) into v_exists from er_codelst where codelst_type='LindDSMB' and codelst_subtyp ='mdaDsmb'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDSMB' ,'mdaDsmb','MD ANDERSON DSMB',1);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDSMB' and codelst_subtyp ='mdaExtdsmb'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDSMB' ,'mdaExtdsmb','MD ANDERSON  External DSMB ',2);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDSMB' and codelst_subtyp ='indpDsmb'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDSMB' ,'indpDsmb','Independent DSMB - This is a DSMB comprised of members who are not affiliated with the research or sponsor in any way.',3);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDSMB' and codelst_subtyp ='intDsmb'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDSMB' ,'intDsmb','Internal DSMB � This is a DSMB comprised of study investigators and/or sponsor employees/contractors.',4);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindDSMB' and codelst_subtyp ='notAppl'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindDSMB' ,'notAppl','Not Applicable',5);
	END IF;
	commit;
end;
/


update er_codelst set codelst_custom_col1 = '{codelst_type:"LindDSMB"}' where codelst_type='studyidtype' and codelst_subtyp='dsmbInt';

commit;

--06

declare
v_exists number :=0;
begin
	select count(*) into v_exists from er_codelst where codelst_type='LindFundingType' and codelst_subtyp ='NCI'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values(seq_er_codelst.nextval,'LindFundingType' ,'NCI','NCI',1);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindFundingType' and codelst_subtyp ='nih'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values(seq_er_codelst.nextval,'LindFundingType' ,'nih','NIH (other than NCI)',2);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindFundingType' and codelst_subtyp ='dod'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values(seq_er_codelst.nextval,'LindFundingType' ,'dod','DOD',3);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindFundingType' and codelst_subtyp ='peerRevFund'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values(seq_er_codelst.nextval,'LindFundingType' ,'peerRevFund','Other peer reviewed funding (e.g. NSF, ACS, etc.)',4);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindFundingType' and codelst_subtyp ='privInd'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values(seq_er_codelst.nextval,'LindFundingType' ,'privInd','Private Industry',5);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindFundingType' and codelst_subtyp ='privFound'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values(seq_er_codelst.nextval,'LindFundingType' ,'privFound','Private Foundation',6);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindFundingType' and codelst_subtyp ='deptFunds'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values(seq_er_codelst.nextval,'LindFundingType' ,'deptFunds','Departmental Funds',7);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindFundingType' and codelst_subtyp ='donorFunds'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values(seq_er_codelst.nextval,'LindFundingType' ,'donorFunds','Donor Funds',8);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindFundingType' and codelst_subtyp ='unfunded'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values(seq_er_codelst.nextval,'LindFundingType' ,'unfunded','Unfunded',9);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindFundingType' and codelst_subtyp ='coopGrp'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values(seq_er_codelst.nextval,'LindFundingType' ,'coopGrp','Cooperative group',10);
	END IF;

	commit;
end;
/


update er_codelst set codelst_custom_col1 = '{codelst_type:"LindFundingType"}' where codelst_type='studyidtype' and codelst_subtyp like 'fundingType%';

commit;

--07

update er_codelst set codelst_custom_col1 = '<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>' where codelst_type ='studyidtype' and codelst_subtyp = 'bayesianComp';

commit;

update er_codelst set codelst_custom_col1 = '<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>' where codelst_type ='studyidtype' and codelst_subtyp = 'spnsrshp1';

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,327,930,'07_misc_codelist.sql',sysdate,'v9.3.0 #727-le15');

commit;

--08

declare
v_exists number :=0;
begin
		select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Aural'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Aural','Aural',1);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Epidural'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Epidural','Epidural',2);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Inhalation'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Inhalation','Inhalation',3);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intraarterial'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intraarterial','Intraarterial',4);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intradermal'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intradermal','Intradermal',5);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intralesional'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intralesional','Intralesional',6);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intramucosal'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intramucosal','Intramucosal',7);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intramuscular'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intramuscular','Intramuscular',8);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intranasal'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intranasal','Intranasal',9);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intraocular'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intraocular','Intraocular',10);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intraosseous'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intraosseous','Intraosseous',11);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intraperitoneal'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intraperitoneal','Intraperitoneal',12);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intrapleural'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intrapleural','Intrapleural',13);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intrathecal'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intrathecal','Intrathecal',14);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intratumoral'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intratumoral','Intratumoral',15);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intravenous'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intravenous','Intravenous',16);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Intravesicular'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Intravesicular','Intravesicular',17);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Ocular'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Ocular','Ocular',18);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Oral'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Oral','Oral',19);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Rectal'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Rectal','Rectal',20);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Subcutaneous'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Subcutaneous','Subcutaneous',21);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Sublingual'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Sublingual','Sublingual',22);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Topical'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Topical','Topical',23);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Transdermal'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Transdermal','Transdermal',24);
	end if;
	select count(*) into v_exists from er_codelst where codelst_type='LindRouteAdmin' and codelst_subtyp ='Vaginal'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindRouteAdmin' ,'Vaginal','Vaginal',25);
	end if;

	commit;
end;
/


update er_codelst set codelst_custom_col1 = '{codelst_type:"LindRouteAdmin"}' where codelst_type='studyidtype' and codelst_subtyp like 'route%';

commit;

/* LE build #16 Scripts */

--01

update er_codelst set codelst_custom_col1 = '<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>' where codelst_type ='studyidtype' and codelst_subtyp = 'knownBiomrkr';

commit;

update er_codelst set codelst_custom_col1 = '<SELECT id="alternateId" NAME="alternateId">
<OPTION value = "">Select an option</OPTION>
<OPTION value = "Yes">Yes</OPTION>
<OPTION value = "No">No</OPTION>
</SELECT>' where codelst_type ='studyidtype' and codelst_subtyp = 'prot_req_ind';

commit;

declare
v_exists number :=0;
begin
	select count(*) into v_exists from er_codelst where codelst_type='LindBiomrkrType' and codelst_subtyp ='gene'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindBiomrkrType' ,'gene','Gene',1);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindBiomrkrType' and codelst_subtyp ='microrna'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindBiomrkrType' ,'microrna','Micro RNA',2);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindBiomrkrType' and codelst_subtyp ='snp'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindBiomrkrType' ,'snp','SNP-Small Nucleotide Polymorphism',3);
	END IF;
	select count(*) into v_exists from er_codelst where codelst_type='LindBiomrkrType' and codelst_subtyp ='other'; 
	if (v_exists < 1) then 
		Insert into er_codelst columns(pk_codelst, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, codelst_seq ) 
		values (seq_er_codelst.nextval,'LindBiomrkrType' ,'other','Other',4);
	END IF;
	commit;
end;
/


update er_codelst set codelst_custom_col1 = '{codelst_type:"LindBiomrkrType"}' where codelst_type='studyidtype' and codelst_subtyp='biomrkrType';

commit;

--03

DECLARE
  v_column_exists number := 0;  
BEGIN
  select count(*) into v_column_exists from user_tab_cols 
  where column_name = 'PAGE_MOD_STAT_SUBTYP' and table_name = 'UI_FLX_PAGEVER';

  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.UI_FLX_PAGEVER ADD (PAGE_MOD_STAT_SUBTYP VARCHAR(15))';
  end if;
end;
/

COMMENT ON COLUMN 
ERES.UI_FLX_PAGEVER.PAGE_MOD_STAT_SUBTYP IS 
'This column stores the codelst subtyp of the status of the respective module mentioned in the PAGE_MOD_TABLE column of this table';

/* LE build #17 Scripts */

--01

DECLARE
  v_item_exists number := 0;  
BEGIN
select count(*) into v_item_exists from user_tab_cols where TABLE_NAME = 'ER_STUDYVER'
and COLUMN_NAME = 'STUDYVER_MAJORVER';
if (v_item_exists = 0) then
  execute immediate 'ALTER TABLE ERES.ER_STUDYVER ADD (STUDYVER_MAJORVER NUMBER DEFAULT 0)';
  dbms_output.put_line('Col STUDYVER_MAJORVER added');
else
  dbms_output.put_line('Col STUDYVER_MAJORVER already exists');
end if;

END;
/

DECLARE
  v_item_exists number := 0;  
BEGIN
select count(*) into v_item_exists from user_tab_cols where TABLE_NAME = 'ER_STUDYVER'
and COLUMN_NAME = 'STUDYVER_MINORVER';
if (v_item_exists = 0) then
  execute immediate 'ALTER TABLE ERES.ER_STUDYVER ADD (STUDYVER_MINORVER NUMBER DEFAULT 0)';
  dbms_output.put_line('Col STUDYVER_MINORVER added');
else
  dbms_output.put_line('Col STUDYVER_MINORVER already exists');
end if;

END;
/

--02

update er_codelst set codelst_custom_col = 'textarea' where 
codelst_type ='studyidtype' and codelst_subtyp in ('dose','doseRationale');

commit;

/* LE build #18 Scripts */

--01

update er_codelst set codelst_hide = 'N', codelst_desc='Informed Consent Report' where codelst_type = 'studyvercat' and codelst_subtyp = 'inf_consent';
commit;

/* LE build #19 Scripts */

--05

DECLARE
  v_column_exists number := 0;  
BEGIN
  select count(*) into v_column_exists from user_tab_cols 
  where column_name = 'PAGE_FORMRESPONSES' and table_name = 'UI_FLX_PAGEVER';

  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.UI_FLX_PAGEVER ADD (PAGE_FORMRESPONSES CLOB)';
  end if;
end;
/

COMMENT ON COLUMN 
ERES.UI_FLX_PAGEVER.PAGE_FORMRESPONSES IS 
'This column stores the form responses archives in json array format';


DECLARE
  v_column_exists number := 0;  
BEGIN
  select count(*) into v_column_exists from user_tab_cols 
  where column_name = 'PAGE_FORMRESPDIFF' and table_name = 'UI_FLX_PAGEVER';

  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.UI_FLX_PAGEVER ADD (PAGE_FORMRESPDIFF CLOB)';
  end if;
end;
/

COMMENT ON COLUMN 
ERES.UI_FLX_PAGEVER.PAGE_FORMRESPDIFF IS 
'This column stores the form responses diff in json array format';


DECLARE
  v_column_exists number := 0;  
BEGIN
  select count(*) into v_column_exists from user_tab_cols 
  where column_name = 'PAGE_COMMENTS' and table_name = 'UI_FLX_PAGEVER';

  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.UI_FLX_PAGEVER ADD (PAGE_COMMENTS CLOB)';
  end if;
end;
/

COMMENT ON COLUMN 
ERES.UI_FLX_PAGEVER.PAGE_COMMENTS IS 
'This column stores the version comments';

--06

DECLARE
  v_table_exists number := 0;  
BEGIN
  select count(*) into v_table_exists from sys.user_tables where table_name = 'ER_RESUBM_DRAFT';

  if (v_table_exists = 0) then
      execute immediate 'CREATE TABLE ERES.ER_RESUBM_DRAFT AS '
      || 'SELECT * FROM ERES.UI_FLX_PAGEVER WHERE PAGE_MOD_TABLE = ''ABC''';
  end if;
end;
/

COMMENT ON TABLE 
ERES.ER_RESUBM_DRAFT IS 
'This table stores re-submission draft data.';

/* LE build #20 Scripts */

--01

declare
v_CodeExists number := 0;
begin
	
SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studyvercat' AND CODELST_SUBTYP = 'studCalSchEvnts';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc, codelst_hide, codelst_seq)
        values (seq_er_codelst.nextval, 'studyvercat', 'studCalSchEvnts', 'Study Calendar/Schedule of Events', 'N', 11);
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;

	
COMMIT;
END;
/

/* LE build #22 Scripts */

--04

update er_codelst set codelst_desc='Data Safety and Monitoring Plan' where codelst_type = 'studyidtype' and codelst_subtyp='dataSftyMntrPln'; 

commit;

declare
v_id varchar(1000);
begin
	select 'alternateId'|| PK_CODELST into v_id from er_codelst where codelst_type = 'studyidtype' and codelst_subtyp='nom_rev_id';
	
	update er_codelst set 
	codelst_custom_col1='{lookupPK:6000, selection:"multi", mapping:[{source:"USRFULLNAME", target:"alternateId"},{source:"USRLOGIN", target:"'|| v_id ||'"}]}' 
	where codelst_type = 'studyidtype' and codelst_subtyp='nom_rev'; 

commit;
end;
/

update er_codelst set codelst_custom_col='hidden-input' where codelst_type = 'studyidtype' and codelst_subtyp='nom_alt_rev'; 

commit;

--05

create or replace
FUNCTION F_DISPOPTION(optn VARCHAR2, cdlstID NUMBER) RETURN CLOB 
IS
v_json json ;
a    dbms_utility.uncl_array;
a1    dbms_utility.uncl_array;
len  pls_integer;
v_json_value json_value;
v_json_list json_list;
v_json_list2 json_list;
v_ccc1 VARCHAR2(4000) := '';
v_ccc12 VARCHAR2(4000) := '';
i number := 0;
j number := 0;
begin 
  select codelst_custom_col1 INTO v_ccc1 from er_codelst where codelst_type = 'studyidtype' and codelst_custom_col = 'checkbox' 
  and pk_codelst =cdlstID and codelst_custom_col1 is not null;
 v_ccc12:=optn;
  v_json := json(v_ccc1);
  v_json_list := json_list(v_json.get('chkArray'));
  dbms_utility.comma_to_table(v_ccc12, len, a);
  v_ccc12 := '';
  for j in 1..a.count loop
  for i in 1..v_json_list.count
  loop
    v_json := json(v_json_list.get(i));
    if (v_json.get('data').get_string() = a(j)) then
    a1(i):= v_json.get('display').get_string();
    v_ccc12 :=v_ccc12||a1(i)||', ';
    end if;
  end loop;
  end loop;
  v_ccc12:=(substr(v_ccc12,0,length(v_ccc12)-2));
BEGIN
  RETURN v_ccc12;
  END;
END F_DISPOPTION;
/

/* LE build #23 Scripts */

--01

update er_codelst set codelst_custom_col='hidden-input' where codelst_type = 'studyidtype' and codelst_subtyp='nom_alt_rev'; 

commit;


update er_codelst set codelst_desc = 'Data Safety and Monitoring Plan' where codelst_type ='section' and codelst_subtyp ='section_6_5';

commit;

/* LE build #24 Scripts */

--Missing entries insert

--prtlBlnd

declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'blinding' AND CODELST_SUBTYP = 'prtlBlnd';	
	IF (v_CodeExists = 0) THEN
		insert into er_codelst (pk_codelst, codelst_type, codelst_subtyp, codelst_desc,codelst_hide)
        values (seq_er_codelst.nextval, 'blinding', 'prtlBlnd', 'Partial Blind','N');
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--05

DECLARE
  v_record_exists NUMBER := 0;
BEGIN
  SELECT COUNT(*) INTO v_record_exists FROM er_codelst WHERE CODELST_DESC = 'Resubmission Memo Response' and Codelst_Type='studyvercat';
  IF (v_record_exists = 0) THEN
    INSERT INTO ER_CODELST 
		(PK_CODELST, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, CODELST_HIDE, CODELST_SEQ)
	VALUES(
		SEQ_ER_CODELST.nextval,  'studyvercat', 'resubMemoResp', 'Resubmission Memo Response', 'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='studyvercat'));
    COMMIT;
  END IF;

END;
/

--06

update er_codelst set codelst_desc = 'Partial Blind' where codelst_type = 'blinding' and codelst_subtyp ='prtlBlnd';

update er_codelst set codelst_desc = 'Triple Blind' where codelst_type = 'blinding' and codelst_subtyp ='bindingTriple';

commit;

/* LE build #26 Scripts */

declare
v_id NUMBER :=0;
begin
	select pk_codelst into v_id from er_codelst where codelst_type = 'theme' and codelst_subtyp = 'th_default';
	
	if (v_id > 0) then
		update er_user set fk_codelst_theme = v_id where fk_codelst_theme in (select pk_codelst from er_codelst where codelst_type = 'theme' and codelst_subtyp = 'th_irb');
	end if;

	update er_codelst set codelst_hide = 'Y' where codelst_type = 'theme' and codelst_subtyp = 'th_irb';
	commit;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,358,7,'07_consolidated_Script_LindFTbuilds.sql',sysdate,'v10 #759');

commit;