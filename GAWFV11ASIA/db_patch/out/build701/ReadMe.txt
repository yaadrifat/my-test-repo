/////*********This readMe is specific to v9.3.0 build #701**********////

*********************************************************************************************
In this build we have released the "Velos eResearch Technical HowTo - Configuring Race and Ethnicity Hierarchy" document.

INF-22589 - Enhancement is fully testable now, the 'More Details' Section has been implemented in All the mentioned locations.
S-22569 and P-22510 - The pending Text Area/Multiline input has also been implemented and testable now.