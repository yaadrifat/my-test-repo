CREATE OR REPLACE FORCE VIEW EV_ADV_TYPE
(
   PK_ADVEVE,
   FK_CODLST_AETYPE,
   AE_TYPE_DESC,
   AE_DESC,
   AE_STDATE,
   AE_ENDDATE,
   FK_STUDY,
   FK_PER,
   AE_OUTNOTES,
   AE_NOTES,
   AE_ADV_GRADE,
   AE_NAME,
   AE_SEVERITY,
   AE_RELATIONSHIP,
   AE_RELATIONSHIP_DESC,
   AE_BDSYSTEM_AFF,
   PK_SITE,
   SITE_NAME,
   ADV_TYPE,
   REL_TYPE,
   AE_RECOVERY_DESC,
   FK_ACCOUNT,
   AE_CATEGORY,
  AE_TOXICITY,
  AE_TOXICTY_DESC,
  AE_GRADE_DESC
)
AS
   SELECT   ad.PK_ADVEVE,
            ad.FK_CODLST_AETYPE,
            cd.codelst_desc,
            ad.AE_DESC,
            ad.AE_STDATE,
            ad.AE_ENDDATE,
            ad.FK_STUDY,
            ad.FK_PER,
            ad.AE_OUTNOTES,
            ad.AE_NOTES,
            ad.AE_GRADE,ad.AE_NAME,
            (SELECT   codelst_desc
               FROM   sch_codelst
              WHERE   codelst_type = 'adve_severity'
                      AND pk_codelst = ad.AE_SEVERITY)
               AE_SEVERITY,
            ad.AE_RELATIONSHIP,
            rel.codelst_desc,
            (SELECT   codelst_desc
               FROM   sch_codelst
              WHERE   codelst_type = 'adve_bdsystem'
                      AND pk_codelst = ad.AE_BDSYSTEM_AFF)
               AE_BDSYSTEM_AFF,
            st.pk_site,
            st.site_name,
            cd.codelst_subtyp adv_type,
            rel.codelst_subtyp rel_type,
            (SELECT   codelst_desc
               FROM   sch_codelst
              WHERE   codelst_type = 'adve_recovery'
                      AND pk_codelst = ad.AE_RECOVERY_DESC)
               AE_RECOVERY_DESC,
            per.fk_account,
            ad.AE_CATEGORY,
            ad.AE_TOXICITY,
            ad.AE_TOXICTY_DESC,
            ad.AE_GRADE_DESC
     FROM   sch_adverseve ad,
            ER_SITE st,
            ER_PER per,
            sch_codelst cd,
            sch_codelst rel
    WHERE       per.pk_per = ad.fk_per
            AND st.pk_site = per.fk_site
            AND cd.pk_codelst = ad.FK_CODLST_AETYPE
            AND ad.AE_RELATIONSHIP = rel.pk_codelst(+);
/

CREATE OR REPLACE SYNONYM ERES.EV_ADV_TYPE FOR EV_ADV_TYPE;
CREATE OR REPLACE SYNONYM EPAT.EV_ADV_TYPE FOR EV_ADV_TYPE;


GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON EV_ADV_TYPE TO EPAT;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON EV_ADV_TYPE TO ERES;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,300,1,'01_EV_ADV_TYPE.sql',sysdate,'v9.3.0 #701');

commit;
