set define off;

create or replace
TRIGGER "ERES"."ER_NWSITES_BU_LM" 
	BEFORE UPDATE ON ER_NWSITES REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
BEGIN
		:NEW.last_modified_date := SYSDATE ;
END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,378,08,'08_ER_NWSITES_BU_LM.sql',sysdate,'v10.1 #779');

commit;