set define off;
DECLARE
      V_REP_EXISTS number;
BEGIN
    
    select count(*) INTO V_REP_EXISTS from er_report where PK_REPORT = 254 AND REP_NAME = 'Annual Accrual Trend (By Study/Site)';
    
    if V_REP_EXISTS >0
    THEN    
        update er_report set REP_FILTERKEYWORD = '[DATEFILTER]Y:studyId:orgId' where PK_REPORT = 254 AND REP_NAME = 'Annual Accrual Trend (By Study/Site)';
    END IF;
    
END;
/
set define off;
DECLARE
      V_REP_EXISTS number;
BEGIN
    
    select count(*) INTO V_REP_EXISTS from er_report where PK_REPORT = 121 AND REP_NAME = 'Annual Accrual Trend (Study Level/ All Sites)';
    
    if V_REP_EXISTS >0
    THEN    
        update er_report set REP_FILTERKEYWORD = '[DATEFILTER]Y:studyId' where PK_REPORT = 121 AND REP_NAME = 'Annual Accrual Trend (Study Level/ All Sites)';
    END IF;
    
END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,428,2,'02_UPDATE_ER_REPORT.sql',sysdate,'v11 #829');

commit;	
/