set define off;

DELETE from er_repxsl where pk_repxsl in (181,182);

commit;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,314,8,'08_ER_REPXSL_DEL.sql',sysdate,'v9.3.0 #715');

commit;