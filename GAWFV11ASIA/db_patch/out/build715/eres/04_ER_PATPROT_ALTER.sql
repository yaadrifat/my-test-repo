set define off;

Update er_patprot 
Set PATPROT_OTHR_DIS_CODE = ANATOMIC_SITE where ANATOMIC_SITE is not null;
commit;

alter table er_patprot
drop column  ANATOMIC_SITE;
commit;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,314,4,'04_ER_PATPROT_ALTER.sql',sysdate,'v9.3.0 #715');

commit;

