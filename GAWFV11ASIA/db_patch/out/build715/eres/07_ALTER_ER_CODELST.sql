set define off;

ALTER TABLE ERES.ER_CODELST MODIFY(CODELST_DESC VARCHAR2(4000 BYTE));

COMMIT;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,314,7,'07_ALTER_ER_CODELST.sql',sysdate,'v9.3.0 #715');

commit;