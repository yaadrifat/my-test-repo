set define off;

delete  from  er_repxsl where pk_repxsl in (106,107);
commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,359,4,'04_del_er_repxsl.sql',sysdate,'v10 #760');

commit;

