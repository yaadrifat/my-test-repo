set define off;
Declare
	table_check number;
	row_check number;
	update_sql clob;
	
Begin
	select count(*) into table_check from USER_TABLES
	where table_name='ER_REPORT';
	
	if(table_check>0) then
		
		select count(*) into row_check from er_report
		WHERE pk_report=128;
		
		if(row_check>0) then
		
		update_sql:='select * from (SELECT spec_id, spec_alternate_id,
				nvl(a.FK_SITE,-1) AS fk_site,
				spec_original_quantity AS qty,
				(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = spec_quantity_units) AS units,
				(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = spec_type AND codelst_type = ''specimen_type'') AS sp_type,
				(SELECT study_number FROM ER_STUDY WHERE pk_study = fk_study) AS study,
				TO_CHAR(spec_collection_date,PKG_DATEUTIL.F_GET_DATEFORMAT)AS spec_collection_date,
				(SELECT per_code FROM ER_PER WHERE pk_per = fk_per) AS patid,
				DECODE(fk_specimen,NULL,'''' ,''Yes'') AS child_yes,
				(SELECT spec_id FROM ER_SPECIMEN WHERE pk_specimen = a.fk_specimen) AS parent,
				storage_name, storage_id,
				(SELECT storage_name FROM ER_STORAGE WHERE b.fk_storage = pk_storage) AS storage_loc
				FROM ER_SPECIMEN a, ER_STORAGE b
				WHERE pk_storage (+) = a.fk_storage
				AND (('':specTyp''=''NonStudy'' AND fk_study is null AND a.fk_account = :sessAccId) or ('':specTyp''=''Study'' AND fk_study in (:studyId)))
				AND (a.fk_per is not null AND exists (select * from epat.person per, ER_PATFACILITY fac, er_usersite usr  WHERE pk_person = a.fk_per and per.fk_account =:sessAccId
				AND usr.fk_user =:sessUserId AND usr.usersite_right>=4 AND usr.fk_site = fac.fk_site  AND fac.patfacility_accessright > 0  and per.pk_person = fac.fk_per))
				AND (spec_collection_date is null or spec_collection_date BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT))
				)
				where fk_site IN (:orgId)
				order by patid, study, spec_id';
				
		update er_report set REP_SQL_CLOB=update_sql where pk_report=128;
		
		End if;
		
		select count(*) into row_check from er_report
		WHERE pk_report=130;
		
		if(row_check>0) then
		
		update_sql:='select * from (SELECT spec_id, spec_alternate_id,
			nvl(a.FK_SITE,-1) AS fk_site,
			spec_original_quantity AS qty,
			(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = spec_quantity_units) AS units,
			(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = spec_type AND codelst_type = ''specimen_type'') AS sp_type,
			(SELECT study_number FROM ER_STUDY WHERE pk_study = fk_study) AS study,
			TO_CHAR(spec_collection_date,PKG_DATEUTIL.F_GET_DATEFORMAT)AS spec_collection_date,
			(SELECT per_code FROM ER_PER WHERE pk_per = fk_per) AS patid,
			DECODE(fk_specimen,NULL,'''' ,''Yes'') AS child_yes,
			(SELECT spec_id FROM ER_SPECIMEN WHERE pk_specimen = a.fk_specimen) AS parent,
			storage_name, storage_id,
			(SELECT storage_name FROM ER_STORAGE WHERE b.fk_storage = pk_storage) AS storage_loc
			FROM ER_SPECIMEN a, ER_STORAGE b
			WHERE pk_storage (+) = a.fk_storage
			AND (a.fk_per is null OR exists (select * from epat.person per, ER_PATFACILITY fac, er_usersite usr  WHERE pk_person = a.fk_per and per.fk_account =:sessAccId
			AND usr.fk_user =:sessUserId AND usr.usersite_right>=4 AND usr.fk_site = fac.fk_site  AND fac.patfacility_accessright > 0  and per.pk_person = fac.fk_per))
			AND '':specTyp''=''Study'' AND fk_study IN (:studyId)
			AND (spec_collection_date is null or spec_collection_date BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT))
			)
			where FK_SITE IN (:orgId)
			order by study, patid, spec_id';
				
		update er_report set REP_SQL_CLOB=update_sql where pk_report=130;
		
		End if;
		
		select count(*) into row_check from er_report
		WHERE pk_report=136;
		
		if(row_check>0) then
		
		update_sql:='select * from (SELECT a.fk_account, a.fk_study, spec_id, spec_alternate_id,
			nvl(a.FK_SITE,-1) As fk_site,
			case when a.fk_study is null then ''[No Study Specified]''
			else (SELECT study_number FROM er_study WHERE pk_study = a.fk_study) end study_number,
			case when (SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_type) is null then ''-''
			else (SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_type) end spec_type,
			TO_CHAR(spec_collection_date,PKG_DATEUTIL.F_GET_DATEFORMAT) spec_collection_date,
			case when (SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_anatomic_site) is null then ''-''
			else (SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_anatomic_site) end  spec_anatomic_site,
			spec_original_quantity,
			(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_quantity_units) spec_quantity_units
			FROM er_specimen a, er_specimen_status b
			WHERE (('':specTyp''=''NonStudy'' AND a.fk_study is null AND a.fk_account = :sessAccId) or ('':specTyp''=''Study'' AND a.fk_study in (:studyId)))
			AND (a.fk_per is null OR exists (select * from epat.person per, ER_PATFACILITY fac, er_usersite usr  WHERE pk_person = a.fk_per and per.fk_account =:sessAccId
			and usr.fk_user =:sessUserId AND usr.usersite_right>=4 AND usr.fk_site = fac.fk_site  AND fac.patfacility_accessright > 0  and per.pk_person = fac.fk_per))
			AND b.fk_specimen = a.pk_specimen
			AND (spec_collection_date is null or spec_collection_date BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT))
			AND b.FK_CODELST_STATUS IN (SELECT pk_codeLst FROM er_codelst WHERE codelst_subtyp=''Depleted'')
			AND b.PK_SPECIMEN_STATUS = (SELECT MAX(c.PK_SPECIMEN_STATUS) FROM er_specimen_status c
			WHERE c.fk_specimen = a.pk_specimen
			AND c.SS_DATE = (SELECT MAX(d.SS_DATE) FROM er_specimen_status d
			WHERE d.fk_specimen = a.pk_specimen))
			)
			where FK_SITE IN (:orgId)
			ORDER BY spec_collection_date';
				
		update er_report set REP_SQL_CLOB=update_sql where pk_report=136;
		
		End if;
		
		select count(*) into row_check from er_report
		WHERE pk_report=138;
		
		if(row_check>0) then
		
		update_sql:='select * from (SELECT pk_specimen, spec_id, spec_alternate_id, a.fk_study,
			nvl(a.FK_SITE,-1) As fk_site,
			(SELECT p.SPEC_ID FROM ER_SPECIMEN p WHERE p.pk_specimen = a.fk_specimen) PSEC_ID,
			(SELECT count(*) FROM ER_SPECIMEN p WHERE p.fk_specimen = a.pk_specimen) ChildCnt,
			case when a.fk_study is null then ''[No Study Specified]''
			else (SELECT study_number FROM er_study WHERE pk_study = a.fk_study) end study_number,
			b.SS_DATE,
			(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst=b.FK_CODELST_STATUS) as Spec_Staus,
			case when (SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_type) is null then ''-''
			else (SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_type) end spec_type,
			spec_collection_date,
			(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_anatomic_site) spec_anatomic_site,
			spec_original_quantity,
			(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_quantity_units) spec_quantity_units,
			(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst=b.SS_PROC_TYPE) as SS_PROC_TYPE,
			SS_HAND_OFF_DATE, SS_PROC_DATE, SS_QUANTITY,
			(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = SS_QUANTITY_UNITS) SS_QUANTITY_UNITS,
			(SELECT USR_FIRSTNAME ||''  ''|| USR_LASTNAME FROM ER_USER where pk_user=b.FK_USER_RECEPIENT ) AS SS_RECEPIENT_NAME,
			(SELECT USR_FIRSTNAME ||''  ''|| USR_LASTNAME FROM ER_USER where pk_user=b.SS_STATUS_BY ) AS SS_STATUSBY_NAME,
			(SELECT STUDY_NUMBER FROM ER_STUDY where pk_study=b.FK_STUDY) AS SS_STUDY_NUMBER
			FROM er_specimen a, er_specimen_status b
			WHERE (('':specTyp''=''NonStudy'' AND a.fk_study is null AND a.fk_account = :sessAccId) or ('':specTyp''=''Study'' AND a.fk_study in (:studyId)))
			AND (a.fk_per is null OR exists (select * from epat.person per, ER_PATFACILITY fac, er_usersite usr  WHERE pk_person = a.fk_per and per.fk_account =:sessAccId
			and usr.fk_user =:sessUserId AND usr.usersite_right>=4 AND usr.fk_site = fac.fk_site  AND fac.patfacility_accessright > 0  and per.pk_person = fac.fk_per))
			AND b.fk_specimen = a.pk_specimen
			AND b.FK_CODELST_STATUS IN (SELECT pk_codeLst FROM er_codelst WHERE codelst_custom_col1=''process'')
			AND (spec_collection_date is null or spec_collection_date BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT))
			)
			where FK_SITE IN (:orgId)
			ORDER BY pk_specimen, spec_collection_date';
				
		update er_report set REP_SQL_CLOB=update_sql where pk_report=138;
		
		End if;
		
		select count(*) into row_check from er_report
		WHERE pk_report=142;
		
		if(row_check>0) then
		
		update_sql:='select * from (SELECT stor.pk_storage,
					nvl(spec.fk_site,-1) As fk_site,
					STORAGE_ID,
					STORAGE_NAME,
					(SELECT p.STORAGE_ID FROM ER_STORAGE p WHERE p.pk_STORAGE = stor.fk_STORAGE
					) PSTORE_ID,
					(SELECT COUNT(*) FROM ER_STORAGE p WHERE p.fk_STORAGE = stor.pk_storage
					) StoreChildCnt,
					(SELECT CODELST_DESC
					FROM er_codelst
					WHERE pk_codelst = FK_CODELST_STORAGE_TYPE
					) STORE_TYPE,
					spec.pk_specimen,
					SPEC_ID,
					(SELECT COUNT(*) FROM ER_SPECIMEN p WHERE p.fk_specimen = spec.pk_specimen
					) SpecChildCnt,
					(SELECT p.SPEC_ID FROM ER_SPECIMEN p WHERE p.pk_specimen = spec.fk_specimen
					) PSPEC_ID,
					(SELECT study_number FROM ER_STUDY WHERE pk_study = FK_STUDY
					) STUDY_NUMBER,
					(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_type
					) SPEC_TYPE,
					SPEC_ORIGINAL_QUANTITY
					|| '' ''
					||
					(SELECT CODELST_DESC
					FROM er_codelst
					WHERE pk_codelst = spec_quantity_units
					) SPEC_ORIGINAL_QUANTITY
					FROM er_storage stor,
					er_specimen spec
					WHERE spec.fk_storage = stor.pk_storage
					AND (('':specTyp''      =''NonStudy''
					AND spec.fk_study    IS NULL
					AND spec.fk_account   = :sessAccId)
					OR ('':specTyp''       =''Study''
					AND spec.fk_study    IN (:studyId)))
					AND (spec.fk_per     IS NULL
					OR EXISTS
					(SELECT *
					FROM epat.person per,
						ER_PATFACILITY fac,
						er_usersite usr
					WHERE pk_person                 = spec.fk_per
					AND per.fk_account              =:sessAccId
					AND usr.fk_user                 =:sessUserId
					AND usr.usersite_right         >=4
					AND usr.fk_site                 = fac.fk_site
					AND fac.patfacility_accessright > 0
					AND per.pk_person               = fac.fk_per
					))
					AND stor.pk_storage       IN (:storageId)
					AND (spec_collection_date IS NULL
					OR spec_collection_date BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT))
					)
					where fk_site IN (:orgId)
					ORDER BY storage_id';
				
			update er_report set REP_SQL_CLOB=update_sql where pk_report=142;
		
		End if;
		
		select count(*) into row_check from er_report
		WHERE pk_report=143;
		
		if(row_check>0) then
		
		update_sql:='select * from (SELECT a.fk_account, a.fk_study, spec_id, spec_alternate_id,
				nvl(a.FK_SITE,-1) As fk_site,
				case when a.fk_study is null then ''[No Study Specified]''
				else (SELECT study_number FROM er_study WHERE pk_study = a.fk_study) end study_number,
				case when FK_USER_PATH is null then ''[Not specified]''
				else (select usr_firstName || '' '' || usr_lastname from er_user where pk_user = FK_USER_PATH) end pathologist,
				case when FK_USER_SURG is null then ''[Not specified]''
				else (select usr_firstName || '' '' || usr_lastname from er_user where pk_user = FK_USER_SURG) end Surgeon,
				(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_type) spec_type,
				TO_CHAR(spec_collection_date,PKG_DATEUTIL.F_GET_DATEFORMAT) spec_collection_date,
				(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_anatomic_site) spec_anatomic_site,
				spec_original_quantity,
				(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_quantity_units) spec_quantity_units
				FROM er_specimen a, er_specimen_status b
				WHERE (('':specTyp''=''NonStudy'' AND a.fk_study is null AND a.fk_account = :sessAccId) or ('':specTyp''=''Study'' AND a.fk_study in (:studyId)))
				AND (a.fk_per is null OR exists (select * from epat.person per, ER_PATFACILITY fac, er_usersite usr  WHERE pk_person = a.fk_per and per.fk_account =:sessAccId
				and usr.fk_user =:sessUserId AND usr.usersite_right>=4 AND usr.fk_site = fac.fk_site  AND fac.patfacility_accessright > 0  and per.pk_person = fac.fk_per))
				AND b.fk_specimen = a.pk_specimen
				AND (spec_collection_date is null or spec_collection_date BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT))
				AND b.PK_SPECIMEN_STATUS = (SELECT MAX(c.PK_SPECIMEN_STATUS) FROM er_specimen_status c
				WHERE c.fk_specimen = a.pk_specimen AND c.SS_DATE = (SELECT MAX(d.SS_DATE) FROM er_specimen_status d WHERE d.fk_specimen = a.pk_specimen ))
				)
				where FK_SITE IN (:orgId)
				ORDER BY spec_collection_date';
				
			update er_report set REP_SQL_CLOB=update_sql where pk_report=143;
		
		End if;
		
		select count(*) into row_check from er_report
		WHERE pk_report=145;
		
		if(row_check>0) then
		
		update_sql:='select * from (SELECT pk_specimen, spec_id, spec_alternate_id, a.fk_study,
			nvl(a.fk_site,-1) As fk_site,
			(SELECT p.SPEC_ID FROM ER_SPECIMEN p WHERE p.pk_specimen = a.fk_specimen) PSEC_ID,
			(SELECT count(*) FROM ER_SPECIMEN p WHERE p.fk_specimen = a.pk_specimen) ChildCnt,
			case when a.fk_study is null then ''[No Study Specified]''
			else (SELECT study_number FROM er_study WHERE pk_study = a.fk_study) end study_number,
			b.SS_DATE,
			(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst=b.FK_CODELST_STATUS) as Spec_Staus,
			case when (SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_type) is null then ''-''
			else (SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_type) end spec_type,
			spec_collection_date,
			(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_anatomic_site) spec_anatomic_site,
			spec_original_quantity,
			(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_quantity_units) spec_quantity_units,
			(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst=b.SS_PROC_TYPE) as SS_PROC_TYPE,
			SS_HAND_OFF_DATE, SS_PROC_DATE, SS_QUANTITY,
			(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = SS_QUANTITY_UNITS) SS_QUANTITY_UNITS,
			(SELECT USR_FIRSTNAME ||''  ''|| USR_LASTNAME FROM ER_USER where pk_user=b.FK_USER_RECEPIENT ) AS SS_RECEPIENT_NAME,
			(SELECT USR_FIRSTNAME ||''  ''|| USR_LASTNAME FROM ER_USER where pk_user=b.SS_STATUS_BY ) AS SS_STATUSBY_NAME,
			(SELECT STUDY_NUMBER FROM ER_STUDY where pk_study=b.FK_STUDY) AS SS_STUDY_NUMBER
			FROM er_specimen a, er_specimen_status b
			WHERE (('':specTyp''=''NonStudy'' AND a.fk_study is null AND a.fk_account = :sessAccId) or ('':specTyp''=''Study'' AND a.fk_study in (:studyId)))
			AND (a.fk_per is null OR exists (select * from epat.person per, ER_PATFACILITY fac, er_usersite usr  WHERE pk_person = a.fk_per and per.fk_account =:sessAccId
			and usr.fk_user =:sessUserId AND usr.usersite_right>=4 AND usr.fk_site = fac.fk_site  AND fac.patfacility_accessright > 0  and per.pk_person = fac.fk_per))
			AND b.fk_specimen = a.pk_specimen
			AND (spec_collection_date is null or spec_collection_date BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT))
			)
			where fk_site IN (:orgId)
			ORDER BY pk_specimen, spec_collection_date, SS_PROC_DATE';
				
			update er_report set REP_SQL_CLOB=update_sql where pk_report=145;
		
		End if;
		
		select count(*) into row_check from er_report
		WHERE pk_report=155;
		
		if(row_check>0) then
		
		update_sql:='select * from (select
			case when (spec.FK_STUDY IS NULL AND SPEC.fk_per IS NOT NULL) THEN ''Patient''
			when (spec.FK_STUDY IS NOT NULL AND SPEC.fk_per IS NULL) THEN ''Study''
			when (spec.FK_STUDY IS NULL AND SPEC.fk_per IS NULL) THEN ''Account''
			when (spec.FK_STUDY IS NOT NULL AND SPEC.fk_per IS NOT NULL) THEN ''Patient Study''
			end as spec_Kind,
			nvl(SPEC.FK_SITE,-1) As fk_site,
			spec.pk_specimen, SPEC_ID,spec_alternate_id,
			(SELECT count(*) FROM ER_SPECIMEN p WHERE p.fk_specimen = spec.pk_specimen) SpecChildCnt,
			(SELECT p.SPEC_ID FROM ER_SPECIMEN p WHERE p.pk_specimen = spec.fk_specimen) PSPEC_ID,
			(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_type) SPEC_TYPE,
			(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = (SELECT s.FK_CODELST_STATUS FROM er_specimen_status s
			WHERE spec.pk_specimen = s.fk_specimen AND s.PK_SPECIMEN_STATUS = (SELECT MAX(ss.PK_SPECIMEN_STATUS) FROM er_specimen_status ss
			WHERE ss.fk_specimen = spec.pk_specimen AND ss.SS_DATE = (SELECT MAX(ss1.SS_DATE) FROM er_specimen_status ss1
			WHERE ss1.fk_specimen = spec.pk_specimen )))) SPEC_STATUS,
			SPEC_ORIGINAL_QUANTITY || '' ''||(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_quantity_units) SPEC_ORIGINAL_QUANTITY,
			TO_CHAR(spec_collection_date,PKG_DATEUTIL.F_GET_DATEFORMAT)AS spec_collection_date,
			(SELECT site_name FROM ER_SITE WHERE pk_site =  FK_SITE) AS spec_orgn,
			(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = SPEC_ANATOMIC_SITE) SPEC_ANATOMIC_SITE,
			(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = SPEC_TISSUE_SIDE) SPEC_TISSUE_SIDE,
			(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = SPEC_PATHOLOGY_STAT) SPEC_PATHOLOGY_STAT,
			(SELECT study_number FROM ER_STUDY WHERE pk_study = spec.FK_STUDY) STUDY_NUMBER,
			(SELECT per_code FROM ER_PER WHERE pk_per = SPEC.fk_per) AS patid,
			DECODE(spec.fk_specimen,NULL,'''' ,''Yes'') AS child_yes,
			(SELECT spec_id FROM ER_SPECIMEN WHERE pk_specimen = spec.fk_specimen) AS spec_parent,
			stor.pk_storage,STORAGE_ID,STORAGE_NAME,
			(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = FK_CODELST_STORAGE_TYPE) STORE_TYPE,
			(SELECT USR_FIRSTNAME ||''  ''|| USR_LASTNAME  FROM ER_USER where pk_user=spec.FK_USER_PATH ) AS SPEC_PATHOLOGY_NAME,
			(SELECT USR_FIRSTNAME ||''  ''|| USR_LASTNAME FROM ER_USER where pk_user=spec.FK_USER_SURG ) AS SPEC_SURGEON_NAME,
			(SELECT USR_FIRSTNAME ||''  ''|| USR_LASTNAME FROM ER_USER where pk_user=spec.FK_USER_COLLTECH ) AS SPEC_COLLTECH_NAME,
			(SELECT USR_FIRSTNAME ||''  ''|| USR_LASTNAME FROM ER_USER where pk_user=spec.FK_USER_PROCTECH ) AS SPEC_PROCTECH_NAME
			from er_specimen spec, er_storage stor
			where stor.pk_storage(+) = spec.fk_storage
			AND (('':specTyp''=''NonStudy'' AND spec.fk_study is null AND spec.fk_account = :sessAccId) or ('':specTyp''=''Study'' AND spec.fk_study in (:studyId)))
			AND (spec.fk_per is null OR exists (select * from epat.person per, ER_PATFACILITY fac, er_usersite usr  WHERE pk_person = spec.fk_per and per.fk_account =:sessAccId
			and usr.fk_user=:sessUserId AND usr.usersite_right>=4 AND usr.fk_site = fac.fk_site  AND fac.patfacility_accessright > 0  and per.pk_person = fac.fk_per))
			AND (spec_collection_date is null or spec_collection_date BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT))
			)
			where FK_SITE IN (:orgId)
			order by storage_id';
				
			update er_report set REP_SQL_CLOB=update_sql where pk_report=155;
		
		End if;	
		
	End if;

commit;	
End;
/
 INSERT INTO track_patches
VALUES(seq_track_patches.nextval,396,4,'04_ER_REPORT.sql',sysdate,'v11 #797');

commit;