set define off;
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='studyteam' and CODELST_SUBTYP= 'stmradioname';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'studyteam','stmradioname','Radio User Data1','N',8,'radio','{radioArray:[{data: ''option1'', display:''MDACC/Regional Centers''}, {data: ''option2'', display:''Non-MDACC Hospital/Clinic Healthcare Facility/Harris County Hospital District Facility''}, {data: ''option3'', display:''Community Center/Shopping Mall (e.g. religious facility)''}]}');
 end if;
   v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='studyteam' and CODELST_SUBTYP= 'datestudyteam';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'studyteam','datestudyteam','Date StudyTeam','N',9,'','');
 
  end if;
  
    v_record_exists  := 0;  

  Select count(*) into v_record_exists
    from er_codelst where CODELST_TYPE='studyteam' and CODELST_SUBTYP= 'stmdatename';
  if (v_record_exists = 0) then
Insert into er_codelst (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1) 
values (SEQ_ER_CODELST.nextval,null,'studyteam','stmdatename','Date Org Data1','N',10,'date','');
 
  end if;
  
  
 end;
 /
 INSERT INTO track_patches
VALUES(seq_track_patches.nextval,396,3,'03_er_codelst_studyteam.sql',sysdate,'v11 #797');

commit;