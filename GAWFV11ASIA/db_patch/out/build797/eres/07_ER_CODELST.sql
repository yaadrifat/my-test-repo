SET DEFINE OFF;
DECLARE
	row_check number;
	
BEGIN
	select count(*) into row_check from er_codelst
	where codelst_type='nwusersrole';
	
	if(row_check=0) then
	
		insert into ER_CODELST (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ) 
		select SEQ_ER_CODELST.nextval,'nwusersrole',CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ
		from er_codelst where codelst_type = 'role';
	
	End if;
	
commit;
End;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,396,7,'07_ER_CODELST.sql',sysdate,'v11 #797');

commit;