----------codelst_type changed for Relation Type in context of networks--------------

SET DEFINE OFF;
DECLARE
	row_check number;
	
BEGIN
	select count(*) into row_check from er_codelst
	where codelst_type='relnshipTyp';
	
	if(row_check=0) then
	
		insert into ER_CODELST (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT) 
		select SEQ_ER_CODELST.nextval,'relnshipTyp',CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT
		from er_codelst where codelst_type = 'site_type';
	
	End if;
	
commit;
End;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,396,2,'02_ER_CODELST.sql',sysdate,'v11 #797');

commit;