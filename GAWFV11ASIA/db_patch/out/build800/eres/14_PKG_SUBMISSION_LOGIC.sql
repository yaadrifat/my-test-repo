create or replace PACKAGE      PKG_SUBMISSION_LOGIC AS
/******************************************************************************
   NAME:       PKG_SUBMISSION_LOGIC
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        5/28/2009   Sonia Abrol          1. Created this package.
******************************************************************************/


  Function f_check_irb(p_study IN NUMBER) return t_nested_results_table;

  Function f_check_cro(p_study IN NUMBER) return t_nested_results_table;

  Function f_check_rsc(p_study IN NUMBER) return t_nested_results_table;


  Function f_check_cic(p_study IN NUMBER) return t_nested_results_table;

  Function f_check_src(p_study IN NUMBER) return t_nested_results_table;
  
  Procedure check_generate_message(p_study in number, p_activity_trigger in varchar2, p_activity_trigger_code in varchar2,
  p_possible_delValues in varchar2,p_notrequired_ifnotmet in varchar2, p_display_name in varchar2, p_display_category in varchar2, 
  v_tab_submission_results in out t_nested_results_table);

END PKG_SUBMISSION_LOGIC;
/

create or replace PACKAGE BODY      PKG_SUBMISSION_LOGIC AS
/******************************************************************************
   NAME:       PKG_SUBMISSION_LOGIC
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        5/28/2009   Sonia Abrol          1. Created this package.

   This package is designed to club together custom logic needed to validate submissions
   There can be one or more functions written to perform validation checks. All functions defined in this package will
   follow following conventions:

   1. Pls see template function - f_check_cro(p_study IN NUMBER) as an example.
   2. The Function will take one parameter : PK of the study
   3. The function will return a table - t_nested_results_table
   4. The table will contain following information:
          result: Pass/Fail information.Possible values:
                  0 : Pass
                 -1 : Fail
                 -2 : Not Required

          result_text : Text to be displayed for results
          TestText - Descriptive text (if any) for the checks performed
   5. The table can return one or more rows to send check results


******************************************************************************/

Function f_check_irb(p_study IN NUMBER) return t_nested_results_table
AS
    v_tab_submission_results t_nested_results_table ;

    v_result number;
    v_result_text varchar2(1000);
    v_doc_count number;
    v_testText varchar2(1000);
    v_isPiMajAuthor NUMBER := 0;
    v_isMdaccSelect NUMBER := 0;
    v_mdacc_count number;
    v_industry_count number;
    v_version_number number;
    v_indide_count number;
    v_schevnt_count number;
begin

    v_tab_submission_results := t_nested_results_table();

    v_testText := 'Check for Uploaded Document in Protocol Prioritization Category';

    select MAX(f_to_number(studyver_number)) into v_version_number   from er_studyver where fk_study=p_study ;

	  select count(*)
	  into v_doc_count
	  from er_studyver v,er_studyapndx x
	  where v.fk_study = p_study and v.studyver_category = (select pk_codelst from er_codelst where codelst_type = 'studyvercat'
	   and codelst_subtyp = 'dept_chair') and x.fk_studyver = pk_studyver and studyapndx_type = 'file' and f_to_number(v.studyver_number)=v_version_number;

	   if v_doc_count > 0 then
			v_result := 0;
			v_result_text := 'Pass';
		else
			v_result := -1;
			v_result_text := 'Fail: No Document Uploaded in Protocol Prioritization Category';
		end if;

	  v_tab_submission_results.extend;
	  v_tab_submission_results(v_tab_submission_results.count) := tab_submission_results(v_result, v_result_text,v_testText );

     v_testText := 'Check for Uploaded Document in Study Calendar/Schedule of Events Category';

     select count(*)
	  into v_schevnt_count
	  from er_studyver v,er_studyapndx x
	  where v.fk_study = p_study and v.studyver_category = (select pk_codelst from er_codelst where codelst_type = 'studyvercat'
	   and codelst_subtyp = 'studCalSchEvnts') and x.fk_studyver = pk_studyver and studyapndx_type = 'file' and f_to_number(v.studyver_number)=v_version_number;

	   if v_schevnt_count > 0 then
			v_result := 0;
			v_result_text := 'Pass';
		else
			v_result := -1;
			v_result_text := 'Fail: No Document Uploaded in Study Calendar/Schedule of Events Category';
		end if;

	  v_tab_submission_results.extend;
	  v_tab_submission_results(v_tab_submission_results.count) := tab_submission_results(v_result, v_result_text,v_testText );


    select count(*)
    into v_industry_count
    from er_studyid
    where fk_study=p_study and studyid_id='indstrStdy';

    IF v_industry_count > 0 THEN

      v_testText := 'Check for Uploaded Document in Protocol Category';

      select count(*)
      into v_doc_count
      from er_studyver v,er_studyapndx x
      where v.fk_study = p_study and v.studyver_category = (select pk_codelst from er_codelst where codelst_type = 'studyvercat'
       and codelst_subtyp = 'protocol') and x.fk_studyver = pk_studyver and studyapndx_type = 'file' and f_to_number(v.studyver_number)=v_version_number;

       if v_doc_count > 0 then
          v_result := 0;
          v_result_text := 'Pass';
      else
          v_result := -1;
          v_result_text := 'Fail: No Document Uploaded in Protocol Category';
      end if;

      v_tab_submission_results.extend;
      v_tab_submission_results(v_tab_submission_results.count) := tab_submission_results(v_result, v_result_text,v_testText );

    END IF;

  select count(*)
  into v_mdacc_count
  from er_study_indide esi, er_codelst ec
  where esi.fk_study= p_study
  and indide_type=1
  and esi.fk_codelst_indide_holder = (select pk_codelst from er_codelst where codelst_type='INDIDEHolder' and codelst_desc='MDACC');

  IF v_mdacc_count > 0 THEN
    v_testText := 'Check for Uploaded Document in Investigator Brochure Category';

	  select count(*)
	  into v_doc_count
	  from er_studyver v,er_studyapndx x
	  where v.fk_study = p_study and v.studyver_category = (select pk_codelst from er_codelst where codelst_type = 'studyvercat'
	   and codelst_subtyp = 'inv_brochure') and x.fk_studyver = pk_studyver and studyapndx_type = 'file' and f_to_number(v.studyver_number)=v_version_number;

	   if v_doc_count > 0 then
			v_result := 0;
			v_result_text := 'Pass';
		else
			v_result := -1;
			v_result_text := 'Fail: No Document Uploaded in Investigator Brochure Category';
		end if;

	  v_tab_submission_results.extend;
	  v_tab_submission_results(v_tab_submission_results.count) := tab_submission_results(v_result, v_result_text,v_testText );

  END IF;
    return v_tab_submission_results;

end;

Function f_check_cro(p_study IN NUMBER) return t_nested_results_table
AS
    v_tab_submission_results t_nested_results_table ;

    v_result number;
    v_result_text varchar2(1000);

    v_testText varchar2(1000);
begin

    v_tab_submission_results := t_nested_results_table();

    v_testText := 'Check for Central Research Office';

    v_result := 0;
    v_result_text := 'Pass';

    v_tab_submission_results.extend;
    v_tab_submission_results(v_tab_submission_results.count) := tab_submission_results(v_result, v_result_text,v_testText );


    return v_tab_submission_results;

end;



Function f_check_rsc(p_study IN NUMBER) return t_nested_results_table
AS
    v_tab_submission_results t_nested_results_table ;

    v_result number;
    v_result_text varchar2(1000);

    v_testText varchar2(1000);
begin

    v_tab_submission_results := t_nested_results_table();

    v_testText := 'Radiation Safety Committee';

    v_result := 0;
    v_result_text := 'Pass';

    v_tab_submission_results.extend;
    v_tab_submission_results(v_tab_submission_results.count) := tab_submission_results(v_result, v_result_text,v_testText );


    return v_tab_submission_results;

end;

Function f_check_cic(p_study IN NUMBER) return t_nested_results_table
AS
    v_tab_submission_results t_nested_results_table ;

    v_result number;
    v_result_text varchar2(1000);

    v_testText varchar2(1000);
begin

    v_tab_submission_results := t_nested_results_table();

    v_testText := 'Conflict of Interest Committee';

    v_result := 0;
    v_result_text := 'Pass';

    v_tab_submission_results.extend;
    v_tab_submission_results(v_tab_submission_results.count) := tab_submission_results(v_result, v_result_text,v_testText );


    return v_tab_submission_results;

end;


Function f_check_src(p_study IN NUMBER) return t_nested_results_table
AS
    v_tab_submission_results t_nested_results_table ;

    v_result number;
    v_result_text varchar2(1000);

    v_testText varchar2(1000);
begin

    v_tab_submission_results := t_nested_results_table();

    v_testText := 'Scientific Review Committee';

    v_result := 0;
    v_result_text := 'Pass';

    v_tab_submission_results.extend;
    v_tab_submission_results(v_tab_submission_results.count) := tab_submission_results(v_result, v_result_text,v_testText );


    return v_tab_submission_results;

end;
 
 Procedure check_generate_message(p_study in number, p_activity_trigger in varchar2, p_activity_trigger_code in varchar2,
 p_possible_delValues in varchar2,p_notrequired_ifnotmet in varchar2, p_display_name in varchar2, p_display_category in varchar2, 
 v_tab_submission_results in out t_nested_results_table)
 is
 v_result number;
 v_result_text varchar2(1000);
 v_testText varchar2(1000);
 begin
    v_testText := 'Check for '''||p_display_name||''' '||p_display_category;
    
    --Calculating the rules
    SELECT PKG_WF_GENERIC_RULES.F_multi_combination_activity(p_study,p_activity_trigger, p_activity_trigger_code,p_possible_delValues,p_notrequired_ifnotmet ) into v_result FROM DUAL ;
    
    if v_result =1 then
      v_result_text := 'Pass';
    else
      v_result_text := 'Missing '''||p_display_name||''' '||p_display_category;
    end if;
    dbms_output.put_line('v_result---->'||v_result);
    if v_result!=-1 then
      if v_result=0 then
        v_result:=-1;
      else if v_result=1 then
        v_result:=0;
      end if;
      end if;
      v_tab_submission_results.extend;
      v_tab_submission_results(v_tab_submission_results.count) := tab_submission_results(v_result, v_result_text,v_testText );
    end if;
 end;

END PKG_SUBMISSION_LOGIC;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,399,14,'14_PKG_SUBMISSION_LOGIC.sql',sysdate,'v11 #800');
commit; 
  