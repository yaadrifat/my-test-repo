Declare
extCheck number;
Begin
select count(*) into extCheck from er_ctrltab where ctrl_key='app_rights' and ctrl_value='QRYDASH';
if(extCheck=1) then
update er_ctrltab set CTRL_CUSTOM_COL1='V' where ctrl_key='app_rights' and ctrl_value='QRYDASH';
commit;
End If;
select count(*) into extCheck from er_ctrltab where ctrl_key='hid_rights' and ctrl_value='QRYDASH';
if(extCheck=0) then
insert into er_ctrltab (PK_CTRLTAB,CTRL_VALUE,CTRL_DESC,CTRL_KEY,CTRL_SEQ) values (SEQ_ER_CTRLTAB.nextval,'QRYDASH','MODQRYDASH','hid_rights',19);
commit;
End If;
update er_account set ac_modright=SUBSTR(ac_modright,0,18)||1;
commit;
update er_account set ac_modright=SUBSTR(ac_modright,0,15)||0||SUBSTR(ac_modright,17,19);
commit;
End;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,351,2,'02_er_ctrltabupdate.sql',sysdate,'v10 #752');

commit;