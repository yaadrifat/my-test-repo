set define off;

DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where column_name = 'FK_SUBMISSION_STATUS' 
      and table_name = 'ER_SUBMISSION_PROVISO';

  if (v_column_exists = 0) then
      execute immediate 'alter table ER_SUBMISSION_PROVISO add (FK_SUBMISSION_STATUS NUMBER)';
  end if;
  commit;
end;

/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,380,10,'10_Alter_er_sub_prov.sql',sysdate,'v10.1 #781');

commit;