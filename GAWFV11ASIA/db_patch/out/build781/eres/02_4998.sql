set define off;
 
DECLARE 
	table_check number;
	row_check number;
	
BEGIN
	SELECT count(*)
	INTO table_check
	FROM USER_TABLES
	WHERE TABLE_NAME = 'ER_BROWSERCONF';
	
	
	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM ER_BROWSERCONF
	    WHERE fk_browser = (select pk_browser from er_browser where browser_module='irbReview') AND browserconf_colname='SUBMISSION_REVIEWERS';
		
		IF (row_check > 0) then	
			UPDATE 
				ER_BROWSERCONF
			SET 
				BROWSERCONF_SETTINGS = '{"key":"SUBMISSION_REVIEWERS", "label":"Reviewer(s)", "sortable":true, "resizeable":true,"hideable":true}' 
			WHERE 
				fk_browser = (select pk_browser from er_browser where browser_module='irbReview') AND browserconf_colname='SUBMISSION_REVIEWERS';
		END IF;
	
	END IF;

	COMMIT;
	
END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,380,2,'02_4998.sql',sysdate,'v10.1 #781');

commit;

