set define off;
 
DECLARE 
	table_check number;
	row_check number;
	
BEGIN
	SELECT count(*)
	INTO table_check
	FROM USER_TABLES
	WHERE TABLE_NAME = 'ER_BROWSERCONF';
	
	
	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM ER_BROWSERCONF
	    WHERE fk_browser = (select pk_browser from er_browser where browser_module='irbSub') AND browserconf_colname='SUBMISSION_BOARD_NAME';
		
		IF (row_check > 0) then	
			UPDATE ER_BROWSERCONF
			SET BROWSERCONF_SETTINGS = '{"key":"SUBMISSION_BOARD_NAME", "label":"Review Board", "sortable":true, "resizeable":true,"hideable":true}' 
			WHERE fk_browser = (select pk_browser from er_browser where browser_module='irbSub') AND browserconf_colname='SUBMISSION_BOARD_NAME';
		END IF;
	
	END IF;

	COMMIT;
	
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,380,5,'05_bug_4159.sql',sysdate,'v10.1 #781');

commit;


