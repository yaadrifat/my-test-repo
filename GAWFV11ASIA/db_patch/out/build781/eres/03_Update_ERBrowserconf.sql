set define off;
 
DECLARE 
	table_check number;
	row_check number;
	
BEGIN
	SELECT count(*)
	INTO table_check
	FROM USER_TABLES
	WHERE TABLE_NAME = 'ER_BROWSERCONF';
	
	
	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM ER_BROWSERCONF
	    WHERE fk_browser = (select pk_browser from er_browser where browser_module='irbMeetingTopic') AND (browserconf_colname='DEL_LINK' );
		IF (row_check > 0) then	
			UPDATE 
				ER_BROWSERCONF
			SET 
				BROWSERCONF_SETTINGS = '{"label":"","format":"delLink","key":"DEL_LINK","hideable":false}'
			WHERE 
			fk_browser = (select pk_browser from er_browser where browser_module='irbMeetingTopic') 
			AND PK_BROWSERCONF = (select pk_browserconf from er_browserconf 
			where fk_browser = (select pk_browser from er_browser where browser_module='irbMeetingTopic')AND browserconf_colname='DEL_LINK' );
		END IF;
	END IF;
	COMMIT;
	
END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,380,3,'03_Update_ERBrowserconf.sql',sysdate,'v10.1 #781');

commit;
