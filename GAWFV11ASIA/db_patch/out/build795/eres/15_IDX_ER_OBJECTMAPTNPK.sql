set define off;

DECLARE
    i INTEGER;
BEGIN
    i := 0;
    SELECT COUNT(*) INTO i FROM user_indexes WHERE index_name = 'IDX_ER_OBJECT_MAP_TNAMPK';
    IF i > 0 THEN
        EXECUTE IMMEDIATE 'DROP INDEX IDX_ER_OBJECT_MAP_TNAMPK';
    END IF;
END;
/

DECLARE
v_indexName VARCHAR2(200) := '';
BEGIN
	SELECT distinct a1.Index_name into v_indexName FROM USER_IND_COLUMNS a1 WHERE a1.COLUMN_NAME = 'TABLE_PK' OR a1.COLUMN_NAME ='TABLE_NAME' AND a1.table_name = 'ER_OBJECT_MAP' AND a1.Index_name NOT IN (SELECT b1.Index_name FROM USER_IND_COLUMNS b1 WHERE (a1.table_name= b1.table_name AND a1.index_name   = b1.index_name) AND b1.COLUMN_NAME <> 'TABLE_PK' OR a1.COLUMN_NAME   <> 'TABLE_NAME');
	
  EXCEPTION WHEN NO_DATA_FOUND THEN
		v_indexName := v_indexName;
    
	if (length(v_indexName) IS NULL) then
		
EXECUTE IMMEDIATE 'CREATE INDEX ERES.IDX_ER_OBJECT_MAP_TNAMPK ON ERES.ER_OBJECT_MAP (TABLE_NAME, TABLE_PK) LOGGING  TABLESPACE ERES_HUGE NOPARALLEL';

END IF;
	
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,394,15,'15_IDX_ER_OBJECTMAPTNPK.sql',sysdate,'v11 #795');
commit;
