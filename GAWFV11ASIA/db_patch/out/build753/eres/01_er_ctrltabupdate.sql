Declare
tcount number;
Begin
select count(*) into tcount from er_ctrltab where ctrl_key='app_rights' and ctrl_value='QRYDASH';
if(tcount>0) then
update er_ctrltab set ctrl_desc='<B>Dashboard</B>' where ctrl_key='app_rights' and ctrl_value='QRYDASH';
commit;
end if;
select count(*) into tcount from er_ctrltab where ctrl_key='module' and ctrl_value='MODDASH';
if(tcount>0) then
update er_ctrltab set ctrl_desc='DO NOT USE or CHECK - Query Dashboard' where ctrl_key='module' and ctrl_value='MODDASH';
commit;
end if;
End;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,352,1,'01_er_ctrltabupdate.sql',sysdate,'v10 #753');

commit;