set define off;
DECLARE
index_exists NUMBER;
BEGIN
index_exists := 0;
SELECT count(*) INTO index_exists FROM user_indexes 
WHERE index_name='IDX_PATPROT_STAT' AND table_name = 'ER_PATPROT';

IF index_exists > 0 THEN
execute immediate 'drop index eres.IDX_PATPROT_STAT';
dbms_output.put_line('Dropped');
END IF;
execute immediate 'CREATE  INDEX "ERES"."IDX_PATPROT_STAT" ON "ERES"."ER_PATPROT" ("PATPROT_STAT")';
dbms_output.put_line('Created');

SELECT count(*) INTO index_exists FROM user_indexes 
WHERE index_name='IDX_ER_CTRLTAB_KEY' AND table_name = 'ER_CTRLTAB';
IF index_exists > 0 THEN
execute immediate 'drop index eres.IDX_ER_CTRLTAB_KEY';
dbms_output.put_line('Dropped');
END IF;
execute immediate 'CREATE  INDEX "ERES"."IDX_ER_CTRLTAB_KEY" ON "ERES"."ER_CTRLTAB" ("CTRL_KEY","CTRL_VALUE")';
dbms_output.put_line('Created');
SELECT count(*) INTO index_exists FROM user_indexes 
WHERE index_name='IDX_PS_PER_STUDY' AND table_name = 'ER_PATSTUDYSTAT';
IF index_exists > 0 THEN
execute immediate 'drop index eres.IDX_PS_PER_STUDY';
dbms_output.put_line('Dropped');
END IF;
execute immediate 'CREATE INDEX "ERES"."IDX_PS_PER_STUDY" ON "ERES"."ER_PATSTUDYSTAT" ("FK_PER","FK_STUDY","FK_CODELST_STAT")';
dbms_output.put_line('Created');
END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,418,2,'02_invoice_perf_indexes.sql',sysdate,'v11 #819');
commit;
/