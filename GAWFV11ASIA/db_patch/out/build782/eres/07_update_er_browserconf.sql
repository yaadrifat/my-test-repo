set define off;

DECLARE 
	table_check number;
	row_check number;
  
BEGIN
	SELECT count(*)
	INTO table_check
	FROM USER_TABLES
	WHERE TABLE_NAME = 'ER_BROWSERCONF';
  
 
	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM ER_BROWSERCONF
	    WHERE fk_browser = (select pk_browser from er_browser where browser_module='irbPend') AND browserconf_colname='DELETE_LINK';
		
		IF (row_check > 0) then	
  
		UPDATE 
			ER_BROWSERCONF
			SET 
			BROWSERCONF_SETTINGS = '{"key":"DELETE_LINK","label":"Delete Letter","format":"deleteLink", "sortable":false, "resizeable":true,"hideable":false}'
			WHERE 
			fk_browser = (select pk_browser from er_browser where browser_module='irbPend') AND browserconf_colname='DELETE_LINK';
		END IF;
	
	END IF;
	
	COMMIT;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,381,7,'07_update_er_browserconf.sql',sysdate,'v10.1 #782');

commit;
