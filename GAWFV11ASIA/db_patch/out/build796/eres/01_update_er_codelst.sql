set define off;
Declare
	codelst_check number;
Begin
	select count(*) into codelst_check from er_codelst where CODELST_TYPE='studyidtype' and CODELST_SUBTYP='STDY_TYP_M';
	
	if(codelst_check>0) Then
  DBMS_OUTPUT.PUT_LINE('test ' || codelst_check);
  update er_codelst 
  SET codelst_desc='<font>Study Type</font><img onmouseover="return overlib(''Nature of the investigation: interventional, observational/correlative, or unknown. Interventional studies are those in which experimental units (human participants or groups of humans or part of human participants) are assigned by an investigator based on a protocol to receive one or more specific interventions (or no intervention) and are then followed to evaluate the effects on outcomes. The assignment of the intervention may or may not be random. Interventions include but are not restricted to drugs, cells and other biological products, surgical procedures, radiologic procedures, devices, behavioral treatments, process-of-care changes, preventive care, etc. Observational/Correlative studies are those in which outcomes are assessed in pre-defined experimental units (human participants or groups of humans or part of human participants) and the investigators do not seek to intervene (i.e., the investigator does not assign participants to specific interventions) but simply observe the course of events. Subjects in the study may receive diagnostic, therapeutic, or other interventions, but the investigator does not assign specific interventions to the subjects of the study.'',CAPTION,''Study Type'',RIGHT, ABOVE,OFFSETY,70,WRAP);" onmouseout="return nd();" alt="" src="../images/jpg/help.jpg" border="0">'
	where CODELST_TYPE='studyidtype' and CODELST_SUBTYP='STDY_TYP_M'; 
	End if;
END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,395,1,'01_update_er_codelst.sql',sysdate,'v11 #796');
commit;