SET DEFINE OFF;
create or replace TRIGGER "ERES"."ER_STORAGE_AD0"
AFTER DELETE
ON ERES.ER_STORAGE
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(2000);
  usr VARCHAR(2000);

begin

  usr := Getuser(:old.last_modified_by);

  select seq_audit.nextval into raid from dual;

audit_trail.record_transaction
    (raid, 'ER_STORAGE', :old.rid, 'D', usr);
    
      Audit_Trail.column_delete (raid, 'PK_STORAGE', :OLD.PK_STORAGE);
      Audit_Trail.column_delete (raid, 'STORAGE_ID', :OLD.STORAGE_ID);
      Audit_Trail.column_delete (raid, 'STORAGE_NAME', :OLD.STORAGE_NAME);
      Audit_Trail.column_delete (raid, 'FK_CODELST_STORAGE_TYPE', :OLD.FK_CODELST_STORAGE_TYPE);	
      Audit_Trail.column_delete (raid, 'FK_STORAGE', :OLD.FK_STORAGE);
      Audit_Trail.column_delete (raid, 'STORAGE_CAP_NUMBER', :OLD.STORAGE_CAP_NUMBER); 
      Audit_Trail.column_delete (raid, 'FK_CODELST_CAPACITY_UNITS', :OLD.FK_CODELST_CAPACITY_UNITS); 
      Audit_Trail.column_delete (raid, 'STORAGE_DIM1_CELLNUM', :OLD.STORAGE_DIM1_CELLNUM); 
      Audit_Trail.column_delete (raid, 'STORAGE_DIM1_NAMING', :OLD.STORAGE_DIM1_NAMING);
      Audit_Trail.column_delete (raid, 'STORAGE_DIM1_ORDER', :OLD.STORAGE_DIM1_ORDER);
      Audit_Trail.column_delete (raid, 'STORAGE_DIM2_CELLNUM', :OLD.STORAGE_DIM2_CELLNUM);
      Audit_Trail.column_delete (raid, 'STORAGE_DIM2_NAMING', :OLD.STORAGE_DIM2_NAMING);
      Audit_Trail.column_delete (raid, 'STORAGE_DIM2_ORDER', :OLD.STORAGE_DIM2_ORDER);
      Audit_Trail.column_delete (raid, 'STORAGE_NOTES', :OLD.STORAGE_NOTES);
      Audit_Trail.column_delete (raid, 'STORAGE_AVAIL_UNIT_COUNT', :OLD.STORAGE_AVAIL_UNIT_COUNT);
      Audit_Trail.column_delete (raid, 'STORAGE_COORDINATE_X', :OLD.STORAGE_COORDINATE_X);
      Audit_Trail.column_delete (raid, 'STORAGE_COORDINATE_Y', :OLD.STORAGE_COORDINATE_Y);
      Audit_Trail.column_delete (raid, 'STORAGE_LABEL', :OLD.STORAGE_LABEL);
      Audit_Trail.column_delete (raid, 'RID', :OLD.RID);
      Audit_Trail.column_delete (raid, 'CREATOR', :OLD.CREATOR);
      Audit_Trail.column_delete (raid, 'CREATED_ON', TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat));
      Audit_Trail.column_delete (raid, 'LAST_MODIFIED_BY', :OLD.LAST_MODIFIED_BY);
      Audit_Trail.column_delete (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat));
      Audit_Trail.column_delete (raid, 'IP_ADD', :OLD.IP_ADD);
      Audit_Trail.column_delete (raid, 'FK_CODELST_CHILD_STYPE', :OLD.FK_CODELST_CHILD_STYPE);
      Audit_Trail.column_delete (raid, 'FK_ACCOUNT', :OLD.FK_ACCOUNT); 
      Audit_Trail.column_delete (raid, 'STORAGE_ISTEMPLATE', :OLD.STORAGE_ISTEMPLATE);
      Audit_Trail.column_delete (raid, 'STORAGE_TEMPLATE_TYPE', :OLD.STORAGE_TEMPLATE_TYPE);
      Audit_Trail.column_delete (raid, 'STORAGE_ALTERNALEID', :OLD.STORAGE_ALTERNALEID);
      Audit_Trail.column_delete (raid, 'STORAGE_UNIT_CLASS', :OLD.STORAGE_UNIT_CLASS);
      Audit_Trail.column_delete (raid, 'STORAGE_KIT_CATEGORY', :OLD.STORAGE_KIT_CATEGORY);
      Audit_Trail.column_delete (raid, 'FK_STORAGE_KIT', :OLD.FK_STORAGE_KIT);
      Audit_Trail.column_delete (raid, 'STORAGE_MULTI_SPECIMEN', :OLD.STORAGE_MULTI_SPECIMEN);
      
--Created by Manimaran for Audit delete
deleted_data :=
to_char(:old.PK_STORAGE) || '|' ||
:old.STORAGE_ID || '|' ||
:old.STORAGE_NAME || '|' ||
to_char(:old.FK_CODELST_STORAGE_TYPE) || '|' ||
to_char(:old.FK_STORAGE) || '|' ||
to_char(:old.STORAGE_CAP_NUMBER) || '|' ||
to_char(:old.FK_CODELST_CAPACITY_UNITS) || '|' ||
--:old.STORAGE_STATUS || '|' ||--KM
to_char(:old.STORAGE_DIM1_CELLNUM) || '|' ||
:old.STORAGE_DIM1_NAMING || '|' ||
:old.STORAGE_DIM1_ORDER || '|' ||
to_char(:old.STORAGE_DIM2_CELLNUM) || '|' ||
:old.STORAGE_DIM2_NAMING || '|' ||
:old.STORAGE_DIM2_ORDER || '|' ||
to_char(:old.STORAGE_AVAIL_UNIT_COUNT) || '|' ||
to_char(:old.STORAGE_COORDINATE_X) || '|' ||
to_char(:old.STORAGE_COORDINATE_Y) || '|' ||
:old.STORAGE_LABEL || '|' ||
to_char(:old.RID) || '|' ||
to_char(:old.CREATOR) || '|' ||
to_char(:old.LAST_MODIFIED_BY) || '|' ||
to_char(:old.LAST_MODIFIED_DATE) || '|' ||
to_char(:old.CREATED_ON) || '|' ||
:old.IP_ADD || '|' ||
TO_CHAR(:OLD.FK_ACCOUNT)|| '|' ||
to_char(:old.STORAGE_ISTEMPLATE)|| '|' ||
to_char(:old.STORAGE_TEMPLATE_TYPE)|| '|' ||
:old.STORAGE_ALTERNALEID|| '|' ||
to_char(:old.STORAGE_UNIT_CLASS)|| '|' ||
to_char(:old.STORAGE_KIT_CATEGORY)|| '|' ||
to_char(:old.FK_STORAGE_KIT)|| '|' ||  --YK Bug#5827
to_char(:old.STORAGE_MULTI_SPECIMEN);

--insert into AUDIT_DELETE
--(raid, row_data) values (raid, deleted_data);
END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,417,5,'05_ER_STORAGE_AD0.sql',sysdate,'v11 #818');
commit;
/