set define off;

declare
columnExists number := 0;
begin
	select  COUNT(*) into columnExists from all_tab_cols where 
	upper(table_name) = 'SCH_LINEITEM' and upper(column_name) = 'LINEITEM_DESC';
	if (columnExists =1) then
   execute immediate 'ALTER TABLE SCH_LINEITEM MODIFY LINEITEM_DESC VARCHAR2(4000)';
   commit;
	end if;
  
end;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,356,5,'05_alter_schlineitemdesc.sql',sysdate,'v10 #757');

commit;

