DECLARE
  v_obj_subtyp number := 0;
  
BEGIN
  SELECT COUNT(*) INTO v_obj_subtyp 
  FROM er_object_settings 
  WHERE object_subtype='report_menu' and object_name='top_menu';
  
  IF (v_obj_subtyp != 0) THEN
    update ER_OBJECT_SETTINGS 
    SET OBJECT_DISPLAYTEXT='Reporting' 
    where object_name='top_menu' And object_subtype='report_menu';
  END IF;
  commit;
  
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,356,4,'04_update_erobjects.sql',sysdate,'v10 #757');

commit;

