set define off;

DECLARE
countFlag number;
v_cnt number;
begin
 SELECT COUNT(1) INTO countFlag FROM USER_TABLES WHERE TABLE_NAME='ER_CODELST';
    if (countFlag > 0) then
     dbms_output.put_line('countFlag-'||countFlag);
      SELECT COUNT(*)  INTO v_cnt from er_codelst  WHERE codelst_type ='milepaytype' and CODELST_SUBTYP='inv';
      if v_cnt=0 then
      dbms_output.put_line('v_cnt-'||v_cnt);
      INSERT INTO ER_CODELST 
		(PK_CODELST,
		CODELST_TYPE,
		CODELST_SUBTYP,
		CODELST_DESC, 
		CODELST_HIDE,
		CODELST_SEQ,
		LAST_MODIFIED_DATE,
		CREATED_ON)
		VALUES(
		(SEQ_ER_CODELST.nextval)+1 ,
		'milepaytype',
		'inv', 
		'Invoiceable', 
		'N', 
		(select max(CODELST_SEQ)+1 from ER_CODELST where CODELST_TYPE='milepaytype'),
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM'), 
		to_timestamp(sysdate,'DD-MON-RR HH.MI.SS.FF AM')
	    );
      commit;
      end if;
      end if;
end;

/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,356,3,'03_ER_CODELST_Insert.sql',sysdate,'v10 #757');

commit;

