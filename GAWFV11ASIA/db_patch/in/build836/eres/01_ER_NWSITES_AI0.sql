set define off;
create or replace TRIGGER "ERES"."ER_NWSITES_AI0" AFTER
INSERT ON ER_NWSITES REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE
v_count Number;
--v_study Number;
v_codelstPk Number;
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'ER_NWSITES_AI0', pLEVEL  => Plog.LFATAL);
BEGIN

insert into er_status_history(PK_STATUS,STATUS_MODPK,STATUS_MODTABLE,FK_CODELST_STAT,STATUS_DATE,CREATOR,RECORD_TYPE,IP_ADD,STATUS_ISCURRENT) values(SEQ_ER_STATUS_HISTORY.nextval,:NEW.PK_NWSITES,'er_nwsites',:NEW.NW_STATUS,:NEW.CREATED_ON,:NEW.CREATOR,'N',:NEW.IP_ADD,1);
--SELECT count(*) into v_count FROM er_status_history WHERE STATUS_PARENTMODPK IS NOT NULL AND status_modpk =nvl(:NEW.FK_NWSITES_MAIN,0) and STATUS_ISCURRENT=1;
SELECT count(*) into v_count FROM er_study WHERE study_network like ''||nvl(:NEW.FK_NWSITES_MAIN,0)||'' or study_network like '%,'||nvl(:NEW.FK_NWSITES_MAIN,0)||'' or study_network like '%,'||nvl(:NEW.FK_NWSITES_MAIN,0)||',%' or study_network like ''||nvl(:NEW.FK_NWSITES_MAIN,0)||',%';
--execute IMMEDIATE 'SELECT count(*) FROM er_study WHERE study_network like '''||nvl(:NEW.FK_NWSITES_MAIN,0)||''' or study_network like ''%,'||nvl(:NEW.FK_NWSITES_MAIN,0)||''' or study_network like ''%,'||nvl(:NEW.FK_NWSITES_MAIN,0)||',%'' or study_network like '''||nvl(:NEW.FK_NWSITES_MAIN,0)||',%''' into v_count;
plog.fatal(pctx,'v_count '|| v_count);
if(v_count>0)
then
select pk_codelst into v_codelstPk from er_codelst where codelst_type='sntwStat' and codelst_subTyp='A';
--FOR i IN (SELECT STATUS_PARENTMODPK FROM er_status_history WHERE STATUS_PARENTMODPK IS NOT NULL AND status_modpk  =nvl(:NEW.FK_NWSITES_MAIN,0) and STATUS_ISCURRENT=1)
--For i in (SELECT pk_study FROM er_study WHERE study_network like '%'||(:NEW.FK_NWSITES_MAIN)||'%')
For i in (SELECT pk_study FROM er_study WHERE study_network like ''||nvl(:NEW.FK_NWSITES_MAIN,0)||'' or study_network like '%,'||nvl(:NEW.FK_NWSITES_MAIN,0)||'' or study_network like '%,'||nvl(:NEW.FK_NWSITES_MAIN,0)||',%' or study_network like ''||nvl(:NEW.FK_NWSITES_MAIN,0)||',%')
LOOP
plog.fatal(pctx,'STATUS_PARENTMODPK ' || i.pk_study);
insert into er_status_history(PK_STATUS,STATUS_MODPK,STATUS_MODTABLE,FK_CODELST_STAT,STATUS_DATE,CREATOR,RECORD_TYPE,IP_ADD,STATUS_ISCURRENT,STATUS_PARENTMODPK) values(SEQ_ER_STATUS_HISTORY.nextval,:NEW.PK_NWSITES,'er_nwsites',v_codelstPk,:NEW.CREATED_ON,:NEW.CREATOR,'N',:NEW.IP_ADD,1,i.pk_study);
end loop;
end if;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,435,1,'01_ER_NWSITES_AI0.sql',sysdate,'v11 #836');

commit;	
/

