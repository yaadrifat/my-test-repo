set define off;

UPDATE ER_CTRLTAB SET CTRL_VALUE = 'v11 #836' 
where CTRL_KEY = 'app_version' ; 

commit;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,435,0,'00_er_version.sql',sysdate,'v11 #836');

commit;	
/
