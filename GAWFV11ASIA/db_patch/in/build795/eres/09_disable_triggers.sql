set define off;
Declare
	trigger_check number;
Begin
	select count(*) into trigger_check from user_triggers where trigger_name='ER_SPECIMEN_APPNDX_AUO';
	
	if(trigger_check>0) Then
		execute immediate 'DROP TRIGGER ER_SPECIMEN_APPNDX_AUO';
	End if;
	
	select count(*) into trigger_check from user_triggers where trigger_name='ER_SPECIMEN_APPNDX_BUO';
	
	if(trigger_check>0) Then
		execute immediate 'DROP TRIGGER ER_SPECIMEN_APPNDX_BUO';
	End if;
	
	select count(*) into trigger_check from user_triggers where trigger_name='ER_SPECIMEN_APPNDX_ADO';
	
	if(trigger_check>0) Then
		execute immediate 'ALTER TRIGGER ER_SPECIMEN_APPNDX_ADO DISABLE';
	End if;
	
	select count(*) into trigger_check from user_triggers where trigger_name='ER_STORAGE_STATUS_BIO';
	
	if(trigger_check>0) Then
		execute immediate 'ALTER TRIGGER ER_STORAGE_STATUS_BIO DISABLE';
	End if;
	
	select count(*) into trigger_check from user_triggers where trigger_name='ER_ALLOWED_ITEMS_BIO';
	
	if(trigger_check>0) Then
		execute immediate 'ALTER TRIGGER ER_ALLOWED_ITEMS_BIO DISABLE';
	End if;
	
	select count(*) into trigger_check from user_triggers where trigger_name='ER_SPECIMEN_APPNDX_BIO';
	
	if(trigger_check>0) Then
		execute immediate 'ALTER TRIGGER ER_SPECIMEN_APPNDX_BIO DISABLE';
	End if;

COMMIT;
End;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,394,9,'09_disable_triggers.sql',sysdate,'v11 #795');