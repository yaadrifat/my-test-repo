set define off;

DECLARE
v_tab_exists number := 0;

begin 

	Select count(*) into v_tab_exists from tabs  where TABLE_NAME = 'ER_NWUSERS_ADDNLROLES';
	if v_tab_exists = 0 then
	execute immediate 'CREATE TABLE ER_NWUSERS_ADDNLROLES
	(
    PK_NWUSERS_ADDROLES      NUMBER NOT NULL ,
    FK_NWUSERS     NUMBER NOT NULL ,
    NWU_MEMBERADDLTROLE  NUMBER ,
	NWU_ADTSTATUS NUMBER,
	RID               NUMBER,
    CREATOR           NUMBER,
    LAST_MODIFIED_BY  NUMBER,
    LAST_MODIFIED_DATE DATE DEFAULT NULL,
    CREATED_ON DATE DEFAULT sysdate,
    IP_ADD          VARCHAR2(15 BYTE)
	)
  PCTFREE 10 PCTUSED 40 TABLESPACE ERES_USER LOGGING STORAGE
  (
    INITIAL 65536 NEXT 1048576 PCTINCREASE 0 MINEXTENTS 1 MAXEXTENTS 2147483645 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT
  )' ;
	execute immediate 'COMMENT ON COLUMN ERES.ER_NWUSERS_ADDNLROLES.PK_NWUSERS_ADDROLES
	IS
  ''Primary key''' ;
	execute immediate 'COMMENT ON COLUMN ERES.ER_NWUSERS_ADDNLROLES.FK_NWUSERS
	IS
  ''FK to the ER_NWUSERS.''' ;
	execute immediate 'COMMENT ON COLUMN ERES.ER_NWUSERS_ADDNLROLES.NWU_MEMBERADDLTROLE
	IS
  ''Network Additional Role''' ;
	execute immediate 'COMMENT ON COLUMN ERES.ER_NWUSERS_ADDNLROLES.NWU_ADTSTATUS
	IS
  ''Network Additional Role Status''' ;
	execute immediate 'COMMENT ON COLUMN ERES.ER_NWUSERS_ADDNLROLES.RID
	IS
  ''This column is used for the audit trail. The RID uniquely identifies the row in the database.''';
	execute immediate 'COMMENT ON COLUMN ERES.ER_NWUSERS_ADDNLROLES.CREATOR
	IS
  ''This column stores the name of user inserting the row in the table.''';
	execute immediate 'COMMENT ON COLUMN ERES.ER_NWUSERS_ADDNLROLES.LAST_MODIFIED_BY
	IS
  ''This column stores the name of user who last modified the row in the table.''';
	execute immediate 'COMMENT ON COLUMN ERES.ER_NWUSERS_ADDNLROLES.LAST_MODIFIED_DATE
	IS
  ''This column stores the date on which the row was last modified.''';
	execute immediate 'COMMENT ON COLUMN ERES.ER_NWUSERS_ADDNLROLES.CREATED_ON
	IS
  ''This column stores the date on which the row was  inserted.''';
	execute immediate 'COMMENT ON COLUMN ERES.ER_NWUSERS_ADDNLROLES.IP_ADD
	IS
  ''This column is used for Audit Trail. Stores the IP ADDRESS of the client machine.''';
	execute immediate 'CREATE UNIQUE INDEX ERES.ER_NWUSERS_ADDNLROLES ON ERES.ER_NWUSERS_ADDNLROLES
  (
    PK_NWUSERS_ADDROLES ASC
  )
  TABLESPACE ERES_USER PCTFREE 10 STORAGE
  (
    INITIAL 65536 NEXT 1048576 PCTINCREASE 0 MINEXTENTS 1 MAXEXTENTS 2147483645 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT
  )
  LOGGING' ;
	execute immediate 'ALTER TABLE ERES.ER_NWUSERS_ADDNLROLES ADD CONSTRAINT ER_NWUSERS_ADDNLROLES PRIMARY KEY ( PK_NWUSERS_ADDROLES ) USING INDEX ERES.ER_NWUSERS_ADDNLROLES' ;
	execute immediate 'ALTER TABLE ERES.ER_NWUSERS_ADDNLROLES ADD CONSTRAINT ER_NWUSERS_ADDNLROLES_FK1 FOREIGN KEY ( FK_NWUSERS ) REFERENCES ERES.ER_NWUSERS ( PK_NWUSERS ) NOT DEFERRABLE' ;
	execute immediate 'CREATE SEQUENCE "ERES"."SEQ_ER_NWUSERS_ADDNLROLES" MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 100 ORDER NOCYCLE' ;
 else
		p('no action needed. Form response Compare utility patch exists');
	end if;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,394,1,'01_ER_NWUSERS_ADDNLROLES.sql',sysdate,'v11 #795');
commit;