set define off;
create or replace
TRIGGER "ERES"."ER_NWUSERS_ADDNLROLES_BU0" 
	BEFORE UPDATE ON ER_NWUSERS_ADDNLROLES REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
BEGIN
		:NEW.last_modified_date := SYSDATE ;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,394,6,'06_ER_NWUSERS_ADDNLROLES_BU0.sql',sysdate,'v11 #795');

commit;

