DECLARE 
	table_check number;
	row_check number:=0;
	
BEGIN
	SELECT count(*)
	INTO table_check
	FROM USER_TABLES
	WHERE TABLE_NAME = 'ER_OBJECT_SETTINGS';
	
	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM er_object_settings
		WHERE object_type='T' AND
		      object_subtype='7' AND
			  object_name='acct_tab';
		
		IF (row_check = 0) then
				INSERT INTO er_object_settings 
							(PK_OBJECT_SETTINGS,
							OBJECT_TYPE,
							OBJECT_SUBTYPE,
							OBJECT_NAME,
							OBJECT_SEQUENCE,
							OBJECT_VISIBLE,
							OBJECT_DISPLAYTEXT,
							FK_ACCOUNT)
				VALUES
					(SEQ_ER_OBJECT_SETTINGS.nextval,
					'T',
					'7',
					'acct_tab',
					7,
					0,
					'Network',
					0);
		END IF;
	
	END IF;		
	
	COMMIT;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,376,2,'02_networkTab.sql',sysdate,'v10.1 #777');

commit;


