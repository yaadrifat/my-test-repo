DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_LKPCOL where FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Patient Adverse Events' and LKPTYPE_TYPE='dyn_p') 
	and LKPCOL_NAME='AE_DISCVRYDATE';
  if (v_record_exists = 0) then
INSERT INTO ER_LKPCOL 
		(PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_TABLE, LKPCOL_KEYWORD)
	 VALUES((select max(PK_LKPCOL)+1 from ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Patient Adverse Events' and LKPTYPE_TYPE='dyn_p'),'AE_DISCVRYDATE','AE Discovery Date','date','erv_patient_adverse_events','AE_DISCVRYDATE');
	 
    commit;
  end if;
end;

/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_LKPCOL where FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Patient Adverse Events' and LKPTYPE_TYPE='dyn_p') 
	and LKPCOL_NAME='AE_LOGGEDDATE';
  if (v_record_exists = 0) then
INSERT INTO ER_LKPCOL 
		(PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_TABLE, LKPCOL_KEYWORD)
	 VALUES((select max(PK_LKPCOL)+1 from ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Patient Adverse Events' and LKPTYPE_TYPE='dyn_p'),'AE_LOGGEDDATE','AE Logged Date','date','erv_patient_adverse_events','AE_LOGGEDDATE');
	 
    commit;
  end if;
end;

/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_LKPCOL where FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Patient Adverse Events' and LKPTYPE_TYPE='dyn_p') 
	and LKPCOL_NAME='AE_REPORTEDBY';
  if (v_record_exists = 0) then
INSERT INTO ER_LKPCOL 
		(PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_TABLE, LKPCOL_KEYWORD)
	 VALUES((select max(PK_LKPCOL)+1 from ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Patient Adverse Events' and LKPTYPE_TYPE='dyn_p'),'AE_REPORTEDBY','Reported By','varchar','erv_patient_adverse_events','AE_REPORTEDBY');
	 
    commit;
  end if;
end;

/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_LKPCOL where FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Patient Adverse Events' and LKPTYPE_TYPE='dyn_p') 
	and LKPCOL_NAME='FK_LINK_ADVERSEVE';
  if (v_record_exists = 0) then
INSERT INTO ER_LKPCOL 
		(PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_TABLE, LKPCOL_KEYWORD)
	 VALUES((select max(PK_LKPCOL)+1 from ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Patient Adverse Events' and LKPTYPE_TYPE='dyn_p'),'FK_LINK_ADVERSEVE','Linked To','number','erv_patient_adverse_events','FK_LINK_ADVERSEVE');
	 
    commit;
  end if;
end;


/

DECLARE
  v_record_exists number := 0;  
BEGIN

 Select count(*) into v_record_exists
    from ER_LKPVIEWCOL where FK_LKPVIEW=(select PK_LKPVIEW from ER_LKPVIEW where FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Patient Adverse Events' and LKPTYPE_TYPE='dyn_p') and LKPVIEW_NAME='Adverse Events') 
	and FK_LKPCOL=(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME='AE_DISCVRYDATE' and FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Patient Adverse Events' and LKPTYPE_TYPE='dyn_p'));
   
  if (v_record_exists = 0) then
  
   INSERT INTO ER_LKPVIEWCOL
  (PK_LKPVIEWCOL,FK_LKPCOL, LKPVIEW_SEQ,FK_LKPVIEW, LKPVIEW_DISPLEN,LKPVIEW_IS_DISPLAY)
  VALUES((select max(PK_LKPVIEWCOL)+1 from ER_LKPVIEWCOL) ,(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME='AE_DISCVRYDATE' and FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Patient Adverse Events' and LKPTYPE_TYPE='dyn_p')),(select max(LKPVIEW_SEQ)+1 from ER_LKPVIEWCOL where FK_LKPVIEW=(select PK_LKPVIEW from ER_LKPVIEW where FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Patient Adverse Events' and LKPTYPE_TYPE='dyn_p') and LKPVIEW_NAME='Adverse Events')),(select PK_LKPVIEW from ER_LKPVIEW where FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Patient Adverse Events' and LKPTYPE_TYPE='dyn_p') and LKPVIEW_NAME='Adverse Events'),'10%','Y');
 
   commit;
  end if;
end;

/

DECLARE
  v_record_exists number := 0;  
BEGIN

 Select count(*) into v_record_exists
    from ER_LKPVIEWCOL where FK_LKPVIEW=(select PK_LKPVIEW from ER_LKPVIEW where FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Patient Adverse Events' and LKPTYPE_TYPE='dyn_p') and LKPVIEW_NAME='Adverse Events') 
	and FK_LKPCOL=(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME='AE_LOGGEDDATE' and FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Patient Adverse Events' and LKPTYPE_TYPE='dyn_p'));
   
  if (v_record_exists = 0) then
  
   INSERT INTO ER_LKPVIEWCOL
  (PK_LKPVIEWCOL,FK_LKPCOL, LKPVIEW_SEQ,FK_LKPVIEW, LKPVIEW_DISPLEN,LKPVIEW_IS_DISPLAY)
  VALUES((select max(PK_LKPVIEWCOL)+1 from ER_LKPVIEWCOL) ,(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME='AE_LOGGEDDATE' and FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Patient Adverse Events' and LKPTYPE_TYPE='dyn_p')),(select max(LKPVIEW_SEQ)+1 from ER_LKPVIEWCOL where FK_LKPVIEW=(select PK_LKPVIEW from ER_LKPVIEW where FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Patient Adverse Events' and LKPTYPE_TYPE='dyn_p') and LKPVIEW_NAME='Adverse Events')),(select PK_LKPVIEW from ER_LKPVIEW where FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Patient Adverse Events' and LKPTYPE_TYPE='dyn_p') and LKPVIEW_NAME='Adverse Events'),'10%','Y');
 
   commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN

 Select count(*) into v_record_exists
    from ER_LKPVIEWCOL where FK_LKPVIEW=(select PK_LKPVIEW from ER_LKPVIEW where FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Patient Adverse Events' and LKPTYPE_TYPE='dyn_p') and LKPVIEW_NAME='Adverse Events') 
	and FK_LKPCOL=(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME='AE_REPORTEDBY' and FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Patient Adverse Events' and LKPTYPE_TYPE='dyn_p'));
   
  if (v_record_exists = 0) then
  
   INSERT INTO ER_LKPVIEWCOL
  (PK_LKPVIEWCOL,FK_LKPCOL, LKPVIEW_SEQ,FK_LKPVIEW, LKPVIEW_DISPLEN,LKPVIEW_IS_DISPLAY)
  VALUES((select max(PK_LKPVIEWCOL)+1 from ER_LKPVIEWCOL) ,(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME='AE_REPORTEDBY' and FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Patient Adverse Events' and LKPTYPE_TYPE='dyn_p')),(select max(LKPVIEW_SEQ)+1 from ER_LKPVIEWCOL where FK_LKPVIEW=(select PK_LKPVIEW from ER_LKPVIEW where FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Patient Adverse Events' and LKPTYPE_TYPE='dyn_p') and LKPVIEW_NAME='Adverse Events')),(select PK_LKPVIEW from ER_LKPVIEW where FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Patient Adverse Events' and LKPTYPE_TYPE='dyn_p') and LKPVIEW_NAME='Adverse Events'),'10%','Y');
 
   commit;
  end if;
end;

/
DECLARE
  v_record_exists number := 0;  
BEGIN

 Select count(*) into v_record_exists
    from ER_LKPVIEWCOL where FK_LKPVIEW=(select PK_LKPVIEW from ER_LKPVIEW where FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Patient Adverse Events' and LKPTYPE_TYPE='dyn_p') and LKPVIEW_NAME='Adverse Events') 
	and FK_LKPCOL=(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME='FK_LINK_ADVERSEVE' and FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Patient Adverse Events' and LKPTYPE_TYPE='dyn_p'));
   
  if (v_record_exists = 0) then
  
   INSERT INTO ER_LKPVIEWCOL
  (PK_LKPVIEWCOL,FK_LKPCOL, LKPVIEW_SEQ,FK_LKPVIEW, LKPVIEW_DISPLEN,LKPVIEW_IS_DISPLAY)
  VALUES((select max(PK_LKPVIEWCOL)+1 from ER_LKPVIEWCOL) ,(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME='FK_LINK_ADVERSEVE' and FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Patient Adverse Events' and LKPTYPE_TYPE='dyn_p')),(select max(LKPVIEW_SEQ)+1 from ER_LKPVIEWCOL where FK_LKPVIEW=(select PK_LKPVIEW from ER_LKPVIEW where FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Patient Adverse Events' and LKPTYPE_TYPE='dyn_p') and LKPVIEW_NAME='Adverse Events')),(select PK_LKPVIEW from ER_LKPVIEW where FK_LKPLIB=(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='Patient Adverse Events' and LKPTYPE_TYPE='dyn_p') and LKPVIEW_NAME='Adverse Events'),'10%','Y');
 
   commit;
  end if;
end;

/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,416,1,'01_ER_LKPVIEWCOL_LKPCOL_INSERT.sql',sysdate,'v11 #817');
commit;
/