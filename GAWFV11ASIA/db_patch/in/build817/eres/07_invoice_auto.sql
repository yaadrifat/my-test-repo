set define off;
Declare
v_invNumber number:=0;
Begin
Begin
SELECT MAX(to_number(invnum)) into v_invNumber
FROM
  (SELECT F_IS_NUMBER(SUBSTR(INV_NUMBER,instr(INV_NUMBER,'-',
    (SELECT LENGTH(study_number) FROM er_study WHERE PK_study = FK_STUDY
    ))+1)) is_number,
    SUBSTR(INV_NUMBER,instr(INV_NUMBER,'-',
    (SELECT LENGTH(study_number) FROM er_study WHERE PK_study = FK_STUDY
    ))+1) invnum
  FROM er_invoice
  )
WHERE is_number>0;
Exception WHEN NO_DATA_FOUND THEN
v_invNumber:=0;
END;

EXECUTE IMMEDIATE 'CREATE SEQUENCE "ERES"."SEQ_ER_INVOICE_ID" MINVALUE 1 MAXVALUE 1000000000000000000000000000 INCREMENT BY 1 START WITH '||to_char(v_invNumber+1)||' NOCACHE NOORDER NOCYCLE';

END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,416,7,'07_invoice_auto.sql',sysdate,'v11 #817');
commit;
/
