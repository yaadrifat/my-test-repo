set define off;

DECLARE
    i INTEGER;
BEGIN
    SELECT COUNT(*) INTO i FROM user_indexes WHERE index_name = 'INDX_PKFKFORMCATSTAT';
    IF i > 0 THEN
        EXECUTE IMMEDIATE 'DROP INDEX INDX_PKFKFORMCATSTAT';
    END IF;    
END;
/
CREATE INDEX "ERES"."INDX_PKFKFORMCATSTAT" ON "ERES"."ER_FORMLIB"
  (
  "FK_CATLIB",
  "FORM_STATUS",
  "FK_ACCOUNT"
  )
  TABLESPACE "ERES_USER" ;
  
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,374,4,'04_INDX_PKFKFORMCATSTAT.sql',sysdate,'v10 #775');

commit;
