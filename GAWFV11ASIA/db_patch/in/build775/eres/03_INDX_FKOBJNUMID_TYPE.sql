set define off;

DECLARE
    i INTEGER;
BEGIN
    SELECT COUNT(*) INTO i FROM user_indexes WHERE index_name = 'INDX_FKOBJNUMID_TYPE';
    IF i > 0 THEN
        EXECUTE IMMEDIATE 'DROP INDEX INDX_FKOBJNUMID_TYPE';
    END IF;    
END;
/
CREATE INDEX "ERES"."INDX_FKOBJNUMID_TYPE" ON "ERES"."ER_OBJECTSHARE"
  (
    "FK_OBJECT",
    "OBJECT_NUMBER",
    "FK_OBJECTSHARE_ID",
    "OBJECTSHARE_TYPE"
  ) TABLESPACE "ERES_USER" ;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,374,3,'03_INDX_FKOBJNUMID_TYPE.sql',sysdate,'v10 #775');
commit;