set define off;
create or replace PACKAGE	PKG_PROPGT_EVTUPDATES IS PROCEDURE SP_PROPAGATE_EVT_UPDATES(p_protocol_id IN NUMBER,p_selected_event_id IN NUMBER,p_table_name IN VARCHAR2,p_primary_keyvalue IN NUMBER,p_copy_to_same_visits IN CHAR,p_copy_to_same_events IN CHAR,p_mode IN CHAR DEFAULT 'M',p_calType IN CHAR DEFAULT 'P');
	PROCEDURE sp_update_event_detail(p_source_event_id IN NUMBER, p_target_event_list IN Types.Number_Array, source IN VARCHAR2,p_target_eventcopyflag_list IN Types.SMALL_STRING_ARRAY);
	PROCEDURE sp_update_event_message(p_source_event_id IN NUMBER, p_target_event_list IN Types.Number_Array, source IN VARCHAR2);
	PROCEDURE sp_add_event_cost(p_source_event_id IN NUMBER, p_target_event_list IN Types.Number_Array, p_pk_eventcost IN NUMBER);
	PROCEDURE sp_update_event_cost(p_source_event_id IN NUMBER, p_target_event_list IN Types.Number_Array, p_pk_eventcost IN NUMBER, p_mode IN CHAR);
	PROCEDURE sp_delete_event_cost(p_source_event_id IN NUMBER, p_pk_eventcost IN NUMBER, p_mode IN CHAR);
	PROCEDURE sp_add_event_appendix(p_source_event_id IN NUMBER, p_target_event_list IN Types.Number_Array, p_pk_docs IN NUMBER);
	PROCEDURE sp_update_event_appendix(p_source_event_id IN NUMBER, p_target_event_list IN Types.Number_Array, p_pk_docs IN NUMBER, p_mode IN CHAR);
	PROCEDURE sp_add_event_resource(p_source_event_id IN NUMBER, p_target_event_list IN Types.Number_Array, p_pk_eventusr IN NUMBER, p_type CHAR);
	PROCEDURE sp_add_event_crflib(p_source_event_id IN NUMBER, p_target_event_list IN Types.Number_Array, pk_crflib IN NUMBER);
	PROCEDURE sp_update_event_crflib(p_source_event_id IN NUMBER, p_target_event_list IN Types.Number_Array, pk_crflib IN NUMBER, p_mode IN CHAR);
	PROCEDURE sp_manage_event_user(p_source_event_id IN NUMBER, p_target_event_list IN Types.Number_Array, p_pk_eventuser IN NUMBER , p_usertype IN VARCHAR2, p_mode IN CHAR);
	PROCEDURE sp_add_event_crf_form(p_source_event_id IN NUMBER, p_target_event_list IN Types.Number_Array);
	PROCEDURE sp_add_event_kit(p_source_event_id IN NUMBER, p_target_event_list IN Types.Number_Array);
	PROCEDURE sp_delete_event_kit(p_source_event_id IN NUMBER, p_target_event_list IN Types.Number_Array,p_pkeventkit  in number);
    PROCEDURE sp_add_network_doc(p_doc_id IN NUMBER,p_network_id IN NUMBER ,p_networkFlag IN VARCHAR2,Creator IN VARCHAR2 , Ipadd IN VARCHAR2 );
	pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'PKG_PROPGT_EVTUPDATES', pLEVEL  => Plog.LFATAL);
END PKG_PROPGT_EVTUPDATES;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,382,2,'02_Pkg_Propgt_Evtupdates.sql',sysdate,'v10.1 #783');

commit;
