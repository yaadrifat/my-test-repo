CREATE OR REPLACE FORCE VIEW "VDA"."VDA_V_STUDY_CALENDAR" ("CALENDAR_TYPE", "CALENDAR_NAME", "CALENDAR_DESC", "CALENDAR_STAT", "CALENDAR_STAT_SUBTYPE", "CALENDAR_DURATION", "CREATOR", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "CREATED_ON", "CALENDAR_PK", "STUDY_NUMBER", "FK_ACCOUNT", "EVENT_CALASSOCTO", "CALENDAR_LINKING_TYPE", "LAST_MODIFIED_BY_FK", "CREATOR_FK", "FK_STUDY")
AS
  SELECT
    (SELECT CATLIB_NAME FROM ERES.ER_CATLIB WHERE PK_CATLIB = FK_CATLIB
    ) LBCAL_TYPE,
    NAME LBCAL_NAME,
    DESCRIPTION LBCAL_DESC,
    (SELECT CODELST_DESC
    FROM esch.sch_codelst
    WHERE PK_CODELST = FK_CODELST_CALSTAT
    ) LBCAL_STAT,
    (SELECT CODELST_SUBTYP
    FROM esch.sch_codelst
    WHERE PK_CODELST = FK_CODELST_CALSTAT
    ) CALENDAR_STAT_SUBTYPE,
    ERES.F_GET_DURATION (DURATION_UNIT, DURATION)
    || ' '
    || ERES.F_GET_DURUNIT (DURATION_UNIT) LBCAL_DURATION,
    (SELECT USR_FIRSTNAME
      || ' '
      || USR_LASTNAME
    FROM eres.ER_USER
    WHERE PK_USER = E.CREATOR
    ) CREATOR,
    (SELECT USR_FIRSTNAME
      || ' '
      || USR_LASTNAME
    FROM eres.ER_USER
    WHERE PK_USER = E.LAST_MODIFIED_BY
    ) LAST_MODIFIED_BY,
    E.LAST_MODIFIED_DATE,
    E.CREATED_ON,
    E.EVENT_ID CALENDAR_PK,
    study_number,
    FK_ACCOUNT,
    EVENT_CALASSOCTO,
    DECODE (EVENT_CALASSOCTO, 'P', 'Patient', 'S', 'Study', EVENT_CALASSOCTO) Calendar_linking_type,
    e.LAST_MODIFIED_BY AS LAST_MODIFIED_BY_FK,
    e.CREATOR          AS CREATOR_FK ,
    pk_study           AS FK_STUDY
  FROM ESCH.EVENT_ASSOC E,
    eres.er_study
  WHERE EVENT_TYPE = 'P'
  AND pk_study     = chain_id;
  COMMENT ON COLUMN "VDA"."VDA_V_STUDY_CALENDAR"."CALENDAR_TYPE"
IS
  'Calendar Type';
  COMMENT ON COLUMN "VDA"."VDA_V_STUDY_CALENDAR"."CALENDAR_NAME"
IS
  'Calendar Name';
  COMMENT ON COLUMN "VDA"."VDA_V_STUDY_CALENDAR"."CALENDAR_DESC"
IS
  'Calendar Description';
  COMMENT ON COLUMN "VDA"."VDA_V_STUDY_CALENDAR"."CALENDAR_STAT"
IS
  'Calendar Status';
  COMMENT ON COLUMN "VDA"."VDA_V_STUDY_CALENDAR"."CALENDAR_STAT_SUBTYPE"
IS
  'Calendar Status SubType';
  COMMENT ON COLUMN "VDA"."VDA_V_STUDY_CALENDAR"."CALENDAR_DURATION"
IS
  'Calendar Duration';
  COMMENT ON COLUMN "VDA"."VDA_V_STUDY_CALENDAR"."CREATOR"
IS
  'The user who created the calendar
record (Audit)';
  COMMENT ON COLUMN "VDA"."VDA_V_STUDY_CALENDAR"."LAST_MODIFIED_BY"
IS
  'The user who last modified the
calendar record(Audit)';
  COMMENT ON COLUMN "VDA"."VDA_V_STUDY_CALENDAR"."LAST_MODIFIED_DATE"
IS
  'The date the calendar record
was last modified on (Audit)';
  COMMENT ON COLUMN "VDA"."VDA_V_STUDY_CALENDAR"."CREATED_ON"
IS
  'The date the calendar record was
created on (Audit)';
  COMMENT ON COLUMN "VDA"."VDA_V_STUDY_CALENDAR"."CALENDAR_PK"
IS
  'The Primary Key of the Calendar
record';
  COMMENT ON COLUMN "VDA"."VDA_V_STUDY_CALENDAR"."STUDY_NUMBER"
IS
  'The Study Number of the study the
payment  is linked with';
  COMMENT ON COLUMN "VDA"."VDA_V_STUDY_CALENDAR"."FK_ACCOUNT"
IS
  'The account the calendar is linked
with';
  COMMENT ON COLUMN "VDA"."VDA_V_STUDY_CALENDAR"."EVENT_CALASSOCTO"
IS
  'The calendar association:
Patient Calendar or Admin Calendar';
  COMMENT ON COLUMN "VDA"."VDA_V_STUDY_CALENDAR"."CALENDAR_LINKING_TYPE"
IS
  'The calendar association
Description';
  COMMENT ON COLUMN "VDA"."VDA_V_STUDY_CALENDAR"."LAST_MODIFIED_BY_FK"
IS
  'The Key to identify the
Last Modified By of the record.';
  COMMENT ON COLUMN "VDA"."VDA_V_STUDY_CALENDAR"."CREATOR_FK"
IS
  'The Key to identify the Creator of
the record.';
  COMMENT ON COLUMN "VDA"."VDA_V_STUDY_CALENDAR"."FK_STUDY"
IS
  'The Key to identify the study linked
with the calendar';
  COMMENT ON TABLE "VDA"."VDA_V_STUDY_CALENDAR"
IS
  'This view provides access to Study Calendars''
summary information';

INSERT INTO VDA_TRACK_PATCHES
(
  PATCH_PK       ,
  DB_VER_MJR     ,
  DB_VER_MNR     ,
  DB_PATCH_NAME  ,
  FIRED_ON       ,
  APP_VERSION    
) VALUES(SEQ_VDA_TRACK_PATCHES.nextval,5,0,'04_VDA_V_STUDY_CALENDAR.sql',sysdate,'VDA1.4#18Reports') ;

commit	;	