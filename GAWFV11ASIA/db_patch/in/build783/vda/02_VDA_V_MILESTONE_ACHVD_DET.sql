CREATE OR REPLACE FORCE VIEW "VDA"."VDA_V_MILESTONE_ACHVD_DET" ("PK_MILEACHIEVED", "MSACH_MILESTONE_DESC", "MSACH_PATIENT_ID", "MSACH_PTSTUDY_ID", "MSACH_ACH_DATE", "CREATOR", "CREATED_ON", "LAST_MODIFIED_DATE", "LAST_MODIFIED_BY", "FK_STUDY", "FK_ACCOUNT", "STUDY_NUMBER", "FK_MILESTONE", "LAST_MODIFIED_BY_FK", "CREATOR_FK", "PAYMENT_DT", "INV_AMT", "INV_NUM", "REC_AMT", "MILESTONE_TYPE", "MILESTONE_AMT", "PAYMENT_TYPE", "PAYMENT_SUBTYPE", "PAYMENT_FOR", "PAYMENTFOR_SUBTYPE", "FK_SITE_ENROLLING", "ENROLLING_SITE", "MILESTONE_HOLDBACK")
AS
  SELECT PK_MILEACHIEVED,
    ERES.PKG_Milestone_New.f_getMilestoneDesc (FK_MILESTONE) MSACH_MILESTONE_DESC,
    (SELECT PER_CODE FROM eres.ER_PER WHERE PK_PER = FK_PER
    ) MSACH_PATIENT_ID,
    (SELECT PATPROT_PATSTDID
    FROM eres.ER_PATPROT pp
    WHERE pp.fk_per     = a.fk_per
    AND pp.fk_study     = a.fk_study
    AND pp.patprot_stat = 1
    AND ROWNUM          < 2
    ) MSACH_PTSTUDY_ID,
    ACH_DATE MSACH_ACH_DATE,
    (SELECT usr_firstname
      || ' '
      || usr_lastname
    FROM eres.ER_USER
    WHERE pk_user = a.CREATOR
    ) creator,
    a.CREATED_ON,
    a.LAST_MODIFIED_DATE,
    (SELECT usr_firstname
      || ' '
      || usr_lastname
    FROM eres.ER_USER
    WHERE pk_user = a.LAST_MODIFIED_BY
    ) LAST_MODIFIED_BY,
    a.fk_study,
    b.fk_account,
    b.STUDY_NUMBER,
    fk_milestone,
    a.LAST_MODIFIED_BY AS LAST_MODIFIED_BY_FK,
    a.CREATOR CREATOR_FK,
    (SELECT wm_concat (TO_CHAR( milepayment_date ,eres.PKG_DATEUTIL.F_GET_DATEFORMAT) )
    FROM eres.er_milepayment,
      eres.er_milepayment_details
    WHERE pk_milepayment            = fk_milepayment
    AND NVL(milepayment_delflag,'N')='N'
    AND fk_mileachieved             = pk_mileachieved
    ) AS payment_dt,
    NVL(
    (SELECT SUM(NVL(amount_invoiced,0) + NVL(amount_holdback,0))
    FROM eres.er_invoice_detail
    WHERE eres.er_invoice_detail.fk_mileachieved = pk_mileachieved
    ),'0') AS inv_amt,
    (SELECT wm_concat(inv_number)
    FROM eres.er_invoice,
      eres.er_invoice_detail
    WHERE pk_invoice                           = fk_inv
    AND eres.er_invoice_detail.fk_mileachieved = pk_mileachieved
    ) AS inv_num,
    NVL(
    (SELECT SUM(NVL(mp_amount,0) + NVL(mp_holdback_amount,0))
    FROM eres.er_milepayment_details
    WHERE fk_mileachieved = pk_mileachieved
    ),'0')         AS rec_amt,
    msrul_category AS milestone_type,
    msrul_amount milestone_amt,
    msrul_pay_type payment_type,
    MSRUL_PAY_SUBTYPE payment_subtype,
    msrul_pay_for payment_for,
    MSRUL_PAYFOR_SUBTYPE paymentfor_subtype,
    (SELECT FK_SITE_ENROLLING
    FROM eres.ER_PATPROT pp
    WHERE pp.fk_per     = a.fk_per
    AND pp.fk_study     = a.fk_study
    AND pp.patprot_stat = 1
    AND ROWNUM          < 2
    ) FK_SITE_ENROLLING ,
    (SELECT SITE_NAME
    FROM eres.ER_PATPROT pp,
      eres.er_Site
    WHERE pp.fk_per       = a.fk_per
    AND pp.fk_study       = a.fk_study
    AND pp.patprot_stat   = 1
    AND ROWNUM            < 2
    AND fk_site_enrolling = pk_site
    ) enrolling_site ,
    milestone_holdback
  FROM eres.ER_MILEACHIEVED a,
    eres.ER_STUDY b,
    VDA.VDA_V_MILESTONES
  WHERE IS_COMPLETE                     = 1
  AND b.pk_study                        = a.fk_study
  AND vda.vda_v_milestones.pk_milestone = a.fk_milestone ;
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONE_ACHVD_DET"."PK_MILEACHIEVED"
IS
  'The Primary of the milestones achievement records';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONE_ACHVD_DET"."MSACH_MILESTONE_DESC"
IS
  'Milestone description';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONE_ACHVD_DET"."MSACH_PATIENT_ID"
IS
  'Patient ID for Patient related milestones';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONE_ACHVD_DET"."MSACH_PTSTUDY_ID"
IS
  'Patient Study ID for Patient related milestones';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONE_ACHVD_DET"."MSACH_ACH_DATE"
IS
  'milestone achievement date';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONE_ACHVD_DET"."CREATOR"
IS
  'The user who created the record (Audit)';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONE_ACHVD_DET"."CREATED_ON"
IS
  'The date the record was created on (Audit)';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONE_ACHVD_DET"."LAST_MODIFIED_BY"
IS
  'The user who last modified the record(Audit)';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONE_ACHVD_DET"."FK_STUDY"
IS
  'The Foreign Key to the study milestone is linked with';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONE_ACHVD_DET"."FK_ACCOUNT"
IS
  'The account the milestone is linked with';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONE_ACHVD_DET"."STUDY_NUMBER"
IS
  'The Study Number of the study the milestone is linked with';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONE_ACHVD_DET"."FK_MILESTONE"
IS
  'The PK_milestone to uniquely identify a milestone';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONE_ACHVD_DET"."LAST_MODIFIED_BY_FK"
IS
  'The Key to identify the Last Modified By of the record.';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONE_ACHVD_DET"."CREATOR_FK"
IS
  'The Key to identify the Creator of the record.';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONE_ACHVD_DET"."PAYMENT_DT"
IS
  'The Payments linked with the Milestone Achievement';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONE_ACHVD_DET"."INV_AMT"
IS
  'The total amount invoiced against the Milestone Achievement';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONE_ACHVD_DET"."INV_NUM"
IS
  'The Invoices linked with the Milestone Achievement';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONE_ACHVD_DET"."REC_AMT"
IS
  'The total amount reconciled against the Milestone Achievement';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONE_ACHVD_DET"."MILESTONE_TYPE"
IS
  'The Milestone Type';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONE_ACHVD_DET"."MILESTONE_AMT"
IS
  'The Milestone Amount';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONE_ACHVD_DET"."PAYMENT_TYPE"
IS
  'Milestone Payment Type';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONE_ACHVD_DET"."PAYMENT_SUBTYPE"
IS
  'Milestone Payment SubType';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONE_ACHVD_DET"."PAYMENT_FOR"
IS
  'Milestone Payment For';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONE_ACHVD_DET"."PAYMENTFOR_SUBTYPE"
IS
  'Milestone Payment For SubType';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONE_ACHVD_DET"."FK_SITE_ENROLLING"
IS
  'The FK to the Enrolling Site (If the achievement record is linked with a patient';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONE_ACHVD_DET"."ENROLLING_SITE"
IS
  'The Enrolling Site name (If the achievement record is linked with a patient';
  COMMENT ON COLUMN "VDA"."VDA_V_MILESTONE_ACHVD_DET"."MILESTONE_HOLDBACK"
IS
  'Milestone Holdback amount per Milestone definition';
  COMMENT ON TABLE "VDA"."VDA_V_MILESTONE_ACHVD_DET"
IS
  'This view provides access to the milestone achievement data';
  
INSERT INTO VDA_TRACK_PATCHES
(
  PATCH_PK       ,
  DB_VER_MJR     ,
  DB_VER_MNR     ,
  DB_PATCH_NAME  ,
  FIRED_ON       ,
  APP_VERSION    
) VALUES(SEQ_VDA_TRACK_PATCHES.nextval,5,0,'02_VDA_V_MILESTONE_ACHVD_DET.sql',sysdate,'VDA1.4#18Reports') ;

commit	;	