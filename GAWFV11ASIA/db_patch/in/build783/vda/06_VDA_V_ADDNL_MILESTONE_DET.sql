CREATE OR REPLACE FORCE VIEW "VDA"."VDA_V_ADDNL_MILESTONE_DET" ("PK_MILESTONE", "MSACH_MILESTONE_DESC", "MSACH_ACH_DATE", "CREATOR", "CREATED_ON", "LAST_MODIFIED_DATE", "LAST_MODIFIED_BY", "FK_STUDY", "FK_ACCOUNT", "STUDY_NUMBER", "LAST_MODIFIED_BY_FK", "CREATOR_FK", "PAYMENT_DT", "INV_AMT", "INV_NUM", "REC_AMT", "MILESTONE_TYPE", "MILESTONE_AMT", "PAYMENT_TYPE", "PAYMENT_FOR", "MILESTONE_HOLDBACK")
AS
  SELECT PK_MILESTONE,
    m.MILESTONE_DESC_CALCULATED MSACH_MILESTONE_DESC,
    NVL(m.LAST_MODIFIED_DATE,m.created_on) MSACH_ACH_DATE,
    m.creator,
    m.CREATED_ON,
    m.LAST_MODIFIED_DATE,
    m.LAST_MODIFIED_BY,
    fk_study,
    b.fk_account,
    b.STUDY_NUMBER,
    m.LAST_MODIFIED_BY AS LAST_MODIFIED_BY_FK,
    m.CREATOR CREATOR_FK,
    SUBSTR(
    (SELECT wm_concat (TO_CHAR(
      (SELECT DISTINCT milepayment_date
      FROM eres.er_milepayment,
        eres.er_milepayment_details
      WHERE pk_milepayment            = fk_milepayment
      AND NVL(milepayment_delflag,'N')='N'
      AND fk_mileachieved             = 0
      AND ( (mp_linkto_type           = 'M'
      AND mp_level1_id                = m.pk_milestone )
      OR (mp_linkto_type              = 'M'
      AND mp_level1_id                = 0
      AND mp_linkto_id                = m.pk_milestone) )
      ) ,eres.PKG_DATEUTIL.F_GET_DATEFORMAT) )
    FROM dual
    ),1,4000) AS payment_dt,
    NVL(
    (SELECT SUM(NVL(amount_invoiced,0) + NVL(amount_holdback,0))
    FROM eres.er_invoice_detail
    WHERE eres.er_invoice_detail.fk_mileachieved = 0
    AND fk_milestone                             =m.pk_milestone
    ),0) AS inv_amt,
    SUBSTR(
    (SELECT wm_concat(DISTINCT inv_number)
    FROM eres.er_invoice,
      eres.er_invoice_detail
    WHERE pk_invoice                           = fk_inv
    AND eres.er_invoice_detail.fk_mileachieved = 0
    AND fk_milestone                           =m.pk_milestone
    ),1,4000) AS inv_num,
    NVL(
    (SELECT SUM(NVL(mp_amount,0) + NVL(mp_holdback_amount,0))
    FROM eres.er_milepayment_details
    WHERE fk_mileachieved = 0
    AND ( (mp_linkto_type = 'M'
    AND mp_level1_id      = m.pk_milestone )
    OR (mp_linkto_type    = 'M'
    AND mp_level1_id      = 0
    AND mp_linkto_id      = m.pk_milestone) )
    ),0)           AS rec_amt ,
    msrul_category AS milestone_type,
    msrul_amount milestone_amt,
    msrul_pay_type payment_type,
    msrul_pay_for payment_for,
    milestone_holdback
  FROM VDA.VDA_V_MILESTONES m,
    eres.ER_STUDY b
  WHERE MSRUL_CATEGORY = 'Additional Milestone'
  AND b.pk_study       = m.fk_study
  AND MSRUL_STATUS     ='Active';
  COMMENT ON COLUMN "VDA"."VDA_V_ADDNL_MILESTONE_DET"."PK_MILESTONE"
IS
  'The Primary of the milestones achievement records';
  COMMENT ON COLUMN "VDA"."VDA_V_ADDNL_MILESTONE_DET"."MSACH_MILESTONE_DESC"
IS
  'Milestone description';
  COMMENT ON COLUMN "VDA"."VDA_V_ADDNL_MILESTONE_DET"."MSACH_ACH_DATE"
IS
  'milestone achievement date';
  COMMENT ON COLUMN "VDA"."VDA_V_ADDNL_MILESTONE_DET"."CREATOR"
IS
  'The user who created the record (Audit)';
  COMMENT ON COLUMN "VDA"."VDA_V_ADDNL_MILESTONE_DET"."CREATED_ON"
IS
  'The date the record was created on (Audit)';
  COMMENT ON COLUMN "VDA"."VDA_V_ADDNL_MILESTONE_DET"."LAST_MODIFIED_DATE"
IS
  'The date the record was last modified on (Audit)';
  COMMENT ON COLUMN "VDA"."VDA_V_ADDNL_MILESTONE_DET"."LAST_MODIFIED_BY"
IS
  'The user who last modified the record(Audit)';
  COMMENT ON COLUMN "VDA"."VDA_V_ADDNL_MILESTONE_DET"."FK_STUDY"
IS
  'The Foreign Key to the study milestone is linked with';
  COMMENT ON COLUMN "VDA"."VDA_V_ADDNL_MILESTONE_DET"."FK_ACCOUNT"
IS
  'The account the milestone is linked with';
  COMMENT ON COLUMN "VDA"."VDA_V_ADDNL_MILESTONE_DET"."STUDY_NUMBER"
IS
  'The Study Number of the study the milestone is linked with';
  COMMENT ON COLUMN "VDA"."VDA_V_ADDNL_MILESTONE_DET"."LAST_MODIFIED_BY_FK"
IS
  'The Key to identify the Last Modified By of the record.';
  COMMENT ON COLUMN "VDA"."VDA_V_ADDNL_MILESTONE_DET"."CREATOR_FK"
IS
  'The Key to identify the Creator of the record.';
  COMMENT ON COLUMN "VDA"."VDA_V_ADDNL_MILESTONE_DET"."PAYMENT_DT"
IS
  'The Payments linked with the Milestone Achievement';
  COMMENT ON COLUMN "VDA"."VDA_V_ADDNL_MILESTONE_DET"."INV_AMT"
IS
  'The total amount invoiced against the Milestone Achievement';
  COMMENT ON COLUMN "VDA"."VDA_V_ADDNL_MILESTONE_DET"."INV_NUM"
IS
  'The Invoices linked with the Milestone Achievement';
  COMMENT ON COLUMN "VDA"."VDA_V_ADDNL_MILESTONE_DET"."REC_AMT"
IS
  'The total amount reconciled against the Milestone Achievement';
  COMMENT ON COLUMN "VDA"."VDA_V_ADDNL_MILESTONE_DET"."MILESTONE_TYPE"
IS
  'The Milestone Type';
  COMMENT ON COLUMN "VDA"."VDA_V_ADDNL_MILESTONE_DET"."MILESTONE_AMT"
IS
  'The Milestone Amount';
  COMMENT ON COLUMN "VDA"."VDA_V_ADDNL_MILESTONE_DET"."PAYMENT_TYPE"
IS
  'Milestone Payment Type';
  COMMENT ON COLUMN "VDA"."VDA_V_ADDNL_MILESTONE_DET"."PAYMENT_FOR"
IS
  'Milestone Payment For';
  COMMENT ON COLUMN "VDA"."VDA_V_ADDNL_MILESTONE_DET"."MILESTONE_HOLDBACK"
IS
  'Milestone Holdback amount per Milestone definition';
  COMMENT ON TABLE "VDA"."VDA_V_ADDNL_MILESTONE_DET"
IS
  'This view provides access to the Additional Milestones data';
  
  

INSERT INTO VDA_TRACK_PATCHES
(
  PATCH_PK       ,
  DB_VER_MJR     ,
  DB_VER_MNR     ,
  DB_PATCH_NAME  ,
  FIRED_ON       ,
  APP_VERSION    
) VALUES(SEQ_VDA_TRACK_PATCHES.nextval,6,0,'06_VDA_V_ADDNL_MILESTONE_DET.sql',sysdate,'VDA1.4#18Reports') ;

commit	;	  