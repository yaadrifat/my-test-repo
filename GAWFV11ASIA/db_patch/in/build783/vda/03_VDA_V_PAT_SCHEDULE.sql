CREATE OR REPLACE FORCE VIEW "VDA"."VDA_V_PAT_SCHEDULE" ("PAT_STUDY_ID", "FK_STUDY", "CALENDAR_PK", "PTSCH_VISIT", "PTSCH_SUGSCH_DATE", "PTSCH_ACTSCH_DATE", "PTSCH_EVENT_PK", "PTSCH_EVENT", "PTSCH_EVENT_STAT", "PTSCH_EVENT_STAT_SUBTYPE", "EVENT_ID", "CREATOR", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "CREATED_ON", "FK_PER", "EVENTSTAT_DATE", "EVENTSTAT_NOTES", "PATPROT_STAT", "FK_SITE_ENROLLING", "EVENT_SEQUENCE", "SERVICE_SITE_ID", "FACILITY_ID", "COVERAGE_TYPE", "ENROLLING_SITE_NAME", "STUDY_NUMBER", "FK_ACCOUNT", "VISIT_NAME", "SCHEDULE_STATUS", "LAST_MODIFIED_BY_FK", "CREATOR_FK", "EVENT_CPTCODE", "TOTAL_EVENT_COST", "CALENDAR_NAME", "PSTAT_ASSIGNED_TO")
AS
  SELECT PATPROT_PATSTDID PAT_STUDY_ID,
    c.FK_STUDY,
    a.SESSION_ID,
    FK_VISIT,
    START_DATE_TIME PTSCH_SUGSCH_DATE,
    ACTUAL_SCHDATE PTSCH_ACTSCH_DATE,
    a.fk_assoc,
    DESCRIPTION PTSCH_EVENT,
    (SELECT CODELST_DESC FROM esch.SCH_CODELST WHERE ISCONFIRMED = PK_CODELST
    ) PTSCH_EVENT_STAT,
    (SELECT CODELST_SUBTYP FROM esch.SCH_CODELST WHERE ISCONFIRMED = PK_CODELST
    ) PTSCH_EVENT_STAT_SUBTYPE,
    EVENT_ID,
    (SELECT USR_FIRSTNAME
      || ' '
      || USR_LASTNAME
    FROM eres.ER_USER
    WHERE PK_USER = a.CREATOR
    ) CREATOR,
    (SELECT USR_FIRSTNAME
      || ' '
      || USR_LASTNAME
    FROM eres.ER_USER
    WHERE PK_USER = a.LAST_MODIFIED_BY
    ) LAST_MODIFIED_BY,
    a.LAST_MODIFIED_DATE,
    a.CREATED_ON,
    c.FK_PER,
    EVENT_EXEON EVENTSTAT_DATE,
    notes EVENTSTAT_NOTES,
    PATPROT_STAT,
    fk_site_enrolling,
    event_sequence,
    DECODE (service_site_id, NULL, NULL, 0, NULL,
    (SELECT site_name FROM eres.er_site WHERE pk_site = service_site_id
    )) service_site_id,
    DECODE (facility_id, NULL, NULL, 0, NULL,
    (SELECT site_name FROM eres.er_site WHERE pk_site = facility_id
    )) facility_id,
    (SELECT CODELST_DESC
    FROM esch.SCH_CODELST
    WHERE fk_codelst_covertype = PK_CODELST
    ) coverage_type,
    (SELECT site_name FROM eres.er_site st WHERE st.pk_site = fk_site_enrolling
    ) enrolling_site_name,
    STUDY_NUMBER,
    FK_ACCOUNT,
    (SELECT visit_name
    FROM esch.sch_protocol_visit
    WHERE pk_protocol_visit = FK_VISIT
    ) visit_name,
    DECODE (status, 0, 'Current', 5, 'Discontinued or old') schedule_status,
    a.LAST_MODIFIED_BY AS LAST_MODIFIED_BY_FK,
    a.CREATOR          AS CREATOR_FK ,
    (SELECT x.event_cptcode FROM esch.event_assoc x WHERE x.event_id = a.fk_assoc
    ) EVENT_CPTCODE ,
    esch.TOT_COST(a.fk_assoc) TOTAL_EVENT_COST,
    (SELECT y.name FROM esch.event_assoc y WHERE y.event_id = a.SESSION_ID
    ) CALENDAR_NAME,
    (SELECT USR_FIRSTNAME
      || ' '
      || USR_LASTNAME
    FROM eres.ER_USER
    WHERE PK_USER = c.FK_USERASSTO
    ) PSTAT_ASSIGNED_TO
  FROM esch.SCH_EVENTS1 a,
    eres.ER_PATPROT c,
    eres.er_study s
  WHERE fk_patprot <> 0
  AND pk_patprot    = fk_patprot
  AND s.pk_study    = c.fk_study;
  COMMENT ON COLUMN "VDA"."VDA_V_PAT_SCHEDULE"."PAT_STUDY_ID"
IS
  'he patient study id';
  COMMENT ON COLUMN "VDA"."VDA_V_PAT_SCHEDULE"."FK_STUDY"
IS
  'The Foreign Key to the study milestone is linked with';
  COMMENT ON COLUMN "VDA"."VDA_V_PAT_SCHEDULE"."CALENDAR_PK"
IS
  'The foreign key to the calendar record';
  COMMENT ON COLUMN "VDA"."VDA_V_PAT_SCHEDULE"."PTSCH_VISIT"
IS
  'The visit the scheduled event is linked with ';
  COMMENT ON COLUMN "VDA"."VDA_V_PAT_SCHEDULE"."PTSCH_SUGSCH_DATE"
IS
  'The suggested or initial scheduled date of the event per calendar design';
  COMMENT ON COLUMN "VDA"."VDA_V_PAT_SCHEDULE"."PTSCH_ACTSCH_DATE"
IS
  'The actual scheduled date of the event';
  COMMENT ON COLUMN "VDA"."VDA_V_PAT_SCHEDULE"."PTSCH_EVENT_PK"
IS
  'The foreign key to the event record in the source calendar';
  COMMENT ON COLUMN "VDA"."VDA_V_PAT_SCHEDULE"."PTSCH_EVENT"
IS
  'The name of the event';
  COMMENT ON COLUMN "VDA"."VDA_V_PAT_SCHEDULE"."PTSCH_EVENT_STAT"
IS
  'The current event status';
  COMMENT ON COLUMN "VDA"."VDA_V_PAT_SCHEDULE"."PTSCH_EVENT_STAT_SUBTYPE"
IS
  'The current event status SubType';
  COMMENT ON COLUMN "VDA"."VDA_V_PAT_SCHEDULE"."EVENT_ID"
IS
  'The Primary Key of the schedule record';
  COMMENT ON COLUMN "VDA"."VDA_V_PAT_SCHEDULE"."CREATOR"
IS
  'The user who created the calendar record (Audit)';
  COMMENT ON COLUMN "VDA"."VDA_V_PAT_SCHEDULE"."LAST_MODIFIED_BY"
IS
  'The user who last modified the calendar record(Audit)';
  COMMENT ON COLUMN "VDA"."VDA_V_PAT_SCHEDULE"."LAST_MODIFIED_DATE"
IS
  'The date the calendar record was last modified on (Audit)';
  COMMENT ON COLUMN "VDA"."VDA_V_PAT_SCHEDULE"."CREATED_ON"
IS
  'The date the calendar record was created on (Audit)';
  COMMENT ON COLUMN "VDA"."VDA_V_PAT_SCHEDULE"."FK_PER"
IS
  'The foreign Key to the patient record';
  COMMENT ON COLUMN "VDA"."VDA_V_PAT_SCHEDULE"."EVENTSTAT_DATE"
IS
  'The date event status was entered on ';
  COMMENT ON COLUMN "VDA"."VDA_V_PAT_SCHEDULE"."EVENTSTAT_NOTES"
IS
  'The notes linked with the event status';
  COMMENT ON COLUMN "VDA"."VDA_V_PAT_SCHEDULE"."PATPROT_STAT"
IS
  'The enrollment record status. A value of 1 indicates that the record is for most recent patient calendar schedule';
  COMMENT ON COLUMN "VDA"."VDA_V_PAT_SCHEDULE"."FK_SITE_ENROLLING"
IS
  'The foreign key to the enrolling site';
  COMMENT ON COLUMN "VDA"."VDA_V_PAT_SCHEDULE"."SERVICE_SITE_ID"
IS
  'The site the event/service is or will be performed at';
  COMMENT ON COLUMN "VDA"."VDA_V_PAT_SCHEDULE"."FACILITY_ID"
IS
  'The facility the event/service is linked with';
  COMMENT ON COLUMN "VDA"."VDA_V_PAT_SCHEDULE"."COVERAGE_TYPE"
IS
  'The patient level coverage type identifier for the calendar-visit-even';
  COMMENT ON COLUMN "VDA"."VDA_V_PAT_SCHEDULE"."ENROLLING_SITE_NAME"
IS
  'Patient Enrolling Site name';
  COMMENT ON COLUMN "VDA"."VDA_V_PAT_SCHEDULE"."STUDY_NUMBER"
IS
  'The Study Number of the study the payment is linked with';
  COMMENT ON COLUMN "VDA"."VDA_V_PAT_SCHEDULE"."FK_ACCOUNT"
IS
  'The account the calendar is linked with';
  COMMENT ON COLUMN "VDA"."VDA_V_PAT_SCHEDULE"."SCHEDULE_STATUS"
IS
  'Flag for indicating if this the current schedule for the patient';
  COMMENT ON COLUMN "VDA"."VDA_V_PAT_SCHEDULE"."LAST_MODIFIED_BY_FK"
IS
  'The Key to identify the Last Modified By of the record.';
  COMMENT ON COLUMN "VDA"."VDA_V_PAT_SCHEDULE"."CREATOR_FK"
IS
  'The Key to identify the Creator';
  COMMENT ON COLUMN "VDA"."VDA_V_PAT_SCHEDULE"."EVENT_CPTCODE"
IS
  'The CPT Code linked with the scheduled event';
  COMMENT ON COLUMN "VDA"."VDA_V_PAT_SCHEDULE"."TOTAL_EVENT_COST"
IS
  'The total Cost calculated per Event Cost in the Calendar ';
  COMMENT ON COLUMN "VDA"."VDA_V_PAT_SCHEDULE"."CALENDAR_NAME"
IS
  'The Name of the Calendar linked with the schedule record  ';
  COMMENT ON COLUMN "VDA"."VDA_V_PAT_SCHEDULE"."PSTAT_ASSIGNED_TO"
IS
  'The user patient is assigned to';
  COMMENT ON TABLE "VDA"."VDA_V_PAT_SCHEDULE"
IS
  'This view provides access to the Patient Schedules on studies';
  
  
  INSERT INTO VDA_TRACK_PATCHES
(
  PATCH_PK       ,
  DB_VER_MJR     ,
  DB_VER_MNR     ,
  DB_PATCH_NAME  ,
  FIRED_ON       ,
  APP_VERSION    
) VALUES(SEQ_VDA_TRACK_PATCHES.nextval,5,0,'03_VDA_V_PAT_SCHEDULE.sql',sysdate,'VDA1.4#18Reports') ;

commit	;	