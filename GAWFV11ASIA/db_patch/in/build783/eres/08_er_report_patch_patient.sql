DECLARE 
	table_check number;
	row_check number;
	update_sql clob;
	
BEGIN
	SELECT count(*)
	INTO table_check
	FROM user_tab_cols
	WHERE TABLE_NAME = 'ER_REPORT';
	
	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=66;
		
		IF (row_check > 0) then
		
		update_sql:='SELECT (SELECT form_name FROM ER_FORMLIB WHERE pk_formlib = fk_formlib) AS form_name,
(SELECT per_code FROM ER_PER WHERE pk_per = fk_per) AS patient_id,
(SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''enrolled'')) AS enrolled_date,
(SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''active'') AND ROWNUM = 1) AS active_date,
(SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''offtreat'') AND ROWNUM = 1) AS offtreat_date,
(SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''offstudy'') AND ROWNUM = 1) AS offstudy_date,
(SELECT (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_stat) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND patstudystat_date = (SELECT MAX(patstudystat_date) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study ) AND ROWNUM = 1) AS current_status,
(SELECT COUNT(*) FROM ER_PATFORMS WHERE fk_formlib = x.fk_formlib AND fk_patprot = pk_patprot AND record_type <> ''D'') AS tot_forms,
(SELECT COUNT(*) FROM ER_PATFORMS WHERE fk_formlib = x.fk_formlib AND fk_patprot = pk_patprot AND record_type <> ''D'' AND form_completed = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''fillformstat'' AND codelst_subtyp = ''complete'')) AS comp_formstat,
(SELECT COUNT(*) FROM ER_PATFORMS WHERE fk_formlib = x.fk_formlib AND fk_patprot = pk_patprot AND record_type <> ''D'' AND form_completed = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''fillformstat'' AND codelst_subtyp = ''working'')) AS work_formstat,
(SELECT COUNT(*) FROM ER_PATFORMS WHERE fk_formlib = x.fk_formlib AND fk_patprot = pk_patprot AND record_type <> ''D'' AND form_completed NOT IN (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''fillformstat'' AND (codelst_subtyp = ''working'' OR codelst_subtyp = ''complete''))) AS othr_formstat,
(SELECT to_char(MAX(created_on),PKG_DATEUTIL.F_GET_DATETIMEFORMAT) FROM ER_PATFORMS WHERE fk_formlib = x.fk_formlib AND fk_patprot = pk_patprot AND record_type <> ''D'' ) AS form_createdon, (SELECT (SELECT usr_firstname || '' '' || usr_lastname FROM ER_USER WHERE pk_user = y.creator) FROM ER_PATFORMS y WHERE fk_formlib = x.fk_formlib AND fk_patprot = pk_patprot AND record_type <> ''D'' AND created_on = (SELECT MAX(created_on) FROM ER_PATFORMS WHERE fk_formlib = x.fk_formlib AND fk_patprot = pk_patprot AND record_type <> ''D'' ) AND ROWNUM = 1) AS form_createdby,
(SELECT to_char(MAX(last_modified_date),PKG_DATEUTIL.F_GET_DATEtimeFORMAT) FROM ER_PATFORMS WHERE fk_formlib = x.fk_formlib AND fk_patprot = pk_patprot AND record_type <> ''D'' ) AS form_modon,
(SELECT (SELECT usr_firstname || '' '' || usr_lastname FROM ER_USER WHERE pk_user = y.last_modified_by) FROM ER_PATFORMS y WHERE fk_formlib = x.fk_formlib AND fk_patprot = pk_patprot AND record_type <> ''D'' AND last_modified_date = (SELECT MAX(last_modified_date) FROM ER_PATFORMS WHERE fk_formlib = x.fk_formlib AND fk_patprot = pk_patprot AND record_type <> ''D'' ) AND ROWNUM = 1) AS form_modby
FROM
(SELECT DISTINCT a.fk_per,fk_formlib,fk_study, pk_patprot
FROM ER_PATPROT a, ER_PATFORMS b
WHERE pk_patprot = fk_patprot
AND fk_study IN (:studyId) AND a.fk_per IN (:patientId) AND
(b.created_on BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) OR b.last_modified_date BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) ) ) x   UNION ALL
SELECT ''Adverse EVENTS'' AS form_name,
(SELECT per_code FROM ER_PER WHERE pk_per = fk_per) AS patient_id,
(SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''enrolled'')) AS enrolled_date,
(SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''active'') AND ROWNUM = 1) AS active_date,
(SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''offtreat'') AND ROWNUM = 1) AS offtreat_date,
(SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''offstudy'') AND ROWNUM = 1) AS offstudy_date,
(SELECT (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_stat) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND patstudystat_date = (SELECT MAX(patstudystat_date) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study ) AND ROWNUM = 1) AS current_status, (SELECT COUNT(*) FROM ESCH.sch_adverseve WHERE fk_study = x.fk_study AND fk_per = x.fk_per) AS tot_forms,
(SELECT COUNT(*) FROM ESCH.sch_adverseve WHERE fk_study = x.fk_study AND fk_per = x.fk_per AND form_status = (SELECT pk_codelst FROM ESCH.sch_codelst WHERE trim(codelst_type) = ''fillformstat'' AND trim(codelst_subtyp) = ''complete'')) AS comp_formstat,
(SELECT COUNT(*) FROM ESCH.sch_adverseve WHERE fk_study = x.fk_study AND fk_per = x.fk_per AND form_status = (SELECT pk_codelst FROM ESCH.sch_codelst WHERE trim(codelst_type) = ''fillformstat'' AND trim(codelst_subtyp) = ''working'')) AS work_formstat,
0 AS othr_formstat,
(SELECT to_char(MAX(created_on),PKG_DATEUTIL.F_GET_DATETIMEFORMAT) FROM ESCH.sch_adverseve WHERE fk_study = x.fk_study AND fk_per = x.fk_per) AS form_createdon,
(SELECT (SELECT usr_firstname || '' '' || usr_lastname FROM ER_USER WHERE pk_user = y.creator) FROM ESCH.sch_adverseve y WHERE fk_study = x.fk_study AND fk_per = x.fk_per AND created_on = (SELECT MAX(created_on) FROM ESCH.sch_adverseve WHERE fk_study = x.fk_study AND fk_per = x.fk_per) AND ROWNUM = 1) AS form_createdby,
(SELECT  to_char(MAX(last_modified_date),PKG_DATEUTIL.F_GET_DATEtimeFORMAT) FROM ESCH.sch_adverseve WHERE fk_study = x.fk_study AND fk_per = x.fk_per) AS form_modon,
(SELECT (SELECT usr_firstname || '' '' || usr_lastname FROM ER_USER WHERE pk_user =  y.last_modified_by) FROM ESCH.sch_adverseve y WHERE fk_study = x.fk_study AND fk_per = x.fk_per AND last_modified_date = (SELECT MAX(last_modified_date) FROM ESCH.sch_adverseve WHERE fk_study = x.fk_study AND fk_per = x.fk_per) AND ROWNUM = 1) AS form_modby
FROM
(SELECT DISTINCT a.fk_per,a.fk_study
FROM ER_PATPROT a, ESCH.sch_adverseve b
WHERE a.FK_PER = b.FK_PER AND a.fk_study = b.fk_study
AND a.fk_study IN (:studyId) AND Patprot_stat = 1 AND a.fk_per IN (:patientId) AND
(b.created_on BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) OR b.last_modified_date BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) ) ) x
 UNION ALL
SELECT ''Labs'' AS form_name,
(SELECT per_code FROM ER_PER WHERE pk_per = fk_per) AS patient_id, 
(SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''enrolled'')) AS enrolled_date,
(SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''active'') AND ROWNUM = 1) AS active_date,
(SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''offtreat'') AND ROWNUM = 1) AS offtreat_date,
(SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''offstudy'') AND ROWNUM = 1) AS offstudy_date,
(SELECT (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_stat) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study AND patstudystat_date = (SELECT MAX(patstudystat_date) FROM ER_PATSTUDYSTAT WHERE fk_per = x.fk_per AND fk_study = x.fk_study ) AND ROWNUM = 1) AS current_status,
(SELECT COUNT(*) FROM ER_PATLABS WHERE fk_per = x.fk_per) AS tot_forms,
0 AS comp_formstat,
0 AS work_formstat,
0 AS othr_formstat,
(SELECT to_char(MAX(created_on),PKG_DATEUTIL.F_GET_DATETIMEFORMAT) FROM ER_PATLABS WHERE fk_per = x.fk_per) AS form_createdon,
(SELECT (SELECT usr_firstname || '' '' || usr_lastname FROM ER_USER WHERE pk_user = y.creator) FROM ER_PATLABS y WHERE fk_per = x.fk_per AND created_on = (SELECT MAX(created_on) FROM ER_PATLABS WHERE fk_per = x.fk_per ) AND ROWNUM = 1) AS form_createdby,
(SELECT to_char(MAX(last_modified_date),PKG_DATEUTIL.F_GET_DATEtimeFORMAT) FROM ER_PATLABS WHERE fk_per = x.fk_per) AS form_modon,
(SELECT (SELECT usr_firstname || '' '' || usr_lastname FROM ER_USER WHERE pk_user =  y.last_modified_by) FROM ER_PATLABS y WHERE fk_per = x.fk_per AND last_modified_date = (SELECT MAX(last_modified_date) FROM ER_PATLABS WHERE fk_per = x.fk_per ) AND ROWNUM = 1) AS form_modby
FROM
(SELECT DISTINCT a.fk_per,a.fk_study
FROM ER_PATPROT a, ER_PATLABS b
WHERE a.fk_per = b.fk_per
AND a.fk_study IN (:studyId) AND Patprot_stat = 1 AND a.fk_per IN (:patientId) AND
(b.created_on BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) OR b.last_modified_date BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) ) ) x
ORDER BY 2,1';
		
		UPDATE 
			ER_REPORT 
				SET 
					rep_name='Study Patient Form Tracking', 
					rep_desc='Study Patient Form Tracking',
					rep_sql_clob=update_sql,
					rep_columns='Patient ID, Form Name, Enrolled Date, Current Status, Total Responses, Complete Responses, Work in Progress Responses, Other Responses, Created On, Last Modified On, Last Modified By',
					rep_filterby='Date (Created On or Last Modified On Date) </br> Study (Study Number) </br> Patient (Patient ID)',
					rep_type='rep_ae'
					
				WHERE 
					pk_report=66;	
		
		END IF;

	
	
	
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=126;
		
		IF (row_check > 0) then
		
		update_sql:='SELECT
(SELECT per_code FROM ER_PER WHERE pk_per = g.fk_per) AS patient_id,
g.fk_per,
g.fk_study,
patprot_patstdid AS patient_study_id,
(SELECT study_number FROM ER_STUDY WHERE pk_study = fk_study) AS study_number,
form_name,
(SELECT site_name FROM ER_SITE WHERE pk_site = g.fk_site_enrolling) AS site,
SUM(DECODE(fk_codelst_querystatus,(select pk_codelst from er_codelst where codelst_type = ''query_status'' and codelst_subtyp = ''open''),1,0)) AS open_count,
SUM(DECODE(fk_codelst_querystatus,(select pk_codelst from er_codelst where codelst_type = ''query_status'' and codelst_subtyp = ''resolved''),1,0)) AS resolved_count,
SUM(DECODE(fk_codelst_querystatus,(select pk_codelst from er_codelst where codelst_type = ''query_status'' and codelst_subtyp = ''re-opened''),1,0)) AS reopen_count,
SUM(DECODE(fk_codelst_querystatus,(select pk_codelst from er_codelst where codelst_type = ''query_status'' and codelst_subtyp = ''closed''),1,0)) AS closed_count
FROM ER_FORMQUERY a, ER_FORMQUERYSTATUS b, ER_FORMFLD c, ER_FORMSEC d, ER_FORMLIB e, ER_PATFORMS f, ER_PATPROT g
WHERE a.pk_formquery = b.fk_formquery
AND b.PK_FORMQUERYSTATUS = (select max(b1.PK_FORMQUERYSTATUS) from ER_FORMQUERYSTATUS b1
where a.pk_formquery = b1.fk_formquery
AND trim(b1.ENTERED_ON) = (select max(trim(b2.ENTERED_ON)) from ER_FORMQUERYSTATUS b2
where a.pk_formquery = b2.fk_formquery))
AND a.fk_field = c.fk_field
AND d.pk_formsec = c.fk_formsec
AND e.pk_formlib = d.fk_formlib AND f.RECORD_TYPE <> ''D''
AND pk_formlib = f.fk_formlib
AND pk_patforms = fk_querymodule
AND pk_patprot = fk_patprot
AND fk_study IN (:studyId) 
and g.fk_site_enrolling in (:orgId)
and (a.created_on BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) )
GROUP BY fk_study,g.fk_per,patprot_patstdid,form_name,g.fk_site_enrolling';
		
		UPDATE 
			ER_REPORT 
				SET 
					rep_name='Study Form Response Query Count',
					rep_desc='Study Form Response Query Count',
					rep_sql_clob=update_sql,
					rep_columns='Study Number, Enrolling Site, Patient Study ID, Form Name, # Open, # Resolved, # Re-Opened, # Closed',
					rep_filterby='Date (Query Status Date) </br> Study (Study Number) </br> Organization (Enrolling Site)',
					rep_type='rep_ae'
					
				WHERE 
					pk_report=126;
		
		END IF;

	
	
	
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=127;
		
		IF (row_check > 0) then
	
		update_sql:='SELECT
(SELECT per_code FROM ER_PER WHERE pk_per = g.fk_per) AS patient_id,
patprot_patstdid AS patient_study_id,
(SELECT study_number FROM ER_STUDY WHERE pk_study = fk_study) AS study_number,
g.fk_per,
g.fk_study,
form_name, formsec_name,
(SELECT fld_name FROM ER_FLDLIB WHERE pk_field = a.fk_field) AS field,
DECODE (formquery_type, ''1'',''System Query'',''2'',''Manual Query'',''3'',''Response'') AS query_type,
(SELECT codelst_desc FROM ER_CODELST WHERE fk_codelst_querytype = pk_codelst)AS querydesc,
query_notes,
(SELECT codelst_desc FROM ER_CODELST WHERE fk_codelst_querystatus = pk_codelst)AS fk_codelst_querystatus,
(SELECT usr_firstname || '' '' || usr_lastname FROM ER_USER WHERE pk_user = a.creator) AS creator,
to_char(a.created_on,PKG_DATEUTIL.F_GET_DATEtimeFORMAT) as created_on,
fk_formquery,
fk_querymodule,to_char(patforms_filldate,PKG_DATEUTIL.F_GET_DATEFORMAT) as patforms_filldate
FROM ER_FORMQUERY a, ER_FORMQUERYSTATUS b, ER_FORMFLD c, ER_FORMSEC d, ER_FORMLIB e, ER_PATFORMS f, ER_PATPROT g
WHERE b.fk_formquery = a.pk_formquery
AND b.PK_FORMQUERYSTATUS = (select max(b1.PK_FORMQUERYSTATUS) from ER_FORMQUERYSTATUS b1
where a.pk_formquery = b1.fk_formquery
AND trim(b1.ENTERED_ON) = (select max(trim(b2.ENTERED_ON)) from ER_FORMQUERYSTATUS b2
where a.pk_formquery = b2.fk_formquery))
AND a.fk_field = c.fk_field
AND d.pk_formsec = c.fk_formsec
AND e.pk_formlib = d.fk_formlib
AND pk_formlib = f.fk_formlib AND f.RECORD_TYPE <> ''D''
AND pk_patforms = fk_querymodule
AND pk_patprot = fk_patprot
AND fk_study IN (:studyId) 
and g.fk_site_enrolling in (:orgId)
and (a.created_on BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) )
ORDER BY patient_id, form_name, formsec_name, field';
	
		UPDATE 
			ER_REPORT 
				SET 
					rep_name='Study Form Response Query Details',
					rep_desc='Study Form Response Query Details',
					rep_sql_clob=update_sql,
					rep_columns='Study Number, Patient Study ID, Form Name, Section, Field, Query Type, Description, Notes, Status, Created By, Created On',
					rep_filterby='Date (Query Status Date) </br> Study (Study Number) </br> Organization (Enrolling Site)',
					rep_type='rep_ae'
					
				WHERE 
					pk_report=127;
		
		END IF;

	
	
	
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=282;
		
		IF (row_check = 0) then
			INSERT INTO er_report 
				(pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable)
				
			VALUES
			(282
			,'Patient Schedule (Current Schedule)'
			,'Patient Schedule (Current Schedule)'
			,0
			,'N'
			,'Scheduled Date, Suggested Date, Study Number, Enrolling Site, Patient Study ID, Assigned, Calendar, Visit, Event, Event Status, Status Date, Notes'
			,'Date (Scheduled Date) </br> Study (Study Number) </br> Organization (Enrolling Site)'
			,1
			,'rep_ae'
			,''
			,'date:studyId:orgId');
			
			update_sql:='select 
STUDY_NUMBER,
TO_CHAR(PTSCH_ACTSCH_DATE,
PKG_DATEUTIL.F_GET_DATEFORMAT) PTSCH_ACTSCH_DATE,
TO_CHAR(PTSCH_SUGSCH_DATE,
 PKG_DATEUTIL.F_GET_DATEFORMAT) PTSCH_SUGSCH_DATE,
ENROLLING_SITE_NAME,
PAT_STUDY_ID,
CALENDAR_PK, 
CALENDAR_NAME,
VISIT_NAME,
PTSCH_EVENT,
PTSCH_EVENT_STAT,
TO_CHAR(EVENTSTAT_DATE,
PKG_DATEUTIL.F_GET_DATEFORMAT) EVENTSTAT_DATE,
EVENTSTAT_NOTES,
EVENT_SEQUENCE,
COVERAGE_TYPE,
PSTAT_ASSIGNED_TO,
SCHEDULE_STATUS,
FK_STUDY,
FK_PER,
FK_SITE_ENROLLING
from vda.vda_v_pat_schedule
where schedule_status = ''Current''
and fk_study IN (:studyId) and FK_SITE_ENROLLING IN (:orgId) AND TRUNC(PTSCH_ACTSCH_DATE) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)


';
			
			UPDATE 
			ER_REPORT 
				SET 
					rep_sql_clob=update_sql
				WHERE 
					pk_report=282;
		
		END IF;

	
	
	
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=283;
		
		IF (row_check = 0) then
			INSERT INTO er_report 
				(pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable)
				
			VALUES
			(283
			,'Patient Schedule (Discontinued Schedule)'
			,'Patient Schedule (Discontinued Schedule)'
			,0
			,'N'
			,'Scheduled Date, Suggested Date, Study Number, Enrolling Site, Patient Study ID, Assigned, Calendar, Visit, Event, Event Status, Status Date, Notes'
			,'Date (Scheduled Date) </br> Study (Study Number) </br> Organization (Enrolling Site)'
			,1
			,'rep_ae'
			,''
			,'date:studyId:orgId');
			
			update_sql:='select 
STUDY_NUMBER,
TO_CHAR(PTSCH_ACTSCH_DATE,
PKG_DATEUTIL.F_GET_DATEFORMAT) PTSCH_ACTSCH_DATE,
TO_CHAR(PTSCH_SUGSCH_DATE,
 PKG_DATEUTIL.F_GET_DATEFORMAT) PTSCH_SUGSCH_DATE,
ENROLLING_SITE_NAME,
PAT_STUDY_ID,
CALENDAR_PK, 
CALENDAR_NAME,
VISIT_NAME,
PTSCH_EVENT,
PTSCH_EVENT_STAT,
TO_CHAR(EVENTSTAT_DATE,
PKG_DATEUTIL.F_GET_DATEFORMAT) EVENTSTAT_DATE,
EVENTSTAT_NOTES,
EVENT_SEQUENCE,
COVERAGE_TYPE,
PSTAT_ASSIGNED_TO,
SCHEDULE_STATUS,
FK_STUDY,
FK_PER,
FK_SITE_ENROLLING
from vda.vda_v_pat_schedule
where schedule_status = ''Discontinued or old''
and fk_study IN (:studyId) and FK_SITE_ENROLLING IN (:orgId) AND TRUNC(PTSCH_ACTSCH_DATE) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)


';
			
			UPDATE 
			ER_REPORT 
				SET 
					rep_sql_clob=update_sql
					
				WHERE 
					pk_report=283;
		
		END IF;

	
	
	
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=284;
		
		IF (row_check = 0) then
			INSERT INTO er_report 
				(pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable)
				
			VALUES
			(284
			,'Patient Current Schedule - Upcoming Events'
			,'Patient Current Schedule - Upcoming Events'
			,0
			,'N'
			,'In # Days, Scheduled Date, Suggested Date, Study Number, Enrolling Site, Patient Study ID, Calendar, Visit, Event'
			,'Date (Scheduled Date) </br> Study (Study Number) </br> Organization (Enrolling Site)'
			,1
			,'rep_ae'
			,''
			,'date:studyId:orgId');
			
			update_sql:='select 
TO_CHAR(PTSCH_ACTSCH_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) PTSCH_ACTSCH_DATE,
TO_CHAR(PTSCH_SUGSCH_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) PTSCH_SUGSCH_DATE,
STUDY_NUMBER,
ENROLLING_SITE_NAME,
PAT_STUDY_ID,
CALENDAR_PK, 
CALENDAR_NAME,
VISIT_NAME,
PTSCH_EVENT,
PTSCH_EVENT_STAT,
TO_CHAR(EVENTSTAT_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) EVENTSTAT_DATE,
EVENTSTAT_NOTES,
EVENT_SEQUENCE,
COVERAGE_TYPE,
SCHEDULE_STATUS,
FK_STUDY,
FK_PER,
FK_SITE_ENROLLING
,
PSTAT_ASSIGNED_TO,
(SELECT to_date(ptsch_actsch_date) - sysdate FROM DUAL) as DIFF
from vda.vda_v_pat_schedule
where schedule_status = ''Current'' and ptsch_actsch_date >= trunc(sysdate) and eventstat_date is null
and fk_study IN (:studyId) and FK_SITE_ENROLLING IN (:orgId) AND TRUNC(PTSCH_ACTSCH_DATE) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)';
			
			UPDATE 
			ER_REPORT 
				SET 
					rep_sql_clob=update_sql
					
				WHERE 
					pk_report=284;
		
		END IF;



	
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=285;
		
		IF (row_check = 0) then
			INSERT INTO er_report 
				(pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable)
				
			VALUES
			(285
			,'Patient Current Schedule - Missed Events'
			,'Patient Current Schedule - Missed Events'
			,0
			,'N'
			,'In # Days, Scheduled Date, Suggested Date, Study Number, Enrolling Site, Patient Study ID, Calendar, Visit, Event'
			,'Date (Scheduled Date) </br> Study (Study Number) </br> Organization (Enrolling Site)'
			,1
			,'rep_ae'
			,''
			,'date:studyId:orgId');
			
			update_sql:='select 
TO_CHAR(PTSCH_ACTSCH_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) PTSCH_ACTSCH_DATE,
TO_CHAR(PTSCH_SUGSCH_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) PTSCH_SUGSCH_DATE,
STUDY_NUMBER,
ENROLLING_SITE_NAME,
PAT_STUDY_ID,
CALENDAR_PK, 
CALENDAR_NAME,
VISIT_NAME,
PTSCH_EVENT,
PTSCH_EVENT_STAT,
TO_CHAR(EVENTSTAT_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) EVENTSTAT_DATE,
EVENTSTAT_NOTES,
EVENT_SEQUENCE,
COVERAGE_TYPE,
SCHEDULE_STATUS,
FK_STUDY,
FK_PER,
FK_SITE_ENROLLING
,
PSTAT_ASSIGNED_TO,
(SELECT sysdate - to_date(ptsch_actsch_date) FROM DUAL) as DIFF
from vda.vda_v_pat_schedule
where schedule_status = ''Current'' and ptsch_actsch_date <= trunc(sysdate) and eventstat_date is null
and fk_study IN (:studyId) and FK_SITE_ENROLLING IN (:orgId) AND TRUNC(PTSCH_ACTSCH_DATE) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)';
			
			UPDATE 
			ER_REPORT 
				SET 
					rep_sql_clob=update_sql
					
				WHERE 
					pk_report=285;
		
		END IF;



	
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=286;
		
		IF (row_check = 0) then
			INSERT INTO er_report 
				(pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable)
				
			VALUES
			(286
			,'Patient Events Done Out Of Window'
			,'Patient Events Done Out Of Window'
			,0
			,'N'
			,'Study Number, Enrolling Organization, Patient Study ID, Calendar, Visit, Event, Event Status, Status Date, Window Start, Window End'
			,'Date (Status Date) </br> Study (Study Number) </br> Organization (Enrolling Site)'
			,1
			,'rep_ae'
			,''
			,'date:studyId:orgId');
			
			update_sql:='select 
FK_STUDY,
FK_PER,
FK_SITE_ENROLLING,
(select STUDY_NUMBER from vda.vda_v_study_summary where vda.vda_v_study_summary.pk_study = VDA.vda_v_patschedule_all.fk_study) as STUDY_NUMBER,
PAT_STUDY_ID,
SITE_NAME,
CALENDAR_NAME,
VISIT_NAME,
PTSCH_EVENT,
PTSCH_EVENT_STAT,
TO_CHAR(EVENTSTAT_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) EVENTSTAT_DATE,
TO_CHAR(EVENT_WINDOW_START,PKG_DATEUTIL.F_GET_DATEFORMAT) EVENT_WINDOW_START,
TO_CHAR(EVENT_WINDOW_END,PKG_DATEUTIL.F_GET_DATEFORMAT) EVENT_WINDOW_END
from VDA.vda_v_patschedule_all where PTSCH_EVENT_STAT = ''Done'' and TRUNC(EVENTSTAT_DATE) NOT BETWEEN TRUNC(EVENT_WINDOW_START) AND TRUNC(EVENT_WINDOW_END
)
and fk_study IN (:studyId) and FK_SITE_ENROLLING IN (:orgId) AND TRUNC(EVENTSTAT_DATE) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)';
			
			UPDATE 
			ER_REPORT 
				SET 
					rep_sql_clob=update_sql
					
				WHERE 
					pk_report=286;
		
		END IF;



	
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=287;
		
		IF (row_check = 0) then
			INSERT INTO er_report 
				(pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable)
				
			VALUES
			(287
			,'Patient Treatment Arm Log'
			,'Patient Treatment Arm Log'
			,0
			,'N'
			,'Study Number, Enrolling Site, Patient Study ID, Treatment Arm, Drug Info, Start Date, End Date, Notes'
			,'Date (Start Date) </br> Study (Study Number) </br> Organization (Enrolling Site)'
			,1
			,'rep_ae'
			,''
			,'date:studyId:orgId');
			
			update_sql:='select
study_number,
patient_study_id,
fk_site_enrolling,
(select site_name from er_site where pk_site = fk_site_enrolling) as site,
treatment_name,
drug_info,
TO_CHAR(treatment_status_date,PKG_DATEUTIL.F_GET_DATEFORMAT) treatment_status_date,
TO_CHAR(treatment_end_date,PKG_DATEUTIL.F_GET_DATEFORMAT) treatment_end_date,
treatment_notes,
fk_study,
fk_per
from VDA.vda_v_pat_txarm
where fk_study IN (:studyId)
 and fk_site_enrolling IN (:orgId)
AND TRUNC(treatment_status_date) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)';
			
			UPDATE 
			ER_REPORT 
				SET 
					rep_sql_clob=update_sql
					
				WHERE 
					pk_report=287;
		
		END IF;



	
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=288;
		
		IF (row_check = 0) then
			INSERT INTO er_report 
				(pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable)
				
			VALUES
			(288
			,'Adverse Event Log'
			,'Adverse Event Log'
			,0
			,'N'
			,'Study Number, Enrolling Site, Patient Study ID, AE Type, Start Date, End Date, Grade, Name, AE Description, MedDRA, Dictionary'
			,'Date (AE Start Date) </br> Study (Study Number) </br> Organization (Enrolling Site)'
			,1
			,'rep_ae'
			,''
			,'date:studyId:orgId');
			
			update_sql:='select STUDY_NUMBER,
SITE_NAME,
(select distinct PATPROT_PATSTDID from ER_PATPROT where fk_per = patient_pk and fk_study = study_pk) as pat_studyid,
STUDY_TAREA,
STUDY_DISEASE_SITE, 
AE_TYPE,
AE_DESC,
TO_CHAR(AE_STDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) AE_STDATE,
TO_CHAR(AE_ENDDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) AE_ENDDATE,
AE_GRADE,
AE_NAME,
MEDDRA,
DICTIONARY,
FK_STUDY,
PATIENT_PK,
FK_SITE_ENROLLING
from VDA.VDA_V_AE_ALL_BYSITE
WHERE fk_site_enrolling IN (:orgId) 
AND TRUNC(AE_STDATE) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
AND fk_study IN (:studyId)';
			
			UPDATE 
			ER_REPORT 
				SET 
					rep_sql_clob=update_sql
					
				WHERE 
					pk_report=288;
		
		END IF;




		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=115;
		
		IF (row_check > 0) then
		
		update_sql:='select f_create_aexml('':studyId'','':orgId'',TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT),TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)) from dual';
		
		UPDATE 
			ER_REPORT 
				SET 
					rep_name='Adverse Event Summary By Arm (Per Study)', 
					rep_desc='Adverse Event Summary By Arm (Per Study)',
					rep_sql_clob=update_sql,
					rep_columns='Adverse Event, Grade 0, Grade 1, Grade 2, Grade 3, Grade 4, Grade 5, By Arm',
					rep_filterby='Date (AE Start Date) </br> Study (Study Number) </br> Organization (Enrolling Site) ',
					rep_type='rep_ae'
					
				WHERE 
					pk_report=115;
		
		END IF;

	
	
	
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=81;
		
		IF (row_check > 0) then
		
		update_sql:='select std.study_number study_number, std.study_title study_title, to_char(std.study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) study_actualdt,
st.site_name site_name,st.pk_site,
nvl((Select count(PK_ADVEVE) from ESCH.EV_ADV_TYPE b
     Where st.pk_site = b.pk_site and b.fk_study = std.pk_study and b.adv_type =''al_adve'' and
      AE_STDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)),0) ADV_COUNT,
nvl((Select count(PK_ADVEVE) from ESCH.EV_ADV_TYPE b
     Where st.pk_site = b.pk_site and b.fk_study = std.pk_study and b.adv_type = ''al_sadve'' and
     AE_STDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)),0) SADV_COUNT,
nvl((select count(PK_ADVEVE)
    from ESCH.EV_ADV_SITE_INFO b
    where st.pk_site = b.pk_site and b.fk_study = std.pk_study and
    b.ADVINFO_TYPE   = ''O'' and
    b.CODELST_SUBTYP = ''al_death'' and
    b.ADVINFO_VALUE = 1 and
    b.AE_STDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
    ),0) DEATH_COUNT,
nvl((Select count(PK_ADVEVE)
    from ESCH.EV_ADV_SITE_INFO b
    where st.pk_site = b.pk_site and b.fk_study = std.pk_study and
    CODELST_SUBTYP = ''adv_unexp''and
    ADVINFO_TYPE   = ''A'' and
    ADVINFO_VALUE = 1 and
    b.AE_STDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)),0) UNEXP_COUNT,
nvl((Select count(PK_ADVEVE)
    from ESCH.EV_ADV_SITE_INFO b
    where st.pk_site = b.pk_site and b.fk_study = std.pk_study and
    CODELST_SUBTYP = ''adv_violation''and
    ADVINFO_TYPE   = ''A'' and
    ADVINFO_VALUE = 1 and
    b.AE_STDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)),0) VIO_COUNT,
nvl((Select count(PK_ADVEVE)
    from ESCH.EV_ADV_SITE_INFO b
    where st.pk_site = b.pk_site and b.fk_study = std.pk_study and
    CODELST_SUBTYP = ''adv_dopped''and
    ADVINFO_TYPE   = ''A'' and
    ADVINFO_VALUE = 1 and
    b.AE_STDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) ),0) DROP_COUNT,
nvl((Select count(PK_ADVEVE) from ESCH.EV_ADV_TYPE b
     Where st.pk_site = b.pk_site and b.fk_study = std.pk_study and
     b.REL_TYPE = ''ad_def'' and
     AE_STDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) ),0) R_DEF_COUNT,
nvl((Select count(PK_ADVEVE) from ESCH.EV_ADV_TYPE b
     Where st.pk_site = b.pk_site and b.fk_study = std.pk_study and
     b.REL_TYPE = ''ad_nr'' and
     AE_STDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) ),0) R_NR_COUNT,
nvl((Select count(PK_ADVEVE) from ESCH.EV_ADV_TYPE b
     Where st.pk_site = b.pk_site and b.fk_study = std.pk_study and
     b.REL_TYPE = ''ad_pos'' and
     AE_STDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) ),0)
R_POS_COUNT,
nvl((Select count(PK_ADVEVE) from ESCH.EV_ADV_TYPE b
     Where st.pk_site = b.pk_site and b.fk_study = std.pk_study and
     b.REL_TYPE = ''ad_prob'' and
     AE_STDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) ),0)
R_PROB_COUNT,
nvl((Select count(PK_ADVEVE) from ESCH.EV_ADV_TYPE b
     Where st.pk_site = b.pk_site and b.fk_study = std.pk_study and
     b.REL_TYPE = ''ad_unknown'' and
     AE_STDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)),0) R_UNK_COUNT,
nvl((Select count(PK_ADVEVE) from ESCH.EV_ADV_TYPE b
     Where st.pk_site = b.pk_site and b.fk_study = std.pk_study and
     b.REL_TYPE = ''ad_unlike'' and
     AE_STDATE between TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)and TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) ),0) R_UNL_COUNT
from er_site st, er_study std
where pk_study = :studyId and
pk_site in (Select distinct er_user.FK_SITEID
from er_studyteam team, er_user
Where team.fk_study = std.pk_study and
er_user.pk_user = team.fk_user and
nvl(study_team_usr_type,''D'') = ''D'' ) ';
		
		UPDATE 
			ER_REPORT 
				SET 
					rep_name='Adverse Event Tracking (Per Study)', 
					rep_sql_clob=update_sql,
					rep_columns='Study Number, Study Start Date, Title, Participating Organizations, Counts of Adverse Events, Serious Adverse Events, Deaths, Attribution and Other Attributes',
					rep_filterby='Date (AE Start Date) </br> Study (Study Number) ',
					rep_type='rep_ae'
					
				WHERE 
					pk_report=81;
		
		END IF;

	
	
	
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=289;
		
		IF (row_check = 0) then
			INSERT INTO er_report 
				(pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable)
				
			VALUES
			(289
			,'Adverse Event Overview (Per Study)'
			,'Adverse Event Overview'
			,0
			,'N'
			,'Study Number, Title, PI, Therapeutic Area, Disease Site, Current Study Status, Pie Chart by Grade, Bar Chart by Type, Count of AEs by Name'
			,'Date (AE Start Date) </br> Study (Study Number) </br> Organization (Enrolling Site)'
			,1
			,'rep_ae'
			,''
			,'date:studyId:orgId');
			
			update_sql:='select STUDY_NUMBER,
STUDY_TITLE,
STUDY_PI,
STUDY_TAREA,
STUDY_DISEASE_SITE,
(select distinct sstat_study_status from vda.vda_v_studystat where sstat_current_stat = ''Yes'' and vda.vda_v_studystat.fk_study = VDA.VDA_V_AE_ALL_BYSITE.fk_study) as currentstat,
SITE_NAME,
AE_TYPE,
AE_DESC,
TO_CHAR(AE_STDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) AE_STDATE,
TO_CHAR(AE_ENDDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) AE_ENDDATE,
AE_GRADE,
AE_NAME,
MEDDRA,
DICTIONARY,
FK_STUDY,
FK_SITE_ENROLLING
from VDA.VDA_V_AE_ALL_BYSITE
WHERE fk_site_enrolling IN (:orgId) 
AND TRUNC(AE_STDATE) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
AND fk_study IN (:studyId)';
			
			UPDATE 
			ER_REPORT 
				SET 
					rep_sql_clob=update_sql
					
				WHERE 
					pk_report=289;
		
		END IF;
	
	
	
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=290;
		
		IF (row_check = 0) then
			INSERT INTO er_report 
				(pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable)
				
			VALUES
			(290
			,'Patient Study Status (Most Recent Status)'
			,'Patient Study Status (Most Recent Status)'
			,0
			,'N'
			,'Study Number, Enrolling Site, Patient Study ID, Patient Study Status, Status Date, Reason, Notes, Current Status?'
			,'Date (Status Date) </br> Study (Study Number) </br> Organization (Enrolling Site)'
			,1
			,'rep_apr'
			,''
			,'date:studyId:orgId');
			
			update_sql:='select  
(select study_number from vda.vda_v_study_summary where VDA.vda_v_pat_status_recent.fk_study = vda.vda_v_study_summary.pk_study) as study_number,
pstat_enroll_site,
pstat_pat_stud_id,
pstat_status,
TO_CHAR(pstat_stat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) pstat_stat_date,
pstat_reason,
pstat_notes,
DECODE (pstat_current_stat,1,''Yes'','''') AS pstat_current_stat,
fk_study,
fk_site_enrolling,
fk_per

from vda.vda_v_pat_status_recent
WHERE TRUNC(pstat_stat_date) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
AND fk_study IN (:studyId)
 and fk_site_enrolling IN (:orgId)';
			
			UPDATE 
			ER_REPORT 
				SET 
					rep_sql_clob=update_sql
					
				WHERE 
					pk_report=290;
		
		END IF;
	
	
	
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=291;
		
		IF (row_check = 0) then
			INSERT INTO er_report 
				(pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable)
				
			VALUES
			(291
			,'Patient Study Status (Current Status Flag)'
			,'Patient Study Status (Current Status Flag)'
			,0
			,'N'
			,'Study Number, Enrolling Site, Patient Study ID, Patient Study Status, Status Date, Reason'
			,'Date (Status Date) </br> Study (Study Number) </br> Organization (Enrolling Site)'
			,1
			,'rep_apr'
			,''
			,'date:studyId:orgId');
			
			update_sql:='select  
(select study_number from vda.vda_v_study_summary where vda.vda_v_all_patstudystat.fk_study = vda.vda_v_study_summary.pk_study) as study_number,
pstat_enroll_site,
pstat_pat_stud_id,
pstat_status,
TO_CHAR(pstat_stat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) pstat_stat_date,
pstat_reason,
pstat_notes,
DECODE (pstat_current_stat,1,''Yes'','''') AS pstat_current_stat,
fk_study,
fk_site_enrolling,
fk_per
from vda.vda_v_all_patstudystat
where pstat_current_stat =''1''
AND TRUNC(pstat_stat_date) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
AND fk_study IN (:studyId)
 and fk_site_enrolling IN (:orgId)';
			
			UPDATE 
			ER_REPORT 
				SET 
					rep_sql_clob=update_sql
					
				WHERE 
					pk_report=291;
		
		END IF;
	
	
	
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=292;
		
		IF (row_check = 0) then
			INSERT INTO er_report 
				(pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable)
				
			VALUES
			(292
			,'Patient Study Status (All Statuses)'
			,'Patient Study Status (All Statuses)'
			,0
			,'N'
			,'Study Number, Enrolling Site, Patient Study ID, Patient Study Status, Status Date, Reason'
			,'Date (Status Date) </br> Study (Study Number) </br> Organization (Enrolling Site)'
			,1
			,'rep_apr'
			,''
			,'date:studyId:orgId');
			
			update_sql:='select  
(select study_number from vda.vda_v_study_summary where vda.vda_v_all_patstudystat.fk_study = vda.vda_v_study_summary.pk_study) as study_number,
pstat_enroll_site,
pstat_pat_stud_id,
pstat_status,
TO_CHAR(pstat_stat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) pstat_stat_date,
pstat_reason,
pstat_notes,
DECODE (pstat_current_stat,1,''Yes'','''') AS pstat_current_stat,
fk_study,
fk_site_enrolling,
fk_per
from vda.vda_v_all_patstudystat
where TRUNC(pstat_stat_date) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
AND fk_study IN (:studyId)
  and fk_site_enrolling IN (:orgId)';
			
			UPDATE 
			ER_REPORT 
				SET 
					rep_sql_clob=update_sql
					
				WHERE 
					pk_report=292;
		
		END IF;
	
	
	SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=295;
		
		IF (row_check = 0) then
			INSERT INTO er_report 
				(pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable)
				
			VALUES
			(295
			,'Study Patients Status Grid'
			,'Study Patients Status Grid'
			,0
			,'N'
			,'Study Number, Enrolling Site, Patient Study ID, Current Status, Screening Date, Informed Consent Date, Enrolled Date, Active/ On Treatment Date, Off Treatment Date, Off Study Date'
			,'Study (Study Number) </br> Organization (Enrolling Site)'
			,1
			,'rep_apr'
			,''
			,'studyId:orgId');
		
			update_sql:='SELECT 
distinct
FK_STUDY,
(SELECT STUDY_NUMBER FROM ER_STUDY where pk_study = ER_PATPROT.fk_study) as STUDYNUMBER,
FK_PER,
FK_SITE_ENROLLING,
(SELECT SITE_NAME FROM ER_SITE where pk_site = ER_PATPROT.fk_site_enrolling) as ENROLLINGSITE,
PATPROT_PATSTDID,
(SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = ER_PATPROT.fk_per AND fk_study = ER_PATPROT.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''screening'') AND ROWNUM = 1) AS screen_date,
(SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = ER_PATPROT.fk_per AND fk_study = ER_PATPROT.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''infConsent'') AND ROWNUM = 1) AS consent_date,
(SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = ER_PATPROT.fk_per AND fk_study = ER_PATPROT.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''enrolled'')) AS enrolled_date,
(SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = ER_PATPROT.fk_per AND fk_study = ER_PATPROT.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''active'') AND ROWNUM = 1) AS active_date,
(SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = ER_PATPROT.fk_per AND fk_study = ER_PATPROT.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''offtreat'') AND ROWNUM = 1) AS offtreat_date,
(SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) FROM ER_PATSTUDYSTAT WHERE fk_per = ER_PATPROT.fk_per AND fk_study = ER_PATPROT.fk_study AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''offstudy'') AND ROWNUM = 1) AS offstudy_date,
(select distinct pstat_status from vda.vda_v_all_patstudystat where fk_per = ER_PATPROT.fk_per AND fk_study = ER_PATPROT.fk_study AND pstat_current_stat = ''1'') as current_status
FROM
 ER_PATPROT
 where 
fk_study IN (:studyId) and FK_SITE_ENROLLING IN (:orgId)';
		
		UPDATE 
			ER_REPORT 
				SET
					rep_sql_clob=update_sql
					
				WHERE 
					pk_report=295;
		
		END IF;
	
	
	
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=293;
		
		IF (row_check = 0) then
			INSERT INTO er_report 
				(pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable)
				
			VALUES
			(293
			,'Study Patients Enrollment Details'
			,'Study Patients Enrollment Details'
			,0
			,'N'
			,'Study Number, Enrolling Site, Patient Study ID, Enrolled By, Enrolled On, Assigned To, Physician, Treating Location, Treating Organization, Disease Code, Other Disease Code 1, Other Disease Code 2'
			,'Date (Enrollment Date) </br> Study (Study Number) </br> Organization (Enrolling Site)'
			,1
			,'rep_apr'
			,''
			,'date:studyId:orgId');
			
			update_sql:='select
FK_STUDY,
FK_PER,
FK_SITE_ENROLLING,
STUDY_NUMBER,
PSTAT_ENROLL_SITE,
PSTAT_PAT_STUD_ID,
PSTAT_ENROLLED_BY,
TO_CHAR(PATPROT_ENROLDT,PKG_DATEUTIL.F_GET_DATEFORMAT) PATPROT_ENROLDT,
PSTAT_ASSIGNED_TO,
PSTAT_PHYSICIAN,
PSTAT_TREAT_LOCAT,
PATPROT_TREATINGORG,
PATPROT_DISEASE_CODE,
PATPROT_MORE_DIS_CODE1,
PATPROT_MORE_DIS_CODE2
from VDA.vda_v_pat_accrual
where fk_study IN (:studyId) and FK_SITE_ENROLLING IN (:orgId) AND TRUNC(PATPROT_ENROLDT) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)';
			
			UPDATE 
			ER_REPORT 
				SET 
					rep_sql_clob=update_sql
					
				WHERE 
					pk_report=293;
		
		END IF;

	
	
	
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=294;
		
		IF (row_check = 0) then
			INSERT INTO er_report 
				(pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable)
				
			VALUES
			(294
			,'Study Patients Demographics'
			,'Study Patients Demographics'
			,0
			,'N'
			,'Study Number, Enrolling Site, Patient Study ID, Gender, Marital Status, State, Country, Primary Ethnicity, Primary Race, Survival Status'
			,'Date (Enrollment Date) </br> Study (Study Number) </br> Organization (Enrolling Site)'
			,1
			,'rep_apr'
			,''
			,'date:studyId:orgId');
			
			update_sql:='select
FK_STUDY,
FK_PER,
FK_SITE_ENROLLING,
STUDY_NUMBER,
TO_CHAR(PATPROT_ENROLDT,PKG_DATEUTIL.F_GET_DATEFORMAT) PATPROT_ENROLDT,
PSTAT_ENROLL_SITE,
PSTAT_PAT_STUD_ID,
(select PTDEM_GENDER from VDA.VDA_V_PAT_ACCRUAL_ALL where fk_per = VDA.vda_v_pat_accrual.FK_PER and FK_STUDY = VDA.vda_v_pat_accrual.FK_STUDY) as GENDER,
(select PTDEM_MARSTAT from VDA.VDA_V_PAT_ACCRUAL_ALL where fk_per = VDA.vda_v_pat_accrual.FK_PER and FK_STUDY = VDA.vda_v_pat_accrual.FK_STUDY) as MARITAL,
(select PTDEM_EMPLOY from VDA.VDA_V_PAT_ACCRUAL_ALL where fk_per = VDA.vda_v_pat_accrual.FK_PER and FK_STUDY = VDA.vda_v_pat_accrual.FK_STUDY) as EMPLOYMENT,
(select PTDEM_EDUCATION from VDA.VDA_V_PAT_ACCRUAL_ALL where fk_per = VDA.vda_v_pat_accrual.FK_PER and FK_STUDY = VDA.vda_v_pat_accrual.FK_STUDY) as EDUCATION,
(select PTDEM_PRI_ETHNY from VDA.VDA_V_PAT_ACCRUAL_ALL where fk_per = VDA.vda_v_pat_accrual.FK_PER and FK_STUDY = VDA.vda_v_pat_accrual.FK_STUDY) as PRIETHNICITY,
(select PTDEM_PRI_RACE from VDA.VDA_V_PAT_ACCRUAL_ALL where fk_per = VDA.vda_v_pat_accrual.FK_PER and FK_STUDY = VDA.vda_v_pat_accrual.FK_STUDY) as PRIRACE,
(select PTDEM_STATE from VDA.VDA_V_PAT_ACCRUAL_ALL where fk_per = VDA.vda_v_pat_accrual.FK_PER and FK_STUDY = VDA.vda_v_pat_accrual.FK_STUDY) as STATE,
(select PTDEM_COUNTRY from VDA.VDA_V_PAT_ACCRUAL_ALL where fk_per = VDA.vda_v_pat_accrual.FK_PER and FK_STUDY = VDA.vda_v_pat_accrual.FK_STUDY) as COUNTRY,
(select PTDEM_SURVIVSTAT from VDA.VDA_V_PAT_ACCRUAL_ALL where fk_per = VDA.vda_v_pat_accrual.FK_PER and FK_STUDY = VDA.vda_v_pat_accrual.FK_STUDY) as SURVIVAL
from VDA.vda_v_pat_accrual
where fk_study IN (:studyId) and FK_SITE_ENROLLING IN (:orgId) AND TRUNC(PATPROT_ENROLDT) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)';
			
			UPDATE 
			ER_REPORT 
				SET 
					rep_sql_clob=update_sql
					
				WHERE 
					pk_report=294;
		
		END IF;
		
	
	
	
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=93;
		
		IF (row_check > 0) then
		
		update_sql:='SELECT TO_CHAR(PATFORMS_FILLDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) AS PATFORMS_FILLDATE,trim(TO_CHAR(PATFORMS_FILLDATE,''MONTH'')) || '' '' || TO_CHAR(PATFORMS_FILLDATE,''yyyy'') AS PATFORMS_FILLDATE_MONTH, form_name, description, study_number, study_title, per_code, patprot_patstdid, TO_CHAR(patprot_enroldt,PKG_DATEUTIL.F_GET_DATEFORMAT) AS patprot_enroldt FROM (
SELECT PATFORMS_FILLDATE,
(SELECT form_name FROM ER_FORMLIB WHERE pK_FORMLIB = a.FK_FORMLIB) form_name,
F_Getformbrowsedata(fk_formlib, pk_patforms) AS Description,
a.FK_PER,
FK_PATPROT, study_number, study_title, per_code, patprot_patstdid, patprot_enroldt
FROM ER_PATFORMS a, ER_PATPROT, ER_STUDY, ER_PER
WHERE pk_patprot = fk_patprot AND
fk_study in (:studyId) AND
a.fk_per in (:patientId)  AND
PATFORMS_FILLDATE BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND
pk_study = fk_study AND
a.fk_per = pk_per AND
NVL(a.record_type,''N'') <> ''D''
UNION
SELECT test_date,''Lab'', (SELECT labtest_name FROM ER_LABTEST WHERE pk_labtest = fk_testid) || ''  :  '' ||  test_result || '' '' || test_unit ,
a.fk_per, fk_patprot,study_number, study_title, per_code, NULL AS patprot_patstdid, NULL AS patprot_enroldt
FROM ER_PATLABS a, ER_STUDY, ER_PER
WHERE a.fk_per in (:patientId) AND
a.fk_study in (:studyId)
AND test_date BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
AND pk_study = a.fk_study AND
a.fk_per = pk_per
UNION
SELECT ae_stdate, ''Adverse Event'', ''Adverse Event : '' || AE_NAME || DECODE(fk_codlst_aetype,(SELECT pk_codelst FROM ESCH.sch_codelst WHERE codelst_type = ''adve_type'' AND codelst_subtyp=''al_sadve''),''X@*@X'','''') || ''X@@@XGrade : '' || AE_GRADE || ''X@@@XStop DATE : '' ||  TO_CHAR(AE_ENDDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) || ''X@@@XRelationship TO THE study : '' || (SELECT codelst_desc FROM ESCH.sch_codelst WHERE pk_codelst=ae_relationship) || ''X@@@XOutcome TYPE : '' || DECODE(SUBSTR(AE_OUTTYPE,1,1),''1'',''[ Death '' || to_char(ae_outdate,PKG_DATEUTIL.F_GET_DATEFORMAT) || '' ] '') || DECODE(SUBSTR(AE_OUTTYPE,2,1),''1'',''[ Life-threatening ] '') || DECODE(SUBSTR(AE_OUTTYPE,3,1),''1'',''[ Hospitalization ] '')  || DECODE(SUBSTR(AE_OUTTYPE,4,1),''1'',''[ Disability ] '') || DECODE(SUBSTR(AE_OUTTYPE,5,1),''1'',''[ Congenital Anomaly ] '') || DECODE(SUBSTR(AE_OUTTYPE,6,1),''1'',''[ Required Intervention ] '') || DECODE(SUBSTR(AE_OUTTYPE,7,1),''1'',''[ Recovered ]''),
pk_per, pk_patprot, study_number, study_title, per_code, patprot_patstdid, patprot_enroldt
FROM ESCH.sch_adverseve a, ER_PER b,ER_STUDY c, ER_PATPROT d
WHERE a.fk_study = c.pk_study AND
a.fk_per = b.pk_per AND
d.patprot_stat = 1 AND
d.PATPROT_ENROLDT IS NOT NULL AND
a.fk_study = d.fk_study AND
a.fk_per = d.fk_per AND
a.fk_per in (:patientId) AND
a.fk_study in (:studyId)  AND
ae_stdate BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
ORDER BY 1)';
		
		UPDATE 
			ER_REPORT 
				SET 
					rep_name='Patient Timeline (Per Study Patient)', 
					rep_desc='Patient Timeline (Per Study Patient)',
					rep_sql_clob=update_sql,
					rep_columns='Patient ID, Patient Study ID, Study Number, Study Title, Study Enrollment Date, Data Entry Date, Event (Page/Form), Description',
					rep_filterby='Date (Data Entry Date) </br> Study (Study Number) </br> Patient (Patient ID)',
					rep_type='rep_psr'
					
				WHERE 
					pk_report=93;
		
		END IF;
		
	
	
	
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=95;
		
		IF (row_check > 0) then
		
		update_sql:='SELECT (SELECT study_number FROM ER_STUDY WHERE pk_study in (:studyId)) AS study_number, year1,DECODE(month1,''01'',''January'',''02'',''February'',''03'',''March'',''04'',''April'',''05'',''May'',''06'',''June'',''07'',''July'',''08'',''August'',''09'',''September'',''10'',''October'',''11'',''November'',''December'') AS month1,
       F_Get_Patevesch(sunday,'':patientId'',:studyId) AS sunday,
       F_Get_Patevesch(monday,'':patientId'',:studyId) AS monday,
       F_Get_Patevesch(tuesday,'':patientId'',:studyId) AS tuesday,
       F_Get_Patevesch(wednesday,'':patientId'',:studyId) AS wednesday,
       F_Get_Patevesch(thursday,'':patientId'',:studyId) AS thursday,
       F_Get_Patevesch(friday,'':patientId'',:studyId) AS friday,
       F_Get_Patevesch(saturday,'':patientId'',:studyId) AS saturday FROM (
SELECT year1,month1,week1,MAX(sunday) AS SUNDAY, MAX(monday) AS MONDAY, MAX(tuesday) AS TUESDAY, MAX(wednesday) AS WEDNESDAY, MAX(thursday) AS THURSDAY, MAX(friday) AS FRIDAY, MAX(saturday) AS SATURDAY FROM (
SELECT year1,month1,week1,date1, TO_CHAR(date1,''d''),DECODE(TO_CHAR(date1,''d''),1,date1,'''') AS Sunday,
                              DECODE(TO_CHAR(date1,''d''),2,date1,NULL) AS Monday,
                              DECODE(TO_CHAR(date1,''d''),3,date1,NULL) AS Tuesday,
                              DECODE(TO_CHAR(date1,''d''),4,date1,NULL) AS Wednesday,
                              DECODE(TO_CHAR(date1,''d''),5,date1,NULL) AS Thursday,
                              DECODE(TO_CHAR(date1,''d''),6,date1,NULL) AS Friday,
                              DECODE(TO_CHAR(date1,''d''),7,date1,NULL) AS Saturday
                           FROM (
   SELECT TO_CHAR(dt,''yyyy'') AS year1, TO_CHAR(dt,''MM'') AS month1, TO_CHAR(dt+1,''IW'') AS week1 , dt AS date1
   FROM
           (SELECT TO_DATE(TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)) + ROWNUM - 1 AS dt
         FROM ALL_OBJECTS
         WHERE ROWNUM <= TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) - TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) + 1)
         ) GROUP BY year1,week1,month1, date1 order by year1,week1,month1, date1
) GROUP BY year1,month1,week1 ORDER BY year1,month1,week1
)';
		
		UPDATE 
			ER_REPORT 
				SET 
					rep_name='Patient Visit Calendar (Per Study Patient)', 
					rep_desc='Patient Visit Calendar (Per Study Patient)',
					rep_sql_clob=update_sql,
					rep_columns='Study Number, Calendar by Month, Patient Study ID, Visit(s), Event(s)',
					rep_filterby='Date (Visit Date) </br> Study (Study Number) </br> Patient (Patient ID)',
					rep_type='rep_psr'
					
				WHERE 
					pk_report=95;
		
		END IF;
		
	END IF;

	COMMIT;

END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,382,8,'08_er_report_patch_patient.sql',sysdate,'v10.1 #783');

commit;
