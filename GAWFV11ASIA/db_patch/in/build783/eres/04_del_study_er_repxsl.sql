declare
begin
	delete  from  er_repxsl where pk_repxsl in (1,5,94,97,98,116,5010);
commit;
end;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,382,4,'04_del_study_er_repxsl.sql',sysdate,'v10.1 #783');

commit;
