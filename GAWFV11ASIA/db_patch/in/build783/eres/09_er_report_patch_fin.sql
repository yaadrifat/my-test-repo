DECLARE 
	table_check number;
	row_check number;
	update_sql clob;
BEGIN
	SELECT count(*)
	INTO table_check
	FROM user_tab_cols
	WHERE TABLE_NAME = 'ER_REPORT';
	
	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=256;

		IF (row_check = 0) then
			INSERT INTO er_report (
				pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable
				)
			VALUES(
				 256
				,'Milestone Achievement Log (All)'
				,'Milestone Achievement Log (All)'
				,0
				,'N'
				,'Date Milestone Achieved, Study, Milestone Description, Payment Type, Payment For, Enrolling Site, Patient Study ID, Milestone Amount, Amount Invoiced, Invoice Number, Amount Reconciled, Payment Date'
				,'Date (Achievement Date) </br> Study (Study Number)'
				,1
				,'ms_rep'
				,''
				,'date:studyId'
				);
				
			update_sql:='select  
study_number,
milestone_type,
msach_milestone_desc,
fk_site_enrolling,
enrolling_site,
msach_ptstudy_id,
TO_CHAR(msach_ach_date,PKG_DATEUTIL.F_GET_DATEFORMAT) msach_ach_date,
milestone_amt,
payment_type,
payment_for,
fk_study,
fk_milestone,
inv_amt,
rec_amt,
payment_dt
from VDA.vda_v_milestone_achvd_det
WHERE fk_study IN (:studyId)
AND TRUNC(MSACH_ACH_DATE) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=256;					
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=257;

		IF (row_check = 0) then
			INSERT INTO er_report (
				pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable
				)
			VALUES(
				 257
				,'Account Receivable (by Milestone)'
				,'Account Receivable (by Milestone)'
				,0
				,'N'
				,'Date Milestone Achievement, Study Number, Milestone Description, Enrolling Site, Patient Study ID,  Milestone Amount, Holdback, Invoiced, Invoice Number, Not Invoiced, Payment Date, Reconciled, Outstanding'
				,'Date (Achievement Date) </br> Study (Study Number)'
				,1
				,'ms_rep'
				,''
				,'date:studyId'
				);
				
				update_sql:='select  
study_number,
milestone_type,
msach_milestone_desc,
fk_site_enrolling,
enrolling_site,
msach_ptstudy_id,
TO_CHAR(msach_ach_date,PKG_DATEUTIL.F_GET_DATEFORMAT) msach_ach_date,
milestone_amt,
milestone_holdback,
payment_type,
payment_for,
fk_study,
fk_milestone,
inv_amt,
inv_num,
rec_amt,
payment_dt
from VDA.vda_v_milestone_achvd_det
where payment_type != ''Payable''
AND fk_study IN (:studyId)
AND TRUNC(MSACH_ACH_DATE) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=257;
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=258;

		IF (row_check = 0) then
			INSERT INTO er_report (
				pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable
				)
			VALUES(
				 258
				,'Account Payable (by Milestone)'
				,'Account Payable (by Milestone)'
				,0
				,'N'
				,'Date Milestone Achievement, Study Number, Payment For, Milestone Description, Enrolling Site, Patient Study ID,  Milestone Amount, Holdback, Invoiced, Invoice Number, Not Invoiced, Payment Date, Reconciled, Outstanding'
				,'Date (Achievement Date) </br> Study (Study Number)'
				,1
				,'ms_rep'
				,''
				,'date:studyId'
				);
				
				update_sql:='select  
study_number,
milestone_type,
msach_milestone_desc,
fk_site_enrolling,
enrolling_site,
msach_ptstudy_id,
TO_CHAR(msach_ach_date,PKG_DATEUTIL.F_GET_DATEFORMAT) msach_ach_date,
milestone_amt,
milestone_holdback,
payment_type,
payment_for,
fk_study,
fk_milestone,
inv_amt,
inv_num,
rec_amt,
payment_dt
from VDA.vda_v_milestone_achvd_det
where payment_type = ''Payable''
AND fk_study IN (:studyId)
AND TRUNC(MSACH_ACH_DATE) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=258;
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=259;

		IF (row_check = 0) then
			INSERT INTO er_report (
				pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable
				)
			VALUES(
				 259
				,'Payments Made with Reconciliation'
				,'Payments Made (with Reconciliation)'
				,0
				,'N'
				,'Payment Date, Description, Study, Amount Paid, Amount Reconciled (Total), Amount Reconciled (Invoice), Reconciled To (Invoice), Amount Reconciled (Milestone), Reconciled To (Non-Invoiced Milestone), Reconciled To (Patient), Comments'
				,'Date (Payment Date) </br> Study (Study Number) '
				,1
				,'ms_rep'
				,''
				,'date:studyId'
				);
				
				update_sql:='select  
study_number,
pk_milepayment,
TO_CHAR(mspay_date,PKG_DATEUTIL.F_GET_DATEFORMAT) mspay_date,
mspay_amt_recvd,
rec_inv,
rec_invnum,
rec_mile,
rec_milenum,
rec_pt,
rec_ptnum,
mspay_desc,
mspay_comments,
fk_study
from vda.vda_v_payments
where mspay_type =''Payment Made''
AND TRUNC(mspay_date) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
AND fk_study IN (:studyId)';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=259;	
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=260;

		IF (row_check = 0) then
			INSERT INTO er_report (
				pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable
				)
			VALUES(
				 260
				,'Payments Received with Reconciliation'
				,'Payments Received (with Reconciliation)'
				,0
				,'N'
				,'Payment Date, Description, Study, Amount Received, Amount Reconciled (Total), Amount Reconciled (Invoice), Reconciled To (Invoice), Amount Reconciled (Milestone), Reconciled To (Non-Invoiced Milestone), Reconciled To (Patient), Comments'
				,'Date (Payment Date) </br> Study (Study Number) '
				,1
				,'ms_rep'
				,''
				,'date:studyId'
				);
				
				update_sql:='select  
study_number,
pk_milepayment,
TO_CHAR(mspay_date,PKG_DATEUTIL.F_GET_DATEFORMAT) mspay_date,
mspay_amt_recvd,
rec_inv,
rec_invnum,
rec_mile,
rec_milenum,
rec_pt,
rec_ptnum,
mspay_desc,
mspay_comments,
fk_study
from vda.vda_v_payments
where mspay_type =''Payment Received''
AND TRUNC(mspay_date) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
AND fk_study IN (:studyId)';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=260;
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=261;

		IF (row_check = 0) then
			INSERT INTO er_report (
				pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable
				)
			VALUES(
				 261
				,'Invoices - Aging Report'
				,'Invoices - Aging Report'
				,0
				,'N'
				,'Invoice Date, Due Date, Study, Invoice Number, Invoice Amount, Payment Reconciled, Amount Overdue, 0-30, 31-60, 61-90,Over 90'
				,'Date (Invoice Date)'
				,1
				,'ms_rep'
				,''
				,'date:studyId'
				);
				
				update_sql:='select 
a.PK_INVOICE, 
(select FK_STUDY from ER_INVOICE where ER_INVOICE.PK_INVOICE = a.pk_invoice) as FK_STUDY,
STUDY_NUMBER, 
STUDY_SPONSOR, 
a.INV_NUMBER, 
TO_CHAR(INVOICE_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT) INVOICE_DATE, 
TO_CHAR(INVOICE_DUEDATE, PKG_DATEUTIL.F_GET_DATEFORMAT) INVOICE_DUEDATE,
INVOICE_AMOUNT, 
PAYMENT, 
AMOUNT_OVERDUE, 
REPLACE ("0_30D_OVERDUE", ''"'', '''') as OVERDUE30, 
REPLACE ("31_60D_OVERDUE", ''"'', '''') as OVERDUE60, 
REPLACE ("61_90D_OVERDUE", ''"'', '''') as OVERDUE90, 
REPLACE ("OVER_90D_OVERDUE", ''"'', '''') as OVER90D_OVERDUE 
from VDA.vda_v_inv_aged_receivables a, ER_INVOICE b
where  a.PK_INVOICE = b.pk_invoice
and TRUNC(INVOICE_DATE) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
AND fk_study IN (:studyId)';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=261;	
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=262;

		IF (row_check = 0) then
			INSERT INTO er_report (
				pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable
				)
			VALUES(
				 262
				,'Payment Records (All)'
				,'Payment Records (All)'
				,0
				,'N'
				,'Payment Date, Payment Type, Study, Amount Paid, Description, Comments'
				,'Date (Payment Date) </br> Study (Study Number) '
				,1
				,'ms_rep'
				,''
				,'date:studyId'
				);
				
				update_sql:='select  
study_number,
TO_CHAR(mspay_date,PKG_DATEUTIL.F_GET_DATEFORMAT) mspay_date,
mspay_type,
mspay_amt_recvd,
mspay_desc,
mspay_comments,
fk_study
from vda.vda_v_payments
where TRUNC(mspay_date) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
AND fk_study IN (:studyId)';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=262;		
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=263;

		IF (row_check = 0) then
			INSERT INTO er_report (
				pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable
				)
			VALUES(
				 263
				,'Account Receivable (by Study)'
				,'Account Receivable (by Study)'
				,0
				,'N'
				,'Study, Division, PI, Coordinator, Milestone Amount, Holdback, Amount Invoiced, Amount Not Invoiced, Amount Reconciled, Outstanding'
				,'Study (Study Number)'
				,1
				,'ms_rep'
				,''
				,'studyId'
				);
				
				update_sql:='select  distinct
b.study_number,
study_division,
study_pi,
study_coordinator,
nvl((select sum(msrul_amount) from vda.vda_v_milestones where vda.vda_v_milestones.fk_study = a.fk_study),''0'') as milestone_amt,
nvl((select sum(milestone_holdback) from vda.vda_v_milestones where vda.vda_v_milestones.fk_study = a.fk_study),''0'') as holdback_amt,
fk_study,
nvl((select sum(amount_invoiced) from er_invoice_detail where er_invoice_detail.fk_study = a.fk_study),''0'') as inv_amt,
nvl((select sum(mp_amount) from er_milepayment_details where fk_milepayment in (select pk_milepayment from er_milepayment where er_milepayment.fk_study = a.fk_study )),''0'') as rec_amt
from VDA.vda_v_milestone_achvd_det a, VDA.vda_v_study_summary b
where (select msrul_pay_type from vda.vda_v_milestones where vda.vda_v_milestones.pk_milestone = a.fk_milestone) != ''Payable''
and fk_study = pk_study
AND fk_study IN (:studyId)
AND TRUNC(MSACH_ACH_DATE) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=263;		
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=264;

		IF (row_check = 0) then
			INSERT INTO er_report (
				pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable
				)
			VALUES(
				 264
				,'Invoice Records (All)'
				,'Invoice Records (All)'
				,0
				,'N'
				,'Invoice Date, Invoice Number, Internal Account Number, Study, Invoice Amount, Amount Paid, Remaining Balance'
				,'Date (Invoice Date) </br> Study (Study Number)'
				,1
				,'ms_rep'
				,''
				,'date:studyId'
				);
				
				update_sql:='select
pk_invoice,
fk_study,
(select study_number from VDA.vda_v_invoice_payments where VDA.vda_v_invoice_payments.pk_invoice = er_invoice.pk_invoice) as study_number,
inv_number,
int_acc_num,
TO_CHAR(inv_date,PKG_DATEUTIL.F_GET_DATEFORMAT) as inv_date,
(select invoice_amount from VDA.vda_v_invoice_payments where VDA.vda_v_invoice_payments.pk_invoice = er_invoice.pk_invoice) as invoice_amount,
(select payments from VDA.vda_v_invoice_payments where VDA.vda_v_invoice_payments.pk_invoice = er_invoice.pk_invoice) as payments,
(select invoice_amount_balance from VDA.vda_v_invoice_payments where VDA.vda_v_invoice_payments.pk_invoice = er_invoice.pk_invoice) as invoice_amount_balance
from er_invoice
 where (select study_number from VDA.vda_v_invoice_payments where VDA.vda_v_invoice_payments.pk_invoice = er_invoice.pk_invoice) is not null
AND fk_study IN (:studyId)
AND TRUNC(INV_DATE) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=264;	
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=265;

		IF (row_check = 0) then
			INSERT INTO er_report (
				pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable
				)
			VALUES(
				 265
				,'Additional Milestones'
				,'Additional Milestones'
				,0
				,'N'
				,'Date Milestone Achievement, Study Number, Milestone Description, Milestone Amount, Holdback, Invoiced, Invoice Number, Not Invoiced, Payment Date, Reconciled, Outstanding'
				,'Date (Achievement Date) </br> Study (Study Number)'
				,1
				,'ms_rep'
				,''
				,'date:studyId'
				);
				
				update_sql:='select
fk_study,
study_number,
msach_milestone_desc,
TO_CHAR(msach_ach_date,PKG_DATEUTIL.F_GET_DATEFORMAT) msach_ach_date,
milestone_amt,
milestone_holdback,
payment_type,
payment_for,
inv_amt,
inv_num,
rec_amt,
payment_dt
from VDA.vda_v_addnl_milestone_det
where fk_study IN (:studyId)
AND TRUNC(MSACH_ACH_DATE) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=265;		
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=266;

		IF (row_check = 0) then
			INSERT INTO er_report (
				pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable
				)
			VALUES(
				 266
				,'Budget Personnel Items'
				,'Budget Personnel Items'
				,0
				,'N'
				,'Study, Calendar, Visit, Line Item, Unit Cost, # Units, Cost Per Patient, Fringe, Indirect, Total Cost Per Patient, # Patients, Total Cost All Patients'
				,'Study (Study Number)'
				,1
				,'mp_rep'
				,''
				,'studyId'
				);
				
				update_sql:='select
a.study_number,
fk_study,
prot_calendar,
lineitem_name,
bgtsection_name,
unit_cost,
number_of_unit,
cost_per_patient,
per_patient_line_fringe,
perpat_indirect,
total_cost_per_pat,
bgtsection_patno,
total_cost_all_pat
from VDA.vda_v_budget_lineitem a, sch_budget b
 where a.pk_budget = b.pk_budget
and fk_study is not null and category_subtyp = ''ctgry_per'' 
AND fk_study IN (:studyId)';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=266;	
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=267;

		IF (row_check = 0) then
			INSERT INTO er_report (
				pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable
				)
			VALUES(
				 267
				,'Calendar Budget and Coverage (WIP - Per Study)'
				,'Calendar Budget and Coverage (WIP - Per Study)'
				,0
				,'N'
				,'Study, Title, Per Patient Charge, Calendar Budget, Event, Category, Coverage Type, Unit Cost, Units, D/M, Direct Cost Per Patient, I/D Applied, Total Cost Per Patient,Total Cost All Patients'
				,'Study (Study Number)'
				,1
				,'mp_rep'
				,''
				,'studyId'
				);
				
				update_sql:=' SELECT pk_budget,
  BUDGET_NAME,
  PROT_CALENDAR,
  BGTCAL_PROTID,
  BUDGET_VERSION,
  BUDGET_DESC,
  STUDY_NUMBER,
  STUDY_TITLE,
  SITE_NAME,
  budgetsection_sequence,
  category_seq,
  NVL(COVERAGE_TYPE,''Coverage Type NOT specified'') AS COVERAGE_TYPE,
  BGTSECTION_NAME,
  LINEITEM_NAME,
  lineitem_seq,
  CASE
    WHEN (FK_EVENT IS NOT NULL
    AND 1           = 0)
    THEN ''''
    ELSE CPT_CODE
  END                                           AS CPT_CODE,
  NVL(COST_PER_PATIENT,0)                       AS COST_PER_PATIENT,
  NVL(TOTAL_COST,0)                             AS TOTAL_COST,
  DECODE(LINE_ITEM_INDIRECTS_FLAG,1,''Yes'','''')   AS LINE_ITEM_INDIRECTS_FLAG,
  DECODE(COST_DISCOUNT_ON_LINE_ITEM,1,''Yes'','''') AS COST_DISCOUNT_ON_LINE_ITEM,
  INDIRECTS,
  BUDGET_INDIRECT_FLAG,
  FRINGE_BENEFIT,
  FRINGE_FLAG,
  PER_PATIENT_LINE_FRINGE,
  TOTAL_LINE_FRINGE,
  BUDGET_DISCOUNT,
  DECODE(BUDGET_DISCOUNT_FLAG,1,''Discount'',2,''Markup'','''') AS BUDGET_DISCOUNT_FLAG,
  TOTAL_COST_AFTER,
  TOTAL_PAT_COST_AFTER,
  PER_PAT_LINE_ITEM_DISCOUNT,
  TOTAL_COST_DISCOUNT,
  PERPAT_INDIRECT,
  TOTAL_COST_INDIRECT,
  BUDGET_CURRENCY,
  lower(LINEITEM_NAME)                                           AS l_LINEITEM_NAME,
  DECODE(COST_CUSTOMCOL,''research'',''No'',''soc'',''Yes'',''Other'') AS standard_of_care,
  LINEITEM_SPONSORAMOUNT                                         AS SPONSOR_AMOUNT,
  LINEITEM_VARIANCE                                              AS L_VARIANCE,
  LINEITEM_DIRECT_PERPAT,
  TOTAL_COST_PER_PAT,
  TOTAL_COST_ALL_PAT,
  BGTCAL_PROTTYPE,
  CATEGORY,
  UNIT_COST,
  NUMBER_OF_UNIT,
  CASE
    WHEN BGTSECTION_PATNO IS NULL
    THEN ''1''
    ELSE BGTSECTION_PATNO
  END AS BGTSECTION_PATNO,
  LAST_MODIFIED_BY,
  LAST_MODIFIED_DATE,
  category_subtyp,
  FK_CODELST_COST_TYPE,
  pk_budgetsec,
  NVL(budgetsec_fkvisit,0)budgetsec_fkvisit,
  pk_lineitem,
  lineitem_inpersec,
  NVL(bgtcal_excldsocflag,0) AS bgtcal_excldsocflag,
  BGTSECTION_TYPE,
  NVL(FK_EVENT, 0) AS FK_EVENT,
  SUBCOST_ITEM_FLAG
FROM ERV_BUDGET
WHERE fk_study  IN (:studyId)
AND BGTCal_PROTID IS NOT NULL
AND (SELECT codelst_subtyp
  FROM event_assoc,
    sch_codelst
  WHERE event_id             = BGTCal_PROTID
  AND pk_codelst             = fk_codelst_calstat)=''W''
AND NVL(BGTCAL_PROTTYPE,''L'')<>''L''
AND BUDGETSEC_FKVISIT       >0

UNION ALL

SELECT pk_budget,
   BUDGET_NAME,
  (SELECT name FROM event_assoc WHERE event_id= b.bgtcal_protid
  ) AS PROT_CALENDAR,
  b.BGTCAL_PROTID,
   BUDGET_VERSION,
  '''' BUDGET_DESC,
  '''' STUDY_NUMBER,
  '''' STUDY_TITLE,
  '''' SITE_NAME,
NULL AS budgetsection_sequence,
  0 category_seq,
  '''' COVERAGE_TYPE,
  '''' BGTSECTION_NAME,
  '''' LINEITEM_NAME,
NULL AS lineitem_seq,
  '''' CPT_CODE,
  0 COST_PER_PATIENT,
  0 TOTAL_COST,
  '''' LINE_ITEM_INDIRECTS_FLAG,
  '''' COST_DISCOUNT_ON_LINE_ITEM,
  0 INDIRECTS,
  '''' BUDGET_INDIRECT_FLAG,
  0 FRINGE_BENEFIT,
  0 FRINGE_FLAG,
  0 PER_PATIENT_LINE_FRINGE,
  0 TOTAL_LINE_FRINGE,
  0 BUDGET_DISCOUNT,
  '''' BUDGET_DISCOUNT_FLAG,
  0 TOTAL_COST_AFTER,
  0 TOTAL_PAT_COST_AFTER,
  0 PER_PAT_LINE_ITEM_DISCOUNT,
  0 TOTAL_COST_DISCOUNT,
  0 PERPAT_INDIRECT,
  0 TOTAL_COST_INDIRECT,
  '''' BUDGET_CURRENCY,
  NULL AS l_LINEITEM_NAME,
  ''No'' standard_of_care,
  0 SPONSOR_AMOUNT,
  0 L_VARIANCE,
  0 LINEITEM_DIRECT_PERPAT,
  0 TOTAL_COST_PER_PAT,
  0 TOTAL_COST_ALL_PAT,
  '''' BGTCAL_PROTTYPE,
  '''' CATEGORY,
  0 UNIT_COST,
  '''' NUMBER_OF_UNIT,
  '''' BGTSECTION_PATNO,
  '''' LAST_MODIFIED_BY,
  '''' LAST_MODIFIED_DATE,
  '''' category_subtyp,
  0 FK_CODELST_COST_TYPE,
  0 pk_budgetsec,
  0 budgetsec_fkvisit,
  0 pk_lineitem,
  0 lineitem_inpersec,
  0 bgtcal_excldsocflag,
  '''' BGTSECTION_TYPE,
  0 FK_EVENT,
  0 SUBCOST_ITEM_FLAG
FROM
  (SELECT pk_budget,
  budget_name,
  BUDGET_VERSION
  FROM sch_budget,
    sch_bgtcal
  WHERE pk_budget            =fk_budget
  AND fk_study IN (:studyId)
  AND NVL(budget_delflag,''N'')=''N''
  AND BGTCAL_PROTTYPE        =''S''
  AND bgtcal_protid         IS NOT NULL
  AND (SELECT codelst_subtyp
  FROM event_assoc,
    sch_codelst
  WHERE event_id = BGTCal_PROTID
  AND pk_codelst = fk_codelst_calstat)=''W''
  ) a,
  (SELECT pk_bgtcal,
    BGTCAL_PROTID
  FROM sch_budget,
    sch_bgtcal
  WHERE pk_budget            =fk_budget
  AND fk_study  IN (:studyId)
  AND NVL(budget_delflag,''N'')=''N''
  AND BGTCAL_PROTTYPE        =''S''
  AND bgtcal_protid         IS NOT NULL
  AND (SELECT codelst_subtyp
  FROM event_assoc,
    sch_codelst
  WHERE event_id = BGTCal_PROTID
  AND pk_codelst = fk_codelst_calstat)=''W''
  ) b
ORDER BY pk_budget,
  PROT_CALENDAR,
    budgetsection_sequence nulls last,
 lineitem_seq nulls last,
  l_LINEITEM_NAME nulls last ';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=267;	
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=268;
		IF (row_check = 0) then
			INSERT INTO er_report (
				pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable
				)
			VALUES(
				 268
				,'Calendar Budget and Coverage (Active - Per Study)'
				,'Calendar Budget and Coverage (Active - Per Study)'
				,0
				,'N'
				,'Study, Title, Per Patient Charge, Calendar Budget, Event, Category, Coverage Type, Unit Cost, Units, D/M, Direct Cost Per Patient, I/D Applied, Total Cost Per Patient,Total Cost All Patients'
				,'Study (Study Number)'
				,1
				,'mp_rep'
				,''
				,'studyId'
				);
				
				update_sql:='SELECT pk_budget,
  BUDGET_NAME,
  PROT_CALENDAR,
  BGTCAL_PROTID,
  BUDGET_VERSION,
  BUDGET_DESC,
  STUDY_NUMBER,
  STUDY_TITLE,
  SITE_NAME,
  budgetsection_sequence,
  category_seq,
  NVL(COVERAGE_TYPE,''Coverage Type NOT specified'') AS COVERAGE_TYPE,
  BGTSECTION_NAME,
  LINEITEM_NAME,
  lineitem_seq,
  CASE
    WHEN (FK_EVENT IS NOT NULL
    AND 1           = 0)
    THEN ''''
    ELSE CPT_CODE
  END                                           AS CPT_CODE,
  NVL(COST_PER_PATIENT,0)                       AS COST_PER_PATIENT,
  NVL(TOTAL_COST,0)                             AS TOTAL_COST,
  DECODE(LINE_ITEM_INDIRECTS_FLAG,1,''Yes'','''')   AS LINE_ITEM_INDIRECTS_FLAG,
  DECODE(COST_DISCOUNT_ON_LINE_ITEM,1,''Yes'','''') AS COST_DISCOUNT_ON_LINE_ITEM,
  INDIRECTS,
  BUDGET_INDIRECT_FLAG,
  FRINGE_BENEFIT,
  FRINGE_FLAG,
  PER_PATIENT_LINE_FRINGE,
  TOTAL_LINE_FRINGE,
  BUDGET_DISCOUNT,
  DECODE(BUDGET_DISCOUNT_FLAG,1,''Discount'',2,''Markup'','''') AS BUDGET_DISCOUNT_FLAG,
  TOTAL_COST_AFTER,
  TOTAL_PAT_COST_AFTER,
  PER_PAT_LINE_ITEM_DISCOUNT,
  TOTAL_COST_DISCOUNT,
  PERPAT_INDIRECT,
  TOTAL_COST_INDIRECT,
  BUDGET_CURRENCY,
  lower(LINEITEM_NAME)                                           AS l_LINEITEM_NAME,
  DECODE(COST_CUSTOMCOL,''research'',''No'',''soc'',''Yes'',''Other'') AS standard_of_care,
  LINEITEM_SPONSORAMOUNT                                         AS SPONSOR_AMOUNT,
  LINEITEM_VARIANCE                                              AS L_VARIANCE,
  LINEITEM_DIRECT_PERPAT,
  TOTAL_COST_PER_PAT,
  TOTAL_COST_ALL_PAT,
  BGTCAL_PROTTYPE,
  CATEGORY,
  UNIT_COST,
  NUMBER_OF_UNIT,
  CASE
    WHEN BGTSECTION_PATNO IS NULL
    THEN ''1''
    ELSE BGTSECTION_PATNO
  END AS BGTSECTION_PATNO,
  LAST_MODIFIED_BY,
  LAST_MODIFIED_DATE,
  category_subtyp,
  FK_CODELST_COST_TYPE,
  pk_budgetsec,
  NVL(budgetsec_fkvisit,0)budgetsec_fkvisit,
  pk_lineitem,
  lineitem_inpersec,
  NVL(bgtcal_excldsocflag,0) AS bgtcal_excldsocflag,
  BGTSECTION_TYPE,
  NVL(FK_EVENT, 0) AS FK_EVENT,
  SUBCOST_ITEM_FLAG
FROM ERV_BUDGET
WHERE fk_study  IN (:studyId)
AND BGTCal_PROTID IS NOT NULL
AND (SELECT codelst_subtyp
  FROM event_assoc,
    sch_codelst
  WHERE event_id             = BGTCal_PROTID
  AND pk_codelst             = fk_codelst_calstat)=''A''
AND NVL(BGTCAL_PROTTYPE,''L'')<>''L''
AND BUDGETSEC_FKVISIT       >0

UNION ALL

SELECT pk_budget,
   BUDGET_NAME,
  (SELECT name FROM event_assoc WHERE event_id= b.bgtcal_protid
  ) AS PROT_CALENDAR,
  b.BGTCAL_PROTID,
   BUDGET_VERSION,
  '''' BUDGET_DESC,
  '''' STUDY_NUMBER,
  '''' STUDY_TITLE,
  '''' SITE_NAME,
NULL AS budgetsection_sequence,
  0 category_seq,
  '''' COVERAGE_TYPE,
  '''' BGTSECTION_NAME,
  '''' LINEITEM_NAME,
NULL AS lineitem_seq,
  '''' CPT_CODE,
  0 COST_PER_PATIENT,
  0 TOTAL_COST,
  '''' LINE_ITEM_INDIRECTS_FLAG,
  '''' COST_DISCOUNT_ON_LINE_ITEM,
  0 INDIRECTS,
  '''' BUDGET_INDIRECT_FLAG,
  0 FRINGE_BENEFIT,
  0 FRINGE_FLAG,
  0 PER_PATIENT_LINE_FRINGE,
  0 TOTAL_LINE_FRINGE,
  0 BUDGET_DISCOUNT,
  '''' BUDGET_DISCOUNT_FLAG,
  0 TOTAL_COST_AFTER,
  0 TOTAL_PAT_COST_AFTER,
  0 PER_PAT_LINE_ITEM_DISCOUNT,
  0 TOTAL_COST_DISCOUNT,
  0 PERPAT_INDIRECT,
  0 TOTAL_COST_INDIRECT,
  '''' BUDGET_CURRENCY,
  NULL AS l_LINEITEM_NAME,
  ''No'' standard_of_care,
  0 SPONSOR_AMOUNT,
  0 L_VARIANCE,
  0 LINEITEM_DIRECT_PERPAT,
  0 TOTAL_COST_PER_PAT,
  0 TOTAL_COST_ALL_PAT,
  '''' BGTCAL_PROTTYPE,
  '''' CATEGORY,
  0 UNIT_COST,
  '''' NUMBER_OF_UNIT,
  '''' BGTSECTION_PATNO,
  '''' LAST_MODIFIED_BY,
  '''' LAST_MODIFIED_DATE,
  '''' category_subtyp,
  0 FK_CODELST_COST_TYPE,
  0 pk_budgetsec,
  0 budgetsec_fkvisit,
  0 pk_lineitem,
  0 lineitem_inpersec,
  0 bgtcal_excldsocflag,
  '''' BGTSECTION_TYPE,
  0 FK_EVENT,
  0 SUBCOST_ITEM_FLAG
FROM
  (SELECT pk_budget,
  budget_name,
  BUDGET_VERSION
  FROM sch_budget,
    sch_bgtcal
  WHERE pk_budget            =fk_budget
  AND fk_study IN (:studyId)
  AND NVL(budget_delflag,''N'')=''N''
  AND BGTCAL_PROTTYPE        =''S''
  AND bgtcal_protid         IS NOT NULL
  AND (SELECT codelst_subtyp
  FROM event_assoc,
    sch_codelst
  WHERE event_id = BGTCal_PROTID
  AND pk_codelst = fk_codelst_calstat)=''A''
  ) a,
  (SELECT pk_bgtcal,
    BGTCAL_PROTID
  FROM sch_budget,
    sch_bgtcal
  WHERE pk_budget            =fk_budget
  AND fk_study  IN (:studyId)
  AND NVL(budget_delflag,''N'')=''N''
  AND BGTCAL_PROTTYPE        =''S''
  AND bgtcal_protid         IS NOT NULL
  AND (SELECT codelst_subtyp
  FROM event_assoc,
    sch_codelst
  WHERE event_id = BGTCal_PROTID
  AND pk_codelst = fk_codelst_calstat)=''A''
  ) b
ORDER BY pk_budget,
  PROT_CALENDAR,
    budgetsection_sequence nulls last,
 lineitem_seq nulls last,
  l_LINEITEM_NAME nulls last ';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=268;		
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=269;

		IF (row_check = 0) then
			INSERT INTO er_report (
				pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable
				)
			VALUES(
				 269
				,'Research Ticket Items (Per Patient)'
				,'Research Ticket Items (Per Patient)'
				,0
				,'N'
				,'Study, PI, Division, Patient Name, Patient ID, Study Contact, Visit, Event, CPT Code, Charges, Date of Service, Additional Comments (Reason for Change)'
				,'Date (Date of Service) </br> Study (Study Number) </br> Patient (Patient ID) '
				,1
				,'rep_pcr'
				,''
				,'date:studyId:patientId'
				);
				
				update_sql:='select distinct
es.study_number as study_number,
es.study_title as Study_title,
visit_name,
se.event_id,
se.reason_for_coveragechange,
(select sum(eventcost_value) from sch_eventcost where fk_cost_desc = (select pk_codelst from sch_codelst where codelst_type = ''cost_desc'' and codelst_subtyp = ''res_cost'') and fk_event = se.fk_assoc) as cost,
(select usr_firstname || '' '' || usr_lastname from er_user where pk_user = es.study_coordinator) as UserName,
(select add_phone from er_add where pk_add = (select fk_peradd from er_user where pk_user = es.study_coordinator)) as Contact,
(select name from event_assoc where event_id = se.fk_assoc) as Event_Name,
(select codelst_desc from sch_codelst where pk_codelst = se.fk_codelst_covertype) as CoverageType,
(select event_cptcode from event_assoc where event_id = se.fk_assoc) as cptcode,
(select eventstat_dt from sch_eventstat where pk_eventstat = (select max(pk_eventstat) from sch_eventstat where fk_event = se.event_id and eventstat = (select pk_codelst from sch_codelst where codelst_type=''eventstatus'' and codelst_subtyp = ''ev_done''))) as ServiceDate,
(select codelst_desc from er_codelst where pk_codelst = es.fk_codelst_tarea) as Department,
(SELECT u.USR_FIRSTNAME || '' ''|| u.USR_LASTNAME FROM er_study,er_user u WHERE pk_study = ea.chain_id AND er_study.STUDY_PRINV=u.pk_user
  ) AS principal_investigator,
study_contact,
(select person_fname  || '' ''|| person_lname from er_patprot ep, esch.person p where fk_study =  :studyId  and p.pk_person = ep.fk_per and p.pk_person = :patientId and ep.patprot_stat = 1) as PatName,
(select pat_facilityid from er_patprot ep, esch.person p where fk_study = :studyId and p.pk_person = ep.fk_per and p.pk_person =  :patientId  and ep.patprot_stat = 1) as PatFacilityId
from event_assoc ea, er_study es, sch_protocol_visit spv, sch_events1 se, sch_eventstat sev
WHERE ea.event_id = spv.fk_protocol and
spv.pk_protocol_visit = se.fk_visit and
sev.fk_event = se.event_id and
(select EVENTSTAT_DT from sch_eventstat where PK_EVENTSTAT = (select max(PK_EVENTSTAT) from sch_eventstat where fk_event = se.event_id)) >= TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) and
(select EVENTSTAT_DT from sch_eventstat where PK_EVENTSTAT = (select max(PK_EVENTSTAT) from sch_eventstat where fk_event = se.event_id)) <= TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) and
eventstat = (select pk_codelst from sch_codelst where codelst_type=''eventstatus'' and codelst_subtyp = ''ev_done'') and
se.fk_codelst_covertype = (select pk_codelst from sch_codelst where codelst_type=''coverage_type'' and codelst_subtyp = ''S'') and
pk_study IN (:studyId)  and
se.fk_patprot IN (select pk_patprot from er_patprot ep, esch.person p where fk_study = :studyId  and p.pk_person = ep.fk_per and p.pk_person = :patientId  and ep.patprot_stat = 1)';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=269;		
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=270;

		IF (row_check = 0) then
			INSERT INTO er_report (
				pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable
				)
			VALUES(
				 270
				,'Routine Care Events (Per Patient)'
				,'Routine Care Events (Per Patient)'
				,0
				,'N'
				,'Study, PI, Division, Patient Name, Patient ID, Study Contact, Visit, Event, CPT Code, Charges, Date of Service, Additional Comments (Reason for Change)'
				,'Date (Date of Service) </br> Study (Study Number) </br> Patient (Patient ID)  '
				,1
				,'rep_pcr'
				,''
				,'date:studyId:patientId'
				);
				
				update_sql:='select distinct
es.study_number as study_number,
es.study_title as Study_title,
visit_name,
se.event_id,
se.reason_for_coveragechange,
(select usr_firstname || '' '' || usr_lastname from er_user where pk_user = es.study_coordinator) as UserName,
(select add_phone from er_add where pk_add = (select fk_peradd from er_user where pk_user = es.study_coordinator)) as Contact,
(select name from event_assoc where event_id = se.fk_assoc) as Event_Name,
(select codelst_desc from sch_codelst where pk_codelst = se.fk_codelst_covertype) as CoverageType,
(select event_cptcode from event_assoc where event_id = se.fk_assoc) as cptcode,
(select eventstat_dt from sch_eventstat where pk_eventstat = (select max(pk_eventstat) from sch_eventstat where fk_event = se.event_id and eventstat = (select pk_codelst from sch_codelst where codelst_type=''eventstatus'' and codelst_subtyp = ''ev_done''))) as ServiceDate,
(select codelst_desc from er_codelst where pk_codelst = es.fk_codelst_tarea) as Department,
(SELECT u.USR_FIRSTNAME || '' ''|| u.USR_LASTNAME FROM er_study,er_user u WHERE pk_study = ea.chain_id AND er_study.STUDY_PRINV=u.pk_user
  ) AS principal_investigator,
study_contact,
(select person_fname  || '' ''|| person_lname from er_patprot ep, esch.person p where fk_study =  :studyId  and p.pk_person = ep.fk_per and p.pk_person = :patientId and ep.patprot_stat = 1) as PatName,
(select pat_facilityid from er_patprot ep, esch.person p where fk_study = :studyId and p.pk_person = ep.fk_per and p.pk_person =  :patientId  and ep.patprot_stat = 1) as PatFacilityId
from event_assoc ea, er_study es, sch_protocol_visit spv, sch_events1 se, sch_eventstat sev
WHERE ea.event_id = spv.fk_protocol and
spv.pk_protocol_visit = se.fk_visit and
sev.fk_event = se.event_id and
(select EVENTSTAT_DT from sch_eventstat where PK_EVENTSTAT = (select max(PK_EVENTSTAT) from sch_eventstat where fk_event = se.event_id)) >= TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) and
(select EVENTSTAT_DT from sch_eventstat where PK_EVENTSTAT = (select max(PK_EVENTSTAT) from sch_eventstat where fk_event = se.event_id)) <= TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) and
eventstat = (select pk_codelst from sch_codelst where codelst_type=''eventstatus'' and codelst_subtyp = ''ev_done'') and
se.fk_codelst_covertype = (select pk_codelst from sch_codelst where codelst_type=''coverage_type'' and codelst_subtyp = ''M'') and
pk_study IN (:studyId)  and
se.fk_patprot IN (select pk_patprot from er_patprot ep, esch.person p where fk_study = :studyId  and p.pk_person = ep.fk_per and p.pk_person = :patientId  and ep.patprot_stat = 1)';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=270;		
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=271;

		IF (row_check = 0) then
			INSERT INTO er_report (
				pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable
				)
			VALUES(
				 271
				,'Research Ticket Items (Per Study)'
				,'Research Ticket Items (Per Study)'
				,0
				,'N'
				,'Study, PI, Division, Patient Name, Patient ID, Study Contact, Visit, Event, CPT Code, Charges, Date of Service, Additional Comments (Reason for Change)'
				,'Date (Date of Service) </br> Study (Study Number) </br> Patient (Patient ID)  '
				,1
				,'rep_pcr'
				,''
				,'date:studyId'
				);
				
				update_sql:='select distinct
es.study_number as study_number,
es.study_title as Study_title,
visit_name,
se.event_id,
se.reason_for_coveragechange,
(select sum(eventcost_value) from sch_eventcost where fk_cost_desc = (select pk_codelst from sch_codelst where codelst_type = ''cost_desc'' and codelst_subtyp = ''res_cost'') and fk_event = se.fk_assoc) as cost,
(select usr_firstname || '' '' || usr_lastname from er_user where pk_user = es.study_coordinator) as UserName,
(select add_phone from er_add where pk_add = (select fk_peradd from er_user where pk_user = es.study_coordinator)) as Contact,
(select name from event_assoc where event_id = se.fk_assoc) as Event_Name,
(select codelst_desc from sch_codelst where pk_codelst = se.fk_codelst_covertype) as CoverageType,
(select event_cptcode from event_assoc where event_id = se.fk_assoc) as cptcode,
(select eventstat_dt from sch_eventstat where pk_eventstat = (select max(pk_eventstat) from sch_eventstat where fk_event = se.event_id and eventstat = (select pk_codelst from sch_codelst where codelst_type=''eventstatus'' and codelst_subtyp = ''ev_done''))) as ServiceDate,
(select codelst_desc from er_codelst where pk_codelst = es.fk_codelst_tarea) as Department,
(SELECT u.USR_FIRSTNAME || '' ''|| u.USR_LASTNAME FROM er_study,er_user u WHERE pk_study = ea.chain_id AND er_study.STUDY_PRINV=u.pk_user
  ) AS principal_investigator,
study_contact,
(select person_fname  || '' ''|| person_lname from esch.person p where p.pk_person IN (select fk_per from er_patprot where fk_study IN (:studyId) and pk_patprot = se.fk_patprot)) as PatName,
(select pat_facilityid from esch.person p where p.pk_person IN (select fk_per from er_patprot where fk_study IN (:studyId) and pk_patprot = se.fk_patprot)) as PatFacilityId
from event_assoc ea, er_study es, sch_protocol_visit spv, sch_events1 se, sch_eventstat sev
, er_patprot ep
WHERE ea.event_id = spv.fk_protocol and
spv.pk_protocol_visit = se.fk_visit and
sev.fk_event = se.event_id and
(select EVENTSTAT_DT from sch_eventstat where PK_EVENTSTAT = (select max(PK_EVENTSTAT) from sch_eventstat where fk_event = se.event_id)) >= TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) and
(select EVENTSTAT_DT from sch_eventstat where PK_EVENTSTAT = (select max(PK_EVENTSTAT) from sch_eventstat where fk_event = se.event_id)) <= TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) and
eventstat = (select pk_codelst from sch_codelst where codelst_type=''eventstatus'' and codelst_subtyp = ''ev_done'') and
se.fk_codelst_covertype = (select pk_codelst from sch_codelst where codelst_type=''coverage_type'' and codelst_subtyp = ''S'') and
pk_study IN (:studyId)  and
se.fk_patprot IN (select pk_patprot from er_patprot ep, esch.person p where fk_study IN (:studyId)   and p.pk_person = ep.fk_per and ep.patprot_stat = 1)
order by PatFacilityId, ServiceDate';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=271;		
		END IF;
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=272;

		IF (row_check = 0) then
			INSERT INTO er_report (
				pk_report
				,rep_name
				,rep_desc
				,fk_account
				,rep_hide
				,rep_columns
				,rep_filterby
				,generate_xml
				,rep_type
				,rep_sql_clob
				,rep_filterapplicable
				)
			VALUES(
				 272
				,'Routine Care Events (Per Study)'
				,'Routine Care Events (Per Study)'
				,0
				,'N'
				,'Study, PI, Division, Patient Name, Patient ID, Study Contact, Visit, Event, CPT Code, Charges, Date of Service, Additional Comments (Reason for Change)'
				,'Date (Date of Service) </br> Study (Study Number) </br> Patient (Patient ID)  '
				,1
				,'rep_pcr'
				,''
				,'date:studyId'
				);
				
				update_sql:='select distinct
es.study_number as study_number,
es.study_title as Study_title,
visit_name,
se.event_id,
se.reason_for_coveragechange,
(select usr_firstname || '' '' || usr_lastname from er_user where pk_user = es.study_coordinator) as UserName,
(select add_phone from er_add where pk_add = (select fk_peradd from er_user where pk_user = es.study_coordinator)) as Contact,
(select name from event_assoc where event_id = se.fk_assoc) as Event_Name,
(select codelst_desc from sch_codelst where pk_codelst = se.fk_codelst_covertype) as CoverageType,
(select event_cptcode from event_assoc where event_id = se.fk_assoc) as cptcode,
(select eventstat_dt from sch_eventstat where pk_eventstat = (select max(pk_eventstat) from sch_eventstat where fk_event = se.event_id and eventstat = (select pk_codelst from sch_codelst where codelst_type=''eventstatus'' and codelst_subtyp = ''ev_done''))) as ServiceDate,
(select codelst_desc from er_codelst where pk_codelst = es.fk_codelst_tarea) as Department,
(SELECT u.USR_FIRSTNAME || '' ''|| u.USR_LASTNAME FROM er_study,er_user u WHERE pk_study = ea.chain_id AND er_study.STUDY_PRINV=u.pk_user
  ) AS principal_investigator,
study_contact,
(select person_fname  || '' ''|| person_lname from esch.person p where p.pk_person IN (select fk_per from er_patprot where fk_study IN (:studyId) and pk_patprot = se.fk_patprot)) as PatName,
(select pat_facilityid from esch.person p where p.pk_person IN (select fk_per from er_patprot where fk_study IN (:studyId) and pk_patprot = se.fk_patprot)) as PatFacilityId
from event_assoc ea, er_study es, sch_protocol_visit spv, sch_events1 se, sch_eventstat sev
, er_patprot ep
WHERE ea.event_id = spv.fk_protocol and
spv.pk_protocol_visit = se.fk_visit and
sev.fk_event = se.event_id and
(select EVENTSTAT_DT from sch_eventstat where PK_EVENTSTAT = (select max(PK_EVENTSTAT) from sch_eventstat where fk_event = se.event_id)) >= TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) and
(select EVENTSTAT_DT from sch_eventstat where PK_EVENTSTAT = (select max(PK_EVENTSTAT) from sch_eventstat where fk_event = se.event_id)) <= TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) and
eventstat = (select pk_codelst from sch_codelst where codelst_type=''eventstatus'' and codelst_subtyp = ''ev_done'') and
se.fk_codelst_covertype = (select pk_codelst from sch_codelst where codelst_type=''coverage_type'' and codelst_subtyp = ''M'') and
pk_study IN (:studyId)  and
se.fk_patprot IN (select pk_patprot from er_patprot ep, esch.person p where fk_study IN (:studyId)   and p.pk_person = ep.fk_per and ep.patprot_stat = 1)
order by PatFacilityId, ServiceDate';
		
		UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=272;		
		END IF;
	END IF;
COMMIT;
END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,382,9,'09_er_report_patch_fin.sql',sysdate,'v10.1 #783');

commit;
