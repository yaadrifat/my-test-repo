DECLARE
	table_check number;
	row_check number;
BEGIN

	SELECT count(*)
	INTO table_check
	FROM user_tab_cols
	WHERE TABLE_NAME = 'ER_CODELST';

	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM er_codelst
		WHERE codelst_subtyp = 'rep_nci' AND codelst_type='report';

		IF (row_check > 0) then
			UPDATE er_codelst 
			SET codelst_custom_col='accrual'
			WHERE codelst_subtyp='rep_nci' AND codelst_type='report';	
		END IF;
	END IF;
	
	COMMIT;
	
END;
/



INSERT INTO track_patches
VALUES(seq_track_patches.nextval,382,17,'17_er_codelst_Update.sql',sysdate,'v10.1 #783');

commit;