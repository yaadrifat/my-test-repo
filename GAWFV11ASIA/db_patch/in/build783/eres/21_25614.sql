DECLARE 
	table_check number;
	row_check number;
	update_sql clob;

BEGIN
	SELECT count(*)
	INTO table_check
	FROM user_tab_cols
	WHERE TABLE_NAME = 'ER_REPORT';
	
	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report=263;

		IF (row_check > 0) then
update_sql:='select  distinct
b.study_number,
study_division,
study_pi,
study_coordinator,
nvl((select sum(msrul_amount) from vda.vda_v_milestones where vda.vda_v_milestones.fk_study = a.fk_study),''0'') as milestone_amt,
nvl((select sum(milestone_holdback) from vda.vda_v_milestones where vda.vda_v_milestones.fk_study = a.fk_study),''0'') as holdback_amt,
fk_study,
nvl((select sum(amount_invoiced) from er_invoice_detail where er_invoice_detail.fk_study = a.fk_study and detail_type=''H''),''0'') as inv_amt,
nvl((select sum(mp_amount) from er_milepayment_details where fk_milepayment in (select pk_milepayment from er_milepayment where er_milepayment.fk_study = a.fk_study )),''0'') as rec_amt
from VDA.vda_v_milestone_achvd_det a, VDA.vda_v_study_summary b
where (select msrul_pay_type from vda.vda_v_milestones where vda.vda_v_milestones.pk_milestone = a.fk_milestone) != ''Payable''
and fk_study = pk_study
AND fk_study IN (:studyId)
AND TRUNC(MSACH_ACH_DATE) BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)';
UPDATE ER_REPORT SET REP_SQL_CLOB=update_sql WHERE pk_report=263;					
		END IF; 
	END IF;
	commit;
end;
/	


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,382,21,'21_25614.sql',sysdate,'v10.1 #783');

commit;