DECLARE 
	table_check number;
	row_check number;
	update_sql clob;
	
BEGIN
	SELECT count(*)
	INTO table_check
	FROM user_tab_cols
	WHERE TABLE_NAME = 'ER_REPORT';
	
	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report = 66;
		
		IF (row_check > 0) then
		
		update_sql:='SELECT
  (SELECT form_name FROM ER_FORMLIB WHERE pk_formlib = fk_formlib
  ) AS form_name,
  (SELECT per_code FROM ER_PER WHERE pk_per = fk_per
  ) AS patient_id,
  (SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT)
  FROM ER_PATSTUDYSTAT
  WHERE fk_per        = x.fk_per
  AND fk_study        = x.fk_study
  AND fk_codelst_stat =
    (SELECT pk_codelst
    FROM ER_CODELST
    WHERE codelst_type = ''patStatus''
    AND codelst_subtyp = ''enrolled''
    )
  ) AS enrolled_date,
  (SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT)
  FROM ER_PATSTUDYSTAT
  WHERE fk_per        = x.fk_per
  AND fk_study        = x.fk_study
  AND fk_codelst_stat =
    (SELECT pk_codelst
    FROM ER_CODELST
    WHERE codelst_type = ''patStatus''
    AND codelst_subtyp = ''active''
    )
  AND ROWNUM = 1
  ) AS active_date,
  (SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT)
  FROM ER_PATSTUDYSTAT
  WHERE fk_per        = x.fk_per
  AND fk_study        = x.fk_study
  AND fk_codelst_stat =
    (SELECT pk_codelst
    FROM ER_CODELST
    WHERE codelst_type = ''patStatus''
    AND codelst_subtyp = ''offtreat''
    )
  AND ROWNUM = 1
  ) AS offtreat_date,
  (SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT)
  FROM ER_PATSTUDYSTAT
  WHERE fk_per        = x.fk_per
  AND fk_study        = x.fk_study
  AND fk_codelst_stat =
    (SELECT pk_codelst
    FROM ER_CODELST
    WHERE codelst_type = ''patStatus''
    AND codelst_subtyp = ''offstudy''
    )
  AND ROWNUM = 1
  ) AS offstudy_date,
  (SELECT
    (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_stat
    )
  FROM ER_PATSTUDYSTAT
  WHERE fk_per          = x.fk_per
  AND fk_study          = x.fk_study
  AND patstudystat_date =
    (SELECT MAX(patstudystat_date)
    FROM ER_PATSTUDYSTAT
    WHERE fk_per = x.fk_per
    AND fk_study = x.fk_study
    )
  AND ROWNUM = 1
  ) AS current_status,
  (SELECT COUNT(*)
  FROM ER_PATFORMS
  WHERE fk_formlib = x.fk_formlib
  AND fk_patprot   = pk_patprot
  AND record_type <> ''D''
  ) AS tot_forms,
  (SELECT COUNT(*)
  FROM ER_PATFORMS
  WHERE fk_formlib   = x.fk_formlib
  AND fk_patprot     = pk_patprot
  AND record_type   <> ''D''
  AND form_completed =
    (SELECT pk_codelst
    FROM ER_CODELST
    WHERE codelst_type = ''fillformstat''
    AND codelst_subtyp = ''complete''
    )
  ) AS comp_formstat,
  (SELECT COUNT(*)
  FROM ER_PATFORMS
  WHERE fk_formlib   = x.fk_formlib
  AND fk_patprot     = pk_patprot
  AND record_type   <> ''D''
  AND form_completed =
    (SELECT pk_codelst
    FROM ER_CODELST
    WHERE codelst_type = ''fillformstat''
    AND codelst_subtyp = ''working''
    )
  ) AS work_formstat,
  (SELECT COUNT(*)
  FROM ER_PATFORMS
  WHERE fk_formlib        = x.fk_formlib
  AND fk_patprot          = pk_patprot
  AND record_type        <> ''D''
  AND form_completed NOT IN
    (SELECT pk_codelst
    FROM ER_CODELST
    WHERE codelst_type  = ''fillformstat''
    AND (codelst_subtyp = ''working''
    OR codelst_subtyp   = ''complete'')
    )
  ) AS othr_formstat,
  (SELECT TO_CHAR(MAX(created_on),PKG_DATEUTIL.F_GET_DATETIMEFORMAT)
  FROM ER_PATFORMS
  WHERE fk_formlib = x.fk_formlib
  AND fk_patprot   = pk_patprot
  AND record_type <> ''D''
  ) AS form_createdon,
  (SELECT
    (SELECT usr_firstname
      || '' ''
      || usr_lastname
    FROM ER_USER
    WHERE pk_user = y.creator
    )
  FROM ER_PATFORMS y
  WHERE fk_formlib = x.fk_formlib
  AND fk_patprot   = pk_patprot
  AND record_type <> ''D''
  AND created_on   =
    (SELECT MAX(created_on)
    FROM ER_PATFORMS
    WHERE fk_formlib = x.fk_formlib
    AND fk_patprot   = pk_patprot
    AND record_type <> ''D''
    )
  AND ROWNUM = 1
  ) AS form_createdby,
  (SELECT TO_CHAR(MAX(last_modified_date),PKG_DATEUTIL.F_GET_DATEtimeFORMAT)
  FROM ER_PATFORMS
  WHERE fk_formlib = x.fk_formlib
  AND fk_patprot   = pk_patprot
  AND record_type <> ''D''
  ) AS form_modon,
  (SELECT
    (SELECT usr_firstname
      || '' ''
      || usr_lastname
    FROM ER_USER
    WHERE pk_user = y.last_modified_by
    )
  FROM ER_PATFORMS y
  WHERE fk_formlib       = x.fk_formlib
  AND fk_patprot         = pk_patprot
  AND record_type       <> ''D''
  AND last_modified_date =
    (SELECT MAX(last_modified_date)
    FROM ER_PATFORMS
    WHERE fk_formlib = x.fk_formlib
    AND fk_patprot   = pk_patprot
    AND record_type <> ''D''
    )
  AND ROWNUM = 1
  ) AS form_modby
FROM
  (SELECT DISTINCT a.fk_per,
    fk_formlib,
    fk_study,
    pk_patprot
  FROM ER_PATPROT a,
    ER_PATFORMS b
  WHERE pk_patprot = fk_patprot
  AND fk_study    IN (:studyId)
  AND a.fk_per    IN (:patientId)
  AND (b.created_on BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
  OR b.last_modified_date BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) )
  ) x
UNION ALL
SELECT ''Adverse EVENTS'' AS form_name,
  (SELECT per_code FROM ER_PER WHERE pk_per = fk_per
  ) AS patient_id,
  (SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT)
  FROM ER_PATSTUDYSTAT
  WHERE fk_per        = x.fk_per
  AND fk_study        = x.fk_study
  AND fk_codelst_stat =
    (SELECT pk_codelst
    FROM ER_CODELST
    WHERE codelst_type = ''patStatus''
    AND codelst_subtyp = ''enrolled''
    )
  ) AS enrolled_date,
  (SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT)
  FROM ER_PATSTUDYSTAT
  WHERE fk_per        = x.fk_per
  AND fk_study        = x.fk_study
  AND fk_codelst_stat =
    (SELECT pk_codelst
    FROM ER_CODELST
    WHERE codelst_type = ''patStatus''
    AND codelst_subtyp = ''active''
    )
  AND ROWNUM = 1
  ) AS active_date,
  (SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT)
  FROM ER_PATSTUDYSTAT
  WHERE fk_per        = x.fk_per
  AND fk_study        = x.fk_study
  AND fk_codelst_stat =
    (SELECT pk_codelst
    FROM ER_CODELST
    WHERE codelst_type = ''patStatus''
    AND codelst_subtyp = ''offtreat''
    )
  AND ROWNUM = 1
  ) AS offtreat_date,
  (SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT)
  FROM ER_PATSTUDYSTAT
  WHERE fk_per        = x.fk_per
  AND fk_study        = x.fk_study
  AND fk_codelst_stat =
    (SELECT pk_codelst
    FROM ER_CODELST
    WHERE codelst_type = ''patStatus''
    AND codelst_subtyp = ''offstudy''
    )
  AND ROWNUM = 1
  ) AS offstudy_date,
  (SELECT
    (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_stat
    )
  FROM ER_PATSTUDYSTAT
  WHERE fk_per          = x.fk_per
  AND fk_study          = x.fk_study
  AND patstudystat_date =
    (SELECT MAX(patstudystat_date)
    FROM ER_PATSTUDYSTAT
    WHERE fk_per = x.fk_per
    AND fk_study = x.fk_study
    )
  AND ROWNUM = 1
  ) AS current_status,
  (SELECT COUNT(*)
  FROM ESCH.sch_adverseve
  WHERE fk_study = x.fk_study
  AND fk_per     = x.fk_per
  ) AS tot_forms,
  (SELECT COUNT(*)
  FROM ESCH.sch_adverseve
  WHERE fk_study  = x.fk_study
  AND fk_per      = x.fk_per
  AND form_status =
    (SELECT pk_codelst
    FROM ESCH.sch_codelst
    WHERE trim(codelst_type) = ''fillformstat''
    AND trim(codelst_subtyp) = ''complete''
    )
  ) AS comp_formstat,
  (SELECT COUNT(*)
  FROM ESCH.sch_adverseve
  WHERE fk_study  = x.fk_study
  AND fk_per      = x.fk_per
  AND form_status =
    (SELECT pk_codelst
    FROM ESCH.sch_codelst
    WHERE trim(codelst_type) = ''fillformstat''
    AND trim(codelst_subtyp) = ''working''
    )
  ) AS work_formstat,
 (SELECT COUNT(*)
  FROM ESCH.sch_adverseve
  WHERE fk_study        = x.fk_study
  AND fk_per          = x.fk_per
  AND form_status NOT IN
    (SELECT pk_codelst
    FROM ESCH.sch_codelst
    WHERE codelst_type  = ''fillformstat''
    AND (codelst_subtyp = ''working''
    OR codelst_subtyp   = ''complete'')
    )
  ) AS othr_formstat,
  (SELECT TO_CHAR(MAX(created_on),PKG_DATEUTIL.F_GET_DATETIMEFORMAT)
  FROM ESCH.sch_adverseve
  WHERE fk_study = x.fk_study
  AND fk_per     = x.fk_per
  ) AS form_createdon,
  (SELECT
    (SELECT usr_firstname
      || '' ''
      || usr_lastname
    FROM ER_USER
    WHERE pk_user = y.creator
    )
  FROM ESCH.sch_adverseve y
  WHERE fk_study = x.fk_study
  AND fk_per     = x.fk_per
  AND created_on =
    (SELECT MAX(created_on)
    FROM ESCH.sch_adverseve
    WHERE fk_study = x.fk_study
    AND fk_per     = x.fk_per
    )
  AND ROWNUM = 1
  ) AS form_createdby,
  (SELECT TO_CHAR(MAX(last_modified_date),PKG_DATEUTIL.F_GET_DATEtimeFORMAT)
  FROM ESCH.sch_adverseve
  WHERE fk_study = x.fk_study
  AND fk_per     = x.fk_per
  ) AS form_modon,
  (SELECT
    (SELECT usr_firstname
      || '' ''
      || usr_lastname
    FROM ER_USER
    WHERE pk_user = y.last_modified_by
    )
  FROM ESCH.sch_adverseve y
  WHERE fk_study         = x.fk_study
  AND fk_per             = x.fk_per
  AND last_modified_date =
    (SELECT MAX(last_modified_date)
    FROM ESCH.sch_adverseve
    WHERE fk_study = x.fk_study
    AND fk_per     = x.fk_per
    )
  AND ROWNUM = 1
  ) AS form_modby
FROM
  (SELECT DISTINCT a.fk_per,
    a.fk_study
  FROM ER_PATPROT a,
    ESCH.sch_adverseve b
  WHERE a.FK_PER   = b.FK_PER
  AND a.fk_study   = b.fk_study
  AND a.fk_study  IN (:studyId)
  AND Patprot_stat = 1
  AND a.fk_per    IN (:patientId)
  AND (b.created_on BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
  OR b.last_modified_date BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) )
  ) x
UNION ALL
SELECT ''Labs'' AS form_name,
  (SELECT per_code FROM ER_PER WHERE pk_per = fk_per
  ) AS patient_id,
  (SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT)
  FROM ER_PATSTUDYSTAT
  WHERE fk_per        = x.fk_per
  AND fk_study        = x.fk_study
  AND fk_codelst_stat =
    (SELECT pk_codelst
    FROM ER_CODELST
    WHERE codelst_type = ''patStatus''
    AND codelst_subtyp = ''enrolled''
    )
  ) AS enrolled_date,
  (SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT)
  FROM ER_PATSTUDYSTAT
  WHERE fk_per        = x.fk_per
  AND fk_study        = x.fk_study
  AND fk_codelst_stat =
    (SELECT pk_codelst
    FROM ER_CODELST
    WHERE codelst_type = ''patStatus''
    AND codelst_subtyp = ''active''
    )
  AND ROWNUM = 1
  ) AS active_date,
  (SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT)
  FROM ER_PATSTUDYSTAT
  WHERE fk_per        = x.fk_per
  AND fk_study        = x.fk_study
  AND fk_codelst_stat =
    (SELECT pk_codelst
    FROM ER_CODELST
    WHERE codelst_type = ''patStatus''
    AND codelst_subtyp = ''offtreat''
    )
  AND ROWNUM = 1
  ) AS offtreat_date,
  (SELECT TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT)
  FROM ER_PATSTUDYSTAT
  WHERE fk_per        = x.fk_per
  AND fk_study        = x.fk_study
  AND fk_codelst_stat =
    (SELECT pk_codelst
    FROM ER_CODELST
    WHERE codelst_type = ''patStatus''
    AND codelst_subtyp = ''offstudy''
    )
  AND ROWNUM = 1
  ) AS offstudy_date,
  (SELECT
    (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_stat
    )
  FROM ER_PATSTUDYSTAT
  WHERE fk_per          = x.fk_per
  AND fk_study          = x.fk_study
  AND patstudystat_date =
    (SELECT MAX(patstudystat_date)
    FROM ER_PATSTUDYSTAT
    WHERE fk_per = x.fk_per
    AND fk_study = x.fk_study
    )
  AND ROWNUM = 1
  ) AS current_status,
  (SELECT COUNT(*) FROM ER_PATLABS WHERE fk_per = x.fk_per
  ) AS tot_forms,
  0 AS comp_formstat,
  0 AS work_formstat,
  0 AS othr_formstat,
  (SELECT TO_CHAR(MAX(created_on),PKG_DATEUTIL.F_GET_DATETIMEFORMAT)
  FROM ER_PATLABS
  WHERE fk_per = x.fk_per
  ) AS form_createdon,
  (SELECT
    (SELECT usr_firstname
      || '' ''
      || usr_lastname
    FROM ER_USER
    WHERE pk_user = y.creator
    )
  FROM ER_PATLABS y
  WHERE fk_per   = x.fk_per
  AND created_on =
    (SELECT MAX(created_on) FROM ER_PATLABS WHERE fk_per = x.fk_per
    )
  AND ROWNUM = 1
  ) AS form_createdby,
  (SELECT TO_CHAR(MAX(last_modified_date),PKG_DATEUTIL.F_GET_DATEtimeFORMAT)
  FROM ER_PATLABS
  WHERE fk_per = x.fk_per
  ) AS form_modon,
  (SELECT
    (SELECT usr_firstname
      || '' ''
      || usr_lastname
    FROM ER_USER
    WHERE pk_user = y.last_modified_by
    )
  FROM ER_PATLABS y
  WHERE fk_per           = x.fk_per
  AND last_modified_date =
    (SELECT MAX(last_modified_date) FROM ER_PATLABS WHERE fk_per = x.fk_per
    )
  AND ROWNUM = 1
  ) AS form_modby
FROM
  (SELECT DISTINCT a.fk_per,
    a.fk_study
  FROM ER_PATPROT a,
    ER_PATLABS b
  WHERE a.fk_per   = b.fk_per
  AND a.fk_study  IN (:studyId)
  AND Patprot_stat = 1
  AND a.fk_per    IN (:patientId)
  AND (b.created_on BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT)
  OR b.last_modified_date BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) )
  ) x
ORDER BY 2,1';
		
		UPDATE ER_REPORT SET rep_sql_clob=update_sql WHERE  pk_report=66;	
		
	END IF;
		
	END IF;
	
	COMMIT;

END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,382,28,'28_er_report.sql',sysdate,'v10.1 #783');

commit;