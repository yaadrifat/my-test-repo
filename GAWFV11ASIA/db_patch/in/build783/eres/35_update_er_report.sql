DECLARE 
	table_check number;
	row_check number;
	update_sql clob; 
	
BEGIN
	SELECT count(*)
	INTO table_check
	FROM USER_TABLES
	WHERE TABLE_NAME = 'ER_REPORT';
	
	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM er_report
		WHERE pk_report = 255;
		
		IF (row_check > 0) then
			update_sql:='select
fk_study,
fk_site,
study_number,
site_name,
studysite_type,
studysite_lsamplesize,
TO_CHAR((select min(patprot_enroldt) from vda.vda_v_pat_accrual where vda.vda_v_pat_accrual.fk_study = a.fk_study and vda.vda_v_pat_accrual.fk_site_enrolling = a.fk_site),PKG_DATEUTIL.F_GET_DATEFORMAT) as first_enrolled_date,
(SELECT COUNT(*) FROM vda.vda_v_pat_accrual WHERE vda.vda_v_pat_accrual.fk_study = a.fk_study AND vda.vda_v_pat_accrual.FK_SITE_ENROLLING = a.fk_site) AS  tot_pt,
(SELECT COUNT(*) FROM ER_PATSTUDYSTAT x,ER_PATPROT y WHERE x.fk_per = y.fk_per AND a.fk_study = x.fk_study AND a.fk_study = y.fk_study AND y.fk_site_enrolling = a.fk_site AND y.PATPROT_STAT=1 AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''screening'')) AS  tot_screened,
(SELECT COUNT(*) FROM ER_PATSTUDYSTAT x,ER_PATPROT y WHERE x.fk_per = y.fk_per AND a.fk_study = x.fk_study AND a.fk_study = y.fk_study AND y.fk_site_enrolling = a.fk_site AND y.PATPROT_STAT=1 AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''scrfail'')) AS  tot_screenfail,
(SELECT COUNT(*) FROM ER_PATSTUDYSTAT x,ER_PATPROT y WHERE x.fk_per = y.fk_per AND a.fk_study = x.fk_study AND a.fk_study = y.fk_study AND y.fk_site_enrolling = a.fk_site AND y.PATPROT_STAT=1 AND  fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''followup'')) AS  tot_followup,
(SELECT COUNT(*) FROM ER_PATSTUDYSTAT x,ER_PATPROT y WHERE x.fk_per = y.fk_per AND a.fk_study = x.fk_study AND a.fk_study = y.fk_study AND y.fk_site_enrolling = a.fk_site AND y.PATPROT_STAT=1 AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''offstudy'')) AS  tot_offstudy,
(SELECT COUNT(*) FROM ER_PATSTUDYSTAT x,ER_PATPROT y WHERE x.fk_per = y.fk_per AND a.fk_study = x.fk_study AND a.fk_study = y.fk_study AND y.fk_site_enrolling = a.fk_site AND y.PATPROT_STAT=1 AND fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST 	WHERE codelst_type = ''patStatus'' AND codelst_subtyp = ''offtreat'')) AS  tot_offtreat
from vda.vda_v_studysites a where include_in_reports = ''Yes''
and fk_study IN (:studyId) and fk_site in (:orgId)';
			UPDATE 
				ER_REPORT 
			SET 
				REP_SQL_CLOB=update_sql				
			WHERE 				
				pk_report=255;
		END IF;
	END IF;		
	
	COMMIT;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,382,35,'35_update_er_report.sql',sysdate,'v10.1 #783');

commit; 
 