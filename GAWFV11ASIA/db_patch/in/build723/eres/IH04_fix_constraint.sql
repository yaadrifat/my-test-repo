set define off;
set serveroutput on;

DECLARE
  v_item_exists number := 0;  
BEGIN
  select count(*) into v_item_exists from USER_CONSTRAINTS where INDEX_NAME = 'UI_FLX_PAGEVER_U01';
  if (v_item_exists > 0) then
    execute immediate 'ALTER TABLE ERES.UI_FLX_PAGEVER DROP CONSTRAINT UI_FLX_PAGEVER_U01';
    dbms_output.put_line('Constraint dropped');
  else
    dbms_output.put_line('Constraint does not exist');
end if;
END;
/

DECLARE
  v_item_exists number := 0;  
BEGIN
  select count(*) into v_item_exists from USER_INDEXES where INDEX_NAME = 'UI_FLX_PAGEVER_U01';
  if (v_item_exists > 0) then
    execute immediate 'DROP INDEX UI_FLX_PAGEVER_U01';
    dbms_output.put_line('Index dropped');
  else
    dbms_output.put_line('Index does not exist');
end if;
END;
/

ALTER TABLE ERES.UI_FLX_PAGEVER ADD 
CONSTRAINT UI_FLX_PAGEVER_U01
 UNIQUE (PAGE_MOD_TABLE, PAGE_MOD_PK, PAGE_VER, PAGE_MINOR_VER)
 ENABLE
 VALIDATE;
 