DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where column_name = 'NWU_MEMBERADDTROLE'
      and table_name = 'ER_NWUSERS';

  if (v_column_exists = 0) then
     execute immediate 'ALTER TABLE ER_NWUSERS ADD NWU_MEMBERADDTROLE varchar2(200)';
  end if;
  commit;
end;
/

	INSERT INTO track_patches
	VALUES(seq_track_patches.nextval,387,4,'04_Add_col_er_nwuser.sql',sysdate,'v11 #788');
	commit;