Set define off;
create or replace procedure     SAVE_NETSTATUS (net_id IN Number,status_Id in number,o_net_id out number)
is
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SAVE_NETSTATUS', pLEVEL  => Plog.LFATAL);
v_subTyp VARCHAR2(50);
p_parent_id number;
begin

 BEGIN
 Plog.fatal(pCTX,'status_Id:'||status_Id) ;
 select codelst_subtyp into v_subTyp  from er_codelst where pk_codelst=status_Id and codelst_type='networkstat';

Plog.fatal(pCTX,'v_subTyp:'||v_subTyp) ;
Plog.fatal(pCTX,'net_id:'||net_id) ;


 for p_net in (SELECT * FROM er_nwsites  START WITH pk_nwsites= net_id CONNECT BY PRIOR pk_nwsites = FK_NWSITES )
 loop
 update  er_nwsites set nw_status= status_Id where pk_nwsites=p_net.pk_nwsites;
 end loop;
 
if v_subTyp='active' then
 select nvl(fk_nwsites,0) into p_parent_id  from er_nwsites where pk_nwsites=net_id;
 while  p_parent_id <>0 
 loop
 update  er_nwsites set nw_status= status_Id where pk_nwsites=p_parent_id;
 select nvl(fk_nwsites,0) into p_parent_id  from er_nwsites where pk_nwsites=p_parent_id;
 end loop;


 end if;
 
 o_net_id:=net_id;
 commit;
 
 END ;

end ;
/

	INSERT INTO track_patches
	VALUES(seq_track_patches.nextval,387,1,'01_Save_Netstatus.sql',sysdate,'v11 #788');
	commit;
