/////*********This readMe is specific to v11 build #798(compatible with Oracle 12c)**********////
*********************************************************************************************************************************************************************
In build#798 we have released Change requests on "ACC-39863 -- Network Tab Feb 14 2018 v0.8.3"  enhancement.  

The change requests are mentioned  from  points 18-23(from page no:15) in Gap Analysis and Mockups 9-14.

Velos has requested to release this on Priority basis to demonstrate progress to customer on Monday.

Keeping this into account we have tried to cover as many places as we can.

PLEASE NOTE THAT THIS BUILD IS JUST FOR VELOS TO DEMONSTRATE THE PROGRESS.

THERE ARE FEW ITEMS THAT TEAM HAS NOT COVERED AT ALL. THE COMPLETE ENHANCEMENT WILL BE DELIVERED IN #799. 

HERE ARE THE LIST OF ITEMS THAT  ARE NOT COVERED IN THIS BUILD.



SO WE REQUEST QA NOT TO VALIDATE THESE ITEMS.  (yellow color)

1. POINT (22).A. (v) (PARTIALLY IMPLEMENTED) NOT IMPLEMENTED PART IS : "Additionally, a user can select multiple organizations from the level panel, and then, let�s say, the user clicks on   icon on level 0, then all the selected organizations will be added on level1 within that network."

2. POINT (23).B. (i) and (ii)

3. POINT (23).C

Note: 

In Build #797 we have released "ACC-39863 -- Network Tab" document Point no. 16 and 17. 
During this implementation we have released db changes that will change the codelst_type to 'relnshipTyp' for Relationship Type and 'nwusersrole' for Network role.

We need to make relevant changes to existing data also.

Now, we are releasing the following data patches:

01_dataPatch_er_nwusers
02_datapatch_er_nwsites

