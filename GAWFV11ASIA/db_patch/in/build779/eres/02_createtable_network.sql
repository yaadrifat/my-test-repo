set define off;

DECLARE
  v_tab_exists number := 0;  
BEGIN
	
	Select count(*) into v_tab_exists from tabs  where TABLE_NAME = 'ER_NWSITES';
	if v_tab_exists = 0 then
execute immediate 'CREATE TABLE ERES.ER_NWSITES
  (
    PK_NWSITES      NUMBER NOT NULL ,
    FK_NWSITES      NUMBER ,
    FK_SITE         NUMBER NOT NULL ,
    NW_LEVEL        NUMBER NOT NULL ,
	RID               NUMBER,
    CREATOR           NUMBER,
    LAST_MODIFIED_BY  NUMBER,
    LAST_MODIFIED_DATE DATE DEFAULT NULL,
    CREATED_ON DATE DEFAULT sysdate,
    IP_ADD          VARCHAR2(15 BYTE),
    NW_MEMBERTYPE   NUMBER ,
    FK_NWSITES_MAIN NUMBER ,
    NW_STATUS       NUMBER
  )
  PCTFREE 10 PCTUSED 40 TABLESPACE ERES_USER LOGGING STORAGE
  (
    INITIAL 65536 NEXT 1048576 PCTINCREASE 0 MINEXTENTS 1 MAXEXTENTS 2147483645 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT
  )' ;
execute immediate 'COMMENT ON COLUMN ERES.ER_NWSITES.PK_NWSITES
IS
  ''Primary key''' ;
execute immediate 'COMMENT ON COLUMN ERES.ER_NWSITES.FK_NWSITES
IS
  ''Self Association for parent child relationship. FK to the network level this network site is a child of.''' ;
execute immediate 'COMMENT ON COLUMN ERES.ER_NWSITES.FK_SITE
IS
  ''Network Site''' ;
execute immediate 'COMMENT ON COLUMN ERES.ER_NWSITES.NW_LEVEL
IS
  ''Level of the Site in a Network. Level 0 indicates the main Network record.''' ;
execute immediate 'COMMENT ON COLUMN ERES.ER_NWSITES.RID
IS
  ''This column is used for the audit trail. The RID uniquely identifies the row in the database.''';
execute immediate 'COMMENT ON COLUMN ERES.ER_NWSITES.CREATOR
IS
  ''This column stores the name of user inserting the row in the table.''';
execute immediate 'COMMENT ON COLUMN ERES.ER_NWSITES.LAST_MODIFIED_BY
IS
  ''This column stores the name of user who last modified the row in the table.''';
execute immediate 'COMMENT ON COLUMN ERES.ER_NWSITES.LAST_MODIFIED_DATE
IS
  ''This column stores the date on which the row was last modified.''';
execute immediate 'COMMENT ON COLUMN ERES.ER_NWSITES.CREATED_ON
IS
  ''This column stores the date on which the row was  inserted.''';
execute immediate 'COMMENT ON COLUMN ERES.ER_NWSITES.IP_ADD
IS
  ''This column is used for Audit Trail. Stores the IP ADDRESS of the client machine.''';
execute immediate 'COMMENT ON COLUMN ERES.ER_NWSITES.NW_MEMBERTYPE
IS
  ''FK_CODELST to the membership roles or types of this site within the network.''' ;
execute immediate 'COMMENT ON COLUMN ERES.ER_NWSITES.FK_NWSITES_MAIN
IS
  ''Redundant column to point to the main network row for this network user. Points to the network site with level 0 of the 
corresponding network.''' ;

execute immediate 'COMMENT ON COLUMN ERES.ER_NWSITES.NW_STATUS
IS
  ''Fk_codelst to the status of Network Element.''' ;

execute immediate 'CREATE UNIQUE INDEX ERES.ER_NWSITES_PK ON ERES.ER_NWSITES
  (
    PK_NWSITES ASC
  )
  TABLESPACE ERES_USER PCTFREE 10 STORAGE
  (
    INITIAL 65536 NEXT 1048576 PCTINCREASE 0 MINEXTENTS 1 MAXEXTENTS 2147483645 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT
  )
  LOGGING' ;
execute immediate 'ALTER TABLE ERES.ER_NWSITES ADD CONSTRAINT ER_NWSITES_PK PRIMARY KEY ( PK_NWSITES ) USING INDEX ERES.ER_NWSITES_PK' ;
execute immediate 'ALTER TABLE ERES.ER_NWSITES ADD CONSTRAINT ER_NWSITES_FK1 FOREIGN KEY ( FK_SITE ) REFERENCES ERES.ER_SITE ( PK_SITE ) NOT DEFERRABLE' ;
execute immediate 'ALTER TABLE ERES.ER_NWSITES ADD CONSTRAINT ER_NWSITES_FK2 FOREIGN KEY ( FK_NWSITES ) REFERENCES ERES.ER_NWSITES ( PK_NWSITES ) NOT DEFERRABLE' ;
execute immediate 'CREATE SEQUENCE "ERES"."SEQ_ER_NWSITES" MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 100 ORDER NOCYCLE' ;
END IF;

v_tab_exists:= 0;
Select count(*) into v_tab_exists from tabs  where TABLE_NAME = 'ER_NWUSERS';
if v_tab_exists = 0 then
execute immediate 'CREATE TABLE ERES.ER_NWUSERS
  (
    PK_NWUSERS      NUMBER NOT NULL ,
    FK_NWSITES      NUMBER NOT NULL ,
    FK_USER         NUMBER NOT NULL ,
	RID               NUMBER,
    CREATOR           NUMBER,
    LAST_MODIFIED_BY  NUMBER,
    LAST_MODIFIED_DATE DATE DEFAULT NULL,
    CREATED_ON DATE DEFAULT sysdate,
    IP_ADD          VARCHAR2(15 BYTE),
    NWU_MEMBERTROLE NUMBER ,
    FK_NWSITES_MAIN NUMBER ,
    NW_LEVEL NUMBER
  )
  PCTFREE 10 PCTUSED 40 TABLESPACE ERES_USER LOGGING STORAGE
  (
    INITIAL 65536 NEXT 1048576 PCTINCREASE 0 MINEXTENTS 1 MAXEXTENTS 2147483645 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT
  )' ;
execute immediate 'COMMENT ON COLUMN ERES.ER_NWUSERS.PK_NWUSERS
IS
  ''Primary key''' ;
execute immediate 'COMMENT ON COLUMN ERES.ER_NWUSERS.FK_NWSITES
IS
  ''FK to the network level this network user is associated with.''' ;
execute immediate 'COMMENT ON COLUMN ERES.ER_NWUSERS.FK_USER
IS
  ''Network USER''' ;
execute immediate 'COMMENT ON COLUMN ERES.ER_NWUSERS.RID
IS
  ''This column is used for the audit trail. The RID uniquely identifies the row in the database.''';
execute immediate 'COMMENT ON COLUMN ERES.ER_NWUSERS.CREATOR
IS
  ''This column stores the name of user inserting the row in the table.''';
execute immediate 'COMMENT ON COLUMN ERES.ER_NWUSERS.LAST_MODIFIED_BY
IS
  ''This column stores the name of user who last modified the row in the table.''';
execute immediate 'COMMENT ON COLUMN ERES.ER_NWUSERS.LAST_MODIFIED_DATE
IS
  ''This column stores the date on which the row was last modified.''';
execute immediate 'COMMENT ON COLUMN ERES.ER_NWUSERS.CREATED_ON
IS
  ''This column stores the date on which the row was  inserted.''';
execute immediate 'COMMENT ON COLUMN ERES.ER_NWUSERS.IP_ADD
IS
  ''This column is used for Audit Trail. Stores the IP ADDRESS of the client machine.''';
execute immediate 'COMMENT ON COLUMN ERES.ER_NWUSERS.NWU_MEMBERTROLE
IS
  ''FK_CODELST to the membership roles of this user within the network.''' ;
execute immediate 'COMMENT ON COLUMN ERES.ER_NWUSERS.FK_NWSITES_MAIN
IS
  ''Redundant column to point to the main network row for this network user. Points to the network site with level 0 of the 
corresponding network.''' ;
execute immediate 'COMMENT ON COLUMN ERES.ER_NWUSERS.NW_LEVEL
IS
  ''Level to User Network.''' ;
execute immediate 'CREATE UNIQUE INDEX ERES.ER_NWUSERS ON ERES.ER_NWUSERS
  (
    PK_NWUSERS ASC
  )
  TABLESPACE ERES_USER PCTFREE 10 STORAGE
  (
    INITIAL 65536 NEXT 1048576 PCTINCREASE 0 MINEXTENTS 1 MAXEXTENTS 2147483645 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT
  )
  LOGGING' ;
execute immediate 'ALTER TABLE ERES.ER_NWUSERS ADD CONSTRAINT ER_NWUSERS PRIMARY KEY ( PK_NWUSERS ) USING INDEX ERES.ER_NWUSERS' ;
execute immediate 'ALTER TABLE ERES.ER_NWUSERS ADD CONSTRAINT ER_NWUSERS_FK1 FOREIGN KEY ( FK_USER ) REFERENCES ERES.ER_USER ( PK_USER ) NOT DEFERRABLE' ;
execute immediate 'ALTER TABLE ERES.ER_NWUSERS ADD CONSTRAINT ER_NWUSERS_FK2 FOREIGN KEY ( FK_NWSITES ) REFERENCES ERES.ER_NWSITES ( PK_NWSITES ) NOT DEFERRABLE' ;
  execute immediate 'CREATE SEQUENCE "ERES"."SEQ_ER_NWUSERS" MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 100 ORDER NOCYCLE' ;
	else
		p('no action needed. Form response Compare utility patch exists');
	end if;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,378,02,'02_createtable_network.sql',sysdate,'v10.1 #779');

commit;