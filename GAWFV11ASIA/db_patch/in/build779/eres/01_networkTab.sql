set define off;
DECLARE 
	table_check number;
	row_check number:=0;
	
BEGIN
	SELECT count(*)
	INTO table_check
	FROM USER_TABLES
	WHERE TABLE_NAME = 'ER_OBJECT_SETTINGS';
	
	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM er_object_settings
		WHERE object_type='T' AND
		      object_subtype='7' AND
			  object_name='acct_tab';
		
		IF (row_check = 1) then
				update  er_object_settings 
				
				set OBJECT_VISIBLE=1
        where object_type='T' AND
		      object_subtype='7' AND
			  object_name='acct_tab';
				
		END IF;
    dbms_output.put_line('Update');
	
	END IF;		
	
	COMMIT;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,378,0,'01_networkTab.sql',sysdate,'v10.1 #779');

commit;
