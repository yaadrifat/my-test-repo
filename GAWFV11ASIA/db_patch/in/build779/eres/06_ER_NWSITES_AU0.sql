set define off;

create or replace
TRIGGER "ERES"."ER_NWSITES_AU0" AFTER UPDATE ON ER_NWSITES FOR EACH ROW
DECLARE
  raid NUMBER(10);
  old_codetype VARCHAR2(200) ;
  new_codetype VARCHAR2(200) ;
  old_modby VARCHAR2(100) ;
  new_modby VARCHAR2(100) ;
  usr VARCHAR2(100);
BEGIN


SELECT seq_audit.NEXTVAL INTO raid FROM dual;

usr := Getuser(:NEW.last_modified_by);

Audit_Trail.record_transaction   (raid, 'ER_NWSITES', :OLD.rid, 'U', usr);



IF NVL(:OLD.PK_NWSITES,0) !=
     NVL(:NEW.PK_NWSITES,0) THEN
     Audit_Trail.column_update
       (raid, 'PK_NWSITES',
       :OLD.PK_NWSITES, :NEW.PK_NWSITES);
END IF;

IF NVL(:OLD.FK_NWSITES,0) !=
     NVL(:NEW.FK_NWSITES,0) THEN
     Audit_Trail.column_update
       (raid, 'FK_NWSITES',
       :OLD.FK_NWSITES, :NEW.FK_NWSITES);
END IF;

IF NVL(:OLD.NW_LEVEL,0) !=
     NVL(:NEW.NW_LEVEL,0) THEN
     Audit_Trail.column_update
       (raid, 'NW_LEVEL',
       :OLD.NW_LEVEL, :NEW.NW_LEVEL);
END IF;

IF NVL(:OLD.NW_MEMBERTYPE,0) !=
     NVL(:NEW.NW_MEMBERTYPE,0) THEN
     Audit_Trail.column_update
       (raid, 'NW_MEMBERTYPE',
       :OLD.NW_MEMBERTYPE, :NEW.NW_MEMBERTYPE);
END IF;

IF NVL(:OLD.FK_NWSITES_MAIN,0) !=
     NVL(:NEW.FK_NWSITES_MAIN,0) THEN
     Audit_Trail.column_update
       (raid, 'FK_NWSITES_MAIN',
       :OLD.FK_NWSITES_MAIN, :NEW.FK_NWSITES_MAIN);
END IF;

IF NVL(:OLD.NW_STATUS,0) !=
     NVL(:NEW.NW_STATUS,0) THEN
     Audit_Trail.column_update
       (raid, 'NW_STATUS',
       :OLD.NW_STATUS, :NEW.NW_STATUS);
END IF;

old_modby := '';
new_modby := '';

IF NVL(:OLD.last_modified_by,0) !=    NVL(:NEW.last_modified_by,0) THEN
   BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM ER_USER
     WHERE pk_user = :OLD.last_modified_by;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      old_modby := NULL;
  END;

 BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM ER_USER
     WHERE pk_user = :NEW.last_modified_by;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      new_modby := NULL;
 END;
	     Audit_Trail.column_update     (raid, 'LAST_MODIFIED_BY', old_modby, new_modby);
END IF;

IF NVL(:OLD.last_modified_date,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     Audit_Trail.column_update     (raid, 'LAST_MODIFIED_DATE',    to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
END IF;

IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN Audit_Trail.column_update    (raid, 'RID',  :OLD.rid, :NEW.rid);
END IF;

IF NVL(:OLD.fk_site,0) !=
     NVL(:NEW.fk_site,0) THEN
     Audit_Trail.column_update
       (raid, 'FK_SITE',
       :OLD.fk_site, :NEW.fk_site);
END IF;

END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,378,06,'06_ER_NWSITES_AU0.sql',sysdate,'v10.1 #779');

commit;