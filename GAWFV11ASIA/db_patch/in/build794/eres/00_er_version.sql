set define off;

UPDATE ER_CTRLTAB SET CTRL_VALUE = 'v11 #794' 
where CTRL_KEY = 'app_version' ; 

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,393,0,'00_er_version.sql',sysdate,'v11 #794');

commit; 

