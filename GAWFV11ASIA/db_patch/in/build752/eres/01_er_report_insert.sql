Declare
repCheck number;
Begin
select count(*) into repCheck from er_report where REP_TYPE='rep_calendar' and REP_NAME='Study Calendar Report';
if(repCheck=0) then
insert into  er_report (pk_report,REP_NAME,rep_desc,REP_TYPE,rep_hide,fk_account,GENERATE_XML)
VALUES(189,'Study Calendar Report','Study Calendar Report','rep_calendar' , 'N',0,0);
Commit;
Else 
select pk_report into repCheck from er_report where REP_TYPE='rep_calendar' and REP_NAME='Study Calendar Report';
delete from er_report where pk_report=repCheck;
commit;
insert into  er_report (pk_report,REP_NAME,rep_desc,REP_TYPE,rep_hide,fk_account,GENERATE_XML)
VALUES(189,'Study Calendar Report','Study Calendar Report','rep_calendar' , 'N',0,0);
Commit;
End if;
End;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,351,1,'01_er_report_insert.sql',sysdate,'v10 #752');

commit;
