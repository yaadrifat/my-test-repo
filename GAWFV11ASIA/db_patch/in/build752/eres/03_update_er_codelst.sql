set define off;
declare 
codeExists number;
begin
select count(*) into codeExists from er_codelst where codelst_type='budtype' and codelst_subtyp='cost_type';
if(codeExists>0) then
update er_codelst set codelst_seq=1 where codelst_type='budtype' and codelst_subtyp='cost_type';
commit;
end if;
select count(*) into codeExists from er_codelst where codelst_type='budtype' and codelst_subtyp='coverage_type';
if(codeExists>0) then
update er_codelst set codelst_seq=2,codelst_hide='Y' where codelst_type='budtype' and codelst_subtyp='coverage_type';
commit;
end if;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,351,3,'03_update_er_codelst.sql',sysdate,'v10 #752');

commit;