set define off;

DECLARE
  v_column_exists number := 0;  
BEGIN
  select count(*) into v_column_exists from user_tab_cols 
  where column_name = 'WA_ONCLICK_JSON' and table_name = 'WORKFLOW_ACTIVITY';

  if (v_column_exists = 0) then
      execute immediate 'alter table WORKFLOW_ACTIVITY add (WA_ONCLICK_JSON VARCHAR2(4000))';
  end if;
end;
/
