set define off;

UPDATE ER_CTRLTAB SET CTRL_VALUE = 'v10 #754' 
where CTRL_KEY = 'app_version' ; 

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,353,0,'00_er_version.sql',sysdate,'v10 #754');

commit;

