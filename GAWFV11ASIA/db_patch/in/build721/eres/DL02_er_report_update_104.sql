set define off;

UPDATE er_report SET rep_sql = 'select  (select grp_name from er_grps where pk_grp = fk_grp_default) as group_name,
(SELECT usr_lastname || '', '' || usr_firstname FROM ER_USER WHERE pk_user=fk_user) AS user_name,
COUNT(fk_user) AS login_count,
ROUND(SUM(TRUNC((86400*(logout_time - login_time))/60) -  60*(TRUNC(((86400*(logout_time - login_time))/60)/60))),2) AS total_login_duration,
ROUND(AVG(TRUNC((86400*(logout_time - login_time))/60) -  60*(TRUNC(((86400*(logout_time - login_time))/60)/60))),2) AS avg_login_duration,
TO_CHAR((SELECT login_time FROM ER_USERSESSION b WHERE fk_user = a.fk_user
  AND pk_usersession =  (SELECT MAX(pk_usersession) FROM ER_USERSESSION WHERE fk_user = b.fk_user AND success_flag = 1)),''dd-Mon-yyyy   [hh:mi AM]'') AS last_loggedin_date,
(SELECT ip_add FROM ER_USERSESSION b WHERE fk_user = a.fk_user AND pk_usersession = (SELECT MAX(pk_usersession) FROM ER_USERSESSION WHERE fk_user = b.fk_user)) AS last_accessed_from
FROM ER_USERSESSION a, ER_USER 
WHERE  pk_user = fk_user AND
success_flag = 1 AND
logout_time IS NOT NULL AND
TRUNC(login_time) BETWEEN to_date(''~2'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND to_date(''~3'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND
fk_user IN (SELECT pk_user FROM ER_USER WHERE fk_account = ~1)
GROUP BY fk_grp_default,fk_user
ORDER BY group_name,user_name '
    WHERE pk_report = 104;
    COMMIT;
UPDATE er_report SET rep_sql_clob = 'select  (select grp_name from er_grps where pk_grp = fk_grp_default) as group_name,
(SELECT usr_lastname || '', '' || usr_firstname FROM ER_USER WHERE pk_user=fk_user) AS user_name,
COUNT(fk_user) AS login_count,
ROUND(SUM(TRUNC((86400*(logout_time - login_time))/60) -  60*(TRUNC(((86400*(logout_time - login_time))/60)/60))),2) AS total_login_duration,
ROUND(AVG(TRUNC((86400*(logout_time - login_time))/60) -  60*(TRUNC(((86400*(logout_time - login_time))/60)/60))),2) AS avg_login_duration,
TO_CHAR((SELECT login_time FROM ER_USERSESSION b WHERE fk_user = a.fk_user
  AND pk_usersession =  (SELECT MAX(pk_usersession) FROM ER_USERSESSION WHERE fk_user = b.fk_user AND success_flag = 1)),''dd-Mon-yyyy   [hh:mi AM]'') AS last_loggedin_date,
(SELECT ip_add FROM ER_USERSESSION b WHERE fk_user = a.fk_user AND pk_usersession = (SELECT MAX(pk_usersession) FROM ER_USERSESSION WHERE fk_user = b.fk_user)) AS last_accessed_from
FROM ER_USERSESSION a, ER_USER 
WHERE  pk_user = fk_user AND
success_flag = 1 AND
logout_time IS NOT NULL AND
TRUNC(login_time) BETWEEN to_date(''~2'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND to_date(''~3'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND
fk_user IN (SELECT pk_user FROM ER_USER WHERE fk_account = ~1)
GROUP BY fk_grp_default,fk_user
ORDER BY group_name,user_name '
    WHERE pk_report = 104;
    COMMIT;
/
