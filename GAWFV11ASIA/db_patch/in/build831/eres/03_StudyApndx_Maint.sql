declare
begin
for filewobackup in (select pk_studyapndx from er_studyapndx where stamp_flag=1 and PRESTAMP_FILEOBJ is null)
loop
update er_studyapndx set PRESTAMP_FILEOBJ=STUDYAPNDX_FILEOBJ, PRESTAMP_FILEURI=STUDYAPNDX_URI where pk_studyapndx=filewobackup.pk_studyapndx;
commit;
end loop;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,430,3,'03_StudyApndx_Maint.sql',sysdate,'v11 #831');
commit;
/