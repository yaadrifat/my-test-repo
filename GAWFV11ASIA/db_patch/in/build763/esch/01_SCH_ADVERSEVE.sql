set define off;
alter table	sch_adverseve modify ( ae_name    varchar2(1000) );
commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,362,0,'01_SCH_ADVERSEVE.sql',sysdate,'v10 #763');

commit;
