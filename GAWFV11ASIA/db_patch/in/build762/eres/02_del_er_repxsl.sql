set define off;

delete  from  er_repxsl where pk_repxsl in (106,107,131,132,180);
commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,361,2,'02_del_er_repxsl.sql',sysdate,'v10 #762');

commit;

