set define off;

CREATE or replace FUNCTION F_IS_NUMBER (p_string IN VARCHAR2)
   RETURN INT
IS
   v_new_num NUMBER;
BEGIN
   v_new_num := TO_NUMBER(p_string);
   RETURN 1;
EXCEPTION
WHEN VALUE_ERROR THEN
   RETURN 0;
END F_IS_NUMBER;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,360,1,'01_F_IS_NUMBER.sql',sysdate,'v10 #761');

commit;