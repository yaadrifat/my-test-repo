declare
v_exists number:=0;
begin
SELECT count(*) into v_exists FROM user_triggers WHERE TRIGGER_NAME = 'ER_STATWORKFLOW_AU0';
if v_exists!=0 then
execute immediate('drop trigger ER_STATWORKFLOW_AU0');
end if;
end;
/

update workflow_activity set wa_name='select ''Protocol Definition'' from dual'
where fk_activityid=(select pk_activity from activity where activity_name='protocolDefinition') and
fk_workflowid=(SELECT PK_WORKFLOW FROM WORKFLOW WHERE WORKFLOW_TYPE = 'STUDY_INITIATION');

update workflow_activity set wa_name='select ''CRC Review'' from dual'
where fk_activityid=(select pk_activity from activity where activity_name='CRC_Review') and
fk_workflowid=(SELECT PK_WORKFLOW FROM WORKFLOW WHERE WORKFLOW_TYPE = 'STUDY_INITIATION');

update workflow_activity set wa_name='select codelst_desc from er_codelst where CODELST_TYPE = ''studystat'' AND CODELST_SUBTYP = ''app_CHR'''
where fk_activityid=(select pk_activity from activity where activity_name='IRB_Review') and
fk_workflowid=(SELECT PK_WORKFLOW FROM WORKFLOW WHERE WORKFLOW_TYPE = 'STUDY_INITIATION');

--Active/Enrolling
update workflow_activity set wa_name='select codelst_desc from er_codelst where CODELST_TYPE = ''studystat'' AND CODELST_SUBTYP = ''active'''
where fk_activityid=(select pk_activity from activity where activity_name='protActivation') and
fk_workflowid=(SELECT PK_WORKFLOW FROM WORKFLOW WHERE WORKFLOW_TYPE = 'STUDY_ACTIVATION');

--Permanant closure
update workflow_activity set wa_name='select codelst_desc from er_codelst where CODELST_TYPE = ''studystat'' AND CODELST_SUBTYP = ''prmnt_cls'''
where fk_activityid=(select pk_activity from activity where activity_name='prmnt_cls') and
fk_workflowid=(SELECT PK_WORKFLOW FROM WORKFLOW WHERE WORKFLOW_TYPE = 'STUDY_CLOSURE');

update workflow_activity set wa_name='select codelst_desc from er_codelst where CODELST_TYPE = ''studystat'' AND CODELST_SUBTYP = ''cdainrev'''
where fk_activityid=(select pk_activity from activity where activity_name='cdainrev') and
fk_workflowid=(SELECT PK_WORKFLOW FROM WORKFLOW WHERE WORKFLOW_TYPE = 'STUDY_INITIATION');

update workflow_activity set wa_name='select codelst_desc from er_codelst where CODELST_TYPE = ''studystat'' AND CODELST_SUBTYP = ''cdasignoff'''
where fk_activityid=(select pk_activity from activity where activity_name='cdasignoff') and
fk_workflowid=(SELECT PK_WORKFLOW FROM WORKFLOW WHERE WORKFLOW_TYPE = 'STUDY_INITIATION');

update workflow_activity set wa_name='select codelst_desc from er_codelst where CODELST_TYPE = ''studystat'' AND CODELST_SUBTYP = ''cainrev'''
where fk_activityid=(select pk_activity from activity where activity_name='cainrev') and
fk_workflowid=(SELECT PK_WORKFLOW FROM WORKFLOW WHERE WORKFLOW_TYPE = 'STUDY_INITIATION');

update workflow_activity set wa_name='select codelst_desc from er_codelst where CODELST_TYPE = ''studystat'' AND CODELST_SUBTYP = ''casignoff'''
where fk_activityid=(select pk_activity from activity where activity_name='casignoff') and
fk_workflowid=(SELECT PK_WORKFLOW FROM WORKFLOW WHERE WORKFLOW_TYPE = 'STUDY_INITIATION');

update workflow_activity set wa_name='select codelst_desc from er_codelst where CODELST_TYPE = ''studystat'' AND CODELST_SUBTYP = ''bgtInreview'''
where fk_activityid=(select pk_activity from activity where activity_name='bgtInreview') and
fk_workflowid=(SELECT PK_WORKFLOW FROM WORKFLOW WHERE WORKFLOW_TYPE = 'STUDY_INITIATION');

update workflow_activity set wa_name='select codelst_desc from er_codelst where CODELST_TYPE = ''studystat'' AND CODELST_SUBTYP = ''bgtsignoff'''
where fk_activityid=(select pk_activity from activity where activity_name='bgtsignoff') and
fk_workflowid=(SELECT PK_WORKFLOW FROM WORKFLOW WHERE WORKFLOW_TYPE = 'STUDY_INITIATION');

update workflow_activity set wa_name='select codelst_desc from er_codelst where CODELST_TYPE = ''studystat'' AND CODELST_SUBTYP = ''irb_init_subm'''
where fk_activityid=(select pk_activity from activity where activity_name='irb_init_subm') and
fk_workflowid=(SELECT PK_WORKFLOW FROM WORKFLOW WHERE WORKFLOW_TYPE = 'STUDY_INITIATION');

update workflow_activity set wa_name='select codelst_desc from er_codelst where CODELST_TYPE = ''studystat'' AND CODELST_SUBTYP = ''active_cls'''
where fk_activityid=(select pk_activity from activity where activity_name='active_cls') and
fk_workflowid=(SELECT PK_WORKFLOW FROM WORKFLOW WHERE WORKFLOW_TYPE = 'STUDY_ACTIVATION');

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,365,4,'04_Workflow_Name.sql',sysdate,'v10 #766');

commit;