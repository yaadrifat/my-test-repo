set define off;
declare
countFlag number;
v_cnt number;
begin
SELECT COUNT(1) INTO countFlag  FROM USER_TABLES WHERE TABLE_NAME='ER_BROWSERCONF';
if (countFlag > 0) then
SELECT COUNT(*) INTO v_cnt  from er_browserconf  WHERE BROWSERCONF_COLNAME ='INV_RIGHTS';
if v_cnt=0 then
insert into er_browserconf (PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ) values(SEQ_ER_BROWSERCONF.nextval,2,'INV_RIGHTS',27);
commit;
end if;
end if;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,365,1,'01_inv_rights_config.sql',sysdate,'v10 #766');

commit;