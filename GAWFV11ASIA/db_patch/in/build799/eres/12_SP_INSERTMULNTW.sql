set define off;
create or replace
PROCEDURE SP_INSERTMULNTW(sitesIds IN ARRAY_STRING,NT_LEVEL IN number,
USER_ID IN number,IP IN varchar2,netTypId IN number,maninNtw IN number,ntw IN number,ntwStaus IN number,
o_ret_number OUT NUMBER)
AS 
i number;
v_pk_nwsites number;
v_count number;
--pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SP_INSERTMULNTW', pLEVEL  => Plog.LFATAL);
BEGIN
v_count:=sitesIds.count;
i:=1;
 while(i<=v_count)loop
 
 SELECT SEQ_ER_NWSITES.NEXTVAL
			INTO v_pk_nwsites FROM dual ;
      
  insert into er_nwsites (PK_NWSITES,FK_NWSITES,FK_SITE,NW_LEVEL,CREATOR,IP_ADD,NW_MEMBERTYPE,FK_NWSITES_MAIN,NW_STATUS) values(v_pk_nwsites,ntw,sitesIds(i),NT_LEVEL,USER_ID,IP,netTypId,maninNtw,ntwStaus); 
  i:=i+1;
  end loop;
  
  if ntw is null then
 o_ret_number:=v_pk_nwsites;
 else
 o_ret_number:=maninNtw;
  end if;
  COMMIT;
END SP_INSERTMULNTW;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,398,12,'12_SP_INSERTMULNTW.sql',sysdate,'v11 #799');

commit; 
