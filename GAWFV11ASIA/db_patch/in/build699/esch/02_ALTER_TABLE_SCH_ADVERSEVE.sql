SET Define OFF;
DECLARE
  v_column_exists NUMBER := 0;
BEGIN

  SELECT COUNT(*)
  INTO v_column_exists
  FROM user_tab_cols
  WHERE TABLE_NAME    = 'SCH_ADVERSEVE'
  AND COLUMN_NAME     = 'AE_CATEGORY';

  IF (v_column_exists = 0) THEN
    EXECUTE immediate 'ALTER TABLE SCH_ADVERSEVE ADD(AE_CATEGORY VARCHAR2(500))';
    dbms_output.put_line('Column AE_CATEGORY added to SCH_ADVERSEVE');
  ELSE
    dbms_output.put_line('Column AE_CATEGORY already exists in table SCH_ADVERSEVE');
  END IF;
  
 
  
  SELECT COUNT(*)
  INTO v_column_exists
  FROM user_tab_cols
  WHERE TABLE_NAME    = 'SCH_ADVERSEVE'
  AND COLUMN_NAME     = 'AE_TOXICITY';
  
  IF (v_column_exists = 0) THEN
    EXECUTE immediate 'ALTER TABLE SCH_ADVERSEVE ADD(AE_TOXICITY VARCHAR2(500))';
    dbms_output.put_line('Column AE_TOXICITY added to SCH_ADVERSEVE');
  ELSE
    dbms_output.put_line('Column AE_TOXICITY already exists in table SCH_ADVERSEVE');
  END IF;
   
  
  SELECT COUNT(*)
  INTO v_column_exists
  FROM user_tab_cols
  WHERE TABLE_NAME    = 'SCH_ADVERSEVE'
  AND COLUMN_NAME     = 'AE_TOXICTY_DESC';
  IF (v_column_exists = 0) THEN
    EXECUTE immediate 'ALTER TABLE SCH_ADVERSEVE ADD(AE_TOXICTY_DESC VARCHAR2(4000))';
    dbms_output.put_line('Column AE_TOXICTY_DESC added to SCH_ADVERSEVE');
  ELSE
    dbms_output.put_line('Column AE_TOXICTY_DESC already exists in table SCH_ADVERSEVE');
  END IF;
    
  SELECT COUNT(*)
  INTO v_column_exists
  FROM user_tab_cols
  WHERE TABLE_NAME    = 'SCH_ADVERSEVE'
  AND COLUMN_NAME     = 'AE_GRADE_DESC';
  
  IF (v_column_exists = 0) THEN
    EXECUTE immediate 'ALTER TABLE SCH_ADVERSEVE ADD(AE_GRADE_DESC VARCHAR2(4000))';
    dbms_output.put_line('Column AE_GRADE_DESC added to SCH_ADVERSEVE');
  ELSE
    dbms_output.put_line('Column AE_GRADE_DESC already exists in table SCH_ADVERSEVE');
  END IF;
  
END;
/

COMMENT ON COLUMN SCH_ADVERSEVE.AE_CATEGORY IS 'Stores Adverse Event category';
COMMENT ON COLUMN SCH_ADVERSEVE.AE_TOXICITY IS 'Stores Adverse Event TOXICITY';
COMMENT ON COLUMN SCH_ADVERSEVE.AE_TOXICTY_DESC IS 'Stores Adverse Event TOXICITY description';
COMMENT ON COLUMN SCH_ADVERSEVE.AE_GRADE_DESC IS 'Stores Adverse Event Grade description';
commit;