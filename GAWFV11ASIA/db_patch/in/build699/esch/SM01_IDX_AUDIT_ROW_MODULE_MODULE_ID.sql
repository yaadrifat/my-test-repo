set define off;

declare
v_index_exists NUMBER := 0;
begin
	select count(*) into v_index_exists from SYS.user_indexes where table_owner = 'ESCH' and lower(table_name) like 'audit_row_module' and index_name = 'IDX_AUDIT_ROW_MODULE_MODULE_ID';
	
	if (v_index_exists = 0) then
		execute immediate 'CREATE INDEX IDX_AUDIT_ROW_MODULE_MODULE_ID ON AUDIT_ROW_MODULE (MODULE_ID ASC)';
	end if;
end;
/
