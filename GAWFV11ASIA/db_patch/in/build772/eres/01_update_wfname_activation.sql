update WORKFLOW set WORKFLOW_NAME='Study Activation', WORKFLOW_DESC='Study Activation' where WORKFLOW_TYPE='STUDY_ACTIVATION';
commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,371,1,'01_update_wfname_activation.sql',sysdate,'v10 #772');

commit;