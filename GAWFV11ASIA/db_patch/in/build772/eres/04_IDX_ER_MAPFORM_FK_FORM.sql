set define off;

declare
v_indexName VARCHAR2(200) := '';
begin
	select a1.Index_name INTO v_indexName from USER_IND_COLUMNS a1 where a1.COLUMN_NAME='PK_MP' and a1.table_name = 'ER_MAPFORM' 
	and a1.Index_name not in (select b1.Index_name from USER_IND_COLUMNS b1 where (a1.table_name= b1.table_name and a1.index_name = b1.index_name) and b1.COLUMN_NAME <> 'PK_MP');
	
	EXCEPTION WHEN NO_DATA_FOUND THEN
		v_indexName := v_indexName;

	if (length(v_indexName) IS NULL) then
		
EXECUTE IMMEDIATE 'CREATE UNIQUE INDEX ERES.IDX_ER_MAPFORM_PK_MP ON ERES.ER_MAPFORM (PK_MP) LOGGING TABLESPACE ERES_USER NOPARALLEL';

end if;
	
end;
/


declare
v_indexName VARCHAR2(200) := '';
begin
	select a1.Index_name INTO v_indexName from USER_IND_COLUMNS a1 where a1.COLUMN_NAME='FK_FORM' and a1.table_name = 'ER_MAPFORM' 
	and a1.Index_name not in (select b1.Index_name from USER_IND_COLUMNS b1 where (a1.table_name= b1.table_name and a1.index_name = b1.index_name) and b1.COLUMN_NAME <> 'FK_FORM');
	
	EXCEPTION WHEN NO_DATA_FOUND THEN
		v_indexName := v_indexName;

	if (length(v_indexName) IS NULL) then
		
EXECUTE IMMEDIATE 'CREATE INDEX ERES.IDX_ER_MAPFORM_FK_FORM ON ERES.ER_MAPFORM (FK_FORM) LOGGING TABLESPACE ERES_USER NOPARALLEL';

end if;
	
end;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,371,4,'04_IDX_ER_MAPFORM_FK_FORM.sql',sysdate,'v10 #772');

commit;