  set define off;
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_LKPVIEW where LKPVIEW_NAME='More Spec Details' and FK_LKPLIB =(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Spec Details' and LKPTYPE_TYPE='dyn_a');
  if (v_record_exists = 0) then
INSERT
INTO ER_LKPVIEW
  (
    PK_LKPVIEW,
    LKPVIEW_NAME,
    FK_LKPLIB,
    LKPVIEW_FILTER,
    LKPVIEW_KEYWORD
  )
  VALUES
  (
    (select (max(PK_LKPVIEW))+1 from ER_LKPVIEW),
    'More Spec Details',
    (select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC='More Spec Details' and LKPTYPE_TYPE='dyn_a'),
    'fk_account=[:ACCID]',
    'morespecdetail'
  );
end if;
end;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,436,7,'07_ER_LKPVIEW.sql',sysdate,'v11 #837');
/
commit;	