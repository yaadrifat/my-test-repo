DECLARE
  v_obj_subtyp number := 0;
  
BEGIN
  SELECT COUNT(*) INTO v_obj_subtyp 
  FROM er_object_settings 
  WHERE object_subtype='1' and object_type = 'T' and object_name='milestone_tab';
  
  IF (v_obj_subtyp != 0) THEN
    update er_object_settings 
    set object_displaytext='Financials' 
    where object_name='milestone_tab' and object_type = 'T' and object_subtype='1';
  END IF;
  commit;
  
END;
/

DECLARE
  v_obj_subtyp number := 0;
  
BEGIN
  SELECT COUNT(*) INTO v_obj_subtyp 
  FROM er_object_settings 
  WHERE object_subtype='12' and object_type = 'T' and object_name='study_tab';
  
  IF (v_obj_subtyp != 0) THEN
    update er_object_settings 
    set object_displaytext='Financials' 
    where object_name='study_tab' and object_type = 'T'and object_subtype='12';
  END IF;
  commit;
  
END;
/

DECLARE
  v_obj_subtyp number := 0;
  
BEGIN
  SELECT COUNT(*) INTO v_obj_subtyp 
  FROM er_ctrltab
  WHERE ctrl_value='MILEST' and ctrl_key='app_rights';
  
  IF (v_obj_subtyp != 0) THEN
    update er_ctrltab
    set ctrl_desc='Financials' 
    where ctrl_value='MILEST' and ctrl_key='app_rights';
  END IF;
  commit;
  
END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,357,1,'01_update_objSettctrltab.sql',sysdate,'v10 #758');

commit;
