set define off;
DECLARE
  row_check   NUMBER;
  table_check NUMBER;

BEGIN
  SELECT COUNT(*)
  INTO table_check
  FROM user_tables
  WHERE table_name='ER_OBJECT_SETTINGS';
 
  IF(table_check   >0) THEN
    SELECT COUNT(*)
    INTO row_check
    FROM ER_OBJECT_SETTINGS
    WHERE OBJECT_NAME='network_document_tab' and OBJECT_SUBTYPE='1';
    
    IF(row_check =0) THEN
      INSERT
      INTO ER_OBJECT_SETTINGS
        (
		  PK_OBJECT_SETTINGS,
		  OBJECT_TYPE,
		  OBJECT_SUBTYPE,
		  OBJECT_NAME,
		  OBJECT_SEQUENCE,
		  OBJECT_VISIBLE,
		  OBJECT_DISPLAYTEXT,
		  FK_ACCOUNT
        )
        VALUES
        (
          SEQ_ER_OBJECT_SETTINGS.nextval,
		  'T',
		  '1',
		  'network_document_tab',
		  1,
		  1,
		  'Organization Documents',
		  0
        );
    END IF;
  END IF;
  
  IF(table_check   >0) THEN
    SELECT COUNT(*)
    INTO row_check
    FROM ER_OBJECT_SETTINGS
    WHERE OBJECT_NAME='network_document_tab' and OBJECT_SUBTYPE='2';
  
	IF(row_check =0) THEN
      INSERT
      INTO ER_OBJECT_SETTINGS
        (
		  PK_OBJECT_SETTINGS,
		  OBJECT_TYPE,
		  OBJECT_SUBTYPE,
		  OBJECT_NAME,
		  OBJECT_SEQUENCE,
		  OBJECT_VISIBLE,
		  OBJECT_DISPLAYTEXT,
		  FK_ACCOUNT
        )
        VALUES
        (
          SEQ_ER_OBJECT_SETTINGS.nextval,
		  'T',
		  '2',
		  'network_document_tab',
		  2,
		  1,
		  'Level Documents',
		  0
        );
    END IF;
  END IF;
  
commit;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,387,5,'05_er_object_settings.sql',sysdate,'v11 #787');

commit;
