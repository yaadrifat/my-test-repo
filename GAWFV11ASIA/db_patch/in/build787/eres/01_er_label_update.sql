DECLARE
	table_check number;
	row_check number;
	update_sql clob;

BEGIN
	SELECT count(*)
	INTO table_check
	FROM user_tables
	WHERE TABLE_NAME = 'ER_LABEL_TEMPLATES';
	
	IF (table_check > 0) then
		SELECT count(*)
		INTO row_check
		FROM er_label_templates
		WHERE template_type = 'specimen' and template_subtype='default';
		
		IF(row_check>0) then
			update_sql:='<?xml version="1.0" encoding="UTF-8"?>
<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
<fo:layout-master-set>
	<fo:simple-page-master master-name="eSample" page-width="3in" page-height="2.5in"
		margin-top="0.1in" margin-bottom="0.05in" margin-left="0.1in" margin-right="0.1in">
	<fo:region-body/>
	</fo:simple-page-master>
</fo:layout-master-set>
<fo:page-sequence master-reference="eSample" initial-page-number="1">
	<fo:flow flow-name="xsl-region-body">
	<fo:block font-size="12pt"
				font-family="sans-serif"
				line-height="15pt"
				space-after.optimum="3pt"
				text-align="center">
			{VEL_SAMPLE_TYPE}
	</fo:block>
	<fo:block text-align="center">{VEL_SPEC_ID}</fo:block>
	<fo:block text-align="center">Pat# {VEL_PATCODE}</fo:block>
	<fo:block text-align="center">Study Number# {VEL_STUDY_NUMBER}</fo:block>
	<fo:block text-align="center">Study Phase# {VEL_STUDY_PHASE}</fo:block>
	<fo:block text-align="center">CD: {VEL_COLL_DATE}</fo:block>
	<fo:block font-size="12pt"
				font-family="sans-serif"
				line-height="15pt"
				space-after.optimum="0pt"
				text-align="center">
		<fo:external-graphic src="file:{VEL_BARCODE}"
			width="116px" height="66px"/>
	</fo:block>
	</fo:flow>
</fo:page-sequence>
</fo:root>';
		
			  Update er_label_templates set TEMPLATE_FORMAT=update_sql where template_type='specimen' and template_subtype='default';
		End if;

		SELECT count(*)
		INTO row_check
		FROM er_label_templates
		WHERE template_type = 'specimen' and template_subtype='pdf417';
		
		IF(row_check>0) then
			update_sql:='<?xml version="1.0" encoding="UTF-8"?>
<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
<fo:layout-master-set>
	<fo:simple-page-master master-name="eSample" page-width="3in" page-height="2.5in"
		margin-top="0.1in" margin-bottom="0.05in" margin-left="0.1in" margin-right="0.1in">
	<fo:region-body/>
	</fo:simple-page-master>
</fo:layout-master-set>
<fo:page-sequence master-reference="eSample" initial-page-number="1">
	<fo:flow flow-name="xsl-region-body">
	<fo:block font-size="12pt"
				font-family="sans-serif"
				line-height="15pt"
				space-after.optimum="3pt"
				text-align="center">
			{VEL_SAMPLE_TYPE}
	</fo:block>
	<fo:block text-align="center">{VEL_SPEC_ID}</fo:block>
	<fo:block text-align="center">Pat# {VEL_PATCODE}</fo:block>
	<fo:block text-align="center">Study Number# {VEL_STUDY_NUMBER}</fo:block>
	<fo:block text-align="center">Study Phase# {VEL_STUDY_PHASE}</fo:block>
	<fo:block text-align="center">CD: {VEL_COLL_DATE}</fo:block>
	<fo:block font-size="12pt"
				font-family="sans-serif"
				line-height="15pt"
				space-after.optimum="0pt"
				text-align="center">
		<fo:external-graphic src="file:{VEL_PDF417_BARCODE}"
			width="116px" height="66px"/>
	</fo:block>
	</fo:flow>
</fo:page-sequence>
</fo:root>';   
		
			Update er_label_templates set TEMPLATE_FORMAT=update_sql where template_type='specimen' and template_subtype='pdf417';
		End if;

		SELECT count(*)
		INTO row_check
		FROM er_label_templates
		WHERE template_type = 'specimen' and template_subtype='datamatrix';
		
		IF(row_check>0) then
			update_sql:='<?xml version="1.0" encoding="UTF-8"?>
<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
<fo:layout-master-set>
	<fo:simple-page-master master-name="eSample" page-width="3in" page-height="2.5in"
		margin-top="0.1in" margin-bottom="0.05in" margin-left="0.1in" margin-right="0.1in">
	<fo:region-body/>
	</fo:simple-page-master>
</fo:layout-master-set>
<fo:page-sequence master-reference="eSample" initial-page-number="1">
	<fo:flow flow-name="xsl-region-body">
	<fo:block font-size="12pt"
				font-family="sans-serif"
				line-height="15pt"
				space-after.optimum="3pt"
				text-align="center">
			{VEL_SAMPLE_TYPE}
	</fo:block>
	<fo:block text-align="center">{VEL_SPEC_ID}</fo:block>
	<fo:block text-align="center">Pat# {VEL_PATCODE}</fo:block>
	<fo:block text-align="center">Study Number# {VEL_STUDY_NUMBER}</fo:block>
	<fo:block text-align="center">Study Phase# {VEL_STUDY_PHASE}</fo:block>
	<fo:block text-align="center">CD: {VEL_COLL_DATE}</fo:block>
	<fo:block font-size="12pt"
				font-family="sans-serif"
				line-height="15pt"
				space-after.optimum="0pt"
				text-align="center">
		<fo:external-graphic src="file:{VEL_DATA_MATRIX_BARCODE}"
			width="116px" height="66px"/>
	</fo:block>
	</fo:flow>
</fo:page-sequence>
</fo:root>';
		
			Update er_label_templates set TEMPLATE_FORMAT=update_sql where template_type='specimen' and template_subtype='datamatrix';
		End if;
	End if;
commit;
End;
/
		
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,387,1,'01_er_label_update.sql',sysdate,'v11 #787');

commit;
		