SET DEFINE OFF;
DECLARE
v_cnt NUMBER;
BEGIN

SELECT COUNT(*) INTO v_cnt FROM ER_OBJECT_SETTINGS WHERE OBJECT_SUBTYPE='12' AND OBJECT_NAME='pat_tab';

IF(v_cnt<1) THEN

INSERT
INTO ER_OBJECT_SETTINGS
  (
    PK_OBJECT_SETTINGS,
    OBJECT_TYPE,
    OBJECT_SUBTYPE,
    OBJECT_NAME,
    OBJECT_SEQUENCE,
    OBJECT_VISIBLE,
    OBJECT_DISPLAYTEXT,
    FK_ACCOUNT
  )
  VALUES
  (
    SEQ_ER_OBJECT_SETTINGS.nextval,
    'T',
    '12',
    'pat_tab',
    6,
	1,
    'Specimens',
    0
  );
  Commit;
END IF;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,387,4,'04_er_obj_settings.sql',sysdate,'v11 #787');

commit;
