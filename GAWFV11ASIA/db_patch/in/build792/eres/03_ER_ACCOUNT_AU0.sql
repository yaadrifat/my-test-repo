create or replace TRIGGER "ERES"."ER_ACCOUNT_AU0" AFTER UPDATE ON ER_ACCOUNT FOR EACH ROW
declare
  raid number(10);
  old_modby varchar2(100) ;
  new_modby varchar2(100) ;
  usr varchar2(100);

begin
  select seq_audit.nextval into raid from dual;

  usr := getuser(:new.last_modified_by);

  audit_trail.record_transaction
    (raid, 'ER_ACCOUNT', :old.rid, 'U', usr);
  if nvl(:old.pk_account,0) !=
     NVL(:new.pk_account,0) then
     audit_trail.column_update
       (raid, 'PK_ACCOUNT',
       :old.pk_account, :new.pk_account);
  end if;
  if nvl(:old.ac_type,' ') !=
     NVL(:new.ac_type,' ') then
     audit_trail.column_update
       (raid, 'AC_TYPE',
       :old.ac_type, :new.ac_type);
  end if;
  if nvl(:old.ac_name,' ') !=
     NVL(:new.ac_name,' ') then
     audit_trail.column_update
       (raid, 'AC_NAME',
       :old.ac_name, :new.ac_name);
  end if;
  if nvl(:old.ac_strtdt,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.ac_strtdt,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'AC_STRTDT',
       to_char(:old.ac_strtdt, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.ac_strtdt, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.ac_endt,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.ac_endt,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'AC_ENDT',
       to_char(:old.ac_endt, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.ac_endt, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.ac_stat,' ') !=
     NVL(:new.ac_stat,' ') then
     audit_trail.column_update
       (raid, 'AC_STAT',
       :old.ac_stat, :new.ac_stat);
  end if;
  if nvl(:old.ac_mailflag,' ') !=
     NVL(:new.ac_mailflag,' ') then
     audit_trail.column_update
       (raid, 'AC_MAILFLAG',
       :old.ac_mailflag, :new.ac_mailflag);
  end if;
  if nvl(:old.ac_pubflag,' ') !=
     NVL(:new.ac_pubflag,' ') then
     audit_trail.column_update
       (raid, 'AC_PUBFLAG',
       :old.ac_pubflag, :new.ac_pubflag);
  end if;
  if nvl(:old.ac_note,' ') !=
     NVL(:new.ac_note,' ') then
     audit_trail.column_update
       (raid, 'AC_NOTE',
       :old.ac_note, :new.ac_note);
  end if;
  if nvl(:old.ac_usrcreator,0) !=
     NVL(:new.ac_usrcreator,0) then
     audit_trail.column_update
       (raid, 'AC_USRCREATOR',
       :old.ac_usrcreator, :new.ac_usrcreator);
  end if;
  if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;
  if nvl(:old.last_modified_by,0) !=
     NVL(:new.last_modified_by,0) then
      Begin
       select to_char(pk_user) ||', '|| usr_lastname ||', '||usr_firstname
       into old_modby
       from er_user
       where pk_user = :old.last_modified_by ;
       Exception When no_data_found then
        old_modby := NULL;
      End ;
    Begin
     select to_char(pk_user) ||', '|| usr_lastname ||', '||usr_firstname
     into new_modby
     from er_user
     where pk_user = :new.last_modified_by ;
       Exception When no_data_found then
        new_modby := NULL;
    End ;
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       old_modby, new_modby);
  end if;
  if nvl(:old.last_modified_date,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.last_modified_date,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.created_on,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.created_on,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'CREATED_ON',
       to_char(:old.created_on, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.created_on, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.FK_CODELST_ROLE,0) !=
     NVL(:new.FK_CODELST_ROLE,0) then
     audit_trail.column_update
       (raid, 'FK_CODELST_ROLE',
       :old.FK_CODELST_ROLE, :new.FK_CODELST_ROLE);
  end if;
  if nvl(:old.IP_ADD,' ') !=
     NVL(:new.IP_ADD, ' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.IP_ADD, :new.IP_ADD);
  end if;

 --KM-032108
 if nvl(:old.AC_AUTOGEN_STUDY,0) !=
     NVL(:new.AC_AUTOGEN_STUDY,0) then
     audit_trail.column_update
       (raid, 'AC_AUTOGEN_STUDY',
       :old.AC_AUTOGEN_STUDY, :new.AC_AUTOGEN_STUDY);
  end if;

  if nvl(:old.AC_AUTOGEN_PATIENT,0) !=
     NVL(:new.AC_AUTOGEN_PATIENT,0) then
     audit_trail.column_update
       (raid, 'AC_AUTOGEN_PATIENT',
       :old.AC_AUTOGEN_PATIENT, :new.AC_AUTOGEN_PATIENT);
  end if;

END;
/
	INSERT INTO track_patches
	VALUES(seq_track_patches.nextval,391,3,'03_ER_ACCOUNT_AU0.sql',sysdate,'v11 #792');

	commit; 



