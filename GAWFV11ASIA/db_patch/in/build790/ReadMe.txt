/////*********This readMe is specific to v11 build #790**********////
*********************************************************************************************************************************************************************
VELOS PM team and MAYO has done gap analysis of Network tab enhancement and came up with  change request and new
implementation. there were around 14 items identified. Check the "ACC-39863 -- Network Tab Nov 12 2017 v0.8.docx" for Changes from Gap Analysis section.  
In  build#790 we are releasing another 1 items (5)
Earlier we have released : we have released 7 items (1,4,6,8,10,12 &13) in  build#788  and 4 items (2 7 3 9 & 11) In  build#789.

The remaining item i.e. 14 will be released in  build 791.


There are to more items we are releasing in this build. 

1. Multiple Entry form support in workflow configuration - As per PM team request to include the support for multiple entry form response, we changed the functionality of adding 'lockdown' form filled status when study is locked for submission. As per new implementation when study is in locked mode we are hiding the submit button and delete icon from user view and showing a message 'This form response cannot be submitted at this time while protocol review is pending.'


2.  I have confirmed with Priti and Stefi, you can proceed with making the change. I have cc'ed QA team as well so they will know about the change. Please note:

    The three circles shall only appear when Lind mode is on.
    The three circles shall only appear on the Protocol Build side of the protocol. Means, only on Study Details, Attachments and Check and Submit tabs.
    The three circles shall not appear on the Study Administration side of the protocol. Means, Study Details, Attachments, Study Team, Study Status and all the other tabs in the study module should not have these three circles appearing above the workflow panel.



Both these changes are came from PM team via email communication. To track these changes more efficiently, i request has been sent to PM  team for formal inclusion of these two items in the "eC-39891 -- Protocol Locking & Unlocking  Oct 05 2017 vR1.8.docx" Once we receive the latest document from PM team , we will include the same in the release notes and update QA team accordingly. 

As of now Old docuement is uploaded. 
