  declare 
  var_count number := 0;
 begin 
  select count(*) INTO var_count from user_tab_columns where table_name = 'ER_STUDYAPNDX' and COLUMN_NAME = 'STAMP_FLAG'; 
  if (var_count = 0) then
  begin
      execute immediate('ALTER TABLE ER_STUDYAPNDX  ADD STAMP_FLAG NUMBER(1)');
  end; 
    end if;
 end;
 /
 
 
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,385,08,'08_alter_studyapndx.sql',sysdate,'v11 #786');

commit;