declare
v_exists_flag number:=0;
begin
select count(*) into v_exists_flag from er_codelst where codelst_type='versionStatus' and codelst_subtyp='V';
if v_exists_flag = 0 then

  insert into er_codelst (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CREATED_ON) values
  (SEQ_ER_CODELST.nextval,'versionStatus','V','Void','N',15,sysdate);
  commit;
  
end if;
end;
/

declare
v_exists_flag number:=0;
begin
select count(*) into v_exists_flag from er_codelst where codelst_type='versionStatus' and codelst_subtyp='A';
if v_exists_flag = 0 then

  insert into er_codelst (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CREATED_ON) values
  (SEQ_ER_CODELST.nextval,'versionStatus','A','Approved','N',16,sysdate);
  commit;
  
end if;
end;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,385,09,'09_insert_verstatus.sql',sysdate,'v11 #786');

commit;