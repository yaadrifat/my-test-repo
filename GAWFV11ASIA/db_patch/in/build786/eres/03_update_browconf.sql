set define off;
declare
v_table_check number;
v_row_check number;
begin

select count(*) into v_table_check from user_tables where table_name='ER_BROWSERCONF';
if v_table_check > 0
then
  select count(*) into v_row_check from er_browserconf where fk_browser=(select pk_browser from er_browser where browser_module='irbPend') and BROWSERCONF_COLNAME='PK_SUBMISSION';
  if v_row_check > 0
  then
    update er_browserconf set BROWSERCONF_SEQ ='' where fk_browser=(select pk_browser from er_browser where browser_module ='irbPend') and BROWSERCONF_COLNAME='PK_SUBMISSION';
    update er_browserconf set BROWSERCONF_SEQ=BROWSERCONF_SEQ+1 where fk_browser=(select pk_browser from er_browser where browser_module ='irbPend') and BROWSERCONF_SEQ>2;
    update er_browserconf set BROWSERCONF_SEQ=3,BROWSERCONF_SETTINGS='{"key":"PK_SUBMISSION", "label":"Submission ID", "sortable":true, "resizeable":true,"hideable":true}',BROWSERCONF_EXPLABEL='PK_SUBMISSION' where fk_browser=(select pk_browser from er_browser where browser_module ='irbPend') and BROWSERCONF_COLNAME='PK_SUBMISSION';
  end if;
end if;
commit;
end;
/
declare
v_table_check number;
v_row_check number;
begin
select count(*) into v_table_check from user_tables where table_name='ER_BROWSERCONF';
if v_table_check > 0
then
  select count(*) into v_row_check from er_browserconf where fk_browser=(select pk_browser from er_browser where browser_module='irbSub')  and BROWSERCONF_COLNAME='PK_SUBMISSION';
  if v_row_check > 0
  then
update er_browserconf set BROWSERCONF_SEQ ='' where fk_browser=(select pk_browser from er_browser where browser_module ='irbSub') and BROWSERCONF_COLNAME='PK_SUBMISSION';
update er_browserconf set BROWSERCONF_SEQ=BROWSERCONF_SEQ+1 where fk_browser=(select pk_browser from er_browser where browser_module ='irbSub') and BROWSERCONF_SEQ>2;
update er_browserconf set BROWSERCONF_SEQ=3,BROWSERCONF_SETTINGS='{"key":"PK_SUBMISSION", "label":"Submission ID", "sortable":true, "resizeable":true,"hideable":true}',BROWSERCONF_EXPLABEL='PK_SUBMISSION' where fk_browser=(select pk_browser from er_browser where browser_module ='irbSub') and BROWSERCONF_COLNAME='PK_SUBMISSION';
end if;
end if;
commit;
end;
/

declare
v_table_check number;
v_row_check number;
begin
select count(*) into v_table_check from user_tables where table_name='ER_BROWSERCONF';
if v_table_check > 0
then
  select count(*) into v_row_check from er_browserconf where fk_browser=(select pk_browser from er_browser where browser_module='irbReview')  and BROWSERCONF_COLNAME='PK_SUBMISSION';
  if v_row_check > 0
  then
update er_browserconf set BROWSERCONF_SEQ ='' where fk_browser=(select pk_browser from er_browser where browser_module ='irbReview') and BROWSERCONF_COLNAME='PK_SUBMISSION';
update er_browserconf set BROWSERCONF_SEQ=BROWSERCONF_SEQ+1 where fk_browser=(select pk_browser from er_browser where browser_module ='irbReview') and BROWSERCONF_SEQ>2;
update er_browserconf set BROWSERCONF_SEQ=3,BROWSERCONF_SETTINGS='{"key":"PK_SUBMISSION", "label":"Submission ID", "sortable":true, "resizeable":true,"hideable":true}',BROWSERCONF_EXPLABEL='PK_SUBMISSION' where fk_browser=(select pk_browser from er_browser where browser_module ='irbReview') and BROWSERCONF_COLNAME='PK_SUBMISSION';
end if;
end if;
commit;
end;
/


declare
v_table_check number;
v_row_check number;
begin
select count(*) into v_table_check from user_tables where table_name='ER_BROWSERCONF';
if v_table_check > 0
then
  select count(*) into v_row_check from er_browserconf where fk_browser=(select pk_browser from er_browser where browser_module='irbMeeting')  and BROWSERCONF_COLNAME='PK_SUBMISSION';
  if v_row_check > 0
  then
update er_browserconf set BROWSERCONF_SEQ ='' where fk_browser=(select pk_browser from er_browser where browser_module ='irbMeeting') and BROWSERCONF_COLNAME='PK_SUBMISSION';
update er_browserconf set BROWSERCONF_SEQ=BROWSERCONF_SEQ+1 where fk_browser=(select pk_browser from er_browser where browser_module ='irbMeeting') and BROWSERCONF_SEQ>2;
update er_browserconf set BROWSERCONF_SEQ=3,BROWSERCONF_SETTINGS='{"key":"PK_SUBMISSION", "label":"Submission ID", "sortable":true, "resizeable":true,"hideable":true}',BROWSERCONF_EXPLABEL='PK_SUBMISSION' where fk_browser=(select pk_browser from er_browser where browser_module ='irbMeeting') and BROWSERCONF_COLNAME='PK_SUBMISSION';
end if;
end if;
commit;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,385,03,'03_update_browconf.sql',sysdate,'v11 #786');

commit;



