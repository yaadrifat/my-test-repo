create or replace FUNCTION "F_CODELST_SUBTYP" (p_id VARCHAR2)
RETURN VARCHAR2
IS
  v_codelst_subtyp VARCHAR2(200);
BEGIN
	 SELECT codelst_subtyp INTO v_codelst_subtyp FROM ER_CODELST WHERE pk_codelst = p_id;
  RETURN v_codelst_subtyp ;
END ;

/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,385,04,'04_f_codelst_subtyp.sql',sysdate,'v11 #786');

commit;
