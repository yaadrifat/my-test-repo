Declare
v_column_exists number;

Begin
  Select count(*) into v_column_exists
  from user_tab_cols where TABLE_NAME = 'ER_STUDY'
  AND COLUMN_NAME = 'STUDY_NETWORK';
  
  IF(v_column_exists = 0) THEN
	execute immediate 'ALTER TABLE er_study ADD study_network varchar2(4000)';
	execute immediate 'COMMENT ON COLUMN ER_STUDY.STUDY_NETWORK IS ''This column stores the PK of er_nwsites table''';
  END IF;
  Commit;
  
END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,385,14,'14_er_study.sql',sysdate,'v11 #786');

commit;
