/////*********This readMe is specific to v10 build #769	  **********////


We have received Four Priority items  from support team to address for UPENN. As per inputs from Sonia, these were put on Priority by Dr. Chahal.Development team has worked on these issues and found that they are Code level fixes. Since UPENN is under v10 Cloud (Build#712) , We have no option left  but to include them in Build#769.
Following are the four items for your reference.

1. Vtiger  Ticket#37306 :  Audit Tracking - IP Address being deleted           BugZilla No:#24451
2. Vtiiger Ticket#36966 :  IND/IDE assigned to fk_study = 0                          BugZilla No:#24258
3. Vtiger  Ticket #36318.  Query history showing incorrect Status info        BugZilla No:#15237
4. Vtiger  Ticket#36312 -- Report Adverse Event Summary                            BugZilla No: Not reported yet
      Desc: After the Upenn ACC production environment was upgrade to v9.3 LE, the following error was reported for the Report "Adverse Event Summary":
2015-10-18 11:27:07,214 FATAL [Report] (http-accvelos.med.upenn.edu%2F172.16.100.210-8443-13) EXCEPTION in ReportDaONew:createReport, excecuting Stored Procedure java.sql.SQLException: ORA-00979: not a GROUP BY expression

All these items were included in Build#769 for QA verification.