SET DEFINE OFF;

update er_milestone set 
FK_CODELST_MILESTONE_STAT = (select pk_codelst from er_codelst
WHERE codelst_type = 'milestone_stat'
  AND codelst_subtyp = 'A')
where pk_milestone in
(select pk_milestone from er_milestone,er_invoice_detail,er_invoice 
where pk_milestone = fk_milestone
AND PK_INVOICE = FK_INV
AND milestone_type='AM'
AND NVL(MILESTONE_DELFLAG,0)<>'Y'
AND FK_CODELST_MILESTONE_STAT   =
  (SELECT pk_codelst
  FROM er_codelst
  WHERE codelst_type = 'milestone_stat'
  AND codelst_subtyp = 'WIP'
  ) );

commit;
/