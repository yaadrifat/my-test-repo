create or replace
TRIGGER "ERES"."ER_FORMSEC_AU4" 
AFTER UPDATE OF FORMSEC_FMT
ON ER_FORMSEC
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
v_formsec_old_fmt CHAR(1) ;
v_formsec_new_fmt CHAR(1) ;
v_fk_formfld NUMBER ;
v_formlib_id NUMBER ;
v_fk_field_id_old NUMBER ;
v_fk_field_id_new NUMBER ;
v_fld_systemid_old  VARCHAR2(50) ;
v_fld_systemid_new VARCHAR2(50) ;
v_formfld_xsl   VARCHAR2(4000);
v_formfld_xsl_init VARCHAR2(4000);
v_formfld_xsl_1   VARCHAR2(4000);
v_formfld_xsl_2   VARCHAR2(4000);
v_formfld_xsl_3   VARCHAR2(4000);
v_pos_1  NUMBER ;
v_pos_2  NUMBER ;
v_pos_3  NUMBER ;
v_pos_endtd NUMBER;
len NUMBER ;
v_strlen NUMBER ;
v_fld_type CHAR(1) ;


BEGIN
    v_formsec_old_fmt := :OLD.formsec_fmt ;
    v_formsec_new_fmt := :NEW.formsec_fmt ;
     IF v_formsec_new_fmt = 'T'   THEN
	  IF v_formsec_old_fmt = 'N' THEN
        FOR i IN ( SELECT pk_repformfld, repformfld_xsl , fld_type,fld_datatype,fld_align,pk_field,fk_formfld
		  	  	     FROM ER_REPFORMFLD , ER_FLDLIB
					 WHERE fk_formsec = :NEW.pk_formsec AND ER_REPFORMFLD.record_type <> 'D' AND
					 fk_field = pk_field AND fld_type <> 'C' AND NVL(fld_datatype,'Z') <> 'ML' AND fld_type <> 'F'  AND fld_type <> 'S' )
		LOOP
              SELECT formfld_xsl
               INTO v_formfld_xsl_1 
  	           FROM ER_FORMFLD WHERE pk_formfld = i.fk_formfld ;


			   		--modified by sonia abrol, 03/16/05 to use a function to cut the label

			 		 SELECT Pkg_Form.f_cut_fieldlabel(v_formfld_xsl_1,v_fld_type, i.fld_datatype,i.fld_align,i.pk_field )
			 		 INTO v_formfld_xsl  FROM dual;

					 /*
					 --modified by Sonika on Nov 21, 03
					 --search only for <td width=''15%'' as value of align cannot be hardcoded
					 --modified by Sonika on April 19, 04 search only for <td
					          v_pos_1 := INSTR( v_formfld_xsl_1, '<td') ;
							  v_formfld_xsl_2 := SUBSTR(v_formfld_xsl_1 , 0 ,v_pos_1-1);

							  len := LENGTH(v_formfld_xsl_1);
							   v_pos_2 := INSTR(v_formfld_xsl_1 , '</label>');
					   		 --Modified by Sonika Talwar on April 22, 04 to remove help icon addition incase of Tabular section
						     --v_strlen := length( '</label>  &#xa0;&#xa0;&#xa0;</td>' ); --important line
							  --find </td> end tag of label
							   v_pos_endtd := INSTR(v_formfld_xsl_1 , '</td>',v_pos_2);
							    --# of characters between </label> and </td>
								v_strlen := LENGTH('</td>');
								 v_strlen := (v_pos_endtd - v_pos_2) + v_strlen;
						  	     v_formfld_xsl_3 := SUBSTR( v_formfld_xsl_1 , v_pos_2+v_strlen , len-1 ) ;
								 v_formfld_xsl := v_formfld_xsl_2 || v_formfld_xsl_3 ; */

		 UPDATE ER_REPFORMFLD
		 SET  repformfld_xsl = v_formfld_xsl
  		 WHERE pk_repformfld = i.pk_repformfld ;
		END LOOP ;
	   END IF;
     ELSIF v_formsec_new_fmt = 'N' THEN
	   IF v_formsec_old_fmt = 'T'  THEN
		  FOR i IN ( SELECT pk_repformfld, fk_formfld, fk_field , fld_type
		  	  	     FROM   ER_REPFORMFLD , ER_FLDLIB WHERE fk_formsec = :NEW.pk_formsec AND
					 fk_field = pk_field AND fld_type <> 'C' AND NVL(fld_datatype,'Z') <> 'ML' AND fld_type <> 'F' AND fld_type <> 'S')
	      	LOOP
		     -- Get the FK_FORMFLD to get the old XSL
			   v_fk_formfld := i.fk_formfld;
              -- Get the old FORMFLD_XSL from the ER_FORMFLD corresponding to the fk got from above
        	   SELECT formfld_xsl , fk_field
               INTO v_formfld_xsl_init , v_fk_field_id_old
  	           FROM ER_FORMFLD WHERE pk_formfld = v_fk_formfld ;
              --get the old fld_systemid
	 	      SELECT fld_systemid
              INTO v_fld_systemid_old
  	         FROM ER_FLDLIB WHERE pk_field = v_fk_field_id_old ;
              --get the new fld_systemid
  		      v_fk_field_id_new := i.fk_field;
               --get the old fld_systemid
	 	     SELECT fld_systemid
             INTO v_fld_systemid_new
  	         FROM ER_FLDLIB WHERE pk_field = v_fk_field_id_new ;
             v_formfld_xsl := REPLACE(v_formfld_xsl_init ,  v_fld_systemid_old , v_fld_systemid_new ) ;



		    UPDATE ER_REPFORMFLD
			SET  repformfld_xsl = v_formfld_xsl
  		    WHERE pk_repformfld = i.pk_repformfld ;
		END LOOP ;
		END IF;
        END IF ;
      	-- To set the FORM_XSLREFRESH  to 1 after the change of the repeat number
    v_formlib_id := :NEW.fk_formlib;
    UPDATE ER_FORMLIB
    SET FORM_XSLREFRESH = 1
    WHERE pk_formlib = v_formlib_id  ;
END ;

/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,372,1,'01_ER_FORMSEC_AU4.sql',sysdate,'v10 #773');

commit;
