set define off;
DECLARE 
v_column_exists NUMBER := 0;
BEGIN
  SELECT COUNT(*)
  INTO v_column_exists
  FROM ER_REPFILTERMAP,ER_REPFILTER
  WHERE  PK_REPFILTER=FK_REPFILTER
  and REPFILTER_KEYWORD='orgId';
  
  IF (v_column_exists > 0) THEN
  update ER_REPFILTER set REPFILTER_COLUMN=
		
			'<td><DIV id="orgDIV"><FONT class = "comments"><A href="javascript:openLookup(document.reports,''viewId=6014'||'&'||'form=reports'||'&'||'seperator=,'||'&'||'keyword=selorgId|site_name~paramorgId|pk_site|[VELHIDE]'','''')">Select Organization</A> </FONT></DIV> </td>
	       	<td><DIV id=''orgdataDIV''><Input TYPE="text" NAME="selorgId" SIZE="20" READONLY VALUE="[SESSSITENAME]">
	       	<Input TYPE="hidden" NAME="paramorgId" VALUE="[SESSSITEID]"></DIV>
	       	</td>'
			where  REPFILTER_KEYWORD='orgId';
  END IF;
  
  COMMIT;
END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,437,1,'01_01_update_repfilter_org.sql',sysdate,'v11 #838');
/
commit;	
/