SET DEFINE OFF;

--irb
declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat_type' AND CODELST_SUBTYP = 'irb';	
	IF (v_CodeExists = 0) THEN
		Insert into er_codelst (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,LAST_MODIFIED_DATE,CREATED_ON,CODELST_STUDY_ROLE) 
    values (seq_er_codelst.nextval,'studystat_type','irb','IRB','N',15,to_date(sysdate,'DD-MON-RR'),to_date(sysdate,'DD-MON-RR'),'default_data');
    commit;
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--budget
declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat_type' AND CODELST_SUBTYP = 'budget';	
	IF (v_CodeExists = 0) THEN
		Insert into er_codelst (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,LAST_MODIFIED_DATE,CREATED_ON,CODELST_STUDY_ROLE) 
    values (seq_er_codelst.nextval,'studystat_type','budget','Budget','N',5,to_date(sysdate,'DD-MON-RR'),to_date(sysdate,'DD-MON-RR'),'default_data');
    commit;
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--contract
declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat_type' AND CODELST_SUBTYP = 'contract';	
	IF (v_CodeExists = 0) THEN
		Insert into er_codelst (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,LAST_MODIFIED_DATE,CREATED_ON,CODELST_STUDY_ROLE) 
    values (seq_er_codelst.nextval,'studystat_type','contract','Contract','N',10,to_date(sysdate,'DD-MON-RR'),to_date(sysdate,'DD-MON-RR'),'default_data');
    commit;
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--trial_stat
declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat_type' AND CODELST_SUBTYP = 'trial_stat';	
	IF (v_CodeExists = 0) THEN
		Insert into er_codelst (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,LAST_MODIFIED_DATE,CREATED_ON,CODELST_STUDY_ROLE) 
    values (seq_er_codelst.nextval,'studystat_type','trial_stat','Trial Status','N',21,to_date(sysdate,'DD-MON-RR'),to_date(sysdate,'DD-MON-RR'),'default_data');
    commit;
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--trial_stat
declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat_type' AND CODELST_SUBTYP = 'trial_stat';	
	IF (v_CodeExists = 0) THEN
		Insert into er_codelst (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,LAST_MODIFIED_DATE,CREATED_ON,CODELST_STUDY_ROLE) 
    values (seq_er_codelst.nextval,'studystat_type','trial_stat','Trial Status','N',21,to_date(sysdate,'DD-MON-RR'),to_date(sysdate,'DD-MON-RR'),'default_data');
    commit;
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--Misc
declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat_type' AND CODELST_SUBTYP = 'Misc';	
	IF (v_CodeExists = 0) THEN
		Insert into er_codelst (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,LAST_MODIFIED_DATE,CREATED_ON) 
    values (seq_er_codelst.nextval,'studystat_type','Misc','Miscellaneous','N',24,to_date(sysdate,'DD-MON-RR'),to_date(sysdate,'DD-MON-RR'));
    commit;
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--reg_apv
declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat_type' AND CODELST_SUBTYP = 'reg_apv';	
	IF (v_CodeExists = 0) THEN
		Insert into er_codelst (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,LAST_MODIFIED_DATE,CREATED_ON,CODELST_STUDY_ROLE) 
    values (seq_er_codelst.nextval,'studystat_type','reg_apv','Regulatory Approvals','N',21,to_date(sysdate,'DD-MON-RR'),to_date(sysdate,'DD-MON-RR'),'default_data');
    commit;
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--departmental
declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat_type' AND CODELST_SUBTYP = 'departmental';	
	IF (v_CodeExists = 0) THEN
		Insert into er_codelst (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,LAST_MODIFIED_DATE,CREATED_ON,CODELST_STUDY_ROLE) 
    values (seq_er_codelst.nextval,'studystat_type','departmental','Departmental','N',10,to_date(sysdate,'DD-MON-RR'),to_date(sysdate,'DD-MON-RR'),'default_data');
    commit;
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--Financial
declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat_type' AND CODELST_SUBTYP = 'Financial';	
	IF (v_CodeExists = 0) THEN
		Insert into er_codelst (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,LAST_MODIFIED_DATE,CREATED_ON,CODELST_STUDY_ROLE) 
    values (seq_er_codelst.nextval,'studystat_type','Financial','Financial Budget','N',15,to_date(sysdate,'DD-MON-RR'),to_date(sysdate,'DD-MON-RR'),'default_data');
    commit;
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

--Regulatory
declare
v_CodeExists number := 0;
begin
	
	---1---
	
	SELECT COUNT(*) INTO v_CodeExists FROM ER_CODELST WHERE CODELST_TYPE = 'studystat_type' AND CODELST_SUBTYP = 'Regulatory';	
	IF (v_CodeExists = 0) THEN
		Insert into er_codelst (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,LAST_MODIFIED_DATE,CREATED_ON,CODELST_STUDY_ROLE) 
    values (seq_er_codelst.nextval,'studystat_type','Regulatory','Financial CA','N',15,to_date(sysdate,'DD-MON-RR'),to_date(sysdate,'DD-MON-RR'),'default_data');
    commit;
		dbms_output.put_line('One row inserted');
	ELSE
		dbms_output.put_line('Row already exists');
	END IF;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,358,5,'05_studystattype_insert.sql',sysdate,'v10 #759');

commit;